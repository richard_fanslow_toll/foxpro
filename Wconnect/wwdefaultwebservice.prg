************************************************************************
* wwWebService :: wwWebService
****************************************
***  Function: Web Service Process Loader. Loads teh default
***    Assume:
***      Pass:
***    Return:
************************************************************************
LPARAMETER loServer
LOCAL loProcess

#INCLUDE WCONNECT.H

loProcess=CREATE("wwDefaultWebService",loServer)

*** Call the Process Method that handles the request
loProcess.Process()

RETURN


*************************************************************
DEFINE CLASS wwDefaultWebService AS WWC_WEBSERVICE
*************************************************************
* wwWebService class is defined in 

* Sample WebService Method - just add regular methods
FUNCTION TestWebService(lcName)
RETURN "Hello " + lcName + ". Time is: " + TRANSFORM(DATETIME())


ENDDEFINE
*EOC wwDemoWebService 
