#INCLUDE WCONNECT.H

*************************************************************
DEFINE CLASS wwHTMLControl AS Relation
*************************************************************
***    Author: Rick Strahl
***            (c) West Wind Technologies, 1996
***   Contact: (541) 386-2087  / rstrahl@west-wind.com
***  Modified: 11/02/96
***  Function:
*************************************************************

*** Custom Properties
oReference=.NULL.
cFullObjectName=""
oHTML=.NULL.

* 1 - actual value  2 - blank  3 -  <%= ControlSource %>  
nValueType = 1
lUseControlSourceAsName=.F.

*** Stock Properties

************************************************************************
* wwHTMLControl :: Init
*********************************
***  Function: Assign the object reference
************************************************************************
FUNCTION Init
LPARAMETERS loObject, loHTML
LOCAL lcFullName, lnParms

lnParms = PARAMETERS()

*** If we're passing in a control object use its properties
IF lnParms > 0
  THIS.oReference=loObject
ELSE
  RETURN .f.  
ENDIF
IF lnParms > 1
  THIS.oHTML=loHTML
ENDIF  

*** Use the full object name minus the first reference which will
*** be our object reference (ie. OFORM.Container1.TxtTest becomes Container1.txtTest)
lcFullName=SYS(1272,loObject)

THIS.cFullObjectName= CHRTRAN( SUBSTR(lcFullName,AT(".",lcFullName) + 1),".","_")

ENDFUNC
* Init

************************************************************************
* wwHTMLControl :: SetControlSourceAsName
*****************************************
***  Function: Uses the control source as a name instead of
***            the FormField name.
***            Call this method once to set the form operation.
***            This method is called internally to set each object's
***            name.
***    Assume: Slower than using field names!
************************************************************************
FUNCTION SetControlSourceAsName

IF  TYPE("THIS.oReference.ControlSource")#"U" AND ;
    !EMPTY(THIS.oReference.ControlSource)
   THIS.cFullObjectName= THIS.oReference.ControlSource
ENDIF   

THIS.lUseControlSourceAsName=.T.  

ENDFUNC
* SetControlSourceAsName



FUNCTION Destroy
THIS.oReference = .NULL.
ENDFUNC


************************************************************************
* wwHTMLControl :: Send
*********************************
***  Function:
***    Assume:
***      Pass:
***    Return:
************************************************************************
FUNCTION Send
LPARAMETER lcOutput
THIS.oHTML.FastWrite(lcOutput)
RETURN ""
ENDFUNC
* Send 

************************************************************************
* HTMLControl :: GetHTML
*********************************
***  Function: Retrieves the HTML text for the object based on previous
***            property settings
***      Pass:
***    Return: HTML text for the object
************************************************************************
FUNCTION GetHTML
RETURN ""
* GetHTML

************************************************************************
* HTMLControl :: IsProperty
*********************************
***  Function: Checks to see if a given property exists
***      Pass: lcProperty  -  the property to check
***    Return: .T. or .F.
************************************************************************
FUNCTION IsProperty
LPARAMETERS lcProperty
lcProperty=IIF(EMPTY(lcProperty),"",lcProperty)
RETURN PEMSTATUS(THIS.oReference,lcProperty,5)
* IsProperty


************************************************************************
* HTMLControl :: AlignStyle
*********************************
***  Function:
***    Assume:
***      Pass:
***    Return:
************************************************************************
FUNCTION AlignStyle
LOCAL loObject

loObject=THIS.oReference
IF loObject.Alignment # 3 
   DO CASE
   CASE loObject.Alignment = 1
      lcTextAlign="text-align:right"
   CASE loObject.Alignment = 2
      lcTextAlign="text-align:center"
   OTHERWISE
      lcTextAlign=""
   ENDCASE   
ELSE
  RETURN ""   
ENDIF
RETURN lcTextAlign
* EOF GetAlignment

************************************************************************
* HTMLControl :: PositionStyle
*********************************
***  Function: Returns the Positioning style tags from an object
************************************************************************
FUNCTION PositionStyle
LPARAMETER loObject

IF VARTYPE(loObject) # "O"
   loObject=THIS.oReference
ENDIF   

RETURN [LEFT:]+TRANS(loObject.Left)+[;TOP:]+TRANS(loObject.Top)+;
       [;WIDTH:]+TRANS(loObject.Width)+[;HEIGHT:]+TRANS(loObject.Height) 
* PositionStyle

************************************************************************
* HTMLControl :: FontStyle
*********************************
***  Function: Builds the Font String for an object style tag
************************************************************************
FUNCTION FontStyle
LPARAMETER loObject

IF VARTYPE(loObject) # "O"
   loObject=THIS.oReference
ENDIF   

RETURN "Font:" + ;
              iif(loObject.Fontitalic,"italic","normal") + " "+;
              iif(loObject.FontBold,"bold","normal") + " "+;
              LTRIM(STR(loObject.FontSize)) +"pt " +;
               loObject.FontName 
             
ENDFUNC
* FontStyle

************************************************************************
* HTMLControl :: ColorStyle
*********************************
***  Function: Sets all the color and visibility aspects of objects
***      Pass: loObject
***    Return: String of attributes
************************************************************************
FUNCTION ColorStyle
LPARAMETER loObject

IF VARTYPE(loObject) # "O"
   loObject=THIS.oReference
ENDIF   

RETURN  [Color:]+ HTMLColor(loObject.Forecolor) +;
        IIF(loObject.BackStyle=1,[;BACKGROUND:]+HTMLColor(loObject.BackColor),[]) +;
        IIF(!loObject.visible,";DISPLAY:none;","")
ENDFUNC
* ColorStyle


************************************************************************
* HTMLControl :: CharValue
*********************************
***  Function: Converts any type of argument to a text variable.
************************************************************************
FUNCTION CharValue
LPARAMETERS lvValue
LOCAL lvValue, lcValue

lvValue=IIF(vartype(lvValue)="C",lvValue,loObject.Value)
lcFieldType=VARTYPE(lvValue)

DO CASE
         CASE THIS.nValueType = 2  && Blank Values always
            lcValue = ""
         CASE THIS.nValueType = 3  && Insert ControlSource
			IF  TYPE("THIS.oReference.ControlSource")#"U" AND ;
			    !EMPTY(THIS.oReference.ControlSource)
			   lcValue = "<%= " + THIS.oReference.ControlSource + " %>"
			else
			   lcValue = ""
			ENDIF   
         CASE ISNULL(lvValue)
            lcValue="n/a"
         CASE lcfieldtype="C" OR lcfieldtype="M"
            lcValue=TRIM(lvValue) 
         CASE lcfieldtype $ "NFBIY"
#IF wwVFPVersion > 5
            lcValue=TRANSFORM(lvValue)
#ELSE
            lcValue=TRANSFORM(lvValue,"")
#ENDIF
         CASE lcfieldtype="L"
            lcValue=IIF(lvValue,"True","False")
         CASE lcfieldtype="D"
            lcValue=DTOC(lvValue)
         CASE lcfieldtype="T"
            lcValue=lower(ttoc(lvValue))
ENDCASE
         
RETURN lcValue
* EOF CharValue

************************************************************************
* HTMLControl :: SetValue
*********************************
***  Function: Sets the value/caption of this control to the value
***            passed. Use this to repopulate a form from the 
***            values returned by a wwCGI object and an HTML form
***    Assume: This is the generic function.
***      Pass: lvValue    -    The value to set the control to
***                            this will most likely be a GetFormVar()
***                            value
***    Return: nothing
************************************************************************
FUNCTION SetValue
LPARAMETERS lvValue

*** Null values simply are not set - IOW leave the field alone!
IF lvValue=WWC_NULLSTRING
   RETURN
ENDIF

lcType=VARTYPE(THIS.oReference.value)

DO CASE
   *** Convert values from text -> proper type
   CASE lcType = "C" or lcType="M"  
         THIS.oReference.value = lvValue
   CASE lcType $ "NIBFY"
         THIS.oReference.value = VAL(lvValue)
   CASE lcType = "D"
         THIS.oReference.value = CTOD(lvValue)
   CASE lcType = "T"
         THIS.oReference.value = CTOT(lvValue)
   CASE lcType = "L"
         IF lvValue = "ON" or lvValue="YES"
            THIS.oReference.value = .T.
         ELSE
            THIS.oReference.value = .F.
         ENDIF
ENDCASE

ENDFUNC
* SetValue

ENDDEFINE
*EOC HTMLControl

*************************************************************
DEFINE CLASS wwHTMLLabel AS WWC_wwHTMLControl
*************************************************************
***    Author: Rick Strahl
***            (c) West Wind Technologies, 1997
***   Contact: (541) 386-2087  / rstrahl@west-wind.com
***  Modified: 04/13/97
***
***  Function:
*************************************************************

************************************************************************
* HTMLLabelControl :: GetHTML
*********************************
***  Function: Retrieves the HTML text for the object based on previous
***            property settings
***      Pass:
***    Return:
************************************************************************
FUNCTION GetHTML
LOCAL lcOutput, lcValue, lnFontSize, loHTML

loObject=THIS.oReference
lcValue = STRTRAN(loOBject.Caption,"\<","")

RETURN THIS.oHTML.FastWrite([<DIV CLASS="TEXT" ]+IIF(loObject.Alignment=1,"ALIGN=RIGHT","ALIGN=LEFT") + ;
  [ STYLE="POSITION:ABSOLUTE;]+;
  THIS.ColorStyle() + [;]+ ;
  THIS.PositionStyle() + [;] +;
  THIS.FontStyle()+ ;
   + [">]+;
  lcValue + [</DIV>] + CRLF)
* GetHTML


ENDDEFINE
*EOC wwHTMLLabel


*************************************************************
DEFINE CLASS wwHTMLTextBox AS WWC_wwHTMLControl
*************************************************************
***    Author: Rick Strahl
***            (c) West Wind Technologies, 1996
***   Contact: (541) 386-2087  / rstrahl@west-wind.com
***  Modified: 11/02/96
***  Function: Renders a TextBox object as DHTML
*************************************************************

************************************************************************
* wwHTMLTextBox :: GetHTML
*********************************
***  Function: Retrieves the HTML text for the object based on previous
***            property settings
***      Pass:
***    Return:
************************************************************************
FUNCTION GetHTML
LPARAMETER llSpinner
LOCAL lcOutput, lcValue, lcColorStyle

loObject = THIS.oReference

lcColorStyle = [Color:]+ HTMLColor(loObject.Forecolor) +;
        IIF(!llSpinner and loObject.BackStyle=1,[;BACKGROUND:]+HTMLColor(loObject.BackColor),[]) +;
        IIF(!loObject.visible,";DISPLAY:none;","")

lcOutput=[<INPUT NAME="] +THIS.cFullObjectName+[" ID="]+THIS.cFullObjectName+["] + ;
  IIF(!llSpinner AND !EMPTY(loObject.PassWordChar),[ TYPE="PASSWORD"],[])+ ;
  IIF(!loObject.enabled OR loObject.ReadOnly," DISABLED","") + ;
  [ STYLE="POSITION:ABSOLUTE;]+ THIS.PositionStyle()+;
  IIF(!loObject.visible,";DISPLAY:none","") + [;] + THIS.AlignStyle()+ ;
  [;]+THIS.FontStyle()+;
  [;]+lcColorStyle + ["] 
  
*!*	IF !EMPTY(loObject.Format)
*!*     *** This code doesn't work right due to VFP Forms
*!*     *** not returning @ in format strings.
*!*	    lcValue=TRANS(loObject.Value,loObject.Format)
*!*	ELSE   
	*** Convert the value of any type to a character value
	lcValue=THIS.CharValue(loObject.Value)
*!* ENDIF

*** And add to the output string
IF !EMPTY(lcValue)
   lcOutput=lcOutput + [ VALUE="]+ALLTRIM(lcValue)+["]
ENDIF
IF loObject.Maxlength > 0
	lcOutput=lcOutput + [ MAXLENGTH="] + TRANSFORM(loObject.Maxlength) + ["]
ENDIF

RETURN THIS.oHTML.FastWrite(lcOutput + ">" + CRLF)
* GetHTML

ENDDEFINE
* wwHTMLTextBox




*************************************************************
DEFINE CLASS wwHTMLEditBox AS WWC_wwHTMLControl
*************************************************************
***    Author: Rick Strahl
***            (c) West Wind Technologies, 1996
***   Contact: (541) 386-2087  / rstrahl@west-wind.com
***  Modified: 11/02/96
***
***  Function:
*************************************************************

*** Custom Properties
*** Stock Properties



************************************************************************
* wwHTMLEditBox :: GetHTML
*********************************
***  Function: Retrieves the HTML text for the object based on previous
***            property settings
***      Pass:
***    Return:
************************************************************************
FUNCTION GetHTML
LOCAL lcOutput, lcValue

loObject=THIS.oReference

lcOutput=[<TEXTAREA NAME="] +THIS.cFullObjectName+[" ID="]+THIS.cFullObjectName+["] +;
  IIF(!loObject.enabled," DISABLED","") + ;
  [ STYLE="POSITION:ABSOLUTE;]+;
  THIS.PositionStyle()+[;]+;
  THIS.ColorStyle() +[;]+;
  THIS.FontStyle()+["] 

* lcOutput=lcOutput + [ SIZE="]+LTRIM(STR(loObject.Width))+["]

*** Convert the value of any type to a character value
lcValue=THIS.CharValue(loObject.Value)


RETURN  THIS.oHTML.FastWrite(lcOutput + [ WRAP="Physical">] +lcValue + "</TEXTAREA>"  + CRLF)
* GetHTML

ENDDEFINE
* wwHTMLEditBox


*************************************************************
DEFINE CLASS wwHTMLButton AS WWC_wwHTMLControl
*************************************************************
***    Author: Rick Strahl
***            (c) West Wind Technologies, 1997
***   Contact: (541) 386-2087  / rstrahl@west-wind.com
***  Modified: 04/13/97
***
***  Function:
*************************************************************


************************************************************************
* HTMLButtonControl :: GetHTML
*********************************
***  Function: Retrieves the HTML text for the object based on previous
***            property settings
***      Pass:
***    Return:
************************************************************************
FUNCTION GetHTML
LOCAL lcOutput, lcValue, lnFontSize

loObject=THIS.oReference

lcOutput=[<INPUT NAME="btnSubmit" ID="btnSubmit" TYPE="SUBMIT" VALUE="]+STRTRAN(loObject.Caption,"\<","")+["]+;
  IIF(!loObject.enabled," DISABLED","") + ;
  [ STYLE="POSITION:ABSOLUTE;]+THIS.PositionStyle() +;
  IIF(!loObject.visible,";DISPLAY:none","") + ;
  [;] + THIS.FontStyle() + [">] 

IF !EMPTY(loObject.Picture)
  lcImage=STRTRAN( justfname( lower(loObject.Picture) ),".bmp",".gif")
  lcImage=STRTRAN(lower(lcImage),".ico",".gif")
  lcOutput =  lcOutput + [<INPUT TYPE="Image" NAME="img] +THIS.cFullObjectName+[" ID="img]+THIS.cFullObjectName+[" SRC="]+WWFORM_IMAGEPATH+lcImage+["]+;
              [ STYLE="POSITION:ABSOLUTE;]+THIS.PositionStyle() +;
              IIF(!loObject.visible,";DISPLAY:none","") + [;BORDER:1px Solid Black" OnClick="btnSubmit.value='wconnect.gif'">]
ENDIF              
		
RETURN THIS.oHTML.FastWrite(lcOutput)
* GetHTML

ENDDEFINE
* EOC wwHTMLButton


*************************************************************
DEFINE CLASS wwHTMLListBox AS WWC_wwHTMLControl
*************************************************************
***    Author: Rick Strahl
***            (c) West Wind Technologies, 1997
***   Contact: (541) 386-2087  / rstrahl@west-wind.com
***  Modified: 04/13/97
***
***  Function:
*************************************************************

*** Custom Properties
lUseListItem2AsValue = .F.

*** Stock Properties

************************************************************************
* HTMLListControl :: GetHTML
*********************************
***  Function: Retrieves the HTML text for the object based on previous
***            property settings
***      Pass:
***    Return:
************************************************************************
FUNCTION GetHTML
LOCAL lcOutput, lcValue, x, loList

loObject=THIS.oReference

llCombo=UPPER(loObject.BaseClass)="COMBOBOX"

lcOutput=CRLF+[<SELECT NAME="]+THIS.cFullObjectName+[" ID="]+THIS.cFullObjectName+;
  [" SIZE="]+LTRIM(STR(IIF(llCombo,1,loObject.Height)))+["] +;
  IIF(!loObject.enabled," DISABLED","") + ;
  [ STYLE="POSITION:ABSOLUTE;]+THIS.PositionStyle() +;
  IIF(!loObject.visible,";DISPLAY:none","") + ;
  [;]+THIS.FontStyle() + ["]

IF !llCombo and loObject.MultiSelect
     lcOutput=lcOutput + [ MULTIPLE ]
ENDIF
IF loObject.TerminateRead
	lcOutput=lcOutput + [ onchange="this.form.submit();return true;"]
ENDIF

lcOutput=lcOutput+[>]+CRLF

IF THIS.nValueType < 2
	*** Convert the value of any type to a character value
	*** For comparison
	*** ///ras - modified 09/28/98
	***          Not needed. Using loList.Selected[x] instead
	***          which should be much faster and more reliable
	*lcValue=UPPER(TRIM(THIS.CharValue(loObject.Value) )) 

	loList=loObject

    lcValue = ""
	FOR x=1 to loList.ListCount
	  lcText=TRIM(loList.ListItem[x,1])
      
      IF THIS.lUseListItem2AsValue AND INLIST(lolist.rowsourcetype,6,3,5,2)
         lcValue = [ value="] + ALLTRIM(loList.ListItem[x,2]) + ["]
      ENDIF
      
      IF loList.Selected[x]
	     lcOutput=lcOutput+[<OPTION] + lcValue + [ SELECTED>]+lcText+CRLF
	  ELSE
	     lcOutput=lcOutput+[<OPTION] + lcValue + [>]+lcText+CRLF
	  ENDIF
	ENDFOR
ENDIF

lcOutput=lcOutput + "</SELECT>" + CRLF

RETURN THIS.oHTML.FastWrite(lcOutput)
* GetHTML

************************************************************************
* HTMLListControl :: FontStyle
*********************************
***  Function: Builds the Font String for an object style tag
***  TEMPORARY: OVERRIDE FONTSIZE OPTION WHICH DOESN"T WORK IN 
***             PREVIEW 2
************************************************************************
*!*	FUNCTION FontStyle
*!*	loObject=THIS.oReference
*!*	RETURN "Font: " + ;
*!*	              iif(loObject.Fontitalic,"italic","normal") + " "+;
*!*	              iif(loObject.FontBold,"bold","normal") + " "+;
*!*	              "'"+ loObject.FontName + "'" 
*!*	             
*!*	ENDFUNC
* FontStyle

ENDDEFINE
* EOC HTMLLISTBOX

*************************************************************
DEFINE CLASS wwHTMLCheckBox AS WWC_wwHTMLControl
*************************************************************
***    Author: Rick Strahl
***            (c) West Wind Technologies, 1996
***   Contact: (541) 386-2087  / rstrahl@west-wind.com
***  Modified: 11/02/96
***
***  Function:
*************************************************************

*** Custom Properties
*** Stock Properties


************************************************************************
* wwHTMLCheckBox :: GetHTML
*********************************
***  Function: Retrieves the HTML text for the object based on previous
***            property settings
***      Pass:
***    Return:
************************************************************************
FUNCTION GetHTML
LOCAL lcOutput, lcValue

loObject=THIS.oReference

lcOutput=[<INPUT TYPE="CHECKBOX" NAME="] +THIS.cFullObjectName+[" ID="]+THIS.cFullObjectName+["]  + IIF(!loObject.enabled," DISABLED","")

lcChecked=""
IF TYPE("loObject.Value")="N"
     lcChecked=IIF(loObject.value=1," CHECKED","")
ENDIF
IF TYPE("loObject.Value")="L"
     lcChecked=IIF(loObject.value=.T.," CHECKED ","")
ENDIF

lcOutput=lcOutput + [ VALUE="ON"]+lcChecked

lcOutput = lcOutput + [ STYLE="POSITION:ABSOLUTE;]+ ;
				      [LEFT:]+LTRIM(STR(loObject.Left))+[;TOP:]+LTRIM(STR(loObject.Top))+;
				      [;HEIGHT:]+LTRIM(STR(loObject.Height))  + ";" + ;
                      IIF(!loObject.visible,";DISPLAY:none;","") + ["]

*!*	lcOutput = lcOutput + [ STYLE="POSITION:ABSOLUTE;]+ ;
*!*	                      THIS.PositionStyle() + ;
*!*	                      IIF(!loObject.visible,";DISPLAY:none;","") + ["]


*** Call the Label Control object to display the actual label
loLabel=CREATE("wwHTMLLabel",loObject,THIS.oHTML)
loObject.left = loOBject.left + 20
loObject.Top = loObject.Top + 2
loObject.Width = loObject.Width - 20
lcLabel=loLabel.GetHTML()

loObject.left = loOBject.left - 20
loObject.Top = loObject.Top - 2
loObject.Width = loObject.Width + 20


RETURN THIS.oHTML.FastWrite(lcOutput + ">"  + lcLabel)
* GetHTML

************************************************************************
* HTMLControl :: SetValue
*********************************
***  Function: Sets the value/caption of this control to the value
***            passed. Use this to repopulate a form from the 
***            values returned by a wwCGI object and an HTML form
***    Assume: Override for CheckBox and Numeric Values
***      Pass: lvValue    -    The value to set the control to
***                            this will most likely be a GetFormVar()
***                            value
***    Return:
************************************************************************
FUNCTION SetValue
LPARAMETERS lvValue

*** Null values simply are not set - IOW leave the field alone!
IF lvValue=WWC_NULLSTRING
   RETURN
ENDIF
   
lcType=TYPE("THIS.oReference.value")
DO CASE
   *** Convert values from text -> proper type
   CASE lcType $ "NIBFY"
         IF lvValue = "ON" or lvValue="YES"
            THIS.oReference.value = 1
         ELSE
            THIS.oReference.value = 0
         ENDIF
   CASE lcType = "L"
         IF lvValue = "ON" or lvValue="YES"
            THIS.oReference.value = .T.
         ELSE
            THIS.oReference.value = .F.
         ENDIF
ENDCASE

ENDFUNC
* SetValue

ENDDEFINE
* wwHTMLCheckBox

*************************************************************
DEFINE CLASS wwHTMLRadio AS WWC_wwHTMLControl
*************************************************************
***    Author: Rick Strahl
***            (c) West Wind Technologies, 1996
***   Contact: (541) 386-2087  / rstrahl@west-wind.com
***  Modified: 11/02/96
***
***  Function:
*************************************************************

*** Custom Properties
*** Stock Properties


************************************************************************
* wwHTMLRadio :: GetHTML
*********************************
***  Function: Retrieves the HTML text for the object based on previous
***            property settings. The name for Radios are all the same
***            with the name of the PARENT OPTIONGROUP being assigned to
***            all radios in the group. Thus the radios are treated as
***            as a group not as individual buttons.
***      Pass:
***    Return:
************************************************************************
FUNCTION GetHTML
LOCAL lcOutput, lcValue

loObject=THIS.oReference
lcFullName=SYS(1272,loObject.Parent)
lcFullName=CHRTRAN( SUBSTR(lcFullName,AT(".",lcFullName) + 1),".","_" )

lcOutput=[<INPUT TYPE="RADIO" NAME="]+lcFullName+[" ID="]+lcFullName+["] +;
         IIF(!loObject.enabled," DISABLED","") 
         
lcChecked=""
lcValue=""

IF TYPE("loObject.Value")="N"
   lcChecked=IIF(loObject.value # 0," CHECKED","")
ENDIF
IF TYPE("loObject.Value")="L"
   lcChecked=IIF(loObject.value=.T.," CHECKED ","")
ENDIF

lcOutput=lcOutput + [ VALUE="]+loObject.Caption+["]+lcChecked

lcOutput = lcOutput + [ STYLE="POSITION:ABSOLUTE; LEFT:]+LTRIM(STR(loObject.Left -3))+[; TOP:]+LTRIM(STR(loObject.Top - 3))+[;]+[;HEIGHT=]+LTRIM(STR(loObject.Height))+["]


*** Call the Label Control object to display the actual label
loLabel=CREATE("wwHTMLLabel",loObject,THIS.oHTML)
loObject.left = loOBject.left + 18
lcLabel=loLabel.GetHTML()
loObject.left = loOBject.left - 18

RETURN THIS.oHTML.FastWrite(lcOutput + ">"  + lcLabel)
* GetHTML

ENDDEFINE
* wwHTMLCheckBox


*************************************************************
DEFINE CLASS wwHTMLShape AS WWC_wwHTMLControl
*************************************************************
***    Author: Rick Strahl
***            (c) West Wind Technologies, 1996
***   Contact: (541) 386-2087  / rstrahl@west-wind.com
***  Modified: 11/02/96
***  Function:
*************************************************************

*** Custom Properties
*** Stock Properties


************************************************************************
* wwHTMLShape :: GetHTML
*********************************
***  Function: Retrieves the HTML text for the object based on previous
***            property settings
***      Pass:
***    Return:
************************************************************************
FUNCTION GetHTML
LOCAL lcOutput, lcValue

loObject=THIS.oReference
IF loObject.Height < 5 
   *** Assume a line
   RETURN THIS.oHTML.FastWrite([<hr style="POSITION:Absolute;]+THIS.PositionStyle() + ["/>])
ENDIF
*!*	IF loObject.Height < 20
*!*	   *** Can't do anything
*!*	   RETURN ""
*!*	ENDIF

lcColor=HTMLColor(loObject.BorderColor)  &&IIF(loObject.SpecialEffect=0,"#808080",HTMLColor(loObject.BorderColor))
IF loObject.SpecialEffect # 0 
lcText= [<DIV STYLE="POSITION:ABSOLUTE;]+THIS.PositionStyle()+;
                    [;BORDER:] + LTRIM(STR(loObject.BorderWidth))+[px solid ] + [;] + ;
        [Color:] + lcColor +;
        IIF(loObject.BackStyle=1,[;BACKGROUND:]+HTMLColor(loObject.BackColor),[]) +;
        IIF(!loObject.visible,";DISPLAY:none;","") + ;
        [">] + ;                    
        [</DIV>]
ELSE
lcText= [<FIELDSET STYLE="POSITION:ABSOLUTE;]+THIS.PositionStyle()+;
        IIF(loObject.BackStyle=1,[;BACKGROUND:]+HTMLColor(loObject.BackColor),[]) +;
        IIF(!loObject.visible,";DISPLAY:none;","") + ;
        [">] + ;                    
        [</FIELDSET>]
ENDIF        

*!*	IF loObject.SpecialEffect = 0
*!*		*** Save position, then move object by one click
*!*		lnLeft=loOBject.Left
*!*		lnTop=loObject.Top
*!*		loObject.Left=loObject.Left + 1
*!*		loObject.Top=loObject.Top + 1

*!*		lcText= lcText + [<DIV STYLE="POSITION:ABSOLUTE;]+THIS.PositionStyle()+;
*!*		                    [;BORDER:] + LTRIM(STR(loObject.BorderWidth))+[px solid ] + [;] + ;
*!*		        [Color:WHITE]+;
*!*		        IIF(!loObject.visible,";DISPLAY:none;","") + ;
*!*		        [">] + ;                    
*!*		        [</DIV>]

*!*		loObject.Left=lnLeft
*!*		loObject.Top=lnTop
*!*	ENDIF

RETURN THIS.oHTML.FastWrite(lcText)
* GetHTML

ENDDEFINE

*************************************************************
DEFINE CLASS wwHTMLImage AS WWC_wwHTMLControl
*************************************************************
***    Author: Rick Strahl
***            (c) West Wind Technologies, 1996
***   Contact: (541) 386-2087  / rstrahl@west-wind.com
***  Modified: 11/02/96
***  Function:
*************************************************************

*** Custom Properties
*** Stock Properties


************************************************************************
* wwHTMLImage :: GetHTML
*********************************
***  Function: Retrieves the HTML text for the object based on previous
***            property settings
***      Pass:
***    Return:
************************************************************************
FUNCTION GetHTML
LOCAL lcText, lcValue

loObject=THIS.oReference 

lcImage=STRTRAN(Justfname(lower(loObject.Picture)),".bmp",".gif")
lcImage=STRTRAN(lower(lcImage),".ico",".gif")

lcText = [<IMG SRC="]+WWFORM_IMAGEPATH + lcImage+[" ]+ ;
         [STYLE="POSITION:ABSOLUTE;]+THIS.PositionStyle()+;
         IIF(!loObject.visible,";DISPLAY:none;","") + [">]                     

RETURN THIS.oHTML.FastWrite(lcText)
* GetHTML

ENDDEFINE

*************************************************************
DEFINE CLASS wwHTMLPageFrame  AS WWC_wwHTMLControl
*************************************************************
***  Function: PageFrame - Frame drawing code only!
*************************************************************

*** Custom Properties
*** Stock Properties


************************************************************************
* wwHTMLPageFrame :: GetHTML
*********************************
***  Function: Retrieves the HTML text for the object based on previous
***            property settings
***      Pass:
***    Return:
************************************************************************
FUNCTION GetHTML
LOCAL lcOutput, lcValue,x,y, loObject, loPage, lcObjName, lnX, lcText, lcColor

loObject=THIS.oReference

lcColor=IIF(loObject.SpecialEffect=0,"#808080",HTMLColor(loObject.BorderColor))
lcPageFrameName = STRTRAN(THIS.cFullObjectName,".","_")


*** Outer Page Frame and 3D Effect
IF loObject.SpecialEffect = 0
	*** Save position, then move object by one click
	lnLeft=loObject.Left
	lnTop=loObject.Top
	loObject.Left=loObject.Left
	loObject.Top=loObject.Top

	lcText= [<DIV STYLE="POSITION:ABSOLUTE;]+;
       [LEFT:]+LTRIM(STR(loObject.Left+2))+[;TOP:]+LTRIM(STR(loObject.Top+2))+;
       [;WIDTH:]+LTRIM(STR(loObject.Width))+[;HEIGHT:]+LTRIM(STR(loObject.Height)) +;
            [;BORDER:2px solid ] + ;
	        [;Color:Black] + ;
	        IIF(!loObject.visible,";DISPLAY:none;","") + ;
	        [">] + ;                    
	        [</DIV>] + CRLF

*!*	lcText= [<FIELDSET STYLE="POSITION:ABSOLUTE;]+THIS.PositionStyle()+;
*!*	        IIF(!loObject.visible,";DISPLAY:none;","") + ;
*!*	        [;BACKGROUND:WHITE] +;
*!*	        [">] + ;                    
*!*	        [</FIELDSET>]
**        [;BACKGROUND:HTMLColor(loObject.BackColor)] +;

ENDIF
lcText= lcText + [<DIV STYLE="POSITION:ABSOLUTE;]+THIS.PositionStyle()+;
                    [;BORDER:] + [1px solid ] + [;] + ;
        [Color: WHITE] +;
        [;BACKGROUND:WHITE] +;
        IIF(!loObject.visible,";DISPLAY:none;","") + ;
        [">] + ;                    
        [</DIV>] +CRLF 

lnBtnSize =  INT((loObject.Width + 1)/ loObject.PageCount ) 

*** Must get the order of pages first so we translate
DIMENSION laOrder[loObject.PageCount]
FOR x=1 to loObject.PageCount
    FOR y=1 to loObject.PageCount
        IF loObject.Pages[y].PageOrder = x
           laOrder[x] = y
           EXIT
        ENDIF
    ENDFOR
ENDFOR

*** Add the 'buttons'
FOR x=loObject.PageCount TO 1 STEP -1
   loPage = loObject.Pages[ laOrder[x] ]
   lcObjName = lcPageFrameName + "_" + loPage.Name
*!*	   wait window lcObjName + CHR(13) + ;
*!*	              TRANS(laOrder[x]) + " " + TRANS(x)
   lnX = loObject.Left + (x-1) * lnBtnSize
   lcText = lcText + CRLF + ;
         [<DIV NAME=btn_]+lcObjName+[ ID=btn_]+lcObjName+;
         [ STYLE="POSITION:ABSOLUTE;COLOR:BLACK;]+;
         [Height:]+LTRIM(STR(24))+[;Width:]+LTRIM(STR(lnBtnSize-1))+[;]+;
         THIS.FontStyle(loPage) + [;] +;
         [Left:]+LTRIM(STR(lnX + 1))+[;Top:]+LTRIM(STR(loObject.Top + 1)) +[;BACKGROUND:]+;
         iif(x=1,"DarkGray","Silver")+[">]+;
         [<DIV STYLE="POSITION:RELATIVE;TOP:5;"><CENTER>]+;
         STRTRAN(loPage.Caption,"\<","") + ;
         [</CENTER></DIV></DIV>]

   lcText = lcText + CRLF + CRLF +;
   [<script language="VBScript"><!--]+CRLF +;
   [Sub btn_]+lcObjName+[_OnClick()]+ CRLF
   
   FOR y=1 to loObject.PageCount
      lcObjName = lcPageFrameName + "_" + loObject.Pages[ laOrder[y] ].Name
      IF x = y
        lcText = lcText + lcObjName + [.style.visibility="visible"] + CRLF +;
                          lcObjName + [.style.display=""] + CRLF + ;
                          [btn_]+lcObjName + [.style.backgroundcolor="darkgray"] + CRLF + ;
                          [btn_]+lcObjName + [.style.height="25px"] + CRLF
      ELSE
        lcText = lcText + lcObjName + [.style.visibility="hidden"] + CRLF +;
                          lcObjName + [.style.display="None"] + CRLF +;
                          [btn_] + lcObjName + [.style.backgroundcolor="silver"] + CRLF +;
                          [btn_]+ lcObjName + [.style.height="24px"] + CRLF
      ENDIF
   ENDFOR && y=1 to loObject.Pages

   lcText = lcText + ;
     [End Sub]+CRLF+;
     [--></script>]+CRLF+CRLF
ENDFOR   


RETURN THIS.oHTML.FastWrite(lcText)
* GetHTML

ENDDEFINE
* wwHTMLShape

#IF !WWLIST_USEOLDGRID

*************************************************************
DEFINE CLASS wwHTMLGrid AS WWC_wwHTMLControl
*************************************************************
***    Author: Rick Strahl
***            (c) West Wind Technologies, 1997
***   Contact: (541) 386-2087  / rstrahl@west-wind.com
***  Modified: 08/21/97
***
***  Function:
*************************************************************

*** Custom Properties

*** Stock Properties

************************************************************************
* wwHTMLGrid :: GetHTML
*********************************
***  Function: Retrieves the HTML text for the object based on previous
***            property settings
***      Pass:
***    Return:
************************************************************************
FUNCTION GetHTML
LOCAL lcOutput, lcValue, y, x

loObject=THIS.oReference
lcObjName = STRTRAN(THIS.cFullObjectName,".","_")

 
loHTML = THIS.oHTML


loHTML.Write ([<object id="]+lcObjName+[" STYLE="position:absolute;] +THIS.PositionStyle()+;
              IIF(!loObject.visible,";DISPLAY:none;","") + ["] + CRLF +;
              [    name="]+lcObjName+["] + CRLF + ;
			  [    classid="clsid:]+LISTVIEW_CLASSID + ["] + CRLF + ;
			  [    codebase="] + LISTVIEW_CODEBASE + ["] + CRLF + ;
			  [    align="baseline"] + CRLF + ;
			  [    border="0" width="]+LTRIM(STR(loObject.Width))+[" height="]+LTRIM(STR(loObject.Height))+[">] + CRLF + ;
			  [    <param name="_ExtentX" value="7064"><param name="_ExtentY" value="7064">] + CRLF + ;
			  [    <PARAM NAME="ForeColor" VALUE="0">] + CRLF + ;
              [    <PARAM NAME="BackColor" VALUE="16777215">] +CRLF+;
         	  [	<PARAM NAME="AllowReorder" VALUE="-1">] + CRLF +;
         	  [</object>])


loHTML.SendLn([<script language="VBScript">])
loHTML.SendLn([Sub ]+lcObjName+[_Load()])
loHTML.Write(;
	[set oList = Document.] + lcObjName  + CRLF +  ;
	[set oRow = nothing]  + CRLF +  ;
	[olist.font.name = "] + loObject.FontName + ["] + CRLF +  ;
	[oList.font.size = ]  + TRANSFORM(loObject.FontSize) + CRLF +  ;
	[oList.view = 3]  + CRLF +  ;
	[oList.FullRowSelect = True]  + CRLF +  ;
	[oList.GridLines = True]  + CRLF +;
	[set oItems = oList.ListItems] + CRLF)

lnColCount = loObject.ColumnCount

** Add Column Headers
FOR x=1 TO lnColCount
    FOR y=1 to loObject.Columns(x).ControlCount
     IF loObject.Columns(x).Controls(y).BaseClass = "Header"
        loHeader = loObject.Columns(x).Controls(y)
        lcheader=PROPER(STRTRAN(loHeader.Caption,"_"," "))
        lnAlign = IIF(x > 1 and VARTYPE(loObject.Columns(x).Controls(2).Value) $ "INBY",1,0)
        loHTML.SendLn([AddColumn "]+lcHeader+[",]+LTRIM(STR(loHeader.Parent.Width))+","+LTRIM(STR(lnAlign)) )     
     ENDIF
    ENDFOR
ENDFOR && x=1 TO ALEN(lvType,1)

lcOldAlias = ALIAS()
lcGridDataSource = loObject.RecordSource
  
*** Loop to form container and set to its datasession
loTObject = loObject
DO WHILE loTObject.BaseClass # "Form"
   loTObject = loTObject.Parent
ENDDO

SET DATASESSION TO loTObject.DataSessionId

SELE (lcGridDataSource)
lnRecSave = Recno()

lcFilter = ""
IF !EMPTY(loObject.LinkMaster)
   SELE (loObject.LinkMaster)
   lcKey = EVAL(loObject.LinkMaster + "." + loOBject.RelationalExpr)
   lcFilter = " FOR " + loOBject.RelationalExpr +;
                      "='" + lcKey +"'"
ENDIF
y=0

SELE (lcGridDataSource)
LOCATE 
SCAN &lcFilter
      FOR x=1 TO lnColCount
         *** ???ras - unresolved 01/17/97
         ***          Add Cell based formatting
                      
         lvValue=Eval(loObject.Columns(x).Controls(2).ControlSource)
         lcFieldType = VARTYPE(lvValue)
         lcVal=""
         DO CASE
         CASE ISNULL(lvValue)
            lcVal="n/a"
         CASE lcfieldtype="C"
            lcVal=STRTRAN(TRIM( lvValue),'"',"'")
         CASE lcFieldType="M"
            lcVal=STRTRAN(MLINE(lvValue,1),'"',"'") 
         CASE lcfieldtype $ "NFBIY"
            lcVal=TRANSFORM(lvValue)
         CASE lcfieldtype="L"
            lcVal=IIF(lvValue,"True","False")
         CASE lcfieldtype="D"
            lcVal=DTOC(lvValue)
         CASE lcfieldtype="T"
            lcVal=lower(ttoc(lvValue))
         ENDCASE

         * lcVal=CHRTRAN(lcVal,CHR(10)+CHR(13)," ")

         *** ///ras - modified 12/07/97
         ***          Use STRTRAN() here to avoid CHRTRAN GPF problems
         lcVal=STRTRAN(lcVal,CRLF," ")
         
*!*            IF x=1
*!*              loHTML.FastWrite([Document.]+lcObjName+[.AddItem "]+lcVal+[","",]+LTRIM(STR(x)) +CRLF)
*!*            ELSE           
           loHTML.FastWrite([AddItem "]+lcVal+[","",]+LTRIM(STR(x)) +CRLF)
*!*         ENDIF

      ENDFOR
ENDSCAN

IF lnRecSave # 0 AND lnRecSave <= reccount()
  GOTO lnRecSave
ENDIF

SET DATASESSION TO
IF USED(lcOldAlias)
  SELE (lcOldAlias)
ENDIF  

loHTML.SendLn([end sub])

*** Write out the column sorter
loHTML.Write(;
		[Sub ]  + lcObjName +[_ColumnClick(ColumnHeader)]  + CRLF +  ;
		[    document.] + lcObjName + [.Sorted = True]  + CRLF +  ;
		[    document.] + lcObjName + [.SortKey = ColumnHeader.Index -1]  + CRLF +  ;
		[End Sub]  + CRLF )

loHTML.SendLn([</script>])

RETURN ""
* GetHTML


ENDDEFINE
*EOC wwHTMLGrid

#ELSE
*** OLD GRID CONTROL 
*************************************************************
DEFINE CLASS wwHTMLGrid AS WWC_wwHTMLControl
*************************************************************
***    Author: Rick Strahl
***            (c) West Wind Technologies, 1997
***   Contact: (541) 386-2087  / rstrahl@west-wind.com
***  Modified: 08/21/97
***
***  Function:
*************************************************************

*** Custom Properties

*** Stock Properties

************************************************************************
* wwHTMLGrid :: GetHTML
*********************************
***  Function: Retrieves the HTML text for the object based on previous
***            property settings
***      Pass:
***    Return:
************************************************************************
FUNCTION GetHTML
LOCAL lcOutput, lcValue, y, x

loObject=THIS.oReference
lcObjName = STRTRAN(THIS.cFullObjectName,".","_")
 
loHTML = THIS.oHTML

loHTML.SendLn([<DIV STYLE="POSITION:ABSOLUTE;]+THIS.PositionStyle()+;
        IIF(!loObject.visible,";",";") + ;
        [">])                    

loHTML.Write([<object id="]+lcObjName+["] +CRLF + ;
            [    name="]+lcObjName+["] + CRLF +;
			[    classid="clsid:]+WWLIST_CLASSID + ["] + CRLF +;
			[    codebase="] + WWLIST_CODEBASE + ["] + CRLF +;
			[    align="baseline"] + CRLF +;
            [    border="0" width="]+LTRIM(STR(loObject.Width))+[" height="]+;
                LTRIM(STR(loObject.Height))+[">] + CRLF+;
			[    <param name="_ExtentX" value="7064"><param name="_ExtentY" value="7064">] +CRLF )

loHTML.SendLn([    </object>])

loHTML.SendLn([<script language="VBScript"><!--])
loHTML.SendLn([Sub ]+lcObjName+[_Load()])

loHTML.SendLn([Document.]+lcObjName+[.SetVisible False])

loHTML.SendLn([Document.]+lcObjName+[.SetFont "]+loObject.FontName+[",]+LTRIM(STR(loObject.FontSize))+;
               [,]+IIF(loObject.FontBold,"True","False")+[,]+IIF(loObject.FontItalic,"True","False") )

lnColCount = loObject.ColumnCount

** Add Column Headers
FOR x=1 TO lnColCount
    FOR y=1 to loObject.Columns(x).ControlCount
     IF loObject.Columns(x).Controls(y).BaseClass = "Header"
        loHeader = loObject.Columns(x).Controls(y)
        lcheader=PROPER(STRTRAN(loHeader.Caption,"_"," "))
        lnAlign = IIF(x > 1 and VARTYPE(loObject.Columns(x).Controls(2).Value) $ "INBY",1,0)
        loHTML.SendLn([Document.]+lcObjName+[.AddColumn "]+lcHeader+[",]+LTRIM(STR(loHeader.Parent.Width * .90))+","+LTRIM(STR(lnAlign)) )     
     ENDIF
    endfor
ENDFOR && x=1 TO ALEN(lvType,1)

lcOldAlias = ALIAS()
lcGridDataSource = loObject.RecordSource
  

*** Loop to form container and set to its datasession
loTObject = loObject
DO WHILE loTObject.BaseClass # "Form"
   loTObject = loTObject.Parent
ENDDO


SET DATASESSION TO loTObject.DataSessionId

SELE (lcGridDataSource)
lnRecSave = Recno()

lcFilter = ""

IF !EMPTY(loObject.LinkMaster)
   SELE (loObject.LinkMaster)
   lcKey = EVAL(loObject.LinkMaster + "." + loOBject.RelationalExpr)
   lcFilter = " FOR " + loOBject.RelationalExpr +;
                      "='" + lcKey +"'"
ENDIF
y=0

SELE (lcGridDataSource)
LOCATE 
SCAN &lcFilter
      FOR x=1 TO lnColCount
         *** ???ras - unresolved 01/17/97
         ***          Add Cell based formatting
         lvValue=Eval(loObject.Columns(x).Controls(2).ControlSource)
         lcFieldType = VARTYPE(lvValue)
         lcVal=""
         DO CASE
         CASE ISNULL(lvValue)
            lcVal="n/a"
         CASE lcfieldtype="C"
            lcVal=STRTRAN(TRIM(MLINE(lvValue,1)),'"',"'")
         CASE lcFieldType="M"
            lcVal=STRTRAN(MLINE(lvValue,1),'"',"'") 
         CASE lcfieldtype $ "NFBIY"
            lcVal=TRANSFORM(lvValue)
         CASE lcfieldtype="L"
            lcVal=IIF(lvValue,"True","False")
         CASE lcfieldtype="D"
            lcVal=DTOC(lvValue)
         CASE lcfieldtype="T"
            lcVal=lower(ttoc(lvValue))
         ENDCASE

         * lcVal=CHRTRAN(lcVal,CHR(10)+CHR(13)," ")

         *** ///ras - modified 12/07/97
         ***          Use STRTRAN() here to avoid CHRTRAN GPF problems
         lcVal=STRTRAN(lcVal,CRLF," ")
         
*!*            IF x=1
*!*              loHTML.FastWrite([Document.]+lcObjName+[.AddItem "]+lcVal+[","",]+LTRIM(STR(x)) +CRLF)
*!*            ELSE           
           loHTML.FastWrite([Document.]+lcObjName+[.AddItem "]+lcVal+[","",]+LTRIM(STR(x)) +CRLF)
*!*         ENDIF

      ENDFOR
ENDSCAN

IF lnRecSave # 0 AND lnRecSave <= reccount()
  GOTO lnRecSave
ENDIF

SET DATASESSION TO
IF USED(lcOldAlias)
  SELE (lcOldAlias)
ENDIF  

loHTML.SendLn([  Document.]+lcObjName+[.SetVisible True])

loHTML.SendLn([end sub])


loHTML.SendLn([--></script>])

loHTML.SendLn([</DIV>])

RETURN ""
* GetHTML


ENDDEFINE
*EOC wwHTMLGrid

**** END OLD GRID CONTROL (wwctls.ocx)
#ENDIF


*** HTML Based version - under construction

*!*	*************************************************************
*!*	DEFINE CLASS wwHTMLGrid2 AS WWC_wwHTMLControl
*!*	*************************************************************
*!*	***    Author: Rick Strahl
*!*	***            (c) West Wind Technologies, 1997
*!*	***   Contact: (541) 386-2087  / rstrahl@west-wind.com
*!*	***  Modified: 08/21/97
*!*	***
*!*	***  Function:
*!*	*************************************************************

*!*	*** Custom Properties

*!*	*** Stock Properties

*!*	************************************************************************
*!*	* wwHTMLGrid :: GetHTML
*!*	*********************************
*!*	***  Function: Retrieves the HTML text for the object based on previous
*!*	***            property settings
*!*	***      Pass:
*!*	***    Return:
*!*	************************************************************************
*!*	FUNCTION GetHTML
*!*	LOCAL lcOutput, lcValue, y, x


*!*	lcOutput = ;
*!*	[<IFRAME name="]+ THIS.cFullObjectName + [" id="] + THIS.cFullObjectName +[" style="POSITION: absolute;] + THIS.PositionStyle() + [" src="about:blank" marginwidth="0" marginheight="0">] + CRLF +;
*!*	[</IFRAME>] +CRLF +CRLF 

*!*	loObject=THIS.oReference
*!*	lcObjName = STRTRAN(THIS.cFullObjectName,".","_")
*!*	 
*!*	loHTML = THIS.oHTML



*!*	loHTML.SendLn([<DIV STYLE="POSITION:ABSOLUTE;]+THIS.PositionStyle()+;
*!*	        IIF(!loObject.visible,";DISPLAY:none;","") + ;
*!*	        [">])                    


*!*	*** Build HTML String
*!*	lcOutput = [<table width=100% border="0" cellspacing="1" style="font:normal normal 8pt Tahoma">] + CRLF +;
*!*	           [<tr style="background:LightGrey">] 
*!*	         
*!*	lnColCount = loObject.ColumnCount

*!*	** Add Column Headers
*!*	FOR x=1 TO lnColCount
*!*	    FOR y=1 to loObject.Columns(x).ControlCount
*!*	     IF loObject.Columns(x).Controls(y).BaseClass = "Header"
*!*	        loHeader = loObject.Columns(x).Controls(y)
*!*	        lcheader=PROPER(STRTRAN(loHeader.Caption,"_"," "))
*!*	        lcOutput = lcOutput + ;
*!*	           [<td align="center" style="border:2px outset">] + lcHeader  + [</td>]
*!*	     ENDIF
*!*	    ENDFOR
*!*	ENDFOR && x=1 TO ALEN(lvType,1)

*!*	lcOutput = lcOutput + [</tr>] + CRLF

*!*	lcOldAlias = ALIAS()
*!*	lcGridDataSource = loObject.RecordSource
*!*	  

*!*	*** Loop to form container and set to its datasession
*!*	loTObject = loObject
*!*	DO WHILE loTObject.BaseClass # "Form"
*!*	   loTObject = loTObject.Parent
*!*	ENDDO


*!*	SET DATASESSION TO loTObject.DataSessionId

*!*	SELE (lcGridDataSource)
*!*	lnRecSave = Recno()

*!*	lcFilter = ""
*!*	IF !EMPTY(loObject.LinkMaster)
*!*	   SELE (loObject.LinkMaster)
*!*	   lcKey = EVAL(loObject.LinkMaster + "." + loOBject.RelationalExpr)
*!*	   lcFilter = " FOR " + loOBject.RelationalExpr +;
*!*	                      "='" + lcKey +"'"
*!*	ENDIF
*!*	y=0

*!*	SELE (lcGridDataSource)
*!*	LOCATE 
*!*	SCAN &lcFilter
*!*	      lcOutput = lcOutput + [<tr>]
*!*	      FOR x=1 TO lnColCount
*!*	         *** ???ras - unresolved 01/17/97
*!*	         ***          Add Cell based formatting
*!*	                      
*!*	         lvValue=Eval(loObject.Columns(x).Controls(2).ControlSource)
*!*	         lcFieldType = VARTYPE(lvValue)
*!*	         lcVal=""
*!*	         lcAlign = ""
*!*	         DO CASE
*!*	         CASE ISNULL(lvValue)
*!*	            lcVal="n/a"
*!*	         CASE lcfieldtype="C"
*!*	            lcVal=DisplayMemo(MLINE(lvValue,1))
*!*	            *TRIM(PADR(STRTRAN(lvValue,'"',"'"),40))
*!*	            *lcVal=STRTRAN(TRIM( lvValue),'"',"'")
*!*	         CASE lcFieldType="M"
*!*	            lcVal=DisplayMemo(lvValue)
*!*	            *lcVal=TRIMPADR(STRTRAN(MLINE(lvValue,1),'"',"'"),40))
*!*	         CASE lcfieldtype $ "NFBIY"
*!*	            lcVal=TRANSFORM(lvValue)
*!*	            lcAlign = [ align="right"]
*!*	         CASE lcfieldtype="L"
*!*	            lcVal=IIF(lvValue,"True","False")
*!*	            lcAlign = [ align="center"]
*!*	         CASE lcfieldtype="D"
*!*	            lcVal=DTOC(lvValue)
*!*	            lcAlign = [ align="center"]
*!*	         CASE lcfieldtype="T"
*!*	            lcVal=lower(ttoc(lvValue))
*!*	            lcAlign = [ align="center"]
*!*	         ENDCASE

*!*	         * lcVal=CHRTRAN(lcVal,CHR(10)+CHR(13)," ")

*!*	         *** ///ras - modified 12/07/97
*!*	         ***          Use STRTRAN() here to avoid CHRTRAN GPF problems
*!*	         *lcVal=STRTRAN(lcVal,CRLF," ")  && Just like grid does
*!*	         
*!*	*!*            IF x=1
*!*	*!*              loHTML.FastWrite([Document.]+lcObjName+[.AddItem "]+lcVal+[","",]+LTRIM(STR(x)) +CRLF)
*!*	*!*            ELSE           

*!*	          lcOutput = lcOutput + [<td]+lcAlign+[ NOWRAP>] + lcVal + [</td>] 
*!*	*           loHTML.FastWrite([Document.]+lcObjName+[.AddItem "]+lcVal+[","",]+LTRIM(STR(x)) +CRLF)
*!*	*!*         ENDIF

*!*	      ENDFOR
*!*	      lcOutput = lcOutput + [</tr>] +CRLF
*!*	ENDSCAN

*!*	ShowHTML( lcOutput )
*!*	WAIT window 
*!*	IF lnRecSave # 0 AND lnRecSave <= reccount()
*!*	  GOTO lnRecSave
*!*	ENDIF

*!*	SET DATASESSION TO
*!*	IF USED(lcOldAlias)
*!*	  SELE (lcOldAlias)
*!*	ENDIF  

*!*	RETURN ""
*!*	* GetHTML


*!*	ENDDEFINE
*!*	*EOC wwHTMLGrid2


*************************************************************
DEFINE CLASS wwHTMLWebBrowser AS WWC_wwHTMLControl
*************************************************************
***    Author: Rick Strahl
***            (c) West Wind Technologies, 1996
***   Contact: (541) 386-2087  / rstrahl@west-wind.com
***  Modified: 11/02/96
***  Function: Renders a TextBox object as DHTML
*************************************************************

cHTMLDiv = ""

************************************************************************
* wwHTMLTextBox :: GetHTML
*********************************
***  Function: Retrieves the HTML text for the object based on previous
***            property settings
***      Pass:
***    Return:
************************************************************************
FUNCTION GetHTML
LOCAL lcOutput, lcValue, lcColorStyle

loObject = THIS.oReference

lcOutput = ;
[<IFRAME name="]+ THIS.cFullObjectName + [" id="] + THIS.cFullObjectName +[" style="POSITION: absolute;] + THIS.PositionStyle() + [" src="about:blank">] + CRLF +;
[</IFRAME>] +CRLF +CRLF 

Doevents
lcOutput = lcOutput + ;
[<div id="HTML_]+ THIS.cFullObjectName + [" name="HTML_]+THIS.cFullObjectName+[" style="Display:none">] + CRLF + ;
loObject.Document.Body.innerhtml + ;
[</div>]


THIS.cHTMLDiv = "HTML_" + THIS.cFullObjectname 

RETURN THIS.oHTML.FastWrite(lcOutput + CRLF)
* GetHTML

ENDDEFINE
* wwHTMLWebBrowser




*************************************************************
DEFINE CLASS wwHTMLListView AS WWC_wwHTMLControl
*************************************************************
***    Author: Rick Strahl
***            (c) West Wind Technologies, 1996
***   Contact: (541) 386-2087  / rstrahl@west-wind.com
***  Modified: 11/02/96
***  Function: Renders a TextBox object as DHTML
*************************************************************


************************************************************************
* wwHTMLListView :: GetHTML
*********************************
***  Function: Retrieves the HTML text for the object based on previous
***            property settings
***      Pass:
***    Return:
************************************************************************
FUNCTION GetHTML
LOCAL lcOutput, lcValue, lcColorStyle

loObject=THIS.oReference
lcObjName = STRTRAN(THIS.cFullObjectName,".","_")
 
loHTML = THIS.oHTML

loHTML.Write ([<object id="]+lcObjName+[" STYLE="position:absolute;] +THIS.PositionStyle()+;
              IIF(!loObject.visible,";DISPLAY:none;","") + ["] + CRLF +;
              [    name="]+lcObjName+["] + CRLF + ;
			  [    classid="clsid:]+LISTVIEW_CLASSID + ["] + CRLF + ;
			  [    codebase="] + LISTVIEW_CODEBASE + ["] + CRLF + ;
			  [    align="baseline"] + CRLF + ;
			  [    border="0" width="]+LTRIM(STR(loObject.Width))+[" height="]+LTRIM(STR(loObject.Height))+[">] + CRLF + ;
			  [    <param name="_ExtentX" value="7064"><param name="_ExtentY" value="7064">] + CRLF + ;
			  [    <PARAM NAME="ForeColor" VALUE="0">] + CRLF + ;
              [    <PARAM NAME="BackColor" VALUE="16777215">] +CRLF+;
         	  [	<PARAM NAME="AllowReorder" VALUE="-1">] + CRLF +;
         	  [</object>])


loHTML.Write([<script language="VBScript">] + CRLF + ;
             [Sub ]+lcObjName+[_Load()] + CRLF +;
			[set oxList = Document.] + lcObjName  + CRLF +  ;
			[set oRow = nothing]  + CRLF +  ;
			[oxList.font.name = "] + loObject.Font.Name + ["] + CRLF +  ;
			[oxList.font.size = ]  + STR(loObject.Font.Size) + CRLF +  ;
			[oxList.view = ] + TRANSFORM(loObject.View)  + CRLF +  ;
			[oxList.FullRowSelect = ]  + IIF(loObject.FullRowSelect,"True","False") + CRLF +  ;
			[oxList.GridLines = ] + IIF(loObject.GridLines,"True","False") + CRLF +;
			[set oItems = oxList.ListItems] + CRLF)


lnColumns = loObject.ColumnHeaders.Count

** Add Column Headers
FOR x=1 TO lnColumns
    loHeader = loObject.ColumnHeaders.item(x)
    loHTML.FastWrite([AddColumn "]+loHeader.Text+[",]+TRANS(loHeader.Width)+","+TRANSFORM(loHeader.Alignment) +CRLF)     
ENDFOR && x=1 TO ALEN(lvType,1)


FOR x = 1 to loObject.ListItems.Count
   FOR y = 1 to lnColumns
   	   loItem = loObject.ListItems.Item(x)
	   IF y = 1
	      loHTML.FastWrite([AddItem "]+loItem.Text+[","",]+LTRIM(STR(y)) +CRLF)
	   ELSE
	      loHTML.FastWrite([AddItem "]+loItem.SubItems(y-1)+[","",]+LTRIM(STR(y)) +CRLF)
	   ENDIF
   ENDFOR
ENDFOR

loHTML.SendLn([end sub])

*** Write out the column sorter
loHTML.Write(;
		[Sub ]  + lcObjName +[_ColumnClick(ColumnHeader)]  + CRLF +  ;
		[    document.] + lcObjName + [.Sorted = True]  + CRLF +  ;
		[    document.] + lcObjName + [.SortKey = ColumnHeader.Index -1]  + CRLF +  ;
		[End Sub]  + CRLF + ;
		[</script>])

RETURN
* GetHTML

ENDDEFINE
* wwHTMLListView
