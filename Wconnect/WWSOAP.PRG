#INCLUDE WCONNECT.h

SET PROCEDURE TO wwSoap ADDITIVE

*** Dependencies
SET CLASSLIB TO wwXML Additive
SET PROCEDURE TO wwUtils additive
SET PROCEDURE TO wwHTTP Additive

*************************************************************
DEFINE CLASS wwSoap AS Relation
*************************************************************
*: Author: Rick Strahl
*:         (c) West Wind Technologies, 2000
*:Contact: http://www.west-wind.com
*:Created: 08/12/2000
*************************************************************
#IF .F.
*:Help Documentation
*:Topic:
Class wwSoap

*:Description:

*:Example:

*:Remarks:

*:SeeAlso:


*:ENDHELP
#ENDIF

*** Class Definition Properties
cMethod = ""
vResult = ""
cResultName = "return"

cServerUrl = ""
cUserName = ""
cPassword = ""

*** Input parameters Name, Value, Type
DIMENSION aParameters(1,3) 
nParameterCount = 0

*** Return value plus any In/Out parameters
DIMENSION aReturnValues(1,2)
nReturnValueCount = 0

DIMENSION aHeaders(1,2)
nHeaderCount = 0

*** Input SOAP Request
cRequestXML = ""

*** Output SOAP Response 
cResponseXML = ""

*** Operational Properties
lRecurseObjects = .F.

*** Include data types for SOAP parameters and returns
lIncludeDataTypes = .T.

*** Determines how data is returned
*** 0 -  as string or type if available
*** 1 -  XML String    (return value node)
*** 2 -  XML DOM Node  (return value node)
nReturnMode = 0

*** Namespace for SOAP envelope. This can vary with SOAP implemenations (IBM: SOAP-ENV:)
cSOAPNameSpace = "SOAP-ENV"

cMethodNameSpace = ""
cMethodNameSpaceUri = ""
cMethodExtraTags = ""

*** Specific SOAPAction to generate
cSOAPAction=""

*** Use ROPE's WireTransfer object for HTTP
lUseWireTransfer = .F.
oXML = .NULL.
oDOM = .NULL.

cErrorMsg = ""
lError = .F.

*** Stored object reference of the last SDL file loaded
oSDL = .NULL.
cSDLUrl = ""

************************************************************************
* wwSOAP :: Init
****************************************
FUNCTION Init
THIS.oXML = CREATEOBJECT("wwXML")
ENDFUNC

************************************************************************
* wwSOAP :: AddParameter
****************************************
***  Function: Adds parameters to the method call.
***      Pass: lcName    -  Parameter Name
***                         RESET resets the array or you can
***                         set nParameterCount to 0
***            lvValue   -  Value of the parameter 
***            lcXMLType -  Optional - XML type of the value
***                         If not passed VFP type is converted to XML type
***    Return: Number of parameters in use
************************************************************************
FUNCTION AddParameter
LPARAMETERS lcName, lvValue,lcXMLType

IF UPPER(lcName) = "RESET" 
   THIS.nParameterCount = 0
   DIMENSION THIS.aParameters(1,2)
   RETURN 0
ENDIF
IF EMPTY(lcXMLType)
  lcXMLType = THIS.FoxTypeToXMLType(VARTYPE(lvValue))
ENDIF

THIS.nParameterCount = THIS.nParameterCount + 1
DIMENSION THIS.aParameters[THIS.nParameterCount,3]

THIS.aParameters[THIS.nParameterCount,1] = lcName
THIS.aParameters[THIS.nParameterCount,2] = lvValue
THIS.aParameters[THIS.nParameterCount,3] = lcXMLType

RETURN THIS.nParameterCount
ENDFUNC
*  wwSOAP :: AddParameter


************************************************************************
* wwSoap :: CallMethod
****************************************
***  Function: High Level SOAP Method call.
***    Assume: Call AddParameter to add parameters to the call
***      Pass: lcMethod      -   Method name to call (.cMethod)
***            lcUrl         -   Server URL to call  (.cServerUrl)
***            lvResult      -   Value that is to be set with result
***                              (optional - use for objects)
***    Return: Variant - typed result or string if no type returned 
***                      by SOAP. Failure returns .F. 
************************************************************************
FUNCTION CallMethod
LPARAMETERS lcMethod, lcUrl, lvResult

IF EMPTY(lcMethod)
   lcMethod = THIS.cMethod
ENDIF
IF EMPTY(lcUrl)
   lcUrl = THIS.cServerUrl
ELSE
   THIS.cServerUrl = lcUrl
ENDIF
   
THIS.CreateSoapRequestXML(lcMethod)
IF THIS.lError
   RETURN .F.
ENDIF

 
THIS.CallSoapServer()
IF THIS.lError
   RETURN .F.
ENDIF   

lvResult = THIS.ParseSoapResponse(THIS.cResponseXML,lvResult)
IF THIS.lError
   RETURN .F.
ENDIF   

RETURN lvResult
ENDFUNC
*  wwSoap :: CallMethod

************************************************************************
* wwSoap :: CreateSoapRequestXML
****************************************
***  Function: Creates a SOAP method request XML string
***      Pass: lcMethod    -   Method name to call
***    Return: XML string of the SOAP request
************************************************************************
FUNCTION CreateSoapRequestXML
LPARAMETERS lcMethod
LOCAL lcType, lcTypeInfo, x, lnParms

IF EMPTY(lcMethod)
  lcMethod = THIS.cMethod
ELSE
  THIS.cMethod = lcMethod  
ENDIF

lcText = ;
[<] + THIS.cSOAPNameSpace + [:Envelope xmlns:] + THIS.cSOAPNameSpace + [="http://schemas.xmlsoap.org/soap/envelope/"] + CRLF +;
[ ] +THIS.cSOAPNameSpace + [:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/" ] + ;
IIF(THIS.lIncludeDataTypes,CRLF + [ xmlns:xsi="http://www.w3.org/1999/XMLSchema-instance" xmlns:xsd="http://www.w3.org/1999/XMLSchema"],[]) + ;
[>]  + CRLF +  ;
[<] + THIS.cSOAPNameSpace + [:Body>]  + CRLF 

* IIF(THIS.lIncludeDataTypes,CRLF + [ xmlns:dt="urn:schemas-microsoft-com:datatypes"],[]) + ;

IF !EMPTY(THIS.cMethodNameSpaceUri)
  lcText = lcText + ;
  [<] + IIF(!EMPTY(THIS.cMethodNameSpace),THIS.cMethodNameSpace + ":","") + lcMethod + ;
  [ xmlns] + IIF(!EMPTY(THIS.cMethodNameSpace),":" + THIS.cMethodNameSpace,"") + [="]+ THIS.cMethodNameSpaceUri + [" ]   +;
  THIS.cMethodExtraTags + [>] +CRLF 
ELSE
  lcText = lcText + ;
  [<] + lcMethod + [>]  + CRLF 
ENDIF

loXML = THIS.oXML
loXML.lRecurseObjects = THIS.lRecurseObjects

*** Walk through each of the parameter pairs
lcTypeInfo = ""
FOR x = 1 to THIS.nParameterCount
    lcField =  THIS.aParameters[x,1]
    lvValue = THIS.aParameters[x,2]
    lcType = VARTYPE(lvValue)
    
    IF THIS.lIncludeDataTypes
	    IF lcType = "O"
          *** Special handling for objects
          lcText = lcText + ;
            	loXML.AddElement( lcField,lvValue,1,[xsi:type="record"]) 
          LOOP 
	    ELSE 
	       lcTypeInfo = [xsi:type="xsd:] + THIS.aParameters[x,3] + ["]
	    ENDIF
    ENDIF
    
    lcText = lcText + ;
        loXML.AddElement( lcField,lvValue,1,lcTypeInfo)
ENDFOR

THIS.cRequestXML = lctext + ;
	[</] + IIF(!EMPTY(THIS.cMethodNameSpace),THIS.cMethodNameSpace + ":","") + lcMethod + [>]  + CRLF +  ;
	[</] + THIS.cSOAPNameSpace + [:Body>]  + CRLF +  ;
	[</] + THIS.cSOAPNameSpace + [:Envelope>]  + CRLF 

RETURN THIS.cRequestXML
ENDFUNC
*  wwSoap :: CreateSoapRequestXML


************************************************************************
* wwSOAP :: CreateSoapResponseXML
****************************************
***  Function: Creates a SOAP XML response from an input value
***      Pass: lcMethod      -  The Method that was called
***            lvValue       -  The result value (native type)
***            lcResultName  -  Name of the result ("return")
***    Return: XML of the SOAP Response
************************************************************************
FUNCTION CreateSoapResponseXML
LPARAMETERS lcMethod, lvValue, lcResultName

IF EMPTY(lcResultName)
   lcResultName = "return"
ELSE
   THIS.cResultName = lcResultName   
ENDIF   

llUseaReturnValues = .F.
IF PCOUNT() < 2
   *** Assume that you want to use aReturnValues
   llUseaReturnValues = .T.
ENDIF

lcText = ;
	[<?xml version="1.0"?>]  + CRLF +  ;
	[<] + THIS.cSOAPNameSpace + [:Envelope xmlns:] +  THIS.cSOAPNameSpace + [="http://schemas.xmlsoap.org/soap/envelope/" ] + CRLF +;
   [ ] + THIS.cSOAPNameSpace + [:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/" ]  + ;
   IIF(THIS.lIncludeDataTypes,CRLF + [ xmlns:xsi="http://www.w3.org/1999/XMLSchema-instance" xmlns:xsd="http://www.w3.org/1999/XMLSchema"],[]) + ;
	[>]  + CRLF +  ;
	[<] + THIS.cSOAPNameSpace + [:Body>]  + CRLF +  ;
	[<] + lcMethod +[Response>]  + CRLF 

*	IIF(THIS.lIncludeDataTypes,CRLF + [ xmlns:dt="urn:schemas-microsoft-com:datatypes"],[]) +;

loXML = THIS.oXML

IF !llUseaReturnValues
   *** Single result value returned - no by ref values
   lcType = VARTYPE(lvValue)
   IF THIS.lIncludeDataTypes
      IF lcType = "O"
          *** Special handling for objects
          *lcName = IIF(VARTYPE(lvValue.name)="C",lvValue.Name,"object")
          loXML.lRecurseObjects=.T.
             *** ??? Not SOAP SPEC COMPLIANT DUE TO LACK OF NAMESPACE TYPING
             *** Special handling for objects
             *** <return xsi:type="record">
             ***   <prop1>value</prop1>
             ***   <prop2>value</prop2>
             *** </return>
             *LOCAL lcName
             *lcName = IIF(VARTYPE(lvValue.class)="C",LOWER(lvValue.CLass),THIS.cResultName)
             RETURN  lcText  + ;
                     THIS.oXML.AddElement(THIS.cResultName,lvValue,1,[xsi:type="record"]) + ;
                  	[</]+ lcMethod  + [Response>] +CRLF +;
                  	[</] + THIS.cSOAPNameSpace + [:Body>] + CRLF + ;
                  	[</] + THIS.cSOAPNameSpace + [:Envelope>] + CRLF
                     
*!*             RETURN lcText + ;
*!*                  "<" + THIS.cResultName + [ xsi:type="record">] +CRLF +;
*!*               	loXML.AddElement( lcName,lvValue,1) + ;
*!*                  "</" + THIS.cResultName + ">" +CRLF +;
*!*                  	[</]+ lcMethod  + [Response>] +CRLF +;
*!*                  	[</] + THIS.cSOAPNameSpace + [:Body>] + CRLF + ;
*!*                  	[</] + THIS.cSOAPNameSpace + [:Envelope>] + CRLF
      ELSE
          lcTypeInfo = [xsi:type="xsd:] + loXML.FoxTypeToXMLType( lcType ) + ["]
      ENDIF
   ELSE
     lcTypeInfo = ""
   ENDIF

   RETURN lcText + ;
      	CHR(9) + loXML.AddElement( lcResultName,lvValue,0,lcTypeInfo) + ;
      	[</]+ lcMethod  + [Response>] +CRLF +;
      	[</] + THIS.cSOAPNameSpace + [:Body>] + CRLF + ;
      	[</] + THIS.cSOAPNameSpace + [:Envelope>] + CRLF
ELSE
   THIS.SetError("Multiple return values are currently not supported")
   RETURN ""
 
   *** Multiple rreturn values - 
   *** return value and byref parrameters from aReturnValues
   FOR x=1 to THIS.nReturnValueCount
      lvValue = THIS.aReturnValues[x,2]
      lcType = VARTYPE(lvValue)

  
      IF THIS.lIncludeDataTypes
         IF lcType = "O"
             *** ??? Not SOAP SPEC COMPLIANT DUE TO LACK OF NAMESPACE TYPING
             *** Special handling for objects
             *** <return xsi:type="record">
             ***   <prop1>value</prop1>
             ***   <prop2>value</prop2>
             *** </return>
             RETURN  lcText  + ;
                     THIS.oXML.AddElement(THIS.cResultName,lvValue,1,[xsi:type="record"]) + ;
                  	[</]+ lcMethod  + [Response>] +CRLF +;
                  	[</] + THIS.cSOAPNameSpace + [:Body>] + CRLF + ;
                  	[</] + THIS.cSOAPNameSpace + [:Envelope>] + CRLF
*!*                lcText + ;
*!*                     "<" + THIS.cResultName + [ xsi:type="record">] +CRLF +;
*!*                  	loXML.AddElement( lvValue.name,lvValue,1) + ;
*!*                     "</" + THIS.aReturnValues[x,1] + ">" +CRLF +;
         ELSE
             lcTypeInfo = [xsi:type="] + loXML.FoxTypeToXMLType( lcType ) + ["]
         ENDIF
      ELSE
        lcTypeInfo = ""
      ENDIF

      RETURN lcText + ;
         	CHR(9) + loXML.AddElement( lcResultName,lvValue,0,lcTypeInfo) + ;
         	[</]+ lcMethod  + [Response>] +CRLF +;
         	[</] + THIS.cSOAPNameSpace + [:Body>] + CRLF + ;
         	[</] + THIS.cSOAPNameSpace + [:Envelope>] + CRLF
   ENDFOR
ENDIF

ENDFUNC
*  wwSOAP :: CreateSoapResponseXML 


************************************************************************
* wwSOAP :: AddReturnValue
****************************************
***  Function: Adds return value to a SOAP response
***            Call before CreateSoapResponseXML. Note
***            you don't need to call this method if you
***            return a single value. Only call it when 
***            returning multiple (By Ref) returns.
***      Pass: lcName    -  Parameter Name
***                         RESET resets the array or you can
***                         set nParameterCount to 0
***            lvValue   -  Value of the parameter 
***            lcXMLType -  Optional - XML type of the value
***                         If not passed VFP type is converted to XML type
***    Return: Number of parameters in use
************************************************************************
FUNCTION AddReturnValue
LPARAMETERS lcName, lvValue,lcXMLType

IF UPPER(lcName) = "RESET" 
   THIS.nReturnValueCount = 0
   DIMENSION THIS.aReturnValues(1,2)
   RETURN 0
ENDIF
IF EMPTY(lcXMLType)
  lcXMLType = THIS.FoxTypeToXMLType(VARTYPE(lvValue))
ENDIF

THIS.nReturnValueCount = THIS.nReturnValueCount + 1
DIMENSION THIS.aReturnValues[THIS.nReturnValueCount,3]

THIS.aReturnValues[THIS.nReturnValueCount,1] = lcName
THIS.aReturnValues[THIS.nReturnValueCount,2] = lvValue
THIS.aReturnValues[THIS.nReturnValueCount,3] = lcXMLType

RETURN THIS.nReturnValueCount
ENDFUNC
*  wwSOAP :: AddReturnValue


************************************************************************
* wwSOAP :: CallSoapServer
****************************************
***  Function: Calls the remote server sends request and retrieves
***            result.
***      Pass: lcSoapRequestXML   -  SOAP XML for request (optional)
***    Return: XML Soap Response
************************************************************************
FUNCTION CallSoapServer
LPARAMETERS lcSOAPRequestXML
LOCAL lnSize, lcData, lnResult, llHTTP, loURl, loHTTP

IF EMPTY(lcSoapRequestXML)
  lcSOAPRequestXML = THIS.cRequestXML
ELSE
  THIS.cRequestXML = lcSOAPRequestXML  
ENDIF

IF THIS.lUseWireTransfer
   loHTTP = CREATEOBJECT("ROPE.WireTransfer")
   loHTTP.AddStdSOAPHeaders(THIS.cServerUrl, THIS.cMethod, LEN(lcSoapRequestXML))
   THIS.cResponseXML = loHTTP.PostDataToURI(THIS.cServerUrl, lcSoapRequestXML)
ELSE
   loHTTP = CREATEOBJECT("wwHTTP")
   loHTTP.nHTTPPostMode = 4   && XML

   *** POST data is the SOAP request XML
   loHTTP.AddPostKey("",lcSOAPRequestXML)

   *** Break the URL into components
   loURl = loHTTP.InternetCrackUrl(THIS.cServerUrl)
   IF ISNULL(loURl)
      THIS.lError =  .T.
      THIS.cerrormsg = "Invalid URL passed."
      RETURN ""
   ENDIF

   llHTTPS = IIF(LOWER(loURl.cProtocol)="https",.T.,.F.)

   loHTTP.nHTTPPort = VAL(loUrl.cPort)
   
   lnResult = loHTTP.HTTPConnect(loURl.cserver,THIS.cUserName,THIS.cPassword,llHTTPS)
   IF lnResult # 0
      THIS.lError = .T.
      THIS.cerrormsg = THIS.cerrormsg
      RETURN ""
   ENDIF

   lnSize=0
   lcData=""

   IF EMPTY( THIS.cSoapAction )
      THIS.cSoapAction = "SOAPAction: " + THIS.cServerUrl + "#" + THIS.cMethod
   ELSE
      THIS.cSoapAction = "SOAPAction: " + THIS.cSoapAction
   ENDIF
   
   lnResult = loHTTP.HTTPGetEx(loURl.cPath + loURl.cQueryString,@lcData,@lnSize,;
                               THIS.cSoapAction)
   IF lnResult # 0
      THIS.cerrormsg = THIS.cerrormsg
      RETURN ""
   ENDIF

   THIS.cResponseXML = lcData
ENDIF

IF THIS.cResponseXML != "<?xml"
   THIS.lError = .T.
   THIS.cerrormsg = "Non XML result returned"
   RETURN ""
ENDIF

RETURN THIS.cResponseXML
ENDFUNC
*  wwSOAP :: CallSoapServer


************************************************************************
* wwSOAP :: SoapErrorResponse
****************************************
***  Function: Creates a SOAP XML error response 
***    Assume:
***      Pass: lcError  -   Error Message
***            lnError  -   Error number
***    Return:
************************************************************************
FUNCTION SoapErrorResponse
LPARAMETERS lcError, lnErrorCode

IF EMPTY(lnErrorCode)
   lnErrorCode = 1098
ENDIF
IF EMPTY(lcError)
  lcError = "Unkown Error"
ENDIF

THIS.cResponseXML = ;
[<?xml version="1.0"?>]  + CRLF +  ;
[<] + THIS.cSOAPNameSpace + [:Envelope xmlns:] + THIS.cSOAPNameSpace +[="http://schemas.xmlsoap.org/soap/envelope/"]  + CRLF +  ;
[ ] + THIS.cSOAPNameSpace + [:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/">]  + CRLF +  ;
[<] + THIS.cSOAPNameSpace + [:Body>]  + CRLF +  ;
[<] + THIS.cSOAPNameSpace + [:Fault>]  + CRLF +  ;
[   <faultcode>] + TRANSFORM(lnErrorCode) + [</faultcode>] +CRLF + ;
[   <faultstring>] + lcError + [</faultstring>] + CRLF + ;
[   <faultactor>] + TRANSFORM(lnErrorCode) + [</faultactor>] + CRLF +;
[   <detail>] + lcError + [</detail>]+ CRLF +;
[</] + THIS.cSOAPNameSpace + [:Fault>]  + CRLF +  ;
[</] + THIS.cSOAPNameSpace + [:Body>]  + CRLF +  ;
[</] + THIS.cSOAPNameSpace + [:Envelope>] 

RETURN THIS.cResponseXML
ENDFUNC
*  wwSOAP :: SoapErrorResponse




************************************************************************
* wwSOAP :: ParseSOAPParameters
****************************************
***  Function: Retrieves parameters from the SOAP XML
***      Pass: lcRequestXML 
***            @aParameters    1 - name 2 - value 3 - type
***            lvStore         Object reference for objects to be filled
***    Return: count of parameters  (aParameters is set)
************************************************************************
FUNCTION ParseSOAPParameters
LPARAMETERS lcRequestXML, aParameters
LOCAL lnParmCount, loT, loMethod, lcXMLType, loAttributes, loType, lcMethod, oDOM

lnParmCount = 0

IF ISNULL(THIS.oDOM)
   oDOM = CREATEOBJECT("Microsoft.XMLDOM")
   oDOM.async = .F.
   THIS.oDOM = oDOM
ELSE
   oDOM = THIS.oDOM
ENDIF

oDOM.LoadXML(lcRequestXML)
IF !EMPTY(oDom.parseError.reason)
   THIS.lError = .T.
   THIS.cErrorMsg = oDOM.parseError.Reason
   RETURN 0
ENDIF

*** Grab the namespace prefix (SOAP, SOAP-ENV, S)
THIS.cSoapNameSpace = oDOM.documentElement.prefix 

loT = oDOM.SelectSingleNode([/] + THIS.cSOAPNameSpace + [:Envelope/] + THIS.cSOAPNameSpace + ":Body")
IF ISNULL(loT)
   THIS.SetError("Invalid Soap Request for parsing parameters")  
   RETURN 0
ENDIF 

*** Find the method name dynamically  
loMethod = loT.Childnodes.item(0)
IF ISNULL(loMethod)
   THIS.SetError("Invalid Soap Request for parsing parameters")  
   RETURN 0
ENDIF   

lcMethod = loMethod.baseName
THIS.cMethod = lcMethod

loSDL = .NULL.

*** Now get each of the parameters 
FOR EACH oParm in loMethod.ChildNodes
   lnParmCount = lnParmCount + 1
   DIMENSION aParameters[lnParmCount,3]
   aParameters[lnParmCount,1] =  oParm.NodeName

   lcXMLType="string"
   loAttributes = oParm.ATTRIBUTES
   IF !ISNULL(loAttributes)
      loType = loAttributes.GetNamedItem("xsi:type")
      IF !ISNULL(loType)
         lcXMLType = loType.Text
      ELSE
         *** Pull the value from the SDL file if it exists
         IF !EMPTY(THIS.cSDLURL)
            IF ISNULL(THIS.oSDL)
               THIS.oSDL = THIS.ParseServiceWSDL(THIS.cSDLUrl)
               * THIS.oSDL = THIS.ParseServiceDescription(THIS.cSDLUrl)
               * THIS.lIncludeDataTypes=.F.
            ENDIF
            IF !ISNULL(THIS.oSDL)
               loSDLMethod = THIS.oSDL.GetMethod(lcMethod)
               IF !ISNULL(loSDLMethod)
                  lcXMLType = loSDLMethod.aParameters[lnParmCount,2]
               ENDIF
            ENDIF
         ENDIF
      ENDIF
   ENDIF

   *** Store the type
   aparameters[lnParmCount,3] =  THIS.XMLTypeToFoxType(lcXMLType)
   
   IF lcXMLType = "record"
      aParameters[lnParmCount,2] = oParm.XML
   ELSE
      aParameters[lnParmCount,2] =  THIS.oXML.XMLValueToFoxValue(oParm.Text,lcXMLType)
   ENDIF
ENDFOR                                     

RETURN lnParmCount
ENDFUNC
*  wwSOAP :: ParseSOAPParameters

************************************************************************
* wwSOAP :: ParseSoapResponse
*************************************
***  Function: Parses SOAP response into vResult property. If an error
***            occurs lError is set to .t.
***    Assume: Typically used by the client
***      Pass: lcSoapResponse
***    Return: Variant - result value
************************************************************************
FUNCTION ParseSoapResponse
LPARAMETERS lcSoapResponse, lvStore
LOCAL loResult, lvResult, lcOldDate,lnOldHours, lnOldCentury, lnOldMark, ;
      loType, loBody, loReturn, lcXMLType, lcValue, x

IF EMPTY(lcSOAPResponse)
   lcSoapResponse = THIS.cResponseXML
ENDIF   

IF ISNULL(THIS.oDOM)
   loDOM = CREATEOBJECT("Microsoft.XMLDOM")
   loDOM.async = .F.
   THIS.oDOM = loDOM
ELSE
   loDOM = THIS.oDOM
ENDIF

loDom.LoadXML(lcSoapResponse)
IF !EMPTY(loDom.parseError.Reason)
   THIS.SetError(loDOM.parseError.Reason)
   RETURN .F.
ENDIF

*** Grab the namespace prefix (SOAP, SOAP-ENV, S)
THIS.cSOAPNameSpace = loDOM.DocumentElement.Prefix 
IF !EMPTY(THIS.cSoapNameSpace)
   lcSoapNameSpace = THIS.cSoapNameSpace + ":"
ELSE
   lcSoapNameSpace = ""   
ENDIF   

loBody = loDOM.SelectSingleNode([/] + lcSOAPNameSpace + [Envelope/] + ;
         lcSOAPNameSpace + "Body")
         
IF ISNULL(loBody)
      THIS.SetError("Error: Error in XML result. SOAP envelope doesn't include SOAP response.")
      RETURN .f.
ENDIF      

loReturn = loBody.ChildNodes(0)

IF ISNULL(loReturn)
   THIS.SetError("Error: Error in XML result. SOAP envelope doesn't include SOAP response.")
   RETURN .f.
ENDIF   


*** Check for errors in body
loError = loDOM.SelectSingleNode([/] + lcSOAPNameSpace + [Envelope/] + lcSOAPNameSpace + "Body/" + ;
                     lcSOAPNameSpace + "Fault")
IF !ISNULL(loError)
   *THIS.SetError(loError.SelectSingleNode(lcSOAPNameSpace + "faultstring").text)
   THIS.SetError(loError.SelectSingleNode( "faultstring").text)
   RETURN .f.   
ENDIF

FOR x=0 to loReturn.ChildNodes.length-1
   *** Grab each Return Value Node
   loResult = loReturn.ChildNodes.item(x)

   *** Try to get the type 
   loType = loResult.Attributes.GetNamedItem("xsi:type")
   IF ISNULL(loType) or loType.text = "string"
      *** Just return the text
      DO CASE 
      CASE  THIS.nReturnMode = 0
        lvResult = loResult.Text
      CASE THIS.nReturnMode = 1
        lvResult = loResult.XML
      CASE THIS.nReturnMode = 2
        lvResult = loResult
      ENDCASE
   ELSE
      *** Do type conversion  
      lcXMLType = loType.text
      IF lcXMLType = "record" or THIS.nReturnMode = 1
        lcValue = loResult.XML
      ELSE
        lcValue = loResult.Text
      ENDIF

      *** Convert the XML string into lvResult
      lvResult = THIS.oXML.XMLValueToFoxValue(lcValue,lcXMLType,@lvStore)
   ENDIF   

   DIMENSION THIS.aReturnValues[x+1,2]
   THIS.aReturnValues[x+1,1] = loResult.nodeName
   THIS.aReturnValues[x+1,2] = lvResult
ENDFOR

THIS.nReturnValueCount = x

IF x = 0
   RETURN .F.
ENDIF

RETURN THIS.aReturnValues[1,2]
ENDFUNC
*  wwSOAP :: ParseSoapResponse


************************************************************************
* wwSOAP :: GetNamedParameter
****************************************
***  Function:
***    Assume: ParseSOAPParameters has been called
***            ParseSOAPResponse has been called (llReturn)
***      Pass: lcParm         - Parameter to retrieve
***            llReturnValues - Retrieve return value 
***    Return: value or .NULL.  Check lError and cErrorMsg
************************************************************************
FUNCTION GetNamedParameter
LPARAMETERS lcParm, llReturnValues
LOCAL x, lnCount

DIMENSION laParms[1]

lcParm = LOWER(lcParm)

IF !llReturnValues
  laParms = THIS.aParameters
  lnCount = THIS.nParameterCount
ELSE
  laParms = THIS.aReturnValues
  laParms = THIS.nReturnValueCount
ENDIF  

FOR x=1 TO lnCount
  IF LOWER(laParms[x,1]) == lcParm
     RETURN laParms[x,2]
  ENDIF
ENDFOR

THIS.SetError("Parameter not found.")

RETURN .NULL.
ENDFUNC
*  wwSOAP :: GetNamedParameter

************************************************************************
* wwSOAP :: CallDotNetMethod
****************************************
***  Function: Like CallMethod but for a VS.Net Beta 1 Web Service
***    Assume: This will change with later beta versions
***      Pass: lcMethod
***    Return: Result value or .F.  - check lError for error flags.
************************************************************************
FUNCTION CallDotNetMethod
LPARAMETERS lcMethod

THIS.cMethodNameSpace=""
THIS.cMethodExtraTags = [xmlns:xsi="http://www.w3.org/1999/XMLSchema-instance"]
THIS.cMethodNameSpaceUri = "http://tempuri.org/"
THIS.lIncludeDataTypes = .F.
THIS.cSoapAction = "http://tempuri.org/" + lcMethod

RETURN THIS.CallMethod(lcMethod)
ENDFUNC
*  wwSOAP :: CallDotNetMethod

************************************************************************
* wwSOAP :: CallWSDLMethod
****************************************
***  Function: Calls a method using a WSDL file for service information
***      Pass: lcMethod  -  The Method to call
***            lcWSDLUrl -  URL to WSDL document for this service
***                         Can also pass an loSDL object
***                         created with ParseServiceWSDL to cache
***                         the WSDL parsing
***    Return: Method return value or .F. on failure (like VFP)
***            Check .lError and cErrorMsg for errors
************************************************************************
FUNCTION CallWSDLMethod
LPARAMETERS lcMethod,lcWSDLUrl, lvResult
LOCAL loSoAP

loSoap = CREATEOBJECT("wwSOAP")

IF VARTYPE(lcWSDLUrl) # "O"
   loSD = loSOAP.ParseServiceWSDL(lcWSDLUrl)
   IF ISNULL(loSD)
      RETURN .F.
   ENDIF
   THIS.oSDL = loSD
ELSE
   loSD = lcWSDLUrl
   THIS.oSDL = loSD
ENDIF

loMethod = loSD.GetMethod(lcMethod)
IF ISNULL(loMethod)
   THIS.SetError("Invalid Method")
   RETURN .F.
ENDIF

THIS.cSOAPAction = loMethod.cSoapAction
THIS.cMethodNameSpaceUri = loMethod.cMethodNameSpaceUri
IF !EMPTY(THIS.cMethodNameSpaceUri)
   THIS.cMethodNameSpace = "m"
ENDIF
THIS.cServerUrl = loSD.cServerUrl


RETURN THIS.CallMethod(lcMethod,,@lvResult)
ENDFUNC
*  wwSOAP :: CallWSDLMethod



************************************************************************
* wwSOAP :: ParseServiceWSDL
****************************************
***  Function: Parses an MS SOAP SDL file into an easily accessed object
***    Assume: 
***      Pass:lcSDLUrl or an XMLDOM object loaded with the doc
***    Return: object
************************************************************************
FUNCTION ParseServiceWSDL
LPARAMETERS lcSDLUrl
LOCAL x, y, loSD, loDOM, lcCLass, loAddress, lcCall, lcResponse

IF ISNULL(THIS.oDOM)
   loDOM = CREATEOBJECT("Microsoft.XMLDOM")
   loDOM.async = .F.
   THIS.oDOM = loDOM
ELSE
   loDOM = THIS.oDOM
ENDIF

*** Load either from URL or XML string
IF lcSDLUrl = "<?xml"
   loDOM.LoadXML(lcSDLUrl)  
ELSE
   loDOM.Load(lcSDLUrl)
ENDIF   

IF !EMPTY(loDom.ParseError.Reason)
   THIS.SetError(loDom.ParseError.Reason)
   RETURN .NULL.
ENDIF

loWSDLMethods = loDOM.SelectNodes("/definitions/portType/operation")
lnWSDLMethodsCount = loWSDLMethods.Length
DIMENSION laWSDLMethods[lnWSDLMethodsCount,3]
FOR x=1 to lnWSDLMethodsCount
    loMethod = loWSDLMethods.item(x-1)
    laWSDLMethods[x,1]  = loMethod.Attributes.GetNamedItem("name").Text
    laWSDLMethods[x,2]  = loMethod.selectSingleNode("input").Attributes.GetNamedItem("message").Text
    laWSDLMethods[x,3]  = loMethod.SelectSingleNode("output").Attributes.GetNamedItem("message").Text
ENDFOR

*** Load the method nodes to parse
loMethods = loDOM.SelectNodes("/definitions/binding/operation")
IF ISNULL(loMethods)
   THIS.SetError("No Method list found in Service Description")    
   RETURN .NULL.
ENDIF

*** Load the class level properties
loClass = loDOM.SelectSingleNode("/definitions/service")
lcClass = loClass.Attributes.GetNamedItem("name").text

loSD = CREATEOBJECT("cSDLInterface")
loSD.cInterface=lcClass

*** Get the URL location of the Web Service 
loSD.cServerUrl=loClass.SelectSingleNode("port/soap:address").Attributes.GetNamedItem("location").text

DIMENSION loSD.aMethods[loMethods.Length]

*** Now parse through each of the methods
FOR x=0 to loMethods.length-1
   loSMethod = loMethods.item(x)
   lcMethod = loSMethod.Attributes.GetNamedItem("name").text
   
   loRequest = loDOM.selectSingleNode("/definitions/message[@name='" + ;
                SUBSTR(laWSDLMethods[x+1,2], AT(":",laWSDLMethods[x+1,2])+1) + "']")
   IF ISNULL(loRequest)
      LOOP
   ENDIF
   loResponse = loDOM.selectSingleNode("/definitions/message[@name='" + ;
                SUBSTR(laWSDLMethods[x+1,3], AT(":",laWSDLMethods[x+1,3])+1) + "']")
   IF ISNULL(loResponse)
      LOOP
   ENDIF

   *** Now loop through the parameters
   loParms = loRequest.selectNodes("part")

   *** Create new method object for each method
   loMethod = CREATEOBJECT("Relation")
   loMethod.AddProperty("cName",lcMethod)
   loMethod.AddProperty("aParameters(1)")
   loMethod.AddProperty("nCount",loParms.Length)
   loMethod.AddProperty("cResultName","")
   loMethod.AddProperty("cResultType","")
   loMethod.AddProperty("cSoapAction","")
   loMethod.AddProperty("cMethodNameSpaceUri","")
   loMethod.AddProperty("cMethodNameSpace","")
   
   IF loParms.Length > 0
      DIMENSION loMethod.aParameters[loParms.Length,2]
      FOR y=0 to loParms.Length-1
          loMethod.aParameters[y+1,1] = loParms.item(y).Attributes.GetNamedItem("name").text
          loMethod.aParameters[y+1,2] = loParms.item(y).Attributes.GetNamedItem("type").text
      ENDFOR
   ENDIF
   
   loReturn = loResponse.selectSingleNode("part")
   loMethod.cResultName = loReturn.Attributes.GetNamedItem("name").text
   loMethod.cResultType = loReturn.Attributes.GetNamedItem("type").text
   
   
   loMethod.cSoapAction = loDOM.selectSingleNode("/definitions/binding/operation[@name='" +lcMethod + "']/soap:operation").Attributes.GetNamedItem("soapAction").text
   loMethod.cMethodNameSpaceUri = loDOM.selectSingleNode("/definitions/binding/operation[@name='" +lcMethod + "']/input/soap:body").Attributes.GetNamedItem("namespace").text
   
   loSD.aMethods[x+1] = loMethod
ENDFOR   

*** Number of methods  
loSD.nCount=x

RETURN loSD
ENDFUNC
*  wwSOAP :: ParseSDL


************************************************************************
* wwSOAP :: ParseServiceDescription
****************************************
***  Function: Parses an MS SOAP SDL file into an easily accessed object
***    Assume: 
***      Pass:lcSDLUrl or an XMLDOM object loaded with the doc
***    Return: object
************************************************************************
FUNCTION ParseServiceDescription
LPARAMETERS lcSDLUrl
LOCAL x, y, loSD, loDOM, lcCLass, loAddress, lcCall, lcResponse

IF ISNULL(THIS.oDOM)
   loDOM = CREATEOBJECT("Microsoft.XMLDOM")
   loDOM.async = .F.
   THIS.oDOM = loDOM
ELSE
   loDOM = THIS.oDOM
ENDIF

*** Load either from URL or XML string
IF lcSDLUrl = "<?xml"
   loDOM.LoadXML(lcSDLUrl)  
ELSE
   loDOM.Load(lcSDLUrl)
ENDIF   

IF !EMPTY(loDom.ParseError.Reason)
   THIS.SetError(loDom.ParseError.Reason)
   RETURN .NULL.
ENDIF

*** Load the method nodes to parse
loMethods = loDOM.SelectNodes("/serviceDescription/soap/interface/requestResponse")
IF ISNULL(loMethods)
   THIS.SetError("No Method list found in Service Description")    
   RETURN .NULL.
ENDIF

*** Load the class level properties
lcClass = loDOM.SelectSingleNode("/serviceDescription/soap/interface").Attributes.GetNamedItem("name").text
loSD = CREATEOBJECT("cSDLInterface")
loSD.cInterface=lcClass

*** Get the URL location of the Web Service 
loSD.cServerUrl=loDOM.SelectSingleNode("/serviceDescription/soap/service/addresses/address").Attributes.GetNamedItem("uri").Text

*** Now parse through each of the methods
FOR x=0 to loMethods.length-1
   loSMethod = loMethods.item(x)
   lcMethod = loSMethod.Attributes.GetNamedItem("name").text
   
   lcCall = loSMethod.SelectSingleNode("request").attributes.GetNamedItem("ref").Text
   lcNameSpace = EXTRACT("~" + lcCall,"~",":")
   lcCall = Extract(lcCall+"~",":","~")
   lcResponse = loSMethod.SelectSingleNode("response").attributes.GetNamedItem("ref").Text
   lcResponse = Extract(lcResponse+"~",":","~")
    
   loCall = loDOM.SelectNodes("/serviceDescription/" + lcNamespace + [:schema/element[@name="] + lcCall + ["] +"]/type/element")
   loReturn = loDOM.SelectSingleNode("/serviceDescription/" + lcNameSpace + [:schema/element[@name="] + lcResponse + ["] +"]/type/element")
   
   *** Create new method object for each method
   DIMENSION loSD.aMethods[x+1]
   loMethod = CREATEOBJECT("Relation")
   loMethod.AddProperty("cName",lcMethod)
   loMethod.AddProperty("aParameters(1)")
   loMethod.AddProperty("cNameSpace","")
   
   *** Add each of the parameters into an array element
   FOR y = 0 to loCall.length - 1
      DIMENSION loMethod.aParameters[y+1,2]
      loMethod.aParameters[y+1,1] = loCall.item(y).Attributes.GetNamedItem("name").text
      loMethod.aParameters[y+1,2] = loCall.item(y).Attributes.GetNamedItem("type").text
   ENDFOR
   
   *** Number of parameters
   loMethod.AddProperty("nCount",y)
   
   *** Return-value information 
   loMethod.AddProperty("cResultName",loReturn.Attributes.GetNamedItem("name").text )
   loMethod.AddProperty("cResultType",loReturn.Attributes.GetNamedItem("type").text )
   
   *** And finally assign it
   loSD.aMethods[x+1] = loMethod
ENDFOR   

*** Number of methods  
loSD.nCount=x

RETURN loSD
ENDFUNC
*  wwSOAP :: ParseSDL

************************************************************************
* wwSOAP :: FoxTypeToXMLType
****************************************
***  Function:
***    Assume:
***      Pass:
***    Return:
************************************************************************
FUNCTION FoxTypeToXMLType
LPARAMETER lcFoxType

DO CASE
   CASE lcFoxType = "C" or lcFoxType = "M"
      lcType = "string"
   CASE lcFoxType $ "N" or lcFoxType $ "F" 
      lcType = "double"
   case lcFoxType $ "YB"
      lcType = "number"
   CASE lcFoxType = "L"
      lcType = "boolean"
   CASE lcFoxType = "O"
      lcType = "object"
   CASE lcFoxType = "I"
      lcType = "i4"
   CASE lcFoxType = "D"
      lcType = "date"
   CASE lcFoxType = "T"
      lcType = "dateTime"
   OTHERWISE
      lcType = lcFoxType
ENDCASE

RETURN lcType
ENDFUNC
*  wwSOAP :: FoxTypeToXMLType


************************************************************************
* wwSOAP :: XMLTypeToFoxType
****************************************
***  Function:
***    Assume:
***      Pass:
***    Return:
************************************************************************
FUNCTION XMLTypeToFoxType
LPARAMETER lcXMLtype

*** strip off type prefixes dt namespace - MS SOAP bullshit
lcXMLType = LOWER(STRTRAN(lcXMLType,"dt:",""))
lcXMLType = STRTRAN(lcXMLType,"xsd:","")

DO CASE
   CASE lcXMLType $ "string,char,uri,uuid,bin.hex"
      lcType = "C"
*!*      CASE lcXMLType = "bin.hex"
*!*         lcType = "M"
   CASE lcXMLType $ "number,single,double,r4,r8,float,fixed.14.4,float.IEEE.754.32,float.IEEE.754.64" 
      lcType = "N"
   CASE lcXMLType $ "integer,i4,i1,i2,i8,ui2,ui4,ui8"
      lcType = "I"
   CASE lcXMLType = "boolean"
      lcType = "L"
   CASE lcXMLType = "object"
      lcType = "O"
   CASE lcXMLType $ "date,date.tz"
      lcType = "D"
   CASE lcXMLType $ "datetime,datetime.tz"
      lcType = "T"
   CASE lcXMLType = "record" or lcXMLType="object"
      lcType = "O"
   OTHERWISE
      lcType = "string"
ENDCASE

RETURN lcType
ENDFUNC
*  wwSOAP :: XMLTypeToFoxType

************************************************************************
* wwSOAP :: SetError
****************************************
***  Function: Internal method used to set error information
***    Assume:
***      Pass: lcError  -  Error string to set
***    Return: nothing
************************************************************************
PROTECTED FUNCTION SetError
LPARAMETERS lcError

IF EMPTY(lcError)
  THIS.cErrormsg = ""
  THIS.lError = .F.
ENDIF

THIS.lError = .T.
THIS.cErrorMsg = lcError 

ENDFUNC
*  wwSOAP :: SetError

ENDDEFINE
*EOC wwSoap 



DEFINE CLASS cSDLInterface AS Relation

cInterface = ""
cServerUrl = ""

DIMENSION aMethods(1)
nCount = 0


************************************************************************
* cSDLInterface :: GetMethod
****************************************
***  Function: Returns an individual method from the aMethods array
***            by name.
***      Pass: lcMethod = The method to be returned
***    Return: Object or NULL if not found
************************************************************************
FUNCTION GetMethod
LPARAMETERS lcMethod
LOCAL x

lcMethod = LOWER(lcMethod)
FOR x=1 TO THIS.nCount 
   loMethod = THIS.aMethods[x]
   IF LOWER(loMethod.cName)=lcMethod
      RETURN loMethod
   ENDIF
ENDFOR

RETURN .NULL.
ENDFUNC

************************************************************************
* wwSOAP :: SDLReturnType
****************************************
***  Function: Returns the 
***    Assume: ParseServiceDescription was called previously
***      Pass:
***    Return:
************************************************************************
FUNCTION ConvertSDLReturn
LPARAMETERS lcMethod, lcValue, lvStore

loMethod = THIS.GetMethod(lcMethod)
IF ISNULL(lcMethod)
   RETURN lcValue
ENDIF

lcType = loMethod.cResultType
loSOAP = CREATEOBJECT("wwSOAP")
RETURN loSOAP.XMLValueToFoxValue(lcValue,lcType,lvStore)
ENDFUNC
*  wwSOAP :: SDLReturnType


ENDDEFINE


*************************************************************
DEFINE CLASS wwSOAPProxy AS Relation
*************************************************************
*: Author: Rick Strahl
*:         (c) West Wind Technologies, 2001
*:Contact: http://www.west-wind.com
*:Created: 04/14/2001
*************************************************************

*** Custom Properties
oSOAP = .NULL.
cErrorMsg = ""
lError = .F.

*** 0 - wwSOAP - 1 MSSOAPV2
nClientMode = 0

PROTECTED cWSDLFile, oSDL
oSDL = .NULL.
cWSDLURl = ""

************************************************************************
* wwSOAPProxy :: Init
****************************************
FUNCTION Init
LPARAMETERS lnClientMode

IF VARTYPE(lnClientMode) # "N"
   lnClientMode = THIS.nClientMode
ELSE
   THIS.nClientMode = lnClientMode   
ENDIF   

DO CASE
CASE lnClientMode = 0
   THIS.oSOAP = CREATEOBJECT("wwSOAP")
CASE lnClientMode = 1
   THIS.oSOAP = CREATEOBJECT("MSSOAP.SoapClient")
ENDCASE   

IF !EMPTY(THIS.cWSDLUrl)
   THIS.LoadWSDL(THIS.cWSDLUrl)
ENDIF

ENDFUNC
*  wwSOAPProxy :: Init

************************************************************************
* wwSOAPProxy :: LoadWSDL
****************************************
***  Function: Loads the WSDL file and holds it in a local
***            reference
***      Pass: lcUrl - URL to the WSDL file
***    Return:  nothing 
************************************************************************
FUNCTION LoadWSDL
LPARAMETERS lcUrl

IF !EMPTY(lcUrl)
   THIS.cWSDLUrl = lcUrl
ENDIF   

DO CASE
   CASE THIS.nClientMode = 0
      THIS.oSDL = THIS.oSOAP.ParseServiceWSDL(THIS.cWSDLUrl)
      IF ISNULL(THIS.oSDL)
         RETURN .F.
      ENDIF
   CASE THIS.nClientMode = 1
      THIS.oSOAP.mssoapinit(THIS.cWSDLUrl)
ENDCASE

ENDFUNC
*  wwSOAPProxy :: LoadWSDL

************************************************************************
* wwSOAP :: SetError
****************************************
***  Function: Internal method used to set error information
***    Assume:
***      Pass: lcError  -  Error string to set
***    Return: nothing
************************************************************************
PROTECTED FUNCTION SetError
LPARAMETERS lcError

IF EMPTY(lcError)
  THIS.cErrormsg = ""
  THIS.lError = .F.
ENDIF

THIS.lError = .T.
THIS.cErrorMsg = lcError 

ENDFUNC
*  wwSOAP :: SetError

ENDDEFINE
*EOC wwSOAPProxy 
