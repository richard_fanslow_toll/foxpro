This \VMP\Graphics\XP\ folder contains a full set
of the graphics files used by the various VMP source 
code files in the \VMP\XLIB folder.

Please note that while these files are in the \Graphics\XP
folder, there is NOT an XP-style equivalent for each file
in \Graphics\PreXP -- many of these files are identical
to those in \Graphics\PreXP.  As we create XP-style 
equivalents for various graphics used in VMP source code, 
we copy those XP-style equivalents into this folder, 
which is refreshed each time you install a VMP maintenance 
release.

Here are the files that have been updated to XP-style thus
far:
	INFO.BMP
	EXCLAIM.BMP
	QMARK.BMP
	STOP.BMP


