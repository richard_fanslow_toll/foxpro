This \VMP\Graphics\PreXP\ folder contains a full set
of the graphics files used by the various VMP source 
code files in the \VMP\XLIB folder.

These graphics were all created in versions of Windows 
prior to XP.  The contents of the \Graphics\XP folder
started with these files.  As we create XP-style equivalents
of these files, we distribute the XP-style graphics in 
the \Graphics\XP folder -- not all those files are
different from those contained in this folder.

