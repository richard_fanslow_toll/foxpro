*
*  X6ISCOM.PRG
*
*
*  RETURN a logical value indicating whether or not the calling code
*  is 'in' a COM server
*
*  As a general rule, I'm against creating a .PRG routine to 
*  substitute for one line of code.  In this case, however, I have
*  broken that rule so as to have a single place to go if this
*  logic ever needs modification.
*
RETURN INLIST(Application.StartMode,2,3,5)
