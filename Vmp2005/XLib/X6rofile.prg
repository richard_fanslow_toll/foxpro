*
*  X6ROFILE.PRG
*  Returns a logical value indicating whether the passed
*  Path+FileName is set to ReadOnly (i.e., the file has
*  not been checked out of source control)
*
*  Copyright (c) 2001-2005 Visionpace   All Rights Reserved
*                17501 East 40 Hwy., Suite 218
*                Independence, MO 64055
*                816-350-7900 
*                http://www.visionpace.com
*                http://vmpdiscussion.visionpace.com
*  Author:  Drew Speedie
*           Special thanks to Art Bergquist
*
*
*  USAGE
*
*  IF X6ROFILE(FULLPATH("XXFWMAIN.PRG"))
*    * take the appropriate action
*  ENDIF
*
*  IF X6ROFILE("C:\VMP\VMNT\NTMAIN.PRG")
*    * take the appropriate action
*  ENDIF
*
*
*  lParameters
*   tcFileName (R) Name of the file (including extension)
*                    to be checked to see if it is ReadOnly 
*                  INCLUDE THE FULL PATH unless you are
*                    absolutely certain tcFileName is in
*                    the VFP path at the moment, in which
*                    case you can pass just the filename
*  
LPARAMETERS tcFileName

LOCAL lcJustPath
lcJustPath = JUSTPATH(m.tcFileName)
IF NOT EMPTY(m.lcJustPath) AND NOT DIRECTORY(m.lcJustPath)
  *
  *  the indicated directory does not exist
  *
  RETURN .f.
ENDIF

 
LOCAL llReadOnly, laFiles[1]
*
*  since we know the full name of the file, we can
*  pass that as the 2nd cFileSkeleton parameter to 
*  ADIR(), so that that file will be the only one
*  found by ADIR()
*
IF ADIR(laFiles,FULLPATH(m.tcFileName)) = 0
  llReadOnly = .f.
 ELSE
  llReadOnly = "R" $ laFiles[1,5]
ENDIF

RETURN m.llReadOnly

