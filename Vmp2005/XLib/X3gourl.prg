*
*  X3GOURL.PRG
*
*  THIS ROUTINE IS INCLUDED WITH VMP FOR BACKWARD-
*  COMPATIBILITY, AND HAS BEEN REPLACED WITH X7SHELLEXECUTE.PRG/X8SHELLEXECUTE.PRG
*
*  This is the utility Rick Strahl published in the
*  March 1998 FoxPro Advisor Magazine.  It
*  executes the WinAPI ShellExecute() function to
*  fire up the passed URL/FileName in its default
*  application.  For example, if the passed tcURL is:
*   - a URL, fires up your default web browser and
*     takes you to that web site.  If you are not
*     currently connected, your dialer will kick
*     in first
*   - a Word.Doc document, fires up Word with the
*     passed document loaded (pass the full path
*     and filename)
*   - a .BMP graphic, fires up your graphics editor
*     to which .BMP files are associated, with that
*     .BMP loaded
*   - a .CHM (Compiled HTML) file, fires up the .CHM
*     interface with the passed .CHM file loaded
*   - etc.
*
*  NOTE:  works in all Visual FoxPro versions, but
*         only in Windows operating systems starting
*         with Win95
*
*  Returns:
*    2- Bad association (invalid URL, etc.)
*   29- Failure to load application
*   30- Application is busy
*   31- No application association for the passed tcURL
*  >31- Success, and the return is an instance handle
*       for the started application
*
lParameters tcURL, tcAction
tcUrl = IIF(type("tcUrl")="C",tcUrl,"http://www.visionpace.com/vmpsite")
tcAction = IIF(type("tcAction")="C",tcAction,"Open")
IF type("Application.StartMode") = "N" AND !inlist(Application.StartMode,2,3,5)
  *  UI only when not being called from a COM server
  WAIT WINDOW "Loading " + tcURL + " ..." NOWAIT
ENDIF
DECLARE INTEGER ShellExecute IN SHELL32.dll ;
     INTEGER nWinHandle, ;
     STRING cOperation, ;
     STRING cFileName, ;
     STRING cParameters, ;
     STRING cDirectory, ;
     INTEGER nShowWindow
DECLARE INTEGER FindWindow IN WIN32API ;
     STRING cNull, ;
     STRING cWinName
RETURN ShellExecute(FindWindow(0,_Screen.Caption),tcAction,tcUrl,"",SYS(2023),1)
