*
*  X3CRECUR.PRG
*  CREATE CURSOR FROM ARRAY based on the passed open ALIAS()
*  and cursor name.  This routine replaces:
*    afields(laStru,<SomeAlias>)
*    create cursor <SomeCursorName> from array laStru
*  primarily because AFIELDS() adds any table rules, 
*  field rules, and triggers for the source ALIAS() to 
*  the target CREATE CURSOR cursor.  These rules and 
*  triggers just get in the way, as VFP attempts to fire
*  them on the CREATE CURSOR fields/rows/etc.
*  The CREATE CURSOR cursor created by this routine is a
*  plain vanilla cursor based on the passed ALIAS(), much
*  like the CREATE CURSOR cursors generated in Fox2x.
*  If you actually NEED/WANT the information in columns
*  7-11 (7-16 starting in VFP 5.0, 7-18 starting in VFP 8.0), 
*  then just call AFIELDS() and CREATE CURSOR.
* 
*  OPTIONALLY preserve just the AFIELDS() columns 7-16
*  that you want.
*
*  Copyright (c) 2000-2005 Visionpace   All Rights Reserved
*                17501 East 40 Hwy., Suite 218
*                Independence, MO 64055
*                816-350-7900 
*                http://www.visionpace.com
*                http://vmpdiscussion.visionpace.com
*  Author:  Drew Speedie
*
*
*  Usage:
*
*    LOCAL lcCursor 
*    lcCursor = SYS(2015)
*    IF NOT X3CRECUR("Customers",lcCursor)
*      * the C_Customers cursor was not created
*    ENDIF
*    SELECT (lcCursor)
*
*    *  assuming you are sure C_Customers does
*    *  not already exist
*    IF NOT X3CRECUR("Customers","C_Customers")
*      * the C_Customers cursor was not created
*    ENDIF
*    SELECT C_Customers
*  
*    * or, if you are sure you don't have to trap for failure:
*    X3CRECUR("Customers","C_Customers")
*    SELECT C_Customers
*
*
*  EXAMPLES
*  ============================================
*  VMREPRTS.VCX/frmPrintInvFromMover.PrepareData()
*    XXDTES("VMREPRTS.VCX","X3CRECUR","frmPrintInvFromMover","PrepareData")
*  VMREPRTS.VCX/frmPrintInvVMDECUSM.PrepareData()
*    (passes taAddlColumns)
*    XXDTES("VMREPRTS.VCX","X3CRECUR","frmPrintInvVMDECUSM","PrepareData")
*
*
*  lParameters
*         tcAlias (R) ALIAS() of an open cursor in the data
*                       session that calls this routine,
*                       whose structure is to be copied to
*                       the new CREATE CURSOR tcCursorName
*    tcCursorName (R) ALIAS() for the CREATE CURSOR, may not
*                       be already USED() (to re-use a cursor
*                       name, close it before calling this
*                       routine)
*   tcKeepColumns (O) String of numbers indicating AFIELDS()
*                       columns > 6 that should be preserved to
*                       the CREATE CURSOR cursor.  For example,
*                       you may want to preserve any DefaultValue
*                       when using a CREATE CURSOR as a temporary
*                       data-entry cursor:
*                         x3crecur("Customers","C_Customers","9")
*                       Or DefaultValues plus FieldRules:
*                         x3crecur("Customers","C_Customers","789")
*                     Default/not passed -- columns 7+ are left
*                       empty and their values not preserved
*                       to the CREATE CURSOR cursor.
*   taAddlColumns (O) AFIELDS() array passed BY REFERENCE,
*                       containing additional fields to
*                       be added to tcCursorName
*
LPARAMETERS tcAlias, ;
            tcCursorName, ;
            tcKeepColumns, ;
            taAddlColumns

EXTERNAL ARRAY taAddlColumns

IF NOT TYPE("tcAlias") = "C" ;
     OR ISNULL(tcAlias) ;
     OR EMPTY(tcAlias) ;
     OR NOT USED(tcAlias)
  *
  *  tcAlias not passed or passed improperly or
  *  passed as an ALIAS() not currently USED()
  *     
  RETURN .f.
ENDIF

IF NOT TYPE("tcCursorName") = "C" ;
     OR ISNULL(tcCursorName) ;
     OR EMPTY(tcCursorName) ;
     OR USED(tcCursorName)
  *
  *  tcCursorName not passed or passed improperly or
  *  passed as an ALIAS() that is currently already
  *  in use
  *     
  return .f.
ENDIF     

LOCAL lcKeepColumns
IF TYPE("tcKeepColumns") = "C" ;
     AND NOT ISNULL(tcKeepColumns) ;
     AND NOT EMPTY(tcKeepColumns) 
  lcKeepColumns = UPPER(ALLTRIM(tcKeepColumns))
 ELSE
  lcKeepColumns = SPACE(0)
ENDIF

*
*  copy the structure to an array
*
LOCAL ARRAY laStru[1]
AFIELDS(laStru,tcAlias)

*
*  blank out columns 7-alen(laStru), which
*  can contain table rule, field rule, and
*  trigger expressions
*
LOCAL lnRow, lnColumn 
FOR lnRow = 1 TO ALEN(laStru,1)
  FOR lnColumn = 7 TO ALEN(laStru,2)
    IF TRANSFORM(lnColumn) $ lcKeepColumns
      *
      *  preserve this AFIELDS() column of information
      *  by doing nothing
      *
     ELSE
*!*	      laStru[lnRow,lnColumn] = SPACE(0)
      IF VARTYPE(laStru[lnRow,lnColumn]) = "N"
        laStru[lnRow,lnColumn] = 0
       ELSE
        laStru[lnRow,lnColumn] = SPACE(0)
      ENDIF
    ENDIF
  ENDFOR
ENDFOR

*!*	IF TYPE("ALEN(taAddlColumns,1)") = "N"
IF NOT VARTYPE(taAddlColumns) = "U" AND X2IsArray(@taAddlColumns)
  *
  *  add any additional columns passed in 
  *  taAddlColumns
  *
  FOR lnRow = 1 TO ALEN(taAddlColumns,1)
    DIMENSION laStru[ALEN(laStru,1)+1,ALEN(laStru,2)]
    FOR lnColumn = 1 TO ALEN(taAddlColumns,2)
      laStru[ALEN(laStru,1),lnColumn] = ;
           taAddlColumns[lnRow,lnColumn]
    ENDFOR
  ENDFOR
ENDIF

*
*  create the cursor
*
CREATE CURSOR (tcCursorName) FROM ARRAY laStru

RETURN .t.

