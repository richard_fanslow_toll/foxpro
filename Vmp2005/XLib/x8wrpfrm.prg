*
*  X8WRPFRM.PRG
*  Calls the passed .VCX-based modal form 
*  and RETURNs a value set in the form.
*
*  NOTE:  the .VCX-based modal form is required to
*         manipulate the puWrpFrm private memvar
*         created here and visible there.  It's the
*         puWrpFrm memvar that is "returned" to
*         this .PRG and to your X8WRPFRM() call.
*         XXFW.VCX/frmBase contains the code necessary
*         to copy THISFORM.iuRetVal to puWrpFrm and
*         RETURN it from this routine.
*
*  Use this routine instead of X3WRPFRM.PRG, which is
*  included for backward compatibility of existing
*  VMP application code.
*
*  Copyright (c) 2004-2005 Visionpace   All Rights Reserved
*                17501 East 40 Hwy., Suite 218
*                Independence, MO 64055
*                816-350-7900 
*                http://www.visionpace.com
*                http://vmpdiscussion.visionpace.com
*  Author:  Drew Speedie, special thanks to Reg Brehaut
*
*
*  USAGE:
*  xx = X8WRPFRM("frmReportDestination")
*  xx = X8WRPFRM("frmReportDestination,XXFW")
*  xx = X8WRPFRM("frmReportDestination,XXFW.VCX",[2])
*  xx = X8WRPFRM("frmReportDestination,XXFW.VCX",[2,"Form Title"])
*  xx = X8WRPFRM("frmGetInputItem,XXFWFRM")
*  xx = X8WRPFRM("frmGetInputItem,XXFWFRM.VCX",["What's your name?"])
*
*  xx = X8WRPFRM("MyForm,MyForms.VCX",SomeObject)
*
*  EXAMPLES:
*    XXFWCTRL.VCX/txtPicklistValid.DoPicklistForm()
*    XXFWFRM.VCX/frmFormat.cmdForeColor.Click()
*    XXFWFRM.VCX/frmFormat.cmdBackColor.Click()
*    XXFWMISC.VCX/frmReport.GetOutputDestination()
*    XXFWSEC.VCX/frmRightsAdmin.CallGroupsDialog()
*    XXFWSEC.VCX/frmRightsAdmin.CallUsersDialog()
*    XXFWSEC.VCX/frmUserProperties.pgfSecurity.Page2.grdSecurity.AddRow()
*    In the VM example application:
*      VMREPRTS.VCX/frmVMCusD.BeforePrepareData()
*      VMDECUSC.SCX/cmdFilter.Click()
*
*  NOTE:
*  You can get the same result without calling this
*  X8WRPFRM(), if you are willing to do a little 
*  extra work.  For example, instead of:
*    LOCAL lcRetVal
*    lcRetVal = X8WRPFRM("frmGetInputItem,XXFWFRM.VCX", ;
*         ["What's your name?","Enter your name","!XXXXXXXXXXXXXX","",40])
*    IF ISNULL(lcRetVal)
*      *  user closed frmGetInputItem by <Cancel>
*     ELSE
*      *  process the collected lcRetVal
*    ENDIF
*  try this equivalent:
*    PRIVATE puWrpFrm
*    puWrpFrm = SPACE(0)
*    X8SETCLS("XXFWFRM.VCX")
*    LOCAL loForm
*    loForm = CREATEOBJECT("frmGetInputItem", ;
*                          "What's your name?", ;
*                          "Enter your name", ;
*                          "!XXXXXXXXXXXXXX", ;
*                          SPACE(15), ;
*                          40)
*    loForm.Show()
*    IF ISNULL(puWrpFrm)
*      *  user closed frmGetInputItem by <Cancel>
*     ELSE
*      *  process the collected lcRetVal
*    ENDIF
*  For an example of doing it this way, see:
*    VMREPRTS.VCX/frmPrintInvFromMover_SomeSelectedPreDelete.PrepareData()
*    XXWB.VCX/ctrAppSetupWizardSubclassPageFromIntermediate.cmdShopXGetDir.Click()
*    
*
*
*  lParameters
*     tcClassName (R) Class Name of the .VCX-based form
*                       to be executed.  You may optionally
*                       (highly recommended) include a comma-
*                       separated .VCX (or .PRG ) file name to 
*                       SET CLASSLIB TO  
*    tuParameters (O) Character string containing
*                     parameters to be sent to the form
*                       OR
*                     a SINGLE object reference 
*                     (note that VMDECUSC.SCX/cmdFilter.Click()
*                     contains an example of sending tuParameters
*                     that contains BOTH an object reference
*                     and other parameters)
*                       XXDTES("VMDECUSC.SCX","X8WRPFRM","Form","cmdFilter.Click")
*                     ...same as the optional 4th parameter
*                        accepted by XXFW.VCX/cusForms.DoForm()...
*  tcInAppEXEFile (O) This parameter optionally specifies the 
*                     name of an .APP file that contains 
*                     the tcClassName class definition, maps
*                     to the IN AppFileName supported by
*                     SET CLASSLIB TO
*                     
*
LPARAMETERS tcClassName, ;
            tuParameters, ;
            tcInAppEXEFile

IF VARTYPE(m.tcClassName) = "C" AND NOT EMPTY(m.tcClassName)
  LOCAL lcClassName
  lcClassName = UPPER(ALLTRIM(m.tcClassName))
 ELSE
  ASSERT .f. message PROGRAM() + ;
       " has not received a valid tcClassName parameter"
  RETURN .NULL.
ENDIF

LOCAL lcInAppEXEFile, llOK
lcInAppEXEFile = SPACE(0)
llOK = .t.
IF VARTYPE(m.tcInAppEXEFile) = "C" AND NOT EMPTY(m.tcInAppEXEFile)
  lcInAppEXEFile = UPPER(ALLTRIM(m.tcInAppEXEFile))
  llOK = FILE(m.lcInAppEXEFile)
ENDIF
IF llOK
  lcClassName = X8SETCPR(m.lcClassName,m.lcInAppEXEFile)
ENDIF
IF ISNULL(m.lcClassName)
  ASSERT .f. message PROGRAM() + ;
       " is unable to find the passed tcClassName: " + ;
       m.lcClassName
  RETURN .NULL.
ENDIF

IF TYPE("oUser.oSecurity.BaseClass") = "C" 
  *
  *  note that tcClassName must match the target
  *  SecuredItems.SIt_ID "ClassName,VCXFileName"
  *  for this security check to be of any value
  *
  IF oUser.oSecurity.GetAccess(m.lcClassName) >= 9
    X3MSGSVC("Access denied, security")
    RETURN .NULL.
  ENDIF
ENDIF

LOCAL luParameters
DO CASE
  CASE VARTYPE(m.tuParameters) = "C" AND NOT EMPTY(m.tuParameters)
    luParameters = ALLTRIM(m.tuParameters)
  CASE VARTYPE(m.tuParameters) = "O"
    luParameters = m.tuParameters
  OTHERWISE
    luParameters = SPACE(0)
ENDCASE

PRIVATE puWrpFrm
puWrpFrm = .NULL.

LOCAL loForm, loMP, laStrings[4]
laStrings[1] = "\XXWB"
laStrings[2] = "\XXDT"
laStrings[3] = "\XXTOOLS"
laStrings[4] = "\TOOLS"
IF NOT X2PSTACK(@m.laStrings) AND X8SETCLS("XXFWUTIL.vcx") 
  loMP = CREATEOBJECT("cusPushPopMousePointers",.t.)
ENDIF

DO CASE
  CASE VARTYPE(m.luParameters) = "C" AND NOT EMPTY(m.luParameters)
    loForm = CREATEOBJECT(m.lcClassName,&luParameters)
  CASE VARTYPE(m.luParameters) = "O"
    loForm = CREATEOBJECT(m.lcClassName,luParameters)
  OTHERWISE 
    loForm = CREATEOBJECT(m.lcClassName)
ENDCASE

IF VARTYPE(m.loForm) = "O"
  loForm.Show(1)   &&& modal no matter what
ENDIF

RETURN m.puWrpFrm
