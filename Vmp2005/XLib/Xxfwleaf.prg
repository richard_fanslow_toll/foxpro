*
*  XXFWLEAF.PRG
*  This .PRG contains VMP base class definitions
*  for the "leaf" classes that can be specified
*  in the MemberClass/HeaderClass properties of
*  parent containers:
*    Column
*    Header
*    OptionButton  (not available yet)
*    Page           
*    CommandButton (will not be available, because
*                   VMP doesn't support CommandGroups)
*
*  Copyright 2003-2005 Visionpace 
*  All Rights Reserved
*                http://www.visionpace.com
*                http://vmpdiscussion.visionpace.com
*  Author:  Drew Speedie
*

**********************************************************
**********************************************************
DEFINE CLASS grcBase AS grcVMPSuperClass OF XXXXgrc.PRG
**********************************************************
**********************************************************
*
*  specify the base VMP header class in this .PRG
*
HeaderClassLibrary = "XXFWLEAF.PRG"
HeaderClass = "grhBase"
***********Name = "Column"
*
*  We'd LIKE to set the Name to "Column" so that when you 
*  look at columns in the Properties Sheet, you see
*    Column1
*    Column2
*    Column3
*    Column4
*    ...etc.
*  just the same as when the Grid.MemberClass properties
*  are not set and you set the ColumnCount property
*
*  HOWEVER!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
*  This technique works for columns in .SCX-based form 
*  instances, but DOES NOT work for .VCX-based form 
*  instances, which generate this error at runtime:
*    "Class definition <ClassName> is not found"
*  This lousy behavior is documented in the Member Classes
*  topic in the VFP 8 help file.  This lousy behavior
*  makes it impossible to have your own column classes
*  that yield names of
*    Column1
*    Column2
*    Column3
*    Column4
*    ...etc.
*  in ALL scenarios, so if you want your columns named
*  <ClassName>+<SequentialInteger> rather than Column1,
*  Column2, etc, then this technique works fine -- just
*  don't specify the Name property. 
*  However, the column and header classes you see here
*  aren't used in VMP -- XXFW.VCX/grdBase does not
*  set its MemberClass and MemberClassLibrary properties,
*  for backward compatibility -- all references in your
*  code to "ColumnX" would have to be updated to "grcBaseX".
*
*  If you do create your own column class(es), we 
*  recommend you subclass them from here -- just remember
*  NOT to set the Name property, and that VFP will
*  name the columns using the class name plus the 
*  sequential integer.
*  

icSubclassingPotential = "G"

*
*  The following properties correspond to settings 
*  in the THIS.PARENT.iaColumnInfo[] array.  Any settings
*  made in your subclasses of this class take precedence
*  over any THIS.PARENT.iaColumnInfo[] settings you 
*  make in grid classes/instances, typically in the
*  SetIndexTagInfo() method.
* 
*  You can completely ignore this class at the intermediate
*  and app-specific levels, and simply specify these
*  settings in the corresponding THIS.PARENT.iaColumnInfo[]
*  array -- this class is included in VMP only if you 
*  like the idea of creating your own column class(es)
*  subclassed from here.  If you want to use your own
*  column class(es) consistently throughout your app,
*  you will also have to set the MemberClass/MemberClassLibrary 
*  properties accordingly in your intermediate/app-specific 
*  grids subclassed from the VMP framework:
*    grd??Base
*    grd??DataEntry
*    grd??Picklist
*    frm??DEGridNavNoPages.pgfPageRefresh1.Page1.grdNav
*    frm??DEGridNav2Pages.grdNav
*    frm??Picklist.grdPicklist1
*    frm??GroupProperties.grdSecurity
*    frm??GroupPermissions.grdSecurity
*    frm??GroupPermissions.grdSecuredItems
*    ctr??UserAccExceptions.grdSecurity
*    frm??PKUserAccExceptions.grdPicklist1
*    frm??DEUsers.grdNav
*    frm??EventLog.grdNav
*    frm??LoginHistory.grdHistory
*
*
*  icReSortDisplayTag
*  Name of the index tag in THIS.PARENT.RecordSource
*  used for the display of THIS.PARENT.RecordSource
*  when this column is the "controlling" column.
*  Corresponds to and takes precedence over any Column3
*  setting in THIS.PARENT.iaColumnInfo[] (typically
*  done in THIS.PARENT.SetIndexTagInfo() method).
*    MODIFY CLASS grdBase OF XXFW METHOD SetIndexTagInfo
*  When this property is set, this column is "ReSortable"
*  and THIS.PARENT.SetReSortBehaviors() sets up the
*  iaColumnInfo array accordingly.
*    MODIFY CLASS grdBase OF XXFW METHOD SetReSortBehaviors()
*
icReSortDisplayTag = ""
*
*  icReSortDisplayTagAscDesc
*  When icReSortDisplayTag is set, this property
*  indicates the ASCENDING/DESCENDING behavior of
*  this column when it is made the controlling column,
*  as described in grdBase::SetColumnInfo().
*    XXDTES("XXFW.VCX","When THIS.icToggleADOnColumnReSort","grdBase","SetColumnInfo")
*  Corresponds to and takes precedence over any Column4
*  setting in THIS.PARENT.iaColumnInfo[] (typically
*  done in THIS.PARENT.SetIndexTagInfo() method).
*    MODIFY CLASS grdBase OF XXFW METHOD SetIndexTagInfo
*
icReSortDisplayTagAscDesc = "ASCENDING"
*
*  icReSortDisplayTagExpression
*  When icReSortDisplayTag is set, this property
*  indicates the expression for that index tag, for
*  those scenarios where THIS.icReSortDisplayTagExpression
*  is created "on the fly" when necessary.
*  Corresponds to and takes precedence over any Column6
*  setting in THIS.PARENT.iaColumnInfo[] (typically
*  done in THIS.PARENT.SetIndexTagInfo() method).
*    MODIFY CLASS grdBase OF XXFW METHOD SetIndexTagInfo
*
icReSortDisplayTagExpression = ""
*
*  icSeekTag
*  When icReSortDisplayTag is set, this property
*  indicates the name of the index tag used to do a
*  SEEK in THIS.PARENT.RecordSource (i.e. from the
*  SeekInGrid() method of a txtPicklistFind textbox
*  in an frmDEGridNav/frmPicklist form
*    MODIFY CLASS txtPicklistFind OF XXFWPICK METHOD SeekInGrid
*  When this property is not set, THIS.icReSortDisplayTag
*  is used both for the display of data and any SEEKs
*  abstracted into the framework.
*  Corresponds to and takes precedence over any Column7
*  setting in THIS.PARENT.iaColumnInfo[] (typically
*  done in THIS.PARENT.SetIndexTagInfo() method).
*    MODIFY CLASS grdBase OF XXFW METHOD SetIndexTagInfo
*
icSeekTag = ""
*
*  icSeekTagExpression
*  When icReSortDisplayTag and icSeekTag are both set, 
*  this property indicates the expression for the icSeekTag
*  index tag, for those scenarios where THIS.icSeekTag
*  needs to be created "on the fly".
*  Corresponds to and takes precedence over any Column8
*  setting in THIS.PARENT.iaColumnInfo[] (typically
*  done in THIS.PARENT.SetIndexTagInfo() method).
*    MODIFY CLASS grdBase OF XXFW METHOD SetIndexTagInfo
*
icSeekTagExpression = ""
*
*  icLookForInputMask
*  When THIS.PARENT.ReSort = .t., this property indicates
*  the InputMask that should be applied to any Look For
*  textbox (inheriting from XXFWPICK.VCX/txtPicklistFind)
*  so that the textbox.InputMask can be matched to 
*  THIS.icReSortDisplayTag.
*  Corresponds to and takes precedence over any Column9
*  setting in THIS.PARENT.iaColumnInfo[] (typically
*  done in THIS.PARENT.SetIndexTagInfo() method).
*    MODIFY CLASS grdBase OF XXFW METHOD SetIndexTagInfo
*
icLookForInputMask = ""

*////////////////////////////////////////////////////////
PROCEDURE Moved
THIS.Parent.CommonColumnMoved(THIS)
ENDPROC
*\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\


*////////////////////////////////////////////////////////
PROCEDURE Resize
THIS.Parent.CommonColumnResize(THIS)
ENDPROC
*\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

**********************************************************
**********************************************************
ENDDEFINE
**********************************************************
**********************************************************




**********************************************************
**********************************************************
DEFINE CLASS grhBase AS grhVMPSuperClass OF XXXXgrh.PRG
**********************************************************
**********************************************************
Caption = "Header1"
*******Name = "Header"    See the comments in grcBase above
icSubclassingPotential = "G"

*////////////////////////////////////////////////////////
PROCEDURE Click
*  
*  THIS.MouseUp() calls THIS.PARENT.PARENT.CommonHeaderMouseUp()
*  where, if a MouseDown()+MouseUp() has been made within 
*  THIS.PARENT.PARENT.inMouseHeaderRadius, then 
*  THIS.PARENT.PARENT.CommonHeaderClick() is executed
*
ENDPROC
*\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\


*////////////////////////////////////////////////////////
PROCEDURE DblClick
THIS.PARENT.PARENT.CommonHeaderDblClick(THIS)
ENDPROC
*\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\


*////////////////////////////////////////////////////////
PROCEDURE RightClick
THIS.PARENT.PARENT.CommonHeaderRightClick(THIS)
ENDPROC
*\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\


*////////////////////////////////////////////////////////
PROCEDURE MouseDown(nButton as Integer, ;
                    nShift as Integer, ;
                    nXCoord as Integer, ;
                    nYCoord as Integer)
THIS.PARENT.PARENT.CommonHeaderMouseDown(nButton, ;
                                         nShift, ;
                                         nXCoord, ;
                                         nYCoord, ;
                                         THIS)
ENDPROC
*\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\


*////////////////////////////////////////////////////////
PROCEDURE MouseUp(nButton as Integer, ;
                  nShift as Integer, ;
                  nXCoord as Integer, ;
                  nYCoord as Integer)
THIS.PARENT.PARENT.CommonHeaderMouseUp(nButton, ;
                                       nShift, ;
                                       nXCoord, ;
                                       nYCoord, ;
                                       THIS)
ENDPROC
*\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

**********************************************************
**********************************************************
ENDDEFINE
**********************************************************
**********************************************************




*!*	**********************************************************
*!*	**********************************************************
*!*	DEFINE CLASS pagBase AS pagVMPSuperClass OF XXXXpag.PRG
*!*	**********************************************************
*!*	**********************************************************

*!*	*
*!*	*  this class is specified in the MemberClass and
*!*	*  MemberClassLibrary properties of XXFW.VCX/pgfBase
*!*	*    MODIFY CLASS pgfBase OF XXFW
*!*	*

*!*	*
*!*	*  specify the Caption to make it obvious you aren't
*!*	*  seeing the base VFP pages in your pageframes
*!*	*
*!*	Caption = "pagBase"

*!*	*
*!*	*  nothing much else to do here at this level, but this
*!*	*  design gives you the option to specify stuff like
*!*	*  font properties in your pagVMPSuperClass, and have
*!*	*  them inherited by all pages in your app 
*!*	*
*!*	*  remember that in order to globally specify your 
*!*	*  intermediate or app-specific subclass of this class, 
*!*	*  you will not only have to update the 
*!*	*  MemberClass/MemberClassLibrary of your intermediate/
*!*	*  app-specific pgf??Base class, but also the 
*!*	*  MemberClass/MemberClassLibrary of the other intermediate/
*!*	*  app-specific pageframe classes:
*!*	*    ??FW.VCX/pgf??PageRefresh          MODIFY CLASS pgfPageRefresh OF XXFW
*!*	*    ??FWFRM.VCX/pgf??DEForms           MODIFY CLASS pgfDEForms OF XXFWFRM
*!*	*  and the MemberClass/MemberClassLibrary of all the
*!*	*  intermediate/app-specific pageframes that are members 
*!*	*  of composite classes, like
*!*	*    ??FWFRM.VCX/frm??DEGridNav2Pages   MODIFY CLASS frmDEGridNav2Pages OF XXFWFRM
*!*	*    ??FWFRM.VCX/frm??ReportCatalog     MODIFY CLASS frmReportCatalog OF XXFWFRM
*!*	*    ??FWSEC.VCX/frm??UserProperties    MODIFY CLASS frmUserProperties OF XXFWSEC
*!*	*  

*!*	**********************************************************
*!*	**********************************************************
*!*	ENDDEFINE
*!*	**********************************************************
*!*	**********************************************************




*!*	**********************************************************
*!*	**********************************************************
*!*	DEFINE CLASS optBase AS optVMPSuperClass OF XXXXopt.PRG
*!*	**********************************************************
*!*	**********************************************************

*!*	*
*!*	*  specify the Caption to make it obvious you aren't
*!*	*  seeing the base VFP optionbuttons in your optiongroups
*!*	*
*!*	Caption = "optBase"

*!*	*
*!*	*  nothing much else to do here at this level, but this
*!*	*  design gives you the option to specify stuff like
*!*	*  font properties in your optVMPSuperClass, and have
*!*	*  them inherited by all optionbuttons in your app
*!*	*  

*!*	**********************************************************
*!*	**********************************************************
*!*	ENDDEFINE
*!*	**********************************************************
*!*	**********************************************************
