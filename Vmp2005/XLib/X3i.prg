*
*  X3I.PRG
*  This is a wrapper program to allow a single call in the 
*  Visual MaxFrame Professional framework for string 
*  translation whether the current app is Steven Black's 
*  INTL Toolkit-enabled or not
*
*  Copyright (c) 1997-2005 Visionpace   All Rights Reserved
*                17501 East 40 Hwy., Suite 218
*                Independence, MO 64055
*                816-350-7900 
*                http://www.visionpace.com
*                http://vmpdiscussion.visionpace.com
*  Author:  Drew Speedie
*
*  lParameters
*      tcString (R) The string to be run through INTL (or not)
*  tuINTLSecond (O) The second parameter accepted by INTL
*   
LPARAMETERS tcString, tuINTLSecond
IF TYPE("_Screen.oINTL.BaseClass") = "C" 
  *
  *  Steven Black's INTL Toolkit is installed -- we refer to the 
  *  I.PRG here by indirection to prevent the Project Manager 
  *  complaining about the absence of I.PRG if you don't have 
  *  INTL or aren't using it in the current project.  Note that
  *  you probably don't have to remember to manually add I.PRG 
  *  to your project because when you process your menu(s) through 
  *  GENMENUX.PRG, it places explicit references to I.PRG in your
  *  .MPR, and the Project Manager then pulls in I.PRG
  *
  LOCAL lcJunk
  lcJunk = "i(tcString,tuINTLSecond)"
  RETURN EVALUATE(m.lcJunk)
 ELSE
  *
  *  Steven Black's INTL Toolkit is not installed
  *
  RETURN m.tcString
ENDIF
