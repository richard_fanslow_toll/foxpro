*
*  X3CKCHEK.PRG
*  Check a value to see if it violates Candidate Key enforcement, 
*  useful for checking that condition prior to a TABLEUPDATE() 
*  which will fail.
*  Returns a logical value indicating whether the tuCKValueToCheck 
*  value currently violates candidate key enforcement.
*
*
*  Copyright (c) 1997-2005 Visionpace   All Rights Reserved
*                17501 East 40 Hwy., Suite 218
*                Independence, MO 64055
*                816-350-7900 
*                http://www.visionpace.com
*                http://vmpdiscussion.visionpace.com
*  Author:  Drew Speedie
*
*
*  This check is based on a SQL-SELECT, making it a better choice 
*  than X3MANUCK.PRG for remote data/client-server applications
*
*  NOTE:
*  If the target table is remote, this program returns .NULL.
*  if a connection error is generated -- when using it with
*  remote data, you will need to check for the possibility of
*  a .NULL. return value.
*
*
*  Usage:
*  Local data:  DO DEMOLOCAL IN X3CKCHEK
*  Remote data:  DO DEMOREMOTE IN X3CKCHEK
*  
*  VM example application examples:
*    too numerous to list here -- 
*    DO XXDTSearch WITH "x3ckchek(" on the VM example app
*
*
*
*  This routine is useful in 1- and 2-tier scenarios -- for
*  n-Tier scenarios, you should be able to use the
*  cusICEnforceCandidateKey class in the ICRules.VCX that
*  ships with VMP and is installed to the \VMP_Int folder.
*  You can most likly just copy the ICRules.VCX and .VCT
*  class library files to your intermediate level, and use
*  cusICEnforceCandidateKey as is.
*    MODIFY CLASS cusICEnforceCandidateKey OF ICRules
*
*
*  lParameters
*           tcDBTable (R) Database!TableName in which
*                         to do the CK check.
*                         If the target table is remote,
*                         do not include the Database!
*                         qualifier, and include the 7th
*                         tnConnectHandle parameter.
*    tuCKValueToCheck (R) Value to check against a CK expression
*       tcCKFieldExpr (R) CK field expression
*                         For local VFP tables (free,
*                         Fox2x, or contained), you can
*                         pass this as X3TAGXPR(CKTagName,<Table>) 
*  tuPKValueToExclude (R) Primary Key value to exclude 
*                         from the CK check.  This allows
*                         you to exclude the current
*                         record from the check (as when
*                         editing), or checking against
*                         all records (as when adding).
*                         If you want the check against
*                         all records, pass this parameter
*                         as a .NULL. value.  If you want
*                         the check against all records
*                         except the current record, pass
*                         this parameter as the PK for
*                         that record.
*       tcPKFieldExpr (R) PK field expression.  If
*                         tuPKValueToExclude has been passed
*                         as a .NULL., this parameter is
*                         ignored; if tuPKValueToExclude has
*                         been passed as a valid value, this
*                         parameter must indicate the PK
*                         expression for tcDBTable.
*                         For local VFP contained tables
*                         you can pass this as X3PKEXPR(<Table>)
*                         For free/Fox2x tables, you can
*                         pass this as X3TAGXPR(UniqueTagName,<Table>)
*   tcFieldToRetrieve (O) Name of one field to retrieve into
*                         the SQL cursor.  Pass this
*                         parameter to limit the retrieval
*                         to a single field to limit the
*                         amount of data retrieved
*     tnConnectHandle (O) Connection handle to a remote data source.
*                         If passed as a non-null integer, indicates
*                         that tcDBTable is a remote table, and SQL
*                         Pass-Thru is used instead of a local VFP
*                         SQL-SELECT
*
LPARAMETERS tcDBTable, ;
            tuCKValueToCheck, ;
            tcCKFieldExpr, ;
            tuPKValueToExclude , ;
            tcPKFieldExpr, ;
            tcFieldToRetrieve, ;
            tnConnectHandle

LOCAL lnSelect, llWasOpen, llRetVal, lcFieldToRetrieve
lnSelect = select(0)
IF TYPE("tcFieldToRetrieve") = "C" ;
     AND !ISNULL(tcFieldToRetrieve) ;
     AND !EMPTY(tcFieldToRetrieve)
  lcFieldToRetrieve = UPPER(ALLTRIM(tcFieldToRetrieve))
 ELSE
  lcFieldToRetrieve = "*"
ENDIF
  
IF type("tnConnectHandle") # "N" OR isnull(tnConnectHandle) 
  *
  *  local SQL-Select
  *    
  *  if tcCKFieldExpr is a VFP contained table with
  *  a Candidate Key tag expression that doesn't specify a 
  *  FOR !DELETED() clause, it is necessary to SET DELETED
  *  OFF before doing the following SQL-Select
  *
  *  note that if tcCKFieldExpr is a VFP contained table
  *  with a Candidate Key tag expression that does specify a
  *  FOR !DELETED() clause, and for some reason you are
  *  running with SET DELETED OFF (I can't imagine why),
  *  this routine will RETURN .T., indicating the passed 
  *  parameters will violate CK uniqueness 
  *
  local lcSetDeleted
  lcSetDeleted = set("DELETED")
  IF lcSetDeleted = "ON"   &&& should be!
    *
    *  see if there's an index tag whose expression
    *  matches tcCKFieldExpr, which tag also has a
    *  filter condition including ".NOT. DELETED()"
    *
    select (substrc(tcDBTable,at_c("!",tcDBTable)+1))
    local xx, llForNotDeleted, llCandidate
    FOR xx = 1 to tagcount("",alias()) 
      IF upper(alltrim(key(xx))) == upper(alltrim(tcCKFieldExpr)) ;
            AND (".NOT. DELETED()" $ upper(sys(2021,xx)) OR ".NOT.DELETED()" $ upper(sys(2021,xx)))
        llForNotDeleted = .t.
        IF candidate(xx)
          llCandidate = .t.
        ENDIF
        exit
      ENDIF
    ENDFOR
    IF !llForNotDeleted AND llCandidate
      set deleted off
    ENDIF
  ENDIF
  local lcTable
  IF occurs("!",tcDBTable) > 0
    lcTable = substrc(tcDBTable,at_c("!",tcDBTable)+1)
   ELSE 
    lcTable = tcDBTable
  ENDIF
  lcTable = upper(alltrim(lcTable))
  IF used(lcTable)
    IF !lcTable == cursorgetprop("SourceName",lcTable)
      *
      *  lcTable is open with an alias
      *
      lcTable = cursorgetprop("SourceName",lcTable)
    ENDIF
  ENDIF  
  IF occurs("!",tcDBTable) > 0
    lcTable = leftc(tcDBTable,at_c("!",tcDBTable)) + lcTable
  ENDIF
  *
  *  now do the SQL-Select
  *
  IF isnull(tcPKFieldExpr) or isnull(tuPKValueToExclude)
    select &lcFieldToRetrieve ; 
      FROM (lcTable) ;
        WHERE &tcCKFieldExpr == tuCKValueToCheck ;
      INTO CURSOR X3CKCHEK
   ELSE
    select &lcFieldToRetrieve ; 
      FROM (lcTable) ;
        WHERE &tcCKFieldExpr == tuCKValueToCheck ;
          AND &tcPKFieldExpr # tuPKValueToExclude ;
      INTO CURSOR X3CKCHEK
  ENDIF
  select X3CKCHEK
  locate
  llRetVal = !eof()
  use in X3CKCHEK
  IF lcSetDeleted = "ON" AND set("DELETED") = "OFF"
    set deleted on
  ENDIF
 ELSE
  *
  *  SQL Pass-Thru to remote table
  *
  local lcString, lcCKValueToCheck, lcPKValueToExclude
  IF type("tuCKValueToCheck") = "C"
    lcCKValueToCheck = ['] + tuCKValueToCheck + [']
   ELSE
    lcCKValueToCheck = x2cnvchr(tuCKValueToCheck)
  ENDIF
  IF isnull(tcPKFieldExpr) or isnull(tuPKValueToExclude)
    lcString = "SELECT " + lcFieldToRetrieve + space(1) + ;
               "FROM " + tcDBTable + space(1) + ;
               "WHERE " + tcCKFieldExpr + " = " + lcCKValueToCheck
   ELSE
    IF type("tuPKValueToExclude") = "C"
      lcPKValueToExclude = ['] + tuPKValueToExclude + [']
     ELSE
      lcPKValueToExclude = x2cnvchr(tuPKValueToExclude)
    ENDIF
    lcString = "SELECT " + lcFieldToRetrieve + space(1) + ;
               "FROM " + tcDBTable + space(1) + ;
               "WHERE " + tcCKFieldExpr + " = " + lcCKValueToCheck + space(1) + ;
               "AND NOT " + tcPKFieldExpr + " = " + lcPKValueToExclude
  ENDIF
  local lcOnError, llError, lnSQLExec
  IF type("oLib.oConnectionSvc.BaseClass") = "C"
    *
    *  see if tnConnectHandle is a valid connection 
    *
    llError = !oLib.oConnectionSvc.ValidConnectHandle(tnConnectHandle)
    llRetVal = .NULL.
  ENDIF

  IF !llError
    lcOnError = on("ERROR")
    on error llError = .t.
    lnSQLExec = sqlexec(tnConnectHandle,lcString)
    on error &lcOnError
    IF llError OR lnSQLExec <= 0
      *
      *  connection error
      *
      llRetVal = .NULL.
     ELSE
      *
      *  SQLExec() was successful, check the results
      *
      select SQLResult
      locate
      llRetVal = !eof()
      use in SQLResult
    ENDIF             
  ENDIF
ENDIF

select (lnSelect)

return llRetVal




PROCEDURE demolocal
*
*  demo using the local VM!Cities table from the
*  data in the VM example application
*
local lcOnError, llError
lcOnError = on("ERROR")
on error llError = .t.
open data VM
on error &lcOnError
IF llError
  wait window "Unable to open VM database"
  return
ENDIF
IF !used("CITIES")
  use CITIES in 0
ENDIF
select CITIES
locate
lcString = upper(Cty_Name)+Cty_SPrFK+Cty_CryFK   &&& tag Cty_NmSCCK
IF x3ckchek("VM!CITIES", ;
            lcString, ;
            x3tagxpr("CTY_NMSCCK","CITIES"), ;
            replicate("!",lenc(CITIES.Cty_PK)), ;
            x3pkexpr("CITIES"), ;
            "Cty_PK")
  wait window lcString + " is already on file"
 ELSE
  wait window lcString + " is not already on file"
ENDIF  
IF x3ckchek("VM!CITIES", ;
            stuffc(lcString,1,2,"XX"), ;
            x3tagxpr("CTY_NMSCCK","CITIES"), ;
            replicate("!",lenc(CITIES.Cty_PK)), ;
            x3pkexpr("CITIES"), ;
            "Cty_PK")
  wait window stuffc(lcString,1,2,"XX") + " is already on file"
 ELSE
  wait window stuffc(lcString,1,2,"XX") + " is not already on file"
ENDIF  
return           



PROCEDURE DemoRemote
*
*  demo using the remote Authors table from the
*  SQL-Server sample PUBS database, also requires the
*  VMPUBS.DBC database in the VM example application --
*  to create it, DO VMMAIN, select Create Remote Data...
*  from the Remote Data menu (assuming you have a
*  data source set up to the PUBS database that ships
*  with SQL-Server)
*
IF !file("VMPUBS.DBC")
  wait window "VMPUBS.DBC does not exist"
  return
ENDIF

local lcOnError, llError, llVMPUBSWasOpen, lcSetDatabase, lnConnectHandle, lcString
lcSetDatabase = set("DATABASE")
lcOnError = on("ERROR")
on error llError = .t.
IF !dbused("VMPUBS")
  llVMPubsWasOpen = .f.
  open data vmpubs
 ELSE
  llVMPubsWasOpen = .t.
ENDIF
on error &lcOnError
IF llError = .t.
  wait window "Unable to open the VMPUBS.DBC"
  return
ENDIF  

lnConnectHandle = SQLConnect("C_PUBS")
IF lnConnectHandle <= 0
  wait window "Unable to connect to the SQL-Server sample PUBS database" + chr(13) + ;
              "via the C_PUBS connection in VMPUBS.DBC"
  return
ENDIF

lcString = "409-56-7008"
IF x3ckchek("Authors", ;
            lcString, ;
            "AU_ID", ;
            "!!!-!!-!!!!", ;
            "AU_ID", ;
            "Au_LName", ;
            lnConnectHandle)
  wait window lcString + " is already on file"
 ELSE
  wait window lcString + " is not already on file"
ENDIF  

lcString = "409-56-xxxx"
IF x3ckchek("Authors", ;
            lcString, ;
            "AU_ID", ;
            "!!!-!!-!!!!", ;
            "AU_ID", ;
            "Au_LName", ;
            lnConnectHandle)
  wait window lcString + " is already on file"
 ELSE
  wait window lcString + " is not already on file"
ENDIF  

IF !llVMPUBSWasOpen
  set database to VMPUBS
  close database
ENDIF

IF empty(lcSetDatabase)
  set database to
 ELSE
  set database to (lcSetDatabase)
ENDIF

=sqldisconnect(lnConnectHandle)
return
