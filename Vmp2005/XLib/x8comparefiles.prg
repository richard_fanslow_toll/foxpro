*
*  X8CompareFiles.PRG
*  RETURNs a logical value indicating whether the passed
*  files are the same, via a simple FILETOSTR() comparison.  
*  RETURNs .NULL. if the parameters are passed incorrectly
*  or the files cannot be located/opened/etc.
*  Saves you from having to do the following steps:
*    1- Click on <Start> (in the lower-left corner of your desktop)
*    2- Click on the "Run..." option
*    3- Enter "command" and click <OK> (or press <Enter>)
*    4- At the DOS prompt:
*		a) optionally, CD <the directory in which the files reside>
*		b) FC <first file> <second file>
*		c) See if it says "FC: no differences encountered"
*		d) EXIT (to exit the "DOS prompt" window)
*
*  Copyright (c) 2003-2005 Visionpace   All Rights Reserved
*                17501 East 40 Hwy., Suite 218
*                Independence, MO 64055
*                816-350-7900 
*                http://www.visionpace.com
*                http://vmpdiscussion.visionpace.com
*  Author:  Art Bergquist, Drew Speedie
*  
LPARAMETERS tcFile1, tcFile2

IF NOT PCOUNT() = 2 ;
     OR NOT VARTYPE(tcFile1) = "C" ;
     OR NOT VARTYPE(tcFile2) = "C"
  ASSERT .f. message ;
       "Parameters must be passed in the format:" + ;
       CHR(13) + CHR(13) + ;
       "FileName1, FileName2"
  RETURN .NULL.
ENDIF             

IF EMPTY(tcFile1)
  ASSERT .f. message ;
       "The first (required) parameter must be passed as a FileName."
  RETURN .NULL.
ENDIF             

IF EMPTY(tcFile2)
  ASSERT .f. message ;
       "The second (required) parameter must be passed as a FileName."
  RETURN .NULL.
ENDIF             

*
*  tcFile1 and tcFile2 are both non-blank character
*  strings, but may not be actual/valid file names,
*  so we'll wrap the FILETOSTR() comparison in 
*  a TRY..CATCH block
*
LOCAL llError, llSame
TRY
llSame = (FILETOSTR(tcFile1) == FILETOSTR(tcFile2))
CATCH
llError = .t.
ENDTRY

IF llError
  RETURN .NULL.
 ELSE
  RETURN llSame
ENDIF

  