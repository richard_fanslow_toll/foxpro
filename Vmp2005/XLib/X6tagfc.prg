*
*  X6TAGFC.PRG
*  Returns a chr(13)-delimited string of Captions for
*  the field names that are found in the expresssion
*  for the passed index tag of the passed ALIAS().
*
*  Copyright (c) 2000-2005 Visionpace   All Rights Reserved
*                17501 East 40 Hwy., Suite 218
*                Independence, MO 64055
*                816-350-7900 
*                http://www.visionpace.com
*                http://vmpdiscussion.visionpace.com
*  Author:  Drew Speedie
*           Special thanks to Mike Potjer
*
*  For any field for which a Caption is not contained
*  in the .DBC, the field name is RETURNed instead.
*
*  Since cursors that are not contained in a .DBC
*  cannot have Captions specified, the field names found
*  in the passed index tag are RETURNed.
*
*  If anything goes wrong/cannot be processed here,
*  SPACE(0) is RETURNed.
*
*  PLEASE NOTE that INTL-enabled applications should
*  be sure to add records to the STRINGS.DBF table
*  for each Caption corresponding to a field in a 
*  Candidate Key index tag, since the X3I() of those
*  Captions are RETURNed here from this routine.
* 
*  lParameters
*    tcTagName (R) Name of the index tag whose Captions/
*                    field names are to be RETURNed
*      tcAlias (O) Alias (or work area number) to which
*                    tcTagName belongs
*                  If not passed, defaults to the current
*                    ALIAS()
*
LPARAMETERS tcTagName, tcAlias

LOCAL lcAlias, ;
	  lcFieldNames, ;
	  lcOldDatabase, ;
	  lcDatabase, ;
	  lcTagExpr, ;
	  laFields[1], ;
	  lcField, ;
	  lnFieldCount, ;
	  lnOccurrence, ;
	  laKeyFields[1], ;
	  lnCount, ;
	  lcCaption

lcFieldNames = SPACE(0)

IF VARTYPE(tcTagName) # "C" OR EMPTY(tcTagName) 
  RETURN lcFieldNames
ENDIF

LOCAL lcAlias, lcOnError, llError
DO CASE
  *********************************************************************
  CASE VARTYPE(tcAlias) = "C" AND !EMPTY(tcAlias) 
  *********************************************************************
    lcAlias = UPPER(ALLTRIM(tcAlias))
  *********************************************************************
  CASE VARTYPE(tcAlias) = "N" AND !EMPTY(tcAlias)
  *********************************************************************
    lcOnError = ON("ERROR")
    ON ERROR llError = .t.
    lcAlias = ALIAS(tcAlias)
    ON ERROR &lcOnError
  *********************************************************************
  CASE !EMPTY(ALIAS())
  *********************************************************************
    lcAlias = UPPER(ALLTRIM(ALIAS()))    
  *********************************************************************
  OTHERWISE
  *********************************************************************
    llError = .t.
ENDCASE
IF llError OR !USED(lcAlias)
  RETURN lcFieldNames
ENDIF
  
lcDatabase = UPPER(JUSTSTEM(CURSORGETPROP("DATABASE",lcAlias)))

*
*  get the expression for the specified tag
*
lcTagExpr = X3TAGXPR(tcTagName,lcAlias)

*
*  get a list/array of fields for the indicated alias
*
IF AFIELDS(laFields,lcAlias) = 0
  RETURN lcFieldNames 
ENDIF

*
*  generate an array of field information for those
*  fields (field name strings) found in the index
*  tag expression
*
lnFieldCount = 0
FOR lnCount = 1 TO ALEN(laFields,1)
  lnOccurrence = 1
  lcField = UPPER(ALLTRIM(laFields[lnCount,1]))
  DO WHILE ATCC(lcField,lcTagExpr,lnOccurrence) > 0
    lnFieldCount = lnFieldCount + 1
    DIMENSION laKeyFields[lnFieldCount,2]
    laKeyFields[lnFieldCount,1] = lcField
    laKeyFields[lnFieldCount,2] = ATCC(lcField,lcTagExpr,lnOccurrence)
    lnOccurrence = lnOccurrence + 1
  ENDDO
ENDFOR

IF lnFieldCount = 0
  *
  *  should rarely, if ever be the case...
  *
  RETURN lcFieldNames
ENDIF

*
*  sort the key fields based on their position in the tag expression
*
ASORT(laKeyFields,2)

*
*  some strings in laKeyFields may not be actual field
*  names -- they may be strings that are portions of 
*  field names
*
lnCount = 1
DO WHILE lnCount < lnFieldCount
  IF laKeyFields[lnCount,2] # laKeyFields[lnCount+1,2]
    *
    *  the current key field is different from the next 
    *  one, so move to the next field
    *
    lnCount = lnCount + 1
   ELSE
    *
    *  two different field names were found in the same
    *  position, remove the smaller field name, since its
    *  name is apparently a subset of the larger field's name 
    *
    IF LENC(ALLTRIM(laKeyFields[lnCount,1])) < LENC(ALLTRIM(laKeyFields[lnCount+1,1]))
      ADEL(laKeyFields,lnCount)
     ELSE
      ADEL(laKeyFields,lnCount+1)
    ENDIF
    lnFieldCount = lnFieldCount - 1
    DIMENSION laKeyFields[lnFieldCount,2]
  ENDIF
ENDDO

*
*  at this point, you could also check if the same field 
*  appears more than once in the list of fields, and 
*  eliminate the duplicates.  However, using the same field 
*  more once in tag is neither desirable nor likely
*

lcOldDatabase = set("DATABASE") 
IF !EMPTY(lcDatabase)
  SET DATABASE TO (lcDatabase)
ENDIF

*
*  now get the Captions from the .DBC, if any
*
FOR lnCount = 1 TO lnFieldCount
  *
  *  save the field Caption in the array
  *
  lcCaption = SPACE(0)
  *
  *  if there is no caption, save the field name 
  *  in place of the caption
  *
  IF !EMPTY(lcDatabase)
    lcCaption = ;
         DBGETPROP(CURSORGETPROP("SourceName",lcAlias) + "." + laKeyFields[lnCount,1], ;
	               "FIELD", ;
		           "Caption" )
    IF !EMPTY(lcCaption)
      lcCaption = X3I(lcCaption)
    ENDIF		           
  ENDIF
  IF EMPTY(lcCaption)
    *  use the field name
    lcCaption = laKeyFields[lnCount,1]
  ENDIF
  *
  *  add the caption to the key fields string, 
  *  inserting a carriage return between each one
  *
  lcFieldNames = lcFieldNames + ;
                 IIF(EMPTY(lcFieldNames),SPACE(0),CHR(13)) + ;
                 lcCaption
ENDFOR

IF EMPTY(lcOldDatabase)
  SET DATABASE TO 
 ELSE
  SET DATABASE TO &lcOldDatabase
ENDIF 
 
RETURN lcFieldNames
