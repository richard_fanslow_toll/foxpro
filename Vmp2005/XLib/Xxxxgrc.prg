*
*  VMP base Column class SuperClass
*
*  changes you make here are inherited by ALL
*  your intermediate and app-specific column
*  classes
*
*  this is the parent class of the grcBase header
*  class in XXFWLEAF.PRG
*    XXDTES("XXFWLEAF.PRG","DEFINE CLASS grcBase")
*
*  see the zReadMe of any of the visual VMP super
*  classes:
*    MODIFY CLASS txtVMPSuperClass OF XXXXtxt.VCX METHOD zReadMe
*
DEFINE CLASS grcVMPSuperClass AS Column 
ENDDEFINE