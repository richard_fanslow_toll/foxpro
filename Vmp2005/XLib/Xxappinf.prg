*
*  XXAPPINF.PRG
*  RETURN the indicated property value from 
*  oAppConfig/oAppInfo (ultimately, the corresponding
*  value from the AppConfig/AppConfig table) 
*
*  RETURNs .NULL. if the value cannot be determined
*  (oAppConfig/oAppInfo/AppConfig/AppInfo item does
*  not exist, or oAppInfo/oAppConfig do not exist)
*
*  Copyright (c) 1997-2005 Visionpace   All Rights Reserved
*                17501 East 40 Hwy., Suite 218
*                Independence, MO 64055
*                816-350-7900 
*                http://www.visionpace.com
*                http://vmpdiscussion.visionpace.com
*  Author:  Drew Speedie  
*
*  you can check an oAppConfig/oAppInfo property value
*  directly, rather than call this routine, if you 
*  know which property of which object you need; however,
*  if you don't know which object has the property you
*  need or if the property even exists, calling this
*  routine takes the place of code like this:
*    LOCAL luValue
*    luValue = oAppConfig.GetPropertyValue("SomeSetting")
*    IF ISNULL(m.luValue) AND VARTYPE(m.oAppInfo) = "O"
*      luValue = oAppInfo.GetPropertyValue("SomeSetting")
*    ENDIF
*
*  This program is called by oApp.GetAppInfo() and 
*  oLib.GetAppConfigValue() -- those methods are just 
*  'wrappers' to this program, which can be called as a 
*  standalone program, to make it easier to use in 
*  standalone forms and stored procedures.
*
*
*  You can also call this routine directly, from the
*  Command Window, and as long as the database containing
*  AppInfo is currently open, this routine RETURNs any
*  AppInfo/AppConfig value.
*  
*
*  lParameters
*         tcItem (R) oAppConfig/oAppInfo property whose value
*                      is to be RETURNed (ultimately a value
*                      from AppConfig/AppInfo table) 
*         tlDesc (O) Pass this parameter as .T. to return the 
*                      description instead (Ap_ItemDsc/
*                      App_ItemDescription field value)
* tlRefreshFirst (O) Pass this parameter as .T. to indicate
*                      that, before retrieving the current
*                      value of tcItem from oAppInfo, that
*                      value should be refreshed from its
*                      value on disk.  For example:
*                      - UserA and UserB are running your app
*                      - UserA updates something in the AppInfo
*                        table, typically by calling 
*                        oAppInfo.SavePropertiesToDataSource()
*                      - UserB's oAppInfo object doesn't know
*                        about the update UserA made, because
*                        oAppInfo is an object whose properties
*                        are loaded up from the AppInfo table
*                        on app startup, and never thereafter
*                        unless you do so manually, re-executing
*                        oAppInfo.LoadPropertiesFromDataSource()
*                        (for all settings or one specific setting)
*                      - You have code that depends on UserB 
*                        reading the setting as it was just 
*                        updated by UserA
*                      - Your code can take any one of these forms,
*                          the first of which must manually refresh
*                          the property value from disk:
*                        LOCAL luValue
*                        oAppInfo.LoadPropertiesFromDataSource(.f.,"PropertyName")  &&& refresh it from disk
*                        luValue = oAppInfo.GetAppInfo.GetAppInfo("PropertyName")  
*                        IF m.llValue = ...
*                          *  take appropriate action
*                        ENDIF
*                          or 
*                        LOCAL luValue
*                        luValue = XXAPPINF("PropertyName",.f.,.t.)
*                        IF m.llValue = ...
*                          *  take appropriate action
*                        ENDIF
*                          or
*                        LOCAL luValue
*                        luValue = oAppInfo.GetAppInfo.GetAppInfo("PropertyName",.f.,.t.)  
*                        IF m.llValue = ...
*                          *  take appropriate action
*                        ENDIF
*                          or
*                        LOCAL luValue
*                        luValue = oAppInfo.GetPropertyValue("PropertyName",.t.)
*                        IF m.llValue = ...
*                          *  take appropriate action
*                        ENDIF
*                    NOTE that this ONLY works for AppInfo values,
*                      NOT for AppConfig values, which are inherently
*                      ReadOnly!           
*                    See also:
*                      XXDTES("XXFW.VCX","tlRefreshFirst","ctrapp","GETAPPINFO")
*                                 
LPARAMETERS tcItem, tlDesc, tlRefreshFirst

LOCAL lcPropertyName, luReturnValue, llDone
luReturnValue = .NULL.

*
*  oAppInfo/AppInfo takes precedence
*
IF VARTYPE(m.oAppInfo) = "O"  
  lcPropertyName = oAppInfo.GetPropertyName(m.tcItem)
  IF NOT TYPE("m.oAppInfo."+m.lcPropertyName) = "U"
    *
    *  tcItem/property exists as an oAppInfo property
    *
    IF VARTYPE(m.tlDesc) = "L" AND m.tlDesc
      IF VARTYPE(m.tlRefreshFirst) = "L" AND m.tlRefreshFirst
        *
        *  refresh its value (and description) to
        *  make sure we're getting the latest value
        *  (which may have been recently updated 
        *  by another user)
        *
        oAppInfo.LoadPropertiesFromDataSource(.f.,m.lcPropertyName)
      ENDIF
      luReturnValue = oAppInfo.GetPropertyDescription(m.lcPropertyName)
      llDone = .t.
     ELSE
      luReturnValue = oAppInfo.GetPropertyValue(m.lcPropertyName,m.tlRefreshFirst)
      llDone = .t.
    ENDIF    
  ENDIF   &&& property exists
ENDIF   &&& VARTYPE(m.oAppInfo) = "O"  
*!*	IF VARTYPE(m.oAppInfo) = "O"  
*!*	  lcPropertyName = oAppInfo.GetPropertyName(m.tcItem)
*!*	  DO CASE
*!*	    ****************************************************
*!*	    CASE TYPE("m.oAppInfo."+m.lcPropertyName) = "U"
*!*	    ****************************************************
*!*	      *
*!*	      *  tcItem/property does not exist on oAppInfo 
*!*	      *
*!*	    ****************************************************
*!*	    CASE VARTYPE(m.tlDesc) = "L" AND m.tlDesc
*!*	    ****************************************************
*!*	      luReturnValue = oAppInfo.GetPropertyDescription(m.lcPropertyName)
*!*	      llDone = .t.
*!*	    ****************************************************
*!*	    OTHERWISE
*!*	    ****************************************************
*!*	      luReturnValue = oAppInfo.GetPropertyValue(m.lcPropertyName)
*!*	      llDone = .t.
*!*	  ENDCASE
*!*	ENDIF   &&& VARTYPE(m.oAppInfo) = "O"  

IF m.llDone
  RETURN m.luReturnValue
ENDIF

IF VARTYPE(m.oAppConfig) = "O"  
  lcPropertyName = oAppConfig.GetPropertyName(m.tcItem)
  DO CASE
    ****************************************************
    CASE TYPE("m.oAppConfig."+m.lcPropertyName) = "U"
    ****************************************************
      *
      *  tcItem/property does not exist on AppConfig
      *
    ****************************************************
    CASE VARTYPE(m.tlDesc) = "L" AND m.tlDesc
    ****************************************************
      luReturnValue = oAppConfig.GetPropertyDescription(m.lcPropertyName)
      llDone = .t.
    ****************************************************
    OTHERWISE
    ****************************************************
      luReturnValue = oAppConfig.GetPropertyValue(m.lcPropertyName)
      llDone = .t.
  ENDCASE
ENDIF   &&& VARTYPE(m.oAppConfig) = "O"  

IF m.llDone
  RETURN m.luReturnValue
ENDIF

*
*  you get here when neither oAppInfo nor oAppConfig exists
*  OR
*  when neither oAppInfo nor oAppConfig contains the
*  specified tcItem as a custom property
*  ...we don't anticipate getting here too often in a
*  VMP application...
*

LOCAL lnSelect
lnSelect = SELECT(0)

*
*  ...so we'll try any open AppInfo cursor
*
IF USED("AppInfo")
  SELECT AppInfo
  LOCATE FOR UPPER(ALLTRIM(App_Item)) == UPPER(ALLTRIM(m.tcItem)) NOOPTIMIZE 
  IF FOUND()
    llDone = .t.
    IF VARTYPE(tlDesc) = "L" AND tlDesc
      luReturnValue = ALLTRIM(App_ItemDescription)
     ELSE
      luReturnValue = X8ConvChar(App_ItemValue,App_ItemDataType)
    ENDIF
  ENDIF
  SELECT (m.lnSelect)
ENDIF

IF m.llDone
  RETURN m.luReturnValue
ENDIF

*
*  ...or any open AppConfig cursor
*
IF USED("AppConfig")
  SELECT AppConfig
  LOCATE FOR UPPER(ALLTRIM(Ap_Item)) == UPPER(ALLTRIM(m.tcItem)) NOOPTIMIZE 
  IF FOUND()
    llDone = .t.
    IF VARTYPE(tlDesc) = "L" AND tlDesc
      luReturnValue = ALLTRIM(Ap_ItemDsc)
     ELSE
      luReturnValue = X8ConvChar(Ap_ItemVal,Ap_ItemDTp)
    ENDIF
  ENDIF
  SELECT (m.lnSelect)
ENDIF

IF m.llDone
  RETURN m.luReturnValue
ENDIF

IF VERSION(2) = 2
  *
  *  this .PRG has been called in the development
  *  environment, from the Command Window, or from
  *  some testing code -- this is just a convenience
  *  so that you can call this routine from the
  *  Command Window, outside a running app/form
  *
  *
  *  ...try AppInfo table in any open database
  *
  LOCAL lcSetDatabase, laDatabases[1], lnDatabases, xx, ;
        lcAppConfig
  lcSetDatabase = SET("DATABASE")
  IF NOT EMPTY(SET("DATABASE")) AND ;
       (INDBC("AppInfo","TABLE") OR INDBC("AppInfo","VIEW"))
    *
    *  the current SET DATABASE TO database contains
    *  AppInfo table (or remote view)
    *
   ELSE
    *
    *  see if we can find an open database that contains
    *  AppInfo and, if so, SET DATABASE to it
    *
    lnDatabases = ADATABASES(laDatabases)
    FOR xx = 1 TO lnDatabases
      SET DATABASE TO (m.laDatabases[m.xx,1])
      IF INDBC("AppInfo","TABLE") OR INDBC("AppInfo","VIEW")
        EXIT
      ENDIF
    ENDFOR
  ENDIF
  IF NOT EMPTY(SET("DATABASE")) ;
       AND (INDBC("AppInfo","TABLE") OR INDBC("AppInfo","VIEW"))
    *
    *  open AppInfo
    *
    TRY 
    USE APPINFO AGAIN IN 0 ALIAS XXAPPINF SHARED
    CATCH
    ENDTRY
    IF USED("XXAPPINF")
      SELECT XXAPPINF
      LOCATE FOR UPPER(App_Item) = PADR(UPPER(ALLTRIM(tcItem)),LENC(App_Item)) NOOPTIMIZE 
      IF FOUND()
        llDone = .t.
        IF VARTYPE(m.tlDesc) = "L" AND m.tlDesc
          luReturnValue = ALLTRIM(App_ItemDescription)
         ELSE
          luReturnValue = X8ConvChar(App_ItemValue,App_ItemDataType)
        ENDIF
       ELSE
        LOCATE FOR UPPER(ALLTRIM(App_Item)) == UPPER("FreeConfigTable")
        IF FOUND()
          lcAppConfig = UPPER(ALLTRIM(App_ItemValue))
        ENDIF
      ENDIF   &&& FOUND()
    ENDIF   &&& USED("XXAPPINF")
  ENDIF
  USE IN SELECT("XXAPPINF")
  SET DATABASE TO &lcSetDatabase
  IF m.llDone
    SELECT (m.lnSelect)
    RETURN m.luReturnValue
  ENDIF
  IF VARTYPE(m.lcAppConfig) = "C" ;
       AND NOT EMPTY(m.lcAppConfig) ;
       AND FILE(m.lcAppConfig)
    TRY
    USE (m.lcAppConfig) AGAIN IN 0 ALIAS XXAPPINF
    CATCH
    ENDTRY
    IF USED("XXAPPINF")
      SELECT XXAPPINF
      LOCATE FOR UPPER(Ap_Item) = PADR(UPPER(ALLTRIM(tcItem)),LENC(Ap_Item)) NOOPTIMIZE 
      IF FOUND()
        IF VARTYPE(m.tlDesc) = "L" AND m.tlDesc
          luReturnValue = ALLTRIM(Ap_ItemDsc)
         ELSE
          luReturnValue = X8ConvChar(Ap_ItemVal,Ap_ItemDTp)
        ENDIF
      ENDIF   &&& FOUND()
    ENDIF   &&& USED("XXAPPINF")
  ENDIF
  USE IN SELECT("XXAPPINF")       
ENDIF   &&& VERSION(2) = 2

SELECT (m.lnSelect)

RETURN m.luReturnValue




*
*  this is the code that was here prior to the
*  March 5, 2004 build 
*

*!*	*
*!*	*  XXAPPINF.PRG
*!*	*  Retrieves the indicated value from APPINFO/APPCONFIG 
*!*	*  table(s).  
*!*	*  This program is called by oApp.GetAppInfo() and 
*!*	*  oLib.GetAppConfigValue() -- those methods are just 
*!*	*  'wrappers' to this program, which can be called as a 
*!*	*  standalone program, to make it easier to use in 
*!*	*  standalone forms and stored procedures.
*!*	*
*!*	*  Copyright (c) 1997-2005 Visionpace   All Rights Reserved
*!*	*                17501 East 40 Hwy., Suite 218
*!*	*                Independence, MO 64055
*!*	*                816-350-7900 
*!*	*                http://www.visionpace.com
*!*	*                http://vmpdiscussion.visionpace.com
*!*	*  Author:  Drew Speedie and Ed Lennon 
*!*	*
*!*	*  Requires that
*!*	*    1) X2PSTACK() be available
*!*	*    2) if called from oApp.GetAppInfo(), that the AppInfo 
*!*	*       and AppConfig tables are open (except for No-VMP-DBC
*!*	*       scenarios, in which case there is no AppInfo, and
*!*	*       oApp.GetAppInfo() calls oLib.GetAppConfigValue()
*!*	*       anyway)
*!*	*    3) if called from oLib.GetAppConfigValue(), that the 
*!*	*       AppConfig table is open
*!*	*    4) if called from anywhere else (stored procedure, 
*!*	*       standalone form, UDF, etc.)
*!*	*       a) the .DBC containing AppInfo is DBUSED()
*!*	*       b) the xxCONFIG.DBF/AppConfig table is in the 
*!*	*          current VFP path
*!*	*
*!*	*  lParameters
*!*	*    tcItem (R) APPINFO.App_Item/APPCONFIG.Ap_Item to find 
*!*	*                 and return
*!*	*    tlDesc (O) Pass this parameter as .T. to return the 
*!*	*                 Ap_ItemDsc/App_ItemDescription field value
*!*	*                 instead of the App_Item/Ap_Item field value
*!*	*
*!*	LPARAMETERS tcItem, tlDesc

*!*	LOCAL luReturnValue, lcWhoCalled, laDatabases[1], lnDatabases
*!*	luReturnValue = .NULL.
*!*	lnDatabases = ADATABASES(laDatabases)

*!*	DO CASE
*!*	  CASE X2PSTACK(".GetAppInfo ")
*!*	    lcWhoCalled = "A"    &&& oApp.GetAppInfo()
*!*	  CASE X2PSTACK(".GetAppConfigValue ")
*!*	    lcWhoCalled = "L"    &&& oLib.GetAppConfigValue()
*!*	  OTHERWISE              
*!*	    lcWhoCalled = "X"    &&& some other code
*!*	ENDCASE

*!*	IF lcWhoCalled = "X" ;
*!*	     AND NOT USED("AppInfo") ;
*!*	     AND NOT USED("AppConfig") ;
*!*	     AND lnDatabases=0 
*!*	  RETURN luReturnValue   &&& .NULL.
*!*	ENDIF

*!*	IF lcWhoCalled = "A" ;
*!*	     AND NOT USED("AppInfo") AND NOT USED("AppConfig")
*!*	  RETURN luReturnValue   &&& .NULL.
*!*	ENDIF

*!*	IF lcWhoCalled = "L" AND NOT USED("AppConfig")
*!*	  RETURN luReturnValue   &&& .NULL.
*!*	ENDIF

*!*	LOCAL lnSelect, llDone, llAppInfoUsed, llAppConfigUsed
*!*	lnSelect = SELECT(0)

*!*	IF USED("APPINFO") AND INLIST(lcWhoCalled,"A","X")
*!*	  llAppInfoUsed = .t.
*!*	  SELECT APPINFO
*!*	  LOCATE FOR UPPER(App_Item) = PADR(UPPER(ALLTRIM(tcItem)),LENC(App_Item)) NOOPTIMIZE 
*!*	  IF FOUND()
*!*	    llDone = .t.
*!*	    IF VARTYPE(tlDesc) = "L" AND tlDesc
*!*	      luReturnValue = ALLTRIM(App_ItemDescription)
*!*	     ELSE
*!*	      luReturnValue = X8ConvChar(App_ItemValue,App_ItemDataType)
*!*	    ENDIF
*!*	  ENDIF
*!*	ENDIF

*!*	IF llDone
*!*	  SELECT (lnSelect)
*!*	  RETURN luReturnValue
*!*	ENDIF 

*!*	IF USED("APPCONFIG")
*!*	  llAppConfigUsed = .t.
*!*	  SELECT APPCONFIG
*!*	  LOCATE FOR UPPER(Ap_Item) = PADR(UPPER(ALLTRIM(tcItem)),LENC(Ap_Item)) NOOPTIMIZE 
*!*	  IF FOUND()
*!*	    llDone = .t.
*!*	    IF VARTYPE(tlDesc) = "L" AND tlDesc
*!*	      luReturnValue = ALLTRIM(Ap_ItemDsc)
*!*	     ELSE
*!*	      luReturnValue = X8ConvChar(Ap_ItemVal,Ap_ItemDTp)
*!*	    ENDIF
*!*	  ENDIF
*!*	ENDIF

*!*	IF llDone ;
*!*	     OR (llAppConfigUsed AND llAppInfoUsed) ;
*!*	     OR (llAppConfigUsed AND lcWhoCalled = "L")
*!*	  *
*!*	  *  llDone
*!*	  *    the LOCATE in one or the other was successful
*!*	  *  llAppConfigUsed AND llAppInfoUsed
*!*	  *    both AppConfig and AppInfo are open, we're done whether
*!*	  *    the LOCATE was successful or not
*!*	  *  llAppConfigUsed AND lcWhoCalled = "L"
*!*	  *    AppConfig is open, and oLib.GetAppConfigValue() called
*!*	  *    here, so we're done, whether the LOCATE was FOUND() or not
*!*	  *
*!*	  SELECT (lnSelect)
*!*	  RETURN luReturnValue
*!*	ENDIF 

*!*	*
*!*	*  either 
*!*	*    AppInfo or AppConfig or both were closed when this 
*!*	*    program was called
*!*	*  or 
*!*	*    AppConfig was closed and lcWhoCalled = "L"
*!*	*
*!*	LOCAL lcSetDatabase, llError, xx
*!*	lcSetDatabase = SET("DATABASE")
*!*	*
*!*	*  try AppInfo first
*!*	*
*!*	IF NOT lcWhoCalled = "L"   &&& oLib.GetAppConfigValue() only cares about AppConfig
*!*	  IF NOT llAppInfoUsed
*!*	    IF NOT EMPTY(SET("DATABASE")) AND INDBC("AppInfo","TABLE")
*!*	      *
*!*	      *  the current SET DATABASE TO database contains
*!*	      *  AppInfo table
*!*	      *
*!*	     ELSE
*!*	      *
*!*	      *  see if we can find an open database that contains
*!*	      *  AppInfo and, if so, SET DATABASE to it
*!*	      *
*!*	      FOR xx = 1 TO lnDatabases
*!*	        SET DATABASE TO (laDatabases[xx,1])
*!*	        IF INDBC("AppInfo","TABLE")
*!*	          EXIT
*!*	        ENDIF
*!*	      ENDFOR
*!*	    ENDIF
*!*	    IF NOT EMPTY(SET("DATABASE")) AND INDBC("AppInfo","TABLE")
*!*	      *
*!*	      *  open AppInfo
*!*	      *
*!*	      TRY 
*!*	        USE APPINFO IN 0 shared 
*!*	      CATCH
*!*	        llError = .t.
*!*	      ENDTRY
*!*	      IF NOT USED("AppInfo") OR llError
*!*	        llError = .t.
*!*	      ENDIF
*!*	      IF NOT llError 
*!*	        SELECT AppInfo
*!*	        LOCATE FOR UPPER(App_Item) = PADR(UPPER(ALLTRIM(tcItem)),lenc(App_Item)) nooptimize
*!*	        IF FOUND()
*!*	          IF VARTYPE(tlDesc) = "L" AND tlDesc
*!*	            luReturnValue = ALLTRIM(App_ItemDescription)
*!*	           ELSE
*!*	            luReturnValue = X8ConvChar(App_ItemValue,App_ItemDataType)
*!*	          ENDIF
*!*	          llDone = .t.
*!*	        ENDIF
*!*	      ENDIF  
*!*	     ELSE
*!*	      *
*!*	      *  no currently open database contains AppInfo --
*!*	      *  nothing more we can do -- luReturnValue = .NULL.
*!*	      *
*!*	      llDone = .t. 
*!*	    ENDIF
*!*	  ENDIF
*!*	ENDIF   &&& NOT lcWhoCalled = "L"

*!*	SET DATABASE TO &lcSetDatabase

*!*	IF llDone OR llError
*!*	  IF USED("APPINFO") AND NOT llAppInfoUsed
*!*	    USE IN APPINFO
*!*	  ENDIF
*!*	  SELECT (lnSelect)
*!*	  RETURN luReturnValue
*!*	ENDIF 

*!*	*
*!*	*  now try AppConfig
*!*	*
*!*	llError = .f.
*!*	IF NOT llAppConfigUsed
*!*	  SELECT APPINFO
*!*	  LOCATE FOR UPPER(ALLTRIM(App_Item)) == "FREECONFIGTABLE"
*!*	  LOCAL lcConfigFile
*!*	  lcConfigFile = SPACE(0)
*!*	  IF FOUND()
*!*	    lcConfigFile = X8ConvChar(App_ItemValue,App_ItemDataType)
*!*	   ELSE
*!*	    lcConfigFile = .NULL.
*!*	  ENDIF
*!*	  IF NOT ISNULL(lcConfigFile) AND FILE(lcConfigFile)
*!*	    TRY
*!*	      USE (lcConfigFile) again IN 0 ALIAS AppConfig shared 
*!*	    CATCH
*!*	      llError = .t.
*!*	    ENDTRY
*!*	    IF NOT USED("AppConfig") OR llError
*!*	      llError = .t.
*!*	    ENDIF
*!*	    IF NOT llError 
*!*	      SELECT AppConfig
*!*	      LOCATE FOR UPPER(Ap_Item) = PADR(UPPER(ALLTRIM(tcItem)),lenc(Ap_Item)) nooptimize
*!*	      IF FOUND()
*!*	        IF VARTYPE(tlDesc) = "L" AND tlDesc
*!*	          luReturnValue = ALLTRIM(Ap_ItemDsc)
*!*	         ELSE
*!*	          luReturnValue = X8ConvChar(Ap_ItemVal,Ap_ItemDTp)
*!*	        ENDIF
*!*	      ENDIF
*!*	    ENDIF  &&& !llError
*!*	  ENDIF  &&& !isnull(lcConfigFile)
*!*	ENDIF

*!*	IF NOT llAppInfoUsed AND USED("APPINFO") 
*!*	  USE IN APPINFO
*!*	ENDIF

*!*	IF NOT llAppConfigUsed AND USED("APPCONFIG")
*!*	  USE IN APPCONFIG
*!*	ENDIF

*!*	SELECT (lnSelect)

*!*	RETURN luReturnValue




