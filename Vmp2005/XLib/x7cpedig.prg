*
*  X7CPEDIG.PRG
*  Answers the question:  
*  Does the passed toObject have the passed tcClassName 
*    in its pedigree?  
*  OR 
*  Is tcClassName somewhere in the ancestry of toObject?
*
*
*  Use this routine instead of X3CPEDIG.PRG, which is
*  included for backward compatibility of existing
*  VMP application code.
*
*
*  Copyright (c) 1997-2005 Visionpace   All Rights Reserved
*                17501 East 40 Hwy., Suite 218
*                Independence, MO 64055
*                816-350-7900 
*                http://www.visionpace.com
*                http://vmpdiscussion.visionpace.com
*  Author:  Drew Speedie
*
*
*  lParameters
*       toObject (R) Object reference to the object whose 
*                      pedigree is to be checked
*    tcClassName (R) Check the ACLASS() pedigree of toObject 
*                      for this class name
*
*  Usage:
*
*  * from form method code:
*  IF X7CPEDIG(THIS,"FRMDATA")
*    * THISFORM is a form from the XXFW.VCX/frmData class hierarchy
*   ELSE
*    * THISFORM does not inherit from XXFW.VCX/frmData
*  ENDIF
*
*  * from textbox method code:
*  IF X7CPEDIG(THIS,"txtPicklistValid")
*    * THIS textbox is from the XXFWCTRL.VCX/txtPicklistValid class hierarchy
*   ELSE
*    * THIS textbox does not inherit from XXFWCTRL.VCX/txtPicklistValid
*  ENDIF
*
*  
*  In general, this routine is faster than X2PSTACK() --
*  when either one will work in a particular situation,
*  use this routine instead of X2PSTACK().
*
*
LPARAMETERS toObject, tcClassName

LOCAL laClass[1]
RETURN ACLASS(laClass,m.toObject)>0 AND ASCAN(laClass,m.tcClassName,1,-1,1,15)>0
 