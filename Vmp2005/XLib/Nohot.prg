Procedure NoHot
*  Author............: Steven M. Black
*} Project...........: common
*  Created...........: 05/09/92
*  Copyright.........: (c) Steven Black Consulting, 1993
*) Description.......: PROCEDURE NoFeatures
*)                     Feed it a string, and it strips out hotkey assignments
*)                     returning the featurless string
*] Dependencies......:
*  Calling Samples...: nohot(<ExpC>)
*  Parameter List....:
PARAMETERS tcPassedPrompt
*-- This is the fastest, though not the most legible, way
*-- to code this.
*--                                     Hot Key, Ctrl Enter, Escape
RETURN STRTRAN( STRTRAN( STRTRAN( tcPassedPrompt, "\<"), "\!"), "\?")
