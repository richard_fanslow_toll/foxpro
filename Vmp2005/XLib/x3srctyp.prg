*
*  X3SRCTYP.PRG
*  Returns the SourceType:
*    "SPT" = SQL Pass-Thru
*     "TC" = Table/Contained
*     "TF" = Table/Free or 2x
*     "VL" = View/Local
*     "VR" = View/Remote
*      "C" = CREATE CURSOR/SELECT-SQL cursor
*    "OVR" = Offline View/Remote
*    "OVL" = Offline View/Local  
*  of whichever of the following has been passed:
*    -the alias of the ControlSource of the passed object
*    -the alias of the passed Database.Alias.FieldName
*    -the alias of the passed Alias.FieldName
*    -the passed alias
*
*  Copyright (c) 1997-2005 Visionpace   All Rights Reserved
*                17501 East 40 Hwy., Suite 218
*                Independence, MO 64055
*                816-350-7900 
*                http://www.visionpace.com
*                http://vmpdiscussion.visionpace.com
*  Author:  Drew Speedie
*
*
*  REQUIRES:
*    X3CSOURC.PRG
*
*
*  Usage:
*    xx = x3srctyp(THIS)
*    xx = x3srctyp(THIS.ControlSource)
*    xx = x3srctyp("VM!Customers")
*    xx = x3srctyp("Customers")
*    xx = x3srctyp("Customers.Cus_Name")
*    xx = x3srctyp("VM!Customers.Cus_Name")
*    xx = x3srctyp("V_Customers")
*    xx = x3srctyp("V_CUSTOMERS.Cus_Name")
*    xx = x3srctyp("VM!V_CUSTOMERS.Cus_Name")
*    xx = x3srctyp("V_PUBLISHERS.Pub_Name")
*    xx = x3srctyp("VM!V_PUBLISHERS.Pub_Name")
*
*  lParameters
*    tuParameter (O) A string containing any of the following:
*                        Alias
*                        Alias.Fieldname
*                        Database!Alias.Fieldname
*                        Database!Alias
*                        (for all the above, the Alias must be
*                         open in the current data session) 
*                    -OR- an object reference whose .ControlSource 
*                         we'll check here
*                    Defaults to ALIAS()
*
lparameters tuParameter 
local lcControlSource
DO CASE
  CASE type("tuParameter.BaseClass") = "C" AND type("tuParameter.ControlSource") = "C"
    lcControlSource = X3CSourc(tuParameter)
  CASE type("tuParameter") = "C" AND !empty(tuParameter) 
    lcControlSource = upper(alltrim(tuParameter))
  CASE pcount() = 0 
    IF empty(alias())
      lcControlSource = space(0)
     ELSE
      lcControlSource = alias()
    ENDIF
  OTHERWISE
    lcControlSource = space(0) 
ENDCASE 
IF empty(lcControlSource)
  return space(0)
ENDIF
*
*  parse out just the alias from  
*    Alias 
*    Alias+Field
*    Database+Alias+Field
*
local lcAlias
lcAlias = lcControlSource
IF "!" $ lcControlSource
  lcAlias = substrc(lcControlSource,at_c("!",lcControlSource)+1)
ENDIF
IF "." $ lcAlias
  lcAlias = leftc(lcAlias,at_c(".",lcAlias)-1)
ENDIF  
IF empty(lcAlias)
  return space(0)
ENDIF
IF type("lcAlias.BaseClass") = "C" OR "." $ lcAlias
  *
  *  ControlSource is in the form Object.Property
  *
  return space(0)
ENDIF
local lcRetVal 
*
*  read-only CURSORGETPROP() properties
*                  SourceType    SourceName     Database     SQL            Offline   
*  =================================================================================
*  Local View          1         View Name      .DBC Name    SQL statement    .f.
*  Local Offline View  1         View Name      .DBC Name    error            .t.
*  Remote View         2         View Name      .DBC Name    SQL statement    .f.
*  Remote Offline View 2         View Name      .DBC Name    error            .t.
*  SPT Cursor          2         space(0)       space(0)     space(0)         .f.
*  Local Cont Table    3         TableName      .DBC Name    error            .f.
*  Local Free Table    3         .DBF filename  space(0)     error            .f.
*  CREATE CURSOR       3         .TMP filename  space(0)     error            .f.
*  SQL-SELECT          3         .TMP filename  .DBC Name    error            .f.
*                                or Table Name  or space(0)
*
DO CASE
  ****************************************************************
  CASE NOT USED(lcAlias)
  ****************************************************************
    lcRetVal = SPACE(0)
  ****************************************************************
  CASE inlist(CURSORGETPROP("SourceType",lcAlias),3,103,203) ;
       AND NOT EMPTY(cursorgetprop("Database",lcAlias)) ;
       AND OCCURS("\",cursorgetprop("SourceName",lcAlias)) = 0 ;
       AND OCCURS(".",cursorgetprop("SourceName",lcAlias)) = 0 
  ****************************************************************
    *
    *  Local contained table
    *  OR
    *  SQL-Select cursor when the result of the 
    *    SELECT is a filtered cursor of a single
    *    contained table
    *
    lcRetVal = "TC"
  ****************************************************************
  CASE inlist(CURSORGETPROP("SourceType",lcAlias),3,103,203) ;
       AND EMPTY(CURSORGETPROP("Database",lcAlias)) ;
       AND RIGHTC(CURSORGETPROP("SourceName",lcAlias),4) = ".DBF"
  ****************************************************************
    *
    *  Local free/2x table
    *
    lcRetVal = "TF"
  ****************************************************************
  CASE CURSORGETPROP("SourceType",lcAlias) = 1
  ****************************************************************
    *
    *  Local view
    *
    IF CURSORGETPROP("Offline",lcAlias) 
      lcRetVal = "OVL"
     ELSE
      lcRetVal = "VL"
    ENDIF
  ****************************************************************
  CASE CURSORGETPROP("SourceType",lcAlias) = 2 ;
       AND EMPTY(CURSORGETPROP("SourceName",lcAlias)) ;
       AND EMPTY(CURSORGETPROP("Database",lcAlias)) ;
       AND EMPTY(CURSORGETPROP("SQL",lcAlias)) 
  ****************************************************************
    *
    *  SQL Pass-Thru cursor
    *
    lcRetVal = "SPT"
  ****************************************************************
  CASE CURSORGETPROP("SourceType",lcAlias) = 2
  ****************************************************************
    *
    *  Remote view
    *
    IF CURSORGETPROP("Offline",lcAlias)
      lcRetVal = "OVR"
     ELSE
      lcRetVal = "VR"
    ENDIF
  ****************************************************************
  CASE inlist(CURSORGETPROP("SourceType",lcAlias),101,201)	&& dy 6/17/15
  ****************************************************************
    *
    *  local cursoradapter
    *
    lcRetVal = "VLA"
  ****************************************************************
  CASE inlist(CURSORGETPROP("SourceType",lcAlias),102,202)
  ****************************************************************
    *
    *  remote cursoradapter
    *
    lcRetVal = "VRA"
  ****************************************************************
  OTHERWISE
  ****************************************************************
    *
    *  CREATE CURSOR cursor or SELECT-SQL cursor
    *
    lcRetVal = "C"
ENDCASE
RETURN lcRetVal
