*
*  XXSTDBY.PRG
*  Wrapper program for XXFWFRM.VCX/frmStandBy to present 
*  a "Please wait..." form message.
*    MODIFY CLASS frmStandBy OF XXFWFRM.VCX
*
*  Copyright (c) 1997-2005 Visionpace   All Rights Reserved
*                17501 East 40 Hwy., Suite 218
*                Independence, MO 64055
*                816-350-7900 
*                http://www.visionpace.com
*                http://vmpdiscussion.visionpace.com
*  Author:  Drew Speedie
*
*  Usage:
*    LOCAL loPleaseWait
*    *  apps using Steven Black's INTL Toolkit:
*    loPleaseWait = XXSTDBY(X3I("This is my message to you!"))
*    *  apps not using Steven Black's INTL Toolkit:
*    loPleaseWait = XXSTDBY("This is my message to you!")
*    *
*    * processing goes here
*    *
*    RELEASE loPleaseWait
*     -OR-
*    loPleaseWait.Release()  &&& or, if this is the last
*                            &&& line of code, there's 
*                            &&& nothing needed because
*                            &&& loPleaseWait is destroyed as
*                            &&& it goes out of scope
*
*  EXAMPLES
*  XXFW.VCX/ctrApp.ValidateDatabase()
*    XXDTES("XXFW.VCX","XXSTDBY","ctrApp","ValidateDatabase")
*  XXFWFRM.VCX/frmEventLog.Load()
*    XXDTES("XXFWFRM.VCX","XXSTDBY","frmEventLog","Load")
*  XXFWFRM.VCX/frmEventLog.cmdClipboard.Click()  
*    XXDTES("XXFWFRM.VCX","XXSTDBY","frmEventLog","cmdClipBoard.Click")
*  XXFWLIBS.VCX/cusDBCSvc.PackTable()
*    XXDTES("XXFWLIBS.VCX","XXSTDBY(X3I(","cusDBCSvc","PackTable")
*  VMDECUSA.SCX/pgfPageRefresh1.Page1.ctrFilter.cmdApplyFilter.Click()
*    XXDTES("VMDECUSA.SCX","XXSTDBY(","Form","pgfPageRefresh1.Page1.ctrFilter.cmdApplyFilter.Click")
*  VMDECUSH.SCX/pgfPageRefresh1.Page1.cmdApplyFilter.Click()
*    XXDTES("VMDECUSH.SCX","XXSTDBY(","Form","pgfPageRefresh1.Page1.cmdApplyFilter.Click")
*
*
*  lParameters
*      tcMessage (O) Message to be presented in the form
*                    Defaults to "Please wait..."
*     tcIconFile (O) Filename of an icon file to be displayed
*                      at the left side of the standby form
*                    In XXFWFRM.VCX/frmStandby, defaults to
*                      any specified oApp.GetAppInfo("IconFile")
*  tcClassAndVCX (O) [ClassName,VCX] containing the
*                      standby form
*                    Defaults to "frmStandby,XXFWFRM.VCX"
*
LPARAMETERS tcMessage, tcIconFile, tcClassAndVCX

LOCAL lcClassAndVCX, lcClassName, lcIconFile
IF VARTYPE(m.tcClassAndVCX) = "C" AND NOT EMPTY(m.tcClassAndVCX)
  lcClassAndVCX = UPPER(ALLTRIM(m.tcClassAndVCX))
 ELSE
  lcClassAndVCX = "frmStandby,XXFWFRM.VCX"
ENDIF
lcClassName = X8SETCPR(m.lcClassAndVCX)
IF ISNULL(m.lcClassName)
  * unable to SET CLASSLIB TO/SET PROCEDURE TO
  RETURN .f.
ENDIF
IF VARTYPE(m.tcIconFile) = "C" AND NOT EMPTY(m.tcIconFile)
  lcIconFile = UPPER(ALLTRIM(m.tcIconFile))
 ELSE
  lcIconFile = SPACE(0)
ENDIF     
LOCAL loStandBy
loStandby = CREATEOBJECT(m.lcClassName,m.tcMessage,m.lcIconFile)
IF VARTYPE(loStandby) = "O"
  loStandby.Show()
ENDIF

IF FILE("X7RedrawWindow.PRG") OR FILE("X7RedrawWindow.FXP")
  *
  *  frequently, VFP doesn't redraw the contents of
  *  this form immediately on Show-ing it, especially
  *  when your next action after calling this routine
  *  is to perform some processor-intensive action --
  *  this code forces VFP to repaint/redraw the
  *  loStandBy form
  *
  LOCAL lcString
  lcString = "X7RedrawWindow(loStandby)"
  &lcString
ENDIF

RETURN m.loStandby
