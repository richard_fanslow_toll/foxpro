*
*  XXDTMEST.PRG
*  Fire up an instance of XXTOOLS.VCX/frmXXDTMEST to 
*  update existing an app-specific MSGSVC.DBF/STRINGS.DBF 
*  with whatever is in the current VMP-supplied 
*  MSGSVC.DBF and STRINGS.DBF.
*
*  This is especially useful when upgrading to
*  a new version of VMP, which likely contains 
*  new/additional MSGSVC/STRINGS records.  When
*  upgrading an existing application to a new
*  version of VMP, simply run this utility against
*  each existing app-specific MSGSVC/STRINGS table.
*  
*  Copyright (c) 1997-2005 Visionpace   All Rights Reserved
*                17501 East 40 Hwy., Suite 218
*                Independence, MO 64055
*                816-350-7900 
*                http://www.visionpace.com
*                http://vmpdiscussion.visionpace.com
*  Author:  Drew Speedie  
*
USE IN SELECT("MSGSVC")
USE IN SELECT("STRINGS")
SET CLASSLIB TO XXTOOLS.VCX ADDITIVE 
PUBLIC xxdtmest
*
*  MODI CLASS frmXXDTMEST OF XXTOOLS
*
xxdtmest = CREATEOBJECT("frmXXDTMEST")
xxdtmest.Show()
RETURN 




PROCEDURE frmGetTable_MSGSVCTest
*
*  toInfo parameter passed here from frmGetTable::OKAction()
*    XXDTES("XXTOOLS.VCX","IF NOT EMPTY(THIS.ioParameter.icOKActionFunction)","frmGetTable","OKAction")
*
LPARAMETERS toInfo
LOCAL lcTable, lcConnectString, lcOnError, llError, llOK, ;
      lnHandle, lnDispLogin, loPleaseWait
lcOnError = ON("ERROR")
llError = .f.
llOK = .t.
IF EMPTY(toInfo.icFreeTableFileName)
  *
  *  remote MSGSVC table
  *
  lcTable = toInfo.icTableName
  lcConnectString = toInfo.icConnectString
  lnDispLogin = SQLGETPROP(0,"DispLogin")
  SQLSETPROP(0,"DispLogin",3)
  loPleaseWait = XXSTDBY("Opening remote MSGSVC table...")
  lnHandle = SQLSTRINGCONNECT(lcConnectString)
  SQLSETPROP(0,"DispLogin",lnDispLogin)
  loPleaseWait = .NULL.
  IF lnHandle < 0
    llOK = .f.
    MESSAGEBOX("Unable to open " + lcTable,16,"Please Note")
   ELSE
    llOK = SQLEXEC(lnHandle,"select * from " + lcTable + " WHERE 0=1","MSGSVCTest")>0
    IF NOT llOK
      MESSAGEBOX("Unable to open " + lcTable,16,"Please Note")
     ELSE 
      IF NOT X6ISFLD("MSGSVCTest.cKey") ;
           OR NOT X6ISFLD("MSGSVCTest.cOriginal") ;
           OR NOT X6ISFLD("MSGSVCTest.cFunction") 
        MESSAGEBOX(lcTable + " does not have the MSGSVC structure.",16,"Please Note")
        llOK = .f.
      ENDIF
    ENDIF    
  ENDIF
  SQLDISCONNECT(lnHandle)
 ELSE
  *
  *  free MSGSVC table
  *
  IF EMPTY(toInfo.icFreeTablePath)
    lcTable = FULLPATH(toInfo.FreeTableFileName)
   ELSE
    lcTable = ADDBS(toInfo.icFreeTablePath) + toInfo.icFreeTableFileName
  ENDIF        
  lcConnectString = SPACE(0)
  ON ERROR llError = .t.
  USE (lcTable) IN 0 SHARED NOUPDATE ALIAS MSGSVCTest
  ON ERROR &lcOnError
  IF llError
    MESSAGEBOX("Unable to open " + lcTable,16,"Please Note")
    llOK = .f.
   ELSE
    IF NOT X6ISFLD("MSGSVCTest.cKey") ;
         OR NOT X6ISFLD("MSGSVCTest.cOriginal") ;
         OR NOT X6ISFLD("MSGSVCTest.cFunction") 
      MESSAGEBOX(lcTable + " does not have the MSGSVC structure.",16,"Please Note")
      llOK = .f.
    ENDIF    
  ENDIF
ENDIF
USE IN SELECT("MSGSVCTest")
RETURN llOK



PROCEDURE frmGetTable_STRINGSTest
*
*  toInfo parameter passed here from frmGetTable::OKAction()
*    XXDTES("XXTOOLS.VCX","IF NOT EMPTY(THIS.ioParameter.icOKActionFunction)","frmGetTable","OKAction")
*
LPARAMETERS toInfo
LOCAL lcTable, lcConnectString, lcOnError, llError, llOK, ;
      lnHandle, lnDispLogin, loPleaseWait
lcOnError = ON("ERROR")
llError = .f.
llOK = .t.
IF EMPTY(toInfo.icFreeTableFileName)
  *
  *  remote STRINGS table
  *
  lcTable = toInfo.icTableName
  lcConnectString = toInfo.icConnectString
  lnDispLogin = SQLGETPROP(0,"DispLogin")
  SQLSETPROP(0,"DispLogin",3)
  loPleaseWait = XXSTDBY("Opening remote STRINGS table...")
  lnHandle = SQLSTRINGCONNECT(lcConnectString)
  SQLSETPROP(0,"DispLogin",lnDispLogin)
  loPleaseWait = .NULL.
  IF lnHandle < 0
    llOK = .f.
    MESSAGEBOX("Unable to open " + lcTable,16,"Please Note")
   ELSE
    llOK = SQLEXEC(lnHandle,"select * from " + lcTable + " WHERE 0=1","STRINGSTest")>0
    IF NOT llOK
      MESSAGEBOX("Unable to open " + lcTable,16,"Please Note")
     ELSE 
      IF NOT X6ISFLD("STRINGSTest.cOriginal") ;
           OR X6ISFLD("STRINGSTest.cKey") 
        MESSAGEBOX(lcTable + " does not have the STRINGS structure.",16,"Please Note")
        llOK = .f.
      ENDIF
    ENDIF    
  ENDIF
  SQLDISCONNECT(lnHandle)
 ELSE
  *
  *  free MSGSVC table
  *
  IF EMPTY(toInfo.icFreeTablePath)
    lcTable = FULLPATH(toInfo.FreeTableFileName)
   ELSE
    lcTable = ADDBS(toInfo.icFreeTablePath) + toInfo.icFreeTableFileName
  ENDIF        
  lcConnectString = SPACE(0)
  ON ERROR llError = .t.
  USE (lcTable) IN 0 SHARED NOUPDATE ALIAS STRINGSTest
  ON ERROR &lcOnError
  IF llError
    MESSAGEBOX("Unable to open " + lcTable,16,"Please Note")
    llOK = .f.
   ELSE
    IF NOT X6ISFLD("STRINGSTest.cOriginal") ;
         OR X6ISFLD("STRINGSTest.cKey")
      MESSAGEBOX(lcTable + " does not have the STRINGS structure.",16,"Please Note")
      llOK = .f.
    ENDIF    
  ENDIF
ENDIF
USE IN SELECT("STRINGSTest")
RETURN llOK
