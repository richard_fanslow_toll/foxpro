* 
*  X8IsAutoInc.PRG
*  RETURNs a logical value indicating whether the
*  passed field is an AutoInc field.
*
*  Copyright (c) 2004-2005 Visionpace   All Rights Reserved
*                17501 East 40 Hwy., Suite 218
*                Independence, MO 64055
*                816-350-7900 
*                http://www.visionpace.com
*                http://vmpdiscussion.visionpace.com
*  Author:  Drew Speedie  
*
*  lParameters
*  tcFieldName (R)  FieldName or Alias.FieldName
*                     to be checked to see if it
*                     is an AutoInc field.
*      tcAlias (O)  If tcFieldName is passed as
*                     Alias.FieldName, this parameter
*                     is ignored.
*                   If tcFieldName is passed as 
*                     just a FieldName, this parameter
*                     is REQUIRED, specifying the ALIAS()
*                     whose tcFieldName is to be
*                     checked to see if it is an AutoInc
*                     field.
*
*  If tcFieldName is in a REMOTE VIEW, this routine
*    RETURNS .F.
*  If tcFieldName is in a LOCAL VIEW, this routine 
*    RETURNS a logical value indicating whether tcFieldName
*    in its base table is an AutoInc field.
*
*  The Alias specified in tcFieldName/tcAlias must be USED(),
*  if the Alias is a local view, its base table must
*  also be USED().
*
LPARAMETERS tcFieldName, tcAlias

IF VERSION(5) < 800
  RETURN .f.
ENDIF

IF NOT VARTYPE(m.tcFieldName) = "C" OR EMPTY(m.tcFieldName) 
  ASSERT .f. MESSAGE "The tcFieldName parameter is required"
  RETURN .NULL.
ENDIF

LOCAL lcAlias, lcFieldName
lcFieldName = UPPER(ALLTRIM(m.tcFieldName))
IF OCCURS(".",m.lcFieldName) = 1
  lcAlias = JUSTSTEM(m.lcFieldName)
  lcFieldName = JUSTEXT(m.lcFieldName)
 ELSE
  IF VARTYPE(m.tcAlias) = "C" AND NOT EMPTY(m.tcAlias) 
    lcAlias = UPPER(ALLTRIM(m.tcAlias))
   ELSE
    ASSERT .f. MESSAGE "tcAlias is required when tcFieldName does not include the Alias"
    RETURN .NULL.
  ENDIF
ENDIF       

IF inlist(CURSORGETPROP("SourceType",m.lcAlias),2,102,202)
  *
  *  remote view
  *
  RETURN .f.
ENDIF

LOCAL laFields[1], xx, llAutoInc, llView, lnSelect
llAutoInc = .f.

lnSelect = SELECT(0)
SELECT (m.lcAlias)

llView = inlist(CURSORGETPROP("SourceType",m.lcAlias),1,101,201)
IF llView
  lcAlias = X8BTABLE(m.lcAlias+"."+m.lcFieldName, ;
                     CURSORGETPROP("Database",m.lcAlias))
  IF EMPTY(m.lcAlias)
    *
    *  there are a number of scenarios that cause
    *  X8BTABLE() to RETURN a blank space
    *
    SELECT (m.lnSelect)
    RETURN .f.
  ENDIF
  SELECT (m.lcAlias)
ENDIF

AFIELDS(m.laFields)
xx = ASCAN(m.laFields,m.lcFieldName,1,-1,1,15)
IF xx > 0
  llAutoInc = laFields[m.xx,17] > 0
ENDIF

SELECT (m.lnSelect)

RETURN m.llAutoInc 
