*
*  XXDTExplorer.PRG
*  DeveloperTool:  Fire up a new instance of the Windows
*  Explorer, with the **current** directory/folder loaded
*
*  Copyright (c) 1998-2005 Visionpace   All Rights Reserved
*                17501 East 40 Hwy., Suite 218
*                Independence, MO 64055
*                816-350-7900 
*                http://www.visionpace.com
*                http://vmpdiscussion.visionpace.com
*  Author:  Drew Speedie
*           Special thanks to Art Bergquist 
*  Inspired by the "Add Menu Bar to Run Explorer" tip 
*  by Mike Yearwood and Jay Burns in FoxPro Advisor, 
*  January 2000 (p. 46a)
* 
asse .f.
LOCAL lcCurDir
lcCurDir = FULLPATH(CURDIR())

DO CASE
  CASE "WINDOWS 5" $ UPPER(OS())							&& Win2K and above
    RUN /N Explorer.EXE /e, , , , &lcCurDir.

  CASE "Windows NT" $ OS()									&& WinNT
    lcCurDir = m.lcCurDir + "*.*"
    LOCAL lcExplorerEXE
    lcExplorerEXE = GETENV("WinDir") + "\Explorer.EXE"
    RUN /N &lcExplorerEXE. /e, , , /Select, &lcCurDir.

  OTHERWISE                                                 &&& Win98
    lcCurDir = lcCurDir + "*.*"
    RUN /N Explorer.EXE /e, , , /Select, &lcCurDir.
ENDCASE
