*
*  X3GETRT.PRG
*  Returns the RuleText for the passed control,
*  based on its ControlSource:
*    Contained table field:  RuleText
*         Local view field:  RuleText (or, if no RuleText, any RuleText for the corresponding base table field)
*        Remote view field:  RuleText
*      Free/2x table field:  space(0)
*     No ControlSource set:  space(0)
*          Object.Property:  space(0)
*            Anything else:  space(0)
*
*  Copyright (c) 1997-2005 Visionpace   All Rights Reserved
*                17501 East 40 Hwy., Suite 218
*                Independence, MO 64055
*                816-350-7900 
*                http://www.visionpace.com
*                http://vmpdiscussion.visionpace.com
*  Author:  Drew Speedie
*
*
*  Usage:
*    * in method code:
*    lcRuleText = x3getrt(THIS) 
*
*  lParameters
*     toControl (R) Object reference to the control whose
*                   ControlSource RuleText is desired
*
*
lparameters toControl

local lcControlSource, lcRuleText
lcRuleText      = SPACE(0)
lcControlSource = X3CSOURC(m.toControl)

IF EMPTY(m.lcControlSource)
  *
  *  no ControlSource has been set for toControl
  *
  RETURN m.lcRuleText
ENDIF

IF OCCURS(".",m.lcControlSource) = 0 OR OCCURS(".",m.lcControlSource) > 1
  *
  *  ControlSource appears to be either a (perish the thought!)
  *  public/private memvar
  *    OR
  *  ControlSource appears to be an Object.Object.Property
  *
  RETURN m.lcRuleText
ENDIF

*
*  at this point, lcControlSource contains a single
*  dot/".", now check to see if it's an Object.Property
*
LOCAL lcAlias, lcDatabase
lcAlias = LEFTC(m.lcControlSource,AT_C(".",m.lcControlSource)-1)
IF TYPE("EVALUATE(m.lcAlias)") = "O"
  *
  *  the value to the left of the dot/"." is an
  *  object reference, not an Alias
  *
  RETURN lcRuleText
ENDIF

lcDatabase = CURSORGETPROP("DATABASE",m.lcAlias)
IF EMPTY(m.lcDatabase)
  *
  *  free VFP table or Fox2x table
  *
  RETURN m.lcRuleText
ENDIF

*
*  lcAlias is either a view or a contained table
*
local lcSetDatabase, lnSelect
lcSetDatabase = SET("DATABASE")
SET DATABASE TO (m.lcDatabase)
DO CASE
  ***************************************************
  CASE inlist(CURSORGETPROP("SourceType",m.lcAlias),3,103,203)
  ***************************************************
    *
    *  lcControlSource is a contained table 
    *
    lcRuleText = DBGETPROP(m.lcControlSource,"FIELD","RULETEXT")
  ***************************************************
  CASE inlist(CURSORGETPROP("SourceType",m.lcAlias),1,102,201)
  ***************************************************
    *
    *  lcControlSource is a local view 
    *
    lcRuleText = DBGETPROP(m.lcControlSource,"FIELD","RULETEXT")
    IF EMPTY(m.lcRuleText)
      *
      *  try the corresponding base table field
      *  
      LOCAL lcBaseTableName
      lcBaseTableName = X3BTABLE(m.lcControlSource) 
      lcControlSource = STRTRAN(m.lcControlSource,m.lcAlias+".",m.lcBaseTableName+".")
      *
      *  we expect that the ultimate base table for this
      *  view is open in this data session
      *
      SET DATABASE TO (CURSORGETPROP("Database",m.lcBaseTableName))
      lcRuleText = DBGETPROP(m.lcControlSource,"FIELD","RULETEXT")
    ENDIF
  ***************************************************
  CASE inlist(CURSORGETPROP("SourceType",m.lcAlias),2,102,202)
  ***************************************************
    *
    *  lcControlSource is a remote view 
    *
    lcBaseTableName = CURSORGETPROP("SourceName")
    lcControlSource = STRTRAN(m.lcControlSource,m.lcAlias+".",m.lcBaseTableName+".")
    lcRuleText = DBGETPROP(m.lcControlSource,"FIELD","RULETEXT")
ENDCASE
lnSelect = SELECT(0)
*
*  select (lcAlias) to help ensure x3rultxt() success
*
SELECT (m.lcAlias)
lcRuleText = X3RULTXT(m.lcRuleText)

SELECT (m.lnSelect)

IF EMPTY(m.lcSetDatabase)
  SET DATABASE TO 
 ELSE
    SET DATABASE TO (m.lcSetDatabase)
ENDIF

RETURN m.lcRuleText
