*
*  X8SETCPR.PRG
*  Take a passed [ClassName,Location] string and:
*    1- if Location is a .PRG, attempt to SET PROCEDURE TO it ADDITIVE
*    2- if Location is a .VCX, attempt to SET CLASSLIB TO it ADDITIVE
*    3- if the above fails, RETURN .NULL. (character data type)
*    4- if #1/#2 succeed or if Location is not passed, RETURN the 
*       ClassName portion of the string
*
*  Use this routine instead of X3SETCPR.PRG, which is
*  included for backward compatibility of existing
*  VMP application code.
*
*  Copyright (c) 2004-2005 Visionpace   All Rights Reserved
*                17501 East 40 Hwy., Suite 218
*                Independence, MO 64055
*                816-350-7900 
*                http://www.visionpace.com
*                http://vmpdiscussion.visionpace.com
*  Author:  Drew Speedie
*
*  Usage:
*    LOCAL lcClassName
*    lcClassName = X8SETCPR("MyClass,MYPROC.PRG")
*    IF ISNULL(lcClassName)
*      * something went wrong -- unable to
*      * SET PROCEDURE TO 
*     ELSE
*      CREATEOBJECT(lcClassName)
*    ENDIF
*    
*    LOCAL lcClassName
*    lcClassName = X8SETCPR("MyClass,MYCLASS.VCX")
*    IF ISNULL(lcClassName)
*      * something went wrong -- unable to SET CLASSLIB TO
*     ELSE
*      CREATEOBJECT(lcClassName)
*    ENDIF
*
*    LOCAL lcClassName
*    lcClassName = X8SETCPR("MyClass,MYCLASS")
*    IF ISNULL(lcClassName)
*      * something went wrong -- unable to SET CLASSLIB TO
*      * (.VCX extension is assumed)
*     ELSE
*      CREATEOBJECT(lcClassName)
*    ENDIF
*
*  lParameters
*     tcClass (R) Name of a class definition plus optional
*                 comma-separated class library file name
*                 or procedure file name.  Note that this
*                 is a single parameter and that the class
*                 location is optional.  If the class location
*                 is passed and has no extension, the extension
*                 is assumed to be ".VCX" and a SET CLASSLIB TO
*                 is attempted
* tcInAppEXEFile (O) Name of an .APP/.EXE file that contains 
*                 tcClass, maps to the IN AppFileName supported 
*                 by SET CLASSLIB TO
*                  
LPARAMETERS tcClass, tcInAppEXEFile

LOCAL lcReturn
lcReturn = SPACE(0)
lcReturn = .NULL.

IF NOT VARTYPE(m.tcClass) = "C" OR EMPTY(m.tcClass)
  RETURN m.lcReturn
ENDIF

IF OCCURS(",",m.tcClass) > 0
  LOCAL lcProcClassLib
  lcReturn = GETWORDNUM(m.tcClass,1,",")
  lcProcClassLib = UPPER(GETWORDNUM(m.tcClass,2,","))
 ELSE
  RETURN m.tcClass
ENDIF
  
*
*  lcProcClasslib contains the name of the 
*  .PRG/.VCX containing lcClass/lcReturn
*
*  lcReturn contains the name of the Class to
*  be ultimately instantiated by the calling code
*
*  attempt to SET PROCEDURE TO/SET CLASSLIB TO
*  
  
IF UPPER(JUSTEXT(m.lcProcClassLib)) = "PRG" 
  IF NOT X8SETPRC(m.lcProcClasslib)
    lcReturn = .NULL.
  ENDIF
 ELSE
  *
  *  default to a .VCX
  *
  IF NOT X8SETCLS(m.lcProcClasslib,m.tcInAppEXEFile)
    lcReturn = .NULL.
  ENDIF
ENDIF

RETURN m.lcReturn
