*
*  X3CSOURC.PRG
*  Returns the "real" THIS.ControlSource -- for
*  controls in grid columns, the ControlSource 
*  property doesn't always include the Alias() prefix,
*  just the fieldname, if the ControlSource for
*  the column is set in method code without the
*  Alias
*
*  Copyright (c) 1997-2005 Visionpace   All Rights Reserved
*                17501 East 40 Hwy., Suite 218
*                Independence, MO 64055
*                816-350-7900 
*                http://www.visionpace.com
*                http://vmpdiscussion.visionpace.com
*  Author:  Drew Speedie
*
*
*  Usage:
*    * from a method for the control:
*    lcControlSource = x3CSourc(THIS)
*
*  lParameters
*    toControl (R) Object reference to the control in question
*
LPARAMETERS toControl
LOCAL lcControlSource
DO CASE
  ************************************************
  CASE TYPE("toControl.ControlSource") = "U"
  ************************************************
    *
    *  the passed control has no ControlSource
    *  property
    *
    lcControlSource = SPACE(0)
  ************************************************
  CASE TYPE("toControl.ControlSource") = "C" ;
       AND "." $ toControl.ControlSource
  ************************************************
    *
    *  normal population, including grid columns
    *  whose ControlSource has been set explicitly,
    *  outside the Properties Sheet
    *
    lcControlSource = UPPER(ALLTRIM(toControl.ControlSource))
  ************************************************
  CASE UPPER(ALLTRIM(toControl.PARENT.BaseClass)) = "COLUMN"
  ************************************************
    *
    *  In VFP3.0b, Grid.Column.ControlSource is
    *  FieldName only, not Alias.FieldName
    *
    *  In later VFP versions, you can still get
    *  FieldName only in some cases where you
    *  set the Column.ControlSource to just
    *  a FieldName in method code
    *
    *  NOTE:  this code assumes that the alias 
    *         for the fieldname that is the
    *         Column.ControlSource is the same
    *         as the Grid.RecordSource.  Since 
    *         that is not necessarily the case
    *         (the ControlSource may be a field
    *         from a related alias), be sure that
    *         if a column is bound to a field of
    *         a related alias you populate the
    *         Column.ControlSource in method code
    *         so that it includes the alias, not
    *         just the VFP default fieldname
    *
    lcControlSource = UPPER(ALLTRIM( ;
         toControl.PARENT.PARENT.RecordSource + "." + toControl.ControlSource))
  ************************************************
  OTHERWISE
  ************************************************
    lcControlSource = SPACE(0)
ENDCASE
RETURN m.lcControlSource
