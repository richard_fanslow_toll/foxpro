*
*  X3CPEDIG.PRG
*  Answers the question:  
*  Does the passed toObject have the passed tcClassName 
*    in its pedigree?  
*  OR 
*  Is tcClassName somewhere in the ancestry of toObject?
*
*  If either (required) parameter is not passed as an
*  object reference, RETURNs .F.
*  
*
*  This routine is included for backward compatibility
*  and is not called anywhere in the current VMP
*  framework -- use X7CPEDIG.PRG instead.
*
*
*  Copyright (c) 1997-2005 Visionpace   All Rights Reserved
*                17501 East 40 Hwy., Suite 218
*                Independence, MO 64055
*                816-350-7900 
*                http://www.visionpace.com
*                http://vmpdiscussion.visionpace.com
*  Author:  Drew Speedie
*
*
*  lParameters
*       toObject (R) Object reference to the object whose 
*                      pedigree is to be checked
*    tcClassName (R) Check the ACLASS() pedigree of toObject 
*                      for this class name
*
*  Usage:
*
*  * from form method code:
*  IF X3CPEDIG(THIS,"FRMDATA")
*    * THISFORM is a form from the XXFW.VCX/frmData class hierarchy
*   ELSE
*    * THISFORM does not inherit from XXFW.VCX/frmData
*  ENDIF
*
*  * from textbox method code:
*  IF X3CPEDIG(THIS,"txtPicklistValid")
*    * THIS textbox is from the XXFWCTRL.VCX/txtPicklistValid class hierarchy
*   ELSE
*    * THIS textbox does not inherit from XXFWCTRL.VCX/txtPicklistValid
*  ENDIF
*
*  
*  In general, this routine is faster than X2PSTACK() --
*  when either one will work in a particular situation,
*  use this routine instead of X2PSTACK().
*
*
LPARAMETERS toObject, tcClassName

IF TYPE("toObject.BaseClass") # "C" 
  RETURN .f.
ENDIF

LOCAL lcClassName

IF TYPE("tcClassName") = "C" AND !EMPTY(tcClassName)
  lcClassName = UPPER(ALLTRIM(tcClassName))
 ELSE
  RETURN .f.
ENDIF

*
*  the following loop thru the laClass array is
*  faster than a single call to ASCAN(), because
*  there has to be a SET EXACT ON before the
*  ASCAN() and a SET EXACT OFF after the ASCAN() --
*  issuing SET EXACT is relatively slow
*
LOCAL laClass[1], llRetVal, xx
IF ACLASS(laClass,toObject) > 0
  FOR xx = 1 TO ALEN(laClass,1)
    IF UPPER(laClass[xx]) == UPPER(lcClassName)
      llRetVal = .t.
      EXIT 
    ENDIF
  ENDFOR
ENDIF

RETURN llRetVal
