*
*  XXBUILDER.PRG
*  Calling program for VMP custom builders
*
*  Copyright (c) 1997-2005 Visionpace   All Rights Reserved
*                17501 East 40 Hwy., Suite 218
*                Independence, MO 64055
*                816-350-7900 
*                http://www.visionpace.com
*                http://vmpdiscussion.visionpace.com
*  Author:  Drew Speedie
*
*  
*  NOTE: to invoke this builder program, _BUILDER
*        must be set to this .PRG, either in your
*        CONFIG.FPW or at the Command Window
*  
*  If you have your own custom builder(s) that you want
*  to be able to call instead of or in addition to the
*  VMP ones implemented here, just create a file named
*  VMPBuilderHook.PRG.  If VMPBuilderHook.PRG exists in
*  the current VFP path, it is called fairly early on
*  in this program.  If your VMPBuilderHook processes
*  the Builder event, RETURN .T., whereupon this program 
*  does not proceed any further.  If the Builder event
*  is for object(s) that your VMPBuilderHook does
*  NOT process, RETURN .F., whereupon this program 
*  proceeds on with the VMP custom Builder behavior.
*  Something like this:
*    *  VMPBuilderHook.PRG
*    lParameters toObject, tcSource, taSelectedObjects
*    IF <taSelectedObjects> indicates a Builder scenario
*       for which you have a custom Builder>
*      *  call your custom builder, and RETURN .T.
*      *  to indicate to XXBUILDER.PRG that the 
*      *  Builder event has already been handled
*      RETURN .T.
*     ELSE
*      *  send control back to XXBUILDER.PRG, which 
*      *  continues to process the Builder event
*      RETURN .F.
*    ENDIF
*    
*
*  lParameters
*    toObject  currently-selected control/object (or, if multiple 
*              controls/objects are selected, toObject will be the
*              last one selected)
*    tcSource  location from which Builder was invoked
*              "PSHEET"     Properties sheet
*              "RTMOUSE"    Rightclick menu when object is selected
*              "TOOLBOX"    As result of Builder Lock
*              "QFORM"      Form Builder button pressed on Form Design toolbar
*              "RI"         From the Referential Integrity Builder 
*              "MEMBERDATA" From places like the "Favorites" shortcut menu selection of the Properties Sheet shorcut menu
*
LPARAMETERS toObject, tcSource, Three, Four   &&& LPARAMETERS Three and Four support VFP features added in VFP9 like the Favorites selection from the Properties Sheet/shortcut menu (tcSource = "MEMBERDATA")

IF UPPER(ALLTRIM(m.tcSource)) == "MEMBERDATA"
  DO (HOME()+"BUILDER.APP") WITH toObject, tcSource, Three, Four
  RETURN
ENDIF

SET ASSERTS ON 

IF (VARTYPE(toObject) # "O" OR VARTYPE(tcSource) # "C") ;
     AND NOT (VARTYPE(tcSource) = "C" AND UPPER(tcSource) == "RI")
  ASSERT .f. message ;
       "Builders are not supported from wherever you have invoked this builder code."  
  RETURN 
ENDIF

*
*  make sure the MessageBeep() Windows API function
*  is loaded to ring the bell rather than via ?? CHR(7), 
*  which generates a problem with commandbuttons having 
*  both Caption and Picture
*    XXDTES("XXFW.VCX","VFP ANOMALY WHEN BOTH Caption AND Picture ARE SET","cmdBase","zReadMe")
*
IF NOT X7ISAPIF("MessageBeep")
  DECLARE Long MessageBeep IN USER32.DLL Long uType 
ENDIF

#DEFINE MB_OK                   0       && OK button only
#DEFINE MB_OKCANCEL             1       && OK and Cancel buttons
#DEFINE MB_ABORTRETRYIGNORE     2       && Abort, Retry, and Ignore buttons
#DEFINE MB_YESNOCANCEL          3       && Yes, No, and Cancel buttons
#DEFINE MB_YESNO                4       && Yes and No buttons
#DEFINE MB_RETRYCANCEL          5       && Retry and Cancel buttons

#DEFINE MB_ICONSTOP             16      && Critical message
#DEFINE MB_ICONQUESTION         32      && Warning query
#DEFINE MB_ICONEXCLAMATION      48      && Warning message
#DEFINE MB_ICONINFORMATION      64      && Information message

#DEFINE MB_APPLMODAL            0       && Application Modal Message Box
#DEFINE MB_DEFBUTTON1           0       && First button is default
#DEFINE MB_DEFBUTTON2           256     && Second button is default
#DEFINE MB_DEFBUTTON3           512     && Third button is default
#DEFINE MB_SYSTEMMODAL          4096    && System Modal

#DEFINE IDOK            1       &&  OK button pressed
#DEFINE IDCANCEL        2       && Cancel button pressed
#DEFINE IDABORT         3       && Abort button pressed
#DEFINE IDRETRY         4       && Retry button pressed
#DEFINE IDIGNORE        5       && Ignore button pressed
#DEFINE IDYES           6       && Yes button pressed
#DEFINE IDNO            7       && No button pressed

LOCAL ARRAY laSelectedObjects[1]
laSelectedObjects[1] = ""

LOCAL lnSelObj, ;
      llSameBaseClass, ;
      lnCount, ;
      llNoBaseClass

lnSelObj = ASELOBJ(laSelectedObjects)

*!*	IF lnSelObj = 0 AND UPPER(toObject.BaseClass) = "CONTAINER"
*!*	  MessageBeep(0)
*!*	  MESSAGEBOX("Select this container and try again -- don't " + ;
*!*	             "select it AND RightClick/Edit...",48,"Please Note")
*!*	  RETURN
*!*	ENDIF

IF lnSelObj = 0
  *
  *  a class itself, like a ctrDataSource instance
  *  or a textbox is being modified --
  *  it is not possible to separately select that control
  *
  laSelectedObjects[1] = toObject
ENDIF

llSameBaseClass = .t.
llNoBaseClass = .f.
IF lnSelObj > 0
  FOR lnCount = 1 TO ALEN(laSelectedObjects,1)
    IF NOT llNoBaseClass AND TYPE("laSelectedObjects[lnCount].BaseClass") = "U"
      llNoBaseClass = .t.
      LOOP
    ENDIF
    IF laSelectedObjects[lnCount].BaseClass == toObject.BaseClass
     ELSE
      llSameBaseClass = .f.
      EXIT 
    ENDIF
  ENDFOR
ENDIF

IF llNoBaseClass
  MESSAGEBOX("You have selected one or more objects " + ;
             "whose BaseClass cannot be determined, " + ;
             "typically because they are members of " + ;
             "another class and have been Protected, " + ;
             "whereupon all their properties are " + ;
             "unavailable." + ;
             CHR(13) + CHR(13) + ;
             "Please select un-Protected objects.", ;
             48,"Please Note")
  RETURN 
ENDIF

IF UPPER(tcSource) == "RI" 
  *
  *  run the native Visual FoxPro RI builder 
  *
  DO (HOME()+"BUILDER.APP") WITH toObject, tcSource
  RETURN 
ENDIF

IF NOT llSameBaseClass
  *
  *  selected objects are of different base classes
  *
  MESSAGEBOX("You have selected multiple objects of different " + ;
             "BaseClass properties", ;
             16, ;
             "Please Note")
  RETURN
ENDIF

LOCAL llVMPBuilderHook
IF FILE("VMPBuilderHook.PRG") ;
     OR FILE("VMPBuilderHook.FXP")
  *
  *  call your procedural VMPBuilderHook.PRG, note
  *  that both 
  *    - the toObject passed by the intrinsic 
  *      VFP Builder action
  *    AND 
  *    - laSelectedObjects, the actual array of
  *      object references created here
  *  are passed to your VMPBuilderHook()
  *
  *  if your VMPBuilderHook() RETURNs .T., indicating
  *  that it has handled the Builder call, this
  *  .PRG RETURNs here 
  *
  *  if your VMPBuilderHook() RETURNs .F., indicating
  *  that it has NOT handled the Builder call, this
  *  .PRG continues on
  *
  llVMPBuilderHook = VMPBuilderHook(toObject,tcSource,@laSelectedObjects)
  IF llVMPBuilderHook
    RETURN 
  ENDIF
ENDIF     

DO CASE
  ****************************************************
  CASE UPPER(toObject.BaseClass) = "FORM"
  ****************************************************
    *
    *  run the native Visual FoxPro builder for forms
    *
    DO (HOME()+"BUILDER.APP") WITH toObject, tcSource
  ****************************************************
  CASE UPPER(toObject.BaseClass) = "LABEL" ;
       AND UPPER(toObject.Name) = "BUILDER_" ;
       AND OCCURS("_",toObject.Name) = 2
  ****************************************************
    *
    *  VMP convention for delayed-instantiation
    *  pageframe pages for which a label on the page 
    *  is named according to the convention
    *    BUILDER_<ClassName>_<VCXName>
    *  and thereby summons the specified class when the 
    *  label is selected and Builder... is chosen from 
    *  the shortcut menu
    *    See XXFWSEC.VCX/frmUserProperties, Page2
    *        VMDECU36.SCX, Page2 and Page3
    *
    LOCAL lcClassName, lcVCXName, lcName
    lcName = toObject.Name
    lcClassName = GETWORDNUM(lcName,2,"_") 
    lcVCXName = GETWORDNUM(lcName,3,"_") + ".VCX"
    IF FILE(lcVCXName)
      MODIFY CLASS (lcClassName) OF (lcVCXName) NOWAIT 
     ELSE
      MessageBeep(0)
      MESSAGEBOX(lcVCXName + " either does not exist " + ;
                 "or is not in the VFP path at the moment.", ;
                 16,"XXBUILDER.PRG")
    ENDIF
  ****************************************************
  CASE UPPER(toObject.BaseClass) = "CONTAINER" ;
       AND X7CPEDIG(toObject,"ctrDESubformMgr") 
  ****************************************************
    DO CASE
      CASE EMPTY(toObject.icSubform)
        MessageBeep(0)
        MESSAGEBOX(toObject.Name + ".icSubform property has not " + ;
                   "been filled in yet - XXBUILDER.PRG is unable " + ;
                   "to summon the subform in the appropriate Designer.", ;
                   16,"XXBUILDER.PRG")
      CASE ".SCX" $ UPPER(toObject.icSubform)
        IF FILE(toObject.icSubform)
          MODIFY FORM (toObject.icSubform) NOWAIT 
         ELSE
          MessageBeep(0)
          MESSAGEBOX(toObject.Name + [.icSubform = "] + toObject.icSubform + [" ] + ;
                     " but " + toObject.icSubform + " either does not exist " + ;
                     "or is not in the VFP path at the moment.",;
                     16,"XXBUILDER.PRG")
        ENDIF
      OTHERWISE  &&& .VCX-based icSubform
        LOCAL lcClassName, lcVCXName
        lcClassName = toObject.icSubform
        lcVCXName = substrc(lcClassName,at_c(",",lcClassName)+1)
        lcClassName = X8SETCPR(lcClassName)
        IF NOT ISNULL(lcClassName)
          LOCAL laClasses[1], xx, llFound
          AVCXCLASSES(laClasses,lcVCXName)
          FOR xx = 1 to alen(laClasses,1)
            IF upper(alltrim(laClasses[xx,1])) == upper(alltrim(lcClassName))
              llFound = .t.
              exit
            ENDIF
          ENDFOR
          IF llFound
            modi class (lcClassName) of (lcVCXName) nowait
           ELSE
            MessageBeep(0)
            messagebox(toObject.Name + [.icSubform = "] + toObject.icSubform + [" ] + ;
                       "but " + lcClassName + " does not exist in " + upper(lcVCXName) + ".", ;
                       16,"XXBUILDER.PRG")
          ENDIF
         ELSE
          MessageBeep(0)
          messagebox(toObject.Name + [.icSubform = "] + toObject.icSubform + [" ] + ;
                     " but " + upper(lcVCXName) + " either does not exist " + ;
                     "or is not in the VFP path at the moment.", ;
                     16,"XXBUILDER.PRG")
        ENDIF
    ENDCASE
  ****************************************************
  CASE UPPER(toObject.BaseClass) = "LABEL"
  ****************************************************
    *
    * run our custom builder for labels 
    *
    DO CustomBuilder WITH laSelectedObjects, "LABEL"
  ****************************************************
  CASE UPPER(ALLTRIM(SYS(0))) = "DSDELL8200 # DSPEEDIE" AND DIRECTORY("C:\DREW") ;
       AND UPPER(toObject.BaseClass) = "TEXTBOX" ;
       AND PEMSTATUS(toObject,"icSeekAliasExpressionForValue",5)
  ****************************************************
    DO CASE
      CASE UPPER(toObject.Name) == "TXTPICKLISTVALID" 
        MESSAGEBOX("You may not use the custom VMP txtPicklistValid Builder " + ;
                   "on the VMP txtPicklistValid class itself.", ;
                   48,"Please Note")
      CASE lnSelObj > 1
        MESSAGEBOX("The custom VMP txtPicklistValid Builder only " + ;
                   "supports selecting ONE txtPicklistValid instance " + ;
                   "at a time.", ;
                   48,"Please Note")
      OTHERWISE 
        *
        *  run our custom builder for textboxes, but only
        *  if toObject is not XXFWCTRL.VCX/txtPicklistValid 
        *  but inherits from XXFWCTRL.VCX/txtPicklistValid 
        *
        DO CustomBuilder WITH laSelectedObjects, "txtPicklistValid" 
    ENDCASE
  ****************************************************
  CASE UPPER(toObject.BaseClass) = "TEXTBOX" 
  ****************************************************
    IF INLIST(UPPER(toObject.Name),"TXTBASE","TXTPICKLISTVALID")
      MESSAGEBOX("You may not use the custom VMP Textbox Builder " + ;
                 "on the VMP txtBase or txtLabel classes.", ;
                 48,"Please Note")
     ELSE
      *
      *  run our custom builder for textboxes, but only
      *  if toObject is not XXFW.VCX/txtBase, or
      *  XXFWCTRL.VCX/txtPicklistValid,txtLabel
      *
      DO CustomBuilder WITH laSelectedObjects, "TEXTBOX" 
    ENDIF
  ****************************************************
  CASE upper(toObject.BaseClass) = "CONTAINER" ;
       AND x7cpedig(toObject,"ctrBusinessObjectManager") 
  ****************************************************
    do custombuilder with laSelectedObjects, "BusinessObjectManager"
  ****************************************************
  CASE upper(toObject.BaseClass) = "CONTAINER" ;
       AND x7cpedig(toObject,"ctrBusinessObject") 
  ****************************************************
    do custombuilder with laSelectedObjects, "BusinessObject"
  ****************************************************
  CASE upper(toObject.BaseClass) = "CONTAINER" ;
       AND UPPER(toObject.Class) == UPPER("ctrDataSource") 
  ****************************************************
    MESSAGEBOX("The VMP data source builder is only " + ;
               "available for subclasses of ctrDataSource, " + ;
               "not XXFWNTDS.VCX/ctrDataSource itself.", ;
               48,"Please Note")       
  ****************************************************
  CASE upper(toObject.BaseClass) = "CONTAINER" ;
       AND x7cpedig(toObject,"ctrDataSource") 
  ****************************************************
    do custombuilder with laSelectedObjects, "DataSource"
  ****************************************************
  CASE upper(toObject.BaseClass) = "LABEL" ;
       AND VARTYPE(toObject.PARENT) = "O" ;
       AND x7cpedig(toObject.PARENT,"ctrDataSource") 
  ****************************************************
    RELEASE laSelectedObjects
    DIMENSION laSelectedObjects[1]
    laSelectedObjects[1] = toObject.PARENT
    do custombuilder with laSelectedObjects, "DataSource"
  ****************************************************
  CASE upper(toObject.BaseClass) = "CUSTOM" ;
       AND x7cpedig(toObject,"cusDataSourceBehavior") 
  ****************************************************
    do custombuilder with laSelectedObjects, "DataSourceBehavior"
  ****************************************************
  CASE upper(toObject.BaseClass) = "CUSTOM" ;
       AND x7cpedig(toObject,"cusConnectionBehavior") 
  ****************************************************
    do custombuilder with laSelectedObjects, "ConnectionBehavior"
  ****************************************************
  OTHERWISE
  ****************************************************
    *
    *  pass anything else on through to the native
    *  Visual FoxPro builder 
    *
    DO (HOME()+"BUILDER.APP") WITH toObject, tcSource
ENDCASE    

RETURN 



PROCEDURE CustomBuilder
*
*  Custom Visual MaxFrame Professional builders
*  single or multiple controls selected
*
lparameters laSelectedObjects, lcType
local loFormRef, ;
      llOK, ;
      loBuilder, ;
      lcClassLib 
local array laFormName[1]
aselobj(laFormName,1)
loFormRef = laFormName[1]
IF upper(alltrim(loFormRef.BaseClass)) == "FORM"
 ELSE
  DO WHILE !upper(alltrim(loFormRef.BaseClass)) == "FORM"
    loFormRef = loFormRef.PARENT
  ENDDO
ENDIF
llOK = .t.
IF loFormRef.ScaleMode # 3 
  MessageBeep(0)
  IF messagebox(upper(loFormRef.Name) + ".ScaleMode will be set to 3 - Pixels", ;
                MB_OKCANCEL+MB_ICONSTOP,"Please Note") = IDOK
    loFormRef.ScaleMode = 3  
   ELSE
    llOK = .f. 
  ENDIF
ENDIF
local array laDE[1]
aselobj(laDE,2)
lcType = UPPER(ALLTRIM(lcType))

IF llOK
  IF wexist("PROPERTIES")
    hide window properties
  ENDIF
  lcClassLib = set("CLASSLIB")
  set classlib to xxwb additive
  DO CASE
    ***************************************************
    CASE lcType = "LABEL"
    ***************************************************
      loBuilder = CREATEOBJECT("frmBldlbl",@laSelectedObjects,loFormRef)
      loBuilder.Show()
    ***************************************************
    CASE lcType = "TXTPICKLISTVALID"
    ***************************************************
      loBuilder = CREATEOBJECT("frmBuildertxtPicklistValid",@laSelectedObjects,loFormRef,laDE[1])
      loBuilder.Show()
    ***************************************************
    CASE lcType = "TEXTBOX"
    ***************************************************
      loBuilder = CREATEOBJECT("frmBldTxt",@laSelectedObjects,loFormRef,laDE[1])
      loBuilder.Show()
    ***************************************************
    CASE lcType = "BUSINESSOBJECTMANAGER"
    ***************************************************
      MESSAGEBOX("The custom VMP BusinessObjectManager " + ;
                   "builder is not yet available.",48,"Please Note")
    ***************************************************
    CASE lcType = "BUSINESSOBJECT"
    ***************************************************
      MESSAGEBOX("The custom VMP BusinessObject " + ;
                   "builder is not yet available.",48,"Please Note")
    ***************************************************
    CASE lcType = "DATASOURCEBEHAVIOR"
    ***************************************************
      MESSAGEBOX("The custom VMP DataSourceBehavior " + ;
                   "builder is not yet available.",48,"Please Note")
    ***************************************************
    CASE lcType = "DATASOURCE"
    ***************************************************
      loBuilder = CREATEOBJECT("frmBuilderDataSources",@laSelectedObjects,loFormRef,laDE[1])
      loBuilder.Show()
    ***************************************************
    CASE lcType = "CONNECTIONBEHAVIOR"
    ***************************************************
      MESSAGEBOX("The custom VMP ConnectionBehavior " + ;
                 "builder is not yet available.",48,"Please Note")
  ENDCASE    
  TRY 
  SET CLASSLIB TO &lcClassLib
  CATCH
  ENDTRY
  IF WEXIST("PROPERTIES")
    SHOW WINDOW Properties BOTTOM 
  ENDIF
ENDIF
RETURN 
