*
*  X3WINMSG.PRG
*  Call the Visual FoxPro MESSAGEBOX() function 
*  See also X3MSGSVC.PRG
*
*  Copyright (c) 1997-2005 Visionpace   All Rights Reserved
*                17501 East 40 Hwy., Suite 218
*                Independence, MO 64055
*                816-350-7900 
*                http://www.visionpace.com
*                http://vmpdiscussion.visionpace.com
*  Author:  Drew Speedie
*
*  Returns the upper character string of the button pushed
*
*  NOTE: MESSAGEBOX() WILL NOT TIMEOUT in versions of
*        VFP through VFP 6.0.
*
*
*  TYPICAL USAGE: 
*    do x3winmsg with "This record is currently being updated by ;
*                      another user.  Please try again later"
*
*    IF x3winmsg("Are you SURE you want to delete this record?",;
*         .f.,"YN","?") = "YES"
*      * proceed with the deletion
*    ENDIF
*
*
*  PARAMETERS: 
*    tcMessage   (R) message to be displayed
*      tcTitle   (O) title on the message window
*                Defaults to "Please Note" (can be passed as "" for no title)
*    tcButtons   (O) string yielding the desired command buttons
*                  "OC"      OK/Cancel
*                  "ARI"     Abort/Retry/Ignore
*                  "YNC"     Yes/No/Cancel
*                  "YN"      Yes/No
*                  "RC"      Retry/Cancel
*                Defaults to "OK" 
*                You can also optionally include a "|"
*                  pipe character before the button to
*                  be initially selected:  "YN|C"
*       tcIcon   (O) icon to be displayed
*                  "S"       Stop sign
*                  "?"       Question mark
*                  "!"       Exclamation point
*                  "I"       Information icon
*                Defaults to "!" (can be passed as "" for no icon)
*                NOTE:  According to The Windows Interface
*                       Guidelines for Software Design, the "?"
*                       icon is no longer recommended
*     tlSilent   (O) Silent/don't ring the bell
*                    Defaults to .f. -- bell rings, using
*                    the sound attached to the tcIcon value
*                    in the Windows Control Panel.
*                    If you pass this parameter as .T., this 
*                    routine will only actually be silent if
*                    the user has attached NO sound to the
*                    Windows event indicated by tcIcon.
*
PARAMETERS tcMessage, ;
           tcTitle, ;
           tcButtons, ;
           tcIcon, ;
           tlSilent

LOCAL loActiveForm
DO CASE
  CASE TYPE("oForms") = "O" AND NOT ISNULL(oForms)
    loActiveForm = oForms.ScreenActiveForm()
  CASE TYPE("_Screen.ActiveForm.BaseClass") = "C"
    loActiveForm = _Screen.ActiveForm
    IF UPPER(loActiveForm.BaseClass) = "OLECONTROL"
      DO WHILE .t.
        loActiveForm = loActiveForm.PARENT
        IF UPPER(loActiveForm.BaseClass) == "FORM"
          EXIT 
        ENDIF
      ENDDO
    ENDIF              
  OTHERWISE
    loActiveForm = .NULL.
ENDCASE
    
IF TYPE("loActiveForm") = "O" AND NOT ISNULL(loActiveForm) ;
     AND TYPE("loActiveForm.ActiveControl") = "O" AND NOT ISNULL(loActiveForm.ActiveControl) ;
     AND UPPER(loActiveForm.ActiveControl.BaseClass) = "GRID" ;
     AND TYPE("loActiveForm.LockScreen") = "L" ;
     AND loActiveForm.LockScreen 
  *
  *  This code is here because of the code we have
  *  in XXFW.VCX/grdBase.BeforeRowColChange() to
  *  fix the ugly "back-scrolling" when the columns'
  *  gross width exactly match the grid width and
  *  the user back-tabs from the first column.
  *  Unfortunately, =MESSAGEBOX() fires the 
  *  Grid.BeforeRowColChange() but not the Grid.Valid(),
  *  so we put this code here to make sure the
  *  active form.LockScreen doesn't remain locked
  *  after presenting a MESSAGEBOX() while in a
  *  grid
  *
  loActiveForm.LockScreen = .f.
ENDIF  

LOCAL lnFMP, lnSMP
IF NOT ISNULL(loActiveForm) 
  lnFMP = loActiveForm.MousePointer
  loActiveForm.MousePointer = 0
ENDIF
lnSMP = _Screen.MousePointer
_Screen.MousePointer = 0

LOCAL lcRetVal, lnButtons, lnFocus, lnIcon, lnMessageBeep
tcMessage = ALLTRIM(tcMessage)
IF TYPE("tcTitle") = "C"
  tcTitle = ALLTRIM(tcTitle)
 ELSE
  tcTitle = "Please Note"
ENDIF
lnFocus = 0
IF NOT TYPE("tcButtons") = "C"
  tcButtons = "O"
  lnButtons = 0                &&& OK                  {ESCAPE} does nothing
 ELSE
  tcButtons = UPPER(ALLTRIM(tcButtons))
  IF "|" $ tcButtons
    lnFocus = AT_C("|",tcButtons)     &&int(val(rightc(tcButtons,1)))
  ENDIF 
  tcButtons = STRTRAN(tcButtons,"|","")
  DO CASE 
    CASE tcButtons = "OC"      &&& OK/Cancel           {ESCAPE} yields Cancel
      lnButtons = 1
    CASE tcButtons = "ARI"     &&& Abort/Retry/Ignore  {ESCAPE} does nothing
      lnButtons = 2
    CASE tcButtons = "YNC"     &&& Yes/No/Cancel       {ESCAPE} yields Cancel
      lnButtons = 3
    CASE tcButtons = "YN"      &&& Yes/No              {ESCAPE} does nothing
      lnButtons = 4
    CASE tcButtons = "RC"      &&& Retry/Cancel        {ESCAPE} does nothing
      lnButtons = 5
    OTHERWISE                  &&& default to OK       {ESCAPE} does nothing
      tcButtons = "O"
      lnButtons = 0 
  ENDCASE 
ENDIF
IF lnFocus = 1 OR lnFocus>LENC(tcButtons)
  lnFocus = 0
 ELSE
  lnFocus = IIF(lnFocus=2,256,512)
ENDIF
IF TYPE("tcIcon") # "C"
  lnIcon = 48                  &&& Exclamation point
 ELSE
  DO CASE
    CASE EMPTY(tcIcon)         &&& no icon
      lnIcon = 0
    CASE tcIcon = "S"          &&& Stop sign
      lnIcon = 16
    CASE tcIcon = "?"          &&& Question mark
      lnIcon = 32
    CASE tcIcon = "I"          &&& Information point
      lnIcon = 64
    OTHERWISE                  &&& Exclamation point
      lnIcon = 48    
  ENDCASE
ENDIF
IF TYPE("tlSilent")#"L" OR !tlSilent
  *
  *  make sure the MessageBeep() Windows API function
  *  is loaded to ring the bell rather than via ?? CHR(7), 
  *  which generates a problem with commandbuttons having 
  *  both Caption and Picture
  *    XXDTES("XXFW.VCX","VFP ANOMALY WHEN BOTH Caption AND Picture ARE SET","cmdBase","zReadMe")
  *
  #IF TYPE("_VFP") = "O" AND VERSION(5) >= 700
    *  VFP 7 or higher, faster
    IF !X7ISAPIF("MessageBeep")
      DECLARE Long MessageBeep IN USER32.DLL Long uType 
    ENDIF
  #ELSE
    *  VFP 6 and below, slower
    DECLARE Long MessageBeep IN USER32.DLL Long uType 
  #ENDIF
  *!*	  ?? chr(7)
  *
  *  please note that you will only actually hear the
  *  following beep/sound if the user has attached NO
  *  sound for the MessageBox() lnIcon/tcIcon in 
  *  the Windows Control Panel -- if they have a sound
  *  setup, then they will hear *that* sound
  *
  MessageBeep(lnMessageBeep)
ENDIF
lxRetVal = MESSAGEBOX(tcMessage,lnButtons+lnFocus+lnIcon,tcTitle) 
DO CASE
  CASE lxRetVal = 1
    lxRetVal = "OK"
  CASE lxRetVal = 2
    lxRetVal = "CANCEL"
  CASE lxRetVal = 3
    lxRetVal = "ABORT"
  CASE lxRetVal = 4
    lxRetVal = "RETRY"
  CASE lxRetVal = 5
    lxRetVal = "IGNORE"
  CASE lxRetVal = 6
    lxRetVal = "YES"
  CASE lxRetVal = 7
    lxRetVal = "NO"
  OTHERWISE
    lxRetVal = "ESCAPE"
ENDCASE

IF TYPE("lnFMP") = "N"
  loActiveForm.MousePointer = lnFMP
ENDIF
_Screen.MousePointer = lnSMP

RETURN lxRetVal
 
 

*!*	#define MB_ICONASTERISK 0x40
*!*	* SystemExclamation 
*!*	#define MB_ICONEXCLAMATION 0x30
*!*	* SystemHand 
*!*	#define MB_ICONHAND 0x10
*!*	* SystemQuestion 
*!*	#define MB_ICONQUESTION 0x20
*!*	* SystemDefault 
*!*	#define MB_OK 0

 
 
