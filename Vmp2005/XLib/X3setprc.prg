*
*  X3SETPRC.PRG
*  SET PROCEDURE to the passed .PRG(s)
*
*  This routine is included for backward compatibility
*  and is not called anywhere in the current VMP
*  framework -- use X8SETPRC.PRG instead.
*
*  Copyright (c) 1997-2005 Visionpace   All Rights Reserved
*                17501 East 40 Hwy., Suite 218
*                Independence, MO 64055
*                816-350-7900 
*                http://www.visionpace.com
*                http://vmpdiscussion.visionpace.com
*  Author:  Drew Speedie and Art Bergquist
*
*  Usage:
*    Specify a single procedure file:
*      X3SETPRC("MYPRG")
*      X3SETPRC("MYPRG.PRG")
*      IF NOT X3SETPRC("MYPRG.PRG")
*        * take the appropriate action
*      ENDIF
*
*    Specify more than one procedure file:
*      X3SETPRC("MYPRG, YOURPRG")
*      X3SETPRC("MYPRG.PRG, YOURPRG.PRG")
*      IF NOT X3SETPRC("MYPRG.PRG, YOURPRG.PRG")
*        * take the appropriate action
*      ENDIF
*
*  lParameters
*    tcProcFiles (R) Name of the .PRG procedure file(s) to be 
*                    added to the SET PROCEDURE "path".  Note 
*                    that you can specify one or more procedure 
*                    files separated by commas.
*                    The ".PRG" extension is optional.
*
*
LPARAMETERS tcProcFiles
LOCAL lcProcFiles, llSuccess, lcOnError
IF TYPE("tcProcFiles") = "C" ;
     AND NOT ISNULL(tcProcFiles) ;
     AND NOT EMPTY(tcProcFiles)
  lcProcFiles = UPPER(ALLTRIM(tcProcFiles))
 ELSE
  RETURN .f.
ENDIF
llSuccess = .t.
lcOnError = ON("ERROR")
ON ERROR llSuccess = .f.
IF OCCURS(",",lcProcFiles) = 0
  *
  *  name indirection is a little faster, and can be used
  *  when only one file is specified
  * 
  SET PROCEDURE TO (lcProcFiles) ADDITIVE 
 ELSE
  SET PROCEDURE TO &lcProcFiles. ADDITIVE
ENDIF
ON ERROR &lcOnError
RETURN llSuccess
