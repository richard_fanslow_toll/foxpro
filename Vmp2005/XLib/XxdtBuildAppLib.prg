*
*  XXDTBuildAppLIB.PRG
*  When you want to distribute your app
*  in separate "modules", this utility generates an .EXE
*  or .APP that contains files that must be copied to disk
*  at runtime because VFP won't otherwise find them 
*  when VMP framework files are compiled to XXLIB.EXE or
*  XXLIB.APP and distributed separately.  
*
*  Copyright (c) 2002-2005 Visionpace   All Rights Reserved
*                17501 East 40 Hwy., Suite 218
*                Independence, MO 64055
*                816-350-7900 
*                http://www.visionpace.com
*                http://vmpdiscussion.visionpace.com
*  Author:  Drew Speedie
*
*  For more detailed information, see the 
*    Distributing a single app in separate .EXE/.APP pieces
*  help topic in the Reference Guide section of the 
*  VMPHELP.CHM online help file.  
*  _VFP.Help(FULLPATH("VMPHELP.CHM"),,"Distributing a single app in separate .EXE/.APP pieces")
*
*  See also XXDTBuildXXLIB.PRG
*    MODIFY COMMAND XXDTBuildXXLIB
*
*  Run this utility AFTER building the application .PJX file
*  "normally", typically done by executing a 
*  "BuildProject.PRG" as demonstrated by the
*  VMBuildPJX.PRG/NTBuildPJX.PRG utilities in the \DVSTUFF
*  folder of the VM and NT example applications that ship
*  with VMP.
*
*  This utility:
*  1- Iterates through the passed main ??.PJX project file 
*     and marks Excluded all intermediate/app-specific 
*     files whose extensions match the passed list of 
*     extensions.  These files are those that VFP doesn't 
*     find when application source code is executed from 
*     the separate XXLIB.EXE file, typically .SCX-based 
*     forms, .FRX/.LBX reports, and graphic files.
*  2- Iterates through the passed main ??.PJX project file
*     and marks Excluded all VMP framework source code 
*     files, which are compiled into a separate XXLIB.EXE
*     by running XXDTBuildXXLIB.PRG.
*  3- Creates a free table containing all the files found
*     by Step 1.  The code in the Methods field of .SCXs
*     is removed, both to reduce the overall size, and to
*     keep the code from being readily accessible in the
*     production environment.
*  4- Creates the app-specific ??LIB.PRG file that
*     - Copies each file in the free table to disk, if 
*       that file doesn't exist or is not the correct
*       date/time.
*     - Issues SET CLASSLIB TO each intermediate/app-specific
*       class library.
*     - Includes an EraseFilesFromDisk local procedure
*       that can optionally be called explicitly on 
*       app exit, as is demonstrated in VMMAIN.PRG:
*         XXDTES("VMMAIN.PRG","EraseFilesFromDisk")
*  5- Turns the ??LIB.PRG created in Step 4 into a self-
*     contained .EXE/.APP, including the free table created
*     in Step 3 which contains source code files that are
*     copied to disk at runtime if/when necessary.
*     
*  The output of this utility is a single ??LIB.EXE --
*  if you want to review the working files created here 
*  to generate that ??LIB.EXE, pass the tlDontErase 
*  parameter as .T.
*
*  Note that when ??LIB.EXE is run, it copies files to disk
*  in the current folder (the folder containing the ??LIB.EXE) 
*  OR the folder you specify in the tcCopyToPath parameter.  
*  So when running your application, the current user must 
*  have WRITE access to that folder, at least long enough to 
*  copy the Excluded files into that folder the first time 
*  the app is run. 
*

*
*  lParameters
*         tcPJX (R) Specify the name of the .PJX project file
*                     to be modified by marking Excluded all
*                     its intermediate/app-specific files of
*                     the types specified in tcExclFileExt. 
*                   If this parameter is not passed but there 
*                     is currently a .PJX Project Manager 
*                     project file open (Application.ActiveProject
*                     is an object), that .PJX project file 
*                     is assumed.
*
*   tlDebugInfo (O) Pass this parameter as .T. to set the
*                     Debug property of the ??LIB.PJX to
*                     .T. (same as checking the Debug
*                     info checkbox of the Project tab
*                     on the Project Info dialog)
*                   Defaults to .F. -- same effect as
*                     checking/selecting the Debug info
*                     checkbox of the Project tab on the
*                     Project Info dialog, yielding a
*                     smaller resulting ??LIB.EXE.
*
*   tlNoEncrypt (O) Pass this parameter as .T. to set the
*                     Encrypted property of the ??LIB.PJX
*                     to .F. (same as UN-checking the
*                     Encrypted checkbox of the Project tab
*                     on the Project Info dialog) 
*                   Defaults to .F. -- same effect as
*                     checking/selecting the Encrypted
*                     checkbox of the Project tab on the
*                     Project Info dialog, resulting in a
*                     more secure ??LIB.EXE.
*
*    tcIconFile (O) Pass the name of an .ICO file to be used
*                     for tcLIBFileName -- this is the 
*                     file specified on the Project tab of
*                     the Project Info dialog.
*                   Defaults to the .ICO attached to 
*                     tcPJX, if any.
*                   This property is IGNORED when tcLIBFileName
*                     indicates an .APP rather than an .EXE,
*                     because VFP only attaches an .ICO file
*                     to an .EXE, not an .APP.
*
*    tlModiProj (O) Pass this parameter as .T. to pause
*                     the action of this utility in the
*                     Project Manager, just before building 
*                     tcLIBFileName, so you can review the
*                     project and/or make changes.
*                   Defaults to .F. -- the Project Manager
*                     is not summoned prior to BUILDing
*                     tcLIBFileName.
*
* tcLIBFileName (O) Specify the filename of the app-
*                     specific ??LIB.EXE that is created
*                     by running this utility.
*                   If passed as a string without an extension,
*                     ".EXE" is the assumed extension.
*                   If passed as a string with an extension,
*                     the extension must be either ".EXE"
*                     or ".APP".
*                   Defaults to JUSTSTEM(tcPJX)+"LIB.EXE":
*                     NT.PJX -> "NTLIB.EXE"
*                     VM.PJX -> "VMLIB.EXE" 
*
*   tlDontErase (O) Pass this parameter as .T. to leave
*                     the working files generated here
*                     around once this utility finishes:
*                       ??LIB.PRG
*                       ??LIB.PJX
*                       AppSource.DBF
*                   Passing this parameter as .T. also
*                     results in the above working files
*                     opened in the corresponding editor
*                     once this utility finishes successfully.
*                   Defaults to .F. -- the above working
*                     files are erased, leaving just
*                     ??LIB.EXE as the output of this 
*                     utility.
*
* tcExclFileExt (O) Specify a comma-separated list of file
*                     extensions to be marked Excluded 
*                     from the tcPJX project.
*                   Defaults to 
*                     "SCX,FRX,LBX,ANI,BMP,CUR,DIB,EMF,EXIF,GFA,GIF,JFIF,JPE,JPEG,JPG,ICO,PNG,TIF,TIFF,WMF"
*  
*  tcCopyToPath (O) Specify a full or relative path to
*                     be prepended onto the names of the
*                     files that are marked Excluded
*                     from tcPJX (tcExclFileExt) and which
*                     are then copied to disk at runtime when 
*                     tcLIBFileName is executed by the ??MAIN
*                     program/??.EXE.  If you specify a
*                     relative path, that path is relative  
*                     to the folder where the main ??.EXE
*                     executes, since it calls tcLIBFileName
*                     on app startup.
*                   NOTE that the ??LIB.EXE generated here 
*                     ALSO accepts a parameter indicating 
*                     the folder to which to copy the files 
*                     -- you have the choice to specify 
*                     tcCopyToPath here, or pass it to 
*                     ??LIB.EXE at runtime.  You can even do
*                     both -- any parameter passed to 
*                     ??LIB.EXE takes precedence.
*                   Defaults to SPACE(0), whereupon when 
*                     tcLIBFileName copies files to disk at 
*                     runtime, those files are copied to 
*                     the same folder where the main ??.EXE 
*                     is executing.
*                   See also this same parameter of
*                     XXDTBuildXXLIB.PRG
*                       XXDTES("XXDTBuildXXLIB.PRG","tcCopyToPath")
*                
* tcOverWriteExpr (O) Specify an expression that is
*                       EVALUATE()d in the ??LIB.EXE
*                       generated here, which expression
*                       yields a logical value indicating
*                       what to do when files that get 
*                       copied to disk already exist - .T. 
*                       (the default) indicates that they 
*                       are overwritten, .F. indicates that 
*                       no copying takes place.
*                     This string can be any expression
*                       that can be EVALUATE()d at runtime
*                       by ??EXE.APP, not just limited to a
*                       simple ".T."/".F.", so you can 
*                       specify a function that is available 
*                       when ??LIB.EXE is executed at runtime.
*                     NOTE that the ??LIB.EXE generated here 
*                       ALSO accepts a tcOverWriteExpr 
*                       parameter -- you have the choice to 
*                       specify tcOverWriteExpr here, or 
*                       pass it to ??LIB.EXE at runtime.  
*                       You can even do both -- any parameter 
*                       passed to ??LIB.EXE takes precedence.
*                    Defaults to ".T." 
*                    See also this same parameter of
*                      XXDTBuildXXLIB.PRG
*                        XXDTES("XXDTBuildXXLIB.PRG","tcOverWriteExpr")
*                    
*
LPARAMETERS tcPJX, ;
            tlDebugInfo, ;
            tlNoEncrypt, ;
            tcIconFile, ;
            tlModiProj, ;
            tcLIBFileName, ;
            tlDontErase, ;
            tcExclFileExt, ;
            tcCopyToPath, ;
            tcOverWriteExpr

SET ASSERTS ON 

IF (VARTYPE(tcPJX) = "C" AND NOT EMPTY(tcPJX)) ;
     OR ;
     TYPE("Application.ActiveProject") = "O" 
 ELSE
  ASSERT .f. ;     
     MESSAGE "tcPJX parameter is required when there is no current Application.ActiveProject"
  RETURN 
ENDIF

IF VARTYPE(tcLIBFileName) = "C" ;
     AND NOT EMPTY(tcLIBFileName) ;
     AND OCCURS(".",tcLIBFileName) = 1
  IF NOT INLIST(UPPER(JUSTEXT(tcLIBFileName)),"EXE","APP")
    ASSERT .f. ;
         MESSAGE "If tcLIBFileName is passed with an extension, that extension must be either .APP or .EXE"
    RETURN 
  ENDIF
ENDIF

*
*  preserve the parameters past the upcoming 
*  CLEAR ALL, CLOSE ALL
*
CREATE CURSOR XXDTBuildAppLib (PJXFile C(20), ;
                               DebugInfo C(1), ;
                               NoEncrypt C(1), ;
                               IconFile C(240), ;
                               ModiProj C(1), ;
                               LIBFile C(20), ;
                               DontErase C(1), ;
                               ExclExts C(100), ;
                               CopyToPath C(240), ;
                               OWExpr C(240))
INSERT INTO XXDTBuildAppLib (PJXFile, ;
                             DebugInfo, ;
                             NoEncrypt, ;
                             IconFile, ;
                             ModiProj, ;
                             LIBFile, ;
                             DontErase, ;
                             ExclExts, ;
                             CopyToPath, ;
                             OWExpr) ;
      VALUES ;
     (IIF(NOT VARTYPE(tcPJX)="C" OR EMPTY(tcPJX),Application.ActiveProject.Name,tcPJX), ;
      IIF(VARTYPE(tlDebugInfo) = "L", IIF(tlDebugInfo,"Y","N"), "N"), ;
      IIF(VARTYPE(tlNoEncrypt) = "L", IIF(tlNoEncrypt,"Y","N"), "N"), ;
      IIF(VARTYPE(tcIconFile) = "C", tcIconFile, SPACE(0)), ;
      IIF(VARTYPE(tlModiProj) = "L", IIF(tlModiProj,"Y","N"), "N"), ;
      IIF(VARTYPE(tcLIBFileName) = "C" AND NOT EMPTY(tcLIBFileName),tcLIBFileName,SPACE(0)), ;
      IIF(VARTYPE(tlDontErase) = "L", IIF(tlDontErase,"Y","N"), "N"), ;
      IIF(VARTYPE(tcExclFileExt) = "C" AND NOT EMPTY(tcExclFileExt),tcExclFileExt,SPACE(0)), ;
      IIF(VARTYPE(tcCopyToPath) = "C" AND NOT EMPTY(tcCopyToPath),tcCopyToPath,SPACE(0)), ;
      IIF(VARTYPE(tcOverWriteExpr) = "C" AND NOT EMPTY(tcOverWriteExpr),tcOverWriteExpr,SPACE(0)))
CURSORTOXML("XXDTBuildAppLib","XXDTBuildAppLib.XML",1,512,0,"")

CLEAR ALL
CLOSE ALL

*
*  get the parameters back
*
LOCAL lcPJXFileName, lcLIBFileName, lcExclFileExt, ;
      lcCopyToPath, lcOverWriteExpr, llDontErase, ;
      llDebugInfo, llNoEncrypt, lcIconFile, llModiProj
XMLTOCURSOR("XXDTBuildAppLib.XML","XXDTBuildAppLib",512)
lcPJXFileName = UPPER(ALLTRIM(PJXFile))
llDebugInfo = UPPER(ALLTRIM(DebugInfo)) = "Y"
llNoEncrypt = UPPER(ALLTRIM(NoEncrypt)) = "Y"
lcIconFile = UPPER(ALLTRIM(IconFile))
llModiProj = UPPER(ALLTRIM(ModiProj)) = "Y"
lcLIBFileName = UPPER(ALLTRIM(LIBFile))
llDontErase = UPPER(ALLTRIM(DontErase)) = "Y"
lcExclFileExt = UPPER(ALLTRIM(ExclExts))
lcCopyToPath = UPPER(ALLTRIM(CopyToPath))
lcOverWriteExpr = ALLTRIM(OWExpr)
USE IN XXDTBuildAppLib
ERASE XXDTBuildAppLib.XML

IF OCCURS(".",lcPJXFileName) = 0 
  lcPJXFileName = lcPJXFileName + ".PJX"
ENDIF
IF NOT UPPER(JUSTEXT(lcPJXFileName)) == "PJX"
  lcPJXFileName = JUSTSTEM(lcPJXFileName) + ".PJX"
ENDIF

IF EMPTY(lcLIBFileName)
  lcLIBFileName = JUSTSTEM(lcPJXFileName) + "LIB.EXE"
ENDIF
IF EMPTY(JUSTEXT(lcLIBFileName))
  lcLIBFileName = lcLIBFileName + ".EXE"
ENDIF

IF EMPTY(lcExclFileExt)
  lcExclFileExt = "SCX,FRX,LBX,ANI,BMP,CUR,DIB,EMF,EXIF,GFA,GIF,JFIF,JPE,JPEG,JPG,ICO,PNG,TIF,TIFF,WMF"
ENDIF

lcCopyToPath = UPPER(ALLTRIM(lcCopyToPath))
IF NOT EMPTY(lcCopyToPath)
  lcCopyToPath = ADDBS(lcCopyToPath)
ENDIF

IF EMPTY(lcOverWriteExpr)
  lcOverWriteExpr = ".T."
ENDIF

IF JUSTEXT(lcLIBFileName) = "EXE"
  IF EMPTY(lcIconFile) 
    *
    *  use the Icon attached to lcPJXFileName, if any
    *
    MODIFY PROJECT (lcPJXFileName) NOWAIT NOSHOW 
    IF NOT EMPTY(Application.ActiveProject.Icon)
      lcIconFile = Application.ActiveProject.Icon
    ENDIF
    CLOSE ALL 
  ENDIF
 ELSE
  *
  *  VFP doesn't support the Icon property for an .APP
  *
  lcIconFile = SPACE(0)
ENDIF

LOCAL loProject, laAppFilesToExclude[1], lnCount, loFile, ;
      lcFileName, lcFileName1, lcJustFile, lcChar2, lcExt, ;
      lcLIBPRG, lcLIBPJX, lcJustPath, lcTempSCX, lcTempSCT, ;
      laAppVCXs[1], lnVCXCount
lcLIBPRG = JUSTSTEM(lcLIBFileName) + ".PRG"
lcLIBPJX = JUSTSTEM(lcLIBFileName) + ".PJX"

*
*  erase existing versions of files to be created here
*
ERASE (lcLIBFileName)
ERASE (STRTRAN(lcLIBFileName,".APP",".EXE"))
ERASE (STRTRAN(lcLIBFileName,".EXE",".APP"))
ERASE (lcLIBPRG)
ERASE (STRTRAN(lcLIBPRG,".PRG",".FXP"))
ERASE (STRTRAN(lcLIBPRG,".PRG",".BAK"))
ERASE (lcLIBPJX)
ERASE (STRTRAN(lcLIBPJX,".PJX",".PJT"))
ERASE AppSource.DBF
ERASE AppSource.FPT

MODIFY PROJECT (lcPJXFileName) NOWAIT NOSHOW 

****************************************************
****************************************************
*  Steps 1 and 2
****************************************************
****************************************************
lnCount = 0
lnVCXCount = 0
loProject = Application.ActiveProject
*
*  iterate through the project to find files that will
*  be marked Excluded here
*
FOR EACH loFile IN Application.ActiveProject.Files
  lcFileName = UPPER(loFile.Name)
  lcJustFile = UPPER(JUSTFNAME(loFile.Name))    
  lcChar2 = SUBSTRC(lcJustFile,2,1)
  lcExt = UPPER(JUSTEXT(lcJustFile))
  IF lcJustFile = "X" ;
        AND;
        (ISDIGIT(lcChar2) OR lcChar2 = "X") ;
        AND ;
        INLIST(lcExt,"SCX","VCX","PRG","FRX","LBX","APP","EXE")
    *
    *  mark Excluded each VMP framework source code file
    *
    loFile.Exclude = .t.
    LOOP 
  ENDIF
  IF lcExt = "VCX"
    *
    *  intermediate or app-specific .VCX
    *
    lnVCXCount = lnVCXCount + 1
    DIMENSION laAppVCXs[lnVCXCount]
    laAppVCXs[lnVCXCount] = JUSTFNAME(lcFileName)
  ENDIF
  IF lcExt $ lcExclFileExt
    *
    *  mark Excluded each intermediate/app-specific file
    *  whose extension is listed in lcExclFileExt
    *
    loFile.Exclude = .t.
    *
    *  and store each thus-excluded file to 
    *  laAppFilesToExclude[], for further processing
    *  when lcLIBFileName is created below
    *    
    lnCount = lnCount + 1
    DIMENSION laAppFilesToExclude[lnCount]
    laAppFilesToExclude[lnCount] = lcFileName
  ENDIF  
ENDFOR

****************************************************
****************************************************
*  Step 3
****************************************************
****************************************************
*
*  create a free table to store each excluded app-specific
*  or intermediate file, which can then be copied to disk
*  at runtime by executing the following, likely in your
*  ??MAIN.PRG:
*    DO <lcLIBFileName>
*  
LOCAL lcSetCompatible
lcSetCompatible = SET("COMPATIBLE")
SET COMPATIBLE ON   &&& so FSIZE() returns the file size
CREATE TABLE AppSource.DBF (F_Name C(250), F_Contents M, F_DateTime T, F_Size I)
FOR EACH lcFileName IN laAppFilesToExclude
  lcFileName = UPPER(lcFileName)
  lcExt = UPPER(JUSTEXT(lcFileName))
  lcFileName1 = SPACE(0)
  lcJustPath = UPPER(JUSTPATH(lcFileName))
  lcTempSCX = SPACE(0)
  lcTempSCT = SPACE(0)
  DO CASE
    CASE lcExt = "SCX"
      lcFileName1 = JUSTSTEM(lcFileName) + ".SCT"
    CASE lcExt = "FRX"
      lcFileName1 = JUSTSTEM(lcFileName) + ".FRT"
    CASE lcExt = "LBX"
      lcFileName1 = JUSTSTEM(lcFileName) + ".LBT"
  ENDCASE
  IF NOT EMPTY(lcFileName1) AND NOT EMPTY(lcJustPath)
    lcFileName1 = ADDBS(lcJustPath) + lcFileName1
  ENDIF
  IF lcExt = "SCX"
    *  remove Methods contents
    lcTempSCX = JUSTSTEM(lcFileName) + "_TEMP.SCX"
    lcTempSCT = JUSTSTEM(lcFileName) + "_TEMP.SCT"
    COPY FILE (lcFileName) TO (lcTempSCX)
    COPY FILE (lcFileName1) TO (lcTempSCT)
    USE (lcTempSCX) 
    replace ALL Methods WITH SPACE(0) IN (lcTempSCX)
    USE IN (lcTempSCX)
    *  add to AppSource.DBF
    INSERT INTO AppSource (F_Name, F_DateTime, F_Size) VALUES (UPPER(JUSTFNAME(lcFileName)), FDATE(lcFileName,1), FSIZE(lcTempSCX))
    APPEND MEMO AppSource.F_Contents FROM (lcTempSCX) OVERWRITE 
    INSERT INTO AppSource (F_Name, F_DateTime, F_Size) VALUES (UPPER(JUSTFNAME(lcFileName1)), FDATE(lcFileName1,1), FSIZE(lcTempSCT))
    APPEND MEMO AppSource.F_Contents FROM (lcTempSCT) OVERWRITE 
    ERASE (lcTempSCX)
    ERASE (lcTempSCT)    
   ELSE
    *  add to AppSource.DBF
    INSERT INTO AppSource (F_Name, F_DateTime, F_Size) VALUES (UPPER(JUSTFNAME(lcFileName)), FDATE(lcFileName,1), FSIZE(lcFileName))
    APPEND MEMO AppSource.F_Contents FROM (lcFileName) OVERWRITE 
    IF NOT EMPTY(lcFileName1)
      INSERT INTO AppSource (F_Name, F_DateTime, F_Size) VALUES (UPPER(JUSTFNAME(lcFileName1)), FDATE(lcFileName1,1), FSIZE(lcFileName1))
      APPEND MEMO AppSource.F_Contents FROM (lcFileName1) OVERWRITE 
    ENDIF
  ENDIF
ENDFOR
SET COMPATIBLE &lcSetCompatible
USE IN AppSource

****************************************************
****************************************************
*  Step 4
****************************************************
****************************************************
*
*  create lcLIBPRG
*
LOCAL lcText
SET TEXTMERGE ON NOSHOW 
TEXT TO lcText NOSHOW 
*
*  <<lcLIBPRG>>
*
*  Copy intermediate/app-specific source files that
*  VFP can't find in the main application .EXE
*    AND
*  SET CLASSLIB TO all the intermediate/app-specific
*  class libraries in the project
*
LPARAMETERS tcCopyToPath, tcOverWriteExpr
LOCAL lnSelect, lcFileName, lcSetSafety, lcOnError, ;
      llSuccess, lcCopyToPath, lcOverWriteExpr
IF VARTYPE(tcCopyToPath) = "C" AND NOT EMPTY(tcCopyToPath)
  lcCopyToPath = ADDBS(UPPER(ALLTRIM(tcCopyToPath)))
 ELSE
  lcCopyToPath = "<<lcCopyToPath>>"
ENDIF
EXTERNAL PROCEDURE X8SETCLS.PRG
IF EMPTY(lcCopyToPath)
  lcCopyToPath = ADDBS(FULLPATH(CURDIR()))
ENDIF
IF VARTYPE(tcOverWriteExpr) = "C" AND NOT EMPTY(tcOverWriteExpr)
  lcOverWriteExpr = ALLTRIM(tcOverWriteExpr)
 ELSE
  lcOverWriteExpr = "<<lcOverWriteExpr>>"
ENDIF
IF EMPTY(lcOverWriteExpr)
  lcOverWriteExpr = ".T."
ENDIF
lnSelect = SELECT(0)
lcSetSafety = SET("SAFETY")
lcOnError = ON("ERROR")
llSuccess = .t.
SET SAFETY OFF 
USE AppSource 
SCAN
  lcFileName = UPPER(ALLTRIM(AppSource.F_Name))
  lcFileName = lcCopyToPath+lcFileName
  *
  *  only copy the file to disk if either
  *
  *  1- It doesn't exist:
  *     - This is the initial installation
  *     - The user has deleted/renamed it 
  *     - Your application deletes all such non .APP/.EXE
  *       files each time it terminates
  *
  *    OR
  *
  *  2- EVALUATE(lcOverWriteExpr) = .T.
  *     lcOverWriteExpr can either be passed to ??LIB.EXE 
  *     at runtime, or can be specified when ??LIB.EXE is 
  *     built, by passing it to the XXDTBuildAppLib.PRG 
  *     utility
  *     
  IF NOT FILE(lcFileName) OR EVALUATE(lcOverWriteExpr)
    ON ERROR llSuccess = .f.
    COPY MEMO AppSource.F_Contents TO (lcFileName)
    ON ERROR &lcOnError
  ENDIF
  IF NOT llSuccess
    EXIT 
  ENDIF
ENDSCAN
SET SAFETY &lcSetSafety
USE IN AppSource
SELECT (lnSelect)
IF NOT llSuccess
  RETURN .f.
ENDIF
ENDTEXT
SET TEXTMERGE OFF 
STRTOFILE(lcText,lcLIBPRG,.f.)
IF lnVCXCount > 0
  LOCAL lcVCXs
  STRTOFILE(CHR(13)+CHR(10) + 'TEXT TO lcVCXs NOSHOW',lcLIBPRG,.t.)
  FOR EACH lcFileName IN laAppVCXs
    STRTOFILE(CHR(13)+CHR(10) + SPACE(2) + lcFileName, ;
              lcLIBPRG,.t.)
  ENDFOR  
ENDIF

STRTOFILE(CHR(13)+CHR(10) + 'ENDTEXT',lcLIBPRG,.t.)
STRTOFILE(CHR(13)+CHR(10) + 'LOCAL ARRAY laVCXs[1]',lcLIBPRG,.t.)
STRTOFILE(CHR(13)+CHR(10) + 'ALINES(laVCXs, lcVCXs, .T.)',lcLIBPRG,.t.)
STRTOFILE(CHR(13)+CHR(10) + 'LOCAL lnVCX, lcVCX',lcLIBPRG,.t.)
STRTOFILE(CHR(13)+CHR(10) + 'FOR lnVCX = 1 TO ALEN(laVCXs, 1)',lcLIBPRG,.t.)
STRTOFILE(CHR(13)+CHR(10) + "  lcVCX = laVCXs[lnVCX]",lcLIBPRG,.t.)
STRTOFILE(CHR(13)+CHR(10) + "  IF NOT X8SETCLS(lcVCX)",lcLIBPRG,.t.)
STRTOFILE(CHR(13)+CHR(10) + "    MESSAGEBOX([Unable to X8SETCLS('] + lcVCX + [')])",lcLIBPRG,.t.)
STRTOFILE(CHR(13)+CHR(10) + '    RETURN .F.',lcLIBPRG,.t.)
STRTOFILE(CHR(13)+CHR(10) + '  ENDIF',lcLIBPRG,.t.)
STRTOFILE(CHR(13)+CHR(10) + 'ENDFOR',lcLIBPRG,.t.)
STRTOFILE(CHR(13)+CHR(10)+CHR(13)+CHR(10)+"RETURN .t.",lcLIBPRG,.t.)

TEXT TO lcText
PROCEDURE EraseFilesFromDisk
*
*  this procedure ERASEs all files that were copied
*  to disk 
*
*  NOTE that this procedure should only be called
*  when you are done with XXLIB.EXE, as demonstrated
*  in VMMAIN.PRG
*    XXDTES("VMMAIN.PRG","EraseFilesFromDisk")
*  where this procedure is called once XXFWMAIN.PRG
*  is finished, and therefore all the VMP XLIB files
*  are no longer needed
*
*  RETURNs a logical value indicating whether the
*  ERASEs were all successful, in case you need to know
*
LOCAL llSuccess, lcOnError
llSuccess = .t.
lcOnError = ON("ERROR")
CLEAR MENUS
CLEAR POPUPS 
ON ERROR llSuccess = .f.
USE AppSource.DBF IN 0   &&& should never fail
IF llSuccess
  SELECT AppSource
  SCAN
    IF JUSTEXT(UPPER(ALLTRIM(F_Name))) = "VCX"
      CLEAR CLASSLIB (ALLTRIM(F_Name))
    ENDIF
    IF ","+JUSTEXT(UPPER(ALLTRIM(F_Name)))+"," $ ",BMP,JPG,GIF,BMP,GIF,JPG,ANI,DIB,EMF,EXIF,GFA,JPEG,JPE,JFIF,PNG,TIF,TIFF,WMF,"
      CLEAR RESOURCES (ALLTRIM(F_Name))
    ENDIF
    ERASE ALLTRIM(F_Name)   &&& could possibly fail
  ENDSCAN
ENDIF
ON ERROR &lcOnError
USE IN SELECT("AppSource")
RETURN llSuccess
ENDTEXT

STRTOFILE(CHR(13)+CHR(10)+CHR(13)+CHR(10)+lcText,lcLIBPRG,.t.)



****************************************************
****************************************************
*  Step 5
****************************************************
****************************************************
*
*  create (lcLIBPJX) project 
*
MODIFY PROJECT (lcLIBPJX) NOWAIT NOSHOW 
LOCAL loProject, loFile
loProject = Application.ActiveProject
loProject.Debug = llDebugInfo
loProject.Encrypted = (NOT llNoEncrypt)
IF NOT EMPTY(lcIconFile)
  loProject.Icon = lcIconFile
ENDIF
loProject.Files.Add(lcLIBPRG)
loProject.Files.Add("AppSource.DBF")
loProject.Files(2).Exclude = .f.
loProject.SetMain(lcLIBPRG)
loProject.Build()
*
*  I don't know why, but the first Build doesn't 
*  always pull in all the files...
*
loProject.Build()

IF llModiProj
  WAIT WINDOW "Here is the " + lcLIBPJX + " where you can make any desired updates" + CHR(13) + ;
              "in the Project Info dialog.  As soon as you close the" + CHR(13) + ;
              "Project Manager, the " + CHR(13) + ;
              SPACE(4) + "BUILD " + JUSTEXT(lcLIBFileName) + SPACE(1) + lcLIBFileName + " FROM " + lcLIBPJX + CHR(13) + ;
              "command will be executed." ;
              NOWAIT NOCLEAR 
  MODIFY PROJECT (lcLIBPJX) 
  WAIT CLEAR 
ENDIF

IF JUSTEXT(lcLIBFileName) = "EXE"
  BUILD EXE (lcLIBFileName) FROM (lcLIBPJX)
 ELSE
  BUILD APP (lcLIBFileName) FROM (lcLIBPJX)
ENDIF

CLOSE ALL 

ACTIVATE SCREEN
CLEAR 
? lcLIBFileName + " has been successfully created."
IF llDontErase = .t.
  *
  *  leave the files created here that are used to
  *  build ??LIB.EXE, usually because you need to 
  *  review them for debugging purposes
  *    ??LIB.PRG
  *    ??LIB.PJX
  *    AppSource.DBF
  *
  USE AppSource
  BROWSE LAST NOWAIT 
  MODIFY PROJECT (lcLIBPJX) NOWAIT 
  MODIFY COMMAND (lcLIBPRG)
  ACTIVATE WINDOW Command
 ELSE
  *
  *  default behavior is to erase all these working
  *  files, yielding only the ??LIB.APP/??LIB.EXE file
  *
  ERASE (lcLIBPRG)
  ERASE (STRTRAN(lcLIBPRG,".PRG",".FXP"))
  ERASE (STRTRAN(lcLIBPRG,".PRG",".BAK"))
  ERASE (lcLIBPJX)
  ERASE (STRTRAN(lcLIBPJX,".PJX",".PJT"))
  ERASE AppSource.DBF
  ERASE AppSource.FPT
ENDIF

RETURN 
