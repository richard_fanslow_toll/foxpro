*
*  XXWBNEWF.PRG
*  DeveloperTool:  Has been renamed to XXWBNEWFORM.PRG

LOCAL lcNewProgram
lcNewProgram = "XXWBNewForm"
MESSAGEBOX(PROGRAM() + " has been replaced with " + lcNewProgram + ".PRG -- " + ;
           "please DO " + lcNewProgram + " instead", 48, "Please Note")
ACTIVATE WINDOW Command 
KEYBOARD "DO " + lcNewProgram
