*
*  XXDTPopC.PRG
*  DeveloperTool:  Has been renamed to XXDTGenPopCode

LOCAL lcNewProgram
lcNewProgram = "XXDTGenPopCode"
MESSAGEBOX(PROGRAM() + " has been replaced with " + lcNewProgram + ".PRG -- " + ;
           "please DO " + lcNewProgram + " instead", 48, "Please Note")
ACTIVATE WINDOW Command 
KEYBOARD "DO " + lcNewProgram
