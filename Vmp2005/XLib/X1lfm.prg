*
*  X1LFM.PRG
*  Accepts 3 parameters, returns them formatted
*  LastName, FirstName, MiddleInitial
*
*  Copyright (c) 1997-2005 Visionpace   All Rights Reserved
*                17501 East 40 Hwy., Suite 218
*                Independence, MO 64055
*                816-350-7900 
*                http://www.visionpace.com
*                http://vmpdiscussion.visionpace.com
*  Author:  Drew Speedie
*
*  PLEASE NOTE!!!
*  The value RETURNed by this program does not contain
*  any trailing blanks -- if you use X1LFM() in a
*  SQL-Select or as the ControlSource for a grid.Column,
*  you must pad it:
*    SELECT *, ;
*         PADR(X1LFM(Usr_LastName,Usr_FirstName,Usr_MiddleInitial),40) AS FullName ;
*      FROM VM!Users ;
*      INTO CURSOR C_TEMP
*
*  Usage:
*    xx = X1LFM("Gallagher","Jack","F")
*    xx = X1LFM(Usr_LastName,Usr_FirstName,Usr_MiddleInitial)
*   
PARAMETERS tcLastName, ;
           tcFirstName, ;
           tcMiddleInit
PRIVATE lcRetVal
IF TYPE("tcLastName") = "C" AND NOT EMPTY(m.tcLastName)
  lcRetVal = ALLTRIM(m.tcLastName) + ", "
 ELSE
  lcRetVal = SPACE(0) 
ENDIF
IF TYPE("tcFirstName") = "C" AND NOT EMPTY(m.tcFirstName)
  lcRetVal = lcRetVal + ALLTRIM(m.tcFirstName) + ", "
ENDIF
IF TYPE("tcMiddleInit") = "C" AND NOT EMPTY(m.tcMiddleInit)
  lcRetVal = lcRetVal + ALLTRIM(m.tcMiddleInit)
ENDIF
lcRetVal = ALLTRIM(m.lcRetVal)
#IF "VISUAL"$UPPER(VERSION())
  IF RIGHTC(m.lcRetVal,1) = ","
    lcRetVal = LEFTC(m.lcRetVal,LENC(m.lcRetVal)-1)
  ENDIF
#ELSE
  IF RIGHT(m.lcRetVal,1) = ","
    lcRetVal = LEFT(m.lcRetVal,LEN(m.lcRetVal)-1)
  ENDIF
#ENDIF
RETURN m.lcRetVal
