*  
*  X3DELTRE.PRG
*  Works like the DOS command DELTREE.
*  Pass it a directory and it will attempt to
*  delete every file and subdirectory in that tree.
*
*  RETURNs a logical value indicating whether or
*  not it was successful:
*    .T.    = tcDirectory was successfully DELTREEd
*             or
*             tcDirectory does not exist
*    .F.    = tcDirectory was not successfully DELTREEd
*             or
*             tcDirectory was the root directory
*        
*  Copyright (c) 1997-2005 Visionpace   All Rights Reserved
*                17501 East 40 Hwy., Suite 218
*                Independence, MO 64055
*                816-350-7900 
*                http://www.visionpace.com
*                http://vmpdiscussion.visionpace.com
*  Author:  Tom DeMay and Drew Speedie
*           Special thanks to Ricky Kwong
*
*
*  NOTE: This program is recursive -- it calls itself to
*        delete subdirectories
*
*  REQUIRES:
*     X2JSTFIL.PRG (in VFP 3.0b and VFP 5.0)
*
*  
*  USAGE:
*    xx = x3deltre("C:\VM\DVSTUFF")
*    IF x3deltre("C:\VM\TEST")
*      ...
*    ENDIF
*    xx = x3deltre("BACKUP")
*
*
*  lParameters 
*   tcDirectory (R) Directory to delete
*
LPARAMETERS tcDirectory

LOCAL lcDirectory
IF TYPE("tcDirectory") = "C" ;
     AND NOT ISNULL(tcDirectory) ;
     AND NOT EMPTY(tcDirectory)
  lcDirectory = UPPER(ALLTRIM(tcDirectory))
 ELSE 
  RETURN .f.
ENDIF

LOCAL lnNumofSubDirs, ;
      laSubDirs[1,5], ;
      lnNumOfFiles, ;
      laFiles[1,5], ;
      lnForNum, ;
      lcPrvDefaultDir, ;
      lcPrvOnError, ;
      llNoDirectory, ;
      llDeletedDir

lcPrvOnError    = ON("error")
lcPrvDefaultDir = SYS(5) + SYS(2003)
llNoDirectory   = .t.
llDeletedDir    = .t.
*
*  remove any trailing backslash
*
IF RIGHTC(lcDirectory,1) = "\"
  lcDirectory = LEFTC(lcDirectory,LENC(lcDirectory)-1)
ENDIF
*
*  do not allow the root directory to be DELTREEd
*
IF LENC(lcDirectory) = 2 AND RIGHTC(lcDirectory,1) = ":"
  RETURN .f.
ENDIF
*
*  CD to lcDirectory to test for its existence 
*
ON ERROR llNoDirectory = .f.
CD (lcDirectory)
ON ERROR &lcPrvOnError
*
*  if lcDirectory doesn't exist, RETURN .T. indicating 
*  that the passed lcDirectory/tree is indeed gone
*
IF llNoDirectory = .f.
  CD (lcPrvDefaultDir)
  RETURN .t.
ENDIF
*
*  recursively locate subdirectories and delete them
*
lnNumofSubDirs = ADIR(laSubDirs,SPACE(0),"D")
FOR lnForNum = 1 TO lnNumOfSubDirs
  IF EMPTY(laSubDirs[lnForNum,1]) ;
       OR ALLTRIM(laSubDirs[lnForNum,1]) == "." ;
       OR ALLTRIM(laSubDirs[lnForNum,1]) == ".."
    LOOP
  ENDIF       
  =X3DELTRE(SYS(5)+SYS(2003)+"\"+laSubDirs[lnForNum,1])
ENDFOR

*
*  erase files contained in the directories
*  
*  suppress ON ERROR because we realize that we may not 
*  be able to delete all files, we'll trap for that later
*
ON ERROR *
lnNumOfFiles = ADIR(laFiles)
FOR lnForNum = 1 TO lnNumOfFiles
  ERASE (laFiles[lnForNum,1])
ENDFOR
ON ERROR &lcPrvOnError
*
*  this next line might look a little strange --
*  why not just 
*    CD ..\   
*  ...because of a VFP anomaly that affects some
*  systems and not others...
*
CD ..\.
ON ERROR llDeletedDir = .f.
RD (lcDirectory)
*
*  attempt to CD to the directory from which we started
*  this process
*
*  if unsuccessful, we must have just deleted the 
*  original starting directory as part of the DELTREE 
*  process, in which case we stay in the parent 
*  directory
*
ON ERROR *
CD (lcPrvDefaultDir)
ON ERROR &lcPrvOnError
RETURN llDeletedDir
