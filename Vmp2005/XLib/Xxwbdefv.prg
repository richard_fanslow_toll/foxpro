*
*  XXWBDefV.PRG
*  Builder to set the DefaultValue property for
*  the PK field of tables/views to an X3GENPK()-generated
*  DefaultValue, yielding "auto-generated" PKs.
*
*  Copyright (c) 2002-2005 Visionpace   All Rights Reserved
*                17501 East 40 Hwy., Suite 218
*                Independence, MO 64055
*                816-350-7900 
*                http://www.visionpace.com
*                http://vmpdiscussion.visionpace.com
*  Author:  Drew Speedie 
*
*  The following local procedures of this .PRG can be
*  called by any external .PRG/method:
*  - SetDVAll
*  - SetDVAllTables
*  - SetDVAllViews
*  - SetDVAllLocalViews
*  - SetDVAllRemoteViews
*  - SetDVSpecificTableOrView
*  ...as we have done in:
*  -NTDVDATA.PRG, which calls the SetDVAll procedure,
*  -XXWB.VCX/frmXXWBDEFV.SaveAction(), which calls
*   the SetDVSpecificTableOrView procedure
*  -XXWBAPPS.VCX/frmWizAppSetup.FinishAction_CreateDatabase(),
*   which calls the SetDVAll procedure
*
*  Each of the above local procedures contain header
*  comments with usage instructions.
*
*  FOR BEST RESULTS, when you call any of the above 
*  procedures, the database should either be in the
*  current directory, or in the VFP path.
*
*  PLEASE NOTE that this routine creates a PUBLIC
*  goDBCSvc object -- if you call any of the above
*  Set...() functions, you will likely need to 
*  RELEASE goDBCSvc manually.
*
*  This utility DOES NOT set the DefaultValue if the 
*  indicated PK field is a VFP (free or contained) table
*  AutoInc field.
*

ERASE xxwbdefv.txt

SET ASSERTS ON

LOCAL lcNotify, lcExclusive
lcNotify = SET("NOTIFY")
SET NOTIFY OFF
lcExclusive = SET("Exclusive")
SET EXCLUSIVE ON

IF NOT LoadDBCSvc()
  SET NOTIFY &lcNotify
  SET EXCLUSIVE &lcExclusive
  RETURN
ENDIF

*
*  first, save current ADATABASES() information for
*  reset when we're done, because I'm going to
*  close all open databases
*
local lcSetDatabase, laDBCs[1], xx, lcDBC, lcExclusive
lcSetDatabase = SET("DATABASE")
IF ADATABASES(laDBCs) > 0
  FOR xx = 1 to alen(laDBCs, 1)
    set database to (laDBCs[xx, 1])
    lcDBC = laDBCs[xx, 1]
    laDBCs[xx, 1] = ISEXCLUSIVE(lcDBC, 2)
    close database
  ENDFOR
ENDIF

*
*  call the XXWB.VCX/frmXXWBDefV (modal) dialog
*
LOCAL loDialog
SET CLASSLIB TO XXWB.VCX ADDITIVE
loDialog = CREATEOBJECT("frmXXWBDefV")
IF VARTYPE(loDialog) = "O"
  *KEYBOARD "C:\VMP\VMNT\DATA\VM.DBC" PLAIN clear
  loDialog.Show()
ENDIF

goDBCSvc = .NULL.
RELEASE goDBCSvc  &&& see LoadDBCSvc local procedure

SET NOTIFY &lcNotify
SET EXCLUSIVE &lcExclusive

*
*  open all the databases that were open when we started
*
IF TYPE("laDBCs[1, 2]") = "C"
  FOR xx = 1 to ALEN(laDBCs, 1)
    lcExclusive = IIF(laDBCs[xx, 1], "EXCLUSIVE", "SHARED")
    open database (laDBCs[xx, 2]) &lcExclusive.
  ENDFOR
ENDIF
set database to &lcSetDatabase.

ERASE xxwbdefv.txt

RETURN




PROCEDURE LoadDBCSvc
IF VARTYPE(goDBCSvc) = "O" AND X7CPEDIG(goDBCSvc,"cusDBCSvc")
  RETURN .t.
ENDIF
PUBLIC goDBCSvc
goDBCSvc = NEWOBJECT("cusDBCSvc","XXFWLIBS.VCX")
IF VARTYPE(goDBCSvc) # "O"
  MESSAGEBOX("XXWBDEFV.PRG/LoadDBCSvc() is unable to instantiate " + ;
             "XXFWLIBS.VCX/cusDBCSvc to goDBCSvc, for " + ;
             "use by set DefaultValue code.",48,"Please Note")
  RETURN .f.
ENDIF
RETURN .t.




#IF .f.
SetDVAll
	SetDVAllTables
		SetDVSpecificTableOrView
	SetDVAllViews
		SetDVSpecificTableOrView
		
SetDVAllTables
	SetDVSpecificTableOrView

SetDVAllViews
	SetDVSpecificTableOrView

SetDVAllLocalViews
	SetDVAllViews
		SetDVSpecificTableOrView
	
SetDVAllRemoteViews
	SetDVAllViews
		SetDVSpecificTableOrView

SetDVSpecificTableOrView		
#ENDIF





PROCEDURE SetDVAll
*
*  this procedure sets the DefaultValue property of all
*  tables and (local and remote) views in the passed 
*  database to the X3GENPK()-based DefaultValue
*
*  this procedure can be called from an external .PRG
*  or method:
*    DO SetDVAll in XXWBDEFV.PRG with <Database>,.t.
*
*  pass tlShowMessage as .T. to display a form to the
*  user with the results of this action
*
*  if you pass tlShowMessage as .F., then XXWBDEFV.TXT
*  IS NOT ERASED, and is available to the calling code
*  to then call the ShowResults() procedure:
*    DO ShowResults IN XXWBDEFV
*  whereupon the ShowResults procedure erases XXWBDEFV.TXT
*  see XXWB.VCX/frmXXWBDEFV.SaveAction() for an example
*    XXDTES("XXWB.VCX","each time SetDVSpecificTableOrView","frmXXWBDEFV","SaveAction")
*      modi class frmXXWBDEFV of xxwb method SaveAction
*
LPARAMETERS tcDatabase, tlShowMessage
ERASE xxwbdefv.txt
SET ASSERTS ON 
ASSERT VARTYPE(tcDatabase) = "C" AND !EMPTY(tcDataBase) ;
     MESSAGE "tcDatabase is invalid"
LOCAL lcDatabase
lcDatabase = UPPER(ALLTRIM(tcDatabase))
IF RIGHTC(lcDatabase,4) # ".DBC"
  lcDatabase = lcDatabase + ".DBC"
ENDIF
ASSERT FILE(lcDatabase) MESSAGE lcDatabase + " does not exist"
IF !LoadDBCSvc()
  RETURN .f.
ENDIF
IF !goDBCSvc.OpenData(lcDatabase,.t.)
  MESSAGEBOX("Unable to open " + lcDatabase + ".",16,program())
  RETURN .f.
ENDIF
LOCAL llRetVal, llSuccess
llSuccess = SetDVAllTables(tcDatabase)
IF !llSuccess
  llRetVal = .f.
ENDIF
llSuccess = SetDVAllViews(tcDatabase)
IF !llSuccess AND llRetVal
  llRetVal = .f.
ENDIF
IF VARTYPE(tlShowMessage) = "L" AND tlShowMessage
  ShowResults()
*!*	 ELSE
*!*	  ERASE xxwbdefv.txt
ENDIF
RETURN llRetVal




PROCEDURE SetDVAllTables
*
*  this procedure sets the DefaultValue property of all
*  tables in the passed 
*  database to the X3GENPK()-based DefaultValue
*
*  this procedure can be called from an external .PRG
*  or method:
*    DO SetDVAllTables in XXWBDEFV.PRG with <Database>,.t.
*
*  pass tlShowMessage as .T. to display a form to the
*  user with the results of this action
*
*  if you pass tlShowMessage as .F., then XXWBDEFV.TXT
*  IS NOT ERASED, and is available to the calling code
*  to then call the ShowResults() procedure:
*    DO ShowResults IN XXWBDEFV
*  whereupon the ShowResults procedure erases XXWBDEFV.TXT
*  see XXWB.VCX/frmXXWBDEFV.SaveAction() for an example
*    XXDTES("XXWB.VCX","each time SetDVSpecificTableOrView","frmXXWBDEFV","SaveAction")
*      modi class frmXXWBDEFV of xxwb method SaveAction
*
LPARAMETERS tcDatabase, tlShowMessage
LOCAL llMessageHere
IF !X2PSTACK("SetDVAll ")
  ERASE xxwbdefv.txt
  llMessageHere = .t.
ENDIF
SET ASSERTS ON 
ASSERT VARTYPE(tcDatabase) = "C" AND !EMPTY(tcDataBase) ;
     MESSAGE "tcDatabase is invalid"
LOCAL lcDatabase
lcDatabase = UPPER(ALLTRIM(tcDatabase))
IF RIGHTC(lcDatabase,4) # ".DBC"
  lcDatabase = lcDatabase + ".DBC"
ENDIF
ASSERT FILE(lcDatabase) MESSAGE lcDatabase + " does not exist"
IF !LoadDBCSvc()
  RETURN .f.
ENDIF
IF !goDBCSvc.OpenData(lcDatabase,.t.)
  MESSAGEBOX("Unable to open " + lcDatabase + ".",16,program())
  RETURN .f.
ENDIF
LOCAL llRetVal, laTables[1], xx, llSuccess
llRetVal = .t.
IF ADBOBJECTS(laTables,"Table") > 0
  ASORT(laTables)
  FOR xx = 1 TO ALEN(laTables,1)
    llSuccess = SetDVSpecificTableOrView(lcDatabase,laTables[xx])
    IF !llSuccess AND llRetVal
      llRetVal = .f.
    ENDIF
  ENDFOR
 ELSE
  LogResult(tcDatabase + " does not contain any tables!")
ENDIF
IF llMessageHere 
  IF VARTYPE(tlShowMessage) = "L" AND tlShowMessage
    ShowResults()
*!*	   ELSE
*!*	    ERASE xxwbdefv.txt
  ENDIF
ENDIF
RETURN llRetVal




PROCEDURE SetDVAllViews
*
*  this procedure sets the DefaultValue property of all
*  (local and remote) views in the passed 
*  database to the X3GENPK()-based DefaultValue
*
*  this procedure can be called from an external .PRG
*  or method:
*    DO SetDVAllViews in XXWBDEFV.PRG with <Database>,"A",.t.
*
*  pass tlShowMessage as .T. to display a form to the
*  user with the results of this action
*
*  tcLR defaults to "A" (all views), but can be passed
*  as "L" to just do local views, or "R" to just do
*  remote views
*
*  if you pass tlShowMessage as .F., then XXWBDEFV.TXT
*  IS NOT ERASED, and is available to the calling code
*  to then call the ShowResults() procedure:
*    DO ShowResults IN XXWBDEFV
*  whereupon the ShowResults procedure erases XXWBDEFV.TXT
*  see XXWB.VCX/frmXXWBDEFV.SaveAction() for an example
*    XXDTES("XXWB.VCX","each time SetDVSpecificTableOrView","frmXXWBDEFV","SaveAction")
*      modi class frmXXWBDEFV of xxwb method SaveAction
*
LPARAMETERS tcDatabase, tcLR, tlShowMessage
LOCAL llMessageHere
IF !X2PSTACK("SetDVAll ")
  ERASE xxwbdefv.txt
  llMessageHere = .t.
ENDIF
SET ASSERTS ON 
ASSERT VARTYPE(tcDatabase) = "C" AND !EMPTY(tcDataBase) ;
     MESSAGE "tcDatabase is invalid"
ASSERT PCOUNT() = 1 ;
     OR VARTYPE(tcLR) = "C" AND INLIST(UPPER(tcLR),"L","R","A") ;
     MESSAGE 'If a tcLR specifier is passed, it must be "L", "R", or "A"'
LOCAL lcDatabase
lcDatabase = UPPER(ALLTRIM(tcDatabase))
IF RIGHTC(lcDatabase,4) # ".DBC"
  lcDatabase = lcDatabase + ".DBC"
ENDIF
ASSERT FILE(lcDatabase) MESSAGE lcDatabase + " does not exist"
IF !LoadDBCSvc()
  RETURN .f.
ENDIF
IF !goDBCSvc.OpenData(lcDatabase,.t.)
  MESSAGEBOX("Unable to open " + lcDatabase + ".",16,program())
  RETURN .f.
ENDIF
LOCAL lcLR
DO CASE
  ***********************************
  CASE PCOUNT() = 1
  ***********************************
    lcLR = "A"  &&& All
  ***********************************
  CASE UPPER(tcLR) = "L"
  ***********************************
    lcLR = "L"  &&& Local
  ***********************************
  CASE UPPER(tcLR) = "R"
  ***********************************
    lcLR = "R"  &&& Remote
ENDCASE
LOCAL llRetVal, laViews[1], xx, llSuccess
llRetVal = .t.
IF ADBOBJECTS(laViews,"View") > 0
  ASORT(laViews)
  FOR xx = 1 TO ALEN(laViews,1)
    IF lcLR = "A" ;
         OR (lcLR = "L" AND DBGETPROP(laViews[xx],"View","SourceType") = 1) ;
         OR (lcLR = "R" AND DBGETPROP(laViews[xx],"View","SourceType") = 2)
      llSuccess = SetDVSpecificTableOrView(lcDatabase,laViews[xx])
      IF !llSuccess AND llRetVal
        llRetVal = .f.
      ENDIF
    ENDIF
  ENDFOR
 ELSE
  LogResult(tcDatabase + " does not contain any views!")
ENDIF
IF llMessageHere 
  IF VARTYPE(tlShowMessage) = "L" AND tlShowMessage
    ShowResults()
*!*	   ELSE
*!*	    ERASE xxwbdefv.txt
  ENDIF
ENDIF
RETURN llRetVal




PROCEDURE SetDVAllLocalViews
*
*  this method is not called from any of the other
*  local procedures here, but can be called from
*  an external .PRG/method:
*
*    DO SetDVAllLocalViews in XXWBDEFV.PRG with <Database>,.t.
*
*  pass tlShowMessage as .T. to display a form to the
*  user with the results of this action
*
LPARAMETERS tcDatabase, tlShowMessage
ERASE xxwbdefv.txt
LOCAL llRetVal
llRetVal = SetDVAllViews(tcDatabase, "L")
IF VARTYPE(tlShowMessage) = "L" AND tlShowMessage
  ShowResults()
ENDIF
RETURN llRetVal


PROCEDURE SetDVAllRemoteViews
*
*  this method is not called from any of the other
*  local procedures here, but can be called from
*  an external .PRG/method:
*    DO SetDVAllRemoteViews in XXWBDEFV.PRG with <Database>,.t.
*
*  pass tlShowMessage as .T. to display a form to the
*  user with the results of this action
*
LPARAMETERS tcDatabase, tlShowMessage
ERASE xxwbdefv.txt
LOCAL llRetVal
llRetVal = SetDVAllViews(tcDatabase, "R")
IF VARTYPE(tlShowMessage) = "L" AND tlShowMessage
  ShowResults()
ENDIF
RETURN llRetVal




PROCEDURE SetDVSpecificTableOrView
*
*  this procedure sets the DefaultValue property of one
*  specific table or (local/remote) view in the passed 
*  database to the X3GENPK()-based DefaultValue
*
*  this procedure can be called from an external .PRG
*  or method:
*    do SetDVSpecificTableOrView in XXWBDEFV ;
*      with <Database>,<TableOrViewName>,.t.
*
*  pass tlShowMessage as .T. to display a form to the
*  user with the results of this action
*
*  if you pass tlShowMessage as .F., then XXWBDEFV.TXT
*  IS NOT ERASED, and is available to the calling code
*  to then call the ShowResults() procedure:
*    DO ShowResults IN XXWBDEFV
*  whereupon the ShowResults procedure erases XXWBDEFV.TXT
*  see XXWB.VCX/frmXXWBDEFV.SaveAction() for an example
*    XXDTES("XXWB.VCX","each time SetDVSpecificTableOrView","frmXXWBDEFV","SaveAction")
*      modi class frmXXWBDEFV of xxwb method SaveAction
*
LPARAMETERS tcDatabase, tcTVName, tlShowMessage
LOCAL llMessageHere
IF X2PSTACK("SetDVAllTables ") OR X2PSTACK("SetDVAllViews ")
  llMessageHere = .f.
 ELSE
  ERASE xxwbdefv.txt
  llMessageHere = .t.
ENDIF
SET ASSERTS ON 
ASSERT VARTYPE(tcDatabase) = "C" AND !EMPTY(tcDataBase) ;
     MESSAGE "tcDatabase is invalid"
ASSERT VARTYPE(tcTVName) = "C" AND !EMPTY(tcTVName) ;
     MESSAGE "tcTVName is invalid"
tcTVName = UPPER(ALLTRIM(tcTVName))
LOCAL lcDatabase
lcDatabase = UPPER(ALLTRIM(tcDatabase))
IF RIGHTC(lcDatabase,4) # ".DBC"
  lcDatabase = lcDatabase + ".DBC"
ENDIF
ASSERT FILE(lcDatabase) MESSAGE lcDatabase + " does not exist"
IF !LoadDBCSvc()
  RETURN .f.
ENDIF
IF !goDBCSvc.OpenData(lcDatabase,.t.)
  MESSAGEBOX("Unable to open " + lcDatabase + ".",16,program())
  RETURN .f.
ENDIF
IF !INDBC(tcTVName,"TABLE") AND !INDBC(tcTVName,"VIEW")
  MESSAGEBOX(tcTVName + " is not contained in " + tcDatabase + ".DBC", ;
             48,"Please Note")
  RETURN .f.
ENDIF
LOCAL llRetVal
IF INDBC(tcTVName,"TABLE")
  llRetVal = SetDefaultValueForTable(tcTVName)
 ELSE
  llRetVal = SetDefaultValueForView(tcTVName)
ENDIF
IF llMessageHere 
  IF VARTYPE(tlShowMessage) = "L" AND tlShowMessage
    ShowResults()
*!*	   ELSE
*!*	    ERASE xxwbdefv.txt
  ENDIF
ENDIF
RETURN llRetVal





PROCEDURE SetDefaultValueForTable
*
*  this procedure CANNOT be called from anywhere other
*  than the above procedures
*
LPARAMETERS tcTableName
IF "GENERATEPK" $ UPPER(ALLTRIM(tcTableName)) 
  LogResult(tcTableName + ":  The PK for " + tcTableName + " is never auto/X3GENPK()-generated")
  RETURN .f.
ENDIF
LOCAL lcPK, lcDV
lcPK = DBGETPROP(tcTableName,"Table","PrimaryKey")
IF EMPTY(lcPK)
  LogResult(tcTableName + ":  Does not have a Primary Key tag specified")
  RETURN .f.
ENDIF
LOCAL llUsed, lnSelect, llExcl, llAutoInc, llRetVal
lnSelect = SELECT(0)
llUsed = USED(tcTableName)
IF llUsed
  llExcl = ISEXCLUSIVE(tcTableName,1)
ENDIF
IF NOT goDBCSvc.OpenTable(tcTableName, ;
                          .f., ;
                          .t., ;
                          .f., ;
                          SET("DATABASE"))
  LogResult(tcTableName + ":  Unable to open " + tcTableName + " to gather needed PK info")
  SELECT (lnSelect)
  RETURN .f.
ENDIF
SELECT (tcTableName)
lcPK = X3PKEXPR(tcTableName)
*
*  DEVNOTE
*  add code to handle concatenated fields - last one is
*  considered ID field
*
IF OCCURS(",",lcPK) > 0 ;
     OR OCCURS("(",lcPK) > 0 ;
     OR OCCURS("+",lcPK) > 0
  LogResult(tcTableName + ":  PK expression " + lcPK + " is not a single field")
  RETURN .f.
ENDIF

llAutoInc = X8IsAutoInc(lcPK,ALIAS())

IF NOT llAutoInc
  lcDV = [X3GENPK("] + tcTableName + [","] + JUSTSTEM(SET("DATABASE")) + ["]
  IF VARTYPE(EVALUATE(lcPK)) = "C"
    lcDV = [PADL(] + lcDV + [),LENC(] + lcPK + [),"0")]
   ELSE
    lcDV = lcDV + [)]
  ENDIF
  LOCAL llError, lcOnError
  lcOnError = ON("ERROR")
  ON ERROR llError = .t.
  ALTER TABLE (tcTableName) alter COLUMN (lcPK) set DEFAULT &lcDV 
  ON ERROR &lcOnError
ENDIF

IF llUsed
  IF NOT llExcl
    goDBCSvc.OpenTable(tcTableName, ;
                       .f., ;
                       .f., ;
                       .f., ;
                       SET("DATABASE"))
  ENDIF
 ELSE
  USE IN (tcTableName)
ENDIF
SELECT (lnSelect)
DO CASE
  CASE llAutoInc
    LogResult(tcTableName + ":  " + lcPK + " is an AutoInc field, so no DefaultValue has been set")
    llRetVal = .t.
  CASE llError
    LOCAL laError[1]
    AERROR(laError)
    LogResult(tcTableName + ":  Unable to ALTER TABLE " + tcTableName + " ALTER COLUMN " + lcPK + " SET DEFAULT " + lcDV + [, because "] + laError[2] + [" (AERROR(2))])
    llRetVal = .f.
  OTHERWISE
    LogResult(tcTableName + ":  Successfully set " + lcPK + " DefaultValue to " + CHR(13) + CHR(10) + lcDV)
    llRetVal = .t.
ENDCASE
RETURN llRetVal




PROCEDURE SetDefaultValueForView
*
*  this procedure CANNOT be called from anywhere other
*  than the above procedures
*
LPARAMETERS tcViewName
IF "GENERATEPK" $ UPPER(ALLTRIM(tcViewName)) 
  LogResult(tcViewName + ":  The PK for " + tcViewName + " is never auto/X3GENPK()-generated")
  RETURN .f.
ENDIF
IF NOT DBGETPROP(tcViewName,"VIEW","SENDUPDATES")
  LogResult(tcViewName + ": is not udpateable (SendUpdates = .F.)")
  RETURN .f.
ENDIF
LOCAL llUsed, lnSelect
lnSelect = SELECT(0)
llUsed = USED(tcViewName)
IF NOT llUsed ;
     AND NOT goDBCSvc.OpenTable(tcViewName, ;
                                .f., ;
                                .f., ;
                                .f., ;
                                SET("DATABASE"))
  LogResult(tcViewName + ":  Unable to open " + tcViewName + " to gather needed PK info")
  SELECT (lnSelect)
  RETURN .f.
ENDIF
SELECT (tcViewName)
LOCAL xx, lcPK, llMoreThanOnePKField, lcBTable, llAutoInc, ;
      llRetVal, lcDV
llMoreThanOnePKField = .f.
FOR xx = 1 TO FCOUNT()
  IF DBGETPROP(tcViewName+"."+FIELD(xx),"FIELD","KeyField")
    IF NOT EMPTY(lcPK)
      llMoreThanOnePKField = .t.
      EXIT 
    ENDIF
    lcPK = FIELD(xx)
  ENDIF
ENDFOR
IF llMoreThanOnePKField
  LogResult(tcViewName + ":  has more than one field where the KeyField property is set to .T.")
  IF NOT llUsed
    USE IN (tcViewName)
  ENDIF
  SELECT (lnSelect)
  RETURN .f.
ENDIF
IF EMPTY(lcPK)
  LogResult(tcViewName + ":  none of the fields have the KeyField property set to .T.")
  IF NOT llUsed
    USE IN (tcViewName)
  ENDIF
  SELECT (lnSelect)
  RETURN .f.
ENDIF

lcBTable = X8BTABLE(tcViewName+"."+lcPK,JUSTSTEM(SET("DATABASE")))

IF CURSORGETPROP("SourceType",tcViewName) = 2   &&& remote view
  llAutoInc = .f.
 ELSE
  *  local view
  IF USED(lcBTable)
    SELECT (lcBTable)
   ELSE
    USE (lcBTable) IN 0 ALIAS XXWBDEFVTemp
    SELECT XXWBDEFVTemp
  ENDIF
  llAutoInc = X8IsAutoInc(lcPK,ALIAS())
  USE IN SELECT("XXWBDEFVTemp")
  SELECT (tcViewName)
ENDIF

IF llAutoInc
  *
  *  nothing to do
  *
 ELSE
  IF CURSORGETPROP("SourceType",tcViewName) = 2   &&& remote view
    lcDV = [X3GENPK("] + lcBTable + [","] + JUSTSTEM(SET("DATABASE")) + [",.f.,.f.,.f.,"] + tcViewName + "." + lcPK + ["]
   ELSE
    lcDV = [X3GENPK("] + lcBTable + [","] + JUSTSTEM(SET("DATABASE")) + ["]
  ENDIF
  IF VARTYPE(EVALUATE(lcPK)) = "C"
    lcDV = [PADL(] + lcDV + [),LENC(] + lcPK + [),"0")]
   ELSE
    lcDV = lcDV + [)]
  ENDIF
ENDIF   &&& llAutoInc

USE IN (tcViewName)
SELECT (lnSelect)

DO CASE
  CASE llAutoInc
    LogResult(tcViewName + ":  " + lcPK + " is an AutoInc field in the base table " + lcBTable + ", so no DefaultValue has been set")
    llRetVal = .t.
  CASE DBSETPROP(tcViewName+"."+lcPK,"Field","DefaultValue",lcDV)
    LogResult(tcViewName + ":  Successfully set " + lcPK + " DefaultValue to " + CHR(13) + CHR(10) + lcDV)
    llRetVal = .t.
  OTHERWISE
    LogResult(tcViewName + [:  Unable to DBSETPROP("] + tcViewName + [.] + lcPK + [","FIELD","DefaultValue",] + lcDV + [)])
    llRetVal = .f.
ENDCASE

RETURN llRetVal

 
 
PROCEDURE LogResult
*
*  this procedure is called from the above 2 procedures
*
LPARAMETERS tcString
LOCAL lcCRLF
lcCRLF = CHR(13) + CHR(10)
STRTOFILE(tcString + lcCRLF + lcCRLF,"XXWBDEFV.TXT",.t.)
RETURN
 

 
PROCEDURE ShowResults
*
*  this procedure is called from several of the above
*  procedures, and can be called from external code
*
LPARAMETERS tcResultsFile
LOCAL loEditBox, loForm, lcResultsFile, loJunkForm
IF VARTYPE(tcResultsFile) = "C" AND !EMPTY(tcResultsFile)
  lcResultsFile = tcResultsFile
 ELSE
  lcResultsFile = "XXWBDEFV.TXT"
ENDIF
SET CLASSLIB TO XXFWUTIL.VCX ADDITIVE 
loForm = CREATEOBJECT("frmShowInfo", ;
                      FILETOSTR(lcResultsFile), ;
                      .f., ;
                      "XXWBDEFV() results", ;
                      .f., ;
                      .f., ;
                      _Screen.Height - 60, ;
                      MIN(_Screen.Width-60,700))
loForm.Show()
ERASE (lcResultsFile)
ERASE XXWBDEFV.TXT
RETURN
