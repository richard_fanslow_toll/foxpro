*
*  XXPROCDT.PRG
*  This is a file of procedures (procedure file)
*  for use by VMP developer tools, wizards, builders,
*  etc. -- any VMP code that needs to call a
*  .PRG-based procedure
*
*  
*  Copyright (c) 2000-2005 Visionpace   All Rights Reserved
*                17501 East 40 Hwy., Suite 218
*                Independence, MO 64055
*                816-350-7900 
*                http://www.visionpace.com
*                http://vmpdiscussion.visionpace.com
*
* 

*  Thanks go to Eric Lendvai who came up with this little
*  goodie that was, in turn, passed on to me by Art
*  Bergquist.  I have modified it just slightly <g> from 
*  its original 2 lines to suppress its behavior in 
*  undesirable circumstances.
*  PLEASE NOTE that this function is never used starting
*  in VMP 2003, but since A.PRG maintains backward-
*  compatibility to previous versions of VFP, and
*  calls this function in VFP 6, we have left this
*  function here.
*******************************************************
*******************************************************
PROCEDURE DrillDown
*******************************************************
*******************************************************
*
*  Author:  Drew Speedie
*
* 1- position the mouse over a container
*      PageFrame/Page
*      Grid/Column
*      OptionGroup
*      Container/Control
* 2- press {F3} 
* 3- you're there!
*
*  
*  This code is called from the A.PRG that ships
*  with VMP, but by being here, you can call it
*  even if you aren't using A.PRG
*
IF EMPTY(WONTOP())
  *
  *  disable this feature if we're at the Command Window
  *  and/or neither Form nor Class Designer is open
  *
  RETURN 
ENDIF
IF type("oForms.BaseClass") = "C" ;
     AND type("oUser.BaseClass") = "C" ;
     AND type("oLib.BaseClass") = "C" ;
     AND type("oToolbars.BaseClass") = "C"
  *
  *  disable this feature when running VMP forms
  *  standalone at the command window
  *
  return
ENDIF  
local loMouseObj
loMouseObj = sys(1270)
IF type("loMouseObj") = "L"
  *
  *  no object currently under the mouse
  *
  return
ENDIF
IF upper(loMouseObj.Name) = "SCREEN"
  *
  *  mouse is currently positioned over _Screen
  *
  return
ENDIF
local array laSelObj[1]
aselobj(laSelObj,1)
IF type("laSelObj[1].BaseClass") = "C"
  laSelObj[1] = upper(laSelObj[1].BaseClass)
 ELSE
  laSelObj[1] = "JUNK"
ENDIF
IF !inlist(upper(loMouseObj.BaseClass), ;
           "PAGEFRAME", ;
           "PAGE", ;
           "GRID", ;
           "COLUMN", ;
           "HEADER", ;
           "CONTAINER", ;
           "CONTROL", ;
           "OPTIONGROUP", ;
           "OPTIONBUTTON") ;
     AND laSelObj[1] = "CONTAINER"
  *
  *  we only want this behavior for containers
  *  we're drilling down into
  *
  return
ENDIF

* If (1) developer pressed <F3> somewhere on the grid and 
*    (2) if we're already editing the grid,
* then exit now since we're already at the lowest level.
IF INLIST(UPPER(loMouseObj.BaseClass),"COLUMN","HEADER") ;
     AND laSelObj[1] = "GRID"
  return
ENDIF

local llExtraMouseClick 

IF upper(loMouseObj.BaseClass) == laSelObj[1]
  *
  *  if we've drilled down as far as possible,
  *  don't do anything, since the "Edit" option
  *  has been removed from the RightClick menu,
  *  and we'll hit the "Builder" option instead
  *
  return
ENDIF
IF upper(loMouseObj.BaseClass) = "PAGEFRAME" ;
    AND laSelObj[1] = "PAGE"
  *
  *  Pageframe is selected and mouse is currently
  *  positioned over the Tab/Caption of a page
  *  that isn't the current page
  *
  mouse click left
  return
ENDIF

IF upper(loMouseObj.BaseClass) = "PAGEFRAME" ;
    AND laSelObj[1] = "FORM"
  *
  *  you've invoked this while the mouse 
  *  is over the Caption/Tab of a pageframe
  *  that is not the frontmost page
  *
  *  we'll execute an extra mouse click to
  *  not only drill down into that pageframe,
  *  but we'll also make that page active
  *
  llExtraMouseClick = .t.
ENDIF
*
*  activate the RightClick menu
*
MOUSE CLICK RIGHT
*
*  Arrow down to the 'Edit' option and press
*  <Enter> thus running the 'Edit' option
*
DO CASE
  CASE version(3) # "00" OR upper(version()) = "VISUAL FOXPRO 05"
    *
    *  VFP 5.0 doesn't have a hotkey on the RightClick
    *  menu for the Edit option...
    *
    KEYBOARD "{DnArrow}{DnArrow}{DnArrow}{Enter}" PLAIN CLEAR

  CASE version(3) = "00"                    && English / VFP 6.0
    keyboard "{E}" plain clear

  CASE version(3) = "49"                    && German / VFP 6.0
    keyboard "{B}" plain clear
ENDCASE
IF llExtraMouseClick
  mouse click left
ENDIF
return





*******************************************************
*******************************************************
PROCEDURE PadPositionFromPadName
*******************************************************
*******************************************************
*
*  Converts the passed Menu Pad name (character string) to a
*  numeric value (#) indicating the (menu) position in which
*  that Menu Pad exists (decremented by 1 to offset the control
*  menu).
*
*  This procedure is called from 
*  XXTOOLS.VCX/frsXXDTSearch.ModifyFile() (XXDTSearch.PRG)
* 
*
*  Author:  Art Bergquist and Franz M�ller
*
*  lParameters
*    tcMenuPadName (R) Name of Menu Pad for which to determine its position
*
LPARAMETERS tcMenuPadName
LOCAL lnMenuPad, llFoundMenuPad

* The Menu Pad Name must be a character string
IF TYPE("tcMenuPadName") # "C"
  RETURN ""
ENDIF

llFoundMenuPad = .F.            && Assume we will *not* find the menu pad

FOR lnMenuPad = 1 TO CNTPAD('_MSYSMENU')
  IF GETPAD('_MSYSMENU', lnMenuPad) == tcMenuPadName
    llFoundMenuPad = .T.
    EXIT
  ENDIF
ENDFOR         &&* lnMenuPad = 1 TO CNTPAD('_MSYSMENU')

IF NOT llFoundMenuPad
  RETURN ""
ENDIF

*
*  If the FOXTOOLS.FLL library is not already open, load
*  it because we need to access some of its functions.
*
IF NOT x2setlib("FOXTOOLS")
  * FOXTOOLS.FLL could not be successfully installed
  DO x3winmsg WITH "The FOXTOOLS.FLL library must be in your VFP path."
ENDIF

*
*  note that the following code requires FoxTools.FLL
*  functions
*
external file foxtools.fll
EXTERNAL PROCEDURE _WONTOP
EXTERNAL PROCEDURE _WTITLE


LOCAL lnWindowHandle, lnOldPosP, lcWindowOnTop, lcFrontMostWindow, lnOffSet
lnWindowHandle    = _WONTOP()
lcFrontMostWindow = _WTITLE(lnWindowHandle)
lcWindowOnTop     = WONTOP()

*
*  It is really a pain to determine if a Designer window is maximized as
*  _WONTOP() returns an empty string if a designer is open.  As it seems that 
*  every object in the designer has its own window handle and that one of these
*  objects is on top, we have to find the *parent* window (i.e. the designer
*  window).  We therefore use the WONTOP()-function, which returns the right
*  window name, but not in the right case.
*
IF EMPTY(lcFrontMostWindow) ;
	AND (   "FORM DESIGNER"     $ lcWindowOnTop ;		&& English
  	     OR "FORMULAR-DESIGNER" $ lcWindowOnTop ;		&& German
  	     OR "CLASS DESIGNER"    $ lcWindowOnTop ;		&& English
  	     OR "KLASSEN-DESIGNER"  $ lcWindowOnTop ;	    && German
  	     OR UPPER("dise�ador de formluarios") $ lcWindowOnTop ;
  	     OR UPPER("dise�ador de clases") $ lcWindowOnTop)

  LOCAL lcFindWindow
  lcFindWindow = LOWER(lcWindowOnTop)
  DO CASE
  	CASE version(3) = "00"
  		lcFindWindow = STRTRAN(lcFindWindow, "form designer",     "Form Designer")
  		lcFindWindow = STRTRAN(lcFindWindow, "class designer",    "Class Designer")

  	CASE version(3) = "49"   &&& German
  		lcFindWindow = STRTRAN(lcFindWindow, "formular-designer", "Formular-Designer")
  		lcFindWindow = STRTRAN(lcFindWindow, "klassen-designer",  "Klassen-Designer")

    CASE VERSION(3) = "34"   &&&  Spanish
        lcFindWindow = Strtran(lcFindWindow, "dise�ador de formluarios", "Dise�ador de Formluarios")
        lcFindWindow = Strtran(lcFindWindow, "dise�ador de clases",  "Dise�ador de Clases")

  ENDCASE
  lcFindWindow = STRTRAN(lcFindWindow, "read only", "Read Only")
  lnWindowHandle = _WFindTitl(lcFindWindow)
  IF lnWindowHandle = 0
  	* We have not found the parent window handle
	DO x3winmsg WITH ;
		"Sorry, we could not determine the window handle of the" + CHR(13) + ;
		"open Form Designer, thus something can go wrong now " + ;
		"(especially if the window is maximized)!"
	lnWindowHandle = _WOnTop()				 
  ENDIF
ENDIF

lnOffSet = 1		&& Default, if a window is not maximized ...

* Is the current window maximized?
IF     _Screen.Width            = _WWidthP(lnWindowHandle)  ;
   AND _Screen.Height           = _WHeightP(lnWindowHandle) ;
   AND _WLeftP(lnWindowHandle)  = SYSMETRIC(4) * -1         ;
   AND _WRightP(lnWindowHandle) = _WWidthP(lnWindowHandle) + SYSMETRIC(4)

  * It appears that the current window is maximized, but let's make sure by
  * trying to move the window (since maximized windows do not move)
  lnOldPosP = _WLeftP(lnWindowHandle)
  _WMove(lnWindowHandle, lnOldPosP + 1, _WTopP(lnWindowHandle))
  IF lnOldPosP = _WLeftP(lnWindowHandle)
    lnOffSet = 0        && The current window does not move; it must, thus,
  ENDIF                 && be maximized and it therefore has a Control menu.
ENDIF

RETURN "{F10}"										   + ;  && Activate the System Menu
       REPLICATE("{RightArrow}", lnMenuPad - lnOffSet) + ;  && Go to requested menu pad
       "{Enter}"                                     		&& and Select it



*******************************************************
*******************************************************
PROCEDURE ChangeFont
*******************************************************
*******************************************************
*
* 1) Position the mouse in one of the following:
*		- Command Window
*		- method code editor
*		- program code editor
*       - BROWSE (editing) window
* 2) Press {Ctrl+F3}
* 3) Font is changed to 'Courier New/Bold/14' for you
*    by default; pass in any other desired font/fontstyle/fontsize
*
*  Author:  Art Bergquist and Franz M�ller and Drew Speedie
*
*  lParameters
*    tcFontName (O) Name of the font you want
*                     Defaults to "Courier New"
*   tnFontStyle (O) Style you want for the font:
*                       0 = Normal
*                       1 = Bold
*                       2 = Italic
*                       3 = Bold Italic
*                     Defaults to 1-Bold
*    tnFontSize (0) Size you want for the font
*   ...optionally, pass the t..SnippetEditor set of
*      parameters to indicate that if the current
*      editor is the Method Snippet Editor, to use those
*      font attributes instead
*
*
LPARAMETERS tcFontName, tnFontStyle, tnFontSize, ;
            tcFontNameSnippetEditor, tnFontStyleSnippetEditor, tnFontSizeSnippetEditor
LOCAL lnParameters
lnParameters = PARAMETERS()

*
*  If the FOXTOOLS.FLL library is not already open, load
*  it because we need to access some of its functions.
*
IF NOT X2SETLIB("FOXTOOLS")
  *  FOXTOOLS.FLL could not be successfully installed
  X3WINMSG("The FOXTOOLS.FLL library must be in your VFP path.")
ENDIF

LOCAL lnWindowHandle, lcChangeFontName, lnChangeFontStyle, ;
      lnChangeFontSize

* Default to "Courier New" if the Font Name was not supplied
IF m.lnParameters < 1	;
    OR NOT VARTYPE(m.tcFontName) = "C" ;
    OR EMPTY(m.tcFontName)
  lcChangeFontName = "Courier New"
 ELSE
  lcChangeFontName = m.tcFontName
ENDIF

* Default to "Bold" if the Font Style was not supplied
* N.B. Available Font styles are: 
*    0 = Normal
*    1 = Bold
*    2 = Italic
*    3 = Bold Italic
*
IF m.lnParameters < 2 ;
    OR NOT VARTYPE(m.tnFontStyle) = "N"	;
    OR NOT BETWEEN(m.tnFontStyle, 0, 3)
  lnChangeFontStyle = 1 && Bold
ELSE
  lnChangeFontStyle = m.tnFontStyle
ENDIF

* Default Font Size if not supplied
IF m.lnParameters < 3				;
    OR NOT VARTYPE(m.tnFontSize) = "N"	;
    OR EMPTY(m.tnFontSize)
  DO CASE
    CASE SYSMETRIC(1) > 1599
      lnChangeFontSize = 16
    CASE SYSMETRIC(1) > 800
      lnChangeFontSize = 12
    CASE SYSMETRIC(1) > 640
      lnChangeFontSize = 11
    OTHERWISE
      lnChangeFontSize = 10
  ENDCASE
 ELSE
  lnChangeFontSize = m.tnFontSize
ENDIF

EXTERNAL PROCEDURE _WONTOP
EXTERNAL PROCEDURE _WTITLE

* Find window
lnWindowHandle = _WOnTop()
lcWindow       = _WTitle(m.lnWindowHandle)

IF m.lnWindowHandle > 0		   && Window found
  LOCAL lnSession
  LOCAL ARRAY laEdEnv[25]
  * Read the current editor Environment
  * (1 is returned if called in a Command or Editor session, otherwise 0 is returned)
  lnSession = _EdGetEnv(m.lnWindowHandle, @m.laEdEnv)

  LOCAL lcWindowType
  DO CASE
    CASE m.lnSession = 0
      lcWindowType = "BROWSE"
    CASE EMPTY(m.lcWindow)
      lcWindowType = SPACE(0)
    CASE INLIST(m.lcWindow, "Command", "Befehl")      && 'Befehl' is German for 'Command'    
      lcWindowType = "COMMAND"
    CASE OCCURS(".",m.lcWindow) = 0
      lcWindowType = "X"
    CASE INLIST(UPPER(JUSTEXT(m.lcWindow)), ;
                "PRG", ;
                "TXT", ;
                "QPR")
      lcWindowType = "EDITOR"
    OTHERWISE
      lcWindowType = "METHOD"
  ENDCASE

  DO CASE
    ****************************************************
    CASE m.lcWindowType = "BROWSE"
    ****************************************************
      * BROWSE editor (window for a table) ... not a Command or Editor session
      LOCAL lcKeyboardString, lcChangeFont, lcChangeFontStyle
      * Check if the Table menu pad exists ...
      lcKeyboardString = PadPositionFromPadName("_MTABLE")
      IF NOT EMPTY(m.lcKeyboardString)
        *  FM I don't know why we need a space before we send the FontName
        *  to the keyboard buffer, but otherwise it is possible that the
        *  FontName is not fully inserted (thus ignoring the 1st letter)
        lcChangeFontName = "{Spacebar}" + m.lcChangeFontName 

        DO CASE
          CASE VERSION(3) = "00"
            DO CASE
              CASE m.lnChangeFontStyle = 0
                lcChangeFontStyle = "Normal"

              CASE m.lnChangeFontStyle = 1
                lcChangeFontStyle = "Bold"

              CASE m.lnChangeFontStyle = 2
                lcChangeFontStyle = "Italic"

              CASE m.lnChangeFontStyle = 3
                lcChangeFontStyle = "Bold Italic"
            ENDCASE

          CASE VERSION(3) = "49"
            DO CASE
              CASE m.lnChangeFontStyle = 0
                lcChangeFontStyle = "Standard"    && German for "Normal"

              CASE m.lnChangeFontStyle = 1
                lcChangeFontStyle = "Fett"        && German for "Bold"

              CASE m.lnChangeFontStyle = 2
                lcChangeFontStyle = "Kursiv"      && German for "Italic"

              CASE m.lnChangeFontStyle = 3
                lcChangeFontStyle = "Fett Kursiv" && German for "Bold Italic"
            ENDCASE

          CASE VERSION(3) = "34"
            Do Case
              Case m.lnChangeFontStyle = 0
                lcChangeFontStyle = "Normal"    && Spanish for "Normal"
              Case m.lnChangeFontStyle = 1
                lcChangeFontStyle = "Negrita"   && Spanish for "Bold"
              Case m.lnChangeFontStyle = 2
                lcChangeFontStyle = "Cursiva"   && Spanish for "Italic"
              Case m.lnChangeFontStyle = 3
                lcChangeFontStyle = "Negrita Cursiva" && Spanish for "Bold Italic"
            Endcase

        ENDCASE
        lcChangeFontSize = ALLTRIM(STR(m.lnChangeFontSize))

        lcChangeFont = m.lcChangeFontName  + ;
               CHR(9)            + ;                           && CHR(9) = {Tab}
			   m.lcChangeFontStyle + ;
			   CHR(9)            + ;                           && CHR(9) = {Tab}
			   m.lcChangeFontSize  + ;  
			   "{Enter}"               && "<Enter>" on <Ok> thus closing Font dialog

        * Since the 'Table' menu pad exists, it is most likely a BROWSE window
        DO CASE                  && Now let's change its font ...
          CASE VERSION(3) = "00"               && English
            lcChangeFont = "F" + m.lcChangeFont  && Select the 'Font' menu pad option

          CASE VERSION(3) = "49"	           && German
            lcChangeFont = "A" + m.lcChangeFont  && Select the 'Schriftart' menu pad option

          CASE VERSION(3) = "34"               && Spanish
            lcChangeFont = "F" + m.lcChangeFont  && Selecciona la opcion 'Fuente' del menu
 
          OTHERWISE
            lcChangeFont = "{DnArrow}{DnArrow}{Enter}" + m.lcChangeFont
        ENDCASE

        KEYBOARD m.lcKeyboardString +  ;    && 'Table' menu pad
                 m.lcChangeFont        ;    && Change to the new font
          PLAIN CLEAR
      ENDIF

    ****************************************************
    CASE EMPTY(m.lcWindowType)
    ****************************************************
      * A window *other* than an editor window (.PRG or method), Command window
      * or BROWSE window (table editor).  Might be one of the following:
      *    - Property Sheet
      *    - Class Designer
      *    - Database Designer
      *    - Form Designer
      *    - Label Designer
      *    - Query Designer
      *    - View Designer
      *    - Report Designer
      *    - "Printing..." (currently printing a report)
      *
      * in which case we do nothing.
      *
    ****************************************************
    CASE m.lcWindowType = "METHOD" ;
         AND m.lnParameters > 3
    ****************************************************
      *
      *  method snippet editor, optional 4,5,6
      *  parameters passed
      *
      IF VARTYPE(m.tcFontNameSnippetEditor) = "C" ;
           AND NOT EMPTY(m.tcFontNameSnippetEditor)
        lcChangeFontName = m.tcFontNameSnippetEditor
      ENDIF
      IF VARTYPE(m.tnFontStyleSnippetEditor) = "N"
        lnChangeFontStyle = m.tnFontStyleSnippetEditor
      ENDIF
      IF VARTYPE(m.tnFontSizeSnippetEditor) = "N"
        lnChnageFontSize = m.tnFontSizeSnippetEditor 
      ENDIF       

      laEdEnv[22] = m.lcChangeFontName
      laEdEnv[23] = m.lnChangeFontSize
      laEdEnv[24] = m.lnChangeFontStyle

      * Write new Font settings to the (Editor) Environment
      =_EdSetEnv(m.lnWindowHandle, @m.laEdEnv)

    ****************************************************
    OTHERWISE
    ****************************************************
      *
      *  parameters 4,5,6 have not been passed
      *
      IF INLIST(m.lcWindowType,"X","EDITOR","COMMAND","METHOD") ;
         OR ;
         (ALEN(laEdEnv) = 25 AND laEdEnv[25] > 0)
      
        laEdEnv[22] = m.lcChangeFontName
        laEdEnv[23] = m.lnChangeFontSize
        laEdEnv[24] = m.lnChangeFontStyle

        * Write new Font settings to the (Editor) Environment
        =_EdSetEnv(m.lnWindowHandle, @m.laEdEnv)

      ENDIF
  ENDCASE
ENDIF
RETURN


*******************************************************
*******************************************************
PROCEDURE SavePJXs
*******************************************************
*******************************************************
ERASE SavePJXs.TXT  &&& ensure we start fresh
IF _VFP.Projects.Count < 1
  RETURN 
ENDIF
LOCAL lnStack
FOR lnStack = 1 TO _VFP.Projects.Count
  STRTOFILE(_VFP.Projects[lnStack].Name+CHR(13),"SavePJXs.TXT",.t.)
ENDFOR
RETURN 



*******************************************************
*******************************************************
PROCEDURE RestorePJXs
*******************************************************
*******************************************************
IF NOT FILE("SavePJXs.TXT")
  RETURN 
ENDIF
LOCAL laLines[1]
ALINES(laLines,FILETOSTR("SavePJXs.TXT"))
ERASE XXWBNEWF.TXT
LOCAL xx
FOR xx = 1 TO ALEN(laLines,1)
  MODIFY PROJECT (laLines[xx]) NOWAIT 
ENDFOR
RETURN 



*******************************************************
*******************************************************
PROCEDURE VMPRequirementsAndAssumptions
*******************************************************
*******************************************************
IF set("COMPATIBLE") # "OFF"  
  messagebox("SET COMPATIBLE is currently set to " + set("COMPATIBLE") + "!!  " + ;
             "As described in the " + ;
             "VMP documentation (Requirements and Assumptions section), " + ;
             "that is a MAJOR No-No!" + chr(13) + chr(13) + ;
             "Since SET COMPATIBLE must be set to OFF (the VFP default " + ;
             "is OFF, the proper setting) for the code in " + ;
             "VMP tools, wizards, builders, and applications " + ;
             "to perform properly, execution will be aborted when you " + ;
             "select <OK> below." + chr(13) + chr(13) + ;
             "Be sure that the 'dBase compatibility' checkbox on the " + ;
             "General tab of the Tools/Options... dialog is UN-checked -- " + ;
             "the un-checked position indicates that SET COMPATIBLE is OFF.  " + ;
             "Be sure to select the <Set as Default> button in Tools/Options... " + ;
             "before you leave it by selecting the <OK> button -- otherwise " + ;
             "your SET COMPATIBLE OFF setting will only be effected in the " + ;
             "current VFP session and will not carry over to each subsequent " + ;
             "VFP session.", ;
             16, ;
             "SET COMPATIBLE is not set to OFF!!")
  return .f.
ENDIF
IF set("DELETED") # "ON" 
  messagebox("SET DELETED is currently set to " + set("DELETED") + "!!  " + ;
             "As described in the " + ;
             "VMP documentation (Requirements and Assumptions section), " + ;
             "VMP assumes that SET DELETED is ON." + chr(13) + chr(13) + ;
             "Since SET DELETED must be set to ON to ensure that " + ;
             "VMP code in tools, wizards, builders, and applications performs reliably, " + ;
             "execution will be aborted " + ;
             "when you select <OK> below." + chr(13) + chr(13) + ;
             "Be sure that the 'Ignore deleted records' checkbox on the " + ;
             "Data tab of the Tools/Options... dialog is CHECKED -- " + ;
             "the checked position indicates that SET DELETED is ON.  " + ;
             "Be sure to select the <Set as Default> button in Tools/Options... " + ;
             "before you leave it by selecting the <OK> button -- otherwise " + ;
             "your SET DELETED ON setting will only be effected in the " + ;
             "current VFP session and will not carry over to each subsequent " + ;
             "VFP session.", ;
             16, ;
             "SET DELETED is not set to ON!!")
  return .f.
ENDIF
IF set("MULTILOCKS") # "ON" 
  messagebox("SET MULTILOCKS is currently set to " + set("MULTILOCKS") + "!!  " + ;
             "As described in the " + ;
             "VMP documentation (Requirements and Assumptions section), " + ;
             "VMP assumes that SET MULTILOCKS is ON." + chr(13) + chr(13) + ;
             "Since SET MULTILOCKS must be set to ON to ensure that " + ;
             "VMP code in tools, wizards, builders, and applications performs reliably, " + ;
             "execution will be aborted " + ;
             "when you select <OK> below." + chr(13) + chr(13) + ;
             "Be sure that the 'Multiple record locks' checkbox in the " + ;
             "'Locking and buffering' section of the " + ;
             "Data tab of the Tools/Options... dialog is CHECKED -- " + ;
             "the checked position indicates that SET MULTILOCKS is ON.  " + ;
             "Be sure to select the <Set as Default> button in Tools/Options... " + ;
             "before you leave it by selecting the <OK> button -- otherwise " + ;
             "your SET MULTILOCKS ON setting will only be effected in the " + ;
             "current VFP session and will not carry over to each subsequent " + ;
             "VFP session.", ;
             16, ;
             "SET MULTILOCKS is not set to ON!!")
  return .f.
ENDIF
IF set("EXACT") # "OFF"  
  messagebox("SET EXACT is currently set to " + set("EXACT") + "!!  " + ;
             "As described in the " + ;
             "VMP documentation (Requirements and Assumptions section), " + ;
             "VMP assumes that SET EXACT is OFF." + chr(13) + chr(13) + ;
             "Since SET EXACT must be set to OFF to ensure that " + ;
             "VMP code in tools, wizards, builders, and applications performs reliably, " + ;
             "execution will be aborted " + ;
             "when you select <OK> below." + chr(13) + chr(13) + ;
             "Be sure that the 'SET EXACT on' checkbox in the " + ;
             "'String comparisons' section of the " + ;
             "Data tab of the Tools/Options... dialog is UN-checked -- " + ;
             "the un-checked position indicates that SET EXACT is OFF.  " + ;
             "Be sure to select the <Set as Default> button in Tools/Options... " + ;
             "before you leave it by selecting the <OK> button -- otherwise " + ;
             "your SET EXACT OFF setting will only be effected in the " + ;
             "current VFP session and will not carry over to each subsequent " + ;
             "VFP session.", ;
             16, ;
             "SET EXACT is not set to OFF!!")
  return .f.
ENDIF
IF OCCURS(",",SET("PATH")) > 0 
  MESSAGEBOX("The current SET PATH includes at least one " + ;
             "folder/directory that contains a comma.  " + ;
             "As described in the " + ;
             "VMP documentation (Requirements and Assumptions section), " + ;
             "VMP does not support folders/directories " + ;
             "in the SET PATH that include commas, because " + ;
             "the FILE() and FULLPATH() functions don't " + ;
             "work with folders that use a comma.", ;
             16, ;
             "SET PATH includes at least one comma!!")
  return .f.
ENDIF
IF ON("ERROR") == "*"
  *
  *  XXDTES("XXFWNT.VCX","ON ERROR *","ctrNTBase","Init")
  *  XXDTES("XXFW.VCX","ON ERROR *","frmBase","Init")
  *
  MESSAGEBOX("ON ERROR is currently set to " + ;
             CHR(13) + CHR(13) + ;
             "ON ERROR *" + ;
             CHR(13) + CHR(13) + ;
             "That is a BAD thing!  It results in VFP simply " + ;
             "skipping an invalid line of code, causing bad/unexpected " + ;
             "behaviors and making it very difficult to debug " + ;
             "the resulting problems." + ;
             CHR(13) + CHR(13) + ;
             "You should IMMEDIATELY determine where your " + ;
             "application issues an ON ERROR * for temporary " + ;
             "error-suppression, without resetting ON ERROR " + ;
             "immediately afterwards.  EVEN BETTER, replace " + ;
             "that ON ERROR * with a TRY..CATCH block." + ;
             CHR(13) + CHR(13) + ;
             "To help find the code:" + CHR(13) + ;
             [DO XXDTSearch with "ON ERROR *"], ;             
             16, ;
             "ON ERROR * !!")
  RETURN .f.
ENDIF
LOCAL lcGenMenuX, lcGenMenu
lcGenMenuX = FULLPATH("GenMenuX.PRG")
lcGenMenu = ADDBS(HOME(1)) + "GenMenu.PRG"
DO CASE
  CASE EMPTY(_GenMenu)
    MESSAGEBOX("The VFP system variable _GenMenu is not set, " + ;
               "which means that attempts to generate .MPR menu " + ;
               "files in the Project Manager or by the BUILD PROJECT " + ;
               "command will fail." + ;
               CHR(13) + CHR(10) + CHR(13) + CHR(13) + ;
               "You should set _GenMenu to indicate the menu-generation " + ;
               "program you want to use, which is typically one of these, " + ;
               "but could be a menu-generation program of your own:" + ;
               CHR(13) + CHR(10) + ;
               SPACE(2) + m.lcGenMenuX + ;
               CHR(13) + CHR(10) + ;
               SPACE(2) + m.lcGenMenu, ;
               48,"_GenMenu is not set!")
    RETURN .f.
  CASE NOT FILE(_GenMenu)
    MESSAGEBOX("The file indicated in the VFP system variable _GenMenu:" + ;
               CHR(13) + CHR(10) + ;
               SPACE(2) + _Builder + ;
               CHR(13) + CHR(10) + ;
               "is invalid, either because it does not exist, " + ;
               "or because it is not in the VFP path at the moment.  " + ;
               CHR(13) + CHR(10) + CHR(13) + CHR(13) + ;
               "You should set _GenMenu to indicate the menu-generation " + ;
               "program you want to use, which is typically one of these, " + ;
               "but could be a menu-generation program of your own:" + ;
               CHR(13) + CHR(10) + ;
               SPACE(2) + m.lcGenMenuX + ;
               CHR(13) + CHR(10) + ;
               SPACE(2) + m.lcGenMenu, ;
               48,"_GenMenu is invalid!")
    RETURN .f.
ENDCASE

RETURN .t.




*-------------------------------------------------------------------
* Procedure: TogglePS_All_Default
*   Purpose: Toggle between All and Default Properties (in VFP 7,
*            even works from the Command Window! [for _Screen ...])
*   Credits: Adapted by Art Bergquist and Drew Speedie from the
*            "Switch Between All and Default Properties Via a Keystroke" 
*            tip by Sean Lee
*            on p. 39 of the December 2001 issue of FoxPro Advisor.
*  Example: On Key Label F8 Do ('NonDef') In ASetup
*-------------------------------------------------------------------
PROCEDURE TogglePS_All_Default
IF !WEXIST("Command") AND !WEXIST("Form Designer") AND !WEXIST("Class Designer")
  return
ENDIF
IF WONTOP("REPORT DESIGNER") ;
     OR WONTOP("LABEL DESIGNER") ;
     OR WONTOP("QUERY")
  return
ENDIF
LOCAL llError, lcOldError, lnMRow, lnMCol
lnMRow = MROW("",3)
lnMCol = MCOL("",3)
lcOldError = ON("ERROR")
ON ERROR llError = .t. 
IF !WVISIBLE("Properties") OR !WONTOP("Properties")
  *  don't toggle anything -- just activate the Properties
  *  Sheet and make it active, which is pretty much the
  *  default VFP behavior on hitting any keystroke while
  *  focus is in the Form/Class Designers
  ACTIVATE WINDOW "Properties" top
  ON ERROR &lcOldError  
  RETURN
ENDIF
*
*  the Properties Sheet is currently the top/active 
*  window -- toggle the All/Default properties
*
ACTIVATE WINDOW "Properties" top
ON ERROR &lcOldError 
IF !llError 
  MOUSE RIGHT CLICK AT 1,1 WINDOW "Properties" PIXELS &&& activate the shortcut menu
  KEYBOARD "N" PLAIN CLEAR &&& toggle the "Non-Default Properties Only" option
  DOEVENTS
  MOUSE DRAG TO lnMRow, lnMCol PIXELS
*!*	 ELSE
*!*	  Wait Window 'No Property Sheet Available!' TimeOut 3
ENDIF
RETURN





*******************************************************
*******************************************************
PROCEDURE VMPMenu
*******************************************************
*******************************************************
*
*  install a VMP menu at the end of _MSYSMENU
*
*  add additional options and/or completely redo/delete 
*  this menu in ASETUP.PRG or ASETUP1.PRG
*  
LOCAL lcConfigFile
lcConfigFile = SYS(2019)
IF NOT FILE("APRG.ICO")
  IF MESSAGEBOX("XXPROCDT.PRG is unable to locate VMP graphics files.  " + ;
  				  "You need to make sure that either \VMP\Graphics\XP " + ;
			      "or \VMP\Graphics\PreXP are in the VFP path." + ;
			      CHR(13) + CHR(13) + ;
			      "The VMP menu has not been installed to the system menu." + ;
			      "  Would you like to edit your configuration file (" + ;
			      lcConfigFile + ") and re-try?", 4 + 32,"Please Note") = 6
     MODIFY COMMAND (lcConfigFile)
  ENDIF
  RETURN
ENDIF

LOCAL lcHelpFile, lcHelpFileSkipFor
lcHelpFile = GetVMPHelpFile()
lcHelpFileSkipFor = [SKIP FOR !FILE("] + lcHelpFile + [")]

DEFINE PAD VMPPad OF _MSYSMENU PROMPT "V\<MP" KEY ALT+M, ""
ON PAD VMPPad OF _MSYSMENU ACTIVATE POPUP VMPPopup

DEFINE POPUP VMPPopup MARGIN RELATIVE 

DEFINE BAR 1 of VMPPopup PROMPT "VMP build: " + XXVMPBuild()  
DEFINE BAR 2 of VMPPopup PROMPT "do A" PICTURE "APrg.ICO" SKIP FOR NOT FILE("A.PRG")
DEFINE BAR 3 of VMPPopup PROMPT "View all inherited code"
DEFINE BAR 4 of VMPPopup PROMPT "Insert all inherited code at top of current method"
DEFINE BAR 5 OF VMPPopup PROMPT "XXDTFileOpen.prg"
DEFINE BAR 6 OF VMPPopup PROMPT "XXDTDIR.prg"
IF FILE("FXU.PRG")
  DEFINE BAR 7 OF VMPPopup PROMPT "\<FoxUnit"
ENDIF
DEFINE BAR 10 of VMPPopup PROMPT "\-"
DEFINE BAR 11 of VMPPopup PROMPT "do XXDT.prg Developer Tools" PICTURE "DevTools.ICO"  &&& TOols.ico
DEFINE BAR 12 of VMPPopup PROMPT "do XXDTSearch.prg"  
DEFINE BAR 13 of VMPPopup PROMPT "do XXDTExplicitPEM.prg"
DEFINE BAR 14 of VMPPopup PROMPT "do XXDTHack.prg"  
DEFINE BAR 15 of VMPPopup PROMPT "do X5WinExplorer.prg"  
DEFINE BAR 16 OF VMPPopup PROMPT "SET PATH, CLASSLIB, etc."
DEFINE BAR 17 OF VMPPopup PROMPT "Work Areas Viewer" KEY Shift+F11, "Shift+F11"
DEFINE BAR 20 of VMPPopup PROMPT "\-"
DEFINE BAR 21 of VMPPopup PROMPT "do XXWB Wizards/Builders" PICTURE "Wizard.BMP" SKIP FOR NOT FILE("XXWB.PRG")
DEFINE BAR 22 of VMPPopup PROMPT "do XXWBNewForm.prg" SKIP FOR NOT FILE("XXWBNEWF.PRG")
DEFINE BAR 30 of VMPPopup PROMPT "\-"
DEFINE BAR 31 of VMPPopup PROMPT "VMP \<Help" PICTURE "Help.BMP" &lcHelpFileSkipFor KEY Alt+F1, "Alt+F1"
DEFINE BAR 32 OF VMPPopup PROMPT "Calendar..." PICTURE "Calendar.BMP" SKIP FOR NOT X8SETCLS("XXFWCAL.VCX")
DEFINE BAR 33 of VMPPopup PROMPT "http://www.visionpace.com/vmpsite" PICTURE "Internet.ICO"
DEFINE BAR 34 of VMPPopup PROMPT "http://vmpdiscussion.visionpace.com" PICTURE "Internet.ICO"
DEFINE BAR 35 OF VMPPopup PROMPT "Check the VMP web site for the current build" PICTURE IIF(FILE("NT.ICO"),"NT.ICO",IIF(FILE("VM.ICO"),"VM.ICO","Internet.ICO"))
DEFINE BAR 40 of VMPPopup PROMPT "\-"
DEFINE BAR 41 OF VMPPopup PROMPT "Create VMPDiagnostic.TXT"
DEFINE BAR 42 OF VMPPopup PROMPT "Erase VMPDiagnostic.TXT"
DEFINE BAR 43 OF VMPPopup PROMPT "Erase XXLOG.TXT diagnostic log file"
DEFINE BAR 44 OF VMPPopup PROMPT "View XXLOG.TXT diagnostic log file"

ON SELECTION BAR 1 of VMPPopup XXVMPBuild(.t.)
ON SELECTION BAR 2 of VMPPopup do ("A.PRG") 
ON SELECTION BAR 3 of VMPPopup do ViewAllInheritedCodeFromDesigner in XXPROCDT   &&& SuperCls
ON SELECTION BAR 4 of VMPPopup do InsertAllInheritedCodeAtTopOfMethod in XXPROCDT 
ON SELECTION BAR 5 of VMPPopup do XXDTFileOpen
ON SELECTION BAR 6 of VMPPopup do XXDTDir
IF FILE("FXU.PRG")
  ON SELECTION BAR 7 of VMPPopup do FXU
ENDIF
*************
ON SELECTION BAR 11 of VMPPopup do ("XXDT.PRG")
ON SELECTION BAR 12 of VMPPopup do ("XXDTSearch.PRG")
ON SELECTION BAR 13 of VMPPopup do ("XXDTExplicitPEM.PRG")
ON SELECTION BAR 14 of VMPPopup do ("XXDTHack.PRG") 
ON SELECTION BAR 15 of VMPPopup do ("X5WinExplorer.PRG")
ON SELECTION BAR 16 OF VMPPopup do GetSetInfo in XXPROCDT with X6SAF()
ON SELECTION BAR 17 OF VMPPopup do CallWorkAreasViewer in XXPROCDT
*************
ON SELECTION BAR 21 of VMPPopup do ("XXWB.PRG")
ON SELECTION BAR 22 of VMPPopup do ("XXWBNewForm.PRG")
*************
ON SELECTION BAR 31 of VMPPopup do VMPMenuVMPHelp in XXPROCDT 
ON SELECTION BAR 32 OF VMPPopup do VMPMenuCalendar in XXPROCDT 
ON SELECTION BAR 33 of VMPPopup do X8ShellExecute with "http://www.visionpace.com/vmpsite"
ON SELECTION BAR 34 of VMPPopup do X8ShellExecute with "http://vmpdiscussion.visionpace.com"
ON SELECTION BAR 35 OF VMPPopup do CompareYourBuildToCurrentBuild in XXPROCDT
*************
ON SELECTION BAR 41 OF VMPPopup do CreateVMPDiagnostic in XXLOGGER
ON SELECTION BAR 42 OF VMPPopup do EraseVMPDiagnostic in XXLOGGER
ON SELECTION BAR 43 OF VMPPopup do EraseLog in XXLOGGER
ON SELECTION BAR 44 OF VMPPopup do ViewLog in XXLOGGER

*  add a DO xxMAIN.PRG bar for every "xxMAIN.PRG" file in the 
*  current folder
LOCAL laFiles[1], xx, lcJunk, lnFirstBar, lnBarNum
lnFirstBar = 60
lnBarNum = lnFirstBar
IF ADIR(laFiles,"*.PRG") > 0
  FOR xx = 1 TO ALEN(laFiles,1)
    IF LENC(laFiles[xx,1]) < 20 AND RIGHTC(laFiles[xx,1],8) = "MAIN.PRG"
      IF lnBarNum = lnFirstBar
        DEFINE BAR ( lnFirstBar ) OF VMPPopup PROMPT "\-"
      ENDIF
      lnBarNum = lnBarNum + 1
      DEFINE BAR ( lnBarNum ) OF VMPPopup PROMPT "do " + laFiles[xx,1] PICTURE "Programs.ICO"
      ON SELECTION BAR ( lnBarNum ) OF VMPPopup do VMPMenuDOCommand in XXPROCDT with PROMPT()
    ENDIF
  ENDFOR
ENDIF

RETURN



*******************************************************
*******************************************************
PROCEDURE VMPMenuDOCommand
*******************************************************
*******************************************************
LPARAMETERS tcCommand
ACTIVATE WINDOW command
KEYBOARD tcCommand+"{ENTER}" PLAIN clear
RETURN



*******************************************************
*******************************************************
PROCEDURE VMPMenuVMPHelp
*******************************************************
*******************************************************
LOCAL lcHelpFile
lcHelpFile = GetVMPHelpFile()
Application.Help(lcHelpFile,,"Introduction")
SET HELP TO 
RETURN



*******************************************************
*******************************************************
PROCEDURE VMPMenuCalendar
*******************************************************
*******************************************************
LPARAMETERS tcStyle, tcAppConfig, tcCalendarFormClass
RELEASE goCalendar
PUBLIC goCalendar
LOCAL lcStyle, lcClass, lcAppConfig
lcClass = SPACE(0)
IF VARTYPE(tcStyle) = "C" 
  lcStyle = tcStyle
 ELSE
  lcStyle = .NULL.
ENDIF
IF VARTYPE(tcAppConfig) = "C" AND NOT EMPTY(tcAppConfig)
  lcAppConfig = UPPER(ALLTRIM(tcAppConfig))
 ELSE
  lcAppConfig = SPACE(0)
ENDIF
IF NOT EMPTY(lcAppConfig)
  TRY
  USE (lcAppConfig) AGAIN IN 0 ALIAS AppConfig_VMPMenuCalendar
  CATCH
  ENDTRY
  IF USED("AppConfig_VMPMenuCalendar")
    SELECT AppConfig_VMPMenuCalendar
    LOCATE FOR UPPER(Ap_Item) = UPPER("CalendarForm")
    lcClass = UPPER(ALLTRIM(Ap_ItemVal))
    IF ISNULL(lcStyle)
      LOCATE FOR UPPER(Ap_Item) = UPPER("CalendarStyle")
      lcStyle = ALLTRIM(Ap_ItemVal)
      IF VARTYPE(lcStyle) = "N"
        lcStyle = TRANSFORM(INT(lcStyle))
      ENDIF
    ENDIF
    USE IN AppConfig_VMPMenuCalendar
  ENDIF
ENDIF
IF EMPTY(lcClass) ;
     AND VARTYPE(tcCalendarFormClass) = "C" ;
     AND NOT EMPTY(tcCalendarFormClass)
  lcClass = UPPER(ALLTRIM(tcCalendarFormClass))     
ENDIF
IF EMPTY(lcClass)
  lcClass = "frmCalendar,XXFWCAL.VCX"
ENDIF
lcClass = X8SETCPR(lcClass)
IF ISNULL(lcClass)
  RETURN
ENDIF
goCalendar = CREATEOBJECT(lcClass)
LOCAL loCalendar
FOR EACH loCalendar IN goCalendar.Controls
  IF UPPER(loCalendar.BaseClass) = "CONTAINER" ;
       AND NOT VARTYPE(loCalendar.icStyle) = "U"
    EXIT 
  ENDIF
ENDFOR       
IF NOT ISNULL(lcStyle) AND ;
     (ISNULL(loCalendar.icStyle) OR NOT loCalendar.icStyle == lcStyle)
  loCalendar.icStyle = lcStyle
  loCalendar.SetupStyle()
ENDIF
loCalendar = .NULL.
goCalendar.Top = (_Screen.Height-goCalendar.Height)/5
goCalendar.Show(2)  
RETURN



*******************************************************
*******************************************************
PROCEDURE GetVMPHelpFile
*******************************************************
*******************************************************
LOCAL lcHelpFile
lcHelpFile = "VMPHELP.CHM"
IF FILE(lcHelpFile)
  lcHelpFile = FULLPATH("VMPHELP.CHM")
 ELSE
  LOCAL lcDOCSPath
  lcDOCSPath = ADDBS(JUSTPATH(FULLPATH("XXPROCDT.PRG")))
  lcDOCSPath = LEFTC(lcDOCSPath,RATC("\",lcDOCSPath,2)) 
  lcDOCSPath = lcDOCSPath + "DOCS\"
  IF DIRECTORY(lcDOCSPath) AND FILE(lcDOCSPath+"VMPHELP.CHM")
    lcHelpFile = lcDOCSPath + "VMPHELP.CHM"
  ENDIF
ENDIF
RETURN lcHelpFile



*******************************************************
*******************************************************
PROCEDURE GetSetInfo
*******************************************************
*******************************************************
LPARAMETERS toForm AS Form
LOCAL loGetSetInfo AS cusGetSetInfo OF XXFWUTIL.VCX, ;
      loForm AS frmShowInfo OF XXFWUTIL.VCX, ;
      loGetSetInfoDelegate AS GetSetInfoDelegate OF XXPROCDT.PRG, ;
      lcAlias, lnSelect, lnLeft, lnTop, lnWidth, lnHeight, ;
      lcItem, lcFontName, lnFontSize, llFontBold, llFontItalic
lnSelect = SELECT(0)
IF OpenXXWBPrefWithAlias("GetSetInfo_XXWBPREF")
  SELECT GetSetInfo_XXWBPREF
  LOCATE FOR UPPER(ALLTRIM(Prf_Item)) = UPPER("frmShowInfo_GetSetInfoPosition")
  IF FOUND()
    lcItem = ALLTRIM(Prf_ItemVl)
    lnLeft = VAL(GETWORDNUM(lcItem,1,","))
    lnTop = VAL(GETWORDNUM(lcItem,2,","))
    lnWidth = VAL(GETWORDNUM(lcItem,3,","))
    lnHeight = VAL(GETWORDNUM(lcItem,4,","))
  ENDIF
  LOCATE FOR UPPER(ALLTRIM(Prf_Item)) = UPPER("frmShowInfo_GetSetInfoFont")
  IF FOUND()
    lcItem = ALLTRIM(Prf_ItemVl)
    lcFontName = GETWORDNUM(lcItem,1,",")
    lnFontSize = VAL(GETWORDNUM(lcItem,2,","))
    llFontBold = EVALUATE(GETWORDNUM(lcItem,3,","))
    llFontItalic = EVALUATE(GETWORDNUM(lcItem,4,","))
  ENDIF
ENDIF  
IF VARTYPE(toForm) = "O" AND PEMSTATUS(toForm,"DataSessionID",5)
  SET DATASESSION TO (toForm.DataSessionID)
ENDIF
lcAlias = "GetSetInfo" + SYS(2015)
CREATE CURSOR (lcAlias) (TheText M)
SELECT (lcAlias)
APPEND BLANK 
SET CLASSLIB TO XXFWUTIL.VCX ADDITIVE 
loGetSetInfo = CREATEOBJECT("cusGetSetInfo")
replace TheText WITH loGetSetInfo.GetSetInfo()
SET CLASSLIB TO XXFWUTIL.VCX ADDITIVE 
loForm = CREATEOBJECT("frmShowInfo", ;
                      lcAlias+".TheText", ;
                      .f., ;
                      "SETtings in DataSession " + TRANSFORM(SET("DATASESSION")), ;
                      .f., ;
                      .f., ;
                      .f., ;
                      .f., ;
                      .f., ;
                      .f., ;
                      .f., ;
                      .f., ;
                      SET("DATASESSION"))
IF VARTYPE(lnLeft) = "N"
  loForm.Move(lnLeft,lnTop,lnWidth,lnHeight)
ENDIF  
IF VARTYPE(lcFontName) = "C" AND NOT EMPTY(lcFontName)
  loForm.edtShowInfo.FontName = lcFontName
  loForm.edtShowInfo.FontSize = lnFontSize
  loForm.edtShowInfo.FontBold = llFontBold
  loForm.edtShowInfo.FontItalic = llFontItalic
ENDIF
loForm.Resize()
loGetSetInfoDelegate = CREATEOBJECT("GetSetInfoDelegate")
*
*  BINDEVENT() here to give us a hook into frmShowInfo::Destroy(),
*  so we can save settings to XXWBPREF
*
BINDEVENT(loForm,"Destroy",loGetSetInfoDelegate,"SaveSettings")
loForm.Show()
USE IN (lcAlias)
IF VARTYPE(toForm) = "O" AND PEMSTATUS(toForm,"DataSessionID",5)
  SET DATASESSION TO 1
ENDIF
USE IN SELECT("GetSetInfo_XXWBPREF")
SELECT (lnSelect)
RETURN

DEFINE CLASS GetSetInfoDelegate AS Custom
PROCEDURE SaveSettings
IF USED("GetSetInfo_XXWBPREF")
  LOCAL laEvents[1], loForm
  AEVENTS(laEvents,0)
  loForm = laEvents[1]
  SELECT GetSetInfo_XXWBPREF
  LOCATE FOR UPPER(ALLTRIM(Prf_Item)) = UPPER("frmShowInfo_GetSetInfoPosition")
  IF NOT FOUND()
    APPEND BLANK 
    replace Prf_Item WITH "frmShowInfo_GetSetInfoPosition", ;
            Prf_ItemDT WITH "C" ;
            IN GetSetInfo_XXWBPREF
  ENDIF    
  replace Prf_ItemVl WITH TRANSFORM(loForm.Left) + "," + ;
                          TRANSFORM(loForm.Top) + "," + ;
                          TRANSFORM(loForm.Width) + "," + ;
                          TRANSFORM(loForm.Height) ;
       IN GetSetInfo_XXWBPREF
  LOCATE FOR UPPER(ALLTRIM(Prf_Item)) = UPPER("frmShowInfo_GetSetInfoFont")
  IF NOT FOUND()
    APPEND BLANK 
    replace Prf_Item WITH "frmShowInfo_GetSetInfoFont", ;
            Prf_ItemDT WITH "C" ;
            IN GetSetInfo_XXWBPREF
  ENDIF    
  replace Prf_ItemVl WITH loForm.edtShowInfo.FontName + "," + ;
                          TRANSFORM(loForm.edtShowInfo.FontSize) + "," + ;
                          TRANSFORM(loForm.edtShowInfo.FontBold) + "," + ;
                          TRANSFORM(loForm.edtShowInfo.FontItalic) ;
       IN GetSetInfo_XXWBPREF
ENDIF
ENDPROC
ENDDEFINE





*******************************************************
*******************************************************
PROCEDURE OpenXXWBPREFWithAlias
*******************************************************
*******************************************************
*
*  USE XXWBPREF IN 0 AGAIN SHARED ALIAS <tcAlias>
*
LPARAMETERS tcAlias AS String
LOCAL llSuccess, loException, lcXLIBFolder, lcSetDatabase

IF NOT FILE("XXWBPREF.DBF")
  *
  *  create it
  *
  lcXLIBFolder = ADDBS(JUSTPATH(FULLPATH("XXFW.VCX")))
  lcSetDatabase = SET("DATABASE")
  SET DATABASE TO 
  TRY
  CREATE TABLE (m.lcXLIBFolder + "XXWBPref.DBF") ;
       (Prf_Item C(100), ;
        Prf_ItemDT C(1), ;
        Prf_ItemVL C(100))
  CATCH
  ENDTRY
  USE IN SELECT("XXWBPref")
  SET DATABASE to &lcSetDatabase
ENDIF
IF USED(tcAlias) ;
     AND UPPER(JUSTFNAME(DBF(m.tcAlias))) == "XXWBPREF.DBF"
  RETURN .t.
ENDIF     
llSuccess = .t.
TRY 
USE XXWBPREF IN 0 AGAIN SHARED ALIAS (m.tcAlias)
CATCH
llSuccess = .f.
ENDTRY
RETURN m.llSuccess



*******************************************************
*******************************************************
PROCEDURE CallWorkAreasViewer
*******************************************************
*******************************************************
*
*  instantiate XXFWUTIL.VCX/frmWorkAreasViewer in the
*  development environment, except when the Form/Class/
*  Report Designers are in use
*
*  this procedure is called from the SHIFT+F11 OKL
*  installed in A.PRG/SetupStuff
*    XXDTES("A.PRG","do CallWorkAreasViewer in XXPROCDT")
*
IF TYPE("_Screen.ActiveForm.Baseclass") = "C" ;
     AND !ISNULL(_Screen.ActiveForm) ;
     AND X7CPEDIG(_Screen.ActiveForm,"frmWorkAreasViewer")
  MESSAGEBOX("The Work Areas Viewer may not be selected " + ;
             "again if it is already running.",48,"Please Note")
  RETURN
ENDIF     
IF WEXIST("Class Designer") OR WEXIST("Form Designer -") ;
     OR WEXIST("Report Designer -")
  RETURN
ENDIF     
IF NOT X8SETCLS("XXFWUTIL.VCX")
  MESSAGEBOX("Unable to locate XXFWUTIL.VCX",48,"Please Note")
  RETURN
ENDIF
IF TYPE("_Screen.ActiveForm.BaseClass") = "C" ;
     AND !ISNULL(_Screen.ActiveForm)
    *  instantiate frmWorkAreasViewer from that data session
    LOCAL lnDataSession 
    lnDataSession = SET("DataSession")
    SET DATASESSION TO _Screen.ActiveForm.DataSessionID
ENDIF
LOCAL loForm
loForm = CREATEOBJECT("frmWorkAreasViewer")
IF VARTYPE(loForm) = "O"
  loForm.Show()
ENDIF
loForm = .NULL.
IF VARTYPE(lnDataSession) = "N"
  SET DATASESSION TO (lnDataSession)
ENDIF
RETURN




*******************************************************
*******************************************************
PROCEDURE ToggleComment
*******************************************************
*******************************************************
LPARAMETERS tlForceCSLookup, tcOKLKey

#IF .F.
*-------------------------------------------------------
*---------------- FILE CREATION INFO -------------------
Program....: TOGGLECOMMENT
Author.....: Michael J. Potjer
Date.......: April 30, 2002
Compiler...: Visual FoxPro 07.00.0000.9465 for Windows
*-------------------------------------------------------
*------------------ USAGE SECTION ----------------------
DESCRIPTION:
	Toggles the comment status of a selected block of text.  If the selected block is
	commented, it will be uncommented, and vice versa.

SCOPE: Public

PARAMETERS:
	  tlForceCSLookup (O) -	Force this function to lookup the editor comment string.
							This is useful if you've gone into Tools/Options and changed
							the editor comment string since you assigned this function
							to an ON KEY LABEL.
							IF omitted, defaults to .T. if the editor comment string has
							not been stored in a property of _Screen, otherwise defaults
							to .F..
			 tcOKLKey (O) -	A key or key combination to assign this function to.  This
							allows you to initialize the function and assign it to an
							ON KEY LABEL in one command.

USAGE: TOGGLECOMMENT( [tlForceCSLookup [, tcOKLKey]] )

EXAMPLE:
	? ToggleComment( .T., "F6" )

	* After changing the comment string in Tools/Options, set the new string.
	? ToggleComment( .T. )

RETURNS: Logical
	Returns .F. if an error occurs, otherwise returns .T.

*------------- MAINTENANCE SECTION -------------------
INPUTS (extracted from environment):
	A key value is read from the registry.

OUTPUTS (changes to environment):
	A property is added to _Screen.

PRE-CONDITION INVARIANTS:
	- The VFP foundation class library REGISTRY.VCX must available.
	- An editor comment string value must be specified under Tools/Options.

POST-CONDITION INVARIANTS: None

NOTES:
	The editor comment string is copied to a _Screen property so that we don't have to
	keep going to the registry every time this function is called.  However, that means
	that any time you change the editor comment string, the _Screen property needs to
	be set to the new value.  The force comment string lookup parameter makes this easy.

COLLABORATING METHODS:
	REGISTRY.VCX/Registry.GetRegKey()

PROCESS:
	1.	Determine what the editor comment string is.
	2.	If the ON KEY LABEL is to be set here, just set it and exit.
	3.	Check if the first line of the selected text is commented.  If is, uncomment
		the block, otherwise comment.

CHANGE LOG:
*-------------------------------------------------------
#ENDIF

#DEFINE HKEY_CURRENT_USER	-2147483647

IF VARTYPE( _Screen.cEditorCommentString ) = "C"
	* The editor comment string has already been added as a property to the Screen,
	* so we will look it up only if the user explicitly passed a .T. to this function.
	tlForceCSLookup = ( VARTYPE( tlForceCSLookup ) = "L" AND tlForceCSLookup )
ELSE
	* The editor comment string hasn't been retrieved from the registry yet, so let's
	* do that before we continue processing.
	tlForceCSLookup = .T.
ENDIF

IF tlForceCSLookup
	LOCAL lcRegistryLibrary, ;
		loRegistry, ;
		lcEditorCommentString

	* Look for the VFP registry class.
	lcRegistryLibrary = HOME() + "FFC\REGISTRY.VCX"
	IF FILE( lcRegistryLibrary )
		* The registry class exists, so attempt to instantiate it.
		loRegistry = NEWOBJECT( "Registry", lcRegistryLibrary )
	ELSE
		* Let the user know why this function didn't work.
		ASSERT .F. MESSAGE "Unable to locate the VFP Registry class library:" + CHR(13) ;
				+ SPACE(4) + lcRegistryLibrary + CHR(13) ;
				+ "The editor comment string cannot be determined."
		RETURN .F.
	ENDIF

	* Attempt to retrieve the editor comment string.
	lcEditorCommentString = ""
	DO CASE
		CASE VARTYPE( loRegistry ) # "O"
			* The registry object could not be instantiated.  Let the user know.
			ASSERT .F. MESSAGE "The registry class could not be instantiated." + CHR(13) ;
					+ "The editor comment string cannot be determined."
			RETURN .F.

		CASE loRegistry.GetRegKey( "EditorCommentString", @lcEditorCommentString, ;
				"Software\Microsoft\VisualFoxPro\" + SUBSTR( VERSION(4), 2, 3 ) + "\Options", ;
				HKEY_CURRENT_USER ) = 0
			* We located the editor comment string value in the registry.

		OTHERWISE
			* We couldn't find the editor comment string.  Bail out.
			ASSERT .F. MESSAGE "The editor comment string could not be located in the registry."
			RETURN .F.
	ENDCASE

	DO CASE
		CASE EMPTY( lcEditorCommentString )
			* Somehow, the editor comment string has been cleared.  Let the user know.
			ASSERT .F. MESSAGE "You must specify an editor comment string under Tools/Options " ;
					+ "in order to comment/uncomment a block of code."
			RETURN .F.

		CASE PEMSTATUS( _Screen, "cEditorCommentString", 5 )
			* The Screen property has already been created, so populate it with the editor
			* comment string.
			_Screen.cEditorCommentString = lcEditorCommentString

		OTHERWISE
			* Create a new property on the screen to store the editor comment string.
			_Screen.AddProperty( "cEditorCommentString", lcEditorCommentString )
	ENDCASE
ENDIF

* If an OKL key combination was passed to this function, then just set up the OKL to
* call this function and exit.
IF VARTYPE( tcOKLKey ) = "C" AND !EMPTY( tcOKLKey )
	* DEVNOTE -- If this function is copied into a function library, then add IN <.PRG Name>
	*- to the DO command.
	ON KEY LABEL &tcOKLKey. DO ToggleComment
	RETURN
ENDIF

LOCAL lnWHandle, ;
	laEnv[25], ;
	lcOldClipText, ;
	lcSelectedText

*-- Attempt to get a handle for the current window, so that we can implement some advanced
*-- features.  We need FoxTools to get the handle.
*-- Note that the essential aspects of toggling comments do not require FoxTools, so we
*-- won't bother with error messages if FoxTools can't be loaded.
EXTERNAL PROCEDURE _WONTOP
lnWHandle = IIF( X2SETLIB( "FOXTOOLS" ), _WOnTop(), 0 )

IF ( lnWHandle > 0 ) AND ( _EdGetEnv( lnWhandle, @laEnv ) = 1 )
	*-- We were able to get a window handle, and retrieve environment info about the editor
	*-- window.  Implement some of the "smart" features of this function.
	LOCAL lnLineNo, ;
		lnStartPos, ;
		lnEndPos

	DO CASE
		CASE INLIST( laEnv[25], 0, 2 )
			*-- The user is currently in the Command window, or a MODIFY FILE window.
			*-- Display an error message and abort this function.
			MESSAGEBOX( "The Toggle Comments function only applies to code editing windows.", ;
					48, "Toggle Comments" )
			RETURN

		CASE laEnv[12] > 0
			*-- The current editor window is read-only.  We can't do anything anyway, so
			*-- tell the user and bail out.
			MESSAGEBOX( "The current editor window is read-only.", 48, "Toggle Comments" )
			RETURN
	ENDCASE

	*-- Get the line number for the start of the selected text, then use the line number to
	*-- find the beginning of the first selected line.
	lnLineNo = _EdGetLNum( lnWHandle, laEnv[17] )
	lnStartPos = _EdGetLPos( lnWHandle, lnLineNo )

	*-- The cursor could just be sitting at the beginning of the line, with no text selected,
	*-- so make sure the end position that we use is at least large enough to include the
	*-- editor comment string.
	lnEndPos = MAX( laEnv[18], lnStartPos + LEN( _Screen.cEditorCommentString ) )

	*-- Get the text between the start and end positions that we've identified.
	lcSelectedText = _EdGetStr( lnWHandle, lnStartPos, lnEndPos )
ELSE
	*-- We couldn't get a window handle, or the editor environment info could not be retrieved,
	*-- so we'll just resort to more primitive means of getting the selected text.
	* Save whatever text is already in the clipboard, so that we can restore it again.
	lcOldClipText = _CLIPTEXT

	* Clear the clipboard, and copy the currently selected text into it.
	_CLIPTEXT = ""
	SYS( 1500, "_MED_COPY", "_MEDIT" )

	* Copy the text from the clipboard into a local variable, and restore the original text.
	lcSelectedText = ALLTRIM( _CLIPTEXT )
	_CLIPTEXT = lcOldClipText
ENDIF

* Note that we are only checking if the first line of text has been commented, and
* toggling the comment status for the entire block.  This means that you cannot select a
* line beginning with the comment string and try to apply a second comment.
*
* If the FoxTools editor functions could not be used above, then there are two additional
* issues you need to be aware of:
* 1.	If you invoke this function from the Command window or a MODIFY FILE window, the
*		context menu will appear, but nothing else will happen.
* 2.	If the start position of the selected text is *AFTER* a comment string, then your
*		selected text will be commented again, rather than uncommented.  The VFP block
*		comment feature can comment or uncomment a line regardless of the current cursor
*		position, but this function won't be able to detect if the selected text is already
*		commented.

LOCAL lcVFPVersion, lcCommentMenuOption

lcVFPVersion = SUBSTRC(VERSION(4), 2, 1)

*!*	IF LEFT( lcSelectedText, LEN( _Screen.cEditorCommentString ) ) == _Screen.cEditorCommentString
*!*	  * This text has already been commented, so uncomment it.
*!*	  DO CASE
*!*	    CASE VERSION(3) = "00"                    && English - Uncomment (Remove comment)
*!*	      lcCommentMenuOption = 'N'

*!*	    CASE VERSION(3) = "49"                    && German - Kommentarmarkierung entfernen
*!*	      DO CASE
*!*	        CASE lcVFPVersion = '6'						&& VFP 6
*!*	          lcCommentMenuOption = 'M'

*!*	        CASE lcVFPVersion = '7'						&& VFP 7
*!*	          lcCommentMenuOption = 'O'
*!*	      ENDCASE
*!*	  ENDCASE
*!*	ELSE
*!*	  * This text isn't commented, so comment it now.
*!*	  DO CASE
*!*	    CASE VERSION(3) = "00"                    && English - Comment
*!*	      lcCommentMenuOption = 'M'

*!*	    CASE VERSION(3) = "49"                    && German - Kommentar
*!*	      DO CASE
*!*	        CASE lcVFPVersion = '6'						&& VFP 6
*!*	          lcCommentMenuOption = 'O'

*!*	        CASE lcVFPVersion = '7'						&& VFP 7
*!*	          lcCommentMenuOption = 'T'
*!*	      ENDCASE
*!*	  ENDCASE
*!*	ENDIF

If Left( lcSelectedText, Len( _Screen.cEditorCommentString ) ) == _Screen.cEditorCommentString
  * This text has already been commented, so uncomment it.
  Do Case
    Case Version(3) = "00"                    && English - Uncomment (Remove comment)
      lcCommentMenuOption = 'N'
    Case Version(3) = "49"                    && German - Kommentarmarkierung entfernen
      Do Case
        Case lcVFPVersion = '6'               && VFP 6
          lcCommentMenuOption = 'M'
        OTHERWISE
          lcCommentMenuOption = 'O'
      Endcase
    Case Version(3)="34"       && Espa�ol - Quitar marca de comentario
      lcCommentMenuOption = Replicate('{UPARROW}',4)+'{ENTER}'
  Endcase
 Else
  * This text isn't commented, so comment it now.
  Do Case
    Case Version(3) = "00"                    && English - Comment
      lcCommentMenuOption = 'M'
    Case Version(3) = "49"                    && German - Kommentar
      Do Case
        Case lcVFPVersion = '6'               && VFP 6
          lcCommentMenuOption = 'O'
        OTHERWISE
          lcCommentMenuOption = 'T'
      Endcase
    Case Version(3) = "34"                    && Espa�ol - Comentar
      lcCommentMenuOption = Replicate('{UPARROW}',5)+'{ENTER}'
  Endcase
Endif

KEYBOARD "{SHIFT+F10}" + lcCommentMenuOption PLAIN
RETURN



*******************************************************
*******************************************************
PROCEDURE Toggle_TRBETWEEN
*******************************************************
*******************************************************
*-------------------------------------------------------------------
* Procedure: Toggle_TRBETWEEN
* Purpose: Toggle SET TRBETWEEN ON/OFF. This makes debugging 
* faster by saving time when you step over large segments of
* code in the debugger, because the executed lines will not 
* be displayed.
* Based on the 'SET TRBWEEN' tip by Mike Yearwood in a
* 1999 issue of FoxPro Advisor.
* Unlike the comments in that issue, in VFP 7 this
* seems to work while the code is displaying in the
* Debugger's trace window.
* Example: ON KEY LABEL CTRL+F11 DO TOGGLE_TRBETWEEN IN XXPROCDT
*-------------------------------------------------------------------
*  XXDTES("A.PRG","CTRL+F7")
IF SET("TRBETWEEN") = "ON"
  SET TRBETWEEN OFF
  WAIT WINDOW "TRBETWEEN has been set to OFF" NOWAIT
 ELSE
  SET TRBETWEEN ON
  WAIT WINDOW "TRBETWEEN has been set to ON" NOWAIT
ENDIF
ACTIVATE SCREEN
RETURN



*******************************************************
*******************************************************
PROCEDURE Toggle_Debugger
*******************************************************
*******************************************************
*  XXDTES("A.PRG","CTRL+F11")
IF WVISIBLE("Debugger")
  CLOSE DEBUGGER
 ELSE
  _Screen.Visible = .t.  
  DEBUG
ENDIF
RETURN




*******************************************************
*******************************************************
PROCEDURE InsertMethodHeader
*******************************************************
*******************************************************
*
*  insert the ClassLib/ClassName::MethodName() into the header of
*  the current class method in the method editor
*  
*  can be installed via an OKL in ASETUP.PRG, for example:
*    on key label CTRL+I do InsertMethodHeader in XXPROCDT.PRG
*
LOCAL loMethodInfo, ;
	lcString, ;
	lcOldClipText

*-- Get all the information available about the current method window.
loMethodInfo = GetObjectPathToMethod()

*-- Make sure that we're in a Class/Form method editing window.
IF INLIST( loMethodInfo.icFileType, "C", "F" )
ELSE
	WAIT WINDOW "This utility only applies to Class/Form method editing windows."
	RETURN
ENDIF

*
*  DWS 10/06/02
*  added the following block to do a better job of
*  formatting method name and class name in the 
*  desired case
*

*
*  first, just PROPER()ize the method name
*
LOCAL lcMethodName
lcMethodName = PROPER(UPPER(ALLTRIM(loMethodInfo.icMethodName)))
DO CASE
  CASE RIGHTC(lcMethodName,7) == "_ASSIGN"
    lcMethodName = STRTRAN(lcMethodName,"_assign","_Assign")
  CASE RIGHTC(lcMethodName,7) == "_ACCESS"
    lcMethodName = STRTRAN(lcMethodName,"_access","_Access")
ENDCASE
loMethodInfo.icMethodName = lcMethodName

*!*	debugging info:
*!*	ACTIVATE SCREEN
*!*	?
*!*	? "loMethodInfo.icFileName: " + loMethodInfo.icFileName
*!*	? "loMethodInfo.icObjectPath: " + loMethodInfo.icObjectPath
*!*	? "loMethodInfo.icMethodName: " + loMethodInfo.icMethodName
*!*	? "loMethodInfo.icMainObjectName: " + loMethodInfo.icMainObjectName
*!*	? "loMethodInfo.icClassName: " + loMethodInfo.icClassName
*!*	? "loMethodInfo.icObjectName: " + loMethodInfo.icObjectName   &&& empty for forms
*!*	? "loMethodInfo.icErrorMessage: " + loMethodInfo.icErrorMessage

*!*	#IF .f.
*!*	* form 
*!*	icFileName = .SCX
*!*	icObjectPath = form name
*!*	icMainObjectName = form name
*!*	icClassName = (empty)
*!*	icObjectName = form name

*!*	* form member
*!*	icFileName = .SCX
*!*	icObjectPath = form.member.member... name
*!*	icMainObjectName = form name
*!*	icClassName = (empty)
*!*	icObjectName = name of last member in icObjectPath
*!*	icErrorMessage = (contains "Please verify", probably OK)
*!*	                 (contains "cannot be determined", bad)

*!*	* class
*!*	icFileName = .VCX
*!*	icObjectPath = class name
*!*	icMainObjectName = class name
*!*	icClassName = class name
*!*	icObjectName = class name

*!*	* class member
*!*	icFileName = .VCX
*!*	icObjectPath = class.member.member... name
*!*	               could be empty, which gives "cannot be determined" icErrorMessage
*!*	icMainObjectName = class name
*!*	icClassName = class name
*!*	icObjectName = name of last member in icObjectPath
*!*	icErrorMessage = (contains "Please verify", probably OK)
*!*	                 (contains "cannot be determined", bad)
*!*	#ENDIF

*
*  now see if we can get the proper class name from
*  the zReadMe text of the class
*
DO CASE
  ***************************************************
  CASE JUSTEXT(loMethodInfo.icFileName) = "SCX"
  ***************************************************
    *
    *  nothing here for .SCX-based forms
    *
  ***************************************************
  CASE UPPER(ALLTRIM(loMethodInfo.icClassName)) == "CUSPEMTOOL" ;
       AND UPPER(ALLTRIM(loMethodInfo.icObjectName)) == "CUSPEMTOOL"
  ***************************************************
    *
    *  this code has been called while in the cusPEMTool
    *  class itself, to insert a method header
    *
  ***************************************************
  OTHERWISE
  ***************************************************
    LOCAL lcClassName
    lcClassName = GetFormattedClassName(loMethodInfo.icFileName, ;
                                        loMethodInfo.icClassName)
    IF NOT EMPTY(lcClassName)

      IF OCCURS("/",lcClassName) = 1
        loMethodInfo.icFileName = GETWORDNUM(lcClassName,1,"/")
        lcClassName = GETWORDNUM(lcClassName,2,"/")
      ENDIF


      IF OCCURS(".",loMethodInfo.icObjectPath)>0
        *
        *  replace leftmost item in the icObjectPath
        *
        loMethodInfo.icObjectPath = lcClassName + ;
                                    SUBSTRC(loMethodInfo.icObjectPath,AT_C(".",loMethodInfo.icObjectPath))
       ELSE
        *  
        *  object path is just the class name
        *
        loMethodInfo.icObjectPath = lcClassName
      ENDIF
    ENDIF
ENDCASE
*
*  DWS 10/06/02
*  end of Drew's modifications to get the proper case
*  formatting for a class name
*


*-- Build the header info, to insert into the method.
lcString =	"*" + CHR(13) + ;
			"*  " + loMethodInfo.icFileName + "/" + loMethodInfo.icObjectPath ;
				+ "::" + loMethodInfo.icMethodName + "()" + CHR(13) + ;
			"*" + CHR(13)

*-- Save the current contents of the clipboard, so that we can
*-- restore them later.
lcOldClipText = _CLIPTEXT

*-- The keyboard buffer can only handle 128 characters at a time, and it doesn't
*-- seem to work to KEYBOARD the method header in separate chunks, so we'll copy
*-- it into the clipboard and paste it instead.
*-- Copy the method header into the clipboard, then force the cursor to the
*-- beginning of the method.  Notice the DOEVENTS, which forces these keystrokes
*-- to be executed immediately, so that the subsequent paste operation will
*-- occur in the correct location.
_CLIPTEXT = lcString
KEYBOARD "{Ctrl+Home}" PLAIN CLEAR
DOEVENTS
SYS( 1500, "_MED_PASTE", "_MEDIT" )

*-- Restore the original clipboard contents.
_CLIPTEXT = lcOldClipText

RETURN



*******************************************************
*******************************************************
PROCEDURE GetFormattedClassName
*******************************************************
*******************************************************
*
*  RETURNs the formatted name of the passed class
*  in the passed .VCX, as it appears in the Class: <ClassName>
*  line in the zReadMe text
*
*  if anything goes wrong here, or the desired
*  zReadMe-based ClassName cannot be found, this
*  procedure RETURNs SPACE(0)
*
LPARAMETERS tcVCXFile, tcClassName
LOCAL lcClassName, loPEMTool
lcClassName = SPACE(0)
IF NOT X8SETCLS("XXTOOLS.VCX") 
  RETURN lcClassName
ENDIF
loPEMTool = CREATEOBJECT("cusPEMTool")
IF NOT VARTYPE(loPEMTool) = "O"
  RETURN lcClassName
ENDIF
LOCAL laMethod[1], llzReadMe, xx, lcLine
IF NOT loPEMTool.OpenMetaFILE(tcVCXFile)
  loPEMTool = .NULL.
  RETURN lcClassName
ENDIF
IF loPEMTool.LocateObject(tcClassName) 
  *
  *  get the zReadMe text
  *
  llzReadMe = loPEMTool.GetMethod("zReadMe",@laMethod)
ENDIF
loPEMTool.CloseMetaFILE()
loPEMTool = .NULL.
IF llzReadMe
  *
  *  look for the Class: line in the zReadMe text, if found,
  *  overwrite lcString with that string
  *   
  WAIT WINDOW "Searching zReadMe text..." NOWAIT 
  FOR xx = 1 TO ALEN(laMethod,1)
    lcLine = laMethod[xx]
    IF UPPER(ALLTRIM(lcLine)) = "CLASS:"
      lcClassName = ALLTRIM(SUBSTRC(lcLine,AT_C(SPACE(1),lcLine)))
      EXIT 
    ENDIF
  ENDFOR
ENDIF
RETURN lcClassName



*******************************************************
*******************************************************
FUNCTION GenES
*******************************************************
*******************************************************
#IF .F.
*-------------------------------------------------------
*---------------- FILE CREATION INFO -------------------
Program....: XXPROCDT
Author.....: Michael J. Potjer
			 Optimal Solutions, Inc.
			 http://www.optimalinternet.com
Date.......: January 7, 2002
Compiler...: Visual FoxPro 07.00.0000.9262 for Windows
Contributed to Visual MaxFrame Professional
*-------------------------------------------------------
#ENDIF

*
*  This is a utility to help simplify bug reporting.  
*  It copies a string in the following format into the 
*  clipboard:
*    XXDTES( "File Name", <Line # or Search String>, "Class Name", "Method Name" )
*  OR
*    EDITSOURCE( "File Name", <Line #>, "Class Name", "Method Name" )
*
*  The resulting string can then be pasted into an email, 
*  and sent to the developer, who can in turn copy and paste 
*  it into the Command Window for execution.
*
*  This utility can be used with .PRG files open in a MODIFY COMMAND window,
*  or in the method code editor of an object or member object open from
*  either the Class or Form Designers.  It can also be used to generate an
*  XXDTES/EDITSOURCE string for text files (such as *.H files).
*
*  Select the text that you want to include as the search string, and run the
*  utility to populate the clipboard.  If the selected text contains all the
*  text delimiters (",', and [ or ]), then a line number corresponding to the
*  selected text will be used in the return value.  If no text is selected,
*  the return value will simply use line 1.
*
* NOTE: If all of the following are true:
*		-	You used EDITSOURCE/XXDTES or the Document View window to open a
*			Class/Form Designer method editing window.
*		-	The method being edited belongs to an object that is a member of
*			some other container object.
*		-	The object whose method is being edited has the same name as
*			other objects elsewhere in the Objects collections.
*		-	No text has been selected in the method, or the selected text is
*			identical to text that exists in the same method of another object
*			with the same name.
*
*	Under these conditions, it's possible that the generated EDITSOURCE/XXDTES
*	will not contain the correct object	path in the fourth parameter.  If
*	that's the case, selecting that object in the Designer before invoking
*	this utility should ensure accurate results.
*
* This utility can be installed via an OKL in ASETUP.PRG, for example:
*    ON KEY LABEL F8 DO GenES IN XXPROCDT.PRG
*  OR
*    ON KEY LABEL F8 DO GenES WITH .T., .T. IN XXPROCDT.PRG
*
* Since WONTOP() ignores the Command Window, you can also run this utility
* from the Command Window like so:
*    DO GenES IN OXPROCDT.PRG
* However, the 2nd XXDTES() parameter will always be 1 when run this way.
*
* PARAMETERS:
*	tlForceEDITSOURCE (O) -	Indicates whether the command should be built using
*							  the EDITSOURCE() command, regardless of whether
*							  the VMP XXDTES.PRG function is available.
*                           If omitted or passed as .F., the utility 
*                             will still use EDITSOURCE() if 
*                            1- the XXDTES.PRG or .FXP file 
*                               can't be found
*                            -OR-
*                            2- the generated command is using
*                               a line number
*			  tlQuiet (O) -	Indicates whether this utility should be "silent".
*							If .T., results will be displayed in a WAIT WINDOW,
*							rather than a MESSAGEBOX().  However, in either
*							case, the display message is returned from this
*							function, in case you still want it.
*
* RETURNS:
*	A character string containing a message about what this function did.
*	If successful, will copy a command to the clipboard.
*****************************************************************
LPARAMETERS tlForceEDITSOURCE, tlQuiet

LOCAL loEditInfo, ;
	lcSearchString, ;
	lcSearchStringOrLine, ;
	lcMessage, ;
	lcWaitWindowString

IF ( VARTYPE( tlForceEDITSOURCE ) = "L" ) AND tlForceEDITSOURCE
	*-- Use the EDITSOURCE() function, regardless of whether the user has the
	*-- VMP XXDTES.PRG function available or not.
ELSE
	*-- Only use the EDITSOURCE() function if the VMP EDITSOURCE() wrapper,
	*-- XXDTES, is not available.
	tlForceEDITSOURCE = !FILE( "XXDTES.PRG" ) AND !FILE( "XXDTES.FXP" )
ENDIF

*-- Attempt to retrieve any file information available, and initialize the
*-- success flag.
loEditInfo = GetObjectPathToMethod()
llSuccess = .F.

IF EMPTY( loEditInfo.icFileType )
	*-- The user didn't select a valid edit window.  Display an error message.
	IF EMPTY( loEditInfo.icErrorMessage )
		*-- What happened?
		lcMessage = "Unknown error in " + ALLTRIM( PROGRAM() )
	ELSE
		*-- Use the error message returned in the file info object.
		lcMessage = loEditInfo.icErrorMessage
	ENDIF
ELSE
	*-- A valid edit window is selected.
	*-- Check if a string was selected, regardless of whether EDITSOURCE() is
	*-- going to be used or not.
	lcSearchString = AddStringDelimiters( loEditInfo.icSelectedText )
	DO CASE
		CASE ISNULL( lcSearchString )
			*-- There's a search string, but it contains too many string
			*-- delimiters, so we can't use it.  Get a line number instead.
			lcSearchStringOrLine = TRANSFORM( loEditInfo.inSelectedLine )
			tlForceEDITSOURCE = .T.

		CASE EMPTY( lcSearchString )
			*-- There's nothing selected, so just use line 1.
			lcSearchStringOrLine = "1"
			tlForceEDITSOURCE = .T.

		CASE tlForceEDITSOURCE
			*-- There is a search string, but we want an EDITSOURCE()
			*-- command.  Get the line number for the selected string.
			lcSearchStringOrLine = TRANSFORM( loEditInfo.inSelectedLine )

		OTHERWISE
			*-- Use the search string.
			lcSearchStringOrLine = lcSearchString
	ENDCASE

	*-- Initialize the EDITSOURCE() command string with the name of the function
	*-- that will be used.
	IF tlForceEDITSOURCE
		lcMessage = "EDITSOURCE("
	ELSE
		lcMessage = "XXDTES("
	ENDIF

	*-- Add the file name and line number or search string to the string.
	*-- These two parameters are the same for all file types.
	lcMessage = lcMessage + ["] + loEditInfo.icFileName + [",] + lcSearchStringOrLine

	IF INLIST( loEditInfo.icFileType, "C", "F", "D" )   &&& "D" is for Spanish support
		*-- Classes and forms require additional parameters.  Pass the name of
		*-- main object (the form or class name) as the third parameter.
*!*			lcMessage = lcMessage + [,"] + loEditInfo.icMainObjectName + ["]
        lcMessage = lcMessage + [,"] + IIF(NOT EMPTY(loEditInfo.icMainObjectName),loEditInfo.icMainObjectName ,loEditInfo.icobjectname)+ ["]

		*-- Add the fourth parameter.
		IF OCCURS( ".", loEditInfo.icObjectPath ) = 0
			*-- There's no object path, so the main object and selected object
			*-- are the same.  All we need is the method name.
			lcMessage = lcMessage + [,"] + loEditInfo.icMethodName + ["]
		ELSE
			*-- Strip the main object from the object path, and add the rest of
			*-- the path, plus the method name.
			lcMessage = lcMessage + [,"] ;
				+ SUBSTRC( loEditInfo.icObjectPath, ATCC( ".", loEditInfo.icObjectPath ) + 1 ) ;
				+ "." + loEditInfo.icMethodName + ["]
		ENDIF
	ENDIF

	*-- Add the closing parenthesis to the function, and copy it to the clipboard.
	lcMessage = lcMessage + ")"
	_CLIPTEXT = lcMessage

	*-- Now add additional information to display for the user.
	lcMessage = "The following has been copied to the clipboard:" ;
			+ CHR(13) + CHR(13) + lcMessage

	*-- If an error message was returned with the edit information, include it.
	IF EMPTY( loEditInfo.icErrorMessage )
	ELSE
		lcMessage = lcMessage + CHR(13) + CHR(13) + loEditInfo.icErrorMessage
	ENDIF
ENDIF

IF tlQuiet
  *  less-intrusive feedback 
  lcWaitWindowString = lcMessage
  IF LENC(lcWaitWindowString) > 254
    WAIT WINDOW LEFTC(lcWaitWindowString,250) + "..." nowait
   ELSE
    WAIT WINDOW lcWaitWindowString NOWAIT
  ENDIF
ELSE
  *-- The user wants feedback on the results of this function, so display a message.
  MESSAGEBOX( lcMessage, 64, "Please Note" )
ENDIF

RETURN lcMessage


*******************************************************
*******************************************************
FUNCTION GetObjectPathToMethod
*******************************************************
*******************************************************
* F.K.A. GenES_GetFullObjectInfo
*****************************************************************
*
* Builds the full object path from the object being edited by the user to
* the main object of which the item is a member (they could be the same).
*
* Returns an object containing information about the file and the object
* being edited.  The properties and their contents are the same as that
* returned by GetFileInfoForEditWindow(), plus the following:
*
*	icMainObjectName  -	The name of the main object.  For a class, this will
*						be the class name.  For a form or formset, this will
*						be the form or formset name.
*	icObjectPath	  -	The full object path from the main object to the
*						object being edited.  If the object being edited
*						can't be found, then it contains an empty string.
*	icErrorMessage	  -	Contains an error message if the object path can't
*						be determined.
*						Will also contain a warning message if an object
*						with the correct name is found, but we can't be sure
*						if it's the correct one.
*
* EXAMPLE:
*	loEditInfo = GetObjectPathToMethod()
*	DO CASE
*		CASE EMPTY( loEditInfo.icFileType )
*			*-- No file info found.  Find out why.
*			WAIT WINDOW loEditInfo.icErrorMessage
*		CASE NOT EMPTY( loEditInfo.icErrorMessage )
*			*-- There's a warning message.  Find out what it is.
*			WAIT WINDOW loEditInfo.icErrorMessage
*	ENDCASE
*
* See also: XXPROCDT.PRG/GenES
*****************************************************************
LOCAL loObjectInfo AS Relation, ;
	laSelectedObjects[1], ;
	loMainObject, ;
	loCurrentObject, ;
	llObjectPathUnknown, ;
	luStringOrLine, ;
	lnCount, ;
	lcObjectPath

*-- Get file info for whatever the user is currently editing.
loObjectInfo = GetFileInfoForEditWindow()

*-- For consistency, add these properties to the object, regardless of
*-- whether we use them or not.
loObjectInfo.AddProperty( "icObjectPath", "" )
loObjectInfo.AddProperty( "icMainObjectName", "" )

*-- If this isn't a Class or Form, there's nothing more to do.
IF INLIST( loObjectInfo.icFileType, "C", "F" )
ELSE
	RETURN loObjectInfo
ENDIF

*-- Get an object reference to the container object, and work our way down
*-- until we find the main parent object.
*-- ASELOBJ(.., 1) always returns an object reference, even if no objects are
*-- actually selected.
ASELOBJ( laSelectedObjects, 1 )
loMainObject = laSelectedObjects[1]

IF loObjectInfo.icFileType = "C"
	*-- For classes, we already know the name of the main parent from the
	*-- designer window, so we just need to move from parent to parent until
	*-- we find a match.
	DO WHILE NOT UPPER( ALLTRIM( loMainObject.Name ) ) ;
			== UPPER( ALLTRIM( loObjectInfo.icClassName ) )
		loMainObject = loMainObject.Parent
	ENDDO
ELSE
	*-- For an SCX file, we just need to find the form.
	*-- ASELOBJ(.., 1) never seems to return an object higher than FORM, even
	*-- if a formset is open in the designer.
	DO WHILE NOT UPPER( ALLTRIM( loMainObject.BaseClass ) ) == "FORM"
		loMainObject = loMainObject.Parent
	ENDDO

	*-- In the Designers, all forms have a parent, even if there isn't really
	*-- a formset.  If this form is really part of a formset, we'll assume
	*-- that there's more than one form.
	IF loMainObject.Parent.FormCount > 1
		loMainObject = loMainObject.Parent
	ENDIF
ENDIF

*-- Save the name of the main object.
loObjectInfo.icMainObjectName = loMainObject.Name

*-- Initialize the current object, and the flag indicating if we know what
*-- the current object is.
loCurrentObject = .NULL.
llObjectPathUnknown = .F.

DO CASE
	CASE UPPER( ALLTRIM( loMainObject.Name ) ) ;
			== UPPER( ALLTRIM( loObjectInfo.icObjectName ) )
		*-- The main object is the one we're looking for.
		loCurrentObject = loMainObject

	CASE ASELOBJ( laSelectedObjects ) = 0
		*-- No objects are selected in the Designer, so we can't be sure what
		*-- the object path is without searcing every object collection in
		*-- the form or class.
		*-- This can happen if the method editing window was opened via an
		*-- EDITSOURCE() command, or using the Document View window.
		llObjectPathUnknown = .T.

	OTHERWISE
		*-- Check if any of the selected objects correspond to the object
		*-- whose method code is currently displayed in the code editing
		*-- window.
		FOR lnCount = 1 TO ALEN( laSelectedObjects )
			IF UPPER( ALLTRIM( laSelectedObjects[lnCount].Name ) ) ;
					== UPPER( ALLTRIM( loObjectInfo.icObjectName ) )
				*-- This is the object we were looking for.
				loCurrentObject = laSelectedObjects[lnCount]
				EXIT
			ENDIF
		ENDFOR

		*-- If we don't have an object reference yet, then we don't know
		*-- for sure what the object path is.  We'll need to search the
		*-- object collections for it.
		llObjectPathUnknown = ISNULL( loCurrentObject )
ENDCASE

IF llObjectPathUnknown
	*-- Try to find the object in one of the object collections.  If there is
	*-- some selected text to look for, include that, otherwise use the line
	*-- number.
	luStringOrLine = IIF( EMPTY( loObjectInfo.icSelectedText ), ;
			loObjectInfo.inSelectedLine, loObjectInfo.icSelectedText )
	loCurrentObject = FindObjectInParent( loMainObject, loObjectInfo.icObjectName, ;
			loObjectInfo.icMethodName, luStringOrLine )
ENDIF

IF ISNULL( loCurrentObject )
	*-- The object still couldn't be found.  Return an error message.
	loObjectInfo.icErrorMessage = "The object path for the item being edited " ;
			+ "can't be determined."
ELSE
	*-- Walk down the object path from the current object to the main object,
	*-- building the object path.
	lcObjectPath = ALLTRIM( loCurrentObject.Name )
	DO WHILE NOT UPPER( ALLTRIM( loCurrentObject.Name ) ) ;
			== UPPER( ALLTRIM( loMainObject.Name ) )
		loCurrentObject = loCurrentObject.Parent
		lcObjectPath = ALLTRIM( loCurrentObject.Name ) + "." + lcObjectPath
	ENDDO
	loObjectInfo.icObjectPath = lcObjectPath

	IF llObjectPathUnknown
		*-- We had to search for the object being edited, so add a warning
		*-- for the user.
		loObjectInfo.icErrorMessage = "If there is more than one object named " ;
				+ loObjectInfo.icObjectName + ", the object path may not be " ;
				+ "correct.  Please verify."
	ENDIF
ENDIF

*-- Clean up these object references before we leave.
RELEASE loMainObject, loCurrentObject

RETURN loObjectInfo


*******************************************************
*******************************************************
PROCEDURE GetFileInfoForEditWindow
*******************************************************
*******************************************************
* F.K.A. GenES_GetFileInfo
*****************************************************************
*
* Returns information about the file that the user is currently editing.
*
* Returns an object containing information about the file.  The properties
* and their contents are:
*
*	icFileName		  -	The name of the file most recently edited.
*	icFileType		  -	The type of file being edited, "C"lass, "F"orm, or
*						"P"rogram.
*	icClassName		  -	The name of the class being edited.  Empty for forms
*						and programs.
*	icObjectName	  -	For a form or class, the name of the object whose
*						method code is being edited.  Empty for forms.
*	icMethodName	  -	For a form or class, the name of the method being
*						edited.  Empty for programs.
*	icSelectedText	  -	Stores any text that the user has selected in the
*						edit window.
*	icSelectedLine	  -	Stores the line number where the cursor is positioned
*						in the edit window.
*	icErrorMessage	  -	If the file info can't be determined or the user is
*						is not in an appropriate editor window, contains an
*						error message.
*
* NOTE: If you intend to use the values in the icSelectedText and
*	icSelectedLine properties, DO NOT run this utility from the Command
*	Window, because they will contain the text and line number for the
*	Command Window, NOT the editor window.  Use a menu item, or call it
*	from an ON KEY LABEL instead.
*
* EXAMPLE:
*	loFileInfo = GetFileInfoForEditWindow()
*	IF EMPTY( loFileInfo.icFileType )
*		*-- No file info found.  Find out why.
*		WAIT WINDOW loFileInfo.icErrorMessage
*	ENDIF
*
* See also: XXPROCDT.PRG/GetObjectPathToMethod
*****************************************************************
LOCAL lnWHandle, ;
	laEnv[25], ;
	loFileInfo AS Relation, ;
	lcWOnTop, ;
	lcReadOnly, ;
	loDesigners, ;
	llMethodWindow, ;
	llCommandWindow, ;
	lcOldClipText

*-- Create an object to store information about the file the user is currently
*-- working on.
loFileInfo = CREATEOBJECT( "Relation" )
loFileInfo.AddProperty( "icFileName", "" )
loFileInfo.AddProperty( "icFileType", "" )
loFileInfo.AddProperty( "icClassName", "" )
loFileInfo.AddProperty( "icObjectName", "" )
loFileInfo.AddProperty( "icMethodName", "" )
loFileInfo.AddProperty( "icSelectedText", "" )
loFileInfo.AddProperty( "inSelectedLine", 0 )
loFileInfo.AddProperty( "icErrorMessage", "" )

*-- Attempt to get a handle for the current window, so that we can implement
*-- some advanced features.  We need FoxTools to get the handle.
lnWHandle = IIF( X2SETLIB( "FOXTOOLS" ), _WOnTop(), 0 )

*-- The Designer and code editing windows will have the string " [Read Only]"
*-- appended to the name of the file or method if the file is flagged
*-- read-only (usually because source control is being used).  Make sure
*-- that string is stripped off of the window captions that we use.
*-- Note that WONTOP() currently returns an uppercase string, but we'll cover
*-- our backside in case that changes in the future.  It would also be nice
*-- to get the window title in its original case, but that doesn't appear to
*-- be possible with standard VFP functions.
lcReadOnly = "[READ ONLY]"
lcWOnTop = ALLTRIM( STRTRAN( UPPER( WONTOP() ), lcReadOnly ) )

*-- Initialize the flags that indicates if the most recent editor window is
*-- a method editor window, and if the current window is the Command Window.
llMethodWindow = .F.
llCommandWindow = .F.

DO CASE
	CASE EMPTY( lnWHandle ) OR EMPTY( _EdGetEnv( lnWhandle, @laEnv ) ) ;
			OR ( laEnv[25] = 0 )
		*-- We can't get a window handle, or can't retrieve environment info
		*-- about the editor window, or we're in the Command Window.  Make
		*-- some best guesses about what window we are in.
		*-- Note that it is better not to run this utility from the Command
		*-- Window, however, most of it will still work because WONTOP()
		*-- ignores the Command Window.
		llCommandWindow = ( TYPE( "laEnv[25]" ) = "N" AND laEnv[25] = 0 )
		DO CASE
			CASE NOT ( "." $ lcWOnTop )
				*-- This is not likely to be a program or method edit window.

			CASE INLIST( UPPER( JUSTEXT( lcWOnTop ) ), "PRG", "H", "TXT" )
				*-- This is a program file, or one of a couple common text
				*-- files.  We'll accept them as valid files.
				loFileInfo.icFileType = "P"

			CASE ( LENC( JUSTEXT( lcWOnTop ) ) > 3 ) ;
					OR INLIST( UPPER( JUSTEXT( lcWOnTop ) ), "BOX", "CLS" )
				*-- This must be a method editing window.  Box() and Cls()
				*-- are currently the only VFP methods that are 3 characters
				*-- (or less).
				llMethodWindow = .T.
		ENDCASE

	CASE laEnv[25] = 10
		*-- We're in a Form or Class Designer method edit window.
		llMethodWindow = .T.

	CASE INLIST( laEnv[25], 1, 2 )
		*-- We're in a MODIFY COMMAND (1) or MODIFY FILE (2) window.
		loFileInfo.icFileType = "P"
ENDCASE

DO CASE
	CASE loFileInfo.icFileType = "P"
		*-- The only information we need for a program is the file name.
		loFileInfo.icFileName = UPPER( lcWOnTop )

	CASE llMethodWindow
		*-- Get the file name from the last designer window that was activated.
		*-- It seems that even if an EDITSOURCE() command is used to open a
		*-- method editing window, the designer window corresponding to the
		*-- object and method being edited will be activated just before the
		*-- method window is open.  This causes the associated designer window
		*-- to always be the last designer window in the "window stack".
		loDesigners = FindClassFormDesignerWindows()
		IF loDesigners.inCount > 0
			*-- Copy the file name, class name, and file type from the
			*-- designer window info.
			loFileInfo.icFileName = loDesigners.iaFileInfo[loDesigners.inCount,1]
			loFileInfo.icClassName = loDesigners.iaFileInfo[loDesigners.inCount,2]
			loFileInfo.icFileType = loDesigners.iaFileInfo[loDesigners.inCount,3]

			*-- Store the object and method name from the current window.
			loFileInfo.icObjectName = JUSTSTEM( lcWOnTop )
			loFileInfo.icMethodName = JUSTEXT( lcWOnTop )
		ELSE
			*-- There appears to be a method editing window open, but we can't
			*-- find a designer window.
			loFileInfo.icErrorMessage = "A method editing window appears to be " ;
					+ "selected, but there are no Class/Form Designer windows " ;
					+ "open."
		ENDIF

	OTHERWISE
		*-- The user must not have been in a valid window.
		loFileInfo.icErrorMessage = "This utility is only valid in Program " ;
				+ "or Class/Form Method editing windows"
ENDCASE

*-- If a valid edit window is selected, get the selected text and line number.
IF EMPTY( loFileInfo.icFileType ) OR llCommandWindow
	*-- The selected text is irrelevant in non-editor windows, or in the
	*-- Command Window.
ELSE
	*-- Save the current contents of the clipboard, so that we can restore them
	*-- later, then clear the clipboard and copy any selected text to it.
	lcOldClipText = _CLIPTEXT
	_CLIPTEXT = ""
	SYS( 1500, "_MED_COPY", "_MEDIT" )

	*-- Store the selected text in the file info object, and get the current
	*-- line number for the edit window.
	loFileInfo.icSelectedText = _CLIPTEXT
	loFileInfo.inSelectedLine = GetCurrentLineNumber()

	*-- Restore the original clipboard contents.
	_CLIPTEXT = lcOldClipText
ENDIF

RETURN loFileInfo


*******************************************************
*******************************************************
PROCEDURE FindClassFormDesignerWindows
*******************************************************
*******************************************************
* F.K.A. GenES_GetFCDesignerInfo
*****************************************************************
*
* This procedure retrieves information about all Class or Form Designer
* windows that the user has open.
*
* Returns an object containing information about the open designers.  The
* properties and their contents are:
*
*	inCount			  -	The number of Class/Form Designers open.
*	iaFileInfo[n,1]	  -	The name of the Form or Class Library.
*						Note that this array is populated in the order that
*						the windows were activated, so the last row in the
*						array contains info about the last designer window
*						that was activated.
*	iaFileInfo[n,2]	  -	The name of the Class being edited (empty for forms).
*	iaFileInfo[n,3]	  -	The type of file being edited, "C"lass or "F"orm.
*	icErrorMessage	  - If no file info was returned, contains an error message.
*
* EXAMPLE:
*	loDesignerWindows = FindClassFormDesignerWindows()
*	IF loDesignerWindows.inCount = 0
*		WAIT WINDOW loDesignerWindows.icErrorMessage
*	ENDIF
*
* See also: XXPROCDT.PRG/GetFileInfoForEditWindow
*****************************************************************
LOCAL loDesignerInfo AS Relation, ;
	lcClassDesigner, ;
	lcFormDesigner, ;
	lcDesignerName, ;
	lcReadOnly, ;
	lnWindowCount, ;
	lcFileName, ;
	lcClassName, ;
	xx

*-- Create an object to return info about all open Class/Form Designers.
*-- (An Empty class would be handy right about now...)
loDesignerInfo = CREATEOBJECT( "Relation" )
loDesignerInfo.AddProperty( "inCount", 0 )
loDesignerInfo.AddProperty( "iaFileInfo[3]", "" )
loDesignerInfo.AddProperty( "icErrorMessage", "" )

DO CASE
  CASE VERSION(3)=="34"   &&& Spanish
    lcClassDesigner = "DISE�ADOR DE CLASES - "
    lcFormDesigner = "DISE�ADOR DE FORMULARIOS - "
    lcReadOnly = "[SOLO LECTURA]"
  CASE VERSION(3) = "49"  &&& German
    lcClassDesigner = "KLASSEN-DESIGNER - "
    lcFormDesigner = "FORMULAR-DESIGNER - "
    lcReadOnly = "[READ ONLY]"   &&& this is likely bogus, but I don't know the German
  OTHERWISE
    lcClassDesigner = "CLASS DESIGNER - "
    lcFormDesigner = "FORM DESIGNER - "
    lcReadOnly = "[READ ONLY]"
ENDCASE


*-- Find out how many windows are open in VFP, and initialize an array to
*-- store their names.
lnWindowCount = WCHILD( "SCREEN" )
LOCAL ARRAY laWindows[lnWindowCount]

*-- Populate the array with the titles of all the windows.
laWindows[1] = UPPER( ALLTRIM( WCHILD( "SCREEN", 0 ) ) )
FOR xx = 2 TO lnWindowCount
	laWindows[xx] = UPPER( ALLTRIM( WCHILD( "SCREEN", 1 ) ) )
ENDFOR

*-- The Class/Form Designer currently being edited will be the last one that
*-- was activated, so work backward through the windows until we find one.
FOR xx = 1 TO lnWindowCount
	*-- Skip and windows that aren't Class or Form Designer windows.
	DO CASE
		CASE LEFTC( laWindows[xx], LENC( lcClassDesigner ) ) == lcClassDesigner
			*-- This is a Class Designer window.  Save the name so we can
			*-- strip it out easier.
			lcDesignerName = lcClassDesigner

		CASE LEFTC( laWindows[xx], LENC( lcFormDesigner ) ) == lcFormDesigner
			*-- This is a Form Designer window.  Save the name so we can strip
			*-- it out easier.
			lcDesignerName = lcFormDesigner

		OTHERWISE
			*-- This isn't a window that we're interested in, so skip it.
			LOOP
	ENDCASE

	*-- Update the designer window count, and add a row to the file info array.
	loDesignerInfo.inCount = loDesignerInfo.inCount + 1
	DIMENSION loDesignerInfo.iaFileInfo[loDesignerInfo.inCount,3]

	*-- Strip off any read-only text, and strip off the designer name.
	lcFileName = ALLTRIM( STRTRAN( laWindows[xx], lcReadOnly ) )
	lcFileName = ALLTRIM( STRTRAN( lcFileName, lcDesignerName ) )

    IF INLIST(LEFTC( lcDesignerName, 5 ),"CLASS","DISE�", "KLASS")   &&& English, Spanish, German
		*-- The window title of a Class Designer contains the file name
		*-- followed by the class name in parentheses.  Separate the file
		*-- and class name.
		lcClassName = SUBSTR( lcFileName, RATC( "(", lcFileName ) + 1 )
		lcClassName = ALLTRIM( LEFTC( lcClassName, LENC( lcClassName ) - 1 ) )
		lcFileName = ALLTRIM( LEFTC( lcFileName, RATC( "(", lcFileName ) - 1 ) )
	ELSE
		*-- Form Designers just display the form name.
		lcClassName = ""
	ENDIF

	*-- Save the file and class name in the designer info object.
	loDesignerInfo.iaFileInfo[loDesignerInfo.inCount,1] = lcFileName
	loDesignerInfo.iaFileInfo[loDesignerInfo.inCount,2] = lcClassName
	loDesignerInfo.iaFileInfo[loDesignerInfo.inCount,3] = LEFTC( lcDesignerName, 1 )
ENDFOR

IF EMPTY( loDesignerInfo.inCount )
	*-- It looks like this method was called by mistake.
	loDesignerInfo.icErrorMessage = "There are no Class or Form Designer " ;
			+ "windows open."
ENDIF

RETURN loDesignerInfo


*******************************************************
*******************************************************
PROCEDURE FindObjectInParent
*******************************************************
*******************************************************
* F.K.A. GenES_FindObject
*****************************************************************
*
* Searches the Object collection of a specified parent object, looking for an
* item with the specified name.  This function will call itself recursively
* (if necessary), drilling down into each Object collection until it finds
* the specified object.  This appears to be pretty fast, but be aware that
* the more members there are in the parent object, the longer it could take
* to find the specified object.
*
* PLEASE NOTE: This function returns an object reference to the *FIRST* object
* it finds that meets the specified criteria.  This means that if there is more
* than one member object with the same criteria (such as column headers), there's
* a good chance that the object reference you get will not be the correct one.
* To increase your chances of an accurate match, be sure to include the
* tcMethodName and tuStringOrLine parameters.
*
* PARAMETERS:
*		  toParent(R) -	An object reference to a parent object, whose members
*						are to be searched for a particular item name.
*	  tcObjectName(R) -	The item name to search for among the member objects.
*	  tcMethodName(O) -	The name of a method name which the object we're looking
*						for must have.
*	tuStringOrLine(O) -	A string to search for in the code for the specified
*						method, or a line number for the method code.
*
* RETURNS:
*	An object which meets the specified criteria.  Returns .NULL. if the item
*	can't be found in any of the Object collections.
*
*****************************************************************
LPARAMETERS toParent, tcObjectName, tcMethodName, tuStringOrLine

LOCAL loObject, ;
	loItem, ;
	lcMethodName

*-- Initialize the object variable, and format the object and method names.
loObject = .NULL.
tcObjectName = UPPER( ALLTRIM( tcObjectName ) )
lcMethodName = IIF( VARTYPE( tcMethodName ) = "C", ALLTRIM( tcMethodName ), "" )

*-- If the parent object has an Objects collection, drill down into it looking
*-- for the specified object.
IF PEMSTATUS( toParent, "Objects", 5 )
	*-- Look for the specified item in the Objects collection.
	FOR EACH loItem IN toParent.Objects
		DO CASE
			CASE NOT UPPER( ALLTRIM( loItem.Name ) ) == tcObjectName
				*-- This is not the correct object.

			CASE EMPTY( lcMethodName )
				*-- No method name was specified, so just assume that this is
				*-- the item that we're looking for.  Use it.
				loObject = loItem
				EXIT

			CASE VerifyObjectMethod( loItem, lcMethodName, tuStringOrLine )
				*-- This object meets the additional criteria that were
				*-- specified, so use it.
				loObject = loItem
				EXIT
		ENDCASE
	ENDFOR

	IF ISNULL( loObject )
		*-- The item is not a member of the parent object, so drill down into
		*-- each item, checking any members that they have.
		FOR EACH loItem IN toParent.Objects
			loObject = FindObjectInParent( loItem, tcObjectName, lcMethodName, ;
					tuStringOrLine )
			IF ISNULL( loObject )
			ELSE
				*-- A match was found in one of the member objects.
				EXIT
			ENDIF
		ENDFOR
	ENDIF
ENDIF

*-- Just in case...
RELEASE loItem

RETURN loObject


*******************************************************
*******************************************************
PROCEDURE VerifyObjectMethod
*******************************************************
*******************************************************
* F.K.A. GenES_CheckObjectMethod
*****************************************************************
*
* This function checks if the passed object contains the specified method,
* and if the method code contains the specified search string, or has the
* specified number of lines of code.
*
* PARAMETERS:
*		  toObject(R) -	An object to check.
*		  tcMethod(R) -	The name of the method to look for.
*	tuStringOrLine(O) -	A string which must exist in the method code,
*						 OR
*						a line number (indicating the minimum number of
*						lines of code in the method).
*						NOTE: the string search is NOT case-sensitive.
*
* RETURNS:
*	.T. if the method exists in the passed object, and contains the
*		specified text or number of lines.  If the tuStringOrLine parameter
*		is omitted, returns .T. if the method simply exists in the object.
*	.F. if the first two parameters are invalid, or the criteria can't be
*		met.
*
* EXAMPLE:
*	FOR EACH loMember IN Thisform.Objects
*		IF VerifyObjectMethod( loMember, "Init", "RETURN .F." )
*			*-- The member has an Init method, which contains RETURN .F.
*		ELSE
*			*-- The member either has no Init method, or it doesn't RETURN .F.
*		ENDIF
*	ENDFOR
*
* See also: XXPROCDT.PRG/FindObjectInParent
*****************************************************************
LPARAMETERS toObject, tcMethod, tuStringOrLine

LOCAL llFoundIt, ;
	lcMethodCode, ;
	laCodeLines[1], ;
	lnLineCount

*-- Make sure the object and method name are valid.
IF ( VARTYPE( toObject ) = "O" ) AND ( VARTYPE( tcMethod ) = "C" ) ;
		AND NOT EMPTY( tcMethod )
ELSE
	RETURN .F.
ENDIF

*-- Check if the object has the specified method.
IF PEMSTATUS( toObject, tcMethod, 5 )
ELSE
	RETURN .F.
ENDIF

*-- Read the method code contained in the specified method.
lcMethodCode = toObject.ReadMethod( tcMethod )

DO CASE
	CASE ( VARTYPE( tuStringOrLine ) = "C" ) AND NOT EMPTY( tuStringOrLine )
		*-- A search string was passed, so check if the string exists in the
		*-- method code.
		llFoundIt = ( ATCC( tuStringOrLine, lcMethodCode ) > 0 )

	CASE ( VARTYPE( tuStringOrLine ) = "N" ) AND ( tuStringOrLine > 0 )
		*-- A line number was specified.  Get the number of lines of method
		*-- code, and compare it to the line number.
		lnLineCount = ALINES( laCodeLines, lcMethodCode )
		llFoundIt = ( lnLineCount >= tuStringOrLine )

	OTHERWISE
		*-- We can't get any more specific about whether this is the correct
		*-- object or not, so report that we found it.
		llFoundIt = .T.
ENDCASE

RETURN llFoundIt


*******************************************************
*******************************************************
PROCEDURE AddStringDelimiters
*******************************************************
*******************************************************
* F.K.A. GenES_FormatSearchString
*****************************************************************
*
* This procedure formats a passed string for macro-expanding or pasting
* into a function call, such as XXDTES().  If the raw text contains
* carriage returns and/or line feeds, the first non-empty "line" in the
* text will be extracted and formatted, so that the result will never
* contain carriage returns or line feeds.
*
* PARAMETERS:
*	 tcRawText(R) -	The raw, unformatted text that needs to be formatted
*					for use in a function call.
*
* RETURNS:
*	The formatted string, with text delimiters added.
*	An empty string if no text was passed.
*	.NULL. if the string already contains all the text delimiters.
*
* EXAMPLE:
*	lcSearchString = AddStringDelimiters( lcSearchString )
*	IF NOT ISNULL( lcSearchString ) AND NOT EMPTY( lcSearchString )
*		KEYBOARD "DO XXDTSearch WITH " + lcSearchString + "{ENTER}" PLAIN CLEAR
*	ENDIF
*
* See also: XXPROCDT.PRG/GenES
*****************************************************************
LPARAMETERS tcRawText

LOCAL laLines[1], ;
	lnLineCount, ;
	xx, ;
	lcDelimitedString, ;
	llDoubleQuotes, ;
	llSingleQuotes, ;
	llBrackets

lcDelimitedString = ""

*-- If the search string is the wrong data type, or empty, we're done.
IF NOT ( VARTYPE( tcRawText ) = "C" ) OR EMPTY( tcRawText )
	RETURN lcDelimitedString
ENDIF

*-- The selected text may contain multiple lines.  Separate the lines
*-- and use the first non-empty line.
lnLineCount = ALINES( laLines, tcRawText )
FOR xx = 1 TO lnLineCount
	IF EMPTY( laLines[xx] )
	ELSE
		lcDelimitedString = laLines[xx]
		EXIT
	ENDIF
ENDFOR

*-- If no text was selected, we're done.
IF EMPTY( lcDelimitedString )
	RETURN lcDelimitedString
ENDIF

*-- Look for any string delimiters within the search string.
llDoubleQuotes = ( ["] $ lcDelimitedString )
llSingleQuotes = ( ['] $ lcDelimitedString )
llBrackets = ( "[" $ lcDelimitedString ) OR ( "]" $ lcDelimitedString )

DO CASE
	CASE !llDoubleQuotes
		*-- Double quotes aren't being used in the selected text, so use
		*-- them as delimiters.
		lcDelimitedString = ["] + lcDelimitedString + ["]

	CASE !llSingleQuotes
		*-- Single quotes aren't being used in the selected text, so use
		*-- them as delimiters.
		lcDelimitedString = ['] + lcDelimitedString + [']

	CASE !llBrackets
		*-- Brackets aren't being used in the selected text, so use them as
		*-- delimiters.
		lcDelimitedString = "[" + lcDelimitedString + "]"

	OTHERWISE
		*-- The selected text contains all the delimiters.  We can't create
		*-- a usable search string.
		lcDelimitedString = .NULL.
ENDCASE

RETURN lcDelimitedString



*******************************************************
*******************************************************
FUNCTION GetCurrentLineNumber
*******************************************************
*******************************************************
* F.K.A. GenES_GetCurrentLineNumber
*****************************************************************
*
* This function uses FoxTools functions to lookup the line number for the
* current cursor position in the current edit window.  This code is more
* reliable than my previous KEYBOARD kludge, and has the added benefit of
* leaving the cursor and selected text exactly as it was before this
* function was called.
*
* NOTE: Since this function recognizes ALL VFP windows, including the
* Command Window, this function must be called from a hot-key or menu item,
* otherwise the window handle that it retrieves may not be for the correct
* window.
*
* RETURNS:
*	The line number where the cursor is positioned in the current edit window.
*	Zero if the line number can't be determined.
*
* EXAMPLE:
*	lnLineNo = GetCurrentLineNumber()
*
* See also: XXPROCDT.PRG/GetFileInfoForEditWindow
*
* Thanks to Christof Lange, whose IntelliSenseX utility pointed me to the
* functions that I needed to make this work, and directed me to George
* Tasker's excellent TOOLHELP.HLP file which documents these functions.
*
*****************************************************************

	LOCAL lnWHandle, ;
		lnCursorPos, ;
		lnLineNo

	IF X2SETLIB( "FOXTOOLS" )
		*-- Attempt to get a handle for the current window.  Note that since
		*-- we can get the handle of ANY window, including the Command Window,
		*-- this function will only work correctly from a hot-key, or menu
		*-- item.
		lnWHandle = _WOnTop()
		IF lnWHandle <= 0
			RETURN 0
		ENDIF

		*-- Get the cursor position within the edit window.
		lnCursorPos = _EdGetPos( lnWHandle )
		IF lnCursorPos < 0
			*-- The cursor position could not be determined.
			lnLineNo = 0
		ELSE
			*-- The line number is zero based, so adjust it after we
			*-- retrieve it.
			lnLineNo = _EdGetLNum( lnWHandle, lnCursorPos )
			lnLineNo = lnLineNo + IIF( lnLineNo >= 0, 1, 0 )
		ENDIF
	ELSE
		*-- FoxTools could not be loaded for some reason.  We'll use KEYBOARD
		*-- commands to attempt to select and copy everything above the
		*-- cursor into the clipboard, then count the number of lines of text
		*-- in the clipboard.  This technique isn't as "clean" a solution as
		*-- using FoxTools, and for reasons that I can't explain, this
		*-- technique is not 100% reliable, either.  Occasionally it will
		*-- return 1, regardless of where you are in the code window.
		*-- Nevertheless, it seems to work well most of the time, and seems
		*-- to always work if you try it a second time.

		LOCAL lcOldClipText, ;
			llStartOfLine, ;
			lnLineCount, ;
			laLines[1]

		*-- Save the current contents of the clipboard, so that we can
		*-- restore them later.
		lcOldClipText = _CLIPTEXT

		*-- Copy everything from the cursor position to the beginning of the
		*-- line into the clipboard.  We will use this information to
		*-- determine if the cursor is at the beginning of the line.  Notice
		*-- the DOEVENTS, which forces these keystrokes to be executed
		*-- immediately, otherwise the _CLIPTEXT variable won't be updated
		*-- in time for subsequent code to check its contents.
		_CLIPTEXT = ""
		KEYBOARD '{SHIFT+HOME}' PLAIN CLEAR
		DOEVENTS
		SYS( 1500, "_MED_COPY", "_MEDIT" )
		llStartOfLine = ( LEN( _CLIPTEXT ) = 0 )

		*-- Now, select everything from the cursor position to the top of the
		*-- code window, and copy it into the clipboard.  This will be used
		*-- to determine the line number for the current cursor position.
		_CLIPTEXT = ""
		KEYBOARD '{CTRL+SHIFT+HOME}' PLAIN CLEAR
		DOEVENTS
		SYS( 1500, "_MED_COPY", "_MEDIT" )

		IF LEN( _CLIPTEXT ) = 0
			*-- Nothing was selected.  That means we're in line 1, position 1.
			lnLineNo = 1
		ELSE
			*-- Use the ALINES() function to get a count of the number of
			*-- lines of text copied to the clipboard.  If the cursor was at
			*-- the beginning of the line, that line will be counted, so we
			*-- need to add one to the count to compensate.
			lnLineCount = ALINES( laLines, _CLIPTEXT )
			lnLineNo = lnLineCount + IIF( llStartOfLine, 1, 0 )
		ENDIF

		IF LEN( _CLIPTEXT ) > 0
			*-- If some text was selected by the above code, we don't want to
			*-- leave it that way.  The right-arrow key (and LeftArrow
			*-- and Home) will de-select the code and return the cursor to its
			*-- original position in the code.
			KEYBOARD '{RIGHTARROW}' PLAIN CLEAR
			DOEVENTS
		ENDIF

		*-- Restore the original clipboard contents.
		_CLIPTEXT = lcOldClipText
	ENDIF

RETURN lnLineNo



*******************************************************
*******************************************************
PROCEDURE DataSource_Open_Browse_ModifyStructure 
*******************************************************
*******************************************************
*
*  This proc is intended for use by VMP developer tools/
*  wizards/builders to allow the calling code to either
*    OPEN/USE
*    BROWSE (default)
*    MODIFY STRUCTURE 
*  a Data Source (XXFWNTDS.VCX/ctrDataSource.icDataSource).
*  This proc is called from several places in the custom
*  VMP Data Source Builder
*    MODIFY CLASS frmBuilderDataSources OF XXWB
*  and several times in the Click of the <Browse> button 
*  of the frmGetTable class
*    XXDTES("XXTOOLS.VCX","DataSource_Open_Browse_ModifyStructure","frmgettable","cmdBrowse.CLICK")
*
*  However, you can use this proc outside the context of
*  a VMP n-Tier services Data Source by passing the right
*  combination of parameters.
*
*  NOTE that the passed tcTableName is left OPEN/USED
*  when this routine finishes, if it has been successful.
*  Ditto any containing .DBC, which is also left open
*  if this proc is successful (when the passed table/view
*  is contained in a .DBC database).
*
*  If the table/data source is a remote table, pass in
*  the icConnectionClass and/or icConnectionClassAlt
*  properties of the Data Source, and that connection
*  information is used here to connect to the remote
*  data:
*    DO DataSourceBrowseOrModifyStructure WITH ;
*        "TableName",.f.,.t.,"CN_DSNLess,NTDECUS.VCX"
*  -OR-
*  you can establish the connection yourself ahead
*  of time, and pass a valid connection handle here:
*    *  establish the connection manually here
*    DO DataSourceBrowseOrModifyStructure IN XXPROCDT WITH "TableName",.f.,.t.,.f.,.f.,3
*  -OR-
*  if you have a connection class that inherits from either
*  XXFWNTDS.VCX/cusCBRemoteView or XXFWNTDS.VCX/cusCBSQLConnect,
*  use that object the same way it would be in your app:
*    LOCAL lnHandle, loConn
*    loConn = NEWOBJECT("MyConnectionClass","MyConnetionClassVCX")
*    lnHandle = loConn.Connect()
*    DO DataSourceBrowseOrModifyStructure IN XXPROCDT WITH "TableName",.f.,.t.,.f.,.f.,3
*  -OR-
*  if you have a connection class that inherits from either
*  XXFWNTDS.VCX/cusCBRemoteView or XXFWNTDS.VCX/cusCBSQLConnect,
*  pass that class as the parameter:
*    DO DataSourceBrowseOrModifyStructure WITH ;
*        "TableName",.f.,.t.,"CN_DSNLess,NTDECUS.VCX"
*  
*
*
*  RETURN values:
*    1: Success
*   -1: Unable to open VFP table
*   -2: Connection information tcConnClass/tcConnClassAlt
*         not passed 
*   -3: Connection information tcConnClass/tcConnClassAlt
*         is invalid, likely because the specified class
*         and/or VCX cannot be found
*   -4: Unable to instantiate the class indicated in
*         the connection parameter information
*   -5: Connection specified in the connection parameter
*         information is based on an existing connection,
*         which is not available from a DT/WB
*   -6: Cannot successfully execute the Connect() method
*         of the connection object
*   -7: SQLEXEC() failed
* -100: Attempt to MODIFY STRUCTURE of a remote table
*
*  lParameters 
*        tcTableName (R) ctrDataSource.icDataSource value -
*                          name of the table to be BROWSEd,
*                          in one of the following formats:
*                            "Table.DBF"      (free)
*                            "Database!Table" (contained)
*                            "Table"          (remote)
*          tcAction  (O) Indicates the action to be performed
*                          here:
*                            "BROWSE" (default)
*                            "MODIFY"
*                            "OPEN"
*  tlHandleMessaging (O) Handle messaging here?
*                        Defaults to .F. - no
*                        If you pass this parameter as .T.,
*                          then you probably don't care
*                          about the RETURN value, because
*                          messaging has already been 
*                          handled here if something goes
*                          wrong
*        tcConnClass (O) ctrDataSource.icConnectionClass
*                          format:  "ClassName,VCXFileName" 
*                        If tcAction is passed as "MODIFY",
*                          this proc RETURNs -100 for a remote
*                          table, in which case this parameter
*                          is ignored
*     tcConnClassAlt (O) ctrDataSource.icConnectionClassAlt
*                          format:  "ClassName,VCXFileName"
*                        If tcAction is passed as "MODIFY",
*                          this proc RETURNs -100 for a remote
*                          table, in which case this parameter
*                          is ignored
*           tnHandle (O) Existing connection handle to a 
*                          remote database for use in 
*                          scenarios where the tcConnClass 
*                          specifies a class inheriting from 
*                          cusCBExistingHandle
*                        If passed, tcConnClass/tcConnClassAlt
*                          are ignored
*                        If tcAction is passed as "MODIFY",
*                          this proc RETURNs -100 for a remote
*                          table, in which case this parameter
*                          is ignored
*                        This parameter allows this proc to
*                          be used when tcTableName is in a
*                          remote database, but you want to
*                          call this proc directly, without
*                          even having a Data Source or 
*                          connection class/information -- 
*                          just establish the connection,
*                          then call this proc
*
LPARAMETERS tcTableName, tcAction, tlHandleMessaging, ;
            tcConnClass, tcConnClassAlt, tnHandle
SET ASSERTS ON 
IF EMPTY(tcTableName)
  ASSERT .f. message "tcTableName parameter is required"
  RETURN 
ENDIF
LOCAL lcAlias, lnSelect, llRemote, lcOnError, llError, ;
      lcTableName, lcConnClass, lcConnClassAlt, ;
      lnRetVal, lcConnClassName, lcConnClassVCX, ;
      llHandleMessaging, llOK, laVCXClasses[1], xx, yy, ;
      loConnection, laClasses[1], lnHandle, lcSetClassLib, ;
      lcAction, lcDatabase, lcSetDatabase, llDBCWasUsed
lcTableName = UPPER(ALLTRIM(tcTableName))      
lcDatabase = SPACE(0)
lcSetDatabase = SET("DATABASE")
lcConnClass = SPACE(0)
lcConnClassAlt = SPACE(0)
llHandleMessaging = .f.
DO CASE
  CASE NOT VARTYPE(tcAction) = "C" OR EMPTY(tcAction)
    lcAction = "BROWSE"   &&& default
  CASE UPPER(ALLTRIM(tcAction)) = "O"
    lcAction = "OPEN"
  CASE UPPER(ALLTRIM(tcAction)) = "M"
    lcAction = "MODIFY"
  CASE UPPER(ALLTRIM(tcAction)) = "B"
    lcAction = "BROWSE"
  OTHERWISE
    lcAction = "BROWSE"   &&& default
ENDCASE
IF VARTYPE(tlHandleMessaging) = "L"
  llHandleMessaging = tlHandleMessaging
ENDIF
IF VARTYPE(tcConnClass) = "C" AND NOT EMPTY(tcConnClass)
  lcConnClass = UPPER(ALLTRIM(tcConnClass))
ENDIF
IF VARTYPE(tcConnClassAlt) = "C" AND NOT EMPTY(tcConnClassAlt)
  lcConnClassAlt = UPPER(ALLTRIM(tcConnClassAlt))
ENDIF
IF VARTYPE(tnHandle) = "N" 
  IF tnHandle > 0
    lnHandle = INT(tnHandle)
   ELSE
    ASSERT .f. message ;
         "tnHandle has been passed as an invalid " + ;
         "integer: " + TRANSFORM(tnHandle)
    RETURN 
  ENDIF
 ELSE
  lnHandle = 0
ENDIF

lcSetClassLib = SET("CLASSLIB")
lnRetVal = 1
lnSelect = SELECT(0)
lcOnError = ON("ERROR")

DO CASE
  CASE OCCURS("!",lcTableName)>0
    lcAlias = GETWORDNUM(lcTableName,2,"!")
    lcDatabase = GETWORDNUM(lcTableName,1,"!")
  CASE JUSTEXT(lcTableName) = "DBF"
    lcAlias = JUSTSTEM(lcTableName)
  OTHERWISE
    llRemote = .t.
    lcAlias = lcTableName
ENDCASE
USE IN SELECT(lcAlias)

DO CASE
  *************************************************
  CASE lcAction = "MODIFY" AND llRemote
  *************************************************
    *
    *  not applicable/supported
    *
    lnRetVal = -100
  *************************************************
  CASE llRemote   &&& AND lcAction = "BROWSE" or "OPEN"
  *************************************************
    *
    *  remote table -- get connection information, if any
    *    
    IF lnHandle > 0
      *
      *  tnHandle was passed here as an explicit (and
      *  supposedly valid handle) -- nothing to do WRT
      *  establishing a connection 
      * 
      *
     ELSE
      *
      *  default behavior - tnHandle was not passed
      * 
      IF EMPTY(lcConnClass) AND EMPTY(lcConnClassAlt)
        lnRetVal = -2
       ELSE
        *
        *  process connection information
        *
        IF NOT EMPTY(lcConnClassAlt)
          lcConnClassName = GETWORDNUM(lcConnClassAlt,1,",")
          lcConnClassVCX = GETWORDNUM(lcConnClassAlt,2,",")
         ELSE
          lcConnClassName = GETWORDNUM(lcConnClass,1,",")
          lcConnClassVCX = GETWORDNUM(lcConnClass,2,",")
        ENDIF
        llOK = FILE(lcConnClassVCX)
        IF NOT llOK
          lnRetVal = -3
         ELSE
          IF AVCXCLASSES(laVCXClasses,lcConnClassVCX)>0
            IF ASCAN(laVCXClasses,lcConnClassName,1,-1,1,1)=0
              lnRetVal = -3
              llOK = .f.
            ENDIF
          ENDIF
        ENDIF
        IF llOK
          *
          *  lcConnClassVCX and lcConnClassName exist,
          *  see what connection information they contain
          *  that we will use to connect to lcAlias
          *
          IF NOT X8SETCLS(lcConnClassVCX)
            *
            *  at this point, this should never happen, but
            *  you know how Murphy's Law works...
            *
            lnRetVal = -3
           ELSE
            loConnection = CREATEOBJECT(lcConnClassName)
            IF NOT VARTYPE(loConnection) = "O"        
              *
              *  unable to instantiate the connection object --
              *  if you get here, then you may have already
              *  received an ASSERT dialog during the 
              *  attempted instantiation -- you will have to 
              *  either remedy the problem or refrain from
              *  whatever Developer Tool/Wizard/Builder option
              *  got you here
              *
              lnRetVal = -4
             ELSE
              ACLASS(laClasses,loConnection)
              IF ASCAN(laClasses,"cusCBExistingHandle",1,-1,1,1)>0
                *
                *  connection class requires an existing handle
                * 
                lnRetVal = -5
               ELSE
                *
                *  connection class inherits from either
                *    cusCBRemoteView
                *    cusCBSQLConnect            
                *
                IF loConnection.Connect() < 1
                  lnRetVal = -6
                 ELSE
                  lnHandle = loConnection.GetHandle()
                ENDIF
              ENDIF
            ENDIF
          ENDIF   &&& NOT X8SETCLS(lcConnClassVCX)
        ENDIF   &&& llOK
      ENDIF   &&& EMPTY(lcConnClass) AND EMPTY(lcConnClassAlt)
    ENDIF   &&& lnHandle > 0
    IF lnHandle > 0
      *
      *  use connection information to grab the first 10,000
      *  records in lcAlias -- restrict the number to save
      *  time on the SELECT, since you most likely only
      *  want to check the structure, not a specific 
      *  individual record
      *
      llError = .f.
      ON ERROR llError = .t.
      lnRetVal = SQLEXEC(lnHandle,"SELECT TOP 10000 * FROM " + lcAlias + " ORDER BY 1",lcAlias)
      ON ERROR &lcOnError
      IF lnRetVal < 1
        lnRetVal = -7
      ENDIF
    ENDIF    
  *************************************************
  OTHERWISE
  *************************************************
    *
    *  VFP table/view, either BROWSE or MODIFY STRUCTURE
    * 
    IF NOT EMPTY(lcDatabase)
      IF DBUSED(JUSTSTEM(JUSTFNAME(lcDatabase)))
        llDBCWasUsed = .t.
       ELSE
        llError = .f.
        ON ERROR llError = .t.
        OPEN DATABASE (lcDatabase)
        ON ERROR &lcOnError
        IF llError
          lnRetVal = -8
        ENDIF
      ENDIF
    ENDIF
    IF lnRetVal = 1
      LOCAL llView
      IF NOT EMPTY(lcDatabase)
        SET DATABASE TO (JUSTSTEM(JUSTFNAME(lcDatabase)))
        llView = INDBC(lcAlias,"VIEW")
      ENDIF
      llError = .f.
      ON ERROR llError = .t.
      IF llView
        USE (lcTableName) IN 0 SHARED ALIAS (lcAlias)
       ELSE
        USE (lcTableName) IN 0 SHARED NOUPDATE ALIAS (lcAlias)
      ENDIF       
      ON ERROR &lcOnError
      IF llError
        lnRetVal = -1
       ELSE
        IF llView
          *
          *  make it readonly
          *
          CURSORSETPROP("SendUpdates",.f.,lcAlias)
        ENDIF
      ENDIF
    ENDIF
ENDCASE

DO CASE
  ******************************************
  CASE lnRetVal < 1
  ******************************************
    *
    *  problem above, do nothing
    *
    USE IN SELECT(lcAlias)
  ******************************************
  CASE lcAction = "OPEN"
  ******************************************
    *
    *  tcAction was passed as "OPEN", do nothing
    *  because this indicates you just wanted this
    *  routine to open/use the table, but do nothing
    *  else
    *
    SELECT (lcAlias)
  ******************************************
  CASE lcAction = "MODIFY"
  ******************************************
    SELECT (lcAlias)
    MODIFY STRUCTURE   &&& ReadOnly
  ******************************************
  OTHERWISE   &&& lcAction = "BROWSE"
  ******************************************
    SELECT (lcAlias)  
    IF lnHandle > 0 AND RECCOUNT() = 10000
      WAIT WINDOW "For performance reasons, only the first 10,000 records have been retrieved" NOWAIT TIMEOUT 5
    ENDIF
    ACTIVATE SCREEN 
    BROWSE LAST NOCAPTIONS   &&& ReadOnly
ENDCASE

IF VARTYPE(loConnection) = "O" 
  *
  *  note that when tcTableName is remote, the connection 
  *  gets closed here, when a connection is not passed in 
  *  explicitly as tnHandle -- while the lcAlias cursor
  *  created here remains open, its ConnectHandle is
  *  now bogus and cannot be used further
  *
  loConnection.Close()
  loConnection = .NULL.
ENDIF

IF VARTYPE(tnHandle) = "N" AND tnHandle > 0
  llError = .f.
  ON ERROR llError = .t.
  SQLDISCONNECT(tnHandle)
  ON ERROR &lcOnError
ENDIF

SET CLASSLIB TO &lcSetClassLib 
IF NOT EMPTY(lcConnClassName)
  CLEAR CLASS (lcConnClassName)
ENDIF

IF INLIST(lcAction,"BROWSE","MODIFY")
  SELECT (lnSelect)
 ELSE
  *
  *  lcAction = "OPEN" -- leave it the current
  *  work area
  *
ENDIF

IF lnRetVal < 1 AND NOT llDBCWasUsed AND DBUSED(JUSTSTEM(JUSTFNAME(lcDatabase)))
  SET DATABASE TO (JUSTSTEM(JUSTFNAME(lcDatabase)))
  CLOSE DATABASE
ENDIF
SET DATABASE TO &lcSetDatabase

IF lnRetVal < 0
  LOCAL laError[1]
  AERROR(laError)
ENDIF

DO CASE
  *************************************************
  CASE lnRetVal = 1
  *************************************************
    *
    *  success, nothing to do
    *
  *************************************************
  CASE NOT llHandleMessaging
  *************************************************
    *
    *  lnRetVal < 1, but no messaging handled here
    *  
  *************************************************
  CASE lnRetVal = -1  
  *************************************************
    MESSAGEBOX("Unable to find/open " + lcAlias + ", perhaps " + ;
               "because it is not in the VFP path at the moment.", ;
               48, "Please Note")
  *************************************************
  CASE lnRetVal = -2
  *************************************************
    MESSAGEBOX(lcAlias + " is apparently a remote table, " + ;
               "but you have not specified icConnectionClass " + ;
               "or icConnectionClassAlt -- without connection " + ;
               "information, the DataSource cannot be accessed/" + ;
               "BROWSEd.", ;
               48, "Please Note")
  *************************************************
  CASE lnRetVal = -3
  *************************************************
    MESSAGEBOX("Unable to find connection information, " + ; 
               "because either " + lcConnClassVCX + " or " + ;
               "the specified " + lcConnClassName + " class does " + ;
               "not exist, perhaps because " + lcConnClassVCX + ;
               " is not in the VFP path.", ;
               48, "Please Note")
  *************************************************
  CASE lnRetVal = -4
  *************************************************
    MESSAGEBOX("Unable to instantiate the connection class " + ; 
               "indicated in " + lcConnClassName + "," + lcConnClassVCX + ".", ;
               48, "Please Note")
  *************************************************
  CASE lnRetVal = -5
  *************************************************
    MESSAGEBOX("The specified connection class " + ;
               lcConnClassName + "," + lcConnClassVCX + ;
               " inherits from cusDBExistingHandle, which " + ;
               "works well in a running application, but " + ;
               "indicates a connection that is not available " + ;
               "from a Developer Tool/Wizard/Builder.", ;   
               48, "Please Note")
  *************************************************
  CASE lnRetVal = -6
  *************************************************
    MESSAGEBOX("An instance of the specified connection class " + ;
               lcConnClassName + "," + lcConnClassVCX + ;
               " has been successfully instantiated, " + ;
               "but is unable to successfully execute its " + ;
               "Connect() method.", ;
               48, "Please Note")
  *************************************************
  CASE lnRetVal = -7 AND NOT EMPTY(lcConnClassName)
  *************************************************
    FOR xx = 1 TO ALEN(laError,1)
      ACTIVATE SCREEN
      FOR yy = 1 TO ALEN(laError,2)
        ? laError[xx,yy]
      ENDFOR
    ENDFOR
    MESSAGEBOX("An instance of the specified connection class " + ;
               lcConnClassName + "," + lcConnClassVCX + ;
               " has been successfully instantiated, " + ;
               "but the SQLEXEC() attempted here:" + ;
               CHR(13) + ;
               "SQLEXEC(" + TRANSFORM(lnHandle) + ",'SELECT * FROM " + lcAlias + "','" + lcAlias + "')" + ;
               CHR(13) + ;
               "failed.", ;
               48, "Please Note")
  *************************************************
  CASE lnRetVal = -7 AND lnHandle > 0
  *************************************************
    FOR xx = 1 TO ALEN(laError,1)
      ACTIVATE SCREEN
      FOR yy = 1 TO ALEN(laError,2)
        ? laError[xx,yy]
      ENDFOR
    ENDFOR
    MESSAGEBOX("The SQLEXEC() attempted here:" + ;
               CHR(13) + ;
               "SQLEXEC(" + TRANSFORM(lnHandle) + ",'SELECT * FROM " + lcAlias + "','" + lcAlias + "')" + ;
               CHR(13) + ;
               "failed.", ;
               48, "Please Note")
  *************************************************
  CASE lnRetVal = -100
  *************************************************
    MESSAGEBOX(lcAlias + " is apparently a remote table -- " + ;
               "MODIFY STRUCTURE is not supported for " + ;
               "remote tables.", ;
               48, "Please Note")
ENDCASE

RETURN lnRetVal




*******************************************************
*******************************************************
PROCEDURE GetAppPrefix
*******************************************************
*******************************************************
*
*  RETURNs the 2-char application prefix based on
*  the files in the current folder. 
*  RETURNs SPACE(0) if unable to determine the 2-char
*  application prefix.
*
LOCAL laFiles[1], laPrefixes[1,2], laPJXs[1], laMains[1], ;
      lcRetVal
lcRetVal = SPACE(0)      
IF ADIR(laFiles) = 0
  RETURN lcRetVal
ENDIF

*
*  set THIS.icAppPrefix based on the files we find
*  in the current folder, for use in saving/restoring
*  preferences
*

LOCAL xx, lnPrefixes, lnPJXs, lnMains, lnPrefix, lcPrefix, ;
      lcFile, lcMain, lcPJX
STORE 0 TO lnPrefixes, lnPJXs, lnMains
STORE SPACE(0) TO lcPrefix, lcFile
FOR xx = 1 TO ALEN(laFiles,1)
  lcFile = UPPER(laFiles[xx,1])
  IF RIGHTC(lcFile,8) == "MAIN.PRG"
    *
    *  track ??MAIN.PRGs
    *
    lnMains = lnMains + 1
    DIMENSION laMains[lnMains]
    laMains[lnMains] = laFiles[xx,1]
  ENDIF
  IF LENC(JUSTSTEM(lcFile))>=2
    *
    *  track 2-char prefixes for filenames
    *
    lcPrefix = LEFTC(JUSTSTEM(lcFile),2)
    lnPrefix = ASCAN(laPrefixes,lcPrefix,1,0,1,8)
    IF lnPrefix = 0 
      *
      *  new prefix, add it 
      *
      lnPrefixes = lnPrefixes + 1
      DIMENSION laPrefixes[lnPrefixes,2]
      laPrefixes[lnPrefixes,1] = lcPrefix
      laPrefixes[lnPrefixes,2] = 1
     ELSE
      *
      *  existing prefix, increment its count
      *
      laPrefixes[lnPrefix,2] = laPrefixes[lnPrefix,2]+1
    ENDIF
  ENDIF
  IF RIGHTC(lcFile,4) == ".PJX"
    *
    *  track projects
    *
    lnPJXs = lnPJXs + 1
    DIMENSION laPJXs[lnPJXs]
    laPJXs[lnPJXs] = laFiles[xx,1]
  ENDIF
ENDFOR

ASORT(laPrefixes,2,-1,1)
IF ALEN(laPrefixes,1) = 1 OR laPrefixes[1,2]>laPrefixes[2,2]
  *
  *  either all the files in the current folder all
  *  start with the same 2 letters
  *    OR
  *  there is one 2-char prefix that is used for 
  *  more files in this folder than any other 2-char
  *  prefix
  *
  lcRetVal = laPrefixes[1,1]
 ELSE
  lcRetVal = SPACE(0)
  *
  *  at least two rows of laPrefixes have the same
  *  number of 2-char prefixes, so there is no clear
  *  majority of files with the same 2-char prefix
  *
  IF ALEN(laMains,1) = 1
    lcMain = laMains[1]
   ELSE
    lcMain = SPACE(0)
  ENDIF
  IF ALEN(laPJXs,1) = 1
    lcPJX = laPJXs[1]
   ELSE
    lcPJX = SPACE(0)
  ENDIF
  IF EMPTY(lcRetVal) ;
       AND NOT EMPTY(lcPJX) AND NOT EMPTY(lcMain) ;
       AND LEFTC(lcPJX,2) == LEFTC(lcMain,2)
    *
    *  there is only one ??MAIN.PRG and one ??.PJX,
    *  and they both start with the same 2 letters,
    *  use that
    *
    lcRetVal = LEFTC(lcPJX,2)
  ENDIF
  *
  *  eliminate the laPrefixes rows that have fewer
  *  total number of files than the first row (leaving
  *  the laPrefixes rows that are all tied for first
  *  place with the same number of files)
  *
  LOCAL lnCount
  lnCount = 1
  FOR xx = 2 TO ALEN(laPrefixes,1)
    IF laPrefixes[xx,2] = laPrefixes[1,2]
      lnCount = lnCount + 1
     ELSE
      EXIT 
    ENDIF
  ENDFOR
  DIMENSION laPrefixes[lnCount,2]
  IF EMPTY(lcRetVal) AND NOT EMPTY(lcMain) 
    *
    *  if any of the prefixes match the first 2
    *  characters of the single ??MAIN.PRG, then
    *  go with that one
    *
    FOR xx = 1 TO ALEN(laPrefixes,1)
      IF LEFTC(lcMain,2) == laPrefixes[xx,1]
        lcRetVal = laPrefixes[xx,1]
        EXIT
      ENDIF
    ENDFOR
  ENDIF
  IF EMPTY(lcRetVal) AND NOT EMPTY(lcPJX) 
    *
    *  if any of the prefixes match the first 2
    *  characters of the single ??.PJX, then
    *  go with that one
    *
    FOR xx = 1 TO ALEN(laPrefixes,1)
      IF LEFTC(lcPJX,2) == laPrefixes[xx,1]
        lcRetVal = laPrefixes[xx,1]
        EXIT
      ENDIF
    ENDFOR
  ENDIF
  *
  *  looks like we struck out, lcRetVal = SPACE(0)
  *
ENDIF
RETURN lcRetVal




*******************************************************
*******************************************************
PROCEDURE GetSourceFolder
*******************************************************
*******************************************************
*
*  figure out where files of the type indicated by the
*  passed tcExt file extension are located, 
*  determined in this order of precedence:
*    1- if there is an Application.ActiveProject, 
*       whether XXWBNEWF.PRG was called by the project
*       hook or not, the folder containing the most
*       .SCXs in the project 
*    2- the subdirectory (if any) of the current 
*       directory that contains the most files of the
*       type indicated by the tcExt extension
*       (must contain at least one tcExt file), unless
*       the CURDIR() contains more files of the type
*       indicated by tcExt than that subdirectory, in 
*       which case RETURN FULLPATH(CURDIR())
*    3- the current FULLPATH(CURDIR()) folder
*
LPARAMETERS tcExt, tlProjectHook

ASSERT VARTYPE(tcExt) = "C" AND NOT EMPTY(tcExt) ;
     MESSAGE "tcExt file extension parameter is required"

IF NOT VARTYPE(tcExt) = "C" OR EMPTY(tcExt)
  RETURN FULLPATH(CURDIR())
ENDIF

LOCAL lcPath, xx, yy, lcExt
lcPath = SPACE(0)
lcExt = UPPER(ALLTRIM(tcExt))
IF LEFTC(lcExt,1) = "."
  lcExt = SUBSTRC(lcExt,2)
ENDIF
*
*  if this .PRG has been called from the VMP project
*  hook, RETURN the folder that contains the majority
*  of the lcExt files in the project
*
IF tlProjectHook OR TYPE("Application.ActiveProject.BaseClass") = "C"
  *
  *  note that VFP 6 is very slow traversing the
  *  ActiveProject.Files collection, so we're not
  *  using this logic for VFP 6
  *
  LOCAL lcPath, laPaths[1,2], lnRows, lnRow
  laPaths[1,1] = SPACE(0)
  laPaths[1,2] = 0
  FOR EACH loFile IN Application.ActiveProject.Files
    IF UPPER(JUSTEXT(loFile.Name)) == lcExt
      lcPath = UPPER(JUSTPATH(loFile.Name))
      IF NOT EMPTY(lcPath)
        lnRow = 0
        IF ASCAN(laPaths,lcPath)>0
          FOR xx = 1 TO ALEN(laPaths,1)
            IF laPaths[xx,1] == lcPath
              lnRow = xx   &&& lcPath is already in this row of laPaths
              EXIT
            ENDIF
          ENDFOR
        ENDIF      
        IF lnRow = 0
          *
          *  lcPath is not already in laPaths - add it
          *
          IF NOT EMPTY(laPaths[1])  
            DIMENSION laPaths[ALEN(laPaths,1)+1,2]
          ENDIF
          lnRows = ALEN(laPaths,1)  
          laPaths[lnRows,1] = lcPath
          laPaths[lnRows,2] = 1
         ELSE
          *
          *  lcPath is already in laPaths -- increment Column2
          *
          laPaths[lnRow,2] = laPaths[lnRow,2] + 1
        ENDIF
      ENDIF   &&& NOT EMPTY(lcPath)
    ENDIF   &&& file of type indicated by lcExt
  ENDFOR   &&& loFile
  *  sort descending, first row has the most
  ASORT(laPaths,2,0,1)  
  IF NOT EMPTY(laPaths[1,1]) AND laPaths[1,2]>0
    lcPath = ADDBS(laPaths[1,1])
  ENDIF
ENDIF   &&& tlProjectHook OR Application.ActiveProject

IF EMPTY(lcPath)
  *
  *  see if we can find files of the type indicated 
  *  by lcExt in any folders under
  *  the current folder, if so, the one containing
  *  the most lcExt files wins (but must contain at
  *  least one lcExt file)
  *
  LOCAL laDirs[1], laEXTs[1], laEXTDirs[1,2]
  laEXTDirs[1,1] = FULLPATH(CURDIR())
  laEXTDirs[1,2] = 0
  IF ADIR(laDirs,"","D")>0
    FOR xx = 1 TO ALEN(laDirs,1)
      IF NOT ISALPHA(laDirs[xx,1])
        *  system
        LOOP 
      ENDIF
      CD (laDirs[xx,1])
      IF ADIR(laEXTs,"*."+lcExt)>0
        *  add this directory to the array of
        *  subdirectories containing .EXTs
        IF NOT EMPTY(laEXTDirs[1,1])
          DIMENSION laEXTDirs[ALEN(laEXTDirs,1)+1,2]
        ENDIF
        lnRow = ALEN(laEXTDirs,1)
        laEXTDirs[lnRow,1] = FULLPATH(CURDIR())
        laEXTDirs[lnRow,2] = ALEN(laEXTs,1)
      ENDIF
      CD ..
    ENDFOR   &&& subdirectories under CURDIR()
  ENDIF   &&& CURDIR() contains subdiretories
  ASORT(laEXTDirs,2,0,1)   &&& descending
  IF NOT EMPTY(laEXTDirs[1,1]) AND laEXTDirs[1,2]>0
    lcPath = ADDBS(laEXTDirs[1,1])
    *
    *  but if CURDIR() has more lcExt files, use it instead
    *  (if we found no laEXTDirs[] above, then we're
    *  about to set lcPath to FULLPATH(CURDIR()) in
    *  the next IF..ENDIF block anyway...)
    *
    LOCAL lcBackup
    lcBackup = "BACKUP\"
    IF (RIGHTC(lcPath, LENC(lcBackup)) = lcBackup) OR (ADIR(junk,"*."+lcExt) > laEXTDirs[1,2])
      lcPath = FULLPATH(CURDIR())
    ENDIF
  ENDIF
ENDIF   &&& empty(lcPath)

IF EMPTY(lcPath)
  *
  *  if we didn't find anything above, use the current 
  *  folder
  *
  lcPath = FULLPATH(CURDIR())
ENDIF

RETURN lcPath




*******************************************************
*******************************************************
PROCEDURE ViewAllInheritedCodeFromDesigner
*******************************************************
*******************************************************
*
*  view all code inherited by the Object::Method
*  currently being edited in the Form/Class Designer
*  method snippet editor
*
LOCAL loInfo, loInheritedCode, lnSelect, lcSetSafety, ;
      lcPRGName, lcWindow, loMP

loInfo = GetSnippetObjectInfo()
IF NOT VARTYPE(loInfo) = "O"
  *
  *  method snippet editor not open, user has been
  *  messaged by a WAIT WINDOW in GetSnippetObjectInfo()
  *
  RETURN
ENDIF

IF EMPTY(loInfo.icObjectClassLibrary) ;
     OR UPPER(loInfo.icObjectClass) == UPPER(loInfo.ioObject.BaseClass)
  MESSAGEBOX(loInfo.ioObject.Name + " is an instance of a VFP base class.", ;
             48, "Please Note")
  RETURN 
ENDIF

IF NOT loInfo.ilMethodIsInherited
  MESSAGEBOX("The " + loInfo.icMethodName + " method " + ;
             "has been added at the " + ;
             IIF(UPPER(loInfo.ioObject.BaseClass)=="FORM" AND UPPER(JUSTEXT(loInfo.icMetaFile))="SCX","form instance",loInfo.ioObject.Name) + ;
             " level -- there is no inherited code to view.", ;
             48,"Please Note")
  RETURN 
ENDIF

IF NOT FILE("X8SETCLS.PRG") ;
     OR NOT X8SETCLS("XXTOOLS.VCX") ;
     OR NOT X8SETCLS("XXTOOLS.VCX") 
  MESSAGEBOX("Unable to locate XXTOOLS/XXFW.VCX",16,"Please Note")
  RETURN 
ENDIF

IF X8SETCLS("XXFWUTIL.VCX")
  loMP = CREATEOBJECT("cusPushPopMousePointers",.t.)
ENDIF

WAIT WINDOW "Preparing method code, please be patient..." NOWAIT NOCLEAR 

loInheritedCode = CREATEOBJECT("cusInheritedCode")   &&& in XXTOOLS.VCX
IF NOT VARTYPE(loInheritedCode) = "O" 
  RELEASE loMP
  WAIT CLEAR 
  MESSAGEBOX("Unable to instantiate XXTOOLS.VCX/cusInheritedCode, " + ;
             "and therefore unable to process inherited code", ;
             48,"Please Note")
  RETURN 
ENDIF

IF UPPER(loInfo.icMethodName) = "ZREADME"
  lcPRGName = "zReadMe.PRG"
 ELSE
  lcPRGName = "Inherit.PRG"
ENDIF     
lnSelect = SELECT(0)
lcSetSafety = SET("SAFETY")
SET SAFETY OFF 

STRTOFILE(loInheritedCode.GetTheCode(loInfo.icMethodName, ;
                                     loInfo.icObjectClass, ;
                                     loInfo.icObjectClassLibrary), ;
                                     lcPRGName)

lcWindow = SPACE(0)
IF SET("RESOURCE") = "OFF"
  lcWindow = "window Inherited"
  DEFINE WINDOW Inherited ;
       AT 0,0 SIZE 20,70 ;
       FONT "Courier New",10 STYLE "B" ; 
       SYSTEM CLOSE FLOAT ZOOM GROW ;
       TITLE "Inherited code for " + loInfo.ioObject.Name + "." + loInfo.ioObject.lcMethodName + " (Read Only)"
  MOVE WINDOW Inherited CENTER 
ENDIF

RELEASE loMP

IF WEXIST(lcPRGName)
  RELEASE WINDOWS (lcPRGName) 
ENDIF
WAIT CLEAR 
MODIFY COMMAND (lcPRGName) NOEDIT NOMENU RANGE 1,1 NOWAIT &lcWindow

SET SAFETY &lcSetSafety
SELECT (lnSelect)

RETURN 




*******************************************************
*******************************************************
PROCEDURE GetSnippetObjectInfo
*******************************************************
*******************************************************
*
*  called by the ViewAllInheritedCodeFromDesigner() function
*  and from the "IFND" IntelliSense script
*
*  RETURN an object of information about the
*  object/method in the topmost current method snippet
*  editor window (or .NULL. if there is not a method
*  snippet window open at the moment)
*                 ioObject - objref to the object whose method is open in the method snippet editor
*            icObjectClass - Class on which ioObject is based 
*     icObjectClassLibrary - .VCX containing the class on which ioObject is based
*            icParentClass - Parent class of ioObject
*     icParentClassLibrary - .VCX containing the parent class of ioObject
*        icClassOrInstance - "CLASS" or "INSTANCE", identifying ioObject
*             icMethodName - name of the method open in the snippet editor
* icContainershipHierarchy - full containership hierarchy for ioObject
*               icMetaFile - .SCX/.VCX containing ioObject
*     ioOutermostContainer - outermost container object for ioObject
*     ilOutermostContainer - logical flag indicating whether ioObject is itself the outermost container in icMetaFile
*   ilVFPBaseClassInstance - logical flag indicating whether ioObject is an instance of a VFP base class
*       iaInheritedMethods - array of the names of methods that are inherited by ioObject (not methods defined at ioObject level when ioObject is a class)
*      ilMethodIsInherited - logical flag indicating if icMethodName is inherited by ioObject (otherwise icMethodName is defined at the ioObject/class level)
*               icComments - the following comment text:
LOCAL lcComments
TEXT TO lcComments NOSHOW 

When THIS.icClassOrInstance = "CLASS":
	THIS.ilOutermostContainer is always .T. 
	THIS.ioObject.Name, THIS.ioObject.Class, and THIS.ioOuterMostContainer.Name are identical 
	THIS.ioObject.ClassLibrary and THIS.icMetaFile are identical
	None of the following apply or are redundant:
		THIS.ilOutermostContainer (always .T.)
		THIS.icContainershipHierarchy (same as THIS.ioObject.Name)		
		THIS.icMetaFile (same as THIS.ioObject.ClassLibrary)

When THIS.icClassOrInstance = "INSTANCE":
	THIS.icClass AND THIS.icParentClass are identical
	THIS.icClassLibrary and THIS.icParentClassLibrary are identical

When THIS.icClassOrInstance = "INSTANCE" AND THIS.ioObject.BaseClass = "FORM"
	THIS.ilOutermostContainer = .t.
	THIS.icMetaFile = the name of the .SCX file
	THIS.ioObjectClass, THIS.ioObjectClassLibrary are indentical to THIS.ioOutermostContainer.Class, THIS.ioOutermostContainerClassLibrary
	THIS.icContainershipHierarchy is identical to THIS.ioObject.Name

When THIS.ilVFPBaseClassInstance = .t.
	THIS.icClassLibrary is EMPTY()
	THIS.icParentClassLibrary is EMPTY()

ENDTEXT

LOCAL loObject, lcMethod, lcObjName, lcClassOrInstance, ;
      lcClass, lcClassLibrary, ;
      lcParentClass, lcParentClassLibrary, ;
      laSelObj[1], loSelObj, loOuterContainer, ;
      lcVCXSCXFileName, laSelObj3[1], lcWontop, ;
      lcContainershipHierarchy, loInfo
loInfo = CREATEOBJECT("Empty")
loInfo = .NULL.
lcWontop = GetDesignerWontop()
IF EMPTY(lcWontop) ;
     OR SPACE(1) $ lcWontop ;
     OR OCCURS(".",lcWontop) = 0
  IF NOT X7ISAPIF("MessageBeep")
    DECLARE Long MessageBeep IN USER32.DLL Long uType 
  ENDIF
  MessageBeep(0)
  WAIT WINDOW "Method snippet editor is not active"
  RETURN loInfo
ENDIF
lcObjName = UPPER(GETWORDNUM(lcWontop,1,"."))
lcMethod = UPPER(JUSTEXT(lcWontop))
IF ASELOBJ(laSelObj,1) = 0
  WAIT WINDOW "Method snippet editor is not active"
  RETURN loInfo
ENDIF
ASELOBJ(laSelObj3,3)
loOuterContainer = laSelObj3[1]
lcVCXSCXFileName = UPPER(laSelObj3[2])
loSelObj = laSelObj[1]
IF UPPER(loSelObj.Name) == lcObjName
  loObject = loSelObj
 ELSE
  RELEASE laSelObj
  DIMENSION laSelObj[1]
  DO CASE
    CASE UPPER(loSelObj.BaseClass) == "FORM" ;
         AND VARTYPE(loSelObj.Parent) = "O" ;
         AND UPPER(loSelObj.Parent.Name) == lcObjName
      loObject = loSelObj.Parent
    CASE ASELOBJ(laSelObj) = 1 ;
         AND UPPER(laSelObj[1].Name) == lcObjName
      loObject = laSelObj[1]
    OTHERWISE
      IF TYPE("loSelObj."+lcObjName) = "O"
        loObject = loSelObj.&lcObjName
       ELSE
        loObject = FindMemberOfContainer(lcObjName,loSelObj)
	  ENDIF
	ENDCASE
ENDIF
RELEASE laSelObj, loSelObj, laSelObj3
IF ISNULL(loObject)
  WAIT WINDOW "Unable to locate object"
  RETURN loInfo
ENDIF
IF UPPER(loObject.Class) == UPPER(loObject.Name)
  *
  *  loObject is a class definition --
  *  loObject.Name, loObject.Class and loOuterContainer.Name are identical
  *  loObject.ClassLibrary and lcVCXSCXFileName are the same
  *
  lcClass = UPPER(loObject.Class)
  lcParentClass = UPPER(loObject.ParentClass)
  lcClassLibrary = lcVCXSCXFileName
  lcParentClassLibrary = UPPER(loObject.ClassLibrary)
  lcClassOrInstance = "CLASS"
  lcContainershipHierarchy = UPPER(loObject.Name)
 ELSE
  *
  *  loObject is an instance --
  *  lcClass and lcParentClass are identical, 
  *  lcClassLibrary and lcParentClassLibrary are identical
  *
  lcClass = UPPER(loObject.Class)
  lcParentClass = UPPER(loObject.Class)
  lcClassLibrary = UPPER(loObject.ClassLibrary)
  lcParentClassLibrary = UPPER(loObject.ClassLibrary)
  lcClassOrInstance = "INSTANCE"
  lcContainershipHierarchy = loObject.Name
  LOCAL loPO
  loPO = loObject
  DO WHILE .t.
    IF TYPE("loPO.PARENT") = "O" AND NOT UPPER(loPO.PARENT.BaseClass) == "FORMSET"
      loPO = loPO.PARENT
      lcContainershipHierarchy = loPO.Name + "." + lcContainershipHierarchy
     ELSE
      EXIT 
    ENDIF
  ENDDO
  loPO = .NULL.
ENDIF
*
*  the absence of a "." in lcContainershipHierarchy
*  indicates loObject is either a class or a form
*  (form is the outermost level of containership),
*  existence of a "." in lcContainershipHierarchy
*  indicates loObject is a member (and therefore an
*  instance)
*

*!*	MESSAGEBOX("Object - " + UPPER(loObject.Name) + CHR(13) + ;
*!*	           "Object Class - " + lcClass + CHR(13) + ;
*!*	           "Object Class Library - " + lcClassLibrary + CHR(13) + ;
*!*	           "Parent Class - " + lcParentClass + CHR(13) + ;
*!*	           "Parent Class Library - " + lcParentClassLibrary + CHR(13) + ;
*!*	           "Instance or Class - " + lcClassOrInstance + CHR(13) + ;
*!*	           "Method - " + lcMethod + CHR(13) + ;
*!*	           "VFP base class - " + IIF(EMPTY(lcClassLibrary),".t.",".f.") + CHR(13) + ;
*!*	           "     none of the following applies to CLASS" + CHR(13) + ;
*!*	           "IsOuter - " + TRANSFORM(IIF(UPPER(loOuterContainer.Name) == UPPER(loObject.Name),.t.,.f.)) + CHR(13) + ;
*!*	           "CH - " + lcContainershipHierarchy + CHR(13) + ;
*!*	           "Containing meta file - " + lcVCXSCXFileName + CHR(13) + ;
*!*	           "OuterContainer - " + UPPER(loOuterContainer.Name) + CHR(13) + ;
*!*	           "OuterContainerClass " + loOuterContainer.Class + CHR(13) + ;
*!*	           "OuterContainerClassLibrary " + loOuterContainer.ClassLibrary)

loInfo = CREATEOBJECT("Empty")
ADDPROPERTY(loInfo,"ioObject",loObject)
ADDPROPERTY(loInfo,"icObjectClass",lcClass)
ADDPROPERTY(loInfo,"icObjectClassLibrary",lcClassLibrary)
ADDPROPERTY(loInfo,"icParentClass",lcParentClass)
ADDPROPERTY(loInfo,"icParentClassLibrary",lcParentClassLibrary)
ADDPROPERTY(loInfo,"icClassOrInstance",lcClassOrInstance)
ADDPROPERTY(loInfo,"icMethodName",lcMethod)
ADDPROPERTY(loInfo,"icContainershipHierarchy",lcContainershipHierarchy)
ADDPROPERTY(loInfo,"icMetaFile",lcVCXSCXFileName)
ADDPROPERTY(loInfo,"ioOutermostContainer",loOuterContainer)
ADDPROPERTY(loInfo,"ilOutermostContainer",UPPER(loOuterContainer.Name) == UPPER(loObject.Name))
ADDPROPERTY(loInfo,"ilVFPBaseClassInstance",EMPTY(lcClassLibrary))
ADDPROPERTY(loInfo,"icComments",lcComments)
ADDPROPERTY(loInfo,"iaInheritedMethods[1]")
*
*  populate loInfo.iaInheritedMethods
*
LOCAL laMembers[1], lnCount, lnIM
lnIM = 0
AMEMBERS(laMembers,loObject,1)
FOR lnCount = 1 TO ALEN(laMembers,1)
  IF NOT INLIST(UPPER(laMembers[lnCount,2]),"METHOD","EVENT")
    LOOP 
  ENDIF
  IF PEMSTATUS(loObject,laMembers[lnCount,1],6)
    lnIM = lnIM + 1
    DIMENSION loInfo.iaInheritedMethods[lnIM]
    loInfo.iaInheritedMethods[lnIM] = laMembers[lnCount,1]
  ENDIF
ENDFOR
ADDPROPERTY(loInfo,"ilMethodIsInherited",ASCAN(loInfo.iaInheritedMethods,lcMethod,1,-1,1,7)>0)
IF NOT VARTYPE(poInfo) = "U"
  poInfo = loInfo
ENDIF
RETURN loInfo

           


*******************************************************
*******************************************************
PROCEDURE GetDesignerWontop
*******************************************************
*******************************************************
*
*  called by the GetSnippetObjectInfo() function
* 
*  RETURN the WONTOP() Designer window string, if any
*
LOCAL lcWontop, lcWChild, lnCount
lcWontop = SPACE(0)
WCHILD(0)
FOR lnCount = 0 TO WCHILD()
  lcWChild = WCHILD(lnCount)
  IF EMPTY(lcWChild)
    EXIT
  ENDIF
  IF "." $ lcWChild OR UPPER(lcWChild) == "COMMAND"
    lcWontop = lcWChild
  ENDIF
ENDFOR
IF UPPER(lcWontop) == "COMMAND"
  RETURN lcWontop
ENDIF
IF EMPTY(lcWontop) AND "." $ Wontop()
  lcWontop = WONTOP()
ENDIF
RETURN lcWontop



*******************************************************
*******************************************************
PROCEDURE FindMemberOfContainer
*******************************************************
*******************************************************
*
*  called from the GetSnippetObjectInfo() function and
*  this FindMemberOfContainer() function
* 
*  recurse through the passed toContainer until
*  finding its member tcMemberName, RETURN an object
*  reference to that member
*
LPARAMETERS tcMemberName, toContainer
LOCAL lcMemberName, loObject, lnCount, laMembers[1]
IF EMPTY(tcMemberName) ;
     OR NOT VARTYPE(toContainer) = "O" ;
     OR AMEMBERS(laMembers,toContainer,2) = 0
  RETURN .NULL.
ENDIF
lcMemberName = UPPER(ALLTRIM(tcMemberName))
loObject = .NULL.
FOR lnCount = 1 TO ALEN(laMembers,1)
  IF UPPER(laMembers[lnCount]) == lcMemberName
    loObject = toContainer.&lcMemberName
   ELSE
    loObject=FindMemberOfContainer(lcMemberName,toContainer.&laMembers[lnCount])
  ENDIF
  IF VARTYPE(loObject) = "O" 
    EXIT
  ENDIF
ENDFOR
RETURN loObject

 
 
 
*******************************************************
*******************************************************
*  FoxResource class definition from the
*  \Tools\xSource\Toolbox\FoxResource.PRG,
*  plus an additional GetListOfSettings() method
*
*  Usage:
*!*	LOCAL oResource, lcToolbox
*!*	oResource = NEWOBJECT("FoxResource", "XXPROCDT.PRG")
*!*	oResource.Load("TOOLBOX")
*!*	oResource.GetListOfSettings()
*!*	lcToolbox = oResource.Get("TOOLBOXTable")
*!*	? lcToolbox
*
*  NOTE:  The parameter passed to the Load method must
*  be in all caps!  The parameter passed to the Get method
*  is NOT, however, case-sensitive.
*
*******************************************************
*******************************************************
           
* Abstract:
*   Class for add/retrieving values
*	from FoxUser resource file.
*

*!*	#define TESTMODE .T.
*!*	#if TESTMODE
*!*	CLEAR
*!*	o = NEWOBJECT("FoxResource", "FoxResource.prg")

*!*	o.set("Height", 100)
*!*	o.set("Name", "ryan")

*!*	o.Load("FOXPANE")

*!*	*!*	? o.set("Name", "Tiffini")
*!*	*!*	? o.save("TEST")

*!*	? o.get("Height")
*!*	? o.get("Name")


*!*	? o.save("FOXPANE")
*!*	#endif


*!*	RETURN

DEFINE CLASS FoxResource AS Custom
	PROTECTED oCollection
	
	oCollection  = .NULL.

	ResourceType = "PREFW"
	ResourceFile = ''
	
	PROCEDURE Init()
		THIS.oCollection = CREATEOBJECT("Collection")
		THIS.ResourceFile = SYS(2005)
	ENDPROC
	
	PROCEDURE Destroy()
		THIS.CloseResource()
	ENDPROC

	* Clear out all options
	FUNCTION Clear()
		THIS.oCollection.Remove(-1)
	ENDFUNC
	
	FUNCTION Set(cOption, xValue)
		TRY
			THIS.oCollection.Remove(UPPER(m.cOption))
		CATCH
		ENDTRY

		* doesn't exist yet, so add
		RETURN THIS.oCollection.Add(m.xValue, UPPER(m.cOption))
	ENDFUNC
	
	FUNCTION Get(cOption)
		LOCAL xValue
		
		TRY
			m.xValue = THIS.oCollection.Item(UPPER(m.cOption))
		CATCH
			m.xValue = .NULL.
		ENDTRY

		RETURN m.xValue
	ENDFUNC
	
	FUNCTION OpenResource()
		IF !(SET("RESOURCE") == "ON")
			RETURN .F.
		ENDIF

		IF !USED("FoxResource")
			IF FILE(THIS.ResourceFile)
				TRY
					USE (THIS.ResourceFile) ALIAS FoxResource IN 0 SHARED AGAIN
				CATCH
				ENDTRY
			ENDIF
		ENDIF
				
		RETURN USED("FoxResource")
	ENDFUNC
	
	FUNCTION CloseResource()
		IF USED("FoxResource")
			USE IN FoxResource
		ENDIF
	ENDFUNC
	
	PROCEDURE Save(cID, cName)
		LOCAL nSelect
		LOCAL cType
		LOCAL i
		LOCAL ARRAY aOptions[1]
		
		IF VARTYPE(m.cName) <> 'C'
			m.cName = ''
		ENDIF
		IF THIS.OpenResource()
			m.nSelect = SELECT()
			
			m.cType = PADR(THIS.ResourceType, LEN(FoxResource.Type))
			m.cID   = PADR(m.cID, LEN(FoxResource.ID))

			SELECT FoxResource
			LOCATE FOR Type == m.cType AND ID == m.cID AND Name == m.cName
			IF !FOUND()
				APPEND BLANK IN FoxResource
				REPLACE ; 
				  Type WITH m.cType, ;
				  Name WITH m.cName, ;
				  ID WITH m.cID, ;
				  ReadOnly WITH .F. ;
				 IN FoxResource
			ENDIF

			IF !FoxResource.ReadOnly
				IF THIS.oCollection.Count > 0
					DIMENSION aOptions[THIS.oCollection.Count, 2]
					FOR m.i = 1 TO THIS.oCollection.Count
						aOptions[m.i, 1] = THIS.oCollection.GetKey(m.i)
						aOptions[m.i, 2] = THIS.oCollection.Item(m.i)
					ENDFOR
					SAVE TO MEMO Data ALL LIKE aOptions
				ELSE
					BLANK FIELDS Data IN FoxResource
				ENDIF

				REPLACE ;
				  Updated WITH DATE(), ;
				  ckval WITH VAL(SYS(2007, FoxResource.Data)) ;
				 IN FoxResource
			ENDIF
		
			THIS.CloseResource()
			
			SELECT (m.nSelect)
		ENDIF
	ENDPROC
	
	PROCEDURE Load(cID, cName)
		LOCAL nSelect
		LOCAL cType
		LOCAL nCnt
		LOCAL i
		LOCAL ARRAY aOptions[1]
		
		IF VARTYPE(m.cName) <> 'C'
			m.cName = ''
		ENDIF
		
		* THIS.Clear()
		IF THIS.OpenResource()
			m.nSelect = SELECT()
			
			m.cType = PADR(THIS.ResourceType, LEN(FoxResource.Type))
			m.cID   = PADR(m.cID, LEN(FoxResource.ID))

			SELECT FoxResource
			LOCATE FOR Type == m.cType AND ID == m.cID AND Name == m.cName
			IF FOUND() AND !EMPTY(Data) AND ckval == VAL(SYS(2007, Data))
				RESTORE FROM MEMO Data ADDITIVE
				IF VARTYPE(aOptions[1,1]) == 'C'
					m.nCnt = ALEN(aOptions, 1)
					FOR m.i = 1 TO m.nCnt
						THIS.Set(aOptions[m.i, 1], aOptions[m.i, 2])
					ENDFOR
				ENDIF
			ENDIF

			THIS.CloseResource()			

			SELECT (m.nSelect)
		ENDIF
	ENDPROC

	FUNCTION GetData(cID, cName)
		LOCAL cData
		LOCAL nSelect
		LOCAL cType

		IF VARTYPE(m.cName) <> 'C'
			m.cName = ''
		ENDIF

		m.cData = .NULL.
		IF THIS.OpenResource()
			m.nSelect = SELECT()
			
			m.cType = PADR(THIS.ResourceType, LEN(FoxResource.Type))
			m.cID   = PADR(m.cID, LEN(FoxResource.ID))

			SELECT FoxResource
			LOCATE FOR Type == m.cType AND ID == m.cID AND Name == m.cName
			IF FOUND() AND !EMPTY(Data) && AND ckval == VAL(SYS(2007, Data))
				m.cData = FoxResource.Data
			ENDIF

			THIS.CloseResource()
	
			SELECT (m.nSelect)
		ENDIF
		
		RETURN m.cData
	ENDFUNC

	* save to a specific fieldname
	FUNCTION SaveTo(cField, cAlias)
		LOCAL i
		LOCAL nSelect
		LOCAL lSuccess
		LOCAL ARRAY aOptions[1]

		IF VARTYPE(m.cAlias) <> 'C'
			m.cAlias = ALIAS()
		ENDIF

		IF USED(m.cAlias)
			m.nSelect = SELECT()
			SELECT (m.cAlias)

			IF THIS.oCollection.Count > 0
				DIMENSION aOptions[THIS.oCollection.Count, 2]
				FOR m.i = 1 TO THIS.oCollection.Count
					aOptions[m.i, 1] = THIS.oCollection.GetKey(m.i)
					aOptions[m.i, 2] = THIS.oCollection.Item(m.i)
				ENDFOR
				SAVE TO MEMO &cField ALL LIKE aOptions
			ELSE
				BLANK FIELDS &cField IN FoxResource
			ENDIF
			SELECT (m.nSelect)
			m.lSuccess = .T.
		ELSE
			m.lSuccess = .F.
		ENDIF
		
		RETURN m.lSuccess
	ENDFUNC


	FUNCTION RestoreFrom(cField, cAlias)
		LOCAL i
		LOCAL nSelect
		LOCAL lSuccess
		LOCAL ARRAY aOptions[1]

		IF VARTYPE(m.cAlias) <> 'C'
			m.cAlias = ALIAS()
		ENDIF

		IF USED(m.cAlias)
			m.nSelect = SELECT()
			SELECT (m.cAlias)

			RESTORE FROM MEMO &cField ADDITIVE
			IF VARTYPE(aOptions[1,1]) == 'C'
				m.nCnt = ALEN(aOptions, 1)
				FOR m.i = 1 TO m.nCnt
					THIS.Set(aOptions[m.i, 1], aOptions[m.i, 2])
				ENDFOR
			ENDIF

			SELECT (m.nSelect)
			m.lSuccess = .T.
		ELSE
			m.lSuccess = .F.
		ENDIF
		
		RETURN m.lSuccess
	ENDFUNC



PROCEDURE GetListOfSettings
*
*  this additional method added by Drew Speedie,
*  to display a list of available settings that
*  can be passed to THIS.Get()
*
	ACTIVATE SCREEN
	CLEAR 
	FOR m.i = 1 TO THIS.oCollection.Count
		? THIS.oCollection.GetKey(m.i)
*!*			? THIS.oCollection.Item(m.i)
	ENDFOR
ENDPROC



ENDDEFINE





*******************************************************
*******************************************************
PROCEDURE GetVFPToolboxSetting
*******************************************************
*******************************************************
*
*  RETURNs the value of a VFP Toolbox setting stored in
*  the FoxUser.Data field for the FoxUser record where
*    Type = "PREFW" 
*      ID = "TOOLBOX"
*    Name = SPACE(0)
*
*  ? GetVFPToolboxSetting("ToolboxTable")
*  GetVFPToolboxSetting(.f.,.t.)
*           
*  simpler than using the FoxResource class above
*  when all you need to do is retrieve a setting
*
LPARAMETERS tcToolboxSetting as String, ; 
            tlGetListOfSettings as Boolean 

TRY 
USE (SYS(2005)) IN 0 ALIAS FoxUserToolbox again SHARED 
CATCH
ENDTRY

IF NOT USED("FoxUserToolbox")
  IF VARTYPE(tlGetListOfSettings) = "L" ;
       AND tlGetListOfSettings
    ACTIVATE SCREEN
    CLEAR 
    ? "Unable to determine TOOLBOX settings"
  ENDIF
  RETURN .NULL.
ENDIF

LOCAL m.lnSelect, m.luRetVal, m.lnRow, m.xx
m.lnSelect = SELECT(0)
m.luRetVal = .NULL.

SELECT FoxUserToolbox
LOCATE FOR UPPER(ALLTRIM(Type)) == "PREFW" ;
     AND UPPER(ALLTRIM(ID)) == "TOOLBOX" ;
     AND EMPTY(Name) 
IF FOUND()
  RESTORE FROM MEMO Data ADDITIVE
  DO CASE
*!*	    CASE TYPE("ALEN(aOptions)") = "N" ;
*!*	         AND VARTYPE(tlGetListOfSettings) = "L" ;
*!*	         AND tlGetListOfSettings = .t.
    CASE NOT VARTYPE(aOptions) = "U" AND X2IsArray(@aOptions) ;
         AND VARTYPE(tlGetListOfSettings) = "L" ;
         AND tlGetListOfSettings = .t.
      ACTIVATE SCREEN
      CLEAR 
      FOR m.xx = 1 TO ALEN(m.aOptions,1)
        ? m.aOptions[xx,1]
      ENDFOR         
*!*	    CASE TYPE("ALEN(aOptions)") = "N" 
    CASE NOT VARTYPE(aOptions) = "U" AND X2IsArray(@aOptions)
      m.lnRow = ASCAN(aOptions,tcToolboxSetting,1,-1,1,15)
      IF m.lnRow > 0
        m.luRetVal = aOptions[m.lnRow,2]
      ENDIF
*!*	    CASE NOT TYPE("ALEN(aOptions)") = "N" ;
*!*	         AND VARTYPE(tlGetListOfSettings) = "L" ;
*!*	         AND tlGetListOfSettings = .t.
    CASE (VARTYPE(aOptions) = "U" OR NOT X2IsArray(@aOptions)) ;
         AND VARTYPE(tlGetListOfSettings) = "L" ;
         AND tlGetListOfSettings = .t.
      ACTIVATE SCREEN
      CLEAR 
      ? "Unable to determine TOOLBOX settings"
*!*	    CASE NOT TYPE("ALEN(aOptions)") = "N" 
    CASE VARTYPE(aOptions) = "U" OR NOT X2IsArray(@aOptions)
  ENDCASE
ENDIF

USE IN FoxUserToolbox
SELECT(m.lnSelect)
RETURN m.luRetVal
           
           
           

*******************************************************
*******************************************************
PROCEDURE GetVMPBuildFromVMPWebSite
*******************************************************
*******************************************************
*  Retrieve VMP build number on main VMP page
LOCAL lcVMPPage, lnConnectToWebPage, lcLatestBuild, llError
lcVMPPage = [http://www.visionpace.com/VMPSite/default.asp]
WAIT WINDOW "Checking for an Internet connection to check for a new VMP build..." NOWAIT 
lnConnectToWebPage = X6ICC(lcVMPPage)
WAIT CLEAR 
lcLatestBuild = "Unable to determine latest VMP build number"
DO CASE
  *************************************************
  CASE lnConnectToWebPage = 0
  *************************************************
    lcLatestBuild = "Could not access the VMP web site" 
  *************************************************
  CASE lnConnectToWebPage = 1
  *************************************************
    LOCAL loIE AS InternetExplorer.Application  &&& InternetExplorer.Application.1
    WAIT WINDOW "Connecting to the VMP web site " + ;
                "to retrieve the number of the latest VMP build..." ;
                NOWAIT
    TRY 
    loIE = CREATEOBJECT("InternetExplorer.Application") &&& 'InternetExplorer.Application.1'
    CATCH 
    llError = .t.
    lcLatestBuild = "Unable to instantiate Internet Explorer to make the check"
    ENDTRY
    IF NOT llError
      LOCAL loRegExp AS VBScript_RegExp_55
      loRegExp = CREATEOBJECT('VBScript.RegExp')
*!*	      IF PEMSTATUS(loRegExp,"Multiline",5)   &&& DWS I couldn't get this to work, seems to be a timing issue
      IF VARTYPE(loRegExp.Multiline) = "L"
       ELSE
        llError = .t.
        lcLatestBuild = "The latest VMP build check requires VBScript 5.5 or higher"
        MESSAGEBOX("We use the Regular Expression VBScript object to perform the check.  " + ;
                   "One of the features we use requires VBScript 5.5 or higher, " + ;
                   "which comes with Microsoft Internet Explorer 5.5 or higher.  " + ;
                   "You can also download VBScript 5.5 separately from the " + ;
                   "Microsoft MSDN Web site at http://msdn.microsoft.com/scripting", ;
                   48,"Please Note")
      ENDIF   &&& PEMSTATUS(loRegExp,"MultiLine",5)
    ENDIF   &&& NOT llError
    IF NOT llError AND VARTYPE(loIE) = "O"
      loIE.Navigate(lcVMPPage)
      IF X5WebPageNavComplete(loIE,30) && Allow time for the specified web page to load up ...
        * VMP build information
        LOCAL loBody, lnHTMLElement, lcHTMLElement
        loBody = loIE.Document.Body
        lcLatestBuild = SPACE(0)
        *  Have to slow things down; otherwise, when I ran it 
        *  subsequent times during the same VFP session, it 
        *  didn't/couldn't find the text 
        *  "Downloads, current YYYY.MM.DD.nn build"
        INKEY(.1)
        *  The following code finds the current build (even if 
        *  it's "moved around" the web page)
        FOR lnHTMLElement = 0 TO loIE.Document.Body.All.Length - 1
          lcHTMLElement = loIE.Document.Body.All(lnHTMLElement).InnerText
          IF LOWER('Downloads, current') $ LOWER(lcHTMLElement)
            lcLatestBuild = ALLTRIM(STREXTRACT(lcHTMLElement, 'current ', 'build'))
            EXIT
          ENDIF
        ENDFOR
        loRegExp.Global = .T.
        loRegExp.IgnoreCase = .T.
        loRegExp.Multiline = .T.
        *  The latest build is supposed to be in the format 
        *  YYYY.MM.DD.NN
        loRegExp.Pattern = '^\d{4}\.\d{2}\.\d{2}\.\d{2}$'
*                       '[0-9]{4}\.[0-9]{2}\.[0-9]{2}\.[0-9]{2}'
*        '[0-9][0-9][0-9][0-9]\.[0-9][0-9]\.[0-9][0-9]\.[0-9][0-9]'
        IF NOT loRegExp.Test(lcLatestBuild)
          lcLatestBuild = lcLatestBuild + ;
                          " (is not in the proper format)"
        ENDIF
      ENDIF   &&& X5WebPageNavComplete(loIE)  
    ENDIF   &&& NOT llError
    loRegExp = .NULL.
    loIE = .NULL. && Release this instance of Internet Explorer
    WAIT CLEAR
  *************************************************
  CASE lnConnectToWebPage = -1
  *************************************************
    lcLatestBuild = ;
         "Unable to load/execute the .DLLs necessary " + ;
         "to attempt to connect to the VMP web site."
ENDCASE

RETURN ALLTRIM(lcLatestBuild)




*******************************************************
*******************************************************
PROCEDURE CompareYourBuildToCurrentBuild
*******************************************************
*******************************************************
LOCAL lcCurrentBuild
lcCurrentBuild = GetVMPBuildFromVMPWebSite()
IF OCCURS(".",m.lcCurrentBuild) < 3
  MESSAGEBOX(m.lcCurrentBuild)
  RETURN 
ENDIF
LOCAL lcYourBuild
lcYourBuild = XXVMPBuild()
IF m.lcYourBuild = m.lcCurrentBuild
  MESSAGEBOX("Your build, " + m.lcYourBuild + ;
             ", is the current build of VMP.", ;
             48, ;
             "You have the current build")
 ELSE
  LOCAL loForm
  loForm = NEWOBJECT("frmNewerVersionOfVMPIsAvailable","","",m.lcCurrentBuild)
  IF VARTYPE(m.loForm) = "O"
    loForm.Show()
   ELSE
    IF MESSAGEBOX( ;
         "Your build of VMP is " + m.lcYourBuild + ;
         ", whereas the current build of VMP is " + ;
         m.lcCurrentBuild + "." + ;
         CHR(13) + CHR(13) + ;
         "Would you like to go to the VMP Downloads web page so that you " + ;
         "can download the latest build of VMP " + LEFTC(m.lcCurrentBuild,4) + "?", ;
         4 + 48, "Download the latest VMP " + LEFTC(m.lcCurrentBuild,4) + " build?") = 6
      X8ShellExecute("http://www.visionpace.com/VMPSite/dld") 
    ENDIF
  ENDIF
ENDIF             
RETURN




*******************************************************
*******************************************************
PROCEDURE InsertAllInheritedCodeAtTopOfMethod
*******************************************************
*******************************************************
*
*  Insert all code inherited by the Object::Method
*  currently being edited in the Form/Class Designer
*  method snippet editor
*
*  this routine is called by the default VMP menu --
*  search for the name of this PROCEDURE in the
*  VMPMenu procedure here in XXPROCDT
*
LOCAL loInfo
loInfo = GetSnippetObjectInfo()
IF NOT VARTYPE(loInfo) = "O"
  *
  *  method snippet editor not open, user has been
  *  messaged by a WAIT WINDOW in GetSnipperObjectInfo()
  *
  RETURN
ENDIF

LOCAL lcKeyboardString
*  KEYBOARD-ing '{Ctrl+Home}' ensures that no text is 
*  currently selected/highlighted in the current method and, 
*  as a result, will not get overwritten.  In addition,
*  it ensures that all the inherited code gets inserted at
*  the top of the method
lcKeyboardString =  ;
      '{Ctrl+A}'    + ;     && Select all text
      '{Pause 0.1}' + ;     && Wait 0.1 seconds to let VFP "catch up" ...
      '{Ctrl+C}'    + ;     && Copy selected text to the Windows clipboard
      '{Ctrl+W}'    + ;     && Exit window for viewing all inherited code
      '{Ctrl+Home}' + ;     && Ensure no text is currently selected/highlighted and we�re at top of method
      '{Ctrl+V}'            && Paste all copied text into the method editor
*
*  stuff the keyboard
*
KEYBOARD lcKeyboardString PLAIN CLEAR
*
*  get the inherited code to a MODI COMM window,
*  the contents of which is then copied, etc.
*  when the KEYBOARD command is executed
*
DO ViewAllInheritedCodeFromDesigner 
RETURN




*******************************************************
*******************************************************
PROCEDURE GetCollectionOfPJXFilesByExtension 
*******************************************************
*******************************************************
*
*  RETURNs a COLLECTION of the names of files (in all
*  upper case) filtered by the passed extension, from 
*  either the passed .PJXFileName or toProject 
*  or _VFP.ActiveProject
*
LPARAMETERS tcExt, tuProject 
LOCAL loProject as Project
LOCAL loFiles as Collection
LOCAL loFile as Object
LOCAL lcFile, lcExt
IF X8SETCLS("XXFW.VCX")
  loFiles = CREATEOBJECT("colBase")
 ELSE
  loFiles = CREATEOBJECT("Collection")
ENDIF
IF NOT VARTYPE(tcExt) = "C" OR EMPTY(tcExt)
  *
  *  loFiles.Count = 0
  *
  RETURN loFiles
 ELSE
  lcExt = UPPER(ALLTRIM(tcExt))
  IF OCCURS(".",lcExt) > 0
    lcExt = UPPER(JUSTEXT(lcExt))
  ENDIF
ENDIF
DO CASE
  ********************************************
  CASE VARTYPE(tuProject) = "O" 
  ********************************************
    IF UPPER(tuProject.BaseClass) = "PROJECT"
      loProject = tuProject
    ENDIF
  ********************************************
  CASE VARTYPE(tuProject) = "C" ;
       AND NOT EMPTY(tuProject) ;
       AND FILE(tuProject) ;
       AND UPPER(JUSTEXT(tuProject)) = "PJX"
  ********************************************
    llError = .f.
    TRY
    MODIFY PROJECT (tuProject) NOWAIT NOSHOW
    CATCH
    llError = .t.
    ENDTRY
    IF NOT llError
      loProject = _VFP.ActiveProject
    ENDIF
  ********************************************
  OTHERWISE       
  ********************************************
    IF TYPE("_VFP.ActiveProject.BaseClass") = "C"
      loProject = _VFP.ActiveProject
    ENDIF
ENDCASE
IF NOT VARTYPE(loProject) = "O" ;
     OR NOT UPPER(loProject.BaseClass) = "PROJECT"
  RETURN .NULL.
ENDIF
FOR EACH loFile IN loProject.Files
  IF UPPER(JUSTEXT(loFile.Name)) == lcExt   &&& loFile.ReadOnly may = .T.
    loFiles.Add(UPPER(loFile.Name))
  ENDIF
ENDFOR
loProject = .NULL.
*
*  loFiles.Count = the number of .lcExt files in
*  the project (may be 0/zero)
*
RETURN loFiles



*******************************************************
*******************************************************
PROCEDURE PicklistOfPJXFilesByExtension
*******************************************************
*******************************************************
*
*  present a picklist of all the files for the
*  specified extension in the specified .PJX
*  or project passed as a project object
*  
*  RETURN the filename of the selected file,
*  or .NULL. if something goes wrong, there
*  are no files of the specified extension,
*  or the user Cancels out of the picklist
*
LPARAMETERS tcExt, tuProject 

LOCAL loProject as Project
LOCAL loFile as Object
LOCAL lcFile, lcExt, lcPJX, llError
IF NOT VARTYPE(tcExt) = "C" OR EMPTY(tcExt)
  *
  *  tcExt not passed or passed improperly
  *
  RETURN .NULL.
 ELSE
  lcExt = UPPER(ALLTRIM(tcExt))
  IF OCCURS(".",lcExt) > 0
    lcExt = UPPER(JUSTEXT(lcExt))
  ENDIF
ENDIF
DO CASE
  ********************************************
  CASE VARTYPE(tuProject) = "O" 
  ********************************************
    IF UPPER(tuProject.BaseClass) = "PROJECT"
      loProject = tuProject
    ENDIF
  ********************************************
  CASE VARTYPE(tuProject) = "C" ;
       AND NOT EMPTY(tuProject) ;
       AND FILE(tuProject) ;
       AND UPPER(JUSTEXT(tuProject)) = "PJX"
  ********************************************
    llError = .f.
    TRY
    MODIFY PROJECT (tuProject) NOWAIT NOSHOW
    CATCH
    llError = .t.
    ENDTRY
    IF NOT llError
      loProject = _VFP.ActiveProject
    ENDIF
  ********************************************
  OTHERWISE       
  ********************************************
    IF TYPE("_VFP.ActiveProject.BaseClass") = "C"
      loProject = _VFP.ActiveProject
    ENDIF
ENDCASE
IF NOT VARTYPE(loProject) = "O" ;
     OR NOT UPPER(loProject.BaseClass) = "PROJECT"
  RETURN .NULL.
ENDIF

CREATE CURSOR C_ProjectFiles (FileName C(254), JustFile C(60))
FOR EACH loFile IN loProject.Files
  IF UPPER(JUSTEXT(loFile.Name)) == lcExt 
    INSERT INTO C_ProjectFiles ;
         (FileName, JustFile) ;
         VALUES ;
         (UPPER(loFile.Name), UPPER(JUSTFNAME(loFile.Name)))  
  ENDIF
ENDFOR
lcPJX = UPPER(JUSTFNAME(loProject.Name))
loProject = .NULL.
IF RECCOUNT("C_ProjectFiles") = 0
  USE IN SELECT("C_ProjectFiles")
  RETURN .NULL.
ENDIF
SELECT C_ProjectFiles
INDEX ON JustFile TAG JustFile

LOCAL loRPF
loRPF = NEWOBJECT("cusRuntimePicklistFormParameter","XXFWPICK.VCX")
WITH loRPF

.icRecordSource = "C_ProjectFiles"
*  .iaColumnInfo[] array is required, containing ControlSources, optionally Column.Widths and iaColumnInfo[] columns 3-6 info
DIMENSION .iaColumnInfo[1,6]
.iaColumnInfo[1,1] = "C_ProjectFiles.JustFile"
.iaColumnInfo[1,3] = "JustFile"
.iaColumnInfo[1,4] = "File Name"
*    Column1 = (REQUIRED) Column.ControlSource
*    Column2 = (OPTIONAL) Column.Width (unless a Width is specified for ALL columns, Width settings are ignored and defaulted
*    Column3 = (OPTIONAL) frmPicklist.grdPicklist1.iaColumnInfo[ColumnX,3] - display index tag associated with this column, for ReSort behavior
*    Column4 = (OPTIONAL) frmPicklist.grdPicklist1.iaColumnInfo[ColumnX,4] - Header.Caption
*    Column5 = (OPTIONAL) frmPicklist.grdPicklist1.iaColumnInfo[ColumnX,5] - specifies "DESCENDING" for the tag in Column3
*    Column6 = (OPTIONAL) frmPicklist.grdPicklist1.iaColumnInfo[ColumnX,6] - specifies index tag expression for Column3 tag, for tags created 'on the fly'
*    Column7 = (OPTIONAL) frmPicklist.grdPicklist1.iaColumnInfo[ColumnX,7] - SEEK index tag associated with this column, for ReSort behavior
*    Column8 = (OPTIONAL) frmPicklist.grdPicklist1.iaColumnInfo[ColumnX,8] - SEEK index tag expression for the tag in Column7
*
*  set the following properties as desired, or delete them
*
.icFormCaption = "Select a " + lcExt + " file from " + lcPJX
.inFormHeight = _Screen.Height-100               &&& Height for the called RPF frm??Picklist
.inFormWidth = 400                &&& Width for the called RPF frm??Picklist
.icReturnValueExpression = "C_ProjectFiles.FileName" 
*!*	.iuInitialValue =              &&& tuInitialValue (1st parameter received by frmPicklist.Init) value
*!*	.icFilter =                    &&& tcFilter (2nd parameter received by frmPicklist.Init) value
.ilNoAddButton = .t.              &&& tlNoAddButton (3rd parameter received by frmPicklist.Init) value
*!*	.icAddForm =                   &&& icAddForm property value for the called RPF frm??Picklist
*!*	.icSetRecordPointerOnAddTag =  &&& Name of the Grid.RecordSource index tag used in THIS.SetRecordPointerOnAdd() to position the record added in the icAddForm.  Required when setting THIS.icAddForm in most n-Tier scenarios.  Copied to the frmPicklist.icSetRecordPointerOnAddTag
*!*	.icFormClass =                 &&& Required when creating an RPF parameter for a txtPicklistValid, specifies the ClassName,VCXFileName of the desired picklist form class, typically frm??Picklist,??FWPICK.VCX
.inFormDataSessionID = SET("DATASESSION")        &&& DataSessionID to which the RPF frm??Picklist form is to be set, frequently _Screen.ActiveForm, if it exists
*!*	.icMTClass =                   &&& ClassName,VCXFileName of a BO/DS to provide the called RPF frm??Picklist.grdPicklist1.RecordSource
*!*	.icRecordSourceTag =           &&& Name of an index tag to be initially set in the called RPF frm??Picklist

*  the .iaSupportingCursors[] array lists tables/views,
*  needed in addition to icRecordSource, typically for
*  non-icRecordSource fields needed for ControlSources
*  DIMENSION .iaSupportingCursors[1]
*  .iaSupportingCursors[1] = 

*  the .iaCommands[] array is completely optional,
*  creating a list of commands to be executed by
*  XXFW.VCX/frmData.Init()
*    XXDTES("XXFW.VCX","iaCommands","frmData","Init")
*  DIMENSION .iaCommands[1]
*  .iaCommands[1] = 

ENDWITH

LOCAL lcFileName
lcFileName = X8WRPFRM("frmPicklist,XXFWPICK.VCX",loRPF)  

USE IN SELECT("C_ProjectFiles")

RETURN lcFileName




*******************************************************
*******************************************************
PROCEDURE DoesMenuBarPromptExistInMNX
*******************************************************
*******************************************************
*
*  RETURNs a logical value indicating whether the
*  passed Bar prompt already exists for the passed
*  Pad prompt of the passed .MNX file
*
*  RETURNS .NULL. if unable to determine
*
*  XXDTES("XXWBNewForm.PRG","DoesMenuBarPromptExistInMNX")
*
*
LPARAMETERS tcMNXFile, tcPadPrompt, tcBarPrompt

LOCAL llRetVal
llRetVal = .NULL.

LOCAL lcMNXFile

IF NOT VARTYPE(tcMNXFile) = "C" OR EMPTY(tcMNXFile)
  RETURN llRetVal
 ELSE
  lcMNXFile = UPPER(ALLTRIM(tcMNXFile))
ENDIF
IF OCCURS(".",lcMNXFile) = 0 
  lcMNXFile = lcMNXFile + ".MNX"
ENDIF
IF NOT FILE(lcMNXFile)
  RETURN llRetVal
ENDIF
lcMNXFile = FULLPATH(lcMNXFile)

LOCAL lcPadPrompt
IF NOT VARTYPE(tcPadPrompt) = "C" OR EMPTY(tcPadPrompt)
  RETURN llRetVal
 ELSE
  lcPadPrompt = ALLTRIM(tcPadPrompt)
ENDIF

LOCAL lcBarPrompt
IF NOT VARTYPE(tcBarPrompt) = "C" OR EMPTY(tcBarPrompt)
  RETURN llRetVal
 ELSE
  lcBarPrompt = ALLTRIM(tcBarPrompt)
ENDIF
IF UPPER(ALLTRIM(lcBarPrompt)) = "\-"
  RETURN .f.
ENDIF

TRY
USE (lcMNXFile) IN 0 ALIAS "C_MNXFile" SHARE NOUPDATE
CATCH
ENDTRY
IF NOT USED("C_MNXFile")
  RETURN llRetVal
ENDIF

SELECT C_MNXFile

*
*  first see if the menu pad exists
*
LOCATE FOR ObjType = 3 ;
     AND ObjCode = 77 ;
     AND NOT EMPTY(Prompt) ;
     AND UPPER(ALLTRIM(STRTRAN(Prompt,"\<",SPACE(0)))) == UPPER(ALLTRIM(STRTRAN(lcPadPrompt,"\<",SPACE(0))))
IF NOT FOUND()
  USE IN SELECT("C_MNXFile")
  RETURN llRetVal
ENDIF
*
*  now see if there's an existing Bar prompt for
*  this pad, matching lcBarPrompt
*  
SKIP 
*
*  this is the record for the actual pad, the
*  record where NumItems > 0 when there is at least
*  one bar already for this menu pad, this record
*  is where we can find the LevelName value
*
*  lnNumItems = the number of existing bars for this menu pad
*
LOCAL lnNumItems, lcLevelName
lnNumItems = NumItems
lcLevelName = ALLTRIM(LevelName)   &&& same as Name
IF lnNumItems = 0
  *
  *  no bars yet for this pad
  *
  llRetVal = .f.
 ELSE
  *
  *  ObjTypes:
  *  67	Command
  *  77	Submenu
  *  78	Bar #
  *  80	Procedure
  *
  LOCATE FOR ObjType = 3 ;
       AND INLIST(ObjCode,67,77,78,80) ;
       AND UPPER(ALLTRIM(LevelName)) == UPPER(lcLevelName) ;
       AND NOT EMPTY(Prompt) AND UPPER(ALLTRIM(STRTRAN(Prompt,"\<",SPACE(0)))) == UPPER(STRTRAN(lcBarPrompt,"\<",SPACE(0)))
  llRetVal = FOUND()
ENDIF
USE IN SELECT("C_MNXFile")

RETURN llRetVal




*******************************************************
*******************************************************
PROCEDURE PicklistOfMenuPadsInMNXFile
*******************************************************
*******************************************************
*
*  present a picklist of the existing menu pads from
*  the specified .MNX file, RETURN the RECNO(.MNXFile)
*  of the selected pad, or .NULL. if unable to present
*  the picklist or the user Cancels out of the picklist
*
*
*  
*
*  .MNX file:
*  ========================================
*  RECNO() 1 = header record
*    ObjCode is 22
*    LevelName is blank
*  RECNO() 2 = menu bar/top level
*    ObjCode = 1
*    Name = (_MSYSMENU)
*    LevelName = Same as Name
*  RECNO() 3 = first pad
*    ObjCode = 67, 77 (usual), 80
*    Name = pad name, if any
*    LevelName = same LevelName (_MSYSMENU) as that of RECNO() 2
*    Prompt = menu pad prompt text
*
*
LPARAMETERS tcMNXFile

LOCAL lcMNXFile

IF NOT VARTYPE(tcMNXFile) = "C" OR EMPTY(tcMNXFile)
  RETURN .NULL.
 ELSE
  lcMNXFile = UPPER(ALLTRIM(tcMNXFile))
ENDIF

IF OCCURS(".",lcMNXFile) = 0 
  lcMNXFile = lcMNXFile + ".MNX"
ENDIF
IF NOT FILE(lcMNXFile)
  RETURN .NULL.
ENDIF
lcMNXFile = FULLPATH(lcMNXFile)

TRY
USE (lcMNXFile) IN 0 ALIAS "C_Temp" SHARED NOUPDATE
CATCH
ENDTRY
IF NOT USED("C_Temp")
  RETURN .NULL.
ENDIF
LOCAL lcLevelName
SELECT C_Temp
LOCATE FOR ObjCode = 1 ;
    AND UPPER(ALLTRIM(Name)) == UPPER(ALLTRIM(LevelName))  &&& should be record #2
lcLevelName = LevelName
SELECT RECNO() as TheRecno, ;
       LEFTC(STRTRAN(Prompt,"\<",SPACE(0)),100) as ThePrompt ;
    FROM DBF("C_Temp") ;
    WHERE LevelName = lcLevelName ;
      AND ObjCode = 77 ;
      AND ObjType = 3 ;
      AND NOT EMPTY(Prompt) ;
      AND NOT Prompt = "\-" ;
    INTO CURSOR PicklistOfMenuPadsInMNXFile NOFILTER READWRITE
USE IN ("C_Temp")

LOCAL loRPF
loRPF = NEWOBJECT("cusRuntimePicklistFormParameter","XXFWPICK.VCX")
WITH loRPF

.icRecordSource = "PicklistOfMenuPadsInMNXFile"             &&& RecordSource for the called RPF frm??Picklist.grdPicklist1
*  .iaColumnInfo[] array is required, containing ControlSources, optionally Column.Widths and iaColumnInfo[] columns 3-6 info
DIMENSION .iaColumnInfo[1,6]
.iaColumnInfo[1,1] = "PicklistOfMenuPadsInMNXFile.ThePrompt"
.iaColumnInfo[1,3] = "DisplayTag"
.iaColumnInfo[1,6] = "TheRecno"
.iaColumnInfo[1,4] = "Pad prompt"
*    Column1 = (REQUIRED) Column.ControlSource
*    Column2 = (OPTIONAL) Column.Width (unless a Width is specified for ALL columns, Width settings are ignored and defaulted
*    Column3 = (OPTIONAL) frmPicklist.grdPicklist1.iaColumnInfo[ColumnX,3] - display index tag associated with this column, for ReSort behavior
*    Column4 = (OPTIONAL) frmPicklist.grdPicklist1.iaColumnInfo[ColumnX,4] - Header.Caption
*    Column5 = (OPTIONAL) frmPicklist.grdPicklist1.iaColumnInfo[ColumnX,5] - specifies "DESCENDING" for the tag in Column3
*    Column6 = (OPTIONAL) frmPicklist.grdPicklist1.iaColumnInfo[ColumnX,6] - specifies index tag expression for Column3 tag, for tags created 'on the fly'
*    Column7 = (OPTIONAL) frmPicklist.grdPicklist1.iaColumnInfo[ColumnX,7] - SEEK index tag associated with this column, for ReSort behavior
*    Column8 = (OPTIONAL) frmPicklist.grdPicklist1.iaColumnInfo[ColumnX,8] - SEEK index tag expression for the tag in Column7
*
*  set the following properties as desired, or delete them
*
.icFormCaption = "Select a pad from " + JUSTFNAME(lcMNXFile)               &&& Caption for the called RPF frm??Picklist
.inFormHeight = _Screen.Height-100               &&& Height for the called RPF frm??Picklist
*!*	.inFormWidth =                 &&& Width for the called RPF frm??Picklist
.icReturnValueExpression = "PicklistOfMenuPadsInMNXFile.TheRecno"    &&& icReturnValueExpression for the called RPF frm??Picklist
*!*	.iuInitialValue =              &&& tuInitialValue (1st parameter received by frmPicklist.Init) value
*!*	.icFilter =                    &&& tcFilter (2nd parameter received by frmPicklist.Init) value
.ilNoAddButton = .t.              &&& tlNoAddButton (3rd parameter received by frmPicklist.Init) value
*!*	.icAddForm =                   &&& icAddForm property value for the called RPF frm??Picklist
*!*	.icSetRecordPointerOnAddTag =  &&& Name of the Grid.RecordSource index tag used in THIS.SetRecordPointerOnAdd() to position the record added in the icAddForm.  Required when setting THIS.icAddForm in most n-Tier scenarios.  Copied to the frmPicklist.icSetRecordPointerOnAddTag
*!*	.icFormClass =                 &&& Required when creating an RPF parameter for a txtPicklistValid, specifies the ClassName,VCXFileName of the desired picklist form class, typically frm??Picklist,??FWPICK.VCX
.inFormDataSessionID = SET("DATASESSION")        &&& DataSessionID to which the RPF frm??Picklist form is to be set, frequently _Screen.ActiveForm, if it exists
*!*	.icMTClass =                   &&& ClassName,VCXFileName of a BO/DS to provide the called RPF frm??Picklist.grdPicklist1.RecordSource
*!*	.icRecordSourceTag =           &&& Name of an index tag to be initially set in the called RPF frm??Picklist

*  the .iaSupportingCursors[] array lists tables/views,
*  needed in addition to icRecordSource, typically for
*  non-icRecordSource fields needed for ControlSources
*  DIMENSION .iaSupportingCursors[1]
*  .iaSupportingCursors[1] = 

*  the .iaCommands[] array is completely optional,
*  creating a list of commands to be executed by
*  XXFW.VCX/frmData.Init()
*    XXDTES("XXFW.VCX","iaCommands","frmData","Init")
*  DIMENSION .iaCommands[1]
*  .iaCommands[1] = 

ENDWITH

LOCAL lnRecno
lnRecno = X8WRPFRM("frmPicklist,XXFWPICK.VCX",loRPF)  

USE IN SELECT("PicklistOfMenuPadsInMNXFile")

RETURN lnRecno




*******************************************************
*******************************************************
PROCEDURE AddMenuBarToMNX
*******************************************************
*******************************************************
*
*  add the specified menu bar Prompt/Command to the
*  specified existing menu pad fo the specified .MNX
*
*  RETURNS .F. for any of the following reasons
*  - parameters not passed/not passed properly
*  - the indicated .MNX file is currently marked ReadOnly
*    (i.e. not checked out of source control)
*  - the indicated .MNX file cannot be found/does not exist
*  - the indicated .MNX file cannot be opened EXCLUSIVE
*
*  
*  XXDTES("XXWBNewForm.PRG","AddMenuBarToMNX")
*  
*
*  ObjType 
*  The type is identified by the following values:
*  1	Menu file default
*  2	Menu bar or popup
*  3	Option
*
*  ObjCode
*  The menu component is identified by the following values:
*  0	Menu pad
*  1	Menu bar
*  22	Default record
*  67	Command
*  77	Submenu
*  78	Bar #
*  80	Procedure
*
*  NumItems
*  Specifies the number of items on a menu
*
*  ItemNum
*  Designates the location of the item on the parent menu
*
*  Location
*  Specifies the location of the menu
*  Corresponds to NEGOTIATE clause of DEFINE PAD. 0=no NEGOTIATE clause, 1=left, 2=middle, 3=right  
*
*
*  NOTE
*  After each separator bar, there is a SYS(2015) record,
*  which is not included in the NumItems, ItemNum = 0,
*  ObjType = 0
*
*
*
*  USE ("C:\PROGRAM FILES\MICROSOFT VISUAL FOXPRO 8\TOOLS\FILESPEC\60MNX.DBF")
*
*
*
*  MODIFY CLASS frmXXWBAppSetup OF XXWB METHOD RemoveMenuBar
*
*
*
*  lParameters
*     tcMNXFile (R) Filename of the .MNX to which the new 
*                     bar is to be added
*     tuMenuPad (R) RECNO(tcMNXFile) for the indicated menu pad
*                   (i.e. the value RETURNed from PicklistOfMenuPadsInMNXFile)
*                     OR
*                   Prompt text for the indicated menu pad
*   tcBarPrompt (R) Prompt text for the new menu bar of tuMenuPad
*  tcBarCommand (R) Command text for the new menu bar of tuMenuPad
*
LPARAMETERS tcMNXFile, ;
            tuMenuPad, ;
            tcBarPrompt, ;
            tcBarCommand

LOCAL lcMNXFile

IF NOT VARTYPE(tcMNXFile) = "C" OR EMPTY(tcMNXFile)
  RETURN .f.
 ELSE
  lcMNXFile = UPPER(ALLTRIM(tcMNXFile))
ENDIF
IF OCCURS(".",lcMNXFile) = 0 
  lcMNXFile = lcMNXFile + ".MNX"
ENDIF
IF NOT FILE(lcMNXFile)
  RETURN .f.
ENDIF 
lcMNXFile = FULLPATH(lcMNXFile)
IF X6ROFILE(lcMNXFile)
  RETURN .f.
ENDIF

LOCAL lcBarPrompt
IF NOT VARTYPE(tcBarPrompt) = "C" OR EMPTY(tcBarPrompt)
  RETURN .f.
 ELSE
  lcBarPrompt = ALLTRIM(tcBarPrompt)
ENDIF
IF UPPER(ALLTRIM(lcBarPrompt)) = "\-"
  RETURN .f.
ENDIF

LOCAL lcBarCommand
IF NOT VARTYPE(tcBarCommand) = "C" OR EMPTY(tcBarCommand)
  RETURN .f.
 ELSE
  lcBarCommand = ALLTRIM(tcBarCommand)
ENDIF

DO CASE
  CASE VARTYPE(tuMenuPad) = "N"
    IF NOT tuMenuPad > 2
      RETURN .f.
    ENDIF
  CASE VARTYPE(tuMenuPad) = "C"
    IF EMPTY(tuMenuPad)
      RETURN .f.
    ENDIF
  OTHERWISE
    RETURN .f.
ENDCASE

TRY
USE (lcMNXFile) IN 0 ALIAS "C_MNXFile" EXCLUSIVE 
CATCH
ENDTRY
IF NOT USED("C_MNXFile")
  RETURN .f.
ENDIF

LOCAL llError

SELECT C_MNXFile
TRY
IF VARTYPE(tuMenuPad) = "N"
  GOTO (tuMenuPad)
 ELSE
  LOCATE FOR ObjType = 3 ;
       AND ObjCode = 77 ;
       AND NOT EMPTY(Prompt) ;
       AND UPPER(ALLTRIM(STRTRAN(Prompt,"\<",SPACE(0)))) == UPPER(ALLTRIM(STRTRAN(tuMenuPad,"\<",SPACE(0))))
ENDIF
llError = .f.
CATCH
llError = .t.
ENDTRY
IF llError OR EOF("C_MNXFile")
  USE IN SELECT("C_MNXFile")
  RETURN .f.
ENDIF

*
*  record pointer is now on the indicated menu pad,
*  where the new bar will be inserted at the
*  end of the existing bars for that pad
*    
LOCAL lcPadName, lnPadRecno, lcLevelName, lnNumItems, ;
      lcMenuName, lnPadRecno2, lnLastPadRecno
lcPadName = UPPER(ALLTRIM(Name))
lnPadRecno = RECNO()
SKIP 
*
*  this is the record for the actual pad, the
*  record where NumItems > 0 when there is at least
*  one bar already for this menu pad, this record
*  is where we can find the LevelName value
*
*  lnNumItems = the number of existing bars for this menu pad
*
lnNumItems = NumItems
lnPadRecno2 = RECNO()
lcLevelName = ALLTRIM(LevelName)   &&& same as Name
*
*  go to record #2, store the menu name -- the highest
*  level menu item, typically _MSYSMENU
*
2
lcMenuName = UPPER(ALLTRIM(Name))   &&& same as LevelName
*
*  back to the record where the pad starts
*
GOTO (lnPadRecno)
SKIP 
*
*  we're going to add a record at the end of the existing
*  records for this menu pad, either
*  1- the next record, when this is the last menu pad
*     and it contains no bars yet
*  2- the next record, when this menu pad contains no
*     bars yet
*  3- at the end of the existing bars for this pad, 
*     indicated as the last record before 
*     LevelName = lcMenuName (typically _MSYSMENU)
*  we need to:
*  a) update NumItems to NumItems + 1
*  b) insert the new bar record at the correct
*     consecutive physical location
*
lnLastPadRecno = RECNO() 
DO CASE
  CASE RECCOUNT() = lnPadRecno2
    *
    *  scenario #1
    *
  CASE lnNumItems = 0
    *
    *  scenario #2
    *
  OTHERWISE
    SCAN REST 
      IF UPPER(ALLTRIM(LevelName)) == lcMenuName
        *
        *  getting here signals that we've arrived 
        *  on the record for the next menu pad
        *
        lnLastPadRecno = RECNO()-1
        EXIT 
      ENDIF
    ENDSCAN  
ENDCASE
GOTO (lnPadRecno2)
replace NumItems WITH NumItems + 1 IN C_MNXFile
*
*  ...so, first we'll copy all the records up to the
*  place where we'll insert the new record, to a
*  cursor that we'll copy to tcMNXFile
*   
LOCAL laFields[1]
AFIELDS(laFields)
CREATE CURSOR C_NewMNX FROM ARRAY laFields 
SELECT C_MNXFile
SCAN
  SCATTER MEMVAR MEMO 
  INSERT INTO C_NewMNX FROM MEMVAR 
  IF RECNO("C_MNXFile") = lnLastPadRecno
    EXIT
  ENDIF
ENDSCAN

*
*  add the new Menu Bar record here
*
INSERT INTO C_NewMNX ;
    (ObjType, ;
     ObjCode, ;     
     Prompt, ;
     Command, ;
     ProcType, ;
     SetupType, ;
     CleanType, ;
     Mark, ;
     NameChange, ;
     NumItems, ;
     LevelName, ;
     ItemNum, ;
     Location, ;
     Scheme, ;
     SysRes) ;
    VALUES ;
    (3, ; 
     67, ;
     lcBarPrompt, ;
     lcBarCommand, ;
     0, ;
     0, ;
     0, ;
     CHR(0), ;
     .f., ;
     0, ;
     lcLevelName, ;
     TRANSFORM(lnNumItems + 1), ;
     0, ;
     0, ; 
     0)

*
*  and add any remaining records
*
SELECT C_MNXFile
SCAN FOR RECNO() > lnLastPadRecno
  SCATTER MEMVAR MEMO 
  INSERT INTO C_NewMNX FROM MEMVAR 
ENDSCAN

*
*  C_MNXFile now contains the full contents of the
*  MNXFile, including the old records plus the
*  new bar record inserted at the correct location
*

*
*  make a backup of tcMNXFile
*
USE IN SELECT("C_MNXFile")
LOCAL lcPath, lcSetSafety
lcSetSafety = SET("SAFETY")
SET SAFETY OFF 
lcPath = ADDBS(JUSTPATH(lcMNXFile))
COPY FILE (lcMNXFile) TO (lcPath + JUSTSTEM(lcMNXFile) + ".XNM")
COPY FILE (lcPath + JUSTSTEM(lcMNXFile) + ".MNT") TO (lcPath + JUSTSTEM(lcMNXFile) + ".TNM")
*
*  copy C_NewMNX to tcMNXFile
*
TRY
USE (lcMNXFile) IN 0 ALIAS "C_MNXFile" EXCLUSIVE 
CATCH
ENDTRY
IF NOT USED("C_MNXFile")
  USE IN SELECT("C_NewMNX")
  RETURN .f.
ENDIF
SELECT C_MNXFile
ZAP 
APPEND FROM DBF("C_NewMNX")
USE IN SELECT("C_NewMNX")
USE IN SELECT("C_MNXFile")

SET SAFETY &lcSetSafety

RETURN .t.




*******************************************************
*******************************************************
PROCEDURE UpdateCommandOfBarInMNX
*******************************************************
*******************************************************
*
*  XXDTES("XXWBNewForm.PRG","UpdateCommandOfBarInMNX")
*
LPARAMETERS tcMNXFile, ;
            tcPadPrompt, ;
            tcBarPrompt, ;
            tcBarCommand

LOCAL lcMNXFile

IF NOT VARTYPE(tcMNXFile) = "C" OR EMPTY(tcMNXFile)
  RETURN .f.
 ELSE
  lcMNXFile = UPPER(ALLTRIM(tcMNXFile))
ENDIF
IF OCCURS(".",lcMNXFile) = 0 
  lcMNXFile = lcMNXFile + ".MNX"
ENDIF
IF NOT FILE(lcMNXFile)
  RETURN .f.
ENDIF
lcMNXFile = FULLPATH(lcMNXFile)
IF X6ROFILE(lcMNXFile)
  RETURN .f.
ENDIF

LOCAL lcPadPrompt
IF NOT VARTYPE(tcPadPrompt) = "C" OR EMPTY(tcPadPrompt)
  RETURN llRetVal
 ELSE
  lcPadPrompt = ALLTRIM(tcPadPrompt)
ENDIF

LOCAL lcBarPrompt
IF NOT VARTYPE(tcBarPrompt) = "C" OR EMPTY(tcBarPrompt)
  RETURN .f.
 ELSE
  lcBarPrompt = ALLTRIM(tcBarPrompt)
ENDIF
IF UPPER(ALLTRIM(lcBarPrompt)) = "\-"
  RETURN .f.
ENDIF

LOCAL lcBarCommand
IF NOT VARTYPE(tcBarCommand) = "C" OR EMPTY(tcBarCommand)
  RETURN .f.
 ELSE
  lcBarCommand = ALLTRIM(tcBarCommand)
ENDIF

TRY
USE (lcMNXFile) IN 0 ALIAS "C_MNXFile" EXCLUSIVE 
CATCH
ENDTRY
IF NOT USED("C_MNXFile")
  RETURN .f.
ENDIF

SELECT C_MNXFile

*
*  first see if the menu pad exists
*
LOCATE FOR ObjType = 3 ;
     AND ObjCode = 77 ;
     AND NOT EMPTY(Prompt) ;
     AND UPPER(ALLTRIM(STRTRAN(Prompt,"\<",SPACE(0)))) == UPPER(ALLTRIM(STRTRAN(lcPadPrompt,"\<",SPACE(0))))
IF NOT FOUND()
  USE IN SELECT("C_MNXFile")
  RETURN .f.
ENDIF
*
*  now see if there's an existing Bar prompt for
*  this pad, matching lcBarPrompt
*  
SKIP 
*
*  this is the record for the actual pad, the
*  record where NumItems > 0 when there is at least
*  one bar already for this menu pad, this record
*  is where we can find the LevelName value
*
IF NumItems = 0
  *
  *  no bars for this pad
  *
  USE IN SELECT("C_MNXFile")
  RETURN .f.
ENDIF
LOCAL lcLevelName
lcLevelName = ALLTRIM(LevelName)   &&& same as Name

LOCATE FOR ALLTRIM(UPPER(STRTRAN(Prompt,"\<",SPACE(0)))) == ;
           UPPER(STRTRAN(lcBarPrompt,"\<",SPACE(0))) ;
     AND ObjType = 3 ;
     AND ObjCode = 67 ;   &&& 67 denotes a bar set to a Command rather than one of the other types like Procedure
     AND UPPER(ALLTRIM(LevelName)) == UPPER(ALLTRIM(lcLevelName))

LOCAL llSuccess

IF FOUND()
  replace Command WITH lcBarCommand IN C_MNXFile
  llSuccess = .t.
 ELSE
  *
  *  we get here if the above LOCATE failed:
  *  - no bar exists for lcBarPrompt for lcPadPrompt
  *  - no bar exists for lcBarPrompt for lcPadPrompt
  *    where the bar is currently set to Command 
  *
  llSuccess = .f.
ENDIF

USE IN SELECT("C_MNXFile")

RETURN 



*******************************************************
*******************************************************
PROCEDURE Get_MSYSMENUPadNumberFromPadName
*******************************************************
*******************************************************
*
*  RETURNs the number of the specified _MSYSMENU pad,
*  .NULL. if _MSYSMENU is not currently the VFP menu or
*  if the passed tcPadName (NOT a prompt, but a name)
*  cannot be found on _MSYSMENU
*
*  ? Get_MSYSMENUPadNumberFromPadName("_MSM_file")
*  ? Get_MSYSMENUPadNumberFromPadName("_MSM_Edit")
*  ? Get_MSYSMENUPadNumberFromPadName("_MSM_VIEW")
*
LPARAMETERS tcPadName
*
*  first, make sure _MSYSMENU exists
*
LOCAL llError
llError = .f.
TRY
CNTPAD("_MSYSMENU")
CATCH
llError = .t.
ENDTRY
IF llError
  *
  *  no _MYSMENU up at the moment
  *
  RETURN .NULL.
ENDIF
LOCAL lnPadNumber
lnPadNumber = .NULL.
FOR xx = 1 TO CNTPAD("_MSYSMENU")
  IF GETPAD("_MSYSMENU",xx) == UPPER(ALLTRIM(tcPadName))
    lnPadNumber = xx
    EXIT 
  ENDIF
ENDFOR
RETURN lnPadNumber



*******************************************************
*******************************************************
PROCEDURE GetBaseClass
*******************************************************
*******************************************************
*
*  RETURNs the BaseClass of the passed ClassName, ClassLib
*
*  ClassLib can be a .VCX or .PRG, when ClassLib is a .VCX,
*  this procedure is a wrapper to 
*  XXTOOLS.VCX/cusPEMTool::GetBaseClass()
*    MODIFY CLASS cusPEMTool OF XXTOOLS METHOD GetBaseClass
*
*  if anything bad happens, RETURNs .NULL.
*
*  XXDTES("XXWB.VCX","GetBaseClass(","cmdGetClass","Click")
* 
LPARAMETERS tcClassName, ;
            tcClassLib

LOCAL lcBaseClass
lcBaseClass = .NULL.

IF UPPER(JUSTEXT(m.tcClassLib)) = "VCX"
  SET CLASSLIB TO XXTOOLS.VCX ADDITIVE 
  LOCAL loPEMTool AS cusPEMTool OF XXTOOLS.VCX
  loPEMTool = CREATEOBJECT("cusPEMTool")
  IF m.loPEMTool.OpenMetaFile(m.tcClassLib)
    IF m.loPEMTool.LocateObject(m.tcClassName)
      lcBaseClass = m.loPEMTool.GetBaseClass()
    ENDIF
    m.loPEMTool.CloseMetaFile()
  ENDIF
  loPEMTool = .NULL.
  RETURN m.lcBaseClass
ENDIF

*
*  UPPER(JUSTEXT(m.tcClassLib)) = "PRG"
*

LOCAL laProcInfo[1], lnRow, laLines[1], lcLine, lnLine, ;
      lcParentClassName, lcParentClassLib, llError, ;
      laBaseClasses[1]
ALANGUAGE(laBaseClasses,3)
lcParentClassName = m.tcClassName
lcParentClassLib = m.tcClassLib
DO WHILE .t.
  llError = .f.
  TRY      
  APROCINFO(laProcInfo,m.lcParentClassLib,1)
  CATCH
  llError = .t.
  ENDTRY
  IF m.llError
    *
    *  can't find lcParentClassName in lcParentClassLib
    *
    lcBaseClass = .NULL.
    EXIT 
  ENDIF
  *
  *  Column1 � class name 
  *  Column2 � line
  *  Column3 � parentclass
  *  Column4 - is OLEpublic
  *
  lnRow = ASCAN(laProcInfo,m.lcParentClassName,1,-1,1,15)
  IF m.lnRow = 0
    *
    *  can't find tcClassName in tcClassLib
    *
    lcBaseClass = .NULL.
    EXIT 
  ENDIF
  *
  *  if the ParentClass is a VFP base class, it holds
  *  the base class, and we're done
  *
  ALINES(laLines,FILETOSTR(m.lcParentClassLib),.t.)
  *  get the DEFINE CLASS line
  lnLine = ASCAN(laLines,"DEFINE CLASS "+m.lcParentClassName+" AS ",1,-1,1,13)
  lcLine = UPPER(ALLTRIM(laLines[m.lnLine]))
  DO CASE
    *******************************************
    CASE " OF " $ m.lcLine
    *******************************************
      *
      *  having the OF clause indicates the class is NOT
      *  of the VFP base class, but rather a subclass of 
      *  a class in the OF class library, and we'll have
      *  to keep on traversing the class hierarchy
      *
      lcParentClassName = GETWORDNUM(m.lcLine,5,SPACE(1))
      lcParentClassLib = GETWORDNUM(m.lcLine,7,SPACE(1))
    *******************************************
    CASE ASCAN(laBaseClasses,GETWORDNUM(m.lcLine,5,SPACE(1)),1,-1,1,15)>0
    *******************************************
      *
      *  the " AS " is a VFP base class
      *
      lcBaseClass = GETWORDNUM(m.lcLine,5,SPACE(1))
      EXIT 
    *******************************************
    OTHERWISE
    *******************************************
      *
      *  the " AS " is not a VFP base class, but there is
      *  no " OF " clause, so the parent class is in 
      *  the same lcParentClassLib
      *
      lcParentClassName = GETWORDNUM(m.lcLine,5,SPACE(1))
  ENDCASE
ENDDO  

RETURN IIF(ISNULL(m.lcBaseClass),.NULL.,UPPER(m.lcBaseClass))


*******************************************************
*******************************************************
PROCEDURE InheritsFrom
*******************************************************
*******************************************************
*
*  RETURNs a logical value indicating whether the passed
*  ClassName, ClassLib inherits from the specified
*  ClassName, ClassLib
* 
*  ClassLib can be a .VCX or .PRG, when ClassLib is a .VCX,
*  this procedure is a wrapper to 
*  XXTOOLS.VCX/cusPEMTool::InheritsFrom()
*    MODIFY CLASS cusPEMTool OF XXTOOLS METHOD InheritsFrom
*
*  if anything bad happens, RETURNs .NULL.
*
*  XXDTES("XXWB.VCX","InheritsFrom(","cmdGetClass","Click")
*  
LPARAMETERS tcClassName, ;
            tcClassLib, ;
            tcInheritsFromClassName, ;
            tcInheritsFromClassLib 

IF UPPER(ALLTRIM(m.tcClassName)) == UPPER(ALLTRIM(m.tcInheritsFromClassName)) 
  IF UPPER(ALLTRIM(m.tcClassLib)) == UPPER(ALLTRIM(m.tcInheritsFromClassLib))
    RETURN .t.
  ENDIF
  IF FILE(m.tcClassLib) AND FILE(m.tcInheritsFromClassLib) ;
       AND ;
       FULLPATH(m.tcClassLib) = FULLPATH(m.tcInheritsFromClassLib)
    RETURN .t.
  ENDIF       
ENDIF      

LOCAL llRetVal
llRetVal = .NULL.

IF UPPER(JUSTEXT(m.tcClassLib)) = "VCX"
  SET CLASSLIB TO XXTOOLS.VCX ADDITIVE 
  LOCAL loPEMTool AS cusPEMTool OF XXTOOLS.VCX
  loPEMTool = CREATEOBJECT("cusPEMTool")
  IF m.loPEMTool.OpenMetaFile(m.tcClassLib)
    IF m.loPEMTool.LocateObject(m.tcClassName)
      IF m.loPEMTool.InheritsFrom(m.tcInheritsFromClassName+","+m.tcInheritsFromClassLib)
        llRetVal = .t.
       ELSE
        llRetVal = .f.
      ENDIF
    ENDIF
    m.loPEMTool.CloseMetaFile()
  ENDIF
  loPEMTool = .NULL.
  RETURN m.llRetVal
ENDIF

*
*  UPPER(JUSTEXT(m.tcClassLib)) = "PRG"
*

LOCAL laProcInfo[1], lnRow, laLines[1], lcLine, lnLine, ;
      lcParentClassName, lcParentClassLib, llError, ;
      laBaseClasses[1]
ALANGUAGE(laBaseClasses,3)
lcParentClassName = m.tcClassName
lcParentClassLib = m.tcClassLib
DO WHILE .t.
  llError = .f.
  TRY      
  APROCINFO(laProcInfo,m.lcParentClassLib,1)
  CATCH
  llError = .t.
  ENDTRY
  IF m.llError
    *
    *  can't find/open m.lcParentClassLib
    *
    llRetVal = .NULL.
    EXIT 
  ENDIF
  *
  *  Column1 � class name 
  *  Column2 � line
  *  Column3 � parentclass
  *  Column4 - is OLEpublic
  *
  lnRow = ASCAN(laProcInfo,m.lcParentClassName,1,-1,1,15)
  IF m.lnRow = 0
    *
    *  can't find lcParentClassName in lcParentClassLib
    *
    llRetVal = .NULL.
    EXIT 
  ENDIF
  *
  *  get the DEFINE CLASS line
  *
  ALINES(laLines,FILETOSTR(m.lcParentClassLib),.t.)
  lnLine = ASCAN(laLines,"DEFINE CLASS "+m.lcParentClassName+" AS ",1,-1,1,13)
  lcLine = UPPER(ALLTRIM(laLines[m.lnLine]))
  DO CASE
    *******************************************
    CASE " OF " $ m.lcLine
    *******************************************
      *
      *  DEFINE CLASS line includes AS and OF clauses
      *
      lcParentClassName = GETWORDNUM(m.lcLine,5,SPACE(1))
      lcParentClassLib = GETWORDNUM(m.lcLine,7,SPACE(1))
    *******************************************
    CASE ASCAN(laBaseClasses,GETWORDNUM(m.lcLine,5,SPACE(1)),1,-1,1,15)>0
    *******************************************
      *
      *  DEFINE CLASS line does not include the OF clause,
      *  but the parent class is a VFP base class
      *
      m.llRetVal = .f.
      EXIT 
    *******************************************
    OTHERWISE
    *******************************************
      *
      *  DEFINE CLASS line does not include the OF clause,
      *  but the parent class is not a VFP base class,
      *  indicating that the parent class is in the same
      *  .PRG as the lcParentClassLib we're already looking
      *  in 
      *
      lcParentClassName = GETWORDNUM(m.lcLine,5,SPACE(1))
  ENDCASE
  IF UPPER(ALLTRIM(m.lcParentClassName)) == UPPER(ALLTRIM(m.tcInheritsFromClassName)) ;
       AND ;
       UPPER(ALLTRIM(m.lcParentClassLib)) == UPPER(ALLTRIM(m.tcInheritsFromClassLib))
    m.llRetVal = .t.
    EXIT 
  ENDIF
ENDDO  
RETURN m.llRetVal
 
 
 
*******************************************************
*******************************************************
PROCEDURE ClassExists
*******************************************************
*******************************************************
*
*  RETURNs a logical (or .NULL.) value indicating whether 
*  the passed ClassName exists in the passed ClassLib  
* 
*  ClassLib can be a .VCX or .PRG
*
*  If the passed ClassLib cannot be found, RETURNs .f.
* 
*  If the passed ClassLib cannot be opened/examined,
*  RETURNs .NULL.
*
*  XXDTES("XXWBCOMWRAPPER.PRG","ClassExists(")
*
LPARAMETERS tcClassName, ;
            tcClassLib 
IF NOT FILE(m.tcClassLib)  
  RETURN .f.
ENDIF

LOCAL llExists, laClasses[1], lnClasses, llError, laProcInfo[1]

IF UPPER(JUSTEXT(m.tcClassLib)) = "VCX"
  llError = .f.
  TRY
  lnClasses = AVCXCLASSES(laClasses,m.tcClassLib)  
  CATCH
  llError = .t.
  ENDTRY
  IF m.llError
    RETURN .NULL.
  ENDIF
  IF m.lnClasses = 0
    RETURN .f.
  ENDIF
  llExists = ASCAN(laClasses,m.tcClassName,1,-1,1,15)>0
  RETURN m.llExists
ENDIF

*
*  UPPER(JUSTEXT(m.tcClassLib)) = "PRG"
*
llError = .f.
TRY
lnClasses = APROCINFO(laProcInfo,m.tcClassLib,1)
CATCH
llError = .t.
ENDTRY
*
*  Column1 � class name 
*  Column2 � line
*  Column3 � parentclass
*  Column4 - is OLEpublic
*
IF m.llError
  RETURN .NULL.
ENDIF
IF m.lnClasses = 0
  RETURN .f.
ENDIF
llExists = ASCAN(laProcInfo,m.tcClassName,1,-1,1,15)>0
RETURN m.llExists
 
 

*******************************************************
*******************************************************
PROCEDURE RemoveClass
*******************************************************
*******************************************************
*
*  RETURNs a logical value indicating whether the passed 
*  ClassName has been removed from the passed ClassLib  
* 
*  ClassLib can be a .VCX or .PRG
*
*  If the passed ClassLib cannot be found/opened/examined, 
*  RETURNs .f.
* 
*  If the passed ClassName does not exist in the passed
*  ClassLib, RETURNs .t.
*
*  XXDTES("XXWBCOMWRAPPER.PRG","RemoveClass(")
*
LPARAMETERS tcClassName, ;
            tcClassLib 

IF NOT FILE(m.tcClassLib)
  RETURN .f.
ENDIF

LOCAL llClassExists
llClassExists = ClassExists(m.tcClassName,m.tcClassLib)
IF ISNULL(m.llClassExists) OR NOT m.llClassExists
  RETURN .f.
ENDIF

LOCAL llSuccess
llSuccess = .t.
IF UPPER(JUSTEXT(m.tcClassLib)) = "VCX"
  TRY
  REMOVE CLASS (m.tcClassName) OF (m.tcClassLib)
  CATCH
  llSuccess = .f.
  ENDTRY
 ELSE   &&& .PRG
  *
  *  VFP has no REMOVE CLASS for a .PRG-based class, so
  *  we'll have to do it the hard way
  *
  LOCAL laLines[1], lcLine, laProcInfo[1], lnLine, xx, ;
        lcBackup, llInClass
  TRY
  APROCINFO(laProcInfo,m.tcClassLib,1)
  CATCH
  llSuccess = .f.
  ENDTRY
  IF NOT m.llSuccess
    RETURN .f.
  ENDIF
  *
  *  Column1 � class name 
  *  Column2 � line
  *  Column3 � parentclass
  *  Column4 - is OLEpublic
  *
  lnLine = ASCAN(laProcInfo,m.tcClassName,1,-1,1,15)
  IF m.lnLine = 0
    *
    *  this shouldn't happen
    *
    llSuccess = .f.
    RETURN .f.
  ENDIF
  *
  *  make a backup of tcClassLib
  *
  lcBackup = FULLPATH(m.tcClassLib)
  lcBackup = FORCEEXT(m.tcClassLib,"BAK")
  STRTOFILE(FILETOSTR(m.tcClassLib),m.lcBackup,0)
  TRY
  *
  *  lnLine is the line in tcClassLib where the 
  *  tcClassName class definition starts
  *
  lnLine = laProcInfo[m.lnLine,2]
  ALINES(laLines,FILETOSTR(m.tcClassLib),.t.)
  ERASE (m.tcClassLib)
  FOR xx = 1 TO ALEN(laLines,1)
    lcLine = laLines[xx]
    DO CASE
      **********************************************
      CASE m.xx < m.lnLine
      **********************************************
        *
        *  copy lines before the tcClassName class definition
        *  back to tcClassLib
        *
        STRTOFILE(m.lcLine + CHR(13) + CHR(10),m.tcClassLib,1)
      **********************************************
      CASE m.xx = m.lnLine
      **********************************************
        *
        *  this is the line where the tcClassName class
        *  definition starts
        *        
        llInClass = .t.
      **********************************************
      CASE m.llInClass = .t. ;
           AND (UPPER(ALLTRIM(m.lcLine)) == "ENDDEFINE" ;
                OR ;
                UPPER(ALLTRIM(m.lcLine)) = "ENDDEFINE ")
      **********************************************
        *
        *  this is the line where the tcClassName class
        *  definition ends
        *        
        llInClass = .f.
      **********************************************
      CASE m.xx > m.lnLine AND NOT m.llInClass 
      **********************************************
        *
        *  copy lines after the tcClassName class definition
        *  back to tcClassLib
        *
        STRTOFILE(m.lcLine + CHR(13) + CHR(10),m.tcClassLib,1)
    ENDCASE      
  ENDFOR
  CATCH
  llSuccess = .f.
  ENDTRY
  IF NOT m.llSuccess
    *
    *  restore m.tcClassLib, and leave the backup in place
    *
    STRTOFILE(FILETOSTR(m.lcBackup),m.tcClassLib,0)
  ENDIF
ENDIF

RETURN m.llSuccess
 
 
 
 
*******************************************************
*******************************************************
PROCEDURE GetCollectionOfMethodsFromClass
*******************************************************
*******************************************************
*
*  RETURNs a collection of the methods in a .PRG-based
*  ClassName -- the methods that are defined in that
*  ClassName by these lines -- note that these are not
*  necessarily those methods defined for the first time
*  at the class level, but those explicitly named in
*  the class definition, including native VFP methods
*  like Init, which are overridden/augmented:
*  PROCEDURE/FUNCTION... 
*  PROTECTED PROCEDURE/FUNCTION...
*  HIDDEN PROCEDURE/FUNCTION...  
*
*  the collection is keyed, but to really just to fake a
*  2-column collection -- the key value is a string
*  indicating Public/Protected/Hidden, plus an integer:
*    XXDTES("XXWB.VCX","loMethods = GetCollectionOfMethodsFromClass","frmxxwbcomwrapper","pgfSettings.Page2.ctrMethodsMover.SETEXPRPROPS")
*
*  If the passed ClassLib cannot be found/opened/examined, 
*  RETURNs .f.
* 
*  If the passed ClassName does not exist in the passed
*  ClassLib, RETURNs .f.
*
*  Usage examples:
*  --------------------------
*  SET PROCEDURE TO XXPROCDT.PRG ADDITIVE
*  LOCAL loCollection
*  loCollection = GetCollectionOfMethodsFromClass("sesCOMWrapper","XXFWNT.PRG")
*  ? loCollection.Item(1)
*  ? loCollection.Item(4)
*  ? loCollection.Item(loCollection.Count)
*
LPARAMETERS tcClassName, ;
            tcClassLib, ;
            tlNoPublicMethods, ;
            tlNoProtectedMethods, ;
            tlNoHiddenMethods

IF NOT FILE(m.tcClassLib)
  RETURN .f.
ENDIF

LOCAL llClassExists
llClassExists = ClassExists(m.tcClassName,m.tcClassLib)
IF ISNULL(m.llClassExists) OR NOT m.llClassExists
  RETURN .f.
ENDIF

LOCAL laLines[1], lcLine, laProcInfo[1], lnLine, xx, ;
      llInClass, llSuccess
llSuccess = .t.
TRY
APROCINFO(laProcInfo,m.tcClassLib,1)
CATCH
llSuccess = .f.
ENDTRY
IF NOT m.llSuccess
  RETURN .f.
ENDIF
*
*  Column1 � class name 
*  Column2 � line
*  Column3 � parentclass
*  Column4 - is OLEpublic
*
lnLine = ASCAN(laProcInfo,m.tcClassName,1,-1,1,15)
IF m.lnLine = 0
  *
  *  this shouldn't happen
  *
  RETURN .f.
ENDIF
*
*  lnLine is the line in tcClassLib where the 
*  tcClassName class definition starts
*
LOCAL loCollection as Collection
loCollection = CREATEOBJECT("Collection")
LOCAL lcMethodName, lnPCount, lnRCount, lnHCount
lnLine = laProcInfo[m.lnLine,2]
STORE 0 TO lnPCount, lnRCount, lnHCount
ALINES(laLines,FILETOSTR(m.tcClassLib),.t.)
FOR xx = m.lnLine TO ALEN(laLines,1)
  lcLine = ALLTRIM(m.laLines[xx])
  DO CASE
    ************************************************
    CASE UPPER(m.lcLine) == "ENDDEFINE" ;
         OR UPPER(m.lcLine) = "ENDDEFINE "
    ************************************************
      EXIT
    ************************************************
    CASE NOT tlNoPublicMethods ;
         AND ;
         (UPPER(m.lcLine) = "PROCEDURE " OR UPPER(m.lcLine) = "FUNCTION ")
    ************************************************
      lcMethodName = ALLTRIM(SUBSTRC(m.lcLine,ATC(SPACE(1),m.lcLine)))
      IF OCCURS("(",m.lcMethodName)>0
        lcMethodName = LEFTC(m.lcMethodName,ATC("(",m.lcMethodName)-1)
      ENDIF
      lnPCount = m.lnPCount + 1
      m.loCollection.Add(m.lcMethodName,"Public"+TRANSFORM(m.lnPCount))
    ************************************************
    CASE NOT tlNoProtectedMethods ;
         AND ;
         (UPPER(m.lcLine) = "PROTECTED PROCEDURE " OR UPPER(m.lcLine) = "PROTECTED FUNCTION ")
    ************************************************
      lcMethodName = ALLTRIM(SUBSTRC(m.lcLine,ATC(SPACE(1),m.lcLine,2)))
      IF OCCURS("(",m.lcMethodName)>0
        lcMethodName = LEFTC(m.lcMethodName,ATC("(",m.lcMethodName)-1)
      ENDIF
      lnRCount = m.lnRCount + 1
      m.loCollection.Add(m.lcMethodName,"Protected"+TRANSFORM(m.lnRCount))
    ************************************************
    CASE NOT tlNoHiddenMethods ;
         AND ;
         (UPPER(m.lcLine) = "HIDDEN PROCEDURE " OR UPPER(m.lcLine) = "HIDDEN FUNCTION ")
    ************************************************
      lcMethodName = ALLTRIM(SUBSTRC(m.lcLine,ATC(SPACE(1),m.lcLine,2)))
      IF OCCURS("(",m.lcMethodName)>0
        lcMethodName = LEFTC(m.lcMethodName,ATC("(",m.lcMethodName)-1)
      ENDIF
      lnHCount = m.lnHCount + 1
      m.loCollection.Add(m.lcMethodName,"Hidden"+TRANSFORM(m.lnHCount))
  ENDCASE  
ENDFOR
RETURN m.loCollection
  
  
  
  
*******************************************************
*******************************************************
PROCEDURE SourceRelations
*******************************************************
*******************************************************
*
*  this PROCEDURE isn't being used anywhere at the 
*  moment, but might be used in some form at some point
*
LPARAMETERS tcAlias
FOR xx = 1 TO 100
  IF EMPTY(TARGET(m.xx,tcAlias))
    EXIT
  ENDIF
  ? tcAlias + " is the source of a relation into", TARGET(m.xx,tcAlias), "on", RELATION(m.xx,tcAlias)
ENDFOR
RETURN

 
 
*******************************************************
*******************************************************
PROCEDURE TargetRelations
*******************************************************
*******************************************************
*
*  this PROCEDURE isn't being used anywhere at the 
*  moment, but might be used in some form at some point
*
LPARAMETERS tcAlias
LOCAL laUsed[1]
IF AUSED(laUsed) > 0
  FOR yy = 1 TO ALEN(laUsed,1)
    FOR xx = 1 TO 100
      IF EMPTY(TARGET(m.xx,laUsed[m.yy,1]))
        EXIT
      ENDIF
      ? tcAlias + " is the target of a relation from", laUsed[m.yy,1], "on", RELATION(m.xx,laUsed[m.yy,1])
    ENDFOR   
  ENDFOR
ENDIF
RETURN


  
*******************************************************
*******************************************************
PROCEDURE IsValidPropertyName 
*******************************************************
*******************************************************
*
*  RETURNs a logical value indicating whether the
*  passed string is a valid name for a property
*  
LPARAMETERS tcPropertyName
LOCAL loTestObject, llError
loTestObject = CREATEOBJECT("Empty")
TRY
ADDPROPERTY(loTestObject,m.tcPropertyName,100)
CATCH
llError = .t.
ENDTRY
loTestObject = .NULL.
RETURN NOT m.llError
  
  
  
  
  
**************************************************
*-- Form:         frmnewerversionofvmpisavailable (c:\vmpyyyy\xlib\anewerversionofvmpisavailable.scx)
*-- ParentClass:  form
*-- BaseClass:    form
*-- Time Stamp:   05/20/05 09:41:11 AM
*
DEFINE CLASS frmnewerversionofvmpisavailable AS form


	Top = 0
	Left = 0
	Height = 368
	Width = 702
	DoCreate = .T.
	BorderStyle = 1
	Caption = "A newer version of VMP is available!"
	MaxButton = .F.
	MinButton = .F.
	ShowTips = .t.
	WindowType = 1
	Name = "frmNewerVersionOfVMPIsAvailable"


	ADD OBJECT shpmain AS shape WITH ;
		Top = 16, ;
		Left = 16, ;
		Height = 109, ;
		Width = 669, ;
		BackColor = RGB(128,128,128), ;
		Name = "shpMain"


	ADD OBJECT shpdontbotherme AS shape WITH ;
		Top = 208, ;
		Left = 16, ;
		Height = 144, ;
		Width = 544, ;
		SpecialEffect = 0, ;
		Name = "shpDontBotherMe"


	ADD OBJECT imgvmp AS image WITH ;
		Picture = ("VMPTiny.GIF"), ;
		Height = 40, ;
		Left = 32, ;
		MousePointer = 15, ;
		Top = 48, ;
		Width = 125, ;
		Name = "imgVMP"


	ADD OBJECT cmdok AS commandbutton WITH ;
		Top = 324, ;
		Left = 596, ;
		Height = 27, ;
		Width = 88, ;
		Caption = "\<OK", ;
		Default = .T., ;
		TabIndex = 1, ;
		Name = "cmdOK"


	ADD OBJECT edtinfo AS editbox WITH ;
		BackStyle = 0, ;
		BorderStyle = 0, ;
		Enabled = .F., ;
		Height = 48, ;
		Left = 172, ;
		ScrollBars = 0, ;
		TabIndex = 4, ;
		Top = 68, ;
		Width = 500, ;
		DisabledForeColor = RGB(255,255,255), ;
		Name = "edtInfo"


	ADD OBJECT lblclickheredownloads AS label WITH ;
		FontUnderline = .T., ;
		BackStyle = 0, ;
		Caption = "Click here", ;
		Height = 17, ;
		Left = 44, ;
		MousePointer = 15, ;
		Top = 144, ;
		Width = 56, ;
		TabIndex = 5, ;
		ForeColor = RGB(0,0,255), ;
		Name = "lblClickHereDownloads"


	ADD OBJECT lbldownloads AS label WITH ;
		BackStyle = 0, ;
		Caption = "for the VMP downloads page (this VFP session will be QUIT, so you can install properly)", ;
		Height = 17, ;
		Left = 104, ;
		Top = 144, ;
		Width = 500, ;
		TabIndex = 10, ;
		Name = "lblDownloads"


	ADD OBJECT lblbugfixes AS label WITH ;
		WordWrap = .T., ;
		BackStyle = 0, ;
		Caption = "for the VMP bug fixes page, where you can review the list of fixes for the current build", ;
		Height = 20, ;
		Left = 104, ;
		Top = 172, ;
		Width = 520, ;
		TabIndex = 12, ;
		Name = "lblBugFixes"


	ADD OBJECT lblclickherebugfixes AS label WITH ;
		FontUnderline = .T., ;
		BackStyle = 0, ;
		Caption = "Click here", ;
		Height = 17, ;
		Left = 44, ;
		MousePointer = 15, ;
		Top = 172, ;
		Width = 56, ;
		TabIndex = 6, ;
		ForeColor = RGB(0,0,255), ;
		Name = "lblClickHereBugFixes"


*!*		ADD OBJECT chkdontbotherme AS checkbox WITH ;
*!*			Top = 212, ;
*!*			Left = 24, ;
*!*			Height = 17, ;
*!*			Width = 16, ;
*!*			Alignment = 0, ;
*!*			Caption = " ", ;
*!*			Value = .F., ;
*!*			TabIndex = 2, ;
*!*			Name = "chkDontBotherMe"


*!*		ADD OBJECT lbldontbotherme1 AS label WITH ;
*!*			WordWrap = .T., ;
*!*			BackStyle = 0, ;
*!*			Caption = "Don't bother me with this notification when I start the first VMP session of the day and I don't have the latest build.  I'll check the VMP web site when I want to see if there's a newer build.", ;
*!*			Height = 44, ;
*!*			Left = 44, ;
*!*			Top = 212, ;
*!*			Width = 504, ;
*!*			TabIndex = 13, ;
*!*			Name = "lblDontBotherMe1"


	ADD OBJECT chkdontbotherme AS checkbox WITH ;
		Top = 212, ;
		Left = 24, ;
		Height = 17, ;
		Width = 520, ;
		Alignment = 0, ;
		Caption = "Don't bother me with this notification when I start the first VMP session of the day and I don't", ;
		Value = .F., ;
		TabIndex = 2, ;
		Name = "chkDontBotherMe"

	ADD OBJECT lbldontbotherme1 AS label WITH ;
		BackStyle = 0, ;
		Caption = "have the latest build.  I'll check the VMP web site when I want to see if there's a newer build.", ;
		Height = 44, ;
		Left = 40, ;
		Top = 227, ;
		Width = 504, ;
		TabIndex = 13, ;
		Name = "lblDontBotherMe1"

	ADD OBJECT lbldontbotherme2 AS label WITH ;
		WordWrap = .T., ;
		BackStyle = 0, ;
		Caption = "I understand that if I select this checkbox, a semaphore file named NoDailyAutoLatestBuildCheck.TXT is created in the \VMPyyyy\XLIB folder.  When that file exists, the daily check/notification in A.PRG is suppressed.", ;
		Height = 48, ;
		Left = 44, ;
		Top = 248, ;
		Width = 504, ;
		TabIndex = 14, ;
		Name = "lblDontBotherMe2"


	ADD OBJECT lbldontbotherme3 AS label WITH ;
		WordWrap = .T., ;
		BackStyle = 0, ;
		Caption = "I further understand that if I want this behavior back, I can simply erase the \VMPyyyy\XLIB\NoDailyAutoLatestBuildCheck.TXT semaphore file from my \VMPyyyy\XLIB folder.", ;
		Height = 48, ;
		Left = 44, ;
		Top = 300, ;
		Width = 500, ;
		TabIndex = 15, ;
		Name = "lblDontBotherMe3"


*!*		ADD OBJECT cmdshowmethebuild AS commandbutton WITH ;
*!*			Top = 208, ;
*!*			Left = 576, ;
*!*			Height = 60, ;
*!*			Width = 108, ;
*!*			WordWrap = .T., ;
*!*			Caption = "Show me the VMP build # in my XXFWMAIN.PRG", ;
*!*			TabIndex = 3, ;
*!*			Name = "cmdShowMeTheBuild"


	ADD OBJECT lblcurrentvmp AS label WITH ;
		BackStyle = 0, ;
		Caption = "The current build of VMP is", ;
		Height = 17, ;
		Left = 172, ;
		Top = 24, ;
		Width = 148, ;
		TabIndex = 7, ;
		ForeColor = RGB(255,255,255), ;
		Name = "lblCurrentVMP"


	ADD OBJECT lblcurrentvmpbuild AS label WITH ;
		FontBold = .T., ;
		FontSize = 11, ;
		BackStyle = 0, ;
		Caption = "9999.99.99.99", ;
		Height = 17, ;
		Left = 320, ;
		Top = 22, ;
		Width = 96, ;
		TabIndex = 9, ;
		ForeColor = RGB(255,255,255), ;
		Name = "lblCurrentVMPBuild"

	ADD OBJECT lblyourvmp AS label WITH ;
		BackStyle = 0, ;
		Caption = "...whereas you are developing with VMP", ;
		Height = 17, ;
		Left = 172, ;
		Top = 44, ;
		Width = 216, ;
		TabIndex = 11, ;
		ForeColor = RGB(255,255,255), ;
		Name = "lblYourVMP"


	ADD OBJECT lblyourvmpbuild AS label WITH ;
		FontBold = .T., ;
		FontSize = 11, ;
		BackStyle = 0, ;
		Caption = "9999.99.99.99", ;
		Height = 17, ;
		Left = 390, ;
		Top = 42, ;
		Width = 96, ;
		TabIndex = 8, ;
		ForeColor = RGB(255,255,0), ;
		Name = "lblYourVMPBuild"

	ADD OBJECT cmdshowmethebuild AS commandbutton WITH ;
		Top = 40, ;
		Left = 488, ;
		Height = 21, ;
		Width = 64, ;
		Caption = "Show me", ;
		FontSize = 8, ;
		TabIndex = 3, ;
		TabStop = .f., ;
		ToolTipText = "Show me the VMP build number in my XXFWMAIN.PRG", ;
		Name = "cmdShowMeTheBuild"



	PROCEDURE notthecurrentyearmessage
LPARAMETERS tcLatestVMPBuild, tcYourVMPBuild

LOCAL lcText

TEXT TO lcText NOSHOW TEXTMERGE
We recommend that you download and install the current
version of VMP.  You can click on the second of the links
below to go to the VMP downloads web page; all you need to
download right now is your UserID and Password indicating 
a current VMP subscription.
ENDTEXT
lcText = X7RemoveCRLFTAB(m.lcText,.t.)

THIS.edtInfo.Value = m.lcText
	ENDPROC



	PROCEDURE notthecurrentbuildmessage
LPARAMETERS tcLatestVMPBuild, tcYourVMPBuild

LOCAL lcText

TEXT TO lcText NOSHOW TEXTMERGE
We recommend that you download and install the current
version of VMP.  You can click on the second of the links
below to go to the VMP downloads web page; all you need to
download right now is your UserID and Password indicating 
a current VMP subscription.
ENDTEXT
lcText = X7RemoveCRLFTAB(m.lcText,.t.)

THIS.edtInfo.Value = m.lcText
	ENDPROC



	PROCEDURE notthecurrentrefreshmessage
LPARAMETERS tcLatestVMPBuild, tcYourVMPBuild

LOCAL lcText

TEXT TO lcText NOSHOW TEXTMERGE
You can click on the links below to check what bug fixes have been 
made in the refreshes that have been posted since the
<<m.tcYourVMPBuild>> version, or to download the latest.
ENDTEXT
lcText = X7RemoveCRLFTAB(m.lcText,.t.)

THIS.edtInfo.Value = m.lcText
	ENDPROC


	PROCEDURE OKAction
		IF THIS.chkDontBotherMe.Value
		  LOCAL lcFileName, loException
		  lcFileName = ADDBS(JUSTPATH(FULLPATH("XXFWMAIN.PRG"))) + "NoDailyAutoLatestBuildCheck.TXT"
		  TRY
		  STRTOFILE("Suppress the default check for the latest VMP build.", ;
		            m.lcFileName, ;
		            .f.)
		  CATCH TO loException
		  ENDTRY
		  IF VARTYPE(m.loException) = "O"  
		    MESSAGEBOX("Unable to write the semaphore file " + ;
		               CHR(13) + CHR(10) + ;
		               m.lcFileName + ;
		               CHR(13) + CHR(10) + ;
		               "because: " + ;
		               CHR(13) + CHR(10) + CHR(13) + CHR(10), ;
		               48,"Please Note")
		  ENDIF
		ENDIF

		THIS.Release()
	ENDPROC


	PROCEDURE Init
		LPARAMETERS tcLatestVMPBuild

		IF PCOUNT() = 0 ;
		     OR NOT VARTYPE(m.tcLatestVMPBuild) = "C" ;
		     OR EMPTY(m.tcLatestVMPBuild) ;
		     OR NOT OCCURS(".",m.tcLatestVMPBuild) = 3
		  ASSERT .f. MESSAGE PROGRAM() + ;
		       " has not been passed the tcLatestVMPBuild parameter"
		  RETURN .f.
		ENDIF            

		THIS.AutoCenter = .t.
		THIS.Top = (_Screen.Height-THIS.Height)/5

		IF FILE("VMP.ICO")
		  THIS.Icon = "VMP.ICO"
		ENDIF

		LOCAL lcYourVMPBuild 
		lcYourVMPBuild = XXVMPBuild()

		THIS.lblYourVMPBuild.Caption = m.lcYourVMPBuild
		THIS.lblCurrentVMPBuild.Caption = m.tcLatestVMPBuild

		DO CASE
		  **************************************************************
		  CASE m.lcYourVMPBuild == m.tcLatestVMPBuild
		  **************************************************************
		    *
		    *  ??? 
		    * 
		    *  this form should not have been called
		    *
		    RETURN .f.
		  **************************************************************
		  CASE NOT GETWORDNUM(m.lcYourVMPBuild,1,".") = GETWORDNUM(m.tcLatestVMPBuild,1,".")
		  **************************************************************
		    *
		    *  your build is not the current year
		    *
		    THIS.NotTheCurrentYearMessage(m.tcLatestVMPBuild, ;
		                                  m.lcYourVMPBuild)
		  **************************************************************
		  CASE GETWORDNUM(m.lcYourVMPBuild,1,".") = GETWORDNUM(m.tcLatestVMPBuild,1,".") ;
		       AND ;
		       NOT SUBSTRC(m.lcYourVMPBuild,6,5) = SUBSTRC(m.tcLatestVMPBuild,6,5)
		  **************************************************************
		    *
		    *  your build is the current year but not the current month/day
		    *
		    THIS.NotTheCurrentBuildMessage(m.tcLatestVMPBuild, ;
		                                   m.lcYourVMPBuild)
		  **************************************************************
		  CASE GETWORDNUM(m.lcYourVMPBuild,1,".") = GETWORDNUM(m.tcLatestVMPBuild,1,".") ;
		       AND ;
		       SUBSTRC(m.lcYourVMPBuild,6,5) = SUBSTRC(m.tcLatestVMPBuild,6,5) ;
		       AND ;
		       NOT GETWORDNUM(m.lcYourVMPBuild,4,".") = GETWORDNUM(m.tcLatestVMPBuild,4,".")
		  **************************************************************
		    *
		    *  your build is the current year and month, but
		    *  not the current refresh
		    *
		    THIS.NotTheCurrentRefreshMessage(m.tcLatestVMPBuild, ;
		                                     m.lcYourVMPBuild)
		  **************************************************************
		  OTHERWISE
		  **************************************************************
		    *
		    *  ???
		    *
		    RETURN .f.
		ENDCASE
	ENDPROC


	PROCEDURE Show
		LPARAMETERS nStyle

		DODEFAULT(m.nStyle)

		DECLARE Long MessageBeep IN USER32.DLL Long uType
		MessageBeep(0)
	ENDPROC


	PROCEDURE shpmain.Init
		THIS.BorderColor = THIS.BackColor
	ENDPROC


	PROCEDURE imgvmp.Click
		X8ShellExecute("http://www.visionpace.com/VMPSite")
	ENDPROC


	PROCEDURE cmdok.Click
		THISFORM.OKAction()
	ENDPROC


	PROCEDURE lblclickheredownloads.Click
		X8ShellExecute("http://www.visionpace.com/VMPSite/dld")
        plKillVFPSession = .t.   &&& XXDTES("A.PRG","PRIVATE plKillVFPSession")
        THISFORM.Release()
	ENDPROC


	PROCEDURE lblclickherebugfixes.Click
		LOCAL lcURL
		lcURL = "http://www.visionpace.com/VMPSite/VMP" + ;
		        LEFTC(THISFORM.lblCurrentVMPBuild.Caption,4) + ;
		        "/bugfixes.asp"

		X8ShellExecute(m.lcURL)
	ENDPROC


	PROCEDURE cmdshowmethebuild.Click
		LOCAL lnStart, lnStop, laLines[1], lcLine, lnSpot
		lnStart = 0
		ALINES(laLines,FILETOSTR("XXFWMAIN.PRG"))
		FOR EACH lcLine IN laLines
		  IF NOT "DEFINE CCBUILD" $ UPPER(m.lcLine)
		    lnStart = m.lnStart + LENC(m.lcLine) 
		    lnStart = m.lnStart + 2   &&& for the CRLFs on each line
		    LOOP 
		  ENDIF
		  lnSpot = ATC(".",m.lcLine)-4
		  lnStart = m.lnStart + m.lnSpot
		  lnStop = m.lnStart + 13
		  EXIT 
		ENDFOR

		MODIFY COMMAND XXFWMAIN.PRG NOEDIT RANGE m.lnStart,m.lnStop
	ENDPROC


ENDDEFINE
*
*-- EndDefine: frmnewerversionofvmpisavailable
**************************************************
