*
*  X6UDofDT.PRG
*  "U"pdate the "D"ate portion of a "D"ate"T"ime value
*
*  Copyright (c) 2005-2005 Visionpace   All Rights Reserved
*                17501 East 40 Hwy., Suite 218
*                Independence, MO 64055
*                816-350-7900 
*                http://www.visionpace.com
*                http://vmpdiscussion.visionpace.com
*  Author:  Drew Speedie 
*
*
*  USAGE:
*    xx = X6UDofDT(SomeDateTimeValue,SomeDateValue)
*
*  EXAMPLES:
*    xx = DATETIME() + 1000000
*    ? xx
*    xx = X6UDofDT(xx,DATE())
*    ? xx
*    
*  XXDTES("XXFW.VCX","X6UDofDT","txtbase","DBLCLICK")
*  XXDTES("XXFW.VCX","X6UDofDT(ltValue,DATE())","txtbase","CALENDARKEYPRESS")
*  XXDTES("XXFW.VCX","X6UDofDT(ltValue,ldValue)","txtbase","CALENDARKEYPRESS")
*  XXDTES("XXFWCONTROLBEHAVIORS.VCX","X6UDofDT(","cuscalendartextbox","OWNERKEYPRESS")
*
*
*  lParameters
*    ttDateTime (R) The DateTime value whose Date portion
*                     is to be updated
*     tdNewDate (O) The Date value to update the Date 
*                     portion of ttDateTime
*                   If not passed, no action is taken here,
*                     and ttDateTime is RETURNed
*
LPARAMETERS ttDateTime, tdNewDate

IF EMPTY(m.tdNewDate)
  RETURN m.ttDateTime
ENDIF

LOCAL ltRetVal, lnYear, lnMonth, lnDay, lnHours, lnMin, lnSec
lnYear = YEAR(m.tdNewDate)
lnMonth = MONTH(m.tdNewDate)
lnDay = DAY(m.tdNewDate)
lnHours = HOUR(m.ttDateTime)
lnMin = MINUTE(m.ttDateTime)
lnSec = SEC(m.ttDateTime)
ltRetval = DATETIME(m.lnYear,m.lnMonth,m.lnDay,m.lnHours,m.lnMin,m.lnSec)

RETURN m.ltRetVal
