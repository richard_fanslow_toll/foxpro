*
*  X3STDBY.PRG
*  Wrapper program for frmStandBy to present a
*  "Please wait..." form message.
*
*  This routine is included for backward compatibility
*  and is not called anywhere in the current VMP
*  framework -- use XXSTDBY.PRG instead.
*
*  Copyright (c) 1997-2005 Visionpace   All Rights Reserved
*                17501 East 40 Hwy., Suite 218
*                Independence, MO 64055
*                816-350-7900 
*                http://www.visionpace.com
*                http://vmpdiscussion.visionpace.com
*  Author:  Drew Speedie
*
*  Usage:
*    local loPleaseWait
*    *  apps using Steven Black's INTL Toolkit:
*    loPleaseWait = x3stdby(x3i("This is my message to you!"))
*    *  apps not using Steven Black's INTL Toolkit:
*    loPleaseWait = x3stdby("This is my message to you!")
*    *
*    * processing goes here
*    *
*    release loPleaseWait
*     -OR-
*    loPleaseWait.Release()  &&& or, if this is the last
*                            &&& line of code, there's 
*                            &&& nothing needed because
*                            &&& loPleaseWait is destroyed as
*                            &&& it goes out of scope
*
*  EXAMPLES
*  XXFW.VCX/ctrApp.ValidateDatabase()
*  XXFWFRM.VCX/frmEventLog.Load()
*    XXDTES("XXFWFRM.VCX","X3STDBY","frmEventLog","Load")
*  XXFWFRM.VCX/frmEventLog.cmdClipboard.Click()  
*  XXFWLIBS.VCX/cusDBCSvc.PackTable()
*  VMDECUSA.SCX/pgfPageRefresh1.Page1.cmdApplyFilter.Click()
*  VMDECUSH.SCX/pgfPageRefresh1.Page1.cmdApplyFilter.Click()
*
*
*  lParameters
*      tcMessage (O) Message to be presented in the form
*                    Defaults to "Please wait..."
*     tcIconFile (O) Filename of an icon file to be displayed
*                      at the left side of the standby form
*                    In XXFWFRM.VCX/frmStandby, defaults to
*                      any specified oApp.GetAppInfo("IconFile")
*  tcClassAndVCX (O) [ClassName,VCX] containing the
*                      standby form
*                    Defaults to "frmStandby,XXFWFRM.VCX"
*
LPARAMETERS tcMessage, tcIconFile, tcClassAndVCX

LOCAL lcClassAndVCX, lcClassName, lcIconFile
IF TYPE("m.tcClassAndVCX") = "C" ;
     AND NOT ISNULL(m.tcClassAndVCX) ;
     AND NOT EMPTY(m.tcClassAndVCX)
  lcClassAndVCX = UPPER(ALLTRIM(m.tcClassAndVCX))
 ELSE
  lcClassAndVCX = "frmStandby,XXFWFRM.VCX"
ENDIF
lcClassName = X3SETCPR(m.lcClassAndVCX)
IF ISNULL(m.lcClassName)
  * unable to SET CLASSLIB TO/SET PROCEDURE TO
  RETURN .f.
ENDIF
IF TYPE("m.tcIconFile") = "C" ;
     AND NOT ISNULL(m.tcIconFile) ;
     AND NOT EMPTY(m.tcIconFile)
  lcIconFile = UPPER(ALLTRIM(m.tcIconFile))
 ELSE
  lcIconFile = SPACE(0)
ENDIF     

LOCAL loStandBy, luNothing
loStandby = CREATEOBJECT(m.lcClassName,m.tcMessage,m.lcIconFile)
IF VARTYPE(m.loStandby) = "O"
  loStandby.Show()
ENDIF

IF FILE("X7RedrawWindow.PRG") OR FILE("X7RedrawWindow.FXP")
  *
  *  frequently, VFP doesn't redraw the contents of
  *  this form immediately on Show-ing it, especially
  *  when your next action after calling this routine
  *  is to perform some processor-intensive action --
  *  this code forces VFP to repaint/redraw the
  *  loStandBy form
  *
  LOCAL lcString
  lcString = "X7RedrawWindow(loStandby)"
  &lcString
ENDIF

RETURN loStandby


