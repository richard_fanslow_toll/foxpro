*
*  XXDTXREL.PRG
*  DeveloperTool:  Has been renamed to XXDTXRelPath

LOCAL lcNewProgram
lcNewProgram = "XXDTXRelPath"
MESSAGEBOX(PROGRAM() + " has been replaced with " + lcNewProgram + ".PRG -- " + ;
           "please DO " + lcNewProgram + " instead", 48, "Please Note")
ACTIVATE WINDOW Command 
KEYBOARD "DO " + lcNewProgram
