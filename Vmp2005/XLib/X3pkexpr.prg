*
*  X3PKEXPR.PRG
*  Returns the index expression for the Primary Key
*  of the passed alias, as long as the passed
*  alias:
*    1- is a VFP contained table
*    2- has a specified PK tag
*    3- is open
*  For all other conditions, returns SPACE(0)
*
*  Copyright (c) 1997-2005 Visionpace   All Rights Reserved
*                17501 East 40 Hwy., Suite 218
*                Independence, MO 64055
*                816-350-7900 
*                http://www.visionpace.com
*                http://vmpdiscussion.visionpace.com
*  Author:  Drew Speedie
*
*  Usage:
*    lcPKExpr = x3pkexpr("CUSTOMERS")
*    lcPKValue = eval(x3pkexpr("CUSTOMERS"))
*
*  lParameters
*    tcAlias (R) Alias of the table whose PK 
*                expression is to be returned.
*                tcAlias must be open
*                  
lparameters tcAlias
local lcAlias
IF type("tcAlias") = "C" AND !empty(tcAlias) 
  lcAlias = upper(alltrim(tcAlias))
 ELSE
  return space(0)
ENDIF
IF !used(lcAlias)
  return space(0)
ENDIF
IF empty(cursorgetprop("DATABASE",lcAlias)) ;
     OR x3srctyp(lcAlias) # "TC"
  *
  *  free table or Fox2x table or view or SPT cursor
  *  or CREATE CURSOR cursor or SQL-Select (non-filtered)
  *  cursor
  *
  return space(0)
ENDIF
*
*  NOTE: we could replace the loop below with less
*        code including a call to 
*        DBGETPROP(...,"TABLE","PRIMARYKEY") but 
*        that would require SET DATABASE TO, etc.,
*        likely making this .PRG slower than the
*        following
*
local lnCount, lcPKExpression
lcPKExpression = space(0)
FOR lnCount = 1 to tagcount("",lcAlias) 
  IF primary(lnCount,lcAlias) 
    lcPKExpression = key(lnCount,lcAlias)
    exit
  ENDIF
ENDFOR &&& all tags in the structural .CDX
return upper(lcPKExpression)
