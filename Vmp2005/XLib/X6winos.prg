*
*  X6WINOS.PRG
*  RETURNS
*     "WIN2003"
*     "WINXP"
*     "WIN2000"
*     "WINNT"
*     "WIN98"
*     "WIN95" 
*     "WIN31"
*     "UNKNOWN"
*  indicating the current Operating System
*  If passed an operating system string, returns a 
*    logical value indicating whether or not that is
*    the current OS.
*
*  Copyright (c) 1997-2005 Visionpace   All Rights Reserved
*                17501 East 40 Hwy., Suite 218
*                Independence, MO 64055
*                816-350-7900 
*                http://www.visionpace.com
*                http://vmpdiscussion.visionpace.com
*  Author:  Based on code provided by Rick Strahl, as 
*           printed as a tip in FoxPro Advisor Magazine, 
*           modified by Drew Speedie, with special
*           thanks to Dave Aring
*
*
*  Usage:
*  IF X6WINOS() = "WINNT"
*    * code here
*  ENDIF
*  IF X6WINOS("WINNT")
*    * code here
*  ENDIF
*  _WIN2003 = (X6WINOS() = "WIN2003")
*  _WINXP = (X6WINOS() = "WINXP")
*  _WIN2K = (X6WINOS() = "WIN2000")
*  _WINNT = (X6WINOS() = "WINNT")
*  _WIN98 = (X6WINOS() = "WIN98")
*  _WIN95 = (X6WINOS() = "WIN95")
*  _WIN31 = (X6WINOS() = "WIN31")
*
LPARAMETERS tcPlatform

LOCAL lcPlatform, lcOS, luRetVal
lcPlatForm = SPACE(0)
lcOS = OS(1)

DO CASE
  CASE "5.02" $ lcOS
     lcPlatform = "WIN2003"
  CASE "5.01" $ lcOS
    lcPlatform = "WINXP"
  CASE "5.0" $ lcOS
    lcPlatForm = "WIN2000"
  CASE "NT" $ lcOS
    lcPlatform = "WINNT"
  CASE "4.9" $ lcOS
     lcPlatform = "WINME"
  CASE "4.1" $ lcOS						&&& e.g., Windows 4.10
    lcPlatform = "WIN98"
  CASE "4.0" $ lcOS OR "3.9" $ lcOS 
    * Win95 including beta versions
    lcPlatform = "WIN95"
  CASE "3." $ lcOS
    * Remaining Win 3.x versions 
    lcPlatform = "WIN31"
  OTHERWISE
    lcPlatform = "UNKNOWN"
ENDCASE

IF VARTYPE(tcPlatform) = "C" AND NOT EMPTY(tcPlatform)
  luRetVal = (UPPER(ALLTRIM(tcPlatform)) == lcPlatform)
 ELSE
  luRetVal = lcPlatForm
ENDIF

RETURN luRetVal
