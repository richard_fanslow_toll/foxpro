*
*  X3RLB.PRG
*  Forces a Refresh of the Local Buffers for the passed ALIAS()
*  (local table), ensuring that a subsequent 
*  SQL-SELECT/USE <ViewName>/etc. retrieves the most up-to-date
*  data to this workstation.
*
*  RETURNs a logical value indicating whether the
*  refresh has been successful
*
*  Copyright (c) 1997-2005 Visionpace   All Rights Reserved
*                17501 East 40 Hwy., Suite 218
*                Independence, MO 64055
*                816-350-7900 
*                http://www.visionpace.com
*                http://vmpdiscussion.visionpace.com
*  Author:  Drew Speedie
*
*  Note that if you open a view or do a SQL-SELECT on
*  a local VFP table from the VMP "report object"
*  XXFWMISC.VCX/frmReport private data session, VFP does
*  not reliably get the latest data from disk -- records
*  added at other workstations on the network don't
*  necessarily show up on the current workstation unless 
*  you manually update the local workstation buffers with 
*  code like this before you do the SQL-SELECT or open the
*  view.
*  See VMREPRTS.VCX/frmVMCusD.PrepareData().
*    EDITSOURCE("VMREPRTS.VCX",1,"frmVMCusD","PrepareData")
*
*  lParameters
*    tcAlias (O) Alias of an open (table) cursor whose
*                  buffers you want to make sure are
*                  current.
*                Defaults the current ALIAS()
*
lparameters tcAlias

local lcAlias, llRetVal
IF type("tcAlias") = "C" AND !isnull(tcAlias) AND !empty(tcAlias)
  lcAlias = upper(alltrim(tcAlias))
 ELSE
  lcAlias = alias()
ENDIF
IF empty(lcAlias)
  return .f.
ENDIF
IF !used(lcAlias)
  return .f.
ENDIF

local lnSelect, llEOF
lnSelect = select(0)

select (lcAlias)
IF eof()
  llEOF = .t.
  locate
ENDIF

*
*  if the current recno(tcAlias) is already
*  locked, do nothing -- this should rarely
*  be the case
*
IF !isrlocked()
  *
  *  now do a quick lock/unlock, forcing
  *  the local buffer to be refreshed
  *
  IF rlock()
    llRetVal = .t.
    unlock
   ELSE
    llRetVal = .f. 
  ENDIF
ENDIF

IF llEOF 
  go bottom
  IF !eof()
    skip 1
  ENDIF
ENDIF

select (lnSelect)

return llRetVal