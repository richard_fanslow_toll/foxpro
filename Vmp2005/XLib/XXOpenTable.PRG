*
*  XXOpenTable.PRG 
*  This routine is a wrapper to XXFWLIBS.VCX/cusDBCSvc::OpenTable(),
*  and calls either an existing oLib.oDBCSvc.OpenTable()
*  or an instance of cusDBCSvc instantiated here.
*    MODIFY CLASS cusDBCSvc OF XXFWLIBS METHOD OpenTable
*  As such, this routine can be used to open either a
*  VFP table OR a local/remote view.
*
*  Copyright (c) 2003-2005 Visionpace   All Rights Reserved
*                17501 East 40 Hwy., Suite 218
*                Independence, MO 64055
*                816-350-7900 
*                http://www.visionpace.com
*                http://vmpdiscussion.visionpace.com
*  Author:  Drew Speedie
*
*  
*  EXAMPLES
*  ==================================
*  XXDTES("XXFWNTDS.VCX",'IF VARTYPE(oLib) = "O"',"cusdsbehaviorvfptable","OPEN")
*  XXDTES("XXFWNTDS.VCX",'IF VARTYPE(oLib) = "O"',"cusdsbehaviorview","OPEN")
*
*
*  this routine does one thing that cusDBCSvc::OpenTable()
*  doesn't -- open any database passed in the tcTable
*  or tcDBC parameter
*
*  this routine is good for use in standalone code,
*  developer tools, or perhaps in middle-tier objects 
*  (although not usually necessary/desirable, as the 
*  middle-tier objects should be opening the table/
*  view) or to open one or two tables/views in a private 
*  data session and you don't feel like calling 
*  oLib.oDBCSvc.OpenTable() because you have to pass
*  the tnDataSession data session parameter when you
*  call this routine -- the indicated table/view is
*  opened in the data session of the calling code
*
*  NOTE that buffering in the cusDBCSvc object
*  DOES NOT respect any oLib.oDBCSvc.inBufferMode_Table
*  or inBufferMode_View -- any table opened here is
*  opened with NO buffering, and any view opened here
*  is opened with buffering set to 3 (Optimistic Row),
*  so you will have to set any desired buffering after
*  you call this routine.
*
*  any table/view opened here is opened in the data
*  session of the code that calls this routine
*
*  WHEN oLib.oDBCSvc DOES NOT EXIST WHEN THIS ROUTINE IS CALLED
*  ...if the requested table/view is not already open,
*  calling this routine is almost as fast as calling
*  oLib.oDBCSvc.OpenTable() directly -- the most
*  time-consuming part of the whole task is by far
*  the execution of the USE command 
*  ...if the requested table/view is already open,
*  this routine is about 3-4 times slower than calling
*  oLib.oDBCSvc.OpenTable() directly -- there is nothing
*  to do with respect to opening the table/view, so
*  most of the time is taken instantiating the cusDBCSvc
*  instance
*
*  just as cusDBCSvc::OpenTable() does, when this wrapper
*  is successful and RETURNs .T., it leaves the passed
*  tcTable/tcAlias as the current work area 
*
*  this is a wrapper -- all the parameters match those 
*  received by XXFWLIBS.VCX/cusDBCSvc::OpenTable()
*    XXDTES("XXFWLIBS.VCX","LPARAMETERS tcTable","cusDBCSvc","OpenTable")
*  EXCEPT that the tnDataSession parameter is NOT 
*  accepted here, but rather the current SET("DATASESSION")
*  is passed on to cusDBCSvc::OpenTable()/oLib.oDBCSvc.OpenTable() 
*  -- this routine opens the indicated table in the
*  data session of calling code
*
LPARAMETERS tcTable, ;
            tcOrder, ;
            tlExclusive, ;
            tcAlias, ;
            tcDBC, ;
            tlNoupdate, ;
            tlGetData

IF NOT "\XXFWLIBS.VCX" $ SET("CLASSLIB")
  SET CLASSLIB TO XXFWLIBS.VCX ADDITIVE 
ENDIF

LOCAL llRetVal, lnBufferMode, lnVBufferMode, lnTBufferMode, ;
      lcDBC, lcTable, ;
      loDBCSvc AS cusDBCSvc OF XXFWLIBS.VCX 

lcTable = UPPER(ALLTRIM(m.tcTable))
IF VARTYPE(m.tcDBC) = "C" AND NOT EMPTY(m.tcDBC)
  lcDBC = UPPER(ALLTRIM(m.tcDBC))
ENDIF
IF OCCURS("!",m.tcTable) = 1
  *
  *  overwrite any passed tcDBC 
  *
  lcDBC = GETWORDNUM(m.tcTable,1,"!")
  lcTable = GETWORDNUM(m.tcTable,2,"!")
ENDIF
IF NOT EMPTY(m.lcDBC)
  *
  *  XXFWLIBS.VCX/cusDBCSvc.OpenTable() doesn't
  *  open any passed database if it's not already 
  *  open -- the indicated database must be opened
  *  here first
  *
  *  NOTE that the 2nd parameter is passed as 
  *  .NULL., indicating that all we care about
  *  is whether the database is open -- it 
  *  doesn't matter whether it is open for
  *  EXCLUSIVE use or not
  *
  IF NOT XXOpenDatabase(m.lcDBC,.NULL.)
    RETURN .f.
  ENDIF
ENDIF

IF TYPE("oLib.oDBCSvc.BaseClass") = "C"
  lnTBufferMode = oLib.oDBCSvc.inBufferMode_Table
  lnVBufferMode = oLib.oDBCSvc.inBufferMode_View
  oLib.oDBCSvc.inBufferMode_Table = 1
  oLib.oDBCSvc.inBufferMode_View = 3                        
  llRetVal = oLib.oDBCSvc.OpenTable(m.lcTable, ;
                                    m.tcOrder, ;
                                    m.tlExclusive, ;
                                    m.tcAlias, ;
                                    m.lcDBC, ;
                                    m.tlNoUpdate, ;
                                    m.tlGetData, ;
                                    SET("DATASESSION"))
  oLib.oDBCSvc.inBufferMode_Table = m.lnTBufferMode
  oLib.oDBCSvc.inBufferMode_View = m.lnVBufferMode
 ELSE
  loDBCSvc = CREATEOBJECT("cusDBCSvc")
  loDBCSvc.inBufferMode_Table = 1
  loDBCSvc.inBufferMode_View = 3
  llRetVal = loDBCSvc.OpenTable(m.lcTable, ;
                                m.tcOrder, ;
                                m.tlExclusive, ;
                                m.tcAlias, ;
                                m.lcDBC, ;
                                m.tlNoUpdate, ;
                                m.tlGetData, ;
                                SET("DATASESSION"))
  loDBCSvc = .NULL.
ENDIF

RETURN m.llRetVal
