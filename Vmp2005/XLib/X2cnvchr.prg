*
*  X2CNVCHR.PRG
*  Converts the passed character string to a value
*    of the passed data type.  If unable to process,
*    returns .NULL. in VFP, SPACE(0) in Fox2x
*  -OR-
*  Converts the passed value to a string (when the 2nd
*    parameter is not passed)  If unable to process,
*    returns .NULL. in VFP, SPACE(0) in Fox2x
*  -OR-
*  If tuValue is passed as a string and tcDataType is passed as
*    "D" or "T", but the resulting date/datetime is invalid,
*    a blank date/datetime is RETURNed, as would be the
*    case from CTOD() or CTOT()
*
*  This routine is included for backward compatibility
*  and is not called anywhere in the current VMP
*  framework -- use X8ConvChar.PRG instead.  This routine
*  IS, however, called by X3CKCHEK.PRG, and so will
*  be pulled into a VMP project that calls X3CKCHEK.PRG.
*
*  Copyright (c) 1997-2005 Visionpace   All Rights Reserved
*                17501 East 40 Hwy., Suite 128
*                Independence, MO 64055
*                816-350-7900 
*                http://www.visionpace.com
*                http://vmpdiscussion.visionpace.com
*  Author:  Drew Speedie and Ed Lennon
*           Special thanks to Dale Kiefling
*
*  lParameters
*       tuValue (R) Value to be converted
*    tcDataType (O) Data type to convert the tuValue to:
*                     "C"haracter
*                     "N"umeric
*                     "D"ate
*                     Date"T"ime
*                     "L"ogical
*                     "E"xpression
*                     Currenc"Y"
*                   IF tuValue is a character string to
*                     be converted to a specific data
*                     type, tcDataType is required.
*                   If tcDataType is not passed, this
*                     routine converts tuValue to a
*                     character string.
*
parameters tuValue, tcDataType
#IF TYPE("_VFP") = "O" AND VERSION(5) >= 700
local luReturnValue, lcString, lcDataType, llStringToValue, ; 
      llDataTypeOK, lnDecimals, lnSetDecimals, llEmpty, ;
      junk, lcOnError, llError, lcSetPoint

  *
  *  support for _VFP.LanguageOptions = 1, strict detection
  *  of undeclared variables
  *
#ELSE
private luReturnValue, lcString, lcDataType, llStringToValue, ; 
        llDataTypeOK, lnDecimals, lnSetDecimals, llEmpty, ;
        junk, lcOnError, llError, lcSetPoint
#ENDIF

#IF "VISUAL"$upper(version())
  luReturnValue = .NULL.
  llDataTypeOK = type("tcDataType") = "C" ;
      AND !isnull(tcDataType) ;
      AND !empty(tcDataType) ;
      AND lenc(alltrim(tcDataType)) = 1 ;
      AND inlist(upper(alltrim(tcDataType)),"C","N","D","L","T","E","Y")
#ELSE
  luReturnValue = space(0)
  llDataTypeOK = type("tcDataType") = "C" ;
      AND !empty(tcDataType) ;
      AND (len(alltrim(tcDataType)) = 1 ;
      AND inlist(upper(tcDataType),"C","N","D","L","E"))
#ENDIF

IF llDataTypeOK
  *
  *  tcDataType was passed as a valid data type
  *
  *  convert character string tuValue to
  *  the passed data type
  *
  #IF "VISUAL"$upper(VERSION())
    lcDataType = upper(alltrim(LEFTC(tcDataType,1)))
  #ELSE
    lcDataType = upper(alltrim(LEFT(tcDataType,1)))
  #ENDIF

  IF lcDataType = "C"
    llStringToValue = .F.
  ELSE
    llStringToValue = .T.
    lcString = tuValue
  ENDIF
 ELSE
  *
  *  tuValue is to be converted to a string
  *  (2nd parameter not passed)
  *
  llStringToValue = .F.
ENDIF
IF llStringToValue
  *
  *  convert the passed string to a value of the
  *  indicated data type
  *
  DO CASE
    **********************************************
    CASE lcDataType = "C"   &&& character
    **********************************************
      luReturnValue = alltrim(lcString)
    **********************************************
    CASE inlist(lcDataType,"N","Y")  &&& numeric/currency
    **********************************************
      IF empty(lcString)
        luReturnValue = 0
       ELSE
        lcString = alltrim(lcString)
        lcSetPoint = SET("POINT")
        lnDecimals = 0
        IF lcSetPoint $ lcString
          lnDecimals = substrc(lcString,at_c(lcSetPoint,lcString)+1)
          lnDecimals = lenc(lnDecimals)
        ENDIF
        IF lnDecimals > 0
          lnSetDecimals = SET("DECIMALS")
          set decimals to lnDecimals
          luReturnValue = val(lcString)
          set decimals to lnSetDecimals
         ELSE
          luReturnValue = int(val(lcString))
         ENDIF
       ENDIF empty(lcString)
    **********************************************
    CASE lcDataType = "D" AND lcString = "^"  
    **********************************************
      *
      *  "^yyyy-mm-dd" strictdate format
      *
      #IF "VISUAL" $ upper(version()) ;
           AND upper(version()) # "VISUAL FOXPRO 05" ;
           AND upper(version()) # "VISUAL FOXPRO 03" 
      *
      *  applies to VFP only, 
      *  this #IF..#ENDIF allows the use of the new format DATE() 
      *  function
      *
      lcString = alltrim(lcString)
      lcString = substrc(lcString,2)   &&& remove the leading "^"
            
      IF lenc(lcString) = 10   
        *
        *  yyyy-mm-dd or any of its variations
        *
       ELSE
        *
        *  you will now crash due to an ambiguous date
        *
        error 2034
      ENDIF 
      lnYear = int(val(leftc(lcString,4)))
      lnMonth = int(val(substrc(lcString,6,2)))
      lnDay = int(val(substrc(lcString,9,2)))
*!*	      luReturnValue = date(lnYear,lnMonth,lnDay)
      IF lnYear < 100
        *
        *  the DATE() function can't handle
        *  dates whose year is less than 100
        *
        *  and the GOMONTH() function can't
        *  handle dates prior to Jan 1 1753
        *
        LOCAL lnYearDateFunction, lnGoYears
        lnYearDateFunction = 100
        lnGoYears = 100 - lnYear
        luReturnValue = DATE(lnYearDateFunction,lnMonth,lnDay) - ;
                        ROUND((365.25*lnGoYears),0)
       ELSE
        luReturnValue = date(lnYear,lnMonth,lnDay)
      ENDIF
      #ENDIF
    **********************************************
    CASE lcDataType = "D" AND lcString # "^" 
    **********************************************
      *
      *  "mm/dd/yyyy" or any of its SET DATE variations
      *   
      lcString = alltrim(lcString)
      #IF "VISUAL"$upper(vers())
      llempty = empty(chrtranc(lcString,"/",space(0))) ;
                OR ;
                EMPTY(CHRTRANC(m.lcString,".",SPACE(0))) ;
                OR ;
                EMPTY(CHRTRANC(m.lcString,"-",SPACE(0)))
      #ELSE
      llempty = empty(chrtran(lcString,"/",SPACE(0))) ;
                OR ;
                EMPTY(CHRTRAN(m.lcString,".",SPACE(0))) ;
                OR ;
                EMPTY(CHRTRAN(m.lcString,"-",SPACE(0)))
      #ENDIF

      IF empty(lcString) OR llempty
        luReturnValue = {}
       ELSE
        #IF upper(version()) = "VISUAL FOXPRO 05" ;
            OR upper(version()) = "VISUAL FOXPRO 03" 
        luReturnValue = ctod(alltrim(lcString))
        #ELSE
        local lcSetDate, lnYear, lnMonth, lnDay, lnStrictDate
        lnStrictDate = set("STRICTDATE")
        lcSetDate = set("DATE")
        IF lenc(lcString) = 10   
          *
          *  mm/dd/yyyy or any of its SET DATE variations
          *
         ELSE
          *
          *  mm/dd/yy or any of its SET DATE variations --
          *  you will now crash due to an ambiguous date
          *
          error 2034
        ENDIF 
        set strictdate to 0
        junk = "lcString = ctod(tuValue)"   &&& respecting current SET DATE
        &junk  &&& prevent a compiler error when SET STRICTDATE = 1 or 2
        IF empty(lcString)
          *
          *  invalid date was passed -- RETURN value
          *  is {}, same as it would be from:
          *    ctod(<Invalid Date>)
          *
          luReturnValue = {}
         ELSE
          set date american
          lcString = dtoc(lcString)
          set strictdate to (lnStrictDate)
          lnMonth = int(val(leftc(lcString,2)))
          lnDay = int(val(substrc(lcString,4,2)))
          lnYear = int(val(substrc(lcString,7)))
          set date &lcSetDate
*!*	          luReturnValue = date(lnYear,lnMonth,lnDay)
          IF lnYear < 100
            *
            *  the DATE() function can't handle
            *  dates whose year is less than 100
            *
            *  and the GOMONTH() function can't
            *  handle dates prior to Jan 1 1753
            *
            LOCAL lnYearDateFunction, lnGoYears
            lnYearDateFunction = 100
            lnGoYears = 100 - lnYear
            luReturnValue = DATE(lnYearDateFunction,lnMonth,lnDay) - ;
                            ROUND((365.25*lnGoYears),0)
           ELSE
            luReturnValue = DATE(lnYear,lnMonth,lnDay)
          ENDIF
        ENDIF
        #ENDIF
      ENDIF  &&& empty(lcString)

    **********************************************
    CASE lcDataType = "T" AND lcString # "^" 
    **********************************************
      lcString = alltrim(lcString)
      IF empty(lcString)
        luReturnValue = {//::}
      ELSE
        #IF upper(version()) = "VISUAL FOXPRO 05" ;
            OR upper(version()) = "VISUAL FOXPRO 03" 
        luReturnValue = ctot(lcString)
        #ELSE
        local lcSetDate, lnYear, lnMonth, lnDay, lnHours, lnMin, lnSec, llPM, lnStrictDate
        lcSetDate = set("DATE")
        lnStrictDate = set("STRICTDATE")
        set strictdate to 0
        junk = "lcString = ctot(lcString)"   &&& respecting current SET DATE
        &junk  &&& prevent a compiler error when SET STRICTDATE = 1 or 2
        set strictdate to (lnStrictDate)
        set date american
        IF empty(lcString)
          set date &lcSetDate
          luReturnValue = {//::}
         ELSE
          lcString = ttoc(lcString)
          *  mm/dd/yyyy hh:mm:ss
          *  mm/dd/yyyy
          *  mm/dd/yy
          *  mm/dd/yy hh:mm:ss
          DO CASE
            CASE lenc(lcString) = 8
              * mm/dd/yy
              error 2034
            CASE lenc(lcString) = 10
              * mm/dd/yyyy
              lcString = lcString + " 00:00:00"
              * mm/dd/yyyy hh:mm:ss
            CASE substrc(lcString,9,1) = space(1)
              * mm/dd/yy hh:mm:ss
              error 2034
            CASE inlist(upper(rightc(lcString,2)),"AM","PM")
              * mm/dd/yyyy hh:mm:ss AM or mm/dd/yyyy hh:mm:ss PM
              lcString = upper(lcString)
              IF rightc(lcString,2) = "PM"
                llPM = .t.
              ENDIF
              * strip off the "AM"/"PM"
              lcString = alltrim(leftc(lcString,lenc(lcString)-2))
              * mm/dd/yyyy hh:mm:ss
            OTHERWISE
              * mm/dd/yyyy hh:mm:ss
          ENDCASE
          lnMonth = int(val(leftc(lcString,2)))
          lnDay = int(val(substrc(lcString,4,2)))
          lnYear = int(val(substrc(lcString,7,4)))
          lnHours = int(val(substrc(lcString,12,2)))
          lnMin = int(val(substrc(lcString,15,2)))
          lnSec = int(val(substrc(lcString,18,2)))
          IF llPM
            lnHours = lnHours + 12
          ENDIF  
          set date &lcSetDate
          luReturnValue = datetime(lnYear,lnMonth,lnDay,lnHours,lnMin,lnSec)
        ENDIF
        #ENDIF
      ENDIF empty(lcString)
    **********************************************
    CASE lcDataType = "T" AND lcString = "^" 
    **********************************************
      *
      *  "^yyyy-mm-dd hh:mm:ss" strictdate format
      *
      #IF "VISUAL" $ upper(version()) ;
           AND upper(version()) # "VISUAL FOXPRO 05" ;
           AND upper(version()) # "VISUAL FOXPRO 03"
      *
      *  applies to VFP only, 
      *  this #IF..#ENDIF allows the use of LOCAL and new format DATETIME()
      *
      lcString = alltrim(lcString)
      lcString = substrc(lcString,2)   &&& remove the leading "^"
      
      local llPM
      IF rightc(lcString,2) = "PM"
        llPM = .t.
      ENDIF
      IF rightc(lcString,2) = "PM" OR rightc(lcString,2) = "AM"
        lcString = leftc(lcString,lenc(lcString)-3)
      ENDIF
            
      IF lenc(lcString) = 19   
        *
        *  yyyy-mm-dd hh:mm:ss or any of its variations
        *
       ELSE
        *
        *  you will now crash due to an ambiguous datetime
        *
        error 2034
      ENDIF 
      lnYear = int(val(leftc(lcString,4)))
      lnMonth = int(val(substrc(lcString,6,2)))
      lnDay = int(val(substrc(lcString,9,2)))
      lnHours = int(val(substrc(lcString,12,2)))
      lnMin = int(val(substrc(lcString,15,2)))
      lnSec = int(val(substrc(lcString,18,2)))
      IF llPM
        lnHours = lnHours + 12
      ENDIF
      luReturnValue = datetime(lnYear,lnMonth,lnDay,lnHours,lnMin,lnSec)
      #ENDIF
    **********************************************
    CASE lcDataType = "L"   &&& logical
    **********************************************
      lcString = upper(alltrim(lcString))
      DO CASE
        CASE empty(lcString)
          luReturnValue = .F.
        CASE inlist(lcString,"T",".T.","TRUE","Y","YES")
          luReturnValue = .T.
        CASE inlist(lcString,"F",".F.","FALSE","N","NO")
          luReturnValue = .F.
        OTHERWISE
          * unable to process
      ENDCASE
    **********************************************
    CASE lcDataType = "E"   &&& expression
    **********************************************
      IF !empty(lcString)
        lcOnError = on("ERROR")
        llError = .f.
        on error llError = .t.
        luReturnValue = eval(alltrim(lcString))
        on error &lcOnError  
        IF llError
          #IF "VISUAL"$upper(version())
            luReturnValue = .NULL.
          #ELSE
            luReturnValue = space(0)
          #ENDIF
        ENDIF
      ENDIF !empty(lcString)
  ENDCASE
 ELSE
  *
  *  convert the passed value to a string
  *
  #IF "VISUAL"$upper(version())
  DO CASE
    CASE type("tuValue") = "C"
      luReturnValue = alltrim(tuValue)
    CASE inlist(type("tuValue"),"D","T") OR isnull(tuValue) 
      luReturnValue = alltrim(transform(tuValue,space(0)))
    CASE inlist(type("tuValue"),"N","Y")
      luReturnValue = alltrim(padr(tuValue,254))
    CASE type("tuValue") = "L"
      luReturnValue = IIF(tuValue,".T.",".F.")
  ENDCASE
  #ELSE
  DO CASE
    CASE type("tuValue") = "C"
      luReturnValue = alltrim(tuValue)
    CASE inlist(type("tuValue"),"D","T")
      luReturnValue = alltrim(transform(tuValue,space(0)))
    CASE type("tuValue") = "N"
      luReturnValue = alltrim(padr(tuValue,254))
    CASE type("tuValue") = "L"
      luReturnValue = IIF(tuValue,".T.",".F.")
  ENDCASE
  #ENDIF
ENDIF
return luReturnValue
