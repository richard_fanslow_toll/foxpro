* 
*  X3UUIDNA.PRG
*  Returns a Universally Unique Network ID/Address 
*  for the current workstation from the Windows registry
*
*  Copyright (c) 2000-2005  Visionpace    All Rights Reserved
*                17501 East 40 Hwy., Suite 218
*                Independence, MO 64055
*                816-350-7900 
*                http://www.visionpace.com
*                http://vmpdiscussion.visionpace.com
*  Author:  Art Bergquist
*
*
local lcKeyPath, lcValueName, lcReturnValue
lcKeyPath     = 'HKEY_LOCAL_MACHINE\SOFTWARE\Description\Microsoft\Rpc\UuidtemporaryData'
lcValueName   = 'NetworkAddress'
lcReturnValue =  Bin2Hex(GetString(lcKeyPath, lcValueName))

Return lcReturnValue

*-----------------------------------------------------------------------
Function GetString                            && Get String Value
LParameters tcKeyPath, tcValueName

Declare Long RegCreateKeyEx In Win32API      ;
   Long   hKey				            ,    ;
   String lpSubKey			            ,    ;
   Long   Reserved			            ,    ;
   String lpClass		            	,    ;
   Long   dwOptions			            ,    ;
   Long   samDesired		            ,    ;
   Long   lpSecurity		            ,    ;
   Long   @phkResult		            ,    ;
   Long   @lpdwDisposition

Declare Long RegCloseKey    In Win32API    ;
   Long   hKey

Declare Long RegQueryValueEx In Win32API    ;
   Long   hKey				            ,	;
   String lpValueName		            ,	;
   Long   Reserved			            ,	;
   Long   @lpdwType			            ,	;
   String @lpData			            ,   ;
   Long   @lpdwcData

#DEFINE KEY_ALL_ACCESS          0x3F
#DEFINE REG_OPTION_NON_VOLATILE 0

Local lcStringValue, lcStringValueLen, lcRegPath, lcPosition, lcKeyName, lnHandle
lcStringValue = ''

lcRegPath  = StrTran(AllTrim(tcKeyPath) + '\', '\\', '\')
lcPosition = At('\', lcRegPath)
lcKeyName  = Upper(Left(lcRegPath, lcPosition - 1))
lcRegPath  = SubStr(lcRegPath, lcPosition + 1)
lcRegPath  = Left(lcRegPath, Len(lcRegPath) - 1)

Do Case
  Case lcKeyName = 'HKEY_CLASSES_ROOT'
    lnHandle = 0x80000000

  Case lcKeyName = 'HKEY_CURRENT_USER'
    lnHandle = 0x80000001

  Case lcKeyName = 'HKEY_LOCAL_MACHINE'
    lnHandle = -2147483646                        && 0x80000002

  Case lcKeyName = 'HKEY_USERS'
    lnHandle = 0x80000003

  Otherwise
    lnHandle = 0
EndCase

Local lnAction, lnNewHandle, lnResult
lnAction    = 0
lnNewHandle = 0
lnResult    = RegCreateKeyEx(lnHandle, lcRegPath, 0, '', REG_OPTION_NON_VOLATILE, ;
                                KEY_ALL_ACCESS, 0, @lnNewHandle, @lnAction)
If Empty(lnResult)
  Local lnValueType
  RegCloseKey(lnHandle)
  lnHandle = lnNewHandle

  lcStringValue    = Space(256)
  lcStringValueLen = Len(lcStringValue)
  lnValueType      = 0
  lnResult         = RegQueryValueEx(lnHandle, tcValueName, 0, @lnValueType, ;
                                        @lcStringValue, @lcStringValueLen)
  If Empty(lnResult)
    lcStringValue = Left(lcStringValue, lcStringValueLen)
  Else
    lcStringValue = ''
  EndIf
  RegCloseKey(lnHandle)
EndIf
Return lcStringValue

*-----------------------------------------------------------------------
Function Bin2Hex            && Convert Binary (Base 2) to Hexadecimal (Base 16) as String
LParameters tcBinaryString

Local lcHexadecimalString, lnByteCounter, lnCurrentByteBase10, lcCurrentByteBase16
lcHexadecimalString = ''

Local Array laHexCharacters[16]
laHexCharacters[ 1] = '0'
laHexCharacters[ 2] = '1'
laHexCharacters[ 3] = '2'
laHexCharacters[ 4] = '3'
laHexCharacters[ 5] = '4'
laHexCharacters[ 6] = '5'
laHexCharacters[ 7] = '6'
laHexCharacters[ 8] = '7'
laHexCharacters[ 9] = '8'
laHexCharacters[10] = '9'
laHexCharacters[11] = 'A'
laHexCharacters[12] = 'B'
laHexCharacters[13] = 'C'
laHexCharacters[14] = 'D'
laHexCharacters[15] = 'E'
laHexCharacters[16] = 'F'

For lnByteCounter = 1 To Len(tcBinaryString)
  lnCurrentByteBase10 = Asc(SubStr(tcBinaryString, lnByteCounter, 1))        && Base 10
  lcCurrentByteBase16 = laHexCharacters[Int(lnCurrentByteBase10 / 16) + 1] + ;
                        laHexCharacters[Mod(lnCurrentByteBase10 , 16) + 1]   && Base 16
  lcHexadecimalString = lcHexadecimalString + lcCurrentByteBase16
EndFor

Return lcHexadecimalString
