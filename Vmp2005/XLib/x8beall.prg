*
*  X8BEALL.PRG
*  BINDEVENT() a property/method of ALL or specific 
*  members of the passed container to the passed delegate.
*
*  Copyright (c) 2003-2005 Visionpace   All Rights Reserved
*                17501 East 40 Hwy., Suite 218
*                Independence, MO 64055
*                816-350-7900 
*                http://www.visionpace.com
*                http://vmpdiscussion.visionpace.com
*  Author:  Drew Speedie  
*
*  EXAMPLES:
*  XXDTES("XXFWCAL.VCX","X8BEALL","ctrCalendar","Init")
*
*
*  lParameters
*        toContainer (R)  The form/pageframe/page/optiongroup/
*                           commandgroup/grid/column container
*                           whose members (tcSourceBaseClass)
*                           are the source of each BINDEVENT()
*                           to toDelegate
*                         For any members of toContainer that
*                           are themselves containers, this
*                           .PRG is called recursively -- 
*                           "drilling down" through each level
*                           of containership
*        tcSourcePEM (R)  BINDEVENT() parameter #2
*         toDelegate (R)  BINDEVENT() parameter #3
*   tcDelegateMethod (R)  BINDEVENT() parameter #4
*            tnFlags (O)  BINDEVENT() parameter #5
*  tcInclBaseClasses (O)  If passed, indicates a comma-separated
*                           list of VFP base classes to be
*                           BINDEVENT()ed, and only toContainer.Members 
*                           (and their members, etc.) of the
*                           indicated base class are BINDEVENT()ed
*                           if they have the passed 
*                           tcSourcePEM.
*                         Default behavior is that ALL
*                           toContainer.Members (and their 
*                           members, etc.) are BINDEVENT()ed
*                           if they have the passed 
*                           tcSourcePEM.
*                         The "drill-down" behavior here is
*                           unaffected by this parameter -- if,
*                           for example, you pass this parameter
*                           as "CONTAINER", this proc still
*                           drills down inside Pages, looking
*                           for Containers.
*  tcExclBaseClasses (O)  If passed, indicates a comma-separated
*                           list of VFP base classes to be
*                           EXCLUDED from this action.  
*                         For example, you may want to bind to
*                           a method of most controls on a
*                           form/page/container/etc., but, for
*                           performance reasons, NOT bother
*                           with the labels -- pass this 
*                           parameter as "LABEL"
*                         The "drill-down" behavior here is
*                           unaffected by this parameter -- if,
*                           for example, you pass this parameter
*                           as "CONTAINER,GRID", each encountered
*                           Container or Grid object is checked, 
*                           just not BINDEVENT()ed itself.
*
*  NOTE -- if you specify a VFP Base Class in BOTH tcInclBaseClasses
*          AND tcExclBaseClasses, then this routine IGNORES any
*          objects of that VFP Base Class
*
LPARAMETERS toContainer as Object, ;
            tcSourcePEM as String, ;
            toDelegate as Object, ;
            tcDelegateMethod as String, ;
            tnFlags as Integer, ;
            tcInclBaseClasses as String, ;
            tcExclBaseClasses as String

LOCAL lcInclude, lnFlags, lcExclude, llBind, loObject
IF VARTYPE(m.tcInclBaseClasses) = "C" ;
     AND NOT EMPTY(m.tcInclBaseClasses)
  lcInclude = UPPER(ALLTRIM(m.tcInclBaseClasses))
 ELSE
  lcInclude = SPACE(0)
ENDIF
IF VARTYPE(m.tcExclBaseClasses) = "C" ;
     AND NOT EMPTY(m.tcExclBaseClasses)
  lcExclude = UPPER(ALLTRIM(m.tcExclBaseClasses))
 ELSE
  lcExclude = SPACE(0)
ENDIF
IF VARTYPE(m.tnFlags) = "N" 
  lnFlags = m.tnFlags
 ELSE
  lnFlags = 0
ENDIF

FOR EACH loObject IN toContainer.Objects
  llBind = .f.
  DO CASE
    ************************************************************
    CASE NOT PEMSTATUS(loObject,m.tcSourcePEM,5)
    ************************************************************
      *  no BINDEVENT() -- loObject doesn't have
      *  the indicated PEM
    ************************************************************
    CASE EMPTY(m.lcInclude) AND EMPTY(m.lcExclude)
    ************************************************************
      llBind = .t.
    ************************************************************
    CASE NOT EMPTY(m.lcInclude) AND NOT EMPTY(m.lcExclude) ;
         AND UPPER(ALLTRIM(loObject.BaseClass)) $ m.lcExclude ;
         AND UPPER(ALLTRIM(loObject.BaseClass)) $ m.lcExclude
    ************************************************************
      *
      *  you have specified this BaseClass in BOTH the
      *  tcInclBaseClasses AND tcExclBaseClasses string
      *  -- no BINDEVENT()
      *         
    ************************************************************
    CASE NOT EMPTY(m.lcInclude) AND NOT EMPTY(m.lcExclude) ;
         AND UPPER(ALLTRIM(loObject.BaseClass)) $ m.lcInclude ;
         AND NOT UPPER(ALLTRIM(loObject.BaseClass)) $ m.lcExclude
    ************************************************************
      llBind = .t.
    ************************************************************
    CASE NOT EMPTY(m.lcInclude) AND UPPER(ALLTRIM(loObject.BaseClass)) $ m.lcInclude
    ************************************************************
      llBind = .t.
    ************************************************************
    CASE NOT EMPTY(m.lcExclude) AND NOT UPPER(ALLTRIM(loObject.BaseClass)) $ m.lcExclude
    ************************************************************
      llBind = .t.
    ************************************************************
    OTHERWISE
    ************************************************************
      *  no BINDEVENT() -- loObject does not meet
      *  the Include/Exclude restrictions
  ENDCASE
  IF m.llBind
    BINDEVENT(loObject,m.tcSourcePEM,toDelegate,m.tcDelegateMethod,m.lnFlags)
  ENDIF
  IF INLIST(UPPER(ALLTRIM(loObject.BaseClass)), ;
       "CONTAINER","GRID","COLUMN","PAGEFRAME","PAGE","OPTIONGROUP","COMMANDGROUP")
    X8BEALL(m.loObject, ;
            m.tcSourcePEM, ;
            m.toDelegate, ;
            m.tcDelegateMethod, ;
            m.tnFlags, ;
            m.lcInclude, ;
            m.lcExclude)
  ENDIF
ENDFOR
loObject = .NULL.

RETURN 
