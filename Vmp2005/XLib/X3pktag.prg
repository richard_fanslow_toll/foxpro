*
*  X3PKTAG.PRG
*  Returns the name of the Primary key index tag of the passed
*  tcAlias.  Returns SPACE(0) if the passed tcAlias is a 
*  free table, Fox2x table, or a contained table that has no
*  PK index tag specified, or if the passed alias is not
*  used().
*
*  Copyright (c) 1997-2005 Visionpace   All Rights Reserved
*                17501 East 40 Hwy., Suite 218
*                Independence, MO 64055
*                816-350-7900 
*                http://www.visionpace.com
*                http://vmpdiscussion.visionpace.com
*  Author:  Drew Speedie
*
*  Usage:
*    lcPKTag = X3PKTAG("CUSTOMERS")
*
*  lParameters
*    tcAlias (O) Alias of the table whose PK tag name
*                  is to be returned (tcAlias must be open)
*                Defaults to the current ALIAS()
*                  
LPARAMETERS tcAlias
LOCAL lcAlias
IF TYPE("tcAlias") = "C" ;
     AND NOT ISNULL(m.tcAlias) ;
     AND NOT EMPTY(m.tcAlias)
  lcAlias = UPPER(ALLTRIM(m.tcAlias))
 ELSE
  lcAlias = ALIAS()  
ENDIF

IF EMPTY(m.lcAlias) OR NOT USED(m.lcAlias)
  RETURN SPACE(0)
ENDIF

LOCAL lcDatabase
lcDatabase = CURSORGETPROP("DATABASE",m.lcAlias)
IF EMPTY(m.lcDatabase) OR NOT X3SRCTYP(m.lcAlias) = "TC"
  *
  *  free table or Fox2x table or view or SPT cursor
  *  or CREATE CURSOR cursor or SQL-Select (non-filtered)
  *  cursor
  *
  RETURN SPACE(0)
ENDIF
LOCAL lcPKTag, lcSetDatabase
lcSetDatabase = SET("DATABASE")
lcPKTag = SPACE(0)
SET DATABASE TO (m.lcDatabase)
lcPKTag = UPPER(DBGETPROP(CURSORGETPROP("SourceName",m.lcAlias),"TABLE","PrimaryKey"))
*
*  NOTE:  if lcAlias has no PK, lcPKTag = space(0)
*
IF EMPTY(m.lcSetDatabase)
  SET DATABASE TO
 ELSE
  SET DATABASE TO (m.lcSetDatabase)
ENDIF
RETURN m.lcPKTag
