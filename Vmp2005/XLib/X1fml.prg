*
*  X1FML.PRG
*  Accepts 3 parameters, returns them formatted
*  FirstName, MiddleInitial, LastName 
*
*  Copyright (c) 2000-2005 Visionpace   All Rights Reserved
*                17501 East 40 Hwy., Suite 218
*                Independence, MO 64055
*                816-350-7900 
*                http://www.visionpace.com
*                http://vmpdiscussion.visionpace.com
*  Author:  Drew Speedie
*
*  PLEASE NOTE!!!
*  The value RETURNed by this program does not contain
*  any trailing blanks -- if you use X1FML() in a
*  SQL-Select or as the ControlSource for a grid.Column,
*  you must pad it:
*    SELECT *, ;
*         padr(X1FML(Usr_FirstName,Usr_MiddleInitial,Usr_FirstName),40) AS FullName ;
*      FROM VM!Users ;
*      INTO CURSOR C_TEMP
*
*  Usage:
*    xx = x1fml("Jane","X","Doe")
*    xx = x1fml(Usr_FirstName,Usr_MiddleInitial,Usr_FirstName)
*   
parameters tcFirstName, ;
           tcMiddleInit, ;
           tcLastName
           
private lcRetVal
lcRetVal = space(0)

IF type("tcFirstName") = "C" AND !empty(tcFirstName)
  lcRetVal = lcRetVal + alltrim(tcFirstName) + " "
ENDIF

IF type("tcMiddleInit") = "C" AND !empty(tcMiddleInit)
  lcRetVal = lcRetVal + alltrim(tcMiddleInit) + ". "
ENDIF

IF type("tcLastName") = "C" AND !empty(tcLastName)
  lcRetVal = lcRetVal + alltrim(tcLastName)
ENDIF

lcRetVal = alltrim(lcRetVal)

return lcRetVal
