*
*  XXDTXRelPath.PRG
*  Remove relative path information from
*  the ClassLoc field and/or DataEnvironment cursors of
*  .SCX/.VCX files in the selected .PJX project or 
*  directory/folder.
*
*  Copyright (c) 2000-2005 Visionpace   All Rights Reserved
*                17501 East 40 Hwy., Suite 218
*                Independence, MO 64055
*                816-350-7900 
*                http://www.visionpace.com
*                http://vmpdiscussion.visionpace.com
*  Author:  Drew Speedie  
*
*  NOTE that XXDTXRelPath.SCX is an .SCX-based form, not a
*  form class in XXTOOLS.VCX and is comprised entirely
*  of VFP base classes.  This design is so that if this
*  utility is run against the folder containing VMP
*  classes, it won't bomb as it hits XXTOOLS.VCX (or any
*  other .VCXs containing potential members of 
*  XXDTXRelPath.SCX).
*

IF UPPER(JUSTFNAME(SYS(16,0))) = "XXDT."   &&& XXDT.FXP
  MESSAGEBOX(JUSTFNAME(PROGRAM()) + ;
             " cannot be run directly from this XXDT utility." + CHR(13) + CHR(13) + ;
             "DO " + JUSTFNAME(PROGRAM()) + CHR(13) + CHR(13) + ;
             "is about to be entered into the Command Window, so you can " + ;
             "run " + JUSTFNAME(PROGRAM()) + " from there.", ;
             48,"Please Note")
  KEYBOARD "DO " + JUSTFNAME(PROGRAM()) PLAIN CLEAR        
  RETURN 
ENDIF

IF FILE("XXPROCDT.PRG")
  DO SavePJXs IN XXPROCDT.PRG
ENDIF
CLEAR ALL
CLOSE ALL
DO FORM XXDTXRelPath
IF FILE("XXPROCDT.PRG")
  DO RestorePJXs IN XXPROCDT.PRG
ENDIF

RETURN 




*****************************************************
PROCEDURE ProcessPJX
*****************************************************
*
*  called from XXDTXRelPath_SCX.Action()
*    XXDTES("XXDTXRelPath.SCX","ProcessPJX(","Form","Action")
*
*  can also be called directly, from other code, 
*  in which case Review = .f. 
*    XXDTES("VMP2002to2003.PRG","ProcessPJX(")
*
LPARAMETERS tcPJX, ;
            toXXDTXRelPath  &&& object reference to XXDTXRelPath.SCX

LOCAL lcCurrDir, lcFileName, lnCount, xx, lcNewFileName, ;
      lcJunk, lcPJX, llReview

lcPJX = UPPER(ALLTRIM(m.tcPJX))
IF VARTYPE(m.toXXDTXRelPath) = "O"
  llReview = toXXDTXRelPath.ilReview
 ELSE
  llReview = .f.
ENDIF

lcCurrDir = FULLPATH(CURDIR())
CD (JUSTPATH(m.lcPJX))

lnCount = 0

TRY 
USE (m.lcPJX) IN 0 EXCLUSIVE ALIAS ProcessPJX
CATCH
ENDTRY
IF NOT USED("ProcessPJX")
  MESSAGEBOX("Unable to open " + m.lcPJX + " for EXCLUSIVE use.",48,"Please Note")
  RETURN .f.
ENDIF

LOCAL laDir[1]
SELECT ProcessPJX
SELECT PADR(UPPER(JUSTFNAME(LEFTC(Name,LENC(ALLTRIM(Name))-1))),80) AS TheFile, ;
       PADR(UPPER(LEFTC(Name,LENC(ALLTRIM(Name))-1)),254) AS FullFile ;
     FROM ProcessPJX ;
     INTO ARRAY laDir

ASORT(laDir)

FOR xx = 1 TO ALEN(laDir,1)
  lcFileName = ALLTRIM(laDir[m.xx,2])
  IF INLIST(UPPER(JUSTEXT(m.lcFileName)),"SCX","VCX")
    lnCount = m.lnCount + 1
*!*	    ProcessSCXVCX(m.lcFileName,m.toXXDTXRelPath)
    *
    *  use just the filename, because we're going
    *  to update it with the CURRENT path to that
    *  file, rather than leaving it pathed however
    *  it is at the moment
    *
    ProcessSCXVCX(JUSTFNAME(m.lcFileName),m.toXXDTXRelPath)
  ENDIF
ENDFOR

IF NOT m.llReview
  *
  *  update the Name field in the .PJX project file 
  *
  SELECT ProcessPJX
  LOCATE
  SCAN 
    IF UPPER(Type) = "H"
      * 
      *  skip the Home record
      *
      LOOP 
    ENDIF
    lcNewFileName = ALLTRIM(Name)
    lcNewFileName = LEFTC(m.lcNewFileName,LENC(m.lcNewFilename)-1)
    lcJunk = RIGHTC(ALLTRIM(Name),1)
    lcNewFileName = LOWER(SYS(2014,JUSTFNAME(m.lcNewFileName),ADDBS(FULLPATH(CURDIR()))))
    replace ProcessPJX.Name WITH ;
             m.lcNewFileName+m.lcJunk
  ENDSCAN
ENDIF

USE IN ProcessPJX

CD (m.lcCurrDir)

IF m.lnCount = 0
  MESSAGEBOX("There are no .SCXs/.VCXs to process in " + lcPJX, ;
             48, ;
             "Please Note")
  RETURN .f.             
ENDIF




*********************************************
PROCEDURE ProcessSCXVCX
*********************************************
*
*  called from ProcessPJX() and from 
*  XXDTXRelPath.SCX/ProcessMetaFiles()
*    XXDTES("XXDTXRelPath.SCX","ProcessSCXVCX(","Form1","ProcessMetaFiles")
*
LPARAMETERS tcFileName, toXXDTXRelPath

LOCAL llError, llSCX, lcCaption, llCompile, ;
      llUpdated, lcDisplay, lcLineX, lcClassLoc, llFile, ;
      lcJustFile, llJustFile, lcSaveDir, lcCL, lcCLAfter, ;
      llRetVal, llDatabase, llSCXVCX, llReview, llUpdate, ;
      llRecompile

IF NOT FILE(m.tcFileName)
  *
  *  unable to find
  *
  IF VARTYPE(toXXDTXRelPath) = "O"
    INSERT INTO XXDTXRelPath (TheText,Status) VALUES ;
                         ("% " + tcFileName,1)
    INSERT INTO XXDTXRelPath (TheText,Status) VALUES ;
                         (SPACE(0),0)
  ENDIF
  RETURN .f.
ENDIF

IF VARTYPE(m.toXXDTXRelPath) = "O"
  *
  *  called from XXDTXRelPath.SCX
  *
  llDatabase = toXXDTXRelPath.opgClassLocOrCursor.Value = 1 ;
               OR toXXDTXRelPath.opgClassLocOrCursor.Value = 3 
  llSCXVCX = toXXDTXRelPath.opgClassLocOrCursor.Value = 1 ;
             OR toXXDTXRelPath.opgClassLocOrCursor.Value = 2 
  llReview = toXXDTXRelPath.ilReview
  llUpdate = toXXDTXRelPath.ilUpdate
  lcCaption = toXXDTXRelPath.Caption
  llRecompile = toXXDTXRelPath.ilRecompile
 ELSE
  *
  *  called directly from ProcessPJX()
  *
  llDatabase = .f.
  llSCXVCX = .t.
  llReview = .f.
  llUpdate = .t.
  lcCaption = SPACE(0)
  llRecompile = .f.
ENDIF

llRetVal = .t.      
lcSaveDir = FULLPATH(CURDIR())
CD (JUSTPATH(FULLPATH(m.tcFileName)))

llError = .f.
TRY
USE (m.tcFileName) IN 0 EXCLUSIVE ALIAS ProcessSCXVCX
CATCH
llError = .t.
ENDTRY

IF m.llError
  *
  *  unable to open
  *
  IF VARTYPE(toXXDTXRelPath) = "O"
    INSERT INTO XXDTXRelPath (TheText,Status) VALUES ;
                         ("# "+tcFileName,2)
    INSERT INTO XXDTXRelPath (TheText,Status) VALUES ;
                         (SPACE(0),0)
  ENDIF
  CD (m.lcSaveDir)
  RETURN .f.
ENDIF

llSCX = (UPPER(JUSTEXT(m.tcFileName)) = "SCX")

IF VARTYPE(toXXDTXRelPath) = "O"
  toXXDTXRelPath.Caption = "Processing " + tcFileName + "..."

  *
  *  default record
  *
 INSERT INTO XXDTXRelPath (TheText,Status) VALUES ;
                       (tcFileName,0)
ENDIF

SELECT ProcessSCXVCX
llCompile = .f.

*
*  DataBase and CursorSource properties of cursors
*  in .SCX-based forms
*
IF m.llDatabase
  *
  *  process the Cursor.Database property for each Cursor
  *  object in the DataEnvironment
  *
  LOCAL laLines[1], lcLine, llContained, lnLine
  SCAN FOR UPPER(ALLTRIM(BaseClass)) = "CURSOR" 
    IF EMPTY(ALLTRIM(Parent)) 
      lcDisplay = ALLTRIM(ObjName)
     ELSE
      lcDisplay = ALLTRIM(Parent) + "." + ALLTRIM(ObjName)
    ENDIF
    llContained = "DATABASE =" $ UPPER(Properties)
    ALINES(laLines,Properties)
    FOR xx = 1 TO ALEN(laLines,1)
      lcLine = UPPER(ALLTRIM(laLines[m.xx]))
      IF m.llContained AND m.lcLine = "DATABASE ="
        lnLine = m.xx
        EXIT 
      ENDIF
      IF NOT m.llContained AND m.lcLine = "CURSORSOURCE ="
        lnLine = m.xx
        EXIT 
      ENDIF
    ENDFOR
    IF VARTYPE(m.lnLine) = "N"
      lcLine = laLines[m.lnLine]    
     ELSE
      LOOP 
    ENDIF
    DO CASE
      ***********************************************
      CASE OCCURS("\",m.lcLine) = 0
      ***********************************************
        *
        *  no update necessary
        *    
        IF VARTYPE(m.toXXDTXRelPath) = "O"
          INSERT INTO XXDTXRelPath (TheText,Status) VALUES ;
                               (SPACE(4) + m.lcDisplay,0)
        ENDIF
      ***********************************************
      CASE m.llReview 
      ***********************************************
        *
        *  relative pathing, review only
        *
        IF VARTYPE(m.toXXDTXRelPath) = "O"
          INSERT INTO XXDTXRelPath (TheText,Status) VALUES ;
                               (SPACE(2) + "* " + m.lcDisplay,5)
        ENDIF
      ***********************************************
      OTHERWISE
      ***********************************************
        *
        *  relative pathing, update
        *
        lcLineX = ALLTRIM(SUBSTRC(m.lcLine,AT_C("=",m.lcLine)+1))
        lcLineX = UPPER(ALLTRIM(JUSTFNAME(m.lcLineX)))
        IF m.llContained
          lcLineX = "Database = " + m.lcLineX
         ELSE
          lcLineX = "CursorSource = " + m.lcLineX
        ENDIF
        llError = .f.
        TRY
        replace Properties ;
             WITH STRTRAN(Properties,laLines[lnLine],m.lcLineX) ;
             IN ProcessSCXVCX
        CATCH
        llError = .t.
        ENDTRY
        IF m.llError OR OCCURS("\",ClassLoc) > 0
          *
          *  unable to update
          *
          IF VARTYPE(m.toXXDTXRelPath) = "O"
            INSERT INTO XXDTXRelPath (TheText,Status) VALUES ;
                                 (SPACE(2) + "@ " + m.lcDisplay,3)
          ENDIF
          llRetVal = .f.
         ELSE
          *
          *  updated successfully
          *
          IF VARTYPE(toXXDTXRelPath) = "O"
            INSERT INTO XXDTXRelPath (TheText,Status) VALUES ;
                                 (SPACE(2) + "* " + m.lcDisplay,4)
          ENDIF
          llCompile = .t.
        ENDIF   &&& llError OR OCCURS("\",ClassLoc) > 0
    ENDCASE
  ENDSCAN 
ENDIF

*
*  ClassLoc field of .SCX and .VCX files
*
IF llSCXVCX
  SELECT ProcessSCXVCX
  SCAN FOR NOT EMPTY(Class) ;
       AND NOT INLIST(UPPER(ALLTRIM(BaseClass)),"DATAENVIRONMENT","CURSOR","RELATION","HEADER")
    IF EMPTY(ALLTRIM(Parent)) 
      lcDisplay = ALLTRIM(ObjName)
     ELSE
      lcDisplay = ALLTRIM(Parent) + "." + ALLTRIM(ObjName)
    ENDIF

    IF EMPTY(ClassLoc)
      IF VARTYPE(toXXDTXRelPath) = "O"
        INSERT INTO XXDTXRelPath (TheText, ;
                              Status, ;
                              ClassLoc) ;
                              VALUES ;
                             (SPACE(4) + m.lcDisplay, ;
                              0, ;
                              "none - VFP base class")
      ENDIF
      LOOP
    ENDIF

    lcCL = ALLTRIM(ClassLoc)
    lcCLAfter = m.lcCL
    lcClassLoc = UPPER(ALLTRIM(ClassLoc))
    llFile = FILE(lcClassLoc)
    lcJustFile = UPPER(JUSTFNAME(ClassLoc))
    llJustFile = FILE(m.lcJustFile)

    *
    *  remove
    *  (and add back in current relative pathing if
    *   llUpdate = .t.)
    *
    llError = .f.
    IF OCCURS("\",ClassLoc) = 0 
      *
      *  no removal necessary, the "~" is a placeholder
      *    
      IF VARTYPE(m.toXXDTXRelPath) = "O"
        INSERT INTO XXDTXRelPath (TheText, ;
                              Status, ;
                              ClassLoc) ;
                              VALUES ;
                             (SPACE(2) + "~ " + m.lcDisplay, ;
                              0, ;
                              LOWER(m.lcCL))
      ENDIF
      IF m.llReview
        IF VARTYPE(m.toXXDTXRelPath) = "O"
          replace XXDTXRelPath.TheText WITH SPACE(4) + m.lcDisplay ;
               IN XXDTXRelPath
        ENDIF
      ENDIF
     ELSE
      *
      *  ClassLoc contains pathing
      *
      IF m.llReview
        IF VARTYPE(m.toXXDTXRelPath) = "O"
          INSERT INTO XXDTXRelPath (TheText, ;
                                Status, ;
                                ClassLoc) ;
                                VALUES ;
                               (SPACE(2) + "* " + m.lcDisplay, ;
                                5, ;
                                LOWER(m.lcCL))
        ENDIF
       ELSE
        TRY
        replace ClassLoc ;
             WITH m.lcJustFile  ; &&& justfname(ClassLoc)
             IN ProcessSCXVCX
        CATCH
        llError = .t.
        ENDTRY
        IF llError 
          *
          *  unable to update
          *
          llRetVal = .f.
          IF VARTYPE(m.toXXDTXRelPath) = "O"
            INSERT INTO XXDTXRelPath (TheText, ;
                                  Status, ;
                                  ClassLoc) ;
                                  VALUES ;
                                 (SPACE(2) + "@ " + m.lcDisplay, ;
                                  3, ;
                                  LOWER(m.lcCL))
          ENDIF
         ELSE
          *
          *  updated successfully
          *
          IF VARTYPE(toXXDTXRelPath) = "O"
            INSERT INTO XXDTXRelPath (TheText, ;
                                  Status, ;
                                  ClassLoc) ;
                                  VALUES ;
                                 (SPACE(2) + "* " + m.lcDisplay, ;
                                  4, ;
                                  LOWER(m.lcCL))
          ENDIF
        ENDIF   &&& llError
      ENDIF   &&& llReview
    ENDIF   &&& OCCURS("\",ClassLoc) = 0 
    *
    *  now see about adding back in relative pathing
    *
    *
    *                Status 
    *  ------------------------
    *  Normal/OK          0
    *  Unable to locate   1  %
    *  Unable to open     2  #
    *  Unable to update   3  @
    *  Updated            4  *
    *  Needs update       5  *  (llReview = .t.)
    *
    DO CASE
      **************************************************
      CASE m.llError
      **************************************************
        *
        *  above REPLACE was unsuccessful
        *
        llRetVal = .f.
      **************************************************
      CASE NOT m.llUpdate AND NOT m.llReview
      **************************************************
        lcCLAfter = LOWER(ALLTRIM(ClassLoc))
      **************************************************
      CASE NOT m.llFile AND NOT m.llJustFile
      **************************************************
        *
        *  the indicated ClassLoc file cannot be located
        *  either in the current VFP path (llJustFile)
        *  or where indicated up until now in ClassLoc
        *  (llFile) -- all pathing has already been removed,
        *  and we'll leave it that way, because there's
        *  nothing else we can do
        *  
        IF m.llReview
          lcCLAfter = " (unable to locate " + LOWER(m.lcCL) + ")"
         ELSE
          IF VARTYPE(m.toXXDTXRelPath) = "O"
            replace XXDTXRelPath.TheText WITH CHRTRAN(TheText,"~","%") ;
                    IN XXDTXRelPath   &&& Unable to locate            
          ENDIF
        ENDIF
      **************************************************
      CASE m.llFile AND NOT m.llJustFile
      **************************************************
        *
        *  lcJustFile is nowhere in the VFP path, but 
        *  whatever pathing is included in ClassLoc/
        *  lcClassLoc does indicate a valid file; we'll 
        *  have to go with that
        *  
  *
  *  from here on, llJustFile = .t.
  *
      **************************************************
      CASE m.llJustFile ;
           AND FULLPATH(m.lcJustFile) == FULLPATH(CURDIR())+m.lcJustFile
      **************************************************
        *
        *  no pathing needed because lcJustFile 
        *  is located in the current folder
        *
        lcCLAfter = m.lcJustFile
        IF m.llUpdate
          replace ClassLoc WITH m.lcCLAfter IN ProcessSCXVCX
        ENDIF
      **************************************************
      CASE m.llJustFile ;
           AND NOT JUSTDRIVE(FULLPATH(m.lcJustFile)) == JUSTDRIVE(SYS(5))
      **************************************************
        *
        *  full pathing, not relative pathing, because
        *  lcFilePath indicates a different drive
        *
        lcCLAfter = LOWER(FULLPATH(m.lcJustFile))
        IF m.llUpdate
          replace ClassLoc WITH m.lcCLAfter IN ProcessSCXVCX
        ENDIF
      **************************************************
      OTHERWISE
      **************************************************
        *
        *  llJustFile = .t.
        *  insert relative pathing to lcFullPath
        *
*!*	        lcCLAfter = LOWER(SYS(2014,lcJustFile,ADDBS(SYS(5)+SYS(2003))))
        lcCLAfter = LOWER(SYS(2014,m.lcJustFile,ADDBS(FULLPATH(CURDIR()))))
        IF m.llUpdate
          replace ClassLoc WITH m.lcCLAfter IN ProcessSCXVCX
        ENDIF
    ENDCASE
    IF NOT m.llError ;
         AND NOT UPPER(ALLTRIM(ProcessSCXVCX.ClassLoc)) == m.lcClassLoc
      llCompile = .t.
    ENDIF
    IF VARTYPE(m.toXXDTXRelPath) = "O"
      replace XXDTXRelPath.ClassLocAfter WITH ALLTRIM(m.lcCLAfter) ;
              IN XXDTXRelPath
      replace XXDTXRelPath.TheText WITH CHRTRAN(TheText,"~"," ") ;
              IN XXDTXRelPath    
      IF "%" $ XXDTXRelPath.TheText
        replace XXDTXRelPath.TheText WITH CHRTRAN(TheText,"%"," ") ;
                IN XXDTXRelPath    
        replace XXDTXRelPath.ClassLocAfter WITH ALLTRIM(m.lcCLAfter) + ;
                " (unable to locate " + m.lcCLAfter + ")"
      ENDIF
    ENDIF
  ENDSCAN
ENDIF

USE IN ProcessSCXVCX

IF m.llCompile AND m.llRecompile
  IF VARTYPE(m.toXXDTXRelPath) = "O"
    toXXDTXRelPath.Caption = "Recompiling " + m.tcFileName + "..."
  ENDIF
  IF m.llSCX
    COMPILE FORM (m.tcFileName)
   ELSE
    COMPILE CLASSLIB (m.tcFileName)
  ENDIF
ENDIF

IF VARTYPE(m.toXXDTXRelPath) = "O"
  toXXDTXRelPath.Caption = m.lcCaption

  *
  *  spacer record
  *
  INSERT INTO XXDTXRelPath (TheText,Status) values ;
                       (SPACE(0),0)
ENDIF                       

CD (m.lcSaveDir)

RETURN m.llRetVal

