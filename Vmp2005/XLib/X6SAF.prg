*
*  X6SAF.PRG
*  RETURNs the current _Screen.ActiveForm, if such a
*  form exists, .NULL. if not
*  
*  Copyright (c) 2002-2005 Visionpace   All Rights Reserved
*                17501 East 40 Hwy., Suite 218
*                Independence, MO 64055
*                816-350-7900 
*                http://www.visionpace.com
*                http://vmpdiscussion.visionpace.com
*  Author:  Drew Speedie
*
*  This method RETURNs the equivalent of _Screen.ActiveForm,
*  but handles a couple of potential Gothcha!s for you,
*  and is essentially the same as calling 
*  oForms.ScreenActiveForm().  
*
*  So why does cusForms::ScreenActiveForm() exist?
*  1- Because it was written several years ago, and 
*     remains for backward compatibility with existing 
*     code in VMP applications written before X6SAF.PRG
*     was included with VMP.
*  2- Because oForms.ScreenActiveForm() offers an extra 
*     feature when passing the optional parameter -- this 
*     utility only RETURNs the _Screen.ActiveForm (if any) 
*     object reference.
*  The reason this method has been added 10/31/2002,
*  years after cusForms::ScreenActiveForm() is to 
*  provide the same feature in those situations when
*  oForms doesn't exist yet, particularly on app
*  startup, and developer tools/wizards/builders, when
*  there is no oForms. 
*  
*
*  the calling code should generally check for a .NULL.
*  value returned from here, because this method RETURNs
*  .NULL. whenever something goes wrong here or there
*  is no _Screen.ActiveForm
*

LOCAL loForm
loForm = .NULL.   &&& default
IF NOT TYPE("_Screen.ActiveForm.BaseClass") = "C"
  *
  *  you cannot reliably check VARTYPE(_Screen.ActiveForm),
  *  because if there is currently no active form, VFP
  *  generates an error
  *
  RETURN loForm   &&& .NULL.
ENDIF

loForm = _Screen.ActiveForm
IF UPPER(loForm.BaseClass) == "FORM" 
  *
  *  this is usually the case when _Screen.ActiveForm
  *  is a valid object reference
  *
  RETURN loForm   &&& form object reference
ENDIF

*
*  loForm.BaseClass can be "OLECONTROL" -- when the 
*  real _Screen.ActiveForm contains certain ActiveX 
*  controls, for whatever reason the _Screen.ActiveForm 
*  reference is to the *ActiveX* control, not the form 
*  itself
*  
IF UPPER(loForm.BaseClass) = "OLECONTROL"
  DO WHILE .t.
    loForm = loForm.PARENT
    IF UPPER(loForm.BaseClass) == "FORM"
      EXIT 
    ENDIF
  ENDDO
ENDIF

RETURN loForm   &&& .NULL. or form object reference


 