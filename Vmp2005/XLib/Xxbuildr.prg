LPARAMETERS toObject, tcSource

LOCAL lcText1, lcText2, lcText3
TEXT TO lcText1 NOSHOW 
The VMP custom Builder program has been renamed from
  XXBUILDR.PRG
to 
  XXBUILDER.PRG (including the "E" character)
ENDTEXT  

lcText2 = SPACE(0)
IF NOT EMPTY(SYS(2019)) ;
     AND "XXBUILDR" $ UPPER(FILETOSTR(SYS(2019)))
TEXT TO lcText2 NOSHOW TEXTMERGE 
Please modify the 
  _BUILDER = ...
line in your <<SYS(2019)>> configuration file to specify XXBUILDER.PRG rather than XXBUILDR.PRG.

The following line has been placed in the clipboard so you can execute it in the Command Window to quickly make this modification:
  XXDTES('<<SYS(2019)>>','XXBUILDR')
ENDTEXT  
ENDIF

_ClipText = "XXDTES('" + SYS(2019) + "','XXBUILDR')"

IF NOT EMPTY(lcText2)
  lcText3 = lcText1 + CHR(13) + CHR(13) + lcText2
 ELSE
  lcText3 = lcText1
ENDIF

MESSAGEBOX(lcText3,48,"Please Note")
 