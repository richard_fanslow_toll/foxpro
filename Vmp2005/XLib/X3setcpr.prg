*
*  X3SETCPR.PRG
*  Take a passed [ClassName,Location] string and:
*    1- if Location is a .PRG, attempt to SET PROCEDURE TO it ADDITIVE
*    2- if Location is a .VCX, attempt to SET CLASSLIB TO it ADDITIVE
*    3- if the above fails, RETURN .NULL. (character data type)
*    4- if #1/#2 succeed or if Location is not passed, RETURN the 
*       ClassName portion of the string
*
*  This routine is included for backward compatibility
*  and is not called anywhere in the current VMP
*  framework -- use X8SETCPR.PRG instead.
*
*  Copyright (c) 1997-2005 Visionpace   All Rights Reserved
*                17501 East 40 Hwy., Suite 218
*                Independence, MO 64055
*                816-350-7900 
*                http://www.visionpace.com
*                http://vmpdiscussion.visionpace.com
*  Author:  Drew Speedie
*
*  Usage:
*    local lcClassName
*    lcClassName = x3setcpr("myClass,MYPROC.PRG")
*    IF isnull(lcClassName)
*      * something went wrong -- unable to
*      * SET PROCEDURE TO 
*     ELSE
*      =createobject(lcClassName)
*    ENDIF
*    
*    local lcClassName
*    lcClassName = x3setcpr("myClass,MYCLASS.VCX")
*    IF isnull(lcClassName)
*      * something went wrong -- unable to
*      * SET CLASSLIB TO
*     ELSE
*      =createobject(lcClassName)
*    ENDIF
*
*    local lcClassName
*    lcClassName = x3setcpr("myClass,MYCLASS")
*    IF isnull(lcClassName)
*      * something went wrong -- unable to
*      * SET CLASSLIB TO
*      * (.VCX extension is assumed)
*     ELSE
*      =createobject(lcClassName)
*    ENDIF
*
*  lParameters
*     tcClass (R) Name of a class definition plus optional
*                 comma-delimited class library file name
*                 or procedure file name.  Note that this
*                 is a single parameter and that the class
*                 location is optional.  If the class location
*                 is passed and has no extension, the extension
*                 is assumed to be ".VCX" and a SET CLASSLIB TO
*                 is attempted
* tcInAppEXEFile (O) Name of an .APP/.EXE file that contains 
*                 tcClass, maps to the IN AppFileName supported 
*                 by SET CLASSLIB TO
*                  
lParameters tcClass, tcInAppEXEFile

local lcReturn
lcReturn = space(0)
lcReturn = .NULL.

IF type("tcClass") # "C" ;
     OR isnull(tcClass) ;
     OR empty(tcClass)
  return lcReturn
ENDIF

IF occurs(",",tcClass) > 0
  local lcProcClassLib
  lcProcClasslib = upper(substrc(tcClass,at_c(",",tcClass)+1))
  lcReturn = leftc(tcClass,at_c(",",tcClass)-1)
 ELSE
  return tcClass
ENDIF
  
*
*  lcProcClasslib contains the name of the 
*  .PRG/.VCX containing lcClass/lcReturn
*
*  lcReturn contains the name of the Class to
*  be ultimately instantiated by the calling code
*
*  attempt to SET PROCEDURE TO/SET CLASSLIB TO
*  
  
IF ".PRG" $ lcProcClasslib
  IF !x3setprc(lcProcClasslib)
    lcReturn = .NULL.
  ENDIF
 ELSE
  *
  *  default to a .VCX
  *
  IF !x3setcls(lcProcClasslib,tcInAppEXEFile)
    lcReturn = .NULL.
  ENDIF
ENDIF

return lcReturn
