*
*  X3VVALID.PRG
*  Returns a .T. if 
*    the passed control is bound to a field in a table
*    -OR-
*    1- the passed control is bound to a field in a local view
*       and
*    2- any field rule on the corresponding base table returns .T.
*
* 
*  NOTE:  this routine is not designed to work with views of views
*         (of views of views... etc...)  It is intended for use
*         with controls bound to fields of:
*           Fox2x tables
*           VFP free tables
*           Local views of 
*             Fox2x tables
*             VFP free tables
*             VFP contained tables
*           Remote views of remote data
*         And even then, the only time this routine ever does 
*         anything but RETURN .T. is if it is fired in the process
*         of validating a control bound to a local view field
*         whose corresponding base table field has a rule
*         established
*  
*  Copyright (c) 1997-2005 Visionpace   All Rights Reserved
*                17501 East 40 Hwy., Suite 218
*                Independence, MO 64055
*                816-350-7900 
*                http://www.visionpace.com
*                http://vmpdiscussion.visionpace.com
*  Author:  Drew Speedie
*
*  REQUIRES:
*    X3SRCTYP.PRG
*    X3CSOURC.PRG
*    X3BTABLE.PRG
*
*  Usage:
*    *  in the Valid method
*    local llValid
*    llValid = x3vvalid(THIS)
*
*  lParameters
*     toControl (R) Object reference to the control being Valid()ated
*
*
LPARAMETERS toControl

LOCAL lcAlias, lcAliasPlusField, lcBaseTableName, llRetVal, ;
      lnSelect, lcSetDatabase

*
*  first, we'll make sure we're working with a complete
*  Alias.FieldName
*
DO CASE
  CASE NOT EMPTY(toControl.ControlSource)
    lcAliasPlusField = X3CSOURC(m.toControl) 
  CASE TYPE("toControl.icControlSourceProxy") = "C" ;
       AND NOT EMPTY(toControl.icControlSourceProxy)
    lcAliasPlusField = toControl.icControlSourceProxy
  OTHERWISE
    *
    *  neither toControl.ControlSource 
    *  nor toControl.icControlSourceProxy are set
    *  
    RETURN .t.
ENDCASE

*
*  toControl.ControlSource is populated -- is it a field 
*  from a view?
*
lnSelect        = SELECT(0)
lcAlias         = SPACE(0)
lcBaseTableName = SPACE(0)
lcSetDatabase   = SET("DATABASE")
IF EMPTY(lcAliasPlusField)
  RETURN .t.
ENDIF
lcAlias = LEFTC(lcAliasPlusField,AT_C(".",lcAliasPlusField)-1)
IF lcAliasPlusField = "THISFORM." ;
     OR lcAliasPlusField = "THIS." ;
     OR OCCURS(".",lcAliasPlusField) > 1 ;
     OR TYPE(lcAlias+".BaseClass") = "C"
  *
  *  toControl.ControlSource is an Object.Property
  *
  RETURN .t.
ENDIF

*
*  now we'll find out if we're working with a table/view/etc.
*
local lcTableOrView
lcTableOrView = x3srctyp(lcAlias)
*
*  and unless we're working with a local view, we'll return .T.
*  right away because we're only here to manually fire
*  any base table field rule for a corresponding local view field 
*
IF empty(lcTableOrView) OR lcTableOrView # "VL"
  return .t.
ENDIF

*
*  from this point on, use the corresponding base table name,
*  and remember that the view may even be in a different
*  database from the ultimate base table 
*
lcBaseTableName = x3BTable(lcAliasPlusField, ;
                           cursorgetprop("DATABASE",lcAlias))
IF empty(lcBaseTableName)
  return .t.
ENDIF
lcAliasPlusField = lcBaseTableName + ;
                   substrc(lcAliasPlusField,at_c(".",lcAliasPlusField))
*
*  see if the base table field has a rule  
*  
select (lcAlias)
***DWS commented out the following line 9/15/00 and 
***    replaced it with the next 2 IF..ENDIF constructs 
***    because particularly when toControl.ControlSource 
***    is a view of a view, etc. then the ultimate base 
***    table may well be in a different database from 
***    the view definition itself
***set database to (cursorgetprop("DATABASE",lcAlias))
*
*  this method assumes that the ultimate base table
*  is open in this data session, because opening the
*  view(s) based on lcBaseTableName automatically open
*  the base table even if not open when the view(s)
*  are opened
*
IF !used(lcBaseTableName) 
  *
  *  we're assuming here that lcBaseTableName is a base (remote) 
  *  table for a remote view -- not just that, for some 
  *  strange reason, lcBaseTableName is just a local table
  *  only open in another data session
  *
  return .t.
ENDIF
IF empty(cursorgetprop("Database",lcBaseTableName))
  *
  *  looks like lcBaseTableName is a free table
  *
  return .t.
ENDIF
set database to (cursorgetprop("Database",lcBaseTableName))
local lcRuleExpression
lcRuleExpression = space(0)
IF indbc(lcAliasPlusField,"FIELD")
  lcRuleExpression = dbgetprop(lcAliasPlusField,"FIELD","RULEEXPRESSION")
ENDIF

IF empty(lcRuleExpression)
  *
  *  no rule
  *
  llRetVal = .t.
 ELSE
  *
  *  eval() the rule to fire it manually
  *
  llRetVal = eval(lcRuleExpression)
ENDIF

select (lnSelect)

set database to &lcSetDatabase

return llRetVal
