*
*  XXModalFormEditboxFind.PRG
*  Fire the Find dialog from an editbox on a modal
*  form
*  
*  Copyright (c) 2004-2005 Visionpace   All Rights Reserved
*                17501 East 40 Hwy., Suite 218
*                Independence, MO 64055
*                816-350-7900 
*                http://www.visionpace.com
*                http://vmpdiscussion.visionpace.com
*  Author:  Drew Speedie
*
*  This routine is part of a workaround for the VFP
*  behavior where CTRL+F does not fire the Find dialog
*  for an editbox on a modal form, because the _MED_FIND
*  menu option is automatically disabled by VFP when
*  a modal form has focus.
*
*  This code is called from an ON KEY LABEL CTRL+F
*  setup in XXFW.VCX/edtBase::GotFocus()
*    XXDTES("XXFW.VCX","CTRL+F","edtBase","GotFocus")
*
*  This code yields the same Find dialog as the
*  shortcut menu Find... option from the RightClick 
*  of VMP editboxes
*    MODIFY CLASS frmFindDialog OF XXFWFRM
*    MODIFY CLASS edtBase OF XXFW METHOD RightClick
*    XXDTES("XXFWMISC.VCX","frmFindDialog","cusShortcutMenu","EditboxMenuActions")
*
LOCAL loScreenActiveForm
loScreenActiveForm = X6SAF()
IF ISNULL(loScreenActiveForm)
  RETURN
ENDIF
IF NOT TYPE("loScreenActiveForm.ActiveControl.BaseClass") = "C"
  loScreenActiveControl = .NULL.
  RETURN
ENDIF
LOCAL loControl
loControl = loScreenActiveForm.ActiveControl
loScreenActiveControl = .NULL.
IF NOT UPPER(loControl.BaseClass) = "EDITBOX"
  loControl = .NULL.
  RETURN
ENDIF
IF NOT PEMSTATUS(loControl,"AutoExpand",5)
  loControl = .NULL.
  RETURN
ENDIF
IF "\XXFWFRM.VCX" $ SET("CLASSLIB") OR X8SETCLS("XXFWFRM.VCX")
 ELSE
  *
  *  we won't be able to instantiate frmFindDialog
  *
  MessageBeep(0)
  RETURN 
ENDIF
LOCAL loForm
loForm = CREATEOBJECT("frmFindDialog",loControl)
*
*  MODIFY CLASS frmFindDialog OF XXFWFRM
*  
*  XXDTES("XXFWMISC.VCX","frmFindDialog","cusShortcutMenu","EditboxMenuActions")
*
loForm.Show()
loForm = .NULL.
RETURN
