*
*  XXCKNSIT.PRG
*  Check for SecuredItems that are not currently in the 
*  SecurityAccess table, if any are found, add the necessary 
*  records to the SecurityAccess table, and update the 
*  GeneratePK table accordingly.
*
*  PLEASE NOTE THAT THE FUNCTIONALITY IN THIS UTILITY HAS
*  BEEN REPLACED BY IMPROVED AND MORE ROBUST FUNCTIONALITY
*  IN XXFWSEC.VCX/cusNewSecuredItemsUpdate.  THIS ROUTINE
*  IS ONLY INCLUDE FOR BACKWARD COMPATIBILITY.
*    MODIFY CLASS cusNewSecuredItemsUpdate OF XXFWSEC METHOD zReadMe
*
*  For each new SecuredItems record found, add one 
*  SecurityAccess record per UserGroup, set 
*  SecurityAccess.Acc_Access to 9 (no access) for all except 
*  the user group(s) specified in the passed 
*  tcFullAccessGroupName parameter, whose 
*  SecurityAccess.Acc_Access is set to 1 (full access).
*
*  This routine is intended to be executed at the client 
*  site when you ship a new SecuredItems (free) table with 
*  new/additional secured items, to ensure that the 
*  SecurityAccess table (and the GeneratePK table) is updated
*  accordingly. 
*
*  A typical usage scenario goes something like this:
*    1- Add SecuredItems records at the development site
*    2- Update the SecuredItems table at the client site
*         Ship a new .EXE, with the SecuredItems table built 
*           in
*         -OR-
*         Ship a new SecuredItems free table (??USYSSI.DBF 
*           and ??USYSSI.CDX), overwriting the existing one
*    3- (optional) Add new UserGroups records programatically 
*       or have the System Administrator add them thru the 
*       VMP Admin|Users/Group interface
*    4- Call this routine from somewhere like one of the 
*       following code locations, oApp.CheckMainDatabase() 
*       is probably best:
*
*         *********************************************
*         CustomSetup.PRG
*         *********************************************
*         IF <Some logical condition indicating that new SecuredItems may exist>
*           local lcSA
*           lcSA = XXCKNSIT("MY.DBC","MYUSYSSI.DBF","ADMINISTRATORS")
*           IF isdigit(lcSA)
*             * reset the flag/condition evaluated in the above IF
*             * so that the XXCKNSIT() check is not performed again/
*             * each time every user enters the application
*             * ... for example, if the above IF condition is
*             *   IF file("XXCKNSIT.TXT")   &&& dummy .TXT file just serving as a flag
*             * then this code should be
*             erase XXCKNSIT.TXT
*            ELSE
*             messagebox(lcSA,16,"Please Note")
*             return .f.
*           ENDIF
*         ENDIF
*
*         *********************************************
*         oApp.SetupTables()
*         *********************************************
*         IF <Some logical condition indicating that new SecuredItems may exist>
*           local lcSA
*           set database to (THIS.icMainDatabase)
*           lcSA = XXCKNSIT(dbc(),"MYUSYSSI.DBF","ADMINISTRATORS")
*           IF isdigit(lcSA)
*             * reset the flag/condition evaluated in the above IF
*             * so that the XXCKNSIT() check is not performed again/
*             * each time every user enters the application
*             * ... for example, if the above IF condition is
*             *   IF file("XXCKNSIT.TXT")   &&& dummy .TXT file just serving as a flag
*             * then the code here should probably include 
*             erase XXCKNSIT.TXT
*            ELSE
*             THIS.ilTerminateAfterInit = .t.
*             x3msgsvc("Unable to process new SecuredItems",lcSA)
*             return .f.
*           ENDIF
*         ENDIF
*         return dodefault()
*
*         *********************************************
*         oApp.CheckMainDatabase()
*         *********************************************
*         IF <Some logical condition indicating that new SecuredItems may exist>
*           local lcSA
*           set database to (THIS.icMainDatabase)
*           lcSA = XXCKNSIT(dbc(),"MYUSYSSI.DBF","ADMINISTRATORS")
*           IF isdigit(lcSA)
*             * reset the flag/condition evaluated in the above IF
*             * so that the XXCKNSIT() check is not performed again/
*             * each time every user enters the application
*             * ... for example, if the above IF condition is
*             *   IF file("XXCKNSIT.TXT")   &&& dummy .TXT file just serving as a flag
*             * then the code here should probably include 
*             erase XXCKNSIT.TXT
*            ELSE
*             x3msgsvc("Unable to process new SecuredItems",lcSA)
*             return .f.
*           ENDIF
*         ENDIF
*         return dodefault()
*
*  WARNING!
*  For best results, this routine should be executed when 
*  there are no other users in the system, the first time 
*  someone enters the application subsequent to the 
*  installation of a a new/updated SecuredItems table.  You 
*  really don't want to have another user entering the 
*  application and hitting this routine a fraction of a 
*  second after the first user, causing this code to run a 
*  second time.  For this reason, this routine closes the 
*  database if it is already open SHARED and opens it for 
*  EXCLUSIVE use, thereby 'automatically'
*    1- guaranteeing that the current user is the only user
*       in the system at the moment
*    2- preventing other users from successfully starting
*       up the application
*  When this routine finishes, it leaves the passed 
*  tcDatabase in the condition it was in when it started:
*    - if it was not open, it is left closed
*    - if it was open EXCLUSIVE, it is left that way
*    - if it was open SHARED, it is closed here, re-opened for
*      EXCLUSIVE use during the actual updates, then closed
*      and re-opened for SHARED use on returning to the calling
*      code
*  PLEASE NOTE that because of this behavior you should 
*  typically only call this routine when either tcDatabase is 
*  not open OR when you have just the tcDatabase open but not 
*  its tables/views -- when tcDatabase is open SHARED (the 
*  typical scenario), this routine closes it and its tables/
*  views but only re-opens the database, NOT any of its 
*  tables/views that were open when this routine was called.
*
*
*  lParameters
*             tcDatabase (R) Name of the database containing the
*                              SecurityAccess (and GeneratePK and
*                              UserGroups) table.
*                            If necessary, can be passed using a fully-
*                              qualified drive+path.
*         tcSecuredItems (R) Name of the SecuredItems free table.
*                            If necessary, can be passed using a fully-
*                              qualified drive+path.
*  tcFullAccessGroupName (O) ALLTRIM(UserGroups.UGr_Group) value specifying
*                              the UserGroup for which Full Access will be
*                              initially granted to any new SecuredItems 
*                              found, by adding the new SecurityAccess records
*                              with Acc_Access = 1
*                            Defaults to "ADMINISTRATORS"
*                            Can also be passed as:
*                              "ALL!" - specifies that all SecurityAccess
*                                       records added here get Acc_Access = 1,
*                                       meaning that all groups initially get
*                                       full access to the new SecuredItems
*                               array - one-dimensional array of UserGroups.UGr_Group
*                                       values specifying any number of user
*                                       groups for which Full Access will be
*                                       initially granted to any new SecuredItems 
*                                       found, by adding the new SecurityAccess 
*                                       records with Acc_Access = 1
*                                       THIS ARRAY MUST BE PASSED BY REFERENCE:
*                                         local lcSA, laGroups[2]
*                                         laGroups[1] = "Administrators"
*                                         laGroups[2] = "Users"
*                                         lcSA = XXCKNSIT(dbc(), ;
*                                                         "MYUSYSSI.DBF", ;
*                                                         @laGroups) 
*                                         
  
lparameters tcDatabase, tcSecuredItems, tcFullAccessGroupName
external array tcFullAccessGroupName

IF vartype(tcDatabase) # "C" OR empty(tcDatabase)
  return "Invalid parameter:  Database containing SecurityAccess table" 
ENDIF

IF vartype(tcSecuredItems) # "C" OR empty(tcSecuredItems)
  return "Invalid parameter:  SecuredItems free table name" 
ENDIF

local lcFullAccessGroupName
DO CASE 
  *******************************************************
*!*	  CASE type("alen(tcFullAccessGroupName)") = "N"
  CASE NOT VARTYPE(tcFullAccessGroups) = "U" AND X2IsArray(@tcFullAccessGroupName)
  *******************************************************
    *
    *  tcFullAccessGroupName is an array of group names
    *
    lcFullAccessGroupName = .NULL.
  *******************************************************
  CASE vartype(tcFullAccessGroupName) # "C" OR empty(tcFullAccessGroupName)
  *******************************************************
    lcFullAccessGroupName = "ADMINISTRATORS"
  *******************************************************
  OTHERWISE
  *******************************************************
    lcFullAccessGroupName = upper(alltrim(tcFullAccessGroupName))
ENDCASE

*
*  several variables are declared PRIVATE in the main code 
*  so that they don't have to be passed to the CleanUp()
*  local procedure 
*

private pcSetDatabase, pcDBC, pcSE 
pcSetDatabase = set("DATABASE")
pcDBC = .NULL.
IF dbused(juststem(tcDatabase))
  set database to (juststem(tcDatabase))
  pcDBC = dbc()
  pcSE = IIF(isexclusive(juststem(tcDatabase),2),"EXCLUSIVE","SHARED")
  close database
ENDIF

local lcOnError, llError
lcOnError = on("ERROR")
llError = .f.
on error llError = .t.

open data (tcDatabase) exclusive
IF llError
  on error &lcOnError
  return "Cannot open the Database containing SecurityAccess table for EXCLUSIVE use"
ENDIF

local loUserGroups, loSecuredItems, loSecurityAccess, loGeneratePK
private plUserGroups, plSecuredItems, plSecurityAccess, plGeneratePK
store .f. to plUserGroups, plSecuredItems, plSecurityAccess, plGeneratePK
IF used("UserGroups")
  X8SETCLS("XXFWUTIL.VCX")
  loUserGroups = CREATEOBJECT("cusPushPopCursorState","UserGroups")
  plUserGroups = .t.
 ELSE
  use UserGroups again in 0 shared
ENDIF
IF llError
  on error &lcOnError
  Cleanup()
  return "Cannot open the UserGroups table"
ENDIF

IF used("SecurityAccess")
  X8SETCLS("XXFWUTIL.VCX")
  loSecurityAccess = CREATEOBJECT("cusPushPopCursorState","SecurityAccess")
  plSecurityAccess = .t.
 ELSE
  use SecurityAccess again in 0 order tag Acc_SitFK shared
ENDIF
IF llError
  on error &lcOnError
  Cleanup()
  return "Cannot open the SecurityAccess table"
ENDIF
cursorsetprop("Buffering",5,"SecurityAccess")

IF used("GeneratePK")
  X8SETCLS("XXFWUTIL.VCX")
  loGeneratePK = CREATEOBJECT("cusPushPopCursorState","GeneratePK")
  plGeneratePK = .t.
 ELSE
  use GeneratePK again in 0 shared
ENDIF
IF llError
  on error &lcOnError
  Cleanup()
  return "Cannot open the GeneratePK table"
ENDIF
cursorsetprop("Buffering",5,"GeneratePK")  &&& VMP default for GeneratePK table is 2

IF used("SecuredItems")
  X8SETCLS("XXFWUTIL.VCX")
  loSecuredItems = CREATEOBJECT("cusPushPopCursorState","SecuredItems")
  plSecuredItems = .t.
 ELSE
  use (tcSecuredItems) again in 0 alias SecuredItems shared
ENDIF
IF llError
  on error &lcOnError
  Cleanup()
  return "Cannot open the SecuredItems table"
ENDIF
private pcSecuredItems
pcSecuredItems = dbf("SecuredItems")

on error &lcOnError

local lcSuccess, lnNewSA
lcSuccess = "0"
lnNewSA = 0

select SecuredItems
SCAN
  select SecurityAccess  
  seek SecuredItems.Sit_PK 
  IF found()
    select SecuredItems
    loop
  ENDIF
  *  the SecuredItems is not in SecurityAccess -- 
  *  add one SecurityAccess per UserGroup for this SecuredItem
  select UserGroups
  SCAN for UGr_System = "N"
    select GeneratePK
    locate for upper(alltrim(GPK_PK)) = "SECURITYACCESS"
    IF !found()
      lcSuccess = "Cannot locate SecurityAccess record in GeneratePK table"
      exit
    ENDIF
    select SecurityAccess
    scatter memvar blank
    replace GeneratePK.GPK_CurrentNumber with GeneratePK.GPK_CurrentNumber + 1 in GeneratePK
    IF vartype(m.Acc_PK) = "N"
      m.Acc_PK = GeneratePK.GPK_CurrentNumber 
     ELSE
      m.Acc_PK = padl(GeneratePK.GPK_CurrentNumber,lenc(SecurityAccess.Acc_PK),"0")
    ENDIF
    m.Acc_UGrFK = UserGroups.Ugr_PK 
    m.Acc_SitFK = SecuredItems.Sit_PK 
    DO CASE
      *****************************************************
      CASE upper(alltrim(lcFullAccessGroupName)) == "ALL!"
      *****************************************************
        m.Acc_Access = 1   &&& full access
      *****************************************************
      CASE !isnull(lcFullAccessGroupName) ;
           AND upper(alltrim(UserGroups.UGr_Group)) == lcFullAccessGroupName
      *****************************************************
        m.Acc_Access = 1   &&& full access
      *****************************************************
      CASE !isnull(lcFullAccessGroupName) ;
           AND !upper(alltrim(UserGroups.UGr_Group)) == lcFullAccessGroupName
      *****************************************************
        m.Acc_Access = 9   &&& no access 
      *****************************************************
      OTHERWISE
      *****************************************************
        *
        *  tcFullAccessGroupName is an array of UGr_Group names
        *
        local xx
        FOR xx = 1 to alen(tcFullAccessGroupName,1)
          IF upper(alltrim(UserGroups.UGr_Group)) == tcFullAccessGroupName[xx]
            m.Acc_Access = 1   &&& full access
            exit
          ENDIF                  
        ENDFOR
        IF empty(m.Acc_Access)
          m.Acc_Access = 9  &&& no access
        ENDIF
    ENDCASE
    insert into SecurityAccess from memvar
    lnNewSA = lnNewSA + 1
  ENDSCAN
  select SecuredItems
ENDSCAN

IF !isdigit(lcSuccess)   
  *
  *  problem encountered above
  *
  tablerevert(.t.,"SecurityAccess")
  tablerevert(.t.,"GeneratePK")
  Cleanup()
  return lcSuccess
ENDIF

IF lnNewSA = 0
  *  no new SecuredItems records to process
  Cleanup()
  return lcSuccess
ENDIF

*
*  lnNewSA > 0 -- new SecuredItems were found, and new
*  SecurityAccess records were added
*

*
*  update the "SecuredItems" record in the GeneratePK table
*
select SecuredItems
go bottom
select GeneratePK
locate for upper(alltrim(GPK_PK)) = "SECUREDITEMS"
DO CASE
  **********************************************
  CASE !found()
  **********************************************
  **********************************************
  CASE vartype(SecuredItems.Sit_PK) = "N"
  **********************************************
    replace GeneratePK.GPK_CurrentNumber with SecuredItems.Sit_PK in GeneratePK
  **********************************************
  CASE vartype(SecuredItems.Sit_PK) = "C"
  **********************************************
    *
    *  ...not expecting to exceed 99999 SecuredItems
    *
    replace GeneratePK.GPK_CurrentNumber with int(val(SecuredItems.Sit_PK)) in GeneratePK
ENDCASE
*
*  post all the updates made here to GeneratePK and SecurityAccess
*
select SecurityAccess
BEGIN TRANSACTION
IF tableupdate(.t.,.t.,"SecurityAccess") AND tableupdate(.t.,.f.,"GeneratePK")
  END TRANSACTION
  lcSuccess = transform(lnNewSA)
 ELSE
  lcSuccess = "Unable to successfully TableUpdate() the SecurityAccess and GeneratePK tables"
  ROLLBACK
  tablerevert(.t.,"SecurityAccess")
ENDIF

Cleanup()

*
*  loUserGroups, loSecuredItems, loSecurityAccess, loGeneratePK
*  all go out of scope here, resetting/popping their condition
*  to what it was on starting this program, if in fact they
*  are open at the moment
*

return lcSuccess



PROCEDURE Cleanup
IF !plSecuredItems AND used("SecuredItems")
  use in SecuredItems
ENDIF
DO CASE
  **********************************************
  CASE isnull(pcDBC)
  **********************************************
    close database  &&& the tcDatabase passed in, opened, and current SET DATABASE TO
    IF plSecuredItems AND !used("SecuredItems") AND vartype(pcSecuredItems) = "C"
      use (pcSecuredItems) again in 0 alias SecuredItems shared    
    ENDIF
  **********************************************
  CASE pcSE = "EXCLUSIVE"
  **********************************************
    *  tcDatabase was already open EXCLUSIVE -- nothing to do
    *  with respect to the database
    IF !plSecurityAccess AND used("SecurityAccess")
      use in SecurityAccess
    ENDIF
    IF !plGeneratePK AND used("GeneratePK")
      use in GeneratePK
    ENDIF
    IF !plUserGroups AND used("UserGroups")
      use in UserGroups
    ENDIF
  **********************************************
  CASE pcSE = "SHARED"
  **********************************************
    *  tcDatabase was already open SHARED -- close it and reopen it SHARED
    close database  &&& the tcDatabase passed in, opened, and current SET DATABASE TO
    local llError, lcOnError
    lcOnError = on("ERROR")
    on error llError = .t.
    open database (pcDBC) shared
    *
    *  note that we're not doing anything if the above
    *  OPEN DATABASE command fails... 
    *
    IF plSecuredItems AND !used("SecuredItems") AND vartype(pcSecuredItems) = "C"
      use (pcSecuredItems) again in 0 alias SecuredItems shared    
    ENDIF
    on error &lcOnError
ENDCASE
set database to &pcSetDatabase
return
