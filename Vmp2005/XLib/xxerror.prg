*
*  XXERROR.PRG
*  Global error handler 
*
*  This error handler is intended to be invoked using 
*  standard ON ERROR do xxerror... syntax, but it can also 
*  be explicitly called from the Error() method of 
*  individual forms/controls/objects.
*
*  Optionally, the local procedure LogError() can be called
*  manually from anywhere in your application to log error-
*  type information.  See PROCEDURE LogError here in this 
*  .PRG and XXFW.VCX/frmData::SaveMessageOnFailure(), 
*  where LogError() is called directly.
*    XXDTES("XXFW.VCX","LogError","frmData","SaveMessageOnFailure")
*
*
*  Copyright (c) 1997-2005 Visionpace   All Rights Reserved
*                17501 East 40 Hwy., Suite 218
*                Independence, MO 64055
*                816-350-7900 
*                http://www.visionpace.com
*                http://vmpdiscussion.visionpace.com
*  Author:  Drew Speedie
*
*     
*  If you have additional specific code you want executed
*  when this routine is going to initiate a system
*  shutdown, you can do so by creating any of the following:
*    ERRHOOK0.PRG
*    ERRHOOK1.PRG
*    ERRHOOK2.PRG
*    ERRHOOK3.PRG
*  each of which, if they exist, is called from
*  here at strategic points along the way.  Your
*  ERRHOOK<n>.PRG must receive the same parameters 
*  received here (what you do with them is up to you):
*    LPARAMETERS tnErrorNumber, ;
*                tcMethodOrPRG, ;
*                tnLine, ;
*                tcSYS16, ;
*                toObject
*  Note that pcErrorDBF is a *private* memvar, and is
*  therefore available in your ERRHOOK<n>.PRGs.
*  Note that since ERRHOOK<ExpN>.PRGs are called via macro
*  substitution to prevent the Project Manager from
*  complaining if they donn't exist, if you DO create
*  ERRHOOK<n>.PRG(s), you must make sure they are added to
*  your project manually, via an EXTERNAL PROCEDURE
*  or dummy procedure like PROCEDURE PMIncludeFiles in
*  VMMAIN.PRG in the VM Example Application that ships
*  with Visual MaxFrame Professional.  Or, you can
*  create them as local procedure(s) in your main calling
*  program instead of standalone .PRGs.
*
*  NOTE that, in addition to the above ERRHOOK*.PRG
*  procedural hooks called from the main body of
*  this .PRG, the following other hooks are available:
*    ERRHOOK1a.PRG - called from the frmShowError::Init(),
*                    and passed an object reference to
*                    the frmShowError form, in case you
*                    want to customize it
*
*  NOTE that the frmShowError form class that is instantiated
*  here to display information to the user, and to allow them
*  to enter their own comments (saved to ErrorLog.Er_UsInfo)
*  contains notes in its Init regarding ways you can customize
*  the frmShowError form.
*
*
*  REQUIRES
*    ERRORLOG.DBF must be open (in the default data session)
*    and in the VFP path.
*
*
*  SEE ALSO
*    XXFW.VCX/ctrApp.InstallGlobalErrorHandler()
*
*
*  NOTE that the following ERRORLOG.DBF fields are optional
*  and are simply ignored if they don't exist.  You may, for
*  example, want to delete these five fields while your app
*  is in heavy testing by the QA team, since the ERRORLOG.FPT
*  will grow very large in a very short time.  Then, when
*  your app goes into production, add these fields back in
*  or simply delete the ERRORLOG.DBF table and let 
*  oApp.InstallGlobalErrorHandler() create it from scratch.
*      ER_lstMemo M 
*      ER_lstStat M 
*      ER_Config  M 
*      ER_Environ M 
*      ER_Forms   M 
*
*
*  USAGE
*    on error do xxerror with error(), ;
*                             program(), ;
*                             lineno(1), ;
*                             sys(16)
*
*  NOTE:  The LINENO(1) only reports a line number in an
*         .EXE-based application when/if you've built
*         the .EXE with debug information ON
*
*
*  lParameters
*    tnErrorNumber (R) Number of the error (corresponds
*                        to the nError parameter of the
*                        Error() event method)
*    tcMethodOrPRG (R) Method/Program where the error occurred
*                        (corresponds to the cMethod parameter
*                        of the Error() event method)
*           tnLine (R) Line of code on which the error occurred
*                        (corresponds to the nLine parameter
*                        of the Error() event method)
*          tcSYS16 (R) SYS(16) when the error occurred
*         toObject (O) Object reference to the THIS object when
*                        the error occurred
*
LPARAMETERS tnErrorNumber, ;
            tcMethodOrPRG, ;
            tnLine, ;
            tcSYS16, ;
            toObject
PRIVATE pcMessage
pcMessage = MESSAGE()

PRIVATE poActiveForm
DO CASE
  CASE VARTYPE(oForms) = "O"
    poActiveForm = oForms.ScreenActiveForm()
  CASE TYPE("_Screen.ActiveForm.BaseClass") = "C" 
    poActiveForm = _Screen.ActiveForm
    IF UPPER(poActiveForm.BaseClass) = "OLECONTROL"
      DO WHILE .t.
        poActiveForm = poActiveForm.PARENT
        IF UPPER(poActiveForm.BaseClass) == "FORM"
          EXIT 
        ENDIF
      ENDDO
    ENDIF
  OTHERWISE
    poActiveForm = .NULL.
ENDCASE

IF VARTYPE(gnErrorNumber) # "N"
  PUBLIC gnErrorNumber
ENDIF
gnErrorNumber = tnErrorNumber

*
*  save the current work area and have it reset
*  "automatically" when loWorkArea goes out of
*  scope, particularly in case ErrHook0() sets
*  lcAction to "LOGANDIGNORE"
*
LOCAL loWorkArea
loWorkArea = CREATEOBJECT("cusPPWorkArea")  &&& local DEFINE CLASS in this .PRG

DO CASE
  ******************************************************************
  CASE "," + TRANSFORM(gnErrorNumber) + "," $ ",1839," 
  ******************************************************************
    *
    *  SQL operation was cancelled by the user
    * 
    *  see XXRQUERY.PRG
    *
    lcAction = "NOTHING"
  ******************************************************************
  CASE "," + TRANSFORM(gnErrorNumber) + "," $ ",1709," 
  ******************************************************************
    *
    *  error when attempting to access a .DBC database object 
    *  at the same moment as another user (usually due to 
    *  attempting to open a view, in which case this error
    *  is already trapped when opening the view via oLib.oDBCSvc.OpenTable)
    *
    IF NOT X3MSGSVC("Server busy, keep trying?") = "N"
      retry                 &&& try again
      lcAction = "NOTHING"                  
    ENDIF
  ******************************************************************
  CASE "," + TRANSFORM(gnErrorNumber) + "," $ ",125," 
  ******************************************************************
    IF X3MSGSVC("Printer not ready") = "R"
      RETRY 
    ENDIF
    lcAction = "NOTHING"                  
  ******************************************************************
  CASE "," + TRANSFORM(gnErrorNumber) + "," $ ",1958,"
  ******************************************************************
    *
    *  don't display a second message that 
    *  there are no printers installed -- the
    *  Windows operating system has already done that
    *
    lcAction = "NOTHING"
  ******************************************************************
  CASE "," + TRANSFORM(gnErrorNumber) + "," $ ;
       ",3,108,109,110,130,1502,1503,1705,1708,1101,1106,1637,"
  ******************************************************************
    *
    *  multi-user errors
    *
    lcAction = "NOTHING"
  ******************************************************************
  CASE (VAL(SYS(1001))-VAL(SYS(1016))) < 10000 ;
       OR "," + TRANSFORM(gnErrorNumber) + "," $ ;
       ",6,56,1410,43,1809,1149,1150,1151,1600," 
  ******************************************************************
    *
    *  low memory or not enough file handles, etc.,
    *  so we're not liable to be able to do anything more
    *  than attempt to write to the error log and 
    *  get the heck out of here
    *
    lcAction = "EMERGENCY"
  ******************************************************************
  OTHERWISE
  ******************************************************************
    *
    *  all other errors
    *
    lcAction = "SHUTDOWN"
ENDCASE

*
*  hook to call your additional code:
*
LOCAL lcErrHook
lcErrHook = "ErrHook0"
*
*  make lcAction available to ErrHook0.PRG by copying
*  it to a private pcAction variable that you can
*  check/access in ErrHook0.PRG
*
PRIVATE pcAction
pcAction = lcAction
IF FILE(lcErrHook+".PRG") OR FILE(lcErrHook+".FXP")
  *
  *  note that you can update lcAction from ErrHook0.PRG,
  *  and that one additional lcAction value supported
  *  here is "LOGandIGNORE", which is not shown above
  *  in the DO CASE..ENDCASE, because it must be set in
  *  ErrHook0.PRG, if you want that option
  *
  lcAction = EVALUATE(lcErrHook + ;
             "(tnErrorNumber,tcMethodOrPRG,tnLine,tcSYS16,toObject)")
  IF VARTYPE(lcAction) = "C"
    lcAction = UPPER(ALLTRIM(lcAction))
   ELSE
    *
    *  if you use the RETRY command in your ErrHook0.PRG,
    *  lcAction is set to a logical value, which causes
    *  a data type mismatch error further on
    *
    lcAction = "NOTHING"
  ENDIF
ENDIF
*
*  keep pcAction in synch with lcAction in case you
*  need to check pcAction (lcAction) in ErrHook1.PRG 
*  further on
*
pcAction = lcAction 
*
*  it's possible under some circumstances for a RETRY 
*  command in your ErrHook0.PRG to not work correctly,
*  so if your ErrHook0() returns the string "RETRY",
*  the RETRY command is executed here
*
IF UPPER(lcAction) = "RETRY"
  RETRY
ENDIF

IF lcAction = "NOTHING"
  *
  *  multi-user errors don't cause a system shutdown,
  *  but gnErrorNumber now contains the error number
  *  and can be queried like this:
  *
  *    select SOMETABLE
  *    gnErrorNumber = 0
  *    append blank
  *    IF gnErrorNumber > 0
  *      *  the APPEND BLANK failed
  *     ELSE
  *      *  whatever
  *    ENDIF
  *
  RETURN
ENDIF  

*
*  lcAction = one of these:
*    "SHUTDOWN"
*    "EMERGENCY"
*    UPPER("LOGandIGNORE")
*
IF INLIST(lcAction,"SHUTDOWN","EMERGENCY")
  
  gnErrorNumber = -9999   &&& various objects abort normal Destroy() procedures when this is the case

  _Screen.MousePointer = 0

  DO CASE
    CASE vartype(pcFatalErrorMessage1) = "C"
      *
      *  this is what we expect
      *
      wait window pcFatalErrorMessage1 nowait
    CASE vartype(_Screen.oINTL) = "O" OR vartype(_Screen.oMsgSvc) = "O" 
      wait window x3msgsvc("Unrecoverable error, being logged") nowait
    OTHERWISE 
      wait window "An unrecoverable system error has occurred.  " + chr(13) + ;
                  "Please wait while this error is logged..." nowait
  ENDCASE
 ELSE
  *
  *  lcAction = upper("LOGandIGNORE")
  *
ENDIF

*
*  hook to call your additional code, which may need
*  to check for pcAction = upper("LOGandIGNORE")
*
*  don't forget that lcAction is available via the private 
*  pcAction memvar which contains the save value as lcAction,
*  but is available/visible in ErrHook1.PRG because
*  it's declared PRIVATE here -- note that we don't
*  check the value of pcAction anywhere from here on,
*  so setting it to a different value in ErrHook1.PRG
*  has no effect here, but you can, of course, check
*  pcAction in any subsequent ErrHook<ExpN>.PRGs of yours
*
IF file("ERRHOOK1.PRG") OR file("ERRHOOK1.FXP")
  lcErrHook = "ErrHook1"
  do (lcErrHook) with tnErrorNumber, ;
                     tcMethodOrPRG, ;
                     tnLine, ;
                     tcSYS16, ;
                     toObject
ENDIF

IF INLIST(lcAction,"SHUTDOWN","EMERGENCY")
  *
  *  we're going to hit the road, so whether lcAction is
  *  "EMERGENCY" or "SHUTDOWN", let's make sure no manual
  *  record locks will be leftover, which can leave them
  *  locked in the OS after the VFP/VMP app QUITs
  *
  unlock all
ENDIF

IF lcAction = "EMERGENCY"
  *
  *  Log just the bare essentials.  This was the least
  *  memory/disk-intensive way I could think of to do
  *  this.  Creating a .TXT file and other options 
  *  probably require more resources than simply 
  *  attempting to write a record to ERRORLOG, which
  *  should already be open at this point
  *
  local lcUserPK
  lcUserPK = GetUserPK()
  set datasession to 1
  insert into ERRORLOG (er_dattime, ;
                        er_user, ;
                        er_error, ;
                        er_methprg, ;
                        er_lineno, ;
                        er_program, ;
                        er_code) ;
                values (datetime(), ;
                        lcUserPK, ;
                        TRANSFORM(tnErrorNumber), ;
                        tcMethodOrPRG, ;
                        TRANSFORM(tnLine), ;
                        tcSYS16,; 
                        "(unlogged)")
  DO CASE
    CASE vartype(pcFatalErrorMessage2) = "C"
      *
      *  this is what we expect
      *
      wait window pcFatalErrorMessage2 to junk timeout 300 
    CASE vartype(_Screen.oINTL) = "O" OR vartype(_Screen.oMsgSvc) = "O" 
      wait window x3msgsvc("Unrecoverable error, press any key") nowait
    OTHERWISE 
      wait window "An unrecoverable system error has occurred.  " + chr(13) + ;
                  "Please report this problem.  " + chr(13) + ;
                  "Press any key to terminate this program..." ;
                  to junk timeout 300
  ENDCASE
  on shutdown
  _Screen.Visible = .t.
  quit
ENDIF

*
*  lcAction = "SHUTDOWN" or UPPER("LOGandIGNORE")
*
IF (FILE("X7ISAPIF.PRG") OR FILE("X7ISAPIF.PRG")) ;
     AND X7ISAPIF("MessageBeep")
  MessageBeep(0)
 ELSE
  ?? CHR(7)
ENDIF

*
*  attempt to log the error
*
private pcErrorDBF  
pcErrorDBF = space(0)  &&& pcErrorDBF gets set in PROCEDURE LogError
do LogError with tnErrorNumber, ;
                 tcMethodOrPRG, ;
                 tnLine, ;
                 tcSYS16, ;
                 toObject, ;
                 lcAction == UPPER("LOGandIGNORE")

*
*  hook to call your additional code, which may need
*  to check for pcAction = upper("LOGandIGNORE")
*
IF file("ERRHOOK2.PRG") OR file("ERRHOOK2.FXP")
  lcErrHook = "ErrHook2"
  do (lcErrHook) with tnErrorNumber, ;
                     tcMethodOrPRG, ;
                     tnLine, ;
                     tcSYS16, ;
                     toObject
ENDIF

IF lcAction = UPPER("LOGandIGNORE")
  *
   *  if a USE AGAIN was done on ERRORLOG.DBF, with the 
   *  alias "XXERROR", close the XXERROR alias  
  *
  USE IN SELECT("XXERROR")
  RETURN
ENDIF

*
*  at this point, lcAction can only be "SHUTDOWN"
*

*
*  prevent any AlwaysOnTop forms from getting in the way
*
IF VARTYPE(poActiveForm) = "O"
  LOCAL xx
  FOR xx = 1 to _Screen.FormCount
    IF TYPE("_Screen.Forms[xx].AlwaysOnTop") = "L" ;
         AND _Screen.Forms[xx].AlwaysOnTop = .t. 
      _Screen.Forms[xx].AlwaysOnTop = .f.
    ENDIF
  ENDFOR &&& loop thru all forms
ENDIF

DO ShowError
SELECT (pcErrorDBF)
IF EMPTY(Er_UsInfo) AND inlist(CURSORGETPROP("SourceType",pcErrorDBF),2,102,202)
  replace Er_UsInfo WITH SPACE(1)
ENDIF

*
*  now that we've done things in an orderly fashion,
*  update the flag in ERRORLOG that tells us if the
*  user re-booted (or something else bad happened)
*  on the way out -- if ERRORLOG.er_reboot = .t.,
*  it means we never made it this far)
*             
replace er_reboot with .f. in (pcErrorDBF)
LOCAL lnSelect
lnSelect = SELECT(0)
SELECT (pcErrorDBF)
*
*  hook to call your additional code (note that
*  pcErrorDBF is still open, is the current work
*  area, and has its record pointer positioned on the
*  just-added record)
* 
*
IF file("ERRHOOK3.PRG") OR file("ERRHOOK3.FXP")
  lcErrHook = "ErrHook3"
  do (lcErrHook) with tnErrorNumber, ;
                     tcMethodOrPRG, ;
                     tnLine, ;
                     tcSYS16, ;
                     toObject
ENDIF

SELECT (lnSelect)
IF CURSORGETPROP("Buffering",pcErrorDBF) > 1
  TABLEUPDATE(.t.,.t.,pcErrorDBF)
ENDIF
USE IN (pcErrorDBF)

*
*  issuing the QUIT command fires whatever is the
*  current ON SHUTDOWN, so we have to clear that out
*  first
*
ON SHUTDOWN 

*
*  if the current form is modal, we need to explicitly
*  release it here to be absolutely sure it doesn't
*  interfere with the shutdown process
*
IF VARTYPE(poActiveForm) = "O" ;
     AND ( (VARTYPE(poActiveForm.WindowType) = "N" AND poActiveForm.WindowType = 1) ;
          OR ;
           (VARTYPE(poActiveForm.icSubclassingPotential) # "U" AND poActiveForm.GetPProp("WindowType") = 1) )
  poActiveForm.Release()
ENDIF

*
*  hit the road
*
local llQuit
IF vartype(oApp) = "O" AND oApp.InDevelopment()
  _Screen.Visible = .t.   &&& in case we're in VFP5
  IF messagebox("You can either CANCEL or QUIT now.  (CANCEL returns you to the Command Window)" + chr(13) + chr(13) + ;
                "Select <Cancel> to CANCEL, <OK> to QUIT", ;
                1+48+256, ;
                "CANCEL or QUIT") = 1
    llQuit = .t.
   ELSE
    llQuit = .f.
  ENDIF                
 ELSE
  *
  *  oApp doesn't exist or oApp.InProduction()
  *
  llQuit = .t.
ENDIF

IF llQuit
  LOCAL loForm
  FOR EACH loForm IN _Screen.Forms  
    IF TYPE("loForm.BaseClass") = "C" ;   &&& VFP Task list protects most properties, including BaseClass, so we can't use VARTYPE()
         AND upper(loForm.BaseClass) = "TOOLBAR" ;
         AND upper(loForm.Class) = "TBRDEVTOOLS"
      loForm.Release()
      exit 
    ENDIF       
  ENDFOR
  gnErrorNumber = -9998   &&& -9998 means "QUIT" -- see XXFWMAIN.PRG 
ENDIF
clear events
return to master    &&& helps when the error occurs in the Init() of a form (or methods called from the Init)
return            
      
      
      

PROCEDURE LogError
*
*  log information about the environment at the time of the error
*
*  lParameters
*  tnErrorNumber (R) See the initial XXERROR.PRG parameters
*  tcMethodOrPRG (R) See the initial XXERROR.PRG parameters
*         tnLine (R) See the initial XXERROR.PRG parameters
*        tcSYS16 (R) See the initial XXERROR.PRG parameters
*       toObject (O) See the initial XXERROR.PRG parameters
*   tlNoChangeDS (O) Don't SET DATASESSION TO the toForm.DataSessionID,
*                      in which case data-related information will
*                      not be logged
*                    Pass this parameter as .T. when calling the
*                      LogError() function manually from a (private
*                      data session) form which has bound controls,
*                      since issuing SET DATASESSION TO in such a 
*                      situation wreaks all kinds of havoc
*                    See XXFW.VCX/frmData::SaveLogFailure()
*                      where we've called LogError() manually
*                      XXDTES("XXFW.VCX","SaveLogFailure","frmdata","SAVELOGFAILURE")
*
LPARAMETERS tnErrorNumber, ;
            tcMethodOrPRG, ;
            tnLine, ;
            tcSYS16, ;
            toObject, ;
            tlNoChangeDS

*
*  grab LIST STATUS before we select ErrorLog.DBF
*
LOCAL lcListStatusFile
lcListStatusFile = ADDBS(SYS(2023)) + SYS(2015) + ".TXT"
LIST STATUS TO FILE (lcListStatusFile) NOCONSOLE 

LOCAL ARRAY laError[1]
AERROR(laError)
*
*  set up some stuff 
*
LOCAL lcTextFile, lcSaveDBC, lcSaveAlias, xx
lcSaveDBC = SET("DATABASE")
lcSaveAlias = ALIAS()
lcTextFile = ADDBS(SYS(2023)) + SYS(2015) + ".TXT"

*
*  this is a little tricky.  We can't just SELECT ERRORLOG
*  because we're not likely to be in the default data 
*  session, in which case it's not USED() in the current
*  data session
* 
pcErrorDBF = "ERRORLOG"
IF NOT USED(pcErrorDBF)
  LOCAL lcDBC, laDBC[1], llJunk
  IF VARTYPE(oApp) = "O"
    pcErrorDBF = oApp.GetAppInfo("ErrorLogDirectory")
    IF NOT VARTYPE(pcErrorDBF) = "C" OR EMPTY(pcErrorDBF) 
      *
      *  no "ErrorLogDirectory" in APPCONFIG/APPINFO --
      *  we can only expect ERRORLOG.DBF is in the path
      *
      pcErrorDBF = "ERRORLOG"
     ELSE
      pcErrorDBF = ADDBS(pcErrorDBF) + "ERRORLOG"
    ENDIF
  ENDIF
  IF FILE(pcErrorDBF+".DBF")
    TRY
    USE (pcErrorDBF) IN 0 AGAIN ALIAS XXERROR SHARED
    CATCH
    ENDTRY
  ENDIF
  IF NOT USED("XXERROR")
    *
    *  this code is primarily to support an ErrorLog
    *  that is a remote view, which we really do not
    *  recommend, since one reason for the error 
    *  condition could easily be that the connection
    *  to the remote database has gone bad
    *
    IF VARTYPE(oApp) = "O"
      lcDBC = oApp.GetPProp("icMainDatabase")
      IF VARTYPE(lcDBC) = "C" ;
           AND NOT EMPTY(lcDBC) ;
           AND NOT lcDBC = "!NONE!" 
        SET DATABASE TO (lcDBC)
        IF INDBC("ErrorLog","VIEW")
          TRY
          USE ErrorLog IN 0 ALIAS XXERROR SHARED NODATA
          CATCH
          ENDTRY
        ENDIF
      ENDIF           
    ENDIF
    IF NOT USED("XXERROR")
      *  look in all open databases for an ErrorLog view
      IF ADATABASES(laDBC) > 0
        FOR xx = 1 TO ALEN(laDBC,1)
          SET DATABASE TO JUSTSTEM(laDBC[xx])
          IF INDBC("ErrorLog","VIEW")
            TRY
            USE ErrorLog IN 0 ALIAS XXERROR SHARED NODATA
            CATCH
            ENDTRY
            EXIT 
          ENDIF
        ENDFOR
      ENDIF
    ENDIF
  ENDIF      
  pcErrorDBF = "XXERROR"
ENDIF
IF NOT USED(pcErrorDBF)
  * 
  *  we won't be able to log this error
  *
  RETURN 
ENDIF

*
*  log the initial error stuff
*
SELECT (pcErrorDBF)
APPEND BLANK 
replace er_dattime WITH DATETIME() 
replace er_user    WITH GetUserPK()
LOCAL lcFieldName
lcFieldName = pcErrorDBF + ".Er_UsrName"
IF TYPE(lcFieldName) = "C"
  *  this new Er_UsrName field was added 1/22/01,
  *  so we test for it here so production code using
  *  an ErrorLog.DBF created before that won't crash
  replace Er_UsrName WITH GetUserName()
ENDIF
replace er_error   with transform(laError[1])
replace er_reboot  with .t.
replace er_methprg with rightc(tcMethodOrPRG,len(er_methprg))
replace er_lineno  with transform(tnLine)
replace er_code    with message(1)
IF vartype(pcMessage) = "C" AND !isnull(pcMessage) AND !empty(pcMessage) 
  replace er_message with pcMessage  &&& laError[2]
 ELSE  
  *  for an example, see call here in XXFW.VCX/frmData::SaveMessageOnFailure()
  replace er_message with transform(tcSYS16)
ENDIF

private pcCRLF 
pcCRLF = chr(13) + chr(10)

*
*  log the AERROR() contents
*
IF type("laError[1]") = "U"
  replace er_aerror with "AERROR() contents unavailable"
 ELSE
  FOR xx = 1 to alen(laError,1)
    replace er_aerror with "AERROR() contents" + pcCRLF
    replace er_aerror with er_aerror + transform(laError[xx,1]) + pcCRLF
    IF type("laError[2]") # "U"
      replace er_aerror with er_aerror + transform(laError[xx,2]) + pcCRLF
    ENDIF  
    IF type("laError[3]") # "U"
      replace er_aerror with er_aerror + transform(laError[xx,3]) + pcCRLF
    ENDIF
    IF type("laError[4]") # "U"
      replace er_aerror with er_aerror + transform(laError[xx,4]) + pcCRLF
    ENDIF
    IF type("laError[5]") # "U"
      replace er_aerror with er_aerror + transform(laError[xx,5]) + pcCRLF
    ENDIF
    IF type("laError[6]") # "U"
      replace er_aerror with er_aerror + transform(laError[xx,6]) + pcCRLF
    ENDIF
    IF type("laError[7]") # "U"
      replace er_aerror with er_aerror + transform(laError[xx,7]) + pcCRLF
    ENDIF
  ENDFOR
ENDIF
*
*  log the program execution path
*
local yy
xx = 1
yy = 1
DO WHILE !empty(sys(16,xx)) 
  IF sys(16,xx) # "ON." .and. !"XXERROR"$sys(16,xx)
    replace er_program with er_program + sys(16,xx) + pcCRLF in (pcErrorDBF)
  ENDIF
  xx = xx + 1
ENDDO 
replace er_program with er_program + "(" + tcMethodOrPrg + ")"
release xx
release yy

*
*  log memory if the Er_LstMemo field exists
*
IF type(pcErrorDBF+".Er_LstMemo") = "M"
  list memory to file (lcTextFile) noconsole
  select (pcErrorDBF)
  append memo er_lstmemo from (lcTextFile)
  erase (lcTextFile)
ENDIF

*
*  log status if the Er_LstStat field exists
*
IF type(pcErrorDBF+".Er_LstStat") = "M"
  *
  *  lcListStatusFile creation has been moved to the
  *  beginning of this LogError procedure, before 
  *  ErrorLog is made the current work area, so that
  *  the LIST STATUS listing shows the correct current
  *  work area
  *
*!*	  list status to file (lcTextFile) noconsole
*!*	  select (pcErrorDBF)
*!*	  append memo er_lststat from (lcTextFile)
*!*	  erase (lcTextFile)
  SELECT (pcErrorDBF)
  APPEND MEMO ER_LstStat FROM (lcListStatusFile)
ENDIF
ERASE (lcListStatusFile)

*
*  log any CONFIG.FPW in effect if Er_Config field exists
*
IF TYPE(m.pcErrorDBF+".Er_Config") = "M"
  SELECT (m.pcErrorDBF)
  IF NOT EMPTY(SYS(2019)) AND FILE(SYS(2019))
    APPEND MEMO Er_Config FROM (SYS(2019))
    replace Er_Config WITH SYS(2019) + m.pcCRLF + Er_Config
   ELSE
    replace Er_Config WITH "no CONFIG.FPW file" + ;
                           m.pcCRLF + ;
                           "(note that VFP doesn't report a SYS(2019) config file in a runtime executable)"
  ENDIF
ENDIF

*
*  log other stuff in the environment if the Er_Environ field exists
*
IF type(pcErrorDBF+".Er_Environ") = "M"
  replace er_environ with              "             Disk remaining: " + TRANSFORM(diskspace()) + pcCRLF in (pcErrorDBF)
  replace er_environ with er_environ + "                  Disk size: " + sys(2020) + pcCRLF in (pcErrorDBF)
  replace er_environ with er_environ + "Available memory below 640K: " + sys(12) + pcCRLF in (pcErrorDBF)
  replace er_environ with er_environ + "           Available memory: " + sys(1001) + pcCRLF in (pcErrorDBF)  
  replace er_environ with er_environ + "              Memory in use: " + sys(1016) + pcCRLF in (pcErrorDBF)
  replace er_environ with er_environ + "                        EMS: " + sys(23) + pcCRLF in (pcErrorDBF)
  replace er_environ with er_environ + "                  EMS limit: " + sys(24) + pcCRLF in (pcErrorDBF)
  replace er_environ with er_environ + "   Foreground memory buffer: " + sys(3050,1) + pcCRLF in (pcErrorDBF)
  replace er_environ with er_environ + "   Background memory buffer: " + sys(3050,2) + pcCRLF in (pcErrorDBF)
  replace er_environ with er_environ + "          Current directory: " + FULLPATH(CURDIR()) + pcCRLF in (pcErrorDBF)
  replace er_environ with er_environ + "           FoxPro start dir: " + home() + pcCRLF in (pcErrorDBF)
  replace er_environ with er_environ + "              Resource file: " + sys(2005) + pcCRLF in (pcErrorDBF)
  replace er_environ with er_environ + "       Temporary file drive: " + sys(2023) + pcCRLF  in (pcErrorDBF)
  replace er_environ with er_environ + "              Graphics card: " + sys(2006) + pcCRLF in (pcErrorDBF)
  replace er_environ with er_environ + "        Files in Config.SYS: " + sys(2010) + pcCRLF in (pcErrorDBF)
  replace er_environ with er_environ + "                    Printer: " + sys(6) + pcCRLF in (pcErrorDBF)
  replace er_environ with er_environ + "             Printer status: " + sys(13) + pcCRLF in (pcErrorDBF)
  replace er_environ with er_environ + "                    Version: " + version(1) + pcCRLF in (pcErrorDBF)
  replace er_environ with er_environ + "                    Machine: " + sys(0) + pcCRLF in (pcErrorDBF)
  replace er_environ with er_environ + "           Operating System: " + os(1) + pcCRLF in (pcErrorDBF)
  replace er_environ with er_environ + "Underlying Operating System: " + os() + pcCRLF in (pcErrorDBF)
  replace er_environ with er_environ + "                  Processor: " + sys(17) + pcCRLF in (pcErrorDBF)
ENDIF
IF UPPER(JUSTEXT(SYS(16,0))) == "EXE" AND FILE(SYS(16,0))
  LOCAL laGFV[1], xx
  IF AGETFILEVERSION(laGFV,SYS(16,0)) > 0
    replace ER_Environ WITH ER_Environ + pcCRLF + pcCRLF + "AGetFileVersion():" + pcCRLF IN (pcErrorDBF)
    FOR xx = 1 TO ALEN(laGFV,1)
      replace ER_Environ WITH ER_Environ + PADL(TRANSFORM(xx),2) + " - " + TRANSFORM(laGFV[xx]) + pcCRLF in (pcErrorDBF)
    ENDFOR
    replace ER_Environ WITH ER_Environ + pcCRLF IN (pcErrorDBF)
  ENDIF
  RELEASE laGFV, xx
ENDIF

*
*  log the current active form if Er_Forms field exists
*
IF type(pcErrorDBF+".Er_Forms") = "M"
  IF VARTYPE(poActiveForm) = "O"
    replace er_forms with "CURRENT ACTIVEFORM" + pcCRLF in (pcErrorDBF)
    do er_forms_loop with poActiveForm, tlNoChangeDS
   ELSE
    replace er_forms with "no current active forms -- Default Data Session:" + pcCRLF in (pcErrorDBF)
    do er_forms_loop with _Screen, tlNoChangeDS
  ENDIF
  *
  *  log each extant form  
  *
  select (pcErrorDBF)
  IF _Screen.FormCount > 0
    replace er_forms with er_forms + "OTHER EXTANT FORMS" + pcCRLF in (pcErrorDBF)
    local xx
    FOR xx = 1 to _Screen.FormCount
      DO CASE
        CASE TYPE("_Screen.Forms[xx].WindowType") = "N" AND _Screen.Forms[xx].WindowType = 1
          * _Screen.ActiveForm, modal form, already been logged above
        CASE TYPE("poActiveForm") = "O" AND _Screen.Forms[xx] = poActiveForm
          *  the current active form has already been logged
        OTHERWISE
          do er_forms_loop with _Screen.Forms[xx], tlNoChangeDS
      ENDCASE
    ENDFOR
   ELSE
    replace er_forms with er_forms + "No extant forms" in (pcErrorDBF)
  ENDIF
ENDIF
IF EMPTY(Er_UsInfo) AND inlist(CURSORGETPROP("SourceType",pcErrorDBF),2,102,202)
  replace Er_UsInfo WITH SPACE(1)
ENDIF
IF CURSORGETPROP("Buffering",pcErrorDBF) > 1
  FOR xx = 1 TO 5
    IF TABLEUPDATE(.t.,.t.,pcErrorDBF)
      EXIT 
    ENDIF
    nothing = INKEY(.5)
  ENDFOR
ENDIF

IF NOT EMPTY(lcSaveAlias)
  SELECT (lcSaveAlias)
ENDIF
IF NOT EMPTY(lcSaveDBC)
  SET DATABASE TO (lcSaveDBC)
ENDIF

IF PROGRAM(PROGRAM(-1)-1) == "XXERROR"
  *
  *  procedure LogError called 'normally' from XXERROR
  *
 ELSE
  *
  *  procedure LogError called directly, from somewhere
  *  else like XXFW.VCX/frmData::SaveMessageOnFailure(),
  *  AND the code above found !USED(pcErrorDBF) 
  *  (pcErrorDBF = "ERRORLOG") -- ErrorLog is kept open
  *  in the Default data session, but most frmData
  *  forms are in a Private data session
  *
  IF pcErrorDBF == "XXERROR"
    USE IN (pcErrorDBF)
  ENDIF
ENDIF

*!*	*
*!*	*  if a USE AGAIN was done on ERRORLOG.DBF, with the alias
*!*	*  "XXERROR", close the XXERROR alias
*!*	*
*!*	USE IN SELECT("XXERROR")
RETURN



PROCEDURE ShowError
*
*  Present the user with a dialog describing the
*  error, etc.
*
IF (FILE("X7ISAPIF.PRG") OR FILE("X7ISAPIF.PRG")) ;
     AND X7ISAPIF("MessageBeep")
  MessageBeep(0)
 ELSE
  ?? CHR(7)
ENDIF
SELECT (pcErrorDBF)
LOCAL loForm
loForm = CREATEOBJECT("frmShowError")
loForm.Show()
RETURN 



PROCEDURE er_forms_loop
*
*  gather and store ERRORLOG.Er_Forms data for the passed form
*
*  lParameters
*        toForm (R) Object reference to the form for which the
*                     information is to be logged
*  tlNoChangeDS (O) Don't SET DATASESSION TO the toForm.DataSessionID,
*                     in which case data-related information will
*                     not be logged
*                   Pass this parameter as .T. when calling the
*                     LogError() function manually from a (private
*                     data session) form which has bound controls,
*                     since issuing SET DATASESSION TO in such a 
*                     situation wreaks all kinds of havoc
*                   See XXFW.VCX/frmData::SaveMessageOnFailure(), 
*                     where we've called LogError() manually
*                  
lParameters toForm, tlNoChangeDS
local lnDataSession, lnOffset
lnDataSession = set("DATASESSION")
lnOffset = 2
replace Er_Forms WITH Er_Forms + SPACE(lnOffset) + REPLICATE("=",48) + pcCRLF IN (pcErrorDBF)
replace er_forms with er_forms + space(lnOffset) + "             Name  " + toForm.Name + pcCRLF in (pcErrorDBF)
replace Er_Forms WITH Er_Forms + SPACE(lnOffset) + REPLICATE("=",48) + pcCRLF IN (pcErrorDBF)
LOCAL lcSCX
*!*	lcSCX = SYS(1271,toForm)
IF NOT X2PSTACK(toForm.Name+".Load ")
  *  the following SYS(1271) crashes until Form.Init(),
  *  with a "Not enough memory..." error
  lcSCX = SYS(1271,toForm)
ENDIF
IF vartype(lcSCX) = "L"
  lcSCX = "(.VCX-/.PRG-based form, or .SCX-based form during Load)"
ENDIF
replace er_forms with er_forms + space(lnOffset) + "        .SCX file  " + lcSCX + pcCRLF in (pcErrorDBF)
replace er_forms with er_forms + space(lnOffset) + "          Caption  " + toForm.Caption + pcCRLF in (pcErrorDBF)
replace er_forms with er_forms + space(lnOffset) + "            Class  " + toForm.Class + pcCRLF in (pcErrorDBF)
replace er_forms with er_forms + space(lnOffset) + "     ClassLibrary  " + toForm.ClassLibrary + pcCRLF in (pcErrorDBF)
replace er_forms with er_forms + space(lnOffset) + "      DataSession  " + IIF(TYPE("toForm.DataSession")="N",IIF(toForm.DataSession=1,"Default","Private"),"unable to determine") + pcCRLF in (pcErrorDBF)
replace er_forms with er_forms + space(lnOffset) + "    DataSessionID  " + TRANSFORM(toForm.DataSessionID) + pcCRLF in (pcErrorDBF)
IF vartype(tlNoChangeDS)="L" AND !isnull(tlNoChangeDS) AND tlNoChangeDS
  replace er_forms with er_forms + space(lnOffset) + "data session information is not available..." + pcCRLF
  return
ENDIF

set datasession to toForm.DataSessionID
local lcString
lcString = space(0)
lcString = lcString + space(lnOffset) + "Transaction level  " + TRANSFORM(TXNLEVEL()) + pcCRLF
lcString = lcString + space(lnOffset) + "  SET DATABASE TO  " + IIF(empty(set("DATABASE")),"none",set("DATABASE")) + pcCRLF 
lcString = lcString + space(lnOffset) + "    Current DBC()  " + IIF(empty(set("DATABASE")),"none",dbc()) + pcCRLF
lcString = lcString + space(lnOffset) + "       WORK AREAS  " + pcCRLF 
local array laCursors[1]
local lnCursors, lnRelations
lnCursors = aused(laCursors)
IF lnCursors > 0
  local lcAlias, xx, yy
  FOR xx = 1 to lnCursors
    lcAlias = laCursors[xx,1]
    select (lcAlias)
    lcString = lcString + SPACE(lnOffset*6) + REPLICATE("-",30) + pcCRLF
    lcString = lcString + space(lnOffset*6) + lcAlias + pcCRLF
    lcString = lcString + SPACE(lnOffset*6) + REPLICATE("-",30) + pcCRLF
    FOR lnRelations = 1 TO 100
      IF EMPTY(TARGET(m.lnRelations,m.lcAlias))
        EXIT 
      ENDIF
      lcString = m.lcString + SPACE(lnOffset*6) + m.lcAlias + " is the source of a relation into " + TARGET(m.lnRelations,m.lcAlias) + " on " + RELATION(m.lnRelations,m.lcAlias) + pcCRLF
    ENDFOR
    FOR yy = 1 TO ALEN(laCursors,1)
      FOR lnRelations = 1 TO 100
        IF EMPTY(TARGET(m.lnRelations,laCursors[m.yy,1]))
          EXIT
        ENDIF
        IF TARGET(m.lnRelations,laCursors[m.yy]) == m.lcAlias
          lcString = m.lcString + SPACE(lnOffset*6) + m.lcAlias + " is the target of a relation from " + laCursors[m.yy,1] + " on " + RELATION(m.lnRelations,laCursors[m.yy,1]) + pcCRLF
        ENDIF
      ENDFOR
    ENDFOR
    lcString = lcString + space(lnOffset*4) + "             DBF()  " + dbf(lcAlias) + pcCRLF
    lcString = lcString + space(lnOffset*4) + "           RECNO()  " + IIF(eof(lcAlias),"eof()",TRANSFORM(RECNO(lcAlias))) + pcCRLF
    lcString = lcString + space(lnOffset*4) + "             EOF()  " + IIF(eof(lcAlias),"yes","no") + pcCRLF
    lcString = lcString + space(lnOffset*4) + "           ORDER()  " + IIF(empty(order(lcAlias)),"none",order(lcAlias)) + pcCRLF
    lcString = lcString + space(lnOffset*4) + "          FILTER()  " + IIF(empty(filter(lcAlias)),"none",filter(lcAlias)) + pcCRLF
    lcString = lcString + space(lnOffset*4) + "          DATABASE  " + IIF(empty(cursorgetprop("DATABASE",lcAlias)),"free table",cursorgetprop("DATABASE",lcAlias)) + pcCRLF
    lcString = lcString + space(lnOffset*4) + "        SourceType  " + TRANSFORM(cursorgetprop("SourceType",lcAlias)) + pcCRLF
    lcString = lcString + space(lnOffset*4) + "        SourceName  " + cursorgetprop("SourceName",lcAlias) + pcCRLF
    lcString = lcString + space(lnOffset*4) + "        BufferMode  " + TRANSFORM(cursorgetprop("BUFFERING",lcAlias)) + pcCRLF
*!*	    if .f.
*!*	    *
*!*	    *  I had to suppress this one because GetNextModified() has
*!*	    *  an implicit record pointer move that fires the field/row
*!*	    *  rule(s), possibly generating another error here
*!*	    *
*!*	    IF cursorgetprop("BUFFERING") > 3
*!*	    lcString = lcString + space(lnOffset*4) + "GetNextModified(0)  " + TRANSFORM(getnextmodified(0,lcAlias)) + pcCRLF
*!*	    ENDIF
*!*	    endif
    *
    *  2/21/04 I put this back in, because VFP now has the 3rd lNoFire
    *  parameter for GetNextModified()
    *
    IF CURSORGETPROP("BUFFERING") > 3
      lcString = lcString + SPACE(lnOffset*4) + " GetNextModified(0) " + TRANSFORM(GETNEXTMODIFIED(0,lcAlias,.t.)) + pcCRLF
    ENDIF
    IF cursorgetprop("BUFFERING") > 1
      IF eof()
        lcString = lcString + space(lnOffset*4) + "   GetFldState(-1)  " + "EOF()" + pcCRLF
       ELSE
        lcString = lcString + space(lnOffset*4) + "   GetFldState(-1)  " + getfldstate(-1,lcAlias) + pcCRLF
      ENDIF
    ENDIF    
  ENDFOR &&& log all work areas
ENDIF &&& there are cursors open in this data session
*
*  we can't QUIT while in the middle of a transaction, 
*  nor should we leave any dirty buffers
*
*  rollback any extant transactions
*
DO WHILE TXNLEVEL() > 0
  ROLLBACK 
ENDDO
*
*  revert any dirty buffers
*
IF lnCursors > 0
  FOR xx = 1 TO lnCursors
    IF CURSORGETPROP("BUFFERING",laCursors[xx,1])>1
      IF UPPER(ALLTRIM(laCursors[xx,1])) == UPPER(ALLTRIM(pcErrorDBF)) 
        *  don't table revert the ErrorLog in 
        *  data session #1 - we're writing to it 
        *  at the moment
        LOOP 
      ENDIF
      SELECT (laCursors[xx,1])
      TABLEREVERT(.t.)
    ENDIF
  ENDFOR
ENDIF &&& any cursors open in this data session
SET DATASESSION TO lnDataSession
SELECT (pcErrorDBF)
replace er_forms WITH er_forms + lcString IN (pcErrorDBF)
RETURN 



PROCEDURE GetUserPK
*
*  returns the PK of the current user as a string
*  (even if PKs are integer)
*
local luUserPK, lnSelect
lnSelect = select(0)
DO CASE
  ******************************************************
  CASE vartype(oUser) = "O" AND vartype(oUser.icSubclassingPotential) # "U"
  ******************************************************
    *
    *  an oUser object exists
    *
    luUserPK = oUser.GetPProp("icUsr_PK")
  ******************************************************
  CASE VARTYPE(poActiveForm) = "O" ;
       AND VARTYPE(poActiveForm.ioUser) = "O" ;
       AND NOT ISNULL(poActiveForm.ioUser) ;
       AND VARTYPE(poActiveForm.icSubclassingPotential) # "U" 
  ******************************************************
    *
    *  no oUser object exists, but the current
    *  active form has an ioUser property
    *  which is an object in the VMP class
    *  hierarchies
    *
    *  note that, by default, when we use an 
    *  app architecture that doesn't use an 
    *  oUser object but rather has to store an
    *  "oUser" object reference to a form
    *  property, we name the form property
    *  ioUser, and this code kicks in
    *
    *
    luUserPK = poActiveForm.ioUser.GetPProp("icUsr_PK")
  ******************************************************
  OTHERWISE
  ******************************************************
    *
    *  if all else fails, use ? question marks
    *
    luUserPK = replicate("?",5)
ENDCASE
IF isnull(luUserPK)
  *
  *  possible if oUser or _Screen.ActiveForm.ioUser
  *  exist - the oUser/ioUser could conceivably
  *  not be the "user" object we're anticipating,
  *  in which case GetPProp() likely returns a .NULL.
  *
  luUserPK = replicate("?",5)
ENDIF      
IF vartype(luUserPK) = "C" AND luUserPK = "?????" AND used("USERS")
  local lcJunk
  lcJunk = type("USERS.Usr_PK")
  DO CASE
    CASE lcJunk = "N"
      luUserPK = 0
    CASE lcJunk = "U"
      lcJunk = space(0)
      local xx
      FOR xx = 1 to tagcount(space(0),"USERS")
        IF primary(xx,"USERS")
          lcJunk = SYS(14,xx,"USERS") 
          exit
        ENDIF
      ENDFOR
      IF !empty(lcJunk)
        select USERS
        lcJunk = eval(lcJunk)
        IF vartype(lcJunk) = "N"
          luUserPK = 0
        ENDIF
      ENDIF
  ENDCASE
ENDIF
IF vartype(luUserPK) = "N"
  luUserPK = TRANSFORM(luUserPK)
ENDIF
select (lnSelect)
return luUserPK



PROCEDURE GetUserName
*
*  returns the Name of the user in the format FirstName MI LastName
*
lparameters tuPK
local lcUserName, lnSelect
lnSelect = select(0)
DO CASE
  ******************************************************
  CASE vartype(oUser) = "O" AND vartype(oUser.icSubclassingPotential) # "U"
  ******************************************************
    *
    *  an oUser object exists
    *
    lcUserName = alltrim(oUser.GetUserInfo("Usr_FirstName")) + space(1) + ;
                 alltrim(oUser.GetUserInfo("Usr_MiddleInitial")) + space(1) + ;
                 alltrim(oUser.GetUserInfo("Usr_LastName"))
  ******************************************************
  CASE VARTYPE(poActiveForm) = "O" ;
       AND VARTYPE(poActiveForm.ioUser) = "O" ;
       AND NOT ISNULL(poActiveForm.ioUser) ;
       AND NOT VARTYPE(poActiveForm.icSubclassingPotential) = "U" 
  ******************************************************
    *
    *  no oUser object exists, but the current
    *  active form has an ioUser property
    *  which is an object in the VMP class
    *  hierarchies
    *
    *  note that, by default, when we use an 
    *  app architecture that doesn't use an 
    *  oUser object but rather has to store an
    *  "oUser" object reference to a form
    *  property, we name the form property
    *  ioUser, and this code kicks in
    *
    *
    lcUserName = ALLTRIM(poActiveForm.ioUser.GetUserInfo("Usr_FirstName")) + SPACE(1) + ;
                 ALLTRIM(poActiveForm.ioUser.GetUserInfo("Usr_MiddleInitial")) + SPACE(1) + ;
                 ALLTRIM(poActiveForm.ioUser.GetUserInfo("Usr_LastName"))
  ******************************************************
  OTHERWISE
  ******************************************************
    *
    *  if all else fails, use ? question marks
    *
    lcUserName = "unknown"
ENDCASE
IF isnull(lcUserName)
  *
  *  possible if oUser or _Screen.ActiveForm.ioUser
  *  exist - the oUser/ioUser could conceivably
  *  not be the "user" object we're anticipating,
  *  in which case GetUserInfo() likely returns a .NULL.
  *
  lcUserName = "unknown"
ENDIF      
select (lnSelect)
return lcUserName



DEFINE CLASS frmShowError AS Form
  *
  *  the form to display to the user is defined here so
  *  that we don't have to rely on any external files,
  *  which we cannot assume are available at this point
  *

  AutoCenter = .t.
  Height = 412
  Width = 640
  BorderStyle = 2
  Closable = .F.
  MaxButton = .F.
  MinButton = .F.
  WindowType = 1
  Name = "frmShowError"
  FontName = "Arial"
  FontSize = 9
  FontBold = .f.
  ShowWindow = 1
  ShowTips = .t.

  *  initial explanatory text
  ADD OBJECT label1 AS lblError WITH ;
	Height = 33, ;
	Top = 12, ;
	Width = 529, ;    &&& keep this Width > 500 -- see below
 	WordWrap = .T., ;
 	Left = 16, ;
	Name = "Label1"

  *  Shape behind the error info
  ADD OBJECT Shape1 AS shape WITH ;
	Height = 144, ;
 	Left = 16, ;
	Top = 44, ;
	Width = 529, ; 
	SpecialEffect = 0, ;
	BackColor = rgb(255,255,255), ;  
	Name = "Shape1"

  *  Error prompt
  ADD OBJECT label2 AS lblErrorPrompt WITH ;
	Top = 48, ;
	Name = "Label2", ;
	Alignment = 1

  *  Error message editbox 
  ADD OBJECT edtErrorText AS editbox WITH ;
	FontBold = .F., ;
	FontSize = 9, ;
	Height = 64, ;
	Top = 46, ;
	Scrollbars = 0, ;
	Enabled = .f., ;
	BackStyle = 0, ;
	DisabledForeColor = rgb(255,0,0), ;
	SpecialEffect = 1, ;
	BorderStyle = 0, ;
	Name = "edtErrorText"

  *  Program section prompt
  ADD OBJECT label3 AS lblErrorPrompt WITH ;
	Top = 112, ;
	Name = "Label3", ;
	Alignment = 1

  *  Program section text
  ADD OBJECT label6 AS lblErrorInfo WITH ;
	Top = 112, ;
	Name = "Label6"

  *  Line Number prompt
  ADD OBJECT label10 AS lblErrorPrompt WITH ;	 
	Top = 132, ;
	Name = "Label10", ;
	Alignment = 1

  *  Line Number text
  ADD OBJECT label11 AS lblErrorInfo WITH ;			 
	Height = 51, ;
	Top = 132, ;
	Name = "Label11"
	
  *  Problem code prompt
  ADD OBJECT label4 AS lblErrorPrompt WITH ;
	Top = 152, ;
	Name = "Label4", ;
	Alignment = 1

  *  Problem code text
  ADD OBJECT Label7 AS lblErrorInfo WITH ;
	Height = 33, ;
	Top = 152, ;
	Name = "Label7", ;
    WordWrap = .t.

  *  text before user-entry editbox
  ADD OBJECT label8 AS lblError WITH ;
	Height = 51, ;
	Top = 196, ;
 	Left = 16, ;
	Width = 529, ;       &&& keep this Width > 500 -- see below
	WordWrap = .T., ;
	Name = "Label8"

  *  User-entry editbox
  ADD OBJECT edtUserInfo AS editbox WITH ;
	FontBold = .F., ;
	FontSize = 9, ;
	Height = 81, ;
 	Left = 16, ;
	Top = 244, ;
	Width = 529, ;   
	Name = "edtUserInfo"

  *  Thank you text
  ADD OBJECT lblCustom1 AS lblError WITH ;
 	Left = 16, ;
	Top = 300, ;
	Name = "lblCustom1", ;
	Caption = "", ;
	Alignment = 2   &&& Centered

  *  Thank you text
  ADD OBJECT lblThankYou AS lblError WITH ;
	AutoSize = .T., ;
 	Left = 16, ;
	Top = 328, ;
	Name = "lblThankYou"

  ADD OBJECT cmdPrintScreen AS commandbutton WITH ;
	Top = 332, ;
	Left = 60, ;   
	Height = 27, ;
	Width = 120, ;   
	FontBold = .F., ;
	FontSize = 9, ;
	Name = "cmdPrintScreen"

  ADD OBJECT cmdPrintErrorInfo AS commandbutton WITH ;
	Top = 332, ;
	Left = 260, ;   
	Height = 27, ;
	Width = 120, ;   
	FontBold = .F., ;
	FontSize = 9, ;
	Name = "cmdPrintErrorInfo"

  ADD OBJECT cmdClipBoard AS commandbutton WITH ;
	Top = 332, ;
	Left = 460, ;     
	Height = 27, ;
	Width = 140, ;     
	FontBold = .F., ;
	FontSize = 9, ;
	Name = "cmdClipBoard"

  ADD OBJECT cmdOK AS commandbutton WITH ;
	Top = 332, ;
	Left = 472, ;
	Height = 27, ;
	Width = 80, ;
	FontBold = .F., ;
	FontSize = 9, ;
	Name = "cmdOK"

  *
  *  while we'd like the user to see this form, and maybe
  *  enter some useful information, an error might happen 
  *  while they're away from their workstation, so we'll 
  *  timeout after an hour to make sure cleanup, release
  *  of any RLOCK()s, etc. takes place
  *
  ADD OBJECT timer1 AS timer WITH ;
	Top = 0, ;
	Left = 0, ;
	Height = 23, ;
	Width = 23, ;
	Interval = 3600000, ;
	Name = "Timer1"

  PROCEDURE Init
	*
	*      Label1 -- Label at the very top of the form
	*
	*      Shape1 -- White shape that is the top white section
	*
	*      Label2 -- "Error" prompt in the top white section
	*                edtErrorText -- red "Error" text
	*      Label3 -- "Program section" prompt in the top white section
    *                Label6 -- red "Program section" text
    *     Label10 -- "Line Number" prompt in the top white section
    *                Label11 - red "Line Number" text
	*      Label4 -- "Problem code" prompt in the top white section
    *                Label7 -- red "Problem code" text
	*
	*      Label8 -- Label between the white sections, asking
	*                the user to document what they were doing
	* edtUserInfo -- Editbox where the user can document what
	*                they were doing
    * 
    *  lblCustom1 -- Just above "Thank you" label, this full-width
    *                label can display whatever you want, by
    *                updating the "frmShowError.lblCustom1.Caption"
    *                record in your MSGSVC table
    *  
    * lblThankYou -- "Thank you" text at the bottom left  
    *
    *
    *  all strings are built here to make it easier
    *  to localize them in the future
    *
    *
    *  ===============================================================
    *  CUSTOMIZING THIS FORM
    *  ===============================================================
    *  NOTE that the display text for the following can be
    *  updated, by updating the corresponding record(s) in your
    *  app-specific MSGSVC.DBF table:
    *    lblCustom1 (don't get carried away -- there is just so much
    *                text you can fit in this label.Caption)
    *                This MSGSVC.DBF record defaults to a blank cOriginal
    *                value, so if you do nothing, it's as if this label
    *                isn't there
    *      BROWSE FOR UPPER(cKey) = UPPER("frmShowError.lblCustom1.Caption")
    *    cmdClipboard.ToolTipText
    *      BROWSE FOR UPPER(cKey) = UPPER("frmShowError.cmdClipboard.ToolTipText")
    *    cmdPrintErrorInfo.ToolTipText
    *      BROWSE FOR UPPER(cKey) = UPPER("frmShowError.cmdPrintErrorInfo.ToolTipText")
    *    cmdPrintScreen.ToolTipText
    *      BROWSE FOR UPPER(cKey) = UPPER("frmShowError.cmdPrintScreen.ToolTipText")
    *  ...you can also update any of the above and a whole lot
    *  more via the ErrHook1a.PRG procedural hook, as described
    *  next.
    *     
    *  The last thing this method (THIS.Init) does is call a
    *  procedural hook program, ErrHook1a.PRG, if it exists, and
    *  passes it an object reference to THISFORM -- you can 
    *  manipulate anything you want from ErrHook1a.PRG, including
    *  adding controls.  If you add a control like a CommandButton,
    *  you can BINDEVENT() its Click to an object you create in
    *  ErrHook1a.PRG -- just be careful not to get too fancy, 
    *  because an error condition is in effect.  And don't forget
    *  to build your ErrHook1a.PRG into your distributed .EXE/.APP,
    *  so it's available here, even in an error scenario.
    *  
    *
    *  ===============================================================
    *  TESTING THIS FORM
    *  ===============================================================
    *  Here's an easy way to test this form:
    *  1- Set the Ap_ItemVal field to ".f." for this AppConfig record:
    *       USE ??CONFIG
    *       BROWSE FOR UPPER(Ap_Item) = UPPER("NoOnErrorDuringDev")
    *  2- Select a form that runs standalone and that contains a button
    *     (Add, Print, Delete, whatever) that contains no Click event
    *     method code, enter a single line that crashes such as:
    *       BOMB
    *       Error 1
    *  3- DO FORM Whatever
    *  4- Click the button
    *  ...just don't forget to remove that error-generating code
    *  when you're done testing -- you have enough bugs in your 
    *  code without this one!
    *
    SELECT (m.pcErrorDBF)
    THIS.Caption         = X3I("System Error") + SPACE(4) + TTOC(DATETIME())
    THIS.Label1.Caption  = X3MSGSVC("Error, please report") 
    THIS.Label2.Caption  = X3I("Error")
    THIS.Label3.Caption  = X3I("Program section")
    THIS.Label10.Caption = X3I("Line Number")	 
    THIS.Label11.Caption = Er_LineNo			 
    THIS.Label4.Caption  = X3I("Problem code")
    LOCAL xx 
    xx = SUBSTRC(Er_Program,RATC(CHR(13)+CHR(10),Er_Program)+3)
    xx = LEFTC(m.xx,LENC(m.xx)-1)
    THIS.Label6.Caption = m.xx 
    THIS.Label7.Caption = LEFTC(ALLTRIM(Er_Code),255)
    IF LENC(THIS.Label7.Caption) = 255
      THIS.Label7.Caption = LEFTC(THIS.Label7.Caption,252) + "..."
    ENDIF
    THIS.edtErrorText.Value = ALLTRIM(er_error) + "/" + ALLTRIM(er_message)
    THIS.Label8.Caption = LEFTC(X3MSGSVC("Error, user description"),254)
    THIS.lblCustom1.Caption = LEFTC(X3MSGSVC("frmShowError.lblCustom1.Caption"),254)
    THIS.lblThankYou.Caption = X3I("Thank you")
    THIS.cmdOK.Caption = X3I("\<OK")
    THIS.cmdClipBoard.Caption = X3I("Copy to Clipboard")
    THIS.cmdClipBoard.ToolTipText = X3MSGSVC("frmShowError.cmdClipboard.ToolTipText")
    THIS.cmdPrintScreen.Caption = X3I("Print screen")
    THIS.cmdPrintScreen.ToolTipText = X3MSGSVC("frmShowError.cmdPrintScreen.ToolTipText")
    THIS.cmdPrintErrorInfo.Caption = X3I("Print info")
    THIS.cmdPrintErrorInfo.ToolTipText = X3MSGSVC("frmShowError.cmdPrintErrorInfo.ToolTipText")
    *
    *  right-justify the Error, Program section, 
    *  Problem code, Line Number prompts
    *
    LOCAL loLabel
    FOR xx = 1 TO 10
      IF INLIST(m.xx,5,9)
        *  there is no Label5 or Label9
        LOOP 
      ENDIF
      loLabel = EVALUATE("THIS.Label" + TRANSFORM(m.xx))
      IF m.loLabel.Width < 500
        m.loLabel.Width = THIS.TextWidth(m.loLabel.Caption)
      ENDIF  
      IF INLIST(m.xx,2,3,4,9,10)
        loLabel.Left = THIS.Label6.Left - 8 - m.loLabel.Width
      ENDIF
    ENDFOR
    *
    *  update Widths and Tops based on the current size 
    *  of this form
    *
    LOCAL lnWMargin, lnHMargin
    lnWMargin = 16
    lnHMargin = 12
    THIS.Label1.Width = THIS.Width - (m.lnWMargin*2)
    THIS.Shape1.Width = THIS.Width - (m.lnWMargin*2)
    THIS.edtErrorText.Left = THIS.Label2.Left + THIS.Label2.Width + 4
    THIS.edtErrorText.Width = THIS.Width-THIS.edtErrorText.Left-18
    THIS.Label7.Width = THIS.Width - THIS.Label7.Left - (THIS.Width-THIS.Shape1.Left-THIS.Shape1.Width) - 4   &&& "Problem code" text
	THIS.Label6.Width = THIS.Label7.Width
    THIS.Label8.Width = THIS.Width - (m.lnWMargin*2)
    THIS.edtUserInfo.Width = THIS.Width - (m.lnWMargin*2)
    THIS.edtUserInfo.Height = THIS.Height - THIS.edtUserInfo.Top - (m.lnHMargin*2) - THIS.cmdOK.Height - 16
    THIS.lblCustom1.Left = THIS.edtUserInfo.Left
    THIS.lblCustom1.Top = THIS.edtUserInfo.Top + THIS.edtUserInfo.Height + 4
    THIS.lblCustom1.Width = THIS.Width - (THIS.lblCustom1.Left*2) 
    THIS.cmdOK.Top = THIS.Height - m.lnHMargin - THIS.cmdOK.Height
    THIS.cmdOK.Left = THIS.Width - m.lnWMargin - THIS.cmdOK.Width
    THIS.lblThankYou.Top = THIS.cmdOK.Top+6 
    THIS.cmdClipboard.Top = THIS.cmdOK.Top 
    THIS.cmdClipboard.Left = THIS.cmdOK.Left - 16 - THIS.cmdClipboard.Width
    THIS.cmdPrintErrorInfo.Top = THIS.cmdClipBoard.Top
    THIS.cmdPrintErrorInfo.Left = THIS.cmdClipboard.Left - 16 - THIS.cmdPrintErrorInfo.Width
    THIS.cmdPrintScreen.Top = THIS.cmdClipBoard.Top
    THIS.cmdPrintScreen.Left = THIS.cmdPrintErrorInfo.Left - 16 - THIS.cmdPrintScreen.Width

    *
    *  make sure we can see this form
    *
    IF _Screen.Visible = .t.
      *
      *  "normal" VFP app, _Screen is visible and 
      *  is the MDI parent
      *
      IF _Screen.Height - 20 < THIS.Height
        _Screen.Height = THIS.Height + 20
      ENDIF
      IF _Screen.Width - 20 < THIS.Width
        _Screen.Width = THIS.Width + 20
      ENDIF
      THIS.AutoCenter = .t.
     ELSE
      *
      *  "SDI" app, _Screen is invisible and has
      *  been replaced by a VFP form whose
      *  ShowWindow = 2
      *
      LOCAL loForm
      FOR EACH loForm in _Screen.Forms
        IF TYPE("loForm.BaseClass") = "C"   &&& VFP Task list protects most properties, including BaseClass, so we can't use VARTYPE()
          IF m.loForm.ShowWindow = 2
            IF m.loForm.Height - 20 < THIS.Height
              m.loForm.Height = THIS.Height + 20
            ENDIF
            IF m.loForm.Width - 20 < THIS.Width
              m.loForm.Width = THIS.Width + 20
            ENDIF
            THIS.Top = 20
            THIS.Left = 20
            EXIT 
          ENDIF
        ENDIF
      ENDFOR
    ENDIF

    THIS.edtUserInfo.ControlSource = "ER_UsInfo"

    IF FILE("XXFWBLANK.ICO")
      THIS.Icon = "XXFWBLANK.ICO"
    ENDIF

    LOCAL lcErrHook
    lcErrHook = "ErrHook1a"
    IF FILE(m.lcErrHook+".PRG") OR FILE(m.lcErrHook+".FXP")
      *
      *  if ErrHook1a.PRG/ErrHook1a.FXP exist in the
      *  VFP path at the moment, it gets called, and
      *  is passed an object reference to this form,
      *  in case you want to customize this form here
      *
      DO (m.lcErrHook) WITH THIS
    ENDIF
  ENDPROC

  PROCEDURE cmdOK.Click
    THISFORM.Release()
  ENDPROC

  PROCEDURE cmdClipboard.Click
    THISFORM.SetAll("MousePointer",11)
    GenerateErrorTextToClipboard()   &&& local procedure here in XXERROR.PRG
    THISFORM.SetAll("MousePointer",0)
  ENDPROC

  PROCEDURE cmdPrintErrorInfo.Click
    LOCAL loException, llDone, lnRecno
    *
    *  first, try XXERROR.FRX
    *
    IF FILE("XXERROR.FRX")
      loException = .f.
      TRY
      USE XXERROR.FRX IN 0 SHARED ALIAS TestXXERROR
      CATCH TO loException
      ENDTRY
      USE IN SELECT("TestXXERROR")      
      IF NOT VARTYPE(m.loException) = "O"
        *
        *  looks like we'll be able to run the report
        *
        llDone = .t.
        SELECT (pcErrorDBF)   &&& just in case, but should be redundant
        lnRecno = RECNO()
        loException = .f.
        WAIT WINDOW X3MSGSVC("Preparing information to print...") NOWAIT NOCLEAR 
        TRY
        REPORT FORM XXERROR FOR RECNO() = m.lnRecno TO PRINTER PROMPT NOCONSOLE 
        CATCH TO loException
          llDone = .f.
        ENDTRY
        WAIT CLEAR 
        GOTO m.lnRecno
      ENDIF
    ENDIF
    IF NOT m.llDone
      *
      *  try XXFWUTIL.VCX/frmXXTextBlockReport 
      *
      IF FILE("XXFWUTIL.VCX") AND X8SETCLS("XXFWUTIL.VCX")
        LOCAL lcClipText, loReport
        lcClipText = _ClipText
        GenerateErrorTextToClipboard()   &&& local procedure here in XXERROR.PRG
        loException = .f.
        TRY
        loReport = CREATEOBJECT("frmTextBlockReport", ;
                                _ClipText, ;
                                THISFORM.Caption, ;
                                "Courier New", ;
                                9, ;
                                .f., ;
                                .f., ;
                                "PRINTER")                
        CATCH TO loException
        ENDTRY
        _ClipText = m.lcClipText
        llDone = (NOT VARTYPE(m.loException)="O")
      ENDIF
    ENDIF
  ENDPROC

  PROCEDURE cmdPrintScreen.Click
    LOCAL loException
    TRY
    IF FILE("X7PrintWindow.PRG") OR FILE("X7PrintWindow.FXP")
      IF _Screen.Visible
        X7PrintWindow(_Screen)
       ELSE
        *
        *  Top-Level form app
        *
        LOCAL loForm
        FOR EACH loForm IN _Screen.Forms
          IF loForm.ShowWindow = 2
            X7PrintWindow(m.loForm)
            EXIT 
          ENDIF
        ENDFOR
      ENDIF
    ENDIF
    CATCH TO loException
    ENDTRY
  ENDPROC

  PROCEDURE Timer1.Timer
    THISFORM.Release()
  ENDPROC

ENDDEFINE


DEFINE CLASS lblError AS Label
  Caption = space(0)
  BackStyle = 0
  FontName = "Arial"
  FontBold = .F.
  FontSize = 9
  Height = 17
  Left = 20
ENDDEFINE


DEFINE CLASS lblErrorPrompt AS lblError
  Alignment = 1
  Left = 24
  Width = 200
ENDDEFINE


DEFINE CLASS lblErrorInfo AS lblError
  ForeColor = RGB(255,0,0) 
  Height = 33 
  Left = 160
  Width = 309 
  WordWrap = .T.
ENDDEFINE



DEFINE CLASS cusPPWorkArea as Custom
*
*  this is pretty much the same as 
*  XXFWUTIL.VCX/cusPushPopAlias, but I've repeated
*  it here because we're in some kind of error 
*  condition, and I don't want to assume/depend on
*  the availability of XXFWUTIL
*

icSaveAlias = SPACE(0)

PROCEDURE Init
THIS.icSaveAlias = ALIAS()
ENDPROC

PROCEDURE Destroy
IF EMPTY(THIS.icSaveAlias) OR NOT USED(THIS.icSaveAlias)
  SELECT 0
 ELSE
  SELECT (THIS.icSaveAlias)
ENDIF
ENDPROC

ENDDEFINE



PROCEDURE X3I
*
*  this is a local procedure that duplicates X3I.PRG
*  (and combines X3I.PRG and the I.PRG/function in 
*  XXFWMAIN.PRG) functionality, because X3I.PRG may not 
*  be available in an error scenario
*
LPARAMETERS tcString, tuINTLSecond
IF TYPE("_Screen.oINTL.BaseClass") = "C" 
  *
  *  Steven Black's INTL Toolkit is installed 
  RETURN _Screen.oINTL.I(m.tcString,m.tuINTLSecond)
 ELSE
  *
  *  Steven Black's INTL Toolkit is not installed
  *
  RETURN m.tcString
ENDIF
RETURN



PROCEDURE GenerateErrorTextToClipboard
*
*  called from the cmdClipboard::Click()
*
IF USED(m.pcErrorDBF)
  LOCAL lnSelect
  lnSelect = SELECT(0)
  SELECT (m.pcErrorDBF)
  _Cliptext = GenerateBasicErrorText() + CHR(13) + CHR(10) + ;
              GenerateListMemoryText() + CHR(13) + CHR(10) + ;
              GenerateListStatusText() + CHR(13) + CHR(10) + ;
              GenerateCONFIGFPWText() + CHR(13) + CHR(10) + ;
              GenerateFormStatusText() + CHR(13) + CHR(10) + ;
              GenerateUserCommentsText() + CHR(13) + CHR(10)
  SELECT (m.lnSelect)
 ELSE
  _Cliptext = X3I("Unable to copy error information to the Clipboard because the Error Log table is unavailable")
ENDIF
RETURN



PROCEDURE GenerateBasicErrorText
*
*  the current work area must be the ErrorLog table/cursor
*  
*  in addition to being called here in XXERROR.PRG,
*  this procedure is called from the Event Log form:
*    XXDTES("XXFWFRM.VCX","GenerateBasicErrorText","frmEventLog","ShellRequeryMainViewAlias")
*
LOCAL lcText, laLines[1], lcLine

lcText = SPACE(0)

TRY
lcText = X3I("Error occurred")+":" + CHR(13) + CHR(10)
lcText = m.lcText + SPACE(4) + TTOC(Er_DatTime) + CHR(13) + CHR(10)
lcText = m.lcText + X3I("User")+":" + CHR(13) + CHR(10)
lcText = m.lcText + SPACE(4) + Er_User + CHR(13) + CHR(10)
lcText = m.lcText + X3I("Did the user reboot instead of allowing the error shutdown?") + CHR(13) + CHR(10)
lcText = m.lcText + SPACE(4) + + IIF(Er_Reboot,X3I("Yes"),X3I("No")) + CHR(13) + CHR(10)
lcText = m.lcText + X3I("Error #")+":" + CHR(13) + CHR(10)
lcText = m.lcText + SPACE(4) + Er_Error + CHR(13) + CHR(10)
lcText = m.lcText + X3I("Method/.PRG in which the error occurred")+":" + CHR(13) + CHR(10)
lcText = m.lcText + SPACE(4) + Er_MethPRG + CHR(13) + CHR(10)
lcText = m.lcText + X3I("Line at which the error occurred")+":" + CHR(13) + CHR(10)
lcText = m.lcText + SPACE(4) + Er_LineNo + CHR(13) + CHR(10)

lcText = m.lcText + X3I("System Message")+":" + CHR(13) + CHR(10)
IF ALINES(laLines,Er_Message,.t.)>0
  FOR EACH lcLine IN laLines
    lcText = m.lcText + SPACE(4) + ALLTRIM(m.lcLine) + CHR(13) + CHR(10)
  ENDFOR
 ELSE
  lcText = m.lcText + "(" + X3I("None") + ")" + CHR(13) + CHR(10)
ENDIF
lcText = m.lcText + CHR(13) + CHR(10)

RELEASE laLines
LOCAL laLines[1]
lcText = m.lcText + X3I("Line of code that generated the error")+":" + CHR(13) + CHR(10)
IF ALINES(laLines,Er_Code,.t.)>0
  FOR EACH lcLine IN laLines
    lcText = m.lcText + SPACE(4) + ALLTRIM(m.lcLine) + CHR(13) + CHR(10)
  ENDFOR
 ELSE
  lcText = m.lcText + "(" + X3I("None") + ")" + CHR(13) + CHR(10)
ENDIF
lcText = m.lcText + CHR(13) + CHR(10)

RELEASE laLines
LOCAL laLines[1]
lcText = m.lcText + X3I("AERROR() contents")+":" + CHR(13) + CHR(10)
IF ALINES(laLines,Er_AError,.t.)>0
  FOR EACH lcLine IN laLines
    IF "AERROR" $ UPPER(m.lcLine) AND "CONTENTS" $ UPPER(m.lcLine)
      LOOP 
     ELSE
      lcText = m.lcText + SPACE(4) + ALLTRIM(m.lcLine) + CHR(13) + CHR(10)
    ENDIF
  ENDFOR
 ELSE
  lcText = m.lcText + "(" + X3I("None") + ")" + CHR(13) + CHR(10)
ENDIF
lcText = m.lcText + CHR(13) + CHR(10)

RELEASE laLines
LOCAL laLines[1]
lcText = m.lcText + X3I("Program execution path")+":" + CHR(13) + CHR(10)
IF ALINES(laLines,Er_Program,.t.)>0
  FOR EACH lcLine IN laLines
    lcText = m.lcText + SPACE(4) + ALLTRIM(m.lcLine) + CHR(13) + CHR(10)
  ENDFOR
 ELSE
  lcText = m.lcText + "(" + X3I("None") + ")" + CHR(13) + CHR(10)
ENDIF
lcText = m.lcText + CHR(13) + CHR(10)

RELEASE laLines
LOCAL laLines[1]
lcText = m.lcText + X3I("General")+":" + CHR(13) + CHR(10)
IF ALINES(laLines,Er_Environ,.t.)>0
  FOR EACH lcLine IN laLines
    lcText = m.lcText + SPACE(4) + ALLTRIM(m.lcLine) + CHR(13) + CHR(10)
  ENDFOR
 ELSE
  lcText = m.lcText + "(" + X3I("None") + ")" + CHR(13) + CHR(10)
ENDIF
lcText = m.lcText + CHR(13) + CHR(10)

IF "STARTERR.TXT" $ UPPER(Er_Code)
  RELEASE laLines
  LOCAL laLines[1]
  lcText = m.lcText + X3I("App startup error information")+":" + CHR(13) + CHR(10)
  IF ALINES(laLines,FILETOSTR("StartErr.TXT"),1)>0
    FOR EACH lcLine IN laLines
      lcText = m.lcText + SPACE(4) + ALLTRIM(m.lcLine) + CHR(13) + CHR(10)
    ENDFOR
   ELSE
    lcText = m.lcText + "(" + X3I("None") + ")" + CHR(13) + CHR(10)
  ENDIF
ENDIF  
CATCH
ENDTRY 
RETURN m.lcText



PROCEDURE GenerateListMemoryText
*
*  the current work area must be the ErrorLog table/cursor
*  
*  in addition to being called here in XXERROR.PRG,
*  this procedure is called from the Event Log form:
*    XXDTES("XXFWFRM.VCX","GenerateListMemoryText","frmEventLog","ShellRequeryMainViewAlias")
*
LOCAL lcText, laLines[1], lcLine

lcText = SPACE(0)

TRY
lcText = X3I("List Memory")+":" + CHR(13) + CHR(10)
IF ALINES(laLines,Er_LstMemo,.t.)>0
  FOR EACH lcLine IN laLines
    lcLine = STRTRAN(m.lcLine,"Local"+SPACE(14),"Local")
    lcLine = STRTRAN(m.lcLine,"Pub"+SPACE(16),"Pub")
    lcLine = STRTRAN(m.lcLine,"Priv"+SPACE(15),"Priv")
    lcLine = STRTRAN(m.lcLine,SPACE(10)+"Local","  Local")
    lcLine = STRTRAN(m.lcLine,SPACE(10)+"Pub","  Pub")
    lcLine = STRTRAN(m.lcLine,SPACE(10)+"Priv","  Priv")
    lcLine = STRTRAN(m.lcLine,SPACE(6)+"Local","  Local")
    lcLine = STRTRAN(m.lcLine,SPACE(6)+"Pub","  Pub")
    lcLine = STRTRAN(m.lcLine,SPACE(6)+"Priv","  Priv")
    IF UPPER(lcLine) = "LOCAL " ;
         OR UPPER(lcLine) = "PUB" ;
         OR UPPER(lcLine) = "PRIV"
      lcLine = SPACE(4) + lcLine
    ENDIF
    lcText = m.lcText + ALLTRIM(m.lcLine) + CHR(13) + CHR(10)
  ENDFOR
 ELSE
  lcText = m.lcText + "(" + X3I("None") + ")" + CHR(13) + CHR(10)
ENDIF
lcText = m.lcText + CHR(13) + CHR(10)
CATCH
ENDTRY
RETURN m.lcText



PROCEDURE GenerateListStatusText
*
*  the current work area must be the ErrorLog table/cursor
*  
*  in addition to being called here in XXERROR.PRG,
*  this procedure is called from the Event Log form:
*    XXDTES("XXFWFRM.VCX","GenerateListStatusText","frmEventLog","ShellRequeryMainViewAlias")
*
LOCAL lcText, laLines[1], lcLine

lcText = SPACE(0)

TRY
lcText = X3I("List Status")+":" + CHR(13) + CHR(10)
IF ALINES(laLines,Er_LstStat,.t.)>0
  FOR EACH lcLine IN laLines
    lcText = m.lcText + SPACE(4) + ALLTRIM(m.lcLine) + CHR(13) + CHR(10)
  ENDFOR
 ELSE
  lcText = m.lcText + "(" + X3I("None") + ")" + CHR(13) + CHR(10)
ENDIF
CATCH
ENDTRY
RETURN m.lcText




PROCEDURE GenerateCONFIGFPWText
*
*  the current work area must be the ErrorLog table/cursor
*  
*  in addition to being called here in XXERROR.PRG,
*  this procedure is called from the Event Log form:
*    XXDTES("XXFWFRM.VCX","GenerateCONFIGFPWText","frmEventLog","ShellRequeryMainViewAlias")
*
LOCAL lcText, laLines[1], lcLine

lcText = SPACE(0)

TRY
lcText = X3I("CONFIG.FPW in effect")+":" + CHR(13) + CHR(10)
IF ALINES(laLines,Er_Config,.t.)>0
  FOR EACH lcLine IN laLines
    lcText = m.lcText + SPACE(4) + ALLTRIM(m.lcLine) + CHR(13) + CHR(10)
  ENDFOR
 ELSE
  lcText = m.lcText + "(" + X3I("None") + ")" + CHR(13) + CHR(10)
ENDIF
CATCH
ENDTRY
RETURN m.lcText



PROCEDURE GenerateFormStatusText
*
*  the current work area must be the ErrorLog table/cursor
*  
*  in addition to being called here in XXERROR.PRG,
*  this procedure is called from the Event Log form:
*    XXDTES("XXFWFRM.VCX","GenerateFormStatusText","frmEventLog","ShellRequeryMainViewAlias")
*
LOCAL lcText, laLines[1], lcLine

lcText = SPACE(0)

TRY
lcText = X3I("Form status")+":" + CHR(13) + CHR(10)
IF ALINES(laLines,Er_Forms,.t.)>0
  FOR EACH lcLine IN laLines
    lcText = m.lcText + SPACE(4) + ALLTRIM(m.lcLine) + CHR(13) + CHR(10)
  ENDFOR
 ELSE
  lcText = m.lcText + "(" + X3I("None") + ")" + CHR(13) + CHR(10)
ENDIF
CATCH
ENDTRY
RETURN m.lcText



PROCEDURE GenerateUserCommentsText
*
*  the current work area must be the ErrorLog table/cursor
*  
*  in addition to being called here in XXERROR.PRG,
*  this procedure is called from the Event Log form:
*    XXDTES("XXFWFRM.VCX","GenerateUserCommentsText","frmEventLog","ShellRequeryMainViewAlias")
*
LOCAL lcText, laLines[1], lcLine

lcText = SPACE(0)

TRY
lcText = X3I("User's comments about the error circumstances")+":" + CHR(13) + CHR(10)
lcText = lcText + REPLICATE("=",60) + CHR(13) + CHR(10)
IF ALINES(laLines,Er_UsInfo,.t.)>0
  FOR EACH lcLine IN laLines
    lcText = m.lcText + SPACE(4) + ALLTRIM(m.lcLine) + CHR(13) + CHR(10)
  ENDFOR
 ELSE
  lcText = m.lcText + "(" + X3I("None") + ")" + CHR(13) + CHR(10)
ENDIF
CATCH
ENDTRY
RETURN m.lcText

