*
*  VMP One2Many Business Object test case template
*
*  VMPFXUTestCaseTemplate_BO_One2Many.TXT
*

**********************************************************************
DEFINE CLASS <<testclass>> as FxuTestCase OF FxuTestCase.prg
**********************************************************************

*
*  to use this class, do a text search on "???" and 
*  take the indicated actions
*  
*  of course, there are many other tests you may wish to 
*  add, and you may have to modify the tests included here
*  to your needs -- this template is intended as a 
*  "quick start"
*

#IF .f.
LOCAL THIS AS <<testclass>> OF <<testclass>>.PRG
#ENDIF
	
*  
*  declare properties here that are used by one or
*  more individual test methods of this class
*
*  for example, if you create an object to a custom
*  THIS.Property in THIS.Setup(), estabish the property
*  here, where it will be available (to IntelliSense)
*  throughout
*

ioBOMgr = .NULL.
ioChildBO = .NULL.
icSetClassLib = SPACE(0)
icSetCentury = SPACE(0)
icSetDeleted = SPACE(0)
icParentAlias = SPACE(0)
icChildAlias = SPACE(0)
icParentBOName = "???"   &&& specify the ClassName of the parent Business Object
icParentBOVCX = "???"    &&& specify the VCX containing THIS.icParentBOName
icParentTable = "???"    &&& if the ultimate data source of the BO.ioMainDS is a VFP table, specify that TableName (no Database! qualifier)
icChildBOName = "???"    &&& specify the ClassName of the child Business Object
icChildBOVCX = "???"     &&& specify the VCX containing THIS.icChildBOName
icChildTable = "???"     &&& if the ultimate data source of the child BO.ioMainDS is a VFP table, specify that TableName (no Database! qualifier)
	
********************************************************************
PROCEDURE Setup
********************************************************************

*
*  put common setup code here -- this method is called
*  whenever THIS.Run() (inherited method) to run each
*  of the custom test methods you add, specific test
*  methods that are not inherited from FoxUnit
*
*  do NOT call THIS.Assert..() methods here -- this is
*  NOT a test method
*

THIS.icSetDeleted = SET("DELETED")
SET DELETED ON
THIS.icSetCentury = SET("CENTURY")
SET CENTURY ON
THIS.icSetClassLib = SET("CLASSLIB")

SET CLASSLIB TO XXFWNTBO.VCX ADDITIVE 
THIS.ioBOMgr = CREATEOBJECT("ctrBusinessObjectManager")

SET CLASSLIB TO (THIS.icParentBOVCX) ADDITIVE
THIS.ioBOMgr.AddObject("o"+THIS.icParentBOName,THIS.icParentBOName)
EVALUATE("THIS.ioBOMgr.o"+THIS.icParentBOName+".SetAsMainBO()")
THIS.icParentAlias = THIS.ioBOMgr.ioMainBO.ioMainDS.icAlias

SET CLASSLIB TO (THIS.icChildBOVCX) ADDITIVE
THIS.ioBOMgr.AddObject("o"+THIS.icChildBOName,THIS.icChildBOName,"o"+THIS.icParentBOName)
THIS.ioChildBO = THIS.ioBOMgr.iaBO[2,1]
THIS.icChildAlias = THIS.ioChildBO.ioMainDS.icAlias

ENDPROC

	

********************************************************************
PROCEDURE TearDown
********************************************************************
*
*  put common cleanup code here -- this method is called
*  whenever THIS.Run() (inherited method) to run each
*  of the custom test methods you add, specific test
*  methods that are not inherited from FoxUnit
*
*  do NOT call THIS.Assert..() methods here -- this is
*  NOT a test method
*
THIS.ioChildBO = .NULL.

THIS.ioBOMgr.ORCleanup()   &&& XXDTES("XXFWNTBO.VCX","!!!IMPORTANT NOTE!!!","ctrbusinessobjectmanager","ZREADME")
THIS.ioBOMgr = .NULL.

*
*  NOTE!
*  PUT ANY ADDITIONAL CODE TO RELEASE OBJECTS HERE!
*  it is important to do so BEFORE the code below
*  to reset SETtings
*

LOCAL lcSet
lcSet = THIS.icSetClassLib
SET CLASSLIB TO &lcSet
lcSet = THIS.icSetCentury
SET CENTURY &lcSet
lcSet = THIS.icSetDeleted
SET DELETED &lcSet

ENDPROC



*********************************************************************
PROCEDURE Test_InstantiationSucceeded
*********************************************************************
THIS.AssertNotNull("The Business Object Manager object was not instantiated during Setup()", ;
	               THIS.ioBOMgr)
THIS.AssertTrue(THIS.icParentBOName + " parent BO was not instantiated during Setup()", ;
                TYPE([EVALUATE("THIS.ioBOMgr.o"+THIS.icParentBOName)]) = "O")
THIS.AssertTrue(THIS.icChildBOName + " child BO was not instantiated during Setup()", ;
                TYPE([EVALUATE("THIS.ioBOMgr.o"+THIS.icChildBOName)]) = "O")

ENDPROC



*********************************************************************
PROCEDURE Test_TablesAreOpenOnInit
*********************************************************************
*
*  the updateable icAlias cursor of both BO.ioMainDS should be open
*
*  for VFP data, the ultimate/base table should be open
*
THIS.AssertTrue("Instantiation of " + THIS.icParentBOName + " did not open its ioMainDS.icAlias", ;
                USED(THIS.ioBOMGR.ioMainBO.ioMainDS.icAlias))
IF NOT EMPTY(THIS.ioBOMgr.ioMainBO.ioMainDS.icTableName) ;
     AND INLIST(THIS.ioBOMgr.ioMainBO.ioMainDS.icDataSourceType,"FT","CT","LV","SPT_CT","SPT_FT")
  THIS.AssertTrue("Instantiation of " + THIS.icParentBOName + " did not open its ioMainDS.icTableName", ;
                  USED(THIS.ioBOMgr.ioMainBO.ioMainDS.icTableName))     
ENDIF
THIS.AssertTrue("Instantiation of " + THIS.icChildBOName + " did not open its ioMainDS.icAlias", ;
                USED(THIS.ioChildBO.ioMainDS.icAlias))
IF NOT EMPTY(THIS.ioChildBO.ioMainDS.icTableName) ;
     AND INLIST(THIS.ioChildBO.ioMainDS.icDataSourceType,"FT","CT","LV","SPT_CT","SPT_FT")
  THIS.AssertTrue("Instantiation of " + THIS.icChildBOName + " did not open its ioMainDS.icTableName", ;
                  USED(THIS.ioChildBO.ioMainDS.icTableName))     
ENDIF

ENDPROC



*********************************************************************
PROCEDURE Test_FetchDataPlusUpdateAndSaveParentAndItsChildren
*********************************************************************
LOCAL loMsgSvc, llRetVal, luPOrigValue, luCOrigValue, lnChildren, ;
      luParentUpdateValue, lcParentUpdateField, luParentPK, ;
      luChildUpdateValue, lcChildUpdateField 

luParentPK = ???             &&& specify PK value of the Parent record to be retrieved (along with all its children)
lcParentUpdateField = "???"  &&& specify the name of a Parent field whose contents will be updated here 
luParentUpdateValue = ???    &&& lcParentUpdateField will be updated with this value (typically a VALID value)
lcChildUpdateField = "???"   &&& specify the name of a Child field whose contents will be updated here 
luChildUpdateValue = ???     &&& lcChildUpdateField will be updated with this value (typically a VALID value)

*
*  do the FetchData
*
llRetVal = THIS.ioBOMgr.FetchData(@m.loMsgSvc,.f.,m.luParentPK)
IF NOT m.llRetVal
  THIS.AssertTrue("BOMgr.FetchData() failed because:" + ;
                  CHR(13) + CHR(10) + ;
                  ALLTRIM(loMsgSvc.iaMessages[1].cOriginal) + ;
                  CHR(13) + CHR(10) + ;
                  TRANSFORM(loMsgSvc.iaMessages[1,2]) + ;
                  IIF(EMPTY(TRANSFORM(loMsgSvc.iaMessages[1,2])),SPACE(0),CHR(13)+CHR(10)) + ;
                  loMsgSvc.icDevText, ;
                  m.llRetVal)
ENDIF	

*
*  check the FetchData results
*
THIS.AssertTrue(THIS.icParentBOName + ".FetchData() succeeded, but did not RETURN one record", ;
                RECCOUNT(THIS.icParentAlias)=1)
THIS.AssertTrue(THIS.icChildBOName + ".FetchData() succeeded, but did not RETURN at least one record", ;
                RECCOUNT(THIS.icChildAlias)>0)
lnChildren = RECCOUNT(THIS.icChildAlias)
THIS.MessageOut(THIS.icChildBOName + ".FetchData() RETURNed " + TRANSFORM(RECCOUNT(THIS.icChildAlias)) + " records")

SELECT (THIS.icParentAlias)
THIS.MessageOut("Original value of " + m.lcParentUpdateField + " is: " + TRANSFORM(EVALUATE(m.lcParentUpdateField)))
SELECT (THIS.icChildAlias)
LOCATE
THIS.MessageOut("Original value of " + m.lcChildUpdateField + " is: " + TRANSFORM(EVALUATE(m.lcChildUpdateField)))

*
*  update the indicated data
*
SELECT (THIS.icParentAlias)
luPOrigValue = EVALUATE(m.lcParentUpdateField)
replace (m.lcParentUpdateField) WITH (m.luParentUpdateValue) 
SELECT (THIS.icChildAlias)
LOCATE 
luCOrigValue = EVALUATE(m.lcChildUpdateField)
replace (m.lcChildUpdateField) WITH (m.luChildUpdateValue)

*
*  save
*
llRetVal = THIS.ioBOMgr.SaveAction(@m.loMsgSvc)
IF NOT m.llRetVal
  THIS.AssertTrue("BOMgr.SaveAction() failed because:" + ;
                  CHR(13) + CHR(10) + ;
                  ALLTRIM(loMsgSvc.iaMessages[1].cOriginal) + ;
                  CHR(13) + CHR(10) + ;
                  TRANSFORM(loMsgSvc.iaMessages[1,2]) + ;
                  IIF(EMPTY(TRANSFORM(loMsgSvc.iaMessages[1,2])),SPACE(0),CHR(13)+CHR(10)) + ;
                  loMsgSvc.icDevText, ;
                  m.llRetVal)
ENDIF	

*
*  test that everything did in fact get saved properly
*  by re-Fetch-ing and checking the values
*
*  do the FetchData
*
llRetVal = THIS.ioBOMgr.FetchData(@m.loMsgSvc,.f.,m.luParentPK)
IF NOT m.llRetVal
  THIS.AssertTrue("BOMgr.FetchData() failed because:" + ;
                  CHR(13) + CHR(10) + ;
                  ALLTRIM(loMsgSvc.iaMessages[1].cOriginal) + ;
                  CHR(13) + CHR(10) + ;
                  TRANSFORM(loMsgSvc.iaMessages[1,2]) + ;
                  IIF(EMPTY(TRANSFORM(loMsgSvc.iaMessages[1,2])),SPACE(0),CHR(13)+CHR(10)) + ;
                  loMsgSvc.icDevText, ;
                  m.llRetVal)
ENDIF	
THIS.AssertTrue(THIS.icParentBOName + ".FetchData() succeeded, but did not RETURN one record", ;
                RECCOUNT(THIS.icParentAlias)=1)
THIS.AssertTrue(THIS.icChildBOName + ".FetchData() succeeded, but did not RETURN at least one record", ;
                RECCOUNT(THIS.icChildAlias)>0)
THIS.AssertEquals(THIS.icChildBOName + ".FetchData() succeeded, but did not RETURN the same number of records as it did on the original FetchData()", ; 
                  RECCOUNT(THIS.icChildAlias), ;
                  m.lnChildren)
THIS.MessageOut(THIS.icChildBOName + ".FetchData() RETURNed " + TRANSFORM(RECCOUNT(THIS.icChildAlias)) + " records")
*
*  check the updated values
*
SELECT (THIS.icParentAlias)
THIS.AssertTrue(THIS.icParentBOName + ".SaveAction() did not actually save the updated Parent value(s)", ;
                EVALUATE(THIS.icParentAlias+"."+m.lcParentUpdateField) = m.luParentUpdateValue)
THIS.MessageOut("Updated value of " + m.lcParentUpdateField + " is: " + TRANSFORM(EVALUATE(m.lcParentUpdateField)))
SELECT (THIS.icChildAlias)
LOCATE 
THIS.AssertTrue(THIS.icChildBOName + ".SaveAction() did not actually save the updated Child value(s)", ;
                EVALUATE(THIS.icChildAlias+"."+m.lcChildUpdateField) = m.luChildUpdateValue)
THIS.MessageOut("Updated value of " + m.lcChildUpdateField + " is: " + TRANSFORM(EVALUATE(m.lcChildUpdateField)))

*
*  put everything back the way it was, and save it
*
SELECT (THIS.icParentAlias)
replace (m.lcParentUpdateField) WITH (m.luPOrigValue)
SELECT (THIS.icChildAlias)
LOCATE 
replace (m.lcChildUpdateField) WITH (m.luCOrigValue)
llRetVal = THIS.ioBOMgr.SaveAction(@m.loMsgSvc)
IF NOT m.llRetVal
  THIS.AssertTrue("BOMgr.SaveAction() failed because:" + ;
                  CHR(13) + CHR(10) + ;
                  ALLTRIM(loMsgSvc.iaMessages[1].cOriginal) + ;
                  CHR(13) + CHR(10) + ;
                  TRANSFORM(loMsgSvc.iaMessages[1,2]) + ;
                  IIF(EMPTY(TRANSFORM(loMsgSvc.iaMessages[1,2])),SPACE(0),CHR(13)+CHR(10)) + ;
                  loMsgSvc.icDevText, ;
                  m.llRetVal)
ENDIF	

SELECT (THIS.icParentAlias)
THIS.MessageOut("Final value of " + m.lcParentUpdateField + " is: " + TRANSFORM(EVALUATE(m.lcParentUpdateField)))
SELECT (THIS.icChildAlias)
LOCATE
THIS.MessageOut("Final value of " + m.lcChildUpdateField + " is: " + TRANSFORM(EVALUATE(m.lcChildUpdateField)))

ENDPROC




**********************************************************************
ENDDEFINE
**********************************************************************



 