*
*  XXRQUERY.PRG
*  This program is a wrapper for the REQUERY() function,
*  providing for some error/busy connection checking, etc.
*  It also allows the user to abort the REQUERY() by 
*  pressing the {ESCAPE} key.
*
*  Copyright (c) 1997-2005 Visionpace   All Rights Reserved
*                17501 East 40 Hwy., Suite 218
*                Independence, MO 64055
*                816-350-7900 
*                http://www.visionpace.com
*                http://vmpdiscussion.visionpace.com
*  Author:  Drew Speedie
*           Special thanks to Mike Yearwood for inspiring the 
*             {ESCAPE}/abort feature
*
*
*  Return values:
*     0 - the REQUERY() failed (native VFP return value) --
*         the REQUERY() was never actually executed
*     1 - the REQUERY() succeeded (native VFP return value)
*    -1 - the REQUERY() failed, tuView is a remote view, and
*         something serious happened to the connection but
*         oApp.ForceShutdown() isn't being called here
*    -2 - the REQUERY() failed, tuView is a remote view, and
*         something serious happened to the connection and
*         oApp.ForceShutdown() is being called here
*    -3 - the REQUERY() was aborted by the user pressing {ESCAPE},
*         the REQUERY() was never actually executed
*
*
*  NOTE that you may decide not to call this .PRG directly in 
*  your intermediate/app-specific method/.PRG code -- you can 
*  create a "wrapper" class for this (and XXSQLEXE.PRG) to call
*  this .PRG, making it easier to subclass/override when 
*  necessary.
*
*
*  Note that, in some situations you may get a performance benefit 
*  by issuing 
*    SET DELETED OFF
*    SET OPTIMIZE OFF
*  before calling this routine and 
*    SET DELETED ON
*    SET OPTIMIZE ON
*  afterwards (we expect both SETtings to be ON for normal app 
*  execution) when the view being REQUERY()d here is:
*    1- a local parameterized view
*    2- the parameter value(s) yield NO records
*
*  Note that some features in this program depend on the 
*  existence of oLib/oLib.oConnectionSvc (instance of 
*  XXFW.VCX/cusConnectionSvc), and are suppressed if this is
*  not the case.  In  particular, this is desirable when this 
*  routine is called from a COM server, where all UI must be 
*  suppressed.
*
*
*  You may be wondering why this code isn't in a method
*  of an object like XXFW.VCX/cusConnectionSvc.
*  Mostly because oLib.oConnectionSvc resides in the
*  default data session #1, but this behavior might be
*  requested from anywhere, frequently from a private data
*  session form.  
*  There are at least 4 ways this code could have been written:
*    1- standalone .PRG program (which always participates in
*       the calling code's data session)
*    2- method of XXFWFRM.VCX/frmData
*    3- method of XXFW.VCX/cusConnectionSvc, make the 
*       developer add an instance of cusConnectionSvc to 
*       any private data session form that needs to call
*       this code
*    4- method of XXFW.VCX/cusConnectionSvc, receive a
*       parameter indicating a private data session number,
*       and issue an explicit SET DATASESSION TO that data
*       session
*  We've gone with #1 above, because 
*    1) we feel it's the most flexible approach
*    2) because we're not particularly keen on the
*       idea of explicit SET DATASESSION TO
*  #3 would be our second choice
*
*
*  See also XXSQLEXE.PRG
*
*
*  use this method instead of the native VFP REQUERY() when using 
*  remote views (you can also use this method for local views, but
*  none of the connection stuff happens)
*
*  this method has many similarities to XXSQLEXE.PRG because a VFP 
*  REQUERY() is an implicit/internal SQLExec()
*
*  lParameters
*    tuView (O) Alias/WorkArea of a currently-open view that is to 
*                 be REQUERY()d
*               Defaults to the current ALIAS()
*
*   taParms (O) Array containing parameter information if tcView 
*                 is parameterized.
*               If passed, must be done by REFERENCE, and must be 
*                 a 2-column array, one parameter per row.  
*                 Column1 contains the parameter name, and 
*                 Column2 contains the value for the parameter in 
*                 Column1.
*               Default/not passed: Any needed parameters are 
*                 assumed to be in scope/available at this time 
*                 (perhaps declared as PRIVATE memvars in the 
*                 calling code)
*
* tlAllowUserToRetryWhenBusy (0) Allow the user to keep trying to
*                                  execute the SQLExec() if the
*                                  tnHandle is busy?
*                                Defaults to the current  
*                                  oLib.oConnectionSvc.ilQueryAllowUserToRetryWhenBusy
*                                  setting, which comes from
*                                  the APPINFO table, which 
*                                  defaults to .F.
*                                IGNORED if tuView is a local view
*                                IGNORED if X6ISCOM()
*
*      tlPresentErrorMessage (O) If unsuccessful, present an
*                                  appropriate X3MSSVC dialog
*                                  to the user here?
*                                Defaults to the current  
*                                  oLib.oConnectionSvc.ilQueryPresentErrorMessage
*                                  setting, which comes from
*                                  the APPINFO table, which 
*                                  defaults to .F.
*                                Note that methods like
*                                  XXFW.VCX/frmData::SaveAction()
*                                  act on a failed TABLEUPDATE() or
*                                  failed SQLExec() with an overall
*                                  <Save>-level user message in 
*                                  THISFORM.SaveMessageOnFailure()
*                                IGNORED if tuView is a local view
*                                IGNORED if X6ISCOM()
*
*        tlShutdownIfProblem (O) Shutdown the application from
*                                  here if the tnHandle connection
*                                  is invalid/busy?
*                                Defaults to the current  
*                                  oLib.oConnectionSvc.ilQueryShutdownIfProblem
*                                  setting, which comes from
*                                  the APPINFO table, which 
*                                  defaults to .F.
*                                Note that methods like 
*                                  XXFW.VCX/frmData::SaveAction()
*                                  act on a failed TABLEUPDATE() or
*                                  failed SQLExec() by issuing
*                                  oApp.ForceShutdown() if that 
*                                  failure is due to a connection
*                                  problem
*                                IGNORED if tuView is a local view
*                                IGNORED if X6ISCOM()
*       

lParameters tuView, ;
            taParms, ;
            tlAllowUserToRetryWhenBusy, ;
            tlPresentErrorMessage, ;
            tlShutdownIfProblem

local lnParameters
lnParameters = pcount()

external array taParms

local lcView
DO CASE
  CASE vartype(tuView) = "C" AND !empty(tuView)
    lcView = upper(alltrim(tuView))
  CASE vartype(tuView) = "N" AND !empty(tuView)
    lcView = tuView
  OTHERWISE
    lcView = alias()
ENDCASE
IF !used(lcView)
  return 0
 ELSE
  *
  * make sure lcView is a character-based alias,
  * not passed as a work area
  *
  lcView = alias(lcView) 
ENDIF

local lnReturn, lcCDMText

IF INLIST(cursorgetprop("SourceType",lcView),1,101,201,3)
  *
  ***************************************************
  *  tuView is a local view, do the REQUERY()
  ***************************************************
  *
  *  ...or is an alias that is not a view at all, in 
  *  which case this will crash (just like issuing a 
  *  REQUERY() without calling this wrapper) because 
  *  you can only REQUERY() a view
  *
  *
  lnReturn = DoRequery(lcView,@taParms,.f.)

  *
  *  log this action, if we're in diagnostic mode
  *
  IF VARTYPE(m.oLib) = "O" AND oLib.DiagnosticMode()
    *
    *  MODIFY CLASS cusAppLibs OF XXFW METHOD DiagnosticMode
    *
    *  don't log anything here if this .PRG is called
    *  from a VMP n-Tier services object -- these
    *  methods call XXLOGGER() directly
    *
    LOCAL llLogIt, laStack[1]
    laStack[1] = ".FetchData "
    IF X2PSTACK(@laStack)  
      *
      *  XXSQLEXE() has been called from a VMP n-Tier 
      *  services method -- suppress the XXLOGGER() here, 
      *  because those methods handle logging there
      *
     ELSE
      *    XXDTES("XXFWNTDS.VCX","XXLOGGER(","cusDataSourceBehavior","SQLExecuteRemote")
      *
      LOCAL lcLogString
      IF lnReturn > 0
        lcLogString = "Successful REQUERY()"
       ELSE
        lcLogString = "Failed REQUERY()"
      ENDIF
      lcLogString = lcLogString + " of " + lcView + " from XXRQUERY():" 
*!*	      IF TYPE("ALEN(taParms,1)") = "N"
      IF NOT VARTYPE(taParms) = "U" AND X2IsArray(@taParms,1)
        lcLogString = lcLogString + ;
                      SPACE(2) + PADR("Parameter Name",24) + SPACE(2) + PADR("Value",50) + SPACE(2) + "DataType" + CHR(13) + CHR(10)
        LOCAL xx
        FOR xx = 1 TO ALEN(taParms,1)
          lcLogString = lcLogString + ;
                        SPACE(2) + ;
                        PADR(taParms[xx,1],24) + SPACE(2) + PADR(TRANSFORM(taParms[xx,2]),50) + SPACE(2) + VARTYPE(taParms[xx,2]) 
          IF xx = ALEN(taParms,1)
           ELSE
            lcLogString = lcLogString + CHR(13) + CHR(10)
          ENDIF
        ENDFOR 
      ENDIF   &&& we have parameters
      XXLOGGER(lcLogString)
    ENDIF
  ENDIF   &&& Diagnostic Mode

  RETURN lnReturn
ENDIF

*
*****************************************************
*  at this point we've determined that tuView/lcView
*  is a remote view (or SPT cursor)
*****************************************************
*

LOCAL lnHandle, llBusy, llInvalid, llHandleCheck, laStack[2]
lnReturn = 0
lnHandle = CURSORGETPROP("ConnectHandle",m.lcView)
laStack[1] = ".FetchData "
laStack[2] = ".SQLExecuteRemote "
IF X2PSTACK(@m.laStack,.f.)
  *
  *  this routine has been called by the VMP middle tier,
  *  here:
  *    XXDTES("XXFWNTDS.VCX","lnXXRQuery = XXRQUERY(THIS.PARENT.icAlias,","cusdsbehaviorview","FETCHDATA")
  *  and the FetchData() method has already 
  *  handled connection handle checking when it called
  *  the Connect()/GetHandle() method, which respect
  *  the connection object's ilCheckHandleOnEveryGet
  *  setting:  
  *    XXDTES("XXFWNTDS.VCX","IF NOT THIS.ilCheckHandleOnEveryGet","cusConnectionBehaviorODBC","GetHandle")
  *
  llHandleCheck = .f.
 ELSE
  *
  *  check the handle for validity
  *
  llHandleCheck = .t.
ENDIF


*
*
*  NOTE:  except for IF..ENDIF block of code that calls
*         DoRequery(), the rest of this
*         code is identical to the code in XXSQLEXE.PRG
*         that performs the same checks/messages/etc.
*         At some time in the future, some of this
*         common code may be split out into separate
*         methods.
*         Unfortunately, XXSQLEXEC receives *three*
*         parameters before the last 3 optional parameters,
*         whereas this .PRG receives *two* parameters
*         before the last 3 optional parameters, so the
*         individual CASE statements here check for
*         an lnParameters value that is *one less*
*         than the same CASE statements in XXSQLEXE
*         
*

*
*  process the optional behavior parameters
*
DO CASE
  ******************************************
  CASE NOT TYPE("oLib.oConnectionSvc.BaseClass") = "C"
  ******************************************
    *
    *  leave 'em all defaulted to whatever they were passed
    *  in as, or .F. if nothing passed
    *
  ******************************************
  CASE X6ISCOM()
  ******************************************
    *
    *  suppress all the UI stuff in this .PRG
    *
    *  note that NOT VARTYPE(m.oLib) = "O"
    *
    tlAllowUserToRetryWhenBusy = .f.
    tlPresentErrorMessage = .f.
    tlShutdownIfProblem = .f.
  ******************************************
  CASE m.lnParameters < 5
  ******************************************
    *
    *  none of them were passed
    *
    tlAllowUserToRetryWhenBusy = oLib.oConnectionSvc.ilQueryAllowUserToRetryWhenBusy
    tlPresentErrorMessage = oLib.oConnectionSvc.ilQueryPresentErrorMessage
    tlShutdownIfProblem = oLib.oConnectionSvc.ilQueryShutdownIfProblem
  ******************************************
  CASE m.lnParameters = 5
  ******************************************
    *
    *  tlAllowUserToRetryWhenBusy was passed
    *
    tlPresentErrorMessage = oLib.oConnectionSvc.ilQueryPresentErrorMessage
    tlShutdownIfProblem = oLib.oConnectionSvc.ilQueryShutdownIfProblem
  ******************************************
  CASE m.lnParameters = 6
  ******************************************
    *
    *  tlPresentErrorMessage was passed
    *
    tlShutdownIfProblem = oLib.oConnectionSvc.ilQueryShutdownIfProblem
  ******************************************
  OTHERWISE m.lnParameters > 6
  ******************************************
    *
    *  all 3 of them were passed
    *
ENDCASE  

DO CASE
  ****************************************************
  CASE NOT m.llHandleCheck
  ****************************************************
    *
    *  no handle check here, typically in an n-Tier
    *  environment, when this routine is called
    *  from cusDataSourceBehavior::SQLExecuteRemote()
    *
    llSuccess = .t.
  ****************************************************
  CASE TYPE("oAppInfo.CheckHandleOnEveryXXSQLEXE_XXRQUERY") = "N" ;
       AND oAppInfo.CheckHandleOnEveryXXSQLEXE_XXRQUERY = 0
  ****************************************************
    *
    *  no handle check here, typically in a 2-tier
    *  environment, when the AppInfo/"CheckHandleOnEveryXXSQLEXE_XXRQUERY"
    *  record is set to "0"/zero
    *
    *  XXDTES("XXFWDATA.PRG","CheckHandleOnEveryXXSQLEXE_XXRQUERY")
    *          
    llSuccess = .t.
  ****************************************************
  OTHERWISE    
  ****************************************************
    *
    *  see if lnHandle is valid 
    *
    IF TYPE("oLib.oConnectionSvc.BaseClass") = "C"    
      *
      *  MODIFY CLASS cusConnectionSvc OF XXFWLIBS METHOD ValidConnectHandle
      *
      llSuccess = oLib.oConnectionSvc.ValidConnectHandle(m.lnHandle)
      IF NOT m.llSuccess
        llInvalid = .t.
        ASSERT .f. message ;
             "When XXSQLEXE.PRG called oLib.oConnectionSvc.ValidConnectHandle(lnHandle), " + ;
             "it found handle/tnHandle " + TRANSFORM(m.lnHandle) + " " + ;
             "to be invalid."
      ENDIF
     ELSE
      llSuccess = .t.
    ENDIF
ENDCASE
  
*
*  now see if lnHandle is busy
*      
IF m.llSuccess
  DO WHILE .t.
    IF TYPE("oLib.oConnectionSvc.BaseClass") = "C"      
      *
      *  MODIFY CLASS cusConnectionSvc OF XXFWLIBS METHOD BusyConnection
      *
      llSuccess = NOT oLib.oConnectionSvc.BusyConnection(m.lnHandle)
    ENDIF
    DO CASE 
      CASE m.llSuccess
        EXIT 
      CASE NOT m.tlAllowUserToRetryWhenBusy
        *
        *  including X6ISCOM()
        *
        EXIT 
      CASE X3MSGSVC("Server busy, keep trying?") = "Y"
        *
        *  stay here and try again
        *
      OTHERWISE
        *
        *  user answered "NO" to the above X3MSGSVC()
        *
        EXIT 
    ENDCASE
  ENDDO
ENDIF

IF NOT m.llSuccess
  llBusy = .t.
ENDIF

IF llSuccess
  *
  *****************************************************
  *  do the REQUERY()
  *****************************************************
  *
  lnReturn = DoRequery(lcView,@taParms,.t.)
  llSuccess = lnReturn > 0

  *
  *  log this action, if we're in diagnostic mode
  *
  IF VARTYPE(m.oLib) = "O" AND oLib.DiagnosticMode()
    *
    *  MODIFY CLASS cusAppLibs OF XXFW METHOD DiagnosticMode
    *
    *  don't log anything here if this .PRG is called
    *  from a VMP n-Tier services object -- these
    *  methods call XXLOGGER() directly
    *
    LOCAL llLogIt, laStack[1]
    laStack[1] = ".FetchData "
    IF X2PSTACK(@laStack)  
      *
      *  XXSQLEXE() has been called from a VMP n-Tier services 
      *  method -- suppress the XXLOGGER() here, because
      *  those methods handle logging there
      *
     ELSE
      *    XXDTES("XXFWNTDS.VCX","XXLOGGER(","cusDataSourceBehavior","SQLExecuteRemote")
      *
      LOCAL lcLogString
      IF llSuccess
        lcLogString = "Successful REQUERY()"
       ELSE
        lcLogString = "Failed REQUERY()"
      ENDIF
      lcLogString = lcLogString + " of " + lcView + " from XXRQUERY():" 
*!*	      IF TYPE("ALEN(taParms,1)") = "N"
      IF VARTYPE(taParms) = "U" AND X2IsArray(@taParms)
        lcLogString = lcLogString + ;
                      SPACE(2) + "Parameters:" + CHR(13) + CHR(10)
        LOCAL xx
        FOR xx = 1 TO ALEN(taParms,1)
          lcLogString = lcLogString + ;
                        SPACE(4) + ;
                        taParms[xx,1] + SPACE(2) + TRANSFORM(taParms[xx,2]) + ;
                        CHR(13) + CHR(10)
        ENDFOR 
      ENDIF  
      XXLOGGER(lcLogString)
    ENDIF
  ENDIF   &&& Diagnostic Mode

ENDIF

IF llSuccess
  return lnReturn
ENDIF

local llSuppressShutdownHere
IF tlPresentErrorMessage
  *
  *  the error can be one of several categories:
  *  - tnHandle was invalid
  *  - tnHandle was busy
  *  - the REQUERY() failed 
  *
  IF llInvalid OR llBusy
   ELSE  
    local array laError[1]
    aerror(laError)
  ENDIF
  DO CASE
    ***************************************************
    CASE llInvalid 
    ***************************************************
      *
      *  tnHandle was invalid 
      *
      IF tlShutdownIfProblem AND vartype(oApp) = "O" AND !llSuppressShutdownHere
        x3msgsvc("Unable to access server -- invalid connection, shutdown")
       ELSE  
        x3msgsvc("Unable to access server - invalid connection")
      ENDIF
    ***************************************************
    CASE llBusy
    ***************************************************
      *
      *  tnHandle was busy, exceeding either 
      *  - the limits set by THIS.BusyConnection()
      *  - the patience of the user
      *
      IF tlShutdownIfProblem AND vartype(oApp) = "O" AND !llSuppressShutdownHere
        x3msgsvc("Unable to access server -- busy connection, shutdown")
       ELSE
        x3msgsvc("Unable to access server - busy connection")
      ENDIF
    ***************************************************
    CASE laError[1] = 1526
    ***************************************************
      *
      *  the SQLExec() failed due to a problem
      *  executing tcSQL on the back end, generating
      *  an ODBC error #1526
      *
      IF TYPE("oLib.oConnectionSvc.BaseClass") = "C" ;
           AND oLib.oConnectionSvc.ODBCErrorMessage(@laError)
        *
        *  if oLib.oConnectionSvc.ODBCErrorMessage()
        *  RETURNs .T., assume you've already
        *  issued the oApp.ForceShutdown() if necessary
        *
        llSuppressShutdownHere = .t.
       ELSE
        *
        * handle here if ODBCErrorMessage() returns .f.
        *
        IF tlShutdownIfProblem AND vartype(oApp) = "O" AND !llSuppressShutdownHere
          x3msgsvc("Unable to access server -- ODBC, shutdown")        
         ELSE
          x3msgsvc("Unable to access server - ODBC")        
        ENDIF
        IF (vartype(oApp) = "O" AND oApp.InDevelopment()) ;
            OR ;
           (vartype(oLib) = "O" AND oLib.DevMode())
          *
          *  in development
          *
          local lcString
          lcString = ;
          "Unable to access data on the server because of an ODBC problem.  " + ;
          "Here is the ODBC error information from AERROR():" + chr(13) + ;
          "AERROR[1]: " + transform(laError[1]) + chr(13) + ;
          "AERROR[2]: " + transform(laError[2]) + chr(13) + ;
          "AERROR[3] (ODBC error message): " + transform(laError[3]) + chr(13) + ;
          "AERROR[4] (current ODBC SQL state): " + transform(laError[4]) + chr(13) + ;
          "AERROR[5] (error number from ODBC data source): " + transform(laError[5]) + chr(13) + ;
          "AERROR[6] (ODBC connection handle): " + transform(laError[6]) 
          X8SETCLS("XXFWUTIL.VCX")
          CREATEOBJECT("cusDevMessage",lcString)
        ENDIF &&& in development
      ENDIF
    ***************************************************
    OTHERWISE
    ***************************************************
      *
      *  let the error handler take over
      *
      error (laError[1])
  ENDCASE
ENDIF

IF tlShutdownIfProblem AND vartype(oApp) = "O" AND !llSuppressShutdownHere
  oApp.ForceShutdown()
  return -2
ENDIF

return -1



PROCEDURE DoRequery
*
*  do the actual REQUERY(), including enabling an {ESCAPE} 
*  keypress that allows the user to abort the query
*
LPARAMETERS tcView, taParameters, tlRemote

LOCAL lnRetVal, llRemote
IF VARTYPE(tlRemote) = "L" 
  llRemote = tlRemote
 ELSE
  llRemote = .f.
ENDIF

*
*  intialize any passed parameters list
*
IF TYPE("taParameters[1]") = "C" AND NOT EMPTY(taParameters[1])
  LOCAL yy
  FOR yy = 1 TO ALEN(taParameters,1)
    LOCAL (taParameters[yy,1])                           &&& declare the variable in taParms[yy,1]
    STORE (taParameters[yy,2]) TO (taParameters[yy,1])   &&& store the variable whose name is in taParms[yy,1] with the value in taParms[yy,2]
  ENDFOR
ENDIF

IF X6ISCOM()
  *
  *  no UI for COM objects, and no COM support for SET ESCAPE
  *
  TRY
  lnRetVal = REQUERY(tcView)
  CATCH
  lnRetVal = -1
  ENDTRY
  RETURN lnRetVal
ENDIF

*
*  setup a temporary ON ESCAPE/SET ESCAPE so pressing {ESCAPE} 
*  will have an effect
*
LOCAL loOnEscape, llQueryStopped, loSetEscape
IF NOT "\XXFWUTIL.VCX" $ SET("CLASSLIB")
  SET CLASSLIB TO XXFWUTIL.VCX ADDITIVE 
ENDIF
loOnEscape = createobject("cusPushPopOn","ESCAPE")
loSetEscape = createobject("cusPushPopSetOnOff","ESCAPE")
ON ESCAPE llQueryStopped = .T.
SET ESCAPE ON 
IF "XXERROR" $ ON("ERROR") OR NOT VARTYPE(gnErrorNumber) = "U"
  *
  *  clear out gnErrorNumber -- any time you need to check 
  *  gnErrorNumber subsequent to a command to see if XXERROR.PRG 
  *  was fired and set gnErrorNumber to a VFP error, you first 
  *  have to "clear it out" so it doesn't contain a value from 
  *  a previous call to XXERROR.PRG that set lcAction = "NOTHING"
  *    XXDTES("XXERROR.PRG","gnErrorNumber = 0")
  *
  gnErrorNumber = 0
 ELSE
  *
  *  the current ON ERROR is not set to XXERROR, so we do not 
  *  want to do anything about gnErrorNumber, which likely does 
  *  not exist at this point in time
  * 
ENDIF

LOCAL lcTempError, lcOnError, loSQLThermo, lnSQLThermo

*
*  direct informational output to the native VFP "thermometer" 
*  status bar -- when the query is a little on the lengthy side, 
*  give the native VFP progress bar, if the user decides he/she 
*  doesn't want to wait for the indicated time, they can hit 
*  {ESCAPE} to abort the REQUERY()
*
lnSQLThermo = XXAPPINF("SQLThermoXXRQUERYBehavior")
IF ISNULL(lnSQLThermo) OR NOT VARTYPE(lnSQLThermo) = "N" OR lnSQLThermo < 0
  lnSQLThermo = 1  &&& default
ENDIF
IF lnSQLThermo = 1   &&& default - set to 0 in AppInfo to suppress
  *
  *  MODIFY CLASS cusPushPopSQLThermo OF XXFWUTIL.VCX METHOD Init
  *
  loSQLThermo = CREATEOBJECT("cusPushPopSQLThermo") 
ENDIF

lcOnError = ON("ERROR")
IF llRemote 
  *
  *  modify the current ON ERROR so we can send just ERROR 1839 
  *  errors to the VMP error handler XXERROR.PRG, while 
  *  suppressing all other errors, which get handled in the 
  *  IF tlPresentErrorMessage ..ENDIF block in the main body 
  *  of this .PRG
  *
  IF EMPTY(lcOnError)
    *
    *  no error handler in effect, like when running forms 
    *  standalone at the Command Window -- simulate the behavior 
    *  in a VMP app of letting the user {ESCAPE} to terminate 
    *  the query instead of generating the 1839 error normally
    *
*!*	    lcTempError = "gnErrorNumber = 1839"
    lcTempError = "DO XXRQUERY_TempOnError WITH ERROR(), MESSAGE()"
   ELSE
    lcTempError = UPPER(ALLTRIM(ON("ERROR")))
  ENDIF
*!*	  IF lcTempError = "DO "   &&& 8/31/04 DWS commented out this line, and replaced it with the equivalent line below, which solves a strange problem reported by a VMP developer
  IF UPPER(LEFTC(lcTempError,3)) = "DO "  
    *
    *  convert to a function so we can put it inside an IIF() below
    *
    lcTempError = SUBSTRC(lcTempError,4)  &&& remove the "DO "
    lcTempError = STRTRAN(lcTempError," WITH ","(")
    lcTempError = lcTempError + ")"
  ENDIF
  PUBLIC junk
  junk = 0
  lcTempError = "IIF(ERROR()=1839," + lcTempError + ",junk=1)"  
  ON ERROR &lcTempError
 ELSE   &&& llRemote = .f.
  IF EMPTY(lcOnError)
    *
    *  no error handler in effect, like when running forms 
    *  standalone at the Command Window -- simulate the behavior 
    *  in a VMP app of letting the user {ESCAPE} to terminate 
    *  the query instead of generating the 1839 error normally
    *
*!*	    lcTempError = "gnErrorNumber = 1839"
    lcTempError = "DO XXRQUERY_TempOnError WITH ERROR(), MESSAGE()"
    ON ERROR &lcTempError
   ELSE
    *
    *  do nothing, let the installed error handler take care of 
    *  things, which should ultimately set gnErrorNumber = 1839
    *
  ENDIF
ENDIF
*
*  do the REQUERY()
*
lnRetVal = REQUERY(tcView)
ON ERROR &lcOnError
RELEASE junk
RELEASE loSetEscape
RELEASE loOnEscape
IF VARTYPE(lnRetVal) = "L" ;
     AND VARTYPE(gnErrorNumber) = "N" AND gnErrorNumber = 1839
  *
  *  if the user stopped the query by pressing {ESCAPE}, VFP has 
  *  fired error 1839 "SQL operation cancelled by the user", 
  *  XXERROR() has been invoked, which set gnErrorNumber to 1839
  *
  *  also, because an error occurred before the REQUERY() 
  *  completed, lnRetVal has not been set to a numeric value 
  *  (indicating the number of rows RETURNed by the REQUERY()
  *  call)
  *
  lnRetVal = -3
ENDIF
IF VARTYPE(lnRetVal) = "L"
  *
  *  in addition to the above IF..ENDIF, this also happens when 
  *  the REQUERY() itself fails... i.e. due to a bug/syntax 
  *  error/etc. in the view definition itself, dirty buffer, etc.
  *
  lnRetVal = 0
ENDIF

RETURN lnRetVal




PROCEDURE XXRQUERY_TempOnError
LPARAMETERS tnError, tcMessage
gnErrorNumber = tnError
IF NOT tnError = 1839
  *
  *  tell the developer about it (cusDevMessage does not show
  *  the message if we're not in development mode or x6iscom(),
  *  in which case we can't have any UI) 
  *
  LOCAL lcCDMText
  lcCDMText = "The REQUERY() in XXRQUERY() failed because:" + CHR(13) + CHR(13) + ;
              "Error " + TRANSFORM(tnError) + "/" + ALLTRIM(tcMessage)
  X8SETCLS("XXFWUTIL.VCX")
  CREATEOBJECT("cusDevMessage",lcCDMText)
ENDIF

RETURN
