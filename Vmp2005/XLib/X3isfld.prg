*
*  X3ISFLD.PRG
*  Returns the logical value indicating whether or not the 
*  passed field name is contained in: 
*      the currently-selected alias
*      the passed alias (which must be open)
*
*  Copyright (c) 1997-2005 Visionpace   All Rights Reserved
*                17501 East 40 Hwy., Suite 218
*                Independence, MO 64055
*                816-350-7900 
*                http://www.visionpace.com
*                http://vmpdiscussion.visionpace.com
*  Author:  Drew Speedie
*
*  Usage:
*    local llValidField
*    llValidField = x3isfld("CUSTOMERS.Cus_Name")
*    select CUSTOMERS
*    llValidField = x3isfld("Cus_Name")
*
*  Parameters
*    tcString (R) Name of the field to check.  May
*                 be in the format 
*                   FieldName -- ALIAS() is assumed
*                   Alias.FieldName -- Alias must be USED()
*
LPARAMETERS tcString
LOCAL llRetval, lcAlias, lcFieldName
DO CASE
  CASE OCCURS(".",tcString) = 1
    lcAlias = LEFTC(tcString,AT_C(".",tcString)-1)
    lcFieldName = SUBSTRC(tcString,AT_C(".",tcString)+1)
  CASE NOT EMPTY(ALIAS())
    lcAlias = ALIAS()
    lcFieldName = tcString
  OTHERWISE 
    lcAlias = SPACE(0)
    lcFieldName = SPACE(0)
ENDCASE
IF EMPTY(lcAlias) OR NOT USED(lcAlias)
  RETURN .f.
ENDIF
IF NOT TYPE(lcAlias+"."+lcFieldName) = "U"
  llRetVal = .t.
ENDIF
RETURN llRetVal
