**  Procedure.........: LBXConA4.PRG
**  Description.......: to convert all the .lbx labels in the
**                      current directory from the American
**                      standard format to the British A4 standard
**  Dependencies......: *.lbx in \vm sub-directory
**  Calling Samples...: do LBXConA4
**  Note that it is unlikely that this will work on actual labels as
**  the standard sizes seem to differ between A4 and American.
**  It does however allow you to print them on plain A4 paper <bg>.
lcOldAli = ALIAS()
SELECT 0
=ADIR( arLbx, '*.LBX')
FOR li = 1 TO ALEN( arLbx, 1)  STEP 1
  USE ( arLbx[ li, 1])
  ** change the setting in the first record from Letter to A4
  REPLACE Expr WITH SUBSTR( Expr, 1, AT( "PAPERSIZE", Expr) - 2) + ;
   "PAPERSIZE=9" + SUBSTR( Expr, AT( "PAPERSIZE", Expr) + 12), ;
   Width WITH 39994
  GO 14
  IF FOUND()
    REPLACE Width WITH 8
  ENDIF
  GO 15
  IF FOUND()
    REPLACE Width WITH 14
  ENDIF
  USE
ENDFOR
IF ! EMPTY( lcOldAli)
  SELECT lcOldAli
ENDIF  
RETURN  
 