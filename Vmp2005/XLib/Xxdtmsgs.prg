*
*  XXDTMSGS.PRG
*  DeveloperTool:  Fire up an instance of 
*  XXTOOLS.VCX/frmXXDTMSGS to maintain a MSGSVC.DBF table
*
*  Copyright (c) 1997-2005 Visionpace   All Rights Reserved
*                17501 East 40 Hwy., Suite 218
*                Independence, MO 64055
*                816-350-7900 
*                http://www.visionpace.com
*                http://vmpdiscussion.visionpace.com
*  Author:  Art Bergquist and Drew Speedie
*

*
*  note that this developer tool can be called multiple
*  times, typically to allow you to load up the 
*  \XLIB\MSGS\MSGSVC.DBF and an app-specific MSGSVC.DBF 
*  (or 2 different app-specific MSGSVC.DBF tables) at 
*  the same time and review them side by side
*

LOCAL laUsed[1], lnUsed
IF AUSED(m.laUsed) > 0
  FOR lnUsed = 1 TO ALEN(m.laUsed,1)
    IF UPPER(ALLTRIM(JUSTFNAME(DBF(m.laUsed[m.lnUsed])))) == "MSGSVC.DBF" ;
         AND ISEXCLUSIVE(m.laUsed[m.lnUsed],1)
      *
      *  close any MSGSVC.DBF that is currently open
      *  for EXCLUSIVE use -- note that we're only doing
      *  this in data session #1, on the assumption 
      *  that we shouldn't be mucking with any private
      *  data sessions, which we don't anticipate
      *  having the MSGSVC.DBF open EXCLUSIVE anyway
      *
      USE IN (m.laUsed[m.lnUsed])
      MESSAGEBOX("XXDTMSGS.PRG has just closed the " + m.laUsed[m.lnUsed] + ;
                 " cursor open in the default data session because it was " + ;
                 "open for EXCLUSIVE use, therefore likely " + ;
                 "interfering with the XXDTMSGS form interface.", ;
                 48, ;
                 "Please Note")                 
    ENDIF
  ENDFOR
ENDIF

*!*	IF TYPE("ALEN(gaXXDTMSGS)") = "N" 
IF NOT VARTYPE(m.gaXXDTMSGS) = "U" AND X2IsArray(@gaXXDTMSGS)
  RELEASE ALL EXCEPT gaXXDTMSGS 
  LOCAL xx, lnCount
  FOR xx = ALEN(gaXXDTMSGS,1) TO 1 STEP -1
    IF NOT VARTYPE(gaXXDTMSGS[m.xx]) = "O" ;
         AND ALEN(gaXXDTMSGS,1) > 1
      ADEL(gaXXDTMSGS,m.xx)
    ENDIF
  ENDFOR
  DO CASE
    CASE ALEN(gaXXDTMSGS,1) = 1 AND VARTYPE(gaXXDTMSGS[1]) = "O"
    CASE ALEN(gaXXDTMSGS,1) = 1 AND NOT VARTYPE(gaXXDTMSGS[1]) = "O"
      gaXXDTMSGS[1] = .f.
    OTHERWISE
      DIMENSION gaXXDTMSGS[ALEN(gaXXDTMSGS,1)+1]
  ENDCASE
 ELSE
  RELEASE ALL 
  PUBLIC gaXXDTMSGS[1]
ENDIF 

*
*  now instantiate frmXXDTMSGS
*
SET CLASSLIB TO XXTOOLS.VCX ADDITIVE  
gaXXDTMSGS[ALEN(gaXXDTMSGS,1)] = CREATEOBJECT("frmXXDTMSGS")
IF VARTYPE(gaXXDTMSGS[alen(gaXXDTMSGS,1)]) = "O"
  gaXXDTMSGS[ALEN(gaXXDTMSGS,1)].Show()
ENDIF

RETURN 




