*
*  X3LCKSCR.PRG
*  If _Screen had a LockScreen property that actually
*  worked, it would do what this routine does --
*  lock/unlock the video for the entire VFP _Screen.
*
*  Copyright (c) 1997-2005 Visionpace   All Rights Reserved
*                17501 East 40 Hwy., Suite 218
*                Independence, MO 64055
*                816-350-7900 
*                http://www.visionpace.com
*                http://vmpdiscussion.visionpace.com
*  Author:  Drew Speedie
*
*  Works in Win95, NT4, 2000, XP, although it's quite a bit 
*  slower in NT4 (NT4 Server, at least...)
*
*  Here are a couple of situations where you might
*  want to put this routine to use:
*  1- when THISFORM.LockScreen doesn't quite keep things
*     locked enough, like when you issue a SetFocus()
*     while THISFORM.LockScreen has been set to .F.
*  2- when you want to perform any other video updates
*     under the cover of a LockScreen that applies to 
*     the entire VFP _Screen, perhaps spanning multiple
*     VFP forms, etc.
*
*  USAGE:
*  X3LCKSCR(.t.)   &&& lock _Screen video here
*  *  do processing here
*  X3LCKSCR()      &&& unlock _Screen video here
*
*  
*  lParameters
*    tlLock (O) Pass as .T. to lock the _Screen video
*               Default/not passed, same as passing .F.
*
LPARAMETERS tlLock

IF _Screen.Visible = .f.
  *
  *  you've called this program during application
  *  startup while SCREEN=OFF in the CONFIG.FPW
  *  is in effect OR while a Top-Level form application
  *  is running -- no point wasting the time...
  *
  RETURN 
ENDIF

LOCAL laDLLs[1], lnHandle, lnDesktopWin, llDeclared

IF TYPE("tlLock") = "L" AND NOT ISNULL(tlLock) AND tlLock
  *
  *  lock _Screen
  *
  #IF TYPE("_VFP") = "O" AND VERSION(5) >= 700  
    IF FILE("X7ISAPIF.PRG") OR FILE("X7ISAPIF.FXP")
      LOCAL lcJunk
      *  refer to X7ISAPIF.PRG via macro substitution
      *  to prevent the Project Manager from complaining
      *  when X7ISAPIF.PRG is not available
      lcJunk = [llDeclared = X7ISAPIF("GetDesktopWindow") AND X7ISAPIF("LockWindowUpdate")]
      &lcJunk
    ENDIF
  #ENDIF
  IF NOT llDeclared
    DECLARE integer GetDesktopWindow IN Win32API
    DECLARE integer LockWindowUpdate IN Win32API integer lnHandle
  ENDIF
  lnDesktopWin = GetDesktopWindow()
  LockWindowUpdate(lnDesktopWin)
 ELSE
  *
  *  unlock _Screen
  *
  #IF TYPE("_VFP") = "O" AND VERSION(5) >= 700
    IF FILE("X7ISAPIF.PRG") OR FILE("X7ISAPIF.FXP")
      LOCAL lcJunk
      *  refer to X7ISAPIF.PRG via macro substitution
      *  to prevent the Project Manager from complaining
      *  when X7ISAPIF.PRG is not available
      lcJunk = [llDeclared = X7ISAPIF("LockWindowUpdate")]
      &lcJunk.
    ENDIF
  #ENDIF
  IF NOT llDeclared
    DECLARE integer LockWindowUpdate IN Win32API integer lnHandle
  ENDIF
  LockWindowUpdate(0)
ENDIF

RETURN
