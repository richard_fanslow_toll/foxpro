*
*  VMP base Header class SuperClass
*
*  changes you make here are inherited by ALL
*  your intermediate and app-specific column
*  classes
*
*  this is the parent class of the grhBase header
*  class in XXFWLEAF.PRG
*    XXDTES("XXFWLEAF.PRG","DEFINE CLASS grhBase")
*
*  see the zReadMe of any of the visual VMP super
*  classes:
*    MODIFY CLASS txtVMPSuperClass OF XXXXtxt.VCX METHOD zReadMe
*
DEFINE CLASS grhVMPSuperClass AS Header
ENDDEFINE
