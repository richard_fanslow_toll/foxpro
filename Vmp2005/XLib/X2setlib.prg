*
*  X2SETLIB.PRG
*  SET LIBRARY TO the passed library 
*
*  RETURNs a logical value indicating success
*
*
*  Copyright (c) 1997-2005 Visionpace   All Rights Reserved
*                17501 East 40 Hwy., Suite 218
*                Independence, MO 64055
*                816-350-7900 
*                http://www.visionpace.com
*                http://vmpdiscussion.visionpace.com
*  Author:  Tom DeMay and Drew Speedie
*
*  Usage:
*    IF x2setlib("FOXTOOLS")
*      * FOXTOOLS.FLL was successfully installed
*     ELSE
*      * FOXTOOLS.FLL could not be successfully installed
*      do x3msgsvc with "No FOXTOOLS.FLL"
*      * or:
*      * do x3winmsg with "Cannot continue without FoxTools.fll"
*      quit
*    ENDIF
*    
*  Parameters
*     tcLibrary (R) Name of library (.FLL/.PLB) to SET LIBRARY TO
*                   NOTE:  if you only pass the filename, no extension
*                          the ".FLL" or ".PLB" extension is assumed,
*                          based on the value of _DOS/_WINDOWS      
*
* 
parameters tcLibrary
private pcOnError, plSuccess

IF type("tcLibrary") = "C" AND !empty(tcLibrary)
  tcLibrary = upper(alltrim(tcLibrary))
  IF "." $ tcLibrary
   ELSE
    IF _DOS
      tcLibrary = tcLibrary + ".PLB"
     ELSE
      tcLibrary = tcLibrary + ".FLL"
    ENDIF
  ENDIF
 ELSE
  return .f.
ENDIF

IF tcLibrary $ set("LIBRARY")
  * tcLibrary is already loaded, no need to continue
  return .t.
ENDIF

pcOnError = on("ERROR")
plSuccess = .t.
on error plSuccess = .f.

DO CASE
  *****************************************
  CASE file(tcLibrary)
  *****************************************
    set library to (tcLibrary) additive  
  *****************************************
  CASE file(sys(2004)+ tcLibrary)
  *****************************************
    *  tcLibrary is in the directory where
    *  FoxPro is installed (for an .EXE-
    *  distributed app in Visual FoxPro, 
    *  this is where VFP5.DLL/VFP6ENU.DLL
    *  are located)
    set library to (sys(2004) + tcLibrary) additive
  *****************************************
  CASE file(LEFT(SYS(16,0),RAT("\",SYS(16,0))) + tcLibrary)
  *****************************************
    *  an .EXE is running in VFP, and the library
    *  is located in the directory where the
    *  .EXE is located 
    set library to (LEFT(SYS(16,0),RAT("\",SYS(16,0))) + tcLibrary) additive
  *****************************************
  OTHERWISE
  *****************************************
    *  assume the library is in the path
    set library to (tcLibrary) additive
ENDCASE

on error &pcOnError

return plSuccess

	
