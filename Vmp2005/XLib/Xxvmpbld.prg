*
*  XXVMPBLD.PRG
*  Returns the VMP build date by parsing it 
*  from the header comments in XXFWMAIN.PRG
*
*  THIS ROUTINE IS OBSOLETE, NOT USED BY VMP AND
*  ONLY INCLUDED FOR BACKWARD COMPATIBILITY, IN CASE
*  YOU WERE USING IT!
*  This routine has been replaced by XXVMPBuild.PRG
*
*  Copyright (c) 2000-2004 Visionpace    All Rights Reserved
*                17501 East 40 Hwy., Suite 218
*                Independence, MO 64055
*                816-350-7900 
*                http://www.visionpace.com
*                http://vmpdiscussion.visionpace.com
*
*  Author:  Drew Speedie
*
*  If XXFWMAIN.PRG cannot be found, or the build date 
*  cannot be otherwise found in XXFWMAIN.PRG, this routine
*  returns a .NULL. date 
*
LOCAL lcBuildDate, ldBuildDate 
lcBuildDate = SPACE(0)
ldBuildDate = DATE()
ldBuildDate = .NULL.

IF X2PSTACK("XXFWMAIN")
  *
  *  running a distributed VMP application (.APP/.EXE)
  *
  lcBuildDate = GetVMPBuildDate()
 ELSE
  *
  *  running a VMP application as source code, 
  *  calling this .PRG from the Command Window, etc.
  *
  lcBuildDate = ALLTRIM( ;
                SUBSTRC(FILETOSTR("XXFWMAIN.PRG"), ;
                        AT_C("#DEFINE ccVMPBuildDate",FILETOSTR("XXFWMAIN.PRG"))+23, ;
                        40) ;
                        )
  lcBuildDate = CHRTRAN(lcBuildDate,["],SPACE(0))
  lcBuildDate = CHRTRAN(lcBuildDate,CHR(13),SPACE(0))
  lcBuildDate = CHRTRAN(lcBuildDate,CHR(10),SPACE(0))
  lcBuildDate = GETWORDNUM(lcBuildDate,1)
ENDIF
IF EMPTY(lcBuildDate) 
  lcBuildDate = "(unknown)"
  ldBuildDate = .NULL.
 ELSE
  ldBuildDate = DATE(VAL(SUBSTRC(lcBuildDate,7,4)), ;
                VAL(LEFTC(lcBuildDate,2)), ;
                VAL(SUBSTRC(lcBuildDate,4,2)))
  lcBuildDate = TRANSFORM(ldBuildDate)
ENDIF

RETURN ldBuildDate



*!*	*
*!*	*  XXVMPBLD.PRG
*!*	*  Returns the VMP build date by parsing it 
*!*	*  from the header comments in XXFWMAIN.PRG
*!*	*
*!*	*  Copyright (c) 2000-2005 Visionpace    All Rights Reserved
*!*	*                17501 East 40 Hwy., Suite 218
*!*	*                Independence, MO 64055
*!*	*                816-350-7900 
*!*	*                http://www.visionpace.com
*!*	*                http://vmpdiscussion.visionpace.com
*!*	*
*!*	*  Author:  Drew Speedie
*!*	*
*!*	*  If XXFWMAIN.PRG cannot be found, or the build date 
*!*	*  cannot be otherwise found in XXFWMAIN.PRG, this routine
*!*	*  returns a .NULL. date 
*!*	*
*!*	*  The RETURN value respects the current SET DATE setting,
*!*	*  for those tnVerInfo values that include the build date/
*!*	*  refresh date 
*!*	*
*!*	*  lParameters 
*!*	*    tnVerInfo (O) Specifies what VMP build information is 
*!*	*                    RETURNed (defaults to 0):
*!*	*                  0- VMP build date, as specified in the
*!*	*                       #DEFINE ccVMPBuildDate line of
*!*	*                       XXFWMAIN.PRG.  
*!*	*                     This value is a Date data type.
*!*	*                  1- VMP build date as a character string.
*!*	*                  2- Refresh date, if any, as specified in 
*!*	*                       the #DEFINE ccVMPRefreshDate line 
*!*	*                       of XXFWMAIN.PRG
*!*	*                     This value is a Date data type.
*!*	*                     If there is no Refresh date, a blank
*!*	*                       date is RETURNed.
*!*	*                  3- Refresh date as a character string.
*!*	*                  4- Refresh suffix ("a", "b", "c", etc.),
*!*	*                       if any.
*!*	*                     This value is a Character data type.
*!*	*                     If there is no Refresh date, an empty
*!*	*                       string is RETURNed.
*!*	*                  5- Refresh date + Refresh suffix (if any):
*!*	*                       RefreshDate<Suffix>
*!*	*                     This value is a Character data type.
*!*	*                     If there is no Refresh date, an empty
*!*	*                       string is RETURNed.
*!*	*                  6- VMP build date plus the date the
*!*	*                       the build was refreshed, plus 
*!*	*                       the "a/b/c/etc." suffix (if any):
*!*	*                         BuildDate/RefreshDate<Suffix>
*!*	*                     This value is a Character data type.
*!*	*
*!*	LPARAMETERS tnVerInfo as Integer
*!*	LOCAL lcBuildDate, lcRefreshDate, luRetVal, ;
*!*	      ldBuildDate, ldRefreshDate, lcRefreshSuffix
*!*	lcBuildDate = SPACE(0)
*!*	lcRefreshDate = SPACE(0)
*!*	lcRefreshSuffix = SPACE(0)
*!*	ldBuildDate = DATE()
*!*	ldBuildDate = .NULL.
*!*	ldRefreshDate = ldBuildDate

*!*	DO CASE
*!*	  ***********************************************
*!*	  CASE X2PSTACK("XXFWMAIN")
*!*	  ***********************************************
*!*	    *
*!*	    *  running a distributed VMP application (.APP/.EXE)
*!*	    *
*!*	    lcBuildDate = GetVMPBuildDate()
*!*	    lcRefreshDate = GetVMPRefreshDate()
*!*	  ***********************************************
*!*	  CASE FILE("XXFWMAIN.PRG")   
*!*	  ***********************************************
*!*	    *
*!*	    *  running a VMP application as source code, 
*!*	    *  calling this .PRG from the Command Window, etc.
*!*	    *
*!*	    lcBuildDate = ALLTRIM( ;
*!*	                  SUBSTRC(FILETOSTR("XXFWMAIN.PRG"), ;
*!*	                          AT_C("#DEFINE ccVMPBuildDate",FILETOSTR("XXFWMAIN.PRG"))+23, ;
*!*	                          40) ;
*!*	                          )
*!*	    lcBuildDate = CHRTRAN(lcBuildDate,["],SPACE(0))
*!*	    lcBuildDate = CHRTRAN(lcBuildDate,CHR(13),SPACE(0))
*!*	    lcBuildDate = CHRTRAN(lcBuildDate,CHR(10),SPACE(0))
*!*	    lcBuildDate = GETWORDNUM(lcBuildDate,1)
*!*	    lcRefreshDate = ALLTRIM( ;
*!*	                    SUBSTRC(FILETOSTR("XXFWMAIN.PRG"), ;
*!*	                            AT_C("#DEFINE ccVMPRefreshDate",FILETOSTR("XXFWMAIN.PRG"))+25, ;
*!*	                            40) ;
*!*	                            )
*!*	ENDCASE
*!*	IF EMPTY(lcBuildDate) 
*!*	  lcBuildDate = "(unknown)"
*!*	  ldBuildDate = .NULL.
*!*	 ELSE
*!*	  ldBuildDate = DATE(VAL(SUBSTRC(lcBuildDate,7,4)), ;
*!*	                VAL(LEFTC(lcBuildDate,2)), ;
*!*	                VAL(SUBSTRC(lcBuildDate,4,2)))
*!*	  lcBuildDate = TRANSFORM(ldBuildDate)
*!*	ENDIF
*!*	IF ISNULL(lcRefreshDate)
*!*	  lcRefreshDate = "(unknown)"
*!*	  ldRefreshDate = .NULL.
*!*	 ELSE
*!*	  lcRefreshDate = CHRTRAN(lcRefreshDate,["],SPACE(0))
*!*	  lcRefreshDate = CHRTRAN(lcRefreshDate,CHR(13),SPACE(0))
*!*	  lcRefreshDate = CHRTRAN(lcRefreshDate,CHR(10),SPACE(0))
*!*	  lcRefreshDate = ALLTRIM(LEFTC(lcRefreshDate,AT_C("*",lcRefreshDate)-1))
*!*	  IF EMPTY(lcRefreshDate)
*!*	    lcRefreshDate = "(none)"
*!*	    ldRefreshDate = {}
*!*	   ELSE
*!*	    IF ISALPHA(RIGHTC(lcRefreshDate,1))
*!*	      lcRefreshSuffix = RIGHTC(lcRefreshDate,1)
*!*	      lcRefreshDate = LEFTC(lcRefreshDate,LENC(lcRefreshDate)-1)
*!*	    ENDIF
*!*	    ldRefreshDate = DATE(VAL(SUBSTRC(lcRefreshDate,7,4)), ;
*!*	                         VAL(LEFTC(lcRefreshDate,2)), ;
*!*	                         VAL(SUBSTRC(lcRefreshDate,4,2)))
*!*	    lcRefreshDate = TRANSFORM(ldRefreshDate)
*!*	  ENDIF
*!*	ENDIF

*!*	DO CASE
*!*	  **************************************************
*!*	  CASE NOT VARTYPE(tnVerInfo) = "N" OR NOT BETWEEN(tnVerInfo,1,6)
*!*	  **************************************************
*!*	    *
*!*	    *  default is just ldBuildDate
*!*	    *
*!*	    luRetVal = ldBuildDate
*!*	  **************************************************
*!*	  CASE tnVerInfo = 1
*!*	  **************************************************
*!*	    *
*!*	    *  just Build date as a character string
*!*	    *
*!*	    luRetVal = lcBuildDate
*!*	  **************************************************
*!*	  CASE tnVerInfo = 2
*!*	  **************************************************
*!*	    *
*!*	    *  just Refresh date
*!*	    *
*!*	    luRetVal = ldRefreshDate
*!*	  **************************************************
*!*	  CASE tnVerInfo = 3
*!*	  **************************************************
*!*	    *
*!*	    *  just Refresh date as a character string
*!*	    *
*!*	    luRetVal = lcRefreshDate
*!*	  **************************************************
*!*	  CASE tnVerInfo = 4
*!*	  **************************************************
*!*	    *
*!*	    *  Refresh suffix
*!*	    *
*!*	    luRetVal = lcRefreshSuffix
*!*	  **************************************************
*!*	  CASE tnVerInfo = 5
*!*	  **************************************************
*!*	    *
*!*	    *  Refresh date + Refresh suffix 
*!*	    *
*!*	    luRetVal = lcRefreshDate + lcRefreshSuffix
*!*	  **************************************************
*!*	  CASE tnVerInfo = 6
*!*	  **************************************************
*!*	    *
*!*	    *  the works -- BuildDate/RefreshDate<Suffix>
*!*	    *
*!*	    luRetVal = "Build: " + lcBuildDate + ;
*!*	               SPACE(2) + ;
*!*	               "Refresh: " + lcRefreshDate + lcRefreshSuffix
*!*	ENDCASE

*!*	RETURN luRetVal

