*
*  XXSYNCPK.PRG
*  For the currently-open GeneratePK alias, synchronize 
*  GeneratePK.GPK_PK values to match the last record 
*  added to the corresponding table (for only those
*  tables that are USED() with the alias stored in 
*  GeneratePK.GPK_PK when this routine is executed).  
*
*  The most common use for this utility is when you have
*  added records to table(s) manually and have populated 
*  the PKs properly but not via X3GENPK, so the GeneratePK
*  table does not contain the correct current PK value.
*  See VMDVPOP.PRG in the example VM application
*  \VMP_Int\DVSTUFF directory.
*
*  This routine RETURNs a numeric value (see below) indicating
*  the degree of success.  You can also optionally pass an
*  array (by reference) which gets populated with the results.
*
*  Copyright (c) 1997-2005 Visionpace   All Rights Reserved
*                17501 East 40 Hwy., Suite 218
*                Independence, MO 64055
*                816-350-7900 
*                http://www.visionpace.com
*                http://vmpdiscussion.visionpace.com
*  Author:  Drew Speedie, special thanks to Reg Brehaut 
*
*
*  NOTE that no records are INSERTed into the
*  GeneratePK table in this routine!  Your
*  GeneratePK table must already have records
*  for the tables (GPK_PK values) whose key
*  values you want synchronized in GeneratePK
*
*  Since this is an X3GENPK/GeneratePK-specific
*  routine, the GeneratePK table must be open
*  when this routine is called.  If not the RETURN
*  value is -2 and nothing happens.
*
*
*  Some folks run this utility on app startup on
*  systems that, for whatever reason, have problems
*  keeping their GeneratePK table in synch, although
*  we think this a potentially dangerous habit, 
*  since it probably masks some other fundamental
*  problem.  XXSYNCPK() is intended to be used as a 
*  developer tool.
*
*
*  If you want to add the action of this utility to
*  your application, you should probably add a custom
*  method to your ctr??App/oApp application object
*  with code that:
*  1- Makes sure no other users are in the system,
*     typically by calling oUsers.CheckForNoOtherUsers()
*       MODIFY CLASS cusUser OF XXFW METHOD CheckForNoOtherUsers
*     Note that it is not technically required that
*     there be no other users in the application,
*     but you certainly don't want anyone adding 
*     records during this process.
*  2- Make sure all the tables referenced in 
*     GeneratePK.GPK_PK are open -- this routine only
*     checks those tables referenced in the GPK_PK
*     field that are USED() at the moment.
*     Note that you will likely want/need to open
*     the SecuredItems free table ??USYSSI.DBF,
*     since VMP doesn't keep that table open.
*     To open tables, call oLib.oDBCSvc.OpenTable()
*       MODIFY CLASS cusDBCSvc OF XXFWLIBS METHOD OpenTable
*     or perhaps explicit USE commands wrapped in 
*     TRY/CATCH blocks, something like this:
*       TRY
*       USE ??!SomeTable IN 0 SHARED   &&& or USE SomeFreeTable.DBF IN 0 SHARED
*       CATCH
*       *  if the table cannot be opened SHARED,
*       *  it won't be synchronized
*       ENDTRY   
*     Note that you probably don't need to close such
*     tables (in the default data session) afterwards,
*     except perhaps for a table like SecuredItems.
*  3- Make sure the GeneratePK table is open.
*  4- Call XXSYNCPK() and then process its RETURN value, 
*     something like this:
*!*	LOCAL lnSynch, laUpdatedRows[1], loStandBy
*!*	loStandby = XXSTDBY(X3I("Synchronizing Primary Keys"+"..."))
*!*	lnSynch = XXSYNCPK(@m.laUpdatedRows)
*!*	DO CASE
*!*	  CASE m.lnSynch = 1 
*!*	    *  good/expected; nothing to do
*!*	  CASE m.lnSynch = -3 OR m.lnSynch = -2
*!*	    *  this should never happen in this particular 
*!*	    *  scenario, since we've already executed explicit
*!*	    *  code that ensures that neither of these conditions 
*!*	    *  can happen in this particular implementation
*!*	  CASE m.lnSynch = 0
*!*	    *  you may want to log something here, because
*!*	    *  at least one table had an out-of-synch PK,
*!*	    *  but which was successfully re-synchronized -- 
*!*	    *  the laUpdatedRows array contains specifics
*!*	    LOCAL xx, lcString
*!*	    lcString = "The Primary Key of the following tables had to be sychronized with the key stored in the GeneratePK table:" + CHR(13) + CHR(10)
*!*	    FOR xx = 1 TO ALEN(laUpdatedRows,1)
*!*	      IF laUpdatedRows[m.xx,4]
*!*	        lcString = m.lcString + laUpdatedRows[m.xx,1] + CHR(13) + CHR(10)
*!*	      ENDIF
*!*	    ENDFOR
*!*	    DO LogError IN XXError WITH ;
*!*	         1883, ;            &&& error number "Primary Key already exists"
*!*	         "XXSYNCPK.PRG", ;  &&& method or PRG
*!*	         -1, ;              &&& line number
*!*	         m.lcString, ;      &&& info text (in a real error scenario, contains SYS(16) info
*!*	         .NULL., ;          &&& object where error occurred
*!*	         .t.                &&& don't SET DATASESSION TO
*!*	  CASE m.lnSynch = -1
*!*	    *  you will definitely want to take some appropriate
*!*	    *  action here -- at least one table's PK is out of 
*!*	    *  synch, and could NOT be re-synched by XXSYNCPK();
*!*	    *  laUpdatedRows contains specifics
*!*	    LOCAL xx, lcString
*!*	    lcString = "The Primary Key of the following tables could not be sychronized with the key stored in the GeneratePK table:" + CHR(13) + CHR(10)
*!*	    FOR xx = 1 TO ALEN(laUpdatedRows,1)
*!*	      IF NOT laUpdatedRows[m.xx,4]
*!*	        lcString = m.lcString + laUpdatedRows[m.xx,1] + CHR(13) + CHR(10)
*!*	      ENDIF
*!*	    ENDFOR
*!*	    DO LogError IN XXError WITH ;
*!*	         1883, ;            &&& error number "Primary Key already exists"
*!*	         "XXSYNCPK.PRG", ;  &&& method or PRG
*!*	         -1, ;              &&& line number
*!*	         m.lcString, ;      &&& info text (in a real error scenario, contains SYS(16) info
*!*	         .NULL., ;          &&& object where error occurred
*!*	         .t.                &&& don't SET DATASESSION TO
*!*	ENDCASE
*  5- Handle cleanup, something like this:
*!*	USE IN SELECT("SecuredItems")
*!*	loStandby = .NULL.
*!*	RELEASE loStandby
*!*	IF m.lnSynch = -1
*!*	  X3MSGSVC("RawTextOK",m.lcString) 
*!*	  THIS.ForceShutdown()
*!*	  X3MSGSVC("Primary Keys all synchronized")
*!*	ENDIF
*!*	RETURN m.lnSynch >= 0
*  Note that we have implemented a scheme like this, for
*  demonsration purposes, in the VM and NT example apps.
*  See the "Synchronize Primary Keys" option on the Admin
*  menu, along with the custom method added at the 
*  intermediate level:
*    MODIFY CLASS ctrICApp OF ICFW METHOD SynchronizePKs
*
*
*  NOTE that (when PKs are character) this routine ONLY
*  works properly if the physical order in which records
*  exist in the table (RECNO()) matches the sequence in
*  which PKs are populated by X3GENPK() -- the highest
*  RECNO() contains the highest X3GENPK()-generated PK.
*
*
*  The following GeneratePK.GPK_PK values are ignored here:
*    1- Tables/aliases that are not currently open
*       (the GPK_PK value may not even indicate
*       a table, but may be a Unique ID used
*       for some other purpose)
*    2- Tables/aliases that are currently open but
*       have no Primary() key tag established
*       (generally free tables) and are not specified
*       in the taFreeTables array
*    3- Tables/aliases that are currently open and
*       have a PK tag but the PK expression is a
*       concatenation or expression
*    4- GeneratePK records where GPK_PK does not
*       indicate a table, but rather a generic ID
*         XXDTES("X3GENPK.PRG","tlGenericID")
*
*
*  FREE TABLE SUPPORT:  When GeneratePK.GPK_PK is the alias
*  of a free table, this routine can process those tables
*  as long as you create an array of information about those
*  tables:
*    LOCAL ARRAY laFreeTables[2,2]
*    laFreeTables[1,1] = "FreeTable1Alias"
*    laFreeTables[1,2] = "FreeTable1CandidateKeyTagName"
*    laFreeTables[2,1] = "FreeTable2Alias"
*    laFreeTables[2,2] = "FreeTable2CandidateKeyTagName"
*    ...etc.
*    xx = XXSYNCPK(.t.,@m.laFreeTables)
*  Note that there is no need to create and pass the array 
*  for the SecuredItems free table -- it is handled whether
*  it is in the array or not, since VMP "knows all about it" --
*  all you have to do is make sure SecuredItems is USED().
*
*
*  RETURN values
*    -3 -- GeneratePK is a local/remote view
*    -2 -- NOT USED("GeneratePK")
*    -1 -- At least one GeneratePK record for a 
*          currently-open table was out of synch
*          but GeneratePK was not able to be updated 
*     0 -- At least one GeneratePK record for a
*          currently-open table was out of synch,
*          although in each such case, the update
*          performed here was successful
*     1 -- For every GeneratePK record that matched
*          a currently-open table, GeneratePK was
*          already in synch (note that this does not
*          necessarily mean all your GeneratePK records
*          contain correct values.  For example,
*          this is the RETURN value if you run this
*          routine and GeneratePK is open but no other
*          tables are, or if only some of the tables
*          to be checked are open, or for any table
*          that is open with an ALIAS that is different
*          from the GPK_PK value)
*     2 -- Same as 0, but at least one GPK_PK field
*          value is for a table whose Primary Key field
*          is AutoInc-generated, and therefore shouldn't
*          be in GeneratePK at all (this is a relatively
*          harmless scenario, as X3GENPK() is not called
*          to generate PKs for the table anyway), and,
*          if GPK_CurrentNumber is not -1, this routine
*          set it to -1 to get your attention
*     3 -- Same as 1, but at least one GPK_PK field
*          value is for a table whose Primary Key field
*          is AutoInc-generated, and therefore shouldn't
*          be in GeneratePK at all (this is a relatively
*          harmless scenario, as X3GENPK() is not called
*          to generate PKs for the table anyway), and,
*          if GPK_CurrentNumber is not -1, this routine
*          set it to -1 to get your attention
*
*  lParameters
*    taUpdatedRows (O) Array (passed by reference), which, if
*                        passed, is filled here with information
*                        when this routine RETURNs -1, 0, or 1:
*                        Column1 = GeneratePK.GPK_PK value
*                        Column2 = GeneratePK.GPK_CurrentAlpha/GeneratePK.GPK_CurrentNumber
*                                  value that is out of synch with
*                                  the highest PK in the GPK_PK table
*                        Column3 = GeneratePK.GPK_CurrentAlpha/GeneratePK.GPK_CurrentNumber
*                                  value to bring things in synch
*                        Column4 = logical value indicating whether 
*                                  this routine was successful in 
*                                  updating GPK_CurrentAlpha/GPK_CurrentNumber
*                                  with the value in Column3
*                      If this routine RETURNs 1, the first array
*                        element is simply the string "none"
*     taFreeTables (O) Array (passed by reference) of free
*                        tables to be processed here, one table
*                        per array row:
*                        Column1 = ALIAS() which, if USED(), is processed
*                        Column2 = Name of candidate key tag, which, if
*                                  the table was contained, would be the
*                                  PRIMARY() key tag
*                      No need to create this array to process the
*                        SecuredItems table -- if it is USED() and 
*                        is a GeneratePK.GPK_PK value, it will be 
*                        processed.
*    tlHighestChar (O) Character PKs only - ignored for Integer PKs
*                      The default behavior of this utility is to
*                        GO BOTTOM to determine the highest-used
*                        PK value, on the assumption that the
*                        last physical record would have the last
*                        (highest) X3GENPK()-generated value.
*                      Pass this parameter as .T. to instead have
*                        this utility determined the highest-used
*                        PK value
*                      Passing this parameter as .T. would only
*                        be necessary in the rare scenario where
*                        PK values have somehow been generated 
*                        not in physical record order; in fact,
*                        in such a scenario you would NEED to
*                        pass this parameter as .T.
*                      You CAN always pass this parameter as .T.,
*                        just in case; it takes longer to run
*                        when this parameter is passed as .T., but
*                        in most typical usage scenarios, speed
*                        isn't your primary concern
*
LPARAMETERS taUpdatedRows, taFreeTables, tlHighestChar

IF NOT USED("GeneratePK")
  RETURN -2
ENDIF

IF INLIST(CURSORGETPROP("SourceType","GeneratePK"),1,2,101,102,201,202)
  *
  *  GeneratePK is a local or remote view --
  *  we won't be able to do anything here with
  *  remote tables, so there is no point going on.
  *  Remote tables/databases should be maintained
  *  in some fashion other than a routine like this.
  *
  RETURN -3
ENDIF

LOCAL lnSelect, lcTable, lcPKTag, lcPKTagExpr, ;
      lcSaveTag, luPKValue, llAllDigits, xx, ;
      llNeedsUpdate, lnBufferMode, lnRecno, llError, ;
      lcOnError, luUpdateValue, lnRetVal, laFreeTables, ;
      lnElement, lcSetDeleted, llAutoInc, laFields[1], ;
      llDigitPKs, llNonDigitPKs, lcSaveAD

IF TYPE("ALEN(taFreeTables)") = "N"
  *
  *  taFreeTables has been passed
  *
  DIMENSION laFreeTables[ALEN(taFreeTables,1),2]
  ACOPY(taFreeTables,laFreeTables)
  *
  *  make sure all elements are upper case, and add a "#"
  *  character to the alias()es in Column1 to make ASCAN()
  *  behave as if SET EXACT is ON, even if it is not
  *
  FOR xx = 1 TO ALEN(laFreeTables,1)
    laFreeTables[m.xx,1] = UPPER(ALLTRIM(laFreeTables[m.xx,1])) + "#"
    laFreeTables[m.xx,2] = UPPER(ALLTRIM(laFreeTables[m.xx,2]))
  ENDFOR
ENDIF

LOCAL ARRAY laUpdatedRows[1,4]
laUpdatedRows[1,1] = "none"
laUpdatedRows[1,2] = SPACE(0)
laUpdatedRows[1,3] = SPACE(0)
laUpdatedRows[1,4] = .f.

lnBufferMode = CURSORGETPROP("Buffering","GeneratePK")
IF NOT m.lnBufferMode = 2
  CURSORSETPROP("Buffering",2,"GeneratePK")
ENDIF

lnRetVal = 0   &&& establish numeric value
lnRetVal = .NULL.

lcSetDeleted = SET("DELETED")

lnSelect = SELECT(0)
SELECT GeneratePK

SCAN
  *
  *  one GeneratePK record/table at a time
  *
  lcTable = UPPER(ALLTRIM(GPK_PK))
  IF m.lcTable = "GENERATEPK"
    LOOP 
  ENDIF
  IF NOT USED(m.lcTable)
    LOOP 
  ENDIF
  SET DELETED OFF  &&& in case the last/bottom record is DELETED()
  SELECT (m.lcTable)
  lnRecno = IIF(EOF(),0,RECNO())
  lcSaveTag = ORDER()
  lcSaveAD = SPACE(0)
  IF NOT EMPTY(m.lcSaveTag) ;
       AND RIGHTC(SET("ORDER"),10) == "DESCENDING"
    lcSaveAD = "DESCENDING"
  ENDIF
  lcPKTag = X3PKTAG()
  IF EMPTY(m.lcPKTag) 
    DO CASE 
      *********************************************
        CASE TYPE("ALEN(taFreeTables)") = "N" ;
             AND ASCAN(laFreeTables,m.lcTable+"#") > 0
      *********************************************
        *
        *  an array of free tables has been passed --
        *  see if lcTable is in the array, and, if so,
        *  lcPKTag is the value in Column2
        *
        lnElement = ASCAN(laFreeTables,m.lcTable+"#")
        IF m.lnElement > 0
          lcPKTag = laFreeTables[m.lnElement + 1]
        ENDIF
      *********************************************
      CASE m.lcTable == "SECUREDITEMS" ;
           AND X2ISTAG("Sit_PK","SecuredItems")
      *********************************************
        *
        *  handle SecuredItems explicitly, since we know
        *  about it
        *  
        lcPKTag = "Sit_PK"
      *********************************************
      OTHERWISE
      *********************************************
        * nothing we can do here
    ENDCASE
  ENDIF
  IF EMPTY(m.lcPKTag)
    *
    *  no Primary Key tag, nothing we can go on
    *  to programmatically determine the PK 
    *  expression (most likely a free table --
    *  most contained tables should have a 
    *  defined PK, certainly any that are to
    *  be handled by X3GENPK/this routine)
    *
    LOOP 
  ENDIF
  lcPKTagExpr = X3TAGXPR(m.lcPKTag)
  IF OCCURS("+",m.lcPKTagExpr) > 0 OR OCCURS("(",m.lcPKTagExpr) > 0
    *
    *  concatenated/expression PKs are not
    *  supported/managed by this code
    *
    LOOP 
  ENDIF
  SELECT (m.lcTable)
  AFIELDS(laFields)
  IF ALEN(laFields,2) >= 17 ;
       AND ASCAN(laFields,m.lcPKTagExpr,1,-1,1,15) > 0 ;
       AND laFields[ASCAN(laFields,m.lcPKTagExpr,1,-1,1,15),17] > 0
    *  
    *  AutoInc Primary Key fields indicate that
    *  m.lcTable should *NOT* be in the GeneratePK
    *  table, so even though you *HAVE* put this
    *  record in GenereratePK, we're going to set
    *  GPK_CurrentNumber to 0 here, if it's not
    *  already set to 0
    *
    llAutoInc = .t.
    IF NOT GeneratePK.GPK_CurrentNumber = 0
      IF NOT laUpdatedRows[1,1] == "none"
        DIMENSION laUpdatedRows[ALEN(laUpdatedRows,1)+1,ALEN(laUpdatedRows,2)]
      ENDIF    
      laUpdatedRows[ALEN(laUpdatedRows,1),1] = m.lcTable
      laUpdatedRows[ALEN(laUpdatedRows,1),2] = TRANSFORM(GeneratePK.GPK_CurrentNumber)
      laUpdatedRows[ALEN(laUpdatedRows,1),3] = "0"
      llError = .f.
      TRY
      replace GPK_CurrentNumber WITH 0 IN GeneratePK
      CATCH
      llError = .t.
      ENDTRY
      laUpdatedRows[ALEN(laUpdatedRows,1),4] = NOT m.llError
    ENDIF
    LOOP 
  ENDIF
  SELECT (m.lcTable)
  IF VARTYPE(EVALUATE(m.lcPKTagExpr)) = "C"
    LOCATE 
    DO CASE
      ***************************************************
      CASE EOF()
      ***************************************************
        *
        *  lcTable doesn't contain any records yet
        *
        luPKValue = EVALUATE(m.lcPKTagExpr)
      ***************************************************
      CASE VARTYPE(m.tlHighestChar) = "L" AND m.tlHighestChar
      ***************************************************
        SELECT (m.lcTable)
*!*	        SELECT MAX(EVALUATE(m.lcPKTagExpr)) AS MaxPK ;
*!*	            FROM (m.lcTable) ;
*!*	            WHERE ISDIGIT(EVALUATE(m.lcPKTagExpr)) ;
*!*	            INTO CURSOR C_XXSyncPK_IsDigit 
        SELECT MAX(&lcPKTagExpr) AS MaxPK ;
        	FROM LineItems ;
        	WHERE ISDIGIT(&lcPKTagExpr) ;
        	INTO CURSOR C_XXSyncPK_IsDigit
        GO TOP IN C_XXSyncPK_IsDigit
        llDigitPKs = RECCOUNT("C_XXSyncPK_IsDigit") = 1 AND NOT ISNULL(C_XXSyncPK_IsDigit.MaxPK)
        SELECT (m.lcTable)
*!*	        SELECT MAX(EVALUATE(m.lcPKTagExpr)) AS MaxPK ;
*!*	            FROM (m.lcTable) ;
*!*	            WHERE NOT ISDIGIT(EVALUATE(m.lcPKTagExpr)) ;
*!*	            INTO CURSOR C_XXSyncPK_IsNotDigit NOFILTER
        SELECT MAX(&lcPKTagExpr) AS MaxPK ;
        	FROM LineItems ;
        	WHERE NOT ISDIGIT(&lcPKTagExpr) ;
        	INTO CURSOR C_XXSyncPK_IsNotDigit 
        GO TOP IN C_XXSyncPK_IsNotDigit
        llNonDigitPKs = RECCOUNT("C_XXSyncPK_IsNotDigit") = 1 AND NOT ISNULL(C_XXSyncPK_IsNotDigit.MaxPK)
        DO CASE
          CASE NOT m.llDigitPKs AND NOT m.llNonDigitPKs
            *
            *  no PKs generated yet; this shouldn't
            *  happen, because we have already checked
            *  for no records in lcTable
            *
            SELECT (m.lcTable)
            luPKValue = EVALUATE(m.lcPKTagExpr)
          CASE NOT m.llDigitPKs AND m.llNonDigitPKs
            *
            *  this shouldn't really happen -- indicates
            *  that ASCII-character keys have been
            *  generated, but not digit-character keys  
            *  (or else that ALL the digit-character keys
            *  have been DELETED(), which would be 
            *  extremely rare)
            *   
            luPKValue = C_XXSyncPK_IsNotDigit.MaxPK          
          CASE m.llDigitPKs AND NOT m.llNonDigitPKs
            *
            *  apparently the "rollover" point hasn't
            *  been reached yet
            *   
            luPKValue = C_XXSyncPK_IsDigit.MaxPK          
          OTHERWISE
            *
            *  the "rollover" point has been reached,
            *  so there are the initial digit-based
            *  keys AND the subsequent ASCII-character-based
            *  keys
            *  
            luPKValue = C_XXSyncPK_IsNotDigit.MaxPK          
        ENDCASE
        USE IN SELECT("C_XXSyncPK_IsDigit")
        USE IN SELECT("C_XXSyncPK_IsNotDigit")
      ***************************************************
      OTHERWISE
      ***************************************************
        *
        *  set the record pointer to the last record added 
        *
        SET ORDER TO 0
        GO BOTTOM 
        luPKValue = EVALUATE(m.lcPKTagExpr)
     ENDCASE
   ELSE
    *
    *  Integer-based keys, set the record pointer 
    *  to the highest used integer
    *
    SET ORDER TO TAG (m.lcPKTag)
    IF RIGHTC(SET("ORDER"),10) == "DESCENDING"
      *  this shouldn't happen, but you never know...
      SET ORDER TO TAG (m.lcPKTag) ASCENDING
    ENDIF
    GO BOTTOM 
    luPKValue = EVALUATE(m.lcPKTagExpr)
  ENDIF
  SELECT GeneratePK
  llNeedsUpdate = .f.
  IF VARTYPE(m.luPKValue) = "C"
    *
    *  character PKs
    *
    llAllDigits = .t.
    FOR xx = 1 TO LENC(m.luPKValue)
      IF NOT SUBSTRC(m.luPKValue,m.xx,1) $ "0123456789"
        llAllDigits = .f.
        EXIT 
      ENDIF
    ENDFOR
    IF m.llAllDigits
      IF NOT GPK_CurrentNumber == VAL(m.luPKValue)
        luUpdateValue = INT(VAL(m.luPKValue))
        llNeedsUpdate = .t.
      ENDIF
     ELSE
      IF NOT GPK_CurrentAlpha == m.luPKValue
        luUpdateValue = m.luPKValue
        llNeedsUpdate = .t.
      ENDIF
    ENDIF
   ELSE
    *
    *  numeric PKs
    *     
    IF NOT GPK_CurrentNumber == m.luPKValue
      luUpdateValue = m.luPKValue
      llNeedsUpdate = .t.
    ENDIF
  ENDIF
  IF m.llNeedsUpdate
    IF NOT laUpdatedRows[1,1] == "none"
      DIMENSION laUpdatedRows[ALEN(laUpdatedRows,1)+1,ALEN(laUpdatedRows,2)]
    ENDIF    
    laUpdatedRows[ALEN(laUpdatedRows,1),1] = m.lcTable
    laUpdatedRows[ALEN(laUpdatedRows,1),3] = TRANSFORM(m.luUpdateValue)
    *
    *  effect an RLOCK() via pessimistic buffering,
    *  to prevent another user from generating a
    *  PK while this next code is running
    *
    llError = .f.
    IF VARTYPE(m.luUpdateValue) = "C"
      laUpdatedRows[ALEN(laUpdatedRows,1),2] = TRANSFORM(GPK_CurrentAlpha)
      TRY
      replace GPK_CurrentAlpha WITH m.luUpdateValue IN GeneratePK
      CATCH 
      llError = .t.
      ENDTRY
      IF NOT m.llError 
        *
        *  if GPK_CurrentAlpha is in use, then 
        *  GPK_CurrentNumber should be 99999
        *
        TRY
        replace GPK_CurrentNumber WITH ;
            INT(VAL(REPLICATE("9",LENC(GPK_CurrentAlpha)))) IN GeneratePK
        CATCH
        llError = .t.
        ENDTRY
      ENDIF
     ELSE
      laUpdatedRows[ALEN(laUpdatedRows,1),2] = TRANSFORM(GPK_CurrentNumber)
      TRY
      replace GPK_CurrentNumber WITH luUpdateValue IN GeneratePK
      CATCH
      llError = .t.
      ENDTRY
      IF NOT m.llError AND NOT EMPTY(GPK_CurrentAlpha)
        *
        *  if GPK_CurrentNumber is in use, then
        *  GPK_CurrentAlpha should be blank
        *
        TRY
        replace GPK_CurrentAlpha WITH SPACE(LENC(GPK_CurrentAlpha)) IN GeneratePK
        CATCH 
        llError = .t.
        ENDTRY
      ENDIF
    ENDIF
    IF m.llError
      laUpdatedRows[ALEN(laUpdatedRows,1),4] = .f.
      lnRetVal = -1
     ELSE 
      IF TABLEUPDATE(.t.,.t.,"GeneratePK")
        laUpdatedRows[ALEN(laUpdatedRows,1),4] = .t.
        IF ISNULL(m.lnRetVal)
          lnRetVal = 0
        ENDIF
       ELSE
        TABLEREVERT(.t.,"GeneratePK")
        laUpdatedRows[ALEN(laUpdatedRows,1),4] = .f.
        lnRetVal = -1
      ENDIF
    ENDIF
  ENDIF
  SET DELETED &lcSetDeleted
  DO CASE
    CASE EMPTY(m.lcSaveTag)
      SET ORDER TO 0 IN (m.lcTable)
    CASE m.lcSaveAD = "DESCENDING"
      SET ORDER TO (m.lcSaveTag) IN (m.lcTable) DESCENDING
    OTHERWISE
      SET ORDER TO (m.lcSaveTag) IN (m.lcTable)
  ENDCASE
  IF m.lnRecno > 0
    GOTO (m.lnRecno) IN (m.lcTable)
   ELSE 
    GO BOTTOM IN (m.lcTable)
    IF NOT EOF(m.lcTable)
      SKIP 1 IN (m.lcTable)
    ENDIF
  ENDIF
  SELECT GeneratePK
ENDSCAN

SET DELETED &lcSetDeleted

IF ISNULL(m.lnRetVal)
  lnRetVal = 1
ENDIF

SELECT GeneratePK
IF NOT m.lnBufferMode = 2
  CURSORSETPROP("Buffering",m.lnBufferMode,"GeneratePK")
ENDIF

IF TYPE("ALEN(taUpdatedRows)") = "N"
  DIMENSION taUpdatedRows[ALEN(laUpdatedRows,1),ALEN(laUpdatedRows,2)]
  ACOPY(laUpdatedRows,taUpdatedRows)
ENDIF

IF m.llAutoInc
  lnRetVal = m.lnRetVal + 2
ENDIF

RETURN m.lnRetVal
