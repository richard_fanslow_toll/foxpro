*
*  X3SETCLS.PRG
*  SET CLASSLIB to the passed .VCX
*
*  Copyright (c) 1997-2005 Visionpace   All Rights Reserved
*                17501 East 40 Hwy., Suite 218
*                Independence, MO 64055
*                816-350-7900 
*                http://www.visionpace.com
*                http://vmpdiscussion.visionpace.com
*  Author:  Drew Speedie and Art Bergquist
*           Special thanks to Brian Grant and Mike Yearwood
*
*  USAGE:
*    X3SETCLS("XXFWUTIL")
*    X3SETCLS("XXFW.VCX")
*    IF NOT X3SETCLS("MY.VCX")
*      * take appropriate action
*    ENDIF
*    X3SETCLS("XXFW.VCX,XXFWUTIL.VCX")
*    IF NOT X3SETCLS("XXFW,XXFWUTIL")
*      * take appropriate action
*    ENDIF
*
*  RETURNS:
*    A logical value indicating success
*
*  lParameters
*     tcClassLib (R) Name of the .VCX class library (or 
*                      libraries) to be added to the 
*                      SET CLASSLIB list.  The ".VCX" 
*                      extension is optional.  
*                    You can specify multiple (comma-separated) 
*                      class libraries, but if you do -- you 
*                      cannot *also* specify an .APP/.EXE 
*                      file in tcInAppExeFile
*                    If tcInAppExeFile is not passed and the
*                      SET CLASSLIB TO (tcClassLib) ADDITIVE
*                      fails and this routine is executed from
*                      within a distributed EXE/APP, then a
*                      SET CLASSLIB TO (tcClassLib) IN (SYS(16,0))
*                      is attempted.
*                    If tcInAppExeFile is not passed and the
*                      SET CLASSLIB TO (tcClassLib) ADDITIVE
*                      fails and FILE("XXLIB.APP"), then a
*                      SET CLASSLIB TO (tcClassLib) IN XXLIB.APP
*                      is attempted.
*                    If tcInAppExeFile is not passed and the
*                      SET CLASSLIB TO (tcClassLib) ADDITIVE
*                      fails and FILE("XXLIB.EXE"), then a
*                      SET CLASSLIB TO (tcClassLib) IN XXLIB.EXE
*                      is attempted.
*                    If tcInAppExeFile is not passed and the
*                      SET CLASSLIB TO (tcClassLib) ADDITIVE
*                      fails and there are EXEs/APPs in the
*                      SET PROCEDURE TO list, then a
*                      SET CLASSLIB TO (tcClassLib) IN APP/EXE
*                      is attempted for each EXE/APP in the
*                      SET PROCEDURE TO list until one is
*                      successful (if any).
*                    If you pass this parameter as a single
*                      Class Library, code near the end of 
*                      this routine moves tcClassLib from its
*                      position as the LAST item in the 
*                      SET("CLASSLIB") list to the FIRST item,
*                      so that if you have a class in tcClassLib
*                      named the same as a class in another
*                      class library in the SET("CLASSLIB") list,
*                      the class in tcClassLib will be found
*                      FIRST rather than LAST when the next
*                      CREATEOBJECT() is performed
* tcInAppExeFile (O) Name of an .APP/.EXE file that contains 
*                      tcClasslib, maps to the IN AppEXEFileName 
*                      supported by the SET CLASSLIB TO
*                      command.
*                    If you specify multiple class libraries 
*                      in tcClassLib, then this parameter is 
*                      not supported, and this routine 
*                      RETURNs .f.
*                      
*    tlListFirst (O) Pass this parameter as .T. if you want
*                      tcClassLib set to the FIRST position
*                      in the SET CLASSLIB TO list, rather
*                      than the LAST position that is the
*                      default behavior -- when there are
*                      potentially more than one .VCX in the
*                      list that contain a class of the same
*                      name, pass this parameter as .T. to
*                      make tcClassLib first and therefore the
*                      one in which VFP will find the class
*                      in a subsequent CREATEOBJECT() call.
*                    Ignored when you pass a comma-separated
*                      list of multiple class libraries.
*
*
LPARAMETERS tcClassLib, tcInAppExeFile, tlListFirst

LOCAL lcClassLib, lcInAppExeFile

IF TYPE("tcClassLib") = "C" ;
     AND NOT ISNULL(tcClassLib) ;
     AND NOT EMPTY(tcClassLib)
  lcClasslib = UPPER(ALLTRIM(tcClassLib))
 ELSE
  RETURN .f.
ENDIF

IF TYPE("tcInAppExeFile") = "C" ;
     AND NOT ISNULL(tcInAppExeFile) ;
     AND NOT EMPTY(tcInAppExeFile)
  lcInAppExeFile = UPPER(ALLTRIM(tcInAppExeFile))
 ELSE
  lcInAppExeFile = SPACE(0)
ENDIF

IF NOT EMPTY(lcInAppExeFile) AND "," $ lcClassLib 
  *
  *  if tcInAppExeFile has been passed, this routine only
  *  supports a single class library passed in tcClassLib
  *
  RETURN .f.
ENDIF

LOCAL llSuccess, lcOnError, lcSingleClassLib
llSuccess = .T.
lcOnError = ON("ERROR")
ON ERROR llSuccess = .f.
DO CASE
  ********************************************
  CASE NOT EMPTY(lcInAppExeFile)
  ********************************************
    SET CLASSLIB TO (lcClassLib) IN (lcInAppExeFile) ADDITIVE 
  ********************************************
  CASE "," $ lcClassLib
  ********************************************
    *  multiple class libraries passed in tcClassLib
    SET CLASSLIB TO &lcClassLib. ADDITIVE
  ********************************************
  OTHERWISE
  ********************************************
    *  single class library passed in tcClassLib
    IF NOT RIGHTC(lcClassLib,4) = ".VCX"
      lcClassLib = lcClassLib + ".VCX"
    ENDIF
    lcSingleClassLib = lcClassLib
    IF FULLPATH(lcClassLib) $ SET("CLASSLIB")
      *
      *  lcClassLib is already in the SET("CLASSLIB") list
      *
     ELSE
      LOCAL lcInModule, llXXLIBAPP, llXXLIBEXE
      llSuccess = .t.
      IF OCCURS(SPACE(1),lcClassLib)>0
        SET CLASSLIB TO (lcClassLib) ADDITIVE 
       ELSE
        SET CLASSLIB TO &lcClassLib ADDITIVE 
      ENDIF
      IF NOT llSuccess 
        lcInModule = SPACE(0)
        IF INLIST(RIGHTC(SYS(16,0),4),".APP",".EXE")
          *
          *  try IN the current highest-level executing .EXE/.APP
          *  
          lcInModule = " IN " + SYS(16,0)
        ENDIF
        llSuccess = .t.
        SET CLASSLIB TO (lcClassLib) &lcInModule ADDITIVE 
      ENDIF
      #IF VERSION(5) >= 700
      IF NOT llSuccess AND NOT EMPTY(SET("PROCEDURE"))
        *
        *  try IN any .APP/.EXE procedures in the
        *  SET PROCEDURE list
        *
        LOCAL laProcs[1], lnCount, lcProc
        lnCount = ALINES(laProcs,SET("PROCEDURE"),",")
        FOR EACH lcProc IN laProcs
          IF INLIST(JUSTEXT(ALLTRIM(lcProc)),"APP","EXE")
            IF UPPER(JUSTFNAME(ALLTRIM(lcProc))) = "XXLIB.APP"
              llXXLIBAPP = .t.
            ENDIF
            IF UPPER(JUSTFNAME(ALLTRIM(lcProc))) = "XXLIB.EXE"
              llXXLIBEXE = .t.
            ENDIF
            llSuccess = .t.
            SET CLASSLIB TO (lcClassLib) IN (ALLTRIM(lcProc)) ADDITIVE 
            IF llSuccess
              EXIT
            ENDIF
          ENDIF
        ENDFOR
      ENDIF
      #ENDIF
      IF NOT llSuccess AND FILE("XXLIB.APP") AND NOT llXXLIBAPP
        *
        *  try IN XXLIB.APP
        *
        llSuccess = .t.
        SET CLASSLIB TO (lcClassLib) IN XXLIB.APP ADDITIVE 
      ENDIF
      IF NOT llSuccess AND FILE("XXLIB.EXE") AND NOT llXXLIBEXE
        *
        *  try in XXLIB.EXE
        *
        llSuccess = .t.
        SET CLASSLIB TO (lcClassLib) IN XXLIB.EXE ADDITIVE 
      ENDIF
    ENDIF  
ENDCASE  

IF llSuccess ;
     AND NOT EMPTY(lcSingleClassLib) ;
     AND OCCURS(",",SET("CLASSLIB"))>0 ;
     AND VARTYPE(tlListFirst) = "L" AND tlListFirst
  *
  *  when you pass tcClassLib as a single .VCX, rearrange
  *  the SET("CLASSLIB") list so that tcClassLib is FIRST
  *  in the list, primarily to ensure that if you have
  *  a class in tcClassLib that is named the same as a
  *  class in some other class library in the SET("CLASSLIB")
  *  list, the just-added tcClassLib is FIRST in the list,
  *  rather than LAST in the list (the VFP default), the
  *  assumption being that you want the class in the
  *  just-added tcClassLib to be found by the next 
  *  CREATEOBJECT()
  *
  LOCAL lcString, lcLine, laClassLibs[1]
  lcString = SPACE(0)
  #IF VAL(SUBSTRC(VERSION(),AT_C(SPACE(1),VERSION(),2)+1,2)) > 6
    *
    *  VFP 7 or higher
    *
    ALINES(laClassLibs,SET("CLASSLIB"),.t.,",")
  #ELSE
    *
    *  VFP 6 or lower, before the optional final ALINES()
    *  parameter was added
    *
    LOCAL xx, lnStart, lnEnd
    lnStart = 1
    FOR xx = 1 TO OCCURS(",",SET("CLASSLIB")) + 1
      IF xx > 1
        lnStart = lnStop + 2
      ENDIF
      lnStop = AT_C(",",SET("CLASSLIB")+",",xx) 
      DIMENSION laClassLibs[xx]
      laClassLibs[xx] = SUBSTRC(SET("CLASSLIB"),lnStart,lnStop-lnStart)
    ENDFOR    
  #ENDIF
  FOR EACH lcLine IN laClassLibs
    IF "\"+JUSTSTEM(lcClassLib)+".VCX" $ lcLine
      *  do nothing, ignoring the just-set CLASSLIB      
      lcClassLib = lcLine
     ELSE
      lcString = lcString + lcLine + ", "
    ENDIF
  ENDFOR
  lcString = ALLTRIM(lcString)
  IF RIGHTC(lcString,1) = ","
    lcString = LEFTC(lcString,LENC(lcString)-1)
  ENDIF
  lcString = lcClassLib + ", " + lcString
  ON ERROR llSuccess = .f.
  *  I expect this to always succeed...
  SET CLASSLIB TO &lcString
ENDIF

ON ERROR &lcOnError

RETURN llSuccess
 


