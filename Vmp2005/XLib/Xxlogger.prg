*
*  XXLOGGER.PRG
*  Log the passed string to XXLOG.TXT, along with the
*  DATETIME() and ASTACKINFO().  
*
*  Copyright (c) 2002-2005 Visionpace   All Rights Reserved
*                17501 East 40 Hwy., Suite 218
*                Independence, MO 64055
*                816-350-7900 
*                http://www.visionpace.com
*                http://vmpdiscussion.visionpace.com
*  Author:  Drew Speedie
*
*  The resulting XXLOG.TXT accumulates logged entries
*  during the course of the day.  When this .PRG is 
*  called and none of its logged entries is for today/
*  DATE(), the existing XXLOG.TXT is erased and started
*  anew.  But, since XXLOG.TXT can grow rather large,
*  slowing down the process of updating it each time this
*  library routine is called, we restrict XXLOG.TXT to
*  4,000 lines.  HOWEVER, you can both eliminate the
*  4,000-line restriction AND speed up that process of
*  logging entries by completely removing the XXLOG.TXT
*  header, INCLUDING the **********HEADER**********... line.
*
*  This routine is called from the VMP framework to
*  log specific actions when the application or specific
*  form/middle-tier object is in "Diagnostic Mode", but
*  you can insert calls to this routine anywhere you 
*  want.
*
*  To review the places where VMP framework code (or your
*  own intermediate/app-specific code) calls this routine
*  to log activities:
*    DO XXDTSearch WITH "XXLOGGER"
*
*  lParameters
*         tcText (O) Custom text you want added to the log
*                    This parameter is optional, but STRONGLY
*                      recommended
* tlNoASTACKINFO (O) Pass this parameter as .T. to suppress
*                      the default behavior where the
*                      ASTACKINFO() is included in the
*                      log entry, reducing the size/length
*                      of XXLOG.TXT 
*                    Pass this parameter as .T. when you just
*                      need to track methods as they are
*                      called, as we have done in the VMP 
*                      middle tier, for example:
*                        XXDTES("XXFWNTDS.VCX","XXLOGGER","ctrDataSource","FetchList")
*       tlIndent (O) Pass this parameter as .T. when
*                      1- tlNoASTACKINFO has been passed as .T.
*                      2- tcText has been passed as PROGRAM()
*                    Pass this parameter as .T. to have XXLOG.TXT
*                      output of tcText/PROGRAM() indented to show
*                      the call stack
*                    This parameter is ignored when tlNoASTACKINFO
*                      passed as anything but .T.
*                    Passing .T. has no effect when tcText is
*                      passed as anything other than PROGRAM()
*                    XXDTES("XXFWNTDS.VCX","XXLOGGER","ctrDataSource","FetchList")
*

LPARAMETERS tcText, ;
            tlNoASTACKINFO, ;
            tlIndent &&& future:  add a flag to store to XXLOGGER.DBF???

LOCAL lcLogFile, llNewLog, lcText, lcCRLF, lcSetSafety
lcCRLF = CHR(13) + CHR(10)
lcSetSafety = SET("SAFETY")
SET SAFETY OFF
*
*  see if there's an existing XXLOG.TXT file
*
lcLogFile = GetExistingLogFile()
llNewLog = EMPTY(m.lcLogFile)
IF m.llNewLog
  lcLogFile = GetNewLogFile()
 ELSE
  *
  *  see if the existing one contains entries
  *  from days other than today, in which case
  *  we need to delete it and start a new log
  *  for today
  *
  IF NOT ExistingLogIsForToday(m.lcLogFile)
    ERASE (m.lcLogFile)
    llNewLog = .t.
  ENDIF
ENDIF
*
*  generate the text
*
lcText = SPACE(0)
IF m.llNewLog
  lcText = GenerateHeaderText() + m.lcCRLF + m.lcCRLF
ENDIF
lcText = m.lcText + GenerateBodyText(tcText,tlNoASTACKINFO,m.tlIndent)
IF llNewLog
  lcText = m.lcText + m.lcCRLF + m.lcCRLF + GenerateFooterText()
ENDIF
*
*  create/update the XXLOG.TXT log file
*
IF m.llNewLog
  STRTOFILE(m.lcText,m.lcLogFile,.f.)
 ELSE
  UpdateExistingLogFile(m.lcLogFile,m.lcText,m.tlIndent)
ENDIF
SET SAFETY &lcSetSafety

RETURN



PROCEDURE ExistingLogIsForToday
*
*  RETURNs a logical value indicating whether
*  the passed XXLOG.TXT file contains entries
*  for today
*
LPARAMETERS tcLogFile
LOCAL llRetVal, laLines[1], lcCentury, laLines[1], lcExact, ;
      lcTestString
IF NOT FILE(m.tcLogFile)
  RETURN m.llRetVal
ENDIF
lcCentury = SET("Century")
lcExact = SET("EXACT")
SET CENTURY ON
SET EXACT OFF 
lcTestString = "Logged by XXLOGGER.PRG " + DTOC(DATE())
IF ALINES(laLines,FILETOSTR(m.tcLogFile)) > 0
  *  see if at least one line starts with DTOC(DATE())
  llRetVal = ASCAN(laLines,m.lcTestString) > 0
ENDIF
RETURN m.llRetVal



PROCEDURE GenerateHeaderText
*
*  generate the log information header text
*
LOCAL lcHeader
TEXT TO lcHeader NOSHOW 
See the footer of this file for information on how it is created/updated.
Entries in this log file are listed in reverse chronological order.
For performance reasons, the size of this file is limited to
4,000 lines - once it exceeds that size, logged items scroll off
from the bottom.
For the best performance AND no limitation on the number of logged entries,
COMPLETELY REMOVE this header, INCLUDING the following **********HEADER***... line.
**********HEADER****************************************** 
ENDTEXT
RETURN m.lcHeader



PROCEDURE GenerateFooterText
*
*  generate the log information footer text
*
LOCAL lcFooter
TEXT TO lcFooter NOSHOW 
**********FOOTER****************************************** 
To find the places in the VMP framework where this log
file is created/updated on a daily basis:
  DO XXDTSearch WITH "XXLOG"

This log file contains only one day's worth of successful
SQL-SELECTs -- the first time each day that a SQL-SELECT 
is logged to this file, the existing contents are deleted/
purged.

The contents of this file are listed in reverse chronological
order -- the most recent SQL-SELECT statement is at the top.

This file can be accessed:
1- Manually, by executing 
    MODIFY FILE XXLOG.TXT
    MODIFY COMMAND XXLOG.TXT
    EDITSOURCE("XXLOG.TXT",1)
2- From the "XXLOG.TXT" option on the VMP shortcut menu 
   for VMP form controls
3- From the "XXLOG.TXT" option on the VMP menu on _MSYSMENU

This file is located, in order of precedence:
1- In the current directory, if possible -- in some
   scenarios the current user may have only ReadOnly
   access to the current directory 
2- In the SYS(2023) directory (which is not likely in the
   VFP path)
ENDTEXT
RETURN m.lcFooter



PROCEDURE GenerateBodyText
*
*  generate the log information text
*
LPARAMETERS tcText, tlNoASTACKINFO, tlIndent
LOCAL lcText, lcCRLF, xx, laStackInfo[1], lcStdText, ;
      llIndent, llNoASTACKINFO, lnIndent
lcText = TRANSFORM(m.tcText)
lcCRLF = CHR(13) + CHR(10)
llNoASTACKINFO = VARTYPE(m.tlNoASTACKINFO)="L" AND m.tlNoASTACKINFO
llIndent = VARTYPE(m.tlIndent)="L" AND m.tlIndent AND m.llNoASTACKINFO
DO CASE
  *********************************************************
  CASE m.llIndent   &&& llNoASTACKINFO = .t.
  *********************************************************
    lcStdText = "Logged by XXLOGGER.PRG " + TTOC(DATETIME()) 
    ASTACKINFO(laStackInfo)
    lnIndent = ASCAN(laStackInfo,m.lcText,3,-1,3,15)
    IF m.lnIndent = 0
      lcText = m.lcText + m.lcCRLF + m.lcStdText 
     ELSE
      lnIndent = laStackInfo[m.lnIndent,1] - 1
      IF m.lnIndent > 0
        lnIndent = m.lnIndent*2
      ENDIF
      lcStdText = SPACE(m.lnIndent) + "Logged by XXLOGGER.PRG " + TTOC(DATETIME()) 
      lcText = SPACE(m.lnIndent) + m.lcText + m.lcCRLF + m.lcStdText 
    ENDIF
  *********************************************************
  CASE m.llNoASTACKINFO  &&& llIndent = .f.
  *********************************************************
    lcStdText = "Logged by XXLOGGER.PRG " + TTOC(DATETIME()) 
    lcText = m.lcText + m.lcCRLF + m.lcStdText 
  *********************************************************
  OTHERWISE   &&& default, llNoASTACKINFO = .f., llIndent = .f.
  *********************************************************
    lcStdText = "Logged by XXLOGGER.PRG " + TTOC(DATETIME()) + ;
                " -- ASTACKINFO():"
    IF ASTACKINFO(laStackInfo) > 2
      lcStdText = m.lcStdText + m.lcCRLF
      FOR xx = 1 TO ALEN(laStackInfo,1) - 2
        lcStdText = m.lcStdText + ;
                    TRANSFORM(laStackInfo[m.xx,1]) + SPACE(2) + ;
                    laStackInfo[m.xx,2] + SPACE(2) + ;
                    laStackInfo[m.xx,3] + SPACE(2) + ;
                    laStackInfo[m.xx,4] + SPACE(2) + ;
                    TRANSFORM(laStackInfo[m.xx,5]) + SPACE(2) + ;
                    laStackInfo[m.xx,6] + ;
                    IIF(m.xx=ALEN(laStackInfo,1)-2,SPACE(0),m.lcCRLF)
      ENDFOR
    ENDIF   &&& ASTACKINFO()>2
    lcText = m.lcText + m.lcCRLF + m.lcStdText 
ENDCASE
RETURN m.lcText



PROCEDURE GetExistingLogFile
*
*  find the XXLOG.TXT file, return it as a fully-
*  qualified filename (or a blank string if it
*  cannot be found)
*
LOCAL lcLogFile
lcLogFile = SPACE(0)
IF FILE("XXLOG.TXT")
  *
  *  XXLOG.TXT is in the VFP path
  *
  lcLogFile = FULLPATH("XXLOG.TXT")
ENDIF
IF EMPTY(m.lcLogFile) AND FILE(ADDBS(SYS(2023)) + "XXLOG.TXT")
  *
  *  XXLOG.TXT is in the SYS(2023) folder
  *
  lcLogFile = ADDBS(SYS(2023)) + "XXLOG.TXT"
ENDIF
RETURN m.lcLogFile



PROCEDURE GetNewLogFile
*
*  RETURNs the fully-qualified path+filename of 
*  the XXLOG.TXT file, when it doesn't exist already
*  OR
*  a blank string when we can't create XXLOG.TXT
*  in the current directory or the SYS(2023) directory
*
LOCAL lcLogFile, lcTestFile, llError
lcLogFile = SPACE(0)
lcTestFile = SYS(2015)
llError = .f.
TRY
STRTOFILE("This is a test",m.lcTestFile,.f.)
CATCH
llError = .t.
ENDTRY
IF m.llError  
  lcTestFile = ADDBS(SYS(2023)) + m.lcTestFile
  llError = .f.
  TRY
  STRTOFILE("This is a test",m.lcTestFile,.f.)
  CATCH
  llError = .t.
  ENDTRY
 ELSE
  lcTestFile = FULLPATH(m.lcTestFile)
ENDIF
IF NOT llError
  lcLogFile = ADDBS(JUSTPATH(m.lcTestFile)) + "XXLOG.TXT"
ENDIF
ERASE (m.lcTestFile)
RETURN m.lcLogFile



PROCEDURE UpdateExistingLogFile
*
*  create/update an existing XXLOG.TXT log file
*
LPARAMETERS tcLogFile, tcText, tlIndent
IF NOT VARTYPE(m.tcLogFile) = "C" OR EMPTY(m.tcLogFile) OR NOT FILE(m.tcLogFile) ;
     OR NOT VARTYPE(m.tcText) = "C" OR EMPTY(m.tcText)
  RETURN .t.
ENDIF
LOCAL lnHeaderLine, laLines[1], xx, lcBody, lcCRLF, lcLine, ;
      lcCRLF2, llIndent
lcBody = SPACE(0)      
lcCRLF = CHR(13) + CHR(10)
lcCRLF2 = m.lcCRLF + m.lcCRLF
IF VARTYPE(m.tlIndent) = "L" AND m.tlIndent
  llIndent = .t.
 ELSE
  llIndent = .f.
ENDIF
IF ALINES(laLines,FILETOSTR(m.tcLogFile)) > 0
  lnHeaderLine = ASCAN(laLines,REPLICATE("*",10)+"HEADER",1,0,0,1) 
  IF m.lnHeaderLine > 0
    FOR xx = m.lnHeaderLine+1 TO ALEN(laLines,1)
      lcLine = laLines[m.xx]
      *
      *  previously-existing body plus footer
      *
      IF EMPTY(m.lcBody) AND EMPTY(m.lcLine)
        *  skip whatever blank lines are after the header
       ELSE
        lcBody = m.lcBody + m.lcCRLF + m.lcLine
      ENDIF
      IF m.xx > 4000
        EXIT
      ENDIF
    ENDFOR
    STRTOFILE(GenerateHeaderText() + ;
              IIF(m.llIndent,m.lcCRLF,m.lcCRLF2) + ;
              m.tcText + ;
              IIF(m.llIndent,m.lcCRLF,m.lcCRLF2) + ;
              m.lcBody, ;
              m.tcLogFile,.f.)
   ELSE
    *
    *  no header line, just insert the new text at the top
    *  
    *  NOTE that this can result in a file > 4000 lines
    *
    STRTOFILE(m.tcText + ;
              IIF(m.llIndent,m.lcCRLF,m.lcCRLF2) + ;
              FILETOSTR(m.tcLogFile),m.tcLogFile,.f.)
  ENDIF
ENDIF
RETURN .t.



PROCEDURE ViewLog
*  
*  view the current XXLOG.TXT
*
*  lParameters
*    tlModiComm (O) Logical flag indicating whether
*                   you want to view XXLOG.TXT via 
*                   MODI COMM rather than the default
*                   XXDTES()/EDITSOURCE()
*                   - MODI COMM: program execution stops
*                     at the MODI COMM, until you close
*                     the MODI COMM window or navigate
*                     out of it, cursor is positioned
*                     wherever it was last time 
*                   - EDITSOURCE():  program execution
*                     does not stop at the EDITSOURCE(),
*                     but rather there is an implicit
*                     NOWAIT, cursor is positioned on 
*                     the first log item
*
LPARAMETERS tlModiComm
*
*  XXLOGGER.PRG itself RETURNs if not run in 
*  VFP 7.0 or higher, so XXLOG.TXT cannot be
*  created by calling XXLOGGER()
*  
*
LOCAL lcLogFile
lcLogFile = GetExistingLogFile()
DO CASE
  *********************************
  CASE EMPTY(m.lcLogFile)
  *********************************
    *
    *  no XXLOG.TXT, or we couldn't find it in the
    *  above call to GetExistingLogFile()
    *
    MESSAGEBOX("XXLOGGER.PRG/ViewLog() is unable to find XXLOG.TXT",48,"Please Note")
  *********************************
  CASE VARTYPE(m.tlModiComm) = "L" AND m.tlModiComm
  *********************************
    *
    *  pass tlModiComm explicitly as .T.
    *
    MODIFY COMMAND XXLOG.TXT
  *********************************
  OTHERWISE
  *********************************
    *
    *  default
    *
    XXDTES("XXLOG.TXT","Logged by")
ENDCASE 
RETURN



PROCEDURE EraseLog
*
*  erase the current XXLOG.TXT, if any
*
LOCAL lcLogFile
lcLogFile = GetExistingLogFile()
IF EMPTY(m.lcLogFile)
  MESSAGEBOX("XXLOGGER.PRG/EraseLog() is unable to locate XXLOG.TXT, nothing to erase.",; 
             48,"Please Note")
  RETURN
ENDIF
ERASE (m.lcLogFile)
RETURN



PROCEDURE CreateVMPDiagnostic
*
*  create VMPDiagnostic.TXT, if it doesn't exist 
*
IF FILE("VMPDiagnostic.TXT")
  MESSAGEBOX(FULLPATH("VMPDiagnostic.TXT") + ;
             " already exists, nothing to create", ; 
             48,"Please Note")
  RETURN
ENDIF
STRTOFILE("Enable diagnostics","VMPDiagnostic.TXT",.f.)
MESSAGEBOX(FULLPATH("VMPDiagnostic.TXT") + ;
           " created -- diagnostic mode is enabled globally.", ;
           48,"Please Note")
RETURN
            
            
            
PROCEDURE EraseVMPDiagnostic
*
*  erase VMPDiagnostic.TXT if it exists
*
IF NOT FILE("VMPDiagnostic.TXT") 
  MESSAGEBOX("XXLOGGER.PRG/EraseVMPDiagnostic() is unable to locate VMPDiagnostic.TXT, nothing to erase.",; 
             48,"Please Note")
  RETURN
ENDIF
ERASE VMPDiagnostic.TXT
RETURN
