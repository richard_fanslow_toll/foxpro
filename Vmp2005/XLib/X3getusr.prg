*
*  X3GETUSR.PRG
*  Call the Windows WNetGetUser() function to 
*  determine the current UserName -- this .PRG is
*  a wrapper to WNetGetUser().
*
*  Copyright (c) 1997-2005 Visionpace   All Rights Reserved
*                17501 East 40 Hwy., Suite 218
*                Independence, MO 64055
*                816-350-7900 
*                http://www.visionpace.com
*                http://vmpdiscussion.visionpace.com
*  Author:  Drew Speedie, Tom DeMay
*
*  NOTE: X3GETUSR() should work in the following:
*    Win 3.1  &&& but only if you un-comment what is commented now
*    Win 3.11 &&& but only if you un-comment what is commented now
*    WFW      &&& but only if you un-comment what is commented now
*    Windows NT
*    Windows '95
*    Windows '98
*    Windows 2000
*    Windows XP
*    Novell
*    ... and likely others
*
*  REQUIRES (Win 3.x and WFW only)
*  =====================================
*  X2SETLIB.PRG
*  FOXTOOLS.FLL in the VFP startup directory or in the VFP path
*
*  Usage: 
*    lcUserName = X3GETUSR()
*    IF empty(lcUserName)
*      lcUserName = "DEFAULT"
*    ENDIF
*
*
LOCAL lcUserName, ;
      lnUserNameLen, ;
      lnWNetGetUser, ;
      lnResult, ;
      lcRetVal, ;
      lcLocalName, ;
      lcWinOS
lcUserName    = SPACE(40)
lnUserNameLen = LENC(m.lcUserName)
lcLocalName   = SPACE(0)
*!*	lcWinOS = x2winos()
*!*	IF lcWinOS = "WIN3"
*!*	  IF !x2setlib("FOXTOOLS")
*!*	    return space(0)
*!*	  ENDIF
*!*	  external file foxtools.fll
*!*	  *
*!*	  *  Windows 3.1, 3.11, Windows for WorkGroups
*!*	  *
*!*	  lnWNetGetUser = RegFn("WNetGetUser","@C@I","I")
*!*	  lnResult      = CallFn(lnWNetGetUser, ;
*!*	                         @lcUserName, ;
*!*	                         @lnUserNameLen)
*!*	  IF lnUserNameLen <= 0 OR empty(lcUserName)
*!*	    lcRetVal = space(0)
*!*	   ELSE
*!*	    lcRetVal = leftc(lcUserName,lnUserNameLen-1)
*!*	  ENDIF
*!*	 ELSE
  *
  *  Windows 95, 98, NT, 2000
  *
  DECLARE Integer WNetGetUser in win32api ;
       String @lcLocalName, ;
       String @lcUserName, ;
       Integer @lnUserNameLen
  lnResult = WNetGetUser(@lcLocalName,@lcUserName,@lnUserNameLen)
  IF m.lnUserNameLen <= 0 OR EMPTY(m.lcUserName)
    lcRetVal = SPACE(0)
   ELSE
    lcRetVal = ALLTRIM(LEFTC(m.lcUserName,m.lnUserNameLen-1))
    IF RIGHTC(m.lcRetVal,1) = CHR(0)
      lcRetVal = LEFTC(m.lcRetVal,LENC(m.lcRetVal)- 1)
    ENDIF
  ENDIF
*!*	ENDIF

RETURN m.lcRetVal



