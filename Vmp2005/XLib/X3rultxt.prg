*
*  X3RULTXT.PRG
*  Returns passed rule text as a string, whether the
*  RuleText set in the database is a string or
*  an expression
*
*  Copyright (c) 1997-2005 Visionpace   All Rights Reserved
*                17501 East 40 Hwy., Suite 218
*                Independence, MO 64055
*                816-350-7900 
*                http://www.visionpace.com
*                http://vmpdiscussion.visionpace.com
*  Author:  Drew Speedie
*
LPARAMETERS tcRuleText
*
*  RuleText can be either a string or an expression
*  or a combination:
*
LOCAL lcRuleText
IF INLIST(LEFTC(m.tcRuleText,1),["],['],"]") ;
     AND !"(" $ m.tcRuleText
  *
  *  RuleText is an expression
  *
  lcRuleText = SUBSTRC(m.tcRuleText,2,LENC(m.tcRuleText)-2)
 ELSE
  *
  *  RuleText is a string that may contain an
  *  embedded expression
  *
  IF EMPTY(m.tcRuleText)
    lcRuleText = "Invalid data"
   ELSE 
    lcRuleText = EVALUATE(m.tcRuleText)
  ENDIF
ENDIF
RETURN m.lcRuleText
