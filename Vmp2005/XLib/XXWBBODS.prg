*
*  XXWBBODS.PRG
*  VMP wizard to create a concrete Business Object + 
*  Data Source composite class.
*  
*  Copyright (c) 2002-2005 Visionpace   All Rights Reserved
*                17501 East 40 Hwy., Suite 218
*                Independence, MO 64055
*                816-350-7900 
*                http://www.visionpace.com
*                http://vmpdiscussion.visionpace.com
*  Author:  Drew Speedie and Art Bergquist
*
*  Note that the local procedure CreateBOandDS() can be called from a
*  utility you might create to automate the process of creating
*  a number of BO+DS classes -- just create the C_XXWBBODS
*  cursor, populate it, and then call CreateBOandDS(), something
*  like this:
*
*!*	CLEAR ALL
*!*	CLOSE ALL
*!*	SET PROCEDURE TO XXWBBODS ADDITIVE 
*!*	CreateCursor()
*!*	*
*!*	*  populate C_XXWBBODS fields
*!*	*
*!*	CreateBOandDS()
*!*	RETURN
*    
*

#DEFINE TotalNumberOfParameters 8
LPARAMETERS tc1stParameter, ;
            tcNameOfParentClassOfDS, ;
            tcVCXName, ;
            tcDSParentVCXFileName,	;
            tcBOName, ;
            tcBOParentClassName, ;
            tcBOParentVCXFileName, ;
            tcDSMemberName
LOCAL lnPCount, lcCRLF, lcFormat
lnPCount = PCOUNT()
lcCRLF   = CHR(13) + CHR(10)
lcFormat = ' ... format is either:  <Database>!<Table Name> or ' + lcCRLF +  lcCRLF + ;
           '<Data Source (DS)>,  <DS Parent class>, <.VCX file containing new DS>, ' + ;
		   '<DS Parent VCX File>, <Business Object (BO)>, <BO Parent class>, ' + ;
		   '<BO Parent VCX File>, <DS Member>'



IF (lnPCount >= 1)
  IF      (VARTYPE(tc1stParameter) # 'C')						;
      OR ((lnPCount = 1) AND (NOT "!" $ tc1stParameter))		;
      OR   BETWEEN(lnPCount, 2, TotalNumberOfParameters - 1)	;
      OR  (lnPCount > TotalNumberOfParameters)
    *------------------------------------------------------------------------------------
    * Since only one (1) parameter was passed, it therefore represents a fully-qualified
    * table name (meaning that it's in the format: <Database>!<Table Name>).
    *
    * We will "derive" all other information from this single parameter.
    *------------------------------------------------------------------------------------
    ASSERT .F. MESSAGE PROGRAM() + lcFormat
    RETURN .F.
  ENDIF

  *-------------------------------------------------------------------------------------------
  * When eight (8) parameters are passed, ensure that the first parameter is not
  * incorrectly passed as <Database>!<Table Name>; it should be simply be the
  * Name of the new Data Source (DS)
  *-------------------------------------------------------------------------------------------
  IF (lnPCount = TotalNumberOfParameters)	;
      AND (    ("!" $ tc1stParameter) OR (VARTYPE(tcNameOfParentClassOfDS) # 'C')	;
            OR (VARTYPE(tcVCXName              ) # 'C')	;
            OR (VARTYPE(tcDSParentVCXFileName  ) # 'C')	;
            OR (VARTYPE(tcBOName               ) # 'C')	;
            OR (VARTYPE(tcBOParentClassName    ) # 'C')	;
            OR (VARTYPE(tcBOParentVCXFileName  ) # 'C')	;
            OR (VARTYPE(tcDSMemberName         ) # 'C'))
    ASSERT .F. MESSAGE PROGRAM() + lcFormat
    RETURN .F.
  ENDIF
ENDIF

*
* Preserve the information past the upcoming:
*    CLEAR ALL
*    CLOSE ALL
*
STRTOFILE(	[lcPCount                = "] + TRANSFORM(lnPCount               ) + ["] + lcCRLF + ;
			[lc1stParameter          = "] + TRANSFORM(tc1stParameter         ) + ["] + lcCRLF + ;
			[lcNameOfParentClassOfDS = "] + TRANSFORM(tcNameOfParentClassOfDS) + ["] + lcCRLF + ;
			[lcVCXName               = "] + TRANSFORM(tcVCXName              ) + ["] + lcCRLF + ;
			[lcDSParentVCXFileName   = "] + TRANSFORM(tcDSParentVCXFileName  ) + ["] + lcCRLF + ;
			[lcBOName                = "] + TRANSFORM(tcBOName               ) + ["] + lcCRLF + ;
			[lcBOParentClassName     = "] + TRANSFORM(tcBOParentClassName    ) + ["] + lcCRLF + ;
			[lcBOParentVCXFileName   = "] + TRANSFORM(tcBOParentVCXFileName  ) + ["] + lcCRLF + ;
			[lcDSMemberName          = "] + TRANSFORM(tcDSMemberName         ) + ["] + lcCRLF,	;
			"XXWBBODS-Parameters.TXT", .F.)

CLEAR ALL
CLOSE ALL

* AB - 10/2003 - Start
LOCAL lcPCount, lc1stParameter, lcNameOfParentClassOfDS, lcVCXName, lcDSParentVCXFileName,	;
      lcBOName, lcBOParentClassName, lnLine, lcString
LOCAL ARRAY laLines[1]
ALINES(laLines, FILETOSTR("XXWBBODS-Parameters.TXT"))
FOR lnLine = 1 TO ALEN(laLines, 1)
  lcString = laLines[lnLine]
  &lcString.
ENDFOR    
ERASE XXWBBODS-Parameters.TXT
* AB - 10/2003 - End

CreateCursor()

SELECT C_XXWBBODS
APPEND BLANK 
replace C_XXWBBODS.Cancelled WITH .t. in C_XXWBBODS

LOCAL lcSetClassLib
lcSetClassLib = SET("ClassLib")

LOCAL lnPCount, lcCRLF, lcDSClassName, llSuccess
lnPCount = VAL(lcPCount)
lcCRLF   = CHR(13) + CHR(10)
llSuccess = .t.

DO CASE
  CASE lnPCount = 0
    SET CLASSLIB TO XXWB.VCX
    PUBLIC XXWBBODS
    XXWBBODS = CREATEOBJECT("frmXXWBBODS") 
    IF TYPE("XXWBBODS.BaseClass") = "C"
      XXWBBODS.Show()
    ENDIF

  CASE lnPCount = 1
    LOCAL lcDatabase, lcTable, lcDSParentVCXFileName, lcBOParentClassName
    lcDatabase = GETWORDNUM(lc1stParameter, 1, "!")
    lcTable    = GETWORDNUM(lc1stParameter, 2, "!")
    OPEN DATABASE (lcDatabase)
    LOCAL llValidTable
    llValidTable = INDBC(lcTable, "Table")
    CLOSE DATABASES
    IF NOT llValidTable
      MESSAGEBOX(lcTable + ' is not a table in the ' + lcDatabase + ' database!', 0 + 48, 'Invalid table')
      RETURN
    ENDIF

    * 'DS_<TableName>'
    lcDSClassName = "DS_" + lcTable

    * '<Drive>:\??\??MT<Table>.VCX'
    lcVCXName = lcDatabase + "MT" + lcTable + ".VCX"

    lcNameOfParentClassOfDS = "ctr" + lcDatabase + "DataSourceDefaultSPTBehavior"
	lcDSParentVCXFileName   = lcDatabase + "DS.VCX"
    lcDSMemberName          = "oDS" + lcTable
    lcBOParentClassName     = "ctr" + lcDatabase + "BusinessObject"
    lcBOParentVCXFileName   = lcDatabase + "FWNTBO.VCX"

    REPLACE Cancelled           WITH .F.								,	;
            DSClassName         WITH  lcDSClassName						,	;
            DSVCXFileName       WITH  lcVCXName							,	;
            DSParentClassName   WITH  lcNameOfParentClassOfDS			,	;
            DSParentVCXFileName WITH  lcDSParentVCXFileName				,	;
            BOClassName         WITH  STRTRAN(lcDSClassName, 'DS', 'BO'),	;
            BOVCXFileName       WITH  lcVCXName							,	;
            BOParentClassName   WITH  lcBOParentClassName				,	;
            BOParentVCXFileName WITH  lcBOParentVCXFileName				,	;
            DSMemberName		WITH  lcDSMemberName					,	;
            FinishAction        WITH 'DSInClassDesignerBuilder'				;
      IN C_XXWBBODS

  CASE lnPCount = TotalNumberOfParameters
    lcDSClassName = lc1stParameter
*!*	    REPLACE Cancelled           WITH .F.								,	;
*!*	            DSClassName         WITH  lcDSClassName						,	;
*!*	            DSVCXFileName       WITH  lcVCXName							,	;
*!*	            DSParentClassName   WITH  lcNameOfParentClassOfDS			,	;
*!*	            DSParentVCXFileName WITH  lcDSParentVCXFileName				,	;
*!*	            BOClassName         WITH  STRTRAN(lcDSClassName, 'DS', 'BO'),	;
*!*	            BOVCXFileName       WITH  lcVCXName							,	;
*!*	            BOParentClassName   WITH  lcBOParentClassName				,	;
*!*	            BOParentVCXFileName WITH  lcBOParentVCXFileName				,	;
*!*	            DSMemberName		WITH  lcDSMemberName					,	;
*!*	            FinishAction        WITH 'DSInClassDesignerBuilder'				;
*!*	      IN C_XXWBBODS
    REPLACE Cancelled           WITH .F.								,	;
            DSClassName         WITH lcDSClassName						,	;
            DSVCXFileName       WITH lcVCXName							,	;
            DSParentClassName   WITH lcNameOfParentClassOfDS			,	;
            DSParentVCXFileName WITH lcDSParentVCXFileName				,	;
            BOClassName         WITH lcBOName,	;
            BOVCXFileName       WITH lcVCXName							,	;
            BOParentClassName   WITH lcBOParentClassName				,	;
            BOParentVCXFileName WITH lcBOParentVCXFileName				,	;
            DSMemberName		WITH lcDSMemberName					,	;
            FinishAction        WITH 'DSInClassDesignerBuilder'				;
      IN C_XXWBBODS
  OTHERWISE
    * Either not enough, or too many, parameters were passed.
    ASSERT .F. MESSAGE PROGRAM() + lcFormat
    RETURN .F.
ENDCASE

IF C_XXWBBODS.Cancelled
  llSuccess = .f.
ENDIF

IF llSuccess
  llSuccess = CreateBOandDS()
ENDIF

DO CASE
  ******************************************************
  CASE EMPTY(C_XXWBBODS.FinishAction) ;
       OR C_XXWBBODS.Cancelled ;
       OR NOT llSuccess
  ******************************************************
    *
    *  nothing more to do
    *
    ACTIVATE WINDOW Command
  ******************************************************
  CASE UPPER(C_XXWBBODS.FinishAction) = UPPER("DSInClassDesigner")
  ******************************************************
    lcDSClassName = ALLTRIM(C_XXWBBODS.DSClassName)
    IF FILE("DSB_InitHook.PRG")
      LOCAL lcDSB_InitHookFile, lcDatabase
      lcDSB_InitHookFile = 'DSB_InitHook.TXT'
      ERASE (lcDSB_InitHookFile)
      lcDatabase = LEFTC(JUSTSTEM(ALLTRIM(C_XXWBBODS.DSVCXFileName)), 2)
      STRTOFILE(lcDatabase + "!" + SUBSTRC(lcDSClassName,4),lcDSB_InitHookFile, .F.)
    ENDIF
    MODIFY CLASS (lcDSClassName) ;
         OF (ALLTRIM(C_XXWBBODS.DSVCXFileName)) NOWAIT
    IF NOT WONTOP() = "CLASS DESIGNER"
      *
      *  method snippet editor, bail out of it
      *
      KEYBOARD "{ESCAPE}" PLAIN CLEAR 
    ENDIF
    IF UPPER(C_XXWBBODS.FinishAction) = UPPER("DSInClassDesignerBuilder")
      LOCAL laDS[1], loDS
      ASELOBJ(laDS,1)
      loDS = laDS[1]
      DO XXBUILDER WITH loDS, "RTMOUSE"
    ENDIF
  ******************************************************
  CASE UPPER(C_XXWBBODS.FinishAction) = UPPER("BOInClassDesigner")
  ******************************************************
    MODIFY CLASS (ALLTRIM(C_XXWBBODS.BOClassName)) ;
         OF (ALLTRIM(C_XXWBBODS.BOVCXFileName)) NOWAIT 
    IF NOT WONTOP() = "CLASS DESIGNER"
      *
      *  method snippet editor, bail out of it
      *
      KEYBOARD "{ESCAPE}" PLAIN CLEAR 
    ENDIF
ENDCASE

USE IN SELECT("C_XXWBBODS")

SET CLASSLIB TO &lcSetClassLib

RETURN 



PROCEDURE CreateCursor
CREATE CURSOR C_XXWBBODS (BOClassName C(60), ;
                          BOVCXFileName C(254), ;
                          BOParentClassName C(60),; 
                          BOParentVCXFileName C(254), ;
                          DSClassName C(60),; 
                          DSVCXFileName C(254), ;
                          DSParentClassName C(60),; 
                          DSParentVCXFileName C(254), ;
                          icDataSource C(60), ;
                          ilUpdateable L,; 
                          icAlias C(60), ;
                          icPKFieldName C(60), ;
                          DSExists L, ;
                          DSMemberName C(60), ;
                          Cancelled L, ;
                          FinishAction C(60))
RETURN



PROCEDURE CreateBOandDS
*
*  process the C_XXWBBODS cursor information
*
*  this local procedure can be called directly, if you have
*  created and populdate the C_XXWBBODS cursor manually from
*  the calling code
*
SELECT C_XXWBBODS
LOCAL llSuccess
llSuccess = .t.
SCAN
  IF NOT CreateDS()
    llSuccess = .f.
    EXIT
  ENDIF
  CreateBO()
ENDSCAN
LOCATE 
RETURN llSuccess



PROCEDURE CreateDS
*
*  create the indicated Data Source
*
IF C_XXWBBODS.DSExists
  *
  *  use the existing specified DS, nothing
  *  to do here
  *
  RETURN .t.
ENDIF
*
*  create the new DS
*
LOCAL lcDSClassName, lcDSVCXFileName, lcDSParentClassName, ;
      lcDSParentVCXFileName, lcDataSource, lcPKFieldName, ;
      llUpdateable, lcAlias, lcSetSafety
lcSetSafety = SET("Safety")      
SET SAFETY OFF 
lcDSClassName = UPPER(ALLTRIM(C_XXWBBODS.DSClassName))
lcDSVCXFileName = UPPER(ALLTRIM(C_XXWBBODS.DSVCXFileName))
lcDSParentClassName = UPPER(ALLTRIM(C_XXWBBODS.DSParentClassName))
lcDSParentVCXFileName = UPPER(ALLTRIM(C_XXWBBODS.DSParentVCXFileName))
IF NOT FILE(lcDSParentVCXFileName)
  MESSAGEBOX(lcDSParentVCXFileName + " cannot be located.  " + ;
             "It appears that you did not create an app-specific " + ;
             "abstract Data Source class.", 48, "Please Note")
  RETURN .F.
ENDIF
CREATE CLASS (lcDSClassName) OF (lcDSVCXFileName) ;
     AS (lcDSParentClassName) FROM (lcDSParentVCXFileName) ;
     NOWAIT 
CLEAR CLASS (lcDSClassName)
CLEAR CLASSLIB (lcDSVCXFileName)
lcDataSource = ALLTRIM(C_XXWBBODS.icDataSource)
lcPKFieldName = ALLTRIM(C_XXWBBODS.icPKFieldName)
lcAlias = ALLTRIM(C_XXWBBODS.icAlias)
llUpdateable = C_XXWBBODS.ilUpdateable
IF NOT EMPTY(lcDataSource) OR NOT EMPTY(lcPKFieldName) ;
     OR NOT EMPTY(lcAlias) OR NOT EMPTY(llUpdateable)
  *
  *  set optional DS properties
  *
  LOCAL laDS[1], loDS
  ASELOBJ(laDS,1)
  loDS = laDS[1]
  IF NOT EMPTY(lcDataSource)
    loDS.icDataSource = lcDataSource
  ENDIF
  IF llUpdateable AND NOT loDS.ilUpdateable
    loDS.ilUpdateable = TRANSFORM(llUpdateable)
  ENDIF
  IF NOT EMPTY(lcPKFieldName) 
    loDS.icPKFieldName = lcPKFieldName
  ENDIF
  IF NOT EMPTY(lcAlias)
    loDS.icAlias = lcAlias
  ENDIF
ENDIF
KEYBOARD "Y" PLAIN CLEAR 
RELEASE WINDOWS ("Class Designer")
CLEAR CLASS (lcDSClassName)
CLEAR CLASSLIB (lcDSVCXFileName)
SET SAFETY &lcSetSafety
RETURN .t.



PROCEDURE CreateBO
*
*  create the indicated Business Object, and
*  add an instance of the Data Source
*
LOCAL lcBOClassName, lcBOVCXFileName, lcBOParentClassName, ;
      lcBOParentVCXFileName, lcMemberName, llExists, ;
      lcDSVCXFileName, lcDSClassName, lcSetSafety
lcSetSafety = SET("Safety")      
SET SAFETY OFF 
lcBOClassName = UPPER(ALLTRIM(C_XXWBBODS.BOClassName))
lcBOVCXFileName = UPPER(ALLTRIM(C_XXWBBODS.BOVCXFileName))
lcBOParentClassName = UPPER(ALLTRIM(C_XXWBBODS.BOParentClassName))
lcBOParentVCXFileName = UPPER(ALLTRIM(C_XXWBBODS.BOParentVCXFileName))
lcMemberName = ALLTRIM(C_XXWBBODS.DSMemberName)
lcDSClassName = UPPER(ALLTRIM(C_XXWBBODS.DSClassName))
lcDSVCXFileName = UPPER(ALLTRIM(C_XXWBBODS.DSVCXFileName))
IF FILE(lcBOVCXFileName)
  IF AVCXCLASSES(laVCXClasses,lcBOVCXFileName)>0
    IF ASCAN(laVCXClasses,lcBOClassName,1,-1,1,1)>0
      llExists = .t.
    ENDIF
  ENDIF
ENDIF
IF llExists
  REMOVE CLASS (lcBOClassName) OF (lcBOVCXFileName)
ENDIF
*
*  create the BO
*
CREATE CLASS (lcBOClassName) OF (lcBOVCXFileName) AS (lcBOParentClassName) FROM (lcBOParentVCXFileName) NOWAIT 
CLEAR CLASS (lcBOClassName)
CLEAR CLASSLIB (lcBOVCXFileName)
*
*  add the member DS in the upper-left corner of the BO
*
LOCAL loBO
ASELOBJ(laBO,1)
loBO = laBO[1]
SET CLASSLIB TO (lcDSVCXFileName) ADDITIVE 
loBO.AddObject(lcMemberName,lcDSClassName)
loBO.&lcMemberName..Top = 4
loBO.&lcMemberName..Left = 4
KEYBOARD "Y" PLAIN CLEAR 
RELEASE WINDOWS ("Class Designer")
CLEAR CLASS (lcBOClassName)
CLEAR CLASSLIB (lcBOVCXFileName)
SET SAFETY &lcSetSafety
RETURN .t.







*!*	*************BODS.PRG

*!*	LOCAL lcBOClassName, ;
*!*	      lcBOVCXFileName, ;
*!*	      lcBOParentClassName, ;
*!*	      lcBOParentVCXFileName, ;
*!*	      lcDSClassName, ;
*!*	      lcDSVCXFileName, ;
*!*	      lcDSParentClassName, ;
*!*	      lcDSParentVCXFileName, ;
*!*	      lcDSicDataSource,; 
*!*	      llDSilUpdateable, ;
*!*	      lcDSicPKFieldName, ;
*!*	      lcDSicAlias, ;
*!*	      laBOParentClass[1], ;
*!*	      laDSParentClass[1], ;
*!*	      laVCXClasses[1], ;
*!*	      xx, ;
*!*	      llExists, ;
*!*	      llOKBO, ;
*!*	      llOKDS, ;
*!*	      laDS[1], ;
*!*	      laBO[1], ;
*!*	      lcMemberName

*!*	ACTIVATE SCREEN
*!*	CLEAR

*!*	lcDSClassName = INPUTBOX("Specify the Name of the new Data Source class","DS Class Name")
*!*	IF EMPTY(lcDSClassName)
*!*	  CLEAR 
*!*	  RETURN
*!*	ENDIF
*!*	? "Specify the name of the .VCX for the new " + lcDSCLassName + " Data Source class"
*!*	lcDSVCXFileName = GETFILE("VCX","DS .VCX")
*!*	IF EMPTY(lcDSVCXFileName)
*!*	  CLEAR 
*!*	  RETURN
*!*	ENDIF
*!*	CLEAR  
*!*	 
*!*	? "Select the ParentClass for the new Data Source"
*!*	IF NOT AGETCLASS(laDSParentClass)
*!*	  CLEAR 
*!*	  RETURN
*!*	ENDIF
*!*	CLEAR 
*!*	lcDSParentVCXFileName = laDSParentClass[1]
*!*	lcDSParentClassName = laDSParentClass[2]

*!*	lcDSicDataSource = INPUTBOX("Specify the icDataSource property for the new Data Source (REQUIRED)",lcDSClassName+".icDataSource")
*!*	IF EMPTY(lcDSicDataSource)
*!*	  CLEAR 
*!*	  RETURN
*!*	ENDIF
*!*	llDSilUpdateable = INPUTBOX("Specify the ilUpdateable property for the new Data Source (OPTIONAL)",lcDSClassName+".ilUpdateable")
*!*	IF EMPTY(llDSilUpdateable)
*!*	  CLEAR 
*!*	  RETURN
*!*	ENDIF
*!*	llDSilUpdateable = UPPER(llDSilUpdateable)
*!*	DO CASE
*!*	  CASE INLIST(llDSilUpdateable,".F.","F","N","NO")
*!*	    llDSilUpdateable = .f.
*!*	  CASE INLIST(llDSilUpdateable,".T.","T","Y","YES")
*!*	    llDSilUpdateable = .t.
*!*	  OTHERWISE
*!*	    CLEAR 
*!*	    RETURN 
*!*	ENDCASE
*!*	lcDSicAlias = INPUTBOX("Specify the icAlias property for the new Data Source (OPTIONAL)",lcDSClassName+".icAlias")
*!*	IF EMPTY(lcDSicAlias) AND llDSilUpdateable
*!*	  CLEAR 
*!*	  RETURN
*!*	ENDIF
*!*	lcDSicPKFieldName = INPUTBOX("Specify the icPKFieldName property for the new Data Source (OPTIONAL)",lcDSClassName+".icPKFieldname")
*!*	IF EMPTY(lcDSicPKFieldName) AND llDSilUpdateable
*!*	  CLEAR 
*!*	  RETURN
*!*	ENDIF


*!*	lcBOClassName = INPUTBOX("Specify the Name of the new Business Object class","BO Class Name") 
*!*	IF EMPTY(lcBOClassName)
*!*	  CLEAR 
*!*	  RETURN
*!*	ENDIF
*!*	? "Specify the name of the .VCX for the new " + lcBOClassName + " Business Object class"
*!*	lcBOVCXFileName = GETFILE("VCX","BO .VCX")
*!*	IF EMPTY(lcBOVCXFileName)
*!*	  CLEAR 
*!*	  RETURN
*!*	ENDIF
*!*	CLEAR 

*!*	? "Select the ParentClass for the new Business Object"
*!*	IF NOT AGETCLASS(laBOParentClass)
*!*	  CLEAR 
*!*	  RETURN 
*!*	ENDIF
*!*	CLEAR 
*!*	lcBOParentVCXFileName = laBOParentClass[1]
*!*	lcBOParentClassName = laBOParentClass[2]


*!*	IF FILE(lcBOVCXFileName)
*!*	  IF AVCXCLASSES(laVCXClasses,lcBOVCXFileName)>0
*!*	    FOR xx = 1 TO ALEN(laVCXClasses,1)
*!*	      IF UPPER(laVCXClasses[xx,1]) == UPPER(lcBOClassName) 
*!*	        llExists = .t.
*!*	        EXIT
*!*	      ENDIF
*!*	    ENDFOR
*!*	  ENDIF
*!*	ENDIF
*!*	llOKBO = .t.
*!*	IF llExists
*!*	  IF MESSAGEBOX(lcBOClassName + " already exists in " + ;
*!*	                lcBOVCXFileName + ".  Overwrite it with " + ;
*!*	                "this new one?",4+48,"Please Note") = 6
*!*	    REMOVE CLASS (lcBOClassName) OF (lcBOVCXFileName)
*!*	   ELSE
*!*	    llOKBO = .f.
*!*	  ENDIF
*!*	ENDIF
*!*	IF NOT llOKBO
*!*	  CLEAR 
*!*	  RETURN
*!*	ENDIF
*!*	*
*!*	*  check to see if lcBOParentClassName/lcBOVCXFileName
*!*	*  is a container inheriting from ctrBusinessObject
*!*	*


*!*	RELEASE laVCXClasses
*!*	llExists = .f.
*!*	LOCAL laVCXClasses[1] 
*!*	IF FILE(lcDSVCXFileName)
*!*	  IF AVCXCLASSES(laVCXClasses,lcDSVCXFileName)>0
*!*	    FOR xx = 1 TO ALEN(laVCXClasses,1)
*!*	      IF UPPER(laVCXClasses[xx,1]) == UPPER(lcDSClassName) 
*!*	        llExists = .t.
*!*	        EXIT
*!*	      ENDIF
*!*	    ENDFOR
*!*	  ENDIF
*!*	ENDIF
*!*	llOKDS = .t.
*!*	IF llExists
*!*	  IF MESSAGEBOX(lcDSClassName + " already exists in " + ;
*!*	                lcDSVCXFileName + ".  Overwrite it with " + ;
*!*	                "this new one?",4+48,"Please Note") = 6
*!*	    REMOVE CLASS (lcDSClassName) OF (lcDSVCXFileName)
*!*	   ELSE
*!*	    llOKDS = .f.
*!*	  ENDIF
*!*	ENDIF
*!*	*
*!*	*  check to see if lcDSParentClassName/lcDSVCXFileName
*!*	*  is a container inheriting from ctrDataSource
*!*	*

*!*	IF llOKDS
*!*	  CREATE CLASS (lcDSClassName) OF (lcDSVCXFileName) AS (lcDSParentClassName) FROM (lcDSParentVCXFileName) NOWAIT 
*!*	  CLEAR CLASS (lcDSClassName)
*!*	  CLEAR CLASSLIB (lcDSVCXFileName)
*!*	  ASELOBJ(laDS,1)
*!*	  xx = laDS[1]
*!*	  xx.icDataSource = lcDSicDataSource
*!*	  IF llDSilUpdateable AND NOT xx.ilUpdateable
*!*	    xx.ilUpdateable = TRANSFORM(llDSilUpdateable)
*!*	  ENDIF
*!*	  IF NOT EMPTY(lcDSicPKFieldName) AND NOT UPPER(xx.icPKFieldName) == UPPER(lcDSicPKFieldName)
*!*	    xx.icPKFieldName = lcDSicPKFieldname
*!*	  ENDIF
*!*	  KEYBOARD "Y" PLAIN CLEAR 
*!*	  RELEASE WINDOWS ("Class Designer")
*!*	 ELSE
*!*	  CLEAR 
*!*	  RETURN 
*!*	ENDIF
*!*	IF llOKBO
*!*	  CREATE CLASS (lcBOClassName) OF (lcBOVCXFileName) AS (lcBOParentClassName) FROM (lcBOParentVCXFileName) NOWAIT 
*!*	  CLEAR CLASS (lcBOClassName)
*!*	  CLEAR CLASSLIB (lcBOVCXFileName)
*!*	  lcMemberName = "o" + lcDSClassName
*!*	  ASELOBJ(laBO,1)
*!*	  xx = laBO[1]
*!*	  SET CLASSLIB TO (lcDSVCXFileName) ADDITIVE 
*!*	  xx.AddObject(lcMemberName,lcDSClassName)
*!*	  xx.&lcMemberName..Top = 4
*!*	  xx.&lcMemberName..Left = 4
*!*	  KEYBOARD "Y" PLAIN CLEAR 
*!*	  RELEASE WINDOWS ("Class Designer")
*!*	  MODIFY CLASS (lcBOClassName) OF (lcBOVCXFileName) NOWAIT 
*!*	 ELSE
*!*	  CLEAR 
*!*	  RETURN 
*!*	ENDIF

*!*	CLEAR 
