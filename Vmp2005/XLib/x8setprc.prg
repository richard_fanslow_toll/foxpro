*
*  X8SETPRC.PRG
*  SET PROCEDURE to the passed .PRG(s) ADDITIVE
*
*  Use this routine instead of X3SETPRC.PRG, which is
*  included for backward compatibility of existing
*  VMP application code.
*
*  Copyright (c) 2004-2005 Visionpace   All Rights Reserved
*                17501 East 40 Hwy., Suite 218
*                Independence, MO 64055
*                816-350-7900 
*                http://www.visionpace.com
*                http://vmpdiscussion.visionpace.com
*  Author:  Drew Speedie and Art Bergquist
*
*  Usage:
*    Specify a single procedure file:
*      X8SETPRC("MYPRG")
*      X8SETPRC("MYPRG.PRG")
*      IF NOT X8SETPRC("MYPRG.PRG")
*        * take the appropriate action
*      ENDIF
*
*    Specify more than one procedure file:
*      X8SETPRC("MYPRG, YOURPRG")
*      X8SETPRC("MYPRG.PRG, YOURPRG.PRG")
*      IF NOT X8SETPRC("MYPRG.PRG, YOURPRG.PRG")
*        * take the appropriate action
*      ENDIF
*
*  lParameters
*    tcProcFiles (R) Name of the .PRG procedure file(s) to 
*                    be added to the SET PROCEDURE list.  
*                    Note that you can specify one or more 
*                    procedure files separated by commas.
*                    The ".PRG" extension is optional.
*
*
LPARAMETERS tcProcFiles
LOCAL lcProcFiles, llSuccess 
IF VARTYPE(tcProcFiles) = "C" AND NOT EMPTY(tcProcFiles)
  lcProcFiles = UPPER(ALLTRIM(tcProcFiles))
 ELSE
  RETURN .f.
ENDIF
llSuccess = .t.
TRY 
IF OCCURS(",",lcProcFiles) = 0
  *
  *  name indirection is a little faster, and can be used
  *  when only one file is specified
  * 
  SET PROCEDURE TO (lcProcFiles) ADDITIVE 
 ELSE
  SET PROCEDURE TO &lcProcFiles. ADDITIVE
ENDIF
CATCH
*
*  any failure in the above SET PROCEDURE TO commands
*  results in llSuccess (the value RETURNed from here)
*  being set to .F.
*
llSuccess = .f.
ENDTRY
RETURN llSuccess
