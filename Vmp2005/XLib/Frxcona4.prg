**  Procedure.........: FRXConA4.PRG
**  Description.......: to convert all the .frx reports in the
**                      current directory from the American
**                      standard format to the British A4 standard
**                      adjust the width of the DATE()
**                      and ensure the Page number details print OK
**  Dependencies......: *.frx in \vm sub-directory
**  Calling Samples...: do FRXConA4

lcOldAli = ALIAS()
SELECT 0
=ADIR( arFrx, '*.FRX')
FOR li = 1 TO ALEN( arFrx, 1)  STEP 1
  USE ( arFRX[ li, 1])
  ** change the setting in the first record from Letter to A4
  REPLACE Expr WITH SUBSTR( Expr, 1, AT( "PAPERSIZE", Expr) - 2) + ;
   "PAPERSIZE=9" + SUBSTR( Expr, AT( "PAPERSIZE", Expr) + 12), ;
   Width WITH 77433.000
  LOCATE FOR TRIM( Expr) = 'DATE()'
  IF FOUND()
    REPLACE HPos WITH 1562.5, Width WITH 8854.167
  ENDIF
  LOCATE FOR TRIM( Expr) = '"Page "'
  IF FOUND()
    REPLACE HPos WITH 66250.000
  ENDIF
  LOCATE FOR TRIM( Expr) = "_PAGENO"
  IF FOUND()
    REPLACE HPos WITH 70416.667
  ENDIF    
  USE
ENDFOR
IF ! EMPTY( lcOldAli)
  SELECT lcOldAli
ENDIF  
RETURN  
 