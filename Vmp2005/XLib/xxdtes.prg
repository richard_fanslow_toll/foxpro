*
*  XXDTES.PRG
*  Wrapper for the EDITSOURCE() function, 
*  introduced in VFP 7.0, with an enhancement to allow 
*  passing the 2nd parameter as a STRING that will be 
*  searched in the indicated source code for its line 
*  number, for passing on to EDITSOURCE() -- you don't have 
*  to know the target line number, just the (first 
*  occurrence of a string contained on that line.
*
*  Copyright (c) 2002-2005 Visionpace   All Rights Reserved
*                17501 East 40 Hwy., Suite 218
*                Independence, MO 64055
*                816-350-7900 
*                http://www.visionpace.com
*                http://vmpdiscussion.visionpace.com
*  Author:  Drew Speedie
*
*  VMP examples:
*
*    XXDTES("XXFWCTRL.VCX","DO CASE","cusCKCheck","CheckIt")
*
*    XXDTES("XXFWNTBR.VCX","FIELD RULE IMPLEMENTATION VARIATIONS","cusRule","zreadme")
*
*    XXDTES("XXFWFRM.VCX",".ActivePage =","frmDEGridNav2Pages","pgfPageRefresh1.Page1.grdNav.SelectionMade")
*
*    XXDTES("NTDECUS.SCX",2,"AnyThing","pgfPagerefresh1.Page1.grdNav.ShelladditionalInit")
*
*    XXDTES("VMDECUS.SCX",2,"AnyThing","pgfPagerefresh1.Page1.grdNav.ShelladditionalInit")
*


*  lParameters
*    tcParm1 (R) 1st EDITSOURCE() parameter
*                Must be a .PRG/.VCX/.SCX file name
*    tuParm2 (O) May be an explicit line number (2nd EDITSOURCE() 
*                  parameter)
*                -OR-
*                A string contained in a line of the .PRG/method,
*                  in which case the line number will be 
*                  determined here (this is the reason this
*                  routine exists)
*    tuParm3 (O) 3rd EDITSOURCE() parameter
*    tuParm4 (O) 4th EDITSOURCE() parameter
*    

LPARAMETERS tcParm1, tuParm2, tuParm3, tuParm4 

IF UPPER(JUSTFNAME(SYS(16,0))) = "XXDT."   &&& XXDT.FXP
  MESSAGEBOX(JUSTFNAME(PROGRAM()) + ;
             " cannot be run directly from this XXDT utility " + ;
             "because XXDTES() has no meaning in an XXDT.PRG context.", ;
             48,"Please Note")
  RETURN 
ENDIF

IF NOT VARTYPE(tcParm1) = "C" OR EMPTY(tcParm1) ;
     OR OCCURS(".",tcParm1) = 0
  ASSERT .f. message "tcParm1 must be passed as a non-blank/non-null file name string"
  RETURN
ENDIF

LOCAL lnLine, lcLine, xx, laLines[1], llError, ;
      lcFileName, llPRG, llVCX, llSCX, lnPCount, lcObjName, ;
      lcMethod, lnSelect

lnPCount = PCOUNT()      
lcFileName = UPPER(ALLTRIM(tcParm1))
llPRG = JUSTEXT(lcFileName) = "PRG"
llVCX = JUSTEXT(lcFileName) = "VCX"
llSCX = JUSTEXT(lcFileName) = "SCX"

IF NOT llPRG AND NOT llVCX AND NOT llSCX
  llPRG = .t.
ENDIF

IF NOT FILE(lcFileName)
  MESSAGEBOX("Unable to locate " + lcFileName + ".",16,"Please Note")
  RETURN
ENDIF
      
lcObjName = SPACE(0)
lcMethod = SPACE(0)
IF llSCX OR llVCX
  IF lnPCount > 2 AND VARTYPE(tuParm3) = "C" AND NOT EMPTY(tuParm3)
    lcObjName = UPPER(ALLTRIM(tuParm3))
  ENDIF
  IF lnPCount > 3 AND VARTYPE(tuParm4) = "C" AND NOT EMPTY(tuParm4)
    lcMethod = UPPER(ALLTRIM(tuParm4))
  ENDIF
ENDIF
IF (llVCX OR llSCX) AND EMPTY(lcObjName)
  MESSAGEBOX("Please pass the 3rd parameter indicating the Object to be edited",16,"Please Note")
  RETURN 
ENDIF

*
*  process tuParm2
*
DO CASE
  CASE VARTYPE(tuParm2) = "N" AND tuParm2 > 0
    lnLine = INT(tuParm2)
  CASE VARTYPE(tuParm2) = "N"  
    lnLine = 1
  CASE NOT VARTYPE(tuParm2) = "C" OR ISNULL(tuParm2) OR EMPTY(tuParm2)
    lnLine = 1
  OTHERWISE
    *
    *  vartype(tuParm2) = "C" AND !isnull(tuParm2) AND !empty(tuParm2)
    *
ENDCASE

lnSelect = SELECT(0)
IF VARTYPE(tuParm2) = "C"
  *
  *  find the line number in the indicated method/PRG
  *
  DO CASE
    ****************************************
    CASE llPRG
    ****************************************
      TRY
      ALINES(laLines,FILETOSTR(lcFileName))
      CATCH
      llError = .t.
      ENDTRY
      IF NOT llError
        lnLine = 0
        FOR xx = 1 TO ALEN(laLines,1)
          lnLine = lnLine + 1
          lcLine = UPPER(ALLTRIM(laLines[xx]))
          IF UPPER(ALLTRIM(tuParm2)) $ lcLine
            EXIT
          ENDIF
        ENDFOR
      ENDIF  
    ****************************************
    CASE llVCX OR llSCX
    ****************************************
      TRY
      USE (tcParm1) AGAIN IN 0 ALIAS XXDTES SHARED
      CATCH
      llError = .t.
      ENDTRY
      IF NOT llError
        SELECT XXDTES
        LOCAL lcActualObjName, lcActualMethod, lcParent
        IF OCCURS(".",lcMethod) = 0
          *
          *  4th parameter passed in as just a method name:
          *  XXDTES("XXFWNTBR.VCX","FIELD RULE IMPLEMENTATION VARIATIONS","cusRule","zreadme")
          *
          IF llSCX
            *
            *  lcObjName is the ObjName for the Form record, no
            *  matter what is passed as the 3rd parameter
            *
            LOCATE FOR UPPER(BaseClass) == "FORM"
            lcObjName = UPPER(ALLTRIM(ObjName))
          ENDIF
          lcActualObjName = UPPER(ALLTRIM(lcObjName))
          lcActualMethod = UPPER(ALLTRIM(lcMethod))
          lcParent = SPACE(0)
         ELSE
          *
          *  4th parameter passed in with a containership
          *  hierarchy respecting lcObjName:
          *  XXDTES("XXFWFRM.VCX",".ActivePage =","frmDEGridNav2Pages","pgfPageRefresh1.Page1.grdNav.SelectionMade")
          *
          lcActualObjName = JUSTSTEM(lcMethod)  &&& parse off the method name
          IF OCCURS(".",lcActualObjName) > 0
            lcActualObjName = JUSTEXT(lcActualObjName)   &&& parse off the containership hierarchy
          ENDIF
          lcActualObjName = UPPER(lcActualObjName)
          lcActualMethod = UPPER(JUSTEXT(lcMethod))
          lcParent = lcObjName + "." + tuParm4
          lcParent = JUSTSTEM(lcParent)  &&& parse off the method name
          lcParent = JUSTSTEM(lcParent)  &&& parse off the object name
          IF llSCX
            *
            *  no matter what is passed as the 3rd parameter, 
            *  make sure lcParent begins with the Form name
            *
            lcParent = SUBSTRC(lcParent,AT_C(".",lcParent))  &&& parse off the Form name
            LOCATE FOR UPPER(BaseClass) == "FORM"             
            lcParent = ALLTRIM(ObjName) + lcParent   &&& add the actual Form name
          ENDIF
          *
          *  NOTE that the above logic only covers the scenario
          *  where the object indicated in lcMethod is a member
          *  added to lcParent at the instance level, NOT when
          *  the object indicated in lcMethod is a member of
          *  a class added to a class from which lcParent 
          *  inherits
          *
          #IF .f.
			lcMethod has no .
	        	must be a method in lcObjName record
			lcMethod has at least one .  
				is a method in lcObjName record if lcMethod object is a 
				member of a class
				-OR-
				is a method in a separate record whose Parent is
				lcObjName plus any containership hierarchy down to the
				individual object name
				-OR-
				is a method in a separate record whose Parent is
				lcObjName plus any combination containership hierarchy
				down to whatever level the actual object is
		  #ENDIF
        ENDIF
        
        SELECT XXDTES
        LOCATE FOR NOT DELETED() ;
             AND UPPER(ALLTRIM(ObjName)) == lcActualObjName ;
             AND UPPER(ALLTRIM(Platform)) == "WINDOWS" ;
             AND UPPER(ALLTRIM(Parent)) == UPPER(ALLTRIM(lcParent)) &&& can fail - see NOTE above 

        IF NOT FOUND()
          *  see NOTE above
          IF llSCX AND OCCURS(".",lcMethod) > 0
            *
            *  see if we can find the specified method in a
            *  member of the class on which the .SCX form is 
            *  based
            * 
            *  for example:
            *    XXDTES("VMDECUS.SCX","CUS_NAME","Form","pgfPageRefresh1.Page1.grdNav.SetIndexTagInfo")
            *  note that the target ShellAdditionalInit() is not
            *  in a separate record for grdNav, but rather in 
            *  the Form record itself, because grdNav is a member
            *  added to a form class up the form hierarchy
            * 
            lcParent = SUBSTRC(lcParent,AT_C(".",lcParent)+1)
            lcActualMethod = lcParent + ;
                             "." + lcActualObjName + ;
                             "." + lcActualMethod
            lcActualMethod = UPPER(lcActualMethod)                             
            LOCATE FOR NOT DELETED() ;
                 AND UPPER(ALLTRIM(Platform)) = "WINDOWS" ;
                 AND UPPER(BaseClass) = "FORM" ;
                 AND UPPER(lcActualMethod) $ UPPER(Methods)
            IF NOT FOUND()
              *
              *  it looks like the specified method belongs to
              *  an object that is a member of a class that 
              *  either:
              *  - has been added to this form as an instance
              *  OR
              *  - has been added to a class that has been added
              *    to a class that has been added to this form
              *    as an instance
              *  OR 
              *  - has been added to a form class from which
              *    this form inherits
              *  OR
              *  - has been added to a class that has been added
              *    to a form class from which this form inherits
              *
            ENDIF
          ENDIF
        ENDIF

        IF FOUND()
          ALINES(laLines,XXDTES.Methods)
          FOR xx = 1 TO ALEN(laLines,1)
            lcLine = UPPER(ALLTRIM(laLines[xx]))
            IF lcLine == "PROCEDURE " + lcActualMethod
              *
              *  initialize lnLine to a numeric to signal that
              *  it's time to start looking at each line  
              *
              lnLine = 0
              LOOP 
            ENDIF
            IF VARTYPE(lnLine) = "N" 
              lnLine = lnLine + 1
              IF UPPER(ALLTRIM(tuParm2)) $ lcLine
                *
                *  bingo
                *
                EXIT
              ENDIF
            ENDIF  &&& VARTYPE(lnLine) = "N" 
          ENDFOR   &&& each Methods line
        ENDIF   &&& FOUND()
      ENDIF  &&& !llError
      USE IN SELECT("XXDTES")
    ****************************************
    OTHERWISE
    ****************************************
      *
      *  ???
      *   
      llError = .t.
  ENDCASE
  IF llError OR NOT VARTYPE(lnLine) = "N" OR lnLine < 1
    lnLine = 1
  ENDIF  
ENDIF   &&& VARTYPE(tuParm2) = "C"

DO CASE
  **************************************************
  CASE lnPCount = 1
  **************************************************
    EDITSOURCE(tcParm1)
  **************************************************
  CASE lnPCount = 2 OR EMPTY(lcObjName) AND EMPTY(lcMethod)
  **************************************************
    EDITSOURCE(tcParm1,lnLine)
  **************************************************
  CASE NOT EMPTY(lcObjName) AND NOT EMPTY(lcMethod)
  **************************************************
    EDITSOURCE(tcParm1,lnLine,lcObjName,lcMethod) 
  **************************************************
  CASE NOT EMPTY(lcObjName) AND EMPTY(lcMethod)
  **************************************************
    EDITSOURCE(tcParm1,lnLine,lcObjName)
  **************************************************
  OTHERWISE
  **************************************************
    SELECT (lnSelect)
    RETURN
ENDCASE
SELECT (lnSelect)

*
*  select/highlight the line of text/code
*
KEYBOARD "{SHIFT+DNARROW}" PLAIN CLEAR 

RETURN
