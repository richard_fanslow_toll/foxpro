*
*  XXWBRI.PRG
*  Builder for the VMP RI "engine".
*   
*  Copyright (c) 1998-2005 Visionpace   All Rights Reserved
*                17501 East 40 Hwy., Suite 218
*                Independence, MO 64055
*                816-350-7900 
*                http://www.visionpace.com
*                http://vmpdiscussion.visionpace.com
*  Author:  Drew Speedie and Art Bergquist
*           Special thanks to Mike Yearwood
*
LOCAL lcSetClassLib, xxwbri
lcSetClassLib = SET("CLASSLIB")
SET CLASSLIB TO XXWB.VCX
*
*    MODIFY CLASS frmXXWBRI OF xxwb
*
xxwbri = CREATEOBJECT("frmXXWBRI") 
IF TYPE("xxwbri.baseclass") = "C"
  xxwbri.Show()
ENDIF
SET CLASSLIB TO &lcSetClasslib
RETURN 

