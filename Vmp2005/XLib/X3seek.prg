*
*  X3SEEK.PRG
*  SEEK() a value, return the FOUND() condition.
*
*  NOTE:  this routine leaves the record pointer in the 
*         SEEK()ed table where it was BEFORE calling X3SEEK 
*         -- the only purpose of this routine is to return 
*         a logical value indicating whether or not the 
*         SEEK()ed value was FOUND() 
*
*  Copyright (c) 1997-2005 Visionpace   All Rights Reserved
*                17501 East 40 Hwy., Suite 218
*                Independence, MO 64055
*                816-350-7900 
*                http://www.visionpace.com
*                http://vmpdiscussion.visionpace.com
*  Author:  Drew Speedie
*
*
*  Features:
*    - After the first (tcDatabase) parameter, the next 3 
*      parameters match those you'd pass to SEEK()
*    - The table in which the SEEK() will be performed doesn't
*      need to be open.  If it is not open now, it will be left
*      closed on leaving here.
*    - Can be used with free tables and contained tables.  For
*      free tables, don't pass the first tcDatabase parameter
*    - Can do a SEEK() in a (not open) contained table whose
*      tablename is the same as an open table with the same 
*      alias at the moment (allows SEEK()s in a table with 
*      the same name as an open table, tables contained in 
*      different .DBCs)
*
*  REQUIRES:
*    X2ISTAG.PRG
*
* 
*  Usage:
*    ? x3seek("VM","00001","CUSTOMERS","CUS_PK")
* 
* 
*  lParameters
*      tcDatabase (O) Database that contains the lookup table
*                       If you pass this parameter, the table
*                         contained in tcDatabase indicated by
*                         tcAlias is used for the SEEK, even if 
*                         there is an open alias of the same 
*                         name as tcAlias that is either contained
*                         in a different .DBC or is a free table 
*                       If you don't pass this parameter, one of
*                         two things happens:
*                         If tcAlias is open, it is used for the SEEK
*                         If tcAlias is not open, we assume a free table 
*     tuSeekValue (R) Value to SEEK in tcAlias -- SEEK(parm1,,)
*         tcAlias (R) Alias in which to do the SEEK -- SEEK(,parm2,)
*           tcTag (R) Index tag to be in effect for the SEEK -- SEEK(,,parm3)
* tlForceUseAgain (O) Force a USE AGAIN for the SEEK()?
*                     If tlForceUseAgain = .f. or not passed,
*                       if tcAlias is already open, it will be used
*                       for the SEEK().  Beware that if tcAlias is
*                       buffered, and the buffer is dirty, the
*                       SEEK() will behave according to the buffer,
*                       not the table values on disk.
*                     If tlForceUseAgain = .t., tcAlias will be
*                       USEd AGAIN, whether it is already open or
*                       not.  Beware that the SEEK() will succeed
*                       or fail based only on table values on disk,
*                       and buffered data is ignored.
*                    
lparameters tcDatabase, ;
            tuSeekValue, ;
            tcAlias, ;
            tcTag, ;
            tlForceUseAgain
LOCAL lcDatabase, lcAlias, lcTag
IF type("tcDatabase")#"C" OR empty(tcDatabase)
  lcDatabase = space(0)
 ELSE
  lcDatabase = upper(alltrim(tcDatabase))
ENDIF
IF type("tcAlias")#"C" OR empty(tcAlias) 
  return .f. 
 ELSE
  lcAlias = upper(alltrim(tcAlias)) 
ENDIF
IF type("tcTag")#"C" OR empty(tcTag) 
  return .f.
 ELSE
  lcTag = upper(alltrim(tcTag))
ENDIF
IF type("tlForceUseAgain") = "L"
 ELSE
  tlForceUseAgain = .f.
ENDIF

local llCloseDBC, ;
      llCloseAlias, ;
      lcSetDatabase, ;
      loRecno, ;
      llRetVal, ;
      lcOnError, ;
      lcSaveAlias, ;
      llOpenNow

llCloseDBC    = .f.
llCloseAlias  = .t.
llRetVal      = .f.
lcSetDatabase = set("DATABASE")
lnRecno       = 0
lcSaveAlias   = alias()
llOpenNow     = .f.

IF empty(lcDatabase)
  IF used(lcAlias) AND !tlForceUseAgain
    *
    *  use it!
    *
    llCloseAlias = .f.
    llCloseDBC   = .f. 
   ELSE
    *
    *  we're assuming a free table here
    *
    llCloseAlias = .f.
    llCloseDBC   = .f.
    set database to
    lcOnError = ON("ERROR")
    on error llRetVal = .NULL.
    use (lcAlias) in 0 again alias X3SEEK
    on error &lcOnError
    IF !isnull(llRetVal)
      lcAlias = "X3SEEK"
      llCloseAlias = .t.
    ENDIF
  ENDIF
 ELSE !empty(lcDatabase)
  *
  * see if lcAlias is open
  *
  IF used(lcAlias) AND !tlForceUseAgain
    *
    * see if it's contained in lcDatabase
    *
    IF dbused(lcDatabase)
      set database to (lcDataBase)
     ELSE 
      open database (lcDataBase)
    ENDIF 
    IF indbc(cursorgetprop("SourceName",lcAlias),"TABLE")
      *
      * use it!
      *
      llCloseAlias = .f. 
      llCloseDBC   = .f. 
      llOpenNow    = .f.
     ELSE
      *
      * lcAlias is open, but it's not in lcDatabase
      * so we need to open the one in lcDatabase
      *
      llOpenNow = .t.
    ENDIF  &&&  indbc(lcAlias)
   ELSE
    llOpenNow = .t. 
  ENDIF   &&& used(lcAlias)
  IF llOpenNow
    *
    *  either !used(lcAlias) or it's open but not
    *  from the passed lcDatabase
    *
    local xx
    xx = dbused(lcDatabase)
    lcOnError = ON("ERROR")
    on error llRetVal = .NULL.
    use (lcDatabase+"!"+lcAlias) in 0 again alias X3SEEK
    on error &lcOnError
    IF isnull(llRetVal)
      llCloseAlias = .f.
      IF dbused(tcDatabase)
        llCloseDBC = !xx
      ENDIF
     ELSE
      lcAlias      = "X3SEEK"
      llCloseAlias = .t.
      llCloseDBC   = !xx
    ENDIF
  ENDIF   &&& llOpenNow
ENDIF   &&& !empty(tcDatabase)  
IF isnull(llRetVal)
  *
  *  we couldn't open lcAlias
  *
  IF llCloseDBC
    set database to (lcDatabase)
    close databases
    IF empty(lcSetDatabase)
      set database to
     ELSE
      set database to (lcSetDatabase)
    ENDIF
  ENDIF
  return .f.
ENDIF

IF !llCloseAlias 
  lnRecno = IIF(eof(lcAlias),0,recno(lcAlias))
ENDIF

IF x2istag(lcTag,lcAlias)
  llRetVal = seek(tuSeekValue,lcAlias,lcTag)
 ELSE
  ASSERT .f. message + ;
       "The " + m.tcTag + " index tag passed to " + PROGRAM() + ;
       " does not exist for the " + m.lcAlias + " cursor."
  llRetVal = .f. 
ENDIF

IF llCloseAlias
  use in (lcAlias)
 ELSE
  IF lnRecno # 0
    goto lnRecno in (lcAlias)
  ENDIF
ENDIF

IF llCloseDBC
  set database to (lcDatabase)
  close databases   &&& lcDatabase 
ENDIF  &&& empty(lcDatabase)

IF empty(lcSetDatabase)
  set database to
 ELSE
  set database to (lcSetDatabase)
ENDIF

IF empty(lcSaveAlias)
  select 0
 ELSE
  select (lcSaveAlias)
ENDIF  

return llRetVal
 