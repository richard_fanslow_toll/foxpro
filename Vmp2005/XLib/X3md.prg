*
*  X3MD.PRG
*  Returns a logical value indicating whether or not
*  the passed directory was created (if it already
*  exists, it will not be created)
*
*  Copyright (c) 1997-2005 Visionpace   All Rights Reserved
*                17501 East 40 Hwy., Suite 218
*                Independence, MO 64055
*                816-350-7900 
*                http://www.visionpace.com
*                http://vmpdiscussion.visionpace.com
*  Author:  Drew Speedie
*           Special thanks to Dale Kiefling
*
*
*  Usage:
*  IF X3MD("C:\SOMEDIR")
*    * do something
*  ELSE
*    * do something else
*  ENDIF
*
*  lParameters
*    tcDirectoryName (R) Name of the directory to be created
*
LPARAMETERS tcDirectoryName
LOCAL llSuccess
IF NOT TYPE("tcDirectoryName")="C" OR EMPTY(tcDirectoryName)
  * invalid parameter
 ELSE
  tcDirectoryName = UPPER(ALLTRIM(tcDirectoryName))
  IF (BETWEEN(LEFTC(tcDirectoryName,1),"A","Z") AND SUBSTRC(tcDirectoryName,2,2)=":\") ;
       OR NOT LEFTC(tcDirectoryName,2)="\\"
    IF RIGHTC(tcDirectoryName,1) = "\"
      tcDirectoryName = LEFTC(tcDirectoryName,LENC(tcDirectoryName)-1)
    ENDIF
    LOCAL lcOnError, llSuccess
    llSuccess = .t.
    lcOnError = on("ERROR")
    ON ERROR llSuccess = .f.
    MD (tcDirectoryName)
    ON ERROR &lcOnError
   ELSE
    * invalid drive designation
  ENDIF   &&&  valid drive designation
ENDIF   &&&  invalid parameter
RETURN llSuccess


