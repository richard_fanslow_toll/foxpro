*
*  X7ISAPIF.PRG
*  RETURN a logical value indicating whether the passed
*  function name is a Windows API function (in a Windows 
*  .DLL) that is currently loaded via the DECLARE command
*
*  Copyright (c) 1997-2005 Visionpace   All Rights Reserved
*                17501 East 40 Hwy., Suite 218
*                Independence, MO 64055
*                816-350-7900 
*                http://www.visionpace.com
*                http://vmpdiscussion.visionpace.com
*  Author:  Drew Speedie
*  
*  This function uses:
*  1- The ADLLS() function introduced in VFP 7.0,
*  2- The optional 6th parameter added to the ASCAN()
*     function in VFP 7.0
*
*  Examples:
*!*	IF NOT X7ISAPIF("MessageBeep")
*!*	  DECLARE Long MessageBeep IN USER32.DLL Long uType 
*!*	ENDIF
*!*	MessageBeep(0)
*
*!*	IF NOT X7ISAPIF("MessageBeepWithAlias")
*!*	  DECLARE Long MessageBeep IN USER32.DLL AS MessageBeepWithAlias Long uType 
*!*	ENDIF
*!*	MessageBeep(0)
*
*!*	IF NOT X7ISAPIF("MessageBeepWithAlias","MessageBeep")
*!*	  DECLARE Long MessageBeep IN USER32.DLL AS MessageBeepWithAlias Long uType 
*!*	ENDIF
*!*	MessageBeep(0)
*
*
*  lParameters
*   tcFunctionAlias (R) The alias of the API Function
*                         to be tested.  
*                       By default, the alias is the same 
*                         as the name of the function, but 
*                         you can
*                           DECLARE DLL .. AS <Alias>
*                         ...so this parameter can be passed
*                         as the function name when it is
*                         not DECLARED .. AS <Alias>
*    tcFunctionName (O) If you pass tcFunctionAlias and
*                         want/need to ensure that this
*                         routine only RETURNs .T. when
*                         tcFunctionAlias is the alias for
*                         a DECLAREation of a specific
*                         Function Name, pass that function
*                         name as this parameter
*
LPARAMETERS tcFunctionAlias, tcFunctionName
LOCAL laDLLs[1], lnRow
IF ADLLS(m.laDLLs) = 0
  RETURN .f.
ENDIF
lnRow = ASCAN(laDLLs,m.tcFunctionAlias,1,-1,2,15)
IF m.lnRow = 0
  RETURN .f.
ENDIF
IF PCOUNT() = 1 ;
     OR NOT VARTYPE(m.tcFunctionName) = "C" ;
     OR EMPTY(m.tcFunctionName)
  RETURN .t.
ENDIF
*
*  tcFunctionName has been passed
*
RETURN UPPER(ALLTRIM(m.laDLLs[m.lnRow,1])) == UPPER(ALLTRIM(m.tcFunctionName))

