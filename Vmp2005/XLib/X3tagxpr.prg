*
*  X3TAGXPR.PRG
*  Returns the index expression for the passed 
*  tag name of the (optional) passed alias.
*  NOTE that the tag must belong to a STRUCTURAL INDEX
*  (.CDX)!!
*
*  Copyright (c) 1997-2005 Visionpace   All Rights Reserved
*                17501 East 40 Hwy., Suite 218
*                Independence, MO 64055
*                816-350-7900 
*                http://www.visionpace.com
*                http://vmpdiscussion.visionpace.com
*  Author:  Drew Speedie
*
*  If anything goes wrong/cannot be processed, this
*  routine RETURNs SPACE(0).
*
*  Usage:
*    lcTagExpr = X3TAGXPR("CUS_NAME","CUSTOMERS")
*
*  lParameters
*    tcTagName (R) Name of the tag whose index expression is to
*                    be returned
*      tcAlias (O) Alias of the (open) table containing tcTagName 
*                  Defaults to ALIAS() -- if not passed
*                    ALIAS() must contain tcTagName
*
*                  
LPARAMETERS tcTagName, tcAlias

IF TYPE("tcTagName") = "C" AND !EMPTY(tcTagName)
  LOCAL lcTagName
  lcTagName = UPPER(ALLTRIM(tcTagName))
 ELSE
  RETURN SPACE(0)
ENDIF
LOCAL lcAlias
IF TYPE("tcAlias") = "C" AND !EMPTY(tcAlias) 
  lcAlias = UPPER(ALLTRIM(tcAlias))
 ELSE
  IF EMPTY(ALIAS())
    RETURN SPACE(0)
   ELSE
    lcAlias = ALIAS()
  ENDIF
ENDIF
IF !USED(lcAlias)
  RETURN SPACE(0)
ENDIF
*!*	return key(tagno(lcTagName,cdx(1,lcAlias),lcAlias),lcAlias)
LOCAL xx, lcCDX, lnTagNo, lcRetVal
lcRetVal = SPACE(0)
*
*  accommodate up to 10 .CDX files
*
FOR xx = 1 TO 10
  lcCDX = CDX(xx,lcAlias)
  IF EMPTY(lcCDX)
    *
    *  we've reached the last .CDX for lcAlias
    *
    EXIT 
  ENDIF
  lnTagNo = TAGNO(lcTagName,lcCDX,lcAlias)
  IF !EMPTY(lnTagNo)
    *
    *  we've found lcTagName in lcCDX of lcAlias
    *
    lcRetVal = KEY(lcCDX,lnTagNo,lcAlias)
    EXIT 
  ENDIF
ENDFOR
RETURN lcRetVal
