*
*  XXWBRPFP.PRG
*  Builder for a Runtime Picklist Form Parameter object or
*  code snippet.
*
*  Copyright (c) 2000-2005 Visionpace   All Rights Reserved
*                17501 East 40 Hwy., Suite 218
*                Independence, MO 64055
*                816-350-7900 
*                http://www.visionpace.com
*                http://vmpdiscussion.visionpace.com
*  Author:  Drew Speedie and Art Bergquist
*           Special thanks to Mike Yearwood
*
*

*
*    MODIFY CLASS cusRuntimePicklistFormParameter OF XXFWPICK METHOD zReadMe
*
*  Note that this builder includes an example of an
*  RPF, presenting an frmPicklist when the target
*  .SCX or .VCX contains more than one grid
*
LPARAMETERS tcFileName

PRIVATE paProps 
DIMENSION paProps[1]
LoadProps()
PRIVATE paColumns
DIMENSION paColumns[1]

IF (NOT VARTYPE(tcFileName) = "C" OR EMPTY(tcFileName)) ;
     AND ;
     MESSAGEBOX("This utility can generate either:" + CHR(13) + CHR(13) ;
     + "(a) cusRuntimePicklistFormParameter " ;
     + "properties code from a specific existing grid (select <Yes>)" ;
     + CHR(13) + CHR(13) ;
     + SPACE(5) + "OR" ;
     + CHR(13) + CHR(13) ;
     + "(b) an empty set of cusRuntimePicklistFormParameter " ;
     + "properties, ready for use at the instance/concrete " ;
     + "subclass level (select <No>)", ;
     4+48, ;
     "Please select") = 7
  *
  *  NO
  *
  GenerateClipText()
  RETURN
ENDIF     

*
*  determine the .SCX/.VCX containing the target grid
*
LOCAL lcFileName
IF VARTYPE(tcFileName) = "C" AND NOT EMPTY(tcFileName) ;
     AND FILE(tcFileName)
  lcFileName = UPPER(ALLTRIM(tcFileName))
 ELSE
  lcFileName = UPPER(GETFILE("SCX,VCX",".SCX/.VCX","",0,"Specify .SCX or .VCX containing grid"))
ENDIF
IF EMPTY(lcFileName)
  RETURN
ENDIF

IF NOT INLIST(JUSTEXT(lcFileName),"SCX","VCX")
  MESSAGEBOX("The selected file must be an .SCX or .VCX " + ;
             "file, because it must contain a grid.  " + ;
             lcFileName + " is not an .SCX or .VCX.", ;
             48,"Please Note")
  RETURN
ENDIF

*
*  open the .SCX/.VCX
*
TRY
USE (lcFileName) IN 0 ALIAS XXDTRPFP NOUPDATE SHARED 
CATCH
ENDTRY
IF NOT USED("XXDTRPFP")
  MESSAGEBOX("Unable to open " + lcFileName + ".", ;
             48,"Please Note")
  RETURN
ENDIF

*
*  find the grid record 
*
LOCAL lnCount, lnRecno 
SELECT XXDTRPFP
COUNT TO lnCount FOR UPPER(BaseClass) = "GRID" ;
     OR UPPER(".Header1") $ UPPER(Properties)
IF lnCount = 0
  USE IN SELECT("XXDTRPFP")
  MESSAGEBOX(lcFileName + " does not contain any grids.", ;
             48,"Please Note")
  RETURN
ENDIF             
LOCATE FOR UPPER(BaseClass) = "GRID" ;
     OR UPPER(".Header1") $ UPPER(Properties)
lnRecno = RECNO()     
*
*  if there is more than one grid in this .SCX/.VCX,
*  find out which one to use
*
IF lnCount > 1
  lnRecno = GetSpecificGrid()
  IF ISNULL(lnRecno)
    USE IN SELECT("XXDTRPFP")
    RETURN 
  ENDIF
ENDIF
SELECT XXDTRPFP
GOTO (lnRecno)

*
*  process RecordSource/icRecordSource
*
ProcessRecordSource()

*
*  process iaColumnInfo[]
*
ProcessColumnInfo()

USE IN SELECT("XXDTRPFP")

GenerateClipText()

RETURN



PROCEDURE LoadProps
DIMENSION paProps[14,3]
paProps = SPACE(0)   &&& column 2
paProps[1,1] = "icRecordSource"
paProps[1,3] = "RecordSource for the called RPF frm??Picklist.grdPicklist1"
paProps[2,1] = "icFormCaption"
paProps[2,3] = "Caption for the called RPF frm??Picklist"
paProps[3,1] = "inFormHeight"
paProps[3,3] = "Height for the called RPF frm??Picklist"
paProps[4,1] = "inFormWidth"
paProps[4,3] = "Width for the called RPF frm??Picklist"
paProps[5,1] = "icReturnValueExpression"
paProps[5,3] = "icReturnValueExpression for the called RPF frm??Picklist"
paProps[6,1] = "iuInitialValue"
paProps[6,3] = "tuInitialValue (1st parameter received by frmPicklist.Init) value"
paProps[7,1] = "icFilter"
paProps[7,3] = "tcFilter (2nd parameter received by frmPicklist.Init) value"
paProps[8,1] = "ilNoAddButton"
paProps[8,3] = "tlNoAddButton (3rd parameter received by frmPicklist.Init) value"
paProps[9,1] = "icAddForm"
paProps[9,3] = "icAddForm property value for the called RPF frm??Picklist"
paProps[10,1] = "icSetRecordPointerOnAddTag"
paProps[10,3] = "Name of the Grid.RecordSource index tag used in THIS.SetRecordPointerOnAdd() to position the record added in the icAddForm.  Required when setting THIS.icAddForm in most n-Tier scenarios.  Copied to the frmPicklist.icSetRecordPointerOnAddTag"
paProps[11,1] = "icFormClass"
paProps[11,3] = "Required when creating an RPF parameter for a txtPicklistValid, specifies the ClassName,VCXFileName of the desired picklist form class, typically frm??Picklist,??FWPICK.VCX"
paProps[12,1] = "inFormDataSessionID"
paProps[12,3] = "DataSessionID to which the RPF frm??Picklist form is to be set, frequently _Screen.ActiveForm, if it exists"
paProps[13,1] = "icMTClass"
paProps[13,3] = "ClassName,VCXFileName of a BO/DS to provide the called RPF frm??Picklist.grdPicklist1.RecordSource"
paProps[14,1] = "icRecordSourceTag"
paProps[14,3] = "Name of an index tag to be initially set in the called RPF frm??Picklist"
RETURN



PROCEDURE ProcessRecordSource
*
*  process the RecordSource from the Properties Sheet
*  and/or method code
*
LOCAL laLines[1], lcLine
ALINES(laLines,Properties)
FOR lnCount = 1 TO ALEN(laLines,1)
  lcLine = ALLTRIM(laLines[lnCount])
  IF UPPER(lcLine) = "RECORDSOURCE =" OR ".RECORDSOURCE =" $ UPPER(lcLine)
    paProps[1,2] = ALLTRIM(GETWORDNUM(lcLine,2,"="))
    EXIT
  ENDIF
ENDFOR

*
*  process the RecordSource from the Methods, in case 
*  RecordSource isn't set in the Properties Sheet and/or
*  set in method code
*
RELEASE laLines
LOCAL laLines[1], lcJunk, lcString
ALINES(laLines,Methods)
FOR lnCount = 1 TO ALEN(laLines,1)
  lcLine = ALLTRIM(laLines[lnCount])
  IF SkipLine(lcLine)
    LOOP
  ENDIF
  IF "RECORDSOURCE =" $ UPPER(lcLine) ;
       OR "RECORDSOURCE=" $ UPPER(lcLine)
    paProps[1,2] = GetPropertyString(lcLine)
  ENDIF
ENDFOR
RETURN



PROCEDURE ProcessColumnInfo
*
*  process the paColumns[] array information
*
LOCAL laLines[1], lcLine, lcString, llColumnCount, xx, ;
      lcColumnNumber
ALINES(laLines,Properties)
*
*  determine how many columns we have - ColumnCount
*
FOR lnCount = 1 TO ALEN(laLines,1)
  lcLine = ALLTRIM(laLines[lnCount])
  IF "INCOLUMNCOUNT =" $ UPPER(lcLine) ;
       OR "INCOLUMNCOUNT=" $ UPPER(lcLine) ;
       OR "COLUMNCOUNT =" $ UPPER(lcLine) ;
       OR "COLUMNCOUNT=" $ UPPER(lcLine)
    lcString = ALLTRIM(GETWORDNUM(lcLine,2,"="))
    DIMENSION paColumns[VAL(lcString),6]
    llColumnCount = .t.
    EXIT 
  ENDIF
ENDFOR
IF llColumnCount
  *  look for inColumnCount in Methods
  RELEASE laLines
  LOCAL laLines[1]
  ALINES(laLines,Methods)
  FOR lnCount = 1 TO ALEN(laLines,1)
    lcLine = ALLTRIM(laLines[lnCount])
    IF SkipLine(lcLine)
      LOOP
    ENDIF
    IF ".INCOLUMNCOUNT =" $ UPPER(lcLine) ;
         OR ".INCOLUMNCOUNT=" $ UPPER(lcLine) ;
         OR ".COLUMNCOUNT =" $ UPPER(lcLine) ;
         OR ".COLUMNCOUNT=" $ UPPER(lcLine) 
      lcString = ALLTRIM(SUBSTRC(lcLine,AT_C("=",lcLine)))
    ENDIF
  ENDFOR
  IF !EMPTY(lcString)
    DIMENSION paColumns[VAL(lcString),6]    
    llColumnCount = .t.
  ENDIF
ENDIF
IF !llColumnCount
  DIMENSION paColumns[1,6]
  paColumns = REPLICATE(CHR(38),3) + " unable to determine column information"
  RETURN 
ENDIF
*
*  process column properties set in the Properties Sheet
*
RELEASE laLines
LOCAL laLines[1]
ALINES(laLines,Properties)
lcColumnNumber = SPACE(0)
FOR lnCount = 1 TO ALEN(laLines,1)
  lcLine = ALLTRIM(laLines[lnCount])
  *
  *  ControlSource
  *
  IF ".COLUMN" $ UPPER(lcLine) AND ".CONTROLSOURCE =" $ UPPER(lcLine) 
    lnColumn = GetColumnNumber(lcLine)
    IF lnColumn > ALEN(paColumns,1)
      *  this column is greater than the inColumnCount
      LOOP 
    ENDIF
    paColumns[lnColumn,1] = GetPropertyString(lcLine)
  ENDIF
  *
  *  Width
  *
  IF ".COLUMN" $ UPPER(lcLine) AND ".WIDTH =" $ UPPER(lcLine) 
    lnColumn = GetColumnNumber(lcLine)
    IF lnColumn > ALEN(paColumns,1)
      *  this column is greater than the inColumnCount
      LOOP 
    ENDIF
    paColumns[lnColumn,2] = GetPropertyString(lcLine)
  ENDIF
ENDFOR
*
*  process column properties set in methods
*
*  ControlSource
*  Width
*  iaColumnInfo column 3
*  iaColumnInfo column 4
*  iaColumnInfo column 5
*  iaColumnInfo column 6
*  iaColumnInfo column 7
*  iaColumnInfo column 8
*
RELEASE laLines
DIMENSION laLines[1]
ALINES(laLines,Methods)
FOR lnCount = 1 TO ALEN(laLines,1)
  lcLine = ALLTRIM(laLines[lnCount])
  IF SkipLine(lcLine)
    LOOP
  ENDIF
  *
  *  this loop will only find column settings if
  *  the ".ColumnExpN." qualifier is in the method
  *  code -- it won't find any column settings if
  *  only ".ControlSource" is specified because a
  *  WITH THIS.ColumnExpN is in effect
  *
  IF ".COLUMN" $ UPPER(lcLine) 
    IF ".CONTROLSOURCE =" $ UPPER(lcLine) ;
         OR ".CONTROLSOURCE=" $ UPPER(lcLine)
      lnColumn = GetColumnNumber(lcLine)
      IF lnColumn <= ALEN(paColumns,1)
        paColumns[lnColumn,1] = GetPropertyString(lcLine)
      ENDIF
    ENDIF
  ENDIF
  IF ".COLUMN" $ UPPER(lcLine) 
    IF ".WIDTH =" $ UPPER(lcLine) ;
         OR ".WIDTH=" $ UPPER(lcLine)
      lnColumn = GetColumnNumber(lcLine)
      IF lnColumn <= ALEN(paColumns,1)
        paColumns[lnColumn,2] = GetPropertyString(lcLine)
      ENDIF
    ENDIF
  ENDIF
  *
  *  now find .iaColumnInfo[] settings
  *
  lcLine = CHRTRAN(lcLine,SPACE(1),SPACE(0))   &&& get rid of spaces
  IF (UPPER(".iaColumnInfo[") $ UPPER(lcLine) ;
       OR UPPER(".iaColumnInfo(") $ UPPER(lcLine)) 
    IF ",3]=" $ lcLine OR ",3)=" $ lcLine
      lnColumn = GetColumnNumberFromArray(lcLine)
      IF lnColumn <= ALEN(paColumns,1)
        paColumns[lnColumn,3] = GetPropertyString(lcLine)
      ENDIF
    ENDIF
    IF ",4]=" $ lcLine OR ",4)=" $ lcLine
      lnColumn = GetColumnNumberFromArray(lcLine)
      IF lnColumn <= ALEN(paColumns,1)
        paColumns[lnColumn,4] = GetPropertyString(lcLine)
      ENDIF
    ENDIF
    IF ",5]=" $ lcLine OR ",5)=" $ lcLine
      lnColumn = GetColumnNumberFromArray(lcLine)
      IF lnColumn <= ALEN(paColumns,1)
        paColumns[lnColumn,5] = GetPropertyString(lcLine)
      ENDIF
    ENDIF
    IF ",6]=" $ lcLine OR ",6)=" $ lcLine
      lnColumn = GetColumnNumberFromArray(lcLine)
      IF lnColumn <= ALEN(paColumns,1)
        paColumns[lnColumn,6] = GetPropertyString(lcLine)
      ENDIF
    ENDIF
    IF ",7]=" $ lcLine OR ",7)=" $ lcLine
      lnColumn = GetColumnNumberFromArray(lcLine)
      IF lnColumn <= ALEN(paColumns,1)
        paColumns[lnColumn,7] = GetPropertyString(lcLine)
      ENDIF
    ENDIF
    IF ",8]=" $ lcLine OR ",8)=" $ lcLine
      lnColumn = GetColumnNumberFromArray(lcLine)
      IF lnColumn <= ALEN(paColumns,1)
        paColumns[lnColumn,8] = GetPropertyString(lcLine)
      ENDIF
    ENDIF
    IF ",9]=" $ lcLine OR ",9)=" $ lcLine
      lnColumn = GetColumnNumberFromArray(lcLine)
      IF lnColumn <= ALEN(paColumns,1)
        paColumns[lnColumn,9] = GetPropertyString(lcLine)
      ENDIF
    ENDIF
  ENDIF

ENDFOR


RETURN



PROCEDURE SkipLine
*
*  returns a logical value indicating whether the
*  passed method line should be skipped because it
*  is a comment
*
*  NOTE that this simple procedure does NOT handle
*  comment-continuation lines or lines inside 
*  #IF..#ENDIF blocks
*
LPARAMETERS tcLine
IF tcLine = "*" ;
     OR UPPER(tcLine) = "NOTE " ;
     OR tcLine = REPLICATE(CHR(38),2)   &&& ampersands
  *  comment line
  RETURN .t.
ENDIF
IF UPPER(tcLine) = "PROCEDURE " OR UPPER(tcLine) = "ENDPROC"
  RETURN .t.
ENDIF
RETURN .f.



PROCEDURE GetPropertyString
LPARAMETERS tcLine
LOCAL lcJunk
*  get the text after the "="
lcJunk = ALLTRIM(SUBSTRC(tcLine,AT_C("=",tcLine)+1))
*!*	IF INLIST(LEFTC(lcJunk,1),["],['],"[")
*!*	  *  remove delimiters (if any -- the assignment could
*!*	  *  be to a memvar or property)
*!*	  lcJunk = CHRTRAN(lcJunk,["],SPACE(0))
*!*	  lcJunk = CHRTRAN(lcJunk,['],SPACE(0))
*!*	  lcJunk = CHRTRAN(lcJunk,"[",SPACE(0))
*!*	  lcJunk = CHRTRAN(lcJunk,"]",SPACE(0))    
*!*	 ELSE
*!*	  *  looks like the assignment is a
*!*	  *  variable or property value
*!*	ENDIF
RETURN lcJunk



PROCEDURE GetColumnNumber
LPARAMETERS tcLine
LOCAL lcString, lcColumnNumber, xx
*  parse off everything up to the column number
lcString = SUBSTRC(tcLine,AT_C(".COLUMN",UPPER(tcLine))+7)
*  find just the column number
lcColumnNumber = SPACE(0)
FOR xx = 1 TO 100
  IF !ISDIGIT(SUBSTRC(lcString,xx,1))
    EXIT
  ENDIF
  lcColumnNumber = lcColumnNumber + SUBSTRC(lcString,xx,1)
ENDFOR        
RETURN INT(VAL(lcColumnNumber))


 
PROCEDURE GetColumnNumberFromArray
LPARAMETERS tcLine
LOCAL lcLine, lcString, lcColumnNumber, xx
lcLine = UPPER(tcLine)
IF OCCURS(".IACOLUMNINFO[",lcLine) = 1
  lcString = SUBSTRC(lcLine,AT_C(".IACOLUMNINFO[",lcLine)+14)
 ELSE
  lcString = SUBSTRC(lcLine,AT_C(".IACOLUMNINFO(",lcLine)+14)
ENDIF
lcColumnNumber = SPACE(0)
FOR xx = 1 TO 100
  IF !ISDIGIT(SUBSTRC(lcString,xx,1))
    EXIT
  ENDIF
  lcColumnNumber = lcColumnNumber + SUBSTRC(lcString,xx,1)
ENDFOR 
RETURN INT(VAL(lcColumnNumber))



PROCEDURE GenerateClipText
LOCAL lcText, lcText1
lcText = SPACE(0)

TEXT TO lcText1 NOSHOW 
*  EITHER
*
*  paste this code into a method to
*  create an instance of an abstract Runtime Picklist 
*  Form parameter object and populate its properties
*  (see also the code at the end of this block to
*  actually call the RPF)
*
*!*	LOCAL loRPF
*!*	loRPF = NEWOBJECT("cus??RuntimePicklistFormParameter","??FWPICK.VCX")
*!*	WITH loRPF

*
*  OR
*
*  paste this code into the ShellAdditionalInit() method
*  of a concrete cusRuntimePicklistFormParameter class,
*  whereupon you can call the picklist form from the
*  Command Window like this (modify accordingly to
*  do the same thing in method code):
*!*	LOCAL loRPFP, luPK
*!*	loRPFP = NEWOBJECT("cusRPFPClass","ClassLibrary.VCX")
*!*	luPK = X8WRPFRM("frm??Picklist,??FWPICK.VCX",loRPFP)  
*!*	? luPK


WITH THIS
ENDTEXT

lcText = lcText1 + CHR(13) + CHR(13)

LOCAL xx, lnMaxLength
lnLength1 = 0
lnMaxLength = 0
FOR xx = 1 TO ALEN(paProps,1)
  paProps[xx,1] = "." + paProps[xx,1] + " = "
  paProps[xx,3] = REPLICATE(CHR(38),3) + SPACE(1) + paProps[xx,3]
  lnMaxLength = MAX(lnMaxLength,LENC(paProps[xx,1])+LENC(paProps[xx,2]))
ENDFOR
lnMaxLength = lnMaxLength + 1
*
*  icRecordSource
*
FOR xx = 1 TO 1
  lcText = lcText ;
           + paProps[xx,1] ;
           + PADR(paProps[xx,2],lnMaxLength-LENC(paProps[xx,1])) ; 
           + paProps[xx,3] ;
           + CHR(13)
ENDFOR
*
*  iaColumnInfo
*
lcText = lcText ;
         + "*  .iaColumnInfo[] array is required, containing ControlSources, optionally Column.Widths and iaColumnInfo[] columns 3-6 info" + CHR(13) 
IF !VARTYPE(paColumns[1,1])="C" OR EMPTY(paColumns[1,1])
  *
  *  we didn't find any column info 
  *
  lcText = lcText ;
           + "DIMENSION .iaColumnInfo[1,6]" + CHR(13) ;
           + '.iaColumnInfo[1,1] = "Alias.FieldName"' + CHR(13) 
 ELSE
  lcText = lcText ;
           + "DIMENSION .iaColumnInfo[" + TRANSFORM(ALEN(paColumns,1)) + ",8]" + CHR(13)
ENDIF           
lcText = lcText ;
         + "*    Column1 = (REQUIRED) Column.ControlSource" + CHR(13) ;
         + "*    Column2 = (OPTIONAL) Column.Width (unless a Width is specified for ALL columns, Width settings are ignored and defaulted" + CHR(13) ;
         + "*    Column3 = (OPTIONAL) frmPicklist.grdPicklist1.iaColumnInfo[ColumnX,3] - display index tag associated with this column, for ReSort behavior" + CHR(13) ;
         + "*    Column4 = (OPTIONAL) frmPicklist.grdPicklist1.iaColumnInfo[ColumnX,4] - Header.Caption" + CHR(13) ;
         + '*    Column5 = (OPTIONAL) frmPicklist.grdPicklist1.iaColumnInfo[ColumnX,5] - specifies "DESCENDING" for the tag in Column3' + CHR(13) ;
         + "*    Column6 = (OPTIONAL) frmPicklist.grdPicklist1.iaColumnInfo[ColumnX,6] - specifies index tag expression for Column3 tag, for tags created 'on the fly'" + CHR(13) ;
         + "*    Column7 = (OPTIONAL) frmPicklist.grdPicklist1.iaColumnInfo[ColumnX,7] - SEEK index tag associated with this column, for ReSort behavior" + CHR(13) ;
         + "*    Column8 = (OPTIONAL) frmPicklist.grdPicklist1.iaColumnInfo[ColumnX,8] - SEEK index tag expression for the tag in Column7" + CHR(13) ;
         + "*    Column9 = (OPTIONAL) frmPicklist.grdPicklist1.iaColumnInfo[ColumnX,9] - Dynamic InputMask for any associated Look For textbox" + CHR(13) 
IF !VARTYPE(paColumns[1,1])="C" OR EMPTY(paColumns[1,1])
  *
  *  we didn't find any column info -- the above comments
  *  will have to do
  *
 ELSE
  FOR xx = 1 TO ALEN(paColumns,1)
    FOR yy = 1 TO ALEN(paColumns,2)
      IF VARTYPE(paColumns[xx,yy]) = "C" AND !EMPTY(paColumns[xx,yy])
        lcText = lcText ;
                 + ".iaColumnInfo[" + TRANSFORM(xx) + "," + TRANSFORM(yy) + "]" ;
                 + " = " ;
                 + paColumns[xx,yy] ;
                 + CHR(13)
      ENDIF
    ENDFOR
  ENDFOR
ENDIF
*
*  rest of the properties
*
lcText = lcText ;
         + "*" + CHR(13) ;
         + "*  set the following properties as desired, or delete them" + CHR(13) ;
         + "*" + CHR(13)
FOR xx = 2 TO ALEN(paProps,1)
  lcText = lcText ;
           + paProps[xx,1] ;
           + PADR(paProps[xx,2],lnMaxLength-LENC(paProps[xx,1])) ; 
           + paProps[xx,3] ;
           + CHR(13)
ENDFOR
lcText = lcText + CHR(13) ;
         + "*  the .iaSupportingCursors[] array lists tables/views," + CHR(13) ;
         + "*  needed in addition to icRecordSource, typically for" + CHR(13) ;
         + "*  non-icRecordSource fields needed for ControlSources" + CHR(13) ;
         + "*  DIMENSION .iaSupportingCursors[1]" + CHR(13) ;
         + "*  .iaSupportingCursors[1] = " + CHR(13)

lcText = lcText + CHR(13) ;
         + "*  the .iaCommands[] array is completely optional," + CHR(13) ;
         + "*  creating a list of commands to be executed by" + CHR(13) ;
         + "*  XXFW.VCX/frmData.Init()" + CHR(13) ;
         + [*    XXDTES("XXFW.VCX","iaCommands","frmData","Init")] + CHR(13) ;
         + "*  DIMENSION .iaCommands[1]" + CHR(13) ;
         + "*  .iaCommands[1] = " 

lcText = lcText + CHR(13) + CHR(13) + "ENDWITH" + CHR(13) + CHR(13)

_ClipText = lcText

MESSAGEBOX("Skeleton Runtime Picklist Form parameter code has been placed in the clipboard", ;
           48,"Please Note")

RETURN
 
 
 
PROCEDURE GetSpecificGrid
LOCAL lnRecno, lcString 
SELECT *, ;
          RECNO() as TheRecno, ;
          SPACE(100) as PicklistDisplay, ;
          SPACE(100) as PicklistRecordSource ;
     FROM XXDTRPFP ;
     WHERE NOT EMPTY(BaseClass) ;      &&& I have no idea why I need this, but without it, I get the COMMENT records...
       AND (UPPER(BaseClass) = "GRID" ;
        OR UPPER(".Header1") $ UPPER(Properties)) ;
     INTO CURSOR XXDTRPFP1 READWRITE
*
*  Grid records
*    Parent field (don't bother with the initial Form if this is an .SCX)
*      plus
*    ObjName field
*  Other records (grid is a member of a parent class)
*    ObjName field 
*      plus
*    Containership hierarchy from the Properties field
*
SCAN
  lcString = GetPicklistRSInfo()
  replace XXDTRPFP1.PicklistDisplay WITH GETWORDNUM(lcString,1,"|"), ;
          XXDTRPFP1.PicklistRecordSource WITH GETWORDNUM(lcString,2,"|") ;
          IN XXDTRPFP1
ENDSCAN  
PRIVATE pnRecno
pnRecno = .NULL.
LOCAL loForm
loForm = CREATEOBJECT("frmSelectGrid")
loForm.Show()
USE IN SELECT("XXDTRPFP1")
SELECT XXDTRPFP
RETURN pnRecno




PROCEDURE GetPicklistRSInfo
*
*  similar to ProcessRecordSource
*
*  return RecordSource and object containership hierarchy
*  in the format
*     ContainerShip|RecordSource
*
LOCAL laLines[1], lcLine, lcString, lcJunk, ;
      lcRS, lcObjString
lcString = SPACE(0)
lcRS = SPACE(0)
lcObjString = ALLTRIM(ObjName)
ALINES(laLines,Properties)
FOR lnCount = 1 TO ALEN(laLines,1)
  lcLine = ALLTRIM(laLines[lnCount])
  IF UPPER(lcLine) = "RECORDSOURCE =" ;
       OR ".RECORDSOURCE =" $ UPPER(lcLine)
      lcRS = ALLTRIM(GETWORDNUM(lcLine,2,"="))
      IF UPPER(BaseClass) = "GRID"
        *  grid instance
        lcObjString = ALLTRIM(Parent) + "." + ALLTRIM(ObjName)
       ELSE
        *  grid is a member of a parent class
        lcJunk = ALLTRIM(GETWORDNUM(lcLine,1,"="))
        lcObjString = ALLTRIM(ObjName) + "." ;
             + LEFTC(lcJunk,RATC(".",lcJunk)-1)
      ENDIF
    EXIT
  ENDIF
ENDFOR

RELEASE laLines
LOCAL laLines[1]
ALINES(laLines,Methods)
FOR lnCount = 1 TO ALEN(laLines,1)
  lcLine = ALLTRIM(laLines[lnCount])
  IF SkipLine(lcLine)
    LOOP
  ENDIF
  IF "RECORDSOURCE =" $ UPPER(lcLine) ;
       OR "RECORDSOURCE=" $ UPPER(lcLine)
    lcRS = GetPropertyString(lcLine) + " (set in method code)"
    IF EMPTY(lcObjString)
      lcObjString = "(unknown)"
    ENDIF
  ENDIF
ENDFOR

IF NOT EMPTY(lcRS) AND INLIST(LEFTC(lcRS,1),["],['],"[")
  *  remove delimiters 
  lcRS = CHRTRAN(lcRS,["],SPACE(0))
  lcRS = CHRTRAN(lcRS,['],SPACE(0))
  lcRS = CHRTRAN(lcRS,"[",SPACE(0))
  lcRS = CHRTRAN(lcRS,"]",SPACE(0))    
ENDIF

IF EMPTY(lcRS)
  lcRS = "(unknown)"
ENDIF
IF EMPTY(lcObjString)
  lcObjString = "(unknown)"
ENDIF

lcString = lcObjString + "|" + lcRS

RETURN lcString 




DEFINE CLASS frmSelectGrid AS Form
AutoCenter = .t.
Height = 241
Width = 924
DataSession = 1
WindowType = 1
Caption = "Select the desired grid from the specified " + JUSTFNAME(DBF("XXDTRPFP")) + " form"
MaxButton = .f.
MinButton = .f.
BorderStyle = 2
Icon = "XXFWBLANK.ICO"
ADD OBJECT grdGrids AS Grid WITH ;
     Top = 24, ;
     Left = 12, ;
     Height = 200, ;
     Width = 900, ;
     ColumnCount = 2, ;
     HighlightStyle = 2, ;
     AllowCellSelection = .f., ;
     GridLines = 0, ;
     DeleteMark = .f., ;
     RecordMark = .f., ;
     ScrollBars = 2
ADD OBJECT cmdOK as CommandButton WITH ;
     Height = 27, ;
     Width = 120, ;
     Default = .t., ;
     Caption = "\<OK"     
ADD OBJECT cmdCancel as CommandButton WITH ;
     Height = 27, ;
     Width = 120, ;
     Cancel = .t., ;
     Caption = "\<Cancel"     
PROCEDURE Init
GO TOP IN XXDTRPFP1
THIS.grdGrids.RecordSource = "XXDTRPFP1"
THIS.grdGrids.Column1.ControlSource = "XXDTRPFP1.PicklistDisplay"
THIS.grdGrids.Column1.Header1.Caption = "Grid"
THIS.grdGrids.Column2.ControlSource = "XXDTRPFP1.PicklistRecordSource"
THIS.grdGrids.Column2.Header1.Caption = "RecordSource"
THIS.grdGrids.AutoFit()
THIS.grdGrids.Column1.Width = THIS.grdGrids.Column1.Width + 8
THIS.grdGrids.Column2.Width = THIS.grdGrids.Column2.Width + 8
IF THIS.grdGrids.Column1.Width < 200
  THIS.grdGrids.Column1.Width = 200
ENDIF
IF THIS.grdGrids.Column1.Width < 150
  THIS.grdGrids.Column1.Width = 150
ENDIF
THIS.grdGrids.Width = THIS.grdGrids.Column1.Width + ;
                      THIS.grdGrids.Column2.Width + ;
                      3 + ;
                      SYSMETRIC(5) + ;
                      4
THIS.Width = 12 + THIS.grdGrids.Width + 12                       
THIS.AutoCenter = .t. 
THIS.cmdCancel.Left = THIS.Width - 12 - THIS.cmdCancel.Width
THIS.cmdOK.Left = THIS.cmdCancel.Left - 12 - THIS.cmdOK.Width
THIS.cmdOK.Top = THIS.grdGrids.Top+THIS.grdGrids.Height+12
THIS.cmdCancel.Top = THIS.cmdOK.Top
THIS.Height = THIS.cmdOK.Top + THIS.cmdOK.Height + 12
ENDPROC
PROCEDURE OKAction
	pnRecno = XXDTRPFP1.TheRecno
	THISFORM.Release()
ENDPROC
PROCEDURE cmdCancel.Click
	THISFORM.Release()
ENDPROC
PROCEDURE cmdOK.Click
	THISFORM.OKAction()
ENDPROC
PROCEDURE grdGrids.KeyPress
	LPARAMETERS nKeyCode, nShiftAltCtrl
	IF nKeyCode = 13
	  THISFORM.OKAction()
	ENDIF
ENDPROC
PROCEDURE grdGrids.DblClick
	THISFORM.OKAction()
ENDPROC
ENDDEFINE





