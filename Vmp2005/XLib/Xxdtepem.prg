*
*  XXDTEPEM.PRG
*  DeveloperTool:  Has been renamed to XXDTExplicitPEM

LPARAMETERS tcString

LOCAL lcNewProgram
lcNewProgram = "XXDTExplicitPEM"
MESSAGEBOX(PROGRAM() + " has been replaced with " + lcNewProgram + ".PRG -- " + ;
           "please DO " + lcNewProgram + " instead", 48, "Please Note")
ACTIVATE WINDOW Command 

KEYBOARD "DO " + lcNewProgram + IIF(EMPTY(tcString), SPACE(0), [ WITH "] + tcString + ["])
