*
*  XXDTDBFList.PRG
*  Create alphabetical listing of all
*  .DBFs in the current directory, including their
*  field structures and .CDX tags.  The DBFs may be
*  open or closed and will be reset to their
*  open/closed status on entering XXDTDBFListing
*
*  Copyright (c) 1997-2005 Visionpace   All Rights Reserved
*                17501 East 40 Hwy., Suite 218
*                Independence, MO 64055
*                816-350-7900
*                http://www.visionpace.com
*                http://vmpdiscussion.visionpace.com
*  Author:  Drew Speedie
*           Special thanks to Ray Kurian
*
*  PLEASE NOTE!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
*  This utility is a quick-and-dirty program which I
*  wrote many, many years ago.  NO warranties of any
*  kind are expressed or implied. 
*
*  Name of the created listing file is:
*  - "DATA.LST" if parameter tcDBFs IS NOT passed
*  - tcDBFs+".LST" if parameter tcDBFs IS passed
*
*  Examples:
*    DO XXDTDBFListing
*      creates DATA.LST, listing all .DBFs in the
*      current directory
*
*    DO XXDTDBFListing with "BB",.t.
*      creates BB.LST, listing all .DBFs that begin with
*      the "BB" prefix that are in the current directory.
*      Prefix each line of BB.LST with an asterisk
*
*    DO XXDTDBFListing with "RFSTATES"
*      creates RFSTATES.LST, listing only the single
*      table RFSTATES.DBF in the current directory
*

*  Parameters:
*        tcDBFs (O) Character string of up to 8 letters.
*                     Only .DBFS starting with these letters
*                     will be included in the listing.
*                   Defaults to all .DBFs in the current
*                     directory if not passed OR passed as a
*                     non-character value
*                   NOTE:  don't include the ".DBF" extension!
*
*            -OR- tcDBFs may be passed as the name of
*                   a .DBC, in which case all tables in
*                   that database are processed
*
* tlAddAsterisk (O) Prefix each line with an asterisk?
*                     Pass this parameter as .T. if you want
*                     the resulting .LST file ready to copy
*                     and paste into a program as comments.
*                   Defaults to .F.; no asterisks if not
*                     passed or passed as a non-logical value
*
*      tlGetDir (O) Present a GETDIR() dialog box to select
*					  the directory containing the data.
*					Defaults to .F.; use the current directory
*
PARAMETERS tcDBFs, tlAddAsterisk, tlGetDir
LOCAL llDBC, lcCRLF, lcTemp, ;
	  lnCount, lcAlias, llWasOpen, lnCnt, lcDBC, lcDatabase, ;
	  llDBCWasOpen, lcSaveDatabase, lcLine, lcDBF, lcDir, ;
	  laDLLs[1]

*
*  make sure the MessageBeep() Windows API function
*  is loaded to ring the bell rather than via ?? CHR(7), 
*  which generates a problem with commandbuttons having 
*  both Caption and Picture
*    XXDTES("XXFW.VCX","VFP ANOMALY WHEN BOTH Caption AND Picture ARE SET","cmdBase","zReadMe")
*
*  note that X7ISAPIF() is not used here, for 
*  VMPToolbox compatibility
*
IF ADLLS(laDLLs) > 0 ;
     AND ASCAN(laDLLs,"MessageBeep",-1,-1,1,15)>0
 ELSE     
  DECLARE Long MessageBeep IN USER32.DLL Long uType 
ENDIF

LOCAL ARRAY laStru[1]
* include these .dbfs:

lcDir = SPACE(0)
IF VARTYPE(tlGetDir) = "L" AND tlGetDir
	lcDir = GETDIR(FULLPATH(CURDIR()), ;
	               "Select the folder that contains the .DBFs " + ;
	               "for which you want to create an .LST listing.", ;
	               "Select the folder containing .DBFs", ;
	               1+64)
	IF EMPTY(lcDir)		&& User pressed "Cancel"
		RETURN
	ELSE
		lcDir = ADDBS(lcDir)
	ENDIF
ENDIF

IF TYPE("tcDBFs") # "C"
	*
	*  all tables in the current directory
	*
	tcDBFs = SPACE(0)
ELSE
	tcDBFs = UPPER(ALLTRIM(tcDBFs))
ENDIF
IF LEN(tcDBFs) > 8 AND !".DBC" $ tcDBFs
    MessageBeep(0)
	WAIT WINDOW ;
		"Character string prefix may not exceed 8 characters"
	RETURN
ENDIF
IF ".DBC" $ tcDBFs
	llDBC = .T.
	lcDatabase = tcDBFs
	tcDBFs = SPACE(0)
ENDIF
WAIT WINDOW "Processing indicated tables..." NOWAIT
CLEAR WINDOWS
CLEAR
PRIVATE pcFontName, pnFontSize, plFontBold
pcFontName = _SCREEN.FONTNAME
pnFontSize = _SCREEN.FONTSIZE
plFontBold = _SCREEN.FONTBOLD
_SCREEN.FONTNAME = "FoxFont"
_SCREEN.FONTSIZE = 9
_SCREEN.FONTBOLD = .F.
PRIVATE pcTalk, pcSafety, pcEscape, pnMemo, pcOnError
pcTalk   = SET("TALK")
pcSafety = SET("SAFETY")
pcEscape = SET("ESCAPE")
pnMemo   = SET("MEMOWIDTH")
pcOnError = ON("ERROR")
SET ESCAPE OFF
SET TALK ON
SET SAFETY OFF
SET MEMOWIDTH TO 200
* add an asterisk to the beginning of each line?
IF TYPE("tlAddAsterisk") # "L" .OR. !tlAddAsterisk
	tlAddAsterisk = ""
ELSE &&& tlAddAsterisk
	tlAddAsterisk = "* "
ENDIF
lcSaveDatabase = SET("DATABASE")
IF !EMPTY(lcSaveDatabase)
	SET DATABASE TO
ENDIF
* create XXDTDBFListing.dbf:
IF !FILE("XXDTDBFListing.DBF")
	CREATE TABLE XXDTDBFListing (db_name C(100), db_stru M, db_tags M)
ENDIF
IF !USED("XXDTDBFListing")
	USE XXDTDBFListing IN 0 EXCLUSIVE
ENDIF
SELECT XXDTDBFListing
ZAP
IF !EMPTY(lcSaveDatabase)
	SET DATABASE TO (lcSaveDatabase)
ENDIF

LOCAL llError
ON ERROR llError = .T.

IF llDBC
	* tcDBFs has been passed as a .DBC
	LOCAL lcJustDBCName
	lcSaveDatabase = SET("DATABASE")
	IF DBUSED(lcDatabase)
		llDBCWasOpen = .T.
	ELSE
		OPEN DATA (lcDatabase)
	ENDIF
	IF llError
		WAIT WINDOW "Unable to open database " + lcDatabase
        DO Cleanup
		RETURN
	ENDIF
	SET DATABASE TO (lcDatabase)
	IF llError
		WAIT WINDOW "Unable to SET DATABASE TO " + lcDatabase
        DO Cleanup 
		RETURN
	ENDIF
	ON ERROR &pcOnError
	LOCAL ARRAY laTemp[1]
	=ADBOBJECTS(laTemp,"TABLE")
	=ASORT(laTemp)
	FOR lnCount = 1 TO ALEN(laTemp,1)
		DIMENSION laDBFs[lnCount,2]
		laDBFs[lnCount,1] = laTemp[lnCount]
	ENDFOR
	IF OCCURS("\",lcDatabase) > 0
		lcTemp = SUBSTRC(lcDatabase,AT_C("\",lcDatabase,OCCURS("\",tcDBFs))+1)
	ENDIF
	lcTemp = STRTRAN(lcDatabase,".DBC",".LST")
ELSE
	* memory variable for textfile:
	lcTemp = IIF(EMPTY(tcDBFs),"DATA",tcDBFs)+".LST"
	* tcDBFs has not been passed, or passed
	* as a prefix string
	*
	* store table info to an array:
	= ADIR(laDBFs,lcDir + "*.DBF")
	* sort in alphabetical order:
	= ASORT(laDBFs,1,ALEN(laDBFs,1))
ENDIF
* store carriage return + line feed to memvar:
lcCRLF = CHR(13)+CHR(10)
* compile info for each table in the array:
FOR lnCount = 1 TO ALEN(laDBFs,1)
	***************************************************
	*  PROCESS THE ALIAS
	***************************************************
	IF OCCURS(".",laDBFs[lnCount,1]) > 0
		lcAlias = LEFTC(laDBFs[lnCount,1],AT_C(".",laDBFs[lnCount,1])-1)
	ELSE
		lcAlias = laDBFs[lnCount,1]
	ENDIF
	IF llDBC
		IF INDBC(lcAlias,"TABLE")
			* process this alias
		ELSE
			* skip this one
			LOOP
		ENDIF
	ELSE
		IF lcAlias=tcDBFs AND NOT lcAlias=UPPER("XXDTDBFListing")
			* process this alias
		ELSE
			* skip this one
			LOOP
		ENDIF
	ENDIF
	* see if the dbf in question is currently open:
	llWasOpen = USED(lcAlias)
	IF llWasOpen
		SELECT (lcAlias)
	ELSE
		IF llDBC
			USE (lcAlias) IN SELECT(1) AGAIN ALIAS USEAGAINALIAS SHARED
			lcAlias = "USEAGAINALIAS"
		ELSE
			ON ERROR llError = .T.
			USE (lcAlias) IN SELECT(1)
			IF llError
				llError = .F.
				LOOP
			ENDIF
		ENDIF
		SELECT (lcAlias)
	ENDIF
	* store the table name
	IF EMPTY(CURSORGETPROP("DATABASE"))
		m.db_name = laDBFs[lnCount,1] + " (free table)"
	ELSE
		lcDBC = CURSORGETPROP("DATABASE")
		lcDBF = DBF(lcAlias)
		m.db_name = CURSORGETPROP("SourceName") + SPACE(2) + ;
			"(" + SUBSTR(lcDBF,AT_C("\",lcDBF,OCCURS("\",lcDBF))+1) + " in " +  + ;
			SUBSTR(lcDBC,AT_C("\",lcDBC,OCCURS("\",lcDBC))+1) + ")"
	ENDIF
	INSERT INTO XXDTDBFListing FROM MEMVAR
	***************************************************
	*  PROCESS THE FIELDS LIST
	***************************************************
	SELECT (lcAlias)
	RELEASE laStru
	=AFIELDS(laStru)
	SELECT XXDTDBFListing
	FOR lnCnt = 1 TO ALEN(laStru,1)
		REPLACE XXDTDBFListing.db_stru WITH XXDTDBFListing.db_stru + ;
			PADL(lnCnt,3) + SPACE(2) + ;
			IIF(LENC(laStru[lnCnt,1])>33,LEFTC(laStru[lnCnt,1],32)+"..",PADR(laStru[lnCnt,1],34)) + SPACE(2) + ;
			laStru[lnCnt,2] + SPACE(2) + ;
			PADL(laStru[lnCnt,3],4) + SPACE(3) + ;
			IIF(laStru[lnCnt,4]>0,PADR(laStru[lnCnt,4],12),SPACE(12)) + ;
			IIF(laStru[lnCnt,5],"Yes","No")
		IF lnCnt # ALEN(laStru,1)
			REPLACE XXDTDBFListing.db_stru WITH XXDTDBFListing.db_stru + lcCRLF
		ENDIF
	ENDFOR
	***************************************************
	*  PROCESS THE INDEX TAG INFO
	***************************************************
	* put the cdx tag information into a memo field:
	SELECT (lcAlias)
	FOR lnCnt = 1 TO 200
		IF EMPTY(TAG(lnCnt))
			IF lnCnt = 1
				SELECT XXDTDBFListing
				REPLACE db_tags WITH db_tags + "No Tags" + lcCRLF
			ENDIF
			EXIT
		ELSE
			* one line per tag:
			lcThisTag = ;
				PADR(TAG(lnCnt),14) + SYS(14,lnCnt) + lcCRLF
			SELECT XXDTDBFListing
			REPLACE db_tags WITH db_tags + lcThisTag
			SELECT (lcAlias)
		ENDIF
	ENDFOR lnCnt = 1 TO 200
	* close this dbf if we didn't find it open:
	IF !llWasOpen
		USE IN (lcAlias)
	ENDIF
ENDFOR lnCount = 1 TO ALEN(laDBFs,1)
IF llDBC
	IF !llDBCWasOpen
		CLOSE DATABASE
	ENDIF
	SET DATABASE TO &lcSaveDatabase
ENDIF
ERASE (lcTemp)
SET TEXTMERGE ON
SET TEXTMERGE TO (lcTemp) NOSHOW
SET TALK OFF
CLEAR
* create the .LST file from the XXDTDBFListing.DBF records:
WAIT WINDOW "Creating "+lcTemp+" listing ..." NOWAIT
SELECT XXDTDBFListing
SCAN
	? ALLTRIM(db_name)
	* write a line for the table name:
  \<<tlAddAsterisk + alltrim(db_name)>>
	* write one line for each field name/structure:
  \<<tlAddAsterisk + "Fields                                 Type  Width  Dec       Nullable">>
	FOR lnCount = 1 TO MEMLINES(db_stru)
		lcThisLine = UPPER(MLINE(db_stru,lnCount))
    \<<tlAddAsterisk + space(1) + mline(db_stru,lnCount)>>
	ENDFOR
  \<<tlAddAsterisk + "Index Tags    Key Expression">>
	* write one line for each .cdx tag:
	FOR lnCount = 1 TO MEMLINES(db_tags)
    \<<tlAddAsterisk + mline(db_tags,lnCount)>>
	ENDFOR
ENDSCAN
SET TEXTMERGE TO
SET TEXTMERGE OFF
DO Cleanup
MODI FILE (lcTemp) NOWAIT RANGE 1,1
RETURN



PROCEDURE Cleanup
ON ERROR &pcOnError
SET MEMOWIDTH TO (pnMemo)
SET TALK &pcTalk
SET TALK &pcSafety
SET ESCAPE &pcEscape
WAIT CLEAR
_SCREEN.FONTNAME = pcFontName
_SCREEN.FONTSIZE = pnFontSize
_SCREEN.FONTBOLD = plFontBold
USE IN SELECT("XXDTDBFListing")
ERASE XXDTDBFListing.DBF
ERASE XXDTDBFListing.FPT
RETURN
