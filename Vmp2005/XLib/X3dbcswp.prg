*
*  X3DBCSWP.PRG
*  Set Form.DataEnvironment.Cursor.Database 
*  property for each cursor in the DataEnvironment
*  to a different .DBC than the one VFP stores in  
*  the .SCX.  "Swap" the existing .DBC to a different
*  one containing the same cursor(s).
*
*  Copyright (c) 1997-2005 Visionpace   All Rights Reserved
*                17501 East 40 Hwy., Suite 218
*                Independence, MO 64055
*                816-350-7900 
*                http://www.visionpace.com
*                http://vmpdiscussion.visionpace.com
*  Author:  Drew Speedie
*
*
*  Requires:
*  X2JSTFIL.PRG in VFP versions before VFP 6.0
*
*  
*  Usage:
*    In the Form.DataEnvironment.BeforeOpenTables(), 
*    put a call like one of these 
*    
*    DO X3DBCSWP WITH THISFORM, "VM", "VMPUBS"
*    * updates any Cursor.Database property that
*    * contains "VM.DBC" to "VMPUBS.DBC"
*
*    DO X3DBCSWP WITH THISFORM, "VM.DBC", "VMPUBS.DBC"
*    * updates any Cursor.Database property that
*    * contains "VM.DBC" to "VMPUBS.DBC"
*
*    DO X3DBCSWP THISFORM, "VM", "..\DATA\VMPUBS"
*    * updates any Cursor.Database property that
*    * contains "VM.DBC" to the VMPUBS.DBC in
*    * the above relative path
*
*  lParameters
*      toForm (R) Object reference to the form whose DataEnvironment.Cursors
*                   are to have their .Database property updated 
*   tcFromDBC (R) String containing the name of the .DBC which, for 
*                   any cursors located in that .DBC, are to be swapped
*                   to the corresponding ones in tcToDBC
*     tcToDBC (R) String containing the name (and optional pathing) for
*                   the .DBC to be used instead of the existing Database
*                   for cursors in toForm.DataEnvironment
*
LPARAMETERS toForm, tcFromDBC, tcToDBC
IF NOT TYPE("toForm.BaseClass") = "C" ;
     OR NOT UPPER(ALLTRIM(toForm.BaseClass)) = "FORM"
  RETURN .f.
ENDIF   
IF PCOUNT() < 3
  RETURN .f.
ENDIF
LOCAL lcFromDBC, lcToDBC
IF NOT TYPE("tcFromDBC") = "C" ;
     OR ISNULL(tcFromDBC) ;
     OR EMPTY(tcFromDBC)
  RETURN .f.
 ELSE
  lcFromDBC = UPPER(ALLTRIM(tcFromDBC))
  IF NOT RIGHTC(lcFromDBC,4) = ".DBC"
    lcFromDBC = lcFromDBC + ".DBC"
  ENDIF
ENDIF
IF NOT TYPE("tcToDBC") = "C" ;
     OR ISNULL(tcToDBC) ;
     OR EMPTY(tcToDBC)
  RETURN .f.
 ELSE
  lcToDBC = UPPER(ALLTRIM(tcToDBC))
  IF NOT RIGHTC(lcToDBC,4) = ".DBC"
    lcToDBC = lcToDBC + ".DBC"
  ENDIF
ENDIF
*
*  loop thru toForm.DataEnvironment and update each
*  Cursor.Database property
*
LOCAL lnCount, loObject, lcDBCProp, lcNewDBC 
LOCAL array laDEMembers[1]
IF AMEMBERS(laDEMembers,toForm.DataEnvironment,2) > 0
  FOR lnCount = 1 TO ALEN(laDEMembers,1)
    loObject = EVALUATE("toForm.DataEnvironment."+laDEMembers[lnCount])
    IF UPPER(ALLTRIM(loObject.BaseClass)) == "CURSOR" 
      #IF UPPER(VERSION()) = "VISUAL FOXPRO 03" ;
           OR UPPER(VERSION()) = "VISUAL FOXPRO 05"
        lcDBCProp = x2jstfil(loObject.Database,.t.)
        lcNewDBC = x2jstfil(lcFromDBC,.t.)
      #ELSE
        lcDBCProp = UPPER(JUSTSTEM(loObject.Database))
        lcNewDBC = UPPER(JUSTSTEM(lcFromDBC))
      #ENDIF
      IF lcDBCProp == lcNewDBC
        loObject.Database = lcToDBC
      ENDIF
    ENDIF 
  ENDFOR 
ENDIF

RETURN .t.
