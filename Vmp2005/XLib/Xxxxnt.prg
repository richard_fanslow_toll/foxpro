DEFINE CLASS sesNTBaseVMPSuperClass as Session
*
*  this class is the super class of XXFWNT.PRG/sesNTBase,
*  and provides you a place to put PEMs that are inherited
*  by all the Session classes in this hierarchy, corresponding
*  to the XXXX*.VCX VMP superclasses
*
*  DEVNOTE
*  comments need to be added here, similar to those found in
*  the zReadMe of the XXXX*.VCX zReadMe()s -- see also  
*  XXXXCTRN.VCX/ctrNTBaseVMPSuperClass.zReadMe()
*
Name = "sesNTBaseVMPSuperClass"
ENDDEFINE
