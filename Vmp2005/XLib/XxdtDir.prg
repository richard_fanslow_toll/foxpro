*
*  XXDTDir.PRG
*  "Replacement" (alternative) to the DIR command
*
*  Copyright (c) 2004-2005 Visionpace   All Rights Reserved
*                17501 East 40 Hwy., Suite 218
*                Independence, MO 64055
*                816-350-7900 
*                http://www.visionpace.com
*                http://vmpdiscussion.visionpace.com
*  Author:  Drew Speedie
*
*  This utility lists the indicated files in what we consider
*  a better format than you get from the DIR command in VFP:
*  1- The files are listed one per row to _Screen
*  2- The list concludes with a count of the total number 
*     of files
*  3- When you optionally specify all ADIR() columns, the
*     _Screen output is nicely formatted in columns
*  4- You can optionally specify that the output is also
*     copied to XXDTDIR.TXT
*  
*  USAGE
*  ====================================
*  XXDTDIR()
*    List FileName for all files in the current folder
*  XXDTDIR(.f.,.t.)
*    List all 4 ADIR() columns for all files in the current folder 
*  XXDTDIR(.f.,.t.,.t.)
*    List all 4 ADIR() columns for all files in the current folder,
*    copy the list to XXDTDIR.TXT
*  XXDTDIR(.f.,.f.,.t.)
*    List FileName for all files in the current folder,
*
*  XXDTDIR("*PRG")
*    List FileName for all .PRG files in the current folder
*  XXDTDIR("*PRG",.t.)
*    List all 4 ADIR() columns for all .PRG files in the current folder 
*  XXDTDIR("*PRG",.f.,.t.)
*    List FileName for all .PRG files in the current folder,
*    copy the list to XXDTDIR.TXT
*  XXDTDIR("*PRG",.t.,.t.)
*    List all 4 ADIR() columns for all .PRG files in the current folder,
*    copy the list to XXDTDIR.TXT
*
*  XXDTDIR("..\XLIB\*VCX")
*    List Filename for all .VCX files in the \XLIB folder
*  XXDTDIR("..\XLIB\*VCX",.t.)
*    List all 4 ADIR() columns for all .VCX files in the \XLIB folder,
*  XXDTDIR("..\XLIB\*VCX",.f.,.t.)
*    List Filename for all .VCX files in the \XLIB folder
*    copy the list to XXDTDIR.TXT
*  XXDTDIR("..\XLIB\*VCX",.t.,.t.)
*    List all 4 ADIR() columns for all .VCX files in the \XLIB folder,
*    copy the list to XXDTDIR.TXT
*
*
*  lParameters
*   tcFileSkeleton (O) The 2nd tcFileSkeleton parameter passed
*                        to ADIR()
*                      Default/not passed, all files in the
*                        current directory are listed
*     tlAllColumns (O) Pass this parameter as .T. to have all
*                        4 ADIR() columns included
*                      Default/not passed, only the filename
*                        is listed
*         tlToFile (O) Pass this parameter as .T. to have the
*                        listing additionally copied to 
*                        XXDTDIR.TXT
*                      Default/not passed, the listing is only
*                        output to _Screen
*        tlNoPause (O) Pass this parameter as .T. to not pause
*                        each time _Screen output fills up 
*                        (like DISPLAY MEMORY, DISPLAY STATUS)
*                        but rather list the output continuously
*                        (like LIST MEMORY, LIST STATUS)
*                      Default/not passed, the list pauses each
*                        time _Screen fills up
*
LPARAMETERS tcFileSkeleton, tlAllColumns, tlToFile, tlNoPause

LOCAL laFiles[1], lnCount, lnMaxF, lnMaxS, lnScreen, lcLine, ;
      lnTotal

*
*  do the ADIR()
*
IF VARTYPE(tcFileSkeleton) = "C" AND NOT EMPTY(tcFileSkeleton)
  lnCount = ADIR(laFiles,tcFileSkeleton)
 ELSE
  lnCount = ADIR(laFiles)
ENDIF

IF lnCount = 0
  MESSAGEBOX("No files to list",48,PROGRAM())
  RETURN
ENDIF

*
*  get the max length of columns 1 and 2 if 
*  we're supposed to display all columns
*
lnMaxF = 0
lnMaxS = 0
lnTotal = 0
FOR lnCount = 1 TO ALEN(laFiles,1)
  lnTotal = lnTotal + laFiles[lnCount,2]
  IF VARTYPE(tlAllColumns) = "L" AND tlAllColumns
    lnMaxF = MAX(lnMaxF,LENC(laFiles[lnCount,1]))
    lnMaxS = MAX(lnMaxS,LENC(TRANSFORM(laFiles[lnCount,2])))
  ENDIF
ENDFOR

*  add a final total/count row
DIMENSION laFiles[ALEN(laFiles,1)+1,ALEN(laFiles,2)]
laFiles[ALEN(laFiles,1),1] = ;
     ALLTRIM(TRANSFORM(lnTotal,REPLICATE("999,",4)+"999")) + ;
     " bytes in " + TRANSFORM(ALEN(laFiles,1)-1) + " files"

*  create XXDTDIR.TXT header
DO CASE
  CASE NOT VARTYPE(tlToFile) = "L" OR NOT tlToFile
  CASE VARTYPE(tcFileSkeleton) = "C" AND NOT EMPTY(tcFileSkeleton) ;
        AND ("\" $ tcFileSkeleton OR "/" $ tcFileSkeleton)
    STRTOFILE(tcFileSkeleton,"XXDTDIR.TXT",0) 
  OTHERWISE
    STRTOFILE(DISPLAYPATH(FULLPATH(CURDIR()),70) + SPACE(2) + tcFileSkeleton, ;
              "XXDTDIR.TXT",0) 
ENDCASE

*
*  display the list of files to _Screen
*
CLEAR 
lnScreen = 1
FOR lnCount = 1 TO ALEN(laFiles,1)
  IF lnScreen = INT(SROWS())
    *
    *  _Screen is full
    *
    IF VARTYPE(tlNoPause) = "L" AND tlNoPause
      *
      *  tlNoPause has been passed as .t.
      *
     ELSE 
      WAIT WINDOW "Press any key to continue..."
      CLEAR 
    ENDIF
    lnScreen = 0
  ENDIF
  IF lnMaxF > 0
    lcLine = PADR(laFiles[lnCount,1],lnMaxF)
   ELSE
    lcLine = laFiles[lnCount,1]
  ENDIF
  DO CASE
    **********************************************
    CASE lnCount = ALEN(laFiles,1)
    **********************************************
      *
      *  last line is the " <ExpN> files" line, which 
      *  has nothing in columns 2-4 
      *
      lcLine = laFiles[lnCount,1]
    **********************************************
    CASE VARTYPE(tlAllColumns) = "L" AND tlAllColumns
    **********************************************
      lcLine = lcLine + SPACE(1) + PADL(laFiles[lnCount,2],lnMaxS)
      lcLine = lcLine + SPACE(1) + DTOC(laFiles[lnCount,3])
      lcLine = lcLine + SPACE(1) + laFiles[lnCount,4]
    **********************************************
    OTHERWISE
    **********************************************
      *
      *  just Column1/FileName
      *
  ENDCASE
  ? lcLine
  IF VARTYPE(tlToFile) = "L" AND tlToFile
    STRTOFILE(CHR(13)+CHR(10) + lcLine,"XXDTDIR.TXT",1)
  ENDIF
  lnScreen = lnScreen + 1
ENDFOR
STRTOFILE(CHR(13)+CHR(10),"XXDTDIR.TXT",1)
IF VARTYPE(tlToFile) = "L" AND tlToFile
  ? "The above listing has been copied to XXDTDIR.TXT"
ENDIF

RETURN 
