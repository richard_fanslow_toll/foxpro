*
*  XXSQLEXE.PRG
*  This program is a wrapper for the SQLExec() function,
*  providing for some error/busy connection checking, etc.
*
*  Copyright (c) 1997-2005 Visionpace   All Rights Reserved
*                17501 East 40 Hwy., Suite 218
*                Independence, MO 64055
*                816-350-7900 
*                http://www.visionpace.com
*                http://vmpdiscussion.visionpace.com
*  Author:  Drew Speedie
*           Special thanks to Gary Farkas for suggesting the 
*           SET FMTONLY enhancement, although please note
*           that it is apparently a SQL-Server-specific 
*           feature and will not likely work for other
*           back ends like Oracle, DB2, etc.
*
*  RETURN values are the same as those from SQLEXEC():
*     1 - the SQLEXEC() succeeded (native VFP return value)
*    -1 - the SQLEXEC() failed, connection problem (native
*         VFP return value)
* 
*  Note that some features in this program depend on the existence of oLib/
*  oLib.oConnectionSvc (instance of XXFW.VCX/cusConnectionSvc), and are
*  suppressed if this is not the case.  In particular, this is desirable when
*  this routine is called from a COM server, where all UI must be suppressed.
*
*
*  NOTE that you may decide not to call this .PRG directly
*  in your intermediate/app-specific method/.PRG code --
*  you can create a "wrapper" class for this (and XXRQUERY.PRG)
*  to call this .PRG, making it easier to subclass/override
*  when necessary.
*
*
*  You may be wondering why this code isn't in a method
*  of an object like XXFW.VCX/cusConnectionSvc.
*  Mostly because oLib.oConnectionSvc resides in the
*  default data session #1, but this behavior might be
*  requested from anywhere, frequently from a private data
*  session form.  
*  There are at least 4 ways this code could have been written:
*    1- standalone .PRG program (which always participates in
*       the calling code's data session)
*    2- method of XXFWFRM.VCX/frmData
*    3- method of XXFW.VCX/cusConnectionSvc, make the 
*       developer add an instance of cusConnectionSvc to 
*       any private data session form that needs to call
*       this code
*    4- method of XXFW.VCX/cusConnectionSvc, receive a
*       parameter indicating a private data session number,
*       and issue an explicit SET DATASESSION TO that data
*       session
*  We've gone with #1 above, because 
*    1) we feel it's the most flexible approach
*    2) because we're not particularly keen on the
*       idea of explicit SET DATASESSION TO
*  #3 would be our second choice
*
*
*  See also XXRQUERY.PRG
*
*
*  lParameters
*       tnHandle (O) 1st SQLExec() parameter specifying the connection 
*                      handle to use for the query
*                    Defaults to oLib.oConnectionSvc.GetSingleConnectHandle()
*
*          tcSQL (R) 2nd SQLExec() parameter specifying the SQL statement 
*                      to execute
*                    Precede the actual SQL string with "~" to wrap the
*                      SQL string/command with "SET FMTONLY ON ... SET FMTONLY OFF",
*                      which results in an empty result cursor -- analagous
*                      to USEing a VIEW NODATA
*
* tcResultCursor (O) 3rd SQLExec() parameter specifying the name of the 
*                      cursor into which to put the result of tcSQL statement
*                    Defaults to no cursor specified, the result set goes in 
*                      the default "SQLResult" cursor
*
*        taParms (O) Array with parameter information, useful if tcSQL 
*                      contains parameters the way a parameterized view does.
*                      If passed, must be done by REFERENCE, and must be a 
*                      2-column array, one parameter per row.  
*                      Column1 contains the parameter name, and Column2 
*                      contains the value for the parameter in Column1.
*                    Default/not passed: Any needed parameters are assumed 
*                      to be in scope/available at this time (perhaps 
*                      declared as PRIVATE memvars in the calling code)
*
* tlAllowUserToRetryWhenBusy (0) Allow the user to keep trying to
*                                  execute the SQLExec() if the
*                                  tnHandle is busy?
*                                Defaults to the current  
*                                  oLib.oConnectionSvc.ilQueryAllowUserToRetryWhenBusy
*                                  setting, which comes from
*                                  the APPINFO table, which 
*                                  defaults to .F.
*                                Ignored if X6ISCOM()
*
*      tlPresentErrorMessage (O) If unsuccessful, present an
*                                  appropriate X3MSGSVC dialog
*                                  to the user here?
*                                Defaults to the current  
*                                  oLib.oConnectionSvc.ilQueryPresentErrorMessage
*                                  setting, which comes from
*                                  the APPINFO table, which 
*                                  defaults to .F.
*                                Note that methods like
*                                  XXFW.VCX/frmData::SaveAction()
*                                  act on a failed TABLEUPDATE() or
*                                  failed SQLExec() with an overall
*                                  <Save>-level user message in 
*                                  THISFORM.SaveMessageOnFailure()
*                                Ignored if X6ISCOM()
*
*        tlShutdownIfProblem (O) Shutdown the application from
*                                  here if the tnHandle connection
*                                  is invalid/busy?
*                                Defaults to the current  
*                                  oLib.oConnectionSvc.ilQueryShutdownIfProblem
*                                  setting, which comes from
*                                  the APPINFO table, which 
*                                  defaults to .F.
*                                Note that methods like 
*                                  XXFW.VCX/frmData::SaveAction()
*                                  act on a failed TABLEUPDATE() or
*                                  failed SQLExec() by issuing
*                                  oApp.ForceShutdown() if that 
*                                  failure is due to a connection
*                                  problem
*                                Ignored if X6ISCOM()
*                                 
* 

LPARAMETERS tnHandle, ;
            tcSQL, ;
            tcResultCursor, ;
            taParms, ;
            tlAllowUserToRetryWhenBusy, ;
            tlPresentErrorMessage, ;
            tlShutdownIfProblem

LOCAL lnHandle, ;
      llSuccess, ;
      llBusy, ;
      llInvalid, ;
      lnReturn, ;
      lnParameters, ;
      lcSQL, ;
      llHandleCheck, ;
      lnSelect

lnParameters = PCOUNT()

EXTERNAL ARRAY taParms
      
lnSelect = SELECT(0)      
      
*
*  determine lnHandle
*
DO CASE
  ***********************************************************
  CASE VARTYPE(m.tnHandle) = "N" AND NOT ISNULL(m.tnHandle) 
  ***********************************************************
    *
    *  handle was passed here explicitly
    *
    lnHandle = m.tnHandle
    IF X2PSTACK(".SQLExecuteRemote ")
      *
      *  this routine has been called by the VMP middle tier,
      *  here:
      *    XXDTES("XXFWNTDS.VCX","XXSQLEXE(","cusDataSourceBehavior","SQLExecuteRemote")
      *  and the SQLExecuteRemote() method has already 
      *  handled connection handle checking when it called
      *  the Connect()/GetHandle() method, which respect
      *  the connection object's ilCheckHandleOnEveryGet
      *  setting:  
      *    XXDTES("XXFWNTDS.VCX","IF NOT THIS.ilCheckHandleOnEveryGet","cusConnectionBehaviorODBC","GetHandle")
      *
      llHandleCheck = .f.
     ELSE
      *
      *  check the handle for validity
      *
      llHandleCheck = .t.
    ENDIF
  ***********************************************************
  CASE TYPE("oLib.oConnectionSvc.BaseClass") = "C"
  ***********************************************************
    *
    *  default to oLib.oConnectionSvc.GetSingleConnectHandle()
    *
    lnHandle = oLib.oConnectionSvc.GetSingleConnectHandle()
    llHandleCheck = .t.
  ***********************************************************
  OTHERWISE
  ***********************************************************
    *
    *  you didn't pass lnHandle, and oLib does not exist
    *    - at the Command Window
    *    - in a COM server business object
    *
    ERROR 46   &&& expression evaluated to an illegal value    
ENDCASE

*
*
*  NOTE:  except for IF..ENDIF block of code that
*         does the SQLEXEC(), the rest of this
*         code is identical to the code in XXRQUERY.PRG
*         that performs the same checks/messages/etc.
*         At some time in the future, some of this
*         common code may be split out into separate
*         methods.
*         Unfortunately, XXRQUERY receives only *two*
*         parameters before the last 3 optional parameters,
*         whereas this .PRG receives *three* parameters
*         before the last 3 optional parameters, so the
*         individual CASE statements here check for
*         an lnParameters value that is *one higher*
*         than the same CASE statements in XXRQUERY
*         

*
*  process the optional behavior parameters
*
DO CASE
  ******************************************
  CASE NOT TYPE("oLib.oConnectionSvc.BaseClass") = "C"
  ******************************************
    *
    *  leave 'em all defaulted to whatever they were passed
    *  in as, or .F. if nothing passed
    *
  ******************************************
  CASE X6ISCOM()
  ******************************************
    *
    *  suppress all the UI stuff in this .PRG
    *
    *  note that NOT VARTYPE(m.oLib) = "O"
    *
    tlAllowUserToRetryWhenBusy = .f.
    tlPresentErrorMessage = .f.
    tlShutdownIfProblem = .f.
  ******************************************
  CASE m.lnParameters < 5
  ******************************************
    *
    *  none of them were passed
    *
    tlAllowUserToRetryWhenBusy = oLib.oConnectionSvc.ilQueryAllowUserToRetryWhenBusy
    tlPresentErrorMessage = oLib.oConnectionSvc.ilQueryPresentErrorMessage
    tlShutdownIfProblem = oLib.oConnectionSvc.ilQueryShutdownIfProblem
  ******************************************
  CASE m.lnParameters = 5
  ******************************************
    *
    *  tlAllowUserToRetryWhenBusy was passed
    *
    tlPresentErrorMessage = oLib.oConnectionSvc.ilQueryPresentErrorMessage
    tlShutdownIfProblem = oLib.oConnectionSvc.ilQueryShutdownIfProblem
  ******************************************
  CASE m.lnParameters = 6
  ******************************************
    *
    *  tlPresentErrorMessage was passed
    *
    tlShutdownIfProblem = oLib.oConnectionSvc.ilQueryShutdownIfProblem
  ******************************************
  OTHERWISE m.lnParameters > 6
  ******************************************
    *
    *  all 3 of them were passed
    *
ENDCASE  

DO CASE
  ****************************************************
  CASE NOT m.llHandleCheck
  ****************************************************
    *
    *  no handle check here, typically in an n-Tier
    *  environment, when this routine is called
    *  from cusDataSourceBehavior::SQLExecuteRemote()
    *
    llSuccess = .t.
  ****************************************************
  CASE TYPE("oAppInfo.CheckHandleOnEveryXXSQLEXE_XXRQUERY") = "N" ;
       AND oAppInfo.CheckHandleOnEveryXXSQLEXE_XXRQUERY = 0
  ****************************************************
    *
    *  no handle check here, typically in a 2-tier
    *  environment, when the AppInfo/"CheckHandleOnEveryXXSQLEXE_XXRQUERY"
    *  record is set to "0"/zero
    *
    *  XXDTES("XXFWDATA.PRG","CheckHandleOnEveryXXSQLEXE_XXRQUERY")
    *          
    llSuccess = .t.
  ****************************************************
  OTHERWISE    
  ****************************************************
    *
    *  see if lnHandle is valid 
    *
    IF TYPE("oLib.oConnectionSvc.BaseClass") = "C"    
      *
      *  MODIFY CLASS cusConnectionSvc OF XXFWLIBS METHOD ValidConnectHandle
      *
      llSuccess = oLib.oConnectionSvc.ValidConnectHandle(m.lnHandle)
      IF NOT m.llSuccess
        llInvalid = .t.
        ASSERT .f. message ;
             "When XXSQLEXE.PRG called oLib.oConnectionSvc.ValidConnectHandle(lnHandle), " + ;
             "it found handle/tnHandle " + TRANSFORM(m.lnHandle) + " " + ;
             "to be invalid."
      ENDIF
     ELSE
      llSuccess = .t.
    ENDIF
ENDCASE
  
*
*  now see if lnHandle is busy
*      
IF m.llSuccess
  DO WHILE .t.
    IF TYPE("oLib.oConnectionSvc.BaseClass") = "C"      
      *
      *  MODIFY CLASS cusConnectionSvc OF XXFWLIBS METHOD BusyConnection
      *
      llSuccess = NOT oLib.oConnectionSvc.BusyConnection(m.lnHandle)
    ENDIF
    DO CASE 
      CASE m.llSuccess
        EXIT 
      CASE NOT m.tlAllowUserToRetryWhenBusy
        *
        *  including X6ISCOM()
        *
        EXIT 
      CASE X3MSGSVC("Server busy, keep trying?") = "Y"
        *
        *  stay here and try again
        *
      OTHERWISE
        *
        *  user answered "NO" to the above X3MSGSVC()
        *
        EXIT 
    ENDCASE
  ENDDO
ENDIF

IF NOT m.llSuccess
  llBusy = .t.
ENDIF

IF m.llSuccess
  *
  *  initialize any passed parameters list
  *
  IF TYPE("m.taParms[1]") = "C" AND NOT ISNULL(m.taParms[1]) AND NOT EMPTY(m.taParms[1])
    LOCAL lcString, lcPName, yy
    FOR yy = 1 TO ALEN(taParms,1)
      lcString = "LOCAL " + m.taParms[m.yy,1]
      &lcString
      lcName = m.taParms[m.yy,1]
      STORE m.taParms[m.yy,2] TO &lcName    
    ENDFOR
  ENDIF
ENDIF

lnReturn = 0
lcSQL = m.tcSQL
IF m.llSuccess
  *
  *  do the SQLExec()
  *
  IF LEFTC(m.lcSQL,1) = "~"
    lcSQL = "SET FMTONLY ON " + SUBSTRC(m.lcSQL,2) + " SET FMTONLY OFF"
  ENDIF
  IF VARTYPE(m.tcResultCursor) = "C" ;
       AND NOT EMPTY(m.tcResultCursor)
    TRY
    lnReturn = SQLExec(m.lnHandle,m.lcSQL,m.tcResultCursor)
    CATCH
    lnReturn = -1
    ENDTRY
   ELSE
    TRY
    lnReturn = SQLExec(m.lnHandle,m.lcSQL)
    CATCH
    lnReturn = -1
    ENDTRY
  ENDIF
  llSuccess = m.lnReturn > 0
  IF NOT m.llSuccess
    *
    *  grab gaDevError right away, since VFP frequently
    *  populates Column2 with something bogus if you
    *  wait too long...
    *
    RELEASE gaDevError
    PUBLIC gaDevError[1]
    AERROR(gaDevError)
  ENDIF
ENDIF

*
*  log this action, if we're in diagnostic mode
*
IF VARTYPE(m.oLib) = "O" AND oLib.DiagnosticMode()
  *
  *  MODIFY CLASS cusAppLibs OF XXFW METHOD DiagnosticMode
  *
  *  don't log anything here if this .PRG is called
  *  from a VMP n-Tier services object -- these
  *  methods call XXLOGGER() directly
  *
  LOCAL llLogIt, laStack[4]
  laStack[1] = ".FetchData "
  laStack[2] = ".Open "
  laStack[3] = ".SQLExecuteLocal "
  laStack[4] = ".SQLExecuteRemote "
  IF X2PSTACK(@m.laStack)  
    *
    *  XXSQLEXE() has been called from a VMP n-Tier services 
    *  method -- suppress the XXLOGGER() here, because
    *  those methods handle logging there
    *    XXDTES("XXFWNTDS.VCX","XXLOGGER(","cusDataSourceBehavior","SQLExecuteRemote")
    *
   ELSE
    *
    *  do the logging here
    *
    LOCAL lcLogString, lcParmString1, lcParmString2, xx, ;
          lcParmString, llError, lcParmValue, lcParmDT, ;
          lcUnParmString
    IF m.llSuccess
      lcLogString = "Successful SQL" 
     ELSE
      lcLogString = "Failed SQL"
    ENDIF
    lcLogString = m.lcLogString + " from XXSQLEXE():" + CHR(13) + CHR(10) + m.lcSQL

    lcParmString1 = SPACE(0)
    IF NOT VARTYPE(m.taParms) = "U" AND X2IsArray(@taParms) 
      lcParmString1 = CHR(13) + CHR(10) + ;
                      SPACE(2) + ;
                      PADR("Parameter Name",24) + SPACE(2) + ;
                      PADR("Value",50) + SPACE(2) + ;
                      "DataType" + ;
                      CHR(13) + CHR(10)
      FOR xx = 1 TO ALEN(taParms,1)
        lcParmString1 = m.lcParmString1 + SPACE(2) + ;
                        PADR(taParms[m.xx,1],24) + SPACE(2) + ;
                        PADR(TRANSFORM(taParms[m.xx,2]),50) + SPACE(2) + ;
                        VARTYPE(taParms[m.xx,2]) 
        IF m.xx = ALEN(taParms,1)
         ELSE
          lcParmString1 = m.lcParmString1 + CHR(13) + CHR(10)
        ENDIF
      ENDFOR 
    ENDIF   &&& we have taParms parameters

    lcParmString2 = SPACE(0)
    lcUnParmString = SPACE(0)
    IF OCCURS("?",m.lcSQL) > 0
      lcUnParmString = m.lcSQL
      IF EMPTY(m.lcParmString1)
        lcParmString2 = CHR(13) + CHR(10) + ;
                        SPACE(2) + ;
                        PADR("Parameter Name",24) + SPACE(2) + ;
                        PADR("Value",50) + SPACE(2) + ;
                        "DataType" + ;
                        CHR(13) + CHR(10)
      ENDIF
      FOR xx = 1 TO OCCURS("?",m.lcSQL)
        lcParmString = GETWORDNUM(m.lcSQL,m.xx+1,"?")
        lcParmString = ALLTRIM(m.lcParmString)
        lcParmString = GETWORDNUM(m.lcParmString,1)
        llError = .f.
        TRY
        lcParmValue = EVALUATE(m.lcParmString)
        CATCH
        llError = .t.
        ENDTRY
        IF m.llError
          lcParmValue = "(unknown)"
          lcParmDT = "(unknown)"
         ELSE
          lcParmDT = VARTYPE(m.lcParmValue)
          IF m.lcParmDT = "C"
            IF OCCURS(['],m.lcParmValue) > 0
              lcParmValue = ["] + m.lcParmValue + ["]
             ELSE
              lcParmValue = ['] + m.lcParmValue + [']
            ENDIF
           ELSE 
            lcParmValue = TRANSFORM(m.lcParmValue)
          ENDIF
        ENDIF
        lcParmString2 = m.lcParmString2 + SPACE(2) + ;
                        PADR(m.lcParmString,24) + SPACE(2) + ;
                        PADR(m.lcParmValue,50) + SPACE(2) + ;
                        m.lcParmDT
        IF m.xx = OCCURS("?",m.lcSQL)
         ELSE
          lcParmString2 = m.lcParmString2 + CHR(13) + CHR(10)
        ENDIF
        lcUnParmString = STRTRAN(m.lcUnParmString, ;
                                 "?" + m.lcParmString, ;
                                 m.lcParmValue, ;
                                 1, ;
                                 1, ;
                                 1)
        lcUnParmString = STRTRAN(m.lcUnParmString, ;
                                 "? " + m.lcParmString, ;
                                 m.lcParmValue, ;
                                 1, ;
                                 1, ;
                                 1)
      ENDFOR 
      lcUnParmString = CHR(13) + CHR(10) + m.lcUnParmString 
    ENDIF   &&& we have ? parameters
  
    lcLogString = m.lcLogString + ;
                  m.lcParmString1 + ;
                  m.lcParmString2 + ;
                  m.lcUnParmString
    XXLOGGER(m.lcLogString) 
  ENDIF
ENDIF

IF NOT m.llSuccess
  *
  *  failure in development mode -- do some debugging
  *  stuff
  *
  LOCAL loActiveForm
  loActiveForm = X6SAF()
  IF (VARTYPE(m.oApp) = "O" AND oApp.InDevelopment()) ;
       OR ;
       (VARTYPE(m.loActiveForm)="O" AND VARTYPE(loActiveForm.icSubclassingPotential)="C" AND (loActiveForm.GetPProp("ilStandalone")=.T. OR loActiveForm.DevMode())) ;
       OR Application.StartMode = 0
    *
    *  output the problem lcSQL to _Screen
    *
    ACTIVATE SCREEN
    ? m.lcSQL
    *
    *  gaDevError was populated earlier in this .PRG,
    *  right after the SQLEXEC() failed
    *
    FOR xx = 1 TO ALEN(gaDevError,1)
      ? gaDevError[m.xx,1]
      ? SPACE(2), gaDevError[m.xx,2]
      ? SPACE(2), gaDevError[m.xx,3]
    ENDFOR
  ENDIF
  _ClipText = m.lcSQL
  ASSERT .f. message PROGRAM() + " failed to execute the SQL successfully"
ENDIF   &&& NOT llSuccess

IF m.llSuccess
  RETURN m.lnReturn
ENDIF

LOCAL m.llSuppressShutdownHere
IF m.tlPresentErrorMessage
  *
  *  the error can be one of several categories:
  *  - tnHandle was invalid
  *  - tnHandle was busy
  *  - the SQLExec() failed because tcSQL was an invalid VFP string, 
  *    in which case AERROR() applies
  *  - the SQLExec() failed because tcSQL had a problem on the back end, 
  *    in which case the native VFP AERROR() yields a 1526 ODBC error
  *
  IF m.llInvalid OR m.llBusy
   ELSE
    LOCAL ARRAY laError[1]
    AERROR(laError)
  ENDIF
  DO CASE
    ***************************************************
    CASE m.llInvalid 
    ***************************************************
      *
      *  tnHandle was invalid 
      *
      IF m.tlShutdownIfProblem AND VARTYPE(m.oApp) = "O" AND NOT m.llSuppressShutdownHere
        X3MSGSVC("Unable to access server -- invalid connection, shutdown")
       ELSE  
        X3MSGSVC("Unable to access server - invalid connection")
      ENDIF
    ***************************************************
    CASE m.llBusy
    ***************************************************
      *
      *  tnHandle was busy, exceeding either 
      *  - the limits set by THIS.BusyConnection()
      *  - the patience of the user
      *
      IF m.tlShutdownIfProblem AND VARTYPE(m.oApp) = "O" ; 
           AND NOT m.llSuppressShutdownHere
        X3MSGSVC("Unable to access server -- busy connection, shutdown")
       ELSE
        X3MSGSVC("Unable to access server - busy connection")
      ENDIF
    ***************************************************
    CASE laError[1] = 1526
    ***************************************************
      *
      *  the SQLExec() failed due to a problem executing tcSQL on the 
      *  back end, generating an ODBC error #1526
      *
      IF TYPE("oLib.oConnectionSvc.BaseClass") = "C" ;
           AND oLib.oConnectionSvc.ODBCErrorMessage(@m.laError)
        *
        *  if oLib.oConnectionSvc.ODBCErrorMessage() RETURNs .T., assume
        *  you've already issued the oApp.ForceShutdown() if necessary
        *
        llSuppressShutdownHere = .t.
       ELSE
        *
        * handle here if ODBCErrorMessage() returns .f.
        *
        IF m.tlShutdownIfProblem AND VARTYPE(m.oApp) = "O" AND NOT m.llSuppressShutdownHere
          X3MSGSVC("Unable to access server -- ODBC, shutdown")        
         ELSE
          X3MSGSVC("Unable to access server - ODBC")        
        ENDIF
        IF (VARTYPE(m.oApp) = "O" AND oApp.InDevelopment()) ;
            OR ;
           (VARTYPE(m.oLib) = "O" AND oLib.DevMode())
          *
          *  in development
          *
          LOCAL lcString
          lcString = ;
          "Unable to access data on the server because of an ODBC problem.  " + ;
          "Here is the ODBC error information from AERROR():" + chr(13) + ;
          "AERROR[1]: " + TRANSFORM(laError[1]) + CHR(13) + ;
          "AERROR[2]: " + TRANSFORM(laError[2]) + CHR(13) + ;
          "AERROR[3] (ODBC error message): " + TRANSFORM(laError[3]) + CHR(13) + ;
          "AERROR[4] (current ODBC SQL state): " + TRANSFORM(laError[4]) + CHR(13) + ;
          "AERROR[5] (error number from ODBC data source): " + TRANSFORM(laError[5]) + CHR(13) + ;
          "AERROR[6] (ODBC connection handle): " + TRANSFORM(laError[6]) 
          X8SETCLS("XXFWUTIL.VCX")
          CREATEOBJECT("cusDevMessage",m.lcString)
        ENDIF &&& in development
      ENDIF
    ***************************************************
    OTHERWISE
    ***************************************************
      *
      *  let the error handler take over
      *
      ERROR (laError[1])
*!*	    ***************************************************
*!*	    CASE laError[1] # 1526
*!*	    ***************************************************
*!*	      *
*!*	      *  the SQLExec() failed due to a problem with the tcSQL 
*!*	      *  string itself -- generate the VFP error 
*!*	      *  "SQL Select statement is invalid" and let the error 
*!*	      *  handler take over
*!*	      *
*!*	      error 1498
*!*	    ***************************************************
*!*	    OTHERWISE
*!*	    ***************************************************
*!*	      *
*!*	      *  this code should never be hit...
*!*	      *
*!*	      x3msgsvc("Unable to access server data")
*!*	      X8SETCLS("XXFWUTIL.VCX")
*!*	      CREATEOBJECT("cusDevMessage","XXSQLEXE() has encountered a problem of an unknown source.")
  ENDCASE
ENDIF

DO CASE
  ******************************************************
  CASE (VARTYPE(m.oApp) = "O" AND oApp.InDevelopment()) ;
        OR ;
       (VARTYPE(m.oLib) = "O" AND oLib.DevMode())
  ******************************************************
    *
    *  in development mode 
    *
    _ClipText = m.lcSQL
    ASSERT .f. message ;
          "XXSQLEXE() has failed to run your SQLExec() successfully.  " + ;
          "In addition to allowing you to <Debug> right now, " + ;
          "the passed SQL statement has been " + ;
          "copied to the clipboard, so you can paste it " + ;
          "anywhere you want when you get back to the Command Window." 
  ******************************************************
  CASE m.tlShutdownIfProblem ;
       AND VARTYPE(m.oApp) = "O" ;
       AND NOT m.llSuppressShutdownHere
  ******************************************************
    *
    *  in an application and things are so bad we're
    *  about to shut down
    *
    oApp.ForceShutdown()
  ******************************************************
  OTHERWISE
  ******************************************************
    *
    *  in an application or otherwise not in "development
    *  mode" and things are not bad enough to shut down
    *    
ENDCASE

SELECT (m.lnSelect)

RETURN -1
