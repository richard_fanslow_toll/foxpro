*
*  X3VUSPHN.PRG
*  Make sure the passed US phone/fax number
*  is either completely filled-in or completely
*  empty -- no blanks
*
*  The phone/fax number is expected to be
*  passed with the area code and with or
*  without formatting characters ( ) -
*
*  Usage:
*  xx = x3vusphn("123-456-7890")
*  xx = x3vusphn("   -456-7890",.f.,.t.)
*  xx = x3vusphn("1 3-456-7890")
*  xx = x3vusphn("   -   -    ",.t.)
*  xx = x3vusphn("(123)456-7890")
*
*  lParameters
*    tcPFString       (R) Passed phone/fax number
*    tlMayNotBeEmpty  (O) Must the passed phone/fax number be filled?
*                         Defaults to .f. -- empty/blank is OK
*    tlAreaCodeNotReq (O) Is the Area Code not required?
*                         Defaults to .f. -- area code is required
*
lparameters tcPFString, tlMayNotBeEmpty, tlAreaCodeNotReq
IF type("tcPFString") # "C" 
  return .f.
ENDIF
IF type("tlMayNotBeEmpty") # "L"
  tlMayNotBeEmpty = .f.
ENDIF
IF empty(tcPFString)
  return !tlMayNotBeEmpty
ENDIF
IF type("tlAreaCodeNotReq") # "L"
  tlAreaCodeNotReq = .f.
ENDIF
local lcString
*
* remove format characters:
*
lcString = chrtranc(tcPFString,")",space(0))
lcString = chrtranc(lcString,"(",space(0))
lcString = chrtranc(lcString,"-",space(0))
IF empty(lcString)
  return !tlMayNotBeEmpty
ENDIF
*
*  at this point, !empty(lcString)
*
IF tlAreaCodeNotReq 
  IF space(1) $ substrc(lcString,4) ;
       OR (!empty(leftc(lcString,3)) AND space(1)$leftc(lcString,3))
    *
    *  Area Code is not required but
    *  either it is incomplete or 
    *  the rest of the phone number is incomplete
    *
    return .f.
  ENDIF
 ELSE
  IF (empty(leftc(lcString,3)) OR space(1)$lcString)
    *
    *  Area Code is required but not filled in
    *  or lcString contains a space  
    *
    return .f.
  ENDIF
ENDIF
local lnCount, llSuccess
llSuccess = .t.
IF tlAreaCodeNotReq
  IF !empty(leftc(lcString,3))
    *
    *  Area Code is not required, but it
    *  should be either all filled in or
    *  empty
    *
    FOR lnCount = 1 to 3
      IF !isdigit(substrc(lcString,lnCount,1))
        llSuccess = .f. 
        exit
      ENDIF make sure each area code character is a digit
    ENDFOR loop thru the area code
  ENDIF
  IF llSuccess
    *
    *  now check the rest of the phone number
    *
    FOR lnCount = 4 to lenc(lcString) 
      IF !isdigit(substrc(lcString,lnCount,1))
        llSuccess = .f. 
        exit
      ENDIF make sure each character is a digit
    ENDFOR loop thru the area code
  ENDIF
 ELSE
  *
  *  Area Code is required -- all characters
  *  must be digits
  *
  FOR lnCount = 1 to lenc(lcString)
    IF !isdigit(substrc(lcString,lnCount,1))
      llSuccess = .f. 
      exit
    ENDIF make sure each filled-in character is a digit
  ENDFOR loop thru all characters in tcPFString
ENDIF tlAreaCodeNotReq
return llSuccess
