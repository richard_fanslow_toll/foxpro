*
*  X2RELXPR.PRG
*  Returns a character string containing the
*  RELATION() relational expression for the
*  SET RELATION TO <relational expression> INTO
*  from the passed "parent" alias into the passed
*  "child" alias.
*
*  If there is no relation in effect from the
*  "parent" alias into the "child" alias, SPACE(0)
*  is returned.
*  
*  Copyright (c) 1997-2005 Visionpace   All Rights Reserved
*                17501 East 40 Hwy., Suite 218
*                Independence, MO 64055
*                816-350-7900 
*                http://www.visionpace.com
*                http://vmpdiscussion.visionpace.com
*  Author:  Drew Speedie
*
*  Usage:
*   IF !empty(x2relxpr(<ParentAlias>,<ChildAlias>))
*     *  a relation is in effect from ParentAlias-->ChildAlias
*   ENDIF
*
*   local lcRelXpr
*   lcRelXpr = x2relxpr(<ParentAlias>,<ChildAlias>,.t.)
*   * use lcRelXpr (with parent ALIAS() inserted before
*   * each parent field name) for something
*
*
*   USE VM!Customers IN 0 ORDER tag Cus_PK
*   USE VM!CustContacts IN 0 ORDER tag CCo_CusFK
*   SELECT Custcontacts
*   SET RELATION TO CCo_CusFK INTO Customers
*   ? x2relxpr("CustContacts","Customers",.t.)
*
*
*  lParameters
*    tcParent (R) Alias of the "parent" to be tested
*     tcChild (R) Alias of the "child" to be tested
*  tlAddAlias (O) Logical flag indicating whether you
*                 want the ALIAS() of the "parent" to
*                 be added to each field in the relational
*                 expression
*
lParameters tcParent, tcChild, tlAddAlias

IF !used(tcParent) OR !used(tcChild)
  return space(0)
ENDIF

IF empty(target(1,tcParent))
  return space(0)
ENDIF

*
*  tcParent has at least one relation in
*  effect, but is tcChild the target of
*  any of them?
*
local lnCount, lcTarget, lcRelation
lcRelation = space(0)
lnCount = 1
DO WHILE .t.
  lcTarget = upper(alltrim(target(lnCount,tcParent)))
  IF empty(lcTarget)
    exit
  ENDIF
  IF lcTarget == upper(alltrim(tcChild))
    lcRelation = upper(relation(lnCount,tcParent))
    exit
  ENDIF
  lnCount = lnCount + 1
ENDDO

IF tlAddAlias AND !empty(lcRelation)
  DO CASE
    CASE occurs("(",lcRelation) = 0 AND occurs("+",lcRelation) = 0
      *
      *  single-field expression, no functions
      *  i.e "FIELD1"
      *
      lcRelation = tcParent + "." + lcRelation
    CASE occurs("(",lcRelation) = 0
      * 
      *  multi-field concatenation, no functions
      *  i.e. "FIELD1+FIELD2+FIELD3"
      *
      lcRelation = tcParent + ;
                   "." + ;
                   strtran(lcRelation,"+","+"+tcParent+".")
    OTHERWISE
      *
      *  single-field or multi-field concatenation, 
      *  at least one part is a field expression, not 
      *  just a field name
      *  i.e. "FIELD1+UPPER(FIELD2)+STR(FIELD3)"
      *  WARNING:  Each field name must be unique
      *            in tcParent -- if not, things
      *            will get pretty hosed up.  For example,
      *            if tcParent has the following
      *            fields:
      *              INV_Number           
      *              INV_Date
      *              INV_DateFlag
      *            and lcRelation is
      *              INV_Number + dtoc(INV_Date) + INV_DateFlag
      *            then this routine can return
      *            "INVOICES.INV_NUMBER+DTOC(INVOICES.INV_DATE)+INVOICES.INV_DATE"
      *           
      *
      FOR lnCount = 1 to fcount(tcParent)
        IF occurs(field(lnCount,tcParent),lcRelation) > 0
          lcRelation = strtran(lcRelation, ;
                       field(lnCount,tcParent), ;
                       tcParent+"."+field(lnCount,tcParent))
        ENDIF
      ENDFOR
  ENDCASE
ENDIF

return lcRelation
