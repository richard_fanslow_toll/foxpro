*
*  X6ICC.PRG
*  This program is a wrapper to the IsDestinationReachable()/
*  InternetCheckConnection() API functions, and returns an
*  integer value indicating whether this computer has a 
*  connection to the Internet:
*    1 = the passed (or default) URL can be reached
*    0 = the passed (or default) URL cannot be reached
*   -1 = unable to load or execute the necessary .DLLs 
*        to attempt the check -- this computer MAY in fact
*        be connected to the Internet
*  The check is done by attempting to connect to the passed URL,
*  which defaults to http://www.microsoft.com/ if not passed.
*
*  Copyright (c) 2001-2005 Visionpace   All Rights Reserved
*                17501 East 40 Hwy., Suite 218
*                Independence, MO 64055
*                816-350-7900 
*                http://www.visionpace.com
*                http://vmpdiscussion.visionpace.com
*  Author:  Glenn Taylor, Drew Speedie, Art Bergquist
*
*  Please note that there are a myriad of scenarios under 
*  which this .PRG RETURNs -1, including:
*    - operating system that doesn't support one of the 
*      .DLLs called here (i.e. some Win95 scenarios)
*    - browser that doesn't respond to the .DLL function
*      calls (.i.e some early versions of IE, other 
*      browsers)
*  ...So when this method RETURNs -1, it is actually possible
*  that this machine IS connected to the Internet... 
*

LPARAMETERS tcURL

LOCAL lcURL, lnResult, lnReturnValue, llError, lcOnError, llResult
lcOnError = on("ERROR")
lnReturnValue = -1
llError = .f.
on error llError = .t.
DECLARE INTEGER IsDestinationReachable IN Sensapi.dll string, string 
on error &lcOnError
IF llError
  return lnReturnValue  &&& -1
ENDIF

*
*  127.0.0.1 is any machine's internal TCP/IP loopback address -
*  if connecting to this one returns false, there is no TCP/IP 
*  stack loaded in your machine  - IsDestinationReachable() uses 
*  TCP/IP only
*
llError = .f.
on error llError = .t.
IF IsDestinationReachable('127.0.0.1',null) # 1
  llError = .t.
ENDIF
on error &lcOnError
IF llError
  return lnReturnValue  &&& -1
ENDIF

IF VARTYPE(tcURL) # "C" OR ISNULL(tcURL) OR EMPTY(tcURL)
  *
  *  default URL if not passed
  *
  lcURL = "http://www.microsoft.com/"
 ELSE
  lcURL = tcURL
ENDIF

llError = .f.
on error llError = .t.
lnResult = IsDestinationReachable(lcURL,null) 
llResult = lnResult=1
on error &lcOnError
IF llError
  return lnReturnValue  &&& -1
ENDIF
IF llResult
  *
  *  IsDestinationReachable() succeeded, no need to continue
  *
  lnReturnValue = 1
  RETURN lnReturnValue   &&& 1
ENDIF

*
*  above call to IsDestinationReachable() did not generate an
*  error, but didn't reach lcURL either, so since failure of
*  IsDestinationReachable() isn't definitive, continue on
*  with the code below to try another technique
*

#DEFINE FLAG_ICC_FORCE_CONNECTION    1
#DEFINE ERROR_NOT_CONNECTED       2250		&& This network connection does not exist

llError = .f.
on error llError = .t.
DECLARE INTEGER InternetCheckConnection ;
     IN wininet STRING @lpszUrl, ;
	 INTEGER  dwFlags, ;
	 INTEGER  dwReserved
on error &lcOnError
IF llError
  return lnReturnValue  &&& -1
ENDIF

llError = .f.
on error llError = .t.
DECLARE INTEGER GetLastError IN Win32API
on error &lcOnError
IF llError
  return lnReturnValue  &&& -1
ENDIF

*
*  trap for scenarios where the call to the 
*  InternetCheckConnection() function generates an error,
*  typically because it is unavailable 
*
llError = .f.
on error llError = .t.
lnResult = InternetCheckConnection(@lcURL, FLAG_ICC_FORCE_CONNECTION, 0)
on error &lcOnError
IF llError
  return lnReturnValue  &&& -1
ENDIF

IF lnResult = 0
  lnResult = GetLastError()
  IF lnResult = ERROR_NOT_CONNECTED
    lnReturnValue = 0
   ELSE
    *
    *  12007 not returned for a bogus URL, but that wasn't
    *  listed in the error messages
    *
    lnReturnValue = 0
  ENDIF
 ELSE
  lnReturnValue = 1
ENDIF

RETURN lnReturnValue


*!*	see also
*!*	http://support.microsoft.com/support/kb/articles/Q242/5/58.ASP
*!*	(InternetGetConnectedState)

