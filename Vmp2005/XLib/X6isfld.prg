*
*  X6ISFLD.PRG
*  Returns the logical value indicating whether or not the 
*  passed field name is contained in: 
*      the currently-selected alias
*      the passed alias (which must be open)
*
*  Copyright (c) 1997-2005 Visionpace   All Rights Reserved
*                17501 East 40 Hwy., Suite 218
*                Independence, MO 64055
*                816-350-7900 
*                http://www.visionpace.com
*                http://vmpdiscussion.visionpace.com
*  Author:  Drew Speedie
*
*  Usage:
*    LOCAL llValidField
*    llValidField = X6ISFLD("CUSTOMERS.Cus_Name")
*    SELECT Customers
*    llValidField = X6ISFLD("Cus_Name")
*
*  Parameters
*    tcString (R) Name of the field to check.  May
*                 be in the format 
*                   FieldName -- ALIAS() is assumed
*                   Alias.FieldName -- Alias must be USED()
*
LPARAMETERS tcString
LOCAL llRetval, lcAlias, lcFieldName
DO CASE
  CASE OCCURS(".",tcString) = 1
    lcAlias = JUSTSTEM(tcString) 
    lcFieldName = JUSTEXT(tcString)
  CASE NOT EMPTY(ALIAS())
    lcAlias = ALIAS()
    lcFieldName = tcString
  OTHERWISE 
    lcAlias = SPACE(0)
    lcFieldName = SPACE(0)
ENDCASE
IF EMPTY(lcAlias) OR NOT USED(lcAlias)
  *
  *  one of the following is true:
  *  - tcString was passed in the format Alias.FieldName,
  *    and the specified Alias is not USED() 
  *    ...note that if there is an existing Object with 
  *    the same name as Alias, and it has a property of 
  *    with the same name as FieldName, the above IF 
  *    checks for that via the NOT USED(lcAlias) -- if 
  *    such an object exists AND Alias is open, the TYPE() 
  *    function used below evaluates the Alias.FieldName 
  *    over the Object.Property...
  *  - tcString was passed in the format FieldName,
  *    and the current ALIAS() (workarea) is empty
  *
  RETURN .f.
ENDIF
IF NOT TYPE(lcAlias+"."+lcFieldName) = "U"
  llRetVal = .t.
ENDIF
RETURN llRetVal
