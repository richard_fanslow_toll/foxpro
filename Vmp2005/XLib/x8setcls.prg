*
*  X8SETCLS.PRG
*  SET CLASSLIB to the passed .VCX ADDITIVE
*
*  Use this routine instead of X3SETCLS.PRG, which is
*  included for backward compatibility of existing
*  VMP application code.
*
*  Copyright (c) 2004-2005 Visionpace   All Rights Reserved
*                17501 East 40 Hwy., Suite 218
*                Independence, MO 64055
*                816-350-7900 
*                http://www.visionpace.com
*                http://vmpdiscussion.visionpace.com
*  Author:  Drew Speedie and Art Bergquist
*           Special thanks to Mike Yearwood and Brian Grant
*
*  USAGE:
*    X8SETCLS("XXFWUTIL")
*    X8SETCLS("XXFW.VCX")
*    IF NOT X8SETCLS("MY.VCX")
*      * take appropriate action
*    ENDIF
*    X8SETCLS("XXFW.VCX,XXFWUTIL.VCX")
*    IF NOT X8SETCLS("XXFW,XXFWUTIL")
*      * take appropriate action
*    ENDIF
*
*  RETURNS:
*    A logical value indicating success
*
*  NOTE that this routine is anywhere from 10 to
*  50 times slower than a plain SET CLASSLIB TO command.
*  Use this wrapper when you need the features is
*  supports, at a minimum to be able to trap for a
*  SET CLASSLIB TO failure.  Only pass the optional
*  tlRemoveNonExistentVCXs and tlPutNewClassLibFirst
*  parameters when you need their specific functionality,
*  because they slow this routine down the most.
*  
*  When you are positive that a class library definitely
*  exists, you can optimize performance by not calling
*  this routine at all, or as a secondary option:
*!*	IF NOT "\MYCLASSLIB.VCX" $ SET("CLASSLIB")
*!*	  SET CLASSLIB TO MyClassLib ADDITIVE 
*!*	ENDIF

*!*	IF NOT "\MYCLASSLIB.VCX" $ SET("CLASSLIB") ;
*!*	     AND NOT X8SETCLS("MyClassLib.VCX")
*!*	  *  take indicated action
*!*	ENDIF     

*!*	IF "\MYCLASSLIB.VCX" $ SET("CLASSLIB") ;
*!*	     OR X8SETCLS("MyClassLib.VCX")
*!*	  *  take indicated action
*!*	ENDIF
*
*
*  lParameters
*     tcClassLib (R) Name of the .VCX class library (or 
*                      libraries) to be added to the 
*                      SET CLASSLIB list.  The ".VCX" 
*                      extension is optional.  
*                    You can specify multiple (comma-separated) 
*                      class libraries, but if you do -- you 
*                      cannot *also* specify an .APP/.EXE 
*                      file in tcInAppExeFile
*                    If tcInAppExeFile is not passed and the
*                      SET CLASSLIB TO (tcClassLib) ADDITIVE
*                      fails and this routine is executed from
*                      within a distributed EXE/APP, then a
*                      SET CLASSLIB TO (tcClassLib) IN (SYS(16,0))
*                      is attempted.
*                    If tcInAppExeFile is not passed and the
*                      SET CLASSLIB TO (tcClassLib) ADDITIVE
*                      fails and FILE("XXLIB.APP"), then a
*                      SET CLASSLIB TO (tcClassLib) IN XXLIB.APP
*                      is attempted.
*                    If tcInAppExeFile is not passed and the
*                      SET CLASSLIB TO (tcClassLib) ADDITIVE
*                      fails and FILE("XXLIB.EXE"), then a
*                      SET CLASSLIB TO (tcClassLib) IN XXLIB.EXE
*                      is attempted.
*                    If tcInAppExeFile is not passed and the
*                      SET CLASSLIB TO (tcClassLib) ADDITIVE
*                      fails and there are EXEs/APPs in the
*                      SET PROCEDURE TO list, then a
*                      SET CLASSLIB TO (tcClassLib) IN APP/EXE
*                      is attempted for each EXE/APP in the
*                      SET PROCEDURE TO list until one is
*                      successful (if any).
*
* tcInAppExeFile (O) Name of an .APP/.EXE file that contains 
*                      tcClasslib, maps to the IN AppEXEFileName 
*                      supported by the SET CLASSLIB TO
*                      command.
*                    If you specify multiple class libraries 
*                      in tcClassLib, then this parameter is 
*                      not supported, and this routine 
*                      RETURNs .f.
*                      
*    tlListFirst (O) Pass this parameter as .T. if you want
*                      tcClassLib set to the FIRST position
*                      in the SET CLASSLIB TO list, rather
*                      than the LAST position that is the
*                      default behavior -- when there are
*                      potentially more than one .VCX in the
*                      list that contain a class of the same
*                      name, pass this parameter as .T. to
*                      make tcClassLib first and therefore the
*                      one in which VFP will find the class
*                      in a subsequent CREATEOBJECT() call.
*                    Ignored when you pass a comma-separated
*                      list of multiple class libraries.
*
* tlClearNonExistentVCXs (O) Pass this parameter as .T. if
*                              you want this routine to check
*                              all class libraries currently 
*                              in the SET CLASSLIB TO list, and
*                              remove any class libraries that
*                              no longer exist/cannot be found.
*                            This feature is important in
*                              scenarios where you create
*                              temporary .VCXs and erase them,
*                              because when the SET CLASSLIB TO
*                              list includes a class library that
*                              no longer exists, issuing a
*                              plain SET CLASSLIB TO not only
*                              generates an error, but it also
*                              *clears* the current SET CLASSLIB
*                              TO list.
*                            Ignored when you pass a comma-
*                              separated list of multiple class 
*                              libraries.
*
LPARAMETERS tcClassLib as String, ;
            tcInAppExeFile as String, ;
            tlListFirst as Boolean, ;
            tlClearNonExistentVCXs as Boolean

LOCAL lcClassLib, lcInAppExeFile

IF VARTYPE(m.tcClassLib) = "C" AND NOT EMPTY(m.tcClassLib)
  lcClasslib = UPPER(ALLTRIM(m.tcClassLib))
  IF EMPTY(JUSTEXT(m.lcClassLib))
    lcClassLib = m.lcClassLib + ".VCX"
  ENDIF
 ELSE
  ASSERT .f. message "tcClassLib has been passed incorrectly or not passed at all"
  RETURN .f.
ENDIF

IF VARTYPE(m.tcInAppExeFile) = "C" AND NOT EMPTY(m.tcInAppExeFile)
  lcInAppExeFile = UPPER(ALLTRIM(m.tcInAppExeFile))
 ELSE
  lcInAppExeFile = SPACE(0)
ENDIF

IF NOT EMPTY(m.lcInAppExeFile) AND "," $ m.lcClassLib 
  *
  *  if tcInAppExeFile has been passed, this routine only
  *  supports a single class library passed in tcClassLib
  *
  ASSERT .f. message "tcInAppExeFile cannot be passed if multiple class libraries are passed in tcClassLib"
  RETURN .f.
ENDIF

LOCAL llSuccess, lcSingleClassLib, lcCurrentSetClassLib, ;
      laClassLibs[1], lcString, lcLine, llSuccess, ;
      lcString, lcLine, laClassLibs[1]
llSuccess = .t.
lcCurrentSetClassLib = SET("CLASSLIB")
lcString = SPACE(0)
IF NOT EMPTY(SET("CLASSLIB")) ;
     AND VARTYPE(m.tlClearNonExistentVCXs) = "L" ;
     AND m.tlClearNonExistentVCXs
  *
  *  remove .VCXs no longer available, for whatever reason
  *
  ALINES(laClassLibs,SET("CLASSLIB"),.t.,",")
  FOR EACH lcLine IN laClassLibs
    IF FILE(CHRTRAN(LEFTC(m.lcLine, AT_C(" ALIAS ", m.lcLine) - 1), ["], SPACE(0)))
      lcString = m.lcString + m.lcLine + ", "
     ELSE
      *  ignore files no longer available
    ENDIF
  ENDFOR
  lcString = ALLTRIM(m.lcString)
  IF NOT EMPTY(m.lcString)
    IF RIGHTC(m.lcString,1) = ","
      lcString = LEFTC(m.lcString,LENC(m.lcString)-1)
    ENDIF
  ENDIF
  IF NOT UPPER(SET("CLASSLIB")) == UPPER(m.lcString)
    *
    *  at least one class library was removed above,
    *  so SET("CLASSLIB") needs updating or we'll
    *  never be able to SET CLASSLIB TO successfully
    *  below
    *
    TRY
    SET CLASSLIB TO (m.lcString) ADDITIVE
    CATCH
    llSuccess = .f.
    ENDTRY
  ENDIF
  lcCurrentSetClassLib = m.lcString
ENDIF
IF NOT m.llSuccess
  RETURN .f.
ENDIF

DO CASE
  ********************************************
  CASE NOT EMPTY(m.lcInAppExeFile)
  ********************************************
    TRY
    SET CLASSLIB TO (m.lcClassLib) IN (m.lcInAppExeFile) ADDITIVE 
    CATCH
    llSuccess = .f.
    ENDTRY
  ********************************************
  CASE "," $ m.lcClassLib
  ********************************************
    *  multiple class libraries passed in tcClassLib
    TRY
    SET CLASSLIB TO &lcClassLib ADDITIVE
    CATCH
    llSuccess = .f.
    ENDTRY
  ********************************************
  CASE FULLPATH(m.lcClassLib) $ SET("CLASSLIB")
  ********************************************
    *  single class library passed in tcClasslib,
    *  already in the SET("CLASSLIB") list
    lcSingleClassLib = m.lcClassLib
  ********************************************
  OTHERWISE
  ********************************************
    *  single class library passed in tcClassLib
    LOCAL lcInModule, llXXLIBAPPDone, llXXLIBEXEDone
    llSuccess = .t.
    TRY
    IF OCCURS(SPACE(1),m.lcClassLib)>0
      SET CLASSLIB TO (m.lcClassLib) ADDITIVE 
     ELSE
      SET CLASSLIB TO &lcClassLib ADDITIVE 
    ENDIF
    CATCH
    llSuccess = .f.
    ENDTRY
    IF NOT llSuccess
      lcInModule = SPACE(0)
      IF INLIST(UPPER(JUSTEXT(SYS(16,0))),"APP","EXE")
        *
        *  try IN the current highest-level executing .EXE/.APP
        *  
        lcInModule = " IN " + SYS(16,0)
      ENDIF
      llSuccess = .t.
      TRY
      SET CLASSLIB TO (m.lcClassLib) &lcInModule ADDITIVE 
      CATCH
      llSuccess = .f.
      ENDTRY
    ENDIF
    IF NOT llSuccess AND NOT EMPTY(SET("PROCEDURE"))
      *
      *  try IN any .APP/.EXE procedures in the
      *  SET PROCEDURE list
      *
      LOCAL laProcs[1], lnCount, lcProc
      lnCount = ALINES(laProcs,SET("PROCEDURE"),",")
      FOR EACH lcProc IN laProcs
        IF INLIST(JUSTEXT(ALLTRIM(m.lcProc)),"APP","EXE")
          IF UPPER(JUSTFNAME(ALLTRIM(m.lcProc))) = "XXLIB.APP"
            llXXLIBAPPDone = .t.
          ENDIF
          IF UPPER(JUSTFNAME(ALLTRIM(m.lcProc))) = "XXLIB.EXE"
            llXXLIBEXEDone = .t.
          ENDIF
          llSuccess = .t.
          TRY
          SET CLASSLIB TO (m.lcClassLib) IN (ALLTRIM(m.lcProc)) ADDITIVE 
          CATCH
          llSuccess = .f.
          ENDTRY
          IF m.llSuccess
            EXIT
          ENDIF
        ENDIF
      ENDFOR
    ENDIF
    IF NOT m.llSuccess AND FILE("XXLIB.APP") AND NOT m.llXXLIBAPPDone
      *
      *  try IN XXLIB.APP
      *
      llSuccess = .t.
      TRY
      SET CLASSLIB TO (m.lcClassLib) IN XXLIB.APP ADDITIVE 
      CATCH
      llSuccess = .f.
      ENDTRY
    ENDIF
    IF NOT m.llSuccess AND FILE("XXLIB.EXE") AND NOT m.llXXLIBEXEDone
      *
      *  try in XXLIB.EXE
      *
      llSuccess = .t.
      TRY
      SET CLASSLIB TO (m.lcClassLib) IN XXLIB.EXE ADDITIVE 
      CATCH
      llSuccess = .f.
      ENDTRY
    ENDIF
    IF llSuccess
      lcSingleClassLib = m.lcClassLib
    ENDIF
ENDCASE  

IF m.llSuccess ;
     AND NOT EMPTY(m.lcSingleClassLib) ;
     AND OCCURS(",",SET("CLASSLIB"))>0 ;
     AND VARTYPE(m.tlListFirst) = "L" AND m.tlListFirst
  *
  *  when you pass tcClassLib as a single .VCX, rearrange
  *  the SET("CLASSLIB") list so that tcClassLib is FIRST
  *  in the list, primarily to ensure that if you have
  *  a class in tcClassLib that is named the same as a
  *  class in some other class library in the SET("CLASSLIB")
  *  list, the just-added tcClassLib is FIRST in the list,
  *  rather than LAST in the list (the VFP default), the
  *  assumption being that you want the class in the
  *  just-added tcClassLib to be found by the next 
  *  CREATEOBJECT()
  *
  RELEASE laClassLibs
  LOCAL laClassLibs[1]
  lcString = SPACE(0)
  ALINES(laClassLibs,SET("CLASSLIB"),.t.,",")
  FOR EACH lcLine IN laClassLibs
    IF FULLPATH(m.lcClassLib) $ m.lcLine
      *  do nothing, ignoring the just-set CLASSLIB      
      lcClassLib = m.lcLine
     ELSE 
      lcString = m.lcString + m.lcLine + ", "
    ENDIF
  ENDFOR
  lcString = ALLTRIM(m.lcString)
  IF RIGHTC(m.lcString,1) = ","
    lcString = LEFTC(m.lcString,LENC(m.lcString)-1)
  ENDIF
  lcString = lcClassLib + ", " + m.lcString
  TRY
    *  I expect this to always succeed...
    SET CLASSLIB TO &lcString
  CATCH
    llSuccess = .f.
  ENDTRY
ENDIF

IF NOT m.llSuccess
  *
  *  when/if the SET CLASSLIB fails, it is
  *  set to blank, clearing out whatever was
  *  already in it, so at least set it to what
  *  it was when this routine was called
  *
  TRY
  SET CLASSLIB TO &lcCurrentSetClassLib
  CATCH
  llSuccess = .f.
  ENDTRY
ENDIF

RETURN m.llSuccess
