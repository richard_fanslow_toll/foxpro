*
*  Included with VMP for backward-compatibility --
*  we recommend that you use X8ShellExecute.PRG instead.
* 
*
*  X7ShellExecute.PRG
*  This routine is based on the utility that Rick Strahl 
*  published in the March 1998 FoxPro Advisor Magazine.  
*  It is a wrapper to the ShellExecute() Windows API 
*  function, loading the passed URL/FileName in the default
*  application as specified by the file extension
*  associations established for the current machine.
*  For example, if the passed tcURLorFilename is:
*   - A URL, fires up your default web browser and
*     takes you to that web site.  If you are not
*     currently connected, your dialer will kick
*     in first.
*   - A Word.Doc document, fires up Word with the
*     passed document loaded (pass the full path
*     and filename).
*   - A .BMP graphic, fires up your graphics editor
*     to which .BMP files are associated, with that
*     .BMP loaded.
*   - A .CHM (Compiled HTML) file, fires up the .CHM
*     interface with the passed .CHM file loaded
*   - etc.
*
*
*  Returns:
*    2- Bad association (invalid URL, invalid filename, etc.)
*   29- Failure to load application
*   30- Application is busy
*   31- No application association for the passed tcURLorFilename
*   33 or higher - Success, and the return is an instance handle
*                  for the started application
*
*
*  this routine is very flexible in that it calls any
*  file/association currently setup in Windows -- for
*  a different approach for files associated with your
*  browser (.HTM, .HTML, .XML, etc. files) with a bit 
*  more control, try something like this:
*!*	LOCAL loIE as "InternetExplorer.Application"
*!*	loIE = CREATEOBJECT("InternetExplorer.Application")
*!*	loIE.Navigate(FULLPATH(tcXMLFile))  
*!*	loIE.Visible = .t.
*!*	*loIE.Quit()
*  NOTE that when the loIE variable in the above 
*    code goes out of scope, Internet Explorer closes,
*    so you'd have to instead store it to a global
*    memvar/property/_Screen.Property if you want it
*    to stay around
*  NOTE that if the target file is on the Internet,
*    you will probabaly want/need to insert this line
*    after the loIE.Navigate() call:
*      X5WebPageNavComplete(loIE)
*
*
*
*  lParameters
*    tcURLorFilename (R) URL or filename to be passed to
*                          the ShellExecute() API function.
*                        When passed as a filename, the
*                          ShellExecute() function requires
*                          the full path -- if
*                          the filename is currently in
*                          the VFP path, you can pass it as
*                            FULLPATH(<FileName>)
*                          OR, if the filename is in the 
*                            VFP path, don't have to include
*                            the FULLPATH() because this 
*                            routine adds it.  
*                          If the filename is NOT in the 
*                            current VFP path, the filename
*                            MUST include the full path.
*           tcAction (O) Action parameter passed to the 
*                          ShellExecute() API function.
*                        Defaults to "Open".
* tcWorkingDirectory (O) Working directory passed to the
*                          ShellExecute() API function.
*                        Defaults to SYS(2023).
*       tcParameters (O) Parameters passed to the 
*                          ShellExecute API function.
*                        Defaults to SPACE(0).
*
LPARAMETERS tcURLorFilename as String, ;
            tcAction as String, ;
            tcWorkingDirectory as String, ;
            tcParameters as String

LOCAL lcURLorFileName, ;
      lcAction, ;
      lcWorkingDirectory, ;
      lcParameters
      
IF VARTYPE(m.tcURLorFilename) = "C" ;
     AND NOT EMPTY(m.tcURLorFilename)
  lcURLorFilename = ALLTRIM(m.tcURLorFilename)
  IF FILE(m.lcURLorFileName)
    lcURLorFileName = FULLPATH(m.lcURLorFileName)
  ENDIF
 ELSE
  lcURLorFileName = "http://www.visionpace.com"
ENDIF     

IF X6ISCOM()
  *  no UI for a COM server
 ELSE
  *  UI is OK for non-COM server code
  WAIT WINDOW "Loading " + m.lcURLorFilename + " ..." NOWAIT
ENDIF

IF VARTYPE(m.tcAction) = "C" AND NOT EMPTY(m.tcAction)
  lcAction = m.tcAction
 ELSE
  lcAction = "Open"
ENDIF

IF VARTYPE(m.tcWorkingDirectory) = "C" AND NOT EMPTY(m.tcWorkingDirectory)
  lcWorkingDirectory = m.tcWorkingDirectory
 ELSE
  lcWorkingDirectory = SYS(2023)
ENDIF

IF VARTYPE(m.tcParameters) = "C" AND NOT EMPTY(m.tcParameters)
  lcParameters = m.tcParameters
 ELSE
  lcParameters = SPACE(0)
ENDIF

IF NOT X7ISAPIF("ShellExecute")
  DECLARE INTEGER ShellExecute IN SHELL32.DLL ;
       INTEGER nWinHandle,;
       STRING cOperation,;
       STRING cFileName,;
       STRING cParameters,;
       STRING cWorkingDirectory,;
       INTEGER nShowWindow
ENDIF

*
*  no need for FindWindow() any more, since VFP 7.0 exposes
*  the HWnd property for _VFP, _Screen, and Form base class
*

*!*	IF NOT X7ISAPIF("FindFindow")
*!*	  DECLARE INTEGER FindWindow IN WIN32API ;
*!*	       STRING cNull, ;
*!*	       STRING cWinName
*!*	ENDIF

*!*	RETURN ShellExecute(FindWindow(0,_Screen.Caption), ;
*!*	                    lcAction, ;
*!*	                    lcURLorFilename, ;
*!*	                    lcParameters, ;
*!*	                    lcWorkingDirectory,1)

RETURN ShellExecute(_Screen.hWnd, ;
                    m.lcAction, ;
                    m.lcURLorFilename, ;
                    m.lcParameters, ;
                    m.lcWorkingDirectory,1)
