*
*  X7DISPTH.PRG
*  RETURN a string potentially modified by this wrapper
*  to the DISPLAYPATH() function, respecting a number
*  of PIXELS rather than a number of CHARACTERS.
*  Please note that, while the native VFP DISPLAYPATH()
*  function returns the string in all lower case, this
*  utility returns the string in all upper case.
*
*  This utility requires VFP 7.0 or higher, when the
*  DISPLAYPATH() function was added to VFP.
*
*  Copyright (c) 1997-2005 Visionpace   All Rights Reserved
*                17501 East 40 Hwy., Suite 218
*                Independence, MO 64055
*                816-350-7900 
*                http://www.visionpace.com
*                http://vmpdiscussion.visionpace.com
*  Author:  Drew Speedie
*  
*  Examples:
*    ? X7DISPTH("C:\Program Files\SomeFolder\SomeOtherFolderVMP\SomeFile.TXT",300,"Arial",9)
*    ? X7DISPTH("C:\Program Files\SomeFolder\SomeOtherFolderVMP\SomeFile.TXT",400,"Arial",9)
*    XXDTES("XXFWCOMP.VCX","X7DISPTH(","ctrGetFile","cmdGetFile1.OnChange")
*
*
*  lParameters 
*     tcString (R) The string to be passed to 
*                    DISPLAYPATH() and fit to the
*                    available Width of toControl
*    tuCtrlPix (R) The control that will display the
*                    DISPLAYPATH() output
*                      OR
*                  The maximum number of pixels for
*                    tcString, as an integer
*   tcFontName (O) REQUIRED when tuCtrlPix is passed as
*                    an integer, ignored when tuCtrlPix
*                    is passed as a control object
*   tnFontSize (O) REQUIRED when tuCtrlPix is passed as
*                    an integer, ignored when tuCtrlPix
*                    is passed as a control object
*   tlFontBold (O) OPTIONAL when tuCtrlPix is passed as
*                    an integer, ignored when tuCtrlPix
*                    is passed as a control object
* tlFontItalic (O) OPTIONAL when tuCtrlPix is passed as
*                    an integer, ignored when tuCtrlPix
*                    is passed as a control object
*

LPARAMETERS tcString, tuCtrlPix, ;
            tcFontName, tnFontSize, tlFontBold, tlFontItalic

ASSERT VARTYPE(m.tcString) = "C" ;
     MESSAGE "tcString parameter has been passed incorrectly"

IF EMPTY(m.tcString)
  RETURN SPACE(0)
ENDIF

ASSERT (VARTYPE(m.tuCtrlPix) = "O" AND VARTYPE(tuCtrlPix.FontName) = "C") ;
     OR ;
       (VARTYPE(m.tuCtrlPix) = "N" AND m.tuCtrlPix>20) ;
     MESSAGE "tuCtrlPix is of the wrong data type, is an object that does not have a FontName property, or is an integer value less than 20"

LOCAL lnMaxWidth, lnDisplayChars, lcFontStyle, ;
      lcFontName, lnFontSize
IF VARTYPE(m.tuCtrlPix) = "O"
  *
  *  based on what kind of control tuCtrlPix is, 
  *  get the average number of characters it can
  *  currently display, respecting its font properties  
  *
  DO CASE
    CASE UPPER(tuCtrlPix.BaseClass) = "LABEL"
      lnMaxWidth = tuCtrlPix.Width
    CASE UPPER(tuCtrlPix.BaseClass) = "TEXTBOX"
      lnMaxWidth = tuCtrlPix.Width - (tuCtrlPix.Margin*2)
    CASE PEMSTATUS(tuCtrlPix,"Width",5)
      lnMaxWidth = tuCtrlPix.Width
    OTHERWISE
      ASSERT .f. message "tuCtrlPix does not have a Width property"
  ENDCASE
  lcFontStyle = SPACE(0)
  DO CASE
    CASE tuCtrlPix.FontBold AND tuCtrlPix.FontItalic
      lcFontStyle = "BI"
    CASE tuCtrlPix.FontBold
      lcFontStyle = "B"
    CASE tuCtrlPix.FontItalic
      lcFontStyle = "B"
    OTHERWISE
      lcFontStyle = "N"
  ENDCASE
  lcFontName = tuCtrlPix.FontName
  lnFontSize = tuCtrlPix.FontSize
 ELSE
  *
  *  tuCtrlPix passed as an integer
  *
  lnMaxWidth = INT(m.tuCtrlPix)
  lcFontStyle = SPACE(0)
  DO CASE
    CASE VARTYPE(m.tlFontBold) = "L" AND m.tlFontBold ;
         AND VARTYPE(m.tlFontItalic) = "L" AND m.tlFontItalic
      lcFontStyle = "BI"
    CASE VARTYPE(m.tlFontBold) = "L" AND m.tlFontBold
      lcFontStyle = "B"
    CASE VARTYPE(m.tlFontItalic) = "L" AND m.tlFontItalic
      lcFontStyle = "B"
    OTHERWISE
      lcFontStyle = "N"
  ENDCASE
  lcFontName = m.tcFontName
  lnFontSize = m.tnFontSize
ENDIF                                      

lnDisplayChars = m.lnMaxWidth/FONTMETRIC(6, ;
                                         m.lcFontName, ;
                                         m.lnFontSize, ;
                                         m.lcFontStyle)
lnDisplayChars = INT(m.lnDisplayChars)   
*
*  DISPLAYPATH() won't accept the 2nd parameter as 
*  an integer value > 260
*                                    
lnDisplayChars = MIN(260,m.lnDisplayChars)

LOCAL loForm
loForm = CREATEOBJECT("Form")
loForm.FontName = m.lcFontName
loForm.FontSize = m.lnFontSize
loForm.FontBold = "B" $ m.lcFontStyle
loForm.FontItalic = "I" $ m.lcFontStyle

*
*  get the DISPLAYPATH() for the passed tcString, to
*  the average number of displayed characters
*
LOCAL lcDisplayPath
lcDisplayPath = DISPLAYPATH(m.tcString,m.lnDisplayChars)

*
*  if the actual width of the above DISPLAYPATH() 
*  output is less than the available width, increase
*  the size of the DISPLAYWIDTH() by increasing the
*  number of characters passed to its 2nd parameter
*
IF loForm.TextWidth(m.lcDisplayPath) < m.lnMaxWidth
  DO WHILE loForm.TextWidth(m.lcDisplayPath) < m.lnMaxWidth
    lnDisplayChars = m.lnDisplayChars + 1
    IF m.lnDisplayChars > LENC(m.tcString)
      *
      *  2nd DISPLAYPATH() parameter may not be 
      *  greater than the number of characters
      *  in the 1st DISPLAYPATH() parameter
      *
      EXIT
    ENDIF
    lcDisplayPath = DISPLAYPATH(m.tcString,m.lnDisplayChars)
  ENDDO
ENDIF

*
*  if the actual width of the above DISPLAYPATH() 
*  output is greater than the available width, decrease
*  the size of the DISPLAYWIDTH() by decreasing the
*  number of characters passed to its 2nd parameter
*  ...note that the above IF/DO WHILE block sets
*  lnDisplayChars to a value that is 1/one greater
*  than the available width, and this logic will kick
*  in to reduce lnDisplayChars by 1/one...
*
IF loForm.TextWidth(m.lcDisplayPath) > m.lnMaxWidth
  DO WHILE loForm.TextWidth(m.lcDisplayPath) > m.lnMaxWidth
    lnDisplayChars = m.lnDisplayChars - 1
    IF m.lnDisplayChars < 10
      *
      *  10 is the minimum for the 2nd DISPLAYPATH() 
      *  parameter
      *
      EXIT
    ENDIF
    lcDisplayPath = DISPLAYPATH(m.tcString,m.lnDisplayChars)
  ENDDO
ENDIF

loForm = .NULL.

RETURN UPPER(m.lcDisplayPath)


