*
*  X3ED.PRG
*  Encrypt/Decrypt text
*
*  Copyright (c) 1997-2005 Visionpace   All Rights Reserved
*                17501 East 40 Hwy., Suite 218
*                Independence, MO 64055
*                816-350-7900 
*                http://www.visionpace.com
*                http://vmpdiscussion.visionpace.com
*  Author:  Drew Speedie
*
*  TYPICAL USAGE:
*    replace Table.Field with x3ed("E",m.somevalue)
*    lcString = X3ED("D",Table.Field)
*    browse fields ... calc1=X3ED("D",Usr_Password):H="Password" ...
*
*  To view passwords unencrypted at the Command Window:
*  SELECT Usr_PK, Usr_LanID, X3ED("D",Usr_Password) as Password FROM Users
* 
*
*  NOTE:  since this routine is used for encryption/decryption,
*         you may want to distribute it separately from the
*         rest of your application, as a standalone X3ED.FXP 
*         or X3ED.APP which has been protected against 
*         reverse-engineering (i.e., via tool like "ReFox") 
*
*
*  NOTE:  X3ED is around 30% faster if memvar gcAscii
*         is available.  If gcAscii does not already
*         exist, it will be initialized here and thus be
*         available for subsequent calls to X3ED
*
*
*  PARAMETERS:
*    tcAction = "E"ncrypt/"D"ecrypt
*    tcString = character string to be Encrypted/Decrypted
*
*
PARAMETERS tcAction, tcString

LOCAL lnLen, ;
      lcString, ;
      lnXX, ;
      lnCharPos, ;
      lnAscChar, ;
      lcString2, ;
      lcRetVal, ;
      lcAction
IF TYPE("m.gcAscii") = "U" ;
     OR NOT LEFTC(m.gcAscii,1) = CHR(255) ;
     OR NOT RIGHTC(m.gcAscii,1) = CHR(0)
  PUBLIC gcAscii
  gcAscii = SPACE(0)
  FOR lnXX = 255 TO 0 STEP -1
    gcAscii = m.gcAscii + CHR(m.lnXX)
  ENDFOR
ENDIF
*!*	IF type("gcAscii") = "U" ;
*!*	     OR leftc(gcAscii,1) # chr(128) ;
*!*	     OR rightc(gcAscii,1) # chr(34)
*!*	  public gcAscii
*!*	  gcAscii = space(0)
*!*	  FOR lnXX = 128 to 34 step -1
*!*	    gcAscii = gcAscii + chr(lnXX)
*!*	  ENDFOR
*!*	ENDIF

lnLen = LENC(m.tcString)

lcAction = UPPER(ALLTRIM(LEFTC(m.tcAction,1)))
IF NOT m.lcAction = "E" AND NOT m.lcAction = "D"
  RETURN .NULL.   &&& this is to get your attention if you don't pass tcAction properly
ENDIF

IF m.lcAction = "E"   &&& encrypt
  lcString = SPACE(0)
  FOR lnXX = 1 TO lnLen
    lnCharPos = m.lnLen-m.lnXX+1
    lnAscChar = ASC(SUBSTRC(m.tcString,m.lnCharPos,1))+IIF(MOD(m.lnXX,2)=0,1,2)
    * if you'd like it a little more scrambled, use this variation
    * and pay a .01/per penalty:
    *lnAscChar = asc(substrc(tcString,lnCharPos,1)) + ;
         IIF(mod(lnXX,7)=0,2,IIF(mod(lnXX,4)=0,1,-1))
    lcString = m.lcString + CHR(m.lnAscChar)
  ENDFOR
  RETURN SYS(15,m.gcAscii,m.lcString)
 ELSE  &&& lcAction = "D" decrypt
  lcString = SYS(15,m.gcAscii,m.tcString)
  STORE SPACE(0) TO lcString2, lcRetVal
  FOR lnXX = 1 TO lnLen
    lcString2 = m.lcString2 + CHR(ASC(SUBSTRC(m.lcString,m.lnXX,1)) - IIF(MOD(m.lnXX,2)=0,1,2))
    * if you'd like it a little more scrambled, use this variation
    * and pay a .01/per penalty:
    *lcString2 = lcString2 + chr(asc(substrc(lcString,lnXX,1)) - ;
         IIF(mod(lnXX,7)=0,2,IIF(mod(lnXX,4)=0,1,-1)))
  ENDFOR
  FOR lnXX = 1 to lnLen
    lcRetVal = m.lcRetVal + SUBSTRC(m.lcString2,m.lnLen-m.lnXX+1,1)
  ENDFOR
  RETURN m.lcRetVal
ENDIF  



PROCEDURE keywords
* ENCRYPTION|MEMO FIELDS
RETURN 



*!*	    IF between(lnXX,127,160) OR lnXX = 175
*!*	      *
*!*	      *  skip these characters, since they include
*!*	      *  non-printable box characters for most fonts,
*!*	      *  which characters display as boxes even when
*!*	      *  a textbox control uses the PasswordChar
*!*	      *  property
*!*	      *
*!*	      loop
*!*	    ENDIF
