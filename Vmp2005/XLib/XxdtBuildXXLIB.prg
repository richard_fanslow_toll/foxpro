*
*  XXDTBuildXXLIB.PRG
*  Create an app-specific XXLIB.EXE
*  for use when distributing a VMP app in separate .EXEs
*  (or .APPs if you prefer):
*  - ??.EXE - app-specific .EXE
*  - XXLIB.EXE - VMP framework code  (or XXLIB.APP, if you prefer)
*  - ??LIB.EXE - app-specific "module" executable  (or ??LIB.APP, if you prefer)
*  The resulting XXLIB.EXE contains the VMP framework source 
*  code.
*
*  Copyright (c) 2002-2005 Visionpace   All Rights Reserved
*                17501 East 40 Hwy., Suite 218
*                Independence, MO 64055
*                816-350-7900 
*                http://www.visionpace.com
*                http://vmpdiscussion.visionpace.com
*  Author:  Drew Speedie
*
*  For more detailed information, see the 
*    Distributing a single app in separate .EXE/.APP pieces
*  help topic in the Reference Guide section of the 
*  VMPHELP.CHM online help file.  
*  _VFP.Help(FULLPATH("VMPHELP.CHM"),,"Distributing a single app in separate .EXE/.APP pieces")
*
*  See also XXDTBuildAppLIB.PRG
*    MODIFY COMMAND XXDTBuildAppLIB
*
*  The output of this utility is a single XXLIB.EXE --
*  if you want to review the working files created here 
*  to generate that XXLIB.EXE, pass the tlDontErase 
*  parameter as .T.
*
*  The resulting XXLIB.EXE includes an EraseFilesFromDisk 
*  local procedure that can optionally be called 
*  explicitly on app exit, as is demonstrated in
*  VMMAIN.PRG:
*         XXDTES("VMMAIN.PRG","EraseFilesFromDisk")
*
*  Note that XXLIB.EXE is created in whatever folder this
*  XXDTBuildXXLIB.PRG utility is executed, rather than in 
*  the \XLIB folder -- this way, you can have an 
*  app-specific XXLIB.EXE for each project/app, and can
*  therefore customize it on a per app/project basis.
*
*  You can, of course, copy this utility to a new file
*  and modify it as you see fit.
*
*  lParameters
*  tcXXLIBFileName (O) Specify the filename of the XXLIB.EXE
*                        generated as the output of this
*                        utility -- name it anything you 
*                        want, as long as you pass a filename
*                        without an extension (.EXE is the
*                        default), or as a filename with an
*                        an ".EXE" or ".APP" extension.  
*                      Defaults to the recommended "XXLIB.EXE"
*                      If passed as a string without an 
*                        extension, ".EXE" is the assumed 
*                        extension.
*                      If passed as a string with an 
*                        extension, the extension must be 
*                        either ".EXE" or ".APP".
*
*   tlDebugInfo (O) Pass this parameter as .T. to set the
*                     Debug property of the XXLIB.PJX to
*                     .T. (same as checking the Debug
*                     info checkbox of the Project tab
*                     on the Project Info dialog)
*                   Defaults to .F. -- same effect as
*                     checking/selecting the Debug info
*                     checkbox of the Project tab on the
*                     Project Info dialog, yielding a
*                     smaller resulting XXLIB.EXE.
*
*   tlNoEncrypt (O) Pass this parameter as .T. to set the
*                     Encrypted property of the XXLIB.PJX
*                     to .F. (same as UN-checking the
*                     Encrypted checkbox of the Project tab
*                     on the Project Info dialog) 
*                   Defaults to .F. -- same effect as
*                     checking/selecting the Encrypted
*                     checkbox of the Project tab on the
*                     Project Info dialog, resulting in a
*                     more secure XXLIB.EXE.
*
*    tcIconFile (O) Pass the name of an .ICO file to be used
*                     for tcXXLIBFileName -- this is the 
*                     file specified on the Project tab of
*                     the Project Info dialog.
*                   Defaults to the "VMPLIB.ICO" file that
*                     ships with VMP.
*                   This property is IGNORED when tcXXLIBFileName
*                     indicates an .APP rather than an .EXE,
*                     because VFP only attaches an .ICO file
*                     to an .EXE, not an .APP.
*
*    tlModiProj (O) Pass this parameter as .T. to pause
*                     the action of this utility in the
*                     Project Manager, just before building 
*                     tcXXLIBFileName, so you can review the
*                     project and/or make changes.
*                   Defaults to .F. -- the Project Manager
*                     is not summoned prior to BUILDing
*                     tcXXLIBFileName.
*
*    tlDontErase (O) Pass this parameter as .T. to leave
*                      the working files generated here
*                      around once this utility finishes:
*                        XXLIB.PRG
*                        XXLIB.PJX
*                        XXLIB.DBF
*                    Passing this parameter as .T. also
*                      results in the above working files
*                      opened in the corresponding editor
*                      once this utility finishes successfully.
*                    Defaults to .F. -- the above working
*                      files are erased, leaving just
*                      XXLIB.EXE as the output of this
*                      utility.
*
*      tlNoNTier (O) Pass this parameter as .T. to prevent
*                      the VMP n-Tier services classes
*                      from being loaded into the XXLIB.EXE
*                      -- ONLY when you are ABSOLUTELY
*                      POSITIVE you don't need n-Tier 
*                      services in your application and
*                      you want to reduce the size of
*                      XXLIB.EXE accordingly.
*
*   tcCopyToPath (O) Specify a full or relative path to
*                      be prepended onto the names of the
*                      files that are copied to disk when
*                      XXLIB.EXE executes from the ??MAIN 
*                      program/??.EXE.  If you specify a 
*                      relative path, that path is relative 
*                      to the folder where the main ??.EXE 
*                      executes, since that main executable
*                      calls tcXXLIBFileName (XXLIB.EXE)
*                      on app startup.
*                    NOTE that the XXLIB.EXE generated here 
*                      ALSO accepts a parameter indicating 
*                      the folder to which to copy the files 
*                      -- you have the choice to specify 
*                      tcCopyToPath here, or pass it to 
*                      ??LIB.EXE at runtime.  You can even 
*                      do both -- any parameter passed to 
*                      ??LIB.EXE takes precedence.
*                    Defaults to SPACE(0), whereupon when 
*                      tcXXLIBFileName copies files to disk 
*                      at runtime, those files are copied to 
*                      the same folder where the main ??.EXE 
*                      is executing.
*                    See also this same parameter of
*                      XXDTBuildAppLib.PRG
*                        XXDTES("XXDTBuildAppLib.PRG","tcCopyToPath")
*
* tcOverWriteExpr (O) Specify an expression that is
*                       EVALUATE()d in the XXLIB.EXE
*                       generated here, which expression
*                       yields a logical value indicating
*                       what to do when files that get 
*                       copied to disk already exist - .T. 
*                       (the default) indicates that they 
*                       are overwritten, .F. indicates that 
*                       no copying takes place.
*                     This string can be any expression
*                       that can be EVALUATE()d at runtime
*                       by XXLIB.EXE, not just limited to
*                       a simple ".T."/".F.", so you can 
*                       specify a function/procedure/.PRG
*                       that is available when XXLIB.EXE
*                       is executed at runtime.
*                     NOTE that the XXLIB.EXE generated here 
*                       ALSO accepts a tcOverWriteExpr 
*                       parameter -- you have the choice to 
*                       specify tcOverWriteExpr here, or 
*                       pass it to XXLIB.EXE at runtime.  
*                       You can even do both -- any parameter
*                       passed to XXLIB.EXE takes precedence.
*                    Defaults to ".T." 
*                    See also this same parameter of
*                      XXDTBuildAppLIB.PRG
*                        XXDTES("XXDTBuildAppLIB.PRG","tcOverWriteExpr")
*
LPARAMETERS tcXXLIBFileName, ;
            tlDebugInfo, ;
            tlNoEncrypt, ;
            tcIconFile, ;
            tlModiProj, ;
            tlDontErase, ;
            tlNoNTier, ;
            tcCopyToPath, ;
            tcOverWriteExpr

IF X2PSTACK("XXDT.FXP")
  MESSAGEBOX(JUSTFNAME(PROGRAM()) + ;
             " cannot be run directly from this XXDT utility." + CHR(13) + CHR(13) + ;
             "DO " + JUSTFNAME(PROGRAM()) + CHR(13) + CHR(13) + ;
             "is about to be entered into the Command Window, so you can " + ;
             "run " + JUSTFNAME(PROGRAM()) + " from there.", ;
             48,"Please Note")
  KEYBOARD "DO " + JUSTFNAME(PROGRAM()) PLAIN CLEAR        
  RETURN 
ENDIF

SET ASSERTS ON 

IF VARTYPE(tcXXLIBFileName) = "C" ;
     AND NOT EMPTY(tcXXLIBFileName) ;
     AND OCCURS(".",tcXXLIBFileName) = 1
  IF NOT INLIST(UPPER(JUSTEXT(tcXXLIBFileName)),"EXE","APP")
    ASSERT .f. ;
         MESSAGE "If tcXLIBFileName is passed with an extension, that extension must be either .APP or .EXE"
    RETURN 
  ENDIF
ENDIF

IF NOT VARTYPE(tlNoNTier) = "L"
  ASSERT .f. ;
       MESSAGE "If tlNoNTier is passed, it must be a logical value"
  RETURN 
ENDIF

*
*  preserve the parameters past the upcoming 
*  CLEAR ALL, CLOSE ALL
*
CREATE CURSOR XXDTBuildXXLib (XXLIBFile C(20), ;
                              DebugInfo C(1), ;
                              NoEncrypt C(1), ;
                              IconFile C(240), ;
                              ModiProj C(1), ;
                              DontErase C(1), ;
                              NoNTier C(1), ;
                              CopyToPath C(240), ;
                              OWExpr C(240))
INSERT INTO XXDTBuildXXLib (XXLIBFile, ;
                            DebugInfo,; 
                            NoEncrypt, ;
                            IconFile, ;
                            ModiProj, ;
                            DontErase, ;
                            NoNTier, ;
                            CopyToPath, ;
                            OWExpr) ;
     VALUES ;
     (IIF(VARTYPE(tcXXLIBFileName) = "C" AND NOT EMPTY(tcXXLIBFileName),tcXXLIBFileName,SPACE(0)), ;
      IIF(VARTYPE(tlDebugInfo) = "L", IIF(tlDebugInfo,"Y","N"), "N"), ;
      IIF(VARTYPE(tlNoEncrypt) = "L", IIF(tlNoEncrypt,"Y","N"), "N"), ;
      IIF(VARTYPE(tcIconFile) = "C", tcIconFile, SPACE(0)), ;
      IIF(VARTYPE(tlModiProj) = "L", IIF(tlModiProj,"Y","N"), "N"), ;
      IIF(VARTYPE(tlDontErase) = "L", IIF(tlDontErase,"Y","N"), "N"), ;
      IIF(VARTYPE(tlNoNTier) = "L", IIF(tlNoNTier,"Y","N"), "N"), ;
      IIF(VARTYPE(tcCopyToPath) = "C" AND NOT EMPTY(tcCopyToPath),tcCopyToPath,SPACE(0)), ;
      IIF(VARTYPE(tcOverWriteExpr) = "C" AND NOT EMPTY(tcOverWriteExpr),tcOverWriteExpr,SPACE(0)))
CURSORTOXML("XXDTBuildXXLib","XXDTBuildXXLib.XML",1,512,0,"")

CLEAR ALL
CLOSE ALL

*
*  get the parameters back
*
LOCAL lcXXLIBFileName, lcCopyToPath, llSecurity, llNTier, ;
      lcOverWriteExpr, llDontErase, llDebugInfo, llNoEncrypt, ;
      lcIconFile, llModiProj
XMLTOCURSOR("XXDTBuildXXLib.XML","XXDTBuildXXLib",512)
lcXXLIBFileName = UPPER(ALLTRIM(XXLIBFile))
llDebugInfo = UPPER(ALLTRIM(DebugInfo)) = "Y"
llNoEncrypt = UPPER(ALLTRIM(NoEncrypt)) = "Y"
lcIconFile = UPPER(ALLTRIM(IconFile))
llModiProj = UPPER(ALLTRIM(ModiProj)) = "Y"
llDontErase = UPPER(ALLTRIM(DontErase)) = "Y"
llNTier = UPPER(ALLTRIM(NoNTier)) = "N"
lcCopyToPath = UPPER(ALLTRIM(CopyToPath))
lcOverWriteExpr = ALLTRIM(OWExpr)
USE IN XXDTBuildXXLib
ERASE XXDTBuildXXLib.XML

IF EMPTY(lcXXLIBFileName)
  lcXXLIBFileName = "XXLIB.EXE"
ENDIF
IF EMPTY(JUSTEXT(lcXXLIBFileName))
  lcXXLIBFileName = lcXXLIBFileName + ".EXE"
ENDIF

lcCopyToPath = UPPER(ALLTRIM(lcCopyToPath))
IF NOT EMPTY(lcCopyToPath)
  lcCopyToPath = ADDBS(lcCopyToPath)
ENDIF

IF EMPTY(lcOverWriteExpr)
  lcOverWriteExpr = ".T."
ENDIF

IF JUSTEXT(lcXXLIBFileName) = "EXE"
  IF EMPTY(lcIconFile) AND FILE("VMPLIB.ICO")
    lcIconFile = FULLPATH("VMPLIB.ICO")
  ENDIF
 ELSE
  *
  *  VFP doesn't support the Icon property for an .APP
  *
  lcIconFile = SPACE(0)
ENDIF

*
*  erase any existing files that are generated here
*
ERASE XXLIB.PRG
ERASE XXLIB.APP
ERASE XXLIB.EXE
ERASE XXLIB.DBF
ERASE XXLIB.FPT
ERASE XXLIB.PJX
ERASE XXLIB.PJT

LOCAL lcText1, lcText2, lcText3 
STORE SPACE(0) TO lcText1, lcText2, lcText3 

SET TEXTMERGE ON NOSHOW 
TEXT TO lcText1 NOSHOW 
*  
*  XXLIB.PRG
*  Note that XXLIB.PRG is created in whatever folder this
*  XXDTBuildXXLIB.PRG is executed, rather than in the
*  \XLIB folder itself -- this way, you can have an 
*  app-specific XXLIB.EXE for each project/app, and
*  can therefore remove some of the optional stuff on
*  a per app/project basis
*
LPARAMETERS tcCopyToPath, tcOverWriteExpr

SET TALK OFF 

LOCAL lcCopyToPath
IF VARTYPE(tcCopyPath) = "C" AND NOT EMPTY(tcCopyToPath)
  lcCopyToPath = ADDBS(UPPER(ALLTRIM(tcCopyToPath)))
 ELSE
  lcCopyToPath = "<<lcCopyToPath>>"
ENDIF
IF EMPTY(lcCopyToPath)
  lcCopyToPath = ADDBS(FULLPATH(CURDIR()))
ENDIF

IF VARTYPE(tcOverWriteExpr) = "C" AND NOT EMPTY(tcOverWriteExpr)
  lcOverWriteExpr = ALLTRIM(tcOverWriteExpr)
 ELSE
  lcOverWriteExpr = "<<lcOverWriteExpr>>"
ENDIF
IF EMPTY(lcOverWriteExpr)
  lcOverWriteExpr = ".T."
ENDIF

*
*  SET PROCEDURE TO this XXLIB.EXE (or XXLIB.APP)
*
LOCAL lcProc, xx
xx = 0
DO WHILE .t.
  lcProc = SYS(16,xx)
  IF EMPTY(lcProc)
    EXIT
  ENDIF
  IF UPPER(JUSTSTEM(JUSTFNAME(lcProc))) == UPPER(PROGRAM())
    SET PROCEDURE TO (lcProc) ADDITIVE 
    EXIT
  ENDIF
  xx = xx + 1
ENDDO  

USE XXLIB IN 0    &&& built into XXLIB.EXE

*
*  SET CLASSLIB TO the absolute minimum-required
*  VMP class libraries
*
EXTERNAL CLASS XXFW.VCX
EXTERNAL CLASS XXFWLIBS.VCX
EXTERNAL CLASS XXFWUTIL.VCX
IF NOT X8SETCLS("XXFW.VCX")
  USE IN XXLIB
  RETURN .f.
ENDIF
IF NOT X8SETCLS("XXFWLIBS.VCX")
  USE IN XXLIB
  RETURN .f.
ENDIF
IF NOT X8SETCLS("XXFWUTIL.VCX")
  USE IN XXLIB
  RETURN .f.
ENDIF

*
*  SET CLASSLIB TO the other VMP class libraries
*  typically used by all VMP desktop apps
*
EXTERNAL CLASS XXFWCal.VCX
IF NOT X8SETCLS("XXFWCal.VCX")
  USE IN XXLIB
  RETURN .f.
ENDIF
EXTERNAL CLASS XXFWControlBehaviors.VCX
IF NOT X8SETCLS("XXFWControlBehaviors.VCX")
  USE IN XXLIB
  RETURN .f.
ENDIF
EXTERNAL CLASS XXFWCOMP.VCX
IF NOT X8SETCLS("XXFWCOMP.VCX")
  USE IN XXLIB
  RETURN .f.
ENDIF
EXTERNAL CLASS XXFWCTRL.VCX
IF NOT X8SETCLS("XXFWCTRL.VCX")
  USE IN XXLIB
  RETURN .f.
ENDIF
EXTERNAL CLASS XXFWED.VCX
IF NOT X8SETCLS("XXFWED.VCX")
  USE IN XXLIB
  RETURN .f.
ENDIF
EXTERNAL CLASS _Crypt.VCX 
IF NOT X8SETCLS("_Crypt.VCX")
  USE IN XXLIB
  RETURN .f.
ENDIF
EXTERNAL CLASS XXFWFRM.VCX
IF NOT X8SETCLS("XXFWFRM.VCX")
  USE IN XXLIB
  RETURN .f.
ENDIF
EXTERNAL CLASS XXFWGRD.VCX
IF NOT X8SETCLS("XXFWGRD.VCX") 
  USE IN XXLIB
  RETURN .f.
ENDIF
EXTERNAL CLASS XXFWMISC.VCX
IF NOT X8SETCLS("XXFWMISC.VCX")
  USE IN XXLIB
  RETURN .f.
ENDIF
EXTERNAL CLASS XXFWPICK.VCX
IF NOT X8SETCLS("XXFWPICK.VCX")
  USE IN XXLIB
  RETURN .f.
ENDIF

*
*  and this one is just in case
*
EXTERNAL CLASS XXFWBC02.VCX
IF NOT X8SETCLS("XXFWBC02.VCX")
  USE IN XXLIB
  RETURN .f.
ENDIF

*
*  add other VMP class libraries you may or may 
*  not need (you can create your own version of 
*  this utility, or run this utility and then 
*  remove unwanted files before creating XXLIB.EXE)
*
EXTERNAL CLASS XXFWAppLoader.VCX
IF NOT X8SETCLS("XXFWAppLoader.VCX")
  USE IN XXLIB
  RETURN .f.
ENDIF
EXTERNAL CLASS XXFWIUDLog.VCX
IF NOT X8SETCLS("XXFWIUDLog.VCX")
  USE IN XXLIB
  RETURN .f.
ENDIF
EXTERNAL CLASS XXFWNTSystem.VCX
IF NOT X8SETCLS("XXFWNTSystem.VCX")
  USE IN XXLIB
  RETURN .f.
ENDIF
EXTERNAL CLASS XXFWWizards.VCX
IF NOT X8SETCLS("XXFWWizards.VCX")
  USE IN XXLIB
  RETURN .f.
ENDIF



*
*  SET CLASSLIB TO XXFWSEC.VCX, for VMP apps that
*  use/install the optional VMP user security system,
*  and ensure that the default VMP reports for the optional
*  VMP user security system are copied to disk 
*
*  note that we haven't provided a tlNoSecurity 
*  parameter to prevent the following, to suppress
*  the inclusion of the XXFWSEC.VCX class library,
*  becuase it will be included in the project 
*  anyway due to the fact that 
*  XXFWFRM.VCX/frmChangePassword.txtNewPassword 
*  inherits from a class in XXFWSEC.VCX
*
EXTERNAL CLASS XXFWSEC.VCX
IF NOT X8SETCLS("XXFWSEC.VCX")
  USE IN XXLIB
  RETURN .f.
ENDIF
IF NOT CopyFileToDisk("XXUSYS1.FRX",lcCopyToPath,lcOverWriteExpr)
  USE IN XXLIB
  RETURN .f.
ENDIF
IF NOT CopyFileToDisk("XXUSYS2.FRX",lcCopyToPath,lcOverWriteExpr)
  USE IN XXLIB
  RETURN .f.
ENDIF
IF NOT CopyFileToDisk("XXUSYSG1.FRX",lcCopyToPath,lcOverWriteExpr)
  USE IN XXLIB
  RETURN .f.
ENDIF
ENDTEXT
SET TEXTMERGE OFF 

IF llNTier
TEXT TO lcText2 NOSHOW 
*
*  SET CLASSLIB TO the VMP n-Tier services classes 
*
EXTERNAL CLASS XXFWNT.VCX
EXTERNAL CLASS XXFWNTBO.VCX
EXTERNAL CLASS XXFWNTBR.VCX
EXTERNAL CLASS XXFWNTDP.VCX
EXTERNAL CLASS XXFWNTDS.VCX
EXTERNAL CLASS XXXXCTRN.VCX
EXTERNAL CLASS XXXXCUSN.VCX
IF NOT X8SETCLS("XXFWNT.VCX")
  USE IN XXLIB
  RETURN .f.
ENDIF
IF NOT X8SETCLS("XXFWNTBO.VCX")
  USE IN XXLIB
  RETURN .f.
ENDIF
IF NOT X8SETCLS("XXFWNTBR.VCX")
  USE IN XXLIB
  RETURN .f.
ENDIF
IF NOT X8SETCLS("XXFWNTDS.VCX")
  USE IN XXLIB
  RETURN .f.
ENDIF
IF NOT X8SETCLS("XXFWNTDP.VCX")
  USE IN XXLIB
  RETURN .f.
ENDIF
IF NOT X8SETCLS("XXXXCTRN.VCX")
  USE IN XXLIB
  RETURN .f.
ENDIF
IF NOT X8SETCLS("XXXXCUSN.VCX")
  USE IN XXLIB
  RETURN .f.
ENDIF
*
*  and include these COM wrapper .PRGs, just for the
*  sake of completeness
*
EXTERNAL PROCEDURE XXXXNT.PRG 
EXTERNAL PROCEDURE XXFWNT.PRG 
ENDTEXT
ENDIF

TEXT TO lcText3 NOSHOW
*
*  the VMP super classes, and even though you
*  can actually modify your VMP framework to 
*  not use them, in which case they are not
*  necessary, they are so small that it won't
*  matter that we pull them in here if they exist
*
#IF FILE("XXXXCBO.VCX")
  EXTERNAL CLASS XXXXCBO.VCX
  IF NOT X8SETCLS("XXXXCBO.VCX") 
    RETURN .f.
  ENDIF
#ENDIF
#IF FILE("XXXXCHK.VCX")
  EXTERNAL CLASS XXXXCHK.VCX
  IF NOT X8SETCLS("XXXXCHK.VCX")  
    RETURN .f.
  ENDIF
#ENDIF
#IF FILE("XXXXCMD.VCX")
  EXTERNAL CLASS XXXXCMD.VCX
  IF NOT X8SETCLS("XXXXCMD.VCX")  
    RETURN .f.
  ENDIF
#ENDIF
#IF FILE("XXXXCTR.VCX")
  EXTERNAL CLASS XXXXCTR.VCX
  IF NOT X8SETCLS("XXXXCTR.VCX")  
    RETURN .f.
  ENDIF
#ENDIF
#IF FILE("XXXXCUS.VCX")
  EXTERNAL CLASS XXXXCUS.VCX
  IF NOT X8SETCLS("XXXXCUS.VCX")  
    RETURN .f.
  ENDIF
#ENDIF
#IF FILE("XXXXEDT.VCX")
  EXTERNAL CLASS XXXXEDT.VCX
  IF NOT X8SETCLS("XXXXEDT.VCX")  
    RETURN .f.
  ENDIF
#ENDIF
#IF FILE("XXXXGRD.VCX")
  EXTERNAL CLASS XXXXGRD.VCX
  IF NOT X8SETCLS("XXXXGRD.VCX")  
    RETURN .f.
  ENDIF
#ENDIF
#IF FILE("XXXXFRM.VCX")
  EXTERNAL CLASS XXXXFRM.VCX
  IF NOT X8SETCLS("XXXXFRM.VCX")  
    RETURN .f.
  ENDIF
#ENDIF
#IF FILE("XXXXHYP.VCX")
  EXTERNAL CLASS XXXXHYP.VCX
  IF NOT X8SETCLS("XXXXHYP.VCX")  
    RETURN .f.
  ENDIF
#ENDIF
#IF FILE("XXXXIMG.VCX")
  EXTERNAL CLASS XXXXIMG.VCX
  IF NOT X8SETCLS("XXXXIMG.VCX")  
    RETURN .f.
  ENDIF
#ENDIF
#IF FILE("XXXXLBL.VCX")
  EXTERNAL CLASS XXXXLBL.VCX
  IF NOT X8SETCLS("XXXXLBL.VCX")  
    RETURN .f.
  ENDIF
#ENDIF
#IF FILE("XXXXLIN.VCX")
  EXTERNAL CLASS XXXXLIN.VCX
  IF NOT X8SETCLS("XXXXLIN.VCX")  
    RETURN .f.
  ENDIF
#ENDIF
#IF FILE("XXXXLST.VCX")
  EXTERNAL CLASS XXXXLST.VCX
  IF NOT X8SETCLS("XXXXLST.VCX")  
    RETURN .f.
  ENDIF
#ENDIF
#IF FILE("XXXXOLB.VCX")
  EXTERNAL CLASS XXXXOLB.VCX
  IF NOT X8SETCLS("XXXXOLB.VCX")  
    RETURN .f.
  ENDIF
#ENDIF
#IF FILE("XXXXOPG.VCX")
  EXTERNAL CLASS XXXXOPG.VCX
  IF NOT X8SETCLS("XXXXOPG.VCX")  
    RETURN .f.
  ENDIF
#ENDIF
#IF FILE("XXXXPGF.VCX")
  EXTERNAL CLASS XXXXPGF.VCX
  IF NOT X8SETCLS("XXXXPGF.VCX")  
    RETURN .f.
  ENDIF
#ENDIF
#IF FILE("XXXXSHP.VCX")
  EXTERNAL CLASS XXXXSHP.VCX
  IF NOT X8SETCLS("XXXXSHP.VCX")  
    RETURN .f.
  ENDIF
#ENDIF
#IF FILE("XXXXSPN.VCX")
  EXTERNAL CLASS XXXXSPN.VCX
  IF NOT X8SETCLS("XXXXSPN.VCX")  
    RETURN .f.
  ENDIF
#ENDIF
#IF FILE("XXXXTBR.VCX")
  EXTERNAL CLASS XXXXTBR.VCX
  IF NOT X8SETCLS("XXXXTBR.VCX")  
    RETURN .f.
  ENDIF
#ENDIF
#IF FILE("XXXXTMR.VCX")
  EXTERNAL CLASS XXXXTMR.VCX
  IF NOT X8SETCLS("XXXXTMR.VCX")  
    RETURN .f.
  ENDIF
#ENDIF
#IF FILE("XXXXTXT.VCX")
  EXTERNAL CLASS XXXXTXT.VCX
  IF NOT X8SETCLS("XXXXTXT.VCX")  
    RETURN .f.
  ENDIF
#ENDIF

*
*  ensure that VMP .PRG routines are pulled into
*  the XXLIB.PJX/.EXE/.APP
*
*  you could remove any that you are SURE aren't going
*  to be used by this application, but the reduction in
*  size probably isn't worth the risk in not having a
*  .PRG when you need it
*
EXTERNAL PROCEDURE XXAPPINF.PRG 
EXTERNAL PROCEDURE XXAPPLoader.PRG 
EXTERNAL PROCEDURE XXCKNSIT.PRG
EXTERNAL PROCEDURE XXCreateDBCWithRemoteViewsOfVMPSystemTables.PRG
EXTERNAL PROCEDURE XXError.PRG 
EXTERNAL PROCEDURE XXFWLeaf.PRG
EXTERNAL PROCEDURE XXFWMain.PRG
EXTERNAL PROCEDURE XXIUDLog.PRG
EXTERNAL PROCEDURE XXLogger.PRG 
EXTERNAL PROCEDURE XXModalFormEditboxFind.PRG
EXTERNAL PROCEDURE XXOpenDatabase.PRG
EXTERNAL PROCEDURE XXOpenTable.PRG
EXTERNAL PROCEDURE XXRQuery.PRG 
EXTERNAL PROCEDURE XXSQLEXE.PRG 
EXTERNAL PROCEDURE XXSTDBY.PRG
EXTERNAL PROCEDURE XXSYNCPK.PRG 
EXTERNAL PROCEDURE XXVMPBLD.PRG 
EXTERNAL PROCEDURE XXVMPBuild.PRG
EXTERNAL PROCEDURE XXXXGRC.PRG
EXTERNAL PROCEDURE XXXXGRH.PRG

EXTERNAL PROCEDURE X1LFM.PRG 
EXTERNAL PROCEDURE X1FML.PRG 

EXTERNAL PROCEDURE X2CnvChr.PRG 
EXTERNAL PROCEDURE X2IsArray.PRG
EXTERNAL PROCEDURE X2IsTag.PRG 
EXTERNAL PROCEDURE X2PStack.PRG 
EXTERNAL PROCEDURE X2RelXpr.PRG 
EXTERNAL PROCEDURE X2SETLIB.PRG 

EXTERNAL PROCEDURE X3BTABLE.PRG 
EXTERNAL PROCEDURE X3CKCHEK.PRG 
EXTERNAL PROCEDURE X3CPEDIG.PRG   &&& your app may call this
EXTERNAL PROCEDURE X3CRECUR.PRG 
EXTERNAL PROCEDURE X3CSOURC.PRG 
EXTERNAL PROCEDURE X3DBCPTH.PRG 
EXTERNAL PROCEDURE X3DBCSWP.PRG 
EXTERNAL PROCEDURE X3DELTRE.PRG 
EXTERNAL PROCEDURE X3ED.PRG 
EXTERNAL PROCEDURE X3FBMEM.PRG 
EXTERNAL PROCEDURE X3GENPK.PRG 
EXTERNAL PROCEDURE X3GETRT.PRG 
EXTERNAL PROCEDURE X3GETUSR.PRG 
EXTERNAL PROCEDURE X3GOURL.PRG 
EXTERNAL PROCEDURE X3I.PRG 
EXTERNAL PROCEDURE X3ISFLD.PRG    &&& your app may call this
EXTERNAL PROCEDURE X3LCKSCR.PRG 
EXTERNAL PROCEDURE X3MD.PRG 
EXTERNAL PROCEDURE X3MSGSVC.PRG 
EXTERNAL PROCEDURE X3PKEXPR.PRG 
EXTERNAL PROCEDURE X3PKTAG.PRG 
EXTERNAL PROCEDURE X3RLB.PRG 
EXTERNAL PROCEDURE X3RULTXT.PRG 
EXTERNAL PROCEDURE X3SEEK.PRG 
EXTERNAL PROCEDURE X3SETCLS.PRG   &&& your app may call this
EXTERNAL PROCEDURE X3SETCPR.PRG   &&& your app may call this
EXTERNAL PROCEDURE X3SETPRC.PRG   &&& your app may call this
EXTERNAL PROCEDURE X3SRCTYP.PRG 
EXTERNAL PROCEDURE X3STDBY.PRG 
EXTERNAL PROCEDURE X3TAGXPR.PRG 
EXTERNAL PROCEDURE X3UUIDNA.PRG 
EXTERNAL PROCEDURE X3VTABLE.PRG 
EXTERNAL PROCEDURE X3VUSPHN.PRG 
EXTERNAL PROCEDURE X3VVALID.PRG 
EXTERNAL PROCEDURE X3WINMSG.PRG 
EXTERNAL PROCEDURE X3WRPFRM.PRG   &&& your app may call this

EXTERNAL PROCEDURE X5Container.PRG
EXTERNAL PROCEDURE X5PITEMS.PRG 
EXTERNAL PROCEDURE X5WebPageNavComplete.PRG
EXTERNAL PROCEDURE X5WinExplorer.PRG

EXTERNAL PROCEDURE X6GetPublicMemvars.PRG
EXTERNAL PROCEDURE X6ICC.PRG 
EXTERNAL PROCEDURE X6ISCOM.PRG 
EXTERNAL PROCEDURE X6ISFLD.PRG 
EXTERNAL PROCEDURE X6ROFILE.PRG 
EXTERNAL PROCEDURE X6SAF.PRG 
EXTERNAL PROCEDURE X6TAGFC.PRG 
EXTERNAL PROCEDURE X6TTOS.PRG
EXTERNAL PROCEDURE X6UDofDT.PRG
EXTERNAL PROCEDURE X6WINOS.PRG 

EXTERNAL PROCEDURE X7ColorToRGBString.PRG
EXTERNAL PROCEDURE X7CPEDIG.PRG
EXTERNAL PROCEDURE X7DISPTH.PRG   
EXTERNAL PROCEDURE X7DTSTRING.PRG   
EXTERNAL PROCEDURE X7ISAPIF.PRG 
EXTERNAL PROCEDURE X7PrintWindow.PRG
EXTERNAL PROCEDURE X7RedrawWindow.PRG
EXTERNAL PROCEDURE X7RemoveCRLFTab.PRG
EXTERNAL PROCEDURE X7ShellExecute.PRG

EXTERNAL PROCEDURE X8BEALL.PRG
EXTERNAL PROCEDURE X8BTABLE.PRG
EXTERNAL PROCEDURE X8CompareFiles.PRG
EXTERNAL PROCEDURE X8ConvChar.PRG 
EXTERNAL PROCEDURE X8ED.PRG
EXTERNAL PROCEDURE X8FindExecutable.PRG
EXTERNAL PROCEDURE X8GetSysFontInfo.PRG
EXTERNAL PROCEDURE X8GUID.PRG
EXTERNAL PROCEDURE X8IsAutoInc.PRG
EXTERNAL PROCEDURE X8IsCollection.PRG
EXTERNAL PROCEDURE X8RegExpTest.PRG
EXTERNAL PROCEDURE X8SETCLS.PRG 
EXTERNAL PROCEDURE X8SETCPR.PRG 
EXTERNAL PROCEDURE X8SETPRC.PRG 
EXTERNAL PROCEDURE X8SHCopyFile.PRG
EXTERNAL PROCEDURE X8ShellExecute.PRG 
EXTERNAL PROCEDURE X8ValidEMailAddress.PRG
EXTERNAL PROCEDURE X8ValidIPAddress.PRG
EXTERNAL PROCEDURE X8ValidURL.PRG
EXTERNAL PROCEDURE X8WRPFRM.PRG 

*
*  MSGSVC routines
*
EXTERNAL PROCEDURE MSGSVC.PRG 
EXTERNAL PROCEDURE NOHOT.PRG  

*
*  ensure that these two generic reports are
*  copied to disk so they can always be located at runtime
*
IF NOT CopyFileToDisk("XXERROR.FRX",lcCopyToPath,lcOverWriteExpr)
  USE IN XXLIB
  RETURN .f.
ENDIF
IF NOT CopyFileToDisk("XXTBLOCK.FRX",lcCopyToPath,lcOverWriteExpr)
  USE IN XXLIB
  RETURN .f.
ENDIF

RETURN 



PROCEDURE CopyFileToDisk
*
*  copy a file from XXLIB.DBF (built into XXLIB.EXE)
*  to disk, in the current directory (the source code
*  directory where XXLIB.EXE is installed and running), 
*  if either of the following is true:
*   1- the passed tcFileName does not exist
*   2- the passed tcFileName exists, but any passed
*      tcOverWriteExpr EVALUATE()s to .T.
*
LPARAMETERS tcFileName, tcCopyToPath, tcOverWriteExpr
LOCAL lcFileName1, lcFileName2, lnSelect, lcSetSafety, ;
      llCopy, llSuccess 
lcSetSafety = SET("SAFETY")
SET SAFETY OFF 
llSuccess = .t.
lcFileName1 = UPPER(ALLTRIM(tcFileName))
lcFileName2 = SPACE(0)
DO CASE
  CASE JUSTEXT(lcFileName1) = "FRX"
    lcFileName2 = JUSTSTEM(lcFileName1) + ".FRT"
  *  other extensions, like .SCX can be handled here
  *  when/if that becomes necessary
ENDCASE
lnSelect = SELECT(0)
SELECT XXLIB

LOCATE FOR UPPER(ALLTRIM(XL_FName)) == lcFileName1

IF NOT FILE(tcCopyToPath+lcFileName1) ;
     OR EVALUATE(tcOverWriteExpr)
  *
  *  only copy the file to disk if either
  *
  *  1- It doesn't exist:
  *     - This is the initial installation
  *     - The user has deleted/renamed it 
  *     - Your application deletes all such non .APP/.EXE
  *       files each time it terminates
  *
  *    OR
  *
  *  2- EVALUATE(lcOverWriteExpr) = .T.
  *     lcOverWriteExpr can either be passed to this
  *     XXLIB.EXE at runtime, or can be specified when 
  *     XXLIB.EXE is built, by passing it to this 
  *     XXDTBuildXXLib.PRG utility
  *     
  TRY
    COPY MEMO XL_File TO (tcCopyToPath+lcFileName1) 
  CATCH
    llSuccess = .f.
  ENDTRY
ENDIF

IF llSuccess AND NOT EMPTY(lcFileName2) 
  IF NOT FILE(tcCopyToPath+lcFileName2) ;
       OR EVALUATE(tcOverWriteExpr)
    *
    *  only copy the file to disk if the above
    *  conditions are met
    *     
    TRY
      COPY MEMO XL_File TO (tcCopyToPath+lcFileName2) 
    CATCH
      llSuccess = .f.
    ENDTRY
  ENDIF
ENDIF

SET SAFETY &lcSetSafety
SELECT (lnSelect)

RETURN llSuccess



PROCEDURE EraseFilesFromDisk
*
*  this procedure ERASEs all files that were copied
*  to disk in CopyFileToDisk() 
*
*  NOTE that this procedure should only be called
*  when you are done with XXLIB.EXE, as demonstrated
*  in VMMAIN.PRG
*    XXDTES("VMMAIN.PRG","EraseFilesFromDisk")
*  where this procedure is called once XXFWMAIN.PRG
*  is finished, and therefore all the VMP XLIB files
*  are no longer needed
*
*  RETURNs a logical value indicating whether the
*  ERASEs were all successful, in case you need to know
*
LOCAL llSuccess, lcOnError
llSuccess = .t.
*
*  old-style ON ERROR local error trapping here,
*  because I don't want anything to be aborted,
*  but to just RETURN a logical value indicating
*  if SOMETHING didn't work
*
lcOnError = ON("ERROR")
CLEAR MENUS
CLEAR POPUPS 
ON ERROR llSuccess = .f.
USE XXLIB.DBF IN 0   &&& should never fail
IF llSuccess
  SELECT XXLIB
  SCAN
    IF JUSTEXT(UPPER(ALLTRIM(XL_FName))) = "VCX"
      CLEAR CLASSLIB (ALLTRIM(XL_FName))
    ENDIF
    IF ","+JUSTEXT(UPPER(ALLTRIM(XL_FName)))+"," $ ",BMP,JPG,GIF,ANI,DIB,EMF,EXIF,GFA,JPEG,JPE,JFIF,PNG,TIF,TIFF,WMF,"
      CLEAR RESOURCES (ALLTRIM(XL_FName))
    ENDIF
    ERASE ALLTRIM(XL_FName)   &&& could possibly fail
  ENDSCAN
ENDIF
ON ERROR &lcOnError
USE IN SELECT("XXLIB")
RETURN llSuccess

ENDTEXT

*
*  create the XXLIB.PRG main program of the XXLIB.PJX
*
LOCAL lcCRLF
lcCRLF = CHR(13) + CHR(10) + CHR(13) + CHR(10)
IF NOT EMPTY(lcText2)
  lcText2 = lcCRLF + lcText2
ENDIF
lcText3 = lcCRLF + lcText3
STRTOFILE(lcText1+lcText2+lcText3,"XXLIB.PRG",.f.)

*
*  create a table to hold files that need to be
*  copied to disk, populate it
* 
LOCAL laFiles[10], lcFileName, lcSetCompatible
laFiles[1] = "XXUSYS1.FRX"
laFiles[2] = "XXUSYS1.FRT"
laFiles[3] = "XXUSYS2.FRX"
laFiles[4] = "XXUSYS2.FRT"
laFiles[5] = "XXUSYSG1.FRX"
laFiles[6] = "XXUSYSG1.FRT"
laFiles[7] = "XXERROR.FRX"
laFiles[8] = "XXERROR.FRT"
laFiles[9] = "XXTBLOCK.FRX"
laFiles[10] = "XXTBLOCK.FRT" 
lcSetCompatible = SET("COMPATIBLE")
SET COMPATIBLE ON    &&& so FSIZE() yields file size
CREATE TABLE XXLIB.DBF (XL_FName C(20), XL_File M, XL_DT T, XL_Size I)
SELECT XXLIB 
FOR EACH lcFileName IN laFiles
  INSERT INTO XXLIB (XL_FName, XL_DT, XL_Size) ;
       VALUES ;
       (lcFileName, FDATE(lcFileName,1), FSIZE(lcFileName))
  APPEND MEMO XL_File FROM (lcFileName) OVERWRITE 
ENDFOR
SET COMPATIBLE &lcSetCompatible
USE IN XXLIB

*
*  create XXLIB.PJX
*
MODIFY PROJECT XXLIB NOWAIT NOSHOW 
LOCAL loProject, loFile
loProject = Application.ActiveProject
loProject.Debug = llDebugInfo
loProject.Encrypted = (NOT llNoEncrypt)
IF NOT EMPTY(lcIconFile)
  loProject.Icon = lcIconFile
ENDIF
loProject.Files.Add("XXLIB.DBF")
loProject.Files(1).Exclude = .F.
loProject.Files.Add("XXLIB.PRG")
loProject.SetMain("XXLIB.PRG")
loProject.Build()
*  exclude all .FRXs to prevent them from being built 
*  into the XXLIB.EXE, causing the FILE() check in 
*  XXLIB.PRG/CopyFileToDisk() to always evaluate to .T.
FOR EACH loFile IN loProject.Files
  IF UPPER(JUSTEXT(loFile.Name)) = "FRX"
    loFile.Exclude = .t.
  ENDIF
ENDFOR
*
*  I don't know why, but the first Build doesn't 
*  always pull in all the files
*
loProject.Build()

IF llModiProj
  WAIT WINDOW "Here is the XXLIB.PJX where you can make any desired updates" + CHR(13) + ;
              "in the Project Info dialog.  As soon as you close the" + CHR(13) + ;
              "Project Manager, the " + CHR(13) + ;
              SPACE(4) + "BUILD " + JUSTEXT(lcXXLIBFileName) + SPACE(1) + lcXXLIBFileName + " FROM XXLIB.PJX" + CHR(13) + ;
              "command will be executed." ;
              NOWAIT NOCLEAR 
  MODIFY PROJECT XXLIB.PJX
  WAIT CLEAR 
ENDIF

*
*  create lcXXLIBFileName
*
IF JUSTEXT(lcXXLIBFileName) = "EXE"
  BUILD EXE (lcXXLIBFileName) FROM XXLIB.PJX
 ELSE
  BUILD APP (lcXXLIBFileName) FROM XXLIB.PJX
ENDIF

CLOSE ALL 

ACTIVATE SCREEN
CLEAR 
? lcXXLIBFileName + " has been successfully created."
IF llDontErase = .t.
  *
  *  leave the files created here that are used to
  *  build XXLIB.EXE, usually because you need to 
  *  review them for debugging purposes
  *    XXLIB.PRG
  *    XXLIB.DBF
  *    XXLIB.PJX
  *
  USE XXLIB
  BROWSE LAST NOWAIT 
  MODIFY PROJECT XXLIB NOWAIT 
  MODIFY COMMAND XXLIB NOWAIT 
  ACTIVATE WINDOW Command
 ELSE
  *
  *  default behavior is to erase all these working
  *  files, yielding only the ??LIB.APP/??LIB.EXE file
  *
  ERASE XXLIB.PRG
  ERASE XXLIB.DBF
  ERASE XXLIB.FPT
  ERASE XXLIB.PJX
  ERASE XXLIB.PJT
ENDIF

RETURN
