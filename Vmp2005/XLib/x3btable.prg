*
*  X3BTABLE.PRG
*  Returns the name of the ultimate base table for the 
*  passed view Alias.FieldName (in this respect this 
*  routine is different from X3VTABLE(), and should be 
*  used when you need to know the *ULTIMATE* base table, 
*  like when a view is based on a view, etc.)
*  
*  Works with tables, too (alias may not be the same as 
*  the table name)
*
*  Note that the value RETURNed can be different if you 
*  pass just an Alias vs an Alias.FieldName -- view fields 
*  can map to different base tables.  For this reason, 
*  it's always best to pass an Alias.FieldName when the 
*  Alias is a view.
*  
*
*  This routine is included for backward compatibility
*  and is not called anywhere in the current VMP
*  framework -- use X8BTABLE.PRG instead.
*
*
*  If the passed tcAliasPlusField is from a CREATE CURSOR 
*  or SQL cursor, this routine returns SPACE(0)
*
*  This program is similar to X3VTABLE.PRG
*
*  Copyright (c) 1997-2005 Visionpace   All Rights Reserved
*                17501 East 40 Hwy., Suite 218
*                Independence, MO 64055
*                816-350-7900 
*                http://www.visionpace.com
*                http://vmpdiscussion.visionpace.com
*  Author:  Drew Speedie
*
*
*  Please note that this routine is similar to 
*  X3VTABLE.PRG, and if you can use X3VTABLE() instead 
*  of this program, you should.
*
*
*  Usage:
*    * from a method of a control bound to a field in a view:
*    lcTableName = X3BTable(THIS.ControlSource)
*
*  lParameters
*    tcAliasPlusField (R) "Alias.FieldName" from the view, which field 
*                            is mapped to the base table you're after 
*                            (if your view has a join)
*                          Optionally, pass just an ALIAS(), but to be
*                            safe, only if the view has FIELD(1) from 
*                            the base table
*          tcDatabase (O)  Name of the database where we can find
*                            tcAliasPlusField
*                          Defaults to the database containing
*                            tcAliasPlusField
*
LPARAMETERS tcAliasPlusField, tcDatabase

IF TYPE("tcAliasPlusField") = "C" AND NOT EMPTY(tcAliasPlusField)
  tcAliasPlusField = UPPER(ALLTRIM(tcAliasPlusField))
 ELSE
  RETURN SPACE(0)
ENDIF
LOCAL lcAlias, lcFieldName
IF "." $ tcAliasPlusField
  lcAlias = LEFTC(tcAliasPlusField,AT_C(".",tcAliasPlusField)-1)
  lcFieldName = SUBSTRC(tcAliasPlusField,AT_C(".",tcAliasPlusField)+1)
 ELSE
  lcAlias = tcAliasPlusField
  lcFieldName = SPACE(0)
ENDIF
IF !USED(lcAlias)
  RETURN space(0)
ENDIF

*
*  check for a CREATE CURSOR or SQL cursor
*
IF RIGHTC(CURSORGETPROP("SourceName",lcAlias),4) = ".TMP"
  RETURN SPACE(0)
ENDIF

*
*  check for a free/Fox2x table
*
IF EMPTY(CURSORGETPROP("DATABASE",lcAlias)) ;
     OR (EMPTY(tcDatabase) AND X2PSTACK("X3BTABLE"))
  *
  *  DWS 05/21/02
  *  the extra OR condition traps for a recursive
  *  call here, and no .DBC found
  *
  RETURN lcAlias
ENDIF

LOCAL lcSetDatabase 
lcSetDatabase = SET("DATABASE")
IF TYPE("tcDatabase") = "C" ;
     AND NOT ISNULL(tcDatabase) ;
     AND NOT EMPTY(tcDatabase)
  SET DATABASE TO (JUSTSTEM(tcDatabase))
 ELSE
  SET DATABASE TO (CURSORGETPROP("DATABASE",lcAlias))
ENDIF
LOCAL lcBaseTableName, llView
llView = .f.
DO CASE
  ******************************************************
  CASE inlist(CURSORGETPROP("SourceType",lcAlias),3,103,203) ;
       AND NOT "." $ tcAliasPlusField 
  ******************************************************
    *
    *  tcAliasPlusField is a table alias
    *   
    lcBaseTableName = CURSORGETPROP("SourceName",lcAlias)
  ******************************************************
  CASE INLIST(CURSORGETPROP("SourceType",lcAlias),1,2,101,201,102,202) AND NOT "." $ tcAliasPlusField 
  ******************************************************
    *
    *  tcAliasPlusField is a view alias
    *
    *  DANGER!  We're evaluating dbgetprop(..."UpdateName")
    *           here by default.  Using field(1) in the
    *           view alias may or may not be what you're
    *           after, but it's the best we can do by
    *           default because you haven't passed in the
    *           complete Alias.FieldName
    *   
    llView = .t.
    lcBaseTableName = ;
         DBGETPROP(CURSORGETPROP("SourceName",lcAlias)+"."+FIELD(1,lcAlias),"FIELD","UpdateName")
    IF OCCURS(".",lcBaseTableName) = 0 ;
         OR OCCURS("(",lcBaseTableName) > 0
      *
      *  tcAliasPlusField is a "calculated" AS column
      *
      lcBaseTableName = SPACE(0)
    ENDIF  &&& AS column
  ******************************************************
  CASE inlist(CURSORGETPROP("SourceType",lcAlias),3,103,203)
  ******************************************************
    *
    *  tcAliasPlusField is a table alias plus field name
    *   
    lcBaseTableName = CURSORGETPROP("SourceName",lcAlias)
  ******************************************************
  OTHERWISE
  ******************************************************
    *
    *  tcAliasPlusField is a view alias plus field name
    *   
    llView = .t.
    lcBaseTableName = ;
         DBGETPROP(CURSORGETPROP("SourceName",lcAlias)+"."+SUBSTRC(tcAliasPlusField,AT_C(".",tcAliasPlusField)+1),"FIELD","UpdateName")
    IF OCCURS(".",lcBaseTableName) = 0 ;
         OR OCCURS("(",lcBaseTableName) > 0
      *
      *  tcAliasPlusField is a "calculated" AS column
      *
      lcBaseTableName = SPACE(0)
    ENDIF  &&& AS column
ENDCASE

IF llView AND !EMPTY(lcBaseTableName)
  * 
  * parse off the fieldname suffix
  *
  lcBaseTableName = LEFTC(lcBaseTableName, ;
                          AT_C(".",lcBaseTableName,OCCURS(".",lcBaseTableName))-1)
  *
  *  if lcBaseTableName is a free table, lcBaseTableName ends in ".DBF"
  *
  IF RIGHTC(lcBaseTableName,4) = ".DBF"
    lcBaseTableName = UPPER(JUSTSTEM(lcBaseTableName))
   ELSE
    *
    *  check to see if lcBaseTableName is itself a view, in which case 
    *  we have to recurse this logic back until we hit the base table
    *
    IF USED(SUBSTRC(lcBaseTableName,AT_C("!",lcBaseTableName)+1))
      *
      *  this should always be true  
      *
      IF EMPTY(lcFieldName)
        lcBaseTableName = X3BTABLE(SUBSTRC(lcBaseTableName,AT_C("!",lcBaseTableName)+1), ;
                                   JUSTSTEM(LEFTC(lcBaseTableName,AT_C("!",lcBaseTableName)-1)))
       ELSE
        lcBaseTableName = X3BTABLE(SUBSTRC(lcBaseTableName,AT_C("!",lcBaseTableName)+1) + "." + lcFieldName, ;
                                   JUSTSTEM(LEFTC(lcBaseTableName,AT_C("!",lcBaseTableName)-1)))
      ENDIF   &&& empty(lcFieldName)
    ENDIF
  ENDIF
  *
  *  parse off any native VFP database prefix
  *
  lcBaseTableName = SUBSTRC(lcBaseTableName,AT_C("!",lcBaseTableName)+1)
  *
  *  parse off any remote data database prefix
  *
  IF occurs(".",lcBaseTableName) > 0
    lcBaseTableName = SUBSTRC(lcBaseTableName, ;
                              AT_C(".",lcBaseTableName,OCCURS(".",lcBaseTableName))+1)
  ENDIF
ENDIF

SET DATABASE TO &lcSetDatabase

return UPPER(ALLTRIM(lcBaseTableName))
