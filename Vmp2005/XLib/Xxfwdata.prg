*
*  XXFWDATA.PRG
*  Create the main VMP database and tables in the current directory
*    Required:
*      AppInfo
*      Users, V_UsersList 
*      AppConfig (free table)
*    Optional:
*      UserPrefs, V_UserPrefs
*      GeneratePK
*      ReportCatalog
*      UserGroups, V_UserGroupsList, UsrGrpXRef, V_UsrGrpXRef, 
*                  V_UsrGrpXRefList, V_GroupMembers
*      SecurityAccess, V_SecurityAccess, 
*      UserAccExceptions, V_UserAccExceptions, 
*      SecuredItems (free table)
*
*  Copyright (c) 1997-2005 Visionpace   All Rights Reserved
*                17501 East 40 Hwy., Suite 218
*                Independence, MO 64055
*                816-350-7900 
*                http://www.visionpace.com
*                http://vmpdiscussion.visionpace.com
*  Author:  Drew Speedie 
*           Special thanks to Ed Lennon and Art Bergquist
*
*
*
*  USAGE:
*  * Create VM database and add all system tables/views
*  * (pass the 4th parameter as .F. for character keys):
*  *   APPINFO, APPCONFIG/xxCONFIG.DBF (free table), GENERATEPK,
*  *   USERS, V_USERSLIST, UserPrefs, V_UserPrefs, REPORTCATALOG
*  DO XXFWDATA WITH "VM","ALL",.f.,.t.
*
*  * Create VM database and minimum VMP tables
*  * (pass the 4th parameter as .F. for character keys):
*  *   APPINFO, APPCONFIG/xxCONFIG.DBF (free table), 
*  *   USERS, V_USERSLIST
*  DO XXFWDATA WITH "VM","MIN",.f.,.t.
*
*  * Create VM database and add system tables
*  * (pass the 4th parameter as .F. for character keys):
*  *   APPINFO, APPCONFIG/xxCONFIG.DBF (free table), 
*  DO XXFWDATA WITH "VM","SYSTEM",.f.,.t.
*
*  * Create USERS, V_USERSLIST in existing VM.DBC:
*  * (pass the 4th parameter as .F. for character keys):
*  DO XXFWDATA WITH "VM","USERS",.t.,.t.
*
*  * Create UserPrefs, V_UserPrefs in existing VM.DBC
*  * (pass the 4th parameter as .F. for character keys):
*  DO XXFWDATA WITH "VM","UserPrefs",.t.,.t.
*
*  * Create GENERATEPK in existing VM.DBC
*  * (pass the 4th parameter as .F. for character keys):
*  DO XXFWDATA WITH "VM","GENPK",.t.,.t.
*
*  * Create REPORTCATALOG in existing VM.DBC
*  * (pass the 4th parameter as .F. for character keys):
*  DO XXFWDATA WITH "VM","REPCAT",.t.,.t.
*
*  * Create optional user-security tables in existing VM.DBC
*  * (pass the 4th parameter as .F. for character keys):
*  DO XXFWDATA WITH "VM","SECURITY",.t.,.t.
*
*  * Create optional Referential Integrity table in existing VM.DBC
*  * (pass the 4th parameter as .F. for character keys):
*  DO XXFWDATA WITH "VM","RI",.t.,.t.
*
*  * Create optional IUDLog table in existing VM.DBC
*  * (pass the 4th parameter as .F. for character keys):
*  DO XXFWDATA WITH "VM","IUDLog",.t.,.t.
*
*  * Create Visual MaxFrame system tables in whatever .DBC
*  * is open and the current SET DATABASE TO database,
*  * prefixing the system table .DBF filenames with "PQ"
*  * (pass the 4th parameter as .F. for character keys):
*      DO XXFWDATA WITH "PQ","ALL",.t.,.t.
*  * note that if a database is not open and current when
*  * you call XXFWDATA with the .T. 3rd parameter, the action
*  * is taken with respect to the database named in the first
*  * parameter
*
*  
*  Also, the following local procedures in this .PRG can be
*  called directly, from the Command Window, intended to allow
*  you to ensure that view(s) and/or stored procedures are up-to-date.  
*  Each requires that the target database be open and the current
*  SET("DATABASE").  The ones that update stored procedures also
*  require that the current SET("DATABASE") be open EXCLUSIVE
*  so that the COMPILE DATABASE command can be successfully 
*  executed.
*    CreateViewsByTable(<TableName>)
*        Re-creates the view(s) associated with the passed VMP
*        system table name in the current SET("DATABASE").
*    UpdateSystemViews()
*        Re-creates ALL the views based on VMP system table(s) 
*        in the current SET("DATABASE") -- views whose underlying
*        table(s) are optional and not currently contained in 
*        the database are not re-created.
*    UpdateRI()
*        1-If the current SET("DATABASE") contains the VMPRI table,
*          deletes and re-creates the VMP RI stored procedures.
*        2-If the current SET("DATABASE") contains the VMPRI table,
*          makes sure the VMPRI table is populated with the records 
*          needed for the Referential Integrity defined here in XXFWDATA.
*        3-If the current SET("DATABASE") contains the VMPRI table,
*          makes sure the Referential Integrity triggers match
*          those specified in the VMPRI table.
*    CreateIUDLogTable()
*        Create either a single IUDLog table or a "dedicated"
*        IUDLog_<TableName> log table
*    UpdateIUDLog()
*        Calls IUDLogStoredProcedures() and CreateTriggers() to
*        update the installation of the IUDLog engine if the
*        database contains the IUDLog table.
*    IUDLogStoredProcedures()
*        Updates (re-writes) the SP_VMP_IUDLog..() Stored Procedures.
*    UpdateSystemStoredProcedures()
*        Deletes the non-VMP-Referential-Integrity stored procedures
*        and re-creates them as specified here.
*    UpdateAllStoredProcedures()
*        1- Deletes the non-VMP-Referential-Integrity stored procedures
*           and re-creates them as specified here.
*        2- Executes UpdateRI(), described above.
*        3- Executes UpdateIUDLog(), described above.
*     ReindexAllVMPTables()
*     Remove_SP_VMP_IUDLog_CallsFromAllTriggersOfAllTables
*     Remove_SP_VMP_IUDLog_CallsFromAllTriggersOfOneTable()
*     Remove_SP_VMP_IUDLog_Trigger()
*     Add_SP_VMP_IUDLog_CallsToOneIUDTriggerOfOneTable()
*     RemoveOptionalFieldFromIUDLogTable()
*     GetLongestTableName()
*  Call these local procedures like this:
*    OPEN DATABASE <MyDBC> EXCLUSIVE
*    DO UpdateSystemViews IN XXFWDATA
*    DO CreateViewsByTable IN XXFWDATA WITH "Users"
*    DO UpdateAllStoredProcedures IN XXFWDATA
*    DO ReindexAllVMPTables IN XXFWData 
*        NOTE that if you run this routine in VFP9, the 
*        default behavior is to create the DELETED() tags
*        as BINARY index tags -- if you don't want this 
*        behavior, possibly because you are developing in
*        VFP9 but deploying in VFP8, then pass an explicit
*        .T. as the first/only parameter to ReindexAllVMPTables()
*
*
*  PLEASE NOTE that several of the views created here cannot
*  be properly created or maintained in the native VFP View Designer.
*  The View Designer has a number of severe limitations, so
*  we always create views programmatically, as is done here, for
*  all but the simplest of views.  For examples of a couple of the
*  problems with the View Designer, see the following MSFT KB articles:
*  
*  http://support.microsoft.com/support/kb/articles/Q157/2/54.asp
*  http://support.microsoft.com/support/kb/articles/Q156/6/67.ASP
*
*

LPARAMETERS tcParameter1, ;                        &&& Req -- Name of the .DBC
            tcParameter2, ;                        &&& Req -- Action:  "ALL", "MIN", "SYSTEM", etc.
            tlSetDBCtoExistingDBC, ;               &&&        Indicates that tcParameter1 is an existing .DBC, which should be updated, rather than created from scratch
            tlIntegerKeys, ;                       &&&        Create Primary Key fields as Integer rather than as C(5)
            tlSkipMBWarningAboutOverwritingDBC, ;  &&&        Pass as .T. to indicate that you want to suppress the MESSAGEBOX() warning about overwriting an existing database, in CreateXXData()
            tlFreeMemoAETable, ;                   &&&        Pass as .T. to indicate that the MemeAE table (if to be created in this trip here, based on Action/tcParameter2) should be created as a Free table
            tlNoBinaryDELETEDTagsInVFP9OrHigher, ; &&&        Pass as .T. to suppress the default behavior to where, when this utility is run in VFP9 or higher, tags on DELETED() are created as BINARY index tags
            tlX8ED                                 &&&        Pass as .T. to implement X8ED()-based encryption rather than the default/older X3ED()-based encryption, where appropriate (only certain VMP system table fields are encrypted, so depending on Action/tcParameter2, this value may be ignored)

IF NOT X8SETPRC("XXPROCDT.PRG")
  MESSAGEBOX("Unable to locate XXPROCDT.PRG.  Please ensure that " + ;
             "\XLIB is in your VFP path and that XXPROCDT.PRG is " + ;
             "in that \XLIB directory.", ;
             16, ;
             "Please Note")
  RETURN .f.
ENDIF
IF NOT VMPRequirementsAndAssumptions()  &&& in XXPROCDT.PRG
  RETURN .NULL.
ENDIF

LOCAL lcAction, ;
      lcXLibraryDirectory, ;
      lcSystemTable, ;
      llOverwriteExistingXXDBC, ;
      llSetDBCtoExistingDBC, ;
      lcCurrentSetDatabaseTo, ;
      llUseIntegerPKFields, ;
      llSkipWarning, ;
      llFreeMemoAETable, ;
      llNoBinaryDELETEDTagsInVFP9OrHigher, ;
      llX8ED
IF NOT VARTYPE(m.tcParameter1)="C" OR EMPTY(m.tcParameter1)
  WAIT WINDOW "Parameters passed incorrectly to " + PROGRAM()
  RETURN .f.
ENDIF
lcAction = UPPER(ALLTRIM(m.tcParameter1))
*
*  remove the ".DBC" extension, if passed
*
lcAction = UPPER(JUSTSTEM(m.lcAction)) 
lcCurrentSetDatabaseTo = SET("DATABASE")
IF NOT VARTYPE(tcParameter2)="C"  
  WAIT WINDOW "Invalid system table name parameter " + TRANSFORM(m.tcParameter2) + " passed to " + PROGRAM()
  RETURN .f.
ENDIF
IF EMPTY(m.tcParameter2)
  WAIT WINDOW "System table name parameter must be passed to " + PROGRAM()
  RETURN .f.
ENDIF
lcSystemTable = UPPER(ALLTRIM(m.tcParameter2))
IF NOT INLIST(m.lcSystemTable, ;
           "SYSTEM",;
           "GENPK", ;
           "USERS", ;
           "USERPREFS", ;
           "LOGINHISTORY", ;
           "LOGGEDINUSERS", ;
           "SECURITY", ;
           "REPCAT", ;
           "ALL", ;
           "MEMOAE", ;
           "MIN", ;
           "RI", ;
           "IUDLOG")
  WAIT WINDOW "Invalid system table name parameter " + TRANSFORM(m.tcParameter2) + " passed to " + PROGRAM()
  RETURN .f.
ENDIF
*
*  pass a .T. as the 3rd parameter to leave the current
*  SET DATABASE TO instead of arbitrarily changing it to 
*  the passed prefix 
*
IF NOT VARTYPE(m.tlSetDBCtoExistingDBC) = "L" 
  llSetDBCtoExistingDBC = .f.
 ELSE
  llSetDBCtoExistingDBC = m.tlSetDBCtoExistingDBC
ENDIF
IF m.llSetDBCtoExistingDBC
  lcCurrentSetDatabaseTo = SET("DATABASE")
ENDIF

IF NOT VARTYPE(m.tlIntegerKeys) = "L" 
  llUseIntegerPKFields = .f.
 ELSE
  llUseIntegerPKFields = m.tlIntegerKeys
ENDIF

IF NOT VARTYPE(m.tlSkipMBWarningAboutOverwritingDBC) = "L"  
  llSkipWarning = .f.
 ELSE
  llSkipWarning = m.tlSkipMBWarningAboutOverwritingDBC
ENDIF

llFreeMemoAETable = m.tlFreeMemoAETable
llNoBinaryDELETEDTagsInVFP9OrHigher = m.tlNoBinaryDELETEDTagsInVFP9OrHigher
llX8ED = m.tlX8ED    &&& DEVNOTE X8ED

RELEASE ALL EXCEPT l*
CLEAR POPUPS
CLEAR MENUS
CLEAR DLLS
CLOSE ALL
IF m.llSetDBCtoExistingDBC
  IF EMPTY(m.lcCurrentSetDatabaseTo)
    lcCurrentSetDatabaseTo = m.lcAction
  ENDIF
  OPEN DATABASE (m.lcCurrentSetDatabaseTo) EXCLUSIVE
  SET DATABASE TO (m.lcCurrentSetDatabaseTo)
ENDIF
SET TALK OFF
SET SAFETY OFF
SET EXCLUSIVE ON
SET MULTILOCKS ON 
PRIVATE plAbort, plSingleTable
plAbort = .f.
plSingleTable = .f.

RELEASE glUseBinaryDELETEDTagsInVFP9OrHigher
PUBLIC glUseBinaryDELETEDTagsInVFP9OrHigher
IF VARTYPE(m.llNoBinaryDELETEDTagsInVFP9OrHigher) = "L" ;
     AND m.llNoBinaryDELETEDTagsInVFP9OrHigher
  glUseBinaryDELETEDTagsInVFP9OrHigher = .f.
 ELSE
  glUseBinaryDELETEDTagsInVFP9OrHigher = .t.   &&& default behavior
ENDIF  

RELEASE glX8ED
PUBLIC glX8ED
#IF .t.   &&& DEVNOTE X8ED
  glX8ED = .f.   
#ELSE
*
*  coming in a future release of VMP 2005
*  
IF VARTYPE(m.llX8ED) = "L" AND m.llX8ED
  *
  *  use newer X8ED()-based encryption where appropriate
  *
  glX8ED = .t.
 ELSE
  *
  *  use older X3ED()-based encryption where appropriate
  *
  glX8ED = .f.
ENDIF
#ENDIF

DO CASE
  *******************************************
  CASE m.lcSystemTable = "ALL"
  *******************************************
    DO CreateXXData  WITH m.lcAction, m.llSetDBCtoExistingDBC, m.llSkipWarning
    pcDBCDir = DBC()
    pcDBCDir = SUBSTRC(m.pcDBCDir,1,RATC("\",m.pcDBCDir))
    DO CreateXXTable WITH m.lcAction, "SYSTEM"
    DO CreateXXTable WITH m.lcAction, "USERS", m.llUseIntegerPKFields
    DO CreateXXTable WITH m.lcAction, "USERPREFS", m.llUseIntegerPKFields
    DO CreateXXTable WITH m.lcAction, "LOGINHISTORY", m.llUseIntegerPKFields
    DO CreateXXTable WITH m.lcAction, "LOGGEDINUSERS", m.llUseIntegerPKFields
    DO CreateXXTable WITH m.lcAction, "GENPK"
    DO CreateXXTable WITH m.lcAction, "MEMOAE", m.llUseIntegerPKFields, m.llFreeMemoAETable
    DO CreateXXTable WITH m.lcAction, "REPCAT", m.llUseIntegerPKFields
    DO CreateXXTable WITH m.lcAction, "SECURITY", m.llUseIntegerPKFields
    DO CreateXXTable WITH m.lcAction, "RI", m.llUseIntegerPKFields
    DO CreateXXTable WITH m.lcAction, "IUDLOG", m.llUseIntegerPKFields
    DO UpdateSystemStoredProcedures  
	DO AddSPDependentRules WITH "System"
	DO AddSPDependentRules WITH "Users"
	DO AddSPDependentRules WITH "UserPrefs"
	DO AddSPDependentRules WITH "LoginHistory"
	DO AddSPDependentRules WITH "LoggedInUsers"
	DO AddSPDependentRules WITH "GeneratePK"
	DO AddSPDependentRules WITH "MemoAE"
	DO AddSPDependentRules WITH "ReportCatalog"
	DO AddSPDependentRules WITH "Security"
	DO AddSPDependentRules WITH "IUDLog"
    DO UpdateRI
    DO UpdateIUDLog
  *******************************************
  CASE lcSystemTable = "SYSTEM"
  *******************************************
    pcDBCDir = DBC()
    pcDBCDir = SUBSTRC(m.pcDBCDir,1,RATC("\",m.pcDBCDir))
    DO CreateXXData  WITH m.lcAction, m.llSetDBCtoExistingDBC, m.llSkipWarning
    DO CreateXXTable WITH m.lcAction, "SYSTEM"
	DO AddSPDependentRules WITH "System"
  *******************************************
  CASE lcSystemTable = "MIN"
  *******************************************
    pcDBCDir = DBC()
    pcDBCDir = SUBSTRC(m.pcDBCDir,1,RATC("\",m.pcDBCDir))
    DO CreateXXData  WITH m.lcAction, m.llSetDBCtoExistingDBC, m.llSkipWarning
    DO CreateXXTable WITH m.lcAction, "SYSTEM"
    DO CreateXXTable WITH m.lcAction, "USERS", m.llUseIntegerPKFields
    DO CreateXXTable WITH m.lcAction, "RI", m.llUseIntegerPKFields
    DO UpdateSystemStoredProcedures  
	DO AddSPDependentRules WITH "System"
	DO AddSPDependentRules WITH "Users"
    DO UpdateRI
    DO UpdateIUDLog
  *******************************************
  OTHERWISE 
  *******************************************
    plSingleTable = .t.
    IF m.llSetDBCtoExistingDBC
      *
      *  .DBC is already open
      *
     ELSE
      IF FILE(m.lcAction+".DBC")
        OPEN DATABASE (m.lcAction+".DBC") EXCLUSIVE
        SET DATABASE TO (m.lcAction)
       ELSE
        plAbort = .t.
        MESSAGEBOX("Unable to find " + m.lcAction+".DBC", ;
                   1+16, ;
                   "WARNING!") 
      ENDIF
    ENDIF
    IF NOT m.plAbort
      DO CreateXXTable WITH m.lcAction, m.lcSystemTable, m.llUseIntegerPKFields
      DO UpdateSystemStoredProcedures  
      DO AddSPDependentRules WITH m.lcSystemTable
      DO UpdateRI
      DO UpdateIUDLog
    ENDIF
ENDCASE
WAIT CLEAR

IF NOT m.plAbort
  IF "XXFWDATA.FXP" $ UPPER(SYS(16,0))
    *
    *  XXFWDATA was called standalone, not from another program
    *
    MODIFY DATABASE
  ENDIF
ENDIF
IF m.plAbort
  RETURN "ABORT"
 ELSE
  RETURN .t.
ENDIF 
RETURN



******************************************************
PROCEDURE CreateXXData
******************************************************
LPARAMETERS tcAppPrefix, ;
            tlSetDBCtoExistingDBC, ;
            tlSkipMBWarningAboutOverwritingDBC
*
*  create the database
*
IF VARTYPE(m.plAbort) = "L" AND m.plAbort
  RETURN
ENDIF
IF NOT m.tlSetDBCtoExistingDBC
  IF FILE(m.tcAppPrefix+".DBC") 
    *
    * if we already have an existing .DBC, warn the user:
    *
    IF VARTYPE(m.tlSkipMBWarningAboutOverwritingDBC) = "L" ;
         AND m.tlSkipMBWarningAboutOverwritingDBC
     ELSE
      IF MESSAGEBOX("You are about to delete the existing " + ;
                    m.tcAppPrefix+".DBC" + ;
                    ".  Do you want to continue?", ;
                    17, ;
                   "WARNING!") = 2
        plAbort = .t.
        RETURN                  
      ENDIF
    ENDIF
    DELETE DATABASE (m.tcAppPrefix+".DBC") DELETETABLES
  ENDIF
  CREATE DATABASE (m.tcAppPrefix)
  SET DATABASE TO (m.tcAppPrefix)  &&& overkill, yes...
ENDIF
RETURN



******************************************************
PROCEDURE CreateXXTable
******************************************************
LPARAMETERS tcAppPrefix, ;
            tcTableName, ;
            tlUseIntegerPKFields, ;
            tlFreeMemoAETable, ;
            tlIncludeUsr_FKInFreeMemoAETable
IF VARTYPE(m.plAbort) = "L" AND m.plAbort
  RETURN
ENDIF
WAIT WINDOW "Creating " + m.tcAppPrefix + " system tables..." NOWAIT
DO CASE
  CASE m.tcTableName = "SYSTEM"
    DO CreateSystemTables WITH m.tcAppPrefix
  CASE m.tcTableName = "GENPK"
    DO CreateGeneratePKTable WITH m.tcAppPrefix
  CASE m.tcTableName = "USERS"
    DO CreateUsersTable WITH m.tcAppPrefix, m.tlUseIntegerPKFields
  CASE m.tcTableName = "MEMOAE"
    DO CreateMemoAETable WITH m.tcAppPrefix, m.tlFreeMemoAETable, m.tlUseIntegerPKFields, tlIncludeUsr_FKInFreeMemoAETable
  CASE m.tcTableName = "USERPREFS"
    DO CreateUserPrefsTable WITH m.tcAppPrefix, m.tlUseIntegerPKFields
  CASE m.tcTableName = "LOGINHISTORY"
    DO CreateLoginHistoryTable WITH m.tcAppPrefix, m.tlUseIntegerPKFields
  CASE m.tcTableName = "LOGGEDINUSERS"
    DO CreateLoggedInUsersTable WITH m.tcAppPrefix, m.tlUseIntegerPKFields
  CASE m.tcTableName = "REPCAT"
    DO CreateReportCatalogTable WITH m.tcAppPrefix, m.tlUseIntegerPKFields
  CASE m.tcTableName = "SECURITY"
    DO CreateSecurityTables WITH m.tcAppPrefix, m.tlUseIntegerPKFields
  CASE m.tcTableName = "RI"
    DO CreateRITable WITH m.tcAppPrefix, m.tlUseIntegerPKFields
  CASE m.tcTableName = "IUDLOG"
    DO CreateIUDLogTable WITH m.tcAppPrefix, m.tlUseIntegerPKFields
ENDCASE
RETURN



******************************************************
PROCEDURE CreateSystemTables
******************************************************
LPARAMETERS tcAppPrefix

LOCAL lcDatabase, lcDBCDir, xx
lcDBCDir = GetDBCDir()

WAIT WINDOW "AppInfo..." NOWAIT 
*
*  includes a call to any procedural hook you
*  may have to update AppInfo
*
CreateAppInfoTable(m.tcAppPrefix,m.lcDBCDir)


lcDatabase = SET("DATABASE")
SET DATABASE TO 

WAIT WINDOW "AppConfig..." NOWAIT 
*
*  includes a call to any procedural hook you
*  may have to update AppInfo
*
CreateAppConfigTable(m.tcAppPrefix,m.lcDBCDir)

SET DATABASE TO (m.lcDatabase)

IF ADBOBJECTS(laTables,"TABLE") > 0
  FOR xx = 1 TO ALEN(laTables,1)
    IF NOT USED(laTables[m.xx])
      USE (laTables[m.xx]) IN 0
    ENDIF
  ENDFOR
ENDIF

RETURN 





******************************************************
PROCEDURE CreateAppInfoTable
******************************************************
LPARAMETERS tcAppPrefix, tcDBCDir
*
* system data
*
IF FILE(tcDBCDir + tcAppPrefix + "SYSTEM.DBF") ;
     OR INDBC("APPINFO","TABLE")
  IF INDBC("APPINFO","TABLE")
    REMOVE TABLE APPINFO DELETE 
  ENDIF    
ENDIF
CREATE TABLE (tcDBCDir + tcAppPrefix + "SYSTEM") NAME APPINFO ;
     (App_Item            C(40), ;
      App_ItemDataType    C(1), ;
      App_ItemValue       C(254), ;
      App_ItemDescription C(254))
SELECT AppInfo
DBSETPROP("APPINFO",                     "TABLE", "Comment", "Application-level setup/configuration information")
DBSETPROP("APPINFO.App_Item",            "FIELD", "Comment", "Application setup/configuration item (Primary Key-no duplicates)")
DBSETPROP("APPINFO.App_ItemDataType",    "FIELD", "Comment", "Data type for App_Item (C,N,L,D,T)")
DBSETPROP("APPINFO.App_ItemValue",       "FIELD", "Comment", "App_Item value")
DBSETPROP("APPINFO.App_ItemDescription", "FIELD", "Comment", "Description of App_Item")

*
*  procedural hook for your code to update the
*  default VMP AppInfo table -- there is another
*  procedural hook at the end of this procedure
*
*  note that CreateAppInfoTableHook1 may be a standalone
*  .PRG/program OR a local procedure/function in the
*  calling "wrapper" program
*
*  NOTE that if CreateAppInfoTableHook1 is a local
*  procedure/function in the calling "wrapper" program,
*  any errors generated there are CATCHed here UNLESS
*  you put your own TRY/CATCH block in your
*  CreateAppInfoTableHook1
*
IF FILE("CreateAppInfoTableHook1.PRG") ;
    OR FILE("CreateAppInfoTableHook1.FXP")
  *
  *  CreateAppInfoTableHook1 is a standalone program
  *
  DO ("CreateAppInfoTableHook1")
 ELSE
  *
  *  there is no CreateAppInfoTableHook1 standalone
  *  program, so we'll try to DO it, do executed
  *  it if there is a procedure/function of that
  *  name available in the calling stack
  *
  TRY
  DO ("CreateAppInfoTableHook1")
  CATCH TO oEx
    IF oEx.ErrorNo = 1 AND "CREATEAPPINFOTABLEHOOK1" $ UPPER(oEx.Message)
      *
      *  there is no CreateAppInfoTableHook1, 
      *  nothing to do
      *
     ELSE
      *
      *  an error occurred in CreateAppInfoTableHook1
      *  that was not specifically CATCHed there
      *
      ASSERT .f. message ;
           "An error occurred in CreateAppInfoTableHook1, " + ;
           "but was not CATCHed there -- oEx contains the " + ;
           "error information.  You can select <Debug> here " + ;
           "and put the oEx object in the Debugger."
    ENDIF
  ENDTRY
ENDIF

*
*  another procedural hook for your code, same as 
*  CreateAppInfoTableHook1 above, except that we
*  pass the tcAppPrefix, tcDBCDir to 
*  CreateAppInfoTableHook1 (if Drew had realized
*  that it would be useful to pass the parameters
*  on to the hook program, then he wouldn't have
*  had to add this 2nd hook in the 2005.03.18.03
*  build)
*
*  NOTE that tcAppPrefix can be passed here as "XX"
*  from places like the Load of the VMP Application
*  Setup Wizard, so your hook should take into
*  account that possibility, and probably immediately
*  RETURN if tcAppPrefix is received as "XX"
*  
IF FILE("CreateAppInfoTableHook1a.PRG") ;
    OR FILE("CreateAppInfoTableHook1a.FXP")
  *
  *  CreateAppInfoTableHook1a is a standalone program
  *
  DO ("CreateAppInfoTableHook1a") WITH m.tcAppPrefix, m.tcDBCDir
 ELSE
  *
  *  there is no CreateAppInfoTableHook1a standalone
  *  program, so we'll try to DO it, do executed
  *  it if there is a procedure/function of that
  *  name available in the calling stack
  *
  TRY
  DO ("CreateAppInfoTableHook1a") WITH m.tcAppPrefix, m.tcDBCDir
  CATCH TO oEx
    IF oEx.ErrorNo = 1 AND "CREATEAPPINFOTABLEHOOK1A" $ UPPER(oEx.Message)
      *
      *  there is no CreateAppInfoTableHook1a, 
      *  nothing to do
      *
     ELSE
      *
      *  an error occurred in CreateAppInfoTableHook1a
      *  that was not specifically CATCHed there
      *
      ASSERT .f. message ;
           "An error occurred in CreateAppInfoTableHook1a, " + ;
           "but was not CATCHed there -- oEx contains the " + ;
           "error information.  You can select <Debug> here " + ;
           "and put the oEx object in the Debugger."
    ENDIF
  ENDTRY
ENDIF

SELECT AppInfo 

m.App_Item            = "---" + "WELCOME to APPINFO!" + REPLICATE("-",20)
m.App_ItemDataType    = SPACE(0)
m.App_ItemValue       = SPACE(0)
m.App_ItemDescription = "AppInfo contains settings that you may allow the user/client to configure in an interface you provide, hence AppInfo is in your main database and available for modification (you can include AppConfig right into your .EXE, making it read-only)."
INSERT INTO APPINFO FROM MEMVAR

m.App_Item            = "---" + "PRODUCTION RUNTIME SETTINGS" + REPLICATE("-",20)
m.App_ItemDataType    = SPACE(0)
m.App_ItemValue       = SPACE(0)
m.App_ItemDescription = SPACE(0) 
INSERT INTO APPINFO FROM MEMVAR

m.App_Item            = "FreeConfigTable"
m.App_ItemDataType    = "C"
m.App_ItemValue       = tcAppPrefix + "CONFIG.DBF"
m.App_ItemDescription = "REQUIRED:  Filename of the APPCONFIG free table containing app information (extension is required but doesn't have to be .DBF)"
INSERT INTO APPINFO FROM MEMVAR

m.App_Item            = "---" + "CLIENT-CONFIGURABLE SETTINGS" + REPLICATE("-",20)
m.App_ItemDataType    = SPACE(0)
m.App_ItemValue       = SPACE(0)
m.App_ItemDescription = SPACE(0) 
INSERT INTO APPINFO FROM MEMVAR

m.App_Item            = "SetPathProd"
m.App_ItemDataType    = "C"
m.App_ItemValue       = SPACE(0)
m.App_ItemDescription = "VFP path to be set in application setup -- Production"
INSERT INTO APPINFO FROM MEMVAR
m.App_Item            = "TimeoutSeconds"
m.App_ItemDataType    = "N"
m.App_ItemValue       = SPACE(0)
m.App_ItemDescription = "Number of seconds of user inactivity before timeout"
INSERT INTO APPINFO FROM MEMVAR
m.App_Item            = "AppTimerInterval"
m.App_ItemDataType    = "N"
m.App_ItemValue       = SPACE(0)
m.App_ItemDescription = "Interval for the application timer (in milliseconds) - interval >= 5 minutes (300000 milliseconds) recommended"
INSERT INTO APPINFO FROM MEMVAR
m.App_Item            = "RefreshFormsOnUpdate"
m.App_ItemDataType    = "N"
m.App_ItemValue       = "1"
m.App_ItemDescription = "Determines if oForms.RefreshFormsOnUpdate() messages open forms RemoteRefresh() -- <Save>s can be much slower if oForms.RefreshFormsOnUpdate() finds many forms whose RemoteRefresh() is called, esp. if those forms requery views.  0=.F., 1/default=.T." 
INSERT INTO APPINFO FROM MEMVAR
m.App_Item            = "MinimumPasswordLength"
m.App_ItemDataType    = "N"
m.App_ItemValue       = "6"
m.App_ItemDescription = "Minimum length for the user Password, enforced in framework forms that maintain User information"
INSERT INTO APPINFO FROM MEMVAR
m.App_Item            = "RequiredControlBackColor"
m.App_ItemDataType    = "E"
m.App_ItemValue       = "RGB(0,255,255)"
m.App_ItemDescription = [BackColor for form controls whose custom VMP ilRequired property is set to .T., in a default VMP cusRequiredControl implementation in xxxBase.ilRequired_Assign - XXDTES("XXFW.VCX","BackColor","cusForms","SeticRequiredControlBehaviorClass")]
INSERT INTO APPINFO FROM MEMVAR
m.App_Item            = "RequiredControlBehaviorClass"
m.App_ItemDataType    = "C"
m.App_ItemValue       = "cusRequiredControl,XXFWControlBehaviors.VCX"
m.App_ItemDescription = [ClasssName,VCXFileName instantiated in oForms.SetupRequiredControlBehavior() to oForms.ioRequiredControlBehavior and called by xxxBase.ilRequired_Assign to determine the (typically look-and-feel) behavior of form controls whose ilRequired is set to .T.]
INSERT INTO APPINFO FROM MEMVAR
m.App_Item            = "TrackLoggedInUsers"
m.App_ItemDataType    = "L"
m.App_ItemValue       = ".F."
m.App_ItemDescription = "Indicates whether this app should 'track currently-logged-in-users' via the optional LoggedInUsers table.  If set .T., then the LoggedInUsers table must exist in the .DBC containing the VMP optional security tables/views"
INSERT INTO APPINFO FROM MEMVAR
m.App_Item            = "DaysAfterWhichPurgeLoginHistory"
m.App_ItemDataType    = "N"
m.App_ItemValue       = "60"
m.App_ItemDescription = "Specifies the age in days after which LoginHistory records are automatically purged on app startup.  Ignored if the LoginHistory table (and therefore its optional features) does not exist.  If this record is missing/invalid, no auto-purge happens."
INSERT INTO APPINFO FROM MEMVAR
m.App_Item            = "SQLThermoXXRQUERYBehavior"
m.App_ItemDataType    = "N"
m.App_ItemValue       = "0"
m.App_ItemDescription = "Specifies whether the SQL Thermo progress bar is displayed in XXRQUERY() for long queries.  0=NO/Default, 1=YES"
INSERT INTO APPINFO FROM MEMVAR

m.App_Item            = "---" + "REMOTE DATA SETTINGS" + REPLICATE("-",20)
m.App_ItemDataType    = SPACE(0)
m.App_ItemValue       = SPACE(0)
m.App_ItemDescription = SPACE(0) 
INSERT INTO APPINFO FROM MEMVAR

m.App_Item            = "BusyConnectionTimeout"
m.App_ItemDataType    = "N"
m.App_ItemValue       = "2"
m.App_ItemDescription = "Establishes oLib.oConnectionSvc.ilBusyConnectionTimeout (in seconds), used in XXFWLIBS.VCX/cusConnectionSvc.BusyConnection() to specify how long to try a busy connection before giving up.  See also XXFWLIBS.VCX/cusConnectionSvc.SetProperties()"
INSERT INTO APPINFO FROM MEMVAR
m.App_Item            = "BusyConnectionRetryInterval"
m.App_ItemDataType    = "N"
m.App_ItemValue       = ".2"
m.App_ItemDescription = "Establishes oLib.oConnectionSvc.ilBusyConnectionRetryInterval (in seconds), used in XXFWLIBS.VCX/cusConnectionSvc.BusyConnection() to specify how long between checks on a busy connection.  See also XXFWLIBS.VCX/cusConnectionSvc.SetProperties()"
INSERT INTO APPINFO FROM MEMVAR
m.App_Item            = "QueryAllowUserToRetryWhenBusy"
m.App_ItemDataType    = "L"
m.App_ItemValue       = ".F."
m.App_ItemDescription = "Establishes oLib.oConnectionSvc.ilQueryAllowUserToRetryWhenBusy, used as the default value of the optional 3rd parameter of XXRQUERY() and XXSQLEXE() functions.  See also XXFWLIBS.VCX/cusConnectionSvc.SetProperties()"
INSERT INTO APPINFO FROM MEMVAR
m.App_Item            = "QueryPresentErrorMessage"
m.App_ItemDataType    = "L"
m.App_ItemValue       = ".F."
m.App_ItemDescription = "Establishes oLib.oConnectionSvc.ilQueryPresentErrorMessage, used as the default value of the optional 4th parameter of XXRQUERY() and XXSQLEXE() functions.  See also XXFWLIBS.VCX/cusConnectionSvc.SetProperties()"
INSERT INTO APPINFO FROM MEMVAR
m.App_Item            = "QueryShutdownIfProblem"
m.App_ItemDataType    = "L"
m.App_ItemValue       = ".F."
m.App_ItemDescription = "Establishes oLib.oConnectionSvc.ilQueryShutdownIfProblem, used as the default value of the optional 5th parameter of XXRQUERY() and XXSQLEXE() functions.  See also XXFWLIBS.VCX/cusConnectionSvc.SetProperties()"
INSERT INTO APPINFO FROM MEMVAR
m.App_Item            = "CheckHandleOnEveryXXSQLEXE_XXRQUERY"
m.App_ItemDataType    = "N"
m.App_ItemValue       = "1"
m.App_ItemDescription = "Checked in XXSQLEXE.PRG/XXRQUERY.PRG, set this value to 0 to suppress the default (1 setting) behavior, where the connection handle is checked for validity each time XXSQLEXE() or XXRQUERY() run."
INSERT INTO APPINFO FROM MEMVAR
m.App_Item            = "TestConnectionSQL"
m.App_ItemDataType    = "C"
m.App_ItemValue       = SPACE(0)
m.App_ItemDescription = "SQL-Select statement that retrieves one-record/one-field as fast as possible, for checking valid connections in XXFWLIBS.VCX/cusConnectionSvc.TestConnection()"
INSERT INTO APPINFO FROM MEMVAR

m.App_Item            = "---" + "INTL SETTINGS" + REPLICATE("-",20)
m.App_ItemDataType    = SPACE(0)
m.App_ItemValue       = SPACE(0)
m.App_ItemDescription = SPACE(0) 
INSERT INTO APPINFO FROM MEMVAR

m.App_Item            = "SetupINTL"
m.App_ItemDataType    = "L"
m.App_ItemValue       = ".F."
m.App_ItemDescription = "Setup/install INTL for this application?"
INSERT INTO APPINFO FROM MEMVAR
m.App_Item            = "INTLLanguage"
m.App_ItemDataType    = "C"
m.App_ItemValue       = "Original"
m.App_ItemDescription = [If the "SetupINTL" record exists and is set to ".T.", this record indicates the language during app startup before user login]
INSERT INTO APPINFO FROM MEMVAR
m.App_Item            = "INTLClass"
m.App_ItemDataType    = "C"
m.App_ItemValue       = ""
m.App_ItemDescription = 'If the "SetupINTL" record exists and is set to ".T.", this record indicates the ClassName[,.VCX/.PRG] of the oINTL object.  Defaults to "INTL,INTL.PRG"'
INSERT INTO APPINFO FROM MEMVAR

m.App_Item            = "---" + "NON-VMP APP-SPECIFIC ITEMS--------" + REPLICATE("-",20)
m.App_ItemDataType    = SPACE(0)
m.App_ItemValue       = SPACE(0)
m.App_ItemDescription = "(your stuff from here on...)"
INSERT INTO APPINFO FROM MEMVAR

SELECT AppInfo

*
*  procedural hook for your code to update the
*  default VMP AppInfo table -- there is another
*  procedural hook at the beginning of this procedure
*
*  note that CreateAppInfoTableHook2 may be a standalone
*  .PRG/program OR a local procedure/function in the
*  calling "wrapper" program
*
*  NOTE that if CreateAppInfoTableHook2 is a local
*  procedure/function in the calling "wrapper" program,
*  any errors generated there are CATCHed here UNLESS
*  you put your own TRY/CATCH block in your
*  CreateAppInfoTableHook2
*
IF FILE("CreateAppInfoTableHook2.PRG") ;
    OR FILE("CreateAppInfoTableHook2.FXP")
  *
  *  CreateAppInfoTableHook2 is a standalone program
  *
  DO ("CreateAppInfoTableHook2")
 ELSE
  *
  *  there is no CreateAppInfoTableHook2 standalone
  *  program, so we'll try to DO it, do executed
  *  it if there is a procedure/function of that
  *  name available in the calling stack
  *
  TRY
  DO ("CreateAppInfoTableHook2")
  CATCH TO oEx
    IF oEx.ErrorNo = 1 AND "CREATEAPPINFOTABLEHOOK2" $ UPPER(oEx.Message)
      *
      *  there is no CreateAppInfoTableHook2, 
      *  nothing to do
      *
     ELSE
      *
      *  an error occurred in CreateAppInfoTableHook2
      *  that was not specifically CATCHed there
      *
      ASSERT .f. message ;
           "An error occurred in CreateAppInfoTableHook2, " + ;
           "but was not CATCHed there -- oEx contains the " + ;
           "error information.  You can select <Debug> here " + ;
           "and put the oEx object in the Debugger."
    ENDIF
  ENDTRY
ENDIF

*
*  another procedural hook for your code, same as 
*  CreateAppInfoTableHook2 above, except that we
*  pass the tcAppPrefix, tcDBCDir to 
*  CreateAppInfoTableHook2 (if Drew had realized
*  that it would be useful to pass the parameters
*  on to the hook program, then he wouldn't have
*  had to add this 2nd hook in the 2005.03.18.03
*  build)
*  
*  NOTE that tcAppPrefix can be passed here as "XX"
*  from places like the Load of the VMP Application
*  Setup Wizard, so your hook should take into
*  account that possibility, and probably immediately
*  RETURN if tcAppPrefix is received as "XX"
*  
IF FILE("CreateAppInfoTableHook2a.PRG") ;
    OR FILE("CreateAppInfoTableHook2a.FXP")
  *
  *  CreateAppInfoTableHook2a is a standalone program
  *
  DO ("CreateAppInfoTableHook2a") WITH m.tcAppPrefix, m.tcDBCDir
 ELSE
  *
  *  there is no CreateAppInfoTableHook2a standalone
  *  program, so we'll try to DO it, do executed
  *  it if there is a procedure/function of that
  *  name available in the calling stack
  *
  TRY
  DO ("CreateAppInfoTableHook2a") WITH m.tcAppPrefix, m.tcDBCDir
  CATCH TO oEx
    IF oEx.ErrorNo = 1 AND "CREATEAPPINFOTABLEHOOK2A" $ UPPER(oEx.Message)
      *
      *  there is no CreateAppInfoTableHook2a, 
      *  nothing to do
      *
     ELSE
      *
      *  an error occurred in CreateAppInfoTableHook2a
      *  that was not specifically CATCHed there
      *
      ASSERT .f. message ;
           "An error occurred in CreateAppInfoTableHook2a, " + ;
           "but was not CATCHed there -- oEx contains the " + ;
           "error information.  You can select <Debug> here " + ;
           "and put the oEx object in the Debugger."
    ENDIF
  ENDTRY
ENDIF

SELECT AppInfo

RETURN 




*********************************************************
PROCEDURE CreateAppConfigTable
*********************************************************
*
*  xxCONFIG.DBF is a free table
*
*
LPARAMETERS tcAppPrefix, tcDir

LOCAL lcTable,lcDir

IF NOT EMPTY(SET("DATABASE"))
  SET DATABASE TO
ENDIF

IF NOT VARTYPE(tcDir) = "C" OR EMPTY(tcDir)
  lcDir = FULLPATH(CURDIR())
 ELSE
  lcDir = UPPER(ALLTRIM(tcDir))
ENDIF
lcTable = lcDir + tcAppPrefix + "CONFIG"

* CREATE (free) TABLE will overwrite any existing ??Config.DBF
CREATE TABLE (lcTable) ;
     (Ap_Item    C(40), ;
      Ap_ItemDTp C(1), ;
      Ap_ItemVal C(254),;
      Ap_ItemDsc C(254))

USE 
USE (lcTable) IN 0 ALIAS AppConfig
SELECT AppConfig

*
*  procedural hook for your code to update the
*  default VMP AppConfig table -- there is another
*  procedural hook at the end of this procedure
*
*  note that CreateAppConfigTableHook1 may be a standalone
*  .PRG/program OR a local procedure/function in the
*  calling "wrapper" program
*
*  NOTE that if CreateAppConfigTableHook1 is a local
*  procedure/function in the calling "wrapper" program,
*  any errors generated there are CATCHed here UNLESS
*  you put your own TRY/CATCH block in your
*  CreateAppConfigTableHook1
*
IF FILE("CreateAppConfigTableHook1.PRG") ;
    OR FILE("CreateAppConfigTableHook1.FXP")
  *
  *  CreateAppConfigTableHook1 is a standalone program
  *
  DO ("CreateAppConfigTableHook1")
 ELSE
  *
  *  there is no CreateAppConfigTableHook1 standalone
  *  program, so we'll try to DO it, do executed
  *  it if there is a procedure/function of that
  *  name available in the calling stack
  *
  TRY
  DO ("CreateAppConfigTableHook1") 
  CATCH TO oEx
    IF oEx.ErrorNo = 1 AND "CREATEAPPCONFIGTABLEHOOK1" $ UPPER(oEx.Message)
      *
      *  there is no CreateAppConfigTableHook1, 
      *  nothing to do
      *
     ELSE
      *
      *  an error occurred in CreateAppConfigTableHook1
      *  that was not specifically CATCHed there
      *
      ASSERT .f. message ;
           "An error occurred in CreateAppConfigTableHook1, " + ;
           "but was not CATCHed there -- oEx contains the " + ;
           "error information.  You can select <Debug> here " + ;
           "and put the oEx object in the Debugger."
    ENDIF
  ENDTRY
ENDIF

*
*  another procedural hook for your code, same as 
*  CreateAppConfigTableHook1 above, except that we
*  pass the tcAppPrefix, tcDBCDir to 
*  CreateAppConfigTableHook1 (if Drew had realized
*  that it would be useful to pass the parameters
*  on to the hook program, then he wouldn't have
*  had to add this 2nd hook in the 2005.03.18.03
*  build)
*  
*  NOTE that tcAppPrefix can be passed here as "XX"
*  from places like the Load of the VMP Application
*  Setup Wizard, so your hook should take into
*  account that possibility, and probably immediately
*  RETURN if tcAppPrefix is received as "XX"
*  
IF FILE("CreateAppConfigTableHook1a.PRG") ;
    OR FILE("CreateAppConfigTableHook1a.FXP")
  *
  *  CreateAppConfigTableHook1a is a standalone program
  *
  DO ("CreateAppConfigTableHook1a") WITH m.tcAppPrefix, m.tcDir
 ELSE
  *
  *  there is no CreateAppConfigTableHook1a standalone
  *  program, so we'll try to DO it, do executed
  *  it if there is a procedure/function of that
  *  name available in the calling stack
  *
  TRY
  DO ("CreateAppConfigTableHook1a") WITH m.tcAppPrefix, m.tcDir
  CATCH TO oEx
    IF oEx.ErrorNo = 1 AND "CREATEAPPCONFIGTABLEHOOK1A" $ UPPER(oEx.Message)
      *
      *  there is no CreateAppConfigTableHook1a, 
      *  nothing to do
      *
     ELSE
      *
      *  an error occurred in CreateAppConfigTableHook1a
      *  that was not specifically CATCHed there
      *
      ASSERT .f. message ;
           "An error occurred in CreateAppConfigTableHook1a, " + ;
           "but was not CATCHed there -- oEx contains the " + ;
           "error information.  You can select <Debug> here " + ;
           "and put the oEx object in the Debugger."
    ENDIF
  ENDTRY
ENDIF

SELECT AppConfig

*
*  Ap_Item, like APPINFO.App_Info, must contain
*  unique values, as if it's a PK/CK
*      

m.Ap_Item    = "---" + "WELCOME to AppConfig!" + REPLICATE("-",20)
m.Ap_ItemDTp = SPACE(0)
m.Ap_ItemVal = SPACE(0)
m.Ap_ItemDsc = "AppConfig contains settings not expected to be user/client-specified, and AppConfig can therefore be built into your .EXE and not updateable by your user/client."
INSERT INTO AppConfig FROM MEMVAR

m.Ap_Item    = "---" + "PRODUCTION RUNTIME SETTINGS" + REPLICATE("-",20)
m.Ap_ItemDTp = SPACE(0)
m.Ap_ItemVal = SPACE(0)
m.Ap_ItemDsc = SPACE(0) 
INSERT INTO AppConfig FROM MEMVAR

m.Ap_Item    = "MainDBC"
m.Ap_ItemDTp = "C"
m.Ap_ItemVal = tcAppPrefix
m.Ap_ItemDsc = [REQUIRED:  Name of the main VMP database, containing AppInfo/xxSYSTEM.DBF -- this .DBC must be in the path when the app is run, or else specified here with the full drive+path.  Can be set to !NONE!, indicating this app uses NO main VMP database]
INSERT INTO AppConfig FROM MEMVAR
m.Ap_Item    = "SetCenturyToRollover"
m.Ap_ItemDTp = "N"
m.Ap_ItemVal = "50"
m.Ap_ItemDsc = "OPTIONAL:  specify the ROLLOVER year of SET CENTURY TO <ExpN> ROLLOVER <ExpN>.  If not specified (this record is missing), your app uses the default VFP behavior"
INSERT INTO AppConfig FROM MEMVAR

m.Ap_Item    = "---" + "GLOBAL APPLICATION OBJECTS" + REPLICATE("-",20)
m.Ap_ItemDTp = SPACE(0)
m.Ap_ItemVal = SPACE(0)
m.Ap_ItemDsc = SPACE(0) 
INSERT INTO AppConfig FROM MEMVAR

m.Ap_Item    = "oAppConfigClass"
m.Ap_ItemDTp = "C"
m.Ap_ItemVal = "cusSettingsVFPTable,XXFWUTIL.VCX"
m.Ap_ItemDsc = [Name of the class instantiated to "oAppConfig" by XXFWMAIN.PRG.  XXDTES("XXFWMAIN.PRG","oAppConfig = CREATEOBJECT"]
INSERT INTO AppConfig FROM MEMVAR
m.Ap_Item    = "oLibClass"
m.Ap_ItemDTp = "C"
m.Ap_ItemVal = "cusAppLibs,XXFW.VCX"
m.Ap_ItemDsc = [Name of the class instantiated to "oLib" by XXFWMAIN.PRG.  XXFWLIBS.VCX concrete classes (like oDBCSvc) are auto-added, to add your "library" .VCXs, and add a "|" pipe separator and any number of pipe-separated VCXFilenames.  See XXFW.VCX/cusAppLibs::Init]
INSERT INTO AppConfig FROM MEMVAR
m.Ap_Item    = "oAppClass"
m.Ap_ItemDTp = "C"
m.Ap_ItemVal = "ctrApp,XXFW.VCX" 
m.Ap_ItemDsc = [Name of the class instantiated to "oApp" by XXFWMAIN.PRG.  Best results if contained in a .VCX, not a .PRG.  If absent or left empty, defaults to "ctrApp,XXFW.VCX"]
INSERT INTO AppConfig FROM MEMVAR
m.Ap_Item    = "oAppInfoClass"
m.Ap_ItemDTp = "C"
m.Ap_ItemVal = "cusSettingsVFPTable,XXFWUTIL.VCX"
m.Ap_ItemDsc = [Name of the class instantiated to "oAppInfo" by oApp.CreateoAppInfoObject().  XXDTES("XXFW.VCX",0,"ctrApp","CreateoAppInfoObject"]
INSERT INTO AppConfig FROM MEMVAR
m.Ap_Item    = "oUserClass"
m.Ap_ItemDTp = "C"
m.Ap_ItemVal = "cusUser,XXFW.VCX" 
m.Ap_ItemDsc = [Name of the class instantiated to "oUser" by XXFWMAIN.PRG.  Best results if contained in a .VCX, not a .PRG.  If absent or left empty, defaults to "cusUser,XXFW.VCX"]
INSERT INTO AppConfig FROM MEMVAR
m.Ap_Item    = "oFormsClass"
m.Ap_ItemDTp = "C"
m.Ap_ItemVal = "cusForms,XXFW.VCX" 
m.Ap_ItemDsc = [Name of the class instantiated to "oForms" by XXFWMAIN.PRG.  Best results if contained in a .VCX, not a .PRG.  If absent or left empty, defaults to "cusForms,XXFW.VCX"]
INSERT INTO AppConfig FROM MEMVAR
m.Ap_Item    = "oToolbarsClass"
m.Ap_ItemDTp = "C"
m.Ap_ItemVal = "cusToolbars,XXFW.VCX" 
m.Ap_ItemDsc = [Name of the class instantiated to "oToolbars" by XXFWMAIN.PRG.  Best results if contained in a .VCX, not a .PRG.  If absent or left empty, defaults to "cusToolbars,XXFW.VCX"]
INSERT INTO AppConfig FROM MEMVAR
m.Ap_Item    = "oMenuClass"
m.Ap_ItemDTp = "C"
m.Ap_ItemVal = "cusMenu,XXFW.VCX" 
m.Ap_ItemDsc = [Name of the class instantiated to "oMenu" by XXFWMAIN.PRG.  Best results if contained in a .VCX, not a .PRG.  If absent or left empty, defaults to "cusMenu,XXFW.VCX"]
INSERT INTO AppConfig FROM MEMVAR

m.Ap_Item    = "---" + "APP CONFIGURATION ITEMS" + REPLICATE("-",20)
m.Ap_ItemDTp = SPACE(0)
m.Ap_ItemVal = SPACE(0)
m.Ap_ItemDsc = SPACE(0) 
INSERT INTO AppConfig FROM MEMVAR

m.Ap_Item    = "Title"
m.Ap_ItemDTp = "C"
m.Ap_ItemVal = tcAppPrefix + " Application"
m.Ap_ItemDsc = "REQUIRED:  String for _Screen.Caption and for use in messaging"
INSERT INTO AppConfig FROM MEMVAR
m.Ap_Item    = "SplashScreen"
m.Ap_ItemDTp = "C"
m.Ap_ItemVal = SPACE(0)
m.Ap_ItemDsc = "<.BMP filename>, <.JPG filename>, <.GIF filename>, <.SCX filename>, or <SplashScreenFormClassName,.VCX filename> indicating the initial splash screen on app startup.  If absent or left empty, defaults to no splash screen on startup"
INSERT INTO AppConfig FROM MEMVAR
m.Ap_Item    = "IconFile"
m.Ap_ItemDTp = "C"
m.Ap_ItemVal = SPACE(0)
m.Ap_ItemDsc = ".ICO file for the application, used as _Screen.Icon, etc."
INSERT INTO AppConfig FROM MEMVAR
m.Ap_Item    = "ScreenWallpaperFile"
m.Ap_ItemDTp = "C"
m.Ap_ItemVal = SPACE(0)
m.Ap_ItemDsc = ".BMP/.GIF/.JPG file for use as _Screen wallpaper"
INSERT INTO AppConfig FROM MEMVAR
m.Ap_Item    = "ScreenHandlerClass"
m.Ap_ItemDTp = "C"
m.Ap_ItemVal = SPACE(0)
m.Ap_ItemDsc = "ClassName,VCXorPRGFileName of the custom screen handler class instantiated to oApp.ioScreenHandler in oApp::SetupScreen().  Default (leave Ap_ItemVal blank) is that oApp is stored to oApp.ioScreenHandler.  See XXFW.VCX/ctrApp::SetupScreen() comments"
INSERT INTO AppConfig FROM MEMVAR
m.Ap_Item    = "DefaultReportClass"
m.Ap_ItemDTp = "C"
m.Ap_ItemVal = "frmReport,XXFWMISC.VCX"
m.Ap_ItemDsc = "REQUIRED (if your application has VMP-based reports):  [ClassName,.VCXFileName] for the default report object class"
INSERT INTO AppConfig FROM MEMVAR
m.Ap_Item    = "SetProcedure"
m.Ap_ItemDTp = "C"
m.Ap_ItemVal = SPACE(0)
m.Ap_ItemDsc = "List of procedure files to SET PROCEDURE TO during app setup"
INSERT INTO AppConfig FROM MEMVAR
m.Ap_Item    = "SetClasslib"
m.Ap_ItemDTp = "C"
m.Ap_ItemVal = "" 
m.Ap_ItemDsc = "List of class library .VCX files to SET CLASSLIB TO during app setup"
INSERT INTO AppConfig FROM MEMVAR
m.Ap_Item    = "SetLibrary"
m.Ap_ItemDTp = "C"
m.Ap_ItemVal = SPACE(0)
m.Ap_ItemDsc = "List of API library files to SET LIBRARY TO during app setup"
INSERT INTO AppConfig FROM MEMVAR
m.Ap_Item    = "ErrorLogDirectory"
m.Ap_ItemDTp = "C"
m.Ap_ItemVal = SPACE(0)
m.Ap_ItemDsc = "Directory (in the path) where ERRORLOG.DBF should be created/maintained -- defaults to the current directory when the error is encountered"
INSERT INTO AppConfig FROM MEMVAR
m.Ap_Item    = "InitialMainMenu" 
m.Ap_ItemDTp = "C"
m.Ap_ItemVal = SPACE(0)
m.Ap_ItemDsc = [If your initial main menu .MPR isn't the default "??MAIN.MPR" and you don't want to subclass XXFW.VCX/cusMenu just to specify icInitialMPR, set this Ap_ItemVal to the filename of your initial main menu .MPR.  Specify "!NONE!" for no initial .MPR menu.]
INSERT INTO AppConfig FROM MEMVAR
m.Ap_Item    = "SetupSDT"
m.Ap_ItemDTp = "L"
m.Ap_ItemVal = ".F."
m.Ap_ItemDsc = "Setup/install Stonefield Database Toolkit for this application?  See XXFW.VCX/ctrApp.SetupExternalToolSDT()."
INSERT INTO AppConfig FROM MEMVAR
m.Ap_Item    = "SetHelpToFile"
m.Ap_ItemDTp = "C"
m.Ap_ItemVal = SPACE(0)
m.Ap_ItemDsc = "Name of the Help file, if any, to be installed in oApp.SetupHelp() via SET HELP TO"
INSERT INTO AppConfig FROM MEMVAR
m.Ap_Item    = "Install_oLib_oXMLDOM"  
m.Ap_ItemDTp = "L"
m.Ap_ItemVal = ".F."
m.Ap_ItemDsc = "During oLib instantiation, should MSXML.DLL/Microsoft.XMLDOM be installed to oLib.oXMLDOM (if not, then ISNULL(oLib.oXMLDOM) at runtime) -- see XXFW.VCX/cusAppLibs.AddMemberXMLDOM()"
INSERT INTO AppConfig FROM MEMVAR
m.Ap_Item    = "Install_oLib_oWWXML"   
m.Ap_ItemDTp = "L"
m.Ap_ItemVal = ".F."
m.Ap_ItemDsc = "During oLib instantiation, should WWXML.VCX/wwXML be installed to oLib.oWWXML (if not, then ISNULL(oLib.oWWXML) at runtime) -- see XXFW.VCX/cusAppLibs.AddMemberWWXML()"
INSERT INTO AppConfig FROM MEMVAR
m.Ap_Item    = "PaPhFactor"   
m.Ap_ItemDTp = "C"
m.Ap_ItemVal = ""   &&& coming in a future release of VMP 2005
m.Ap_ItemDsc = [XXDTES("XXFWED.VCX","PaPhFactor","cusED","zReadMe")]
INSERT INTO AppConfig FROM MEMVAR
m.Ap_Item    = "StBlFactor"   
m.Ap_ItemDTp = "C"
m.Ap_ItemVal = ""   &&& coming in a future release of VMP 2005
m.Ap_ItemDsc = [XXDTES("XXFWED.VCX","StBlFactor","cusED","zReadMe")]
INSERT INTO AppConfig FROM MEMVAR

m.Ap_Item    = "---" + "USER LOGIN ITEMS" + REPLICATE("-",20)
m.Ap_ItemDTp = SPACE(0)
m.Ap_ItemVal = SPACE(0)
m.Ap_ItemDsc = SPACE(0) 
INSERT INTO AppConfig FROM MEMVAR

m.Ap_Item    = "UserLoginFormClass"
m.Ap_ItemDTp = "C"
m.Ap_ItemVal = "frmLogin,XXFW.VCX"
m.Ap_ItemDsc = [Specify the 'ClassName,VCXFilename' for the user login form here instead of subclassing XXFW.VCX/cusUser just to set its icUserLoginFormClassDefinition property.  Can optionally be set to an .SCX filename.]
INSERT INTO AppConfig FROM MEMVAR
m.Ap_Item    = "LoginControl"
m.Ap_ItemDTp = "C"
m.Ap_ItemVal = "AUTO"
m.Ap_ItemDsc = [Type of auto-login control desired:  "AUTO", "NORMAL", "TRUSTED", or "NONE".  "AUTO" is only available in development, see XXFW.VCX/cusUser::zReadMe() for all the details]
INSERT INTO AppConfig FROM MEMVAR
m.Ap_Item    = "AutoLoginUserID"
m.Ap_ItemDTp = "C"
m.Ap_ItemVal = "DEFAULT"
m.Ap_ItemDsc = "User ID to use for auto-login"
INSERT INTO AppConfig FROM MEMVAR
m.Ap_Item    = "AutoLoginPassword"
m.Ap_ItemDTp = "C"
m.Ap_ItemVal = "PASSWORD"
m.Ap_ItemDsc = "Password to use for auto-login"
INSERT INTO AppConfig FROM MEMVAR
m.Ap_Item    = "TrustedLoginActionOnFailure"
m.Ap_ItemDTp = "N"
m.Ap_ItemVal = "0"
m.Ap_ItemDsc = "Indicates what action to take when a TRUSTED login fails:  0=typical denial of access (default), 1=allow manual NORMAL login"
INSERT INTO AppConfig FROM MEMVAR
m.Ap_Item    = "MultipleInstancesOneWorkstation"
m.Ap_ItemDTp = "N"
m.Ap_ItemVal = "0"
m.Ap_ItemDsc = "Specifies what happens when the user attempts to launch a 2nd instance of the app from one workstation:  0/default=deny 2nd instance, bring existing 1st instance on top; 1=deny 2nd instance, alert user via MESSAGEBOX(); 2=allow multiple instances"
INSERT INTO AppConfig FROM MEMVAR
m.Ap_Item    = "MultipleWorkstationLogin"
m.Ap_ItemDTp = "N"
m.Ap_ItemVal = "1"
m.Ap_ItemDsc = [Specifies what happens when the user has already logged in from one workstation, and then attempts to login again from another workstation.  0=Prevent login from more than one workstation at a time; 1=Permit login from additional workstations (default)]
INSERT INTO AppConfig FROM MEMVAR
m.Ap_Item    = "MaxSimultaneousUsers"
m.Ap_ItemDTp = "N"
m.Ap_ItemVal = "0"
m.Ap_ItemDsc = [Specifies the maximum allowable number of simultaneous users.  Ignored if missing or set to 0 (requires opt VMP sec system installed and LoggedInUsers table and APPINFO/"TrackLoggedInUsers" set to ".T.")]
INSERT INTO AppConfig FROM MEMVAR
m.Ap_Item    = "PasswordCaseSensitivity"
m.Ap_ItemDTp = "N"
m.Ap_ItemVal = "0"
m.Ap_ItemDsc = [Specifies whether the User Password is case-sensitive or not:  0=case-sensitive (default), 1=case-insensitive]
INSERT INTO AppConfig FROM MEMVAR

m.Ap_Item    = "---" + "USER SECURITY SYSTEM ITEMS" + REPLICATE("-",20)
m.Ap_ItemDTp = SPACE(0)
m.Ap_ItemVal = SPACE(0)
m.Ap_ItemDsc = SPACE(0) 
INSERT INTO AppConfig FROM MEMVAR

m.Ap_Item    = "NoAccessControlBehavior"
m.Ap_ItemDTp = "C"
m.Ap_ItemVal = "0"
m.Ap_ItemDsc = [Specifies the behavior for form controls secured to No Access.  Valid values are "0", "0a", and "1".  XXDTES("XXFW.VCX","NoAccessControlBehavior","cusForms","zReadMe")]
INSERT INTO AppConfig FROM MEMVAR
m.Ap_Item    = "RespectFullAccessControlsOnReadOnlyForms"
m.Ap_ItemDTp = "N"
m.Ap_ItemVal = "0"
m.Ap_ItemDsc = [The SetUserSecurity method of VMP controls checks this setting and, when set to 1 (non-default) and THISFORM.ilReadOnly, controls secured to Full Access are left Enabled/Visible on the otherwise ReadOnly form.]
INSERT INTO AppConfig FROM MEMVAR
m.Ap_Item    = "AllowGroupsAsGroupMembers"
m.Ap_ItemDTp = "L"
m.Ap_ItemVal = ".F."
m.Ap_ItemDsc = [Specifies whether Groups can belong to Groups in addition to Users belonging to Groups.  Defaults to .F. and should be set to .T. with extreme caution.  Accessed in XXFWSEC.VCX/cusUserSecurity::SetProperties()]
INSERT INTO AppConfig FROM MEMVAR
m.Ap_Item    = "FilterItemsSecuredToNoAccess"
m.Ap_ItemDTp = "L"
m.Ap_ItemVal = ".T."
m.Ap_ItemDsc = [Specifies whether SecuredItems to which the Administrator has no access are presented either disabled/readonly or filtered out in the XXFWSEC.VCX forms.  Defaults to .T.: filter out completely.  Accessed in XXFWSEC.VCX/cusUserSecurity::SetProperties()]
INSERT INTO AppConfig FROM MEMVAR
m.Ap_Item    = "FullAccessToUnsecuredItems"
m.Ap_ItemDTp = "L"
m.Ap_ItemVal = ".T."
m.Ap_ItemDsc = [Specifies whether oUser.oSecurity::GetAccess() allows full access or no access to app components not found in the SecuredItems table.  Defaults to .T., accessed in XXFWSEC.VCX/cusUserSecurity::SetProperties()]
INSERT INTO AppConfig FROM MEMVAR
m.Ap_Item    = "NewUserGroupAccessToExistingSecuredItems"
m.Ap_ItemDTp = "N"
m.Ap_ItemVal = "1"
m.Ap_ItemDsc = [Specifies the default access to existing SecuredItems when a new UserGroup is added in the Group Properties form XXFWSEC.VCX/frmGroupPermissions.  1=Full (default) or 9=None]
INSERT INTO AppConfig FROM MEMVAR
m.Ap_Item    = "UserPropertiesDialog"
m.Ap_ItemDTp = "C"
m.Ap_ItemVal = "frmUserProperties,XXFWSEC.VCX"
m.Ap_ItemDsc = [Specifies the User Properties dialog (.SCX or ClassName,.VCXFileName) summoned by the <Properties...> button under the list of users in XXFWSEC.VCX/frmRightsAdmin -- MODIFY CLASS frmRightsAdmin OF XXFWSEC METHOD CallUsersDialog]
INSERT INTO AppConfig FROM MEMVAR
m.Ap_Item    = "UserMembershipDialog"
m.Ap_ItemDTp = "C"
m.Ap_ItemVal = "frmUserMembership,XXFWSEC.VCX"
m.Ap_ItemDsc = [Specifies the Group Membership dialog (.SCX or ClassName,.VCXFileName) summoned by the <Membership...> button under the list of users in XXFWSEC.VCX/frmRightsAdmin -- MODIFY CLASS frmRightsAdmin OF XXFWSEC METHOD cmdUserMembership.Click]
INSERT INTO AppConfig FROM MEMVAR
m.Ap_Item    = "GroupMembersDialog"
m.Ap_ItemDTp = "C"
m.Ap_ItemVal = "frmGroupMembers,XXFWSEC.VCX"
m.Ap_ItemDsc = [Specifies the Group Members dialog (.SCX or ClassName,.VCXFileName) summoned by the <Members...> button under the list of groups in XXFWSEC.VCX/frmRightsAdmin -- MODIFY CLASS frmRightsAdmin OF XXFWSEC METHOD cmdGroupMembers.Click]
INSERT INTO AppConfig FROM MEMVAR
m.Ap_Item    = "GroupPropertiesDialog"
m.Ap_ItemDTp = "C"
m.Ap_ItemVal = "frmGroupProperties,XXFWSEC.VCX"
m.Ap_ItemDsc = [Specifies the Group Properties dialog (.SCX or ClassName,.VCXFileName) summoned by the <Properties...> button under the list of groups in XXFWSEC.VCX/frmRightsAdmin -- MODIFY CLASS frmRightsAdmin OF XXFWSEC METHOD CallGroupsDialog]
INSERT INTO AppConfig FROM MEMVAR
m.Ap_Item    = "GroupPermissionsDialog"
m.Ap_ItemDTp = "C"
m.Ap_ItemVal = "frmGroupPermissions,XXFWSEC.VCX"
m.Ap_ItemDsc = [Specifies the Group Permissions dialog (.SCX or ClassName,.VCXFileName) summoned by the <Permissions...> button under the list of groups in XXFWSEC.VCX/frmRightsAdmin -- MODIFY CLASS frmRightsAdmin OF XXFWSEC METHOD cmdGroupPermissions.Click]
INSERT INTO AppConfig FROM MEMVAR
m.Ap_Item    = "ctrUserAccExceptionsPageClass"
m.Ap_ItemDTp = "C"
m.Ap_ItemVal = "ctrUserAccExceptionsPage,XXFWSEC.VCX"
m.Ap_ItemDsc = [Specifies the container of controls loaded to the exceptions page of the user properties form]
INSERT INTO AppConfig FROM MEMVAR
m.Ap_Item    = "UserAccExceptionPicklistForm"
m.Ap_ItemDTp = "C"
m.Ap_ItemVal = "frmPKUserAccException,XXFWSEC.VCX"
m.Ap_ItemDsc = [Specifies the picklist form summoned from the <Add Item> button of the grid on the User Access Exceptions page of the user properties form.]
INSERT INTO AppConfig FROM MEMVAR
m.Ap_Item    = "UGReportSelectionDialog"
m.Ap_ItemDTp = "C"
m.Ap_ItemVal = "frmSelectSecurityReportAll,XXFWSEC.VCX"
m.Ap_ItemDsc = [Specifies the print selection dialog (.SCX or ClassName,.VCXFileName) summoned by the <Reports> button of XXFWSEC.VCX/frmRightsAdmin -- MODIFY CLASS frmRightsAdmin OF XXFWSEC METHOD PrintAction]
INSERT INTO AppConfig FROM MEMVAR
m.Ap_Item    = "UserGroupsReport"
m.Ap_ItemDTp = "C"
m.Ap_ItemVal = "frmReportUSYSG1,XXFWSEC.VCX"
m.Ap_ItemDsc = [Specifies the report object run from the OK button of frmSelectGroupsReport -- MODIFY CLASS frmSelectGroupsReport OF XXFWSEC METHOD cmdOK.Click]
INSERT INTO AppConfig FROM MEMVAR
m.Ap_Item    = "UserListingReport"
m.Ap_ItemDTp = "C"
m.Ap_ItemVal = "frmReportUSYS1,XXFWSEC.VCX"
m.Ap_ItemDsc = [Specifies the user listing report object run from the OK button of frmSelectGroupsReport -- MODIFY CLASS frmSelectUsersReport OF XXFWSEC METHOD cmdOK.Click]
INSERT INTO AppConfig FROM MEMVAR
m.Ap_Item    = "UsersAndGroupsReport"
m.Ap_ItemDTp = "C"
m.Ap_ItemVal = "frmReportUSYS2,XXFWSEC.VCX"
m.Ap_ItemDsc = [Specifies the users and groups report object run from the OK button of frmSelectGroupsReport -- MODIFY CLASS frmSelectUsersReport OF XXFWSEC METHOD cmdOK.Click]
INSERT INTO AppConfig FROM MEMVAR
m.Ap_Item    = "UserListingFRX"
m.Ap_ItemDTp = "C"
m.Ap_ItemVal = "XXUSYS1.FRX"
m.Ap_ItemDsc = [Specifies the .FRX file run from the UserListingReport -- MODIFY CLASS frmReportUSYS1 OF XXFWSEC METHOD SetFRXFileName]
INSERT INTO AppConfig FROM MEMVAR
m.Ap_Item    = "UsersAndGroupsFRX"
m.Ap_ItemDTp = "C"
m.Ap_ItemVal = "XXUSYS2.FRX"
m.Ap_ItemDsc = [Specifies the .FRX file run from the UsersAndGroupsReport -- MODIFY CLASS frmReportUSYS2 OF XXFWSEC METHOD SetFRXFileName]
INSERT INTO AppConfig FROM MEMVAR
m.Ap_Item    = "GroupsAndMembersFRX"
m.Ap_ItemDTp = "C"
m.Ap_ItemVal = "XXUSYSG1.FRX"
m.Ap_ItemDsc = [Specifies the .FRX file run from the UserGroupsReport -- MODIFY CLASS frmReportUSYSG1 OF XXFWSEC METHOD SetFRXFileName]
INSERT INTO AppConfig FROM MEMVAR

m.Ap_Item    = "---" + "APP BEHAVIOR ITEMS" + REPLICATE("-",20)
m.Ap_ItemDTp = SPACE(0)
m.Ap_ItemVal = SPACE(0)
m.Ap_ItemDsc = SPACE(0) 
INSERT INTO AppConfig FROM MEMVAR

m.Ap_Item    = "TimeoutType"
m.Ap_ItemDTp = "C"
m.Ap_ItemVal = "N"
m.Ap_ItemDsc = [Timeout type "A"pp (terminate the app), "M"enu (close open forms, leave the user in the app, menu available/at login form), or "N"one.  See XXFW.VCX/ctrApp.zReadMe for more info.]
INSERT INTO AppConfig FROM MEMVAR
m.Ap_Item    = "ForceTableupdate"
m.Ap_ItemDTp = "N"
m.Ap_ItemVal = "1"
m.Ap_ItemDsc = [Specifies the 2nd parameter for TABLEUPDATE()s in the UpdateBuffers() method of forms inheriting from XXFW.VCX/frmData.  0=.F., 1/default=.T.  See XXFW.VCX/ctrApp.zReadMe for more info.  For n-Tier services, use the ctrDataSource.ilForceTableUpdate prop.]
INSERT INTO AppConfig FROM MEMVAR
*!*	m.Ap_Item    = "Install_Screen.oMsgSvc"   
*!*	m.Ap_ItemDTp = "N"
*!*	m.Ap_ItemVal = "1"
*!*	m.Ap_ItemDsc = [Specifies whether user-messages are all routed thru MSGSVC.*  0=No, 1/default=Yes.  Ignored if APPINFO indicates that INTL is to be installed, in which case _Screen.oMsgSvc is installed automatically.  See XXFW.VCX/ctrApp.zReadMe for more info.]
*!*	INSERT INTO AppConfig FROM MEMVAR
m.Ap_Item    = "Install_Screen_oMsgSvc"   
m.Ap_ItemDTp = "N"
m.Ap_ItemVal = "1"
m.Ap_ItemDsc = [Specifies whether user-messages are all routed thru MSGSVC.*  0=No, 1/default=Yes.  Ignored if APPINFO indicates that INTL is to be installed, in which case _Screen.oMsgSvc is installed automatically.  See XXFW.VCX/ctrApp.zReadMe for more info.]
INSERT INTO AppConfig FROM MEMVAR
m.Ap_Item    = "OpenTablesAtStartup"
m.Ap_ItemDTp = "N"
m.Ap_ItemVal = "0"
m.Ap_ItemDsc = [Specifies what tables opened on app startup: 0=all tables in main VMP .DBC (default), 1=only VMP system tables, 2=same as 1 AND tables opened by Form.DataEnvironment/oLib.oDBCSvc.OpenTable() opened in Default data session as encountered.]
INSERT INTO AppConfig FROM MEMVAR
m.Ap_Item    = "OpenDatabaseValidateDatabase"
m.Ap_ItemDTp = "N"
m.Ap_ItemVal = "1"
m.Ap_ItemDsc = [Specifies whether oLib.oDBCSvc.ValidateDatabase() is called on OpenData(). 0=never, 1=always (default), 2=not on app startup, but anytime thereafter]
INSERT INTO AppConfig FROM MEMVAR
m.Ap_Item    = "SmallerVMPFormSizes"
m.Ap_ItemDTp = "N"
m.Ap_ItemVal = "0"
m.Ap_ItemDsc = [Specifies whether VMP concrete forms like frmReportCatalog, frmEventLog, and the optional VMP security system forms resize themselves to smaller pre-VMP2003 sizes by calling their ResizeSmaller() method.  0=Designed/larger size, 1=Runtime resized smaller]
INSERT INTO AppConfig FROM MEMVAR
m.Ap_Item    = "AddUserNametoLogOutOptionOfFileMenu"
m.Ap_ItemDTp = "N"
m.Ap_ItemVal = "0"
m.Ap_ItemDsc = [Specifies whether oMenu.InstallUser() should add the name of the current user to the Log Out menu option on the File menu.  0=No (default), 1=Yes.]
INSERT INTO AppConfig FROM MEMVAR


m.Ap_Item    = "---" + "FORM/CONTROL BEHAVIOR ITEMS" + REPLICATE("-",20)
m.Ap_ItemDTp = SPACE(0)
m.Ap_ItemVal = SPACE(0)
m.Ap_ItemDsc = SPACE(0) 
INSERT INTO AppConfig FROM MEMVAR

m.Ap_Item    = "GenPKOnAdd"
m.Ap_ItemDTp = "N"
m.Ap_ItemVal = "0"
m.Ap_ItemDsc = [Specifies default THISFORM.ilGenPKOnAdd and therefore whether Primary Keys are "auto-generated/populated" on <Add>ing a record vs <Save>ing a record.  0/default=.F. (gen on Save), 1=.T. (gen on Add).  See XXFW.VCX/cusForms.SetilGenPKOnAdd for more info.]
INSERT INTO AppConfig FROM MEMVAR
m.Ap_Item    = "AddDeleteButtonsIfFormReadOnly"
m.Ap_ItemDTp = "N"
m.Ap_ItemVal = "0"
m.Ap_ItemDsc = [Specifies whether <Add>, <Delete> buttons on VMP data-entry forms are invisible or disabled if the form is readonly.  0/default=Invisible, 1=Disabled.  See XXFW.VCX/frmData::ActionButtonRefreshLogic() for more info.]
INSERT INTO AppConfig FROM MEMVAR
m.Ap_Item    = "FormQueryUnload"
m.Ap_ItemDTp = "N"
m.Ap_ItemVal = "0"
m.Ap_ItemDsc = [Specifies the behavior on QueryUnload of data-entry forms of VMP.  Values are 0,1,2,3,4 -- see XXFW.VCX/frmData::QueryUnload for a description of the five available behaviors.  See XXFW.VCX/frmBase.QueryUnload, cusForms.zReadMe(), cusForms.Init()]
INSERT INTO AppConfig FROM MEMVAR
m.Ap_Item    = "SaveCancelButtonsOnFormMode"
m.Ap_ItemDTp = "N"
m.Ap_ItemVal = "0"
m.Ap_ItemDsc = [Specifies whether <Save>, <Cancel> buttons on VMP data-entry forms toggle their Visible or Disabled property on changing form "modes". 0/default=Visible, 1=Enabled. See XXFW.VCX/frmData::ActionButtonRefreshLogic() for more info.]
INSERT INTO AppConfig FROM MEMVAR
m.Ap_Item    = "AddFormSizer"
m.Ap_ItemDTp = "N"
m.Ap_ItemVal = "1"
m.Ap_ItemDsc = [Specifies whether VMP forms whose AddSizer() method hasn't been overridden with a NODEFAULT add a custom "sizer" control.  0=No, 1/default=Yes.  See XXFW.VCX/ctrApp.zReadMe for more info.]
INSERT INTO AppConfig FROM MEMVAR
m.Ap_Item    = "CopyToolTipTextToStatusBarText"
m.Ap_ItemDTp = "N"
m.Ap_ItemVal = "0"
m.Ap_ItemDsc = [Specifies whether VMP controls with both ToolTipText and StatusBarText copy their ToolTipText to their StatusBarText, keeping StatusBarText always the same as ToolTipText.  0/default=No, 1=Yes.]
INSERT INTO AppConfig FROM MEMVAR
m.Ap_Item    = "ReportDestinationSPFScreenPreviewCaption"
m.Ap_ItemDTp = "N"
m.Ap_ItemVal = "0"
m.Ap_ItemDsc = [Specifies whether XXFWCOMP.VCX/ctrReportDestinationSPF display "\<Screen" or "Pre\<view".  0/default="\<Screen", 1="Pre\view"  See XXFWFRM.VCX/frmReportDestinationSPF.zReadMe() and XXFWCOMP.VCX/ctrReportDestinationSPF.ScreenPreviewCaption().]
INSERT INTO AppConfig FROM MEMVAR
m.Ap_Item    = "FormAutoCenterTopSpace"
m.Ap_ItemDTp = "N"
m.Ap_ItemVal = "0"
m.Ap_ItemDsc = [When Form.AutoCenter = .t. and there is no user pref, specifies a value by which the available _Screen.Height-Form.Height is divided, to raise the Form.Top higher than AutoCenter = .t.  Defaults to 0 (normal VFP behavior), 3 or 4 recommended when set]
INSERT INTO AppConfig FROM MEMVAR
m.Ap_Item    = "cmdGetFileGetFileORPutFile"
m.Ap_ItemDTp = "C"
m.Ap_ItemVal = "G"
m.Ap_ItemDsc = [Set this to "P" to execute PUTFILE() instead of GETFILE() in XXFWCTRL.VCX/cmdGetFile.Click()]
INSERT INTO AppConfig FROM MEMVAR
m.Ap_Item    = "DynamictxtPicklistFindCaption"
m.Ap_ItemDTp = "N"
m.Ap_ItemVal = "0"
m.Ap_ItemDsc = [Specifies whether the "Look for" Caption of the label next to any find textbox associated with a grdBase is updated to include the controlling Column.Header.Caption.  0/default=NO, 1=YES  See UpdateFindTextboxCaptionOnReSort() of grdBase]
INSERT INTO AppConfig FROM MEMVAR
m.Ap_Item    = "Initial_frmPicklist_txtFind_Value"
m.Ap_ItemDTp = "N"
m.Ap_ItemVal = "1"
m.Ap_ItemDsc = [Specifies whether frmPicklist.txtFind.Value is initially set to received tuInitialValue value/calling txtPicklist.Value.  0=No, 1/default=Yes, 2=Only if the frmPicklist.grdPicklist1 initial controlling column is Column1 (first/leftmost column)]
INSERT INTO AppConfig FROM MEMVAR
m.Ap_Item    = "EditboxMouseWheelFontSizeToUserPrefs"
m.Ap_ItemDTp = "N"
m.Ap_ItemVal = "0"
m.Ap_ItemDsc = [Specifies whether a FontSize change made by the user via CTRL+MouseWheel is saved/restored via the VMP user preferences engine (or just a temporary adjustment).  0/default=NO, 1=YES  See the MouseWheel method of XXFW.VCX/edtBase]
INSERT INTO AppConfig FROM MEMVAR
m.Ap_Item    = "LinkedToolbarVisibility"
m.Ap_ItemDTp = "N"
m.Ap_ItemVal = "0"
m.Ap_ItemDsc = [Specifies whether VMP linked toolbars are invisible or disabled if the linked form is not the active form.  0/default=Invisible, 1=Disabled.]
INSERT INTO AppConfig FROM MEMVAR
m.Ap_Item    = "ModalFormIconBehavior"
m.Ap_ItemDTp = "N"
m.Ap_ItemVal = "0"
m.Ap_ItemDsc = [Specifies the frmBase::SetIcon() for the Icon of modal forms.  0/default=Windows standard to force a blank icon, 1=Take no action (leave the Icon property alone, set to its Properties Sheet/method code setting)]
INSERT INTO AppConfig FROM MEMVAR
m.Ap_Item    = "PageNavigationKeyCodes"
m.Ap_ItemDTp = "C"
m.Ap_ItemVal = SPACE(0)
m.Ap_ItemDsc = [Specify frmBase::KeyPress() parameters to enable PreviousPage/NextPage hotkey navigation when focus is anywhere in a PageFrame.  "31/30,2" enables Ctrl+PgUp/Ctrl+PgDn for next/prev page.  XXDTES("XXFW.VCX","BEWARE","cusforms","SetPageframeHotkeyBehavior")]
INSERT INTO AppConfig FROM MEMVAR

m.Ap_Item    = "---" + "CALENDAR, AUTO-CALENDAR TEXTBOX ITEMS" + REPLICATE("-",20)
m.Ap_ItemDTp = SPACE(0)
m.Ap_ItemVal = SPACE(0)
m.Ap_ItemDsc = SPACE(0) 
INSERT INTO AppConfig FROM MEMVAR

m.Ap_Item    = "CalendarButtonClass"
m.Ap_ItemDTp = "C"
m.Ap_ItemVal = "cmdCalendar,XXFWCAL.VCX"
m.Ap_ItemDsc = [Specifies the ClassName,VCXFileName of the calendar button that is added at runtime by XXFWCAL.VCX/cusCalendarTextbox::AddCalendarButton(), to the right of the owner textbox]
INSERT INTO AppConfig FROM MEMVAR
m.Ap_Item    = "CalendarForm"
m.Ap_ItemDTp = "C"
m.Ap_ItemVal = "frmCalendar,XXFWCAL.VCX"
m.Ap_ItemDsc = [Specifies the calendar form ClassName,VCXFileName or SCXFileName used throughout this app.  See XXFWControlBehviors/cusCalendarTextbox::OwnerOnBind() and the Calendar... option on the Tools menu of XXFWMAIN.MNX]
INSERT INTO AppConfig FROM MEMVAR
m.Ap_Item    = "CalendarFormPosition"
m.Ap_ItemDTp = "C"
m.Ap_ItemVal = "1,2,3,4,5,6,7,8,9,10"
m.Ap_ItemDsc = [Specifies the position of the Calendar Form relative to the calling control (if any).  Checked in XXFWCAL.VCX/frmCalendar::SetInitialVisibleLocation(), which contains a list of what the values indicate.]
INSERT INTO AppConfig FROM MEMVAR
m.Ap_Item    = "CalendarFormTitleBar"
m.Ap_ItemDTp = "N"
m.Ap_ItemVal = "0"
m.Ap_ItemDsc = [When the CalendarForm is called from cusCalendarTextbox::DoCalendarForm(), this value determines whether the CalendarForm.TitleBar is set to 0-Off (default) or 1-On]
INSERT INTO AppConfig FROM MEMVAR
m.Ap_Item    = "ActionOnCalendarFormSelection"
m.Ap_ItemDTp = "N"
m.Ap_ItemVal = "1"
m.Ap_ItemDsc = [Determines the optional behavior of cusCalendarTextbox::DoCalendarForm, on seletion/closing the calendar.  0 - Only set the owner textbox.Value;  1 (default) - Additionally KEYBOARD "{TAB}" PLAIN CLEAR, typically to move focus to the next control]
INSERT INTO AppConfig FROM MEMVAR
m.Ap_Item    = "CalendarStyle"
m.Ap_ItemDTp = "C"
m.Ap_ItemVal = "0"
m.Ap_ItemDsc = [Specifies the ctrCalendar.icStyle property.  Default "0" includes the month dropdown and year spinner, but "-1" setting replaces those 2 controls with labels.  Optionally add ",FontName,FontSize" settings.  See ctrCalendar::SetupStyle()]
INSERT INTO AppConfig FROM MEMVAR
m.Ap_Item    = "CalendarHeaderDayCharacters"
m.Ap_ItemDTp = "N"
m.Ap_ItemVal = "3"
m.Ap_ItemDsc = [Specifies the number of characters of each day displayed in the 'header buttons' in ctrCalendar.  Checked in ctrCalendar::SetupHeaderButtonCaptions()]
INSERT INTO AppConfig FROM MEMVAR
m.Ap_Item    = "CalendarFirstDayOfWeek"
m.Ap_ItemDTp = "N"
m.Ap_ItemVal = "1"
m.Ap_ItemDsc = [Specifies what is considered to be the first day of the week, as displayed in ctrCalendar and as set in the First/LastDayofWeek hotkeys for cusCalendarTextbox.]
INSERT INTO AppConfig FROM MEMVAR
m.Ap_Item    = "CalendarKeys"
m.Ap_ItemDTp = "C"
m.Ap_ItemVal = "C,c"
m.Ap_ItemDsc = [These keystrokes invoke the calendar when in a textbox bound to Date/DateTime, and are translated to other languages in corresponding STRINGS.DBF record.  See cusForms::SetCalendarBehaviors() and txtBase::KeyPress() and frmCalendar::KeyPress()]
INSERT INTO AppConfig FROM MEMVAR
m.Ap_Item    = "TodayKeys"
m.Ap_ItemDTp = "C"
m.Ap_ItemVal = "T,t,D,d"
m.Ap_ItemDsc = [These keystrokes specify Today in a textbox bound to Date/DateTime or its popup calendar, and are translated to other languages in corresponding STRINGS.DBF record.  See cusForms::SetCalendarBehaviors(), txtBase::KeyPress() and frmCalendar::KeyPress()]
INSERT INTO AppConfig FROM MEMVAR
m.Ap_Item    = "NextDayKeys"
m.Ap_ItemDTp = "C"
m.Ap_ItemVal = "+,="
m.Ap_ItemDsc = [Keystrokes specifying the next day in a textbox bound to Date/DateTime or its popup calendar, and are translated to other languages in corresponding STRINGS.DBF record.  See cusForms::SetCalendarBehaviors(), txtBase::KeyPress() and frmCalendar::KeyPress()]
INSERT INTO AppConfig FROM MEMVAR
m.Ap_Item    = "PrevDayKeys"
m.Ap_ItemDTp = "C"
m.Ap_ItemVal = "-,_"
m.Ap_ItemDsc = [Keystrokes specifying the prev day in a textbox bound to Date/DateTime or its popup calendar, and are translated to other languages in corresponding STRINGS.DBF record.  See cusForms::SetCalendarBehaviors(), txtBase::KeyPress() and frmCalendar::KeyPress()]
INSERT INTO AppConfig FROM MEMVAR
m.Ap_Item    = "FirstDayOfWeekKeys"
m.Ap_ItemDTp = "C"
m.Ap_ItemVal = "W,w"
m.Ap_ItemDsc = [These keystrokes specify first day of week in a textbox bound to Date/DateTime or its popup calendar, are translated in corresponding STRINGS.DBF record.  See cusForms::SetCalendarBehaviors(), txtBase::KeyPress() and frmCalendar::KeyPress()]
INSERT INTO AppConfig FROM MEMVAR
m.Ap_Item    = "LastDayOfWeekKeys"
m.Ap_ItemDTp = "C"
m.Ap_ItemVal = "K,k"
m.Ap_ItemDsc = [These keystrokes specify last day of week in a textbox bound to Date/DateTime or its popup calendar, are translated in corresponding STRINGS.DBF record.  See cusForms::SetCalendarBehaviors(), txtBase::KeyPress() and frmCalendar::KeyPress()]
INSERT INTO AppConfig FROM MEMVAR
m.Ap_Item    = "FirstDayOfMonthKeys"
m.Ap_ItemDTp = "C"
m.Ap_ItemVal = "M,m"
m.Ap_ItemDsc = [These keystrokes specify first day of month in a textbox bound to Date/DateTime or its popup calendar, are translated in corresponding STRINGS.DBF record.  See cusForms::SetCalendarBehaviors(), txtBase::KeyPress() and frmCalendar::KeyPress()]
INSERT INTO AppConfig FROM MEMVAR
m.Ap_Item    = "LastDayOfMonthKeys"
m.Ap_ItemDTp = "C"
m.Ap_ItemVal = "H,h"
m.Ap_ItemDsc = [These keystrokes specify last day of month in a textbox bound to Date/DateTime or its popup calendar, are translated in corresponding STRINGS.DBF record.  See cusForms::SetCalendarBehaviors(), txtBase::KeyPress() and frmCalendar::KeyPress()]
INSERT INTO AppConfig FROM MEMVAR
m.Ap_Item    = "FirstDayOfQuarterKeys"
m.Ap_ItemDTp = "C"
m.Ap_ItemVal = "Q,q"
m.Ap_ItemDsc = [These keystrokes specify first day of quarter in a textbox bound to Date/DateTime or its popup calendar, are translated in corresponding STRINGS.DBF record.  See cusForms::SetCalendarBehaviors(), txtBase::KeyPress() and frmCalendar::KeyPress()]
INSERT INTO AppConfig FROM MEMVAR
m.Ap_Item    = "LastDayOfQuarterKeys"
m.Ap_ItemDTp = "C"
m.Ap_ItemVal = "U,u"
m.Ap_ItemDsc = [These keystrokes specify last day of quarter in a textbox bound to Date/DateTime or its popup calendar, are translated in corresponding STRINGS.DBF record.  See cusForms::SetCalendarBehaviors(), txtBase::KeyPress() and frmCalendar::KeyPress()]
INSERT INTO AppConfig FROM MEMVAR
m.Ap_Item    = "FirstDayOfYearKeys"
m.Ap_ItemDTp = "C"
m.Ap_ItemVal = "Y,y"
m.Ap_ItemDsc = [These keystrokes specify first day of year in a textbox bound to Date/DateTime or its popup calendar, are translated in corresponding STRINGS.DBF record.  See cusForms::SetCalendarBehaviors(), txtBase::KeyPress() and frmCalendar::KeyPress()]
INSERT INTO AppConfig FROM MEMVAR
m.Ap_Item    = "LastDayOfYearKeys"
m.Ap_ItemDTp = "C"
m.Ap_ItemVal = "R,r"
m.Ap_ItemDsc = [These keystrokes specify last day of year in a textbox bound to Date/DateTime or its popup calendar, are translated in corresponding STRINGS.DBF record.  See cusForms::SetCalendarBehaviors(), txtBase::KeyPress() and frmCalendar::KeyPress()]
INSERT INTO AppConfig FROM MEMVAR
m.Ap_Item    = "SuppressCalendarTextboxToolTipText"
m.Ap_ItemDTp = "E"
m.Ap_ItemVal = "0"
m.Ap_ItemDsc = [Suppress the (rather lengthy) hotkey-specific ToolTipText for a calendar textbox.  0=No (default, 1=Yes.]
INSERT INTO AppConfig FROM MEMVAR
m.Ap_Item    = "SuppressCalendarToolTipText"
m.Ap_ItemDTp = "E"
m.Ap_ItemVal = "0"
m.Ap_ItemDsc = [Suppress the (rather lengthy) ToolTipText for JUST THE BOTTOM-CENTER LABEL (lblDate) of the Calendar container.  0=No (default, 1=Yes.]
INSERT INTO AppConfig FROM MEMVAR
m.Ap_Item    = "SuppressCalendarTextboxHotkeys"
m.Ap_ItemDTp = "E"
m.Ap_ItemVal = "0"
m.Ap_ItemDsc = [Suppress the hotkeys for a calendar textbox.  0=No (default), 1=Yes.  If set to "1", then "SuppressCalendarTextboxToolTipText is also set to "1".]
INSERT INTO AppConfig FROM MEMVAR
m.Ap_Item    = "SuppressCalendarHotkeys"
m.Ap_ItemDTp = "E"
m.Ap_ItemVal = "0"
m.Ap_ItemDsc = [Suppress the hotkeys for the Calendar container.  0=No (default), 1=Yes.  If set to "1", then "SuppressCalendarToolTipText is also set to "1".]
INSERT INTO AppConfig FROM MEMVAR
m.Ap_Item    = "AddHotkeyActionsToTextboxShortcutMenu"
m.Ap_ItemDTp = "E"
m.Ap_ItemVal = "0"
m.Ap_ItemDsc = [Add the hotkey actions to the calendar textbox shortcut menu, as menu prompts/items?  0=No (default), 1=Yes.  Requires "SuppressCalendarTextboxHotkeys" set to "0", ignored otherwise.]
INSERT INTO AppConfig FROM MEMVAR
m.Ap_Item    = "AddHotkeyStrokesToTextboxShortcutMenu"
m.Ap_ItemDTp = "E"
m.Ap_ItemVal = "0"
m.Ap_ItemDsc = [Include the hotkey keystrokes as the KEY for menu prompts/items of the textbox shortcut menu?  0=No (default), 1=Yes.  Requires "SuppressCalendarTextboxHotkeys" set to "0" and "AddHotkeyActionsToTextboxShortcutMenu" set to "1", ignored otherwise.]
INSERT INTO AppConfig FROM MEMVAR


m.Ap_Item    = "---" + "GRID BEHAVIOR ITEMS" + REPLICATE("-",20)
m.Ap_ItemDTp = SPACE(0)
m.Ap_ItemVal = SPACE(0)
m.Ap_ItemDsc = SPACE(0) 
INSERT INTO AppConfig FROM MEMVAR

m.Ap_Item    = "AutoPadSpacesForLRAlignedHeaderCaptions"
m.Ap_ItemDTp = "N"
m.Ap_ItemVal = "0"
m.Ap_ItemDsc = [Specifies the number of leading/trailing SPACE(1)s automatically added to Left/Right aligned Grid.Column.Header.Captions in XXFW.VCX/grdBase::ApplyCaptionPadding()]
INSERT INTO AppConfig FROM MEMVAR
m.Ap_Item    = "GridPrefsClass"
m.Ap_ItemDTp = "C"
m.Ap_ItemVal = "cusGridPreference,XXFWGRD.VCX"
m.Ap_ItemDsc = [Specifies the ClassName,VCXFileName for the user preferences object that collaborates with grids and is instantiated in XXFW.VCX/grdBase::SetupUserPreference()]
INSERT INTO AppConfig FROM MEMVAR
m.Ap_Item    = "ReSortOnSingleClick"
m.Ap_ItemDTp = "L"
m.Ap_ItemVal = ".T."
m.Ap_ItemDsc = [Specifies whether column-resorting is effected on Column.Click() (default) or Column.DblClick()]
INSERT INTO AppConfig FROM MEMVAR
m.Ap_Item    = "HeaderClickRadius"
m.Ap_ItemDTp = "N"
m.Ap_ItemVal = "3"
m.Ap_ItemDsc = [When ReSort behavior is in effect and ReSortOnSingleClick is set to .T., determines how close together (in pixels) a MouseDown()+MouseUp() on a column.Header must be in order to be considered a Click() requiring a call to the grid.ReSort()]
INSERT INTO AppConfig FROM MEMVAR
m.Ap_Item    = "ShiftControllingColumnLeftMost"
m.Ap_ItemDTp = "L"
m.Ap_ItemVal = ".F."
m.Ap_ItemDsc = [Specifies whether grid.ReSort() additionally moves the "controlling" column to the leftmost position (ColumnOrder=1).  Defaults to .F., ColumnOrder is unchanged on grid.ReSort()]
INSERT INTO AppConfig FROM MEMVAR
m.Ap_Item    = "SortableHeaderFontBold"
m.Ap_ItemDTp = "L"
m.Ap_ItemVal = ".T."
m.Ap_ItemDsc = [Specifies whether "sortable" column headers display their Caption text in FontBold.  Defaults to .T.]
INSERT INTO AppConfig FROM MEMVAR
m.Ap_Item    = "RestoreInitialControllingColumnTagAD"
m.Ap_ItemDTp = "L"
m.Ap_ItemVal = ".T."
m.Ap_ItemDsc = [When grid.ReSort() is in effect and the initial controlling column is restored on instantiation via user prefs (default behavior), this flag indicates whether ORDER(Grid.RecordSource) should be set to whatever it was last time the user closed the form]
INSERT INTO AppConfig FROM MEMVAR
m.Ap_Item    = "ToggleADOnColumnReSort"
m.Ap_ItemDTp = "L"
m.Ap_ItemVal = ".T."
m.Ap_ItemDsc = [Specifies whether a call to the grid.ReSort() additionally toggles the Ascending/Descending order of the controlling column index tag]
INSERT INTO AppConfig FROM MEMVAR
m.Ap_Item    = "ToggleADPictureAsc"
m.Ap_ItemDTp = "C"
m.Ap_ItemVal = "ExpUpArr.BMP"
m.Ap_ItemDsc = [When ToggleADOnColumnReSort is set to .t., indicates the graphic to be used for the controlling Column.Header.Picture when the column is sorted in ascending order.  Takes precedence over any ToggleADCharAsc]
INSERT INTO AppConfig FROM MEMVAR
m.Ap_Item    = "ToggleADPictureDesc"
m.Ap_ItemDTp = "C"
m.Ap_ItemVal = "ExpDnArr.BMP"
m.Ap_ItemDsc = [When ToggleADOnColumnReSort is set to .t., indicates the graphic to be used for the controlling Column.Header.Picture when the column is sorted in descending order.  Takes precedence over any ToggleADCharDesc]
INSERT INTO AppConfig FROM MEMVAR
m.Ap_Item    = "ToggleADCharAsc"
m.Ap_ItemDTp = "C"
m.Ap_ItemVal = ">"
m.Ap_ItemDsc = [When ToggleADOnColumnReSort is set to .t. and ToggleADPictureAsc is not set, indicates the character(s) to be added to the controlling Column.Header.Caption when the column is sorted in ascending order]
INSERT INTO AppConfig FROM MEMVAR
m.Ap_Item    = "ToggleADCharDesc"
m.Ap_ItemDTp = "C"
m.Ap_ItemVal = "<"
m.Ap_ItemDsc = [When ToggleADOnColumnReSort is set to .t. and ToggleADPictureDesc is not set, indicates the character(s) to be added to the controlling Column.Header.Caption when the column is sorted in descending order]
INSERT INTO AppConfig FROM MEMVAR
m.Ap_Item    = "ToggleADCharSpaces"
m.Ap_ItemDTp = "N"
m.Ap_ItemVal = "0"
m.Ap_ItemDsc = [When ToggleADOnColumnReSort is set to .t. and ToggleADPicture../ToggleADChar.. are set, indicates the number of SPACE()s inserted into the controlling Column.Header.Caption between the text and the Picture/Character]
INSERT INTO AppConfig FROM MEMVAR
m.Ap_Item    = "ToggleADCharLocation"
m.Ap_ItemDTp = "C"
m.Ap_ItemVal = "L"
m.Ap_ItemDsc = [When ToggleADOnColumnReSort is set to .t. ToggleADChar.. properties are set and ToggleADPicture.. properties are not set, indicates where ToggleADCharSpaces are located relative to the Header.Caption text: "L"eft (default) or "R"ight] 
INSERT INTO AppConfig FROM MEMVAR
m.Ap_Item     = "MakeCurrentRowTopmostOnReSort"
m.Ap_ItemDTp  = "N"
m.Ap_ItemVal  = "0"
m.Ap_ItemDsc  = [When Grid.ReSort()/ChangeDisplayOrder() is called, make the current record the topmost row in the grid?  0/default=No, 1=Yes]
INSERT INTO AppConfig FROM MEMVAR
m.Ap_Item     = "AutoAdjustColumnsToExactlyFillGrid_Pct"
m.Ap_ItemDTp  = "N"
m.Ap_ItemVal  = "0"
m.Ap_ItemDsc  = [Checked in grdBase::AdjustColumnWidthsToExactlyFillGrid, indicates a pct of the total inner grid width within which the total column widths must fall to auto-size the columns to exactly fill the grid.  To disable this feature, set to <=0 or del this rec]
INSERT INTO AppConfig FROM MEMVAR
m.Ap_Item    = "GridHighlightForeColor"
m.Ap_ItemDTp = "E"
m.Ap_ItemVal = "-1"
m.Ap_ItemDsc = [grdBase::SetColors(), AllowCellSelection_Assign() apply this value to Grid.HighlightForeColor, providing a single place to set the HighlightForeColor for all grids in 1 VMP app.  -1: use default, -2: Windows color, >=0: use this explicit color] 
INSERT INTO AppConfig FROM MEMVAR
m.Ap_Item    = "GridHighlightBackColor"
m.Ap_ItemDTp = "E"
m.Ap_ItemVal = "-1"
m.Ap_ItemDsc = [grdBase::SetColors(), AllowCellSelection_Assign() apply this value to Grid.HighlightBackColor, providing a single place to set the HighlightBackColor for all grids in 1 VMP app.  -1: use default, -2: Windows color, >=0: use this explicit color] 
INSERT INTO AppConfig FROM MEMVAR
m.Ap_Item    = "GridSelectedItemForeColor"
m.Ap_ItemDTp = "E"
m.Ap_ItemVal = "-1"
m.Ap_ItemDsc = [This value is read in grdBase::SetColors() and, if set to a value >=0, is applied to the Grid.SelectedItemForeColor, providing a single place to explicitly set the SelectedItemForeColor for all grids in one VMP application.] 
INSERT INTO AppConfig FROM MEMVAR
m.Ap_Item    = "GridSelectedItemBackColor"
m.Ap_ItemDTp = "E"
m.Ap_ItemVal = "-1"
m.Ap_ItemDsc = [This value is read in grdBase::SetColors() and, if set to a value >=0, is applied to the Grid.SelectedItemBackColor, providing a single place to explicitly set the SelectedItemBackColor for all grids in one VMP application.] 
INSERT INTO AppConfig FROM MEMVAR
m.Ap_Item    = "MousePointerForHeaderOfMovableColumns"
m.Ap_ItemDTp = "E"
m.Ap_ItemVal = "-1"
m.Ap_ItemDsc = [VFP9+ only.  Checked in grdBase::SetHeaderProperties(), specifies the Header.MousePointer for Columns whose Movable is set to .T.  Ignored if set to a negative value.] 
INSERT INTO AppConfig FROM MEMVAR
m.Ap_Item    = "MousePointerForHeaderOfNonMovableColumns"
m.Ap_ItemDTp = "E"
m.Ap_ItemVal = "-1"
m.Ap_ItemDsc = [VFP9+ only.  Checked in grdBase::SetHeaderProperties(), specifies the Header.MousePointer for Columns whose Movable is set to .F.  Ignored if set to a negative value.] 
INSERT INTO AppConfig FROM MEMVAR
m.Ap_Item    = "MousePointerForHeaderOfReSortColumns"
m.Ap_ItemDTp = "E"
m.Ap_ItemVal = "-1"
m.Ap_ItemDsc = [takes precedence] 
m.Ap_ItemDsc = [VFP9+ only.  Checked in grdBase::SetReSortBehaviors(), specifies the Header.MousePointer for "ReSort-able" Columns.  Ignored if negative.  Takes precedence over MousePointerForHeaderOfMovableColumns and MousePointerForHeaderOfNonMovableColumns.] 
INSERT INTO AppConfig FROM MEMVAR
m.Ap_Item    = "MouseIconForHeaderOfMovableColumns"
m.Ap_ItemDTp = "C"
m.Ap_ItemVal = ""
m.Ap_ItemDsc = [VFP9+ only.  Checked in grdBase::SetHeaderProperties(), specifies the Header.MouseIcon for Columns whose Movable is set to .T.  Ignored if set to a negative value.  ALSO IGNORED when MousePointerForHeaderOfMovableColumns is not set to 99.] 
INSERT INTO AppConfig FROM MEMVAR
m.Ap_Item    = "MouseIconForHeaderOfNonMovableColumns"
m.Ap_ItemDTp = "C"
m.Ap_ItemVal = ""
m.Ap_ItemDsc = [VFP9+ only.  Checked in grdBase::SetHeaderProperties(), specifies the Header.MouseIcon for Columns whose Movable is set to .F.  Ignored if set to a negative value.  ALSO IGNORED when MousePointerForHeaderOfNonMovableColumns is not set to 99.] 
INSERT INTO AppConfig FROM MEMVAR
m.Ap_Item    = "MouseIconForHeaderOfReSortColumns"
m.Ap_ItemDTp = "C"
m.Ap_ItemVal = ""
m.Ap_ItemDsc = [VFP9+ only.  Checked in grdBase::SetReSortBehaviors(), specifies the Header.MouseIcon for "ReSort-able" Columns.  Ignored if set to a negative value.  ALSO IGNORED when MousePointerForHeaderOfReSortColumns is set to anything other than 99.] 
INSERT INTO AppConfig FROM MEMVAR


m.Ap_Item    = "---" + "n-TIER SERVICES ITEMS" + REPLICATE("-",20)
m.Ap_ItemDTp = SPACE(0)
m.Ap_ItemVal = SPACE(0)
m.Ap_ItemDsc = SPACE(0) 
INSERT INTO AppConfig FROM MEMVAR
m.Ap_Item    = "frmData_icBOMgrClass"
m.Ap_ItemDTp = "C"
m.Ap_ItemVal = ""
m.Ap_ItemDsc = [Yields the icBOMgrClass property value for forms whose ilNT=.T. when the Form.icBOMgrClass is blank/empty (the default).  If Form.icBOMgrClass is empty and this record is empty/missing, defaults to "ctrBusinessObjectManager,XXFWNTBO.VCX"]
INSERT INTO AppConfig FROM MEMVAR

m.Ap_Item    = "---" + "REMOTE DATA ITEMS" + REPLICATE("-",20)
m.Ap_ItemDTp = SPACE(0)
m.Ap_ItemVal = SPACE(0)
m.Ap_ItemDsc = SPACE(0) 
INSERT INTO AppConfig FROM MEMVAR
m.Ap_Item    = "ODBCErrorMessageRoutine"
m.Ap_ItemDTp = "C"
m.Ap_ItemVal = ""
m.Ap_ItemDsc = "Name of a program called in oLib.oConnectionSvc.ODBCErrorMessage to determine/present the user with a message when AERROR[2]=1526"
INSERT INTO AppConfig FROM MEMVAR
*!*	m.Ap_Item    = "oLib.oConnectionSvcClass"  
*!*	m.Ap_ItemDTp = "C"
*!*	m.Ap_ItemVal = "cusConnectionSvc,XXFWLIBS.VCX"
*!*	m.Ap_ItemDsc = [Name of the class (usually cusConnectionSvc,XXFWLIBS.VCX or a subclass thereof) instantiated as oLib.oConnectionSvc by oApp.SetupRemoteDataServices().  If this record is absent or empty, oLib.oConnectionSvc defaults to cusConnectionSvc,XXFWLIBS.VCX]
*!*	INSERT INTO AppConfig FROM MEMVAR
m.Ap_Item    = "oLib_oConnectionSvcClass"   
m.Ap_ItemDTp = "C"
m.Ap_ItemVal = "cusConnectionSvc,XXFWLIBS.VCX"
m.Ap_ItemDsc = [Name of the class (usually cusConnectionSvc,XXFWLIBS.VCX or a subclass thereof) instantiated as oLib.oConnectionSvc by oApp.SetupRemoteDataServices().  If this record is absent or empty, oLib.oConnectionSvc defaults to cusConnectionSvc,XXFWLIBS.VCX]
INSERT INTO AppConfig FROM MEMVAR

m.Ap_Item    = "---" + "DEVELOPMENT SETTINGS" + REPLICATE("-",20)
m.Ap_ItemDTp = SPACE(0)
m.Ap_ItemVal = SPACE(0)
m.Ap_ItemDsc = SPACE(0) 
INSERT INTO AppConfig FROM MEMVAR

m.Ap_Item    = "DevModeExpr"
m.Ap_ItemDTp = "E"
m.Ap_ItemVal = "NOT VERSION(2) = 0"
m.Ap_ItemDsc = "Expression indicating whether or not app is running in development"
INSERT INTO AppConfig FROM MEMVAR
m.Ap_Item   = "NoOnErrorDuringDev" 
m.Ap_ItemDTp= "L"
m.Ap_ItemVal= ".T."
m.Ap_ItemDsc= "Indicates whether you'd rather suppress the global ON ERROR error handler during development, so you can just <Cancel> <Ignore> <Suspend> instead"
INSERT INTO AppConfig FROM MEMVAR
m.Ap_Item    = "SetPathDev"
m.Ap_ItemDTp = "C"
m.Ap_ItemVal = SPACE(0)
m.Ap_ItemDsc = "VFP path to be set in application setup -- Development"
INSERT INTO AppConfig FROM MEMVAR
m.Ap_Item    = "DevToolbarClass"
m.Ap_ItemDTp = "C"
m.Ap_ItemVal = IIF(FILE("XXTOOLS.VCX"),"tbrDevTools,XXTOOLS.VCX",SPACE(0))
m.Ap_ItemDsc = "Class name for any desired Developerís Toolbar"
INSERT INTO AppConfig FROM MEMVAR
m.Ap_Item    = "DevToolbarPad"
m.Ap_ItemDTp = "C"
m.Ap_ItemVal = "Tools"
m.Ap_ItemDsc = "Pad prompt of system menu popup to which to add Dev toolbar"
INSERT INTO AppConfig FROM MEMVAR
m.Ap_Item    = "DevToolbarForStandaloneForms"
m.Ap_ItemDTp = "L"
m.Ap_ItemVal = ".T."
m.Ap_ItemDsc = "Specifies whether the Developer's Toolbar is instantiated with standalone forms"
INSERT INTO AppConfig FROM MEMVAR
m.Ap_Item    = "OnBuildProjectSetStrictDateTo"
m.Ap_ItemDTp = "N"
m.Ap_ItemVal = "2"
m.Ap_ItemDsc = [When building a .PJX project whose project hook inherits from XXTOOLS.VCX/pjhVMP, the project hook SETs STRICTDATE TO this value (0,1, or 2/default) temporarily during the build - EDITSOURCE("XXTOOLS.VCX",1,"pjhVMP","BeforeBuildSetStrictDate")]
INSERT INTO AppConfig FROM MEMVAR
m.Ap_Item    = "ProjectHookAskBeforeWriteExternal"
m.Ap_ItemDTp = "N"
m.Ap_ItemVal = "1"
m.Ap_ItemDsc = [When adding a file to a .PJX project whose project hook inherits from XXTOOLS.VCX/pjhVMP, ask before writing the EXTERNAL command to the PMIncludeFiles local procedure of ??MAIN.PRG.  0=No, 1=Yes (default)]
INSERT INTO AppConfig FROM MEMVAR
m.Ap_Item    = "SuppressStandaloneForm_ScreenOutput"
m.Ap_ItemDTp = "L"
m.Ap_ItemVal = ".F."
m.Ap_ItemDsc = [Logical flag indicating if the default VMP behavior where standalone form startup information is displayed/output to _Screen is SUPPRESSED in XXFW.VCX/cusStandaloneForms::SetupoUser()]
INSERT INTO AppConfig FROM MEMVAR
m.Ap_Item    = "StandaloneFormsClass"
m.Ap_ItemDTp = "C"
m.Ap_ItemVal = "cusStandaloneForms,XXFW.VCX"
m.Ap_ItemDsc = [Specifies the ClassName,FileName instantiated to Form.oStandaloneForms in Form.StandaloneSetup(), but can be overridden by any Form.icStandaloneFormsClass on a per-form basis.]
INSERT INTO AppConfig FROM MEMVAR

m.Ap_Item    = "---" + "OPTIONAL APP INFORMATION" + REPLICATE("-",20)
m.Ap_ItemDTp = SPACE(0)
m.Ap_ItemVal = SPACE(0)
m.Ap_ItemDsc = SPACE(0) 
INSERT INTO AppConfig FROM MEMVAR

m.Ap_Item    = "VersionNumber"
m.Ap_ItemDTp = "C"
m.Ap_ItemVal = "1.00"
m.Ap_ItemDsc = "(not currently used in VMP)  Application version number (NOTE: VFP apps should build this info into the .EXE, use FOXTOOLS/GetFileVersion)"
INSERT INTO AppConfig FROM MEMVAR
m.Ap_Item    = "SerialNumber"
m.Ap_ItemDTp = "C"
m.Ap_ItemVal = "ABC123"
m.Ap_ItemDsc = "(not currently used in VMP)  Application serial number  (NOTE: VFP apps should build this info into the .EXE, use FOXTOOLS/GetFileVersion)"
INSERT INTO AppConfig FROM MEMVAR
m.Ap_Item    = "Copyright"
m.Ap_ItemDTp = "C"
m.Ap_ItemVal = "Copyright " + RIGHT(STR(YEAR(DATE())),4) + " ...  All Rights Reserved."
m.Ap_ItemDsc = "(not currently used in VMP)  Copyright information  (NOTE: VFP apps should build this info into the .EXE, use FOXTOOLS/GetFileVersion)"
INSERT INTO AppConfig FROM MEMVAR
m.Ap_Item    = "RegisteredToName"
m.Ap_ItemDTp = "C"
m.Ap_ItemVal = "John Doe"
m.Ap_ItemDsc = "(not currently used in VMP)  Name of registered owner  (NOTE: VFP apps should build this info into the .EXE, use FOXTOOLS/GetFileVersion)"
INSERT INTO AppConfig FROM MEMVAR
m.Ap_Item    = "RegisteredToCompany"
m.Ap_ItemDTp = "C"
m.Ap_ItemVal = "John Doe's Company"
m.Ap_ItemDsc = "(not currently used in VMP)  Name of registered company/owner  (NOTE: VFP apps should build this info into the .EXE, use FOXTOOLS/GetFileVersion)"
INSERT INTO AppConfig FROM MEMVAR

m.Ap_Item    = "---" + "NON-VMP ITEMS and/or VMP items added by XXDTAppConfig.PRG---" + REPLICATE("-",20)
m.Ap_ItemDTp = SPACE(0)
m.Ap_ItemVal = SPACE(0)
m.Ap_ItemDsc = "(your stuff from here on...)"
INSERT INTO AppConfig FROM MEMVAR

SELECT AppConfig
*
*  procedural hook for your code to update the
*  default VMP AppConfig table -- there is another
*  procedural hook at the beginning of this procedure
*
*  note that CreateAppConfigTableHook2 may be a standalone
*  .PRG/program OR a local procedure/function in the
*  calling "wrapper" program
*
*  NOTE that if CreateAppConfigTableHook2 is a local
*  procedure/function in the calling "wrapper" program,
*  any errors generated there are CATCHed here UNLESS
*  you put your own TRY/CATCH block in your
*  CreateAppConfigTableHook2
*
IF FILE("CreateAppConfigTableHook2.PRG") ;
    OR FILE("CreateAppConfigTableHook2.FXP")
  *
  *  CreateAppConfigTableHook2 is a standalone program
  *
  DO ("CreateAppConfigTableHook2")
 ELSE
  *
  *  there is no CreateAppConfigTableHook2 standalone
  *  program, so we'll try to DO it, do executed
  *  it if there is a procedure/function of that
  *  name available in the calling stack
  *
  TRY
  DO ("CreateAppConfigTableHook2")
  CATCH TO oEx
    IF oEx.ErrorNo = 1 AND "CREATEAPPCONFIGTABLEHOOK2" $ UPPER(oEx.Message)
      *
      *  there is no CreateAppConfigTableHook2, 
      *  nothing to do
      *
     ELSE
      *
      *  an error occurred in CreateAppConfigTableHook2
      *  that was not specifically CATCHed there
      *
      ASSERT .f. message ;
           "An error occurred in CreateAppConfigTableHook2, " + ;
           "but was not CATCHed there -- oEx contains the " + ;
           "error information.  You can select <Debug> here " + ;
           "and put the oEx object in the Debugger."
    ENDIF
  ENDTRY
ENDIF

*
*  another procedural hook for your code, same as 
*  CreateAppConfigTableHook2 above, except that we
*  pass the tcAppPrefix, tcDBCDir to 
*  CreateAppConfigTableHook2 (if Drew had realized
*  that it would be useful to pass the parameters
*  on to the hook program, then he wouldn't have
*  had to add this 2nd hook in the 2005.03.18.03
*  build)
*  
*  NOTE that tcAppPrefix can be passed here as "XX"
*  from places like the Load of the VMP Application
*  Setup Wizard, so your hook should take into
*  account that possibility, and probably immediately
*  RETURN if tcAppPrefix is received as "XX"
*  
IF FILE("CreateAppConfigTableHook2a.PRG") ;
    OR FILE("CreateAppConfigTableHook2a.FXP")
  *
  *  CreateAppConfigTableHook2a is a standalone program
  *
  DO ("CreateAppConfigTableHook2a") WITH m.tcAppPrefix, m.tcDir
 ELSE
  *
  *  there is no CreateAppConfigTableHook2a standalone
  *  program, so we'll try to DO it, do executed
  *  it if there is a procedure/function of that
  *  name available in the calling stack
  *
  TRY
  DO ("CreateAppConfigTableHook2a") WITH m.tcAppPrefix, m.tcDir
  CATCH TO oEx
    IF oEx.ErrorNo = 1 AND "CREATEAPPCONFIGTABLEHOOK2A" $ UPPER(oEx.Message)
      *
      *  there is no CreateAppConfigTableHook2a, 
      *  nothing to do
      *
     ELSE
      *
      *  an error occurred in CreateAppConfigTableHook2a
      *  that was not specifically CATCHed there
      *
      ASSERT .f. message ;
           "An error occurred in CreateAppConfigTableHook2a, " + ;
           "but was not CATCHed there -- oEx contains the " + ;
           "error information.  You can select <Debug> here " + ;
           "and put the oEx object in the Debugger."
    ENDIF
  ENDTRY
ENDIF

SELECT AppConfig
USE 
USE (lcTable) IN 0  
RETURN 



***************************************************
PROCEDURE CreateGeneratePKTable
***************************************************
LPARAMETERS tcAppPrefix
WAIT WINDOW "GeneratePK..." NOWAIT
LOCAL lcDBCDir
lcDBCDir = GetDBCDir()
IF FILE(lcDBCDir + tcAppPrefix + "GENPK.DBF")
  IF INDBC("GENERATEPK","TABLE")
    REMOVE TABLE GENERATEPK DELETE
  ENDIF    
ENDIF
CREATE TABLE (lcDBCDir + tcAppPrefix + "GENPK") NAME "GENERATEPK" ;
     (GPK_PK            C(128), ;
      GPK_CurrentNumber I(4), ;
      GPK_CurrentAlpha  C(5))

* AB - 10/2004 - Start
DO IndexGeneratePKTable WITH tcAppPrefix

*!*	LOCAL lcSetCollate
*!*	lcSetCollate = SET("COLLATE")
*!*	SET COLLATE TO "MACHINE"  &&& other collate sequences generate an Invalid Key Length error on expressions > 120 characters
*!*	ALTER TABLE GENERATEPK ADD PRIMARY KEY GPK_PK TAG GPK_PK
*!*	SET COLLATE TO (lcSetCollate)
*!*	INDEX ON DELETED() TAG GPK_Del
* AB - 10/2004 - End

DBSETPROP("GENERATEPK",                   "TABLE","Comment","System-generated Primary Keys")
DBSETPROP("GENERATEPK.GPK_PK",            "FIELD","Caption","Primary Key")
DBSETPROP("GENERATEPK.GPK_PK",            "FIELD","Comment","TableName for which to generate PKs")
DBSETPROP("GENERATEPK.GPK_CurrentNumber", "FIELD","Caption","Current PK")
DBSETPROP("GENERATEPK.GPK_CurrentNumber", "FIELD","Comment","Current PK Number in use")
DBSETPROP("GENERATEPK.GPK_CurrentAlpha",  "FIELD","Caption","Current AlphaNumeric PK")
DBSETPROP("GENERATEPK.GPK_CurrentAlpha",  "FIELD","Comment","Current AlphaNumeric PK in use")
RETURN



***************************************************
PROCEDURE CreateMemoAETable
***************************************************
*
*  AutoExpand "dictionary" table for editboxes
*
*  note that the 4th tlUsr_FK parameter is not passed from
*  the code in this .PRG -- it can only be called when making
*  an external call to this method:
*    DO CreateMemoAETable in xxfwdata WITH "AP",.t.,.t.,.t.
*
*  note that tlIntegerKeys is ignored unless tlUsr_FK is passed as .T.
*
LPARAMETERS tcAppPrefix, tlFree, tlIntegerKeys, tlUsr_FK
WAIT WINDOW "MemoAE..." NOWAIT
IF VARTYPE(tlFree) = "L" AND tlFree
  LOCAL lcSetDatabase
  lcSetDatabase = SET("DATABASE")
  SET DATABASE TO
  * CREATE (free) TABLE will overwrite any existing MemoAE.DBF
  CREATE TABLE MemoAE.DBF ;
       (MAE_Code C(5), ; 
        MAE_Text M)
  SET DATABASE TO &lcSetDatabase.
 ELSE
  LOCAL lcDBCDir
  lcDBCDir = GetDBCDir()
  IF FILE(lcDBCDir + tcAppPrefix + "MEMOAE.DBF")
    IF INDBC("MEMOAE","TABLE")
      REMOVE TABLE MemoAE DELETE
    ENDIF    
  ENDIF
  CREATE TABLE (lcDBCDir + tcAppPrefix + "MEMOAE") NAME "MemoAE" ;
       (MAE_Code C(5), ; 
        MAE_Text M)
  DBSETPROP("MemoAE",          "TABLE","Comment","AutoExpand 'dictionary' table for editboxes")
  DBSETPROP("MemoAE.MAE_Code", "FIELD","Caption","Code")
  DBSETPROP("MemoAE.MAE_Text", "FIELD","Caption","Expanded text")
ENDIF

* AB - 10/2004 - Start
DO CASE
  CASE tlUsr_FK AND tlIntegerKeys
    ALTER TABLE MemoAE ADD COLUMN MAE_UsrFK I
*!*	    INDEX ON STR(MAE_UsrFK) + UPPER(MAE_Code) TAG MAE_Code CANDIDATE FOR NOT DELETED() 
  CASE tlUsr_FK AND !tlIntegerKeys
    ALTER TABLE MemoAE ADD COLUMN MAE_UsrFK C(5)
*!*	    INDEX ON MAE_UsrFK + UPPER(MAE_Code) TAG MAE_Code CANDIDATE FOR NOT DELETED() 
  OTHERWISE
*!*	    INDEX ON UPPER(MAE_Code) TAG MAE_Code CANDIDATE FOR NOT DELETED() 
ENDCASE

DO IndexMemoAETable	WITH tcAppPrefix
* AB - 10/2004 - End
RETURN




***************************************************
PROCEDURE CreateUsersTable
***************************************************
LPARAMETERS tcAppPrefix, ;
            tlUseIntegerPKFields
WAIT WINDOW "Users..." NOWAIT
LOCAL lcDBCDir, xx, lcPKField
lcDBCDir = GetDBCDir()
IF INDBC("USERS","TABLE")
  REMOVE TABLE USERS DELETE
ENDIF    
*
*  to change (increase) the size/width of the Usr_Password
*  field in an existing VMP application, see XXFW.VCX/cusUser.zReadMe()
*    XXDTES("XXFW.VCX","If you need to change the width of the Usr_Password field","cususer","ZREADME")
*
lcPKField = "USR_PK" + SPACE(1) + IIF(tlUseIntegerPKFields,"I(4)","C(5)")
CREATE TABLE (m.lcDBCDir + m.tcAppPrefix + "USYS") NAME USERS ;
     (&lcPKField., ;
      Usr_LanID         C(8), ;
      Usr_Password      C(8), ;  
      Usr_Rights        N(1) DEFAULT 5, ;
      Usr_System        C(1) DEFAULT "N", ;
      Usr_Active        C(1) DEFAULT "Y", ;
      Usr_LastLogin     T(8) DEFAULT .NULL. NULL, ;
      Usr_FirstName     C(15), ;
      Usr_MiddleInitial C(1), ;
      Usr_LastName      C(20), ;
      Usr_Phone         C(12), ;
      Usr_PhoneExt      C(4), ;
      Usr_Fax           C(12), ;
      Usr_EnablePrefs   C(1) DEFAULT "Y", ;
      Usr_INTLLanguage  C(20) DEFAULT "Original", ;
      Usr_Notes         M)

IF VARTYPE(m.glX8ED) = "L" AND m.glX8ED
  *
  *  coming in a future release of VMP 2005 
  *
  LOCAL lcPassPhrase, lcStringOrBlock, lcValue, llUsed
  lcPassPhrase = SPACE(0)
  lcStringOrBlock = SPACE(0)
  *
  *  see if these have been specified in the AppConfig
  *  table
  *
  llUsed = USED("AppConfig")
  IF NOT m.llUsed
    USE (m.lcDBCDir + m.tcAppPrefix + "CONFIG.DBF") AGAIN IN 0 ALIAS AppConfig
  ENDIF
  SELECT AppConfig
  LOCATE FOR UPPER(ALLTRIM(Ap_Item)) == UPPER("PaPhFactor")
  IF FOUND() AND NOT EMPTY(Ap_ItemVal)
    lcPassPhrase = X8ConvChar(ALLTRIM(Ap_ItemVal),Ap_ItemDTp)
    IF Ap_ItemDTp = "C"
      lcPassPhrase = X3ED("D",m.lcPassPhrase)
    ENDIF
  ENDIF
  LOCATE FOR UPPER(ALLTRIM(Ap_Item)) == UPPER("StBlFactor")
  IF FOUND() AND NOT EMPTY(Ap_ItemVal)
    lcStringOrBlock = X8ConvChar(ALLTRIM(Ap_ItemVal),Ap_ItemDTp)
  ENDIF
  IF EMPTY(m.lcPassPhrase) OR EMPTY(m.lcStringOrBlock)
    MESSAGEBOX("You have indicated that you want X8ED()-based " + ;
               "encryption for the Users.Usr_Password field." + ;
               CHR(13) + CHR(13) + ;
               "In order to populate the 2 default Users records " + ;
               "for Dudley E. Fault and the 'External' user, you " + ;
               "must supply the PassPhrase and 'S'/'B' String/Block " + ;
               "encryption settings, in order to populate the Usr_Password " + ;
               "field with the X8ED()-encrypted 'PASSWORD' string, " + ;
               "because XXFWDATA.PRG is unable to locate one or both of those " + ;
               "settings in your AppConfig table." + ;
               CHR(13) + CHR(13) + ;
               "Once you close this MESSAGEBOX() " + ;
               "dialog, you will have the opportunity to set (or verify) those " + ;
               "2 values in X8WRPFRM() dialogs.  If you leave either " + ;
               "one blank, or enter something invalid, XXFWDATA.PRG " + ;
               "will terminate." + ;
               CHR(13) + CHR(13) + ;
               "NOTE that the PassPhrase you supply here should " + ;
               "be your **REAL** PassPhrase, and will be posted to " + ;
               "the Ap_ItemVal field of the 'PaPhFactor' record in " + ;
               "your AppConfig table (if any).  Any leading/trailing blank " + ;
               "spaces will be ALLTRIM()med.", ;
               0+48,"Please Note -- XXFWDATA.PRG")             
    lcValue = m.lcPassPhrase
    lcPassPhrase = X8WRPFRM("frmGetInputItem,XXFWFRM.VCX", ;
                           ["PassPhrase - will be posted to 'PaPhFactor' AppConfig record","PassPhrase must be at least 6 characters",REPLICATE("X",100),] + m.lcValue)
    IF ISNULL(m.lcPassPhrase) OR EMPTY(m.lcPassPhrase) 
      plAbort = .t.
      RETURN .f.
    ENDIF
    lcPassPhrase = ALLTRIM(m.lcPassPhrase)
    IF NOT LENC(m.lcPassPhrase) >= 6
      MESSAGEBOX("The PassPhrase you entered:" + ;
                 CHR(13) + CHR(13) + ;
                 m.lcPassPhrase + ;
                 CHR(13) + CHR(13) + ;
                 "is not at least 6 characters long.", ;
                 0+16,"Please Note")
      plAbort = .t.
      RETURN .f.
    ENDIF
    *
    *  OK, we have lcPassPhrase, now we need lcStringOrBlock
    *  
    lcValue = m.lcStringOrBlock
    lcStringOrBlock = X8WRPFRM("frmGetInputItem,XXFWFRM.VCX", ;
                               ["'S'tring or 'B'lock encryption - will be posted to 'StBlFactor' AppConfig record","Enter 'S'tring or 'B'lock only, indicating String or Block encryption","!",] + m.lcValue)
    IF ISNULL(m.lcStringOrBlock) OR EMPTY(m.lcStringOrBlock) 
      plAbort = .t.
      RETURN .f.
    ENDIF
    lcStringOrBlock = LEFTC(UPPER(ALLTRIM(m.lcStringOrBlock)),1)
    IF NOT INLIST(m.lcStringOrBlock,"B","S")
      MESSAGEBOX("The 'S'tring or 'B'lock value you entered:" + ;
                 CHR(13) + ;
                 m.lcStringOrBlock + ;
                 CHR(13) + ;
                 "is invalid -- it must be 'S'tring or 'B'lock.", ;
                 0+16,"Please Note")
      plAbort = .t.
      RETURN .f.
    ENDIF
    *
    *  frmGetInputItem has closed AppConfig and Users
    *
    IF NOT USED("AppConfig")
      USE (m.lcDBCDir + m.tcAppPrefix + "CONFIG.DBF") AGAIN IN 0 ALIAS AppConfig
    ENDIF
    SET DATABASE TO (m.tcAppPrefix)
    CLOSE DATABASES 
    OPEN DATABASE (m.lcDBCDir + m.tcAppPrefix) EXCLUSIVE 
    SET DATABASE TO (m.tcAppPrefix)
    IF NOT USED("Users")
      USE (m.lcDBCDir + m.tcAppPrefix + "USYS") again IN 0 ALIAS Users 
    ENDIF
    *
    *  post these to AppConfig
    *
    SELECT AppConfig
    LOCATE FOR UPPER(ALLTRIM(Ap_Item)) == UPPER("PaPhFactor")
    IF FOUND() 
      replace Ap_ItemVal WITH m.lcPassPhrase, ;
              Ap_ItemDTp WITH "C"
    ENDIF
    LOCATE FOR UPPER(ALLTRIM(Ap_Item)) == UPPER("StBlFactor")
    IF FOUND() 
      replace Ap_ItemVal WITH m.lcStringOrBlock, ;
              Ap_ItemDTp WITH "C" 
    ENDIF
    IF NOT m.llUsed
      USE IN AppConfig
    ENDIF
  ENDIF   &&& EMPTY(m.lcPassPhrase) OR EMPTY(m.lcStringOrBlock)
  ALTER TABLE Users ALTER COLUMN Usr_Password C(IIF(m.lcStringOrBlock="S",12,24))
 ELSE
  *
  *  NOTE:  if you change the width of the Usr_Password
  *  field, you must change the 2nd PADR() value to the 
  *  same value
  *
  ALTER TABLE Users ALTER COLUMN Usr_Password C(8) DEFAULT X3ED("E",PADR("PASSWORD",8))
ENDIF

*
*  the following fields are OPTIONAL and can be removed
*  without crashing any VMP framework classes like
*  XXFWSEC.VCX/frmDEUsers, frmUserProperties, ctrUsersBoundControls
*  that refer to them (those classes are "smart" enough
*  to remove any controls that would be bound to these
*  fields if they don't exist):
*    Usr_Notes
*    Usr_Phone
*    Usr_PhoneExt
*    Usr_Fax
*

SELECT USERS

DO IndexUsersTable WITH tcAppPrefix

*
*  note that you might want additional non-candidate keys
*  on the Usr_Name and Usr_LanID tags because those (filtered)
*  tags are not Rushmore optimizable -- we have not created them
*  by default because there are generally a relatively 
*  small number of users, and the performance benefit for
*  SQL selects and joins, filters, etc. is probably not
*  worth the cost of adding 2 more tags
*
*!*	INDEX ON UPPER(Usr_LanID) + Usr_PassWord TAG Usr_IDPass
*!*	INDEX ON DELETED() TAG Usr_Del

DBSETPROP("USERS",                   "TABLE","Comment","Users")
DBSETPROP("USERS.Usr_PK",            "FIELD","Caption","Primary Key")
DBSETPROP("USERS.Usr_PK",            "FIELD","Comment","System-generated Primary Key")
DBSETPROP("USERS.Usr_LanID",         "FIELD","Caption","UserName")
DBSETPROP("USERS.Usr_LanID",         "FIELD","Comment","User's UserName/ID (login UserID)")
DBSETPROP("USERS.Usr_Password",      "FIELD","Caption","Password")
DBSETPROP("USERS.Usr_Password",      "FIELD","Comment","User's Password")
DBSETPROP("USERS.Usr_Rights",        "FIELD","Caption","Rights")
DBSETPROP("USERS.Usr_Rights",        "FIELD","Comment","Ignored if optional VMP user-security system is installed (XXFWSEC.VCX/cusUserSecurity).  Global app-specific user rights: 1=Administrator, 5=User (RW), 8=Guest (RO), any additional values specified in XXFW.VCX/cusUserSec.SetRightsArray()")
DBSETPROP("USERS.Usr_System",        "FIELD","Caption","System")
DBSETPROP("USERS.Usr_System",        "FIELD","Comment",[Set to "Y", indicates that this user is a 'hidden' user, and does not show up on any form/control interfaces])
DBSETPROP("USERS.Usr_Active",        "FIELD","Caption","Active")
DBSETPROP("USERS.Usr_Active",        "FIELD","Comment","Indicates whether this user is an Active account or not (if inactive, that UserID+Password will not be able to successfully login")
DBSETPROP("USERS.Usr_LastLogin",     "FIELD","Caption","Last Login")
DBSETPROP("USERS.Usr_LastLogin",     "FIELD","Comment","Indicates the last time this user logged into the app (see oUser.LogLoginLogout).  Optional field, can be deleted, VMP framework will ignore it, including removing from XXFWSEC.VCXctrUsersBoundControls at runtime")
DBSETPROP("USERS.Usr_FirstName",     "FIELD","Caption","First Name")
DBSETPROP("USERS.Usr_MiddleInitial", "FIELD","Caption","Middle Initial")
DBSETPROP("USERS.Usr_LastName",      "FIELD","Caption","Last Name")
DBSETPROP("USERS.Usr_EnablePrefs",   "FIELD","Caption","Preferences?")
DBSETPROP("USERS.Usr_EnablePrefs",   "FIELD","Comment","Indicates whether this user wants preferences saved/restored by default")
DBSETPROP("USERS.Usr_INTLLanguage",  "FIELD","Caption","Language")
DBSETPROP("USERS.Usr_INTLLanguage",  "FIELD","Comment","Language of preference for this user, requires Steven Black's INTL Toolkit")
IF X6ISFLD("USERS.Usr_Phone")  &&& optional field
  DBSETPROP("USERS.Usr_Phone",       "FIELD","Caption","Phone Number") 
  DBSETPROP("USERS.Usr_Phone",       "FIELD","Comment","Optional field, can be deleted ignored in the VMP framework, including removal in XXFWSEC.VCX/ctrUsersBoundControls")
ENDIF
IF X6ISFLD("USERS.Usr_PhoneExt")  &&& optional field
  DBSETPROP("USERS.Usr_PhoneExt",    "FIELD","Caption","Ext")
  DBSETPROP("USERS.Usr_PhoneExt",    "FIELD","Comment","Optional field, can be deleted ignored in the VMP framework, including removal in XXFWSEC.VCX/ctrUsersBoundControls")
ENDIF
IF X6ISFLD("USERS.Usr_Fax")  &&& optional field
  DBSETPROP("USERS.Usr_Fax",         "FIELD","Caption","Fax Number") 
  DBSETPROP("USERS.Usr_Fax",         "FIELD","Comment","Optional field, can be deleted ignored in the VMP framework, including removal in XXFWSEC.VCX/ctrUsersBoundControls")
ENDIF
IF X6ISFLD("USERS.Usr_Notes")  &&& optional field
  DBSETPROP("USERS.Usr_Notes",       "FIELD","Caption","Notes") 
  DBSETPROP("USERS.Usr_Notes",       "FIELD","Comment","Optional field, can be deleted ignored in the VMP framework, including removal in XXFWSEC.VCX/frmDEUsers and XXFWSEC.VCX/frmUserProperties")
ENDIF

*
*  Field rule on Users.Usr_Rights -- note that 0 is
*  allowed, so you can add users at the Command Window
*  that are "super-duper" users (developers, maybe?)
*  for apps that don't install the optional VMP user
*  security system
*
ALTER TABLE USERS ;
     ALTER COLUMN Usr_Rights ;
       SET CHECK INLIST(Usr_Rights,0,1,5,8) ;
       ERROR XXFWRuleErrorText("FR_Usr_Rights","User Rights must be specified: 1=Administrator, 5=User, or 8=Guest")
*
*  Field rule on Users.Usr_Active
*
ALTER TABLE USERS ;
     ALTER COLUMN Usr_Active ;
       SET CHECK INLIST(Usr_Active,"Y","N") ;
       ERROR XXFWRuleErrorText("FR_Usr_Active",[Active indicator must be "Y"es or "N"o])
*
*  Field rule on Users.Usr_System
*
ALTER TABLE USERS ;
     ALTER COLUMN Usr_System ;
       SET CHECK INLIST(Usr_System,"Y","N") ;
       ERROR XXFWRuleErrorText("FR_Usr_System",[System indicator must be "Y"es or "N"o])
*
*  Field rule on Users.Usr_Phone
*
*!*	ALTER TABLE USERS ;
*!*	     ALTER COLUMN Usr_Phone;
*!*	       SET CHECK x3vusphn(Usr_Phone) ;
*!*	       ERROR XXFWRuleErrorText("FR_Phone","Phone number is invalid") 
*
*  Field rule on Users.Usr_Fax
*
*!*	ALTER TABLE USERS ;
*!*	     ALTER COLUMN Usr_Fax ;
*!*	       SET CHECK x3vusphn(Usr_Fax) ;
*!*	       ERROR XXFWRuleErrorText("FR_Fax","Fax number is invalid")
*
*  Field rule on Users.Usr_Password
*
IF VARTYPE(m.glX8ED) = "L" AND m.glX8ED
  *
  *  coming in a future release of VMP 2005 
  *
  *  NOTE:  no field rule Usr_Password -- when X8ED() 
  *  runs and creates the goED object, oAppConfig (and
  *  therefore oAppConfig.PaPhFactor) is only available
  *  in an application context, and too complicated to
  *  be worth trying to code in a Field Rule, although
  *  you might come up with something that works for
  *  you in an app-specific context
  *
 ELSE
  ALTER TABLE USERS ;
       ALTER COLUMN Usr_Password ;
         SET CHECK NOT X3ED("D",Usr_Password) = Usr_LanID ;
              AND NOT SPACE(1) $ ALLTRIM(X3ED("D",Usr_Password)) ;
              AND NOT EMPTY(CHRTRANC(ALLTRIM(X3ED("D",Usr_Password)),LEFTC(X3ED("D",Usr_Password),1),SPACE(0))) ;
         ERROR XXFWRuleErrorText("FR_Usr_Password","Password may not:  1) be the same as UserName/UserID; 2) contain embedded spaces/blanks; 3) consist of all the same character/number.  The Password you have entered violates one or more of these rules.") 
ENDIF

*
*  Row rule 
*
ALTER TABLE USERS ;
     SET CHECK NOT EMPTY(Usr_PK) ;
           AND NOT EMPTY(Usr_FirstName) ;
           AND NOT EMPTY(Usr_LastName) ;
           AND NOT EMPTY(Usr_LanID) ;
           AND NOT EMPTY(Usr_Password) ;
     ERROR XXFWRuleErrorText("RR_USERS","Primary Key, First Name, Last Name, UserName/ID, Password, and Rights must all be entered.") 

*
*  add 2 default Users to get you started --
*  gotta have something here in order to log in
*
IF VARTYPE(m.glX8ED) = "L" AND m.glX8ED
  *  NOTE:  no DEFAULT for Usr_Password -- when X8ED() 
  *  runs and creates the goED object, oAppConfig (and
  *  therefore oAppConfig.PaPhFactor) is only available
  *  in an application context, and too complicated to
  *  be worth trying to code in a DefaultValue
  *
  m.Usr_Password = X8ED("E","PASSWORD",m.lcPassPhrase,.f.,m.lcStringOrBlock,.t.,.f.,.t.)
ENDIF

SELECT USERS
m.Usr_PK = IIF(tlUseIntegerPKFields,2000000001,"DEFA")
m.Usr_Rights = 1
m.Usr_FirstName = "Dudley"
m.Usr_MiddleInitial = "E"
m.Usr_LastName = "Fault"
m.Usr_LanID = "DEFAULT"
m.Usr_System = "N"

INSERT INTO Users FROM MEMVAR 
m.Usr_PK = IIF(tlUseIntegerPKFields,2000000002,"EXTR")
m.Usr_Rights = 8
m.Usr_FirstName = "External"
m.Usr_MiddleInitial = "X"
m.Usr_Lastname = "Ternal"
m.Usr_LanID = "EXTERNAL"
m.Usr_System = "Y"
*  if m.llX8ED, same m.Usr_Password as for Dudley E. Fault,
*  if NOT m.llX8ED, DefaultValue on Usr_Password yields "PASSWORD"
INSERT INTO Users FROM MEMVAR 

*!*	INSERT INTO USERS (Usr_PK, ;
*!*	                   Usr_Rights, ;
*!*	                   Usr_FirstName, ;
*!*	                   Usr_MiddleInitial, ;
*!*	                   Usr_LastName, ;
*!*	                   Usr_LanID) ;
*!*	                   VALUES ;
*!*	                  (IIF(tlUseIntegerPKFields,2000000001,"DEFA"), ;
*!*	                   1, ;
*!*	                   "Dudley", ;
*!*	                   "E", ;
*!*	                   "Fault", ;
*!*	                   "DEFAULT")
*!*	INSERT INTO USERS (Usr_PK, ;
*!*	                   Usr_Rights, ;
*!*	                   Usr_FirstName, ;
*!*	                   Usr_MiddleInitial, ;
*!*	                   Usr_LastName, ;
*!*	                   Usr_LanID, ;
*!*	                   Usr_System) ;
*!*	                   VALUES ;
*!*	                  (IIF(tlUseIntegerPKFields,2000000002,"EXTR"), ;
*!*	                   8, ;
*!*	                   "External", ;
*!*	                   "X", ;
*!*	                   "Ternal", ;
*!*	                   "EXTERNAL", ;
*!*	                   "Y")

CreateViewsByTable("Users")
RETURN



***************************************************
PROCEDURE CreateUserPrefsTable
***************************************************
LPARAMETERS tcAppPrefix, tlUseIntegerPKFields
WAIT WINDOW "UserPrefs..." NOWAIT
LOCAL lcDBCDir
lcDBCDir = GetDBCDir()
IF FILE(lcDBCDir + tcAppPrefix + "USRPRF.DBF")
  IF INDBC("USERPREFS","TABLE")
    REMOVE TABLE USERPREFS DELETE
  ENDIF    
ENDIF

lcPKField = "PRF_USRFK" + SPACE(1) + IIF(tlUseIntegerPKFields,"I(4)","C(5)")
CREATE TABLE (lcDBCDir + tcAppPrefix + "USRPRF") NAME USERPREFS ;
     (&lcPKField., ;
      Prf_Item             C(220) , ;  
      Prf_ItemDataType     C(1)  , ;
      Prf_ItemValue        C(254) NULL, ;
      Prf_DesignedValue    C(254) NULL, ;
      Prf_ItemDescription  C(254))
SELECT USERPREFS

* AB - 10/2004 - Start
DO IndexUserPrefsTable WITH tcAppPrefix

*!*	IF tlUseIntegerPKFields
*!*	*!*	  ALTER TABLE USERPREFS ADD PRIMARY KEY PADL(Prf_UsrFK,10) + UPPER(Prf_Item) TAG Prf_PK	
*!*	  INDEX ON PADL(Prf_UsrFK,10) + UPPER(Prf_Item) TAG Prf_CK ;
*!*	       CANDIDATE FOR NOT DELETED()
*!*	 ELSE
*!*	*!*	  ALTER TABLE USERPREFS ADD PRIMARY KEY Prf_UsrFK + UPPER(Prf_Item) TAG Prf_PK 
*!*	  INDEX ON Prf_UsrFK + UPPER(Prf_Item) TAG Prf_CK ;
*!*	       CANDIDATE FOR NOT DELETED()
*!*	ENDIF

*!*	INDEX ON Prf_UsrFK TAG Prf_UsrFK
*!*	INDEX ON DELETED() TAG Prf_Del
* AB - 10/2004 - End

DBSETPROP("UserPrefs",                     "TABLE", "Comment", "User Preferences")
DBSETPROP("UserPrefs.Prf_UsrFK",           "FIELD", "Comment", "Foreign Key to the USERS table")
DBSETPROP("UserPrefs.Prf_Item",            "FIELD", "Comment", "Preference item")
DBSETPROP("UserPrefs.Prf_ItemDataType",    "FIELD", "Comment", "Data type for Prf_ItemValue (C,N,L,D,T)")
DBSETPROP("UserPrefs.Prf_ItemValue",       "FIELD", "Comment", "Prf_Item value")
DBSETPROP("UserPrefs.Prf_ItemDescription", "FIELD", "Comment", "Description of Prf_Item")

*!*	ALTER TABLE USERPREFS ADD FOREIGN KEY TAG Prf_UsrFK REFERENCES USERS		&& AB - 10/2004 - commented out

IF !USED("USERS")
  USE USERS IN 0
ENDIF
SELECT USERS
SCAN
  INSERT INTO UserPrefs ;
      (Prf_UsrFK, Prf_Item, Prf_ItemDataType, Prf_ItemValue) ;
      VALUES ;
      (USERS.Usr_PK, "ScreenWindowState", "N", "2")
  INSERT INTO UserPrefs ;
      (Prf_UsrFK, Prf_Item, Prf_ItemDataType, Prf_ItemValue) ;
      VALUES ;
      (USERS.Usr_PK, "DevTbrDockPosition", "N", "-1")
ENDSCAN

CreateViewsByTable("UserPrefs")
RETURN



********************************************************
PROCEDURE CreateLoginHistoryTable
********************************************************
LPARAMETERS tcAppPrefix, tlUseIntegerPKFields
WAIT WINDOW "LoginHistory..." NOWAIT
LOCAL lcDBCDir
lcDBCDir = GetDBCDir()
IF FILE(m.lcDBCDir + m.tcAppPrefix + "USYSLH.DBF")
  IF INDBC("LoginHistory","TABLE")
    REMOVE TABLE LoginHistory DELETE
  ENDIF    
ENDIF
CREATE TABLE (m.lcDBCDir + m.tcAppPrefix + "USYSLH") NAME LoginHistory ;
     (ULH_PK      C(5), ;
      ULH_UsrFK   C(5), ;
      ULH_In      T, ;
      ULH_Out     T NULL DEFAULT .NULL., ;
      ULH_OutFlag C(1) DEFAULT "U", ;
      ULH_Machine C(40) DEFAULT SYS(0), ;     
      ULH_UUIDNA  C(12) DEFAULT X3UUIDNA(), ;
      ULH_WstFK   C(5))   &&& future possible Workstation enhancement
SELECT LoginHistory
IF m.tlUseIntegerPKFields
  ALTER TABLE LoginHistory ALTER COLUMN ULH_PK I
  ALTER TABLE LoginHistory ALTER COLUMN ULH_UsrFK I
  ALTER TABLE LoginHistory ALTER COLUMN ULH_WstFK I
ENDIF  

* AB - 10/2004 - Start
DO IndexLoginHistoryTable WITH m.tcAppPrefix

*!*	ALTER TABLE LoginHistory ADD PRIMARY KEY ULH_PK TAG ULH_PK
*!*	INDEX ON ULH_UsrFK TAG ULH_UsrFK
*!*	INDEX ON DELETED() TAG ULH_Del
* AB - 10/2004 - End

DBSETPROP("LoginHistory",             "TABLE", "Comment", "Optional table to log each user login/logout")
DBSETPROP("LoginHistory.ULH_UsrFK",   "FIELD", "Comment", "Foreign Key to the USERS table")
DBSETPROP("LoginHistory.ULH_OutFlag", "FIELD", "Comment", "Indicates how the logout occurred.  See XXFW.VCX/cusUser.LogLoginLogout()") 
DBSETPROP("LoginHistory.ULH_UUIDNA",  "FIELD", "Comment", "Universal Unique ID/Network Address from the Registry (HKEY_LocalMachine/Software/Description/Microsoft/RPC/UuidTemporaryData) see X3UUIDNA.PRG")

*!*	ALTER TABLE LoginHistory ADD FOREIGN KEY TAG ULH_UsrFK REFERENCES USERS		&& AB - 10/2004 - commented out

*
*  Field rule on LoginHistory.ULH_UsrFK
*
lcJunk = [ALTER TABLE LoginHistory ALTER COLUMN ULH_UsrFK SET CHECK IIF(_TriggerLevel>0,.t.,X3SEEK("] + ;
     JUSTSTEM(CURSORGETPROP("Database",ALIAS())) + ;
     [",ULH_UsrFK,"USERS","Usr_PK",.t.)) ERROR XXFWRuleErrorText("FR_ULH_UsrFK","The User you have entered is not on file in the Users reference file")]
&lcJunk.
*
*  Field rule on LoginHistory.ULH_In
*
ALTER TABLE LoginHistory ;
     ALTER COLUMN ULH_In ;
       SET CHECK NOT EMPTY(ULH_In) AND ULH_In <= DATETIME() ;
       ERROR XXFWRuleErrorText("FR_ULH_In","Login may not be in the future.") 
*
*  Field rule on LoginHistory.ULH_Out
*
ALTER TABLE LoginHistory ;
     ALTER COLUMN ULH_Out ;
       SET CHECK ISNULL(ULH_Out) OR (NOT EMPTY(ULH_Out) AND ULH_Out <= DATETIME()) ;
       ERROR XXFWRuleErrorText("FR_ULH_Out","Logout may not be in the future.") 
*
*  Field rule on LoginHistory.ULH_OutFlag
*
ALTER TABLE LoginHistory ;
     ALTER COLUMN ULH_OutFlag ;
       SET CHECK INLIST(ULH_OutFlag,"A","E","N","S","T","U") ;
       ERROR XXFWRuleErrorText("FR_ULH_OutFlag","Logout flag must be one of the following:  'A', 'E', 'N', 'S', 'T', 'U'")
*
*  Row rule 
*
ALTER TABLE LoginHistory ;
     SET CHECK NOT EMPTY(ULH_PK) ;
           AND NOT EMPTY(ULH_In) ;
           AND (ISNULL(ULH_Out) OR NOT EMPTY(ULH_Out)) ;
           AND NOT EMPTY(ULH_OutFlag) ;
     ERROR XXFWRuleErrorText("RR_LoginHistory","Primary Key, In, Out, and Logout Flag must all be entered")
CreateViewsByTable("LoginHistory")
RETURN




*********************************************************
PROCEDURE CreateLoggedInUsersTable
*********************************************************
LPARAMETERS tcAppPrefix, tlUseIntegerPKFields
WAIT WINDOW "LoggedInUsers..." NOWAIT
LOCAL lcDBCDir
lcDBCDir = GetDBCDir()
IF FILE(m.lcDBCDir + m.tcAppPrefix + "USYSCL.DBF")
  IF INDBC("LoggedInUsers","TABLE")
    REMOVE TABLE LoggedInUsers DELETE
  ENDIF    
ENDIF
*
*  table containing one record per user+workstation/machine
*
*  see XXFW.VCX/cusUser.TrackLoggedInUser()
*        MODIFY CLASS cusUser of XXFW METHOD TrackLoggedInUser
*      XXFW.VCX/cusUser.LoginFromAnotherWorkstationCheck()
*        MODIFY CLASS cusUser OF XXFW METHOD LoginFromAnotherWorkstationCheck
*
IF VARTYPE(m.glX8ED) = "L" AND m.glX8ED
  *
  *  coming in a future release of VMP 2005 
  *
  CREATE TABLE (lcDBCDir + tcAppPrefix + "USYSCL") NAME LoggedInUsers ;
       (CLU_FK     C(35), ;     &&& Foreign Key to the Users table, X8ED()-encrypted
        CLU_MK     C(72) , ;    &&& SYS(0) machine information, X8ED()-encrypted
        CLU_DT     T DEFAULT DATETIME(), ;
        CLU_MK1    C(40), ;     &&& X3UUIDNA() value, X8ED()-encrypted
        CLU_FK1    C(35))       &&& FK to a possible future Workstation table, X8ED()-encrypted

 ELSE
  CREATE TABLE (lcDBCDir + tcAppPrefix + "USYSCL") NAME LoggedInUsers ;
       (CLU_FK     C(5), ;      &&& Foreign Key to the Users table, X3ED()-encrypted
        CLU_MK     C(20) , ;    &&& SYS(0) machine information, X3ED()-encrypted
        CLU_DT     T DEFAULT DATETIME(), ;
        CLU_MK1    C(12), ;     &&& X3UUIDNA() value, X3ED()-encrypted
        CLU_FK1    C(5))        &&& FK to a possible future Workstation table, X3ED()-encrypted
  IF m.tlUseIntegerPKFields
    *  the foreign keys are C(10) if you're using
    *  integer keys because we store this value
    *  as an encrypted string to keep this information
    *  as obscure as possible...
    ALTER TABLE LoggedInUsers ALTER COLUMN CLU_FK  C(10)
    ALTER TABLE LoggedInUsers ALTER COLUMN CLU_FK1 C(10)
  ENDIF  
ENDIF
*
*  no default values, etc. because we want to
*  keep this table as obscure as possible
*     
RETURN



********************************************************
PROCEDURE CreateReportCatalogTable
********************************************************
LPARAMETERS tcAppPrefix, tlUseIntegerPKFields
WAIT WINDOW "ReportCatalog..." NOWAIT 
LOCAL lcDBCDir
lcDBCDir = GetDBCDir()
IF FILE(lcDBCDir + tcAppPrefix + "REPCAT.DBF")
  IF INDBC("REPORTCATALOG","TABLE")
    REMOVE TABLE REPORTCATALOG DELETE 
  ENDIF    
ENDIF

lcPKField = "REP_PK" + SPACE(1) + IIF(tlUseIntegerPKFields,"I(4)","C(5)")
CREATE TABLE (lcDBCDir + tcAppPrefix + "REPCAT") NAME REPORTCATALOG ;
        (&lcPKField.,;
         Rep_Name        C(80), ;
         Rep_Description M(4), ;
         Rep_FRX         C(60), ;
         Rep_Class       C(100), ;
         Rep_Type        C(1), ;
         Rep_Dialog      C(100), ;
         Rep_FRXString   C(21), ;
         Rep_Added       C(1) DEFAULT "S")

* AB - 10/2004 - Start
DO IndexReportCatalogTable WITH tcAppPrefix
*!*	ALTER TABLE REPORTCATALOG ADD PRIMARY KEY Rep_PK TAG Rep_PK
*!*	INDEX ON UPPER(Rep_Name) TAG Rep_Name
*!*	INDEX ON UPPER(Rep_FRX) TAG Rep_FRX
* AB - 10/2004 - End

DBSETPROP("REPORTCATALOG",          	   "Table", "Comment", "Report Catalog")
DBSETPROP("REPORTCATALOG.Rep_PK",          "Field", "Caption", "Primary Key" )
DBSETPROP("REPORTCATALOG.Rep_FRX",         "Field", "Caption", ".FRX Filename" )
DBSETPROP("REPORTCATALOG.Rep_FRX",         "Field", "Comment", "Required if EMPTY(Rep_Class) and EMPTY(Rep_Dialog), optional otherwise, specifies the name of the .FRX to be run")
DBSETPROP("REPORTCATALOG.Rep_Class",       "Field", "Caption", "Class")
DBSETPROP("REPORTCATALOG.Rep_Class",       "Field", "Comment", "Required if EMPTY(Rep_FRX) and EMPTY(Rep_Dialog), ignored if !EMPTY(Rep_Dialog), specifies [ClassName,VCXFile] of a report object inheriting from XXFWMISC.VCX/frmReport")
DBSETPROP("REPORTCATALOG.Rep_Name",        "Field", "Caption", "Name" )
DBSETPROP("REPORTCATALOG.Rep_Description", "Field", "Caption", "Description" )
DBSETPROP("REPORTCATALOG.Rep_Type",        "Field", "Caption", "Type" )
DBSETPROP("REPORTCATALOG.Rep_Type",        "Field", "Comment", ["R"eport or "L"abel or any character you want, to provide a filter for XXFWFRM.VCX/frmReportCatalog] )
DBSETPROP("REPORTCATALOG.Rep_Dialog",      "Field", "Caption", "Dialog" )
DBSETPROP("REPORTCATALOG.Rep_Dialog",      "Field", "Comment", "Required if EMPTY(Rep_Class) and EMPTY(Rep_FRX), if set takes precedence over Rep_Class and specifies an .SCX or [ClassName,VCXFile] dialog that ultimately runs a frmReport object")
DBSETPROP("REPORTCATALOG.Rep_Added",       "Field", "Comment", [Indicates how this record was added, defaults to "S"ystem, allowing you to set to "U"ser-added when/if you allow users to add reports, and thereby distinguish system/base/permanent reports from those added by users, administrators, etc. in production])

*
*  Row rule 
*
ALTER TABLE ReportCatalog ;
     SET CHECK NOT EMPTY(Rep_PK) ;
           AND NOT EMPTY(Rep_Name) ;
           AND NOT EMPTY(Rep_FRX+Rep_Class+Rep_Dialog) ;
     ERROR XXFWRuleErrorText("RR_ReportCatalog","Primary Key, Name, Type, and either .FRX Filename/Class/Dialog must all be entered")

**************************************
*  add VMP framework-supplied user security reports
**************************************

xx = RECCOUNT("REPORTCATALOG") + 1
IF !tlUseIntegerPKFields
	xx = PADL(xx,LENC(REPORTCATALOG.Rep_PK),"0")
ENDIF
INSERT INTO REPORTCATALOG ;
     (Rep_PK, Rep_FRX, Rep_Name, Rep_Type, Rep_Class, Rep_Description, Rep_FRXString) ;
     VALUES (xx, ;
             "XXUSYS1.FRX", ;
             "Users Listing", ;
             "R", ;
             "frmReportUSYS1,xxfwsec.vcx", ;
             "Simple listing of Users", ;
             "Listing of all Users Report")
IF INDBC("SecurityAccess","TABLE")
  *
  *  optional VMP user security system tables exist
  *
  xx = RECCOUNT("REPORTCATALOG") + 1
  IF !tlUseIntegerPKFields
    xx = PADL(xx,LENC(REPORTCATALOG.Rep_PK),"0")
  ENDIF
  INSERT INTO REPORTCATALOG ;
       (Rep_PK, Rep_FRX, Rep_Name, Rep_Type, Rep_Class, Rep_Description, Rep_FRXString) ;
       VALUES (xx, ;
               "XXUSYS2.FRX", ;
               "Users and the Groups to which they belong", ;
               "R", ;
               "frmReportUSYS2,xxfwsec.vcx", ;
               "Simple listing of Users and the Groups to which each belongs", ;
               "Users and Groups Report")
  xx = RECCOUNT("REPORTCATALOG") + 1
  IF !tlUseIntegerPKFields
    xx = PADL(xx,LENC(REPORTCATALOG.Rep_PK),"0")
  ENDIF
  INSERT INTO REPORTCATALOG ;
       (Rep_PK, Rep_FRX, Rep_Name, Rep_Type, Rep_Class, Rep_Description, Rep_FRXString) ;
       VALUES (xx, ;
               "XXUSYSG1.FRX", ;
               "Groups and their Members", ;
               "R", ;
               "frmReportUSYSG1,xxfwsec.vcx", ;
               "Simple listing of Groups and the Users who are Members", ;
               "Groups and Members Report")
ENDIF

IF INDBC("GENERATEPK","TABLE") 
  IF !USED("GeneratePK")
    USE GeneratePK IN 0
  ENDIF
  SELECT GeneratePK
  LOCATE FOR UPPER(GPK_PK) = "REPORTCATALOG"
  IF !FOUND()
    APPEND BLANK
    REPLACE GPK_PK WITH "REPORTCATALOG"
  ENDIF
  REPLACE GPK_CurrentNumber WITH RECCOUNT("ReportCatalog")  
ENDIF
RETURN



************************************************************
PROCEDURE CreateSecurityTables
************************************************************
LPARAMETERS tcAppPrefix, tlUseIntegerPKFields
WAIT WINDOW "Security tables..." NOWAIT
LOCAL lcDBCDir
lcDBCDir = GetDBCDir()
IF FILE(lcDBCDir + tcAppPrefix + "USYSAE.DBF")
  IF INDBC("UserAccExceptions","TABLE")
    REMOVE TABLE UserAccExceptions DELETE
  ENDIF    
ENDIF
IF FILE(lcDBCDir + tcAppPrefix + "USYSGX.DBF")
  IF INDBC("UsrGrpXRef","TABLE")
    REMOVE TABLE UsrGrpXRef DELETE
  ENDIF    
ENDIF
IF FILE(lcDBCDir + tcAppPrefix + "USYSAC.DBF")
  IF INDBC("SecurityAccess","TABLE")
    REMOVE TABLE SecurityAccess DELETE
  ENDIF
ENDIF
IF FILE(lcDBCDir + tcAppPrefix + "USYSG.DBF")
  IF INDBC("UserGroups","TABLE")
    REMOVE TABLE UserGroups DELETE
  ENDIF    
ENDIF
IF FILE(lcDBCDir + tcAppPrefix + "USYSSI.DBF")
  * free table
  ERASE (tcAppPrefix+"USYSSI.DBF")
  ERASE (tcAppPrefix+"USYSSI.CDX")
ENDIF

***********************************************************
*  UserGroups (User Groups table)
***********************************************************
WAIT WINDOW "UserGroups..." NOWAIT
CREATE TABLE (lcDBCDir + tcAppPrefix + "USYSG") NAME UserGroups ;
        (UGr_PK          C(5), ; 
         UGr_Group       C(20), ;
         UGr_Description C(60), ;
         UGr_System      C(1) DEFAULT "N")

DO IndexUserGroupsTable WITH tcAppPrefix		&& AB - 10/2004

IF tlUseIntegerPKFields
  ALTER TABLE UserGroups ALTER COLUMN UGR_PK I
ENDIF

* AB - 10/2004 - Start
*!*	ALTER TABLE UserGroups ADD PRIMARY KEY UGr_PK TAG UGr_PK
*!*	INDEX ON UPPER(UGr_Group) TAG UGr_Group CANDIDATE FOR NOT DELETED()
*!*	INDEX ON UPPER(UGr_Description) TAG UGr_Desc CANDIDATE FOR NOT DELETED()
*!*	*
*!*	*  note that you might want additional non-candidate keys
*!*	*  on the above two expressions because filtered tags are
*!*	*  not Rushmore optimizable -- we have not created them
*!*	*  by default because there are generally a relatively 
*!*	*  small number of user groups, and the performance benefit for
*!*	*  SQL selects and joins, filters, etc. is probably not
*!*	*  worth the cost of adding 2 more tags
*!*	*
*!*	INDEX ON DELETED() TAG UGr_Del
* AB - 10/2004 - End

DBSETPROP( "UserGroups",                 "Table", "Comment", "This is an optional table used in the VMP user-security system, containing one record per UserGroup" )
DBSETPROP( "UserGroups.UGr_PK",          "Field", "Caption", "Primary Key" )
DBSETPROP( "UserGroups.UGr_Group",       "Field", "Caption", "Group")
DBSETPROP( "UserGroups.UGr_Description", "Field", "Caption", "Description" )
DBSETPROP( "UserGroups.UGr_System",      "FIELD", "Caption", "System")
DBSETPROP( "USerGroups.UGr_System",      "FIELD", "Comment", [Set to "Y", indicates that this user is a 'hidden' group, and does not show up on any form/control interfaces])

*
*  Row rule 
*
ALTER TABLE UserGroups ;
     SET CHECK !EMPTY(UGr_PK) ;
           AND !EMPTY(UGr_Group) ;
           AND !EMPTY(UGr_Description) ;
           AND !EMPTY(UGr_System) ;
     ERROR XXFWRuleErrorText("RR_UserGroups","Primary Key, Group, Description, and Rights must all be entered") 

*
*  Field rule on UserGroups.UGr_System
*
ALTER TABLE USERGROUPS ;
     ALTER COLUMN UGr_System ;
       SET CHECK INLIST(UGr_System,"Y","N") ;
       ERROR XXFWRuleErrorText("FR_UGr_System",[System indicator must be "Y"es or "N"o])

CreateViewsByTable("UserGroups")

*
*  add default groups
*
m.UGr_PK = IIF(tlUseIntegerPKFields,1,"00001")
m.UGr_Group = "Administrators"
m.UGr_Description = "System administrators"
INSERT INTO UserGroups FROM MEMVAR
m.UGr_PK = IIF(tlUseIntegerPKFields,2,"00002")
m.UGr_Group = "Supervisors"
m.UGr_Description = "Supervisors"
INSERT INTO UserGroups FROM MEMVAR
m.UGr_PK = IIF(tlUseIntegerPKFields,3,"00003")
m.UGr_Group = "Users"
m.UGr_Description = "All users"
INSERT INTO UserGroups FROM MEMVAR
m.UGr_PK = IIF(tlUseIntegerPKFields,4,"00004")
m.UGr_Group = "Guests"
m.UGr_Description = "Guests"
INSERT INTO UserGroups FROM MEMVAR

LOCAL lcDatabasePlusTable, llError
lcDatabasePlusTable = UPPER(JUSTSTEM(CURSORGETPROP("DATABASE","UserGroups"))) + "!GeneratePK"

IF INDBC("GeneratePK","TABLE")
  IF NOT USED("GeneratePK")
    USE GeneratePK IN 0
  ENDIF
  SELECT GeneratePK
  LOCATE FOR UPPER(ALLTRIM(GPK_PK)) == "USERGROUPS"
  llError = .f.
  TRY
  IF FOUND()
    REPLACE GPK_CurrentNumber WITH 4 IN GeneratePK
   ELSE
    INSERT INTO (lcDatabasePlusTable) ;
         (GPK_PK, GPK_CurrentNumber) ;
         VALUES ;
         ("USERGROUPS",4)
  ENDIF
  CATCH
  llError = .t.
  ENDTRY
  IF llError 
    MESSAGEBOX("Unable to update " + lcDatabasePlusTable + " after inserting 4 default records into UserGroups table.  See XXFWDATA.PRG",48,"Please Note")
  ENDIF
ENDIF


***********************************************************
*  UsrGrpXRef (User Group cross-reference table)
***********************************************************
WAIT WINDOW "UsrGrpXRef..." NOWAIT
CREATE TABLE (lcDBCDir + tcAppPrefix + "USYSGX") NAME UsrGrpXRef ;
        (UGX_PK C(5), ;
         UGX_UGrFK C(5), ;
         UGX_MemberFK C(5), ;
         UGX_MemberType C(1) DEFAULT "U")

* AB - 10/2004 - Start
IF tlUseIntegerPKFields
  ALTER TABLE UsrGrpXRef ALTER COLUMN UGX_PK I
  ALTER TABLE UsrGrpXRef ALTER COLUMN UGX_UGrFK I
  ALTER TABLE UsrGrpXRef ALTER COLUMN UGX_MemberFK I
ENDIF

DO IndexUsrGrpXRefTable WITH tcAppPrefix

*!*	ALTER TABLE UsrGrpXRef ADD PRIMARY KEY UGX_PK TAG UGX_PK
*!*	INDEX ON UGX_UGrFK TAG UGX_UGrFK
*!*	INDEX ON UGX_MemberFK TAG UGX_MbrFK
*!*	INDEX ON UGX_MemberType TAG UGX_MbrTyp
*!*	INDEX ON DELETED() TAG UGX_Del
*!*	*

*!*	* Note that the following persistent relations have a FOR condition.
*!*	* This is to prevent RI (both VMP and/or the native VFP RI)
*!*	* from deleting all entries that could possibly contain the
*!*	* same values (e.g.: "00001" for both a user and a group).
*!*	* See also the chapter on Referential Integrity in the 
*!*	* VMP Reference Guide.
*!*	*
*!*	* Also note that VMP RI does not depend on persistent relations.
*!*	*
*!*	*  (we could just go with the index tags as above, but
*!*	*  this way we get the relation lines drawn in the Database
*!*	*  Designer)
*!*	*
*!*	ALTER TABLE UsrGrpXRef ;
*!*	     ADD FOREIGN KEY UGX_MemberFK ;
*!*	     FOR UGX_MemberType = "U" ;
*!*	     TAG UGX_UsrFK ;
*!*	     REFERENCES Users
*!*	ALTER TABLE UsrGrpXRef ;
*!*	     ADD FOREIGN KEY UGX_MemberFK ;
*!*	     FOR UGX_MemberType = "G" ;
*!*	     TAG UGX_GrpFK ;
*!*	     REFERENCES UserGroups
* AB - 10/2004 - End

DBSETPROP( "UsrGrpXRef",                "Table", "Comment", "This is an optional table used in the VMP user-security system, containing one record per user per group (many-to-many link table)" )
DBSETPROP( "UsrGrpXRef.UGX_PK",         "Field", "Caption", "Primary Key" )
DBSETPROP( "UsrGrpXRef.UGX_UGrFK",      "Field", "Caption", "Group")
DBSETPROP( "UsrGrpXRef.UGX_MemberFK",   "Field", "Caption", "Member" )
DBSETPROP( "UsrGrpXRef.UGX_MemberFK",   "Field", "Comment", "User/Group")
DBSETPROP( "UsrGrpXRef.UGX_MemberType", "Field", "Caption", 'Member Type "U"ser or "G"roup')

*!*	lcJunk = [ALTER TABLE UsrGrpXRef ALTER COLUMN UGX_MemberFK SET CHECK ] + ;
*!*	     [IIF(_TriggerLevel>0,.t.,X3SEEK("] + JUSTSTEM(CURSORGETPROP("Database",ALIAS())) + [",UGX_MemberFK,"Users","Usr_PK")) ] +  ;
*!*	     [OR ] + ;
*!*	     [IIF(_TriggerLevel>0,.t.,X3SEEK("] + JUSTSTEM(CURSORGETPROP("Database",ALIAS())) + [",UGX_MemberFK,"Users","Usr_PK",.t.)) ] +  ;
*!*	     [OR ] + ;
*!*	     [IIF(_TriggerLevel>0,.t.,X3SEEK("] + JUSTSTEM(CURSORGETPROP("Database",ALIAS())) + [",UGX_MemberFK,"UserGroups","UGr_PK")) ] + ;
*!*	     [OR ] + ;
*!*	     [IIF(_TriggerLevel>0,.t.,X3SEEK("] + JUSTSTEM(CURSORGETPROP("Database",ALIAS())) + [",UGX_MemberFK,"UserGroups","UGr_PK",.t.)) ] + ;
*!*	     [ERROR XXFWRuleErrorText("FR_UGX_MemberFK","The Member you have entered is not on file.")]
*!*	&lcJunk.

*
*  field rule
*
lcJunk = [ALTER TABLE UsrGrpXRef ALTER COLUMN UGX_UGrFK SET CHECK IIF(_TriggerLevel>0,.t.,X3SEEK("] + ;
     JUSTSTEM(CURSORGETPROP("Database",ALIAS())) + ;
     [",UGX_UGrFK,"USERGROUPS","UGr_PK",.t.)) ERROR XXFWRuleErrorText("FR_UGX_UGrFK","The Group you have entered is not on file in the UserGroups reference file")]
&lcJunk.
*
*  field rule
*
ALTER TABLE UsrGrpXRef ;
     ALTER COLUMN UGX_MemberType ;
     SET CHECK INLIST(UGX_MemberType,"U","G") ;
     ERROR XXFWRuleErrorText("FR_UGX_MemberType","Member Type must be 'U' or 'G'.")

CreateViewsByTable("UsrGrpXRef")


*
*  add records for the 2 default users
*
LOCAL lcUGr_PK
*
*  Dudley E Fault (DEFAULT) goes in the Administrators group
*
lcUGr_PK = LOOKUP(UserGroups.UGr_PK, ;
                  UPPER("Administrators"), ;
                  UserGroups.UGr_PK, ;
                  "UGr_Group")
INSERT INTO UsrGrpXRef ;
     (UGX_PK, UGX_UGrFK, UGX_MemberFK) ;
     VALUES ;
     (IIF(tlUseIntegerPKFields,1,"00001"), ;
     lcUGr_PK, ;
     IIF(tlUseIntegerPKFields,2000000001,"DEFA"))
*
*  EXTR goes in the Guests group
*
lcUGr_PK = LOOKUP(UserGroups.UGr_PK, ;
                  UPPER("Guests"), ;
                  UserGroups.UGr_PK, ;
                  "UGr_Group")
INSERT INTO UsrGrpXRef ;
     (UGX_PK, UGX_UGrFK, UGX_MemberFK) ;
     VALUES ;
     (IIF(tlUseIntegerPKFields,2,"00002"), ;
     lcUGr_PK, ;
     IIF(tlUseIntegerPKFields,2000000002,"EXTR"))
*
*  update the GENERATEPK table
*
IF INDBC("GeneratePK","TABLE")
  IF NOT USED("GeneratePK")
    USE GeneratePK IN 0
  ENDIF
  SELECT GeneratePK
  LOCATE FOR UPPER(ALLTRIM(GPK_PK)) == "USRGRPXREF"
  llError = .f.
  TRY
  IF FOUND()
    REPLACE GPK_CurrentNumber WITH 2 IN GeneratePK
   ELSE
    INSERT INTO (lcDatabasePlusTable) ;
         (GPK_PK, GPK_CurrentNumber) ;
         VALUES ;
         ("USRGRPXREF",2)
  ENDIF
  CATCH
  llError = .t.
  ENDTRY
  IF llError 
    MESSAGEBOX("Unable to update " + lcDatabasePlusTable + " after inserting 4 default records into UserGroups table.  See XXFWDATA.PRG",48,"Please Note")
  ENDIF
ENDIF

***********************************************************
*  SecurityAccess
***********************************************************
*
*  one record per SecuredItems per UserGroups, 
*  to define the access for that combination
*
WAIT WINDOW "SecurityAccess..." NOWAIT
CREATE TABLE (lcDBCDir + tcAppPrefix + "USYSAC") NAME SecurityAccess ;
        (Acc_PK C(5), ;
         Acc_UGrFK C(5), ;
         Acc_SItFK C(5), ;
         Acc_Access   N(1) DEFAULT 1)

IF tlUseIntegerPKFields
  ALTER TABLE SecurityAccess ALTER COLUMN Acc_PK I
  ALTER TABLE SecurityAccess ALTER COLUMN Acc_UGrFK I
  ALTER TABLE SecurityAccess ALTER COLUMN Acc_SItFK I
ENDIF

DO IndexSecurityAccessTable WITH tcAppPrefix	&& AB - 10/2004

* AB - 10/2004 - Start
*!*	ALTER TABLE SecurityAccess ADD PRIMARY KEY Acc_PK TAG Acc_PK
*!*	INDEX ON Acc_UGrFK TAG Acc_UGrFK
*!*	INDEX ON Acc_SItFK TAG Acc_SItFK
*!*	INDEX ON DELETED() TAG Acc_Del

*!*	ALTER TABLE SecurityAccess ADD FOREIGN KEY TAG Acc_UGrFK REFERENCES UserGroups
*!*	*  SecuredItems is a free table, so there is no Foreign Key Acc_SItFK to it
* AB - 10/2004 - End

DBSETPROP( "SecurityAccess",           "Table", "Comment", "This is an optional table used in the VMP user-security system, defining access restrictions per UserGroup on individual SecuredItems (many-to-many)" )
DBSETPROP( "SecurityAccess.Acc_PK",    "Field", "Caption", "Primary Key" )
DBSETPROP( "SecurityAccess.Acc_UGrFK", "Field", "Caption", "Group")
DBSETPROP( "SecurityAccess.Acc_SItFK", "Field", "Caption", "Secured Item")
DBSETPROP( "SecurityAccess.Acc_Access","Field", "Caption", "Access")
DBSETPROP( "SecurityAccess.Acc_Access","Field", "Comment", "Indicates the level of access for this group+secured item: 1=Full 8=Readonly 9=None")

*
*  Row rule 
*
ALTER TABLE SecurityAccess ; 
     SET CHECK !EMPTY(Acc_PK) ;
           AND !EMPTY(Acc_UGrFK) ;
           AND !EMPTY(Acc_SItFK) ;
           AND INLIST(Acc_Access,1,8,9) ;
     ERROR XXFWRuleErrorText("RR_SecurityAccess","Primary Key, Group, SecuredItem, and Access must all be entered") 

*
*  field rule
*
lcJunk = [ALTER TABLE SecurityAccess ALTER COLUMN Acc_UGrFK SET CHECK ] + ;
         [IIF(_TriggerLevel>0,.t.,X3SEEK("] + JUSTSTEM(CURSORGETPROP("Database",ALIAS())) + [",Acc_UGrFK,"USERGROUPS","UGr_PK",.t.)) ] + ;
         [OR IIF(_TriggerLevel>0,.t.,X3SEEK("] + JUSTSTEM(CURSORGETPROP("Database",ALIAS())) + [",Acc_UGrFK,"USERGROUPS","UGr_PK")) ] + ;
         [ERROR XXFWRuleErrorText("FR_Acc_UGrFK","The Group you have entered is not on file in the UserGroups reference file")]
&lcJunk.
*
*  field rule
*
lcJunk = [ALTER TABLE SecurityAccess ALTER COLUMN Acc_SItFK SET CHECK IIF(_TriggerLevel>0,.t.,X3SEEK("] + ;
     [",Acc_SItFK,"SECUREDITEMS","SIt_PK")) ERROR XXFWRuleErrorText("FR_Acc_SItFK","Either the SecuredItem you have entered is not on file in the SecuredItems reference file, or the SecuredItems table is unavailable")]
&lcJunk.
*
*  field rule
*
ALTER TABLE SecurityAccess ;
     ALTER COLUMN Acc_Access ;
     SET CHECK INLIST(Acc_Access,1,8,9) ;
     ERROR XXFWRuleErrorText("FR_Acc_Access","The Access code you have entered is invalid")

CreateViewsByTable("SecurityAccess")


***********************************************************
*  SecuredItems -- FREE TABLE
*  This table contains one record per application 
*  component item for which you want to establish
*  some sort of security restriction per UserGroup.
*  This table is maintained by the development team,
*  not from within the application.  It is a free
*  table so that it can be built into the .APP/.EXE
*  for additional security.
*
*  Default valid SIt_Type values:
*     SIt_Type        SIt_TypeCD
*     ===========================
*     "Menu pad"      "1"
*     "Menu bar"      "2"
*     "Form"          "3"
*     "Control"       "4"
*     "Report"        "5"
*     "Label"         "6"
*     "Process"       "7"
*     "Misc"          "8"
*  These values are hard-coded and violate all rules
*  of normalization, but are done this way to avoid
*  having a second table for the SIt_Type values,
*  based on a lookup of the SIt_TypeCD value.
*
* 
*  since SecuredItems is a free table, we have
*  no Referential Integrity built in for the
*    SecuredItems-->SecurityAccess
*    SecuredItems-->UserAccessExceptions
*  but since we have no VMP application interface
*  from which the Administrator can delete 
*  SecuredItems records, we're not worried about it...
*
*  SIt_PK          Primary Key value
*  SIt_Type C(8)   "Menu pad"/"Menu bar"/"Form"/"Control"/"Report"/"Label"/"Process"/"Misc"
*  SIt_TypeCD C(1)    "1"         "2"      "3"     "4"       "5"     "6"      "7"      "8"
*  SIt_ID   C(60)  Unique identifier for each secured item, typically the icSecurityID property
*  Sit_Desc C(50)  User-friendly description, displayed in form interfaces
*
***********************************************************
WAIT WINDOW "SecuredItems..." NOWAIT
LOCAL lcDatabase
lcDatabase = SET("DATABASE")
SET DATABASE TO

lcPKField = "SIT_PK" + SPACE(1) + IIF(tlUseIntegerPKFields,"I(4)","C(5)")
* CREATE (free) TABLE will overwrite any existing ??USYSSI.DBF
CREATE TABLE (lcDBCDir + tcAppPrefix + "USYSSI") ;
        (&lcPKField., ;
         SIt_Type   C(8), ;
         SIt_TypeCD C(1), ;
         SIt_ID     C(60), ;
         SIt_Desc   C(50))
* AB - 10/2004 - Start
DO IndexSecuredItemsTable WITH tcAppPrefix

*!*	INDEX ON SIt_PK TAG SIt_PK CANDIDATE 
*!*	INDEX ON UPPER(SIt_ID) TAG SIt_ID CANDIDATE FOR NOT DELETED()
*!*	*
*!*	*  the following CK tag includes both Description and Type
*!*	*  to two records with the same Description but different
*!*	*  type -- i.e., a "Customers" + "Form" and a "Customers" + "Report"
*!*	*
*!*	*  the following 2 tags are used in the SecuredItems grid
*!*	*  in XXFWSEC.VCX/frmGroupPermissions
*!*	*
*!*	INDEX ON UPPER(SIt_Desc) + UPPER(Sit_TypeCD) TAG SIt_DescT CANDIDATE FOR NOT DELETED()
*!*	INDEX ON UPPER(Sit_TypeCD) + UPPER(Sit_Desc) TAG Sit_TypeD
*!*	*
*!*	*  note that you might want additional non-candidate keys
*!*	*  for the above two candidate keys because filtered tags are
*!*	*  not Rushmore optimizable -- we have not created them
*!*	*  by default because there are generally a relatively 
*!*	*  small number of secured items, and the performance benefit for
*!*	*  SQL selects and joins, filters, etc. is probably not
*!*	*  worth the cost of adding 2 more tags
*!*	*
*!*	INDEX ON SIt_TypeCD TAG SIt_TypeCD 
*!*	INDEX ON SIt_Type   TAG SIt_Type 
* AB - 10/2004 - End

SET DATABASE TO (lcDatabase)

CreateViewsByTable("SecuredItems")

***********************************************************
*  UserAccExceptions (User Access Exceptions)
***********************************************************
*
*  one record per SecuredItems per Users, to specify
*  an Access level that overrides that of the group
*  membership for that user for that secured item
*
WAIT WINDOW "UserAccExceptions..." NOWAIT
lcPKField = "UAE_PK" + SPACE(1) + IIF(tlUseIntegerPKFields,"I(4)","C(5)")
lcUSRFKField = "UAE_USRFK" + SPACE(1) + IIF(tlUseIntegerPKFields,"I(4)","C(5)")
lcSITFKField = "UAE_SITFK" + SPACE(1) + IIF(tlUseIntegerPKFields,"I(4)","C(5)")
CREATE TABLE (lcDBCDir + tcAppPrefix + "USYSAE") NAME UserAccExceptions ;
        (&lcPKField.,;
         &lcUSRFKField., ;
         &lcSITFKField., ;
         UAE_Access   N(1) DEFAULT 9)

* AB - 10/2004 - Start
DO IndexUserAccExceptionsTable WITH tcAppPrefix

*!*	ALTER TABLE UserAccExceptions ADD PRIMARY KEY UAE_PK TAG UAE_PK
*!*	INDEX ON UAE_USrFK TAG UAE_USrFK
*!*	INDEX ON UAE_SItFK TAG UAE_SItFK
*!*	INDEX ON DELETED() TAG UAE_Del

*!*	ALTER TABLE UserAccExceptions ADD FOREIGN KEY TAG UAE_USrFK REFERENCES Users
*!*	*  SecuredItems is a free table, so there is no Foreign Key UAE_SItFK to it
* AB - 10/2004 - End

DBSETPROP( "UserAccExceptions",           "Table", "Comment", "This is an optional table used in the VMP user-security system, defining access restrictions per User on individual SecuredItems (many-to-many), overriding group membership security" )
DBSETPROP( "UserAccExceptions.UAE_PK",    "Field", "Caption", "Primary Key" )
DBSETPROP( "UserAccExceptions.UAE_USrFK", "Field", "Caption", "User")
DBSETPROP( "UserAccExceptions.UAE_SItFK", "Field", "Caption", "Secured Item")
DBSETPROP( "UserAccExceptions.UAE_Access","Field", "Caption", "Access")
DBSETPROP( "UserAccExceptions.UAE_Access","Field", "Comment", "Indicates the level of access for this user+secured item: 1=Full 8=Readonly 9=None")

*
*  Row rule 
*
ALTER TABLE UserAccExceptions; 
     SET CHECK !EMPTY(UAE_PK) ;
           AND !EMPTY(UAE_USrFK) ;
           AND !EMPTY(UAE_SItFK) ;
           AND INLIST(UAE_Access,1,8,9) ;
     ERROR XXFWRuleErrorText("RR_UserAccExceptions","Primary Key, Group, SecuredItem, and Access must all be entered") 

*
*  field rule
*
lcJunk = [ALTER TABLE UserAccExceptions ALTER COLUMN UAE_USrFK SET CHECK IIF(_TriggerLevel>0,.t.,X3SEEK("] + ;
     JUSTSTEM(CURSORGETPROP("Database",ALIAS())) + ;
     [",UAE_USrFK,"USERS","Usr_PK",.t.)) ERROR XXFWRuleErrorText("FR_UAE_USrFK","The User you have entered is not on file in the Users reference file")]
&lcJunk.
*
*  field rule (identical to FR_Acc_SItFK)
*
lcJunk = [ALTER TABLE UserAccExceptions ALTER COLUMN UAE_SItFK SET CHECK IIF(_TriggerLevel>0,.t.,X3SEEK("] + ;
     [",UAE_SItFK,"SECUREDITEMS","SIt_PK")) ERROR XXFWRuleErrorText("FR_Acc_SItFK","Either the SecuredItem you have entered is not on file in the SecuredItems reference file, or the SecuredItems table is unavailable")]
&lcJunk.
*
*  field rule (identical to FR_Acc_Access)
*
ALTER TABLE UserAccExceptions ;
     ALTER COLUMN UAE_Access ;
     SET CHECK INLIST(UAE_Access,1,8,9) ;
     ERROR XXFWRuleErrorText("FR_Acc_Access","The Access code you have entered is invalid")

CreateViewsByTable("UserAccExceptions")

************************************************
*  add two optional reports to the ReportCatalog
************************************************
*
*  see also the local procedure in this .PRG
*  where the ReportCatalog is created
*
IF INDBC("ReportCatalog","TABLE")
  IF !USED("ReportCatalog")
    USE ReportCatalog IN 0 
  ENDIF
  SELECT ReportCatalog
  SET ORDER TO 0
  LOCAL lnPK
  lnPK = 0
  SCAN
    lnPK = lnPK + 1
    IF TYPE("Rep_PK") = "N"
      REPLACE Rep_PK WITH lnPK
     ELSE
      REPLACE Rep_PK WITH PADL(lnPK,5,"0")
    ENDIF
  ENDSCAN
  LOCATE FOR UPPER(Rep_FRX) = "XXUSYS2.FRX"
  IF !FOUND()  
    xx = RECCOUNT("REPORTCATALOG") + 1
    IF !tlUseIntegerPKFields
      xx = PADL(xx,LENC(REPORTCATALOG.Rep_PK),"0")
    ENDIF
    INSERT INTO REPORTCATALOG ;
         (Rep_PK, Rep_FRX, Rep_Name, Rep_Type, Rep_Class, Rep_Description) ;
         VALUES (xx, ;
                 "XXUSYS2.FRX", ;
                 "Users and the Groups to which they belong", ;
                 "R", ;
                 "frmReportUSYS2,xxfwsec.vcx", ;
                 "Simple listing of Users and the Groups to which each belongs")
  ENDIF
  LOCATE FOR UPPER(Rep_FRX) = "XXUSYSG1.FRX"
  IF !FOUND()
    xx = RECCOUNT("REPORTCATALOG") + 1
    IF !tlUseIntegerPKFields
      xx = PADL(xx,LENC(REPORTCATALOG.Rep_PK),"0")
    ENDIF
    INSERT INTO REPORTCATALOG ;
         (Rep_PK, Rep_FRX, Rep_Name, Rep_Type, Rep_Class, Rep_Description) ;
         VALUES (xx, ;
                 "XXUSYSG1.FRX", ;
                 "Groups and their Members", ;
                 "R", ;
                 "frmReportUSYSG1,xxfwsec.vcx", ;
                 "Simple listing of Groups and the Users who are Members")
  ENDIF
ENDIF

************************************************
*  populate security tables with known values
************************************************

IF !USED("SecuredItems")
*!*	  USE (JUSTSTEM(SET("DATABASE")) + "USYSSI") IN 0 ALIAS SecuredItems   &&& 2/19/01 
  USE (tcAppPrefix+"USYSSI") AGAIN IN 0 ALIAS SecuredItems
ENDIF

*
*  add XXUSYS1.FRX report
*
xx = RECCOUNT("SecuredItems") + 1
IF !tlUseIntegerPKFields
  xx = PADL(xx,LENC(SecuredItems.Sit_PK),"0")
ENDIF
INSERT INTO SecuredItems ;
            (SIt_PK, SIt_Type, SIt_TypeCD, SIt_ID, SIt_Desc) ;
     VALUES (xx,"Report","5","frmReportUSYS1,XXFWSEC.VCX","Users Listing report")
m.Acc_SItFK = xx
m.Acc_PK = IIF(tlUseIntegerPKFields,RECCOUNT("SecurityAccess")+1,PADL(RECCOUNT("SecurityAccess")+1,5,"0"))
m.Acc_UGrFK = IIF(tlUseIntegerPKFields,1,"00001")  &&& Administrators
m.Acc_Access = 1
INSERT INTO SecurityAccess FROM MEMVAR
m.Acc_PK = IIF(tlUseIntegerPKFields,RECCOUNT("SecurityAccess")+1,PADL(RECCOUNT("SecurityAccess")+1,5,"0"))
m.Acc_UGrFK = IIF(tlUseIntegerPKFields,2,"00002")  &&& Supervisors
m.Acc_Access = 1
INSERT INTO SecurityAccess FROM MEMVAR
m.Acc_PK = IIF(tlUseIntegerPKFields,RECCOUNT("SecurityAccess")+1,PADL(RECCOUNT("SecurityAccess")+1,5,"0"))
m.Acc_UGrFK = IIF(tlUseIntegerPKFields,3,"00003")  &&& Users
m.Acc_Access = 9
INSERT INTO SecurityAccess FROM MEMVAR
m.Acc_PK = IIF(tlUseIntegerPKFields,RECCOUNT("SecurityAccess")+1,PADL(RECCOUNT("SecurityAccess")+1,5,"0"))
m.Acc_UGrFK = IIF(tlUseIntegerPKFields,4,"00004")  &&& Guests
m.Acc_Access = 9         &&& none
INSERT INTO SecurityAccess FROM MEMVAR

*
*  add XXUSYS2.FRX report
*
xx = RECCOUNT("SecuredItems") + 1
IF !tlUseIntegerPKFields
  xx = PADL(xx,LENC(SecuredItems.Sit_PK),"0")
ENDIF
INSERT INTO SecuredItems ;
            (SIt_PK, SIt_Type, SIt_TypeCD, SIt_ID, SIt_Desc) ;
     VALUES (xx,"Report","5","frmReportUSYS2,XXFWSEC.VCX","Users/Group Membership report")
m.Acc_SItFK = xx
m.Acc_PK = IIF(tlUseIntegerPKFields,RECCOUNT("SecurityAccess")+1,PADL(RECCOUNT("SecurityAccess")+1,5,"0"))
m.Acc_UGrFK = IIF(tlUseIntegerPKFields,1,"00001")  &&& Administrators
m.Acc_Access = 1
INSERT INTO SecurityAccess FROM MEMVAR
m.Acc_PK = IIF(tlUseIntegerPKFields,RECCOUNT("SecurityAccess")+1,PADL(RECCOUNT("SecurityAccess")+1,5,"0"))
m.Acc_UGrFK = IIF(tlUseIntegerPKFields,2,"00002")  &&& Supervisors
m.Acc_Access = 1
INSERT INTO SecurityAccess FROM MEMVAR
m.Acc_PK = IIF(tlUseIntegerPKFields,RECCOUNT("SecurityAccess")+1,PADL(RECCOUNT("SecurityAccess")+1,5,"0"))
m.Acc_UGrFK = IIF(tlUseIntegerPKFields,3,"00003")  &&& Users
m.Acc_Access = 9
INSERT INTO SecurityAccess FROM MEMVAR
m.Acc_PK = IIF(tlUseIntegerPKFields,RECCOUNT("SecurityAccess")+1,PADL(RECCOUNT("SecurityAccess")+1,5,"0"))
m.Acc_UGrFK = IIF(tlUseIntegerPKFields,4,"00004")  &&& Guests
m.Acc_Access = 9         &&& none
INSERT INTO SecurityAccess FROM MEMVAR

*
*  add XXUSYSG1.FRX report
*
xx = RECCOUNT("SecuredItems") + 1
IF !tlUseIntegerPKFields
  xx = PADL(xx,LENC(SecuredItems.Sit_PK),"0")
ENDIF
INSERT INTO SecuredItems ;
            (SIt_PK, SIt_Type, SIt_TypeCD, SIt_ID, SIt_Desc) ;
     VALUES (xx,"Report","5","frmReportUSYSG1,XXFWSEC.VCX","Groups and their Members report")
m.Acc_SItFK = xx
m.Acc_PK = IIF(tlUseIntegerPKFields,RECCOUNT("SecurityAccess")+1,PADL(RECCOUNT("SecurityAccess")+1,5,"0"))
m.Acc_UGrFK = IIF(tlUseIntegerPKFields,1,"00001")  &&& Administrators
m.Acc_Access = 1
INSERT INTO SecurityAccess FROM MEMVAR
m.Acc_PK = IIF(tlUseIntegerPKFields,RECCOUNT("SecurityAccess")+1,PADL(RECCOUNT("SecurityAccess")+1,5,"0"))
m.Acc_UGrFK = IIF(tlUseIntegerPKFields,2,"00002")  &&& Supervisors
m.Acc_Access = 1
INSERT INTO SecurityAccess FROM MEMVAR
m.Acc_PK = IIF(tlUseIntegerPKFields,RECCOUNT("SecurityAccess")+1,PADL(RECCOUNT("SecurityAccess")+1,5,"0"))
m.Acc_UGrFK = IIF(tlUseIntegerPKFields,3,"00003")  &&& Users
m.Acc_Access = 9
INSERT INTO SecurityAccess FROM MEMVAR
m.Acc_PK = IIF(tlUseIntegerPKFields,RECCOUNT("SecurityAccess")+1,PADL(RECCOUNT("SecurityAccess")+1,5,"0"))
m.Acc_UGrFK = IIF(tlUseIntegerPKFields,4,"00004")  &&& Guests
m.Acc_Access = 9         &&& none
INSERT INTO SecurityAccess FROM MEMVAR

*
*  add "MP_ADMIN" default VMP Admin menu pad
*
xx = RECCOUNT("SecuredItems") + 1
IF !tlUseIntegerPKFields
  xx = PADL(xx,LENC(SecuredItems.Sit_PK),"0")
ENDIF
INSERT INTO SecuredItems ;
            (SIt_PK, SIt_Type, SIt_TypeCD, SIt_ID, SIt_Desc) ;
     VALUES (xx,"Menu pad","1","MP_ADMIN","Admin menu pad (if menu created from XXFWMAIN.MNX)")
m.Acc_SItFK = xx
m.Acc_PK = IIF(tlUseIntegerPKFields,RECCOUNT("SecurityAccess")+1,PADL(RECCOUNT("SecurityAccess")+1,5,"0"))
m.Acc_UGrFK = IIF(tlUseIntegerPKFields,1,"00001")  &&& Administrators
m.Acc_Access = 1
INSERT INTO SecurityAccess FROM MEMVAR
m.Acc_PK = IIF(tlUseIntegerPKFields,RECCOUNT("SecurityAccess")+1,PADL(RECCOUNT("SecurityAccess")+1,5,"0"))
m.Acc_UGrFK = IIF(tlUseIntegerPKFields,2,"00002")  &&& Supervisors
m.Acc_Access = 1
INSERT INTO SecurityAccess FROM MEMVAR
m.Acc_PK = IIF(tlUseIntegerPKFields,RECCOUNT("SecurityAccess")+1,PADL(RECCOUNT("SecurityAccess")+1,5,"0"))
m.Acc_UGrFK = IIF(tlUseIntegerPKFields,3,"00003")  &&& Users
m.Acc_Access = 9
INSERT INTO SecurityAccess FROM MEMVAR
m.Acc_PK = IIF(tlUseIntegerPKFields,RECCOUNT("SecurityAccess")+1,PADL(RECCOUNT("SecurityAccess")+1,5,"0"))
m.Acc_UGrFK = IIF(tlUseIntegerPKFields,4,"00004")  &&& Guests
m.Acc_Access = 9         &&& none
INSERT INTO SecurityAccess FROM MEMVAR

IF INDBC("GENERATEPK","TABLE") 
  IF !USED("GeneratePK")
    USE GeneratePK IN 0
  ENDIF
  SELECT GeneratePK
  LOCATE FOR UPPER(GPK_PK) = "SECUREDITEMS"
  IF !FOUND()
    APPEND BLANK
    REPLACE GPK_PK WITH "SECUREDITEMS"
  ENDIF
  REPLACE GPK_CurrentNumber WITH RECCOUNT("SecuredItems")  
  LOCATE FOR UPPER(GPK_PK) = "SECURITYACCESS"
  IF !FOUND()
    APPEND BLANK
    REPLACE GPK_PK WITH "SECURITYACCESS"
  ENDIF
  REPLACE GPK_CurrentNumber WITH RECCOUNT("SecurityAccess")
ENDIF
RETURN


********************************************************
PROCEDURE CreateRITable
********************************************************
LPARAMETERS tcAppPrefix, tlUseIntegerPKFields
LOCAL lcDBCDir
lcDBCDir = GetDBCDir()
IF FILE(lcDBCDir + tcAppPrefix + "RI.DBF")
  IF INDBC("VMPRI","TABLE")
    REMOVE TABLE VMPRI DELETE 
  ENDIF    
ENDIF

IF VARTYPE(plSingleTable) = "U"
  *
  *  this procedure was called directly, not from XXFWDATA itself
  *  (i.e., from the VMP application setup wizard)
  *
  PRIVATE plSingleTable
  plSingleTable = .t.
ENDIF

*
*  if none of the optional tables exist, there are no
*  relationships to maintain
*
IF NOT plSingleTable ;   &&& if plSingleTable, XXFWDATA.PRG has been called just to add the VMP RI table DO XXFWDATA WITH "XX","RI",.t.
    AND NOT INDBC("USERGROUPS","TABLE") ;     
    AND NOT INDBC("UserPrefs","TABLE") ;
    AND NOT INDBC("UserAccExceptions","TABLE") ;
    AND NOT INDBC("LoginHistory","TABLE") ;
    AND NOT INDBC("SecurityAccess","TABLE")
  RETURN 
ENDIF

WAIT WINDOW "VMPRI..." NOWAIT 
CREATE TABLE (lcDBCDir + tcAppPrefix + "RI") NAME VMPRI ;
     (XRI_Parent_TableName C(128) NOT NULL, ;
      XRI_Child_TableName C(128) NOT NULL, ;
      XRI_Child_Index_Tag C(10) NOT NULL, ;
      XRI_Insert_Type C(1) NOT NULL, ;
      XRI_Update_Type C(1) NOT NULL, ;
      XRI_Delete_Type C(1) NOT NULL)

* AB - 10/2004 - Start
DO IndexVMPRITable WITH tcAppPrefix

*!*	*
*!*	*  create a candidate key index tag of the maximum
*!*	*  length VFP will allow (if your table names are 
*!*	*  not unique for the first 115 characters, we don't
*!*	*  want to hear about it!  <g>):
*!*	*
*!*	LOCAL lcSetCollate
*!*	lcSetCollate = SET("COLLATE")
*!*	SET COLLATE TO "MACHINE"  &&& other collate sequences generate an Invalid Key Length error on expressions > 120 characters
*!*	INDEX ON UPPER(LEFTC(xri_parent_tablename,115)) + ;
*!*	         UPPER(LEFTC(xri_child_tablename,115)) + ;
*!*	         UPPER(xri_child_index_tag) ;
*!*	         TAG XRI_ID CANDIDATE FOR NOT DELETED()
*!*	SET COLLATE TO (lcSetCollate)
* AB - 10/2004 - End

DBSETPROP("VMPRI",                 "Table", "Comment", "Referential Integrity")
DBSETPROP("VMPRI.XRI_INSERT_TYPE", "Field", "Comment", ;
     "Valid values are: 'I'nsert-Allow insert, 'R'estrict-No insert unless parent exists" )
DBSETPROP("VMPRI.XRI_UPDATE_TYPE", "Field", "Comment", ;
     "Valid values are: 'I'gnore-Allow update, 'C'ascade-Post changes to the parent PK to child FKs, 'R'estrict - Prevents update if child records exist related to parent record." )
DBSETPROP("VMPRI.XRI_DELETE_TYPE", "Field", "Comment", ;
     "Valid values are: 'I'gnore - Allows delete, 'C'ascade-Deletes child records with a FK matching the parent PK, 'R'estrict-Prevents deletion if child records exist related to parent record." )
RETURN



************************************************************
PROCEDURE CreateIUDLogTable
************************************************************
*
*  IUDLog "audit trail" table
*
*  this PROCEDURE can be called by itself, typically to
*  add a dedicated IUDLog_<TableName> table to the
*  current database
*
LPARAMETERS tcAppPrefix, ;
            tlUseIntegerPKFields, ;    &&& pretty much ignored here
            tcTableName                &&& IUDLog_<TableName> or <TableName>

*!*	DO CASE
*!*	  CASE X2PSTACK("NTDVDATA.") AND UPPER(m.tcAppPrefix) == "VM"
*!*	    *  NT example app
*!*	  CASE X2PSTACK("VMDVDATA.") AND UPPER(m.tcAppPrefix) == "VM"
*!*	    *  VM example app
*!*	  CASE UPPER("VMP_IUDLog_Tests.FXP") $ SYS(16,8)
*!*	    *  Drew is running a FoxUnit test
*!*	  CASE "TADATA.FXP" $ SYS(16,0) 
*!*	    *  Drew is testing this feature in the TA app
*!*	  OTHERWISE 
*!*	    *
*!*	    *  coming in a future refresh of the March 18, 2005 
*!*	    *  build of VMP 2005 -- likely before the end of this month
*!*	    *  DEVNOTE IUD
*!*	    *
*!*	    RETURN .t.
*!*	ENDCASE

LOCAL lcTableName
IF VARTYPE(m.tcTableName) = "C" AND NOT EMPTY(m.tcTableName)
  *
  *  tcTableName passed explicitly, expected to be
  *  the name of an existing table in this database
  *
  IF LEFTC(UPPER(tcTableName),7) = "IUDLOG_"
    *  passed as IUDLog_<TableName>
    lcTableName = m.tcTableName
   ELSE
    *  passed as <TableName>
    lcTableName = "IUDLog_" + m.tcTableName
  ENDIF
  IF NOT INDBC(SUBSTRC(m.lcTableName,8),"TABLE")
    ASSERT .f. MESSAGE PROGRAM() + ;
          " is unable to locate a table named '" + m.lcTableName + "' in the current database"
    RETURN .f.
  ENDIF
 ELSE
  lcTableName = "IUDLog"
ENDIF

LOCAL lcDBCDir
lcDBCDir = GetDBCDir()
IF FILE(m.lcDBCDir + m.tcAppPrefix + m.lcTableName)
  IF INDBC(m.lcTableName,"TABLE")
    REMOVE TABLE (m.lcTableName) DELETE 
  ENDIF    
ENDIF

IF VARTYPE(m.plSingleTable) = "U"
  *
  *  this procedure was called directly, not from XXFWDATA itself
  *  (i.e., from the VMP application setup wizard)
  *
  PRIVATE plSingleTable
  plSingleTable = .t.
ENDIF

WAIT WINDOW m.lcTableName + "..." NOWAIT 
CREATE TABLE (m.lcDBCDir + m.tcAppPrefix + m.lcTableName) NAME (m.lcTableName) ;
    (IUD_Action C(1) CHECK INLIST(IUD_Action,"I","U","D") ERROR "IUD_Action must be either 'I', 'U', or 'D'", ;
     IUD_DateTime T, ;
     IUD_UsrFK C(5), ;
     IUD_Usr C(40), ;
     IUD_TransID C(16) NULL NOCPTRANS, ;
     IUD_TXNLevel N(1,0), ;
     IUD_TableName C(30), ;
     IUD_PKFieldName C(6), ;
     IUD_PKFieldType C(1), ;
     IUD_PKFieldValue C(5), ;
     IUD_FValues M NOCPTRANS, ;
     IUD_MValues M NOCPTRANS, ;
     IUD_WValues M NOCPTRANS, ;
     IUD_QValues M NOCPTRANS, ;
     IUD_UFields M NOCPTRANS, ;
     IUD_TableDBC C(160), ;
     IUD_TableDBF C(160), ;
     IUD_Stack M NOCPTRANS, ;
     IUD_SYS0 C(40), ;
     IUD_DS I, ;
     IUD_TL I, ;
     IUD_Exception M)
     
IF INDBC("USERS","TABLE")
  LOCAL llWasUsed
  IF USED("Users")
    llWasUsed = .t.
    SELECT Users
   ELSE
    USE Users IN 0 
    SELECT Users
  ENDIF
  LOCAL lcString
  lcString = IIF(VARTYPE(Users.Usr_PK)="C","C("+TRANSFORM(LENC(Users.Usr_PK))+")","I")
  ALTER TABLE (m.lcTableName) ALTER COLUMN IUD_UsrFK &lcString
  lcString = IIF(VARTYPE(Users.Usr_PK)="C","C("+TRANSFORM(LENC(Users.Usr_PK))+")","C(10)")
  ALTER TABLE (m.lcTableName) ALTER COLUMN IUD_PKFieldValue &lcString
  IF NOT m.llWasUsed
    USE IN SELECT("Users")
  ENDIF
ENDIF

DBSETPROP(m.lcTableName+'.IUD_TransID',      'Field', 'Comment', "OPTIONAL.  GUID of the Transaction (if any) in which the IUD_Action occurs, so that you can see which records were logged as part of the same transaction.")
DBSETPROP(m.lcTableName+'.IUD_TXNLevel',     'Field', 'Comment', "OPTIONAL.  TXNLEVEL() at the time this log record was written.")
DBSETPROP(m.lcTableName+'.IUD_UsrFK',        'Field', 'Comment', "Users.Usr_PK of who made the Insert/Update/Delete.  If there is no oUser, or if oUser.GetPProp('icUsr_PK') RETURNs .NULL., this field is left blank/empty.")
DBSETPROP(m.lcTableName+'.IUD_Usr',          'Field', 'Comment', "When a Users.Usr_PK cannot be determined (IUD_UsrFK is blank), this field contains what we CAN figure out, in this order of precedence: WNetGetUser(), SYS(0), or blank.") 
DBSETPROP(m.lcTableName+'.IUD_DateTime',     'Field', 'Comment', "DateTime() this record was added to this table (DATETIME() of the Insert/Update/Delete action).")
DBSETPROP(m.lcTableName+'.IUD_Action',       'Field', 'Comment', "'I'nsert or 'U'pdate or 'D'elete, indicating which trigger called SP_VMP_IUDLog to add this record.")
DBSETPROP(m.lcTableName+'.IUD_TableName',    'Field', 'Comment', "The name of the contained table being Inserted/Updated/Deleted.  If IUD_TableName is a free table, the JUSTFNAME() of the .DBF file.")
DBSETPROP(m.lcTableName+'.IUD_TableDBC',     'Field', 'Comment', "OPTIONAL.  DISPLAYPATH(CURSORGETPROP(IUD_TableName),<Length of IUD_TableDBC>).  Blank/empty if IUD_TableName is a free table.")  
DBSETPROP(m.lcTableName+'.IUD_TableDBF',     'Field', 'Comment', "OPTIONAL.  DISPLAYPATH(DBF(IUD_TableName),<Length of IUD_TableDBF>))")
DBSETPROP(m.lcTableName+'.IUD_PKFieldName',  'Field', 'Comment', "The name of the Primary Key field of the record in IUD_TableName that is being Inserted/Updated/Deleted.")
DBSETPROP(m.lcTableName+'.IUD_PKFieldType',  'Field', 'Comment', "Data type of the Primary Key field IUD_PKFieldName.")
DBSETPROP(m.lcTableName+'.IUD_PKFieldValue', 'Field', 'Comment', "The TRANSFORM() of the Primary Key field value of the record in IUD_TableName that is being Inserted/Updated/Deleted.")
DBSETPROP(m.lcTableName+'.IUD_FValues',      'Field', 'Comment', "Stores the values of all Regular/non-Memo fields as of the Insert/Update/Delete.")
DBSETPROP(m.lcTableName+'.IUD_MValues',      'Field', 'Comment', "OPTIONAL.  Stores the values of all Memo fields as of the Insert/Update/Delete.  This field is optional - remove it to not log any Memo fields for any tables.")
DBSETPROP(m.lcTableName+'.IUD_WValues',      'Field', 'Comment', "OPTIONAL.  Stores the values of all Blob fields (VFP 9.0+) as of the Insert/Update/Delete.  This field is optional -remove it to not log any Blob fields for any tables.")
DBSETPROP(m.lcTableName+'.IUD_QValues',      'Field', 'Comment', "OPTIONAL.  Stores the values of all VarBinary fields (VFP 9.0+) as of the Insert/Update/Delete.  This field is optional - remove it to not log any VarBinary fields for any tables.") 
DBSETPROP(m.lcTableName+'.IUD_UFields',      'Field', 'Comment', "OPTIONAL.  When IUD_Action='U' and the logged table is buffered, this field stores a list of the names of the ('regular') fields that have been updated.  Mostly serves as a quick way for the developer to see which fields were changed.")
DBSETPROP(m.lcTableName+'.IUD_Stack',        'Field', 'Comment', "OPTIONAL.  Stores the VFP program execution (SYS(16) loop) stack, indicating how the Insert/Update/Delete was accomplished.")
DBSETPROP(m.lcTableName+'.IUD_SYS0',         'Field', 'Comment', "OPTIONAL.  Stores the value of the VFP SYS(0) function at the time of the Insert/Update/Delete.") 
DBSETPROP(m.lcTableName+'.IUD_DS',           'Field', 'Comment', "OPTIONAL.  Stores the Integer value of the Data Session in which the Insert/Update/Delete occurred.")
DBSETPROP(m.lcTableName+'.IUD_TL',           'Field', 'Comment', "OPTIONAL.  Stores the Integer value of _TRIGGERLEVEL at which the Insert/Update/Delete occurred for the logged table.")
DBSETPROP(m.lcTableName+".IUD_Exception",    "Field", "Comment", "OPTIONAL.  If an exception occurs in SP_VMP_IUDLOG that is trapped by a TRY/CATCH block, exception information is stored here.")

IF VERSION(5)>= 900
  ALTER TABLE (m.lcTableName) ALTER COLUMN IUD_WValues W
*!*	  ALTER TABLE (m.lcTableName) ALTER COLUMN IUD_QValues W
 ELSE
  ALTER TABLE (m.lcTableName) DROP COLUMN IUD_WValues
  ALTER TABLE (m.lcTableName) DROP COLUMN IUD_QValues
ENDIF
ERASE (FORCEEXT(DBF(m.lcTableName),"TBK"))
ERASE (FORCEEXT(DBF(m.lcTableName),"BAK"))

ALTER TABLE (m.lcTableName) ;
     SET CHECK (NOT EMPTY(IUD_Usr) OR NOT EMPTY(IUD_UsrFK)) AND NOT EMPTY(IUD_Action) AND NOT EMPTY(IUD_TableName) AND NOT EMPTY(IUD_PKFieldName) AND NOT EMPTY(IUD_PKFieldValue) ;
     ERROR XXFWRuleErrorText("RR_IUDLog","User, Action, TableName, PK Field Name, and PK Field Value must all be entered.")

DO IndexIUDLogTable WITH tcAppPrefix, m.lcTableName

*
*  this for when this PROCEDURE is called directly
*  (otherwise calling UpdateIUDLog() updates all
*  the IUDLog table triggers)
*
CREATE TRIGGER ON (m.lcTableName) FOR INSERT AS SP_VMP_IUDLog_RW()  &&& cannot manually INSERT records, outside the SP_VMP_IUDLog Stored procedure
CREATE TRIGGER ON (m.lcTableName) FOR UPDATE AS SP_VMP_IUDLog_RW()  &&& once created, cannot be updated
CREATE TRIGGER ON (m.lcTableName) FOR DELETE AS SP_VMP_IUDLog_RW()  &&& once created, cannot be deleted

RETURN



********************************************************
PROCEDURE SP_XXFWRuleErrorText
********************************************************
*
*  if the XXFWRuleErrorText stored procedure exists in
*  the current SET DATABASE TO database, remove it and
*  then add it back in from scratch
*
LOCAL ARRAY laLines[1]
LOCAL lnStartLine, lnEndLine
*
*  first create an array of the lines of the stored procedures
*  minus the XXFWRuleErrorText stored procedure
*
CreateSP1(@laLines,"XXFWRuleErrorText",@lnStartLine,@lnEndLine)

*
*  create the XXFWRuleErrorText stored procedure
*
SET TEXTMERGE ON NOSHOW
SET TEXTMERGE TO thissp.txt
TEXT
PROCEDURE XXFWRuleErrorText
*
*  RETURN the error text whenever a field/row rule returns .f.
*
*  LPARAMETERS
*       tcRule (R) Identifier for the row/field rule,
*                    intended to be a MSGSVC.cKey value
*   tcRuleText (O) Rule error text in case MSGSVC can't
*                    be accessed.  
*                  This parameter is usually
*                    identical to the contents of MSGSVC.cOriginal,
*                    so for every rule whose Error text is
*                    generated by calling this stored procedure,
*                    you should have a corresponding record
*                    in MSGSVC.DBF, as is the case with the
*                    rules here in XXFWDATA.PRG, and also
*                    demonstrated in VMDVRULE.PRG/MSGSVC.DBF
*                    in the VM/NT example apps that ships with
*                    VMP.
*                  Defaults to "Invalid data"
*                  
* tcReturntcRule (O) Pass as .T. to RETURN the tcRule parameter value
*                    from here (see XXFWNTDS.VCX/ctrDataSource.LoadDBCRules)
*
LPARAMETERS tcRule, tcRuleText, tlReturntcRule
IF VARTYPE(m.tlReturntcRule) = "L" AND m.tlReturntcRule
  RETURN m.tcRule
ENDIF
tcRule = UPPER(ALLTRIM(m.tcRule))
LOCAL lcRuleText
*
*  if _Screen.oMsgSvc is available
*
IF TYPE("_Screen.oMsgSvc.Baseclass") = "C" AND (FILE("X3MSGSVC.PRG") OR FILE("X3MSGSVC.FXP"))
  lcRuleText = X3MSGSVC(m.tcRule)
  RETURN m.lcRuleText
ENDIF
IF TYPE("_Screen.oMsgSvc.Baseclass") = "C" 
  lcRuleText = _Screen.oMsgSvc.MsgSvc(m.tcRule)
  RETURN m.lcRuleText
ENDIF
*
*  if X3MSGSVC.PRG and MSGSVC.DBF are available
*
IF USED("MSGSVC") AND (FILE("X3MSGSVC.PRG") OR FILE("X3MSGSVC.FXP"))
  lcRuleText = X3MSGSVC(m.tcRule)
  RETURN m.lcRuleText
ENDIF
*
*  user is doing a raw BROWSE at the Command Window
*  and X3MSGSVC is not in the path or _Screen.oMsgSvc
*  hasn't been installed
*
*  First, see if maybe there is a MSGSVC.DBF available,
*  assuming that, if there is, it has the necessary
*  message text
*
LOCAL lnSelect, llError, llCloseMsgSvc
lcRuleText = SPACE(0)
lnSelect = SELECT(0)
IF USED("MSGSVC")
  * nothing to do yet
 ELSE
  IF FILE("MSGSVC.DBF")
    llCloseMsgSvc = .t.
    llError = .f.
    TRY
    USE MSGSVC IN 0 SHARED NOUPDATE
    CATCH
    llError = .t.
    ENDTRY
  ENDIF
ENDIF  
IF USED("MSGSVC")
  SELECT MSGSVC
  LOCATE FOR UPPER(ALLTRIM(MSGSVC.cKey)) == UPPER(ALLTRIM(m.tcRule))
  IF FOUND()
    lcRuleText = ALLTRIM(MSGSVC.cOriginal)
  ENDIF
  IF m.llCloseMsgSvc
    USE IN SELECT('MSGSVC')
  ENDIF
ENDIF
SELECT (m.lnSelect)
IF NOT EMPTY(m.lcRuleText)
  RETURN m.lcRuleText
ENDIF
*
*  we haven't been able to use MSGSVC-generated
*  error message text -- use the passed tcRuleText
*
IF VARTYPE(m.tcRuleText) = "C" AND NOT EMPTY(m.tcRuleText)
  lcRuleText = ALLTRIM(m.tcRuleText)
 ELSE
  lcRuleText = "Invalid data"
ENDIF
RETURN m.lcRuleText      
ENDTEXT 
SET TEXTMERGE OFF SHOW
SET TEXTMERGE TO

*
*  now put it all together
*
CreateSP2(@laLines,lnStartLine,lnEndLine)
RETURN




PROCEDURE CreateSP1
*
*  create an array of lines from the current stored
*  procedures, minus the passed stored procedure 
*
LPARAMETERS taLines, tcSPName, tnStartLine, tnEndLine
LOCAL laLines[1], xx, lnStartLine, lnEndLine
lnStartLine = 0
lnEndLine = 0
COPY PROCEDURES TO oldsp.txt
*
*  first see if it exists, and, if so, note the starting
*  and ending lines
*
IF ALINES(laLines,ALLTRIM(FILETOSTR("oldsp.txt"))) > 0
  IF ALEN(laLines,1) > 1
    DO WHILE EMPTY(laLines[1])
      *  get rid of any leading blank lines
      ADEL(laLines,1)
      DIMENSION laLines[ALEN(laLines,1)-1]
    ENDDO
  ENDIF
  FOR xx = 1 TO ALEN(laLines,1)
    IF UPPER(ALLTRIM(laLines[xx])) == UPPER("PROCEDURE " + tcSPName)
      lnStartLine = xx
    ENDIF
    IF lnStartLine > 0 ;
         AND !UPPER(ALLTRIM(laLines[xx])) == UPPER("PROCEDURE " + tcSPName) ;
         AND (UPPER(ALLTRIM(laLines[xx])) = UPPER("PROCEDURE ") ;
              OR UPPER("Begin VMP RI Comment") $ UPPER(laLines[xx])) 
      lnEndLine = xx - 1
      EXIT
    ENDIF
  ENDFOR
  IF lnStartLine = 0
    *
    *  tcSPName is not currently in the stored procedures
    *
    lnStartLine = 1
   ELSE
    *
    *  remove the lines from laLines[] that contain the
    *  tcSPName stored procedure
    *
    FOR xx = lnEndLine TO lnStartLine STEP -1
      ADEL(laLines,xx)
      DIMENSION laLines[ALEN(laLines,1)-1]
    ENDFOR
  ENDIF  &&& lnStartLine = 0
 ELSE
  *
  *  existing stored procedures are empty
  *
  lnStartLine = 1
ENDIF
ERASE oldsp.txt
ACOPY(laLines,taLines)
tnStartLine = lnStartLine
tnEndLine = lnEndLine
RETURN




PROCEDURE CreateSP2
LPARAMETERS taLines, tnStartLine, tnEndLine
LOCAL ARRAY laLines[1]
ACOPY(taLines,laLines)
ERASE newsp.txt   &&& just in case
*
*  build the new stored procedures starting with the
*  code that preceded the old stored procedure
*
LOCAL xx, lnCount 
lnCount = 0
IF tnEndLine > 0
  FOR xx = 1 TO tnStartLine-1
    STRTOFILE(laLines[xx]+CHR(13)+CHR(10),"newsp.txt",.t.)
    lnCount = lnCount + 1
  ENDFOR
 ELSE
  FOR xx = 1 TO ALEN(laLines,1)
    STRTOFILE(laLines[xx]+CHR(13)+CHR(10),"newsp.txt",.t.)
    lnCount = lnCount + 1
  ENDFOR
ENDIF

*
*  now insert this new stored procedure
*
LOCAL laNewLines[1]
ALINES(laNewLines,FILETOSTR("thissp.txt"))
*
*  put 4 blank lines between procedures after the first one
*
IF lnCount > 0
  STRTOFILE(CHR(13)+CHR(10)+CHR(13)+CHR(10)+CHR(13)+CHR(10)+CHR(13)+CHR(10),"newsp.txt",.t.)
ENDIF
FOR xx = 1 TO ALEN(laNewLines,1)
  STRTOFILE(laNewLines[xx]+CHR(13)+CHR(10),"newsp.txt",.t.)
ENDFOR

*
*  finally add the stored procedures that were after
*  the old stored procedure
*
IF tnEndLine > 0
  FOR xx = tnStartLine TO ALEN(laLines,1)
    STRTOFILE(laLines[xx]+CHR(13)+CHR(10),"newsp.txt",.t.)
  ENDFOR
ENDIF

*
*  and post the new set of stored procedures to the .DBC
*
APPEND PROCEDURES FROM newsp.txt OVERWRITE
CLOSE TABLES
COMPILE DATABASE (DBC())
ERASE oldsp.txt
ERASE thissp.txt
ERASE newsp.txt
RETURN

PROCEDURE SP_Row_Rule_UsrGrpXRef
*
*  if the Row_Rule_UsrGrpXRef stored procedure exists in
*  the current SET DATABASE TO database, remove it
*    then
*  if it is needed in the current SET DATABASE TO database,
*  add it back in from scratch
*
LOCAL ARRAY laLines[1]
LOCAL lnStartLine, lnEndLine
*
*  first create an array of the lines of the stored procedures
*  minus the Row_Rule_UsrGrpXRef stored procedure
*
CreateSP1(@laLines,"Row_Rule_UsrGrpXRef",@lnStartLine,@lnEndLine)

*
*  see if this stored procedure is actually needed
*
IF !INDBC("UsrGrpXRef","TABLE")
  RETURN
ENDIF

*
*  create the Row_Rule_UsrGrpXRef stored procedure
*
SET TEXTMERGE ON NOSHOW
SET TEXTMERGE TO thissp.txt
TEXT
PROCEDURE Row_Rule_UsrGrpXRef
*
*  row rule for UsrGrpXRef
*
RETURN !EMPTY(UGX_PK) ;
   AND !EMPTY(UGX_UGrFK) ;
   AND !EMPTY(UGX_MemberFK) ;
   AND !EMPTY(UGX_MemberType) ;
   AND IIF(UGX_MemberType="U",X3SEEK(CURSORGETPROP("Database",ALIAS()),UGX_MemberFK,"Users","Usr_PK"),X3SEEK(CURSORGETPROP("Database",ALIAS()),UGX_MemberFK,"UserGroups","UGr_PK",.t.)) 
ENDTEXT
SET TEXTMERGE OFF SHOW
SET TEXTMERGE TO

*
*  now put it all together
*
CreateSP2(@laLines,lnStartLine,lnEndLine)
RETURN

PROCEDURE SP_Field_Rule_UGX_MemberFK
*
*  if the Field_Rule_UGX_MemberFK stored procedure exists 
*  in the current SET DATABASE TO database, remove it
*    then
*  if it is needed in the current SET DATABASE TO database,
*  add it back in from scratch
*
LOCAL ARRAY laLines[1]
LOCAL lnStartLine, lnEndLine

*
*  first create an array of the lines of the stored procedures
*  minus the Field_Rule_UGX_MemberFK stored procedure
*
CreateSP1(@laLines,"Field_Rule_UGX_MemberFK",@lnStartLine,@lnEndLine)

*
*  see if this stored procedure is actually needed
*
IF !INDBC("UsrGrpXRef","TABLE")
  RETURN
ENDIF

*
*  create the Field_Rule_UGX_MemberFK stored procedure
*
SET TEXTMERGE ON NOSHOW
SET TEXTMERGE TO thissp.txt
TEXT
PROCEDURE Field_Rule_UGX_MemberFK
*
*  field rule for UsrGrpXRef.UGX_MemberFK
*
LOCAL lcDBC, llRetVal
lcDBC = JUSTSTEM(CURSORGETPROP("Database",ALIAS()))
*
*  ANY of the following are acceptable -- I used
*  to have this as one large line of code separated
*  by ORs, but this is easier to debug
*
llRetVal = IIF(_TriggerLevel>0,.t.,X3SEEK(lcDBC,UGX_MemberFK,"Users","Usr_PK")) 
IF NOT llRetVal
  llRetVal = IIF(_TriggerLevel>0,.t.,X3SEEK(lcDBC,UGX_MemberFK,"Users","Usr_PK",.t.)) 
ENDIF
IF NOT llRetVal
  llRetVal = IIF(_TriggerLevel>0,.t.,X3SEEK(lcDBC,UGX_MemberFK,"UserGroups","UGr_PK")) 
ENDIF
IF NOT llRetVal 
  llRetVal = IIF(_TriggerLevel>0,.t.,X3SEEK(lcDBC,UGX_MemberFK,"UserGroups","UGr_PK",.t.)) 
ENDIF
RETURN llRetVal
ENDTEXT
SET TEXTMERGE OFF SHOW
SET TEXTMERGE TO

*
*  now put it all together
*
CreateSP2(@laLines,lnStartLine,lnEndLine)
RETURN



PROCEDURE UpdateRIStoredProcedures
*
*  update existing RI Stored Procedures, or add them
*  if they don't exist
*
*  call this program after creating and populating the
*  VMPRI table
*
*  first, delete them -- whether this .DBC needs them or
*  not, we'll start from scratch
*
IF VARTYPE(m.plAbort) = "L" AND m.plAbort
  RETURN
ENDIF
WAIT WINDOW "Updating Referential Integrity stored procedures..." NOWAIT
LOCAL laLines[1], xx, lnStartLine, lnEndLine
lnStartLine = 0
lnEndLine = 0
COPY PROCEDURES TO oldsp.txt
*
*  first see the VMP RI stored procedures exist, and, if so, 
*  note the starting and ending lines
*
IF ALINES(laLines,ALLTRIM(FILETOSTR("oldsp.txt"))) > 0
  IF ALEN(laLines,1) > 1
    DO WHILE EMPTY(laLines[1])
      *  get rid of any leading blank lines
      ADEL(laLines,1)
      DIMENSION laLines[ALEN(laLines,1)-1]
    ENDDO
  ENDIF
  FOR xx = 1 TO ALEN(laLines,1)
    IF UPPER("Begin VMP RI Comment") $ UPPER(laLines[xx]) 
      lnStartLine = xx
    ENDIF
    IF UPPER("End VMP RI Comment") $ UPPER(laLines[xx]) 
      lnEndLine = xx 
      EXIT
    ENDIF
  ENDFOR
  IF lnStartLine = 0
    *
    *  VMP RI are not currently in the stored procedures
    *
    lnStartLine = ALEN(laLines,1) + 1
   ELSE
    *
    *  remove the lines from laLines[] that contain the
    *  VMP RI stored procedures
    *
    FOR xx = lnEndLine TO lnStartLine STEP -1
      ADEL(laLines,xx)
      DIMENSION laLines[ALEN(laLines,1)-1]
    ENDFOR
  ENDIF  &&& lnStartLine = 0
 ELSE
  *
  *  existing stored procedures are empty
  *
  lnStartLine = 1
ENDIF
ERASE oldsp.txt

IF !INDBC("VMPRI","TABLE")
  *
  *  we're done -- no VMP RI in this .DBC
  *
  FOR xx = 1 TO ALEN(laLines,1)
    STRTOFILE(laLines[xx]+CHR(13)+CHR(10),"newsp.txt",.t.)
  ENDFOR
  APPEND PROCEDURES FROM newsp.txt OVERWRITE
  CLOSE TABLES
  COMPILE DATABASE (DBC())
  PACK DATABASE 
  ERASE newsp.txt
  RETURN
ENDIF

*
*  now generate the current set of VMP RI procedures
*
SET TEXTMERGE ON NOSHOW
SET TEXTMERGE TO thissp.txt
TEXT
*  !!!!!!!!!!!!!!!!!!!! BEGIN VMP RI Comment !!!!!!!!!!!!!!!!!!!!
*
*  The following is VMP Referential-Integrity code, used by
*  the RI triggers added to the VMP tables in this database.
*  DO NOT REMOVE THESE PROCEDURES!
*
*  You are welcome to use this RI implementation for your
*  tables, too -- DO XXDTRI to maintain the VMPRI table
*  that drives Referential Integrity.  Of course, you can
*  use the native VFP RI Builder (or any other scheme to
*  provide RI), but these procedures are necessary for the
*  RI we establish for the VMP tables in this database.
*
*  The following stored procedures have been inserted into the
*  Stored Procedures for this database by the XXFWDATA.PRG utility
*
*
PROCEDURE VMP_RI
*
*  Referential Integrity program for Visual MaxFrame professional 
*  (VMP) applications 
*
*  Copyright (c) 1996-2005 Visionpace   All Rights Reserved
*                17501 East 40 Hwy., Suite 218
*                Independence, MO 64055
*                816-350-7900 
*                http://www.visionpace.com
*                http://vmpdiscussion.visionpace.com
*  Author:  Ed Lennon
*           Drew Speedie
*           Based on ideas from the native VFP RI Builder code,
*           and precipitated by suggestions from Mike Potjer
*
*  PARAMETERS: 
*  tcTriggerType (R) trigger type
*                    Valid triggers types are: INSERT
*                                              UPDATE
*                                              DELETE
*  ASSUMPTIONS:
*  This program assumes that there is a table named VMPRI
*  in the database, with the following structure:
*
*      Field  Name                           Type    Len,Dec
*      ----------------------------------------------------
*          1  XRI_Parent_TableName            C      128,0  
*          2  XRI_Child_TableName             C      128,0  
*          3  XRI_Child_Index_Tag             C       10,0  
*          4  XRI_Insert_Type                 C        1,0  
*          5  XRI_Update_Type                 C        1,0  
*          6  XRI_Delete_Type                 C        1,0  
*
*  For example usage, please see the VM Example Application's VMPRI table.
*

LPARAMETERS tcTriggerType

LOCAL lcTrigger

IF NOT VARTYPE(m.tcTriggerType) = "C" OR EMPTY(m.tcTriggerType)
  RETURN .F.
ENDIF
lcTrigger = UPPER(ALLTRIM(LEFTC(m.tcTriggerType,1)))
IF NOT INLIST(m.lcTrigger,"I","U","D")
  RETURN .f.
ENDIF

*
*  DECLARE some LOCAL memvars used throughout this program.
*  Note that this program can be called recursively for 
*  cascade updates and deletes.
*
LOCAL llRetVal, ;
      lcDBC, ;
      lcTriggerSourceName, ;
      lcParentAlias, ;
      lcParentPKTag, ;
      lcParentPKExpr, ;
      lcParentPK, ;
      lcOldParentPK, ;
      lcChildAlias, ;
      lcChildFKField, ;
      lcChildFKToSEEKInParentAlias, ;
      lnSelect, ;
      lcSetDatabase, ;
      lcSetTalk, ;
      lcSetExact, ;
      lcSetDeleted, ;
      lcSetCompatible, ;
      llTransaction, ;
      lnRecno, ;
      lnTag, ;
      lcForExpr, ;
      loException, ;   
      loOuterException  

llRetVal                     = .T.      && Return value
lcDBC                        = SPACE(0) && Database to be SET for this RI
lcTriggerSourceName          = SPACE(0) && The source name of the trigger table in the DBC
lcParentAlias                = SPACE(0) && The alias of the parent table
lcParentPKExpr               = SPACE(0) && The parent table PK expression
lcParentPK                   = SPACE(0) && The parent table PK value
lcOldParentPK                = SPACE(0) && The OLDVAL() value of the parent table PK value
lcChildAlias                 = SPACE(0) && The alias of the child table
lcChildFKField               = SPACE(0) && The child table FK field to the parent table
lcChildFKToSEEKInParentAlias = SPACE(0) && The child FK value to SEEK in the parent table
llTransaction                = .f.      && Indicates a BEGIN TRANSACTION has been issued
lnRecno                      = 0        && The VMPRI_For_Triggers recno (push and pop)

lnSelect      = SELECT(0)
lcSetDatabase = SET("DATABASE")
lcSetTalk     = SET("TALK")
lcSetExact    = SET("EXACT")
lcSetDeleted  = SET("DELETED")

SET TALK OFF
SET EXACT OFF
SET DELETED ON

*
* Store the database for the current ALIAS()
*
lcDBC = JUSTSTEM(CURSORGETPROP("DATABASE"))

SET DATABASE TO (m.lcDBC)

IF _TRIGGERLEVEL = 1
  *
  *  just in case
  *
  RELEASE gaVMPRIErrors
  RELEASE gnTriggerLevel
ENDIF

IF VARTYPE(m.gnTriggerLevel) = "U"
  *
  *  set a flag indicating the _TRIGGERLEVEL at
  *  which this procedure was first called (this
  *  procedure gets called "recursively" when a
  *  trigger causes another trigger to fire,
  *  like during cascade updates and cascade deletes)
  *
  PUBLIC gnTriggerLevel
  gnTriggerLevel = _TRIGGERLEVEL
  *
  *  array of error information, should something
  *  go wrong (see VMP_RI_Error()...)
  *
  PUBLIC gaVMPRIErrors[1,12]
ENDIF

IF USED("VMPRI_For_Triggers")
  IF UPPER(ALLTRIM(CURSORGETPROP("DataBase","VMPRI_For_Triggers"))) ;
       == UPPER(ALLTRIM(DBC()))
    *
    *  the VMPRI_For_Triggers that is open at the moment
    *  is contained in the same database as the table
    *  whose trigger is being fired at the moment (there
    *  is an explicit SET DATABASE command above)
    *
   ELSE
    *
    *  the VMPRI_For_Triggers that is open at the moment
    *  is NOT contained in the same database as the table
    *  whose trigger is being fired at the moment, so 
    *  close it and let the subsequent code open the 
    *  VMPRI_For_Triggers table, which will be from the
    *  current SET DATABASE TO database
    *
    USE IN SELECT('VMPRI_For_Triggers')
  ENDIF
ENDIF 

IF NOT USED("VMPRI_For_Triggers")
  loException = .f.
  TRY
  USE VMPRI AGAIN IN 0 ALIAS VMPRI_For_Triggers
  CATCH TO loException
  llRetVal = .f.
  ENDTRY
  IF VARTYPE(m.loException) = "O"
    VMP_RI_Error(m.loException)
  ENDIF
  *
  *  note that this procedure does not close the
  *  VMPRI_For_Triggers cursor for performance 
  *  reasons -- once this RI code has been called
  *  once in a data session, the VMPRI_For_Triggers
  *  table remains open for the life of the form,
  *  making subsequent calls to this procedure faster
  *
ENDIF  
IF NOT m.llRetVal
  RETURN .f.  
ENDIF

TRY

*
* The following CASE statement contains the logic as far as
* what to do for each type of trigger
*
DO CASE
  **************************************************** 
  CASE m.lcTrigger = "I"  && "INSERT"
  **************************************************** 
    *
    * INSERTs are from the CHILD's perspective:
    *
    *   RESTRICT - Prohibits the INSERT INTO the CHILD table
    *              if a matching KEY value does not exist in 
    *              the PARENT table
    *
    *     IGNORE - Allows the CHILD insert, PARENT record may
    *              or may not exist
    *
	
    lcTriggerSourceName = CURSORGETPROP("SOURCENAME")  && Could be an Alias
    lcParentAlias       = SPACE(0)
    lcParentPKExpr      = SPACE(0)
    lcParentPK          = SPACE(0)
    lcChildAlias        = ALIAS()

    SELECT VMPRI_For_Triggers
    lcTriggerSourceName = PADR(m.lcTriggerSourceName,LENC(XRI_Child_TableName))
    lnRecno = IIF(EOF(),0,RECNO())
    SCAN FOR UPPER(XRI_Child_TableName) = m.lcTriggerSourceName
  	  IF NOT XRI_Insert_Type = "R"
        LOOP
      ENDIF
      *
      * OK, should this rule be enforced for this INSERT attempt?
      *
      * Why do we care? Well consider the following example.
      *
      * Let's say we have 3 tables:
      *
      *   ParentA
      *   ParentB
      *   Child
      *
      * The Child table can be related to both ParentA and ParentB.
      * There is a field in Child that tells us which Parent table the entry
      * in Child is related to (chi_parent_table).
      *
      * If chi_parent_table = "A" && the Child record would be related to ParentA
      * If chi_parent_table = "B" && the Child record would be related to ParentB
      *
      * Now, if we were attempting to insert a record into Child and we had 2
      * rules in the VMPRI table, 1 for ParentA/Child and 1 for ParentB/Child,
      * we should only enforce the 1 rule for the relationship we're creating.
      *
      * We should have a logical condition in the TAG for the child that
      * would EVAL()uate to .T. in order for us to enforce the rule. Something
      * like "chi_parent_table = 'A'" for the ParentA/Child relationship. And
      * "chi_parent_table = 'B'" for the ParentB/Child relationship.
      *
      * For a real life example, see the Users/UsrGrpXref and UserGroups/UsrGrpXref
      * rules in the VMP Example Application's VMPRI table or see the chapter
      * in the VMP Reference Guide that covers Referential Integrity.
      *
      SELECT (m.lcChildAlias)

      lcForExpr = SPACE(0)
      FOR lnTag = 1 TO TAGCOUNT()
        IF ALLTRIM(VMPRI_For_Triggers.XRI_Child_Index_Tag) == TAG(m.lnTag)
          *
          * Do we have a FOR condition?
          *
          lcForExpr = SYS(2021,m.lnTag,ALIAS())
          EXIT
        ENDIF
      ENDFOR
      IF NOT EMPTY(m.lcForExpr) AND NOT EVALUATE(m.lcForExpr)
        LOOP
      ENDIF
      *
      *  lcChildFKField could be an expression, and is
      *  stored here only for debugging information
      *  passed to VMP_RI_Error -- all we really need
      *  to do the work is lcChildFKToSEEKInParentAlias,
      *  which could be obtained directly as:
      *     lcChildFKToSEEKInParentAlias = ;
      *           EVALUATE(KEY(TAGNO(ALLTRIM(VMPRI_For_Triggers.XRI_Child_Index_Tag), ;
      *           CDX(1,ALIAS()),ALIAS()),ALIAS()))
      *
      lcChildFKField = KEY(TAGNO(ALLTRIM(VMPRI_For_Triggers.XRI_Child_Index_Tag),CDX(1,ALIAS()),ALIAS()),ALIAS())
      lcChildFKToSEEKInParentAlias = EVALUATE(m.lcChildFKField)

      lcParentAlias = "_VMP_RI_" + ALLTRIM(VMPRI_For_Triggers.XRI_Parent_TableName)
      *
      * Attempt to open the PARENT alias:
      *
      llRetVal = VMP_RI_Open(ALLTRIM(VMPRI_For_Triggers.XRI_Parent_TableName),m.lcParentAlias)
      IF NOT m.llRetVal
        *
        *  'log' this problem
        *
        VMP_RI_Error("Unable to open the Parent table", ;
                     "Unable to open the Parent table", ;
                     "Unable to VMP_RI_Open("+ALLTRIM(VMPRI_For_Triggers.XRI_Parent_TableName)+")", ;
                     PROGRAM(), ;
                     m.lcParentAlias, ;
                     m.lcParentPK, ;
                     m.lcParentPKExpr, ;
                     m.lcChildAlias, ;
                     m.lcChildFKField )
        EXIT && the SCAN...ENDSCAN
      ENDIF

      SELECT (m.lcParentAlias)
      lcParentPKTag = DBGETPROP(CURSORGETPROP("SourceName",ALIAS()),"TABLE","PRIMARYKEY")
      IF EMPTY(m.lcParentPKTag)
        llRetVal = .F.
      ENDIF
      *
      *  We will RESTRICT the INSERT INTO the child table if 
      *  there is NOT a matching key value in the PARENT table:
      *
      IF m.llRetVal
        llRetVal = SEEK(m.lcChildFKToSEEKInParentAlias,m.lcParentAlias,m.lcParentPKTag)
      ENDIF

      USE IN SELECT(m.lcParentAlias)	&& USE IN (m.lcParentAlias)

      IF NOT m.llRetVal
        *
        *  'log' this problem
        *
        IF EMPTY(m.lcParentPKTag)
          VMP_RI_Error("Not a VFP error, but rather no PRIMARY KEY index tag!", ;
                       "No PRIMARY KEY index tag!", ;
                       "Insert Trigger", ;
                       PROGRAM(), ;
                       m.lcParentAlias, ;
                       m.lcParentPK, ;
                       m.lcParentPKExpr, ;
                       m.lcChildAlias, ;
                       m.lcChildFKField )
          IF _VFP.StartMode = 0 AND _Screen.Visible = .t.
            ACTIVATE SCREEN
            CLEAR 
            ?  "VMP_RI() Insert Trigger for " + m.lcParentAlias + " failed because it has no PRIMARY KEY index tag"
          ENDIF
         ELSE
          VMP_RI_Error("Not a VFP error, but rather the SEEK() in the Parent table failed", ;
                       "Insert-Restrict SEEK() failed to find the Child.FK value in the Parent.PK", ;
                       [SEEK(] + IIF(VARTYPE(m.lcChildFKToSEEKInParentAlias)=[N],TRANSFORM(m.lcChildFKToSEEKInParentAlias),["]+m.lcChildFKToSEEKInPARENTAlias+["]) + [,"] + m.lcParentAlias + [","] + m.lcParentPKTag + [") failed], ;
                       PROGRAM(), ;
                       m.lcParentAlias, ;
                       m.lcParentPK, ;
                       m.lcParentPKExpr, ;
                       m.lcChildAlias, ;
                       m.lcChildFKField )
        ENDIF   &&& EMPTY(lcParentPKTag)
        EXIT   &&& the SCAN...ENDSCAN
      ENDIF   &&& NOT llRetVal
    ENDSCAN
    IF m.lnRecno > 0
      GOTO m.lnRecno
    ENDIF

  ************************************************* 
  CASE lcTrigger = "U" && "UPDATE"
  ************************************************* 
    *
    * UPDATEs are from the PARENT's perspective:
    *
    *    CASCADE - UPDATES ALL related records in the 
    *              CHILD table with the PARENT's key
    *
    *   RESTRICT - Prohibits UPDATE if there are related 
    *              records in the CHILD table
    *
    *     IGNORE - Allows the UPDATE and leaves related
    *              records in the CHILD table (most likely 
    *              creating orphan records in the CHILD table)
    *
    lcTriggerSourceName = CURSORGETPROP("SOURCENAME")  && Could be an Alias
    lcParentAlias       = ALIAS()
    lcParentPKTag       = DBGETPROP(CURSORGETPROP("SourceName",ALIAS()),"TABLE","PRIMARYKEY")
    IF EMPTY(m.lcParentPKTag)
      llRetVal = .f.
      VMP_RI_Error("Not a VFP error, but rather no PRIMARY KEY index tag!", ;
                   "No PRIMARY KEY index tag!", ;
                   "Update Trigger", ;
                   PROGRAM(), ;
                   m.lcParentAlias, ;
                   "no Parent PK value", ;
                   "no Parent PK tag", ;
                   "no Child Alias", ;
                   "no Child PK field")
      lcParentPK = SPACE(0)
      IF _VFP.StartMode = 0 AND _Screen.Visible = .t.
        ACTIVATE SCREEN
        CLEAR 
        ?  "VMP_RI() Update Trigger for " + m.lcParentAlias + " failed because it has no PRIMARY KEY index tag"
      ENDIF
     ELSE
      lcParentPKExpr = KEY(TAGNO(DBGETPROP(CURSORGETPROP("SourceName",ALIAS()),"TABLE","PRIMARYKEY"), ;
                       CDX(1,ALIAS()),ALIAS()),ALIAS())
      lcParentPK     = EVALUATE(m.lcParentPKExpr)
      lcChildAlias   = SPACE(0)
      *
      *  If a table in a database or a cursor has 
      *  validation rules, OLDVAL() does not require
      *  that row or table buffering be enabled
      *  in order to return original field values.
      *  And while a trigger is executing, OLDVAL()
      *  works, I assume because VFP has to implicitly
      *  buffer the update in order to be able to
      *  deny it (although I can't find this documented
      *  anywhere).
      *
      m.lcOldParentPK = OLDVAL(m.lcParentPKExpr)
    ENDIF
    IF NOT EMPTY(m.lcParentPKTag) ;
         AND NOT m.lcOldParentPK = m.lcParentPK
      *
      *  for each table that is a CHILD of this PARENT,
      *  perform the UPDATE
      *
      *  this IF condition evaluates to .f. if either of
      *  the following are true:
      *
      *  1- for some reason, you haven't created a PRIMARY
      *     index tag on the PARENT table 
      *     (m.lcParentPKTag is empty)
      *     ...llRetVal = .f.
      *
      *  2- the PARENT.PKField has not been changed 
      *     (m.lcOldParentPK = m.lcParentPK)
      *     ...llRetVal = .t.
      *
      SELECT VMPRI_For_Triggers
      lnRecno = IIF(EOF(),0,RECNO())
      lcTriggerSourceName = PADR(m.lcTriggerSourceName,LENC(XRI_Parent_TableName))
      SCAN FOR UPPER(XRI_Parent_TableName) = m.lcTriggerSourceName
        IF NOT INLIST(XRI_Update_Type,"C","R")
          LOOP
        ENDIF
				
        lcChildAlias = "_VMP_RI_" + ALLTRIM(VMPRI_For_Triggers.XRI_Child_TableName)
        *
        * Attempt to open the CHILD table and set the CHILD order:
        *
        llRetVal = VMP_RI_Open(ALLTRIM(VMPRI_For_Triggers.XRI_Child_TableName), ;
                               m.lcChildAlias, ;
                               ALLTRIM(VMPRI_For_Triggers.XRI_Child_Index_Tag))
        IF NOT m.llRetVal
          *
          *  'log' this problem
          *
          LOCAL laError[1]
          AERROR(laError)
          VMP_RI_Error(m.laError[1], ;
                       m.laError[2], ;
                       "Unable to VMP_RI_Open(" + ALLTRIM(VMPRI_For_Triggers.XRI_Child_TableName) + ")", ;
                       PROGRAM(), ;
                       m.lcParentAlias, ;
                       m.lcParentPK, ;
                       m.lcParentPKExpr, ;
                       m.lcChildAlias, ;
                       "unknown - Child Alias could not be opened" )
          RELEASE laError
          EXIT && the SCAN...ENDSCAN
        ENDIF

        SELECT (m.lcChildAlias)

        DO CASE
          ****************************************************
          CASE VMPRI_For_Triggers.XRI_Update_Type = "C"
          ****************************************************
            *
            * We need to UPDATE all CHILD records
            * related to this PARENT record:
            *
            lcChildFKField = KEY(TAGNO(ALLTRIM(VMPRI_For_Triggers.XRI_Child_Index_Tag), ;
                              CDX(1,ALIAS()),ALIAS()),ALIAS())
            DO WHILE .T.
              SEEK m.lcOldParentPK
              IF NOT FOUND()
                EXIT && the DO WHILE .T./ENDDO
              ENDIF
              *
              * Attempt to UPDATE this record:
              *
              IF _TriggerLevel = 1 AND NOT m.llTransaction
                * 
                *  the first time we have an actual
                *  update to perform, start a transaction
                *
                llTransaction = .t.
                BEGIN TRANSACTION
              ENDIF
              IF ISRLOCKED() OR NOT RLOCK()
                *
                *  either this record is locked by another process
                *  by this user, which we'll assume means we won't
                *  be able to delete it here
                *  OR
                *  we cannot get an RLOCK() here, indicating that
                *  there's no point attempting the REPLACE below
                *
                llRetVal = .F.
                *
                *  'log' this problem
                *
                LOCAL laError[1]
                AERROR(laError)
                IF ISRLOCKED()
                  VMP_RI_Error(m.laError[1], ;
                               m.laError[2], ;
                               "Unable RLOCK() " + m.lcChildAlias + " because it is already locked by another process", ;
                               PROGRAM(), ;
                               m.lcParentAlias, ;
                               m.lcParentPK, ;
                               m.lcParentPKExpr, ;
                               m.lcChildAlias, ;
                               m.lcChildFKField )
                 ELSE   &&& NOT RLOCK()
                  VMP_RI_Error(m.laError[1], ;
                               m.laError[2], ;
                               "Unable RLOCK() " + m.lcChildAlias, ;
                               PROGRAM(), ;
                               m.lcParentAlias, ;
                               m.lcParentPK, ;
                               m.lcParentPKExpr, ;
                               m.lcChildAlias, ;
                               m.lcChildFKField )
                ENDIF   
                RELEASE laError
               ELSE
                *
                * Is this a concatenated Foreign Key field?
                *
                IF OCCURS("+", m.lcChildFKField) > 0
                  *
                  * This IS a concatenated Foreign Key Field:
                  *
                  LOCAL lnStart, lnEnd, lnStringPosition, lcFieldName, ;
                        lnFieldLen, lnCount                         
                  lnStart = 1
                  lnStringPosition = 1
                  FOR lnCount = 1 TO OCCURS("+", m.lcChildFKField) + 1
                    *
                    * For each field that is in the FK Expression, REPLACE 
                    * that field with a SUBSTRC() of the Parent's Primary Key
                    * value:
                    *
                    lnEnd = AT_C("+", m.lcChildFKField, m.lnCount)
                    lnEnd = IIF(m.lnEnd = 0, LENC(m.lcChildFKField) + 1, m.lnEnd)
                    lcFieldName = SUBSTRC(m.lcChildFKField, m.lnStart, m.lnEnd - m.lnStart)
                    lnFieldLen  = LENC(EVALUATE(m.lcFieldName))
                    loException = .f.
                    TRY
                    REPLACE (m.lcFieldName) ;
                         WITH SUBSTRC(m.lcParentPK, m.lnStringPosition, m.lnFieldLen) ;
                         IN (m.lcChildAlias)
                    CATCH TO loException
                    llRetVal = .f.
                    ENDTRY
                    IF NOT m.llRetVal
                      *
                      *  'log' this problem
                      *
                      LOCAL laError[1]
                      AERROR(laError)
                      VMP_RI_Error(m.laError[1], ;
                                   m.laError[2], ;
                                   [REPLACE ] + m.lcFieldName + [ WITH ] + SUBSTRC(m.lcParentPK, m.lnStringPosition, m.lnFieldLen) + " IN " + lcChildAlias + " failed", ;
                                   PROGRAM(), ;
                                   m.lcParentAlias, ;
                                   m.lcParentPK, ;
                                   m.lcParentPKExpr, ;
                                   m.lcChildAlias, ;
                                   m.lcChildFKField )
                      RELEASE laError
                      EXIT 
                    ENDIF
                    lnStart = m.lnStart + m.lnEnd
                    lnStringPosition = m.lnStringPosition + m.lnFieldLen
                  ENDFOR
                 ELSE
                  *
                  * Nope, we just have 1 Foreign Key field:
                  *
                  loException = .f.
                  TRY
                  REPLACE (m.lcChildFKField) WITH m.lcParentPK IN (m.lcChildAlias)
                  CATCH TO loException
                  llRetVal = .f.
                  ENDTRY
                  IF NOT m.llRetVal
                    *
                    *  'log' this problem
                    *
                    VMP_RI_Error(m.loException, ;
                                 .f., ;
                                 .f., ;
                                 .f., ;
                                 m.lcParentAlias, ;
                                 m.lcParentPK, ;
                                 m.lcParentPKExpr, ;
                                 m.lcChildAlias, ;
                                 m.lcChildFKField )
                  ENDIF
                ENDIF
                UNLOCK RECORD (RECNO())
              ENDIF
              *
              * If we were unsuccessful, then abort the whole thing:
              *
              IF NOT m.llRetVal
                EXIT && the DO WHILE .T./ENDDO
              ENDIF
            ENDDO
          ****************************************************
          CASE VMPRI_For_Triggers.XRI_Update_Type = "R"
          ****************************************************
            *
            * We're RESTRICTing this PARENT from being
            * deleted if there are any CHILD records 
            * for this PARENT record:
            *
            llRetVal = NOT SEEK(m.lcOldParentPK,m.lcChildAlias)
        ENDCASE
        USE IN SELECT(m.lcChildAlias)		&& USE IN (m.lcChildAlias)
        *
        * If we were unsuccessful, then abort the whole thing:
        *
        IF NOT m.llRetVal
          EXIT && the SCAN...ENDSCAN
        ENDIF
      ENDSCAN
      IF m.lnRecno > 0
        GOTO m.lnRecno
      ENDIF
    ENDIF

  ***************************************************   
  CASE lcTrigger = "D" && "DELETE"
  *************************************************** 
    *
    * DELETEs are from the PARENT's perspective:
    *
    *    CASCADE - Deletes ALL related records in the CHILD table
    *
    *   RESTRICT - Prohibits deletion if there are related 
    *              records in the CHILD table
    *
    *     IGNORE - Allows the deletion and leaves related 
    *              records in the CHILD table (most likely
    *              creating orphan records in the CHILD table)
    *
    lcTriggerSourceName = CURSORGETPROP("SOURCENAME")  && Could be an Alias
    lcParentAlias       = ALIAS()
    lcParentPKTag       = DBGETPROP(CURSORGETPROP("SourceName",ALIAS()),"TABLE","PRIMARYKEY")
    IF EMPTY(m.lcParentPKTag)
      llRetVal = .f.
      VMP_RI_Error("Not a VFP error, but rather no PRIMARY KEY index tag!", ;
                   "No PRIMARY KEY index tag!", ;
                   "Update Trigger", ;
                   PROGRAM(), ;
                   m.lcParentAlias, ;
                   "no Parent PK value", ;
                   "no Parent PK tag", ;
                   "no Child Alias", ;
                   "no Child PK field")
      lcParentPK = SPACE(0)
      IF _VFP.StartMode = 0 AND _Screen.Visible = .t.
        ACTIVATE SCREEN
        CLEAR 
        ?  "VMP_RI() Delete Trigger for " + m.lcParentAlias + " failed because it has no PRIMARY KEY index tag"
      ENDIF
     ELSE
      lcParentPKExpr = KEY(TAGNO(DBGETPROP(CURSORGETPROP("SourceName",ALIAS()),"TABLE","PRIMARYKEY"), ;
                           CDX(1,ALIAS()),ALIAS()),ALIAS())
      lcParentPK     = EVALUATE(m.lcParentPKExpr)
      lcChildAlias   = SPACE(0)
      *
      *  for each table that is a CHILD of this PARENT, perform 
      *  the DELETE:
      *
      SELECT VMPRI_For_Triggers
      lnRecno = IIF(EOF(),0,RECNO())
      lcTriggerSourceName = PADR(m.lcTriggerSourceName,LENC(XRI_Parent_TableName))
      SCAN FOR UPPER(XRI_Parent_TableName) == m.lcTriggerSourceName
        IF NOT INLIST(XRI_Delete_Type,"C","R")
          LOOP
        ENDIF
        lcChildAlias = "_VMP_RI_" + ALLTRIM(VMPRI_For_Triggers.XRI_Child_TableName)
        *
        * Attempt to open the CHILD table and set the CHILD order:
        *
        llRetVal = VMP_RI_Open(ALLTRIM(VMPRI_For_Triggers.XRI_Child_TableName), ;
                    m.lcChildAlias, ;
                    ALLTRIM(VMPRI_For_Triggers.XRI_Child_Index_Tag))
        IF NOT m.llRetVal  
          *
          *  'log' this problem
          *
          LOCAL laError[1]
          AERROR(laError)
          VMP_RI_Error(m.laError[1], ;
                       m.laError[2], ;
                       "Unable to VMP_RI_Open(" + ALLTRIM(VMPRI_For_Triggers.XRI_Child_TableName) + ")", ;
                       PROGRAM(), ;
                       m.lcParentAlias, ;
                       m.lcParentPK, ;
                       m.lcParentPKExpr, ;
                       m.lcChildAlias, ;
                       "unknown - Child Alias could not be opened" )
          RELEASE laError
          EXIT && the SCAN...ENDSCAN
        ENDIF

        SELECT (m.lcChildAlias)
        lcChildFKField = KEY(TAGNO(ALLTRIM(VMPRI_For_Triggers.XRI_Child_Index_Tag), ;
                         CDX(1,ALIAS()),ALIAS()),ALIAS())

        DO CASE
          ****************************************************
          CASE VMPRI_For_Triggers.XRI_Delete_Type = "C"
          ****************************************************
            *
            * We need to DELETE all CHILD records 
            * related to this PARENT record:
            *
            DO WHILE .T.
              SEEK m.lcParentPK
              IF NOT FOUND()
                EXIT && the DO WHILE .T./ENDDO
              ENDIF
              *
              * Attempt to DELETE this record
              *
              IF _TriggerLevel = 1 AND NOT m.llTransaction
                * 
                *  the first time we have an actual
                *  update to perform, start a transaction
                *
                llTransaction = .t.
                BEGIN TRANSACTION
              ENDIF
              *
              DO CASE
                ************************************************
                CASE ISRLOCKED() AND NOT DELETED()
                ************************************************
                  *
                  *  assume that this condition indicates that
                  *  the fact that another process on this machine
                  *  has a physical RLOCK() on the record means
                  *  that we won't be able to effect the DELETEion
                  *  here
                  *
                  llRetVal = .f.
                ************************************************
                CASE DELETED()
                ************************************************
                  * 
                  *  ???
                  *
                ************************************************
                CASE NOT RLOCK()
                ************************************************
                  llRetVal = .f.
                ************************************************
                OTHERWISE
                ************************************************
                  *
                  *  RLOCK() AND !DELETED() -- effect the DELETE Cascade 
                  *
                  loException = .f.
                  TRY
                  DELETE 
                  CATCH TO loException
                  llRetVal = .f.
                  ENDTRY
                  IF NOT m.llRetVal
                    *
                    *  'log' this problem
                    *
                    VMP_RI_Error(m.loException, ;
                                 .f., ;
                                 .f., ;
                                 .f., ;
                                 m.lcParentAlias, ;
                                 m.lcParentPK, ;
                                 m.lcParentPKExpr, ;
                                 m.lcChildAlias, ;
                                 m.lcChildFKField )
                  ENDIF
              ENDCASE
              *
              *
              * If we were unsuccessful, abort 
              *
              IF NOT m.llRetVal
                EXIT && the DO WHILE .T./ENDDO
              ENDIF
            ENDDO
          ****************************************************
          CASE VMPRI_For_Triggers.XRI_Delete_Type = "R"
          ****************************************************
            *
            * We're RESTRICTing this PARENT from being
            * deleted if there are any CHILD records 
            * for this PARENT record:
            *
            llRetVal = NOT SEEK(m.lcParentPK,m.lcChildAlias)
            IF NOT m.llRetVal
              VMP_RI_Error("DELETE-RESTRICT RI failed - there is at least 1 Child record for this Parent", ;
                   "Delete-Restrict failed", ;
                   "Delete Trigger", ;
                   PROGRAM(), ;
                   m.lcParentAlias, ;
                   m.lcParentPK, ;
                   m.lcParentPKExpr, ;
                   m.lcChildAlias, ;
                   m.lcChildFKField)
            ENDIF
        ENDCASE
        USE IN SELECT(m.lcChildAlias)	&& USE IN (m.lcChildAlias)
        *
        * If we were unsuccessful, abort
        *
        IF NOT m.llRetVal
          EXIT && the SCAN...ENDSCAN
        ENDIF
      ENDSCAN
      IF m.lnRecno > 0
        GOTO m.lnRecno
      ENDIF
    ENDIF &&& NOT m.llRetVal
ENDCASE

CATCH TO loOuterException
llRetVal = .f.
ENDTRY
IF VARTYPE(m.loOuterException) = "O"
  *
  *  'log' this problem
  *
  VMP_RI_Error(m.loOuterException)
ENDIF

IF _TRIGGERLEVEL = m.gnTriggerLevel
  *
  *  program execution is at the trigger
  *  level where this procedure was first
  *  called (it can be called recursively)
  *
  IF m.llTransaction
    *
    *  a Delete-Cascade or Update-Cascade
    *  updated related records
    *
    IF m.llRetVal
	  END TRANSACTION
     ELSE
	  SET DELETED OFF
	  ROLLBACK
	  SET DELETED ON
    ENDIF
  ENDIF &&& llTransaction
  RELEASE gnTriggerLevel
  IF m.llRetVal
    RELEASE gaVMPRIErrors
   ELSE
    *
    *  NOTE that gaVMPRIErrors is not released here --
    *  you may want/need its information once the
    *  calling code finds that the trigger has failed
    *
  ENDIF &&& llRetVal
ENDIF  &&& _TRIGGERLEVEL = gnTriggerLevel

SET DELETED &lcSetDeleted.
SET EXACT   &lcSetExact.
SET TALK    &lcSetTalk.

IF EMPTY(m.lcSetDatabase) OR NOT DBUSED(m.lcSetDatabase)
  SET DATABASE TO 
 ELSE
  *
  *  trap for strange scenarios (for example, a temporary
  *  database created via XXFWMISC.VCX/cusTempDBC)
  *
  loException = .f.
  TRY
  SET DATABASE TO (m.lcSetDatabase)
  CATCH TO loException
  ENDTRY
  IF VARTYPE(m.loException) = "O"
    SET DATABASE TO 
  ENDIF
ENDIF

SELECT (m.lnSelect)
RETURN m.llRetVal




***********************************************************
PROCEDURE VMP_RI_Open
***********************************************************
*
* Procedure used to open the passed table, give
* it a passed alias and optionally set the order
*
LPARAMETERS tcTable, tcAlias, tcOrder

LOCAL llSuccess

llSuccess = .T.

IF NOT USED(m.tcAlias)
  TRY
  USE (m.tctable) AGAIN ALIAS (m.tcAlias) SHARED IN 0
  CATCH
  llSuccess = .f.
  ENDTRY
ENDIF
IF m.llSuccess AND NOT EMPTY(m.tcOrder)
  TRY
  SET ORDER TO (m.tcOrder) IN (m.tcAlias)
  CATCH
  llSuccess = .f.
  ENDTRY
  *
  *  index tag doesn't exist 
  *
  IF NOT m.llSuccess
    USE IN SELECT(m.tcAlias)	 
  ENDIF
ENDIF

RETURN m.llSuccess



***********************************************************
PROCEDURE VMP_RI_Error
***********************************************************
*
*  this procedure logs a variety of VMP_RI() errors 
*
*  prior to the VMP 2005.06.07.05 build, this
*  was an actual ON ERROR routine, and RETURNed .F.,
*  but now that all error handling is done via
*  TRY/CATCH blocks, this procedure is just an 
*  error LOGGING routine ...I didn't rename this
*  to VMP_RI_LogError, for backward-compatibility
*
LPARAMETERS tnErrNo, ;      &&& may be passed as an Exception object
			tcMessage, ;
			tcCode, ;
			tcProgram, ;
			tcParentAlias, ;
            tcParentPK, ;
			tcParentPKExpr, ;
			tcChildAlias, ;
			tcChildFKField

LOCAL lnErrorRows, ;
      lnXX, ;
      lnErrNo, ;
      lcMessage, ;
      lcCode, ;
      lcProgram, ;
      lcParentAlias, ;
      lcParentPK,; 
      lcParentPKExpr, ;
      lcChildAlias, ;
      lcChildFKField, ;
      lcValue

DO CASE
  CASE VARTYPE(m.tnErrNo) = "O"
    lnErrNo = tnErrNo.ErrorNo
  CASE VARTYPE(m.tnErrNo) = "N"
    lnErrNo = tnErrNo
  OTHERWISE
    lnErrNo = tnErrNo
ENDCASE
DO CASE
  CASE VARTYPE(m.tnErrNo) = "O"
    lcMessage = tnErrNo.Message
  CASE VARTYPE(m.tcMessage) = "C"
    lcMessage = ALLTRIM(m.tcMessage)
  OTHERWISE
    lcMessage = SPACE(0)
ENDCASE
DO CASE
  CASE VARTYPE(m.tnErrNo) = "O"
    lcCode = tnErrNo.LineContents
  CASE VARTYPE(m.tcCode) = "C"
    lcCode = ALLTRIM(m.tcCode)
  OTHERWISE
    lcCode = SPACE(0)
ENDCASE
DO CASE
  CASE VARTYPE(m.tnErrNo) = "O"
    lcProgram = tnErrNo.Procedure
  CASE VARTYPE(m.tcProgram) = "C"
    lcProgram = ALLTRIM(m.tcProgram)
  OTHERWISE
    lcProgram = SPACE(0)
ENDCASE
IF VARTYPE(m.tcParentAlias) = "C"
  lcParentAlias = ALLTRIM(m.tcParentAlias)
 ELSE
  lcParentAlias = SPACE(0)
ENDIF
IF VARTYPE(m.tcParentPK) = "C"
  lcParentPK = ALLTRIM(m.tcParentPK)
 ELSE
  lcParentPK = SPACE(0)
ENDIF
IF VARTYPE(m.tcParentPKExpr) = "C"
  lcParentPKExpr = ALLTRIM(m.tcParentPKExpr)
 ELSE
  lcParentPKExpr = SPACE(0)
ENDIF
IF VARTYPE(m.tcChildAlias) = "C"
  lcChildAlias = ALLTRIM(m.tcChildAlias)
 ELSE
  lcChildAlias = SPACE(0)
ENDIF
IF VARTYPE(m.tcChildFKField) = "C"
  lcChildFKField = ALLTRIM(m.tcChildFKField)
 ELSE
  lcChildFKField = SPACE(0)
ENDIF

*
* If necessary, redimension the error information array.
*
lnErrorRows = ALEN(gaVMPRIErrors, 1)
IF TYPE("gaVMPRIErrors[m.lnErrorRows,1]") # "L"
  DIMENSION gaVMPRIErrors[m.lnErrorRows+1,ALEN(gaVMPRIErrors,2)]
  lnErrorRows = m.lnErrorRows + 1
ENDIF
*
* Populate the rows in the error information array.
*
gaVMPRIErrors[m.lnErrorRows,1] = "VFP ERROR:" + SPACE(1) + TRANSFORM(m.lnErrNo)
gaVMPRIErrors[m.lnErrorRows,2] = "VFP ERROR MESSAGE:" + SPACE(1) + m.lcMessage
gaVMPRIErrors[m.lnErrorRows,3] = "OFFENDING CODE:" + SPACE(1) + m.lcCode
gaVMPRIErrors[m.lnErrorRows,4] = "PROGRAM STACK:" + SPACE(1)
*
* Record the program call stack at the time of the error.
*
lnXX = 1
DO WHILE NOT EMPTY(PROGRAM(m.lnXX))
  gaVMPRIErrors[m.lnErrorRows,4] = gaVMPRIErrors[m.lnErrorRows,4] + IIF(m.lnXX = 1,SPACE(0),",") + PROGRAM(m.lnXX)
  lnXX = m.lnXX + 1
ENDDO

IF NOT EMPTY(m.lcParentAlias) ;
	AND USED(m.lcParentAlias)
  *
  * If a parent alias was passed, and the table is open,
  * get the table name and current record number.
  *
  gaVMPRIErrors[m.lnErrorRows,5] = "PARENT TABLE:" + SPACE(1) + DBF(m.lcParentAlias)
  gaVMPRIErrors[m.lnErrorRows,6] = "PARENT TABLE RECNO():" + SPACE(1) + TRANSFORM(RECNO(m.lcParentAlias))
 ELSE  
  IF NOT EMPTY(m.lcParentAlias)
    gaVMPRIErrors[m.lnErrorRows,5] = "PARENT TABLE:" + SPACE(1) + m.lcParentAlias
  ENDIF
  gaVMPRIErrors[m.lnErrorRows,6] = "PARENT TABLE RECNO():" + SPACE(1) + "unknown"
ENDIF
gaVMPRIErrors[m.lnErrorRows,7] = "PARENT TABLE PK VALUE:" + SPACE(1) + TRANSFORM(m.lcParentPK)
gaVMPRIErrors[m.lnErrorRows,8] = "PARENT TABLE PK EXPRESSION:" + SPACE(1) + m.lcParentPKExpr

DO CASE
  CASE NOT EMPTY(m.lcChildAlias) ;
	    AND USED(m.lcChildAlias)
    *
    * If a child alias was passed, and the table is open,
    * get the table name and current record number.
    *
    gaVMPRIErrors[m.lnErrorRows,9]  = "CHILD TABLE:" + SPACE(1) + DBF(m.lcChildAlias)
    gaVMPRIErrors[m.lnErrorRows,10] = "CHILD TABLE RECNO():" + SPACE(1) + TRANSFORM(RECNO(m.lcChildAlias))
    IF NOT EMPTY(m.lcChildFKField)
      *
      * If a Foreign Key expression was passed for the child table,
      * get it's current value.
      *
      lcValue = EVALUATE(m.lcChildAlias+"."+m.lcChildFKField)
      IF VARTYPE(m.lcValue) = "C"
        lcValue = ["] + m.lcValue + ["]
       ELSE
        lcValue = TRANSFORM(m.lcValue)
      ENDIF
      gaVMPRIErrors[m.lnErrorRows,11] = "CHILD TABLE FK VALUE:" + SPACE(1) + m.lcValue
     ELSE
      gaVMPRIErrors[m.lnErrorRows,11] = "CHILD TABLE FK VALUE:"
    ENDIF
  CASE NOT EMPTY(m.tcChildAlias) AND NOT USED(m.tcChildAlias)
    gaVMPRIErrors[m.lnErrorRows,9]  = "CHILD TABLE VMPRI alias (could not be opened):" + SPACE(1) + m.lcChildAlias
ENDCASE
gaVMPRIErrors[m.lnErrorRows,12] = "CHILD TABLE FK EXPRESSION:" + SPACE(1) + m.lcChildFKField
RETURN 

*
*  This is the end of the VMP Referential-Integrity code.
*
*  The preceding stored procedures were inserted into the
*  Stored Procedures by the XXFWDATA.PRG VMP utility
*
*  !!!!!!!!!!!!!!!!!!!! END VMP RI Comment !!!!!!!!!!!!!!!!!!!!
ENDTEXT
SET TEXTMERGE OFF SHOW
SET TEXTMERGE TO

*
*  and put it all together
*
ERASE newsp.txt   &&& just in case
*
*  build the new stored procedures starting with the
*  code that preceded the old VMP RI stored procedures
*
LOCAL xx, lnCount 
lnCount = 0
IF lnEndLine > 0
  FOR xx = 1 TO lnStartLine-1
    STRTOFILE(laLines[xx]+CHR(13)+CHR(10),"newsp.txt",.t.)
    lnCount = lnCount + 1
  ENDFOR
 ELSE
  FOR xx = 1 TO ALEN(laLines,1)
    STRTOFILE(laLines[xx]+CHR(13)+CHR(10),"newsp.txt",.t.)
    lnCount = lnCount + 1
  ENDFOR
ENDIF

*
*  now insert this new stored procedure
*
LOCAL laNewLines[1]
ALINES(laNewLines,FILETOSTR("thissp.txt"))
DO WHILE EMPTY(laNewLines[1])
  *  get rid of any leading blank lines
  ADEL(laNewLines,1)
  DIMENSION laNewLines[ALEN(laNewLines,1)-1]
ENDDO
*
*  put 4 blank lines between procedures after the first one
*
IF lnCount > 0
  STRTOFILE(CHR(13)+CHR(10)+CHR(13)+CHR(10)+CHR(13)+CHR(10)+CHR(13)+CHR(10),"newsp.txt",.t.)
ENDIF
FOR xx = 1 TO ALEN(laNewLines,1)
  STRTOFILE(laNewLines[xx]+CHR(13)+CHR(10),"newsp.txt",.t.)
ENDFOR

*
*  finally add the stored procedures that were after
*  the old stored procedure
*
IF lnEndLine > 0
  FOR xx = lnStartLine TO ALEN(laLines,1)
    STRTOFILE(laLines[xx]+CHR(13)+CHR(10),"newsp.txt",.t.)
  ENDFOR
ENDIF

*
*  and add a few blank lines in case there are app-specific
*  stored procedures after this
*
STRTOFILE(CHR(13)+CHR(10)+CHR(13)+CHR(10)+CHR(13) + CHR(10),"newsp.txt",.t.)

*
*  and post the new set of stored procedures to the .DBC
*
APPEND PROCEDURES FROM newsp.txt OVERWRITE
CLOSE TABLES
COMPILE DATABASE (DBC())
ERASE oldsp.txt
ERASE thissp.txt
ERASE newsp.txt
PACK DATABASE 
RETURN



PROCEDURE PopulateRITable
IF NOT INDBC("VMPRI","TABLE")
  RETURN
ENDIF
WAIT WINDOW "Populating VMPRI table..." NOWAIT
IF USED("VMPRI") AND NOT ISEXCLUSIVE("VMPRI",1)
  USE IN SELECT('VMPRI')
ENDIF
IF NOT USED("VMPRI")
  USE vmpri EXCLUSIVE
ENDIF
SELECT VMPRI
*
*  Users-->UsrGrpXRef
*    Insert-Restrict (where UGX_MemberType = "U")
*    Update-Cascade
*    Delete-Cascade
*     
DELETE ALL FOR UPPER(ALLTRIM(XRI_Parent_TableName)) == "USERS" ;
     AND UPPER(ALLTRIM(XRI_Child_TableName)) == "USRGRPXREF" ;
     AND UPPER(ALLTRIM(XRI_Child_Index_Tag)) == "UGX_USRFK"
PACK
IF INDBC("UsrGrpXRef","TABLE")    
  INSERT INTO VMPRI ;
	  (XRI_Parent_TableName,XRI_Child_TableName,XRI_CHILD_INDEX_TAG,XRI_INSERT_TYPE,XRI_UPDATE_TYPE,XRI_DELETE_TYPE) ;
  VALUES ;
	  ("USERS","USRGRPXREF","UGX_USRFK","R","C","C")
ENDIF
*
*  UserGroups-->UsrGrpXRef
*  (relationship between UGr_PK and UGX_MemberFK FOR UGX_MemberType = "G")
*    Insert-Restrict  
*    Update-Cascade   
*    Delete-Restrict  
*     
DELETE ALL FOR UPPER(ALLTRIM(XRI_Parent_TableName)) == "USERGROUPS" ;
     AND UPPER(ALLTRIM(XRI_Child_TableName)) == "USRGRPXREF" ;
     AND UPPER(ALLTRIM(XRI_Child_Index_Tag)) == "UGX_GRPFK"
PACK
IF INDBC("UserGroups","TABLE")   
  INSERT INTO VMPRI ;
	  (XRI_Parent_TableName,XRI_Child_TableName,XRI_CHILD_INDEX_TAG,XRI_INSERT_TYPE,XRI_UPDATE_TYPE,XRI_DELETE_TYPE) ;
  VALUES ;
	  ("USERGROUPS","USRGRPXREF","UGX_GRPFK","R","C","R")
ENDIF	  
*
*  UserGroups-->UsrGrpXRef
*  (relationship between UGr_PK and UGX_UGrFK)
*    Insert-Restrict 
*    Update-Cascade  
*    Delete-Restrict 
*     
DELETE ALL FOR UPPER(ALLTRIM(XRI_Parent_TableName)) == "USERGROUPS" ;
     AND UPPER(ALLTRIM(XRI_Child_TableName)) == "USRGRPXREF" ;
     AND UPPER(ALLTRIM(XRI_Child_Index_Tag)) == "UGX_UGRFK"
PACK
IF INDBC("UserGroups","TABLE")   
  INSERT INTO VMPRI ;
	  (XRI_Parent_TableName,XRI_Child_TableName,XRI_CHILD_INDEX_TAG,XRI_INSERT_TYPE,XRI_UPDATE_TYPE,XRI_DELETE_TYPE) ;
  VALUES ;
	  ("USERGROUPS","USRGRPXREF","UGX_UGRFK","R","C","R")
ENDIF
IF INDBC("UserAccExceptions","TABLE")
  *
  *  Users-->UserAccExceptions
  *    Insert-Restrict
  *    Update-Cascade
  *    Delete-Cascade
  *     
  DELETE ALL FOR UPPER(ALLTRIM(XRI_Parent_TableName)) == "USERS" ;
       AND UPPER(ALLTRIM(XRI_Child_TableName)) == "USERACCEXCEPTIONS" ;
       AND UPPER(ALLTRIM(XRI_Child_Index_Tag)) == "UAE_USRFK"
  PACK
  INSERT INTO VMPRI ;
	  (XRI_Parent_TableName,XRI_Child_TableName,XRI_CHILD_INDEX_TAG,XRI_INSERT_TYPE,XRI_UPDATE_TYPE,XRI_DELETE_TYPE) ;
  VALUES ;
	  ("USERS","USERACCEXCEPTIONS","UAE_USRFK","R","C","C")
ENDIF  && UserAccExceptions

IF INDBC("UserPrefs","TABLE")
  *
  *  Users-->UserPrefs
  *    Insert-Restrict
  *    Update-Cascade
  *    Delete-Cascade
  *     
  DELETE ALL FOR UPPER(ALLTRIM(XRI_Parent_TableName)) == "USERS" ;
       AND UPPER(ALLTRIM(XRI_Child_TableName)) == "USERPREFS" ;
       AND UPPER(ALLTRIM(XRI_Child_Index_Tag)) == "PRF_USRFK"
  PACK
  INSERT INTO VMPRI ;
	  (XRI_Parent_TableName,XRI_Child_TableName,XRI_CHILD_INDEX_TAG,XRI_INSERT_TYPE,XRI_UPDATE_TYPE,XRI_DELETE_TYPE) ;
  VALUES ;
	  ("USERS","USERPREFS","PRF_USRFK","R","C","C")
ENDIF && UserPrefs	  

IF INDBC("LoginHistory","TABLE")
  *
  *  Users-->LoginHistory
  *    Insert-Restrict
  *    Update-Cascade
  *    Delete-Restrict
  *     
  DELETE ALL FOR UPPER(ALLTRIM(XRI_Parent_TableName)) == "USERS" ;
       AND UPPER(ALLTRIM(XRI_Child_TableName)) == "LOGINHISTORY" ;
       AND UPPER(ALLTRIM(XRI_Child_Index_Tag)) == "ULH_USRFK"
  PACK
  INSERT INTO VMPRI ;
	  (XRI_Parent_TableName,XRI_Child_TableName,XRI_CHILD_INDEX_TAG,XRI_INSERT_TYPE,XRI_UPDATE_TYPE,XRI_DELETE_TYPE) ;
  VALUES ;
	  ("USERS","LOGINHISTORY","ULH_USRFK","R","C","R")
ENDIF

IF INDBC("SecurityAccess","TABLE")
  *
  *  UserGroups-->SecurityAccess
  *    Insert-Restrict 
  *    Update-Cascade
  *    Delete-Cascade
  *     
  DELETE ALL FOR UPPER(ALLTRIM(XRI_Parent_TableName)) == "USERGROUPS" ;
       AND UPPER(ALLTRIM(XRI_Child_TableName)) == "SECURITYACCESS" ;
       AND UPPER(ALLTRIM(XRI_Child_Index_Tag)) == "ACC_UGRFK"
  PACK
  INSERT INTO VMPRI ;
	  (XRI_Parent_TableName,XRI_Child_TableName,XRI_CHILD_INDEX_TAG,XRI_INSERT_TYPE,XRI_UPDATE_TYPE,XRI_DELETE_TYPE) ;
  VALUES ;
	  ("USERGROUPS","SECURITYACCESS","ACC_UGRFK","R","C","C")
ENDIF

*
*  Users-->IUDLog
*  Insert-Ignore
*  Update-Cascade
*  Delete-Restrict
*
IF INDBC("IUDLOG","TABLE") AND INDBC("USERS","TABLE")
  DELETE ALL FOR UPPER(ALLTRIM(XRI_Parent_TableName)) == "USERS" ;
       AND UPPER(ALLTRIM(XRI_Child_TableName)) == "IUDLOG" ;
       AND UPPER(ALLTRIM(XRI_Child_Index_Tag)) == "IUD_USRFK"
  PACK
  INSERT INTO VMPRI (XRI_PARENT_TABLENAME,XRI_CHILD_TABLENAME,XRI_CHILD_INDEX_TAG,XRI_INSERT_TYPE,XRI_UPDATE_TYPE,XRI_DELETE_TYPE) VALUES ;
       ( ; 										
       "USERS", ; 
       "IUDLOG", ; 
       "IUD_UsrFK", ; 
       "I", ;     &&& IUD_UsrFK is left empty when Users.Usr_PK info is not available, and IUD_Usr is populated instead
       "C", ; 
       "R" ; 
       )
ENDIF

USE IN SELECT('VMPRI')
RETURN



********************************************************
PROCEDURE CreateTriggers
********************************************************
*
*  add triggers, assuming 
*    UpdateRIStoredProcedures()
*    UpdateIUDLogStoredProcedures()
*  have been called, and the 
*    VMPRI 
*    VMPIUDLog 
*  tables exist -- if not the triggers are
*  REMOVED here
*
WAIT WINDOW "Updating Referential Integrity/IUDLog triggers..." NOWAIT

LOCAL laTables[5], xx, lcTable

*
*  tables that get both RI and IUDLog triggers
*

laTables[1] = "USERS"
laTables[2] = "USERGROUPS"
laTables[3] = "USRGRPXREF"
laTables[4] = "SECURITYACCESS"
laTables[5] = "USERACCEXCEPTIONS"

FOR xx = 1 TO ALEN(laTables,1)
  lcTable = laTables[m.xx]
  IF INDBC(m.lcTable,"TABLE")
    DO CASE
      CASE INDBC("VMPRI","TABLE") AND (INDBC("IUDLOG","TABLE") OR INDBC("IUDLog_"+m.lcTable,"TABLE"))
        CREATE TRIGGER ON (m.lcTable) FOR INSERT AS VMP_RI('INSERT') AND SP_VMP_IUDLog('I')
        CREATE TRIGGER ON (m.lcTable) FOR UPDATE AS VMP_RI('UPDATE') AND SP_VMP_IUDLog('U')
        CREATE TRIGGER ON (m.lcTable) FOR DELETE AS VMP_RI('DELETE') AND SP_VMP_IUDLog('D')
      CASE INDBC("VMPRI","TABLE")
        CREATE TRIGGER ON (m.lcTable) FOR INSERT AS VMP_RI('INSERT')
        CREATE TRIGGER ON (m.lcTable) FOR UPDATE AS VMP_RI('UPDATE')
        CREATE TRIGGER ON (m.lcTable) FOR DELETE AS VMP_RI('DELETE')
      CASE INDBC("IUDLOG","TABLE")
        CREATE TRIGGER ON (m.lcTable) FOR INSERT AS SP_VMP_IUDLog('I')
        CREATE TRIGGER ON (m.lcTable) FOR UPDATE AS SP_VMP_IUDLog('U')
        CREATE TRIGGER ON (m.lcTable) FOR DELETE AS SP_VMP_IUDLog('D')
      OTHERWISE
        DELETE TRIGGER ON (m.lcTable) FOR INSERT 
        DELETE TRIGGER ON (m.lcTable) FOR UPDATE  
        DELETE TRIGGER ON (m.lcTable) FOR DELETE  
    ENDCASE
  ENDIF
ENDFOR

*
*  tables that get IUDLog triggers only
*

DIMENSION laTables[2]
laTables[1] = "REPORTCATALOG"
laTables[2] = "VMPRI"

FOR xx = 1 TO ALEN(laTables,1)
  lcTable = laTables[m.xx]
  IF INDBC(m.lcTable,"TABLE")
    DO CASE
      CASE INDBC("IUDLOG","TABLE") OR INDBC("IUDLog_"+m.lcTable,"TABLE")
        CREATE TRIGGER ON (m.lcTable) FOR INSERT AS SP_VMP_IUDLog('I')
        CREATE TRIGGER ON (m.lcTable) FOR UPDATE AS SP_VMP_IUDLog('U')
        CREATE TRIGGER ON (m.lcTable) FOR DELETE AS SP_VMP_IUDLog('D')
      OTHERWISE
        DELETE TRIGGER ON (m.lcTable) FOR INSERT 
        DELETE TRIGGER ON (m.lcTable) FOR UPDATE  
        DELETE TRIGGER ON (m.lcTable) FOR DELETE  
    ENDCASE
  ENDIF
ENDFOR

*
*  tables that get VMPRI triggers only
*
DIMENSION laTables[1]
laTables[1] = "LOGINHISTORY"
FOR xx = 1 TO ALEN(laTables,1)
  lcTable = laTables[m.xx]
  IF INDBC(m.lcTable,"TABLE")
    CREATE TRIGGER ON (m.lcTable) FOR INSERT AS VMP_RI('INSERT')
    CREATE TRIGGER ON (m.lcTable) FOR UPDATE AS VMP_RI('UPDATE')
    CREATE TRIGGER ON (m.lcTable) FOR DELETE AS VMP_RI('DELETE')
  ENDIF
ENDFOR

*
*  other tables
*

IF INDBC("USERPREFS","TABLE")
  *
  *  note:  do not attempt to add Update/DELETE
  *         VMP_RI() trigger from Users to UserPrefs,
  *         which won't work since UserPrefs has
  *         no Primary Key index
  *
  *  note:  we're not bothering with the IUDLog trigger,
  *         because UserPrefs is not a table we care
  *         about logging
  *
  DO CASE
    CASE INDBC("VMPRI","TABLE")
      CREATE TRIGGER ON USERPREFS FOR INSERT AS VMP_RI('INSERT')
    OTHERWISE
      DELETE TRIGGER ON USERPREFS FOR INSERT
  ENDCASE
ENDIF

RELEASE laTables
LOCAL laTables[1]
IF ADBOBJECTS(laTables,"TABLE") > 0
  FOR xx = 1 TO ALEN(laTables,1)
    lcTable = laTables[m.xx]
    IF UPPER(m.lcTable) == "IUDLOG" OR LEFTC(UPPER(m.lcTable),7) == "IUDLOG_"
      CREATE TRIGGER ON IUDLog FOR INSERT AS SP_VMP_IUDLog_RW()  &&& cannot manually INSERT records, outside the SP_VMP_IUDLog Stored procedure
      CREATE TRIGGER ON IUDLog FOR UPDATE AS SP_VMP_IUDLog_RW()  &&& once created, cannot be updated
      CREATE TRIGGER ON IUDLog FOR DELETE AS SP_VMP_IUDLog_RW()  &&& once created, cannot be deleted
    ENDIF  
  ENDFOR
ENDIF

*
*  these tables get no triggers:
*    APPCONFIG (free)
*    APPINFO (no PK, and to pass "App_Item" as the 2nd SP_VMP_IUDLog() parameter, the IUDLog.IUD_PKFieldValue field would have to be increased to C(40) in size)
*    GeneratePK (don't want logging for this table)
*    MemoAE (no need)
*    LoggedInUsers (no PK, is itself a system-generated log table)
*

PACK DATABASE 

RETURN




PROCEDURE GetDBCDir
*
* Procedure used to return the fullpath of the DBC
* currently in use. We will create all specified
* tables in the directory where the DBC resides.
*
* This procedure assumes that either:
*
* - the developer passed .F. as the third parameter
*   (the default) and this program will create the
*   specified database and tables in the current
*   default directory
*
* - the developer has explicitly opened a database,
*   passed .T. as the third parameter and the specified
*   tables will be created in the directory where the
*   database resides
*
*
RETURN ADDBS(JUSTPATH(DBC()))




PROCEDURE AddSPDependentRules
*
*  add Stored Procedure-dependent rules (call this procedure
*  only after UpdateSystemStoredProcedures has been executed)
*
LPARAMETERS tcTable
IF VARTYPE(m.plAbort) = "L" AND m.plAbort
  RETURN
ENDIF
WAIT WINDOW "Adding Stored Procedure-dependent rules..." NOWAIT
LOCAL lcTable
lcTable = UPPER(ALLTRIM(tcTable))
DO CASE
  CASE lcTable = "APPINFO"
  CASE lcTable = "USERS"
  CASE lcTable = "USERPREFS"
  CASE lcTable = "LOGINHISTORY"
  CASE lcTable = "LOGGEDINUSERS"
  CASE lcTable = "GENERATEPK"
  CASE lcTable = "MEMOAE"
  CASE lcTable = "REPORTCATALOG"
  CASE lcTable = "SECURITY"
    *
    *  Row rule   
    *
    ALTER TABLE UsrGrpXRef ;
         SET CHECK Row_Rule_UsrGrpXRef() ;
         ERROR XXFWRuleErrorText("RR_UsrGrpXRef","Primary Key, Group, Member, and Member Type must all be entered, and the Member must be on file") 
    *
    *  field rule
    *  (the extra OR conditions calling X3SEEK() are
    *  necessary because, depending on when the rule 
    *  gets called, the member may be in the buffer 
    *  of Users/UserGroups but not yet in the Users/
    *  UserGroups table)
    *
    ALTER TABLE UsrGrpXRef ;
         ALTER COLUMN UGX_MemberFK ;
         SET CHECK Field_Rule_UGX_MemberFK() ;
         ERROR XXFWRuleErrorText("FR_UGX_MemberFK","The Member you have entered is not on file.")
ENDCASE
WAIT CLEAR
RETURN

PROCEDURE CreateV_UsersList
IF !UpdateCheck(1)
  RETURN .f.
ENDIF
IF !INDBC("Users","TABLE")
  RETURN .t.
ENDIF
*
*  create an updateable view designed to retrieve one
*  record per user in the Users table for those users:
*  1- whose Usr_System = "N"
*  2- who are Active, Inactive, or Either (All non-hidden users):
*		All users:  	lcUsr_Active = "Y"	lcUsr_Inactive = "Y"
*		Active only:	lcUsr_Active = "Y"	lcUsr_Inactive = "N"
*		Inactive only:	lcUsr_Active = "N"	lcUsr_Inactive = "Y"
*
LOCAL lcDatabasePlusTable
lcDatabasePlusTable = JUSTSTEM(SET("DATABASE")) + "!USERS"

CREATE SQL VIEW V_UsersList AS ;
    SELECT * ;
    FROM &lcDatabasePlusTable. ;
    WHERE Usr_System = 'N' ;
		AND (NOT Usr_Active = ?lcUsr_Inactive OR Usr_Active = ?lcUsr_Active)

DBSETPROP('V_UsersList', 'View', 'UpdateType', 1 )
DBSETPROP('V_UsersList', 'View', 'WhereType', 3 )
DBSETPROP('V_UsersList', 'View', 'FetchMemo', .T. )
DBSETPROP('V_UsersList', 'View', 'SendUpdates', .T. )
DBSETPROP('V_UsersList', 'View', 'UseMemoSize', 255 )
DBSETPROP('V_UsersList', 'View', 'FetchSize', -1 )
DBSETPROP('V_UsersList', 'View', 'MaxRecords', -1 )
DBSETPROP('V_UsersList', 'View', 'Tables', lcDatabasePlusTable )
DBSETPROP('V_UsersList', 'View', 'Prepared', .F.)
DBSETPROP('V_UsersList', 'View', 'CompareMemo', .F.)
DBSETPROP('V_UsersList', 'View', 'FetchAsNeeded', .F.)
DBSETPROP('V_UsersList', 'View', 'Comment', "")
DBSETPROP('V_UsersList', 'View', 'BatchUpdateCount', 1)
DBSETPROP('V_UsersList', 'View', 'ShareConnection', .T.)

DBSETPROP( 'V_UsersList.usr_pk', 'Field', 'KeyField', .T. )
DBSETPROP( 'V_UsersList.usr_pk', 'Field', 'Updatable', .T. )
DBSETPROP( 'V_UsersList.usr_pk', 'Field', 'UpdateName', lcDatabasePlusTable+".Usr_pk" )

IF INDBC("Users.Usr_Rights","FIELD")
  DBSETPROP("V_UsersList.Usr_Rights","FIELD","DefaultValue", ;
            dbgetprop("USERS.Usr_Rights", ;
                      "FIELD", ;
                      "DEFAULTVALUE"))
ENDIF
DBSETPROP("V_UsersList.Usr_System","FIELD","DefaultValue", ;
             dbgetprop("USERS.Usr_System", ;
                       "FIELD", ;
                       "DEFAULTVALUE"))
DBSETPROP("V_UsersList.Usr_Active","FIELD","DefaultValue", ;
             dbgetprop("USERS.Usr_Active", ;
                       "FIELD", ;
                       "DEFAULTVALUE"))
DBSETPROP("V_UsersList.Usr_EnablePrefs","FIELD","DefaultValue", ;
             dbgetprop("USERS.Usr_EnablePrefs", ;
                       "FIELD", ;
                       "DEFAULTVALUE"))
DBSETPROP("V_UsersList.Usr_INTLLanguage","FIELD","DefaultValue", ;
             dbgetprop("USERS.Usr_INTLLanguage", ;
                       "FIELD", ;
                       "DEFAULTVALUE"))
IF INDBC("Users.Usr_LastLogin","FIELD")
  DBSETPROP("V_UsersList.Usr_LastLogin","FIELD","DefaultValue", ;
            dbgetprop("USERS.Usr_LastLogin", ;
                      "FIELD", ;
                      "DEFAULTVALUE"))
ENDIF
RETURN

PROCEDURE CreateV_LoginHistoryOneUser
IF !UpdateCheck(1)
  RETURN .f.
ENDIF
IF !INDBC("LoginHistory","TABLE") OR !INDBC("Users","TABLE")
  RETURN .t.
ENDIF
LOCAL lcDatabasePlusTable, lcDatabase
lcDatabasePlusTable = JUSTSTEM(SET("DATABASE")) + "!LoginHistory"
lcDatabase = JUSTSTEM(SET("DATABASE"))

CREATE SQL VIEW V_LoginHistoryOneUser AS ;
    SELECT LoginHistory.*, SPACE(35) AS FullName, ;
         Users.Usr_LastName, Users.Usr_FirstName, Users.Usr_MiddleInitial ;
    FROM &lcDatabasePlusTable. ;
      INNER JOIN &lcDatabase.!Users ;
        ON LoginHistory.ULH_UsrFK = Users.Usr_PK ;
    WHERE ULH_UsrFK = ?luUsr_PK ;
      AND ULH_In >= ?ltULH_In
DBSETPROP( 'V_LoginHistoryOneUser', 'View', 'UpdateType', 1 )
DBSETPROP( 'V_LoginHistoryOneUser', 'View', 'WhereType', 3 )
DBSETPROP( 'V_LoginHistoryOneUser', 'View', 'FetchMemo', .T. )
DBSETPROP( 'V_LoginHistoryOneUser', 'View', 'SendUpdates', .T. )
DBSETPROP( 'V_LoginHistoryOneUser', 'View', 'UseMemoSize', 255 )
DBSETPROP( 'V_LoginHistoryOneUser', 'View', 'FetchSize', -1 )
DBSETPROP( 'V_LoginHistoryOneUser', 'View', 'MaxRecords', -1 )
DBSETPROP( 'V_LoginHistoryOneUser', 'View', 'Tables', lcDatabasePlusTable )
DBSETPROP('V_LoginHistoryOneUser', 'View', 'Prepared', .F.)
DBSETPROP('V_LoginHistoryOneUser', 'View', 'CompareMemo', .F.)
DBSETPROP('V_LoginHistoryOneUser', 'View', 'FetchAsNeeded', .F.)
DBSETPROP('V_LoginHistoryOneUser', 'View', 'Comment', "")
DBSETPROP('V_LoginHistoryOneUser', 'View', 'BatchUpdateCount', 1)
DBSETPROP('V_LoginHistoryOneUser', 'View', 'ShareConnection', .T.)

DBSETPROP( 'V_LoginHistoryOneUser.ULH_PK', 'Field', 'KeyField', .T. )
DBSETPROP( 'V_LoginHistoryOneUser.ULH_PK', 'Field', 'Updatable', .T. )
DBSETPROP( 'V_LoginHistoryOneUser.ULH_PK', 'Field', 'UpdateName', lcDatabasePlusTable+".ULH_PK" )

DBSETPROP("V_LoginHistoryOneUser.Usr_LastName","Field","KeyField",.f.)
DBSETPROP("V_LoginHistoryOneUser.Usr_LastName","Field","Updatable",.f.)
DBSETPROP("V_LoginHistoryOneUser.Usr_FirstName","Field","KeyField",.f.)
DBSETPROP("V_LoginHistoryOneUser.Usr_FirstName","Field","Updatable",.f.)
DBSETPROP("V_LoginHistoryOneUser.Usr_MiddleInitial","Field","KeyField",.f.)
DBSETPROP("V_LoginHistoryOneUser.Usr_MiddleInitial","Field","Updatable",.f.)

DBSETPROP("V_LoginHistoryOneUser.ULH_Out","FIELD","DefaultValue", ;
             dbgetprop("LoginHistory.ULH_Out", ;
                       "FIELD", ;
                       "DEFAULTVALUE"))
DBSETPROP("V_LoginHistoryOneUser.ULH_OutFlag","FIELD","DefaultValue", ;
             dbgetprop("LoginHistory.ULH_OutFlag", ;
                       "FIELD", ;
                       "DEFAULTVALUE"))
DBSETPROP("V_LoginHistoryOneUser.ULH_Machine","FIELD","DefaultValue", ;
             dbgetprop("LoginHistory.ULH_Machine", ;
                       "FIELD", ;
                       "DEFAULTVALUE"))
RETURN 

PROCEDURE CreateV_LoginHistoryAllUsers
IF !UpdateCheck(1)
  RETURN .f.
ENDIF
IF !INDBC("LoginHistory","TABLE") OR !INDBC("Users","TABLE")
  RETURN .t.
ENDIF
LOCAL lcDatabasePlusTable, lcDatabase
lcDatabasePlusTable = JUSTSTEM(SET("DATABASE")) + "!LoginHistory"
lcDatabase = JUSTSTEM(SET("DATABASE"))

CREATE SQL VIEW V_LoginHistoryAllUsers AS ;
    SELECT LoginHistory.*, SPACE(35) AS FullName, ;
         Users.Usr_LastName, Users.Usr_FirstName, Users.Usr_MiddleInitial ;
    FROM &lcDatabasePlusTable. ;
      INNER JOIN &lcDatabase.!Users ;
        ON LoginHistory.ULH_UsrFK = Users.Usr_PK ;
    WHERE Usr_System = 'N' ;
      AND ULH_In >= ?ltULH_In 

DBSETPROP( 'V_LoginHistoryAllUsers', 'View', 'UpdateType', 1 )
DBSETPROP( 'V_LoginHistoryAllUsers', 'View', 'WhereType', 3 )
DBSETPROP( 'V_LoginHistoryAllUsers', 'View', 'FetchMemo', .T. )
DBSETPROP( 'V_LoginHistoryAllUsers', 'View', 'SendUpdates', .T. )
DBSETPROP( 'V_LoginHistoryAllUsers', 'View', 'UseMemoSize', 255 )
DBSETPROP( 'V_LoginHistoryAllUsers', 'View', 'FetchSize', -1 )
DBSETPROP( 'V_LoginHistoryAllUsers', 'View', 'MaxRecords', -1 )
DBSETPROP( 'V_LoginHistoryAllUsers', 'View', 'Tables', lcDatabasePlusTable )
DBSETPROP('V_LoginHistoryAllUsers', 'View', 'Prepared', .F.)
DBSETPROP('V_LoginHistoryAllUsers', 'View', 'CompareMemo', .F.)
DBSETPROP('V_LoginHistoryAllUsers', 'View', 'FetchAsNeeded', .F.)
DBSETPROP('V_LoginHistoryAllUsers', 'View', 'Comment', "")
DBSETPROP('V_LoginHistoryAllUsers', 'View', 'BatchUpdateCount', 1)
DBSETPROP('V_LoginHistoryAllUsers', 'View', 'ShareConnection', .T.)

DBSETPROP( 'V_LoginHistoryAllUsers.ULH_PK', 'Field', 'KeyField', .T. )
DBSETPROP( 'V_LoginHistoryAllUsers.ULH_PK', 'Field', 'Updatable', .T. )
DBSETPROP( 'V_LoginHistoryAllUsers.ULH_PK', 'Field', 'UpdateName', lcDatabasePlusTable+".ULH_PK" )

DBSETPROP("V_LoginHistoryAllUsers.Usr_LastName","Field","KeyField",.f.)
DBSETPROP("V_LoginHistoryAllUsers.Usr_LastName","Field","Updatable",.f.)
DBSETPROP("V_LoginHistoryAllUsers.Usr_FirstName","Field","KeyField",.f.)
DBSETPROP("V_LoginHistoryAllUsers.Usr_FirstName","Field","Updatable",.f.)
DBSETPROP("V_LoginHistoryAllUsers.Usr_MiddleInitial","Field","KeyField",.f.)
DBSETPROP("V_LoginHistoryAllUsers.Usr_MiddleInitial","Field","Updatable",.f.)

DBSETPROP("V_LoginHistoryAllUsers.ULH_Out","FIELD","DefaultValue", ;
             dbgetprop("LoginHistory.ULH_Out", ;
                       "FIELD", ;
                       "DEFAULTVALUE"))
DBSETPROP("V_LoginHistoryAllUsers.ULH_OutFlag","FIELD","DefaultValue", ;
             dbgetprop("LoginHistory.ULH_OutFlag", ;
                       "FIELD", ;
                       "DEFAULTVALUE"))
DBSETPROP("V_LoginHistoryAllUsers.ULH_Machine","FIELD","DefaultValue", ;
             dbgetprop("LoginHistory.ULH_Machine", ;
                       "FIELD", ;
                       "DEFAULTVALUE"))
RETURN

PROCEDURE CreateV_GroupMembers
IF NOT UpdateCheck(1)
  RETURN .f.
ENDIF
IF NOT INDBC("Users","TABLE") ;
     OR NOT INDBC("UserGroups","TABLE") ;
     OR NOT INDBC("UsrGrpXRef","TABLE")
  RETURN .t.
ENDIF
LOCAL lcDatabase
lcDatabase = JUSTSTEM(SET("DATABASE"))

*
*  create an updateable view designed to retrieve
*  all Members of a specific Group 
*

*!*	*  NOTE: the following view can be used instead
*!*	*  of the more complicated one below when
*!*	*  oUser.oSecurity.ilAllowGroupsAsGroupMembers = .F.
*!*	CREATE SQL VIEW V_GroupMembers AS ;
*!*	    SELECT UsrGrpXRef.*, UserGroups.*, Usr_PK, Usr_LastName, Usr_FirstName, Usr_MiddleInitial ;
*!*	    FROM &lcDatabase.!UsrGrpXRef ;
*!*	      INNER JOIN &lcDatabase.!UserGroups ;
*!*	        ON UGX_UGrFK = UGr_PK ;
*!*	      INNER JOIN &lcDatabase.!Users ;
*!*	        ON UGX_MemberFK = Usr_PK ;
*!*	    WHERE UGX_UGrFK = ?luUGr_PK ;
*!*	      AND UGr_System = "N" ;
*!*	      AND Usr_System = "N" ;
*!*	    ORDER BY Usr_LastName, Usr_FirstName, Usr_MiddleInitial

CREATE SQL VIEW V_GroupMembers AS ;
	SELECT DISTINCT USRGRPXREF.*, ;
	                USERGROUPS.*, ;
	                Usr_LastName, ;
	                Usr_Firstname, ;
	                Usr_MiddleInitial, ;
	                Usr_PK ;
	FROM &lcDatabase.!USERS, ;
	     &lcDatabase.!USERGROUPS, ;
	     &lcDatabase.!USRGRPXREF, ;
	     &lcDatabase.!USERGROUPS AS SRCGROUP ;
	WHERE UGX_UGrFK = ?luUGr_PK ;
	  AND UGX_UGrFK = USERGROUPS.UGr_PK ;
	  AND Usr_System = 'N' ;
      AND USERGROUPS.UGr_System = 'N' ;
	  AND (UGX_MemberType = 'U' AND UGX_MemberFK = Usr_PK) ;
	UNION ;
	SELECT USRGRPXREF.*, ;
	       USERGROUPS.*, ;
	       SRCGROUP.UGr_group AS Usr_LastName, ;
		   '' AS Usr_FirstName, ;
		   '' AS Usr_MiddleInitial, ;
		   SRCGROUP.UGr_PK AS Usr_PK ;
	FROM &lcDatabase.!USERS, ;
	     &lcDatabase.!USERGROUPS, ;
		 &lcDatabase.!USRGRPXREF, ;
		 &lcDatabase.!USERGROUPS AS SRCGROUP ;
	WHERE UGX_UGrFK = ?luUGr_PK ;
	  AND UGX_UGrFK = USERGROUPS.UGr_PK ;
	  AND USERGROUPS.UGr_System = 'N' ;
	  AND (UGX_MemberType = 'G' AND UGX_MemberFK = SRCGROUP.UGr_PK) ;
        ORDER BY 8,9,10 
**		ORDER BY Usr_LastName, Usr_FirstName, Usr_MiddleInitial

DBSETPROP( 'V_GroupMembers', 'View', 'UpdateType', 1 )
DBSETPROP( 'V_GroupMembers', 'View', 'WhereType', 3 )
DBSETPROP( 'V_GroupMembers', 'View', 'FetchMemo', .T. )
DBSETPROP( 'V_GroupMembers', 'View', 'SendUpdates', .T. )
DBSETPROP( 'V_GroupMembers', 'View', 'UseMemoSize', 255 )
DBSETPROP( 'V_GroupMembers', 'View', 'FetchSize', -1 )
DBSETPROP( 'V_GroupMembers', 'View', 'MaxRecords', -1 )
DBSETPROP( 'V_GroupMembers', 'View', 'Tables', lcDatabase + "!UsrGrpXRef" )
DBSETPROP( 'V_GroupMembers', 'View', 'Prepared', .F.)
DBSETPROP( 'V_GroupMembers', 'View', 'CompareMemo', .F.)
DBSETPROP( 'V_GroupMembers', 'View', 'FetchAsNeeded', .F.)
DBSETPROP( 'V_GroupMembers', 'View', 'Comment', "")
DBSETPROP( 'V_GroupMembers', 'View', 'BatchUpdateCount', 1)
DBSETPROP( 'V_GroupMembers', 'View', 'ShareConnection', .T.)

DBSETPROP( 'V_GroupMembers.UGX_PK', 'Field', 'KeyField', .T. )
DBSETPROP( 'V_GroupMembers.UGX_PK', 'Field', 'Updatable', .T. )
DBSETPROP( 'V_GroupMembers.UGX_PK', 'Field', 'UpdateName', lcDatabase + "!UsrGrpXRef.UGX_PK" )

DBSETPROP( 'V_GroupMembers.UGX_UgrFK', 'Field', 'KeyField', .F. )
DBSETPROP( 'V_GroupMembers.UGX_UGrFK', 'Field', 'Updatable', .T. )
DBSETPROP( 'V_GroupMembers.UGX_UGrFK', 'Field', 'UpdateName', lcDatabase + "!UsrGrpXRef.UGX_UGrFK" )

DBSETPROP( 'V_GroupMembers.UGX_MemberFK', 'Field', 'KeyField', .F. )
DBSETPROP( 'V_GroupMembers.UGX_MemberFK', 'Field', 'Updatable', .T. )
DBSETPROP( 'V_GroupMembers.UGX_MemberFK', 'Field', 'UpdateName', lcDatabase + "!UsrGrpXRef.UGX_MemberFK" )

DBSETPROP( 'V_GroupMembers.UGX_MemberType', 'Field', 'KeyField', .F. )
DBSETPROP( 'V_GroupMembers.UGX_MemberType', 'Field', 'Updatable', .T. )
DBSETPROP( 'V_GroupMembers.UGX_MemberType', 'Field', 'UpdateName', lcDatabase + "!UsrGrpXRef.UGX_MemberType" )
RETURN

PROCEDURE CreateV_UserGroupsList
IF !UpdateCheck(1)
  RETURN .f.
ENDIF
IF !INDBC("UserGroups","TABLE")
  RETURN .t.
ENDIF
LOCAL lcDatabasePlusTable 
lcDatabasePlusTable = JUSTSTEM(SET("DATABASE")) + "!UserGroups"

*
*  V_UserGroupsList updateable view of all non-hidden UserGroups
*
CREATE SQL VIEW V_UserGroupsList AS ;
    SELECT * ;
    FROM &lcDatabasePlusTable. ;
    WHERE UGr_System = 'N' ;
    ORDER BY UGr_Group
DBSETPROP( 'V_UserGroupsList', 'View', 'UpdateType', 1 )
DBSETPROP( 'V_UserGroupsList', 'View', 'WhereType', 3 )
DBSETPROP( 'V_UserGroupsList', 'View', 'FetchMemo', .T. )
DBSETPROP( 'V_UserGroupsList', 'View', 'SendUpdates', .T. )
DBSETPROP( 'V_UserGroupsList', 'View', 'UseMemoSize', 255 )
DBSETPROP( 'V_UserGroupsList', 'View', 'FetchSize', -1 )
DBSETPROP( 'V_UserGroupsList', 'View', 'MaxRecords', -1 )
DBSETPROP( 'V_UserGroupsList', 'View', 'Tables', lcDatabasePlusTable )
DBSETPROP( 'V_UserGroupsList', 'View', 'Prepared', .F.)
DBSETPROP( 'V_UserGroupsList', 'View', 'CompareMemo', .F.)
DBSETPROP( 'V_UserGroupsList', 'View', 'FetchAsNeeded', .F.)
DBSETPROP( 'V_UserGroupsList', 'View', 'Comment', "")
DBSETPROP( 'V_UserGroupsList', 'View', 'BatchUpdateCount', 1)
DBSETPROP( 'V_UserGroupsList', 'View', 'ShareConnection', .T.)

DBSETPROP( 'V_UserGroupsList.UGr_PK', 'Field', 'KeyField', .T. )
DBSETPROP( 'V_UserGroupsList.UGr_PK', 'Field', 'Updatable', .T. )
DBSETPROP( 'V_UserGroupsList.UGr_PK', 'Field', 'UpdateName', lcDatabasePlusTable+".UGr_PK" )

DBSETPROP("V_UserGroupsList.UGr_System","FIELD","DefaultValue", ;
          dbgetprop("UserGroups.UGr_System", ;
                    "FIELD", ;
                    "DEFAULTVALUE"))
RETURN

PROCEDURE CreateV_UsrGrpXRef
IF !UpdateCheck(1)
  RETURN .f.
ENDIF
IF !INDBC("UsrGrpXRef","TABLE") OR !INDBC("UserGroups","TABLE")
  RETURN .t.
ENDIF
LOCAL lcDatabase
lcDatabase = JUSTSTEM(SET("DATABASE"))
    
*
*  create an updateable view designed to retrieve
*  all user groups to which one user belongs
*

CREATE SQL VIEW V_UsrGrpXRef AS ;
    SELECT * ;
    FROM &lcDatabase.!UsrGrpXRef ;
      INNER JOIN &lcDatabase.!UserGroups ;
        ON UsrGrpXRef.UGX_UGrFK = UserGroups.UGr_PK ;
    WHERE UGX_MemberFK = ?luUsr_PK ;
      AND UGX_MemberType = 'U' ;
    ORDER BY UGr_Group
    
DBSETPROP( 'V_UsrGrpXRef', 'View', 'UpdateType', 1 )
DBSETPROP( 'V_UsrGrpXRef', 'View', 'WhereType', 3 )
DBSETPROP( 'V_UsrGrpXRef', 'View', 'FetchMemo', .T. )
DBSETPROP( 'V_UsrGrpXRef', 'View', 'SendUpdates', .T. )
DBSETPROP( 'V_UsrGrpXRef', 'View', 'UseMemoSize', 255 )
DBSETPROP( 'V_UsrGrpXRef', 'View', 'FetchSize', -1 )
DBSETPROP( 'V_UsrGrpXRef', 'View', 'MaxRecords', -1 )
DBSETPROP( 'V_UsrGrpXRef', 'View', 'Tables', lcDatabase+"!UsrGrpXRef" )
DBSETPROP( 'V_UsrGrpXRef', 'View', 'Prepared', .F.)
DBSETPROP( 'V_UsrGrpXRef', 'View', 'CompareMemo', .F.)
DBSETPROP( 'V_UsrGrpXRef', 'View', 'FetchAsNeeded', .F.)
DBSETPROP( 'V_UsrGrpXRef', 'View', 'Comment', "")
DBSETPROP( 'V_UsrGrpXRef', 'View', 'BatchUpdateCount', 1)
DBSETPROP( 'V_UsrGrpXRef', 'View', 'ShareConnection', .T.)

DBSETPROP( 'V_UsrGrpXRef.UGX_PK', 'Field', 'KeyField', .T. )
DBSETPROP( 'V_UsrGrpXRef.UGX_PK', 'Field', 'Updatable', .T. )
DBSETPROP( 'V_UsrGrpXRef.UGX_PK', 'Field', 'UpdateName', lcDatabase+"!UsrGrpXRef.UGX_PK" )

DBSETPROP( 'V_UsrGrpXRef.UGX_UGrFK', 'Field', 'KeyField', .F. )
DBSETPROP( 'V_UsrGrpXRef.UGX_UGrFK', 'Field', 'Updatable', .T. )
DBSETPROP( 'V_UsrGrpXRef.UGX_UGrFK', 'Field', 'UpdateName', lcDatabase+"!UsrGrpXRef.UGX_UGrFK" )

DBSETPROP( 'V_UsrGrpXRef.UGX_MemberFK', 'Field', 'KeyField', .F. )
DBSETPROP( 'V_UsrGrpXRef.UGX_MemberFK', 'Field', 'Updatable', .T. )
DBSETPROP( 'V_UsrGrpXRef.UGX_MemberFK', 'Field', 'UpdateName', lcDatabase+"!UsrGrpXRef.UGX_MemberFK" )

DBSETPROP( 'V_UsrGrpXRef.UGX_MemberType', 'Field', 'KeyField', .F. )
DBSETPROP( 'V_UsrGrpXRef.UGX_MemberType', 'Field', 'Updatable', .T. )
DBSETPROP( 'V_UsrGrpXRef.UGX_MemberType', 'Field', 'UpdateName', lcDatabase+"!UsrGrpXRef.UGX_MemberType")

DBSETPROP( 'V_UsrGrpXRef.UGr_PK', 'Field', 'KeyField', .F. )
DBSETPROP( 'V_UsrGrpXRef.UGr_PK', 'Field', 'Updatable', .F. )
DBSETPROP( 'V_UsrGrpXRef.UGr_PK', 'Field', 'UpdateName', lcDatabase+"!UserGroups.UGr_PK")
RETURN

PROCEDURE CreateV_UsrGrpXRefList
IF !UpdateCheck(1)
  RETURN .f.
ENDIF
IF !INDBC("UsrGrpXRef","TABLE") OR !INDBC("UserGroups","TABLE")
  RETURN .t.
ENDIF
LOCAL lcDatabase
lcDatabase = JUSTSTEM(SET("DATABASE"))
*
*  create a non-updateable, non-parameterized view
*  designed to retrieve all UsrGrpXRef records 
*  for groups that are not "hidden"/"system" groups
*
CREATE SQL VIEW V_UsrGrpXRefList AS ;
    SELECT * ;
    FROM &lcDatabase.!UsrGrpXRef ;
      INNER JOIN &lcDatabase.!UserGroups ;
        ON UsrGrpXRef.UGX_UGrFK = UserGroups.UGr_PK ;
    WHERE UGr_System = 'N' ;
    ORDER BY UGr_Group
DBSETPROP( 'V_UsrGrpXRefList', 'View', 'UpdateType', 1 )
DBSETPROP( 'V_UsrGrpXRefList', 'View', 'WhereType', 3 )
DBSETPROP( 'V_UsrGrpXRefList', 'View', 'FetchMemo', .T. )
DBSETPROP( 'V_UsrGrpXRefList', 'View', 'SendUpdates', .F. )
DBSETPROP( 'V_UsrGrpXRefList', 'View', 'UseMemoSize', 255 )
DBSETPROP( 'V_UsrGrpXRefList', 'View', 'FetchSize', -1 )
DBSETPROP( 'V_UsrGrpXRefList', 'View', 'MaxRecords', -1 )
DBSETPROP( 'V_UsrGrpXRefList', 'View', 'Tables', lcDatabase + "!UsrGrpXRef" )
DBSETPROP( 'V_UsrGrpXRefList', 'View', 'Prepared', .F.)
DBSETPROP( 'V_UsrGrpXRefList', 'View', 'CompareMemo', .F.)
DBSETPROP( 'V_UsrGrpXRefList', 'View', 'FetchAsNeeded', .F.)
DBSETPROP( 'V_UsrGrpXRefList', 'View', 'Comment', "")
DBSETPROP( 'V_UsrGrpXRefList', 'View', 'BatchUpdateCount', 1)
DBSETPROP( 'V_UsrGrpXRefList', 'View', 'ShareConnection', .T.)
RETURN

PROCEDURE CreateV_SecurityAccess
IF !UpdateCheck(1)
  RETURN .f.
ENDIF
LOCAL lcDatabase, lcSITable
lcDatabase = JUSTSTEM(SET("DATABASE"))
lcSITable = lcDatabase + "USYSSI"
IF !FILE(lcSITable+".DBF")
  IF !USED("AppInfo")
    USE AppInfo IN 0
  ENDIF
  lcSITable = UPPER(JUSTSTEM(DBF("AppInfo")))
  lcSITable = STRTRAN(lcSITable,"SYSTEM","")
  lcSITable = lcSITable + "USYSSI"
ENDIF
IF !INDBC("SecurityAccess","TABLE") ;
    OR !INDBC("UserGroups","TABLE") ;
    OR !FILE(lcSITable+".DBF")
  RETURN .t.
ENDIF
*
*  create a view of 
*  SecuredItems + SecurityAccess + UserGroups
*  where SecurityAccess is the updateable table,
*  allowing the Administrator to maintain access
*  to SecuredItems and to build the Security
*  cursor in XXFWSEC.VCX/cusUserSecurity.SetupCursors()
*
*  note that SecuredItems linked to "hidden"/system 
*  UserGroups are not retrieved
*
*  note that CLIU_Access stores the access for the
*  currently-logged-in user
*
CREATE SQL VIEW V_SecurityAccess AS ;
    SELECT SecurityAccess.*, ;
          SIt_Desc, SIt_Type, SIt_TypeCD, SIt_PK, SIt_ID, ;
          UGr_Group, UGr_PK, ;
          0 AS CLIU_Access ;
    FROM &lcSITable. ;
      INNER JOIN &lcDatabase.!SecurityAccess ;
        ON &lcSITable..SIt_PK = SecurityAccess.Acc_SItFK ;
      INNER JOIN &lcDatabase.!UserGroups ;
        ON SecurityAccess.Acc_UGrFK = UserGroups.UGr_PK ;
    WHERE UGr_System = 'N'
   
DBSETPROP( 'V_SecurityAccess', 'View', 'UpdateType', 1 )
DBSETPROP( 'V_SecurityAccess', 'View', 'WhereType', 3 )
DBSETPROP( 'V_SecurityAccess', 'View', 'FetchMemo', .T. )
DBSETPROP( 'V_SecurityAccess', 'View', 'SendUpdates', .T. )
DBSETPROP( 'V_SecurityAccess', 'View', 'UseMemoSize', 255 )
DBSETPROP( 'V_SecurityAccess', 'View', 'FetchSize', -1 )
DBSETPROP( 'V_SecurityAccess', 'View', 'MaxRecords', -1 )
DBSETPROP( 'V_SecurityAccess', 'View', 'Tables', lcDatabase + "!SecurityAccess" )
DBSETPROP( 'V_SecurityAccess', 'View', 'Prepared', .F.)
DBSETPROP( 'V_SecurityAccess', 'View', 'CompareMemo', .F.)
DBSETPROP( 'V_SecurityAccess', 'View', 'FetchAsNeeded', .F.)
DBSETPROP( 'V_SecurityAccess', 'View', 'Comment', "")
DBSETPROP( 'V_SecurityAccess', 'View', 'BatchUpdateCount', 1)
DBSETPROP( 'V_SecurityAccess', 'View', 'ShareConnection', .T.)

DBSETPROP( 'V_SecurityAccess.Acc_PK', 'Field', 'KeyField', .T. )
DBSETPROP( 'V_SecurityAccess.Acc_PK', 'Field', 'Updatable', .T. )
DBSETPROP( 'V_SecurityAccess.Acc_PK', 'Field', 'UpdateName', lcDatabase + "!SecurityAccess.Acc_PK" )

DBSETPROP( 'V_SecurityAccess.Acc_UGrFK', 'Field', 'KeyField', .F. )
DBSETPROP( 'V_SecurityAccess.Acc_UGrFK', 'Field', 'Updatable', .T. )
DBSETPROP( 'V_SecurityAccess.Acc_UGrFK', 'Field', 'UpdateName', lcDatabase + "!SecurityAccess.Acc_UGrFK")

DBSETPROP( 'V_SecurityAccess.Acc_SItFK', 'Field', 'KeyField', .F. )
DBSETPROP( 'V_SecurityAccess.Acc_SItFK', 'Field', 'Updatable', .T. )
DBSETPROP( 'V_SecurityAccess.Acc_SItFK', 'Field', 'UpdateName', lcDatabase + "!SecurityAccess.Acc_SItFK")

DBSETPROP( 'V_SecurityAccess.Acc_Access', 'Field', 'KeyField', .F. )
DBSETPROP( 'V_SecurityAccess.Acc_Access', 'Field', 'Updatable', .T. )
DBSETPROP( 'V_SecurityAccess.Acc_Access', 'Field', 'UpdateName', lcDatabase + "!SecurityAccess.Acc_Access")

DBSETPROP("V_SecurityAccess.Acc_Access","FIELD","DefaultValue", ;
             dbgetprop("SecurityAccess.Acc_Access", ;
                       "FIELD", ;
                       "DEFAULTVALUE"))
USE IN SELECT(lcSITable)	&& USE IN (lcSITable)
RETURN

PROCEDURE CreateV_UserAccExceptions
IF !UpdateCheck(1)
  RETURN .f.
ENDIF
LOCAL lcDatabase, lcSITable
lcDatabase = JUSTSTEM(SET("DATABASE"))
lcSITable = lcDatabase + "USYSSI"
IF !FILE(lcSITable+".DBF")
  IF !USED("AppInfo")
    USE AppInfo IN 0
  ENDIF
  lcSITable = UPPER(JUSTSTEM(DBF("AppInfo")))
  lcSITable = STRTRAN(lcSITable,"SYSTEM","")
  lcSITable = lcSITable + "USYSSI"
ENDIF
IF !INDBC("UserAccExceptions","TABLE") OR !FILE(lcSITable+".DBF")
  RETURN .t.
ENDIF
*
*  add a view of UserAccExceptions intended
*  for data-entry, and parameterized to
*  retrieve all records for a single user
*
*  note that CLIU_Access stores the access for the
*  currently-logged-in user
*
CREATE SQL VIEW V_UserAccExceptions AS ;
    SELECT UserAccExceptions.*, ;
          SIt_Desc, SIt_Type, SIt_TypeCD, SIt_PK, SIt_ID, ;
          0 AS CLIU_Access ;
    FROM &lcSITable. ;
      INNER JOIN &lcDatabase.!UserAccExceptions ;
        ON &lcSITable..SIt_PK = UserAccExceptions.UAE_SItFK ;
    WHERE UAE_UsrFK = ?luUAE_UsrFK
   
DBSETPROP( 'V_UserAccExceptions', 'View', 'UpdateType', 1 )
DBSETPROP( 'V_UserAccExceptions', 'View', 'WhereType', 3 )
DBSETPROP( 'V_UserAccExceptions', 'View', 'FetchMemo', .T. )
DBSETPROP( 'V_UserAccExceptions', 'View', 'SendUpdates', .T. )
DBSETPROP( 'V_UserAccExceptions', 'View', 'UseMemoSize', 255 )
DBSETPROP( 'V_UserAccExceptions', 'View', 'FetchSize', -1 )
DBSETPROP( 'V_UserAccExceptions', 'View', 'MaxRecords', -1 )
DBSETPROP( 'V_UserAccExceptions', 'View', 'Tables', lcDatabase + "!UserAccExceptions" )
DBSETPROP( 'V_UserAccExceptions', 'View', 'Prepared', .F.)
DBSETPROP( 'V_UserAccExceptions', 'View', 'CompareMemo', .F.)
DBSETPROP( 'V_UserAccExceptions', 'View', 'FetchAsNeeded', .F.)
DBSETPROP( 'V_UserAccExceptions', 'View', 'Comment', "")
DBSETPROP( 'V_UserAccExceptions', 'View', 'BatchUpdateCount', 1)
DBSETPROP( 'V_UserAccExceptions', 'View', 'ShareConnection', .T.)

DBSETPROP( 'V_UserAccExceptions.UAE_PK', 'Field', 'KeyField', .T. )
DBSETPROP( 'V_UserAccExceptions.UAE_PK', 'Field', 'Updatable', .T. )
DBSETPROP( 'V_UserAccExceptions.UAE_PK', 'Field', 'UpdateName', lcDatabase + "!UserAccExceptions.UAE_PK" )

DBSETPROP("V_UserAccExceptions.UAE_Access","FIELD","DefaultValue", ;
             dbgetprop("UserAccExceptions.UAE_Access", ;
                       "FIELD", ;
                       "DEFAULTVALUE"))
USE IN SELECT(lcSITable)	&& USE IN (lcSITable)
RETURN

PROCEDURE CreateV_UserPrefs
IF NOT UpdateCheck(1)
  RETURN .f.
ENDIF
IF NOT INDBC("UserPrefs","TABLE")
  RETURN .t.
ENDIF
LOCAL lcDatabasePlusTable
lcDatabasePlusTable = JUSTSTEM(SET("DATABASE")) + "!UserPrefs"
CREATE SQL VIEW V_UserPrefs AS ;
    SELECT * ;
    FROM &lcDatabasePlusTable. ;
    WHERE UserPrefs.Prf_UsrFK = ?luUsr_PK
DBSETPROP('V_UserPrefs', 'View', 'UpdateType', 1 )
DBSETPROP('V_UserPrefs', 'View', 'WhereType', 3 )
DBSETPROP('V_UserPrefs', 'View', 'FetchMemo', .T. )
DBSETPROP('V_UserPrefs', 'View', 'SendUpdates', .T. )
DBSETPROP('V_UserPrefs', 'View', 'UseMemoSize', 255 )
DBSETPROP('V_UserPrefs', 'View', 'FetchSize', -1 )
DBSETPROP('V_UserPrefs', 'View', 'MaxRecords', -1 )
DBSETPROP('V_UserPrefs', 'View', 'Tables', lcDatabasePlusTable )
DBSETPROP('V_UserPrefs', 'View', 'Prepared', .F.)
DBSETPROP('V_UserPrefs', 'View', 'CompareMemo', .F.)
DBSETPROP('V_UserPrefs', 'View', 'FetchAsNeeded', .F.)
DBSETPROP('V_UserPrefs', 'View', 'Comment', "")
DBSETPROP('V_UserPrefs', 'View', 'BatchUpdateCount', 1)
DBSETPROP('V_UserPrefs', 'View', 'ShareConnection', .T.)

DBSETPROP('V_UserPrefs.Prf_UsrFK', 'Field', 'KeyField', .T. )
DBSETPROP('V_UserPrefs.Prf_UsrFK', 'Field', 'Updatable', .T. )
DBSETPROP('V_UserPrefs.Prf_UsrFK', 'Field', 'UpdateName', lcDatabasePlusTable+".Prf_UsrFK" )
DBSETPROP('V_UserPrefs.Prf_Item', 'Field', 'KeyField', .T. )
DBSETPROP('V_UserPrefs.Prf_Item', 'Field', 'Updatable', .T. )
DBSETPROP('V_UserPrefs.Prf_Item', 'Field', 'UpdateName', lcDatabasePlusTable+".Prf_Item" )
RETURN 




PROCEDURE UpdateCheck
*
*  this procedure is called from the procedures in this 
*  .PRG that can be called directly, to ensure that a 
*  database is open and current (and sometimes EXCLUSIVE)
*
*  when called from those procedures as a result of 
*  executing DO XXFWDATA WITH ..., this procedure should 
*  always RETURN .T.
*
LPARAMETERS tnSingleCheck
LOCAL lnSingleCheck
IF VARTYPE(tnSingleCheck) = "N" AND INLIST(tnSingleCheck,1,2)
  lnSingleCheck = tnSingleCheck
 ELSE
  lnSingleCheck = 0
ENDIF
IF lnSingleCheck = 0 OR lnSingleCheck = 1
  IF EMPTY(SET("DATABASE"))
    MESSAGEBOX("Procedure " + PROGRAM(PROGRAM(-1)-1) + " in XXFWDATA.PRG " + ;
               "requires that a VFP database be open and the current SET " + ;
               "DATABASE TO database." + CHR(13) + CHR(13) + ;
               "Please open and SET DATABASE TO " + ;
               "the desired database before trying again.", ;
               16, ;
               "Please Note")
    RETURN .f.
  ENDIF
ENDIF
IF lnSingleCheck = 0 or lnSingleCheck = 2
  IF !ISEXCLUSIVE(SET("DATABASE"),2)
    MESSAGEBOX("Procedure " + PROGRAM(PROGRAM(-1)-1) + " in XXFWDATA.PRG " + ;
               "requires that the current SET DATABASE TO database be " + ;
               "open for EXCLUSIVE use, so that the COMPILE DATABASE " + ;
               "command can be successfully executed." + CHR(13) + CHR(13) + ;
               "Please OPEN DATABASE " + SET("DATABASE") + " EXCLUSIVE " + ;
               "before trying again.", ;
               16, ;
               "Please Note")
    RETURN .f.
  ENDIF
ENDIF
SET SAFETY OFF
SET TALK OFF
RETURN .t.



*****************************************************
PROCEDURE UpdateRI
*****************************************************
*
*  update the RI stored procedures,
*  populate the VMPRI table,
*  create the RI triggers
*
IF VARTYPE(m.plAbort) = "L" AND m.plAbort
  RETURN
ENDIF
IF NOT UpdateCheck()
  RETURN 
ENDIF
UpdateRIStoredProcedures()
PopulateRITable()
CreateTriggers()
WAIT WINDOW "Update of VMPRI and RI stored procedures is complete" NOWAIT
RETURN 



*****************************************************
PROCEDURE UpdateSystemStoredProcedures
*****************************************************
*
*  for each non-RI and non-IUDLog stored procedure created here:
*    - see if it exists, if so, remove it
*    - see if it is needed, if so, add it again from scratch
*   
IF VARTYPE(m.plAbort) = "L" AND m.plAbort
  RETURN
ENDIF
IF NOT UpdateCheck()
  RETURN 
ENDIF
WAIT WINDOW "Updating system stored procedures..." NOWAIT
SP_XXFWRuleErrorText()
SP_Row_Rule_UsrGrpXRef()
SP_Field_Rule_UGX_MemberFK()
PACK DATABASE 
WAIT WINDOW "Update of system stored procedures is complete" NOWAIT
RETURN 



*****************************************************
PROCEDURE UpdateAllStoredProcedures
*****************************************************
IF VARTYPE(m.plAbort) = "L" AND m.plAbort
  RETURN
ENDIF
IF !UpdateCheck()
  RETURN
ENDIF
WAIT WINDOW "Updating all stored procedures..." NOWAIT
UpdateSystemStoredProcedures()
UpdateRI()
UpdateIUDLog()
PACK DATABASE 
WAIT WINDOW "Update of all stored procedures is complete" NOWAIT
RETURN



*****************************************************
PROCEDURE UpdateIUDLog
*****************************************************
*
*  update the IUDLog stored procedures,
*  add the SP_VMP_IUDLog() triggers to existing tables
*
IF VARTYPE(m.plAbort) = "L" AND m.plAbort
  RETURN
ENDIF
IF NOT INDBC("IUDLOG","TABLE")
  RETURN
ENDIF
IF NOT UpdateCheck()
  RETURN
ENDIF
IUDLogStoredProcedures()
CreateTriggers()
RETURN 



*****************************************************
PROCEDURE IUDLogStoredProcedures
*****************************************************
*
*  update existing IUD Stored Procedures, or add them
*  if they don't exist
*
*  call this program after creating and populating the
*  IUDLog table
*
*  first, delete them -- whether this .DBC needs them or
*  not, we'll start from scratch
*
IF VARTYPE(m.plAbort) = "L" AND m.plAbort
  RETURN
ENDIF
WAIT WINDOW "Updating IUDLog stored procedures..." NOWAIT
LOCAL laLines[1], xx, lnStartLine, lnEndLine
lnStartLine = 0
lnEndLine = 0
COPY PROCEDURES TO oldsp.txt
*
*  first see the SP_VMP_IUDLog stored procedures exist, and, if so, 
*  note the starting and ending lines
*
IF ALINES(laLines,ALLTRIM(FILETOSTR("oldsp.txt"))) > 0
  IF ALEN(laLines,1) > 1
    DO WHILE EMPTY(laLines[1])
      *  get rid of any leading blank lines
      ADEL(laLines,1)
      DIMENSION laLines[ALEN(laLines,1)-1]
    ENDDO
  ENDIF
  FOR xx = 1 TO ALEN(laLines,1)
    IF UPPER(" Begin VMP IUDLog Comment ") $ UPPER(laLines[m.xx]) 
      lnStartLine = xx
    ENDIF
    IF UPPER(" End VMP IUDLog Comment ") $ UPPER(laLines[m.xx]) 
      lnEndLine = m.xx 
      EXIT
    ENDIF
  ENDFOR
  IF m.lnStartLine = 0
    *
    *  VMP IUDLog are not currently in the stored procedures
    *
    lnStartLine = ALEN(laLines,1) + 1
   ELSE
    *
    *  remove the lines from laLines[] that contain the
    *  VMP IUDLog stored procedures
    *
    FOR xx = lnEndLine TO lnStartLine STEP -1
      ADEL(laLines,xx)
      DIMENSION laLines[ALEN(laLines,1)-1]
    ENDFOR
  ENDIF  &&& lnStartLine = 0
 ELSE
  *
  *  existing stored procedures are empty
  *
  lnStartLine = 1
ENDIF
ERASE oldsp.txt

IF NOT INDBC("IUDLOG","TABLE")
  *
  *  we're done -- no IUDLog table in this .DBC
  *
  WAIT WINDOW "Removing IUDLog stored procedures..." NOWAIT 
  FOR xx = 1 TO ALEN(laLines,1)
    STRTOFILE(laLines[m.xx]+CHR(13)+CHR(10),"newsp.txt",.t.)
  ENDFOR
  APPEND PROCEDURES FROM newsp.txt OVERWRITE
  CLOSE TABLES
  COMPILE DATABASE (DBC())
  PACK DATABASE 
  ERASE newsp.txt
  WAIT CLEAR 
  RETURN
ENDIF

*
*  now generate the current set of VMP IUDLog procedures
*
SET TEXTMERGE ON NOSHOW
SET TEXTMERGE TO thissp.txt
TEXT
*  !!!!!!!!!!!!!!!!!!!! BEGIN VMP IUDLog Comment !!!!!!!!!!!!!!!!!!!!
*
*  The following is VMP IUDLog code, used by
*  the IUDLog triggers added to the VMP tables in this database.
*  DO NOT REMOVE THESE PROCEDURES (unless you also remove all
*  calls to SP_VMP_IUDLog(...) in the triggers of the tables in
*  this database)
*
*  The following stored procedures have been inserted into the
*  Stored Procedures for this database by the XXFWDATA.PRG utility
*
*
****************************************************
PROCEDURE SP_VMP_IUDLog
****************************************************
*
*  _VFP.Help(FULLPATH("VMPHELP.CHM"),,"SP_VMP_IUDLog header comments")
*
LPARAMETERS tcTriggerType, ;
            PLACEHOLDER, ;
            tlIgnoreMemos, ;
            tlIgnoreBlobs, ;
            tlIgnoreVarBinarys, ;
            tcPKFieldName

*
*  check to see if the Trigger expression contains
*  more than just a call to SP_VMP_IUDLog(),
*  and if one of those other calls in the trigger
*  expression has failed because the function/UDF()
*  doesn't exist (not in the path at the moment,
*  for example), such as
*    Bogus().AND.SP_VMP_IUDLog("U")
*  otherwise, VFP fires this next SP_VMP_IUDLog()  
*  part of the trigger expression, even though the
*  trigger will ultimately fail because a prior
*  function/UDF call fails when it cannot be found
*
*  unfortunately, this check has to be the very first
*  set of code here because otherwise SYS(2018)/
*  AERROR(,3) get updated to reflect other values,
*  such as when SP_VMP_IUDLog_XLogging() queries
*  the VARTYPE() of glSuppress_IUDLog_Logging
*  
LOCAL lcSetDatabase
lcSetDatabase = UPPER(SET("DATABASE"))
IF NOT EMPTY(CURSORGETPROP("Database",ALIAS())) ;
     AND NOT JUSTSTEM(m.lcSetDatabase) == UPPER(JUSTSTEM(CURSORGETPROP("Database",ALIAS())))
  *  contained table being logged
  SET DATABASE TO (JUSTSTEM(CURSORGETPROP("Database",ALIAS())))
  IF VARTYPE(SYS(2018)) = "C" AND NOT EMPTY(SYS(2018))
    LOCAL laError[1], lcTriggerString, lnTriggerCount, ;
          lcTriggerCall, llReturnF
    AERROR(laError)
    IF VARTYPE(laError[1,3]) = "C" AND NOT EMPTY(laError[1,3]) ;
         AND UPPER(JUSTSTEM(laError[1,3])) == UPPER(JUSTSTEM(SYS(2018))) 
      lcTriggerString = DBGETPROP(JUSTSTEM(CURSORGETPROP("SourceName")), ;
                                  "TABLE", ;
                                  IIF(UPPER(LEFTC(m.tcTriggerType,1))="I","InsertTrigger",IIF(UPPER(LEFTC(m.tcTriggerType,1))="U","UpdateTrigger","DeleteTrigger")))
      lcTriggerString = UPPER(m.lcTriggerString)
      IF ".AND." $ m.lcTriggerString
        *
        *  check the other functions/UDFs in the
        *  trigger string for one that is also
        *  specified in SYS(2018)/AERROR[1,3]
        *
        lcTriggerString = STRTRAN(m.lcTriggerString,".AND.","~")
        FOR lnTriggerCount = 1 TO OCCURS("~",m.lcTriggerString)+1
          lcTriggerCall = GETWORDNUM(m.lcTriggerString,m.lnTriggerCount,"~")
          IF m.lcTriggerCall = "SP_VMP_IUDLOG("
            EXIT
          ENDIF
          lcTriggerCall = GETWORDNUM(m.lcTriggerCall,1,"(")
          IF UPPER(JUSTSTEM(SYS(2018))) = m.lcTriggerCall
            *
            *  this function call in the trigger expression
            *  has already failed because VFP can't find
            *  it, either because it doesn't exist, or,
            *  more likely, it's not in the PATH at the moment
            *
            llReturnF = .t.
            EXIT
          ENDIF          
        ENDFOR
      ENDIF   &&& ".AND." $ m.lcTriggerString
    ENDIF   &&& VARTYPE(laError[1,3]) = "C" ...
    IF m.llReturnF
      SET DATABASE TO &lcSetDatabase
      RETURN .f.
    ENDIF
    RELEASE laError, lcTriggerString, lnTriggerCount, ;
          lcTriggerCall, llReturnF
  ENDIF   &&& VARTYPE(SYS(2018)) = "C" AND NOT EMPTY(SYS(2018))
ENDIF   &&& contained table being logged

IF SP_VMP_IUDLog_XLogging()
  RETURN .t.
ENDIF

IF NOT VARTYPE(m.tcTriggerType) = "C" ;
     OR EMPTY(m.tcTriggerType) ;
     OR NOT INLIST(LEFTC(UPPER(ALLTRIM(m.tcTriggerType)),1),"I","U","D")
  *
  *  ASSERT for the development environment
  *
  ASSERT .f. MESSAGE PROGRAM() + ;
      " has not been passed the required tcTriggerType parameter as 'I'/'U'/'D'"
  RETURN .f.
ENDIF     

*********************************************************
*********************************************************
*
*  setup
*
*********************************************************
*********************************************************

LOCAL lcAlias, lcTableName, lcDatabase, lcDBC, lnSelect, ;
      lcTrigger, lcPKFieldName, xx, yy, lcIUDAlias, ;
      loException, lcField, lnFileSize, llSuccess, lnDeleted, ;
      llLogIUD_UFields, luFieldValue, luOldValue, lnRow, lnRows, ;
      llIgnoreMemos, llIgnoreVarBinarys, llIgnoreBlobs, lcSetSafety, ;
      luIUD_UsrFK, lcIUD_Usr, lcIUD_FValues, lcIUD_UFields, llUFields, ;
      laRFields[1], laJunk[1], laMFields[1], laWFields[1], laQFields[1], ;
      lcIUD_MValuesTXT, lcIUD_WValuesTXT, lcIUD_QValuesTXT, ;
      lnMRows, lnWRows, lnQRows, llCheckMemos, llCheckBlobs, llCheckVarbinarys,; 
      lcIUD_Stack, laStackInfo[1], lcString, llNoDebugInfo, lnSize

llIgnoreMemos = (VARTYPE(m.tlIgnoreMemos)="L" AND m.tlIgnoreMemos)
llIgnoreVarBinarys = (VARTYPE(m.tlIgnoreVarBinarys)="L" AND m.tlIgnoreVarBinarys)
llIgnoreBlobs = (VARTYPE(m.tlIgnoreBlobs)="L" AND m.tlIgnoreBlobs)
llSuccess = .t.
lcAlias = ALIAS()    &&& alias of the table whose trigger has just fired
lcDBC = UPPER(CURSORGETPROP("Database",m.lcAlias))
lcDatabase = UPPER(JUSTSTEM(m.lcDBC))
lcTableName = UPPER(JUSTSTEM(CURSORGETPROP("SourceName",m.lcAlias)))
lnSelect = SELECT(0)
lcTrigger = LEFTC(UPPER(ALLTRIM(m.tcTriggerType)),1)
lcSetSafety = SET("SAFETY")

*
*  SET DATABASE TO the database containing the table (lcAlias)
*  whose trigger just fired -- when you call this procedure
*  manually, typically to log a free table action, there is
*  no containing database
*
IF EMPTY(m.lcDatabase)
  SET DATABASE TO 
 ELSE
  SET DATABASE TO (m.lcDatabase)
ENDIF

*********************************************************
*********************************************************
*  get the name of the PK field
*********************************************************
*********************************************************

DO CASE
  ********************************************
  CASE VARTYPE(m.tcPKFieldName) = "C"
  ********************************************
    *
    *  tcPKFieldName was passed explicitly
    *
    lcPKFieldName = UPPER(ALLTRIM(m.tcPKFieldName))
  ********************************************
  CASE NOT EMPTY(SET("DATABASE")) AND INDBC(m.lcTableName,"TABLE")
  ********************************************
    lcPKFieldName = DBGETPROP(m.lcTableName,"TABLE","PrimaryKey")
ENDCASE
IF EMPTY(m.lcPKFieldName)
  SET DATABASE TO &lcSetDatabase
  SELECT (m.lnSelect)
  *
  *  if there's no PK, and you haven't passed tcPKFieldName,
  *  there's nothing we can do, so we won't RETURN .f.
  *  or generate any kind of error -- you really shouldn't
  *  have added the SP_VMP_IUDLog(...) trigger to m.lcTableName
  *  (or, if there's a field which is a candidate key, you
  *  should have passed the name of that field as the
  *  2nd tcPKFieldName parameter)
  *
  RETURN .t.
ENDIF

*********************************************************
*********************************************************
*
*  make sure the IUDLog table is open -- in order to
*  use the free table support when passing tcPKFieldName,
*  the IUDLog table *MUST* ALREADY be open, with the
*  alias "IUDLog", and the free table MUST be the
*  current work area
*
*********************************************************
*********************************************************
IF NOT SP_VMP_IUDLog_SetIUDAlias(@m.lcIUDAlias, ;
                                 m.lcDatabase, ;
                                 m.lcDBC, ;
                                 m.lcTableName)
  SET DATABASE TO &lcSetDatabase
  SELECT (m.lnSelect)
  RETURN .f.
ENDIF

*********************************************************
*********************************************************
*
*  get Usr_PK or whatever User information we can,
*  for populating to the IUD_UsrFK field -- the
*  first of these we can find:
*    - the Users.Usr_PK 
*    - the Windows WNetGetUser() API function UserID 
*    - the SYS(0) User ID 
*
*********************************************************
*********************************************************

IF NOT SP_VMP_IUDLog_SetUserInfo(@m.luIUD_UsrFK, ;
                                 @m.lcIUD_Usr, ;
                                 m.lcIUDAlias)
  SET DATABASE TO &lcSetDatabase
  SELECT (m.lnSelect)
  RETURN .f.
ENDIF

*********************************************************
*********************************************************
*
*  insert the IUDLog record
*
*********************************************************
*********************************************************

IF FILE("SP_VMP_IUDLog_SuppressDebug.TXT") ;
     OR FILE("IUD_NoDe.TXT")
  *
  *  if a semaphore file named either using the
  *  long file name or the 8.3 file name exist,
  *  that indicates that we will save time/
  *  file size by NOT populating the IUDLog
  *  memo fields that are primarily used for 
  *  debugging purposes, including:
  *    IUD_Stack
  *    IUD_Exception
  *
  llNoDebugInfo = .t.
ENDIF

*
*  set the field values we know right now -- all the
*  non "before/after" fields
*
SELECT (m.lcIUDAlias)
SCATTER MEMVAR MEMO BLANK
*  these will be handled by REPLACEs, not the upcoming INSERT:
RELEASE IUD_FValues, IUD_MValues, IUD_WValues, IUD_QValues, IUD_UFields, IUD_Stack, IUD_Exception
IF NOT ISNULL(m.luIUD_UsrFK)
  IUD_UsrFK = m.luIUD_UsrFK
ENDIF
IF NOT ISNULL(m.lcIUD_Usr)
  IUD_Usr = m.lcIUD_Usr
ENDIF
IUD_Action = m.lcTrigger
IUD_TableName = m.lcTableName
DO CASE
  CASE NOT TYPE(m.lcIUDAlias+".IUD_TableDBC") = "C"
    * nothing to do
  CASE LENC(EVALUATE(m.lcIUDAlias+".IUD_TableDBC")) < 21
    IUD_TableDBC = JUSTSTEM(m.lcDBC)
  OTHERWISE
    IUD_TableDBC = DISPLAYPATH(m.lcDBC,LENC(EVALUATE(m.lcIUDAlias+".IUD_TableDBC")))
ENDCASE    
IF TYPE(m.lcIUDAlias+".IUD_TableDBF") = "C"
  IUD_TableDBF = DISPLAYPATH(DBF(m.lcAlias),LENC(EVALUATE(m.lcIUDAlias+".IUD_TableDBF")))
ENDIF
IUD_PKFieldName = m.lcPKFieldName
SELECT (m.lcAlias)
IUD_PKFieldType = VARTYPE(EVALUATE(m.lcPKFieldName))
IUD_PKFieldValue = TRANSFORM(EVALUATE(m.lcPKFieldName))
SELECT (m.lcIUDAlias)
IUD_DateTime = DATETIME()
IF INLIST(VARTYPE(m.gcIUD_TransID),"C","X")
  *  XXDTES("XXFWNTBO.VCX","PUBLIC gcIUD_TransID","ctrBusinessObjectManager","SaveAction")
  *  XXDTES("XXFW.VCX","PUBLIC gcIUD_TransID","frmData","SaveAction")
  IUD_TransID = m.gcIUD_TransID
 ELSE
  IUD_TransID = SPACE(16)    
ENDIF
IUD_TXNLevel = TXNLEVEL()
IUD_SYS0 = TRANSFORM(SYS(0))
IF TYPE(m.lcIUDAlias+".IUD_Stack") = "U"
  lcIUD_Stack = .NULL.
 ELSE
  lcIUD_Stack = SPACE(0)
ENDIF
IUD_DS = SET("DATASESSION")
IF EMPTY(m.lcDatabase)
  *  free table, this code cannot be an actual trigger, but called manually
  IUD_TL = _TRIGGERLEVEL
 ELSE
  IUD_TL = MAX(_TRIGGERLEVEL-1,0)   &&& subtract 1 from _TRIGGERLEVEL to indicate the trigger level of the logged table I/U/D, NOT the current _TRIGGERLEVEL, which, by definition, is always > 0
ENDIF

*
*  enable the INSERT and UPDATEs we need to do here
*
IF NOT SP_VMP_IUDLog_ERW()
  SET DATABASE TO &lcSetDatabase
  SELECT (m.lnSelect)
  RETURN .f.
ENDIF

*
*  INSERT the record  
*
TRY
INSERT INTO (m.lcIUDAlias) FROM MEMVAR 
CATCH TO loException
ENDTRY
IF VARTYPE(m.loException) = "O"
  SP_VMP_IUDLog_DRW()   
  *
  *  ASSERT for the development environment
  *
  ASSERT .f. MESSAGE PROGRAM() + ;
      " failed, loException.Message:" + ;
      CHR(13) + CHR(10) + ;
      loException.Message
  SET DATABASE TO &lcSetDatabase
  SELECT (m.lnSelect)
  RETURN .f.
ENDIF 

*********************************************************
*********************************************************
*
*  fill in the field details 
*
*********************************************************
*********************************************************

lcIUD_MValuesTXT = ADDBS(SYS(2023)) + "IUD_MValues.TXT"
lcIUD_WValuesTXT = ADDBS(SYS(2023)) + "IUD_WValues.TXT"
lcIUD_QValuesTXT = ADDBS(SYS(2023)) + "IUD_QValues.TXT"
IF m.lcSetSafety = "ON"
  *
  *  if/when the CURSORTOXML() calls generate XML to a file,
  *  the SET SAFETY setting is respected
  *
  SET SAFETY OFF 
ENDIF

TRY

*
*  create an AFIELDS() array with just the first 6 columns
*
SELECT (m.lcAlias)
LOCAL laJunk[1]
AFIELDS(laJunk,m.lcAlias)
DIMENSION laRFields[ALEN(laJunk,1),6]
lnRows = ALEN(laJunk,1)
FOR xx = 1 TO m.lnRows
  FOR yy = 1 TO 6
    laRFields[m.xx,m.yy] = laJunk[m.xx,m.yy]
  ENDFOR  
ENDFOR
RELEASE laJunk

*
*  remove "non-regular" fields from laRFields, 
*  and create laMFields/laWFields/laQFields
*
lnDeleted = 0
lnRows = ALEN(laRFields,1)
STORE 0 TO lnMRows, lnWRows, lnQRows
llCheckMemos = (NOT m.llIgnoreMemos AND NOT TYPE(m.lcIUDAlias+".IUD_MValues") = "U")
llCheckBlobs = (NOT m.llIgnoreBlobs AND NOT TYPE(m.lcIUDAlias+".IUD_WValues") = "U")
llCheckVarbinarys = (NOT m.llIgnoreVarbinarys AND NOT TYPE(m.lcIUDAlias+".IUD_QValues") = "U")
FOR xx = m.lnRows TO 1 STEP -1
  DO CASE
    *************************************************
    CASE NOT VARTYPE(laRFields[m.xx,1]) = "C"
    *************************************************
      *  ADEL()ed row
    *************************************************
    CASE NOT INLIST(laRFields[m.xx,2],"G","M","W","Q")
    *************************************************
      LOOP 
    *************************************************
    CASE laRFields[m.xx,2] = "G"
    *************************************************
      ADEL(laRFields,m.xx)
      lnDeleted = m.lnDeleted + 1
    *************************************************
    CASE laRFields[m.xx,2] = "M"
    *************************************************
      IF m.llCheckMemos
        *
        *  copy this laRFields row to a new laMFields row
        * 
        lnMRows = m.lnMRows + 1
        DIMENSION laMFields[m.lnMRows,6]
        FOR yy = 1 TO 6
          laMFields[m.lnMRows,m.yy] = laRFields[m.xx,m.yy]
        ENDFOR
      ENDIF   
      *
      *  delete this row from laRFields
      *
      ADEL(laRFields,m.xx)
      lnDeleted = m.lnDeleted + 1
    *************************************************
    CASE laRFields[m.xx,2] = "W"
    *************************************************
      IF m.llCheckBlobs
        *
        *  copy this laRFields row to a new laWFields row
        * 
        lnWRows = m.lnWRows + 1
        DIMENSION laWFields[m.lnWRows,6]
        FOR yy = 1 TO 6 
          laWFields[m.lnWRows,m.yy] = laRFields[m.xx,m.yy]
        ENDFOR
      ENDIF    
      *
      *  delete this row from laRFields
      *
      ADEL(laRFields,m.xx)
      lnDeleted = m.lnDeleted + 1
    *************************************************
    CASE laRFields[m.xx,2] = "Q"
    *************************************************
      IF m.llCheckVarbinarys
        *
        *  copy this laRFields row to a new laQFields row
        * 
        lnQRows = m.lnQRows + 1
        DIMENSION laQFields[m.lnQRows,6]
        FOR yy = 1 TO 6 
          laQFields[m.lnQRows,m.yy] = laRFields[m.xx,m.yy]
        ENDFOR
      ENDIF    
      *
      *  delete this row from laRFields
      *
      ADEL(laRFields,m.xx)
      lnDeleted = m.lnDeleted + 1
  ENDCASE
ENDFOR
DO CASE
  CASE m.lnDeleted = 0
    *  nothing to do
  CASE m.lnDeleted < ALEN(laRFields,1)
    *
    *  remove the ADEL()ed rows
    *
    DIMENSION laRFields[ALEN(laRFields,1)-m.lnDeleted,ALEN(laRFields,2)]
  OTHERWISE
    *
    *  the table being logged contains only non-"regular" fields
    *
    RELEASE laRFields
ENDCASE

IF m.lnMRows = 0
  RELEASE laMFields
ENDIF
IF m.lnWRows = 0
  RELEASE laWFields
ENDIF
IF m.lnQRows = 0
  RELEASE laQFields
ENDIF

lcIUD_UFields = SPACE(0)
IF m.lcTrigger = "U" ;
      AND CURSORGETPROP("Buffering",m.lcAlias) > 1 ;
      AND TYPE(m.lcIUDAlias+".IUD_UFields") = "M" ;
      AND NOT TYPE("laRFields[1]") = "U"
  *
  *  store to lcIUD_UFields the Names of all "regular" 
  *  fields that have been updated 
  *     
  #DEFINE C_CRLF CHR(13)+CHR(10)
  SELECT (m.lcAlias)
  FOR xx = 1 TO FCOUNT()
    lcField = FIELD(m.xx)
    IF ASCAN(laRFields,m.lcField,1,-1,1,15)=0
      LOOP 
    ENDIF
    luFieldValue = EVALUATE(m.lcField)
    luOldVal = OLDVAL(m.lcField)
    IF (ISNULL(m.luOldVal) AND ISNULL(m.luFieldValue)) ;
         OR ;
        (VARTYPE(m.luOldVal) = VARTYPE(m.luFieldValue) ;
         AND ;
         m.luOldVal == m.luFieldValue)
      *
      *  no change
      *
     ELSE
      lcIUD_UFields = m.lcIUD_UFields + "," + m.lcField + "," + C_CRLF
    ENDIF
  ENDFOR
  #UNDEF C_CRLF CHR(13)+CHR(10)
  llUFields = NOT EMPTY(m.lcIUD_UFields)
ENDIF    &&& log Names of changed fields on buffered Updates to IUD_UFields

*
*  log "regular" fields (if any) to IUD_FValues
*
lcIUD_FValues = SPACE(0)
IF TYPE("laRFields[1]") = "U"
  *
  *  there are no "regular" fields -- all fields
  *  in the table being logged are General/Memo/Blob/Varbinary
  *
 ELSE
  *
  *  log the "regular fields
  *
  *  copy the regular fields from m.lcAlias to a temp cursor
  *
  SELECT (m.lcAlias)
  SCATTER MEMVAR 
  CREATE CURSOR C_IUD_FValues FROM ARRAY laRFields
  SELECT C_IUD_FValues
  APPEND BLANK 
  GATHER MEMVAR   
  *
  *  create XML from the temp cursor
  *
  CURSORTOXML("C_IUD_FValues","lcIUD_FValues",1,0+2+8+16,0,"")
  *
  *  cleanup
  *
  USE IN SELECT("C_IUD_FValues")
ENDIF

*
*  log Memo fields (if any) to IUD_MValues
*
IF m.lnMRows > 0 
  *
  *  log the Memo fields
  *
  *
  *  copy the Memo fields from m.lcAlias to a temp cursor
  *
  ASORT(laMFields,1,-1,0)
  CREATE CURSOR C_IUD_MValues FROM ARRAY laMFields
  SELECT C_IUD_MValues
  APPEND BLANK 
  SELECT (m.lcAlias)
  lnSize = 0
  FOR xx = 1 TO ALEN(laMFields,1)
    lcField = laMFields[m.xx,1]
    replace (m.lcField) WITH EVALUATE(m.lcAlias+"."+m.lcField) IN C_IUD_MValues
    lnSize = m.lnSize + LENC(EVALUATE("C_IUD_MValues."+m.lcField))
  ENDFOR
  *
  *  create temp XML file from the temp cursor
  *
  IF m.lnSize <= 250000
    *
    *  the total size of the Memos is <= 250K, so
    *  store it to memory rather than a file 
    *  ...faster...
    *
    LOCAL lcIUD_MValues   
    CURSORTOXML("C_IUD_MValues","lcIUD_MValues",1,2+8+16,0,"")
   ELSE
    *
    *  the total size of the Memos is > 250K, so
    *  store it to a file rather than to memory
    *  ...slower, but no memory problems...
    *
    CURSORTOXML("C_IUD_MValues",m.lcIUD_MValuesTXT,1,2+8+16+512,0,"")
  ENDIF   
  USE IN SELECT("C_IUD_MValues")
ENDIF

*
*  log Blob fields (if any) to IUD_MValues
*
IF m.lnWRows > 0
  *
  *  log the Blob fields
  *
  *  copy the Blob fields from m.lcAlias to a temp cursor
  *
  ASORT(laWFields,1,-1,0)
  CREATE CURSOR C_IUD_WValues FROM ARRAY laWFields
  SELECT C_IUD_WValues
  APPEND BLANK 
  SELECT (m.lcAlias)
  lnSize = 0
  FOR xx = 1 TO ALEN(laWFields,1)
    lcField = laWFields[m.xx,1]
    replace (m.lcField) WITH EVALUATE(m.lcAlias+"."+m.lcField) IN C_IUD_WValues
    lnSize = m.lnSize + LENC(EVALUATE("C_IUD_WValues."+m.lcField))
  ENDFOR
  *
  *  create temp XML file from the temp cursor
  *
  IF m.lnSize <= 250000
    *
    *  the total size of the Blobs is <= 250K, so
    *  store it to memory rather than a file 
    *  ...faster...
    *
    LOCAL lcIUD_WValues   
    CURSORTOXML("C_IUD_WValues","lcIUD_WValues",1,2+8+16,0,"")
   ELSE
    *
    *  the total size of the Blobs is > 250K, so
    *  store it to a file rather than to memory
    *  ...slower, but no memory problems...
    *
    CURSORTOXML("C_IUD_WValues",m.lcIUD_WValuesTXT,1,2+8+16+512,0,"")
  ENDIF   
  USE IN SELECT("C_IUD_WValues")
ENDIF

*
*  log Varbinary fields (if any) to IUD_QValues
*
IF m.lnQRows > 0
  *
  *  log the Varbinary fields
  *
  *  copy the Varbinary fields from m.lcAlias to a temp cursor
  *
  ASORT(laQFields,1,-1,0)
  CREATE CURSOR C_IUD_QValues FROM ARRAY laQFields
  SELECT C_IUD_QValues
  APPEND BLANK 
  SELECT (m.lcAlias)
  lnSize = 0
  FOR xx = 1 TO ALEN(laQFields,1)
    lcField = laQFields[m.xx,1]
    replace (m.lcField) WITH EVALUATE(m.lcAlias+"."+m.lcField) IN C_IUD_QValues
    lnSize = m.lnSize + LENC(EVALUATE("C_IUD_QValues."+m.lcField))
  ENDFOR
  *
  *  create temp XML file from the temp cursor
  *
  *  DEVNOTE
  *  the following code needs modification to properly
  *  preserve the Varbinary value(s) -- one possible remedy
  *  is to use XMLAdapter for just the Varbinary fields,
  *  including the schema and proper Varbinary mapping
  *     XXDTES("XXFWIUDLOG.VCX","preserve the Varbinary value(s)","bo_iudlog","POPULATERESULTCURSOR")
  *
  IF m.lnSize <= 250000
    *
    *  the total size of the Varbinary is <= 250K, so
    *  store it to memory rather than to memory
    *  ...faster...
    *
    LOCAL lcIUD_QValues   
    CURSORTOXML("C_IUD_QValues","lcIUD_QValues",1,2+8+16,0,"")
   ELSE
    *
    *  the total size of the Varbinarys is > 250K, so
    *  store it to a file rather than to memory
    *  ...slower, but no memory problems...
    *
    CURSORTOXML("C_IUD_QValues",m.lcIUD_QValuesTXT,1,2+8+16+512,0,"")
  ENDIF   
  USE IN SELECT("C_IUD_QValues")
 ELSE
  *
  *  there are no Varbinary fields to log
  *
ENDIF

*
*  store ASTACKINFO() info, if 
*  1- the IUD_Stack field exists
*  2- no semaphore file named either IUD_NoDe.TXT
*     or SP_VMP_IUDLog_SuppressDebug.TXT exists
*     in the path at the moment
*
IF NOT ISNULL(m.lcIUD_Stack) AND NOT m.llNoDebugInfo
  DO CASE
    CASE ASTACKINFO(laStackInfo) = 0
      lcIUD_Stack = "ASTACKINFO() = 0, no code being executed (???)"
    CASE ASTACKINFO(laStackInfo) = 1
      lcIUD_Stack = "ASTACKINFO() = 1, only SP_VMP_IUDLog being executed, such as a plain BROWSE update or direct APPEND/REPLACE/DELETE at the Command Window"
    OTHERWISE
      lcIUD_Stack = SPACE(0)
      #DEFINE C_CRLF CHR(13)+CHR(10)
      FOR xx = 1 TO ALEN(laStackInfo,1)-1   &&& the last one is always the SP_VMP_IUDLog() entry - ignore it
        lcString = SPACE(0)
        *  format the output
        FOR yy = 1 TO ALEN(laStackInfo,2)
          lcString = ALLTRIM(TRANSFORM(laStackInfo[m.xx,m.yy]))
          IF m.yy = 4 AND JUSTSTEM(UPPER(m.lcString)) == JUSTSTEM(UPPER(laStackInfo[m.xx,2]))
            LOOP 
          ENDIF
          IF m.yy = 1
            lcString = m.lcString + SPACE(1)
          ENDIF
          IF m.yy = 2 AND JUSTSTEM(UPPER(m.lcString)) == JUSTSTEM(UPPER(laStackInfo[m.xx,4]))
            lcString = ALLTRIM(TRANSFORM(laStackInfo[m.xx,4]))
          ENDIF
          IF m.yy = 5
            lcString = "Line " + m.lcString + ":  "  
          ENDIF
          IF INLIST(m.yy,3,4,5)
            lcString = SPACE(4) + m.lcString
          ENDIF
          IF INLIST(m.yy,2,3,4)
            lcString = m.lcString + C_CRLF
          ENDIF
          lcIUD_Stack = m.lcIUD_Stack + m.lcString
        ENDFOR
        lcIUD_Stack = m.lcIUD_Stack + C_CRLF
        TRY
        lcString = UPPER(JUSTFNAME(laStackInfo[m.xx,2]))
        DO CASE
          CASE JUSTEXT(m.lcString) = "VCT"
            lcString = FORCEEXT(m.lcString,"VCX")
          CASE JUSTEXT(m.lcString) = "SCT"
            lcString = FORCEEXT(m.lcString,"SCT")
          CASE JUSTEXT(m.lcString) = "FXP"
            lcString = FORCEEXT(m.lcString,"PRG")
          CASE JUSTEXT(m.lcString) = "MPX"
            lcString = FORCEEXT(m.lcString,"MPR")
          CASE JUSTEXT(m.lcString) = "QPX"
            lcString = FORCEEXT(m.lcString,"QPR")
          CASE JUSTEXT(m.lcString) = "DCT"
            lcString = FORCEEXT(m.lcString,"DCX")
        ENDCASE
        IF INLIST(JUSTEXT(m.lcString),"VCX","SCX")
          lcString = [EDITSOURCE("] + ;
                     m.lcString + [",] + TRANSFORM(laStackInfo[m.xx,5]) + ;
                     [,"] + GETWORDNUM(laStackInfo[m.xx,3],1,".") + ;
                     [","] + SUBSTRC(laStackInfo[m.xx,3],AT_C(".",laStackInfo[m.xx,3])+1) + ;
                     [")]
         ELSE
          lcString = [EDITSOURCE("] + m.lcString + [",] + TRANSFORM(laStackInfo[m.xx,5]) + ")"
        ENDIF
        CATCH
        lcString = "(no EDITSOURCE() was generated)"
        ENDTRY
        IF NOT EMPTY(m.lcString)
          lcIUD_Stack = m.lcIUD_Stack + m.lcString + C_CRLF
        ENDIF
        lcIUD_Stack = m.lcIUD_Stack + C_CRLF
      ENDFOR
      #UNDEF C_CRLF CHR(13)+CHR(10)
  ENDCASE
ENDIF

*
*  all the log information has been generated, update
*  the IUDLog Memo fields in a single REPLACE statement,
*  so as to not bloat the IUDLog.FPT by doing multiple
*  REPLACEs
*
SELECT (m.lcIUDAlias)
lcReplaceStatement = "REPLACE IUD_FValues WITH m.lcIUD_FValues"
IF m.llUFields
  lcReplaceStatement = m.lcReplaceStatement + ", IUD_UFields WITH m.lcIUD_UFields"
ENDIF
IF m.lnMRows > 0
  IF TYPE("m.lcIUD_MValues") = "C"
    lcReplaceStatement = m.lcReplaceStatement + ", IUD_MValues WITH m.lcIUD_MValues"
   ELSE
    lcReplaceStatement = m.lcReplaceStatement + ", IUD_MValues WITH FILETOSTR(m.lcIUD_MValuesTXT)"
  ENDIF
ENDIF
IF m.lnWRows > 0
  IF TYPE("m.lcIUD_WValues") = "C"
    lcReplaceStatement = m.lcReplaceStatement + ", IUD_WValues WITH m.lcIUD_WValues"
   ELSE
    lcReplaceStatement = m.lcReplaceStatement + ", IUD_WValues WITH FILETOSTR(m.lcIUD_WValuesTXT)"
  ENDIF
ENDIF
IF m.lnQRows > 0
  IF TYPE("m.lcIUD_QValues") = "C"
    lcReplaceStatement = m.lcReplaceStatement + ", IUD_QValues WITH m.lcIUD_QValues"
   ELSE
    lcReplaceStatement = m.lcReplaceStatement + ", IUD_QValues WITH FILETOSTR(m.lcIUD_QValuesTXT)"
  ENDIF
ENDIF
IF VARTYPE(m.lcIUD_Stack) = "C" AND NOT EMPTY(m.lcIUD_Stack) 
  lcReplaceStatement = m.lcReplaceStatement + ", IUD_Stack WITH m.lcIUD_Stack"
ENDIF
lcReplaceStatement = m.lcReplaceStatement + " IN " + m.lcIUDAlias
&lcReplaceStatement

IF m.lcSetSafety = "ON"
  SET SAFETY ON
ENDIF

CATCH TO loException
ENDTRY

IF VARTYPE(m.loException) = "O" ;
     AND TYPE(m.lcIUDAlias+".IUD_Exception") = "M" ;
     AND NOT m.llNoDebugInfo
  SELECT (m.lcIUDAlias)
  TRY
  replace IUD_Exception WITH IUD_Exception + ;
          "Unable to log one or more values because" + ;
          CHR(13) + CHR(10) + ;
          TRANSFORM(loException.ErrorNo) + ": " + loException.Message + ;
          CHR(13) + CHR(10) + ;
          CHR(13) + CHR(10) 
  CATCH
  llSuccess = .f.
  ENDTRY    
ENDIF

*********************************************************
*********************************************************
*
*  check how large IUDLog.DBF and .FPT are getting
*
*********************************************************
*********************************************************

IF m.llSuccess
  *
  *  perform the check every one hundred thousand records 
  *
  IF MOD(RECCOUNT(m.lcIUDAlias),100000) = 0 
    *
    *  get the size of whichever is larger
    *
    lnFileSize = SP_VMP_IUDLog_GetFileSize(m.lcIUDAlias)
    *
    *  take whatever action you deem appropriate
    *
    SP_VMP_IUDLog_ActionOnCheckFileSize(m.lnFileSize,m.lcIUDAlias)
  ENDIF
ENDIF

*********************************************************
*********************************************************
*
*  cleanup
*
*  for performance reasons, m.lcIUDAlias (IUDLog table) 
*  is left open for the next usage
*
*  same for IUDLogTrigger_Users
*
*********************************************************
*********************************************************

*
*  reverse the previous SP_VMP_IUDLog_ERW() call
*
SP_VMP_IUDLog_DRW()

SET DATABASE TO &lcSetDatabase
SELECT (m.lnSelect)

RETURN m.llSuccess




***********************************************************
PROCEDURE SP_VMP_IUDLog_SetIUDAlias
***********************************************************
*
*  populates the value of tcIUDAlias, which is passed
*  here BY REFERENCE
*
*  RETURNs .f. if something goes wrong
*
*  if the table being logged is a contained table,
*  the current SET DATABASE must be the database
*  containing that table
*
*  if the table being logged is a free table, the
*  current SET DATABASE must be the database 
*  that contains the IUDLog/IUDLog_<TableName> 
*  table, -AND- the desired IUDLog/IUDLog_<TableName>
*  MUST be USED(), with either the alias "IUDLog"
*  or "IUDLog_<TableName>"
*
LPARAMETERS tcIUDAlias, ;     &&& lcIUDAlias passed here BY REFERENCE
            tcDatabase, ;     &&& just the Database Name
            tcDBC, ;          &&& FULLPATH() of the .DBC
            tcTableName       &&& contained TableName/free FileName without .DBF

LOCAL llOK, lcIUDAlias, loException, lcTable

IF EMPTY(m.tcDatabase)
  *
  *  free table action being logged
  *
  DO CASE
    ***************************************************
    CASE USED("IUDLog_"+m.tcTableName)
    ***************************************************
      *
      *  dedicated IUDLog table for this free table
      *
      llOK = .t.
      tcIUDAlias = "IUDLog_"+m.tcTableName
    ***************************************************
    CASE USED("IUDLog")
    ***************************************************
      *
      *  single IUDLog table for all logging
      *
      llOK = .t.
      tcIUDAlias = "IUDLog"
    ***************************************************
    OTHERWISE
    ***************************************************
      llOK = .f.
      ASSERT .f. MESSAGE PROGRAM() + ;
           " has been called to log a free table action, but " + ;
           "there is no open ALIAS() of 'IUDLog' or 'IUDLog_" + m.tcTableName + "'" 
  ENDCASE

  RETURN m.llOK
ENDIF

*
*  contained table action being logged
*
DO CASE
  ***************************************************
  CASE INDBC("IUDLog_"+m.tcTableName,"TABLE")
  ***************************************************
    *  
    *  dedicated IUDLog for this contained table
    *
    lcIUDAlias = "IUDLog_" + m.tcDatabase + "_" + m.tcTableName + "_Triggers"
    lcTable = "IUDLog_" + m.tcTableName
  ***************************************************
  CASE INDBC("IUDLog","TABLE")
  ***************************************************
    *
    *  log this contained table to the single/default IUDLog table
    *
    lcIUDAlias = "IUDLog_" + m.tcDatabase + "_Triggers"
    lcTable = "IUDLog"
  ***************************************************
  OTHERWISE
  ***************************************************
    ASSERT .f. message PROGRAM() + ;
         " is unable to find an IUDLog table in the " + m.lcDatabase + " database"
ENDCASE
IF USED(m.lcIUDAlias) 
  IF UPPER(CURSORGETPROP("Database",m.lcIUDAlias)) == m.tcDBC
    *
    *  good, this is what we expect once this SP has
    *  been called the first time
    *
    IF NOT CURSORGETPROP("Buffering",m.lcIUDAlias) = 1
      CURSORSETPROP("Buffering",1,m.lcIUDAlias)
    ENDIF
    tcIUDAlias = m.lcIUDAlias
    RETURN .t.
   ELSE
    *
    *  lcIUDAlias is USED(), but it's not contained
    *  in the same database as the table being
    *  logged -- this shouldn't really ever happen, 
    *  even in multiple-database scenarios, because of 
    *  the unique database-specific ALIAS() we're
    *  using, but this code is for Murphy's Law
    *   
    USE IN SELECT(m.lcIUDAlias)
  ENDIF
ENDIF   &&& USED(m.lcIUDAlias) 
IF NOT USED(m.lcIUDAlias)
  *
  *  open the IUDLog table in the current database,
  *  with this explicit alias, this is the code
  *  that should execute the first time SP_VMP_IUDLog()
  *  is called in the current data session
  *
  *  we already know that either IUDLog or IUDLog_TableName
  *  is contained in the current database, or we
  *  wouldn't have gotten this far
  *
  TRY
  USE (m.lcTable) AGAIN IN 0 ALIAS (m.lcIUDAlias) SHARED 
  CATCH TO loException
  ENDTRY
  IF VARTYPE(m.loException) = "O"
    ASSERT .f. message PROGRAM() + ;
         " is unable to open the " + m.lcTable + " table in " + m.lcDatabase + ".DBC because" + ;
         CHR(13) + CHR(10) + ;
         loException.Message
    RETURN .f.
  ENDIF 
ENDIF   &&& NOT USED(m.lcIUDAlias)
tcIUDAlias = m.lcIUDAlias
RETURN .t.




***********************************************************
PROCEDURE SP_VMP_IUDLog_SetUserInfo
***********************************************************
LPARAMETERS tuIUD_UsrFK, ;
            tcIUD_Usr, ;
            tcIUDAlias
*
*  store luIUD_UsrFK and lcIUD_Usr values, passed
*  here BY REFERENCE:
*    1- oUser.GetPProp("icUsr_PK")
*    2- WNetGetUser()
*    3- SYS(0)
*
*  RETURN .f. if something goes wrong
*

*
*  if we're running in a VMP application and oUser
*  is available, this is a no-brainer
*
IF VARTYPE(m.oUser) = "O" AND PEMSTATUS(m.oUser,"GetPProp",5) 
  tuIUD_UsrFK = oUser.GetPProp("icUsr_PK")   &&& can be .NULL.
  tcIUD_Usr = .NULL.
  RETURN .t.
ENDIF  

*
*  not running in a VMP application, or in some
*  other way oUser is not available, so we don't
*  know the Users.Usr_PK
*
*  try the Windows API function WNetGetUser() to get
*  the UserID
*
LOCAL luIUD_UsrFK, lcIUD_Usr, laDLLs[1], lnRow, lcLocalName, ;
      lcUserName, lnUserNameLen, lnResult, lcUserID
luIUD_UsrFK = .NULL.
lcIUD_Usr = .NULL.
lcUserName = SPACE(40)
lnUserNameLen = LENC(m.lcUserName)
lcLocalName = SPACE(0)
*
*  DECLARE WNetGetUser() only if it hasn't already
*  been declared, for performance -- this manual
*  ADLLS() call is SLIGHTLY faster than calling 
*  X7IsAPIF()
*
lnRow = 0
IF ADLLS(m.laDLLs) > 0
  lnRow = ASCAN(laDLLs,"WNetGetUser",1,-1,2,15)
ENDIF
IF m.lnRow = 0
  DECLARE Integer WNetGetUser IN WIN32API ;
          String @lcLocalName, ;
          String @lcUserName, ;
          Integer @lnUserNameLen
ENDIF
lnResult = WNetGetUser(@m.lcLocalName,@m.lcUserName,@m.lnUserNameLen)
IF m.lnUserNameLen <= 0 OR EMPTY(m.lcUserName)
  lcUserID = SPACE(0)
 ELSE
  lcUserID = ALLTRIM(LEFTC(m.lcUserName,m.lnUserNameLen-1))
  IF RIGHTC(m.lcUserID,1) = CHR(0)
    lcUserID = LEFTC(m.lcUserID,LENC(m.lcUserID)- 1)
  ENDIF
ENDIF
IF EMPTY(m.lcUserID)
  *  WNetGetUser() failed, so try SYS(0)
  lcUserID = SYS(0)
  IF OCCURS("#",m.lcUserID) = 1 
    lcUserID = ALLTRIM(GETWORDNUM(m.lcUserID,2,"#"))  &&& could be blank
  ENDIF
ENDIF
IF NOT EMPTY(m.lcUserID)
  *  see if we can find this UserID in the Users table
  *  (in the same .DBC as the IUDLog table), as a 
  *  Users.Usr_LanID value
  IF EMPTY(SET("DATABASE"))
    *  logging a free table action
    SET DATABASE TO (CURSORGETPROP("Database",m.tcIUDAlias))
  ENDIF
  IF USED("IUDLogTrigger_Users")
    IF UPPER(CURSORGETPROP("Database","IUDLogTrigger_Users")) == UPPER(CURSORGETPROP("Database",m.tcIUDAlias))
      *  IUDLogTrigger_Users is in the same .DBC as the IUDLog
      *  for this log action
     ELSE
      *
      *  IUDLogTrigger_Users is NOT in the same .DBC as the
      *  IUDLog for this log action
      *
      USE IN IUDLogTrigger_Users
    ENDIF
  ENDIF
  IF NOT USED("IUDLogTrigger_Users") AND INDBC("Users","TABLE")
    TRY
    USE Users AGAIN IN 0 ALIAS IUDLogTrigger_Users
    CATCH
    ENDTRY
  ENDIF  
  IF USED("IUDLogTrigger_Users")
    SELECT IUDLogTrigger_Users
    LOCATE FOR UPPER(ALLTRIM(Usr_LanID)) == UPPER(ALLTRIM(m.lcUserID))
    IF FOUND()
      *  found the PK
      luIUD_UsrFK = Usr_PK
    ENDIF
  ENDIF
ENDIF
DO CASE
  CASE NOT ISNULL(m.luIUD_UsrFK)
    *  good!
  CASE NOT EMPTY(m.lcUserID)
    *  WNetGetUser() or SYS(0) 
    lcIUD_Usr = m.lcUserID
  OTHERWISE
    *  we couldn't find anything
    luIUD_UsrFK = .NULL.
    lcIUD_Usr = .NULL.        
ENDCASE   
tuIUD_UsrFK = m.luIUD_UsrFK
tcIUD_Usr = m.lcIUD_Usr
*
*  for performance reasons, IUDLogTrigger_Users is
*  left open
*
RETURN .t.



***********************************************************
PROCEDURE SP_VMP_IUDLog_GetFileSize
***********************************************************
*
*  RETURNs an Integer indicating the File Size of
*  IUDLog.DBF or IUDLog.FPT, whichever is larger
*
*  tcIUDAlias is REQUIRED, and is expected to be USED()
*
LPARAMETERS tcIUDAlias

LOCAL lnDBF, lnFPT

*
*  this test for the approximate file size of
*  IUDLog.DBF is very fast
*
lnDBF = RECCOUNT(m.tcIUDAlias)*RECSIZE(m.tcIUDAlias)

*
*  this test for the approximate file size of
*  IUDLog.FPT is relatively fast, although not as
*  fast as the lnDBF population above, but, by
*  passing the fully-qualified path and filename,
*  much faster than CD-ing to the folder 
*  before/after the ADIR()
*
LOCAL laFPT[1]
ADIR(laFPT,FORCEEXT(DBF(m.tcIUDAlias),"FPT"))
lnFPT = laFPT[1,2]

RETURN MAX(m.lnDBF,m.lnFPT)




***********************************************************
PROCEDURE SP_VMP_IUDLog_ActionOnCheckFileSize
***********************************************************
*
*  _VFP.Help(FULLPATH("VMPHELP.CHM"),,"SP_VMP_IUDLog_ActionOnCheckFileSize header comments")
*
LPARAMETERS tnFileSize, tcIUDAlias
LOCAL lcCommand 
IF FILE("Custom_SP_VMP_IUDLog_ActionOnCheckFileSize.PRG") ;
      OR FILE("Custom_SP_VMP_IUDLog_ActionOnCheckFileSize.FXP") ;
      OR FILE("Custom_SP_VMP_IUDLog_ActionOnCheckFileSize.APP") ;
      OR FILE("Custom_SP_VMP_IUDLog_ActionOnCheckFileSize.EXE") 
  *
  *  Custom_SP_VMP_IUDLog_ActionOnCheckFileSize.* exists,
  *  call it now
  *
  lcCommand = "DO Custom_SP_VMP_IUDLog_ActionOnCheckFileSize " + ;
              "WITH " + TRANSFORM(m.tnFileSize) + [,"] + m.tcIUDAlias + ["]
  &lcCommand
ENDIF
IF FILE("IUD_CFS.PRG") ;
      OR FILE("IUD_CFS.FXP") ;
      OR FILE("IUD_CFS.APP") ;
      OR FILE("IUD_CFS.EXE") 
  *
  *  IUD_CFS.* exists, call it now
  *
  lcCommand = "DO IUD_CFS " + ;
              "WITH " + TRANSFORM(m.tnFileSize) + [,"] + m.tcIUDAlias + ["]
  &lcCommand
ENDIF
RETURN 



***********************************************************
PROCEDURE SP_VMP_IUDLog_XLogging
***********************************************************
*
*  _VFP.Help(FULLPATH("VMPHELP.CHM"),,"SP_VMP_IUDLog_XLogging header comments")
*
RETURN VARTYPE(m.glSuppress_IUDLog_Logging)="L" AND m.glSuppress_IUDLog_Logging



***********************************************************
PROCEDURE SP_VMP_IUDLog_ELogging
***********************************************************
*
*  _VFP.Help(FULLPATH("VMPHELP.CHM"),,"SP_VMP_IUDLog_ELogging header comments")
*
RELEASE glSuppress_IUDLog_Logging
RETURN 



***********************************************************
PROCEDURE SP_VMP_IUDLog_DLogging
***********************************************************
*
*  _VFP.Help(FULLPATH("VMPHELP.CHM"),,"SP_VMP_IUDLog_DLogging header comments")
*
PUBLIC glSuppress_IUDLog_Logging
glSuppress_IUDLog_Logging = .t.
RETURN 



***********************************************************
PROCEDURE SP_VMP_IUDLog_RW
***********************************************************
*
*  _VFP.Help(FULLPATH("VMPHELP.CHM"),,"SP_VMP_IUDLog_RW header comments")
*
RETURN VARTYPE(m.glEnable_IUDLog_RW)="L" AND m.glEnable_IUDLog_RW



***********************************************************
PROCEDURE SP_VMP_IUDLog_ERW
***********************************************************
*
*  _VFP.Help(FULLPATH("VMPHELP.CHM"),,"SP_VMP_IUDLog_ERW header comments")
*
PUBLIC glEnable_IUDLog_RW
glEnable_IUDLog_RW = .t.
RETURN 



***********************************************************
PROCEDURE SP_VMP_IUDLog_DRW
***********************************************************
*
*  _VFP.Help(FULLPATH("VMPHELP.CHM"),,"SP_VMP_IUDLog_DRW header comments")
*
RELEASE glEnable_IUDLog_RW
RETURN 
*
*  this is the end of the SP_VMP_IUDLog trigger code
*
*  The preceding stored procedures were inserted into the
*  Stored Procedures by the XXFWDATA.PRG VMP utility
*
*  !!!!!!!!!!!!!!!!!!!! END VMP IUDLog Comment !!!!!!!!!!!!!!!!!!!!
ENDTEXT
SET TEXTMERGE OFF SHOW
SET TEXTMERGE TO

*
*  and put it all together
*
ERASE newsp.txt   &&& just in case
*
*  build the new stored procedures starting with the
*  code that preceded the old VMP RI stored procedures
*
LOCAL xx, lnCount 
lnCount = 0
IF lnEndLine > 0
  FOR xx = 1 TO lnStartLine-1
    STRTOFILE(laLines[m.xx]+CHR(13)+CHR(10),"newsp.txt",.t.)
    lnCount = m.lnCount + 1
  ENDFOR
 ELSE
  FOR xx = 1 TO ALEN(laLines,1)
    STRTOFILE(laLines[m.xx]+CHR(13)+CHR(10),"newsp.txt",.t.)
    lnCount = m.lnCount + 1
  ENDFOR
ENDIF

*
*  now insert this new stored procedure
*
LOCAL laNewLines[1]
ALINES(laNewLines,FILETOSTR("thissp.txt"))
DO WHILE EMPTY(laNewLines[1])
  *  get rid of any leading blank lines
  ADEL(laNewLines,1)
  DIMENSION laNewLines[ALEN(laNewLines,1)-1]
ENDDO
*
*  put 4 blank lines between procedures after the first one
*
IF m.lnCount > 0
  STRTOFILE(CHR(13)+CHR(10)+CHR(13)+CHR(10)+CHR(13)+CHR(10)+CHR(13)+CHR(10),"newsp.txt",.t.)
ENDIF
FOR xx = 1 TO ALEN(laNewLines,1)
  STRTOFILE(laNewLines[m.xx]+CHR(13)+CHR(10),"newsp.txt",.t.)
ENDFOR

*
*  finally add the stored procedures that were after
*  the old stored procedure
*
IF m.lnEndLine > 0
  FOR xx = lnStartLine TO ALEN(laLines,1)
    STRTOFILE(laLines[m.xx]+CHR(13)+CHR(10),"newsp.txt",.t.)
  ENDFOR
ENDIF

*
*  and add a few blank lines in case there are app-specific
*  stored procedures after this
*
STRTOFILE(CHR(13)+CHR(10)+CHR(13)+CHR(10)+CHR(13) + CHR(10),"newsp.txt",.t.)

*
*  and post the new set of stored procedures to the .DBC
*
APPEND PROCEDURES FROM newsp.txt OVERWRITE
CLOSE TABLES
COMPILE DATABASE (DBC())
ERASE oldsp.txt
ERASE thissp.txt
ERASE newsp.txt
PACK DATABASE 
WAIT CLEAR 
RETURN




*****************************************************
PROCEDURE UpdateSystemViews
*****************************************************
IF VARTYPE(m.plAbort) = "L" AND m.plAbort
  RETURN
ENDIF
IF !UpdateCheck(1)
  RETURN
ENDIF
CreateV_UsersList()                &&& Users
CreateV_UserPrefs()                &&& UserPrefs
CreateV_LoginHistoryOneUser()      &&& LoginHistory + Users
CreateV_LoginHistoryAllUsers()     &&& LoginHistory + Users
CreateV_UserGroupsList()           &&& UserGroups
CreateV_UsrGrpXRef()               &&& UsrGrpXRef + UserGroups
CreateV_UsrGrpXRefList()           &&& UsrGrpXRef + UserGroups
CreateV_GroupMembers()             &&& UserGroups + UsrGrpXRef + Users
CreateV_SecurityAccess()           &&& SecurityAccess + UserGroups + SecuredItems
CreateV_UserAccExceptions()        &&& UserAccExceptions + SecuredItems
RETURN

PROCEDURE CreateViewsByTable
LPARAMETERS tcTable
IF !UpdateCheck(1)
  RETURN .f.
ENDIF
LOCAL lcTable
IF VARTYPE(tcTable) = "C" AND !EMPTY(tcTable)
  lcTable = UPPER(ALLTRIM(tcTable))
 ELSE
  MESSAGEBOX("Procedure CreateViewsByTable in XXFWDATA.PRG " + ;
             "must be passed a table name parameter.", ;
             16, ;
             "Please Note")
  RETURN .f.
ENDIF
DO CASE
  *************************************************
  CASE lcTable = "USERS"
  *************************************************
    RETURN CreateV_UsersList() ;
       AND CreateV_LoginHistoryOneUser() ;
       AND CreateV_LoginHistoryAllUsers() ;
       AND CreateV_GroupMembers()
  *************************************************
  CASE lcTable = "USERPREFS"
  *************************************************
    RETURN CreateV_UserPrefs()
  *************************************************
  CASE lcTable = "USERGROUPS"
  *************************************************
    RETURN CreateV_UserGroupsList() ;
       AND CreateV_UsrGrpXRef() ;
       AND CreateV_UsrGrpXRefList() ; 
       AND CreateV_GroupMembers()
  *************************************************
  CASE lcTable = "USRGRPXREF"
  *************************************************
    RETURN CreateV_UsrGrpXRef() ;
       AND CreateV_UsrGrpXRefList() ; 
       AND CreateV_GroupMembers() 
  *************************************************
  CASE lcTable = "LOGINHISTORY"
  *************************************************
    RETURN CreateV_LoginHistoryOneUser() ; 
       AND CreateV_LoginHistoryAllUsers()   
  *************************************************
  CASE lcTable = "SECURITYACCESS"
  *************************************************
    RETURN CreateV_SecurityAccess()
  *************************************************
  CASE lcTable = "SECUREDITEMS"
  *************************************************
    RETURN CreateV_SecurityAccess() ;
       AND CreateV_UserAccExceptions()
  *************************************************
  CASE lcTable = "USERACCEXCEPTIONS"
  *************************************************
    RETURN CreateV_UserAccExceptions()
ENDCASE
RETURN



* AB - 10/2004 - Start
*****************************************************
PROCEDURE ReindexAllVMPTables
*****************************************************
*!*	LPARAMETERS tlIntegerKeys, tlFreeMemoAETable
LPARAMETERS tlNoBinaryDELETEDTagsInVFP9OrHigher  
IF VARTYPE(m.plAbort) = "L" AND m.plAbort
  RETURN
ENDIF
DO CASE
  CASE PCOUNT() > 0 ;
       AND VARTYPE(m.tlNoBinaryDELETEDTagsInVFP9OrHigher) = "L" ;
       AND m.tlNoBinaryDELETEDTagsInVFP9OrHigher
    RELEASE glUseBinaryDELETEDTagsInVFP9OrHigher
    PUBLIC glUseBinaryDELETEDTagsInVFP9OrHigher
    glUseBinaryDELETEDTagsInVFP9OrHigher = .f.
  CASE PCOUNT() > 0
    RELEASE glUseBinaryDELETEDTagsInVFP9OrHigher
    PUBLIC glUseBinaryDELETEDTagsInVFP9OrHigher
    glUseBinaryDELETEDTagsInVFP9OrHigher = .t.
  CASE VARTYPE(m.glUseBinaryDELETEDTagsInVFP9OrHigher) = "L"
    *
    *  glUseBinaryDELETEDTagsInVFP9OrHigher already
    *  exists, because the optional 7th parameter was
    *  passed to XXFWDATA.PRG, or was created ahead
    *  of time, like this usage in the VMP Application
    *  Setup Wizard:
    *    XXDTES("XXWB.VCX","glUseBinaryDELETEDTagsInVFP9OrHigher = .t.","frmxxwbappsetup","FINISHACTION_CREATEDATABASE")
    *
  OTHERWISE
    *
    *  default behavior
    *
    RELEASE glUseBinaryDELETEDTagsInVFP9OrHigher
    PUBLIC glUseBinaryDELETEDTagsInVFP9OrHigher
    glUseBinaryDELETEDTagsInVFP9OrHigher = .t.
ENDCASE       

IF NOT UpdateCheck(1)
  RETURN .f.
ENDIF

LOCAL lcAppPrefix, llSuccess
lcAppPrefix = SET("DATABASE")

llSuccess = IndexGeneratePKTable(lcAppPrefix)

IF llSuccess
*!*	  llSuccess = IndexMemoAETable(lcAppPrefix, tlFreeMemoAETable, tlIntegerKeys)
  llSuccess = IndexMemoAETable(lcAppPrefix)
ENDIF

IF llSuccess
  llSuccess = IndexUsersTable(lcAppPrefix)
ENDIF

IF llSuccess
*!*	  llSuccess = IndexUserPrefsTable(lcAppPrefix, tlIntegerKeys)
  llSuccess = IndexUserPrefsTable(lcAppPrefix)
ENDIF

IF llSuccess
  llSuccess = IndexLoginHistoryTable(lcAppPrefix)
ENDIF

IF llSuccess
  llSuccess = IndexReportCatalogTable(lcAppPrefix)
ENDIF

IF llSuccess
*!*	  llSuccess = IndexUserGroupsTable(lcAppPrefix, tlIntegerKeys)
  llSuccess = IndexUserGroupsTable(lcAppPrefix)
ENDIF

IF llSuccess
*!*	  llSuccess = IndexUsrGrpXRefTable(lcAppPrefix, tlIntegerKeys)
  llSuccess = IndexUsrGrpXRefTable(lcAppPrefix)
ENDIF

IF llSuccess
*!*	  llSuccess = IndexSecurityAccessTable(lcAppPrefix, tlIntegerKeys)
  llSuccess = IndexSecurityAccessTable(lcAppPrefix)
ENDIF

IF llSuccess
  llSuccess = IndexSecuredItemsTable(lcAppPrefix)
ENDIF

IF llSuccess
  llSuccess = IndexUserAccExceptionsTable(lcAppPrefix)
ENDIF

IF llSuccess
  llSuccess = IndexVMPRITable(lcAppPrefix)
ENDIF

RELEASE glUseBinaryDELETEDTagsInVFP9OrHigher

RETURN llSuccess



********************************************
PROCEDURE IndexGeneratePKTable
********************************************
*
* system-generated PK data
*
LPARAMETERS tcAppPrefix

LOCAL ARRAY laIndexTags[2, 4]
laIndexTags[1, 1] = "GPK_PK"
laIndexTags[1, 2] = "GPK_PK"
laIndexTags[1, 3] = "Primary"

laIndexTags[2, 1] = "GPK_Del"
laIndexTags[2, 2] = "DELETED()"

RETURN IndexXXTable(tcAppPrefix, "GenPK", "GeneratePK", @laIndexTags)



********************************************
PROCEDURE IndexMemoAETable
********************************************

*
*  AutoExpand "dictionary" table for editboxes
*
LPARAMETERS tcAppPrefix

LOCAL ARRAY laIndexTags[1, 4]
laIndexTags[1, 1] = "MAE_Code"
laIndexTags[1, 3] = "CANDIDATE"
laIndexTags[1, 4] = "NOT DELETED()"

LOCAL lnSelect, lcTempAlias, llUsr_FK, llIntegerKeys
lnSelect    = SELECT()
lcTempAlias = SYS(2015)
USE MemoAE AGAIN ALIAS (lcTempAlias) IN 0
LOCAL ARRAY laFields[1]
AFIELDS(laFields, lcTempAlias)
llUsr_FK = (ALEN(laFields, 1) = 3)		&& .T. if a MAE_UsrFK exists; otherwise, .F.
IF llUsr_FK
  llIntegerKeys = (laFields[3, 2] = "I")		&& MAE_UsrFK
ENDIF
USE IN SELECT(lcTempAlias)
SELECT (lnSelect)

DO CASE
  CASE llUsr_FK AND llIntegerKeys
    laIndexTags[1, 2] = "STR(MAE_UsrFK) + UPPER(MAE_Code)"

  CASE llUsr_FK AND !llIntegerKeys
    laIndexTags[1, 2] = "MAE_UsrFK + UPPER(MAE_Code)"

  OTHERWISE
    laIndexTags[1, 2] = "UPPER(MAE_Code)"
ENDCASE

RETURN IndexXXTable(tcAppPrefix, "MemoAE", "MemoAE", @laIndexTags, llUsr_FK)



********************************************
PROCEDURE IndexUserPrefsTable
********************************************

*
*  User Preferences table
*
LPARAMETERS tcAppPrefix

LOCAL ARRAY laIndexTags[3, 4]
laIndexTags[1, 1] = "Prf_CK"
laIndexTags[1, 3] = "CANDIDATE"
laIndexTags[1, 4] = "NOT DELETED()"

LOCAL lnSelect, lcTempAlias, llIntegerKeys
lnSelect    = SELECT()
lcTempAlias = SYS(2015)
USE UserPrefs AGAIN ALIAS (lcTempAlias) IN 0
LOCAL ARRAY laFields[1]
AFIELDS(laFields, lcTempAlias)
llIntegerKeys = (laFields[1, 2] = "I")	&& Prf_UsrFK
USE IN SELECT(lcTempAlias)
SELECT (lnSelect)

IF llIntegerKeys
  laIndexTags[1, 2] = "PADL(Prf_UsrFK, 10) + UPPER(Prf_Item)"
 ELSE
  laIndexTags[1, 2] = "Prf_UsrFK + UPPER(Prf_Item)"
ENDIF

laIndexTags[2, 1] = "Prf_UsrFK"
laIndexTags[2, 2] = "Prf_UsrFK"

laIndexTags[3, 1] = "Prf_Del"
laIndexTags[3, 2] = "DELETED()"

LOCAL llReturnValue
llReturnValue = IndexXXTable(tcAppPrefix, "UsrPrf", "UserPrefs", @laIndexTags)
IF llReturnValue
  ALTER TABLE UserPrefs ADD FOREIGN KEY TAG Prf_UsrFK REFERENCES Users
ENDIF
RETURN llReturnValue




***********************************************************
PROCEDURE IndexLoginHistoryTable
***********************************************************

*
*  Login History table
*
LPARAMETERS tcAppPrefix

LOCAL ARRAY laIndexTags[3, 4]
laIndexTags[1, 1] = "ULH_PK"
laIndexTags[1, 2] = "ULH_PK"
laIndexTags[1, 3] = "Primary"

laIndexTags[2, 1] = "ULH_UsrFK"
laIndexTags[2, 2] = "ULH_UsrFK"

laIndexTags[3, 1] = "ULH_Del"
laIndexTags[3, 2] = "DELETED()"

LOCAL llReturnValue
llReturnValue = IndexXXTable(tcAppPrefix, "USysLH", "LoginHistory", @laIndexTags)
IF llReturnValue
  ALTER TABLE LoginHistory ADD FOREIGN KEY TAG ULH_UsrFK REFERENCES Users
ENDIF
RETURN llReturnValue




***********************************************************
PROCEDURE IndexReportCatalogTable
***********************************************************

*
*  Report Catalog table
*
LPARAMETERS tcAppPrefix

LOCAL ARRAY laIndexTags[3, 4]
laIndexTags[1, 1] = "Rep_PK"
laIndexTags[1, 2] = "Rep_PK"
laIndexTags[1, 3] = "Primary"

laIndexTags[2, 1] = "Rep_Name"
laIndexTags[2, 2] = "UPPER(Rep_Name)"

laIndexTags[3, 1] = "Rep_FRX"
laIndexTags[3, 2] = "UPPER(Rep_FRX)"

RETURN IndexXXTable(tcAppPrefix, "RepCat", "ReportCatalog", @laIndexTags)

*--------------------------------------------------------------------------------------------
* Create index tags for the Users table
*--------------------------------------------------------------------------------------------
PROCEDURE IndexUsersTable
*
*  Users table
*
LPARAMETERS tcAppPrefix

LOCAL ARRAY laIndexTags[5, 4]
laIndexTags[1, 1] = "Usr_PK"
laIndexTags[1, 2] = "Usr_PK"
laIndexTags[1, 3] = "Primary"

laIndexTags[2, 1] = "Usr_Name"
laIndexTags[2, 2] = "UPPER(Usr_LastName + Usr_FirstName + Usr_MiddleInitial)"
laIndexTags[2, 3] = "CANDIDATE"
laIndexTags[2, 4] = "NOT DELETED()"
*
*  note that you might want additional non-candidate keys
*  on the above two expressions because filtered tags are
*  not Rushmore optimizable -- we have not created them
*  by default because there are generally a relatively 
*  small number of users, and the performance benefit for
*  SQL-Selects and joins, filters, etc. is probably not
*  worth the cost of adding 2 more tags
*

laIndexTags[3, 1] = "Usr_LanID"
laIndexTags[3, 2] = "UPPER(Usr_LanID)"
laIndexTags[3, 3] = "CANDIDATE"
laIndexTags[3, 4] = "NOT DELETED()"

laIndexTags[4, 1] = "Usr_IDPass"
laIndexTags[4, 2] = "UPPER(Usr_LanID) + Usr_PassWord"

laIndexTags[5, 1] = "Usr_Del"
laIndexTags[5, 2] = "DELETED()"

RELEASE gaRelations				&& laRels
PUBLIC ARRAY gaRelations[1]
DO SaveAllRelationsToUsersTable

LOCAL llReturnValue
llReturnValue = IndexXXTable(tcAppPrefix, "USys", "Users", @laIndexTags)
IF llReturnValue
  DO RestoreAllRelationsToUsersTable
  RELEASE gaRelations
ENDIF
RETURN llReturnValue

*--------------------------------------------------------------------------------------------
* Create index tags for the UserGroups table
*--------------------------------------------------------------------------------------------
PROCEDURE IndexUserGroupsTable

*
*  User Groups table
*
LPARAMETERS tcAppPrefix
WAIT WINDOW "Creating indexes for the UserGroups table ..." NOWAIT

LOCAL ARRAY laIndexTags[4, 4]
laIndexTags[1, 1] = "UGr_PK"
laIndexTags[1, 2] = "UGr_PK"
laIndexTags[1, 3] = "Primary"

laIndexTags[2, 1] = "UGr_Group"
laIndexTags[2, 2] = "UPPER(UGr_Group)"
laIndexTags[2, 4] = "NOT DELETED()"

laIndexTags[3, 1] = "UGr_Desc"
laIndexTags[3, 2] = "UPPER(UGr_Description)"
laIndexTags[3, 4] = "NOT DELETED()"

laIndexTags[4, 1] = "UGr_Del"
laIndexTags[4, 2] = "DELETED()"

*
*  note that you might want additional non-candidate keys
*  on the above two expressions because filtered tags are
*  not Rushmore optimizable -- we have not created them
*  by default because there are generally a relatively 
*  small number of user groups, and the performance benefit for
*  SQL-Selects and joins, filters, etc. is probably not
*  worth the cost of adding 2 more tags
*

RETURN IndexXXTable(tcAppPrefix, "USysG", "UserGroups", @laIndexTags)

*--------------------------------------------------------------------------------------------
* Create index tags for the UsrGrpXRef table
*--------------------------------------------------------------------------------------------
PROCEDURE IndexUsrGrpXRefTable

*
*  User Group cross-reference table
*
LPARAMETERS tcAppPrefix

LOCAL ARRAY laIndexTags[5, 4]
laIndexTags[1, 1] = "UGX_PK"
laIndexTags[1, 2] = "UGX_PK"
laIndexTags[1, 3] = "Primary"

laIndexTags[2, 1] = "UGX_UGrFK"
laIndexTags[2, 2] = "UGX_UGrFK"

laIndexTags[3, 1] = "UGX_MbrFK"
laIndexTags[3, 2] = "UGX_MemberFK"

laIndexTags[4, 1] = "UGX_MbrTyp"
laIndexTags[4, 2] = "UGX_MemberType"

laIndexTags[5, 1] = "UGX_Del"
laIndexTags[5, 2] = "DELETED()"

LOCAL llReturnValue
llReturnValue = IndexXXTable(tcAppPrefix, "USYSGX", "UsrGrpXRef", @laIndexTags)
IF llReturnValue
  * Note that the following persistent relations have a FOR condition.
  * This is to prevent RI (both VMP and/or the native VFP RI)
  * from deleting all entries that could possibly contain the
  * same values (e.g.: "00001" for both a user and a group).
  * See also the chapter on Referential Integrity in the 
  * VMP Reference Guide.
  *
  * Also note that VMP RI does not depend on persistent relations.
  *
  *  (we could just go with the index tags as above, but
  *  this way we get the relation lines drawn in the Database
  *  Designer)
  *
  ALTER TABLE UsrGrpXRef ;
       ADD FOREIGN KEY UGX_MemberFK ;
       FOR UGX_MemberType = "U" ;
       TAG UGX_UsrFK ;
       REFERENCES Users
  ALTER TABLE UsrGrpXRef ;
       ADD FOREIGN KEY UGX_MemberFK ;
       FOR UGX_MemberType = "G" ;
       TAG UGX_GrpFK ;
       REFERENCES UserGroups
ENDIF
RETURN llReturnValue

*--------------------------------------------------------------------------------------------
* Create index tags for the SecurityAccess table
*--------------------------------------------------------------------------------------------
PROCEDURE IndexSecurityAccessTable

*
*  Security Access table
*
LPARAMETERS tcAppPrefix

LOCAL ARRAY laIndexTags[4, 4]
laIndexTags[1, 1] = "Acc_PK"
laIndexTags[1, 2] = "Acc_PK"
laIndexTags[1, 3] = "Primary"

laIndexTags[2, 1] = "Acc_UGrFK"
laIndexTags[2, 2] = "Acc_UGrFK"

laIndexTags[3, 1] = "Acc_SItFK"
laIndexTags[3, 2] = "Acc_SItFK"

laIndexTags[4, 1] = "Acc_Del"
laIndexTags[4, 2] = "DELETED()"

LOCAL llReturnValue
llReturnValue = IndexXXTable(tcAppPrefix, "USysAc", "SecurityAccess", @laIndexTags)
*!*	MESSAGEBOX('llReturnValue = ' + TRANSFORM(llReturnValue) + ' ...')
IF llReturnValue
  ALTER TABLE SecurityAccess ADD FOREIGN KEY TAG Acc_UGrFK REFERENCES UserGroups
  * SecuredItems is a free table, so there is no Foreign Key Acc_SItFK to it
ENDIF
RETURN llReturnValue



**********************************************************
PROCEDURE IndexSecuredItemsTable
**********************************************************

*
*  Secured Items table
*
LPARAMETERS tcAppPrefix

LOCAL ARRAY laIndexTags[6, 4]
laIndexTags[1, 1] = "SIt_PK"
laIndexTags[1, 2] = "SIt_PK"
laIndexTags[1, 3] = "Candidate"

laIndexTags[2, 1] = "SIt_ID"
laIndexTags[2, 2] = "UPPER(SIt_ID)"
laIndexTags[2, 3] = "Candidate"
laIndexTags[2, 4] = "NOT DELETED()"

*
*  the following CK tag includes both Description and Type
*  to two records with the same Description but different
*  type -- i.e., a "Customers" + "Form" and a "Customers" + "Report"
*
*  the following 2 tags are used in the SecuredItems grid
*  in XXFWSEC.VCX/frmGroupPermissions
*
laIndexTags[3, 1] = "SIt_DescT"
laIndexTags[3, 2] = "UPPER(SIt_Desc) + UPPER(Sit_TypeCD)"
laIndexTags[3, 3] = "Candidate"
laIndexTags[3, 4] = "NOT DELETED()"
*
*  note that you might want additional non-candidate keys
*  for the above two candidate keys because filtered tags are
*  not Rushmore optimizable -- we have not created them
*  by default because there are generally a relatively 
*  small number of secured items, and the performance benefit for
*  SQL-Selects and joins, filters, etc. is probably not
*  worth the cost of adding 2 more tags
*
laIndexTags[4, 1] = "Sit_TypeD"
laIndexTags[4, 2] = "UPPER(Sit_TypeCD) + UPPER(Sit_Desc)"

laIndexTags[5, 1] = "SIt_TypeCD"
laIndexTags[5, 2] = "SIt_TypeCD"

laIndexTags[6, 1] = "SIt_Type"
laIndexTags[6, 2] = "SIt_Type"

RETURN IndexXXTable(tcAppPrefix, "USysSI", "SecuredItems", @laIndexTags, .T.)



**********************************************************
PROCEDURE IndexUserAccExceptionsTable
**********************************************************

*
*  User Access Exceptions table
*
LPARAMETERS tcAppPrefix

LOCAL ARRAY laIndexTags[4, 4]
laIndexTags[1, 1] = "UAE_PK"
laIndexTags[1, 2] = "UAE_PK"
laIndexTags[1, 3] = "Primary"

laIndexTags[2, 1] = "UAE_USrFK"
laIndexTags[2, 2] = "UAE_USrFK"

laIndexTags[3, 1] = "UAE_SItFK"
laIndexTags[3, 2] = "UAE_SItFK"

laIndexTags[4, 1] = "UAE_Del"
laIndexTags[4, 2] = "DELETED()"

LOCAL llReturnValue
llReturnValue = IndexXXTable(tcAppPrefix, "USysAE", "UserAccExceptions", @laIndexTags)
IF llReturnValue
  ALTER TABLE UserAccExceptions ADD FOREIGN KEY TAG UAE_USrFK REFERENCES Users
  * SecuredItems is a free table, so there is no Foreign Key UAE_SItFK to it
ENDIF





**********************************************************
PROCEDURE IndexVMPRITable
**********************************************************

*
*  Referential Integrity (RI) table
*
LPARAMETERS tcAppPrefix
RETURN IndexXXTable(m.tcAppPrefix, "RI", "VMPRI")




**********************************************************
PROCEDURE IndexIUDLogTable
**********************************************************
*
*  IUDLog table
*
LPARAMETERS tcAppPrefix, tcTableName

LOCAL ARRAY laIndexTags[8, 4]

LOCAL llWasUsed
IF USED("IUDLog")
  SELECT IUDLog
  llWasUsed = .t.
 ELSE
  USE IUDLog IN 0
  SELECT IUDLog
ENDIF 
 
laIndexTags[1, 1] = "IUD_Table"
laIndexTags[1, 2] = "UPPER(IUD_TableName)"

laIndexTags[2, 1] = "IUD_UsrFK"
laIndexTags[2, 2] = "IUD_UsrFK"

laIndexTags[3, 1] = "IUD_Usr"
laIndexTags[3, 2] = "UPPER(IUD_Usr)"

laIndexTags[4, 1] = "IUD_Action"
laIndexTags[4, 2] = "IUD_Action"

laIndexTags[5, 1] = "IUD_Date"
laIndexTags[5, 2] = "TTOD(IUD_DateTime)"

laIndexTags[6, 1] = "IUD_DT"
laIndexTags[6, 2] = "IUD_DateTime"

laIndexTags[7, 1] = "IUD_TranID"
laIndexTags[7, 2] = "IUD_TransID"

laIndexTags[8, 1] = "IUD_SYS0"
laIndexTags[8, 2] = "UPPER(IUD_SYS0)"

*!*	laIndexTags[3, 1] = "IUD_Del"     no need for this tag, since few, if any records
*!*	laIndexTags[3, 2] = "DELETED()"   will be DELETED from this table without a PACK

LOCAL llReturnValue
llReturnValue = IndexXXTable(tcAppPrefix, m.tcTableName, m.tcTableName, @m.laIndexTags)
IF m.llReturnValue AND INDBC("Users","TABLE")
  ALTER TABLE IUDLog ADD FOREIGN KEY TAG IUD_UsrFK REFERENCES Users
  *  IUD_UsrFK is left empty when the Users.Usr_PK is not available (and IUD_Usr populated instead)
ENDIF
RETURN m.llReturnValue








*--------------------------------------------------------------------------------------------
*   Procedure: IndexXXTable
*     Purpose: Create index tags for the specified table
*  Parameters: tcAppPrefix = Database (2-letter abbreviation)
*              tcDBF       = Name of .DBF (minus the 2-letter prefix)
*              tcTable     = Name of table (contained in the .DBC)
*              taIndexTags = Index tags to create for the specified table
* Sample Call: DO IndexXXTable WITH tcAppPrefix, 'GeneratePK'
*--------------------------------------------------------------------------------------------
***********************************************************
PROCEDURE IndexXXTable
***********************************************************
LPARAMETERS tcAppPrefix, tcDBF, tcTable, taIndexTags, tlFreeTable

LOCAL lnPCount, llDBWasAlreadyOpenExclusively, llDBWasAlreadyOpenShared, ;
      llDBWasNotAlreadyOpen, lcDir
lnPCount = PCOUNT()

STORE .F. TO llDBWasAlreadyOpenExclusively, llDBWasAlreadyOpenShared, llDBWasNotAlreadyOpen

IF NOT m.tlFreeTable
  IF DBUSED(m.tcAppPrefix)
    IF ISEXCLUSIVE(m.tcAppPrefix, 2)	&& Was database OPENed EXCLUSIVEly?
      llDBWasAlreadyOpenExclusively = .T.
    ELSE
      llDBWasAlreadyOpenShared = .T.
    ENDIF
  ELSE
    OPEN DATABASE (m.tcAppPrefix) EXCLUSIVE
    llDBWasNotAlreadyOpen = .T.
  ENDIF

  * If table is not in the database, 
  IF NOT INDBC(m.tcTable, "Table")
    RETURN .F.
  ENDIF
ENDIF

WAIT WINDOW "Creating indexes for the " + m.tcTable + " table ..." NOWAIT
LOCAL llUsed
llUsed = .T.				&& Assume that the table is currently open
IF NOT USED(m.tcTable)
  llUsed = .F.

  LOCAL llSuccess, loError
  llSuccess = .T.
  lcDir = GetDBCDir()
  TRY
    USE (m.lcDir + m.tcAppPrefix + m.tcDBF + ".DBF") ALIAS (m.tcTable) EXCLUSIVE
  CATCH TO loError
    llSuccess = .F.
  ENDTRY
  IF (NOT m.llSuccess) AND (VARTYPE(m.loError) = "O")
    IF loError.ErrorNo = 1567
      * Primary key property is invalid; please validate database (Error 1567)
      MESSAGEBOX(loError.Message, 0 + 16, "XXFWData Error")
      WAIT CLEAR
      RETURN .F.
    ENDIF
  ENDIF
*!*	The primary key property for this table in the database is corrupted.
*!*	Use VALIDATE DATABASE to correct the problem.
ENDIF
LOCAL lnSelect
lnSelect = SELECT()
SELECT (m.tcTable)

DELETE TAG ALL

IF UPPER(m.tcTable) = UPPER('VMPRI')
  *
  *  create a candidate key index tag of the maximum
  *  length VFP will allow (if your table names are 
  *  not unique for the first 115 characters, we don't
  *  want to hear about it!  <g>):
  *
  INDEX ON UPPER(LEFTC(XRI_Parent_TableName,115)) + ;
           UPPER(LEFTC(XRI_Child_TableName,115)) + ;
           UPPER(XRI_Child_Index_Tag) ;
           TAG XRI_ID COLLATE "MACHINE" CANDIDATE FOR NOT DELETED()
  * COLLATE "MACHINE" since other collate sequences generate an Invalid Key Length error on expressions > 120 characters
  *
  *  add tags to optimize the SCANs in the VMP_RI 
  *  Stored Procedure
  *    XXDTES("XXFWDATA.PRG","SCAN WHILE UPPER(XRI_Child_TableName)")
  *    XXDTES("XXFWDATA.PRG","SCAN WHILE UPPER(XRI_Parent_TableName) = m.lcTriggerSourceName")
  *    XXDTES("XXFWDATA.PRG",'IF NOT INLIST(XRI_Delete_Type,"C","R")')   &&& the line above this line
  *
  INDEX ON UPPER(XRI_Parent_TableName) TAG XRI_Parent
  INDEX ON UPPER(XRI_Child_TableName) TAG XRI_Child
ELSE
  LOCAL lnIndexTag, lcIndexTag, lcIndexExpr, lcIndexType, lcForExpression, lcSetCollate, ;
        lcBinary
  FOR lnIndexTag = 1 TO ALEN(taIndexTags, 1)
    lcIndexTag      = taIndexTags[m.lnIndexTag, 1]	&& Index tag name
    lcIndexExpr     = taIndexTags[m.lnIndexTag, 2]	&& Index tag expression
    lcIndexType     = taIndexTags[m.lnIndexTag, 3]	&& Index tag type
    lcForExpression = taIndexTags[m.lnIndexTag, 4]	&& Index tag "FOR" (filter) expression
    IF (NOT EMPTY(m.lcIndexType)) AND (UPPER(m.lcIndexType) == UPPER("Primary"))
      ALTER TABLE (m.tcTable) ADD PRIMARY KEY &lcIndexExpr. TAG (m.lcIndexTag) COLLATE "MACHINE"
      * COLLATE "MACHINE" since other collate sequences generate an Invalid Key Length error on expressions > 120 chars
    ELSE
      IF EMPTY(m.lcForExpression)
        lcForExpression =  SPACE(0)
      ELSE
        lcForExpression = "FOR " + m.lcForExpression
      ENDIF
      IF EMPTY(m.lcIndexType)
        lcIndexType = SPACE(0)
      ENDIF
      IF VERSION(5) >= 900 ;   &&& VFP9 or higher
           AND ;
           VAL(GETWORDNUM(VERSION(4),4,".")) >= 2124 ;   &&& exclude the public beta of VFP9
           AND ;
           VARTYPE(m.glUseBinaryDELETEDTagsInVFP9OrHigher) = "L" AND m.glUseBinaryDELETEDTagsInVFP9OrHigher ;
           AND ;
           (UPPER(ALLTRIM(m.lcIndexExpr)) == "DELETED()" ;
           OR UPPER(ALLTRIM(m.lcIndexExpr)) == "NOT DELETED()" ;
           OR UPPER(ALLTRIM(m.lcIndexExpr)) == ".NOT. DELETED()" ;
           OR UPPER(ALLTRIM(m.lcIndexExpr)) == "! DELETED()" ;
           OR UPPER(ALLTRIM(m.lcIndexExpr)) == "!DELETED()")
        *
        *  in VFP9, take advantage of BINARY index for 
        *  DELETED() tags
        *
        lcBinary = "BINARY"
       ELSE
        lcBinary = SPACE(0)
      ENDIF     
      INDEX ON &lcIndexExpr. TAG (m.lcIndexTag) &lcIndexType. &lcForExpression. &lcBinary.
    ENDIF
  ENDFOR
ENDIF

IF NOT m.llUsed
  USE IN SELECT(m.tcTable)
ENDIF
SELECT (m.lnSelect)

DO CASE
  CASE m.llDBWasNotAlreadyOpen
    * Since the database was not already open, CLOSE it now
    CLOSE DATABASES

  CASE m.llDBWasAlreadyOpenShared
    * Since the database had already been OPENed SHARED,
    * CLOSE it now and re-OPEN it SHARED.
    CLOSE DATABASES
    OPEN DATABASE (tcAppPrefix) SHARED

  CASE m.llDBWasAlreadyOpenExclusively
    * Since the database had already been OPENed EXCLUSIVEly,
    * nothing to do now.
ENDCASE

WAIT CLEAR
RETURN




*--------------------------------------------------------------------------------------------
*   Procedure: SaveAllRelationsToUsersTable
*     Purpose: Save all relations to the Users table
*  Parameters: None
* Sample Call: RELEASE gaRelations
*              PUBLIC ARRAY gaRelations[1]
*              DO SaveAllRelationsToUsersTable
*--------------------------------------------------------------------------------------------
PROCEDURE SaveAllRelationsToUsersTable
*
*  check for existing persistent relations to
*  the Users table, which will prevent removing
*  the Users table, if necessary
*
IF ADBOBJECTS(gaRelations,"RELATION") > 0
  FOR xx = 1 TO ALEN(gaRelations,1)
    IF gaRelations[xx,2] == "USERS" 
      *
      *  this is a persistent relation to the Users
      *  table -- blow it away and restore once
      *  we've re-created the Users table
      *
      ALTER TABLE (gaRelations[xx,1]) DROP FOREIGN KEY TAG (gaRelations[xx,3]) SAVE
    ENDIF
  ENDFOR  &&& loop thru all relations in the .DBC
ENDIF  &&& the .DBC contains relations
RETURN

*--------------------------------------------------------------------------------------------
*   Procedure: RestoreAllRelationsToUsersTable
*     Purpose: Restore all relations to the Users table
*  Parameters: None
* Sample Call: DO RestoreAllRelationsToUsersTable
*--------------------------------------------------------------------------------------------
PROCEDURE RestoreAllRelationsToUsersTable
*
*  put back any persistent relations we blew
*  away above (note that if XXFWDATA.PRG has been
*  called with a 2nd parameter that means tables
*  other than Users are being created/re-created,
*  those tables will execute their ALTER TABLE..ADD FOREIGN KEY..
*  code, which is fine and doesn't crash -- in fact,
*  it's a good thing, because ADBOBJECTS() doesn't
*  include any FOR condition for the persistent
*  relation
*
IF TYPE("gaRelations[1]") = "C"
  FOR xx = 1 TO ALEN(gaRelations,1)
    IF gaRelations[xx,2] == "USERS"
      ALTER TABLE (gaRelations[xx,1]) ADD FOREIGN KEY TAG (gaRelations[xx,3]) REFERENCES (gaRelations[xx,2])  
    ENDIF
  ENDFOR
ENDIF
RETURN
* AB - 10/2004 - End



*********************************************************
PROCEDURE GetLongestTableName
*********************************************************
*
*  RETURNs the TableName of the table in the current
*  SET DATABASE TO database that has the longest TableName
*
*  XXDTES("XXWB.VCX","GetLongestTableName()","frmXXWBIUDLog","SETPROPERTIESFORCURRENTDATABASE")
*
LPARAMETERS tcExcludePrefix
LOCAL lcTableName, laTables[1], lcTable, lcTableName, lnLen
lcTableName = SPACE(0)
lcTableName = .NULL.

IF EMPTY(SET("DATABASE"))
  RETURN m.lcTableName
ENDIF

IF ADBOBJECTS(laTables,"TABLE") = 0
  RETURN m.lcTableName
ENDIF

lnLen = 0
FOR EACH lcTable IN laTables
  IF VARTYPE(m.tcExcludePrefix) = "C" ;
       AND NOT EMPTY(m.tcExcludePrefix) ;
       AND UPPER(m.lcTable) = UPPER(ALLTRIM(m.tcExcludePrefix))
    LOOP 
  ENDIF
  IF LENC(m.lcTable) > m.lnLen
    lcTableName = m.lcTable
    m.lnLen = LENC(m.lcTableName)
  ENDIF
ENDFOR

RETURN m.lcTableName



*********************************************************
PROCEDURE Remove_SP_VMP_IUDLog_CallsFromAllTriggersOfAllTables
*********************************************************
*
*  remove the SP_VMP_IUDLog() calls from all triggers
*  of all tables contained in the current SET("DATABASE")
*
*  RETURNs a logical value indicating success, but each
*  removal is attempted no matter what -- when this routine 
*  RETURNs .F., at least one removal failed, but others
*  may have succeeded
*
LOCAL laTables[1], xx, llSuccess
IF ADBOBJECTS(laTables,"TABLE") = 0
  RETURN .t.
ENDIF
llSuccess = .t.
FOR xx = 1 TO ALEN(laTables,1)
  IF NOT Remove_SP_VMP_IUDLog_CallsFromAllTriggersOfOneTable(laTables[m.xx])
    llSuccess = .f.
  ENDIF
ENDFOR
RETURN m.llSuccess



*********************************************************
PROCEDURE Remove_SP_VMP_IUDLog_CallsFromAllTriggersOfOneTable
*********************************************************
*
*  remove the SP_VMP_IUDLog() calls from all triggers
*  of the indicated tables contained in the current SET("DATABASE")
*
*  each removal is attempted, whether any of them fail
*  or not -- if this routine RETURNs .F., then one or
*  more removals failed
*
LPARAMETERS tcTableName
llSuccess = .t.
IF NOT Remove_SP_VMP_IUDLog_Trigger(m.tcTableName,"I")
  llSuccess = .f.
ENDIF
IF NOT Remove_SP_VMP_IUDLog_Trigger(m.tcTableName,"U")
  llSuccess = .f.
ENDIF
IF NOT Remove_SP_VMP_IUDLog_Trigger(m.tcTableName,"D")
  llSuccess = .f.
ENDIF
RETURN m.llSuccess



*********************************************************
PROCEDURE Remove_SP_VMP_IUDLog_Trigger
*********************************************************
*
*  remove the SP_VMP_IUDLog() call from the indicated
*  trigger of the the passed table (contained in the current 
*  SET("DATABASE"))
*
*  RETURNs a logical value indicating success
*
LPARAMETERS tcTableName, tcIUD
LOCAL lcTrigger, lcNewTrigger, loException, lcString1, xx
DO CASE
  CASE tcIUD = "I"
    lcTrigger = DBGETPROP(m.tcTableName,"TABLE","InsertTrigger")
  CASE tcIUD = "U"
    lcTrigger = DBGETPROP(m.tcTableName,"TABLE","UpdateTrigger")
  OTHERWISE
    lcTrigger = DBGETPROP(m.tcTableName,"TABLE","DeleteTrigger")
ENDCASE
IF NOT UPPER("SP_VMP_IUDLog(") $ UPPER(m.lcTrigger) ;
    OR EMPTY(m.lcTrigger)
  *  no SP_VMP_IUDLog() trigger on this table
  RETURN .t.
ENDIF
DO CASE
  ****************************************************
  CASE NOT ".AND." $ UPPER(m.lcTrigger)
  ****************************************************
    *
    *  trigger consists entirely of the SP_VMP_IUDLog() call
    *  
    lcNewTrigger = SPACE(0)
  ****************************************************
  CASE UPPER(m.lcTrigger) = UPPER("SP_VMP_IUDLog(")
  ****************************************************
    *
    *  SP_VMP_IUDLog() is the first expression in the trigger
    *
    lcNewTrigger = SUBSTRC(m.lcTrigger, ;
                           AT_C(".AND.",m.lcTrigger,1)+5)
  ****************************************************
  CASE NOT UPPER(".AND.SP_VMP_IUDLog(") $ UPPER(m.lcTrigger)
  ****************************************************
    *
    *  SP_VMP_IUDLog() is the last expression in the trigger
    *
    lcNewTrigger = LEFTC(m.lcTrigger, ;
                         RATC(".AND.",m.lcTrigger,1)-1)
  ****************************************************
  OTHERWISE
  ****************************************************
    *
    *  SP_VMP_IUDLog() is somewhere in the middle,
    *  with ANDs on both sides
    *
    lcNewTrigger = STRTRAN(m.lcTrigger,".AND.","~",1,-1,1)
    LOCAL lcString1
    lcString1 = SPACE(0)
    FOR xx = 1 TO OCCURS("~",m.lcNewTrigger)+1
      IF NOT UPPER("SP_VMP_IUDLog(") $ UPPER(GETWORDNUM(m.lcNewTrigger,m.xx,"~"))
        lcString1 = m.lcString1 + GETWORDNUM(m.lcNewTrigger,m.xx,"~") + ".AND."
      ENDIF
    ENDFOR
    lcNewTrigger = LEFTC(m.lcString1,LENC(m.lcString1)-5)
ENDCASE
TRY
  DO CASE
    CASE EMPTY(m.lcNewTrigger) AND m.tcIUD = "I"
      DELETE TRIGGER ON &tcTableName FOR INSERT
    CASE EMPTY(m.lcNewTrigger) AND m.tcIUD = "U"
      DELETE TRIGGER ON &tcTableName FOR UPDATE
    CASE EMPTY(m.lcNewTrigger) AND m.tcIUD = "D"
      DELETE TRIGGER ON &tcTableName FOR DELETE
    CASE m.tcIUD = "I"
      CREATE TRIGGER ON &tcTableName FOR INSERT AS &lcNewTrigger
    CASE m.tcIUD = "U"
      CREATE TRIGGER ON &tcTableName FOR UPDATE AS &lcNewTrigger
    CASE m.tcIUD = "D"
      CREATE TRIGGER ON &tcTableName FOR DELETE AS &lcNewTrigger
  ENDCASE
CATCH TO loException
ENDTRY
RETURN NOT VARTYPE(m.loException) = "O"
  
  
  
  
*********************************************************
PROCEDURE Add_SP_VMP_IUDLog_CallsToOneIUDTriggerOfOneTable
*********************************************************
*
*  Add the SP_VMP_IUDLog() calls from all triggers
*  of the indicated tables contained in the current SET("DATABASE")
*
*  each add is attempted, whether any of them fail
*  or not -- if this routine RETURNs .F., then one or
*  more removals failed
*
*  ASSUMES that the current SET DATABASE is the one
*  containing tcTableName
*
LPARAMETERS tcTableName, ;
            tcTriggerType, ;       &&& SP_VMP_IUDLog() parameter 1
            PLACEHOLDER, ;         &&& SP_VMP_IUDLog() parameter 2
            tlIgnoreMemos, ;       &&& SP_VMP_IUDLog() parameter 3
            tlIgnoreBlobs, ;       &&& SP_VMP_IUDLog() parameter 4
            tlIgnoreVarbinarys, ;  &&& SP_VMP_IUDLog() parameter 5
            tcPKFieldName          &&& SP_VMP_IUDLog() parameter 6

IF VARTYPE(m.tcTableName) = "C" AND NOT EMPTY(m.tcTableName) AND INDBC(m.tcTableName,"TABLE") 
 ELSE
  ASSERT .f. MESSAGE PROGRAM() + ;
      " has not been passed a valid tcTableName parameter"
  RETURN .f.
ENDIF
IF VARTYPE(m.tcTriggerType) = "C" AND NOT EMPTY(m.tcTableName) AND INLIST(m.tcTriggerType,"I","U","D")
 ELSE
  ASSERT .f. MESSAGE PROGRAM() + ;
      " has not been passed a valid tcTriggerType parameter"
  RETURN .f.
ENDIF

LOCAL llSuccess, lcString, lcTriggerType, lcPlaceHolder, ;
      lcPKFieldName, loException
lcTriggerType = LEFTC(UPPER(ALLTRIM(m.tcTriggerType)),1)
lcTriggerType = ["] + m.lcTriggerType + ["]
IF VARTYPE(m.PLACEHOLDER) = "C"
  lcPlaceHolder = ["] + m.PLACEHOLDER + ["]
 ELSE 
  lcPlaceHolder = TRANSFORM(m.PLACEHOLDER)
ENDIF
IF VARTYPE(m.tcPKFieldName) = "C" AND NOT EMPTY(m.tcPKFieldName)
  lcPKFieldName = ["] + m.tcPKFieldName + ["]
 ELSE
  lcPKFieldName = .NULL.
ENDIF
llSuccess = .t.
DO CASE
  CASE PCOUNT() = 2
    lcString = [SP_VMP_IUDLog(] + ;
               m.lcTriggerType + ;
               [)]
  CASE PCOUNT() = 3
    lcString = [SP_VMP_IUDLog("] + ;
               m.lcTriggerType + "," + ;
               m.PLACEHOLDER + ;
               [)]
  CASE PCOUNT() = 4
    lcString = [SP_VMP_IUDLog("] + ;
               m.lcTriggerType + "," + ;
               m.PLACEHOLDER + "," + ;
               TRANSFORM(m.tlIgnoreMemos) + ;
               [)]
  CASE PCOUNT() = 5
    lcString = [SP_VMP_IUDLog("] + ;
               m.lcTriggerType + "," + ;
               m.PLACEHOLDER + "," + ;
               TRANSFORM(m.tlIgnoreMemos) + "," + ;
               TRANSFORM(m.tlIgnoreBlobs) + ;
               [)]
  CASE PCOUNT() = 6
    lcString = [SP_VMP_IUDLog("] + ;
               m.lcTriggerType + "," + ;
               m.PLACEHOLDER + "," + ;
               TRANSFORM(m.tlIgnoreMemos) + "," + ;
               TRANSFORM(m.tlIgnoreBlobs) + "," + ;
               TRANSFORM(m.tlIgnoreVarbinarys) + ;
               [)]
  CASE PCOUNT() = 7 AND NOT ISNULL(m.lcPKFieldName)
    lcString = [SP_VMP_IUDLog("] + ;
               m.lcTriggerType + "," + ;
               m.PLACEHOLDER + "," + ;
               TRANSFORM(m.tlIgnoreMemos) + "," + ;
               TRANSFORM(m.tlIgnoreBlobs) + "," + ;
               TRANSFORM(m.tlIgnoreVarbinarys) + "," + ;
               m.lcPKFieldName + ;
               [)]
ENDCASE  

DO CASE
  CASE m.lcTriggerType = ["I"]
    IF NOT EMPTY(DBGETPROP(m.tcTableName,"TABLE","InsertTrigger"))
      lcString = DBGETPROP(m.tcTableName,"TABLE","InsertTrigger") + " AND " + m.lcString
    ENDIF      
  CASE m.lcTriggerType = ["U"]
    IF NOT EMPTY(DBGETPROP(m.tcTableName,"TABLE","UpdateTrigger"))
      lcString = DBGETPROP(m.tcTableName,"TABLE","UpdateTrigger") + " AND " + m.lcString
    ENDIF      
  CASE m.lcTriggerType = ["D"]
    IF NOT EMPTY(DBGETPROP(m.tcTableName,"TABLE","DeleteTrigger"))
      lcString = DBGETPROP(m.tcTableName,"TABLE","DeleteTrigger") + " AND " + m.lcString
    ENDIF      
ENDCASE

TRY
DO CASE
  CASE m.lcTriggerType = ["I"]
    CREATE TRIGGER ON (m.tcTableName) FOR INSERT AS &lcString
  CASE m.lcTriggerType = ["U"]
    CREATE TRIGGER ON (m.tcTableName) FOR UPDATE AS &lcString
  CASE m.lcTriggerType = ["D"]
    CREATE TRIGGER ON (m.tcTableName) FOR DELETE AS &lcString
ENDCASE
CATCH TO loException
llSuccess = .f.
ENDTRY

IF VARTYPE(m.loException) = "O"
  ASSERT .f. MESSAGE PROGRAM() + ;
       " has failed to CREATE the indicated TRIGGER ON " + m.tcTableName + " because:" + ;
       CHR(13) + CHR(10) + ;
       loException.Message
ENDIF

RETURN m.llSuccess




*********************************************************
PROCEDURE RemoveOptionalFieldFromIUDLogTable
*********************************************************
*
*  remove the passed (optional) IUD_FieldName from
*  the passed IUDLog table, which could be "IUDLog"
*  or a 'dedicated' IUDLog_<TableName> table
*
*  ASSUMES that the tcIUDLogTableName is in the
*  current SET DATABASE TO database
*
LPARAMETERS tcIUDLogTableName, tcFieldName

LOCAL loException, llClose

TRY
IF UPPER(ALLTRIM(m.tcFieldName)) == "IUD_SYS0"
  DO CASE
    CASE USED(m.tcIUDLogTableName) AND ISEXCLUSIVE(m.tcIUDLogTableName,1)
    CASE NOT USED(m.tcIUDLogTableName)
      llClose = .t.
      USE (m.tcIUDLogTableName) IN 0 EXCLUSIVE 
    OTHERWISE
      USE IN (m.tcIUDLogTableName)
      USE (m.tcIUDLogTableName) IN 0 EXCLUSIVE 
  ENDCASE      
  SELECT (m.tcIUDLogTableName)
  DELETE TAG IUD_SYS0
  IF m.llClose
    USE IN (m.tcIUDLogTableName)
  ENDIF
ENDIF
ALTER TABLE (m.tcIUDLogTableName) DROP COLUMN (m.tcFieldName)
CATCH TO loException
ENDTRY

IF VARTYPE(m.loException) = "O"
  ASSERT .f. MESSAGE PROGRAM() + ;
      " failed to ALTER TABLE " + TRANSFORM(m.tcIUDLogTableName) + " DROP COLUMN " + TRANSFORM(m.tcFieldName) + " because:" + ;
      CHR(13) + CHR(10) + ;
      loException.Message
ENDIF

RETURN NOT VARTYPE(m.loException) = "O"
  