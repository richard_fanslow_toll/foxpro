*
*  XXDTCNTC.PRG
*  DeveloperTool:  Has been renamed to XXDTCountCode

LOCAL lcNewProgram
lcNewProgram = "XXDTCountCode"
MESSAGEBOX(PROGRAM() + " has been replaced with " + lcNewProgram + ".PRG -- " + ;
           "please DO " + lcNewProgram + " instead", 48, "Please Note")
ACTIVATE WINDOW Command 
KEYBOARD "DO " + lcNewProgram
