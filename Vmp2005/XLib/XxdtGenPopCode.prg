*
*  XXDTGenPopCode.PRG
*  Call XXTOOLS.VCX/frmXXDTGenPopCode to collect 
*  information to pass to XXTOOLS.VCX/cusCreatePopCode to 
*  turn each record in all/one table in a .DBC or one free 
*  table into INSERT INTO code.
*
*  Copyright (c) 2004-2005 Visionpace   All Rights Reserved
*                17501 East 40 Hwy., Suite 218
*                Independence, MO 64055
*                816-350-7900 
*                http://www.visionpace.com
*                http://vmpdiscussion.visionpace.com
*  Author:  Drew Speedie
*

*
*  frmXXDTGenPopCode is a modeless form, so:
*
CLOSE ALL 

PUBLIC goXXDTGenPopCode 
X8SETCLS("XXTOOLS.VCX")
goXXDTGenPopCode = CREATEOBJECT("frmXXDTGenPopCode")
goXXDTGenPopCode .Show()

RETURN 



**************************************
*  OLD CODE 
*  this was the way this worked before 10/29/03,
*  when the dialog called here was MODAL -- the
*  current logic above calls the now-MODELESS
*  frmXXDTGenPopCode form
*
*  in addition to the code modification here, 
*  the frmXXDTGenPopCode class has been modified to:
*    1- Change its WindowType property to 0-Modeless
*    2- Add this line of code:
*         XXDTES("XXTOOLS.VCX","CLOSE ALL ","frmxxdtgenpopcode","OKACTION")
*    3- Comment-out this line of code:
*         XXDTES("XXTOOLS.VCX","DODEFAULT()","frmxxdtgenpopcode","OKACTION")
*    4- Change the Caption of the OK button to "Generate"
*    5- Change the Caption of the Cancel button to "Done"
**************************************

*
*  first, save current ADATABASES() information for
*  reset when we're done, because I'm going to
*  close all open databases
*
LOCAL lcSetDatabase, laDBCs[1], xx, lcDBC, lcExcl
lcSetDatabase = SET("DATABASE")
IF ADATABASES(laDBCs) > 0
  FOR xx = 1 TO ALEN(laDBCs,1)
    SET DATABASE TO (laDBCs[xx,1])
    lcDBC = laDBCs[xx,1]
    laDBCs[xx,1] = ISEXCLUSIVE(lcDBC,2)
    CLOSE DATABASE
  ENDFOR
ENDIF

LOCAL lcNotify
lcNotify = SET("NOTIFY")
SET NOTIFY OFF

*
*  call the XXTOOLS.VCX/frmXXDTGenPopCode dialog
*
LOCAL loDialog
X8SETCLS("XXTOOLS.VCX")
loDialog = CREATEOBJECT("frmXXDTGenPopCode")
loDialog.Show()

*
*  open all the databases that were open when we started
*
IF TYPE("laDBCs[1,2]") = "C"
  FOR xx = 1 TO ALEN(laDBCs,1)
    lcExcl = IIF(laDBCs[xx,1],"EXCLUSIVE","SHARED")
    OPEN DATABASE (laDBCs[xx,2]) &lcExcl
  ENDFOR
ENDIF
SET DATABASE TO &lcSetDatabase
SET NOTIFY &lcNotify

ACTIVATE WINDOW Command

RETURN 
