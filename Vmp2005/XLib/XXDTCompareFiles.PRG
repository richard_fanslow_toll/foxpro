*
*  XXDTCompareFiles.PRG
*  Call XXTOOLS.VCX/frmXXDTCompareFiles
*  as a simple interface to X8CompareFiles.PRG
*
*  Copyright (c) 2003-2005 Visionpace   All Rights Reserved
*                17501 East 40 Hwy., Suite 218
*                Independence, MO 64055
*                816-350-7900 
*                http://www.visionpace.com
*                http://vmpdiscussion.visionpace.com
*  Author:  Drew Speedie
*

*
*  frmXXDTCompareFiles is a modeless form, so:
*
CLOSE ALL 

PUBLIC goXXDTCompareFiles
X8SETCLS("XXTOOLS.VCX")
goXXDTCompareFiles = CREATEOBJECT("frmXXDTCompareFiles")
goXXDTCompareFiles .Show()

RETURN 
