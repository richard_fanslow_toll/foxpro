*
*  XXWB.PRG
*  Summon a dialog from which all VMP wizards 
*  and externally-called builders (builders
*  that are invoked from the Command Window
*  rather than by calling _BUILDER) be 
*  reviewed and/or executed
*
*  Copyright (c) 1997-2005 Visionpace   All Rights Reserved
*                17501 East 40 Hwy., Suite 218
*                Independence, MO 64055
*                816-350-7900 
*                http://www.visionpace.com
*                http://vmpdiscussion.visionpace.com
*  Author:  Drew Speedie
*

SET ASSERTS on

IF VARTYPE(oApp) = "O" ;
     OR VARTYPE(oLib) = "O" ;
     OR VARTYPE(oForms) = "O" ;
     OR TYPE("_Screen.Activeform.BaseClass") = "C"
  *
  *  looks like a standalone VMP form, a VMP application,
  *  or some VFP form is running at the moment -- this
  *  utility is intended to be run when none of those are
  *  going on
  *
  MESSAGEBOX("The XXWB.PRG is intended to be run outside " + ;
             "the context of a VMP app, VMP standalone form, " + ;
             "or any other executing VFP code.  Please " + ;
             "try XXWB again when there is nothing else " + ;
             "going on in the VFP development environment.", ;
             48,"Please Note")
  return             
ENDIF

CLEAR

*
*  determine the XLIB path --
*  the path where XXWB.PRG is located
*
local lcXDir
lcXDir = JUSTPATH(FULLPATH("XXWB.PRG"))
*
*  find all xxwb*.PRGs
*
local lcCurDir
lcCurDir = FULLPATH(CURDIR())
cd (lcXDir)
private paPRGs 
IF adir(paPRGs,"XXWB*.PRG") = 0 ;
     OR (alen(paPRGs,1) = 1 AND paPRGs[1,1] = "XXWB.PRG")
  messagebox("No XXWB*.PRGs found",0,"Please Note")
  cd (lcCurDir)
  return
ENDIF
local xx
FOR xx = alen(paPrgs,1) TO 1 STEP -1
  IF INLIST(paPrgs[xx,1],"XXWB.PRG", ;
                         "XXWBAPPS.PRG", ;
                         "XXWBNEWF.PRG")
    adel(paPrgs,xx)
    dimension paPrgs[alen(paPrgs,1)-1,alen(paPrgs,2)]
  ENDIF
ENDFOR
asort(paPrgs)
*
*  create a form from which to select one
*
private pcPRG
pcPRG = space(0)
local loForm
loForm = createobject("frmxxwizPicker")
loForm.Show()
cd (lcCurDir)
IF isnull(pcPRG) OR empty(pcPRG)
  ACTIVATE WINDOW command
  return
ENDIF
*
*  execute the selection
*
xx = "do " + pcPRG
activate window Command   &&& in case VFP 7 dockable windows are up
keyboard xx + chr(13) plain clear
RETURN 



DEFINE CLASS frmxxwizPicker AS FORM

	DataSession = 2
	Top = 0
	Left = 0
	Height = 300
	Icon = "VMP.ICO"
	Width = 520
	DoCreate = .T.
	BorderStyle = 2
	Caption = "Available XXWB*.PRG Wizards/Builders"
	WindowType = 1
	icretval = (space(0))
    MinButton = .f.
    MaxButton = .f.
	Name = "Form1"

	ADD OBJECT list1 AS listbox WITH ;
		ColumnCount = 1, ;
		RowSourceType = 6, ;
		RowSource = "xxwbPRGS.FileName", ;
		Height = 228, ;
		Left = 16, ;
		TabIndex = 1, ;
		Top = 16, ;
		Width = 200, ;
		IntegralHeight = .T., ;
		ItemTips = .t., ;
		Name = "List1" 

	ADD OBJECT edit1 AS editbox WITH ;
		Height = 228, ;
		Left = 228, ;
		ReadOnly = .T., ;
		TabIndex = 2, ;
		Top = 16, ;
		Width = 376, ;
		DisabledBackColor = RGB(255,255,255), ;
		ControlSource = "xxwbPRGS.Comments", ;
		IntegralHeight = .T., ;
		Name = "Edit1"

	ADD OBJECT cmdok AS commandbutton WITH ;
		Top = 262, ;
		Left = 420, ;
		Height = 27, ;
		Width = 84, ;
		Caption = "\<OK", ;
		Cancel = .T., ;
		Default = .T., ;
		TabIndex = 5, ;
		Name = "cmdOK"

	ADD OBJECT cmdRun AS commandbutton WITH ;
		Top = 262, ;
		Left = 16, ;
		Height = 27, ;
		Width = 84, ;
		Caption = "Run", ;
		TabIndex = 3, ;
		Name = "cmdRun"

	ADD OBJECT cmdView AS commandbutton WITH ;
		Top = 262, ;
		Left = 116, ;
		Height = 27, ;
		Width = 84, ;
		Caption = "View", ;
		TabIndex = 4, ;
		Name = "cmdView"

	PROCEDURE Unload
		return THIS.icRetVal
	ENDPROC

	PROCEDURE Init
        WITH THIS
        IF SYSMETRIC(1) > 800
          .List1.Height = .List1.Height + 100
          .Edit1.Height = .List1.Height
        ENDIF
        local lnTopOfButtons, lnMargin
        lnTopOfButtons = .List1.Top + .List1.Height + .List1.Left
        lnMargin = .List1.Left
        .Height = lnTopOfButtons + .cmdOK.Height + lnMargin
        .Width = .Edit1.Left + .Edit1.Width + lnMargin
        .cmdRun.Top = lnTopOfButtons
        .cmdView.Top = lnTopOfButtons
        .cmdOK.Top = lnTopOfButtons
        .cmdRun.Left = lnMargin
        .cmdView.Left = .cmdRun.Left + .cmdRun.Width + lnMargin
        .cmdOK.Left = .Width - .cmdOK.Width - lnMargin
		.AutoCenter = .t.
		.Top = (_Screen.Height-.Height)/4
		ENDWITH
        IF FILE("VMPTiny.GIF")
  		  THIS.AddObject("imgVMP","Image")
		  THIS.imgVMP.Picture = "VMPTiny.GIF"
		  THIS.imgVMP.Left = THIS.cmdOK.Left - THIS.imgVMP.Width - 12
		  THIS.imgVMP.Top = THIS.cmdOK.Top - 6
		  THIS.imgVMP.Visible = .t. 
		ENDIF  
	ENDPROC

	PROCEDURE Load
        local lcSetSafety
        lcSetSafety = set("SAFETY")
        set safety off
		create cursor xxwbJUNK (Junk M)
		create cursor xxwbPRGS (FileName C(40), Comments M)
		local xx
		FOR xx = 1 to alen(paPRGs,1)
          IF paPRGs[xx,1] # "XXWB.PRG"
    		  insert into xxwbPRGS (FileName) values (paPRGs[xx,1])
    		  THIS.GetComments(paPRGs[xx,1])
	      ENDIF
		ENDFOR
        use in xxwbJUNK
        IF lcSetSafety = "ON"
          set safety on
        ENDIF
        select xxwbPRGS
        index on FileName tag FileName
		LOCATE 
	ENDPROC

*!*	    PROCEDURE GetComments
*!*	        lParameters tcFileName
*!*	        select xxwbJUNK
*!*	        zap
*!*	        append blank
*!*	        append memo Junk from (tcFileName)
*!*	        local lnSetMemoWidth, lnStart, lnCount
*!*	        lnCount = 0
*!*	        lnSetMemoWidth = set("MEMOWIDTH")
*!*	        set memowidth to 200
*!*	        _mline = 0
*!*	        local lnLineCount, lcLine, llValidLine
*!*	        FOR lnLineCount = 1 to memlines(xxwbJUNK.Junk)
*!*	          lcLine = mline(xxwbJUNK.Junk,1,_mline)
*!*	          IF lcLine == "*" OR empty(lcLine)
*!*	            lcLine = space(0)
*!*	           ELSE
*!*	            lcLine = substrc(lcLine,2)
*!*	            lcLine = space(1) + alltrim(lcLine)
*!*	            IF lnCount = 1
*!*	              lcLine = ALLTRIM(lcLine)
*!*	            ENDIF
*!*	          ENDIF
*!*	          IF UPPER(tcFileName) $ UPPER(lcLine) ;
*!*	               AND NOT llValidLine
*!*	            lnStart = AT_C(UPPER(tcFileName),UPPER(lcLine))
*!*	            replace XXWBPRGS.FileName WITH ;
*!*	                SUBSTRC(lcLine,lnStart,LENC(tcFileName)) ;
*!*	                IN XXWBPRGS            
*!*	            llValidLine = .t.
*!*	          ENDIF
*!*	          IF llValidLine
*!*	            IF EMPTY(lcLine)
*!*	              EXIT 
*!*	            ENDIF
*!*	            lnCount = lnCount + 1
*!*	            replace xxwbPRGS.Comments ;
*!*	                 with xxwbPRGS.Comments + lcLine + IIF(lnCount=1,CHR(13)+CHR(10),SPACE(0)) ;
*!*	                 in xxwbPRGS
*!*	          ENDIF  
*!*	        ENDFOR
*!*	        replace xxwbPRGS.Comments ;
*!*	             with alltrim(xxwbPRGS.Comments) ;
*!*	             in xxwbPRGS
*!*	        set memowidth to lnSetMemoWidth
*!*	    ENDPROC
    
    PROCEDURE GetComments
        LPARAMETERS tcFileName
        LOCAL lcLine, laLines[1], lnCount, llNewP, llCopyRight
        lnCount = 0
        ALINES(laLines,FILETOSTR(tcFileName),.t.)
        FOR EACH lcLine IN laLines
          DO CASE
            CASE UPPER(ALLTRIM(CHRTRAN(lcLine,"*",SPACE(0)))) = "COPYRIGHT"
              *  skip the Copyright block
              llCopyRight = .t.
              LOOP 
            CASE llCopyRight AND NOT EMPTY(ALLTRIM(CHRTRAN(lcLine,"*",SPACE(0))))
              *  Copyright line
              LOOP 
            CASE llCopyRight AND lcLine == "*"
              *  first blank line after the Copyright
              llCopyRight = .f.
              LOOP 
            CASE lcLine == "*" AND EMPTY(XXWBPRGS.Comments)
              *  ignore initial comment lines
              LOOP 
            CASE lcLine == "*"
              *  carriage return/line feed (blank line)
              replace XXWBPRGS.Comments WITH ;
                      XXWBPRGS.Comments + ;
                      CHR(13) + CHR(10) + ;
                      CHR(13) + CHR(10) 
              llNewP = .t.
              LOOP 
            CASE EMPTY(lcLine) ;
                 OR UPPER(ALLTRIM(lcLine)) = "LPARAMETERS " ;
                 OR NOT lcLine = "*"
              *  first line of real code, we're outta here
              EXIT 
            CASE lcLine = "*!*"
              *  code comment line
              replace XXWBPRGS.Comments WITH ;
                      XXWBPRGS.Comments + ;
                      CHR(13) + CHR(10) + ;
                      lcLine 
              LOOP 
            OTHERWISE
              lcLine = ALLTRIM(SUBSTRC(lcLine,2))
              DO CASE
                CASE lcLine = "- " ;
                     OR ;
                     (ISDIGIT(LEFTC(lcLine,1)) AND SUBSTRC(lcLine,2,1) = "-")
                  *  itemized list item
                  replace XXWBPRGS.Comments WITH ;
                          XXWBPRGS.Comments + ;
                          CHR(13) + CHR(10) + ;
                          CHR(13) + CHR(10) + ;
                          lcLine 
                  LOOP 
                CASE llNewP
                  *  first line after a blank line
                  lcLine = ALLTRIM(lcLine)
                  llNewP = .f.
                OTHERWISE
                  * regular line
                  lcLine = SPACE(1) + lcLine
                  llNewP = .f.
              ENDCASE
          ENDCASE
          IF UPPER(ALLTRIM(lcLine)) == UPPER(tcFileName) 
            *
            *  update XXDTPRGS.FileName with the
            *  .PRG filename in the header comments,
            *  to get better capitalization
            *   
            replace XXWBPRGS.FileName WITH ALLTRIM(lcLine) IN XXWBPRGS
            llNewP = .t.   
          ENDIF
          lnCount = lnCount + 1
          replace XXWBPRGS.Comments ;
               WITH XXWBPRGS.Comments + lcLine + IIF(lnCount=1,CHR(13)+CHR(10),SPACE(0)) ;
               IN XXWBPRGS
        ENDFOR
        replace XXWBPRGS.Comments ;
             WITH ALLTRIM(XXWBPRGS.Comments) ;
             IN XXWBPRGS
    ENDPROC

    PROCEDURE List1.Init
	  THIS.Value = 1  
    ENDPROC

    PROCEDURE List1.InteractiveChange
      THISFORM.Edit1.Refresh()
      *
      *  once we make the first change, put the
      *  filename in the form.Caption
      *
      THISFORM.Caption = ALLTRIM(XXWBPRGS.FileName)
    ENDPROC

    PROCEDURE List1.Valid
      THISFORM.cmdRun.SetFocus()
      keyboard "{ENTER}" plain clear
    ENDPROC

	PROCEDURE cmdok.Click
        pcPRG = .NULL.
		THISFORM.icRetVal = .NULL.
		THISFORM.Release()
	ENDPROC

	PROCEDURE cmdRun.Click
        pcPRG = THISFORM.List1.DisplayValue
		THISFORM.icRetVal = space(0)
		THISFORM.Release()
	ENDPROC

    PROCEDURE cmdView.Click
      modify command (ALLTRIM(XXWBPRGS.FileName)) nomodify nomenu range 1,1
    ENDPROC

ENDDEFINE