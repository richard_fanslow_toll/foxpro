*
*  XXDTHier.PRG
*  Print out a listing of the class hierarchies for all
*  classes in .VCXs contained in a particular directory.
*
*  Note that if those classes are subclasses, only the parent
*  classes for the classes in the .VCXs in the indicated directory
*  are included -- classes in the .VCXs containing the parent
*  classes are not included unless they provide inheritance to
*  the classes in the .VCXs in the selected directory.
*
*  Copyright (c) 1997-2005 Visionpace   All Rights Reserved
*                17501 East 40 Hwy., Suite 218
*                Independence, MO 64055
*                816-350-7900 
*                http://www.visionpace.com
*                http://vmpdiscussion.visionpace.com
*  Author:  Drew Speedie  
*

IF file("XXPROCDT.PRG")
  do SavePJXs in XXPROCDT.PRG
ENDIF

TRY
CLEAR ALL
CLOSE ALL
CATCH
ENDTRY 

IF NOT X8SETCLS("XXFW.VCX")
  messagebox("Unable to find/load XXFW.VCX",16,"Please Note")
  PopPJXs()
  return
ENDIF
IF NOT X8SETCLS("XXTOOLS.VCX")
  messagebox("Unable to find/load XXTOOLS.VCX",16,"Please Note")
  PopPJXs()
  return
ENDIF
IF NOT X8SETCLS("XXFWFRM.VCX")
  messagebox("Unable to find/load XXFWFRM.VCX",16,"Please Note")
  PopPJXs()
  return
ENDIF
IF NOT FILE("XXDTHIER.FRX")
  messagebox("Unable to find XXDTHIER.FRX",16,"Please Note")
  PopPJXs()
  return
ENDIF
IF NOT FILE("X8WRPFRM.PRG") AND NOT FILE("X8WRPFRM.FXP")
  messagebox("Unable to find X8WRPFRM.PRG",16,"Please Note")
  PopPJXs()
  return
ENDIF

*
*  XXDTHIER_FILES and paBaseClasses are created now
*  and are available when XXTOOLS.VCX/frmXXDTHIER
*  instantiates, so that it can populate them
*  for use here
*
*  paBaseClasses/Column2 is for use as a flag 
*  indicating that only the flagged base classes
*  should be included 
*
create cursor XXDTHIER_FILES (FileName C(254))
private paBaseClasses[21,2]
dimension paBaseClasses[21,2]
paBaseClasses = .f.
paBaseClasses[1,1] = "Checkbox"
paBaseClasses[2,1] = "Combobox"
paBaseClasses[3,1] = "CommandButton"
paBaseClasses[4,1] = "Container"
paBaseClasses[5,1] = "Custom"
paBaseClasses[6,1] = "Editbox"
paBaseClasses[7,1] = "Form"
paBaseClasses[8,1] = "Grid"
paBaseClasses[9,1] = "Image"
paBaseClasses[10,1] = "Label"
paBaseClasses[11,1] = "Line"
paBaseClasses[12,1] = "Listbox"
paBaseClasses[13,1] = "OLEBoundControl"
paBaseClasses[14,1] = "OLEContainer"
paBaseClasses[15,1] = "OptionGroup"
paBaseClasses[16,1] = "PageFrame"
paBaseClasses[17,1] = "Shape"
paBaseClasses[18,1] = "Spinner"
paBaseClasses[19,1] = "Textbox"
paBaseClasses[20,1] = "Timer"
paBaseClasses[21,1] = "Toolbar"

*
*  selection mechanism goes here, populates XXDTHIER_FILES
*
*  for now it's a simple GETDIR()
*
*  On the drawing board:
*  Create a new XXDTHIER form class (modal, default data session) to 
*  allow the following options:
*     1- All .VCXs in a directory 
*     2- All .VCXs and .SCXs in a directory
*     3- All .SCXs in a directory
*     4- Project
*     Filter by VFP base class
*         Multi-select listbox to allow filtering by one or more VFP base classes
*     Filter by files
*         Multi-select listbox to allow specifying a subset of the 
*         .VCXs/.SCXs in main options #1-#4
*     If XXFW*.VCXs are selected, and XXTOOLS.VCX/XXWB.VCX are included
*         (because they're in the same directory), ask if the user
*         really means to include them -- they're not part of the 
*         XXFW*.VCX framework
*     Also:
*       Screen/Printer/HTML
*

*********************************************************
*********************************************************
*********************************************************
local lcDir
lcDir = getdir(FULLPATH(CURDIR()), ;
               "Select the folder containing the .VCXs " + ;
               "for which you want to create the class " + ;
               "hierarchy listing.", ;
               "Folder for .VCXs", ;
               1+64)
IF empty(lcDir)
  clear all
  close all
  PopPJXs()
  return
ENDIF
local lcCurDir
lcCurDir = FULLPATH(CURDIR())
cd (lcDir)
local array laFiles[1,2]
IF adir(laFiles,"*.VCX") > 0
 ELSE
  cd (lcCurDir)
  messagebox("No .VCXs in " + lcDir,48,"Please Note")
  clear all
  close all
  PopPJXs()
  return
ENDIF
local xx
FOR xx = 1 to alen(laFiles,1)
  IF !"XXWB.VCX" $ upper(laFiles[xx,1]) ;
       AND !"XXTOOLS.VCX" $ upper(laFiles[xx,1]) ;
       AND !"XXZFUN.VCX" $ upper(laFiles[xx,1]) ;
       AND !"XXOTHER.VCX" $ upper(laFiles[xx,1])
    insert into XXDTHIER_FILES ;
         (FileName) ;
         values ;
         (ADDBS(FULLPATH(CURDIR()))+laFiles[xx,1])
  ENDIF
ENDFOR
cd (lcCurDir)
release laFiles


*********************************************************
*********************************************************
*********************************************************


local lcSafety, lcExclusive, lcDeleted
lcSafety = set("SAFETY")
lcExclusive = set("EXCLUSIVE")
lcDeleted = set("DELETED")
set safety off
set exclusive off
set deleted on

*
*  see if no base classes have been flagged in 
*  frmXXDTHIER/paBaseClasses, indicating that
*  the user does not want to filter the list
*  by base class(es)
*
local llAllBaseClasses
llAllBaseClasses = .t.
FOR xx = 1 to alen(paBaseClasses,1)
  IF paBaseClasses[xx,2] = .t.
    llAllBaseClasses = .f.
    exit
  ENDIF
ENDFOR

*
*  selection has been made, XXDTHIER_FILES populated
*  and paBaseClasses populated
*
create cursor XXDTHIER ;
     (BaseClass C(15), ;
      ClassName C(60), ;
      ClassLib  C(254), ;
      Level     I, ;
      ParentClassName C(60), ;
      ParentClassLib  C(254), ;
      Hierarchy C(225), ;
      ReportLine C(120), ;
      ReportLine2 C(100), ;
      PageBreak I, ;
      VMP L)
select XXDTHIER_FILES
local lcFile, llError
*
*  scan .VCX/.SCX files in XXDTHIER_FILES and
*  add one record to XXDTHIER per class in those files
*
SCAN FOR inlist(rightc(alltrim(FileName),4),".SCX",".VCX")
  lcFile = alltrim(XXDTHIER_FILES.FileName)
  wait window "Processing classes in " + lcFile + "..." nowait
  on error llError = .t.
  use (lcFile) in 0 again alias METAFILE shared
  on error
  IF llError
    exit
  ENDIF
  select METAFILE
  *
  *  scan each class in this .VCX/.SCX
  *
  SCAN FOR empty(Parent) and !empty(Class)
    IF llAllBaseClasses OR MeetsBaseClassFilter(METAFILE.BaseClass)
      insert into XXDTHIER (BaseClass, ;
                            ClassName, ;
                            ClassLib, ;
                            Level, ;
                            ParentClassName, ;
                            ParentClassLib) ;
                           values ;
                           (alltrim(METAFILE.BaseClass), ;
                            alltrim(METAFILE.ObjName), ;
                            lcFile, ;
                            0, ;
                            alltrim(METAFILE.Class), ;
                            alltrim(METAFILE.ClassLoc))
    ENDIF
  ENDSCAN && METAFILE
  use in METAFILE
  select XXDTHIER_FILES
ENDSCAN && XXDTHIER_FILES
use in XXDTHIER_FILES
wait clear

IF llError
  messagebox("Unable to access " + lcFile,16,"Please Note")
  set safety &lcSafety
  set exclusive &lcExclusive
  set deleted &lcDeleted
  wait clear
  PopPJXs()
  return
ENDIF

*
*  now pull in the classes from which
*  those classes inherit
*
select XXDTHIER
replace all Level with 999
local lcParentClassName, lcParentClassLib, lnRecno
SCAN FOR Level = 999
  *
  *  nothing to do if we're dealing with a
  *  class at the top of the hierarchy
  *
  IF empty(XXDTHIER.ParentClassLib)
    loop
  ENDIF
  wait window "Processing " + alltrim(XXDTHIER.ClassName) + " and its parent classes..." nowait
  lnRecno = recno()
  lcParentClassName = upper(alltrim(XXDTHIER.ParentClassName))
  lcParentClassLib = upper(alltrim(XXDTHIER.ParentClassLib))
  IF !PullInClasses(lcParentClassName,lcParentClassLib)
    llError = .t.
    exit
  ENDIF
  select XXDTHIER
  goto (lnRecno)
ENDSCAN
wait clear

IF llError
  messagebox("Unable to access " + lcParentClassLib,16,"Please Note")
  set safety &lcSafety
  set exclusive &lcExclusive
  set deleted &lcDeleted
  PopPJXs()
  return
ENDIF

select XXDTHIER
replace all Level with 1

*
*  populate the Level and Hierarchy fields
*
select XXDTHIER
local lcPCLib, lcPCName, lnCount, lnNewRecno
SCAN 
  IF empty(ParentClassLib)
    *
    *  we're at a VFP base class, nothing to do
    *  
    loop
  ENDIF
  wait window "Determining the hierarchy for " + alltrim(XXDTHIER.ClassName) + "..." nowait
  lnRecno = recno()
  lnCount = 1
  DO WHILE !empty(XXDTHIER.ParentClassLib)
    lnCount = lnCount + 1
    lcPCLib = upper(justfname(XXDTHIER.ParentClassLib))
    lcPCName = upper(alltrim(XXDTHIER.ParentClassName))
    locate for upper(alltrim(XXDTHIER.ClassName)) == lcPCName ;
           AND upper(justfname(XXDTHIER.ClassLib)) == lcPCLib
    lnNewRecno = recno()
    goto (lnRecno)
    replace XXDTHIER.Hierarchy with substrc(lcPCName,4) + "-" + XXDTHIER.Hierarchy
    goto (lnNewRecno)
  ENDDO
  goto (lnRecno)    
  replace Level with lnCount
ENDSCAN
SCAN
  *replace XXDTHIER.Hierarchy with alltrim(XXDTHIER.Hierarchy) + upper(XXDTHIER.ClassName)
  *replace XXDTHIER.Hierarchy with rtrim(XXDTHIER.Hierarchy) + padl(recno(),4)
  *replace XXDTHIER.Hierarchy with alltrim(XXDTHIER.Hierarchy) + soundex(XXDTHIER.ClassName)
  replace XXDTHIER.Hierarchy with alltrim(XXDTHIER.Hierarchy) + substrc(upper(XXDTHIER.ClassName),4)
ENDSCAN

wait window "Preparing data for printing..." nowait

*
*  prepare the XXDTHIER.ReportLine field containing
*  the report contents
*
select XXDTHIER
index on BaseClass + Hierarchy tag Junk

local lnIndentLength, lcCName, lcCLib, lcLine, lnSpace
SCAN
  lnIndentLength = IIF(XXDTHIER.Level>1,(XXDTHIER.Level-1)*3,0) + 2
  lcCName = LOWER(ALLTRIM(XXDTHIER.ClassName))
  lcCLib = UPPER(JUSTFNAME(XXDTHIER.ClassLib))
  replace PageBreak with 1
*!*	  replace ReportLine with SPACE(lnIndentLength) + ;
*!*	                          PADR(lcCName,79-lnIndentLength,".") + ;
*!*	                          lcCLib
*!*	  replace ReportLine with SPACE(lnIndentLength) + ;
*!*	                          PADR(lcCName,67-lnIndentLength,".") + ;
*!*	                          lcCLib
  lnSpace = 92-lnIndentLength-LENC(lcCName)-25   &&& 25 is the space for lcCLib
  IF lnSpace < 3
    DO WHILE lnSpace < 3
      lcCName = LEFTC(lcCName,LENC(lcCName)-1)
      lnSpace = 92-lnIndentLength-LENC(lcCName)-25   &&& 25 is the space for lcCLib
    ENDDO
    lcCName = LEFTC(lcCName,LENC(lcCName)-3) + "---"
  ENDIF
  IF LENC(lcCLib) > 25
    lcCLib = JUSTSTEM(lcCLib)
    IF LENC(lcCLib) > 25
      lcCLib = LEFTC(lcCLib,25)
    ENDIF
  ENDIF
  lcLine = SPACE(lnIndentLength) + ;
           lcCName + ;
           REPLICATE(".",lnSpace) + ;
           lcCLib
  replace ReportLine WITH lcLine
*!*	  replace ReportLine2 with SPACE(lnIndentLength) + ;
*!*	                           lcCName + SPACE(2) + ;
*!*	                           lcCLib + " 1"
  IF lcCLib = "XXFW" OR lcCLib = "XXXX"
    replace VMP with .t.
  ENDIF
ENDSCAN

*
*  add 2 blank lines after each baseclass
*
locate
local lcBC, lcNewBC
lcBC = XXDTHIER.BaseClass 
lcNewBC = space(0)
SCAN
  lnRecno = recno()
  IF !XXDTHIER.BaseClass == lcBC
    lcNewBC = XXDTHIER.BaseClass
    insert into XXDTHIER (BaseClass,Hierarchy) values (lcBC,"ZZZZZZZZZZZZZ")    
    insert into XXDTHIER (BaseClass,Hierarchy) values (lcBC,"ZZZZZZZZZZZZZ")    
    locate for XXDTHIER.BaseClass == lcNewBC
    lcBC = XXDTHIER.BaseClass
  ENDIF
ENDSCAN

*
*  determine page breaks
*
local lcBC, lnPageTotal, lnBCCount, lnPageBreak, lnPageMax
lcBC = "!!!!!"
lnPageTotal = 0
lnPageBreak = 1
lnPageMax = 42
SCAN
  lnRecno = recno()
  IF !XXDTHIER.BaseClass == lcBC
    lcBC = XXDTHIER.BaseClass
    *
    *  new base class
    *
    count to lnBCCount for XXDTHIER.BaseClass == lcBC
    goto (lnRecno)
    IF lnBCCount + lnPageTotal + 1 <= lnPageMax
      *
      *  print these on the same page
      *
      lnPageTotal = lnPageTotal + 1   &&& group header
     ELSE
      *
      *  start a new page
      *
      lnPageBreak = lnPageBreak + 1
      lnPageTotal = 1   &&& group header
    ENDIF
  ENDIF
  lnPageTotal = lnPageTotal + 1
  IF lnPageTotal = lnPageMax
    lnPageTotal = 0
    lnPageBreak = lnPageBreak + 1
  ENDIF
  replace XXDTHIER.PageBreak with lnPageBreak  
ENDSCAN


LOCAL lcDestination, llViewNow
*lcDestination = X8WRPFRM("frmReportDestinationSPF,XXFWFRM.VCX",".f.,.f.,[VP]")    
EXTERNAL CLASS XXFWFRM.VCX
lcDestination = X8WRPFRM("frmReportDestinationSPF,XXFWFRM.VCX") 

DO CASE
  CASE ISNULL(lcDestination)
    *
    *  <Cancel> from the dialog
    *
  CASE lcDestination = "SCREEN"
    REPORT FORM xxdthier TO PRINTER PROMPT PREVIEW
  CASE lcDestination = "PRINTER"
    REPORT FORM xxdthier TO PRINTER PROMPT NOCONSOLE 
  OTHERWISE
    LOCAL lcFileName, ln_ASCIICols
    lcFileName = SUBSTRC(lcDestination,AT_C(",",lcDestination)+1)
    ln_ASCIICols = _ASCIICols
    _ASCIICols = 110
    REPORT FORM xxdthier NOCONSOLE TO FILE (lcFileName) ASCII
    _ASCIICols = ln_ASCIICols
    llViewNow = MESSAGEBOX("View " + lcFileName + " now?",4+48,"View now?") = 6
ENDCASE

USE IN SELECT("XXDTHIER")
SET SAFETY &lcSafety
SET EXCLUSIVE &lcExclusive
SET DELETED &lcDeleted
PopPJXs()
IF llViewNow
  MODIFY FILE (lcFileName) NOWAIT 
ENDIF
RETURN 



PROCEDURE PullInClasses
lParameters tcParentClassName, tcParentClassLib
local lcParentClassName, lcParentClassLib, llError
store 0 to lcParentClassName, lcParentClassLib
select XXDTHIER
locate for upper(alltrim(XXDTHIER.ClassName)) == tcParentClassName ;
         and upper(justfname(XXDTHIER.ClassLib)) == upper(justfname(tcParentClassLib))
IF !found()
  on error llError = .t.
  use (tcParentClassLib) in 0 shared alias ParentClassLib
  on error
  IF !llError
    select ParentClassLib
    locate for upper(alltrim(ObjName)) == upper(alltrim(tcParentClassName)) ;
           and empty(Parent) AND !empty(Class)
    insert into XXDTHIER (BaseClass, ;
                          ClassName, ;
                          ClassLib, ;
                          ParentClassName, ;
                          ParentClassLib) ;
                         values ;
                         (alltrim(ParentClassLib.BaseClass), ;
                          alltrim(ParentClassLib.ObjName), ;
                          tcParentClassLib, ;
                          alltrim(ParentClassLib.Class), ;
                          alltrim(ParentClassLib.ClassLoc))
    lcParentClassName = upper(alltrim(ParentClassLib.Class)) 
    lcParentClassLib = upper(alltrim(ParentClassLib.ClassLoc))
    use in ParentClassLib
  ENDIF
ENDIF
IF !llError AND !empty(lcParentClassLib)
  *
  *  keep on going upwards thru the hierarchy
  *
  PullInClasses(lcParentClassName,lcParentClassLib)
ENDIF
return !llError



PROCEDURE MeetsBaseClassFilter
*
*  see if the passed base class is one the
*  user wants to see
*
lparameters tcBaseClass
local llBaseClass, llOK, xx
lcBaseClass = upper(alltrim(tcBaseClass))
llOK = .t.
FOR xx = 1 to alen(paBaseClasses,1)
  IF paBaseClasses[xx,2] = .t.
    llOK = .f.
    exit
  ENDIF
ENDFOR
IF llOK
  *
  *  no base classes have be specified --
  *  all base classes qualify
  *
  return .t.
ENDIF
FOR xx = 1 to alen(paBaseClasses,1)
  IF upper(lcBaseClass) == upper(paBaseClasses[xx,1])
    llOK = paBaseClasses[xx,2]
    exit
  ENDIF       
ENDFOR
return llOK


PROCEDURE PopPJXs
IF file("XXPROCDT.PRG")
  do RestorePJXs in XXPROCDT.PRG
ENDIF
return
