*
*  XXFWNT.PRG
*  This procedure file contains:
*
*  Abstract classes based on the VFP Session class, for use
*  as COM servers, specifically a COM wrapper for a
*  VMP BusinessObjectManager
*    sesNTBase
*      sesNTCOMServer
*        sesCOMWrapper
*

*
*  The classes defined here only compile in VFP 7.0 or 
*  higher, because they use a number of COM-specific 
*  enhancements made in VFP 7.0
*  

**********************************************************
**********************************************************
DEFINE CLASS sesNTBase AS sesNTBaseVMPSuperClass OF XXXXNT.PRG
**********************************************************
**********************************************************
*
*  this abstract class is the parent class of all VMP NT 
*  session classes
*
Name = "sesNTBase"
PROTECTED icStartPath
icStartPath = SPACE(0)

*////////////////////////////////////////////////////////
PROTECTED PROCEDURE Init
*
*  XXFWNT.PRG/sesNTBase::Init()
*
IF NOT DODEFAULT()
  RETURN .f.
ENDIF
*
*  add the ilCOM property via AddProperty(), which
*  renders it 'invisible' in the type library and
*  IntelliSense, essentially the same as a Protected
*  property, because the COM client can't 'see' it,
*  this technique allows the VMPNTInitDevHook.PRG
*  called in THIS.InitDevHook() to set THIS.ilCOM
*  for testing purposes
*  
THIS.AddProperty("ilCOM",.f.)
THIS.AddProperty("ilDiagnosticMode",.f.)
THIS.AddProperty("ilCOMLogCoverage",.f.)
THIS.InitDevHook()
*
*  it doesn't matter what THIS.InitDevHook() might
*  have set THIS.ilCOM to, if this is a COM object,
*  ensure that THIS.icCOM is set to .T.
*
THIS.ilCOM = X6ISCOM()
EXTERNAL CLASS XXFWUTIL.VCX
IF NOT "\XXFWUTIL.VCX" $ SET("CLASSLIB") ;
     AND NOT X8SETCLS("XXFWUTIL.VCX")
  ASSERT .f. message PROGRAM() + ;
       " is unable to SET CLASSLIB TO XXFWUTIL ADDITIVE"
  RETURN .f.
ENDIF
*
*  shell method
*
IF NOT THIS.Setup1()
  RETURN .f.
ENDIF
*
*  setup the environment for this data session,
*  the most obvious of which is executing SETs
*  that are scoped to private data sessions
*
IF NOT THIS.SetupPrivateDataSession()
  RETURN .f.
ENDIF
*
*  shell method
*
IF NOT THIS.Setup2()
  RETURN .f.
ENDIF
RETURN .t.
ENDPROC
*////////////////////////////////////////////////////////



*////////////////////////////////////////////////////////
PROTECTED PROCEDURE Setup1
*
*  XXFWNT.PRG/sesNTBase::Setup1()
*
*  this is a shell method called from THIS.Init()
*
IF THIS.ilCOMLogCoverage
  XXLOGGER(PROGRAM(),.t.,.t.)
ENDIF
ENDPROC
*////////////////////////////////////////////////////////


*////////////////////////////////////////////////////////
PROTECTED PROCEDURE SetupPrivateDataSession
*
*  XXFWNT.PRG/sesNTBase::SetupPrivateDataSession()
*
*  called from THIS.Init() after it calls THIS.Setup1()
*
*  setup the private data session for this COM object,
*  primarily to SET the SET commands scoped to a 
*  private data session
*
*  you might augment this method to add a SET PATH 
*  statement, ensuring, for example, that your data is
*  in the VFP path (or you could instead code the
*  SET PATH in THIS.Setup1() or THIS.Setup2() shell
*  methods)
*
*  these SET commands are set to match those in 
*  XXFW.VCX/ctrApp::SetupSets()
*    XXDTES("XXFW.VCX","IF THIS.RunTime()","ctrApp","SetupSets")
*  XXFW.VCX/cusPrivateDataSessionSets::ExplicitSettings()
*    XXDTES("XXFW.VCX","SetupPrivateDataSession","cusPrivateDataSessionSets","ExplicitSettings")
*
IF THIS.ilCOMLogCoverage
  XXLOGGER(PROGRAM(),.t.,.t.)
ENDIF
SET CENTURY ON 
SET CENTURY TO  
SET CPDIALOG OFF
SET DELETED ON 
SET EXCLUSIVE OFF 
SET HOURS TO 24
SET MULTILOCKS ON  
SET NOTIFY OFF 
SET RESOURCE OFF 
SET SAFETY OFF 
SET TALK OFF 
THIS.icStartPath = ADDBS(JUSTPATH(Application.ServerName))
IF THIS.ilCOMLogCoverage
  XXLOGGER("THIS.icStartPath="+THIS.icStartPath,.t.)
ENDIF
ENDPROC
*////////////////////////////////////////////////////////


*////////////////////////////////////////////////////////
PROTECTED PROCEDURE Setup2
*
*  XXFWNT.PRG/sesNTBase::Setup2()
*
*  this is a shell method called from THIS.Init()
*  after it calls THIS.SetupPrivateDataSession()
*
IF THIS.ilCOMLogCoverage
  XXLOGGER(PROGRAM(),.t.,.t.)
ENDIF
ENDPROC
*////////////////////////////////////////////////////////


*////////////////////////////////////////////////////////
PROTECTED PROCEDURE InitDevHook
*
*  XXFWNT.PRG/sesNTBase::InitDevHook()
*
*  this method is primarily intended to allow you to set 
*  THIS.ilCOM to .T. in development, to test VMP middle-tier
*  objects at the Command Window in native VFP, as if they 
*  were COM objects, making it easier to debug them
*
*  so you can test this object as if it was a COM object
*  before building an actual COM object
*
*  See XXFWNT.VCX/cusNTBase::InitDevHook() and
*      XXFWNT.VCX/ctrNTBase::InitDevHook()
*  MODIFY CLASS cusNTBase OF XXFWNT METHOD InitDevHook
*  MODIFY CLASS ctrNTBase OF XXFWNT METHOD InitDevHook
*
*!*	IF NOT _VFP.StartMode = 0
*!*	  *
*!*	  *  the current VFP session is not a development session
*!*	  *
*!*	  RETURN
*!*	ENDIF

IF TYPE("THISFORM.BaseClass") = "C"
  *
  *  this object is a member of a VFP (VMP) form
  *
  RETURN
ENDIF

IF VARTYPE(m.oLib) = "O"
  *
  *  oLib exists - a VMP desktop app is running or a 
  *  standalone form is running at the Command Window
  *
  RETURN
ENDIF

IF VARTYPE(m.oApp) = "O"
  *
  *  oApp exists - a VMP desktop app is running
  *
  RETURN
ENDIF

IF FILE("VMPNTInitDevHook.PRG")
  DO ("VMPNTInitDevHook") WITH THIS
ENDIF
ENDPROC
*////////////////////////////////////////////////////////


*////////////////////////////////////////////////////////
PROTECTED PROCEDURE ORCleanup
*
*  XXFWNT.PRG/sesNTBase::ORCleanup()
*
*  object reference cleanup on THIS.Destroy()
*
ENDPROC
*////////////////////////////////////////////////////////


*!*	*////////////////////////////////////////////////////////
*!*	PROTECTED PROCEDURE Error(nError, cMethod, nLine) 
*!*	*
*!*	*  XXFWNT.PRG/sesNTBase::Error()
*!*	*
*!*	COMRETURNERROR(cMethod + ;
*!*	               "  Err#=" + STR(nError,5) + ;
*!*	               "  Line=" + STR(nline,6) + ;
*!*	               " " + MESSAGE(), ;
*!*	               _VFP.ServerName)
*!*	ENDPROC
*!*	*////////////////////////////////////////////////////////


*////////////////////////////////////////////////////////
PROTECTED PROCEDURE Destroy
*
*  XXFWNT.PRG/sesNTBase::Destroy()
*
THIS.ORCleanup()
ENDPROC
*////////////////////////////////////////////////////////

**********************************************************
**********************************************************
ENDDEFINE
**********************************************************
**********************************************************



**********************************************************
**********************************************************
DEFINE CLASS sesNTCOMServer as sesNTBase OF XXFWNT.PRG
**********************************************************
**********************************************************
*
*  this abstract class defines the interface for VMP COM 
*  servers
*
*  use an intermediate/app-specific subclass of this class
*  as a 'wrapper' for business object (or any other kind of)
*  COM servers
*
*  at the level where a subclass of this class is actually 
*  compiled into a COM server, add OLEPUBLIC to that DEFINE 
*  CLASS definition
*
Name = "sesNTCOMServer"

*////////////////////////////////////////////////////////
PROTECTED PROCEDURE GetMTSContext
*
*  XXFWNT.PRG/sesNTCOMServer::GetMTSContext()
*
*  return an MTS context object for use in 
*  THIS.SetMTSContext()
*
*  call this method manually, something like this:
*    local loMTS, llSuccess
*    loMTS = THIS.GetMTSContext()
*    llSuccess = <transaction processing here>
*    THIS.SetMTSContext(loContext, llSuccess)
*
LOCAL llError, loReturn

TRY
*
*  the following should always succeed, even when not
*  running under MTS, but I've done error trapping anyway
*
loReturn = CREATEOBJECT("MTXAS.APPSERVER.1")
CATCH
llError = .t.
ENDTRY

IF m.llError
  loReturn = .NULL.
 ELSE 
  *
  *  if this object is not running under MTS, loReturn
  *  gets set to .NULL. here
  *
  loReturn = loReturn.GetObjectContext()
ENDIF

RETURN m.loReturn
ENDPROC
*////////////////////////////////////////////////////////

*////////////////////////////////////////////////////////
PROTECTED PROCEDURE SetMTSContext(toContext, tlComplete)
*
*  XXFWNT.PRG/sesNTCOMServer::SetMTSContext()
*
*
*  execute SetComplete() or SetAbort(), as indicated by 
*  tlComplete
*
*  call this method manually, something like this:
*    local loMTS, llSuccess
*    loMTS = THIS.GetMTSContext()
*    llSuccess = <transaction processing here>
*    THIS.SetMTSContext(loContext, llSuccess)
*      
IF NOT VARTYPE(m.toContext) = "O"
  *
  *  this server is not running under MTS -- see 
  *  THIS.GetMTSContext()
  *
  RETURN .t.
ENDIF

IF m.tlComplete
  toContext.SetComplete()
 ELSE
  toContext.SetAbort()
ENDIF

RETURN .t.
ENDPROC
*////////////////////////////////////////////////////////

**********************************************************
**********************************************************
ENDDEFINE
**********************************************************
**********************************************************



**********************************************************
**********************************************************
DEFINE CLASS sesCOMWrapper as sesNTCOMServer OF XXFWNT.PRG
**********************************************************
**********************************************************
*
*  this abstract class is a 'wrapper' of an instance of
*  XXFWNTBO.VCX/ctrBusinessObjectManager, where one or more
*  business objects (XXFWNTBO.VCX/ctrBusinessObject) are
*  aggregated into a single COM server interface
*
*  please note that while none of the business objects 
*  contained in the ctrBusinessObjectManager are exposed 
*  here, you can certainly do so by adding custom properties 
*  in subclasses of this class, and storing object references 
*  to specific business objects you want to expose to the 
*  COM client:
*    Dim oInvoice 
*	 Set oInvoice = Server.CreateObject("Project.InvoiceBO")
*    oInvoice.Method()
*    oInvoice.oCustomer.Method()
*    oInvoice.oLineItem.Method()
*    oInvoice.oSomeOtherBO.Method()
*  for more details, see the header comments of
*  THIS.LoadBOMgr() and THIS.LoadBO()
*
*  specify OLEPublic in the subclass that will actually be
*  instantiated as a COM server
*
*  to create a concrete subclass of this class, for 
*  your COM object(s), you can use the custom VMP 
*  builder XXWBCOMWrapper.PRG
*  
*
Name = "sesCOMWrapper"
PROTECTED icBOMgrClass
icBOMgrClass = "ctrBusinessObjectManager,XXFWNTBO.VCX"
PROTECTED ioBOMgr 
ioBOMgr = .NULL.

*////////////////////////////////////////////////////////
PROTECTED PROCEDURE Init
*
*  XXFWNT.PRG/sesCOMWrapper::Init()
*
*  call THIS.LoadBOMgr() and THIS.LoadBO()
*
*  this method is similar in functionality to
*  XXFW.VCX/frmData::Init() -- this class provides the
*  private data session in much the same way frmData 
*  does in a desktop application
*
*  this method is a template method and not intended
*  for augmentation in subclasses -- we expect you to
*  do your augmentation in the methods called from this
*  method, or the called by these methods (like
*  THIS.SetupPrivateDataSession, called from THIS.Setup)
*
*
*  there is no message handling here because you can't 
*  pass a parameter to the Init() of a COM server, and 
*  since this object won't instantiate, you can't even 
*  store a message to a property that the client can 
*  query when this object isn't running under MTS and is 
*  therefore stateful
*
IF NOT DODEFAULT()
  *
  *  this callback includes calls to
  *    THIS.Setup1()
  *    THIS.SetupPrivateDataSession()
  *    THIS.Setup2()
  *
  RETURN .f.
ENDIF
*
*  shell method
*
IF NOT THIS.Setup3()
  RETURN .f.
ENDIF
*
*  instantiate THIS.icBOMgrClass to THIS.ioBOMgr
*
IF NOT THIS.LoadBOMgr()
  RETURN .f.
ENDIF
*
*  shell method
*
IF NOT THIS.SetupAfterLoadBOMgr()
  RETURN .f.
ENDIF
*
*  possibly load business objects here
*
IF NOT THIS.LoadBO()
  RETURN .f.
ENDIF
*
*  shell method
*
IF NOT THIS.Setup4()
  RETURN .f.
ENDIF
ENDPROC
*////////////////////////////////////////////////////////


*////////////////////////////////////////////////////////
PROTECTED PROCEDURE LoadBOMgr
*
*  XXFWNT.PRG/sesCOMWrapper::LoadBOMgr()
*
*  instantiate THIS.icBOMgrClass to THIS.ioBOMgr
*
*  this method approximates XXFW.VCX/frmData::LoadBOMgr(), 
*  inasmuch as frmData provides a private data session
*
*  You have at least these options:
*    1- Set THIS.icBOMgrClass to a specific 
*       ctrBusinessObjectManager subclass that has the
*       necessary BO(s) added visually or in its LoadBO
*       method -- no code necessary here or THIS.LoadBO()
*    2- Leave THIS.icBOMgrClass set to the default
*       XXFWNTBO.VCX/ctrBusinessObjectManager, override
*       THIS.LoadBO() to instantiate the desired BO(s) --
*       see the header comments of THIS.LoadBO
*    3- Set THIS.icBOMgrClass to a specific
*       (app-specific?) ctrBusinessObjectManager subclass 
*       that has no BO(s) added to it (either visually
*       or programmatically), override THIS.LoadBO() to 
*       instantiate the desired BO(s) -- see the header
*       comments of THIS.LoadBO
*
IF THIS.ilCOMLogCoverage
  XXLOGGER(PROGRAM(),.t.,.t.)
ENDIF
IF NOT VARTYPE(THIS.icBOMgrClass) = "C" ;
     OR EMPTY(THIS.icBOMgrClass)
  THIS.icBOMgrClass = "ctrBusinessObjectManager,XXFWNTBO.VCX"
ENDIF
LOCAL lcClass
lcClass = X8SETCPR(THIS.icBOMgrClass)
IF ISNULL(m.lcClass)
  ASSERT .f. message PROGRAM() + " is unable to SET CLASSLIB/PROCEDURE TO the indicated Business Object Manager class"
  RETURN .f.
ENDIF
THIS.ioBOMgr = CREATEOBJECT(m.lcClass)
RETURN VARTYPE(THIS.ioBOMgr) = "O"
ENDPROC
*////////////////////////////////////////////////////////


*////////////////////////////////////////////////////////
PROTECTED PROCEDURE SetupAfterLoadBOMgr
*
*  XXFWNT.PRG/sesCOMWrapper::SetupAfterLoadBOMgr
*
*  this is a shell method called from THIS.Init()
*  after it calls THIS.LoadBOMgr()
*
IF THIS.ilCOMLogCoverage
  XXLOGGER(PROGRAM(),.t.,.t.)
ENDIF
ENDPROC
*////////////////////////////////////////////////////////


*////////////////////////////////////////////////////////
PROTECTED PROCEDURE LoadBO
*
*  XXFWNT.PRG/sesCOMWrapper::LoadBO()
*
*  instantiate one or more business objects 
*
*  this method approximates XXFW.VCX/frmData::LoadBO(), 
*  inasmuch as frmData provides a private data session
*
*  Note that you may NOT have any code here -- if
*  THIS.icBOMgrClass/THIS.ioBOMgr is a concrete
*  ctrBusinessObjectManager subclass that has all the 
*  necessary BOs added visually (or programmatically in 
*  its LoadBO method), then there is nothing more to do
*
*  However, if THIS.ioBOMgr is just a generic/empty
*  ctrBusinessObjectManager (XXFWNTBO.VCX/ctrBusinessObjectManager
*  by default), then you will have to code this method
*  to instantiate the individual BO(s) here.  Since
*  the Session base class does not support AddObject()
*  (or the NewObject() method) you will have to instantiate 
*  BO(s) here by using one of the following 2 techniques:
*
*  THIS.ioBOMgr.AddObject(), something like this:
*    IF NOT X8SETCLS("<VCXFileName>")
*      RETURN .f.
*    ENDIF
*    THIS.ioBOMgr.AddObject("o<BOName>","<BOName>")
*    IF VARTYPE(THIS.ioBOMgr.o<BOName>) = "O"
*      THIS.ioBOMgr.o<BOName>.SetAsMainBO()
*     ELSE
*      RETURN .f.
*    ENDIF
*
*  -OR-
*
*  1- THIS.AddProperty() a custom property to hold an
*     object reference to the BO -OR- add a custom
*     property (most likely Protected) in your (sub)class
*     definition
*  2- Instantiate the BO to the custom property
*  3- Register the BO with THIS.ioBOMgr
*  Something like this:
*    THIS.AddProperty("o"+"<BOName>")
*    IF NOT X8SETCLS("<VCXFileName>")
*      RETURN .F.
*    ENDIF
*    THIS.o<BOName> = CREATEOBJECT("<BOName>")
*    IF VARTYPE(THIS.o<BOName>) = "O"
*      THIS.o<BOName>.RegisterWithManager(THIS.ioBOMgr)
*      THIS.o<BOName>.SetAsMainBO()
*     ELSE
*      RETURN .F.
*    ENDIF
*  And you should then set THIS.o<BOName> to .NULL. in
*  THIS.Destroy:
*    THIS.o<BOName> = .NULL.
*    DODEFAULT()
*  NOTE that an object that is AddProperty()d here in
*  the session object is NOT made visible/public in the
*  runtime COM object nor its type library (which drives
*  IntelliSense) -- this is a GOOD thing.  Of course, a
*  protected custom property is also not visible/available
*  at runtime/IntelliSense.
*
*
*  If you want to instantiate BOs here that are visible
*  in IntelliSense, for each such BO:
*  1- Add a custom PUBLIC property to your OLEPublic subclass
*     of this class -- note that I'm not sure if you
*     must add the property in the class definition itself,
*     or if an AddProperty() will work in this method as you 
*     see in the sample code below; I think either will
*     work to yield a public (visible in IntelliSense)
*     member of the COM object
*  2- Here in this method, instantiate the BO to that 
*     custom property:
*
*!*	DODEFAULT()
*!*	THIS.AddProperty("CustomerBusiness")
*!*	IF NOT X8SETCLS("VCXFileName")
*!*	  RETURN .f.
*!*	ENDIF
*!*	THIS.CustomerBusiness = CREATEOBJECT("ClassName")
*!*	IF NOT VARTYPE(THIS.CustomerBusiness) = "O"
*!*	  RETURN .f.
*!*	ENDIF
*!*	THIS.ioBOMgr.RegisterBO(THIS.CustomerBusiness)
*    
*  BEWARE that you will have to mark PROTECTED all the 
*  existing PEMs of such a VMP Business Object that you
*  DON'T want to be visible in IntelliSense -- all the
*  native VFP PEMs and the VMP custom PEMs that shouldn't
*  be exposed to the developer via IntelliSense.
*
*  Alternatively, you could use the "normal" technique
*  of keeping the ctrBusinessObjectManager and its BO(s)
*  hidden/invisible, and create custom methods/properties
*  in your OLEPublic subclass of this class to expose those
*  PEMs you want visible in IntelliSense, passing those
*  method calls/property accesses to the corresponding
*  BO(s).
*
IF THIS.ilCOMLogCoverage
  XXLOGGER(PROGRAM(),.t.,.t.)
ENDIF
RETURN .t.
ENDPROC
*////////////////////////////////////////////////////////


*////////////////////////////////////////////////////////
PROTECTED PROCEDURE Setup3
*
*  XXFWNT.PRG/sesCOMWrapper::Setup3()
*
*  this is a shell method called from THIS.Init()
*  before it RETURNs 
*
IF THIS.ilCOMLogCoverage
  XXLOGGER(PROGRAM(),.t.,.t.)
ENDIF
ENDPROC
*////////////////////////////////////////////////////////


*////////////////////////////////////////////////////////
PROTECTED PROCEDURE Setup4
*
*  XXFWNT.PRG/sesCOMWrapper::Setup4()
*
*  this is a shell method called from THIS.Init()
*  before it RETURNs 
*
IF THIS.ilCOMLogCoverage
  XXLOGGER(PROGRAM(),.t.,.t.)
ENDIF
ENDPROC
*////////////////////////////////////////////////////////


*////////////////////////////////////////////////////////
PROCEDURE GetData(tuMsgSvc as String @, ;
                  tuData as String @, ;
                  tuFetchInfo as String @, ;
                  tuSecurityInfo as String @) ;
                  as Boolean ;
                  HelpString "Retrieve a set of data to the tuData parameter, as indicated by the tuFetchInfo parameter"
*
*  XXFWNT.PRG/sesCOMWrapper::GetData()
*
*  if you don't like the name of this method, just
*  use whatever name you prefer in your OLEPublic
*  subclass, and either call this method in your 
*  callback (like the commented code below), or ignore
*  this method altogether and make the call to
*  THIS.ioBOMgr.FetchData() directly
*
*!*	PROCEDURE MethodNameIPrefer(tuMsgSvc as String @, ;
*!*	                            tuData as String @, ;
*!*	                            tuFetchInfo as String @, ;
*!*	                            tuSecurityInfo as String @) ;
*!*	                            as Boolean ;
*!*	                            HelpString "My help string"
*!*	IF THIS.ilCOMLogCoverage
*!*	  XXLOGGER(PROGRAM(),.t.,.t.)
*!*	ENDIF
*!*	LOCAL llSuccess
*!*	llSuccess = THIS.GetData(@tuMsgSvc, ;
*!*	                         @tuData, ;
*!*	                         @tuFetchInfo, ;
*!*	                         @tuSecurityInfo)
*!*	RETURN llSuccess                         
*  
*
IF THIS.ilCOMLogCoverage
  XXLOGGER(PROGRAM(),.t.,.t.)
ENDIF
LOCAL llSuccess
llSuccess = THIS.ioBOMgr.FetchData(@m.tuMsgSvc, ;
                                   @m.tuData, ;
                                   @m.tuFetchInfo, ;
                                   @m.tuSecurityInfo)
RETURN m.llSuccess
ENDPROC
*////////////////////////////////////////////////////////


*////////////////////////////////////////////////////////
PROCEDURE GetBlank(tuMsgSvc as String @, ;
                   tuData as String @, ;
                   tuSecurityInfo as String @) ; 
                   as Boolean ;
                   HelpString "Retrieve a blank set of data to the tuData parameter"
*
*  XXFWNT.PRG/sesCOMWrapper::GetBlank()
*
*  NOTE that Primary/Foreign keys are NOT generated/
*  populated, because that would result in key conflicts
*  and wasted keys -- keys are generated and populated
*  during the Save process
*
*  if you don't like the name of this method, just
*  use whatever name you prefer in your OLEPublic
*  subclass, and either call this method in your 
*  callback (like the commented code below), or ignore
*  this method altogether and make the call to
*  THIS.ioBOMgr.FetchBlank() directly
*
*!*	PROCEDURE MethodNameIPrefer(tuMsgSvc as String @, ;
*!*	                            tuData as String @, ;
*!*	                            tuSecurityInfo as String @) ;
*!*	                            as Boolean ;
*!*	                            HelpString "My help string"
*!*	IF THIS.ilCOMLogCoverage
*!*	  XXLOGGER(PROGRAM(),.t.,.t.)
*!*	ENDIF
*!*	LOCAL llSuccess
*!*	llSuccess = THIS.FetchBlank(@tuMsgSvc, ;
*!*	                            @tuData, ;
*!*	                            @tuSecurityInfo)
*!*	RETURN llSuccess                         
*
*
IF THIS.ilCOMLogCoverage
  XXLOGGER(PROGRAM(),.t.,.t.)
ENDIF
LOCAL llSuccess
llSuccess = THIS.ioBOMgr.FetchBlank(@m.tuMsgSvc, ;
                                    @m.tuData, ;
                                    @m.tuSecurityInfo)
RETURN m.llSuccess                                  
ENDPROC
*////////////////////////////////////////////////////////


*////////////////////////////////////////////////////////
PROCEDURE GetList(tuMsgSvc as String @, ;
                  tuData as String @, ;
                  tuFetchInfo as String @, ;
                  tuSecurityInfo as String @) ;
                  as Boolean ;
          HelpString "Retrieve a readonly list of data from the main Business Object"
*
*  XXFWNT.PRG/sesCOMWrapper::GetList()
*
*  if you don't like the name of this method, just
*  use whatever name you prefer in your OLEPublic
*  subclass, and either call this method in your 
*  callback (like the commented code below), or ignore
*  this method altogether and make the call to
*  THIS.ioBOMgr.FetchList() directly
*
*!*	PROCEDURE MethodNameIPrefer(tuMsgSvc as String @, ;
*!*	                            tuData as String @, ;
*!*	                            tuFetchInfo as String @, ;
*!*	                            tuSecurityInfo as String @) ;
*!*	                            as Boolean ;
*!*	                            HelpString "My help string"
*!*	IF THIS.ilCOMLogCoverage
*!*	  XXLOGGER(PROGRAM(),.t.,.t.)
*!*	ENDIF
*!*	LOCAL llSuccess
*!*	llSuccess = THIS.GetList(@tuMsgSvc, ;
*!*	                         @tuData, ;
*!*	                         @tuFetchInfo, ;
*!*	                         @tuSecurityInfo)
*!*	RETURN llSuccess                         
*
IF THIS.ilCOMLogCoverage
  XXLOGGER(PROGRAM(),.t.,.t.)
ENDIF
LOCAL llSuccess 
llSuccess = THIS.ioBOMgr.FetchList(@m.tuMsgSvc, ;
                                   @m.tuData, ;
                                   @m.tuFetchInfo, ;
                                   @m.tuSecurityInfo)
RETURN m.llSuccess                                   
ENDPROC
*////////////////////////////////////////////////////////


*////////////////////////////////////////////////////////
PROCEDURE Delete(tuMsgSvc as String @, ;
                 tuPK as Integer, ;
                 tuSecurityInfo as String @) ;
                 as Boolean ;
                 HelpString "Call the main Business Object to Delete the record indicated by the Primary Key passed in tuData"
*
*  XXFWNT.PRG/sesCOMWrapper::Delete()
*
*  tuPK is specified here as an Integer, because that
*  is the most common type of PK -- if you use Character
*  PKs, then override this method in your OLEPublic
*  subclass (at the intermediate/app-specific/concrete
*  level), to specify 
*    tuPK AS Character
*
*  tuPK is not updated, and is therefore passed by value
*  rather than by reference  -- the only meaningful 
*  information yielded by this method is:
*    - its Boolean RETURN value
*    - tuMsgSvc information if it RETURNs .F.
*
*  if you don't like the name of this method, just
*  use whatever name you prefer in your OLEPublic
*  subclass, and either call this method in your 
*  callback (like the commented code below), or ignore
*  this method altogether and make the call to
*  THIS.ioBOMgr.DeleteAction() directly
*
*!*	PROCEDURE MethodNameIPrefer(tuMsgSvc as String @, ;
*!*	                            tuPK as Integer, ;
*!*	                            tuSecurityInfo as String @) ;
*!*	                            as Boolean ;
*!*	                            HelpString "My help string"
*!*	IF THIS.ilCOMLogCoverage
*!*	  XXLOGGER(PROGRAM(),.t.,.t.)
*!*	ENDIF
*!*	LOCAL llSuccess
*!*	llSuccess = THIS.DeleteAction(@tuMsgSvc, ;
*!*	                              @tuPK, ; 
*!*                               @tuSecurityInfo)
*!*	RETURN llSuccess                         
*
IF THIS.ilCOMLogCoverage
  XXLOGGER(PROGRAM(),.t.,.t.)
ENDIF
LOCAL loMTSContext, llSuccess, llMTS 
*
*  wrap the delete in a transaction
*
loMTSContext = THIS.GetMTSContext()
llMTS = VARTYPE(m.loMTSContext) = "O"
IF THIS.ioBOMgr.DeleteAction(@m.tuMsgSvc, ;
                             m.tuPK, ;
                             @m.tuSecurityInfo)   &&& DEVNOTE tlMTS
  THIS.SetMTSContext(m.loMTSContext,.t.)
  llSuccess = .t.
 ELSE
  THIS.SetMTSContext(m.loMTSContext,.f.)
  llSuccess = .f.
ENDIF
RETURN m.llSuccess
ENDPROC
*////////////////////////////////////////////////////////


*////////////////////////////////////////////////////////////
PROCEDURE Save(tuMsgSvc as String @, ;
               tuData as String, ;
               tuSecurityInfo as String @) ;
               as Boolean ;
               HelpString "Save the passed data"
*
*  XXFWNT.PRG/sesCOMWrapper::Save()
*
*  do the Save, wrapped in an MTS transaction, if applicable
*  (please note that this version of VMP does nothing 
*  MTS-specific, but if you understand all the issues in 
*  involved and want to implement MTS transactions, you may 
*  have nothing extra to do here, although you may well have 
*  to override this method in your intermediate/OLEPublic 
*  subclass, in which case this code serves as an 
*  example/reminder)
*
*  if you don't like the name of this method, just
*  use whatever name you prefer in your OLEPublic
*  subclass, and either call this method in your 
*  callback (like the commented code below), or ignore
*  this method altogether and make the call to
*  THIS.ioBOMgr.SaveAction() directly
*
*!*	PROCEDURE MethodNameIPrefer(tuMsgSvc as String @, ;
*!*	                            tuData as String, ;
*!*	                            tuSecurityInfo as String @, ;
*!*	                            as Boolean ;
*!*	                            HelpString "My help string"
*!*	IF THIS.ilCOMLogCoverage
*!*	  XXLOGGER(PROGRAM(),.t.,.t.)
*!*	ENDIF
*!*	LOCAL llSuccess
*!*	llSuccess = THIS.SaveAction(@tuMsgSvc, ;
*!*	                            tuData, ;
*!*	                            @tuSecurityInfo)
*!*	RETURN llSuccess                         
*
IF THIS.ilCOMLogCoverage
  XXLOGGER(PROGRAM(),.t.,.t.)
ENDIF
LOCAL loMTSContext, llSuccess, llMTS, tuSecurityInfo
*
*  wrap the update in a transaction
*
loMTSContext = THIS.GetMTSContext()
llMTS = VARTYPE(m.loMTSContext) = "O"
IF THIS.ioBOMgr.SaveAction(@m.tuMsgSvc, ;
                           m.tuData, ;
                           @m.tuSecurityInfo, ;
                           m.llMTS)
  THIS.SetMTSContext(m.loMTSContext,.t.)
  llSuccess = .t.
 ELSE
  THIS.SetMTSContext(m.loMTSContext,.f.)
  llSuccess = .f.
ENDIF
RETURN m.llSuccess
ENDPROC
*////////////////////////////////////////////////////////


*////////////////////////////////////////////////////////
PROTECTED PROCEDURE ValidateFieldValue(tuMsgSvc as String @, ;
                             tcFieldName as String, ;
                             tuValue as String) ;
                             as Boolean 
*
*  XXFWNT.PRG/sesCOMWrapper::ValidateFieldValue()
*
*  this method accepts a tcFieldName parameter to identify the
*  field whose tuValue is to be validated by passing the information
*  to the Business Object Manager, which in turn passes it on to the 
*  Business Object responsible for that data
*
*  EXPOSE/CALL THIS METHOD JUDICIOUSLY/SPARINGLY, IF AT ALL
*  -- COM clients typically do not execute field-by-field 
*  validation because each one requires a trip to the server  
*  (to expose it, augment it in your subclass but don't
*  define is as PROTECTED)
*
IF THIS.ilCOMLogCoverage
  XXLOGGER(PROGRAM(),.t.,.t.)
ENDIF
LOCAL llSuccess
llSuccess = THIS.ioBOMgr.ValidateFieldValue(@m.tuMsgSvc, ;
                                            m.tcFieldName, ;
                                            m.tuValue)
RETURN m.llSuccess                                            
*////////////////////////////////////////////////////////



*////////////////////////////////////////////////////////
PROTECTED PROCEDURE ORCleanup()
*
*  XXFWNT.PRG/sesCOMWrapper::ORCleanup()
*
*  object reference cleanup on THIS.Destroy()
*
IF THIS.ilCOMLogCoverage
  XXLOGGER(PROGRAM(),.t.,.t.)
ENDIF
IF VARTYPE(THIS.ioBOMgr) = "O"
  THIS.ioBOMgr.ORCleanup()
ENDIF
DODEFAULT()
THIS.ioBOMgr = .NULL.
ENDPROC
*////////////////////////////////////////////////////////

**********************************************************
**********************************************************
ENDDEFINE
**********************************************************
**********************************************************
