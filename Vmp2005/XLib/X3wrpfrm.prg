*
*  X3WRPFRM.PRG
*  Calls the passed .VCX-based modal form 
*  and RETURNs a value set in the form.
*
*  NOTE:  the .VCX-based modal form is required to
*         manipulate the puX3wrpfrm private memvar
*         created here and visible there.  It's the
*         puX3wrpfrm memvar that is "returned" to
*         this .PRG and to your X3WRPFRM() call.
*         XXFW.VCX/frmBase contains the code necessary
*         to copy THISFORM.iuRetVal to puX3wrpfrm and
*         RETURNed from this routine.
*
*  This routine is included for backward compatibility
*  and is not called anywhere in the current VMP
*  framework -- use X8WRPFRM.PRG instead.
*
*  Copyright (c) 1997-2005 Visionpace   All Rights Reserved
*                17501 East 40 Hwy., Suite 218
*                Independence, MO 64055
*                816-350-7900 
*                http://www.visionpace.com
*                http://vmpdiscussion.visionpace.com
*  Author:  Drew Speedie, special thanks to Reg Brehaut
*
*
*  USAGE:
*  xx = x3wrpfrm("frmReportDestination")
*  xx = x3wrpfrm("frmReportDestination,xxfw")
*  xx = x3wrpfrm("frmReportDestination,xxfw.vcx",[2])
*  xx = x3wrpfrm("frmReportDestination,xxfw.vcx",[2,"Form Title"])
*  xx = x3wrpfrm("frmGetInputItem,xxfwfrm")
*  xx = x3wrpfrm("frmGetInputItem,xxfwfrm.vcx",["What's your name?"])
*
*  xx = x3wrpfrm("MyForm,MyForms.VCX",SomeObject)
*
*  EXAMPLES:
*    XXFWCTRL.VCX/txtPicklistValid.DoPicklistForm()
*    XXFWFRM.VCX/frmFormat.cmdForeColor.Click()
*    XXFWFRM.VCX/frmFormat.cmdBackColor.Click()
*    XXFWMISC.VCX/frmReport.GetOutputDestination()
*    XXFWSEC.VCX/frmRightsAdmin.CallGroupsDialog()
*    XXFWSEC.VCX/frmRightsAdmin.CallUsersDialog()
*    XXFWSEC.VCX/frmUserProperties.pgfSecurity.Page2.grdSecurity.AddRow()
*    In the VM example application:
*      VMREPRTS.VCX/frmVMCusD.BeforePrepareData()
*      VMDECUSC.SCX/cmdFilter.Click()
*
*  NOTE:
*  You can get the same result without calling this
*  X3WRPFRM(), if you are willing to do a little 
*  extra work.  For example, instead of:
*    local lcRetVal
*    lcRetVal = x3wrpfrm("frmGetInputItem,xxfwfrm.vcx", ;
*         ["What's your name?","Enter your name","!XXXXXXXXXXXXXX","",40])
*    IF isnull(lcRetVal)
*      *  user closed frmGetInputItem by <Cancel>
*     ELSE
*      *  process the collected lcRetVal
*    ENDIF
*  try this equivalent:
*    private puX3WrpFrm
*    puX3WrpFrm = space(0)
*    x3setcls("xxfwfrm.vcx")
*    local loForm
*    loForm = createobject("frmGetInputItem", ;
*                          "What's your name?", ;
*                          "Enter your name", ;
*                          "!XXXXXXXXXXXXXX", ;
*                          space(15), ;
*                          40)
*    loForm.Show()
*    IF isnull(puX3WrpFrm)
*      *  user closed frmGetInputItem by <Cancel>
*     ELSE
*      *  process the collected lcRetVal
*    ENDIF
*  For an example of doing it this way, see:
*    VMREPRTS.VCX/frmPrintInvFromMover_SomeSelectedPreDelete.PrepareData()
*    XXWB.VCX/ctrAppSetupWizardSubclassPageFromIntermediate.cmdShopXGetDir.Click()
*    
*
*
*  lParameters
*     tcClassName (R) Class Name of the .VCX-based form
*                       to be executed.  You may optionally
*                       (highly recommended) include a comma-
*                       delimited .VCX (or .PRG ) file name to 
*                       SET CLASSLIB TO  
*    tuParameters (O) Character string containing
*                     parameters to be sent to the form
*                       OR
*                     a SINGLE object reference 
*                     (note that VMDECUSC.SCX/cmdFilter.Click()
*                     contains an example of sending tuParameters
*                     that contains BOTH an object reference
*                     and other parameters)
*                       XXDTES("VMDECUSC.SCX","X3WRPFRM","Form","cmdFilter.Click")
*                     ...same as the optional 4th parameter
*                        accepted by XXFW.VCX/cusForms.DoForm()...
*  tcInAppEXEFile (O) This parameter optionally specifies the 
*                     name of an .APP file that contains 
*                     the tcClassName class definition, maps
*                     to the IN AppFileName supported by
*                     SET CLASSLIB TO
*                     
*
LPARAMETERS tcClassName, ;
            tuParameters, ;
            tcInAppEXEFile

IF TYPE("tcClassName") = "C" AND !EMPTY(tcClassName)
  LOCAL lcClassName
  lcClassName = UPPER(ALLTRIM(tcClassName))
 ELSE
  ASSERT .f. message PROGRAM() + ;
       " has not received a valid tcClassName parameter"
  RETURN .NULL.
ENDIF

LOCAL lcInAppEXEFile, llOK
lcInAppEXEFile = SPACE(0)
llOK = .t.
IF TYPE("tcInAppEXEFile") = "C" ;
    AND !ISNULL(tcInAppEXEFile) ;
    AND !EMPTY(tcInAppEXEFile)
  lcInAppEXEFile = UPPER(ALLTRIM(tcInAppEXEFile))
  llOK = FILE(lcInAppEXEFile)
ENDIF
IF llOK
  lcClassName = x3setcpr(lcClassName,lcInAppEXEFile)
ENDIF
IF ISNULL(lcClassName)
  ASSERT .f. message PROGRAM() + ;
       " is unable to find the passed tcClassName: " + ;
       lcClassName
  RETURN .NULL.
ENDIF

IF TYPE("oUser.oSecurity.BaseClass") = "C" 
  *
  *  note that tcClassName must match the target
  *  SecuredItems.SIt_ID "ClassName,VCXFileName"
  *  for this security check to be of any value
  *
  IF oUser.oSecurity.GetAccess(lcClassName) >= 9
    X3MSGSVC("Access denied, security")
    RETURN .NULL.
  ENDIF
ENDIF

local luParameters
DO CASE
  CASE type("tuParameters") = "C" AND !empty(tuParameters)
    luParameters = alltrim(tuParameters)
  CASE type("tuParameters.BaseClass") = "C"
    luParameters = tuParameters
  OTHERWISE
   luParameters = space(0)
ENDCASE

PRIVATE puX3WrpFrm
puX3WrpFrm = .NULL.

local loForm, loMP, laStrings[4]
laStrings[1] = "\XXWB"
laStrings[2] = "\XXDT"
laStrings[3] = "\XXTOOLS"
laStrings[4] = "\TOOLS"
IF !x2pstack(@laStrings) AND x3setcls("XXFWUTIL.vcx") 
  loMP = createobject("cusPushPopMousePointers",.t.)
ENDIF

DO CASE
  CASE type("luParameters") = "C" AND !empty(luParameters)
    loForm = createobject(lcClassName,&luParameters)
  CASE type("luParameters.BaseClass") = "C"
    loForm = createobject(lcClassName,luParameters)
  OTHERWISE 
    loForm = createobject(lcClassName)
ENDCASE

IF type("loForm.BaseClass") = "C" 
  loForm.Show(1)
ENDIF

return puX3WrpFrm
