*
*  X8GUID.PRG
*  RETURNs a GUID (Globally Unique IDentifier) 128-bit
*  string in one of four formats:
*    - 16-character string:
*          B6444636537433AD
*    - 32-character string:
*          B5454541C4324543AD423542B12D3436
*    - 36-character string (hex representation A-F and 0-9, and including dashes):
*          B1353345-B342-4436-AD38-4538B62D3432
*    - 38-character string (hex representation including dashes, plus curly braces):
*          {B1353345-B342-4436-AD38-4538B62D3432}
*  RETURNS .NULL. on failure.
*
*  Copyright (c) 2005 Visionpace   All Rights Reserved
*                17501 East 40 Hwy., Suite 218
*                Independence, MO 64055
*                816-350-7900 
*                http://www.visionpace.com
*                http://vmpdiscussion.visionpace.com
*  Author:  Drew Speedie
*           Special thanks to Christopher Bohling -- this routine
*           is a "Drewized" version of code Chris wrote in Dec 2003.
*
*
*  USAGE (beware that lcGUID can be .NULL.)
*  ==============================================
*  lcGUID = X8GUID()
*  lcGUID = X8GUID(16)
*  lcGUID = X8GUID(32)
*  lcGUID = X8GUID(36)
*  lcGUID = X8GUID(38)
*  lnHandle = SQLSTRINGCONNECT(...)
*  lcGUID = X8GUID(,m.lnHandle)
*  lcGUID = X8GUID(16,m.lnHandle)
*  lcGUID = X8GUID(32,m.lnHandle)
*  lcGUID = X8GUID(36,m.lnHandle)
*  lcGUID = X8GUID(38,m.lnHandle)
*
*
*  lParameters
*    tnGUIDLength (O) Specify the length of the GUID you
*                       want RETURNed
*                     The GUID is created using the 
*                       CoCreateGUID() API function call
*                     Must be 16, 32, 36, or 38 -- if not,
*                       defaults to 16
*        tnHandle (O) Specify the connection handle of
*                       an existing connection to SQL Server,
*                       and the GUID is created using the
*                       NewID() function in SQL Server
*                     If the connection handle is invalid
*                       or in any other way the process
*                       fails, .NULL. is RETURNed -- this
*                       routine DOES NOT then generate 
*                       the GUID by calling CoCreateGUID(),
*                       because by passing this parameter,
*                       you have indicated that you only
*                       want a NewID()-generated GUID.
*
LPARAMETERS tnGUIDLength AS INTEGER, ;
            tnHandle AS INTEGER

LOCAL loException AS Exception, ;
      lcGUID AS String, ;
      llSQLServer AS Boolean, ;
      lcSQLResult AS String, ;
      lnLen AS Integer, ;
      lcString AS String, ;
      xx AS Integer

lcGUID = SPACE(0)
lcString = SPACE(0)
IF VARTYPE(m.tnGUIDLength) = "N" AND INLIST(m.tnGUIDLength,16,32,36,38)
  lnLen = m.tnGUIDLength
 ELSE
  *  default
  lnLen = 16
ENDIF

*
*  see if we're supposed to call the SQL Server
*  NewID() function
*
IF VARTYPE(m.tnHandle ) = "N" AND m.tnHandle > 0 
  lcSQLResult = "C" + SYS(2015)
  TRY
  llSQLServer = "SQL SERVER" $ UPPER(SQLGETPROP(m.tnHandle,"ConnectString"))
  CATCH TO loException
  ENDTRY
  IF VARTYPE(m.loException) = "O" OR NOT m.llSQLServer
    *
    *  the SQLGETPROP() failed due to an invalid connection
    *  OR 
    *  the connection handle is NOT to a SQL Server
    *
    lcGUID = .NULL.
    RETURN m.lcGUID 
  ENDIF
  TRY
  lnResult = SQLEXEC(m.tnHandle,"SELECT NewID() AS GUID", lcSQLResult)>0
  CATCH TO loException
  ENDTRY
  IF VARTYPE(m.loException) = "O" OR NOT m.lnResult OR NOT RECCOUNT(m.lcSQLResult)=1
    *
    *  the SQLEXEC() failed
    *  OR
    *  the SQLEXEC() did not RETURN exactly 1 record
    *
    USE IN (m.lcSQLResult)
    lcGUID = .NULL.
    RETURN m.lcGUID   &&& .NULL.
  ENDIF
  lcGUID = EVALUATE(m.lcSQLResult + ".GUID" )
  USE IN (m.lcSQLResult)
  *
  *  continue on in order to process the tnGUIDLength
  *
ENDIF

IF EMPTY(m.lcGUID)
  *
  *  "regular" (non-SQL Server NewID) GUID generation 
  *  via CoCreateGUID() API call
  *
  lcGUID = SPACE(16)
  IF NOT X7ISAPIF("CoCreateGuid")
    DECLARE INTEGER CoCreateGuid IN OLE32.DLL STRING @ lcGUID
  ENDIF
  TRY
  CoCreateGuid(@lcGUID)    &&& this is the main line of code in this routine
  CATCH TO loException
  ENDTRY
  IF VARTYPE(m.loException) = "O"
    lcGUID = .NULL.
    RETURN m.lcGUID
  ENDIF
ENDIF

FOR xx = 1 TO 16 STEP 4
  m.lcString = m.lcString + ;
               RIGHTC(TRANSFORM(CTOBIN(SUBSTRC(m.lcGUID,m.xx,4)),"@0"),8) 
ENDFOR               
m.lcGUID = m.lcString

DO CASE
  CASE m.lnLen = 16
    lcGUID = LEFTC(m.lcGUID,8) + SUBSTRC(m.lcGUID,10,4) + SUBSTR(m.lcGUID,15,4)
  CASE m.lnLen >= 36
    *Like this: B8E3CE2E-EC41-4EB4-857E-DC885F946541
    IF AT("-",m.lcGUID) = 0
      lcGUID = STUFF(m.lcGUID, 9,0,"-" )
      lcGUID = STUFF(m.lcGUID,14,0,"-" )
      lcGUID = STUFF(m.lcGUID,19,0,"-" )
      lcGUID = STUFF(m.lcGUID,24,0,"-" )
    ENDIF
    IF m.lnLen = 38
      *Like this: {B8E3CE2E-EC41-4EB4-857E-DC885F946541}
      lcGUID = "{" + m.lcGUID + "}"
    ENDIF
ENDCASE

RETURN m.lcGUID
