*
*  X5WebPageNavComplete.PRG
*  Delay processing until a web page is fully loaded 
*  to the Internet Explorer object -- until navigation 
*  to a web page is complete.
*  RETURNs .T. if the delay is successful and navigation
*  is completed.
*  RETURNs .F. if the navigation is unsuccessful or in
*  some other way is not completed.
*
*  Copyright (c) 2003-2005 Visionpace   All Rights Reserved
*                17501 East 40 Hwy., Suite 218
*                Independence, MO 64055
*                816-350-7900 
*                http://www.visionpace.com
*                http://vmpdiscussion.visionpace.com
*  Author:  Art Bergquist (based on NavComplete function in 
*                          Erik Moore's article "Automate Internet 
*                          "Explorer from VFP" in the June 2000 
*                          issue of FoxPro Advisor)
*
*  USAGE
*  ===============================
*  LOCAL lcVMPDownloadsWebPage
*  lcVMPDownloadsWebPage = [http://www.visionpace.com/VMPSite]
*  LOCAL loIE AS InternetExplorer.Application	 
*  loIE = CREATEOBJECT('InternetExplorer.Application')		&& 'InternetExplorer.Application.1'
*  loIE.Navigate(lcVMPDownloadsWebPage)
*  IF X5WebPageNavComplete(loIE)   &&& allow time for the page to load up
*    <do processing based on the web page that just loaded ...>
*  ENDIF
*  loIE = .NULL. 
*
*  USAGE in VMP
*  ===============================
*  XXDTES("XXPROCDT.PRG","X5WebPageNavComplete")
*  
* 
*  lParameters
*       toIE (R) Reference to the Internet Explorer application 
*                  object
*  tnTimeOut (O) Specify the number of seconds after which
*                  this routine gives up and RETURNs .F.
*                Defaults to 60 seconds
*
LPARAMETERS toIE, tnTimeOut

IF NOT VARTYPE(toIE) = "O"
  ASSERT .f. message ;
       "Parameters must be passed in the format:" + ;
       CHR(13) + CHR(13) + ;
       "toIE [, tnTimeOut] (note that tnTimeOut is optional)"
  RETURN .f.
ENDIF             

LOCAL lnTimeout, lnElapsedSeconds, lnStartSeconds
lnTimeout        = IIF(TYPE('tnTimeout') = 'N', tnTimeout, 60)
lnElapsedSeconds = 0
lnStartSeconds   = SECONDS()
*
*  first, poll toIE.ReadyState until it equals 4 
*  (or it times out)
*
DO WHILE .T.
  * 
  *  Possible values for .ReadyState
  *
  *    0 = ReadyState_Uninitialized
  *    1 = ReadyState_Loading
  *    2 = ReadyState_Loaded
  *    3 = ReadyState_Interactive
  *    4 = ReadyState_Complete
  * 
  IF toIE.ReadyState = 4
    *
    *  now poll toIE.Document.ReadyState until it equals 'complete' 
    *  (or it times out)
    *
    DO WHILE .T.
      IF toIE.Document.ReadyState = 'complete'
        RETURN .T.
      ENDIF
      *
      *  check for toIE.Document time out
      *  
      IF (SECONDS() - lnStartSeconds) > lnTimeout
        RETURN .F.        
      ENDIF
    ENDDO 
  ENDIF
  *
  *  check for toIE time out
  *
  IF (SECONDS() - lnStartSeconds) > lnTimeout
    RETURN .F. 
  ENDIF
ENDDO          
RETURN .t.
