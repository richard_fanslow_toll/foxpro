*
*  XXDTSIt.PRG
*  Maintain an app-specific SecuredItems free table for a 
*  specific .PJX project.
*
*  Copyright (c) 1997-2005 Visionpace   All Rights Reserved
*                17501 East 40 Hwy., Suite 218
*                Independence, MO 64055
*                816-350-7900 
*                http://www.visionpace.com
*                http://vmpdiscussion.visionpace.com
*  Author:  Mike Meer and Drew Speedie
*

IF UPPER(JUSTFNAME(SYS(16,0))) = "XXDT."   &&& XXDT.FXP
  MESSAGEBOX(JUSTFNAME(PROGRAM()) + ;
             " cannot be run directly from this XXDT utility." + CHR(13) + CHR(13) + ;
             "DO " + JUSTFNAME(PROGRAM()) + CHR(13) + CHR(13) + ;
             "is about to be entered into the Command Window, so you can " + ;
             "run " + JUSTFNAME(PROGRAM()) + " from there.", ;
             48,"Please Note")
  KEYBOARD "DO " + JUSTFNAME(PROGRAM()) PLAIN CLEAR        
  RETURN 
ENDIF

IF file("XXPROCDT.PRG")
  do SavePJXs in XXPROCDT.PRG
ENDIF

*
*  we don't want anything unexpected getting
*  in the way...
*
clear all
close all

set classlib to xxtools.vcx additive
public xxdtsit
xxdtsit = createobject("frmXXDTSIT")
IF type("xxdtsit.baseclass") = "C"
  xxdtsit.Show()
 ELSE
  IF type("xx") = "N"
    *  frmXXDTSit failed the Load() because the
    *  user <Cancel>led from the GetFile() 
    *  dialog and didn't select a .PJX
   ELSE
    *  frmXXDTSit failed to make it thru the Load()
    do x3winmsg with "Unable to createobject('frmXXDTSit') from XXTOOLS.VCX"
  ENDIF
  clear all
  close all
ENDIF

IF file("XXPROCDT.PRG")
  do RestorePJXs in XXPROCDT.PRG
ENDIF

IF vartype(xxdtsit) = "O"
  xxdtsit.show()   &&& get it back on top in case a .PJX was open
ENDIF

return
