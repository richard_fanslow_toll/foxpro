*
*  X8ConvChar.PRG
*  Converts the passed character string to a value
*    of the passed data type.  If unable to process,
*    returns .NULL.  
*
*  -OR-
*
*  Converts the passed value to a string (when the 2nd
*    parameter is not passed)  If unable to process,
*    returns .NULL. 
*
*  -OR-
*
*  If tuValue is passed as a string and tcDataType is passed as
*    "D" or "T", but the resulting date/datetime is invalid,
*    a blank date/datetime is RETURNed, as would be the
*    case from CTOD() or CTOT()
*
*  -OR-
*
*  If tuValue is passed as a Date/DateTime and 
*    tcDataType is passed as "S", converts the passed
*    Date/DateTime to a string in the format
*    {^yyyy-mm-dd} or {^yyyy-mm-dd,hh:mm:ss}, suitable
*    for use in filter conditions/SQL WHERE clauses.
*    If tuValue is empty, RETURNs "{}" or "{-:}".
*    Generates ERROR 9 "Data type mismatch" if tcDataType
*    is passed as "S", but tuValue is neither a Date nor
*    a DateTime.
*    tcDateType can optionally be passed as "SR", the
*    extra "R" indicating that you want a 'remote-data-compatible'
*    string RETURNed, in the format, without the VFP curly
*    braces and caret convention:
*      yyyy-mm-dd or yyyy-mm-dd hh:mm:ss 
*
*  -OR-
*
*  If tuValue is passed as a string in the format
*    "YYYYDDD" and tcDataType is passed as "D", the
*    indicated Date is RETURNed (DDD days from January
*    1st of the year indicated in YYYY) -- note that
*    up to 3 digits can be passed in the DDD portion
*    of the string, which is therefore up to 7 characters
*    in length.
*    Please note that this functionality is based on
*    the cvtyyyydddd2date routine that late Ed Ruah (MVP)
*    put in the Public Domain.
*    For example:
*      ? X8ConvChar("2005111","D")   &&& RETURNs 04/21/2005 in SET DATE AMERICAN
*    ...the converse functionality is available, too:
*      ? X8ConvChar({^2005-04-21},"YYYYDDD")   &&& RETURNs "2005111"
*    Note that to convert a date into the "YYYYDDD" format,
*    the 2nd tcDataType parameter MUST be passed as "YYYYDDD"
*
*  Use this routine instead of X2CNVCHR.PRG, which is
*  included for backward compatibility of existing pre-VMP2004
*  application code.
*
*  Copyright (c) 2004-2005 Visionpace   All Rights Reserved
*                17501 East 40 Hwy., Suite 128
*                Independence, MO 64055
*                816-350-7900 
*                http://www.visionpace.com
*                http://vmpdiscussion.visionpace.com
*  Author:  Drew Speedie and Ed Lennon
*           Special thanks to Dale Kiefling, Alex Feldstein, Mike Yearwood, and Mike Potjer
*
*  lParameters
*       tuValue (R) Value to be converted
*    tcDataType (O) Data type to convert the tuValue to:
*                     "C"haracter
*                     "N"umeric
*                     "D"ate
*                     Date"T"ime
*                     "L"ogical
*                     "E"xpression
*                     Currenc"Y"
*                     "S" (but only if tuValue is passed as a Date/DateTime)
*                   IF tuValue is a character string to
*                     be converted to a specific data
*                     type, tcDataType is required.
*                   If tcDataType is not passed, this
*                     routine converts tuValue to a
*                     character string.
*                   If tuValue is a Date, may be passed
*                     as "YYYYDDD", whereupon the value
*                     RETURNed is the Date converted to
*                     "YYYYDDD" format:
*                     "YYYY" is the year of the passed Date
*                     "DDD" is the ordinal number of days from January 1, of "YYYY"
*
LPARAMETERS tuValue, tcDataType

LOCAL luReturnValue, lcString, lcDataType, llStringToValue, ; 
      llDataTypeOK, lnDecimals, lnSetDecimals, llEmpty, llPM, ;
      junk, llError, lcSetPoint, lnYearDateFunction, lnGoYears, ;
      lcSetDate, lnYear, lnMonth, lnDay, lnHours, lnMin, lnSec, ;
      lnStrictDate

********************************************************
********************************************************
*
*  handle the "YYYYDDD" -> Date possiblity first
*
********************************************************
********************************************************
IF VARTYPE(m.tuValue) = "C" AND NOT EMPTY(m.tuValue) ;
     AND BETWEEN(LENC(ALLTRIM(m.tuValue)),4,7) ;
     AND OCCURS(SPACE(1),ALLTRIM(m.tuValue)) = 0 ;
     AND VARTYPE(m.tcDataType) = "C" AND UPPER(ALLTRIM(m.tcDataType)) = "D"
  llError = .f.
  FOR junk = 1 TO LENC(ALLTRIM(m.tuValue))
    IF NOT ISDIGIT(SUBSTRC(ALLTRIM(m.tuValue),m.junk))
      llError = .t.
      EXIT 
    ENDIF
  ENDFOR
  IF NOT m.llError
    *
    *  convert "YYYYDDD" tuValue to a Date corresponding
    *  to the DDDth day from January 1, YYYY
    *
    lnDay = VAL(SUBSTRC(ALLTRIM(m.tuValue),5))
    IF m.lnDay = 0
      lnDay = 1
    ENDIF
    TRY
    luReturnValue = EVALUATE("{^" + LEFTC(ALLTRIM(m.tuValue),4) + "-01-01}") + (lnDay-1)
    CATCH
    luReturnValue = DATE()
    luReturnValue = .NULL.
    ENDTRY
    RETURN m.luReturnValue
  ENDIF  
ENDIF     
********************************************************
********************************************************
*
*  next handle the Date -> "YYYYDDD" -> possiblity 
*
********************************************************
********************************************************
IF VARTYPE(m.tuValue) = "D" AND NOT EMPTY(m.tuValue) ;
     AND VARTYPE(m.tcDataType) = "C" ;
     AND UPPER(ALLTRIM(m.tcDataType)) == "YYYYDDD"
  TRY
  lnYear = YEAR(m.tuValue)
  lnDay = m.tuValue - DATE(m.lnYear,01,01) + 1
  luReturnValue = PADR(m.lnYear,4) + TRANSFORM(m.lnDay)
  CATCH
  luReturnValue = SPACE(0)
  luReturnValue = .NULL.
  ENDTRY   
  RETURN m.luReturnValue     
ENDIF

luReturnValue = .NULL.
IF VARTYPE(m.tcDataType) = "C" AND NOT EMPTY(m.tcDataType)
  lcDataType = UPPER(ALLTRIM(LEFTC(m.tcDataType,1)))
 ELSE
  lcDataType = SPACE(0)
ENDIF

********************************************************
********************************************************
*
*  now handle tcDataType = "S" (and "SR")
*
********************************************************
********************************************************
IF m.lcDataType = "S"   &&& includes "SR"
  DO CASE
    *********************************************
    CASE INLIST(VARTYPE(m.tuValue),"T","D") AND EMPTY(m.tuValue)
    *********************************************
      IF m.tcDataType = "SR"
        RETURN IIF(VARTYPE(m.tuValue) = "T","1900-01-01 00:00:00","1900-01-01")
       ELSE
        RETURN IIF(VARTYPE(m.tuValue) = "T","{-:}","{}")
      ENDIF
    *********************************************
    CASE INLIST(VARTYPE(m.tuValue),"T","D")
    *********************************************
      *  process below
    *********************************************
    OTHERWISE
    *********************************************
      *  tuValue passed as a non-D/T
      *  data type
      ERROR 9   &&& Data Type mismatch
      RETURN m.luReturnValue
  ENDCASE
  IF VARTYPE(m.tuValue) = "T"
    *yyyymmddhhmmss
    lcString = X6TTOS(m.tuValue)
   ELSE
    *yyyymmdd
    lcString = DTOS(m.tuValue)
  ENDIF
  luReturnValue = "{^" + LEFTC(m.lcString,4) + "-" + ;
                  SUBSTRC(m.lcString,5,2) + "-" + ;
                  SUBSTRC(m.lcString,7,2)
  IF VARTYPE(m.tuValue) = "T"
    luReturnValue = m.luReturnValue + "," + ;
                    SUBSTRC(m.lcString,9,2) + ":" + ;
                    SUBSTRC(m.lcString,11,2) + ":" + ;
                    RIGHTC(m.lcString,2)
  ENDIF
  luReturnValue = m.luReturnValue + "}"
  IF m.tcDataType = "SR"
    luReturnValue = CHRTRAN(m.luReturnValue,"{^}",SPACE(0))
    luReturnValue = CHRTRAN(m.luReturnValue,",",SPACE(1))
  ENDIF
  RETURN m.luReturnValue
ENDIF

********************************************************
********************************************************
*
*  now handle the "regular" conversions
*
********************************************************
********************************************************

llDataTypeOK = ;
      NOT EMPTY(m.lcDataType) ;
      AND INLIST(m.lcDataType,"C","N","D","L","T","E","Y")

IF m.llDataTypeOK
  *
  *  tcDataType was passed as a valid data type
  *
  *  convert character string tuValue to
  *  the passed data type
  *
  IF m.lcDataType = "C"
    llStringToValue = .F.
   ELSE
    llStringToValue = .T.
    lcString = tuValue
  ENDIF
 ELSE
  *
  *  tuValue is to be converted to a string
  *  (2nd parameter not passed)
  *
  llStringToValue = .F.
ENDIF

IF m.llStringToValue
  *
  *  convert the passed string to a value of the
  *  indicated data type
  *
  DO CASE
    **********************************************
    CASE m.lcDataType = "C"   &&& character
    **********************************************
      luReturnValue = ALLTRIM(m.lcString)
    **********************************************
    CASE INLIST(m.lcDataType,"N","Y")  &&& numeric/currency
    **********************************************
      IF EMPTY(m.lcString)
        luReturnValue = 0
       ELSE
        lcSetPoint = SET("POINT")
        

*  DREW -- decimal separator could be ","?

        lcString = ALLTRIM(m.lcString)
        lnDecimals = 0
        IF m.lcSetPoint $ m.lcString
          lnDecimals = SUBSTRC(m.lcString,AT_C(m.lcSetPoint,m.lcString)+1)
          lnDecimals = LENC(m.lnDecimals)
        ENDIF
        IF m.lnDecimals > 0
          lnSetDecimals = SET("DECIMALS")
          SET DECIMALS TO (m.lnDecimals)
          luReturnValue = VAL(m.lcString)
          SET DECIMALS TO (m.lnSetDecimals)
         ELSE
          luReturnValue = INT(VAL(m.lcString))
         ENDIF
       ENDIF   &&&  EMPTY(lcString)
    **********************************************
    CASE m.lcDataType = "D" AND m.lcString = "^"  
    **********************************************
      *
      *  "^yyyy-mm-dd" strictdate format
      *
      lcString = ALLTRIM(SUBSTRC(m.lcString,2))
      IF LENC(m.lcString) = 10   
        *
        *  yyyy-mm-dd or any of its variations
        *
       ELSE
        *
        *  you will now crash due to an ambiguous date
        *
        ERROR 2034
      ENDIF 
      lnYear = INT(VAL(SUBSTRC(m.lcString,1,4)))
      lnMonth = INT(VAL(SUBSTRC(m.lcString,6,2)))
      lnDay = INT(VAL(SUBSTRC(m.lcString,9,2)))
      IF m.lnYear < 100
        *
        *  the DATE() function can't handle
        *  dates whose year is less than 100
        *
        *  and the GOMONTH() function can't
        *  handle dates prior to Jan 1 1753
        *
        lnYearDateFunction = 100
        lnGoYears = 100 - m.lnYear
        luReturnValue = DATE(m.lnYearDateFunction,m.lnMonth,m.lnDay) - ;
                        ROUND((365.25*lnGoYears),0)
       ELSE
        luReturnValue = DATE(m.lnYear,m.lnMonth,m.lnDay)
      ENDIF
    **********************************************
    CASE lcDataType = "D" AND NOT lcString = "^" 
    **********************************************
      *
      *  "mm/dd/yyyy" or any of its SET DATE variations
      *   
      lcString = ALLTRIM(m.lcString)
      llEmpty = EMPTY(CHRTRANC(m.lcString,"/",SPACE(0))) ;
                OR ;
                EMPTY(CHRTRANC(m.lcString,".",SPACE(0))) ;
                OR ;
                EMPTY(CHRTRANC(m.lcString,"-",SPACE(0)))

      IF EMPTY(m.lcString) OR m.llEmpty
        luReturnValue = {}
       ELSE
        lnStrictDate = SET("STRICTDATE")
        lcSetDate = SET("DATE")
        IF LENC(m.lcString) = 10   
          *
          *  mm/dd/yyyy or any of its SET DATE variations
          *
         ELSE
          *
          *  mm/dd/yy or any of its SET DATE variations --
          *  you will now crash due to an ambiguous date
          *
          ERROR 2034
        ENDIF
        SET STRICTDATE TO 0  
        junk = "lcString = CTOD(tuValue)"   &&& respecting current SET DATE
        &junk  &&& prevent a compiler error when SET STRICTDATE = 1 or 2
        IF EMPTY(m.lcString)
          *
          *  invalid date was passed -- RETURN value
          *  is {}, same as it would be from:
          *    ctod(<Invalid Date>)
          *
          SET DATE (m.lcSetDate)
          luReturnValue = {}
         ELSE
          SET DATE AMERICAN 
          lcString = DTOC(m.lcString)
          SET STRICTDATE TO (m.lnStrictDate)
          lnMonth = INT(VAL(LEFTC(m.lcString,2)))
          lnDay = INT(VAL(SUBSTRC(m.lcString,4,2)))
          lnYear = INT(VAL(SUBSTRC(m.lcString,7)))
          SET DATE (m.lcSetDate)
          IF m.lnYear < 100
            *
            *  the DATE() function can't handle
            *  dates whose year is less than 100
            *
            *  and the GOMONTH() function can't
            *  handle dates prior to Jan 1 1753
            *
            lnYearDateFunction = 100
            lnGoYears = 100 - m.lnYear
            luReturnValue = DATE(m.lnYearDateFunction,m.lnMonth,m.lnDay) - ;
                            ROUND((365.25*m.lnGoYears),0)
           ELSE
            luReturnValue = DATE(m.lnYear,m.lnMonth,m.lnDay)
          ENDIF
        ENDIF
      ENDIF  &&& empty(lcString)

    **********************************************
    CASE m.lcDataType = "T" AND NOT m.lcString = "^" 
    **********************************************
      lcString = ALLTRIM(m.lcString)
      IF EMPTY(m.lcString)
        luReturnValue = {//::}
      ELSE
        lcSetDate = SET("DATE")
        lnStrictDate = SET("STRICTDATE")
        SET STRICTDATE TO 0
        junk = "lcString = CTOT(lcString)"   &&& respecting current SET DATE
        &junk  &&& prevent a compiler error when SET STRICTDATE = 1 or 2
        SET STRICTDATE TO (m.lnStrictDate)
        SET DATE AMERICAN 
        IF EMPTY(m.lcString)
          SET DATE (m.lcSetDate) 
          luReturnValue = {//::}
         ELSE
          lcString = TTOC(m.lcString)
          *  mm/dd/yyyy hh:mm:ss
          *  mm/dd/yyyy
          *  mm/dd/yy
          *  mm/dd/yy hh:mm:ss
          DO CASE
            CASE LENC(m.lcString) = 8
              * mm/dd/yy
              ERROR 2034
            CASE LENC(m.lcString) = 10
              * mm/dd/yyyy
              lcString = m.lcString + " 00:00:00"
              * mm/dd/yyyy hh:mm:ss
            CASE SUBSTRC(m.lcString,9,1) = SPACE(1)
              * mm/dd/yy hh:mm:ss
              ERROR 2034
            CASE INLIST(UPPER(RIGHTC(m.lcString,2)),"AM","PM")
              * mm/dd/yyyy hh:mm:ss AM or mm/dd/yyyy hh:mm:ss PM
              lcString = UPPER(m.lcString)
              IF RIGHTC(m.lcString,2) = "PM"
                llPM = .t.
              ENDIF
              * strip off the "AM"/"PM"
              lcString = ALLTRIM(LEFTC(m.lcString,LENC(m.lcString)-2))
              * mm/dd/yyyy hh:mm:ss
            OTHERWISE
              * mm/dd/yyyy hh:mm:ss
          ENDCASE
          lnMonth = INT(VAL(LEFTC(m.lcString,2)))
          lnDay = INT(VAL(SUBSTRC(m.lcString,4,2)))
          lnYear = INT(VAL(SUBSTRC(m.lcString,7,4)))
          lnHours = INT(VAL(SUBSTRC(m.lcString,12,2)))
          lnMin = INT(VAL(SUBSTRC(m.lcString,15,2)))
          lnSec = INT(VAL(SUBSTRC(m.lcString,18,2)))
          IF llPM
            lnHours = m.lnHours + 12
          ENDIF  
          SET DATE (m.lcSetDate)
          luReturnValue = DATETIME(m.lnYear,m.lnMonth,m.lnDay,m.lnHours,m.lnMin,m.lnSec)
        ENDIF
      ENDIF   &&& empty(lcString)
    **********************************************
    CASE lcDataType = "T" AND lcString = "^" 
    **********************************************
      *
      *  "^yyyy-mm-dd hh:mm:ss" strictdate format
      *
      lcString = ALLTRIM(m.lcString)
      lcString = SUBSTRC(m.lcString,2)   &&& remove the leading "^"
      
      IF RIGHTC(m.lcString,2) = "PM"
        llPM = .t.
      ENDIF
      IF RIGHTC(m.lcString,2) = "PM" OR RIGHTC(m.lcString,2) = "AM"
        lcString = LEFTC(m.lcString,LENC(m.lcString)-3)
      ENDIF
            
      IF LENC(m.lcString) = 19   
        *
        *  yyyy-mm-dd hh:mm:ss or any of its variations
        *
       ELSE
        *
        *  you will now crash due to an ambiguous datetime
        *
        ERROR 2034
      ENDIF 

      lnYear = INT(VAL(SUBSTRC(m.lcString,1,4)))
      lnMonth = INT(VAL(SUBSTRC(m.lcString,6,2)))
      lnDay = INT(VAL(SUBSTRC(m.lcString,9,2)))
      lnHours = INT(VAL(SUBSTRC(m.lcString,12,2)))
      lnMin = INT(VAL(SUBSTRC(m.lcString,15,2)))
      lnSec = INT(VAL(SUBSTRC(m.lcString,18,2)))

      IF m.llPM
        lnHours = m.lnHours + 12
      ENDIF
      luReturnValue = DATETIME(m.lnYear,m.lnMonth,m.lnDay,m.lnHours,m.lnMin,m.lnSec)
    **********************************************
    CASE m.lcDataType = "L"   &&& logical
    **********************************************
      lcString = UPPER(ALLTRIM(m.lcString))
      DO CASE
        CASE EMPTY(m.lcString)
          luReturnValue = .F.
        CASE INLIST(m.lcString,"T",".T.","TRUE","Y","YES")
          luReturnValue = .T.
        CASE INLIST(m.lcString,"F",".F.","FALSE","N","NO")
          luReturnValue = .F.
        OTHERWISE
          * unable to process
      ENDCASE
    **********************************************
    CASE m.lcDataType = "E"   &&& expression
    **********************************************
      IF NOT EMPTY(m.lcString)
        llError = .f.
        TRY
        luReturnValue = EVALUATE(ALLTRIM(m.lcString))
        CATCH
        llError = .t.
        ENDTRY
        IF m.llError
          luReturnValue = .NULL.
        ENDIF
      ENDIF 
  ENDCASE
 ELSE
  *
  *  convert the passed value to a string
  *
  DO CASE
    CASE VARTYPE(m.tuValue) = "C"
      luReturnValue = ALLTRIM(m.tuValue)
    CASE INLIST(VARTYPE(m.tuValue),"D","T","X")
      luReturnValue = ALLTRIM(TRANSFORM(m.tuValue,SPACE(0)))
    CASE INLIST(VARTYPE(m.tuValue),"N","Y")
      luReturnValue = ALLTRIM(PADR(m.tuValue,254))
    CASE VARTYPE(m.tuValue) = "L" 
      luReturnValue = IIF(m.tuValue,".T.",".F.")
  ENDCASE
ENDIF
RETURN m.luReturnValue
