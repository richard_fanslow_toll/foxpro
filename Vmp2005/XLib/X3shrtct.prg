*
*  X3SHRTCT.PRG
*  Wrapper to X3SHRTCT.DLL, creates a desktop
*  shortcut in Win95 or WinNT4, Win98, Win2000, WinXP
*
*  Copyright (c) 1997-2005 Visionpace   All Rights Reserved
*                17501 East 40 Hwy., Suite 218
*                Independence, MO 64055
*                816-350-7900 
*                http://www.visionpace.com
*                http://vmpdiscussion.visionpace.com
*  Author: Jim Haentzschel 
*
* Parameters.: tcCommandLine  - Shortcut command line (c:\xx.exe)
*              tcFileName     - Shortcut file name (xx.lnk)
*              tcWhere        - create the .lnk file where (c:\windows\desktop)
*              tcStartInDir   - Shortcut start in directory
*              tcCLparameters - Command Line parameters (-cc:\xx\config.xxd)
*
* Returns....: .T. if runs to completion
*              .F. if any of the first 4 (required) parameters
*                  is not passed as a non-blank character string
*
* Syntax.....: do x3shrtct with "d:\vfp50a\vfp.exe", ;
*                               "XX.lnk", ;
*                               "c:\windows\desktop", ;
*                               "c:\xx", ;
*                               "-cc:\xx\config.xxd"
*
*****************************************************************************

LPARAMETERS tcCommandLine, ;
            tcFileName, ;
            tcWhere, ;
            tcStartInDir, ;
            tcCLParameters

IF type("tcCommandLine") # "C" ;
     OR isnull(tcCommandLine) ;
     OR empty(tcCommandLine) ;
     OR type("tcFileName") # "C" ;
     OR isnull(tcFileName) ;
     OR empty(tcFileName) ;
     OR type("tcWhere") # "C" ;
     OR isnull(tcWhere) ;
     OR empty(tcWhere) ;
     OR type("tcStartInDir") # "C" ;
     OR isnull(tcStartInDir) ;
     OR empty(tcStartInDir) 
  RETURN .F.
ENDIF

tcStartInDir = ALLTRIM(tcStartInDir)
IF RIGHT(tcStartInDir,1) # "\"
	tcStartInDir = tcStartInDir + "\"
ENDIF

DECLARE CreateShellLinkFile IN X3SHRTCT.DLL STRING @tcCommandLine, STRING @tcFileName, STRING @tcWhere, STRING @tcStartInDir, STRING @tcCLParameters

CreateShellLinkFile(tcCommandLine, tcFileName, tcWhere, tcStartInDir, tcCLParameters)

RETURN .t.
