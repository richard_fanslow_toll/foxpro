*
*  XXCBSEEZ.PRG
*  Class Browser add-in to allow instant
*  viewing of the zReadMe method for the
*  selected class 
*
*  Copyright (c) 1997-2005 Visionpace   All Rights Reserved
*                17501 East 40 Hwy., Suite 218
*                Independence, MO 64055
*                816-350-7900 
*                http://www.visionpace.com
*                http://vmpdiscussion.visionpace.com
*  Author:  Drew Speedie



*  To install this add-in (called from the Add-In button/menu),
*  execute:
*    _oBrowser.AddIn("See zReadMe","XXCBSEEZ")
*  XXCBSEEZ.PRG must be in the VFP path; otherwise pass the path
*  plus filename.
*
*  To uninstall this add-in, execute:
*    _oBrowser.AddIn("See zReadMe",.NULL.)
lparameters toBrowser

IF toBrowser.lFileMode 
  toBrowser.MsgBox(toBrowser.cClass + " is a file, not a class",16,"Please Note")
  return
ENDIF  

local lcMethodsField, lnSaveSelect 

lnSaveSelect = select()

GOTO toBrowser.aClassList[toBrowser.nClassListIndex+1,2] IN (toBrowser.cAlias)

lcMethodsField = toBrowser.cAlias + ".Methods"

create cursor XXCBSEEZ (Methods M(4), zReadMe M(4))
select XXCBSEEZ
append blank
replace XXCBSEEZ.Methods with &lcMethodsField

IF empty(XXCBSEEZ.Methods) ;
     OR !"PROCEDURE ZREADME"$upper(XXCBSEEZ.Methods)
  toBrowser.MsgBox(toBrowser.cClass + " either has no zReadMe method, or its zReadMe method is empty",16,"Please Note")
  select (lnSaveSelect)
  use in XXCBSEEZ
  return
ENDIF

local lnSetMemoWidth, ;
      lnCount, ;
      lnLines, ;
      llProczReadMe, ;
      lltext, ;
      lcLine
lnSetMemoWidth = set("MEMOWIDTH")
set memowidth to 100
store 0 to _mline
lnLines = memlines(XXCBSEEZ.Methods)
FOR lnCount = 1 to lnLines
  lcLine = mline(XXCBSEEZ.Methods,1,_MLINE)
  IF !llProczReadMe AND "PROCEDURE ZREADME" $ upper(lcLine)
    llProczReadMe = .t.
  ENDIF
  *IF llProczReadMe AND upper(alltrim(lcLine)) == "TEXT"
  IF llProczReadMe AND upper(alltrim(lcLine)) = "#IF" 
    lltext = .t.
    loop
  ENDIF
  *IF llProczReadMe AND lltext AND upper(alltrim(lcLine)) = "ENDTEXT"
  IF llProczReadMe AND lltext AND upper(alltrim(lcLine)) = "#ENDIF"
    exit
  ENDIF
  IF llProczReadMe AND lltext 
    IF "COPYRIGHT" $ upper(lcLine) OR "AUTHOR" $ upper(lcLine)
     ELSE
      IF empty(lcLine) AND empty(XXCBSEEZ.zReadMe)
       ELSE
        replace XXCBSEEZ.zReadMe with XXCBSEEZ.zReadMe + lcLine + chr(13)
      ENDIF
    ENDIF
  ENDIF
ENDFOR
set memowidth to lnSetMemoWidth

IF empty(alltrim(XXCBSEEZ.zReadMe))
  toBrowser.MsgBox(toBrowser.cClass + ".zReadMe is empty",16,"Please Note")
 ELSE
  define window XXcbseez at 0,0 size 20,70 ;
       title toBrowser.cClassLibrary + "/" + toBrowser.cClass + ".zReadMe" ;
       system float grow zoom close ;
       font "Courier New",10 style "N"
  modi memo XXCBSEEZ.zReadMe nomodify window XXcbseez
  release window XXcbseez
ENDIF  

select (lnSaveSelect)
use in XXCBSEEZ
return
  