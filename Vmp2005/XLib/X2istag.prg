*
*  X2ISTAG.PRG
*  Returns a logical indicating whether or not the passed
*  tag name exists for the alias in the current work area 
*  or an explicitly passed alias
*
*  Copyright (c) 1997-2005 Visionpace   All Rights Reserved
*                17501 East 40 Hwy., Suite 218
*                Independence, MO 64055
*                816-350-7900 
*                http://www.visionpace.com
*                http://vmpdiscussion.visionpace.com
*  Author:  Drew Speedie
*
*
*  Parameters
*    tcTagName (R) Tag name to be verified
*      tcAlias (O) Alias of a table in an open work area
*                  to use for the tag name check
*
parameters tcTagName, tcAlias
private llRetVal, xx
llRetVal = .f.
IF type("tcTagName")#"C" OR isnull(tcTagName) OR empty(tcTagName)
  return .f.
 ELSE
  tcTagName = upper(alltrim(tcTagName))
ENDIF
IF type("tcAlias")#"C" OR empty(tcAlias)
  tcAlias = upper(alltrim(alias()))
 ELSE
  tcAlias = upper(alltrim(tcAlias))
ENDIF
FOR xx = 1 to tagcount(space(0),tcAlias)
  IF empty(tag(xx,tcAlias))
    exit
  ENDIF
  IF tcTagName == upper(alltrim(tag(xx,tcAlias)))
    llRetVal = .t.
    exit
  ENDIF
ENDFOR
return llRetVal




PROCEDURE keywords
* INDEXING|
return