*
*  X3MSGSVC.PRG
*  Wrapper for Steven Black's MSGSVC.PRG message service
*  utility, but ONLY for presenting MessageBox() replacement
*  dialogs, NOT for any of the other message services you
*  can get from MSGSVC.PRG.  If Steven Black's MSGSVC is
*  not installed, the message is presented in a MessageBox()
*  by calling X3WINMSG.PRG.
*
*  Copyright (c) 1997-2005 Visionpace   All Rights Reserved
*                17501 East 40 Hwy., Suite 218
*                Independence, MO 64055
*                816-350-7900 
*                http://www.visionpace.com
*                http://vmpdiscussion.visionpace.com
*  Author:  Drew Speedie
*
*  This wrapper program is necessary in the VMP framework
*  because one call to X3MSGSVC() handles replacement MessageBox()  
*  messages whether Steven Black's MSGSVC.PRG is installed or
*  not.  
*
*  If your VMP application does use Steven Black's MSGSVC, 
*  as we recommend, then you can call MSGSVC() directly in
*  your application-specific code, rather than making calls
*  to X3MSGSVC.PRG
* 
*
*  If Steven Black's INTL Toolkit is installed,
*    call the MSGSVC() function that in turn
*    calls _Screen.oMsgSvc.MsgSvc() to present
*    the message service.  Localization of the
*    message is handled in MSGSVC()/_Screen.oMsgSvc()
*    and obeys settings of _Screen.oINTL
*  Returns the value indicated by the MSGSVC.cRetType field
*
*  If _Screen.oMsgSvc exists, call the MSGSVC()
*    function that in turn calls _Screen.oMsgSvc.MsgSvc()
*    (Steven Black's MSGSVC system is available in the
*    public domain, so you can install it in your app
*    irrespective of the INTL Toolkit.  The public
*    domain version of MSGSVC.PRG and MSGSVC.DBF are
*    included with Visual MaxFrame and Visual MaxFrame
*    Professional)
*  Returns the value indicated by the MSGSVC.cRetType field
*    
*  If neither of the above is true, find the 
*    tcMessageKey value in MSGSVC.DBF and use
*    the values to call X3WINMSG/MESSAGEBOX()
*  Returns the value indicated by the MSGSVC.cRetType field,
*    which should be set to "C" for compatibility
*    with X3WINMSG -- the value returned is the caption
*    of the selected commandbutton
*
*
*  NOTE that X3WINMSG/MESSAGEBOX() does not timeout (until VFP 7.0,
*  but that won't help as long as VMP supports VFP 6.0), while MSGSVC()
*  supports a timeout interval in the MSGSVC.cTimeout field
*
*  NOTE that in order to have an application run either with 
*  MSGSVC() (letting MSGSVC() handle messaging) or without 
*  (letting X3WINMSG handle messaging), you must have a MSGSVC.DBF 
*  populated with field values that produce a MSGSVC() dialog 
*  equivalent to what X3WINMSG would produce in the native 
*  MESSAGEBOX() 
*
*  NOTE that when the value returned by _Screen.oMsgSvc/MSGSVC 
*  is a character string (the Caption of the selected commandbutton), 
*  _Screen.oMsgSvc/MSGSVC returns the text exactly as displayed, 
*  and that value is passed on to the code that calls this X3MSGSVC() 
*  wrapper.  However, beware that X3WINMSG() returns the 
*  upper(alltrim()) of such text. 
*
*  PARAMETERS
*  tcMessageKey (R) String to be SEEK()ed, corresponds
*                     to MSGSVC.cKey
*    tuVariable (O) When populated with a numeric value
*                     between 0 and 99.99, indicates that
*                     the message service is a thermometer
*                     status bar window/form, and presents
*                     the form accordingly
*                   When populated with a string, contains
*                     up to 9 tokens parsed and substituted
*                     in the indicated locations of 
*                     MSGSVC.cOriginal and other languages
*                   For example:
*                     X3MSGSVC("Adding, count","One~Two~Three")
*                   Note that MSGSVC.PRG is really cool in that if 
*                     you pass this parameter and the MSGSVC.cOriginal 
*                     doesn't include the "%C1%, "%C2%", etc. markers, 
*                     MSGSVC() just ignores them.
*                   Ditto if you pass more than there are 
*                     MSGSVC.cOriginal "%C1%", etc. markers.
*  
LPARAMETERS tcMessageKey, tuVariable

LOCAL luRetVal, llMsgSvc, xx, llLockScreen, loActiveForm, ;
      lnFMP, lnSMP, llTextOnly

EXTERNAL FILE Stop.BMP
EXTERNAL FILE Exclaim.BMP
EXTERNAL FILE QMark.BMP
EXTERNAL FILE Info.BMP

IF TYPE("m.oApp.BaseClass") = "C"
  oApp.SetLastUserActivity()
ENDIF

DO CASE
  CASE TYPE("_Screen.oINTL.BaseClass") = "C" ;
     OR TYPE("_Screen.oMsgSvc.BaseClass") = "C" 
    llMsgSvc = .t.
  CASE FILE("MSGSVC.PRG") OR FILE("MSGSVC.FXP")
    llMsgSvc = .t.
  OTHERWISE
    llMsgSvc = .f.
ENDCASE

IF m.llMsgSvc
  DO CASE
    CASE USED("MsgSvc") ;
         AND UPPER(ALLTRIM(LOOKUP(MsgSvc.cFunction,UPPER(ALLTRIM(m.tcMessageKey)),MsgSvc.cFunction,"cKey"))) == "TEXT"
      *
      *  for cFunction = "text" records, we won't be
      *  displaying a message, just RETURNing the text,
      *  so there's no need to check for a place to
      *  display a MSGSVC() dialog
      *
      llTextOnly = .t.
    CASE TYPE("_Screen.oMsgSvc.cAlias") = "C" AND USED(_Screen.oMsgSvc.cAlias) 
      *
      *  for cFunction = "text" records, we won't be
      *  displaying a message, just RETURNing the text,
      *  so there's no need to check for a place to
      *  display a MSGSVC() dialog
      *
      llMsgSvc = .t.
      SELECT (_Screen.oMsgSvc.cAlias)
      LOCATE FOR UPPER(ALLTRIM(cKey)) == UPPER(ALLTRIM(m.tcMessageKey))
      IF UPPER(cFunction) == "TEXT"
        llTextOnly = .t.
      ENDIF
  ENDCASE
ENDIF  
  
IF NOT m.llMsgSvc AND NOT m.llTextOnly AND NOT _Screen.Visible
  *
  *  if _Screen.Visible = .f. AND there is 
  *  no Top-Level form (during initial application
  *  setup, for example), the MSGSVC dialog
  *  will never become visible because its
  *  ShowWindow property = 1-In Top Level Form,
  *  but there's no Top Level Form/_Screen in
  *  which to be Show()n.  In that situation,
  *  llMsgSvc = .F. and the message is presented
  *  to the user in a MESSAGEBOX() dialog, which,
  *  being a Windows system dialog, will be
  *  visible
  *
  FOR xx = 1 TO _Screen.FormCount
    IF TYPE("_Screen.Forms[m.xx].BaseClass") = "C" ;   &&& VFP 7 Task list protects most properties, including BaseClass, so we can't use VARTYPE(), not to mention the fact that this is an X3 routine
        AND UPPER(_Screen.Forms[m.xx].BaseClass) = "FORM" ;   &&& ignore toolbars
        AND _Screen.Forms[m.xx].ShowWindow = 2 ;
        AND _Screen.Forms[m.xx].Visible = .t. ;
        AND (_Screen.Forms[m.xx].Height*_Screen.Forms[m.xx].Width > 600*400)  &&& minimum size to be large enough to have a shot at displaying a MSGSVC() dialog
      *
      *  there's a Top-Level form in which the
      *  message dialog can be displayed, and
      *  it's large enough to have a shot at 
      *  displaying a MSGSVC() dialog -- if not
      *  we'll be doing a MESSAGEBOX() when we
      *  call WinMsg() below
      *
      llMsgSvc = .t.
      EXIT 
    ENDIF
  ENDFOR
ENDIF   

*
*  to protect you against yourself...
*  if you have a LockScreen = .t. in effect,
*  you should always set it to .F. before calling
*  any other form, but, just in case, we'll take
*  care of that for you here...
*
DO CASE
  CASE TYPE("oForms") = "O" AND NOT ISNULL(m.oForms)
    loActiveForm = oForms.ScreenActiveForm()
  CASE TYPE("_Screen.ActiveForm.BaseClass") = "C"
    loActiveForm = _Screen.ActiveForm
    IF UPPER(loActiveForm.BaseClass) = "OLECONTROL"
      DO WHILE .t.
        loActiveForm = loActiveForm.PARENT
        IF UPPER(loActiveForm.BaseClass) == "FORM"
          EXIT 
        ENDIF
      ENDDO
    ENDIF              
  OTHERWISE
    loActiveForm = .NULL.
ENDCASE
IF NOT ISNULL(m.loActiveForm) 
  IF PEMSTATUS(m.loActiveForm,"LockScreen",5)
    llLockScreen = loActiveForm.LockScreen
    loActiveForm.LockScreen = .f.
  ENDIF
ENDIF  

IF m.llMsgSvc
  IF NOT ISNULL(m.loActiveForm)
    IF PEMSTATUS(m.loActiveForm,"MousePointer",5) 
      lnFMP = loActiveForm.MousePointer
      loActiveForm.MousePointer = 0
    ENDIF
  ENDIF
  lnSMP = _Screen.MousePointer
  _Screen.MousePointer = 0
  *
  *  if either Steven Black's INTL Toolkit is installed
  *    OR
  *  MSGSVC()/oMsgSvc is installed
  *                   
  *  then tcMessage should be routed through _Screen.oMsgSvc
  *  (the MSGSVC call below is in case _Screen.oINTL
  *  has been installed, but not _Screen.oMsgSvc, in
  *  which case MSGSVC() will install _Screen.oMsgSvc)
  *
  IF FILE("MsgSvcX.fxp") OR FILE("MsgSvcX.prg")
    *
    *  this strange-looking convention is here to 
    *  support those VMP developers who have a 
    *  need to substitute their own customized 
    *  MSGSVCX() function for the usual MSGSVC() 
    *  function 
    *
    EXTERNAL ARRAY MsgSvcX   &&& prevent the Project Manager from complaining if it can't find MSGSVCX.PRG
    luRetVal = MsgSvcX(m.tcMessageKey, m.tuVariable)
   ELSE
    *
    *  normal/default call to MSGSVC()
    *
    luRetVal = MSGSVC(m.tcMessageKey,m.tuVariable)
  ENDIF
  IF TYPE("luRetVal") = "C"
    luRetVal = ALLTRIM(m.luRetVal)
  ENDIF
  IF TYPE("lnFMP") = "N" AND NOT ISNULL(m.loActiveForm) 
    loActiveForm.MousePointer = m.lnFMP
  ENDIF
  _Screen.MousePointer = m.lnSMP
 ELSE
  *
  *  no MSGSVC/INTL services are installed,
  *  route the message through X3WINMSG()
  *
  luRetVal = WINMSG(m.tcMessageKey, m.tuVariable) 
ENDIF

IF TYPE("m.oApp.BaseClass") = "C"
  oApp.SetLastUserActivity()
ENDIF

IF m.llLockScreen AND NOT ISNULL(m.loActiveForm)
  *
  *  put it back the way we found it, although
  *  at this point, that's not necessarily the
  *  greatest visual effect (but then, you shouldn't
  *  have called X3MSGSVC with a LockScreen in 
  *  effect anyway)
  *
  loActiveform.LockScreen = .t.
ENDIF  

RETURN m.luRetVal



**********************************************************
PROCEDURE WinMsg
**********************************************************
LPARAMETERS tcMessageKey, tuVariable
LOCAL llError, lcOnError, lcMsgSvc, lnSelect, lcRetVal
lnSelect = SELECT(0)
IF TYPE("_Screen.oMsgSvc.cAlias") = "C" AND USED(_Screen.oMsgSvc.cAlias)
  lcMsgSvc = _Screen.oMsgSvc.cAlias
 ELSE
  lcMsgSvc = "MSGSVC"
ENDIF
lcOnError = ON("ERROR")
IF NOT USED(m.lcMsgSvc)   &&& lcMsgSvc = "MSGSVC"
  ON ERROR llError = .t.
  USE MSGSVC IN 0 SHARED
  ON ERROR &lcOnError
  IF m.llError
    MESSAGEBOX(m.tcMessageKey + CHR(13) + CHR(13) + ;
               CHRTRAN(TRANSFORM(m.tuVariable),"~",CHR(13)), ;
               48, ;
               "Please Note")
    RETURN "OK"
  ENDIF
ENDIF
=SEEK(UPPER(m.tcMessageKey),m.lcMsgSvc,"CKEY")
IF NOT FOUND(m.lcMsgSvc)
  m.cKey       = m.tcMessageKey
  m.cFunction  = "OK"
  m.cRetType   = "C"
  m.cGUIVisual = "exclaim.bmp"
  m.cOriginal  = m.tcMessageKey
  m.cBell      = "Y"
  llError = .f.
  ON ERROR llError = .t.
  INSERT INTO (m.lcMsgSvc) FROM MEMVAR 
  ON ERROR &lcOnError
  IF m.llError
    MESSAGEBOX(m.tcMessageKey + CHR(13) + CHR(13) + ;
               CHRTRAN(TRANSFORM(m.tuVariable),"~",CHR(13)), ;
               48, ;
               "Please Note")
    SELECT (m.lnSelect)
    RETURN "OK"
  ENDIF
ENDIF
SELECT (m.lcMsgSvc)
DO CASE
  CASE TYPE("tuVariable") = "N" ;
       AND NOT ISNULL(m.tuVariable) ;
       AND BETWEEN(m.tuVariable,0,99.99)
    *
    *  thermometer bar...
    *     
    lcRetVal = .t.
  CASE UPPER(cFunction) = "TEXT"
    lcRetVal = cOriginal
  OTHERWISE
    LOCAL lcMessage, lcTitle, lcButtons, lcIcon, llSilent
    lcMessage = ALLTRIM(cOriginal)
    IF ("%C%" $ UPPER(m.lcMessage) OR "%C1%" $ UPPER(m.lcMessage)) ;
         AND TYPE("tuVariable") = "C"
      *
      *  process tokens
      *
      =ProcessTokens(@m.lcMessage,m.tuVariable)
    ENDIF
    lcTitle   = IIF(EMPTY(cTitle),.f.,ALLTRIM(cTitle))
    lcButtons = ALLTRIM(cFunction)
    lcIcon    = UPPER(ALLTRIM(cGUIVisual))
    DO CASE
      CASE m.lcIcon = "QMARK.BMP"
        lcIcon = "?"
      CASE m.lcIcon = "STOP.BMP"
        lcIcon = "S"
      OTHERWISE
        lcIcon = "!"
    ENDCASE
    llSilent = NOT INLIST(UPPER(ALLTRIM(cBell)),"T","Y")
    lcRetVal = X3WINMSG(m.lcMessage, ;
                        m.lcTitle, ;
                        m.lcButtons, ;
                        m.lcIcon, ;
                        m.llSilent)
ENDCASE
SELECT (m.lnSelect)
RETURN m.lcRetVal



**********************************************************
PROCEDURE ProcessTokens
**********************************************************
LPARAMETERS tcMessage, tcTokens
LOCAL lcTokens
lcTokens = ALLTRIM(m.tcTokens)
IF NOT LEFTC(m.lcTokens,1) = "~"
  lcTokens = "~" + m.lcTokens
ENDIF
IF LENC(m.lcTokens) > 1 AND RIGHTC(m.lcTokens,1) = "~"
  lcTokens = LEFTC(m.lcTokens,LENC(m.lcTokens)-1)
ENDIF
LOCAL xx, lnStart, lnStop
FOR xx = 1 TO OCCURS("~",m.lcTokens)
  DO CASE
    *******************************************
    CASE xx = 1
    *******************************************
      * first token
      lnStart = 2
      IF OCCURS("~",m.lcTokens) = 1
        *
        *  only 1 token
        *
        lnStop = LENC(m.lcTokens)
       ELSE
        lnStop = AT_C("~",m.lcTokens,2)-1
      ENDIF
      tcMessage = STRTRAN(m.tcMessage, ;
                          IIF(OCCURS("%C%",m.lcTokens)>0,"%C%","%C1%"), ;
                          SUBSTRC(m.lcTokens, ;
                                  m.lnStart, ;
                                  m.lnStop-m.lnStart+1))
    *******************************************
    CASE xx = OCCURS("~",m.lcTokens)
    *******************************************
      * last token
      lnStart = AT_C("~",m.lcTokens,m.xx)+1
      tcMessage = STRTRAN(m.tcMessage, ;
                          "%C" + ALLTRIM(STR(m.xx)) + "%", ;
                          SUBSTRC(m.lcTokens,m.lnStart))
    *******************************************
    OTHERWISE
    *******************************************
      * non-first, non-last token
      lnStart = AT_C("~",m.lcTokens,m.xx)+1
      lnStop  = AT_C("~",m.lcTokens,m.xx+1)-1     
      tcMessage = STRTRAN(m.tcMessage, ;
                          "%C" + ALLTRIM(STR(m.xx)) + "%", ;
                          SUBSTRC(m.lcTokens,m.lnStart,m.lnStop-m.lnStart+1))
  ENDCASE                           
ENDFOR
RETURN 
