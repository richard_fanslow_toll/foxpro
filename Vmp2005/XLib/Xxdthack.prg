*
*  XXDTHack.PRG
*  Fire up an instance of XXTOOLS.VCX/frmXXDTHack to "Hack" 
*  an .SCX or .VCX, mainly for the purpose of re-defining 
*  the ParentClass of members.
*
*  Copyright (c) 1997-2005 Visionpace   All Rights Reserved
*                17501 East 40 Hwy., Suite 218
*                Independence, MO 64055
*                816-350-7900 
*                http://www.visionpace.com
*                http://vmpdiscussion.visionpace.com
*  Author:  Drew Speedie  
*

*
*  lParameters
*    tcFileName (O) The .SCX or .VCX file name
*
LPARAMETERS tcFileName
CLOSE DATABASES ALL 
CLOSE TABLES 

IF NOT X7ISAPIF("MessageBeep")
  DECLARE Long MessageBeep IN USER32.DLL Long uType 
ENDIF

SET CLASSLIB TO xxtools.vcx ADDITIVE 
PUBLIC xxdthack
xxdthack = CREATEOBJECT("frmHackSV",tcFileName)
IF VARTYPE(xxdthack) = "O" 
  xxdthack.Show()
ENDIF

RETURN 
