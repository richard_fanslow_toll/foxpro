*
*  XXDT.PRG
*  DeveloperTool:  Summon a dialog from which all the
*  VMP developer tools XXDT*.PRG can be reviewed and/or
*  executed
*
*  Copyright (c) 1997-2005 Visionpace   All Rights Reserved
*                17501 East 40 Hwy., Suite 218
*                Independence, MO 64055
*                816-350-7900 
*                http://www.visionpace.com
*                http://vmpdiscussion.visionpace.com
*  Author:  Drew Speedie, special thanks to Franz M�ller
*
*  To automatically have your own (non-VMP) developer
*  tools added to the list in this tool, just create
*  a procedural hook VMPDTADDONS.PRG, which is called
*  here if it exists.  In VMPDTADDONS.PRG, just add your
*  own developer tools to the paPRGs array, something
*  like this:
*     *  VMPDTADDONS.PRG
*     *  called from XXDT.PRG
*     LOCAL lnRows, lnCols
*     lnRows = ALEN(paPrgs,1)
*     lnCols = ALEN(paPrgs,2)
*     DIMENSION paPrgs[lnRows+1,lnCols]
*     paPrgs[lnRows+1,1] = "MYDEVTOOL.PRG"
*     *  EOF VMPDTADDONS.PRG
*  Your VMPDTADDONS.PRG can be located anywhere in your
*  VFP path.  If the developer tools you add to the 
*  paPRGs is not in the path, you can include the full
*  path when you add the filenames to the paPRGs array.
*  However, such added developer tools will not be
*  able to be Viewed by the XXDT interface.
*
SET ASSERTS ON

IF VARTYPE(oApp) = "O" ;
     OR VARTYPE(oLib) = "O" ;
     OR VARTYPE(oForms) = "O" ;
     OR TYPE("_Screen.Activeform.BaseClass") = "C"
  *
  *  looks like a standalone VMP form, a VMP application,
  *  or some VFP form is running at the moment -- this
  *  utility is intended to be run when none of those are
  *  going on
  *
  MESSAGEBOX("The XXDT.PRG is intended to be run outside " + ;
             "the context of a VMP app, VMP standalone form, " + ;
             "or any other executing VFP code.  Please " + ;
             "try XXDT again when there is nothing else " + ;
             "going on in the VFP development environment.", ;
             48,"Please Note")
  RETURN               
ENDIF

CLEAR 

*
*  determine the XLIB path --
*  the path where XXDT.PRG is located
*
local lcXDir, lcSetTalk
*!*	lcXDir = sys(16,0)
*!*	lcXDir = leftc(lcXDir,at_c("\",lcXDir,occurs("\",lcXDir)))
lcXDir = JUSTPATH(FULLPATH("XXDT.PRG"))
lcSetTalk = set("TALK")
set talk off
*
*  find all XXDT*.PRGs
*
local lcCurDir
lcCurDir = FULLPATH(CURDIR()) 
cd (lcXDir)
private paPRGs 
IF adir(paPRGs,"XXDT*.PRG") = 0 ;
     OR (alen(paPRGs,1) = 1 AND paPRGs[1,1] = "XXDT.PRG")
  MESSAGEBOX("No XXDT*.PRGs found",0,"Please Note")
  SET TALK &lcSetTalk
  return
ENDIF
LOCAL xx
FOR xx = ALEN(paPrgs,1) TO 1 STEP -1
  IF INLIST(paPrgs[xx,1],"XXDT.PRG", ;
                         "XXDTSRCH.PRG", ;
                         "XXDTEPEM.PRG", ;
                         "XXDTCNTC.PRG", ;
                         "XXDTPOPC.PRG", ;
                         "XXDTXPLR.PRG", ;
                         "XXDTDBFL.PRG", ;
                         "XXDTXREL.PRG", ;
                         "XXDTAPEM.PRG", ;
                         "XXDTFC.PRG")
    ADEL(paPrgs,xx)
    DIMENSION paPrgs[alen(paPrgs,1)-1,alen(paPrgs,2)]
  ENDIF
ENDFOR
CD (lcCurDir)
*
*  here's your chance to add any of your own Developer Tools
*
LOCAL lcAddOnHookFile
lcAddOnHookFile = ""
IF file("VMPDTADDONS.PRG")
  lcAddOnHookFile = LOCFILE("VMPDTADDONS.PRG")
ENDIF
IF file("VMPDTADDONS.FXP") AND EMPTY(lcAddOnHookFile)
  lcAddOnHookFile = LOCFILE("VMPDTADDONS.FXP")
ENDIF
IF NOT EMPTY(lcAddOnHookFile)
  do (lcAddOnHookFile)
ENDIF
=asort(paPrgs)

*
*  create a form from which to select one
*
private pcPRG
pcPRG = space(0)
local loForm
loForm = createobject("frmXXDTPicker")
loForm.Show()
IF isnull(pcPRG) OR empty(pcPRG)
  ACTIVATE WINDOW command
  KEYBOARD "{CTRL+F2}" PLAIN clear
  SET TALK &lcSetTalk
  RETURN 
ENDIF
loForm = .NULL.
RELEASE loForm


*
*  execute the selection
*
*!*	xx = "do " + pcPRG
*!*	activate window Command   &&& in case VFP 7 dockable windows are up
*!*	keyboard xx + chr(13) plain clear
*!*	*KEYBOARD '{CTRL+F2}' + ALLTRIM(xx) + chr(13) plain clear

PUBLIC goXXDT
goXXDT = CREATEOBJECT("HandGrenade",pcPRG)

ACTIVATE WINDOW command
KEYBOARD "{CTRL+F2}" PLAIN clear

SET TALK &lcSetTalk

RETURN 


DEFINE CLASS frmXXDTPicker AS FORM

	DataSession = 2
	DoCreate = .T.
	BorderStyle = 2
	Caption = "Available XXDT*.PRG Developer Tools"
	WindowType = 1
	Icon = "VMP.ICO"
	icretval = (space(0))
    MinButton = .f.
    MaxButton = .f.
	Name = "Form1"

	ADD OBJECT list1 AS listbox WITH ;
		ColumnCount = 1, ;
		RowSourceType = 6, ;
		RowSource = "XXDTPRGS.FileName", ;
		Height = 280, ;
		Left = 16, ;
		TabIndex = 1, ;
		Top = 16, ;
		Width = 220, ;
		IntegralHeight = .T., ;
        ItemTips = .t., ;
		Name = "List1" 

	ADD OBJECT edit1 AS editbox WITH ;
		Height = 280, ;
		Left = 252, ;
		ReadOnly = .T., ;
		TabIndex = 2, ;
		Top = 16, ;
		Width = 360, ;   
		DisabledBackColor = RGB(255,255,255), ;
		ControlSource = "XXDTPRGS.Comments", ;
		IntegralHeight = .T., ;
		Name = "Edit1"

	ADD OBJECT cmdok AS cmdXXDT WITH ;
		Caption = "\<OK", ;
		Cancel = .T., ;
		Default = .T., ;
		TabIndex = 5, ;
		Name = "cmdOK"

	ADD OBJECT cmdRun AS cmdXXDT WITH ;
		Caption = "Run", ;
		TabIndex = 3, ;
		Name = "cmdRun"

	ADD OBJECT cmdView AS cmdXXDT WITH ;
		Caption = "View", ;
		TabIndex = 4, ;
		Name = "cmdView"
		
	ADD OBJECT lblInfo as Label WITH ;
		Caption = "To add in your own non-VMP developer tools " + ;
		          "to this interface, see the header comments of " + ;
		          "XXDT.PRG", ;
		WordWrap = .t., ;
		Height = 48, ;
		Width = 224

	PROCEDURE Unload
		return THIS.icRetVal
	ENDPROC

	PROCEDURE Load
		set talk off
        local lcSetSafety
        lcSetSafety = set("SAFETY")
        set safety off
		create cursor XXDTPRGS (FileName C(40), Comments M)
		local xx
		FOR xx = 1 to alen(paPRGs,1)
          IF paPRGs[xx,1] # "XXDT.PRG"
    		  insert into XXDTPRGS ;
    		  (FileName) values (paPRGs[xx,1])
    		  THIS.GetComments(paPRGs[xx,1])
	      ENDIF
		ENDFOR
        IF lcSetSafety = "ON"
          set safety on
        ENDIF
        select XXDTPRGS
        index on UPPER(FileName) tag FileName
        LOCATE 
	ENDPROC

	PROCEDURE Init
        WITH THIS
        IF SYSMETRIC(1) > 800
          .List1.Height = .List1.Height + 154
          .Edit1.Height = .List1.Height
          .Edit1.Width = .Edit1.Width + 40
        ENDIF
        LOCAL lnTopOfButtons, lnMargin
        lnTopOfButtons = .List1.Top + .List1.Height + .List1.Left
        lnMargin = .List1.Left
        .Height = lnTopOfButtons + .cmdOK.Height + lnMargin
        .Width = .Edit1.Left + .Edit1.Width + lnMargin
        .cmdRun.Top = lnTopOfButtons
        .cmdView.Top = lnTopOfButtons
        .cmdOK.Top = lnTopOfButtons
        .cmdRun.Left = lnMargin
        .cmdView.Left = .cmdRun.Left + .cmdRun.Width + lnMargin
        .cmdOK.Left = .Width - .cmdOK.Width - lnMargin
		.AutoCenter = .t.
		.Top = (_Screen.Height-.Height)/4
		ENDWITH
        IF FILE("VMPTiny.GIF") 
  		  THIS.AddObject("imgVMP","Image")
		  THIS.imgVMP.Picture = "VMPTiny.GIF"
		  THIS.imgVMP.Top = THIS.cmdOK.Top - 6
          *  center the image between the View and OK buttons
		  LOCAL lnView, lnOK
		  lnView = THIS.cmdView.Left + THIS.cmdView.Width
		  lnOK = THIS.cmdOK.Left
		  THIS.imgVMP.Left =  lnView + ((lnOK-lnView-THIS.imgVMP.Width)/2)
		  THIS.imgVMP.Visible = .t. 
		  THIS.lblInfo.Left = THIS.cmdView.Left + THIS.cmdView.Width + 12
		  THIS.lblInfo.Top = THIS.imgVMP.Top - 4
          THIS.imgVMP.Left = THIS.lblInfo.Left + THIS.lblInfo.Width
         ELSE
          THIS.lblInfo.Left = THIS.cmdView.Left + THIS.cmdView.Width + 68
          THIS.lblInfo.Top = THIS.cmdOK.Top - 12
		ENDIF  
	ENDPROC

*!*	    PROCEDURE GetComments
*!*	        LPARAMETERS tcFileName
*!*	        LOCAL lnLineCount, lcLine, llValidLine, laLines[1], ;
*!*	              lnStart, lnCount
*!*	        lnCount = 0
*!*	        ALINES(laLines,FILETOSTR(tcFileName))
*!*	        FOR lnLineCount = 1 TO ALEN(laLines,1)
*!*	          lcLine = laLines[lnLineCount]
*!*	          IF lcLine == "*" OR EMPTY(lcLine)
*!*	            lcLine = SPACE(0)
*!*	           ELSE
*!*	            lcLine = SUBSTRC(lcLine,2)
*!*	            lcLine = SPACE(1) + ALLTRIM(lcLine)
*!*	            IF lnCount = 1
*!*	              lcLine = ALLTRIM(lcLine)
*!*	            ENDIF
*!*	          ENDIF
*!*	          IF UPPER(tcFileName) $ UPPER(lcLine) ;
*!*	               AND NOT llValidLine
*!*	            lnStart = AT_C(UPPER(tcFileName),UPPER(lcLine))
*!*	            replace XXDTPRGS.FileName WITH ;
*!*	                SUBSTRC(lcLine,lnStart,LENC(tcFileName)) ;
*!*	                IN XXDTPRGS            
*!*	            llValidLine = .t.
*!*	          ENDIF
*!*	          IF llValidLine
*!*	            IF EMPTY(lcLine)
*!*	              EXIT 
*!*	            ENDIF
*!*	            lnCount = lnCount + 1
*!*	            replace XXDTPRGS.Comments ;
*!*	                 WITH XXDTPRGS.Comments + lcLine + IIF(lnCount=1,CHR(13)+CHR(10),SPACE(0)) ;
*!*	                 IN XXDTPRGS
*!*	          ENDIF  
*!*	        ENDFOR
*!*	        replace XXDTPRGS.Comments ;
*!*	             WITH ALLTRIM(XXDTPRGS.Comments) ;
*!*	             IN XXDTPRGS
*!*	    ENDPROC
    
    PROCEDURE GetComments
        LPARAMETERS tcFileName
        LOCAL lcLine, laLines[1], lnCount, llNewP, llCopyRight
        lnCount = 0
        ALINES(laLines,FILETOSTR(tcFileName),.t.)
        FOR EACH lcLine IN laLines
          DO CASE
            CASE UPPER(ALLTRIM(CHRTRAN(lcLine,"*",SPACE(0)))) = "COPYRIGHT"
              *  skip the Copyright block
              llCopyRight = .t.
              LOOP 
            CASE llCopyRight AND NOT EMPTY(ALLTRIM(CHRTRAN(lcLine,"*",SPACE(0))))
              *  Copyright line
              LOOP 
            CASE llCopyRight AND lcLine == "*"
              *  first blank line after the Copyright
              llCopyRight = .f.
              LOOP 
            CASE lcLine == "*" AND EMPTY(XXDTPRGS.Comments)
              *  ignore initial comment lines
              LOOP 
            CASE lcLine == "*"
              *  carriage return/line feed
              replace XXDTPRGS.Comments WITH ;
                      XXDTPRGS.Comments + ;
                      CHR(13) + CHR(10) + ;
                      CHR(13) + CHR(10) 
              llNewP = .t.
              LOOP 
            CASE EMPTY(lcLine) ;
                 OR UPPER(ALLTRIM(lcLine)) = "LPARAMETERS " ;
                 OR NOT lcLine = "*"
              *  first line of real code, we're done
              EXIT 
            CASE lcLine = "*!*"
              *  code comment line
              replace XXDTPRGS.Comments WITH ;
                      XXDTPRGS.Comments + ;
                      CHR(13) + CHR(10) + ;
                      lcLine 
              LOOP 
            OTHERWISE
              lcLine = ALLTRIM(SUBSTRC(lcLine,2))
              DO CASE
                CASE lcLine = "- " ;
                     OR ;
                     (ISDIGIT(LEFTC(lcLine,1)) AND SUBSTRC(lcLine,2,1) = "-")
                  *  itemized list item
                  replace XXDTPRGS.Comments WITH ;
                          XXDTPRGS.Comments + ;
                          CHR(13) + CHR(10) + ;
                          CHR(13) + CHR(10) + ;
                          lcLine 
                  LOOP 
                CASE llNewP
                  *  first line after a blank line
                  lcLine = ALLTRIM(lcLine)
                  llNewP = .f.
                OTHERWISE
                  * regular line
                  lcLine = SPACE(1) + lcLine
                  llNewP = .f.
              ENDCASE
          ENDCASE
          IF UPPER(ALLTRIM(lcLine)) == UPPER(tcFileName) 
            *
            *  update XXDTPRGS.FileName with the
            *  .PRG filename in the header comments,
            *  to get better capitalization
            *   
            replace XXDTPRGS.FileName WITH ALLTRIM(lcLine) IN XXDTPRGS
            llNewP = .t.   
          ENDIF
          lnCount = lnCount + 1
          replace XXDTPRGS.Comments ;
               WITH XXDTPRGS.Comments + lcLine + IIF(lnCount=1,CHR(13)+CHR(10),SPACE(0)) ;
               IN XXDTPRGS
        ENDFOR
        replace XXDTPRGS.Comments ;
             WITH ALLTRIM(XXDTPRGS.Comments) ;
             IN XXDTPRGS
    ENDPROC

    PROCEDURE List1.Init
      THIS.Value = 1 
    ENDPROC

    PROCEDURE List1.InteractiveChange
      THISFORM.Edit1.Refresh()
      *
      *  once we make the first change, put the
      *  filename in the form.Caption
      *
      THISFORM.Caption = ALLTRIM(XXDTPRGS.FileName)
    ENDPROC

    PROCEDURE List1.Valid
      THISFORM.cmdRun.SetFocus()
      KEYBOARD "{ENTER}" PLAIN CLEAR 
    ENDPROC

	PROCEDURE cmdok.Click
        pcPRG = .NULL.
		THISFORM.icRetVal = .NULL.
		THISFORM.Release()
	ENDPROC

	PROCEDURE cmdRun.Click
        pcPRG = THISFORM.List1.DisplayValue
		THISFORM.icRetVal = space(0)
		THISFORM.Release()
	ENDPROC

    PROCEDURE cmdView.Click
      modify command (ALLTRIM(XXDTPRGS.FileName)) nomodify nomenu range 1,1
    ENDPROC

ENDDEFINE

DEFINE CLASS cmdXXDT as CommandButton
	Height = 27
	Width = 84 
ENDDEFINE



DEFINE CLASS HandGrenade as Timer
Enabled = .t.
Interval = 100
icProgram = SPACE(0)

PROCEDURE Init
LPARAMETERS tcProgram
THIS.icProgram = tcProgram
ENDPROC

PROCEDURE Timer
THIS.Enabled = .f.
THIS.Interval = 0
DO (THIS.icProgram)
RELEASE THIS
ENDPROC

ENDDEFINE
