Close All
Clear All
*Set Step On
Set Safety Off
Set Hours To 24
Set Date Japan
Set Century On
Set TablePrompt Off
On Error Do ErrRoutine
Set Deleted On
Set Multilocks On

Public lluseError,llProcessDeleted,lcFanzKey,lcFanzKey2
lluseError = .F.
If Inlist(Minute(Datetime()),00,01,02,03,04,05)
	llProcessDeleted = .T.
Else
	llProcessDeleted = .F.
Endif
*llProcessDeleted = .T.
*_Screen.WindowState= 1
*
Open Database F:\3PL\Data\FanzCheck.Dbc Exclusive

If !lluseError
	If File("f:\3pl\data\edi_trigger.DBF") && Check File/Network exists
		lcDriveLetter = "F:\"
		lcPathinfo    = "3PL\DATA\"
		lcDbfName     = "Edi_Trigger"
		lcTagName     = ""
		lcShared      = "SHARED"
		lcAlias       = "Edi_trigger"
		Use F:\3PL\Data\edi_trigger.Dbf	In 0 Share Alias Triggers
		*Do Useit With lcDriveLetter,lcPathinfo,lcDbfName,lcTagName,lcShared,lcAlias
		lluseError = .F.
	Endif
Endif
If !lluseError
	If File("f:\3pl\data\edi_trigger_Log.DBF") && Check File/Network exists
		lcDriveLetter = "F:\"
		lcPathinfo    = "3PL\DATA\"
		lcDbfName     = "Edi_Trigger_log"
		lcTagName     = ""
		lcShared      = "SHARED"
		lcAlias       = "Edi_trigger"
		Use F:\3PL\Data\edi_trigger_log.Dbf	Order FanzKey In 0 Exclusive Alias Logs
		*Do Useit With lcDriveLetter,lcPathinfo,lcDbfName,lcTagName,lcShared,lcAlias
		lluseError = .F.
	Endif
Endif
=CursorSetProp("Buffering",3,"Logs")
=CursorSetProp("Buffering",3,"Triggers")
If !lluseError
	*Select edi_trigger_log
	*Delete For Empty(when_proc) And trig_time>=Datetime()-(3600*96) And trig_time <=Ctot(Dtoc(Date()+1) + ' 00:00:00 AM')

	* Create 96 hour window
	* building a unique string to matching
	********
	If llProcessDeleted = .T.
		Set Deleted Off
		Select ;
			Alltrim(edi_type) + '-' + Padl(accountid,4,"0") + '-' + Iif(Empty(office),'-',Alltrim(office)) + '-' +  Alltrim(Str(wo_num)) + '-' + Alltrim(bol) + '-' + Alltrim(Ship_ref) + '-' + Strtran(Strtran(Strtran(Ttoc(trig_time),"/",""),":","")," ","") As 'FanzKey';
			,Strtran(Strtran(Strtran(Ttoc(trig_time),"/",""),":","")," ","") As 'TimeStamp';
			,Iif(!Empty(when_Proc),trig_time-when_Proc,trig_time-Datetime())  As 'Delayed';
			,* ;
			FROM Triggers ;
			WHERE trig_time>=Datetime()-((3600*24)*5);
			AND trig_time <=Ctot(Dtoc(Date()+1) + ' 00:00:00 AM');
			AND edi_type='945';
			AND Deleted();
			ORDER By trig_time ;
			INTO Cursor Tmp_Triggers
	Else
		Select ;
			Alltrim(edi_type) + '-' + Padl(accountid,4,"0") + '-' + Iif(Empty(office),'-',Alltrim(office)) + '-' +  Alltrim(Str(wo_num)) + '-' + Alltrim(bol) + '-' + Alltrim(Ship_ref) + '-' + Strtran(Strtran(Strtran(Ttoc(trig_time),"/",""),":","")," ","") As 'FanzKey';
			,Strtran(Strtran(Strtran(Ttoc(trig_time),"/",""),":","")," ","") As 'TimeStamp';
			,Iif(!Empty(when_Proc),trig_time-when_Proc,trig_time-Datetime())  As 'Delayed';
			,* ;
			FROM Triggers ;
			WHERE trig_time>=Datetime()-((3600*24)*5);
			AND trig_time <=Ctot(Dtoc(Date()+1) + ' 00:00:00 AM');
			AND edi_type='945';
			ORDER By trig_time ;
			INTO Cursor Tmp_Triggers
	Endif
	********
	If Used("Triggers")
		Use In  Triggers
	Endif
	********
	Select Tmp_Triggers
	********
	Do While !Eof()
		Clear
		Wait Window "Now Processing : "  + Padl(Recno(),10,"0") + " of " + Padl(Reccount(),10,"0") Nowait
		lcFanzKey   = Alltrim(Tmp_Triggers.FanzKey)
		lcFanzKey2  = Alltrim(Tmp_Triggers.FanzKey)
		lcTimeStamp = Tmp_Triggers.Timestamp
		llprocessed = Tmp_Triggers.processed
		llErrorFlag = Tmp_Triggers.errorFlag
		lcFinStatus = Tmp_Triggers.Fin_status
		Scatter To arrTriggers
		Select Logs
		Seek lcFanzKey
		********
		If !Found()
			*insert data into Logs
			lProp = CursorSetProp("Buffering",3,"Logs")
			Append Blank
			Gather From arrTriggers
			Use FanzCheck!Insert Nodata In 0 Alias InsertData
			Select InsertData
			lProp = CursorSetProp("Buffering",3,"InsertData")
			Append Blank
			Gather From arrTriggers
			lUpdated=Tableupdate()
			If llProcessDeleted = .T.
				ldError_At  = Iif(Empty(Logs.when_Proc),Datetime(),Logs.when_Proc)
				lcError_Msg = "DELETED-" + Alltrim(Logs.Fin_status)
				Replace ;
					errorFlag With .F.,;
					Fin_status With lcError_Msg,;
					error_at With ldError_At,;
					error_msg With lcError_Msg,;
					when_Proc With Datetime()
				Replace Delayed With Iif(!Empty(when_Proc),trig_time-when_Proc,trig_time-Datetime())
			Endif
			Do CleanupData
		Else
			*?Iif(errorFlag,"True ","False") + "  Error Flag " + Iif(llErrorFlag,"True ","False")
			*?Iif(llProcessDeleted,"True ","False") + "  Process Deleted"
			If llProcessDeleted = .T.
				Do Case
				Case errorFlag = .T. And llErrorFlag = .F.
					Do One
				Case errorFlag = .T. And llErrorFlag = .T.
					Do Two
				Case errorFlag = .F. And llErrorFlag = .F.
					Set Step On
				Case errorFlag = .F. And llErrorFlag = .T.
					If Empty(error_at)
						Set Step On
					ELSE
						DO One
					Endif
				Otherwise
					Set Step On
				Endcase
			Else
				Do Case
				Case Fin_status # lcFinStatus And processed=.t. And llprocessed=.t.
					DO Two
				Case ALLTRIM(Fin_status) # ALLTRIM(lcFinStatus) And processed=.F. And llprocessed=.F.
					SET STEP on
					DO two
				Case Fin_status # lcFinStatus And processed=.F. And llprocessed=.T.
					Do Two
				Case errorFlag = .T. And llErrorFlag = .F.
					Do two
				Case errorFlag = .T. And llErrorFlag = .T.
					Do Two
				Case errorFlag = .F. And llErrorFlag = .F.
					*IF !INLIST(fin_status,'945 CREATED','GSI FILE-CREATED','NO 945 POSSIBLE - NO CSZ INFO')
					*Set Step On
					*endif
				Case errorFlag = .F. And llErrorFlag = .T.
					Do Two
				Otherwise
					Set Step On
				Endcase

			Endif
		Endif
		********
		Release arrTriggers,lcFanzKey,lcTimeStamp,llErrorFlag,ldError_At,lcError_Msg
		********
		Select Tmp_Triggers
		Skip
		Loop
	Enddo
Endif
Do CleanDates

Close All
Clear All
_Screen.WindowState = 0
Quit

******************************************************************************************************************************************************************************************
Procedure Useit
* setup open data safely
lluseError = .F.
Return lluseError

******************************************************************************************************************************************************************************************
Procedure ErrRoutine
*setup some recording
Aerror(arrError)
lcMsg1 = Alltrim(Padl(Iif(Isnull(arrError[1,1]),"",arrError[1,1]),250," "))
lcMsg2 = Alltrim(Padl(Iif(Isnull(arrError[1,2]),"",arrError[1,2]),250," "))
lcMsg3 = Alltrim(Padl(Iif(Isnull(arrError[1,3]),"",arrError[1,3]),250," "))
lcMsg4 = Alltrim(Padl(Iif(Isnull(arrError[1,4]),"",arrError[1,4]),250," "))
lcMsg5 = Alltrim(Padl(Iif(Isnull(arrError[1,5]),"",arrError[1,5]),250," "))
lcMsg6 = Alltrim(Padl(Iif(Isnull(arrError[1,6]),"",arrError[1,6]),250," "))
lcMsg7 = Alltrim(Padl(Iif(Isnull(arrError[1,7]),"",arrError[1,7]),250," "))
lcBody = "EDI Lookback Error: " +Ttoc(Datetime()) + " : " + Iif(Type("lcFanzKey")="C",lcFanzKey,"") + Chr(13);
	+ " Array Value 01 : " + lcMsg1 + Chr(13) ;
	+ " Array Value 02 : " + lcMsg2 + Chr(13) ;
	+ " Array Value 03 : " + lcMsg3 + Chr(13) ;
	+ " Array Value 04 : " + lcMsg4 + Chr(13) ;
	+ " Array Value 05 : " + lcMsg5 + Chr(13)
If At("Update conflict in cursor 'INSERTDATA'",lcMsg2)>0
	Select InsertData
	=Tablerevert()
Endif

lcFrom     = "NoReply <noreply@tollgroup.com>"
lcsubject= "EDI Lookback Error: " +Ttoc(Datetime()) + " : " + Iif(Type("lcFanzKey")="C",lcFanzKey,"")
lcattach = ""
lcTo = "richard.fanslow@tollgroup.com"
Set Step On
*sendMail_au with lcTO,lcFrom,lcSubject,lcCC,lcAttach,lcBody,lcBCC
lcServer = "TGFNJSQL01"
gnconnhandle= Sqlstringconnect("Driver=SQL Server;Server=" + lcServer + ";UID=sa;PWD=B%g23!7#$;Database=Carteret")
okproperties1= SQLSetprop(gnconnhandle, 'Displogin',3)
okproperties2= SQLSetprop(gnconnhandle, 'asynchronous', .F.)
lcSql = ""
lcSql = lcSql + "exec [msdb].[dbo].[sp_send_dbmail] "
lcSql = lcSql + "@profile_name =  'Fanz',"
lcSql = lcSql + "@reply_to = 'no-reply@tollgroup.com',"
lcSql = lcSql + "@body_format =  'TEXT',"
lcSql = lcSql + "@recipients = '" + Strtran(lcTo,",",";") + "',"
lcSql = lcSql + Iif(!Empty(lcFrom)   ,"@from_address = '" + Strtran(lcFrom,",",";") + "',","")
lcSql = lcSql + Iif(!Empty(lcsubject),"@subject = '" + lcsubject + "',","")
lcSql = lcSql + Iif(!Empty(lcBody)   ,"@body = '" + lcBody + "',","")
lcSql = Iif(Right(Alltrim(lcSql),1)=","  ,Substr(Alltrim(lcSql),1,Len(Alltrim(lcSql))-1), Substr(Alltrim(lcSql),1,Len(Alltrim(lcSql))))
lcSql = lcSql + ";"
*20171213 Fanz - Added Criteria for DONOT SEND eMail
gConnected = SQLExec(gnconnhandle, lcSql,"vData")
gConnected = SQLDisconnect(gnconnhandle)
Quit

******************************************************************************************************************************************************************************************
Procedure SqlAdd
Parameters DoWhat,lcFanzKey2
lcSql2 = "select * from carteret.dbo.edi_trigger_log where fanzKey='" + Alltrim(lcFanzKey2) + "'"
gnconnhandle= Sqlstringconnect("Driver=SQL Server;Server=" + Alltrim(Thisform.txtserver.Value) + ";UID=sa;PWD=B%g23!7#$;Database=Carteret")
okprop1= SQLSetprop(gnconnhandle, 'Displogin',3)
okprop2= SQLSetprop(gnconnhandle, 'asynchronous', .F.)
gConnected = SQLExec(gnconnhandle, lcSql,"vFound")
If gConnected>0
	If  Reccount("vFound")>0
		Do Case
		Case DoWhat = 'Insert'
			gConnected = SQLExec(gnconnhandle, lcSql,"vData")
			If gConnected<=0
				=Aerror(Crap)
				*trigger onerror
			Endif
			gConnected = SQLDisconnect(gnconnhandle)
		Case DoWhat = 'Edit'
			*make some updated to fields
		Endcase
	Else
		* No SQL Data returned
	Endif
Else
	* Bad SQL Exec - retuned -1
Endif
Return


******************************************************************************************************************************************************************************************
Procedure InsertData
*OPEN DATABASE F:\3PL\Data\FanzCheck.DBC EXCLUSIVE
Use FanzCheck!Insert Nodata In 0 Alias InsertData
Select InsertData
Append Blank
lProp = CursorSetProp("Buffering",3,"InsertData")
Return
******************************************************************************************************************************************************************************************
Procedure CleanupData
If Alias() = "INSERTDATA"
	*	lProp = CursorSetProp("buffering",3,"InsertData")
	If time212 = {}
		*Replace time212 With .Null.
	Endif
	If errtime = {}
		*Replace errtime With .Null.
	Endif
	If confirm_dt = {}
		*Replace confirm_dt With .Null.
	Endif
	If when_Proc = {}
		*Replace when_Proc With .Null.
	Endif
	*	lUpdated=Tableupdate(.T.)
Else
	If time212 = {}
		Replace time212 With .Null.
	Endif
	If errtime = {}
		Replace errtime With .Null.
	Endif
	If confirm_dt = {}
		Replace confirm_dt With .Null.
	Endif
	If when_Proc = {}
		Replace when_Proc With .Null.
	Endif
	lUpdated=Tableupdate(.T.)
Endif

If Used("InsertData")
	Use In InsertData
Endif
Return

******************************************************************************************************************************************************************************************
*!*	INSERT INTO [dbo].[edi_trigger_log]
*!*	           ([urow]
*!*	           ,[datetime]
*!*	           ,[FanzKey]
*!*	           ,[Timestamp]
*!*	           ,[Delayed]
*!*	           ,[Error_At]
*!*	           ,[Error_Msg]
*!*	           ,[edi_type]
*!*	           ,[processed]
*!*	           ,[errorflag]
*!*	           ,[office]
*!*	           ,[accountid]
*!*	           ,[trig_time]
*!*	           ,[when_proc]
*!*	           ,[bol]
*!*	           ,[masterbill]
*!*	           ,[consignee]
*!*	           ,[wo_num]
*!*	           ,[ship_ref]
*!*	           ,[cnee_ref]
*!*	           ,[scac]
*!*	           ,[fin_status]
*!*	           ,[outshipid]
*!*	           ,[parcel]
*!*	           ,[leg]
*!*	           ,[tripid]
*!*	           ,[date]
*!*	           ,[time]
*!*	           ,[dsdc]
*!*	           ,[arrival]
*!*	           ,[style]
*!*	           ,[dellocid]
*!*	           ,[isa_num]
*!*	           ,[proc945]
*!*	           ,[ack945]
*!*	           ,[proc214]
*!*	           ,[proc856]
*!*	           ,[created]
*!*	           ,[bybol]
*!*	           ,[ponum]
*!*	           ,[ptdate]
*!*	           ,[ctrprfx]
*!*	           ,[container]
*!*	           ,[awb]
*!*	           ,[trig_from]
*!*	           ,[trig_by]
*!*	           ,[file944crt]
*!*	           ,[file945]
*!*	           ,[file214]
*!*	           ,[file856]
*!*	           ,[whse_from]
*!*	           ,[whse_to]
*!*	           ,[bcodes]
*!*	           ,[cuts]
*!*	           ,[send212]
*!*	           ,[file212]
*!*	           ,[time212]
*!*	           ,[comments]
*!*	           ,[ptflag]
*!*	           ,[errtime]
*!*	           ,[holdflag]
*!*	           ,[confirm_dt]
*!*	           ,[adny]
*!*	           ,[moret]
*!*	           ,[testflag])
*!*	     VALUES
*!*	           (<urow, uniqueidentifier,>
*!*	           ,<datetime, datetime2(7),>
*!*	           ,<FanzKey, nvarchar(75),>
*!*	           ,<Timestamp, nvarchar(15),>
*!*	           ,<Delayed, numeric(20,0),>
*!*	           ,<Error_At, datetime,>
*!*	           ,<Error_Msg, nvarchar(100),>
*!*	           ,<edi_type, nvarchar(4),>
*!*	           ,<processed, bit,>
*!*	           ,<errorflag, bit,>
*!*	           ,<office, nvarchar(1),>
*!*	           ,<accountid, int,>
*!*	           ,<trig_time, datetime,>
*!*	           ,<when_proc, datetime,>
*!*	           ,<bol, nvarchar(20),>
*!*	           ,<masterbill, bit,>
*!*	           ,<consignee, nvarchar(30),>
*!*	           ,<wo_num, numeric(8,0),>
*!*	           ,<ship_ref, nvarchar(20),>
*!*	           ,<cnee_ref, nvarchar(20),>
*!*	           ,<scac, nvarchar(4),>
*!*	           ,<fin_status, nvarchar(45),>
*!*	           ,<outshipid, int,>
*!*	           ,<parcel, bit,>
*!*	           ,<leg, numeric(2,0),>
*!*	           ,<tripid, int,>
*!*	           ,<date, datetime,>
*!*	           ,<time, nvarchar(10),>
*!*	           ,<dsdc, bit,>
*!*	           ,<arrival, nvarchar(10),>
*!*	           ,<style, nvarchar(10),>
*!*	           ,<dellocid, nvarchar(20),>
*!*	           ,<isa_num, nvarchar(9),>
*!*	           ,<proc945, bit,>
*!*	           ,<ack945, bit,>
*!*	           ,<proc214, bit,>
*!*	           ,<proc856, bit,>
*!*	           ,<created, bit,>
*!*	           ,<bybol, bit,>
*!*	           ,<ponum, nvarchar(10),>
*!*	           ,<ptdate, datetime,>
*!*	           ,<ctrprfx, nvarchar(4),>
*!*	           ,<container, nvarchar(7),>
*!*	           ,<awb, nvarchar(7),>
*!*	           ,<trig_from, nvarchar(30),>
*!*	           ,<trig_by, nvarchar(10),>
*!*	           ,<file944crt, nvarchar(70),>
*!*	           ,<file945, nvarchar(100),>
*!*	           ,<file214, nvarchar(70),>
*!*	           ,<file856, nvarchar(70),>
*!*	           ,<whse_from, nvarchar(5),>
*!*	           ,<whse_to, nvarchar(5),>
*!*	           ,<bcodes, nvarchar(max),>
*!*	           ,<cuts, nvarchar(max),>
*!*	           ,<send212, bit,>
*!*	           ,<file212, nvarchar(70),>
*!*	           ,<time212, datetime,>
*!*	           ,<comments, nvarchar(max),>
*!*	           ,<ptflag, bit,>
*!*	           ,<errtime, datetime,>
*!*	           ,<holdflag, bit,>
*!*	           ,<confirm_dt, datetime,>
*!*	           ,<adny, bit,>
*!*	           ,<moret, bit,>
*!*	           ,<testflag, bit,>)
*!*	GO

******************************************************************************************************************************************************************************************
Procedure One
* update record
*capture existing when_proc,Fin_Stat
Set Step On
If Fin_status =  "DELETED-"
	*Set Step On
	ldError_At  = Iif(Empty(Logs.when_Proc),Datetime(),Logs.when_Proc)
	lcError_Msg = "DELETED-" + Alltrim(STRTRAN(Logs.Fin_status,"DELETED-",""))
	Gather From arrTriggers
	Replace ;
		errorFlag With .F.,;
		Fin_status With "DELETED-",;
		error_at With ldError_At,;
		error_msg With lcError_Msg,;
		when_Proc With Datetime()
	Replace Delayed With Iif(!Empty(when_Proc),trig_time-when_Proc,trig_time-Datetime())
	Gather From arrTriggers
	Do CleanupData
	Use FanzCheck!Insert In 0 Alias InsertData
	Select InsertData
	Append Blank
	Gather From arrTriggers
	lProp = CursorSetProp("Buffering",3,"InsertData")
	lUpdated = Tableupdate()
	Do CleanupData
Else
	ldError_At  = Iif(Empty(Logs.when_Proc),Datetime(),Logs.when_Proc)
	lcError_Msg = "DELETED-" + Alltrim(Logs.Fin_status)
	Gather From arrTriggers
	Replace ;
		errorFlag With .F.,;
		Fin_status With "DELETED-",;
		error_at With ldError_At,;
		error_msg With lcError_Msg,;
		when_Proc With Datetime()
	Replace Delayed With Iif(!Empty(when_Proc),trig_time-when_Proc,trig_time-Datetime())
	*Do SqlAdd With 'Edit'
	Gather From arrTriggers
	Do CleanupData
	*Do SqlAdd With 'Edit'
	Use FanzCheck!Insert In 0 Alias InsertData
	Select InsertData
	Append Blank
	Gather From arrTriggers
	lProp = CursorSetProp("Buffering",3,"InsertData")
	lUpdated = Tableupdate()
	Do CleanupData
Endif
Return

******************************************************************************************************************************************************************************************
Procedure Two
*Case errorFlag = .T. And llErrorFlag = .F. And llProcessDeleted = .F.
*Set Step On
* update record
*capture existing when_proc,Fin_Stat
If Fin_status =  "DELETED-"
	*Set Step On
Else
	If llProcessDeleted = .T.
		ldError_At  = Iif(Empty(Logs.when_Proc),Datetime(),Logs.when_Proc)
		lcError_Msg = "DELETED-" + Alltrim(Logs.Fin_status)
		Gather From arrTriggers
		Replace ;
			errorFlag With .F.,;
			Fin_status With lcError_Msg,;
			error_at With ldError_At,;
			error_msg With lcError_Msg,;
			when_Proc With Datetime()
		Replace Delayed With Iif(!Empty(when_Proc),trig_time-when_Proc,trig_time-Datetime())
	Else
		ldError_At  = Logs.when_Proc
		lcError_Msg = Alltrim(Logs.Fin_status)
		Gather From arrTriggers
		Replace ;
			error_at With ldError_At,;
			error_msg With lcError_Msg
	Endif

	*Do SqlAdd With 'Edit'
	Set Deleted On
	Use FanzCheck!Insert In 0 Alias InsertData
	Select InsertData
	If Reccount("InsertData")=0
		Append Blank
		lProp = CursorSetProp("Buffering",3,"InsertData")
	Endif
	If Reccount("InsertData")>1
		Delete For FanzKey=lcFanzKey
		=Tableupdate()
		Use In InsertData
		Use FanzCheck!Insert In 0 Alias InsertData
		Select InsertData
		Append Blank
		lProp = CursorSetProp("Buffering",3,"InsertData")
	Endif
	Gather From arrTriggers
	If llProcessDeleted = .T.
		ldError_At  = Iif(Empty(Logs.when_Proc),Datetime(),Logs.when_Proc)
		lcError_Msg = "DELETED-" + Alltrim(Logs.Fin_status)
		Gather From arrTriggers
		Replace ;
			errorFlag With .F.,;
			Fin_status With lcError_Msg,;
			error_at With ldError_At,;
			error_msg With lcError_Msg,;
			when_Proc With Datetime()
		Replace Delayed With Iif(!Empty(when_Proc),trig_time-when_Proc,trig_time-Datetime())
	Else
		ldError_At  = Logs.when_Proc
		lcError_Msg = Alltrim(Logs.Fin_status)
		Gather From arrTriggers
		Replace ;
			error_at With ldError_At,;
			error_msg With lcError_Msg
	Endif
	Do CleanupData
	If llProcessDeleted = .T.
		Set Deleted Off
	Endif
Endif
Return
Procedure three
Return
Procedure Four
Return

******************************************************************************************************************************************************************************************

Procedure CleanDates
*SET STEP ON 
lcSql2      = "update carteret.dbo.edi_trigger_log set ptdate =NULL where ptdate='1900-01-01 00:00:00.000'"
gchandle2   = Sqlstringconnect("Driver=SQL Server;Server=TGFNJSQL01;UID=sa;PWD=B%g23!7#$;Database=Carteret")
okprop2     = SQLSetprop(gchandle2, 'asynchronous', .F.)
gConnected2 = SQLExec(gchandle2, lcSql2,"vFound")
=SQLDisconnect(gchandle2)
******
lcSql2      = "update carteret.dbo.edi_trigger_log set time212 = NULL where time212='1900-01-01 00:00:00.000'"
gchandle2   = Sqlstringconnect("Driver=SQL Server;Server=TGFNJSQL01;UID=sa;PWD=B%g23!7#$;Database=Carteret")
okprop2     = SQLSetprop(gchandle2, 'asynchronous', .F.)
gConnected2 = SQLExec(gchandle2, lcSql2,"vFound")
=SQLDisconnect(gchandle2)
******
lcSql2      = "update carteret.dbo.edi_trigger_log set errtime = NULL where errtime='1900-01-01 00:00:00.000'"
gchandle2   = Sqlstringconnect("Driver=SQL Server;Server=TGFNJSQL01;UID=sa;PWD=B%g23!7#$;Database=Carteret")
okprop2     = SQLSetprop(gchandle2, 'asynchronous', .F.)
gConnected2 = SQLExec(gchandle2, lcSql2,"vFound")
=SQLDisconnect(gchandle2)
******
lcSql2      = "update carteret.dbo.edi_trigger_log set confirm_dt = NULL where confirm_dt='1900-01-01 00:00:00.000'"
gchandle2   = Sqlstringconnect("Driver=SQL Server;Server=TGFNJSQL01;UID=sa;PWD=B%g23!7#$;Database=Carteret")
okprop2     = SQLSetprop(gchandle2, 'asynchronous', .F.)
gConnected2 = SQLExec(gchandle2, lcSql2,"vFound")
=SQLDisconnect(gchandle2)
******
lcSql2      = "update carteret.dbo.edi_trigger_log set when_proc = NULL where when_proc='1900-01-01 00:00:00.000'"
gchandle2   = Sqlstringconnect("Driver=SQL Server;Server=TGFNJSQL01;UID=sa;PWD=B%g23!7#$;Database=Carteret")
okprop2     = SQLSetprop(gchandle2, 'asynchronous', .F.)
gConnected2 = SQLExec(gchandle2, lcSql2,"vFound")
=SQLDisconnect(gchandle2)
Return

