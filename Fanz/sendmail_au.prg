*******************************************************************************
*Program    : sendmail_au
*Author      : Richard (Fanz) Fanslow
*        : Toll Group
*Project    :
*Created    : 20171211
*                            :
*Copyright    : � 2017, Toll
*Description  : replace dart mail with  SQL dbMail
*
*20171211 Fanz - Initial Rollout of  new Email Form
*20171213 Fanz - Added DoNotSend Parameter for testing to capture but DO NOT send email
*20171215 Fanz - Created Method  so buttons could work  rather  then  from Init
*20171215 PG   - collapsed top a simple prg
*
********************************************************************************
*20171211 Fanz - Creating execution of dbMail
Parameter lcTO,lcFrom,lcSubject,lcCC,lcAttach,lcBody,lcBCC

Local llAutoSend,lcServer

lcServer = "TGFNJSQL01"

gnconnhandle= Sqlstringconnect("Driver=SQL Server;Server=" + lcServer + ";UID=sa;PWD=B%g23!7#$;Database=Carteret")
okproperties1= SQLSetprop(gnconnhandle, 'Displogin',3)
okproperties2= SQLSetprop(gnconnhandle, 'asynchronous', .F.)
lcSql = ""
lcSql = lcSql + "exec [msdb].[dbo].[sp_send_dbmail] "
lcSql = lcSql + "@profile_name =  'Fanz',"
lcSql = lcSql + "@reply_to = 'no-reply@tollgroup.com',"
lcSql = lcSql + "@body_format =  'TEXT',"
lcSql = lcSql + "@recipients = '" + Strtran(lcTO,",",";") + "',"
lcSql = lcSql + Iif(!Empty(lcCC)     ,"@copy_recipients = '" + Strtran(lcCC,",",";") + "',","")
lcSql = lcSql + Iif(!Empty(lcBCC)    ,"@blind_copy_recipients = '" + Strtran(lcBBC,",",";") + "',","")
lcSql = lcSql + Iif(!Empty(lcFrom)   ,"@from_address = '" + Strtran(lcFrom,",",";") + "',","")
lcSql = lcSql + Iif(!Empty(lcSubject),"@subject = '" + lcSubject + "',","")
lcSql = lcSql + Iif(!Empty(lcBody)   ,"@body = '" + lcBody + "',","")
lcSql = lcSql + Iif(!Empty(lcAttach) ,"@file_attachments  = '" + lcAttach + "'","")
lcSql = Iif(Right(Alltrim(lcSql),1)=","  ,Substr(Alltrim(lcSql),1,Len(Alltrim(lcSql))-1), Substr(Alltrim(lcSql),1,Len(Alltrim(lcSql))))
lcSql = lcSql + ";"
*20171213 Fanz - Added Criteria for DONOT SEND eMail

gConnected = SQLExec(gnconnhandle, lcSql,"vData")
If gConnected<=0
	Aerror(arrError)
	tFrom     = "NoReply <noreply@tollgroup.com>"
	tsubjecty= "SendMail_Au Error: " +Ttoc(Datetime())
	tattachy = ""
	tmessagey = "SendMail_Au Error: " +Ttoc(Datetime())  + Chr(13)+;
	"   Array Value 01 : " + Iif(Isnull(arrError[1,1)),"",Alltrim(Upper(arrError[1,1]))) + Chr(13) + ;
	"   Array Value 02 : " + Iif(Isnull(arrError[1,2]),"",Alltrim(Upper(arrError[1,2]))) + Chr(13) + ;
	"   Array Value 03 : " + Iif(Isnull(arrError[1,3]),"",Alltrim(Upper(arrError[1,3]))) + Chr(13) + ;
	"   Array Value 04 : " + Iif(Isnull(arrError[1,4]),"",Alltrim(Upper(arrError[1,4]))) + Chr(13) + ;
	"   Array Value 05 : " + Iif(Isnull(arrError[1,5]),"",Alltrim(Upper(arrError[1,5]))) + Chr(13) + ;
	"   Array Value 06 : " + Iif(Isnull(arrError[1,6]),"",Alltrim(Upper(arrError[1,6]))) + Chr(13) + ;
	"   Array Value 07 : " + Iif(Isnull(arrError[1,7]),"",Alltrim(Upper(arrError[1,7]))) + Chr(13)
	lDontSendy = .F.
	lcModey = ""
	tsendtoy = "richard.fanslow@tollgroup.com,paul.gaidis@tollgroup.com"
	tccy = ""
	tbbcy = " "
	Set Step On
	Do m:\dev\prg\sendmail_au With tsendtoy,tFrom,tsubjecty,tccy,tattachy,tmessagey,""
Endif
gConnected = SQLDisconnect(gnconnhandle)

*20171211 Fanz - Need to added a SQL insert to capture data
Release gnconnhandle, okproperties1, okproperties2,lcSql,gConnected
gnconnhandle= Sqlstringconnect("Driver=SQL Server;Server=" + lcServer + ";UID=sa;PWD=B%g23!7#$;Database=Carteret")
okproperties1= SQLSetprop(gnconnhandle, 'Displogin',3)
okproperties2= SQLSetprop(gnconnhandle, 'asynchronous', .F.)
lcSql = ""
lcSql = lcSql + "Insert into [Carteret].[dbo].[MailSent] ([Servername],[to],[from],[cc],[bcc],[Subject],[Body],[Sent],[Attachments]) values ("
lcSql = lcSql + "'" + Alltrim(lcServer)  + "',"
lcSql = lcSql + "'" + Alltrim(Strtran(lcTO,",",";"))      + "',"
lcSql = lcSql + "'" + Alltrim(Strtran(lcFrom,",",";"))    + "',"
lcSql = lcSql + "'" + Alltrim(Strtran(lcCC,",",";"))      + "',"
lcSql = lcSql + "'" + Alltrim(Strtran(lcBCC,",",";"))     + "',"
lcSql = lcSql + "'" + Alltrim(Strtran(lcSubject,",","")) + "',"
lcSql = lcSql + "'" + Alltrim(Strtran(lcBody,",","")) + "',"
lcSql = lcSql + "'" + "1" + "',"
lcSql = lcSql + "'" + Alltrim(lcAttach)  + "')"
gConnected = SQLExec(gnconnhandle, lcSql,"vData")
If gConnected<=0
	Aerror(arrError)
	tFrom     = "NoReply <noreply@tollgroup.com>"
	tsubjecty= "SendMail_Au Error: " +Ttoc(Datetime())
	tattachy = ""
	tmessagey = "SendMail_Au Error: " +Ttoc(Datetime())  + Chr(13)+;
	"   Array Value 01 : " + Iif(Isnull(arrError[1,1)),"",Alltrim(Upper(arrError[1,1]))) + Chr(13) + ;
	"   Array Value 02 : " + Iif(Isnull(arrError[1,2]),"",Alltrim(Upper(arrError[1,2]))) + Chr(13) + ;
	"   Array Value 03 : " + Iif(Isnull(arrError[1,3]),"",Alltrim(Upper(arrError[1,3]))) + Chr(13) + ;
	"   Array Value 04 : " + Iif(Isnull(arrError[1,4]),"",Alltrim(Upper(arrError[1,4]))) + Chr(13) + ;
	"   Array Value 05 : " + Iif(Isnull(arrError[1,5]),"",Alltrim(Upper(arrError[1,5]))) + Chr(13) + ;
	"   Array Value 06 : " + Iif(Isnull(arrError[1,6]),"",Alltrim(Upper(arrError[1,6]))) + Chr(13) + ;
	"   Array Value 07 : " + Iif(Isnull(arrError[1,7]),"",Alltrim(Upper(arrError[1,7]))) + Chr(13)
	lDontSendy = .F.
	lcModey = ""
	tsendtoy = "richard.fanslow@tollgroup.com"
	tccy = ""
	tbbcy = ""
	Set Step On
	Do m:\dev\prg\sendmail_au With tsendtoy,tFrom,tsubjecty,tccy,tattachy,tmessagey
Endif
gConnected = SQLDisconnect(gnconnhandle)
Release gnconnhandle, okproperties1, okproperties2,lSql,gConnected
*************************************************************************************************************
