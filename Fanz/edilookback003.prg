Close All
Clear All
*Set Step On
Set Safety Off
Set Hours To 24
Set Date Japan
Set Century On
Set TablePrompt Off
On Error Do ErrRoutine
Set Deleted On
Set Multilocks On
Clear

Public lluseError,llProcessDeleted,lcFanzKey,lcFanzKey2,lcStart
lcStart = "start time : " + Ttoc(Datetime())
?lcStart
lluseError = .F.
If Inlist(Minute(Datetime()),00,01,02,03,04,05)
	llProcessDeleted = .T.
Else
	llProcessDeleted = .F.
Endif
*llProcessDeleted = .T.
*_Screen.WindowState= 1
*


Open Database F:\3PL\Data\FanzCheck.Dbc Exclusive

If !lluseError
	If File("f:\3pl\data\edi_trigger.DBF") && Check File/Network exists
		lcDriveLetter = "F:\"
		lcPathinfo    = "3PL\DATA\"
		lcDbfName     = "Edi_Trigger"
		lcTagName     = ""
		lcShared      = "SHARED"
		lcAlias       = "Edi_trigger"
		Use F:\3PL\Data\edi_trigger.Dbf	In 0 Share Alias Triggers
		*Do Useit With lcDriveLetter,lcPathinfo,lcDbfName,lcTagName,lcShared,lcAlias
		lluseError = .F.
	Endif
Endif
If !lluseError
	If File("f:\3pl\data\edi_trigger_Log.DBF") && Check File/Network exists
		lcDriveLetter = "F:\"
		lcPathinfo    = "3PL\DATA\"
		lcDbfName     = "Edi_Trigger_log"
		lcTagName     = ""
		lcShared      = "SHARED"
		lcAlias       = "Edi_trigger"
		Use F:\3PL\Data\edi_trigger_log.Dbf	Order FanzKey In 0 Exclusive Alias Logs
		*Do Useit With lcDriveLetter,lcPathinfo,lcDbfName,lcTagName,lcShared,lcAlias
		lluseError = .F.
		*SELECT Logs
		*ZAP
	Endif
Endif
=CursorSetProp("Buffering",3,"Logs")
=CursorSetProp("Buffering",3,"Triggers")
If !lluseError
	*Select edi_trigger_log
	*Delete For Empty(when_proc) And trig_time>=Datetime()-(3600*96) And trig_time <=Ctot(Dtoc(Date()+1) + ' 00:00:00 AM')

	* Create 96 hour window
	* building a unique string to matching
	********
	If llProcessDeleted = .T.
		Set Deleted Off
		Select ;
			Alltrim(edi_type) + '-' + Padl(accountid,4,"0") + '-' + Iif(Empty(office),'-',Alltrim(office)) + '-' +  Alltrim(Str(wo_num)) + '-' + Alltrim(bol) + '-' + Alltrim(Ship_ref) + '-' + Strtran(Strtran(Strtran(Ttoc(trig_time),"/",""),":","")," ","") As 'FanzKey';
			,Strtran(Strtran(Strtran(Ttoc(trig_time),"/",""),":","")," ","") As 'TimeStamp';
			,Iif(!Empty(when_Proc),trig_time-when_Proc,trig_time-Datetime())  As 'Delayed';
			,* ;
			FROM Triggers ;
			WHERE trig_time>=Datetime()-((3600*24)*8);
			AND trig_time <=Ctot(Dtoc(Date()+1) + ' 00:00:00 AM');
			AND edi_type='945';
			AND Deleted();
			ORDER By trig_time Desc;
			INTO Cursor Tmp_Triggers
	Else
		Select ;
			Alltrim(edi_type) + '-' + Padl(accountid,4,"0") + '-' + Iif(Empty(office),'-',Alltrim(office)) + '-' +  Alltrim(Str(wo_num)) + '-' + Alltrim(bol) + '-' + Alltrim(Ship_ref) + '-' + Strtran(Strtran(Strtran(Ttoc(trig_time),"/",""),":","")," ","") As 'FanzKey';
			,Strtran(Strtran(Strtran(Ttoc(trig_time),"/",""),":","")," ","") As 'TimeStamp';
			,Iif(!Empty(when_Proc),trig_time-when_Proc,trig_time-Datetime())  As 'Delayed';
			,* ;
			FROM Triggers ;
			WHERE trig_time>=Datetime()-((3600*24)*8);
			AND trig_time <=Ctot(Dtoc(Date()+1) + ' 00:00:00 AM');
			AND edi_type='945';
			ORDER By trig_time Desc;
			INTO Cursor Tmp_Triggers
	Endif
	********
	If Used("Triggers")
		Use In  Triggers
	Endif
	********
	Select Tmp_Triggers
	********
	Do While !Eof()
		Wait Window "Now Processing : "  + Padl(Recno(),10,"0") + " of " + Padl(Reccount(),10,"0") Nowait
		lcFanzKey   = Alltrim(Tmp_Triggers.FanzKey)
		lcFanzKey2  = Alltrim(Tmp_Triggers.FanzKey)
		lcTimeStamp = Tmp_Triggers.Timestamp
		llprocessed = Tmp_Triggers.processed
		llErrorFlag = Tmp_Triggers.errorFlag
		lcFinStatus = Tmp_Triggers.Fin_status
		Scatter To arrTriggers
		Select Logs
		Seek lcFanzKey
		********
		If !Found()
			*insert data into Logs
			lProp = CursorSetProp("Buffering",3,"Logs")
			Append Blank
			Gather From arrTriggers
			Use FanzCheck!Insert Nodata In 0 Alias InsertData
			Select InsertData
			lProp = CursorSetProp("Buffering",3,"InsertData")
			Append Blank
			Gather From arrTriggers
			lUpdated=Tableupdate()
			If llProcessDeleted = .T.
				ldError_At  = Iif(Empty(Logs.when_Proc),Datetime(),Logs.when_Proc)
				lcError_Msg = "DELETED-" + Strtran(Alltrim(Logs.Fin_status),"DELETED-","")
				Replace errorFlag With .F.,Fin_status With lcError_Msg,error_at With ldError_At,error_msg With lcError_Msg,when_Proc With Datetime(),Delayed With Iif(!Empty(when_Proc),trig_time-when_Proc,trig_time-Datetime())
				lUpdated=Tableupdate(.t.,.t.)
			Endif
			Do CleanupData
		Else
			If llProcessDeleted = .T.
				Do One
			Else
				If llprocessed And !llErrorFlag && Process = .T. and ErrorFlag=.F.
					If !Empty(error_msg)
						Set Step On
						ldError_At  = tmp_trigger.errtime
						lcError_Msg = Alltrim(Logs.error_msg)
						Gather From arrTriggers
						Replace Delayed With Iif(!Empty(when_Proc),trig_time-when_Proc,trig_time-Datetime()),error_at With ldError_At,error_msg With lcError_Msg
						lUpdated=Tableupdate(.t.,.t.)
						Do CleanupData
					Endif
				Endif
				If Alltrim(Fin_status) # Alltrim(lcFinStatus)
					If !Empty(error_msg)
						Set Step On
						ldError_At  = tmp_trigger.errtime
						lcError_Msg = Alltrim(Logs.error_msg)
						Gather From arrTriggers
						Replace Delayed With Iif(!Empty(when_Proc),trig_time-when_Proc,trig_time-Datetime()),error_at With ldError_At,error_msg With lcError_Msg
						lUpdated=Tableupdate(.t.,.t.)
						Do CleanupData
					Endif
				Endif

			Endif
		Endif
		********
		Release arrTriggers,lcFanzKey,lcTimeStamp,llErrorFlag,ldError_At,lcError_Msg
		********
		Select Tmp_Triggers
		Skip
		Loop
	Enddo
Endif
Do CleanDates
?"End Time   : " + Ttoc(Datetime())
*DO Sucess WITH lcStart
Close All
Clear All
DO FullPass
_Screen.WindowState = 0
Quit

******************************************************************************************************************************************************************************************
Procedure Useit
* setup open data safely
lluseError = .F.
Return lluseError

******************************************************************************************************************************************************************************************
Procedure ErrRoutine
*setup some recording
Aerror(arrError)
lcMsg1 = Alltrim(Padl(Iif(Isnull(arrError[1,1]),"",arrError[1,1]),250," "))
lcMsg2 = Alltrim(Padl(Iif(Isnull(arrError[1,2]),"",arrError[1,2]),250," "))
lcMsg3 = Alltrim(Padl(Iif(Isnull(arrError[1,3]),"",arrError[1,3]),250," "))
lcMsg4 = Alltrim(Padl(Iif(Isnull(arrError[1,4]),"",arrError[1,4]),250," "))
lcMsg5 = Alltrim(Padl(Iif(Isnull(arrError[1,5]),"",arrError[1,5]),250," "))
lcMsg6 = Alltrim(Padl(Iif(Isnull(arrError[1,6]),"",arrError[1,6]),250," "))
lcMsg7 = Alltrim(Padl(Iif(Isnull(arrError[1,7]),"",arrError[1,7]),250," "))
lcBody = "EDI Lookback Error: " +Ttoc(Datetime()) + " : " + Iif(Type("lcFanzKey")="C",lcFanzKey,"") + Chr(13);
	+ " Array Value 01 : " + lcMsg1 + Chr(13) ;
	+ " Array Value 02 : " + lcMsg2 + Chr(13) ;
	+ " Array Value 03 : " + lcMsg3 + Chr(13) ;
	+ " Array Value 04 : " + lcMsg4 + Chr(13) ;
	+ " Array Value 05 : " + lcMsg5 + Chr(13) ;
	+ " Database Name  : " + Dbf() + Chr(13) ;
	+ " Database Record: " + Padl(Recno(),10," ") + Chr(13)
If At("Update conflict in cursor 'INSERTDATA'",lcMsg2)>0
	Select InsertData
	=Tablerevert()
Endif

lcFrom     = "NoReply <noreply@tollgroup.com>"
lcsubject= "EDI Lookback Error: " +Ttoc(Datetime()) + " : " + Iif(Type("lcFanzKey")="C",lcFanzKey,"")
lcattach = ""
lcTo = "richard.fanslow@tollgroup.com"
*Set Step On
*sendMail_au with lcTO,lcFrom,lcSubject,lcCC,lcAttach,lcBody,lcBCC
lcServer = "TGFNJSQL01"
gnconnhandle= Sqlstringconnect("Driver=SQL Server;Server=" + lcServer + ";UID=sa;PWD=B%g23!7#$;Database=Carteret")
okproperties1= SQLSetprop(gnconnhandle, 'Displogin',3)
okproperties2= SQLSetprop(gnconnhandle, 'asynchronous', .F.)
lcSql = ""
lcSql = lcSql + "exec [msdb].[dbo].[sp_send_dbmail] "
lcSql = lcSql + "@profile_name =  'Fanz',"
lcSql = lcSql + "@reply_to = 'no-reply@tollgroup.com',"
lcSql = lcSql + "@body_format =  'TEXT',"
lcSql = lcSql + "@recipients = '" + Strtran(lcTo,",",";") + "',"
lcSql = lcSql + Iif(!Empty(lcFrom)   ,"@from_address = '" + Strtran(lcFrom,",",";") + "',","")
lcSql = lcSql + Iif(!Empty(lcsubject),"@subject = '" + lcsubject + "',","")
lcSql = lcSql + Iif(!Empty(lcBody)   ,"@body = '" + lcBody + "',","")
lcSql = Iif(Right(Alltrim(lcSql),1)=","  ,Substr(Alltrim(lcSql),1,Len(Alltrim(lcSql))-1), Substr(Alltrim(lcSql),1,Len(Alltrim(lcSql))))
lcSql = lcSql + ";"
*20171213 Fanz - Added Criteria for DONOT SEND eMail
gConnected = SQLExec(gnconnhandle, lcSql,"vData")
gConnected = SQLDisconnect(gnconnhandle)
Quit

******************************************************************************************************************************************************************************************
Procedure SqlAdd
Parameters DoWhat,lcFanzKey2
lcSql2 = "select * from carteret.dbo.edi_trigger_log where fanzKey='" + Alltrim(lcFanzKey2) + "'"
gnconnhandle= Sqlstringconnect("Driver=SQL Server;Server=" + Alltrim(Thisform.txtserver.Value) + ";UID=sa;PWD=B%g23!7#$;Database=Carteret")
okprop1= SQLSetprop(gnconnhandle, 'Displogin',3)
okprop2= SQLSetprop(gnconnhandle, 'asynchronous', .F.)
gConnected = SQLExec(gnconnhandle, lcSql,"vFound")
If gConnected>0
	If  Reccount("vFound")>0
		Do Case
		Case DoWhat = 'Insert'
			gConnected = SQLExec(gnconnhandle, lcSql,"vData")
			If gConnected<=0
				=Aerror(Crap)
				*trigger onerror
			Endif
			gConnected = SQLDisconnect(gnconnhandle)
		Case DoWhat = 'Edit'
			*make some updated to fields
		Endcase
	Else
		* No SQL Data returned
	Endif
Else
	* Bad SQL Exec - retuned -1
Endif
Return
******************************************************************************************************************************************************************************************
Procedure InsertData
*OPEN DATABASE F:\3PL\Data\FanzCheck.DBC EXCLUSIVE
Use FanzCheck!Insert Nodata In 0 Alias InsertData
Select InsertData
Append Blank
lProp = CursorSetProp("Buffering",3,"InsertData")
Return
******************************************************************************************************************************************************************************************
Procedure CleanupData
If Alias() = "INSERTDATA"
	*	lProp = CursorSetProp("buffering",3,"InsertData")
	If time212 = {}
		*Replace time212 With .Null.
	Endif
	If errtime = {}
		*Replace errtime With .Null.
	Endif
	If confirm_dt = {}
		*Replace confirm_dt With .Null.
	Endif
	If when_Proc = {}
		*Replace when_Proc With .Null.
	Endif
	*	lUpdated=Tableupdate(.T.)
Else
	If time212 = {}
		*Replace time212 With .Null.
	Endif
	If errtime = {}
		*Replace errtime With .Null.
	Endif
	If confirm_dt = {}
		*Replace confirm_dt With .Null.
	Endif
	If when_Proc = {}
		*Replace when_Proc With .Null.
	Endif
	*lUpdated=Tableupdate(.T.)
Endif
If Used("InsertData")
	Use In InsertData
Endif
Select Logs
Return
******************************************************************************************************************************************************************************************
Procedure One
* update record
*capture existing when_proc,Fin_Stat

If Fin_status =  "DELETED-"
	*Set Step On
	ldError_At  = Iif(Empty(Logs.when_Proc),Datetime(),Logs.when_Proc)
	lcError_Msg = "DELETED-" + Alltrim(Strtran(Logs.Fin_status,"DELETED-",""))
	Gather From arrTriggers
	Replace ;
		errorFlag With .F.,;
		Fin_status With "DELETED-",;
		error_at With ldError_At,;
		error_msg With lcError_Msg,;
		when_Proc With Datetime()
	Replace Delayed With Iif(!Empty(when_Proc),trig_time-when_Proc,trig_time-Datetime())
	Gather From arrTriggers
	Do CleanupData
	If Reccount("InsertData")=0
		Append Blank
		lProp = CursorSetProp("Buffering",3,"InsertData")
		Gather From arrTriggers
		lUpdated = Tableupdate()
	Endif
	If Reccount("InsertData")>1
		Delete For FanzKey=lcFanzKey
		=Tableupdate()
		If Used("insertData")
			Use In InsertData
		Endif
		Use FanzCheck!Insert In 0 Alias InsertData
		Select InsertData
		Append Blank
		lProp = CursorSetProp("Buffering",3,"InsertData")
		Gather From arrTriggers
		lUpdated = Tableupdate()
	Endif
	Use FanzCheck!Insert In 0 Alias InsertData
	Select InsertData

	Gather From arrTriggers
	lProp = CursorSetProp("Buffering",3,"InsertData")
	lUpdated = Tableupdate()
	Do CleanupData
Else
	ldError_At  = Iif(Empty(Logs.when_Proc),Datetime(),Logs.when_Proc)
	lcError_Msg = "DELETED-" + Strtran(Alltrim(Logs.Fin_status),"DELETED-","")
	Gather From arrTriggers
	Replace ;
		errorFlag With .F.,;
		Fin_status With "DELETED-",;
		error_at With ldError_At,;
		error_msg With lcError_Msg,;
		when_Proc With Datetime()
	Replace Delayed With Iif(!Empty(when_Proc),trig_time-when_Proc,trig_time-Datetime())
	*Do SqlAdd With 'Edit'
	Gather From arrTriggers
	Do CleanupData
	If Used("insertData")
		Use In InsertData
	Endif
	Use FanzCheck!Insert In 0 Alias InsertData
	Select InsertData
	If Reccount("InsertData")=0
		Append Blank
		lProp = CursorSetProp("Buffering",3,"InsertData")
		Gather From arrTriggers
	Endif
	If Reccount("InsertData")>1
		Delete For FanzKey=lcFanzKey
		=Tableupdate()
		Use In InsertData
		Use FanzCheck!Insert In 0 Alias InsertData
		Select InsertData
		Append Blank
		lProp = CursorSetProp("Buffering",3,"InsertData")
		Gather From arrTriggers
	Endif
	If Used("insertData")
		Use In InsertData
	Endif
	Use FanzCheck!Insert In 0 Alias InsertData
	Select InsertData
	lProp = CursorSetProp("Buffering",3,"InsertData")
	Gather From arrTriggers
	Replace ;
		errorFlag With .F.,;
		Fin_status With "DELETED-",;
		error_at With ldError_At,;
		error_msg With lcError_Msg,;
		when_Proc With Datetime()
	Replace Delayed With Iif(!Empty(when_Proc),trig_time-when_Proc,trig_time-Datetime())
	lUpdated = Tableupdate()
	Do CleanupData
Endif
Return

******************************************************************************************************************************************************************************************
Procedure Two
*Case errorFlag = .T. And llErrorFlag = .F. And llProcessDeleted = .F.
*Set Step On
* update record
*capture existing when_proc,Fin_Stat
If Fin_status =  "DELETED-"
	*Set Step On
Else
	If llProcessDeleted = .T.
		ldError_At  = Iif(Empty(Logs.when_Proc),Datetime(),Logs.when_Proc)
		lcError_Msg = "DELETED-" + Alltrim(Logs.Fin_status)
		Gather From arrTriggers
		Replace ;
			errorFlag With .F.,;
			Fin_status With lcError_Msg,;
			error_at With ldError_At,;
			error_msg With lcError_Msg,;
			when_Proc With Datetime()
		Replace Delayed With Iif(!Empty(when_Proc),trig_time-when_Proc,trig_time-Datetime())
	Else
		ldError_At  = Logs.when_Proc
		lcError_Msg = Alltrim(Logs.Fin_status)
		Gather From arrTriggers
		Replace ;
			error_at With ldError_At,;
			error_msg With lcError_Msg
	Endif

	*Do SqlAdd With 'Edit'
	Set Deleted On
	Use FanzCheck!Insert In 0 Alias InsertData
	Select InsertData
	If Reccount("InsertData")=0
		Append Blank
		lProp = CursorSetProp("Buffering",3,"InsertData")
	Endif
	If Reccount("InsertData")>1
		Delete For FanzKey=lcFanzKey
		=Tableupdate()
		Use In InsertData
		Use FanzCheck!Insert In 0 Alias InsertData
		Select InsertData
		Append Blank
		lProp = CursorSetProp("Buffering",3,"InsertData")
	Endif
	Gather From arrTriggers
	If llProcessDeleted = .T.
		ldError_At  = Iif(Empty(Logs.when_Proc),Datetime(),Logs.when_Proc)
		lcError_Msg = "DELETED-" + Alltrim(Logs.Fin_status)
		Gather From arrTriggers
		Replace ;
			errorFlag With .F.,;
			Fin_status With lcError_Msg,;
			error_at With ldError_At,;
			error_msg With lcError_Msg,;
			when_Proc With Datetime()
		Replace Delayed With Iif(!Empty(when_Proc),trig_time-when_Proc,trig_time-Datetime())
	Else
		ldError_At  = Logs.when_Proc
		lcError_Msg = Alltrim(Logs.Fin_status)
		Gather From arrTriggers
		Replace ;
			error_at With ldError_At,;
			error_msg With lcError_Msg
	Endif
	Do CleanupData
	If llProcessDeleted = .T.
		Set Deleted Off
	Endif
Endif
Return
Procedure three
Return
Procedure Four
Return

******************************************************************************************************************************************************************************************

Procedure CleanDates
*SET STEP ON
lcSql2      = "update carteret.dbo.edi_trigger_log set ptdate =NULL where ptdate='1900-01-01 00:00:00.000'"
gchandle2   = Sqlstringconnect("Driver=SQL Server;Server=TGFNJSQL01;UID=sa;PWD=B%g23!7#$;Database=Carteret")
okprop2     = SQLSetprop(gchandle2, 'asynchronous', .F.)
gConnected2 = SQLExec(gchandle2, lcSql2,"vFound")
=SQLDisconnect(gchandle2)
******
lcSql2      = "update carteret.dbo.edi_trigger_log set time212 = NULL where time212='1900-01-01 00:00:00.000'"
gchandle2   = Sqlstringconnect("Driver=SQL Server;Server=TGFNJSQL01;UID=sa;PWD=B%g23!7#$;Database=Carteret")
okprop2     = SQLSetprop(gchandle2, 'asynchronous', .F.)
gConnected2 = SQLExec(gchandle2, lcSql2,"vFound")
=SQLDisconnect(gchandle2)
******
lcSql2      = "update carteret.dbo.edi_trigger_log set errtime = NULL where errtime='1900-01-01 00:00:00.000'"
gchandle2   = Sqlstringconnect("Driver=SQL Server;Server=TGFNJSQL01;UID=sa;PWD=B%g23!7#$;Database=Carteret")
okprop2     = SQLSetprop(gchandle2, 'asynchronous', .F.)
gConnected2 = SQLExec(gchandle2, lcSql2,"vFound")
=SQLDisconnect(gchandle2)
******
lcSql2      = "update carteret.dbo.edi_trigger_log set confirm_dt = NULL where confirm_dt='1900-01-01 00:00:00.000'"
gchandle2   = Sqlstringconnect("Driver=SQL Server;Server=TGFNJSQL01;UID=sa;PWD=B%g23!7#$;Database=Carteret")
okprop2     = SQLSetprop(gchandle2, 'asynchronous', .F.)
gConnected2 = SQLExec(gchandle2, lcSql2,"vFound")
=SQLDisconnect(gchandle2)
******
lcSql2      = "update carteret.dbo.edi_trigger_log set when_proc = NULL where when_proc='1900-01-01 00:00:00.000'"
gchandle2   = Sqlstringconnect("Driver=SQL Server;Server=TGFNJSQL01;UID=sa;PWD=B%g23!7#$;Database=Carteret")
okprop2     = SQLSetprop(gchandle2, 'asynchronous', .F.)
gConnected2 = SQLExec(gchandle2, lcSql2,"vFound")
=SQLDisconnect(gchandle2)
Return
******************************************************************************************************************************************************************************************
Procedure Truncate
*SET STEP ON
lcSql3      = "Truncate table carteret.dbo.edi_trigger_log"
gchandle3   = Sqlstringconnect("Driver=SQL Server;Server=TGFNJSQL01;UID=sa;PWD=B%g23!7#$;Database=Carteret")
okprop3     = SQLSetprop(gchandle3, 'asynchronous', .F.)
gConnected3 = SQLExec(gchandle3, lcSql3,"vFound")
=SQLDisconnect(gchandle3)
******************************************************************************************************************************************************************************************
Procedure SqlUpdate
Set Deleted On
Use FanzCheck!Insert In 0 Alias InsertData
Select InsertData
If Reccount("InsertData")=0
	Append Blank
	lProp = CursorSetProp("Buffering",3,"InsertData")
Endif
If Reccount("InsertData")>1
	Delete For FanzKey=lcFanzKey
	=Tableupdate()
	Use In InsertData
	Use FanzCheck!Insert In 0 Alias InsertData
	Select InsertData
	Append Blank
	lProp = CursorSetProp("Buffering",3,"InsertData")
Endif
Gather From arrTriggers
If llProcessDeleted = .T.
	ldError_At  = Iif(Empty(Logs.when_Proc),Datetime(),Logs.when_Proc)
	lcError_Msg = "DELETED-" + Alltrim(Logs.Fin_status)
	Gather From arrTriggers
	Replace ;
		errorFlag With .F.,;
		Fin_status With lcError_Msg,;
		error_at With ldError_At,;
		error_msg With lcError_Msg,;
		when_Proc With Datetime()
	Replace Delayed With Iif(!Empty(when_Proc),trig_time-when_Proc,trig_time-Datetime())
Else
	ldError_At  = Logs.when_Proc
	lcError_Msg = Alltrim(Logs.Fin_status)
	Gather From arrTriggers
	Replace ;
		error_at With ldError_At,;
		error_msg With lcError_Msg
Endif
Do CleanupData
If llProcessDeleted = .T.
	Set Deleted Off
Endif

******************************************************************************************************************************************************************************************
Procedure Sucess
PARAMETERS lStarted
lcBody = "EDI Lookback Completed" + CHR(13) + lStarted + CHR(13) + "stopped time : " + Ttoc(Datetime())
lcFrom     = "NoReply <noreply@tollgroup.com>"
lcsubject= "EDI Lookback Completed: " +Ttoc(Datetime())
lcattach = ""
lcTo = "richard.fanslow@tollgroup.com"
lcServer = "TGFNJSQL01"
gnconnhandle= Sqlstringconnect("Driver=SQL Server;Server=" + lcServer + ";UID=sa;PWD=B%g23!7#$;Database=Carteret")
okproperties1= SQLSetprop(gnconnhandle, 'Displogin',3)
okproperties2= SQLSetprop(gnconnhandle, 'asynchronous', .F.)
lcSql = ""
lcSql = lcSql + "exec [msdb].[dbo].[sp_send_dbmail] "
lcSql = lcSql + "@profile_name =  'Fanz',"
lcSql = lcSql + "@reply_to = 'no-reply@tollgroup.com',"
lcSql = lcSql + "@body_format =  'TEXT',"
lcSql = lcSql + "@recipients = '" + Strtran(lcTo,",",";") + "',"
lcSql = lcSql + Iif(!Empty(lcFrom)   ,"@from_address = '" + Strtran(lcFrom,",",";") + "',","")
lcSql = lcSql + Iif(!Empty(lcsubject),"@subject = '" + lcsubject + "',","")
lcSql = lcSql + Iif(!Empty(lcBody)   ,"@body = '" + lcBody + "',","")
lcSql = Iif(Right(Alltrim(lcSql),1)=","  ,Substr(Alltrim(lcSql),1,Len(Alltrim(lcSql))-1), Substr(Alltrim(lcSql),1,Len(Alltrim(lcSql))))
lcSql = lcSql + ";"
*20171213 Fanz - Added Criteria for DONOT SEND eMail
gConnected = SQLExec(gnconnhandle, lcSql,"vData")
gConnected = SQLDisconnect(gnconnhandle)
RETURN

PROCEDURE FullPass
lcServer = "TGFNJSQL01"
gnconnhandle= Sqlstringconnect("Driver=SQL Server;Server=" + lcServer + ";UID=sa;PWD=B%g23!7#$;Database=Carteret")
okproperties1= SQLSetprop(gnconnhandle, 'Displogin',3)
okproperties2= SQLSetprop(gnconnhandle, 'asynchronous', .F.)
lcSql = ""
lcSql = lcSql + "update carteret.dbo.taskAudit set location='" + ALLTRIM(SYS(0)) + "',AppName='EdiLookback003',lastaction=getdate() where AppName='EdiLookback003'"
lcSql = lcSql + ";"
gConnected = SQLExec(gnconnhandle, lcSql,"vData")
gConnected = SQLDisconnect(gnconnhandle)
