Close All
Release All
Set Step On

*Declare some values
nSendPort                = 2525
cSmtpServer              = "mail.smtp2go.com"
cloginname               = "richard@fanslow.com"   
cpassword                = "Florida!1"

*This section provides the configuration information for the remote SMTP server.
cdoSendUsingPickup       = 1
cdoSendUsingPort         = 2 && Must use this to use Delivery Notification
cdoSendUsingExchange     = 3 && Must use this to use Delivery Notification
cdoAnonymous             = 0
cdoBasic                 = 1 && clear text
cdoNTLM                  = 2 && NTLM
* If your server requires outgoing authentication uncomment the lines bleow and use a valid email address and password.
* Delivery Status Notifications
cdoDSNDefault            = 0 && None
cdoDSNNever              = 1 && None
cdoDSNFailure            = 2 && Failure
cdoDSNSuccess            = 4 && Success
cdoDSNDelay              = 8 && Delay
cdoDSNSuccessFailOrDelay = 14 && Success, failure or delay


oMsg    = Createobject("CDO.Message")
oConf   = Createobject("CDO.Configuration")
oFields = oConf.Fields
With oFields
	.Item("http://schemas.microsoft.com/cdo/configuration/sendusing") = cdoSendUsingPort
	.Item("http://schemas.microsoft.com/cdo/configuration/smtpserver") = cSmtpServer
	*.Item("http://schemas.microsoft.com/cdo/configuration/smtpauthenticate") = cdoAnonymous &&0
	.Item("http://schemas.microsoft.com/cdo/configuration/smtpauthenticate") = cdoBasic && 1
	*.Item("http://schemas.microsoft.com/cdo/configuration/smtpauthenticate") = cdoNTLM && 2
	.Item("http://schemas.microsoft.com/cdo/configuration/smtpusessl") = .F.  &&Use SSL for the connection (True or False)
	.Item("http://schemas.microsoft.com/cdo/configuration/smtpconnectiontimeout") = 60
	.Item("http://schemas.microsoft.com/cdo/configuration/sendusername") = cLoginName
	.Item("http://schemas.microsoft.com/cdo/configuration/sendpassword") = cPassword
	.Item ("http://schemas.microsoft.com/cdo/configuration/smtpserverport") = nSendPort && 25
	.Update
Endwith

oMsg.Subject="CDOSYS Email"
oMsg.From="richard@fanslow.com"
oMsg.To="richard@fanslow.com"
oMsg.TextBody="This message is using CDOSYS."
oMsg.Send
Release oFields,oConf,oMsg



