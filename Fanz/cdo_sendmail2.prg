Close All
Release All
Set Step On

*'==========================================================================
*'
*' COMMENT: Generic subroutine that sends mail to an external SMTP server.
*' Message can be HTML or Plain Text.
*'
*' SOURCES:
*' http://msdn.microsoft.com/library/default.asp?url=/library/en-us/e2k3/e2k3/_cdo_configuration_coclass.asp
*' http://support.microsoft.com/default.aspx?scid=kb;en-us;Q286431
*'==========================================================================

Public cServer, cSender, cRecipient, cSubject, cMessage

cServer = "mail.smtp2go.com"
cSender = "richard@fanslow.com"
cRecipient = "richard@fanslow.com"
cSubject = "Test"
* cMessage = "This is a test."
cMessage = "<HTML><BODY><b>This is a test.</b></br></BODY></HTML>"

Do SendEmail With cServer,cSender,cRecipient,cSubject,cMessage

Procedure SendEmail
Parameters Server, sndr, rcpt, subj, msg
Local iMsg, iConf, Flds
* Set the visual basic constants as they do not exist within VBScript.
* Do not set your smtp server information here.
cdoSendUsingMethod = "http://schemas.microsoft.com/cdo/configuration/sendusing"
cdoSendUsingPort = 2
cdoSMTPServer = "http://schemas.microsoft.com/cdo/configuration/smtpserver"

* The following field names are not needed, but can be enabled
cdoSMTPServerPort = "http://schemas.microsoft.com/cdo/configuration/smtpserverport"
cdoSendEmailAddress = "http://schemas.microsoft.com/cdo/configuration/sendemailaddress"
*     Const cdoSendUserReplyEmailAddress = "http://schemas.microsoft.com/cdo/configuration/senduserreplyemailaddress"
cdoSMTPAuthenticate = "http://schemas.microsoft.com/cdo/configuration/smtpauthenticate"
cdoBasic = 1
cdoSendUserName = "http://schemas.microsoft.com/cdo/configuration/sendusername"
cdoSendPassword = "http://schemas.microsoft.com/cdo/configuration/sendpassword"
cdoSMTPConnectionTimeout = "http://schemas.microsoft.com/cdo/configuration/smtpconnectiontimeout"

* Create the CDO connections.
oMsg = Createobject("CDO.Message")
oConf = Createobject("CDO.Configuration")
oFields = oConf.Fields

*' SMTP server configuration.
With oFields
	.Item(cdoSendUsingMethod) = cdoSendUsingPort
*' Set the SMTP server address here.
	.Item(cdoSMTPServer) = Server
	.Item(cdoSMTPServerPort) = 2525
	.Item(cdoSendEmailAddress) = "'Richard Fanslow' <ricard@fanslow.com>"
*'         .Item(cdoSendUserReplyEmailAddress) = """Another"" <another@example.com>"
	.Item(cdoSMTPAuthenticate) = cdoBasic
	.Item(cdoSendUserName) = "richard@fanslow.com"
	.Item(cdoSendPassword) = "Florida11"
	.Item(cdoSMTPConnectionTimeout) = 10
	.Update
Endwith

*' Set the message properties.
With oMsg
	.Configuration = oConf
	.To = rcpt
	If(!Empty(rcpt))
		.CC = rcpt
	Endif
	.From = sndr
	.Subject = subj
Endwith

If At(Upper(msg),"<HTML>")>0
	oMsg.HTMLBody = msg
Else
	oMsg.TextBody = msg
Endif

*' An attachment can be included.
*'oMsg.AddAttachment Attachment

*' Send the message.
x=1
FOR i = 1 TO x

Endfor

oMsg.Send
Return
