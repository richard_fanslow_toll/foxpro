#include SFReporting.H

* File extensions.

#define ccFILE_EXT_EMF				'EMF'
#define ccFILE_EXT_TIFF1			'TIF'
#define ccFILE_EXT_TIFF2			'TIFF'
#define ccFILE_EXT_GIF				'GIF'
#define ccFILE_EXT_JPG1				'JPG'
#define ccFILE_EXT_JPG2				'JPEG'
#define ccFILE_EXT_PNG				'PNG'
#define ccFILE_EXT_BMP				'BMP'

* Localized strings.

#define ccSTR_PAGE_X_OF_Y			'Page <Insert1> of <Insert2>'

* Directives.

#define ccDIRECTIVE_LISTENER		'*:LISTENER'
#define ccDIRECTIVE_ROTATE			'ROTATE'
#define ccDIRECTIVE_FORECOLOR		'FORECOLOR'
#define ccDIRECTIVE_BACKCOLOR		'BACKCOLOR'
#define ccDIRECTIVE_ALPHA			'ALPHA'
#define ccDIRECTIVE_STYLE			'STYLE'
