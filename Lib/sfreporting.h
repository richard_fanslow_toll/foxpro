* Include other include files.

#include SFCTRLS.H
#include '\Program Files\Microsoft Visual FoxPro 9\FFC\FoxPro_Reporting.h'
#include '\Program Files\Microsoft Visual FoxPro 9\FFC\GDIPlus.h'

* Object positions options.

#define cnPOSITION_FLOAT            0
#define cnPOSITION_TOP              1
#define cnPOSITION_BOTTOM           2

* ccAVG_CHAR is the "average" character in a font used to determine object
* sizing.

#define ccAVG_CHAR                  'N'

* Range of alpha values.

#define cnPEN_ALPHA_MIN				  0
#define cnPEN_ALPHA_MAX				255

* The value in the FRX for default colors.

#define cnCOLOR_DEFAULT				-1

* The bits and characters used for FONTSTYLE.

#define ccSTYLE_BOLD            	'B'
#define cnSTYLE_BOLD_BIT            0
#define ccSTYLE_ITALIC          	'I'
#define cnSTYLE_ITALIC_BIT          1
#define ccSTYLE_UNDERLINE       	'U'
#define cnSTYLE_UNDERLINE_BIT       2
#define ccSTYLE_STRIKEOUT       	'-'
#define cnSTYLE_STRIKEOUT_BIT       7
