**********************************************************************
* Added this block 11/22/2017 MB to prevent more than one instance of this process from running at the same time, which was causing 'file not found' errors.
LOCAL lnError, lcProcessName

DECLARE INTEGER CreateMutex IN Win32API ;
	STRING @lpMutexAttributes, ;
	STRING bInitialOwner, ;
	STRING @lpName

DECLARE INTEGER GetLastError IN Win32API

* if running in EXE check to see if already running
IF _VFP.STARTMODE > 0 THEN
	lcProcessName = "BILLABONG832"
	CreateMutex(0,CHR(1),@lcProcessName)
	lnError = GetLastError()
	IF lnError = 183
		WAIT WINDOW TIMEOUT 10 "BILLABONG832 process is already running..."
		RETURN .F.
	ENDIF
ENDIF
***********************************************************************

utilsetup("BILLABONG832")
close data all

goffice="L"
gmasteroffice="L"

public ltesting
public array a856(1)

ltesting = .f.
do m:\dev\prg\_setvars with ltesting

useca("upcmast","wh",,,"select * from upcmast where accountid in (6718,6744,6747)",,"upcmast")
xsqlexec("select * from upcmast where accountid in (6718,6744,6747)","xupcmast",,"wh")
select xupcmast
index on upc tag upc
set order to

lccurrdir = ""

if !ltesting
	lcpath = 'F:\FTPUSERS\Billabong\832in\'
	lcarchivepath = 'F:\FTPUSERS\Billabong\832in\archive\'
	tsendto = "tmarg@fmiint.com"
	tcc = "tmarg@fmiint.com"
else
	lcpath = 'F:\FTPUSERS\Billabong\832in\'
	lcarchivepath = 'F:\FTPUSERS\Billabong\832in\archive\'
	tsendto="tmarg@fmiint.com"
	tcc=""
endif

cd &lcpath
len1 = adir(ary1,"*.*")
if len1 = 0
	wait window "No files found...exiting" timeout 2
	close data all
	schedupdate()
	_screen.caption=gscreencaption
	on error
	return
endif

lcaddedstr  ="Styles Added:"+chr(13)
lcupdatestr ="Styles Updated:"+chr(13)

for xtodd = 1 to len1
	cfilename = alltrim(ary1[xtodd,1])
	xfile = lcpath+cfilename

	select * ;
	from upcmast ;
	where .f. ;
	into cursor tempmast readwrite
	alter table tempmast drop column upcmastid
	do m:\dev\prg\createx856a
	do m:\dev\prg\loadedifile with xfile,"|","TILDE"

	m.addby = "FMI-PROC"
	m.adddt = datetime()
	m.addproc = "BILL832"
*  m.accountid =6718
	m.pnp = .t.
	m.uom = "EA"

	select x856


	locate
	scan

		do case
		case x856.segment = "LIN"
**reset in case segments do not exist for upc - mvw 03/19/14
			store "" to m.sid,m.info,m.descrip,m.coo
			store 0 to m.price,m.rprice

			if x856.f4 = "UP"
				m.upc = allt(x856.f5)
			endif
			if x856.f2 = "VC"
				m.style = allt(x856.f3)
			endif
			if x856.f6 = "CL"
				m.color = allt(x856.f7)
			endif
			if x856.f8 = "IZ"
				m.id = allt(x856.f9)
			endif
			if x856.f10 = "CH"
				m.coo =allt(x856.f11)
			endif
			if x856.f12 = "N1"
				m.info = m.info+chr(13)+"NAFTA*"+allt(x856.f13)
			endif
		case x856.segment = "PO1"
			if inlist(x856.f3,"UN","EA")
				m.uom = "EA"
			else m.uom="CS"
			endif
			m.price = val(x856.f4)
			m.info = m.info+chr(13)+"CATEGORY*"+allt(x856.f7)
			m.info = m.info+chr(13)+"SUBCAT*"+allt(x856.f9)
			m.info = m.info+chr(13)+"SUBSUBCAT*"+allt(x856.f11)
			m.info = m.info+chr(13)+"ARCHETYPE*"+allt(x856.f13)
			m.info = m.info+chr(13)+"PRODTYPE*"+allt(x856.f15)
			m.info = m.info+chr(13)+"PRICEARCH*"+allt(x856.f17)
			m.info = m.info+chr(13)+"CATDESC2*"+allt(x856.f21)
			m.info = m.info+chr(13)+"DIVISION*"+allt(x856.f25)


		case x856.segment = "PID" and x856.f1 = "F" and x856.f2 = "BND"
			m.info = m.info+chr(13)+"BRANDCODE*"+alltrim(upper(x856.f4))
			m.info = m.info+chr(13)+"BRANDDESC*"+allt(x856.f5)

			do case
			case alltrim(upper(x856.f4))="BB"
				m.accountid= 6744
				ccustname='BILLABONG'
			case alltrim(upper(x856.f4))="EL"
				m.accountid= 6718
				ccustname='BILLABONG - ELEMENT'
			case alltrim(upper(x856.f4))="RV"
				m.accountid= 6747
				ccustname='BILLABONG - RVCA'
			case alltrim(upper(x856.f4))="CA"
				m.accountid= 6744
				ccustname='BILLABONG'
			case alltrim(upper(x856.f4))="EC"
				m.accountid= 6718
				ccustname='BILLABONG - ELEMENT'
			case alltrim(upper(x856.f4))="CR"
				m.accountid= 6747
				ccustname='BILLABONG - RVCA'
			endcase


		case x856.segment = "PID" and x856.f1 = "F" and x856.f2 = "73"
			m.info = m.info+chr(13)+"COLORDESC*"+alltrim(upper(x856.f5))

		case x856.segment = "PID" and x856.f1 = "F" and x856.f2 = "74"
			m.info = m.info+chr(13)+"SIZEDESC*"+alltrim(upper(x856.f5))

		case x856.segment = "PID" and x856.f1 = "F" and x856.f2 = "92"
			m.info = m.info+chr(13)+"MATCONTCODE*"+alltrim(upper(x856.f4))
			m.info = m.info+chr(13)+"MATCONT*"+alltrim(upper(x856.f5))

		case x856.segment = "PID" and x856.f1 = "F" and x856.f2 = "63"
			m.info = m.info+chr(13)+"SEASONCODE*"+alltrim(upper(x856.f4))
			m.info = m.info+chr(13)+"SEASON*"+alltrim(upper(x856.f5))

		case x856.segment = "PID" and x856.f1 = "F" and x856.f2 = "08"
			m.descrip = allt(x856.f5)

		case x856.segment = "TC2" and x856.f1 ="A"
			m.htscode = allt(x856.f2)

		case x856.segment = "TC2" and x856.f1 ="C"
			m.info = m.info+chr(13)+"HTSCA*"+alltrim(upper(x856.f2))

		case x856.segment = "CTP" and x856.f2 = "MSR"
			m.rprice = val(x856.f3)

		case x856.segment = "CTP" and x856.f2 = "RTL"
			m.info = m.info+chr(13)+"RTLPRICE*"+(x856.f3)
			skip
			m.info = m.info+"-"+(x856.f2)

		case x856.segment = "N1" and x856.f1 = "SE"
			m.info = m.info+chr(13)+"VENDOR*"+(x856.f2)
			m.info = m.info+chr(13)+"VENDORCODE*"+(x856.f4)
			insert into tempmast from memvar
		endcase
	endscan


	select upcmast
	scatter memvar memo blank
*** we have read in this file into tempmast
*** now check for 6303
	select tempmast
	recctr=0
	scan
		scatter memvar memo
		recctr = recctr +1
		numadded = 0
		numupdated = 0
		m.problem=.f.
		m.adddt=datetime()
		m.updatedt=datetime()
		m.addby='BILL832'
		m.updateby='BILL832'
**    m.accountid=6718
		m.addproc='BILL832'
		m.updproc='BILL832'
*!*      select upcmast
*!*      scatter fields except upcmastid memvar memo blank
*!*      select tempmast && has all new records.......
**From tempmast we check for 6303 and add or update 6303 records

		if !empty(upc)
			release m.cube, m.weight
			if !seek(alltrim(tempmast.upc),"xupcmast","upc")
				**get cube/weight from another item in the same category - mvw 01/16/18
				xcategory=getmemodata("m.info","CATEGORY",,.t.)
				xsubcategory=getmemodata("m.info","CATDESC2",,.t.)
				if !empty(xcategory) or !empty(xsubcategory)
					select xupcmast
					xfound=.f.
					if !empty(xsubcategory)
						locate for "CATEGORY*"+alltrim(xcategory)$info and "CATDESC2*"+xsubcategory$info
						xfound=found()
					endif
					if !xfound
						locate for "CATEGORY*"+alltrim(xcategory)$info
					endif
					scatter fields cube, weight memv
				endif

				m.flag=.f.
				insertinto("upcmast","wh",.t.)
			else
				select upcmast
				locate for alltrim(upc)=alltrim(tempmast.upc)

*!*          replace style                with t2.style   in upcmast
*!*          replace color               with alltrim(t2.color)   in upcmast
*!*          replace id                      with alltrim(t2.id)   in upcmast
*!*          replace descrip           with t2.descrip   in upcmast
*!*          replace info                  with m.info   in upcmast
*!*          replace updatedt        with datetime()   in upcmast

				if found() then
					m.updatedt = datetime()
					replace upcmast.updatedt     with m.updatedt in upcmast
					replace upcmast.upc          with tempmast.upc in upcmast
					replace upcmast.sid          with tempmast.sid in upcmast
					replace upcmast.price        with tempmast.price in upcmast
					replace upcmast.rprice       with tempmast.rprice in upcmast
					replace upcmast.style        with tempmast.style in upcmast
					replace upcmast.color        with tempmast.color in upcmast
					replace upcmast.id           with tempmast.id in upcmast
					replace upcmast.htscode        with tempmast.htscode in upcmast
					replace upcmast.descrip      with upper(tempmast.descrip) in upcmast
					replace upcmast.info         with upper(tempmast.info) in upcmast
					replace upcmast.flag         with .f. in upcmast
					replace upcmast.whseloc      with '' in upcmast
				endif
			endif
		endif

*!*    scan
*!*      recctr = recctr +1
*!*      scatter fields except upcmastid memvar memo
*!*      select upcmastsql
*!*  *    locate for  accountid =tempmast.accountid and upc=tempmast.upc
*!*    =Seek(Str(upc,"upcmastsql","upc")
*!*  *   =Seek(Str(accountid,4)+style+color+id,"upcmast","stylecolid")
*!*      wait window at 10,10 "Checking Record # "+transform(recctr) nowait
*!*      if !found()
*!*        numadded = numadded +1
*!*  ************************TMARG
*!*  *!*        Select upcmast
*!*  *!*        Go Bott
*!*  *!*        m.adddt = Datetime()
*!*  *!*        Cd m:\dev
*!*  *!*        m.upcmastid=GENPK('UPCMAST','WHALL')
*!*  *!*        Insert Into upcmast  From Memvar
*!*  *!*        lcAddedStr = lcAddedStr +m.upc+"-"+m.style+"-"+m.color+"-"+m.id+Chr(13)
*!*  *!*        Insert Into upcmastsql  From Memvar
*!*  *!*      Else
*!*  *!*        Replace upcmast.upc          With tempmast.upc In upcmast
*!*  *!*        Replace upcmast.sid          With tempmast.sid In upcmast
*!*  *!*        Replace upcmast.price        With tempmast.price In upcmast
*!*  *!*  *      Replace upcmast.rprice       With tempmast.rprice In upcmast
*!*  *!*        Replace upcmast.Style        With tempmast.Style In upcmast
*!*  *!*        Replace upcmast.Color        With tempmast.Color In upcmast
*!*  *!*        Replace upcmast.Id           With tempmast.Id In upcmast
*!*  *!*        Replace upcmast.updatedt     With tempmast.adddt In upcmast
*!*  *!*        Replace upcmast.Descrip      With Upper(tempmast.Descrip) In upcmast
*!*  *!*        Replace upcmast.Info         With Upper(tempmast.Info) In upcmast
*!*  *!*        NumUpdated = NumUpdated +1
*!*  *!*        lcUpdateStr = lcUpdateStr +tempmast.upc+"-"+tempmast.Style+"-"+tempmast.Color+"-"+tempmast.Id+Chr(13)

*!*  *!*        Select upcmastsql
*!*  *!*        Locate For upcmastid=upcmast.upcmastid
*!*  *!*        If !Found()
*!*        m.flag=.f.
*!*        m.adddt = datetime()
*!*        m.updatedt = datetime()
*!*        m.upcmastid=dygenpk('UPCMAST','WHALL')
*!*        insert into upcmastsql  from memvar
*!*  *!*        Replace upcmastsql.upcmastid    With upcmast.upcmastid In upcmastsql
*!*  *!*        Replace upcmastsql.updatedt     With upcmast.updatedt In upcmastsql
*!*  *!*        Replace upcmastsql.adddt     With upcmast.adddt In upcmastsql
*!*  *!*          If m.problem=.F.
*!*  *!*            tsendto = "tmarg@fmiint.com,todd.margolin@tollgroup.com"
*!*  *!*            tcc =""
*!*  *!*            tattach = ""
*!*  *!*            tFrom ="Toll EDI System Operations <fmi-transload-ops@fmiint.com>"
*!*  *!*            tmessage = "Missing UPC master data in upcmastsql" +Dtoc (Date())
*!*  *!*            tSubject = "Missing UPC master data in upcmastsql"
*!*  *!*            Do Form m:\dev\frm\dartmail2 With tsendto,tFrom,tSubject, tcc,tattach,tmessage  ,"A"
*!*  *!*            m.problem=.T.
*!*  *!*          Else
*!*  *!*          Endif
*!*      else

*!*        m.updatedt = datetime()
*!*        replace upcmastsql.updatedt     with m.updatedt in upcmastsql
*!*        replace upcmastsql.upc          with tempmast.upc in upcmastsql
*!*        replace upcmastsql.sid          with tempmast.sid in upcmastsql
*!*        replace upcmastsql.price        with tempmast.price in upcmastsql
*!*        replace upcmastsql.rprice       with tempmast.rprice in upcmastsql
*!*        replace upcmastsql.style        with tempmast.style in upcmastsql
*!*        replace upcmastsql.color        with tempmast.color in upcmastsql
*!*        replace upcmastsql.id           with tempmast.id in upcmastsql
*!*        replace upcmastsql.htscode        with tempmast.htscode in upcmastsql
*!*        replace upcmastsql.descrip      with upper(tempmast.descrip) in upcmastsql
*!*        replace upcmastsql.info         with upper(tempmast.info) in upcmastsql
*!*        replace upcmastsql.flag         with .f. in upcmastsql
*!*        replace upcmastsql.whseloc      with '' in upcmastsql
*!*      endif
*!*  && add in all the updates

	endscan

	tu("upcmast")
************************************** END   AX2012
	tattach = ""
	tfrom ="Toll/FMI EDI System Operations <fmi-transload-ops@fmiint.com>"
	tmessage = "Billabong Stylemaster updated at: "+ttoc(datetime())+chr(13)+;
	chr(13)+lcaddedstr+chr(13)+;
	chr(13)+lcupdatestr

	tsubject = "Billabong Stylemaster Update"
	do form m:\dev\frm\dartmail2 with tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
	wait window "All 832 Files finished processing...exiting" timeout 2
	cloadfile = (lcpath+cfilename)
	use in tempmast
	set step on
	carchivefile = (lcarchivepath+cfilename)
	copy file [&cLoadFile] to [&cArchiveFile]

	if !ltesting
		delete file [&cLoadFile]
	endif
endfor
close data all

schedupdate()
*_Screen.Caption=gscreencaption
on error
