LPARAMETERS xcursor, xdatabase

xcursor=UPPER(xcursor)
xdatabase=UPPER(xdatabase)
xgenpkalias=xdatabase+"genpk"

IF !USED(xgenpkalias)
	DO CASE
		CASE xdatabase="WH" and len(trim(xdatabase))=3
			xdycursor=xcursor+"-"+substr(xdatabase,3,1)		&& dy 12/24/17
			xnextvalue=sqlgenpk(xdycursor,"wh")
			return xnextvalue
		CASE xdatabase="WHALL"
			xdycursor=xcursor	&& dy 12/24/17
			xnextvalue=sqlgenpk(xdycursor,"wh")
			return xnextvalue
		CASE xdatabase="WHI"
			xgenpktable="f:\whi\whdata\whgenpk"
		CASE xdatabase="WHJ"
			xgenpktable="f:\whj\whdata\whgenpk"
		CASE xdatabase="WH1"
			xgenpktable="f:\wh1\whdata\whgenpk"
		CASE xdatabase="WH2"
			xgenpktable="f:\wh2\whdata\whgenpk"
		CASE xdatabase="WH5"
			xgenpktable="f:\wh5\whdata\whgenpk"
		CASE xdatabase="WH6"
			xgenpktable="f:\wh6\whdata\whgenpk"
		CASE xdatabase="WH7"
			xgenpktable="f:\wh7\whdata\whgenpk"
		CASE xdatabase="WH8"
			xgenpktable="f:\wh8\whdata\whgenpk"
		CASE xdatabase="WHM"
			xgenpktable="f:\whm\whdata\whgenpk"
		CASE xdatabase="WHK"
			xgenpktable="f:\whk\whdata\whgenpk"
		CASE xdatabase="WHS"
			xgenpktable="f:\whs\whdata\whgenpk"
		CASE xdatabase="WHR"
			xgenpktable="f:\whr\whdata\whgenpk"
		CASE xdatabase="WHL"
			xgenpktable="f:\whl\whdata\whgenpk"
		CASE xdatabase="WHO"
			xgenpktable="f:\who\whdata\whgenpk"
		CASE xdatabase="WHX"
			xgenpktable="f:\whx\whdata\whgenpk"
		CASE xdatabase="WHY"
			xgenpktable="f:\why\whdata\whgenpk"
		CASE xdatabase="WHZ"
			xgenpktable="f:\whz\whdata\whgenpk"
		CASE xdatabase="WHP"
			xgenpktable="f:\whp\whdata\whgenpk"
		CASE xdatabase="WH8"
			xgenpktable="f:\wh8\whdata\whgenpk"
		CASE xdatabase="AR"
			xgenpktable="f:\watusi\ardata\argenpk"
			email("Dyoung@fmiint.com","INFO: dygenpk called for AR",xcursor,,,,.t.,,,,,.t.,,.t.)
		CASE xdatabase="EX"
			xgenpktable="f:\ex\exdata\exgenpk"
		CASE xdatabase="ME"
			xgenpktable="f:\monthend\medata\megenpk"
		CASE xdatabase="BT"
			xgenpktable="f:\bugtrack\btdata\btgenpk"
		CASE xdatabase="HR"
			xgenpktable="f:\hr\hrdata\hrgenpk"
		CASE xdatabase="OO"
			xgenpktable="f:\ownerop\oodata\oogenpk"
		CASE xdatabase="QQ"
			xgenpktable="f:\sysdata\qqdata\qqgenpk"
		CASE xdatabase="RC"
			xgenpktable="f:\rcv\rcdata\rcgenpk"
		CASE xdatabase="SH"
			xgenpktable="f:\shop\shdata\shgenpk"
		CASE xdatabase="FX"
			xgenpktable="f:\express\fxdata\fxgenpk"
			email("Dyoung@fmiint.com","INFO: dygenpk called for FX",,,,,.t.,,,,,.t.,,.t.)
		CASE xdatabase="US"
			xgenpktable="f:\users\usdata\usgenpk"
		CASE xdatabase="TY"
			xgenpktable="f:\trailyrd\tydata\tygenpk"
		CASE xdatabase="WO"
			xgenpktable="f:\wo\wodata\wogenpk"
		OTHERWISE
			SET STEP ON
			crlf = CHR(13)+CHR(10)
			email("Dyoung@fmiint.com","INFO: genpk table not found in dygenpk",xcursor+crlf+xdatabase,,,,.T.,,,,,.T.,,.T.)
			RETURN VAL(SYS(3))
	ENDCASE

	USE (xgenpktable) ALIAS (xgenpkalias) AGAIN IN 0
	CURSORSETPROP("buffering",2,xgenpkalias)
ENDIF

**added in case the table was previously open without the correct buffering - mvw 03/15/17
if cursorgetprop("Buffering",xgenpkalias)#2
	CURSORSETPROP("buffering",2,xgenpkalias)
endif

IF !SEEK(PADR(xcursor,128),xgenpkalias,"gpk_pk")	&& add into genpk if first time
	INSERT INTO &xgenpkalias (gpk_pk) VALUE (xcursor)
ENDIF

* removed try/catch   dy 4/9/17
* fully implemented	  dy 4/19/17

REPLACE gpk_currentnumber WITH &xgenpkalias..gpk_currentnumber+1 IN (xgenpkalias)

*!*	if inlist(xcursor,"PT","ACKDATA")
*!*		REPLACE gpk_currentnumber WITH &xgenpkalias..gpk_currentnumber+1 IN (xgenpkalias)
*!*	else
*!*		try
*!*			REPLACE gpk_currentnumber WITH &xgenpkalias..gpk_currentnumber+1 IN (xgenpkalias)
*!*		catch
*!*			set step on
*!*		endtry
*!*	endif

if _tally=0
	email("Dyoung@fmiint.com","INFO: dygenpk failed",xcursor,,,,.t.,,,,,.t.,,.t.)
endif

xreturn=&xgenpkalias..gpk_currentnumber

*Wait Window At 10,10 "got ID: "+Transform(xreturn) nowait 

GO BOTTOM IN (xgenpkalias)
SKIP IN (xgenpkalias)

RETURN xreturn
