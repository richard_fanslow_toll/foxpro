*!* Nanjing940_start.prg

PARAMETERS cOfficeIn

runack("NANJING940")

IF TYPE("cOfficeIn") = "L"
	cOfficeIn = "Y"
*!*	WAIT WINDOW "No Office Provided...try again" TIMEOUT 2
*!*	RETURN
ENDIF

CLOSE DATABASES ALL
CLEAR
*!* Set and initialize public variables
PUBLIC cWhichfile,nRunID,xFile,cXdate1,dXdate2,cOffice,lTesting,cUseDir,cCustName,cProperName
PUBLIC cDelimiter,cTranslateOption,EmailCommentStr,LogCommentStr,nAcctNum,cAcct_num,l997
PUBLIC chgdate,ptid,ptctr,nPTQty,lTestUploaddet,cOfficename,cConsignee,cStoreNum,cTotPT
PUBLIC cPickticket_num_start,cPickticket_num_end,cLoadID,lcPath,lcStr,cUseFolder,lADNY,cTransfer
PUBLIC lcArchivePath,lBrowFiles,NormalExit,lTestImport,Archivefile,tfrom,tsendto,tcc,lcArchivefile
PUBLIC lTestTableLoad,tsendtoerr,tccerr,lOverrideBusy,cFilename,cMod,cISA_Num,nFileSize
PUBLIC cSCCNumber,m.sccnumber,cCHKDigit,m.chkdigit,cartonid,m.cartonid,cSTStoreNum
PUBLIC m.custsku,cSizenum,ptid,accountid,m.id,m.ptid,m.acct_num,lOldStyle,lPrepackRep,lLoadSQL
DO m:\dev\prg\lookups

TRY
	lTestRun = .f.  && Default = .f.
	lTestImport = lTestRun  && Default = .f.
	lTestTableLoad = .F.  && Rarely set; Default = .f.
	lOverrideBusy = .f.  && If set, will overcome FTPSETUP chkbusy flag; default = .f.
	lBrowFiles = lTestRun
	DO m:\dev\prg\_setvars WITH lTestRun

*	lBrowFiles = .t.
*	lOverrideBusy = .t.

	NormalExit = .F.
	PUBLIC ARRAY a856(1)
	ON ERROR debug

	_SCREEN.CAPTION = "Nanjing USA 940 Process"
*	_SCREEN.WINDOWSTATE = IIF(lTesting,2,1)
	IF VARTYPE(cOfficeIn) # "C"
		cOfficeIn = "Y"
		cOffice = cOfficeIn
	ENDIF
	tfrom    ="Toll EDI Processing Center <toll-edi-ops@tollgroup.com>"

	cTransfer = "940-NANJING"
	IF !lTesting
		SELECT 0
		USE F:\edirouting\ftpsetup SHARED
		IF ftpsetup.chkbusy AND !lOverrideBusy
			WAIT WINDOW "Nanjing 940 already in process...exiting" TIMEOUT 1
			NormalExit = .T.
			THROW
		ENDIF
		REPLACE chkbusy WITH .T.,trig_time WITH DATETIME() FOR ftpsetup.TRANSFER = cTransfer
		USE IN ftpsetup
	ENDIF

	STORE "" TO cPickticket_num_start,cPickticket_num_end,EmailCommentStr,cLoadID,LogCommentStr,xFile,cXdate1
	STORE "" TO tsendto,tcc,tsendtoerr,tccerr
	STORE cOfficeIn TO cOffice
	gMasterOffice = IIF(lTestRun,"P",cOffice)
	IF lTestRun
		cMod = "P"
	ELSE
		cMod = IIF(cOffice = "C","1",cOffice)
	ENDIF
	gOffice = cMod
	gMasterOffice = IIF(cOffice="I","N",gMasterOffice)

	lLoadSQL = .T.

	cOfficename = ICASE(cOffice = "C","California",cOffice = "I","New Jersey",cOffice = "M","Florida","Carson 2")
	nAcctNum = IIF(cOffice="Y",4610,4694)

	_SCREEN.WINDOWSTATE=IIF(lTestImport OR lOverrideBusy,2,1)
	dXdate2 = DATE()
	nRunID = 999
	cAcct_num = ALLTRIM(STR(nAcctNum))

	SELECT 0
	USE F:\3pl\DATA\mailmaster ALIAS mm
	LOCATE FOR mm.edi_type = "940" AND mm.accountid = nAcctNum AND mm.office = cOfficeIn
	IF FOUND()
		STORE TRIM(mm.acctname) TO cCustName
		STORE TRIM(mm.basepath) TO lcPath
		STORE TRIM(mm.archpath) TO lcArchivePath
		IF lTestImport
			LOCATE FOR mm.edi_type = "MISC" AND mm.taskname = "GENERAL"
			STORE TRIM(mm.sendto) TO tsendtotest
			STORE TRIM(mm.cc)   TO tcctest
		ENDIF
		STORE TRIM(mm.sendto) TO tsendto
		STORE TRIM(mm.cc)   TO tcc
		LOCATE FOR mm.accountid = 9999 AND mm.office = "X"
		STORE TRIM(mm.sendto) TO tsendtoerr
		STORE TRIM(mm.cc)   TO tccerr
		USE IN mm
	ELSE
		WAIT WINDOW AT 10,10  "No parameters set for this acct# "+ALLTRIM(STR(Acctnum))+"  ---> Office "+lcOffice TIMEOUT 2
		USE IN mm
		RELEASE ALL
		RETURN
	ENDIF

	IF lTestImport
		IF lTestRun
			lcPath = ALLTRIM(lcPath)+"TEST\"
			WAIT WINDOW "PATH: "+lcPath TIMEOUT 2
		ENDIF
		cUseFolder = "F:\WHP\WHDATA\"
	ELSE
		xReturn = "XXX"
		DO m:\dev\prg\wf_alt WITH cOffice,nAcctNum
		cUseFolder = UPPER(xReturn)
	ENDIF

	cProperName = "Nanjing USA"
	cDelimiter = CHR(0x07)
	cTranslateOption ="NANJING"
	cUseDir = ("m:\dev\prg\"+cCustName+"940_")
	l997 = .F.

	IF lTestRun
		lTesting = .T.
		lTestUploaddet = .T.
	ELSE
		lTesting = .F.
		lTestUploaddet = .F.
	ENDIF

	DO m:\dev\prg\createx856a

	WAIT CLEAR
	DO (cUseDir+"PROCESS") WITH cOffice

	NormalExit = .T.
	THROW
CATCH TO oErr
	IF NormalExit = .F.
		ASSERT .F. MESSAGE "At Error Catch..."
		tsubject = "Nanjing 940 Upload Error ("+TRANSFORM(oErr.ERRORNO)+") at "+TTOC(DATETIME())
		tattach  = ""
		tsendto  = tsendtoerr
		tcc = tccerr
		tmessage = "Nanjing 940 Upload Error..... Please fix me........!"
		lcSourceMachine = SYS(0)
		lcSourceProgram = SYS(16)

		tmessage =tmessage+ "Try/CATCH Exeception Message:"+CHR(13)+;
			[  Error: ] + STR(oErr.ERRORNO) +CHR(13)+;
			[  LineNo: ] + STR(oErr.LINENO) +CHR(13)+;
			[  Message: ] + oErr.MESSAGE +CHR(13)+;
			[  Procedure: ] + oErr.PROCEDURE +CHR(13)+;
			[  Details: ] + oErr.DETAILS +CHR(13)+;
			[  StackLevel: ] + STR(oErr.STACKLEVEL) +CHR(13)+;
			[  LineContents: ] + oErr.LINECONTENTS+CHR(13)+;
			[  UserValue: ] + oErr.USERVALUE+CHR(13)+;
			[  Computer:  ] +lcSourceMachine+CHR(13)+;
			[  940 file:  ] +xFile+CHR(13)+;
			[  Program:   ] +lcSourceProgram

		tattach  = ""
		tfrom    ="Toll EDI Processing Center <toll-edi-ops@tollgroup.com>"
		DO FORM m:\dev\frm\dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
	ELSE
		WAIT WINDOW AT 10,10 "Normal Exit " TIMEOUT 2
		SELECT 0
		USE F:\edirouting\ftpsetup SHARED
		REPLACE chkbusy WITH .F. FOR ftpsetup.TRANSFER = cTransfer
		USE IN ftpsetup
	ENDIF
FINALLY
	CLOSE DATABASES ALL
ENDTRY
