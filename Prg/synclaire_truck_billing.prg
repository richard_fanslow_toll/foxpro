Do m:\dev\prg\lookups

Do _setvars

Set Safety Off
Public gsystemmodule

gsystemmodule ="wh"

runack("SYNCLAIRE_TRUCKBILLING")

goffice="2"

Wait Window At 10,10 "Synclaire Trucking Billing Processing..........." Nowait
close data all 

xsqlexec("select * from glacct",,,"ar")

xsqlexec("select invoice.invnum, header, invdt, invamt, xwo_num, invoice.wo_num, invdet.wo_num as wo_numd, " + ;
	"linenum, description, glcode, invdetamt,invdetid from invoice, invdet where invoice.invoiceid=invdet.invoiceid " + ;
  	"and invdt>={"+dtoc(date()-365)+"} and inlist(accountid,6521,6221) and inlist(company,'U','T','X','I')","xdy",,"ar")

select invnum, header, invdt, invamt, xwo_num, wo_num, space(10) as truck_wonum, wo_numd, ; 
	linenum, description, space(30) as desc2, space(30) as desc3, space(30) as desc4, space(30) as desc5, ;
	substr(description,4,8) as wo, glcode, space(30) as type, invdetamt, invdetid, space(10) as billing ;
	from xdy into cursor temp readwrite


*  WO: 1712262 
*  WO# 1710887

Select temp
Scan
  Do case
  case "WO:"$description
    lnStart = At("WO:",description)
    If lnStart > 0
      tkwo= Substr(description,lnStart+4,7)
      replace truck_wonum With tkwo
    Endif  
  case "WO#"$description
    lnStart = At("WO:",description)
    If lnStart > 0
      tkwo= Substr(description,lnStart+4,7)
      replace truck_wonum With tkwo
    Endif  
  Otherwise 
      replace wo With ""
Endcase 
  lnLines =Alines(ah,header)
  If lnLines >=1
  replace desc2 With ah[1]
  Endif 
  If lnLines >=2
  replace desc3 With ah[2]
  Endif 
  If lnLines >=3
  replace desc4 With ah[3]
  Endif 
  If lnLines >=4
  replace desc5 With ah[4]
  Endif 
  Select glacct
  Locate For temp.glcode = glcode
  If Found()
   replace temp.type With glacct.description 
  Else
   replace temp.type With "NA"
  Endif 
  Select temp
Endscan
