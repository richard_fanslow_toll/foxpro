


 **This script will identify BOLs with multiple accounts
utilsetup("MJ_MULTIPLE_ACCT_ON_BOL")
Set Exclusive Off
Set Deleted On
Set Talk Off
Set Safety Off 
WITH _SCREEN	
	.AUTOCENTER = .T.
	.WINDOWSTATE = 0
	.BORDERSTYLE = 1
	.WIDTH = 320
	.HEIGHT = 210
	.TOP = 290
	.LEFT = 110
	.CLOSABLE = .T.
	.MAXBUTTON = .t.
	.MINBUTTON = .T.
	.SCROLLBARS = 0
	.CAPTION = "MJ_MULTIPLE_ACCT_ON_BOL"
ENDWITH	
schedupdate()

goffice="J"

xsqlexec("select bol_no, accountid from outship where mod='J' " + ;
	"and inlist(accountid,6303,6304,6305,6320,6321,6322,6323,6364) " + ;
	"and del_date>{"+dtoc(date()-90)+"}","xdy",,"wh")

select bol_no, accountid from xdy where !inlist(bol_no,'04070086303321223','04070086304626730','04070086304690182','04070086303767052','04070086303791804') ;
	group by 1,2 into cursor temp22 readwrite

SELECT bol_no, COUNT(1) FROM temp22 GROUP BY 1 HAVING COUNT(1) >1 INTO CURSOR temp23 readwrite
 set step on 
 SELECT  temp23
If Reccount() > 0
  Export To "S:\MarcJacobsData\TEMP\mj_multiple_acct_on_bol"  Type Xls


  tsendto = "tmarg@fmiint.com"
  tattach = "S:\MarcJacobsData\TEMP\mj_multiple_acct_on_bol.xls"
  tcc =""
  tFrom ="Toll EDI System Operations <fmi-transload-ops@fmiint.com>"
  tmessage = "Multiple accounts exist on a BOL  "+Ttoc(Datetime()) + "Ensure 945's and slip/vouchers were created"
  tSubject = "Multiple accounts exist on a BOL"
  Do Form m:\dev\frm\dartmail2 With tsendto,tFrom,tSubject, tcc,tattach,tmessage  ,"A"
ELSE 
  tsendto = "tmarg@fmiint.com"
  tattach = ""
  tcc =""
  tFrom ="Toll EDI System Operations <fmi-transload-ops@fmiint.com>"
  tmessage = "NO_Multiple accounts exist on a BOL "+Ttoc(Datetime())
  tSubject = "NO_Multiple accounts exist on a BOL"
  Do Form m:\dev\frm\dartmail2 With tsendto,tFrom,tSubject, tcc,tattach,tmessage  ,"A"
ENDIF



Close Data All
_Screen.Caption=gscreencaption
