**wipXls.prg
**
**create a 6 page work in progress worksheet
**  page 1 - Undelivered Freight (Not UPS/RPS)
**  page 2 - Delivered Last 10 Days (Not UPS/RPS)
**  page 3 - Picked & Unshipped (Not UPS/RPS)
**  page 4 - UPS/RPS - Undelivered Freight
**  page 5 - UPS/RPS - Delivered Last 10 Days
**  page 6 - UPS/RPS - Picked & Unshipped
**  page 7 - All delivered (Pony only)
**
** 05/28/2015 MB - misc changes to add style descript to the report per Maria/PG

parameters lwebsite, lstyledetail, xdelfilter, unshippedonly
**unshippedonly - only populate undelivered tabs to save time generating report, user is prompted on shipment screen - mvw 05/23/16

if empty(xdelfilter)	&& only used if delivery date filter on WIP screen is entered  dy 9/15/08
	xdelfilter=""
endif

xacctname=acctname(xaccountid)
=seek(xaccountid,"account","accountid")
if type("gmasteroffice")="C"
	cfield="pnp"+gmasteroffice
else
	cfield="pnp"+xoffice
endif
xacctpnp=account.&cfield

if !lwebsite
	xoffice=goffice
endif

if lwebsite
	select offices
	locate for locale=xoffice and priority=1
	cofficename=offices.city

	if !found()
		select whoffice
		locate for rateoffice=xoffice
		cofficename=whoffice.city
	endif

	cwhpath = wf(xoffice, xaccountid)
	open database &cwhpath.wh

else
	cofficename=goffdesc
endif

cuniqueid = iif(lwebsite, alltrim(str(reports.unique_id)), substr(sys(2015), 3, 10))

public oexcel
oexcel = createobject("Excel.Application")
oexcel.displayalerts = .f.

store "h:\fox\"+cuniqueid+".xls" to cfile

if !empty(xdelfilter)
	copy file f:\webrpt\wipxlsdel.xls to &cfile
else
	copy file f:\webrpt\wipxls2.xls to &cfile
endif

oworkbook = oexcel.workbooks.open(cfile)

ldatafound = .f.

if type("wowipxls")="U"
	do lookups
endif

cups    = "(inlist(ship_via,"+xsmallpackagecarriers+") and !inlist(ship_via,"+xnotsmallpackagecarriers+"))"
cnotups = "(!inlist(ship_via,"+xsmallpackagecarriers+") or inlist(ship_via,"+xnotsmallpackagecarriers+"))"

**change in filter - stole for voutship filter in frmshipment.dataenvironment - mvw 09/24/15
*xqtyfilter="(ctnQty#0 or masterpack)"
xqtyfilter="(((picknpack=.t. or masterpack=.t.) and qty#0) or ctnqty#0)"

**Page 1
if !empty(xdelfilter)
	cfilter=".f."
else
	cfilter = "del_date={} and notOnWip=.f. and "+xqtyfilter
endif
store iif(writepage(cfilter, cnotups, 1), .t., ldatafound) to ldatafound

**Page 2
**changed to match filter in shipment form in wh app
*cfilter = "(del_date >= DATE()-10 or del_date = {}) and !notOnWip and ctnQty # 0"
if !unshippedonly
	if !empty(xdelfilter)
		cfilter = xdelfilter+" and notOnWip=.f. and "+xqtyfilter
		store iif(writepage(cfilter, cnotups, 1), .t., ldatafound) to ldatafound
set step on
		oworkbook.sheets[1].range("A3").value="Delivered between "+substr(xdelfilter,19,10)+" and "+substr(xdelfilter,32,10)
	else
		cfilter = "del_date>={"+dtoc(DATE()-10)+"} and notOnWip=.f. and "+xqtyfilter
		store iif(writepage(cfilter, cnotups, 2), .t., ldatafound) to ldatafound
	endif
endif

**Page 3
if !empty(xdelfilter)
	cfilter=".f."
else
	cfilter = "del_date={} and PICKED#{} and notOnWip=.f. and "+xqtyfilter
endif
store iif(writepage(cfilter, cnotups, 3), .t., ldatafound) to ldatafound

**Page 4

if !empty(xdelfilter)
	cfilter=".f."
else
	cfilter = "del_date={} and notOnWip=.f. and "+xqtyfilter
endif
store iif(writepage(cfilter, cups, 4), .t., ldatafound) to ldatafound

**Page 5
**changed to match filter in shipment form in wh app
*cfilter = "(del_date >= DATE()-10 or del_date = {}) and !notOnWip and ctnQty # 0"
if !unshippedonly
	if !empty(xdelfilter)
		cfilter = ".f."
*		cfilter = xdelfilter+" and notOnWip=.f. and "+xqtyfilter	&& dy 2/13/18
	else
		cfilter = "del_date>={"+dtoc(DATE()-10)+"} and notOnWip=.f. and "+xqtyfilter
	endif
	store iif(writepage(cfilter, cups, 5), .t., ldatafound) to ldatafound
endif

**Page 6
if !empty(xdelfilter)
	cfilter=".f."
else
	cfilter = "del_date={} and PICKED#{} and notOnWip=.f. and "+xqtyfilter
endif
store iif(writepage(cfilter, cups, 6), .t., ldatafound) to ldatafound

**Page 7 - Pony removed dy 12/27/13

oworkbook.sheets[1].activate()
oworkbook.save()

if lwebsite
	oworkbook.close()
	oexcel.quit()
	release oworkbook, oexcel

	select pdf_file
	insert into pdf_file (unique_id) values (reports.unique_id)

	if !ldatafound &&need something to distinguish from "poller down"
		replace memo with "NO DATA"
	else
		**in order to avoid "VFPOLEDB" "provider ran out of memory" errors for large files (>2mb) no longer move file 
		**  thru linked server, just copy file directly to \\iis2\apps\CargoTrackingWeb\reports\ - mvw 03/08/13
		*append memo memo from &cfile overwrite
		copy file "&cfile" to \\iis2\apps\CargoTrackingWeb\reports\*.*
		replace memo with "FILE COPIED TO REPORT FOLDER" in pdf_file
	endif
else
	oexcel.visible = .t.
	release oworkbook, oexcel
endif

if lwebsite
	set database to wh
	close databases
endif


*******************
procedure writepage
parameters cfilter, cupsfilter, npgnum

wait "Gathering data - Page "+transform(npgnum) window nowait noclear

**page 5 (ups, last 10 days) must show individual pts.
if lstyledetail
	*!*	select os.*, sum(iif(!xacctpnp or od.units, od.totqty, 000000)) as qtyfld, iif(combinedpt and npgnum # 5, combpt, ship_ref) as ptnum, ;
	*!*	left(dtoc(start),5) as startview, left(dtoc(min(cancel)),5) as cancelview, ;
	*!*	left(dtoc(picked),5) as pickedview, left(dtoc(max(called)),5) as calledview, ;
	*!*	left(dtoc(max(appt)),5) as apptview, left(dtoc(del_date),5) as delview, ;
	*!*	iif(sp, "Y", "N") as spview, od.style, od.color, od.id, od.pack, space(20) as status ;
	*!*	from outship os left join outdet od on os.outshipid = od.outshipid ;
	*!*	where os.accountid = xaccountid and &cfilter and &cupsfilter group by ptnum, style, color, id, pack ;
	*!*	into cursor csroutship readwrite
	
**changed to only show unit recs for xacctpnp and non-unit recs for !xacctpnp - mvw 09/24/15
*!*		select os.*, sum(iif(!xacctpnp or od.units, od.totqty, 000000)) as qtyfld, iif(combinedpt and npgnum # 5, combpt, ship_ref) as ptnum, ;
*!*		left(dtoc(start),5) as startview, left(dtoc(min(cancel)),5) as cancelview, ;
*!*		left(dtoc(picked),5) as pickedview, left(dtoc(max(called)),5) as calledview, ;
*!*		left(dtoc(max(appt)),5) as apptview, left(dtoc(del_date),5) as delview, ;
*!*		iif(sp, "Y", "N") as spview, od.style, od.color, od.id, SPACE(50) AS DESCRIP, od.pack, space(20) as status ;
*!*		from outship os left join outdet od on os.outshipid = od.outshipid ;
*!*		where os.accountid = xaccountid and &cfilter and &cupsfilter group by ptnum, style, color, id, pack ;
*!*		into cursor csroutship readwrite

	xunitfilter=iif(xacctpnp,"units=.t.","units=.f.")

	xsqlexec("select outship.*, units, totqty, style, color, id, pack from outship, outdet " + ;
		"where outship.outshipid=outdet.outshipid and outship."+gmodfilter+" " + ;
		"and outship.accountid="+transform(xaccountid)+" and "+cfilter+" " + ;
		"and "+xunitfilter,"xdytemp",,"wh")

	select *, sum(iif(!xacctpnp or units, totqty, 000000)) as qtyfld, iif(combinedpt and npgnum # 5, combpt, ship_ref) as ptnum, ;
		left(dtoc(start),5) as startview, left(dtoc(min(cancel)),5) as cancelview, ;
		left(dtoc(picked),5) as pickedview, left(dtoc(max(called)),5) as calledview, ;
		left(dtoc(max(appt)),5) as apptview, left(dtoc(del_date),5) as delview, ;
		iif(sp, "Y", "N") as spview, SPACE(50) AS DESCRIP, space(20) as status, cacctnum as soldto  ;
		from xdytemp where &cupsfilter group by ptnum, style, color, id, pack into cursor csroutship readwrite
	
	* populate style descript in csroutship
	**in order to speed up, get unique styles and only go to sql for all of one style at a time - mvw 10/10/16
*!*		select csroutship
*!*		scan
*!*			if upcmastsql(xaccountid, padr(csroutship.style,20))
*!*				replace csroutship.descrip with upcmast.descrip in csroutship
*!*			endif
*!*		endscan

	select * from csroutship group by style into cursor xtemp
	scan
		if upcmastsql(xaccountid, padr(xtemp.style,20))
			replace descrip with upcmast.descrip for style=xtemp.style in csroutship
		endif
	endscan
	use in xtemp
	
	select csroutship

else
	*!*	select *, sum(ctnqty) as qtyfld, iif(combinedpt and npgnum # 5, combpt, ship_ref) as ptnum, "Asst" as style, "" as color, "" as id, "" as pack, ;
	*!*	left(dtoc(start),5) as startview, left(dtoc(min(cancel)),5) as cancelview, ;
	*!*	left(dtoc(picked),5) as pickedview, left(dtoc(max(called)),5) as calledview, ;
	*!*	left(dtoc(max(appt)),5) as apptview, left(dtoc(del_date),5) as delview, ;
	*!*	iif(sp, "Y", "N") as spview, space(20) as status ;
	*!*	from outship ;
	*!*	where accountid = xaccountid and &cfilter and &cupsfilter group by ptnum ;
	*!*	into cursor csroutship readwrite

	xsqlexec("select * from outship where "+gmodfilter+" and accountid="+transform(xaccountid)+" " + ;
		"and "+cfilter,"xdytemp",,"wh")

	select *, sum(ctnqty) as qtyfld, iif(combinedpt and npgnum # 5, combpt, ship_ref) as ptnum, "Asst" as style, "" as color, "" as id, "" as pack, "" as DESCRIP, ;
		left(dtoc(start),5) as startview, left(dtoc(min(cancel)),5) as cancelview, ;
		left(dtoc(picked),5) as pickedview, left(dtoc(max(called)),5) as calledview, ;
		left(dtoc(max(appt)),5) as apptview, left(dtoc(del_date),5) as delview, ;
		iif(sp, "Y", "N") as spview, space(20) as status, cacctnum as soldto ;
		from xdytemp where &cupsfilter group by ptnum into cursor csroutship readwrite
endif

**fill in new status field - mvw 04/16/13
wait "Adjusting data - Page "+transform(npgnum) window nowait noclear

select csroutship
scan
	do case
	case notonwip and left(keyrec="CANCEL",6)
		replace status with "Ret To Stock"
	case !pulled
		replace status with "Not Pulled"
	case !emptynul(del_date) and &cups
		replace status with "Shipped"
	case !emptynul(del_date)
		replace status with "Delivered"
	case !emptynul(truckloaddt)
		replace status with "Loaded"
	case !emptynul(staged)
		replace status with "Staged"
	case !emptynul(labeled)
		replace status with "Labeled"
	case !emptynul(picked)
		replace status with "Picked"
	otherwise
		replace status with "Pulled"
	endcase
endscan

locate

wait "Writing data to spreadsheet - Page "+transform(npgnum) window nowait noclear

ldata = .f.
if !eof()
	ldata = .t.

	nreccnt = reccount()
	if reccount() > 65528 &&excel's row limit
		nreccnt = 65528
		strmsg = "The record count for this account's style detail WIP exceeds excel's limit. Only the first 65,535 records will be displayed."
		if !lwebsite
			=messagebox(strmsg, 16, "")
		endif
	endif

	**removed pickedView, replaced with status per Juan - mvw 04/16/13
	*!*	store "ptNum, wo_num, consignee, cnee_ref, style, color, id, pack, ship_via, qtyFld, startView, " + ;
	*!*		"cancelView, status, calledView, apptView, appt_time, appt_num, " + ;
	*!*		iif(inlist(npgnum, 2, 5), "delView, ", "") + "spView" ;
	*!*		to cfieldlist

	if goffice="L"
		store "ptNum, wo_num, consignee, cnee_ref, soldto, style, color, id, DESCRIP, pack, ship_via, qtyFld, startView, " + ;
			"cancelView, status, calledView, apptView, appt_time, appt_num, " + ;
			iif(inlist(npgnum, 2, 5), "delView, ", "") + "spView" ;
			to cfieldlist
	else
		store "ptNum, wo_num, consignee, cnee_ref, style, color, id, DESCRIP, pack, ship_via, qtyFld, startView, " + ;
			"cancelView, status, calledView, apptView, appt_time, appt_num, " + ;
			iif(inlist(npgnum, 2, 5), "delView, ", "") + "spView" ;
			to cfieldlist
	endif
	
	store "h:\fox\"+substr(sys(2015),3,10)+".xls" to ctmpfile
	copy to &ctmpfile fields &cfieldlist type xl5

	otmpworkbook = oexcel.workbooks.open(ctmpfile)
	**start at row 2 (A2) to eliminate headers, nRecCnt+1 to account for header row
	*store iif(inlist(npgnum, 2, 5), "R", "Q") to crcol
	store iif(inlist(npgnum, 2, 5), "S", "R") to crcol
	otmpworkbook.activesheet.range("A2:"+crcol+alltrim(str(nreccnt+1))).copy()

	oworkbook.sheets[nPgNum].range("A4").value = "Account: "+alltrim(xacctname)+space(3)+"Office: "+alltrim(cofficename)
	oworkbook.sheets[nPgNum].range("A6").pastespecial()

	if lwebsite
		oworkbook.sheets[nPgNum].range("E5").value=xid1
		oworkbook.sheets[nPgNum].range("F5").value=xid2
		oworkbook.sheets[nPgNum].range("G5").value=xid3
	endif

	**next 2 lines used to avoid an excel warning msg stating that the clipboard is full...
	**  these 2 lines empty the clipboard automatically
	oexcel.cutcopymode = "False"
	oexcel.cutcopymode = "True"
	otmpworkbook.close()
	release otmpworkbook
endif

wait clear

use in csroutship

return ldata
*****
