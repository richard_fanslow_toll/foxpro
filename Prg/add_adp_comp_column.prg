LOCAL oExcel, oWorkbook, oWorksheet, lnRow, lcRow, llSaveAgain, lnFileNum

CLOSE DATABASES

USE C:\ACCRUALS_2007_FILES\ADP_COMPS IN 0 ALIAS ADP_COMPS


WAIT WINDOW NOWAIT "Opening Excel..."
oExcel = CREATEOBJECT("excel.application")
oWorkbook = oExcel.workbooks.OPEN("C:\ACCRUALS_2007_FILES\TUG_SICK_12-31-2007.xls")
*oWorkbook = oExcel.workbooks.OPEN("C:\ACCRUALS_2007_FILES\NONTUG_SICK_12-31-2007.xls")

oExcel.VISIBLE = .T.
oWorksheet = oWorkbook.Worksheets[1]

llSaveAgain = .T.

FOR lnRow = 3 TO 96
	lcRow = LTRIM(STR(lnRow))
	lnFileNum = INT(oWorksheet.RANGE("D" + lcRow).VALUE)
	SELECT ADP_COMPS
	LOCATE FOR FILE_NUM = lnFileNum
	IF FOUND() THEN
		oWorksheet.RANGE("K" + lcRow).VALUE = ADP_COMPS.adp_comp
		oWorksheet.RANGE("A" + lcRow,"K" + lcRow).Interior.ColorIndex = IIF(((lnRow % 2) = 1),15,0)
	ENDIF
ENDFOR

* save again
IF llSaveAgain THEN
	oWorkbook.SAVE()
ENDIF
oWorkbook.CLOSE()
oExcel.QUIT()

CLOSE DATABASES
