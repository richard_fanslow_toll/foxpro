**OUTGATE files to Cargowise
**
**UPDATE: Added processaing of DOCS RCVD, AVAILBLE & DELIVERED events (21C, 214A & 214D) from here as well, passed by xeditype parameter - mvw 10/24/16
**
parameter xwonum, xeditype

xoutpath="f:\ftpusers\edienterprise\out\archive\"

set date to ymd

**tables should be opened by calling prg
if !used("wolog")
	use f:\wo\wodata\wolog in 0
endif

do case
case !seek(xwonum,"wolog","wo_num")
	xerrormsg="WO NOT FOUND"
	return .f.
case wolog.type#"O"
	xerrormsg="NO FILE REQUIRED FOR TYPE '"+alltrim(wolog.wotype)+"'"
	return .f.

case date()<={10/27/16} and xeditype='214D' and empty(wolog.pickedup)
	xerrormsg="BAD TRIGGER"
	return .f.
endcase

lcStr1 = Filetostr("f:\edienterprise\xmltemplates\outgate.xml")
lcStr1 = Strtran(lcStr1,"<CONTAINER>",Alltrim(wolog.container))
lcStr1 = Strtran(lcStr1,"<MBOL>",Alltrim(wolog.mblnum))

**Event type codes: OUTGATE "GOU", DOCS RCVD "BKD", AVAILABLE "CAV", DELIVERED "DLV"
xtype=iif(xeditype="214P","GOU",iif(xeditype="214C","BKD",iif(xeditype="214A","CAV","DLV")))
lcStr1 = Strtran(lcStr1,"<214TYPE>",xtype)

do case
case xtype="GOU"
	lcStr1 = Strtran(lcStr1,"<DATE>",Strtran(dtoc(wolog.pickedup),"/","-"))
	lcStr1 = Strtran(lcStr1,"<TYPECODE>","JobContainer.JC_FCLWharfGateOut")
case xtype="BKD"
	lcStr1 = Strtran(lcStr1,"<DATE>",Strtran(dtoc(wolog.wo_date),"/","-"))
	lcStr1 = Strtran(lcStr1,"<TYPECODE>","JobContainer.JC_ArrivalCartageAdvised")
case xtype="CAV"
	lcStr1 = Strtran(lcStr1,"<DATE>",Strtran(dtoc(wolog.availdt),"/","-"))
	lcStr1 = Strtran(lcStr1,"<TYPECODE>","JobContainer.JC_FCLAvailable")
otherwise
	lcStr1 = Strtran(lcStr1,"<DATE>",Strtran(dtoc(wolog.delivered),"/","-"))
	lcStr1 = Strtran(lcStr1,"<TYPECODE>","JobContainer.JC_ArrivalCartageComplete")
endcase

* TGFSCS_UACU8451414_GOU_YYYYMMDDHHSSMM.XML.

*xoutfile=xoutpath+"TGFSCS_"+Alltrim(wolog.container)+"_GOU_"+Ttoc(Datetime(),1)+".xml"
xoutfile=xoutpath+"TGFSCS_"+Alltrim(wolog.container)+"_"+xtype+"_"+Ttoc(Datetime(),1)+".xml"

Strtofile(lcstr1,xoutfile)
