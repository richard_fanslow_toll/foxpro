CLOSE DATABASES ALL

DO M:\DEV\PRG\_SETVARS WITH .T.
lTesting = .F.
_SCREEN.WINDOWSTATE=IIF(lTesting,2,1)

SELECT 0
USE "f:\edirouting\ftpsetup"
REPLACE chkbusy WITH .T. FOR transfer = "997-TJX-RENAME" IN ftpsetup

WAIT WINDOW "Now renaming TJX 997 output files" NOWAIT
ASSERT .F.

CD "F:\ftpusers\TJMAX\997RENAME\"
cFilename = "F:\ftpusers\TJMAX\997RENAME\997out.edi"
cOutname = ("997TJX"+TTOC(DATETIME(),1)+".edi")
IF !FILE(cFilename)
	WAIT WINDOW "No TJX 997 files to output" TIMEOUT 1
	CLOSE DATA ALL
	return
ELSE
	WAIT WINDOW "TJX 997 file processing" TIMEOUT 1
ENDIF
cOutfolder = IIF(lTesting,"f:\ftpusers\tjmax\testout\","F:\FTPUSERS\TJMAX\OUT\")

cOutfile = (cOutfolder+cOutname)
cArchivefile = ("F:\FTPUSERS\TJMAX\997rename\archive\"+cOutname)

cString = FILETOSTR(cFilename)
STRTOFILE(cString,cArchivefile)
STRTOFILE(cString,cOutfile)

IF FILE(cFilename)
	DELETE FILE [&cFilename]
ENDIF

SELECT ftpsetup
REPLACE chkbusy WITH .F. FOR transfer = "997-TJX-RENAME" IN ftpsetup

CLOSE DATABASES ALL
RETURN
