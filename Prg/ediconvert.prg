PARAMETERS cFilename

runack("EDI_CONVERTER")
DO m:\dev\prg\_setvars WITH .t.
PUBLIC cDirNow
cDirNow = ADDBS(JUSTPATH(cFilename))
IF EMPTY(cDirNow)
cDirNow = ADDBS(SYS(5)+SYS(2003))
ENDIF
WAIT WINDOW cDirNow TIMEOUT 2
IF VARTYPE(cFilename)="L"
	WAIT WINDOW "No filename provided" TIMEOUT 1
	RETURN
ELSE
	WAIT WINDOW "Opening filename "+cFilename TIMEOUT 1
ENDIF
cFilename2 = JUSTFNAME(cFilename)

SET SAFETY OFF
cStringIn = FILETOSTR(cFilename)
cFileOut = ("c:\tempfox\"+cFilename2)
cFieldDelim = SUBSTR(cStringIn,4,1)
IF cFieldDelim <> "*"
	cStringIn = STRTRAN(cStringIn,cFieldDelim,"*")
ENDIF
cFieldDelim = "*"

cSegDelim = ""
FOR i = 1 TO LEN(cStringIn)
	IF INLIST(SUBSTR(cStringIn,i,1),">","\")
		cSegDelim = SUBSTR(cStringIn,(i+1),1)
		EXIT
	ENDIF
ENDFOR
cStringIn = STRTRAN(cStringIn,cSegDelim,CHR(13))
STRTOFILE(cStringIn,cFileOut)

#IF 0
	DO m:\dev\prg\createx856a
	APPEND FROM &cFileOut TYPE DELIMITED WITH CHARACTER &cFieldDelim
	BROWSE
	RETURN
#ENDIF

cRunfile = IIF(FILE("d:\program files\textpad 4\textpad.exe"),"d:\program files\textpad 4\textpad.exe","c:\program files\textpad 4\textpad.exe")
! /N &cRunfile -q &cFileOut
WAIT WINDOW "" TIMEOUT 2
SET STEP ON 
CLOSE DATA ALL
DELETE FILE [&cFileOut]
IF FILE(cDirNow+"foxuser.dbf")
*	DELETE FILE (ADDBS(cDirNow)+"foxuser.*")
ENDIF

