PARAMETERS tcWhichType

* written by PG
* modified by MB 9/11/2017

* 2 exes were being made:
* F:\3PL\bbb_210_los.exe
* F:\3PL\bbb_210_sdg.exe

* 9/11/2017 MB - changed it to one exe with a passed parameter:

* Build EXE as =====> F:\3PL\bbb_210.exe

* 9/19/2017 MB - streamlined test mode processing so that glBBB_210_Testing controls test mode in the critical called programs.
* 9/19/2017 MB - wrote code that bypassed calls to dygenpk(), which was often resulting in 'record in use' errors.
*
* 3/1/2018 MB updated rates per Brian F. : 0.280-->0.288, 0.320-->0.329, 0.450-->0.463
*
* 4/24/20188 MB: changed fmiint.com emails to Toll emails.


utilsetup("BBB_210")
*CLOSE DATA ALL

tmessage = ''

*DO _setvars

SET DATE AMERICAN
*Set Resource off
SET DELETED ON
SET ENGINEBEHAVIOR 70
SET EXCLUSIVE OFF
SET EXACT OFF
SET SAFETY OFF
SET MULTILOCKS ON
SET STATUS BAR ON
SET REPROCESS TO 10 SECONDS  && so RLOCK()s in code will retry for xx seconds; to implement an attempt to prevent 'record in use' errors in DYGENPK calls to production ARGENPK.
*SET TALK OFF
*SYS(3054,0)

LOCAL lc210OutPath, lc210PrePath, lnNumFiles, lnCurrentFile, lc210PreArchivePath, lcSourceFile, lcArchivedFile, lcTargetFile, llArchived
PRIVATE m.c210FileName

m.c210FileName = ''  && will assign this in called bb_210.prg


PUBLIC lcInvoiceNum,cISA_Num
PUBLIC nSTCount,nSegCtr
PUBLIC c_CntrlNum,c_GrpCntrlNum
PUBLIC ARRAY acsid[500]

PUBLIC glBBB_210_Testing, gcUserMessage, glBBB_210_Reload

gcUserMessage = ''

******************************************************************
******************************************************************
glBBB_210_Testing = .F.   && production
*glBBB_210_Testing = .T.		&& development
glBBB_210_Reload = .F.
*glBBB_210_Reload = .T.   && reinvoicing
******************************************************************
******************************************************************

nSTCount=0
nSegCtr =0
c_CntrlNum =""
c_GrpCntrlNum=""

lcInvoiceNum =""
cISA_Num =""

zuserid=UPPER(GETENV("USERNAME"))

*lltesting        = .F.
lltesting = glBBB_210_Testing

SET SAFETY OFF
SET EXCLUSIVE OFF

********************************************************************
* Added block 09/11/2017 MB
IF EMPTY(tcWhichType) THEN
	WAIT WINDOW "====> Missing source city parameter...exiting" TIMEOUT 5
	CLOSE DATA ALL
	schedupdate()
	RETURN
ENDIF

whichType = UPPER(ALLTRIM(tcWhichType))

DO CASE
	CASE whichType = "SDG"
		IF lltesting
			lcpath = 'F:\FTPUSERS\BEDBATH\TESTDATAIN\'
			lcarchivepath = 'F:\FTPUSERS\BEDBATH\TESTDATAIN\ARCHIVE\'
		ELSE
			lcpath = 'F:\FTPUSERS\BEDBATH\DATAINSDG\'
			lcarchivepath = 'F:\FTPUSERS\BEDBATH\DATAINSDG\ARCHIVE\'
		ENDIF
	CASE whichType = "LOS"
		IF lltesting
			lcpath = 'F:\FTPUSERS\BEDBATH\TESTDATAIN\'
			lcarchivepath = 'F:\FTPUSERS\BEDBATH\TESTDATAIN\ARCHIVE\'
		ELSE
			lcpath = 'F:\FTPUSERS\BEDBATH\DATAIN\'
			lcarchivepath = 'F:\FTPUSERS\BEDBATH\DATAIN\ARCHIVE\'

			*!*					lcpath = 'F:\FTPUSERS\BEDBATH\SDATAIN\'
			*!*					lcarchivepath = 'F:\FTPUSERS\BEDBATH\SDATAIN\ARCHIVE\'
		ENDIF
	OTHERWISE
		WAIT WINDOW "====> Unexpected source city parameter...exiting" TIMEOUT 5
		CLOSE DATA ALL
		schedupdate()
		RETURN
ENDCASE
********************************************************************


USE F:\3PL\DATA\bbb_invoicedata IN 0


*!*  useca("bbtn","ar")
*!*  useca("bbin","ar")

**  csr to pass to the invoice prg
CREATE CURSOR curInvoice( ;
	transdate d, ;
	lcapex l, ;
	amount N(15,2),;
	qty  N(15,2),;
	rate  N(6,2),;
	weight N(15,2),;
	trailer CHAR(20),;
	filename CHAR(30),;
	invnum CHAR(12),;
	manifest CHAR(30),;
	shipname CHAR(30),;
	addr1 CHAR(30),;
	addr2 CHAR(30),;
	city CHAR(30),;
	state CHAR(2),;
	zip CHAR(10),;
	whse CHAR(6),;
	isa  CHAR(15),;
	TYPE CHAR(4))

SELECT * FROM curInvoice WHERE .F. INTO CURSOR curEmailInvoice READWRITE

**  csr to pass to the email alert
CREATE CURSOR csrUpload( ;
	TYPE CHAR(4),;
	filename CHAR(30), ;
	ref1 CHAR(30), ;
	ref2 CHAR(30), ;
	qty   CHAR(10),;
	trailer CHAR(20),;
	whse CHAR(6),;
	closedt DATE )

*SET STEP ON

IF lltesting THEN
	USE M:\DEV\ardata\invoice ALIAS vinvoice SHARED IN 0
	USE M:\DEV\ardata\invdet ALIAS vinvdet SHARED IN 0
ELSE
	* use sql invoice tables; per Darren the code below allows us to index vinvdet, which is needed by LOADPLAT.prg which is called from bb_creatinv.prg
	OPEN DATABASE F:\watusi\ardata\arsql
	USERV("vinvoice","arsql")
	USERV("vinvdet","arsql")
	CURSORSETPROP("buffering",3,"vinvdet")
	SELECT vinvdet
	INDEX ON invdetid TAG invdetid
	SET ORDER TO
	CURSORSETPROP("BUFFERING",5,"VINVDET")
	*!*				* use foxpro invoice tables
	*!*				USE F:\watusi\ardata\invoice ALIAS vinvoice SHARED IN 0
	*!*				USE F:\watusi\ardata\invdet ALIAS vinvdet SHARED IN 0

ENDIF && lltesting


llLoadSql          = .T.
llUpdateSQL        = .T.  && set this only when creating a 210 or creating an invoice
llCreateInvoice    = .T.
llUpdateInvoice    = .T.  && when reloading and you want to get the invoice and update the data fields invnum and invdt
llCreate210        = .T.
llCreateCurInvoice = .T.

llDeletefile     = .T.

***********************************************************************************
llreload = glBBB_210_Reload   && either take the file name from a dbf list or an ADIR command

*SET STEP ON

IF llreload = .F.
	len1 = ADIR(ary1,lcpath+"*.*")
	*set step On
	IF len1 = 0
		WAIT WINDOW "No files found...exiting" TIMEOUT 2
		CLOSE DATA ALL
		schedupdate()
		*ON ERROR
		RETURN
	ENDIF
ELSE

	*************************************************************************************************************************************
	* change source file and source file archived to the reinvoice paths - the source folder should contain the files to be reprocessed
	*
	* !!!!!==> don't mix LOS and SDG source files in one run, i.e. in BBBINV_RELOAD table - use just one type and pass the appropriate parameter to the program.
	*
	*************************************************************************************************************************************
	lcpath = 'F:\FTPUSERS\BEDBATH\REINVOICES\'
	lcarchivepath = 'F:\FTPUSERS\BEDBATH\REINVOICES\ARCHIVED\'

	USE F:\3PL\DATA\BBBINV_RELOAD ALIAS BBBINV_RELOAD SHARED IN 0

	DIMENSION ary1(800,1)
	i=0

	SELECT BBBINV_RELOAD
	SCAN FOR PROCESS AND (FILESIZE > 0)
		i=i+1
		ary1[i,1]= BBBINV_RELOAD.filename
		len1 = i
	ENDSCAN

ENDIF

TRY

	lnNumRecs = 0
	***************************************************************************************
	FOR xxx1 = 1 TO len1

		cfilename = ALLTRIM(ary1[xxx1,1])
		xfile = UPPER(lcpath+cfilename)
		carchivefile = (lcarchivepath+cfilename)
		cloadfile = (lcpath+cfilename)
		tfile=ALLTRIM(ary1[xxx1,1])
		tfilepath=lcpath+ALLTRIM(ary1[xxx1,1])

		IF NOT llreload THEN
			* DELETE ZERO BYTE FILES
			IF ary1[xxx1,2]=0
				archivefile=lcarchivepath+cfilename
				COPY FILE [&xfile] TO [&archivefile]
				WAIT WINDOW "0 byte File Moved to Archive: "+xfile TIMEOUT 1
				DELETE FILE  [&xfile]
				WAIT WINDOW "0 byte File Deleted: "+xfile NOWAIT
				LOOP
			ENDIF
		ENDIF

		WAIT WINDOW "Now importing File: "+xfile NOWAIT

		DO bb_data_import WITH cloadfile  && parse out and load the data

		DO CASE

				*************  HERE WE PROCESS THE TN FILES************* ************************

			CASE "TN"$UPPER(cloadfile) && Crossdock and Store Manifest files loaded here
				useca("bbtn","ar")
				SELECT bbbman, SUM(VAL(weight)) AS totweight,COUNT(1) AS ctnqty,pcsman,filename FROM csrbbbtn GROUP BY filename INTO CURSOR temptn
				SELECT curInvoice
				ZAP
				SELECT csrbbbtn
				GO TOP

				DO CASE
					CASE csrbbbtn.shiptype="B"
						lcShiptype = "B"
						*lnRate = 0.32
						lnRate = 0.329  && 3/1/2018 MB
						lnAmount =  temptn.ctnqty*lnRate
					CASE INLIST(csrbbbtn.shiptype,"S","P")
						*lnRate = 0.45
						lnRate = 0.463 && 3/1/2018 MB
						lcShiptype = "S"
						lnAmount =  temptn.ctnqty*lnRate
					OTHERWISE
						*=MESSAGEBOX(" Invalid ShipType in Filename: "+xfile,0,"BedBath Data Processing")
						WAIT WINDOW " Invalid ShipType in Filename: "+xfile TIMEOUT 5

						lnRate = 0.0
						lcShiptype = "X"
						EXIT
				ENDCASE

				SELECT csrbbbtn
				GO TOP
				SELECT temptn
				INSERT INTO curInvoice(transdate,lcapex,amount,TYPE,manifest,trailer,qty,rate,weight,filename,shipname,addr1,addr2,city,state,zip,whse) VALUES;
					(DATE(),.T.,lnAmount,"TN"+ALLTRIM(lcShiptype),temptn.bbbman,temptn.pcsman,RECCOUNT("csrbbbtn"),lnRate,temptn.totweight,temptn.filename,;
					csrbbbtn.shipname,csrbbbtn.addr1,csrbbbtn.addr2,csrbbbtn.city,csrbbbtn.state,csrbbbtn.zip,ALLTRIM(whichType))

				SELECT curInvoice
				SCATTER MEMVAR
				INSERT INTO bbb_invoicedata FROM MEMVAR

				*!*		IF lltesting THEN
				*!*			SET STEP ON
				*!*		ENDIF
				*!*					IF llreload THEN
				*!*						SELECT BBBINV_RELOAD
				*!*						LOCATE FOR alltrim(filename) == ALLTRIM(cfilename)
				*!*						IF FOUND() THEN
				*!*							REPLACE BBBINV_RELOAD.NEWBALANCE WITH lnAmount IN BBBINV_RELOAD
				*!*						ENDIF
				*!*					ENDIF

				IF llCreateInvoice = .T.
					bb_CreateInv()
					SELECT curInvoice
					REPLACE invnum WITH lcInvoiceNum IN curInvoice
					SELECT bbb_invoicedata
					REPLACE invnum WITH lcInvoiceNum IN bbb_invoicedata
					REPLACE invdt WITH DATE() IN bbb_invoicedata
				ENDIF

				IF llCreate210 = .T.
					IF EMPTY(lcInvoiceNum)
						SELECT vinvoice
						LOCATE FOR accountid=6695 AND JUSTFNAME(xfile)$vinvoice.HEADER
						IF FOUND()
							lcInvoiceNum=ALLTRIM(vinvoice.invnum)
							REPLACE curInvoice.invnum WITH lcInvoiceNum IN curInvoice

							IF llreload THEN
								SELECT BBBINV_RELOAD
								LOCATE FOR ALLTRIM(filename) == ALLTRIM(cfilename)
								IF FOUND() THEN
									REPLACE BBBINV_RELOAD.NEWINVNUM WITH lcInvoiceNum, BBBINV_RELOAD.NEWBALANCE WITH lnAmount IN BBBINV_RELOAD
								ENDIF
							ENDIF

						ELSE
							*=MESSAGEBOX(" No Invoice set for 210 creation Filename: "+xfile,0,"BedBath 210 creation process")

							IF llreload THEN
								SELECT BBBINV_RELOAD
								LOCATE FOR ALLTRIM(filename) == ALLTRIM(cfilename)
								IF FOUND() THEN
									REPLACE BBBINV_RELOAD.COMMENT WITH "No Invoice set for 210 creation" IN BBBINV_RELOAD
								ENDIF
							ENDIF

							WAIT WINDOW " No Invoice set for 210 creation Filename: "+xfile TIMEOUT 5
							EXIT
						ENDIF
					ENDIF

					SELECT curInvoice
					GO TOP

					DO CASE
						CASE xxx1=1 AND len1=1  && there is only 1 file being processed; open and close output 210 in first call
							bb_210(.T.,.T.)
						CASE xxx1=1  			&& there is more than one file and we are on the first one; just open output 210
							bb_210(.T.,.F.)
						CASE xxx1=len1  		&& there is more than one file and we are on the last one; just close output 210
							bb_210(.F.,.T.)
						OTHERWISE  				&& there is more than one file and we are not on the first or last one; do no opening or closing of output 210
							bb_210(.F.,.F.)
					ENDCASE
				ENDIF

				SELECT csrbbbtn
				REPLACE ALL filename WITH UPPER(filename)
				GO TOP
				SCAN
					SCATTER MEMVAR
					IF llCreateInvoice = .T.
						m.invnum = ALLTRIM(lcInvoiceNum)
						m.invdt = DATE()
					ENDIF
					IF llCreate210 = .T.
						m.isa   = cISA_Num
						m.isadt = DATE()
						m.whse  = ALLTRIM(whichType)
					ENDIF
					insertinto("bbtn","ar",.T.)
				ENDSCAN

				IF llCreateInvoice = .T.
					SELECT curInvoice
					REPLACE isa WITH cISA_Num IN curInvoice
					SELECT bbb_invoicedata
					REPLACE isa WITH cISA_Num IN bbb_invoicedata
					REPLACE isadt WITH DATE() IN bbb_invoicedata

				ENDIF

				IF llLoadSql = .T.

					*!*        If Reccount("csrbbbtn") = 1
					*!*          =Messagebox("Upload count error: "+xfile+"  Check csrbbtn qty = 1 "+ Transform(Reccount("csrbbbtn")),0,"BedBath 210 creation Process")
					*!*        Endif

					xsqlexec("select Count(1) as xqty from bbtn where filename = '"+JUSTFNAME(xfile)+"'","xbbtn",,"ar")
					lnNumRecs = xbbtn.xqty

					IF lnNumRecs >= 1
						xsqlexec("delete from bbtn where filename = '"+JUSTFNAME(xfile)+"'","xbbtn",,"ar")
					ENDIF

					xsqlexec("select Count(1) as xqty from bbtn where filename = '"+JUSTFNAME(xfile)+"'","xbbtn",,"ar")
					lnNumRecs = xbbtn.xqty

					IF lnNumRecs =0
						SELECT bbtn
						REPLACE ALL uploaddt WITH DATE()
						REPLACE ALL uploadtm WITH DATETIME()
						REPLACE ALL whse WITH ALLTRIM(whichType)
						IF glBBB_210_Testing THEN
							*SELECT bbtn
							*BROWSE
						ELSE
							tu("bbtn")
						ENDIF
					ELSE
						*=MESSAGEBOX("File was already uploaded: "+xfile,0,"BedBath 210 creation process")
						WAIT WINDOW "File was already uploaded: "+xfile TIMEOUT 5
					ENDIF

					xsqlexec("select Count(1) as xqtyuploaded from bbtn where filename = '"+JUSTFNAME(xfile)+"'","xbbtn",,"ar")
					IF RECCOUNT("csrbbbtn") != xbbtn.xqtyuploaded
						*=MESSAGEBOX("Upload count error: CSR Qty= "+ TRANSFORM(RECCOUNT("csrbbbtn"))+" Qty in Sql = "+ TRANSFORM(xbbtn.xqtyuploaded),0,"BedBath 210 creation process")
						WAIT WINDOW "Upload count error: CSR Qty= "+ TRANSFORM(RECCOUNT("csrbbbtn"))+" Qty in Sql = "+ TRANSFORM(xbbtn.xqtyuploaded) TIMEOUT 5
					ENDIF
				ENDIF

				IF llUpdateSQL = .T. AND llCreate210 = .T.
					xsqlexec([update bbtn set isa = ']+ALLTRIM(cISA_Num)+"' where filename = '"+JUSTFNAME(xfile)+"'","xbbtn",,"ar")
					xsqlexec([update bbtn set isadt = ']+ALLTRIM(DTOC(DATE()))+"' where filename = '"+JUSTFNAME(xfile)+"'","xbbtn",,"ar")
				ENDIF

				IF llUpdateSQL = .T. AND llCreateInvoice = .T.
					xsqlexec([update bbtn set invnum = ']+ALLTRIM(lcInvoiceNum)+"' where filename = '"+JUSTFNAME(xfile)+"'","xbbtn",,"ar")
					xsqlexec([update bbtn set invdt = ']+ALLTRIM(DTOC(DATE()))+"' where filename = '"+JUSTFNAME(xfile)+"'","xbbtn",,"ar")
				ENDIF

				IF llUpdateInvoice = .T.
					SELECT vinvoice
					LOCATE FOR accountid=6695 AND JUSTFNAME(xfile)$vinvoice.HEADER
					IF FOUND()
						xsqlexec([update bbtn set invnum = ']+ALLTRIM(vinvoice.invnum)+"' where filename = '"+JUSTFNAME(xfile)+"'","xbbtn",,"ar")
						xsqlexec([update bbtn set invdt = ']+ALLTRIM(DTOC(vinvoice.invdt))+"' where filename = '"+JUSTFNAME(xfile)+"'","xbbtn",,"ar")
					ELSE
						xsqlexec([update bbtn set invnum = 'UNK' where filename = ']+JUSTFNAME(xfile)+"'","xbbtn",,"ar")
					ENDIF
				ENDIF


				WAIT WINDOW "Data loaded into SQL from file: "+xfile NOWAIT
				USE IN "bbtn"
				SELECT csrbbbtn
				GO TOP
				IF lnNumRecs = 0 OR llCreate210
					INSERT INTO csrUpload (TYPE,closedt,filename,trailer,ref1,ref2,qty,whse) VALUES ("TN-"+ALLTRIM(lcShiptype),csrbbbtn.closedt,cfilename,"            ",PADR(ALLTRIM(csrbbbtn.pcsman),30," "),;
						PADR(ALLTRIM(csrbbbtn.bbbman),30," "),TRANSFORM(RECCOUNT("csrbbbtn")),ALLTRIM(whichType))
				ENDIF

				*************  HERE WE PROCESS THE IN FILES************* ************************
			CASE "IN"$UPPER(cloadfile)  && Inbound files loaded here
				useca("bbin","ar")
				SELECT trailer,bol,pro,SUM(VAL(weight)) AS totweight,COUNT(1) AS ctnqty,filename FROM csrbbbin GROUP BY filename INTO CURSOR tempin
				SELECT curInvoice
				ZAP
				SELECT csrbbbtn
				GO TOP

				*lnRate = 0.28
				lnRate = 0.288  && 3/1/2018 MB
				lnAmount =  tempin.ctnqty*lnRate

				SELECT csrbbbin
				GO TOP
				SELECT tempin
				INSERT INTO curInvoice(transdate,lcapex,amount,TYPE,trailer,manifest,qty,rate,weight,filename,shipname,addr1,addr2,city,state,zip,whse) VALUES;
					(DATE(),.T.,lnAmount,"IN",tempin.trailer,tempin.bol,RECCOUNT("csrbbbin"),lnRate,tempin.totweight,tempin.filename,;
					csrbbbin.shipname,csrbbbin.addr1,csrbbbin.addr2,csrbbbin.city,csrbbbin.state,csrbbbin.zip,ALLTRIM(whichType))

				SELECT curInvoice
				SCATTER MEMVAR
				INSERT INTO bbb_invoicedata FROM MEMVAR


				IF llCreateInvoice = .T.
					bb_CreateInv()
					SELECT curInvoice
					REPLACE invnum WITH lcInvoiceNum IN curInvoice
					SELECT bbb_invoicedata
					REPLACE invnum WITH lcInvoiceNum IN bbb_invoicedata
					REPLACE invdt WITH DATE() IN bbb_invoicedata
				ENDIF

				IF llCreate210 = .T.
					IF EMPTY(lcInvoiceNum)
						SELECT vinvoice
						LOCATE FOR accountid=6695 AND JUSTFNAME(xfile)$vinvoice.HEADER
						IF FOUND()
							lcInvoiceNum=ALLTRIM(vinvoice.invnum)
							REPLACE curInvoice.invnum WITH lcInvoiceNum IN curInvoice

							IF llreload THEN
								SELECT BBBINV_RELOAD
								LOCATE FOR ALLTRIM(filename) == ALLTRIM(cfilename)
								IF FOUND() THEN
									REPLACE BBBINV_RELOAD.NEWINVNUM WITH lcInvoiceNum, BBBINV_RELOAD.NEWBALANCE WITH lnAmount IN BBBINV_RELOAD
								ENDIF
							ENDIF

						ELSE
							*=MESSAGEBOX(" No Invoice set for 210 creation ",0,"BedBath 210 creation process")

							IF llreload THEN
								SELECT BBBINV_RELOAD
								LOCATE FOR ALLTRIM(filename) == ALLTRIM(cfilename)
								IF FOUND() THEN
									REPLACE BBBINV_RELOAD.COMMENT WITH "No Invoice set for 210 creation" IN BBBINV_RELOAD
								ENDIF
							ENDIF

							WAIT WINDOW " No Invoice set for 210 creation " TIMEOUT 5
							EXIT
						ENDIF
					ENDIF

					SELECT curInvoice
					GO TOP

					DO CASE
						CASE xxx1=1 AND len1=1  && there is only 1 file being processed; open and close output 210 in first call
							bb_210(.T.,.T.)
						CASE xxx1=1  			&& there is more than one file and we are on the first one; just open output 210
							bb_210(.T.,.F.)
						CASE xxx1=len1  		&& there is more than one file and we are on the last one; just close output 210
							bb_210(.F.,.T.)
						OTHERWISE  				&& there is more than one file and we are not on the first or last one; do no opening or closing of output 210
							bb_210(.F.,.F.)
					ENDCASE
				ENDIF

				SELECT csrbbbin
				REPLACE ALL filename WITH UPPER(filename)
				GO TOP

				SCAN
					SCATTER MEMVAR
					IF llCreateInvoice = .T.
						m.invnum = ALLTRIM(lcInvoiceNum)
						m.invdt = DATE()
					ENDIF
					IF llCreate210 = .T.
						m.isa   = cISA_Num
						m.isadt = DATE()
						m.whse  = ALLTRIM(whichType)
					ENDIF
					insertinto("bbin","ar",.T.)
				ENDSCAN

				IF llCreateInvoice = .T.
					SELECT curInvoice
					REPLACE isa WITH cISA_Num IN curInvoice
					SELECT bbb_invoicedata
					REPLACE isa WITH cISA_Num IN bbb_invoicedata
					REPLACE isadt WITH DATE() IN bbb_invoicedata
				ENDIF

				IF llLoadSql = .T.

					*!*        If Reccount("csrbbbin") = 1
					*!*          =Messagebox("Upload count error: "+xfile+"  Check csrbbin qty = 1 "+ Transform(Reccount("csrbbbin")),0,"BedBath 210 creation Process")
					*!*        Endif
					xsqlexec("select Count(1) as xqty from bbin where filename = '"+JUSTFNAME(xfile)+"'","xbbin",,"ar")
					lnNumRecs = xbbin.xqty

					IF lnNumRecs >= 1
						xsqlexec("delete from bbin where filename = '"+JUSTFNAME(xfile)+"'","xbbin",,"ar")
					ENDIF

					xsqlexec("select Count(1) as xqty from bbin where filename = '"+JUSTFNAME(xfile)+"'","xbbin",,"ar")
					lnNumRecs = xbbin.xqty

					IF lnNumRecs =0
						SELECT bbin
						REPLACE ALL uploaddt WITH DATE()
						REPLACE ALL uploadtm WITH DATETIME()
						REPLACE ALL whse WITH ALLTRIM(whichType)
						IF glBBB_210_Testing THEN
							*SELECT bbin
							*BROWSE
						ELSE
							tu("bbin")
						ENDIF
					ELSE
						*=MESSAGEBOX("File was already uploaded: "+xfile,0,"BedBath 210 creation process")
						WAIT WINDOW "File was already uploaded: "+xfile TIMEOUT 5
					ENDIF

					xsqlexec("select Count(1) as xqtyuploaded from bbin where filename = '"+JUSTFNAME(xfile)+"'","xbbin",,"ar")
					IF RECCOUNT("csrbbbin") != xbbin.xqtyuploaded
						*=MESSAGEBOX("Upload count error: CSR Qty= "+TRANSFORM(RECCOUNT("csrbbbin"))+" Qty in Sql = "+ TRANSFORM(xbbin.xqtyuploaded),0,"BedBath 210 creation process")
						WAIT WINDOW "Upload count error: CSR Qty= "+TRANSFORM(RECCOUNT("csrbbbin"))+" Qty in Sql = "+ TRANSFORM(xbbin.xqtyuploaded) TIMEOUT 5
					ENDIF
				ENDIF

				IF llUpdateSQL = .T. AND llCreate210 = .T.
					xsqlexec([update bbin set isa = ']+ALLTRIM(cISA_Num)+"' where filename = '"+JUSTFNAME(xfile)+"'","xbbin",,"ar")
					xsqlexec([update bbin set isadt = ']+ALLTRIM(DTOC(DATE()))+"' where filename = '"+JUSTFNAME(xfile)+"'","xbbin",,"ar")
				ENDIF

				IF llUpdateSQL = .T. AND llCreateInvoice = .T.
					xsqlexec([update bbin set invnum = ']+ALLTRIM(lcInvoiceNum)+"' where filename = '"+JUSTFNAME(xfile)+"'","xbbin",,"ar")
					xsqlexec([update bbin set invdt = ']+ALLTRIM(DTOC(DATE()))+"' where filename = '"+JUSTFNAME(xfile)+"'","xbbin",,"ar")
				ENDIF

				IF llUpdateInvoice = .T.
					SELECT vinvoice
					LOCATE FOR accountid=6695 AND JUSTFNAME(xfile)$vinvoice.HEADER
					IF FOUND()
						xsqlexec([update bbin set invnum = ']+ALLTRIM(vinvoice.invnum)+"' where filename = '"+JUSTFNAME(xfile)+"'","xbbin",,"ar")
						xsqlexec([update bbin set invdt = ']+ALLTRIM(DTOC(vinvoice.invdt))+"' where filename = '"+JUSTFNAME(xfile)+"'","xbbin",,"ar")
					ELSE
						xsqlexec([update bbin set invnum = 'UNK' where filename = ']+JUSTFNAME(xfile)+"'","xbbin",,"ar")
					ENDIF
				ENDIF

				WAIT WINDOW "Data loaded into SQ from file: "+xfile TIMEOUT 1
				USE IN "bbin"
				SELECT csrbbbin
				GO TOP
				IF lnNumRecs = 0 OR llCreate210
					INSERT INTO csrUpload (TYPE,closedt,filename,trailer,ref1,ref2,qty,whse) VALUES ("IN  ",csrbbbin.closedt,cfilename,csrbbbin.trailer,PADR(ALLTRIM(csrbbbin.bol),30," "),;
						PADR(ALLTRIM(csrbbbin.pro),30," "),TRANSFORM(RECCOUNT("csrbbbin")),ALLTRIM(whichType))
				ENDIF
		ENDCASE

		archivefile=lcarchivepath+cfilename
		COPY FILE [&xfile] TO [&archivefile]
		WAIT WINDOW "File Moved to Archive: "+xfile TIMEOUT 1
		IF llDeletefile = .T.
			DELETE FILE  [&xfile]
		ENDIF
		WAIT WINDOW "File Deleted: "+xfile NOWAIT

		SELECT curInvoice
		SCATTER MEMVAR
		SELECT curEmailInvoice
		APPEND BLANK
		GATHER MEMVAR
		REPLACE invnum WITH lcInvoiceNum IN curEmailInvoice
		lcInvoiceNum = ""

	ENDFOR  && do next xxx1


	* We have processed all files
	* tableupate SQL invoice tables
	* do INVDET first, and only do INVOICE if it succeeds

	IF tu("vinvdet") THEN
		tmessage = tmessage + '==> SUCCESS: TU(vinvdet) returned .T.!' + CHR(13)
		IF tu("vinvoice") THEN
			tmessage = tmessage + '==> SUCCESS: TU(vinvoice) returned .T.!' + CHR(13)
		ELSE
			* ERROR on table update
			tmessage = tmessage + '=====> ERROR: TU(vinvoice) returned .F.!' + CHR(13)
			=AERROR(UPCERR)
			IF TYPE("UPCERR")#"U" THEN
				tmessage = tmessage + '=====> Error Description = ' + UPCERR[1,2] + CHR(13)
			ENDIF
		ENDIF
	ELSE
		* ERROR on table update
		tmessage = tmessage + '=====> ERROR: TU(vinvdet) returned .F.!' + CHR(13)
		=AERROR(UPCERR)
		IF TYPE("UPCERR")#"U" THEN
			tmessage = tmessage + '=====> Error Description = ' + UPCERR[1,2] + CHR(13)
		ENDIF
	ENDIF


	*!*		IF tu("vinvoice") THEN
	*!*			tmessage = tmessage + '==> SUCCESS: TU(vinvoice) returned .T.!' + CHR(13)
	*!*		ELSE
	*!*			* ERROR on table update
	*!*			tmessage = tmessage + '=====> ERROR: TU(vinvoice) returned .F.!' + CHR(13)
	*!*			=AERROR(UPCERR)
	*!*			IF TYPE("UPCERR")#"U" THEN
	*!*				tmessage = tmessage + '=====> Error Description = ' + UPCERR[1,2] + CHR(13)
	*!*			ENDIF
	*!*		ENDIF
	*!*		IF tu("vinvdet") THEN
	*!*			tmessage = tmessage + '==> SUCCESS: TU(vinvdet) returned .T.!' + CHR(13)
	*!*		ELSE
	*!*			* ERROR on table update
	*!*			tmessage = tmessage + '=====> ERROR: TU(vinvdet) returned .F.!' + CHR(13)
	*!*			=AERROR(UPCERR)
	*!*			IF TYPE("UPCERR")#"U" THEN
	*!*				tmessage = tmessage + '=====> Error Description = ' + UPCERR[1,2] + CHR(13)
	*!*			ENDIF
	*!*		ENDIF


	*trigger the sftp job FOR THE 210s
	INSERT INTO F:\edirouting\sftpjobs (jobname, USERID) VALUES ("PUT_210_TO_BERMANBLAKE","PGAIDIS")


	cISA_Num= ""

	************************** email alert for just SQL uploading ***************************************
	IF llLoadSql = .T.
		tfrom ="TGF EDI Ops <fmicorporate@fmiint.com>"
		tsubject = IIF(glBBB_210_Testing,'TESTING-','') + "BBB Data Transactions Upload for: "+ALLTRIM(whichType)
		tattach = " "
		IF glBBB_210_Testing THEN
			tto = "mark.bennett@tollgroup.com"
			tcc = ""
		ELSE
			tto = "paul.gaidis@tollgroup.com"
			tcc = "mark.bennett@tollgroup.com"
		ENDIF
		tmessage = tmessage + CHR(13) + IIF(glBBB_210_Testing,'TESTING-','') + "BBB Data uploads for: "+DTOC(DATE())+CHR(13)
		tmessage = tmessage + "Type  close date   Filename             Trailer              PCSMan/BOL                     BBMAN/Pro                 Qty"+CHR(13)
		SELECT csrUpload
		*tattach = "h:\fox\upload.txt"
		SCAN
			tmessage = tmessage + csrUpload.TYPE+"   "+DTOC(csrUpload.closedt)+"  "+ PADR(ALLTRIM(csrUpload.filename),20," ")+"  "+csrUpload.trailer+" "+PADR(ALLTRIM(csrUpload.ref1),30," ")+;
				" "+PADR(ALLTRIM(csrUpload.ref2),30," ")+TRANSFORM(csrUpload.qty)+"  "+ALLTRIM(whichType)+CHR(13)
		ENDSCAN
		SELECT csrUpload

		lcAlertFilename = "h:\fox\BBB_Upload_"+TTOC(DATETIME(),1)+".xls"

		EXPORT TO &lcAlertFilename TYPE XLS
		tattach = lcAlertFilename

		*STRTOFILE(tmessage,"h:\fox\upload.txt")

		DO FORM M:\dev\frm\dartmail2 WITH tto,tfrom,tsubject,tcc,tattach,tmessage,"A"
	ENDIF

	************************** email alert for just Invoicing and 210s ***************************************
	IF llCreateInvoice = .T. OR llCreate210 = .T.
		tfrom ="TGF EDI Ops <fmicorporate@fmiint.com>"
		tsubject = IIF(glBBB_210_Testing,'TESTING-','') + "BBB Data File Invoicing"
		tsubject = IIF(glBBB_210_Reload,'RELOADING-','') + tsubject
		tattach = " "
		IF glBBB_210_Testing THEN
			tto = "mark.bennett@tollgroup.com"
			tcc = ""
		ELSE
			tto = "paul.gaidis@tollgroup.com"
			tcc = "mark.bennett@tollgroup.com"
		ENDIF

		tmessage = tmessage + IIF(glBBB_210_Testing,'TESTING-','') + "BBB Data Invoicing for: "+DTOC(DATE())+CHR(13)
		tmessage = IIF(glBBB_210_Reload,'RELOADING-','') + tmessage
		tmessage = tmessage + "Filename                      Type     Inv#           Qty            Amount "+CHR(13)

		SELECT curEmailInvoice
		GO TOP
		SCAN
			tmessage = tmessage + PADR(ALLTRIM(curEmailInvoice.filename),20," ")+"   "+curEmailInvoice.TYPE+"  "+PADR(ALLTRIM(curEmailInvoice.invnum),12," ")+"  "+STR(curEmailInvoice.qty,6,0)+"  "+STR(curEmailInvoice.amount,15,2)+CHR(13)
		ENDSCAN
		SELECT csrUpload

		lcAlertFilename = "h:\fox\BBB_InvoiceUpload_"+TTOC(DATETIME(),1)+".xls"

		EXPORT TO &lcAlertFilename TYPE XLS
		tattach = lcAlertFilename

		*STRTOFILE(tmessage,"h:\fox\upload.txt")

		* if we got to here (i.e., no errors), move the 210 file created in BB_210 from PRE210OUT to 210OUT folder from which it will be ftp'd to BBB
		lc210OutPath = "F:\FTPUSERS\BEDBATH\210OUT\"
		lc210PrePath = "F:\FTPUSERS\BEDBATH\PRE210OUT\"
		lc210PreArchivePath = "F:\FTPUSERS\BEDBATH\PRE210OUT\ARCHIVE\"

		*!*			lnNumFiles = ADIR(laFiles,(lc210PrePath+ "tgf*.*"))

		*!*			IF lnNumFiles > 0 THEN

		*!*				* copy the files
		*!*				FOR lnCurrentFile = 1 TO lnNumFiles

		*lcSourceFile = lc210PrePath + laFiles[lnCurrentFile,1]
		*lcArchivedFile = lc210PreArchivePath + laFiles[lnCurrentFile,1]
		*lcTargetFile = lc210OutPath + laFiles[lnCurrentFile,1]

		lcSourceFile = lc210PrePath + m.c210FileName
		lcArchivedFile = lc210PreArchivePath + m.c210FileName
		lcTargetFile = lc210OutPath + m.c210FileName

		IF FILE(lcSourceFile) THEN

			tmessage = tmessage + '====> Found a 210 file to be moved!' + CHR(13)

			*m.c210FileName

			tmessage = tmessage + 'lcSourceFile = ' + lcSourceFile + CHR(13)
			tmessage = tmessage + 'lcArchivedFile = ' + lcArchivedFile + CHR(13)
			tmessage = tmessage + 'lcTargetFile = ' + lcTargetFile + CHR(13)

			COPY FILE (lcSourceFile) TO (lcTargetFile)

			* archive the source file

			* if the file already exists in the archive folder, make sure it is not read-only.
			* this is to prevent errors we get copying into archive on a resend of an already-archived file.
			* Necessary because the files in the archive folders were sometimes marked as read-only (not sure why)
			llArchived = FILE(lcArchivedFile)
			IF llArchived THEN
				RUN ATTRIB -R &lcArchivedFile.
			ENDIF

			* archive the source file
			COPY FILE (lcSourceFile) TO (lcArchivedFile)

			* delete the source file if the copy was successful
			IF FILE(lcTargetFile) THEN
				tmessage = CHR(13) + tmessage + 'Copied to 210Out folder: ' + lcSourceFile + CHR(13)
				DELETE FILE (lcSourceFile)
			ENDIF

			*!*	ENDFOR && lnCurrentFile = 1 TO lnNumFiles
		ELSE
			tmessage = tmessage + '======> WARNING: Found no 210 files in the source folder.' + CHR(13)
		ENDIF  &&  FILE(lcSourceFile)

		tmessage = CHR(13) + tmessage + '=====> The process ended normally <====' + CHR(13)

		DO FORM M:\dev\frm\dartmail2 WITH tto,tfrom,tsubject,tcc,tattach,tmessage,"A"

	ENDIF

	schedupdate()

	CLOSE DATABASES ALL

	WAIT CLEAR

CATCH TO loError

	tmessage = tmessage + CHR(13) + 'There was an error.' + CHR(13)
	tmessage = tmessage + 'The Error # is: ' + TRANSFORM(loError.ERRORNO) + CHR(13)
	tmessage = tmessage + 'The Error Message is: ' + TRANSFORM(loError.MESSAGE) + CHR(13)
	tmessage = tmessage + 'The Error occurred in Program: ' + TRANSFORM(loError.PROCEDURE) + CHR(13) + CHR(13)
	tmessage = tmessage + 'The Error occurred at line #: ' + TRANSFORM(loError.LINENO) + CHR(13)
	tmessage = tmessage + CHR(13) + CHR(13) + gcUserMessage

	tfrom ="TGF EDI Ops <fmicorporate@fmiint.com>"
	tsubject = IIF(glBBB_210_Testing,'TESTING-','') + "ERROR in BBB Data File Invoicing"
	tattach = ""
	tcc=''
	DO FORM M:\dev\frm\dartmail2 WITH "mark.bennett@tollgroup.com",tfrom,tsubject,tcc,tattach,tmessage,"A"

	CLOSE DATABASES ALL

	WAIT CLEAR


ENDTRY

RETURN



PROCEDURE PrepReload

	*!*		USE F:\3PL\DATA\BBBINV_RELOAD SHARED IN 0
	*!*		USE F:\3PL\DATA\BBB_INVOICEDATA SHARED IN 0

	*!*		SELECT BBBINV_RELOAD
	*!*		SCAN
	*!*			SELECT BBB_INVOICEDATA
	*!*			LOCATE FOR ALLTRIM(INVNUM) = ALLTRIM(BBBINV_RELOAD.INVNUM)
	*!*			IF FOUND() THEN
	*!*				REPLACE BBBINV_RELOAD.FILENAME WITH BBB_INVOICEDATA.FILENAME IN BBBINV_RELOAD
	*!*			ENDIF
	*!*		ENDSCAN

	*!*		SELECT BBBINV_RELOAD
	*!*		BROWSE

	* ----------------------------------

	* NOT ALL INVOICES WERE FOUND IN BBB_INVOICEDATA, SO LOOK IN RESULTS OF bbin and bbtn sql queries to get the missing ones
	*!*		CLOSE DATABASES ALL
	*!*		xsqlexec("select DISTINCT invnum, filename from bbin order by invnum","curbbin",,"ar")
	*!*		SELECT curbbin
	*!*		BROWSE
	*!*		SELECT curbbin
	*!*		INDEX ON ALLTRIM(INVNUM) TAG INVNUM

	*!*		USE F:\3PL\DATA\BBBINV_RELOAD SHARED IN 0
	*!*
	*!*		SELECT BBBINV_RELOAD
	*!*		SCAN FOR EMPTY(FILENAME)
	*!*			SELECT curbbin
	*!*			LOCATE FOR ALLTRIM(INVNUM) = ALLTRIM(BBBINV_RELOAD.INVNUM)
	*!*			IF FOUND() THEN
	*!*				REPLACE BBBINV_RELOAD.FILENAME WITH curbbin.FILENAME IN BBBINV_RELOAD
	*!*			ENDIF
	*!*		ENDSCAN

	*!*		SELECT BBBINV_RELOAD
	*!*		BROWSE

	* ----------------------------------
	*!*		CLOSE DATABASES ALL
	*!*		xsqlexec("select DISTINCT invnum, filename from bbtn order by invnum","curbbtn",,"ar")
	*!*		SELECT curbbtn
	*!*		BROWSE
	*!*		SELECT curbbtn
	*!*		INDEX ON ALLTRIM(INVNUM) TAG INVNUM

	*!*		USE F:\3PL\DATA\BBBINV_RELOAD SHARED IN 0
	*!*
	*!*		SELECT BBBINV_RELOAD
	*!*		SCAN FOR EMPTY(FILENAME)
	*!*			SELECT curbbtn
	*!*			LOCATE FOR ALLTRIM(INVNUM) = ALLTRIM(BBBINV_RELOAD.INVNUM)
	*!*			IF FOUND() THEN
	*!*				REPLACE BBBINV_RELOAD.FILENAME WITH curbbtn.FILENAME IN BBBINV_RELOAD
	*!*			ENDIF
	*!*		ENDSCAN

	*!*		SELECT BBBINV_RELOAD
	*!*		BROWSE
	*!*		BROWSE FOR EMPTY(filename)

	* ----------------------------------
	*!*	* 	check for duplicate filenames in what we are re-invoicing
	*!*		CLOSE DATABASES ALL
	*!*		USE F:\3PL\DATA\BBBINV_RELOAD SHARED IN 0
	*!*		SELECT filename, COUNT(*) as cnt FROM BBBINV_RELOAD GROUP BY filename ORDER BY filename HAVING cnt > 1

	* ----------------------------------
	*!*	* make sure all files to be loaded exist in the source folder AND GET THEIR SIZES!
	*!*		LOCAL lcFileStr, lcFileName
	*!*		CLOSE DATABASES ALL
	*!*		USE F:\3PL\DATA\BBBINV_RELOAD SHARED IN 0
	*!*		SELECT BBBINV_RELOAD
	*!*		SCAN
	*!*			lcFileName = 'F:\FTPUSERS\Bedbath\REINVOICES\'+ALLTRIM(BBBINV_RELOAD.FILENAME)
	*!*			IF FILE(lcFileName) THEN
	*!*				lcFileStr = FILETOSTR(lcFileName)
	*!*				REPLACE BBBINV_RELOAD.FILEEXISTS WITH .T., BBBINV_RELOAD.filesize WITH LEN(lcFileStr) IN BBBINV_RELOAD
	*!*			ELSE
	*!*				REPLACE FILEEXISTS WITH .F., BBBINV_RELOAD.filesize WITH 0 IN BBBINV_RELOAD
	*!*			ENDIF
	*!*		ENDSCAN
	*!*		SELECT BBBINV_RELOAD
	*!*		BROWSE


	* ----------------------------------


	*!*		* AFTER ALL THIS, if there are duplicate filenames each with 2 different invnums in BBBINV_RELOAD - flag the first of each pair as dupes
	*!*		CLOSE DATABASES ALL
	*!*		USE F:\3PL\DATA\BBBINV_RELOAD SHARED IN 0
	*!*		SELECT BBBINV_RELOAD
	*!*		SET ORDER TO INVNUM
	*!*		BROWSE
	*!*
	*!*		USE F:\3PL\DATA\BBB_DUPES SHARED IN 0
	*!*
	*!*		SELECT BBB_DUPES
	*!*		SCAN
	*!*			SELECT BBBINV_RELOAD
	*!*			LOCATE FOR FILENAME = BBB_DUPES.FILENAME
	*!*			IF FOUND() THEN
	*!*				REPLACE BBBINV_RELOAD.DUPLICATE WITH .T. IN BBBINV_RELOAD
	*!*			ENDIF
	*!*		ENDSCAN
	*!*
	*!*		SELECT BBBINV_RELOAD
	*!*		SET ORDER TO INVNUM
	*!*		BROWSE




ENDPROC

*!*	************************************************************************************************************
*!*	*Do bbb_stats

*!*	****  code to prep reload of the data for the pink invoices in Jim C.'s spreadsheet. The edi files created were invalid so the original source files must be reprocessed
*!*	LOCAL lcSourceFolder, lcTargetFolder, lcSourceFile, lcTargetFile, lcFileName

*!*	lcSourceFolder = 'F:\FTPUSERS\BEDBATH\DATAIN\ARCHIVE\'

*!*	lcTargetFolder = 'F:\FTPUSERS\BEDBATH\REINVOICES\'

*!*	USE f:\3pl\data\pinkreload.dbf IN 0 ALIAS Pinkreload

*!*	USE f:\3pl\data\bbb_invoicedata.dbf IN 0 ALIAS bbb_invoicedata

*!*	SELECT Pinkreload
*!*	SCAN FOR lprocess
*!*		SELECT bbb_invoicedata
*!*		LOCATE FOR ALLTRIM(INVNUM) == ALLTRIM(Pinkreload.INVNUM)
*!*		IF FOUND() THEN
*!*			IF NOT EMPTY(bbb_invoicedata.FILENAME) THEN
*!*				lcFileName = ALLTRIM(bbb_invoicedata.FILENAME)
*!*				REPLACE Pinkreload.ORIGFILE WITH lcFileName IN Pinkreload
*!*				lcSourceFile = lcSourceFolder + lcFileName
*!*				IF NOT FILE(lcSourceFile) THEN
*!*					REPLACE Pinkreload.CFILENAME WITH '*NOT FOUND*' IN Pinkreload
*!*				ELSE
*!*					lcTargetFile = lcTargetFolder + lcFileName
*!*					COPY FILE(lcSourceFile) TO (lcTargetFile)
*!*					IF FILE(lcTargetFile) THEN
*!*						REPLACE Pinkreload.CFILENAME WITH lcFileName IN Pinkreload
*!*					ENDIF
*!*				ENDIF
*!*			ENDIF
*!*		ENDIF
*!*	ENDSCAN

*!*	SELECT Pinkreload
*!*	BROWSE
*!*	CLOSE DATA ALL

*!*	****************************************************************************************************************
*!*	* get the count of ST segments from a BBB 210 file; to help fix the 210s that were missing the trailer segments.
*!*	LOCAL lcFile, lcLine, lnHandle, lnSTCount
*!*	lnSTCount = 0
*!*	lcFile = 'F:\FTPUSERS\Bedbath\210out\Resent\TGFJ20170906092440.002'
*!*	lnHandle = FOPEN(lcFile)
*!*	DO WHILE NOT FEOF(lnHandle)
*!*		lcLine = FGETS(lnHandle)
*!*		IF LEFT(lcLine,3) == 'ST*' THEN
*!*			lnSTCount = lnSTCount + 1
*!*		ENDIF
*!*	ENDDO
*!*	FCLOSE(lnHandle)
*!*	WAIT WINDOW 'STCount =' + TRANSFORM(lnSTCount)






















