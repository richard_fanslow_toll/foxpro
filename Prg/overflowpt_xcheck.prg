PARAMETERS cBOL

IF VARTYPE(cBOL) = "L"
	close data all
	cBOL = '04907316521679239'
	USE F:\wh2\whdata\outship ALIAS outship
	SET STEP ON 
ENDIF

WAIT WINDOW "Now selecting PTs for reference" nowait
SELECT outship
SELECT ship_ref FROM  outship WHERE bol_no = cBOL INTO CURSOR temppicks
SELECT temppicks
LOCATE
WAIT CLEAR 
SCAN
	cSR1 = ALLTRIM(temppicks.ship_ref)
	SELECT outship
	COUNT TO N FOR outship.ship_ref = cSR1
	IF N > 1
		lOverflow = .T.
		WAIT WINDOW "Overflow found at PT "+cSR1+", setting DELETED off" TIMEOUT 2
		EXIT
	ENDIF
ENDSCAN
SELECT outship
RELEASE cSR1
RETURN lOverflow
