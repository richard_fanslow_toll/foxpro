Create cursor ftplog (;
  xfertime c(20),;
  customer c(20),;
  direction c(8),;
  filename c(100),;
  sessionid c(6),;
  uploaddt d )
  
Create cursor test (;
  f1 c(200))

Select ftplog
Index on xfertime  tag time
Index on customer  tag cust
Index on direction tag status
Index on filename  tag file
Index on sessionid tag sessionid

Public custfilter,sessionfilter  

custfilter = ""
sessionfilter = ""

Set Deleted on
*_Screen.WindowState = Iif(Inlist(whichuser,"JOEB","JBIANCHI"),1,2)


lcPath ="f:\ftplogs\"
lnNum = ADIR(tarray,"f:\ftplogs\*.txt")

*Cd "f:\ftplogs\"
*lcFile =Getfile("*.*")

IF lnNum = 0
	WAIT WINDOW AT 10,10 "      No "+cCustname+" 940s to import     " TIMEOUT 3
	NormalExit = .T.
	THROW
ENDIF

FOR thisfile = 1  TO lnNum
  Select test
  Delete all
  cfilename = LOWER(TRIM(tarray[thisfile,1]))
  tfile = lcPath+TRIM(tarray[thisfile,1])
  Append From [&tfile] type delimited with character "|"

Select test
If Substr(test.f1,29,1) = "("
  session_offset = 30
Else
  session_offset = 29
Endif

Scan
  If "Sent"$f1 
   Insert into ftplog (xfertime,direction,filename,sessionid) values (Alltrim(Substr(test.f1,5,21)),Substr(test.f1,37,4),Substr(test.f1,47,100),Substr(test.f1,session_offset ,6))
  EndIf
  If "Received"$f1 
   Insert into ftplog (xfertime,direction,filename,sessionid) values (Substr(test.f1,5,20),Substr(test.f1,37,8),Substr(test.f1,51,100),Substr(test.f1,session_offset ,6))
  EndIf
  If "Receiving"$f1 
   Insert into ftplog (xfertime,direction,filename,sessionid) values (Substr(test.f1,5,20),Substr(test.f1,37,8),Substr(test.f1,51,100),Substr(test.f1,session_offset ,6))
  EndIf
  If "RNTO "$f1 
   Insert into ftplog (xfertime,direction,filename,sessionid) values (Substr(test.f1,5,20),"RNTO",Substr(test.f1,42,100),Substr(test.f1,session_offset ,6))
  EndIf
EndScan

endfor

Select ftplog
Scan
   Do case
     Case "alpha"$filename
       replace customer with "Alpha6"
     Case "dodenim"$filename
       replace customer with "DoDenim"
     Case "cmn"$filename
       replace customer with "CMN"
     Case "moret"$filename
       replace customer with "Moret"
     Case "saralee"$filename
       replace customer with "Courtaulds"
     Case "cleatskins"$filename
       replace customer with "Cleatskins"
     Case "guess"$filename
       replace customer with "Guess"
     Case "modernshoe"$filename
       replace customer with "ModernShoe"
     Case "fiji"$filename
       replace customer with "FIJI"
     Case "ipack"$filename
       replace customer with "IPack"
     Case "under"$filename
       replace customer with "UnderArmour"
     Case "vf"$filename
       replace customer with "VF"
     Case "footlocker"$filename
       replace customer with "Footlocker"
     Case "mamiye"$filename
       replace customer with "MamiyeBros"
     Case "meldisco"$filename
       replace customer with "Sears/Kmart"
     Case "baby"$filename
       replace customer with "Baby Toggs"
     Case "pgp"$filename
       replace customer with "BOA"
     Case "sum-edi"$filename
       replace customer with "Summit Mgt"
     Case "bugaboo"$filename
       replace customer with "Bugaboo"
     Case "fed210"$filename
       replace customer with "Federated"
     Case "pony"$filename
       replace customer with "Pony"
     Case "tko"$filename
       replace customer with "TKO"
     Case "sportswear"$filename
       replace customer with "Sportswear Group"
     Case "sears"$filename
       replace customer with "Sears/Kmart"
     Case "agegroup"$filename
       replace customer with "Age Group"
     Case "pgp"$filename
       replace customer with "BOA"
     Case "945be"$filename
       replace customer with "Bernardo"
     Case "bioworld"$filename
       replace customer with "Bioworld"
     Case "Synclaire"$filename
       replace customer with "Synclaire"
     Case "lifefactory"$filename
       replace customer with "Lifefactory"
     Case "rackroom"$filename
       replace customer with "Rackroom"
     Case "grandvoyage"$filename
       replace customer with "Grandvoyage"
     Case "bedbath"$filename
       replace customer with "BedBath"
     Case "steelseries"$filename
       replace customer with "Steel Series"
     Case "SaleOrder"$filename
       replace customer with "2XU"
     Case "2xu"$filename
       replace customer with "2XU"
     Case "biglots"$filename
       replace customer with "Big Lots"
     Case "WMSASN"$filename
       replace customer with "UnderArmour"
     Case "g-iii"$filename
       replace customer with "G3"
     Case "ragbone"$filename
       replace customer with "Ragand Bone"
     Otherwise
      replace customer with "UNK"
   EndCase
   If "Sen"$direction
     replace direction With "Sent" In ftplog
    Endif 
    replace filename With Strtran(filename,[ "],"") in ftplog

Endscan

Select ftplog 
Index on uploaddt Tag when

Try 
  Copy To h:\fox\ftplog
Catch
  MessageBox("You need to have an H:\FOX mapped drive to run this program",0,"EDI Report Generator")
EndTry 