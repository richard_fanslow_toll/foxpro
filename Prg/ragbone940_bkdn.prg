*!* m:\dev\prg\bbc940_bkdn.prg

CLEAR
lcheckstyle = .T.
lprepack = .F.  && Inventory to be kept as PnP
m.color=""
cuccnumber = ""
cusepack = ""
lcuccseed = 1

SELECT x856
COUNT FOR TRIM(segment) = "ST" TO nsegcnt
csegcnt = ALLTRIM(STR(nsegcnt))
SELECT x856
LOCATE

ptctr = 0

WAIT WINDOW "Now in "+cmailname+" 940 Breakdown" NOWAIT

WAIT "There are "+csegcnt+" P/Ts in this file" WINDOW NOWAIT
SELECT x856
LOCATE

*!* Constants
m.acct_name = "RAGBONE"

STORE nacctnum TO m.accountid
m.careof = " "
m.sf_addr1 = "416 WEST 13TH STREET"
m.sf_addr2 = ""
m.sf_csz = "NEW YORK, NY 10014"
m.printed = .F.
m.print_by = ""
m.printdate = DATE()
*!* End constants

SELECT xpt
DELETE ALL
SELECT xptdet
DELETE ALL

STORE 0 TO pt_total_eaches
STORE 0 TO pt_total_cartons
STORE 0 TO nctncount,m.ptid,m.ptdetid

SELECT x856
SET FILTER TO
LOCATE
CREATE CURSOR curppk (;
	upc CHAR(12),;
	qty N(6),;
	STYLE CHAR(20),;
	COLOR CHAR(10),;
	ID    CHAR(20),;
	musical CHAR(40))

STORE "" TO cisa_num

WAIT WINDOW "NOW IN INITAL HEADER BREAKDOWN" TIMEOUT 1
WAIT WINDOW "NOW IN INITAL HEADER BREAKDOWN" NOWAIT
*ASSERT .F. MESSAGE "At header bkdn...debug"
DO WHILE !EOF("x856")
	csegment = TRIM(x856.segment)
	ccode = TRIM(x856.f1)

	IF TRIM(x856.segment) = "ISA"
		cisa_num = ALLTRIM(x856.f13)
		currentisanum = ALLTRIM(x856.f13)
		WAIT WINDOW "This is a "+cmailname+" PT upload" NOWAIT
		SELECT x856
		SKIP
		LOOP
	ENDIF

	IF TRIM(x856.segment) = "GS"
		cgs_num = ALLTRIM(x856.f6)
		lccurrentgroupnum = ALLTRIM(x856.f6)
		SELECT x856
		SKIP
		LOOP
	ENDIF

	IF TRIM(x856.segment) = "ST" AND TRIM(x856.f1) = "940"
		IF !ltesting
			SELECT x856
			SKIP
			LOOP
		ENDIF
	ENDIF

	IF TRIM(x856.segment) = "W05"  && new shipment
		nctncount = 0
		m.style = ""
		SELECT xpt
		ndetcnt = 0
		APPEND BLANK
		ddt1 = DATETIME()
		REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"FILENAME*"+cfilename IN xpt
		REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"CONAME*BBC" IN xpt
		REPLACE xpt.addby WITH "TOLLPROC" IN xpt
		REPLACE xpt.adddt WITH ddt1 IN xpt
		REPLACE xpt.addproc WITH "RB940" IN xpt
		REPLACE xpt.office WITH coffice IN xpt
		REPLACE xpt.MOD WITH cmod IN xpt
		REPLACE xpt.qty WITH 0 IN xpt
		REPLACE xpt.origqty WITH 0 IN xpt
		STORE 0 TO pt_total_eaches
		STORE 0 TO pt_total_cartons
		ptctr = ptctr +1
		WAIT WINDOW AT 10,10 "Now processing PT# "+ ALLTRIM(x856.f2)+" Number "+CHR(13)+TRANSFORM(ptctr)+" of "+csegcnt NOWAIT

		REPLACE xpt.ptid       WITH ptctr IN xpt
		REPLACE xpt.accountid  WITH nacctnum IN xpt
		REPLACE xpt.ptdate     WITH dfiledate IN xpt
		IF ltesting  && Added 04.03.2018, Joe, per Fanz/R&B.
			m.ship_ref = ALLTRIM(x856.f2)
			REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"LINKSEQ*"+ALLTRIM(x856.f4) IN xpt
			REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"REFLINK*"+ALLTRIM(x856.f5) IN xpt
		ELSE
			m.ship_ref = ALLTRIM(x856.f5)
			m.pt = ALLTRIM(x856.f5)
		ENDIF
		cship_ref = m.ship_ref
		REPLACE xpt.ship_ref   WITH m.ship_ref IN xpt  && Pick Ticket
		REPLACE xpt.cnee_ref   WITH ALLTRIM(x856.f3)  && cust PO
		SELECT x856
		SKIP
		LOOP
	ENDIF

***Added TMARG 09/27/17 to capture consignee from N1 BT 02  as Per RnB
** Remapped all BT segments to Billto in Shipins  PG 3/12/2018
** ShipTo-->consignee is ST loop data
	IF TRIM(x856.segment) = "N1" AND TRIM(x856.f1) = "BT" && Ship-to data
		cconsignee = UPPER(ALLTRIM(x856.f2))
		REPLACE xpt.consignee WITH  cconsignee IN xpt
		REPLACE xpt.NAME WITH  cconsignee IN xpt
*		SELECT xpt
*		m.st_name = cconsignee
*		REPLACE xpt.consignee WITH  cconsignee IN xpt
		REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"BILLTONAME*"+ALLTRIM(x856.f2) IN xpt
		SKIP 1 IN x856
		DO CASE
			CASE TRIM(x856.segment) = "N2"  && name ext. info
				REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"BILLTOADDR1*"+ALLTRIM(x856.f1) IN xpt
				SKIP 1 IN x856
				IF TRIM(x856.segment) = "N3"  && address info
					REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"BILLTOADDR2*"+ALLTRIM(x856.f1) IN xpt
				ENDIF
				SKIP 1 IN x856
				IF TRIM(x856.segment) = "N4"  && address info
					m.st_csz = UPPER(ALLTRIM(x856.f1))+", "+UPPER(ALLTRIM(x856.f2))+" "+TRIM(x856.f3)
					REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"BILLTOCSZ*"+ALLTRIM(m.st_csz) IN xpt
					REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"COUNTRY*"+ALLTRIM(x856.f4) IN xpt
				ENDIF
			CASE TRIM(x856.segment) = "N3"  && address info
				REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"BILLTOADDR1*"+ALLTRIM(x856.f1) IN xpt
				REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"BILLTOADDR2*"+ALLTRIM(x856.f2) IN xpt
				SKIP 1 IN x856
				IF TRIM(x856.segment) = "N4"  && address info
					m.st_csz = UPPER(ALLTRIM(x856.f1))+", "+UPPER(ALLTRIM(x856.f2))+" "+TRIM(x856.f3)
					REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"BILLTOCSZ*"+ALLTRIM(m.st_csz) IN xpt
					REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"COUNTRY*"+ALLTRIM(x856.f4) IN xpt
				ENDIF
			OTHERWISE
				WAIT WINDOW "UNKNOWN ADDRESS SEGMENT" TIMEOUT 2
				THROW
		ENDCASE

	ENDIF

	IF TRIM(x856.segment) = "N1" AND TRIM(x856.f1) = "ST" && Ship-to data
		cconsignee = UPPER(ALLTRIM(x856.f2))
		SELECT xpt
*		REPLACE xpt.consignee WITH  cConsignee IN xpt
*		REPLACE xpt.NAME WITH  cconsignee IN xpt

		cstorenum = ALLTRIM(x856.f4)
		IF EMPTY(xpt.shipins)
			REPLACE xpt.shipins WITH "STORENUM*"+cstorenum IN xpt
		ELSE
			REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"STORENUM*"+cstorenum IN xpt
		ENDIF
		IF EMPTY(xpt.dcnum)
			REPLACE xpt.dcnum WITH cstorenum IN xpt
		ENDIF
		IF LEN(cstorenum)>5
			REPLACE xpt.storenum  WITH VAL(RIGHT(cstorenum,5)) IN xpt
		ELSE
			REPLACE xpt.storenum  WITH VAL(cstorenum) IN xpt
		ENDIF

		SKIP 1 IN x856
		DO CASE
			CASE TRIM(x856.segment) = "N2"  && name ext. info
				m.name = UPPER(ALLTRIM(x856.f1))
				REPLACE xpt.NAME WITH m.name IN xpt
				SKIP 1 IN x856
				IF TRIM(x856.segment) = "N3"  && address info
					m.st_addr1 = UPPER(ALLTRIM(x856.f1))
					REPLACE xpt.address  WITH m.st_addr1 IN xpt
					m.st_addr2 = UPPER(ALLTRIM(x856.f2))
					REPLACE xpt.address2 WITH m.st_addr2 IN xpt
				ENDIF
				SKIP 1 IN x856
				IF TRIM(x856.segment) = "N4"  && address info
					m.zipbar = UPPER(ALLTRIM(x856.f3))
					m.st_csz = UPPER(ALLTRIM(x856.f1))+", "+UPPER(ALLTRIM(x856.f2))+" "+TRIM(x856.f3)
					REPLACE xpt.csz WITH m.st_csz IN xpt
					REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"COUNTRY*"+ALLTRIM(x856.f4) IN xpt
				ENDIF
			CASE TRIM(x856.segment) = "N3"  && address info
				m.st_addr1 = UPPER(ALLTRIM(x856.f1))
				REPLACE xpt.address  WITH m.st_addr1 IN xpt
				m.st_addr2 = UPPER(ALLTRIM(x856.f2))
				REPLACE xpt.address2 WITH m.st_addr2 IN xpt
				SKIP 1 IN x856
				IF TRIM(x856.segment) = "N4"  && address info
					m.zipbar = UPPER(ALLTRIM(x856.f3))
					m.st_csz = UPPER(TRIM(x856.f1))+", "+UPPER(TRIM(x856.f2))+" "+TRIM(x856.f3)
					REPLACE xpt.csz WITH m.st_csz IN xpt
					REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"COUNTRY*"+ALLTRIM(x856.f4) IN xpt
					REPLACE xpt.country WITH ALLTRIM(x856.f4) IN xpt
				ENDIF
			OTHERWISE
				WAIT WINDOW "UNKNOWN ADDRESS SEGMENT" TIMEOUT 2
				THROW
		ENDCASE
		SELECT x856
		SKIP
		LOOP
	ENDIF

	IF TRIM(x856.segment) = "N1" AND TRIM(x856.f1) = "Z7" && Returned data
		cnom = ALLTRIM(x856.f1)
		REPLACE xpt.shipfor   WITH  UPPER(ALLTRIM(x856.f2)) IN xpt
		REPLACE xpt.sforstore WITH  UPPER(ALLTRIM(x856.f4)) IN xpt
** changed from cstorenum (N1*ST) to
*		REPLACE xpt.storenum  WITH (x856.f4) IN xpt
		REPLACE xpt.shipins   WITH xpt.shipins+CHR(13)+cnom+"_N1*"+ALLTRIM(x856.f2) IN xpt
		SKIP 1 IN x856

		DO CASE
			CASE TRIM(x856.segment) = "N2"
				REPLACE xpt.sforaddr1 WITH UPPER(ALLTRIM(x856.f1)) IN xpt
				REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+cnom+"_N2*"+UPPER(ALLTRIM(x856.f1)) IN xpt
				SKIP 1 IN x856
				IF TRIM(x856.segment) = "N3"
					REPLACE xpt.shipfor   WITH UPPER(ALLTRIM(x856.f1)) IN xpt
					REPLACE xpt.sforaddr1 WITH UPPER(ALLTRIM(x856.f1)) IN xpt
					REPLACE xpt.sforaddr2 WITH UPPER(ALLTRIM(x856.f2)) IN xpt

					REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+cnom+"_N3*"+ALLTRIM(x856.f1)+"|"+ALLTRIM(x856.f2) IN xpt
					SKIP 1 IN x856
				ENDIF
				IF TRIM(x856.segment) = "N4"
					REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+cnom+"_N4*"+ALLTRIM(x856.f1)+"|"+ALLTRIM(x856.f2)+"|"+ALLTRIM(x856.f3) IN xpt
					REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"COUNTRY*"+ALLTRIM(x856.f4) IN xpt
					REPLACE xpt.sforcsz WITH ALLTRIM(x856.f1)+", "+ALLTRIM(x856.f2)+" "+ALLTRIM(x856.f3) IN xpt
				ENDIF

			CASE TRIM(x856.segment) = "N3"
				REPLACE xpt.shipfor  WITH  UPPER(ALLTRIM(x856.f1)) IN xpt
				REPLACE xpt.sforaddr1 WITH UPPER(ALLTRIM(x856.f1)) IN xpt
				REPLACE xpt.sforaddr2 WITH UPPER(ALLTRIM(x856.f2)) IN xpt
				REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+cnom+"_N3*"+ALLTRIM(x856.f1)+"|"+ALLTRIM(x856.f2) IN xpt
				SKIP 1 IN x856
				IF TRIM(x856.segment) = "N4"
					REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+cnom+"_N4*"+ALLTRIM(x856.f1)+"|"+ALLTRIM(x856.f2)+"|"+ALLTRIM(x856.f3) IN xpt
					REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"COUNTRY*"+ALLTRIM(x856.f4) IN xpt
					REPLACE xpt.sforcsz WITH UPPER(ALLTRIM(x856.f1))+", "+UPPER(ALLTRIM(x856.f2))+" "+ALLTRIM(x856.f3) IN xpt
				ENDIF

			CASE TRIM(x856.segment) = "N4"
				REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+cnom+"_N4*"+ALLTRIM(x856.f1)+"|"+ALLTRIM(x856.f2)+"|"+ALLTRIM(x856.f3) IN xpt
				REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"COUNTRY*"+ALLTRIM(x856.f4) IN xpt
				REPLACE xpt.sforcsz WITH UPPER(ALLTRIM(x856.f1))+", "+UPPER(ALLTRIM(x856.f2))+" "+ALLTRIM(x856.f3) IN xpt
		ENDCASE

		SELECT x856
		SKIP
		LOOP
	ENDIF

	IF TRIM(x856.segment) = "PER"  && Contact
		REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"PERTYPE*"+ALLTRIM(x856.f3) IN xpt
		REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"PHONE*"+ALLTRIM(x856.f4) IN xpt
		SELECT x856
		SKIP
		LOOP
	ENDIF

	IF TRIM(x856.segment) = "N9"  && Additional info segments
		DO CASE
			CASE ALLTRIM(x856.f1) = "IT"  && Department/Division
				REPLACE xpt.div     WITH ALLTRIM(x856.f2) IN xpt
				REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"CUSTNUM*"+ALLTRIM(x856.f2) IN xpt
				SELECT x856
				SKIP
				LOOP

			CASE ALLTRIM(x856.f1) = "DV"  && Department/Division
				REPLACE xpt.div     WITH ALLTRIM(x856.f2) IN xpt
				REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"DIV*"+ALLTRIM(x856.f2) IN xpt
				SELECT x856
				SKIP
				LOOP

			CASE ALLTRIM(x856.f1) = "MR"  && Department/Division
				REPLACE xpt.div     WITH ALLTRIM(x856.f2) IN xpt
				REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"DIV*"+ALLTRIM(x856.f2) IN xpt
				SELECT x856
				SKIP
				LOOP

			CASE ALLTRIM(x856.f1) = "DP"  && Department/Division
				REPLACE xpt.dept WITH ALLTRIM(x856.f2) IN xpt
				REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"DEPT*"+ALLTRIM(x856.f2) IN xpt
				SELECT x856
				SKIP
				LOOP

			CASE ALLTRIM(x856.f1) = "ZZ"  && Customer Order number
				REPLACE xpt.terms WITH ALLTRIM(x856.f2) IN xpt
				REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"TERMS*"+ALLTRIM(x856.f2) IN xpt
				SELECT x856
				SKIP
				LOOP
		ENDCASE
	ENDIF

	IF TRIM(x856.segment) = "G62" AND TRIM(x856.f1) == "04"  && start date changed 11/01/17 TMARG as per Greg from '01'
		lcdate = ALLTRIM(x856.f2)
		lxdate = SUBSTR(lcdate,5,2)+"/"+SUBSTR(lcdate,7,2)+"/"+LEFT(lcdate,4)
		lddate = CTOD(lxdate)
		STORE lddate TO dstart
		REPLACE xpt.START WITH lddate IN xpt
		SELECT x856
		SKIP
		LOOP
	ENDIF

	IF TRIM(x856.segment) = "G62" AND TRIM(x856.f1) == "10"  && cancel date
		lcdate = ALLTRIM(x856.f2)
		lxdate = SUBSTR(lcdate,5,2)+"/"+SUBSTR(lcdate,7,2)+"/"+LEFT(lcdate,4)
		lddate = CTOD(lxdate)
		STORE lddate TO dcancel
		REPLACE xpt.CANCEL WITH lddate IN xpt
		SELECT x856
		SKIP
		LOOP
	ENDIF

	IF TRIM(x856.segment) = "NTE" AND TRIM(x856.f1) = "WHI"
		ninc = 0
		DO WHILE TRIM(x856.segment) = "NTE"
			ninc = ninc+1
			REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"SPINS"+TRANSFORM(ninc)+"*"+ALLTRIM(x856.f2) IN xpt  && use to be the cacctnum
			SELECT x856
			SKIP
		ENDDO
		SELECT x856
		LOOP
	ENDIF

	llprepackflag = .F.

	IF TRIM(x856.segment) = "W66"
		IF ALLTRIM(x856.f1) = "PP"
			llprepackflag = .T.
		ENDIF
		REPLACE xpt.terms WITH ALLTRIM(x856.f1) IN xpt
		REPLACE xpt.ship_via WITH ALLTRIM(x856.f10) IN xpt
		SELECT x856
		SKIP
		LOOP
	ENDIF


	IF TRIM(x856.segment) = "R2" && start of PT detail, stay here until this PT is complete
		ASSERT .F. MESSAGE cship_ref+" Detail, Type = "+IIF(lprepack,"Prepack","Pickpack")+"...debug"
		REPLACE xpt.scac WITH ALLTRIM(x856.f1) IN xpt

		m.units = .T.
		llprepackflag = .F.

		WAIT "NOW IN DETAIL LOOP PROCESSING" WINDOW NOWAIT

		SELECT xptdet
		m.ptdetid = m.ptdetid + 1

		SELECT xptdet
		SKIP 1 IN x856
		lnlinenum=1


		DO WHILE ALLTRIM(x856.segment) != "SE"
			csegment = TRIM(x856.segment)
			ccode = TRIM(x856.f1)

			IF csegment ="LX"
				SKIP 1 IN x856
			ENDIF
			IF TRIM(x856.segment) = "W01" && Destination Quantity
				lcitemtype = ALLTRIM(x856.f2)  && either CA or EA
				m.totqty   = VAL(TRIM(x856.f1))
				m.upc      = ALLTRIM(x856.f5)
				m.style    = ALLTRIM(x856.f7)
				SELECT x856
				SKIP 1 IN x856
			ENDIF

			DO WHILE TRIM(x856.segment) = "N9"
				DO CASE
					CASE TRIM(x856.f1) = "D5"
						lccolordesc = TRIM(x856.f2)
					CASE TRIM(x856.f1) = "P4"
						lcsizedesc = TRIM(x856.f2)
					CASE TRIM(x856.f1) = "P5"
						lcsizeqty = TRIM(x856.f2)
					CASE TRIM(x856.f1) = "SZ"
						lcsize = TRIM(x856.f2)
					CASE TRIM(x856.f1) = "91"
						lcprice = TRIM(x856.f2)
				ENDCASE
				SKIP 1 IN x856
			ENDDO

			SELECT xptdet
			APPEND BLANK

			REPLACE xptdet.office  WITH coffice IN xptdet
			REPLACE xptdet.MOD     WITH cmod IN xptdet
			REPLACE xptdet.addby   WITH "TOLLPROC" IN xptdet
			REPLACE xptdet.adddt   WITH ddt1 IN xptdet
			REPLACE xptdet.addproc WITH "RB940" IN xptdet
			REPLACE xptdet.ptid    WITH xpt.ptid IN xptdet
			REPLACE xptdet.ptdetid WITH m.ptdetid IN xptdet
			REPLACE xptdet.units   WITH m.units IN xptdet
			REPLACE xptdet.linenum WITH ALLTRIM(STR(lnlinenum)) IN xptdet
			lnlinenum = lnlinenum + 1
			m.ptdetid = m.ptdetid + 1
			REPLACE xptdet.accountid WITH xpt.accountid IN xptdet
			cusepack = "1"
			REPLACE xptdet.PACK WITH cusepack IN xptdet
			m.casepack = INT(VAL(cusepack))

			REPLACE xptdet.ship_ref WITH xpt.ship_ref  IN xptdet
			REPLACE xptdet.casepack WITH m.casepack    IN xptdet
			REPLACE xptdet.totqty   WITH m.totqty      IN xptdet
			REPLACE xptdet.origqty  WITH xptdet.totqty IN xptdet
			REPLACE xptdet.STYLE    WITH m.style       IN xptdet
			REPLACE xptdet.ID       WITH lcsize        IN xptdet
			REPLACE xptdet.COLOR    WITH lccolordesc   IN xptdet
			REPLACE xptdet.upc      WITH m.upc         IN xptdet

			REPLACE xpt.qty     WITH xpt.qty + xptdet.totqty IN xpt
			REPLACE xpt.origqty WITH xpt.qty IN xpt
			REPLACE printstuff  WITH "COLORDESC*"+lccolordesc+CHR(13)
			REPLACE printstuff  WITH printstuff+"SIZE*"+lcsizedesc+CHR(13)
			REPLACE printstuff  WITH printstuff+"SIZEQTY*"+lcsizeqty+CHR(13)
			REPLACE printstuff  WITH printstuff+"PRICE*"+lcprice+CHR(13)
		ENDDO
	ENDIF

	SELECT x856
	SKIP 1 IN x856
ENDDO

SELECT xptdet
SUM(totqty) TO lnpttotal
REPLACE xpt.qty WITH lnpttotal IN xpt
REPLACE xpt.origqty WITH xpt.qty IN xpt
**************************************************
SELECT xptdet

*set step On  && added in the new roll up 12/27/2017  PG
DO rollup_ptdet_units
WAIT WINDOW "At end of ROLLUP procedure" NOWAIT

IF lbrowfiles OR ltesting
	WAIT WINDOW "If tables are incorrect, CANCEL when XPTDET window opens"+CHR(13)+"Otherwise, will load PT table" TIMEOUT 2
	SELECT xpt
	LOCATE
	BROWSE
	SELECT xptdet
	LOCATE
	BROWSE
*  cancel
ENDIF
***************************************************

WAIT WINDOW ccustname+" Breakdown Round complete..." NOWAIT
SELECT x856
IF USED("PT")
	USE IN pt
ENDIF
IF USED("PTDET")
	USE IN ptdet
ENDIF
WAIT CLEAR
SELECT xpt
REPLACE ALL office WITH "N" IN xpt
REPLACE ALL MOD WITH "I" IN xpt

SELECT xptdet

REPLACE ALL office WITH "N" IN xptdet
REPLACE ALL MOD WITH "I" IN xptdet

RETURN

*************************************************************************************************
*!* Error Mail procedure - Cancel Before Start Date
*************************************************************************************************
PROCEDURE errordates

	IF lemail
		tsendto = tsendtotest
		tcc =  tcctest
		cmailloc = IIF(coffice = "C","West",IIF(coffice = "N","New Jersey","Florida"))
		tsubject= "TGF "+cmailloc+" "+ccustname+" Pickticket Error: " +TTOC(DATETIME())
		tmessage = "File: "+cfilename+CHR(13)+" has Cancel date(s) which precede Start date(s)"
		tmessage = tmessage + "File needs to be checked and possibly resubmitted."
		DO FORM m:\dev\frm\dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
	ENDIF
ENDPROC
*************************************************************************************************

