
*!* f:\nanjing\prg\nanjing940_bkdn.prg
*!* Last revision: 03.05.2008, Joe

PARAMETERS cOffice,cFilename
lCheckStyle = .T.
lPrepack = .F.
lPrepackRep = .F.
lPick = .T.
lJCPenney = .F.
lOldStyle = .T.
nAcctNum = 4610
m.isa_num = ""
m.id = ""
cStylecheck = ""
cSTStoreNum = ""
ptctr = 0
lADNY = .F.

lDSDC = .F.

SELECT x856
LOCATE
LOCATE FOR TRIM(x856.segment) = "N9" AND TRIM(x856.f1) = "WS"
IF FOUND()
	cWSCode = ALLTRIM(x856.f2)
*cWSCode = "3"  && Comment this back out
	IF cWSCode = "3" && AND DATE()>{^2009-04-20}
		WAIT WINDOW "This is a Carson 2 PT Upload" TIMEOUT 1
		ASSERT .F. MESSAGE "In N9WS NJ file transfer"
		cOffice = "Y"
		gMasterOffice = cOffice
		nAcctNum = 4694
		ArchiveFile = STRTRAN(ArchiveFile,"nanjing","nanjing-cr")
		SELECT 0
		USE F:\3pl\DATA\mailmaster ALIAS mm
		LOCATE FOR mm.edi_type = "940" AND mm.accountid = nAcctNum AND mm.office = cOffice
		tsendto = IIF(mm.use_alt,sendtoalt,sendto)
		tcc = IIF(mm.use_alt,ccalt,cc)
		USE IN mm
		xReturn = "XX"
		DO m:\dev\prg\wf_alt WITH cOffice,nAcctNum
		cUseFolder = UPPER(TRIM(xReturn))
		SELECT x856
	ELSE
		cOffice = "Y"
		gMasterOffice = cOffice
		nAcctNum = 4610
		WAIT WINDOW "This is a California PT Upload" TIMEOUT 1
		ArchiveFile = STRTRAN(ArchiveFile,"nanjing-nj","nanjing")
		SELECT 0
		USE F:\3pl\DATA\mailmaster ALIAS mm
		LOCATE FOR mm.edi_type = "940" AND mm.accountid = nAcctNum AND mm.office = cOffice
		tsendto = IIF(mm.use_alt,sendtoalt,sendto)
		tcc = IIF(mm.use_alt,ccalt,cc)
		USE IN mm
		xReturn = "XX"
		DO m:\dev\prg\wf_alt WITH cOffice,nAcctNum
		cUseFolder = UPPER(TRIM(xReturn))
		SELECT x856
	ENDIF
ELSE
	WAIT WINDOW "PT Will Upload Based on Intake Folder" TIMEOUT 1
ENDIF

LOCATE
LOCATE FOR TRIM(x856.segment) = "GS" AND TRIM(x856.f1) = "FA"
IF FOUND()
	WAIT WINDOW "File processed is FA (997)...returning" TIMEOUT 2
	l997 = .T.
	RETURN
ENDIF

COUNT FOR TRIM(x856.segment) = "ST" TO nPTQty
cSegCnt = ALLTRIM(STR(nPTQty))
lNJPick = .F.  && Flag for NJ Pick to Prepack

LOCATE
m.shipins =""

SELECT x856
LOCATE
LOCATE FOR TRIM(x856.segment) = "N9" AND TRIM(x856.f1) = "98" && Changed 2.13.2007 per Howard
IF FOUND()
	IF INLIST(TRIM(x856.f2),"PRE","PIK","PRR")
		m.shipins = "ITEMTYPE*"+TRIM(x856.f2)  && Added 8/13/15  TM to capture item type for labeling as per Maria E
		IF INLIST(TRIM(x856.f2),"PRE","PRR")
			IF cOffice = "I"
				lNJPick = .F.
				lPrepack = .T.
				nAcctNum = 4694
			ENDIF
			lPrepackRep = IIF(TRIM(x856.f2)="PRR",.T.,.F.)
		ENDIF
	ENDIF
ENDIF
LOCATE

IF nAcctNum = 4694
	IF cOffice = "I"
		WAIT WINDOW "This is a NJ PRE/PRR (Prepack) order..." TIMEOUT 3
	ELSE
		WAIT WINDOW "Pack TYPE(s) will be determined during processing..." TIMEOUT 1
	ENDIF
ENDIF

IF lTestRun
	WAIT WINDOW "Now in 940 Breakdown"+CHR(13)+"RUNNING IN TEST MODE..." TIMEOUT 1
	m.ship_via = "WILL CALL CUST"
ELSE
	WAIT WINDOW "Now in 940 Breakdown" TIMEOUT 2
ENDIF
WAIT "There are "+cSegCnt+" P/Ts in this file" WINDOW TIMEOUT 1

SELECT x856
LOCATE

*!* Constants
m.acct_name = cCustname && Set in start prg
m.acct_num = nAcctNum
m.careof = " "
m.sf_addr1 = "SUITE# 800"
m.sf_addr2 = "15851 SW 41ST STREET"
m.sf_csz = "DAVIE, FL 33331"
m.printed = .F.
m.print_by = ""
m.printdate = DATE()
nUCCSuffix = 1
nSCCSuffix = 1
cCHKDigit = ""
*cUCCNumber = ""
cSCCNumber = ""
m.color=""

*!* End constants

*!*	IF !USED("uploaddet")
*!*		IF lTestRun
*!*			USE F:\ediwhse\test\uploaddet IN 0 ALIAS uploaddet
*!*		ELSE
*!*			USE F:\ediwhse\DATA\uploaddet IN 0 SHARED ALIAS uploaddet
*!*		ENDIF
*!*	ENDIF

SELECT xpt
DELETE ALL
SELECT xptdet
DELETE ALL

STORE 0 TO pt_total_eaches
STORE 0 TO pt_total_cartons

STORE 0 TO nCtnCount

SELECT x856
SET FILTER TO
GOTO TOP

IF lTesting
*	assert .f.
ENDIF

DO WHILE !EOF("x856")
*	WAIT WINDOW "NOW IN INITAL HEADER BREAKDOWN" NOWAIT
	cSegment = TRIM(x856.segment)
	cCode = TRIM(x856.f1)

	IF TRIM(x856.segment) = "ISA"
		m.isa_num = ALLTRIM(x856.f13)
		cISA_Num = ALLTRIM(x856.f13)
		SELECT x856
		SKIP
		LOOP
	ENDIF

	IF TRIM(x856.segment) = "GS"
		SELECT x856
		SKIP
		LOOP
	ENDIF

	IF TRIM(x856.segment) = "ST" AND TRIM(x856.f1) = "940"
		SELECT x856

		STORE RECNO() TO nSTRec
		SCAN FOR RECNO()>nSTRec
			m.goh = .F.
			IF TRIM(x856.segment)="N9" AND TRIM(x856.f1)="98"
				IF TRIM(x856.f2)="PIK" && AND cOffice = "C"
					lPick = .T.
					lPrepack = .F.
					lPrepackRep = .F.
					m.goh = .T.
					m.acct_num = 4610
					STORE m.acct_num TO nAcctNum
				ELSE  && Now includes PRR
					lPrepackRep = IIF(TRIM(x856.f2)="PRR",.T.,.F.)
					lPick = .F.
					lPrepack = .T.
					m.goh = .F.
					m.acct_num = 4694
					m.office = "Y"
					m.mod = "Y"
					STORE m.acct_num TO nAcctNum
				ENDIF
				EXIT
			ENDIF
		ENDSCAN
		GO nSTRec
		SKIP
		LOOP
	ENDIF

	IF TRIM(x856.segment) = "W05"  && new shipment
		cShip_ref = ALLTRIM(x856.f2)
		lDoInsert = .F.
		cPrepackNote = IIF(lPrepackRep,"PREPACK-REP","PREPACK")
		cWinString = "Now processing "+IIF(lPick,"PICKPACK",cPrepackNote)+" PT# "+ cShip_ref
		WAIT WINDOW AT 10,10 "Now processing PT# "+ cShip_ref NOWAIT NOCLEAR
		SELECT xpt
		nDetCnt = 0
		m.pack = "1"
		APPEND BLANK
		STORE 0 TO pt_total_eaches
		STORE 0 TO pt_total_cartons
		ptctr = ptctr +1
		REPLACE xpt.ptid WITH ptctr IN xpt
		REPLACE xpt.goh WITH m.goh IN xpt
		STORE ptctr TO m.ptid
		REPLACE xpt.accountid  WITH m.acct_num IN xpt
		REPLACE xpt.ptdate     WITH DATE() IN xpt
		REPLACE xpt.ship_ref   WITH cShip_ref IN xpt  && Pick Ticket
		REPLACE xpt.shipins    WITH m.shipins
		REPLACE xpt.shipins WITH IIF(EMPTY(xpt.shipins),"FILENAME*"+cFilename,xpt.shipins+CHR(13)+"FILENAME*"+cFilename) IN xpt
		IF lPrepack OR lNJPick
			cPrepackNote = IIF(lPrepackRep,"PREPACK-REP","PREPACK")
			REPLACE xpt.shipins WITH IIF(EMPTY(xpt.shipins),cPrepackNote,xpt.shipins+CHR(13)+cPrepackNote) IN xpt
		ENDIF
		IF lNJPick
			REPLACE xpt.shipins WITH IIF(EMPTY(xpt.shipins),"NJPICK",xpt.shipins+CHR(13)+"NJPICK") IN xpt
		ENDIF

		m.pt = ALLTRIM(x856.f2)
		IF lTestRun
			REPLACE xpt.cacctnum   WITH "X999" IN xpt
		ENDIF
		REPLACE xpt.cnee_ref WITH ALLTRIM(x856.f3) IN xpt
		SELECT x856
		SKIP
		LOOP
	ENDIF

	IF TRIM(x856.segment) = "N1" AND TRIM(x856.f1) = "SF" && Ship-for data
		SELECT xpt
		REPLACE xpt.shipfor WITH ALLTRIM(x856.f2) IN xpt
		REPLACE xpt.duns WITH ALLTRIM(x856.f4) IN xpt
	ENDIF

	IF TRIM(x856.segment) = "N1" AND TRIM(x856.f1) = "ST" && Ship-to data
		SELECT xpt
		cConsignee = UPPER(TRIM(x856.f2))
		IF "PENNEY"$cConsignee
			lJCPenney = .T.
			cConsignee = "JC PENNEY CO"
			IF !USED('PENNEY_SIZES')
				USE F:\3pl\DATA\PENNEY_SIZES ORDER TAG SIZE IN 0
			ENDIF
		ENDIF
		REPLACE xpt.NAME      WITH cConsignee
		m.st_name = TRIM(x856.f2)
		REPLACE xpt.consignee WITH cConsignee

		cStoreNum = TRIM(x856.f4)
		cSTStoreNum = TRIM(x856.f4)
*		ASSERT .f. MESSAGE "At store number conversion"
		IF LEN(cStoreNum)>5
			REPLACE xpt.storenum  WITH VAL(RIGHT(cStoreNum,5))
		ELSE
			REPLACE xpt.storenum  WITH VAL(cStoreNum)
		ENDIF
		REPLACE xpt.dcnum  WITH cStoreNum
		REPLACE xpt.shipins WITH IIF(EMPTY(xpt.shipins),"STORENUM*"+cStoreNum,xpt.shipins+CHR(13)+"STORENUM*"+cStoreNum)

		SKIP 1 IN x856
		IF TRIM(x856.segment) = "N3"  && address info
			m.st_addr1 = UPPER(TRIM(x856.f1))
			IF LEN(m.st_addr1)>20
				cSt_addr1ext = SUBSTR(m.st_addr1,21)
				REPLACE xpt.shipins WITH ;
					IIF(EMPTY(xpt.shipins),"ST_ADDR1EXT*"+cSt_addr1ext,;
					xpt.shipins+CHR(13)+"ST_ADDR1EXT*"+cSt_addr1ext)
				m.st_addr1 = LEFT(m.st_addr1,20)
			ENDIF
			m.st_addr2 = UPPER(TRIM(x856.f2))
			IF LEN(m.st_addr2)>20
				cSt_addr2ext = SUBSTR(m.st_addr2,21)
				REPLACE xpt.shipins WITH ;
					IIF(EMPTY(xpt.shipins),"ST_ADDR2EXT*"+cSt_addr2ext,;
					xpt.shipins+CHR(13)+"ST_ADDR2EXT*"+cSt_addr2ext)
				m.st_addr2 = LEFT(m.st_addr2,20)
			ENDIF
			REPLACE xpt.address  WITH m.st_addr1
			REPLACE xpt.address2 WITH m.st_addr2
		ENDIF
		SKIP 1 IN x856
		IF TRIM(x856.segment) = "N3"  && 2nd line address info
			m.st_addr2 = UPPER(TRIM(x856.f1))
			IF LEN(m.st_addr2)>20
				cSt_addr2ext = SUBSTR(m.st_addr2,21)
				REPLACE xpt.shipins WITH ;
					IIF(EMPTY(xpt.shipins),"ST_ADDR2EXT*"+cSt_addr2ext,;
					xpt.shipins+CHR(13)+"ST_ADDR2EXT*"+cSt_addr2ext)
				m.st_addr2 = LEFT(m.st_addr2,20)
			ENDIF
			SKIP 1 IN x856
		ENDIF

		IF TRIM(x856.segment) = "N4"  && address info
			m.zipbar = TRIM(x856.f3)
			cZipcode = LEFT(m.zipbar,5)
			cCity = LEFT(UPPER(TRIM(x856.f1)),20)
			cState = UPPER(TRIM(x856.f2))
			REPLACE xpt.shipins WITH ;
				IIF(EMPTY(xpt.shipins),;
				"CITY*"+cCity+CHR(13)+"STATE*"+cState+CHR(13)+"ZIPCODE*"+m.zipbar,;
				xpt.shipins+CHR(13)+"CITY*"+cCity+CHR(13)+"STATE*"+cState+CHR(13)+"ZIPCODE*"+m.zipbar)
			m.st_csz = cCity+", "+cState+" "+cZipcode
			REPLACE xpt.csz WITH m.st_csz
		ENDIF
		SELECT x856
		SKIP
		LOOP
	ENDIF

	IF TRIM(x856.segment) = "N1" AND INLIST(TRIM(x856.f1),"Z7","BY") && Ship-for data - this is when 940 is DSDC
		SELECT xpt
		REPLACE xpt.shipfor WITH ALLTRIM(x856.f2)
		cStoreNum = TRIM(x856.f4)
		IF cStoreNum # cSTStoreNum
			cNoBulk = "WHSEINST*>>NOT<< BULK ORDER"
			REPLACE xpt.shipins WITH IIF(EMPTY(xpt.shipins),cNoBulk,xpt.shipins+CHR(13)+cNoBulk)
		ENDIF
		REPLACE xpt.sforstore  WITH ALLTRIM(cStoreNum)
		SKIP 1 IN x856
		IF TRIM(x856.segment) = "N3"  && address info
			m.sforaddr1 = UPPER(TRIM(x856.f1))
			m.sforaddr2 = UPPER(TRIM(x856.f2))
			REPLACE xpt.sforaddr1 WITH m.sforaddr1
			REPLACE xpt.sforaddr2 WITH m.sforaddr2
		ELSE
			LOOP
		ENDIF
		SKIP 1 IN x856
		IF TRIM(x856.segment) = "N3"  && address info
			m.sforaddr2 = UPPER(TRIM(x856.f1))
			REPLACE xpt.sforaddr2 WITH m.sforaddr2
			SKIP 1 IN x856
		ENDIF
		IF TRIM(x856.segment) = "N4"  && address info
			m.sforcsz = UPPER(TRIM(x856.f1))+", "+UPPER(TRIM(x856.f2))+" "+TRIM(x856.f3)
			REPLACE xpt.sforcsz WITH m.sforcsz
		ENDIF
		SELECT x856
		SKIP
		LOOP
	ENDIF

	IF TRIM(x856.segment) = "N9" AND TRIM(x856.f1) = "DP"  && customer dept
		m.dept = ALLTRIM(x856.f2)
		REPLACE xpt.dept WITH m.dept
		SELECT x856
		SKIP
		LOOP
	ENDIF

	IF TRIM(x856.segment) = "N9" AND TRIM(x856.f1) = "MR" && Merchandise Type Code
		REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"MR*"+ALLTRIM(x856.f2)
*		REPLACE xpt.cacctnum WITH ALLTRIM(x856.f2)
		IF TRIM(x856.f2) = "0073"
			lDSDC = .T.
			REPLACE xpt.shipins WITH IIF(EMPTY(xpt.shipins),"DSDC",xpt.shipins+CHR(13)+"DSDC")
		ELSE
			lDSDC = .F.
		ENDIF
		SELECT x856
		SKIP
		LOOP
	ENDIF

	IF TRIM(x856.segment) = "N9" AND TRIM(x856.f1) = "IA" && Internal Cust. Acct. ID
*		WAIT WINDOW "In N9-IA segment" TIMEOUT 2
		cVendNum =ALLTRIM(x856.f2)
		REPLACE xpt.vendor_num WITH cVendNum IN xpt
		IF LEFT(cVendNum,3)="10-"
			REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"ADNY"
			lADNY = .T.
		ENDIF
		IF ALLTRIM(cVendNum) = "WALCOM340"
			REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"SPECIAL*WMCOM"
		ENDIF
		SELECT x856
		SKIP
		LOOP
	ENDIF

	IF TRIM(x856.segment) = "G62" AND (TRIM(x856.f1) == "37" OR TRIM(x856.f1) == "10") && Start date
		lcDate = ALLTRIM(x856.f2)
		lxDate = SUBSTR(lcDate,5,2)+"/"+SUBSTR(lcDate,7,2)+"/"+LEFT(lcDate,4)
		ldDate = CTOD(lxDate)
		REPLACE xpt.START WITH ldDate IN xpt
		SELECT x856
		SKIP
		LOOP
	ENDIF

	IF TRIM(x856.segment) = "G62" AND (TRIM(x856.f1) == "38"  OR TRIM(x856.f1) == "01") && Cancel date
		lcDate = ALLTRIM(x856.f2)
		lxDate = SUBSTR(lcDate,5,2)+"/"+SUBSTR(lcDate,7,2)+"/"+LEFT(lcDate,4)
		ldDate = CTOD(lxDate)
		REPLACE xpt.CANCEL WITH ldDate IN xpt
		SELECT x856
		SKIP
		LOOP
	ENDIF

	IF TRIM(x856.segment) = "W66"  && Shipper Information
		cShipcode = UPPER(ALLTRIM(x856.f2))
		cShip_via = UPPER(ALLTRIM(x856.f5))
		IF "CARSON II"$cShip_via
			m.office = "Y"
			m.mod = "Y"
		ENDIF
		cSCAC = ALLTRIM(x856.f10)
		REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"SHIPCODE*"+cShipcode IN xpt
		REPLACE xpt.scac WITH cSCAC IN xpt
		REPLACE xpt.ship_via WITH cShip_via IN xpt
		SELECT x856
		SKIP
		LOOP
	ENDIF

	IF TRIM(x856.segment) = "LX" && start of PT detail, stay here until this PT is complete
*		ASSERT .F. MESSAGE "At initial LX Segment...debug"
		SELECT xptdet
		CALCULATE MAX(ptdetid) TO m.ptdetid
		m.ptdetid = m.ptdetid + 1
		APPEND BLANK
		cPrintStuff = ""
		REPLACE xptdet.ptid      WITH ptctr IN xptdet
		REPLACE xptdet.ptdetid   WITH m.ptdetid IN xptdet
		REPLACE xptdet.accountid WITH xpt.accountid IN xptdet
		SCATTER MEMVAR FIELDS ptid,accountid

		SELECT xptdet
		SKIP 1 IN x856

		llStoredtheStorenum = .F.

		DO WHILE ALLTRIM(x856.segment) != "SE"
			cSegment = TRIM(x856.segment)
			cCode = TRIM(x856.f1)
			IF TRIM(x856.segment) = "W01" && Destination Quantity
				lOldStyle = .T.
				IF lPrepack
					nDetCnt = 0
					lDoInsert = .F.
					m.color = ""
				ENDIF
				m.itemnum = ""
				SELECT xptdet
				STORE ALLTRIM(x856.f1) TO m.casepack,ctotqty
				m.totqty = INT(VAL(ctotqty))

				IF lNJPick
					m.totqty = (m.totqty/12)
				ENDIF

				REPLACE xptdet.printstuff WITH "FILENAME*"+cFilename IN xptdet
				IF !("UNITSTYPE*"$xptdet.printstuff)
					REPLACE xptdet.printstuff WITH xptdet.printstuff+CHR(13)+"UNITSTYPE*"+ALLTRIM(x856.f2) IN xptdet
				ENDIF
				STORE INT(VAL(m.casepack)) TO nCasePack
				STORE ALLTRIM(x856.f5) TO m.custsku
				m.size_scale = ""
				IF "*ONL"$m.custsku
					IF lPick OR lJCPenney
						REPLACE xptdet.printstuff WITH ;
							xptdet.printstuff+CHR(13)+"ADD*ONLY"
*						m.size_scale = "ADD*ONLY"
					ENDIF
					cDropStr = "*ONL"
					IF "*ONLY"$m.custsku
						cDropStr = "*ONLY"
					ENDIF
					m.custsku = STRTRAN(m.custsku,cDropStr,"")
				ENDIF
				REPLACE xptdet.custsku WITH m.custsku IN xptdet
				IF !EMPTY(m.size_scale)
					REPLACE xptdet.size_scale WITH m.size_scale IN xptdet
				ENDIF

				IF lPrepack
					m.color = ""
					lDoInsert = .T.
				ENDIF

				m.upc = ""
				STORE ALLTRIM(x856.f7) TO m.upc
				REPLACE xptdet.upc WITH m.upc IN xptdet
				STORE ALLTRIM(x856.f4) TO cW0104
				STORE ALLTRIM(x856.f6) TO cW0106
				STORE ALLTRIM(x856.f15) TO cW0115

				STORE LEFT(m.upc,5) TO cMfrUPC

				IF !("W0104"$xptdet.printstuff)
					REPLACE xptdet.printstuff WITH xptdet.printstuff+CHR(13)+"W0104*"+cW0104 IN xptdet
				ENDIF

				IF !("W0106"$xptdet.printstuff)
					REPLACE xptdet.printstuff WITH xptdet.printstuff+CHR(13)+"W0106*"+cW0106 IN xptdet
				ENDIF

				IF !("W0115"$xptdet.printstuff)
					REPLACE xptdet.printstuff WITH xptdet.printstuff+CHR(13)+"W0115*"+cW0115 IN xptdet
				ENDIF

				IF lJCPenney
					IF ALLTRIM(x856.f4) = "IN"
						STORE ALLTRIM(x856.f5) TO m.itemnum
					ENDIF
				ENDIF

				IF cOffice = "I" AND !lPrepack
					IF ALLTRIM(x856.f4) = "IN"
						STORE ALLTRIM(x856.f5) TO m.style
					ENDIF
					IF ALLTRIM(x856.f15) = "VN"
						IF lNJPick
							STORE ALLTRIM(x856.f16) TO m.id
							REPLACE xptdet.printstuff WITH xptdet.printstuff+CHR(13)+;
								"ORIG_NJID*"+m.id IN xptdet
						ENDIF
						STORE ALLTRIM(x856.f16) TO m.shipstyle
					ENDIF
				ELSE
					IF ALLTRIM(x856.f6) = "VN" AND EMPTY(x856.f15)
						STORE ALLTRIM(x856.f7) TO m.style
						STORE m.style TO m.shipstyle
						cStylecheck = PADR(ALLTRIM(x856.f7),20)
					ELSE
						IF ALLTRIM(x856.f15) = "VN"
							STORE ALLTRIM(x856.f16) TO m.style
							STORE m.style TO m.shipstyle
							cStylecheck = PADR(ALLTRIM(x856.f16),20)
						ENDIF
					ENDIF
				ENDIF

				IF cOffice = "I"
					IF ALLTRIM(x856.f4) = "IN"
						STORE ALLTRIM(x856.f5) TO m.id
					ENDIF
				ENDIF

				IF LEFT(m.style,3) = "FGL" AND cOffice = "I"
*					m.id = " "
				ENDIF

				IF !EMPTY(cStylecheck)
					USE F:\3pl\DATA\new_nanjing IN 0 ALIAS new_nanjing
					IF SEEK(cStylecheck,'new_nanjing','style')
						lOldStyle = .F.
					ENDIF
					USE IN new_nanjing
				ENDIF


				IF lPrepack
					IF ALLTRIM(x856.f6) = "VN" AND EMPTY(x856.f15)
						STORE ALLTRIM(x856.f7) TO m.style
						STORE m.style TO m.shipstyle
					ELSE
						IF ALLTRIM(x856.f15) = "VN"
							STORE ALLTRIM(x856.f16) TO m.style
							STORE m.style TO m.shipstyle
						ENDIF
					ENDIF
				ENDIF

				SELECT xptdet
				IF lJCPenney
					IF "*ONL"$m.style
						cDropStr = "*ONL"
						IF "*ONLY"$m.style
							cDropStr = "*ONLY"
						ENDIF
						m.style = STRTRAN(m.style,cDropStr,"")
					ENDIF
				ENDIF
				REPLACE xptdet.STYLE WITH m.style IN xptdet

				IF !"ORIG_STYLE"$xptdet.printstuff
					IF lNJPick
						REPLACE xptdet.printstuff WITH xptdet.printstuff+CHR(13)+;
							"ORIG_STYLE*"+m.id IN xptdet
					ENDIF
				ENDIF

				IF ATCLINE("SCCNUMBER",xptdet.printstuff)=0 AND lPrepack
					=scc14generate()
					IF EMPTY(xptdet.printstuff)
						REPLACE xptdet.printstuff WITH "SCCNUMBER*"+TRIM(m.sccnumber)+CHR(13)+"CHKDIGIT*"+TRIM(m.chkdigit) IN xptdet
					ELSE
						IF !("SCCNUMBER*"$xptdet.printstuff)
							REPLACE xptdet.printstuff WITH xptdet.printstuff+;
								CHR(13)+"SCCNUMBER*"+TRIM(m.sccnumber)+CHR(13)+"CHKDIGIT*"+TRIM(m.chkdigit) IN xptdet
						ENDIF
					ENDIF
				ENDIF
				SCATTER FIELDS ptid,accountid MEMVAR

*				ASSERT .F. MESSAGE "At STYLE/COLOR...debug"
				SKIP IN x856
				IF TRIM(x856.segment) = "N9" AND TRIM(x856.f1) = "VCL"
					cColor = ALLTRIM(x856.f2)
					IF LEN(cColor)>1
						SELECT xptdet
						IF !"ORIG_COLOR"$xptdet.printstuff
							REPLACE xptdet.printstuff WITH xptdet.printstuff+CHR(13)+;
								"ORIG_COLOR*"+cColor IN xptdet
						ENDIF
					ELSE
						cColor = ""
					ENDIF
					SKIP 1 IN x856

					IF TRIM(x856.segment) = "N9" AND TRIM(x856.f1) = "VSZ"
						cID = ALLTRIM(x856.f2)
						SELECT xptdet
						IF !"ORIG_ID"$xptdet.printstuff
							REPLACE xptdet.printstuff WITH xptdet.printstuff+CHR(13)+;
								"ORIG_ID*"+cID IN xptdet
						ENDIF
						IF !lNJPick
							IF EMPTY(m.upc)
								IF lJCPenney AND !EMPTY(m.itemnum) && AND DATE() = {^2007-03-29}
									=sizesub(m.id)
									m.upc = m.dept+m.itemnum+cSizenum
									SELECT xptdet
									REPLACE xptdet.upc WITH m.upc IN xptdet
								ENDIF
							ENDIF
						ENDIF

					SKIP 1 IN x856
					ENDIF

						*!* Added this to correctly calculate the unit totals for the daily shipping report
					*!* per Jason @ Nanjing, 01.17.2018, Joe
					IF TRIM(x856.segment) = "N9" AND TRIM(x856.f1) = "98"
						REPLACE xptdet.printstuff WITH xptdet.printstuff+CHR(13)+"ITEMTYPE*"+TRIM(x856.f2) IN xptdet
					ENDIF
					*!* End added code
					
					lDoInsert = .T.
				ENDIF

				IF lDoInsert
					lDoInsert = .F.
					IF nDetCnt = 0
						nDetCnt = 1
						IF !lNJPick
							IF lPick
								REPLACE xptdet.units WITH .T. IN xptdet
								REPLACE xptdet.PACK WITH "1" IN xptdet
								REPLACE xptdet.casepack WITH 1 IN xptdet
								REPLACE xptdet.totqty WITH nCasePack IN xptdet
								REPLACE xptdet.origqty WITH xptdet.totqty IN xptdet
								nCtnCount = nCtnCount + nCasePack
							ELSE
								REPLACE xptdet.units WITH .F. IN xptdet
								REPLACE xptdet.PACK WITH "12" IN xptdet
								REPLACE xptdet.casepack WITH 12 IN xptdet
								REPLACE xptdet.totqty WITH m.totqty IN xptdet
								IF lNJPick
									REPLACE xptdet.ID WITH m.id IN xptdet
									nCtnCount = nCtnCount + m.totqty
								ENDIF
								REPLACE xptdet.origqty WITH xptdet.totqty IN xptdet
							ENDIF
						ENDIF
						SELECT xptdet
						GATHER MEMVAR FIELDS ptid,size_scale

						IF !("COLORDESC*"$xptdet.printstuff)
							REPLACE xptdet.printstuff WITH ;
								xptdet.printstuff+CHR(13)+"COLORDESC*"+cColor IN xptdet
						ENDIF

						IF !"ORIG_STYLE"$xptdet.printstuff
							IF lNJPick
								REPLACE xptdet.printstuff WITH xptdet.printstuff+CHR(13)+;
									"ORIG_STYLE*"+cID IN xptdet
							ELSE
								REPLACE xptdet.printstuff WITH xptdet.printstuff+CHR(13)+;
									"ORIG_STYLE*"+m.style IN xptdet
							ENDIF
						ENDIF

						SELECT xptdet
*					BROW
					ELSE
						IF lPick
							locstr = "xptdet.STYLE = m.style and xptdet.upc = m.upc ;
							and xptdet.custsku = m.custsku AND xptdet.ptid = m.ptid"
						ELSE
							locstr = "xptdet.STYLE = m.style ;
							and xptdet.custsku = m.custsku AND xptdet.ptid = m.ptid"
						ENDIF

						LOCATE FOR &locstr
						IF lPick
* ASSERT .F. MESSAGE "At locate for style/color/size"
						ENDIF

						IF FOUND()
							IF !lNJPick
								IF lPick
									REPLACE xptdet.ptid WITH m.ptid IN xptdet
									REPLACE xptdet.accountid WITH m.accountid IN xptdet
									REPLACE xptdet.units WITH .T. IN xptdet
									REPLACE xptdet.PACK WITH "1" IN xptdet
									REPLACE xptdet.casepack WITH 1 IN xptdet
									REPLACE xptdet.totqty  WITH (xptdet.totqty + nCasePack) IN xptdet
									REPLACE xptdet.origqty WITH xptdet.totqty IN xptdet
								ELSE
									REPLACE xptdet.PACK WITH "12"
									REPLACE xptdet.casepack WITH 12
									REPLACE xptdet.totqty  WITH (xptdet.totqty + m.totqty)
									IF lNJPick
										REPLACE xptdet.ID WITH m.id IN xptdet
										nCtnCount = nCtnCount + m.totqty
									ENDIF
									REPLACE xptdet.origqty WITH xptdet.totqty
								ENDIF
							ENDIF
							SELECT xptdet
*						BROW
						ELSE
							APPEND BLANK
							m.ptdetid = m.ptdetid + 1
							GATHER MEMVAR FIELDS ptid,accountid,STYLE,ptdetid,casepack
							GATHER MEMVAR FIELDS COLOR,PACK,ID,upc,custsku,size_scale,shipstyle
							IF lPrepack
								=scc14generate()
							ENDIF

							IF !("COLORDESC*"$xptdet.printstuff)
								REPLACE xptdet.printstuff WITH ;
									xptdet.printstuff+CHR(13)+"COLORDESC*"+cColor
							ENDIF

							IF !"ORIG_STYLE"$xptdet.printstuff
								REPLACE xptdet.printstuff WITH xptdet.printstuff+CHR(13)+;
									"ORIG_STYLE*"+m.style IN xptdet
							ENDIF

							IF !"ORIG_COLOR"$xptdet.printstuff
								REPLACE xptdet.printstuff WITH xptdet.printstuff+CHR(13)+;
									"ORIG_COLOR*"+cColor IN xptdet
							ENDIF

							IF !"ORIG_ID"$xptdet.printstuff
								REPLACE xptdet.printstuff WITH xptdet.printstuff+CHR(13)+;
									"ORIG_ID*"+cID IN xptdet
							ENDIF

							IF EMPTY(xptdet.printstuff)
								REPLACE xptdet.printstuff WITH "PRINTSTYLE*"+M.style
							ELSE
								IF !("PRINTSTYLE*"$xptdet.printstuff)
									REPLACE xptdet.printstuff WITH xptdet.printstuff+CHR(13)+"PRINTSTYLE*"+M.style
								ENDIF
							ENDIF

							IF EMPTY(xptdet.printstuff)
								REPLACE xptdet.printstuff WITH "UNITSTYPE*"+ALLTRIM(x856.f2)
							ELSE
								IF !("UNITSTYPE*"$xptdet.printstuff)
									REPLACE xptdet.printstuff WITH xptdet.printstuff+CHR(13)+"UNITSTYPE*"+ALLTRIM(x856.f2)
								ENDIF
							ENDIF

							IF !("SCCNUMBER"$xptdet.printstuff) AND lPrepack
								IF EMPTY(xptdet.printstuff)
									REPLACE xptdet.printstuff WITH "SCCNUMBER*"+TRIM(m.sccnumber)+CHR(13)+"CHKDIGIT*"+TRIM(m.chkdigit) IN xptdet
								ELSE
									REPLACE xptdet.printstuff WITH xptdet.printstuff+;
										CHR(13)+"SCCNUMBER*"+TRIM(m.sccnumber)+CHR(13)+"CHKDIGIT*"+TRIM(m.chkdigit) IN xptdet
								ENDIF
							ENDIF

							IF !("W0104"$xptdet.printstuff)
								REPLACE xptdet.printstuff WITH xptdet.printstuff+CHR(13)+"W0104*"+cW0104 IN xptdet
							ENDIF

							IF !("W0106"$xptdet.printstuff)
								REPLACE xptdet.printstuff WITH xptdet.printstuff+CHR(13)+"W0106*"+cW0106 IN xptdet
							ENDIF

							IF !("W0115"$xptdet.printstuff)
								REPLACE xptdet.printstuff WITH xptdet.printstuff+CHR(13)+"W0115*"+cW0115 IN xptdet
							ENDIF

							REPLACE xptdet.ptdetid WITH m.ptdetid
							SCATTER FIELDS ptid,ptdetid,accountid MEMVAR
							IF !lNJPick
								IF lPick
									REPLACE xptdet.units WITH .T.
									REPLACE xptdet.casepack   WITH 1
									REPLACE xptdet.totqty WITH m.totqty
									REPLACE xptdet.origqty WITH xptdet.totqty
									nCtnCount = nCtnCount + nCasePack
								ELSE
									REPLACE xptdet.units WITH .F.
									REPLACE xptdet.PACK WITH "12"
									REPLACE xptdet.casepack WITH 12
									REPLACE xptdet.totqty WITH m.totqty
									IF lNJPick
										REPLACE xptdet.ID WITH m.id IN xptdet
										nCtnCount = nCtnCount + m.totqty
									ENDIF
									REPLACE xptdet.origqty WITH xptdet.totqty
								ENDIF
							ENDIF
							SELECT xptdet
*						BROW
						ENDIF
					ENDIF
				ENDIF
				SKIP 1 IN x856
			ENDIF

			IF TRIM(x856.segment) = "W20"
				IF lPrepack
					REPLACE xptdet.PACK WITH ALLTRIM(x856.f1) IN xptdet
					REPLACE xptdet.casepack WITH INT(VAL(ALLTRIM(x856.f1))) IN xptdet
				ENDIF
			ENDIF

			IF ALLTRIM(x856.segment)="LX" && start of new PT detail, use same header
				SELECT xptdet
				APPEND BLANK
				nDetCnt = 0
				m.ptdetid = m.ptdetid+1
				REPLACE xptdet.ptid      WITH ptctr
				REPLACE xptdet.ptdetid   WITH m.ptdetid
				REPLACE xptdet.accountid WITH xpt.accountid
				SKIP 1 IN x856
				LOOP
			ENDIF

			IF TRIM(x856.segment) = "W76"  && Total carton quantity
				IF !lNJPick
					nCtnCount = INT(VAL(ALLTRIM(x856.f1)))
				ENDIF
				IF lPrepack
					REPLACE xpt.qty WITH INT(VAL(ALLTRIM(x856.f1))) IN xpt && ALLTRIM(STR(nCtnCount))
				ELSE
					REPLACE xpt.qty WITH nCtnCount IN xpt
				ENDIF

				REPLACE xpt.origqty WITH xpt.qty IN xpt
				IF lPick
					nCtnCount = 0
				ENDIF
				SELECT xptdet
				nCtnCount = 0
			ENDIF

			SELECT xptdet
			SKIP 1 IN x856
		ENDDO
		SELECT xptdet
	ENDIF

	SELECT x856
	IF !EOF()
		SKIP 1 IN x856
	ENDIF
ENDDO

DO m:\dev\prg\setuppercase940

IF lBrowfiles
	SELECT xpt
	LOCATE
	BROWSE
*	COPY TO h:\xpt
	SELECT xptdet
	BROWSE
*	COPY TO h:\xptdet
ENDIF

#IF 0
	IF lTestRun
		bol_stem = "1234567890"
		SELECT xpt
		SELECT ship_ref FROM xpt GROUP BY 1 INTO CURSOR tempxpt
		SELECT tempxpt
		SCAN
			bol_suffix = PADL(TRIM(tempxpt.ship_ref),7,"0")
			SELECT xpt
			LOCATE
			REPLACE ALL xpt.bol_no WITH bol_stem+bol_suffix FOR xpt.ship_ref = tempxpt.ship_ref
		ENDSCAN

		SELECT xpt
		REPLACE ALL xpt.wo_num WITH 656565
		COPY TO h:\xpt_ea
		COPY TO h:\xpt_ea TYPE XL5
		BROWSE
		SELECT xptdet
		COPY TO h:\xptdet_ea
		COPY TO h:\xptdet_ea TYPE XL5
	ENDIF
#ENDIF

WAIT cCustname+" Breakdown Round complete..." WINDOW TIMEOUT 2

SELECT x856
IF USED("PT")
	USE IN pt
ENDIF
IF USED("PTDET")
	USE IN ptdet
ENDIF
WAIT CLEAR
RETURN
**************************************************************************************
PROCEDURE uccgenerate
	cUCCPrefix = "00 0 0123456 "
	cUCCSuffix = PADL(ALLTRIM(STR(nUCCSuffix)),6,"0")
	nUCCSuffix = nUCCSuffix + 1
	cUCCNumber = cUCCPrefix+cUCCSuffix
	cUCCNumber2 = STRTRAN(cUCCNumber," ","")

	nValue1 = 0  && "ODD" Digits
	nValue2 = 0  && "EVEN" Digits
	nValue3 = 0  && SUMMARY Value
	nChkDigit = 0

	FOR I = LEN(TRIM(cUCCNumber2)) TO 1 STEP -2
		nValue1 = nValue1 + INT(VAL(SUBSTR(cUCCNumber2,I,1)))
	ENDFOR
	nValue1 = nValue1 * 3

	FOR I = LEN(TRIM(cUCCNumber2))-1 TO 1 STEP -2
		nValue2 = nValue2 + INT(VAL(SUBSTR(cUCCNumber2,I,1)))
	ENDFOR
	nValue3 = nValue1 + nValue2
	nChkDigit = 10-(nValue3%10)  && <--- The output check digit here

	cCHKDigit = ALLTRIM(STR(nChkDigit))
	m.chkdigit = cCHKDigit
	cUCCNumber = cUCCNumber2 + cCHKDigit
	m.uccnumber = cUCCNumber

ENDPROC

**************************************************************************************
PROCEDURE scc14generate
	STORE "" TO cartonid,cSCCNumber,cCHKDigit

	cPkgIndicator = IIF(lPick,"0","1")
	cSCCPrefix = cPkgIndicator+" "+PADL(LEFT(m.upc,1),2,"0") + SUBSTR(m.upc,2,5)
	cSCCSuffix = RIGHT(m.upc,5)
	cSCCNumber2 = cSCCPrefix+" "+cSCCSuffix

	nValue1 = 0  && "ODD" Digits
	nValue2 = 0  && "EVEN" Digits
	nValue3 = 0  && SUMMARY Value
	nChkDigit = 0

	cSCCNumber = STRTRAN(cSCCNumber2," ","")

	FOR I = LEN(TRIM(cSCCNumber)) TO 1 STEP -2
		nValue1 = nValue1 + INT(VAL(SUBSTR(cSCCNumber,I,1)))
	ENDFOR
	nValue1 = nValue1 * 3

	FOR I = LEN(TRIM(cSCCNumber))-1 TO 1 STEP -2
		nValue2 = nValue2 + INT(VAL(SUBSTR(cSCCNumber,I,1)))
	ENDFOR
	nValue3 = nValue1 + nValue2
	nChkDigit = 10-(nValue3%10)  && <--- The output check digit here

	cCHKDigit = ALLTRIM(STR(nChkDigit))
	cSCCNumber = cSCCNumber + cCHKDigit
	cartonid = cSCCNumber

	STORE TRIM(cSCCNumber) TO m.cartonid
	STORE LEFT(TRIM(cSCCNumber),13) TO m.sccnumber
	STORE TRIM(cCHKDigit) TO m.chkdigit

ENDPROC

**************************************************************************************
PROCEDURE sizesub
	PARAMETERS cSize
	cSize = PADR(cSize,5)
	SELECT PENNEY_SIZES
	IF SEEK(cSize)
		STORE PENNEY_SIZES.sizenum TO cSizenum
	ELSE
		cSizenum =""
		WAIT WINDOW "Size "+cSize+" not found in conversion table" TIMEOUT 2
	ENDIF
	RETURN cSizenum
ENDPROC
