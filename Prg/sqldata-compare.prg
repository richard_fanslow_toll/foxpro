*!* This program cross-checks the record counts for
*!* the SQL LABELS and CARTONS tables by BOL/WO
*!* to address a prior Courtaulds SQL problem

PARAMETERS cUseFolder,cBOL,nWO_Num,cOffice,nAcctNum,cPPName,lSQL3,lMoret
PUBLIC cFileType1,lLabelRecs
SET TALK OFF
SET ASSERTS ON
SET ESCAPE ON
ON ESCAPE CANCEL

*!* nDoVar = 2, will change WO#
*!* nDoVar = 3, will change Carton count
nDoVar = 0
lError = .F.
lLabelRecs = .F.
*ASSERT .F. MESSAGE "At SQL-COMPARE BOL selection"

xsqlexec("select * from outship where accountid = "+TRANSFORM(nAcctNum)+" and bol_no = '"+cBOL+"'","sqlwocomp",,"wh")

IF lTesting
*	BROWSE
ENDIF

cFileOutName1 = "L"+TRIM(cPPName)
cFileOutName2 = "C"+TRIM(cPPName)
nAcct = ALLTRIM(STR(nAcctNum))
cSQL = "tgfnjsql01"

SELECT 0
USE F:\wh\sqlpassword
cSQLPass = ALLTRIM(sqlpassword.PASSWORD)
USE IN sqlpassword

lcDSNLess="driver=SQL Server;server=tgfnjsql01;uid=SA;pwd=&cSQLPass;DATABASE=PICKPACK"

*!*	DO CASE
*!*	  Case gldor =.f.
*!*	    lcDSNLess="driver=SQL Server;server=njsql1;uid=SA;pwd=&cSQLPass;DATABASE=PICKPACK"
*!*		CASE lTestinput AND INLIST(nAcctNum,6182,6026,6137)
*!*			lcDSNLess="driver=SQL Server;server=sql5;uid=SA;pwd=&cSQLPass;DATABASE=PICKPACK"
*!*		CASE (lTestinput AND INLIST(nAcctNum,6416,6438)) OR nAcctNum = 6532
*!*			lcDSNLess="driver=SQL Server;server=sql5;uid=SA;pwd=&cSQLPass;DATABASE=PICKPACK"
*!*		CASE lTesting AND INLIST(nAcctNum,4859,6026)
*!*			lcDSNLess="driver=SQL Server;server=&csql;uid=SA;pwd=&cSQLPass;DATABASE=PICKPACK"
*!*		CASE lTestinput && AND INLIST(nAcctNum,6036,5836,5910,4677,6059)
*!*			lcDSNLess="driver=SQL Server;server=sql3;uid=SA;pwd=&cSQLPass;DATABASE=PICKPACK"
*!*		CASE (BETWEEN(nAcctNum,6303,6306) AND INLIST(cOffice,"N","J"))  && MARC JACOBS
*!*			lcDSNLess="driver=SQL Server;server=sql5;uid=SA;pwd=&cSQLPass;DATABASE=PICKPACK"
*!*		CASE INLIST(cOffice,"N","I") && AND INLIST(nAcctNum,5446,6059,5836)
*!*			lcDSNLess="driver=SQL Server;server=&csql;uid=SA;pwd=&cSQLPass;DATABASE=PICKPACK"
*!*		CASE cOffice = "M"
*!*	*		lcDSNLess="driver=SQL Server;server=10.244.152.137;uid=SA;pwd=&cSQLPass;DATABASE=PICKPACK"
*!*			lcDSNLess="driver=SQL Server;server=&csql;uid=SA;pwd=&cSQLPass;DATABASE=PICKPACK"
*!*		OTHERWISE
*!*			lcDSNLess="driver=SQL Server;server=sql5;uid=SA;pwd=&cSQLPass;DATABASE=PICKPACK"	&& dy 2/18/18
*!*	ENDCASE




nHandle=SQLSTRINGCONNECT(m.lcDSNLess,.T.)
SQLSETPROP(nHandle,"DispLogin",3)
WAIT WINDOW "Now processing "+cPPName+" SQL view...please wait" NOWAIT

IF nHandle=0 && bailout
	WAIT WINDOW "Could not make SQL connection" TIMEOUT 3
	closefiles()
	cRetMsg = "NO SQL CONNECT"
	RETURN cRetMsg
ENDIF

*!* Scans through all WOs within the OUTSHIP BOL#
IF lTesting
*SET STEP ON
ENDIF
SELECT sqlwocomp
LOCATE
SCAN
	nWO_Num1 = sqlwocomp.wo_num
	IF nDoVar = 2 AND lTesting
		nWO_Num = 652885
	ENDIF
	cWo   = ALLTRIM(STR(nWO_Num1))
	IF cBOL = "04907314677000532"
		WAIT WINDOW "At Work Order "+cWo NOWAIT
	ENDIF
	lcQ3 = " &cWo "
	WAIT WINDOW "Now scanning WO# "+cWo NOWAIT

	if usesql()
		xsqlexec("select * from labels where wo_num="+cwo,cFileOutName1,,"pickpack")
		llsuccess=1
	else
		lcQ1=[SELECT * FROM dbo.labels Labels]
		lcQ2 = [ WHERE Labels.wo_num = ]
		lcsql = lcQ1+lcQ2+lcQ3
		llSuccess=SQLEXEC(nHandle,lcsql,cFileOutName1)
	endif
	
	successrun("L")

	IF lError
		IF lLabelRecs
			cRetMsg = "LR"
		ELSE
			cRetMsg = "SQL ERROR-LBL"
		ENDIF
		RETURN cRetMsg
	ENDIF

	if usesql()
		xsqlexec("select * from cartons where wo_num="+cwo,cFileOutName2,,"pickpack")
		llsuccess=1
	else
		lcQ1 = [SELECT * FROM dbo.cartons Cartons]
		lcQ2 = [ WHERE Cartons.wo_num = ]
		lcsql = lcQ1+lcQ2+lcQ3
		llSuccess=SQLEXEC(nHandle,lcsql,cFileOutName2)
	endif
	
	successrun("C")

	IF lError
		cRetMsg = "SQL ERROR-CTN"
		RETURN cRetMsg
	ENDIF

	SELECT &cFileOutName1 && Labels
	COUNT TO nLabels
	cCountLabels = ALLTRIM(STR(nLabels))
	SELECT &cFileOutName2 && Cartons
	COUNT TO nCartons
	IF nDoVar = 3 AND lTesting
		nCartons = nCartons - 5
	ENDIF
	cCountCartons = ALLTRIM(STR(nCartons))

	IF nLabels <> nCartons && Mismatched count
		IF lSQL3 AND (cOffice = "N" AND nAcctNum=5446 OR cOffice = "C" AND lMoret) AND nLabels = 0
			WAIT WINDOW "Correct zero-label COUNT(SQL3 data)" NOWAIT
		ELSE
			errormail(1)
			cRetMsg = "LBL/CTN UNEQUAL, WO# "+cWO
			RETURN cRetMsg
		ENDIF
	ENDIF
ENDSCAN

WAIT WINDOW "Labels and Cartons had equal record counts...continuing" NOWAIT

SQLCANCEL(nHandle)
SQLDISCONNECT(nHandle)
closefiles()

cRetMsg = "OK"
RETURN cRetMsg

**********************
PROCEDURE successrun
	PARAMETERS cFileType1
	cFileWithError = IIF(cFileType1="C","CARTONS","LABELS")
	DO CASE
		CASE INLIST(llSuccess,0,-1)  && no records 0 indicates no records and 1 indicates records found
			WAIT WINDOW "No SQL Connection" TIMEOUT 3
			errormail(2)
			RETURN
		CASE RECCOUNT()=0
			IF lSQL3 AND (cOffice = "C" AND lMoret OR cOffice = "N" AND INLIST(nAcctNum,5446))
				WAIT WINDOW "No records correctly found in LABELS - MORET" NOWAIT
			ELSE
				ASSERT .F. MESSAGE "Zero Record Count flagged in "+cFileWithError+"...debug"
				WAIT WINDOW "No "+cPPName+" records found for this WO# "+ALLTRIM(STR(nWO_Num1)) TIMEOUT 3
				errormail(3)
				RETURN
			ENDIF
		CASE RECCOUNT()>0
			IF lSQL3 AND cOffice = "C" AND lMoret AND cFileType1 = "L"
				WAIT WINDOW cPPName+" LABELS records were found for this WO# "+ALLTRIM(STR(nWO_Num1)) TIMEOUT 3
				lLabelRecs = .T.
				errormail(4)
				RETURN
			ENDIF
	ENDCASE
ENDPROC

**********************
PROCEDURE errormail
	PARAMETERS nType
	lError = .T.
	DO CASE
		CASE nType = 1
			tmessage = "The SQL counts (Labels vs. Cartons) did not match for this BOL."
			tmessage = tmessage + CHR(13) +;
				"Count at WO# "+cWo+": "+cCountLabels+" (Labels) <> "+cCountCartons+" (Cartons)."
			tmessage = tmessage + CHR(13) + "Office: "+cOffice+", Account: "+ALLTRIM(STR(nAcctNum))
		CASE nType = 2
			tmessage = "SQL could not properly connect while processing WO "+cWo
			tmessage = tmessage + CHR(13) + "Office: "+cOffice+", Account: "+ALLTRIM(STR(nAcctNum))
		CASE nType = 3
			tmessage = "No records were found in the SQL data for this BOL at WO# "+cWo
			tmessage = tmessage + CHR(13) + "Office: "+cOffice+", Account: "+ALLTRIM(STR(nAcctNum))
			tmessage = tmessage + CHR(13) + "Error occurred in "+cFileWithError
		OTHERWISE
			tmessage = "Records were found in the MORET SQL LABELS for this BOL at WO# "+cWo
			tmessage = tmessage + CHR(13) + "Office: "+cOffice+", Account: "+ALLTRIM(STR(nAcctNum))
			tmessage = tmessage + CHR(13) + "Error occurred in "+cFileWithError
			tmessage = tmessage + CHR(13) + "THIS ERROR MUST BE CORRECTED IMMEDIATELY."
			stopmoretwo()
	ENDCASE
	IF nType = 2
		tmessage = tmessage + CHR(13) + "Processing stopped at WO# "+cWo
	ENDIF

*assert .f. MESSAGE "At Compare Mailing process"
	tfrom = "TGF EDI Operations <toll-edi-ops@tollgroup.com>"
	tsubject = "SQL ERROR(Labels-Cartons) at BOL#: "+cBOL+", WO# "+ALLTRIM(STR(nWO_Num1))
	IF USED('mm')
		USE IN mm
	ENDIF
	USE F:\3pl\DATA\mailmaster ALIAS mm IN 0
	IF INLIST(nAcctNum,6521,6221)
		LOCATE FOR mm.accountid = 6521 AND mm.taskname = "BSSQLERROR"
	ELSE
		LOCATE FOR mm.taskname = "GENERAL"
	ENDIF
	tsendtosc = IIF(mm.use_alt,mm.sendtoalt,mm.sendto)
	tccsc = IIF(mm.use_alt,mm.ccalt,mm.cc)
	USE IN mm

	tattach = " "
	DO FORM m:\dev\frm\dartmail2 WITH tsendtosc,tfrom,tsubject,tccsc,tattach,tmessage,"A"
	closefiles()
	SQLCANCEL(nHandle)
	SQLDISCONNECT(nHandle)
ENDPROC

**********************
PROCEDURE closefiles
	IF USED(cFileOutName1)
		USE IN &cFileOutName1
	ENDIF
	IF USED(cFileOutName2)
		USE IN &cFileOutName2
	ENDIF
	IF USED('sqlwocomp')
		USE IN sqlwocomp
	ENDIF
	RETURN

**********************
PROCEDURE stopmoretwo
	IF !lTesting
		SELECT edi_trigger
		LOCATE
		REPLACE processed WITH .T.,errorflag WITH .T.,fin_status WITH "RECS IN LABELS" ;
			FOR edi_trigger.wo_num = nWO_Num AND edi_trigger.accountid = nAcctNum
		LOCATE
	ENDIF
ENDPROC
