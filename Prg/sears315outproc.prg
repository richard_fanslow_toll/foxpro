
************************************************************************
* Import 310s and produce 315s for Sears
* Build EXE in F:\UTIL\SEARSEDI\
************************************************************************



RETURN  && DISABLING PER DOUG 2/25/2011



LOCAL loSears315OutProcess, lnError, lcProcessName

DECLARE INTEGER CreateMutex IN Win32API ;
	STRING @lpMutexAttributes, ;
	STRING bInitialOwner, ;
	STRING @lpName

DECLARE INTEGER GetLastError IN Win32API

* if running in EXE check to see if already running
IF _VFP.STARTMODE > 0 THEN
	lcProcessName = "Sears315OutProcess"
	CreateMutex(0,CHR(1),@lcProcessName)
	lnError = GetLastError()
	IF lnError = 183
		WAIT WINDOW TIMEOUT 10 "Sears 315 Out process is already running..."
		RETURN .F.
	ENDIF
ENDIF
utilsetup("SEARS315OUTPROC")

loSears315OutProcess = CREATEOBJECT('Sears315OutProcess')
loSears315OutProcess.MAIN()

schedupdate()

CLOSE DATABASES ALL
RETURN

#DEFINE LOGIT 1
#DEFINE WAITIT 2
#DEFINE NOWAITIT 4
#DEFINE SENDIT 8
#DEFINE CR CHR(13)
#DEFINE LF CHR(10)
#DEFINE CRLF CHR(13) + CHR(10)
#DEFINE RETURN_DATA_MANDATORY .F.
#DEFINE RETURN_DATA_NOT_MANDATORY .T.
#DEFINE CSVFORMAT 6
#DEFINE WRITE_ONLY_UNBUFFERED 11
#DEFINE COMMA ","

DEFINE CLASS Sears315OutProcess AS CUSTOM

	cProcessName = 'Sears315OutProcess'

	lTestMode = .F.  && .F. FOR NORMAL OPERATION, .T. TO SUPPRESS ALL BUT DEVELOPER EMAILS AND CHANGE INPUT TABLE, ETC.

	lAutoYield = .T.

	* date time props
	dToday = DATE()

	cStartTime = TTOC(DATETIME())

	* file/folder properties
	c310InDir = "F:\FTPUSERS\MELDISCO\MIRALOMA\PROD\SIS\AS2DOWNLOAD\MYMAILBOX\"
	*c310OutDir = "F:\FTPUSERS\SEARSEDI\EDIDATAOUT\"
	*c310ArchiveDir = "F:\FTPUSERS\SEARSEDI\RGTIDATAIN\ARCHIVE\"
	c315ArchiveDir = "F:\FTPUSERS\SEARSEDI\EDIDATAOUT\ARCHIVE\"
	c315OutDir = "F:\FTPUSERS\MELDISCO\MIRALOMA\PROD\SIS\AS2UPLOAD\MYMAILBOX\"

	* table properties
	cControlTable = "F:\SEARSEDI\DATA\CONTROLNUM"
	cSearsASNTable = "F:\SEARSEDI\DATA\ASN310"

	* processing properties
	c310SegTerminator = "~" + CHR(13)
	lUpload315s = .T.
	n315RecLength = 200

	*error handling properties
	cThrowDescription = "Unknown error"

	* wait window properties
	nWaitWindowTimeout = 2
	lWaitWindowIsOn = .T.

	* logfile properties
	lLoggingIsOn = .T.
	cLogFile = 'F:\FTPUSERS\SEARSEDI\LOGFILES\Sears315OutProcess_log.txt'

	* INTERNAL email properties
	lSendInternalEmailIsOn = .T.
	cFrom = 'TGF Corporate Communication Department <transload-ops@fmiint.com>'
	cSendTo = 'mbennett@fmiint.com,DougWachs@fmiint.com'
	*cSendTo = 'DougWachs@fmiint.com'
	cCC = ''
	*cCC = 'mbennett@fmiint.com,pgaidis@fmiint.com'
	cSubject = 'Sears 315 Out Process Results for ' + TTOC(DATETIME())
	cAttach = ''
	cBodyText = ''
	cTopBodyText = ''

	cCOMPUTERNAME = ''
	cUSERNAME = ''


	FUNCTION INIT
		IF NOT DODEFAULT()
			RETURN .F.
		ENDIF
		WITH THIS
			*SET RESOURCE OFF
			CLOSE DATABASES ALL
			*DO setenvi
			SET CENTURY ON
			SET DATE YMD
			SET DECIMAL TO 0
			SET TALK OFF
			SET DELETED ON
			SET CONSOLE OFF
			SET EXCLUSIVE OFF
			SET SAFETY OFF
			SET EXACT OFF
			SET STATUS BAR ON
			SET SYSMENU OFF
			SET ENGINEBEHAVIOR 70
			_VFP.AUTOYIELD = .lAutoYield
			.cCOMPUTERNAME = GETENV("COMPUTERNAME")
			.cUSERNAME = GETENV("USERNAME")
			IF .lTestMode THEN
				.cLogFile = 'F:\FTPUSERS\SEARSEDI\LOGFILES\Sears315OutProcess_LOG_TESTMODE.txt'
				.cSendTo = 'mbennett@fmiint.com'
				.cCC = ''
				* file/folder properties
				.c310InDir = "C:\SEARSEDI\MELDISCO\MYMAILBOX\"
				*.c310OutDir = "C:\SEARSEDI\EDIDATAOUT\"
				*.c310ArchiveDir = "C:\SEARSEDI\RGTIDATAIN\ARCHIVE\"
				.c315OutDir = "C:\SEARSEDI\MELDISCO\MYMAILBOX\"
				.c315ArchiveDir = "C:\SEARSEDI\EDIDATAOUT\ARCHIVE\"
				* table properties
				.cSearsASNTable = "C:\SEARSEDI\DATA\ASN310"
			ENDIF
			.lLoggingIsOn = .lLoggingIsOn AND NOT EMPTY(.cLogFile)
		ENDWITH
	ENDFUNC


	FUNCTION DESTROY
		WITH THIS
			IF .lLoggingIsOn  THEN
				SET ALTERNATE OFF
				SET ALTERNATE TO
			ENDIF
		ENDWITH
		DODEFAULT()
	ENDFUNC


	FUNCTION MAIN
		WITH THIS

			LOCAL lnNumberOfErrors, laFiles[1,5], laFiles2[1,5], laFilesSorted[1,6], lc315OutFileRoot
			LOCAL lnNumFiles, lnCurrentFile, lcSourceFile, lcArchiveFile, lcTargetFile, lc315ControlNum
			LOCAL lcEDISourceFile, lcEDIArchiveFile, lcOutstrx, lcDelimChar, llArchived, ln315ControlNum
			LOCAL lnHandle, lcStr, ln315Handle, lc315OutFile, lcHeader, lnNumRecs, lcDetail, lcFooter, ldDateTime
			LOCAL lnNumGroups
			
			lnNumberOfErrors = 0

			TRY

				lnNumGroups = 0

				IF .lLoggingIsOn THEN
					SET ALTERNATE TO (.cLogFile) ADDITIVE
					SET ALTERNATE ON
				ENDIF

				.TrackProgress('=========================================================', LOGIT+SENDIT)
				.TrackProgress('Sears 315 Out Process process started....', LOGIT+SENDIT+NOWAITIT)
				.TrackProgress('COMPUTERNAME = ' + .cCOMPUTERNAME, LOGIT+SENDIT)
				.TrackProgress('USERNAME = ' + .cUSERNAME, LOGIT+SENDIT)
				IF .lTestMode THEN
					.TrackProgress('TEST MODE', LOGIT+SENDIT)
				ENDIF
				.TrackProgress('.cControlTable = ' + .cControlTable, LOGIT+SENDIT)
				.TrackProgress('.cSearsASNTable = ' + .cSearsASNTable, LOGIT+SENDIT)
				.TrackProgress('.c310InDir = ' + .c310InDir, LOGIT+SENDIT)
				.TrackProgress('.c315OutDir = ' + .c315OutDir, LOGIT+SENDIT)
				.TrackProgress('.c315ArchiveDir = ' + .c315ArchiveDir, LOGIT+SENDIT)

				*!*		cControlTable = "F:\SEARSEDI\DATA\CONTROLNUM"
				*!*		cSearsASNTable = "F:\SEARSEDI\DATA\ASN310"
				*!*		c310InDir = "F:\FTPUSERS\SEARSEDI\RGTIDATAIN\"
				*!*		c310OutDir = "F:\FTPUSERS\SEARSEDI\EDIDATAOUT\"
				*!*		c310ArchiveDir = "F:\FTPUSERS\SEARSEDI\RGTIDATAIN\ARCHIVE\"

				****************************************************************************
				* First, import 310 info into ASN310 table
				****************************************************************************

				lnNumFiles = ADIR(laFiles,(.c310InDir + "km_310*.*"))

				IF lnNumFiles > 0 THEN

					* sort file list by date/time
					.SortArrayByDateTime(@laFiles, @laFilesSorted)

					USE (.cSearsASNTable) IN 0 ALIAS asn310
					SELECT * FROM asn310 ;
						INTO CURSOR CURTEMP310 ;
						WHERE .F. READWRITE

					FOR lnCurrentFile = 1 TO lnNumFiles
						lcSourceFile = .c310InDir + laFilesSorted[lnCurrentFile,1]
						*lcArchiveFile = .c310ArchiveDir + laFilesSorted[lnCurrentFile,1]
						*lcTargetFile = .c310OutDir + laFilesSorted[lnCurrentFile,1]

						.TrackProgress("Importing 310 file: " + lcSourceFile,LOGIT+SENDIT+NOWAITIT)

						lnHandle = FOPEN(lcSourceFile)
						DO WHILE !FEOF(lnHandle)
							lcStr = FGETS(lnHandle,600)
							IF ALLTRIM(SUBSTR(lcStr,1,2)) = "FM" THEN
								SELECT CURTEMP310
								APPEND BLANK
								REPLACE CURTEMP310.partner    WITH ALLTRIM(SUBSTR(lcStr,1,2)), ;
									CURTEMP310.hbol       WITH ALLTRIM(SUBSTR(lcStr,169,16)), ;
									CURTEMP310.etd        WITH ALLTRIM(SUBSTR(lcStr,196,8)), ;
									CURTEMP310.scac       WITH ALLTRIM(SUBSTR(lcStr,204,4)), ;
									CURTEMP310.vessel     WITH ALLTRIM(SUBSTR(lcStr,208,35)), ;
									CURTEMP310.voyage     WITH ALLTRIM(SUBSTR(lcStr,243,10)), ;
									CURTEMP310.disdc      WITH ALLTRIM(SUBSTR(lcStr,255,3)), ;
									CURTEMP310.port       WITH ALLTRIM(SUBSTR(lcStr,262,35)), ;
									CURTEMP310.etadc      WITH ALLTRIM(SUBSTR(lcStr,330,8)), ;
									CURTEMP310.vendor     WITH ALLTRIM(SUBSTR(lcStr,356,35)), ;
									CURTEMP310.CONTAINER  WITH ALLTRIM(SUBSTR(lcStr,391,15)), ;
									CURTEMP310.SIZE       WITH ALLTRIM(SUBSTR(lcStr,406,3)), ;
									CURTEMP310.seal       WITH ALLTRIM(SUBSTR(lcStr,416,15)), ;
									CURTEMP310.ITEM       WITH ALLTRIM(SUBSTR(lcStr,113,15)), ;
									CURTEMP310.wt         WITH ALLTRIM(SUBSTR(lcStr,431,8)), ;
									CURTEMP310.cube       WITH ALLTRIM(SUBSTR(lcStr,453,8)), ;
									CURTEMP310.ctnqty     WITH ALLTRIM(SUBSTR(lcStr,464,5)), ;
									CURTEMP310.units      WITH ALLTRIM(SUBSTR(lcStr,469,9)), ;
									CURTEMP310.fcr        WITH ALLTRIM(SUBSTR(lcStr,481,15)), ;
									CURTEMP310.c310file   WITH lcSourceFile, ;
									CURTEMP310.dt310proc  WITH DATETIME() ;
									IN CURTEMP310
							ENDIF
						ENDDO
						=FCLOSE(lnHandle)

*!*				SELECT CURTEMP310
*!*				BROWSE
*!*				THROW
			
						****************************************************************************
						* Move / archive 310 source file
						****************************************************************************

						*!*	* if the file already exists in the archive folder, make sure it is not read-only.
						*!*	* this is to prevent errors we get copying into archive on a resend of an already-archived file.
						*!*	* Necessary because the files in the archive folders were sometimes marked as read-only (not sure why)
						*!*	llArchived = FILE(lcArchiveFile)
						*!*	IF llArchived THEN
						*!*		RUN ATTRIB -R &lcArchiveFile.
						*!*		.TrackProgress('   Used ATTRIB -R for re-archiving : ' + lcSourceFile,LOGIT+SENDIT)
						*!*	ENDIF

						* no longer needed -- we will just rename the file when done, in the original folder
						*!*	* copy source 310 file to target folder
						*!*	COPY FILE (lcSourceFile) TO (lcTargetFile)

						*!*	* archive 310 source file
						*!*	COPY FILE (lcSourceFile) TO (lcArchiveFile)

						*!*	IF NOT .lTestMode THEN
						*!*		* delete original source file if the copies were ok...
						*!*		IF FILE(lcTargetFile) AND FILE(lcArchiveFile) THEN
						*!*			DELETE FILE(lcSourceFile)
						*!*			.TrackProgress("Archived file: " + lcSourceFile + ' to ' + .cAS2DownLoadArchiveFolder,LOGIT+SENDIT+NOWAITIT)
						*!*			.TrackProgress("Copied file: " + lcSourceFile + ' to ' + .cEdiInputFolder,LOGIT+SENDIT+NOWAITIT)
						*!*		ELSE
						*!*			.TrackProgress('!! There were errors copying file: ' + lcSourceFile + ' to ' + .cEdiInputFolder,LOGIT+SENDIT+NOWAITIT)
						*!*		ENDIF
						*!*	ENDIF  &&  NOT .lTestMode

						
						****************************************************************************
						* rename source 310 file
						****************************************************************************
						ldDateTime = DATETIME()
						lcTargetFile = .c310InDir + ;
							"DOSGTINTOFMI_" + ;
							ALLTRIM(STR(YEAR(ldDateTime))) + ;
							PADL(ALLTRIM(STR(MONTH(ldDateTime))),2,"0") + ;
							PADL(ALLTRIM(STR(DAY(ldDateTime))),2,"0") + ;
							PADL(ALLTRIM(STR(HOUR(ldDateTime))),2,"0") + ;
							PADL(ALLTRIM(STR(MINUTE(ldDateTime))),2,"0") + ;
							PADL(ALLTRIM(STR(SEC(ldDateTime))),2,"0")
							
						* insert a second here to prevent duplicate renaming attempts
						WAIT WINDOW "Inserting 1 sec delay..." TIMEOUT 1						
						
						* note this rename does not preserve Upper Case...but Doug needs it to be upper case...
						RENAME (lcSourceFile) TO (lcTargetFile)
						
						* so....Rename file to upper case (Win API function used in this function)
						IF .RenameFileForceCase(lcTargetFile,UPPER(lcTargetFile)) = 0
							.TrackProgress("RenameFileForceCase() of lcTargetFile failed!",LOGIT+SENDIT)
						ENDIF
						
						IF FILE(lcTargetFile) THEN
							.TrackProgress("Renamed source file to: " + lcTargetFile,LOGIT+SENDIT)
						ELSE
							.TrackProgress('!! There were errors renaming file: ' + lcSourceFile ,LOGIT+SENDIT)
						ENDIF


						****************************************************************************
						* update ASN310 table
						****************************************************************************

						IF USED('CURALLHBOLS') THEN
							USE IN CURALLHBOLS
						ENDIF

						SELECT * FROM CURTEMP310 ;
							INTO CURSOR CURALLHBOLS ;
							WHERE disdc = "FMS" ;
							GROUP BY hbol, CONTAINER, disdc

						SELECT CURALLHBOLS
						SCAN
							SCATTER MEMVAR
							INSERT INTO asn310 FROM MEMVAR
						ENDSCAN
						
						.cTopBodyText = .cTopBodyText + "Imported 310 file: " + lcSourceFile + CRLF

						*!*				SELECT asn310
						*!*				BROWSE

						****************************************************************************
						* Second, use ASN310 table to create 315
						****************************************************************************
						
						* get control num for end of filename.
						* filenames are like: 'em315_FMI_to_Import.dat.20100325-999999' where 999999 is a unique # that gets incremented.
						IF NOT USED('SEARSEDICTRL') THEN
							USE (.cControlTable) IN 0 ALIAS SEARSEDICTRL SHARED
						ENDIF
						SELECT SEARSEDICTRL
						ln315ControlNum = SEARSEDICTRL.cntrl315
						REPLACE SEARSEDICTRL.cntrl315 WITH (ln315ControlNum + 1) IN SEARSEDICTRL
						USE IN SEARSEDICTRL
						lc315ControlNum = PADL(ln315ControlNum,7,'0')

						*!*	lc315OutFile = "em315_FMI_"+ALLTRIM(STR(YEAR(DATE())))+PADL(ALLTRIM(STR(MONTH(DATE()))),2,"0")+;
						*!*		PADL(ALLTRIM(STR(DAY(DATE()))),2,"0")+PADL(ALLTRIM(STR(HOUR(DATETIME()))),2,"0")+;
						*!*		PADL(ALLTRIM(STR(MINUTE(DATETIME()))),2,"0")+PADL(ALLTRIM(STR(SEC(DATETIME()))),2,"0")+".txt"
							
						lc315OutFileRoot = "em315_FMI_to_Import.dat." + ALLTRIM(STR(YEAR(DATE()))) + PADL(ALLTRIM(STR(MONTH(DATE()))),2,"0") + ;
							PADL(ALLTRIM(STR(DAY(DATE()))),2,"0") + "-" + lc315ControlNum

						.TrackProgress("Creating 315 file: " + lc315OutFileRoot,LOGIT+SENDIT)

						lc315ArchiveFile = .c315ArchiveDir + lc315OutFileRoot
						
						lc315OutFile = .c315OutDir + lc315OutFileRoot
						
						* create file in the archive folder, then will copy to Out/upload folder...
						ln315Handle = FCREATE(lc315ArchiveFile)

						lcHeader = "#KM315 FROM FMI TO KMART " + ;
							ALLTRIM(STR(YEAR(DATE()))) + ;
							PADL(MONTH(DATE()),2,'0') + ;
							PADL(DAY(DATE()),2,'0') + " " + ;
							STRTRAN(SUBSTR(TIME(),1,5),":","") + " " + ;
							lc315ControlNum
							
						lcHeader = PADR(lcHeader,.n315RecLength," ")


						FPUTS(ln315Handle,lcHeader)

						lnNumRecs=0
						SELECT asn310
						SCAN FOR sent = .F.
							lnNumRecs = lnNumRecs + 1
							
							*!*	lcDetail = "FM"+asn310.scac+PADR(ALLTRIM(asn310.CONTAINER),15," ")+PADR(ALLTRIM(asn310.hbol),16," ")+ ;
							*!*		PADR(ALLTRIM(asn310.hbol),16," ")+"R "+SPACE(20)+;
							*!*		"FM  "+SUBSTR(STRTRAN(TIME(),":",""),1,6)+PADR(ALLTRIM(asn310.disdc),5," ")+ ;
							*!*		PADR(ALLTRIM(asn310.vessel),35," ")+PADR(ALLTRIM(asn310.voyage),10," ")+ ;
							*!*		SPACE(16)+ALLTRIM(STR(YEAR(DATE())))+PADL(ALLTRIM(STR(MONTH(DATE()))),2,"0")+ ;
							*!*		PADL(ALLTRIM(STR(DAY(DATE()))),2,"0") + "Y"+SPACE(40)
								
							lcDetail = "FM"+asn310.scac + ;
								PADR(ALLTRIM(asn310.CONTAINER),15," ") + ;
								PADR(ALLTRIM(asn310.hbol),16," ") + ;
								PADR(ALLTRIM(asn310.hbol),16," ") + ;
								"R " + SPACE(20)+;
								"FM  " + SUBSTR(STRTRAN(TIME(),":",""),1,6) + ;
								PADR(ALLTRIM(asn310.disdc),5," ") + ;
								PADR(ALLTRIM(asn310.vessel),35," ") + ;
								PADR(ALLTRIM(asn310.voyage),10," ") + ;
								SPACE(16)+ALLTRIM(STR(YEAR(DATE()))) + ;
								PADL(ALLTRIM(STR(MONTH(DATE()))),2,"0") + ;
								PADL(ALLTRIM(STR(DAY(DATE()))),2,"0") + ;
								"Y"
								
							lcDetail = PADR(lcDetail,.n315RecLength," ")
								
							FPUTS(ln315Handle,lcDetail)

						ENDSCAN

						*lcFooter = "#EOT     1 "+PADL(ALLTRIM(STR(lnNumRecs)),5," ")+" "+"0000001"+SPACE(175)
						
						lcFooter = "#EOT " + ;
							PADL(lnNumGroups,5,'0') + " " + ;
							PADL(lnNumRecs,5,'0') + " " + ;
							lc315ControlNum

						lcFooter = PADR(lcFooter,.n315RecLength," ")

						FPUTS(ln315Handle,lcFooter)

						FCLOSE(ln315Handle)

						* Rename file with its original name, to ensure that FMI is in upper case (Win API function used in this function)
						IF .RenameFileForceCase(lc315ArchiveFile,lc315ArchiveFile) = 0
							.TrackProgress("RenameFileForceCase() of lc315ArchiveFile failed!",LOGIT+SENDIT)
						ENDIF
						
						IF FILE(lc315ArchiveFile) THEN
							.TrackProgress("Created 315 file: " + lc315ArchiveFile,LOGIT+SENDIT)
							.cTopBodyText = .cTopBodyText + "Created 315 file: " + lc315ArchiveFile + CRLF + CRLF
							
							IF .lUpload315s THEN
								COPY FILE (lc315ArchiveFile) TO (lc315OutFile)
								* Rename file with its original name, to ensure that FMI is in upper case (Win API function used in this function)
								* (Renaming again after the COPY in case the OS does not preserve the case from source filename)
								IF .RenameFileForceCase(lc315OutFile,lc315OutFile) = 0
									.TrackProgress("RenameFileForceCase() of lc315OutFile failed!",LOGIT+SENDIT)
								ENDIF
								IF FILE(lc315OutFile) THEN
									.TrackProgress("Uploaded 315 file: " + lc315OutFile,LOGIT+SENDIT)
									.cTopBodyText = .cTopBodyText + "Sent 315 file to upload folder: " + lc315OutFile + CRLF + CRLF
								ELSE
									.TrackProgress("!! Error uploading 315 file: " + lc315OutFile,LOGIT+SENDIT)
								ENDIF
							ENDIF
							
							* mark the 310 records as processed
							SELECT asn310
							SCAN FOR sent = .F.
								REPLACE asn310.sent WITH .T., c315File WITH lc315OutFile, dt315proc WITH DATETIME()							
							ENDSCAN							
						ELSE
							.TrackProgress("!! Error creating 315 file: " + lc315OutFile,LOGIT+SENDIT)
						ENDIF

					ENDFOR

				ELSE
					.TrackProgress('No Sears 310 Flat Files to import', LOGIT+SENDIT)
				ENDIF

				.TrackProgress('=========================================================', LOGIT+SENDIT)


				.TrackProgress('Sears 315 Out Process process ended normally!',LOGIT+SENDIT)

				.cBodyText = .cTopBodyText + CRLF + CRLF + "-------------------- <Report log follows> ------------------------" + CRLF + CRLF + .cBodyText

			CATCH TO loError

				.TrackProgress('There was an error.',LOGIT+SENDIT)
				.TrackProgress('The Error # is: ' + TRANSFORM(loError.ERRORNO), LOGIT+SENDIT)
				.TrackProgress('The Error Message is: ' + TRANSFORM(loError.MESSAGE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred in Program: ' + TRANSFORM(loError.PROCEDURE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred at line #: ' + TRANSFORM(loError.LINENO), LOGIT+SENDIT)
				.TrackProgress(.cThrowDescription, LOGIT+SENDIT)
				lnNumberOfErrors = lnNumberOfErrors + 1
				CLOSE DATA

			ENDTRY

			CLOSE DATA
			*CLOSE ALL
			WAIT CLEAR
			***************** INTERNAL email results ******************************
			.TrackProgress('About to send status email.',LOGIT)
			.TrackProgress('The logfile is: ' + .cLogFile, SENDIT)
			.TrackProgress('Sears 315 Out Process process started: ' + .cStartTime, LOGIT+SENDIT)
			.TrackProgress('Sears 315 Out Process process finished: ' + TTOC(DATETIME()), LOGIT+SENDIT)

			IF .lSendInternalEmailIsOn THEN
				DO FORM dartmail2 WITH .cSendTo,.cFrom,.cSubject,.cCC,.cAttach,.cBodyText,"A"
				.TrackProgress('Sent status email.',LOGIT)
			ELSE
				.TrackProgress('Did not send status email: THIS.lSendInternalEmailIsOn = FALSE.',LOGIT)
			ENDIF

		ENDWITH
		RETURN
	ENDFUNC && main


	FUNCTION ConvertYYYYMMDDStringToDate
		LPARAMETERS tcYYYYMMDDString
		* this assumes we are already in SET DATE YMD mode...
		LOCAL ldDate
		ldDate = CTOD( LEFT(tcYYYYMMDDString,4) + "/" + SUBSTR(tcYYYYMMDDString,5,2) + "/" + SUBSTR(tcYYYYMMDDString,7,2) )
		RETURN ldDate
	ENDFUNC


	PROCEDURE TrackProgress
		* do any combination of Wait Window, writing to logfile, and adding to body of email,
		* based on nFlags parameter.
		LPARAMETERS tcExpression, tnFlags
		WITH THIS
			IF BITAND(tnFlags,LOGIT) = LOGIT THEN
				IF .lLoggingIsOn THEN
					?
					? .cProcessName + "  " + TTOC(DATETIME()) + ": " + tcExpression
				ENDIF
			ENDIF
			IF .lWaitWindowIsOn THEN
				IF BITAND(tnFlags,NOWAITIT) = NOWAITIT THEN
					WAIT WINDOW tcExpression NOWAIT
				ENDIF
				IF BITAND(tnFlags,WAITIT) = WAITIT THEN
					WAIT WINDOW tcExpression TIMEOUT .nWaitWindowTimeout
				ENDIF
			ENDIF
			IF BITAND(tnFlags,SENDIT) = SENDIT THEN
				IF .lSendInternalEmailIsOn THEN
					.cBodyText = .cBodyText + tcExpression + CRLF + CRLF
				ENDIF
			ENDIF
		ENDWITH
	ENDPROC  &&  TrackProgress


	PROCEDURE SortArrayByDateTime
		* expects taInArray[] to be the result of an ADIR()
		LPARAMETERS taInArray, taOutArray
		EXTERNAL ARRAY taInArray, taOutArray
		LOCAL lnRows, lnCols, i, j
		* extending taInArray[] to process the files in date/time order
		lnRows = ALEN(taInArray,1)
		lnCols = ALEN(taInArray,2)
		* create taOutArray with one more column
		DIMENSION taOutArray[lnRows,lnCols + 1]
		* and fill it with taInArray columns AND (filedate + filetime) in new column
		* ( ACOPY() doesn't preserve elements properly because of different # of columns )
		FOR i = 1 TO lnRows
			FOR j = 1 TO lnCols
				taOutArray[i,j] = taInArray[i,j]
			ENDFOR
			taOutArray[i,lnCols + 1] = DTOS(taInArray[i,3]) + "_" + taInArray[i,4]
		ENDFOR
		* sort taOutArray on date/time column
		=ASORT(taOutArray,6)
	ENDPROC
	
	
	FUNCTION RenameFileForceCase(tcFileName,tcNewFileName)
		DECLARE LONG MoveFile IN WIN32API STRING SourceFileName, STRING DestFileName
		RETURN MoveFile(tcFileName, tcNewFileName)
	ENDFUNC
	
	
*!*		FUNCTION RenameFileUpperCase(tcFileName)
*!*			DECLARE LONG MoveFile IN WIN32API STRING SourceFileName, STRING DestFileName
*!*			RETURN MoveFile(tcFileName, UPPER(tcFileName))
*!*		ENDFUNC


ENDDEFINE
