Parameters otf_flag

Wait Window "Now in IMPORT Phase..." Nowait
EmailcommentStr = ""

*tfrom    ="NoReply <noreply@tollgroup.com>"
tfrom = "Toll EDI Operations <toll-edi-ops@tollgroup.com>"

cOfficeImp = cOffice
nUploadCount = 0
m.acct_num = nAcctNum
cPTString = ""
Public Array thisarray(1)

guserid = "ALL940IM"

** create the updateable cursors
useca("pt","wh")
useca("ptdet","wh")

If Used('outship')
  Use In outship
Endif

Select xpt
Set Order To
Locate

lnCount = 0
Store Reccount() To lnCount
Wait Window "Picktickets to load ...........  "+Str(lnCount) Nowait &&TIMEOUT 2

If nAcctNum = 6561
  cPTString = Padr("CONSIGNEE",42)+Padr("PO NUM",27)+Padr("CANCEL",12)+Padr("PICKTICKET",22)+Padr("QTY",6)+"SALESORDER"+Chr(13)
Else
  cPTString = Padr("CONSIGNEE",42)+Padr("PO NUM",27)+Padr("CANCEL",12)+Padr("PICKTICKET",22)+"QTY"+Chr(13)
Endif

Wait Window  "At PT upload scan" Nowait &&TIMEOUT 2

gMasterOffice = Iif(Inlist(cOffice,"N","J","I"),"N",cOffice)
gOffice = cMod

** init some variables for the extra PTID verification  PG 04/04/2017
xptid =0
xdisaster = .F.

Scan
  Wait "AT XPT RECORD "+Alltrim(Str(Recno())) Window Nowait Noclear
*!* Check for existence of PT in outship
*!* First choice does a select by PT

  csq1 = [select * from outship where accountid = ]+Transform(nAcctNum)+[ and office = ']+cOfficeImp+[' and ship_ref = ']+Padr(Alltrim(xpt.ship_ref),20)+[']
  xsqlexec(csq1,"tempos1",,"wh",,,,,,.T.)
  If Reccount('tempos1')>0
    Wait Window "Found in outship...looping" Nowait
    Use In tempos1
    Select xpt
    Loop
  Endif

  Select pt
  xShip_ref = xpt.ship_ref
  lcQuery = [select * from pt where accountid = ]+Transform(nAcctNum)+[ and ship_ref = ']+xShip_ref+[']
  xsqlexec(lcQuery,"p1",,"wh")
  Select p1
  Locate
  If Reccount('p1') > 0
    Select xpt
    Loop
  Endif
  Use In p1

  Select xpt
  Scatter Memvar Memo Blank

  If nAcctNum = 6561 && Merkury, per Viren
    alength = Alines(axpt,xpt.shipins,.T.,Chr(13))
    cCO = segmentget(@axpt,"OLDPT",alength)
    cPTString = cPTString+Chr(13)+Padr(Allt(xpt.consignee),42)+Padr(Alltrim(xpt.cnee_ref),27)+Padr(Dtoc(xpt.Cancel),12)+Padr(Alltrim(cCO),22)+Padr(Alltrim(Str(xpt.qty)),6)+Alltrim(xpt.ship_ref)
  Else
    cPTString = cPTString+Chr(13)+Padr(Allt(xpt.consignee),42)+Padr(Alltrim(xpt.cnee_ref),27)+Padr(Dtoc(xpt.Cancel),12)+Padr(Alltrim(xpt.ship_ref),22)+Alltrim(Str(xpt.qty))
  Endif
  nPTID = xpt.ptid
  Scatter Fields Except ptid Memvar Memo
  m.adddt = Datetime()
  m.addby = "TGF-PROC"
  m.addproc = "940UPLOAD"

  Select pt
  m.office = cOffice
  m.mod = cMod
  insertinto("pt","wh",.T.)

  If xptid!=0 And xptid=pt.ptid && check to make sure that xptid has changed since the last scan loop, if not then abort and alert
    xdisaster = .T.
  Endif

  nUploadCount = nUploadCount  +1

  Select xptdet
  Scatter Memvar Memo Blank
  Scan For ptid=xpt.ptid
    Scatter Memvar Memo
    m.office = cOffice
    m.mod = cMod
    m.accountid = xpt.accountid
    m.ship_ref = xpt.ship_ref
    m.adddt = Datetime()
    m.addby = "TGF-PROC"
    m.addproc = "940UPLOAD"
    m.ptid=pt.ptid
    insertinto("ptdet","wh",.T.)
  Endscan
  xptid= pt.ptid
  xShip_ref = Alltrim(pt.ship_ref)

  If !xdisaster
*    Wait Window At 10,10 "at the table update update ......"
    If !tu("pt")  && something went wrong during the tabelupdate most likely a PTID issue
      Wait Window At 10,10 "PT update had an issue ......" Timeout 1
      tsubjectz= "EDI PT Upload Error, Table Update Error for "+cCustname+" Pickticket Upload: " +Ttoc(Datetime())
      tattachz = ""
      tmessagez = "PT upload error for "+cCustname+Chr(13)+"From File: "+cfilename+Chr(13)+"PTDETID = "+Transform(ptdet.ptdetid)+Chr(13)+"Pick Ticket: "+Alltrim(pt.ship_ref)
      tsendtoz = "pgaidis@fmiint.com,joe.bianchi@tollgroup.com,Mike.Winter@tollgroup.com"
      tccz = "darren.young@tollgroup.com"
      tbbcz=""

*      DO sendmail_au WITH tsendtoz,tfrom,tsubjectz,tccz,tattachz,tmessagez,"  "
      Do Form m:\dev\frm\dartmail2 With tsendtoz,tfrom,tsubjectz,tccz,tattachz,tmessagez,"A"
      Return
    Else
      If !tu("ptdet")
*       Wait Window At 10,10 "PTDET update had an issue ......" Timeout 1
        Delete In pt  && OK the PTDET did not work so delete the header
        tu("pt")      && and update the deletion
        tsubjectx= "EDI PTDET Upload Error, Mixed PTDETIDs for "+cCustname+" Pickticket Upload: " +Ttoc(Datetime())
        tattachx = ""
        tmessagex = "PTDET upload error for "+cCustname+Chr(13)+"From File: "+cfilename+Chr(13)+"PTDETID = "+Transform(ptdet.ptdetid)+Chr(13)+"Pick Ticket: "+Alltrim(pt.ship_ref)

        tsendtox = "pgaidis@fmiint.com,joe.bianchi@tollgroup.com,Mike.Winter@tollgroup.com"
        tccx = "darren.young@tollgroup.com"
        tbbcx=""
*        DO sendmail_au WITH tsendtox,tfrom,tsubjectx,tccx,tattachx,tmessagex," "
        Do Form m:\dev\frm\dartmail2 With tsendtox,tfrom,tsubjectx,tccx,tattachx,tmessagex,"A"
        Return
      Endif
    Endif
  Endif

  If xdisaster
*   Wait Window At 10,10 "PT/PTDET disaster update ......" Timeout 1
    tsubjecty= "EDI PT Upload Error, Mixed PTIDs for "+cCustname+" Pickticket Upload: " +Ttoc(Datetime())
    tattachy = ""
    tmessagey = "PT upload error for "+cCustname+Chr(13)+"From File: "+cfilename+Chr(13)+"and PT# "+xShip_ref+", PTID = "+Transform(xptid)

    tsendtoy = "pgaidis@fmiint.com,joe.bianchi@tollgroup.com,Mike.Winter@tollgroup.com"
    tccy = "darren.young@tollgroup.com"
    tbbcy =""
*    DO sendmail_au WITH tsendtoy,tfrom,tsubjecty,tccy,tattachy,tmessagey,"  "
    Do Form m:\dev\frm\dartmail2 With tsendtoy,tfrom,tsubjecty,tccy,tattachy,tmessagey,"A"
    Return
  Endif
&& OK now process the next pickticket
Endscan

Select ptid,ship_ref From ptdet Group By ptid,ship_ref Into Cursor xpts

*Wait Window At 10,10 "at the pt hist section ......"


*** not for MJ ecomm

cErrMsg="at the pt hist section ......"

If otf_flag = .F.
  If nUploadCount > 0 And !lTesting
    pthist("P")
*  Wait Window At 10,10 "just did  pt hist P section ......"
    pthist("E")
*  Wait Window At 10,10 "just did  pt hist E section ......"
  Endif
Endif

cErrMsg="at the all940 import 1......"

Select xpt
Locate
pickticket_num_start = xpt.ship_ref

cErrMsg="at the all940 import 2......"

Goto Bott
pickticket_num_end = xpt.ship_ref
Locate
cErrMsg="at the all940 import 3......"
Try
  Copy File [&xfile] To [&ArchiveFile]
Catch
Endtry

cErrMsg="at the all940 import 4......"
Try
  Delete File [&xfile]
Catch
Endtry

cErrMsg="at the all940 import 5......"

If nUploadCount > 0
  Wait Window "Picktickets uploaded...........  "+Str(nUploadCount) Nowait &&TIMEOUT 2
Else
  Wait Window "No New Picktickets uploaded from file "+xfile Nowait &&TIMEOUT 2
Endif

Wait "940 Import Round Complete" Window Nowait && TIMEOUT 2
Wait Clear

Use In pt
Use In ptdet
If Used("outship")
  Use In outship
Endif
cErrMsg="at the all940 import 6......"

currfile = xfile
Do Case
Case cOffice = "C"
  cOfficeGroup = "SP"
Case Inlist(cOffice,"L","Z")
  cOfficeGroup = "ML"
Case Inlist(cOffice,"K","S")
  cOfficeGroup = "KY"
Case Inlist(cOffice,"J","I","N")
  cOfficeGroup = "NJ"
Case cOffice = "M"
  cOfficeGroup = "FL"
Case Inlist(cOffice,"X","Y")
  cOfficeGroup = "CR"
Otherwise
  Select 0
  Use F:\3pl\Data\mailmaster Alias mm Shared
  Locate For edi_type = "MISC" And taskname = "GENERAL"
  tsendto = Alltrim(Iif(mm.use_alt,mm.sendtoalt,mm.sendto))
  tcc = Alltrim(Iif(mm.use_alt,mm.ccalt,mm.cc))
  Use In mm
  cOfficeGroup = "XX"
Endcase

cErrMsg="at the all940 import 7......"


If lTesting
  tcc = ""
Endif

EmailcommentStr = EmailcommentStr+Chr(13)+cPTString

*************************************************************************************************
*!* E-Mail process
*************************************************************************************************

Assert .F. Message "At email"

cErrMsg="at the all940 import 8......"

If nUploadCount > 0
  cPTQty = Allt(Str(nUploadCount))
  If lEmail
    If cOfficeGroup = "XX"
      tcc = ""
    Endif
    Do Case
    Case Inlist(cOffice,"L")
      cMailLoc = "Mira Loma"
    Case Inlist(cOffice,"K","S")
      cMailLoc = "Louisville"
    Case Inlist(cOffice,"Y","Z")
      cMailLoc = "Carson"
    Case Inlist(cOffice,"M")
      cMailLoc = "Miami"
    Case Inlist(cOffice,"I","N","J")
      cMailLoc = "New Jersey"
    Otherwise
      cMailLoc = "San Pedro"
    Endcase

    cErrMsg="at the all940 import 9......"


    tsubject= "TGF "+cMailLoc+" "+cCustname+" Pickticket Upload: " +Ttoc(Datetime())
    tattach = ""
    tmessage = "Uploaded "+cPTQty+" Picktickets for "+cCustname+Chr(13)+"From File: "+cfilename+Chr(13)
    tmessage = tmessage + EmailcommentStr

    If lTesting Or lTestImport
      tmessage = tmessage+Chr(13)+"*TEST DATA* - LOADED INTO F:\WHP TABLES"
    Endif
    xbbc =""
    cErrMsg="at the all940 import 10......"
    If nAcctNum != 6532
      Do Form m:\dev\frm\dartmail2 With tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
*    DO sendmail_au WITH tsendto,"noreply@tollgroup.com",tsubject,tcc,tattach,tmessage,""
**Parameter lcTO, lcFrom,                lcSubject,lcCC,lcAttach,lcBody,lcBCC
    Endif
  Endif
  nUploadCount = 0
  cPTQty = ""
Endif
*************************************************************************************************
Release thisarray
Return

****************************
Procedure segmentget
****************************

Parameter thisarray,lckey,nlength

For i = 1 To nlength
  If i > nlength
    Exit
  Endif
  lnend= At("*",thisarray[i])
  If lnend > 0
    lcthiskey =Trim(Substr(thisarray[i],1,lnend-1))
    If Occurs(lckey,lcthiskey)>0
      Return Substr(thisarray[i],lnend+1)
      i = 1
    Endif
  Endif
Endfor

Return ""

