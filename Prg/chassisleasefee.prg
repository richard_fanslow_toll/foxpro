* automatic charge slip creation
* runs when a WO is saved and from fsheet and confirm when an export WO is created

lparameters xtable, xforce

set step on

if (wolog.accountid=470 and wolog.billtoid=875 and !inlist(wolog.ssco,"EVER","MSC")) ;
or (inlist(wolog.accountid,6480,6420) and inlist(wolog.wotype,"OCEAN FULL","OCEAN EXPORT")) ;
or (inlist(wolog.accountid,1,62,1747) and wolog.type="O" and goffice="V") ;
or (wolog.accountid=6614 and wolog.office="C")

	if xtable
		xsqlexec("select csid from cs where wo_num="+transform(wolog.wo_num)+" and cstype='CHASSIS LEASE FEE'","xcs",,"wo")
	else
		select vcs
		locate for cstype="CHASSIS LEASE FEE"
	endif
	
	if !found() or xforce
		if xtable
			select cs
			scatter memvar memo blank
			select csdet
			scatter memvar memo blank
		else
			select vcs
			scatter memvar memo blank
			select vcsdet
			scatter memvar memo blank
		endif
				
		select wolog
		scatter memvar fields wologid, wo_num, wo_date, accountid, billtoid, office, acctname
		m.containerin=wolog.container
		
		m.invnum=""
		m.csdt=qdate()
		m.cstype="CHASSIS LEASE FEE"

		if getrate(wolog.accountid,,"ACCESSORIAL",goffice,wolog.wo_date,"stop='CHASSIS LEASE FEE'")
			m.totchg=rate.rate
		else
			m.totchg=0
		endif

		m.description="Chassis lease fee"
		m.charge=m.totchg
		m.addfrom="|a"

		if xtable
			insertinto("cs","wo",.t.)
			m.csid=cs.csid
			insertinto("csdet","wo",.t.)
		else
			insertinto("vcs","wo",.t.)
			m.csid=vcs.csid
			insertinto("vcsdet","wo",.t.)
		endif

		replace csrevenue with wolog.csrevenue+m.totchg in wolog
	endif
endif

*

if wolog.hazmat
	if xtable
		xsqlexec("select csid from cs where wo_num="+transform(wolog.wo_num)+" and cstype='HAZMAT'","xcs",,"wo")
	else
		select vcs
		locate for cstype="HAZMAT"
	endif
	
	if !found() or xforce
		if xtable
			select cs
			scatter memvar memo blank
			select csdet
			scatter memvar memo blank
		else
			select vcs
			scatter memvar memo blank
			select vcsdet
			scatter memvar memo blank
		endif
		
		select wolog
		scatter memvar fields wologid, wo_num, wo_date, accountid, office, acctname
		m.containerin=wolog.container

		select acctbill
		locate for accountid=m.accountid and main
		m.billtoid=acctbill.billtoid

		m.invnum=""
		m.csdt=qdate()
		m.cstype="HAZMAT"

		if getrate(wolog.accountid,,"ACCESSORIAL",goffice,qdate(),"stop='HAZMAT  '")
			m.totchg=rate.rate
		else
			m.totchg=grt("HAZMAT","T",qdate(),,rateacct.rateheadid)
		endif

		m.rate=m.totchg
		m.description="Hazmat"
		m.charge=m.totchg
		m.addfrom="|b"

		if xtable
			insertinto("cs","wo",.t.)
			m.csid=cs.csid
			insertinto("csdet","wo",.t.)
		else
			insertinto("vcs","wo",.t.)
			m.csid=vcs.csid
			insertinto("vcsdet","wo",.t.)
		endif

		if getrate(wolog.accountid,,"ACCESSORIAL",goffice,wolog.wo_date,"stop='HAZMAT PLACARDS'")
			m.charge=rate.rate
			m.description="Driver placing hazmat placards"
			m.addfrom="|c"

			if xtable
				insertinto("csdet","wo",.t.)
			else
				insertinto("vcsdet","wo",.t.)
			endif
			
			m.totchg=m.totchg+m.charge
			if xtable
				replace totchg with m.totchg in cs
			else
				replace totchg with m.totchg in vcs
			endif
		endif

		replace csrevenue with wolog.csrevenue+m.totchg in wolog
	endif
endif

*

if wolog.type#"D" and getrate(wolog.accountid,,"ACCESSORIAL",goffice,wolog.wo_date,"stop='DO PROCESSING'")
	if xtable
		xsqlexec("select csid from cs where wo_num="+transform(wolog.wo_num)+" and cstype='DO PROCESSING'","xcs",,"wo")
	else
		select vcs
		locate for cstype="DO PROCESSING"
	endif

	if !found() or xforce
		if xtable
			select cs
			scatter memvar memo blank
			select csdet
			scatter memvar memo blank
		else
			select vcs
			scatter memvar memo blank
			select vcsdet
			scatter memvar memo blank
		endif
		
		select wolog
		scatter memvar fields wologid, wo_num, wo_date, accountid, office, acctname
		m.containerin=wolog.container

		select acctbill
		locate for accountid=m.accountid and main
		m.billtoid=acctbill.billtoid

		m.invnum=""
		m.csdt=qdate()
		m.cstype="DO PROCESSING"
		m.totchg=rate.rate
		m.description="Delivery order processing"
		m.charge=m.totchg
		m.addfrom="|d"

		if xtable
			insertinto("cs","wo",.t.)
			m.csid=cs.csid
			insertinto("csdet","wo",.t.)
		else
			insertinto("vcs","wo",.t.)
			m.csid=vcs.csid
			insertinto("vcsdet","wo",.t.)
		endif

		replace csrevenue with wolog.csrevenue+m.totchg in wolog
	endif
endif
