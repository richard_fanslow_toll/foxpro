runack("MODERNSHOE997PROCESS")

SET ASSERTS on

CD F:\ftpusers\modernshoe\997hold
IF ADIR(arynushoe,"*.txt")>0
	WAIT WINDOW "There are ModernShoe 997 files to process" TIMEOUT 1
	len1 = ALEN(arynushoe,1)
ELSE
	CLOSE DATABASES ALL
	RETURN
ENDIF

lTesting = .f.

FOR repeater = 1 TO len1
	cString = ""
	cTimeString = TTOC(DATETIME(),1)
	cFilename = ALLTRIM(arynushoe[repeater,1])
	cFilestem = JUSTSTEM(cFilename)
	cFileExt = ".txt"
	IF lTesting
		cOutfile = LOWER("F:\JOEB\TEMP\"+cFilestem+cTimeString+cFileExt)
	ELSE
		cOutfile = LOWER("F:\FTPUSERS\MODERNSHOE\997OUT\"+cFilestem+cTimeString+cFileExt)
		cArchiveFile = LOWER("F:\FTPUSERS\MODERNSHOE\997OUT\997archive\"+cFilestem+cTimeString+cFileExt)
	ENDIF

*	ASSERT .f.
	cString = FILETOSTR(cFilename)
	cString = STRTRAN(cString,"FMI M","FMI K")
	cString = STRTRAN(cString,"~","")
	cString = STRTRAN(cString,CHR(10),"~"+CHR(10))
*!*		nPos = AT(CHR(10),cString,1)
*!*		cString = LEFT(cString,nPos-2)+">"+SUBSTR(cString,nPos-1)
	nStrLen = LEN(cString)
	cString = LEFT(cString,nStrLen-1)
	WAIT "" TIMEOUT 2	
	STRTOFILE(cString,cOutfile)
	
	IF !lTesting
		COPY FILE &cOutfile TO &cArchiveFile
	ENDIF
	DELETE FILE &cFilename
ENDFOR

IF lTesting
	MODIFY FILE &cOutfile
ENDIF

CLEAR ALL
RETURN
