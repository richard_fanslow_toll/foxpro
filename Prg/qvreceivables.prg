* QVRECEIVABLES
* Create a receivable spreadsheet for import into QlikView.
* To meet requirement for creating a 'hybrid' report based on 'Daily Receivables' & 'Revenue by GL Account' FoxPro reports.

* EXE = F:\UTIL\QLIK\QVRECEIVABLES.EXE
* run it daily
*
* 4/25/2018 MB: changed fmiint.com email to my Toll email.

runack("QVRECEIVABLES")

LOCAL lnError, lcProcessName, loQVRECEIVABLES

DECLARE INTEGER CreateMutex IN Win32API ;
	STRING @lpMutexAttributes, ;
	STRING bInitialOwner, ;
	STRING @lpName

DECLARE INTEGER GetLastError IN Win32API

* if running in EXE check to see if already running
IF _VFP.STARTMODE > 0 THEN
	lcProcessName = "QVRECEIVABLES"
	CreateMutex(0,CHR(1),@lcProcessName)
	lnError = GetLastError()
	IF lnError = 183
		WAIT WINDOW TIMEOUT 10 "QVRECEIVABLES process is already running..."
		RETURN .F.
	ENDIF
ENDIF

utilsetup("QVRECEIVABLES")


loQVRECEIVABLES = CREATEOBJECT('QVRECEIVABLES')
loQVRECEIVABLES.MAIN()

schedupdate()

CLOSE DATABASES ALL
RETURN

#DEFINE LOGIT 1
#DEFINE WAITIT 2
#DEFINE NOWAITIT 4
#DEFINE SENDIT 8
#DEFINE CRLF CHR(13) + CHR(10)
#DEFINE LF CHR(10)
#DEFINE RETURN_DATA_MANDATORY .F.
#DEFINE RETURN_DATA_NOT_MANDATORY .T.
#DEFINE DAILY_OT_MULTIPLIER 1.5
#DEFINE DAILY_OT20_MULTIPLIER 2.0
#DEFINE xlEdgeBottom 9
#DEFINE xlContinuous 1
#DEFINE xlMedium -4138

DEFINE CLASS QVRECEIVABLES AS CUSTOM

	cProcessName = 'QVRECEIVABLES'

	lTestMode = .T.  && .F. FOR NORMAL OPERATION, .T. TO SUPPRESS ALL BUT DEVELOPER EMAILS AND CHANGE INPUT TABLE, ETC.

	lAutoYield = .T.

	cStartTime = TTOC(DATETIME())
	
	* processing properties


	* date properties
	dtNow = DATETIME()

	dToday = DATE()

	cToday = DTOC(DATE())

	* wait window properties
	nWaitWindowTimeout = 2
	lWaitWindowIsOn = .T.

	* logfile properties
	lLoggingIsOn = .T.
	cLogFile = 'F:\UTIL\QLIK\LOGFILES\QVRECEIVABLES_LOG.TXT'

	* INTERNAL email properties
	lSendInternalEmailIsOn = .T.
	cFrom = 'Mark.Bennett@Tollgroup.com'
	cSendTo = 'Mark.Bennett@Tollgroup.com'
	cCC = ''
	cSubject = 'QVRECEIVABLES process'
	cAttach = ''
	cBodyText = ''
	cTopBodyText = ''

	cCOMPUTERNAME = ''
	cUSERNAME = ''

	FUNCTION INIT
		IF NOT DODEFAULT()
			RETURN .F.
		ENDIF
		WITH THIS
			CLEAR
			*SET RESOURCE OFF
			CLOSE DATA
			SET CENTURY ON
			SET DATE AMERICAN
			SET HOURS TO 24
			SET ANSI ON
			SET TALK OFF
			SET DELETED ON
			SET CONSOLE OFF
			SET EXCLUSIVE OFF
			SET SAFETY OFF
			SET EXACT OFF
			SET STATUS BAR ON
			SET SYSMENU OFF
			SET ENGINEBEHAVIOR 70
			_VFP.AUTOYIELD = .lAutoYield
			.cCOMPUTERNAME = GETENV("COMPUTERNAME")
			.cUSERNAME = GETENV("USERNAME")
			IF .lTestMode THEN
				.cSendTo = 'Mark.Bennett@Tollgroup.com'
				.cCC = ''
				.cLogFile = 'F:\UTIL\QLIK\LOGFILES\QVRECEIVABLES_LOG_TESTMODE.TXT'
			ENDIF
			.lLoggingIsOn = .lLoggingIsOn AND NOT EMPTY(.cLogFile)
		ENDWITH
	ENDFUNC


	FUNCTION DESTROY
		WITH THIS
			IF .lLoggingIsOn  THEN
				SET ALTERNATE OFF
				SET ALTERNATE TO
			ENDIF
		ENDWITH
		DODEFAULT()
	ENDFUNC


	FUNCTION MAIN

		WITH THIS
			LOCAL lnNumberOfErrors

			TRY
				lnNumberOfErrors = 0

				IF .lLoggingIsOn THEN
					SET ALTERNATE TO (.cLogFile) ADDITIVE
					SET ALTERNATE ON
				ENDIF

				.TrackProgress('QVRECEIVABLES process started....', LOGIT+SENDIT+NOWAITIT)
				.TrackProgress('COMPUTERNAME = ' + .cCOMPUTERNAME, LOGIT+SENDIT)
				.TrackProgress('USERNAME = ' + .cUSERNAME, LOGIT+SENDIT)
				.TrackProgress('PROJECT = QVRECEIVABLES', LOGIT+SENDIT)
				IF .lTestMode THEN
					.TrackProgress('===>TEST MODE', LOGIT+SENDIT)
				ENDIF

				.cSubject = 'QVRECEIVABLES process for ' + DTOC(.dToday)
				
				*.SetUpArrays()

				xsqlexec("select * from division",,,"ar")
				index on company tag company
				set order to
								
				xsqlexec("select * from glacct",,,"ar")
				index on glcode tag glcode
				set order to

				xsqlexec("select * from invoice",,,"ar")
				index on invnum tag invnum
				set order to
								
				LOCAL ldFromDate, ldToDate
				
				* Reprocess data from 1/1/2016 to date every day...

*!*					ldFromDate = {^2015-01-01}
*!*					ldToDate = {^2015-12-31}
				
				ldFromDate = {^2016-01-01}
				ldToDate = DATE()
				
				.ProcessDateRange( ldFromDate, ldToDate )
				

				.TrackProgress('QVRECEIVABLES process ended normally....', LOGIT+SENDIT+NOWAITIT)

			CATCH TO loError
				
				.lSendInternalEmailIsOn = .T.

				.TrackProgress('There was an error.',LOGIT+SENDIT)
				.TrackProgress('The Error # is: ' + TRANSFORM(loError.ERRORNO), LOGIT+SENDIT)
				.TrackProgress('The Error Message is: ' + TRANSFORM(loError.MESSAGE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred in Program: ' + TRANSFORM(loError.PROCEDURE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred at line #: ' + TRANSFORM(loError.LINENO), LOGIT+SENDIT)
				lnNumberOfErrors = lnNumberOfErrors + 1

			ENDTRY

			WAIT CLEAR
			***************** INTERNAL email results ******************************
			.TrackProgress('The logfile is: ' + .cLogFile, SENDIT)
			.TrackProgress('QVRECEIVABLES process started: ' + .cStartTime, LOGIT+SENDIT)
			.TrackProgress('QVRECEIVABLES process finished: ' + TTOC(DATETIME()), LOGIT+SENDIT)

			IF .lSendInternalEmailIsOn THEN
				.TrackProgress('About to send status email.',LOGIT)
				.TrackProgress('==================================================================================================================', SENDIT)
				* try to trap error from not having dartmail dll's registered on user's pc...
				TRY
					.cBodyText = .cTopBodyText + CRLF + .cBodyText

					DO FORM dartmail2 WITH .cSendTo,.cFrom,.cSubject,.cCC,.cAttach,.cBodyText,"A"
					.TrackProgress('Sent status email.',LOGIT)
				CATCH TO loError
					*=MESSAGEBOX('There was an error sending the status email. Please contact Mark Bennett.',0+64,"KRONOS Temp Time")
					.TrackProgress('There was an error sending the status email.',LOGIT)
					.TrackProgress('The Error # is: ' + TRANSFORM(loError.ERRORNO), LOGIT)
					.TrackProgress('The Error Message is: ' + TRANSFORM(loError.MESSAGE), LOGIT)
					.TrackProgress('The Error occurred in Program: ' + TRANSFORM(loError.PROCEDURE), LOGIT)
					.TrackProgress('The Error occurred at line #: ' + TRANSFORM(loError.LINENO), LOGIT)
					lnNumberOfErrors = lnNumberOfErrors + 1
				ENDTRY

			ELSE
				.TrackProgress('Did not send status email: THIS.lSendInternalEmailIsOn = FALSE.',LOGIT)
			ENDIF

		ENDWITH
		RETURN
	ENDFUNC && main


	PROCEDURE ProcessDateRange
		LPARAMETERS tdFromDate, tdToDate
		WITH THIS

				***
				* We want to get the same data that is in runtotal.frx, run from runotal.scx. But instead of producing totals for each day for each Division,
				* we want to have totals by Revenue Type for each day for each Division.  
				*
				* The Revenue Types are those found in revacct.frx run from salesrptbyglcompany.scx.
				
				* get same data used by the runtotal.frx report.
				
				LOCAL i, xx, lcCSVFilename

		
				PRIVATE xstartdt, xenddt  && , xnumcomp
				
				xenddt = tdToDate
				xstartdt = tdFromDate

				*xnumcomp = alen(ainvcomp,1)-1
				
				IF (xenddt = DATE()) THEN			
					lcCSVFilename = 'S:\Qlikview\Receivables\qvrecv_' + DTOS(xstartdt) + '_To_Date.CSV'
				ELSE
					lcCSVFilename = 'S:\Qlikview\Receivables\qvrecv_' + DTOS(xstartdt) + "-" + DTOS(xenddt) + '.CSV'
				ENDIF
				
				DO PLATINV

				select xplatinv
				delete for revacct="1205"	&& dy 3/30/10

				select xplatinv
				
				*xx=reccount("xplatinv")
				scan && for recno()<=xx
					do case
					case revacct="5475"
						replace group with "COMMISSIONS" IN xplatinv
					case revacct="5550"
						replace group with "MANAGE FEES" IN xplatinv
					case inlist(revacct,"6200","1415") or (revacct="5800" and company#"L")
						replace group with "RENT/UTILITIES" IN xplatinv
					case inlist(revacct,"4000","5900","1","6","8")
						replace group with "MISC REVENUE" IN xplatinv
					endcase
				ENDSCAN
				
				* join to add compname and glname field to xplatinv
				SELECT a.*, NVL(c.name,' ') AS compname, NVL(c.division,' ') AS division, NVL(b.description,' ') AS glname, YEAR(a.invdate) AS YEAR, PADR(CMONTH(a.invdate),10) AS MONTH ;
				FROM xplatinv a ;
				LEFT OUTER JOIN glacct b ;
				ON b.glcode == a.glcode ;
				LEFT OUTER JOIN division c ;
				ON c.company = a.company ;
				INTO CURSOR	xplatinv2 ;
				ORDER BY a.invdate, c.name, b.description ;
				READWRITE
								
				SELECT xplatinv2
				COPY TO (lcCSVFilename) csv
				*browse
				
				IF FILE(lcCSVFilename) THEN
					.TrackProgress('Created file: ' + lcCSVFilename,LOGIT+SENDIT)				
				ELSE
					.TrackProgress('===> There was an error creating file: ' + lcCSVFilename,LOGIT+SENDIT)
				ENDIF

		ENDWITH
	ENDPROC  &&  ProcessDateRange


	PROCEDURE TrackProgress
		* do any combination of Wait Window, writing to logfile, and adding to body of email,
		* based on nFlags parameter.
		LPARAMETERS tcExpression, tnFlags
		WITH THIS
			IF BITAND(tnFlags,LOGIT) = LOGIT THEN
				IF .lLoggingIsOn THEN
					?
					? .cProcessName + "  " + TTOC(DATETIME()) + ": " + tcExpression
				ENDIF
			ENDIF
			IF .lWaitWindowIsOn THEN
				IF BITAND(tnFlags,NOWAITIT) = NOWAITIT THEN
					WAIT WINDOW tcExpression NOWAIT
				ENDIF
				IF BITAND(tnFlags,WAITIT) = WAITIT THEN
					WAIT WINDOW tcExpression TIMEOUT .nWaitWindowTimeout
				ENDIF
			ENDIF
			IF BITAND(tnFlags,SENDIT) = SENDIT THEN
				IF .lSendInternalEmailIsOn THEN
					.cBodyText = .cBodyText + tcExpression + CRLF + CRLF
				ENDIF
			ENDIF
		ENDWITH
	ENDPROC  &&  TrackProgress


ENDDEFINE
