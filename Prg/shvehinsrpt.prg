LPARAMETERS tcVehno, tcVehicleType, tdFromDate, tdToDate
*ON ERROR

#DEFINE CRLF CHR(13) + CHR(10)

LOCAL lcCentury, lnYearsInService, lnTotOil, lnTotParts, lnTotTires
LOCAL ARRAY laOrdhdr(1)
PRIVATE pdFromDate, pdToDate, pcXtraNotes
pdFromDate = tdFromDate
pdToDate = tdToDate

lcCentury = SET('CENTURY')

SET CENTURY ON

IF NOT USED('ordhdr')
	USE SH!ordhdr IN 0
ENDIF

IF NOT USED('repdetl')
	USE SH!repdetl IN 0 ORDER repdate
ENDIF

IF NOT USED('tiredetl')
	USE SH!tiredetl IN 0 ORDER tiredate
ENDIF

IF NOT USED('oildetl')
	USE SH!oildetl IN 0 ORDER oildate
ENDIF

SELECT ordhdr
SET ORDER TO vehno
SEEK tcVehno
IF !FOUND()
	=MESSAGEBOX("Couldn't find this vehicle in the Work Orders ",0+48,"Fleet Maintenance")
	RETURN
ENDIF

IF USED('temp') THEN
	USE IN temp
ENDIF

SELECT ordhdr
=AFIELDS(laOrdhdr)
CREATE CURSOR temp FROM ARRAY laOrdhdr
ALTER TABLE temp ADD COLUMN start_mile N(10)
ALTER TABLE temp ADD COLUMN end_mile   N(10)
ALTER TABLE temp ADD COLUMN st_date    d
ALTER TABLE temp ADD COLUMN end_date   d
ALTER TABLE temp ADD COLUMN service    N(4,2)
ALTER TABLE temp ADD COLUMN oilsvc     N(4)
ALTER TABLE temp ADD COLUMN repairsvc  N(4)
ALTER TABLE temp ADD COLUMN tiresvc    N(4)
ALTER TABLE temp ADD COLUMN pmsvc      N(4)
ALTER TABLE temp ADD COLUMN wsvc       N(4)
ALTER TABLE temp ADD COLUMN washsvc    N(4)
ALTER TABLE temp ADD COLUMN vehtype    C(8)

SELECT ordhdr
SET ORDER TO vehord
SCAN FOR vehno = tcVehno AND crdate >= tdFromDate AND crdate <= tdToDate
	WAIT WINDOW "Processing Work Order " + TRIM(STR(ordno)) NOWAIT
	SELECT ordhdr
	SCATTER MEMVAR MEMO
	IF EMPTY(m.workdesc) THEN
		m.workdesc = "NO WORK DESCRIPTION"
	ENDIF
	m.workdesc = "Work Description:" + CRLF + ALLTRIM(m.workdesc)
	INSERT INTO temp FROM MEMVAR

	STORE 0 TO lnTotOil, lnTotParts, lnTotTires

	SELECT oildetl
	SET ORDER TO ordno
	SEEK ordhdr.ordno
	IF FOUND()
		m.workdesc = m.workdesc + CRLF + CRLF + "Oil:" + CRLF + ;
			"Part#                Description                    Unit         Qty      Price  Ext Price" + CRLF + ;
			"-------------------- ------------------------------ ---------- ----- ---------- ----------"
		SCAN FOR ordno = ordhdr.ordno
			lnTotOil = lnTotOil + (oilqty * unitprice)

			m.workdesc = m.workdesc + CRLF + ;
				PADR(partno,21) + ;
				PADR(partdesc,31) + ;
				PADR(unitdim,11) + ;
				PADR(TRANSFORM(oilqty,"99999"),6) + ;
				PADR(TRANSFORM(unitprice,"9999999.99"),11) + ;
				TRANSFORM(extprice,"9999999.99")

		ENDSCAN
	ENDIF
	LOCATE

	SELECT repdetl
	SET ORDER TO ordno
	SEEK ordhdr.ordno
	IF FOUND()
		m.workdesc = m.workdesc + CRLF + CRLF + "Repairs:" + CRLF + ;
			"Part#                Description                    Unit         Qty      Price  Ext Price" + CRLF + ;
			"-------------------- ------------------------------ ---------- ----- ---------- ----------"
		SCAN FOR ordno = ordhdr.ordno

			lnTotParts = lnTotParts + (repqty * unitprice)

			m.workdesc = m.workdesc + CRLF + ;
				PADR(partno,21) + ;
				PADR(partdesc,31) + ;
				PADR(unitdim,11) + ;
				PADR(TRANSFORM(repqty,"99999"),6) + ;
				PADR(TRANSFORM(unitprice,"9999999.99"),11) + ;
				TRANSFORM(extprice,"9999999.99")

		ENDSCAN
	ENDIF
	LOCATE

	SELECT tiredetl
	SET ORDER TO ordno
	SEEK ordhdr.ordno
	IF FOUND()
		m.workdesc = m.workdesc + CRLF + CRLF + "Tires:" + CRLF + ;
			"Part#                Description                    Position     Qty      Price  Ext Price" + CRLF + ;
			"-------------------- ------------------------------ ---------- ----- ---------- ----------"
		SCAN FOR ordno = ordhdr.ordno
			lnTotTires = lnTotTires + (tireqty * unitprice)

			m.workdesc = m.workdesc + CRLF + ;
				PADR(partno,21) + ;
				PADR(partdesc,31) + ;
				PADR(tirepos,11) + ;
				PADR(TRANSFORM(tireqty,"99999"),6) + ;
				PADR(TRANSFORM(unitprice,"9999999.99"),11) + ;
				TRANSFORM(extprice,"9999999.99")

		ENDSCAN
	ENDIF
	LOCATE

	REPLACE temp.tirecost WITH lnTotTires, temp.partcost WITH lnTotParts, temp.oilcost WITH lnTotOil, temp.workdesc WITH m.workdesc

ENDSCAN

IF RECCOUNT("TEMP") = 0 THEN
	=MESSAGEBOX("No Work Orders for the Date Range entered.",0+48,"Fleet Maintenance")
	USE IN TEMP
	RETURN
ENDIF

* put inspects and washes with summary info
pcXtraNotes = ""
SELECT inspects
LOCATE FOR equip_no = tcVehno AND insp_date >= tdFromDate AND insp_date <= tdToDate
IF FOUND()
	pcXtraNotes = pcXtraNotes + CRLF + CRLF + "Inspections in the Activity Period:" + CRLF + ;
		"Date" + CRLF + ;
		"----------"
	SCAN FOR equip_no = tcVehno AND insp_date >= tdFromDate AND insp_date <= tdToDate
		pcXtraNotes = pcXtraNotes + CRLF + DTOC(insp_date)
	ENDSCAN
ENDIF
LOCATE

SELECT washes
LOCATE FOR equip_no = tcVehno AND wash_date >= tdFromDate AND wash_date <= tdToDate
IF FOUND()
	pcXtraNotes = pcXtraNotes + CRLF + CRLF + "Washes in the Activity Period:" + CRLF + ;
		"Date" + CRLF + ;
		"----------"
	SCAN FOR equip_no = tcVehno AND wash_date >= tdFromDate AND wash_date <= tdToDate
		pcXtraNotes = pcXtraNotes + CRLF + DTOC(wash_date)
	ENDSCAN
ENDIF
LOCATE

SELECT ordhdr
SEEK tcVehno

DO WHILE ordhdr.vehno = tcVehno
	SKIP 1
ENDDO

SKIP -1
SELECT temp
REPLACE ALL end_mile WITH ordhdr.mileage, end_date WITH ordhdr.crdate

SELECT ordhdr
SEEK tcVehno

SELECT temp
REPLACE ALL start_mile WITH ordhdr.mileage, st_date WITH ordhdr.crdate
SKIP -1
lnYearsInService = (temp.end_date - temp.st_date)/365
REPLACE ALL temp.service WITH lnYearsInService

SELECT temp
COUNT FOR mainttype = "P" TO pmService
COUNT FOR mainttype = "O" TO oilService
COUNT FOR mainttype = "R" TO repairService
COUNT FOR mainttype = "T" TO tireService
*!*	COUNT FOR mainttype = "W" TO wartyService
*!*	COUNT FOR mainttype = "C" TO WashService
COUNT FOR mainttype = "W" TO WashService
wartyService = 0.00

REPLACE ALL temp.oilsvc WITH oilService, ;
	temp.repairsvc WITH repairService, ;
	temp.tiresvc   WITH tireService, ;
	temp.wsvc      WITH wartyService, ;
	temp.pmsvc     WITH pmService, ;
	temp.washsvc   WITH WashService

SELECT temp
REPLACE ALL vehtype WITH tcVehicleType
GOTO TOP

WAIT CLEAR
KEYBOARD '{CTRL+F10}' CLEAR
REPORT FORM sh_ins_rpt PREVIEW NOCONSOLE

SET CENTURY &lcCentury

IF USED('temp') THEN
	USE IN temp
ENDIF

RETURN
