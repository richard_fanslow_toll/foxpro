* WIP BY PICK TICKET REPORT	for Alpha6
*
* 04/05/2017 MB
*
* per Chris Malcolm, this is the report from the 'wip report' button on form SHIPMENT, 
* with default filters (all carriers + undelivered) for Alpha6.
*
* EXE = F:\AUTO\ALPHA6WIPBYPTRPT.EXE
*
* Run daily at 4:30pm
*
* Modified 1/10/2018 per CM:  Any PT's marked as NW or CX in the "Stat" column should no longer appear on this report.
* 4/30/2018 MB: changed fmiint.com to toll email


runack("ALPHA6WIPBYPTRPT")

IF HOLIDAY(DATE()) THEN
	schedupdate()
	RETURN
ENDIF

LOCAL lnError, lcProcessName, loALPHA6WIPBYPTRPT

DECLARE INTEGER CreateMutex IN Win32API ;
	STRING @lpMutexAttributes, ;
	STRING bInitialOwner, ;
	STRING @lpName

DECLARE INTEGER GetLastError IN Win32API

* if running in EXE check to see if already running
IF _VFP.STARTMODE > 0 THEN
	lcProcessName = "ALPHA6WIPBYPTRPT"
	CreateMutex(0,CHR(1),@lcProcessName)
	lnError = GetLastError()
	IF lnError = 183
		WAIT WINDOW TIMEOUT 10 "ALPHA6WIPBYPTRPT..."
		RETURN .F.
	ENDIF
ENDIF

utilsetup("ALPHA6WIPBYPTRPT")


loALPHA6WIPBYPTRPT = CREATEOBJECT('ALPHA6WIPBYPTRPT')
loALPHA6WIPBYPTRPT.MAIN()

schedupdate()

CLOSE DATABASES ALL
RETURN

#DEFINE LOGIT 1
#DEFINE WAITIT 2
#DEFINE NOWAITIT 4
#DEFINE SENDIT 8
#DEFINE CRLF CHR(13) + CHR(10)
#DEFINE LINEFEED CHR(10)
#DEFINE RETURN_DATA_MANDATORY .F.
#DEFINE RETURN_DATA_NOT_MANDATORY .T.

DEFINE CLASS ALPHA6WIPBYPTRPT AS CUSTOM

	cProcessName = 'ALPHA6WIPBYPTRPT'

	lTestMode = .F.  && .F. FOR NORMAL OPERATION, .T. TO SUPPRESS ALL BUT DEVELOPER EMAILS AND CHANGE INPUT TABLE, ETC.

	lAutoYield = .T.

	cStartTime = TTOC(DATETIME())

	* processing properties
	cProcessDesc = "Alpha 6 WIP by PT Report process process"
	cAccountID = '5726'

	* date properties
	dtNow = DATETIME()

	dToday = DATE()

	cToday = DTOC(DATE())

	* wait window properties
	nWaitWindowTimeout = 2
	lWaitWindowIsOn = .T.

	* logfile properties
	lLoggingIsOn = .T.
	cLogFile = 'F:\UTIL\ALPHA6\LOGFILES\ALPHA6WIPBYPTRPT_LOG.TXT'

	* INTERNAL email properties
	lSendInternalEmailIsOn = .T.
	cFrom = 'Mark.Bennett@Tollgroup.com'
	*cSendTo = 'mbennett@fmiint.com'
	cSendTo = 'Judy@arctixbottoms.com, Lauren@arctixbottoms.com, Sheik@arctixbottoms.com, May@arctixbottoms.com'
	cCC = 'Mark.Bennett@Tollgroup.com'
	cSubject = ''
	cAttach = ''
	cBodyText = ''
	cTopBodyText = ''

	cCOMPUTERNAME = ''
	cUSERNAME = ''

	FUNCTION INIT
		IF NOT DODEFAULT()
			RETURN .F.
		ENDIF
		WITH THIS
			CLEAR
			*SET RESOURCE OFF
			CLOSE DATA
			SET CENTURY ON
			SET DATE AMERICAN
			SET HOURS TO 24
			SET ANSI ON
			SET TALK OFF
			SET DELETED ON
			SET CONSOLE OFF
			SET EXCLUSIVE OFF
			SET SAFETY OFF
			SET EXACT OFF
			SET STATUS BAR ON
			SET SYSMENU OFF
			SET ENGINEBEHAVIOR 70
			_VFP.AUTOYIELD = .lAutoYield
			.cCOMPUTERNAME = GETENV("COMPUTERNAME")
			.cUSERNAME = GETENV("USERNAME")
			IF .lTestMode THEN
				.cSendTo = 'Mark.Bennett@Tollgroup.com'
				.cCC = ''
				.cLogFile = 'F:\UTIL\ALPHA6\ALPHA6WIPBYPTRPT_LOG_TESTMODE.TXT'
			ENDIF
			.lLoggingIsOn = .lLoggingIsOn AND NOT EMPTY(.cLogFile)
		ENDWITH
	ENDFUNC


	FUNCTION DESTROY
		WITH THIS
			IF .lLoggingIsOn  THEN
				SET ALTERNATE OFF
				SET ALTERNATE TO
			ENDIF
		ENDWITH
		DODEFAULT()
	ENDFUNC


	FUNCTION MAIN

		WITH THIS
			LOCAL lnNumberOfErrors, lnAccountID, ldToday, lcSQL, lcPDF

			TRY
				lnNumberOfErrors = 0

				IF .lLoggingIsOn THEN
					SET ALTERNATE TO (.cLogFile) ADDITIVE
					SET ALTERNATE ON
				ENDIF

				.cSubject = .cProcessDesc + ' for ' + DTOC(.dToday)

				.TrackProgress(.cProcessDesc + ' started....', LOGIT+SENDIT+NOWAITIT)
				.TrackProgress('COMPUTERNAME = ' + .cCOMPUTERNAME, LOGIT+SENDIT)
				.TrackProgress('USERNAME = ' + .cUSERNAME, LOGIT+SENDIT)
				.TrackProgress('PROJECT = ALPHA6WIPBYPTRPT', LOGIT+SENDIT)
				IF .lTestMode THEN
					.TrackProgress('======> TEST MODE <========', LOGIT+SENDIT)
				ENDIF
				
				PUBLIC goffice, gwipbybl, gwipbywo, gwipbycombwo, goption
				goffice = 'I'
				STORE .F. TO gwipbybl, gwipbywo, gwipbycombwo
				goption = 'EXCEL'

				xsqlexec("select * from account where inactive=0","account",,"qq")
				SELECT account
				index on acctname tag acctname
				index on accountid tag accountid
				set order to

				xsqlexec("select * from outship where mod='I' and accountid=" + .cAccountID,"outship",,"wh")

				xsqlexec("select * from outship where mod='I' and accountid=" + .cAccountID,"outshippre",,"wh")

				* MB: added [AND (NOT notonwip)] where condition 1/11/2018 per CM to clean up report
				select ship_ref, wo_num, space(20) as xbolgroup, outshipid, outwologid, wo_date, sp, notonwip, ;
					routedfor, del_date, accountid, consignee, ship_via, bol_no, spduedt, pulled, cnee_ref, ;
					keyrec, picked, appt, dept, ptdate, labeled, qty, ctnqty, weight, staged, pulleddt, ;
					cuft, start, cancel, appt_num, called, storenum, asncode, fromupload, appt_time, picknpack, ;
					duns, name, address, address2, csz, apptremarks, apptstatus, masterpack, combinedpt, scac, ;
					cacctnum, terms, vendor_num, div, expdeldt, shipfor, sforstore, sforaddr1, sforaddr2, ;
					sforcsz, swcnum, space(19) as asnsent, wavenum, dcnum, upsacctnum, carrcode, rout_code, ; 
					prereceive, noshow, delenterdt, delenterby, swcover, truckloaddt, stageenterby, stageenterdt, ;
					stageenterproc, startenterby, startenterdt, startenterproc, cancelenterby, cancelenterdt, ;
					cancelenterproc, blpod, b2, cancelrts, cancelrtsdt, xferacct, .f. as mark, .f. as updatemark, ;
					.f. as carson, 0000 as pq, .f. as scanpackdone, .f. as pickfromsp, space(40) as sppickfrom, ;
					avoidship, batch_num, addby, adddt, updateby, updatedt, space(10) as procstatus, ;
					space(1) as ordstatus, container, vas, {} as zstart, {} as zcancel, {} as zappt, ;
					space(5) as zappt_time, space(20) as zappt_num from outshippre into cursor voutship ;
					WHERE emptynul(del_date) ;
					AND (NOT notonwip) ;
					readwrite


*!*					SELECT voutship
*!*					BROWSE


				SELECT voutship
				SCAN
					replace procstatus with iif(!pulled,"NOT PULLED",iif(!emptynul(del_date),"DELIVERED",iif(!emptynul(truckloaddt),"LOADED",iif(!emptynul(staged),"STAGED",iif(!emptynul(labeled),"LABELED",iif(!emptynul(picked),"PICKED","PULLED")))))), ;
						ordstatus with iif(!pulled,"1",iif(!emptynul(del_date),"9",iif(!emptynul(truckloaddt),"8",iif(!emptynul(staged),"7",iif(!emptynul(labeled),"6",iif(!emptynul(picked),"3","2")))))) in voutship
				ENDSCAN

				
				PRIVATE	xheader, xacctname, xtitle, xpnpacct, xwip
				xheader = 'TGF INC.'
				xacctname = 'ALPHA 6 DISTRIBUTORS'
				xtitle = 'Work in Progress'
				xpnpacct = .T.
				xwip = .T.
				
				SELECT voutship
				LOCATE
				IF EOF('voutship') THEN
				
					.cTopBodyText = 'There was no data to report today!' + CRLF + CRLF + '<<processing log follows>>' + CRLF
				
				ELSE

					.cTopBodyText = 'See attached WIP by PT report.' + CRLF + CRLF + '<<processing log follows>>' + CRLF


					select xacctname as acctname, *, 0000 as ptcnt from voutship into cursor xrpt where .f. readwrite

					select voutship
					scan 
						scatter memvar memo

						m.ptcnt=1
						if m.combinedpt
							select sum(1) as ptcnt from outship where wo_num=voutship.wo_num and combpt=voutship.ship_ref into cursor xtemp
							m.ptcnt=xtemp.ptcnt
							use in xtemp
						endif

						insert into xrpt from memvar
					endscan

	*!*					select xrpt
	*!*					BROWSE
					
					lcPDF = 'C:\TEMPFOX\WIPBYPTRPT_' + DTOS(DATE()) + '.PDF'
					
					SELECT xrpt
					*REPORT FORM m:\dev\rpt\whappts PREVIEW
					*REPORT FORM whappts PREVIEW
					=RPT2PDF('whappts',lcPDF)
					
					IF FILE(lcPDF) THEN
						* attach output file to email
						.cAttach = lcPDF
					ELSE
						.TrackProgress('ERROR: unable to attach report: ' + lcPDF, LOGIT+SENDIT)
					ENDIF
					
				ENDIF && EOF('voutship')
				

				.TrackProgress(.cProcessDesc + ' ended normally....', LOGIT+SENDIT+NOWAITIT)

			CATCH TO loError

				.TrackProgress('There was an error.',LOGIT+SENDIT)
				.TrackProgress('The Error # is: ' + TRANSFORM(loError.ERRORNO), LOGIT+SENDIT)
				.TrackProgress('The Error Message is: ' + TRANSFORM(loError.MESSAGE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred in Program: ' + TRANSFORM(loError.PROCEDURE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred at line #: ' + TRANSFORM(loError.LINENO), LOGIT+SENDIT)
				lnNumberOfErrors = lnNumberOfErrors + 1
				IF TYPE('.oExcel') = 'O' AND NOT ISNULL(.oExcel) THEN
					.oExcel.QUIT()
				ENDIF

			ENDTRY

			WAIT CLEAR
			***************** INTERNAL email results ******************************
			.TrackProgress('About to send status email.',LOGIT)
			.TrackProgress('==================================================================================================================', SENDIT)
			.TrackProgress('The logfile is: ' + .cLogFile, LOGIT+SENDIT)
			.TrackProgress(.cProcessDesc + ' started: ' + .cStartTime, LOGIT+SENDIT)
			.TrackProgress(.cProcessDesc + ' finished: ' + TTOC(DATETIME()), LOGIT+SENDIT)

			IF .lSendInternalEmailIsOn THEN
				* try to trap error from not having dartmail dll's registered on user's pc...
				TRY
					.cBodyText = .cTopBodyText + CRLF + .cBodyText

					DO FORM dartmail2 WITH .cSendTo,.cFrom,.cSubject,.cCC,.cAttach,.cBodyText,"A"
					.TrackProgress('Sent status email.',LOGIT)
				CATCH TO loError
					.TrackProgress('There was an error sending the status email.',LOGIT)
					.TrackProgress('The Error # is: ' + TRANSFORM(loError.ERRORNO), LOGIT)
					.TrackProgress('The Error Message is: ' + TRANSFORM(loError.MESSAGE), LOGIT)
					.TrackProgress('The Error occurred in Program: ' + TRANSFORM(loError.PROCEDURE), LOGIT)
					.TrackProgress('The Error occurred at line #: ' + TRANSFORM(loError.LINENO), LOGIT)
					lnNumberOfErrors = lnNumberOfErrors + 1
				ENDTRY

			ELSE
				.TrackProgress('Did not send status email: THIS.lSendInternalEmailIsOn = FALSE.',LOGIT)
			ENDIF

		ENDWITH
		RETURN
	ENDFUNC && main
	
	
		
	
	PROCEDURE TrackProgress
		* do any combination of Wait Window, writing to logfile, and adding to body of email,
		* based on nFlags parameter.
		LPARAMETERS tcExpression, tnFlags
		WITH THIS
			IF BITAND(tnFlags,LOGIT) = LOGIT THEN
				IF .lLoggingIsOn THEN
					?
					? .cProcessName + "  " + TTOC(DATETIME()) + ": " + tcExpression
				ENDIF
			ENDIF
			IF .lWaitWindowIsOn THEN
				IF BITAND(tnFlags,NOWAITIT) = NOWAITIT THEN
					WAIT WINDOW tcExpression NOWAIT
				ENDIF
				IF BITAND(tnFlags,WAITIT) = WAITIT THEN
					WAIT WINDOW tcExpression TIMEOUT .nWaitWindowTimeout
				ENDIF
			ENDIF
			IF BITAND(tnFlags,SENDIT) = SENDIT THEN
				IF .lSendInternalEmailIsOn THEN
					.cBodyText = .cBodyText + tcExpression + CRLF + CRLF
				ENDIF
			ENDIF
		ENDWITH
	ENDPROC  &&  TrackProgress
	

ENDDEFINE
