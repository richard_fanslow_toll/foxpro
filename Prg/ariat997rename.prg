DO m:\dev\prg\_setvars WITH .T.
CD "F:\FTPUSERS\ARIAT\997Out\hold\"
len1 = ADIR(aryariat,"*.edi")
IF len1 = 0
	WAIT WINDOW "No 997s to process..." TIMEOUT 1
	CLOSE DATABASES ALL
	RETURN
ENDIF

lTesting = .F. && Change to .F. for production 997 creation.

_SCREEN.CAPTION = "Ariat 997 Rename Process"
_SCREEN.WINDOWSTATE = IIF(lTesting,2,1)
cFileExt = ".edi"
cString = ""
FOR uy = 1 TO len1
	cFilenameIn = ALLTRIM(aryariat[uy])
	stringin = FILETOSTR(cFilenameIn)
	stringin = STRTRAN(stringin,"~","~"+CHR(13))
	STRTOFILE(stringin,"WFile.997")
	DELETE FILE [&cFilenameIn]
	CREATE CURSOR temp1 (f1 c(254))
	SELECT temp1
	APPEND FROM WFile.997 TYPE SDF
	DELETE FOR EMPTY(ALLTRIM(temp1.f1))
	DELETE FILE WFile.997
	LOCATE
	ASSERT .F.
	nRecs = RECCOUNT()
	LOCATE
	IF lTesting
		SET STEP ON
	ENDIF
	SCAN
		cF1 = ALLTRIM(f1)
		IF cF1 = "ISA"
			cTimeString = TTOC(DATETIME(),1)
			cOutfilename = "997ariat"+cTimeString+cFileExt
			cArchiveFile = LOWER("F:\FTPUSERS\ARIAT\997Out\archive\"+cOutfilename)
			cOutfile = LOWER("F:\FTPUSERS\ARIAT\997Out\"+cOutfilename)
			nFnum = FCREATE(cOutfile)
			nFnum2 = FCREATE(cArchiveFile)
		ENDIF

		FPUTS(nFnum,cF1)
		FPUTS(nFnum2,cF1)

		IF cF1 = "IEA"
			WAIT WINDOW "" TIMEOUT 1
			IF lTesting
*				SET STEP ON
			ENDIF
			FCLOSE(nFnum)
			FCLOSE(nFnum2)
		ENDIF
	ENDSCAN
	WAIT WINDOW "Process complete for "+cOutfilename TIME 1
	CD "F:\FTPUSERS\ARIAT\997Out\"
	IF lTesting
		COPY FILE *.edi TO "F:\FTPUSERS\ARIAT\997OUT\testout\*.edi"
	ELSE
		COPY FILE *.edi TO "F:\FTPUSERS\ARIAT\OUT\*.edi"
	ENDIF
	DELETE FILE *.*
ENDFOR

RETURN
