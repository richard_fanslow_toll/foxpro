*runack("SYNCLAIRE945CONFIRM")

SET ESCAPE ON
ON ESCAPE CANCEL

CLEAR
CLEAR ALL
CLOSE DATABASES ALL
PUBLIC nAccountid,cFilename,dconfirm_dt,lTesting,dDaysBack,cOffice,cState,tsendto,tcc,lUnconfirmed,nfor,lTesting,lEmail

USE F:\3pl\DATA\edi_trigger IN 0
DO m:\dev\prg\_setvars WITH .T.
DO m:\dev\prg\lookups

lTesting = .T.
lEmail = .T.
cOffice = "C"

_SCREEN.WINDOWSTATE = IIF(lTesting,0,1)
*utilsetup("SYNCLAIRE945CONFIRM")

IF !lTesting
	SELECT 0
	USE F:\edirouting\ftpsetup SHARED
	IF ftpsetup.chkbusy AND !lOverridebusy
		WAIT WINDOW "Process is busy...exiting" TIMEOUT 2
		RETURN
	ENDIF
	REPLACE chkbusy WITH .T. FOR ftpsetup.TRANSFER = "997-SYNCLAIRE-IN-SP"
	USE IN ftpsetup
ENDIF
cState = "SP"

WAIT WINDOW "Now Checking Received Confirmations in EDI_TRIGGER for State "+cState TIMEOUT 2
check_trigger_confirm()

IF lTesting
	SELECT edi_trigger
	BROWSE FOR INLIST(edi_trigger.accountid,&gbcnyrecaccounts) AND !ack945 AND when_proc>DATETIME(2016,10,01,00,00,01)
ENDIF

IF !lTesting
	SELECT 0
	USE F:\edirouting\ftpsetup SHARED
	REPLACE chkbusy WITH .F. FOR ftpsetup.TRANSFER = "997-SYNCLAIRE-IN-SP"
	USE IN ftpsetup
ENDIF
CLOSE DATA ALL
WAIT WINDOW "Process complete" TIMEOUT 2

*******************************
PROCEDURE check_trigger_confirm
*******************************
*SET STEP ON
	IF lTesting
		ASSERT .F. MESSAGE "In Trigger Confirm Mailing"
	ENDIF
	WAIT WINDOW "Now scanning EDI_TRIGGER ("+cState+") for overdue confirmations" NOWAIT
	SELECT accountid,"San Pedro" AS warehouse,bol,ship_ref,trig_time ;
		FROM edi_trigger ;
		WHERE INLIST(accountid,&gbcnyrecaccounts) ;
		AND BETWEEN(TTOD(trig_time),DATE()-10,DATE()) ;
		AND edi_type = "945" ;
		AND !EMPTY(when_proc) ;
		AND when_proc < (DATETIME()-3600) ;
		AND !errorflag ;
		AND fin_status # "NO 945" ;
		AND EMPTY(confirm_dt) ;
		AND office = cOffice ;
		INTO CURSOR tempconf
	SELECT tempconf
	LOCATE
	IF EOF()
		WAIT WINDOW "No unconfirmed 945s for "+cState TIMEOUT 2
		lUnconfirmed = .F.
	ELSE
		COPY TO ("c:\tempfox\sync_noconfirm"+cState)
		LOCATE
		lUnconfirmed = .T.
		IF lTesting
			BROWSE
		ENDIF

		WAIT WINDOW "Errors found...creating Excel error file and mailing" TIMEOUT 3
		cExcelFile = ("F:\3pl\DATA\missconfirms_"+cState+".xls")
		COPY TO [&cExcelFile] TYPE XL5
	ENDIF

	IF lUnconfirmed
		tsubject= "Unconfirmed BCNY/Synclaire 945s: Office "+cOffice
		tattach = cExcelFile
		tmessage = "The attached Excel file contains PTs for which we have not received BCNY/Synclaire 997s."
	ELSE
		tsubject= "No unconfirmed BCNY/Synclaire 945s: Office "+cOffice+" ("+cState+")"
		tattach = ""
		tmessage = "We have received BCNY/Synclaire 997s for all sent 945s."
	ENDIF
	tFrom ="Toll EDI Operations <toll-edi-ops@tollgroup.com>"
	SELECT 0
	USE F:\3pl\DATA\mailmaster ALIAS mm SHARED
	IF lTesting
		LOCATE FOR edi_type = "MISC" AND taskname = "JOETEST"
	ELSE
		LOCATE FOR edi_type = "MISC" AND taskname = "SYNC945CFM"
	ENDIF
	tsendto = IIF(mm.use_alt,sendtoalt,sendto)
	tcc = IIF(mm.use_alt,ccalt,cc)
	USE IN mm

	IF lEmail
		DO FORM m:\dev\frm\dartmail2 WITH tsendto,tFrom,tsubject,tcc,tattach,tmessage,"A"
	ENDIF
	IF FILE(cExcelFile)
		DELETE FILE [&cExcelFile]
	ENDIF
	USE IN tempconf
ENDPROC
