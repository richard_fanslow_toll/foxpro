*!* MARC JACOBS WHLS 945 (Whse. Shipping Advice) Creation Program
*!* Creation Date: 12.08.2011 by Joe


PARAMETERS cbol,nacctnum,cboldt,coffice

PUBLIC ARRAY thisarray(1)
PUBLIC c_cntrlnum,c_grpcntrlnum,creturned,cwo_num,cwo_numstr,nwo_num1,ltesting,cisa_num,lupdated,lsinglebol,lOverflow
PUBLIC lemail,ldomjfilesout,ddatetimecal,nfilenum,tsendto,tcc,tsendtoerr,tccerr,lpsu,cprogname,normalexit,cerrmsg
PUBLIC lups,lfedex,tccwhse,csendid,cmailname
PUBLIC cmod,cmbol,cfilenameshort,cfilenameout,cfilenamearch,lparceltype,cwo_numlist,lcpath,ceditype,cisa_num
cprocname = "marcjacobsw945_create"
WAIT WIND "At start of standard MJW process" TIMEOUT 2

ltesting   = .F.  && Set to .t. for testing
ltestinput = .F.  &&  Set to .t. for test input files only!
ldocompare = .F.
*WAIT WINDOW "" TIMEOUT 1 WITH '04302015114101
DO m:\dev\prg\_setvars WITH ltesting
ON ESCAPE CANCEL
cerrmsg = ""

IF (VARTYPE(coffice) # "C") OR (!INLIST(coffice,"J","L","N"))
	DO CASE
		CASE ltestinput
			coffice = "P"
		CASE ltesting
			coffice = "N"
		OTHERWISE
			WAIT WINDOW "Must pass an office or a correct office............."
			RETURN
	ENDCASE
ENDIF
cmod = IIF(coffice="N","J",coffice)
cmbol = ""
lparceltype = .F.
ceditype = "945"
norigseq = ""

cprogname = "marcjacobsw945_create"
STORE "" TO tattach,cship_ref,ccustname,cwo_numstr,cwo_numlist,tcc
liserror = .F.
lcloseoutput = .T.
nfilenum = 0
lpick = .T.
lprepack = !lpick
normalexit = .F.
lupdated = .F.
ctrknumber = ""
lOverflow=.F.
twotracksbol = '1Z6E751A0105013638'


WAIT WINDOW "At the start of MARC JACOBS WHLS 945 STD. PICKPACK process..." NOWAIT
***** added 4/14/16 TMARG  intercept HUdson Bay small carrier to perform rollup into one BOL per PO
IF ltesting   = .F.
**DHW - NC**	IF INLIST(nacctnum,6303,6543) AND consignee='HUDSON' AND INLIST(scac,'FD','FE','U','CTDP') AND edi_trigger.fin_status !="945 ROLLUP NEEDED "
	IF INLIST(nacctnum,6303,6543) AND INLIST(consignee,'HUDSON','NORDSTROM CANADA','NORDSTROM RACK CANADA') AND INLIST(scac,'FD','FE','U','CTDP') AND edi_trigger.fin_status !="945 ROLLUP NEEDED "
		IF !USED("edi_trigger")
			USE F:\3pl\DATA\edi_trigger IN 0
		ENDIF
		SELECT edi_trigger
*!*	DHW   processed/errorflag must be set to T/F so that future iterations of the 945create program do not look at the ROLLUP NEEDED
*!*	DHW   records until after mj_hbc_rollup has had a chance to update the BOL. (mj_hbc_rollup will update the processed flag to .F.

			REPLACE edi_trigger.processed WITH .T.,edi_trigger.errorflag WITH .F., edi_trigger.fin_status WITH "945 ROLLUP NEEDED " ;
				FOR INLIST(accountid,6303,6543) AND INLIST(consignee,'HUDSON','NORDSTROM CANADA','NORDSTROM RACK CANADA') AND INLIST(scac,'FD','FE','U','CTDP') AND edi_trigger.edi_type = "945 " ;
				AND edi_trigger.processed =.F. IN edi_trigger
**DHW - NC**		REPLACE edi_trigger.processed WITH .T.,edi_trigger.errorflag WITH .F., edi_trigger.fin_status WITH "945 ROLLUP NEEDED " ;
**DHW - NC**			FOR INLIST(accountid,6303,6543) AND consignee = 'HUDSON' AND INLIST(scac,'FD','FE','U','CTDP') AND edi_trigger.edi_type = "945 " ;
**DHW - NC**			AND edi_trigger.processed =.F. IN edi_trigger
		closedata()
		RETURN
	ENDIF
ELSE
ENDIF


useca("ackdata","wh")

TRY
	lfederated = .F.
	IF VARTYPE(nacctnum) = "L"
		nacctnum = 6303
	ENDIF

	IF nacctnum = 6453
		IF !USED("edi_trigger")
			USE F:\3pl\DATA\edi_trigger IN 0
		ENDIF
		SELECT edi_trigger
		REPLACE edi_trigger.processed WITH .T.,edi_trigger.errorflag WITH .F., edi_trigger.fin_status WITH "NO 945 NEEDED  RETAIL SUPPLIES" ;
			FOR edi_trigger.bol = cbol AND edi_trigger.accountid = 6453 AND edi_trigger.edi_type = "945 " IN edi_trigger
		closedata()
		RETURN
	ENDIF

	cppname = "MarcJacobsW"
	tfrom = "Toll EDI Operations <toll-edi-ops@tollgroup.com>"
	tmessage=""
	STORE .F. TO lparceltype,lpsu,lsqlmail,lups,lfedex,lfegrd
	lisaflag = .T.
	lstflag = .T.
	nloadid = 1

	IF ltesting && AND lTestinput
*!* TEST DATA AREA
		CLOSE DATABASES ALL
		IF !USED('edi_trigger')
			cedifolder = "F:\3PL\DATA\"
			USE (cedifolder+"edi_trigger") IN 0 ALIAS edi_trigger
		ENDIF
		cbol = "04070086303162109"
		ctime = DATETIME()
	ENDIF
	CREATE CURSOR temp945mj (accountid i,isa_num c(10),st_num c(10),wo_num i,bol_no c(20),ship_ref c(20),filename c(50),filedate T)
	cbol=TRIM(cbol)

	IF ltestinput
		cusefolder = "F:\WHP\WHDATA\"
	ELSE
		xreturn = "XXX"
		DO m:\dev\prg\wf_alt WITH cmod,nacctnum
		cusefolder = UPPER(xreturn)
	ENDIF

	IF ltesting
		WAIT WINDOW "Folder Used: "+cusefolder TIMEOUT 1
	ENDIF

	lemail = .T.
	ltestmail = ltesting && Sends mail to Joe only
	ldomjfilesout = !ltesting && If true, copies output to FTP folder (default = .t.)
	lstandalone = ltesting
	STORE "LB" TO cweightunit

*!*  lTestMail = .t.
*!*  lDoMJFilesOut = .F.  && Keep set to .f.; Files in 945-Staging will be externally checked for duplicates and moved to the OUT folder

	WAIT WINDOW "Now preparing tables and variables" NOWAIT NOCLEAR
	STORE "" TO lcpath,lcarchivepath,lcholdpath

*!* SET CUSTOMER CONSTANTS
	ccustname = "MARCJACOBSW"  && Customer Identifier
	cx12 = "004010"  && X12 Standards Set used
	cmailname = "Marc Jacobs (W)"
	SELECT 0
	USE F:\3pl\DATA\mailmaster ALIAS mm
	LOCATE FOR mm.edi_type = "945" AND mm.office=IIF(INLIST(coffice,"J","P"),"N",coffice) AND mm.accountid = 6303
	lcpath        = ALLTRIM(mm.basepath)
	lcarchivepath = ALLTRIM(mm.archpath)
	lcholdpath    = ALLTRIM(mm.holdpath)
	tsendto       = ALLTRIM(IIF(mm.use_alt,sendtoalt,sendto))
	tcc           = ALLTRIM(IIF(mm.use_alt,ccalt,cc))

*  WAIT WINDOW "Mailing to: "+tsendto+CHR(13)+tcc TIMEOUT 2
	LOCATE
	LOCATE FOR mm.edi_type = 'MISC' AND mm.taskname = "MJADDWHSE"
	tccwhse = IIF(mm.use_alt,ccalt,cc)
	LOCATE
	LOCATE FOR mm.edi_type = 'MISC' AND mm.taskname = "MJTEST"
	tsendtotest = IIF(mm.use_alt,sendtoalt,sendto)
	tcctest = IIF(mm.use_alt,ccalt,cc)
	tsendtoerr = IIF(mm.use_alt,sendtoalt,sendto)
	tccerr = IIF(mm.use_alt,ccalt,cc)
	USE IN mm

	IF ltesting
		IF DATETIME() < DATETIME(2017,06,26,17,00,00)
			tsendto ="joe.bianchi@tollgroup.com,todd.margolin@tollgroup.com"
			tsendtotest ="joe.bianchi@tollgroup.com"
			tsendtoerr ="joe.bianchi@tollgroup.com"
		ELSE
			tsendto = "pgaidis@fmiint.com,tmarg@fmiint.com"
			tsendtotest = "pgaidis@fmiint.com"
			tsendtoerr = "pgaidis@fmiint.com,tmarg@fmiint.com"
		ENDIF
		tcc=""
		tcctest = ""
		tccerr = ""
	ENDIF

*!* SET OTHER CONSTANTS
	DO CASE
		CASE cmod = "J"
			ccustloc =  "NJ"
			cfmiwarehouse = ""
			ccustprefix = "945"+"j"
			cdivision = "New Jersey"
			cfolder = "WHN"
			csf_addr1  = "800 FEDERAL BLVD"
			csf_csz    =  "CARTERET*NJ*07008"
			csenderid  = 'USNJ1DC'
			crcvid    = '2129070080'
			cfilepath = 'F:\FTPUSERS\MJ_Wholesale\WMS_Outbound\945Out\'

		CASE coffice = "L"
			ccustloc =  "CA"
			cfmiwarehouse = ""
			ccustprefix = "945"+"c"
			cdivision = "California"
			cfolder = "WHC"
			csf_addr1  = "3355 DULLES DRIVE"
			csf_csz    =  "MIRA LOMA*CA*91752"
			csenderid  = 'USCA1DC'
			crcvid    = '2129070080'
			cfilepath = 'F:\FTPUSERS\MJ_Wholesale_CA\WMS_Out\945Out\'

		CASE coffice = "M"
			ccustloc =  "FL"
			cfmiwarehouse = ""
			ccustprefix = "945"+"f"
			cdivision = "Florida"
			cfolder = "WHM"
			csf_addr1  = "11400 NW 32ND AVE"
			csf_csz    = "MIAMI*FL*33167"

		OTHERWISE
			ccustloc =  "CA"
			cfmiwarehouse = ""
			ccustprefix = "945"+"c"
			cdivision = "California"
			cfolder = "WHC"
			csf_addr1  = "450 WESTMONT DRIVE"
			csf_csz    = "SAN PEDRO*CA*90731"
			cfilepath = 'F:\FTPUSERS\MJ_Wholesale_CA\WMS_Outbound\945Out\'
	ENDCASE

	lnonedi = .F.  && Assumes not a Non-EDI 945
	STORE "" TO lckey
	STORE 0 TO alength,nlength
	ctime = DATETIME()
	cdeltime = SUBSTR(TTOC(ctime,1),9,4)
	ccarriertype = "M" && Default as "Motor Freight"
	dapptnum = ""

*!* SET OTHER CONSTANTS
	IF INLIST(coffice,'C','L')
		ddatetimecal = (DATETIME()-(3*3600))
	ELSE
		ddatetimecal = DATETIME()
	ENDIF
	dt1 = TTOC(ddatetimecal,1)
	dtmail = TTOC(ddatetimecal)
	dt2 = ddatetimecal
	cstring = ""
	DO CASE
		CASE INLIST(coffice,'J','N')
			csendqual = "ZZ"
			csendid = "USNJ1DC"
			crecqual = "12"
			crecid = "2129070080"
		CASE coffice='L'
			csendqual = "ZZ"
			csendid = "USCA1DC"
			crecqual = "12"
			crecid = "2129070080"
		CASE coffice='P'
			csendqual = "ZZ"
			csendid = "USCA1DC"
			crecqual = "12"
			crecid = "2129070080"
	ENDCASE
	cfd = "*"
	csegd = "~"
	nsegctr = 0
	cterminator = ">" && Used at end of ISA segment
	cdate = DTOS(DATE())
	ctruncdate = RIGHT(cdate,6)
	ctrunctime = SUBSTR(TTOC(ddatetimecal,1),9,4)
	cfiledate = cdate+ctrunctime

	DO CASE
		CASE INLIST(coffice,'N','J')
			corig = "J"
		CASE coffice='L'
			corig = "L"
		CASE coffice='P'
			corig = "P"
	ENDCASE

	nstcount = 0
	cstyle = ""
	cptstring = ""
	ntotctnwt = 0
	ntotctncount = 0
	ctargetstyle = ""

** PAD ID Codes
	csendidlong = PADR(csendid,15," ")
	crecidlong = PADR(crecid,15," ")

	ccustfolder = "MarcJacobs"
	cfilenamehold = (lcholdpath+ccustprefix+dt1+".txt")
	cfilenameshort = JUSTFNAME(cfilenamehold)
	cfilenameout = (lcpath+cfilenameshort)
	cfilenamearch = (lcarchivepath+cfilenameshort)
	nfilenum = FCREATE(cfilenamehold)

	IF USED('OUTWOLOG')
		USE IN outwolog
	ENDIF

	lookups()
*gmjacctlist

	IF USED('SHIPMENT')
		USE IN shipment
	ENDIF

	IF USED('PACKAGE')
		USE IN package
	ENDIF

	lookups()
	goffice=cmod
	gmasteroffice = coffice

	ddate = DATE()-30
	csq1 = [select * from shipment where right(rtrim(str(accountid)),4) in (]+gmjacctlist+[) and shipdate >= {]+DTOC(ddate)+[}]
	xsqlexec(csq1,,,"wh")
	INDEX ON shipmentid TAG shipmentid
	INDEX ON pickticket TAG pickticket

	SELECT shipment

	LOCATE
	IF RECCOUNT("shipment") > 0
		xjfilter="shipmentid in ("
		SCAN
			nshipmentid = shipment.shipmentid
			xsqlexec("select * from package where shipmentid="+TRANSFORM(nshipmentid),,,"wh")

			IF RECCOUNT("package")>0
				xjfilter=xjfilter+TRANSFORM(shipmentid)+","
			ELSE
				xjfilter="1=0"
			ENDIF
		ENDSCAN
		xjfilter=LEFT(xjfilter,LEN(xjfilter)-1)+")"

		xsqlexec("select * from package where "+xjfilter,,,"wh")
	ELSE
		xsqlexec("select * from package where .f.",,,"wh")
	ENDIF
	SELECT package
	INDEX ON shipmentid TAG shipmentid
	SET ORDER TO TAG shipmentid

	IF USED('OUTSHIP')
		USE IN outship
	ENDIF
	IF USED('outdet')
		USE IN outdet
	ENDIF

	xsqlexec("select * from outship where accountid = "+TRANSFORM(nacctnum)+" and bol_no = '"+cbol+"'",,,"wh")
	SELECT outship
	INDEX ON bol_no TAG bol_no
	INDEX ON ship_ref TAG ship_ref
	INDEX ON wo_num TAG wo_num

	selectoutdet()
	SELECT outdet
	INDEX ON outdetid TAG outdetid

	IF !USED("scacs")
		USE ("f:\3pl\data\parcel_carriers") IN 0 ALIAS scacs ORDER TAG scac
	ENDIF

	IF !USED('nofreight')
		USE F:\3pl\DATA\mj_nofreight IN 0 ALIAS nofreight
	ENDIF

	IF !USED('sbaccts')
		USE F:\3pl\DATA\mjsinglebolaccts IN 0 ALIAS sbaccts
	ENDIF



	IF !SEEK(cbol,'outship','bol_no')
		WAIT WINDOW "BOL not found in OUTSHIP" TIMEOUT 2
		DO ediupdate WITH "BOL NOT FOUND",.T.
		THROW
	ELSE
		nwo_num = outship.wo_num
		cscac = ALLTRIM(outship.scac)
		IF !EMPTY(cscac) AND outship.bol_no # '742446234610'
			IF SEEK(cscac,'scacs','scac')
				lparceltype = .T.
				ccarriertype = "U"
				lups = scacs.ups
				lfedex = scacs.fedex
				lfegrd = IIF(lfedex AND "GROUND"$scacs.dispname,.T.,.F.)
			ENDIF
		ELSE
			REPLACE outship.scac WITH "CUST" IN outship FOR outship.bol_no = cbol AND EMPTY(outship.scac)
		ENDIF
	ENDIF

&&  why this
	IF ALLTRIM(outship.keyrec)=ALLTRIM(outship.bol_no) AND !ISBLANK(outship.keyrec)
		lparceltype = .F.
	ENDIF

	IF lparceltype AND lfedex AND !EMPTY(outship.cacctnum)
		cfedexcode = ICASE(cscac = "FEPL","fdx",INLIST(cscac,"FDE ","FDEB"),'fdn','fdf')
		lsinglebol =  .F.
		ccacctnum = ALLTRIM(outship.cacctnum)
		ccons = ALLTRIM(outship.consignee)
		coname = ALLTRIM(outship.NAME)

		IF SEEK(ccacctnum,'sbaccts','cacctnum')
			WAIT WINDOW "This is a consolidated parcel 'BOL' acct. num#" TIMEOUT 1
			lsinglebol =  .T.
		ENDIF
	ENDIF

	SELECT dcnum FROM outship WHERE outship.bol_no = cbol GROUP BY 1 INTO CURSOR tempx
	LOCATE
	IF EOF() OR EMPTY(tempx.dcnum)
		USE IN tempx
		SELECT outship
		REPLACE dcnum WITH '9999',storenum WITH 9999 FOR outship.bol_no = cbol AND EMPTY(outship.dcnum) IN outship
		SELECT dcnum FROM outship WHERE outship.bol_no = cbol GROUP BY 1 INTO CURSOR tempx
*    xsqlexec("update outship set dcnum = '9999',storenum = 9999 where bol_no = '"+cBOL+"' and dcnum = ' '",,,"wh")
*    xsqlexec("select dcnum from outship where bol_no = '"+cBOL+"'","tempx",,"wh")
	ENDIF
	IF EMPTY(tempx.dcnum)
		USE IN tempx
		SELECT storenum FROM outship WHERE outship.bol_no = cbol GROUP BY 1 INTO CURSOR tempx
		IF EOF()
			DO ediupdate WITH "EMPTY DC/STORE#",.T.
			THROW
		ENDIF

		IF EMPTY(tempx.storenum)
			DO ediupdate WITH "EMPTY DC/STORE#",.T.
			THROW
		ELSE
			IF RECCOUNT('tempx') > 1
				DO ediupdate WITH "MULTI DC NUMS",.T.
				THROW
			ENDIF
		ENDIF
	ENDIF
	IF USED('tempx')
		USE IN tempx
	ENDIF

	ldocompare = .F. && these are all scanpack
	IF ldocompare
		cretmsg = "X"
		DO m:\dev\prg\sqldata-comparemj WITH cusefolder,cbol,nwo_num,coffice,nacctnum,cppname
		IF cretmsg<>"OK"
			lcloseoutput = .T.
			cwo_num = ALLTRIM(STR(nwo_num1))
			DO ediupdate WITH "BAD SQL COMPARE",.T.
			THROW
		ENDIF
	ENDIF

&& Added code blocks to the following to overcome BOL vs. WO selection issues, 12.23.2005
	IF lparceltype
		SELECT 0
		USE F:\3pl\DATA\mjw_wonum
		IF mjw_wonum.wo_num # nwo_num
			REPLACE mjw_wonum.wo_num WITH nwo_num IN mjw_wonum
			USE IN mjw_wonum
		ENDIF
	ENDIF

	SELECT outship
	LOCATE
	IF USED("sqlwomj")
		USE IN sqlwomj
	ENDIF
	DELETE FILE "F:\3pl\DATA\sqlwomj.dbf"
	SELECT bol_no,wo_num ;
		FROM outship WHERE bol_no = cbol ;
		GROUP BY 1,2 ;
		ORDER BY 1,2 ;
		INTO DBF F:\3pl\DATA\sqlwomj
	USE IN sqlwomj
	USE F:\3pl\DATA\sqlwomj IN 0 ALIAS sqlwomj

	cretmsg = ""

	DO m:\dev\prg\sqlconnectmj_bol  WITH nacctnum,ccustname,cppname,.T.,coffice

	IF cretmsg<>"OK"
		DO ediupdate WITH cretmsg,.T.
		THROW
	ENDIF

	SELECT outship
	LOCATE
	WAIT CLEAR
	WAIT WINDOW "Now creating Header information" NOWAIT NOCLEAR

*!* HEADER LEVEL EDI DATA
	DO num_incr_isa

	cisa_num = PADL(c_cntrlnum,9,"0")
	nisa_num = INT(VAL(cisa_num))

	IF ltesting
		cisacode = "T"
	ELSE
		cisacode = "P"
	ENDIF

	STORE "ISA"+cfd+"00"+cfd+"          "+cfd+"00"+cfd+"          "+cfd+csendqual+cfd+csendidlong+cfd+;
		crecqual+cfd+crecidlong+cfd+ctruncdate+cfd+ctrunctime+cfd+"U"+cfd+"00401"+cfd+;
		cisa_num+cfd+"0"+cfd+cisacode+cfd+cterminator+csegd TO cstring
	currentisanum = cisa_num
	DO cstringbreak

	STORE "GS"+cfd+"SW"+cfd+csendid+cfd+crecid+cfd+cdate+cfd+ctrunctime+cfd+c_cntrlnum+;
		cfd+"X"+cfd+cx12+csegd TO cstring
	lccurrentgroupnum = c_cntrlnum
	DO cstringbreak

	SELECT outship
	SET ORDER TO wo_num
	LOCATE

	IF !SEEK(nwo_num)
		WAIT CLEAR
		WAIT WINDOW "Invalid Work Order Number - Not Found in OUTSHIP!" TIMEOUT 2
		DO ediupdate WITH "WO NOT FOUND",.T.
		THROW
	ENDIF

	IF !ltesting
		IF EMPTY(cbol)  && May be empty if triggered from PT screen, or FedEx shipment
			WAIT CLEAR
			WAIT WINDOW "MISSING Bill of Lading Number!" TIMEOUT 2
			DO ediupdate WITH "BOL# EMPTY",.T.
			THROW
		ELSE
			IF !SEEK(PADR(TRIM(cbol),20),"outship","bol_no")
				WAIT CLEAR
				WAIT WINDOW "Invalid Bill of Lading Number - Not Found!" TIMEOUT 2
				DO ediupdate WITH "BOL# NOT FOUND",.T.
				THROW
			ENDIF
		ENDIF
	ENDIF

*************************************************************************
*1  PTIDSOURCE/OUTSHIP RECORD MAIN LOOP
*  Loops through individual OUTSHIP lines for BOL#
*************************************************************************
	WAIT CLEAR
	WAIT WINDOW "Now creating BOL#-based information..." NOWAIT NOCLEAR

	SELECT outship
	SET ORDER TO
	IF ltesting
		COUNT TO N FOR outship.bol_no = cbol
	ELSE
		COUNT TO N FOR outship.bol_no = cbol AND !emptynul(del_date)
	ENDIF
	IF N=0
		DO ediupdate WITH "INCOMP BOL, Del Date",.T.
		THROW
	ENDIF

	IF ltesting
		oscanstr = "outship.bol_no = cBOL"
	ELSE
		oscanstr = "outship.bol_no = cBOL AND !EMPTYnul(del_date)"
	ENDIF

	LOCATE
	SCAN FOR &oscanstr
		SCATTER MEMVAR MEMO
		IF outship.qty = 0 OR (outship.ctnqty = 0 AND !outship.masterpack)
			LOOP
		ENDIF
		alength = ALINES(apt,outship.shipins,.T.,CHR(13))
		lmacy = IIF("MACY"$m.consignee,.T.,.F.)
		ljcp = IIF("PENNEY"$m.consignee,.T.,.F.)
		luniform = IIF(m.consignee='UNIFORM ACCOUNT',.T.,.F.)
		nwo_num = outship.wo_num
		cwo_num = ALLTRIM(STR(nwo_num))
		IF !(cwo_num$cwo_numstr)
			cwo_numstr = IIF(EMPTY(cwo_numstr),cwo_num,cwo_numstr+","+cwo_num)
		ENDIF
		IF !(cwo_num$cwo_numlist)
			cwo_numlist = IIF(EMPTY(cwo_numlist),cwo_num,cwo_numlist+CHR(13)+cwo_num)
		ENDIF
		IF !ltestinput
			xsqlexec("select * from outwolog where accountid = "+TRANSFORM(nacctnum)+" and wo_num = "+TRANSFORM(nwo_num),,,"wh")
			LOCATE
			lpick = outwolog.picknpack
			lprepack = !lpick
			USE IN outwolog
		ENDIF
		ntotctncount = m.qty
		cpo_num = ALLTRIM(outship.cnee_ref)
		cship_ref = ALLTRIM(outship.ship_ref)

		IF LEFT(cbol,3)="PSU" AND "MACY"$outship.consignee
			lpsu = .T.
		ENDIF

*!* Added this code to trap miscounts in OUTDET Units

		IF INLIST(cship_ref,'130142185','130142209')
			SET STEP ON
		ENDIF
		SELECT outdet
		SET ORDER TO
		SUM totqty TO nunittot1 FOR units AND outdet.outshipid = outship.outshipid


		IF lprepack
			SUM (VAL(PACK)*totqty) TO nunittot2 FOR !units AND outdet.outshipid = outship.outshipid
			IF nunittot1<>nunittot2
				SET STEP ON
				WAIT WINDOW "TOTQTY ERROR AT PT "+cship_ref TIMEOUT 5
				DO ediupdate WITH "OUTDET TOTQTY ERR, PT# "+cship_ref,.T.
				THROW
			ENDIF
		ENDIF

		SELECT vmarcjacobswpp
		SUM totqty TO nunittot2 FOR vmarcjacobswpp.outshipid = outship.outshipid AND ucc <> 'CUTS'
		IF nunittot1<>nunittot2
			SET STEP ON
			DO ediupdate WITH "SQL TOTQTY ERR, PT# "+cship_ref,.T.
			THROW
		ENDIF

		SELECT outdet
		SET ORDER TO outdetid
		LOCATE

*!* End code addition

		IF (("PENNEY"$UPPER(outship.consignee)) OR ("KMART"$UPPER(outship.consignee)) OR ("K-MART"$UPPER(outship.consignee)))
			lapptflag = .T.
		ELSE
			lapptflag = .F.
		ENDIF

		IF ltestinput OR ltesting
			ddel_date = DATE()
		ELSE
			ddel_date = outship.del_date
		ENDIF

		IF ltestinput
			dapptnum = "99999"
			dapptdate = DATE()
		ELSE
			dapptnum = ALLTRIM(outship.appt_num)

			IF EMPTY(dapptnum) && Penney/KMart Appt Number check
				IF (!(lapptflag) OR (DATE()={^2007-08-20} AND cbol = "04907314677036340"))
					dapptnum = ""
				ELSE
					DO ediupdate WITH "EMPTY APPT #",.T.
					THROW
				ENDIF
			ENDIF
			dapptdate = outship.appt

			IF EMPTY(dapptdate)
				dapptdate = IIF(ltesting,DATE(),outship.del_date)
			ENDIF
		ENDIF

		IF ALLTRIM(outship.sforcsz) = ","
			BLANK FIELDS outship.sforcsz NEXT 1 IN outship
		ENDIF

		ctrnum = ""
		cpronum = ""

		IF (("WALMART"$outship.consignee) OR ("WAL-MART"$outship.consignee) OR ("WAL MART"$outship.consignee))
			IF LEFT(outship.keyrec,2) = "PR"
				STORE TRIM(keyrec) TO cpronum
			ENDIF
			IF LEFT(outship.keyrec,2) = "TR"
				STORE TRIM(keyrec) TO ctrnum
			ENDIF
		ENDIF

		IF cbol = "99911111111111111"
			cship_ref = ALLTRIM(STRTRAN(cship_ref,"OVER",""))
		ENDIF

		noutshipid = m.outshipid
		cptstring = IIF(EMPTY(cptstring),m.consignee+" "+cship_ref,cptstring+CHR(13)+m.consignee+" "+cship_ref)

		ccountry = ""
		IF "CITY"$shipins AND "STATE"$shipins AND "ZIPCODE"$shipins AND "COUNTRY"$shipins
			ccity = segmentget(@apt,"CITY",alength)
			cstate =segmentget(@apt,"STATE",alength)
			czip =segmentget(@apt,"ZIPCODE",alength)
			ccountry =segmentget(@apt,"COUNTRY",alength)
			ccountry = IIF(EMPTY(ccountry),"US",ccountry) && default value
		ELSE
			ccountry = IIF(EMPTY(ccountry),"US",ccountry) && default value
			m.csz = TRIM(m.csz)

			IF EMPTY(M.csz)
				WAIT CLEAR
				WAIT WINDOW "No SHIP-TO City/State/ZIP info...exiting" TIMEOUT 2
				DO ediupdate WITH "BAD ADDRESS INFO",.T.
				THROW
			ENDIF
			m.csz = ALLTRIM(STRTRAN(m.csz,"  "," "))
			IF !","$m.csz
				DO ediupdate WITH "NO COMMA CSZ-"+cship_ref,.T.
				THROW
			ENDIF
			nspaces = OCCURS(" ",ALLTRIM(m.csz))
*      WAIT WINDOW "nSpaces = "+TRANSFORM(nSpaces) TIMEOUT 2
			IF nspaces = 0
				WAIT WINDOW "Ship-to CSZ Segment Error" TIMEOUT 1
				DO ediupdate WITH "Ship-to CSZ: "+cship_ref,.T.
				THROW
			ELSE
				ncommapos = AT(",",m.csz)
				nlastspace = AT(" ",m.csz,nspaces)


				IF ISALPHA(SUBSTR(TRIM(m.csz),AT(" ",m.csz,nspaces-1)+1,2))
					ccity = LEFT(TRIM(m.csz),AT(",",m.csz)-1)
					cstate = SUBSTR(m.csz,ncommapos+2,2)
					czip = TRIM(SUBSTR(m.csz,nlastspace+1))

				ELSE
					WAIT CLEAR
					WAIT WINDOW "NOT ALPHA: "+SUBSTR(TRIM(m.csz),AT(" ",m.csz,nspaces-1)+1,2) TIMEOUT 3
					ccity = LEFT(TRIM(m.csz),AT(",",m.csz)-1)
					cstate = SUBSTR(TRIM(m.csz),AT(" ",m.csz,nspaces-2)+1,2)
					czip = TRIM(SUBSTR(m.csz,nlastspace+1))

				ENDIF
			ENDIF
		ENDIF

		STORE "" TO csforcity,csforstate,csforzip,csforcountry
		IF !lfederated
			IF "SFORCITY"$shipins AND "SFORSTATE"$shipins AND "SFORZIPCODE"$shipins AND "SFORCOUNTRY"$shipins
				csforcity = segmentget(@apt,"SFORCITY",alength)
				csforstate =segmentget(@apt,"SFORSTATE",alength)
				csforzip =segmentget(@apt,"SFORZIPCODE",alength)
				csforcountry =segmentget(@apt,"SFORCOUNTRY",alength)
				csforcountry = IIF(EMPTY(csforcountry),"US",csforcountry) && default value
			ELSE
				IF !EMPTY(M.sforcsz)
					m.sforcsz = ALLTRIM(STRTRAN(m.sforcsz,"  "," "))
					nspaces = OCCURS(" ",ALLTRIM(m.sforcsz))
					IF nspaces = 0
						m.sforcsz = STRTRAN(m.sforcsz,",","")
						csforcity = ALLTRIM(m.sforcsz)
						csforstate = ""
						csforzip = ""
					ELSE
						ncommapos = AT(",",m.sforcsz)
						nlastspace = AT(" ",m.sforcsz,nspaces)
						nminusspaces = IIF(nspaces=1,0,1)
						IF ISALPHA(SUBSTR(TRIM(m.sforcsz),AT(" ",TRIM(m.sforcsz),nspaces-nminusspaces)+1,2))
							csforcity = LEFT(TRIM(m.sforcsz),AT(",",m.sforcsz)-1)
							csforstate = SUBSTR(m.sforcsz,ncommapos+2,2)
*              cSForZip = TRIM(m.SForCSZ)
							csforzip = TRIM(SUBSTR(m.sforcsz,nlastspace+1))
						ELSE
							WAIT CLEAR
							WAIT WINDOW "NOT ALPHA: "+SUBSTR(TRIM(m.sforcsz),AT(" ",m.sforcsz,nspaces-1)+1,2) NOWAIT
							csforcity = LEFT(TRIM(m.sforcsz),AT(",",m.sforcsz)-1)
							csforstate = SUBSTR(TRIM(m.sforcsz),AT(" ",m.sforcsz,nspaces-2)+1,2)
*              cSforZIP = TRIM(SUBSTR(m.SforCSZ,nLastSpace+1))
							csforzip = TRIM(m.sforcsz)
						ENDIF
					ENDIF
				ELSE
					cstorename = segmentget(@apt,"STORENAME",alength)
				ENDIF
			ENDIF
		ENDIF
		csforcountry = IIF(EMPTY(csforcountry),"US",csforcountry) && default value

		DO num_incr_st
		WAIT CLEAR
		WAIT WINDOW "Now creating Line Item information" NOWAIT NOCLEAR

		INSERT INTO temp945mj (accountid,isa_num,st_num,wo_num,bol_no,ship_ref,filename,filedate) ;
			VALUES (m.accountid,c_cntrlnum,c_grpcntrlnum,m.wo_num,m.bol_no,m.ship_ref,JUSTFNAME(cfilenameshort),dt2)

		m.groupnum = lccurrentgroupnum
		m.isanum = currentisanum
		m.transnum = PADL(c_grpcntrlnum,9,"0")
		m.edicode = "SW"
		m.accountid = m.accountid
		m.loaddt = DATE()
		m.loadtime = DATETIME()
		m.filename = cfilepath+JUSTFNAME(cfilenameshort)
		m.ship_ref = m.ship_ref
		m.senderid = csendid
		m.rcvid = crecid
		IF !ltesting
			insertinto("ackdata","wh",.T.)
			tu("ackdata")
		ELSE
		ENDIF
		STORE "ST"+cfd+"945"+cfd+PADL(c_grpcntrlnum,9,"0")+csegd TO cstring

		DO cstringbreak
		nstcount = nstcount + 1
		nsegctr = 1


		cstorenum = ALLTRIM(m.dcnum)
		IF EMPTY(TRIM(cstorenum))
			IF (!EMPTY(m.storenum) AND m.storenum<>0)
				IF LEN(ALLTRIM(STR(m.storenum))) < 5
					cstorenum = PADL(ALLTRIM(STR(m.storenum)),4,"0")
				ELSE
					cstorenum = ALLTRIM(STR(m.storenum))
				ENDIF
			ENDIF
			IF EMPTY(cstorenum)
				cstorenum = segmentget(@apt,"STORENUM",alength)
				IF EMPTY(cstorenum)
					DO ediupdate WITH "MISS STORENUM",.T.
					THROW
				ENDIF
			ENDIF
		ENDIF

		IF lfedex AND lsinglebol
			cfxdcin = ALLTRIM(segmentget(@apt,"STORENUM",alength))
			IF LEN(cfxdcin)>2 AND ISALPHA(cfxdcin)
				cfxdcnum = PADR(cfxdcin,6)
				IF SEEK(cfxdcnum,'sbaccts','center')

					cfx = ALLTRIM(sbaccts.&cfedexcode)
					cboldt = ALLTRIM(cfx)+cboldt
					RELEASE ALL LIKE cfx*
				ELSE
					cboldt = ALLTRIM(cstorenum)+cboldt
				ENDIF
			ELSE
				cboldt = ALLTRIM(cstorenum)+cboldt
			ENDIF
		ENDIF

		IF USED('sbaccts')
			USE IN sbaccts
		ENDIF
*Insert Into ACKDATA (groupnum,isanum,transnum,edicode,accountid,loaddt,loadtime,filename,ship_ref) Values (lcCurrentGroupNum,CurrentISANum,lcCurrentGroupNum,"SW",m.accountid,Date(),Datetime(),JUSTFNAME(cFilenameShort),m.ship_ref)
*STORE "ST"+cfd+"945"+cfd+PADL(c_GrpCntrlNum,9,"0")+csegd TO cString


		coutbol = IIF(lsinglebol,cboldt,cbol)

		STORE "W06"+cfd+"N"+cfd+cship_ref+cfd+cdate+cfd+TRIM(coutbol)+cfd+TRIM(cwo_num)+cfd+cpo_num+cfd+cship_ref+csegd TO cstring
		IF ALLTRIM(m.consignee)='AMAZON'  &&&& added 8/13/15  TM to send PRO num
			STORE "W06"+cfd+"N"+cfd+cship_ref+cfd+cdate+cfd+TRIM(coutbol)+cfd+TRIM(outship.keyrec)+cfd+cpo_num+cfd+cship_ref+csegd TO cstring
		ELSE
		ENDIF

		DO cstringbreak
		nsegctr = nsegctr + 1
		nctnnumber = 1  && Seed carton sequence count

		STORE "N1"+cfd+"SF"+cfd+"FMI INC."+cfd+"91"+cfd+"01"+csegd TO cstring
		DO cstringbreak
		nsegctr = nsegctr + 1

		IF !lfederated
			STORE "N3"+cfd+TRIM(csf_addr1)+csegd TO cstring
			DO cstringbreak
			nsegctr = nsegctr + 1

			STORE "N4"+cfd+csf_csz+cfd+"US"+csegd TO cstring
			DO cstringbreak
			nsegctr = nsegctr + 1
		ENDIF

		STORE "N1"+cfd+"ST"+cfd+ALLTRIM(m.consignee)+cfd+"92"+cfd+cstorenum+csegd TO cstring
		DO cstringbreak
		nsegctr = nsegctr + 1

		STORE "N3"+cfd+TRIM(outship.address)+csegd TO cstring
		DO cstringbreak
		nsegctr = nsegctr + 1

		STORE "N4"+cfd+ccity+cfd+cstate+cfd+czip+cfd+ccountry+csegd TO cstring
		DO cstringbreak
		nsegctr = nsegctr + 1

		IF !EMPTY(m.sforstore)
			STORE "N1"+cfd+"Z7"+cfd+ALLTRIM(m.shipfor)+cfd+"91"+cfd+TRIM(m.sforstore)+csegd TO cstring
			DO cstringbreak
			nsegctr = nsegctr + 1

			STORE "N3"+cfd+TRIM(m.sforaddr1)+csegd TO cstring
			DO cstringbreak
			nsegctr = nsegctr + 1

			IF !EMPTY(csforstate)
				STORE "N4"+cfd+csforcity+cfd+csforstate+cfd+csforzip+cfd+csforcountry+csegd TO cstring
			ELSE
				STORE "N4"+cfd+csforcity+csegd TO cstring
			ENDIF
			DO cstringbreak
			nsegctr = nsegctr + 1
		ENDIF

		STORE "N9"+cfd+"PGC"+cfd+"CTN25"+csegd TO cstring
		DO cstringbreak
		nsegctr = nsegctr + 1

		IF ljcp
			STORE "N9"+cfd+"PUA"+cfd+dapptnum+csegd TO cstring
			DO cstringbreak
			nsegctr = nsegctr + 1
		ENDIF

		IF !EMPTY(cpronum)
			STORE "N9"+cfd+"RE"+cfd+cpronum+csegd TO cstring
			DO cstringbreak
			nsegctr = nsegctr + 1
		ENDIF

*!* For JCPenney orders only
		IF cacctnum = "JCP" OR ("PENNEY"$UPPER(m.consignee))
			IF ltesting AND EMPTY(m.batch_num)
				cloadid = PADR(ALLTRIM(STR(nloadid)),6,"0")
				nloadid = nloadid + 1
			ELSE
				cloadid = ALLTRIM(m.batch_num)
				IF EMPTY(cloadid)
					cloadid = ALLTRIM(m.appt_num)
					IF EMPTY(cloadid)
						DO ediupdate WITH "MISS LOADID",.T.
						THROW
					ENDIF
				ENDIF
			ENDIF
			STORE "N9"+cfd+"P8"+cfd+TRIM(cloadid)+csegd TO cstring
			DO cstringbreak
			nsegctr = nsegctr + 1
		ENDIF

		IF "AMAZON"$outship.consignee
			STORE "N9"+cfd+"AO"+cfd+TRIM(outship.appt_num)+csegd TO cstring
			DO cstringbreak
			nsegctr = nsegctr + 1
		ENDIF
		IF "ZAPPO"$outship.consignee
			STORE "N9"+cfd+"AO"+cfd+TRIM(outship.appt_num)+csegd TO cstring
			DO cstringbreak
			nsegctr = nsegctr + 1
		ENDIF
* Added the two lines below to prevent freight charges for certain MJ accounts
		STORE ALLTRIM(m.cacctnum) TO ccacctnum
		lchargefreight = IIF((SEEK(ccacctnum,'nofreight','acctnum') OR LEFT(ccacctnum,3) = "IMA" OR LEFT(ccacctnum,3) = "NET"),.F.,.T.)

		STORE "G62"+cfd+"10"+cfd+TRIM(DTOS(ddel_date))+cfd+"A"+cfd+cdeltime+csegd TO cstring  && Ship date/time
		DO cstringbreak
		nsegctr = nsegctr + 1

		SELECT scacs
		IF ltesting AND (EMPTY(cscac) OR EMPTY(m.ship_via))
			STORE "TEST" TO cscac
			STORE "TEST SHIPPER" TO m.ship_via
		ENDIF

		cusescac = ICASE(lups,"UPSN",lfedex AND lfegrd,"FDEG",lfedex AND !lfegrd,"FDE",cscac)
		IF !EMPTY(ctrnum)
			STORE "W27"+cfd+ccarriertype+cfd+TRIM(cusescac)+cfd+TRIM(m.ship_via)+REPLICATE(cfd,2)+"TL"+;
				REPLICATE(cfd,2)+ctrnum+csegd TO cstring
		ELSE
			STORE "W27"+cfd+ccarriertype+cfd+TRIM(cusescac)+cfd+TRIM(m.ship_via)+csegd TO cstring
		ENDIF
		DO cstringbreak
		nsegctr = nsegctr + 1
&&this is the one - comment out for production
*   lParcelType = .f.  &&this is the one comment out for production
		IF !ltesting
			IF lparceltype
				IF lchargefreight
					cship_reffull = PADR(cship_ref,20)
					SELECT shipment
					IF SEEK(cship_reffull,"shipment","pickticket")
						SELECT package
						SUM package.pkgcharge TO nfreight FOR package.shipmentid=shipment.shipmentid
						cfreight = STRTRAN(ALLTRIM(STR(nfreight,10,2)),".","")
					ELSE
						DO ediupdate WITH "PT "+cship_ref+" not found in SHIPMENT table",.T.
						THROW
					ENDIF
					STORE "G72"+cfd+"504"+cfd+"06"+REPLICATE(cfd,6)+ALLT(cfreight)+csegd TO cstring
					DO cstringbreak
					nsegctr = nsegctr + 1
				ELSE
					cfreight = STRTRAN(ALLTRIM(STR(0,10,2)),".","")
					STORE "G72"+cfd+"504"+cfd+"06"+REPLICATE(cfd,6)+ALLT(cfreight)+csegd TO cstring
					DO cstringbreak
					nsegctr = nsegctr + 1
				ENDIF
			ENDIF
		ELSE
			cfreight = STRTRAN(ALLTRIM(STR(0,10,2)),".","")
			STORE "G72"+cfd+"504"+cfd+"06"+REPLICATE(cfd,6)+ALLT(cfreight)+csegd TO cstring
			DO cstringbreak
			nsegctr = nsegctr + 1
		ENDIF

		IF lparceltype
			SELECT shipment
			ntrk = 0
			cship_reffull = PADR(ALLTRIM(cship_ref),20)
			IF SEEK(cship_reffull,"shipment","pickticket")
				SELECT 0
				SELECT trknumber FROM package WHERE .F. INTO CURSOR tracknum READWRITE
				IF INLIST(cbol,(twotracksbol)) && Three trknumbers across two cartons
					SELECT ucc FROM package WHERE .F. INTO CURSOR trackref4 READWRITE
				ENDIF
				SELECT shipment
				LOCATE

				SCAN FOR shipment.pickticket = cship_reffull
					IF INLIST(cbol,(twotracksbol)) && Three trknumbers across two cartons
						SELECT ucc ;
							FROM package ;
							WHERE package.shipmentid = shipment.shipmentid ;
							INTO CURSOR temptrack2
						SELECT trackref4
						APPEND FROM DBF('temptrack2')
					ENDIF

					SELECT trknumber ;
						FROM package ;
						WHERE package.shipmentid = shipment.shipmentid ;
						INTO CURSOR temptrack
					SELECT tracknum
					APPEND FROM DBF('temptrack')

					SELECT shipment
				ENDSCAN
				USE IN temptrack
				SELECT tracknum
				ntrk = RECCOUNT()
				IF INLIST(cbol,(twotracksbol))
					SELECT trackref4
					IF ltesting
						BROWSE
					ENDIF
					SELECT DISTINCT ucc FROM trackref4 INTO CURSOR tempzzz
					USE IN trackref4
					LOCATE
					ntrk = RECCOUNT()
				ENDIF
				LOCATE
				IF ltesting
*  BROWSE
				ENDIF
				SELECT shipment
				LOCATE
			ELSE
				WAIT WINDOW "Pickticket "+cship_ref+" does not exist in SHIPMENT" TIMEOUT 2
				DO ediupdate WITH "NO PT "+cship_ref+": UPS",.T.
				THROW
			ENDIF
			SELECT DISTINCT ucc ;
				FROM vmarcjacobswpp ;
				WHERE vmarcjacobswpp.ship_ref = cship_reffull ;
				INTO CURSOR tempv
			SELECT tempv
			nvcnt = RECCOUNT()
			USE IN tempv
			IF nvcnt # ntrk
				SET STEP ON
				DO ediupdate WITH "SQL->UPS CTN COUNT OFF",.T.
				THROW
			ENDIF
			RELEASE vcnt
		ENDIF


*************************************************************************
*2  DETAIL LOOP - MATCHING SHIP_REF FIELD (FROM OUTSHIP)
*************************************************************************
		WAIT CLEAR
		WAIT WINDOW "Now creating Detail information "+cship_ref NOWAIT NOCLEAR

		SELECT vmarcjacobswpp
		SELECT outdet
		SET ORDER TO TAG outdetid
		SELECT vmarcjacobswpp
		SET RELATION TO outdetid INTO outdet
		STORE 0 TO nshipctntotqty,nshipdettotqty
		LOCATE
		LOCATE FOR ship_ref = TRIM(cship_ref) AND outshipid = noutshipid
		IF !FOUND()
			IF !ltesting
				WAIT WINDOW "PT "+cship_ref+" NOT FOUND in vmarcjacobswpp...ABORTING" TIMEOUT 2
				IF !ltesting
					lsqlmail = .T.
				ENDIF
				DO ediupdate WITH "MISS PT-SQL: "+cship_ref,.T.
				THROW
			ELSE
				LOOP
			ENDIF
		ENDIF

		scanstr = "vmarcjacobswpp.ship_ref = cShip_ref and vmarcjacobswpp.outshipid = nOutshipid"

		SELECT vmarcjacobswpp
		LOCATE
		IF ltesting
*      BROWSE
		ENDIF
		LOCATE FOR &scanstr
		ccartonnum= "XXX"
		DO WHILE &scanstr

			alength = ALINES(apt,outdet.printstuff,.T.,CHR(13))
			lskipback = .T.

			IF TRIM(vmarcjacobswpp.ucc) # ccartonnum
				STORE TRIM(vmarcjacobswpp.ucc) TO ccartonnum
				lchangectn = .T.
			ENDIF
			IF ltesting AND ccartonnum = '00008061660004203394'
			ENDIF

			STORE "LX"+cfd+ALLTRIM(STR(nctnnumber))+csegd TO cstring   && Carton Seq. # (up to Carton total)
			ldomansegment = .T.
			ldopalsegment = .T.
			DO cstringbreak
			nsegctr = nsegctr + 1

			DO WHILE vmarcjacobswpp.ucc = ccartonnum
				IF lparceltype && AND DATETIME() > tusetime
					SELECT tracknum
					ctrknumber = ALLTRIM(tracknum.trknumber)
					IF !EOF('tracknum') AND lchangectn
						lchangectn = .F.
						SKIP 1 IN tracknum
					ENDIF
					SELECT vmarcjacobswpp
				ELSE
					ctrknumber = cbol
				ENDIF

				IF vmarcjacobswpp.totqty = 0
					SELECT vmarcjacobswpp
					IF EOF()

					ENDIF
					SKIP 1 IN vmarcjacobswpp
					LOOP
				ENDIF

				cuccnumber = vmarcjacobswpp.ucc
				IF EMPTY(cuccnumber) OR ISNULL(cuccnumber)
					WAIT CLEAR
					WAIT WINDOW "Empty UCC Number in vmarcjacobswpp "+cship_ref TIMEOUT 2
					lsqlmail = .T.
					DO ediupdate WITH "EMPTY UCC# in "+cship_ref,.T.
					THROW
				ENDIF
				cuccnumber = TRIM(STRTRAN(TRIM(cuccnumber)," ",""))

				IF ldomansegment
					ldomansegment = .F.
					IF lparceltype
						IF ltesting
						ENDIF
						STORE "MAN"+cfd+"GM"+cfd+TRIM(cuccnumber)+cfd+cfd+cfd+ctrknumber+csegd TO cstring
					ELSE
						STORE "MAN"+cfd+"GM"+cfd+TRIM(cuccnumber)+csegd TO cstring
					ENDIF
					DO cstringbreak
					nsegctr = nsegctr + 1
					nshipctnqty = INT(VAL(outdet.PACK))
					IF nshipctnqty = 0 AND luniform  && Added per Juan, 01.23.2012
						nshipctnqty = 1
					ENDIF

					nshipctntotqty = nshipctntotqty+1
					cctnwt= "0.00" && init the weight value
					IF (EMPTY(outdet.ctnwt) OR outdet.ctnwt = 0) AND outdet.totqty > 0
						cctnwt = ALLTRIM(STR(INT(CEILING(outship.weight)/outship.ctnqty)))
						IF EMPTY(cctnwt) OR INT(VAL(cctnwt)) < 1
							cctnwt = '1'
*              DO ediupdate WITH "WT ERR: PT "+cShip_ref,.T.
*              THROW
						ENDIF
						ntotctnwt = ntotctnwt + INT(VAL(cctnwt))
					ELSE
						STORE ALLTRIM(STR(CEILING(outdet.ctnwt))) TO cctnwt
						ntotctnwt = ntotctnwt + outdet.ctnwt
					ENDIF

				ENDIF

				ccolor = TRIM(outdet.COLOR)
				csize = TRIM(outdet.ID)
				cupc = TRIM(outdet.upc)

				IF (ISNULL(cupc) OR EMPTY(cupc))
					cupc = TRIM(vmarcjacobswpp.upc)
				ENDIF

				cstyle = TRIM(outdet.STYLE)
				citemnum = TRIM(outdet.custsku)

				nshipdetqty = vmarcjacobswpp.totqty
				nshipdettotqty = nshipdettotqty+nshipdetqty
				IF ISNULL(nshipdetqty)
					nshipdetqty = outdet.totqty
					nshipdettotqty = nshipdettotqty+nshipdetqty
				ENDIF

				norigdetqty = vmarcjacobswpp.qty
				IF ISNULL(norigdetqty)
					norigdetqty = nshipdetqty
				ENDIF

				IF ldopalsegment
					STORE "PAL"+REPLICATE(cfd,11)+cctnwt+cfd+cweightunit+csegd TO cstring
					DO cstringbreak
					nsegctr = nsegctr + 1
					ldopalsegment = .F.
				ENDIF

				IF lparceltype AND lmacy
					STORE "N9"+cfd+"BM"+cfd+ctrknumber+csegd TO cstring
					DO cstringbreak
					nsegctr = nsegctr + 1
				ENDIF

*!* Changed the following to utilize original 940 unit codes from Printstuff field
				cunitcode = TRIM(segmentget(@apt,"UNITCODE",alength))
				IF EMPTY(cunitcode)
					cunitcode = "EA"
				ENDIF

*********** OK here we swap the style master values for retail shipment's 945s going back to AX2012
* removed dy 5/11/16

************************************************************************************************
				cshipstat = "SH"
				STORE "W12"+cfd+cshipstat+cfd+ALLTRIM(STR(norigdetqty))+cfd+ALLTRIM(STR(nshipdetqty))+cfd+;
					cfd+cunitcode+cfd+cfd+"UP"+cfd+cupc+REPLICATE(cfd,13)+"VN"+cfd+cstyle+csegd TO cstring
				DO cstringbreak
				nsegctr = nsegctr + 1

**** maybe add in here a upc,qty monitor to record exactly what goes into the 945
**** and then add in a check back to outdet at then end of the PT

				IF !EMPTY(ccolor)
					STORE "N9"+cfd+"VC"+cfd+ccolor+csegd TO cstring
					DO cstringbreak
					nsegctr = nsegctr + 1
				ENDIF

				IF !EMPTY(csize)
					STORE "N9"+cfd+"SZ"+cfd+csize+csegd TO cstring
					DO cstringbreak
					nsegctr = nsegctr + 1
				ENDIF

				SELECT vmarcjacobswpp
				SKIP 1 IN vmarcjacobswpp
			ENDDO

*** this is the end of this pick ticket
*** maybe add in the 945 qty to outdet check here ?

*!*        IF lParcelType AND DATETIME() > tUseTime
*!*          SELECT tracknum
*!*          IF !EOF()
*!*            SKIP 1 IN tracknum
*!*          ENDIF
*!*        ENDIF

			lskipback = .T.
			nctnnumber = nctnnumber + 1
		ENDDO

*************************************************************************
*  END DETAIL MATCHING SHIP_REF LOOP
*************************************************************************
		WAIT CLEAR
		WAIT WINDOW "Now creating Section Closure information" NOWAIT NOCLEAR
		STORE "W03"+cfd+ALLTRIM(STR(nshipdettotqty))+cfd+ALLTRIM(STR(ntotctnwt))+cfd+;
			cweightunit+cfd+cfd+cfd+ALLTRIM(STR(nshipctntotqty))+cfd+"CT"+csegd TO cstring   && Units sum, Weight sum, carton count

		ntotctnwt = 0
		ntotctncount = 0
		FWRITE(nfilenum,cstring)

		STORE  "SE"+cfd+ALLTRIM(STR(nsegctr+2))+cfd+PADL(c_grpcntrlnum,9,"0")+csegd TO cstring
		FWRITE(nfilenum,cstring)

		SELECT outship
		WAIT CLEAR
	ENDSCAN

	INSERT INTO F:\wh\mjfiles (TYPE,voucher,pickticket,bol,filename,WHEN) VALUES ("945","WHOLESALE","",cbol,cfilenamehold,DATETIME())


*************************************************************************
*1^  END OUTSHIP MAIN LOOP
*************************************************************************

	DO close945
	=FCLOSE(nfilenum)

	SET STEP ON

**had to comment out below for Hudson Bay test
	IF ltesting
*!*      CLOSE DATABASES ALL

*!*      THROW
	ENDIF

*!* Transfers files to correct output folders

	COPY FILE [&cFilenameHold] TO [&cFilenameArch]
	IF !FILE(cfilenamearch)
		DO ediupdate WITH "NO ARCHIVE CREATED",.T.
	ENDIF
	IF ldomjfilesout AND !ltesting
		COPY FILE [&cFilenameHold] TO [&cFilenameOut]
		IF !FILE(cfilenameout)
			DO ediupdate WITH "NO 945 OUT CREATED",.T.
		ENDIF
		DELETE FILE [&cFilenameHold]
	ENDIF

	SELECT temp945mj
	COPY TO "f:\3pl\data\temp945omj.dbf"
	USE IN temp945mj
*!*    SELECT 0
*!*    USE "f:\3pl\data\temp945omj.dbf" ALIAS temp945omj

	IF USED('pts_sent945') && Added this closure, 09.18.2014, Joe.
		USE IN pts_sent945
	ENDIF
	SELECT 0
	USE "f:\3pl\data\pts_sent945.dbf" ALIAS pts_sent945
	APPEND FROM "f:\3pl\data\temp945omj.dbf"
	USE IN pts_sent945
*!*    USE IN temp945omj
	DELETE FILE "f:\3pl\data\temp945omj.dbf"

	IF !ltesting
		SELECT edi_trigger
		DO ediupdate WITH "945 CREATED",.F.
		SELECT edi_trigger
		LOCATE

		IF !USED("ftpedilog")
			SELECT 0
			USE F:\edirouting\ftpedilog ALIAS ftpedilog
			INSERT INTO ftpedilog (transfer,ftpdate,filename,acct_name,TYPE) VALUES ("945-"+ccustname+"-"+ccustloc,dt2,cfilenamehold,UPPER(ccustname),"945")
			USE IN ftpedilog
		ENDIF
	ENDIF

	WAIT CLEAR

*!* Create eMail confirmation message
	IF ltestmail
		tsendto = tsendtotest
		tcc = tcctest
	ENDIF

	coutfolder = "FMI"+ccustloc

	tsubject = cmailname+" 945 EDI File from TGF as of "+dtmail+" ("+ccustloc+")"
	tattach = " "
	tmessage = "945 EDI Info from TGF, file "+cfilenameshort+CHR(13)
	tmessage = tmessage + "Division: "+cdivision+", BOL# "+TRIM(cbol)+CHR(13)
	tmessage = tmessage + "For Work Orders: "+cwo_numstr+","+CHR(13)
	tmessage = tmessage + "containing these picktickets:"+CHR(13)
	tmessage = tmessage + cptstring + CHR(13)
	tmessage = tmessage +"has been created and will be transmitted ASAP."+CHR(13)+CHR(13)
	tmessage = tmessage + "If you have any questions, please eMail or call 732-750-9000 x217."
	IF lemail
		DO FORM m:\dev\frm\dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
	ENDIF

	RELEASE ALL LIKE c_cntrlnum,c_grpcntrlnum
	WAIT CLEAR
	WAIT WINDOW cmailname+" 945 EDI File output complete" TIMEOUT 1

	asn_out_data()

	normalexit = .T.
CATCH TO oErr
	IF !normalexit
		SET STEP ON
		IF ltesting
*      return
			tsendto  = tsendtotest
			tcc = tcctest
			tsendto  = "pgaidis@fmiint.com,doug.wachs@tollgroup.com"
			tcc = ""
		ELSE
			tsendto  = tsendtoerr
			tcc = tccerr
		ENDIF

		tmessage = ccustname+" ERROR processing "+CHR(13)

		IF !lupdated
			IF BETWEEN(oErr.LINENO,542,546)
				tmessage =tmessage+"MISSING COMMA OR OTHER ERROR IN CSZ FIELD"+CHR(13)
			ELSE
				DO ediupdate WITH UPPER(ALLTRIM(oErr.MESSAGE)),.T.
				tsubject = "ERROR " +ccustname+" ERROR ("+TRANSFORM(oErr.ERRORNO)+") at "+TTOC(DATETIME())
				tattach  = ""

				tmessage =tmessage+ "Try/CATCH Exeception Message:"+CHR(13)+;
					[  Error: ] + STR(oErr.ERRORNO) +CHR(13)+;
					[  LineNo: ] + STR(oErr.LINENO) +CHR(13)+;
					[  Message: ] + oErr.MESSAGE +CHR(13)+;
					[  Procedure: ] + oErr.PROCEDURE +CHR(13)+;
					[  Details: ] + oErr.DETAILS +CHR(13)+;
					[  StackLevel: ] + STR(oErr.STACKLEVEL) +CHR(13)+;
					[  LineContents: ] + oErr.LINECONTENTS+CHR(13)+;
					[  UserValue: ] + oErr.USERVALUE
			ENDIF
		ENDIF
		tmessage =tmessage+CHR(13)+cprogname
		tmessage =tmessage+CHR(13)+"BOL "+cbol
		tmessage =tmessage+CHR(13)+CHR(13)+cerrmsg

		tsubject = "ERROR " +cmailname+" 945 EDI Error at "+TTOC(DATETIME())
		tattach  = ""
		tfrom    ="TGF EDI Operations <toll-edi-ops@tollgroup.com>"
		DO FORM m:\dev\frm\dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
	ENDIF
FINALLY
	IF lparceltype
		IF USED('mjw_wonum')
			USE IN mjw_wonum
		ENDIF
		SELECT 0
		USE F:\3pl\DATA\mjw_wonum
		REPLACE mjw_wonum.wo_num WITH 999999 IN mjw_wonum
		USE IN mjw_wonum
	ENDIF
	closedata()
	ON ERROR
ENDTRY

*** END OF CODE BODY

****************************
PROCEDURE close945
****************************
** Footer Creation

	STORE  "GE"+cfd+ALLTRIM(STR(nstcount))+cfd+c_cntrlnum+csegd TO cstring
	FWRITE(nfilenum,cstring)

	STORE  "IEA"+cfd+"1"+cfd+PADL(c_cntrlnum,9,"0")+csegd TO cstring
	FWRITE(nfilenum,cstring)

ENDPROC

****************************
PROCEDURE num_incr_isa
****************************

	IF !USED("serfile")
		USE ("f:\3pl\data\serial\"+ccustname+"945_serial") IN 0 ALIAS serfile
	ENDIF
	SELECT serfile
	IF ltesting AND lisaflag
		norigseq = serfile.seqnum
		lisaflag = .F.
	ENDIF
	nisa_num = serfile.seqnum
	c_cntrlnum = ALLTRIM(STR(serfile.seqnum))
	REPLACE serfile.seqnum WITH serfile.seqnum + 1 IN serfile
	SELECT outship
ENDPROC

****************************
PROCEDURE num_incr_st
****************************

	IF !USED("serfile")
		USE ("f:\3pl\data\serial\"+ccustname+""945_serial") IN 0 ALIAS serfile
	ENDIF
	SELECT serfile
	IF ltesting AND lstflag
		noriggrpseq = serfile.grpseqnum
		lstflag = .F.
	ENDIF
	c_grpcntrlnum = ALLTRIM(STR(serfile.grpseqnum))
	REPLACE serfile.grpseqnum WITH serfile.grpseqnum + 1 IN serfile
	SELECT outship
ENDPROC

****************************
PROCEDURE segmentget
****************************

	PARAMETER thisarray,lckey,nlength

	FOR i = 1 TO nlength
		IF i > nlength
			EXIT
		ENDIF
		lnend= AT("*",thisarray[i])
		IF lnend > 0
			lcthiskey =TRIM(SUBSTR(thisarray[i],1,lnend-1))
			IF OCCURS(lckey,lcthiskey)>0
				RETURN SUBSTR(thisarray[i],lnend+1)
				i = 1
			ENDIF
		ENDIF
	ENDFOR

	RETURN ""

****************************
PROCEDURE ediupdate
****************************
	PARAMETER cstatus,liserror
	lupdated = .T.
	cerrmsg = cstatus

	IF !ltesting
		SELECT edi_trigger
		nrec = RECNO()
		IF !liserror
			normalexit = .T.
			REPLACE edi_trigger.processed WITH .T.,edi_trigger.proc945 WITH .T.,edi_trigger.file945 WITH cfilenamearch,;
				edi_trigger.isa_num WITH cisa_num,;
				edi_trigger.fin_status WITH "945 CREATED",edi_trigger.errorflag WITH .F.,edi_trigger.when_proc WITH DATETIME() ;
				FOR edi_trigger.bol = cbol AND edi_trigger.accountid = nacctnum AND edi_type = "945 "
		ELSE

			REPLACE edi_trigger.processed WITH .T.,edi_trigger.proc945 WITH .F.,edi_trigger.file945 WITH "",;
				edi_trigger.fin_status WITH cstatus,edi_trigger.errorflag WITH .T. ;
				FOR edi_trigger.bol = cbol AND edi_trigger.accountid = nacctnum AND edi_type = "945 "

			IF lcloseoutput
				=FCLOSE(nfilenum)
				ERASE &cfilenamehold
			ENDIF
		ENDIF
	ENDIF

	IF liserror AND lemail AND cstatus<>"SQL ERROR"
		SET STEP ON
		tsubject = "ERROR - 945 ERROR in "+cmailname+" BOL "+TRIM(cbol)+"(At PT "+cship_ref+")"
		tattach = " "
		tsendto = tsendtoerr
		tcc = IIF(cerrmsg = "EMPTY DC/STORE#",tccwhse,tccerr)
		tmessage = "945 Processing for BOL# "+cbol+", WO# "+cwo_num+" produced this error: (Office: "+coffice+"): "+cstatus+CHR(13)+"Check EDI_TRIGGER and re-run"
		IF "TOTQTY ERR"$cstatus
			tmessage = tmessage + CHR(13) + "At OUTSHIPID: "+ALLTRIM(STR(m.outshipid))
		ENDIF
		DO FORM m:\dev\frm\dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
		normalexit = .T.
	ENDIF

	closedata()

	IF !ltesting
		SELECT edi_trigger
		LOCATE
	ENDIF
	IF liserror
	ENDIF
ENDPROC

****************************
PROCEDURE cstringbreak
****************************
	clen = LEN(ALLTRIM(cstring))
	FWRITE(nfilenum,cstring)
ENDPROC

****************************
PROCEDURE closedata
****************************
	IF USED('outship')
		USE IN outship
	ENDIF
	IF USED('outdet')
		USE IN outdet
	ENDIF

	IF USED('serfile')
		SELECT serfile

		IF ltesting
			REPLACE serfile.seqnum WITH norigseq
			REPLACE serfile.grpseqnum WITH noriggrpseq
		ENDIF
		USE IN serfile
	ENDIF
	IF USED('scacs')
		USE IN scacs
	ENDIF
	IF USED('mm')
		USE IN mm
	ENDIF
	IF USED('tempx')
		USE IN tempx
	ENDIF
	IF USED('shipment')
		USE IN shipment
	ENDIF
	IF USED('package')
		USE IN package
	ENDIF
ENDPROC
