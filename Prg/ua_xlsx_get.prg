CREATE CURSOR tempmain (ibd c(50), ;
      b c(30), ;
      c c(30), ;
      d c(50), ;
      e c(30), ;
      F c(30), ;
      g c(30), ;
      h c(30), ;
      i c(30), ;
      j c(30), ;
      k c(30))

CREATE CURSOR ibdout (ibd c(20), ;
      qty c(6))


lcpath ="h:\fox\"
cfilename = "ibds.xlsx"
xfilecsv = "ibds.csv"

DO m:\dev\prg\excel_to_csv WITH cfilename
SELECT tempmain
APPEND FROM [&xfilecsv] TYPE CSV
set step On 

Select tempmain
Scan
  lcquery =[select count(1) as qty,reference from ctnucc where reference = ']+Alltrim(tempmain.ibd)+[' group by reference]
  xsqlexec(lcquery,"xctn",,"wh")
  If Reccount("xctn") >0
    Insert Into ibdout (qty,ibd) Values (Transform(xctn.qty),Alltrim(tempmain.ibd))
    
  Else
    Insert Into ibdout (qty,ibd) Values ("0",Alltrim(tempmain.ibd))

  endif
endscan

Return

xsqlexec("select count(1) as qty,reference from ctnucc where reference = '6000049800' group by reference","xctn",,"wh")
