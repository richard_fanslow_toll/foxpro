*!* m:\dev\prg\moret940_process.prg
WAIT WINDOW "Now in PROCESS phase" nowait
lcpath = "f:\0-picktickets\archive\moret-ml\"
SET DEFAULT  TO  &lcpath
*lcTransPath = ("f:\ftpusers\"+cCustname+"\940translate\")

LogCommentStr = ""
lnNum = ADIR(tarray,"*.*")

IF lnNum = 0
	WAIT WINDOW AT 10,10 "      No "+cCustname+" 940s to import     " TIMEOUT 1
	
	NormalExit = .T.
	THROW
ENDIF

ASSERT .F. MESSAGE "At 940 file loop"
FOR thisfile = 1  TO lnNum
	dDate = {^2008-01-29}
	IF tarray[thisfile,3]<dDate
	LOOP
	endif
	cfilename = LOWER(TRIM(tarray[thisfile,1]))
	Xfile = lcPath+TRIM(tarray[thisfile,1])
	IF LEFT(cfilename,4) = "W754"
		WAIT WINDOW "754 File Found...moving" TIMEOUT 2
		IF SUBSTR(cfilename,5,1) = "4"
			lc754Path = "F:\ftpusers\moret-nj\754in\"
		ELSE
			lc754Path = "F:\ftpusers\moret-ml\754in\"
		ENDIF
		lc754File = lc754Path+cfilename
		COPY FILE [&xfile] TO [&lc754file]
		DELETE FILE [&xfile]
		LOOP
	ENDIF

	WAIT WINDOW "Processing file "+Xfile NOWAIT
	ArchiveFile = lcArchivePath+TRIM(tarray[thisfile,1])
*	ASSERT .F. MESSAGE "At 997 transfer"
*	TransFile = lcTransPath+TRIM(tarray[thisfile,1])
	cCoNumber = INT(VAL(SUBSTR(cfilename,6,1)))
	cSubCo    = LOWER(SUBSTR(cfilename,6,1))
	STORE nAcct_num TO m.accountid

	USE F:\wh1\whdata\pt IN 0 ALIAS ptbase
	SELECT * FROM ptbase WHERE .F. INTO CURSOR xpt READWRITE
	USE IN ptbase
	SELECT xpt
	INDEX ON ship_ref TAG ship_ref
	SET ORDER TO

	USE F:\wh1\whdata\ptdet IN 0 ALIAS ptdetbase
	SELECT * FROM ptdetbase WHERE .F. INTO CURSOR xptdet READWRITE
	USE IN ptdetbase
	SELECT xptdet
	INDEX ON ptid TAG ptid
	SET ORDER TO

	DO CASE
		CASE cCoNumber = 1  && "JMI"
			m.accountid = 5446
		CASE cCoNumber = 2  && "SBH"
			m.accountid = 5451  && SBH Main (Scanty's determined in BKDN)
		CASE cCoNumber = 3  && "HBE"
			m.accountid = 5453
		CASE cCoNumber = 4  && HPD/HPI
			m.accountid = 5452
		OTHERWISE
			LOOP
	ENDCASE

	IF !lTestrun
*	COPY FILE [&xfile] TO [&Transfile]  && creates 997 file
	ENDIF
	WAIT "" TIMEOUT 2

	WAIT WINDOW "Importing file: "+cfilename NOWAIT
	DO m:\dev\prg\MORET940dt_bkdn
	IF lDoImport
		DO m:\dev\prg\MORET940dt_import
	ELSE
		LOOP
	ENDIF

	IF USED('xpt')
		USE IN xpt
	ENDIF
	IF USED('xptdet')
		USE IN xptdet
	ENDIF
	WAIT WINDOW "End of process for file: "+cfilename nowait

	IF USED(Xfile)
		USE IN [&xfile]
	ENDIF
*	COPY FILE [&xfile] TO [&archivefile]

	IF USED(ArchiveFile)
		USE IN [&archivefile]
	ENDIF

NEXT thisfile

WAIT CLEAR
WAIT "Entire "+cCustname+" 940 Process Now Complete" WINDOW TIMEOUT 2
