CLOSE DATA ALL
WAIT WINDOW "Now processing current Synclaire USPS records" NOWAIT NOCLEAR
gMasterOffice = "C"
gOffice = "2"
lookups()
dDate = DATE()-10

IF usesqloutwo()
	csq1 = [select * from shipment where mod = ']+gOffice+[' and right(rtrim(str(accountid)),4) in (]+&gbcnyrecaccounts+[) and del_date >= {]+DTOC(dDate)+[}]
	xsqlexec(csq1,,,"wh")
ELSE
	USE F:\wh2\whdata\outship IN 0 SHARED
ENDIF

csq1 = [select * from shipment where mod = ']+gOffice+[' and right(rtrim(str(accountid)),4) in (]+&gbcnyrecaccounts+[) and shipdate >= {]+DTOC(dDate)+[}]
xsqlexec(csq1,,,"wh")
INDEX ON shipmentid TAG shipmentid
INDEX ON pickticket TAG pickticket
IF RECCOUNT("shipment") > 0
	xjfilter="shipmentid in ("
	SCAN
		nShipmentId = shipment.shipmentid
		xsqlexec("select * from package where shipmentid="+TRANSFORM(nShipmentId),,,"wh")

		IF RECCOUNT("package")>0
			xjfilter=xjfilter+TRANSFORM(shipmentid)+","
		ENDIF
	ENDSCAN
	xjfilter=LEFT(xjfilter,LEN(xjfilter)-1)+")"

	xsqlexec("select * from package where "+xjfilter,,,"wh")
ENDIF

SELECT outship
SCAN FOR accountid = 6521 AND (INLIST(outship.scac,"PRRM","SPAR","SPRI","SPAD","SFIR") OR LEFT(ALLTRIM(outship.ship_via),4) = "USPS") AND (wo_date>DATE()-10 AND (EMPTY(del_date)) OR del_date >= dDate)
	SELECT shipment
	LOCATE FOR accountid = 6521 AND shipment.pickticket = ALLTRIM(outship.ship_ref)
	IF !FOUND()
		WAIT WINDOW "SHIPMENT PT "+outship.ship_ref+" not found!" NOWAIT
		LOOP
	ENDIF
	SELECT package
	LOCATE FOR package.shipmentid = shipment.shipmentid
	cTrk = ALLTRIM(package.trknumber)
	IF LEN(cTrk)<= 20
		LOOP
	ENDIF
	SELECT outship
	IF !"LONGTRKNUM"$outship.shipins
		REPLACE outship.shipins WITH outship.shipins+CHR(13)+"LONGTRKNUM*"+cTrk NEXT 1 IN outship
	ENDIF
ENDSCAN
CLOSE DATA ALL
WAIT CLEAR
WAIT WINDOW "Process complete" TIMEOUT 3
