*!* BCNY 945 PickPack (Whse. Shipping Advice) Creation Program
*!* Creation DatcStoreName = segmentget(@apt,"STORENAME",alength)e: 10.15.2012 by Joe, modded by Paul (from Nanjing 945PP prog)

PARAMETERS cBOL,cOffice

** try to define all variables that may be included in the "Catch Email in case of an error
** that is not the result of a deliberate Throw.......
RELEASE m.groupnum,m.transnum,m.isanum,m.edicode,m.ship_ref,m.processed,m.accountid,m.loaddt,m.loadtime,m.filename,m.fafilename
PUBLIC ARRAY thisarray(1)
PUBLIC nWO_num,cWO_Num,cWO_NumStr,cWO_NumOld,cShip_ref,cCustname,nUnitSum,cFileNameShort,cFileNameOut,cFileNameArch,lDoSQLConnect,cErrMsg,lTesting,lOverflow
PUBLIC lBCNYFilesout,lTestmail,lParcelType,lTestinput,lDoCompare,cTimeMsg,cBOLTest,cISA_Num,tccerr,cCuFt,tfrom,cMBOL,cMod,lcPath,cWO_NumList,cEDIType,lUseBLSQL
PUBLIC m.groupnum,m.transnum,m.isanum,m.edicode,m.ship_ref,m.processed,m.accountid,m.loaddt,m.loadtime,m.filename,m.fafilename  && For 997 ack, per Todd, 08.08.2014

lTesting = .f.
lTestinput = .F. && lTesting
lDoSQLConnect = .T.
lDoCompare = !lTesting
lPGTesting = .F.
tccerr=""
cErrMsg ="TOP LEVEL"
cTimeMsg = IIF(lTesting,"TIMEOUT 2","NOWAIT")
cCustname ="BCNY"
cMBOL = ""
cEDIType = "945"
lOverflow = .f.

lDoCatch = .T.
tsendtotest=""
tsendtoerr=""
tcctest=""

DO m:\dev\prg\_setvars WITH lTesting
nAcctNum = 6221
m.accountid = nAcctNum
m.edicode = "SW"
m.fafilename = ""
cMBOL = ""
IF VARTYPE(cOffice) # "C"
	cOffice = "C"
ENDIF
cMod = IIF(cOffice = "C","2",cOffice)
gOffice = cMod
gMasterOffice = cOffice

lUseSQLBL = .T.

ON ESCAPE CANCEL

TRY
	STORE 0 TO  nFilenum,nSCCSuffix,nOrigSeq,nOrigGrpSeq
	STORE "" TO cWO_NumStr,cWO_NumList,cWO_Num,cShip_ref
	STORE "XXX" TO cWO_NumOld

	lIsError = .F.
	lDoCatch = .T.
	lCloseOutput = .T.

	cPPName = "BCNY"
	lISAFlag = .T.
	lSTFlag = .T.
	lJCPenney = .F.
	lCloseOutput = .T.
	lParcelType = .F.
	tfrom = "Toll EDI Operations <toll-edi-ops@tollgroup.com>"

	IF lTesting
		CLOSE DATABASES ALL
		DO m:\dev\prg\lookups
		cBOL = "04907316221710492"
		cBOLTest = cBOL
	ENDIF
	IF EMPTY(cBOL) AND lTesting
		WAIT WINDOW "Empty BOL#" TIMEOUT 2
	ENDIF
	cBOL = ALLTRIM(cBOL)
*!* SET CUSTOMER CONSTANTS
	cCustname = "BCNY"  && Customer Identifier
	cX12 = "004010"  && X12 Standards Set used
	cMailName = "BCNY"
*nAcctNum = 6221

	SELECT 0
	CREATE CURSOR temp945 (accountid i,isa_num c(10),st_num c(10),wo_num i,bol_no c(20),;
		ship_ref c(20),filename c(50),filedate T)
	SELECT 0

	lEmail = .T.
	lTestmail = lTesting && Sends mail to Joe only
	lBCNYFilesout = .t. && If true, copies output to FTP folder (default = .t.)

*!*	lBCNYFilesout = .F.
*!*	lTestMail = .T.

	STORE "L" TO cWeightUnit
	lPrepack = .F.
	lPick = !lPrepack
	xReturn = "XXX"

	cUseFolder = 'F:\wh2\whdata\'
	IF lTestinput
		cUseFolder = "F:\WHP\WHDATA\"
	ENDIF

	DO m:\dev\prg\swc_cutctns WITH cBOL

	IF !lDoCompare
		WAIT WINDOW "Skipping Labels/Cartons Comparison..." &cTimeMsg
	ELSE
		cRetMsg = "X"
		DO m:\dev\prg\sqldata-COMPARE_bcny WITH cUseFolder,cBOL,nWO_num,cOffice,nAcctNum,cPPName
		IF cRetMsg<>"OK"
			lCloseOutput = .F.
			cErrMsg = "SQL ERROR "+cRetMsg
			DO ediupdate WITH cErrMsg,.T.
			THROW
		ENDIF
	ENDIF

	PUBLIC c_CntrlNum,c_GrpCntrlNum,cReturned
	WAIT WINDOW "Now preparing tables and variables" NOWAIT NOCLEAR

	USE F:\3pl\DATA\mailmaster ALIAS mm IN 0
	SELECT mm
	LOCATE FOR edi_type = '945' AND mm.office = cOffice AND mm.accountid = 6221
	tsendto = IIF(mm.use_alt,sendtoalt,mm.sendto)
	tcc = IIF(mm.use_alt,mm.ccalt,mm.cc)
	LOCATE
	LOCATE FOR mm.edi_type = 'MISC' AND mm.taskname = 'GENERAL'
	tsendtotest = IIF(mm.use_alt,sendtoalt,mm.sendto)
	tcctest = IIF(mm.use_alt,mm.ccalt,mm.cc)
	tsendtoerr = IIF(mm.use_alt,sendtoalt,mm.sendto)
	tccerr = IIF(mm.use_alt,mm.ccalt,mm.cc)

*!* SET OTHER CONSTANTS
	DO CASE
		CASE cOffice = "M"
			cCustLoc =  "FL"
			cFMIWarehouse = ""
			cCustPrefix = "945bw"+"f"
			cDivision = "Florida"
			cSF_Addr1  = "11400 NW 32ND AVE"
			cSF_CSZ    = "MIAMI*FL*33167"

		CASE cOffice = "N"
			cCustLoc =  "NJ"
			cFMIWarehouse = ""
			cCustPrefix = "945bw"+"j"
			cDivision = "New Jersey"
			cSF_Addr1  = "800 FEDERAL BLVD"
			cSF_CSZ    =  "CARTERET*NJ*07008"

		OTHERWISE
			cCustLoc =  "CA"
			cFMIWarehouse = ""
			cCustPrefix = "945bw"+"c"
			cDivision = "California"
			cSF_Addr1  = "450 WESTMONT DRIVE"
			cSF_CSZ    = "SAN PEDRO*CA*90731"
	ENDCASE

	IF lTesting
		IF !USED("edi_trigger")
			USE F:\3pl\DATA\EDI_TRIGGER IN 0
		ENDIF
	ENDIF

	SELECT 0
	SELECT * FROM "f:\3pl\data\ackdatashell" WHERE .F. INTO CURSOR tempack READWRITE

	IF USED('OUTSHIP')
		USE IN outship
	ENDIF

	csq1 = [select * from outship where accountid = ]+TRANSFORM(nAcctNum)+[ and office = ']+cOffice+[' and bol_no = ']+cBOL+[']
	xsqlexec(csq1,,,"wh")
	IF RECCOUNT() = 0
		cErrMsg = "MISS BOL "+cBOL
		DO ediupdate WITH cErrMsg,.T.
		THROW
	ENDIF
	INDEX ON bol_no TAG bol_no

	SELECT outship
	IF !SEEK(cBOL,'outship','bol_no')
		cErrMsg = "MISS BOL "+cBOL
		DO ediupdate WITH cErrMsg,.T.
		THROW
	ENDIF

	cCustFolder = UPPER(cCustname)  && +"-"+cCustLoc
	lNonEDI = .F.  && Assumes not a Non-EDI 945
	lDoPAL = .T.
	STORE "" TO lcKey
	STORE 0 TO alength,nLength

*!* SET OTHER CONSTANTS
	dt1 = TTOC(DATETIME(),1)
	dtmail = TTOC(DATETIME())
	dt2 = DATETIME()
	m.loaddt  = DATE()
	m.loadtime = dt2
	cString = ""

	csendqual = "ZZ"
	IF lTesting = .T.
		csendid = "TGF-US-TEST"
	ELSE
		csendid = "TGF-US-"+ICASE(cOffice = "I","NJ",cOffice = "M","FL","SP")
	ENDIF

	crecqual = "01"
	crecid = "087607334"

	cfd = "*"
	csegd = "^"
	nSegCtr = 0
	cterminator = ">" && Used at end of ISA segment
	cdate = DTOS(DATE())
	ctruncdate = RIGHT(cdate,6)
	ctime = LEFT(TIME(DATE()),2)+SUBSTR(TIME(DATE()),4,2)
	cfiledate = cdate+ctime
	cOrig = "J"
	nSTCount = 0
	cStyle = ""
	cPTString = ""
	nTotCtnWt = 0
	nTotCtnCount = 0
	nUnitSum = 0
	cTargetStyle = ""

** PAD ID Codes
	csendidlong = PADR(csendid,15," ")
	crecidlong = PADR(crecid,15," ")

	lcPath = ("f:\ftpusers\"+cCustFolder+"\945OUT\")
	cFilenameHold = ("f:\ftpusers\"+cCustFolder+"\945-Staging\"+cCustPrefix+dt1+".edi")
	cFilename = cFilenameHold
	cFileNameShort = JUSTFNAME(cFilenameHold)
	cFileNameArch = ("f:\ftpusers\"+cCustFolder+"\945OUT\archive\"+cFileNameShort)
	cFileNameOut = (lcPath+cFileNameShort)
	m.filename = cFileNameOut
	nFilenum = FCREATE(cFilenameHold)

	IF !USED("parcel_carriers")
		USE F:\3pl\DATA\parcel_carriers IN 0 ALIAS parcel_carriers
	ENDIF

	IF USED('ackdata')
		USE IN ackdata
	ENDIF

	useca("ackdata","wh")
	
	IF !USED('OUTSHIP')
		USE (cUseFolder+"OUTSHIP") IN 0 ALIAS outship
	ENDIF

	=SEEK(cBOL,'outship','bol_no')
	nWO_num = outship.wo_num
	m.scac = ALLTRIM(outship.scac)
	cShip_SCAC = IIF(m.scac="UPSZ","UPSN",ALLTRIM(outship.scac))  && Changed per Maria Estrella, 03.20.2014
	lParcelType = IIF(SEEK(cShip_SCAC,'parcel_carriers','scac'),.T.,.F.)


	lDoSQLUPS = .T.
	IF lParcelType
		IF USED('upswo')
			USE IN upswo
		ENDIF
		SELECT 0
		USE F:\3pl\DATA\upswo && Table of UPS WO#'s for back reference
		LOCATE
		LOCATE FOR upswo.accountid = nAcctNum
		IF !FOUND()
			INSERT INTO upswo (accountid,wo_num) VALUES (nAcctNum,nWO_num)
		ELSE
			lDoSQLUPS = IIF(upswo.wo_num = nWO_num,.F.,.T.)
			IF lDoSQLUPS
				REPLACE upswo.wo_num WITH nWO_num IN upswo NEXT 1
			ENDIF
		ENDIF
		USE IN upswo
	ENDIF
	USE IN parcel_carriers

	IF USED('OUTDET')
		USE IN outdet
	ENDIF

	selectoutdet()
	SELECT outdet
	INDEX ON outdetid TAG outdetid

	IF !USED('BL')
		IF lUseSQLBL
			useca("bl","wh")
		ELSE
			USE (cUseFolder+"BL") IN 0 ALIAS BL
		ENDIF
	ENDIF

*  Set Step On
	IF lDoSQLConnect
		IF lDoSQLUPS
			SELECT outship
			IF USED("sqlwobcny")
				USE IN sqlwobcny
			ENDIF
			DELETE FILE "F:\3pl\DATA\sqlwobcny.dbf"
			SELECT wo_num ;
				FROM outship WHERE bol_no = PADR(cBOL,20) ;
				GROUP BY 1 ;
				ORDER BY 1 ;
				INTO DBF F:\3pl\DATA\sqlwobcny
			USE IN sqlwobcny
			USE F:\3pl\DATA\sqlwobcny IN 0 ALIAS sqlwobcny

			SELECT sqlwobcny
			IF lTesting
				BROWSE
			ENDIF
			cRetMsg = "X"
*ASSERT .F. MESSAGE "At sqlconnect BCNY load"
			DO m:\dev\prg\sqlconnect_bol_bcny  WITH nAcctNum,cCustname,cPPName,.T.,cOffice && Amended for UCC number sequencing
			IF cRetMsg<>"OK"
				cErrMsg = cRetMsg
				DO ediupdate WITH cErrMsg,.T.
				THROW
			ENDIF

			SELECT vbcnypp
			IF lTesting
				BROWSE
			ENDIF

			IF !FILE('c:\tempfox\'+cCustname+'wo') AND lParcelType
				COPY TO ('c:\tempfox\'+cCustname+'wo')
			ENDIF
			LOCATE
		ELSE
			USE ('c:\tempfox\'+cCustname+'wo') IN 0 ALIAS wofile
			SELECT * FROM wofile INTO CURSOR vbcnypp
			USE IN wofile
		ENDIF
	ENDIF

*!* Added this code block to remove zero-qty cartons from SQL cursor
	SELECT ucc,SUM(totqty) AS uccqty FROM vbcnypp GROUP BY 1 INTO CURSOR tempchk HAVING uccqty=0
	SELECT tempchk
	LOCATE
	IF !EOF()
		SCAN
			STORE ALLT(ucc) TO cUCC
			SELECT vbcnypp
			LOCATE
			DELETE FOR ucc = cUCC
		ENDSCAN
	ENDIF
	USE IN tempchk

	SELECT vbcnypp
	SET RELATION TO outdetid INTO outdet

	IF !USED("scacs")
		xsqlexec("select * from scac","scacs",,"whall")
		INDEX ON scac TAG scac
	ENDIF
	IF !USED("parcel_carriers")
		USE F:\3pl\DATA\parcel_carriers IN 0 ALIAS parcel_carriers
	ENDIF
	SELECT outship
	LOCATE
	WAIT CLEAR


************************************************************************************************************************
	WAIT WINDOW "Now creating Header information" NOWAIT NOCLEAR

*!* HEADER LEVEL EDI DATA
	DO num_incr_isa

	cISA_Num = PADL(c_CntrlNum,9,"0")
	nISA_Num = INT(VAL(cISA_Num))
	m.isanum = cISA_Num
	m.groupnum = c_CntrlNum

	STORE "ISA"+cfd+"00"+cfd+"          "+cfd+"00"+cfd+"          "+cfd+csendqual+cfd+csendidlong+cfd+;
		crecqual+cfd+crecidlong+cfd+ctruncdate+cfd+ctime+cfd+"U"+cfd+"00401"+cfd+;
		cISA_Num+cfd+"0"+cfd+"P"+cfd+cterminator+csegd TO cString
	DO cstringbreak

	STORE "GS"+cfd+"SW"+cfd+csendid+cfd+crecid+cfd+cdate+cfd+ctime+cfd+c_CntrlNum+;
		cfd+"X"+cfd+cX12+csegd TO cString
	DO cstringbreak

*************************************************************************
*1  PTIDSOURCE/OUTSHIP RECORD MAIN LOOP
*  Loops through individual OUTSHIP lines for BOL#
*************************************************************************

	WAIT CLEAR
	WAIT WINDOW "Now creating BOL#-based information..." NOWAIT NOCLEAR

	nPTCount = 0
	lSplitPT = .F.
	cShip_refLast = "XXXX"

	SELECT outship
	SCAN FOR bol_no = cBOL

		SCATTER MEMVAR MEMO
		lAmazon = IIF("AMAZON"$outship.consignee,.T.,.F.)
		IF lAmazon
			WAIT WINDOW "This is an Amazon shipment" TIMEOUT 2
		ENDIF
		cConsignee = ALLTRIM(outship.consignee)
		lSCACConv = IIF("KOHL"$cConsignee OR "BELK."$cConsignee OR "QVC"$cConsignee OR "TOYS"$cConsignee OR "BBB"$cConsignee OR 'STAGESTORES.COM'  $cConsignee OR "BB&B"$cConsignee OR ("BED"$cConsignee AND "BATH"$cConsignee),.T.,.F.)
		m.ship_ref = ALLTRIM(m.ship_ref)
		m.processed = .F.
		nOutshipid = m.outshipid
		cShip_ref = ALLTRIM(m.ship_ref)
		lSplitPT = IIF("~"$cShip_ref,.T.,.F.)

		IF lSplitPT
			cPTbase = ALLTRIM(LEFT(ALLTRIM(cShip_ref),AT("~",cShip_ref,1)-1))
			nRec1 = RECNO()
			cRefSR = IIF("~C"$cShip_ref,cPTbase+" ~U",cPTbase+" ~C")
			LOCATE
			LOCATE FOR outship.ship_ref = cRefSR  AND outship.bol_no = cBOL
			IF !FOUND()
				cErrMsg = "SPLIT PT - DIFF BOLs"
				DO ediupdate WITH cErrMsg,.T.
				THROW
			ENDIF
			RELEASE cPTbase,cRefSR
			GO nRec1
		ENDIF

		IF (" OV"$cShip_ref) OR (RIGHT(ALLTRIM(cShip_ref),1)=".")
			LOOP
		ENDIF
		lJCPenney= IIF("PENNEY"$outship.consignee,.T.,.F.)
		lWalMart = IIF(("WALMART"$UPPER(m.consignee)) OR ("WAL-MART"$UPPER(m.consignee)),.T.,.F.)
		lSpencer = IIF("SPENCER"$outship.consignee,.T.,.F.)
		lTarget = IIF("TARGET"$outship.consignee,.T.,.F.)
		lGap = IIF("GAP"$outship.consignee,.T.,.F.)
		lKmart = IIF("KMART"$outship.consignee OR "K-MART"$outship.consignee,.T.,.F.)
		lToys= IIF("TOYS"$outship.consignee,.T.,.F.)
		nRec = RECNO()
		IF lToys
			SUM cuft TO nCuFt FOR bol_no = cBOL
			cCuFt = ALLTRIM(STR(CEILING(nCuFt)))
			RELEASE nCuFt
			GO nRec
		ENDIF
		cShip_refUse = IIF(lSplitPT,ALLTRIM(LEFT(cShip_ref,AT("~",ALLTRIM(cShip_ref))-1)),cShip_ref)
		lNewPT = IIF(cShip_refLast = cShip_refUse,.F.,.T.)
		STORE cShip_refUse TO cShip_refLast

		IF !lSplitPT OR (lSplitPT AND lNewPT) && We only want to process the PT header once for each split PT root-number.
			nCtnNumber = 1  && Begin sequence count
			lDoFirstRound = .T.

			IF lWalMart
				WAIT WINDOW "This is a Wal-Mart order" &cTimeMsg
			ENDIF
			ddel_date = outship.del_date
			IF EMPTY(ddel_date) AND notonwip AND !lTesting
				LOOP
			ENDIF
			nOSQty = m.qty
			IF nOSQty = 0
				LOOP
			ENDIF

			alength = ALINES(apt,outship.shipins,.T.,CHR(13))
			nTotCtnCount = m.qty
			cPO_Num = ALLTRIM(m.cnee_ref)
			nPTCount = nPTCount + 1

			nWO_num = outship.wo_num
			cWO_Num = ALLTRIM(STR(nWO_num))
			cPadWO_Num = PADR(cWO_Num,10)
			IF TRIM(cWO_Num) <> TRIM(cWO_NumOld)
				ASSERT .F. MESSAGE "At WO_NUMSTR creation...DEBUG"
				STORE cWO_Num TO cWO_NumOld
				IF EMPTY(cWO_NumStr)
					STORE cWO_Num TO cWO_NumStr,cWO_NumList
				ELSE
					IF !(cWO_Num$cWO_NumStr)
					cWO_NumStr = (cWO_NumStr+", "+cWO_Num)
					endif
					IF !(cWO_Num$cWO_NumList)
					cWO_NumList = (cWO_NumList+CHR(13)+cWO_Num)
					endif
				ENDIF
			ENDIF

			cPTString = IIF(EMPTY(cPTString),m.consignee+" "+cShip_refUse,cPTString+CHR(13)+m.consignee+" "+cShip_refUse)

			DO num_incr_st
			WAIT CLEAR
			WAIT WINDOW "Now creating Line Item information" NOWAIT NOCLEAR

			INSERT INTO temp945 (accountid,isa_num,st_num,wo_num,bol_no,ship_ref,filename,filedate) ;
				VALUES (m.accountid,c_CntrlNum,c_GrpCntrlNum,m.wo_num,m.bol_no,cShip_refUse,cFileNameShort,dt2)

			m.transnum = PADL(c_GrpCntrlNum,9,"0")
			STORE "ST"+cfd+"945"+cfd+m.transnum+csegd TO cString
			
			insertinto("ackdata","wh",.t.)
			tu("ackdata")

			IF lTesting
				nTotCtnWt = 0
				nTotCtnCount = 0
				nUnitSum = 0
			ENDIF
			DO cstringbreak
			nSTCount = nSTCount + 1
			nSegCtr = 1

			cShipmentID = ""
			cShipmentID = segmentget(@apt,"LONGTRKNUM",alength)
			IF EMPTY(cShipmentID)
				cShipmentID = TRIM(cBOL)
			ENDIF
			cRELEASENUM = segmentget(@apt,"RELEASENUM",alength)
			cW0504 = segmentget(@apt,"W0504",alength)
			STORE "W06"+cfd+"N"+cfd+cShip_refUse+cfd+cdate+cfd+TRIM(cShipmentID)+cfd+TRIM(cWO_Num)+cfd+cPO_Num+cfd+IIF(!EMPTY(cRELEASENUM),cRELEASENUM,"")+IIF(!EMPTY(cW0504),cfd+cW0504+csegd,csegd) TO cString
			DO cstringbreak
			nSegCtr = nSegCtr + 1

			cDUNSQUAL = segmentget(@apt,"DUNSQUAL",alength)
			cDUNSQUAL = IIF(EMPTY(cDUNSQUAL),"9",cDUNSQUAL)
			cDUNSNumber = segmentget(@apt,"DUNSNUMBER",alength)
			cDUNSNumber = IIF(EMPTY(cDUNSNumber),ALLTRIM(outship.duns),cDUNSNumber)
			IF EMPTY(ALLTRIM(outship.duns))
				STORE "N1"+cfd+"SF"+cfd+"BCNY"+csegd TO cString
			ELSE
				STORE "N1"+cfd+"SF"+cfd+"BCNY"+cfd+cDUNSQUAL+cfd+cDUNSNumber+csegd TO cString
			ENDIF
			DO cstringbreak
			nSegCtr = nSegCtr + 1

			STORE "N3"+cfd+"400-450 WESTMONT DRIVE"+csegd TO cString
			DO cstringbreak
			nSegCtr = nSegCtr + 1

			STORE "N4"+cfd+"SAN PEDRO"+cfd+"CA"+cfd+"90731"+csegd TO cString
			DO cstringbreak
			nSegCtr = nSegCtr + 1

			STORE "" TO cBT,cBTDC

			cBTName   = segmentget(@apt,"BILLTO",alength)
			cBTDC = segmentget(@apt,"BILLDC",alength)
			cBYName = segmentget(@apt,"BYNAME",alength)
			cBYDC = segmentget(@apt,"N1-BY",alength)
			IF EMPTY(cBYDC)
				cBYDC = cBTDC
			ENDIF
			IF EMPTY(cBYName)
				cBYName = cBTName
			ENDIF

			IF !EMPTY(cBYName) AND !EMPTY(cBYDC)
				STORE "N1"+cfd+"BY"+cfd+ALLTRIM(cBYName)+cfd+"92"+cfd+ALLTRIM(cBYDC)+csegd TO cString
				DO cstringbreak
				nSegCtr = nSegCtr + 1
			ENDIF

			IF !EMPTY(cBTName) AND !EMPTY(cBTDC)
				STORE "N1"+cfd+"BT"+cfd+ALLTRIM(cBTName)+cfd+"92"+cfd+ALLTRIM(cBTDC)+csegd TO cString
				DO cstringbreak
				nSegCtr = nSegCtr + 1
			ENDIF

			cDCNum = ""
			cDCNum = ALLT(segmentget(@apt,"STORENUM",alength))
			cDCNum = IIF((cDCNum # m.dcnum) OR (EMPTY(cDCNum)),ALLTRIM(m.dcnum),cDCNum)

			m.CSZ = TRIM(outship.CSZ)
			IF EMPTY(ALLT(STRTRAN(M.CSZ,",","")))
				WAIT CLEAR
				WAIT WINDOW "No SHIP-TO City/State/ZIP info...exiting" TIMEOUT 2
				cErrMsg = "NO CSZ INFO"
				DO ediupdate WITH cErrMsg,.T.
				THROW
			ENDIF
			IF !(", "$m.CSZ)
				m.CSZ = ALLTRIM(STRTRAN(m.CSZ,",",", "))
			ENDIF
			m.CSZ = ALLTRIM(STRTRAN(m.CSZ,"  "," "))
			cCity = ALLT(LEFT(TRIM(m.CSZ),AT(",",m.CSZ)-1))
			cStateZip = ALLT(SUBSTR(TRIM(m.CSZ),AT(",",m.CSZ)+1))

			cState = ALLT(LEFT(cStateZip,2))
			cOrigState = segmentget(@apt,"ORIGSTATE",alength)
			cState = IIF(EMPTY(cOrigState),cState,cOrigState)
			cZip = ALLT(SUBSTR(cStateZip,3))

			cSHIPTO = segmentget(@apt,"SHIPTO",alength)
			cSHIPTO = IIF(EMPTY(cSHIPTO),ALLTRIM(outship.consignee),cSHIPTO)
			STORE "N1"+cfd+"ST"+cfd+cSHIPTO+IIF(!EMPTY(cDCNum),+cfd+"92"+cfd+cDCNum,"")+csegd TO cString
			DO cstringbreak
			nSegCtr = nSegCtr + 1

			STORE "N3"+cfd+TRIM(outship.address)+IIF(!EMPTY(outship.address2),cfd+ALLT(outship.address2),"")+csegd TO cString
			DO cstringbreak
			nSegCtr = nSegCtr + 1

			STORE "N4"+cfd+cCity+cfd+cState+cfd+cZip+csegd TO cString
			DO cstringbreak
			nSegCtr = nSegCtr + 1

			IF !EMPTY(m.shipfor)
				STORE "" TO cSForCity,cSForState,cSForZip
				m.SForCSZ = ALLTRIM(STRTRAN(m.SForCSZ,"  "," "))
				nSpaces = OCCURS(" ",ALLTRIM(m.SForCSZ))
				IF nSpaces = 0
					m.SForCSZ = STRTRAN(m.SForCSZ,",","")
					cSForCity = ALLTRIM(m.SForCSZ)
					cSForState = ""
					cSForZip = ""
				ELSE
					nCommaPos = AT(",",m.SForCSZ)
					nLastSpace = AT(" ",m.SForCSZ,nSpaces)
					nMinusSpaces = IIF(nSpaces=1,0,1)
					IF ISALPHA(SUBSTR(TRIM(m.SForCSZ),AT(" ",TRIM(m.SForCSZ),nSpaces-nMinusSpaces)+1,2))
						cSForCity = LEFT(TRIM(m.SForCSZ),AT(",",m.SForCSZ)-1)
						cSForState = SUBSTR(m.SForCSZ,nCommaPos+2,2)
						cSForZip = TRIM(RIGHT(TRIM(m.SForCSZ),5))
						IF ISALPHA(cSForZip)
							cSForZip = ""
						ENDIF
					ELSE
						WAIT CLEAR
						WAIT WINDOW "NOT ALPHA: "+SUBSTR(TRIM(m.SForCSZ),AT(" ",m.SForCSZ,nSpaces-1)+1,2) &cTimeMsg
						cSForCity = LEFT(TRIM(m.SForCSZ),AT(",",m.SForCSZ)-1)
						cSForState = SUBSTR(TRIM(m.SForCSZ),AT(" ",m.SForCSZ,nSpaces-2)+1,2)
						cSForZip = TRIM(RIGHT(TRIM(m.SForCSZ),5))
						IF ISALPHA(cSForZip)
							cSForZip = ""
						ENDIF
					ENDIF
				ENDIF

				IF EMPTY(ALLTRIM(m.sforstore))
					STORE "N1"+cfd+"Z7"+cfd+ALLTRIM(m.shipfor)+csegd TO cString
				ELSE
					STORE "N1"+cfd+"Z7"+cfd+ALLTRIM(m.shipfor)+cfd+"92"+cfd+ALLTRIM(m.sforstore)+csegd TO cString
				ENDIF
				DO cstringbreak
				nSegCtr = nSegCtr + 1
				STORE "N3"+cfd+ALLTRIM(m.sforaddr1)+csegd TO cString
				DO cstringbreak
				nSegCtr = nSegCtr + 1
				STORE "N4"+cfd+ALLTRIM(cSForCity)+cfd+ALLTRIM(cSForState)+cfd+ALLTRIM(cSForZip)+csegd TO cString
				DO cstringbreak
				nSegCtr = nSegCtr + 1
			ENDIF

** Ok, now in the header N9 segments

			lParcelType = IIF(SEEK(cShip_SCAC,'parcel_carriers','scac'),.T.,.F.)
			lFedex = IIF(SEEK(cShip_SCAC,'parcel_carriers','scac') AND parcel_carriers.fedex,.T.,.F.)
			lUPS = IIF(SEEK(cShip_SCAC,'parcel_carriers','scac') AND parcel_carriers.ups,.T.,.F.)
			IF lUPS AND lTarget
				cShip_SCAC = "UPSN"
			ENDIF
			IF lFedex
				cShip_SCAC = "FDEG"
			ENDIF

			IF lParcelType AND "PENNEY"$UPPER(m.consignee)
				STORE "N9"+cfd+"BM"+cfd+"0"+csegd TO cString
				DO cstringbreak
				nSegCtr = nSegCtr + 1
			ELSE
				STORE "N9"+cfd+"BM"+cfd+TRIM(cBOL)+csegd TO cString
				DO cstringbreak
				nSegCtr = nSegCtr + 1
			ENDIF

			IF lToys AND lParcelType
				STORE "N9"+cfd+"CN"+cfd+TRIM(cBOL)+csegd TO cString
				DO cstringbreak
				nSegCtr = nSegCtr + 1
			ENDIF

			cDept = ALLT(outship.dept)
			IF EMPTY(cDept)
				cDept = segmentget(@apt,"DEPT",alength)
			ENDIF
			IF !EMPTY(cDept)
				STORE "N9"+cfd+"DP"+cfd+TRIM(outship.dept)+csegd TO cString
				DO cstringbreak
				nSegCtr = nSegCtr + 1
			ENDIF

			cVendnum = ""
			cVendnum = segmentget(@apt,"VENDORNUM",alength) && Added for 10+ character vendor numbers
			IF EMPTY(cVendnum)
				cVendnum = TRIM(outship.vendor_num)
			ENDIF
			IF !EMPTY(cVendnum)
				STORE "N9"+cfd+"IA"+cfd+cVendnum+csegd TO cString
				DO cstringbreak
				nSegCtr = nSegCtr + 1
			ENDIF

			cSNNum = segmentget(@apt,"SNNUM",alength)
			IF !EMPTY(cSNNum)
				STORE "N9"+cfd+"SN"+cfd+TRIM(cSNNum)+csegd TO cString
				DO cstringbreak
				nSegCtr = nSegCtr + 1
			ENDIF

			cPKNum = segmentget(@apt,"PKNUM",alength)
			IF !EMPTY(cPKNum)
				STORE "N9"+cfd+"PK"+cfd+TRIM(cPKNum)+csegd TO cString
				DO cstringbreak
				nSegCtr = nSegCtr + 1
			ENDIF

			cVNInfo = segmentget(@apt,"VNINFO",alength)
			IF !EMPTY(cVNInfo)
				STORE "N9"+cfd+"VN"+cfd+TRIM(cVNInfo)+csegd TO cString
				DO cstringbreak
				nSegCtr = nSegCtr + 1
			ENDIF

			cVINum = segmentget(@apt,"VINUM",alength)
			IF !EMPTY(cVINum)
				STORE "N9"+cfd+"VI"+cfd+TRIM(cVINum)+csegd TO cString
				DO cstringbreak
				nSegCtr = nSegCtr + 1
			ENDIF

			cTPInfo = segmentget(@apt,"TPINFO",alength)
			IF !EMPTY(cTPInfo)
				STORE "N9"+cfd+"TP"+cfd+TRIM(cTPInfo)+csegd TO cString
				DO cstringbreak
				nSegCtr = nSegCtr + 1
			ENDIF

			cSCAInfo = segmentget(@apt,"SCAINFO",alength)
			IF !EMPTY(cSCAInfo)
				STORE "N9"+cfd+"SCA"+cfd+TRIM(cSCAInfo)+csegd TO cString
				DO cstringbreak
				nSegCtr = nSegCtr + 1
			ENDIF

*!*				cDTMInfo = segmentget(@apt,"DTMINFO",alength)
*!*				IF !EMPTY(cDTMInfo)
*!*					STORE "N9"+cfd+"DTM"+cfd+cDTMInfo+csegd TO cString
*!*					DO cstringbreak
*!*					nSegCtr = nSegCtr + 1
*!*				ENDIF

			cSTInfo = segmentget(@apt,"STINFO",alength)
			IF !EMPTY(cSTInfo)
				STORE "N9"+cfd+"ST"+cfd+TRIM(cSTInfo)+csegd TO cString
				DO cstringbreak
				nSegCtr = nSegCtr + 1
			ENDIF

			cWHInfo = segmentget(@apt,"WHINFO",alength)
			IF !EMPTY(cWHInfo)
				STORE "N9"+cfd+"WH"+cfd+TRIM(cWHInfo)+csegd TO cString
				DO cstringbreak
				nSegCtr = nSegCtr + 1
			ENDIF

			cATInfo = segmentget(@apt,"ATINFO",alength)
			IF !EMPTY(cATInfo)
				STORE "N9"+cfd+"AT"+cfd+TRIM(cATInfo)+csegd TO cString
				DO cstringbreak
				nSegCtr = nSegCtr + 1
			ENDIF

			cEDIInfo = segmentget(@apt,"EDI-INFO",alength)
			IF !EMPTY(cEDIInfo)
				STORE "N9"+cfd+"TI"+cfd+TRIM(cEDIInfo)+csegd TO cString
				DO cstringbreak
				nSegCtr = nSegCtr + 1
			ENDIF

			IF !lParcelType
				IF lTesting
					cLoadID = ALLT("DIL"+ALLT(STR(INT(RAND()*(10^7)))))
				ELSE
					cLoadID = ALLTRIM(outship.appt_num)
					IF EMPTY(cLoadID)
						cErrMsg = "MISS LOADID"
						DO ediupdate WITH cErrMsg,.T.
						THROW
					ENDIF
				ENDIF
				STORE "N9"+cfd+"LO"+cfd+TRIM(cLoadID)+csegd TO cString
				DO cstringbreak
				nSegCtr = nSegCtr + 1
			ELSE
				IF lToys
					cLoadID = "NA"
					STORE "N9"+cfd+"LO"+cfd+TRIM(cLoadID)+csegd TO cString
					DO cstringbreak
					nSegCtr = nSegCtr + 1
				ENDIF
			ENDIF

			SELECT outship

*****************************************************8
			cBOL = ALLTRIM(outship.bol_no)
			cShipType      =""
*    cShip_SCAC     =""
			cTrailerNumber =""
			cProNumber     =""
			cTerms         =""

			IF lParcelType
				cTrackNum = ALLTRIM(outship.bol_no)  && UPS Tracking Number
			ELSE
				IF lTesting
					cShipSCAC = outship.scac
					cTrailerNumber = ""
					cProNumber = "123456"
					cTerms = "PP"
				ELSE

					IF lUseSQLBL
						csq1 = [select * from bl where bol_no = ']+cBOL+[']
						xsqlexec(csq1,"bl1",,"wh")
						SELECT bl1
						LOCATE
						IF !EOF()
							cShip_SCAC    = IIF(!EMPTY(BL.scac),BL.scac,cShip_SCAC)
							cTrailerNumber= LEFT(ALLTRIM(BL.trailer),10)
							cProNumber    = BL.pronumber
							DO CASE
								CASE BL.terms ="1"
									cTerms = "PP"
								CASE BL.terms ="2"
									cTerms = "CC"
								OTHERWISE
									cTerms = ""
							ENDCASE
						ELSE
							cErrMsg = "MISSING BL Data"
							DO ediupdate WITH cErrMsg,.T.
							THROW
						ENDIF
					ELSE
						SELECT BL
						LOCATE FOR bol_no = cBOL
						IF FOUND()
							cShip_SCAC    = IIF(!EMPTY(BL.scac),BL.scac,cShip_SCAC)
							cTrailerNumber= LEFT(ALLTRIM(BL.trailer),10)
							cProNumber    = BL.pronumber
							DO CASE
								CASE BL.terms ="1"
									cTerms = "PP"
								CASE BL.terms ="2"
									cTerms = "CC"
								OTHERWISE
									cTerms = ""
							ENDCASE
						ELSE
							cErrMsg = "MISSING BL Data"
							DO ediupdate WITH cErrMsg,.T.
							THROW
						ENDIF
					ENDIF
				ENDIF
			ENDIF

			IF EMPTYnul(outship.del_date)
				IF lJCPenney
					cErrMsg = "MISSING DELDATE"
					DO ediupdate WITH cErrMsg,.T.
					THROW
				ELSE
					ddel_date = outship.appt
				ENDIF
			ELSE
				ddel_date = outship.del_date
				IF EMPTY(ddel_date)
					IF lTesting
						ddel_date = DATE()
					ELSE
						SELECT EDI_TRIGGER
						LOCATE
						LOCATE FOR accountid = nAcctNum AND ship_ref = cShip_ref AND bol = cBOL AND edi_type = "945"
						IF FOUND()
* if xsqlexec("select * from edi_trigger where accountid="+trans(nAcctNum)+" " + ;
	"and ship_ref='"+cShip_ref+"' and bol='"+cBOL+"' and edi_type='945'",,,"stuff") # 0
							REPLACE fin_status WITH "MISSING DELDATE",processed WITH .T.,errorflag WITH .T.
							tsubject = "945 Error in BCNY/EA BOL "+TRIM(cBOL)+" (At PT "+cShip_ref+")"
							tattach = " "
							tsendto = tsendtoerr
							tcc = tccerr
							tmessage = "945 Processing for WO# "+TRIM(cWO_Num)+" produced this error: (Office: "+cOffice+"): "+cStatus+CHR(13)+"Please notify Joe of actual status for reprocessing ASAP."
							DO FORM m:\dev\frm\dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
						ENDIF
						LOOP
					ENDIF
				ENDIF

				dapptdate = outship.appt
				IF EMPTY(dapptdate)
					dapptdate = outship.del_date
				ENDIF

			ENDIF

			IF EMPTYnul(outship.del_date) OR EMPTYnul(outship.START) OR EMPTYnul(outship.CANCEL)
				cErrMsg = "EMPTY "+ICASE(EMPTYnul(outship.del_date),"DELDATE",EMPTYnul(outship.START),"START","CANCEL")
				DO ediupdate WITH cErrMsg,.T.
				THROW
			ENDIF

			cUseTime = "153000"
			STORE "G62"+cfd+"11"+cfd+DTOS(del_date)+cfd+"9"+cfd+cUseTime+csegd TO cString  && Ship date
			DO cstringbreak
			nSegCtr = nSegCtr + 1

*		IF ("MACY"$UPPER(m.consignee) OR "MMG"$UPPER(m.consignee) OR "KMART"$UPPER(m.consignee) OR "K-MART"$UPPER(m.consignee))
			cUseTime = "153000"
			STORE "G62"+cfd+"67"+cfd+DTOS(del_date)+cfd+"9"+cfd+cUseTime+csegd TO cString  && Sched ship date
			DO cstringbreak
			nSegCtr = nSegCtr + 1
*		ENDIF

*!*			STORE "G62"+cfd+"01"+cfd+DTOS(outship.CANCEL)+csegd TO cString  && Ship date
*!*			DO cstringbreak
*!*			nSegCtr = nSegCtr + 1

			STORE "G62"+cfd+"10"+cfd+DTOS(outship.START)+csegd TO cString  && Ship date
			DO cstringbreak
			nSegCtr = nSegCtr + 1

			alength = ALINES(apt,outship.shipins,.T.,CHR(13))
			cPODate = segmentget(@apt,"PODATE",alength)
			IF !EMPTY(cPODate)
				STORE "G62"+cfd+"04"+cfd+cPODate+csegd TO cString  && Ship date
				DO cstringbreak
				nSegCtr = nSegCtr + 1
			ENDIF

			cDNLDate = segmentget(@apt,"DNLDATE",alength)
			IF !EMPTY(cDNLDate)
				STORE "G62"+cfd+"54"+cfd++DTOS(del_date)+csegd TO cString  && Ship date
				DO cstringbreak
				nSegCtr = nSegCtr + 1
			ENDIF

			SELECT outship
			cShipType=IIF(lParcelType,"U","M")
			cTerms = IIF(lParcelType,"CC",cTerms)
			cSCAInfo = ALLTRIM(segmentget(@apt,"SCAINFO",alength))  && Added this section per Todd, 08.18.2014
**IF lSCACConv AND LEFT(cSCAINFO,3) = "UNS"
			IF lSCACConv AND SUBSTR(cSCAInfo,1,3) = "UNS"
*				SET STEP ON
				USE F:\wh2\whdata\bcny_scac_conv IN 0
				SELECT bcny_scac_conv
				LOCATE FOR bcny_scac_conv.scac_940 = cSCAInfo AND ALLTRIM(bcny_scac_conv.scac) = ALLTRIM(outship.scac)
				IF FOUND()
					cShip_SCAC = ALLTRIM(bcny_scac_conv.scac_945)
				ENDIF
			ENDIF

			DO CASE
				CASE EMPTY(cTerms) AND EMPTY(cTrailerNumber)
					STORE "W27"+cfd+cShipType+cfd+ALLTRIM(cShip_SCAC)+cfd+ALLTRIM(outship.ship_via)+csegd TO cString
				CASE !EMPTY(cTerms) AND EMPTY(cTrailerNumber)
					STORE "W27"+cfd+cShipType+cfd+ALLTRIM(cShip_SCAC)+cfd+ALLTRIM(outship.ship_via)+cfd+ALLTRIM(cTerms)+csegd TO cString
				OTHERWISE
					STORE "W27"+cfd+cShipType+cfd+ALLTRIM(cShip_SCAC)+cfd+ALLTRIM(outship.ship_via)+cfd+ALLTRIM(cTerms)+cfd+cfd+cfd+ALLTRIM(cTrailerNumber)+csegd TO cString
			ENDCASE
			DO cstringbreak
			nSegCtr = nSegCtr + 1
		ENDIF

*************************************************************************
*2  DETAIL LOOP - MATCHING SHIP_REF FIELD (FROM OUTSHIP)
*************************************************************************

		WAIT CLEAR
		WAIT WINDOW "Now creating Detail information "+cShip_ref NOWAIT NOCLEAR
		SELECT vbcnypp
		LOCATE
		LOCATE FOR ship_ref = TRIM(cShip_ref) AND outshipid = nOutshipid
		IF !FOUND()
			IF !lTesting
				WAIT WINDOW "PT "+cShip_ref+" NOT FOUND in vbcnypp...ABORTING" &cTimeMsg
				IF !lTesting
					cErrMsg = "MISS PT: "+cShip_ref
					DO ediupdate WITH cErrMsg,.T.
				ENDIF
				=FCLOSE(nFilenum)
				ERASE &cFilename
				THROW
			ELSE
				LOOP
			ENDIF
		ENDIF

*!* Added this code to trap unmatched OUTDETIDs in OUTDET, Joe 11.22.2005
		SCAN FOR vbcnypp.ship_ref = TRIM(cShip_ref) AND vbcnypp.outshipid = nOutshipid AND vbcnypp.totqty#0
			IF EMPTY(outdet.outdetid)
				SET STEP ON
				WAIT WINDOW "OUTDETID "+TRANSFORM(vbcnypp.outdetid)+" NOT FOUND IN OUTDET!...CLOSING" &cTimeMsg
				cErrMsg = "MISS ODID/WO: "+TRANSFORM(vbcnypp.outdetid)+"/"+TRANSFORM(vbcnypp.wo_num)
				DO ediupdate WITH cErrMsg,.T.
				=FCLOSE(nFilenum)
				ERASE &cFilename
				THROW
			ENDIF
		ENDSCAN

		SCANSTR = "vbcnypp.ship_ref = cShip_ref and vbcnypp.outshipid = nOutshipid and !DELETED()"
		SELECT vbcnypp
		LOCATE FOR &SCANSTR
		cCartonNum= "XXX"
		cUCC="XXX"

		DO WHILE &SCANSTR
			lSkipBack = .T.

			IF vbcnypp.totqty = 0
				SKIP 1 IN vbcnypp
				LOOP
			ENDIF

			IF vbcnypp.ucc = 'CUTS'
				SKIP 1 IN vbcnypp
				LOOP
			ENDIF

			IF TRIM(vbcnypp.ucc) <> cCartonNum
				STORE TRIM(vbcnypp.ucc) TO cCartonNum
			ENDIF

			STORE "LX"+cfd+ALLTRIM(STR(nCtnNumber))+csegd TO cString   && Carton Seq. # (up to Carton total)
			lDoManSegment = .T.
			DO cstringbreak
			nSegCtr = nSegCtr + 1

			cDoString = "vbcnypp.ucc = cCartonNum and vbcnypp.totqty > 0"
			DO WHILE &cDoString

				IF lDoManSegment
					cUCC = TRIM(vbcnypp.ucc)
					IF !EMPTY(cUCC) AND cUCC # "XXX"
						lDoManSegment = .F.
						cPRONum = IIF(lTesting,"999888777",ALLTRIM(outship.keyrec))
						IF lToys AND !EMPTY(cPRONum)
							STORE "MAN"+cfd+"GM"+cfd+TRIM(cUCC)+cfd+cfd+"ZZ"+cfd+cPRONum+csegd TO cString  && UCC Number (Wal-mart DSDC only)
						ELSE
							STORE "MAN"+cfd+"GM"+cfd+TRIM(cUCC)+csegd TO cString  && UCC Number (Wal-mart DSDC only)
						ENDIF
						DO cstringbreak
						nSegCtr = nSegCtr + 1
** now write out the saved inner and pack info that was uploaded from the 940, if we have the data........
						alength = ALINES(apt,outdet.printstuff,.T.,CHR(13))
						cPalPack  = ALLTRIM(segmentget(@apt,"W20_1",alength))
						cPalInner = ALLTRIM(segmentget(@apt,"W20_2",alength))
						cOrigWt = ALLTRIM(segmentget(@apt,"ORIG_WT",alength))
						cUOW = IIF(EMPTY(cOrigWt),"",ALLTRIM(segmentget(@apt,"ORIG_UOW",alength)))
						cOrigVol = ALLTRIM(segmentget(@apt,"ORIG_VOL",alength))
						cUOV = IIF(EMPTY(cOrigVol),"",ALLTRIM(segmentget(@apt,"ORIG_UOV",alength)))
*						STORE "PAL"+cfd+cfd+cfd+cfd+ALLTRIM(cPalPack)+cfd+cfd+cfd+cfd+cfd+cfd+cfd+cOrigWt+cfd+cUOW+cfd+cOrigVol+cfd+cUOV+cfd+cfd+ALLTRIM(cPalInner)+csegd TO cString
*						DO cstringbreak
*						nSegCtr = nSegCtr + 1
					ELSE
						WAIT CLEAR
						WAIT WINDOW "Empty UCC Number in vbcnypp "+cShip_ref &cTimeMsg
						IF lTesting
							ASSERT .F. MESSAGE "EMPTY UCC...DEBUG"
							CLOSE DATABASES ALL
							=FCLOSE(nFilenum)
							ERASE &cFilename
							THROW
						ELSE
							cErrMsg = "EMPTY UCC# in "+cShip_ref
							DO ediupdate WITH cErrMsg,.T.
							=FCLOSE(nFilenum)
							ERASE &cFilename
							THROW
						ENDIF
					ENDIF
				ENDIF

				alength = ALINES(aptdet,outdet.printstuff,.T.,CHR(13))
				cUPCCode = segmentget(@aptdet,"UPCCODE",alength)
				cColor = TRIM(outdet.COLOR)
				STORE ALLTRIM(outdet.ID) TO cSize
				cStyle = TRIM(outdet.STYLE)
				cUPC = IIF(lTesting,TRIM(vbcnypp.upc),TRIM(outdet.upc))
				cGTIN = ""
				IF LEN(ALLTRIM(cUPC)) # 12
					IF cUPCCode = "UK"
						cGTIN = TRIM(outdet.upc)
						cUPC = ""
					ENDIF
				ENDIF
				cStyle = ALLTRIM(outdet.STYLE)

				cUOM = segmentget(@aptdet,"UOM",alength)
*					cUOM = segmentget(@aptdet,"ORIG_UOM",alength)
				IF EMPTY(cUOM)
					cUOM = segmentget(@aptdet,"UNITCODE",alength)
				ENDIF
				lPrepack = IIF(cUOM = "EA",.F.,.T.)
				IF LEN(ALLTRIM(cUPC)) # 12
					IF cUOM = "UK"  && if cUPCCode =
						cGTIN = TRIM(outdet.upc)
						cUPC = ""
					ENDIF
				ENDIF
				cStyle = ALLTRIM(outdet.STYLE)
				cItemNum = segmentget(@aptdet,"CUSTSKU",alength)
				IF EMPTY(ALLT(cItemNum))
					cItemNum = ALLTRIM(outdet.custsku)
				ENDIF
				nDetQty = vbcnypp.totqty
				nOrigQty = vbcnypp.qty

				IF EMPTY(cItemNum) AND !lSplitPT
					SET STEP ON
					cErrMsg = "MISS ItNum, Style "+cStyle
					DO ediupdate WITH cErrMsg,.T.
					=FCLOSE(nFilenum)
					ERASE &cFilename
					THROW
				ENDIF

				IF lTesting AND EMPTY(outdet.ctnwt)
					cCtnWt = IIF(lPrepack,"5","2")
					nTotCtnWt = nTotCtnWt + INT(VAL(cCtnWt))
				ELSE
					STORE ALLTRIM(STR(outdet.ctnwt)) TO cCtnWt
					nTotCtnWt = nTotCtnWt + outdet.ctnwt

					IF EMPTY(cCtnWt) OR outdet.ctnwt=0
						nCtnWt = outship.weight/outship.ctnqty
						cCtnWt = ALLTRIM(STR(nCtnWt))
						nTotCtnWt = nTotCtnWt + nCtnWt
						IF EMPTY(cCtnWt)
							cErrMsg = "MISS CTNWT "+TRANSFORM(vbcnypp.outdetid)
							DO ediupdate WITH cErrMsg,.T.
							=FCLOSE(nFilenum)
							ERASE &cFilename
							THROW
						ENDIF
					ENDIF
				ENDIF

				IF nOrigQty=nDetQty
					nShipType = "CL"
				ELSE
					nShipType = "PR"
				ENDIF

				IF nDetQty>0
					nUnitSum = nUnitSum + nDetQty
					cUPC = IIF(lGap,"",cUPC)
					nOriqqty = INT(VAL(segmentget(@aptdet,"ORIGQTY",alength)))
					IF lPrepack AND "SUBUPC"$aptdet
						STORE "W12"+cfd+nShipType+cfd+ALLTRIM(STR(nOrigQty))+cfd+ALLTRIM(STR(nDetQty))+cfd+;
							ALLTRIM(STR(nOrigQty-nDetQty))+cfd+cUOM+cfd+cUPC+cfd+cUOM+cfd+cUPC+REPLICATE(cfd,2)+cCtnWt+cfd+"G"+cfd+cWeightUnit+csegd TO cString  && Eaches/Style
						DO cstringbreak
						nSegCtr = nSegCtr + 1
						CREATE CURSOR tempdetail (STYLE c(20),COLOR c(20),SIZE c(15),subqty i)
						lenarydet = ALEN(aptdet,1)
						FOR irq = 1 TO lenarydet
							arystr = SUBSTR(UPPER(ALLTRIM(aptdet[irq])),2)
							DO CASE
								CASE EMPTY(arystr)
									LOOP
								CASE LEFT(arystr,6)= "SUBUPC"
									cSubUPC = SUBSTR(ALLTRIM(arystr),AT("*",arystr)+1)
								CASE LEFT(arystr,5) = "STYLE"
									cStyle = SUBSTR(ALLTRIM(arystr),AT("*",arystr)+1)
								CASE LEFT(arystr,5)= "COLOR"
									cColor = SUBSTR(ALLTRIM(arystr),AT("*",arystr)+1)
								CASE LEFT(arystr,4) = "SIZE"
									cSize = SUBSTR(ALLTRIM(arystr),AT("*",arystr)+1)
								CASE LEFT(arystr,6) = "SUBQTY"  && Write to cString at this point
									cSubqty = SUBSTR(ALLTRIM(arystr),AT("*",arystr)+1)
									STORE "N9"+cfd+"UP"+cfd+cSubUPC+cfd+cSubqty+IIF(!EMPTY(cStyle),REPLICATE(cfd,4)+"IN>"+cStyle+">CL>"+cColor+">SZ>"+cSize+csegd,csegd) TO cString
									DO cstringbreak
									nSegCtr = nSegCtr + 1
							ENDCASE
						ENDFOR
					ELSE
						cStyle = segmentget(@aptdet,"ORIGSTYLE",alength)
						IF cUPCCode = "UK" AND EMPTY(cUPC) AND !EMPTY(cGTIN) && GTIN data
							STORE "W12"+cfd+nShipType+cfd+ALLTRIM(STR(nOrigQty))+cfd+ALLTRIM(STR(nDetQty))+cfd+;
								ALLTRIM(STR(nOrigQty-nDetQty))+cfd+cUOM+cfd+cUPC+cfd+IIF(!EMPTY(cStyle),"VN","")+cfd+TRIM(cStyle)+;
								cfd+cfd+cCtnWt+cfd+"G"+cfd+cWeightUnit+REPLICATE(cfd,5)+IIF(!EMPTY(cItemNum),"IN","")+cfd+ALLTRIM(cItemNum)+cfd+cfd+cfd+cUPCCode+cfd+cGTIN+csegd TO cString  && Eaches/Style
						ELSE
							STORE "W12"+cfd+nShipType+cfd+ALLTRIM(STR(nOrigQty))+cfd+ALLTRIM(STR(nDetQty))+cfd+;
								ALLTRIM(STR(nOrigQty-nDetQty))+cfd+cUOM+cfd+cUPC+cfd+IIF(!EMPTY(cStyle),"VN","")+cfd+TRIM(cStyle)+;
								cfd+cfd+cCtnWt+cfd+"G"+cfd+cWeightUnit+REPLICATE(cfd,5)+IIF(!EMPTY(cItemNum),"IN","")+cfd+ALLTRIM(cItemNum)+csegd TO cString  && Eaches/Style
						ENDIF
						DO cstringbreak
						nSegCtr = nSegCtr + 1
					ENDIF

					cDesc = segmentget(@aptdet,"DESC",alength)
					IF !EMPTY(cDesc)
						STORE "G69"+cfd+cDesc+csegd TO cString
						DO cstringbreak
						nSegCtr = nSegCtr + 1
					ENDIF

					cColor = segmentget(@aptdet,"ORIGCOLOR",alength)
					IF !EMPTY(cColor)
						STORE "N9"+cfd+"CL"+cfd+cColor+csegd TO cString
						DO cstringbreak
						nSegCtr = nSegCtr + 1
					ENDIF
					cSize = segmentget(@aptdet,"ORIGSIZE",alength)
					IF !EMPTY(cSize)
						STORE "N9"+cfd+"SZ"+cfd+cSize+csegd TO cString
						DO cstringbreak
						nSegCtr = nSegCtr + 1
					ENDIF

					IF !EMPTY(cItemNum)
						STORE "N9"+cfd+"IN"+cfd+cItemNum+csegd TO cString
						DO cstringbreak
						nSegCtr = nSegCtr + 1
					ENDIF

					cBuyerNum = segmentget(@aptdet,"BUYERSIZE",alength)
					IF !EMPTY(cBuyerNum)
						STORE "N9"+cfd+"IZ"+cfd+cBuyerNum+csegd TO cString
						DO cstringbreak
						nSegCtr = nSegCtr + 1
					ENDIF
				ENDIF

				SKIP 1 IN vbcnypp
			ENDDO
			lSkipBack = .T.
			nCtnNumber = nCtnNumber + 1
		ENDDO

		IF lNewPT
			IF !lDoFirstRound
				LOOP
			ELSE
				lDoFirstRound = .F.
				IF !lSplitPT
					EndSEloop()
				ENDIF
			ENDIF
		ELSE
			EndSEloop()
		ENDIF

*************************************************************************
*2^  END DETAIL MATCHING SHIP_REF LOOP
*************************************************************************
		SELECT outship
		WAIT CLEAR
	ENDSCAN

*************************************************************************
*1^  END OUTSHIP MAIN LOOP
*************************************************************************

	DO close945
	=FCLOSE(nFilenum)

	SELECT tempack
	IF lTesting
*!*			SET STEP ON
*!*			LOCATE
*!*			BROW
	endif
	
	USE IN tempack
	USE IN ackdata

*  Set Step On

	IF !lTesting
		SELECT EDI_TRIGGER
		DO ediupdate WITH "945 CREATED",.F.

		IF !USED("ftpedilog")
			SELECT 0
			USE F:\edirouting\ftpedilog ALIAS ftpedilog
			INSERT INTO ftpedilog (TRANSFER,ftpdate,filename,acct_name,TYPE) VALUES ("945-"+cCustname+"-"+cCustLoc,dt2,cFilename,UPPER(cCustname),"945")
			USE IN ftpedilog
		ENDIF
	ENDIF

	WAIT CLEAR
	WAIT WINDOW cCustFolder+" 945 Process complete..." TIMEOUT 1

*!* Create eMail confirmation message
	IF lTestmail
		tsendto = tsendtotest
		tcc = tcctest
	ENDIF

	cPTCount = ALLTRIM(STR(nPTCount))
	nPTCount = 0
	tsubject = cMailName+" 945 EDI File from TGF as of "+dtmail+" ("+cCustLoc+")"
	tattach = " "
	tmessage = "945 EDI Info from TGF for BOL# "+TRIM(cBOL)+CHR(13)
	tmessage = tmessage + "(File: "+cFileNameShort+")"+CHR(13)
	tmessage = tmessage + "(WO# "+cWO_NumStr+"), containing these "+cPTCount+" picktickets:"+CHR(13)
	tmessage = tmessage + cPTString + CHR(13)
	tmessage = tmessage + "has been created and will be transmitted ASAP."+CHR(13)+CHR(13)
	tmessage = tmessage + "If you have any questions, please eMail or call 732-750-9000 x217."

	IF lEmail
		DO FORM m:\dev\frm\dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
	ENDIF

	cFilenameArchive = UPPER("f:\ftpusers\"+cCustFolder+"\945OUT\945ARCHIVE\"+cCustPrefix+dt1+".txt")

	RELEASE ALL LIKE c_CntrlNum,c_GrpCntrlNum
	WAIT CLEAR
	WAIT WINDOW cMailName+" 945 EDI File output complete"+CHR(13)+"for BOL# "+TRIM(cBOL) AT 20,60 &cTimeMsg

*!* Transfers files to correct output folders
	COPY FILE [&cFilenameHold] TO [&cFilenameArch]
*	SET STEP ON

**********************************************************************************************88
	IF lBCNYFilesout
		COPY FILE &cFilenameHold TO &cFileNameOut
		DELETE FILE &cFilenameHold
		SELECT temp945
		COPY TO "f:\3pl\data\temp945a.dbf"
		USE IN temp945
		SELECT 0
		USE "f:\3pl\data\temp945a.dbf" ALIAS temp945a
		SELECT 0
		USE "f:\3pl\data\pts_sent945.dbf" ALIAS pts_sent945
		APPEND FROM "f:\3pl\data\temp945a.dbf"
		USE IN pts_sent945
		USE IN temp945a
		DELETE FILE "f:\3pl\data\temp945a.dbf"
	ENDIF
	IF USED('parcel_carriers')
		USE IN parcel_carriers
	ENDIF
	
	IF !lTesting
*	asn_out_data()
	endif

CATCH TO oErr
	IF lDoCatch
		SET STEP ON
		lEmail = .F.
		lCloseOutput = .F.

		IF !cErrMsg = "TOP LEVEL"
			DO ediupdate WITH ALLT(oErr.MESSAGE),.T.
		ENDIF

		tsubject = cCustname+" Error ("+TRANSFORM(oErr.ERRORNO)+") at "+TTOC(DATETIME())
		tattach  = ""
		tsendto = IIF(lTesting,tsendtotest,tsendtoerr)
		tcc= IIF(lTesting,tcctest,tccerr)

		IF lPGTesting
			tsendto="pgaidis@fmiint.com"
			tcc=""
		ENDIF

		tmessage = cCustname+" Error processing "+CHR(13)
		tmessage =tmessage+ "Try/CATCH Exeception Message:"+CHR(13)+;
			[  Error: ] + STR(oErr.ERRORNO) +CHR(13)+;
			[  LineNo: ] + STR(oErr.LINENO) +CHR(13)+;
			[  Message: ] + oErr.MESSAGE +CHR(13)+;
			[  Procedure: ] + oErr.PROCEDURE +CHR(13)+;
			[  Details: ] + oErr.DETAILS +CHR(13)+;
			[  StackLevel: ] + STR(oErr.STACKLEVEL) +CHR(13)+;
			[  LineContents: ] + oErr.LINECONTENTS+CHR(13)+;
			[  UserValue: ] + oErr.USERVALUE

		tsubject = "945 EDI Poller Error at "+TTOC(DATETIME())
		tattach  = ""
		DO FORM m:\dev\frm\dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
	ENDIF
FINALLY
*RELEASE tsendto,tfrom,tsubject,tcc,tattach,tmessage
	SET DELETED ON
	ON ERROR
ENDTRY


*** END OF CODE BODY

****************************
PROCEDURE close945
****************************
** Footer Creation

	STORE  "GE"+cfd+ALLTRIM(STR(nSTCount))+cfd+c_CntrlNum+csegd TO cString
*!*		IF lTesting
*!*			FPUTS(nFilenum,cString)
*!*		ELSE
	FWRITE(nFilenum,cString)
*!*		ENDIF

	STORE  "IEA"+cfd+"1"+cfd+PADL(c_CntrlNum,9,"0")+csegd TO cString
*!*		IF lTesting
*!*			FPUTS(nFilenum,cString)
*!*		ELSE
	FWRITE(nFilenum,cString)
*!*		ENDIF

	RETURN

****************************
PROCEDURE num_incr_isa
****************************

	IF !USED("serfile")
		USE ("f:\3pl\data\serial\BCNY-Synclaire945_serial") IN 0 ALIAS serfile
	ENDIF
	SELECT serfile
	IF lTesting AND lISAFlag
		nOrigSeq = serfile.seqnum
		lISAFlag = .F.
	ENDIF
	nISA_Num = serfile.seqnum
	c_CntrlNum = ALLTRIM(STR(serfile.seqnum))
	REPLACE serfile.seqnum WITH serfile.seqnum + 1 IN serfile
	SELECT outship
	RETURN

****************************
PROCEDURE num_incr_st
****************************

	IF !USED("serfile")
		USE ("f:\3pl\data\serial\"+cCustname+"945_serial") IN 0 ALIAS serfile
	ENDIF
	SELECT serfile
	IF lTesting AND lSTFlag
		nOrigGrpSeq = serfile.grpseqnum
		lSTFlag = .F.
	ENDIF
	c_GrpCntrlNum = ALLTRIM(STR(serfile.grpseqnum))
	REPLACE serfile.grpseqnum WITH serfile.grpseqnum + 1 IN serfile
	SELECT outship
	RETURN


****************************
PROCEDURE segmentget
****************************

	PARAMETER thisarray,lcKey,nLength

	FOR i = 1 TO nLength
		IF i > nLength
			EXIT
		ENDIF
		lnEnd= AT("*",thisarray[i])
		IF lnEnd > 0
			lcThisKey =TRIM(SUBSTR(thisarray[i],1,lnEnd-1))
			IF OCCURS(lcKey,lcThisKey)>0
				RETURN SUBSTR(thisarray[i],lnEnd+1)
				i = 1
			ENDIF
		ENDIF
	ENDFOR

	RETURN ""

****************************
PROCEDURE ediupdate
****************************
	PARAMETER cStatus,lIsError
	lDoCatch = .F.

	IF !lTesting
		SELECT EDI_TRIGGER
		nRec = RECNO()
*		DO m:\dev\prg\edistatusmove WITH cBOL
		LOCATE
		IF !lIsError
			REPLACE processed WITH .T.,proc945 WITH .T.,file945 WITH cFilename,;
				fin_status WITH cStatus,errorflag WITH .F.,when_proc WITH DATETIME() ;
				isa_num WITH cISA_Num ;
				FOR EDI_TRIGGER.bol = cBOL AND accountid = nAcctNum

*			xsqlexec("update edi_trigger set processed=1, proc945=1, file945='"+cfilename+"', " + ;
				"fin_status='"+cstatus+"', errorflag=0, when_proc={"+ttoc(datetime())+"}, " + ;
				"isa_num='"+cisa_num+"' where bol='"+cbol+"' and accountid="+transform(nacctnum),,,"stuff")

		ELSE
			REPLACE processed WITH .T.,proc945 WITH .F.,file945 WITH "",;
				fin_status WITH cStatus,errorflag WITH .T. ;
				isa_num WITH cISA_Num ;
				FOR EDI_TRIGGER.bol = cBOL AND accountid = nAcctNum

*			xsqlexec("update edi_trigger set processed=1, proc945=0, file945='', " + ;
				"fin_status='"+cstatus+"', errorflag=1, isa_num='"+cisa_num+"' " + ;
				"where bol='"+cbol+"' and accountid="+transform(nacctnum),,,"stuff")
		ENDIF
		IF lCloseOutput
			=FCLOSE(nFilenum)
		ENDIF
	ENDIF

	IF lIsError AND lEmail
		tsubject = "945 Error in BCNY BOL "+TRIM(cBOL)+" (At PT "+cShip_ref+")"
		tattach = " "
		tsendto = tsendtoerr
		tcc = tccerr

		tmessage = "Generated from program: BCNYpp_create, within EDI Outbound Poller"
		tmessage = tmessage + CHR(13) + "945 Processing for BOL# "+TRIM(cBOL)+"(WO# "+cWO_Num+"), produced this error: (Office: "+cOffice+"): "+cStatus+CHR(13)+"Check EDI_TRIGGER and re-run"
		IF cStatus = "MISS PT"
			tmessage = tmessage + CHR(13) + "This error means that this PT exists in OUTSHIP, but not in the Pickpack table. Please add this WO to SQL."
		ENDIF
		IF lPGTesting
			tsendto="pgaidis@fmiint.com"
			tcc=""
		ENDIF

		DO FORM m:\dev\frm\dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
	ENDIF

	IF USED('outship')
		USE IN outship
	ENDIF
	IF USED('outdet')
		USE IN outdet
	ENDIF

	IF USED('serfile')
		SELECT serfile
		IF lTesting
			REPLACE serfile.seqnum WITH nOrigSeq
			REPLACE serfile.grpseqnum WITH nOrigGrpSeq
		ENDIF
		USE IN serfile
	ENDIF

	IF USED('scacs')
		USE IN scacs
	ENDIF

	IF USED('mm')
		USE IN mm
	ENDIF

	IF !lTesting
		SELECT EDI_TRIGGER
		LOCATE
	ENDIF
	RETURN

****************************
PROCEDURE cstringbreak
****************************
	cLen = LEN(ALLTRIM(cString))
*!*		IF lTesting
*!*			FPUTS(nFilenum,cString)
*!*		ELSE
	FWRITE(nFilenum,cString)
*!*		ENDIF
ENDPROC

****************************
PROCEDURE EndSEloop
****************************
	WAIT CLEAR
	WAIT WINDOW "Now creating Section Closure information" NOWAIT NOCLEAR
	IF lToys
		STORE "W03"+cfd+ALLTRIM(STR(nUnitSum))+cfd+ALLTRIM(STR(nTotCtnWt))+cfd+;
			"LB"+cfd+cCuFt+cfd+"CF"+csegd TO cString   && Units sum, Weight sum, Volume sum, carton count
	ELSE
		STORE "W03"+cfd+ALLTRIM(STR(nUnitSum))+cfd+ALLTRIM(STR(nTotCtnWt))+cfd+;
			"LB"+csegd TO cString   && Units sum, Weight sum, carton count
	ENDIF
	FWRITE(nFilenum,cString)

	STORE  "SE"+cfd+ALLTRIM(STR(nSegCtr+2))+cfd+PADL(c_GrpCntrlNum,9,"0")+csegd TO cString
	FWRITE(nFilenum,cString)
	nTotCtnWt = 0
	nTotCtnCount = 0
	nUnitSum = 0
ENDPROC