parameters lwebsite

if lwebsite
	xacctname=acctname(xaccountid)
	use f:\wo\wodata\manifest in 0
endif

xquery="select * from fxtrips where accountid="+transform(xaccountid)+" and delivered is null and delappt is not null"
xsqlexec(xquery,"xfxtrips",,"fx")

select * from xfxtrips group by wo_num into cursor csrtrip

select t.pickedup as "puDt", t.delappt, m.* ;
	from csrtrip t left join manifest m on t.wo_num = m.wo_num ;
	where m.complete into cursor csrtrip

if lwebsite &&create .pdf file put into MEMO field of PDF_FILE.DBF
	select csrtrip
	loadwebpdf("inTrans")

	use in fxtrips
	use in manifest
endif
