**ReportToPDF.prg
**
**Covert a foxpro report form into a PDF file via amyuni
**
Parameter lAuto, cReportName, cOutFilePath, cOutFileName, lnofilesearch
**cReportName: path+reportName (no ".frx" extension)
**cOutFileName: just filename (no ".pdf" extension)
**lnofilesearch: no need if the report form is in the project of exe

If !Directory(cOutFilePath)
	cOnError=on("error")
	xError = .f.
	on error xerror=.t.
	Md &cOutFilePath
	If xError
		If !lAuto
			strMsg = "The necessary output directory was not found and could not be created ("+;
					 Alltrim(cOutFilePath)+")."
			=MessageBox(strMsg,16,gcMsgTitle)
		EndIf
		Return
	EndIf
	on error &cOnError
EndIf

Store cOutFileName+[.pdf] TO cOutFileName

If !lnofilesearch and !File(cReportName+[.frx])
	If !lAuto
		=MessageBox("Report file not found..("+cReportName+[.frx]+"). Call MIS.",16,gcMsgTitle)
	EndIf
	Return
EndIf

**If the AMYUNI .DLL has been registered, no error will occur
cOnError=on("error")
xerror=.f.
on error xerror=.t.
zzpdf=createobject("CDINTFEX.CDINTFEX")

If !xerror
	zzPdf.DriverInit("Amyuni PDF")
	zzPdf.EnablePrinter("FMI International, LLC","07EFCDAB01000100623D96E70BCA3B68B46DA2DCA944A0EDD64F5E74834A4E6FB79B506FF685751CE497E011B7D2357C6F97D3E9C1D001A21BDA22D827CDE4EBAA72C918EE30FD7047742684C89A3D8E87FF835A531360B988A89C17C7B8B2A56689F7E7E02856")
else
	xerror=.f.
	zzPdf=CreateObject("CDINTF.CDINTF")
	on error &cOnError
	If !xerror
	  zzPdf.DriverInit("Amyuni PDF")
	EndIf
EndIf

If !xerror
	If File(cOutFilePath+cOutFileName)
	  Delete File (cOutFilePath+cOutFileName)
	EndIf

	zzPdf.FileNameOptions = 1 + 2
	zzPdf.DefaultFileName = cOutFilePath+cOutFileName

	set printer to name "Amyuni PDF"
	Report Form (cReportName) NOEJECT NOCONSOLE TO PRINTER

	** these lines added for the Amyuni printer on terminal server 2000
    cTSFilename = "E:\DOCUMENTS AND SETTINGS\ADMINISTRATOR\TEMPFOX\"+cOutFileName
    if file(cTSFilename)
       copy file "E:\DOCUMENTS AND SETTINGS\ADMINISTRATOR\TEMPFOX\"+cOutFileName to (cOutFilePath+cOutFileName)
    endif

    cTSFilename = "E:\DOCUMENTS AND SETTINGS\ADMINISTRATOR.NJI\TEMPFOX\"+cOutFileName
    if file(cTSFilename)
       copy file "E:\DOCUMENTS AND SETTINGS\ADMINISTRATOR.NJI\TEMPFOX\"+cOutFileName to (cOutFilePath+cOutFileName)
    endif
EndIf

lFileCreated = Iif(File(cOutFilePath+cOutFileName), .t., .f.)
