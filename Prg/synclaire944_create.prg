*!* SYNCLAIRE944_CREATE.PRG
*!* Created from the S3_944_CREATE.PRG, 10.02.2017, Joe

PARAMETERS nWO_Num
nTrk_WO = 0

PUBLIC lXfer944,lTesting,cSuffix,lHold944,lcArchivePath,lcOutPath,cFilenameOut,cFilenameHold,cFilenameArchive,cFin,cGroup,cMod,cOffice

lTesting = .T. && If true, disables certain functions (Default = .f.)
lTestInput = .F.  && If true, uses alternate source tables (Default = .f.)
lHold944 = lTesting && If true, holds output in 944HOLD (Default = .f.)
lEMail = .T.  && If true, sends regular email notices (Default = .t.)
*lTestMail = .t. && If true, sends mail ONLY to Joe & Jim (Default = .f.)
lOverrideXcheck = .F. && If true, doesn't check ASN carton count against barcode scans.
lEMail = .T.

DO m:\dev\prg\_setvars WITH lTesting

IF lTesting
	nWO_Num =     5360060
	CLOSE DATABASES ALL
ENDIF

nAcctNum = 6521
cOffice = "C"
cMod = '2'

IF USED('inwolog')
USE IN inwolog
endif

SET STEP ON 
xsqlexec("select * from inwolog where accountid = 6521 and wo_num = "+TRANSFORM(nWO_Num),,,"wh")
SELECT inwolog
alength = ALINES(apt,inwolog.comments,.T.,CHR(13)) && Creates an array of Comments lines

DIMENSION thisarray(1)
*!*	IF lTesting
*!*		cUseFolder = "F:\WHP\WHDATA\"
*!*	ELSE
	xReturn = "XXX"
	DO m:\dev\prg\wf_alt WITH cOffice,nAcctNum
	cUseFolder = UPPER(xReturn)
*!*	ENDIF

IF TYPE("nWO_Num") <> "N"  OR EMPTY(nWO_Num) && If WO is not numeric or equals Zero
	WAIT WINDOW "No WO# Provided...closing" TIMEOUT 3
	DO ediupdate WITH "NO WO#"
	closeout()
	RETURN
ENDIF

cWO_Num = ALLTRIM(STR(nWO_Num))
cIntUsage = IIF(lTesting,"T","P")

SELECT 0
USE F:\3pl\DATA\mailmaster ALIAS mm SHARED
SELECT mm
LOCATE FOR mm.accountid = nAcctNum ;
	AND mm.office = cOffice ;
	AND mm.edi_type = "944"
IF !FOUND()
	ASSERT .F. MESSAGE "At missing Office/Acct"
	WAIT WINDOW "Office/Loc/Acct not found!" TIMEOUT 2
	DO ediupdate WITH .T.,"ACCT/LOC !FOUND"
	NormalExit = .F.
	THROW
ENDIF
STORE TRIM(mm.acctname) TO cCustName
STORE TRIM(mm.basepath) TO lcOutPath
STORE TRIM(mm.holdpath) TO lcHoldPath
STORE TRIM(mm.archpath) TO lcArchivePath
_SCREEN.CAPTION = ALLTRIM(mm.scaption)+" "+TRANSFORM(nWO_Num)
IF lTesting
	LOCATE
	LOCATE FOR edi_type = "MISC" AND taskname = "GENERAL"
ENDIF
lUseAlt = mm.USE_ALT
tsendto = IIF(lUseAlt,mm.sendtoalt,mm.sendto)
tcc = IIF(lUseAlt,mm.ccalt,mm.cc)
LOCATE
LOCATE FOR mm.accountid = 9999 AND mm.office = "X"
lUseAlt = mm.USE_ALT
tsendtotest = IIF(lUseAlt,mm.sendtoalt,mm.sendto)
tcctest = IIF(lUseAlt,mm.ccalt,mm.cc)
LOCATE
LOCATE FOR edi_type = "MISC" AND taskname = "BBCERR"
lUseAlt = mm.USE_ALT
tsendtoerr = IIF(lUseAlt,mm.sendtoalt,mm.sendto)
tccerr = IIF(lUseAlt,mm.ccalt,mm.cc)
USE IN mm

goffice = cMod
gMasterOffice = cOffice

cASN = ""
nSTSets = 0
cSpecChar = .F.
XFERUPDATE = .T.
nFilenum = 0
lBackup = .F.
lOverage = .F.
lsendmail = .T.
lCopyCA = .T.
cErrMsg = ""
lIsError = .F.
lXfer944 = .F.
cTrk_WO = ""
nCTNTotal = 0


IF !lTesting
	IF !USED("edi_xfer")
		USE F:\edirouting\DATA\edi_xfer IN 0 ALIAS edi_xfer ORDER TAG inven_wo
		SELECT edi_xfer
		SET FILTER TO edi_type = "944" AND accountid = nAcctNum
		IF SEEK(nWO_Num)
			WAIT WINDOW "Work Order "+cWO_Num+" already created and logged in EDI_XFER" TIMEOUT 2
			IF MESSAGEBOX("944 for Work Order "+cWO_Num+" already created...Overwrite Date?",4+32+256,"WO# FOUND",8000) = 7 &&No
				USE IN edi_xfer
				DO ediupdate WITH "DUPLICATE WO#"
				closeout()
				RETURN
			ELSE
				XFERUPDATE = .F.
			ENDIF
		ENDIF
	ENDIF
ENDIF

*!* SET CUSTOMER CONSTANTS
cDirUsed = lcOutPath

cSyncMailName = cCustName
dt1 = TTOC(DATETIME(),1)
dt2 = DATETIME()
dtmail = TTOC(DATETIME())

cCustPrefix = "sync"
cFileShort = cCustPrefix+dt1+".944"
cFilenameHold = (lcHoldPath+cFileShort)
lcOutPath = IIF(lTesting, lcOutPath+"test\",lcOutPath)
cFilenameOut = lcOutPath+cFileShort
cFilenameArchive = (lcArchivePath+cFileShort)

STORE "" TO c_CntrlNum,c_GrpCntrlNum
nFilenum = FCREATE(cFilenameHold)

*!* SET OTHER CONSTANTS
nCtnNumber = 1

crecqual = "12"
crecid = '2122024725'
csendqual = "ZZ"
csendid = "TGF-US-SP"
cX12 = "004010"  && X12 Standards Set used

cfd = "*"  && Field/element delimiter
csegd = "!" && Line/segment delimiter
nSegCtr = 0
cterminator = ":" && Used at end of ISA segment
cdate = DTOS(DATE())
ctruncdate = RIGHT(cdate,6)
ctime = LEFT(TIME(DATE()),2)+SUBSTR(TIME(DATE()),4,2)
cfiledate = cdate+ctime
cOrig = "J"
cQtyRec = "1"
cCTypeUsed = ""
cMissingCtn = "Missing Cartons: "+CHR(13)
STORE 0 TO nQtyExpected,nCTNDamaged,nCTNShortage,nCTNExpected,nCTNActual
STORE 0 TO nPOExpecte,nPOActual,nCTNShortage
STORE "" TO cString,cPONum,cStyle

** PAD ID Codes
csendidlong = PADR(csendid,15," ")
crecidlong = PADR(crecid,15," ")

*!* Serial number incrementing table
IF !USED("serfile")
	USE ("F:\3pl\data\serial\bbc944_serial") IN 0 ALIAS serfile
ENDIF

*!* Trigger Data
IF !USED('edi_trigger')
	cEDIFolder = "F:\3PL\DATA\"
	USE (cEDIFolder+"edi_trigger") IN 0 ALIAS edi_trigger ORDER TAG wo_num
ENDIF

*!* Search table (parent)

SELECT inwolog
IF RECCOUNT()  = 0
	WAIT WINDOW "WO# "+cWO_Num+" not found in INWOLOG...Terminating" TIMEOUT 3
	IF !lTesting
		DO ediupdate WITH "MISS INWOLOG WO",.T.
		closeout()
		RETURN
	ENDIF
ENDIF

*!* Search table (xref for PO's)
IF USED('INDET')
	USE IN indet
ENDIF

IF lTesting
	xsqlexec("select * from pl where accountid = "+TRANSFORM(nAcctNum)+" and wo_num = "+TRANSFORM(nWO_Num),"indet",,"wh")
ELSE
	xsqlexec("select * from indet where accountid = "+TRANSFORM(nAcctNum)+" and wo_num = "+TRANSFORM(nWO_Num),,,"wh")
ENDIF
INDEX ON inwologid TAG inwologid

nRCnt = 0
nISACount = 0
nSTCount = 1
nSECount = 1

*!*  SELECT inwolog
*!*  WAIT WINDOW "" TIMEOUT 2
*!*  SET STEP ON
*!*  IF !SEEK(nWO_Num,'inwolog','wo_num')
*!*    WAIT WINDOW "WO# "+cWO_Num+" not found in INWOLOG...Terminating" TIMEOUT 3
*!*    IF !lTesting
*!*      DO ediupdate WITH "MISS INWOLOG WO",.T.
*!*      closeout()
*!*      RETURN
*!*    ENDIF
*!*  ENDIF

*!*  alength = ALINES(apt,inwolog.comments,.T.,CHR(13)) && Creates an array of Comments lines
*!*  nTrk_WO = INT(VAL(inwolog.comments))  && Extracts Trucking WO# from INWOLOG
*!*  cTrk_WO = ALLTRIM(STR(nTrk_WO))
cContainer = inwolog.CONTAINER

IF EMPTY(TRIM(cContainer))
	WAIT WINDOW "There is no container attached to "+cWO_Num TIMEOUT 3
	DO ediupdate WITH "NO CONTAINER"
	closeout()
	RETURN
ENDIF

SELECT indet
LOCATE FOR wo_num = nWO_Num
IF !FOUND()
	waitstr = "WO# "+cWO_Num+" Not Found in Inbd Detail Table..."+CHR(13)+;
		"WO has not been confirmed...Terminating"
	WAIT WINDOW waitstr TIMEOUT 3
	DO ediupdate WITH "WO NOT CONF. IN INDET"
	closeout()
	RETURN
ENDIF

SELECT inwolog
cAcctname = TRIM(inwolog.acctname)
*cRefer_num = TRIM(inwolog.reference)
cRefer_num = ""
cAcct_ref = ALLTRIM(inwolog.acct_ref)
IF nISACount = 0
	DO num_incr
	STORE "ISA"+cfd+"00"+cfd+"          "+cfd+"00"+cfd+"          "+cfd+csendqual+cfd+csendidlong+cfd+;
		crecqual+cfd+crecidlong+cfd+ctruncdate+cfd+ctime+cfd+"U"+cfd+"00401"+cfd+;
		PADL(c_CntrlNum,9,"0")+cfd+"0"+cfd+cIntUsage+cfd+cterminator+csegd TO cString
	DO cstringbreak

	STORE "GS"+cfd+"RE"+cfd+csendid+cfd+crecid+cfd+cdate+cfd+ctime+cfd+c_CntrlNum+;
		cfd+"X"+cfd+cX12+csegd TO cString
	DO cstringbreak

	nSegCtr = 0
	nISACount = 1
ENDIF

IF nSECount = 1
	STORE "ST"+cfd+"944"+cfd+PADL(c_GrpCntrlNum,9,"0")+csegd TO cString
	DO cstringbreak
	nSegCtr = nSegCtr + 1

*!*    cRefer_num = segmentget(@apt,"BOL",alength)
*!*    IF EMPTY(ALLTRIM(cRefer_num))
*!*  *!*      cRefer_num = "UNKNOWN"
*!*      DO ediupdate WITH "MISS REF BOL"
*!*      closeout()
*!*      RETURN
*!*    ENDIF

	STORE "W17"+cfd+cOrig+cfd+cdate+cfd+cRefer_num+cfd+TRIM(cWO_Num)+csegd TO cString
	DO cstringbreak
	nSegCtr = nSegCtr + 1

	STORE "N1"+cfd+"ZL"+cfd+cCustname+csegd TO cString
	DO cstringbreak
	nSegCtr = nSegCtr + 1

	STORE "N9"+cfd+"CN"+cfd+cContainer+csegd TO cString  && Used for either AWB or container type
	DO cstringbreak
	nSegCtr = nSegCtr + 1

	STORE "N9"+cfd+"TL"+cfd+cAcct_ref+csegd TO cString
	DO cstringbreak
	nSegCtr = nSegCtr + 1

	STORE "N9"+cfd+"DE"+cfd+ALLTRIM(TRANSFORM(nWO_Num))+csegd TO cString
	DO cstringbreak
	nSegCtr = nSegCtr + 1


	STORE "G62"+cfd+"09"+cfd+DTOS(DATE())+csegd TO cString
	DO cstringbreak
	nSegCtr = nSegCtr + 1

	nSECount = 0
ENDIF

SELECT indet
SUM totqty  TO nSumTotQty  FOR wo_num = nWO_Num AND indet.inwologid = inwolog.inwologid AND units
*SUM origqty TO nSumOrigQty FOR wo_num = nWO_Num AND indet.inwologid = inwolog.inwologid
*nDiffQty = (nSumOrigQty - nSumTotQty)
nSumOrigQty = nSumTotQty
nDiffQty = (nSumTotQty)

SELECT STYLE,COLOR,ID,ECHO,SUM(totqty) AS totqty ;
	FROM indet ;
	WHERE wo_num = nWO_Num ;
	AND units ;
	AND indet.inwologid = inwolog.inwologid ;
	GROUP BY 1,2,3,4 ;
	ORDER BY 1,2,3,4 ;
	INTO CURSOR indet1

SELECT indet1

SELECT indet
SCAN FOR !units
&& first check for Multiline details

	IF "MULTILINES"$ECHO
		CREATE CURSOR rollout (;
			upc CHAR(13),;
			linenum CHAR(10),;
			ponum CHAR(15),;
			poqty CHAR(4),;
			PACK CHAR(10))

		SELECT indet
		numRollout = OCCURS("UPC",ECHO)
		alength = ALINES(apt,indet.ECHO,.T.,CHR(13))
		offset = 0
		FOR xxi = 1 TO numRollout
			m.upc     = SUBSTR(apt[xxi+1+offset],AT("*",apt[xxi+1+offset])+1)
			m.linenum = SUBSTR(apt[xxi+2+offset],AT("*",apt[xxi+2+offset])+1)
			m.ponum   = SUBSTR(apt[xxi+3+offset],AT("*",apt[xxi+3+offset])+1)
			m.poqty   = SUBSTR(apt[xxi+5+offset],AT("*",apt[xxi+5+offset])+1)
			m.pack    = indet.PACK
			SELECT rollout
			INSERT INTO rollout FROM MEMVAR
			offset = offset +4
		ENDFOR

		SELECT rollout
		SCAN
			lnxqty = VAL(rollout.poqty)* VAL(rollout.PACK)
			STORE "W07"+cfd+ALLTRIM(STR(lnxqty))+cfd+"EA"+cfd+cfd+"VN"+cfd+rollout.upc+csegd TO cString
			DO cstringbreak
			nSegCtr = nSegCtr + 1

			STORE "N9"+cfd+"ST"+cfd+ALLTRIM(indet.STYLE)+csegd TO cString
			DO cstringbreak
			nSegCtr = nSegCtr + 1

			STORE "N9"+cfd+"CL"+cfd+ALLTRIM(indet.COLOR)+csegd TO cString
			DO cstringbreak
			nSegCtr = nSegCtr + 1

			STORE "N9"+cfd+"SZ"+cfd+ALLTRIM(indet.ID)+csegd TO cString
			DO cstringbreak
			nSegCtr = nSegCtr + 1

			STORE "N9"+cfd+"PO"+cfd+rollout.ponum+csegd TO cString
			DO cstringbreak
			nSegCtr = nSegCtr + 1

			STORE "N9"+cfd+"LI"+cfd+rollout.linenum+csegd TO cString
			DO cstringbreak
			nSegCtr = nSegCtr + 1
		ENDSCAN
*!*      Select indet
*!*      Skip 1 In indet
*!*      loop
	ELSE

		STORE "W07"+cfd+ALLTRIM(STR(indet.totqty*VAL(indet.PACK)))+cfd+"EA"+cfd+cfd+"VN"+cfd+TRIM(getmemodata("echo","UPC"))+csegd TO cString
		nCTNExpected = nCTNExpected + 1
		DO cstringbreak
		nSegCtr = nSegCtr + 1

		STORE "N9"+cfd+"ST"+cfd+ALLTRIM(indet.STYLE)+csegd TO cString
		DO cstringbreak
		nSegCtr = nSegCtr + 1

		STORE "N9"+cfd+"CL"+cfd+ALLTRIM(indet.COLOR)+csegd TO cString
		DO cstringbreak
		nSegCtr = nSegCtr + 1

		STORE "N9"+cfd+"SZ"+cfd+ALLTRIM(indet.ID)+csegd TO cString
		DO cstringbreak
		nSegCtr = nSegCtr + 1

		STORE "N9"+cfd+"PO"+cfd+TRIM(getmemodata("echo","PONUM"))+csegd TO cString
		DO cstringbreak
		nSegCtr = nSegCtr + 1

		STORE "N9"+cfd+"LI"+cfd+TRIM(getmemodata("echo","LINENUM"))+csegd TO cString
		DO cstringbreak
		nSegCtr = nSegCtr + 1
	ENDIF


ENDSCAN

WAIT WINDOW "END OF SCAN/CHECK PHASE ROUND..." NOWAIT

nSTCount = 0
DO close944
lXfer944 = .T.
=FCLOSE(nFilenum)


SET STEP ON
IF USED('serfile')
	USE IN serfile
ENDIF

IF !lTesting
	IF !USED("ftpedilog")
		SELECT 0
		USE F:\edirouting\ftpedilog ALIAS ftpedilog
		INSERT INTO ftpedilog (ftpdate,filename,acct_name,TYPE) VALUES (dt2,cFilenameHold,"BBC","944")
		USE IN ftpedilog
	ENDIF
ENDIF

cFin = "944 FILE "+cFilenameOut+" CREATED WITHOUT ERRORS"
IF !lTesting
	SELECT edi_trigger
	REPLACE edi_trigger.processed WITH .T.,edi_trigger.created WITH .T.,edi_trigger.when_proc WITH DATETIME(),;
		edi_trigger.fin_status WITH "944 CREATED",edi_trigger.errorflag WITH .F.,file944crt WITH cFilenameHold ;
		FOR  edi_trigger.accountid = nAcctNum AND edi_trigger.wo_num = nWO_Num IN edi_trigger
	sendmail()
ENDIF

WAIT "BBC 944 Creation process complete:"+CHR(13)+cFilenameHold WINDOW AT 45,60 TIMEOUT 3

COPY FILE &cFilenameHold TO &cFilenameArchive
IF lHold944 = .F.
	COPY FILE [&cFilenameHold] TO [&cFilenameOut]
	ERASE [&cFilenameHold]
ENDIF

RELEASE ALL LIKE T*
closedata()

************************************************************************
************************************************************************
*!* END OF MAIN PROGRAM
************************************************************************
************************************************************************


****************************
PROCEDURE close944
****************************
** Footer Creation

	IF nSTCount = 0
*!* For 944 process only!
		STORE "W14"+cfd+ALLTRIM(STR(nSumTotQty))+;
			cfd+ALLTRIM(STR(nSumOrigQty))+;
			cfd+ALLTRIM(STR(nSumOrigQty-nSumTotQty))+csegd TO cString
		DO cstringbreak
		nSegCtr = nSegCtr + 1
*!* End 944 addition

		STORE  "SE"+cfd+ALLTRIM(STR(nSegCtr+1))+cfd+PADL(c_GrpCntrlNum,9,"0")+csegd TO cString
		FPUTS(nFilenum,cString)
		nSegCtr = 0
		nSTSets = nSTSets + 1
	ENDIF

	STORE  "GE"+cfd+ALLTRIM(STR(nSTSets))+cfd+c_CntrlNum+csegd TO cString
	FPUTS(nFilenum,cString)

	STORE  "IEA"+cfd+"1"+cfd+PADL(c_CntrlNum,9,"0")+csegd TO cString
	FPUTS(nFilenum,cString)

ENDPROC

****************************
PROCEDURE cstringbreak
****************************
	FPUTS(nFilenum,cString)
ENDPROC

****************************
PROCEDURE num_incr
****************************
	SELECT serfile
	REPLACE serfile.seqnum WITH serfile.seqnum + 1 IN serfile
	REPLACE serfile.grpseqnum WITH serfile.grpseqnum + 1 IN serfile
	c_CntrlNum = ALLTRIM(STR(serfile.seqnum))
	c_GrpCntrlNum = ALLTRIM(STR(serfile.grpseqnum))
ENDPROC

****************************
PROCEDURE ediupdate
****************************
	PARAMETERS cFin
	IF !lTesting
		SELECT edi_trigger
		REPLACE processed WITH .T.
		REPLACE created WITH .F.
		REPLACE fin_status WITH cFin
		IF lIsError
			REPLACE errorflag WITH .T.
		ELSE
			REPLACE when_proc WITH DATETIME()
			REPLACE errorflag WITH .F.
		ENDIF
		BLANK FIELDS edi_trigger.file944crt NEXT 1
		IF lsendmail
			sendmail()
		ENDIF
	ENDIF
	closeout()
ENDPROC

****************************
PROCEDURE closeout
****************************
	IF !EMPTY(nFilenum)
		=FCLOSE(nFilenum)
		ERASE &cFilename
	ENDIF
	IF USED('barcodes')
		SELECT BARCODES
		SET FILTER TO
		USE IN BARCODES
	ENDIF
	closedata()
ENDPROC

****************************
PROCEDURE closedata
****************************
	IF USED('barcodes')
		SELECT BARCODES
		SET FILTER TO
		USE IN BARCODES
	ENDIF
	IF USED('edi_xfer')
		USE IN edi_xfer
	ENDIF
	IF USED('csrBCdata')
		USE IN csrBCdata
	ENDIF
	IF USED('detail')
		USE IN DETAIL
	ENDIF
	IF USED('indet')
		USE IN indet
	ENDIF
	IF USED('inwolog')
		USE IN inwolog
	ENDIF

	ON ERROR
	WAIT CLEAR
	_SCREEN.CAPTION = "INBOUND POLLER - EDI"
	RETURN

****************************
PROCEDURE sendmail
****************************
	tfrom = "TGF EDI Operations <toll-edi-ops@tollgroup.com>"

	IF lIsError
		tsendto = tsendtoerr
		tcc = tccerr
		tsubject = cS3MailName+" EDI ERROR, Inv. WO "+cWO_Num
	ELSE
		tsubject = cS3MailName+" EDI FILE (944) Created, Inv. WO "+cWO_Num
	ENDIF


	tattach = " "
	IF (cTrk_WO = "0" OR EMPTY(cTrk_WO))
		cThisTrk_WO = "MISSING"
		tmessage = "Inv. WO #: "+cWO_Num++CHR(10)
	ELSE
		cThisTrk_WO = cTrk_WO
		tmessage = "Inv. WO #: "+cWO_Num+", Trucking WO #: "+cThisTrk_WO+CHR(10)
	ENDIF

	IF EMPTY(cErrMsg)
		tmessage = tmessage + cFin
	ELSE
		tmessage = tmessage + cErrMsg
	ENDIF

	IF lEMail
*  Do Form m:\dev\frm\dartmail2 With tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
	ENDIF

ENDPROC


****************************
PROCEDURE errormail
****************************
	IF nDiffQty > 0
		tnote = "n OVERAGE"
	ELSE
		tnote = " SHORTAGE"
	ENDIF
	tsendto = tsendtotest
	tcc = tccerr
	tfrom = "TGF EDI Operations <toll-edi-ops@tollgroup.com>"
	tsubject = "ERROR: BBC 944 WO# "+cWO_Num
	tmessage = "The 944 for this WO was received as a"+ tnote
	tmessage = "Original Quantity per ASN: "+ALLTRIM(STR(nSumOrigQty))
	tmessage = "Quantity As Received: "+ALLTRIM(STR(nSumTotQty))

	DO FORM m:\dev\frm\dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
ENDPROC

****************************
PROCEDURE segmentget
****************************
	PARAMETER thisarray,lcKey,nLength

	FOR i = 1 TO nLength
		IF i > nLength
			EXIT
		ENDIF
		lnEnd= AT("*",thisarray[i])
		IF lnEnd > 0
			lcThisKey =TRIM(SUBSTR(thisarray[i],1,lnEnd-1))
			IF OCCURS(lcKey,lcThisKey)>0
				RETURN SUBSTR(thisarray[i],lnEnd+1)
				i = 1
			ENDIF
		ENDIF
	ENDFOR

	RETURN ""
