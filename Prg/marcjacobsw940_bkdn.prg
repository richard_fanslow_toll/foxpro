*!* m:\dev\prg\marcjacobsw940_bkdn.prg, 11.29.2011, Joe
lCheckStyle = .T.
lPrepack = .F.
lPick = .T.
lJCPenney = .F.
lDoImport = .T.

STORE "" TO m.isa_num,m.id,cStylecheck,cSTStoreNum,cDesc
ptctr = 0

lBrowfiles = lTesting
*lBrowfiles = .t.

SELECT x856
COUNT FOR TRIM(x856.segment) = "ST" TO nPTQty
cSegCnt = ALLTRIM(STR(nPTQty))
WAIT WINDOW "There are "+cSegCnt+" picktickets in file "+cFilename TIMEOUT WAITTIME

LOCATE
IF lTesting
	WAIT WINDOW "Now in 940 Breakdown"+CHR(13)+"RUNNING IN TEST MODE..." TIMEOUT WAITTIME
	m.ship_via = "WILL CALL CUST"
ELSE
	WAIT WINDOW "Now in 940 Breakdown" TIMEOUT WAITTIME
ENDIF
WAIT "There are "+cSegCnt+" P/Ts in this file" WINDOW TIMEOUT WAITTIME

SELECT x856
LOCATE

*!* Constants
m.acct_name = cCustname && Set in start prg
m.acct_num = nAcctNum
m.sf_addr1 = "72 SPRING STREET"
m.sf_addr2 = "7TH FLOOR"
m.sf_csz = "NEW YORK, NY 10012"

*!*	IF !USED("uploaddet")
*!*		IF lTesting
*!*			USE F:\ediwhse\test\uploaddet IN 0 ALIAS uploaddet
*!*		ELSE
*!*			USE F:\ediwhse\DATA\uploaddet IN 0 SHARED ALIAS uploaddet
*!*		ENDIF
*!*	ENDIF

SELECT xpt
DELETE ALL
SELECT xptdet
DELETE ALL

SELECT x856
SET FILTER TO
LOCATE

cPTDupes = ""

DO WHILE !EOF("x856")
	cSegment = TRIM(x856.segment)
	cCode = TRIM(x856.f1)

	IF TRIM(x856.segment) = "ISA"
		m.isa_num = ALLTRIM(x856.f13)
		SELECT x856
		SKIP
		LOOP
	ENDIF

	IF TRIM(x856.segment) = "GS"
		SELECT x856
		SKIP
		LOOP
	ENDIF

	IF TRIM(x856.segment) = "ST" AND TRIM(x856.f1) = "940"
		SELECT x856
		SKIP
		LOOP
	ENDIF

	IF TRIM(x856.segment) = "W05"  && new shipment
		cShip_ref = ALLTRIM(x856.f5) && PG from F2 to F5 1/12/2012
		IF SEEK(PADR(cShip_ref,20),'pt','ship_ref')
			cPTDupes = IIF(EMPTY(cPTDupes),cShip_ref,cPTDupes+CHR(13)+cShip_ref)
			SELECT x856
			DO WHILE trim(x856.segment)#"SE"
			SKIP
			ENDDO
			SKIP 1 IN x856
			LOOP
		ENDIF
		cOrderNumber=ALLTRIM(x856.f2) && the MJ Order Number, stored in shipins
		lDoInsert = .F.
		WAIT WINDOW AT 10,10 "Now processing PT# "+ cShip_ref NOWAIT NOCLEAR
		SELECT xpt
		nDetCnt = 0
		m.pack = "1"
		APPEND BLANK
		STORE 0 TO pt_total_eaches
		STORE 0 TO pt_total_cartons
		ptctr = ptctr +1
		REPLACE xpt.ptid WITH ptctr IN xpt
		STORE ptctr TO m.ptid
		REPLACE xpt.goh WITH .T. IN xpt
		REPLACE xpt.accountid  WITH m.acct_num IN xpt
		REPLACE xpt.ptdate     WITH DATE() IN xpt
		REPLACE xpt.ship_ref   WITH cShip_ref IN xpt  && Pick Ticket
		m.pt = ALLTRIM(x856.f2)
		IF lTesting
*			REPLACE xpt.cacctnum   WITH "X999" IN xpt
		ENDIF
		REPLACE xpt.cnee_ref WITH ALLTRIM(x856.f3) IN xpt
		REPLACE xpt.shipins WITH IIF(EMPTY(xpt.shipins),"ORDERNUMBER*"+cOrderNumber,xpt.shipins+CHR(13)+"ORDERNUMBER*"+cOrderNumber) IN xpt
		SELECT x856
		SKIP
		LOOP
	ENDIF

	IF TRIM(x856.segment) = "N1" AND TRIM(x856.f1) = "ST" && Ship-to data
*!*			IF !EMPTY(m.isa_num)
*!*				SELECT uploaddet
*!*				APPEND BLANK
*!*				REPLACE uploaddet.office    WITH cOffice
*!*				REPLACE uploaddet.cust_name WITH UPPER(TRIM(x856.f2))
*!*				REPLACE uploaddet.acct_num  WITH m.acct_num
*!*				REPLACE uploaddet.DATE      WITH DATETIME()
*!*				REPLACE uploaddet.isa_num   WITH m.isa_num
*!*				REPLACE uploaddet.startpt   WITH xpt.ship_ref
*!*				REPLACE uploaddet.runid     WITH nRunID
*!*			ENDIF

		SELECT xpt
		cConsignee = UPPER(TRIM(x856.f2))
		m.st_name = TRIM(x856.f2)
		REPLACE xpt.consignee WITH cConsignee IN xpt
		REPLACE xpt.NAME      WITH cConsignee IN xpt  && added this as Consignee will be over written by the N9*CU*new name value

		cStoreNum = TRIM(x856.f4)
		cSTStoreNum = TRIM(x856.f4)
*		ASSERT .f. MESSAGE "At store number conversion"
		IF LEN(cStoreNum)>5
			REPLACE xpt.storenum  WITH VAL(RIGHT(cStoreNum,5)) IN xpt
		ELSE
			REPLACE xpt.storenum  WITH VAL(cStoreNum) IN xpt
		ENDIF
		REPLACE xpt.dcnum  WITH cStoreNum IN xpt
		REPLACE xpt.shipins WITH IIF(EMPTY(xpt.shipins),"STORENUM*"+cStoreNum,xpt.shipins+CHR(13)+"STORENUM*"+cStoreNum) IN xpt

		SKIP 1 IN x856
		IF TRIM(x856.segment) = "N3"  && address info
			m.st_addr1 = UPPER(TRIM(x856.f1))
			IF LEN(m.st_addr1)>30
				cSt_addr1ext = SUBSTR(m.st_addr1,21)
				REPLACE xpt.shipins WITH ;
					IIF(EMPTY(xpt.shipins),"ST_ADDR1EXT*"+cSt_addr1ext,;
					xpt.shipins+CHR(13)+"ST_ADDR1EXT*"+cSt_addr1ext)
				m.st_addr1 = LEFT(m.st_addr1,30)
			ENDIF
			m.st_addr2 = UPPER(TRIM(x856.f2))
			IF LEN(m.st_addr2)>30
				cSt_addr2ext = SUBSTR(m.st_addr2,21)
				REPLACE xpt.shipins WITH ;
					IIF(EMPTY(xpt.shipins),"ST_ADDR2EXT*"+cSt_addr2ext,;
					xpt.shipins+CHR(13)+"ST_ADDR2EXT*"+cSt_addr2ext)
				m.st_addr2 = LEFT(m.st_addr2,30)
			ENDIF
			REPLACE xpt.address  WITH m.st_addr1
			REPLACE xpt.address2 WITH m.st_addr2
		ENDIF
		SKIP 1 IN x856
		IF TRIM(x856.segment) = "N3"  && 2nd line address info
			m.st_addr2 = UPPER(TRIM(x856.f1))
			IF LEN(m.st_addr2)>30
				cSt_addr2ext = SUBSTR(m.st_addr2,21)
				REPLACE xpt.shipins WITH ;
					IIF(EMPTY(xpt.shipins),"ST_ADDR2EXT*"+cSt_addr2ext,;
					xpt.shipins+CHR(13)+"ST_ADDR2EXT*"+cSt_addr2ext)
				m.st_addr2 = LEFT(m.st_addr2,30)
			ENDIF
			SKIP 1 IN x856
		ENDIF

		IF TRIM(x856.segment) = "N4"  && address info
			cZipcode = TRIM(x856.f3)
			cCity = LEFT(UPPER(TRIM(x856.f1)),20)
			cState = UPPER(TRIM(x856.f2))
			cCountry = ALLTRIM(x856.f4)
			REPLACE xpt.shipins WITH ;
				IIF(EMPTY(xpt.shipins),;
				"CITY*"+cCity+CHR(13)+"STATE*"+cState+CHR(13)+"ZIPCODE*"+cZipcode+CHR(13)+"COUNTRY*"+cCountry,;
				xpt.shipins+CHR(13)+"CITY*"+cCity+CHR(13)+"STATE*"+cState+CHR(13)+"ZIPCODE*"+cZipcode+CHR(13)+"COUNTRY*"+cCountry)
			m.st_csz = cCity+", "+cState+" "+cZipcode
			REPLACE xpt.csz WITH m.st_csz
		ENDIF
		SELECT x856
		SKIP
		LOOP
	ENDIF

	IF TRIM(x856.segment) = "N1" AND TRIM(x856.f1) = "Z7"
		SELECT xpt
		REPLACE xpt.shipfor WITH ALLTRIM(x856.f2)
		cSFStoreNum = TRIM(x856.f4)
		REPLACE xpt.sforstore  WITH ALLTRIM(cSFStoreNum)
		REPLACE xpt.storenum  WITH VAL(RIGHT(ALLTRIM(cSFStoreNum),5))
		SKIP 1 IN x856
		IF TRIM(x856.segment) = "N3"  && address info
			m.sforaddr1 = UPPER(TRIM(x856.f1))
			m.sforaddr2 = UPPER(TRIM(x856.f2))
			REPLACE xpt.sforaddr1 WITH m.sforaddr1
			REPLACE xpt.sforaddr2 WITH m.sforaddr2
		ELSE
			LOOP
		ENDIF
		SKIP 1 IN x856
		IF TRIM(x856.segment) = "N3"  && address info
			m.sforaddr2 = UPPER(TRIM(x856.f1))
			REPLACE xpt.sforaddr2 WITH m.sforaddr2
			SKIP 1 IN x856
		ENDIF
		IF TRIM(x856.segment) = "N4"  && address info
			cSforZipcode = TRIM(x856.f3)
			cSforCity = LEFT(UPPER(TRIM(x856.f1)),20)
			cSforState = UPPER(TRIM(x856.f2))
			cSforCountry = ALLTRIM(x856.f4)
			m.sforcsz = UPPER(TRIM(x856.f1))+", "+UPPER(TRIM(x856.f2))+" "+TRIM(x856.f3)
			REPLACE xpt.sforcsz WITH m.sforcsz
		ENDIF
		REPLACE shipins WITH ;
			IIF(EMPTY(xpt.shipins),;
			"SFORCITY*"+cCity+CHR(13)+"SFORSTATE*"+cSforState+CHR(13)+"SFORZIPCODE*"+cSforZipcode+CHR(13)+;
			"SFORCOUNTRY*"+cSforCountry,;
			xpt.shipins+CHR(13)+"SFORCITY*"+cSforCity+CHR(13)+"SFORSTATE*"+cSforState+CHR(13)+"SFORZIPCODE*"+;
			cSforZipcode+CHR(13)+"SFORCOUNTRY*"+cSforCountry)
		SELECT x856
		SKIP
		LOOP
	ENDIF

	IF TRIM(x856.segment) = "N9"
		DO CASE
			CASE TRIM(x856.f1) = "DP"
				cDept = ALLTRIM(x856.f2)
				cDeptname = "DEPTNAME*"+ALLTRIM(x856.f3)
				REPLACE xpt.dept WITH cDept IN xpt
				REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+cDeptname IN xpt

			CASE TRIM(x856.f1) = "IA" && Internal Cust. Acct. ID
				cVendNum =ALLTRIM(x856.f2)
				REPLACE xpt.vendor_num WITH cVendNum IN xpt

			CASE TRIM(x856.f1) = "DV"
				cDivCode = "DIVCODE*"+ALLTRIM(x856.f2)
				REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+cDivCode IN xpt

			CASE TRIM(x856.f1) = "CU"
				cCacctnum = ALLTRIM(x856.f2)
				REPLACE xpt.cacctnum WITH cCacctnum IN xpt
				cConsignee = UPPER(TRIM(x856.f3))  && added this update 2/7/2012 PG

				IF !"MARC JACOBS"$cConsignee  && on retail orders keep the original shipto
					m.st_name = TRIM(x856.f2)
					REPLACE xpt.consignee WITH cConsignee IN xpt
				ENDIF

			CASE TRIM(x856.f1) = "TM"
				cTerms = ALLTRIM(x856.f2)
				cTermsDesc = "TERMS*"+ALLTRIM(x856.f3)
				REPLACE xpt.terms WITH cTerms IN xpt
				REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+cTermsDesc IN xpt

			CASE TRIM(x856.f1) = "3P"
				cTPA = "TPA*"+ALLTRIM(x856.f2)
				REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+cTPA IN xpt

			CASE TRIM(x856.f1) = "VAT"
				cVAT = "VAT*"+ALLTRIM(x856.f2)
				REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+cVAT IN xpt

			CASE TRIM(x856.f1) = "TR"
				cTR = "TERMSCODE*"+ALLTRIM(x856.f2)
				REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+cTR IN xpt
		ENDCASE
		SELECT x856
		SKIP
		LOOP
	ENDIF

	IF TRIM(x856.segment) = "G62" AND (TRIM(x856.f1) == "10") && Start date
		lcDate = ALLTRIM(x856.f2)
		lxDate = SUBSTR(lcDate,5,2)+"/"+SUBSTR(lcDate,7,2)+"/"+LEFT(lcDate,4)
		ldDate = CTOD(lxDate)
		REPLACE xpt.START WITH ldDate IN xpt
		SELECT x856
		SKIP
		LOOP
	ENDIF

	IF TRIM(x856.segment) = "G62" AND TRIM(x856.f1) == "01" && Cancel date
		lcDate = ALLTRIM(x856.f2)
		lxDate = SUBSTR(lcDate,5,2)+"/"+SUBSTR(lcDate,7,2)+"/"+LEFT(lcDate,4)
		ldDate = CTOD(lxDate)
		REPLACE xpt.CANCEL WITH ldDate IN xpt
		SELECT x856
		SKIP
		LOOP
	ENDIF

	IF TRIM(x856.segment) = "NTE"  && Notes
		DO CASE
			CASE TRIM(x856.f1) = "EDI"
				REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"FOB*"+ALLT(x856.f2) IN xpt
			CASE TRIM(x856.f1) = "DEL"
				REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"DELIVINST*"+ALLT(x856.f2) IN xpt
			CASE TRIM(x856.f1) = "PKG"
				REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"PKGINST*"+ALLT(x856.f2) IN xpt
			CASE TRIM(x856.f1) = "SPH"
				REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"SPHANDLE*"+ALLT(x856.f2) IN xpt
			CASE TRIM(x856.f1) = "WHI"
				REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"SPINS*"+ALLT(x856.f2) IN xpt
			CASE TRIM(x856.f1) = "GFT"
				REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"GIFTMSG*"+ALLT(x856.f2) IN xpt
			CASE TRIM(x856.f1) = "PRO"
				REPLACE xpt.keyrec WITH "PR"+ALLT(x856.f2) IN xpt
			CASE TRIM(x856.f1) = "MBO"
				REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"MBOL*"+ALLT(x856.f2)
			CASE TRIM(x856.f1) = "BOL"
				REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"BOL*"+ALLT(x856.f2)
			CASE TRIM(x856.f1) = "PDN"
				REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"PROMO*"+ALLT(x856.f2)
			CASE TRIM(x856.f1) = "REF"
				REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"REFNUMPO*"+ALLT(x856.f2)
			CASE TRIM(x856.f1) = "ZZZ"
				REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"REFNUMOTH*"+ALLT(x856.f2)
			CASE TRIM(x856.f1) = "MRT"
				REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"EDIPOTYPE*"+ALLT(x856.f2)
			CASE TRIM(x856.f1) = "STT"
				REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"STORETYPE*"+ALLT(x856.f2)
			CASE TRIM(x856.f1) = "BWV"
				REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"BCWAVENUM*"+ALLT(x856.f2)
			CASE TRIM(x856.f1) = "BOS"
				REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"BCORDERSOURCE*"+ALLT(x856.f2)
			CASE TRIM(x856.f1) = "CAC"
				REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"DBA*"+ALLT(x856.f2)
		ENDCASE
	ENDIF


	IF TRIM(x856.segment) = "W66"  && Shipper Information
		cShipcode = UPPER(ALLTRIM(x856.f2))
		cShip_via = UPPER(ALLTRIM(x856.f5))
		cSCAC = ALLTRIM(x856.f10)
		REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"SHIPCODE*"+cShipcode IN xpt
		REPLACE xpt.scac WITH cSCAC IN xpt
		REPLACE xpt.ship_via WITH cShip_via IN xpt
		SELECT x856
		SKIP
		LOOP
	ENDIF

	IF TRIM(x856.segment) = "LX" && start of PT detail, stay here until this PT is complete
*		ASSERT .F. MESSAGE "At initial LX Segment...debug"
		SELECT xptdet
		CALCULATE MAX(ptdetid) TO m.ptdetid
		m.ptdetid = m.ptdetid + 1
		nCtnCount = 0
		APPEND BLANK
		cPrintStuff = ""
		REPLACE xptdet.ptid      WITH ptctr IN xptdet
		REPLACE xptdet.ptdetid   WITH m.ptdetid IN xptdet
		REPLACE xptdet.accountid WITH xpt.accountid IN xptdet
		SCATTER MEMVAR FIELDS ptid,accountid

		SELECT xptdet
		SKIP 1 IN x856

		DO WHILE ALLTRIM(x856.segment) != "SE"
			cSegment = TRIM(x856.segment)
			cCode = TRIM(x856.f1)
			IF TRIM(x856.segment) = "W01" && Destination Quantity
				lG69 = .T.
				lOldStyle = .T.
				IF lPrepack
					nDetCnt = 0
					lDoInsert = .F.
					m.color = ""
				ENDIF
				m.itemnum = ""
				SELECT xptdet
				STORE ALLTRIM(x856.f1) TO ctotqty
				m.totqty = INT(VAL(ctotqty))

				IF EMPTY(xptdet.printstuff)
					REPLACE xptdet.printstuff WITH "UNITSTYPE*"+ALLTRIM(x856.f2)
				ELSE
					IF !("UNITSTYPE*"$xptdet.printstuff)
						REPLACE xptdet.printstuff WITH xptdet.printstuff+CHR(13)+"UNITSTYPE*"+ALLTRIM(x856.f2)
					ENDIF
				ENDIF
				STORE 1 TO nCasePack
				STORE ALLTRIM(x856.f5) TO m.custsku
				REPLACE xptdet.custsku WITH m.custsku IN xptdet

				m.upc = ""
				IF ALLTRIM(x856.f4) = "UP"
					STORE ALLTRIM(x856.f5) TO m.upc
					REPLACE xptdet.upc WITH m.upc IN xptdet
				ENDIF

				IF ALLTRIM(x856.f6) = "VN"
					STORE ALLTRIM(x856.f7) TO m.style
					REPLACE xptdet.STYLE WITH m.style IN xptdet
				ENDIF
				SELECT x856
				SKIP 1
				LOOP
			ENDIF
*				ASSERT .F. MESSAGE "At STYLE/COLOR...debug"

			IF TRIM(x856.segment) = "G69" AND lG69
				lG69 = .F.
				cDesc = IIF(EMPTY(cDesc),ALLTRIM(x856.f1),cDesc+" "+ALLTRIM(x856.f1))
				SELECT x856
				SKIP 3
				LOOP
			ENDIF

			IF TRIM(x856.segment) = "N9" AND TRIM(x856.f1) = "VC"
				cColor = ALLTRIM(x856.f2)
				SELECT x856
				SKIP 1
				LOOP
			ENDIF

			IF TRIM(x856.segment) = "N9" AND TRIM(x856.f1) = "SZ"
				cID = ALLTRIM(x856.f2)
				lDoInsert = .T.
				SELECT x856
				SKIP 1
				LOOP
			ENDIF

			IF lDoInsert
				lDoInsert = .F.
				IF nDetCnt = 0
					nDetCnt = 1
					REPLACE xptdet.units WITH .T. IN xptdet
					REPLACE xptdet.PACK WITH "1" IN xptdet
					REPLACE xptdet.casepack WITH 1 IN xptdet
					REPLACE xptdet.totqty WITH m.totqty IN xptdet
					REPLACE xptdet.origqty WITH xptdet.totqty IN xptdet
					REPLACE xptdet.COLOR WITH cColor IN xptdet
					REPLACE xptdet.ID WITH cID IN xptdet
					REPLACE xptdet.printstuff WITH xptdet.printstuff+CHR(13)+"DESC*"+cDesc IN xptdet
					cDesc = ""

					nCtnCount = nCtnCount + m.totqty
					SELECT xptdet
					GATHER MEMVAR FIELDS ptid

					IF !("COLORDESC*"$xptdet.printstuff)
						REPLACE xptdet.printstuff WITH ;
							xptdet.printstuff+CHR(13)+"COLORDESC*"+cColor IN xptdet
					ENDIF

					SELECT xptdet
				ELSE
					locstr = "xptdet.STYLE = m.style and xptdet.upc = m.upc ;
							and xptdet.custsku = m.custsku AND xptdet.ptid = m.ptid"

					LOCATE FOR &locstr
					IF FOUND()
						REPLACE xptdet.ptid WITH m.ptid IN xptdet
						REPLACE xptdet.accountid WITH m.accountid IN xptdet
						REPLACE xptdet.units WITH .T. IN xptdet
						REPLACE xptdet.PACK WITH "1" IN xptdet
						REPLACE xptdet.casepack WITH 1 IN xptdet
						REPLACE xptdet.totqty  WITH (xptdet.totqty + m.totqty) IN xptdet
						REPLACE xptdet.origqty WITH xptdet.totqty IN xptdet
						REPLACE xptdet.COLOR WITH cColor IN xptdet
						REPLACE xptdet.ID WITH cID IN xptdet
						REPLACE xptdet.printstuff WITH xptdet.printstuff+CHR(13)+"DESC*"+cDesc IN xptdet
						SELECT xptdet
					ELSE
						APPEND BLANK
						m.ptdetid = m.ptdetid + 1
						GATHER MEMVAR FIELDS ptid,accountid,STYLE,ptdetid,casepack
						GATHER MEMVAR FIELDS COLOR,PACK,ID,upc,custsku,shipstyle

						IF EMPTY(xptdet.printstuff)
							REPLACE xptdet.printstuff WITH "UNITSTYPE*"+ALLTRIM(x856.f2)
						ELSE
							IF !("UNITSTYPE*"$xptdet.printstuff)
								REPLACE xptdet.printstuff WITH xptdet.printstuff+CHR(13)+"UNITSTYPE*"+ALLTRIM(x856.f2)
							ENDIF
						ENDIF
						REPLACE xptdet.printstuff WITH xptdet.printstuff+CHR(13)+"DESC*"+cDesc IN xptdet

						REPLACE xptdet.ptdetid WITH m.ptdetid

						SCATTER FIELDS ptid,ptdetid,accountid MEMVAR
						IF lPick
							REPLACE xptdet.units WITH .T.
							REPLACE xptdet.casepack   WITH 1
							REPLACE xptdet.totqty WITH m.totqty
							REPLACE xptdet.origqty WITH xptdet.totqty
							nCtnCount = nCtnCount + m.totqty
						ENDIF
					ENDIF
					SELECT xptdet
				ENDIF
				SELECT x856
				SKIP 1 IN x856
				LOOP
			ENDIF

			IF ALLTRIM(x856.segment)="LX" && start of new PT detail, use same header
				SELECT xptdet
				APPEND BLANK
				nDetCnt = 0
				m.ptdetid = m.ptdetid+1
				REPLACE xptdet.ptid      WITH ptctr
				REPLACE xptdet.ptdetid   WITH m.ptdetid
				REPLACE xptdet.accountid WITH xpt.accountid
				SKIP 1 IN x856
				LOOP
			ENDIF

			IF TRIM(x856.segment) = "W76"  && Total carton quantity
*				SET STEP ON
				nCtnCount = INT(VAL(ALLTRIM(x856.f1)))
				REPLACE xpt.qty WITH nCtnCount IN xpt
				REPLACE xpt.origqty WITH xpt.qty IN xpt
				IF lPick
					nCtnCount = 0
				ENDIF
				SELECT xptdet
			ENDIF

			SELECT xptdet
			SKIP 1 IN x856
		ENDDO
		SELECT xptdet
	ENDIF

	SELECT x856
	IF !EOF()
		SKIP 1 IN x856
	ENDIF
ENDDO

*************** all the PTs are loaded into Xpt and Xptdet
***
*!* Added this block to create dupe-detail error file and mail
****************************************************************
SELECT xpt

SELECT ptid,upc,STYLE,COUNT(upc) AS cnt1 ;
	FROM xptdet ;
	GROUP BY ptid,upc ;
	INTO CURSOR tempdet1 ;
	HAVING cnt1 > 1
LOCATE
IF !EOF()
	SELECT a.ship_ref AS pickticket,b.STYLE,b.upc,b.cnt1 AS linecount ;
		FROM xpt a,tempdet1 b ;
		WHERE a.ptid = b.ptid ;
		INTO CURSOR tempdupes
	LOCATE
	cExcelDupes = "c:\tempfox\dupedetail.xls"
	COPY TO [&cExcelDupes] TYPE XL5
	USE IN tempdet1
	USE IN tempdupes

	IF !lTesting
		SELECT 0
		USE F:\3pl\DATA\mailmaster ALIAS mm
		LOCATE FOR GROUP = "MARCJACOBS" AND taskname = 'MJPLERROR'
		STORE TRIM(IIF(mm.use_alt,mm.sendtoalt,mm.sendto)) TO tsendto
		STORE TRIM(IIF(mm.use_alt,mm.ccalt,mm.cc)) TO tcc
		USE IN mm
	ELSE
		tsendto = tsendtoerr
		tcc = tccerr
	ENDIF

	tsubject = "Error: Marc Jacobs Duplicate Detail Lines"
	tmessage = "Marc Jacobs (W) 940 Upload Error!"
	tmessage = tmessage+CHR(13)+"The attached Excel file contains duplicate line items by PT"
	tmessage = tmessage+CHR(13)+"From file "+cFilename
	tattach  = cExcelDupes
	tfrom    ="Toll EDI Processing Center <toll-edi-ops@tollgroup.com>"
	DO FORM m:\dev\frm\dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
	lDoImport = .F.
ENDIF
*!* End added code block

IF lDoImport
	SELECT xptdet
	rollup()
	DO m:\dev\prg\setuppercase940

	IF lBrowfiles
		SELECT xpt
		LOCATE
		BROWSE
		SELECT xptdet
		BROWSE
	ENDIF

	WAIT cCustname+" Breakdown Round complete..." WINDOW TIMEOUT WAITTIME

	SELECT x856
	IF USED("PT")
		USE IN pt
	ENDIF
	IF USED("PTDET")
		USE IN ptdet
	ENDIF
	WAIT CLEAR
	lDoImport = .T.
ENDIF

******************************
PROCEDURE rollup
******************************
	ASSERT .F. MESSAGE "In rollup procedure"
	nChangedRecs = 0

	SELECT xptdet
	SET DELETED ON
	LOCATE
	IF lTesting
		WAIT WINDOW "Browsing XPTDET pre-rollup" TIMEOUT 2
		BROW
	ENDIF
	cMsgStr = "PT/OSID/ODIDs changed: "
	SCAN FOR !DELETED()
		SCATTER MEMVAR
		nRecno = RECNO()
		LOCATE FOR xptdet.ptid = m.ptid ;
			AND xptdet.STYLE = m.style ;
			AND xptdet.COLOR = m.color ;
			AND xptdet.ID = m.id ;
			AND xptdet.PACK = m.pack ;
			AND xptdet.upc = m.upc ;
			AND RECNO('xptdet') > nRecno
		IF FOUND()
			SELECT xpt
			LOCATE FOR xpt.ptid = m.ptid
			IF !FOUND()
				WAIT WINDOW "PROBLEM!"
			ENDIF
			cShip_ref = ALLT(xpt.ship_ref)
			IF nChangedRecs > 10
				cMsgStr = "More than 10 records rolled up"
			ELSE
				cMsgStr = cMsgStr+CHR(13)+cShip_ref+" "+TRANSFORM(m.ptid)+" "+TRANSFORM(m.ptdetid)
			ENDIF
			nChangedRecs = nChangedRecs+1
			SELECT xptdet
			nTotqty1 = xptdet.totqty
			DELETE NEXT 1 IN xptdet
		ENDIF
		IF !EOF()
			GO nRecno
			REPLACE xptdet.totqty WITH xptdet.totqty+nTotqty1 IN xptdet
			REPLACE xptdet.origqty WITH xptdet.totqty IN xptdet
		ELSE
			GO nRecno
		ENDIF
	ENDSCAN

	IF nChangedRecs > 0
		WAIT WINDOW cMsgStr TIMEOUT 2
		WAIT WINDOW "There were "+TRANS(nChangedRecs)+" records rolled up" TIMEOUT 1
	ELSE
		WAIT WINDOW "There were NO records rolled up" TIMEOUT 1
	ENDIF

ENDPROC

