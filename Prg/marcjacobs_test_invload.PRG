test(.t.,.t.)


Public coffice, ltesting, ltestimport,gMasterOffice,nAcctNum,gOffice,cMod

ltesting = .t.
ltestimport =.t.
nAcctNum = 6303
gMasterOffice = "N"
gOffice = "J"
cMod ="J"

fileDir = 'M:\doug\MarcJacobs\'
archDir = fileDir + '\Archive\'
ackDir = fileDir

CLOSE DATABASES ALL

CREATE CURSOR invData;
	(RecordID C(2),;
	ExtWarehouseNum C(3),;
	FulfillmentLoc C(5),;
	Upc C(14),;
	Qty N(7,0),;
	statusFlag C(1),;
	InvoiceDate N(14,0))

LOCAL inFileHandle

fileCnt = ADIR(fileList, fileDir+"ESSINV*.dat")
IF fileCnt = 0
	WAIT WINDOW "No files found... exiting" TIMEOUT 4
	ON ERROR
	RETURN
ENDIF

FOR i = 1 TO fileCnt
	thisfile = ALLTRIM(fileList[i,1])
	infile   = fileDir + thisfile
	
	ZAP IN invData
	
	SELECT invData
	APPEND FROM (infile)  DELIMITED WITH CHARACTER |
ENDFOR 

xsqlexec("select * from upcmast where accountid = 6303",,,"wh")
useca("inven","wh",,,"select * from inven where accountid = 6303")

SELECT invData.*, accountid, style, color, id FROM invData LEFT JOIN upcmast ON invData.upc = upcmast.upc ;
	WHERE accountid = 6303 INTO CURSOR invStyles
	
SELECT a.style, a.color, a.id, a.qty, b.availqty FROM invStyles a ;
	LEFT JOIN inven b ON a.accountid = b.accountid and ;
	                      a.style = b.style and ;
	                      a.color = b.color and ;
	                      a.id = b.id ;
	INTO CURSOR d1
	
dCount = 1
WAIT WINDOW "Processing data..." TIMEOUT 1
SELECT d1
totcnt = RECCOUNT()
SCAN
	IF (dCount % 50) = 0 
		WAIT WINDOW "Processing record " + TRANSFORM(dCount) + " of " + TRANSFORM(totcnt) NOWAIT
	endif
	IF  ISNULL(d1.availQty)
		SELECT inven
		LOCATE 
		SCATTER MEMVAR fields EXCEPT invenid
		m.style = d1.style
		m.color = d1.color
		m.id = d1.id
		m.addby = 'DWACHS'
		m.availQty = 50
		insertinto("inven","wh",.t.)
	ELSE 
		REPLACE availqty WITH 50 FOR style = d1.style AND color = d1.color AND id = d1.id IN inven
	ENDIF
	dCount = dCount + 1	
ENDSCAN  

tu('inven')
WAIT WINDOW "Finished" NOWAIT 
USE IN inven
USE IN upcmast

