**Order Cycle Time KPI

**Time from the lastest of start date & order drop date till the freight was staged
**  - Based on max(outShip.start-3bus. days, outShip.ptDate, outship.wo_date) vs. outShip.staged
**  - Keep track of how many of the staged were automatically entered by the system (outShip.manstage)
**  
**  - No lead time: wos that have a greater ptDate than start-3 (3 bus days)
**  -   Also, do not need to compare wo_date in for the acceptedDt, just max(start-3, ptDate)

**define gofffice - added mvw 03/20/17
**xoffice=mod
goffice=xoffice

Wait window "Gathering data for "+xrptname+" Spreadsheet..." nowait noclear

lLeadTime=iif(lower(xrptname)="oct - no lead time",.f.,.t.)

cAcctFilter = Iif(Empty(cWhseAcct), "", iif(lower(cwhseacct)="inlist",strtran(cwhseacct,"inlist(","inlist(o."),"o."+cWhseAcct))

**new order cycle time rpt for BUGABOO for consumer orders (outship.b2="C"), xcustorders defined in in kpi form - mvw 12/02/11
**need to include "B" as well for this bugaboo consumer report - mvw 01/21/13
**added a new filter for Synclaire (".COM"$consignee) - mvw 10/30/14
**added steel series, its either web or non-web - mvw 09/11/15
**added 2XU, its either web or non-web - mvw 04/28/17
**added Merkury, its either web or non-web - mvw 03/23/18

**cannot be in outship because outship contains a field cacctnum which is same as variable used below - mvw 11/30/15
select 0

xb2filter=" and .t."
xordertype="" &&for use in title below
do case
case xcustorders and inlist(Val(cacctnum),5102,6120) &&bugaboo (5102,6120)
	xb2filter=" and inlist(o.b2,'C','B')"
case xcustorders and inlist(Val(cacctnum),6521) &&synclaire (6521)
	xb2filter=" and '.COM'$consignee"
case inlist(Val(cacctnum),6612) &&steelseries (6612)
	xb2filter=iif(xcustorders," and inlist(batch_num,'WEB','RMA','SAMPLE')"," and !inlist(batch_num,'WEB','RMA','SAMPLE')")
	xordertype=iif(xcustorders," (Web Orders) "," (Non-Web Orders) ")
case inlist(Val(cacctnum),6665) &&2XU (6665)
	xb2filter=" and "+iif(xcustorders,"","!")+"inlist(cacctnum,'52975000','52862000','51257000','50029999','51696000','51004000','51003000','54012000','52145000','WEBPERF')"
	xordertype=iif(xcustorders," (Web Orders) "," (Non-Web Orders) ")
case inlist(Val(cacctnum),6561) &&merkury
	**if PT starts with H (Home Depot), D (Groupon) or G (Gilt), they are web orders
	xb2filter=" and "+iif(xcustorders,"","!")+"inlist(ship_ref,'H','D','G')"
	xordertype=iif(xcustorders," (Web Orders) "," (Non-Web Orders) ")
endcase

xdatefilter="Between(staged, dStartDt, dEndDt) and !emptynul(staged)"
xsqldatefilter="between(o.staged,{"+transform(dStartDt)+"},{"+transform(dEndDt)+"}) and not o.staged={}"
xdate1="acceptedDt"
xdate2="staged"
xdesc="Staged Date Vs. Allocation Date"
if inlist(lower(xrptname),"oct - allocated to shipped","oct - staged to shipped")
	xdatefilter="Between(del_date, dStartDt, dEndDt) and !emptynul(del_date)"
	xsqldatefilter="between(o.del_date,{"+transform(dStartDt)+"},{"+transform(dEndDt)+"}) and not o.del_date={}"
	xdate1=iif(lower(xrptname)="oct - staged to shipped","staged",xdate1)
	xdate2="del_date"
	xdesc=iif(lower(xrptname)="oct - allocated to shipped","Shipped Date Vs. Allocation Date","Shipped Date Vs. Staged Date")
endif

xquery="select o.* from outship o left join outwolog w on w.outwologid=o.outwologid where "+cAcctFilter+xsqldatefilter+" and (NOT o.ptdate={} or NOT o.start={}) and o.sp=0 and w.kpiexcept=0 and o.mod = '"+goffice+"'"
xsqlexec(xquery,"csrdetail",,"wh",,,,,,,.t.)

**remove the leading " and" since it is the only filter at that point
xb2filter=right(xb2filter,len(xb2filter)-4)
Select wo_num, wo_date, o.accountId as acctNumFld, a.acctName as acctNameFld, &xdate2 as rangeDt, ship_ref, manStage, consignee, ;
		start, start as oldstart, cancel, ptDate, ptDate as acceptedDt, staged, del_date, qty as unitqty, ctnqty, 00 as dateDiff, batch_num as ordertype, apptremarks ;
	from csrdetail o left join account a on a.accountId = o.accountId ;
	where &xb2filter ;
	order by a.acctName, &xdate2, wo_num ;
  into cursor csrDetail readWrite

**NOTE: if you are updating entire cursor in kpichgdate, must be at the top of the cursor as it does a do while !eof()
=kpichgdate("start", -3, .t.) &&change start date to start - 3 bus. days

**added for staged to shipped, if empty staged set staged to del_date - mvw 10/09/12
replace staged with del_date for emptynul(staged)

If lLeadTime
	**change to "Allocation Date" per seteve sykes - mvw 10/08/12
*!*			cComment = " - P/T Acceptance Date is the latest of the w/o creation date, "+;
*!*				"order drop date & start date less 3 bus. days."
	cComment = " - Allocation Date is the latest of the w/o creation date, "+;
		"order drop date & start date less 3 bus. days."

	**acceptedDt is the latest of ptDate, wo_date and (start - 3 bus. days)
	**for steel series (6612), ignore wo_date... later of ptdate and start - 3 bus days - mvw 09/11/15
	**if xpttoship=.t. (defined in kpi form for some accounts), ignore wo_date... later of ptdate and start - 3 bus days - mvw 05/16/16
	if val(cacctnum)=6612 or (type("xpttoship")="L" and xpttoship=.t.)
		cComment = " - Allocation Date is the latest of the order drop date & start date less 3 bus. days."
		replace all acceptedDt with Iif(ptdate>start,ptdate,start)
	else
		replace all acceptedDt with Iif(ptDate>start, Iif(ptDate>wo_date, ptDate, wo_date), Iif(start>wo_date, start, wo_date))

		**for use if only want to use PT Date
*		replace all acceptedDt with ptDate
	endif
	Locate
Else
	cComment = " - No Lead Time: Wos. with an order drop date later than the start date less 3 bus. days."
	Select * from csrDetail where ptDate > start into cursor csrDetail readWrite
EndIf

**calculate date differential into dateDiff - make necessary adjustments for weekends & FMI holidays
DO kpicalcdiff with xdate1, xdate2, "dateDiff"

Locate
If Eof()
	strMsg = "No data found."
	**if sending email in automated run, do not show messagebox - mvw 01/06/14
	if empty(cemaillist)
		=MessageBox(strMsg, 48, xrptname)
	endif
Else
	Wait window "Creating "+xrptname+" Spreadsheet..." nowait noclear

	oWorkbook=""
	cTitle="Order Cycle Time"+right(xrptname,len(xrptname)-3)+xordertype
	xcommentcol="'Manually entered staged dates: '+Alltrim(Str(totManStage))+'/'+Alltrim(Str(totCnt))+' = '+Alltrim(Str((totManStage/totCnt)*100,5,1))+'%'"
	xcommentcol=iif(lower(xrptname)="oct - allocated to shipped","''",xcommentcol)
	DO xlsTimeGeneric with "", "rangeDt", iif(xunits,"unitqty","ctnQty"), "sum(Iif(manStage, "+iif(xunits,"unitqty","ctnQty")+", 0)) as totManStage", cTitle, ;
		xdesc, cComment, " - Totals are calculated in "+iif(xunits,"units","cartons")+".", ;
		xcommentcol, .t., .t.

	If lDetail
		Select csrTotals
		nWorksheet = 2
		Scan
			store tempfox+substr(sys(2015), 3, 10)+".xls" to ctmpfile

			select *, iif(manstage, "Yes", "No") as manual from csrdetail ;
				where year(rangedt) = csrtotals.year and month(rangedt) = csrtotals.month into cursor csrdet

			do case
			case cacctnum="6532"
				select * from csrdet order by ordertype, acctnamefld, &xdate2, wo_num into cursor csrdet
				copy to &ctmpfile fields acctnamefld, wo_num, wo_date, ship_ref, consignee, unitqty, ctnqty, accepteddt, cancel, staged, del_date, datediff, manual, ordertype type xl5
				DO xlsTimeGenDetail with cTmpFile, nWorkSheet, "N"
			case cacctnum="6665"
				select *, padr(iif("-"$apptremarks,alltrim(right(apptremarks,len(apptremarks)-at("-",apptremarks))),""),35) as remarks from csrdet into cursor csrdet
				copy to &ctmpfile fields acctnamefld, wo_num, wo_date, ship_ref, consignee, unitqty, ctnqty, accepteddt, cancel, staged, del_date, datediff, manual, remarks type xl5
				DO xlsTimeGenDetail with cTmpFile, nWorkSheet, "N"
			otherwise
				copy to &ctmpfile fields acctnamefld, wo_num, wo_date, ship_ref, consignee, unitqty, ctnqty, accepteddt, cancel, staged, del_date, datediff, manual type xl5
				DO xlsTimeGenDetail with cTmpFile, nWorkSheet, "M"
			endcase

			oWorkbook.worksheets[nWorksheet].Range("A"+Alltrim(Str(Reccount("csrDet")+5))).font.color = Rgb(255,0,0) &&red
			oWorkbook.worksheets[nWorksheet].Range("A"+Alltrim(Str(Reccount("csrDet")+5))).Value = ;
				"Note: 'Manual' column indicates a manually entered stage date."

			nWorksheet = nWorksheet+1
		EndScan
		use in csrdet
	EndIf

	oWorkbook.Worksheets[1].range("A1").activate()
	oWorkbook.Save()
EndIf

if used('outship')
	use in outship
endif
if used('outwolog')
	use in outwolog
endif

use in csrdetail
if used("csrtotals")
  use in csrtotals
endif
