* KRONOSREPORTS.PRG

* process various reports from Kronos, based on passed parameter

* Build EXE as F:\UTIL\KRONOS\KRONOSREPORTS.EXE

LPARAMETERS tcReportType

LOCAL loKRONOSREPORTS, lcReportType


IF EMPTY(tcReportType) THEN
	lcReportType = "?"
ELSE
	lcReportType = UPPER(ALLTRIM(tcReportType))
ENDIF

_SCREEN.CAPTION = "KRONOSREPORTS"

*!*	runack("KRONOSREPORTS")

*!*	utilsetup("KRONOSREPORTS")

loKRONOSREPORTS = CREATEOBJECT('KRONOSREPORTS')

loKRONOSREPORTS.MAIN( lcReportType )

*!*	schedupdate()

CLOSE DATABASES ALL
RETURN


#DEFINE LOGIT 1
#DEFINE WAITIT 2
#DEFINE NOWAITIT 4
#DEFINE SENDIT 8
#DEFINE CRLF CHR(13) + CHR(10)
#DEFINE RETURN_DATA_MANDATORY .F.
#DEFINE RETURN_DATA_NOT_MANDATORY .T.
#DEFINE CSVFORMAT 6
#DEFINE WRITE_ONLY_UNBUFFERED 11
#DEFINE COMMA ","
#DEFINE xlEdgeBottom 9
#DEFINE xlContinuous 1
#DEFINE xlMedium -4138
#DEFINE xlFONT_RED -16776961

DEFINE CLASS KRONOSREPORTS AS CUSTOM

	cProcessName = 'KRONOSREPORTS'

	lTestMode = .T.  && .F. FOR NORMAL OPERATION, .T. TO SUPPRESS ALL BUT DEVELOPER EMAILS AND CHANGE INPUT TABLE, ETC.

	dToday = DATE()

	lAutoYield = .T.

	cStartTime = TTOC(DATETIME())

	* processing properties
	cReportType = ''

	* connection properties
	nSQLHandle = 0

	* Excel properties
	oExcel = NULL

	* wait window properties
	nWaitWindowTimeout = 2
	lWaitWindowIsOn = .T.

	* logfile properties
	lLoggingIsOn = .T.
	cLogFile = 'F:\UTIL\KRONOS\LOGFILES\KRONOSREPORTS_log.txt'

	* INTERNAL email properties
	lSendInternalEmailIsOn = .T.
	cFrom = 'Mark.Bennett@Tollgroup.com'
	cSendTo = 'mbennett@fmiint.com'
	cCC = ''
	cSubject = ''
	cAttach = ''
	cBodyText = ''

	cCOMPUTERNAME = ''
	cUSERNAME = ''


	FUNCTION INIT
		IF NOT DODEFAULT()
			RETURN .F.
		ENDIF
		WITH THIS
			*SET RESOURCE OFF
			CLOSE DATA
			SET DECIMALS TO 4
			SET CENTURY ON
			SET DATE AMERICAN
			SET HOURS TO 24
			SET ANSI ON
			SET TALK OFF
			SET DELETED ON
			SET CONSOLE OFF
			SET EXCLUSIVE OFF
			SET SAFETY OFF
			SET EXACT OFF
			SET STATUS BAR ON
			SET SYSMENU OFF
			SET ENGINEBEHAVIOR 70
			_VFP.AUTOYIELD = .lAutoYield
			.cCOMPUTERNAME = GETENV("COMPUTERNAME")
			.cUSERNAME = GETENV("USERNAME")
			IF .lTestMode THEN
				.cLogFile = 'F:\UTIL\KRONOS\LOGFILES\KRONOSREPORTS_log_TESTMODE.txt'
				.cSendTo = 'mbennett@fmiint.com'
				.cCC = ''
			ENDIF
			.lLoggingIsOn = .lLoggingIsOn AND NOT EMPTY(.cLogFile)
			IF .lLoggingIsOn THEN
				SET ALTERNATE TO (.cLogFile) ADDITIVE
				SET ALTERNATE ON
			ENDIF
			SET PROCEDURE TO VALIDATIONS ADDITIVE
		ENDWITH
	ENDFUNC


	FUNCTION DESTROY
		WITH THIS
			IF .lLoggingIsOn  THEN
				SET ALTERNATE OFF
				SET ALTERNATE TO
			ENDIF
		ENDWITH
		IF TYPE('.oExcel') = 'O' AND NOT ISNULL(.oExcel) THEN
			.oExcel.QUIT()
		ENDIF
		DODEFAULT()
	ENDFUNC


	FUNCTION MAIN
		LPARAMETERS tcReportType
		WITH THIS
			LOCAL lnNumberOfErrors, lcSQL, lcSQL2, loError, lcDateSuffix
			LOCAL lcFiletoSaveAs, lcSpreadsheetTemplate
			LOCAL i, oExcel, oWorkbook, oWorksheet, lnStartRow, lcStartRow, lnRow, lcRow

			TRY
				lnNumberOfErrors = 0

				.TrackProgress("KRONOS REPORTS process started....", LOGIT+SENDIT+NOWAITIT)
				.TrackProgress('COMPUTERNAME = ' + .cCOMPUTERNAME, LOGIT+SENDIT)
				.TrackProgress('USERNAME = ' + .cUSERNAME, LOGIT+SENDIT)
				.TrackProgress('PROJECT = KRONOSREPORTS', LOGIT+SENDIT)
				.TrackProgress('REPORT TYPE = ' + tcReportType, LOGIT+SENDIT)

				IF .lTestMode THEN
					.TrackProgress('                        TEST MODE                        ', LOGIT+SENDIT)
				ENDIF

				lcDateSuffix = DTOC(DATE())
				.cSubject = 'KRONOS Report: ' + tcReportType + ' for ' + lcDateSuffix

				IF NOT INLIST(tcReportType,"TEMPINFOFORKEN") THEN
					.TrackProgress('ERROR: unexpected Report Type!', LOGIT+SENDIT)
					THROW
				ENDIF

				.cReportType = tcReportType

				* OPEN Excel
				.oExcel = CREATEOBJECT("excel.application")
				.oExcel.displayalerts = .F.
				.oExcel.VISIBLE = .F.

				* connect to Kronos Server
				.nSQLHandle = SQLCONNECT("WTK2","sa","kr0n1tes")

				IF .nSQLHandle > 0 THEN
					.TrackProgress('Connected to WTK System.', LOGIT+SENDIT)
				ELSE
					* connection error
					.TrackProgress('===> Unable to connect to WTK System!', LOGIT+SENDIT)
				ENDIF   &&  .nSQLHandle > 0

				DO CASE
					CASE .cReportType == "TEMPINFOFORKEN"
						.TempInfoForKen()
				ENDCASE

				.TrackProgress("KRONOS REPORTS process ended normally.", LOGIT+SENDIT+NOWAITIT)
				
				IF TYPE('.oExcel') = 'O' AND NOT ISNULL(.oExcel) THEN
					.oExcel.QUIT()
					.oExcel = NULL
				ENDIF

			CATCH TO loError

				.TrackProgress('There was an error.',LOGIT+SENDIT)
				.TrackProgress('The Error # is: ' + TRANSFORM(loError.ERRORNO), LOGIT+SENDIT)
				.TrackProgress('The Error Message is: ' + TRANSFORM(loError.MESSAGE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred in Program: ' + TRANSFORM(loError.PROCEDURE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred at line #: ' + TRANSFORM(loError.LINENO), LOGIT+SENDIT)
				lnNumberOfErrors = lnNumberOfErrors + 1

				IF TYPE('.oExcel') = 'O' AND NOT ISNULL(.oExcel) THEN
					.oExcel.QUIT()
					.oExcel = NULL
				ENDIF

			ENDTRY

			WAIT CLEAR
			***************** INTERNAL email results ******************************


			.TrackProgress('About to send status email.',LOGIT)
			.TrackProgress('The logfile is: ' + .cLogFile, SENDIT)
			.TrackProgress("KRONOS REPORTS process started: " + .cStartTime, LOGIT+SENDIT)
			.TrackProgress("KRONOS REPORTS process finished: " + TTOC(DATETIME()), LOGIT+SENDIT)

			IF .lSendInternalEmailIsOn THEN
				DO FORM dartmail2 WITH .cSendTo,.cFrom,.cSubject,.cCC,.cAttach,.cBodyText,"A"
				.TrackProgress('Sent status email.',LOGIT)
			ELSE
				.TrackProgress('Did not send status email: THIS.lSendInternalEmailIsOn = FALSE.',LOGIT)
			ENDIF

		ENDWITH
		RETURN
	ENDFUNC && main


	PROCEDURE TempInfoForKen
		WITH THIS
			LOCAL lcFiletoSaveAs, ldToday, lcSQL, lcSQL2
			LOCAL lcStartDate, lcEndDate, ldStartDate, ldEndDate, ldTomorrow, lcSQLEndDatePlus1, lcSQLToday, lcSQLTomorrow

			* these dates bound the PAY RATE HISTORY
			ldStartDate = {^2016-01-01}
			ldEndDate = DATE()

			ldToday = DATE()
			ldTomorrow = ldToday + 1

			lcFiletoSaveAs = 'F:\UTIL\KRONOS\REPORTS\TempInfoForKen_' + DTOS(ldToday) + '.XLS'

			IF USED('CURWAGESPRE') THEN
				USE IN CURWAGESPRE
			ENDIF
			IF USED('CURWAGES') THEN
				USE IN CURWAGES
			ENDIF
			IF USED('CURTEMPS') THEN
				USE IN CURTEMPS
			ENDIF
			IF USED('CURFINAL') THEN
				USE IN CURFINAL
			ENDIF

			SET DATE AMERICAN

			lcStartDate = STRTRAN(DTOC(ldStartDate),"/","-")
			lcEndDate = STRTRAN(DTOC(ldEndDate),"/","-")

			SET DATE YMD

			*	 create "YYYYMMDD" safe date format for use in SQL query
			* NOTE: for SQL we are going to use < EndDate ( not <= ), so we are going to add 1 to that date here for querying purposes.
			lcSQLStartDate = STRTRAN(DTOC(ldStartDate),"/","")
			lcSQLEndDate = STRTRAN(DTOC(ldEndDate),"/","")
			lcSQLEndDatePlus1 = STRTRAN(DTOC(ldEndDate + 1),"/","")
			lcSQLToday = STRTRAN(DTOC(ldToday),"/","")
			lcSQLTomorrow = STRTRAN(DTOC(ldTomorrow),"/","")

			SET DATE AMERICAN


			* wage history SQL
			SET TEXTMERGE ON
			TEXT TO	lcSQL2 NOSHOW

SELECT
U.USERACCOUNTNM AS TIMERVWR,
P.FULLNM AS EMPLOYEE,P.PERSONNUM AS FILE_NUM,L.LABORLEV5DSC AS AGENCY,L.LABORLEV1NM AS ADP_COMP,
L.LABORLEV2NM AS DIVISION, L.LABORLEV3NM AS DEPT,
C.EXPIRATIONDTM, C.EFFECTIVEDTM,E.EMPLOYEEID,
E.BASEWAGEHOURLYAMT AS WAGE, E.EFFECTIVEDTM AS UPDATEDT
FROM BASEWAGERTHIST E
JOIN WTKEMPLOYEE B ON B.EMPLOYEEID = E.EMPLOYEEID
JOIN PERSON P ON P.PERSONID = B.PERSONID
JOIN COMBHOMEACCT C ON C.EMPLOYEEID = E.EMPLOYEEID
JOIN LABORACCT L ON L.LABORACCTID = C.LABORACCTID
JOIN USERACCOUNT U ON U.USERACCOUNTID = E.UPDATEDBYUSRACCTID
WHERE (L.LABORLEV5NM IN ('R','U'))
AND (C.EXPIRATIONDTM >= '<<lcSQLStartDate>>')
AND (C.EFFECTIVEDTM < '<<lcSQLEndDatePlus1>>')
AND (E.UPDATEDTM >= '<<lcSQLStartDate>>')
AND (E.UPDATEDTM < '<<lcSQLEndDatePlus1>>')
AND (E.EFFECTIVEDTM >= '<<lcSQLStartDate>>')
ORDER BY P.FULLNM, P.PERSONNUM, E.VERSIONCNT

			ENDTEXT
			SET TEXTMERGE OFF


			*!*	L.LABORLEV2NM AS DIVISION, L.LABORLEV3NM AS DEPT, L.LABORLEV4NM AS WORKSITE,


			IF .lTestMode THEN
				.TrackProgress('', LOGIT+SENDIT)
				.TrackProgress('lcSQL2 =' + lcSQL2, LOGIT+SENDIT)
			ENDIF




			SET TEXTMERGE ON

			TEXT TO	lcSQL NOSHOW


SELECT
HOMELABORLEVELDSC5 AS OFFICE,
EMPLOYMENTSTATUS AS STATUS,
PERSONNUM AS FILE_NUM,
PERSONFULLNAME AS EMPLOYEE,
LASTNM,
FIRSTNM,
HOMELABORLEVELNM2 AS DIVISION,
HOMELABORLEVELNM3 AS DEPT,
HOMELABORLEVELDSC3 AS DEPTDESC,
COMPANYHIREDTM AS HIREDATE,
HOMELABORLEVELDSC7 AS TIMERVWR,
BASEWAGEHOURLYAMT AS PAYRATE
FROM VP_EMPLOYEEV42
WHERE HOMELABORLEVELNM1 = 'TMP'
AND HOMELABORLEVELNM5 IN ('R','U')
AND EMPLOYMENTSTATUS <> 'Terminated'
AND (PERSONNUM NOT IN (230000))
ORDER BY HOMELABORLEVELDSC5, EMPLOYMENTSTATUS, LASTNM, FIRSTNM


			ENDTEXT

			SET TEXTMERGE OFF

			*!*	AND LABORLEVELNAME1 = 'TMP'
			*!*

			IF .lTestMode THEN
				.TrackProgress('', LOGIT+SENDIT)
				.TrackProgress('lcSQL =' + lcSQL, LOGIT+SENDIT)
			ENDIF

			IF NOT .ExecSQL(lcSQL2, 'CURWAGESPRE', RETURN_DATA_MANDATORY) THEN

				.TrackProgress("===> ERROR: SQL query lcSQL2 returned no data!", LOGIT+SENDIT)
				THROW

			ENDIF  &&  .ExecSQL(lcSQL

			* ORDER WAGE HIST SO THAT WHEN WE LOCATE WE GET THE MOST RECENT UPDATEDT
			SELECT A.* ;
				FROM CURWAGESPRE A ;
				INTO CURSOR CURWAGES ;
				WHERE A.expirationdtm IN (SELECT MAX(expirationdtm) FROM CURWAGESPRE WHERE EMPLOYEE = A.EMPLOYEE AND FILE_NUM = A.FILE_NUM) ;
				AND EMPLOYEE IN (SELECT EMPLOYEE FROM CURWAGESPRE WHERE BETWEEN(UPDATEDT, ldStartDate, ldEndDate) ) ;
				ORDER BY EMPLOYEE, UPDATEDT DESCENDING ;
				READWRITE

			* TO SPEED UP LOCATES
			SELECT CURWAGES
			INDEX ON UPDATEDT TAG UPDATEDT DESCENDING
			INDEX ON EMPLOYEE TAG EMPLOYEE



			*!*				SELECT CURWAGES
			*!*				BROWSE


			IF .ExecSQL(lcSQL, 'CURTEMPS', RETURN_DATA_NOT_MANDATORY) THEN

				IF USED('CURTEMPS') AND NOT EOF('CURTEMPS') THEN

					*!*	SELECT CURTEMPS.*, 000.00 AS LASTRATE, {} AS WHEN ;
					*!*	FROM CURTEMPS ;
					*!*	INTO CURSOR CURFINAL ;
					*!*	ORDER BY OFFICE, STATUS, LASTNM, FIRSTNM ;
					*!*	READWRITE

					SELECT CURTEMPS.*, {} AS UPDT2016 ;
						FROM CURTEMPS ;
						INTO CURSOR CURFINAL ;
						ORDER BY OFFICE, STATUS, LASTNM, FIRSTNM ;
						READWRITE


					* Populate 2016 wage change, if any
					SELECT CURFINAL
					SCAN
						SELECT CURWAGES
						LOCATE FOR (EMPLOYEE == CURFINAL.EMPLOYEE) AND (FILE_NUM = CURFINAL.FILE_NUM)
						IF FOUND() THEN
							*REPLACE CURFINAL.LASTRATE WITH CURWAGES.WAGE, CURFINAL.WHEN WITH CURWAGES.UPDATEDT IN CURFINAL
							REPLACE CURFINAL.UPDT2016 WITH CURWAGES.UPDATEDT IN CURFINAL
						ELSE
							.TrackProgress("No wage change for: " + CURFINAL.EMPLOYEE, LOGIT+SENDIT)
						ENDIF
					ENDSCAN

					SELECT CURFINAL
					COPY TO (lcFiletoSaveAs) XL5

				ELSE
					.TrackProgress("SQL query lcSQL returned no data!", LOGIT+SENDIT)
				ENDIF

			ENDIF  &&  .ExecSQL(lcSQL


			IF FILE(lcFiletoSaveAs) THEN
				* attach output file to email
				.cAttach = lcFiletoSaveAs
				.TrackProgress('Report ' + .cReportType + ' finished normally', LOGIT+SENDIT+NOWAITIT)
			ELSE
				.TrackProgress('==> ERROR: unable to email spreadsheet: ' + lcFiletoSaveAs, LOGIT+SENDIT+NOWAITIT)
			ENDIF

			RETURN
		ENDWITH

	ENDPROC  &&  TempInfoForKen


	FUNCTION ExecSQL
		LPARAMETERS tcSQL, tcCursorName, tlNoDataReturnedIsOkay
		LOCAL llRetVal, lnResult
		WITH THIS
			* close target cursor if it's open
			IF USED(tcCursorName)
				USE IN (tcCursorName)
			ENDIF
			WAIT WINDOW "Fetching remote data; please wait..." NOWAIT
			lnResult = SQLEXEC(.nSQLHandle, tcSQL, tcCursorName)
			llRetVal = ( lnResult > 0 )
			IF llRetVal THEN
				* see if any data came back
				IF NOT tlNoDataReturnedIsOkay THEN
					IF (NOT USED(tcCursorName)) OR EOF(tcCursorName) THEN
						llRetVal = .F.
						.TrackProgress('No data was returned by this query: ' +CRLF + tcSQL, LOGIT+SENDIT)
					ENDIF
				ENDIF
			ELSE
				.TrackProgress("Result Code [" + TRANSFORM(lnResult) + "] returned by this query: " + tcSQL, LOGIT+SENDIT)
			ENDIF
			WAIT CLEAR
			RETURN llRetVal
		ENDWITH
	ENDFUNC


	PROCEDURE TrackProgress
		* do any combination of Wait Window, writing to logfile, and adding to body of email,
		* based on nFlags parameter.
		LPARAMETERS tcExpression, tnFlags
		WITH THIS
			IF BITAND(tnFlags,LOGIT) = LOGIT THEN
				IF .lLoggingIsOn THEN
					?
					? .cProcessName + "  " + TTOC(DATETIME()) + ": " + tcExpression
				ENDIF
			ENDIF
			IF .lWaitWindowIsOn THEN
				IF BITAND(tnFlags,NOWAITIT) = NOWAITIT THEN
					WAIT WINDOW tcExpression NOWAIT
				ENDIF
				IF BITAND(tnFlags,WAITIT) = WAITIT THEN
					WAIT WINDOW tcExpression TIMEOUT .nWaitWindowTimeout
				ENDIF
			ENDIF
			IF BITAND(tnFlags,SENDIT) = SENDIT THEN
				IF .lSendInternalEmailIsOn THEN
					.cBodyText = .cBodyText + tcExpression + CRLF + CRLF
				ENDIF
			ENDIF
		ENDWITH
	ENDPROC  &&  TrackProgress


	FUNCTION GetSQLStartDateTimeString
		* returns a timestamp string for use in SQL queries
		LPARAMETERS tdDate
		LOCAL lcSetDate, lcSQLDateTimeString
		lcSetDate = SET('DATE')
		SET DATE YMD
		lcSQLDateTimeString   = "{ts '" + TRANSFORM(YEAR(tdDate)) + "-" + PADL(MONTH(tdDate),2,"0") + "-" + PADL(DAY(tdDate),2,"0") + " 00:00:00.0'}"
		SET DATE &lcSetDate.
		RETURN lcSQLDateTimeString
	ENDFUNC


	FUNCTION GetSQLEndDateTimeString
		* returns a timestamp string for use in SQL queries
		LPARAMETERS tdDate
		LOCAL lcSetDate, lcSQLDateTimeString
		lcSetDate = SET('DATE')
		SET DATE YMD
		lcSQLDateTimeString = "{ts '" + TRANSFORM(YEAR(tdDate)) + "-" + PADL(MONTH(tdDate),2,"0") + "-" + PADL(DAY(tdDate),2,"0") + " 23:59:59.9'}"
		SET DATE &lcSetDate.
		RETURN lcSQLDateTimeString
	ENDFUNC


	PROCEDURE SortArrayByDateTime
		* expects taInArray[] to be the result of an ADIR()
		LPARAMETERS taInArray, taOutArray
		EXTERNAL ARRAY taInArray, taOutArray
		LOCAL lnRows, lnCols, i, j
		* extending taInArray[] to process the files in date/time order
		lnRows = ALEN(taInArray,1)
		lnCols = ALEN(taInArray,2)
		* create taOutArray with one more column
		DIMENSION taOutArray[lnRows,lnCols + 1]
		* and fill it with taInArray columns AND (filedate + filetime) in new column
		* ( ACOPY() doesn't preserve elements properly because of different # of columns )
		FOR i = 1 TO lnRows
			FOR j = 1 TO lnCols
				taOutArray[i,j] = taInArray[i,j]
			ENDFOR
			taOutArray[i,lnCols + 1] = DTOS(taInArray[i,3]) + "_" + taInArray[i,4]
		ENDFOR
		* sort taOutArray on date/time column
		=ASORT(taOutArray,6)
	ENDPROC


ENDDEFINE


*!*				lcSpreadsheetTemplate = "F:\UTIL\KRONOS\TEMPLATES\CADRIVER_DAILY_TEMPLATE.XLS"

*!*				IF FILE(lcFileToSaveAs) THEN
*!*					DELETE FILE (lcFileToSaveAs)
*!*				ENDIF

*!*				** output to Excel spreadsheet
*!*				.TrackProgress('Creating spreadsheet: ' + lcFileToSaveAs, LOGIT+SENDIT+NOWAITIT)

*!*				oWorkbook = .oExcel.workbooks.OPEN(lcSpreadsheetTemplate)
*!*				oWorkbook.SAVEAS(lcFileToSaveAs)

*!*				oWorksheet1 = oWorkbook.Worksheets[1]
*!*				oWorksheet1.RANGE("A4","J200").ClearContents()
*!*				lcSubjectR = "CA Driver Daily Report for " + TRANSFORM(ld2DaysAgo)
*!*				oWorksheet1.RANGE("A1").VALUE = lcSubjectR
*!*				oWorksheet1.RANGE("G1").VALUE = lnTotDollarsForDay

*!*				lnRow = 3

*!*				SELECT CURSUMMARY
*!*				SCAN
*!*					lnRow = lnRow + 1
*!*					lcRow = ALLTRIM(STR(lnRow))

*!*					oWorksheet1.RANGE("A"+lcRow).VALUE = CURSUMMARY.WORKDATE
*!*					oWorksheet1.RANGE("B"+lcRow).VALUE = CURSUMMARY.NAME
*!*					oWorksheet1.RANGE("C"+lcRow).VALUE = "'" + CURSUMMARY.FILE_NUM
*!*					oWorksheet1.RANGE("D"+lcRow).VALUE = CURSUMMARY.GTDHOURS
*!*					oWorksheet1.RANGE("E"+lcRow).VALUE = CURSUMMARY.REGHOURS
*!*					oWorksheet1.RANGE("F"+lcRow).VALUE = CURSUMMARY.OVTHOURS
*!*					oWorksheet1.RANGE("G"+lcRow).VALUE = CURSUMMARY.INCPAY
*!*					oWorksheet1.RANGE("H"+lcRow).VALUE = CURSUMMARY.PTOHOURS
*!*					oWorksheet1.RANGE("I"+lcRow).VALUE = CURSUMMARY.BASEWAGE
*!*					oWorksheet1.RANGE("J"+lcRow).VALUE = CURSUMMARY.TOTDOLLARS
*!*				ENDSCAN

*!*				oWorkbook.SAVE()
*!*				.oExcel.QUIT()

