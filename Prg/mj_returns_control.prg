utilsetup("MJ_RETURNS_CONTROL")
Set Exclusive Off
Set Deleted On
Set Talk Off
Set Safety Off
With _Screen
	.AutoCenter = .T.
	.WindowState = 0
	.BorderStyle = 1
	.Width = 320
	.Height = 210
	.Top = 290
	.Left = 110
	.Closable = .T.
	.MaxButton = .T.
	.MinButton = .T.
	.ScrollBars = 0
	.Caption = "MJ_RETURNS_CONTROL"
Endwith
schedupdate()

xsqlexec("select * from invenloc where mod='J'",,,"wh")

*************identifies return locations with qty greater than 7
Select Style, Color,Id, WHSELOC, Sum(LOCQTY) From INVENLOC Where Between(WHSELOC,'2RT10A','2RT67J') ;
	AND ACCOUNTID=6303 And LOCQTY !=0  Group By Style, Color,Id, WHSELOC Order By Style, Color,Id, WHSELOC Into Cursor RTRNS Readwrite
Select * From RTRNS Where  SUM_LOCQTY>7 Into Cursor rtrns2

Select rtrns2
Export To C:\RETURNS_QTY_GREATER_THAN7 Xls
If Reccount() > 0
	tsendto = "tmarg@fmiint.com,yenny.garcia@tollgroup.com"
	tattach = "C:\RETURNS_QTY_GREATER_THAN7.XLS"
	tcc =""
	tFrom ="Toll EDI System Operations <fmi-transload-ops@fmiint.com>"
	tmessage = "Returns location qty greater than 7"
	tmessage = tmessage+Chr(13)+"MJ_RETURNS_CONTROL.prg"
	tSubject = "Returns location qty greater than 7"
	Do Form m:\dev\frm\dartmail2 With tsendto,tFrom,tSubject, tcc,tattach,tmessage  ,"A"
Else
	tsendto = "tmarg@fmiint.com"
	tattach = ""
	tcc =""
	tFrom ="Toll EDI System Operations <fmi-transload-ops@fmiint.com>"
	tmessage = "NO_Returns location qty greater than 7_exist_"+Ttoc(Datetime())
	tSubject = "NO_Returns location qty greater than 7_exist"
	Do Form m:\dev\frm\dartmail2 With tsendto,tFrom,tSubject, tcc,tattach,tmessage  ,"A"
Endif


*************identifies return SKU's in multiple locations
Select Cnt(1) As LOCATION_COUNT, Style, Color, Id From RTRNS Group By Style, Color, Id Into Cursor rtrns3 Readwrite
Select * From rtrns3 Where LOCATION_COUNT >1 Into Cursor rtrns4 Readwrite
Select Distinct Style,Color,Id From rtrns4 Into Cursor RTRNS5 Readwrite
Select B.Style,B.Color,B.Id,B.LOCQTY,B.WHSELOC From RTRNS5 a Left Join INVENLOC B On a.Style=B.Style ;
	AND a.Color=B.Color And a.Id=B.Id And B.ACCOUNTID=6303 And B.WHSELOC!='2RT67G' And Between(B.WHSELOC,'2RT10A','2RT67J');
	INTO Cursor RTRNS6 Readwrite

Export To C:\RETURNS_MULTI_LOCATIONS Xls

Select rtrns4
If Reccount() > 0
	tsendto = "tmarg@fmiint.com,yenny.garcia@tollgroup.com"
	tattach = "c:\RETURNS_MULTI_LOCATIONS.XLS"
	tcc =""
	tFrom ="Toll EDI System Operations <fmi-transload-ops@fmiint.com>"
	tmessage = "Returns in multi locations"
	tmessage = tmessage+Chr(13)+"MJ_RETURNS_CONTROL.prg"
	tSubject = "Returns in multi locations"
	Do Form m:\dev\frm\dartmail2 With tsendto,tFrom,tSubject, tcc,tattach,tmessage  ,"A"
Else
Endif

xsqlexec("select * from invenloc where mod='J'",,,"wh")

Select INVENLOC
Select ACCOUNTID, Style, Color, Id, LOCQTY, WHSELOC, Dtoc(ADDDT) As Date From INVENLOC Where WHSELOC='CART' And LOCQTY!=0;
	AND ADDDT <=(Date()-2)  Into Cursor cart
Select cart

If Reccount() > 0
	Export To "S:\MarcJacobsData\Inventory\CART_inventory"  Type Xls

	tsendto = "tmarg@fmiint.com,yenny.garcia@tollgroup.com"
	tattach = "S:\MarcJacobsData\inventory\CART_inventory.xls"
	tcc =""
	tFrom ="Toll EDI System Operations <fmi-transload-ops@fmiint.com>"
	tmessage = "INVENTORY EXISTS IN CART LOCATIONS  "+Ttoc(Datetime())
	tmessage = tmessage+Chr(13)+"MJ_RETURNS_CONTROL.prg"
	tSubject = "INVENTORY exists in carts older than 2 days"
	Do Form m:\dev\frm\dartmail2 With tsendto,tFrom,tSubject, tcc,tattach,tmessage  ,"A"
Else
Endif

Use In  INVENLOC

****************identifies PL inventory for which the PL has been put away - I.E. left over inventory in PL location
****NJ

xsqlexec("select * from invenloc where mod='J'",,,"wh")

goffice="J"
xsqlexec("select * from adj where mod='J' and between(adjdt,{"+dtoc(date()-30)+"},{"+dtoc(date()-1)+"})",,,"wh")
Select Distinct WHSELOC From ADJ Where WHSELOC='PL' Into Cursor BC1 Readwrite

Select Distinct WHSELOC From INVENLOC Where WHSELOC='PL' And LOCQTY!=0 Into Cursor T1 Readwrite
Select * From T1 a Left Join BC1 B On a.WHSELOC=B.WHSELOC Into Cursor C1 Readwrite
Set Step On
Select WHSELOC_B As WHSELOC From C1 Where !Isnull(WHSELOC_B) Into Cursor C2 Readwrite
Select B.WHSELOC,Style,Color,Id,LOCQTY From C2 a Left Join INVENLOC B On a.WHSELOC=B.WHSELOC And  !Isnull(a.WHSELOC) Into Cursor C3 Readwrite

Select C3

If Reccount() > 0
	Export To "S:\MarcJacobsData\temp\leftover_pl_inventory"  Type Xls

	tsendto = "tmarg@fmiint.com,yenny.garcia@tollgroup.com"
	tattach = "S:\MarcJacobsData\temp\leftover_pl_inventory.xls"
	tcc =""
	tFrom ="Toll EDI System Operations <fmi-transload-ops@fmiint.com>"
	tmessage = "INVENTORY EXISTS IN PL locations  "+Ttoc(Datetime())
	tmessage = tmessage+Chr(13)+"MJ_RETURNS_CONTROL.prg"
	tSubject = "INVENTORY EXISTS IN PL locations"
	Do Form m:\dev\frm\dartmail2 With tsendto,tFrom,tSubject, tcc,tattach,tmessage  ,"A"
Else
Endif

xsqlexec("select * from invenloc where mod='J'",,,"wh")

goffice='J'
useca("inwolog","wh",,,"mod='"+goffice+"' and  accountid in (6303,6325,6543) and confirmdt>{"+dtoc(date()-200)+"}")
useca("indet","wh",,,"mod='"+goffice+"' and  accountid in (6303,6325,6543) and date_rcvd>{"+dtoc(date()-200)+"}")

SELECT WHSELOC,STYLE,COLOR,ID,LOCQTY  FROM INVENLOC WHERE whseloc='2RT' AND ACCOUNTID=6303 INTO CURSOR T1 READWRITE
SELECT (MAX(CONFIRMDT))  as confirmdt, STYLE,COLOR,ID FROM INWOLOG A, INDET B WHERE A.INWOLOGID=B.INWOLOGID AND zRETURNS GROUP BY STYLE,COLOR,ID  INTO CURSOR B1 READWRITE
SELECT A.*, CONFIRMDT FROM T1 A LEFT JOIN B1 B ON a.style=b.style AND a.color=b.color AND a.id=b.id  INTO CURSOR C1 READWRITE
REPLACE confirmdt WITH {01/01/2014} FOR ISNULL(CONFIRMDT)
SELECT * FROM C1 where confirmdt<date()-200 ORDER BY CONFIRMDT INTO CURSOR C2 READWRITE

set step on

If Reccount() > 0
	Export To S:\MarcJacobsData\TEMP\RETURNS_FOR_LOC_CHANGE Xls
	tsendto = "todd.margolin@tollgroup.com,yenny.garcia@tollgroup.com"
	tattach = "S:\MarcJacobsData\TEMP\RETURNS_FOR_LOC_CHANGE.xls"
	tcc =""
	tFrom ="Toll EDI System Operations <fmi-transload-ops@fmiint.com>"
	tmessage = "RETURNS AVAILABLE FOR LOCATION CHANGE"
	tmessage = tmessage+Chr(13)+"MJ_RETURNS_CONTROL.prg"
	tSubject = "RETURNS AVAILABLE FOR LOCATION CHANGE"
	Do Form m:\dev\frm\dartmail2 With tsendto,tFrom,tSubject, tcc,tattach,tmessage  ,"A"
Else
Endif

xsqlexec("select * from cmtrans where empty(invloc) and scantime>{"+dtoc((date()-90))+"} AND trolly='PL'",,,"wh")
Select Distinct trolly From cmtrans Into Cursor temp70

If Reccount() > 0
	Export To S:\MarcJacobsData\TEMP\PL_missing_RA Xls
	tsendto = "tmarg@fmiint.com,yenny.garcia@tollgroup.com,doug.wachs@tollgroup.com"
	tattach = "S:\MarcJacobsData\TEMP\PL_missing_RA.xls"
	tcc =""
	tFrom ="Toll EDI System Operations <fmi-transload-ops@fmiint.com>"
	tmessage = "PL's missing RA, please supply RA and carton id"
	tSubject = "PL's missing RA"
	Do Form m:\dev\frm\dartmail2 With tsendto,tFrom,tSubject, tcc,tattach,tmessage  ,"A"
Else
*!*		tsendto = "yenny.garcia@tollgroup.com"
*!*		tattach = ""
*!*		tcc =""
*!*		tFrom ="Toll EDI System Operations <fmi-transload-ops@fmiint.com>"
*!*		tmessage = "NO AX2012 Input not performed "+Ttoc(Datetime())
*!*		tSubject = "NO AX2012 Input not performed"
*!*		Do Form m:\dev\frm\dartmail2 With tsendto,tFrom,tSubject, tcc,tattach,tmessage  ,"A"
Endif

Close Databases All

****ML

xsqlexec("select * from invenloc where mod='L'",,,"wh")

goffice="L"
xsqlexec("select * from adj where mod='L' and between(adjdt,{"+dtoc(date()-30)+"},{"+dtoc(date()-1)+"})",,,"wh")
Select Distinct WHSELOC From ADJ Where WHSELOC='PL' Into Cursor BC1 Readwrite

Select Distinct WHSELOC From INVENLOC Where WHSELOC='ML' And LOCQTY!=0 Into Cursor T1 Readwrite
Select * From T1 a Left Join BC1 B On a.WHSELOC=B.WHSELOC Into Cursor C1 Readwrite
Select WHSELOC_B As WHSELOC From C1 Where !Isnull(WHSELOC_B) Into Cursor C2 Readwrite
Select B.WHSELOC,Style,Color,Id,LOCQTY From C2 a Left Join INVENLOC B On a.WHSELOC=B.WHSELOC And  !Isnull(a.WHSELOC) Into Cursor C3 Readwrite

Select C3

If Reccount() > 0
	Export To "S:\MarcJacobsData\temp\leftover_ML_inventory"  Type Xls

	tsendto = "tmarg@fmiint.com"
	tattach = "S:\MarcJacobsData\temp\leftover_ML_inventory.xls"
	tcc =""
	tFrom ="Toll EDI System Operations <fmi-transload-ops@fmiint.com>"
	tmessage = "INVENTORY EXISTS IN ML locations  "+Ttoc(Datetime())
	tmessage = tmessage+Chr(13)+"MJ_RETURNS_CONTROL.prg"
	tSubject = "INVENTORY EXISTS IN ML locations"
	Do Form m:\dev\frm\dartmail2 With tsendto,tFrom,tSubject, tcc,tattach,tmessage  ,"A"
Else
Endif


*********************Identlifies inventory in ML locations greater than 3 days

Close Databases All

****ML

xsqlexec("select * from invenloc where mod='L'",,,"wh")

xsqlexec("select * from cmtrans",,,"wh")
index on trolly tag trolly
index on invloc tag invloc
set order to

Select ACCOUNTID,WHSELOC,Style,Color,Id,LOCQTY From INVENLOC Where LOCQTY!=0 And Inlist(ACCOUNTID,6303) And WHSELOC ='ML' Into Cursor D1 Readwrite
Select a.*, SCANTIME From D1 a Left Join cmtrans B On a.WHSELOC=B.TROLLY Into Cursor CML1 Readwrite
Select a.*, Max(SCANTIME) As SCANTIME From D1 a Left Join cmtrans B On a.WHSELOC=B.TROLLY Group By a.ACCOUNTID,a.WHSELOC,a.Style,a.Color,a.Id,LOCQTY Into Cursor CML1 Readwrite
Select * From CML1 Where SCANTIME<Date()-3 Into Cursor CML2 Readwrite

Select CML2

If Reccount() > 0
	Export To "S:\MarcJacobsData\temp\returns_in_ml_location"  Type Xls

	tsendto = "todd.margolin@TOLLGROUP.com,ana.hernandez@tollgroup.com,jimmy.saracay@tollgroup.com,TGFAmericas.Marcjacobs@tollgroup.com"
	tattach = "S:\MarcJacobsData\temp\returns_in_ml_location.xls"
	tcc =""
	tFrom ="Toll EDI System Operations <fmi-transload-ops@fmiint.com>"
	tmessage = "INVENTORY EXISTS IN ML locations, please move to non ML non hold location "+Ttoc(Datetime())
	tmessage = tmessage+Chr(13)+"MJ_RETURNS_CONTROL.prg"
	tSubject = "INVENTORY EXISTS IN ML locations"
	Do Form m:\dev\frm\dartmail2 With tsendto,tFrom,tSubject, tcc,tattach,tmessage  ,"A"
Else
Endif

If !Used("inwolog")
	useca("inwolog","wh",,,"mod='"+goffice+"' and  accountid in (6303,6325,6543) and confirmdt>{"+dtoc(date()-10)+"}",,"inwologml")
Endif


***identifies when the AX2012 Input button has not been hit
*!*	Select wo_num, Container,Reference From inwolog Where RETURNS And Inlist(ACCOUNTID,6303,6325) And !Empty(CONFIRMDT) And Empty(AXDT) And Container='ML'  And Reference !='MLNO' And Reference !='2' And brokerref!='NO AX' And brokerref!='BOL' Into Cursor temp44
*!*	Select temp44

*!*	If Reccount() > 0
*!*		Export To S:\MarcJacobsData\TEMP\ax2012_input_not_performed Xls
*!*		tsendto = "todd.margolin@tollgroup.com,jimmy.saracay@tollgroup.com,achmad.suaidy@tollgroup.com,achmad.suaidy@tollgroup.com,Tgfamericas.marcjacobs@tollgroup.com,ana.hernandez@tollgroup.com "
*!*		tattach = "S:\MarcJacobsData\TEMP\ax2012_input_not_performed.xls"
*!*		tcc =""
*!*		tFrom ="Toll EDI System Operations <fmi-transload-ops@fmiint.com>"
*!*		tmessage = "AX2012 Input not performed"
*!*		tmessage = tmessage+Chr(13)+"MJ_RETURNS_CONTROL.prg"
*!*		tSubject = "AX2012 Input not performed"
*!*		Do Form m:\dev\frm\dartmail2 With tsendto,tFrom,tSubject, tcc,tattach,tmessage  ,"A"
*!*	Else
*!*	Endif

***identifies ML's missing RA's
Select Distinct TROLLY From cmtrans Where Empty(invloc) And TROLLY='ML' And  SCANTIME>{10/17/2014} Into Cursor temp70
If Reccount() > 0
	Export To S:\MarcJacobsData\TEMP\ML_missing_RA Xls
	tsendto = "todd.margolin@tollgroup.com,doug.wachs@tollgroup.com,Tgfamericas.marcjacobs@tollgroup.com,ana.hernandez@tollgroup.com"
	tattach = "S:\MarcJacobsData\TEMP\ML_missing_RA.xls"
	tcc =""
	tFrom ="Toll EDI System Operations <fmi-transload-ops@fmiint.com>"
	tmessage = "ML's missing RA"
	tmessage = tmessage+Chr(13)+"MJ_RETURNS_CONTROL.prg"
	tSubject = "ML's missing RA"
	Do Form m:\dev\frm\dartmail2 With tsendto,tFrom,tSubject, tcc,tattach,tmessage  ,"A"
Else
Endif

***identifies WO's not created
Select Distinct TROLLY From cmtrans Where !wo_created And SCANTIME>{10/17/2014} And TROLLY='ML'  and invloc !='MLNO'   Into Cursor temp60
If Reccount() > 0
	Export To S:\MarcJacobsData\TEMP\WO_not_created_for_ML Xls
	tsendto = "todd.margolin@tollgroup.com,jimmy.saracay@tollgroup.com,Tgfamericas.marcjacobs@tollgroup.com,ana.hernandez@tollgroup.com"
	tattach = "S:\MarcJacobsData\TEMP\WO_not_created_for_ML.xls"
	tcc =""
	tFrom ="Toll EDI System Operations <fmi-transload-ops@fmiint.com>"
	tmessage = "WO's not created"
	tmessage = tmessage+Chr(13)+"MJ_RETURNS_CONTROL.prg"
	tSubject = "WO's not created"
	Do Form m:\dev\frm\dartmail2 With tsendto,tFrom,tSubject, tcc,tattach,tmessage  ,"A"
Else
Endif

Close Data All
_Screen.Caption=gscreencaption
