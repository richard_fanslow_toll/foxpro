utilsetup("MJ_PI_STATUS")
Set Exclusive Off
Set Deleted On
Set Talk Off
Set Safety Off 


WITH _SCREEN	
	.AUTOCENTER = .T.
	.WINDOWSTATE = 0
	.BORDERSTYLE = 1
	.WIDTH = 320
	.HEIGHT = 210
	.TOP = 290
	.LEFT = 110
	.CLOSABLE = .T.
	.MAXBUTTON = .T.
	.MINBUTTON = .T.
	.SCROLLBARS = 0
	.CAPTION = "MJ_RETURN_EXPORT_FILE"
ENDWITH	
**********************************START NJ
	CLOSE DATABASES all
*!*	If !Used("phys")
*!*	useca("phys","wh",,,"select * from phys where accountid in (6303,6325,6543)",,"phys")
*!*	ENDIF

*!*		if !used("FROZ2016")
*!*			use s:\marcjacobsdata\reports\inventorysyncfiles\bi\prod\frozen\froz2016.dbf in 0
*!*		endif

*!*	xsqlexec("select * from phystag where mod='J'",,,"wh")

*!*	SELECT ACCOUNTID AS WHSEACCT, SPACE(4) AS AXACCT,WHSELOC, STYLE,COLOR,ID,00000 AS FROZEN_QTY,ORIGQTY AS COUNT1 FROM PHYS;
*!*	WHERE ZCOUNT='F' INTO CURSOR T1 READWRITE

*!*	SELECT A.*, TOTQTY AS COUNT2, 00000 AS FINAL_COUNT FROM T1 A LEFT JOIN PHYS B ON A.WHSEACCT=B.ACCOUNTID AND A.WHSELOC=B.WHSELOC AND;
*!*	 a.style=b.style AND a.color=b.color AND a.id=b.id AND ZCOUNT='2' INTO CURSOR T2 READWRITE

*!*	**REPLACE COUNT2 WITH 0 FOR ISNULL(COUNT2)

*!*	SELECT A.*, TOTQTY AS ZCOUNT FROM T2 A LEFT JOIN PHYS B ON A.WHSEACCT=B.ACCOUNTID AND A.WHSELOC=B.WHSELOC AND;
*!*	 a.style=b.style AND a.color=b.color AND a.id=b.id AND ZCOUNT='F' INTO CURSOR T3 READWRITE

*!*	SELECT A.*, DIFFQTY,DONE FROM T3 A LEFT JOIN PHYSTAG B ON A.WHSELOC=B.WHSELOC INTO CURSOR T4 READWRITE

*!*	**REPLACE FINAL_COUNT WITH 0 FOR ISNULL(FINAL_COUNT)


*!*	REPLACE axacct WITH '1000' FOR whseacct = 6303
*!*	REPLACE axacct WITH '1200' FOR whseacct = 6304
*!*	REPLACE axacct WITH '1151' FOR whseacct = 6325
*!*	REPLACE axacct WITH '1250' FOR whseacct = 6324
*!*	REPLACE axacct WITH '1600' FOR whseacct = 6543
*!*	*USE H:\MJ\Inventory\PI\FROZ2015.DBF IN 0
*!*	USE m:\temp\FROZ2015.DBF IN 0

*!*	SELECT * FROM T4 A FULL JOIN FROZ2015 B ON A.WHSEACCT=B.WHSEACCT AND A.WHSELOC=B.WHSELOC AND a.style=b.style AND;
*!*	 a.color=b.color AND a.id=b.id INTO CURSOR T5 READWRITE
*!*	REPLACE locqty WITH 0 FOR ISNULL(LOCQTY)
*!*	REPLACE locqty WITH 0 FOR (LOCQTY)<0
*!*	DELETE FROM T5 WHERE WHSELOC_B='CYC'

*!*	REPLACE whseacct_a WITH WHSEACCT_B FOR ISNULL(whseacct_a)
*!*	REPLACE AXacct_a WITH AXACCT_B FOR ISNULL(AXacct_a)
*!*	REPLACE whseLOC_a WITH WHSELOC_B FOR ISNULL(whseLOC_a)
*!*	REPLACE STYLE_a WITH STYLE_B FOR ISNULL(STYLE_a)
*!*	REPLACE COLOR_a WITH COLOR_B FOR ISNULL(COLOR_a)
*!*	REPLACE ID_a WITH ID_B FOR ISNULL(ID_a)
*!*	REPLACE FROZEN_QTY WITH LOCQTY FOR ISNULL(FROZEN_QTY)
*!*	REPLACE frozen_qty WITH LOCQTY FOR !ISNULL(COUNT1)
*!*	REPLACE final_count WITH ZCOUNT FOR FROZEN_QTY=ZCOUNT
*!*	REPLACE final_count WITH ZCOUNT FOR DONE


*!*	SELECT WHSEACCT_A AS WHSEACCT, AXACCT_A AS AXACCT, WHSELOC_A AS WHSELOC, STYLE_A AS STYLE, COLOR_A AS COLOR, ;
*!*	ID_A AS ID,FROZEN_QTY,COUNT1,COUNT2,FINAL_COUNT FROM T5 INTO CURSOR T6 READWRITE
*!*	EXPORT TO S:\MarcJacobsData\Reports\InventorySyncFiles\BI\pi_progress\PI_STATUS XLS


*!*	**********************************START ML





*!*	Close Data All
*!*	schedupdate()
*!*	_screen.Caption=gscreencaption
*!*	on error




*!*			If Reccount() > 0 
*!*			export TO "S:\MarcJacobsData\Reports\InventorySyncFiles\BI\pi_progress\pi_status"   TYPE xls 
*!*			tsendto = "tmarg@fmiint.com"
*!*			tattach = "" 
*!*			tcc =""                                                                                                                               
*!*			tFrom ="Toll EDI System Operations <fmi-transload-ops@fmiint.com>"                                
*!*			tmessage = "PI_status_:"+Ttoc(Datetime())        
*!*			tSubject = "PI STATUS REPORT COMPLETE"                                                           
*!*			Do Form m:\dev\frm\dartmail2 With tsendto,tFrom,tSubject, tcc,tattach,tmessage  ,"A"     
*!*				
*!*		ELSE 


*!*		ENDIF
*!*	close data all 

If !Used("phys")
useca("phys","wh",,,"select * from phys where accountid in (6303,6325,6543) and office='N'",,"phys")
ENDIF

	if !used("FROZ2016")
		use s:\marcjacobsdata\reports\inventorysyncfiles\bi\prod\frozen\froz2016.dbf in 0
	endif

SELECT WHSEACCT,SUM(LOCQTY) AS FROZQTY FROM FROZ2016 WHERE INLIST(WHSEACCT,6303,6325,6543) GROUP BY WHSEACCT INTO CURSOR F1 READWRITE

SELECT ACCOUNTID,SUM(TOTQTY) AS PHYSQTY FROM PHYS WHERE INLIST(accountid,6303,6325,6543) and zcount ='F' GROUP BY ACCOUNTID INTO CURSOR P1 READWRITE
SELECT WHSEACCT,FROZQTY,PHYSQTY, ROUND((PHYSQTY/FROZQTY)*100,0) AS PERCENT FROM F1 A LEFT JOIN P1 B ON WHSEACCT=ACCOUNTID INTO CURSOR C1 READWRITE

		If Reccount() > 0 
		export TO "S:\MarcJacobsData\Reports\InventorySyncFiles\BI\pi_progress\pi_status"   TYPE xls 
		tsendto = "tmarg@fmiint.com, E.SKRZYNIARZ@marcjacobs.com, A.Verilhac@marcjacobs.com, L.LOPEZ@marcjacobs.com, J.HALPIN@marcjacobs.com, E.PHILLIPS@marcjacobs.com"
		tattach = "S:\MarcJacobsData\Reports\InventorySyncFiles\BI\pi_progress\pi_status.XLS" 
		tcc =""                                                                                                                               
		tFrom ="Toll EDI System Operations <fmi-transload-ops@fmiint.com>"                                
		tmessage = "PI_status_:"+Ttoc(Datetime())        
		tSubject = "PI STATUS REPORT COMPLETE"                                                           
		Do Form m:\dev\frm\dartmail2 With tsendto,tFrom,tSubject, tcc,tattach,tmessage  ,"A"     
			
	ELSE 


	ENDIF
	
********************   ml	
CLOSE DATABASES all	
If !Used("phys")
useca("phys","wh",,,"select * from phys where accountid in (6303,6325,6543) and office='L'",,"phys")
ENDIF

	if !used("FROZ2016")
		use s:\marcjacobsdata\reports\inventorysyncfiles\bi\prod\frozen\froz2016ML.dbf in 0 ALIAS froz2016
	endif

SELECT WHSEACCT,SUM(LOCQTY) AS FROZQTY FROM FROZ2016 WHERE INLIST(WHSEACCT,6303,6325,6543) GROUP BY WHSEACCT INTO CURSOR F1 READWRITE

SELECT ACCOUNTID,SUM(TOTQTY) AS PHYSQTY FROM PHYS WHERE INLIST(accountid,6303,6325,6543) and zcount ='F' GROUP BY ACCOUNTID INTO CURSOR P1 READWRITE
SELECT WHSEACCT,FROZQTY,PHYSQTY, ROUND((PHYSQTY/FROZQTY)*100,0) AS PERCENT FROM F1 A LEFT JOIN P1 B ON WHSEACCT=ACCOUNTID INTO CURSOR C1 READWRITE

		If Reccount() > 0 
		export TO "S:\MarcJacobsData\Reports\InventorySyncFiles\BI\pi_progress\pi_status_ML"   TYPE xls 
		tsendto = "tmarg@fmiint.com;E.SKRZYNIARZ@marcjacobs.com;A.Verilhac@marcjacobs.com;L.LOPEZ@marcjacobs.com;o.batista@marcjacobs.com;J.HALPIN@marcjacobs.com;E.PHILLIPS@marcjacobs.com"
		tattach = "S:\MarcJacobsData\Reports\InventorySyncFiles\BI\pi_progress\pi_status_ML.XLS" 
		tcc =""                                                                                                                               
		tFrom ="Toll EDI System Operations <fmi-transload-ops@fmiint.com>"                                
		tmessage = "PI_status_ML_:"+Ttoc(Datetime())        
		tSubject = "PI STATUS REPORT COMPLETE FOR ML"                                                           
		Do Form m:\dev\frm\dartmail2 With tsendto,tFrom,tSubject, tcc,tattach,tmessage  ,"A"     
			
	ELSE 


	ENDIF
	
	Close Data All
schedupdate()
_screen.Caption=gscreencaption
on error