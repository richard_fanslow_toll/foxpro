* separate email addreses with ;
* sample:  emailx("darren.young@tollgroup.com",,"subject line","body of email",,"h:\pdf\filename1.xlsx","h:\pdf\filename1.xlsx")

lparameter xxto, xcc, xsubject, xbody, xscreencapture, xattach1, xattach2, xattach3, xattach4, xattach5, xattach6

*if (type("gldor")="L" and !gldor)
*	return .t.
*endif

if type("guserid")="U"
	public guserid
	guserid=GETENV("COMPUTERNAME")
endif
if type("gprocess")="U"
	public gprocess
	gprocess="???"
endif

*if type("gdevelopment")="L" and gdevelopment and type("xto")="C" and xto="dyoung"
*	return .t.
*endif

if !used("dedemail")
	useca("dedemail","stuff")
endif

if !used("stuffgenpk")
	use f:\common\stuffgenpk in 0
endif

xselect=select()

select stuffgenpk
locate for table="DEDEMAIL"
replace id with stuffgenpk.id+1 in stuffgenpk

select dedemail
scatter memvar memo blank

m.dedemailid = stuffgenpk.id
m.xto = iif(empty(xxto),'',xxto)
m.cc = iif(empty(xcc),'',xcc)
m.subject = iif(empty(xsubject),'',xsubject)
m.body = iif(empty(xbody),'',xbody)
m.addby=guserid
m.adddt=datetime()
m.addproc=gprocess
insert into dedemail from memvar

if xscreencapture
	replace screencap with .t.
	**added possible failed capture within capture.prg bc having issues running from some task servers (ie task 1) - mvw 02/18/18 
	if capture(.t.) and file("h:\fox\screenimage.jpg")
		append memo attach1 from "h:\fox\screenimage.jpg"
	endif
else
	if !empty(xattach1)
		replace attname1 with juststem(xattach1)+"."+justext(xattach1)
		append memo attach1 from (xattach1) 
	endif
	if !empty(xattach2)
		replace attname2 with juststem(xattach2)+"."+justext(xattach2)
		append memo attach2 from (xattach2) 
	endif
	if !empty(xattach3)
		replace attname3 with juststem(xattach3)+"."+justext(xattach3)
		append memo attach3 from (xattach3) 
	endif
	if !empty(xattach4)
		replace attname4 with juststem(xattach4)+"."+justext(xattach4)
		append memo attach4 from (xattach4) 
	endif
	if !empty(xattach5)
		replace attname5 with juststem(xattach5)+"."+justext(xattach5)
		append memo attach5 from (xattach5) 
	endif
	if !empty(xattach6)
		replace attname6 with juststem(xattach6)+"."+justext(xattach6)
		append memo attach6 from (xattach6) 
	endif
endif

tu("dedemail")

try
	select (xselect)
catch
endtry