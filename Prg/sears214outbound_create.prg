PARAMETERS cRunType,nWO_Num,cOffice,dReturn

PUBLIC c_CntrlNum,c_GrpCntrlNum,cWO_Num,cGroupName,cDivision,cCustName,cFilename,div214,cST_CSZ
PUBLIC tsendto,tcc,tsendtoerr,tccerr,tcctkrerr,tsendtotest,tcctest,cFin_Status,lDoCatch,cPrgName

cPrgName = "SEARS 214"

cFilename = ""
lTesting = .F.
lFilesOut = !lTesting
lEmail = .T.
lUseTestData = .F.
IF !USED('account')
	xsqlexec("select * from account where inactive=0","account",,"qq")
	index on accountid tag accountid
	set order to
ENDIF

ON ESCAPE CANCEL
lFClose = .T.
lIsError = .F.
lDoCatch = .T.
cShip_ref = ""
cST_CSZ = ""
cFin_Status = ""
nFilenum = 0
SET HOURS TO 24

*ASSERT .f.
TRY
	cSystemName = "UNASSIGNED"
	nAcctNum = 5837
	cWO_Num = ""
	lDoError = .F.
	IF VARTYPE(nWO_Num)= "L"
		IF lTesting
			CLOSE DATABASES ALL
			lFilesOut = .F.
			nWO_Num = 1558227
			cOffice = "C"
			cRunType = "V"
		ELSE
			WAIT WINDOW "No params specified" TIMEOUT 2
			cFin_Status = "NO PARAMS"
			lDoCatch = .F.
			THROW
		ENDIF
	ENDIF

	cDivision = IIF(cOffice="C","California",IIF(cOffice="M","Miami","New Jersey"))
	cOffCity = IIF(cOffice="C","SAN PEDRO",IIF(cOffice="M","MIAMI","CARTERET"))
	cOffState = IIF(cOffice="C","CA",IIF(cOffice="M","FL","NJ"))

	cWO_Num = ALLTRIM(STR(nWO_Num))
	cProcType = "DELIVERED"

	DIMENSION thisarray(1)
	cCustName = "SEARS"
	cMailName = "SEARS"
	cCustFolder = "SEARS"
	dtmail = TTOC(DATETIME())
	dt2 = DATETIME()
	cCustPrefix = cCustName+"214"+LOWER(cRunType)+"_"+LOWER(cOffice)
	dt1 = TTOC(DATETIME(),1)

	IF USED('mm')
	USE IN mm
	ENDIF
	IF USED('mailmaster')
	USE IN mailmaster
	endif
	SELECT 0
	USE F:\3pl\DATA\mailmaster ALIAS mm SHARED
	LOCATE FOR mm.edi_type = "214" AND mm.accountid = nAcctNum ;
		AND mm.acctname = ICASE(cRunType="V","SEARS-EVERGREEN",cRunType="D","SEARS-CHINASHIP",cRunType="E","SEARS-CHINASHIP",cRunType="C","SEARS-CMA/CG",cRunType="M","SEARS-MATSON","SEARS-CHINASHIP")
	_SCREEN.CAPTION = UPPER(ALLTRIM(mm.scaption))

	tsendto = IIF(mm.use_alt,mm.sendtoalt,mm.sendto)
	tcc = IIF(mm.use_alt,mm.ccalt,mm.cc)
	cFilename = (ALLTRIM(mm.holdpath)+cCustPrefix+dt1+".txt")
	cFilenameShort = JUSTFNAME(cFilename)
	cFilename2 = (ALLTRIM(mm.archpath)+cFilenameShort)
	cFilename3 = (ALLTRIM(mm.basepath)+cFilenameShort)

	LOCATE
	LOCATE FOR mm.edi_type = "MISC" AND mm.taskname = "GENERAL"
	tsendtoerr = IIF(mm.use_alt,mm.sendtoalt,mm.sendto)
	tccerr = IIF(mm.use_alt,mm.ccalt,mm.cc)

	LOCATE
	LOCATE FOR mm.edi_type = "MISC" AND mm.taskname = "TRUCKINGERR"
	tcctkrerr = IIF(mm.use_alt,mm.ccalt,mm.cc)

	IF lTesting
		LOCATE
		LOCATE FOR mm.edi_type = "MISC" AND mm.taskname = "GENERAL"
		tsendto = IIF(mm.use_alt,mm.sendtoalt,mm.sendto)
		tcc = IIF(mm.use_alt,mm.ccalt,mm.cc)
		tsendtoerr = tsendto
		tccerr = tcc
	ENDIF
	USE IN mm

	tfrom = "Toll EDI Operations <toll-edi-ops@tollgroup.com>"
	tattach = ""
	STORE "" TO cST_CSZ,cST_Name,cST_Addr,cSF_CSZ,cSF_Name,cSF_Addr
	STORE "" TO cPUDLocCity,cPUDLocState,cPUDLocZip
	STORE "" TO cFMICity,cFMIState,cFMIZip

	SELECT 0
	USE

	nFilenum = FCREATE(cFilename)

*!* Open the Tables
	xsqlexec("select * from pudlocs",,,"wo")
	index on accountid tag accountid
	set order to

	IF !USED('wolog')
		SELECT 0
		USE F:\wo\wodata\wolog ALIAS wolog ORDER TAG wo_num SHARED NOUPDATE
	ENDIF

	cfd = "*"  && Field delimiter
	csegd = "~"  && Segment delimiter
	cterminator = ">"  && Used at end of ISA segment

	SELECT wolog
	SEEK nWO_Num
*	SET STEP ON
	SCATTER FIELDS accountid,fromloc,toloc MEMVAR
	IF INLIST(cRunType,"D","M","C","E")
		STORE m.toloc TO nLocCode
	ELSE
		STORE m.fromloc TO nLocCode
	ENDIF

	IF EMPTY(nLocCode)
		nLocCode = 4145
	ENDIF

	IF !SEEK(nWO_Num)
		cFin_Status = "MISS WO-WOLOG"
		THROW
	ELSE
		SELECT pudlocs
		LOCATE
		LOCATE FOR pudlocs.accountid = m.accountid AND pudlocs.dash = nLocCode
		ASSERT .F. MESSAGE "At PUDLoc process...debug"
		IF FOUND()
			cPUDLocCity = UPPER(ALLTRIM(pudlocs.city))
			cPUDLocState = UPPER(ALLTRIM(pudlocs.state))
			cPUDLocZip = ALLTRIM(pudlocs.zip)
			IF INLIST(cRunType,"D","M","C")
				cST_CSZ = cPUDLocCity+cfd+cPUDLocState+cfd+cPUDLocZip
				cST_Name = UPPER(ALLTRIM(pudlocs.acct_name))
				cST_Addr = UPPER(ALLTRIM(pudlocs.acct_name2))
			ELSE
				cSF_CSZ = cPUDLocCity+cfd+cPUDLocState+cfd+cPUDLocZip
				cSF_Name = UPPER(ALLTRIM(pudlocs.acct_name))
				cSF_Addr = UPPER(ALLTRIM(pudlocs.acct_name2))
			ENDIF
		ELSE
			cFin_Status = "MISS PUDLOC INFO"
			THROW
		ENDIF
		IF INLIST(cRunType,"D","M","C")
			IF (EMPTY(cST_CSZ) OR EMPTY(cST_Name) OR EMPTY(cST_Addr))
				cFin_Status = "MISS PUDLOC ST ADDR INFO"
				WAIT WINDOW cFin_Status TIMEOUT 3
				THROW
			ENDIF
		ELSE
			IF (EMPTY(cSF_CSZ) OR EMPTY(cSF_Name) OR EMPTY(cSF_Addr))
				cFin_Status = "MISS PUDLOC SF ADDR INFO"
				WAIT WINDOW cFin_Status TIMEOUT 3
				THROW
			ENDIF
		ENDIF
	ENDIF

	DO CASE
		CASE cOffice = "L"
			cFMIAddr = "3355 DULLES DR"
			cFMICity = "MIRA LOMA"
			cFMIState = "CA"
			cFMIZip = "91752"
		CASE cOffice = "C"
			cFMIAddr = "350-400 WESTMONT DR"
			cFMICity = "SAN PEDRO"
			cFMIState = "CA"
			cFMIZip = "90731"
		CASE cOffice = "N"
			cFMIAddr = "800 FEDERAL BLVD"
			cFMICity = "CARTERET"
			cFMIState = "NJ"
			cFMIZip = "07008"
		CASE cOffice = "M"
			cFMIAddr = "2100 NW 129TH AVE SUITE 100"
			cFMICity = "MIAMI"
			cFMIState = "FL"
			cFMIZip = "33178"
	ENDCASE

	DO CASE
		CASE INLIST(cRunType,"D","M","E")
			STORE "TGF INC." TO cSF_Name
			STORE cFMIAddr TO cSF_Addr
			STORE cFMICity+cfd+cFMIState+cfd+cFMIZip TO cSF_CSZ
		OTHERWISE
			STORE "TGF INC." TO cST_Name
			STORE cFMIAddr TO cST_Addr
			STORE cFMICity+cfd+cFMIState+cfd+cFMIZip TO cST_CSZ
	ENDCASE

	csendid = "TGFJ" && IIF(DATETIME()>DATETIME(2014,01,27,15,00,00),"TGFJ","FMIT") && && Sender ID code
	csendqual = "02"  && Sender qualifier
	crecqual = "ZZ"  && Recip qualifier
	DO CASE
		CASE cRunType = "M"
			crecid = "MNC001"   && Recip ID Code
		CASE cRunType = "C"
			crecid = "CMACGM214"   && Recip ID Code
		CASE cRunType = "D"
			crecid = "CHINASHIP"   && Recip ID Code
		CASE cRunType = "V"
			crecid = "EVERGREEN"   && Recip ID Code
		OTHERWISE
			crecid = "CHINASHIP"   && Recip ID Code
	ENDCASE

	cDate = DTOS(DATE())
	cTruncDate = RIGHT(cDate,6)
	cTruncTime = SUBSTR(TTOC(DATETIME(),1),9,4)
	cfiledate = cDate+cTruncTime
	csendidlong = PADR(csendid,15," ")
	crecidlong = PADR(crecid,15," ")
	cString = ""

	nSTCount = 0
	nSegCtr = 0
	nLXNum = 1
	STORE "" TO cEquipNum,cHAWB

	WAIT CLEAR
	WAIT WINDOW "Now creating Trucking WO#-based 214 "+IIF(INLIST(cRunType,"D","M"),"Delivery","Pickup")+" information..." NOWAIT

	IF lTesting
		cISACode = "T"
	ELSE
		cISACode = "P"
	ENDIF

	DO num_incr_isa
	cISA_Num = PADL(c_CntrlNum,9,"0")
	STORE "ISA"+cfd+"00"+cfd+"          "+cfd+"00"+cfd+"          "+cfd+csendqual+cfd+csendidlong+cfd+;
		crecqual+cfd+crecidlong+cfd+cTruncDate+cfd+cTruncTime+cfd+"^"+cfd+"00401"+cfd+;
		cISA_Num+cfd+"0"+cfd+cISACode+cfd+":"+csegd TO cString  && +cfd+cterminator
	DO cstringbreak

	STORE "GS"+cfd+"QM"+cfd+csendid+cfd+crecid+cfd+cDate+cfd+cTruncTime+cfd+c_CntrlNum+;
		cfd+"X"+cfd+"004010"+csegd TO cString
	DO cstringbreak
	DO num_incr_st

	STORE "ST"+cfd+"214"+cfd+PADL(c_GrpCntrlNum,9,"0")+csegd TO cString
	DO cstringbreak
	nSTCount = nSTCount + 1
	nSegCtr = nSegCtr + 1

	SELECT wolog
	SEEK(nWO_Num)
	cRefID = IIF(cRunType = "V",ALLTRIM(wolog.brokref),ALLT(STR(wolog.wo_num)))
	SET STEP ON 
	IF EMPTY(cRefID)
		cFin_Status = "EMPTY "+IIF(cRunType = "V","BROKREF","WO#")+" /SEARS WO "+ALLT(STR(wolog.wo_num))
		THROW
	ENDIF

	cBL = IIF(cRunType = "V",SUBSTR(ALLTRIM(wolog.mblnum),2),ALLTRIM(wolog.brokref))
	IF EMPTY(cBL)
		cBL = ALLTRIM(wolog.mblnum)
		IF EMPTY(cBL)
			cFin_Status =  IIF(cRunType = "V","EMPTY MBLNUM","wolog.brokref")
			THROW
		ENDIF
	ENDIF

	cMBL = ALLTRIM(wolog.mblnum)
	IF EMPTY(cMBL) AND crecid = "CHINASHIP"
		cFin_Status = "EMPTY MBLNUM/CHINA SHIPPING Work Order "+ALLT(STR(wolog.wo_num))
		THROW
	ENDIF

	cMBL = IIF(cRunType = "V",RIGHT(cMBL,4),cMBL)
	STORE "B10"+cfd+cRefID+cfd+cMBL+cfd+csendid+csegd TO cString
	DO cstringbreak
	nSegCtr = nSegCtr + 1

	DO CASE
		CASE INLIST(cRunType,"D","E","V")
			STORE "N1"+cfd+"SH"+cfd+cSF_Name+csegd TO cString
		CASE cRunType = "M"
			STORE "N1"+cfd+"SH"+cfd+cSF_Name+cfd+"ZZ"+cfd+"USMIK"+csegd TO cString
		OTHERWISE
			STORE "N1"+cfd+"SH"+cfd+cSF_Name+cfd+"ZZ"+cfd+"USMIKFMII"+csegd TO cString
	ENDCASE

	DO cstringbreak
	nSegCtr = nSegCtr + 1

	STORE "N3"+cfd+cSF_Addr+csegd TO cString
	DO cstringbreak
	nSegCtr = nSegCtr + 1

	STORE "N4"+cfd+cSF_CSZ+csegd TO cString
	DO cstringbreak
	nSegCtr = nSegCtr + 1

	DO CASE
		CASE cRunType = "M"
			STORE "N1"+cfd+"CN"+cfd+cST_Name+cfd+"ZZ"+cfd+"USMIK"+csegd TO cString
		CASE INLIST(cRunType,"D","E")
			STORE "N1"+cfd+"CN"+cfd+cST_Name+cfd+"ZZ"+cfd+"USMIKFMII"+csegd TO cString
		OTHERWISE
			STORE "N1"+cfd+"CN"+cfd+cST_Name+csegd TO cString
	ENDCASE
	DO cstringbreak
	nSegCtr = nSegCtr + 1

	IF !EMPTY(cST_Addr)
		STORE "N3"+cfd+cST_Addr+csegd TO cString
		DO cstringbreak
		nSegCtr = nSegCtr + 1
	ENDIF

	IF !EMPTY(cST_CSZ)
		STORE "N4"+cfd+cST_CSZ+csegd TO cString
		DO cstringbreak
		nSegCtr = nSegCtr + 1
	ENDIF

*!* Now in line loop
	STORE "LX"+cfd+ALLTRIM(STR(nLXNum))+csegd TO cString  && Size
	DO cstringbreak
	nSegCtr = nSegCtr + 1
	nLXNum = nLXNum + 1

	cShipStatus = IIF(cRunType = "M","X3","X1")
	IF EMPTY(wolog.delivered)
		cStatusDate = DTOC(DATE(),1)
	ELSE
		cStatusDate = DTOC(wolog.delivered,1)
	ENDIF
	IF cRunType ="E"
		cStatusDate = DTOC(dReturn,1)
		IF EMPTY(cStatusDate)
			cFin_Status = "MISS TRIG ATDOCK DT"
			THROW
		ENDIF
		cShipStatus ="D1"
	ENDIF


	IF !EMPTY(wolog.updatedt)
		IF cOffice = "C"
			cStatusTime = STRTRAN(LEFT(TTOC(DATETIME()-(3*3600),2),5),":","")
		ELSE
			cStatusTime = STRTRAN(LEFT(TTOC(wolog.updatedt,2),5),":","")
		ENDIF
	ELSE
		cStatusTime = STRTRAN(LEFT(TTOC(DATETIME()-(3*3600),2),5),":","")
	ENDIF
	IF lTesting AND EMPTY(wolog.updatedt)
		cStatusTime = "1530"
	ENDIF

*	SET STEP ON
	STORE "AT7"+cfd+cShipStatus+cfd+"NS"+REPLICATE(cfd,3)+cStatusDate+cfd+cStatusTime+cfd+"LT"+csegd TO cString
	DO cstringbreak
	nSegCtr = nSegCtr + 1

	STORE "MS1"+cfd+cPUDLocCity+cfd+cPUDLocState+csegd TO cString
	DO cstringbreak
	nSegCtr = nSegCtr + 1

	CCtrPrefix = LEFT(wolog.CONTAINER,4)
	cCtrNumber = ALLTRIM(SUBSTR(wolog.CONTAINER,5))
	STORE "MS2"+cfd+CCtrPrefix+cfd+cCtrNumber+csegd TO cString
	DO cstringbreak
	nSegCtr = nSegCtr + 1

	IF cRunType = "V"
		STORE "L11"+cfd+cBL+cfd+"BM"+csegd TO cString  && added 9/27/06
	ELSE
		STORE "L11"+cfd+cBL+cfd+"IC"+csegd TO cString  && added 9/27/06
	ENDIF
	DO cstringbreak
	nSegCtr = nSegCtr + 1

*!* Finish processing
	DO close214
	IF nFilenum>0
		=FCLOSE(nFilenum)
	ENDIF

	ASSERT .F. MESSAGE "At file closure"
	DO ediupdate WITH "214 CREATED",.F.,.F.

	COPY FILE [&cFilename] TO [&cFilename2]
	IF lFilesOut
		COPY FILE &cFilename TO &cFilename3
		DELETE FILE &cFilename
	ENDIF

	IF !lTesting
		IF !USED("ftpedilog")
			SELECT 0
			USE F:\edirouting\ftpedilog ALIAS ftpedilog
			INSERT INTO ftpedilog (TRANSFER,ftpdate,filename,acct_name,TYPE) VALUES ("214-"+cCustName,dt2,cFilename,UPPER(cCustName),"214")
			USE IN ftpedilog
		ENDIF
	ENDIF

	IF lTesting
		tsubject = "SEARS 214 *TEST* EDI from TGF as of "+dtmail
	ELSE
		tsubject = "SEARS 214 EDI from TGF as of "+dtmail
	ENDIF

	tmessage = "214 ("+cProcType+") EDI Info from TGF-CA, Truck WO# "+cWO_Num+CHR(10)
	tmessage = tmessage + "for "+cMailName+"-"+ICASE(cRunType = "D","CHINASHIP",cRunType = "C","CMA/CG",cRunType = "V","EVERGREEN","MATSON")
	tmessage = tmessage +" has been created."+CHR(10)+"(Filename: "+cFilenameShort+")"
*!*	+CHR(10)+CHR(10)
*!*	tmessage = tmessage + "If you have any questions, please eMail or call 732-750-9000 x217."
	IF lTesting
		tmessage = tmessage + CHR(10)+CHR(10)+"This is a TEST RUN..."
	ENDIF

	SELECT 0
	USE F:\edirouting\ftpjobs
	INSERT INTO ftpjobs (jobname,USERID,jobtime) VALUES ("214-CHINASHIPPING","fmi",DATETIME())

	IF lEmail
		DO FORM dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
	ENDIF

	RELEASE ALL LIKE c_CntrlNum,c_GrpCntrlNum
	RELEASE ALL LIKE nOrigSeq,nOrigGrpSeq
	WAIT CLEAR
	WAIT WINDOW cMailName+" 214 "+cProcType+" EDI File output complete" AT 20,60 TIMEOUT 2

	closefiles()

CATCH TO oErr
	IF lDoCatch
		ASSERT .F. MESSAGE "In error catch"
		SET STEP ON
		lEmail = .T.
		cErrMsg = ""
		IF EMPTY(cFin_Status)
			cFin_Status = "ERRHAND ERROR"
		ELSE
			cErrMsg = cFin_Status
		ENDIF
		DO ediupdate WITH cFin_Status,.T.
		tsubject = cCustName+" Error ("+TRANSFORM(oErr.ERRORNO)+") at "+TTOC(DATETIME())
		tattach  = ""
		tsendto  = tsendtoerr
		tcc = tccerr
		IF cFin_Status = "MISS WOLOG RETURNED DT"
			tcc = tcctkrerr
		ENDIF
		tmessage = cCustName+" Error processing "+CHR(13)

		IF EMPTY(cErrMsg)
			tmessage =tmessage+ "Try/CATCH Exeception Message:"+CHR(13)+;
				[  Error: ] + STR(oErr.ERRORNO) +CHR(13)+;
				[  LineNo: ] + STR(oErr.LINENO) +CHR(13)+;
				[  Message: ] + oErr.MESSAGE +CHR(13)+;
				[  Procedure: ] + oErr.PROCEDURE +CHR(13)+;
				[  Details: ] + oErr.DETAILS +CHR(13)+;
				[  StackLevel: ] + STR(oErr.STACKLEVEL) +CHR(13)+;
				[  LineContents: ] + oErr.LINECONTENTS+CHR(13)+;
				[  UserValue: ] + oErr.USERVALUE
		ELSE
			tmessage =tmessage+cErrMsg+CHR(13)+"At WOLOG WO# "+cWO_Num
		ENDIF

		tsubject = "214 EDI Poller Error at "+TTOC(DATETIME())
		tattach  = ""
		tfrom    ="Toll EDI 214 Poller Operations <toll-edi-ops@tollgroup.com>"
		DO FORM dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
		lEmail = .F.
	ENDIF
FINALLY
	SET HOURS TO 12
	FCLOSE(nFilenum)
	closefiles()
ENDTRY

&& END OF MAIN CODE SECTION



****************************
PROCEDURE num_incr_isa
****************************

	IF !USED("serfile")
		USE ("f:\3pl\data\serial\"+cCustName+"214_serial") IN 0 ALIAS serfile
	ENDIF
	SELECT serfile
	IF lTesting
		nOrigSeq = serfile.seqnum
		nOrigGrpSeq = serfile.grpseqnum
		lISAFlag = .F.
	ENDIF
	nISA_Num = serfile.seqnum
	c_CntrlNum = ALLTRIM(STR(serfile.seqnum))
	REPLACE serfile.seqnum WITH serfile.seqnum + 1 IN serfile
	SELECT wolog
ENDPROC

****************************
PROCEDURE num_incr_st
****************************

	IF !USED("serfile")
		USE ("f:\3pl\data\serial\"+cCustName+"214_serial") IN 0 ALIAS serfile
	ENDIF
	SELECT serfile
	IF lTesting
		nOrigSeq = serfile.seqnum
		nOrigGrpSeq = serfile.grpseqnum
		lSTFlag = .F.
	ENDIF
	c_GrpCntrlNum = ALLTRIM(STR(serfile.grpseqnum))
	REPLACE serfile.grpseqnum WITH serfile.grpseqnum + 1 IN serfile
	SELECT wolog
ENDPROC

****************************
PROCEDURE segmentget
****************************

	PARAMETER thisarray,lcKey,nLength

	FOR i = 1 TO nLength
		IF i > nLength
			EXIT
		ENDIF
		lnEnd= AT("*",thisarray[i])
		IF lnEnd > 0
			lcThisKey =TRIM(SUBSTR(thisarray[i],1,lnEnd-1))
			IF OCCURS(lcKey,lcThisKey)>0
				RETURN SUBSTR(thisarray[i],lnEnd+1)
				i = 1
			ENDIF
		ENDIF
	ENDFOR

	RETURN ""

****************************
PROCEDURE close214
****************************
	STORE  "SE"+cfd+ALLTRIM(STR(nSegCtr+1))+cfd+PADL(c_GrpCntrlNum,9,"0")+csegd TO cString
	DO cstringbreak

	STORE  "GE"+cfd+ALLTRIM(STR(nSTCount))+cfd+c_CntrlNum+csegd TO cString
	DO cstringbreak

	STORE  "IEA"+cfd+"1"+cfd+PADL(c_CntrlNum,9,"0")+csegd TO cString
	DO cstringbreak

	RETURN

****************************
PROCEDURE cstringbreak
****************************
	cLen = LEN(ALLTRIM(cString))
	IF cRunType = 'C'
		FWRITE(nFilenum,cString)
	ELSE
		FPUTS(nFilenum,cString)
	ENDIF
	RETURN



**************************
PROCEDURE ediupdate
**************************
	PARAMETERS cFin_Status, lIsError,lFClose
	cRunVar1 = cRunType+"214"
	cRunVar2 = "214"+cRunType

	IF !lTesting
		SELECT edi_trigger
		LOCATE
		IF lIsError
			REPLACE edi_trigger.processed WITH .T.,edi_trigger.created WITH .F.,;
				edi_trigger.fin_status WITH cFin_Status,edi_trigger.errorflag WITH .T.;
				edi_trigger.when_proc WITH DATETIME() ;
				FOR edi_trigger.wo_num = nWO_Num AND INLIST(edi_type,cRunVar1,cRunVar2)
			IF nFilenum>0
				=FCLOSE(nFilenum)
			ENDIF
			IF FILE(cFilename)
				DELETE FILE &cFilename
			ENDIF
			closefiles()
		ELSE
			REPLACE edi_trigger.processed WITH .T.,edi_trigger.created WITH .T.,proc214 WITH .T.;
				edi_trigger.when_proc WITH DATETIME(),;
				edi_trigger.fin_status WITH cRunType+"214 CREATED";
				edi_trigger.errorflag WITH .F.,file214 WITH cFilename ;
				FOR edi_trigger.wo_num = nWO_Num AND INLIST(edi_type,cRunVar1,cRunVar2)
		ENDIF
	ENDIF
ENDPROC

*********************
PROCEDURE closefiles
*********************
	IF USED('ftpedilog')
		USE IN ftpedilog
	ENDIF
	IF USED('serfile')
		USE IN serfile
	ENDIF
	IF USED('wolog')
		USE IN wolog
	ENDIF
	IF USED('account')
		USE IN account
	ENDIF
	IF USED('xref')
		USE IN xref
	ENDIF
	IF USED('pudlocs')
		USE IN pudlocs
	ENDIF
	IF USED('ftpjobs')
		USE IN ftpjobs
	ENDIF
ENDPROC
