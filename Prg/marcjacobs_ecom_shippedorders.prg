*!* Marc Jacobs ECOMM "Shipped Orders Feed" Creation Program
*test(.t.,.t.)

Parameters lcBOL

MJDir = 'f:\ftpusers\mj_ecom\MJ_SO\'
RadialDir='f:\ftpusers\mj_ecom\Radial_SO\'

CREATE CURSOR shippedOrd;
	(RecordID C(2),;
	ExtWarehouseNum C(3),;
	FulfillmentLoc C(5),;
	OrdInvoiceNum C(14),;
	CustOrdNum C(30),;
	OrdLineNum C(4),;
	PartNum C(15),;
	Status C(1),;
	CancelQty N(7,0),;
	ShipQty N(7,0),;
	CancReasonCd C(2),;
	ShipDate C(14),;
	CarrierCode C(4),;
	CarrierMode C(6),;
	TrackingNo C(22),;	
	GiftCdID C(20),;
	SerialNo C(20),;
	CartonID C(20),;
	FileCreateDate C(14))
	
*cBOL = "1Z6E751A0300030642"

cProcName = "marcjacobs_ecom_shippedorders"

cInclude = "office = cOffice"


MJAcct    = 6303
MJOffice  = 'N'
MJMod     = 'J'

batchNo = PADL(TRANSFORM(dygenpk("mjecom","whj")),9,'0')

osSQL = "select " +;
         "       os.office, " +;
         "       os.shipins, " +;
         "       os.cnee_ref, " +;
         "       os.ship_ref, " +;
         "       os.bol_no, " +;
         "       od.linenum, " +;
         "       od.upc, " +;
         "       od.totqty, " +;
         "       od.wo_date, " +;
         "       os.scac " +;
         "from outship os, outdet od " +;
         "where os.outshipid = od.outshipid AND " +;
         "os.mod='"+MJMod+"' and os.bol_no='"+LcBOL+"'"   


lTesting = .t.

DO m:\dev\prg\_setvars WITH lTesting
cProgname = "mjecom_shippedorders_create"
ON ERROR DEBUG
SET ESCAPE ON
ON ESCAPE CANCEL
NormalExit = .T.

cUseFolder = "F:\whl\whdata\"

lnSlipAccount = 0

set step On 
	
IF xsqlexec(osSQL,"osd",,"wh") = 0
	WAIT WINDOW AT 10,10 "BOL not found in OUTSHIP:--> "+cBOL TIMEOUT 2
ENDIF
	
SELECT osd
thisCount = RECCOUNT()
	
SCAN
	lcQuery = "select * from cartons where ship_ref = '"+osd.ship_ref+"' AND upc = '"+osd.upc+"'"

	xsqlexec(lcQuery,"ctn",,"pickpack_sql5")

  	If Reccount("ctn")=0
		WAIT WINDOW AT 10,10 "PT "+osd.ship_ref+" UPC "+osd.upc+" Not found in CARTONS"
    	return
	ENDIF 
		
	If !lTesting 
		lcQuery="select * from shipment where office='"+osd.office+"' AND pickticket='"+osd.ship_ref+"'"
		xsqlexec(lcQuery,"shp",,"wh") 

  		If Reccount("shp")= 0
			WAIT WINDOW AT 10,10 "PT "+osd.ship_ref+" Not found in SHIPMENT" 
    		return
		ENDIF 		

		lcQueryL="select * from package where shipmentid = "+TRANSFORM(shp.shipmentid)
		xsqlexec(lcQuery,"pkg",,"wh")
  		If Reccount("pkg") = 0
			WAIT WINDOW AT 10,10 "Shipment ID "+shp.shipmentid+" Not found in PACKAGE" 
    		return
		ENDIF 			
	Endif 
	
	SELECT ctn
	SCAN 
	
		SELECT shippedOrd
		APPEND BLANK 
		REPLACE recordID WITH 'D2' IN shippedOrd
		
********** COMMENTED OUT DURING TESTING - UNCOMMENT WHEN USABLE DATA AVAILABLE
*!*		REPLACE ExtWarehouseNum WITH getmemodata("shipins", "EXTWHNO") IN shippedOrd
*!*		REPLACE FulfillmentLoc  WITH getmemodata("shipins", "FULFILLOC") IN shippedOrd
***********************************************************************************
		REPLACE ExtWarehouseNum WITH '419' IN shippedOrd
		REPLACE FulfillmentLoc WITH '00419' IN shippedOrd
************************************************************************************
		
		REPLACE ShipDate WITH TTOC(osd.wo_date,1) IN shippedOrd
		REPLACE OrdInvoiceNum WITH osd.ship_ref IN shippedOrd
		REPLACE CustOrdNum WITH osd.cnee_ref IN shippedOrd
	
		*SET STEP ON 
	
		x1 = VAL(osd.linenum)
		x2 = TRANSFORM(VAL(osd.linenum))
		x3 = PADL(TRANSFORM(VAL(osd.linenum)),4,'0')	
	
		REPLACE OrdLineNum WITH PADL(TRANSFORM(VAL(osd.linenum)),4,'0') IN shippedOrd
		REPLACE PartNum WITH osd.upc IN shippedOrd
		REPLACE CarrierCode WITH getmemodata("osd.shipins", "CARRIERCODE") IN shippedOrd
		REPLACE CarrierMode WITH getmemodata("osd.shipins", "CARRIERMODE") IN shippedOrd

		IF ctn.ucc != 'CUTS'
			REPLACE Status WITH 'S' in shippedOrd
			REPLACE CancelQty WITH 0 in shippedOrd
			REPLACE ShipQty WITH ctn.totqty in shippedOrd
			If !lTesting 
				REPLACE TrackingNo WITH pkg.trknumber in shippedOrd
			Else
  				REPLACE TrackingNo WITH lcBol in shippedOrd
			Endif 

			REPLACE CartonID WITH ctn.ucc in shippedOrd
		ELSE 
			REPLACE Status WITH 'X' in shippedOrd
			REPLACE CancelQty WITH ctn.totqty in shippedOrd
			REPLACE ShipQty WITH 0 in shippedOrd
			REPLACE CancReasonCd WITH 'IO' in shippedOrd
		ENDIF 

		REPLACE FileCreateDate WITH TTOC(DATETIME(),1)in shippedOrd
	ENDSCAN 
ENDSCAN 

SELECT shippedOrd
SET STEP ON 

recCnt = RECCOUNT()
trailerRec = "T2|" + TRANSFORM(recCnt+1) + '|' + batchNo

RoutFileName = RadialDir + 'ESSSHP' + TTOC(DATETIME(),1) + '.dat'
MoutFileName = MJDir + 'ESSSHP' + TTOC(DATETIME(),1) + '.dat'

COPY TO h:\fox\MJtest.dat DELIMITED WITH "" WITH CHARACTER '|'
lcStr= Filetostr("h:\fox\MJtest.dat")
lcStr = lcStr+trailerRec
Strtofile(lcStr,(RoutFileName))
Strtofile(lcStr,(MoutFileName))
DELETE FILE h:\fox\MJtest.dat
WAIT WINDOW " " TIMEOUT 2


IF !USED("edi_trigger")
	USE F:\3pl\DATA\edi_trigger IN 0
ENDIF

SELECT edi_trigger

REPLACE edi_trigger.processed WITH .T.,edi_trigger.proc945 WITH .T.,edi_trigger.file945 WITH RoutFileName,;
				edi_trigger.fin_status WITH "SHIPPED ORD FILE CREATED",edi_trigger.errorflag WITH .F.,edi_trigger.when_proc WITH DATETIME() ;
				FOR edi_trigger.bol = lcBOL AND edi_trigger.accountid = MJAcct AND edi_type = "945E"
				
USE IN edi_trigger
