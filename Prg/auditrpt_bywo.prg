Parameters workorder,Printflag

Public array apos[20,1]
Public wonum,whichpallet,Whichstyle,whichpack,whichcarton,cartonstatus,polystatus,innerstatus,whichuser,sumdata

For i = 1 to 20
  apos[i,1]=""
next

=Seek(workorder,"inwolog","wo_num")
If Found()
  lcMoretRefNum = inwolog.acct_ref
  lcContainer = inwolog.container
Else
  lcMoretRefNum =""
  lcContainer=""
EndIf 

Select style,echo,space(12) as ponum from indet where !Empty(echo) and wo_num = workorder into cursor temp readwrite
Select temp
poctr = 1
Scan
  tpo = getmemodata("echo","PONUM")
  replace ponum with tpo
  If Ascan(apos,tpo) =0
    apos[poctr,1]=tpo
    poctr= poctr+1
  endif
EndScan


Select qcscans
Set Order To wo_num
Locate for wo_num = workorder
replace all ponum with "" for wo_num = workorder



Select qcscans 
Set Order To wo_style

Select temp
Scan
  lcSeekStr =Str(workorder)+temp.style
  
  =Seek(lcSeekStr,"qcscans","wo_style")
  if Found("qcscans")
    Select qcscans
    replace ponum with temp.ponum for wo_num = workorder and style=temp.style in qcscans
  EndIf 
  
EndScan 



&&close data all 



if InList(Upper(vartype(printflag)),"L","U")
  SendtoPrinter = .f.
Else
  SendtoPrinter = .t.
EndIf 


If !Used("qcscans")
  Use f:\wh\qcscans In 0
EndIf

If !Used("moretdata")  
  Use f:\wh\moretdata In 0
EndIf   

Create Cursor rpt (;
  group char(1),;
  rdata char(90))

Create Cursor rpt2 (;
  rdata char(90))

*!*  if InList(Upper(vartype(cartontotest)),"L","U")
*!*    thiscarton = getwonumber()
*!*    whichcarton = thiscarton
*!*  Else
*!*    thiscarton = cartontotest
*!*    whichcarton = thiscarton
*!*  EndIf 

Select rpt 
Zap
Select rpt2
Zap

cartonstatus = "PASSED"
polystatus   = "PASSED"  
innerstatus  = "PASSED"

cartonpassed = .t.
polypassed   = .t.
innerpassed  = .t.

Select qcscans

Select cartonid,style from qcscans where wo_num = workorder and polyscan into cursor ctns group by cartonid order by cartonid
 
Select cartonid,style,errorflag,errormsg,scanval,qty,expqty,whichpoly from qcscans where wo_num = workorder and polyscan and errorflag into cursor polyerrors order by cartonid

Select cartonid,style,errorflag,errormsg,scanval,qty,expqty,whichpoly from qcscans where wo_num = workorder and !polyscan and errorflag into cursor innererrors order by cartonid

If Reccount("ctns") =0
  MessageBox("No CTNs on this work order........",0,"Moret QC Audit")
  return
endif

Select rpt
append blank
Replace rdata With "Carton Level Detail Data"

Select ctns
Scan
  Select rpt
  Append blank
  Replace rdata With "------------------------------------------------------------------------"
  Append blank
  Replace rdata With "Carton# "+Alltrim(ctns.cartonid)+"  ->"+ctns.style

  Select polyerrors
  Count for errorflag = .t. and cartonid = ctns.cartonid to num_ctn_errors
  If num_ctn_errors = 0
    Select rpt
    Append Blank
    Replace rdata With Alltrim(rdata)+ "--> No Poly Errors   "
  else
    Select polyerrors
    Scan for polyerrors.cartonid = ctns.cartonid
       Select rpt
      Append blank
      Do case
        Case polyerrors.errormsg = "LABEL ISSUE"
          Replace rdata With "Poly Errors: "+Alltrim(polyerrors.errormsg)+" --> "+Alltrim(polyerrors.scanval)
        Case polyerrors.errormsg = "UNEXPECTED VALUE"
          Replace rdata With "Poly Errors: "+Alltrim(polyerrors.errormsg)+" was-> "+Alltrim(polyerrors.scanval)+"  should be --> "+Alltrim(polyerrors.style)
        Case polyerrors.errormsg = "TOO FEW"
          Replace rdata With "Poly Errors: "+Alltrim(polyerrors.errormsg)+" was-> "+Alltrim(Str(polyerrors.qty))+"  should be --> "+Alltrim(Str(polyerrors.expqty))          
        Case polyerrors.errormsg = "TOO MANY"
          Replace rdata With "Poly Errors: "+Alltrim(polyerrors.errormsg)+" was-> "+Alltrim(Str(polyerrors.qty))+"  should be --> "+Alltrim(Str(polyerrors.expqty))          
      endcase 
    EndScan 
  EndIf 
  Select innererrors
  Count for errorflag = .t. and cartonid = ctns.cartonid to num_inner_errors
  If num_inner_errors = 0
    Select rpt
    Replace rdata With Alltrim(rdata)+ "--> No Inner Errors   "
  else
    Select innererrors
    Scan for innererrors.cartonid = ctns.cartonid
      Select rpt
      Append blank
      Do case
        Case innererrors.errormsg = "LABEL ISSUE"
          Replace rdata With "Inner Errors: "+Alltrim(innererrors.errormsg)+" --> "+Alltrim(innererrors.scanval)
        Case innererrors.errormsg = "UNEXPECTED VALUE"
          Replace rdata With "Inner Errors: "+Alltrim(innererrors.errormsg)+" was-> "+Alltrim(innererrors.scanval)+" in style --> "+Alltrim(innererrors.style    )
        Case innererrors.errormsg = "TOO FEW"
          Replace rdata With "Inner Errors: "+Alltrim(innererrors.errormsg)+" was-> "+Alltrim(Str(innererrors.qty))+" should be --> "+Alltrim(Str(innererrors.expqty))          
        Case innererrors.errormsg = "TOO MANY"
          Replace rdata With "Inner Errors: "+Alltrim(innererrors.errormsg)+" was-> "+Alltrim(Str(innererrors.qty))+" should be --> "+Alltrim(Str(innererrors.expqty))          
      
      endcase 
    EndScan 
  EndIf 
    
EndScan

Select rpt
replace all group with "1"

*workorder = 6146562
Select * from qcscans where wo_num = workorder into cursor summ

Select cartonid from summ where polyscan=.t. into cursor ctnchk group by cartonid
ctnschecked = Reccount("ctnchk")

Select cartonid,errorflag  from summ where polyscan=.t. And errorflag = .t. into cursor ctnsfail group by cartonid,errorflag
ctnsfailed = Reccount("ctnsfail")

Select summ
Select cartonid,whichpoly from summ where !polyscan into cursor innerchk group by cartonid,whichpoly
innerstested = Reccount("innerchk")

Select summ
Select cartonid,scanval,errorflag,whichpoly from summ where !polyscan and errorflag =.t. into cursor innerfail group by cartonid,scanval,errorflag,whichpoly
innersfailed = Reccount("innerfail")

Select summ
Count to lnBadPolyLabel         for Polyscan and Errormsg                    = "BAD LABEL"
Count to lnUnexpectedPolyLabel  for Polyscan and Errormsg                    = "UNEXPECTED VALUE"
Count to lnPolyShort            for Polyscan and whichpoly = 0 and Errormsg  = "TOO FEW"
Count to lnPolyOver             for Polyscan and whichpoly = 0 and Errormsg  = "TOO MANY"

Count to lnBadlabels            for !Polyscan and Errormsg                   = "UNEXPECTED VALUE"
Count to lnInnerShort           for !Polyscan and whichpoly = 0 and Errormsg = "TOO FEW"
Count to lnInnerOver            for !Polyscan and whichpoly = 0 and Errormsg = "TOO MANY"
Count to lnEachesShort          for !Polyscan and qty < expqty  and Errormsg = "TOO FEW"
Count to lnEachesOver           for !Polyscan and qty > expqty  and Errormsg = "TOO MANY"
Count to lnUnexpectedInnerLabel for !Polyscan and Errormsg                   = "BAD LABEL"


Select rpt
Append blank
Insert into rpt (rdata) values ("===================================================")
Insert into rpt (rdata) values ("Summary Carton\Poly Data")
Insert into rpt (rdata) values ("===================================================")
Insert into rpt (rdata) values ("Cartons Checked : "+Transform(ctnschecked))
Insert into rpt (rdata) values ("Cartons Failed  : "+Transform(ctnsfailed))
Insert into rpt (rdata) values ("Poly Label Issue: "+Transform(lnBadPolyLabel))
Insert into rpt (rdata) values ("Poly Short      : "+Transform(lnPolyShort))
Insert into rpt (rdata) values ("Poly Over       : "+Transform(lnPolyOver))
Insert into rpt (rdata) values ("Unexpected Poly : "+Transform(lnUnexpectedPolyLabel))
Append blank
Insert into rpt (rdata) values ("===================================================")
Insert into rpt (rdata) values ("Summary Inner Data")
Insert into rpt (rdata) values ("===================================================")
Insert into rpt (rdata) values ("Inners Checked  : "+Transform(innerstested))
Insert into rpt (rdata) values ("Inner  Failures : "+Transform(innersfailed))
Insert into rpt (rdata) values ("InnerLabel Issue: "+Transform(lnBadlabels ))
Insert into rpt (rdata) values ("Inner Short     : "+Transform(lnInnerShort))
Insert into rpt (rdata) values ("Inner Over      : "+Transform(lnInnerOver))
Insert into rpt (rdata) values ("Eaches Short    : "+Transform(lnEachesShort))
Insert into rpt (rdata) values ("Eaches Over     : "+Transform(lnEachesOver ))
Insert into rpt (rdata) values ("Unexpected Eachs: "+Transform(lnUnexpectedInnerLabel))

replace all group with "2" for group !="1"
sumdata=""
Select rpt
Scan for group="2"
  sumdata = sumdata+rdata+chr(13)
EndScan 

*Select rpt
*Delete For group="2"


RPTOK = .t.
Select rpt
workorder = Transform(workorder)
Report Form auditrpt_bywo preview
Return




Select pscans
Goto top

Select moretdata
Set Order To styleplus
lcSeek = Padr(Alltrim(pscans.style),20," ")
Select moretdata
Seek lcSeek

sumdata =""
If Found()
  Sum qty to totinners for styleplus = lcSeek
  Select moretdata
  Scan for styleplus = lcSeek
 *   Select rpt2
 *   Append blank
    If Empty(sumdata)
      sumdata = "UPC: "+moretdata.upc+"  Qty: "+Transform(moretdata.qty)+"  Size: "+Alltrim(moretdata.size)
    Else
      sumdata = sumdata+Chr(13)+"UPC: "+moretdata.upc+"  Qty: "+Transform(moretdata.qty)+"  Size: "+Alltrim(moretdata.size)
    EndIf 
  EndScan
Else
  Select moretdata
  Set Order To STYLE   && STYLE
  lcSeek = Padr(Alltrim(pscans.style),20," ")
  Select moretdata
  Seek lcSeek
  If Found()
    Sum qty to totinners for styleplus = lcSeek
  Else
    MessageBox("Inner data not found in Moretdata...",0,"QC Audit")
    return
  EndIf  
EndIf 



Select pscans
Goto top
totinners_expected = totinners*Val(Alltrim(pscans.pack))

Count for errorflag = .t. to badpolys
If badpolys > 0
  cartonpassed = .f.
  polypassed = .f.
EndIf 

Count for scanval = "NOLABEL" AND errorflag = .t. to nolabels
If nolabels > 0
  cartonpassed = .f.
  polypassed = .f.
EndIf 

Count for scanval = "BADLABEL" AND errorflag = .t. to badlabels
If badlabels > 0
  cartonpassed = .f.
  polypassed = .f.
EndIf 

Count for errorflag = .t. and errormsg ="UNEXPECTED VALUE" to unexpected
If unexpected > 0
  cartonpassed = .f.
  polypassed = .f.
EndIf 


Select innerscans
Count for errorflag = .t. and !polyscan to badinners

If badinners > 0
  cartonpassed = .f.
  innerpassed = .f.
EndIf 

Count for scanval = "NOLABEL" AND errorflag = .t. to innernolabels
If nolabels > 0
  cartonpassed = .f.
  polypassed = .f.
EndIf 

Count for scanval = "BADLABEL" AND errorflag = .t. to innerbadlabels
If badlabels > 0
  cartonpassed = .f.
  polypassed = .f.
EndIf 

Count for errorflag = .t. and errormsg ="UNEXPECTED VALUE" to innerunexpected
If unexpected > 0
  cartonpassed = .f.
  polypassed = .f.
EndIf 

Count for errorflag = .t. and !Empty(inspecterr) to inspecterrors
If inspecterrors > 0
  cartonpassed = .f.
  polypassed = .f.
EndIf 


Select pscans
Goto top
wonum       = Transform(pscans.wo_num)
whichpallet = pscans.palletid
whichstyle  = pscans.style
whichpack   = pscans.pack
whichuser   = pscans.userid

Select pscans
Goto top
expectedpolys = Val(Alltrim(pscans.pack))

polycount = "OK"
Count for !errorflag to numpolys
If numpolys > expectedpolys
  polycount ="Over "+Transform(numpolys-expectedpolys)
EndIf

If numpolys < expectedpolys
  polycount ="Short "+Transform(expectedpolys-numpolys)
EndIf

Select pscans
Goto top

If numpolys != Val(Alltrim(pscans.pack))
  cartonpassed = .f.
  polypassed   = .f.
endif 

Select innerscans
Sum qty to totinner_scans for !errorflag

innercount = "OK"

If totinner_scans > totinners_expected
  innercount ="Over "+Transform(totinner_scans-totinners_expected)
EndIf

If totinner_scans < totinners_expected
  innercount ="Short "+Transform(totinners_expected-totinner_scans)
EndIf

If totinner_scans != totinners_expected
  cartonpassed = .f.
  innerpassed  = .f.
endif 


Select rpt
Append blank
Replace rdata With "Summary Poly Level Data"
Append blank
Replace rdata With "------------------------------------------------------------------------"
Append blank
Replace rdata With "PolyScans  Expected: "+Transform(expectedpolys)+ " Scanned Polys: "+Transform(numpolys)
Append blank
Replace rdata With "Polyscan Errors   : "+Transform(badpolys)
Append blank
Replace rdata With "Bad Label Errors   : "+Transform(badlabels)
Append blank
Replace rdata With "No Label  Errors   : "+Transform(nolabels)
Append blank
Replace rdata With "Unexpected Scans   : "+Transform(unexpected)
Append blank
Replace rdata With "PolyCount Errors  : "+polycount


Select rpt
Append blank
Replace rdata With "------------------------------------------------------------------------"
Append blank
Replace rdata With "Summmary InnerPoly Level Data"
Append blank
Replace rdata With "------------------------------------------------------------------------"
Append blank
Replace rdata With "Total Inners Expected: "+Transform(totinners_expected)+ " Scanned Inners: "+Transform(totinner_scans)
Append blank
Replace rdata With "Innerscan Errors   : "+Transform(badinners)
Append blank
Replace rdata With "InnerCount Errors  : "+innercount
Append blank
Replace rdata With "Bad Label Errors   : "+Transform(innerbadlabels)
Append blank
Replace rdata With "No Label  Errors   : "+Transform(innernolabels)
Append blank
Replace rdata With "Unexpected Scans   : "+Transform(innerunexpected)
Append blank
Replace rdata With "Inspection Errors  : "+Transform(inspecterrors)


Select rpt
Append blank
Select rpt
Append blank
Replace rdata With "------------------------------------------------------------------------"
Append blank
Replace rdata With "Poly Detail Level Data"
Append blank
Replace rdata With "------------------------------------------------------------------------"

Select pscans
Scan for errorflag
  Select rpt
  Append Blank
  replace rdata with "Error at Poly#: "+Transform(pscans.whichpoly)+" Expected: "+pscans.style+" Scanned: "+pscans.scanval
EndScan 



Select rpt
Append blank
Select rpt
Append blank
Replace rdata With "------------------------------------------------------------------------"
Append blank
Replace rdata With "Inner Detail Level Data"
Append blank
Replace rdata With "------------------------------------------------------------------------"
Select innerscans
Scan for errorflag

  If innerscans.expqty != innerscans.qty and innerscans.expqty > innerscans.qty
    Select rpt
    Append Blank
    replace rdata with "QTY Error (short) at Poly#: "+Transform(innerscans.whichpoly)+" Exp Qty: "+Transform(innerscans.expqty)+" Scanned: "+Transform(innerscans.qty)+" ("+Alltrim(innerscans.scanval)+")"
  EndIf   
  If innerscans.expqty != innerscans.qty and innerscans.expqty < innerscans.qty
    Select rpt
    Append Blank
    replace rdata with "QTY Error (over) at Poly#: "+Transform(innerscans.whichpoly)+" Exp Qty: "+Transform(innerscans.expqty)+" Scanned: "+Transform(innerscans.qty)+" ("+Alltrim(innerscans.scanval)+")"
  EndIf   
  If innerscans.errormsg = "UNEXPECTED VALUE"
    Select rpt
    Append Blank
    replace rdata with "UPC Error at Poly#: "+Transform(innerscans.whichpoly)+" Scanned: "+innerscans.scanval
  EndIf   
  If innerscans.errormsg = "LABEL ISSUE"
    Select rpt
    Append Blank
    replace rdata with innerscans.scanval+" at Poly: "+Transform(innerscans.whichpoly)+" Scanned: "+innerscans.scanval
  EndIf   
  If !Empty(innerscans.inspecterr)
    Select rpt
    Append Blank
    replace rdata with "Inspection Error at Poly#: "+Transform(innerscans.whichpoly)+" "+innerscans.inspecterr+" "+innerscans.comment
  EndIf   
  Select innerscans

EndScan 

If cartonpassed = .f.
  cartonstatus = "FAILED"
EndIf 
If polypassed = .f.  
  polystatus   = "FAILED"  
EndIf   
If innerpassed = .f.
  innerstatus  = "FAILED"
EndIf 



RPTOK = .t.
Select rpt
Set Step On 
Report Form auditrpt preview

Return



If sendtoPrinter = .t.
  Report Form auditrpt to printer
Else
  lcReportname = "m:\dev\rpt\auditrpt"
  lcfilename = "h:\fox\"+whichcarton+".pdf"
  Store  .F. TO REPORTOK
  do rpt2pdf with lcReportname,lcFilename,REPORTOK
  if  REPORTOK = .F.
    =MESSAGEBOX("PDF file not created",48,"PDF Creator")
  EndIf 
endif 

status = cartonstatus

Return cartonstatus


************************************************************
*  FUNCTION getwonumber()
************************************************************
FUNCTION getwonumber()
LOCAL lnWo,lnRetVal
lcRetval=""
 DO WHILE .T.
   lcBOL=INPUTBOX('Carton#',"Enter a Cartons.....")   && WO num has to be an integer
   lcRetVal=lcBOL
    IF Empty(lcBOL)  && either nothing was put into the box or cancel was pressed
      lnAnswer=MESSAGEBOX("There is no WO  entered or Cancel was pressed"+ CHR(13)+;
                 "   Do you want to exit program ?",36)
      IF lnAnswer=6  && yes exit
       RETURN lcRetval  && return 0
      ELSE  && try again
       LOOP
      Endif
    ENDIF
    RETURN lcRetVal
 ENDDO
EndFunc
************************************************************
