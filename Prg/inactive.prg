* remove inactive accounts, billtos, acctbills, rates, users
* check for some dups

utilsetup("INACTIVE")

gprocess=""
gdevelopment=.f.
guserid="INACTIVE"

* inactivate accounts

xsqlexec("select * from account where inactive=0","account",,"qq")
index on accountid tag accountid
set order to

xsqlexec("select * from acctbill","acctbill",,"qq")
index on billtoid tag billtoid
index on accountid tag accountid
index on STR(billtoid,4)+STR(accountid,4) tag billacct

use f:\wo\wodata\wolog in 0

if usesqlbl()
else
	use f:\wo\wodata\bl in 0
endif

xsqlexec("select * from whoffice",,,"wh")

xsqlexec("select wo_num, accountid, invnum, invdt from invoice",,,"ar")
index on invnum tag invnum
set order to

xsqlexec("select * from farhdr",,,"ar")

select ;
	accountid, acctname, 00000000.00 as balancedue, ttod(adddt) as adddt, "    " as lastwotype, {} as lastwodt, ;
	{} as lastinvdt, "    " as lastaction, {} as lastdt, broker, .f. as inactivate, .f. as new ;
	from account ;
	where !inlist(accountid,9990,9999) ;
	and !inactive ;
	and updatedt<date()-30 ;
	into cursor xinv readwrite

index on accountid tag accountid 

? "balance"

taccountid="%"
tbilltoid="%"
gaccountid=0
goffsetsince={}

arcalc(.t.)

select accountid, sum(owed) as owed from xrpt group by 1 into cursor xtemp
scan 
	=seek(accountid,"xinv","accountid")
	replace balancedue with xtemp.owed in xinv
endscan

? "invoice"

xsqlexec("select accountid, invdt from invoice where void=0",,,"ar")
select accountid, max(invdt) as invdt from invoice group by 1 into cursor xtemp
scan 
	=seek(accountid,"xinv","accountid")
	replace lastinvdt with xtemp.invdt in xinv
endscan

? "wolog"

select accountid, max(wo_date) as wo_date from wolog group by 1 into cursor xtemp
scan 
	=seek(accountid,"xinv","accountid")
	replace lastwodt with xtemp.wo_date, lastwotype with "WO" in xinv
endscan

? "fxwolog"

xsqlexec("select accountid, max(wo_date) as wo_date from fxwolog group by accountid","xtemp",,"fx")
scan
	=seek(accountid,"xinv","accountid")
	if xinv.lastwodt<xtemp.wo_date
		replace lastwodt with xtemp.wo_date, lastwotype with "FX" in xinv
	endif
endscan

? "outbound to consolidators"

if usesqlbl()
	xsqlexec("select accountid, pu_date from bl",,,"wo")
	select accountid, max(pu_date) as pu_date from bl group by 1 into cursor xtemp
else
	select accountid, max(pu_date) as pu_date from bl group by 1 into cursor xtemp
endif

scan
	=seek(accountid,"xinv","accountid")
	if xinv.lastwodt<xtemp.pu_date
		replace lastwodt with xtemp.pu_date, lastwotype with "OB" in xinv
	endif
endscan

select whoffice
scan for !test
	goffice=whoffice.office
	? "inven-"+whoffice.office
	
	xsqlexec("select * from inven where mod='"+goffice+"'",,,"wh")

	select accountid, max(date_rcvd) as date_rcvd from inven group by 1 into cursor xtemp
	scan
		=seek(accountid,"xinv","accountid")
		if xinv.lastwodt<xtemp.date_rcvd
			replace lastwodt with xtemp.date_rcvd, lastwotype with "WH-"+whoffice.office in xinv
		endif
	endscan

	use in inven
endscan

set step on 

? "process"

select xinv
scan 
	if empty(lastwodt) and empty(lastinvdt)
		replace lastdt with adddt, lastaction with "ADD"
	else
		if lastinvdt>lastwodt
			replace lastdt with lastinvdt, lastaction with "INV"
		else
			replace lastdt with lastwodt, lastaction with lastwotype
		endif
	endif
endscan

select xinv
replace inactivate with .t. for lastdt<date()-360 and balancedue=0
replace inactivate with .t. for lastdt<date()-360 and balancedue=0 and broker	&& dy 12/20/15 per Jim C

select xinv
scan for inactivate and !inlist(accountid,6492,6621,6601,5318,6680,6304,6572,6689,6666,6719,6746,6748,6749)	&& don't inactivate these accounts
	=seek(accountid,"account","accountid")
	if !account.inactive
		replace new with .t. in xinv
		replace inactive with .t., inactivedt with date() in account
		xsqlexec("update account set inactive=1 where accountid="+transform(account.accountid),,,"qq")
	endif
endscan

select xinv
index on lastdt tag lastdt
copy to f:\auto\inactive

count for new
if _tally>0
	email("Dyoung@fmiint.com","INACTIVE: "+transform(_tally)+" accounts inactivated","inactive",,,,.t.,,,,,.t.)
endif

* delete inactive acctbills & billtos

close databases all

xsqlexec("select * from acctbill","acctbill",,"qq")
index on billtoid tag billtoid
index on accountid tag accountid
index on STR(billtoid,4)+STR(accountid,4) tag billacct

xsqlexec("select * from billto","billto",,"qq")
index on billtoid tag billtoid
set order to

xsqlexec("select * from rateacct",,,"ar")
index on accountid tag accountid
set order to

xsqlexec("select * from rate",,,"ar")
index on rateheadid tag rateheadid
index on billtoid tag billtoid
set order to

xsqlexec("select accountid, billtoid, invdt from invoice",,,"ar")
index on accountid tag accountid
index on billtoid tag billtoid
set order to

use f:\wo\wodata\wolog in 0

select * from acctbill into cursor xacctbill where .f. readwrite
select * from billto into cursor xbillto where .f. readwrite

select acctbill
scan for adddt<gomonth(date(),-24) and !main
	select invoice
	locate for accountid=acctbill.accountid and billtoid=acctbill.billtoid and invdt>gomonth(date(),-24)
	if !found()
		select wolog
		locate for accountid=acctbill.accountid and billtoid=acctbill.billtoid and wo_date>gomonth(date(),-24)
		if !found()
			xfound=.f.			
			select rateacct
			locate for accountid=acctbill.accountid 
			select rate
			locate for rateheadid=rateacct.rateheadid and billtoid=acctbill.billtoid and (emptynul(cancels) or cancels>date())
			if !found()
				select acctbill
				scatter memvar
				insert into xacctbill from memvar
				replace inactive with .t. in acctbill
				delete in acctbill
			endif
		endif
	endif
endscan

select xacctbill
count
if _tally>0
	copy to f:\auto\xacctbill
	email("Dyoung@fmiint.com","INACTIVE: "+transform(_tally)+" acctbills inactivated","xacctbill",,,,.t.,,,,,.t.)
endif

*!*	select billto
*!*	scan for adddt<gomonth(date(),-24)
*!*		select acctbill
*!*		locate for billtoid=billto.billtoid and !inactive
*!*		if !found()
*!*			select billto
*!*			scatter memvar
*!*			insert into xbillto from memvar
*!*			replace inactive with .t. in billto
*!*			delete in billto
*!*		endif
*!*	endscan

*!*	select xbillto
*!*	count 
*!*	if _tally>0
*!*		copy to f:\auto\xbillto
*!*		email("Dyoung@fmiint.com","INACTIVE: "+transform(_tally)+" billtos inactivated","xbillto",,,,.t.,,,,,.t.)
*!*	endif


* remove ratehead and rates for inactive accounts

close databases all 

xsqlexec("select * from account where inactive=0","account",,"qq")
index on accountid tag accountid
set order to

xsqlexec("select * from rateacct","rateacct",,"ar")
index on accountid tag accountid
set order to

xsqlexec("select * from rate",,,"ar")
index on rateheadid tag rateheadid
set order to

useca("ratehead","ar",.t.)

create cursor xrpt (rateheadid i, acctname c(30))
index on acctname tag acctname

select ratehead
scan for !inactive
	xinactive=.t.

	select rateacct
	scan for rateheadid=ratehead.rateheadid
		=seek(accountid,"account","accountid")
		if !account.inactive
			xinactive=.f.
			exit
		endif
	endscan

	if xinactive
		replace inactive with .t. in ratehead
		replace cancelbyinactivedt with date() in rate for rateheadid=ratehead.rateheadid and empty(cancelbyinactivedt)
		insert into xrpt (rateheadid, acctname) values (ratehead.rateheadid, ratehead.acctname)
		tu("ratehead")
	endif
endscan

select xrpt
if reccount()>0
	copy to ("f:\auto\xinactiveaccountrates")
	email("Dyoung@fmiint.com","INACTIVE: "+transform(reccount())+" ratehead and rates removed for inactive accounts","xinactiveaccountrates",,,,.t.,,,,,.t.)
endif

use in account
use in rate
use in ratehead

* check for inactive accounts in rateacct

*!*	valset("check for inactive accounts in rateacct")

*!*	use f:\watusi\ardata\rateacct in 0
*!*	use f:\watusi\ardata\ratehead in 0

*!*	xsqlexec("select * from account","account",,"qq")

*!*	select rateheadid, rateacctid, rateacct.accountid, rateacct.acctname ;
*!*		from rateacct, account ;
*!*		where rateacct.accountid=account.accountid ;
*!*		and account.inactive ;
*!*		into cursor xrpt readwrite

*!*	*!*	select xrpt
*!*	*!*	scan 
*!*	*!*		=seek(rateheadid,"ratehead","rateheadid")
*!*	*!*		if ratehead.inactive
*!*	*!*			delete in xrpt
*!*	*!*		else
*!*	*!*			delete in rateacct for rateacctid=xrpt.rateacctid
*!*	*!*		endif
*!*	*!*	endscan

*!*	select xrpt
*!*	count to aa

*!*	if aa>0
*!*		copy to f:\auto\xinactiverateacct
*!*		email("Dyoung@fmiint.com","INACTIVE: "+transform(aa)+" inactive accounts in rateacct","xinactiverateacct",,,,.t.,,,,,.t.)
*!*	endif

* delete inactive users

close databases all

use f:\main\userpwrd in 0
use f:\mm\activity in 0

select .f. as run, .f. as deleted, userid, office, forklift, autosystem, lastlogin from userpwrd ;
	where userid#"MARK " and updatedt<date()-30 ;
	into cursor xrpt readwrite
	
index on userid tag userid

select userid from activity where zdatetime>gomonth(date(),-4) group by 1 into cursor xact
scan 
	if seek(userid,"xrpt","userid")
		replace run with .t. in xrpt
	endif
endscan

select xrpt
scan for !run
	if seek(userid,"userpwrd","userid")
		if empty(lastlogin) or lastlogin<date()-120
			replace inactivedt with date() in userpwrd
			delete in userpwrd
			replace deleted with .t. in xrpt
		endif
	endif
endscan

select xrpt
count for deleted to aa
if aa>0
	copy to ("f:\auto\xinactiveusers") for deleted
	email("Dyoung@fmiint.com","INACTIVE: "+transform(aa)+" users inactivated","xinactiveusers",,,,.t.,,,,,.t.)
endif

use in userpwrd
use in activity

* check for duplicate user names in userpwrd

*!*	close databases all

*!*	use f:\main\userpwrd in 0

*!*	select user_name, userid from userpwrd group by 1 where active and !inlist(user_name,"CARLOS VEGA","CESAR LOZADA","JUAN GONZALEZ") having count(1)>1 into cursor xrpt

*!*	if reccount()>0
*!*		copy to ("f:\auto\xdupusername") 
*!*		email("Dyoung@fmiint.com","INACTIVE: "+transform(reccount())+" duplicate user names in userpwrd","xdupusername",,,,.t.,,,,,.t.)
*!*	endif

* check for inactive warehouse accounts in upsacct

valset("check for inactive warehouse accounts in upsacct")

xsqlexec("select * from account where inactive=0","account",,"qq")
index on accountid tag accountid
set order to

select whoffice
scan for !test and !inlist(office,"8","F","L","J")
	xoffice=whoffice.office
	xfolder=whoffice.folder+"\whdata\"
	
	xsqlexec("select * from upsacct where mod='"+xoffice+"'","upsacct",,"wh")

	select .f. as missing, * from upsacct where .f. into cursor xrpt readwrite
	
	do case
	case whoffice.rateoffice="N"
		xfilter="account.njinven"
	case whoffice.rateoffice="C"
		xfilter="account.cainven"
	case whoffice.rateoffice="L"
		xfilter="account.mlinven"
	case whoffice.rateoffice="M"
		xfilter="account.flinven"
	case whoffice.rateoffice="R"
		xfilter="account.riinven"
	case whoffice.rateoffice="K"
		xfilter="account.kyinven"
	case whoffice.rateoffice="O"
		xfilter="account.oninven"
	case whoffice.rateoffice="X"
		xfilter="account.xxinven"
	case whoffice.rateoffice="Y"
		xfilter="account.yyinven"
	case whoffice.rateoffice="Z"
		xfilter="account.zzinven"
	otherwise
		email("Dyoung@fmiint.com","missing rateoffice in inactive.prg",whoffice.rateoffice,,,,.t.,,,,,.t.)
	endcase
	
	select upsacct
	scan for accountid<9999

		if !seek(accountid,"account","accountid")
			scatter memvar 
			m.missing=.t.
			insert into xrpt from memvar
		else
			if !&xfilter
				scatter memvar 
				m.missing=.f.
				insert into xrpt from memvar
			endif
		endif
	endscan

	select xrpt
	if reccount()>0
		copy to ("f:\auto\xinactupsacct"+xoffice)
		email("Dyoung@fmiint.com","VAL "+transform(reccount())+" inactive warehouse accounts in upsacct, office "+xoffice,"xinactupsacct"+xoffice,,,,.t.,,,,,.t.)
	endif
	
	use in upsacct
endscan

use in account

*

schedupdate()

_screen.Caption=gscreencaption
on error
