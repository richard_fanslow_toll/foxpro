**NOTE: Pick up of files from tollgroup.us setup in fox scheduler (FTP_GETFILES) - every hour from 7am - 8pm
**UPDATE: Calderon now logs directly into our FTP server and are pointed directly to f:\ftpusers\Calderon\850in\
**	Site: ftp.fmiint.com
**	Login: calderon
**	Pwd: gocalderon
**
**Changes: 
**
** - Need to keep all PO data in podata.dbf in order to send missing lines from the 850 that are not included 
**	 in the 860 (860 only contains updated lines) - mvw 03/05/14
**
**Notes:
**
** - Cancel of an entire PO will only work if they send an 850 for the cancel. An 860 cancelling each line will 
**   not send a cancel file to ICON for PO - mvw 03/05/14
**

**Because Caleron cannot place test files into a different location than production files, for time being only run 
**the production files from 850in between the hours of 9pm and 7am to allow for the MANUAL moving 
**of test files from prod folder to test folder; test files run all the time - mvw 08/05/14
**
xstartrun=1
if between(hour(datetime()),7,20)
	xstartrun=2
endif

**because they will now be placing test files in separate folder (see below), we can run both test and production 
**files all day - mvw 02/02/15
xstartrun=1

**Now they will be placing the test files in "f:\ftpusers\Calderon\850in\850Test\" so I will need to move those 
**files to "f:\ftpusers\Calderon\850test\" for processing - mvw 01/21/15
if adir(xx,"f:\ftpusers\Calderon\850in\850Test\*.*")>0
	copy files f:\ftpusers\Calderon\850in\850Test\*.* to f:\ftpusers\Calderon\850test\*.*
	delete files f:\ftpusers\Calderon\850in\850Test\*.*
endif

**loop twice, once checking production folder and the other checking test folder - mvw 08/05/14
for xruncnt = xstartrun to 1
	xtest=iif(xruncnt=1,.f.,.t.)

	xindir=iif(xtest,"f:\ftpusers\Calderon\850test\","f:\ftpusers\Calderon\850in\")
	xoutdir=iif(xtest,"f:\ftpusers\toll\Calderon\850test\","f:\ftpusers\toll\Calderon\850out\")
	x997dir="f:\ftpusers\Calderon\850-997\"

	**check if files exist in the inbound folder
	xfilecnt=Adir(afiles,xindir+"*.edi")
	if xfilecnt=0
		loop
	endif

	*xseparator=chr(13)+chr(10)
	xcolcnt=25
	xtollcustcode="CALD" &&temporarily entered as "CALD" for testing 

	if empty(xtollcustcode) or xtollcustcode="CALD"
		xFrom="TGFSYSTEM"
		if xtollcustcode="CALD"
			xSubject="CALDERON 850 ICON Transalation May Have A Test Toll Customer Code!! Must Change Before Going Live!"
		else
			xSubject="CALDERON 850 ICON Transalation Cannot Be Run - Empty Toll Customer Code!!"
		endif
		xbody=""
		do form dartmail2 with "mwinter@fmiint.com",xfrom,xsubject," ",,xbody,"a"

		if empty(xtollcustcode)
			return
		endif
	endif

	set century on
	set hours to 24

	create cursor xfilelist (file c(100), x c(10), moddt C(10), modtm c(10), moddttm t(1))
	append from array afiles
	replace all moddttm with ctot(moddt+" "+modtm)

	**handle 997 creation
	*!*	scan
	*!*		xfile=alltrim(xfilelist.file)
	*!*		xinfile=xindir+alltrim(xfilelist.file)
	*!*		xfs=filetostr(xinfile)
	*!*		if "ST*997*"$xfs
	*!*			**save in 997in in case we need them
	*!*			copy file &xinfile to &x997in.&xfile
	*!*			delete file &xinfile
	*!*			delete in xfilelist
	*!*		else
	*!*			**copy ONLY 850 files to the 997 directory so 997s are sent as needed - mvw 06/08/12
	*!*			copy file &xinfile to &x997dir.&xfile
	*!*		endif
	*!*	endscan

	use f:\sysdata\qqdata\unloccode in 0
	use f:\ftpusers\calderon\data\vendors in 0
	use f:\ftpusers\calderon\data\podata in 0
	use f:\sysdata\qqdata\uom in 0

	**need a cursor to hold po data as its being processed - used to load podata as well as 
	**  fill in missing lines not included on 860 - mvw 03/05/14
	**added cancel field for line cancel logic - 03/05/14
	select *, .f. as cancel from podata where .f. into cursor xpodata readwrite

	select 0
	create cursor x850 (field1 c(254),recno n(14,3))
	index on recno tag recno

	xfilecreated=.f.
	select * from xfilelist order by moddttm into cursor xfilelist

	scan
		zap in x850

		**start with column headings
		xfilestr="order_no|order_split|order_status|client_cust_code|supplier_code|supplier_name|supplier_address_1|"+;
			"supplier_address_2|supplier_city|supplier_region_state|supplier_country|supplier_postcode|confirm_number|"+;
			"confirm_date|exw_required_by|delivery_required_by|description|order_date_time|order_value|order_currency_code|"+;
			"exchange_rate|incoterms|additional_terms|transport_mode|container_mode|milestone_tracking_date_exw|"+;
			"milestone_tracking_date_etd|milestone_tracking_date_eta|custom_milestone_est1|custom_milestone_act1|"+;
			"custom_milestone_est2|custom_milestone_act2|custom_milestone_est3|custom_milestone_act3|custom_milestone_est4|"+;
			"custom_milestone_act4|sending_agent|sending_agent_name|origin_port|destination_port|custom_text1|custom_text2|"+;
			"custom_text3|custom_text4|custom_text5|custom_date1|custom_date2|custom_decimal1|custom_decimal2|custom_decimal3|"+;
			"custom_decimal4|custom_decimal5|custom_flag1|custom_flag2|custom_flag3|custom_flag4|custom_flag5|contact1|"+;
			"contact2|country_code|notes|line_no|subline_no|linesplit_no|product|line_description|qty_ordered|innerpacks|"+;
			"outerpacks|unit_of_measure|itemprice|lineprice|linestatus|required_date|part_attrib1|part_attrib2|part_attrib3|"+;
			"line_custom_attrib1|line_custom_attrib2|line_custom_attrib3|line_custom_attrib4|line_custom_attrib5|"+;
			"line_custom_date1|line_custom_date2|line_custom_date3|line_custom_date4|line_custom_date5|line_custom_decimal1|"+;
			"line_custom_decimal2|line_custom_decimal3|line_custom_decimal4|line_custom_decimal5|line_custom_flag1|"+;
			"line_custom_flag2|line_custom_flag3|line_custom_flag4|line_custom_flag5|line_custom_text1|container_number|"+;
			"container_packing_order|country_of_origin|dg_substance|dg_flashpoint|weight|volume|weight_type|volume_type|"+;
			"special_instructions|additional_information|delivery_port|address_code|delivery_address_name|"+;
			"delivery_address_address1|delivery_address_address2|delivery_address_city|delivery_address_state|"+;
			"delivery_address_country|delivery_address_postcode"+chr(13)+chr(10)

		xhdrlen=len(xfilestr)
		xfilename = xindir+alltrim(xfilelist.file)
		xarchiveinfile = xindir+"archive\"+alltrim(xfilelist.file)
		xholdinfile = xindir+"hold\"+alltrim(xfilelist.file)

		**no need for temp file, etc if terminator segment is already chr(13)+chr(10), looks at pvhtotoll850.prg for that code
		xfile=filetostr(xfilename)
		
*		Do parse_850 With xfile
		 
		xchar=substr(xfile,106,1)

		**only do the replacement of the segment terminitor if xchar is NOT a carriage retrun or line feed. It appears the 
		**  new file format will have chr(13)+chr(10) terminator - mvw 01/13/15
		if !inlist(xchar,chr(13),chr(10))
			xfile=strtran(xfile,xchar,chr(13)+chr(10))
		endif

		**change the pipe (|) delimiters to asterisks (*) - FOR CALDERON ONLY
		**the delimter has been changed to "*" in the new format, but do the replacement anyway for the old 
		**  formatted files we will continue to see - mvw 01/13/15
		xfile=strtran(xfile,"|","*")

		xtmpfile=xindir+"tmp"+strtran(strtran(strtran(ttoc(datetime()),"/",""),":","")," ","")+".tmp"
		strtofile(xfile,xtmpfile)

		xchar=chr(13)+chr(10)

		select x850

		**added try catch in attempt to avoid "file access denied" error, believe its coming 
		**  from the append if the file has yet to be completely ftp'd - mvw 06/29/12
		xerror=.f.
		try
			append from "&xtmpfile" type delimited with character &xchar
		catch
			xerror=.t.
		endtry

		if xerror
			loop
		endif

		replace all field1 with alltrim(upper(field1)), recno with recno()
		locate

		delete file &xtmpfile

		**default delivery port UNLOC code required for Icon - default to LAX
	 	m.delivport="USNYC"

		**no CUR segment sent for CALDERON
		m.currency="USD"

		**no TD5 for shipvia for CALDERON, always SEA
		m.shipvia="SEA"

		**no incoterms included, so default to "FOB" as is mandatory for ICON - 04/10/13
		m.incoterms="FOB"

		**keep track of previous po line as we have seen instances where PVH has sent the same line number 
		**	multiple times, each containing different skus - mvw 02/14/112
		xprevlineno=""

		m.reqdt={}
		xhreqdt={}
		m.contact=""

		**need sln to be defined here now in case i need to combine 2 identical line numbers for same po - mvw 2/14/12
		xslncnt=1

		xerror=.f.
		do while !eof()
			do case
			case left(field1,3)="ISA"
			case left(field1,2)="GS"
			case left(field1,2)="ST"
				do case
				case substr(field1,4,3)="850"
					xfiletype="850"
				case substr(field1,4,3)="860"
					if xtest
						xfiletype="860-NEW"
					else
						xfiletype="860-OLD"
					endif
				otherwise
					**skip thru unknown file types
					locate for left(field1,2)="ST" while !eof()
					loop
				endcase

*			case (xfiletype="850" and left(field1,3)="BEG") or (xfiletype="860" and left(field1,3)="BCH") &&BEG for 850, BCH for 860
			case inlist(left(field1,3),"BEG","BCH") &&BEG for 850, BCH for 860
				**clear out xpodata for each new po
				zap in xpodata

				xlevel="O"
				xponum=alltrim(substr(field1,at("*",field1,3)+1,at("*",field1,4)-(at("*",field1,3)+1)))
				xaction=alltrim(substr(field1,at("*",field1,1)+1,at("*",field1,2)-(at("*",field1,1)+1)))
				**we expect only a 00 or 06 (original or "confirmation" per spec)
				**leave 05 just in case
				**added 04 (change) for 860 files - mvw 03/20/13
				xaction=iif(inlist(xaction,"00","05","06","04"),"PLC","")
				xitemaction=xaction
				if empty(xaction)
					xfrom ="Cometrics Edi Translation <fTollEdi@tollgroup.com>"
					xmessage = "Error in file '"+xfilename+"' for PO '"+xponum+"': Order status has unexpected value - expect only '00','05' or '06'. Entire file cannot be processed. File moved to the HOLD folder."
					do form dartmail2 With "mwinter@fmiint.com",xFrom,"Cadlderon 850 Translation Error"," ","",xmessage,"A"

					xerror=.t.
					exit
				endif

				xpos=iif(left(field1,3)="BCH",6,5) &&order date located in BEG05 in the 850, BCH06 in the 860
				m.orderdt=right(alltrim(field1),len(alltrim(field1))-at("*",field1,xpos))
				m.orderdt=left(m.orderdt,4)+"-"+substr(m.orderdt,5,2)+"-"+substr(m.orderdt,7,2)
				**taken from TD5 in the PO1 loop for AE
	*			m.shipvia="SEA" &&hardcode SEA bc not provided
				**reset for every po change - mvw 02/14/12
				xprevlineno=""

				**need to indicate in BB if an 860 was previously sent for a PO (only for xfiletype="860") per Bill L - mvw 06/25/14
				**saved in podata.z860rcvd
				x860rcvd=.f.

				xcanceldt=datetime() &&define it here in case of cancellations so all cancellations show the same time - mvw 09/10/14
				
				**predefine all optional fields as blank - mvw 03/13/13
				m.msg=""

				**predefine all fields not sent in the 860 as blank or from podata.dbf - mvw 03/13/13
				if xfiletype="860"
					if !seek(padr(xponum,len(podata.po_num)),"podata","po_num")
						xfrom ="TOLLEDIIMPORT"
						xmessage = "Error in file '"+xfilename+"' for PO '"+xponum+"': 860 is being sent for a PO that we never received an 850 for. Entire file cannot be processed. File moved to the HOLD folder."
						do form dartmail2 With "mwinter@fmiint.com",xFrom,"Calderon 850 Translation Error"," ","",xmessage,"A"

						xerror=.t.
						exit
					endif

					x860rcvd=podata.z860rcvd

					**only grab data from pofile.dbf for old 860 format, new format contains all necessary info - 02/24/15
					if xfiletype="860-OLD"
						select podata
						**grab all header data from po sent in 850 as some of the data doesnt get sent in 860
						scatter fields reqdt,orderdt,currency,incoterms,shipvia,reqdt,originport,delivport,contact,msg memvar

						xvendorid=transform(podata.vendorid)
						m.originport=alltrim(podata.originport)

						if !seek(val(xvendorid),"vendors","vendorid")
							xfrom ="TOLLEDIIMPORT"
							xmessage = "Error in file '"+xfilename+"' for PO '"+xponum+"': Vendor code "+xvendorid+" cannot be found in the vendor table. Entire file cannot be processed. File moved to the HOLD folder."
							do form dartmail2 With "mwinter@fmiint.com",xFrom,"Calderon 850 Translation Error"," ","",xmessage,"A"

							xerror=.t.
							exit
						endif

						xsuppliername=alltrim(vendors.vendorname)
						xsupplieraddr1=alltrim(vendors.physaddr1)
						xsupplieraddr2=alltrim(vendors.physaddr2)

						xsuppliercity=alltrim(vendors.physcity)
						xsupplierstate=alltrim(vendors.physstate)
						xsupplierzip=alltrim(vendors.physzip)
						xsuppliercountry=alltrim(vendors.pcountry)

						**change to 3-digit iso country (unloccode.isocountry) - mvw 12/07/11
						=seek(xsuppliercountry,"unloccode","country")
						xsuppliercountry=unloccode.isocountry
					endif
				endif

			**DTM*002 is now sent at the order level. It will be used for req by date unless a release is added, then use SCH06
			case left(field1,3)="DTM" and substr(field1,5,3)="002" and xlevel="O" &&seg in 850 only
				m.reqdt=right(alltrim(field1),len(alltrim(field1))-at("*",field1,2))
				m.reqdt=left(m.reqdt,4)+"-"+substr(m.reqdt,5,2)+"-"+substr(m.reqdt,7,2)
				m.itemreqdt=m.reqdt
				xhreqdt=m.reqdt &&to be used if no releases in SCH

			case left(field1,3)="PER" and substr(field1,5,2)="BD"
				m.contact=alltrim(substr(field1,at("*",field1,2)+1,at("*",field1,3)-(at("*",field1,2)+1)))
				if occurs("*",field1)=2
					m.contact=right(alltrim(field1),len(alltrim(field1))-at("*",field1,2))
				endif

	**no CUR segment sent for CALDERON, always USD. m.currency set above
	*!*			case left(field1,3)="CUR"
	*!*				m.currency=right(alltrim(field1),len(alltrim(field1))-at("*",field1,2))

			case left(field1,3)="MSG"
				m.msg=right(alltrim(field1),len(alltrim(field1))-at("*",field1,1))

			**DTM*106 is required by date in 860 (required segment), will be overwritten by SCH06 if 
			**	the date for that item is different from rest of PO 
			case xfiletype="860-OLD" and left(field1,3)="DTM" and substr(field1,5,3)="106"
				m.reqdt=right(alltrim(field1),len(alltrim(field1))-at("*",field1,2))
				m.reqdt=left(m.reqdt,4)+"-"+substr(m.reqdt,5,2)+"-"+substr(m.reqdt,7,2)
				xhreqdt=m.reqdt &&to be used if no releases in SCH

			case left(field1,2)="N1" and substr(field1,4,2)="ST"
				m.shiptocode=right(alltrim(field1),len(alltrim(field1))-at("*",field1,4))
				m.shiptoname=alltrim(substr(field1,at("*",field1,2)+1,at("*",field1,3)-(at("*",field1,2)+1)))

				skip
				store "" to m.shiptoadd1, m.shiptoadd2

				**possible N2 segment has been added for ship to name - 01/09/15
				if left(field1,2)="N2"
					if occurs("*",field1)=1
						m.shiptoadd1=right(alltrim(field1),len(alltrim(field1))-at("*",field1,1))
					else
						m.shiptoadd1=alltrim(substr(field1,at("*",field1,1)+1,at("*",field1,2)-(at("*",field1,1)+1)))
					endif
					skip
				endif

				if left(field1,2)="N3" &&should have N3 but if not, leave blank
					**if no N2 above, try for both in N3 - mvw 01/09/15
					if empty(m.shiptoadd1)
						if occurs("*",field1)=1
							m.shiptoadd1=right(alltrim(field1),len(alltrim(field1))-at("*",field1,1))
						else
							m.shiptoadd1=alltrim(substr(field1,at("*",field1,1)+1,at("*",field1,2)-(at("*",field1,1)+1)))
							m.shiptoadd2=right(alltrim(field1),len(alltrim(field1))-at("*",field1,2))
						endif
					else
						if occurs("*",field1)=1
							m.shiptoadd2=right(alltrim(field1),len(alltrim(field1))-at("*",field1,1))
						else
							m.shiptoadd2=alltrim(substr(field1,at("*",field1,1)+1,at("*",field1,2)-(at("*",field1,1)+1)))
						endif
					endif
					skip
				endif

				if left(field1,2)="N3" &&if 2nd N3, skip past
					skip
				endif

				store "" to m.shiptocity, m.shiptost, m.shiptozip, m.shiptoctry
				if left(field1,2)#"N4"
					skip -1 &&if for some reason not provided, skip back bc there is a skip at end of loop
				else
					m.shiptocity=alltrim(substr(field1,at("*",field1,1)+1,at("*",field1,2)-(at("*",field1,1)+1)))
					**if N4 comes only with city, need to use the right() function below - applies to other fields below as well
					if occurs("*",field1)=1
						m.shiptocity=right(alltrim(field1),len(alltrim(field1))-at("*",field1,1))
					endif
					
					m.shiptost=alltrim(substr(field1,at("*",field1,2)+1,at("*",field1,3)-(at("*",field1,2)+1)))
					if occurs("*",field1)=2
						m.shiptost=right(alltrim(field1),len(alltrim(field1))-at("*",field1,2))
					endif

					m.shiptozip=left(alltrim(substr(field1,at("*",field1,3)+1,at("*",field1,4)-(at("*",field1,3)+1))),5)
					if occurs("*",field1)=3
						m.shiptozip=left(right(alltrim(field1),len(alltrim(field1))-at("*",field1,3)),5)
					endif
					m.shiptozip=iif(inlist(upper(m.shiptozip),"N/A","NA","N\A"),"",m.shiptozip)

					**if doesnt contain 4 *'s, then country is not provided. if so default to US - mvw 11/29/11
					m.shiptoctry=iif(occurs("*",field1)<4,"US",right(alltrim(field1),len(alltrim(field1))-at("*",field1,4)))
					**change to 3-digit iso country (unloccode.isocountry) - mvw 12/07/11
					=seek(m.shiptoctry,"unloccode","country")
					m.shiptoctry=unloccode.isocountry
				endif

			**older format 850 use the N1*SE, new format uses the REF*IA - mvw 01/13/15
			case (left(field1,2)="N1" and substr(field1,4,2)="SE") or (left(field1,3)="REF" and substr(field1,5,2)="IA")
				xelement=iif(left(field1,2)="N1",4,2) &&either 2nd or 4th element depending on the segment
				xvendorid=right(alltrim(field1),len(alltrim(field1))-at("*",field1,xelement))

				if !seek(val(xvendorid),"vendors","vendorid")
					xfrom ="Cometrics Edi Translation <TollEdi@tollgroup.com>"
					xmessage = "Error in file '"+xfilename+"' for PO '"+xponum+"': Vendor code "+xvendorid+" cannot be found in the vendor table. Entire file cannot be processed. File moved to the HOLD folder."
					do form dartmail2 With "mwinter@fmiint.com",xFrom,"Calderon 850 Translation Error"," ","",xmessage,"A"

					xerror=.t.
					exit
				endif

				xsuppliername=alltrim(vendors.vendorname)
				xsupplieraddr1=alltrim(vendors.physaddr1)
				xsupplieraddr2=alltrim(vendors.physaddr2)

				xsuppliercity=alltrim(vendors.physcity)
				xsupplierstate=alltrim(vendors.physstate)
				xsupplierzip=alltrim(vendors.physzip)
				xsuppliercountry=alltrim(vendors.pcountry)
				
				select unloccode
				locate for name=xsuppliercity and country=xsuppliercountry
				do case
				case found()
					m.originport=alltrim(country)+alltrim(unloccode)
				otherwise
					**default UNLOC code for origin port for CALDERON to PKKHI, per rebecca
					m.originport="PKKHI"
				endcase

				select x850

				**change to 3-digit iso country (unloccode.isocountry) - mvw 12/07/11
				=seek(xsuppliercountry,"unloccode","country")
				xsuppliercountry=unloccode.isocountry

				**update podata with 850 supplier code and origin port, will need for 860 processing for that po - mvw 03/13/13
				**no longer add 1 line per po, need all lines. Now they are added in writestr() - 03/05/14
	*!*				if xfiletype="850" &&should be in here for any other file type but better safe than sorry!
	*!*					m.po_num=padr(xponum,len(podata.po_num))
	*!*					if !seek(m.po_num,"podata","po_num")
	*!*						insert into podata from memvar
	*!*					endif
	*!*					replace vendorid with val(xvendorid), originport with m.originport in podata
	*!*				endif

			**start item level
*			case (xfiletype="850" and left(field1,3)="PO1") or (xfiletype="860" and left(field1,3)="POC")
			case inlist(left(field1,3),"PO1","POC")
				xlevel="I"
				m.lineno=alltrim(substr(field1,at("*",field1,1)+1,at("*",field1,2)-(at("*",field1,1)+1)))

				**if duplicate line number for a given po, need to correct - mvw 02/14/12
				if xprevlineno==m.lineno
					xslncnt=xslncnt+1
				else
					xslncnt=1
				endif

				xprevlineno=m.lineno

				*reset m.descrip - grab description from first PID, looks as if they send 2
				m.descrip=""

				**define blank in case associated segment is not sent
				store "" to m.hts,m.size,m.service,m.extqty,m.hotflag

				if xfiletype="850"
					m.style=alltrim(substr(field1,at("*",field1,7)+1,at("*",field1,8)-(at("*",field1,7)+1)))
					if occurs("*",field1)=7
						m.style=right(alltrim(field1),len(alltrim(field1))-at("*",field1,7))
					endif

					m.qty=alltrim(substr(field1,at("*",field1,2)+1,at("*",field1,3)-(at("*",field1,2)+1)))
					m.unitprice=alltrim(substr(field1,at("*",field1,4)+1,at("*",field1,5)-(at("*",field1,4)+1)))
					m.uom=alltrim(substr(field1,at("*",field1,3)+1,at("*",field1,4)-(at("*",field1,3)+1)))
				else
					if xfiletype="860-OLD"
						select podata
						locate for po_num=padr(xponum,len(podata.po_num)) and lineno=m.lineno
						if found()
							scatter fields hts,size,service,extqty memvar
						endif
					endif

					select x850
					m.style=right(alltrim(field1),len(alltrim(field1))-at("*",field1,9))
					m.qty=alltrim(substr(field1,at("*",field1,3)+1,at("*",field1,4)-(at("*",field1,3)+1)))
					m.unitprice=alltrim(substr(field1,at("*",field1,6)+1,at("*",field1,7)-(at("*",field1,6)+1)))
					m.uom=alltrim(substr(field1,at("*",field1,5)+1,at("*",field1,6)-(at("*",field1,5)+1)))
					**possible status codes are CA, DI, AI (CA = Changes To Line Items, AI = Add Additional Items, DI = Delete Items)
					xitemaction=alltrim(substr(field1,at("*",field1,2)+1,at("*",field1,3)-(at("*",field1,2)+1)))
					**updates and new are treated the same as "PLC", delete sent as "CAN"
					xitemaction=iif(xitemaction="DI","CAN","PLC")

					**per Edmond, Rebecca and Ryan, do not send any 860 file containing cancels while in test mode - mvw 09/20/13
					**  this is bc Calderon doesnt have a test environment and therefore test pos that are created need to be 
					**  immmediately canceled from their production environment
	*!*					if xtest and xitemaction="CAN"
	*!*						xerror=.t.
	*!*						exit
	*!*					endif

					&&reset m.itemreqdt to po's reqdt for each item, if its different from po's reqdt, SCH seg sent for that item below
					m.itemreqdt=m.reqdt
				endif

				m.qty=transform(val(m.qty)) &&remove unnecesaary "0" decimal places, ie 50.000000 - mvw 04/16/13
				m.unitprice=iif(empty(m.unitprice),"0.0000",m.unitprice)

			case left(field1,3)="PID"
				**grab description from first PID, looks as if they send 2
				if empty(m.descrip)
					m.descrip=alltrim(substr(field1,at("*",field1,5)+1,at("*",field1,6)-(at("*",field1,5)+1)))
					if occurs("*",field1)=5
						m.descrip=right(alltrim(field1),len(alltrim(field1))-at("*",field1,5))
					endif
				endif

				**this may be the last seg of the item loop (SCH seg is optional). if next seg is not SCH, write the data to
				**  xfilestr here. otherwise, write it in the SCH case
				**SCH has now been replaced by DTM*160 segment - mvw 12/20/13
				if xfiletype="860-OLD"
					skip
	*				if left(field1,3)#"SCH"
					if !(left(field1,3)="DTM" and substr(field1,5,3)="160")
						if !writetostr(.t.)
							exit
						endif
					endif
					skip -1
				endif

			**new SCH segment in 850s to hold release # (which will be added to the lineno, if included) and required by date - mvw 12/29/14
			**required by date had been sent in the DTM*002 segment, now sent in the SCH06 element - mvw 01/13/15
			case left(field1,3)="SCH" and xlevel="I"
				m.release=iif(occurs("*",field1)<11,"",right(alltrim(field1),len(alltrim(field1))-at("*",field1,11)))
				if !empty(m.release)
					**add release # to lineno (preceeded by "00") if included - mvw 12/29/14
					**for multiple releases, after the 1st release need to extract the original lineno from m.lineno which now contains lineno+release# - mvw 02/12/15
					if len(alltrim(m.lineno))>3
						m.lineno=left(m.lineno,len(m.lineno)-3)
					endif
					m.lineno=alltrim(m.lineno)+padl(alltrim(m.release),3,"0")

					m.qty=alltrim(substr(field1,at("*",field1,1)+1,at("*",field1,2)-(at("*",field1,1)+1)))
				endif

				**changed to only use SCH06 as req by date if releases exist, otherwise it will be grabbed from DTM002 at order level - mvw 02/11/15
				if !empty(m.release) or empty(xhreqdt)
					m.reqdt=alltrim(substr(field1,at("*",field1,6)+1,at("*",field1,7)-(at("*",field1,6)+1)))
					if occurs("*",field1)=6
						m.reqdt=right(alltrim(field1),len(alltrim(field1))-at("*",field1,6))
					endif
					m.reqdt=left(m.reqdt,4)+"-"+substr(m.reqdt,5,2)+"-"+substr(m.reqdt,7,2)
					m.itemreqdt=m.reqdt
				else
					**otherwise go with date from header
					m.reqdt=xhreqdt
					m.itemreqdt=m.reqdt
				endif

			**optional field in 860 only - sent only if req date for an item differs from rest of the PO 
			**  Originally this was sent as an SCH segment, the DTM*160 replaces that segment - mvw 12/20/13
			case xfiletype="860-OLD" and left(field1,3)="DTM" and substr(field1,5,3)="160"
				m.itemreqdt=right(alltrim(field1),len(alltrim(field1))-at("*",field1,2))
				m.itemreqdt=left(m.itemreqdt,4)+"-"+substr(m.itemreqdt,5,2)+"-"+substr(m.itemreqdt,7,2)

				m.reqdt=m.itemreqdt &&per Bill L, need to send the itemreq dt in col O

				**if optional DTM*160 seg exists, write the data to xfilestr here. otherwise, it should have been done in the PID case
				if !writetostr(.t.)
					exit
				endif

	*!*			**optional field in 860 only - sent only if req date for an item differs from rest of the PO
	*!*			**removed, DTM*160 above now replaces SCH seg - mvw 12/20/13
	*!*			case xfiletype="860" and left(field1,3)="SCH"
	*!*				m.itemreqdt=alltrim(substr(field1,at("*",field1,6)+1,at("*",field1,7)-(at("*",field1,6)+1)))
	*!*				if occurs("*",field1)=6
	*!*					m.itemreqdt=right(alltrim(field1),len(alltrim(field1))-at("*",field1,2))
	*!*				endif
	*!*				m.itemreqdt=left(m.itemreqdt,4)+"-"+substr(m.itemreqdt,5,2)+"-"+substr(m.itemreqdt,7,2)

	*!*				**if optional SCH seg exists, write the data to xfilestr here. otherwise, it should have been done in the PID case
	*!*				if !writetostr(.t.)
	*!*					exit
	*!*				endif

			case left(field1,3)="PO4"
				**get uom from PO103, if empty grab from PO401
				if empty(m.uom)
					m.uom=alltrim(substr(field1,at("*",field1,1)+1,at("*",field1,2)-(at("*",field1,1)+1)))
				endif

				m.size=alltrim(substr(field1,at("*",field1,2)+1,at("*",field1,3)-(at("*",field1,2)+1)))
				if occurs("*",field1)=2
					m.size=right(alltrim(field1),len(alltrim(field1))-at("*",field1,2))
				endif

			case left(field1,3)="REF" and substr(field1,5,2)="MF"
				m.hts=alltrim(substr(field1,at("*",field1,2)+1,at("*",field1,3)-(at("*",field1,2)+1)))
				if occurs("*",field1)=2
					m.hts=right(alltrim(field1),len(alltrim(field1))-at("*",field1,2))
				endif

			case left(field1,3)="REF" and substr(field1,5,2)="ST" and xlevel="I"
				m.service=alltrim(substr(field1,at("*",field1,2)+1,at("*",field1,3)-(at("*",field1,2)+1)))
				if occurs("*",field1)=2
					m.service=right(alltrim(field1),len(alltrim(field1))-at("*",field1,2))
				endif

			**new segment for hot item flag - mvw 12/29/14
			case left(field1,3)="REF" and substr(field1,5,2)="CR" and xlevel="I"
				**"1"= not hot, "2" = hot
				m.hotflag=iif(right(alltrim(field1),len(alltrim(field1))-at("*",field1,2))="1","","TRUE")

				**for the new format, this should be last segment inside the item loop (PO1 loop), write to file here - mvw 01/13/15
				if !writetostr(.t.)
					exit
				endif

			**this date is used for all 3 date slots for CALDERON - EXW Required by (O), CustomMilestoneEst1 (AC) and Required Date (BV)
			**this segment will no longer exist in new file format at the item level, dates will taken from the SCH seg - mvw 01/13/15
			case left(field1,3)="DTM" and substr(field1,5,3)="002" and xlevel="I"
				m.reqdt=right(alltrim(field1),len(alltrim(field1))-at("*",field1,2))
				m.reqdt=left(m.reqdt,4)+"-"+substr(m.reqdt,5,2)+"-"+substr(m.reqdt,7,2)
				m.itemreqdt=m.reqdt

			**no TD5 for shipvia for CALDERON, m.shipvia defined above (always SEA)
	*!*			case left(field1,3)="TD5"
	*!*				**sent as A, S or J (motor), only account for A or S bc thats all icon accepts
	*!*				m.shipvia=right(alltrim(field1),len(alltrim(field1))-at("*",field1,4))
	*!*				m.shipvia=iif(m.shipvia="A","AIR","SEA")

			case left(field1,3)="AMT" and substr(field1,5,2)="M1"
				m.extqty=alltrim(substr(field1,at("*",field1,2)+1,at("*",field1,3)-(at("*",field1,2)+1)))
				if occurs("*",field1)=2
					m.extqty=right(alltrim(field1),len(alltrim(field1))-at("*",field1,2))
				endif

				**this should be last segment inside the item loop (PO1 loop), write to file here
				if !writetostr(.t.)
					exit
				endif

				if xerror
					exit
				endif

			case left(field1,3)="CTT"
			case left(field1,3)="SE"
				m.po_num=padr(xponum,len(podata.po_num))
				**if 850 or new 860, just delete all existing podata recs for this po and add in data from xpodata
				if xfiletype#'860-OLD'
					select podata
					delete for po_num=m.po_num

					select xpodata
					scan
						scatter memvar
						m.adddt=datetime()
						m.updatedt=m.adddt
						insert into podata from memvar
					endscan

					if xfiletype='860'
						replace z860rcvd with .t. for po_num=m.po_num in podata
					endif
				else
				**if 860, need to add any missing lines that exist in podata to the file and update any 
				**  lines that were sent in 860 in podata

					**if line cancel sent in 860, need to delete from podata
					**if new line added in 860, need to add to podata
					select xpodata
					scan
						select podata
						locate for po_num=m.po_num and lineno=xpodata.lineno
						do case
						case found() and xpodata.cancel
							replace canceldt with xcanceldt
							delete
						case !found() and !xpodata.cancel
							select xpodata
							scatter memvar
							m.adddt=datetime()
							m.updatedt=m.adddt
							insert into podata from memvar
						endcase
					endscan

					**if not po lines exist, cancel entire po. Need to get 1st deleted rec for that po to process file then re-delete - mvw 05/30/14
					xcancelpo=.f.
					if !seek(m.po_num,"podata","po_num")
						set deleted off
						if seek(m.po_num,"podata","po_num")
							xcancelpo=.t.
							recall for canceldt=xcanceldt in podata
						endif
						set deleted on
					endif

					select podata
					scan for po_num=m.po_num
						select xpodata
						locate for lineno=podata.lineno
						**use podata (not xpodata) for xcancelpo - mvw 05/30/14
						if found() and !xcancelpo
							scatter fields except adddt, updatedt memvar
							m.updatedt=datetime()
							select podata
							gather memvar
						else
							select podata
							scatter memvar

							=seek(val(xvendorid),"vendors","vendorid")
							xsuppliername=alltrim(vendors.vendorname)
							xsupplieraddr1=alltrim(vendors.physaddr1)
							xsupplieraddr2=alltrim(vendors.physaddr2)
							xsuppliercity=alltrim(vendors.physcity)
							xsupplierstate=alltrim(vendors.physstate)
							xsupplierzip=alltrim(vendors.physzip)
							xsuppliercountry=alltrim(vendors.pcountry)

							**change to 3-digit iso country (unloccode.isocountry) - mvw 12/07/11
							=seek(xsuppliercountry,"unloccode","country")
							xsuppliercountry=unloccode.isocountry

							if xcancelpo
								xaction="CAN"
								xitemaction="CAN"
							endif

							=writetostr(.f.)
						endif
					endscan

					**for PO cancel, re-delete all podata recs - mvw 05/30/14
					if xcancelpo
						delete for po_num=m.po_num in podata
					endif

					replace z860rcvd with .t. for po_num=m.po_num in podata
				endif
			endcase

			select x850
			skip
		enddo

		select x850
		locate

		do case
		case xerror
			copy file &xfilename to &xholdinfile
			delete file &xfilename

		case !xerror
			if len(xfilestr)>xhdrlen
				do while .t.
					set date ymd
					xoutfile="PO_TGFDI_"+strtran(strtran(strtran(ttoc(datetime()),":","")," ",""),"/")+xtollcustcode+".txt"
					set date american

					xarchiveoutfile=xoutdir+"archive\"+xoutfile
					if !file(xarchiveoutfile)
						exit
					endif
				enddo

				xoutfile=xoutdir+xoutfile
				strtofile(xfilestr,xoutfile)
				copy file &xoutfile to &xarchiveoutfile

				xfilecreated=.t.
			endif

			copy file &xfilename to &xarchiveinfile
			delete file &xfilename
		endcase
	endscan

	if xfilecreated
		**create an ftpjob
		use f:\edirouting\ftpjobs in 0
		xjob=iif(xtest,"CALD-TEST-850-ICON","CALD-850-ICON")
		insert into ftpjobs (jobname, userid, jobtime) values (xjob,"CALD850",datetime())
		use in ftpjobs
	endif

	use in x850
	use in xfilelist
	use in unloccode
	use in vendors
	use in podata
	use in uom
endfor &&xruncnt


procedure writetostr
parameters xinsertrec
**insertrec: logical, insert record into xpodata

	if empty(m.reqdt)
		xfrom ="Toll Edi Translation <TollEdi@tollgroup.com>"
		xmessage = "Error in file '"+xfilename+"' for PO '"+xponum+"': order has no required by date. Entire file cannot be processed. File moved to the HOLD folder."
		do form dartmail2 With "mwinter@fmiint.com",xFrom,"Cadlderon 850 Translation Error"," ","",xmessage,"A"

		xerror=.t.
		return .f.
	endif

	**moved xhdrstr definition here from the top of the PO1 case because m.shipvia isn't defined until after PO1 in the TD5 seg
	xhdrstr=alltrim(xponum)+"|"+; &&A
		"|"+; &&column(s) B empty
		xaction+"|"+; &&C
		xtollcustcode+"|"+;&&D
		xvendorid+"|"+; &&E
		xsuppliername+"|"+; &&F
		xsupplieraddr1+"|"+; &&G
		xsupplieraddr2+"|"+; &&H
		xsuppliercity+"|"+; &&I
		xsupplierstate+"|"+; &&J
		xsuppliercountry+"|"+; &&K
		xsupplierzip+"|"+; &&L
		"||"+; &&column(s) M-N empty
		m.reqdt+"|"+; &&O
		"||"+; &&column(s) P-Q empty
		m.orderdt+"|"+; &&R
		"|"+; &&column(s) S empty
		m.currency+"|"+; &&T
		"|"+; &&column(s) U empty
		m.incoterms+"|"+; &&V
		"|"+; &&column(s) W empty
		m.shipvia+"|"+; &&X
		"||"+; &&column(s) Y-Z is empty
		"|"+; &&AA
		"|"+; &&column(s) AB is empty
		m.reqdt+"|"+; &&AC
		"|||||||||"+; &&column(s) AD-AL empty
		m.originport+"|"+; &&AM
		m.delivport+"|"+; &&AN
		"|"+; &&AO
		"|"+; &&AP
		"|"+; &&AQ
		"|||||||||"+; &&column(s) AR-AZ
		iif(xfiletype="860","Y","N")+"|"+; &&BA (860 flag)
		iif(x860rcvd,"Y","N")+"|"+; &&BB (860 sent previously flag)
		"|||"+; &&column(s) BC-BE empty
		m.contact+"|"+; &&BF
		"|"+; &&BG
		"|"+; &&BH
		m.msg+"|" &&BI


	**only add po line if the qty>0, in case Calderon sends cancelled lines with a 0 qty like AE does - mvw 05/09/12
	**do not add for line cancels (sent in 860) - mvw 03/05/14
	if val(m.qty)>0 and !(xitemaction="CAN" and xaction#"CAN")
		**must change UOM from 2-digit to 3-digit for ICON translation - mvw 04/11/13
		if len(m.uom)=2
			**default to units (UNT) if not found is UOM master
			**if the uom3 is only 2 digits, need to trim per Bill L - mvw 07/02/14
			m.uom=alltrim(iif(seek(m.uom,"uom","uom2"),uom.uom3,"UNT"))
		endif

		**removed the "-" from the lineno as ICON could not accept it. Now line number "001" will go as "0011" 
		**  for the 1st SLN, "0012" for the 2nd and so on. The right-most character will need to be stripped 
		**  for the 856 going back to PVH - mvw 01/12/12
		*m.lineno+"-"+transform(xslncnt)+"|" &&BJ
		**no need to add slncnt to po line as there should only be 1 sln per po1 seg
		*m.lineno+padl(transform(xslncnt),2,"0")+"|" &&BJ
		xfilestr=xfilestr+xhdrstr+;
			m.lineno+"|"+; &&BJ
			"||"+; &&column(s) BK-BL empty
			m.style+"|"+; &&BM
			m.descrip+"|"+; &&BN
			transform(m.qty)+"|"+; &&BO
			"||"+; &&BP-BQ empty
			m.uom+"|"+; &&BR
			transform(m.unitprice)+"|"+; &&BS
			"|"+; &&column(s) BT empty
			xitemaction+"|"+; &&BU
			m.itemreqdt+"|"+; &&BV
			"|"+; &&BW
			"|"+; &&BX - empty, only need 2 attributes style and sku
			"|"+; &&BY - empty, had held the 1st HTS # - mvw 02/10/12
			m.hts+"|"+; &&BZ
			m.size+"|"+; &&CA
			m.service+"|"+; &&CB
			m.extqty+"|"+; &&CC
			"|"+; &&CD
			"||||||||||"+; &&CE-CN
			m.hotflag+"|"+; &&CO
			"|||||||"+; &&column(s) CP-CV empty
			"|"+; &&CW
			"||||||||"+;&&column(s) CX-DE empty
			m.delivport+"|"+; &&DF
			m.shiptocode+"|"+; &&DG
			m.shiptoname+"|"+; &&DH
			m.shiptoadd1+"|"+; &&DI
			m.shiptoadd2+"|"+; &&DJ
			m.shiptocity+"|"+; &&DK
			m.shiptost+"|"+; &&DL
			m.shiptoctry+"|"+; &&DM
			m.shiptozip+; &&DN
			chr(13)+chr(10)
	endif

	if xinsertrec and val(m.qty)>0
		**insert record into xpodata
		m.po_num=xponum
		m.cancel=iif(xitemaction="CAN",.t.,.f.)
		m.vendorid=val(xvendorid)
		insert into xpodata from memvar
	endif

return .t.
