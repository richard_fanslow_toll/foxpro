
* process Positive Pay files sent from CitiBank to us.
* It should be called when files appear in F:\FTPUSERS\CitiBankIn\

* Build EXE as F:\BOA\CITIBANKPOSPAYRETURNS.exe
*
* 4/27/2018 MB: changed fmiint.com to my Toll email

LOCAL loPositivePayReturns, lnError, lcProcessName

utilsetup("CITIBANKPOSPAYRETURNS")

DECLARE INTEGER CreateMutex IN Win32API ;
	STRING @lpMutexAttributes, ;
	STRING bInitialOwner, ;
	STRING @lpName

DECLARE INTEGER GetLastError IN Win32API

* if running in EXE check to see if already running
IF _VFP.STARTMODE > 0 THEN
	lcProcessName = "PositivePayReturns"
	CreateMutex(0,CHR(1),@lcProcessName)
	lnError = GetLastError()
	IF lnError = 183
		WAIT WINDOW TIMEOUT 5 "This program is already running..."
		schedupdate()
		RETURN .F.
	ENDIF
ENDIF

loPositivePayReturns = CREATEOBJECT('PositivePayReturns')
loPositivePayReturns.MAIN()
CLOSE DATABASES ALL

schedupdate()

RETURN

#DEFINE LOGIT 1
#DEFINE WAITIT 2
#DEFINE NOWAITIT 4
#DEFINE SENDIT 8
#DEFINE LF CHR(10)
#DEFINE CRLF CHR(13) + CHR(10)
#DEFINE RETURN_DATA_MANDATORY .F.
#DEFINE RETURN_DATA_NOT_MANDATORY .T.
#DEFINE CSVFORMAT 6
#DEFINE WRITE_ONLY_UNBUFFERED 11
#DEFINE COMMA ","

DEFINE CLASS PositivePayReturns AS CUSTOM

	cProcessName = 'PositivePayReturns'

	lTestMode = .F.  && .F. FOR NORMAL OPERATION, .T. TO SUPPRESS ALL BUT DEVELOPER EMAILS AND CHANGE INPUT TABLE, ETC.

	lAutoYield = .T.

	* date time props
	dToday = DATE()
	*dToday = {^2010-07-24}

	cStartTime = TTOC(DATETIME())

	* data table properties
	cTranslogTable = 'F:\BOA\DATA\TRANSLOG'

	* folder properties
	cInputFolder = 'F:\FTPUSERS\CITIBANKIN\'
	cArchivedFolder = 'F:\FTPUSERS\CITIBANKIN\ARCHIVE\'
	cConvertedFolder = 'F:\FTPUSERS\CITIBANKIN\CONVERTED\'

	* processing properties
	lArchiveFiles = .T.
	lConvertFiles = .T.
	cErrors = ''

	* connection properties
	nSQLHandle = 0

	* wait window properties
	nWaitWindowTimeout = 2
	lWaitWindowIsOn = .T.

	* logfile properties
	lLoggingIsOn = .T.
	cLogFile = 'F:\BOA\LOGFILES\CitiBank_POS_PAY_RETURNS_log.txt'

	* INTERNAL email properties
	lSendInternalEmailIsOn = .T.
	cFrom = 'FMI Corporate <fmicorporate@fmiint.com>'
	cSendTo = 'mark.bennett@tollgroup.com'
	*cCC = 'pgaidis@fmiint.com'
	cCC = ''
	cSubject = ''
	cAttach = ''
	cBodyText = ''

	cCOMPUTERNAME = ''
	cUSERNAME = ''


	FUNCTION INIT
		IF NOT DODEFAULT()
			RETURN .F.
		ENDIF
		WITH THIS
			*SET RESOURCE OFF
			CLOSE DATA
			SET CENTURY ON
			SET DATE AMERICAN
			SET HOURS TO 24
			SET ANSI ON
			SET TALK OFF
			SET DELETED ON
			SET CONSOLE OFF
			SET EXCLUSIVE OFF
			SET SAFETY OFF
			SET EXACT OFF
			SET STATUS BAR ON
			SET SYSMENU OFF
			SET ENGINEBEHAVIOR 70
			_VFP.AUTOYIELD = .lAutoYield
			.cCOMPUTERNAME = GETENV("COMPUTERNAME")
			.cUSERNAME = GETENV("USERNAME")
			IF .lTestMode THEN
				.cLogFile = 'F:\BOA\LOGFILES\CitiBank_POS_PAY_RETURNS_LOG_TESTMODE.txt'
				.cSendTo = 'mark.bennett@tollgroup.com'
				.cCC = ''
			ENDIF
			.lLoggingIsOn = .lLoggingIsOn AND NOT EMPTY(.cLogFile)
			IF .lLoggingIsOn THEN
				SET ALTERNATE TO (.cLogFile) ADDITIVE
				SET ALTERNATE ON
			ENDIF
		ENDWITH
	ENDFUNC


	FUNCTION DESTROY
		WITH THIS
			IF .lLoggingIsOn  THEN
				SET ALTERNATE OFF
				SET ALTERNATE TO
			ENDIF
		ENDWITH
		DODEFAULT()
	ENDFUNC


	FUNCTION MAIN
		WITH THIS
			LOCAL lnNumberOfErrors, lcTranslogTable, loError, ldToday, ldYesterday, lcCompany, lcYesterday
			LOCAL lcTransText, lcInputFolder, lcArchivedFolder, lnNumFiles, lcRootFileName
			LOCAL laFiles[1,5], laFilesSorted[1,6], i, lcFile, lcArchivedFile, llArchived
			LOCAL lcConvertedFolder, lcConvertedFile, lcStringOld, lcStringNew, lcFileToProcess
			LOCAL lcDateTimeStamp, ltDateTimeStamp

			TRY

				* Because this program is triggered by the first file that appears in the input folder,
				* wait a bit in case other files are still being copied, before proceeding.
*!*					WAIT WINDOW TIMEOUT 10 'Waiting for all input files to be copied...'

				lnNumberOfErrors = 0

				.TrackProgress('', LOGIT)
				.TrackProgress('', LOGIT)
				.TrackProgress('=========================================================', LOGIT+SENDIT)
				.TrackProgress('CitiBank Positive Pay Return process started....', LOGIT+SENDIT+NOWAITIT)
				.TrackProgress('COMPUTERNAME = ' + .cCOMPUTERNAME, LOGIT+SENDIT)
				.TrackProgress('USERNAME = ' + .cUSERNAME, LOGIT+SENDIT)
				.TrackProgress('=========================================================', LOGIT+SENDIT)
				IF .lTestMode THEN
					.TrackProgress('                        TEST MODE                        ', LOGIT+SENDIT)
					.TrackProgress('=========================================================', LOGIT+SENDIT)
				ENDIF
				.TrackProgress('', LOGIT)

				ldToday = .dToday
				ldYesterday = ldToday - 1
				lcYesterday = DTOC(ldYesterday)

				.cSubject = 'CitiBank Positive Pay Return Processing for ' + TRANSFORM(DATETIME())
				lcTranslogTable = .cTranslogTable

*!*					USE (lcTranslogTable) AGAIN IN 0 ALIAS BOATRANSLOG

				lcInputFolder = .cInputFolder
				lcArchivedFolder = .cArchivedFolder
				lcConvertedFolder = .cConvertedFolder

				* get list of files in input folder
				SET DEFAULT TO (lcInputFolder)
				lnNumFiles = ADIR(laFiles,"*.*")

				.TrackProgress(TRANSFORM(lnNumFiles) + ' files found in ' + lcInputFolder,LOGIT+SENDIT)

				IF lnNumFiles > 0 THEN

					* sort file list by date/time
					.SortArrayByDateTime(@laFiles, @laFilesSorted)

					FOR i = 1 TO lnNumFiles
						lcRootFileName = laFilesSorted[i,1]
						lcDateTimeStamp = ( DTOC(laFilesSorted[i,3]) + " " + laFilesSorted[i,4] )
						ltDateTimeStamp = CTOT( lcDateTimeStamp )
						lcFile = lcInputFolder + lcRootFileName
						lcArchivedFile = lcArchivedFolder + lcRootFileName
						lcConvertedFile = lcConvertedFolder + lcRootFileName

						* process each file
						.TrackProgress('Processing ' + lcFile + '....',LOGIT+SENDIT)

						IF .lConvertFiles THEN
							* take input file, convert LF rec delimiter to cr/lf
							lcStringOld = FILETOSTR(lcFile)
							lcStringNew = STRTRAN(lcStringOld,LF,CRLF)
							* and write out in converted folder
							=STRTOFILE(lcStringNew,UPPER(lcConvertedFile))

							IF FILE(lcConvertedFile) THEN
								.TrackProgress('   Converted ' + lcFile,LOGIT+SENDIT)
							ELSE
								.LOGERROR('   ERROR converting ' + lcFile)
							ENDIF
							* run processing against the converted file
							lcFileToProcess = lcConvertedFile
						ELSE
							* run processing against original, unconverted file
							lcFileToProcess = lcFile

						ENDIF && lConvertFiles

						* process each file
						DO CASE
							CASE UPPER(LEFT(lcRootFileName,3)) = 'ACK'
								.ProcessACKFile(lcFileToProcess, lcRootFileName, ltDateTimeStamp)
							CASE UPPER(LEFT(lcRootFileName,3)) = 'RTN'
								.ProcessRTNFile(lcFileToProcess, lcRootFileName, ltDateTimeStamp)
							OTHERWISE
								.LOGERROR('   ERROR - unexpected file type for ' + lcFile)
						ENDCASE

						* archive each file
						IF .lArchiveFiles THEN

							*!*	* if the file already exists in the archive folder, delete it in the archive folder.
							*!*	* this is to prevent errors we get copying into archive on a resend of an already-archived file.
							*!*	llArchived = FILE(lcArchivedFile)
							*!*	IF llArchived THEN
							*!*		DELETE FILE (lcArchivedFile)
							*!*		.TrackProgress('   Deleted archived file before re-archiving : ' + lcFile,LOGIT+SENDIT)
							*!*	ENDIF

							* if the file already exists in the archive folder, make sure it is not read-only.
							* this is to prevent errors we get copying into archive on a resend of an already-archived file.
							* Necessary because the files in the archive folders were sometimes marked as read-only (not sure why)
							llArchived = FILE(lcArchivedFile)
							IF llArchived THEN
								RUN ATTRIB -R &lcArchivedFile.
								.TrackProgress('   Used ATTRIB -R for re-archiving : ' + lcFile,LOGIT+SENDIT)
							ENDIF

							* archive it...
							COPY FILE (lcFile) TO (lcArchivedFile)
							llArchived = FILE(lcArchivedFile)
							IF llArchived THEN
								DELETE FILE (lcFile)
								.TrackProgress('   Archived ' + lcFile,LOGIT+SENDIT)
							ELSE
								.LOGERROR('   ERROR archiving ' + lcFile)
							ENDIF
						ENDIF  && .lArchiveFiles

					ENDFOR  && i = 1 TO lnNumFiles

				ENDIF  &&  lnNumFiles > 0


				.TrackProgress('CitiBank Positive Pay Return process ended normally!',LOGIT+SENDIT)
				.cBodyText = .cErrors + CRLF + CRLF + .cBodyText

			CATCH TO loError

				.TrackProgress('There was an error.',LOGIT+SENDIT)
				.TrackProgress('The Error # is: ' + TRANSFORM(loError.ERRORNO), LOGIT+SENDIT)
				.TrackProgress('The Error Message is: ' + TRANSFORM(loError.MESSAGE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred in Program: ' + TRANSFORM(loError.PROCEDURE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred at line #: ' + TRANSFORM(loError.LINENO), LOGIT+SENDIT)
				lnNumberOfErrors = lnNumberOfErrors + 1
				CLOSE DATA

			ENDTRY

			CLOSE DATA
			WAIT CLEAR
			***************** INTERNAL email results ******************************
			.TrackProgress('About to send status email.',LOGIT)
			.TrackProgress('The logfile is: ' + .cLogFile, SENDIT)
			.TrackProgress('CitiBank Positive Pay Return process started: ' + .cStartTime, LOGIT+SENDIT)
			.TrackProgress('CitiBank Positive Pay Return process finished: ' + TTOC(DATETIME()), LOGIT+SENDIT)

			IF .lSendInternalEmailIsOn THEN
				DO FORM dartmail2 WITH .cSendTo,.cFrom,.cSubject,.cCC,.cAttach,.cBodyText,"A"
				.TrackProgress('Sent status email.',LOGIT)
			ELSE
				.TrackProgress('Did not send status email: THIS.lSendInternalEmailIsOn = FALSE.',LOGIT)
			ENDIF

		ENDWITH
		RETURN
	ENDFUNC && main


	PROCEDURE LOGERROR
		LPARAMETERS tcError
		WITH THIS
			.TrackProgress(tcError,LOGIT+SENDIT)
			.cErrors = .cErrors + ALLTRIM(tcError) + CRLF
		ENDWITH
	ENDPROC


	PROCEDURE ProcessRTNFile
		LPARAMETERS tcFileToProcess, tcRootFileName, ttDateTimeStamp
		* Update the CitiBank fields in Transdet table for matching Acct# / Check #
		PRIVATE m.citiprocdtm, m.checknum, m.acctnum, m.citiissuedt, m.citipaiddt, m.citichkamt, m.citimatchind, m.citirtnfile
		m.citiprocdtm = DATETIME()

		USE F:\BOA\DATA\TRANSDET IN 0 ALIAS TRANSDET

		CREATE CURSOR currtn (cLine C(100))
		SELECT currtn
		APPEND FROM &tcFileToProcess TYPE SDF

		* exclude Trailer record, which begins with 'T' from scan
		* removed payee per CitiBank 8/4/10 MB
		SELECT currtn
		SCAN FOR NOT INLIST( UPPER(LEFT(currtn.cLine,1)),'T','F')
			m.checknum = SUBSTR(currtn.cLine,3,10)
			m.acctnum = SUBSTR(currtn.cLine,18,10)
			m.citiissuedt = CTOD(SUBSTR(currtn.cLine,46,2)+"/"+SUBSTR(currtn.cLine,48,2)+"/20"+SUBSTR(currtn.cLine,50,2))
			m.citipaiddt = CTOD(SUBSTR(currtn.cLine,39,2)+"/"+SUBSTR(currtn.cLine,41,2)+"/20"+SUBSTR(currtn.cLine,43,2))
			m.citichkamt = VAL(SUBSTR(currtn.cLine,28,11))/100
			m.citimatchind = SUBSTR(currtn.cLine,45,1)
			m.citirtnfile = tcRootFileName
			m.citirtndtm = ttDateTimeStamp

			SELECT TRANSDET
			LOCATE FOR (acctnum == m.acctnum) AND (checknum == m.checknum)
			IF FOUND() THEN
				* update citi fields
				REPLACE TRANSDET.citiissuedt WITH m.citiissuedt, ;
					TRANSDET.citipaiddt WITH m.citipaiddt, ;
					TRANSDET.citichkamt WITH m.citichkamt, ;
					TRANSDET.citimatchind WITH m.citimatchind, ;
					TRANSDET.citirtnfile WITH m.citirtnfile
					
				.TrackProgress('      :) :) - updated TRANSDET for Acct# / Check #: [' + m.acctnum + '] / [' + m.checknum + ']',LOGIT+SENDIT)
			ELSE
				* error insert a record with just the citi fields
				INSERT INTO TRANSDET FROM MEMVAR
				.LOGERROR('      ERROR - could not find matching Acct# / Check #: [' + m.acctnum + '] / [' + m.checknum + ']')
			ENDIF
		ENDSCAN

		IF USED('TRANSDET') THEN
			USE IN TRANSDET
		ENDIF
		IF USED('currtn') THEN
			USE IN currtn
		ENDIF
		
		RETURN
	ENDPROC  && ProcessRTNFile




	PROCEDURE ProcessACKFile
		LPARAMETERS tcFileToProcess, tcRootFileName, ttDateTimeStamp
		LOCAL lnFileHandle, lcLine, lnNumSecsToGoBack, lnIID
		PRIVATE m.cprefix, m.cacknfile, m.procackndtm, m.recvackndtm, m.namtackn, m.ctrlrmatch
		

		*!*	ACKN layout (after converted to have CRs):
		*!*	         STRT      STRT  END
		*!*	PROFILE  DATE      TIME  TIME     RECS SENT    RECS RECV        AMOUNT
		*!*	--------------------------------------------------------------------------------
		*!*	TH4339IR 08/23/10  09:34 09:34             0   0000000010    000000010.00

		lnFileHandle = FOPEN(tcFileToProcess)&& Open the file
		lcLine = FGETS(lnFileHandle)  && read line 1 (discard)
		lcLine = FGETS(lnFileHandle)  && read line 2 (discard)
		lcLine = FGETS(lnFileHandle)  && read line 3 (discard)
		lcLine = FGETS(lnFileHandle)  && read line 4 (to be parsed)
		=FCLOSE(lnFileHandle)  && Close the file
		
		m.cprefix = SUBSTR(tcRootFileName,5,4)
		m.cacknfile = tcRootFileName
		m.nrecsackn = VAL(SUBSTR(lcLine,48,10))
		m.namtackn = VAL(SUBSTR(lcLine,60,14))
		m.ctrlrmatch = SUBSTR(lcLine,79,1)
		m.procackndtm = DATETIME()
		m.recvackndtm = ttDateTimeStamp

		USE F:\BOA\DATA\TRANSLOG IN 0 ALIAS TRANSLOG
		* look in translog for most recent record that matches the Prefix ;
		* and was sent within last XX seconds
		* and has not already been updated with ACKN info.
		lnNumSecsToGoBack = 60 * 60 * 24

		IF USED('CURTRANSLOG') THEN
			USE IN CURTRANSLOG
		ENDIF

		SELECT iid, cprefix, insdttm, ( ttDateTimeStamp - insdttm ) AS NSECSPAST ;
			FROM TRANSLOG ;
			INTO CURSOR CURTRANSLOG ;
			WHERE COMPLETED = 'YES' ;
			AND ( ttDateTimeStamp - insdttm ) <= lnNumSecsToGoBack ;
			AND ( ttDateTimeStamp - insdttm ) > 0 ;
			AND CPREFIX = m.cprefix ;
			AND SENTTO = 'CITIBANK' ;
			AND EMPTY(cacknfile) ;
			ORDER BY insdttm DESC

		IF USED('CURTRANSLOG') AND NOT EOF('CURTRANSLOG') THEN
		
			* get IID from top (most recent) record to use to update back into TRANSLOG
			SELECT CURTRANSLOG
			GOTO TOP
			lnIID = CURTRANSLOG.IID
			
			SELECT TRANSLOG
			LOCATE FOR iid = lnIID
			IF FOUND() THEN
				REPLACE TRANSLOG.cacknfile WITH m.cacknfile, ;
					TRANSLOG.nrecsackn WITH m.nrecsackn, ;
					TRANSLOG.namtackn WITH m.namtackn, ;
					TRANSLOG.ctrlrmatch WITH m.ctrlrmatch, ;
					TRANSLOG.procackndtm WITH m.procackndtm, ;
					TRANSLOG.recvackndtm WITH m.recvackndtm
					
				.TrackProgress('      :) :) - updated TRANSLOG rec for iid = ' + TRANSFORM(lnIID), LOGIT+SENDIT)
				
				IF ( TRANSLOG.nrecsackn <> TRANSLOG.nrecssent ) THEN
					.LOGERROR('      WARNING - # Recs Sent Mismatch in TRANSLOG rec for iid = ' + TRANSFORM(lnIID))
				ENDIF
				
				IF ( TRANSLOG.namtackn <> TRANSLOG.namtsent ) THEN
					.LOGERROR('      WARNING - $ Amount Mismatch in TRANSLOG rec for iid = ' + TRANSFORM(lnIID))
				ENDIF				
				
			ELSE
				.LOGERROR('      ERROR - could not find matching TRANSLOG rec for iid = ' + TRANSFORM(lnIID))
			ENDIF
		
		ELSE
			.LOGERROR('      ERROR - could not find matching TRANSLOG rec for ACK file: ' + tcRootFileName)
			*************** insert into TRANSLOG from memvar???
		ENDIF

		IF USED('TRANSLOG') THEN
			USE IN TRANSLOG
		ENDIF
		IF USED('CURTRANSLOG') THEN
			USE IN CURTRANSLOG
		ENDIF

		RETURN
	ENDPROC && ProcessACKFile



	FUNCTION ExecSQL
		LPARAMETERS tcSQL, tcCursorName, tlNoDataReturnedIsOkay
		LOCAL llRetval, lnResult
		WITH THIS
			* close target cursor if it's open
			IF USED(tcCursorName)
				USE IN (tcCursorName)
			ENDIF
			WAIT WINDOW "Fetching remote data; please wait..." NOWAIT
			lnResult = SQLEXEC(.nSQLHandle, tcSQL, tcCursorName)
			llRetval = ( lnResult > 0 )
			IF llRetval THEN
				* see if any data came back
				IF NOT tlNoDataReturnedIsOkay THEN
					IF (NOT USED(tcCursorName)) OR EOF(tcCursorName) THEN
						llRetval = .F.
						.TrackProgress('No data was returned by this query: ' +CRLF + tcSQL, LOGIT+SENDIT)
						.cSendTo = 'Mark Bennett <mark.bennett@tollgroup.com>'
					ENDIF
				ENDIF
			ELSE
				.TrackProgress("Result Code [" + TRANSFORM(lnResult) + "] returned by this query: " + tcSQL, LOGIT+SENDIT)
				.cSendTo = 'Mark Bennett <mark.bennett@tollgroup.com>'
			ENDIF
			WAIT CLEAR
			RETURN llRetval
		ENDWITH
	ENDFUNC


	PROCEDURE TrackProgress
		* do any combination of Wait Window, writing to logfile, and adding to body of email,
		* based on nFlags parameter.
		LPARAMETERS tcExpression, tnFlags
		WITH THIS
			IF BITAND(tnFlags,LOGIT) = LOGIT THEN
				IF .lLoggingIsOn THEN
					?
					? .cProcessName + "  " + TTOC(DATETIME()) + ": " + tcExpression
				ENDIF
			ENDIF
			IF .lWaitWindowIsOn THEN
				IF BITAND(tnFlags,NOWAITIT) = NOWAITIT THEN
					WAIT WINDOW tcExpression NOWAIT
				ENDIF
				IF BITAND(tnFlags,WAITIT) = WAITIT THEN
					WAIT WINDOW tcExpression TIMEOUT .nWaitWindowTimeout
				ENDIF
			ENDIF
			IF BITAND(tnFlags,SENDIT) = SENDIT THEN
				IF .lSendInternalEmailIsOn THEN
					.cBodyText = .cBodyText + tcExpression + CRLF + CRLF
				ENDIF
			ENDIF
		ENDWITH
	ENDPROC  &&  TrackProgress


	PROCEDURE SortArrayByDateTime
		* expects taInArray[] to be the result of an ADIR()
		LPARAMETERS taInArray, taOutArray
		EXTERNAL ARRAY taInArray, taOutArray
		LOCAL lnRows, lnCols, i, j
		* extending taInArray[] to process the files in date/time order
		lnRows = ALEN(taInArray,1)
		lnCols = ALEN(taInArray,2)
		* create taOutArray with one more column
		DIMENSION taOutArray[lnRows,lnCols + 1]
		* and fill it with taInArray columns AND (filedate + filetime) in new column
		* ( ACOPY() doesn't preserve elements properly because of different # of columns )
		FOR i = 1 TO lnRows
			FOR j = 1 TO lnCols
				taOutArray[i,j] = taInArray[i,j]
			ENDFOR
			taOutArray[i,lnCols + 1] = DTOS(taInArray[i,3]) + "_" + taInArray[i,4]
		ENDFOR
		* sort taOutArray on date/time column
		=ASORT(taOutArray,6)
	ENDPROC


ENDDEFINE
