* validate WMS data - runs on Saturday
 
utilsetup("VAL12")

store "" to guserid, gprocess
gdevelopment=.f.
xjobno=0
xsystem="VAL12"

external array xdbf

use f:\auto\val12 exclusive
zap
use

*

valset("fix G-III dimensions in indet")

useca("indet","wh",,,"units=0 and length=9 and remainqty>0 and date_rcvd>{"+dtoc(date()-16)+"} and " + ;
	"(inlist(accountid,"+gg3accounts+") or inlist(accountid,"+gg3accounts+"))")

scan 
	if xsqlexec("select * from inven where accountid="+transform(accountid)+" " + ;
		"and units=0 and style='"+style+"' and color='"+color+"' and id='"+id+"' and pack='"+pack+"' and length#0",,,"wh") # 0
		
		replace length with inven.length, width with inven.width, depth with inven.depth in indet
		assert .f.
	endif
endscan

tu("indet")

*

valset("purge old ptbak & ptdetbak")

useca("ptbak","wh",,,"ptdate<{"+dtoc(date()-150)+"}")

scan 
	? xsqlexec("delete from ptdetbak where ptbakid="+transform(ptbak.ptbakid),,,"wh")
	delete in ptbak
endscan

tu("ptbak")

*

valset("remove palletid from inloc for RACK locations")

release guserid

select whoffice
scan for !test and !novalid and wireless
	xoffice=whoffice.office
	goffice=whoffice.office
	xfolder=whoffice.folder+"\whdata\"

	xsqlexec("select * from inloc where mod='"+goffice+"' and whseloc='RACK' and !empty(palletid)",,,"wh")

	replace palletid with "" in inloc for whseloc="RACK" and !empty(palletid)

	use in inloc
endscan

*

valset("check for inventory for dead accounts")

xsqlexec("select * from account where inactive=0","account",,"qq")
index on accountid tag accountid
set order to

select whoffice
scan for !test and !novalid
	xoffice=whoffice.office
 	goffice=xoffice
	xfolder=whoffice.folder+"\whdata\"
	xfilter=trim(whoffice.wmsaccounts)
	
	if usesqlinven()
		xsqlexec("select * from inven where mod='"+goffice+"'",,,"wh")
	else
		use (xfolder+"inven") in 0
	endif
	
	select accountid, .f. as nf, .f. as inven from inven group by 1 into cursor xrpt readwrite
	
	scan 
		if !seek(accountid,"account","accountid")
			replace nf with .t. in xrpt
		else
			select account
			if !&xfilter
				replace inven with .t. in xrpt
			endif
		endif
	endscan

	select xrpt
	count to aa for nf or inven
	if aa>0
		copy to ("f:\auto\xdeadinven"+xoffice) for nf or inven
		email("Dyoung@fmiint.com","VAL "+transform(aa)+" inventory for dead accounts for "+xoffice,"xdeadinven"+xoffice,,,,.t.,,,,,.t.)
	endif

	use in inven
endscan

*

valset("fix incorrect remainqty")

select whoffice
scan for !test and !novalid
	xoffice=whoffice.office
 	goffice=xoffice
	xfolder=whoffice.folder+"\whdata\"

	useca("indet","wh",,,"mod='"+goffice+"' and remainqty#totqty-removeqty")
	useca("inloc","wh",,,"mod='"+goffice+"' and remainqty#locqty-removeqty")
	useca("adj","wh",,,"mod='"+goffice+"' and remainqty#totqty-removeqty")
	
	select indet
	count for remainqty#totqty-removeqty
	if _tally>0
		xtally=_tally
		copy to ("f:\auto\xindetremqty"+xoffice) for remainqty#totqty-removeqty
		replace remainqty with totqty-removeqty for remainqty#totqty-removeqty
		tu("indet")
		email("Dyoung@fmiint.com","VAL (fixed) "+transform(xtally)+" incorrect remainqty in indet, office "+xoffice,"xindetremqty"+xoffice,,,,.t.,,,,,.t.)
	endif

	select inloc
	count for remainqty#locqty-removeqty
	if _tally>0
		xtally=_tally
		copy to ("f:\auto\xinlocremqty"+xoffice) for remainqty#locqty-removeqty
		replace remainqty with locqty-removeqty for remainqty#locqty-removeqty
		tu("inloc")
		email("Dyoung@fmiint.com","VAL (fixed) "+transform(xtally)+" incorrect remainqty in inloc, office "+xoffice,"xinlocremqty"+xoffice,,,,.t.,,,,,.t.)
	endif

	select adj
	count for (inadj and remainqty#totqty-removeqty) or (!inadj and remainqty>0)
	if _tally>0
		xtally=_tally
		copy to ("f:\auto\xadjremqty"+xoffice) for (inadj and remainqty#totqty-removeqty) or (!inadj and remainqty>0)
		replace remainqty with totqty-removeqty for inadj and remainqty#totqty-removeqty
		replace remainqty with 0 for !inadj and remainqty>0
		email("Dyoung@fmiint.com","VAL (fixed) "+transform(xtally)+" incorrect remainqty in adj, office "+xoffice,"xadjremqty"+xoffice,,,,.t.,,,,,.t.)
	endif

	use in indet
	use in inloc
	use in adj
endscan

*

valset("check for removeqty>totqty in adj")

select whoffice
scan for !test and !novalid 
	xoffice=whoffice.office
 	goffice=xoffice
	xfolder=whoffice.folder+"\whdata\"

	if usesqladj()
		useca("adj","wh",,,"mod='"+goffice+"' and inadj=1 and removeqty>totqty")
	else	
		use (xfolder+"adj") in 0
	endif
	
	select * from adj where inadj and removeqty>totqty into cursor xrpt
	scan 
		select adj
		locate for adjid=xrpt.adjid
		replace removeqty with totqty, remainqty with 0 in adj
	endscan

	if reccount()>0
		if usesqladj()
			tu("adj")
		endif
		
		copy to ("f:\auto\xadjremove"+xoffice)
		email("Dyoung@fmiint.com","VAL (fixed) "+transform(reccount())+" removeqty>totqty in adj, office "+xoffice,"xadjremove"+xoffice,,,,.t.,,,,,.t.)
	endif

	use in adj
endscan

*

valset("fix inadj flag")

select whoffice
scan for !test and !novalid
	xoffice=whoffice.office
 	goffice=xoffice
	xfolder=whoffice.folder+"\whdata\"

	useca("adj","wh",,,"mod='"+goffice+"' and ((inadj=1 and totqty<0) or (inadj=0 and totqty>0))")

	select adj
	count for (inadj and totqty<0) or (!inadj and totqty>0)
	if _tally>0
		copy to ("f:\auto\xinadj"+xoffice) for (inadj and totqty<0) or (!inadj and totqty>0)
		xtally=_tally
		replace inadj with (totqty>0) for (inadj and totqty<0) or (!inadj and totqty>0)
		email("Dyoung@fmiint.com","VAL (fixed) "+transform(xtally)+" wrong inadj flags in adj, office "+xoffice,"xinadj"+xoffice,,,,.t.,,,,,.t.)
		if usesqladj()
			tu("adj")
		endif
	endif

	use in adj
endscan

* 

valset("check for mismatched P&P flag between outship & outwolog")

select whoffice
scan for !test and !novalid
	xoffice=whoffice.office
 	goffice=xoffice
	xfolder=whoffice.folder+"\whdata\"

	if usesqloutwo()
	else
		use (xfolder+"outship") in 0
		use (xfolder+"outwolog") in 0
	endif
	
	if usesqloutwo()
		xsqlexec("select outwolog.wo_num, outwolog.accountid, outwolog.wo_date, outship.outshipid, " + ;
			"outwolog.picknpack as outwologpnp, outship.picknpack as outshippnp " + ;
			"from outwolog, outship where outwolog.mod='"+goffice+"' and outwolog.outwologid=outship.outwologid " + ;
			"and outwolog.wo_date>{"+dtoc(date()-14)+"} and outwolog.picknpack#outship.picknpack","xoutpnp",,"wh")
	else
		select outwolog.wo_num, outwolog.accountid, outwolog.wo_date, outship.outshipid, ;
			outwolog.picknpack as outwologpnp, outship.picknpack as outshippnp ;
			from outwolog, outship ;
			where outwolog.outwologid=outship.outwologid ;
			and outwolog.wo_date>date()-14 ;
			and outwolog.picknpack#outship.picknpack ;
			into cursor xoutpnp
	endif

	if reccount()>0
*!*			select xoutpnp
*!*			scan
*!*				if seek(xoutpnp.outshipid,"outship","outshipid")
*!*					replace picknpack with xoutpnp.outwologpnp in outship
*!*				endif
*!*			endscan
*!*		
		copy to ("f:\auto\xoutpnp"+xoffice)
		email("Dyoung@fmiint.com","VAL "+transform(reccount())+" mismatched P&P flags between outship & outwolog, office "+xoffice,"xoutpnp"+xoffice,,,,.t.,,,,,.t.)
	endif
	
	if used("outship")
		use in outship
	endif
	if used("outwolog")
		use in outwolog
	endif
endscan	

*

valset("check for mismatched pulled status in outwolog/outship")

select whoffice
scan for !test and !novalid
	xoffice=whoffice.office
 	goffice=xoffice
	xfolder=whoffice.folder+"\whdata\"

	xsqlexec("select outwolog.wo_num, outwolog.wo_date, outwolog.accountid, ship_ref, " + ;
		"outwolog.pulled as wopull, outwolog.pulleddt as wopulldt, " + ;
		"outship.pulled as ptpull, outship.pulleddt as ptpulldt " + ;
		"from outwolog, outship where outwolog.mod='"+goffice+"' and outwolog.outwologid=outship.outwologid " + ;
		"and ((outwolog.pulled=1 and outship.pulled=0) or (outwolog.pulled=0 and outship.pulled=1)) " + ;
		"and outwolog.wo_date>{"+dtoc(date()-14)+"}","xrpt",,"wh")
	
	count to xcnt
	
	if xcnt>0
*!*			xcnt=_tally
*!*			scan 
*!*				=seek(str(accountid,4)+ship_ref,"outship","acctpt")
*!*				replace pulled with xrpt.wopull, pulleddt with xrpt.wopulldt in outship
*!*			endscan
			
		copy to ("f:\auto\xoutpull"+xoffice)
		email("Dyoung@fmiint.com","VAL "+transform(xcnt)+" mismatched pulled status in outwolog/outship for "+xoffice,"xoutpull"+xoffice,,,,.t.,,,,,.t.)
	endif
	
	if used("outship")
		use in outship
	endif
	if used("outwolog")
		use in outwolog
	endif
endscan	

*

valset("check for dead accounts in upcloc")

xsqlexec("select * from account where inactive=0","account",,"qq")
index on accountid tag accountid
set order to

select whoffice
scan for !test and !novalid
	xrateoffice=whoffice.rateoffice
	xoffice=whoffice.office
	goffice=xoffice
	xfolder=whoffice.folder+"\whdata\"

	useca("upcloc","wh",,,"mod='"+xoffice+"'")

	select accountid from upcloc group by 1 into cursor xacct readwrite
	
	scan 
		=seek(accountid,"account","accountid")
		if (xrateoffice="N" and !account.njinven) or (xrateoffice="C" and !account.cainven) ;
		or (xrateoffice="F" and !account.flinven) or (xrateoffice="R" and !account.riinven) ;
		or (xrateoffice="K" and !account.kyinven) or (xrateoffice="L" and !account.mlinven) ;
		or (xrateoffice="X" and !account.xxinven) or (xrateoffice="O" and !account.oninven) ;
		or (xrateoffice="Y" and !account.yyinven) or (xrateoffice="Z" and !account.zzinven) 
			delete in upcloc for accountid=account.accountid
		else
			delete in xacct
		endif
	endscan
	
	select xacct
	count to aa
	if aa>0
		tu("upcloc")
		copy to ("f:\auto\xupclocdelete"+xoffice)
		email("Dyoung@fmiint.com","VAL (OK) "+transform(aa)+" dead accounts in upcloc, office "+xoffice,"xupclocdelete"+xoffice,,,,.t.,,,,,.t.)
	endif

	use in upcloc
endscan

*

valset("check for dead accounts in upcmast")

xsqlexec("select * from account where inactive=1","account",,"qq")
index on accountid tag accountid
set order to

upcmastsql(,,,,,"accountid#6550")

select accountid, count(1) from upcmast group by 1 into cursor xacct readwrite

scan 
	if seek(accountid,"account","accountid")
		xsqlexec("delete from upcmast where accountid="+transform(account.accountid),,,"wh")
	else
		delete in xacct
	endif
endscan

select xacct
count to aa
if aa>0
	copy to f:\auto\xupcmastdelete
	email("Dyoung@fmiint.com","VAL (OK) "+transform(aa)+" dead accounts in upcmast","xupcmastdelete",,,,.t.,,,,,.t.)
endif

*

valset("check for duplicate SCACs")

xsqlexec("select * from scac","scac",,"wh")

select scac, count(1) from scac where !inlist(scac,"CTDP","FDEG","UPSN","FSP ") group by 1 having count(1)>1 into cursor xrpt

if reccount()>0
	copy to ("f:\auto\xdupscac")
	email("Dyoung@fmiint.com","VAL "+transform(reccount())+" duplicate SCACs","xdupscac",,,,.t.,,,,,.t.)
endif

*

valset("reactivate inactivated accounts with a balance")

xsqlexec("select * from account","account",,"qq")
index on accountid tag accountid
set order to

xsqlexec("","billto",,"qq")
index on billtoid tag billtoid
set order to

xsqlexec("","acctbill",,"qq")
index on billtoid tag billtoid
set order to

xsqlexec("select wo_num, accountid, invnum, invdt from invoice",,,"ar")
index on invnum tag invnum
set order to

xsqlexec("select * from farhdr",,,"ar")

gaccountid=0
taccountid="%"
goffsetsince={}

arcalc(.t.)

select .f. as reactivate, accountid, sum(amt1+amt2+amt3+amt4) as amt from xrpt group by 2 order by 2 into cursor xaging readwrite

scan for amt>0
	=seek(accountid,"account","accountid")
	if account.inactive
		replace reactivate with .t. in xaging
	endif
endscan

select xaging
count to aa for reactivate

if aa>0
	select xaging
	scan for reactivate
		xsqlexec("update account set inactive=0 where accountid="+transform(xaging.accountid),,,"qq")
	endscan
	
	copy to f:\auto\xreactivate
	email("Dyoung@fmiint.com","INACTIVE: "+transform(aa)+" accounts reactivated with a balance","xreactivate",,,,.t.,,,,,.t.)
endif

*

valset("weekly development environment backup")

copy file m:\dev\frm\*.* to m:\bak\devweekly\frm\*.*
copy file m:\dev\mnu\*.* to m:\bak\devweekly\mnu\*.*
copy file m:\dev\rpt\*.* to m:\bak\devweekly\rpt\*.*
copy file m:\dev\prg\*.* to m:\bak\devweekly\prg\*.*
copy file m:\dev\lib\*.* to m:\bak\devweekly\lib\*.*
copy file m:\dev\proj\*.* to m:\bak\devweekly\proj\*.*

*

use f:\auto\val12 
go bottom
replace zendtime with datetime()
replace all seconds with zendtime-zstarttime
use 

schedupdate()

_screen.Caption=gscreencaption
on error
