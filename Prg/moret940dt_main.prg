*!* m:\dev\prg\moret940_main.prg
PARAMETERS cOfficeIn
SET ASSERTS ON
*ASSERT .F.
IF VARTYPE(cOfficeIn) # 'C'
	cOfficeIn = "C"
ENDIF

*Set Step On

CLOSE DATABASES ALL
SET ENGINEBEHAVIOR 70
SET tablevalidate TO 0
*SET RESOURCE OFF
SET EXCLUSIVE OFF
SET SAFETY OFF
SET TALK OFF
SET ESCAPE ON
ON ESCAPE FCLOSE(xhan)
SET MULTILOCKS ON
SET DELETED ON
SET STATUS BAR ON

USE m:\joeb\whdata\pt.DBF EXCLUSIVE
ZAP
USE m:\joeb\whdata\ptdet.DBF EXCLUSIVE
ZAP
Close Databases ALL

TRY

	PUBLIC nAcct_num,cOffice,cCustname,cPropername,cStorenum,nFileCount,units,lMoretSend,lDoImport
	PUBLIC cFilter,nRunID,lComplete,lTesting,lEmail,lTestImport,lTestRun,emailcommentstr,cUseFolder
	PUBLIC ptid,ptctr,ptqty,xfile,cfilename,UploadCount,pickticket_num_start,pickticket_num_end
	PUBLIC lcPath,lcArchivePath,tsendto,tcc,cTransfer,NormalExit,m.accountid,cCoCode,cUserMessage
	PUBLIC ARRAY a856(1)

	cTransfer = "X"
	STORE cOfficeIn TO cOffice

	NormalExit = .F.
	lMoretSend = .F.
	cUserMessage = ""

	IF cOffice = "C"
		cExtension = "-ML"  && Mira Loma-HPD
		nAcct_num = 5452
	ELSE
		cExtension = "-NJ"  && Norma Kamali
		nAcct_num = 5446
	ENDIF
	STORE ("940-MORET"+cExtension) TO cTransfer

	IF !lTesting
	SELECT 0
	USE F:\edirouting\FTPSETUP SHARED
	REPLACE chkbusy WITH .T.,trig_time WITH DATETIME() FOR FTPSETUP.TRANSFER = cTransfer
	USE IN FTPSETUP
	endif

	lTesting = .F.
	lTestRun = .F.
	lTestImport = .F.

	SELECT 0
	USE F:\3pl\DATA\mailmaster alias mm
	LOCATE FOR (mm.edi_type = "940" and mm.accountid = nAcct_num AND mm.office = cOfficeIn)
	IF FOUND()
		STORE TRIM(mm.acctname) TO cAccountName
		STORE TRIM(mm.basepath) TO lcPath
		STORE TRIM(mm.archpath) TO lcArchivePath
		STORE TRIM(mm.sendmail) TO tsendto
		STORE TRIM(mm.ccmail)   TO tcc
		STORE mm.testflag 	   TO lTest
		USE IN mm
	ELSE
		WAIT WINDOW AT 10,10  "No parameters set for this acct# "+ALLTRIM(STR(Acctnum))+"  ---> Office "+lcOffice TIMEOUT 2
		USE IN mm
*	RELEASE ALL
		throw
	ENDIF
	STORE lTest TO lTestRun,lTestImport
	STORE cAccountName TO cCustname
	cUseName = cCustname
	cPropername = PROPER(cUseName)

*	_SCREEN.CAPTION = cPropername+" 940 Process"
	CLEAR
	WAIT WINDOW "Now running "+cPropername+" 940 upload..." TIMEOUT 2

	dXdate1 = ""
	dXxdate2 = DATE()
	nFileCount = 0

* Set Step On

*!*		IF !lTestImport
*!*			xReturn="XXX"
*!*			DO m:\dev\prg\wf_alt WITH cOffice,nAcct_num
*!*			cUseFolder = xReturn
*!*		ELSE
		cUseFolder = "m:\joeb\WHDATA\"
*!*		ENDIF

*!* Set up temp structure for XPT/XPTDET

	DO m:\dev\prg\moret940dt_PROCESS

	SELECT 0
	USE F:\edirouting\FTPSETUP SHARED
	REPLACE CHKBUSY WITH .F. FOR FTPSETUP.TRANSFER = cTransfer
	USE IN FTPSETUP

CATCH TO oErr
	IF NormalExit = .F.
		ASSERT .F. MESSAGE "AT CATCH SECTION"
		tsubject = cCustname+" 940 Upload Error ("+TRANSFORM(oErr.ERRORNO)+") at "+TTOC(DATETIME())
		tattach  = ""
		tsendto  = "joe.bianchi@tollgroup.com"
		tcc      = "pgaidis@fmiint.com"
		IF lMoretSend
			tcc      = tcc+",lupe@moret.com"  && Added 1/24/08, Joe
		ENDIF
		tmessage = cCustname+" 940 Upload Error..... Please fix me........!"
		lcSourceMachine = SYS(0)
		lcSourceProgram = SYS(16)

		tmessage =tmessage+ "Try/CATCH Exeception Message:"+CHR(13)+;
			[  Error: ] + STR(oErr.ERRORNO) +CHR(13)+;
			[  LineNo: ] + STR(oErr.LINENO) +CHR(13)+;
			[  Message: ] + oErr.MESSAGE +CHR(13)+;
			[  Procedure: ] + oErr.PROCEDURE +CHR(13)+;
			[  Details: ] + oErr.DETAILS +CHR(13)+;
			[  StackLevel: ] + STR(oErr.STACKLEVEL) +CHR(13)+;
			[  LineContents: ] + oErr.LINECONTENTS+CHR(13)+;
			[  UserValue: ] + oErr.USERVALUE+CHR(13)+;
			[  Computer:  ] +lcSourceMachine+CHR(13)+;
			[  940 file:  ] +xfile+CHR(13)+;
			[  Program:   ] +lcSourceProgram
		tmessage = tmessage + CHR(13) + cUserMessage
		tattach  = ""
		tfrom    ="TGF EDI Processing Center <transload-ops@fmiint.com>"
		DO FORM m:\dev\FRM\dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
	ELSE
		WAIT WINDOW AT 10,10 "Upload complete...normal exit " TIMEOUT 2
	ENDIF
FINALLY
	CLOSE ALL
ENDTRY
CLEAR ALL
CLOSE DATABASES ALL