* GetInvRegMailAddr
*
* author: Mark Bennett
*
* create date: 04/27/2018
*
* based on date, returns the text that should go under 'By Regular mail' in the invoices.
*
* for RF 408861

LPARAMETERS tdDate

#DEFINE CRLF CHR(13) + CHR(10)

LOCAL lcText
lcText = ''

DO CASE
	CASE tdDate >= {^2018-06-01}
		lcText = ;
			'TGF Management Group Holdco Inc.' + CRLF + ;
			'Mail Code 11113' + CRLF + ;
			'P.O. Box 7247' + CRLF + ;
			'Philadelphia, PA 19170-2113'	
	OTHERWISE
		lcText = ;
			'TGF Management Group Holdco Inc.' + CRLF + ;
			'Mail Code 11113' + CRLF + ;
			'P.O. Box 11839' + CRLF + ;
			'Newark, NJ 07101-8138'	
ENDCASE

RETURN lcText