* This program extracts cutu data from inven
utilsetup("RAGBONE_RCV_RPT")
SET EXCLUSIVE OFF 
SET DELETED ON 
SET TALK OFF  
SET SAFETY OFF   &&Allows for overwrite of existing files

close databases all

useca('inwolog','wh',,,'a-6699')
useca('pl','wh',,,'a-6699')
useca('indet','wh',,,'a-6699')
SELECT WO_DATE, WO_NUM, ACCOUNTID, ACCTNAME,CONTAINER,REFERENCE,BROKERREF,;
ACCT_REF,SEAL,QUANTITY as actual_ctn,PLINQTY as expected_ctn,PLUNITSINQTY as total_expected_qty,CONFIRMDT FROM INWOLOG WHERE ;
CONTAINER !='PPA' AND CONTAINER !='CYCL' AND BROKERREF!='FROM' INTO CURSOR T1 READWRITE
SELECT a.*, style,color,id,pack,totqty as expected_qty FROM t1 a LEFT JOIN pl b ON a.wo_num=b.wo_num INTO CURSOR c1 READWRITE
SELECT wo_num,style,color,id,pack,SUM(totqty) as expected_qty FROM pl where units GROUP BY wo_num,style,color,id,pack INTO CURSOR cc1 READWRITE
SELECT wo_num, style,color,id,pack, SUM(totqty) as rcvdqty FROM indet WHERE units GROUP BY wo_num, style,color,id,pack INTO CURSOR vindet READWRITE
SELECT * FROM cc1 a full JOIN vindet b ON a.wo_num=b.wo_num AND a.style=b.style AND a.color=b.color AND a.id=b.id AND a.pack=b.pack  INTO  CURSOR cc2 READWRITE
REPLACE wo_num_a WITH wo_num_b FOR ISNULL(wo_num_a) IN cc2
REPLACE style_a WITH style_b FOR ISNULL(style_a) IN cc2
REPLACE color_a WITH color_b FOR ISNULL(color_a) IN cc2
REPLACE id_a WITH id_b FOR ISNULL(id_a) IN cc2
REPLACE pack_a WITH pack_b FOR ISNULL(pack_a) IN cc2
REPLACE expected_qty WITH 0 FOR ISNULL(expected_qty) IN cc2
SELECT wo_num_a as wo_num, style_a as style, color_a as color,id_a as id, pack_a as pack,expected_qty, rcvdqty FROM cc2 INTO CURSOR cc3 READWRITE
SELECT t1
DELETE FOR confirmdt < DATE()-10
SET STEP ON 
SELECT * FROM t1 a LEFT JOIN cc3 b ON a.wo_num=b.wo_num INTO CURSOR cc4 READWRITE
*!*	scan
*!*	  if seek(STR(xtemp.accountid,4)+xtemp.style+xtemp.color+xtemp.size,"upcmast","stylecolid")
*!*	      replace div with getmemodata("upcmast.info","DIV") in xtemp
*!*	  endif
*!*	ENDSCAN
SELECT cc4
		If Reccount() > 0 
		export TO "F:\FTPUSERS\ragbone\reports\ragbone_rcv_rpt"   TYPE xls 
		tsendto = "tmarg@fmiint.com"
		tattach = "" 
		tcc =""                                                                                                                               
		tFrom ="Toll EDI System Operations <fmi-transload-ops@fmiint.com>"                                
		tmessage = "RAG AND BONE RECEIVING REPORT COMPLETE_:"+Ttoc(Datetime())        
		tSubject = "RAG AND BONE RECEIVING REPORT COMPLETE"                                                           
		Do Form m:\dev\frm\dartmail2 With tsendto,tFrom,tSubject, tcc,tattach,tmessage  ,"A"     
			
	ELSE 


	ENDIF
close data all 




schedupdate()
_screen.Caption=gscreencaption
on error