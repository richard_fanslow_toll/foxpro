*!* This is for 315D FILES ONLY!

PARAMETERS nWO_Num,cOffice,dtTrigTime,lcStatus

PUBLIC c_CntrlNum,c_GrpCntrlNum,cWO_Num,cGroupName,cDivision,cCustName,cFilename,div214,cST_CSZ
PUBLIC tsendto,tcc,cTrigDate,cTrigTime,cProcType,cLocation,lNoRecs,errmsg,cPrgName
ON ESCAPE CANCEL
cPrgName = "ANF 315"

lTesting = .F.
DO m:\dev\prg\_setvars WITH lTesting

lFilesOut = !lTesting

tsendtoerr = ""
tsendto =""
tcc=""
tccerr=""
tattach = " "
errmsg =""

cFilename = ""
lIsError = .F.
lNoRecs = .F.
lDoCatch = .T.
nFilenum = 0
cProcType = IIF(lcStatus="X4","P","O") && 315A = Arrival, 315D = Delivery
nAcctNum = 6576

WAIT WINDOW "At start of 315"+cProcType+" Process" TIMEOUT 1

IF USED("sftpjobs")
	USE IN sftpjobs
ENDIF
USE F:\edirouting\sftpjobs IN 0 ALIAS sftpjobs

tfrom = "Toll Operations <toll-edi-ops@tollgroup.com>"
SELECT 0
IF !USED("mm")
	USE F:\3pl\DATA\mailmaster ALIAS mm
ENDIF
SELECT mm
LOCATE FOR mm.edi_type = "315" AND mm.taskname = "CTR STATUS"

lUseAlt = mm.use_alt
tsendto = IIF(lUseAlt,sendtoalt,sendto)
tcc = IIF(lUseAlt,ccalt,cc)
lcOutpath = ALLTRIM(mm.basepath)
lcHoldpath = ALLTRIM(mm.holdpath)
lcArchivepath = ALLTRIM(mm.archpath)

USE IN mm
tsendtoerr = tsendto
tccerr = tcc
tattach = " "

TRY

	cWO_Num = ALLTRIM(STR(nWO_Num))
	lEmail = .T.
	lDoError = .F.
	IF !USED("WOLOG")
		USE F:\wo\wodata\wolog IN 0
	ENDIF

	=SEEK(nWO_Num,"wolog","wo_num")
	IF !FOUND("wolog")
		errmsg ="WorkOrder Not found: "+TRANSFORM(nWO_Num)
		THROW
	ELSE
		lcMBolNum=ALLTRIM(wolog.mblnum)
		lcContainer =ALLTRIM(wolog.CONTAINER)
	ENDIF

	IF wolog.office ="C"
		**adjusted to handle the possibility of shipping to/from CARSON II as opposed to SP - mvw 03/05/17
		IF lcStatus = "X4"
			if wolog.tocs='CARSON, CA'
				lcUNCode ="USCRU"
				lcAddr ='CARSON, CA'
			else
				lcUNCode ="USSPQ"
				lcAddr ="SAN PEDRO, CA"
			endif
		ENDIF

		IF lcStatus = "OA"
			lcUNCode ="USLGB"
			lcAddr ="LONG BEACH, CA"
		ENDIF
	ENDIF

	IF wolog.office ="N"
		IF lcStatus = "X4"
			lcUNCode ="USEWR"
			lcAddr ="NEWARK, NJ"
		ENDIF

		IF lcStatus = "OA"
			lcUNCode ="USCSF"
			lcAddr ="CARTERET, NJ"
		ENDIF
	ENDIF

*************************************************************************************
	IF lTesting
		CLOSE DATABASES ALL
		lFilesOut = .F.
		nWO_Num =    1661934
		cOffice = "C"
		dtTrigTime = DATETIME()-2400

		lcMBolNum="MAEUMAEU8554400"
		lcContainer ="MAEU8554400"
*    lcStatus ="X4"
*    lcStatus ="OA"

		IF lcStatus = "X4"
			lcUNCode ="USSPQ"
			lcAddr ="SAN PEDRO, CA"
		ENDIF

		IF lcStatus = "OA"
			lcUNCode ="USLGB"
			lcAddr ="LONG BEACH, CA"
		ENDIF
	ENDIF
*************************************************************************************

	cTrigDate = DTOS(TTOD(dtTrigTime))
	cTrigTime = LEFT(RIGHT(TTOC(dtTrigTime,1),6),4)

	cCustName = "ANF"
	cCustFolder = "ANF"
	cCustPrefix = "ANF315"+LOWER(cProcType)+"_"+LOWER(cOffice)
	cMailName = PROPER(cCustName)


	cST_Name = "TGF INC."
	cfd = "*"  && Field delimiter
	csegd = "~"  && Segment delimiter
	cterminator = ">"  && Used at end of ISA segment

	csendqual = "ZZ"  && Sender qualifier
	csendid = IIF(lTesting,"TGFSCST","TGFSCS")  && Sender ID code
	crecqual = "ZZ"  && Recip qualifier, was "08"
	crecid = IIF(lTesting,"ANFTLPT","ANFTLP")   && Recip ID Code, was "6113310214"

	cDate = DTOS(DATE())
	cTruncDate = RIGHT(cDate,6)
	cTruncTime = SUBSTR(TTOC(DATETIME(),1),9,4)
	cfiledate = cDate+cTruncTime
	csendidlong = PADR(csendid,15," ")
	crecidlong = PADR(crecid,15," ")

	IF lTesting
		cISACode = "T"
	ELSE
		cISACode = "P"
	ENDIF

	nSTCount = 0
	nSegCtr = 0
	nLXNum = 1
	STORE "" TO cEquipNum,cHAWB
	dt1 = TTOC(DATETIME(),1)
	dtmail = TTOC(DATETIME())
	dt2 = DATETIME()

	cFilename = (lcHoldpath+"anf_315_"+lcStatus+"_"+dt1+".edi")
	cFilenameShort = JUSTFNAME(cFilename)
	cFilename2 = (lcArchivepath+cFilenameShort)
	cFilename3 = (lcOutpath+cFilenameShort)
	nFilenum = FCREATE(cFilename)

	DO num_incr_isa
	cISA_Num = PADL(c_CntrlNum,9,"0")
	STORE "ISA"+cfd+"00"+cfd+"          "+cfd+"00"+cfd+"          "+cfd+csendqual+cfd+csendidlong+cfd+;
		crecqual+cfd+crecidlong+cfd+cTruncDate+cfd+cTruncTime+cfd+"U"+cfd+"00401"+cfd+;
		cISA_Num+cfd+"0"+cfd+cISACode+cfd+cterminator+csegd TO cString
	DO cstringbreak

	STORE "GS"+cfd+"QO"+cfd+csendid+cfd+crecid+cfd+cDate+cfd+cTruncTime+cfd+c_CntrlNum+;
		cfd+"X"+cfd+"004010"+csegd TO cString
	DO cstringbreak

	DO num_incr_st
	STORE "ST"+cfd+"315"+cfd+PADL(c_GrpCntrlNum,9,"0")+csegd TO cString
	DO cstringbreak
	nSTCount = nSTCount + 1
	nSegCtr = nSegCtr + 1

&& here its either OA or X4

	STORE "B4"+cfd+cfd+cfd+lcStatus+cfd+cTrigDate+cfd+cTrigTime+cfd+cfd+SUBSTR(lcContainer,1,4)+cfd+SUBSTR(lcContainer,5)+cfd+"L"+cfd+"4300"+cfd+lcUNCode+cfd+"UN"+csegd TO cString
	DO cstringbreak
	nSegCtr = nSegCtr + 1

	STORE "N9"+cfd+"WO"+cfd+TRANSFORM(nWO_Num)+csegd TO cString
	DO cstringbreak
	nSegCtr = nSegCtr + 1

	STORE "N9"+cfd+"BM"+cfd+ALLTRIM(lcMBolNum)+csegd TO cString
	DO cstringbreak
	nSegCtr = nSegCtr + 1

	STORE "R4"+cfd+"D"+cfd+"UN"+cfd+lcUNCode+cfd+lcAddr+csegd TO cString
	DO cstringbreak
	nSegCtr = nSegCtr + 1

*!* Finish processing
	DO close315
	IF nFilenum>0
		=FCLOSE(nFilenum)
	ENDIF

	DO ediupdate WITH "315"+cProcType+" CREATED",.F.

	IF lFilesOut
		COPY FILE &cFilename TO &cFilename2
		COPY FILE &cFilename TO &cFilename3
		DELETE FILE &cFilename
	ENDIF

	IF !lTesting
		IF !USED("ftpedilog")
			SELECT 0
			USE F:\edirouting\ftpedilog ALIAS ftpedilog
			INSERT INTO ftpedilog (TRANSFER,ftpdate,filename,acct_name,TYPE) VALUES ("214-"+cCustName,dt2,cFilename,UPPER(cCustName),"214")
			USE IN ftpedilog
		ENDIF
	ENDIF

	IF lTesting
		tsubject = "AnF 315"+cProcType+" *TEST* EDI from TGF as of "+dtmail
	ELSE
		tsubject = "Anf 315"+cProcType+" EDI from TGF as of "+dtmail
	ENDIF

	tmessage = "315"+cProcType+" EDI Info from TGF, Truck WO# "+cWO_Num+CHR(13)
	tmessage = tmessage +" has been created."+CHR(13)+"(Filename: "+cFilenameShort+")"
	IF lTesting
		tmessage = tmessage + CHR(13)+CHR(13)+"This is a TEST RUN..."
	ENDIF

	IF lEmail
		DO FORM m:\dev\frm\dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
	ENDIF

	WAIT WINDOW cMailName+" 315"+cProcType+" EDI File complete for WO# "+cWO_Num AT 20,60 TIMEOUT 2

	RELEASE ALL LIKE c_CntrlNum,c_GrpCntrlNum
	RELEASE ALL LIKE nOrigSeq,nOrigGrpSeq

	WAIT CLEAR
	WAIT WINDOW "All "+cMailName+" 315"+cProcType+" EDI File output complete" AT 20,60 TIMEOUT 2

	SELECT sftpjobs
	INSERT INTO sftpjobs (jobname) VALUES ("ANF_PUT_TO_AMBERROAD")
	USE IN sftpjobs

	closefiles()

CATCH TO oErr
	IF lDoCatch
		lEmail = .T.
		SET STEP ON
*Do ediupdate With "ERRHAND ERROR",.T.
		tmessage = "AnF 315 Error processing "+CHR(13)

		tmessage =tmessage+ "Try/CATCH Exeception Message:"+CHR(13)+;
			[  Error: ] + STR(oErr.ERRORNO) +CHR(13)+;
			[  LineNo: ] + STR(oErr.LINENO) +CHR(13)+;
			[  Message: ] + oErr.MESSAGE +CHR(13)+;
			[  Procedure: ] + oErr.PROCEDURE +CHR(13)+;
			[  Details: ] + oErr.DETAILS +CHR(13)+;
			[  StackLevel: ] + STR(oErr.STACKLEVEL) +CHR(13)+;
			[  LineContents: ] + oErr.LINECONTENTS+CHR(13)+;
			[  Message: ] + errmsg+CHR(13)+;
			[  UserValue: ] + oErr.USERVALUE

		tsendto = tsendtoerr
		tcc = tccerr
		tsubject = "AnF 315"+cProcType+" EDI Poller Error at "+TTOC(DATETIME())
		tattach  = ""
		tfrom    ="TGF EDI 315"+cProcType+" Poller Operations <toll-edi-ops@tollgroup.com>"
		DO FORM m:\dev\frm\dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
		lEmail = .F.
	ENDIF
FINALLY
	closefiles()
	ON ERROR
	lTesting = .F.
ENDTRY

&& END OF MAIN CODE SECTION



****************************
PROCEDURE num_incr_isa
****************************

	IF !USED("serfile")
		USE ("F:\3pl\data\serial\anf_serial") IN 0 ALIAS serfile
	ENDIF
	SELECT serfile
	IF lTesting
		nOrigSeq = serfile.seqnum
		nOrigGrpSeq = serfile.grpseqnum
		lISAFlag = .F.
	ENDIF
	nISA_Num = serfile.seqnum
	c_CntrlNum = ALLTRIM(STR(serfile.seqnum))
	REPLACE serfile.seqnum WITH serfile.seqnum + 1 IN serfile
ENDPROC

****************************
PROCEDURE num_incr_st
****************************

	IF !USED("serfile")
		USE ("F:\3pl\data\serial\anf_serial") IN 0 ALIAS serfile
	ENDIF
	SELECT serfile
	IF lTesting
		nOrigSeq = serfile.seqnum
		nOrigGrpSeq = serfile.grpseqnum
		lSTFlag = .F.
	ENDIF
	c_GrpCntrlNum = ALLTRIM(STR(serfile.grpseqnum))
	REPLACE serfile.grpseqnum WITH serfile.grpseqnum + 1 IN serfile
ENDPROC

****************************
PROCEDURE close315
****************************
	STORE  "SE"+cfd+ALLTRIM(STR(nSegCtr+1))+cfd+PADL(c_GrpCntrlNum,9,"0")+csegd TO cString
	DO cstringbreak

	STORE  "GE"+cfd+ALLTRIM(STR(nSTCount))+cfd+c_CntrlNum+csegd TO cString
	DO cstringbreak

	STORE  "IEA"+cfd+"1"+cfd+PADL(c_CntrlNum,9,"0")+csegd TO cString
	DO cstringbreak

ENDPROC

****************************
PROCEDURE cstringbreak
****************************
	cLen = LEN(ALLTRIM(cString))
	FWRITE(nFilenum,cString)
ENDPROC

****************************
PROCEDURE errormail
****************************
	PARAMETERS msgtype
*Assert .F. Message "In ERRORMAIL"
	lDoCatch = .F.

	SET STEP ON
	tsubject = "Aeropostale 315 EDI File Error"
	tmessage = "Aeropostale 315 EDI Info, WO# "+cWO_Num+CHR(10)
	tsendto = tsendtoerr
	tcc = tccerr

	DO CASE
		CASE msgtype = "MISSGROUP"
			tmessage = tmessage + "There was no Group Name in WO# "+cWO_Num
			DO ediupdate WITH msgtype,.T.
		CASE msgtype = "MISSWO"
			tmessage = tmessage + "WO# "+cWO_Num+" does not exist in F:\WO\WODATA\WOLOG."
			DO ediupdate WITH msgtype,.T.
		CASE msgtype = "MISSEQUIP"
			tmessage = tmessage + "Equipment not specified for WO# "+cWO_Num+CHR(10)
			tmessage = tmessage + "Data in WOLOG must be corrected."
			DO ediupdate WITH msgtype,.T.
		CASE msgtype = "BAD TRIPID"
			tmessage = tmessage + "Trip ID does not exist in F:\WO\WODATA\WOLOG."
			DO ediupdate WITH msgtype,.T.
		CASE msgtype = "MISSLOC"
			tmessage = tmessage + "Location "+cLocation+" does NOT EXIST IN DELLOCS for Acct. 980"
			DO ediupdate WITH msgtype,.T.
	ENDCASE

	IF lTesting
		tmessage = tmessage + CHR(10)+CHR(10)+"This is TEST DATA only..."
	ENDIF
	IF lEmail
		DO FORM m:\dev\frm\dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
	ENDIF
ENDPROC

**************************
PROCEDURE ediupdate
**************************
	PARAMETERS cFin_status, lIsError
*Assert .F. Message "At EDIUPDATE process"
	IF nFilenum>0
		=FCLOSE(nFilenum)
	ENDIF
	IF cFin_status = "NO RECS"
		lNoRecs = .T.
	ENDIF

	IF !lTesting
		SELECT edi_trigger
		LOCATE
		IF lIsError
			lErrorFlag = IIF(cFin_status = "NO 315 POSSIBLE",.F.,.T.)
			REPLACE edi_trigger.processed WITH .T.,edi_trigger.created WITH .F.,;
				edi_trigger.fin_status WITH cFin_status,edi_trigger.errorflag WITH lErrorFlag ;
				edi_trigger.when_proc WITH DATETIME() ;
				FOR edi_trigger.wo_num = nWO_Num AND edi_type="315"+cProcType
			IF FILE(cFilename)
				DELETE FILE &cFilename
			ENDIF
		ELSE
			IF lNoRecs
				REPLACE edi_trigger.processed WITH .T.,edi_trigger.created WITH .F.,proc214 WITH .T.;
					edi_trigger.when_proc WITH DATETIME(),;
					edi_trigger.fin_status WITH "NO 315 NEEDED";
					edi_trigger.errorflag WITH .F.,file214 WITH "" ;
					FOR edi_trigger.wo_num = nWO_Num AND edi_type="315"+cProcType
				IF FILE(cFilename)
					DELETE FILE &cFilename
				ENDIF
			ELSE
				REPLACE edi_trigger.processed WITH .T.,edi_trigger.created WITH .T.,proc214 WITH .T.;
					edi_trigger.when_proc WITH DATETIME(),;
					edi_trigger.fin_status WITH "315 CREATED";
					edi_trigger.errorflag WITH .F.,file214 WITH cFilename ;
					FOR edi_trigger.wo_num = nWO_Num AND edi_type="315"+cProcType
			ENDIF
		ENDIF
	ENDIF

*!* Added 09/18/2014, Joe
	IF !lIsError
		SELECT 0
		USE F:\edirouting\ftpjobs
		INSERT INTO ftpjobs (jobname,USERID,jobtime) VALUES (IIF(lTesting,"315-TEST-OHL","315-OHL"),"tollglobal",DATETIME())
		USE IN ftpjobs
	ENDIF
ENDPROC


*********************
PROCEDURE closefiles
*********************
	IF USED('wolog')
		USE IN wolog
	ENDIF
	IF USED('dellocs')
		USE IN dellocs
	ENDIF
ENDPROC

*********************
PROCEDURE missdetmail
*********************
	PARAMETERS cDetailErrors
	SELECT 0
	USE F:\3pl\DATA\mailmaster ALIAS mm SHARED
	LOCATE FOR edi_type = "MISC" AND taskname = "MISS315INFO"
	lUseAlt = mm.use_alt
	tsendto = IIF(lUseAlt,sendtoalt,sendto)
	tcc = IIF(lUseAlt,ccalt,cc)
	USE IN mm
	tsubject = cCustName+" Detail Error at "+TTOC(DATETIME())
	tattach  = ""
	tmessage = "315"+cProcType+" processing shows missing Detail arrivals in the following PO(s):"
	tmessage = tmessage+CHR(13)+cDetailErrors
	tattach = " "
	IF lEmail
		DO FORM m:\dev\frm\dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
	ENDIF
ENDPROC

*********************
PROCEDURE misswomail
*********************
	SELECT 0
	USE F:\3pl\DATA\mailmaster ALIAS mm SHARED
	LOCATE FOR edi_type = "MISC" AND taskname = "MISS315INFO"
	lUseAlt = mm.use_alt
	tsendto = IIF(lUseAlt,sendtoalt,sendto)
	tcc = IIF(lUseAlt,ccalt,cc)
	USE IN mm
	tsubject = cCustName+" Detail Error at "+TTOC(DATETIME())
	tattach  = ""
	tmessage = "315"+cProcType+" processing shows no Detail records for WO# "+cWO_Num+", please determine cause."
	tattach = " "
	IF lEmail
		DO FORM m:\dev\frm\dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
	ENDIF
ENDPROC
