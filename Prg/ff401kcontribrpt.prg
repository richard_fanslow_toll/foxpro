* FF401KCONTRIBRPT.PRG
* produce weekly FF 401k contribution report for Tracy - run each Thursday for that Friday's pay....

* Build EXE as F:\UTIL\ADPREPORTS\FF401KCONTRIBRPT.EXE
LOCAL loFF401KCONTRIBRPT

runack("FF401KCONTRIBRPT")

utilsetup("FF401KCONTRIBRPT")

loFF401KCONTRIBRPT = CREATEOBJECT('FF401KCONTRIBRPT')

loFF401KCONTRIBRPT.MAIN()

schedupdate()

CLOSE DATABASES ALL
RETURN


#DEFINE LOGIT 1
#DEFINE WAITIT 2
#DEFINE NOWAITIT 4
#DEFINE SENDIT 8
#DEFINE CRLF CHR(13) + CHR(10)
#DEFINE RETURN_DATA_MANDATORY .F.
#DEFINE RETURN_DATA_NOT_MANDATORY .T.
#DEFINE CSVFORMAT 6
#DEFINE WRITE_ONLY_UNBUFFERED 11
#DEFINE COMMA ","
#DEFINE xlEdgeBottom 9
#DEFINE xlContinuous 1
#DEFINE xlMedium -4138
#DEFINE xlFONT_RED -16776961

DEFINE CLASS FF401KCONTRIBRPT AS CUSTOM

	cProcessName = 'FF401KCONTRIBRPT'

	lTestMode = .F.  && .F. FOR NORMAL OPERATION, .T. TO SUPPRESS ALL BUT DEVELOPER EMAILS AND CHANGE INPUT TABLE, ETC.

	dToday = DATE()
	*dToday = {^2013-03-27}

	lAutoYield = .T.

	cStartTime = TTOC(DATETIME())

	* connection properties
	nSQLHandle = 0
	
	* processing properties
	cDupesList = ''

	* wait window properties
	nWaitWindowTimeout = 2
	lWaitWindowIsOn = .T.

	* logfile properties
	lLoggingIsOn = .T.
	cLogFile = 'F:\UTIL\ADPREPORTS\LOGFILES\FF401KCONTRIBRPT_log.txt'

	* INTERNAL email properties
	lSendInternalEmailIsOn = .T.
	cFrom = 'Mark.Bennett@Tollgroup.com'
	cSendTo = 'Tracy.Wang@Tollgroup.com'
	cCC = 'mbennett@fmiint.com'
	cSubject = ''
	cAttach = ''
	cBodyText = ''

	cCOMPUTERNAME = ''
	cUSERNAME = ''


	FUNCTION INIT
		IF NOT DODEFAULT()
			RETURN .F.
		ENDIF
		WITH THIS
			*SET RESOURCE OFF
			CLOSE DATA
			SET DECIMALS TO 3
			SET CENTURY ON
			SET DATE AMERICAN
			SET HOURS TO 24
			SET ANSI ON
			SET TALK OFF
			SET DELETED ON
			SET CONSOLE OFF
			SET EXCLUSIVE OFF
			SET SAFETY OFF
			SET EXACT OFF
			SET STATUS BAR ON
			SET SYSMENU OFF
			SET ENGINEBEHAVIOR 70
			_VFP.AUTOYIELD = .lAutoYield
			.cCOMPUTERNAME = GETENV("COMPUTERNAME")
			.cUSERNAME = GETENV("USERNAME")
			IF .lTestMode THEN
				.cLogFile = 'F:\UTIL\ADPREPORTS\LOGFILES\FF401KCONTRIBRPT_log_TESTMODE.txt'
				.cSendTo = 'mbennett@fmiint.com'
				.cCC = ''
			ENDIF
			.lLoggingIsOn = .lLoggingIsOn AND NOT EMPTY(.cLogFile)
			IF .lLoggingIsOn THEN
				SET ALTERNATE TO (.cLogFile) ADDITIVE
				SET ALTERNATE ON
			ENDIF
			SET PROCEDURE TO VALIDATIONS ADDITIVE
		ENDWITH
	ENDFUNC


	FUNCTION DESTROY
		WITH THIS
			IF .lLoggingIsOn  THEN
				SET ALTERNATE OFF
				SET ALTERNATE TO
			ENDIF
		ENDWITH
		DODEFAULT()
	ENDFUNC


	FUNCTION MAIN
		WITH THIS
			LOCAL lnNumberOfErrors, lcSQL, lcSQL2, lcSQL3, loError, ldToday
			LOCAL lcFiletoSaveAs, lcSpreadsheetTemplate
			LOCAL ldFromDate, ldToDate, lcADPFromDate, lcADPToDate
			LOCAL lcDateSuffix, lnTotalFSA, ldEligDate
			LOCAL i, oExcel, oWorkbook, oWorksheet, lnStartRow, lcStartRow, lnRow, lcRow
			LOCAL lcFFWhere, lnPercent, lnContPCT, lnFullMatchPCT, lnQTRMatchPCT, lnERPercent, lnERMatch	


			TRY
				lnNumberOfErrors = 0

				.TrackProgress("FF 401K Contrib Report process started....", LOGIT+SENDIT+NOWAITIT)
				.TrackProgress('COMPUTERNAME = ' + .cCOMPUTERNAME, LOGIT+SENDIT)
				.TrackProgress('USERNAME = ' + .cUSERNAME, LOGIT+SENDIT)
				.TrackProgress('PROJECT = FF401KCONTRIBRPT', LOGIT+SENDIT)
				IF .lTestMode THEN
					.TrackProgress('                        TEST MODE                        ', LOGIT+SENDIT)
				ENDIF
				
				* we want to report on period from last Saturday thru upcoming Friday - this will handle cases where a Friday holiday cause payroll to run on a Thursday.

				* determine prior Saturday
				ldFromDate = .dToday
				DO WHILE DOW(ldFromDate,1) <> 7
					ldFromDate = ldFromDate - 1
				ENDDO
				ldToDate = ldFromDate + 6
				
*!*	******************************************
*!*	** activate to force specific date range
*!*	ldFromDate = {^2013-06-01}
*!*	ldToDate   = {^2013-06-07}
*!*	******************************************


				lcDateSuffix = DTOC(ldFromDate) + " - " + DTOC(ldToDate)
				.cSubject = 'FF 401K Contrib Report for ' + lcDateSuffix


				lcADPFromDate = "DATE'" + TRANSFORM(YEAR(ldFromDate)) + "-" + PADL(MONTH(ldFromDate),2,'0') + "-" + PADL(DAY(ldFromDate),2,'0') + "'"
				lcADPToDate = "DATE'" + TRANSFORM(YEAR(ldToDate)) + "-" + PADL(MONTH(ldToDate),2,'0') + "-" + PADL(DAY(ldToDate),2,'0') + "'"

				lcSpreadsheetTemplate = 'F:\UTIL\ADPREPORTS\FF401KCONTRIBRPT_TEMPLATE.XLS'
				
				lcFiletoSaveAs = 'F:\UTIL\ADPREPORTS\REPORTS\FF401KCONTRIBRPT ' + STRTRAN(lcDateSuffix,"/","") + '.XLS'
				.TrackProgress('Creating spreadsheet: ' + lcFiletoSaveAs, LOGIT+SENDIT+NOWAITIT)


				* delete output file if it already exists...
				IF FILE(lcFiletoSaveAs) THEN
					DELETE FILE (lcFiletoSaveAs)
				ENDIF

				* now connect to ADP...

				.nSQLHandle = SQLCONNECT("PCPAYWIN","MBENNETT","VP737A")

				IF .nSQLHandle > 0 THEN


					SET TEXTMERGE ON
					TEXT TO	lcFFWhere NOSHOW					
(
{fn LEFT(A.CHECKVIEWHOMEDEPT,2)} IN ('11','12','20','21','22','23','24','25','26','27','28','29','30','35','36','37','38','71','72','73','74','76','78')
OR
A.FILE# IN (3896,3892,3831,3862,3897,3830,3863,3836)
)
					ENDTEXT
					SET TEXTMERGE OFF


					
					SET TEXTMERGE ON
					TEXT TO	lcSQL NOSHOW
					
SELECT 
A.NAME,
A.COMPANYCODE AS ADP_COMP,
{fn LEFT(A.CHECKVIEWHOMEDEPT,2)} AS DIVISION,
{fn SUBSTRING(A.CHECKVIEWHOMEDEPT,3,4)} AS DEPARTMENT,
A.SOCIALSECURITY# AS SS_NUM,
SUM({FN IFNULL(A.CHECKVIEWREGERNING,0.000)}) AS REGEARNING,
SUM({FN IFNULL(A.CHECKVIEWOTEARNING,0.000)}) AS OTEARNING,
SUM({FN IFNULL(A.CHECKVIEWGROSSPAYA,0.000)}) AS GROSSPAY
FROM REPORTS.V_CHK_VW_INFO A
WHERE A.CHECKVIEWPAYDATE >= <<lcADPFromDate>> AND A.CHECKVIEWPAYDATE <= <<lcADPToDate>>
AND
<<lcFFWhere>>
GROUP BY A.NAME, A.COMPANYCODE, {fn LEFT(A.CHECKVIEWHOMEDEPT,2)}, {fn SUBSTRING(A.CHECKVIEWHOMEDEPT,3,4)}, A.SOCIALSECURITY#
ORDER BY A.NAME

					ENDTEXT
					SET TEXTMERGE OFF

					
					SET TEXTMERGE ON
					TEXT TO	lcSQL2 NOSHOW
					
SELECT 
A.NAME,
A.COMPANYCODE AS ADP_COMP,
{fn LEFT(A.CHECKVIEWHOMEDEPT,2)} AS DIVISION,
{fn SUBSTRING(A.CHECKVIEWHOMEDEPT,3,4)} AS DEPARTMENT,
SUM(DECODE(A.CHECKVIEWDEDCODE, '4', A.CHECKVIEWDEDAMT, 0.000)) AS CONT401K,
SUM(DECODE(A.CHECKVIEWDEDCODE, 'O', A.CHECKVIEWDEDAMT, 0.000)) AS LOAN1,
SUM(DECODE(A.CHECKVIEWDEDCODE, 'P', A.CHECKVIEWDEDAMT, 0.000)) AS LOAN2,
SUM(DECODE(A.CHECKVIEWDEDCODE, '21', A.CHECKVIEWDEDAMT, 0.000)) AS CATCHUP
FROM REPORTS.V_CHK_VW_DEDUCTION A
WHERE A.CHECKVIEWPAYDATE >= <<lcADPFromDate>> AND A.CHECKVIEWPAYDATE <= <<lcADPToDate>>
AND A.CHECKVIEWDEDCODE IN ('4','O','P','21')
AND
<<lcFFWhere>>
GROUP BY A.NAME, A.COMPANYCODE, {fn LEFT(A.CHECKVIEWHOMEDEPT,2)}, {fn SUBSTRING(A.CHECKVIEWHOMEDEPT,3,4)}
ORDER BY A.NAME

					ENDTEXT
					SET TEXTMERGE OFF

					
					SET TEXTMERGE ON
					TEXT TO	lcSQL3 NOSHOW
					
SELECT 
A.NAME,
A.COMPANYCODE AS ADP_COMP,
{fn LEFT(A.CHECKVIEWHOMEDEPT,2)} AS DIVISION,
{fn SUBSTRING(A.CHECKVIEWHOMEDEPT,3,4)} AS DEPARTMENT,
SUM(DECODE(A.CHECKVIEWEARNSCD, '1', A.CHECKVIEWEARNSAMT, 0.000)) AS OPTOUT 
FROM REPORTS.V_CHK_VW_EARNINGS A
WHERE A.CHECKVIEWPAYDATE >= <<lcADPFromDate>> AND A.CHECKVIEWPAYDATE <= <<lcADPToDate>>
AND A.CHECKVIEWEARNSCD IN ('1')
AND
<<lcFFWhere>>
GROUP BY A.NAME, A.COMPANYCODE, {fn LEFT(A.CHECKVIEWHOMEDEPT,2)}, {fn SUBSTRING(A.CHECKVIEWHOMEDEPT,3,4)}
ORDER BY A.NAME

					ENDTEXT
					SET TEXTMERGE OFF




*!*	(SELECT SUM({fn IFNULL(CHECKVIEWDEDAMT,0.00)}) FROM REPORTS.V_CHK_VW_DEDUCTION WHERE CHECKVIEWPAYDATE >= <<lcADPFromDate>> AND CHECKVIEWPAYDATE <= <<lcADPToDate>> AND CHECKVIEWDEDCODE IN ('P')
*!*	AND NAME = A.NAME AND {fn LEFT(CHECKVIEWHOMEDEPT,2)} = {fn LEFT(A.CHECKVIEWHOMEDEPT,2)} AND {fn SUBSTRING(CHECKVIEWHOMEDEPT,3,4)} = {fn SUBSTRING(A.CHECKVIEWHOMEDEPT,3,4)} ) AS LOAN2



					IF USED('CURFFPRE') THEN
						USE IN CURFFPRE
					ENDIF
					IF USED('CURFF') THEN
						USE IN CURFF
					ENDIF
					IF USED('CURDEDS') THEN
						USE IN CURDEDS
					ENDIF
					IF USED('CUREARNS') THEN
						USE IN CUREARNS
					ENDIF

					IF .lTestMode THEN
						.TrackProgress('lcSQL = ' + lcSQL, LOGIT+SENDIT)
						.TrackProgress('lcSQL2 = ' + lcSQL2, LOGIT+SENDIT)
						.TrackProgress('lcSQL3 = ' + lcSQL3, LOGIT+SENDIT)
					ENDIF

					IF .ExecSQL(lcSQL, 'CURFFPRE', RETURN_DATA_MANDATORY) AND ;
						.ExecSQL(lcSQL2, 'CURDEDS', RETURN_DATA_MANDATORY) AND ;						
						.ExecSQL(lcSQL3, 'CUREARNS', RETURN_DATA_MANDATORY) THEN

						
*!*							SELECT CURDEDS
*!*							BROWSE
*!*							SELECT CUREARNS
*!*							BROWSE
*!*							
*!*							THROW
						
						IF USED('CURFFPRE') AND NOT EOF('CURFFPRE') THEN
												
						
							SELECT lcDateSuffix AS DATERANGE, A.*, ;
								NVL(B.LOAN2,000000.000) AS LOAN2, ;
								NVL(B.LOAN1,000000.000) AS LOAN1, ;
								NVL(B.CONT401K,000000.000) AS CONT401K, ;
								NVL(B.CATCHUP,000000.000) AS CATCHUP, ;
								NVL(C.OPTOUT,000000.000) AS OPTOUT, ;
								000000.000 AS BASEAMT, ;
								000.000 AS CONTPCT, ;
								000.000 AS AT100PCT, ;
								000.000 AS AT25PCT, ;
								000.000 AS ERPCT, ;
								000000.000 AS ERMATCH, ;
								SPACE(100) AS COMMENT ;
								FROM CURFFPRE A ;
								LEFT OUTER JOIN CURDEDS B ;
								ON B.NAME = A.NAME AND B.ADP_COMP = A.ADP_COMP AND B.DIVISION = A.DIVISION AND B.DEPARTMENT = A.DEPARTMENT ;
								LEFT OUTER JOIN CUREARNS C ;
								ON C.NAME = A.NAME AND C.ADP_COMP = A.ADP_COMP AND C.DIVISION = A.DIVISION AND C.DEPARTMENT = A.DEPARTMENT ;
								INTO CURSOR CURFF ;
								ORDER BY A.NAME ;
								READWRITE
								
								* FROM TRACY
*!*	I need a base amount to calculate ER contribution. For Salary employees , the base amount would be "regearning + medical insurance compensation". 
*!*	Car allowance, phone allowance, & commission/bonus are not subject to ER contribution.

*!*	For hourly employees, the base amount should be the " Grosspay".

*!*	To calculate the contribution % - use EE 401K contribution ( column BX ) divide by the base amount. 

*!*	Rule for ER contribution - 100% for the first EE 1%, 25% for each additional % EE contribution for up to 5 %.

*!*	If a EE contributes 5% or more, ER match would be ( 1% + 1%)    
*!*	EE - 4%, ER = 1.75%
*!*	EE - 3%, ER = 1.5%
*!*	EE- 2%, ER = 1.25%

*!*	Once the ER % is determined, use the base amount multiply the ER % to come up with the ER contribution amount.

*!*	SELECT CURFF
*!*	BROWSE
*!*	throw

							
							* POPULATE BASEAMT
							SELECT CURFF
							SCAN
								IF ALLTRIM(CURFF.ADP_COMP) = "E87" THEN
									* HOURLY EMPLOYEE
									REPLACE CURFF.BASEAMT WITH CURFF.GROSSPAY IN CURFF
								ELSE
									* SALARIED
									REPLACE CURFF.BASEAMT WITH (CURFF.REGEARNING + CURFF.OPTOUT) IN CURFF
								ENDIF
							ENDSCAN	
							
							* CALC CONTRIBUTION %						
							SELECT CURFF
							SCAN
								* added logic to handle non-positive baseamt 06/13/2013 MB
								IF CURFF.BASEAMT > 0.00 THEN
									lnPercent = (CURFF.CONT401K / CURFF.BASEAMT) * 100.000
									REPLACE CURFF.CONTPCT WITH lnPercent IN CURFF
								ELSE
									* we can't do the calc if baseamt = 0 because of division-by-zero; and it makes not sense to do it if baseamt is negative, so just create 0 lnPercent
									REPLACE CURFF.CONTPCT WITH 0.00, CURFF.COMMENT WITH "Non-positive Base Amount; no calculations were done." IN CURFF
								ENDIF
							ENDSCAN	
							
							
							* CALC ER% AND ER MATCH					
							SELECT CURFF
							SCAN
								lnContPCT = CURFF.CONTPCT
								
								IF lnContPCT >= 1.000 THEN
									lnFullMatchPCT = 1.000
									lnQTRMatchPCT = lnContPCT - 1.000
								ELSE
									lnFullMatchPCT = lnContPCT
									lnQTRMatchPCT = 0.000
								ENDIF
								
								* 25% match is only up to a total of 5%, meaning it is capped at 4% beyond the 1 % full match
								IF lnQTRMatchPCT > 4.000 THEN
									lnQTRMatchPCT = 4.000
								ENDIF
								
								lnERPercent = lnFullMatchPCT + (0.250 * lnQTRMatchPCT)
								
								lnERMatch = (lnERPercent * CURFF.BASEAMT) /100.000								
								
								REPLACE CURFF.AT100PCT WITH lnFullMatchPCT, CURFF.AT25PCT WITH lnQTRMatchPCT, CURFF.ERPCT WITH lnERPercent, CURFF.ERMATCH WITH lnERMatch IN CURFF
								
							ENDSCAN		

						

*!*			SELECT CURFF
*!*			BROWSE

*!*			SELECT CURFF
*!*			COPY TO c:\a\tracy.xls xl5


						* build warning list if a name appears more than once
						SELECT NAME, COUNT(*) AS CNT ;
							FROM CURFF ;
							INTO CURSOR CURDUPES ;
							GROUP BY 1 ;
							ORDER BY 1 ;
							HAVING CNT > 1
							
						IF USED('CURDUPES') AND NOT EOF('CURDUPES') THEN
							.cDupesList = 'WARNING: the following employees appear in more than one spreadsheet row because they changed demographic data during the reporting period:'	+ CRLF + CRLF					
							SELECT CURDUPES
							SCAN
								.cDupesList = .cDupesList + CURDUPES.NAME + CRLF
							ENDSCAN
						ELSE
							.cDupesList = ''						
						ENDIF
						

						oExcel = CREATEOBJECT("excel.application")
						oExcel.displayalerts = .F.
						oExcel.VISIBLE = .F.
						oWorkbook = oExcel.workbooks.OPEN(lcSpreadsheetTemplate)
						oWorkbook.SAVEAS(lcFiletoSaveAs)
						oWorksheet = oWorkbook.Worksheets[1]

						lnRow = 1

						SELECT CURFF 
						SCAN
							lnRow = lnRow + 1
							lcRow = ALLTRIM(STR(lnRow))
							oWorksheet.RANGE("A"+lcRow).VALUE = CURFF.DATERANGE
							oWorksheet.RANGE("B"+lcRow).VALUE = CURFF.NAME
							oWorksheet.RANGE("C"+lcRow).VALUE = "'" + CURFF.SS_NUM
							
							
							oWorksheet.RANGE("D"+lcRow).VALUE = CURFF.ADP_COMP
							oWorksheet.RANGE("E"+lcRow).VALUE = CURFF.DIVISION
							oWorksheet.RANGE("F"+lcRow).VALUE = CURFF.DEPARTMENT
							oWorksheet.RANGE("G"+lcRow).VALUE = CURFF.REGEARNING
							oWorksheet.RANGE("H"+lcRow).VALUE = CURFF.OTEARNING
							oWorksheet.RANGE("I"+lcRow).VALUE = CURFF.GROSSPAY
							oWorksheet.RANGE("J"+lcRow).VALUE = CURFF.LOAN2
							oWorksheet.RANGE("K"+lcRow).VALUE = CURFF.LOAN1
							oWorksheet.RANGE("L"+lcRow).VALUE = CURFF.CONT401K
							oWorksheet.RANGE("M"+lcRow).VALUE = CURFF.CATCHUP
							oWorksheet.RANGE("N"+lcRow).VALUE = CURFF.OPTOUT
							oWorksheet.RANGE("O"+lcRow).VALUE = CURFF.BASEAMT
							oWorksheet.RANGE("P"+lcRow).VALUE = CURFF.CONTPCT
							oWorksheet.RANGE("Q"+lcRow).VALUE = CURFF.AT100PCT
							oWorksheet.RANGE("R"+lcRow).VALUE = CURFF.AT25PCT
							oWorksheet.RANGE("S"+lcRow).VALUE = CURFF.ERPCT
							oWorksheet.RANGE("T"+lcRow).VALUE = CURFF.ERMATCH
							oWorksheet.RANGE("U"+lcRow).VALUE = CURFF.COMMENT
						ENDSCAN

						IF TYPE('oWorkbook') = "O" THEN
							oWorkbook.SAVE()
							oWorkbook = NULL
						ENDIF
						IF TYPE('oExcel') = "O" THEN
							oExcel.QUIT()
							oExcel = NULL
						ENDIF


						IF FILE(lcFiletoSaveAs) THEN
							.cAttach = lcFiletoSaveAs
							.cBodyText = "See attached report." + CRLF + CRLF + "<<REPORT LOG FOLLOWS>>" + CRLF + .cBodyText
						ELSE
							.TrackProgress("ERROR attaching " + lcFiletoSaveAs, LOGIT+SENDIT+NOWAITIT)
						ENDIF

						ELSE
							.TrackProgress('No paydata was found for the date range!', LOGIT+SENDIT)

						ENDIF  &&  USED('CURFFPRE') AND NOT EOF('CURFFPRE')
						
						IF NOT EMPTY(.cDupesList) THEN
							.cBodyText =  .cDupesList + CRLF + .cBodyText
						ENDIF


*!*							=SQLDISCONNECT(.nSQLHandle)

					ENDIF  &&  .ExecSQL(lcSQL, 'SQLCURSOR1', RETURN_DATA_MANDATORY)
				ELSE
					* connection error
					.TrackProgress('Unable to connect to ADP Payroll System.', LOGIT+SENDIT)
				ENDIF   &&  .nSQLHandle > 0

				.TrackProgress("FF 401K Contrib Report process ended normally.", LOGIT+SENDIT+NOWAITIT)


			CATCH TO loError

				.TrackProgress('There was an error.',LOGIT+SENDIT)
				.TrackProgress('The Error # is: ' + TRANSFORM(loError.ERRORNO), LOGIT+SENDIT)
				.TrackProgress('The Error Message is: ' + TRANSFORM(loError.MESSAGE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred in Program: ' + TRANSFORM(loError.PROCEDURE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred at line #: ' + TRANSFORM(loError.LINENO), LOGIT+SENDIT)
				lnNumberOfErrors = lnNumberOfErrors + 1

				IF TYPE('oWorkbook') = "O" AND NOT ISNULL(oWorkbook) THEN
					oWorkbook.SAVE()
				ENDIF
				IF TYPE('oExcel') = "O" AND NOT ISNULL(oExcel) THEN
					oExcel.QUIT()
				ENDIF
				
				CLOSE DATABASES ALL

			ENDTRY

			WAIT CLEAR
			***************** INTERNAL email results ******************************

			.TrackProgress('About to send status email.',LOGIT)
			.TrackProgress('The logfile is: ' + .cLogFile, SENDIT)
			.TrackProgress("FF 401K Contrib Report process started: " + .cStartTime, LOGIT+SENDIT)
			.TrackProgress("FF 401K Contrib Report process finished: " + TTOC(DATETIME()), LOGIT+SENDIT)

			IF .lSendInternalEmailIsOn THEN
				DO FORM dartmail2 WITH .cSendTo,.cFrom,.cSubject,.cCC,.cAttach,.cBodyText,"A"
				.TrackProgress('Sent status email.',LOGIT)
			ELSE
				.TrackProgress('Did not send status email: THIS.lSendInternalEmailIsOn = FALSE.',LOGIT)
			ENDIF 

		ENDWITH
		RETURN
	ENDFUNC && main


	FUNCTION ExecSQL
		LPARAMETERS tcSQL, tcCursorName, tlNoDataReturnedIsOkay
		LOCAL llRetval, lnResult
		WITH THIS
			* close target cursor if it's open
			IF USED(tcCursorName)
				USE IN (tcCursorName)
			ENDIF
			WAIT WINDOW "Fetching remote data; please wait..." NOWAIT
			lnResult = SQLEXEC(.nSQLHandle, tcSQL, tcCursorName)
			llRetval = ( lnResult > 0 )
			IF llRetval THEN
				* see if any data came back
				IF NOT tlNoDataReturnedIsOkay THEN
					IF (NOT USED(tcCursorName)) OR EOF(tcCursorName) THEN
						llRetval = .F.
						.TrackProgress('No data was returned by this query: ' +CRLF + tcSQL, LOGIT+SENDIT)
						.cSendTo = 'Mark Bennett <mbennett@fmiint.com>'
					ENDIF
				ENDIF
			ELSE
				.TrackProgress("Result Code [" + TRANSFORM(lnResult) + "] returned by this query: " + tcSQL, LOGIT+SENDIT)
				.cSendTo = 'Mark Bennett <mbennett@fmiint.com>'
			ENDIF
			WAIT CLEAR
			RETURN llRetval
		ENDWITH
	ENDFUNC


	PROCEDURE TrackProgress
		* do any combination of Wait Window, writing to logfile, and adding to body of email,
		* based on nFlags parameter.
		LPARAMETERS tcExpression, tnFlags
		WITH THIS
			IF BITAND(tnFlags,LOGIT) = LOGIT THEN
				IF .lLoggingIsOn THEN
					?
					? .cProcessName + "  " + TTOC(DATETIME()) + ": " + tcExpression
				ENDIF
			ENDIF
			IF .lWaitWindowIsOn THEN
				IF BITAND(tnFlags,NOWAITIT) = NOWAITIT THEN
					WAIT WINDOW tcExpression NOWAIT
				ENDIF
				IF BITAND(tnFlags,WAITIT) = WAITIT THEN
					WAIT WINDOW tcExpression TIMEOUT .nWaitWindowTimeout
				ENDIF
			ENDIF
			IF BITAND(tnFlags,SENDIT) = SENDIT THEN
				IF .lSendInternalEmailIsOn THEN
					.cBodyText = .cBodyText + tcExpression + CRLF + CRLF
				ENDIF
			ENDIF
		ENDWITH
	ENDPROC  &&  TrackProgress


	FUNCTION GetSQLStartDateTimeString
		* returns a timestamp string for use in SQL queries
		LPARAMETERS tdDate
		LOCAL lcSetDate, lcSQLDateTimeString
		lcSetDate = SET('DATE')
		SET DATE YMD
		lcSQLDateTimeString   = "{ts '" + TRANSFORM(YEAR(tdDate)) + "-" + PADL(MONTH(tdDate),2,"0") + "-" + PADL(DAY(tdDate),2,"0") + " 00:00:00.0'}"
		SET DATE &lcSetDate.
		RETURN lcSQLDateTimeString
	ENDFUNC


	FUNCTION GetSQLEndDateTimeString
		* returns a timestamp string for use in SQL queries
		LPARAMETERS tdDate
		LOCAL lcSetDate, lcSQLDateTimeString
		lcSetDate = SET('DATE')
		SET DATE YMD
		lcSQLDateTimeString = "{ts '" + TRANSFORM(YEAR(tdDate)) + "-" + PADL(MONTH(tdDate),2,"0") + "-" + PADL(DAY(tdDate),2,"0") + " 23:59:59.9'}"
		SET DATE &lcSetDate.
		RETURN lcSQLDateTimeString
	ENDFUNC
	
	


ENDDEFINE
