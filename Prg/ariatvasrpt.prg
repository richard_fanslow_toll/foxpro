* ariatvasrpt.prg
* based on m:\darren\vascheri.prg (thanks, Darren!)
* programmer: Mark Bennett

LOCAL lnError, lcProcessName, loARIATVASREPORT, lTestMode

lTestMode = .T.

IF NOT lTestMode THEN
	runack("ARIATVASREPORT")
ENDIF

DECLARE INTEGER CreateMutex IN Win32API ;
	STRING @lpMutexAttributes, ;
	STRING bInitialOwner, ;
	STRING @lpName

DECLARE INTEGER GetLastError IN Win32API

* if running in EXE check to see if already running
IF _VFP.STARTMODE > 0 THEN
	lcProcessName = "ARIATVASREPORT"
	CreateMutex(0,CHR(1),@lcProcessName)
	lnError = GetLastError()
	IF lnError = 183
		WAIT WINDOW TIMEOUT 10 "Process is already running..."
		RETURN .F.
	ENDIF
ENDIF

utilsetup("ARIATVASREPORT")

loARIATVASREPORT = CREATEOBJECT('ARIATVASREPORT')
loARIATVASREPORT.MAIN( lTestMode )

IF NOT lTestMode THEN
	schedupdate()
ENDIF

CLOSE DATABASES ALL
RETURN

#DEFINE LOGIT 1
#DEFINE WAITIT 2
#DEFINE NOWAITIT 4
#DEFINE SENDIT 8
#DEFINE CRLF CHR(13) + CHR(10)
#DEFINE LF CHR(10)
#DEFINE RETURN_DATA_MANDATORY .F.
#DEFINE RETURN_DATA_NOT_MANDATORY .T.

DEFINE CLASS ARIATVASREPORT AS CUSTOM

	cProcessName = 'ARIATVASREPORT'

	lTestMode = .T.  && .F. FOR NORMAL OPERATION, .T. TO SUPPRESS ALL BUT DEVELOPER EMAILS AND CHANGE INPUT TABLE, ETC.

	lAutoYield = .T.
	cStartTime = TTOC(DATETIME())

	* connection properties
	nSQLHandle = 0

	* file properties
	nFileHandle = 0

	* date properties
	dtNow = DATETIME()

	dToday = DATE()

	* processing props
	nMonth = 0
	nYear = 0
	cMod = 'E'
	cReportPath = 'F:\UTIL\ARIAT\REPORTS\'

	* wait window properties
	nWaitWindowTimeout = 2
	lWaitWindowIsOn = .T.

	* logfile properties
	lLoggingIsOn = .T.
	cLogFile = 'F:\UTIL\ARIAT\LOGFILES\ARIATVASREPORT_log.txt'

	* INTERNAL email properties
	lSendInternalEmailIsOn = .T.
	cFrom = 'mark.bennett@tollgroup.com'
	cSendTo = 'mbennett@fmiint.com'
	cCC = ''
	cSubject = 'ARIAT WIP Report for: ' + DTOC(DATE())
	cAttach = ''
	cBodyText = ''

	cCOMPUTERNAME = ''
	cUSERNAME = ''

	FUNCTION INIT
		IF NOT DODEFAULT()
			RETURN .F.
		ENDIF
		WITH THIS
			CLEAR
			*SET RESOURCE OFF
			CLOSE DATA
			SET CENTURY ON
			SET DATE AMERICAN
			SET HOURS TO 24
			SET ANSI ON
			SET TALK OFF
			SET DELETED ON
			SET CONSOLE OFF
			SET EXCLUSIVE OFF
			SET SAFETY OFF
			SET EXACT OFF
			SET STATUS BAR ON
			SET SYSMENU OFF
			SET ENGINEBEHAVIOR 70
			_VFP.AUTOYIELD = .lAutoYield
			.cCOMPUTERNAME = GETENV("COMPUTERNAME")
			.cUSERNAME = GETENV("USERNAME")
			IF .lTestMode THEN
				.cSendTo = 'mbennett@fmiint.com'
				.cLogFile = 'F:\UTIL\ARIAT\Logfiles\ARIATVASREPORT_log_TESTMODE.txt'
			ENDIF
			.lLoggingIsOn = .lLoggingIsOn AND NOT EMPTY(.cLogFile)
			IF .lLoggingIsOn THEN
				SET ALTERNATE TO (.cLogFile) ADDITIVE
				SET ALTERNATE ON
			ENDIF
		ENDWITH
	ENDFUNC


	FUNCTION DESTROY
		WITH THIS
			IF .lLoggingIsOn  THEN
				SET ALTERNATE OFF
				SET ALTERNATE TO
			ENDIF
		ENDWITH
		DODEFAULT()
	ENDFUNC


	FUNCTION MAIN
		LPARAMETERS tlTestMode

		WITH THIS

			.lTestMode = tlTestMode

			LOCAL lcXLSfolder, lnNumberOfErrors, ldToday, lcReportFile

			lnNumberOfErrors = 0

			TRY
				* NOTE: code below was based on Darren's Lifewip.prg.

				ldToday = .dToday


WAIT WINDOW NOWAIT "Gathering data for VAS report - please wait...."

LOCAL ldToday, ldStartDate, lcFileToSaveAs, lcTemplateFile, loExcel, loWorkbook, lnRow, lcRow, lcDecimals
LOCAL lnVASFrom, lnVASTo, i, lcVASCode, lcRRC_Num, lnRRCLen, lcRRC_NumCommas, lcChar

lnVASFrom = 1
lnVASTo = 29

lcDecimals = SET('DECIMALS')
SET DECIMALS TO 2

ldToday = DATE()
ldStartDate = ldToday - DAY(ldToday) + 1   && first of current month
ldStartDate = GOMONTH(ldStartDate,-12)  && this is first day of 12 months ago

LOCAL lcSendTo, lcFrom, lcSubject, lcCC, lcAttach, lcBodyText

lcFrom = "tollsupport@tollgroup.com"
lcSendTo = "MBennett@fmiint.com"
lcCC = ""
lcSubject = "Ariat VAS Report for " + DTOC(ldToday)

lcTemplateFile = 'F:\UTIL\ARIAT\TEMPLATES\ARIATVASRPT_TEMPLATE01.XLS'
lcFileToSaveAs = 'F:\UTIL\ARIAT\REPORTS\ARIATVASRPT_' + DTOS(ldToday) + '.XLS' 

*close databases all
SET DELETED ON
SET SAFETY OFF
SET TALK OFF

*!*	CREATE CURSOR xrpt (zmonth c(4), vaspt N(5), totpt N(5), pctvas N(6,4), CMONTH c(2), cyear c(4))
*!*	INDEX ON zmonth TAG zmonth

xsqlexec("select * from outship where mod='k'","outship",,"wh")

* NOTE: because Ariat does not use the rrc_num field (it does not do 754s), Darren is using that to store the VAS codes. Multiples separated by commas: 01,28, etc.
SELECT ship_ref, del_date, vas, rrc_num, qty, 00000000 AS vasqty, SPACE(4) AS zmonth, 0000000 as vaspt, 0000000 as totpt, SPACE(2) AS cmonth, SPACE(4) AS cyear, ;
	00000000 as v01qty, 00000000 as v02qty, 00000000 as v03qty, 00000000 as v04qty, 00000000 as v05qty, 00000000 as v06qty, 00000000 as v07qty, 00000000 as v08qty, 00000000 as v09qty, 00000000 as v10qty, ;
	00000000 as v11qty, 00000000 as v12qty, 00000000 as v13qty, 00000000 as v14qty, 00000000 as v15qty, 00000000 as v16qty, 00000000 as v17qty, 00000000 as v18qty, 00000000 as v19qty, 00000000 as v20qty, ;
	00000000 as v21qty, 00000000 as v22qty, 00000000 as v23qty, 00000000 as v24qty, 00000000 as v25qty, 00000000 as v26qty, 00000000 as v27qty, 00000000 as v28qty, 00000000 as v29qty ;
	FROM outship ;
	INTO CURSOR CUROUTSHIP ;
	WHERE (wo_date >= ldStartDate) AND !EMPTY(del_date) ;
	READWRITE
	
*!*	SELECT CUROUTSHIP
*!*	BROWSE
	
	
*!*	*****************************************************************
*!*	*** FOR TESTING - REMOVE WHEN THERE IS REAL DATA!!!
*!*	SELECT CUROUTSHIP
*!*	GO BOTTOM
*!*	REPLACE CUROUTSHIP.RRC_NUM WITH "01,29" IN CUROUTSHIP
*!*	*BROWSE
*!*	*****************************************************************

SELECT CUROUTSHIP
SCAN
	REPLACE CUROUTSHIP.cmonth WITH PADL(MONTH(CUROUTSHIP.del_date),2,"0"), ;
		CUROUTSHIP.cyear WITH ALLTRIM(STR(YEAR(CUROUTSHIP.del_date),4,0)), ;
		CUROUTSHIP.totpt WITH 1, ;
		CUROUTSHIP.vasqty WITH IIF(CUROUTSHIP.vas,CUROUTSHIP.qty,0), ;
		CUROUTSHIP.vaspt WITH IIF(CUROUTSHIP.vas,1,0) IN CUROUTSHIP
		
		lcRRC_Num = ALLTRIM(CUROUTSHIP.rrc_num)
		IF (NOT EMPTY(lcRRC_Num)) AND (NOT ',' $ lcRRC_Num) THEN
			* DY is concatenating the VAS codes, without commas in between, in the rrc_num field.
			* So rewrite the rrc_num string with commas between every 2 characters,
			* so we can use the simple $ function below to see if it contains each VAS code.
			lcRRC_NumCommas = ''
			lnRRCLen = LEN(lcRRC_Num)
			FOR i = 1 TO lnRRCLen
				lcChar = SUBSTR(lcRRC_Num,i,1)
				lcRRC_NumCommas = lcRRC_NumCommas + lcChar
				IF MOD(i,2) = 0 THEN
					lcRRC_NumCommas = lcRRC_NumCommas + ','
				ENDIF
			ENDFOR
			
			REPLACE CUROUTSHIP.RRC_NUM WITH lcRRC_NumCommas IN CUROUTSHIP  && optional, activate so we can browse to see the reformatted strings
			
			* now check for existence of all 29 VAS codes in the reformatted string.
			FOR i = lnVASFrom TO lnVASTo
				lcVASCode = PADL(i,2,"0")
				IF lcVASCode $ lcRRC_NumCommas THEN
					REPLACE v&lcVASCode.QTY WITH CUROUTSHIP.QTY IN CUROUTSHIP
				ENDIF		
			ENDFOR
		ENDIF && (NOT EMPTY(lcRRC_Num)) AND (NOT ',' $ lcRRC_Num)		
ENDSCAN

*!*	SELECT CUROUTSHIP
*!*	BROWSE

*!*	CLOSE DATABASES all
*!*	RETURN

SELECT ;
	000.00 AS PCTVAS, ;
	CMONTH, ;
	CYEAR, ;
	SUM(QTY) AS QTY, ;
	SUM(VASQTY) AS VASQTY, ;
	SUM(TOTPT) AS TOTPT, ;
	SUM(VASPT) AS VASPT, ;
	SUM(V01QTY) AS V01QTY, ;
	SUM(V02QTY) AS V02QTY, ;
	SUM(V03QTY) AS V03QTY, ;
	SUM(V04QTY) AS V04QTY, ;
	SUM(V05QTY) AS V05QTY, ;
	SUM(V06QTY) AS V06QTY, ;
	SUM(V07QTY) AS V07QTY, ;
	SUM(V08QTY) AS V08QTY, ;
	SUM(V09QTY) AS V09QTY, ;
	SUM(V10QTY) AS V10QTY, ;
	SUM(V11QTY) AS V11QTY, ;
	SUM(V12QTY) AS V12QTY, ;
	SUM(V13QTY) AS V13QTY, ;
	SUM(V14QTY) AS V14QTY, ;
	SUM(V15QTY) AS V15QTY, ;
	SUM(V16QTY) AS V16QTY, ;
	SUM(V17QTY) AS V17QTY, ;
	SUM(V18QTY) AS V18QTY, ;
	SUM(V19QTY) AS V19QTY, ;
	SUM(V20QTY) AS V20QTY, ;
	SUM(V21QTY) AS V21QTY, ;
	SUM(V22QTY) AS V22QTY, ;
	SUM(V23QTY) AS V23QTY, ;
	SUM(V24QTY) AS V24QTY, ;
	SUM(V25QTY) AS V25QTY, ;
	SUM(V26QTY) AS V26QTY, ;
	SUM(V27QTY) AS V27QTY, ;
	SUM(V28QTY) AS V28QTY, ;
	SUM(V29QTY) AS V29QTY ;
	FROM CUROUTSHIP ;
	INTO CURSOR CURFINAL ;
	GROUP BY CMONTH, CYEAR ;
	ORDER BY CYEAR, CMONTH ;
	READWRITE

SELECT CURFINAL
SCAN
	REPLACE CURFINAL.pctvas WITH (CURFINAL.VASPT / CURFINAL.TOTPT) IN CURFINAL
ENDSCAN

WAIT WINDOW NOWAIT "Opening Excel - please wait...."

loExcel = CREATEOBJECT("Excel.Application")
loExcel.VISIBLE = .F.
loExcel.displayalerts = .F.

loWorkbook = loExcel.workbooks.OPEN(lcTemplateFile)
loWorkbook.SAVEAS(lcFileToSaveAs)

loWorksheet = loWorkbook.Worksheets[1]
				
WAIT WINDOW NOWAIT "Creating spreadsheet - please wait...."
lnRow = 2
SELECT CURFINAL
SCAN
	lnRow = lnRow + 1
	lcRow = ALLTRIM(STR(lnRow))
	
	loWorksheet.RANGE("A"+lcRow).VALUE = "'" + CURFINAL.cmonth
	loWorksheet.RANGE("B"+lcRow).VALUE = "'" + CURFINAL.cyear
	loWorksheet.RANGE("C"+lcRow).VALUE = CURFINAL.vaspt
	loWorksheet.RANGE("D"+lcRow).VALUE = CURFINAL.totpt
	loWorksheet.RANGE("E"+lcRow).VALUE = CURFINAL.pctvas
	
	loWorksheet.RANGE("F"+lcRow).VALUE = CURFINAL.v01qty	
	loWorksheet.RANGE("G"+lcRow).VALUE = CURFINAL.v02qty
	loWorksheet.RANGE("H"+lcRow).VALUE = CURFINAL.v03qty
	loWorksheet.RANGE("I"+lcRow).VALUE = CURFINAL.v04qty
	loWorksheet.RANGE("J"+lcRow).VALUE = CURFINAL.v05qty
	loWorksheet.RANGE("K"+lcRow).VALUE = CURFINAL.v06qty
	loWorksheet.RANGE("L"+lcRow).VALUE = CURFINAL.v07qty
	loWorksheet.RANGE("M"+lcRow).VALUE = CURFINAL.v08qty
	loWorksheet.RANGE("N"+lcRow).VALUE = CURFINAL.v09qty
	loWorksheet.RANGE("O"+lcRow).VALUE = CURFINAL.v10qty
	
	loWorksheet.RANGE("P"+lcRow).VALUE = CURFINAL.v11qty	
	loWorksheet.RANGE("Q"+lcRow).VALUE = CURFINAL.v12qty
	loWorksheet.RANGE("R"+lcRow).VALUE = CURFINAL.v13qty
	loWorksheet.RANGE("S"+lcRow).VALUE = CURFINAL.v14qty
	loWorksheet.RANGE("T"+lcRow).VALUE = CURFINAL.v15qty
	loWorksheet.RANGE("U"+lcRow).VALUE = CURFINAL.v16qty
	loWorksheet.RANGE("V"+lcRow).VALUE = CURFINAL.v17qty
	loWorksheet.RANGE("W"+lcRow).VALUE = CURFINAL.v18qty
	loWorksheet.RANGE("X"+lcRow).VALUE = CURFINAL.v19qty
	loWorksheet.RANGE("Y"+lcRow).VALUE = CURFINAL.v20qty
	
	loWorksheet.RANGE("Z"+lcRow).VALUE = CURFINAL.v21qty	
	loWorksheet.RANGE("AA"+lcRow).VALUE = CURFINAL.v22qty
	loWorksheet.RANGE("AB"+lcRow).VALUE = CURFINAL.v23qty
	loWorksheet.RANGE("AC"+lcRow).VALUE = CURFINAL.v24qty
	loWorksheet.RANGE("AD"+lcRow).VALUE = CURFINAL.v25qty
	loWorksheet.RANGE("AE"+lcRow).VALUE = CURFINAL.v26qty
	loWorksheet.RANGE("AF"+lcRow).VALUE = CURFINAL.v27qty
	loWorksheet.RANGE("AG"+lcRow).VALUE = CURFINAL.v28qty
	loWorksheet.RANGE("AH"+lcRow).VALUE = CURFINAL.v29qty
	
ENDSCAN

loWorkbook.SAVE()
loExcel.QUIT()				

WAIT CLEAR

IF FILE(lcFileToSaveAs) THEN
	=MESSAGEBOX("Spreadsheet will be emailed to: " + lcSendTo)
	lcAttach = lcFileToSaveAs
	lcBodyText = "See attached Ariat VAS spreadsheet."
	DO FORM M:\DEV\FRM\dartmail2 WITH lcSendTo, lcFrom, lcSubject, lcCC, lcAttach, lcBodyText,"A"
ELSE
	=MESSAGEBOX("ERROR: there was a problem creating the Ariat VAS spreadsheet.")
ENDIF


IF USED('CUROUTSHIP') THEN
	USE IN CUROUTSHIP
ENDIF
IF USED('CURFINAL') THEN
	USE IN CURFINAL
ENDIF

SET DECIMALS TO &lcDecimals.

				.TrackProgress('Process ' + .cProcessName + ' ended normally.', LOGIT+SENDIT)

			CATCH TO loError

				.TrackProgress('There was an error.',LOGIT+SENDIT)
				.TrackProgress('The Error # is: ' + TRANSFORM(loError.ERRORNO), LOGIT+SENDIT)
				.TrackProgress('The Error Message is: ' + TRANSFORM(loError.MESSAGE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred in Program: ' + TRANSFORM(loError.PROCEDURE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred at line #: ' + TRANSFORM(loError.LINENO), LOGIT+SENDIT)
				lnNumberOfErrors = lnNumberOfErrors + 1

			ENDTRY

			CLOSE DATA
			WAIT CLEAR
			***************** INTERNAL email results ******************************
			.TrackProgress('About to send status email.',LOGIT)
			.TrackProgress('==================================================================================================================', SENDIT)
			.TrackProgress('The logfile is: ' + .cLogFile, SENDIT)
			.TrackProgress(.cProcessName + ' process started: ' + .cStartTime, LOGIT+SENDIT)
			.TrackProgress(.cProcessName + ' process finished: ' + TTOC(DATETIME()), LOGIT+SENDIT)

			IF .lSendInternalEmailIsOn THEN
				* try to trap error from not having dartmail dll's registered on user's pc...
				TRY
					DO FORM dartmail2 WITH .cSendTo,.cFrom,.cSubject,.cCC,.cAttach,.cBodyText,"A"
					.TrackProgress('Sent status email.',LOGIT)
				CATCH TO loError
					.TrackProgress('There was an error sending the status email.',LOGIT)
					.TrackProgress('The Error # is: ' + TRANSFORM(loError.ERRORNO), LOGIT)
					.TrackProgress('The Error Message is: ' + TRANSFORM(loError.MESSAGE), LOGIT)
					.TrackProgress('The Error occurred in Program: ' + TRANSFORM(loError.PROCEDURE), LOGIT)
					.TrackProgress('The Error occurred at line #: ' + TRANSFORM(loError.LINENO), LOGIT)
					lnNumberOfErrors = lnNumberOfErrors + 1
				ENDTRY

			ELSE
				.TrackProgress('Did not send status email: THIS.lSendInternalEmailIsOn = FALSE.',LOGIT)
			ENDIF

		ENDWITH
		RETURN
	ENDFUNC && main


	PROCEDURE TrackProgress
		* do any combination of Wait Window, writing to logfile, and adding to body of email,
		* based on nFlags parameter.
		LPARAMETERS tcExpression, tnFlags
		WITH THIS
			IF BITAND(tnFlags,LOGIT) = LOGIT THEN
				IF .lLoggingIsOn THEN
					?
					? .cProcessName + "  " + TTOC(DATETIME()) + ": " + tcExpression
				ENDIF
			ENDIF
			IF .lWaitWindowIsOn THEN
				IF BITAND(tnFlags,NOWAITIT) = NOWAITIT THEN
					WAIT WINDOW tcExpression NOWAIT
				ENDIF
				IF BITAND(tnFlags,WAITIT) = WAITIT THEN
					WAIT WINDOW tcExpression TIMEOUT .nWaitWindowTimeout
				ENDIF
			ENDIF
			IF BITAND(tnFlags,SENDIT) = SENDIT THEN
				IF .lSendInternalEmailIsOn THEN
					.cBodyText = .cBodyText + tcExpression + CRLF + CRLF
				ENDIF
			ENDIF
		ENDWITH
	ENDPROC  &&  TrackProgress


ENDDEFINE

