Parameters cRunType,nWO_Num,cOffice,nAcctNum,nTripId

cRunType = "VFDDL"
c214Type = "VFDDL"

*cRunType = "VFDPU"
*c214Type = "VFDPU"


nWO_Num = 1529711
cOffice = "C"
nAcctNum = 687

Close Data All

Public c_CntrlNum,c_GrpCntrlNum,cWO_Num,cGroupName,cDivision,cCustName,cFilename,div214,cST_CSZ
Public tsendto,tcc,tsendtoerr,tccerr,tFrom,cPrgName

cFilename = ""
cPrgName = "VANITY FAIR 214"

lTesting = .f.
If !Used('account')
	xsqlexec("select * from account where inactive=0","account",,"qq")
	index on accountid tag accountid
	set order to
Endif

On Escape Cancel
lFClose = .T.
lIsError = .F.
lDoCatch = .T.
cShip_ref = ""
cST_CSZ = ""
nFilenum = 0
nAcctNum = 687
*ASSERT .f.
Try
  cSystemName = "UNASSIGNED"
  cWO_Num = ""
  lEmail = .F.
  lDoError = .F.
  lFilesOut = .T.
  If Vartype(nWO_Num)= "L"
    If lTesting
      Close Databases All
      lFilesOut = .F.
      nWO_Num = 711024
      cOffice = "N"
      cRunType = "E"
      nTripId =  947942
    Else
      Wait Window "No params specified" Timeout 2
      lDoCatch = .F.
      Throw
    Endif
  Endif

  cDivision = Iif(cOffice="C","California",Iif(cOffice="M","Miami","New Jersey"))
  cOffCity = Iif(cOffice="C","SAN PEDRO",Iif(cOffice="M","MIAMI","CARTERET"))
  cOffState = Iif(cOffice="C","CA",Iif(cOffice="M","FL","NJ"))

  xReturn = "XXX"
  Do m:\dev\prg\wf_alt With cOffice,nAcctNum
  If Empty(Alltrim(xReturn))
    Do errormail With "NO WHSE"
    Throw
  Endif
  cUseFolder = Upper(xReturn)

  cWO_Num = Alltrim(Str(nWO_Num))
  cProcType = Iif(cRunType = "E","ENROUTE","DELIVERED")

  Dimension thisarray(1)
  tFrom = "TGF Warehouse Operations <warehouse-ops@fmiint.com>"
  tattach = ""

  Select 0
  Use F:\3pl\Data\mailmaster Alias mm
  Locate For mm.edi_type = 'MISC' And mm.taskname = 'GENERAL'
  tsendto = Iif(mm.use_alt,mm.sendtoalt,mm.sendto)
  tcc = Iif(mm.use_alt,mm.ccalt,mm.cc)
  tsendtoerr = Iif(mm.use_alt,mm.sendtoalt,mm.sendto)
  tccerr = Iif(mm.use_alt,mm.ccalt,mm.cc)
  Use In mm

  cCustName = "VF"
  cCustFolder = "VF"
  cCustPrefix = "VF214_"
  dt1 = Ttoc(Datetime(),1)
  dtmail = Ttoc(Datetime())
  dt2 = Datetime()

  cFilename = ("f:\ftpusers\"+cCustFolder+"\214-Staging\"+cCustPrefix+dt1+".txt")
 
  cFilenameShort = Justfname(cFilename)
  cFilename2 = ("f:\ftpusers\"+cCustFolder+"\214OUT\archive\"+cFilenameShort)
  cFilename3 = ("f:\ftpusers\"+cCustFolder+"\214OUT\"+cFilenameShort)
  nFilenum = Fcreate(cFilename)

  If !Used("WOLOG")
    Use F:\wo\wodata\wolog In 0 Alias wolog
  Endif

  If !Used('xref')
    Select 0
    Use "f:\vf\data\vf_xref" Alias xref Order accountid Noupdate
  Endif
  Assert .F. Message "At table checks"
  Select xref

  If Seek(nAcctNum,"xref","accountid")
    cGroupName = Alltrim(xref.div214)
    cSystemName = Alltrim(xref.acct_name)
  Else
    Do errormail With "MISSACCT"
    Throw
  Endif
  Use In xref
  If Empty(cGroupName)
    Do errormail With "MISSGROUP"
    Throw
  Endif

  cMailName = Proper(Strtran(cGroupName,"VF","VANITY FAIR"))
  cMailName = Iif(Empty(cMailName),"VF",cMailName)

*!* Open Tables

*!*     If !Used('fxtrips')
*!*          Select 0
*!*          Use F:\Express\fxdata\fxtrips Order Tag fxtripsid Noupdate
*!*      Endif
*!*      Select fxtrips
*!*      If !Seek(nTripId,"fxtrips","fxtripsid")
*!*          Do errormail With "BAD TRIPID"
*!*          Throw
*!*      Else
*!*          If Empty(fxtrips.pickedup)
*!*              Do errormail With "MISSPICKUP"
*!*              Throw
*!*          Endif
*!*      Endif

  Select wolog
  If Inlist(cRunType,"VFDPU","VFDDL")
    If !Seek(nWO_Num,"wolog","wo_num")
      Do errormail With "NO WORK ORDER IN WOLOG"
      Throw
    Else
      If Empty(wolog.Container)
        Do errormail With "NO CONTAINER NUMBER"
        Throw
      Endif
      lcContainerNumber = wolog.Container
      lcConsignee = wolog.acctname
      If!Empty(wolog.brokref)
        lcRefvalue = wolog.brokref
      Else
        lcRefvalue = wolog.cust_ref
      Endif
      Do Case
      Case c214Type = "VFDPU"
        cDate      = Dtos(wolog.pickedup)
        cPUDate    = Right(cDate,8)
        cTruncTime = "1530"  &&SUBSTR(TTOC(DATETIME(),1),9,4)
      Case c214Type = "VFDDL"
        cDate      = Dtos(wolog.delivered)
        cDelDate   = Right(cDate,8)
        cTruncTime = "1530"  &&SUBSTR(TTOC(DATETIME(),1),9,4)
      Endcase
    Endif
  Endif

*  cGroupName = ""

	xsqlexec("select * from dellocs",,,"wo")
	index on str(accountid,4)+location tag acct_loc
	set order to

  If !Used('unloccodes')
    Use F:\3pl\Data\vf_unloccodes In 0 Alias unloccodes
  Endif

*!*      Select 0
*!*      If !Used('manifest')
*!*          Use F:\wo\wodata\manifest Alias manifest Order Tag wo_num Shared Noupdate
*!*      Endif
*!*      If !Seek(nWO_Num)
*!*          Do errormail With "MISSWO"
*!*          Throw
*!*      Else
*!*          Select dellocs
*!*          Locate
*!*          Locate For dellocs.accountid = manifest.accountid And dellocs.div = manifest.div
*!*          If Found()
*!*              cDellocCity = Upper(Alltrim(dellocs.city))
*!*              cDellocState = Upper(Alltrim(dellocs.state))
*!*              cDellocZip = Alltrim(dellocs.zip)
*!*              If cDellocCity = "JFK"
*!*                  cDelCS = "JFK"
*!*              Else
*!*                  cDelCS = cDellocCity+", "+cDellocState
*!*              Endif
*!*              cDellocUNCode = "US*UN*XYZ"
*!*              If !Seek(cDelCS,'unloccodes','city')
*!*                  Do errormail With "MISSLOC"
*!*                  closefiles()
*!*                  Return
*!*              Else
*!*                  cDellocUNCode = Alltrim(unloccodes.Code)
*!*              Endif
*!*              cST_CSZ = cDellocCity+"*"+cDellocState+"*"+cDellocZip+"*"+cDellocUNCode
*!*          Endif
*!*      Endif

  cST_Name = "TGF INC."
  Do Case
  Case cOffice = "C"
    cSF_Port  =  "TOLL GLOBAL FORAWARDING-CA"
    cSF_Addr  =  "450 WESTMONT DRIVE"
    cFMICity  = "SAN PEDRO"
    cFMIState = "CA"
    cFMIZip   = "90731"
    cSF_CSZ   = "SAN PEDRO*CA*90731*US*UN*SPQ"
    cUNLocode = "SPQ"
  Case cOffice = "N"
    cSF_Port  =  "TOLL GLOBAL FORAWARDING-NJ"
    cSF_Addr  = "800 FEDERAL BLVD"
    cFMICity  = "CARTERET"
    cFMIState = "NJ"
    cFMIZip   = "07008"
    cSF_CSZ   = "CARTERET*NJ*07095*US*UN*CSF"
    cUNLocode = "CSF"
  Case cOffice = "M"
    cSF_Port  =  "TOLL GLOBAL FORAWARDING-FL"
    cSF_Addr  =  "2100 NW 129TH AVE SUITE 100"
    cFMICity  = "MIAMI"
    cFMIState = "FL"
    cFMIZip   = "33182"
    cSF_CSZ   = "MIAMI*FL*33178*US*UN*MDY"
    cUNLocode = "MDY"
  Endcase

  cfd = "*"  && Field delimiter
  csegd = "~"  && Segment delimiter
  cterminator = ">"  && Used at end of ISA segment

  csendqual = "ZZ"   && Sender qualifier
  csendid = "FMIFSP" && Sender ID code
  crecqual = "ZZ"    && Recip qualifier
  crecid = "GTNEXUS" && Recip ID Code

  cDate = Dtos(Date())
  cTruncDate = Right(cDate,6)
  cTruncTime = Substr(Ttoc(Datetime(),1),9,4)
  cfiledate = cDate+cTruncTime

  csendidlong = Padr(csendid,15," ")
  crecidlong = Padr(crecid,15," ")
  cString = ""

  nSTCount = 0
  nSegCtr = 0
  nLXNum = 1
  Store "" To cEquipNum,cHAWB

  If lTesting
    cISACode = "T"
  Else
    cISACode = "P"
  Endif

  Do num_incr_isa
  cISA_Num = Padl(c_CntrlNum,9,"0")
  Store "ISA"+cfd+"00"+cfd+"          "+cfd+"00"+cfd+"          "+cfd+csendqual+cfd+csendidlong+cfd+;
    crecqual+cfd+crecidlong+cfd+cTruncDate+cfd+cTruncTime+cfd+"U"+cfd+"00401"+cfd+;
    cISA_Num+cfd+"0"+cfd+cISACode+cfd+cterminator+csegd To cString
  Do cstringbreak

  Store "GS"+cfd+"QM"+cfd+csendid+cfd+crecid+cfd+cDate+cfd+cTruncTime+cfd+c_CntrlNum+;
    cfd+"X"+cfd+"004010"+csegd To cString
  Do cstringbreak

  Do num_incr_st
  Store "ST"+cfd+"214"+cfd+Padl(c_GrpCntrlNum,9,"0")+csegd To cString
  Do cstringbreak
  nSTCount = nSTCount + 1
  nSegCtr = nSegCtr + 1

*  Select manifest
*  Set Order To
*  cTrailer = Allt(manifest.trailer)
*  cBL = cWO_Num

  Store "B10"+cfd+Alltrim(lcRefvalue)+cfd+Alltrim(Transform(nWO_Num))+cfd+"FMIF"+csegd To cString
  Do cstringbreak
  nSegCtr = nSegCtr + 1

*!*    STORE "MS3"+cfd+csendid+cfd+"1"+cfd+cFMICity+cfd+"J"+cfd+cFMIState+csegd TO cString
*!*    DO cstringbreak
*!*    nSegCtr = nSegCtr + 1


  Wait Clear
  Wait Window "Now creating Trucking WO#-based 214 information..." Nowait

  Do Case
  Case c214Type = "VFDPU"
    Store "L11"+cfd+lcContainerNumber+cfd+"EQ"+csegd To cString
    Do cstringbreak
    nSegCtr = nSegCtr + 1

    Store "N1"+cfd+"SH"+cfd+cSF_Port+cfd+"94"+cfd+cUNLocode+csegd To cString
    Do cstringbreak
    nSegCtr = nSegCtr + 1

    Store "N4"+cfd+cSF_CSZ+csegd To cString
    Do cstringbreak
    nSegCtr = nSegCtr + 1

    Store "G62"+cfd+"17"+cfd+"20"+cTruncDate+csegd To cString
    Do cstringbreak
    nSegCtr = nSegCtr + 1

    Store "N1"+cfd+"CN"+cfd+Alltrim(lcConsignee)+cfd+"94"+cfd+"ID Code"+csegd To cString
    Do cstringbreak
    nSegCtr = nSegCtr + 1

    Store "N4"+cfd+cSF_CSZ+csegd To cString
    Do cstringbreak
    nSegCtr = nSegCtr + 1

    Store "G62"+cfd+"69"+cfd+"20"+cTruncDate+csegd To cString
    Do cstringbreak
    nSegCtr = nSegCtr + 1

    Store "MS3"+cfd+"FMIF*O**SC"+csegd To cString
    Do cstringbreak
    nSegCtr = nSegCtr + 1

    Store "LX*1"+csegd To cString
    Do cstringbreak
    nSegCtr = nSegCtr + 1

    Store "AT7*AF*NS**"+cfd+cPUDate+cfd+"1530*LT"+csegd To cString
    Do cstringbreak
    nSegCtr = nSegCtr + 1

    Store "MS1"+cfd+Alltrim(cFMICity)+cfd+cfd+"US"+csegd To cString
    Do cstringbreak
    nSegCtr = nSegCtr + 1

  Case c214Type = "VFDDL"
    Store "L11"+cfd+lcContainerNumber+cfd+"EQ"+csegd To cString
    Do cstringbreak
    nSegCtr = nSegCtr + 1

    Store "N1"+cfd+"SH"+cfd+cSF_Port+cfd+"94"+cfd+cUNLocode+csegd To cString
    Do cstringbreak
    nSegCtr = nSegCtr + 1

    Store "N4"+cfd+cSF_CSZ+csegd To cString
    Do cstringbreak
    nSegCtr = nSegCtr + 1

    Store "G62"+cfd+"17"+cfd+"20"+cTruncDate+csegd To cString
    Do cstringbreak
    nSegCtr = nSegCtr + 1


    Store "N1"+cfd+"CN"+cfd+Alltrim(lcConsignee)+cfd+"94"+cfd+"ID Code"+csegd To cString
    Do cstringbreak
    nSegCtr = nSegCtr + 1

    Store "N4"+cfd+cSF_CSZ+csegd To cString
    Do cstringbreak
    nSegCtr = nSegCtr + 1

    Store "G62"+cfd+"69"+cfd+"20"+cTruncDate+csegd To cString
    Do cstringbreak
    nSegCtr = nSegCtr + 1

    Store "MS3"+cfd+"FMIF*O**SC"+csegd To cString
    Do cstringbreak
    nSegCtr = nSegCtr + 1

    Store "LX*1"+csegd To cString
    Do cstringbreak
    nSegCtr = nSegCtr + 1

    Store "AT7*D1*NS***"+cDelDate+"*1530*LT"+csegd To cString
    Do cstringbreak
    nSegCtr = nSegCtr + 1

    Store "MS1"+cfd+Alltrim(cFMICity)+cfd+cfd+"US"+csegd To cString
    Do cstringbreak
    nSegCtr = nSegCtr + 1

  Case c214Type = "XXXX"

    Store "N1"+cfd+"SH"+cfd+"TOLL GLOBAL FORWARDING"+cfd+"94"+cfd+"TGF"+csegd To cString
    Do cstringbreak
    nSegCtr = nSegCtr + 1

    Store "N3*"+cSF_Addr+csegd To cString
    Do cstringbreak
    nSegCtr = nSegCtr + 1

    Store "N4"+cSF_CSZ+csegd To cString
    Do cstringbreak
    nSegCtr = nSegCtr + 1

    Store "G62"+cfd+"69"+cfd+"date"+csegd To cString
    Do cstringbreak
    nSegCtr = nSegCtr + 1

    Store "L11"+cfd+"003"+cfd+"8X"+csegd To cString
    Do cstringbreak
    nSegCtr = nSegCtr + 1

    Store "L11"+cfd+"VF CORP"+cfd+"IC"+csegd To cString  && added 9/27/06
    Do cstringbreak
    nSegCtr = nSegCtr + 1

    cEquipNum = Alltrim(manifest.Container)
    cEquipQual = "EQ"
    cEquipDesc = "EQUIPMENT NUMBER"
    If Empty(cEquipNum)
      cEquipNum = Alltrim(manifest.hawb)
      cEquipQual = "AW"
      cEquipDesc = "AIRBILL NUMBER"
      If Empty(cEquipNum)
        Do errormail With "MISSEQUIP"
        Throw
      Endif
    Endif

    Store "L11"+cfd+cEquipNum+cfd+cEquipQual+cfd+cEquipDesc+csegd To cString
    Do cstringbreak
    nSegCtr = nSegCtr + 1

*!* FXTRIPS Data Acquisition
    If cRunType = "E"
      cShipStatus = Iif(doloop = 1,"AF","B6")
      dStatusDate = Iif(doloop = 1,fxtrips.pickedup,fxtrips.delappt)
      cStatusTime = Iif(doloop = 1,fxtrips.picktime,fxtrips.delatime)
    Else
      cShipStatus = "X1"
      dStatusDate = fxtrips.delivered
      cStatusTime = fxtrips.delvtime
    Endif
    cStatusDate = Dtoc(dStatusDate,1)
    If Empty(cStatusDate)
      cStatusDate = Dtoc(Date(),1)
    Endif
    cStatusTime = "1530"

    Store "AT7"+cfd+cShipStatus+Replicate(cfd,4)+cStatusDate+cfd+cStatusTime+cfd+"LT"+csegd To cString
    Do cstringbreak
    nSegCtr = nSegCtr + 1

    cSeal = Alltrim(manifest.seal)
    If Empty(cSeal)
      cSeal = "XXX"
    Endif
    Store "M7"+cfd+cSeal+Replicate(cfd,4)+"SH"+csegd To cString
    Do cstringbreak
    nSegCtr = nSegCtr + 1

    N101 = "XG"
    cEventLoc = Iif(N101="XG","EVENT LOCATION",cSFName)
    Store "N1"+cfd+N101+cfd+cEventLoc+csegd To cString
    Do cstringbreak
    nSegCtr = nSegCtr + 1

    Store "N4"+cfd+cST_CSZ+csegd To cString
    Do cstringbreak
    nSegCtr = nSegCtr + 1

* added arrival# here 5/17 in OID11
    cPO_Num = Alltrim(manifest.po_num)
    cDesc = Alltrim(manifest.Style)
    cQty = Alltrim(Str(manifest.quantity))
    cWtUnit = "L"
    cWeight = Alltrim(Str(manifest.weight))
    cVolume = Alltrim(Str(manifest.cbm,6,2))
    cArrival = Alltrim(manifest.arrival)
    Store "OID"+cfd+cfd+cPO_Num+cfd+cDesc+cfd+"CTN"+cfd+cQty+cfd+;
      cWtUnit+cfd+cWeight+cfd+"X"+cfd+cVolume+cfd+cfd+cArrival+csegd To cString
    Do cstringbreak
    nSegCtr = nSegCtr + 1

    Store "SDQ"+cfd+"SX"+cfd+"56"+cfd+cGroupName+cfd+"1"+csegd To cString
    Do cstringbreak
    nSegCtr = nSegCtr + 1

  Endcase



*!* Finish processing
  Do close214
  If nFilenum>0
    =Fclose(nFilenum)
  Endif
  Do ediupdate With "214 CREATED",.F.,.F.

 If lFilesOut
    cTruncTime=substr(Ttoc(Datetime(),1),9,6)
    cMilliseconds = Padl(Transform(Round(Val(right(Alltrim(transform(seconds())),3))/10,0)),2,"0")
    cGTFilename= "FMIFSP_"+"GTNEXUS_"+"QM_"+cISA_Num+"_"+cTruncDate+"T"+cTruncTime+cMilliseconds+".X12"

    cFilename2 = ("f:\ftpusers\"+cCustFolder+"\214OUT\archive\"+cGTFilename)
    cFilename3 = ("f:\ftpusers\"+cCustFolder+"\214OUT\"+cGTFilename)

    Copy File &cFilename To &cFilename2
 *   Copy File &cFilename To &cFilename3
    Delete File &cFilename
  Endif

  If !lTesting
    If !Used("ftpedilog")
      Select 0
      Use F:\edirouting\ftpedilog Alias ftpedilog
      Insert Into ftpedilog (TRANSFER,ftpdate,filename,acct_name,Type) Values ("214-"+cCustName,dt2,cFilename,Upper(cCustName),"214")
      Use In ftpedilog
    Endif
  Endif

  If lTesting
    tsubject = "VF 214 *TEST* EDI from TGF as of "+dtmail
  Else
    tsubject = "VF 214 EDI from TGF as of "+dtmail
  Endif

  tmessage = "214 ("+cProcType+") EDI Info from TGF-CA, Truck WO# "+cWO_Num+Chr(10)
  tmessage = tmessage + "for coalition "+cMailName
  tmessage = tmessage +" has been created."+Chr(10)+"(Filename: "+cFilenameShort+")"
*!*  +CHR(10)+CHR(10)
*!*  tmessage = tmessage + "If you have any questions, please eMail or call 732-750-9000 x217."
  If lTesting
    tmessage = tmessage + Chr(10)+Chr(10)+"This is a TEST RUN..."
  Endif

  If lEmail
    Do Form dartmail2 With tsendto,tFrom,tsubject,tcc,tattach,tmessage,"A"
  Endif

  Release All Like c_CntrlNum,c_GrpCntrlNum
  Release All Like nOrigSeq,nOrigGrpSeq
  Wait Clear
  Wait Window cMailName+" 214 "+cProcType+" EDI File output complete" At 20,60 Timeout 2

  closefiles()

Catch To oErr
  If lDoCatch
    lEmail = .T.
    Do ediupdate With "ERRHAND ERROR",.T.
    tsubject = cCustName+" Error ("+Transform(oErr.ErrorNo)+") at "+Ttoc(Datetime())
    tattach  = ""
    tsendto  = tsendtoerr
    tcc = tccerr
    tmessage = cCustName+" Error processing "+Chr(13)

    tmessage =tmessage+ "Try/CATCH Exeception Message:"+Chr(13)+;
      [  Error: ] + Str(oErr.ErrorNo) +Chr(13)+;
      [  LineNo: ] + Str(oErr.Lineno) +Chr(13)+;
      [  Message: ] + oErr.Message +Chr(13)+;
      [  Procedure: ] + oErr.Procedure +Chr(13)+;
      [  Details: ] + oErr.Details +Chr(13)+;
      [  StackLevel: ] + Str(oErr.StackLevel) +Chr(13)+;
      [  LineContents: ] + oErr.LineContents+Chr(13)+;
      [  UserValue: ] + oErr.UserValue

    tsubject = "945 EDI Poller Error at "+Ttoc(Datetime())
    tattach  = ""
    Do Form dartmail2 With tsendto,tFrom,tsubject,tcc,tattach,tmessage,"A"
    lEmail = .F.
  Endif
Finally
  Fclose(nFilenum)
  closefiles()
Endtry

&& END OF MAIN CODE SECTION



****************************
Procedure num_incr_isa
****************************

If !Used("serfile")
  Use ("F:\3pl\data\serial\"+cCustName+"214_serial") In 0 Alias serfile
Endif
Select serfile
If lTesting
  nOrigSeq = serfile.seqnum
  nOrigGrpSeq = serfile.grpseqnum
  lISAFlag = .F.
Endif
nISA_Num = serfile.seqnum
c_CntrlNum = Alltrim(Str(serfile.seqnum))
Replace serfile.seqnum With serfile.seqnum + 1 In serfile
Endproc

****************************
Procedure num_incr_st
****************************
If !Used("serfile")
  Use ("F:\3pl\data\serial\"+cCustName+"214_serial") In 0 Alias serfile
Endif
Select serfile
If lTesting
  nOrigSeq = serfile.seqnum
  nOrigGrpSeq = serfile.grpseqnum
  lSTFlag = .F.
Endif
c_GrpCntrlNum = Alltrim(Str(serfile.grpseqnum))
Replace serfile.grpseqnum With serfile.grpseqnum + 1 In serfile
Endproc

****************************
Procedure segmentget
****************************

Parameter thisarray,lcKey,nLength

For i = 1 To nLength
  If i > nLength
    Exit
  Endif
  lnEnd= At("*",thisarray[i])
  If lnEnd > 0
    lcThisKey =Trim(Substr(thisarray[i],1,lnEnd-1))
    If Occurs(lcKey,lcThisKey)>0
      Return Substr(thisarray[i],lnEnd+1)
      i = 1
    Endif
  Endif
Endfor

Return ""

****************************
Procedure close214
****************************
Store  "SE"+cfd+Alltrim(Str(nSegCtr+1))+cfd+Padl(c_GrpCntrlNum,9,"0")+csegd To cString
Do cstringbreak

Store  "GE"+cfd+Alltrim(Str(nSTCount))+cfd+c_CntrlNum+csegd To cString
Do cstringbreak

Store  "IEA"+cfd+"1"+cfd+Padl(c_CntrlNum,9,"0")+csegd To cString
Do cstringbreak

Return

****************************
Procedure cstringbreak
****************************
cLen = Len(Alltrim(cString))

Fwrite(nFilenum,cString)
Return


****************************
Procedure errormail
****************************
Parameters msgtype
*ASSERT .f.
lDoCatch = .F.

*ASSERT .F. MESSAGE "At Errormail...Debug"
tsubject = cSystemName+" 214 EDI File Error"
tmessage = cDivision+" 214 EDI Info, WO# "+cWO_Num+Chr(10)
tsendto = tsendtoerr
tcc = tccerr
Do Case
Case msgtype = "MISSGROUP"
  tmessage = tmessage + "There was no Group Name in WO# "+cWO_Num
  Do ediupdate With msgtype,.T.,.F.
Case msgtype = "MISSACCT"
  tmessage = tmessage + "Account ID "+Alltrim(Str(nAcctNum))+" does not exist in ACCOUNT."+Chr(10)
  tmessage = tmessage + "Check account information submitted from PICKUP trigger."
  Do ediupdate With msgtype,.T.,.F.
Case msgtype = "MISSWO"
  tmessage = tmessage + "WO# "+cWO_Num+" does not exist in F:\WO\WODATA\MANIFEST."
  Do ediupdate With msgtype,.T.,.F.
Case msgtype = "MISSEQUIP"
  tmessage = tmessage + "Equipment not specified for WO# "+cWO_Num+Chr(10)
  tmessage = tmessage + "Data in MANIFEST must be corrected."
  Do ediupdate With msgtype,.T.,.F.
Case msgtype = "BAD TRIPID"
  tmessage = tmessage + "Trip ID does not exist in F:\WO\WODATA\MANIFEST."
  Do ediupdate With msgtype,.T.,.F.
Case msgtype = "MISSPICKUP"
  tmessage = tmessage + "WO# "+cWO_Num+" contains no pickup date."+Chr(10)
  tmessage = tmessage + "Data in MANIFEST must be corrected."
  Do ediupdate With msgtype,.T.,.F.
  If lTesting
    lEmail = .F.
  Else
    tsendto = tsendtoerr
    tcc = tccerr
  Endif
Endcase

If lTesting
  tmessage = tmessage + Chr(10)+Chr(10)+"This is TEST DATA only..."
Endif
If lEmail
  Do Form dartmail2 With tsendto,tFrom,tsubject,tcc,tattach,tmessage,"A"
Endif
Endproc

**************************
Procedure ediupdate
**************************
Parameters cFin_status, lIsError,lFClose
If !lTesting
  Select edi_trigger
  Locate
  If lIsError
    Replace edi_trigger.processed With .T.,edi_trigger.created With .F.,;
      edi_trigger.fin_status With cFin_status,edi_trigger.errorflag With .F.;
      edi_trigger.when_proc With Datetime() ;
      FOR edi_trigger.wo_num = nWO_Num And Inlist(edi_type,cRunType+"214","214"+cRunType)
    If nFilenum>0
      =Fclose(nFilenum)
    Endif
    If File(cFilename)
      Delete File &cFilename
    Endif
    closefiles()
  Else
    Replace edi_trigger.processed With .T.,edi_trigger.created With .T.,proc214 With .T.;
      edi_trigger.when_proc With Datetime(),;
      edi_trigger.fin_status With cRunType+"214 CREATED";
      edi_trigger.errorflag With .F.,file214 With cFilename ;
      FOR edi_trigger.wo_num = nWO_Num And Inlist(edi_type,cRunType+"214","214"+cRunType)
  Endif
Endif
Endproc

*********************
Procedure closefiles
*********************
If Used('ftpedilog')
  Use In ftpedilog
Endif
If Used('manifest')
  Use In manifest
Endif
If Used('serfile')
  Use In serfile
Endif
If Used('fxtrips')
  Use In fxtrips
Endif
If Used('account')
  Use In account
Endif
If Used('xref')
  Use In xref
Endif
If Used('dellocs')
  Use In dellocs
Endif
If Used('unloccodes')
  Use In unloccodes
Endif
Endproc
