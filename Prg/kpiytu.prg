**Yard To Unload KPI

**Time from container pickup to container stripping
**  - Full ocean containers Only
**  - Times based on either wolog.pickedUp (if trucking wo) or inWolog.availDt ;
**	  Vs. detail.stripped (if bkdn exists) or inWolog.confirmDt
**
**Comments/Issues:
**  - For the time being these nos. will only be run on ctrs picked up by FMI (trucking wos)
**		- The idea was to have our users enter availDt if the ctr was dropped to us (no trucking wo), ;
**		  but darren claims this will not happen in the near future
**		- These nos. are to be calculated on only full ocean ctrs, without a trucking wo there is no ;
**		  way at this point to tell that a wo is full ocean ctr - no such data in inWolog
**  - if no empty(pickedUp), wo_date used in its stead
Wait window "Gathering data for Yard to Unload Time Spreadsheet..." nowait noclear

use f:\wo\wodata\wolog in 0
use f:\wo\wodata\detail in 0

**as per Greg D. we hardcode 0 days for all wos. not picked up by FMI (No trucking wo)
**  - filter "!IsNull(w.wo_num)" was removed to account for this
**
**
**filter below must take into acct all possibilities, it could be a problem for whse wos no related 
**  trk wo that has no confirmDt and multiple date_rcvd
**ignore any wos with container="PPADJ" as per jimk - mvw 01/13/09

cAcctFilter = Iif(Empty(cWhseAcct), "", iif(lower(cwhseacct)="inlist",strtran(cwhseacct,"inlist(","inlist(iw."),"iw."+cWhseAcct))

xquery="Select iw.*, d.date_rcvd from inwolog iw left join indet d on d.inwologid=iw.inwologid "+;
	"where "+cacctfilter+" "+;
		"((iw.confirmDt>={"+transform(dstartdt)+"} and iw.confirmDt<= {"+transform(denddt+1)+"}) or (d.date_rcvd >= {"+transform(dstartdt)+"} and d.date_rcvd <= {"+transform(denddt)+"})) and iw.mod = '"+goffice+"'"


**cannot add cursor name (param 3), causes issue - result cursor by default is "sqlresult" when pulling from multiple tables
xsqlexec(xquery,,,"wh")
select * from sqlresult group by wo_num into cursor xinwo readwrite

select * from wolog iw where &cAcctFilter (Between(wo_date, dStartDt, dEndDt) or Between(pickedUp, dStartDt, dEndDt)) into cursor xwolog

Select iw.wo_num as inwo, iw.wo_date, iw.accountId as acctNumFld, iw.acctName as acctNameFld, ;
	   iw.container, iw.reference, w.wo_num, Iif(Empty(w.pickedUp), w.wo_date, w.pickedUp) as pickedUp, ;
	   Iif(!Empty(iw.confirmDt) or IsNull(date_rcvd), Ttod(iw.confirmDt), Min(date_rcvd)) as stripped, ;
	   00 as DateDiff ;
	from xinwo iw ;
		left join xwolog w on iw.wo_num = w.inwo ;
	group by iw.wo_num, w.wo_num order by iw.acctName, iw.wo_date, iw.wo_num, w.wo_num ;
	where Iif(IsNull(w.wo_date) and (!Empty(iw.confirmDt) or IsNull(date_rcvd)), Between(iw.confirmDt, dStartDt, dEndDt+1), .t.) and ;
		Iif(IsNull(w.wo_date) and Empty(iw.confirmDt) and !IsNull(date_rcvd), Between(date_rcvd, dStartDt, dEndDt), .t.) and ;
		Iif(!IsNull(w.wo_date) and Empty(w.pickedUp), Between(w.wo_date, dStartDt, dEndDt), .t.) and ;
		Iif(!IsNull(w.wo_date) and !Empty(w.pickedUp), Between(w.pickedUp, dStartDt, dEndDt), .t.) and ;
		!iw.sp and !iw.returntostock and !iw.zreturns and !iw.cyclecount and iw.container#'PPADJ' ;
  into cursor csrDetail readwrite

**need to account for not finding recs in main.wolog (isNull(wo_num), etc.)

**changed to use the date the wo was added to the receiving screen as opposed to confirmation date for a more accurate calculation - mvw 07/06/16
*!*	Select inwo, wo_date, acctNumFld, acctNameFld, container, reference, Iif(IsNull(wo_num), 0000000, wo_num) as wo_num, ;
*!*		   Iif(IsNull(pickedUp), stripped, pickedUp) as pickedUp, stripped, dateDiff ;
*!*		from csrDetail where !Emptynul(stripped) ;
*!*	  into cursor csrDetail readwrite

delete for Emptynul(stripped)
scan for isnull(wo_num)
	xsqlexec("select * from rcv where wo_num = "+transform(csrdetail.inwo),,,"wh")
	replace wo_num with 0000000, pickedup with iif(eof("rcv"),stripped,rcv.adddt) in csrdetail
	use in rcv
endscan
locate

**calculate date differential into dateDiff - make necessary adjustments for weekends & FMI holidays
DO kpicalcdiff with "pickedUp", "stripped", "dateDiff"

Locate
If Eof()
	strMsg = "No data found."
	=MessageBox(strMsg, 48, "Yard to Unload Time")
Else
	Wait window "Creating Yard to Unload Time Spreadsheet..." nowait noclear

	oWorkbook=""
	DO xlsTimeGeneric with "", "pickedUp", "1", "", "Yard to Unload Times", "Strip Date Vs. Picked Up Date", "", "", "''", .t., .t.

	If lDetail
		Select csrTotals
		nWorksheet = 2
		Scan
			STORE tempfox+SUBSTR(SYS(2015), 3, 10)+".xls" TO cTmpFile
			Select * from csrDetail where Year(pickedUp) = csrTotals.year and Month(pickedUp) = csrTotals.month into cursor csrDet
			Copy to &cTmpFile fields acctNameFld, inwo, wo_date, container, reference, wo_num, pickedUp, stripped, dateDiff ;
				for Year(pickedUp) = csrTotals.year and Month(pickedUp) = csrTotals.month type xl5

			DO xlsTimeGenDetail with cTmpFile, nWorkSheet, "I"

			nWorksheet = nWorksheet+1
		EndScan
		USE in csrDet
	EndIf

	oWorkbook.Worksheets[1].range("A1").activate()
	oWorkbook.Save()
EndIf

use in wolog
use in detail
use in csrdetail
if used("csrtotals")
  use in csrtotals
endif
