PARAMETERS cFilename

SET CENTURY ON
SELECT x856
GOTO TOP

STORE 0 TO lnTotalAdded
lcCurrentHAWB = ""

POs_added = 0
origawb = ""
lcCurrentShipID = ""
lcCurrentArrival = ""
lcHLLevel = ""
m.office = cOffice
m.accountid = nAcctNum
m.acctname = 'BIOWORLD MERCHANDISING'
m.wo_date = DATE()
m.date_rcvd = DATE()
m.wo_num = 0
m.inwologid = 0
m.addby = 'TGF-PROC'
m.adddt = DATETIME()
m.addproc = 'BIO856IN'
cContainer = "XYZ"
m.plid = 0
npo = 0
m.comments = ""
lLicensor = .F.

SELECT x856
COUNT FOR x856.segment = "LIN" TO nCntz1
COUNT FOR x856.segment = "SN1" TO nCntz2
COUNT FOR x856.segment = "PO4" TO nCntz3
COUNT FOR x856.segment = "PID" TO nCntz4
IF (nCntz1#nCntz2 OR nCntz1#nCntz3 OR nCntz1#nCntz4 OR nCntz2#nCntz3 OR nCntz2#nCntz4 OR nCntz3#nCntz4)
	nShort= MIN(nCntz1,nCntz2,nCntz3,nCntz4)
	cShortSeg = ICASE(nShort = nCntz1,"LIN",nShort = nCntz2,"SN1",nShort = nCntz3,"PO4","PID")
	cErrMsg = "There are one or more missing detail "+cShortSeg+" segments in File "+cFilename
	WAIT WINDOW  cErrMsg TIMEOUT 2
	SET STEP ON
	RELEASE ALL LIKE nCntz*
	RELEASE nShort,cShortSeg
	THROW
ENDIF

SET FILTER TO
LOCATE
IF lTesting
*	BROWSE
ENDIF

set step on


WAIT WINDOW "Now importing file..........   "+cFilename NOWAIT
DO WHILE !EOF("x856")
	cSegment = ALLTRIM(x856.segment)
	cField1 = ALLTRIM(x856.f1)

	IF INLIST(cSegment,"ISA")
	  If ALLTRIM(x856.f8) ="TGF-US-ML"
      xgMod = "L"
    Else
      xgMod = " "
    Endif 

  	SELECT x856
		SKIP 1 IN x856
		LOOP
	ENDIF

  IF INLIST(cSegment,"GS","ST","SE","GE","IEA")
    SELECT x856
    SKIP 1 IN x856
    LOOP
  ENDIF

	IF TRIM(x856.segment) = "BSN"
		m.dateloaded= DATE()
		m.shipID    = ALLTRIM(x856.f2)
		SELECT x856
		SKIP 1 IN x856
		LOOP
	ENDIF

	IF TRIM(x856.segment) = "N1" AND TRIM(x856.f1) = "SU"
		cVendorName    = ALLTRIM(x856.f2)
		SELECT x856
		SKIP 1 IN x856
		LOOP
	ENDIF

	IF AT("HL",x856.segment)>0
		lcHLLevel = ALLTRIM(x856.f3)
		SELECT x856
		SKIP 1 IN x856
		LOOP
	ENDIF

	IF lcHLLevel = "S"
		DO WHILE lcHLLevel = "S"
			IF AT("HL",x856.segment)>0
				lcHLLevel = ALLTRIM(x856.f3)
				SELECT x856
				SKIP 1 IN x856
				LOOP
			ENDIF
			cSegment = TRIM(x856.segment)
			cF1 = TRIM(x856.f1)
			DO CASE
				CASE TRIM(x856.segment) = "TD5"
					SELECT x856
					cTT1 = ALLTRIM(x856.f4)
					cTT2 = ICASE(cTT1="S","N OCEAN",cTT1="A","N AIR"," MOTOR")
					cTT3 = "THIS IS A"+cTT2+" SHIPMENT"
					m.comments = IIF(EMPTY(m.comments),cTT3,m.comments+CHR(13)+cTT3)
					RELEASE ALL LIKE cTT*
					SKIP 1 IN x856
					LOOP
				CASE TRIM(x856.segment) = "REF" AND ALLTRIM(x856.f1) = "ZZ"
					cTT1 = STRTRAN(ALLTRIM(x856.f2),"DOOR","")
					cTTa = LEFT(cTT1,AT("/",cTT1)-1)
					cTTb = SUBSTR(cTTa,AT("/",cTTa)+1)
					DO CASE
						CASE ("CFS"$cTTa AND cTTb = "CFS")
							cTTComm = "LCL SHIPMENT"
						CASE ("CFS"$cTTa AND cTTb = "CY")
							cTTComm = "CONSOLIDATION -> FULL CONTAINER"
						CASE ("CY"$cTTa AND cTTb = "CFS")
							cTTComm = "FULL CONTAINER -> DECONSOLIDATION"
						CASE ("CY"$cTTa AND cTTb = "CY")
							cTTComm = "FULL CONTAINER LOAD"
						OTHERWISE
							cTTComm = "UNKNOWN SHIP TYPE"
					ENDCASE
					m.comments = IIF(EMPTY(m.comments),cTTComm,m.comments+CHR(13)+cTTComm)
					RELEASE ALL LIKE cTT*
					SKIP 1 IN x856
					LOOP
				CASE TRIM(x856.segment) = "TD3"  && AND ALLTRIM(x856.f1) = "4B"  f1 is container type and we dont care
					m.container = ALLTRIM(x856.f2)+ALLTRIM(x856.f3)
					lDoHeader = IIF(m.container = cContainer,.F.,.T.)
					IF lDoHeader
						STORE m.container TO cContainer
					ENDIF
				CASE TRIM(x856.segment) = "REF" AND ALLTRIM(x856.f1) = "2I"
					cTracknum = ALLTRIM(x856.f2)
				CASE TRIM(x856.segment) = "REF" AND ALLTRIM(x856.f1) = "BM"
					m.reference = ALLTRIM(x856.f2)
				CASE TRIM(x856.segment) = "REF" AND ALLTRIM(x856.f1) = "Y8"
					m.accountid = IIF(x856.f2 = "N", 6554,m.accountid)
					m.acctname = IIF(x856.f2 = "N", "NOLAN ORIGINALS, LLC",m.acctname)
				CASE TRIM(x856.segment) = "V1"
					m.reference = ALLTRIM(x856.f2)+" "+ALLTRIM(x856.f4)
				OTHERWISE
					m.acct_name = "UNK"
					m.acct_num  = 9999
			ENDCASE
			SELECT x856
			SKIP 1 IN x856
		ENDDO
	ENDIF

	IF lcHLLevel = "O"
		SELECT x856
		DO WHILE INLIST(lcHLLevel,"O")
			IF AT("HL",x856.segment)>0
				lcHLLevel = ALLTRIM(x856.f3)
				SELECT x856
				SKIP 1 IN x856
				LOOP
			ENDIF
			cSegment = TRIM(x856.segment)
			cF1 = TRIM(x856.f1)

			IF TRIM(x856.segment) = "PRF"
				lDoMix = .F.
*				SET STEP ON
				cPONum = ALLTRIM(x856.f1)
				IF lDoHeader
					m.comments  = IIF(EMPTY(m.comments),"FILENAME: "+cFilename+CHR(13),m.comments+CHR(13)+"FILENAME: "+cFilename+CHR(13))
					m.comments = m.comments+CHR(13)+"PO "+ALLTRIM(x856.f1)
				ELSE
					IF 	!(("PO "+ALLTRIM(x856.f1))$xinwolog.comments)
						REPLACE xinwolog.comments WITH IIF(EMPTY(ALLT(xinwolog.comments)),"PO "+ALLTRIM(x856.f1),xinwolog.comments+CHR(13)+"PO "+ALLTRIM(x856.f1)) IN xinwolog
					ENDIF
				ENDIF
*m.cayset = ALLTRIM(x856.f1)
			ENDIF
			SELECT x856
			SKIP 1 IN x856
		ENDDO
		SELECT x856
	ENDIF
	IF lDoHeader
		m.inwologid = m.inwologid + 1
		m.wo_num = m.wo_num + 1
		SET STEP ON 
		INSERT INTO xinwolog FROM MEMVAR
		lDoHeader = .F.
		m.comments = ""
	ENDIF

	IF lcHLLevel = "P"
*!* Added this section to cover mixed carton breakouts
		IF TRIM(x856.segment) = "MAN" AND TRIM(x856.f1) = "MX"
			lDoMix = .T.
			cF3 = ALLT(x856.f2)
			IF LEFT(ALLTRIM(cF3),3) = "MIX"
				cErrMsg = "Incorrect style value in MAN*MX segment: "+ALLTRIM(cF3)+"...file must be corrected/re-sent"
				DELETE FILE [&xfile]
				THROW
			ENDIF
			m.color = ALLT(LEFT(cF3,AT(",",cF3)-1))
			m.style = ALLT(SUBSTR(cF3,AT(",",cF3)+1))
			nThisRec = RECNO('x856')
*			SET STEP ON
			nPack = 0
			DO WHILE !INLIST(TRIM(x856.segment),"PRF","SE")
				SKIP 1 IN x856
				DO CASE
					CASE TRIM(x856.segment) = "SN1"
						nCtnQty = INT(VAL(x856.f4))
					CASE TRIM(x856.segment) = "PO4"
						nPack = nPack+INT(VAL(x856.f1))
				ENDCASE
			ENDDO
			m.pack = ALLT(STR(nPack))
			GO nThisRec
			RELEASE nPack,nThisRec
			insertdetlines_domixhdr()
		ENDIF

		DO WHILE lcHLLevel = "P"
			IF AT("HL",x856.segment)>0
				lcHLLevel = ALLTRIM(x856.f3)
				SELECT x856
				SKIP 1 IN x856
				LOOP
			ENDIF
			cSegment = TRIM(x856.segment)
			cF1 = TRIM(x856.f1)
			SELECT x856
			SKIP 1 IN x856
		ENDDO
	ENDIF

	IF lcHLLevel = "I"
		DO WHILE lcHLLevel = "I" && AND trim(x856.segment) # "SE"
			m.color = ""
			IF AT("HL",x856.segment)>0
				lcHLLevel = ALLTRIM(x856.f3)
				SELECT x856
				SKIP 1 IN x856
				LOOP
			ENDIF
			cSegment = TRIM(x856.segment)
			cF1 = TRIM(x856.f1)
			DO CASE
				CASE TRIM(x856.segment) = "LIN"
					m.style = ALLTRIM(x856.f3)

				CASE TRIM(x856.segment) = "SN1"

					IF !lDoMix
						nUnitQty = INT(VAL(x856.f2))
						nCtnQty = INT(VAL(x856.f4))
					ELSE
						nTotQtyToAdd = INT(VAL(x856.f2))
						nUnitQty = nTotQtyToAdd
					ENDIF

				CASE TRIM(x856.segment) = "PO4"
					IF lDoMix
						nTotQty = INT(VAL(x856.f1))
					ELSE
						cPack = ALLTRIM(x856.f1)
						cInners = ALLTRIM(x856.f2)
						nInners = INT(VAL(cInners))
						nInnerpack = INT(VAL(ALLTRIM(x856.f14)))
						cInnerpack = ALLT(STR(nInnerpack))
						IF nInners > 0
							m.comments = m.comments+CHR(13)+"CONTAINS "+cInners+" INNERS OF PACK "+cInnerpack
							IF !"INNERS OF"$xinwolog.comments
								REPLACE xinwolog.comments WITH xinwolog.comments+CHR(13)+m.comments IN xinwolog
							ENDIF
							nCtnQty = nCtnQty * nInners
						ENDIF
					ENDIF

					SKIP 1 IN x856
					IF INLIST(TRIM(x856.segment),"HL","SE")  && No PID segment'
						IF TRIM(x856.segment) = "HL"
							lcHLLevel = ALLTRIM(x856.f3)
						ENDIF
						m.comments = ""
						IF TRIM(x856.segment) = "SE"
							EXIT
						ENDIF
					ELSE
						SKIP -1 IN x856
					ENDIF

				CASE TRIM(x856.segment) = "PID"
					cCustmr = UPPER(ALLTRIM(x856.f5))
					cCustPO = ""
					IF "-"$cCustmr  && Added per Jonathan @ Bioworld
						cCustPO = ALLT(SUBSTR(cCustmr,AT('-',cCustmr,1)+1))
						cCustmr = ALLT(LEFT(cCustmr,AT('-',cCustmr,1)-1))
					ENDIF
					lInners = .F.
					IF ("INNERS"$cCustmr) OR ("INNERS"$cCustPO)
						lInners = .T.
						m.printcomments = "SHIPPABLE INNERS PLEASE UNPACK"
					ENDIF
					m.comments = "CUST: "+ALLTRIM(x856.f5)+CHR(13)
					IF !EMPTY(cVendorName)
						m.echo  = "VENDORNAME*"+cVendorName
						m.echo  = m.echo+CHR(13)+"RETAILER*"+cCustmr+CHR(13)+"PONUM*"
					ELSE
						m.echo  = "RETAILER*"+cCustmr+CHR(13)+"PONUM*"
					ENDIF

					m.color = cPONum
					m.id = IIF(cCustPO="INNERS","",cCustPO)

*!* "Licensor Sample"-field value adjustments
					lLicensor = IIF("LICENSOR"$cCustmr,.T.,.F.)
					m.color = IIF(lLicensor,m.style,m.color)  && Style becomes Style-2 for Licensors
					m.style = IIF(lLicensor,"LICENSOR SAMPLE",m.style) && Style becomes "LICENSOR SAMPLE" for Licensors

*!* "Hot Topic/Old Navy"- field value adjustments
					lAddCustPO = IIF(!EMPTY(cCustPO) AND !lLicensor,.T.,.F.)
					IF (cCustmr = "HOT TOPIC" OR "GAP"$cCustmr)
						m.color = IIF(lAddCustPO,cPONum,m.color)  && Style becomes Style-2 for these
					ENDIF

					IF !"CUST: "+ALLTRIM(x856.f5)$xinwolog.comments
						REPLACE xinwolog.comments WITH xinwolog.comments+CHR(13)+m.comments IN xinwolog
					ENDIF
					m.comments = ""
					IF lDoMix
*!* Insertion of only units detail lines for mixed cartons
						insertdetlines_domixdet()
					ELSE
*!* Insertion of alternating detail lines
						insertdetlines()
					ENDIF
			ENDCASE

			SELECT x856
			IF !EOF()
				SKIP 1 IN x856
			ELSE
				EXIT
			ENDIF
		ENDDO
		SELECT x856
	ENDIF
ENDDO

IF lTesting OR lBrowFiles
	SELECT xinwolog
	LOCATE
	BROWSE && TIMEOUT 10
	SELECT xpl
	LOCATE
	BROWSE && TIMEOUT 10
ENDIF

lProcessOK = .T.
RETURN

****************************
PROCEDURE insertdetlines
****************************
	m.units = .F.
	m.totqty = nCtnQty
*	m.totqty = IIF(lInners,nCtnQty*nInners,nCtnQty)
	m.pack = IIF(lInners,cInnerpack,cPack)  && Changed from "1" for Inners, per Maria Estrella, 09.26.2014
	m.plid = m.plid + 1
	npo = npo+1
	m.po = TRANSFORM(npo)
	REPLACE xinwolog.plinqty WITH xinwolog.plinqty+nCtnQty IN xinwolog
	INSERT INTO xpl FROM MEMVAR
	m.units = .T.
	m.pack = '1'
	m.plid = m.plid + 1
	m.totqty = nUnitQty

	IF lInners
*		STORE (nCtnQty*nInners) TO nUnitQty
		STORE (nCtnQty) TO nUnitQty
	ENDIF
	REPLACE xinwolog.plunitsinqty WITH xinwolog.plunitsinqty+IIF(!lDoMix,nUnitQty,nTotQtyToAdd) IN xinwolog
	INSERT INTO xpl FROM MEMVAR
ENDPROC


******************************
PROCEDURE insertdetlines_domixhdr
******************************
	m.units = .F.
	m.totqty = nCtnQty
	m.plid = m.plid + 1
	npo = npo+1
	m.po = TRANSFORM(npo)
	REPLACE xinwolog.plinqty WITH xinwolog.plinqty+nCtnQty IN xinwolog
	INSERT INTO xpl FROM MEMVAR

******************************
PROCEDURE insertdetlines_domixdet
******************************
	m.units = .T.
	m.pack = '1'
	m.plid = m.plid + 1
	IF lInners
*		STORE (nCtnQty*nInners) TO nUnitQty
		STORE (nCtnQty) TO nUnitQty
	ENDIF
	m.po = TRANSFORM(npo)
	m.totqty = nUnitQty
	REPLACE xinwolog.plunitsinqty WITH xinwolog.plunitsinqty+nUnitQty IN xinwolog
	INSERT INTO xpl FROM MEMVAR
ENDPROC
