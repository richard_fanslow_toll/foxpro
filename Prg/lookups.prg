* general stuff

PUBLIC crlf, gnocalc, gexcelerror, gdisablemenu, gholdtype

crlf = CHR(13)+CHR(10)
gnocalc=.F.
gexcelerror=.F.
gdisablemenu=.F.
gholdtype=""

IF TYPE("gmilitarytime")="U"
	PUBLIC gmilitarytime
	gmilitarytime=.F.
ENDIF

public gupsintl
gupsintl="'PR','VI','GU','AE','MP','PW'"

PUBLIC xsmallpackagecarriers, xnotsmallpackagecarriers

**any adjustments to UPS/FEDEX must also be made on the website!
**added SUREPOST - mvw 06/11/14
**added 'UPS TRADE DIRECT TRUCK' - mvw 05/25/17
xsmallpackagecarriers="'BORDERLINX','AIRBORNE','DHL','FEDERAL EXP','FED EX','FEDEX','FED-EX','FREIGHT FORWARDER','PARCEL POST','RPS','SMARTPOST','SMART POST','UNITED PARCEL','U.P.S.','UPCO','UPS','US MAIL','US POSTAL','USPS','SUREPOST','MAIL INNOVATIONS'"
xnotsmallpackagecarriers="'FEDEX FREIGHT','FEDEX NATIONAL','UPS FREIGHT','UPS SUPPLY','UPS GRD - CONSO CANADA','DHL GLOBAL FORWARDING','DHL - FREIGHT','UPS TRADE DIRECT TRUCK','UPS TRADE DIRECT COD','UPS STANDARD TRADE DIRECT'"		&& removed Fedex Express dy 11/16/14

**added for invoicep report - mvw 01/01/13
PUBLIC gmakecheckspayableto
gmakecheckspayableto = "TGF Management Group Holdco Inc."

* account lists

PUBLIC gbillabongaccts
gbillabongaccts="6718,6719,6722,6744,6745,6746,6747,6748,6749"

PUBLIC gg3accounts, gg3accounts2
gg3accounts="6614,6621,6622,6623,6639,6657,6661,6664,6666,6667,6678,6679,6680,6698,6700,6701,6713,6714,6715,6716,6724"
gg3accounts2="6731,6732,6733,6734,6735,6736,6737,6738,6739,6740,6741,6742,6743,6750,6753,6767,6775,6807"

PUBLIC gofficedepotaccounts
gofficedepotaccounts="6493,6494,6495,6496,6519"

PUBLIC gjchooaccounts
gjchooaccounts="6135,6145,6146,6385"

PUBLIC gtkohighlineaccounts
gtkohighlineaccounts="4752,5318,5864,5910,6414,6668,6669,6670,6671"
*******added TMARG 6/16/17 6718
PUBLIC gcr8recaccounts
gcr8recaccounts="6416,6417,6418"

PUBLIC gbcnyrecaccounts
gbcnyrecaccounts = "6221,6520,6521,6522"

PUBLIC gbioworldaccounts
gbioworldaccounts = "6182,6554"

PUBLIC gtkoaccounts
gtkoaccounts = "5836,5865,6114,6705,6787"

PUBLIC gintradecoaccts
gintradecoaccts = "5154,6237"

PUBLIC gnanjingaccounts
gnanjingaccounts="4610,4694"

PUBLIC gcourtauldsaccounts
gcourtauldsaccounts="4677"

PUBLIC gyellsteelaccounts
gyellsteelaccounts="6209"

PUBLIC gariataccounts
gariataccounts="6532"

PUBLIC gs3accounts
gs3accounts="6803"

PUBLIC glifefactoryaccts
glifefactoryaccts="6034"

PUBLIC gnauticaaccounts
gnauticaaccounts="687,6356"

PUBLIC gin7accounts
gin7accounts="6059"

PUBLIC growoneaccounts
growoneaccounts="6468"

PUBLIC gmerkuryaccounts
gmerkuryaccounts="6561"

PUBLIC gsteelseriesaccts
gsteelseriesaccts="6612"

PUBLIC ggrandvoyageaccts
ggrandvoyageaccts="6617"

PUBLIC gagegroupaccounts
gagegroupaccounts="1285,6769"

PUBLIC g2xuaccounts
g2xuaccounts="6665"

PUBLIC galpha6accounts
galpha6accounts="5726"

*!*	PUBLIC gbillabongaccounts
*!*	gbillabongaccounts = "6718,6719,6744"

PUBLIC gbbcaccounts
gbbcaccounts = "6757,6761,6763,6771,6781,6783,6799"

PUBLIC gragboneaccounts
gragboneaccounts = "6699"

* when you add to this list make sure you alert Mike Winter to update the list in ViewBOL stored procedure for web tracking - mvw 02/27/13
* when you add to this list you must also add it to one of the 6 inlists below (except 5442 is trucking only) dy 8/12/11
* we are now at the 25 value limit...all further additions must be made to gMoretAcctList2. Joe, 03.23.2016
* new accounts go into gMoretAcctList2   dy 11/16/16

PUBLIC gMoretAcctList
gMoretAcctList = "5442,5447,5451,5452,5453,5741,5742,6088,6255,6262,6313,6402,6403,6405,6406,6407,6492,6598,6599,6600,6601,6648,6688,6603"

PUBLIC gMoretAcctList2
gMoretAcctList2 = "6690,6691,6692"

PUBLIC gMoretHBEST  && H BEST
gMoretHBEST = "5444,5453,5454,5455,5456,5457,5458,5459,5460,5461,5462,5463"

PUBLIC gMoretHPD && HIGH POINT DESIGN - HPD
gMoretHPD   = "5445,5452"

PUBLIC gMoretJM && JM base accounts
gMoretJM    = "5447,5741,5742,5743,6088,6255,6262,6313,6598,6599,6600,6601,6688,6690,6691,6692"

PUBLIC gMoretSBH  && SBH
gMoretSBH   = "5443,5451"

PUBLIC gMoretSK  && SK
gMoretSK    = "6402,6403,6405,6406,6407,6492"

PUBLIC gMoretSPM  && Spray Moret
gMoretSPM    = "6648"

PUBLIC gcourtauldsaccounts && Courtaulds accounts
gcourtauldsaccounts = "4677"

PUBLIC gSamsungaccounts && Samsung accounts
gSamsungaccounts   = "6649,6650,6651,6652"

**when you add to this list, make sure you alert Mike Winter to update the list in ViewBOL stored procedure for web tracking - mvw 02/27/13
**added 1747 - mvw 10/06/16
PUBLIC gJonesAcctList
gJonesAcctList = "1,62,1747"

PUBLIC gvfacctlist
gvfacctlist = "3272,4700,4727,4730,4731,4784,4826,4921,5504"

**when you add to this list, make sure you alert Mike Winter to update the list in ViewBOL stored procedure for web tracking - mvw 02/27/13
PUBLIC gmjacctlist, gmjacctlistplatinum
gmjacctlist = "6303,6304,6305,6306,6320,6321,6322,6323,6324,6325,6453,6364,6543"
gmjacctlistplatinum = ["000000006303","000000006304","000000006305","000000006306","000000006320","000000006321","000000006322","000000006323","000000006324","000000006325","000000006453","000000006364","000000006543"]

*public gdiv57acctlist, gdiv57acctlistplatinum	&& accounts where warehouse revenue goes into division 57
*gdiv57acctlist="4610,4677,4694,6137,6178,6182,6468,6476"
*gdiv57acctlistplatinum=["000000004610","000000004677","000000004694","000000006137","000000006178","000000006182","000000006468","000000006476"]

PUBLIC ituserlist
ituserlist =["JUAN","PAULG","DYOUNG","JOEB","MARK","ESTRELLA","WINTER","TMARG","FI7" ]

* arrays

if vartype(abillabong)="U"
	public array abillabong[18]
	abillabong[1]="RETAIL"
	abillabong[2]="WEB"
	abillabong[3]="CON"
	abillabong[4]="PNP"
	abillabong[5]="ITX"
	abillabong[6]="STD"
	abillabong[7]="TD-MISS"
	abillabong[8]="TD-CALG"
	abillabong[9]="TD-MISSCOD"
	abillabong[10]="TD-CALGCOD"
	abillabong[11]="VAS"
	abillabong[12]="WEB-VAS"
	abillabong[13]="CON-VAS"
	abillabong[14]="PNP-VAS"
	abillabong[15]="ITX-VAS"
	abillabong[16]="STD-VAS"
	abillabong[17]="SINGLES"
	abillabong[18]="MULTI-UNIT"
endif

IF VARTYPE(aswctype)="U"
	PUBLIC ARRAY aswctype[10,2]
	aswctype[1,1]="CONSIGNEE/DC"
	aswctype[1,2]="N"
	aswctype[2,1]="CNEE/DC (MERGE GROUPS)"
	aswctype[2,2]="G"
	aswctype[3,1]="CNEE/DC/CARRIER"
	aswctype[3,2]="Z"
	aswctype[4,1]="ONE PER PT"
	aswctype[4,2]="P"
	aswctype[5,1]="GROUP BY BL"
	aswctype[5,2]="B"
	aswctype[6,1]="GROUP BY PO"
	aswctype[6,2]="O"
	aswctype[7,1]="GROUP BY PO/CARRIER"
	aswctype[7,2]="C"
	aswctype[8,1]="GROUP BY GROUPS"
	aswctype[8,2]="Y"
	aswctype[9,1]="ONE PER WO"
	aswctype[9,2]="A"
	aswctype[10,1]="SEPARATED"
	aswctype[10,2]="S"
ENDIF

IF VARTYPE(awotype)="U"
	PUBLIC ARRAY awotype[4,2]
	awotype[1,1]="OCEAN"
	awotype[1,2]="O"
	awotype[2,1]="AIR"
	awotype[2,2]="A"
	awotype[3,1]="DOMESTIC"
	awotype[3,2]="D"
	awotype[4,1]="RAIL"
	awotype[4,2]="R"
ENDIF

IF VARTYPE(axwotype)="U"
	PUBLIC ARRAY axwotype[6]
	axwotype[1]="ALL"
	axwotype[2]="OCEAN FULL"
	axwotype[3]="OCEAN LOOSE"
	axwotype[4]="OCEAN EXPORT"
	axwotype[5]="AIR"
	axwotype[6]="DOMESTIC"
ENDIF

IF VARTYPE(aunittype)="U"
	PUBLIC ARRAY aunittype[12,3]
	aunittype[1,1]=""
	aunittype[1,2]=""
	aunittype[1,3]=""
	aunittype[2,1]="cartons"
	aunittype[2,2]="1"
	aunittype[2,3]="ctn"
	aunittype[3,1]="units"
	aunittype[3,2]="2"
	aunittype[3,3]="un"
	aunittype[4,1]="dozens"
	aunittype[4,2]="3"
	aunittype[4,3]="dz"
	aunittype[5,1]="bundle"
	aunittype[5,2]="4"
	aunittype[5,3]="bun"
	aunittype[6,1]="pallets"
	aunittype[6,2]="5"
	aunittype[6,3]="pall"
	aunittype[7,1]="cubic ft"
	aunittype[7,2]="6"
	aunittype[7,3]="cuft"
	aunittype[8,1]="container"
	aunittype[8,2]="7"
	aunittype[8,3]="cont"
	aunittype[9,1]="polybags"
	aunittype[9,2]="8"
	aunittype[9,3]="poly"
	aunittype[10,1]="hours"
	aunittype[10,2]="9"
	aunittype[10,3]="hour"
	aunittype[11,1]="side"
	aunittype[11,2]="10"
	aunittype[11,3]="side"
	aunittype[12,1]="project"
	aunittype[12,2]="11"
	aunittype[12,3]="project"
ENDIF

IF VARTYPE(awarehouse)="U"
	PUBLIC ARRAY awarehouse[5]
	awarehouse[1]="N"
	awarehouse[2]="S1"
	awarehouse[3]="S2"
	awarehouse[4]="ST"
	awarehouse[5]="M"
ENDIF

IF VARTYPE(achassize)="U"
	PUBLIC ARRAY achassize[3]
	achassize[1]="20'"
	achassize[2]="40'"
	achassize[3]="40'S"
ENDIF

IF VARTYPE(acfsinstruc)="U"
	PUBLIC ARRAY acfsinstruc[4,2]
	acfsinstruc[1,1]="FULL CTR"
	acfsinstruc[1,2]="C"
	acfsinstruc[2,1]="STRIP"
	acfsinstruc[2,2]="S"
	acfsinstruc[3,1]="HEADLOAD"
	acfsinstruc[3,2]="H"
	acfsinstruc[4,1]="SWINGLOAD"
	acfsinstruc[4,2]="W"
ENDIF

IF VARTYPE(aqtytype)="U"
	PUBLIC ARRAY aqtytype[7,2]
	aqtytype[1,1]="CTN"
	aqtytype[1,2]="CARTONS"
	aqtytype[2,1]="GOH"
	aqtytype[2,2]="GOH"
	aqtytype[3,1]="ROL"
	aqtytype[3,2]="ROLLS"
	aqtytype[4,1]="PCG"
	aqtytype[4,2]="PCGOODS"
	aqtytype[5,1]="PAL"
	aqtytype[5,2]="PALLETS"
	aqtytype[6,1]="TRL"
	aqtytype[6,2]="TRAILER"
	aqtytype[7,1]="CON"
	aqtytype[7,2]="CONTRS"
ENDIF

IF VARTYPE(ablstatus)="U"
	PUBLIC ARRAY ablstatus[10,2]
	ablstatus[1,1]=""
	ablstatus[1,2]=""
	ablstatus[2,1]="PENDING STRIPPING"
	ablstatus[2,2]="1"
	ablstatus[3,1]="ON HAND"
	ablstatus[3,2]="2"
	ablstatus[4,1]="OUT FOR DELIVERY"
	ablstatus[4,2]="3"
	ablstatus[5,1]="REFUSED"
	ablstatus[5,2]="4"
	ablstatus[6,1]="UNDELIVERED"
	ablstatus[6,2]="5"
	ablstatus[7,1]="PARTIALLY REFUSED"
	ablstatus[7,2]="6"
	ablstatus[8,1]="PARTIALLY UNDELIVERED"
	ablstatus[8,2]="7"
	ablstatus[9,1]="PARTIALLY DELIVERED"
	ablstatus[9,2]="8"
	ablstatus[10,1]="DELIVERED"
	ablstatus[10,2]="9"
ENDIF

IF VARTYPE(astate)="U"
	PUBLIC ARRAY astate[51]
	astate[1]="AK"
	astate[2]="AL"
	astate[3]="AR"
	astate[4]="AZ"
	astate[5]="CA"
	astate[6]="CO"
	astate[7]="CT"
	astate[8]="DC"
	astate[9]="DE"
	astate[10]="FL"
	astate[11]="GA"
	astate[12]="HI"
	astate[13]="IA"
	astate[14]="ID"
	astate[15]="IL"
	astate[16]="IN"
	astate[17]="KS"
	astate[18]="KY"
	astate[19]="LA"
	astate[20]="MA"
	astate[21]="MD"
	astate[22]="ME"
	astate[23]="MI"
	astate[24]="MN"
	astate[25]="MO"
	astate[26]="MS"
	astate[27]="MT"
	astate[28]="NC"
	astate[29]="ND"
	astate[30]="NE"
	astate[31]="NH"
	astate[32]="NJ"
	astate[33]="NM"
	astate[34]="NV"
	astate[35]="NY"
	astate[36]="OH"
	astate[37]="OK"
	astate[38]="OR"
	astate[39]="PA"
	astate[40]="RI"
	astate[41]="SC"
	astate[42]="SD"
	astate[43]="TN"
	astate[44]="TX"
	astate[45]="UT"
	astate[46]="VA"
	astate[47]="VT"
	astate[48]="WA"
	astate[49]="WI"
	astate[50]="WV"
	astate[51]="WY"
ENDIF

IF VARTYPE(aoffice)="U"
	PUBLIC ARRAY aoffice[10]
	aoffice[1]="N"
	aoffice[2]="C"
	aoffice[3]="L"
	aoffice[4]="M"
	aoffice[5]="R"
	aoffice[6]="K"
	aoffice[7]="O"
	aoffice[8]="X"
	aoffice[9]="Y"
	aoffice[10]="Z"
ENDIF

IF VARTYPE(ahawbstatus)="U"
	PUBLIC ARRAY ahawbstatus[5,2]
	nmonth = MONTH(DATE())
	ahawbstatus[1,1]=CMONTH(DATE())
	ahawbstatus[2,1]=CMONTH(CTOD(TRANSFORM(IIF(nmonth=12,1,nmonth+1))+"/01/"+TRANSFORM(YEAR(DATE())+IIF(nmonth=12,1,0))))
	ahawbstatus[3,1]=CMONTH(CTOD(TRANSFORM(IIF(nmonth>=11,nmonth+2-12,nmonth+2))+"/01/"+TRANSFORM(YEAR(DATE())+IIF(nmonth>=11,1,0))))
	ahawbstatus[4,1]=CMONTH(CTOD(TRANSFORM(IIF(nmonth>=10,nmonth+3-12,nmonth+3))+"/01/"+TRANSFORM(YEAR(DATE())+IIF(nmonth>=10,1,0))))
	ahawbstatus[5,1]="HOT"
	ahawbstatus[1,2]=UPPER(LEFT(ahawbstatus[1,1],3))
	ahawbstatus[2,2]=UPPER(LEFT(ahawbstatus[2,1],3))
	ahawbstatus[3,2]=UPPER(LEFT(ahawbstatus[3,1],3))
	ahawbstatus[4,2]=UPPER(LEFT(ahawbstatus[4,1],3))
	ahawbstatus[5,2]="HOT"
ENDIF

IF VARTYPE(acarriercode)="U"
	PUBLIC ARRAY acarriercode[6,2]
	acarriercode[1,1]="UPS"
	acarriercode[1,2]="UPS"
	acarriercode[2,1]="FEDEX"
	acarriercode[2,2]="FDX"
	acarriercode[3,1]="SMARTPOST"
	acarriercode[3,2]="FSP"
	acarriercode[4,1]="MAIL INNOVATIONS"
	acarriercode[4,2]="UMI"
	acarriercode[5,1]="USPS"
	acarriercode[5,2]="USP"
	acarriercode[6,1]="DHL"
	acarriercode[6,2]="DHL"
ENDIF

IF VARTYPE(ayard)="U"
	PUBLIC ARRAY ayard[4,2]
	ayard[1,1]="CARTERET"
	ayard[1,2]="N"
	ayard[2,1]="SAN PEDRO"
	ayard[2,2]="C"
	ayard[3,1]="WILMINGTON"
	ayard[3,2]="W"
	ayard[4,1]="MIRA LOMA"
	ayard[4,2]="A"
ENDIF

IF VARTYPE(acstype)="U"
	PUBLIC ARRAY acstype[41]
	acstype[1]="APPOINTMENT FEE"
	acstype[2]="BOBTAIL"
	acstype[3]="CHASSIS LEASE FEE"
	acstype[4]="CHASSIS MOVE"
	acstype[5]="CLEAN TRUCK FEE"
	acstype[6]="DELIVERY ATTEMPT"
	acstype[7]="DO CANCEL ADMIN FEE"
	acstype[8]="DO PROCESSING"
	acstype[9]="DRIVER ASSIST"
	acstype[10]="DRIVER DETENTION"
	acstype[11]="EXPEDITED SERVICE"
	acstype[12]="HAZMAT"
	acstype[13]="HAZMAT PLACARDS"
	acstype[14]="IN-BOND"
	acstype[15]="NIGHT GATE"
	acstype[16]="NIT TUNNEL"
	acstype[17]="NYCT"
	acstype[18]="OTHER"
	acstype[19]="OVERWEIGHT"
	acstype[20]="PALLET"
	acstype[21]="PER DIEM SURCHARGE"
	acstype[22]="PERMIT TO TRANSFER"
	acstype[23]="PICKUP ATTEMPT"
	acstype[24]="PIER PASS"
	acstype[25]="PORT EVERGLADES"
	acstype[26]="PRE-PULL"
	acstype[27]="RETURN TO YARD"
	acstype[28]="SATURDAY/HOLIDAY"
	acstype[29]="SCALE FEE"
	acstype[30]="SEARS WAITING TIME"
	acstype[31]="SHUTTLE"
	acstype[32]="SORTING"
	acstype[33]="STOPOFF"
	acstype[34]="STORAGE"
	acstype[35]="SUMMIT CFT"
	acstype[36]="SUMMIT PIER PASS"
	acstype[37]="SUMMIT NIGHT GATE"
	acstype[38]="SUMMIT DAY GATE"
	acstype[39]="SUMMIT SATURDAY GATE"
	acstype[40]="TERMINAL CONGESTION"
	acstype[41]="TIR"
ENDIF

IF VARTYPE(acscontsize)="U"
	PUBLIC ARRAY acscontsize[3]
	acscontsize[1]="20'"
	acscontsize[2]="40'"
	acscontsize[3]="45'"
ENDIF

IF VARTYPE(apureason)="U"
	PUBLIC ARRAY apureason[18]
	apureason[1]="Booking not on file"
	apureason[2]="Cancelled pick up"
	apureason[3]="Closed area"
	apureason[4]="Container not at pier/rail"
	apureason[5]="Container not available"
	apureason[6]="Container not unloaded"
	apureason[7]="Container picked by outside carrier"
	apureason[8]="Customs exam"
	apureason[9]="Driver shut-out"
	apureason[10]="Freight not ready"
	apureason[11]="No available equipment"
	apureason[12]="No chassis"
	apureason[13]="No customs release"
	apureason[14]="No customs or freight release"
	apureason[15]="No freight release"
	apureason[16]="No usable chassis"
	apureason[17]="Not guaranteed"
	apureason[18]="Vessel not arrived"
ENDIF

IF VARTYPE(adelreason)="U"
	PUBLIC ARRAY adelreason[12]
	adelreason[1]="Booking not on file"
	adelreason[2]="Cartons shortage"
	adelreason[3]="Container not in location"
	adelreason[4]="Damaged freight"
	adelreason[5]="Hazardous not updated"
	adelreason[6]="Incorrect delivery loc given"
	adelreason[7]="Incorrect haz documentation"
	adelreason[8]="Lost at pier"
	adelreason[9]="No appointment"
	adelreason[10]="No C.O.D. upon arrival"
	adelreason[11]="P.O. cancelled"
	adelreason[12]="Refused freight"
ENDIF

IF VARTYPE(amovetype)="U"
	PUBLIC ARRAY amovetype[5,2]
	amovetype[1,1]="EXPORT"
	amovetype[1,2]="E"
	amovetype[2,1]="IMPORT"
	amovetype[2,2]="I"
	amovetype[3,1]="LIVE LOAD"
	amovetype[3,2]="L"
	amovetype[4,1]="LIVE UNLOAD"
	amovetype[4,2]="U"
	amovetype[5,1]="RETURN"
	amovetype[5,2]="R"
ENDIF

IF VARTYPE(aoldratetype)="U"
	PUBLIC ARRAY aoldratetype[23]
	aoldratetype[1]="ACCT DETENTION "
	aoldratetype[2]="AIR FREIGHT    "
	aoldratetype[3]="BILL OF LADING "
	aoldratetype[4]="CONT STORAGE   "
	aoldratetype[5]="DEDICATED SPACE"
	aoldratetype[6]="DOMESTIC       "
	aoldratetype[7]="HANDLING       "
	aoldratetype[8]="LABELING       "
	aoldratetype[9]="LABOR          "
	aoldratetype[10]="OUTBOUND       "
	aoldratetype[11]="PALLET         "
	aoldratetype[12]="PICK & PACK    "
	aoldratetype[13]="PIER FULL      "
	aoldratetype[14]="PIER LOOSE     "
	aoldratetype[15]="PRE-PULL       "
	aoldratetype[16]="RETURN TO YARD "
	aoldratetype[17]="RETURNS        "
	aoldratetype[18]="SHIPPING       "
	aoldratetype[19]="SORTING        "
	aoldratetype[20]="SPECIAL PROJECT"
	aoldratetype[21]="STORAGE        "
	aoldratetype[22]="TRANSLOAD      "
	aoldratetype[23]="UPS-FEDEX      "
ENDIF

IF VARTYPE(aapptstatus)="U"
	PUBLIC ARRAY aapptstatus[7]
	aapptstatus[1]=''
	aapptstatus[2]="REMOVED"
	aapptstatus[3]="REFUSED"
	aapptstatus[4]="REJECTED"
	aapptstatus[5]="CANCELED"
	aapptstatus[6]="NO SHOW"
	aapptstatus[7]="OVERFLOW"
ENDIF

IF VARTYPE(anjmod)="U"
	PUBLIC ARRAY anjmod[2,2]
	anjmod[1,1]="MOD I"
	anjmod[1,2]="I"
	anjmod[2,1]="MOD J"
	anjmod[2,2]="J"
ENDIF

IF VARTYPE(acamod)="U"
	PUBLIC ARRAY acamod[7,2]
	acamod[1,1]="MOD A"
	acamod[1,2]="4"
	acamod[2,1]="MOD B"
	acamod[2,2]="5"
	acamod[3,1]="MOD C"
	acamod[3,2]="6"
	acamod[4,1]="MOD D"
	acamod[4,2]="7"
	acamod[5,1]="MOD E"
	acamod[5,2]="0"
	acamod[6,1]="MOD F"
	acamod[6,2]="1"
	acamod[7,1]="Tload"
	acamod[7,2]="8"
ENDIF

IF VARTYPE(akymod)="U"
	PUBLIC ARRAY akymod[2,2]
	akymod[1,1]="ARIAT"
	akymod[1,2]="K"
	akymod[2,1]="MOD S"
	akymod[2,2]="S"
ENDIF

IF VARTYPE(afxqtytype)="U"
	PUBLIC ARRAY afxqtytype[7]
	afxqtytype[1]="CARTONS"
	afxqtytype[2]="CONTRS"
	afxqtytype[3]="GOH"
	afxqtytype[4]="PALLETS"
	afxqtytype[5]="PCGOODS"
	afxqtytype[6]="ROLL"
	afxqtytype[7]="TRAILER"
ENDIF

IF VARTYPE(areqstatus)="U"
	PUBLIC ARRAY areqstatus[5]
	areqstatus[1]="NEW"
	areqstatus[2]="ON ORDER"
	areqstatus[3]="NOT APPROVED"
	areqstatus[4]="RECEIVED"
	areqstatus[5]="RETURNED"
ENDIF

IF VARTYPE(acomponstat)="U"
	PUBLIC ARRAY acomponstat[3]
	acomponstat[1]="MISSING"
	acomponstat[2]="PARTIAL"
	acomponstat[3]="RECEIVED"
ENDIF

IF VARTYPE(aholdtype)="U"
	PUBLIC ARRAY aholdtype[20]
	aholdtype[1]="Alternate"
	aholdtype[2]="Canada"
	aholdtype[3]="Customer"
	aholdtype[4]="Cut"
	aholdtype[5]="Cycle"
	aholdtype[6]="Damage"
	aholdtype[7]="IC"
	aholdtype[8]="IC Int"	&& dy 1/30/18
	aholdtype[9]="IC US"
	aholdtype[10]="Other"
	aholdtype[11]="Overflow"
	aholdtype[12]="Pick Short"
	aholdtype[13]="Pre-Count"
	aholdtype[14]="QC"
	aholdtype[15]="Return"
	aholdtype[16]="Short Pick"
	aholdtype[17]="Skip"
	aholdtype[18]="SKU Cycle"
	aholdtype[19]="Spec Proj"
	aholdtype[20]="Trailer"
ENDIF

IF VARTYPE(apallettype)="U"
	PUBLIC ARRAY apallettype[7]
	apallettype[1]="CHEP"
	apallettype[2]="GRADE A"
	apallettype[3]="HEAT TREATED"
	apallettype[4]="HT FUMIGATED"
	apallettype[5]="PLASTIC"
	apallettype[6]="STANDARD"
	apallettype[7]="STD + STRETCH"
ENDIF

IF VARTYPE(abltype)="U"
	PUBLIC ARRAY abltype[2]
	abltype[1]="REGULAR"
	abltype[2]="MASTER"
ENDIF

IF VARTYPE(afmioox)="U"
	PUBLIC ARRAY afmioox[3]
	afmioox[1]=""
	afmioox[2]="FMI"
	afmioox[3]="O/O"
ENDIF

IF VARTYPE(aqcloctype)="U"
	PUBLIC ARRAY aqcloctype[7]
	aqcloctype[1]="AIRPORT"
	aqcloctype[2]="CFS"
	aqcloctype[3]="CUSTOMER"
	aqcloctype[4]="DEPOT"
	aqcloctype[5]="REST AREA"
	aqcloctype[6]="TERMINAL"
	aqcloctype[7]="TOLL"
ENDIF

IF VARTYPE(arateaccttype)="U"
	PUBLIC ARRAY arateaccttype[2]
	arateaccttype[1]="CONTRACT"
	arateaccttype[2]="NON-CONTRACT"
ENDIF

IF VARTYPE(ahousedirect)="U"
	PUBLIC ARRAY ahousedirect[2]
	ahousedirect[1]="HOUSE"
	ahousedirect[2]="DIRECT"
ENDIF

IF VARTYPE(aratecommtype)="U"
	PUBLIC ARRAY aratecommtype[3]
	aratecommtype[1]="GENERAL"
	aratecommtype[2]="IT"
	aratecommtype[3]="RATE"
ENDIF

IF VARTYPE(ayear)="U"
	PUBLIC ARRAY ayear[2]
	ayear[1]="Calendar"
	ayear[2]="Fiscal"
ENDIF

IF VARTYPE(atrackwotype)="U"
	PUBLIC ARRAY atrackwotype[5]
	atrackwotype[1]="OCEAN FULL"
	atrackwotype[2]="OCEAN EXPORT"
	atrackwotype[3]="OCEAN LOOSE"
	atrackwotype[4]="AIR"
	atrackwotype[5]="DOMESTIC"
ENDIF

IF VARTYPE(awostatus)="U"
	PUBLIC ARRAY awostatus[14]
	awostatus[1]="NOT AVAILABLE"
	awostatus[2]="NOT DISPATCHED"
	awostatus[3]="AVAILABLE"
	awostatus[4]="PICKUP IN PROGRESS"
	awostatus[5]="PICKED UP"
	awostatus[6]="EXPORT IN PROGRESS"
	awostatus[7]="DELIVERY IN PROGRESS"
	awostatus[8]="DELIVERED"
	awostatus[9]="EMPTY IN YARD"
	awostatus[10]="DROPPED CONTAINER"
	awostatus[11]="DROPPED TRAILER"
	awostatus[12]="ON HOLD IN YARD"
	awostatus[13]="RETURN IN PROGRESS"
	awostatus[14]="DONE"
ENDIF

PUBLIC ARRAY aariattype[5]
aariattype[1]="RUSH"
aariattype[2]="STD"
aariattype[3]="DTOC"
aariattype[4]="SINGLES"
aariattype[5]=""


DECLARE INTEGER ShellExecute;
	IN shell32.DLL;
	INTEGER nWinHandle,;
	STRING  cOperation,;
	STRING  cFilename ,;
	STRING  cParameters,;
	STRING  cDirectory,;
	INTEGER nShowWindow

DECLARE INTEGER FindWindow;
	IN Win32api;
	STRING cNull,STRING cWinN
