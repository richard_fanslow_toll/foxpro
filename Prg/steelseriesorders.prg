* Steelseries order report

utilsetup("STEELSERIESORDERS")

use (wf("K",6612)+"outship") in 0
use (wf("K",6612)+"pt") in 0

xsqlexec("accountid=6612 and ptdate>{"+dtoc(date()-7)+"}","ptsave",,"wh")
index on str(accountid,4)+ship_ref tag acctpt

* 

xdate=date()
*xdate={9/7}

xstartdt=ctot(dtoc(xdate-2)+" 12:00:00")
xenddt=ctot(dtoc(xdate-1)+" 11:59:59")

select ;
	xdate-1 as orderdate, ;
	sum(iif(batch_num="WEB" and between(deletedt,xstartdt,xenddt),qty,0)) as webreceived, ;
	sum(iif(batch_num="WEB" and !emptynul(packed) and packed=xdate-1,qty,0)) as webpicked, ;
	sum(iif(batch_num="WEB" and !emptynul(del_date) and del_date=xdate-1,qty,0)) as webdispatched, ;
	sum(iif(batch_num="WEB" and !emptynyl(cancelrtsdt) and cancelrtsdt=xdate-1,qty,0)) as webcanceled, ;
	sum(iif(batch_num#"WEB" and between(deletedt,xstartdt,xenddt),qty,0)) as distreceived, ;
	sum(iif(batch_num#"WEB" and !emptynul(packed) and packed=xdate-1,qty,0)) as distpicked, ;
	sum(iif(batch_num#"WEB" and !emptynul(del_date) and del_date=xdate-1,qty,0)) as distdispatched, ;
	sum(iif(batch_num#"WEB" and !emptynul(cancelrtsdt) and cancelrtsdt=xdate-1,qty,0)) as distcanceled ;
	from outship ;
	where accountid=6612 ;
	group by accountid ;
	into cursor xrpt readwrite

if reccount()=0
	append blank in xrpt
	replace orderdate with xdate-1 in xrpt
endif

*

select ;
	xdate-1 as orderdate, ;
	sum(iif(batch_num="WEB",qty,0)) as webreceived, ;
	sum(iif(batch_num#"WEB",qty,0)) as distreceived ;
	from pt ;
	where accountid=6612 and between(adddt,xstartdt,xenddt) ;
	group by accountid ;
	into cursor xtemp
	
replace webreceived with xrpt.webreceived+xtemp.webreceived, distreceived with xrpt.distreceived+xtemp.distreceived in xrpt

*

store 0 to xwebdel, xdistdel

xsqlexec("select * from info where zdate={"+dtoc(xdate-1)+"} and mod='S' and message='PT deleted'",,,"wh")

scan 
	if info.qty#0
		if seek(str(accountid,4)+ship_ref,"ptsave","acctpt")
			if ptsave.batch_num="WEB"
				xwebdel=xwebdel+info.qty
			else
				xdistdel=xdistdel+info.qty
			endif
		endif
	else		
		if seek(str(accountid,4)+ship_ref,"ptsave","acctpt")
			if ptsave.batch_num="WEB"
				xwebdel=xwebdel+ptsave.qty
			else
				xdistdel=xdistdel+ptsave.qty
			endif
		endif
	endif
endscan

replace webcanceled with xrpt.webcanceled+xwebdel, distcanceled with xrpt.distcanceled+xdistdel in xrpt

*

oexcel=createobject("excel.application")
oexcel.visible=.f.
oworkbook=oexcel.workbooks.open("f:\auto\steelseriesorders.xls")
osheet1=oworkbook.worksheets[1]

with osheet1

.range("A2").value = xrpt.orderdate
.range("B2").value = xrpt.webreceived
.range("C2").value = xrpt.webpicked
.range("D2").value = xrpt.webdispatched
.range("E2").value = xrpt.webcanceled
.range("F2").value = xrpt.distreceived
.range("G2").value = xrpt.distpicked
.range("H2").value = xrpt.distdispatched
.range("I2").value = xrpt.distcanceled

endwith

oworkbook.saveas("h:\fox\orders.xlsx",51)
oexcel.quit()
release oexcel
set step on 
*

tsendto="juan.rocio@tollgroup.com"
*tsendto="Dyoung@fmiint.com"
tcc=""
tattach="h:\fox\orders.xlsx"
tFrom="TGF Operations  <tgfoperations@fmiint.com>"
tmessage=""
tSubject="Orders for "+dtoc(xrpt.orderdate)

Do Form dartmail2 With tsendto,tFrom,tSubject,tcc,tattach,tmessage,"A"

delete file h:\fox\orders.xlsx

use in outship
use in pt
use in ptsave
use in info

*

schedupdate()

_screen.Caption=gscreencaption
on error
