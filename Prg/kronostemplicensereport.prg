* create count of timekeeper licenses used by the temp agencies
LPARAMETERS tcTempCo

utilsetup("KRONOSTEMPLICENSEREPORT")

LOCAL loKronosTempLicenseReport, lcTempCo

* build as f:\util\kronos\kronostemplicensereport.exe
loKronosTempLicenseReport = CREATEOBJECT('KronosTempLicenseReport')

IF TYPE('tcTempCo') = "L" THEN
	lcTempCo = "ALL"
ELSE
	lcTempCo = ALLTRIM(tcTempCo)
ENDIF
loKronosTempLicenseReport.SetTempCo( lcTempCo )

loKronosTempLicenseReport.MAIN()

schedupdate()

CLOSE DATABASES ALL
RETURN

#DEFINE LOGIT 1
#DEFINE WAITIT 2
#DEFINE NOWAITIT 4
#DEFINE SENDIT 8
#DEFINE CRLF CHR(13) + CHR(10)
#DEFINE LF CHR(10)
#DEFINE RETURN_DATA_MANDATORY .F.
#DEFINE RETURN_DATA_NOT_MANDATORY .T.

DEFINE CLASS KronosTempLicenseReport AS CUSTOM

	cProcessName = 'KronosTempLicenseReport'

	lTestMode = .T.  && .F. FOR NORMAL OPERATION, .T. TO SUPPRESS ALL BUT DEVELOPER EMAILS AND CHANGE INPUT TABLE, ETC.

	***********************   SET REPORT TYPE BEFORE BUILDING EXE   **********************
	**************************************************************************************
	**************************************************************************************

	* which payroll server, and associated connections, to hit
	lUsePAYROLL2Server = .T.

	lAutoYield = .T.

	*!*		* !!! When you set this flag to .T. make the exe = KronosTempLicenseReport-TUG.EXE !!!!!
	*!*		lDoSpecialTUGReport = .F.

	cStartTime = TTOC(DATETIME())

	* connection properties
	nSQLHandle = 0

	* file properties
	nFileHandle = 0

	* table properties
	*cFoxProTimeReviewerTable = "F:\UTIL\KRONOS\DATA\TIMERVWR"
	
	* report filter properties
	cTempCo = ""
	cTempCoName = ""

	* date properties
	dtNow = DATETIME()

	dToday = DATE()
	* to force different pay weeks
	*dToday = {^2009-07-09}

	* wait window properties
	nWaitWindowTimeout = 2
	lWaitWindowIsOn = .T.

	* logfile properties
	lLoggingIsOn = .T.
	cLogFile = 'F:\UTIL\KRONOS\Logfiles\KronosTempLicenseReport_log.txt'

	* INTERNAL email properties
	lSendInternalEmailIsOn = .T.
	cFrom = 'FMI Corporate <fmicorporate@fmiint.com>'
	*cSendTo = 'Mark Bennett <mbennett@fmiint.com>'
	cSendTo = 'Mark Bennett <mbennett@fmiint.com>'
	cCC = ''
	cSubject = 'Kronos Temp Licensing Report for: ' + DTOC(DATE())
	cAttach = ''
	cBodyText = ''

	cCOMPUTERNAME = ''
	cUSERNAME = ''

	FUNCTION INIT
		IF NOT DODEFAULT()
			RETURN .F.
		ENDIF
		WITH THIS
			CLEAR
			*SET RESOURCE OFF
			CLOSE DATA
			SET CENTURY ON
			SET DATE AMERICAN
			SET HOURS TO 24
			SET ANSI ON
			SET TALK OFF
			SET DELETED ON
			SET CONSOLE OFF
			SET EXCLUSIVE OFF
			SET SAFETY OFF
			SET EXACT OFF
			SET STATUS BAR ON
			SET SYSMENU OFF
			SET ENGINEBEHAVIOR 70
			_VFP.AUTOYIELD = .lAutoYield
			.cCOMPUTERNAME = GETENV("COMPUTERNAME")
			.cUSERNAME = GETENV("USERNAME")
			IF .lTestMode THEN
				.cSendTo = 'Mark Bennett <mbennett@fmiint.com>'
				.cLogFile = 'F:\UTIL\KRONOS\Logfiles\KronosTempLicenseReport_log_TESTMODE.txt'
			ENDIF
			.lLoggingIsOn = .lLoggingIsOn AND NOT EMPTY(.cLogFile)
			IF .lLoggingIsOn THEN
				SET ALTERNATE TO (.cLogFile) ADDITIVE
				SET ALTERNATE ON
			ENDIF
		ENDWITH
	ENDFUNC


	FUNCTION DESTROY
		WITH THIS
			IF .lLoggingIsOn  THEN
				SET ALTERNATE OFF
				SET ALTERNATE TO
			ENDIF
		ENDWITH
		DODEFAULT()
	ENDFUNC


	FUNCTION MAIN
		WITH THIS
			LOCAL lnNumberOfErrors, lcSQL, lloError, llValid
			LOCAL lcXLFileName
			LOCAL oExcel, oWorkbook, oWorksheet, lnRow, lcRow
			LOCAL lcColsToProcess, lnColumn, lcBoldColumn, i, j, lcBoldRow, luCellValue, lcTargetDirectory, lcFiletoSaveAs
			LOCAL lcFileDate, lcTempCo, lcTitle, llSaveAgain, lcFileDate
			LOCAL lcSendTo, lcCC, lcBodyText, lcTempCoWhere

			TRY
				lnNumberOfErrors = 0

				.TrackProgress('Kronos Temp Licensing Report process started....', LOGIT+SENDIT+NOWAITIT)
				.TrackProgress('COMPUTERNAME = ' + .cCOMPUTERNAME, LOGIT+SENDIT)
				.TrackProgress('USERNAME = ' + .cUSERNAME, LOGIT+SENDIT)
				IF .lTestMode THEN
					.TrackProgress('                        TEST MODE                        ', LOGIT+SENDIT)
				ENDIF
				.TrackProgress('PROJECT = KRONOSTEMPLICENSEREPORT', LOGIT+SENDIT)
				
				IF EMPTY(.cTempCoName) THEN
					THROW
				ENDIF

				.cSubject = 'Kronos Temp Licensing Report for ' + .cTempCoName + DTOC(.dToday) && + " TRISTATEAUTOFWD"

				* calculate pay period start/end dates.
				* We go back from today until we hit a Saturday (DOW = 7) , which is the end day of the previous pay period.
				* then we subtract 6 days from that to get the prior Sunday (DOW = 1), which is the start day of the previous pay period.
			
				IF USED('CUR_ACTIVE') THEN
					USE IN CUR_ACTIVE
				ENDIF
				IF USED('CUR_NOT_ACTIVE') THEN
					USE IN CUR_NOT_ACTIVE
				ENDIF

				IF NOT .lTestMode THEN
					* assign Send to based on TempCo (which indicates temp agency)
					DO CASE
						CASE .cTempCo == 'A'
							.cSendTo = 'TriState'
							.cCC = 'Mark Bennett <mbennett@fmiint.com>'
						CASE .cTempCo == 'B'
							.cSendTo = 'Select'
							.cCC = 'Mark Bennett <mbennett@fmiint.com>'
						OTHERWISE
							.cSendTo = 'Mark Bennett <mbennett@fmiint.com>'
							.cCC = ""
					ENDCASE
				ENDIF

				* filter by TempCo if there was a passed TempCo parameters
				IF (NOT EMPTY(.cTempCo)) AND (NOT .cTempCo == 'ALL') THEN
					lcTempCoWhere = " AND (HOMELABORLEVELNM5 = '" + .cTempCo + "') "
				ELSE
					lcTempCoWhere = ""
				ENDIF
						
				* main SQL
				lcSQL = ;
					" SELECT " + ;
					" HOMELABORLEVELNM5 AS AGENCYNUM, " + ;
					" HOMELABORLEVELDSC5 AS AGENCYDESC, " + ;
					" COUNT(*) AS HEADCOUNT " + ;
					" FROM VP_EMPLOYEEV42 " + ;
					" WHERE EMPLOYMENTSTATUS <> 'Terminated' " + ;
					" AND HOMELABORLEVELNM1 IN ('AXA','SXI','ZXU','TMP') " + ;
					lcTempCoWhere + ;
					" GROUP BY HOMELABORLEVELNM5, HOMELABORLEVELDSC5 " + ;
					" ORDER BY HOMELABORLEVELNM5, HOMELABORLEVELDSC5 "

				IF .lTestMode THEN
					.TrackProgress('', LOGIT+SENDIT)
					.TrackProgress('lcSQL =' + lcSQL, LOGIT+SENDIT)
				ENDIF

				.TrackProgress('Connecting to WTK System....', LOGIT+SENDIT)

				* access kronos
				OPEN DATABASE F:\UTIL\KRONOS\KRONOSEDI

				.nSQLHandle = SQLCONNECT("WTK_ON_PAYROLL2","sa","kr0n1tes")

				IF .nSQLHandle > 0 THEN

					IF .ExecSQL(lcSQL, 'CUR_ACTIVE', RETURN_DATA_MANDATORY) THEN

*!*		SELECT CUR_ACTIVE
*!*		COPY TO c:\a\activetemps.xls xl5
*!*		throw
	
		
						WAIT WINDOW NOWAIT "Preparing data..."

						WAIT WINDOW NOWAIT "Opening Excel..."
						oExcel = CREATEOBJECT("excel.application")
						oExcel.VISIBLE = .F.
						oWorkbook = oExcel.workbooks.OPEN("F:\UTIL\KRONOS\TEMPLATES\KronosTempLicenseReport.XLS")

						WAIT WINDOW NOWAIT "Looking for target directory..."
						* see if target directory exists, and, if not, create it, e.g. F:\timeclk\adpfiles\01-07-2005
						lcFileDate = PADL(MONTH(.dToday),2,"0") + "-"  + PADL(DAY(.dToday),2,"0") + "-" + PADL(YEAR(.dToday),4,"0")
						lcTargetDirectory = "F:\UTIL\KRONOS\TEMPTIMEREPORTS\" && + lcFileDate + "\"
						lcFiletoSaveAs = lcTargetDirectory + .cTempCoName + "Licensing " + lcFileDate
						lcXLFileName = lcFiletoSaveAs + ".XLS"

						* create directory if it doesn't exist
						IF NOT DIRECTORY(lcTargetDirectory) THEN
							MKDIR (lcTargetDirectory)
							WAIT WINDOW TIMEOUT 1 "Made Target Directory " + lcTargetDirectory
						ENDIF

						* if target directory exists, save there
						llSaveAgain = .F.
						IF DIRECTORY(lcTargetDirectory) THEN
							IF FILE(lcXLFileName) THEN
								DELETE FILE (lcXLFileName)
							ENDIF
							oWorkbook.SAVEAS(lcFiletoSaveAs)
							* set flag to save again after sheet is populated
							llSaveAgain = .T.
						ENDIF

						WAIT WINDOW NOWAIT "Building Temp Licensing spreadsheet..."

						lnRow = 1
						lnStartRow = lnRow + 1
						lcStartRow = ALLTRIM(STR(lnStartRow))

						oWorksheet = oWorkbook.Worksheets[1]
						oWorksheet.RANGE("A" + lcStartRow,"C10").clearcontents()

						oWorksheet.RANGE("A" + lcStartRow,"C10").FONT.SIZE = 11
						oWorksheet.RANGE("A" + lcStartRow,"C10").FONT.NAME = "Arial"
						oWorksheet.RANGE("A" + lcStartRow,"C10").FONT.bold = .F.
													
						* main scan/processing
						
						SELECT CUR_ACTIVE
						SCAN
							lnRow = lnRow + 1
							lcRow = LTRIM(STR(lnRow))
							oWorksheet.RANGE("A" + lcRow).VALUE = "'" + CUR_ACTIVE.AGENCYNUM
							oWorksheet.RANGE("B" + lcRow).VALUE = CUR_ACTIVE.AGENCYDESC
							oWorksheet.RANGE("C" + lcRow).VALUE = CUR_ACTIVE.HEADCOUNT
							*oWorksheet.RANGE("A" + lcRow,"D" + lcRow).Interior.ColorIndex = IIF(((lnRow % 2) = 1),15,0)
						ENDSCAN

*!*							lnRow = lnRow + 1
*!*							lcRow = LTRIM(STR(lnRow))

						oWorksheet.PageSetup.PrintArea = "$A$"  + lcStartRow + ":$C$" + lcRow

						* save again
						IF llSaveAgain THEN
							oWorkbook.SAVE()
						ENDIF
						oWorkbook.CLOSE()

						oExcel.QUIT()

						IF FILE(lcXLFileName) THEN
							.cAttach = lcXLFileName
							.TrackProgress('Created Temp Licensing Report File : ' + lcXLFileName, LOGIT+SENDIT)
							.TrackProgress('Kronos Temp Licensing Report process ended normally.', LOGIT+SENDIT+NOWAITIT)
						ELSE
							.TrackProgress('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!', LOGIT+SENDIT)
							.TrackProgress("ERROR: There was a problem creating Temp Licensing File : " + lcXLFileName, LOGIT+SENDIT)
							.TrackProgress('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!', LOGIT+SENDIT)
						ENDIF

					ENDIF  &&  .ExecSQL(...)
					
					.cBodyText = "The attached spreadsheet shows # of licenses in use." + CRLF + CRLF + ;
					"----------------------------------------------------------------------" + CRLF + ;
					"<process log follows>" + CRLF + ;
					"----------------------------------------------------------------------" + CRLF + ;
					.cBodyText

					CLOSE DATABASES ALL
				ELSE
					* connection error
					.TrackProgress('Unable to connect to WTK System.', LOGIT+SENDIT)
				ENDIF   &&  .nSQLHandle > 0
			CATCH TO loError

				.TrackProgress('There was an error.',LOGIT+SENDIT)
				.TrackProgress('The Error # is: ' + TRANSFORM(loError.ERRORNO), LOGIT+SENDIT)
				.TrackProgress('The Error Message is: ' + TRANSFORM(loError.MESSAGE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred in Program: ' + TRANSFORM(loError.PROCEDURE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred at line #: ' + TRANSFORM(loError.LINENO), LOGIT+SENDIT)
				lnNumberOfErrors = lnNumberOfErrors + 1
				IF TYPE('oExcel') = 'O' AND NOT ISNULL(oExcel) THEN
					oExcel.QUIT()
				ENDIF
				CLOSE DATA
				CLOSE ALL

			ENDTRY

			CLOSE DATA
			WAIT CLEAR
			***************** INTERNAL email results ******************************
			.TrackProgress('About to send status email.',LOGIT)
			.TrackProgress('==================================================================================================================', SENDIT)
			.TrackProgress('The logfile is: ' + .cLogFile, SENDIT)
			.TrackProgress('Kronos Temp Licensing Report process started: ' + .cStartTime, LOGIT+SENDIT)
			.TrackProgress('Kronos Temp Licensing Report process finished: ' + TTOC(DATETIME()), LOGIT+SENDIT)

			IF .lSendInternalEmailIsOn THEN
				* try to trap error from not having dartmail dll's registered on user's pc...
				TRY
					DO FORM dartmail2 WITH .cSendTo,.cFrom,.cSubject,.cCC,.cAttach,.cBodyText,"A"
					.TrackProgress('Sent status email.',LOGIT)
				CATCH TO loError
					*=MESSAGEBOX('There was an error sending the status email. Please contact Mark Bennett.',0+64,"KRONOS Temp Time")
					.TrackProgress('There was an error sending the status email.',LOGIT)
					.TrackProgress('The Error # is: ' + TRANSFORM(loError.ERRORNO), LOGIT)
					.TrackProgress('The Error Message is: ' + TRANSFORM(loError.MESSAGE), LOGIT)
					.TrackProgress('The Error occurred in Program: ' + TRANSFORM(loError.PROCEDURE), LOGIT)
					.TrackProgress('The Error occurred at line #: ' + TRANSFORM(loError.LINENO), LOGIT)
					lnNumberOfErrors = lnNumberOfErrors + 1
					CLOSE DATA
				ENDTRY

			ELSE
				.TrackProgress('Did not send status email: THIS.lSendInternalEmailIsOn = FALSE.',LOGIT)
			ENDIF

		ENDWITH
		RETURN
	ENDFUNC && main


	FUNCTION ExecSQL
		LPARAMETERS tcSQL, tcCursorName, tlNoDataReturnedIsOkay
		LOCAL llRetval, lnResult
		WITH THIS
			* close target cursor if it's open
			IF USED(tcCursorName)
				USE IN (tcCursorName)
			ENDIF
			WAIT WINDOW "Fetching remote data; please wait..." NOWAIT
			lnResult = SQLEXEC(.nSQLHandle, tcSQL, tcCursorName)
			llRetval = ( lnResult > 0 )
			IF llRetval THEN
				* see if any data came back
				IF NOT tlNoDataReturnedIsOkay THEN
					IF (NOT USED(tcCursorName)) OR EOF(tcCursorName) THEN
						llRetval = .F.
						.TrackProgress('No data was returned by this query: ' +CRLF + tcSQL, LOGIT+SENDIT)
					ENDIF
				ENDIF
			ELSE
				.TrackProgress("Result Code [" + TRANSFORM(lnResult) + "] returned by this query: " + tcSQL, LOGIT+SENDIT)
			ENDIF
			WAIT CLEAR
			RETURN llRetval
		ENDWITH
	ENDFUNC


	PROCEDURE TrackProgress
		* do any combination of Wait Window, writing to logfile, and adding to body of email,
		* based on nFlags parameter.
		LPARAMETERS tcExpression, tnFlags
		WITH THIS
			IF BITAND(tnFlags,LOGIT) = LOGIT THEN
				IF .lLoggingIsOn THEN
					?
					? .cProcessName + "  " + TTOC(DATETIME()) + ": " + tcExpression
				ENDIF
			ENDIF
			IF .lWaitWindowIsOn THEN
				IF BITAND(tnFlags,NOWAITIT) = NOWAITIT THEN
					WAIT WINDOW tcExpression NOWAIT
				ENDIF
				IF BITAND(tnFlags,WAITIT) = WAITIT THEN
					WAIT WINDOW tcExpression TIMEOUT .nWaitWindowTimeout
				ENDIF
			ENDIF
			IF BITAND(tnFlags,SENDIT) = SENDIT THEN
				IF .lSendInternalEmailIsOn THEN
					.cBodyText = .cBodyText + tcExpression + CRLF + CRLF
				ENDIF
			ENDIF
		ENDWITH
	ENDPROC  &&  TrackProgress

	
	
	PROCEDURE SetTempCo
		LPARAMETERS tcTempCo
		WITH THIS
			DO CASE
				CASE tcTempCo == 'ALL'
					.cTempCo = tcTempCo
					.cTempCoName = 'All Companies '
					.TrackProgress('Set TempCo = ' + tcTempCo,LOGIT+SENDIT)
					.TrackProgress('Set TempCoName = ' + .cTempCoName,LOGIT+SENDIT)
				CASE tcTempCo == 'A'
					.cTempCo = tcTempCo
					.cTempCoName = 'Tri-State '
					.TrackProgress('Set TempCo = ' + tcTempCo,LOGIT+SENDIT)
					.TrackProgress('Set TempCoName = ' + .cTempCoName,LOGIT+SENDIT)
				CASE tcTempCo == 'B'
					.cTempCo = tcTempCo
					.cTempCoName = 'Mira Loma '
					.TrackProgress('Set TempCo = ' + tcTempCo,LOGIT+SENDIT)
					.TrackProgress('Set TempCoName = ' + .cTempCoName,LOGIT+SENDIT)
				OTHERWISE
					.TrackProgress('INVALID TempCo = ' + tcTempCo,LOGIT+SENDIT)
					.cTempCoName = ''
			ENDCASE
		ENDWITH
	ENDPROC
	

ENDDEFINE
