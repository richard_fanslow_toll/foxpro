*!* SAMSUNG 943 CREATION PROGRAM
*Parameters cOffice

public lxfer943,cwhse,cfilename,tsendto,tcc,tsendtoerr,tccerr,tsendtotest,tcctest,liserror,norigacctnum,ndivloops,cinfolder,cwhse,nreclu,ltesting
public ldomail,tfrom,tattach,cxerrmsg,normalexit,nacctnum,acctname,lpick,lbrowfiles,ctransfer,carchivepath,lcpath,ccontainer,cacct_ref,cmod
public array a856(1)
public cfilename
set step on
cfilename ="UNK"
close databases all
coffice='Y'
ltesting = .f. && If TRUE, disables certain functions
loverridebusy = ltesting
loverridebusy = .t.
store "" to tmessage,tsubject,tsendto,tcc,tattach,tsendtoerr,tccerr,tfrom
do m:\dev\prg\_setvars with ltesting
on error throw
set step on

if ltesting
	wait window "This is a "+iif(ltesting,"TEST","PRODUCTION")+" upload" timeout 1
endif
use f:\3pl\data\mailmaster in 0 alias mm
try
	do m:\dev\prg\lookups
	tfrom = "Toll/FMI EDI Operations <toll-edi-ops@tollgroup.com>"
	tattach = " "
	coffice = iif(ltesting,"I",coffice)

	store iif(coffice = "Z","Y",coffice) to cmod

	ltestmail = ltesting
	lbrowfiles = ltesting
	liserror = .f.
	ldomail = .t.  && If .t., sends mail
	normalexit = .f.
	lpick = .f.
	ndivloops = 0
	nreclu = 0
	cmastercomments = ""
	ccontainer = ""
	creference = ""

	xacct_ref = ""
*  nAcctNum  = Icase(cDivision = "16",6651,cDivision = "8",6650,cDivision = "22",6652,6649)
	cxerrmsg = ""
	cwo_num = ""
	ccustname = "SAMSUNG"  && Customer Identifier (for folders, etc.)
	cmailname = "SAMSUNG"  && Proper name for eMail, etc.

	ctransfer = "943-SAMSUNG "


	select 0
	use f:\edirouting\ftpsetup
	locate for ftpsetup.transfer = ctransfer
	IF !lTesting
	if ftpsetup.chkbusy and !loverridebusy
		wait window "Process is busy...exiting" timeout 2
		close data all
		normalexit = .t.
		throw
	else
		carchivepath = alltrim(ftpsetup.archive)
		lcpath = alltrim(ftpsetup.localpath)
	endif

	replace chkbusy WITH .T.,trig_time WITH DATETIME() for transfer = ctransfer
	use in ftpsetup
	endif

	setuprcvmail(coffice)

*!*		If !lTesting
*!*			Delete File (lcPath+'*.*)
*!*		Endif

	if ltesting
		len1 = adir(ary1,"f:\ftpusers\samsung\943in\*.*")
	else
		len1 = adir(ary1,"f:\ftpusers\samsung\943in\*.*")
	endif

	if len1 = 0
		wait window "There are no SAMSUNG 943 files to process...exiting" timeout 2
		close databases all
		normalexit = .t.
		throw
	endif

	wait window "There are "+transform(len1)+" SAMSUNG 943 files to process" timeout 2
	m.filedate = date()

	for kk = 1 to len1
		ccontainer = ""
		cacct_ref = ""
		cfilename = alltrim(ary1[kk,1])
		carchivefile  = (carchivepath+cfilename)
		dfiledate = ary1[kk,3]
		xfile = lcpath+cfilename

		do m:\dev\prg\createx856a
		loadedifile(xfile,"*","NOTILDE",ccustname)

		select x856
		locate
		locate for x856.segment = "GS" and x856.f1 = "AR"
		if !found()
			cxerrmsg = cfilename+" is NOT a 943 document"
			errormail()
		endif

		select x856
*    Wait Window "This is a "+cWhse+" inbound file" Timeout 1
		nacctnum=6650

		cusefolder='f:\why\whdata\'

		if ltesting
			cusefolder = "F:\WHP\WHDATA\"
		else
			xreturn = "XXX"
			do m:\dev\prg\wf_alt with coffice,nacctnum
			if xreturn = "XXX"
				cxerrmsg = "NO WHSE FOUND IN WMS"
				errormail()
				return
			endif
			cusefolder = upper(xreturn)
		endif
		if used('inrcv')
			use in inrcv
		endif

		useca("inrcv","wh")  && an updateable cursor with all existing records
****added 1/13/2017 TMARG SQL
		goffice=coffice
		useca("inwolog","wh",,,"mod=' "+goffice+"' and  accountid ="+transform(nacctnum) )

*!*			IF USED('inwolog')
*!*				USE IN inwolog
*!*			ENDIF
*    USE (cUseFolder+"INRCV") IN 0 ALIAS inrcv
*!*			USE (cUseFolder+"INWOLOG") IN 0 ALIAS inwolog
		if used('pl')
			use in pl
		endif

		useca("pl","wh",,,"mod=' "+goffice+"' and  accountid ="+transform(nacctnum) )
		index on plid tag plid
		set order to

		xsqlexec("select * from account where inactive=0","account",,"qq")
		index on accountid tag accountid
		set order to

		select * from inwolog where .f. into cursor xinwolog readwrite
		select * from pl where .f. into cursor xpl readwrite


		select xinwolog
		scatter memvar blank
		=seek(nacctnum,'account','accountid')
		m.acctname = account.acctname
		m.accountid = account.accountid

		store 0 to m.inwologid,m.plid,npo
		store "" to m.echo,m.po
		store "FMI-PROC" to m.addby
		store datetime() to m.adddt
		store "SAMSUNG943" to m.addproc
		store date() to m.wo_date,m.date_rcvd
		prepackind=''
		select x856
		locate
		m.inwologid = 0
		m.plid = 0
		poctr=0
		scan
			do case
			case inlist(x856.segment,"ISA","GS","ST")
				loop

			case x856.segment = "W06"
				lluploaderrorflag = .f.
				m.inwologid = m.inwologid + 1
				m.container= alltrim(x856.f7)
				m.acct_ref = alltrim(x856.f2)  && Shipment ID number
				store m.acct_ref  to m.ship_num
				store m.acct_ref  to cacct_ref

			case x856.segment = "N1" and x856.f1 = "DV"
				do case
				case alltrim(x856.f4) ="16"
					m.accountid = 6651
					m.acctname = "Q4 KIDS "
				case alltrim(x856.f4) ="8"
					m.accountid = 6650
					m.acctname = "Q4 DESIGN"
				case alltrim(x856.f4) ="22"
					m.accountid = 6652
					m.acctname = "TAYLOR FASHION"
				otherwise
					m.accountid= 6649
					m.acctname = "SAMSUNG C&T AMERICA INC"
				endcase
			case x856.segment = "N1" and x856.f1 = "VN"
				m.seal = alltrim(x856.f2)

			case x856.segment = "G62" and x856.f1 = "89"
				m.comments = "FIRST_ARVL_DT*"+alltrim(x856.f2)

			case x856.segment = "N9" and x856.f1 = "DV"
				m.comments = m.comments+chr(13)+"FILE943*"+cfilename
				m.comments = m.comments+chr(13)+"FILETIME*"+ttoc(datetime())
				m.comments = m.comments+chr(13)+"DIVERSION_AUTH*"+alltrim(x856.f2)

				insert into xinwolog from memvar
				m.comments =""

			case x856.segment = "W04"
				poctr=poctr+1
				m.plid = m.plid + 1
				m.totqty = int(val(x856.f1))
				replace xinwolog.plinqty with xinwolog.plinqty+m.totqty in xinwolog
				m.units = .t.
				m.echo = "PACKAGE_TYPE*"+alltrim(x856.f2)
				m.echo = m.echo+chr(13)+ "UPC*"+allt(x856.f5)
				m.style = iif(x856.f6 = "ST",allt(x856.f7),"")
				if  x856.segment = "W04" and alltrim(x856.f2) ='CA'
					prepackind='-PREPK'
				else
					prepackind=''
				endif
			case x856.segment = "G69"
				m.echo = m.echo+chr(13)+ "STYLEDESC*"+allt(x856.f1)
			case x856.segment = "N9" and x856.f1 = "CL"
				m.color=allt(x856.f2)
*!*					IF prepackind='-PREPK'
*!*					m.color=''
*!*					ELSE
*!*					endif
				m.echo = m.echo+chr(13)+ "COLOR*"+allt(x856.f2)
			case x856.segment = "N9" and x856.f1 = "SZ"
				m.id=allt(x856.f2)&&&+(prepackind)
			case x856.segment = "N9" and x856.f1 = "CO"
				m.echo = m.echo+chr(13)+ "CUSTORDER*"+allt(x856.f2)
			case x856.segment = "N9" and x856.f1 = "LT"
				m.echo = m.echo+chr(13)+ "LOTNUMBER*"+allt(x856.f2)
				insert into xpl from memvar
				replace xinwolog.plunitsinqty with xinwolog.plinqty in xinwolog
				if prepackind !='-PREPK'
					replace xpl.units with  .f. in xpl
					replace xpl.pack with alltrim(str(m.totqty)) in xpl
					replace xpl.po with alltrim(str(poctr)) in xpl
					replace xpl.totqty with 1
					insert into xpl from memvar
					replace xpl.units with  .t. in xpl
					replace xpl.pack with '1' in xpl
					replace xpl.po with alltrim(str(poctr)) in xpl
				else
					replace xpl.units with  .f. in xpl
					replace xpl.pack with '1' in xpl
					replace xpl.po with alltrim(str(poctr)) in xpl
					insert into xpl from memvar
					replace xpl.units with  .t. in xpl
					replace xpl.pack with '1' in xpl
					replace xpl.po with alltrim(str(poctr)) in xpl
				endif
				m.echo=""
			endcase
		endscan

*    xplrollup()

*!*      If lTesting
*!*        Select xinwolog
*!*        Delete For plinqty = 0
*!*        Locate
*!*        Browse
*!*        Select xpl
*!*        Delete For totqty = 0
*!*        Locate
*!*        Browse
*!*      Endif

**********************************************start dupe check here
		set step on
		select * from xinwolog into cursor testwos
		nuploadcount = 0
		select testwos
		locate

		scan
			niwrec = testwos.inwologid
			wait window "AT TESTWOS RECORD "+alltrim(str(niwrec)) nowait
			xacct_num =  testwos.accountid
			xacct_ref = alltrim(testwos.acct_ref)
			select inwolog
			locate
			locate for inwolog.accountid = xacct_num and alltrim(inwolog.acct_ref) = alltrim(xacct_ref)
			if found()
				cwo = transform(inwolog.wo_num)
				select xinwolog
				locate
				locate for alltrim(xinwolog.acct_ref) = alltrim(xacct_ref)
				if found()
					wait window "This Acct. Ref., "+xacct_ref+", already exists in the INWOLOG table...skipping" timeout 1
					ninwologid = xinwolog.inwologid
					delete next 1 in xinwolog
					select xpl
					delete for xpl.inwologid = ninwologid
					set step on
					dupemail(cwo,xacct_ref)
				else
					wait window "Acct. Ref "+xacct_ref+" not found in INWOLOG!" timeout 20
				endif
			endif
			select testwos
		endscan
**********************************************end  dupe check here

		nuploadcount = 0

		select xinwolog
		locate
		wait window "Currently in "+upper(iif(ltesting,"test","production"))+" mode, base folder: "+cusefolder timeout 2

		scan  for !deleted() && Scanning xinwolog here
			wologid = xinwolog.inwologid
			scatter memvar memo
			m.inwologid= dygenpk("INWOLOG","why")
			newinwologid = m.inwologid
			m.wo_num  = dygenpk("WONUM","why")
			newwonum = m.wo_num

			cplctns = alltrim(str(m.plinqty))
			cplunits = alltrim(str(m.plunitsinqty))
			set step on
****added 1/31/2017 TMARG code for SQL inwolog
			m.mod=coffice
			m.office=coffice
			insertinto ('inwolog','wh',.t.) 
*			nUploadCount = nUploadCount+1
			select xpl
			scan for xpl.inwologid = xinwolog.inwologid
				select xpl
				scatter memvar
				m.plid= dygenpk("pl","why")
				m.echo=xpl.echo
				m.inwologid = newinwologid
				m.wo_num= newwonum
				insertinto ('pl','wh',.t.) 
				m.echo=""
			endscan
			tu('pl')
			tu('inwolog')
		endscan

		tu('pl')
		tu('inwolog')

		goodmail()

		if nuploadcount = 0
			set step on

			if file(lcpath+cfilename)
				copy file(lcpath+cfilename) to [&cArchivefile]
				if file(carchivefile) and !ltesting
					set step on
					delete file (lcpath+cfilename)
				endif
			endif
*			Exit
		endif
	endfor

	wait window "All Samsung 943s processed...exiting" timeout 2

	closeftpsetup()

	normalexit = .t.

catch to oerr

	if normalexit = .f.
		assert .f. message "AT CATCH SECTION"
		set step on
		tsubject = ccustname+" 943 Upload Error ("+transform(oerr.errorno)+") at "+ttoc(datetime())
		tattach  = ""
		tsendto  = tsendtoerr
		tcc      = tccerr
		tmessage = ccustname+" 943 Upload Error"
		lcsourcemachine = sys(0)
		lcsourceprogram = sys(16)

		if empty(cxerrmsg)
			tmessage =tmessage+ "Try/CATCH Exeception Message:"+chr(13)+;
				[  Error: ] + str(oerr.errorno) +chr(13)+;
				[  Procedure: ] + oerr.procedure +chr(13)+;
				[  LineNo: ] + str(oerr.lineno) +chr(13)+;
				[  Message: ] + oerr.message +chr(13)+;
				[  Details: ] + oerr.details +chr(13)+;
				[  StackLevel: ] + str(oerr.stacklevel) +chr(13)+;
				[  LineContents: ] + oerr.linecontents+chr(13)+;
				[  UserValue: ] + oerr.uservalue+chr(13)+;
				[  Computer:  ] +lcsourcemachine+chr(13)+;
				[  943 file:  ] +cfilename+chr(13)+;
				[  Program:   ] +lcsourceprogram
		else
			tmessage = tmessage + chr(13) + chr(13) + cxerrmsg
		endif
		tattach  = ""
		tfrom    ="FMI EDI Processing Center <fmi-transload-ops@fmiint.com>"
		do form m:\dev\frm\dartmail2 with tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
	endif
finally
	set status bar on
*	Delete File (lcPath+'*.*')
	close databases all
	on error
endtry


****************************
procedure goodmail
****************************
*!*  tsubject = cMailName+" EDI FILE (943) Processed, Inv. WO"+cWO_Num
*!*  tmessage = Replicate("-",90)+Chr(13)
*!*  tmessage = tmessage+"Packinglist upload run from machine: "+Sys(0)+Chr(13)
*!*  tmessage = tmessage+Replicate("-",90)+Chr(13)
*!*  tmessage = tmessage+"Inbound Workorders created for "+Alltrim(m.Acctname)+", Loc: "+cWhse+Chr(13)
*!*  tmessage = tmessage+Iif(!Empty(cContainer),"Container: "+cContainer+Chr(13),"AWB :"+cReference)+Chr(13)
*!*  tmessage = tmessage+"Data from File : "+cFilename+Chr(13)
*!*  tmessage = tmessage+Replicate("-",90)+Chr(13)
*!*  tmessage = tmessage+"Acct. Ref: "+cAcct_ref+", WO# "+cWO_Num+Space(2)+"CTNS: "+cPLCtns+Space(2)+"UNITS: "+cPLUnits

*!*  If lDoMail
*!*    Do Form m:\dev\frm\dartmail2 With tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
*!*  Endif
endproc

****************************
procedure dupemail
****************************
parameters cwo,xacct_ref
select mm
set step on
locate for taskname = "SSDUPE" and office = "Y" and group='SAMSUNG'
tsubject = ccustname+" 943 Duplicate Upload  at "+ttoc(datetime())
tsendto = mm.sendto
tcc     = mm.cc
tattach  = ""
set step on
tsubject = cmailname+" EDI FILE (943): Duplicate"
tmessage = replicate("-",90)+chr(13)
tmessage = tmessage+"Data from File : "+cfilename+chr(13)
tmessage = tmessage+"For Account ID: "+transform(nacctnum)
tmessage = tmessage+ ", Container "+ccontainer
tmessage = tmessage+ ", Acct. Reference "+cacct_ref+chr(13)
tmessage = tmessage+"already exists in the Inbound WO table, in Work Order "+cwo
tmessage = tmessage+chr(13)+replicate("-",90)+chr(13)

if ltesting
	set step on
endif

if ldomail
	do form m:\dev\frm\dartmail2 with tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
endif
endproc

****************************
procedure errormail
****************************
set step on
tsubject = cmailname+" EDI ERROR (943)"
tsendto = tsendtoerr
tcc = tccerr
tmessage = "File: "+cfilename
tmessage = tmessage+chr(13)+cxerrmsg

do form m:\dev\frm\dartmail2 with tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
normalexit = .t.
set step on
throw
endproc

****************************
procedure setuprcvmail
****************************
parameters coffice

select mm
locate for mm.edi_type = "943" and mm.group = "SAMSUNG" and mm.office = coffice
if !found()
	set step on
	cxerrmsg = "Office/Loc/Acct not in Mailmaster!"
	errormail()
endif
store trim(mm.basepath) to lcpath
store trim(mm.archpath) to carchivepath
lusealt = mm.use_alt
store iif(lusealt,trim(mm.sendtoalt),trim(mm.sendto)) to tsendto
store iif(lusealt,trim(mm.ccalt),trim(mm.cc)) to tcc
locate
locate for mm.edi_type = "MISC" and mm.taskname = "GENERAL"
lusealt = mm.use_alt
store iif(lusealt,trim(mm.sendtoalt),trim(mm.sendto)) to tsendtotest
store iif(lusealt,trim(mm.ccalt),trim(mm.cc)) to tcctest
store iif(lusealt,trim(mm.sendtoalt),trim(mm.sendto)) to tsendtoerr
store iif(lusealt,trim(mm.ccalt),trim(mm.cc)) to tccerr
if ltesting or ltestmail
	tsendto = tsendtotest
	tcc = tcctest
endif
endproc

***************************************
procedure closeftpsetup
***************************************
select 0
use f:\edirouting\ftpsetup
replace chkbusy with .f. for transfer = ctransfer
use in ftpsetup
endproc

***************************************
procedure assignofficelocs
***************************************
coffice=iif(inlist(coffice,"1","2","7"),"C",coffice)
cwhseloc = icase(coffice = "M","FL",coffice = "I","NJ",coffice = "X","CR","CA")
cwhse = icase(coffice = "M","MIAMI",coffice = "I","CARTERET",coffice = "X","CARSON","SAN PEDRO")
endproc

*!*	***************************************
*!*	Procedure xplrollup  && Added 02.20.2014, Joe, per Chris M.
*!*	***************************************
*!*	Set Step On
*!*	Select xpl
*!*	Select inwologid,Style,Count(Style) As cnt1 From xpl Group By 1,2 Into Cursor tempxpl
*!*	Select tempxpl
*!*	Locate
*!*	If lTesting
*!*		Browse For cnt1>1
*!*	Endif
*!*	Locate
*!*	Scan For tempxpl.cnt1>1
*!*		cStyle1 = Alltrim(tempxpl.Style)
*!*		nIWID = tempxpl.inwologid
*!*		Select xpl
*!*		Locate For inwologid = nIWID And Style = cStyle1
*!*		nrec1 = Recno()
*!*		Replace xpl.Echo With xpl.Echo+Chr(13)+"QTY*"+Allt(Str(xpl.totqty)) In xpl
*!*		Scan For !Deleted() And inwologid = nIWID And Style = cStyle1 And Recno()>nrec1
*!*			nrec2 = Recno()
*!*			Scatter Fields Echo Memo Memvar
*!*			nTotqty = xpl.totqty
*!*			Go nrec1
*!*			If !"MULTILINES"$xpl.Echo
*!*				Replace xpl.Echo With "MULTILINES"+Chr(13)+xpl.Echo In xpl
*!*			Endif
*!*			Replace xpl.Echo With xpl.Echo+Chr(13)+m.echo In xpl
*!*			Replace xpl.Echo With xpl.Echo+Chr(13)+"QTY*"+Allt(Str(nTotqty)) In xpl
*!*			Replace xpl.totqty With xpl.totqty+nTotqty In xpl
*!*			Go nrec2
*!*			Delete Next 1 In xpl
*!*		Endscan
*!*		Store "" To m.echo
*!*	Endscan
*!*	Endproc
