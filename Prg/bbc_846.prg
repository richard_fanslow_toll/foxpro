*!* BBC 846 CREATOR

*Parameters cOffice

cOffice ="L"
gOffice = "C"


utilsetup("BBC_846")
Set Exclusive Off
Set Deleted On
Set Talk Off
Set Safety Off
*!*  WITH _SCREEN
*!*    .AUTOCENTER = .T.
*!*    .WINDOWSTATE = 0
*!*    .BORDERSTYLE = 1
*!*    .WIDTH = 320
*!*    .HEIGHT = 210
*!*    .TOP = 290
*!*    .LEFT = 110
*!*    .CLOSABLE = .T.
*!*    .MAXBUTTON = .t.
*!*    .MINBUTTON = .T.
*!*    .SCROLLBARS = 0
*!*    .CAPTION = "BBC_846"
*!*  ENDWITH


Public cWhse,cFilename,cFilename2,tsendto,tcc,tsendtoerr,tccerr,tsendtotest,tcctest,lIsError,lJoeMail
Public leMail,tfrom,lDoCatch
Close Data All
lTesting = .F. && If TRUE, disables certain functions
lUsePL = .F.
lIsError = .F.
On Error Throw
nAcctNum = 6757
cOffice = "L"
lDoCatch  = .T.
cErrMsg = ""
gOffice=cOffice
If lTesting
  cIntUsage = "T"
Else
  cIntUsage = "P"
Endif

Do m:\dev\prg\_setvars With .T.

tfrom = "TGF Warehouse Operations <warehouse-ops@fmiint.com>"
*Set STEP ON
Try

  cOfficeUsed = cOffice
  xReturn = "XXX"
  Do m:\dev\prg\wf_alt With cOffice,nAcctNum
  If xReturn = "XXX"
    Wait Window "No Bldg. # Provided...closing" Timeout 3
    Return
  Endif

  If lTesting
    cUseFolder = "f:\whp\"
  Else
    cUseFolder = Upper(xReturn)
  Endif



  Dimension thisarray(1)
  cSuffix = ""
  cWhse = "MIRALOMA"

  lOverrideXcheck = .T. && If true, doesn't check ASN carton count against barcode scans.
  lHoldFile = .T. && If true, keeps output file in 846HOLD folder, otherwise into OUT
  leMail = .T.  && If .t., sends mail to WAREHOUSES

  cBarMiss = ""
  cASN = ""
  cUPC = ""
  nSTSets = 0
  cSpecChar = .F.
  XFERUPDATE = .T.
  nFilenum = 0
  lBackup = .F.
  lOverage = .F.
  lJoeMail = .T.
  lCopyCA = .T.
  cErrMsg = ""
  lIsError = .T.
  lXfer944 = .F.
  cTrk_WO = ""
  nCTNTotal = 0

*!* SET CUSTOMER CONSTANTS
  cZLAddress = ""
  cZLCity = ""
  cZLState = ""
  cZLZip = ""

  Do Case
  Case nAcctNum = 6757
    cCOName = "BBCRW"
    cCustPrefix = 'RW'

  Case nAcctNum = 6761
    cCOName = "BBC"
    cCustPrefix = 'BB'

  Case nAcctNum = 6763
    cCOName = "BBCCF"
    cCustPrefix = 'CF'

  Case nAcctNum = 6771
    cCOName = "BBCCFB"
    cCustPrefix = 'CF'

  Case nAcctNum = 6781
    cCOName = "BBCB"
    cCustPrefix = 'BB'

  Case nAcctNum = 6783
    cCOName = "BBCIN"
    cCustPrefix = 'BB'

  Case nAcctNum = 6799
    cCOName = "BBCCFN"
    cCustPrefix = 'CF'
  Endcase
  Store cCustPrefix To cCustName, cMailName  && Customer Identifier (for folders, etc.) and Proper name for eMail, etc.

  cX12 = "005010"  && X12 Standards Set used
  crecid = "BBCTOLLPRD"  && Recipient EDI address

  dt1 = Ttoc(Datetime(),1)
  dt2 = Datetime()
  dtmail = Ttoc(Datetime())

  cFilestem = Upper(cCustPrefix)+dt1+".846"
  cFilename = "f:\ftpusers\bbcrw\846hold\"+Upper(cFilestem) && Holds file here until complete and ready to send

  cFilenameOUT = "f:\ftpusers\bbcrw\846out\"+Upper(cFilestem)
  cArchiveFile = "F:\FTPUSERS\BBCRW\846OUT\ARCHIVE\"+Upper(cFilestem)

  Store "" To c_CntrlNum,c_GrpCntrlNum
  nFilenum = Fcreate(Upper(cFilename))

*!* SET OTHER CONSTANTS
  nCtnNumber = 1
  cString = ""
  csendqual = "ZZ"
  csendid = "TGFUSA"
  crecqual = "ZZ"
  cfd = "|"  && Field/element delimiter
  csegd = "" && Line/segment delimiter
  nSegCtr = 0
  cterminator = ">" && Used at end of ISA segment
  cdate = Dtos(Date())
  ctruncdate = Right(cdate,6)
  ctime = Left(Time(Date()),2)+Substr(Time(Date()),4,2)
  cfiledate = cdate+ctime
  cOrig = "J"
  cdate = Dtos(Date())
  ctruncdate = Right(cdate,6)
  ctime = Left(Time(Date()),2)+Substr(Time(Date()),4,2)

** PAD ID Codes
  csendidlong = Padr(csendid,15," ")
  crecidlong = Padr(crecid,15," ")

*!* Serial number incrementing table
  If Used("serfile")
    Use In serfile
  Endif

  Use ("F:\3pl\data\serial\bbc_846_serial.dbf") In 0 Alias serfile

*close data all

  xsqlexec("select * from upcmast where  accountid in (6757)",,,"wh")
  xsqlexec("select * from inven where  accountid in (6757)",,,"wh")

**Use f:\whl\whdata\inven In 0

  Select Style,Color,Id,Space(13) As upc,Space(20) As brand, 00000 As wipqty, totqty From inven Where accountid = 6757 And units Into Cursor tinven Readwrite
*!*    SELECT tinven
*!*    SCAN
*!*      IF '-' $(tinven.STYLE)
*!*        pos1=ATC('-', tinven.STYLE)
*!*        REPLACE  tinven.STYLE WITH  SUBSTR(tinven.STYLE,1,pos1-1)
*!*      ENDIF
*!*    ENDSCAN

  Select tinven
  Scan
    Select upcmast
    If '-' $(tinven.Style)
      pos1=Atc('-', tinven.Style)
      lcStyle = Substr(tinven.Style,1,pos1-1)
      Locate For Style= lcStyle And Color= tinven.Color And Id= tinven.Id
      If Found()
        Replace tinven.upc With upcmast.upc
        Replace tinven.brand With getmemodata("info","BRANDDESC")
      Endif
    Else
      Locate For Style= tinven.Style And Color= tinven.Color And Id= tinven.Id
      If Found()
        Replace tinven.upc With upcmast.upc
        Replace tinven.brand With getmemodata("info","BRANDDESC")
      Else

      Endif
    Endif
  Endscan

  Select tinven
  Delete For totqty =0

  Set Step On
  xsqlexec("select * from outship where mod='"+gOffice+"' and accountid=6757 " + ;
    "and del_date={} and notonwip=0 and qty#0 order by wo_num","inprocess",,"wh")

  xsqlexec("select units, style, color, id, pack, totqty from outship, outdet " + ;
    "where outship.outshipid=outdet.outshipid " + ;
    "and outship.mod='"+gOffice+"' and outship.accountid=6757 " + ;
    "and del_date={} and outdet.units=.t. and notonwip=0 and qty#0","xdy",,"wh")

  Select a.*,upc From xdy a Left Join upcmast b On a.Style=b.Style And a.Color=b.Color And a.Id=b.Id Into Cursor xdy2 Readwrite

  Select units, Style, Color, Id, Pack, Sum(totqty) As pickedqty, upc ;
    from xdy2 Group By units, Style, Color, Id, Pack, upc Into Cursor xunshipped readwrite

  Select xunshipped
  Scan For Isnull(upc)
    Select upcmast
      If '-' $(xunshipped.Style)
        pos1=Atc('-', xunshipped.Style)
        lcStyle = Substr(xunshipped.Style,1,pos1-1)
        Locate For Style= lcStyle And Color= xunshipped.Color And Id= xunshipped.Id
        If Found()
          Replace xunshipped.upc With upcmast.upc In xunshipped
        Endif
    Endif 
  Endscan 

  Select * From xunshipped Into Cursor xunshipped Order By upc readwrite

set step on

  Select Sum(pickedqty) As wipqty,Style,Color,Id,upc,00 as addme  From xunshipped Group By Style,Color,Id Into Cursor xxtemp Readwrite
*!*    Select xxtemp
*!*    Scan For Isnull(upc)
*!*      Select upcmast
*!*        If '-' $(xxtemp.Style)
*!*          pos1=Atc('-', xxtemp.Style)
*!*          lcStyle = Substr(xxtemp.Style,1,pos1-1)
*!*          Locate For Style= lcStyle And Color= xxtemp.Color And Id= xxtemp.Id
*!*          If Found()
*!*            Replace xxtemp.upc With upcmast.upc In xxtemp
*!*          Endif
*!*      Endif 
*!*    endscan

  Set Step On

  Select xxtemp
  Scan
    Select tinven
    Locate For Alltrim(style) = Alltrim(xxtemp.style) and Alltrim(color) = Alltrim(xxtemp.color) and Alltrim(id) = Alltrim(xxtemp.id)
    If Found()
      Replace tinven.wipqty With xxtemp.wipqty In tinven
    Else
      replace addme With 1 In xxtemp
    Endif
  Endscan

Select xxtemp
Scan For addme = 1
  Select xxtemp
  scatter memvar
  select tinven
 Insert Into tinven From memvar
Endscan

Select upc,wipqty,style From tinven Where "-"$style Into Cursor finishwip

*!*  Select finishwip
*!*  Scan
*!*    Select tinven
*!*    Locate For upc=finishwip.upc And Alltrim(style)==Alltrim(finishwip.style)
*!*    If Found()
*!*      replace tinven.wipqty With tinven.wipqty + finishwip.wipqty In tinven
*!*    endif  
*!*  endscan

Select * From tinven into Cursor inven Order By upc

set step On 

*Endscan


*!*  xsqlexec("select * from inven where mod='L' and accountid = 6718 ",,,"wh")

*!*  xsqlexec("select * from upcmast where  accountid in (6718,6719,6720)",,,"wh")

*!*  SELECT style,color,id,upc,info,SPACE(5) as brand FROM upcmast INTO CURSOR vupcmast READWRITE

*!*  replace all BRAND with getmemodata("info","BRANDDESC")


*!*    Select wip_items
*!*    Scan
*!*      Select items
*!*      Locate For Alltrim(items.upc) = Alltrim(wip_items.upc)
*!*      If Found()
*!*        Replace items.totqty With items.totqty + wip_items.total_units
*!*        Replace items.wipqty With wip_items.total_units
*!*      Else
*!*        Insert Into items (upc,totqty) Values (wip_items.upc,wip_items.total_units)
*!*        Replace items.wipqty With wip_items.total_units
*!*      Endif
*!*    Endscan


*!*    Set Step On
*!*    Select upc,Sum(totqty) As newqty From items Into Cursor newitems Group By upc





nSECount = 1

Do num_incr
Store "ISA"+cfd+"00"+cfd+"          "+cfd+"00"+cfd+"          "+cfd+csendqual+cfd+csendidlong+cfd+;
  crecqual+cfd+crecidlong+cfd+ctruncdate+cfd+ctime+cfd+"U"+cfd+"00501"+cfd+;
  PADL(c_CntrlNum,9,"0")+cfd+"0"+cfd+cIntUsage+cfd+cterminator+csegd To cString
Do cstringbreak

Store "GS"+cfd+"IB"+cfd+csendid+cfd+crecid+cfd+cdate+cfd+ctime+cfd+c_CntrlNum+;
  cfd+"X"+cfd+cX12+csegd To cString
Do cstringbreak

nSegCtr = 0
nISACount = 1

If nSECount = 1
  Store "ST"+cfd+"846"+cfd+Padl(c_GrpCntrlNum,9,"0")+csegd To cString
  Do cstringbreak
  nSegCtr = nSegCtr + 1

  Store "BIA"+cfd+"08"+cfd+"MM"+cfd+"FMM"+cfd+cdate+csegd To cString
  Do cstringbreak
  nSegCtr = nSegCtr + 1
  nSECount = 0
Endif

Select tinven
lineCtr = 1

Scan &&FOR indet.inwologid = inwolog.inwologid AND units
*    STORE "LX"+cfd+ALLTRIM(TRANSFORM(LineCtr))+csegd TO cString
*    DO cstringbreak
*    nSegCtr = nSegCtr + 1


  If "-"$tinven.Style
    pos1=Atc('-', tinven.Style)
    lcFinishCode = Substr(tinven.Style,pos1+1)
    Store "LIN"+cfd+cfd+"UP"+cfd+Alltrim(tinven.upc)+cfd+cfd+cfd+"BN"+cfd+lcFinishCode+csegd To cString
    Do cstringbreak
    nSegCtr = nSegCtr + 1
  Else
    Store "LIN"+cfd+cfd+"UP"+cfd+Alltrim(tinven.upc)+cfd+cfd+cfd+"BN"+csegd To cString
    Do cstringbreak
    nSegCtr = nSegCtr + 1
  Endif

  Store "QTY"+cfd+"17"+cfd+Alltrim(Transform(tinven.totqty+tinven.wipqty))+csegd To cString
  Do cstringbreak
  nSegCtr = nSegCtr + 1


*!*      If tinven.wipqty > 0
*!*        STORE "QTY"+cfd+"37"+cfd+ALLTRIM(TRANSFORM(tinven.wipqty))+csegd TO cString
*!*        DO cstringbreak
*!*        nSegCtr = nSegCtr + 1
*!*      Endif

  lineCtr = lineCtr +1

Endscan

Wait Window "END OF SCAN/CHECK PHASE ROUND..." Nowait

nSTCount = 0

Store "CTT"+cfd+Alltrim(Transform(lineCtr))+csegd To cString
Do cstringbreak
nSegCtr = nSegCtr + 1

Store  "SE"+cfd+Alltrim(Str(nSegCtr+1))+cfd+Padl(c_GrpCntrlNum,9,"0")+csegd To cString
Do cstringbreak
nSegCtr = 0
nSTSets = nSTSets + 1

Store  "GE"+cfd+Alltrim(Str(nSTSets))+cfd+c_CntrlNum+csegd To cString
Do cstringbreak

Store  "IEA"+cfd+"1"+cfd+Padl(c_CntrlNum,9,"0")+csegd To cString
Do cstringbreak

=Fclose(nFilenum)


Set Step On
If !Used("ftpedilog")
  Select 0
  Use F:\edirouting\ftpedilog Alias ftpedilog
  Insert Into ftpedilog (ftpdate,filename,acct_name,Type) Values (dt2,cFilename,"cCustname","944")
  Use In ftpedilog
Endif

Wait "846 Creation process complete:"+Chr(13)+cFilename Window At 45,60 Timeout 3
cFin = "846 FILE "+cFilenameOUT+" CREATED WITHOUT ERRORS"

Set Step On

oFSO=Createobject("Scripting.FileSystemObject")
oFSO.CopyFile("&cFilename","&cFilenameOUT",.T.)

Release oFSO

oFSO=Createobject("Scripting.FileSystemObject")
oFSO.CopyFile("&cFilename","&cArchiveFile",.T.)

Set Step On
* Copy File &cFilename To &cFilenameOUT
*  COPY FILE &cFilename TO &cArchiveFile

Erase &cFilename

lDoCatch = .F.

Catch To oErr
  If lDoCatch And Empty(Alltrim(cErrMsg))
    Wait Window "At error catch block" Timeout 1
    Set Step On
    tsubject = "846 Error in "+cCustName
    tattach = " "
    tsendto = tsendtoerr
    tcc = tccerr

    tmessage = cCustName+" Error processing "+Chr(13)
    tmessage =tmessage+ "Try/CATCH Exeception Message:"+Chr(13)+;
      [  Error: ] + Str(oErr.ErrorNo) +Chr(13)+;
      [  Procedure: ] + oErr.Procedure +Chr(13)+;
      [  LineNo: ] + Str(oErr.Lineno) +Chr(13)+;
      [  Message: ] + oErr.Message +Chr(13)+;
      [  Details: ] + oErr.Details +Chr(13)+;
      [  StackLevel: ] + Str(oErr.StackLevel) +Chr(13)+;
      [  LineContents: ] + oErr.LineContents+Chr(13)+;
      [  UserValue: ] + oErr.UserValue
  Endif
Endtry

************************************************************************
*!* END OF MAIN PROGRAM
************************************************************************

****************************
Procedure close846
****************************
** Footer Creation
Return
****************************
Procedure cstringbreak
****************************
Fputs(nFilenum,cString)
*Fwrite(nFilenum,cString)
Return

****************************
Procedure num_incr
****************************
Select serfile
Replace serfile.seqnum With serfile.seqnum + 1 In serfile
Replace serfile.grpseqnum With serfile.grpseqnum + 1 In serfile
c_CntrlNum = Alltrim(Str(serfile.seqnum))
c_GrpCntrlNum = Alltrim(Str(serfile.grpseqnum))
Return

************************************************************************************************

schedupdate()
