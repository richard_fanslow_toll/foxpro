Parameters coffice,acctnum
* call this like this: DO m:\dev\prg\BILLABONG_943.prg WITH "C", 6718

acctnum=6718
if empty(coffice)&& or empty(acctnum)
	coffice = "L"
endif

edi_type = "PL"
upcerror=.F.

*runack("WMSINBOUND")
ltesting = .F.
Do m:\dev\prg\_setvars With ltesting

If Vartype(acctnum) = "C"
	acctnum = Int(Val(acctnum))
Endif

Public goffice,tsendto,tcc,lcaccountname,lcartonacct,ldogeneric,lcreateerr,pname,nacctnum,lholdfile,ldomix,nlinestart,cmod
Public m.style,m.color,m.id,m.size,m.pack,ctransfer,ltest,lallpicks,lcwhpath,lnormalexit,lcerrormessage,newacctnum
Public cacctname,lacctname,xacctnum,lprepack,nprepacks,npickpacks,tfrom,lprocerror,thisfile,lcpath,ldoslerror
Public cslerror,lpik,lpre,cmessage,cfilename,ltestmail,lmoderr,xfile,cmyfile,lxlsx,archivefile,ldeldbf,plqty1,plqty2,lcerrormessage

Store "" To m.style,m.color,m.id,m.size,m.pack,lcbrokerref,cmessage,cfilename,xfile

_SCREEN.CAPTION = "BILLABONG 943 UPLOADER"

Close Databases All
Set Path To
Set Path To "M:\DEV\PRG\"
Do m:\dev\prg\_setvars With .T.
gmasteroffice ="L"

Set Status Bar On
On Escape Cancel
Set tablevalidate To 0
Set Talk Off
Set enginebehavior 70
Set Escape On
Set Safety Off
Set Multilocks On
Set Deleted On
Set Exclusive Off

Do m:\dev\prg\lookups
ccustname = 'BILLABONG'
lcerrormessage=""
ltest = .F.  && This directs output to F:\WHP\WHDATA test files (default = .f.)
ltestmail = .F. &&lTest && Causes mail to go to default test recipient(s)
lholdfile = .F. && Causes error CATCH message; on completion with no errors, changed to .t. (default = .f.)
lbrowse = lholdfile  && If this is set with lHoldFile, browsing will occur at various points (default = .f.)
loverridebusy = Iif(lholdfile Or ltest,.T.,.F.) && If this is set, CHKBUSY flag in FTPSETUP will be ignored (default = .f.)

loverridebusy = .T.

lallpicks = .F.  && If this is set, all inbounding will be done as units...should not be used (default = .f.)
ldoslerror = .F.  && This flag will be set if there is missing info in the Courtaulds inbound sheet
lxlsx = .F.
ldeldbf = .T.

If Type("acctnum") = "C"
	acctnum = Val(acctnum)
Endif

Store acctnum To nacctnum

tfrom = "TGF WMS Inbound EDI System Operations <inbound-ops@fmiint.com>"
ldogeneric = .F.
lcreateerr = .F.
lmainmail = .T.
tsendto = ""
tcc = ""
cslerror = ""
lcartonacct = .F.
Select 0
Use F:\edirouting\ftpsetup

m.office = coffice
Do Case
Case coffice = "L" And acctnum = 6718
	ctransfer = "PL-BILL-CA"
	cmod = "L"
Otherwise
	ctransfer = "XXX"  && For Modern Shoe, etc.
Endcase

Locate For Upper(ftpsetup.transfer) = Upper(ctransfer)
If Found()
	If !lholdfile And !lbrowse And !loverridebusy
		If ftpsetup.chkbusy=.T.
			Set Step On
			Wait Window At 10,10  "This transfer is busy...will try again later."+Chr(13)+ctransfer Timeout 1
			lnormalexit = .T.
			Throw
		Else
			Replace chkbusy With .T.,trig_time With Datetime()  In ftpsetup Next 1
		Endif
	Endif
Endif

If ltest
	Wait Window "This is a test inbound upload into WHP tables" Timeout 2
Endif

If Used('mm')
	Use In mm
Endif
Select 0
Use F:\3pl\Data\mailmaster Alias mm
Locate For (mm.accountid = 6718  And mm.office = "L") And edi_type = "PL"

If Found()
	Store Trim(mm.acctname) To lcaccountname
	Store Trim(mm.basepath) To lcpath
	Store Trim(mm.archpath) To lcarchivepath
	tsendto = Alltrim(Iif(mm.use_alt,mm.sendtoalt,mm.sendto))
	tcc = Alltrim(Iif(mm.use_alt,mm.ccalt,mm.cc))
	Store Trim(mm.fmask)    To filemask
	Store Trim(mm.progname) To pname
	Store Trim(mm.scaption) To _Screen.Caption
	Store mm.ctnacct    To lcartonacct
	Store mm.dogeneric To ldogeneric
	Locate For edi_type = "MISC" And taskname = "GENERAL"
	tsendtoerr = Alltrim(Iif(mm.use_alt,mm.sendtoalt,mm.sendto))
	tccerr = Alltrim(Iif(mm.use_alt,mm.ccalt,mm.cc))
	tsendtotest = Alltrim(Iif(mm.use_alt,mm.sendtoalt,mm.sendto))
	tcctest = Alltrim(Iif(mm.use_alt,mm.ccalt,mm.cc))
	Use In mm
Else
	lnormalexit=.T.
	Wait Window At 10,10  "No parameters set for this acct# "+Alltrim(Str(acctnum))+"  ---> Office "+coffice Timeout 2
	Throw
Endif

**********************************************************************************************
If ltest Or lholdfile
	tsendto = tsendtotest
	tcc = tcctest
Endif

Wait Window At 10,10  "Now setting up a WMS Inbound for "+lcaccountname+"  ---> Office "+coffice Nowait

Select 0

xsqlexec("select * from account where inactive=0","account",,"qq")
Index On accountid Tag accountid

Select account

If Seek(acctnum,"account","accountid")
	cacctname= account.acctname
Else
	lnormalexit=.T.
	Wait Window At 10,10 "No account name on file.........." Timeout 2
	Close Databases All
	Throw
Endif

xreturn = "X"
Do m:\dev\prg\wf_alt With coffice,acctnum
lcwhpath = Upper(xreturn)
If xreturn = "X"
	Wait Window "No such account for this office" Timeout 2
	Throw
Endif
***********************************************start
If Inlist(acctnum,6718)
	lcwhpath = "F:\whl\whdata\"
Endif

************************added TM
If !ltesting
	lcpath = "F:\FTPUSERS\BILLABONG\943IN\"
	lcarchivepath = 'F:\FTPUSERS\BILLABONG\943IN\archive\'
*  tsendto="todd.margolin@tollgroup.com"
*  tcc="PGAIDIS@FMIINT.COM"
Else
	lcpath = "F:\FTPUSERS\BILLABONG\943IN\"
	lcarchivepath = 'F:\FTPUSERS\BILLABONG\943IN\archive\'
	tsendto="todd.margolin@tollgroup.com"
	tcc="PGAIDIS@FMIINT.COM,joe.bianchi@tollgroup.com"
Endif
************************************************************

len1 = Adir(tarray,lcpath +"*.xls*")
If len1 = 0
	Wait Window "No files found...exiting" Timeout 2
	Close Data All
	_Screen.Caption="Billabong Packing List Upload.............."
	On Error
	Return
Endif

goffice='L'
useca("inwolog","wh",,,"mod=' "+goffice+"' and  accountid ="+Transform(acctnum) )

&&USE &lcwhpath.whgenpk IN 0 ALIAS whgenpk

useca("pl","wh")
xsqlexec("select * from pl where .f.","xpl",,"wh")

For thisfile = 1 To len1
***  cfilename = alltrim(ary1[thisfile,1])
	cfilename = Alltrim(tarray[thisfile,1])
	xfile = lcpath+cfilename
	llcontinue = .T.
	Try
		xfile = lcpath+Alltrim(tarray[thisfile,1])
		If Inlist(Upper(Justext(xfile)),"TMP","DBF")
			Loop
		Endif

		attrfile = '"'+lcpath+Alltrim(tarray[thisfile,1])+'"'
		cfilename = Justfname(xfile)
		!Attrib -r &attrfile  && Removes read-only flag from file to allow deletion
		Wait Window "Filename = "+xfile Timeout 1
		Create Cursor tempmain (a c(50), ;
			b c(30), ;
			c c(30), ;
			d c(50), ;
			e c(30), ;
			f c(30), ;
			g c(30), ;
			h c(30), ;
			i c(30), ;
			j c(30), ;
			k c(30))
		xfilecsv = Juststem(Justfname(xfile))+".csv"
		Do m:\dev\prg\excel_to_csv With cfilename
		Select tempmain
		Append From [&xfilecsv] Type Csv
		doimport()
		archivefile=lcarchivepath+cfilename
		Copy File [&xfile] To [&archivefile]
		xfile= Lower(xfile)
		Delete File  [&xfile]
		xfile = Strtran(xfile,"xlsx","csv")
		Delete File  [&xfile]
		If !upcerror
			goodmail()
			DELETE FILE  [&xfilecsv]
		Endif
	Catch To oerr
		Assert .F. Message "In internal Catch Section (Not main loop)...debug"
		If lnormalexit = .F.
			tmessage = "Account: "+Alltrim(Str(acctnum))+" (Internal loop)"
			tmessage = tmessage+Chr(13)+"TRY/CATCH Exeception Message:"+Chr(13)+;
				[  Error: ] + Str(oerr.ErrorNo) +Chr(13)+;
				[  LineNo: ] + Str(oerr.Lineno) +Chr(13)+;
				[  Message: ] + oerr.Message +Chr(13)+;
				[  Procedure: ] + oerr.Procedure +Chr(13)+;
				[  Details: ] + oerr.Details +Chr(13)+;
				[  StackLevel: ] + Str(oerr.StackLevel) +Chr(13)+;
				[  LineContents: ] + Left(oerr.LineContents,50)+Chr(13)+;
				[  UserValue: ] + oerr.UserValue+Chr(13)

			tsubject = "Error Loading Inbound File "+xfile
			tsendto = "pgaidis@fmiint.com" &&tsendtoerr
			Do Case
			Case Inlist(coffice,"L") And Inlist(nacctnum,6718)
				tcc = ""
			Otherwise
				tcc = tccerr
			Endcase
			tattach = ""
			Do Form m:\dev\frm\dartmail2 With tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"

			Messagebox(tmessage,0,"EDI Error",5000)

			Wait Clear
			llcontinue = .F.
		Else
			Wait Window "Normal Exit..." Timeout 2
		Endif
		Set Status Bar On
	Endtry
Next

Select ftpsetup
Replace ftpsetup.chkbusy With .F. In ftpsetup Next 1


**************************************************************try
Procedure doimport


If Empty(goffice) And ltest Or lholdfile
	goffice = "1"
Else
	goffice = coffice
Endif
Store "" To m.style,m.color,m.id,m.size,m.pack

Select * From inwolog Where .F. Into Cursor xinwolog Readwrite
Select tempmain
LOCATE FOR UPPER(tempmain.b) = UPPER('Carton')
nRecCtn = RECNO()

Set Step On
Delete For Empty(b) AND RECNO()>nRecCtn
Replace a With Upper(a) For !Empty(a)
Replace b With Upper(b) For !Empty(b)
Replace c With Upper(c) For !Empty(c)
Replace d With Upper(d) For !Empty(d)
Replace e With Upper(e) For !Empty(e)
Replace F With Upper(F) For !Empty(F)
Replace g With Upper(g) For !Empty(g)
Replace h With Upper(h) For !Empty(h)
Replace i With Upper(i) For !Empty(i)

Count To Recno
****** added TMARG  5/24/17
Select * From tempmain Where a='PREPAR' Into Cursor T1 Readwrite
Select tempmain
Scan
	If tempmain.a='ON DATE'
		Scatter Memvar
		Select T1
		Append Blank
		Gather Memvar
	Endif
	If tempmain.a='CONTAINER'
		Scatter Memvar
		Select T1
		Append Blank
		Gather Memvar
	Endif
	If tempmain.a='ACCOUNT REF'
		Scatter Memvar
		Select T1
		Append Blank
		Gather Memvar
	Endif
	If tempmain.a='BROKER REF'
		Scatter Memvar
		Select T1
		Append Blank
		Gather Memvar
	Endif
	If tempmain.a='VESSEL/AWB'
		Scatter Memvar
		Select T1
		Append Blank
		Gather Memvar
	Endif
	If tempmain.a='SEAL'
		Scatter Memvar
		Select T1
		Append Blank
		Gather Memvar
	Endif
	If tempmain.a='REGION'
		Scatter Memvar
		Select T1
		Append Blank
		Gather Memvar
	Endif
	If tempmain.a='BRAND'
		Scatter Memvar
		Select T1
		Append Blank
		Gather Memvar
	Endif
	If tempmain.a='PALLET ID'
		Scatter Memvar
		Select T1
		Append Blank
		Gather Memvar
	Endif
Endscan
Select * From tempmain Where !Empty(d) And d!='COLOR' Order By i,c,d,e Into Cursor B1 Readwrite
Select B1
Scan
	Scatter Memvar
	Select T1
	Append Blank
	Gather Memvar
Endscan


Select * From T1 Into Cursor tempmain Readwrite



Select tempmain
Locate

*****************************************************************start loop for multiple files here

Select * From inwolog Where .F. Into Cursor xinwolog Readwrite
recctr = 0

recctr = recctr +1
Wait Window At 10,10 "Checking Record # "+Transform(recctr) Nowait

m.adddt=Datetime()
m.updatedt=Datetime()
m.addby='BILLBONG943'
m.updateby='BILLBONG943'
m.accountid=6718
m.addproc='BILLBONG943'
m.updproc='BILLBONG943'
Select tempmain
Scan
	Do Case
	Case tempmain.a = 'ON DATE'
		m.wo_date=Ctod(tempmain.b)
	Case tempmain.a = 'CONTAINER'
		m.container= tempmain.b
	Case tempmain.a = 'ACCOUNT REF'
		m.acct_ref= tempmain.b
	Case tempmain.a = 'BROKER REF'
		m.brokerref=  tempmain.b
	Case tempmain.a = 'VESSEL/AWB'
		m.reference= tempmain.b
	Case tempmain.a = 'SEAL'
		m.seal= tempmain.b
	Case tempmain.a = 'REGION'
		m.comments=Chr(13)+Chr(13)+Chr(13)+Chr(13)+Chr(13)+"REGION*"+tempmain.b
	Case tempmain.a = 'BRAND'

		m.comments= m.comments+Chr(13)+"BRAND*"+tempmain.b
*****added TMARG 5/23/17

		Do Case
		Case Alltrim(tempmain.b)="BB"
			nacctnum= 6744
			m.acctname='BILLABONG'
			vacctname='BILLABONG'
		Case Alltrim(tempmain.b )="EL"
			nacctnum= 6718
			m.acctname='BILLABONG - ELEMENT'
			vacctname='BILLABONG - ELEMENT'
		Case Alltrim(tempmain.b)="RV"
			nacctnum= 6747
			m.acctname='BILLABONG - RVCA'
			vacctname='BILLABONG - RVCA'
		Case Alltrim(tempmain.b )="CA"
			nacctnum= 6744
			m.acctname='BILLABONG'
			vacctname='BILLABONG'
		Case Alltrim(tempmain.b )="EC"
			nacctnum= 6718
			m.acctname='BILLABONG - ELEMENT'
			vacctname='BILLABONG - ELEMENT'
		Case Alltrim(tempmain.b )="CR"
			nacctnum= 6747
			m.acctname='BILLABONG - RVCA'
			vacctname='BILLABONG - RVCA'
		Endcase
	Endcase
Endscan

*m.acctname ="BILLABONG"
m.office = "L"  &&gMasterOffice
m.mod = "L"

Insert Into xinwolog From Memvar

********************************

Select * From pl Where .F. Into Cursor xpl Readwrite

Select tempmain
Scan
	If At("~",tempmain.b) > 0
		loval=Val(Substr(tempmain.b,1,(At("~",tempmain.b))-1))
		hival=Val(Substr(tempmain.b,(At("~",tempmain.b))+1))
		Replace tempmain.k With Transform((hival-loval)+1) In tempmain
	Else
		Replace tempmain.k With "1" In tempmain
	Endif
Endscan

Locate For Upper(tempmain.b) = "CARTON"
nrecdet = Recno()
Select c As Style,d As Color, e As Id, a As ctngrp, Sum(Int(Val(k))) As ctnqty,  g As Pack, Sum(Int(Val(g))) As totqty, h As vpo,i As wipvalue ;
	from tempmain ;
	where !Empty(g) And Recno()>nrecdet ;
	group By Style,Color,Id,Pack,wipvalue,vpo ;
	order By Style,Color,Id,Pack,ctngrp ;
	into Cursor T1 Readwrite
Select T1

*SELECT style,color,id,pack,SUM(ctnqty) as cnt1 FROM t1 GROUP BY 1,2,3,4

If  !Empty(wipvalue)
******* Changed TMARG 5/16/17
***REPLACE c WITH ALLTRIM(STYLE)+"-"+ALLTRIM(vpo) IN tempmain
	Replace c With Alltrim(Style)+"-"+Alltrim(i) In tempmain
Endif

recctr = 0
poctr=0
plid=0
m.units = .F.

m.adddt=Datetime()
*m.updatedt=DATETIME()
m.addby='BBG943'
*m.updateby='TGFPROC'
m.accountid=nacctnum
*m.acctname='BILLABONG'
m.addproc='BBG943'
*m.updproc='BBG943'

* SELECT *, SUM(totqty) AS linetot, VAL(ctngrp) as ctngrpval FROM t1 GROUP BY ctngrpval INTO CURSOR ctntypes READWRITE

Select *,Val(ctngrp) As ctngrpn,Sum(totqty) As linetot, Sum(ctnqty) As sumctns From T1 Group By Style,Color,Id,Pack Into Cursor ctntypes Readwrite

Select ctntypes
m.office = "L"  &&gMasterOffice
m.mod = "L"
m.date_rcvd = Date()
*!*	SCAN
*!*		xtotqty= ctntypes.linetot
*!*		WAIT WINDOW "Now scanning through pallet IDs" NOWAIT
Select T1
*!*		SCAN FOR ctngrp = ctntypes.ctngrp
Scan
	poctr=poctr+1
	m.plid=m.plid+1
	recctr = recctr +1
	m.units = .F.
	m.cayset = Alltrim(T1.ctngrp)
	ntotqty = T1.ctnqty
	m.totqty = ntotqty
	m.po = Alltrim(Str(poctr))
	csq = [select * from upcmast where style = ']+Alltrim(T1.Style)+[' and color = ']+Alltrim(T1.Color)+[' and id = ']+Alltrim(T1.Id)+[']
	xsqlexec(csq,,,"wh")
	cupc = Alltrim(upcmast.upc)
	Use In upcmast
	cpack = Alltrim(T1.Pack)
	m.echo = "PONUM*"+Alltrim(T1.vpo)+Chr(13)+"UPC*"+cupc+Chr(13)+"PACK*"+cpack
*		REPLACE ECHO WITH cEcho IN xpl
	m.pack = cpack
	m.style = Iif(!Empty(T1.wipvalue),Alltrim(T1.Style)+'-'+Alltrim(T1.wipvalue),T1.Style)
	m.color = Alltrim(T1.Color)
	m.id = Alltrim(T1.Id)
	Insert Into xpl From Memvar
	m.plid = m.plid+1
	m.pack = '1'
	m.units = .T.
	m.totqty = ntotqty* Val(cpack)
	Insert Into xpl From Memvar
Endscan
*ENDSCAN
Wait Clear

Select xpl

If Datetime()<Datetime(2017,05,03,21,45,00)
	Locate
	Browse
Endif

Sum(totqty) To xunitsqty  For units
Sum(totqty) To xcartonsqty For !units

nuploadcount = 0

Select xinwolog
Scan  && Scanning xinwolog here
	m.addby = "BBG943"
	m.adddt = Datetime()
	m.addproc = "BBG943"
	m.plunitsinqty=xunitsqty
	m.plinqty=xcartonsqty
	m.quantity=xcartonsqty
	m.inwologid=dygenpk("INWOLOG","whl") && was inwologid
	m.wo_num   =dygenpk("WONUM","whl")  && wonum was lower case
	m.wo_date = Date()
	nwo_num = m.wo_num
	m.office = "L"  &&gMasterOffice
	m.mod = "L"
*	m.acctname = "BILLABONG"
	insertinto("inwolog","wh",.T.)
	newinwologid = m.inwologid

	Select xpl
	Scan For xpl.inwologid = xinwolog.inwologid
		Scatter Memvar Memo
		m.addby = "BBG943"
		m.adddt = Datetime()
		m.addproc = "BBG943"
		m.inwologid = inwolog.inwologid
**		m.accountid = 6718
		m.wo_num = nwo_num
		m.office = "L"  &&gMasterOffice
		m.mod = "L"
		insertinto("pl","wh",.T.)
	Endscan
Endscan

************added 5/25/17 verify SKU's vs. upcmast
If Used('upcmast')
	Use In upcmast
Endif


**xsqlexec("select * from upcmast where accountid='"+nacctnum+"'",,,"wh")
xsqlexec("select * from upcmast where accountid in (6718,6744,6747)",,,"wh")
Select Distinct Style,Color,Id From xpl Into Cursor t1 Readwrite

select t1
scan
if '-' $(style)
pos1=ATC('-',style)	
replace style with substr(style,1,pos1-1) IN t1	
endif
endscan

Select a.*,b.upc From t1 a Left Join upcmast b On a.Style=b.Style And a.Color=b.Color And a.Id=b.Id Into Cursor xplsku2 Readwrite
Select * From xplsku2 Where Isnull(upc) Into Cursor noupc Readwrite
Select noupc
If Reccount() > 0
	Export To S:\Billabong\temp\missupc Xls
	upcerror=.T.
	Do smerrmail
*	Set Step On
	Return
Endif
tu("pl")
tu('inwolog')

*endif
*  endscan

Endproc
******************************
Procedure closedata
******************************

If Used('inwolog')
	Use In inwolog
Endif

If Used('pl')
	Use In pl
Endif

If Used('upcmast')
	Use In upcmast
Endif

If Used('upcmastsql')
	Use In upcmastsql
Endif

If Used('account')
	Use In account
Endif

*If Used('whgenpk')
*	Use In whgenpk
*Endif
Endproc

*****************************
Procedure slerrormail
******************************
*!*	if used('mm')
*!*		use in mm
*!*	endif
*!*	select 0
*!*	use f:\3pl\data\mailmaster alias mm
*!*	locate for accountid =6718 and taskname = "EXCELERROR"
*!*	lusealt = mm.use_alt
*!*	tsendto = alltrim(iif(lusealt,mm.sendtoalt,mm.sendto))
*!*	tcc = alltrim(iif(lusealt,mm.ccalt,mm.cc))
*!*	use in mm
*!*	tsubject = "Missing information in Inbound Excel sheet"
*!*	tattach = ""
*!*	tmessage = "The following required FIELD(s) is/are missing data:"
*!*	tmessage = tmessage+chr(13)+cslerror
*!*	do form m:\dev\frm\dartmail2 with tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
Endproc

******************************
Procedure smerrmail
******************************
&& Used for all GENERIC INBOUND accounts
If Used('account')
	Use In account
Endif
Select 0
xsqlexec("select * from account where inactive=0","account",,"qq")
Index On accountid Tag accountid
Set Order To
If Seek(nacctnum,'account','accountid')
	cacctname = Alltrim(account.acctname)
Endif
Set Step On
If Used('mm')
	Use In mm
Endif
Select 0
Use F:\3pl\Data\mailmaster Alias mm
Locate For mm.accountid = 6718 And mm.office = "L" And M.edi_type = 'PL'
Use In mm

tsubject = "Style Errors in Inbound Excel sheet: "+cacctname
tattach = "S:\Billabong\temp\missupc.xls"
tmessage = "File Name: "+Iif(!Empty(cfilename),cfilename,Justfname(xfile))
tmessage = tmessage+Chr(13)+Chr(13)+"The following Style/Color/Size combination(s) is/are not in our Style Master."
tmessage = tmessage+Chr(13)+"Please send these Style Master Updates, then RE-TRANSMIT the PL file. Thanks."+Chr(13)
**tmessage = tmessage+chr(13)+padr("STYLE",22)+padr("COLOR",12)+"SIZE"
tmessage = tmessage+Chr(13)+Replicate("=",38)
*!*	select sm_err
*!*	scan
*!*		cstyle = padr(alltrim(sm_err.style),22)
*!*		ccolor = padr(alltrim(sm_err.color),12)
*!*		csize = alltrim(sm_err.id)
*!*		tmessage = tmessage+chr(13)+cstyle+ccolor+csize
*!*	endscan
*!*	use in sm_err
Do Form m:\dev\frm\dartmail2 With tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
Endproc
*************************
Procedure goodmail
*************************
*Set Step On
If Used('account')
	Use In account
Endif
Select 0
xsqlexec("select * from account where inactive=0","account",,"qq")
Index On accountid Tag accountid
Set Order To
If Seek(nacctnum,'account','accountid')
	cacctname = Alltrim(account.acctname)
Endif
*tsubject= "TGF "+ccustname+" Packing List Upload: " +Ttoc(Datetime())
tsubject= "TGF "+cacctname+" Packing List Upload: " +Ttoc(Datetime())
tattach = ""
tmessage = "Packing List uploaded for "+cacctname+Chr(13)+"From File: "+cfilename+Chr(13)
tfrom ="TGF EDI Operations <toll-edi-ops@tollgroup.com>"
If ltesting
	tmessage = tmessage+Chr(13)+"*TEST UPLOAD*"
Endif
Do Form m:\dev\frm\dartmail2 With tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
