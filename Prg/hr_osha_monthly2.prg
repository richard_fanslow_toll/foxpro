* monthly OSHA report for HR - run 1st of each month against prior month
* Provide total # emps, total hours by worksite for prior month
* Build EXE as F:\UTIL\HR\OSHA\HR_OSHA_MONTHLY.EXE

LOCAL loOSHA_MonthlyReport, lcProcessName
lcProcessName = "HR_OSHA_MONTHLY"

runack(lcProcessName)

utilsetup(lcProcessName)

loOSHA_MonthlyReport = CREATEOBJECT('OSHA_MonthlyReport')
loOSHA_MonthlyReport.MAIN()

schedupdate()

CLOSE DATABASES ALL
RETURN

#DEFINE LOGIT 1
#DEFINE WAITIT 2
#DEFINE NOWAITIT 4
#DEFINE SENDIT 8
#DEFINE CRLF CHR(13) + CHR(10)
#DEFINE RETURN_DATA_MANDATORY .F.
#DEFINE RETURN_DATA_NOT_MANDATORY .T.
#DEFINE CSVFORMAT 6
#DEFINE WRITE_ONLY_UNBUFFERED 11
#DEFINE COMMA ","

DEFINE CLASS OSHA_MonthlyReport AS CUSTOM

	cProcessName = 'OSHA_MonthlyReport'

	lTestMode = .F.  && .F. FOR NORMAL OPERATION, .T. TO SUPPRESS ALL BUT DEVELOPER EMAILS AND CHANGE INPUT TABLE, ETC.

	lAutoYield = .T.

	cToday = DTOC(DATE())

	cStartTime = TTOC(DATETIME())

	* connection properties
	nSQLHandle = 0

	* wait window properties
	nWaitWindowTimeout = 2
	lWaitWindowIsOn = .T.

	* logfile properties
	lLoggingIsOn = .T.
	cLogFile = 'F:\UTIL\HR\OSHA\LOGFILES\OSHA_Monthly_log.txt'

	* INTERNAL email properties
	lSendInternalEmailIsOn = .T.
	cFrom = 'mark.bennett@tollgroup.com'
	cSendTo = 'Lauren.Wojcik@tollgroup.com'
	cCC = 'mbennett@fmiint.com, Diana.Landeros@tollgroup.com'
	cSubject = 'OSHA Monthly Report for ' + DTOC(DATE())
	cAttach = ''
	cBodyText = ''

	cCOMPUTERNAME = ''
	cUSERNAME = ''


	FUNCTION INIT
		IF NOT DODEFAULT()
			RETURN .F.
		ENDIF
		WITH THIS
			*SET RESOURCE OFF
			CLOSE DATA
			SET CENTURY ON
			SET DATE AMERICAN
			SET HOURS TO 24
			SET ANSI ON
			SET TALK OFF
			SET DELETED ON
			SET CONSOLE OFF
			SET EXCLUSIVE OFF
			SET SAFETY OFF
			SET EXACT OFF
			SET STATUS BAR ON
			SET SYSMENU OFF
			SET ENGINEBEHAVIOR 70
			_VFP.AUTOYIELD = .lAutoYield
			.cCOMPUTERNAME = GETENV("COMPUTERNAME")
			.cUSERNAME = GETENV("USERNAME")
			IF .lTestMode THEN
				.cLogFile = 'F:\UTIL\HR\OSHA\LOGFILES\OSHA_Monthly_log_TESTMODE.txt'
				.cSendTo = 'mbennett@fmiint.com'
				.cCC = ''
			ENDIF
			.lLoggingIsOn = .lLoggingIsOn AND NOT EMPTY(.cLogFile)
			IF .lLoggingIsOn THEN
				SET ALTERNATE TO (.cLogFile) ADDITIVE
				SET ALTERNATE ON
			ENDIF
		ENDWITH
	ENDFUNC


	FUNCTION DESTROY
		WITH THIS
			IF .lLoggingIsOn  THEN
				SET ALTERNATE OFF
				SET ALTERNATE TO
			ENDIF
		ENDWITH
		DODEFAULT()
	ENDFUNC


	FUNCTION MAIN
		WITH THIS
			LOCAL lnNumberOfErrors, lcSQL, lcSQL2, loError, ldDate, ldFromDate, ldToDate, lcToDate, lcFromDate
			TRY
				lnNumberOfErrors = 0

				.TrackProgress('OSHA MONTHLY REPORT process started....', LOGIT+SENDIT+NOWAITIT)
				.TrackProgress('COMPUTERNAME = ' + .cCOMPUTERNAME, LOGIT+SENDIT)
				.TrackProgress('USERNAME = ' + .cUSERNAME, LOGIT+SENDIT)
				.TrackProgress('PROJECT = HR_OSHA_MONTHLY', LOGIT+SENDIT)
				IF .lTestMode THEN
					.TrackProgress('=============== TEST MODE ===============', LOGIT+SENDIT)
				ENDIF

				*SET STEP ON

				* construct dates for prior month
				ldDate = DATE()

				* TO FORCE OTHER MONTHS
				*ldDate = {^2018-05-01}

				*!*					IF .lTestMode THEN
				ldToDate = ldDate - DAY(ldDate)  && this is last day of prior month
				ldFromDate = GOMONTH(ldToDate + 1,-1)
				*!*					ENDIF

*!*	* to force date ranges
*!*	ldFromDate = {^2015-01-01}
*!*	ldToDate = {^2015-12-31}

				* construct sql filters
				lcFromDate = "DATE'" + TRANSFORM(YEAR(ldFromDate)) + "-" + PADL(MONTH(ldFromDate),2,"0") + "-" + PADL(DAY(ldFromDate),2,"0") + "'"

				lcToDate = "DATE'" + TRANSFORM(YEAR(ldToDate)) + "-" + PADL(MONTH(ldToDate),2,"0") + "-" + PADL(DAY(ldToDate),2,"0") + "'"


				IF USED('SQLCURSOR1') THEN
					USE IN SQLCURSOR1
				ENDIF
				IF USED('SQLCURSOR2') THEN
					USE IN SQLCURSOR2
				ENDIF
				IF USED('SQLCURSOR3') THEN
					USE IN SQLCURSOR3
				ENDIF

				SELECT ;
					NAME, ;
					(DIVISION + DEPT) AS HOMEDEPT, ;
					FILE_NUM, ;
					(CODEHOURS + REGHOURS + OTHOURS + DTHOURS) AS HOURS, ;
					WORKDATE, ;
					WORKSITE ;
					FROM F:\UTIL\TIMESTAR\DATA\TIMEDATA ;
					INTO CURSOR SQLCURSOR1 ;
					WHERE WORKDATE >= ldFromDate ;
					AND WORKDATE <= ldToDate ;
					AND LEN(ALLTRIM(WORKSITE)) = 3 ;
					AND (DIVISION + DEPT) <> '020655' ;
					ORDER BY 1 ;
					READWRITE

				*!*		SELECT SQLCURSOR1
				*!*		BROWSE
				*!*		THROW

				*!*		SELECT distinct name, worksite from	SQLCURSOR1 INTO CURSOR curcheck WHERE INLIST(worksite,'NJ1','NJ2') ORDER BY NAME
				*!*		SELECT curcheck
				*!*		COPY TO C:\A\TESTOSHA2.XLS XL5
				*!*		throw


				USE F:\UTIL\TIMESTAR\DATA\EEINFO.DBF IN 0 ALIAS EEINFO
				*SET STEP ON
				* for OTR drivers give 303 hours per MONTH
				* for OTR drivers give 195 hours per MONTH 7/23/14 MB - TO MATCH JACK DROHANS HOURS BY WORKSITE REPORT.
				SELECT EEINFO
				SCAN FOR DIVISION = '02' AND DEPT = '0655' AND STATUS = 'A' AND HIREDATE <= ldToDate ;

					m.HOMEDEPT = EEINFO.DIVISION + EEINFO.DEPT
					m.WORKSITE = EEINFO.WORKSITE
					m.NAME = EEINFO.NAME
					m.HOURS = 195
					m.FILE_NUM = ALLTRIM(TRANSFORM(EEINFO.INSPID))
					m.WORKDATE = ldFromDate
					*INSERT INTO SQLCURSOR1 FROM MEMVAR
					INSERT INTO SQLCURSOR1 (HOMEDEPT, WORKSITE, NAME, HOURS, FILE_NUM) VALUES (m.HOMEDEPT, M.WORKSITE, m.NAME, m.HOURS, m.FILE_NUM )
				ENDSCAN


				*!*		SELECT distinct name, worksite from	SQLCURSOR1 INTO CURSOR curcheck WHERE UPPER(LEFT(worksite,2)) = 'NJ' ORDER BY NAME
				*!*		SELECT curcheck
				*!*		BROW
				*!*		COPY TO C:\A\TESTOSHA2.XLS XL5
				*!*		throw
*!*	SELECT SQLCURSOR1
*!*	COPY TO C:\A\TESTOSHA.XLS XL5


*!*					SELECT ;
*!*						CMONTH(ldFromDate) AS MONTH, ;
*!*						YEAR(ldFromDate) AS YEAR, ;
*!*						WORKSITE, ;
*!*						COUNT(DISTINCT FILE_NUM) AS HEADCNT, ;
*!*						SUM(HOURS) AS TOTHOURS ;
*!*						FROM SQLCURSOR1 ;
*!*						INTO CURSOR SQLCURSOR2 ;
*!*						GROUP BY 1, 2, 3 ;
*!*						ORDER BY 1, 2, 3

			* adding the OSHA Site num per diana landeros' request 1/5/2017

				SELECT ;
					CMONTH(ldFromDate) AS MONTH, ;
					YEAR(ldFromDate) AS YEAR, ;
					A.WORKSITE, ;
					NVL(B.DESC,'') AS SITENAME, ;
					NVL(B.OSHANUM,'') AS SITENUM, ;
					COUNT(DISTINCT A.FILE_NUM) AS HEADCNT, ;
					SUM(A.HOURS) AS TOTHOURS ;
					FROM SQLCURSOR1 A ;
					LEFT OUTER JOIN ;
					F:\UTIL\HR\DATA\WORKSITES B ;
					ON B.WORKSITE = A.WORKSITE ;
					INTO CURSOR SQLCURSOR2 ;
					GROUP BY 1, 2, 3, 4, 5 ;
					ORDER BY 1, 2, 3					


				lcOutputFile = "F:\UTIL\HR\OSHA\REPORTS\OSHA_MONTHLY_" + CMONTH(ldFromDate) + "_" + TRANSFORM(YEAR(ldFromDate)) + ".XLS"
				.cSubject = 'OSHA Monthly Report for ' + PROPER(CMONTH(ldFromDate)) + " " + TRANSFORM(YEAR(ldFromDate))

				SELECT SQLCURSOR2
				COPY TO (lcOutputFile) XL5
				.TrackProgress('Output to spreadsheet: ' + lcOutputFile, LOGIT+SENDIT+NOWAITIT)

				IF FILE(lcOutputFile) THEN
					* attach output file to email
					.cAttach = lcOutputFile
					.cBodyText = "See attached OSHA Monthly report." + ;
						CRLF + CRLF + "(do not reply - this is an automated report)" + ;
						CRLF + CRLF + "<report log follows>" + ;
						CRLF + .cBodyText
				ELSE
					.TrackProgress('ERROR: unable to email spreadsheet: ' + lcOutputFile, LOGIT+SENDIT+NOWAITIT)
				ENDIF

				CLOSE DATABASES ALL

				.TrackProgress('OSHA MONTHLY REPORT process ended normally.', LOGIT+SENDIT+NOWAITIT)

			CATCH TO loError

				.TrackProgress('There was an error.',LOGIT+SENDIT)
				.TrackProgress('The Error # is: ' + TRANSFORM(loError.ERRORNO), LOGIT+SENDIT)
				.TrackProgress('The Error Message is: ' + TRANSFORM(loError.MESSAGE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred in Program: ' + TRANSFORM(loError.PROCEDURE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred at line #: ' + TRANSFORM(loError.LINENO), LOGIT+SENDIT)
				lnNumberOfErrors = lnNumberOfErrors + 1
				CLOSE DATA

			ENDTRY

			CLOSE DATA
			WAIT CLEAR
			***************** INTERNAL email results ******************************
			.TrackProgress('About to send status email.',LOGIT)
			.TrackProgress('The logfile is: ' + .cLogFile, SENDIT)
			.TrackProgress('OSHA MONTHLY REPORT process started: ' + .cStartTime, LOGIT+SENDIT)
			.TrackProgress('OSHA MONTHLY REPORT process finished: ' + TTOC(DATETIME()), LOGIT+SENDIT)

			IF .lSendInternalEmailIsOn THEN
				DO FORM dartmail2 WITH .cSendTo,.cFrom,.cSubject,.cCC,.cAttach,.cBodyText,"A"
				.TrackProgress('Sent status email.',LOGIT)
			ELSE
				.TrackProgress('Did not send status email: THIS.lSendInternalEmailIsOn = FALSE.',LOGIT)
			ENDIF

		ENDWITH
		RETURN
	ENDFUNC && main


	FUNCTION ExecSQL
		LPARAMETERS tcSQL, tcCursorName, tlNoDataReturnedIsOkay
		LOCAL llRetval, lnResult
		WITH THIS
			* close target cursor if it's open
			IF USED(tcCursorName)
				USE IN (tcCursorName)
			ENDIF
			WAIT WINDOW "Fetching remote data; please wait..." NOWAIT
			lnResult = SQLEXEC(.nSQLHandle, tcSQL, tcCursorName)
			llRetval = ( lnResult > 0 )
			IF llRetval THEN
				* see if any data came back
				IF NOT tlNoDataReturnedIsOkay THEN
					IF (NOT USED(tcCursorName)) OR EOF(tcCursorName) THEN
						llRetval = .F.
						.TrackProgress('No data was returned by this query: ' +CRLF + tcSQL, LOGIT+SENDIT)
						.cSendTo = 'Mark Bennett <mbennett@fmiint.com>'
					ENDIF
				ENDIF
			ELSE
				.TrackProgress("Result Code [" + TRANSFORM(lnResult) + "] returned by this query: " + tcSQL, LOGIT+SENDIT)
				.cSendTo = 'Mark Bennett <mbennett@fmiint.com>'
			ENDIF
			WAIT CLEAR
			RETURN llRetval
		ENDWITH
	ENDFUNC


	PROCEDURE TrackProgress
		* do any combination of Wait Window, writing to logfile, and adding to body of email,
		* based on nFlags parameter.
		LPARAMETERS tcExpression, tnFlags
		WITH THIS
			IF BITAND(tnFlags,LOGIT) = LOGIT THEN
				IF .lLoggingIsOn THEN
					?
					? .cProcessName + "  " + TTOC(DATETIME()) + ": " + tcExpression
				ENDIF
			ENDIF
			IF .lWaitWindowIsOn THEN
				IF BITAND(tnFlags,NOWAITIT) = NOWAITIT THEN
					WAIT WINDOW tcExpression NOWAIT
				ENDIF
				IF BITAND(tnFlags,WAITIT) = WAITIT THEN
					WAIT WINDOW tcExpression TIMEOUT .nWaitWindowTimeout
				ENDIF
			ENDIF
			IF BITAND(tnFlags,SENDIT) = SENDIT THEN
				IF .lSendInternalEmailIsOn THEN
					.cBodyText = .cBodyText + tcExpression + CRLF + CRLF
				ENDIF
			ENDIF
		ENDWITH
	ENDPROC  &&  TrackProgress


ENDDEFINE
