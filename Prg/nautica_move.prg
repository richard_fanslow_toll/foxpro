*!* Nautica incoming file mover
*!* 06.13.2012

CLOSE DATA ALL

lTesting  = .f.
IF lTesting
	SET STEP ON
ENDIF

cInFolder = "f:\ftpusers\nautica\in\"
CD &cInFolder

len1 = ADIR(ary1,'*.*')
IF len1 = 0
	CLOSE DATA ALL
	RETURN
ENDIF

FOR i = 1 TO len1
	cFilename = ALLTRIM(ary1[i,1])
	IF cFilename = "FOX" && In case edi_converter files are present
		DELETE FILE &cFilename
		LOOP
	ENDIF
	cFilenameIn = cInFolder+cFilename


	STORE .F. TO l940,l856,l997
	cstring = FILETOSTR(cFilename)
	DO CASE
		CASE "GS*OW"$cstring
			l940 = .T.
		CASE "GS*SH"$cstring
			l856 = .T.
		CASE "GS*FA"$cstring
			l997 = .T.
	ENDCASE

	cOutDir = IIF(lTesting,"F:\FTPUSERS\NAUTICA\TEST\","F:\FTPUSERS\NAUTICA\")

	cOutFolder = ICASE(l997,"997in\",l856,"856in\","940in\")

	cFilenameOut = cOutDir+cOutFolder+cFilename

	COPY FILE &cFilenameIn TO &cFilenameOut
	DELETE FILE &cFilenameIn
ENDFOR

CLOSE DATA ALL
RETURN
