*!* Row One 846 CREATOR

Parameters cOffice

Public cWhse,cFilename,cFilename2,tsendto,tcc,tsendtoerr,tccerr,tsendtotest,tcctest,lIsError,lJoeMail
Public leMail,tfrom,lDoCatch
Close Data All
lTesting = .F. && If TRUE, disables certain functions
lUsePL = .F.
lIsError = .F.
On Error Throw
nAcctNum = 6468
cOffice = "C"
lDoCatch  = .T.
cErrMsg = ""

If lTesting
  cIntUsage = "T"
Else
  cIntUsage = "P"
Endif

Do m:\dev\prg\_setvars With .T.

tfrom = "TGF Warehouse Operations <warehouse-ops@fmiint.com>"
*Set STEP ON
Try

  cOfficeUsed = cOffice
  xReturn = "XXX"
  Do m:\dev\prg\wf_alt With cOffice,nAcctNum
  If xReturn = "XXX"
    Wait Window "No Bldg. # Provided...closing" Timeout 3
    Return
  Endif

  If lTesting
    cUseFolder = "f:\whp\"
  Else
    cUseFolder = Upper(xReturn)
  Endif

  Dimension thisarray(1)
  cSuffix = ""
  cWhse = "SP"

  lOverrideXcheck = .T. && If true, doesn't check ASN carton count against barcode scans.
  lHoldFile = .T. && If true, keeps output file in 846HOLD folder, otherwise into OUT
  leMail = .T.  && If .t., sends mail to WAREHOUSES

  cBarMiss = ""
  cASN = ""
  cUPC = ""
  nSTSets = 0
  cSpecChar = .F.
  XFERUPDATE = .T.
  nFilenum = 0
  lBackup = .F.
  lOverage = .F.
  lJoeMail = .T.
  lCopyCA = .T.
  cErrMsg = ""
  lIsError = .T.
  lXfer944 = .F.
  cTrk_WO = ""
  nCTNTotal = 0

*!* SET CUSTOMER CONSTANTS
  cCustName = "ROWONE"  && Customer Identifier (for folders, etc.)
  cMailName = "ROWONE"  && Proper name for eMail, etc.
  cZLAddress = "101 SPRING STREET"
  cZLCity = "NEW YORK"
  cZLState = "NY"
  cZLZip = "10002"

  cCustPrefix = "MS" && Customer Prefix (for output file names)
  cX12 = "005010"  && X12 Standards Set used
  crecid = "2122441111"  && Recipient EDI address

  dt1 = Ttoc(Datetime(),1)
  dt2 = Datetime()
  dtmail = Ttoc(Datetime())

  cFilestem = cCustPrefix+dt1+".txt"
  cFilename = "f:\ftpusers\rowone\846hold\"+cFilestem && Holds file here until complete and ready to send
  cFilename2 = "f:\ftpusers\rowone\846out\"+cFilestem
  cFilenameOUT= "f:\ftpusers\rowone\out\"+cFilestem
  cArchiveFile = "f:\ftpusers\rowone\846out\archive\"+cFilestem

  Store "" To c_CntrlNum,c_GrpCntrlNum
  nFilenum = Fcreate(cFilename)

*!* SET OTHER CONSTANTS
  nCtnNumber = 1
  cString = ""
  csendqual = "ZZ"
  csendid = "TGF"
  crecqual = "12"
  cfd = "*"  && Field/element delimiter
  csegd = "~" && Line/segment delimiter
  nSegCtr = 0
  cterminator = ">" && Used at end of ISA segment
  cdate = Dtos(Date())
  ctruncdate = Right(cdate,6)
  ctime = Left(Time(Date()),2)+Substr(Time(Date()),4,2)
  cfiledate = cdate+ctime
  cOrig = "J"
  cdate = Dtos(Date())
  ctruncdate = Right(cdate,6)
  ctime = Left(Time(Date()),2)+Substr(Time(Date()),4,2)

** PAD ID Codes
  csendidlong = Padr(csendid,15," ")
  crecidlong = Padr(crecid,15," ")

*!* Serial number incrementing table
  If Used("serfile")
    Use In serfile
  Endif

  Use ("F:\3pl\data\serial\rowone_serial") In 0 Alias serfile
  
	xsqlexec("select * from inven where mod='5' and accountid=6468",,,"wh")

	if usesqloutwo("5")
	else  
	  Use F:\wh5\whdata\outship In 0
  	  Use F:\wh5\whdata\outdet In 0
  	endif

	if usesqloutwo('5')
	  xsqlexec("select outship.wo_num, style, color, id, outdet.upc, outdet.pack, totqty, from outship, outdet " + ;
	  	  "where outdet.outshipid=outship.outshipid and outship.accountid=6468 and del_date={} and pulled=1","xdy",,"wh")

	  select wo_num, style, color, id, upc, pack, totqty, totqty*val(pack) as total_units from xdy into cursor wip_items readwrite

	else
	  Select outship.wo_num,style,color,id,outdet.upc,outdet.Pack,totqty, totqty*Val(Pack) As total_units From outship Left Outer Join outdet On outdet.outshipid = outship.outshipid;
		  where Inlist(outship.accountid,6468) And Emptynul(del_date) And pulled into Cursor wip_items Readwrite
	endif

  Select wip_items
  Delete For Empty(upc)


  If !Used("edi_trigger")
    Use F:\3pl\Data\edi_trigger In 0
  Endif

  nRCnt = 0
  nISACount = 0
  nSTCount = 1
  nSECount = 1

  Select Style,Color,Id,Pack,totqty,0000 As eachqty From inven Where Inlist(accountid,6468) And totqty>0 And units Into Cursor items Readwrite
  Select items
*!*    Scan
*!*      Replace eachqty With totqty*Val(Transform(Pack))
*!*    Endscan

set step On 

  Select items

*!*    Select wip_items
*!*    Scan
*!*      Select items
*!*      Locate For Alltrim(items.upc) = Alltrim(wip_items.upc)
*!*      If Found()
*!*        Replace items.totqty With items.totqty + wip_items.total_units
*!*        Replace items.wipqty With wip_items.total_units
*!*      Else
*!*        Insert Into items (upc,totqty) Values (wip_items.upc,wip_items.total_units)
*!*        Replace items.wipqty With wip_items.total_units
*!*      Endif
*!*    Endscan

*!*    Select upc,Sum(totqty) as newqty From items Into Cursor newitems Group By upc

  Do num_incr
  Store "ISA"+cfd+"00"+cfd+"          "+cfd+"00"+cfd+"          "+cfd+csendqual+cfd+csendidlong+cfd+;
  crecqual+cfd+crecidlong+cfd+ctruncdate+cfd+ctime+cfd+"U"+cfd+"00501"+cfd+;
  PADL(c_CntrlNum,9,"0")+cfd+"0"+cfd+cIntUsage+cfd+cterminator+csegd To cString
  Do cstringbreak

  Store "GS"+cfd+"IB"+cfd+csendid+cfd+crecid+cfd+cdate+cfd+ctime+cfd+c_CntrlNum+;
  cfd+"X"+cfd+cX12+csegd To cString
  Do cstringbreak

  nSegCtr = 0
  nISACount = 1

  If nSECount = 1
    Store "ST"+cfd+"846"+cfd+Padl(c_GrpCntrlNum,9,"0")+csegd To cString
    Do cstringbreak
    nSegCtr = nSegCtr + 1

    Store "BIA"+cfd+"08"+cfd+"MM"+cfd+"FMM"+cfd+cdate+csegd To cString
    Do cstringbreak
    nSegCtr = nSegCtr + 1
    nSECount = 0
  Endif

  Select items
  lineCtr = 1

  Scan &&FOR indet.inwologid = inwolog.inwologid AND units
*    STORE "LX"+cfd+ALLTRIM(TRANSFORM(LineCtr))+csegd TO cString
*    DO cstringbreak
*    nSegCtr = nSegCtr + 1

    Store "LIN"+cfd+cfd+"SW"+cfd+Alltrim(items.style)+cfd+"CL"+cfd+Alltrim(items.color)+cfd+"SZ"+cfd+Alltrim(items.id)+csegd To cString
    Do cstringbreak
    nSegCtr = nSegCtr + 1

    Store "QTY"+cfd+"17"+cfd+Alltrim(Transform(items.totqty))+csegd To cString
    Do cstringbreak
    nSegCtr = nSegCtr + 1

    lineCtr = lineCtr +1

  Endscan

  Wait Window "END OF SCAN/CHECK PHASE ROUND..." Nowait

  nSTCount = 0

  Store "CTT"+cfd+Alltrim(Transform(lineCtr))+csegd To cString
  Do cstringbreak
  nSegCtr = nSegCtr + 1

  Store  "SE"+cfd+Alltrim(Str(nSegCtr+1))+cfd+Padl(c_GrpCntrlNum,9,"0")+csegd To cString
  Do cstringbreak
  nSegCtr = 0
  nSTSets = nSTSets + 1

  Store  "GE"+cfd+Alltrim(Str(nSTSets))+cfd+c_CntrlNum+csegd To cString
  Do cstringbreak

  Store  "IEA"+cfd+"1"+cfd+Padl(c_CntrlNum,9,"0")+csegd To cString
  Do cstringbreak

  =Fclose(nFilenum)

  If !lTesting
    If !Used("ftpedilog")
      Select 0
      Use F:\edirouting\ftpedilog Alias ftpedilog
      Insert Into ftpedilog (ftpdate,filename,acct_name,Type) Values (dt2,cFilename,"cCustname","944")
      Use In ftpedilog
    Endif
    Select edi_trigger
  Endif

  Wait "846 Creation process complete:"+Chr(13)+cFilename Window At 45,60 Timeout 3
  cFin = "846 FILE "+cFilename2+" CREATED WITHOUT ERRORS"

*  Set Step On
  Copy File &cFilename To &cArchiveFile 
  Copy File &cFilename To &cFilename2
*  Copy File &cFilename To &cFilenameOUT   cArchiveFile 
 * tsendto = "todd.margolin@tollgroup.com,Maria.estrella@tollgroup.com"
 tsendto = "todd.margolin@tollgroup.com,smitra@xperiasolutions.com,Joe@RowOneBrands.Com"
  tattach = cfilename
  tcc =""
  tFrom ="Toll EDI System Operations <fmi-transload-ops@fmiint.com>"
  tmessage = "Row One inventory as of   "+Ttoc(Datetime())
  tSubject = "Row One inventory "
  Do Form m:\dev\frm\dartmail2 With tsendto,tFrom,tSubject, tcc,tattach,tmessage  ,"A"
SET STEP ON 
DELETE FILE &cFilename 

SET STEP ON 
*!*	  If !lHoldFile And !lTesting
*!*	    Erase &cFilename
*!*	  Endif

  lDoCatch = .F.

Catch To oErr
  If lDoCatch And Empty(Alltrim(cErrMsg))
    Wait Window "At error catch block" Timeout 1
    Set Step On
    tsubject = "846 Error in "+cCustName
    tattach = " "
    tsendto = tsendtoerr
    tcc = tccerr

    tmessage = cCustName+" Error processing "+Chr(13)
    tmessage =tmessage+ "Try/CATCH Exeception Message:"+Chr(13)+;
    [  Error: ] + Str(oErr.ErrorNo) +Chr(13)+;
    [  Procedure: ] + oErr.Procedure +Chr(13)+;
    [  LineNo: ] + Str(oErr.Lineno) +Chr(13)+;
    [  Message: ] + oErr.Message +Chr(13)+;
    [  Details: ] + oErr.Details +Chr(13)+;
    [  StackLevel: ] + Str(oErr.StackLevel) +Chr(13)+;
    [  LineContents: ] + oErr.LineContents+Chr(13)+;
    [  UserValue: ] + oErr.UserValue
  Endif
Endtry

************************************************************************
*!* END OF MAIN PROGRAM
************************************************************************

****************************
Procedure close846
****************************
** Footer Creation
Return
****************************
Procedure cstringbreak
****************************
*  FPUTS(nFilenum,cString)
Fwrite(nFilenum,cString)
Return

****************************
Procedure num_incr
****************************
Select serfile
Replace serfile.seqnum With serfile.seqnum + 1 In serfile
Replace serfile.grpseqnum With serfile.grpseqnum + 1 In serfile
c_CntrlNum = Alltrim(Str(serfile.seqnum))
c_GrpCntrlNum = Alltrim(Str(serfile.grpseqnum))
Return

************************************************************************************************
