*!* TKO 940

PARAMETERS cOfficeUsed
IF VARTYPE(cOfficeUsed) = "L"
	cOfficeUsed = "C" && Change here during testing
ENDIF
cOfficeUsed = UPPER(cOfficeUsed)
CLOSE DATABASES ALL
*!* Set and initialize public variables
PUBLIC xFile,archivefile,cXdate1,dXdate2,cOffice,lTesting,cUseDir,cCustName,nTotPT,cMailName,nFileSize,lEmail
PUBLIC cDelimiter,cTranslateOption,EmailCommentStr,LogCommentStr,nAcctNum,cAcctNum,lLoop,lDoPkg,lDropShip
PUBLIC chgdate,ptid,ptctr,nPTQty,lTestUploaddet,cOfficename,cConsignee,cStoreNum,cTotPT,tsendtoerr,tccerr,lMB
PUBLIC cPickticket_num_start,cPickticket_num_end,cLoadID,lcPath,cUseFolder,cShip_ref,tsendtoerrtko,tccerrtko
PUBLIC tsendto,tcc,lcArchivepath,nXPTQty,NormalExit,cMessage,fa997file,m.printstuff,lTestImport,cMod,cISA_Num,lLord
PUBLIC lOverrideBusy,cFilemask,cCNEE_Ref,lBrowFiles,lSams,lPick,lPullByCtn,cFilename,lNewPack,lTestEMail,lLoadSQL
PUBLIC ARRAY a856(1)
SET DELETED ON

TRY

	lTesting = .F.  && default = .f.
	lTestEMail = lTesting  && default = .f.
	lTestImport = lTesting  && default = .f.
	lTestUploaddet = lTesting  && default = .f.
	lOverrideBusy = IIF(lTesting OR lTestEMail,.T.,.F.)
*	lOverrideBusy = .T.
	lEmail = .T.

	DO m:\dev\prg\_setvars WITH lTesting
	_SCREEN.CAPTION ="TKO 940 PROCESS"
*	_SCREEN.WINDOWSTATE=IIF(lTesting OR lTestEMail OR lOverrideBusy,2,1)

	IF !lTesting
		SELECT 0
		USE F:\edirouting\FTPSETUP SHARED
		cTransfer = "940-TKO-"
		LOCATE FOR FTPSETUP.TRANSFER = cTransfer
		IF !chkbusy
			REPLACE chkbusy WITH .T.,trig_time WITH DATETIME() FOR FTPSETUP.TRANSFER = cTransfer
		ELSE
			IF !lOverrideBusy AND !lTesting
				WAIT WINDOW "Process is flagged 'BUSY'...closing"
				CLOSE DATABASES ALL
				NormalExit = .T.
				THROW
			ENDIF
		ENDIF
		USE IN FTPSETUP
	ENDIF

	lBrowFiles = lTesting && Set to true to brow XPT/XPTDET files (in development only)
*	lBrowfiles = .T.
	lNewPack = .T. && default = .f. until testing is complete

	WAIT WINDOW "New Pack setting is "+IIF(lNewPack,"ON","OFF") TIMEOUT 2
	IF lTesting
		CLOSE DATABASES ALL
		CLEAR
		IF lTestImport
			WAIT WINDOW "This is a TEST upload into F:\WHP" TIMEOUT 2
		ENDIF
	ENDIF

	SET EXACT OFF
	NormalExit = .F.
*ON ERROR THROW

	CaptionStr = "TKO 940 Process"
	STORE CaptionStr TO _SCREEN.CAPTION

	STORE cOfficeUsed TO cOffice
	cMod = ICASE(cOffice = "C","2",cOffice = "N","I",cOffice)
	gOffice = cMod
	lLoadSQL = .T.

	cOfficename = IIF(cOffice = "C","California",IIF(cOffice = "M","Florida","New Jersey"))
	STORE "" TO EmailCommentStr,cLoadID,cConsignee
	nAcctNum = 5865  && Pickpack account number - will change if Prepack flag found
	cMessage = ""


	cCustName = "TKO"
	cMailName = "TKO"
	cAlias = "TKO"
	cDelimiter = "*"
	cTranslateOption ="CR_TILDE"
	cUseDir = "m:\dev\prg\"+cCustName+"940_"

	xFile = ""
	cXdate1 = ""
	dXdate2 = DATE()

	STORE nAcctNum TO Acctnum
	cAcctNum = ALLTRIM(STR(nAcctNum))
	LogCommentStr = ""
	cShip_ref = ""

****tmarg
**IF  !lTesting
	SET STEP ON
	SELECT 0
	USE F:\3pl\DATA\mailmaster ALIAS mm SHARED
	LOCATE FOR mm.edi_type = "940" AND mm.office = cOfficeUsed AND mm.GROUP = cAlias
	IF FOUND()
		STORE TRIM(mm.acctname) TO cAccountName
		STORE TRIM(mm.basepath) TO lcPath
		STORE TRIM(mm.archpath) TO lcArchivepath
		STORE TRIM(IIF(mm.use_alt,mm.sendtoalt,mm.sendto)) TO tsendto
		STORE TRIM(IIF(mm.use_alt,mm.ccalt,mm.cc)) TO tcc
		STORE JUSTFNAME(TRIM(mm.fmask))    TO cFilemask
		STORE mm.CtnAcct        TO lCartonAcct
		IF lTesting
			LOCATE
			LOCATE FOR mm.edi_type = "MISC" AND mm.taskname = "GENERAL"
			STORE TRIM(mm.sendto) TO tsendto
			STORE TRIM(mm.cc)   TO tcc
		ENDIF
		tsubject= "Pickticket Upload for: "+cMailName+"-"+cOfficename+"  at "+TTOC(DATETIME())
	ELSE
		WAIT WINDOW AT 10,10  "No parameters set for this acct# "+ALLTRIM(STR(Acctnum))+"  ---> Office "+cOffice TIMEOUT 2
		THROW
	ENDIF
	LOCATE
	LOCATE FOR mm.edi_type = "MISC" AND mm.GROUP = "TKO" AND mm.taskname = "PTERR"
	STORE TRIM(IIF(mm.use_alt,mm.sendtoalt,mm.sendto)) TO tsendtoerrtko
	STORE TRIM(IIF(mm.use_alt,mm.ccalt,mm.cc)) TO tccerrtko
	LOCATE
	LOCATE FOR mm.edi_type = "MISC" AND mm.taskname = "GENERAL"
	STORE TRIM(IIF(mm.use_alt,mm.sendtoalt,mm.sendto)) TO tsendtoerr
	STORE TRIM(IIF(mm.use_alt,mm.ccalt,mm.cc)) TO tccerr
	USE IN mm


	IF lTesting AND lTestImport
		lcPath = "f:\ftpusers\tko-nj\940intest\"
		WAIT WINDOW "Path used = "+lcPath NOWAIT
		cUseFolder = "F:\WHP\WHDATA\"
	ELSE
		xReturn = "XXX"
		xReturn  = wf(cOffice,nAcctNum)
		cUseFolder = UPPER(xReturn)
	ENDIF

	cOffice = cOffice
	cMod = cMod
	gmasteroffice = cOffice
	gOffice = cMod

	WAIT CLEAR
	SET STEP ON

	DO (cUseDir+"PROCESS")


	USE F:\edirouting\FTPSETUP SHARED
	cTransfer = "940-TKO"
	REPLACE chkbusy WITH .F. FOR FTPSETUP.TRANSFER = cTransfer
	USE IN FTPSETUP
	NormalExit = .T.

CATCH TO oErr
	IF NormalExit = .F.
		ASSERT .F. MESSAGE "In Error Catch"
		SET STEP ON
		tattach  = ""
		IF !EMPTY(cMessage)
			tsubject = cMailName+" 940 File Upload Error at "+TTOC(DATETIME())
			tmessage = "The following file error(s) halted processing; there may be others: "
			tmessage = tmessage+CHR(13)+cMessage+CHR(13)+CHR(13)
			tmessage = tmessage+CHR(13)+CHR(13)+"FILE: "+cFilename+CHR(13)+CHR(13)
			tsendto  = tsendtoerrtko
			tcc = tccerrtko
		ELSE
			tsubject = cMailName+" 940 Upload Error ("+TRANSFORM(oErr.ERRORNO)+") at "+TTOC(DATETIME())
			tattach  = ""
			tsendto  = tsendtoerr
			tcc = tccerr
			tmessage = cMailName+" 940 Upload Error..... Please fix me........!"
			lcSourceMachine = SYS(0)
			lcSourceProgram = SYS(16)

			tmessage =tmessage+ "Try/CATCH Exeception Message:"+CHR(13)+;
				[  Error: ] + STR(oErr.ERRORNO) +CHR(13)+;
				[  LineNo: ] + STR(oErr.LINENO) +CHR(13)+;
				[  Message: ] + oErr.MESSAGE +CHR(13)+;
				[  Procedure: ] + oErr.PROCEDURE +CHR(13)+;
				[  Details: ] + oErr.DETAILS +CHR(13)+;
				[  StackLevel: ] + STR(oErr.STACKLEVEL) +CHR(13)+;
				[  LineContents: ] + oErr.LINECONTENTS+CHR(13)+;
				[  UserValue: ] + oErr.USERVALUE+CHR(13)+;
				[  Computer:  ] +lcSourceMachine+CHR(13)+;
				[  940 file:  ] +xFile+CHR(13)+;
				[  Program:   ] +lcSourceProgram
		ENDIF

		tfrom="TGF EDI Processing Center <toll-edi-ops@tollgroup.com>"
		DO FORM m:\dev\FRM\dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
		COPY FILE [&xFile] TO [&archivefile]
		IF FILE(archivefile)
			DELETE FILE [&xFile]
		ENDIF
		USE F:\edirouting\FTPSETUP SHARED
		cTransfer = "940-TKO"
		REPLACE chkbusy WITH .F. FOR FTPSETUP.TRANSFER = cTransfer
		USE IN FTPSETUP
	ELSE
		WAIT WINDOW AT 10,10 "Normal Exit " TIMEOUT 2
	ENDIF
FINALLY
	ON ERROR
	CLOSE DATABASES ALL
	SET STATUS BAR ON
ENDTRY

