PARAMETERS nAcctNum,cCustName,cPPName,lUCC,cOffice,lSQL3,lMoret,lDoCartons
IF VARTYPE(cOffice) # "C"
	cOffice = "C"
ENDIF

SET ESCAPE ON
ON ESCAPE CANCEL

cFileOutName = "V"+TRIM(cPPName)+"pp"
IF USED(cFileOutName)
	USE IN &cFileOutName
ENDIF

nAcct = ALLTRIM(STR(nAcctNum))
cSQL = IIF(lTesting,"njsql1","tgfnjsql01")

SELECT 0
USE F:\wh\sqlpassword
cSQLPass = ALLTRIM(sqlpassword.PASSWORD)
USE IN sqlpassword
*cSqlPass = "B%g23!7#$"
IF USED("tempnanj1")
	USE IN tempnanj1
ENDIF

IF lTesting
SET STEP ON 
endif

lcDSNLess="driver=SQL Server;server=&csql;uid=SA;pwd=&cSQLPass;DATABASE=PICKPACK"

nHandle=SQLSTRINGCONNECT(m.lcDSNLess,.T.)
SQLSETPROP(0,'DispLogin',3)
SQLSETPROP(0,"dispwarnings",.F.)

WAIT WINDOW "Now processing "+cCustName+" SQL view...please wait" NOWAIT
IF nHandle=0 && bailout
	WAIT WINDOW "Could not make SQL connection" TIMEOUT 3
	cRetMsg = "NO SQL CONNECT"
	RETURN cRetMsg
	THROW
ENDIF

lAppend = .F.

SELECT sqlwonanjing
*!* Scans through all WOs within the OUTSHIP BOL#

SCAN
	nWO_Num = sqlwonanjing.wo_num
	nWo   = ALLTRIM(STR(nWO_Num))

*!* Changed select to drop accountid requirement, 12.30.2005
	IF DATETIME() < DATETIME(2018,03,27,22,30,00) OR lTestInput

		lcQ1=[SELECT * FROM dbo.labels Labels]
		lcQ2 = [ WHERE Labels.wo_num = ]
		lcQ3 = " &nWo "
*!*			lcQ4 = [ AND  Labels.accountid = ]
*!*			lcQ5 = " &nAcct "
		IF lUCC
			lcQ6 = [order by Labels.ship_ref,Labels.outshipid,Labels.ucc]
		ELSE
			lcQ6 = [order by Labels.ship_ref,Labels.outshipid,Labels.CartonNum]
		ENDIF
		lcsql = lcQ1+lcQ2+lcQ3+lcQ6

	else
		if usesql()
			if lucc
				xorderby="order by ship_ref, outshipid, ucc"
			else
				xorderby="order by ship_ref, outshipid, cartonnum"
			endif
			
			xsqlexec("select * from cartons where wo_num="+nwo+" "+xorderby,cFileOutName,,"pickpack")

		else
			lcQ1 = [SELECT * FROM dbo.cartons Cartons]
			lcQ2 = [ WHERE Cartons.wo_num = ]
			lcQ3 = " &nWo "
			IF lUCC
				lcQ6 = [order by Cartons.ship_ref,Cartons.outshipid,Cartons.ucc]
			ELSE
				lcQ6 = [order by Cartons.ship_ref,Cartons.outshipid,Cartons.CartonNum]
			ENDIF

			lcsql = lcQ1+lcQ2+lcQ3+lcQ6
		endif
	ENDIF

	if usesql() AND !lTesting
	else
		llSuccess=SQLEXEC(nHandle,lcsql,cFileOutName)
		IF llSuccess=0  && no records 0 indicates no records and 1 indicates records found
			WAIT WINDOW "No "+cCustName+" records found" TIMEOUT 2
			cRetMsg = "NO SQL CONNECT"
			RETURN cRetMsg
		ENDIF
		SQLCANCEL(nHandle)
	endif
	
*	SET STEP ON
	IF lAppend = .F.
		lAppend = .T.
		SELECT &cFileOutName
		LOCATE
		IF EOF()
			ASSERT .F. MESSAGE "At empty SQL file"
			cRetMsg = "Empty SQL Retrieval File"
			RETURN cRetMsg
		ENDIF
		COPY TO ("F:\3pl\DATA\tempnanj1")
		USE ("F:\3pl\DATA\tempnanj1") IN 0 ALIAS tempnanj1
	ELSE
		SELECT &cFileOutName
		SCAN
			SCATTER MEMVAR
			INSERT INTO tempnanj1 FROM MEMVAR
		ENDSCAN
	ENDIF
ENDSCAN

SELECT tempnanj1

LOCATE

IF lUCC
	SELECT * FROM tempnanj1 ;
		ORDER BY ship_ref,outshipid,ucc ;
		INTO CURSOR &cFileOutName READWRITE
ELSE
	SELECT * FROM tempnanj1 ;
		ORDER BY ship_ref,outshipid,cartonnum ;
		INTO CURSOR &cFileOutName READWRITE
ENDIF
IF lTesting
	SELECT &cFileOutName
*BROWSE
ENDIF

USE IN tempnanj1
USE IN sqlwonanjing
GO BOTT
WAIT CLEAR
*WAIT "" TIMEOUT 3
SQLDISCONNECT(nHandle)

cRetMsg = "OK"
RETURN cRetMsg
