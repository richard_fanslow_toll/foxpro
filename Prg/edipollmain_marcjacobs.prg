runack("EDIPOLLER_MARCJACOBS")

CLOSE DATABASES ALL
CLEAR ALL
RELEASE ALL
WITH _SCREEN
	.AUTOCENTER = .T.
	.WINDOWSTATE = 0
	.BORDERSTYLE = 1
	.WIDTH = 260
	.HEIGHT = 210
	.TOP = 290
	.LEFT = 940
	.CLOSABLE = .F.
	.MAXBUTTON = .F.
	.MINBUTTON = .F.
	.SCROLLBARS = 0
	.CAPTION = "MARC JACOBS POLLER - 945/XFERS"
ENDWITH
TRY
	DO m:\dev\prg\_setvars
	PUBLIC lSkipError,cProcName
	cProcName = ""
	lSkipError =  .F.
	SET STATUS BAR OFF
	SET SYSMENU OFF
	ON ESCAPE CANCEL

	SELECT 0
	USE F:\3pl\DATA\edipollertrip SHARED ALIAS emj
	REPLACE emj.marcjacobs WITH .F. IN emj
	USE IN emj

	SELECT 0
	USE F:\3pl\DATA\last945time SHARED ALIAS mjlast
	REPLACE checkflag WITH .T. FOR poller = "MJ"
	REPLACE tries WITH 0 FOR poller = "MJ"
	USE IN mjlast

	DO FORM m:\dev\frm\edi_jobhandler_marcjacobs

CATCH TO oErr
	IF !lSkipError
		ASSERT .F. MESSAGE "AT CATCH SECTION"
		SET STEP ON

		tfrom    ="TGF EDI Processing Center <tgf-edi-ops@fmiint.com>"
		tsubject = "Marc Jacobs EDI Poller Error ("+TRANSFORM(oErr.ERRORNO)+") at "+TTOC(DATETIME())
		tattach  = ""
		SELECT 0
		USE F:\3pl\DATA\mailmaster ALIAS mm
		LOCATE FOR mm.edi_type = 'MISC' AND mm.taskname = "GENERAL"
		tsendto = IIF(mm.use_alt,sendtoalt,sendto)
		tcc = IIF(mm.use_alt,ccalt,cc)
		USE IN mm
		tmessage = "Marc Jacobs 945 Poller Major Error..... Shutdown"+CHR(13)
		message =tmessage+"Process: "+cProcName+CHR(13)
		lcSourceMachine = SYS(0)

		tmessage =tmessage+ "Try/CATCH Exeception Message:"+CHR(13)+;
			[  Error: ] + STR(oErr.ERRORNO) +CHR(13)+;
			[  LineNo: ] + STR(oErr.LINENO) +CHR(13)+;
			[  Message: ] + oErr.MESSAGE +CHR(13)+;
			[  Procedure: ] + oErr.PROCEDURE +CHR(13)+;
			[  Details: ] + oErr.DETAILS +CHR(13)+;
			[  StackLevel: ] + STR(oErr.STACKLEVEL) +CHR(13)+;
			[  LineContents: ] + oErr.LINECONTENTS+CHR(13)+;
			[  UserValue: ] + oErr.USERVALUE+CHR(13)+;
			[  Computer:  ] +lcSourceMachine+CHR(13)
		DO FORM m:\dev\FRM\dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
	ENDIF
	SELECT 0
	USE F:\3pl\DATA\edipollertrip SHARED ALIAS emj
	REPLACE emj.marcjacobs WITH .T.
	CLOSE DATABASES ALL
FINALLY
	SET LIBRARY TO
	SET STATUS BAR ON
	ON ERROR
ENDTRY
