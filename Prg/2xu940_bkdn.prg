*!* m:\dev\prg\ariat_bkdn.prg
tFrom ="Toll EDI Operations <toll-edi-ops@tollgroup.com>"
CLEAR
lCheckStyle = .T.
lPrepack = .F.
m.color=""
cUCCNumber = ""
cUsePack = ""
lcUCCSeed = 1

*close data all

*Set Step On

SELECT x856
COUNT FOR TRIM(segment) = "ST" TO nSegCnt
cSegCnt = ALLTRIM(STR(nSegCnt))
LOCATE

ptctr = 0

WAIT WINDOW "Now in 940 Breakdown" TIMEOUT 1

WAIT "There are "+cSegCnt+" P/Ts in this file" WINDOW TIMEOUT 1
SELECT x856
LOCATE

*!* Constants
m.userid = "2XU940PROC"
m.insdttm = DATETIME()
m.accountid = 6665
nacct_num = 6665
goffice="Y"
gmasteroffice="Y"

m.acct_name = "2XU"
STORE nacct_num TO m.acct_num
m.careof = " "
m.sf_addr1 = "3242 WHIPPLE RD"
m.sf_csz = "UNION CITY, CA 94587"
*!* End constants

useca("ackdata","wh")

SELECT xpt
DELETE ALL
SELECT xptdet
DELETE ALL

STORE 0 TO pt_total_eaches,pt_total_cartons,nCtnCount,nptid,nptdetid,m.weight
STORE 1 TO m.carton_id

SELECT x856
SET FILTER TO
LOCATE

IF lTesting
	SET STEP ON
ENDIF

STORE "" TO cisa_num

lcCurrentGroupNum = ""

WAIT WINDOW "NOW IN INITAL HEADER BREAKDOWN" TIMEOUT 1
WAIT WINDOW "NOW IN INITAL HEADER BREAKDOWN" NOWAIT
*ASSERT .F. MESSAGE "At header bkdn...debug"
nptid = 0
DO WHILE !EOF("x856")
	cSegment = TRIM(x856.segment)
	cCode = TRIM(x856.f1)
	DO CASE
		CASE TRIM(x856.segment) = "ISA"
			cisa_num = ALLTRIM(x856.f13)
			WAIT WINDOW "This is a "+cProperName+" upload." TIMEOUT 2
			ptctr = 0
			SELECT x856
			SKIP
			LOOP

		CASE TRIM(x856.segment) = "GS"
			SELECT x856
			SKIP
			LOOP

		CASE TRIM(x856.segment) = "ST" AND TRIM(x856.f1) = "940"
			IF !lTesting
				lnTryAck = 1
				llACKUpdate = .F.
				lntry =1

				m.groupnum=lcCurrentGroupNum
				m.isanum=cisa_num
				m.transnum=x856.f2
				m.edicode="OW"
				m.accountid=6665
				m.loaddt=DATE()
				m.loadtime=DATETIME()
				m.filename=xfile
				insertinto("ackdata","wh",.T.)

				IF lnTryAck=10
					cErrorMessage ="Error updating ACKDATA......."
					THROW
				ENDIF
			ENDIF
			SELECT x856
			SKIP
			LOOP

		CASE TRIM(x856.segment) = "W05"  && new shipment
			lnVasCtr=0
			lnShipInsCtr = 1
			nCtnCount = 0
			m.style = ""
			REPLACE ackdata.ship_ref WITH ALLTRIM(x856.f2) IN ackdata
			SELECT xpt
			nDetCnt = 0
			APPEND BLANK

			REPLACE xpt.shipins WITH "FILENAME*"+cFilename IN xpt
			REPLACE xpt.qty WITH 0 IN xpt
			REPLACE xpt.origqty WITH 0 IN xpt
			STORE 0 TO pt_total_eaches
			STORE 0 TO pt_total_cartons
			nptid = nptid +1
			WAIT WINDOW AT 10,10 "Now processing PT# "+TRANSFORM(nptid)+" Number "+CHR(13)+TRANSFORM(ptctr)+" of "+cSegCnt NOWAIT

			REPLACE xpt.ptid       WITH nptid IN xpt
			REPLACE xpt.accountid  WITH nacct_num IN xpt
			DO CASE
				CASE lNextDay
					REPLACE xpt.ptdate     WITH dFileDate+1 IN xpt
				CASE lMonday
					DO CASE
						CASE DOW(dFileDate) = 6
							REPLACE xpt.ptdate     WITH dFileDate+3 IN xpt
						CASE DOW(dFileDate) = 7
							REPLACE xpt.ptdate     WITH dFileDate+2 IN xpt
						CASE DOW(dFileDate) = 1
							REPLACE xpt.ptdate     WITH dFileDate+1 IN xpt
					ENDCASE
				CASE l4Day
					REPLACE xpt.ptdate     WITH dFileDate+4 IN xpt
				OTHERWISE
					REPLACE xpt.ptdate     WITH dFileDate IN xpt
			ENDCASE
			m.ship_ref = ALLTRIM(x856.f2)
			m.cnee_ref = ALLTRIM(x856.f3)
			cship_ref = ALLTRIM(x856.f2)
			REPLACE xpt.ship_ref   WITH m.ship_ref IN xpt  && Pick Ticket
			REPLACE xpt.cnee_ref   WITH m.cnee_ref IN xpt  && PO Number

			m.pt = ALLTRIM(x856.f2)
			IF lTesting
*        REPLACE xpt.cacctnum   WITH "999" IN xpt
			ENDIF
			SELECT x856
			SKIP
			LOOP

		CASE TRIM(x856.segment) = "N1" AND TRIM(x856.f1) = "ST" && Ship-to data
			SELECT xpt
			m.consignee = UPPER(ALLTRIM(x856.f2))
			m.consignee = STRTRAN(m.consignee," - "," ")
			m.consignee = STRTRAN(m.consignee,"#","")
			m.consignee = STRTRAN(m.consignee,","," ")

			REPLACE xpt.consignee WITH m.consignee IN xpt
			REPLACE xpt.dcnum WITH ALLTRIM(x856.f4) IN xpt

			SELECT x856
			SKIP 1 IN x856

			IF TRIM(x856.segment) = "N2"  && address info
				REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"N2ADDR*"+UPPER(ALLTRIM(x856.f1)) IN xpt
				REPLACE xpt.NAME    WITH UPPER(ALLTRIM(x856.f1)) IN xpt
				SELECT x856
				SKIP 1 IN x856
				IF TRIM(x856.segment) = "N3"  && address info
					REPLACE xpt.address  WITH UPPER(ALLTRIM(x856.f1)) IN xpt
					REPLACE xpt.address2 WITH UPPER(ALLTRIM(x856.f2)) IN xpt
				ENDIF
				SELECT x856
				SKIP 1 IN x856
				IF TRIM(x856.segment) = "N4"  && address info
					REPLACE xpt.residential WITH .F. IN xpt
					m.CSZ = UPPER(TRIM(x856.f1))+", "+UPPER(TRIM(x856.f2))+" "+TRIM(x856.f3)
					REPLACE xpt.CSZ WITH m.CSZ IN xpt
					REPLACE xpt.country WITH ALLTRIM(x856.f4) IN xpt
					IF TRIM(x856.f5) = "ZZ"
						IF TRIM(x856.f6) = "Y"
							REPLACE xpt.residential WITH .T. IN xpt
						ENDIF
					ENDIF
				ENDIF
			ELSE
				IF TRIM(x856.segment) = "N3"  && address info
					REPLACE xpt.address  WITH UPPER(ALLTRIM(x856.f1)) IN xpt
					REPLACE xpt.address2 WITH UPPER(ALLTRIM(x856.f2)) IN xpt
				ENDIF
				SELECT x856
				SKIP 1 IN x856
				IF TRIM(x856.segment) = "N4"  && address info
					REPLACE xpt.residential WITH .F. IN xpt
					m.CSZ = UPPER(TRIM(x856.f1))+", "+UPPER(TRIM(x856.f2))+" "+TRIM(x856.f3)
					REPLACE xpt.CSZ WITH m.CSZ IN xpt
					REPLACE xpt.country WITH ALLTRIM(x856.f4) IN xpt
					IF TRIM(x856.f5) = "ZZ"
						IF TRIM(x856.f6) = "Y"
							REPLACE xpt.residential WITH .T. IN xpt
						ENDIF
					ENDIF
				ENDIF
			ENDIF

			SELECT x856
			SKIP 1 IN x856
			LOOP

		CASE TRIM(x856.segment) = "N1" AND TRIM(x856.f1) = "OB" && Bill to data
			SELECT xpt
			REPLACE xpt.shipins   WITH xpt.shipins+CHR(13)+"BILLTONAME*"+ALLTRIM(x856.f2) IN xpt
			REPLACE xpt.shipins   WITH xpt.shipins+CHR(13)+"BILLTOCODE*"+ALLTRIM(x856.f4) IN xpt
			SELECT x856
			SKIP 1 IN x856
			IF !INLIST(x856.segment,"N2","N3","N4")
				LOOP
			ENDIF

			IF TRIM(x856.segment) = "N2"  && address info
				REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"BILLTON2*"+UPPER(ALLTRIM(x856.f1)) IN xpt
				SELECT x856
				SKIP 1 IN x856
				IF TRIM(x856.segment) = "N3"  && address info
					REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"BILLTOADDR1*"+UPPER(ALLTRIM(x856.f1)) IN xpt
					REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"BILLTOADDR2*"+UPPER(ALLTRIM(x856.f2)) IN xpt
				ENDIF
				SELECT x856
				SKIP 1 IN x856
				IF TRIM(x856.segment) = "N4"  && address info
					REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"BILLTOCSZ*"+UPPER(TRIM(x856.f1))+", "+UPPER(TRIM(x856.f2))+" "+TRIM(x856.f3) IN xpt
					REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"BILLTOCNTRY*"+UPPER(TRIM(x856.f4)) IN xpt
				ENDIF
			ELSE
				IF TRIM(x856.segment) = "N3"  && address info
					REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"BILLTOADDR1*"+UPPER(ALLTRIM(x856.f1)) IN xpt
					REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"BILLTOADDR2*"+UPPER(ALLTRIM(x856.f2)) IN xpt
				ENDIF
				SELECT x856
				SKIP 1 IN x856
				IF TRIM(x856.segment) != "N4"
					LOOP
				ENDIF
				IF TRIM(x856.segment) = "N4"  && address info
					REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"BILLTOCSZ*"+UPPER(TRIM(x856.f1))+", "+UPPER(TRIM(x856.f2))+" "+TRIM(x856.f3) IN xpt
					REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"BILLTOCNTRY*"+UPPER(TRIM(x856.f4)) IN xpt
				ENDIF
			ENDIF

			SELECT x856
			SKIP 1 IN x856
			LOOP

		CASE TRIM(x856.segment) = "N1" AND TRIM(x856.f1) = "BY" && Ship-for data
			IF lTesting
*        SET STEP ON
			ENDIF
			SELECT xpt
			REPLACE xpt.shipfor WITH UPPER(ALLTRIM(x856.f2)) IN xpt
			REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"STORENAME*"+ALLTRIM(x856.f2) IN xpt
			cStoreNum = ALLTRIM(x856.f4)
			REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"STORENUM*"+cStoreNum IN xpt
			nStoreNum = INT(VAL(cStoreNum))
			IF !nStoreNum > 99999
				REPLACE xpt.storenum  WITH nStoreNum IN xpt
			ENDIF
			REPLACE xpt.sforstore WITH ALLTRIM(x856.f4) IN xpt
			SKIP 1 IN x856
			IF !INLIST(x856.segment,"N2","N3","N4")
				LOOP
			ENDIF
			IF TRIM(x856.segment) = "N2"  && name/address info
				REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"N2ADDR*"+UPPER(ALLTRIM(x856.f1)) IN xpt
				SELECT x856
				SKIP 1 IN x856
				IF TRIM(x856.segment) = "N3"  && address info
					REPLACE xpt.sforaddr1  WITH UPPER(ALLTRIM(x856.f1)) IN xpt
					REPLACE xpt.sforaddr2 WITH UPPER(ALLTRIM(x856.f2)) IN xpt
				ENDIF
				SELECT x856
				SKIP 1 IN x856
				IF TRIM(x856.segment) = "N4"  && address info
					m.sforCSZ = UPPER(TRIM(x856.f1))+", "+UPPER(TRIM(x856.f2))+" "+TRIM(x856.f3)
					REPLACE xpt.sforCSZ WITH m.CSZ IN xpt
				ENDIF
			ELSE
				IF TRIM(x856.segment) = "N3"  && address info
					REPLACE xpt.sforaddr1  WITH UPPER(ALLTRIM(x856.f1)) IN xpt
					REPLACE xpt.sforaddr2 WITH UPPER(ALLTRIM(x856.f2)) IN xpt
				ENDIF
				SELECT x856
				SKIP 1 IN x856
				IF TRIM(x856.segment) != "N4"
					LOOP
				ENDIF

				IF TRIM(x856.segment) = "N4"  && address info
					m.sforCSZ = UPPER(TRIM(x856.f1))+", "+UPPER(TRIM(x856.f2))+" "+TRIM(x856.f3)
					REPLACE xpt.sforCSZ WITH m.sforCSZ IN xpt
				ENDIF
			ENDIF

			SELECT x856
			SKIP
			LOOP

		CASE TRIM(x856.segment) = "N9" AND TRIM(x856.f1) = "DP"  && customer dept
			STORE "" TO m.dept,m.deptname
			m.dept = ALLTRIM(x856.f2)
			m.deptname = ALLTRIM(x856.f3)
			REPLACE xpt.dept WITH m.dept IN xpt
			REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"DEPTNAME*"+m.deptname IN xpt
			SELECT x856
			SKIP
			LOOP

		CASE TRIM(x856.segment) = "N9" AND TRIM(x856.f1) = "ST"  && store name for labels
			REPLACE xpt.shipfor WITH ALLTRIM(x856.f2)
			cStorename = "STORENAME*"+ALLTRIM(x856.f2)
			REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+cStorename IN xpt
			SELECT x856
			SKIP
			LOOP

		CASE TRIM(x856.segment) = "N9" AND ALLTRIM(x856.f1) = "CO"
			REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"SALESORDER*"+UPPER(ALLTRIM(x856.f2)) IN xpt
			REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"CUSTORDER*"+UPPER(ALLTRIM(x856.f3)) IN xpt
			SELECT x856
			SKIP
			LOOP

		CASE TRIM(x856.segment) = "N9" AND TRIM(x856.f1) = "12"  && Billing Acct #
			REPLACE xpt.upsacctnum WITH ALLTRIM(x856.f2) IN  xpt
			REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"BILLACCT*"+ALLT(x856.f2) IN xpt
			SELECT x856
			SKIP
			LOOP

		CASE TRIM(x856.segment) = "N9" AND TRIM(x856.f1) = "14"  && Master Acct #
			REPLACE xpt.cacctnum WITH ALLTRIM(x856.f2)IN xpt
			REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"CUSTACCT*"+ALLT(x856.f2) IN xpt
			DO CASE
				CASE ALLTRIM(x856.f2) ="52975000"
					REPLACE xpt.batch_num WITH "AAFESDROP"
				CASE ALLTRIM(x856.f2) ="52862000"
					REPLACE xpt.batch_num WITH "BODYBDROP"
				CASE ALLTRIM(x856.f2) ="51257000"
					REPLACE xpt.batch_num WITH "BOXBDROP"
				CASE ALLTRIM(x856.f2) ="50029999"
					REPLACE xpt.batch_num WITH "DICKSDROP"
				CASE ALLTRIM(x856.f2) ="51696000"
					REPLACE xpt.batch_num WITH "GOVXDROP"
				CASE ALLTRIM(x856.f2) ="51004000"
					REPLACE xpt.batch_num WITH "RUNNERDROP"
				CASE ALLTRIM(x856.f2) ="51003000"
					REPLACE xpt.batch_num WITH "SHOEBDROP"
				CASE ALLTRIM(x856.f2) ="54012000"
					REPLACE xpt.batch_num WITH "THQSDROP"
				CASE ALLTRIM(x856.f2) ="52145000"
					REPLACE xpt.batch_num WITH "EXPERDROP"
				CASE ALLTRIM(x856.f2) ="WEBPERF"
					REPLACE xpt.batch_num WITH "WEB"
			ENDCASE
			SELECT x856
			SKIP
			LOOP

		CASE TRIM(x856.segment) = "N9" AND TRIM(x856.f1) = "IA"  && Vendor number
			REPLACE xpt.vendor_num WITH ALLTRIM(x856.f2)IN xpt
			SELECT x856
			SKIP
			LOOP


		CASE TRIM(x856.segment) = "N9" AND TRIM(x856.f1) = "FOB"  && customer dept
			cFOB = ALLTRIM(x856.f2)
			REPLACE xpt.div WITH cFOB IN xpt
			REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"FOB*"+cFOB IN xpt
			SELECT x856
			SKIP
			LOOP

		CASE TRIM(x856.segment) = "N9" AND TRIM(x856.f1) = "PC"  && PO #
			IF ALLTRIM(x856.f2) = "01"
				REPLACE xpt.batch_num WITH "RUSH" IN  xpt
				REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"PRIORITY*RUSH" IN xpt
			ENDIF
			IF ALLTRIM(x856.f2) = "02"
				REPLACE xpt.batch_num WITH "DTOC " IN  xpt
				REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"PRIORITY*DTOC" IN xpt
			ENDIF
			IF ALLTRIM(x856.f2) = "03"
				REPLACE xpt.batch_num WITH "STD " IN  xpt
				REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"PRIORITY*STD" IN xpt
			ENDIF
			SELECT x856
			SKIP
			LOOP


		CASE TRIM(x856.segment) = "N9" AND TRIM(x856.f1) = "ZZ"  && PO #
			lnVasCtr = lnVasCtr+1
			ThisVAS = ALLTRIM(x856.f2)
			DO CASE
				CASE ThisVAS = "V01"
					REPLACE xpt.v01 WITH .T. IN xpt
					IF !"VASCODE*PACKINGLIST"$xpt.shipins
						REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"VASCODE*PACKINGLIST" IN xpt
					ENDIF
				CASE ThisVAS = "V02"
					REPLACE xpt.v02 WITH .T. IN xpt
					IF !"VASCODE*INSERT"$xpt.shipins
						REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"VASCODE*INSERT" IN xpt
					ENDIF
				CASE ThisVAS = "V05"
					REPLACE xpt.v05 WITH .T. IN xpt
					IF !"VASCODE*BARCODES"$xpt.shipins
						REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"VASCODE*BARCODES" IN xpt
					ENDIF
			ENDCASE

			IF lnVasCtr = 1
				IF !"VASFLAG*TRUE"$xpt.shipins
					REPLACE xpt.vas WITH .T.
					REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"VASFLAG*TRUE" IN xpt
				ENDIF
				lcVASStr = ALLTRIM(x856.f2)
				REPLACE xpt.vas WITH .T.
			ELSE
				lcVASStr= lcVASStr +","+ALLTRIM(x856.f2)
				REPLACE xpt.vas WITH .T.
			ENDIF

			SELECT x856
			SKIP
			LOOP


		CASE TRIM(x856.segment) = "G62" AND TRIM(x856.f1) == "53"  && start date
			lcDate = ALLTRIM(x856.f2)
			lxDate = SUBSTR(lcDate,5,2)+"/"+SUBSTR(lcDate,7,2)+"/"+LEFT(lcDate,4)
			ldDate = CTOD(lxDate)
			STORE ldDate TO dStart
			REPLACE xpt.START WITH ldDate IN xpt
			SELECT x856
			SKIP
			LOOP

		CASE TRIM(x856.segment) = "G62" AND TRIM(x856.f1) == "54"  && cancel date
			lcDate = ALLTRIM(x856.f2)
			lxDate = SUBSTR(lcDate,5,2)+"/"+SUBSTR(lcDate,7,2)+"/"+LEFT(lcDate,4)
			ldDate = CTOD(lxDate)
			STORE ldDate TO dCancel
			REPLACE xpt.CANCEL WITH ldDate IN xpt
			SELECT x856
			SKIP
			LOOP

		CASE TRIM(x856.segment) = "G62" AND TRIM(x856.f1) == "04"  && Order date
			lcDate = ALLTRIM(x856.f2)
			REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"ORDERDATE*"+lcDate IN xpt
			SELECT x856
			SKIP
			LOOP

		CASE TRIM(x856.segment) = "G62" AND TRIM(x856.f1) == "52"  && Order RCV date
			lcDate = ALLTRIM(x856.f2)
			REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"ORDERRCVDATE*"+lcDate IN xpt
			SELECT x856
			SKIP
			LOOP

		CASE TRIM(x856.segment) = "N9" AND ALLTRIM(x856.f1) = "PO"
			REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"N9PO*"+ALLTRIM(x856.f2) IN xpt
			SELECT x856
			SKIP
			LOOP

		CASE TRIM(x856.segment) = "NTE" AND TRIM(x856.f1) = "TSD"
			lnShipInsCtr=1
			DO WHILE TRIM(x856.segment) = "NTE" AND TRIM(x856.f1) = "TSD"
				REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"TSD"+ALLTRIM(TRANSFORM(lnShipInsCtr))+"*"+UPPER(ALLTRIM(x856.f2)) IN xpt
				SKIP 1 IN x856
			ENDDO
			SELECT x856
*      SKIP
			LOOP

		CASE TRIM(x856.segment) = "NTE" AND TRIM(x856.f1) = "PKG"
			lnShipInsCtr=1
			DO WHILE TRIM(x856.segment) = "NTE" AND TRIM(x856.f1) = "PKG"
				REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"PKG"+ALLTRIM(TRANSFORM(lnShipInsCtr))+"*"+UPPER(ALLTRIM(x856.f2)) IN xpt
				SKIP 1 IN x856
			ENDDO
			SELECT x856
*      SKIP
			LOOP

		CASE TRIM(x856.segment) = "NTE" AND TRIM(x856.f1) = "TRA"
			lnShipInsCtr=1
			DO WHILE TRIM(x856.segment) = "NTE" AND TRIM(x856.f1) = "TRA"
				REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"TRA"+ALLTRIM(TRANSFORM(lnShipInsCtr))+"*"+UPPER(ALLTRIM(x856.f2)) IN xpt
				SKIP 1 IN x856
			ENDDO
			SELECT x856
*      SKIP
			LOOP

		CASE TRIM(x856.segment) = "W66"
			lcShip_via= Upper(ALLTRIM(x856.f10))
			lcSCAC    = Upper(ALLTRIM(x856.f5))
			REPLACE xpt.scac WITH lcSCAC IN xpt
			REPLACE xpt.ship_via WITH lcShip_via IN xpt
			REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"SHIPVIA*"+lcShip_via IN xpt
			REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"SCAC*"+lcSCAC IN xpt
			SELECT x856
			SKIP
			LOOP

*!* Start of PT detail, stay here until this PT is complete
			WAIT WINDOW "NOW IN DETAIL LOOP PROCESSING" NOWAIT

		CASE TRIM(x856.segment) = "LX"
			m.units = .T.
			SELECT xptdet
			SKIP 1 IN x856

			lnVasCtr = 0
			lcVASStr = ""

			DO WHILE ALLTRIM(x856.segment) != "SE"
				cSegment = TRIM(x856.segment)
				cCode = TRIM(x856.f1)


				IF TRIM(x856.segment) = "W01" && Destination Quantity
					nptdetid = nptdetid + 1
					SELECT xptdet
					APPEND BLANK
					REPLACE xptdet.ptid       WITH xpt.ptid IN xptdet
					REPLACE xptdet.ptdetid    WITH nptdetid IN xptdet
					REPLACE xptdet.units      WITH m.units IN xptdet
					REPLACE xptdet.accountid  WITH xpt.accountid IN xptdet
					REPLACE xptdet.printstuff WITH "UOM*"+ALLT(x856.f2) IN xptdet

					m.upc   = ALLTRIM(x856.f7)
					m.style = UPPER(ALLTRIM(x856.f5))
					m.totqty= VAL(ALLTRIM(x856.f1))

					REPLACE xptdet.totqty  WITH m.totqty IN xptdet
					REPLACE xptdet.origqty WITH m.totqty IN xptdet
					IF ALLTRIM(x856.f8) =  "CB"
						REPLACE xptdet.custsku WITH ALLTRIM(x856.f9)
					ENDIF
					IF ALLTRIM(x856.f10) =  "IT"
						REPLACE xptdet.shipstyle WITH ALLTRIM(x856.f11)
					ENDIF

					IF ALLTRIM(x856.f12) =  "BO"
						REPLACE xptdet.printstuff WITH xptdet.printstuff+CHR(13)+"COLORNUMBER*"+ALLTRIM(x856.f13)
					ENDIF

					IF ALLTRIM(x856.f14) =  "IZ"
						REPLACE xptdet.printstuff WITH xptdet.printstuff+CHR(13)+"CUSTOMERSIZE*"+ALLTRIM(x856.f15)
					ENDIF

					IF ALLTRIM(x856.f16) =  "GQ"
						REPLACE xptdet.printstuff WITH xptdet.printstuff+CHR(13)+"GROUPCODE*"+ALLTRIM(x856.f17)
					ENDIF

					REPLACE xptdet.STYLE WITH m.style  IN xptdet
					REPLACE xptdet.upc   WITH m.upc    IN xptdet
					REPLACE xptdet.PACK  WITH "1"      IN xptdet
					REPLACE xptdet.printstuff WITH xptdet.printstuff+CHR(13)+"UNITCODE*"+ALLTRIM(x856.f2)
				ENDIF

				IF TRIM(x856.segment) = "G69" && shipstyle
					cPrtstr = "DESC*"+UPPER(TRIM(x856.f1))
					IF !("DESC"$xptdet.printstuff)
						REPLACE xptdet.printstuff WITH xptdet.printstuff+CHR(13)+cPrtstr IN xptdet
					ENDIF
				ENDIF

				IF TRIM(x856.segment) = "N9" AND ALLTRIM(x856.f1) = "LI"
					REPLACE xptdet.linenum WITH ALLTRIM(x856.f2) IN xptdet
				ENDIF

				IF TRIM(x856.segment) = "N9" AND ALLTRIM(x856.f1) = "VC"
					lcColor = ALLTRIM(x856.f2)
					m.color = ALLTRIM(x856.f2)
					REPLACE xptdet.COLOR WITH m.color  IN xptdet
					REPLACE xptdet.printstuff WITH xptdet.printstuff+CHR(13)+"COLOR*"+ALLTRIM(lcColor) IN xptdet
				ENDIF

				IF TRIM(x856.segment) = "N9" AND ALLTRIM(x856.f1) = "VS"
					m.id = ALLTRIM(x856.f2)
					REPLACE xptdet.ID WITH m.id IN xptdet
				ENDIF

				IF TRIM(x856.segment) = "AMT" AND ALLTRIM(x856.f1) = "LI"
					cPrtstr = "AMT*"+UPPER(TRIM(x856.f2))
					REPLACE xptdet.printstuff WITH xptdet.printstuff+CHR(13)+cPrtstr IN xptdet
				ENDIF

				IF TRIM(x856.segment) = "N9"  AND ALLTRIM(x856.f1) = "ZZ"
					lnVasCtr = lnVasCtr+1
					ThisVAS = ALLTRIM(x856.f2)
					DO CASE
						CASE ThisVAS = "V01"
							REPLACE xpt.v01 WITH .T. IN xpt
							IF !"VASCODE*PACKINGLIST"$xpt.shipins
								REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"VASCODE*PACKINGLIST" IN xpt
							ENDIF
						CASE ThisVAS = "V02"
							REPLACE xpt.v02 WITH .T. IN xpt
							IF !"VASCODE*INSERT"$xpt.shipins
								REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"VASCODE*INSERT" IN xpt
							ENDIF
						CASE ThisVAS = "V05"
							REPLACE xpt.v05 WITH .T. IN xpt
							IF !"VASCODE*BARCODELABELS"$xpt.shipins
								REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"VASCODE*BARCODELABELS" IN xpt
							ENDIF
					ENDCASE
				ENDIF

				SKIP 1 IN x856

				IF ALLTRIM(x856.segment) = "LX"
					IF !EMPTY(lcVASStr)
						REPLACE xptdet.printstuff WITH xptdet.printstuff+CHR(13)+"ALLVAS*"+ALLTRIM(lcVASStr) IN xptdet
					ENDIF
					lnVasCtr = 0
					lcVASStr = ""
				ENDIF

			ENDDO

			m.shipins =""
			lnVasCtr = 0
			lcVASStr = ""

			SELECT xptdet
	ENDCASE

	SELECT x856
	IF !EOF()
		SKIP 1 IN x856
	ENDIF
ENDDO

tu("ackdata")

SELECT xptdet
set step On  && added in the new roll up 12/27/2017  PG
Do rollup_ptdet_units
set step on

*WAIT WINDOW "At end of ROLLUP procedure" TIMEOUT 2

*********** added by PG 09/23/2014, somehow carcode gets set to "UPS", this should fix it
SELECT xpt
GOTO TOP
REPLACE ALL carrcode WITH rout_code

DO m:\dev\prg\setuppercase940

IF lTesting
*SET STEP ON
ENDIF

IF lBrowfiles
	WAIT WINDOW "If tables are incorrect, CANCEL when XPTDET window opens"+CHR(13)+"Otherwise, will load PT table" TIMEOUT 2
	SELECT xpt
	LOCATE
	BROWSE && TIMEOUT 10
	SELECT xptdet
	LOCATE
	BROWSE
ENDIF

IF lreload
	SELECT xpt
	DELETE FOR !INLIST(xpt.ship_ref,"0082571867","0082571891","0082571961","0082571975","0082572034")
	SELECT xpt
	BROWSE
ENDIF



SELECT xpt
SCAN
	SELECT xptdet
	SUM totqty TO xtotal FOR xptdet.ptid = xpt.ptid
	REPLACE xpt.qty WITH xtotal IN xpt
	REPLACE xpt.origqty WITH xtotal IN xpt
ENDSCAN



&& Added the following to attempt to trap PT records with missing detail
SELECT xpt
SCAN
	SELECT xptdet
	LOCATE FOR xptdet.ptid = xpt.ptid
	IF !FOUND()
		SET STEP ON
		tsubjectqq = "2XU 940 did not load all PT Detail, File "+cFilename
		tattach  = ""
		tsendtoqq  = tsendtoerr
		tccqq = tccerr
		tmessageqq = "File "+xfile+" was missing PT detail during the upload process...reload this file locally"
		tmessageqq = tmessageqq+CHR(13)+"(Error occurred at "+TTOC(DATETIME())
		tFrom ="Toll EDI Operations <toll-edi-ops@tollgroup.com>"
		DO FORM m:\dev\frm\dartmail2 WITH tsendtoqq,tFrom,tsubjectqq,tccqq,tattach,tmessageqq,"A"
		lOK = .F.
		RETURN
	ENDIF
ENDSCAN

WAIT WINDOW cCustname+" Breakdown Round complete..." TIMEOUT 2
SELECT x856
WAIT CLEAR

*************************************************************************************************
*!* Error Mail procedure - W01*12 Segment
*************************************************************************************************
PROCEDURE errormail1

	IF lEmail
		SELECT 0
		USE F:\3pl\DATA\mailmaster ALIAS mm SHARED
		LOCATE FOR mm.edi_type = "MISC" AND mm.taskname = "MODSHOEERR"
		STORE TRIM(IIF(mm.use_alt,mm.sendtoalt,mm.sendto)) TO tsendto
		STORE TRIM(IIF(mm.use_alt,mm.ccalt,mm.cc)) TO tcc
		USE IN mm
		cMailLoc = "Louisville, Kentucky"
		tsubject= "TGF "+cMailLoc+" "+cCustname+" Pickticket Error: " +TTOC(DATETIME())
		tmessage = "File: "+cFilename+CHR(13)+" is missing W01*12 segment(s), causing a divide by zero error."
		tmessage = tmessage + "Please re-create and resend this file."
		DO FORM m:\dev\frm\dartmail2 WITH tsendto,tFrom,tsubject,tcc,tattach,tmessage,"A"
	ENDIF
ENDPROC

*************************************************************************************************
*!* Error Mail procedure - only for QVC UPC issues
*************************************************************************************************
PROCEDURE errormail_qvc
	PARAMETERS lcstyle

*  tsendto ="pgaidis@fmiint.com,juan@fmiint.com,maria.estrella@tollgroup.com,joe.bianchi@tollgroup.com"
	tsendto ="pgaidis@fmiint.com"
	tcc=""
	tattach  = ""
	tsubject= "Ariat QVC UPC Issue Pickticket Error: " +TTOC(DATETIME())
	tmessage = "Missing UPC for :"+lcstyle+CHR(13)
	tmessage = tmessage + "Please make corrections and reload file: "+cFilename
	DO FORM m:\dev\frm\dartmail2 WITH tsendto,tFrom,tsubject,tcc,tattach,tmessage,"A"

ENDPROC
*************************************************************************************************
*!* Error Mail procedure - Cancel Before Start Date
*************************************************************************************************
PROCEDURE errordates

	IF lEmail
		tsendto = tsendtotest
		tcc =  tcctest
		tattach  = ""
		cMailLoc = "Louisville, Kentucky"
		tsubject= "TGF "+cMailLoc+" "+cCustname+" Pickticket Error: " +TTOC(DATETIME())
		tmessage = "File: "+cFilename+CHR(13)+" has Cancel date(s) which precede Start date(s)"
		tmessage = tmessage + "File needs to be checked and possibly resubmitted."
		DO FORM m:\dev\frm\dartmail2 WITH tsendto,tFrom,tsubject,tcc,tattach,tmessage,"A"
	ENDIF
ENDPROC

************************************************************************************************************************
PROCEDURE segmentget
****************************
	PARAMETER thisarray,lcKey,nLength

	FOR i = 1 TO nLength
		IF i > nLength
			EXIT
		ENDIF
		lnEnd= AT("*",thisarray[i])
		IF lnEnd > 0
			lcThisKey =TRIM(SUBSTR(thisarray[i],1,lnEnd-1))
			IF OCCURS(lcKey,lcThisKey)>0
				RETURN SUBSTR(thisarray[i],lnEnd+1)
				i = 1
			ENDIF
		ENDIF
	ENDFOR

	RETURN ""
************************************************************************************************************************
