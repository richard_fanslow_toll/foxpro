runack("SFTPFOLDERSCAN")

DO m:\dev\prg\_setvars WITH .T.

TRY
	WITH _SCREEN
		.TOP = 70
		.LEFT = 450
		.WINDOWSTATE = 0
		.BORDERSTYLE = 1
		.WIDTH = 460
		.HEIGHT = 200
		.CLOSABLE = .F.
		.MAXBUTTON = .F.
		.SCROLLBARS = 0
		.CAPTION = "SFTP Uploads Folder Checker"
	ENDWITH

	DO FORM m:\dev\FRM\SFTPFOLDERSCAN

CATCH TO oErr

		tmessage = "SFTP Folder Scan Error"+CHR(13)

		tmessage =tmessage+ "Try/CATCH Exeception Message:"+CHR(13)+;
			[  Error: ] + STR(oErr.ERRORNO) +CHR(13)+;
			[  LineNo: ] + STR(oErr.LINENO) +CHR(13)+;
			[  Message: ] + oErr.MESSAGE +CHR(13)+;
			[  Procedure: ] + oErr.PROCEDURE +CHR(13)+;
			[  Details: ] + oErr.DETAILS +CHR(13)+;
			[  StackLevel: ] + STR(oErr.STACKLEVEL) +CHR(13)+;
			[  LineContents: ] + oErr.LINECONTENTS+CHR(13)+;
			[  UserValue: ] + oErr.USERVALUE
			

		tsubject = "SFTP Uploads Folder Checker Error at "+TTOC(DATETIME())
		tfrom    = "mark.bennett@tollgroup.com"
		tsendto  = "mbennett@fmiint.com"
		tcc      = ""
		tattach  = ""
		
		DO FORM m:\dev\frm\dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
FINALLY
	ON ERROR
	SET STATUS BAR ON
ENDTRY
