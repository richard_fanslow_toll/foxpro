***********************************************************************
** this prg use to have both PA and ASIN, split out on 4/16/2018  PG
*************************************************************************
Set Deleted On
Set Escape On
Set enginebehavior 70
Set Safety Off
Set Date Dmy
Set Century On
Set Date AMERICAN

Do setenvi
utilsetup("UA_PO_UPLOAD")

Public RecPtr
Public gcDocNumber
Public thisfile
Public xfile

gcDocNumber=""
RecPtr =0

Close Data All

xrunall=.T.

xaccountid=5687
xacctname="UNDER ARMOUR"

xtype = "PO"
*xtype = "ASN"

If xtype = "ASN"
  lcpath="f:\ftpusers\underarmour\asnin\"
  lcholdpath="f:\ftpusers\underarmour\asnin\newhold\"
  lcarchivepath="f:\ftpusers\underarmour\asnin\archive\"
Endif

If xtype = "PO"
  lcpath="f:\ftpusers\underarmour\poin\"
  lcarchivepath="f:\ftpusers\underarmour\poin\archive\"
  lcholdpath="f:\ftpusers\underarmour\asnin\newhold\"
Endif


If !xrunall
  lnnum=1
*  cfilename = alltrim(getfile())
  cfilename=Alltrim(lcarchivepath+"manh_tpm_shipment0004192734.xml ")
Else
  lnnum = Adir(tarray,lcpath+"*.xml")
  If lnnum = 0
    Return
  Endif
Endif

Create Cursor csrpodata (;
ponum Char(25),;
start d,;
cancel d,;
name Char(25),;
name2 Char(25),;
street Char(25),;
street2 Char(25),;
city Char(25),;
state Char(2),;
zip Char(10);
)
#Define Tab Chr(0x9)

Create Cursor orderdetailsdata (;
ibd     Char(20),;
ponum   Char(25),;
linenum Char(10),;
odata   Memo)

Create Cursor xmlloaded (;
container char(20),;
ibd     Char(20),;
ponum   Char(25),;
whseloc Char(10),;
shipid Char(10),;
qty     Int )

use f:\wo\wodata\detail in 0
use f:\underarmour\data\uadetail in 0
select * from uadetail where .f. into cursor uatemp readwrite
select uatemp
scatter memvar memo blank


useca("ctnucc","wh")
Select * From ctnucc Where .F. Into Cursor uaucctemp Readwrite
Scatter Memvar Memo Blank

Use F:\underarmour\Data\podata In 0

llclosetables = .F.
*!*  If !Used("shiphdr")
*!*    Use F:\underarmour\Data\shiphdr In 0
*!*    Use F:\underarmour\Data\cartons In 0
*!*    llclosetables = .T.
*!*  Endif

*!*  Use F:\underarmour\Data\ShipMfst In 0

xsqlexec("select * from dellocs",,,"wo")
index on accountid tag accountid
index on str(accountid,4)+location tag acct_loc
index on str(accountid,4)+div tag acct_div
set order to

Select 200
Create Cursor temp1 (field1 c(254))
Select temp1

xdateloaded=Dtot({})
*Set Step On



For thisfile = 1  To lnnum

  Select 200
  Create Cursor temp1 (field1 c(254))
  Select temp1

  xfile = lcpath+tarray[thisfile,1] && +"."
  xholdfile = lcholdpath+tarray[thisfile,1] && +"."
  cfilename = Alltrim(Lower(tarray[thisfile,1]))
  ctns=0

**issues with dateloaded being same for multiple files - mvw 11/30/10
  Do While xdateloaded=Datetime()
* Wait Timeout 1
  Enddo
  xdateloaded=Datetime()

  Wait Window At 10,10 "uploading file: "+xfile Nowait

  lcStr = Filetostr(xfile)
  lnAt = At(Chr(9),lcStr)
 
 If lnAt >0
    lcStr = Strtran(lcStr,Chr(9),"")
    Strtofile(lcStr,"h:\fox\text.xml")
    cchar=Chr(13)+Chr(10)
    Append From "h:\fox\text.xml" Type Delimited With Character &cchar
    Replace All field1 With Alltrim(field1)
    Locate
  Else
    lcStr = Strtran(lcStr,"><",">"+Chr(13)+"<")
    Strtofile(lcStr,"h:\fox\text.xml")
    cchar=Chr(13)+Chr(10)
    Append From "h:\fox\text.xml" Type Delimited With Character &cchar
    Replace All field1 With Alltrim(field1)
    Locate
  Endif

 If lnAt >0 And Reccount("temp1") = 2
    lcStr = Filetostr(xfile)
    lcStr = Strtran(lcStr,"><",">"+Chr(13)+"<")
    Strtofile(lcStr,"h:\fox\text.xml")
    cchar=Chr(13)+Chr(10)
    Append From "h:\fox\text.xml" Type Delimited With Character &cchar
    Replace All field1 With Alltrim(field1)
    Locate
 Endif 


  Strtofile(lcStr,"h:\fox\text.xml")
  cchar=Chr(13)+Chr(10)
  Append From "h:\fox\text.xml" Type Delimited With Character &cchar
  Replace All field1 With Alltrim(field1)
  Locate

  GotoTag("<DocType")

  Do Case
  Case ">PO<"$field1
    Doctype = "PO"
    Select temp1
    Go Top
  Case ["STANDARD">ASN</DocType>]$field1
    Doctype = "ASN"
    DocVariant = "STANDARD"
    Select temp1
    Go Top
  Case ["RESERVED">ASN</DocType>]$field1
    Doctype = "ASN"
    DocVariant = "RESERVED"
    Select temp1
    Go Top
  Otherwise
    Doctype = "ASN"
    Messagebox("Invalid XML type",0,"UA XML Upload")
    Return
  Endcase

  If Doctype = "PO"
    new_parse_po()
    archivefile  = (lcarchivepath+cfilename)
    If !File(archivefile)
      Copy File [&Xfile] To [&archivefile]
    Endif
    If File(xfile)
      Delete File [&xfile]
    Endif
*   Loop
  Endif

*!*    If Doctype = "ASN"
*!*      Select orderdetailsdata
*!*      Select temp1
*!*      parse_newasn2()
*!*      archivefile  = (lcarchivepath+cfilename)
*!*      If !File(archivefile)
*!*        Copy File [&Xfile] To [&archivefile]
*!*      Endif
*!*      If File(xfile)
*!*        Delete File [&xfile]
*!*      Endif
*!*  *   LOOP
*!*    Endif
Next

If xtype = "ASN"
  tfrom    ="TOLL EDI Processing Center <fmi-transload-ops@fmiint.com>"
  tsendto  = "Kristen.Floeck@tollgroup.com,Joe.Abbate@tollgroup.com,adonis.perez@tollgroup.com,isis.gonzalez@tollgroup.com"
  tsubject = "XML Data uploaded from UA........."
  tcc= "pgaidis@fmiint.com"
  tattach =""
  tmessage = ""
  Select xmlloaded
  Scan
    tmessage = tmessage+Alltrim(xmlloaded.container)+" - "+Alltrim(xmlloaded.ibd)+" - "+Alltrim(xmlloaded.ponum)+" - "+Alltrim(xmlloaded.whseloc)+" - "+Alltrim(xmlloaded.shipid)+" - "+Transform(xmlloaded.qty)+Chr(13)
  Endscan
  Do Form m:\dev\FRM\dartmail2 With tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
Endif

schedupdate()

*************************************************************************************************************
Procedure new_parse_po

done = .F.

Do While !done

  GotoTag("<Orders")

  GotoTag("<Order>")
  m.ponum=getfield(field1,[Order>],"<")

  GotoTag("<SoldTo")
  GotoTag("<ID")    && get Hawb
  m.soldtoID=getfield(field1,[ID>],"<")

  GotoTag("<ShipTo")
  GotoTag("<ID")    && get Hawb
  m.shiptoID=getfield(field1,[ID>],"<")


  GotoTag("<Name")    && get Hawb
  m.name=Strtran(Upper(getfield(field1,[Name>],"<")),"&APOS;","")

  GotoTag("<Name2")    && get Hawb
  m.name2=Upper(getfield(field1,[Name2>],"<"))
  If m.name2 = "NA"
    m.name2=""
  Endif

  GotoTag("<Street")    && get Hawb
  m.street=Upper(getfield(field1,[Street>],"<"))

  GotoTag("<Street2")    && get Hawb
  m.street2=Upper(getfield(field1,[Street2>],"<"))
  If m.street2 = "NA"
    m.street2=""
  Endif

  GotoTag("<City")    && get Hawb
  m.city=Upper(getfield(field1,[City>],"<"))

  GotoTag("<State")    && get Hawb
  m.state=Upper(getfield(field1,[State>],"<"))

  GotoTag("<PostalCode")    && get Hawb
  m.zip=Upper(getfield(field1,[PostalCode>],"<"))

  GotoTag([<Date DateQualifier="ReqDelDate">])
  lcDate=getfield(field1,[Date DateQualifier="ReqDelDate">],"<")
  lxDate = Substr(lcDate,5,2)+"/"+Substr(lcDate,7,2)+"/"+Left(lcDate,4)
  m.start = Ctod(lxDate)

  GotoTag([<Date DateQualifier="CancelDate">])
  lcDate=getfield(field1,[Date DateQualifier="CancelDate">],"<")
  lxDate = Substr(lcDate,5,2)+"/"+Substr(lcDate,7,2)+"/"+Left(lcDate,4)
  m.cancel = Ctod(lxDate)

  Select csrpodata
  Insert Into csrpodata From Memvar
  Select podata
  Locate For podata.ponum = csrpodata.ponum
  If !Found()
    m.filename = xfile
    Insert Into podata From Memvar
  Else
    Select podata
    Delete For  podata.ponum = csrpodata.ponum
    m.filename = xfile
    Insert Into podata From Memvar
  Endif

  done=.T.
  Exit

Enddo

Select csrpodata
Zap

Endproc
*****************************************************************************
Procedure getCtnRecord

Select uaucctemp
Append Blank
m.asnfile = Justfname(xfile)
m.accountid= 5687
m.loaddt = Date()
Gather Memvar Memo
Replace orddetails With orderdetailsdata.odata In uaucctemp
m.suppdata = ""
Select temp1
Endproc
*************************************************************************************************************

**************************************************************************
Procedure getfield
Parameters sdata,match,enddata

lnstart = At(match,sdata)
firstquote = lnstart+Len(match)
cc = Substr(sdata,firstquote,1)
If cc ="<"
* ? "NA"
  Return "NA"
Endif
If Len(Alltrim(sdata)) = Len(Alltrim(match))+1
* ? "NA"
  Return "NA"
Endif

lnsecond = firstquote+1
Do While Substr(sdata,lnsecond,1)!=enddata
  lnsecond = lnsecond+1
Enddo

? "Data: "+Substr(sdata,firstquote,lnsecond-(firstquote))

Return Substr(sdata,firstquote,lnsecond-(firstquote))

Endproc


*************************************************************************************************************
Procedure getfield
Parameters sdata,match,enddata

lnstart = At(match,sdata)
firstquote = lnstart+Len(match)
cc = Substr(sdata,firstquote,1)
If cc ="<"
* ? "NA"
  Return "NA"
Endif
If Len(Alltrim(sdata)) = Len(Alltrim(match))+1
* ? "NA"
  Return "NA"
Endif

lnsecond = firstquote+1
Do While Substr(sdata,lnsecond,1)!=enddata
  lnsecond = lnsecond+1
Enddo

*? "Data: "+Substr(sdata,firstquote,lnsecond-(firstquote))

Return Substr(sdata,firstquote,lnsecond-(firstquote))

Endproc

**************************************************************************
Procedure GotoTag
Parameters tagname,stoptag

lnThisRec = Recno("temp1")

*? "lloking for TAG: "+tagname
xtagfilter=Iif(Empty(stoptag),"","and FIELD1!="+stoptag)

xxRec = 1
Do While field1!= tagname &&xtagfilter
  Skip 1 In temp1
  xxRec = xxRec+1
  If xxRec > 1000 or eof('temp1')
	if tagname="<Carton"
		return .f.
	endif
	
    Select temp1
    Goto lnThisRec
    Set Step On
  Endif
Enddo
Endproc

**************************************************************************
Procedure FindTag
Parameters tagname,stoptag
Do While field1!= tagname
  Skip 1 In temp1
Enddo
Endproc
**************************************************************************
Procedure getRecord
Select uatemp
Append Blank
m.filename = xfile
m.dateloaded=Date()
m.accountid= 5687
m.qty_type="CARTONS"
m.loose = .F.
m.type = "O"
m.uploadtm=Datetime()
m.acctname ="UNDER ARMOUR"
Gather Memvar Memo
Select temp1
Endproc
**************************************************************************
Procedure getCtnRecord

Select uaucctemp
Append Blank
m.asnfile = Justfname(xfile)
m.accountid= 5687
m.loaddt = Date()
Gather Memvar Memo
Replace orddetails With orderdetailsdata.odata In uaucctemp
m.suppdata = ""
Select temp1
Endproc
**************************************************************************

