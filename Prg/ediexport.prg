**ediexport.prg
**
**Description: Main prg for ediexport proj, which processes several outbound edi document types
**Developer: Mike Winter
**
set sysmenu off
set margin to 0
set menu off
set safety off
set notify off
set status off
set status bar off
set exclusive off
set escape off
set cpdialog off
set delete on
set exact off
set century off
set enginebehavior 70
set talk off

runack("EDIEXPORT")

_screen.Caption="Edi Export Processing"

**run the doemstic xml files to Icon process (drayage and outbound mfsts)
do domtoicon

wait "Processing AE 850 Files..." window nowait noclear
do aetotoll850

wait "Processing PVH 850 Files..." window nowait noclear
do pvhtotoll850

**calderon turned on per Edmond - 7/23/13 mvw
**turned on per Ryan Szeto, still in testing (UAT) mode - 05/14/14
*do caldtotoll850

wait "Processing Calderon 850 Files..." window nowait noclear
Do calderon_parse_850  && new Calderon PO software went live  7/09/2015  PG

**sears redeployment file creation - mvw 07/22/13
wait "Processing Sears Redeployment Files..." window nowait noclear
do searsretfile

**albertsons 850 to ICON - 11/07/13 mvw
wait "Processing Albertson's 850 Files..." window nowait noclear
do albertsons850

**polo 850 to ICON - mvw 09/12/14
wait "Processing Polo 850 Files..." window nowait noclear
do prltotoll850

wait clear

schedupdate()
