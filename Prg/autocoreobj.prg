**autoCoreObj.prg
**
parameters cbuilding, ltest
**removed the MOD - no longer used and complex code including acctgrp, etc. - mvw 12/22/15
**cBuilding = NJ, MOD D, MOD C, MOD B, MOD A, MORETGROUP

_screen.Caption = "Auto Core Objectives - Building "+alltrim(cbuilding)

xsqlexec("select * from account where inactive=0","account",,"qq")
index on accountid tag accountid
set order to

xsqlexec("select * from offices",,,"wo")
index on locale tag locale
set order to

USE f:\hr\hrdata\holidays In 0
USE f:\kpi\data\emailAddr In 0

Set Sysmenu off

lCC = iif(ltest,.f.,.t.) &&for testing purposes

Select emailAddr
cAddrFilter = Iif(lTest, "test = .t.", "building = '"+cbuilding+"'")
Locate for report = "Core Objectives" and &cAddrFilter

If !Found()
  Return
EndIf

**removed the MOD - no longer used and complex code including acctgrp, etc. - mvw 12/22/15
*!*	If cBuilding="MOD"
*!*	  cMod = cBuilding 
*!*	  cBuilding = ""
*!*	EndIf

cEmailList = emailAddr.toAddrs
cCCList = Iif(!lCC, "", emailAddr.ccAddrs)
DO case
  Case cBuilding = "NJ"
	cTo = "mwinter@fmiint.com"
	cCc = ""
	cAttach = ""
	cFrom ="TGF WMS Operations <fmicorporate@fmiint.com>"
	cSubject = "NJ Core Objectives Started"
	cMessage = "Sent from autocoreobj.prg"
	DO FORM dartmail2 WITH cTo,cFrom,cSubject,cCc,cAttach,cMessage,"A"

	use f:\kpi\kpidata\kpilog in 0
	insert into kpilog (zdate, kpirpt, step1) values (datetime(), "Core Objectives", .t.)

	**run for both mods in NJ - 11/22/11 mvw
	DO kpi.fxp with "daily core objectives", "", "All Accounts", ;
		"N", Iif(Dow(Date())= 2, Date()-3, Date()-1), {}, .t., cEmailList, cCCList, "I"

	DO kpi.fxp with "daily core objectives", "", "All Accounts", ;
		"N", Iif(Dow(Date())= 2, Date()-3, Date()-1), {}, .t., cEmailList, cCCList, "J"
	
	use in kpilog

  Case cBuilding = "MORET"
	DO kpi.fxp with "daily core objectives", "", "All Accounts", ;
		"L", Iif(Dow(Date())= 2, Date()-3, Date()-1), {}, .t., cEmailList, cCCList

  Otherwise
	**removed the MOD - no longer used and complex code including acctgrp, etc. - mvw 12/22/15
*!*		DO kpi.fxp with "daily core objectives", "", "All Accounts", ;
*!*			"C", Iif(Dow(Date())= 2, Date()-3, Date()-1), {}, .t., cEmailList, cCCList, cBuilding, cMod
	DO kpi.fxp with "daily core objectives", "", "All Accounts", ;
		"C", Iif(Dow(Date())= 2, Date()-3, Date()-1), {}, .t., cEmailList, cCCList, cBuilding
EndCase

Wait clear

