**tlSetup.prg
**
**call to set_envi to set up the environment
**creates necessary public variables
**
DO set_envi &&sets system settings, such as ENGINEBEHAVIOR, etc.

PUBLIC tempFox, cTlData, cQqData, cDataDrive, cSharedDrive, ;
	   cInDir, cOutDir, cFmiInDir, cTraceDir, gcMsgTitle, gnCbmMax, gnWtMax

tempfox = "h:\fox\"
IF !DIRECTORY(tempFox)
  MKDIR &tempFox
ELSE
  cOnError = On("error")
  On ERROR o=0
  DELETE FILE &tempfox.*.*
  On Error &cOnError
ENDIF

cDataDrive = "f:\"
cSharedDrive = "v:\"
Store cDataDrive+"transload\tldata\" TO cTlData
Store cDataDrive+"sysData\qqData\"   TO cQqData
Store cSharedDrive+"wcs\runTime\inbound\" TO cInDir
Store cSharedDrive+"wcs\runTime\outbound\" TO cOutDir
Store cSharedDrive+"wcs\runTime\fmiImport\" TO cFmiInDir
Store cSharedDrive+"wcs\runTime\outbound_trace\" TO cTraceDir

Store 80 to gnCbmMax
Store 39000 to gnWtMax

DO macros WITH cDataDrive

utilsetup("TL")

STORE "Transload System" TO gcMsgTitle

SET SYSMENU OFF
_screen.BackColor = RGB(0,128,64)
_screen.caption = "Transload System"
_screen.windowState = 2
