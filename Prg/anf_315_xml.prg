PARAMETERS xwo_num,xcode

* 02/28/2018 MB: added code to subtract 3 hours from the event time if the Office is C, IF WE USED DATETIME() AS THE EVENT TIME.  
* This was requested by Bill Lillicrap because they are sometimes seeing 'datetimes in the future'.
LOCAL lcOffice, lnHours, lcDate
lcOffice = ''

* added this so we can restore the settings to what the calling program had, after changing them in here 4/5/2018 MB
lnHours = SET('hours') 
lcDate = SET('date')

SET HOURS TO 24  && added 4/5/2018 because it was coming in as 12 and the code below was not identifying it as am or pm in the event date/time - so the date/times were very misleading.
SET DATE YMD

*xcode = "OUTGATE"
*xcode = "RETURN"

SET STEP ON 
*SET HOURS TO 12   && we want hours to be 24 4/10/2018 MB

IF !USED("wolog")
	USE F:\wo\wodata\wolog IN 0
ENDIF

IF !USED("tdaily")
	USE F:\wo\wodata\tdaily IN 0
ENDIF


SELECT wolog
LOCATE FOR xwo_num = wolog.wo_num
IF !FOUND()

ELSE
	lcContainer = wolog.CONTAINER
	lcBol       = wolog.brokref
	lcOffice 	= wolog.office  && 2/28/2018 MB
ENDIF

*set step On

SELECT tdaily

lcEventTime = DATETIME()

* added 3/27/2018 MB
IF lcOffice = 'C' THEN
	* subtract 3 hours from the east coast time
	lcEventTime = lcEventTime - (60*60*3)
ENDIF

DO CASE
	CASE xcode = "OUTGATE"
		SELECT tdaily
		LOCATE FOR wo_num = xwo_num AND action = "PU"
		IF FOUND()
			lcEventTime = tdaily.confirmdt
		ELSE
			lcEventTime = DATETIME()
			* 2/28/2018 MB
			IF lcOffice = 'C' THEN
				* subtract 3 hours from the east coast time
				lcEventTime = lcEventTime - (60*60*3)
			ENDIF
		ENDIF

	CASE xcode = "RETURN"
		SELECT tdaily
		LOCATE FOR wo_num = xwo_num AND action = "MT"
		IF FOUND()
			lcEventTime = tdaily.confirmdt
		ELSE
			lcEventTime = DATETIME()
			* 2/28/2018 MB
			IF lcOffice = 'C' THEN
				* subtract 3 hours from the east coast time
				lcEventTime = lcEventTime - (60*60*3)
			ENDIF
		ENDIF

ENDCASE


*SET DATE YMD  && MOVED UP HIGHER 4/5/2018 mb

lcEventTime = STRTRAN(TTOC(lcEventTime ),"/","-")
lcEventTime = STRTRAN(lcEventTime," ","T")
lcEventTime = STRTRAN(lcEventTime,"TPM","")
lcEventTime = ALLTRIM(STRTRAN(lcEventTime,"TAM",""))

lcHeader = FILETOSTR("f:\anf\data\anf_214_template.txt")

lcHeader = STRTRAN(lcHeader,"<EVENTTIME>",lcEventTime)
lcHeader = STRTRAN(lcHeader,"<CONTAINER>",ALLTRIM(lcContainer))
lcHeader = STRTRAN(lcHeader,"<BOLNUMBER>",ALLTRIM(lcBol))

**<CONTAINERRETURNTIME>

DO CASE
	CASE xcode = "OUTGATE"
		lcHeader = STRTRAN(lcHeader,"<EVENTTYPE>","GOU")
		lcHeader = STRTRAN(lcHeader,"<JOBCONTAINERTYPE>","JobContainer.JC_FCLWharfGateOut")
		lcHeader = STRTRAN(lcHeader,"<CONTAINERRETURNTIME>",lcEventTime)
		lcEventCode="GOU"
	CASE xcode = "RETURN"
		lcHeader = STRTRAN(lcHeader,"<EVENTTYPE>","DHR")
		lcHeader = STRTRAN(lcHeader,"<JOBCONTAINERTYPE>","JobContainer.JC_ContainerYardEmptyReturnGateIn")
		lcHeader = STRTRAN(lcHeader,"<CONTAINERRETURNTIME>",lcEventTime )
		lcEventCode="DHR"
ENDCASE

*!* For 214 file types:

lcFilestamp = TTOC(DATETIME())
lcFilestamp = STRTRAN(lcFilestamp,"/","")
lcFilestamp = STRTRAN(lcFilestamp,":","")
lcFilestamp = STRTRAN(lcFilestamp," ","")

&&TGFSCS_MRKU0065216_GOU_20160922112543.xml

Filename = "f:\ftpusers\anf\tgf315\TGFSCS_"+ALLTRIM(lcContainer)+"_"+ALLTRIM(xcode)+"_"+lcFilestamp+".xml"
* for testing
*Filename = "c:\a\TGFSCS_"+ALLTRIM(lcContainer)+"_"+ALLTRIM(xcode)+"_"+lcFilestamp+".xml"

STRTOFILE(lcHeader,Filename)

* 4/5/2018 Mb
SET HOURS TO lnHours
SET DATE &lcDate

ENDPROC
