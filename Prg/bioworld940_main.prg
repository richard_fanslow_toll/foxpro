*!* m:\dev\prg\bioworld940_main.prg

PARAMETERS cOfficeIn
CLOSE DATA ALL
PUBLIC nAcctNum,cDelimiter,nRun_num,cTranslateOption,cWhichFile,cOffice,cCustname,cPropername,cStorenum
PUBLIC cFilter,nRunID,lComplete,lTesting,m.UCCNUMBER,lEmail,lTestImport,emailcommentstr,cUseFolder
PUBLIC ptid,ptctr,ptqty,xfile,cfilename,UploadCount,pickticket_num_start,pickticket_num_end,nFileCount,units
PUBLIC cOldScreen,tsendto,tcc,lcPath,lcArchivePath,nPTQty,cPTQty,archivefile,lBrowfiles,cISA_num,gMasteroffice,lHotTopic
PUBLIC tsendtoerr,tccerr,cOfficeLoc,ldeletefile,cErrMsg,lJCP,lJCPMail,lN2,m.custsku,lLoadSQL,cMod,nFileSize,nPTQtyRec
PUBLIC ARRAY a856(1)

PUBLIC message_timeout,message_timeout2
*SET STEP ON 
message_timeout ="nowait"
message_timeout2 ="timeout 2"

TRY
	cErrMsg = ""
	cISA_num = ""
	nPTQtyRec = 0

	lTesting = .f.
	lTestImport = lTesting
	lOverridebusy = lTesting && Will override FTPSETUP chkbusy flag if .t.
	lBrowfiles = lTesting  && Will allow browsing of XPT/XPTDET cursors before upload if .t.
	lEmail = !lTesting
	
*!*		lBrowfiles = .t.
*!*		lOverridebusy = lBrowfiles
*!*		lEmail = .F.
*!*		SET ASSERTS OFF

	IF VARTYPE(cOfficeIn)="L"
		cOfficeIn = "L"
		cOffice = "L"
		cOfficeLoc = "ML"
	ENDIF


	STORE "" TO cOldScreen
	STORE _SCREEN.CAPTION TO cOldScreen
	cCustname = "BIOWORLD"
	cUseName = "BIOWORLD"
	cOfficeLoc = " "
	cPropername = "Bioworld"
	nAcctNum  = 6182
	coffice='C'
	cMod = "5"
	gOffice = cMod
	gMasteroffice = cOffice
	lLoadSQL = .t.

	xfile = ""
	cErrMsg = "NOT SET"
	xfile ="NO FILE YET"
	ldeletefile = .T.
	NormalExit = .F.
	lN2 = .F.
	tsendto =""
	tcc=""
	tccerr=""

	STORE cPropername+" 940 Process" TO _SCREEN.CAPTION
	CLEAR
	WAIT WINDOW "Now setting up "+cPropername+" 940 process..." &message_timeout
	DO m:\dev\prg\_setvars WITH .T.

	_SCREEN.WINDOWSTATE = IIF(lTesting OR lTestImport OR lOverridebusy,2,1)

	DO m:\dev\prg\createx856a

	cfile = ""
	dXdate1 = ""
	dXxdate2 = DATE()
	nFileCount = 0

	nRun_num = 999
	lnRunID = nRun_num

	SELECT 0
	USE F:\edirouting\FTPSETUP SHARED
	LOCATE FOR FTPSETUP.TRANSFER = "940-BIOWORLD-SP"
	ASSERT .F.
	IF FOUND()
		IF chkbusy AND !lOverridebusy
			WAIT WINDOW "940 Process is busy...exiting" TIMEOUT 2
			NormalExit = .T.
			THROW
		ENDIF
	ENDIF
	REPLACE chkbusy WITH .T.,trig_time WITH DATETIME() FOR FTPSETUP.TRANSFER = "940-BIOWORLD-SP"
	USE IN FTPSETUP

*	ASSERT .F. MESSAGE "At MM Config load"
	SELECT 0
	USE F:\3pl\DATA\mailmaster ALIAS mm SHARED
	LOCATE FOR mm.accountid = nAcctNum AND mm.office = cOffice ;
		AND edi_type = "940"
	IF FOUND()
		STORE TRIM(mm.basepath) TO lcPath
		STORE TRIM(mm.archpath) TO lcArchivePath
		tsendto = IIF(mm.use_alt,TRIM(mm.sendtoalt),TRIM(mm.sendto))
		tcc = IIF(mm.use_alt,TRIM(mm.ccalt),TRIM(mm.cc))
		STORE TRIM(mm.scaption) TO thiscaption
		STORE mm.testflag 	   TO lTest
		_SCREEN.CAPTION = thiscaption
		IF lTesting
		locate
		LOCATE FOR (mm.edi_type = "MISC") AND (mm.taskname = "JOETEST")
			tsendto = IIF(mm.use_alt,TRIM(mm.sendtoalt),TRIM(mm.sendto))
			tcc = IIF(mm.use_alt,TRIM(mm.ccalt),TRIM(mm.cc))
		ENDIF
		locate
		LOCATE FOR (mm.edi_type = "MISC") AND (mm.taskname = "GENERAL")
		tsendtoerr = IIF(mm.use_alt,TRIM(mm.sendtoalt),TRIM(mm.sendto))
		tccerr = IIF(mm.use_alt,TRIM(mm.ccalt),TRIM(mm.cc))
		USE IN mm
	ELSE
		WAIT WINDOW "No parameters set for this acct# "+ALLTRIM(STR(Acctnum))+"  ---> Office "+lcOffice TIMEOUT 2
		USE IN mm
		NormalExit = .T.
		THROW
	ENDIF

	xsqlexec("select * from upcdet where accountid = 6182",,,"wh")

	IF lTesting
		lcPath = "F:\0-Picktickets\bioworld\Test\"
		lcArchivePath = "F:\0-Picktickets\archive\bioworld\Test\"
		cUseFolder = "F:\WHP\WHDATA\"
		WAIT WINDOW "Test data...Importing into WHP tables"  &message_timeout
	ELSE
		xReturn = "XXX"
		DO m:\dev\prg\wf_alt WITH cOffice,nAcctNum
		cUseFolder = UPPER(xReturn)
	ENDIF

	DO ("m:\dev\PRG\"+cCustname+"940_PROCESS")

	SELECT 0
	USE F:\edirouting\FTPSETUP SHARED
	REPLACE chkbusy WITH .F. FOR FTPSETUP.TRANSFER = "940-BIOWORLD-SP"
	USE IN FTPSETUP

CATCH TO oErr
	IF !NormalExit
		ASSERT .F. MESSAGE "At CATCH..."
		SET STEP ON

		tsubject = cCustname+" 940 "+cOfficeLoc+" Upload Error ("+TRANSFORM(oErr.ERRORNO)+") at "+TTOC(DATETIME())
		tattach  = ""
		tsendto  = tsendtoerr
		tcc = tccerr
		tmessage = cCustname+" 940 "+cOfficeLoc+" Upload Error..... Please fix me........!"+CHR(13)+cErrMsg
		lcSourceMachine = SYS(0)
		lcSourceProgram = SYS(16)

		tmessage =tmessage+ "Try/CATCH Exeception Message:"+CHR(13)+;
			[  Message: ] + oErr.MESSAGE +CHR(13)+;
			[  Procedure: ] + oErr.PROCEDURE +CHR(13)+;
			[  LineNo: ] + STR(oErr.LINENO) +CHR(13)+;
			[  Error: ] + STR(oErr.ERRORNO) +CHR(13)+;
			[  Details: ] + oErr.DETAILS +CHR(13)+;
			[  StackLevel: ] + STR(oErr.STACKLEVEL) +CHR(13)+;
			[  LineContents: ] + oErr.LINECONTENTS+CHR(13)+;
			[  UserValue: ] + oErr.USERVALUE+CHR(13)+;
			[  Computer:  ] +lcSourceMachine+CHR(13)+;
			[  940 file:  ] +xfile+CHR(13)+;
			[  Program:   ] +lcSourceProgram

		tattach  = ""
		tFrom ="TGF EDI Operations <toll-edi-ops@tollgroup.com>"
		DO FORM m:\dev\frm\dartmail2 WITH tsendto,tFrom,tsubject,tcc,tattach,tmessage,"A"
	ELSE
		WAIT WINDOW "Normal Exit " TIMEOUT 2
	ENDIF
FINALLY
	CLOSE DATABASES ALL
ENDTRY
