*!*	get current trucl mileage for Mike DiVirgilio

SET EXCLUSIVE OFF
SET CENTURY ON
SET DATE AMERICAN


LOCAL lnRow, lcRow, lcFiletoSaveAs, lcSpreadsheetTemplate, lnNumberOfErrors
LOCAL lcErr, lcTitle, loError, lnRate, ldToday
LOCAL lcDivision, lnLastRow, lnRate, lcDetailBasisFile
LOCAL ldToday, i, ldDay

PRIVATE poExcel, poWorkbook, poWorksheet, pnNumQuarters, pdLastQTRDate, pdEarliestCRDate

pnNumQuarters = 9

ldToday = DATE()

lcFiletoSaveAs = "F:\UTIL\SHOPREPORTS\REPORTS\TRUCK_MILEAGE_for_" + DTOS(DATE()) + ".XLS"
CLOSE DATABASES ALL



USE F:\SHOP\SHDATA\ordhdr AGAIN IN 0 ALIAS TORDHDR
USE F:\SHOP\SHDATA\TRUCKS AGAIN IN 0 ALIAS TTRUCKS

SELECT VEHNO AS TRUCK_NUM, CRDATE, MILEAGE, "  " AS DIVISION ;
	FROM Tordhdr ;
	INTO CURSOR CURMILEAGE ;
	WHERE .F. ;
	READWRITE

SELECT TTRUCKS
SCAN FOR ACTIVE
	SCATTER MEMVAR
	* first get the latest work order date that is not a Wash (they always put mileage = 1 for washes so we want to exclude those).
	SELECT MAX(CRDATE) AS CRDATE FROM TORDHDR INTO CURSOR CURCRDATE WHERE (VEHNO == TTRUCKS.TRUCK_NUM) AND (TYPE == 'TRUCK ') AND (NOT EMPTY(CRDATE)) AND (MILEAGE > 1)
	m.CRDATE = NVL(CURCRDATE.CRDATE,{})
	* not get the mileage for that work order date...
	SELECT MAX(MILEAGE) AS MILEAGE FROM TORDHDR INTO CURSOR CURTEMP WHERE (VEHNO == TTRUCKS.TRUCK_NUM) AND CRDATE = m.CRDATE
	
	* save in cursor
	m.MILEAGE = NVL(CURTEMP.MILEAGE,0)
	INSERT INTO CURMILEAGE FROM MEMVAR
ENDSCAN


SELECT CURMILEAGE
BROWSE
COPY TO (lcFiletoSaveAs) XL5
