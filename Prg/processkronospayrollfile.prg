* Process file F:\UTIL\KRONOS\EDI\ADPFILES\EPIE87.CSV so that extra lines due to temp Depts are rolled up.
*
#DEFINE LOGIT 1
#DEFINE WAITIT 2
#DEFINE NOWAITIT 4
#DEFINE SENDIT 8
#DEFINE CRLF CHR(13) + CHR(10)
#DEFINE CARRIAGE_RETURN CHR(13)
#DEFINE LINE_FEED CHR(10)
#DEFINE RETURN_DATA_MANDATORY .F.
#DEFINE RETURN_DATA_NOT_MANDATORY .T.
#DEFINE INPUT_PAYROLL_FILE 'F:\UTIL\KRONOS\EDI\ADPFILES\EPIE8799.CSV'

* NOTE: BUILD EXE AS F:\UTIL\ADPEDI\ProcessKronosPayrollFile.exe  !!!!
IF NOT FILE(INPUT_PAYROLL_FILE) THEN
	RETURN
ENDIF
LOCAL loProcessKronosPayrollFileProcess
loProcessKronosPayrollFileProcess = CREATEOBJECT('ProcessKronosPayrollFileProcess')
loProcessKronosPayrollFileProcess.MAIN()
CLOSE DATABASES ALL
RETURN


DEFINE CLASS ProcessKronosPayrollFileProcess AS CUSTOM

	cProcessName = 'ProcessKronosPayrollFileProcess'

	lTestMode = .T.  && .F. FOR NORMAL OPERATION, .T. TO SUPPRESS ALL BUT DEVELOPER EMAILS AND CHANGE INPUT TABLE, ETC.

	lAutoYield = .T.

	dToday = DATE()

	cStartTime = TTOC(DATETIME())

	* output file properties
	cADPPath = 'C:\ADP\PCPW\ADPDATA\'
	nCurrentOutputCSVFileHandle = -1
	cOutputCSVHeaderLine1 = 'CO CODE,FILE #,SOCIAL SECURITY NUMBER,EMPLOYEE LAST NAME,EMPLOYEE FIRST NAME,ADDRESS LINE 1,ADDRESS LINE 2,CITY,STATE POSTAL CODE,'
	cOutputCSVHeaderLine2 = 'ZIP CODE,GENDER,HOME DEPARTMENT,RATE TYPE,RATE 1 AMOUNT,HIRE STATUS,HIRE DATE,BIRTH DATE,WORKED STATE TAX CODE,SUI/SDI TAX JURISDICTION CODE'
	*cOutputCSVHeaderLine2 = 'ZIP CODE,GENDER,HOME DEPARTMENT,RATE TYPE,RATE 1 AMOUNT,HIRE STATUS,HIRE DATE,BIRTH DATE,WORKED STATE TAX CODE'
	cArchiveTargetCSVPath = 'F:\UTIL\ADPEDI\FROMKRONOSHR\TARGETARCHIVED\'
	cFileDate = STRTRAN(DTOC(DATE()),"/","_")

	* connection properties
	nSQLHandle = 0

	* wait window properties
	nWaitWindowTimeout = 2
	lWaitWindowIsOn = .T.

	* logfile properties
	lLoggingIsOn = .T.
	cLogFile = 'F:\UTIL\ADPEDI\Logfiles\ProcessKronosPayrollFileProcess_log.txt'

	* INTERNAL email properties
	lSendInternalEmailIsOn = .F.
	cFrom = 'TGF Corporate <fmicorporate@fmiint.com>'
	cSendTo = 'Mark Bennett <mbennett@fmiint.com>'
	cCC = ''
*!*		cSendTo = 'lwaldrip@fmiint.com'
*!*		cCC = 'MarieDesaye@fmiint.com, RSelawsky@fmiint.com, mbennett@fmiint.com'
	cSubject = 'Process Kronos Payroll File for: ' + TTOC(DATETIME())
	cAttach = ''
	cBodyText = ''
	
	* other properties
	cCOMPUTERNAME = ''
	cUSERNAME = ''

	FUNCTION INIT
		IF NOT DODEFAULT()
			RETURN .F.
		ENDIF
		WITH THIS
			CLEAR
			*SET RESOURCE OFF
			CLOSE DATA
			SET CENTURY ON
			SET DATE AMERICAN
			SET DECIMALS TO 2
			SET HOURS TO 24
			SET ANSI ON
			SET TALK OFF
			SET DELETED ON
			SET CONSOLE OFF
			SET EXCLUSIVE OFF
			SET SAFETY OFF
			SET EXACT OFF
			SET STATUS BAR ON
			SET SYSMENU OFF
			SET ENGINEBEHAVIOR 70
			_VFP.AUTOYIELD = .lAutoYield
			.cCOMPUTERNAME = GETENV("COMPUTERNAME")
			.cUSERNAME = GETENV("USERNAME")
			IF .lTestMode THEN
				.cSendTo = 'Mark Bennett <mbennett@fmiint.com>'
				.cCC = ''
				.cLogFile = 'F:\UTIL\ADPEDI\LOGFILES\ProcessKronosPayrollFileProcess_log_TESTMODE.txt'
			ENDIF
			.lLoggingIsOn = .lLoggingIsOn AND NOT EMPTY(.cLogFile)
			IF .lLoggingIsOn THEN
				SET ALTERNATE TO (.cLogFile) ADDITIVE
				SET ALTERNATE ON
			ENDIF
			* re-init date properties after all SETTINGS
			.dToday = DATE()
		ENDWITH
	ENDFUNC


	FUNCTION DESTROY
		WITH THIS
			IF .lLoggingIsOn  THEN
				SET ALTERNATE OFF
				SET ALTERNATE TO
			ENDIF
		ENDWITH
		DODEFAULT()
	ENDFUNC


	FUNCTION MAIN
		WITH THIS
			LOCAL lnNumberOfErrors, loError, oExcel, oWorkbook, oWorksheet, lnRow, lcRow

			TRY
				lnNumberOfErrors = 0

				*.TrackProgress('==================================================================================================================', LOGIT+SENDIT)
				.TrackProgress('Process Kronos Payroll File process started....', LOGIT+SENDIT+NOWAITIT)
				.TrackProgress('COMPUTERNAME = ' + .cCOMPUTERNAME, LOGIT+SENDIT)
				.TrackProgress('USERNAME = ' + .cUSERNAME, LOGIT+SENDIT)
				.TrackProgress('==================================================================================================================', LOGIT+SENDIT)
				IF .lTestMode THEN
					.TrackProgress('', LOGIT)
					.TrackProgress('==================================================================================================================', LOGIT+SENDIT)
					.TrackProgress('                        TEST MODE                        ', LOGIT+SENDIT)
					.TrackProgress('==================================================================================================================', LOGIT+SENDIT)
				ENDIF
				
				WAIT WINDOW NOWAIT "Opening Excel..."
				oExcel = CREATEOBJECT("excel.application")
				oExcel.visible = .F.
				oWorkbook = oExcel.workbooks.OPEN(INPUT_PAYROLL_FILE)
				oWorksheet = oWorkbook.Worksheets[1]
				
				CREATE CURSOR CURPAY( COCODE C(3), FILENUM I, BATCHID C(6), TRANSTYPE C(10), TYPECODE C(1), TRANSAMT N(9,2) )
				
				lnRow = 2
				lcRow = LTRIM(STR(lnRow))				
				m.CoCode = oWorksheet.RANGE("A" + lcRow).VALUE
				
				DO WHILE NOT ISNULL(m.CoCode)				
				
					* init memvars per each spreadsheet row
					STORE '' TO m.TransType, m.TypeCode, m.CharValue
					STORE 0 TO m.NumericValue, m.TransAmt

					* these are always present in a valid row
					*m.CoCode = oWorksheet.RANGE("A" + lcRow).VALUE   && GOTTEN AT END OF SCAN
					m.BatchID = oWorksheet.RANGE("B" + lcRow).VALUE
					m.FileNum = oWorksheet.RANGE("C" + lcRow).VALUE  && numeric INT
					
					* optional fields follow
					* REG HOURS
					m.NumericValue = oWorksheet.RANGE("J" + lcRow).VALUE
					IF (NOT ISNULL(m.NumericValue)) AND (m.NumericValue > 0.0) THEN
						m.TransType = "REGHOURS"
						m.TransAmt = m.NumericValue
						m.TypeCode = ''
						INSERT INTO CURPAY FROM MEMVAR
					ENDIF
					
					* OT HRS
					m.NumericValue = oWorksheet.RANGE("K" + lcRow).VALUE
					IF (NOT ISNULL(m.NumericValue)) AND (m.NumericValue > 0.0) THEN
						m.TransType = "OTHOURS"
						m.TransAmt = m.NumericValue
						m.TypeCode = ''
						INSERT INTO CURPAY FROM MEMVAR
					ENDIF
				
					* Hours 3
					m.CharValue = oWorksheet.RANGE("L" + lcRow).VALUE
					m.NumericValue = oWorksheet.RANGE("M" + lcRow).VALUE
					IF (NOT ISNULL(m.CharValue)) AND (NOT EMPTY(m.CharValue)) THEN
						IF (NOT ISNULL(m.NumericValue)) AND (m.NumericValue > 0.0) THEN
							m.TransType = "HOURS3"
							m.TransAmt = m.NumericValue
							m.TypeCode = m.CharValue
							INSERT INTO CURPAY FROM MEMVAR
						ENDIF
					ENDIF
				
					* Hours 4 - Set 1
					m.CharValue = oWorksheet.RANGE("N" + lcRow).VALUE
					m.NumericValue = oWorksheet.RANGE("O" + lcRow).VALUE
					IF (NOT ISNULL(m.CharValue)) AND (NOT EMPTY(m.CharValue)) THEN
						IF (NOT ISNULL(m.NumericValue)) AND (m.NumericValue > 0.0) THEN
							m.TransType = "HOURS4"
							m.TransAmt = m.NumericValue
							m.TypeCode = m.CharValue
							INSERT INTO CURPAY FROM MEMVAR
						ENDIF
					ENDIF
				
					* Hours 4 - Set 2
					m.CharValue = oWorksheet.RANGE("P" + lcRow).VALUE
					m.NumericValue = oWorksheet.RANGE("Q" + lcRow).VALUE
					IF (NOT ISNULL(m.CharValue)) AND (NOT EMPTY(m.CharValue)) THEN
						IF (NOT ISNULL(m.NumericValue)) AND (m.NumericValue > 0.0) THEN
							m.TransType = "HOURS4"
							m.TransAmt = m.NumericValue
							m.TypeCode = m.CharValue
							INSERT INTO CURPAY FROM MEMVAR
						ENDIF
					ENDIF
				
					* Earnings 4 - Set 1
					m.CharValue = oWorksheet.RANGE("R" + lcRow).VALUE
					m.NumericValue = oWorksheet.RANGE("S" + lcRow).VALUE
					IF (NOT ISNULL(m.CharValue)) AND (NOT EMPTY(m.CharValue)) THEN
						IF (NOT ISNULL(m.NumericValue)) AND (m.NumericValue > 0.0) THEN
							m.TransType = "EARNINGS4"
							m.TransAmt = m.NumericValue
							m.TypeCode = m.CharValue
							INSERT INTO CURPAY FROM MEMVAR
						ENDIF
					ENDIF
				
					* Earnings 4 - Set 2
					m.CharValue = oWorksheet.RANGE("T" + lcRow).VALUE
					m.NumericValue = oWorksheet.RANGE("U" + lcRow).VALUE
					IF (NOT ISNULL(m.CharValue)) AND (NOT EMPTY(m.CharValue)) THEN
						IF (NOT ISNULL(m.NumericValue)) AND (m.NumericValue > 0.0) THEN
							m.TransType = "EARNINGS4"
							m.TransAmt = m.NumericValue
							m.TypeCode = m.CharValue
							INSERT INTO CURPAY FROM MEMVAR
						ENDIF
					ENDIF
				
					lnRow = lnRow + 1
					lcRow = LTRIM(STR(lnRow))
					m.CoCode = oWorksheet.RANGE("A" + lcRow).VALUE
				ENDDO
				SELECT CURPAY
				BROWSE
				
				*oWorkbook.CLOSE()
				oExcel.QUIT()
				oExcel = .NULL.

			CATCH TO loError

				.TrackProgress('There was an error.',LOGIT+SENDIT)
				.TrackProgress('The Error # is: ' + TRANSFORM(loError.ERRORNO), LOGIT+SENDIT)
				.TrackProgress('The Error Message is: ' + TRANSFORM(loError.MESSAGE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred in Program: ' + TRANSFORM(loError.PROCEDURE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred at line #: ' + TRANSFORM(loError.LINENO), LOGIT+SENDIT)
				lnNumberOfErrors = lnNumberOfErrors + 1
				IF TYPE('oExcel') = 'O' AND NOT ISNULL(oExcel) THEN
					oExcel.QUIT()
				ENDIF
				CLOSE DATA
				CLOSE ALL

			ENDTRY

			CLOSE DATA
			WAIT CLEAR
			***************** INTERNAL email results ******************************
			.TrackProgress('About to send status email.',LOGIT)
			.TrackProgress('==================================================================================================================', SENDIT)
			.TrackProgress('The logfile is: ' + .cLogFile, SENDIT)
			.TrackProgress('Process Kronos Payroll File process started: ' + .cStartTime, LOGIT+SENDIT)
			.TrackProgress('Process Kronos Payroll File process finished: ' + TTOC(DATETIME()), LOGIT+SENDIT)

			IF .lSendInternalEmailIsOn THEN
				* try to trap error from not having dartmail dll's registered on user's pc...
				TRY
					DO FORM dartmail2 WITH .cSendTo,.cFrom,.cSubject,.cCC,.cAttach,.cBodyText,"A"
					.TrackProgress('Sent status email.',LOGIT)
					
				CATCH TO loError
					=MESSAGEBOX('There was an error sending the status email. Please contact Mark Bennett.',0+64,"Process Kronos Payroll File Report")
					.TrackProgress('There was an error sending the status email.',LOGIT)
					.TrackProgress('The Error # is: ' + TRANSFORM(loError.ERRORNO), LOGIT)
					.TrackProgress('The Error Message is: ' + TRANSFORM(loError.MESSAGE), LOGIT)
					.TrackProgress('The Error occurred in Program: ' + TRANSFORM(loError.PROCEDURE), LOGIT)
					.TrackProgress('The Error occurred at line #: ' + TRANSFORM(loError.LINENO), LOGIT)
					lnNumberOfErrors = lnNumberOfErrors + 1
					CLOSE DATA
				ENDTRY
			ELSE
				.TrackProgress('Did not send status email: THIS.lSendInternalEmailIsOn = FALSE.',LOGIT)
			ENDIF

		ENDWITH
		RETURN
	ENDFUNC && main


	PROCEDURE ProcessKronosOutputFiles
		LPARAMETERS tcADPCOMP
		WITH THIS
			LOCAL lcOutputFilename, lcOutputLine, lcTargetArchivedFilename, llWroteOutputHeader, lcEmployeeIdentifier

			llWroteOutputHeader = .F.
			.nCurrentOutputCSVFileHandle = -1

			SELECT CURMASTER
			SCAN FOR ADP_COMP == tcADPCOMP

				IF NOT llWroteOutputHeader THEN
					.CreateOutputCSVFile(tcADPCOMP)
					llWroteOutputHeader = .WriteOutputHeader()
				ENDIF

				lcEmployeeIdentifier = tcADPCOMP + " " + TRANSFORM(CURMASTER.FILE_NUM) + " :  " + ;
					ALLTRIM(CURMASTER.LASTNAME) + ", " + ALLTRIM(CURMASTER.FIRSTNAME) + CRLF
					
				.cNewHireList = .cNewHireList + lcEmployeeIdentifier
				
				*!*	* build express hire list
				*!*	IF CURMASTER.HOMEDEPT == EXPRESSHOMEDEPT THEN
				*!*		.cExpressHireList = .cExpressHireList + lcEmployeeIdentifier
				*!*	ENDIF

				* Write Detail Line

				lcOutputLine = ;
					GetNewADPCompany(.CleanUpField(CURMASTER.ADP_COMP)) + "," + ;
					.CleanUpField(CURMASTER.FILE_NUM) + "," + ;
					.CleanUpField(CURMASTER.SS_NUM) + "," + ;
					.CleanUpField(CURMASTER.LASTNAME) + "," + ;
					.CleanUpField(CURMASTER.FIRSTNAME) + "," + ;
					.CleanUpField(CURMASTER.STREET1) + "," + ;
					.CleanUpField(CURMASTER.STREET2) + "," + ;
					.CleanUpField(CURMASTER.CITY) + "," + ;
					.CleanUpField(CURMASTER.STATE) + "," + ;
					.CleanUpField(CURMASTER.ZIPCODE) + "," + ;
					.CleanUpField(CURMASTER.GENDER) + "," + ;
					.CleanUpField(CURMASTER.HOMEDEPT) + "," + ;
					.CleanUpField(CURMASTER.RATETYPE) + "," + ;
					.CleanUpField(ALLTRIM(STR(CURMASTER.RATE1AMT,9,2))) + "," + ;
					.CleanUpField(CURMASTER.STATUS) + "," + ;
					.CleanUpField(TRANSFORM(CURMASTER.HIREDATE)) + "," + ;
					.CleanUpField(TRANSFORM(CURMASTER.DOB)) + "," + ;
					.CleanUpField(CURMASTER.WORKSTATE) + "," + ;
					.CleanUpField(CURMASTER.SUISDI)

				=FPUTS(.nCurrentOutputCSVFileHandle,lcOutputLine)

				*** removed tax jurisdiction per Lucille 5/8/08 MB because they were not always right
				*** .CleanUpField(lcSUISDITAXJURISCD)

			ENDSCAN

			* close output file
			.CloseOutputCSVFile()

			* copy output file (renamed so it is unique) to TARGETARCHIVED folder on F:
			lcOutputFilename = .cADPPath + "PR" + GetNewADPCompany(tcADPCOMP) + "EMP.CSV"
			lcTargetArchivedFilename = .cArchiveTargetCSVPath + "PR" + GetNewADPCompany(tcADPCOMP) + "EMP_" + .cFileDate + ".CSV"
			IF FILE(lcOutputFilename) THEN
				COPY FILE (lcOutputFilename) TO (lcTargetArchivedFilename)
			ENDIF

			.TrackProgress('=========================================================', LOGIT+SENDIT)
		ENDWITH
	ENDPROC


	FUNCTION CleanUpField
		* prepare field for inclusion in the string to be written with FPUTS().
		* especially critical to remove any commas, since that creates bogus column in CSV output file.
		LPARAMETERS tuValue
		LOCAL lcRetVal
		lcRetVal = STRTRAN(ALLTRIM(TRANSFORM(tuValue)),",","")
		RETURN lcRetVal
	ENDFUNC


	FUNCTION CreateOutputCSVFile
		LPARAMETERS tcADPCOMP
		WITH THIS
			LOCAL lcOutputFilename, llRetval
			lcOutputFilename = .cADPPath + "PR" + GetNewADPCompany(tcADPCOMP) + "EMP.CSV"
			llRetval = .T.
			.nCurrentOutputCSVFileHandle = FCREATE(lcOutputFilename)
			IF .nCurrentOutputCSVFileHandle < 0 THEN
				.TrackProgress('***** Error on FCREATE of: ' + lcOutputFilename, LOGIT+SENDIT)
				llRetval = .F.
			ELSE
				.TrackProgress('FCREATEed: ' + lcOutputFilename, LOGIT+SENDIT)
				.nNumberOfOutputFiles = .nNumberOfOutputFiles + 1
				DO CASE
					CASE tcADPCOMP = "AXA"
						.lOutputAXAFile = .T.
					CASE tcADPCOMP = "SXI"
						.lOutputSXIFile = .T.
					CASE tcADPCOMP = "ZXU"
						.lOutputZXUFile = .T.
					OTHERWISE
						* nothing
				ENDCASE
			ENDIF
			.TrackProgress('nCurrentOutputCSVFileHandle = ' + TRANSFORM(.nCurrentOutputCSVFileHandle), LOGIT+SENDIT)
		ENDWITH
		RETURN llRetval
	ENDFUNC


	FUNCTION WriteOutputHeader
		WITH THIS
			LOCAL llRetval, lnBytesWritten
			* note: no CRLF on first part of header line
			llRetval = .T.
			lnBytesWritten = FWRITE(.nCurrentOutputCSVFileHandle,.cOutputCSVHeaderLine1)
			.TrackProgress('FWROTE # of bytes: ' + TRANSFORM(lnBytesWritten), LOGIT+SENDIT)
			lnBytesWritten = FWRITE(.nCurrentOutputCSVFileHandle,.cOutputCSVHeaderLine2 + CRLF)
			.TrackProgress('FWROTE # of bytes: ' + TRANSFORM(lnBytesWritten), LOGIT+SENDIT)
		ENDWITH
		RETURN llRetval
	ENDFUNC


	FUNCTION CloseOutputCSVFile
		WITH THIS
			LOCAL llRetval
			llRetval = .T.
			IF .nCurrentOutputCSVFileHandle > -1 THEN
				llRetval = FCLOSE(.nCurrentOutputCSVFileHandle)
			ENDIF
		ENDWITH
		RETURN llRetval
	ENDFUNC


	FUNCTION ExecSQL
		LPARAMETERS tcSQL, tcCursorName, tlNoDataReturnedIsOkay
		LOCAL llRetval, lnResult
		WITH THIS
			* close target cursor if it's open
			IF USED(tcCursorName)
				USE IN (tcCursorName)
			ENDIF
			WAIT WINDOW "Fetching remote data; please wait..." NOWAIT
			lnResult = SQLEXEC(.nSQLHandle, tcSQL, tcCursorName)
			llRetval = ( lnResult > 0 )
			IF llRetval THEN
				* see if any data came back
				IF NOT tlNoDataReturnedIsOkay THEN
					IF (NOT USED(tcCursorName)) OR EOF(tcCursorName) THEN
						llRetval = .F.
						.TrackProgress('No data was returned by this query: ' +CRLF + tcSQL, LOGIT+SENDIT)
					ENDIF
				ENDIF
			ELSE
				.TrackProgress("Result Code [" + TRANSFORM(lnResult) + "] returned by this query: " + tcSQL, LOGIT+SENDIT)
			ENDIF
			WAIT CLEAR
			RETURN llRetval
		ENDWITH
	ENDFUNC


	PROCEDURE TrackProgress
		* do any combination of Wait Window, writing to logfile, and adding to body of email,
		* based on nFlags parameter.
		LPARAMETERS tcExpression, tnFlags
		WITH THIS
			IF BITAND(tnFlags,LOGIT) = LOGIT THEN
				IF .lLoggingIsOn THEN
					?
					? .cProcessName + "  " + TTOC(DATETIME()) + ": " + tcExpression
				ENDIF
			ENDIF
			IF .lWaitWindowIsOn THEN
				IF BITAND(tnFlags,NOWAITIT) = NOWAITIT THEN
					WAIT WINDOW tcExpression NOWAIT
				ENDIF
				IF BITAND(tnFlags,WAITIT) = WAITIT THEN
					WAIT WINDOW tcExpression TIMEOUT .nWaitWindowTimeout
				ENDIF
			ENDIF
			IF BITAND(tnFlags,SENDIT) = SENDIT THEN
				IF .lSendInternalEmailIsOn THEN
					.cBodyText = .cBodyText + tcExpression + CRLF + CRLF
				ENDIF
			ENDIF
		ENDWITH
	ENDPROC  &&  TrackProgress

ENDDEFINE

