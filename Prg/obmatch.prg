* Checks for errors in the outbound data

lparameter xdev, xauto


if usesqlbl()
	xsqlexec("select * from bl",,,"wo")
	xsqlexec("select * from delivery",,,"wo")
else
	use f:\wo\wodata\delivery in 0
	use f:\wo\wodata\bl in 0
endif

create cursor xbl ( ;
	blid i, ;
	bl_num c(20), ;
	ship_qty n(6), ;
	ship_wt n(8,2), ;
	rcv_qty n(6), ;
	rcv_wt n(8,2), ;
	bl_del_qty n(6), ;
	de_del_qty n(6), ;
	po_del_qty n(6), ;
	dp_del_qty n(6), ;
	bl_del_wt n(8,2), ;
	de_del_wt n(8,2), ;
	po_del_wt n(8,2), ;
	dp_del_wt n(8,2), ;
	bl_undel_qty n(8), ;
	de_undel_qty n(8), ;
	po_undel_qty n(8), ;
	dp_undel_qty n(8), ;
	bl_undel_wt n(8,2), ;
	de_undel_wt n(9,2), ;
	po_undel_wt n(9,2), ;
	dp_undel_wt n(9,2))
	
index on blid tag blid

select delivery
scan 
	if !seek(blid,"xbl","blid")
		select xbl
		append blank
		replace blid with delivery.blid, bl_num with delivery.bl_num
	endif
	
	replace de_del_qty with xbl.de_del_qty+delivery.del_qty, ;
			de_del_wt with xbl.de_del_wt+delivery.del_wt in xbl
endscan

select bl
scan 
	if !seek(blid,"xbl","blid")
		select xbl
		append blank
		replace blid with bl.blid, bl_num with bl.bl_num
	endif
	
	select xbl
	replace ship_qty with bl.ship_qty
	replace	rcv_qty with bl.rcv_qty
	replace	bl_del_qty with bl.del_qty
	replace	bl_undel_qty with bl.undel_qty
	replace	ship_wt with bl.ship_wt
	replace	rcv_wt with bl.rcv_wt
	replace	bl_del_wt with bl.del_wt
	replace	bl_undel_wt with bl.undel_wt
endscan
	
replace all de_undel_qty with rcv_qty-de_del_qty, ;
			de_undel_wt with rcv_wt-de_del_wt in xbl

select xbl

xfilter="(bl_del_qty#de_del_qty or "+ ;
		"bl_del_wt#de_del_wt or "+ ;
		"((bl_del_qty#po_del_qty) and po_del_qty#0) or "+ ;
		"((bl_del_wt#po_del_wt) and po_del_wt#0) or "+ ;
		"((bl_del_qty#dp_del_qty) and dp_del_qty#0) or "+ ;
		"((bl_del_wt#dp_del_wt) and dp_del_wt#0) or "+ ;
		"(rcv_qty#0 and bl_del_wt # round(bl_del_qty/rcv_qty*rcv_wt,2)) or "+ ;
		"(ship_qty#0 and rcv_wt # round(rcv_qty/ship_qty*ship_wt,2)) or "+ ;
		"bl_undel_qty<0) and bl_num#'04070086210179795'"

copy to f:\auto\xbl for &xfilter and blid>251592

if !xauto
	brow for &xfilter and blid>251592
endif

*select delivery
*brow for mfst_wt#0 and del_wt # round(del_qty/mfst_qty*mfst_wt,2)
*brow for del_qty=0 and del_wt#0


*bl_del_qty#de_del_qty or bl_undel_qty#de_undel_qty or bl_del_wt#de_del_wt or bl_undel_wt#de_undel_wt