*!* fed214outbound_create.prg (derived from VF214 routine)
*!* Created 2010.02.22, Joe

PARAMETERS cConsignee,nWO_Num,cEventCode,nTripId,dUseDate,cStatusTime,nLeg,lPU

IF DATETIME()<DATETIME(2011,03,07,12,00,01)
	ASSERT .F. MESSAGE "At start of Fed214 process"
ENDIF

PUBLIC c_CntrlNum,c_GrpCntrlNum,cWO_Num,cGroupName,cCustName,cFilename,div214,cST_CSZ
PUBLIC tsendto,tcc,tsendtoerr,tccerr,cDelName,cMacysnet,cTripid,cMailName,cPrgName

cPrgName = "FED 214"

lTesting = .F.

DO m:\dev\prg\_setvars WITH lTesting
ON ESCAPE CANCEL

TRY
	closefiles()
	IF !USED('account')
		xsqlexec("select * from account where inactive=0","account",,"qq")
		index on accountid tag accountid
		set order to
	ENDIF
	STORE "" TO cFilename,cShip_ref,cST_CSZ,cMacysnet,cTripid,cWO_Num,cLeg,cFileInfo
	lIsError = .F.
	lDoCatch = .T.
	lEmail = .T.
	lDoError = .F.
	lFilesOut = .T.
	nAcctNum = 647
	cOffice = "I"

	IF VARTYPE(nWO_Num)= "L"
		IF lTesting
			CLOSE DATABASES ALL
			lFilesOut = .F.
			SELECT 0
			USE h:\fox\edi_trigger.DBF ALIAS edi_trigger
			LOCATE
			LOCATE FOR !processed
			IF EOF()
				LOCATE
			ENDIF
*			BROWSE FIELDS consignee,wo_num,tripid,DATE,TIME,leg,STYLE TIMEOUT 10
			SCATTER MEMVAR FIELDS consignee,wo_num,tripid,DATE,TIME,leg,STYLE
			nWO_Num = m.wo_num
			cConsignee = ALLTRIM(m.consignee)
			nTripId = m.tripid
			dUseDate = m.date
			cStatusTime = ALLTRIM(m.time)
			nLeg = m.leg
		ELSE
			cMsg = "No params specified"
			WAIT WINDOW cMsg TIMEOUT 2
			DO ediupdate WITH cMsg,.T.
		ENDIF
	ENDIF
	IF VAL(cStatusTime)=0 OR LEN(TRANSFORM(nTripId))>7
		ASSERT .F. MESSAGE "In bad parameters area...>>DEBUG<<"
		DO ediupdate WITH "BAD PARAMS",.T.
		THROW
	ENDIF
	cStatusTime = ALLTRIM(cStatusTime)

	cConsignee = ALLTRIM(cConsignee)
	cTripid = ALLTRIM(STR(nTripId))
	cEventCode = ALLTRIM(m.style)
	cWO_Num = ALLTRIM(STR(nWO_Num))
	cStatusDate = DTOS(dUseDate)

	xReturn = "XXX"
	DO m:\dev\prg\wf_alt WITH cOffice,nAcctNum
	IF EMPTY(ALLTRIM(xReturn))
		DO ediupdate WITH "NO WHSE",.T.
		THROW
	ENDIF
	cUseFolder = UPPER(xReturn)

	DIMENSION thisarray(1)
	tfrom = "TGF Warehouse Operations <tgf-warehouse-ops@fmiint.com>"
	SELECT 0
	USE F:\3pl\DATA\mailmaster ALIAS mm
	LOCATE FOR mm.edi_type = "MISC" AND mm.taskname = "GENERAL"
	tsendto = IIF(mm.use_alt,mm.sendtoalt,mm.sendto)
	tcc = IIF(mm.use_alt,mm.ccalt,mm.cc)
	USE IN mm
	tattach = ""

	cCustName = "FEDERATED"
	cCustFolder = "FED214"
	cCustPrefix = "FED214"+LOWER(cOffice)
	dtmail = TTOC(DATETIME())
	dt2 = DATETIME()
	cMailName = PROPER(cCustName)
	lcpath = "f:\ftpusers\"+cCustFolder+"\214-Staging\"
	CD &lcpath

*!* Open Tables

	xsqlexec("select * from fxtrips where wo_num="+transform(nwo_num)+" and deadhead=0","xfxtrips",,"fx")

	COUNT TO nStops 

	cLeg = PADL(ALLTRIM(STR(nLeg)),2,"0")
	DO CASE
		CASE INLIST(cEventCode,"X1","CD")
			nCheckStop = (nLeg/2)
		CASE INLIST(cEventCode,"X3","AF") AND !lPU
			nCheckStop = (nLeg/2)
		OTHERWISE
			nCheckStop = ((nLeg+1)/2)
	ENDCASE
	lFinalStop = IIF(nStops = nCheckStop,.T.,.F.)

	xsqlexec("select * from fxwolog where wo_num="+transform(nwo_num),"xfxwolog",,"fx")
	if reccount()#0		
		DO ediupdate WITH "NO FXWOLOG WO# MATCH: "+cWO_Num,.T.
		THROW
	ENDIF
	cBrokRef = ALLT(xfxwolog.shipref)
	cMacysnet = PADL(cBrokRef,11,'0')+"M"
	nFXWologID = xfxwolog.fxwologid

	cST_Name = "TGF INC."
	DO CASE
		CASE cOffice = "L"
			cFMICity = "MIRA LOMA"
			cFMIState = "CA"
		CASE cOffice = "C"
			cFMICity = "SAN PEDRO"
			cFMIState = "CA"
		CASE cOffice = "M"
			cFMICity = "MIAMI"
			cFMIState = "FL"
		OTHERWISE
			cFMICity = "CARTERET"
			cFMIState = "NJ"
	ENDCASE

	cterminator = ">"  && Used at end of ISA segment
	cfd = "*"  && Field delimiter
	csegd = CHR(133) && Unicode 0x85, Segment delimiter

	csendqual = "ZZ"  && Sender qualifier
	csendid = "FMIF"  && Sender ID code
	crecqual = "08"  && Recip qualifier
	crecid = "6113310214"   && Recip ID Code, production (05.11.2010)
*	crecid = "6113310099"   && Recip ID Code, testing

	cDate = DTOS(DATE())
	cTruncDate = RIGHT(cDate,6)
	cTruncTime = SUBSTR(TTOC(DATETIME(),1),9,4)
	cfiledate = cDate+cTruncTime
	csendidlong = PADR(csendid,15," ")
	crecidlong = PADR(crecid,15," ")
	cString = ""

	nSTCount = 0
	nSegCtr = 0
	nLXNum = 1

	cISACode = IIF(lTesting,"T","P")

	WAIT CLEAR
	WAIT WINDOW "Now creating FXTRIPS WO#-based 214 information..." NOWAIT

	xsqlexec("select * from fxtrips where wo_num="+transform(nwo_num)+" and deadhead=0 and !empty(deladdress)","xfxtrips",,"fx")

	cDelName = ALLTRIM(xfxtrips.delname2)
	IF EMPTY(cDelName)
		cDelName = "MACYS"
	ENDIF
	cDelAddress = ALLTRIM(xfxtrips.deladdress)
	cDelCSZ = ALLTRIM(xfxtrips.delcsz)
	cDelCity = ALLTRIM(LEFT(cDelCSZ,AT(",",cDelCSZ,1)-1))
	cDelState = ALLTRIM(SUBSTR(cDelCSZ,AT(",",cDelCSZ,1)+1,3))
	cDelZip = ALLTRIM(SUBSTR(cDelCSZ,AT(",",cDelCSZ,1)+4))
	cDelCityState = cDelCity+", "+cDelState

	IF USED('FEDCODES')
		USE IN FEDCODES
	ENDIF
	USE F:\3pl\DATA\fed214dccodes IN 0 ALIAS FEDCODES
	IF SEEK(cDelCityState,'fedcodes','citystate')
		cDelCode = TRIM(FEDCODES.CODE)
		USE IN FEDCODES
	ELSE
		ASSERT .F. MESSAGE "At Unmatched Del. Code..."+CHR(13)+cDelCityState+CHR(13)+">>DEBUG<<"
		USE IN FEDCODES
		DO ediupdate WITH "UNMATCHED MACYS DEL.CODE, WO# "+cWO_Num,.T.
		THROW
	ENDIF

	xsqlexec("select * from fxtrips where fxtripsid="+transform(ntripid),"xfxtrips",,"fx")
	IF reccount()=0
		DO ediupdate WITH "Trip ID not in FXTRIPS: "+TRANSFORM(nTripId),.T.
		THROW
	ENDIF
	cTripTrailer = ALLT(xfxtrips.triptrailer)
	cFXCSZ = ALLT(xfxtrips.pucsz)
	IF EMPTY(cFXCSZ)
		cFXCSZ = ALLT(xfxtrips.puloc)
	ENDIF
	cSFName = ALLT(xfxtrips.puname)
	IF EMPTY(cSFName)
		cSFName = "UNK PU CLIENT"
		IF EMPTY(cConsignee)
			cConsignee = cSFName
		ENDIF
*!*			DO ediupdate WITH "Missing PUNAME: "+TRANSFORM(nTripId),.T.
*!*			THROW
	ENDIF
	cSFAddress = ALLT(xfxtrips.puaddress)
	IF EMPTY(cSFAddress)
		cSFAddress = "UNK PU ADDR"
	ENDIF
*!* More Variables

	dt1 = TTOC(DATETIME(),1)
	cFilename = (cCustPrefix+dt1+".txt")
	cFilenameShort = JUSTFNAME(cFilename)
	cFilename2 = ("f:\ftpusers\"+cCustFolder+"\214OUT\archive\"+cFilenameShort)
	cFilename3 = ("f:\ftpusers\"+cCustFolder+"\214OUT\"+cFilenameShort)
	nHandle = FCREATE(cFilename)
	DO num_incr_isa
	cISA_Num = PADL(c_CntrlNum,9,"0")
	STORE "ISA"+cfd+"00"+cfd+"          "+cfd+"00"+cfd+"          "+cfd+csendqual+cfd+csendidlong+cfd+;
		crecqual+cfd+crecidlong+cfd+cTruncDate+cfd+cTruncTime+cfd+"U"+cfd+"00200"+cfd+;
		cISA_Num+cfd+"0"+cfd+cISACode+cfd+cterminator+csegd TO cString
	DO cstringbreak

	STORE "GS"+cfd+"QM"+cfd+csendid+cfd+crecid+cfd+cDate+cfd+cTruncTime+cfd+c_CntrlNum+;
		cfd+"X"+cfd+"005010VICS"+csegd TO cString
	DO cstringbreak

	DO num_incr_st
	STORE "ST"+cfd+"214"+cfd+PADL(c_GrpCntrlNum,9,"0")+csegd TO cString
	DO cstringbreak
	nSTCount = nSTCount + 1
	nSegCtr = nSegCtr + 1

	STORE "B10"+cfd+cWO_Num+cfd+cfd+csendid+csegd TO cString
	DO cstringbreak
	nSegCtr = nSegCtr + 1

	STORE "LX"+cfd+ALLTRIM(STR(nLXNum))+csegd TO cString  && Size
	DO cstringbreak
	nSegCtr = nSegCtr + 1
	nLXNum = nLXNum + 1

	STORE "L11"+cfd+REPLICATE('1',8)+cfd+"BM"+csegd TO cString
	DO cstringbreak
	nSegCtr = nSegCtr + 1

	STORE "L11"+cfd+REPLICATE('9',8)+cfd+"ZZ"+csegd TO cString
	DO cstringbreak
	nSegCtr = nSegCtr + 1

	STORE "MAN"+cfd+"CP"+cfd+cMacysnet+csegd TO cString
	DO cstringbreak
	nSegCtr = nSegCtr + 1

	IF EMPTY(cStatusDate)
		cStatusDate = DTOS(DATE())
	ENDIF
	IF EMPTY(cStatusTime)
		cStatusTime = "1530"
	ENDIF

	IF cEventCode = "AB"
*		ASSERT .f. MESSAGE "At AB>AT7 process...debug"
		STORE "AT7"+REPLICATE(cfd,2)+cfd+cEventCode+cfd+"AJ"+cfd+;
			cStatusDate+cfd+cStatusTime+cfd+"LT"+csegd TO cString
	ELSE
		STORE "AT7"+cfd+cEventCode+REPLICATE(cfd,4)+cStatusDate+cfd;
			+cStatusTime+cfd+"LT"+csegd TO cString
	ENDIF
	DO cstringbreak
	nSegCtr = nSegCtr + 1

	STORE "MS2"+cfd+"FMIX"+cfd+cTripTrailer+cfd+"TL"+csegd TO cString
	DO cstringbreak
	nSegCtr = nSegCtr + 1

	STORE "K1"+cfd+"STOP"+cfd+cLeg+csegd TO cString
	DO cstringbreak
	nSegCtr = nSegCtr + 1

	cSTName = cDelName
	STORE "N1"+cfd+"ST"+cfd+cSTName+cfd+"94"+cfd+cDelCode+csegd TO cString
	DO cstringbreak
	nSegCtr = nSegCtr + 1


	STORE "N1"+cfd+"SF"+cfd+cSFName+csegd TO cString
	DO cstringbreak
	nSegCtr = nSegCtr + 1

	STORE "N3"+cfd+cSFAddress+csegd TO cString
	DO cstringbreak
	nSegCtr = nSegCtr + 1

	cStopCSZ = IIF(INLIST(cEventCode,"X1","CD") OR lFinalStop,cDelCSZ,cFXCSZ)
	cStopCity = ALLTRIM(LEFT(cStopCSZ,AT(",",cStopCSZ,1)-1))
	cStopState = ALLTRIM(SUBSTR(cStopCSZ,AT(",",cStopCSZ,1)+1,3))
	cStopZip = ALLTRIM(SUBSTR(cStopCSZ,AT(",",cStopCSZ,1)+4))
	IF EMPTY(cStopZip)
		cSF_CSZ = cStopCity+cfd+cStopState
	ELSE
		cSF_CSZ = cStopCity+cfd+cStopState+cfd+cStopZip
	ENDIF
	STORE "N4"+cfd+cSF_CSZ+csegd TO cString
	DO cstringbreak
	nSegCtr = nSegCtr + 1

	close214()
	lClosedFile=FCLOSE(nHandle)
	IF lClosedFile AND lTesting
		WAIT WINDOW "LL File closed successfully" TIMEOUT 1
*		MODIFY FILE &cFilename
		RELEASE nHandle
	ENDIF


	DO ediupdate WITH "214 CREATED",.F.
	IF lFilesOut
		COPY FILE &cFilename TO &cFilename2
		COPY FILE &cFilename TO &cFilename3
		DELETE FILE &cFilename
	ENDIF
	IF !lTesting
		IF !USED("ftpedilog")
			SELECT 0
			USE F:\edirouting\ftpedilog ALIAS ftpedilog
			INSERT INTO ftpedilog (TRANSFER,ftpdate,filename,acct_name,TYPE) ;
				VALUES ("214-"+cCustName,dt2,cFilename,UPPER(cCustName),"214")
			USE IN ftpedilog
		ENDIF
	ENDIF
	cFileInfo = "MACYSNET#: "+cMacysnet+CHR(13)+CHR(13)
	cFileInfo = cFileInfo+PADR("STOP",24)+PADR("FMI TRIPID",12)+PADR("LEG",5)+"EVENT CODE"
	cFileInfo = cFileInfo+CHR(13)+PADR(REPLICATE("=",4),24)+PADR(REPLICATE("=",10),12)+PADR(REPLICATE("=",3),5)+REPLICATE("=",10)
	cFileInfo = cFileInfo+CHR(13)+PADR(cConsignee,24)+PADR(cTripid,12)+PADR(cLeg,5)+cEventCode
	WAIT WINDOW "Finished EventCode "+cEventCode+;
		" of Stop "+cLeg TIMEOUT 2

*!* Finish processing
	IF lTesting
		tsubject = "FED 214 *TEST* EDI from TGF as of "+dtmail
	ELSE
		tsubject = "FED 214 EDI from TGF as of "+dtmail
	ENDIF

	tmessage = cMailName+" 214 EDI FILE (TGF trucking WO# "+cWO_Num+")"
	tmessage = tmessage+CHR(13)+"has been created for the following:"+CHR(13)
	tmessage = tmessage+CHR(13)+cFileInfo
	IF lTesting
		tmessage = tmessage + CHR(13)+CHR(13)+"This is a TEST RUN..."
	ENDIF

	IF lEmail
		DO FORM dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
	ENDIF
	RELEASE ALL LIKE c_CntrlNum,c_GrpCntrlNum
	RELEASE ALL LIKE nOrigSeq,nOrigGrpSeq
	WAIT CLEAR
	WAIT WINDOW cMailName+" 214 EDI File output complete" AT 20,60 TIMEOUT 2
	closefiles()

CATCH TO oErr
	IF lDoCatch
		ASSERT .F. MESSAGE "In CATCH...debug"
		lEmail = .T.
		DO ediupdate WITH "TRY/CATCH ERROR",.T.
		tsubject = cMailName+" 214 Error ("+TRANSFORM(oErr.ERRORNO)+") at "+TTOC(DATETIME())
		tattach  = ""
		tmessage = cCustName+" Error processing "+CHR(13)
		tmessage = tmessage+TRIM(PROGRAM())+CHR(13)

		tmessage =tmessage+ "Try/CATCH Exeception Message:"+CHR(13)+;
			[  Error: ] + STR(oErr.ERRORNO) +CHR(13)+;
			[  LineNo: ] + STR(oErr.LINENO) +CHR(13)+;
			[  Message: ] + oErr.MESSAGE +CHR(13)+;
			[  Procedure: ] + oErr.PROCEDURE +CHR(13)+;
			[  Details: ] + oErr.DETAILS +CHR(13)+;
			[  StackLevel: ] + STR(oErr.STACKLEVEL) +CHR(13)+;
			[  LineContents: ] + oErr.LINECONTENTS+CHR(13)+;
			[  UserValue: ] + oErr.USERVALUE

		tsubject = "214 Process Error at "+TTOC(DATETIME())
		tattach  = ""
		tcc=""
		tfrom    ="TGF EDI 214 Process Ops <fmi-warehouse-ops@fmiint.com>"
		DO FORM dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
		lEmail = .F.
	ENDIF
FINALLY
	closefiles()
ENDTRY

&& END OF MAIN CODE SECTION

**************************
PROCEDURE ediupdate
**************************
	PARAMETERS cFin_status,lIsError
	ASSERT .F. MESSAGE "In EDIUPDATE"
	lDoCatch = .F.
	SELECT edi_trigger
	LOCATE
	IF lIsError
		REPLACE edi_trigger.processed WITH .T.,edi_trigger.created WITH .F.,;
			edi_trigger.fin_status WITH cFin_status,edi_trigger.errorflag WITH .T.;
			edi_trigger.when_proc WITH DATETIME() ;
			FOR edi_trigger.wo_num = nWO_Num AND edi_trigger.tripid = nTripId;
			AND edi_trigger.STYLE = cEventCode AND edi_trigger.edi_type ="214F"
		IF FILE(cFilename)
			DELETE FILE &cFilename
		ENDIF
		closefiles()
		errormail(cFin_status)
		THROW
	ELSE
		REPLACE edi_trigger.processed WITH .T.,edi_trigger.created WITH .T.,proc214 WITH .T.;
			edi_trigger.when_proc WITH DATETIME(),;
			edi_trigger.fin_status WITH "214F CREATED";
			edi_trigger.errorflag WITH .F.,file214 WITH cFilename ;
			FOR edi_trigger.wo_num = nWO_Num AND edi_trigger.tripid = nTripId;
			AND edi_trigger.STYLE = cEventCode AND edi_trigger.edi_type ="214F"
	ENDIF
ENDPROC

****************************
PROCEDURE errormail
****************************
	PARAMETERS cFin_status
	lDoCatch = .F.

	ASSERT .F. MESSAGE "At Errormail...Debug"
	tsubject = cCustName+" 214 EDI File Error"
	tmessage = "214 EDI File Error, WO# "+cWO_Num+CHR(13)
	tmessage = tmessage+CHR(13)+CHR(13)+"Error: "+cFin_status

	IF lTesting
		tmessage = tmessage + CHR(10)+CHR(10)+"This is TEST run..."
	ENDIF
	IF lEmail
		DO FORM dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
	ENDIF
ENDPROC

*********************
PROCEDURE closefiles
*********************
	IF USED('ftpedilog')
		USE IN ftpedilog
	ENDIF
	IF USED('serfile')
		USE IN serfile
	ENDIF
	IF USED('fedcodes')
		USE IN FEDCODES
	ENDIF
	IF USED('account')
		USE IN account
	ENDIF
	IF USED('xref')
		USE IN xref
	ENDIF
ENDPROC

****************************
PROCEDURE num_incr_isa
****************************

	IF !USED("serfile")
		USE ("F:\3pl\data\serial\"+cCustName+"214_serial") IN 0 ALIAS serfile
	ENDIF
	SELECT serfile
	IF lTesting
		nOrigSeq = serfile.seqnum
		nOrigGrpSeq = serfile.grpseqnum
		lISAFlag = .F.
	ENDIF
	nISA_Num = serfile.seqnum
	c_CntrlNum = ALLTRIM(STR(serfile.seqnum))
	REPLACE serfile.seqnum WITH serfile.seqnum + 1 IN serfile
ENDPROC

****************************
PROCEDURE num_incr_st
****************************

	IF !USED("serfile")
		USE ("F:\3pl\data\serial\"+cCustName+"214_serial") IN 0 ALIAS serfile
	ENDIF
	SELECT serfile
	IF lTesting
		nOrigSeq = serfile.seqnum
		nOrigGrpSeq = serfile.grpseqnum
		lSTFlag = .F.
	ENDIF
	c_GrpCntrlNum = ALLTRIM(STR(serfile.grpseqnum))
	REPLACE serfile.grpseqnum WITH serfile.grpseqnum + 1 IN serfile
ENDPROC

****************************
PROCEDURE segmentget
****************************

	PARAMETER thisarray,lcKey,nLength

	FOR i = 1 TO nLength
		IF i > nLength
			EXIT
		ENDIF
		lnEnd= AT("*",thisarray[i])
		IF lnEnd > 0
			lcThisKey =TRIM(SUBSTR(thisarray[i],1,lnEnd-1))
			IF OCCURS(lcKey,lcThisKey)>0
				RETURN SUBSTR(thisarray[i],lnEnd+1)
				i = 1
			ENDIF
		ENDIF
	ENDFOR

	RETURN ""

****************************
PROCEDURE close214
****************************
	STORE  "SE"+cfd+ALLTRIM(STR(nSegCtr+1))+cfd+PADL(c_GrpCntrlNum,9,"0")+csegd TO cString
	DO cstringbreak

	STORE  "GE"+cfd+ALLTRIM(STR(nSTCount))+cfd+c_CntrlNum+csegd TO cString
	DO cstringbreak

	STORE  "IEA"+cfd+"1"+cfd+PADL(c_CntrlNum,9,"0")+csegd TO cString
	DO cstringbreak

ENDPROC


****************************
PROCEDURE cstringbreak
****************************
	cLen = LEN(ALLTRIM(cString))
	=FPUTS(nHandle,cString)
ENDPROC
