* This program extracts returns qty by day for Steve Sykes
utilsetup("MJ_RETURNS_QTY_BY_DAY")
SET EXCLUSIVE OFF 
SET DELETED ON 
SET TALK OFF  
SET SAFETY OFF   &&Allows for overwrite of existing files

close databases all

goffice='J'
useca("inwolog","wh",,,"mod='"+goffice+"' and  accountid in (6303,6325,6543) and confirmdt>{"+dtoc(date()-10)+"}")
useca("indet","wh",,,"mod='"+goffice+"' and  accountid in (6303,6325,6543) and date_rcvd>{"+dtoc(date()-10)+"}")

*Wait window at 10,10 "Performing the first query...." nowait
SELECT SUM(PLUNITSINQTY),TTOD(CONFIRMDT) FROM INWOLOG WHERE container='PL' AND CONFIRMDT >=(date()-1) and CONFIRMDT<date()  GROUP BY CONFIRMDT INTO CURSOR RTRNTEMP
SELECT SUM(SUM_PLUNITSINQTY) as QTY,  EXP_2  as DATE FROM RTRNTEMP GROUP BY EXP_2 INTO CURSOR rtrntemp2
 SET STEP ON 
SELECT rtrntemp2
 
	If Reccount() > 0 
		export TO "c:\mj_returns_by_day"  TYPE xls
		tsendto = "tmarg@fmiint.com,steven.sykes@tollgroup.com"
		tattach = "c:\mj_returns_by_day.XLS" 
		tcc =""                                                                                                                               
		tFrom ="Toll EDI System Operations <fmi-transload-ops@fmiint.com>"                                
		tmessage = "MJ returns qty is complete for:  "+Ttoc(Datetime())        
		tSubject = "MJ returns qty is complete"                                                           
		Do Form m:\dev\frm\dartmail2 With tsendto,tFrom,tSubject, tcc,tattach,tmessage  ,"A"     
			
	ELSE 
		export TO "c:\mj_returns_by_day_do_not_exist" + TTOC(DATE()-1, 0)  TYPE xls
		tsendto ="tmarg@fmiint.com,steven.sykes@tollgroup.com"
		tattach = "" 
		tcc =""                                                                                                                               
		tFrom ="Toll EDI System Operations <fmi-transload-ops@fmiint.com>"                                
		tmessage = "MJ returns qty do not exist for:   "+Ttoc(Datetime())        
		tSubject = "MJ returns qty do not exist"                                                           
		Do Form m:\dev\frm\dartmail2 With tsendto,tFrom,tSubject, tcc,tattach,tmessage  ,"A"   

	ENDIF
close data all 

Wait window at 10,10 "all done...." timeout 2


schedupdate()
_screen.Caption=gscreencaption
on error

