Public lTestImport, gcEDIType

lTestImport = .F.
gcEDIType = "999"

Wait Window "Sears Upload: Upload begin.............." Nowait

lcpath = "f:\ftpusers\searsedi\edidatain\"
lcarchivepath = "f:\ftpusers\searsedi\edidatain\archive\"
Close Data All

*Copy File f:\ftpusers\searsedi\as2upload\*.* to 


*Set Step On 

cDelimiter = "*"
cTranslateOption = "NONE"
Do createx856a
Use F:\searsedi\Data\outpo In 0
Use F:\searsedi\Data\stores In 0

lnNum = Adir(tarray,lcpath+"*.txt")
*Set Step On 
If lnNum=0
  Messagebox("No Sears EDI file to import",16,"Sears EDI Import/Formatter",1500)
  Return
Endif

=ASORT(tArray,3)
If lnNum > 0
  For thisfile = 1  To lnNum
    xfile = lcpath+tarray[thisfile,1]
    archivefile = lcarchivepath+tarray[thisfile,1]
    Wait Window "Importing file: "+xfile Nowait
    lcOutstrx=Filetostr(xfile)
    If Substr(lcOutstrx,1,3) != "ISA"
      Wait Window At 10,10 "Not an EDI File !!.........."
      Return
    Endif
    delimchar = Substr(lcOutstrx,4,1)
    eolchar = Substr(lcOutstrx,106,1)

    Do loadedifile With xfile,delimchar,"SEARSEDI"
    Select x856

    Do While !Eof("x856")
      m.filename=tarray[thisfile,1]
      cSegment = Trim(x856.segment)
      cCode = Trim(x856.f1)

      If Trim(x856.segment) = "ISA"
        m.isa = Alltrim(x856.f13)
        Select x856
        Skip
        Loop
      Endif

      If Trim(x856.segment) = "GS"
        Select x856
        Skip
        Loop
      Endif

      If Trim(x856.segment) = "ST" And Trim(x856.f1) = "850"
        gcEDIType = "850"
        Do parse850out
      Endif
      If Trim(x856.segment) = "ST" And Trim(x856.f1) = "860"
        gcEDIType = "860"
        Do parse860out
      Endif
      If Trim(x856.segment) = "ST" And Trim(x856.f1) = "816"
        gcEDIType = "816"
        Do parse816out
      Endif
      Select x856
      Skip

    Enddo
&& here we shouldarchive and delete the files
    If !File(archivefile)
      Copy File [&xfile] To [&archivefile]
    Endif
    If !lTestImport
      Erase [&xfile]
    Endif
   
    Do WriteOutFile
 
  Next
Endif
***********************************************************************************************
Procedure WriteOutfile

If Inlist(gcEDIType ,"850","860")

  seqnum = "1001"
  tfile = "SEARSPODATA_"+Alltrim(Str(Year(Date())))+Padl(Alltrim(Str(Month(Date()))),2,"0")+;
    PADL(Alltrim(Str(Day(Date()))),2,"0")+Padl(Alltrim(Str(Hour(Datetime()))),2,"0")+;
    padl(Alltrim(Str(Minute(Datetime()))),2,"0")+Padl(Alltrim(Str(Sec(Datetime()))),2,"0")+"_"+seqnum+".txt"

  tfile = "f:\ftpusers\searsedi\RGTIDataOut\"+tfile

  handle=Fcreate(tfile)
  If !handle>0
    Messagebox("Cant create the output file",16,"Sears EDI Import/Formatter")
    Return
  Endif

  CTR = 1
  Select * From outpo Where sent =.F. Into Cursor temp
  Select temp
  polinectr=0
  Select temp
  Goto Top
  currentpo=temp.ponum
  Set Date To YMD

  Scan
    outstr= ""
    outstr= alltrim(Str(year(Datetime())))+Padl(month(Datetime()),2,"0")+Padl(day(Datetime()),2,"0")
    outstr= outstr+Padl(hour(Datetime()),2,"0")+Padl(minute(Datetime()),2,"0")+Padl(sec(Datetime()),2,"0")
    outstr= outstr+Padl(alltrim(Str(ctr)),7,"0")
    outstr= outstr+Padl(alltrim(isa),10,"0")
    outstr= outstr+Padl(alltrim(editype),6," ")
    outstr= outstr+potype
    outstr= outstr+pcode
    outstr= outstr+purpose
    outstr= outstr+ponum
    outstr= outstr+Strtran(Dtoc(shipdate),"/","")
    outstr= outstr+Strtran(Dtoc(reqdate),"/","")
    outstr= outstr+Strtran(Dtoc(newshipdt),"/","")
    outstr= outstr+dept
    outstr= outstr+vendorid
    outstr= outstr+Strtran(Dtoc(podate),"/","")
    outstr= outstr+Padr(Alltrim(shipdc),10," ")
    outstr= outstr+Padr(Alltrim(shipstore),10," ")
    outstr= outstr+Padr(Alltrim(billto),10," ")
    outstr= outstr+Padr(Alltrim(Style),20," ")
    outstr= outstr+Padr(Alltrim(upc),14," ")
    outstr= outstr+Padr(Alltrim(dept)+Alltrim(Style)+Alltrim(sku),17," ")  &&Padl(Alltrim(sku),17," ")
    outstr= outstr+Padl(Alltrim(Str(qty)),7,"0")
    outstr= outstr+Padl(Alltrim(Str(chngqty)),7,"0")
    outstr= outstr+chngcode
    outstr= outstr+chngdesc
    outstr= outstr+sac
    outstr= outstr+Strtran(Dtoc(dateloaded),"/","")
    outstr= outstr+filename
    outstr= outstr+Strtran(Dtoc(datesent),"/","")
    Fputs(handle,outstr)
    Ctr = Ctr+1
  Endscan

  Fclose(handle)

  Select outpo
  Replace datesent With Datetime() For sent = .F.
  Replace sent With .T. For sent = .F.
Endif

If Inlist(gcEDIType ,"816")

  seqnum = "1001"
  tfile = "SEARSSTOREDATA_"+Alltrim(Str(Year(Date())))+Padl(Alltrim(Str(Month(Date()))),2,"0")+;
    PADL(Alltrim(Str(Day(Date()))),2,"0")+Padl(Alltrim(Str(Hour(Datetime()))),2,"0")+;
    padl(Alltrim(Str(Minute(Datetime()))),2,"0")+Padl(Alltrim(Str(Sec(Datetime()))),2,"0")+"_"+seqnum+".txt"

  tfile = "f:\ftpusers\searsedi\RGTIDataOut\"+tfile

  handle=Fcreate(tfile)
  If !handle>0
    Messagebox("Cant create the output file",16,"Sears EDI Import/Formatter")
    Return
  Endif

  CTR = 0
  Select * From stores Where sent =.F. Into Cursor temp
  Select temp
  Select temp
  Goto Top
  Set Date To YMD

  Scan
    outstr = ""
    outstr = "CSTM"+"A"+"12"
    outstr= outstr+Padl(Alltrim(storenum),5,"0")
    outstr= outstr+Padr(Alltrim(storename),30," ")
    outstr= outstr+Padr(Alltrim(storealias),30," ")
    outstr= outstr+Padr(Alltrim(addr1),30," ")
    outstr= outstr+Padr(Alltrim(addr2),30," ")
    outstr= outstr+Padr(Alltrim(city),30," ")
    outstr= outstr+state
    outstr= outstr+Padr(Alltrim(zip),10," ")
    outstr= outstr+Space(15)
    outstr= outstr+Space(8)
    outstr= outstr+Space(10)
    outstr= outstr+Space(3)
    outstr= outstr+Space(5)
    Fputs(handle,outstr)
  Endscan

  Fclose(handle)

  Select stores
  Replace datesent   With Datetime() For sent = .F.
  Replace dateloaded With Datetime() For sent = .F.
  Replace sent       With .T. For sent = .F.
Endif
Endproc

************************************************************************************************
Procedure parse850out

Do clear_variables With "ALL"

Select x856
Skip

Do While !InList(Trim(x856.segment),"SE","GE","IEA")
  m.editype="850"
  If Trim(x856.segment) = "BEG"  && new PO
    m.ponum = Alltrim(f3)
    m.potype=Alltrim(f2)
    lcDate = Alltrim(x856.f5)
    lxDate = Ctod(Substr(lcDate,5,2)+"/"+Substr(lcDate,7,2)+"/"+Left(lcDate,4))
    m.podate = lxDate
  Endif
  If Trim(x856.segment) = "REF" And Alltrim(f1)="DP"
    m.dept = Alltrim(f2)
  Endif
  If Trim(x856.segment) = "REF" And Alltrim(f1)="IA"
    m.vendorid = Alltrim(f2)
  Endif
  If Trim(x856.segment) = "SAC"
    m.sac = Alltrim(f15)
  Endif
  If Trim(x856.segment) = "DTM"
    lcDate = Alltrim(x856.f2)
    lxDate = Ctod(Substr(lcDate,5,2)+"/"+Substr(lcDate,7,2)+"/"+Left(lcDate,4))
    m.shipdate = lxDate
  Endif
  If Trim(x856.segment) = "N1" And Alltrim(f1)="BY"
    m.billto = Alltrim(f4)
  Endif
  If Trim(x856.segment) = "N1" And Alltrim(f1)="ST"
    m.shipdc = Alltrim(f4)
  Endif
  If Trim(x856.segment) = "N1" And Alltrim(f1)="Z7"
    m.shipstore = Alltrim(f4)
  Endif

  If Trim(x856.segment) = "PO1"
    Do While Trim(x856.segment) = "PO1"
      Do clear_variables With "DETAIL"
      m.dateloaded = Date()
      m.sent = .F.
      Do Case
      Case Alltrim(f6)="IN"
        m.style= Alltrim(x856.f7)
      Case Alltrim(f6)="IZ"
        m.sku= Alltrim(x856.f7)
      Case Alltrim(f6)="UP"
        m.upc  = Alltrim(x856.f7)
      Endcase
      Do Case
      Case Alltrim(f8)="IN"
        m.style= Alltrim(x856.f9)
      Case Alltrim(f8)="IZ"
        m.sku= Alltrim(x856.f9)
      Case Alltrim(f8)="UP"
        m.upc  = Alltrim(x856.f9)
      Endcase
      Do Case
      Case Alltrim(f10)="IN"
        m.style= Alltrim(x856.f11)
      Case Alltrim(f10)="IZ"
        m.sku= Alltrim(x856.f11)
      Case Alltrim(f10)="UP"
        m.upc  = Alltrim(x856.f11)
      Endcase
      m.qty  = Val(Alltrim(x856.f2))
      Select outpo
      Append Blank
      Gather Memvar
      Select x856
      Skip 1
    Enddo
  Endif
  Select x856
  Skip
Enddo
Endproc
************************************************************************************************
Procedure parse816out

Do clear_variables With "816"

Select x856
Skip

Do While !InList(Trim(x856.segment),"SE","GE","IEA")
  Do clear_variables With "816"
  m.editype="816"

  If Trim(x856.segment) = "N1" And Trim(x856.f1) = "BU"  && new Store
    m.storename = Alltrim(x856.f2)
    m.storenum = Alltrim(x856.f4)

    Skip 1 In x856  && got to N2
    m.storealias = Alltrim(x856.f1)

    Skip 1 In x856  && go to N3
    m.addr1 = Alltrim(x856.f1)
    If Len(Alltrim(x856.f2)) >=1
      m.addr2 = Alltrim(x856.f2)
    Endif

    Skip 1 In x856  && go to N4
    m.city = Alltrim(x856.f1)
    m.state = Alltrim(x856.f2)
    m.zip = Alltrim(x856.f3)

    Select stores
    Append Blank
    Gather Memvar

  Endif

  If Trim(x856.segment) = "CTT" And Alltrim(f1)="0"
    m.dateloaded = Date()
  Endif
  Select x856
  Skip
Enddo
Endproc

*************************************************************************************************************
Procedure parse860out

Do clear_variables With "ALL"

Select x856
Skip

Do While Trim(x856.segment) != "SE"
  m.editype="860"

  If Trim(x856.segment) = "BCH"  && new PO
    m.potype=Alltrim(f2)
    m.pcode=Alltrim(f1)
    lcDate = Alltrim(x856.f6)
    lxDate = Ctod(Substr(lcDate,5,2)+"/"+Substr(lcDate,7,2)+"/"+Left(lcDate,4))
    m.podate = lxDate
    lcDate = Alltrim(x856.f11)
    lxDate = Ctod(Substr(lcDate,5,2)+"/"+Substr(lcDate,7,2)+"/"+Left(lcDate,4))
    m.reqdate = lxDate
    Do Case
    Case m.pcode="01"
      m.purpose="CANCEL"
    Case m.pcode="04"
      m.purpose="CHANGE"
    Endcase
    m.ponum = Alltrim(f3)
  Endif
  If Trim(x856.segment) = "REF" And Alltrim(f1)="DP"
    m.dept = Alltrim(f2)
  Endif
  If Trim(x856.segment) = "REF" And Alltrim(f1)="IA"
    m.vendorid = Alltrim(f2)
  Endif
  If Trim(x856.segment) = "DTM"
    lcDate = Alltrim(x856.f2)
    lxDate = Ctod(Substr(lcDate,5,2)+"/"+Substr(lcDate,7,2)+"/"+Left(lcDate,4))
    m.newshipdt = lxDate
  Endif
  If Trim(x856.segment) = "N1" And Alltrim(f1)="BY"
    m.shipstore = Alltrim(f4)
  Endif
  If Trim(x856.segment) = "N1" And Alltrim(f1)="ST"
    m.shipdc = Alltrim(f4)
  Endif
  If Trim(x856.segment) = "N1" And Alltrim(f1)="Z7"
    m.billto = Alltrim(f4)
  Endif
  If Trim(x856.segment) = "SAC"
    m.sac = Alltrim(f15)
  Endif

  If Trim(x856.segment) = "POC"
    Do While Trim(x856.segment) = "POC"
      Do clear_variables With "DETAIL"
      m.dateloaded = Date()
      m.sent = .F.
      m.chngcode = Alltrim(x856.f2)
      Do Case
      Case m.chngcode = "AI"
        m.chngdesc="ADDITEM"
      Case m.chngcode = "CA"
        m.chngdesc="CHANGEITEM"
      Case m.chngcode = "CT"
        m.chngdesc="CHANGEDATE"
      Case m.chngcode = "DI"
        m.chngdesc="DELETEITEM"
      Case m.chngcode = "PC"
        m.chngdesc="PRICECHANG"
      Case m.chngcode = "QD"
        m.chngdesc="DECQTY"
      Case m.chngcode = "QI"
        m.chngdesc="INCQTY"
      Endcase
      m.qty     = Val(Alltrim(x856.f3))
      m.chngqty = Val(Alltrim(x856.f4))

      Do Case
      Case Alltrim(f8)="IN"
        m.style= Alltrim(x856.f9)
      Case Alltrim(f8)="IZ"
        m.sku= Alltrim(x856.f9)
      Case Alltrim(f8)="UP"
        m.upc  = Alltrim(x856.f9)
      Endcase

      Do Case
      Case Alltrim(f10)="IN"
        m.style= Alltrim(x856.f11)
      Case Alltrim(f10)="IZ"
        m.sku= Alltrim(x856.f11)
      Case Alltrim(f10)="UP"
        m.upc  = Alltrim(x856.f11)
      Endcase

      Do Case
      Case Alltrim(f12)="IN"
        m.style= Alltrim(x856.f13)
      Case Alltrim(f12)="IZ"
        m.sku= Alltrim(x856.f13)
      Case Alltrim(f12)="UP"
        m.upc  = Alltrim(x856.f13)
      Endcase

      If m.chngcode = "CT"
        Select x856
        Skip 1
        If Alltrim(x856.segment)!= "DTM"
          Messagebox("860 upload sequence error, no DTM after a line item date change",16,"Sears EDI Import/Formatter",1500)
          Return
        Endif
        lcDate = Alltrim(x856.f2)
        lxDate = Ctod(Substr(lcDate,5,2)+"/"+Substr(lcDate,7,2)+"/"+Left(lcDate,4))
        m.newshipdt = lxDate
      Endif
      Select outpo
      Append Blank
      Gather Memvar
      Select x856
      Skip 1
    Enddo
  Endif

  If Trim(x856.segment) = "CTT" And Alltrim(f1)="0"
    m.dateloaded = Date()
    Select outpo
    Append Blank
    Gather Memvar
  Endif
  Select x856
  Skip
Enddo
Endproc
************************************************************************************************
Procedure clear_variables
Parameters whichtype

If Inlist(whichtype,"ALL","HEADER")
  m.dateloaded = Date()
  m.sent = .F.
  m.ponum = ""
  m.chngcode=""
  m.chngdesc=""
  m.purpose =""
  m.pcode=""
  m.dept= ""
  m.vendorid =""
  m.shipdc=""
  m.shipstore=""
  m.billto=""
  m.sac=""
Endif

If Inlist(whichtype,"ALL","DETAIL")
  m.chngqty=0
  m.style=""
  m.sku=""
  m.upc=""
  m.qty=0
Endif

If Inlist(whichtype,"816")
  m.storename=""
  m.storenum =""
  m.storealias=""
  m.addr1=""
  m.addr2=""
  m.state=""
  m.zip=""
Endif

Endproc
********************************************************************


*!*	   outstr = "OF"+"WD000"
*!*	   cycle="000"
*!*	   file_seq="0001"
*!*	   rec_seq =Padl(Alltrim(Str(ctr)),7," ")
*!*	   trans_date = Str(Year(Date()))+Padl(Alltrim(Str(month(Date()))),2,"0")+Padl(Alltrim(Str(Day(Date()))),2,"0")
*!*	   trans_time = Padl(alltrim(Str(hour(Date()))),2,"0")+Padl(Alltrim(Str(minute(Date()))),2,"0")+Padl(alltrim(Str(Seconds())),2," ")
*!*	   from_node ="SH"
*!*	   to_node ="02"
*!*
*!*	   cntr_type = "W"
*!*	   company ="12"
*!*	   pool   = Padl(alltrim(temp.shipdc),10," ")
*!*	   Store  = Padl(alltrim(temp.shipstore),10," ")
*!*	   zsku   = Padl(alltrim(temp.sku),17," ")
*!*	   record_cd="12"
*!*	   zupc   = Padl(alltrim(temp.upc),14," ")
*!*	   zponum = Padl(alltrim(temp.ponum),16," ")
*!*	   zshipcode="A"
*!*	   zqty =  Padl(alltrim(Str(temp.qty)),7," ")
*!*	   cube = Replicate("0",5)
*!*	   orderid = Space(16)
*!*	   orderline=Padl(Alltrim(Str(polinectr)),4," ")
*!*	   order_type = Space(2)
*!*	   tpurpose=Padl(temp.purpose,10," ")
*!*	   tchngdesc=Padl(temp.chngdesc,10," ")
*!*	   user       = Space(57)
*!*	   adj_priority = Replicate("0",7)
*!*	   priority     = Replicate("0",7)
*!*	   outstr = outstr +cycle+file_seq+trans_date+trans_time+from_node+to_node
*!*	   outstr = outstr +cntr_type+company+pool+store+zsku+record_cd+zupc+zponum+zshipcode+zqty+cube+orderid+orderline+order_type
*!*	   outstr = outstr +alltrim(temp.editype)+tpurpose+tchngdesc+user+adj_priority+priority



********************************************************************
