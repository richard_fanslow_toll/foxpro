* Daily inbound file for Lifefactory

utilsetup("LIFEINBOUND")

if holiday(date())
	return
endif

if date()={3/31/17}
	xdate={3/30/17}
else
	xdate=date()
endif

xsqlexec("select * from account where inactive=0","account",,"qq")
index on accountid tag accountid
set order to

goffice="I"

*!*	** testing
*!*	xdate={^2017-11-13}

*!*		select inwolog.* ;
*!*			from inwolog, indet where inwolog.inwologid=indet.inwologid and inwolog.accountid=6034 and date_rcvd=xdate AND inwolog.wo_num = 3200228 into cursor xrpt
*!*			
*!*			SELECT xrpt
*!*			BROWSE
*!*			USE IN xrpt
*!*	*testing


*!*	select date_rcvd, iif(sp,"SPECIAL PROJECT","OTHER          ") as type, style, color, id, sum(totqty) as qty ;
*!*		from inwolog, indet ;
*!*		where inwolog.inwologid=indet.inwologid ;
*!*		and inwolog.accountid=6034 ;
*!*		and date_rcvd=date() ;
*!*		group by 2,3,4,5 ;
*!*		order by 2 descending, 3,4,5 ;
*!*		into cursor xrpt

* add wo_num per CM 08/24/2015
* added indet.units = TRUE plus several header fields per CM 1/6/2017 MB


xsqlexec("select date_rcvd, sp, inwolog.wo_num, style, color, id, container, brokerref, seal, reference, acct_ref, printcomments, totqty " + ;
	"from inwolog, indet where inwolog.inwologid=indet.inwologid and inwolog.accountid=6034 " + ;
	"and indet.units=1 and date_rcvd={"+dtoc(xdate)+"}","xdytemp",,"wh")

select date_rcvd, iif(sp,"SPECIAL PROJECT","OTHER          ") as type, wo_num, ;
	style, color, id, container, brokerref, seal, reference, acct_ref, left(printcomments,100) as pcomments, ;
	sum(totqty) as qty from xdytemp ;
	group by 2,3,4,5,6,7,8,9,10,11 order by 2 descending, 3,4,5,6 into cursor xrpt
	
delete file h:\fox\lifeinbound.csv
xfilename="h:\fox\lifeinbound.csv"

select xrpt
copy to (xfilename) csv
set step on 
tattach=xfilename
tsendto="Greg@Lifefactory.com, chris.malcolm@tollgroup.com, ethan@lifefactory.com, vlad@lifefactory.com, joe@lifefactory.com"
*tsendto="cmalcolm@fmiint.com"
*tsendto="dyoung@fmiint.com"
*tsendto="mbennett@fmiint.com"
tcc=""
tFrom ="TGF Operations <fmi-transload-ops@fmiint.com>"
tmessage = "See attached files"
tSubject = "Daily inbound report"
set step on 
Do Form dartmail2 With tsendto,tFrom,tSubject," ",tattach,tmessage,"A"

schedupdate()

_screen.Caption=gscreencaption
on error
