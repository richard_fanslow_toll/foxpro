*!*	Quarterly odometer report for April Fraze.
* show trucks in div 02 and 03 9 quarters back
* as of end quarter dates like : 12/31/2015...03/31/2016 etc.

SET EXCLUSIVE OFF
SET CENTURY ON
SET DATE AMERICAN


LOCAL lnRow, lcRow, lcFiletoSaveAs, lcSpreadsheetTemplate, lnNumberOfErrors
LOCAL lcErr, lcTitle, loError, lnRate, ldToday
LOCAL lcDivision, lnLastRow, lnRate, lcDetailBasisFile
LOCAL ldToday, i, ldDay

PRIVATE poExcel, poWorkbook, poWorksheet, pnNumQuarters, pdLastQTRDate, pdEarliestCRDate

pnNumQuarters = 9

ldToday = DATE()

pdEarliestCRDate = {^2014-01-01}

* determine ending qtr date to report on
ldDay = ldToday
FOR i = 1 TO 100
	IF MONTH(ldDay) = 3 AND DAY(ldDay) = 31 THEN
		pdLastQTRDate = ldDay
		EXIT
	ENDIF
	IF MONTH(ldDay) = 12 AND DAY(ldDay) = 31 THEN
		pdLastQTRDate = ldDay
		EXIT
	ENDIF
	IF MONTH(ldDay) = 9 AND DAY(ldDay) = 30 THEN
		pdLastQTRDate = ldDay
		EXIT
	ENDIF
	IF MONTH(ldDay) = 6 AND DAY(ldDay) = 30 THEN
		pdLastQTRDate = ldDay
		EXIT
	ENDIF
	ldDay = ldDay - 1
ENDFOR

*SET STEP ON 

lcSpreadsheetTemplate = "F:\UTIL\SHOPREPORTS\TEMPLATES\SHOPODOMETER_TEMPLATE.XLSX"

lcFiletoSaveAs = "F:\UTIL\SHOPREPORTS\REPORTS\SHOPODOMETERTRENDING_for_" + DTOS(pdLastQTRDate) + ".XLSX"


USE F:\SHOP\SHDATA\ordhdr AGAIN IN 0 ALIAS TORDHDR
USE F:\SHOP\SHDATA\TRUCKS AGAIN IN 0 ALIAS TTRUCKS
*!*					USE F:\SHOP\SHDATA\trailer IN 0
*!*					USE F:\SHOP\SHDATA\chaslist IN 0
*!*					USE F:\SHOP\SHDATA\mainttyp IN 0
*!*					USE F:\SHOP\SHDATA\repcause IN 0

SELECT A.ORDNO, A.VEHNO, A.CRDATE, A.MILEAGE, .F. AS ACTIVE, "  " AS DIVISION ;
	FROM Tordhdr A ;
	INTO CURSOR CURORDHDRPRE ;
	WHERE A.CRDATE >= pdEarliestCRDate ;
	AND (NOT EMPTY(A.ORDNO)) ;
	AND (NOT EMPTY(A.VEHNO)) ;
	AND A.TYPE = "TRUCK  " ;
	ORDER BY A.CRDATE ;
	READWRITE

*!*		WHERE A.CRDATE >= ld2YearsAgo ;

*!*					* POPULATE RATE BY CRDATE AND CALC LABOR COST FOR EACH WO REC
*!*					SELECT CURORDHDRPRE
*!*					SCAN
*!*						lnRate = .GetHourlyRateByCRDate( CURORDHDRPRE.crdate )
*!*						REPLACE CURORDHDRPRE.RATE WITH lnRate, CURORDHDRPRE.TOTLCOST WITH ( CURORDHDRPRE.tothours * lnRate ) IN CURORDHDRPRE
*!*					ENDSCAN

SELECT CURORDHDRPRE
SCAN

	WAIT WINDOW "checking Vehicle #" + TRANSFORM(CURORDHDRPRE.VEHNO) NOWAIT

	SELECT Ttrucks
	LOCATE FOR ALLTRIM(truck_num) == ALLTRIM(CURORDHDRPRE.VEHNO)
	IF FOUND() THEN
		REPLACE CURORDHDRPRE.DIVISION WITH Ttrucks.DIVISION, CURORDHDRPRE.ACTIVE WITH Ttrucks.ACTIVE IN CURORDHDRPRE
	ENDIF

ENDSCAN
SELECT Ttrucks
LOCATE

*!*					* POPULATE MONTH/YEAR FIELDS
*!*					SELECT CURORDHDRPRE
*!*					SCAN
*!*						REPLACE CURORDHDRPRE.NMONTH WITH MONTH(CRDATE), CURORDHDRPRE.CMONTH WITH CMONTH(CRDATE), CURORDHDRPRE.NYEAR WITH YEAR(CRDATE) IN CURORDHDRPRE
*!*					ENDSCAN

*!*	SELECT CURORDHDRPRE
*!*	browse
*!*					* detail as needed
*!*					lcDetailBasisFile = "C:\A\SHOPCOSTSTRENDING_DETAIL_for_" + .cReportPeriod + ".XLS"
*!*					SELECT CURORDHDRPRE
*!*					COPY TO (lcDetailBasisFile) XL5 && FOR (DIVISION = '02') AND (NMONTH = 2)



WAIT CLEAR

poExcel = CREATEOBJECT("excel.application")
poExcel.displayalerts = .F.
poExcel.VISIBLE = .F.

poWorkbook = poExcel.workbooks.OPEN(lcSpreadsheetTemplate) &&
poWorkbook.SAVEAS(lcFiletoSaveAs)


poWorksheet = poWorkbook.Worksheets[1]
ProcessDivision('02')

poWorksheet = poWorkbook.Worksheets[2]
ProcessDivision('03')


poWorkbook.SAVE()

poExcel.QUIT()

*!*
*!*					.cAttach = lcFiletoSaveAs


*!*					.cAttach = .cAttach + "," + lcDetailBasisFile
*!*
*!*
*!*
*!*					*.CreateRowsMaintType()
*!*

IF USED('CURORDHDRPRE') THEN 
	USE IN CURORDHDRPRE
ENDIF

IF USED('TORDHDR') THEN
	USE IN TORDHDR
ENDIF

IF USED('TTRUCKS') THEN
	USE IN TTRUCKS
ENDIF

IF USED('CURORDERS') THEN
	USE IN CURORDERS
ENDIF

IF USED('CURVEHNO') THEN
	USE IN CURVEHNO
ENDIF

RETURN


FUNCTION ProcessDivision
	LPARAMETERS tcCurrentDiv

	LOCAL lnRow, lcRow, i, ldDate, lcCol
	
	IF USED('CURORDERS') THEN
		USE IN CURORDERS
	ENDIF
	
	IF USED('CURVEHNO') THEN
		USE IN CURVEHNO
	ENDIF

	
	SELECT * ;
	FROM CURORDHDRPRE ;
	INTO CURSOR CURORDERS ;
	WHERE DIVISION = tcCurrentDiv ;
	ORDER BY CRDATE ;
	READWRITE
	
	SELECT CURORDERS
	INDEX ON CRDATE TAG CRDATE
	INDEX ON VEHNO TAG VEHNO
	
	*BROWSE
	
	
	SELECT DISTINCT VEHNO ;
	FROM CURORDERS ;
	INTO CURSOR CURVEHNO ;
	ORDER BY VEHNO 

*!*		WHERE ACTIVE ;

*!*		SELECT CURVEHNO
*!*		BROW
	
	* fill in column headers with the quarter dates
	*ldDate = pdLastQTRDate
	ldDate = GOMONTH(pdLastQTRDate + 1,3) - 1
	FOR i = 1 TO pnNumQuarters
		ldDate = GOMONTH(ldDate + 1,-3) - 1
		lcCol = SUBSTR("JIHGFEDCB",i,1)
		poWorksheet.RANGE(lcCol + "3").VALUE = ldDate
	ENDFOR

	lnRow = 3

	SELECT CURVEHNO
	SCAN
	
		lnRow = lnRow + 1
		lcRow = ALLTRIM(STR(lnRow))
		poWorksheet.RANGE("A"+lcRow).VALUE = "'" + CURVEHNO.VEHNO 

		ldDate = GOMONTH(pdLastQTRDate + 1,3) - 1

		FOR i = 1 TO pnNumQuarters
			ldDate = GOMONTH(ldDate + 1,-3) - 1
			lcCol = SUBSTR("JIHGFEDCB",i,1)
			
			SELECT MILEAGE ;
			FROM CURORDERS ;
			INTO CURSOR CURMILEAGE ;		
			WHERE VEHNO == CURVEHNO.VEHNO ;
			AND CRDATE IN (SELECT MAX(CRDATE) FROM CURORDERS WHERE (VEHNO == CURVEHNO.VEHNO) AND CRDATE <= ldDate)
			
			IF USED('CURMILEAGE') AND NOT EOF('CURMILEAGE') THEN
				* work order for vehno and <= qtr date was found; enter mileage in a cell in the spreadsheet
				poWorksheet.RANGE(lcCol + lcRow).VALUE = CURMILEAGE.MILEAGE			
			ENDIF
		
			IF USED('CURMILEAGE') THEN
				USE IN CURMILEAGE
			ENDIF
			
		ENDFOR
	
	ENDSCAN
	
	
	*!*
	RETURN
ENDFUNC && ProcessDivision
