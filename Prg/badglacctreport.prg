* create XL file of ADP employees missing one or more of:
* eeocjobclass  CLASSIF
* Custarea1  XXX where 1st X = W or B for white collar or blue collar, and last 2 Xs are TGF dept code.
* Custarea2  P or F for part-time or full-time, if not = P assume F
* Custarea3	 WORKSITE

* build EXE in:  F:\UTIL\ADPEDI\

utilsetup("BADGLACCTREPORT")

LOCAL loloBadCustomFields
loloBadCustomFields = CREATEOBJECT('loBadCustomFields')
loloBadCustomFields.MAIN()

schedupdate()

CLOSE DATA
RETURN

#DEFINE LOGIT 1
#DEFINE WAITIT 2
#DEFINE NOWAITIT 4
#DEFINE SENDIT 8
#DEFINE CRLF CHR(13) + CHR(10)
#DEFINE RETURN_DATA_MANDATORY .F.
#DEFINE RETURN_DATA_NOT_MANDATORY .T.

DEFINE CLASS loBadCustomFields AS CUSTOM

	cProcessName = 'loBadCustomFields'

	lTestMode = .F.  && .F. FOR NORMAL OPERATION, .T. TO SUPPRESS ALL BUT DEVELOPER EMAILS AND CHANGE INPUT TABLE, ETC.

	lAutoYield = .T.

	cStartTime = TTOC(DATETIME())

	dToday = DATE()

	* Output file
	cOutputFile = "F:\UTIL\ADPEDI\REPORTS\MISSING_ADP_CUSTOM_FIELDS_" + STRTRAN(DTOC(DATE()),"/","-") + ".XLS"

	* connection properties
	nHandle = 0

	* wait window properties
	nWaitWindowTimeout = 2
	lWaitWindowIsOn = .T.

	* logfile properties
	lLoggingIsOn = .T.
	cLogFile = 'F:\UTIL\ADPEDI\Logfiles\MISSING_ADP_CUSTOM_FIELDS_LOG.txt'

	* INTERNAL email properties
	lSendInternalEmailIsOn = .T.
	cFrom = 'TGF Corporate <fmicorporate@fmiint.com>'
	cSendTo = 'mbennett@fmiint.com'
	cCC = ''
	cSubject = 'MISSING ADP CUSTOM FIELDS Report Results for: ' + TTOC(DATETIME())
	cAttach = ''
	cBodyText = ''

	cCOMPUTERNAME = ''
	cUSERNAME = ''

	FUNCTION INIT
		IF NOT DODEFAULT()
			RETURN .F.
		ENDIF
		WITH THIS
			*SET RESOURCE OFF
			CLOSE DATA
			SET CENTURY ON
			SET DATE YMD
			SET HOURS TO 24
			SET ANSI ON
			SET TALK OFF
			SET DELETED ON
			SET CONSOLE OFF
			SET EXCLUSIVE OFF
			SET SAFETY OFF
			SET EXACT OFF
			SET STATUS BAR OFF
			SET SYSMENU OFF
			SET ENGINEBEHAVIOR 70
			_VFP.AUTOYIELD = .lAutoYield
			.cCOMPUTERNAME = GETENV("COMPUTERNAME")
			.cUSERNAME = GETENV("USERNAME")
			.lLoggingIsOn = .lLoggingIsOn AND NOT EMPTY(.cLogFile)
			IF .lLoggingIsOn THEN
				SET ALTERNATE TO (.cLogFile) ADDITIVE
				SET ALTERNATE ON
			ENDIF
		ENDWITH
	ENDFUNC


	FUNCTION DESTROY
		WITH THIS
			IF .lLoggingIsOn  THEN
				SET ALTERNATE OFF
				SET ALTERNATE TO
			ENDIF
		ENDWITH
		DODEFAULT()
	ENDFUNC


	FUNCTION MAIN
		WITH THIS
			LOCAL lnNumberOfErrors, lcSQL, loError, lnThisMonth, lnThisYear, lnLastMonth, lnLastYear, lcTopBodyText
			TRY
				lnNumberOfErrors = 0
				lnRecsAdded = 0
				lcTopBodyText = ''

				.TrackProgress('', LOGIT)
				.TrackProgress('', LOGIT)
				.TrackProgress('=========================================================', LOGIT+SENDIT)
				.TrackProgress('MISSING ADP CUSTOM FIELDS Report process started.', LOGIT+SENDIT)
				.TrackProgress('COMPUTERNAME = ' + .cCOMPUTERNAME, LOGIT+SENDIT)
				.TrackProgress('USERNAME = ' + .cUSERNAME, LOGIT+SENDIT)
				.TrackProgress('=========================================================', LOGIT+SENDIT)
				.TrackProgress('', LOGIT)
				.TrackProgress('Extracting SS #s from ADP...', LOGIT+SENDIT+NOWAITIT)

				* construct month/year values for SELECT
				lnThisMonth = MONTH(.dToday)
				lnThisYear = YEAR(.dToday)

				IF lnThisMonth > 1 THEN
					lnLastMonth = lnThisMonth - 1
					lnLastYear = lnThisYear
				ELSE
					lnLastMonth = 12
					lnLastYear = lnThisYear - 1
				ENDIF

				* get ss#s from ADP
				OPEN DATABASE F:\UTIL\ADPCONNECTION\CONNECT1

				.nHandle = SQLCONNECT("PCPAYWIN","MBENNETT","VP737A")

				IF .nHandle > 0 THEN

					lcSQL = ;
						"SELECT " + ;
						" COMPANYCODE AS ADPCOMP, " + ;
						" HOMEDEPARTMENT, " + ;
						" FILE# AS FILENUM, " + ;
						" SOCIALSECURITY# AS SS_NUM, " + ;
						" NAME, " + ;
						" STATUS, " + ;
						" TERMINATIONDATE, " + ;
						" {fn IFNULL(EEOCJOBCLASS,' ')} AS CLASSIF, " + ;
						" {fn IFNULL(CUSTAREA1,'   ')} AS TGFCODE, " + ;
						" {fn IFNULL(CUSTAREA2,' ')} AS TIMETYPE, " + ;
						" {fn IFNULL(CUSTAREA3,'   ')} AS WORKSITE " + ;
						" FROM REPORTS.V_EMPLOYEE " + ;
						" WHERE COMPANYCODE IN ('E87','E88','E89') "

					lcSQL2 = ;
						" SELECT DISTINCT " + ;
						" COMPANYCODE AS ADPCOMP, " + ;
						" FILE# AS FILENUM, " + ;
						" SOCIALSECURITY# AS SS_NUM " + ;
						" FROM REPORTS.V_CHK_VW_INFO " + ;
						" WHERE " + ;
						" ( " + ;
						" {fn MONTH(CHECKVIEWPAYDATE)} = " + TRANSFORM(lnThisMonth) + ;
						" AND {fn YEAR(CHECKVIEWPAYDATE)} = " + TRANSFORM(lnThisYear) + ;
						" ) " + ;
						" OR " + ;
						" ( " + ;
						" {fn MONTH(CHECKVIEWPAYDATE)} = " + TRANSFORM(lnLastMonth) + ;
						" AND {fn YEAR(CHECKVIEWPAYDATE)} = " + TRANSFORM(lnLastYear) + ;
						" ) "

*!*						lcSQL2 = ;
*!*							" SELECT DISTINCT " + ;
*!*							" COMPANYCODE AS ADPCOMP, " + ;
*!*							" FILE# AS FILENUM, " + ;
*!*							" SOCIALSECURITY# AS SS_NUM " + ;
*!*							" FROM REPORTS.V_CHK_VW_INFO " + ;
*!*							" WHERE " + ;
*!*							" ( " + ;
*!*							" {fn MONTH(CHECKVIEWPAYDATE)} = " + TRANSFORM(lnThisMonth) + ;
*!*							" AND {fn YEAR(CHECKVIEWPAYDATE)} = " + TRANSFORM(lnThisYear) + ;
*!*							" ) "

					.TrackProgress('', LOGIT+SENDIT)
					.TrackProgress('lcSQL = ' + lcSQL, LOGIT+SENDIT)
					.TrackProgress('', LOGIT+SENDIT)
					.TrackProgress('lcSQL2 = ' + lcSQL2, LOGIT+SENDIT)

					IF USED('SQLCURSOR1') THEN
						USE IN SQLCURSOR1
					ENDIF
					IF USED('SQLCURSOR2') THEN
						USE IN SQLCURSOR2
					ENDIF

					IF .ExecSQL(lcSQL, 'SQLCURSOR1', RETURN_DATA_MANDATORY) ;
							AND .ExecSQL(lcSQL2, 'SQLCURSOR2', RETURN_DATA_MANDATORY) THEN
*!*								
*!*								
*!*							SELECT SQLCURSOR1
*!*							BROWSE FOR EMPTY(WORKSITE)
*!*							SELECT SQLCURSOR2
*!*							BROWSE

						IF USED('SQLCURSOR1A') THEN
							USE IN SQLCURSOR1A
						ENDIF

						* now filter list of employees by those who are in SQLCURSOR2
						* AND who have an empty classif/JOBCLASS or worksite.
*!*							SELECT ;
*!*								A.ADPCOMP, ;
*!*								A.FILENUM, ;
*!*								A.SS_NUM, ;
*!*								LEFT(A.HOMEDEPARTMENT,2) AS DIVISION, ;
*!*								SUBSTR(A.HOMEDEPARTMENT,3,4) AS DEPARTMENT, ;
*!*								A.NAME, ;
*!*								A.STATUS, ;
*!*								A.TERMINATIONDATE, ;
*!*								A.CLASSIF, ;
*!*								A.EEOCCLASS, ;
*!*								A.WORKSITE ;
*!*								FROM SQLCURSOR1 A ;
*!*								JOIN SQLCURSOR2 B ;
*!*								ON B.ADPCOMP = A.ADPCOMP ;
*!*								AND B.FILENUM = A.FILENUM ;
*!*								AND B.SS_NUM = A.SS_NUM ;
*!*								INTO CURSOR SQLCURSOR1A ;
*!*								WHERE (EMPTY(A.CLASSIF) OR EMPTY(A.EEOCCLASS) OR EMPTY(A.WORKSITE)) ;
*!*								ORDER BY 1, 2
						SELECT ;
							A.ADPCOMP, ;
							A.FILENUM, ;
							A.SS_NUM, ;
							LEFT(A.HOMEDEPARTMENT,2) AS DIVISION, ;
							SUBSTR(A.HOMEDEPARTMENT,3,4) AS DEPARTMENT, ;
							A.NAME, ;
							A.STATUS, ;
							A.TERMINATIONDATE, ;
							A.CLASSIF, ;
							A.TGFCODE, ;
							A.TIMETYPE, ;
							A.WORKSITE ;
							FROM SQLCURSOR1 A ;
							JOIN SQLCURSOR2 B ;
							ON B.ADPCOMP = A.ADPCOMP ;
							AND B.FILENUM = A.FILENUM ;
							AND B.SS_NUM = A.SS_NUM ;
							INTO CURSOR SQLCURSOR1A ;
							WHERE ( EMPTY(A.CLASSIF) OR ;
								EMPTY(A.WORKSITE) OR ;
								(LEN(ALLTRIM(A.TGFCODE)) <> 3) OR ;
								(NOT INLIST(LEFT(A.TGFCODE,1),'W','B')) );
							ORDER BY 1, 2

*!*									(NOT INLIST(A.TIMETYPE,' ','F','P')) OR ;

						SELECT SQLCURSOR1A
						GOTO TOP
						*BROWSE

						IF EOF('SQLCURSOR1A') THEN
*!*								lcTopBodyText = 'No employees who had recent paychecks were missing Classifs/EEOC Jobclass/Worksites in ADP.'
							lcTopBodyText = 'No employees had bad/missing Classifs/Worksites/TGFCodes in ADP.'
							lcTopBodyText = lcTopBodyText + CRLF + CRLF + 'No corrective action is required.'
							.TrackProgress('', LOGIT+SENDIT)
							.TrackProgress('No error data to send to output file!', LOGIT+SENDIT)
						ELSE
*!*								lcTopBodyText = 'The following employees who had recent paychecks were missing Classifs/EEOC Jobclass/Worksites in ADP:'
							lcTopBodyText = 'The following employees had bad/missing Classifs/Worksites/TGFCodes in ADP:'
							SCAN
*!*									lcTopBodyText = lcTopBodyText + CRLF + CRLF + ;
*!*										LEFT(SQLCURSOR1A.NAME,25) + ;
*!*										": COMP=[" + ALLTRIM(SQLCURSOR1A.ADPCOMP) + ;
*!*										"] FILENUM=[" + TRANSFORM(SQLCURSOR1A.FILENUM) + ;
*!*										"] CUSTAREA1/CLASSIF=[" + SQLCURSOR1A.CLASSIF + ;
*!*										"] EEOCJOBCLASS=[" + SQLCURSOR1A.EEOCCLASS + ;
*!*										"] CUSTAREA3/WORKSITE=[" + SQLCURSOR1A.WORKSITE + "]"
								lcTopBodyText = lcTopBodyText + CRLF + CRLF + ;
									LEFT(SQLCURSOR1A.NAME,25) + ;
									": COMP=[" + ALLTRIM(SQLCURSOR1A.ADPCOMP) + ;
									"] FILENUM=[" + TRANSFORM(SQLCURSOR1A.FILENUM) + ;
									"] EEOCJOBCLASS/CLASSIF=[" + SQLCURSOR1A.CLASSIF + ;
									"] CUSTAREA1/TGFCODE=[" + SQLCURSOR1A.TGFCODE + ;
									"] CUSTAREA3/WORKSITE=[" + SQLCURSOR1A.WORKSITE + "]"
							ENDSCAN

*!*										"] CUSTAREA2/TIMETYPE=[" + SQLCURSOR1A.TIMETYPE + ;

							*lcTopBodyText = 'Some employees who had recent paychecks were missing Classifs or EEOC Jobclass or Worksites in ADP!'
							*lcTopBodyText = lcTopBodyText + CRLF + CRLF + 'Please review the attached file!'
							*.TrackProgress('', LOGIT+SENDIT)
							*.TrackProgress('Copying to output file: ' + .cOutputFile, LOGIT+SENDIT)
							*COPY TO (.cOutputFile) XL5
							** email output file
							*.cAttach = .cOutputFile
						ENDIF

						CLOSE DATABASES ALL

					ENDIF  &&  .ExecSQL(lcSQL, 'SQLCURSOR1', RETURN_DATA_MANDATORY)

					.TrackProgress('', LOGIT+SENDIT)
					.TrackProgress('MISSING ADP CUSTOM FIELDS Report process ended normally.', LOGIT+SENDIT)

				ELSE
					* connection error
					.TrackProgress('Unable to connect to ADP Payroll System.', LOGIT+SENDIT)
				ENDIF   &&  .nHandle > 0

			CATCH TO loError

				.TrackProgress('There was an error.',LOGIT+SENDIT)
				.TrackProgress('The Error # is: ' + TRANSFORM(loError.ErrorNo), LOGIT+SENDIT)
				.TrackProgress('The Error Message is: ' + TRANSFORM(loError.MESSAGE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred in Program: ' + TRANSFORM(loError.PROCEDURE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred at line #: ' + TRANSFORM(loError.LINENO), LOGIT+SENDIT)
				lnNumberOfErrors = lnNumberOfErrors + 1
				CLOSE DATA

			ENDTRY

			CLOSE DATA
			WAIT CLEAR
			***************** INTERNAL email results ******************************
			.TrackProgress('About to send status email.',LOGIT)
			.TrackProgress('The logfile is: ' + .cLogFile, SENDIT)
			.TrackProgress('MISSING ADP CUSTOM FIELDS Report process started: ' + .cStartTime, LOGIT+SENDIT)
			.TrackProgress('MISSING ADP CUSTOM FIELDS Report process finished: ' + TTOC(DATETIME()), LOGIT+SENDIT)

			* put some text at top of body
			.cBodyText = lcTopBodyText + CRLF + CRLF + ;
				"(Report log follows)" + ;
				CRLF + CRLF + .cBodyText

			IF .lSendInternalEmailIsOn THEN
				DO FORM M:\DEV\FRM\dartmail2 WITH .cSendTo,.cFrom,.cSubject,.cCC,.cAttach,.cBodyText,"A"
				.TrackProgress('Sent status email.',LOGIT)
			ELSE
				.TrackProgress('Did not send status email: THIS.lSendInternalEmailIsOn = FALSE.',LOGIT)
			ENDIF

		ENDWITH
		RETURN
	ENDFUNC && main


	FUNCTION ExecSQL
		LPARAMETERS tcSQL, tcCursorName, tlNoDataReturnedIsOkay
		LOCAL llRetval, lnResult
		WITH THIS
			* close target cursor if it's open
			IF USED(tcCursorName)
				USE IN (tcCursorName)
			ENDIF
			WAIT WINDOW "Fetching remote data; please wait..." NOWAIT
			lnResult = sqlexec(.nHandle, tcSQL, tcCursorName)
			llRetval = ( lnResult > 0 )
			IF llRetval THEN
				* see if any data came back
				IF NOT tlNoDataReturnedIsOkay THEN
					IF (NOT USED(tcCursorName)) OR EOF(tcCursorName) THEN
						llRetval = .F.
						.TrackProgress('No data was returned by this query: ' +CRLF + tcSQL, LOGIT+SENDIT)
					ENDIF
				ENDIF
			ELSE
				.TrackProgress("Result Code [" + TRANSFORM(lnResult) + "] returned by this query: " + tcSQL, LOGIT+SENDIT)
			ENDIF
			WAIT CLEAR
			RETURN llRetval
		ENDWITH
	ENDFUNC


	PROCEDURE TrackProgress
		* do any combination of Wait Window, writing to logfile, and adding to body of email,
		* based on nFlags parameter.
		LPARAMETERS tcExpression, tnFlags
		WITH THIS
			IF BITAND(tnFlags,LOGIT) = LOGIT THEN
				IF .lLoggingIsOn THEN
					?
					? .cProcessName + "  " + TTOC(DATETIME()) + ": " + tcExpression
				ENDIF
			ENDIF
			IF .lWaitWindowIsOn THEN
				IF BITAND(tnFlags,NOWAITIT) = NOWAITIT THEN
					WAIT WINDOW tcExpression NOWAIT
				ENDIF
				IF BITAND(tnFlags,WAITIT) = WAITIT THEN
					WAIT WINDOW tcExpression TIMEOUT .nWaitWindowTimeout
				ENDIF
			ENDIF
			IF BITAND(tnFlags,SENDIT) = SENDIT THEN
				IF .lSendInternalEmailIsOn THEN
					.cBodyText = .cBodyText + tcExpression + CRLF + CRLF
				ENDIF
			ENDIF
		ENDWITH
	ENDPROC  &&  TrackProgress

ENDDEFINE

