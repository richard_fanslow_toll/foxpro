parameters lwebsite

if (type("gldor")="L" and !gldor)
	dappt={3/10/16}
	xoffice="N"
	xaccountid=6209
endif

cwhpath = wf(xoffice, xaccountid)
xmod=wf(xoffice, xaccountid,,,,,,.t.)

if usesqloutwo()
else
	open database &cwhpath.wh
	use wh!outship in 0
	xsqlexec("select * from apptcall where mod='"+xmod+"' and accountid="+transform(xaccountid)+" and appt={"+dtoc(dappt)+"} and status='NO SHOW'",,,"wh")
endif
set step on 
if usesqloutwo(xmod)
	xsqlexec("select outship.outshipid, consignee, ship_ref, cnee_ref, outship.wo_num, wo_date, start, cancel, ctnqty, " + ;
		"outship.ship_via, apptcall.called, apptcall.appt, apptcall.appt_num, apptcall.remarks, sp, noshow, bol_no " + ;
		"from outship, apptcall where outship.outshipid=apptcall.outshipid and outship.mod='"+xmod+"' " + ;
		"and apptcall.appt={"+dtoc(dappt)+"} and apptcall.accountid="+transform(xaccountid)+" and apptcall.status='NO SHOW'","xdytemp",,"wh")

	select outshipid, consignee, ship_ref, cnee_ref, wo_num, wo_date, start, cancel, sum(ctnqty) as qty, ship_via, ;
		called, appt, appt_num, remarks as apptremarks, sp, noshow, bol_no ;
		from xdytemp group by cnee_ref order by 1,2,3 into cursor xrpt readwrite

else
	select o.outshipid, consignee, ship_ref, cnee_ref, o.wo_num, wo_date, start, cancel, sum(ctnqty) as qty, o.ship_via, ;
		a.called, a.appt, a.appt_num, a.remarks as apptremarks, sp, noshow, bol_no ;
		from apptcall a left join outship o on o.outshipid = a.outshipid ;
		where a.appt=dappt and a.accountid=xaccountid and a.status = "NO SHOW" ;
		group by cnee_ref order by 1,2,3 into cursor xrpt readwrite
endif

xheader="Missed Pickup Log"

if lwebsite
	loadwebpdf("noShows")
endif

if used("apptcall")
	use in apptcall
endif
if used("outship")
	use in outship
endif

if usesqloutwo()
else
	set database to wh
	close databases
endif