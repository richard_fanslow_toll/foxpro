Parameters cartontotest,Printflag

&&close data all 

Public wonum,whichpallet,Whichstyle,whichpack,whichcarton,cartonstatus,polystatus,innerstatus,whichuser,sumdata

if InList(Upper(vartype(printflag)),"L","U")
  SendtoPrinter = .f.
Else
  SendtoPrinter = .t.
EndIf 


If !Used("qcscans")
  Use f:\wh\qcscans In 0
EndIf

If !Used("moretdata")  
  Use f:\wh\moretdata In 0
EndIf   

Create Cursor rpt (;
  rdata char(80))

Create Cursor rpt2 (;
  sumdata2 char(80))

if InList(Upper(vartype(cartontotest)),"L","U")
  thiscarton = getwonumber()
  whichcarton = thiscarton
Else
  thiscarton = cartontotest
  whichcarton = thiscarton
EndIf 

Select rpt 
Zap
Select rpt2
Zap


cartonstatus = "PASSED"
polystatus   = "PASSED"  
innerstatus  = "PASSED"

cartonpassed = .t.
polypassed   = .t.
innerpassed  = .t.

Select qcscans

Select * from qcscans where cartonid = thiscarton  and polyscan into cursor pscans order by whichpoly desc 

If Reccount("pscans") =0
  MessageBox("Carton ID not found........",0,"Moret QC Audit")
  return
endif

Select * from qcscans where cartonid = thiscarton and !polyscan into cursor innerscans order by whichpoly desc

*Set Step On 
Select pscans
Goto top

Select moretdata
Set Order To styleplus
lcSeek = Padr(Alltrim(pscans.style),20," ")
Select moretdata
Seek lcSeek

sumdata =""
If Found()
  Sum qty to totinners for styleplus = lcSeek
  Select moretdata
  Scan for styleplus = lcSeek
 *   Select rpt2
 *   Append blank
    If Empty(sumdata)
      sumdata = "UPC: "+moretdata.upc+"  Qty: "+Transform(moretdata.qty)+"  Size: "+Alltrim(moretdata.size)
    Else
      sumdata = sumdata+Chr(13)+"UPC: "+moretdata.upc+"  Qty: "+Transform(moretdata.qty)+"  Size: "+Alltrim(moretdata.size)
    EndIf 
  EndScan
Else
  Select moretdata
  Set Order To STYLE   && STYLE
  lcSeek = Padr(Alltrim(pscans.style),20," ")
  Select moretdata
  Seek lcSeek
  If Found()
    Sum qty to totinners for styleplus = lcSeek
  Else
    MessageBox("Inner data not found in Moretdata...",0,"QC Audit")
    return
  EndIf  
EndIf 



Select pscans
Goto top
totinners_expected = totinners*Val(Alltrim(pscans.pack))

Count for errorflag = .t. to badpolys
If badpolys > 0
  cartonpassed = .f.
  polypassed = .f.
EndIf 

Count for scanval = "NOLABEL" AND errorflag = .t. to nolabels
If nolabels > 0
  cartonpassed = .f.
  polypassed = .f.
EndIf 

Count for scanval = "BADLABEL" AND errorflag = .t. to badlabels
If badlabels > 0
  cartonpassed = .f.
  polypassed = .f.
EndIf 

Count for errorflag = .t. and errormsg ="UNEXPECTED VALUE" to unexpected
If unexpected > 0
  cartonpassed = .f.
  polypassed = .f.
EndIf 


Select innerscans
Count for errorflag = .t. and !polyscan to badinners

If badinners > 0
  cartonpassed = .f.
  innerpassed = .f.
EndIf 

Count for scanval = "NOLABEL" AND errorflag = .t. to innernolabels
If nolabels > 0
  cartonpassed = .f.
  polypassed = .f.
EndIf 

Count for scanval = "BADLABEL" AND errorflag = .t. to innerbadlabels
If badlabels > 0
  cartonpassed = .f.
  polypassed = .f.
EndIf 

Count for errorflag = .t. and errormsg ="UNEXPECTED VALUE" to innerunexpected
If unexpected > 0
  cartonpassed = .f.
  polypassed = .f.
EndIf 

Count for errorflag = .t. and !Empty(inspecterr) to inspecterrors
If inspecterrors > 0
  cartonpassed = .f.
  polypassed = .f.
EndIf 


Select pscans
Goto top
wonum       = Transform(pscans.wo_num)
whichpallet = pscans.palletid
whichstyle  = pscans.style
whichpack   = pscans.pack
whichuser   = pscans.userid

Select pscans
Goto top
expectedpolys = Val(Alltrim(pscans.pack))

polycount = "OK"
Count for !errorflag to numpolys
If numpolys > expectedpolys
  polycount ="Over "+Transform(numpolys-expectedpolys)
EndIf

If numpolys < expectedpolys
  polycount ="Short "+Transform(expectedpolys-numpolys)
EndIf

Select pscans
Goto top

If numpolys != Val(Alltrim(pscans.pack))
  cartonpassed = .f.
  polypassed   = .f.
endif 

Select innerscans
Sum qty to totinner_scans for !errorflag

innercount = "OK"

If totinner_scans > totinners_expected
  innercount ="Over "+Transform(totinner_scans-totinners_expected)
EndIf

If totinner_scans < totinners_expected
  innercount ="Short "+Transform(totinners_expected-totinner_scans)
EndIf

If totinner_scans != totinners_expected
  cartonpassed = .f.
  innerpassed  = .f.
endif 


Select rpt
Append blank
Replace rdata With "Summary Poly Level Data"
Append blank
Replace rdata With "------------------------------------------------------------------------"
Append blank
Replace rdata With "PolyScans  Expected: "+Transform(expectedpolys)+ " Scanned Polys: "+Transform(numpolys)
Append blank
Replace rdata With "Polyscan Errors   : "+Transform(badpolys)
Append blank
Replace rdata With "Bad Label Errors   : "+Transform(badlabels)
Append blank
Replace rdata With "No Label  Errors   : "+Transform(nolabels)
Append blank
Replace rdata With "Unexpected Scans   : "+Transform(unexpected)
Append blank
Replace rdata With "PolyCount Errors  : "+polycount


Select rpt
Append blank
Replace rdata With "------------------------------------------------------------------------"
Append blank
Replace rdata With "Summmary InnerPoly Level Data"
Append blank
Replace rdata With "------------------------------------------------------------------------"
Append blank
Replace rdata With "Total Inners Expected: "+Transform(totinners_expected)+ " Scanned Inners: "+Transform(totinner_scans)
Append blank
Replace rdata With "Innerscan Errors   : "+Transform(badinners)
Append blank
Replace rdata With "InnerCount Errors  : "+innercount
Append blank
Replace rdata With "Bad Label Errors   : "+Transform(innerbadlabels)
Append blank
Replace rdata With "No Label  Errors   : "+Transform(innernolabels)
Append blank
Replace rdata With "Unexpected Scans   : "+Transform(innerunexpected)
Append blank
Replace rdata With "Inspection Errors  : "+Transform(inspecterrors)


Select rpt
Append blank
Select rpt
Append blank
Replace rdata With "------------------------------------------------------------------------"
Append blank
Replace rdata With "Poly Detail Level Data"
Append blank
Replace rdata With "------------------------------------------------------------------------"

Select pscans
Scan for errorflag
  Select rpt
  Append Blank
  replace rdata with "Error at Poly#: "+Transform(pscans.whichpoly)+" Expected: "+pscans.style+" Scanned: "+pscans.scanval
EndScan 



Select rpt
Append blank
Select rpt
Append blank
Replace rdata With "------------------------------------------------------------------------"
Append blank
Replace rdata With "Inner Detail Level Data"
Append blank
Replace rdata With "------------------------------------------------------------------------"
Select innerscans
Scan for errorflag

  If innerscans.expqty != innerscans.qty and innerscans.expqty > innerscans.qty
    Select rpt
    Append Blank
    replace rdata with "QTY Error (short) at Poly#: "+Transform(innerscans.whichpoly)+" Exp Qty: "+Transform(innerscans.expqty)+" Scanned: "+Transform(innerscans.qty)+" ("+Alltrim(innerscans.scanval)+")"
  EndIf   
  If innerscans.expqty != innerscans.qty and innerscans.expqty < innerscans.qty
    Select rpt
    Append Blank
    replace rdata with "QTY Error (over) at Poly#: "+Transform(innerscans.whichpoly)+" Exp Qty: "+Transform(innerscans.expqty)+" Scanned: "+Transform(innerscans.qty)+" ("+Alltrim(innerscans.scanval)+")"
  EndIf   
  If innerscans.errormsg = "UNEXPECTED VALUE"
    Select rpt
    Append Blank
    replace rdata with "UPC Error at Poly#: "+Transform(innerscans.whichpoly)+" Scanned: "+innerscans.scanval
  EndIf   
  If innerscans.errormsg = "LABEL ISSUE"
    Select rpt
    Append Blank
    replace rdata with innerscans.scanval+" at Poly: "+Transform(innerscans.whichpoly)+" Scanned: "+innerscans.scanval
  EndIf   
  If !Empty(innerscans.inspecterr)
    Select rpt
    Append Blank
    replace rdata with "Inspection Error at Poly#: "+Transform(innerscans.whichpoly)+" "+innerscans.inspecterr+" "+innerscans.comment
  EndIf   
  Select innerscans

EndScan 

If cartonpassed = .f.
  cartonstatus = "FAILED"
EndIf 
If polypassed = .f.  
  polystatus   = "FAILED"  
EndIf   
If innerpassed = .f.
  innerstatus  = "FAILED"
EndIf 



RPTOK = .t.
Select rpt
*Set Step On 
Report Form auditrpt preview

Return



If sendtoPrinter = .t.
  Report Form auditrpt to printer
Else
  lcReportname = "m:\dev\rpt\auditrpt"
  lcfilename = "h:\fox\"+whichcarton+".pdf"
  Store  .F. TO REPORTOK
  do rpt2pdf with lcReportname,lcFilename,REPORTOK
  if  REPORTOK = .F.
    =MESSAGEBOX("PDF file not created",48,"PDF Creator")
  EndIf 
endif 

status = cartonstatus

Return cartonstatus


************************************************************
*  FUNCTION getwonumber()
************************************************************
FUNCTION getwonumber()
LOCAL lnWo,lnRetVal
lcRetval=""
 DO WHILE .T.
   lcBOL=INPUTBOX('Carton#',"Enter a Cartons.....")   && WO num has to be an integer
   lcRetVal=lcBOL
    IF Empty(lcBOL)  && either nothing was put into the box or cancel was pressed
      lnAnswer=MESSAGEBOX("There is no WO  entered or Cancel was pressed"+ CHR(13)+;
                 "   Do you want to exit program ?",36)
      IF lnAnswer=6  && yes exit
       RETURN lcRetval  && return 0
      ELSE  && try again
       LOOP
      Endif
    ENDIF
    RETURN lcRetVal
 ENDDO
EndFunc
************************************************************
