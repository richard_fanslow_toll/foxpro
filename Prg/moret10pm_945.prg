utilsetup("MORET10PM_945REPORT")

SET DELETED ON
SET EXCLUSIVE OFF
SET ENGINEBEHAVIOR 70
SET MULTILOCKS ON

CLOSE DATABASES ALL
DO m:\dev\prg\lookups
lTesting = .F.

nDaysback = IIF(lTesting,1,0)
_screen.WindowState = IIF(lTesting,2,1)

xsqlexec("select * from account where inactive=0","account",,"qq")
index on accountid tag accountid
set order to

IF !USED('edi_trigger')
	cEDIFolder = "F:\3PL\DATA\"
	USE (cEDIFolder+"edi_trigger") IN 0 ALIAS edi_trigger
ENDIF


WAIT WINDOW "Running 10PM 945 report selection information for Moret" NOWAIT
SELECT a.accountid,b.acctname,a.bol,COUNT(ship_ref) AS ptcount,TTOC(a.trig_time) AS trig_time,;
	TTOC(a.when_proc) AS run_time,JUSTFNAME(a.file945) AS filesent ;
	FROM edi_trigger a,account b;
	WHERE (INLIST(a.accountid,&gMoretAcctList) or INLIST(a.accountid,&gMoretAcctList2)) ;
	AND TTOD(a.trig_time) = DATE()-nDaysback ;
	AND a.accountid = b.accountid ;
	AND !a.errorflag ;
	GROUP BY a.accountid, a.bol ;
	INTO CURSOR temp10pm

IF lTesting
	BROWSE
ENDIF

cFileout = "f:\3pl\data\moret10pmout.xls"
COPY TO &cFileout TYPE XL5
USE IN temp10pm

SELECT 0
USE F:\3pl\DATA\mailmaster ALIAS mm SHARED
IF !lTesting
	LOCATE FOR mm.edi_type = "MISC" AND mm.taskname = "MORETPMRPT"
	STORE TRIM(IIF(mm.use_alt,mm.sendtoalt,mm.sendto)) TO tsendto
	STORE TRIM(IIF(mm.use_alt,mm.ccalt,mm.cc)) TO tcc
ELSE
	LOCATE FOR mm.edi_type = "MISC" AND mm.taskname = "JOETEST"
	STORE TRIM(IIF(mm.use_alt,mm.sendtoalt,mm.sendto)) TO tsendto
	STORE TRIM(IIF(mm.use_alt,mm.ccalt,mm.cc)) TO tcc
ENDIF
USE IN mm

tsubject = "Moret 10PM 945 Report"
tattach = cFileout
tfrom = "TGF Warehouse Operations <toll-warehouse-ops@tollgroup.com>"
tmessage = "The attached list contains today's delivered shipments and related 945 information."
tmessage = tmessage + "Includes: Acct. info, BOL, pickticket count, trigger and run times, and "
tmessage = tmessage + "Name of output file. There may be other 945s sent after this report is received."
DO FORM m:\dev\frm\dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
DELETE FILE &cFileout

schedupdate()
