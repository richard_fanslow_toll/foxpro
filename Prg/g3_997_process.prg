Close Data All
SET EXCLUSIVE OFF
SET SAFETY OFF
SET DELETED on
ctransfer='G3_997_PROCESS'

lcDELIMITER = "*"
lcTranslateOption ="G3997"
delimchar = lcDELIMITER
lcTranOpt= lcTranslateOption

SET STEP ON 
Do m:\dev\prg\997_resend_cleanup WITH 6622
Do m:\dev\prg\997_resend_cleanup WITH 6623
Do m:\dev\prg\997_resend_cleanup WITH 6639
Do m:\dev\prg\997_resend_cleanup WITH 6661
Do m:\dev\prg\997_resend_cleanup WITH 6666
Do m:\dev\prg\997_resend_cleanup WITH 6678
Do m:\dev\prg\997_resend_cleanup WITH 6680

Do m:\dev\prg\createx856

*******************************************************************

lcpath="F:\FTPUSERS\G-III\997IN\"
lcarchivepath="F:\FTPUSERS\G-III\997IN\archive\"

Cd &lcpath

lnNum = Adir(tarray,"F:\FTPUSERS\G-III\997IN\*.*")

If lnNum = 0
     Wait Window At 10,10 "    No G3 997's to import.............." Timeout 2
     Use F:\edirouting\ftpsetup Shared
     Locate For ftpsetup.transfer =  cTransfer
     If Found()
          Replace chkbusy With .F. For ftpsetup.transfer = cTransfer
     Endif
     NormalExit = .T.

 *    Throw
Endif

For thisfile = 1  To lnNum
* Archivename = Ttoc(Datetime(),1)
     xfile = lcpath+tarray[thisfile,1]+"."

*Xfile = lcPath+Allt(tarray[thisfile,1])
     !Attrib -R &xfile  && Removes read-only flag from file to allow deletion

     archivefile = lcarchivepath+tarray[thisfile,1]
     lcFilename = tarray[thisfile,1]
     Wait Window "Importing file: "+xfile Nowait
     If File(xfile)
* load the file into the 856 array
SET STEP ON 
          Select x856
          Zap
          Do m:\dev\prg\loadedifile With xfile,delimchar,lcTranOpt,"G3997" &&"NAUTICA"
          lProcessOK = .F.
         
          Do m:\dev\prg\g3_997_bkdn With xfile  && do the bkdn and reconile the 997 into ACKDATA

          If lProcessOK
               Copy File &xfile To &archivefile
               If File(archivefile)
                    Delete File &xfile
               Endif
          Endif
     ENDIF
     
Next thisfile

&&&& now run through and look for errors

 Do m:\dev\prg\g3_997_status_check  && do the bkdn and reconile the 997 into ACKDATA
 
 
 
 **********Added 1/30/2015 for San Pedro  TM
 
*!*	 Do m:\dev\prg\createx856

*!*	*******************************************************************

*!*	lcpath="F:\FTPUSERS\MJ_Wholesale_CA\WMS_Out\997in\"
*!*	lcarchivepath="F:\FTPUSERS\MJ_Wholesale_CA\WMS_Out\997in\archive\"

*!*	Cd &lcpath

*!*	lnNum = Adir(tarray,"F:\FTPUSERS\MJ_Wholesale_CA\WMS_Out\997in\*.*")

*!*	If lnNum = 0
*!*	     Wait Window At 10,10 "    No MJ 997's to import.............." Timeout 2
*!*	     Use F:\edirouting\ftpsetup Shared
*!*	     Locate For ftpsetup.transfer =  cTransfer
*!*	     If Found()
*!*	          Replace chkbusy With .F. For ftpsetup.transfer = cTransfer
*!*	     Endif
*!*	     NormalExit = .T.

*!*	 *    Throw
*!*	Endif

*!*	For thisfile = 1  To lnNum
*!*	* Archivename = Ttoc(Datetime(),1)
*!*	     xfile = lcpath+tarray[thisfile,1]+"."

*!*	*Xfile = lcPath+Allt(tarray[thisfile,1])
*!*	     !Attrib -R &xfile  && Removes read-only flag from file to allow deletion

*!*	     archivefile = lcarchivepath+tarray[thisfile,1]
*!*	     lcFilename = tarray[thisfile,1]
*!*	     Wait Window "Importing file: "+xfile Nowait
*!*	     If File(xfile)
*!*	* load the file into the 856 array
*!*	SET STEP ON 
*!*	          Select x856
*!*	          Zap
*!*	          Do m:\dev\prg\loadedifile With xfile,delimchar,lcTranOpt,"MJ997" &&"NAUTICA"
*!*	          lProcessOK = .F.
*!*	         
*!*	          Do m:\dev\prg\mj_997_bkdn With xfile  && do the bkdn and reconile the 997 into ACKDATA

*!*	          If lProcessOK
*!*	               Copy File &xfile To &archivefile
*!*	               If File(archivefile)
*!*	                    Delete File &xfile
*!*	               Endif
*!*	          Endif
*!*	     ENDIF
*!*	     
*!*	Next thisfile

*!*	&&&& now run through and look for errors

*!*	 Do m:\dev\prg\mj_997_status_check  && do the bkdn and reconile the 997 into ACKDATA

