* create list of EEs by worksite for Lucille. Schedule to run on the 24th of each month.

* Build exe as F:\UTIL\ADPREPORTS\HREEBYWORKSITE.EXE

LOCAL loEmployeeListByWorksiteProcess
loEmployeeListByWorksiteProcess = CREATEOBJECT('EmployeeListByWorksiteProcess')
loEmployeeListByWorksiteProcess.MAIN()
CLOSE DATABASES ALL
RETURN

#DEFINE LOGIT 1
#DEFINE WAITIT 2
#DEFINE NOWAITIT 4
#DEFINE SENDIT 8
#DEFINE CRLF CHR(13) + CHR(10)
#DEFINE CARRIAGE_RETURN CHR(13)
#DEFINE LINE_FEED CHR(10)
#DEFINE RETURN_DATA_MANDATORY .F.
#DEFINE RETURN_DATA_NOT_MANDATORY .T.

DEFINE CLASS EmployeeListByWorksiteProcess AS CUSTOM

  cProcessName = 'EmployeeListByWorksiteProcess'

  lTestMode = .F.  && .F. FOR NORMAL OPERATION, .T. TO SUPPRESS ALL BUT DEVELOPER EMAILS AND CHANGE INPUT TABLE, ETC.

  lAutoYield = .T.

  lCalcSickBalance = .F.

  dToday = DATE()

  cStartTime = TTOC(DATETIME())

  * connection properties
  nSQLHandle = 0

  * wait window properties
  nWaitWindowTimeout = 2
  lWaitWindowIsOn = .T.

  * logfile properties
  lLoggingIsOn = .T.
  cLogFile = 'F:\UTIL\ADPREPORTS\Logfiles\EmployeeListByWorksiteProcess_log.txt'

  * INTERNAL email properties
  lSendInternalEmailIsOn = .T.
  cFrom = 'TGF Corporate <fmicorporate@fmiint.com>'
  cSendTo = 'lwaldrip@fmiint.com'
  cCC = 'mbennett@fmiint.com'
  cSubject = 'Employee List By Worksite for: ' + TTOC(DATETIME())
  cAttach = ''
  cBodyText = ''

  cCOMPUTERNAME = ''
  cUSERNAME = ''

  FUNCTION INIT
    IF NOT DODEFAULT()
      RETURN .F.
    ENDIF
    WITH THIS
      CLEAR
      *SET RESOURCE OFF
      CLOSE DATA
      SET CENTURY ON
      SET DATE AMERICAN
      SET DECIMALS TO 3
      SET HOURS TO 24
      SET ANSI ON
      SET TALK OFF
      SET DELETED ON
      SET CONSOLE OFF
      SET EXCLUSIVE OFF
      SET SAFETY OFF
      SET EXACT OFF
      SET STATUS BAR ON
      SET SYSMENU OFF
      SET ENGINEBEHAVIOR 70
      _VFP.AUTOYIELD = .lAutoYield
      .cCOMPUTERNAME = GETENV("COMPUTERNAME")
      .cUSERNAME = GETENV("USERNAME")
      IF .lTestMode THEN
        .cSendTo = 'Mark Bennett <mbennett@fmiint.com>'
        .cCC = ''
        .cLogFile = 'F:\UTIL\ADPREPORTS\Logfiles\EmployeeListByWorksiteProcess_log_TESTMODE.txt'
      ENDIF
      .lLoggingIsOn = .lLoggingIsOn AND NOT EMPTY(.cLogFile)
      IF .lLoggingIsOn THEN
        SET ALTERNATE TO (.cLogFile) ADDITIVE
        SET ALTERNATE ON
      ENDIF
    ENDWITH
  ENDFUNC


  FUNCTION DESTROY
    WITH THIS
      IF .lLoggingIsOn  THEN
        SET ALTERNATE OFF
        SET ALTERNATE TO
      ENDIF
    ENDWITH
    DODEFAULT()
  ENDFUNC


  FUNCTION MAIN
    WITH THIS
      LOCAL lnNumberOfErrors, loError, lcSQL, lcOutputFile

      TRY
        lnNumberOfErrors = 0

        *.TrackProgress('==================================================================================================================', LOGIT+SENDIT)
        .TrackProgress('Employee List By Worksite process started....', LOGIT+SENDIT+NOWAITIT)
        .TrackProgress('COMPUTERNAME = ' + .cCOMPUTERNAME, LOGIT+SENDIT)
        .TrackProgress('USERNAME = ' + .cUSERNAME, LOGIT+SENDIT)
        .TrackProgress('==================================================================================================================', LOGIT+SENDIT)
        IF .lTestMode THEN
          .TrackProgress('', LOGIT)
          .TrackProgress('==================================================================================================================', LOGIT+SENDIT)
          .TrackProgress('                        TEST MODE                        ', LOGIT+SENDIT)
          .TrackProgress('==================================================================================================================', LOGIT+SENDIT)
        ENDIF

        OPEN DATABASE F:\UTIL\ADPCONNECTION\CONNECT1

        .nSQLHandle = SQLCONNECT("PCPAYWIN","MBENNETT","VP737A")

        IF .nSQLHandle > 0 THEN
          lcSQL = ;
            " SELECT " + ;
            " {FN IFNULL(A.CUSTAREA3,'   ')} AS WORKSITE, " + ;
            " A.NAME, " + ;
            " A.FILE# AS FILE_NUM, " + ;
            " A.STATUS, " + ;
            " A.COMPANYCODE AS ADP_COMP, " + ;
            " {fn LEFT(A.HOMEDEPARTMENT,2)} AS DIVISION, " + ;
            " {fn SUBSTRING(A.HOMEDEPARTMENT,3,4)} AS DEPARTMENT, " + ;
            " A.HIREDATE " + ;
            " FROM REPORTS.V_EMPLOYEE A " + ;
            " WHERE A.COMPANYCODE IN ('E87','E88','E89') " + ;
            " AND A.STATUS <> 'T' " + ;
            " ORDER BY 1, 2 "

          IF .lTestMode THEN
            .TrackProgress('lcSQL = ' + lcSQL, LOGIT+SENDIT)
          ENDIF

          IF USED('CURADP') THEN
            USE IN CURADP
          ENDIF
          IF USED('CURADP2') THEN
            USE IN CURADP2
          ENDIF

          IF .ExecSQL(lcSQL, 'CURADP', RETURN_DATA_MANDATORY) THEN
          
            SELECT ;
            WORKSITE, NAME, FILE_NUM, STATUS, ADP_COMP, DIVISION, DEPARTMENT, ;
            TTOD(HIREDATE) AS HIREDATE ;
            FROM CURADP ;
            INTO CURSOR CURADP2 ;
            ORDER BY 1,2

            lcOutputFile = "F:\UTIL\ADPREPORTS\REPORTS\EE_BY_WKSITE_" + STRTRAN(DTOC(.dToday),"/","") + ".XLS"
            SELECT CURADP2
			COPY TO (lcOutputFile) XL5
			
			IF FILE(lcOutputFile) THEN
				.cAttach = lcOutputFile
				.cBodyText = "See attached list of active employees by worksite. You will need to manually widen the Name column." + ;
				CRLF + CRLF + .cBodyText
			ELSE
            	.TrackProgress('There was an error attaching output file: ' + lcOutputFile, LOGIT+SENDIT)
			ENDIF
			
          ENDIF

        ELSE
          * connection error
          .TrackProgress('Unable to connect to ADP Payroll System.', LOGIT+SENDIT)
        ENDIF

        CLOSE DATABASES ALL
      CATCH TO loError

        .TrackProgress('There was an error.',LOGIT+SENDIT)
        .TrackProgress('The Error # is: ' + TRANSFORM(loError.ERRORNO), LOGIT+SENDIT)
        .TrackProgress('The Error Message is: ' + TRANSFORM(loError.MESSAGE), LOGIT+SENDIT)
        .TrackProgress('The Error occurred in Program: ' + TRANSFORM(loError.PROCEDURE), LOGIT+SENDIT)
        .TrackProgress('The Error occurred at line #: ' + TRANSFORM(loError.LINENO), LOGIT+SENDIT)
        lnNumberOfErrors = lnNumberOfErrors + 1
        IF TYPE('oExcel') = 'O' AND NOT ISNULL(oExcel) THEN
          oExcel.QUIT()
        ENDIF
        CLOSE DATA
        CLOSE ALL

      ENDTRY

      CLOSE DATA
      WAIT CLEAR
      ***************** INTERNAL email results ******************************
      .TrackProgress('About to send status email.',LOGIT)
      .TrackProgress('==================================================================================================================', SENDIT)
      .TrackProgress('The logfile is: ' + .cLogFile, SENDIT)
      .TrackProgress('Employee List By Worksite process started: ' + .cStartTime, LOGIT+SENDIT)
      .TrackProgress('Employee List By Worksite process finished: ' + TTOC(DATETIME()), LOGIT+SENDIT)

      IF .lSendInternalEmailIsOn THEN
        * try to trap error from not having dartmail dll's registered on user's pc...
        TRY
          DO FORM dartmail2 WITH .cSendTo,.cFrom,.cSubject,.cCC,.cAttach,.cBodyText,"A"
          .TrackProgress('Sent status email.',LOGIT)
        CATCH TO loError
          =MESSAGEBOX('There was an error sending the status email. Please contact Mark Bennett.',0+64,"Employee List By Worksite")
          .TrackProgress('There was an error sending the status email.',LOGIT)
          .TrackProgress('The Error # is: ' + TRANSFORM(loError.ERRORNO), LOGIT)
          .TrackProgress('The Error Message is: ' + TRANSFORM(loError.MESSAGE), LOGIT)
          .TrackProgress('The Error occurred in Program: ' + TRANSFORM(loError.PROCEDURE), LOGIT)
          .TrackProgress('The Error occurred at line #: ' + TRANSFORM(loError.LINENO), LOGIT)
          lnNumberOfErrors = lnNumberOfErrors + 1
          CLOSE DATA
        ENDTRY
      ELSE
        .TrackProgress('Did not send status email: THIS.lSendInternalEmailIsOn = FALSE.',LOGIT)
      ENDIF

    ENDWITH
    RETURN
  ENDFUNC && main


  FUNCTION ExecSQL
    LPARAMETERS tcSQL, tcCursorName, tlNoDataReturnedIsOkay
    LOCAL llRetval, lnResult
    WITH THIS
      * close target cursor if it's open
      IF USED(tcCursorName)
        USE IN (tcCursorName)
      ENDIF
      WAIT WINDOW "Fetching remote data; please wait..." NOWAIT
      lnResult = SQLEXEC(.nSQLHandle, tcSQL, tcCursorName)
      llRetval = ( lnResult > 0 )
      IF llRetval THEN
        * see if any data came back
        IF NOT tlNoDataReturnedIsOkay THEN
          IF (NOT USED(tcCursorName)) OR EOF(tcCursorName) THEN
            llRetval = .F.
            .TrackProgress('No data was returned by this query: ' +CRLF + tcSQL, LOGIT+SENDIT)
          ENDIF
        ENDIF
      ELSE
        .TrackProgress("Result Code [" + TRANSFORM(lnResult) + "] returned by this query: " + tcSQL, LOGIT+SENDIT)
      ENDIF
      WAIT CLEAR
      RETURN llRetval
    ENDWITH
  ENDFUNC


  PROCEDURE TrackProgress
    * do any combination of Wait Window, writing to logfile, and adding to body of email,
    * based on nFlags parameter.
    LPARAMETERS tcExpression, tnFlags
    WITH THIS
      IF BITAND(tnFlags,LOGIT) = LOGIT THEN
        IF .lLoggingIsOn THEN
          ?
          ? .cProcessName + "  " + TTOC(DATETIME()) + ": " + tcExpression
        ENDIF
      ENDIF
      IF .lWaitWindowIsOn THEN
        IF BITAND(tnFlags,NOWAITIT) = NOWAITIT THEN
          WAIT WINDOW tcExpression NOWAIT
        ENDIF
        IF BITAND(tnFlags,WAITIT) = WAITIT THEN
          WAIT WINDOW tcExpression TIMEOUT .nWaitWindowTimeout
        ENDIF
      ENDIF
      IF BITAND(tnFlags,SENDIT) = SENDIT THEN
        IF .lSendInternalEmailIsOn THEN
          .cBodyText = .cBodyText + tcExpression + CRLF + CRLF
        ENDIF
      ENDIF
    ENDWITH
  ENDPROC  &&  TrackProgress

ENDDEFINE

