* SHCOSTSREPORT 05/12/2015

* Create a utility report that calculates costs by wo date range.

* Break out inside, outside and accident costs.

* Use same burdened labor rates as in the financial shop reports.
*
* exe = F:\UTIL\SHOPREPORTS\SHCOSTSREPORT.EXE

LOCAL lTestMode
lTestMode = .T.

utilsetup("SHCOSTSREPORT")

DECLARE INTEGER CreateMutex IN Win32API ;
	STRING @lpMutexAttributes, ;
	STRING bInitialOwner, ;
	STRING @lpName

DECLARE INTEGER GetLastError IN Win32API

* if running in EXE check to see if already running
IF _VFP.STARTMODE > 0 THEN
	lcProcessName = "SHCOSTSREPORT"
	CreateMutex(0,CHR(1),@lcProcessName)
	lnError = GetLastError()
	IF lnError = 183
		WAIT WINDOW TIMEOUT 5 "This program is already running..."
		IF NOT lTestMode THEN
			schedupdate()
		ENDIF
		RETURN .F.
	ENDIF
ENDIF

loSHCOSTSREPORT = CREATEOBJECT('SHCOSTSREPORT')
loSHCOSTSREPORT.MAIN( lTestMode )

IF NOT lTestMode THEN
	schedupdate()
ENDIF

CLOSE DATABASES ALL
RETURN

#DEFINE LOGIT 1
#DEFINE WAITIT 2
#DEFINE NOWAITIT 4
#DEFINE SENDIT 8
#DEFINE CRLF CHR(13) + CHR(10)
#DEFINE RETURN_DATA_MANDATORY .F.
#DEFINE RETURN_DATA_NOT_MANDATORY .T.
#DEFINE CSVFORMAT 6
#DEFINE WRITE_ONLY_UNBUFFERED 11
#DEFINE COMMA ","

DEFINE CLASS SHCOSTSREPORT AS CUSTOM

	cProcessName = 'SHCOSTSREPORT'

	lTestMode = .T.  && .F. FOR NORMAL OPERATION, .T. TO SUPPRESS ALL BUT DEVELOPER EMAILS AND CHANGE INPUT TABLE, ETC.

	dToday = DATE()
	*dToday = {^2015-04-19}
	
	* processing properties
	*************************************************************************************************************
	lCalcDateRange = .T.   && FALSE = process all work orders; TRUE = just for the date range entered in the code.
	*************************************************************************************************************


	lAutoYield = .T.

	cStartTime = TTOC(DATETIME())

	* wait window properties
	nWaitWindowTimeout = 2
	lWaitWindowIsOn = .T.

	* logfile properties
	lLoggingIsOn = .T.
	cLogFile = 'F:\UTIL\SHOPREPORTS\LOGFILES\SHCOSTSREPORT_log.txt'

	* INTERNAL email properties
	lSendInternalEmailIsOn = .T.
	cFrom = 'fmi-transload-ops@fmiint.com'
	cSendTo = 'mbennett@fmiint.com'
	cCC = ''
	cSubject = "Costs Report for " + TRANSFORM(DATETIME())
	cAttach = ''
	cBodyText = ''
	cMGRSendTo = ''
	cMGRFrom = ''
	cMGRSubject = ''
	cMGRCC = ''
	cMGRAttach = ''
	cMGRBodyText = ''

	cCOMPUTERNAME = ''
	cUSERNAME = ''


	FUNCTION INIT
		IF NOT DODEFAULT()
			RETURN .F.
		ENDIF
		WITH THIS
			*SET RESOURCE OFF
			CLOSE DATA
			SET CENTURY ON
			*SET DATE AMERICAN
			*SET HOURS TO 24
			SET DECIMALS TO 0
			SET ANSI ON
			SET TALK OFF
			SET DELETED ON
			SET CONSOLE OFF
			SET EXCLUSIVE OFF
			SET SAFETY OFF
			SET EXACT OFF
			SET STATUS BAR ON
			SET SYSMENU OFF
			SET ENGINEBEHAVIOR 70
			SET MULTILOCKS ON
			_VFP.AUTOYIELD = .lAutoYield
			.cCOMPUTERNAME = GETENV("COMPUTERNAME")
			.cUSERNAME = GETENV("USERNAME")
			.lLoggingIsOn = .lLoggingIsOn AND NOT EMPTY(.cLogFile)
		ENDWITH
	ENDFUNC


	FUNCTION DESTROY
		WITH THIS
			IF .lLoggingIsOn  THEN
				SET ALTERNATE OFF
				SET ALTERNATE TO
			ENDIF
		ENDWITH
		DODEFAULT()
	ENDFUNC


	FUNCTION MAIN
		LPARAMETERS tlTestMode
		WITH THIS
			LOCAL lcFiletoSaveAs, ldFromDate, ldToDate, lnNumberOfErrors

			TRY
				lnNumberOfErrors = 0

				.lTestMode = tlTestMode

				IF .lTestMode THEN
					.cLogFile = 'F:\UTIL\SHOPREPORTS\LOGFILES\SHCOSTSREPORT_log_TESTMODE.txt'
					.cSendTo = 'mbennett@fmiint.com'
				ENDIF

				IF .lLoggingIsOn THEN
					SET ALTERNATE TO (.cLogFile) ADDITIVE
					SET ALTERNATE ON
				ENDIF

				.cStartTime = TTOC(DATETIME())

				.TrackProgress('Costs Report process started....', LOGIT+SENDIT)
				.TrackProgress('COMPUTERNAME = ' + .cCOMPUTERNAME, LOGIT+SENDIT)
				.TrackProgress('USERNAME = ' + .cUSERNAME, LOGIT+SENDIT)
				.TrackProgress('PROJECT = SHCOSTSREPORT', LOGIT+SENDIT)
				IF .lTestMode THEN
					.TrackProgress('                        TEST MODE                        ', LOGIT+SENDIT)
				ENDIF

				ldToday = .dToday

				USE F:\SHOP\SHDATA\ORDHDR IN 0
				USE F:\SHOP\SHDATA\trucks IN 0
				USE F:\SHOP\SHDATA\trailer IN 0
				USE F:\SHOP\SHDATA\chaslist IN 0
				USE F:\SHOP\SHDATA\vehmast IN 0


				WAIT WINDOW "Selecting data..." NOWAIT
				
				*ldFromDate = {^2013-07-01}				
				*ldToDate = {^2014-06-30}
				
				*ldFromDate = {^2014-07-01}				
				*ldToDate = {^2015-06-30}
				
				ldFromDate = {^2013-01-01}				
				ldToDate = {^2017-03-15}
				
				*ldToDate = GOMONTH(ldFromDate,1) - 1
				
				lcFromDate = DTOS(ldFromDate)
				lcToDate = DTOS(ldToDate)
				
				.TrackProgress('lcFromDate = ' + lcFromDate, LOGIT+SENDIT)
				.TrackProgress('lcToDate = ' + lcToDate, LOGIT+SENDIT)
					
*!*					SELECT ORDNO, CRDATE, SPACE(2) AS DIVISION, MECHNAME, VEHNO, TYPE, REPCAUSE, TOTHOURS, TOTLCOST, TOTPCOST, OUTSCOST, 0000.00 AS RATE, ;
*!*						MONTH(CRDATE) AS NMONTH, YEAR(CRDATE) AS NYEAR, SPACE(15) AS CMONTH ;
*!*						FROM ORDHDR ;
*!*						INTO CURSOR CURORDHDRPRE ;
*!*						WHERE INLIST(UPPER(ALLTRIM(TYPE)),"TRUCK") ;
*!*						AND CRDATE >= ldFromDate ;
*!*						AND CRDATE <= ldToDate ;
*!*						AND NOT EMPTY(VEHNO) ;
*!*						ORDER BY CRDATE ;
*!*						READWRITE
					
				SELECT ORDNO, CRDATE, '??' AS DIVISION, SPACE(1) AS COMPANY, MECHNAME, VEHNO, TYPE, MAINTTYPE, REPCAUSE, ALLTRIM(LEFT(remarks,250)) as remarks, ALLTRIM(LEFT(workdesc,250)) as workdesc, TOTHOURS, TOTLCOST, TOTPCOST, OUTSCOST, MILEAGE, 0000.00 AS RATE, ;
					MONTH(CRDATE) AS NMONTH, YEAR(CRDATE) AS NYEAR, SPACE(15) AS CMONTH ;
					FROM ORDHDR ;
					INTO CURSOR CURORDHDRPRE ;
					WHERE INLIST(UPPER(ALLTRIM(TYPE)),"TRUCK") ;
					AND CRDATE >= ldFromDate ;
					AND CRDATE <= ldToDate ;
					AND NOT EMPTY(VEHNO) ;
					ORDER BY CRDATE ;
					READWRITE

					*AND ALLTRIM(VEHNO) = '3122' ;
					
*!*					SELECT ORDNO, CRDATE, SPACE(2) AS DIVISION, MECHNAME, VEHNO, TYPE, REPCAUSE, TOTHOURS, TOTLCOST, TOTPCOST, OUTSCOST, 0000.00 AS RATE, ;
*!*						MONTH(CRDATE) AS NMONTH, YEAR(CRDATE) AS NYEAR, SPACE(15) AS CMONTH ;
*!*						FROM ORDHDR ;
*!*						INTO CURSOR CURORDHDRPRE ;
*!*						WHERE INLIST(UPPER(ALLTRIM(TYPE)),"CHASSIS") ;
*!*						AND CRDATE >= ldFromDate ;
*!*						AND CRDATE <= ldToDate ;
*!*						AND NOT EMPTY(VEHNO) ;
*!*						ORDER BY CRDATE ;
*!*						READWRITE
					
*!*					SELECT ORDNO, CRDATE, SPACE(2) AS DIVISION, MECHNAME, VEHNO, TYPE, REPCAUSE, TOTHOURS, TOTLCOST, TOTPCOST, OUTSCOST, 0000.00 AS RATE, ;
*!*						MONTH(CRDATE) AS NMONTH, YEAR(CRDATE) AS NYEAR, SPACE(15) AS CMONTH ;
*!*						FROM ORDHDR ;
*!*						INTO CURSOR CURORDHDRPRE ;
*!*						WHERE INLIST(UPPER(ALLTRIM(TYPE)),"MISC") ;
*!*						AND CRDATE >= ldFromDate ;
*!*						AND CRDATE <= ldToDate ;
*!*						AND NOT EMPTY(VEHNO) ;
*!*						ORDER BY CRDATE ;
*!*						READWRITE
					
				SELECT CURORDHDRPRE 
				SCAN
					REPLACE CURORDHDRPRE.CMONTH WITH CMONTH(CURORDHDRPRE.CRDATE) IN CURORDHDRPRE
				ENDSCAN

				WAIT WINDOW "Calculating labor costs..." NOWAIT
				
				* POPULATE RATE BY CRDATE AND CALC LABOR COST FOR EACH WO REC
				SELECT CURORDHDRPRE
				SCAN
					lnRate = .GetHourlyRateByCRDate( CURORDHDRPRE.CRDATE )
					REPLACE CURORDHDRPRE.RATE WITH lnRate, CURORDHDRPRE.TOTLCOST WITH ( CURORDHDRPRE.tothours * lnRate )
				ENDSCAN


				* populate divisions
				SELECT CURORDHDRPRE
				SCAN

					WAIT WINDOW "checking Vehicle #" + TRANSFORM(CURORDHDRPRE.vehno) NOWAIT

					DO CASE
						CASE ALLTRIM(CURORDHDRPRE.TYPE) == "MISC"
							SELECT vehmast
							LOCATE FOR ALLTRIM(vehno) == ALLTRIM(CURORDHDRPRE.vehno)
							IF FOUND() THEN
								REPLACE CURORDHDRPRE.DIVISION WITH vehmast.DIVISION IN CURORDHDRPRE
							ELSE
								.TrackProgress('ERROR: FORKLIFT NOT FOUND: ' + CURORDHDRPRE.vehno, LOGIT+SENDIT)
							ENDIF
							SELECT vehmast
							LOCATE
							LOOP
						CASE ALLTRIM(CURORDHDRPRE.TYPE) == "TRUCK"
							SELECT trucks
							LOCATE FOR ALLTRIM(truck_num) == ALLTRIM(CURORDHDRPRE.vehno)
							IF FOUND() THEN
								REPLACE CURORDHDRPRE.DIVISION WITH trucks.DIVISION, CURORDHDRPRE.COMPANY WITH trucks.COMPANY IN CURORDHDRPRE
							ELSE
								.TrackProgress('ERROR: TRUCK NOT FOUND: ' + CURORDHDRPRE.vehno, LOGIT+SENDIT)
							ENDIF
							SELECT trucks
							LOCATE
							LOOP
						CASE ALLTRIM(CURORDHDRPRE.TYPE) == "TRAILER"
							SELECT trailer
							LOCATE FOR ALLTRIM(trail_num) == ALLTRIM(CURORDHDRPRE.vehno)
							IF FOUND() THEN
								REPLACE CURORDHDRPRE.DIVISION WITH trailer.DIVISION IN CURORDHDRPRE
							ELSE
								.TrackProgress('ERROR: TRAILER NOT FOUND: ' + CURORDHDRPRE.vehno, LOGIT+SENDIT)
							ENDIF
							SELECT trailer
							LOCATE
							LOOP
						CASE ALLTRIM(CURORDHDRPRE.TYPE) == "CHASSIS"
							SELECT chaslist
							LOCATE FOR ALLTRIM(chassis) == ALLTRIM(CURORDHDRPRE.vehno)
							IF FOUND() THEN
								REPLACE CURORDHDRPRE.DIVISION WITH chaslist.DIVISION IN CURORDHDRPRE
							ELSE
								.TrackProgress('ERROR: CHASSIS NOT FOUND: ' + CURORDHDRPRE.vehno, LOGIT+SENDIT)
							ENDIF
							SELECT trailer
							LOCATE
							LOOP
					ENDCASE
					
				ENDSCAN
				
				* added mechname for Gary Fechtenburg to help him determine the coast
				SELECT VEHNO, CRDATE, MECHNAME, DIVISION, COMPANY, TYPE, ORDNO, MAINTTYPE, REPCAUSE, REMARKS, WORKDESC, (TOTLCOST + TOTPCOST) AS INSCOST, OUTSCOST, MILEAGE ;
				FROM CURORDHDRPRE ;
				INTO CURSOR CURFINAL ;
				ORDER BY VEHNO, CRDATE 
				
				SELECT CURFINAL
				COPY TO ("C:\A\SHOPCOSTS_DETAIL_TRUCK_" + lcFromDate + "-" + lcToDate + ".CSV") CSV FOR ( DIVISION = '02' OR DIVISION = '??' )
				
				
				* roll up by Vehicle
				SELECT VEHNO, DIVISION, TYPE, SUM(INSCOST) AS INSCOST, SUM(OUTSCOST) AS OUTSCOST, SUM(INSCOST+OUTSCOST) AS TOTCOST, MAX(MILEAGE) AS MILEAGE, 0000.00 AS CPM ;
				FROM CURFINAL ;
				INTO CURSOR CURSUMMARY ;
				GROUP BY VEHNO, DIVISION, TYPE ;
				ORDER BY VEHNO ;
				READWRITE
				
				SELECT CURSUMMARY
				SCAN
					IF CURSUMMARY.MILEAGE > 0 THEN
						REPLACE CURSUMMARY.CPM WITH (CURSUMMARY.TOTCOST/CURSUMMARY.MILEAGE) IN CURSUMMARY					
					ENDIF
				ENDSCAN
				
				SELECT CURSUMMARY
				COPY TO ("C:\A\SHOPCOSTS_SUMMARY_TRUCK_" + lcFromDate + "-" + lcToDate + ".CSV") CSV
				
				
				
				
*!*					SELECT TYPE, CMONTH AS MONTH, TRANSFORM(NYEAR) AS YEAR, DIVISION, SUM(TOTLCOST+TOTPCOST) AS INSCOST, SUM(OUTSCOST) AS OUTSCOST ;
*!*					FROM CURORDHDRPRE ;
*!*					INTO CURSOR CURFINAL ;
*!*					GROUP BY TYPE, CMONTH, NYEAR, DIVISION ;
*!*					ORDER BY TYPE, NMONTH, NYEAR, DIVISION 
*!*					
*!*					SELECT TYPE, CMONTH AS MONTH, TRANSFORM(NYEAR) AS YEAR, DIVISION, SUM(TOTLCOST+TOTPCOST) AS INSCOST, SUM(OUTSCOST) AS OUTSCOST ;
*!*					FROM CURORDHDRPRE ;
*!*					INTO CURSOR CURFINAL ;
*!*					WHERE DIVISION = '02' ;
*!*					GROUP BY TYPE, CMONTH, NYEAR, DIVISION ;
*!*					ORDER BY TYPE, NYEAR, NMONTH, DIVISION 
*!*					
*!*					SELECT CURFINAL
*!*					COPY TO C:\A\COSTS_20140701-20150430.XLS XL5
				

				*******************************************************************************************************************
				.TrackProgress('Cost Reports process ended normally.', LOGIT+SENDIT)

			CATCH TO loError

				.TrackProgress('There was an error.',LOGIT+SENDIT)
				.TrackProgress('The Error # is: ' + TRANSFORM(loError.ERRORNO), LOGIT+SENDIT)
				.TrackProgress('The Error Message is: ' + TRANSFORM(loError.MESSAGE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred in Program: ' + TRANSFORM(loError.PROCEDURE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred at line #: ' + TRANSFORM(loError.LINENO), LOGIT+SENDIT)
				lnNumberOfErrors = lnNumberOfErrors + 1
				.cSendTo = 'mbennett@fmiint.com'

				IF TYPE('oExcel') = 'O' AND NOT ISNULL(oExcel) THEN
					oExcel.QUIT()
				ENDIF

			ENDTRY

			WAIT CLEAR
			***************** INTERNAL email results ******************************
			.TrackProgress('About to send status email.',LOGIT+SENDIT)
			.TrackProgress('The logfile is: ' + .cLogFile, LOGIT+SENDIT)
			.TrackProgress('Cost Reports process started: ' + .cStartTime, LOGIT+SENDIT)
			.TrackProgress('Cost Reports process finished: ' + TTOC(DATETIME()), LOGIT+SENDIT)

			IF .lSendInternalEmailIsOn THEN
				DO FORM dartmail2 WITH .cSendTo,.cFrom,.cSubject,.cCC,.cAttach,.cBodyText,"A"
				.TrackProgress('Sent status email.',LOGIT)
			ELSE
				.TrackProgress('Did not send status email: THIS.lSendInternalEmailIsOn = FALSE.',LOGIT+SENDIT)
			ENDIF

		ENDWITH
		RETURN
	ENDFUNC && main


	FUNCTION GetHourlyRateByCRDate
		LPARAMETERS tdCRdate
		DO CASE
			CASE tdCRdate < {^2008-09-01}
				lnRate = 60.00
			OTHERWISE
				lnRate = 70.00
		ENDCASE
		RETURN lnRate
	ENDFUNC


	FUNCTION GetHourlyRateByDivision
		LPARAMETERS tcDivision
		DO CASE
			CASE INLIST(tcDivision,'05','52','56','57','58','71','74')
				lnRate = 60
			OTHERWISE
				lnRate = 60  && the default rate
		ENDCASE
		RETURN lnRate
	ENDFUNC


	PROCEDURE TrackProgress
		* do any combination of Wait Window, writing to logfile, and adding to body of email,
		* based on nFlags parameter.
		LPARAMETERS tcExpression, tnFlags
		WITH THIS
			IF BITAND(tnFlags,LOGIT) = LOGIT THEN
				IF .lLoggingIsOn THEN
					?
					? .cProcessName + "  " + TTOC(DATETIME()) + ": " + tcExpression
				ENDIF
			ENDIF
			IF .lWaitWindowIsOn THEN
				IF BITAND(tnFlags,NOWAITIT) = NOWAITIT THEN
					WAIT WINDOW tcExpression NOWAIT
				ENDIF
				IF BITAND(tnFlags,WAITIT) = WAITIT THEN
					WAIT WINDOW tcExpression TIMEOUT .nWaitWindowTimeout
				ENDIF
			ENDIF
			IF BITAND(tnFlags,SENDIT) = SENDIT THEN
				IF .lSendInternalEmailIsOn THEN
					.cBodyText = .cBodyText + tcExpression + CRLF + CRLF
				ENDIF
			ENDIF
		ENDWITH
	ENDPROC  &&  TrackProgress


ENDDEFINE
