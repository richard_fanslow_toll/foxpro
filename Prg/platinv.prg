create cursor xplatinv ( ;
	accountid i, ;
	billtoid i, ;
	company c(1), ;
	glcode c(2), ;
	revacct c(10), ;
	wo_num c(7), ;
	invdate d, ;
	amount n(10,2), ;
	group c(14))

xsqlexec("select arhdrh.applyto, arhdrh.shiptokey, arhdrh.customerkey, arhdrh.customerponumber, arlinh.locationkey, " + ;
	"arlinh.revenueacctkey, arhdrh.invoicedate, arlinh.unitprice, arlinh.qtyordered, arlinh.glcode from arhdrh, arlinh " + ;
	"where arhdrh.transactionnumber=arlinh.documentnumber and arhdrh.sysdocid=arlinh.sysdocid " + ;
	"and !inlist(revenueacctkey,'1200','1205','1270','1415','1900','6612','6910','8435','8445','8450','8455','8900','9250') " + ;
	"and between(arhdrh.invoicedate,{"+dtoc(xstartdt)+"},{"+dtoc(xenddt)+"})","xdy",,"ar")

select applyto, shiptokey, customerkey, customerponumber, locationkey, revenueacctkey, invoicedate, glcode, ;
	unitprice*qtyordered as invamt from xdy	into cursor xtemp

scan for invamt#0
	m.company = left(locationkey,1)
	m.amount = invamt
	m.billtoid = val(customerkey)
	m.wo_num = customerponumber
	m.invdate = invoicedate
	m.glcode = glcode

	m.revacct = revenueacctkey
	if substr(m.revacct,8,1)#substr(m.revacct,10,1) and substr(m.revacct,8,1)#'0'
		if !inlist(m.revacct,"5100000168","5200000268","5280000168")
			gunshot()
			set step on 
			m.revacct=left(m.revacct,9)+substr(m.revacct,8,1)
		endif
	endif
	
	if !empty(shiptokey)
		m.accountid=eval("0x"+shiptokey)
	else
		if isdigit(applyto) or inlist(applyto,"Z","X")
			if seek(left(applyto,6),"invoice","invnum")
				m.accountid=invoice.accountid
			else
				loop
			endif
		else
			loop
		endif
	endif		
	insert into xplatinv from memvar
endscan

select xplatinv