* run EMPLOYEE HOURS detail list for passed division

* Build EXE as F:\UTIL\ADPREPORTS\divxxhours.exe

LPARAMETERS tnDivision, tcBreakByJobclass

utilsetup("DIVXXHOURS")

LOCAL loDivXXHoursWeeklyReport, lnDivision, llBreakByJobclass
DO CASE
	CASE TYPE('tnDivision') = 'N'
		lnDivision = tnDivision
		*WAIT WINDOW "TYPE = NUMERIC" TIMEOUT 5
	CASE TYPE('tnDivision') = 'C'
		lnDivision = INT(VAL(tnDivision))
		*WAIT WINDOW "TYPE = CHAR" TIMEOUT 5
	OTHERWISE
		WAIT WINDOW "INVALID OR MISSING DIVISION PARAMETER" TIMEOUT 5
		RETURN
ENDCASE
llBreakByJobclass = .F.
DO CASE
	CASE TYPE('tcBreakByJobclass') = "C"
		IF UPPER(ALLTRIM(tcBreakByJobclass)) = "Y" THEN
			llBreakByJobclass = .T.
		ENDIF
	OTHERWISE
		*NOTHING
ENDCASE

loDivXXHoursWeeklyReport = CREATEOBJECT('DivXXHoursWeeklyReport')
loDivXXHoursWeeklyReport.SetDivision( lnDivision )
IF llBreakByJobclass THEN
	loDivXXHoursWeeklyReport.RunByJobclass()
ENDIF
loDivXXHoursWeeklyReport.MAIN()

schedupdate()

CLOSE DATABASES ALL
RETURN

#DEFINE LOGIT 1
#DEFINE WAITIT 2
#DEFINE NOWAITIT 4
#DEFINE SENDIT 8
#DEFINE CRLF CHR(13) + CHR(10)
#DEFINE RETURN_DATA_MANDATORY .F.
#DEFINE RETURN_DATA_NOT_MANDATORY .T.
#DEFINE CSVFORMAT 6
#DEFINE WRITE_ONLY_UNBUFFERED 11
#DEFINE COMMA ","

DEFINE CLASS DivXXHoursWeeklyReport AS CUSTOM

	cProcessName = 'DivXXHoursWeeklyReport'

	lTestMode = .F.  && .F. FOR NORMAL OPERATION, .T. TO SUPPRESS ALL BUT DEVELOPER EMAILS AND CHANGE INPUT TABLE, ETC.

	dToday = DATE()
	*dToday = {^2005-02-01}  && to force report for January
	*dToday = {^2005-05-01}  && to force report for April
	*dToday = {^2005-06-01}  && to force report for May
	*dToday = {^2012-07-05}  && to force report for June

	lAutoYield = .T.

	cStartTime = TTOC(DATETIME())

	* which division to report on
	nDivision = 0
	cDivision = '00'

	* report by jobclass?
	lBreakByJobclass = .F.

	* connection properties
	nSQLHandle = 0

	* wait window properties
	nWaitWindowTimeout = 2
	lWaitWindowIsOn = .T.

	* logfile properties
	lLoggingIsOn = .T.
	cLogFile = 'F:\UTIL\ADPREPORTS\LOGFILES\DIVXXHOURS_WEEKLY_REPORT_log.txt'


	* INTERNAL email properties
	lSendInternalEmailIsOn = .T.
	cFrom = 'TGF Corporate <fmicorporate@fmiint.com>'
	cSendTo = 'mbennett@fmiint.com'
	cCC = 'mbennett@fmiint.com'
	cSubject = ''
	cAttach = ''
	cBodyText = ''

	cCOMPUTERNAME = ''
	cUSERNAME = ''


	FUNCTION INIT
		IF NOT DODEFAULT()
			RETURN .F.
		ENDIF
		WITH THIS
			*SET RESOURCE OFF
			CLOSE DATA
			SET CENTURY ON
			*SET DATE AMERICAN
			*SET HOURS TO 24
			SET ANSI ON
			SET TALK OFF
			SET DELETED ON
			SET CONSOLE OFF
			SET EXCLUSIVE OFF
			SET SAFETY OFF
			SET EXACT OFF
			SET STATUS BAR ON
			SET SYSMENU OFF
			SET ENGINEBEHAVIOR 70
			_VFP.AUTOYIELD = .lAutoYield
			.cCOMPUTERNAME = GETENV("COMPUTERNAME")
			.cUSERNAME = GETENV("USERNAME")
			IF .lTestMode THEN
				.cLogFile = 'F:\UTIL\ADPREPORTS\LOGFILES\DIVXXHOURS_WEEKLY_REPORT_log_TESTMODE.txt'
				.cSendTo = 'mbennett@fmiint.com'
			ENDIF
			.lLoggingIsOn = .lLoggingIsOn AND NOT EMPTY(.cLogFile)
			IF .lLoggingIsOn THEN
				SET ALTERNATE TO (.cLogFile) ADDITIVE
				SET ALTERNATE ON
			ENDIF
		ENDWITH
	ENDFUNC


	FUNCTION DESTROY
		WITH THIS
			IF .lLoggingIsOn  THEN
				SET ALTERNATE OFF
				SET ALTERNATE TO
			ENDIF
		ENDWITH
		DODEFAULT()
	ENDFUNC


	FUNCTION RunByJobclass
		THIS.lBreakByJobclass = .T.
	ENDFUNC


	FUNCTION MAIN
		WITH THIS
			LOCAL lnNumberOfErrors, lcSQL, lcSQL2, lcSQL3, loError
			LOCAL ldLatestPaydate, lcFiletoSaveAs
			TRY
				lnNumberOfErrors = 0

				.cStartTime = TTOC(DATETIME())

				.TrackProgress('DIV XX WEEKLY HOURS REPORT process started....', LOGIT+SENDIT)
				.TrackProgress('COMPUTERNAME = ' + .cCOMPUTERNAME, LOGIT+SENDIT)
				.TrackProgress('USERNAME = ' + .cUSERNAME, LOGIT+SENDIT)
				.TrackProgress('PROJECT = DIVXXHOURS', LOGIT+SENDIT)
				IF .lBreakByJobclass THEN
					.TrackProgress('                 RUNNING BY JOB CLASS                    ', LOGIT+SENDIT)
					.TrackProgress('=========================================================', LOGIT+SENDIT)
				ENDIF
				.TrackProgress('', LOGIT)

				* get ss#s from ADP
				OPEN DATABASE F:\UTIL\ADPCONNECTION\CONNECT1

				.nSQLHandle = SQLCONNECT("PCPAYWIN","MBENNETT","VP737A")


				*!*	, PCPAYSYS.T_CO_JOB_CLASS B
				*!*	WHERE (B.CO_C = A.COMPANYCODE) AND (B.EEOC_JOB_CLASS_C = A.EEOCJOBCLASS)

				IF .nSQLHandle > 0 THEN

					lcSQL = ;
						" SELECT " + ;
						" CHECKVIEWPAYDATE as PAYDATE, " + ;
						" CHECKVIEWPAYROLL# AS PAYNUMBER, " + ;
						" COMPANYCODE AS ADP_COMP, " + ;
						" NAME, " + ;
						" FILE# AS FILE_NUM, " + ;
						" {FN IFNULL(CHECKVIEWREGHOURS,0.00)} AS REGHOURS, " + ;
						" {FN IFNULL(CHECKVIEWOTHOURS,0.00)} AS OTHOURS, " + ;
						" ({FN IFNULL(CHECKVIEWREGHOURS,0.00)} + {FN IFNULL(CHECKVIEWOTHOURS,0.00)}) AS TOTHOURS " + ;
						" FROM REPORTS.V_CHK_VW_INFO " + ;
						" WHERE {fn LEFT(checkviewhomedept,2)} = '" +  .cDivision + "'" + ;
						" AND COMPANYCODE = 'E87' " + ;
						" AND CHECKVIEWPAYDATE IN (SELECT MAX(CHECKVIEWPAYDATE) AS  CHECKVIEWPAYDATE FROM REPORTS.V_CHK_VW_INFO) " + ;
						" ORDER BY CHECKVIEWPAYDATE, NAME "

					lcSQL2 = ;
						" SELECT " + ;
						" CO_C AS ADP_COMP, " + ;
						" EEOC_JOB_CLASS_C AS JOBCLASS, " + ;
						" DESCRIPTION_TX AS JOBDESC " + ;
						" FROM PCPAYSYS.T_CO_JOB_CLASS "

					lcSQL3 = ;
						" SELECT " + ;
						" COMPANYCODE AS ADP_COMP, " + ;
						" FILE# AS FILE_NUM, " + ;
						" {fn IFNULL(EEOCJOBCLASS,' ')} AS JOBCLASS " + ;
						" FROM REPORTS.V_EMPLOYEE "

					IF .lTestMode THEN
						.TrackProgress('                        TEST MODE                        ', LOGIT+SENDIT)
						.TrackProgress('lcSQL =' + lcSQL, LOGIT+SENDIT)
					ENDIF

					IF USED('SQLCURSOR1') THEN
						USE IN SQLCURSOR1
					ENDIF

					IF .ExecSQL(lcSQL, 'SQLCURSOR1', RETURN_DATA_MANDATORY)  AND ;
							.ExecSQL(lcSQL2, 'SQLCURJOBS', RETURN_DATA_MANDATORY)  AND ;
							.ExecSQL(lcSQL3, 'SQLCUREMPS', RETURN_DATA_MANDATORY)  THEN


						IF USED('SQLCURSOR2') THEN
							USE IN SQLCURSOR2
						ENDIF

						IF .lBreakByJobclass THEN
							SELECT ;
								ADP_COMP, ;
								TTOD(PAYDATE) AS PAYDATE, ;
								PAYNUMBER, ;
								NAME, ;
								0000 AS JOBCLASS, ;
								SPACE(30) AS JOBDESC, ;
								FILE_NUM, ;
								REGHOURS, ;
								OTHOURS, ;
								TOTHOURS ;
								FROM SQLCURSOR1 ;
								INTO CURSOR SQLCURSOR15 ;
								ORDER BY PAYDATE, NAME, PAYNUMBER ;
								READWRITE

							SELECT ADP_COMP, INT(VAL(JOBCLASS)) AS JOBCLASS, JOBDESC ;
								FROM SQLCURJOBS INTO CURSOR SQLCURJOBS2

							*!*					SELECT SQLCURJOBS2
							*!*					BROWSE

							* POPULATE JOBCLASS FIELD
							SELECT SQLCURSOR15
							SCAN
								SELECT SQLCUREMPS
								LOCATE FOR FILE_NUM == SQLCURSOR15.FILE_NUM
								IF FOUND() THEN
									REPLACE SQLCURSOR15.JOBCLASS WITH INT(VAL(SQLCUREMPS.JOBCLASS))
								ENDIF
								SELECT SQLCUREMPS
								LOCATE
							ENDSCAN

							* POPULATE JOBDESC FIELD
							SELECT SQLCURSOR15
							SCAN
								SELECT SQLCURJOBS2
								LOCATE FOR (ADP_COMP == SQLCURSOR15.ADP_COMP) AND (JOBCLASS = SQLCURSOR15.JOBCLASS)
								IF FOUND() THEN
									REPLACE SQLCURSOR15.JOBDESC WITH ALLTRIM(SQLCURJOBS2.JOBDESC)
								ENDIF
								SELECT SQLCURJOBS2
								LOCATE
							ENDSCAN

							*!*					SELECT SQLCURSOR15
							*!*					BROWSE

							SELECT ;
								PAYDATE, ;
								PAYNUMBER, ;
								JOBDESC, ;
								NAME, ;
								REGHOURS, ;
								OTHOURS, ;
								TOTHOURS ;
								FROM SQLCURSOR15 INTO CURSOR SQLCURSOR2 ;
								ORDER BY PAYDATE, PAYNUMBER, JOBDESC, NAME

						ELSE
							SELECT ;
								TTOD(PAYDATE) AS PAYDATE, ;
								PAYNUMBER, ;
								NAME, ;
								REGHOURS, ;
								OTHOURS, ;
								TOTHOURS ;
								FROM SQLCURSOR1 ;
								INTO CURSOR SQLCURSOR2 ;
								ORDER BY PAYDATE, NAME, PAYNUMBER
						ENDIF


						* determine latest paydate for documentation
						IF USED('CURLATESTPAYDATE') THEN
							USE IN CURLATESTPAYDATE
						ENDIF

						SELECT MAX(PAYDATE) AS PAYDATE ;
							FROM SQLCURSOR2 ;
							INTO CURSOR CURLATESTPAYDATE

						SELECT CURLATESTPAYDATE
						GOTO TOP
						ldLatestPaydate = CURLATESTPAYDATE.PAYDATE

						.cSubject = 'Divison ' + .cDivision + ' Weekly Report, for Pay Date: ' + DTOC(ldLatestPaydate)

						**************************************************************************************************************
						**************************************************************************************************************
						** setup file names

						lcFiletoSaveAs = "F:\UTIL\ADPREPORTS\REPORTS\DIV" + .cDivision + "HOURS" + STRTRAN(DTOC(ldLatestPaydate),"/","-") + ".XLS"

						**************************************************************************************************************
						**************************************************************************************************************

						IF FILE(lcFiletoSaveAs) THEN
							DELETE FILE (lcFiletoSaveAs)
						ENDIF

						SELECT SQLCURSOR2
						LOCATE
						COPY TO (lcFiletoSaveAs) XL5

						IF FILE(lcFiletoSaveAs) THEN
							* attach output file to email
							.cAttach = lcFiletoSaveAs
							.cBodyText = "See attached Division " + .cDivision + " Hours Breakdown Weekly report." + ;
								CRLF + CRLF + "(do not reply - this is an automated report)" + ;
								CRLF + CRLF + "<report log follows>" + ;
								CRLF + .cBodyText
						ELSE
							.TrackProgress('ERROR: unable to email spreadsheet: ' + lcFiletoSaveAs, LOGIT+SENDIT+NOWAITIT)
						ENDIF

						CLOSE DATABASES ALL
					ENDIF  &&  .ExecSQL(lcSQL, 'SQLCURSOR1', RETURN_DATA_MANDATORY)

					.TrackProgress('DIV XX WEEKLY HOURS REPORT process ended normally.', LOGIT+SENDIT+NOWAITIT)
				ELSE
					* connection error
					.TrackProgress('Unable to connect to ADP Payroll System.', LOGIT+SENDIT)
				ENDIF   &&  .nSQLHandle > 0

			CATCH TO loError

				.TrackProgress('There was an error.',LOGIT+SENDIT)
				.TrackProgress('The Error # is: ' + TRANSFORM(loError.ErrorNo), LOGIT+SENDIT)
				.TrackProgress('The Error Message is: ' + TRANSFORM(loError.MESSAGE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred in Program: ' + TRANSFORM(loError.PROCEDURE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred at line #: ' + TRANSFORM(loError.LINENO), LOGIT+SENDIT)
				lnNumberOfErrors = lnNumberOfErrors + 1
				IF TYPE('oWorkbook') = "O" THEN
					oWorkbook.SAVE()
				ENDIF
				IF TYPE('oExcel') = "O" THEN
					oExcel.QUIT()
				ENDIF
				CLOSE DATABASES ALL

			ENDTRY

			CLOSE DATA
			WAIT CLEAR
			***************** INTERNAL email results ******************************
			.TrackProgress('About to send status email.',LOGIT+SENDIT)
			.TrackProgress('The logfile is: ' + .cLogFile, LOGIT+SENDIT)
			.TrackProgress('DIV XX WEEKLY HOURS REPORT process started: ' + .cStartTime, LOGIT+SENDIT)
			.TrackProgress('DIV XX WEEKLY HOURS REPORT process finished: ' + TTOC(DATETIME()), LOGIT+SENDIT)

			IF .lSendInternalEmailIsOn THEN
				DO FORM dartmail2 WITH .cSendTo,.cFrom,.cSubject,.cCC,.cAttach,.cBodyText,"A"
				.TrackProgress('Sent status email.',LOGIT)
			ELSE
				.TrackProgress('Did not send status email: THIS.lSendInternalEmailIsOn = FALSE.',LOGIT)
			ENDIF

		ENDWITH
		RETURN
	ENDFUNC && main


	FUNCTION SetDivision
		LPARAMETERS tnDivision
		WITH THIS
			.nDivision = tnDivision
			.cDivision = PADL(.nDivision,2,'0')
			.TrackProgress('Set Division = ' + .cDivision,LOGIT+SENDIT)
			IF NOT .lTestMode THEN
				DO CASE
					CASE .nDivision = 3
						*.cSendTo = 'bsouthwell@fmiint.com'
						.cSendTo = 'michael.divirgilio@tollgroup.com'
					CASE .nDivision = 4
						.cSendTo = 'jim.killen@tollgroup.com'
					CASE .nDivision = 14
						.cSendTo = 'jim.killen@tollgroup.com, Todd.Margolin@Tollgroup.com'
					OTHERWISE
						* nothing
				ENDCASE
			ENDIF
			.TrackProgress("Set Division to '" + .cDivision + "'",LOGIT+SENDIT)
			.TrackProgress(".cSendTo = " + .cSendTo,LOGIT+SENDIT)
		ENDWITH
	ENDFUNC


	FUNCTION ExecSQL
		LPARAMETERS tcSQL, tcCursorName, tlNoDataReturnedIsOkay
		LOCAL llRetval, lnResult
		WITH THIS
			* close target cursor if it's open
			IF USED(tcCursorName)
				USE IN (tcCursorName)
			ENDIF
			WAIT WINDOW "Fetching remote data; please wait..." NOWAIT
			lnResult = sqlexec(.nSQLHandle, tcSQL, tcCursorName)
			llRetval = ( lnResult > 0 )
			IF llRetval THEN
				* see if any data came back
				IF NOT tlNoDataReturnedIsOkay THEN
					IF (NOT USED(tcCursorName)) OR EOF(tcCursorName) THEN
						llRetval = .F.
						.TrackProgress('No data was returned by this query: ' +CRLF + tcSQL, LOGIT+SENDIT)
						.cSendTo = 'Mark Bennett <mbennett@fmiint.com>'
					ENDIF
				ENDIF
			ELSE
				.TrackProgress("Result Code [" + TRANSFORM(lnResult) + "] returned by this query: " + tcSQL, LOGIT+SENDIT)
				.cSendTo = 'Mark Bennett <mbennett@fmiint.com>'
			ENDIF
			WAIT CLEAR
			RETURN llRetval
		ENDWITH
	ENDFUNC


	PROCEDURE TrackProgress
		* do any combination of Wait Window, writing to logfile, and adding to body of email,
		* based on nFlags parameter.
		LPARAMETERS tcExpression, tnFlags
		WITH THIS
			IF BITAND(tnFlags,LOGIT) = LOGIT THEN
				IF .lLoggingIsOn THEN
					?
					? .cProcessName + "  " + TTOC(DATETIME()) + ": " + tcExpression
				ENDIF
			ENDIF
			IF .lWaitWindowIsOn THEN
				IF BITAND(tnFlags,NOWAITIT) = NOWAITIT THEN
					WAIT WINDOW tcExpression NOWAIT
				ENDIF
				IF BITAND(tnFlags,WAITIT) = WAITIT THEN
					WAIT WINDOW tcExpression TIMEOUT .nWaitWindowTimeout
				ENDIF
			ENDIF
			IF BITAND(tnFlags,SENDIT) = SENDIT THEN
				IF .lSendInternalEmailIsOn THEN
					.cBodyText = .cBodyText + tcExpression + CRLF + CRLF
				ENDIF
			ENDIF
		ENDWITH
	ENDPROC  &&  TrackProgress

ENDDEFINE

