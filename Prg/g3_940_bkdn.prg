*!* m:\dev\prg\bernardo_bkdn.prg
CLEAR
lCheckStyle = .T.
lPrepack = .F.
m.color=""
cUCCNumber = ""
cUsePack = ""
lcUCCSeed = 1
lcPtType = "STD"

SELECT x856
COUNT FOR TRIM(segment) = "ST" TO nSegCnt
cSegCnt = ALLTRIM(STR(nSegCnt))
SELECT x856
LOCATE

lTestrun = .F.
ptctr = 0

WAIT WINDOW "Now in 940 Breakdown" TIMEOUT 1

WAIT "There are "+cSegCnt+" P/Ts in this file" WINDOW TIMEOUT 1
SELECT x856
LOCATE

*!* Constants
m.userid = "BER940PROC"
m.insdttm = DATETIME()
m.accountid = nAcctNum
m.acct_name = "G-III"
STORE nAcctNum TO m.acct_num
m.careof = " "
m.sf_addr1 = "463 SEVENTH AVE. - 7TH FLOOR"
m.sf_csz = "NEW YORK, NY 10019"
*!* End constants

*!*	IF !USED("isa_num")
*!*		IF lTesting
*!*			USE F:\3pl\test\isa_num IN 0 ALIAS isa_num
*!*		ELSE
*!*			USE F:\3pl\DATA\isa_num IN 0 ALIAS isa_num
*!*		ENDIF
*!*	ENDIF

*!*	IF !USED("uploaddet")
*!*		IF lTesting
*!*			USE F:\ediwhse\test\uploaddet IN 0 ALIAS uploaddet
*!*		ELSE
*!*			USE F:\ediwhse\DATA\uploaddet IN 0 SHARED ALIAS uploaddet
*!*		ENDIF
*!*	ENDIF

SELECT xpt
DELETE ALL
SELECT xptdet
DELETE ALL

STORE 0 TO pt_total_eaches,pt_total_cartons,nCtnCount,nptid,nptdetid,m.weight
STORE 1 TO m.carton_id

SELECT x856
SET FILTER TO
LOCATE

IF lTesting
*  SET STEP ON
ENDIF

STORE "" TO cisa_num

WAIT WINDOW "NOW IN INITAL HEADER BREAKDOWN" TIMEOUT 1
WAIT WINDOW "NOW IN INITAL HEADER BREAKDOWN" NOWAIT
*ASSERT .F. MESSAGE "At header bkdn...debug"
DO WHILE !EOF("x856")
	cSegment = TRIM(x856.segment)
	cCode = TRIM(x856.f1)

	IF TRIM(x856.segment) = "ISA"
		cisa_num = ALLTRIM(x856.f13)
		WAIT WINDOW "This is a "+cProperName+" upload." TIMEOUT 2
		ptctr = 0
		SELECT x856
		SKIP
		LOOP
	ENDIF

	IF TRIM(x856.segment) = "GS"
		cgs_num = ALLTRIM(x856.f6)
		SELECT x856
		SKIP
		LOOP
	ENDIF

	IF TRIM(x856.segment) = "ST" AND TRIM(x856.f1) = "940"
		SELECT x856
		SKIP
		LOOP
	ENDIF

	IF TRIM(x856.segment) = "W05"  && new shipment
		nCtnCount = 0
		m.style = ""
		SELECT xpt
		nDetCnt = 0
		APPEND BLANK

		REPLACE xpt.shipins WITH "FILENAME*"+cFilename IN xpt
		REPLACE xpt.qty WITH 0 IN xpt
		REPLACE xpt.origqty WITH 0 IN xpt
		STORE 0 TO pt_total_eaches
		STORE 0 TO pt_total_cartons
		nptid = nptid +1
		WAIT WINDOW AT 10,10 "Now processing PT# "+ ALLTRIM(x856.f2)+" Number "+CHR(13)+TRANSFORM(ptctr)+" of "+cSegCnt NOWAIT

		REPLACE xpt.ptid       WITH nptid IN xpt
		REPLACE xpt.accountid  WITH nAcctNum IN xpt
		REPLACE xpt.ptdate     WITH DATE() IN xpt
		m.cnee_ref = ALLTRIM(x856.f3)

&& PG Removed  05/19/2017 via email

		m.ship_ref = ALLTRIM(x856.f2)&&+"-16"
		cship_ref  = ALLTRIM(x856.f2)&&+"-16"
		m.pt       = ALLTRIM(x856.f2)&&+"-16"

		REPLACE xpt.ship_ref   WITH m.ship_ref IN xpt  && Pick Ticket
		REPLACE xpt.cnee_ref   WITH m.cnee_ref IN xpt  && PO Number
		IF lTestrun
			REPLACE xpt.cacctnum   WITH "999" IN xpt
		ENDIF
		SELECT x856
		SKIP
		LOOP
	ENDIF

	IF ALLTRIM(x856.segment) = "N1" AND ALLTRIM(x856.f1) = "SF"

		cAcctCode = ALLTRIM(x856.f4)
		DO CASE && added this because G-III won't send us a complete division list, 07.30.2016
*!*        CASE cAcctCode = "LH"  && Merged with CK Handbags per Esther, 08.23.2016
*!*          nAcctNum = 6664
		CASE cAcctCode = "PL"
			nAcctNum = 6623
		CASE INLIST(cAcctCode,"LH","CH")
			nAcctNum = 6639
		CASE cAcctCode = "TP"
			nAcctNum = 6657
		CASE cAcctCode = "62"
			nAcctNum = 6678
		CASE cAcctCode = "CM"
			nAcctNum = 6679
		CASE cAcctCode = "KE"
			nAcctNum = 6621
		CASE cAcctCode = "AP"
			nAcctNum = 6622
		CASE cAcctCode = "AL"
			nAcctNum = 6661
		CASE cAcctCode = "MM"
			nAcctNum = 6666
		CASE cAcctCode = "MW"
			nAcctNum = 6667
		CASE cAcctCode = "BC"
			nAcctNum = 6680
		CASE cAcctCode = "TD"
			nAcctNum = 6698
		CASE cAcctCode = "TJ"
			nAcctNum = 6700
		CASE cAcctCode = "15"
			nAcctNum = 6701
		CASE cAcctCode = "TL"
			nAcctNum = 6713
		CASE cAcctCode = "TS"
			nAcctNum = 6714
		CASE cAcctCode = "TT"
			nAcctNum = 6715
		CASE cAcctCode = "IS"
			nAcctNum = 6724
		CASE cAcctCode = "CD"
			nAcctNum = 6731
		CASE cAcctCode = "CL"
			nAcctNum = 6732
		CASE cAcctCode = "PF"
			nAcctNum = 6733
		CASE cAcctCode = "CW"
			nAcctNum = 6734
		CASE cAcctCode = "CS"
			nAcctNum = 6735
		CASE cAcctCode = "CG"
			nAcctNum = 6736
		CASE cAcctCode = "17"
			nAcctNum = 6738
		CASE cAcctCode = "ID"
			nAcctNum = 6739
		CASE cAcctCode = "VC"
			nAcctNum = 6740
		CASE cAcctCode = "22"
			nAcctNum = 6741
		CASE cAcctCode = "IW"
			nAcctNum = 6743
		CASE cAcctCode = "CK"
			nAcctNum = 6750
		CASE cAcctCode = "AH"
			nAcctNum = 6753
		CASE cAcctCode = "DW"
			nAcctNum = 6767
		CASE cAcctCode = "DH"
			nAcctNum = 6807
		CASE cAcctCode = "TW"
			nAcctNum = 6716
		CASE cAcctCode = "TJ"
			nAcctNum = 6700


		OTHERWISE
			lcUpErrPath = "F:\FTPUSERS\G-III\940IN"+IIF(cOffice = "I","-NJ","-CA")+"\hold\"
			lcUpErrFile = lcUpErrPath+cFilename
			COPY FILE [&xfile] TO [&lcuperrfile]
			DELETE FILE [&xfile]

			cErrMsg = "N1SF contains unknown Division code: "+cAcctCode+", Div. "+ALLTRIM(x856.f3)
			SET STEP ON
			THROW
		ENDCASE
		REPLACE xpt.accountid WITH nAcctNum IN xpt NEXT 1
	ENDIF

	IF TRIM(x856.segment) = "N1" AND TRIM(x856.f1) = "ST" && Ship-to data
*!*			#IF 0
*!*				SELECT uploaddet
*!*				LOCATE
*!*				IF !EMPTY(cisa_num)
*!*					APPEND BLANK
*!*					REPLACE uploaddet.office    WITH cOffice IN uploaddet
*!*					REPLACE uploaddet.cust_name WITH UPPER(TRIM(x856.f2)) IN uploaddet
*!*					REPLACE uploaddet.acct_num  WITH nAcctNum IN uploaddet
*!*					REPLACE uploaddet.DATE      WITH DATETIME() IN uploaddet
*!*					REPLACE uploaddet.isa_num   WITH cisa_num IN uploaddet
*!*					REPLACE uploaddet.startpt   WITH xpt.ship_ref IN uploaddet
*!*					REPLACE uploaddet.runid     WITH lnRunID IN uploaddet
*!*				ENDIF
*!*			#ENDIF

		SELECT xpt
		m.consignee = UPPER(ALLTRIM(x856.f2))
		REPLACE xpt.consignee WITH m.consignee IN xpt
		REPLACE xpt.dcnum WITH ALLTRIM(x856.f4) IN xpt

		SKIP 1 IN x856
		IF TRIM(x856.segment) = "N3"  && address info
			REPLACE xpt.address  WITH UPPER(ALLTRIM(x856.f1)) IN xpt
			REPLACE xpt.address2 WITH UPPER(ALLTRIM(x856.f2)) IN xpt
		ENDIF
		SKIP 1 IN x856
		IF TRIM(x856.segment) = "N4"  && address info
			m.CSZ = UPPER(TRIM(x856.f1))+", "+UPPER(TRIM(x856.f2))+" "+TRIM(x856.f3)
			REPLACE xpt.CSZ WITH m.CSZ IN xpt
			REPLACE xpt.country WITH ALLTRIM(x856.f4) IN xpt
		ENDIF
		SELECT x856
		SKIP
		LOOP
	ENDIF

	IF TRIM(x856.segment) = "N1" AND TRIM(x856.f1) = "BY" && Ship-for data
		SELECT xpt
		REPLACE xpt.shipfor WITH UPPER(ALLTRIM(x856.f2)) IN xpt
		REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"STORENAME*"+ALLTRIM(x856.f2) IN xpt
		cStoreNum = ALLTRIM(x856.f4)
		REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"STORENUM*"+cStoreNum IN xpt
		nStoreNum = INT(VAL(cStoreNum))
		IF !nStoreNum > 99999
			REPLACE xpt.storenum  WITH nStoreNum IN xpt
		ENDIF

		REPLACE xpt.sforstore WITH ALLTRIM(x856.f4) IN xpt
		SKIP 1 IN x856
		IF TRIM(x856.segment) = "N3"  && address info
			REPLACE xpt.sforaddr1 WITH UPPER(TRIM(x856.f1)) IN xpt
			REPLACE xpt.sforaddr2 WITH UPPER(TRIM(x856.f2)) IN xpt
		ELSE
			SELECT x856
			LOOP
		ENDIF
		SKIP 1 IN x856
		IF TRIM(x856.segment) = "N4"  && address info
			csforcsz = UPPER(TRIM(x856.f1))+", "+UPPER(TRIM(x856.f2))+" "+TRIM(x856.f3)
			REPLACE xpt.sforcsz WITH csforcsz IN xpt
		ENDIF
		SELECT x856
		SKIP
		LOOP
	ENDIF

	IF TRIM(x856.segment) = "N9" AND TRIM(x856.f1) = "DP"  && customer dept
		STORE "" TO m.dept,m.deptname
		m.dept = ALLTRIM(x856.f2)
		m.deptname = ALLTRIM(x856.f3)
		REPLACE xpt.dept WITH m.dept IN xpt
		REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"DEPTNAME*"+m.deptname IN xpt
		SELECT x856
		SKIP
		LOOP
	ENDIF

	IF TRIM(x856.segment) = "N9" AND TRIM(x856.f1) = "FOB"  && customer dept
		cFOB = ALLTRIM(x856.f2)
		REPLACE xpt.div WITH cFOB IN xpt
		REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"FOB*"+cFOB IN xpt
		SELECT x856
		SKIP
		LOOP
	ENDIF

	IF TRIM(x856.segment) = "N9" AND TRIM(x856.f1) = "14"
		REPLACE xpt.cacctnum WITH ALLTRIM(x856.f2) IN xpt
		IF ALLTRIM(x856.f2) = "08077"
			llQVC_pickticket = .T.
		ELSE
			llQVC_pickticket = .F.
		ENDIF
		SELECT x856
		SKIP
		LOOP
	ENDIF

	IF TRIM(x856.segment) = "N9" AND TRIM(x856.f1) = "IA"  && PO #
		REPLACE xpt.vendor_num WITH ALLTRIM(x856.f2)IN xpt
		SELECT x856
		SKIP
		LOOP
	ENDIF

	IF TRIM(x856.segment) = "N9" AND TRIM(x856.f1) = "TYP"  && order type
		REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"ORDERTYPE*"+ALLTRIM(x856.f2) IN xpt
		SELECT x856
		SKIP
		LOOP
	ENDIF

	IF TRIM(x856.segment) = "N9" AND TRIM(x856.f1) = "WM"  && Sams Walmart order type
		REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"MRTYPE*"+ALLTRIM(x856.f2) IN xpt
		SELECT x856
		SKIP
		LOOP
	ENDIF

	IF TRIM(x856.segment) = "N9" AND TRIM(x856.f1) = "BT"  && order type
		REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"BOL*"+ALLTRIM(x856.f2) IN xpt
		SELECT x856
		SKIP
		LOOP
	ENDIF
	IF TRIM(x856.segment) = "N9" AND TRIM(x856.f1) = "CR"  && order type
		REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"CRVALUE*"+ALLTRIM(x856.f2) IN xpt
		SELECT x856
		SKIP
		LOOP
	ENDIF


	IF TRIM(x856.segment) = "G62" AND TRIM(x856.f1) == "53"  && start date
		lcDate = ALLTRIM(x856.f2)
		lxDate = SUBSTR(lcDate,5,2)+"/"+SUBSTR(lcDate,7,2)+"/"+LEFT(lcDate,4)
		ldDate = CTOD(lxDate)
		STORE ldDate TO dStart
		REPLACE xpt.START WITH ldDate IN xpt
		SELECT x856
		SKIP
		LOOP
	ENDIF

	IF TRIM(x856.segment) = "G62" AND TRIM(x856.f1) == "54"  && cancel date
		lcDate = ALLTRIM(x856.f2)
		lxDate = SUBSTR(lcDate,5,2)+"/"+SUBSTR(lcDate,7,2)+"/"+LEFT(lcDate,4)
		ldDate = CTOD(lxDate)
		STORE ldDate TO dCancel
		REPLACE xpt.CANCEL WITH ldDate IN xpt
		SELECT x856
		SKIP
		LOOP
	ENDIF

	IF TRIM(x856.segment) = "G62" AND TRIM(x856.f1) == "52"  && ORDER DATE
		REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"ORDERDATE*"+ALLTRIM(x856.f2) IN xpt
		SELECT x856
		SKIP
		LOOP
	ENDIF
	IF TRIM(x856.segment) = "G62" AND TRIM(x856.f1) == "04"  && PO DATE
		REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"PODATE*"+ALLTRIM(x856.f2) IN xpt
		SELECT x856
		SKIP
		LOOP
	ENDIF

	IF TRIM(x856.segment) = "W66"
		cSCAC = ALLTRIM(x856.f10)
		REPLACE xpt.scac WITH cSCAC IN xpt
		cShip_via = ALLTRIM(x856.f5)
		REPLACE xpt.ship_via WITH cShip_via IN xpt
		SELECT x856
		SKIP
		LOOP
	ENDIF

*!* Start of PT detail, stay here until this PT is complete
	WAIT WINDOW "NOW IN DETAIL LOOP PROCESSING" NOWAIT


	IF TRIM(x856.segment) = "LX"
		IF TRIM(x856.segment) = "LX" AND ALLTRIM(x856.f1) = "1"
			m.ucc =""
			SKIP 1 IN x856
			IF TRIM(x856.segment) = "MAN"
				lcPtType = "PRECART"
			ENDIF
			SKIP -1 IN x856 && reset the record pointer
		ENDIF

		SKIP 1 IN x856

		DO WHILE !INLIST(ALLTRIM(x856.segment),"SE")
			cSegment = TRIM(x856.segment)
			cCode = TRIM(x856.f1)

			IF TRIM(x856.segment) = "MAN" AND TRIM(x856.f1) = "GM"
				m.ucc = ALLT(x856.f2)
				IF !"PRECART"$xpt.shipins
					REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"PRECART" IN xpt
				ENDIF

			ENDIF


			IF TRIM(x856.segment) = "W01" && Destination Quantity
				nptdetid = nptdetid + 1

				SELECT xptdet
				APPEND BLANK
				REPLACE xptdet.ptid WITH xpt.ptid IN xptdet
				REPLACE xptdet.ptdetid WITH nptdetid IN xptdet
				REPLACE xptdet.units WITH m.units IN xptdet
				REPLACE xptdet.accountid WITH xpt.accountid IN xptdet

				REPLACE xptdet.totqty  WITH VAL(ALLTRIM(ALLTRIM(x856.f1)))
				m.totqty = VAL(ALLTRIM(ALLTRIM(x856.f1)))
				REPLACE xptdet.origqty WITH xptdet.totqty IN xptdet
				REPLACE xpt.qty        WITH xpt.qty+xptdet.totqty IN xpt

				IF ALLTRIM(x856.f4) = "VA"
					m.style = ALLTRIM(x856.f5)
				ENDIF
				IF ALLTRIM(x856.f6) = "UP"  && added 06.06.2013
					m.upc = ALLTRIM(x856.f7)
				ENDIF
				REPLACE xptdet.printstuff WITH xptdet.printstuff+CHR(13)+"UNITCODE*"+ALLTRIM(x856.f2)

				REPLACE xptdet.STYLE WITH m.style IN xptdet
				REPLACE xptdet.upc   WITH m.upc IN xptdet
				REPLACE xptdet.PACK  WITH "1" IN xptdet
				REPLACE xptdet.units WITH .T. IN xptdet
			ENDIF


			IF TRIM(x856.segment) = "N9" AND ALLTRIM(x856.f1) = "IN"
				REPLACE xptdet.custsku WITH ALLTRIM(x856.f2) IN xptdet
			ENDIF

			IF TRIM(x856.segment) = "N9" AND ALLTRIM(x856.f1) = "VC"
				m.color = ALLTRIM(x856.f2)
				m.colorname = ALLTRIM(x856.f3)
				REPLACE xptdet.COLOR WITH m.color IN xptdet
				REPLACE xptdet.printstuff WITH xptdet.printstuff+CHR(13)+"COLORNAME*"+m.colorname
			ENDIF

			IF TRIM(x856.segment) = "N9" AND ALLTRIM(x856.f1) = "SZ"
				m.id = ALLTRIM(x856.f2)
				REPLACE xptdet.ID WITH m.id IN xptdet
			ENDIF

			IF TRIM(x856.segment) = "G69"
				cprtstr = "DESC*"+UPPER(TRIM(x856.f1))
				REPLACE xptdet.printstuff WITH xptdet.printstuff+CHR(13)+cprtstr IN xptdet
			ENDIF

			IF TRIM(x856.segment) = "AMT" AND ALLTRIM(x856.f1) = "LI"
				IF lcPtType = "PRECART"
					INSERT INTO tempppdata FROM MEMVAR
					SELECT tempppdata

				ENDIF
			ENDIF
			IF !EOF()
				SKIP 1 IN x856
			ENDIF
			IF TRIM(x856.segment) = "SE"
				EXIT
			ENDIF
		ENDDO

		SELECT xptdet
	ENDIF

	SELECT x856
	IF !EOF()
		SKIP 1 IN x856
	ENDIF
ENDDO

SELECT xptdet

rollup()
WAIT WINDOW "At end of ROLLUP procedure" TIMEOUT 2

IF lBrowfiles
	WAIT WINDOW "If tables are incorrect, CANCEL when XPTDET window opens"+CHR(13)+"Otherwise, will load PT table" TIMEOUT 2
	SELECT xpt
	LOCATE
	BROWSE && TIMEOUT 10
	SELECT xptdet
	LOCATE
	BROWSE
ENDIF

gmasteroffice="C"
goffice="6"

IF lcPtType = "PRECART"
	SELECT ship_ref FROM xpt INTO CURSOR xxpts GROUP BY ship_ref
	SELECT xxpts
	SCAN
		lcquery = [delete from ppdata where ship_ref=']+ALLTRIM(xxpts.ship_ref)+[']
		xsqlexec(lcquery,,,"wh")
	ENDSCAN
ENDIF

IF USED('ppdata')
	USE IN ppdata
ENDIF
useca("ppdata","wh")

SELECT ppdata
SCATTER MEMVAR MEMO BLANK
SELECT tempppdata

LOCATE
REPLACE ALL filename WITH xfile IN tempppdata
DELETE FOR EMPTY(ucc)

LOCATE
SCAN
	SCATTER MEMVAR MEMO
	m.scc214 = ALLTRIM(cFilename)  && adde this 9/9/2016 so I can Id file uploads
	TRY
		m.ppdataid=dygenpk("ppdata","whall")
	CATCH TO oErr
		? tempppdata.ucc,tempppdata.upc,tempppdata.STYLE,tempppdata.COLOR,tempppdata.ID
		THROW
	ENDTRY
	insertinto("ppdata","wh")
ENDSCAN

tu("ppdata")

*BROWSE
** made some changes here as we need the ppdata to be persistent as we may upload many 940s before we make work orders
** and cartonize the pick tickets

*COPY TO F:\3pl\DATA\ppdata-test

*Select ppdata_carson
*Append From F:\3pl\Data\ppdata-test

WAIT WINDOW cCustname+" Breakdown Round complete..." TIMEOUT 2
SELECT x856
WAIT CLEAR



*************************************************************************************************
*!* Error Mail procedure - W01*12 Segment
*************************************************************************************************
PROCEDURE errormail1

IF lEmail
	SELECT 0
	USE F:\3pl\DATA\mailmaster ALIAS mm SHARED
	LOCATE FOR mm.edi_type = "MISC" AND mm.taskname = "MODSHOEERR"
	STORE TRIM(IIF(mm.use_alt,mm.sendtoalt,mm.sendto)) TO tsendto
	STORE TRIM(IIF(mm.use_alt,mm.ccalt,mm.cc)) TO tcc
	USE IN mm
	tsubject= "FMI "+cMailLoc+" "+cCustname+" Pickticket Error: " +TTOC(DATETIME())
	tmessage = "File: "+cFilename+CHR(13)+" is missing W01*12 segment(s), causing a divide by zero error."
	tmessage = tmessage + "Please re-create and resend this file."
	DO FORM m:\dev\frm\dartmail2 WITH tsendto,tFrom,tsubject,tcc,tattach,tmessage,"A"
ENDIF
ENDPROC

*************************************************************************************************
*!* Error Mail procedure - only for QVC UPC issues
*************************************************************************************************
PROCEDURE errormail_qvc
PARAMETERS lcstyle

tFrom ="Toll EDI Operations <toll-edi-ops@tollgroup.com>"
tsendto ="pgaidis@fmiint.com,juan@fmiint.com,maria.estrella@tollgroup.com,joe.bianchi@tollgroup.com"
tsendto ="pgaidis@fmiint.com"
tcc=""
tattach  = ""
tsubject= "Bernardo QVC UPC Issue Pickticket Error: " +TTOC(DATETIME())
tmessage = "Missing UPC for :"+lcstyle+CHR(13)
tmessage = tmessage + "Please make corrections and reload file: "+cFilename
DO FORM m:\dev\frm\dartmail2 WITH tsendto,tFrom,tsubject,tcc,tattach,tmessage,"A"

ENDPROC
*************************************************************************************************
*!* Error Mail procedure - Cancel Before Start Date
*************************************************************************************************
PROCEDURE errordates

IF lEmail
	tsendto = tsendtotest
	tcc =  tcctest
	tsubject= "FMI "+cMailLoc+" "+cCustname+" Pickticket Error: " +TTOC(DATETIME())
	tmessage = "File: "+cFilename+CHR(13)+" has Cancel date(s) which precede Start date(s)"
	tmessage = tmessage + "File needs to be checked and possibly resubmitted."
	DO FORM m:\dev\frm\dartmail2 WITH tsendto,tFrom,tsubject,tcc,tattach,tmessage,"A"
ENDIF
ENDPROC

******************************
PROCEDURE rollup
******************************
*  Set Step On

WAIT WINDOW  "In rollup procedure" TIMEOUT 1
WAIT WINDOW  "In rollup procedure" NOWAIT
nChangedRecs = 0

SELECT xptdet
SET DELETED ON
LOCATE
WAIT WINDOW "Record count: "+TRANSFORM(RECCOUNT()) TIMEOUT 3
IF lTesting
	WAIT WINDOW "Browsing XPTDET pre-rollup" TIMEOUT 2
	BROWSE TIMEOUT 10
ENDIF
cMsgStr = "PT/OSID/ODIDs changed: "
nReccnt1= 0
SCAN FOR !DELETED()
	SCATTER MEMVAR
	nRecno = RECNO()
	COUNT TO nLoops FOR xptdet.ptid = m.ptid ;
		AND xptdet.STYLE = m.style ;
		AND xptdet.COLOR = m.color ;
		AND xptdet.ID = m.id ;
		AND xptdet.PACK = m.pack ;
		AND xptdet.upc = m.upc ;
		AND RECNO('xptdet') > nRecno
	GO nRecno
	IF nLoops = 0
		LOOP
	ENDIF
	
	IF EOF()
		EXIT
	ENDIF
	nReccnt1=nReccnt1+1

	WAIT WINDOW "Number of loops to run, PT "+ALLTRIM(xpt.ship_ref)+" # Loops: "+TRANSFORM(nLoops)+CHR(13)+"Record "+TRANSFORM(nReccnt1)+" of "+TRANSFORM(RECCOUNT()) nowait
	FOR tt = 1 TO nLoops
		LOCATE FOR xptdet.ptid = m.ptid ;
			AND xptdet.STYLE = m.style ;
			AND xptdet.COLOR = m.color ;
			AND xptdet.ID = m.id ;
			AND xptdet.PACK = m.pack ;
			AND xptdet.upc = m.upc ;
			AND RECNO('xptdet') > nRecno
		IF FOUND()
			SELECT xpt
			LOCATE FOR xpt.ptid = m.ptid
			IF !FOUND()
				WAIT WINDOW "PROBLEM!"
			ENDIF
			cship_ref = ALLT(xpt.ship_ref)
			IF nChangedRecs > 10
				cMsgStr = "More than 10 records rolled up"
			ELSE
				cMsgStr = cMsgStr+CHR(13)+cship_ref+" "+TRANSFORM(m.ptid)+" "+TRANSFORM(m.ptdetid)
			ENDIF
			nChangedRecs = nChangedRecs+1
			SELECT xptdet
			alength = ALINES(aptdet,xptdet.printstuff,.T.,CHR(13))
			cStoredUCC = ALLTRIM(segmentget(@aptdet,"UCC",alength))
			nTotqty1 = xptdet.totqty
			DELETE NEXT 1 IN xptdet
		ENDIF
		GO nRecno
*   Replace xptdet.printstuff With xptdet.printstuff+Chr(13)+"UCC*"+cStoredUCC In xptdet
		REPLACE xptdet.totqty WITH xptdet.totqty+nTotqty1 IN xptdet
		REPLACE xptdet.origqty WITH xptdet.totqty IN xptdet
	ENDFOR

	SELECT xptdet
ENDSCAN

*SET STEP ON

IF nChangedRecs > 0
	WAIT WINDOW cMsgStr TIMEOUT 2
	WAIT WINDOW "There were "+TRANS(nChangedRecs)+" records rolled up" TIMEOUT 3
ELSE
	WAIT WINDOW "There were NO records rolled up" TIMEOUT 1
ENDIF

ENDPROC

****************************
PROCEDURE segmentget
****************************

PARAMETER thisarray,lcKey,nLength

FOR i = 1 TO nLength
	IF i > nLength
		EXIT
	ENDIF
	lnEnd= AT("*",thisarray[i])
	IF lnEnd > 0
		lcThisKey =TRIM(SUBSTR(thisarray[i],1,lnEnd-1))
		IF OCCURS(lcKey,lcThisKey)>0
			RETURN SUBSTR(thisarray[i],lnEnd+1)
			i = 1
		ENDIF
	ENDIF
ENDFOR

RETURN ""
