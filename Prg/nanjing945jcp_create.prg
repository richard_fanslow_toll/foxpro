*!* NANJING USA 945 JCPenney & SEARS ONLY (Whse. Shipping Advice) Creation Program
*!* Creation Date: 12.12.2005 by Joe, last mod 03.27.2007

PARAMETERS nWO_Num,cBOL,cShip_ref,cOffice,lPTFlag,tTrigtime

PUBLIC ARRAY thisarray(1)
PUBLIC cWO_Num,cWO_NumStr,cWO_NumOld,cCustName,nUnitSum,tsendto,tcc,tsendtoerr,tccerr,nOrigSeq,nOrigGrpSeq,cErrMsg
PUBLIC cMod,cMBOL,cFilenameShort,cFilenameOut,cFilenameArch,cWO_NumList,lcPath,cEDIType,cISA_Num
PUBLIC tsendtotest,tcctest,tsendtoqty,tccqty,cUseFolder,lNanFilesOut,cProgName,lOverflow
cProgName = "nanjing945jcp-create.prg"


lIsError = .F.
lDoCatch = .T.
IF !lPTFlag
	cShip_ref = ""
ENDIF

cSize = " "
cMBOL = ""
lCloseOutput = .T.
lNewPTStruc = .T.  && When Nanjing starts new PT format, approx 08.01.2008, set to .t.
tfrom = "Toll EDI 945 Poller Operations <toll-edi-ops@tollgroup.com>"

TRY
	lTesting = .F.
	lTestinput = .f.
	lISAFlag = .T.
	lSTFlag = .T.
	lOutlet = .F.
	lCloseOutput = .T.

	DO m:\dev\prg\_setvars WITH lTesting
	ON ESCAPE CANCEL

	lMismatch = .F.
	nAcctNum = 4694
	lPick = .F.
	nOrigSeq = 0
	nOrigGrpSeq = 0

	STORE "" TO cWO_NumStr,cErrMsg
	STORE "XXX" TO cWO_NumOld
	nRec1 = 9999999
	SET ESCAPE ON
	ON ESCAPE CANCEL

	cTrigTime = TIME(tTrigtime)
	cTime = LEFT(cTrigTime,2)+SUBSTR(cTrigTime,4,2)
	lOverflow = .F.

	cPPName = "Nanjing"

	IF TYPE("cOffice") = "L"
		IF !lTesting
			WAIT WINDOW "Office not provided...terminating" TIMEOUT 3
			lCloseOutput = .F.
			cErrMsg = "No OFFICE"
			DO ediupdate WITH cErrMsg,.T.
			THROW
		ELSE
			CLOSE DATABASES ALL
			cOffice = "C"
			nWO_Num = 6006119
			cBOL = "04907314610000009"
		ENDIF
	ENDIF
	cMod = IIF(cOffice = "C","1",cOffice)
	goffice = cMod
	gMasterOffice = IIF(cOffice = "I","N",cOffice)
	cEDIType = "945"
	cBOL = ALLTRIM(cBOL)

	lParcelType = .F.
	CREATE CURSOR temp945 (accountid i,isa_num c(10),st_num c(10),wo_num i,bol_no c(20),ship_ref c(20),filename c(50),filedate T)
	CREATE CURSOR qtycompare (ship_ref c(10), osqty i, ppqty i,pctdiff i)

	IF TYPE("nWO_Num")<> "N"
		lCloseOutput = .F.
		cErrMsg = "BAD WO#"
		DO ediupdate WITH cErrMsg,.T.
		THROW
	ENDIF

	lEmail = .T.
	lTestMail = lTesting && Sends mail to Joe only
	lNanFilesOut = !lTesting && If true, copies output to FTP folder (default = .t.)
	lStandalone = lTesting
	STORE "LB" TO cWeightUnit

	cUseFolder = "F:\WHY\WHDATA\"
	IF lTestinput
		cUseFolder = "F:\WHP\WHDATA\"
	ENDIF
	ASSERT .F.
	DO m:\dev\prg\swc_cutctns WITH cBOL

*!*		lNanFilesout = .F.
*!*		lTestMail = .T.

	PUBLIC c_CntrlNum,c_GrpCntrlNum,cReturned
	WAIT WINDOW "Now preparing tables and variables" NOWAIT NOCLEAR

*!* SET CUSTOMER CONSTANTS
	cCustName = "NANJING"  && Customer Identifier
	cX12 = "004050"  && X12 Standards Set used
	cMailName = "Nanjing USA"
	SELECT 0
	USE F:\3pl\DATA\mailmaster ALIAS mm
	LOCATE FOR edi_type = '945' AND mm.office = cOffice AND mm.accountid = nAcctNum
	tsendto = IIF(mm.use_alt,sendtoalt,mm.sendto)
	tcc = IIF(mm.use_alt,mm.ccalt,mm.cc)
	LOCATE
	LOCATE FOR mm.office = 'X' AND mm.accountid = 9999
	tsendtoerr = IIF(mm.use_alt,sendtoalt,mm.sendto)
	tccerr = IIF(mm.use_alt,mm.ccalt,mm.cc)
	LOCATE
	LOCATE FOR mm.edi_type = 'MISC' AND mm.taskname = 'GENERAL'
	tsendtotest = IIF(mm.use_alt,sendtoalt,mm.sendto)
	tcctest = IIF(mm.use_alt,mm.ccalt,mm.cc)
	LOCATE
	LOCATE FOR mm.edi_type = 'MISC' AND mm.taskname = 'NANQTYERR'
	tsendtoqty = IIF(mm.use_alt,sendtoalt,mm.sendto)
	tccqty = IIF(mm.use_alt,mm.ccalt,mm.cc)


*!* SET OTHER CONSTANTS
	DO CASE
	CASE cOffice = "M"
		cCustLoc =  "FL"
		cFMIWarehouse = ""
		cCustPrefix = "945"+"f"
		cDivision = "Florida"
*		cFolder = "WHM"
		cSF_Addr1  = "11400 NW 32ND AVE"
		cSF_CSZ    = "MIAMI*FL*33167"

	CASE cOffice = "N"
		cCustLoc =  "NJ"
		cFMIWarehouse = ""
		cCustPrefix = "945"+"j"
		cDivision = "New Jersey"
*		cFolder = "WHN"
		cSF_Addr1  = "800 FEDERAL BLVD"
		cSF_CSZ    =  "CARTERET*NJ*07008"

	OTHERWISE
		cCustLoc =  "C2"
		cFMIWarehouse = ""
		cCustPrefix = "945"+"y"
		cDivision = "Carson2"
*		cFolder = "WHC"
		cSF_Addr1  = "1000 E 223RD ST"
		cSF_CSZ    = "CARSON*CA*90745"
	ENDCASE
	cCustFolder = UPPER(cCustName)  && +"-"+cCustLoc
	lIsError = .T.
	lNonEDI = .F.  && Assumes not a Non-EDI 945
	lDoPAL = .T.
	STORE "" TO lcKey
	STORE 0 TO alength,nLength

*!* SET OTHER CONSTANTS
	dt1 = TTOC(DATETIME(),1)
	dtmail = TTOC(DATETIME())
	dt2 = DATETIME()
	cString = ""

	csendqual = "ZZ"
	csendid = "FMIF"
	crecqual = "01"
	crecid = "654351030"

	cfd = CHR(0x07)
	csegd = "~"
	nSegCtr = 0
	cterminator = ">" && Used at end of ISA segment
	cdate = DTOS(DATE())
	ctruncdate = RIGHT(cdate,6)
	cTime = LEFT(TIME(DATE()),2)+SUBSTR(TIME(DATE()),4,2)
	cfiledate = cdate+cTime
	cOrig = "J"
	nSTCount = 0
	cStyle = ""
	cPTString = ""
	nTotCtnWt = 0
	nTotCtnCount = 0
	nUnitSum = 0
	cTargetStyle = ""

** PAD ID Codes
	csendidlong = PADR(csendid,15," ")
	crecidlong = PADR(crecid,15," ")

	lcPath = ("f:\ftpusers\"+cCustFolder+"\945OUT\")
	cFilenameHold = ("f:\ftpusers\"+cCustFolder+"\945-Staging\"+cCustPrefix+dt1+".txt")
	cFilenameShort = JUSTFNAME(cFilenameHold)
	cFilenameArch = UPPER("f:\ftpusers\"+cCustFolder+"\945OUT\945ARCHIVE\"+cCustPrefix+dt1+".txt")
	cFilenameOut = (lcPath+cFilenameShort)
	IF lTesting
		cFilenameOut = (cFilenameOut+"test\")
	ENDIF
	nFilenum = FCREATE(cFilenameHold)

	IF !USED('OUTSHIP')
		xsqlexec("select * from outship where accountid = 4694 and bol_no = '"+cBOL+"'",,,"wh")
		INDEX ON bol_no TAG bol_no
		INDEX ON outshipid TAG outshipid
		INDEX ON wo_num TAG wo_num
	ENDIF

	IF !USED('OUTDET')
		selectoutdet()
		SELECT outdet
		INDEX ON outdetid TAG outdetid
	ENDIF

	IF !USED('parcel_carriers')
		USE F:\3pl\DATA\parcel_carriers IN 0 ORDER TAG SCAC ALIAS parcel_carriers
	ENDIF

&& Added code blocks to the following to overcome BOL vs. WO selection issues, 12.23.2005
	SELECT outship
	IF SEEK(TRIM(outship.SCAC),"parcel_carriers","scac")
		lParcelType = .T.
	ENDIF

	IF cBOL = '9999'
		lOverflow = .T.
		SET DELETED OFF
	ENDIF

	IF USED("sqlwonanjing")
		USE IN sqlwonanjing
	ENDIF
	DELETE FILE "F:\3pl\DATA\sqlwonanjing.dbf"
	SET STEP ON

	IF !lParcelType
		SELECT bol_no,wo_num ;
			FROM outship WHERE bol_no = cBOL ;
			GROUP BY 1,2 ;
			ORDER BY 1,2 ;
			INTO DBF F:\3pl\DATA\sqlwonanjing
		USE IN sqlwonanjing
		USE F:\3pl\DATA\sqlwonanjing IN 0 ALIAS sqlwonanjing
	ENDIF
	IF DATETIME()<DATETIME(2017,08,07,14,30,00)
*!*		SELECT sqlwonanjing
*!*		BROWSE TIMEOUT 30
	ENDIF

	cRetMsg = "X"
	ASSERT .F. MESSAGE "At SQLCONNECT"
	DO m:\dev\prg\sqlconnectnanjing_bol  WITH nAcctNum,cCustName,cPPName,.T.,cOffice && Amended for UCC number sequencing

	IF cRetMsg<>"OK"
		cErrMsg = cRetMsg
		DO ediupdate WITH cErrMsg,.T.
		THROW
	ENDIF

*!* Added this code block to remove zero-qty cartons from SQL cursor
	SELECT ucc,SUM(totqty) AS uccqty FROM vnanjingpp GROUP BY 1 INTO CURSOR tempchk HAVING uccqty=0
	SELECT tempchk
	LOCATE
	IF !EOF()
		SCAN
			STORE ALLT(ucc) TO cUCC
			SELECT vnanjingpp
			LOCATE
			DELETE FOR ucc = cUCC
		ENDSCAN
	ENDIF
	USE IN tempchk

	IF DATE() > {^2012-11-03}
		CREATE CURSOR tempqtyerr (ship_ref c(20),ctnqty i,totqty i,cqty i,uqty i)

		SELECT ship_ref,outshipid,ctnqty,IIF("PREPACK-REP"$outship.shipins,.T.,.F.) AS PRR FROM outship WHERE bol_no = cBOL INTO CURSOR out1
		SELECT a.*,INT(VAL(b.PACK)) AS npack,b.totqty AS rawqty,b.origqty,0 AS totqty2 ;
			FROM out1 a LEFT JOIN outdet b ;
			ON a.outshipid = b.outshipid ;
			INTO CURSOR out2

		SELECT ship_ref,ctnqty,SUM(rawqty) AS qty ;
			FROM out2 ;
			GROUP BY 1 ;
			INTO CURSOR temptotals

		SELECT temptotals
		SCAN
			cSRUse = ALLTRIM(temptotals.ship_ref)
			SELECT vnanjingpp
			SUM totqty TO m.uqty FOR vnanjingpp.ship_ref = cSRUse
			SELECT 0
			SELECT DISTINCT(ucc) FROM vnanjingpp WHERE vnanjingpp.ship_ref = cSRUse AND vnanjingpp.totqty > 0 INTO CURSOR tempxyz
			m.uqty = RECCOUNT()
			m.cqty = RECCOUNT()
			USE IN tempxyz
			IF (uqty # temptotals.qty) OR (cqty # temptotals.ctnqty)
				INSERT INTO tempqtyerr (ship_ref,ctnqty,totqty,cqty,uqty) VALUES (cSRUse,temptotals.ctnqty,temptotals.ctnqty,m.cqty,m.uqty)
			ENDIF
		ENDSCAN

		SELECT tempqtyerr
		LOCATE
		IF !EOF()
			IF lTesting
*			BROWSE
			ENDIF
			totqtyerr()
			cErrMsg = "OD/CZN QTY DISCREP."
			DO ediupdate WITH cErrMsg,.T.
			THROW
		ENDIF
	ENDIF

	SELECT vnanjingpp
	SET RELATION TO outdetid INTO outdet

	IF !USED("scacs")
		xsqlexec("select * from scac","scacs",,"wh")
		INDEX ON SCAC TAG SCAC
	ENDIF

	SELECT outship
	LOCATE
	WAIT CLEAR
	WAIT WINDOW "Now creating Header information" NOWAIT NOCLEAR

*!* HEADER LEVEL EDI DATA
	DO num_incr_isa

	cISA_Num = PADL(c_CntrlNum,9,"0")
	nISA_Num = INT(VAL(cISA_Num))

	STORE "ISA"+cfd+"00"+cfd+"          "+cfd+"00"+cfd+"          "+cfd+csendqual+cfd+csendidlong+cfd+;
		crecqual+cfd+crecidlong+cfd+ctruncdate+cfd+cTime+cfd+"U"+cfd+"00401"+cfd+;
		cISA_Num+cfd+"0"+cfd+"P"+cfd+cterminator+csegd TO cString
	DO cstringbreak

	STORE "GS"+cfd+"SW"+cfd+csendid+cfd+crecid+cfd+cdate+cfd+cTime+cfd+c_CntrlNum+;
		cfd+"X"+cfd+cX12+csegd TO cString
	DO cstringbreak

	SELECT outship
	LOCATE

	IF !SEEK(nWO_Num,"outship","wo_num")
		WAIT CLEAR
		WAIT WINDOW "Invalid Work Order Number - Not Found in OUTSHIP!" TIMEOUT 2
*	DO ediupdate WITH "WO# NOT FOUND",.T.
		=FCLOSE(nFilenum)
		ERASE &cFilenameHold
		THROW
	ENDIF

	LOCATE
	IF !lTesting
		IF EMPTY(cBOL)  && May be empty if triggered from PT screen, or FedEx shipment
			WAIT CLEAR
			WAIT WINDOW "MISSING Bill of Lading Number!" TIMEOUT 2
			cErrMsg = "BOL# EMPTY"
			DO ediupdate WITH cErrMsg,.T.
			=FCLOSE(nFilenum)
			ERASE &cFilenameHold
			THROW
		ELSE
			IF !SEEK(PADR(TRIM(cBOL),20),"outship","bol_no")
				WAIT CLEAR
				WAIT WINDOW "Invalid Bill of Lading Number - Not Found!" TIMEOUT 2
				cErrMsg = "BOL# NOT FOUND"
				DO ediupdate WITH cErrMsg,.T.
				=FCLOSE(nFilenum)
				ERASE &cFilenameHold
				THROW
			ENDIF
		ENDIF

		IF EMPTYnul(outship.del_date)
			cErrMsg = "EMPTY DEL_DATE"
			DO ediupdate WITH cErrMsg,.T.
			=FCLOSE(nFilenum)
			ERASE &cFilenameHold
			THROW
		ENDIF

	ENDIF


*************************************************************************
*1	PTIDSOURCE/OUTSHIP RECORD MAIN LOOP
*	Loops through individual OUTSHIP lines for BOL#
*************************************************************************

	WAIT CLEAR

	STORE "" TO cWO_NumStr
	STORE "XXX" TO cWO_NumOld
	SELECT 0
	SELECT DISTINCT wo_num FROM outship WHERE bol_no = cBOL INTO CURSOR tempwofile
	LOCATE
	SCAN
		cWO_Num = ALLTRIM(STR(outship.wo_num))
		IF cWO_Num <> cWO_NumOld
			STORE  cWO_Num TO cWO_NumOld
			cWO_NumStr = IIF(EMPTY(cWO_NumStr),cWO_Num,cWO_NumStr+", "+cWO_Num)
			cWO_NumList = IIF(EMPTY(cWO_NumList),cWO_Num,cWO_NumList+CHR(13)+cWO_Num)
		ENDIF
	ENDSCAN
	USE IN tempwofile

	SELECT outship
	SET ORDER TO
	LOCATE && in OUTSHIP
	WAIT WINDOW "Now creating BOL#-based information..." NOWAIT NOCLEAR
	IF !lPTFlag
		SCANSTR = "outship.BOL_NO = cBOL"
	ELSE
		SCANSTR = "outship.ship_ref = cShip_ref AND accountid = nAcctNum"
	ENDIF
	SCAN FOR &SCANSTR
		nWO_Num = outship.wo_num
		cWO_Num = TRANSFORM(nWO_Num)
		cPadWO_Num = PADR(cWO_Num,10)
*		WAIT WINDOW "" TIMEOUT 2
		IF lParcelType
			cBOL2 = ALLTRIM(outship.bol_no)
			IF SEEK(cBOL2,'edi_trigger','bol')
				IF edi_trigger.wo_num = outship.wo_num AND edi_trigger.processed
					LOOP
				ENDIF
			ENDIF
		ENDIF

		lJCPenney = IIF("PENNEY"$outship.consignee,.T.,.F.)
		SCATTER MEMVAR MEMO
		IF (("OUTLET"$outship.consignee) OR ("CATALOG"$outship.consignee))
			lOutlet = .T.
		ENDIF

		IF lParcelType
			cBOL = TRIM(outship.bol_no)
		ELSE
			nWO_Num = outship.wo_num
		ENDIF

		nOSQty = m.qty
		IF nOSQty = 0
			LOOP
		ENDIF
		alength = ALINES(apt,outship.shipins,.T.,CHR(13))
		nTotCtnCount = m.qty
		cPO_Num = ALLTRIM(m.cnee_ref)

		IF !lPTFlag
			cShip_ref = ALLTRIM(m.ship_ref)
		ENDIF

		IF " OV"$cShip_ref
			LOOP
		ENDIF

		IF "!!"$cShip_ref
			cBaseShip_ref = LEFT(cShip_ref,AT("!",cShip_ref,1)-1)
			cShip_ref2 = LEFT(cShip_ref,AT("!",cShip_ref,1)-4)
			nSuffix = INT(VAL(RIGHT(cBaseShip_ref,3)))
			DO CASE
			CASE ("!!D"$cShip_ref)
				cSuffix = PADL(ALLTRIM(STR(nSuffix+4)),3,'0')
			CASE ("!!C"$cShip_ref)
				cSuffix = PADL(ALLTRIM(STR(nSuffix+3)),3,'0')
			CASE ("!!B"$cShip_ref)
				cSuffix = PADL(ALLTRIM(STR(nSuffix+2)),3,'0')
			OTHERWISE
				cSuffix = PADL(ALLTRIM(STR(nSuffix+1)),3,'0')
			ENDCASE
			cShip_refedi = cShip_ref2+cSuffix
		ELSE
			cShip_refedi = cShip_ref
		ENDIF

		nOutshipid = m.outshipid
		IF lParcelType
			cPTString = IIF(EMPTY(cPTString),m.consignee+" "+cShip_ref+" "+cBOL,;
				cPTString+CHR(10)+m.consignee+" "+cShip_ref+" "+cBOL)
		ELSE
			cPTString = IIF(EMPTY(cPTString),m.consignee+" "+cShip_ref,cPTString+CHR(10)+m.consignee+" "+cShip_ref)
		ENDIF
		m.CSZ = TRIM(m.CSZ)

		IF EMPTY(M.CSZ)
			WAIT CLEAR
			WAIT WINDOW "No City/State/ZIP info...exiting" TIMEOUT 2
			cErrMsg = "BAD ADDRESS INFO"
			DO ediupdate WITH cErrMsg,.T.
			THROW
		ENDIF

		cCity = segmentget(@apt,"CITY",alength)
		cState = segmentget(@apt,"STATE",alength)
		cZip = segmentget(@apt,"ZIPCODE",alength)

		IF EMPTY(cCity) OR EMPTY(cState) OR EMPTY(cZip)
			m.CSZ = ALLTRIM(STRTRAN(m.CSZ,"  "," "))
			nSpaces = OCCURS(" ",ALLTRIM(m.CSZ))
			nCommaPos = AT(",",m.CSZ)
			nLastSpace = AT(" ",m.CSZ,nSpaces)

			IF ISALPHA(SUBSTR(TRIM(m.CSZ),AT(" ",m.CSZ,nSpaces-1)+1,2))
				cCity = LEFT(TRIM(m.CSZ),AT(",",m.CSZ)-1)
				cState = SUBSTR(m.CSZ,nCommaPos+2,2)
				cZip = TRIM(SUBSTR(m.CSZ,nLastSpace+1))
			ELSE
				WAIT CLEAR
				WAIT WINDOW "NOT ALPHA: "+SUBSTR(TRIM(m.CSZ),AT(" ",m.CSZ,nSpaces-1)+1,2) TIMEOUT 3
				cCity = LEFT(TRIM(m.CSZ),AT(",",m.CSZ)-1)
				cState = SUBSTR(TRIM(m.CSZ),AT(" ",m.CSZ,nSpaces-2)+1,2)
				cZip = TRIM(SUBSTR(m.CSZ,nLastSpace+1))
			ENDIF
		ENDIF

		nCtnNumber = 1  && Begin sequence count

		DO num_incr_st
		WAIT CLEAR
		WAIT WINDOW "Now creating Line Item information" NOWAIT NOCLEAR

		IF !lTesting
			INSERT INTO temp945 (accountid,isa_num,st_num,wo_num,bol_no,ship_ref,filename,filedate) ;
				VALUES (m.accountid,c_CntrlNum,c_GrpCntrlNum,m.wo_num,m.bol_no,m.ship_ref,cFilenameShort,dt2)
		ENDIF

		STORE "ST"+cfd+"945"+cfd+PADL(c_GrpCntrlNum,9,"0")+csegd TO cString
		DO cstringbreak
		nSTCount = nSTCount + 1
		nSegCtr = 1

		cShipmentID = RIGHT(TRIM(cBOL),10)
		IF lParcelType
			STORE "W06"+cfd+"N"+cfd+cShip_refedi+cfd+cdate+cfd+"0"+cfd+TRIM(cWO_Num)+cfd+cPO_Num+csegd TO cString
		ELSE
			STORE "W06"+cfd+"N"+cfd+cShip_refedi+cfd+cdate+cfd+TRIM(cShipmentID)+cfd+TRIM(cWO_Num)+cfd+cPO_Num+csegd TO cString
		ENDIF
		DO cstringbreak
		nSegCtr = nSegCtr + 1

		cStoreNum = segmentget(@apt,"STORENUM",alength)
		IF EMPTY(cStoreNum)
			cStoreNum = ALLTRIM(m.dcnum)
		ENDIF
		STORE "N1"+cfd+"ST"+cfd+ALLTRIM(m.consignee)+cfd+"92"+cfd+cStoreNum+csegd TO cString
		DO cstringbreak
		nSegCtr = nSegCtr + 1

		STORE "N1"+cfd+"Z7"+cfd+ALLTRIM(m.shipfor)+cfd+"92"+cfd+ALLTRIM(m.sforstore)+csegd TO cString
		DO cstringbreak
		nSegCtr = nSegCtr + 1

		IF lParcelType
			STORE "N9"+cfd+"BM"+cfd+TRIM(cShipmentID)+csegd TO cString
		ELSE
			STORE "N9"+cfd+"BM"+cfd+TRIM(cBOL)+csegd TO cString
		ENDIF
		DO cstringbreak
		nSegCtr = nSegCtr + 1

		STORE "N9"+cfd+"DP"+cfd+TRIM(outship.dept)+csegd TO cString
		DO cstringbreak
		nSegCtr = nSegCtr + 1

		cInvoice = IIF(EMPTY(ALLTRIM(m.keyrec)),ALLTRIM(m.ship_ref),ALLTRIM(m.keyrec))
		STORE "N9"+cfd+"CN"+cfd+TRIM(cInvoice)+csegd TO cString
		DO cstringbreak
		nSegCtr = nSegCtr + 1

		STORE "N9"+cfd+"IA"+cfd+TRIM(outship.vendor_num)+csegd TO cString
		DO cstringbreak
		nSegCtr = nSegCtr + 1

		IF lTesting
			ddel_date = DATE()
			dapptnum = "99999"
			dapptdate = DATE()
		ELSE
			ddel_date = outship.del_date
			IF EMPTY(ddel_date)
				cErrMsg = "MISSING DELDATE"
				DO ediupdate WITH cErrMsg,.T.
				THROW
			ENDIF

			dapptnum = IIF(EMPTY(outship.appt_num),"UNK",TRIM(outship.appt_num))
			dapptdate = outship.appt
			IF EMPTY(dapptdate)
				dapptdate = outship.del_date
			ENDIF
		ENDIF
		STORE "N9"+cfd+"AO"+cfd+dapptnum+csegd TO cString
		DO cstringbreak
		nSegCtr = nSegCtr + 1

		cUseTime = "15300000"
*!*			STORE "G62"+cfd+"70"+cfd+DTOS(dapptdate)+cfd+"3"+cfd+cTime+csegd TO cString  && Ship date
*!*			DO cstringbreak
*!*			nSegCtr = nSegCtr + 1

		STORE "G62"+cfd+"11"+cfd+DTOS(ddel_date)+cfd+"9"+cfd+cUseTime+csegd TO cString  && Ship date
		DO cstringbreak
		nSegCtr = nSegCtr + 1

		SELECT scacs
		IF lTesting
			STORE "TEST" TO m.SCAC
		ELSE
			IF !EMPTY(TRIM(outship.SCAC))
				STORE outship.SCAC TO m.SCAC
			ELSE
				IF UPPER(LEFT(outship.SHIP_VIA,3)) = "UPS"
					REPLACE outship.SCAC WITH "UPSN" IN outship
				ELSE
					cErrMsg = "MISSING SCAC"
					DO ediupdate WITH cErrMsg,.T.
					THROW
				ENDIF
			ENDIF
		ENDIF

		SELECT outship

		cShipType = IIF(lParcelType,"U","J")
		STORE "W27"+cfd+cShipType+cfd+TRIM(m.SCAC)+cfd+TRIM(m.SHIP_VIA)+csegd TO cString
		DO cstringbreak
		nSegCtr = nSegCtr + 1

*************************************************************************
*2	DEtAIL LOOP - MATCHING SHIP_REF FIELD (FROM OUTSHIP)
*************************************************************************
		WAIT CLEAR

		SELECT vnanjingpp

		WAIT WINDOW "Now creating Detail information "+cShip_ref NOWAIT NOCLEAR

*		IF INLIST(cBOL,"04907314694021541","04907314694021503","04907314694021527","04907314694021534")
		COUNT TO nPPQty FOR ship_ref = TRIM(cShip_ref) AND outshipid = nOutshipid
*		ELSE
*			SUM totqty TO nPPQty FOR ship_ref = TRIM(cShip_ref) AND outshipid = nOutshipid
*		ENDIF

		ASSERT .F. MESSAGE "At QTYCompare insertion...debug"

		INSERT INTO qtycompare (ship_ref,osqty,ppqty) VALUES (cShip_ref,nOSQty,nPPQty)
		SELECT qtycompare
		GO BOTT
		nPctdiff = INT(ABS((osqty-ppqty)/osqty)*100)
		REPLACE pctdiff WITH nPctdiff
		STORE 0 TO nOSQty,nPPQty,nPctdiff
		SELECT vnanjingpp
		LOCATE
		LOCATE FOR vnanjingpp.ship_ref = TRIM(cShip_ref) AND vnanjingpp.outshipid = nOutshipid
		IF !FOUND()
			WAIT WINDOW "PT "+cShip_ref+" NOT FOUND in VNANJINGPP...ABORTING" TIMEOUT 2
			cErrMsg = "MISS PT: "+cShip_ref
			DO ediupdate WITH cErrMsg,.T.
			THROW
		ENDIF

*!* Added this code to trap unmatched OUTDETIDs in OUTDET, Joe 11.22.2005
		SCAN FOR ship_ref = TRIM(cShip_ref) AND outshipid = nOutshipid
			IF EMPTY(outdet.outdetid)
				SET STEP ON
				WAIT WINDOW "OUTDETID "+TRANSFORM(vnanjingpp.outdetid)+" NOT FOUND IN OUTDET!...CLOSING" TIMEOUT 3
				cErrMsg = "MISS OD-ID: "+TRANSFORM(vnanjingpp.outdetid)
				DO ediupdate WITH cErrMsg,.T.
				THROW
			ENDIF
		ENDSCAN

		SCANSTR = "vnanjingpp.ship_ref = cShip_ref and vnanjingpp.outshipid = nOutshipid"
		SELECT vnanjingpp
		LOCATE FOR &SCANSTR
		cUCC = "XXX"
*	SCAN FOR &scanstr
		DO WHILE &SCANSTR
			lSkipBack = .T.

			IF vnanjingpp.totqty = 0
				SKIP 1 IN vnanjingpp
				LOOP
			ENDIF

			IF vnanjingpp.ucc = 'CUTS'
				SKIP 1 IN vnanjingpp
				LOOP
			ENDIF

			IF TRIM(vnanjingpp.ucc) <> cUCC
				STORE TRIM(vnanjingpp.ucc) TO cUCC
			ENDIF

			STORE "LX"+cfd+ALLTRIM(STR(nCtnNumber))+csegd TO cString   && Carton Seq. # (up to Carton total)
			lDoManSegment = .T.
			DO cstringbreak
			nSegCtr = nSegCtr + 1
			DO WHILE vnanjingpp.ucc = cUCC
				IF lDoManSegment
					IF !EMPTY(cUCC)
						lDoManSegment = .F.
						STORE "MAN"+cfd+"GM"+cfd+TRIM(cUCC)+csegd TO cString  && UCC Number (Wal-mart DSDC only)
						DO cstringbreak
						nSegCtr = nSegCtr + 1
						nDetQty = INT(VAL(outdet.PACK))
*						nUnitSum = nUnitSum + 1
					ELSE
						WAIT CLEAR
						WAIT WINDOW "Empty UCC Number in vNanjingPP "+cShip_ref TIMEOUT 2
						IF lTesting
							CLOSE DATABASES ALL
							=FCLOSE(nFilenum)
							ERASE &cFilenameHold
							THROW
						ELSE
							cErrMsg = "EMPTY UCC# in "+cShip_ref
							DO ediupdate WITH cErrMsg,.T.
							=FCLOSE(nFilenum)
							ERASE &cFilenameHold
							THROW
						ENDIF
					ENDIF
				ENDIF

				cColor = ""
				IF "ORIG_COLOR*"$outdet.printstuff
					valreturn("orig_color")
					STORE cOrig_color TO cColor
				ELSE
					IF EMPTY(TRIM(cColor))
						cColor = TRIM(outdet.COLOR)
					ENDIF
				ENDIF

				cSize = ""
				IF "ORIG_ID*"$outdet.printstuff
					valreturn("orig_id")
					STORE cOrig_id TO cSize
				ELSE
					IF EMPTY(TRIM(cSize))
						STORE TRIM(outdet.ID) TO cSize
					ENDIF
				ENDIF

				cStyle = ""
				IF "ORIG_STYLE*"$outdet.printstuff
					valreturn("orig_style")
					STORE cOrig_style TO cStyle
				ELSE
					IF EMPTY(cStyle)
						cStyle = TRIM(outdet.STYLE)
					ENDIF
				ENDIF

				cUPC = TRIM(outdet.upc)
				cPrintstyle = TRIM(outdet.shipstyle)



				cItemNum = TRIM(outdet.custsku)
				IF "ADD*ONLY"$outdet.printstuff
					cItemNum = cItemNum+"*ONLY"
				ENDIF

				IF lOutlet
					cStyle = TRIM(outdet.custsku)
					cItemNum = ""
				ENDIF


				valreturn("OLDSTYLE")
				IF !EMPTY(cOldStyle)
					STORE TRIM(cOldStyle) TO cStyle
				ENDIF

				nDetQty = 1
				nOrigQty = 1

				IF lTesting AND EMPTY(outdet.ctnwt)
					cCtnWt = "2"
					nTotCtnWt = nTotCtnWt + 2
				ELSE
					STORE ALLTRIM(STR(outdet.ctnwt)) TO cCtnWt
					nTotCtnWt = nTotCtnWt + outdet.ctnwt
				ENDIF

				IF EMPTY(cCtnWt) OR outdet.ctnwt=0
					nCtnWt = outship.weight/outship.ctnqty
					nTotCtnWt = nTotCtnWt + nCtnWt
					cCtnWt = ALLTRIM(STR(nCtnWt))
					IF EMPTY(cCtnWt)
						cErrMsg = "MISS CTNWT "+TRANSFORM(vnanjingpp.outdetid)
						DO ediupdate WITH cErrMsg,.T.
						=FCLOSE(nFilenum)
						ERASE &cFilenameHold
						THROW
					ENDIF
				ENDIF

				valreturn("W0104")
				valreturn("W0106")
				valreturn("W0115")
				valreturn("UNITSTYPE")

				IF EMPTY(cUnitstype)
					cUnitstype = "AS"
				ENDIF

				IF EMPTY(cW0104)
					cW0104 = "IN"
				ENDIF

				IF EMPTY(cW0106)
					cW0106 = "UP"
				ENDIF

				IF EMPTY(cW0115)
					cW0115 = "VN"
				ENDIF

				IF nOrigQty=nDetQty
					nShipType = "CL"
				ELSE
					nShipType = "PR"
				ENDIF

				IF nDetQty>0
					nUnitSum = nUnitSum + nDetQty
					STORE "W12"+cfd+"CL"+cfd+ALLTRIM(STR(nOrigQty))+cfd+ALLTRIM(STR(nDetQty))+cfd+;
						ALLTRIM(STR(nOrigQty-nDetQty))+cfd+cUnitstype+cfd+cUPC+cfd+cW0104+cfd+TRIM(cItemNum)+;
						cfd+cfd+cCtnWt+cfd+cWeightUnit+REPLICATE(cfd,6)+cW0106+cfd+cUPC+;
						REPLICATE(cfd,3)+cW0115+cfd+cStyle+csegd TO cString  && Eaches/Style
					DO cstringbreak
					nSegCtr = nSegCtr + 1

					IF !EMPTY(cColor)
						STORE "N9"+cfd+"VCL"+cfd+cColor+csegd TO cString
						DO cstringbreak
						nSegCtr = nSegCtr + 1
					ENDIF

					IF !EMPTY(cSize)
						STORE "N9"+cfd+"VSZ"+cfd+cSize+csegd TO cString
						DO cstringbreak
						nSegCtr = nSegCtr + 1
					ENDIF
				ENDIF
				SKIP 1 IN vnanjingpp
			ENDDO
			lSkipBack = .T.
			nCtnNumber = nCtnNumber + 1
		ENDDO

*************************************************************************
*2^	END DETAIL MATCHING SHIP_REF LOOP
*************************************************************************
		WAIT CLEAR
		WAIT WINDOW "Now creating Section Closure information" NOWAIT NOCLEAR
		STORE "W03"+cfd+ALLTRIM(STR(nUnitSum))+cfd+ALLTRIM(STR(nTotCtnWt))+cfd+;
			cWeightUnit+csegd TO cString   && Units sum, Weight sum, carton count

		nTotCtnWt = 0
		nTotCtnCount = 0
		nUnitSum = 0
		FPUTS(nFilenum,cString)

		STORE  "SE"+cfd+ALLTRIM(STR(nSegCtr+2))+cfd+PADL(c_GrpCntrlNum,9,"0")+csegd TO cString
		FPUTS(nFilenum,cString)

		SELECT outship
		WAIT CLEAR
	ENDSCAN

*************************************************************************
*1^	END OUTSHIP MAIN LOOP
*************************************************************************
	DO close945
	=FCLOSE(nFilenum)

	IF cBOL # "04907314694117794"
		SELECT qtycompare
		LOCATE
		IF !EOF()
			SUM qtycompare.osqty TO sum_os
			SUM qtycompare.ppqty TO sum_pp
			IF sum_pp <> sum_os
				lMismatch = .T.
				lEmail = .F.
				lNanFilesOut = .F.
				countmail()
				cErrMsg = "COUNT ERR "+cBOL
				DO ediupdate WITH cErrMsg,.T.
				=FCLOSE(nFilenum)
				ERASE &cFilenameHold
				THROW
			ENDIF
		ENDIF
	ELSE
		lMismatch = .F.
	ENDIF

	IF !lTesting
		SELECT edi_trigger
		lIsError = .F.
		IF lMismatch
			DO ediupdate WITH "945 COUNT OFF",.T.
		ELSE
			DO ediupdate WITH "945 CREATED",.F.
		ENDIF
		SELECT edi_trigger
		LOCATE

		IF !USED("ftpedilog")
			SELECT 0
			USE F:\edirouting\ftpedilog ALIAS ftpedilog
			INSERT INTO ftpedilog (TRANSFER,ftpdate,filename,acct_name,TYPE) VALUES ("945-"+cCustName+"-"+cCustLoc,dt2,cFilenameHold,UPPER(cCustName),"945")
			USE IN ftpedilog
		ENDIF
	ENDIF

	WAIT CLEAR
	WAIT WINDOW cCustFolder+" 945 Process complete..." TIMEOUT 1

*!* Create eMail confirmation message
	IF lTestMail
		tsendto = tsendtotest
		tcc = tcctest
	ENDIF

	tsubject = cMailName+" 945 EDI File from TGF as of "+dtmail+" for JCP ("+cCustLoc+")"
	tattach = " "
	IF lParcelType
		tmessage = "945 JCP EDI Info from TGF for "+cDivision+", (WO# "+cWO_NumStr+"),"+CHR(10)
		tmessage = tmessage + "containing these picktickets:"+CHR(10)
	ELSE
		tmessage = "945 JCP EDI Info from TGF for "+cDivision+", BOL# "+TRIM(cBOL)+CHR(10)
		tmessage = tmessage + "(WO# "+cWO_NumStr+"), containing these picktickets:"+CHR(10)
	ENDIF
	tmessage = tmessage + cPTString + CHR(10)
	IF lMismatch
		tmessage = tmessage + "has a quantity mismatch and is being held pending verification." + CHR(10)
		tmessage = tmessage + "We will notify you of final status shortly."
	ELSE
		tmessage = tmessage + "has been created and will be transmitted ASAP."+CHR(10)+CHR(10)
		tmessage = tmessage + "If you have any questions, please eMail or call 732-750-9000 x217."
	ENDIF

	tmessage = tmessage+CHR(13)+"Run from prog: "+cProgName
	IF lEmail
		DO FORM m:\dev\frm\dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
	ENDIF

	RELEASE ALL LIKE c_CntrlNum,c_GrpCntrlNum
	WAIT CLEAR
	WAIT WINDOW cMailName+" 945 EDI File output complete"+CHR(13)+"for BOL# "+TRIM(cBOL) TIMEOUT 1

*!* Transfers files to correct output folders
	IF lNanFilesOut
		COPY FILE [&cFilenameHold] TO [&cFilenameOut]
		DELETE FILE &cFilenameHold
	ENDIF
	IF !lTesting
		SELECT temp945
		COPY TO "f:\3pl\data\temp945a.dbf"
		USE IN temp945
		SELECT 0
		USE "f:\3pl\data\temp945a.dbf" ALIAS temp945a
		SELECT 0
		USE "f:\3pl\data\pts_sent945.dbf" ALIAS pts_sent945
		APPEND FROM "f:\3pl\data\temp945a.dbf"
		USE IN pts_sent945
		USE IN temp945a
		DELETE FILE "f:\3pl\data\temp945a.dbf"
	ENDIF

	IF !lTesting
		asn_out_data()
	ENDIF

CATCH TO oErr
	IF lDoCatch
		SET STEP ON
		lEmail = .F.
		IF EMPTY(cErrMsg)
			DO ediupdate WITH ALLTRIM(oErr.MESSAGE),.T.
			tsubject = cCustName+" Error ("+TRANSFORM(oErr.ERRORNO)+") at "+TTOC(DATETIME())
			tattach  = ""
			tsendto  = IIF(lTesting,tsendtotest,tsendtoerr)
			tcc = IIF(lTesting,tcctest,tccerr)

			tmessage = cCustName+" Error processing "+CHR(13)

			tmessage =tmessage+ "Try/CATCH Exeception Message:"+CHR(13)+;
				[  Error: ] + STR(oErr.ERRORNO) +CHR(13)+;
				[  LineNo: ] + STR(oErr.LINENO) +CHR(13)+;
				[  Message: ] + oErr.MESSAGE +CHR(13)+;
				[  Procedure: ] + oErr.PROCEDURE +CHR(13)+;
				[  Details: ] + oErr.DETAILS +CHR(13)+;
				[  StackLevel: ] + STR(oErr.STACKLEVEL) +CHR(13)+;
				[  LineContents: ] + oErr.LINECONTENTS+CHR(13)+;
				[  UserValue: ] + oErr.USERVALUE

			tsubject = "945 EDI Poller Error at "+TTOC(DATETIME())
			tattach  = ""
			tcc=""
			tfrom = "Toll EDI 945 Poller Operations <toll-edi-ops@tollgroup.com>"
			DO FORM m:\dev\frm\dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
		ENDIF
	ENDIF
FINALLY
*	RELEASE ALL LIKE t*
ENDTRY


*** END OF CODE BODY

****************************
PROCEDURE close945
****************************
** Footer Creation

STORE  "GE"+cfd+ALLTRIM(STR(nSTCount))+cfd+c_CntrlNum+csegd TO cString
FPUTS(nFilenum,cString)

STORE  "IEA"+cfd+"1"+cfd+PADL(c_CntrlNum,9,"0")+csegd TO cString
FPUTS(nFilenum,cString)

ENDPROC

****************************
PROCEDURE num_incr_isa
****************************

IF !USED("serfile")
	USE ("F:\3pl\DATA\serial\"+cCustName+"945_serial") IN 0 ALIAS serfile
ENDIF
SELECT serfile
IF lTesting AND lISAFlag
	nOrigSeq = serfile.seqnum
	lISAFlag = .F.
ENDIF
nISA_Num = serfile.seqnum
c_CntrlNum = ALLTRIM(STR(serfile.seqnum))
REPLACE serfile.seqnum WITH serfile.seqnum + 1 IN serfile
SELECT outship
ENDPROC

****************************
PROCEDURE num_incr_st
****************************

IF !USED("serfile")
	USE ("f:\3pl\data\serial\"+cCustName+"945_serial") IN 0 ALIAS serfile
ENDIF
SELECT serfile
IF lTesting AND lSTFlag
	nOrigGrpSeq = serfile.grpseqnum
	lSTFlag = .F.
ENDIF
c_GrpCntrlNum = ALLTRIM(STR(serfile.grpseqnum))
REPLACE serfile.grpseqnum WITH serfile.grpseqnum + 1 IN serfile
SELECT outship
ENDPROC


****************************
PROCEDURE segmentget
****************************

PARAMETER thisarray,lcKey,nLength

FOR i = 1 TO nLength
	IF i > nLength
		EXIT
	ENDIF
	lnEnd= AT("*",thisarray[i])
	IF lnEnd > 0
		lcThisKey =TRIM(SUBSTR(thisarray[i],1,lnEnd-1))
		IF OCCURS(lcKey,lcThisKey)>0
			RETURN SUBSTR(thisarray[i],lnEnd+1)
			i = 1
		ENDIF
	ENDIF
ENDFOR

RETURN ""

****************************
PROCEDURE ediupdate
****************************
PARAMETER cStatus,lIsError
lDoCatch = .F.

IF !lTesting
	SELECT edi_trigger
	LOCATE
	IF !lIsError
		IF lPTFlag
			REPLACE processed WITH .T.,proc945 WITH .T.,file945 WITH cFilenameHold,;
				fin_status WITH cStatus,errorflag WITH .F.,when_proc WITH DATETIME() ;
				FOR edi_trigger.ship_ref = cShip_ref AND accountid = nAcctNum
		ELSE
			REPLACE processed WITH .T.,proc945 WITH .T.,file945 WITH cFilenameHold,;
				fin_status WITH cStatus,errorflag WITH .F.,when_proc WITH DATETIME() ;
				FOR edi_trigger.bol = cBOL AND edi_trigger.accountid = nAcctNum
		ENDIF
	ELSE
		IF lPTFlag
			REPLACE processed WITH .T.,proc945 WITH .F.,file945 WITH "",;
				fin_status WITH cStatus,errorflag WITH .T. ;
				FOR edi_trigger.ship_ref = cShip_ref AND accountid = nAcctNum
		ELSE
			REPLACE processed WITH .T.,proc945 WITH .F.,file945 WITH "",;
				fin_status WITH cStatus,errorflag WITH .T. ;
				FOR edi_trigger.bol = cBOL AND accountid = nAcctNum
		ENDIF
		IF lCloseOutput
			=FCLOSE(nFilenum)
			IF !lMismatch
				ERASE &cFilenameHold
			ENDIF
		ENDIF
	ENDIF
ENDIF

IF lIsError AND lEmail
	tsubject = "945 Error in NANJING/JCP BOL "+TRIM(cBOL)+" (At PT "+cShip_ref+")"
	tattach = " "
	tsendto = tsendtoerr
	tcc = tccerr
	tmessage = "Generated from program: nanjing945jcp_create, within EDI Outbound Poller"
	tmessage = tmessage + CHR(13) + "945 Processing for BOL# "+TRIM(cBOL)+"(WO# "+cWO_Num+"), produced this error: (Office: "+cOffice+"): "+cStatus+CHR(10)+"Check EDI_TRIGGER and re-run"

	IF cStatus = "MISS PT"
		tmessage = tmessage + CHR(13) + "This error means that this PT exists in OUTSHIP, but not in the Pickpack table. Please add this WO to SQL."
	ENDIF

	IF TIME(DATE())>"17:00:00" OR INLIST(DOW,DATE(),1,7)
*			tcc = tcc+",jsb1956@yahoo.com"
	ENDIF
	DO FORM m:\dev\frm\dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
ENDIF

IF USED('outship')
	USE IN outship
ENDIF
IF USED('outdet')
	USE IN outdet
ENDIF
SELECT serfile
IF lTesting
	REPLACE serfile.seqnum WITH nOrigSeq
	REPLACE serfile.grpseqnum WITH nOrigGrpSeq
ENDIF
USE IN serfile
IF USED('parcel_carriers')
	USE IN parcel_carriers
ENDIF
IF USED('scacs')
	USE IN scacs
ENDIF
IF USED('mm')
	USE IN mm
ENDIF

IF !lTesting
	SELECT edi_trigger
	LOCATE
ENDIF
ENDPROC

****************************
PROCEDURE cstringbreak
****************************
cLen = LEN(ALLTRIM(cString))

FPUTS(nFilenum,cString)
ENDPROC

****************************
PROCEDURE valreturn
****************************
PARAMETERS cIdentifier
cReturned = "c"+cIdentifier
RELEASE ALL LIKE &cReturned
PUBLIC &cReturned
nGetline = ATCLINE(cIdentifier,outdet.printstuff)
dataline = MLINE(outdet.printstuff,nGetline)
STORE SUBSTR(dataline,AT("*",dataline)+1) TO &cReturned
RETURN &cReturned
ENDPROC

****************************
PROCEDURE countmail
****************************
SELECT qtycompare
LOCATE
tmessage = "SCAN PACK REPORT NOT RUN!"
cString = PADR("WO #",10)+PADR("PICKTICKET",15)+PADR("O/S QTY",10)+PADR("P/P QTY",10)+PADR("QTY DIFF",10)+PADR("PCT DIFF",10)
tmessage = tmessage + CHR(10) + cString
IF !EOF()
	ASSERT .F. MESSAGE "In QTY Compare error"
	SCAN
		cShip_ref = PADR(qtycompare.ship_ref,15)
		cOSQty =  PADR(ALLTRIM(STR(qtycompare.osqty)) ,10)
		cPPqty = PADR(ALLTRIM(STR(qtycompare.ppqty)) ,10)
		cQtyDiff = PADR(ALLTRIM(STR(qtycompare.ppqty-qtycompare.osqty)) ,10)
		cPctDiff = PADR(ALLTRIM(STR(qtycompare.pctdiff)) ,10)
		IF cOSQty<>cPPqty
			tmessage = tmessage + CHR(10) + cPadWO_Num + cShip_ref + cOSQty + cPPqty + cQtyDiff + cPctDiff
		ENDIF
	ENDSCAN

	tsubject = "945 Count for NANJING/EA DSDC BOL "+TRIM(cBOL)
	tattach = " "
	tsendto = tsendtoerr
	tcc = tccerr

	DO FORM m:\dev\frm\dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
ENDIF
ENDPROC

****************************
PROCEDURE totqtyerr
****************************
SET STEP ON
tsubject = "Cartons/Units Discrep., Outbound vs. Cartons Data (JCP)"
tmessage = "BOL# "+cBOL+", WO#(s) "+cWO_NumStr+CHR(13)
tmessage = tmessage+"The following PT(s) had unmatched counts between the outbound ('O') and cartonization ('C') data:"+CHR(13)+CHR(13)
tmessage = tmessage+PADR("PT#",20)+PADR("O.Ctns",10)+PADR("C.Ctns",10)+PADR("O.Units",10)+PADR("C.Units",10)+CHR(13)
SELECT tempqtyerr
SCAN
	cSRUse = ALLTRIM(tempqtyerr.ship_ref)
	cnctnqty = STR(tempqtyerr.ctnqty)
	cncqty = STR(tempqtyerr.cqty)
	cntotqty = STR(tempqtyerr.totqty)
	cnuqty = STR(tempqtyerr.uqty)
	DO CASE
	CASE (ctnqty # cqty) AND (totqty # uqty)
		cErrorLine = "Both Carton and Unit totals don't match"
	CASE (ctnqty # cqty)
		cErrorLine = "Only Carton totals don't match"
	CASE (totqty # uqty)
		cErrorLine = "Only Unit totals don't match"
	ENDCASE
	tmessage = tmessage+PADR(cSRUse,20)+PADR(cnctnqty,10)+PADR(cncqty,10)+PADR(cntotqty,10)+PADR(cnuqty,10)+" "+cErrorLine+CHR(13)
ENDSCAN

tmessage = tmessage+CHR(13)+"Please contact Joe IMMEDIATELY to confirm which numbers are correct."
tsendto = IIF(lTesting,tsendtotest,tsendtoqty)
tcc = IIF(lTesting,tcctest,tccqty)
tattach = ""
DO FORM m:\dev\frm\dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
USE IN tempqtyerr
ENDPROC
