*!* BIOWORLD 945 PickPack (Whse. Shipping Advice) Creation Program
*!* Creation DatcStoreName = segmentget(@apt,"STORENAME",alength)e: 10.15.2012 by Joe, modded by Paul (from Nanjing 945PP prog)

PARAMETERS cBOL,cOffice,nAcctNum

** try to define all variables that may be included in the "Catch Email in case of an error
** that is not the result of a deliberate Throw.......

PUBLIC ARRAY thisarray(1)
PUBLIC nWO_num,cWO_Num,cWO_NumStr,cWO_NumOld,cShip_ref,cCustname,nUnitSum,lDoSQLConnect,cErrMsg,lTesting
PUBLIC lBioFilesout,lTestmail,lTestinput,lDoCompare,cTimeMsg,cBOLTest,tfrom,lW06,lHotTopic,lOverflow
PUBLIC cMod,cMBOL,cFilenameShort,cFilenameOut,cFilenameArch,lParcelType,lParcelSCAC,cWO_NumList,lcPath,cEDIType,cISA_Num

lTesting = .F.
lTestinput = .F.
lDoSQLConnect = .T.
lDoCompare = !lTesting
lDoCompare = .F.
lPGTesting = .F.

cErrMsg ="TOP LEVEL"
cTimeMsg = IIF(lTesting,"TIMEOUT 2","NOWAIT")
cCustname ="BIOWORLD"
lDoCatch = .T.
tsendtotest=""
tsendtoerr=""
tcctest=""
cISA_Num = "0"
DO m:\dev\prg\_setvars WITH lTesting

ON ESCAPE CANCEL

TRY
	STORE 0 TO  nFilenum,nSCCSuffix,nOrigSeq,nOrigGrpSeq
	STORE "" TO cWO_NumStr,cWO_NumList,cWO_Num,cShip_ref
	STORE "XXX" TO cWO_NumOld

	lIsError = .F.
	lDoCatch = .T.
	lCloseOutput = .T.
	lW06 = .F. && Added this variable to prevent blank 945 output files

	cPPName = "Bioworld"
	lISAFlag = .T.
	lSTFlag = .T.
	lJCPenney = .F.
	lCloseOutput = .T.
	lParcelType = .F.
	tfrom = "Toll EDI Operations <toll-edi-ops@tollgroup.com>"
	lOverflow = .F.

	IF TYPE("cOffice") = "L"
		cOffice = "C"
	ENDIF

	useca("ackdata","wh")

	IF lTesting
		CLOSE DATABASES ALL
		DO m:\dev\prg\lookups
		nAcctNum = 6182
		cBOL = "04907316182074374"
		cBOLTest = cBOL
	ENDIF
	IF EMPTY(cBOL) AND lTesting
		WAIT WINDOW "Empty BOL#" TIMEOUT 2
	ENDIF
	cMod = IIF(cOffice = "C","5",cOffice)
	cEDIType = "945"
	cMBOL = ""
	gOffice = cMod
	m.mod = cMod
	gMasterOffice = IIF(cOffice = "I","N",cOffice)
	m.office = cOffice
	lLoadSQLBL = .T.

	cBOL = ALLTRIM(cBOL)
*!* SET CUSTOMER CONSTANTS
	cCustname = "BIOWORLD"  && Customer Identifier
	cX12 = "004010"  && X12 Standards Set used
	cMailName = IIF(nAcctNum=6554,"Nolan","Bioworld")

	SELECT 0
	CREATE CURSOR temp945 (accountid i,isa_num c(10),st_num c(10),wo_num i,bol_no c(20),;
		ship_ref c(20),filename c(50),filedate T)
	SELECT 0

	lEmail = .T.
	lTestmail = lTesting && Sends mail to Joe only
	lBioFilesout = !lTesting && If true, copies output to FTP folder (default = .t.)

*!*	lBioFilesout = .F.
*!*	lTestMail = .T.

	STORE "L" TO cWeightUnit
	lPrepack = .F.
	lPick = !lPrepack
	xReturn = "XXX"

	DO m:\dev\prg\swc_cutctns WITH cBOL

	PUBLIC c_CntrlNum,c_GrpCntrlNum,cReturned
	WAIT WINDOW "Now preparing tables and variables" NOWAIT NOCLEAR

*!* SET OTHER CONSTANTS
	DO CASE
		CASE cOffice = "M"
			cCustLoc =  "FL"
			cFMIWarehouse = ""
			cCustPrefix = "945bw"+"f"
			cDivision = "Florida"
			cSF_Addr1  = "11400 NW 32ND AVE"
			cSF_CSZ    = "MIAMI*FL*33167"

		CASE cOffice = "L"
			cCustLoc =  "ML"
			cFMIWarehouse = ""
			cCustPrefix = "945bw"+"l"
			cDivision = "Mira Loma"
			cSF_Addr1  = "3355 DULLES DR"
			cSF_CSZ    = "MIRA LOMA*CA*91752"

		CASE INLIST(cOffice,"N","I")
			cCustLoc =  "NJ"
			cFMIWarehouse = ""
			cCustPrefix = "945bw"+"j"
			cDivision = "New Jersey"
			cSF_Addr1  = "800 FEDERAL BLVD"
			cSF_CSZ    =  "CARTERET*NJ*07008"

		OTHERWISE
			cCustLoc =  "CA"
			cFMIWarehouse = ""
			cCustPrefix = "945bw"+"c"
			cDivision = "California"
			cSF_Addr1  = "450 WESTMONT DRIVE"
			cSF_CSZ    = "SAN PEDRO*CA*90731"
	ENDCASE

	dt1 = TTOC(DATETIME(),1)
	dtmail = TTOC(DATETIME())
	dt2 = DATETIME()
	cString = ""

	IF !USED("mm")
		USE F:\3pl\DATA\mailmaster ALIAS mm IN 0
	ENDIF
	SELECT mm
	LOCATE FOR edi_type = '945' AND mm.office = cOffice AND mm.accountid = nAcctNum
	tsendto = IIF(mm.use_alt,sendtoalt,mm.sendto)
	tcc = IIF(mm.use_alt,mm.ccalt,mm.cc)
	cFilenameHold = (ALLTRIM(mm.holdpath)+cCustPrefix+dt1+".edi")
	cFilename = cFilenameHold
	cFilenameShort = JUSTFNAME(cFilenameHold)
	cFilenameArch = (ALLTRIM(mm.archpath)+cFilenameShort)
	lcPath = ALLTRIM(mm.basepath)
	cFilenameOut = (lcPath+cFilenameShort)
	nFilenum = FCREATE(cFilenameHold)

	LOCATE
	LOCATE FOR mm.edi_type = 'MISC' AND mm.taskname = 'GENERAL'
	tsendtotest = IIF(mm.use_alt,sendtoalt,mm.sendto)
	tcctest = IIF(mm.use_alt,mm.ccalt,mm.cc)
	tsendtoerr = IIF(mm.use_alt,sendtoalt,mm.sendto)
	tccerr = IIF(mm.use_alt,mm.ccalt,mm.cc)


	IF !USED("edi_trigger")
		USE F:\3pl\DATA\EDI_TRIGGEr IN 0
	ENDIF

	IF USED('OUTSHIP')
		USE IN outship
	ENDIF

	csq1 = [select * from outship where right(rtrim(str(accountid)),4) = ]+TRANSFORM(nAcctNum)+[ and office = ']+IIF(cOffice = "I","N",cOffice)+[' and bol_no = ']+cBOL+[']
	xsqlexec(csq1,,,"wh")
	INDEX ON bol_no TAG bol_no

	SELECT outship
	IF !SEEK(cBOL,'outship','bol_no')
		cErrMsg = "MISS BOL "+cBOL
		DO ediupdate WITH cErrMsg,.T.
		THROW
	ELSE
		lNolan = IIF("GROUPNAME*NOLAN"$outship.shipins OR nAcctNum = 6554,.T.,.F.)
		lHotTopic = IIF(("HOT"$outship.consignee AND "TOPIC"$outship.consignee) OR "TORRID"$outship.consignee,.T.,.F.)
		IF lHotTopic
			WAIT WINDOW "This is a HOT TOPIC shipment" NOWAIT
			lDoCompare = .F.
		ENDIF

	ENDIF

	cCustFolder = UPPER(cCustname)  && +"-"+cCustLoc
	lNonEDI = .F.  && Assumes not a Non-EDI 945
	lDoPAL = .T.
	STORE "" TO lcKey
	STORE 0 TO alength,nLength

*!* SET OTHER CONSTANTS

	csendqual = "ZZ"
	IF lTesting
		csendid = "TGF-US-TEST"
	ELSE
		csendid = "TGF-US-"+ICASE(cOffice = "I","NJ",cOffice = "M","FL","SP")
	ENDIF

	crecqual = "01"
	crecid = IIF(lNolan,"079360796","087607334")

	cfd = "^"
	csegd = "~"
	nSegCtr = 0
	cterminator = ">" && Used at end of ISA segment
	cdate = DTOS(DATE())
	ctruncdate = RIGHT(cdate,6)
	ctime = LEFT(TIME(DATE()),2)+SUBSTR(TIME(DATE()),4,2)
	cfiledate = cdate+ctime
	cOrig = "J"
	nSTCount = 0
	cStyle = ""
	cPTString = ""
	nTotCtnWt = 0
	nTotCtnCount = 0
	nUnitSum = 0
	cTargetStyle = ""

** PAD ID Codes
	csendidlong = PADR(csendid,15," ")
	crecidlong = PADR(crecid,15," ")

	IF INLIST(cBOL,'04907316182205952')
		lOverflow = .T.
		SET DELETED OFF
	ENDIF

	IF	USED("parcel_carriers")
		USE IN parcel_carriers
	ENDIF
	USE F:\3pl\DATA\parcel_carriers IN 0 ALIAS parcel_carriers

	SELECT outship
	=SEEK(cBOL,'outship','bol_no')
	nWO_num = outship.wo_num
	cShip_SCAC = IIF(ALLTRIM(outship.scac)="UPSZ","UPSN",ALLTRIM(outship.scac))  && Changed per Maria Estrella, 03.20.2014
	lParcelType = IIF(SEEK(cShip_SCAC,'parcel_carriers','scac'),.T.,.F.)
	lParcelSCAC = lParcelType
	lParcelType = IIF(cShip_SCAC = "DHLG",.T.,lParcelType)

	lDoSQLUPS = .T.
	IF lParcelType AND !lTesting
		IF USED('upswo')
			USE IN upswo
		ENDIF
		SELECT 0
		USE F:\3pl\DATA\upswo && Table of UPS WO#'s for back reference
		LOCATE
		LOCATE FOR upswo.accountid = nAcctNum
		IF !FOUND()
			INSERT INTO upswo (accountid,wo_num) VALUES (nAcctNum,nWO_num)
		ELSE
			lDoSQLUPS = IIF(upswo.wo_num = nWO_num,.F.,.T.)
			IF lDoSQLUPS
				REPLACE upswo.wo_num WITH nWO_num IN upswo NEXT 1
			ENDIF
		ENDIF
		USE IN upswo
	ENDIF
	USE IN parcel_carriers

	IF USED('OUTDET')
		USE IN outdet
	ENDIF

	selectoutdet()
	SELECT outdet
	INDEX ON outdetid TAG outdetid

	IF USED('BL')
		USE IN BL
	ENDIF
	csq1 = [select * from bl where bol_no = ']+cBOL+[']
	xsqlexec(csq1,,,"wh")

	IF lDoSQLConnect
		IF lDoSQLUPS
			IF !lDoCompare
				WAIT WINDOW "Skipping Labels/Cartons Comparison..." &cTimeMsg
			ELSE
				WAIT WINDOW "Performing Labels/Cartons Comparison..." &cTimeMsg
				cRetMsg = "X"
				DO m:\dev\prg\sqldata-COMPARE WITH '',cBOL,nWO_num,cOffice,nAcctNum,cPPName
				IF cRetMsg<>"OK"
					lCloseOutput = .F.
					cErrMsg = "SQL ERROR: "+cRetMsg
					SET STEP ON
					DO ediupdate WITH cErrMsg,.T.
					THROW
				ENDIF
			ENDIF

			SELECT outship
			IF USED("sqlwo")
				USE IN sqlwo
			ENDIF
			DELETE FILE "F:\3pl\DATA\sqlwo.dbf"
			SELECT wo_num ;
				FROM outship WHERE bol_no = PADR(cBOL,20) ;
				GROUP BY 1 ;
				ORDER BY 1 ;
				INTO DBF F:\3pl\DATA\sqlwo
			USE IN sqlwo
			USE F:\3pl\DATA\sqlwo IN 0 ALIAS sqlwo

			SELECT sqlwo
*ASSERT .F. MESSAGE "At SQL connect...debug"
			cRetMsg = "X"

			DO m:\dev\prg\sqlconnect_bol  WITH nAcctNum,cCustname,cPPName,.T.,cOffice && Amended for UCC number sequencing
			IF cRetMsg<>"OK"
				cErrMsg = cRetMsg
				DO ediupdate WITH cErrMsg,.T.
				THROW
			ENDIF

			SELECT vbioworldpp
			IF lTesting
				BROWSE
			ENDIF

*			IF !FILE('c:\tempfox\'+cCustname+'wo') AND lParcelType
			SET SAFETY OFF
			COPY TO ('c:\tempfox\'+cCustname+'wo')
*			ENDIF
			LOCATE
		ELSE
			USE ('c:\tempfox\'+cCustname+'wo') IN 0 ALIAS wofile
			SELECT * FROM wofile INTO CURSOR vbioworldpp
			USE IN wofile
		ENDIF
	ENDIF

*!* Added this code block to remove zero-qty cartons from SQL cursor

	IF !USED("vbioworldpp")
		cErrMsg = "No vBioworldpp cursor or table"
		DO ediupdate WITH cErrMsg,.T.
		THROW
	ENDIF

	SELECT ucc,SUM(totqty) AS uccqty FROM vbioworldpp GROUP BY 1 INTO CURSOR tempchk HAVING uccqty=0
	SELECT tempchk
	LOCATE
	IF !EOF()
		SCAN
			STORE ALLT(ucc) TO cUCC
			SELECT vbioworldpp
			LOCATE
			DELETE FOR ucc = cUCC
		ENDSCAN
	ENDIF
	USE IN tempchk

	SELECT vbioworldpp
	SET RELATION TO outdetid INTO outdet

	IF !USED("scacs")
		xsqlexec("select * from scac","scacs",,"whall")
		INDEX ON scac TAG scac
	ENDIF
	IF !USED("parcel_carriers")
		USE F:\3pl\DATA\parcel_carriers IN 0 ALIAS parcel_carriers
	ENDIF
	SELECT outship
	LOCATE
	WAIT CLEAR


************************************************************************************************************************
	WAIT WINDOW "Now creating Header information" NOWAIT NOCLEAR

*!* HEADER LEVEL EDI DATA
	DO num_incr_isa

	cISA_Num = PADL(c_CntrlNum,9,"0")
	nISA_Num = INT(VAL(cISA_Num))

	STORE "ISA"+cfd+"00"+cfd+"          "+cfd+"00"+cfd+"          "+cfd+csendqual+cfd+csendidlong+cfd+;
		crecqual+cfd+crecidlong+cfd+ctruncdate+cfd+ctime+cfd+"U"+cfd+"00401"+cfd+;
		cISA_Num+cfd+"0"+cfd+"P"+cfd+cterminator+csegd TO cString
	DO cstringbreak

	STORE "GS"+cfd+"SW"+cfd+csendid+cfd+crecid+cfd+cdate+cfd+ctime+cfd+c_CntrlNum+;
		cfd+"X"+cfd+cX12+csegd TO cString
	DO cstringbreak

	SELECT outship
	LOCATE
	SET ORDER TO

*************************************************************************
*1  PTIDSOURCE/OUTSHIP RECORD MAIN LOOP
*  Loops through individual OUTSHIP lines for BOL#
*************************************************************************

	WAIT CLEAR
	WAIT WINDOW "Now creating BOL#-based information..." NOWAIT NOCLEAR

	SCANSTR = "outship.BOL_NO = cBOL"
	nPTCount = 0
	SCAN FOR &SCANSTR
* Wait Window "" Timeout 2
		SCATTER MEMVAR MEMO
		cShip_SCAC = ALLTRIM(m.scac)
		lHotTopic = IIF(("HOT"$outship.consignee AND "TOPIC"$outship.consignee) OR "TORRID"$outship.consignee,.T.,.F.)
		IF lHotTopic
			WAIT WINDOW "This is a HOT TOPIC/TORRID shipment" NOWAIT
		ENDIF
		lJCPenney = IIF("PENNEY"$outship.consignee,.T.,.F.)
		lWalMart = IIF(("WALMART"$UPPER(m.consignee)) OR ("WAL-MART"$UPPER(m.consignee)),.T.,.F.)
		lSpencer = IIF("SPENCER"$outship.consignee,.T.,.F.)
		lTarget = IIF("TARGET"$outship.consignee,.T.,.F.)
		lGap = IIF("GAP"$outship.consignee,.T.,.F.)
		lKmart = IIF("KMART"$outship.consignee OR "K-MART"$outship.consignee,.T.,.F.)
		lToys= IIF("TOYS"$outship.consignee OR "CAL CARTAGE"$outship.consignee OR "BABIES"$outship.consignee,.T.,.F.)  && Added Cal Cartage and Babies are us

		IF lWalMart
			WAIT WINDOW "This is a Wal-Mart order" &cTimeMsg
		ENDIF
		ddel_date = outship.del_date
		IF EMPTY(ddel_date) AND notonwip
			LOOP
		ENDIF
		nOSQty = m.qty
		IF nOSQty = 0
			LOOP
		ENDIF

		alength = ALINES(apt,outship.shipins,.T.,CHR(13))
		nTotCtnCount = m.qty
		cPO_Num = ALLTRIM(m.cnee_ref)
		cShip_ref = ALLTRIM(m.ship_ref)
		IF (" OV"$cShip_ref) OR (RIGHT(ALLTRIM(cShip_ref),1)=".")
			LOOP
		ENDIF

		nPTCount = nPTCount + 1

		nWO_num = outship.wo_num
		cWO_Num = ALLTRIM(STR(nWO_num))
		cPadWO_Num = PADR(cWO_Num,10)
		IF TRIM(cWO_Num) <> TRIM(cWO_NumOld)
*ASSERT .F. MESSAGE "At WO_NUMSTR creation...DEBUG"
			STORE cWO_Num TO cWO_NumOld
			IF EMPTY(cWO_NumStr)
				STORE cWO_Num TO cWO_NumStr
			ELSE
				cWO_NumStr = (cWO_NumStr+", "+cWO_Num)
			ENDIF
			IF EMPTY(cWO_NumList)
				STORE cWO_Num TO cWO_NumList
			ELSE
				cWO_NumList = (cWO_NumList+CHR(13)+cWO_Num)
			ENDIF
		ENDIF

		nOutshipid = outship.outshipid
		cPTString = IIF(EMPTY(cPTString),m.consignee+" "+cShip_ref,cPTString+CHR(13)+m.consignee+" "+cShip_ref)
		nCtnNumber = 1  && Begin sequence count

		DO num_incr_st
		WAIT CLEAR
		WAIT WINDOW "Now creating Line Item information" NOWAIT NOCLEAR
		INSERT INTO temp945 (accountid,isa_num,st_num,wo_num,bol_no,ship_ref,filename,filedate) ;
			VALUES (m.accountid,c_CntrlNum,c_GrpCntrlNum,m.wo_num,m.bol_no,m.ship_ref,cFilenameShort,dt2)
******** added 1/9/17 TMARG to capture 997 data

		IF ship_ref !='LICENSOR SAMPLE' AND !lTesting
			xsqlexec("select * from ackdata where .f.","xack",,"wh")
			SELECT xack
			SCATTER MEMVAR MEMO BLANK
			USE IN xack
			m.groupnum=c_CntrlNum
			m.isanum=cISA_Num
			m.transnum=PADL(c_GrpCntrlNum,9,"0")
			m.edicode="SW"
			m.accountid=m.accountid
			m.loaddt=DATE()
			m.loadtime=DATETIME()
			m.filename=cFilenameShort
			m.ship_ref=m.ship_ref
			insertinto("ackdata","wh",.T.)
			tu("ackdata")
		ELSE
		ENDIF

		SELECT outship


		STORE "ST"+cfd+"945"+cfd+PADL(c_GrpCntrlNum,9,"0")+csegd TO cString
		DO cstringbreak
		nSTCount = nSTCount + 1
		nSegCtr = 1

		cShipmentID = TRIM(cBOL)
		STORE "W06"+cfd+"N"+cfd+cShip_ref+cfd+cdate+cfd+TRIM(cShipmentID)+cfd+TRIM(cWO_Num)+cfd+cPO_Num+csegd TO cString
		DO cstringbreak
		nSegCtr = nSegCtr + 1
		lW06 = .T.

		cDUNSQUAL = segmentget(@apt,"DUNSQUAL",alength)
		cDUNSQUAL = IIF(EMPTY(cDUNSQUAL),"9",cDUNSQUAL)
		cDUNSNumber = segmentget(@apt,"DUNSNUMBER",alength)
		cDUNSNumber = IIF(EMPTY(cDUNSNumber),ALLTRIM(outship.duns),cDUNSNumber)
		IF EMPTY(ALLTRIM(outship.duns))
			STORE "N1"+cfd+"SF"+cfd+IIF(nAcctNum = 6182,"BIOWORLD","NOLAN")+csegd TO cString
		ELSE
			STORE "N1"+cfd+"SF"+cfd+IIF(nAcctNum = 6182,"BIOWORLD","NOLAN")+cfd+cDUNSQUAL+cfd+cDUNSNumber+csegd TO cString
		ENDIF
		DO cstringbreak
		nSegCtr = nSegCtr + 1

		STORE "N3"+cfd+"400-450 WESTMONT DRIVE"+csegd TO cString
		DO cstringbreak
		nSegCtr = nSegCtr + 1

		STORE "N4"+cfd+"SAN PEDRO"+cfd+"CA"+cfd+"90731"+csegd TO cString
		DO cstringbreak
		nSegCtr = nSegCtr + 1

		STORE "" TO cBT,cBTDC

		cBTName   = segmentget(@apt,"BILLTO",alength)
		cBTDC = segmentget(@apt,"BILLDC",alength)
		cBYName = segmentget(@apt,"BYNAME",alength)
		cBYDC = segmentget(@apt,"N1-BY",alength)
		IF EMPTY(cBYDC)
			cBYDC = cBTDC
		ENDIF
		IF EMPTY(cBYName)
			cBYName = cBTName
		ENDIF

		IF !EMPTY(cBYName) AND !EMPTY(cBYDC)
			STORE "N1"+cfd+"BY"+cfd+ALLTRIM(cBYName)+cfd+"92"+cfd+ALLTRIM(cBYDC)+csegd TO cString
			DO cstringbreak
			nSegCtr = nSegCtr + 1
		ENDIF

		IF !EMPTY(cBTName) AND !EMPTY(cBTDC)
			STORE "N1"+cfd+"BT"+cfd+ALLTRIM(cBTName)+cfd+"92"+cfd+ALLTRIM(cBTDC)+csegd TO cString
			DO cstringbreak
			nSegCtr = nSegCtr + 1
		ENDIF

		cDCNum = ""
		cDCNum = ALLT(segmentget(@apt,"STORENUM",alength))
		cDCNum = IIF((cDCNum # m.dcnum) OR (EMPTY(cDCNum)),ALLTRIM(m.dcnum),cDCNum)

		m.CSZ = TRIM(outship.CSZ)
		IF EMPTY(ALLT(STRTRAN(M.CSZ,",","")))
			WAIT CLEAR
			WAIT WINDOW "No SHIP-TO City/State/ZIP info...exiting" TIMEOUT 2
			cErrMsg = "NO CSZ INFO"
			DO ediupdate WITH cErrMsg,.T.
			THROW
		ENDIF
		IF !(", "$m.CSZ)
			m.CSZ = ALLTRIM(STRTRAN(m.CSZ,",",", "))
		ENDIF
		m.CSZ = ALLTRIM(STRTRAN(m.CSZ,"  "," "))
		cCity = ALLT(LEFT(TRIM(m.CSZ),AT(",",m.CSZ)-1))
		cStateZip = ALLT(SUBSTR(TRIM(m.CSZ),AT(",",m.CSZ)+1))

		cState = ALLT(LEFT(cStateZip,2))
		cOrigState = segmentget(@apt,"ORIGSTATE",alength)
		cState = IIF(EMPTY(cOrigState),cState,cOrigState)
		cZip = ALLT(SUBSTR(cStateZip,3))

		cSHIPTO = STRTRAN(ALLTRIM(segmentget(@apt,"SHIPTO",alength)),"<>","#") && Added the strtran to force the pound sign after internal conversion.
		cSHIPTO = IIF(EMPTY(cSHIPTO),ALLTRIM(outship.consignee),cSHIPTO)
		STORE "N1"+cfd+"ST"+cfd+cSHIPTO+IIF(!EMPTY(cDCNum),+cfd+"92"+cfd+cDCNum,"")+csegd TO cString
		DO cstringbreak
		nSegCtr = nSegCtr + 1

		STORE "N3"+cfd+TRIM(outship.address)+IIF(!EMPTY(outship.address2),cfd+ALLT(outship.address2),"")+csegd TO cString
		DO cstringbreak
		nSegCtr = nSegCtr + 1

		STORE "N4"+cfd+cCity+cfd+cState+cfd+cZip+csegd TO cString
		DO cstringbreak
		nSegCtr = nSegCtr + 1


		IF !EMPTY(m.shipfor)
			STORE "" TO cSForCity,cSForState,cSForZip
			m.SForCSZ = ALLTRIM(STRTRAN(m.SForCSZ,"  "," "))
			nSpaces = OCCURS(" ",ALLTRIM(m.SForCSZ))
			IF nSpaces = 0
				m.SForCSZ = STRTRAN(m.SForCSZ,",","")
				cSForCity = ALLTRIM(m.SForCSZ)
				cSForState = ""
				cSForZip = ""
			ELSE
				nCommaPos = AT(",",m.SForCSZ)
				nLastSpace = AT(" ",m.SForCSZ,nSpaces)
				nMinusSpaces = IIF(nSpaces=1,0,1)
				IF ISALPHA(SUBSTR(TRIM(m.SForCSZ),AT(" ",TRIM(m.SForCSZ),nSpaces-nMinusSpaces)+1,2))
					cSForCity = LEFT(TRIM(m.SForCSZ),AT(",",m.SForCSZ)-1)
					cSForState = SUBSTR(m.SForCSZ,nCommaPos+2,2)
					cSForZip = TRIM(RIGHT(TRIM(m.SForCSZ),5))
					IF ISALPHA(cSForZip)
						cSForZip = ""
					ENDIF
				ELSE
					WAIT CLEAR
					WAIT WINDOW "NOT ALPHA: "+SUBSTR(TRIM(m.SForCSZ),AT(" ",m.SForCSZ,nSpaces-1)+1,2) &cTimeMsg
					cSForCity = LEFT(TRIM(m.SForCSZ),AT(",",m.SForCSZ)-1)
					cSForState = SUBSTR(TRIM(m.SForCSZ),AT(" ",m.SForCSZ,nSpaces-2)+1,2)
					cSForZip = TRIM(RIGHT(TRIM(m.SForCSZ),5))
					IF ISALPHA(cSForZip)
						cSForZip = ""
					ENDIF
				ENDIF
			ENDIF

			csForStore = segmentget(@apt,"SFSTOREFULL",alength)
			csForStore = IIF(EMPTY(csForStore),m.sforstore,csForStore)
			IF EMPTY(ALLTRIM(m.sforstore))
				STORE "N1"+cfd+"Z7"+cfd+ALLTRIM(m.shipfor)+csegd TO cString
			ELSE
				STORE "N1"+cfd+"Z7"+cfd+ALLTRIM(m.shipfor)+cfd+"92"+cfd+ALLTRIM(csForStore)+csegd TO cString
			ENDIF
			DO cstringbreak
			nSegCtr = nSegCtr + 1
			STORE "N3"+cfd+ALLTRIM(m.sforaddr1)+csegd TO cString
			DO cstringbreak
			nSegCtr = nSegCtr + 1

			STORE "N4"+cfd+ALLTRIM(cSForCity)+cfd+ALLTRIM(cSForState)+cfd+ALLTRIM(cSForZip)+csegd TO cString
			DO cstringbreak
			nSegCtr = nSegCtr + 1
		ENDIF

** Ok, now in the header N9 segments
		lFedex = IIF(SEEK(cShip_SCAC,'parcel_carriers','scac') AND parcel_carriers.fedex,.T.,.F.)
		IF lFedex
			cShip_SCAC = "FDEG"
		ENDIF

		IF lParcelType AND "PENNEY"$UPPER(m.consignee)
			STORE "N9"+cfd+"BM"+cfd+"0"+csegd TO cString
			DO cstringbreak
			nSegCtr = nSegCtr + 1
		ELSE
			STORE "N9"+cfd+"BM"+cfd+TRIM(cBOL)+csegd TO cString
			DO cstringbreak
			nSegCtr = nSegCtr + 1
		ENDIF

		IF EMPTY(outship.dept)
			STORE "N9"+cfd+"DP"+cfd+"NA"+csegd TO cString
		ELSE
			STORE "N9"+cfd+"DP"+cfd+TRIM(outship.dept)+csegd TO cString
		ENDIF
		DO cstringbreak
		nSegCtr = nSegCtr + 1

		cMR = segmentget(@apt,"MR",alength)
		IF lWalMart AND cMR#"0073"
			lDoManUC = .T.
		ENDIF

		IF !EMPTY(cMR)
			STORE "N9"+cfd+"MR"+cfd+TRIM(cMR)+csegd TO cString
			DO cstringbreak
			nSegCtr = nSegCtr + 1
		ENDIF

		cContract = segmentget(@apt,"CT",alength)
		IF !EMPTY(cContract)
			STORE "N9"+cfd+"CT"+cfd+TRIM(cContract)+csegd TO cString
			DO cstringbreak
			nSegCtr = nSegCtr + 1
		ENDIF

		cInvoice = IIF(EMPTY(ALLTRIM(m.keyrec)),ALLTRIM(m.ship_ref),ALLTRIM(m.keyrec))
		IF !EMPTY(cInvoice)
			STORE "N9"+cfd+"CN"+cfd+TRIM(cInvoice)+csegd TO cString
			DO cstringbreak
			nSegCtr = nSegCtr + 1
		ENDIF

		cVendnum = ""
		cVendnum = segmentget(@apt,"VENDORNUM",alength) && Added for 10+ character vendor numbers
		IF EMPTY(cVendnum)
			cVendnum = TRIM(outship.vendor_num)
		ENDIF
		IF !EMPTY(cVendnum)
			STORE "N9"+cfd+"IA"+cfd+cVendnum+csegd TO cString
			DO cstringbreak
			nSegCtr = nSegCtr + 1
		ENDIF

		cCOID = segmentget(@apt,"COID",alength) && Company ID needed for Bioworld ASNs
		IF !EMPTY(cCOID)
			STORE "N9"+cfd+"ZM"+cfd+cCOID+csegd TO cString
			DO cstringbreak
			nSegCtr = nSegCtr + 1
		ENDIF

*		IF lToys
		cRoute = IIF(lTesting AND lToys,"DSP"+PADL(ALLTRIM(STR(CEILING(RAND()*(10^8)))),9,"0"),segmentget(@apt,"ROUTENUM",alength))
		IF EMPTY(cRoute)
			cRoute = ALLTRIM(outship.appt_num)
*			cRoute = ALLTRIM(outship.keyrec) && Changed from keyrec per Maria E., 09.12.2016
			IF EMPTY(cRoute)
				IF lParcelType OR lParcelSCAC
					cRoute = DTOS(DATE())
				ELSE
					cErrMsg = "NO ROUTE CODE"
					DO ediupdate WITH cErrMsg,.T.
				ENDIF
			ENDIF
		ENDIF
		IF !EMPTY(cRoute)
			STORE "N9"+cfd+"RU"+cfd+TRIM(cRoute)+csegd TO cString
			DO cstringbreak
			nSegCtr = nSegCtr + 1
		ELSE
			IF lToys
				WAIT WINDOW "No 'RU' Routing code...exiting" TIMEOUT 2
				cErrMsg = "NO ROUTE CODE"
				DO ediupdate WITH cErrMsg,.T.
				THROW
			ENDIF
		ENDIF
*		ENDIF

		cRL = segmentget(@apt,"RLNUM",alength)
		IF !EMPTY(cRL)
			STORE "N9"+cfd+"RL"+cfd+TRIM(cRL)+csegd TO cString
			DO cstringbreak
			nSegCtr = nSegCtr + 1
		ENDIF

		cPHC = segmentget(@apt,"PHCNUM",alength)
		IF !EMPTY(cPHC)
			STORE "N9"+cfd+"PHC"+cfd+TRIM(cPHC)+csegd TO cString
			DO cstringbreak
			nSegCtr = nSegCtr + 1
		ENDIF

		cIDNum = segmentget(@apt,"IDNUM",alength)
		IF !EMPTY(cIDNum)
			STORE "N9"+cfd+"ID"+cfd+cIDNum+csegd TO cString
			DO cstringbreak
			nSegCtr = nSegCtr + 1
		ENDIF

		SELECT outship

*****************************************************8
		cBOL = ALLTRIM(outship.bol_no)
		cShipType      =""
*    cShip_SCAC     =""
		cTrailerNumber =""
		cProNumber     =""
		cTerms         =""

		IF lParcelType
			cTrackNum = ALLTRIM(outship.bol_no)  && UPS Tracking Number
		ELSE
			SELECT BL
			LOCATE FOR bol_no = cBOL
			IF FOUND()
				cShip_SCAC    = IIF(!EMPTY(BL.scac),BL.scac,cShip_SCAC)
				cTrailerNumber= LEFT(ALLTRIM(BL.trailer),10)
				cProNumber    = BL.pronumber
				DO CASE
					CASE BL.terms ="1"
						cTerms = "PP"
					CASE BL.terms ="2"
						cTerms = "CC"
					OTHERWISE
						cTerms = ""
				ENDCASE
			ELSE
				cErrMsg = "MISSING BL Data: "+cBOL
				DO ediupdate WITH cErrMsg,.T.
				THROW
			ENDIF
		ENDIF

*!* For JCPenney orders only
		IF "PENNEY"$UPPER(outship.consignee) && AND !lParcelType, Parcel exclusion removed per Siggy, 10.11.2013
			cLoadID = ALLTRIM(outship.appt_num)
			IF EMPTY(cLoadID)
				cErrMsg = "MISS LOADID for Penney"
				DO ediupdate WITH cErrMsg,.T.
				THROW
			ENDIF
			IF !INLIST(cLoadID,"DIRECT TO STORE","DTS")
				STORE "N9"+cfd+"AO"+cfd+TRIM(cLoadID)+csegd TO cString
				DO cstringbreak
				nSegCtr = nSegCtr + 1
			ENDIF
		ENDIF

*!* For Kohl'st orders only
		IF "KOHL"$UPPER(outship.consignee) AND !lParcelType
			cLoadID = ALLTRIM(outship.appt_num)
			IF EMPTY(cLoadID)
				cErrMsg = "MISS LOADID for Kohls........"
				DO ediupdate WITH cErrMsg,.T.
				THROW
			ENDIF
			STORE "N9"+cfd+"06"+cfd+TRIM(cLoadID)+csegd TO cString
			DO cstringbreak
			nSegCtr = nSegCtr + 1
		ENDIF

*!* For Kohl'st orders only
		IF "BOSCOV"$UPPER(outship.consignee)
			cLoadID = ALLTRIM(outship.appt_num)
			IF EMPTY(cLoadID)
				cErrMsg = "MISS LOADID for Boscov........"
				DO ediupdate WITH cErrMsg,.T.
				THROW
			ENDIF
			STORE "N9"+cfd+"LO"+cfd+TRIM(cLoadID)+csegd TO cString
			DO cstringbreak
			nSegCtr = nSegCtr + 1
		ENDIF

		IF EMPTYnul(outship.del_date)
			IF lJCPenney
				cErrMsg = "MISSING DELDATE"
				DO ediupdate WITH cErrMsg,.T.
				THROW
			ELSE
				ddel_date = outship.appt
			ENDIF
		ELSE
			ddel_date = outship.del_date
			IF EMPTY(ddel_date)
				SELECT EDI_TRIGGEr
				LOCATE
				LOCATE FOR accountid = nAcctNum AND ship_ref = cShip_ref AND bol = cBOL AND edi_type = "945"
				IF FOUND()
					REPLACE fin_status WITH "MISSING DELDATE",processed WITH .T.,errorflag WITH .T.
					tsubject = "945 Error in "+IIF(nAcctNum=6554,"NOLAN","BIOWORLD")+"/EA BOL "+TRIM(cBOL)+" (At PT "+cShip_ref+")"
					tattach = " "
					tsendto = tsendtoerr
					tcc = tccerr
					tmessage = "945 Processing for WO# "+TRIM(cWO_Num)+" produced this error: (Office: "+cOffice+"): "+cStatus+CHR(13)+"Please notify Joe of actual status for reprocessing ASAP."
					DO FORM m:\dev\frm\dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
				ENDIF
				LOOP
			ENDIF

			dapptdate = outship.appt
			IF EMPTY(dapptdate)
				dapptdate = outship.del_date
			ENDIF

		ENDIF

		IF EMPTYnul(outship.del_date) OR EMPTYnul(outship.START) OR EMPTYnul(outship.CANCEL)
			cErrMsg = "EMPTY "+ICASE(EMPTYnul(outship.del_date),"DELDATE",EMPTYnul(outship.START),"START","CANCEL")
			DO ediupdate WITH cErrMsg,.T.
			THROW
		ENDIF

		cUseTime = "153000"
		STORE "G62"+cfd+"11"+cfd+DTOS(del_date)+cfd+"9"+cfd+cUseTime+csegd TO cString  && Ship date
		DO cstringbreak
		nSegCtr = nSegCtr + 1

*		IF ("MACY"$UPPER(m.consignee) OR "MMG"$UPPER(m.consignee) OR "KMART"$UPPER(m.consignee) OR "K-MART"$UPPER(m.consignee))
		cUseTime = "153000"
		STORE "G62"+cfd+"67"+cfd+DTOS(del_date)+cfd+"9"+cfd+cUseTime+csegd TO cString  && Sched ship date
		DO cstringbreak
		nSegCtr = nSegCtr + 1
*		ENDIF

*!*			STORE "G62"+cfd+"01"+cfd+DTOS(outship.CANCEL)+csegd TO cString  && Ship date
*!*			DO cstringbreak
*!*			nSegCtr = nSegCtr + 1

		STORE "G62"+cfd+"10"+cfd+DTOS(outship.START)+csegd TO cString  && Ship date
		DO cstringbreak
		nSegCtr = nSegCtr + 1

		alength = ALINES(apt,outship.shipins,.T.,CHR(13))
		cPODate = segmentget(@apt,"PODATE",alength)
		IF !EMPTY(cPODate)
			STORE "G62"+cfd+"04"+cfd+cPODate+csegd TO cString  && Ship date
			DO cstringbreak
			nSegCtr = nSegCtr + 1
		ENDIF

		cDNLDate = segmentget(@apt,"DNLDATE",alength)
		IF !EMPTY(cDNLDate)
			STORE "G62"+cfd+"54"+cfd++DTOS(del_date)+csegd TO cString  && Ship date
			DO cstringbreak
			nSegCtr = nSegCtr + 1
		ENDIF

		SELECT outship
		cShipType=IIF(lParcelType,"U","M")
		cTerms = IIF(lParcelType,"CC",cTerms)
		DO CASE
			CASE EMPTY(cTerms) AND EMPTY(cTrailerNumber)
				STORE "W27"+cfd+cShipType+cfd+ALLTRIM(cShip_SCAC)+cfd+ALLTRIM(outship.ship_via)+csegd TO cString
			CASE !EMPTY(cTerms) AND EMPTY(cTrailerNumber)
				STORE "W27"+cfd+cShipType+cfd+ALLTRIM(cShip_SCAC)+cfd+ALLTRIM(outship.ship_via)+cfd+ALLTRIM(cTerms)+csegd TO cString
			OTHERWISE
				STORE "W27"+cfd+cShipType+cfd+ALLTRIM(cShip_SCAC)+cfd+ALLTRIM(outship.ship_via)+cfd+ALLTRIM(cTerms)+cfd+cfd+cfd+ALLTRIM(cTrailerNumber)+csegd TO cString
		ENDCASE
		DO cstringbreak
		nSegCtr = nSegCtr + 1

*************************************************************************
*2  DEtAIL LOOP - MATCHING SHIP_REF FIELD (FROM OUTSHIP)
*************************************************************************
		WAIT CLEAR
		WAIT WINDOW "Now creating Detail information "+cShip_ref NOWAIT NOCLEAR
		SELECT vbioworldpp
		LOCATE
		LOCATE FOR ship_ref = TRIM(cShip_ref) AND outshipid = nOutshipid
		IF !FOUND()
			SET STEP ON
			IF !lTesting
				WAIT WINDOW "PT "+cShip_ref+" NOT FOUND in vbioworldpp...ABORTING" &cTimeMsg
				IF !lTesting
					cErrMsg = "MISS PT: "+cShip_ref
					DO ediupdate WITH cErrMsg,.T.
				ENDIF
				=FCLOSE(nFilenum)
				ERASE &cFilename
				THROW
			ELSE
				LOOP
			ENDIF
		ENDIF

*!* Added this code to trap unmatched OUTDETIDs in OUTDET, Joe 11.22.2005

		SCAN FOR vbioworldpp.ship_ref = TRIM(cShip_ref) AND vbioworldpp.outshipid = nOutshipid AND vbioworldpp.totqty#0
			IF EMPTY(outdet.outdetid)
				SET STEP ON
				WAIT WINDOW "OUTDETID "+TRANSFORM(vbioworldpp.outdetid)+" NOT FOUND IN OUTDET!...CLOSING" &cTimeMsg
				cErrMsg = "MISS ODID/WO: "+TRANSFORM(vbioworldpp.outdetid)+"/"+TRANSFORM(vbioworldpp.wo_num)
				DO ediupdate WITH cErrMsg,.T.
				=FCLOSE(nFilenum)
				ERASE &cFilename
				THROW
			ENDIF
		ENDSCAN

		IF cBOL = '04907316182681770'
			SCANSTR = "vbioworldpp.ship_ref = cShip_ref and vbioworldpp.outshipid = nOutshipid and !DELETED()"
		ELSE
*SET DELETED ON
			SCANSTR = "vbioworldpp.ship_ref = cShip_ref and vbioworldpp.outshipid = nOutshipid and !DELETED()"
		ENDIF

		SELECT vbioworldpp
		LOCATE FOR &SCANSTR
		cCartonNum= "XXX"
		cUCC="XXX"

		DO WHILE &SCANSTR
			lSkipBack = .T.

			IF INLIST(vbioworldpp.outdetid,8203996,8203997)
				SET STEP ON
			ENDIF

			IF vbioworldpp.totqty = 0
				SKIP 1 IN vbioworldpp
				LOOP
			ENDIF

			IF vbioworldpp.ucc = 'CUTS'
				SKIP 1 IN vbioworldpp
				LOOP
			ENDIF

			IF TRIM(vbioworldpp.ucc) <> cCartonNum
				STORE TRIM(vbioworldpp.ucc) TO cCartonNum
			ENDIF

			STORE "LX"+cfd+ALLTRIM(STR(nCtnNumber))+csegd TO cString   && Carton Seq. # (up to Carton total)
			lDoManSegment = .T.
			DO cstringbreak
			nSegCtr = nSegCtr + 1

			cDoString = "vbioworldpp.ucc = cCartonNum and vbioworldpp.totqty > 0"

			DO WHILE &cDoString
				alength = ALINES(aptdet,outdet.printstuff,.T.,CHR(13))
				IF lDoManSegment
					cUCC = TRIM(vbioworldpp.ucc)
					IF !EMPTY(cUCC) AND cUCC # "XXX"
						lDoManSegment = .F.
						cPRONum = IIF(lTesting,"999888777",ALLTRIM(outship.keyrec))
						IF lToys AND !EMPTY(cPRONum)
							STORE "MAN"+cfd+"GM"+cfd+TRIM(cUCC)+cfd+cfd+"ZZ"+cfd+cPRONum+csegd TO cString  && UCC Number (Wal-mart DSDC only)
						ELSE
							STORE "MAN"+cfd+"GM"+cfd+TRIM(cUCC)+csegd TO cString  && UCC Number (Wal-mart DSDC only)
						ENDIF
						DO cstringbreak
						nSegCtr = nSegCtr + 1
** now write out the saved inner and pack info that was uploaded from the 940, if we have the data........
						cPalPack  = ALLTRIM(segmentget(@aptdet,"W20_1",alength))
						cPalInner = ALLTRIM(segmentget(@aptdet,"W20_2",alength))
						cOrigWt = ALLTRIM(segmentget(@aptdet,"ORIG_WT",alength))
						cUOW = IIF(EMPTY(cOrigWt),"",ALLTRIM(segmentget(@aptdet,"ORIG_UOW",alength)))
						cOrigVol = ALLTRIM(segmentget(@aptdet,"ORIG_VOL",alength))
						cUOV = IIF(EMPTY(cOrigVol),"",ALLTRIM(segmentget(@aptdet,"ORIG_UOV",alength)))
						STORE "PAL"+cfd+cfd+cfd+cfd+ALLTRIM(cPalPack)+cfd+cfd+cfd+cfd+cfd+cfd+cfd+cOrigWt+cfd+cUOW+cfd+cOrigVol+cfd+cUOV+cfd+cfd+ALLTRIM(cPalInner)+csegd TO cString
						DO cstringbreak
						nSegCtr = nSegCtr + 1
					ELSE
						WAIT CLEAR
						WAIT WINDOW "Empty UCC Number in vbioworldpp "+cShip_ref &cTimeMsg
						IF lTesting
							ASSERT .F. MESSAGE "EMPTY UCC...DEBUG"
							CLOSE DATABASES ALL
							=FCLOSE(nFilenum)
							ERASE &cFilename
							THROW
						ELSE
							cErrMsg = "EMPTY UCC# in "+cShip_ref
							DO ediupdate WITH cErrMsg,.T.
							=FCLOSE(nFilenum)
							ERASE &cFilename
							THROW
						ENDIF
					ENDIF
				ENDIF

				cUPCCode = segmentget(@aptdet,"UPCCODE",alength)
				cColor = TRIM(outdet.COLOR)
				STORE ALLTRIM(outdet.ID) TO cSize
				cStyle = TRIM(outdet.STYLE)

				cUPC = ""
				cUPC = segmentget(@aptdet,"ORIGUPC",alength)
				IF EMPTY(cUPC)
					cUPC = IIF(lTesting,TRIM(vbioworldpp.upc),TRIM(outdet.upc))
				ENDIF
				cUPC = IIF(lGap,"",cUPC)

				cGTIN = ""
				cGTIN = segmentget(@aptdet,"GTIN",alength)
				IF LEN(ALLTRIM(cUPC)) # 12
					IF cUPCCode = "UK"
						IF EMPTY(cGTIN)
							cGTIN = TRIM(outdet.upc)
						ELSE
							cUPC = ""
						ENDIF
					ENDIF
				ENDIF
				cStyle = ALLTRIM(outdet.STYLE)

				cItemNum = segmentget(@aptdet,"CUSTSKU",alength)
				IF EMPTY(ALLT(cItemNum))
					cItemNum = ALLTRIM(outdet.custsku)
				ENDIF
				nDetQty = vbioworldpp.totqty
				nOrigQty = vbioworldpp.qty

				IF EMPTY(cItemNum)
					SET STEP ON
					cErrMsg = "MISS ItNum, OSID "+ALLTRIM(STR(outship.outshipid))
					DO ediupdate WITH cErrMsg,.T.
					=FCLOSE(nFilenum)
					ERASE &cFilename
					THROW
				ENDIF

				IF lTesting AND EMPTY(outdet.ctnwt)
					cCtnWt = "2"
					nTotCtnWt = nTotCtnWt + 2
				ELSE
					STORE ALLTRIM(STR(outdet.ctnwt)) TO cCtnWt
					nTotCtnWt = nTotCtnWt + outdet.ctnwt
				ENDIF

				IF EMPTY(cCtnWt) OR outdet.ctnwt=0
					nCtnWt = outship.weight/outship.ctnqty
					cCtnWt = ALLTRIM(STR(nCtnWt))
					nTotCtnWt = nTotCtnWt + nCtnWt
					IF EMPTY(cCtnWt)
						cErrMsg = "MISS CTNWT "+TRANSFORM(vbioworldpp.outdetid)
						DO ediupdate WITH cErrMsg,.T.
						=FCLOSE(nFilenum)
						ERASE &cFilename
						THROW
					ENDIF
				ENDIF
				cUnitstype = "EA"
				IF nOrigQty=nDetQty
					nShipType = "CL"
				ELSE
					nShipType = "PR"
				ENDIF

				IF nDetQty>0
					IF cShip_ref = '0001980762'
						SET STEP ON
					ENDIF
					cUOM = segmentget(@aptdet,"ORIG_UOM",alength)
					IF EMPTY(cUOM)
						cUOM = segmentget(@aptdet,"UNITCODE",alength)
					ENDIF
					IF EMPTY(cUOM)
						cUOM = 'EA'
					ENDIF
					nUnitSum = nUnitSum + nDetQty
					IF cUPCCode = "UK" AND EMPTY(cUPC) AND !EMPTY(cGTIN) && GTIN data
						STORE "W12"+cfd+nShipType+cfd+ALLTRIM(STR(nOrigQty))+cfd+ALLTRIM(STR(nDetQty))+cfd+;
							ALLTRIM(STR(nOrigQty-nDetQty))+cfd+cUOM+cfd+cUPC+cfd+"VN"+cfd+TRIM(cStyle)+;
							cfd+cfd+cCtnWt+cfd+"G"+cfd+cWeightUnit+REPLICATE(cfd,5)+"IN"+cfd+ALLTRIM(cItemNum)+cfd+cfd+cfd+cUPCCode+cfd+cGTIN+csegd TO cString  && Eaches/Style
					ELSE
						STORE "W12"+cfd+nShipType+cfd+ALLTRIM(STR(nOrigQty))+cfd+ALLTRIM(STR(nDetQty))+cfd+;
							ALLTRIM(STR(nOrigQty-nDetQty))+cfd+cUOM+cfd+cUPC+cfd+"VN"+cfd+TRIM(cStyle)+;
							cfd+cfd+cCtnWt+cfd+"G"+cfd+cWeightUnit+REPLICATE(cfd,5)+"IN"+cfd+ALLTRIM(cItemNum)+csegd TO cString  && Eaches/Style
					ENDIF
					DO cstringbreak
					nSegCtr = nSegCtr + 1

					cDesc = segmentget(@aptdet,"DESC",alength)
					IF !EMPTY(cDesc)
						STORE "G69"+cfd+cDesc+csegd TO cString
						DO cstringbreak
						nSegCtr = nSegCtr + 1
					ENDIF

					IF !EMPTY(cColor)
						STORE "N9"+cfd+"CL"+cfd+cColor+csegd TO cString
						DO cstringbreak
						nSegCtr = nSegCtr + 1
					ENDIF

					IF !EMPTY(cSize)
						STORE "N9"+cfd+"SZ"+cfd+cSize+csegd TO cString
						DO cstringbreak
						nSegCtr = nSegCtr + 1
					ENDIF

					IF !EMPTY(cItemNum)
						STORE "N9"+cfd+"IN"+cfd+cItemNum+csegd TO cString
						DO cstringbreak
						nSegCtr = nSegCtr + 1
					ENDIF

					cBuyerNum = segmentget(@aptdet,"BUYERSIZE",alength)
					IF !EMPTY(cBuyerNum)
						STORE "N9"+cfd+"IZ"+cfd+cBuyerNum+csegd TO cString
						DO cstringbreak
						nSegCtr = nSegCtr + 1
					ENDIF
				ENDIF

				SKIP 1 IN vbioworldpp
			ENDDO
			lSkipBack = .T.
			nCtnNumber = nCtnNumber + 1
		ENDDO

*************************************************************************
*2^  END DETAIL MATCHING SHIP_REF LOOP
*************************************************************************
		WAIT CLEAR
		WAIT WINDOW "Now creating Section Closure information" NOWAIT NOCLEAR
		STORE "W03"+cfd+ALLTRIM(STR(nUnitSum))+cfd+ALLTRIM(STR(nTotCtnWt))+cfd+;
			"LB"+csegd TO cString   && Units sum, Weight sum, carton count

		nTotCtnWt = 0
		nTotCtnCount = 0
		nUnitSum = 0
		IF lTesting
			FPUTS(nFilenum,cString)
		ELSE
			FWRITE(nFilenum,cString)
		ENDIF

		STORE  "SE"+cfd+ALLTRIM(STR(nSegCtr+2))+cfd+PADL(c_GrpCntrlNum,9,"0")+csegd TO cString
		IF lTesting
			FPUTS(nFilenum,cString)
		ELSE
			FWRITE(nFilenum,cString)
		ENDIF

		SELECT outship
		WAIT CLEAR
	ENDSCAN

*************************************************************************
*1^  END OUTSHIP MAIN LOOP
*************************************************************************
	DO close945
	=FCLOSE(nFilenum)
	IF !lW06
		cErrMsg = "Incomp. 945 File "+cBOL
		DO ediupdate WITH cErrMsg,.T.
		THROW
	ENDIF

	IF !lTesting
		SELECT EDI_TRIGGEr
		DO ediupdate WITH "945 CREATED",.F.

		IF !USED("ftpedilog")
			SELECT 0
			USE F:\edirouting\ftpedilog ALIAS ftpedilog
			INSERT INTO ftpedilog (TRANSFER,ftpdate,filename,acct_name,TYPE) VALUES ("945-"+cCustname+"-"+cCustLoc,dt2,cFilename,UPPER(cCustname),"945")
			USE IN ftpedilog
		ENDIF
	ENDIF

	WAIT CLEAR
	WAIT WINDOW cCustFolder+" 945 Process complete..." TIMEOUT 1

*!* Create eMail confirmation message
	IF lTestmail
		tsendto = tsendtotest
		tcc = tcctest
	ENDIF

	cPTCount = ALLTRIM(STR(nPTCount))
	nPTCount = 0
	tsubject = cMailName+" 945 EDI File from TGF as of "+dtmail+" ("+cCustLoc+")"
	tattach = " "
	tmessage = "945 EDI Info from TGF for BOL# "+TRIM(cBOL)+CHR(13)
	tmessage = tmessage + "(File: "+cFilenameShort+")"+CHR(13)
	tmessage = tmessage + "(WO# "+cWO_NumStr+"), containing these "+cPTCount+" picktickets:"+CHR(13)
	tmessage = tmessage + cPTString + CHR(13)
	tmessage = tmessage + "has been created and will be transmitted ASAP."+CHR(13)+CHR(13)
	tmessage = tmessage + "If you have any questions, please eMail or call 732-750-9000 x217."

	IF lEmail
		DO FORM m:\dev\frm\dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
	ENDIF

	RELEASE ALL LIKE c_CntrlNum,c_GrpCntrlNum
	WAIT CLEAR
	WAIT WINDOW cMailName+" 945 EDI File output complete"+CHR(13)+"for BOL# "+TRIM(cBOL) &cTimeMsg

**********************************************************************************************

*!* Transfers files to correct output folders
	COPY FILE [&cFilenameHold] TO [&cFilenameArch]
*	SET STEP ON
	IF lBioFilesout AND lW06  && Added the W06 check to prevent empty 945s going out
		COPY FILE &cFilenameHold TO &cFilenameOut
		DELETE FILE &cFilenameHold
		SELECT temp945
		COPY TO "f:\3pl\data\temp945a.dbf"
		USE IN temp945
		SELECT 0
		USE "f:\3pl\data\temp945a.dbf" ALIAS temp945a
		SELECT 0
		USE "f:\3pl\data\pts_sent945.dbf" ALIAS pts_sent945
		APPEND FROM "f:\3pl\data\temp945a.dbf"
		USE IN pts_sent945
		USE IN temp945a
		DELETE FILE "f:\3pl\data\temp945a.dbf"
	ENDIF
	IF USED('parcel_carriers')
		USE IN parcel_carriers
	ENDIF

*!* asn_out_data()()

CATCH TO oErr
	IF lDoCatch
		SET STEP ON
		lEmail = .F.
		lCloseOutput = .F.

		IF !cErrMsg = "TOP LEVEL"
			DO ediupdate WITH ALLT(oErr.MESSAGE),.T.
		ENDIF

		tsubject = cCustname+" Error ("+TRANSFORM(oErr.ERRORNO)+") at "+TTOC(DATETIME())
		tattach  = ""
		tsendto = IIF(lTesting,tsendtotest,tsendtoerr)
		tcc= IIF(lTesting,tcctest,tccerr)

		IF lPGTesting
			tsendto="pgaidis@fmiint.com"
			tcc=""
		ENDIF

		tmessage = cCustname+" Error processing "+CHR(13)
		tmessage =tmessage+ "Try/CATCH Exeception Message:"+CHR(13)+;
			[  Error: ] + STR(oErr.ERRORNO) +CHR(13)+;
			[  LineNo: ] + STR(oErr.LINENO) +CHR(13)+;
			[  Message: ] + oErr.MESSAGE +CHR(13)+;
			[  Procedure: ] + oErr.PROCEDURE +CHR(13)+;
			[  Details: ] + oErr.DETAILS +CHR(13)+;
			[  StackLevel: ] + STR(oErr.STACKLEVEL) +CHR(13)+;
			[  LineContents: ] + oErr.LINECONTENTS+CHR(13)+;
			[  UserValue: ] + oErr.USERVALUE

		tsubject = "945 EDI Poller Error at "+TTOC(DATETIME())
		tattach  = ""
		DO FORM m:\dev\frm\dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
	ENDIF
FINALLY
*RELEASE tsendto,tfrom,tsubject,tcc,tattach,tmessage
	SET DELETED ON
	ON ERROR
ENDTRY


*** END OF CODE BODY

****************************
PROCEDURE close945
****************************
** Footer Creation

	STORE  "GE"+cfd+ALLTRIM(STR(nSTCount))+cfd+c_CntrlNum+csegd TO cString
	IF lTesting
		FPUTS(nFilenum,cString)
	ELSE
		FWRITE(nFilenum,cString)
	ENDIF

	STORE  "IEA"+cfd+"1"+cfd+PADL(c_CntrlNum,9,"0")+csegd TO cString
	IF lTesting
		FPUTS(nFilenum,cString)
	ELSE
		FWRITE(nFilenum,cString)
	ENDIF

	RETURN

****************************
PROCEDURE num_incr_isa
****************************

	IF !USED("serfile")
		USE ("f:\3pl\data\serial\"+cCustname+"945_serial") IN 0 ALIAS serfile
	ENDIF
	SELECT serfile
	IF lTesting AND lISAFlag
		nOrigSeq = serfile.seqnum
		lISAFlag = .F.
	ENDIF
	nISA_Num = serfile.seqnum
	c_CntrlNum = ALLTRIM(STR(serfile.seqnum))
	REPLACE serfile.seqnum WITH serfile.seqnum + 1 IN serfile
	SELECT outship
	RETURN

****************************
PROCEDURE num_incr_st
****************************

	IF !USED("serfile")
		USE ("f:\3pl\data\serial\"+cCustname+"945_serial") IN 0 ALIAS serfile
	ENDIF
	SELECT serfile
	IF lTesting AND lSTFlag
		nOrigGrpSeq = serfile.grpseqnum
		lSTFlag = .F.
	ENDIF
	c_GrpCntrlNum = ALLTRIM(STR(serfile.grpseqnum))
	REPLACE serfile.grpseqnum WITH serfile.grpseqnum + 1 IN serfile
	SELECT outship
	RETURN


****************************
PROCEDURE segmentget
****************************

	PARAMETER thisarray,lcKey,nLength

	FOR i = 1 TO nLength
		IF i > nLength
			EXIT
		ENDIF
		lnEnd= AT("*",thisarray[i])
		IF lnEnd > 0
			lcThisKey =TRIM(SUBSTR(thisarray[i],1,lnEnd-1))
			IF OCCURS(lcKey,lcThisKey)>0
				RETURN SUBSTR(thisarray[i],lnEnd+1)
				i = 1
			ENDIF
		ENDIF
	ENDFOR

	RETURN ""

****************************
PROCEDURE ediupdate
****************************
	PARAMETER cStatus,lIsError
	lDoCatch = .F.

	IF !lTesting
		SELECT EDI_TRIGGEr
		nRec = RECNO()
*		DO m:\dev\prg\edistatusmove WITH cBOL
		LOCATE
		IF !lIsError
			REPLACE processed WITH .T.,proc945 WITH .T.,file945 WITH cFilename,;
				fin_status WITH cStatus,errorflag WITH .F.,when_proc WITH DATETIME() ;
				isa_num WITH cISA_Num ;
				FOR EDI_TRIGGEr.bol = cBOL AND accountid = nAcctNum
		ELSE
			REPLACE processed WITH .T.,proc945 WITH .F.,file945 WITH "",;
				fin_status WITH cStatus,errorflag WITH .T. ;
				isa_num WITH cISA_Num ;
				FOR EDI_TRIGGEr.bol = cBOL AND accountid = nAcctNum
		ENDIF
		IF lCloseOutput
			=FCLOSE(nFilenum)
		ENDIF
	ENDIF

	IF lIsError AND lEmail
		tsubject = "945 Error in "+IIF(nAcctNum=6554,"Nolan","Bioworld")+" BOL "+TRIM(cBOL)+" (At PT "+cShip_ref+")"
		tattach = " "
		tsendto = tsendtoerr
		tcc = tccerr

		tmessage = "Generated from program: Bioworldpp_create, within EDI Outbound Poller"
		tmessage = tmessage + CHR(13) + "945 Processing for BOL# "+TRIM(cBOL)+"(WO# "+cWO_Num+"), produced this error: (Office: "+cOffice+"): "+cStatus+CHR(13)+"Check EDI_TRIGGER and re-run"
		IF cStatus = "MISS PT"
			tmessage = tmessage + CHR(13) + "This error means that this PT exists in OUTSHIP, but not in the Pickpack table. Please add this WO to SQL."
		ENDIF
		IF lPGTesting
			tsendto="pgaidis@fmiint.com"
			tcc=""
		ENDIF

		DO FORM m:\dev\frm\dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
	ENDIF

	IF USED('outship')
		USE IN outship
	ENDIF
	IF USED('outdet')
		USE IN outdet
	ENDIF

	IF USED('serfile')
		SELECT serfile
		IF lTesting
			REPLACE serfile.seqnum WITH nOrigSeq
			REPLACE serfile.grpseqnum WITH nOrigGrpSeq
		ENDIF
		USE IN serfile
	ENDIF

	IF USED('scacs')
		USE IN scacs
	ENDIF

	IF USED('mm')
		USE IN mm
	ENDIF

	IF !lTesting
		SELECT EDI_TRIGGEr
		LOCATE
	ENDIF
	RETURN

****************************
PROCEDURE cstringbreak
****************************
	cLen = LEN(ALLTRIM(cString))
	IF lTesting
		FPUTS(nFilenum,cString)
	ELSE
		FWRITE(nFilenum,cString)
	ENDIF
	RETURN

****************************
PROCEDURE valreturn
****************************
	PARAMETERS cIdentifier
	cReturned = "c"+cIdentifier
	RELEASE ALL LIKE &cReturned
	PUBLIC &cReturned
	nGetline = ATCLINE(cIdentifier,outdet.printstuff)
	dataline = MLINE(outdet.printstuff,nGetline)
	STORE SUBSTR(dataline,AT("*",dataline)+1) TO &cReturned
	RETURN &cReturned
ENDPROC
