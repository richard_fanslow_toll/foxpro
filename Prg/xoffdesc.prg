lparameters xdatabase, xaccountid, xofficeletter

if xaccountid=0
	return space(6)
endif

do case
case xdatabase="N"
	=seek(xaccountid,"account","accountid")
	xwhofficekey=account.njmod

case xdatabase="C"
	=seek(xaccountid,"account","accountid")
	do case
	case account.building="4"
		xwhofficekey="1"
	case account.building="7"
		xwhofficekey="2"
	case account.building="6"
		xwhofficekey="5"
	case account.building="5"
		xwhofficekey="6"
	case account.building="0"
		xwhofficekey="7"
	case account.building="1"
		xwhofficekey="8"
	endcase

case xdatabase="K"
	=seek(xaccountid,"account","accountid")
	xwhofficekey=account.kymod
	
otherwise
	xwhofficekey=xdatabase
endcase
 
if xofficeletter
	return xwhofficekey
else
	=seek(xwhofficekey,"whoffice","office")
	return whoffice.abbrev
endif
