lparameters xaccountid

xform="inv210"
xdy = ascan(oforms.iaforminstances,xform,1)
if xdy#0
	x3winmsg("You must close the invoice 210 screen before entering for another account")
	return .f.
endif

public ginvaccountid
ginvaccountid=xaccountid

oforms.doform(xform,'F',.t.)
