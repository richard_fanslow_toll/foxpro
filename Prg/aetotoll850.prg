**FILES ARE GRABBED FROM AEO'S FTP SITE VIA TASKPOLLER RUN OF "ftp_getfiles.exe AEO850"


xtest=.f.
**if xtest set to true later (in commented code below), xno997 will be .f. so file will be sent as needed
xno997=xtest

xindir="f:\ftpusers\AmericanEagle\850in\"
*xindir="c:\temp\aetestin\"

xoutdir=iif(xtest,"f:\ftpusers\toll\aeo\850test\","f:\ftpusers\toll\aeo\850out\")
*xoutdir="c:\temp\aetestout\"

x997dir="f:\ftpusers\AmericanEagle\850-997\"
**save 997s from 856 transmission from ICON directly to AE - mvw 06/08/12
x997in="f:\ftpusers\AmericanEagle\997in\"


**FTP into AEO to grab files
*!*	xftpaddr="testsite.ae.com" &&test site
*!*	xftpaddr="access.ae.com" &&live site
*!*	xusername="tollair"
*!*	xpassword="T0!!@ir"


**Begin Test Code
**To pickup test 850 and save to F:\FTPUSERS\AmericanEagle\850TEST
**Then send 997 to AE test site and send translated 850 to Icon's development folder

**1. turn off taskpoller on ftp2 
**2. Make sure no AEO-850-997 waiting to be processed in ftpjobs
**3. (this must be run on ftp2) Run f:\edirouting\proj\ftp_getfiles.exe AEO850-TEST
**4. Once a file exists in f:\ftpusers\AmericanEagle\850test\, run this prg with below uncommented (xindir, xoutdir and xtest code):
*!*	xindir="f:\ftpusers\AmericanEagle\850test\"
*!*	xoutdir="f:\ftpusers\toll\aeo\850test\"
*!*	xtest=.t.
**
**5. comment out above lines of code (xindir, xoutdir and xtest code)
**6. Once 997 files exist in f:\ftpusers\americaneagle\997out\ (may take a few minutes!), run below code (test trigger created)
*!*	xindir="f:\ftpusers\americaneagle\997out\"
*!*	xoutdir="f:\ftpusers\americaneagle\997out\send\"
*!*	xfilecnt=Adir(afiles,xindir+"*.*")
*!*	if xfilecnt>0
*!*		create cursor xfilelist (file c(100), x c(10), moddt C(8), modtm c(10), moddttm t(1))
*!*		append from array afiles
*!*		replace all moddttm with ctot(moddt+" "+modtm)

*!*		select * from xfilelist order by moddttm into cursor xfilelist
*!*		scan
*!*			xfilename = xindir+alltrim(xfilelist.file)

*!*			do while .t.
*!*				set date ymd
*!*				xoutfile="ae997-"+strtran(strtran(strtran(ttoc(datetime()),":","")," ",""),"/")+".dat"
*!*				set date american

*!*				xarchiveoutfile=xoutdir+"archive\"+xoutfile
*!*				if !file(xarchiveoutfile)
*!*					exit
*!*				endif
*!*			enddo

*!*			xoutfile=lower(xoutdir+xoutfile)
*!*			copy file &xfilename to &xoutfile
*!*			rename &xoutfile to &xoutfile &&make sure its lower, sometimnes copy changes to uppper
*!*		endscan

*!*		use in xfilelist

*!*		delete file f:\ftpusers\americaneagle\997out\*.*

*!*		use f:\edirouting\ftpjobs in 0
*!*		insert into ftpjobs (jobname, userid, jobtime) values ("AEO-850-997-TEST","FTP2",datetime())
*!*		use in ftpjobs
*!*	endif

**7. Restart taskpoller on FTP2

**End test code



**check if files exist in the inbound folder
xfilecnt=Adir(afiles,xindir+"*.dat")
if xfilecnt=0
	return
endif

xseparator=chr(13)+chr(10)
xcolcnt=25
xtollcustcode="AEO" &&temporarily entered as "PVH" for testing 

if empty(xtollcustcode) or xtollcustcode="AEO"
	xFrom="TGFSYSTEM"
	if xtollcustcode="AEO"
		xSubject="AEO 850 ICON Transalation May Have A Test Toll Customer Code!! Must Change Before Going Live!"
	else
		xSubject="AEO 850 ICON Transalation Cannot Be Run - Empty Toll Customer Code!!"
	endif
	xbody=""
	do form dartmail2 with "mwinter@fmiint.com",xfrom,xsubject," ",,xbody,"a"

	if empty(xtollcustcode)
		return
	endif
endif

set century on
set hours to 24

create cursor xfilelist (file c(100), x c(10), moddt C(10), modtm c(10), moddttm t(1))
append from array afiles
replace all moddttm with ctot(moddt+" "+modtm)

scan
	xfile=alltrim(xfilelist.file)
	xinfile=xindir+alltrim(xfilelist.file)
	xfs=filetostr(xinfile)

	if "ST*997*"$xfs
		**save in 997in in case we need them
		copy file &xinfile to &x997in.&xfile
	endif

	**need to account for possibility of having 997s and 850s in same file. if not 850s, delete. else, process normally - mvw 11/08/12
	if "ST*850*"$xfs
		**copy ONLY 850 files to the 997 directory so 997s are sent as needed - mvw 06/08/12
		if !xno997
			copy file &xinfile to &x997dir.&xfile
		endif
	else 
		**if just 997, delete
		delete file &xinfile
		delete in xfilelist
	endif
endscan

use f:\sysdata\qqdata\unloccode in 0

select 0
create cursor x850 (field1 c(254),recno n(14,3))
index on recno tag recno

xfilecreated=.f.
select * from xfilelist order by moddttm into cursor xfilelist
scan
	zap in x850

	**start with column headings
	xfilestr="order_no|order_split|order_status|client_cust_code|supplier_code|supplier_name|supplier_address_1|"+;
		"supplier_address_2|supplier_city|supplier_region_state|supplier_country|supplier_postcode|confirm_number|"+;
		"confirm_date|exw_required_by|delivery_required_by|description|order_date_time|order_value|order_currency_code|"+;
		"exchange_rate|incoterms|additional_terms|transport_mode|container_mode|milestone_tracking_date_exw|"+;
		"milestone_tracking_date_etd|milestone_tracking_date_eta|custom_milestone_est1|custom_milestone_act1|"+;
		"custom_milestone_est2|custom_milestone_act2|custom_milestone_est3|custom_milestone_act3|custom_milestone_est4|"+;
		"custom_milestone_act4|sending_agent|sending_agent_name|origin_port|destination_port|custom_text1|custom_text2|"+;
		"custom_text3|custom_text4|custom_text5|custom_date1|custom_date2|custom_decimal1|custom_decimal2|custom_decimal3|"+;
		"custom_decimal4|custom_decimal5|custom_flag1|custom_flag2|custom_flag3|custom_flag4|custom_flag5|contact1|"+;
		"contact2|country_code|notes|line_no|subline_no|linesplit_no|product|line_description|qty_ordered|innerpacks|"+;
		"outerpacks|unit_of_measure|itemprice|lineprice|linestatus|required_date|part_attrib1|part_attrib2|part_attrib3|"+;
		"line_custom_attrib1|line_custom_attrib2|line_custom_attrib3|line_custom_attrib4|line_custom_attrib5|"+;
		"line_custom_date1|line_custom_date2|line_custom_date3|line_custom_date4|line_custom_date5|line_custom_decimal1|"+;
		"line_custom_decimal2|line_custom_decimal3|line_custom_decimal4|line_custom_decimal5|line_custom_flag1|"+;
		"line_custom_flag2|line_custom_flag3|line_custom_flag4|line_custom_flag5|line_custom_text1|container_number|"+;
		"container_packing_order|country_of_origin|dg_substance|dg_flashpoint|weight|volume|weight_type|volume_type|"+;
		"special_instructions|additional_information|delivery_port|address_code|delivery_address_name|"+;
		"delivery_address_address1|delivery_address_address2|delivery_address_city|delivery_address_state|"+;
		"delivery_address_country|delivery_address_postcode"+chr(13)+chr(10)
	xhdrlen=len(xfilestr)
	xfilename = xindir+alltrim(xfilelist.file)
	xarchiveinfile = xindir+"archive\"+alltrim(xfilelist.file)

	**no need for temp file, etc if terminator segment is already chr(13)+chr(10), looks at pvhtotoll850.prg for that code
	xfile=filetostr(xfilename)
	xchar=substr(xfile,106,1)
	xfile=strtran(xfile,xchar,chr(13)+chr(10))

	xtmpfile=xindir+"tmp"+strtran(strtran(strtran(ttoc(datetime()),"/",""),":","")," ","")+".tmp"
	strtofile(xfile,xtmpfile)

	xchar=chr(13)+chr(10)

	select x850

	**added try catch in attempt to avoid "file access denied" error, believe its coming 
	**  from the append if the file has yet to be completely ftp'd - mvw 06/29/12
	xerror=.f.
	try
		append from "&xtmpfile" type delimited with character &xchar
	catch
		xerror=.t.
	endtry

	replace all field1 with alltrim(upper(field1)), recno with recno()
	locate

	delete file &xtmpfile

	**default delivery port UNLOC code required for Icon - default to LAX
 	xdelivport="USNYC"

	**predefine new fields (priority/hold flag, red vendor flag, vendor number and fish/wildlife flag) in case not found in file - mvw 08/29/12
	store "" to xpriorityhold, xredvendor, xvendornum, xwildlife

	**keep track of previous po line as we have seen instances where PVH has sent the same line number 
	**	multiple times, each containing different skus - mvw 02/14/112
	xprevlineno=""
	**need sln to be defined here now in case i need to combine 2 identical line numbers for same po - mvw 2/14/12
	xslncnt=1

	**string to hold the data for each po as we need to only add to xfilestr once a po is complete - mvw 01/16/13
	xpostr=""

	xerror=.f.
	do while !eof()
		do case
		case left(field1,3)="ISA"
		case left(field1,2)="GS"
		case left(field1,2)="ST"
			**if encounter a 997, skip to next 850 "ST" seg - mvw 11/08/12
			if substr(field1,4,3)="997"
				locate for left(field1,2)="ST" and substr(field1,4,3)="850" while !eof()
				loop
			endif

		case left(field1,3)="BEG"
			xlevel="O"
			xaction=alltrim(substr(field1,at("*",field1,1)+1,at("*",field1,2)-(at("*",field1,1)+1)))
			**we expect only a 05 or 06 (replace or "confirmation" which is new for AE per spec)
			**leave 00 just in case
			xaction=iif(inlist(xaction,"00","05","06"),"PLC","")
			if empty(xaction)
				xfrom ="TGFSYSTEM"
				xmessage = "Error in file '"+xfilename+"' for PO '"+xponum+"': Order status has unexpected value - expect only '00','05' or '06'. Entire file cannot be processed."
				do form dartmail2 With "mwinter@fmiint.com",xFrom,"AEO 850 Translation Error"," ","",xmessage,"A"

				xerror=.t.
				exit
			endif

			xponum=substr(field1,at("*",field1,3)+1,at("*",field1,4)-(at("*",field1,3)+1))
			xorderdt=right(alltrim(field1),len(alltrim(field1))-at("*",field1,5))
			**check that its a valid date (within 10 years)
			if !between(ctod(substr(xorderdt,5,2)+"/"+substr(xorderdt,7,2)+"/"+left(xorderdt,4)),gomonth(date(),-(10*12)),gomonth(date(),10*12))
				xorderdt=transform(year(date()))+"-"+padl(transform(month(date())),2,"0")+"-"+padl(transform(day(date())),2,"0")
			else
				xorderdt=left(xorderdt,4)+"-"+substr(xorderdt,5,2)+"-"+substr(xorderdt,7,2)
			endif

			**need to make sure we haven't received the same po twice in the same file - in some cases they will send it twice (different ISA envelopes)! 
			**  use the latter instance - mvw 10/09/13
			xrecno=recno()
			skip
			locate for field1="BEG" and "*"+xponum+"*" $ field1 while !eof()
			**if found below, skip to next ST*850 rec
			if found()
				go xrecno
				locate for left(field1,2)="ST" and substr(field1,4,3)="850" while !eof()
				loop
			endif

			go xrecno
			**end changes for po multiple times within file

			**taken from TD5 in the PO1 loop for AE
*			xshipvia="SEA" &&hardcode SEA bc not provided
			**reset for every po change - mvw 02/14/12
			xprevlineno=""
			xpostr=""

		case left(field1,3)="CUR"
			xcurrency=right(alltrim(field1),len(alltrim(field1))-at("*",field1,2))

		case left(field1,3)="REF" and substr(field1,5,2)="DP"
			xdept=alltrim(substr(field1,at("*",field1,2)+1,at("*",field1,3)-(at("*",field1,2)+1)))
			if occurs("*",field1)=2
				xdept=right(alltrim(field1),len(alltrim(field1))-at("*",field1,2))
			endif

		**new REF segment for priority/hold flag - mvw 08/29/12
		**changed from "PH" to "PC" per Rebecca's email/spec from 02/20/14
*		case left(field1,3)="REF" and substr(field1,5,2)="PH"
		case left(field1,3)="REF" and substr(field1,5,2)="PC"
			xpriorityhold=right(alltrim(field1),len(alltrim(field1))-at("*",field1,2))

		case left(field1,3)="DTM" and substr(field1,5,3)="011" and xlevel="O"
			**start date, hand over in the "AA" column unless Toll disagrees
			xstartdt=right(alltrim(field1),len(alltrim(field1))-at("*",field1,2))

			**check that its a valid date (within 10 years), if not enter as today's date
			if !between(ctod(substr(xstartdt,5,2)+"/"+substr(xstartdt,7,2)+"/"+left(xstartdt,4)),gomonth(date(),-(10*12)),gomonth(date(),10*12))
				xstartdt=transform(year(date()))+"-"+padl(transform(month(date())),2,"0")+"-"+padl(transform(day(date())),2,"0")
			else
				xstartdt=left(xstartdt,4)+"-"+substr(xstartdt,5,2)+"-"+substr(xstartdt,7,2)
			endif

		case left(field1,3)="DTM" and substr(field1,5,3)="061" and xlevel="O"
			**cancel date
			xexwreqdt=right(alltrim(field1),len(alltrim(field1))-at("*",field1,2))

			**check that its a valid date (within 10 years), if not enter as today's date
			if !between(ctod(substr(xexwreqdt,5,2)+"/"+substr(xexwreqdt,7,2)+"/"+left(xexwreqdt,4)),gomonth(date(),-(10*12)),gomonth(date(),10*12))
				xexwreqdt=transform(year(date()))+"-"+padl(transform(month(date())),2,"0")+"-"+padl(transform(day(date())),2,"0")
			else
				xexwreqdt=left(xexwreqdt,4)+"-"+substr(xexwreqdt,5,2)+"-"+substr(xexwreqdt,7,2)
			endif

		case left(field1,2)="N1" and substr(field1,4,2)="BY"
			**buyer name and code need to be sent back in the asn, save in contact1 (BF) and contact2 (BG)
			xbuyercode=right(alltrim(field1),len(alltrim(field1))-at("*",field1,4))
			xbuyername=alltrim(substr(field1,at("*",field1,2)+1,at("*",field1,3)-(at("*",field1,2)+1)))

		case left(field1,2)="N1" and substr(field1,4,2)="ST"
			xshiptocode=right(alltrim(field1),len(alltrim(field1))-at("*",field1,4))
			xshiptoname=alltrim(substr(field1,at("*",field1,2)+1,at("*",field1,3)-(at("*",field1,2)+1)))

			skip
			store "" to xshiptoaddr1, xshiptoaddr2
			if left(field1,2)="N3" &&should have N3 but if not, leave blank
				if occurs("*",field1)=1
					xshiptoaddr1=right(alltrim(field1),len(alltrim(field1))-at("*",field1,1))
				else
					xshiptoaddr1=alltrim(substr(field1,at("*",field1,1)+1,at("*",field1,2)-(at("*",field1,1)+1)))
					xshiptoaddr2=right(alltrim(field1),len(alltrim(field1))-at("*",field1,2))
				endif
				skip
			endif

			if left(field1,2)="N3" &&if 2nd N3, skip past
				skip
			endif

			store "" to xshiptocity, xshiptostate, xshiptozip, xshiptocountry
			if left(field1,2)#"N4"
				skip -1 &&if for some reason not provided, skip back bc there is a skip at end of loop
			else
				xshiptocity=alltrim(substr(field1,at("*",field1,1)+1,at("*",field1,2)-(at("*",field1,1)+1)))
				**if N4 comes only with city, need to use the right() function below - applies to other fields below as well
				if occurs("*",field1)=1
					xshiptocity=right(alltrim(field1),len(alltrim(field1))-at("*",field1,1))
				endif
				
				xshiptostate=alltrim(substr(field1,at("*",field1,2)+1,at("*",field1,3)-(at("*",field1,2)+1)))
				if occurs("*",field1)=2
					xshiptostate=right(alltrim(field1),len(alltrim(field1))-at("*",field1,2))
				endif
				xshiptostate=iif(inlist(xshiptostate,"N/","N\","NA"),"",xshiptostate)

				xshiptozip=left(alltrim(substr(field1,at("*",field1,3)+1,at("*",field1,4)-(at("*",field1,3)+1))),5)
				if occurs("*",field1)=3
					xshiptozip=left(right(alltrim(field1),len(alltrim(field1))-at("*",field1,3)),5)
				endif
				xshiptozip=iif(inlist(upper(xshiptozip),"N/A","NA","N\A"),"",xshiptozip)

				**if doesnt contain 4 *'s, then country is not provided. if so default to US - mvw 11/29/11
				xshiptocountry=iif(occurs("*",field1)<4,"US",right(alltrim(field1),len(alltrim(field1))-at("*",field1,4)))
				**change to 3-digit iso country (unloccode.isocountry) - mvw 12/07/11
				=seek(xshiptocountry,"unloccode","country")
				xshiptocountry=unloccode.isocountry
			endif

		case left(field1,2)="N1" and substr(field1,4,2)="CT"
			skip
			if left(field1,2)#"N4"
				skip -1 &&if for some reason not provided, skip back bc there is a skip at end of loop
			else
				xorigincountry=right(alltrim(field1),len(alltrim(field1))-at("*",field1,4))

				**change to 3-digit iso country (unloccode.isocountry) - mvw 12/07/11
				=seek(xorigincountry,"unloccode","country")
				xorigincountry=unloccode.isocountry
			endif

		**new REF segment for red vendor flag - mvw 08/29/12
		case left(field1,3)="REF" and substr(field1,5,2)="VF"
			xredvendor=right(alltrim(field1),len(alltrim(field1))-at("*",field1,2))

		**start item level
		case left(field1,3)="PO1"
			xlevel="I"
			xlineno=alltrim(substr(field1,at("*",field1,1)+1,at("*",field1,2)-(at("*",field1,1)+1)))
			**remove the " - ", needs to be numeric going into ICON
			**NO LONGER REMOVE THE " - " per Andi - 04/18/12
*			xlineno=strtran(strtran(xlineno,"-","")," ","")

			**if duplicate line number for a given po, need to correct - mvw 02/14/12
			if xprevlineno==xlineno
				**error out if duplicate lines - send email, move to next po, continue with file - mvw 01/16/13
				xfrom ="TGFSYSTEM"
				xmessage = "Error in file '"+xfilename+"' for PO '"+xponum+"' (Origin - "+xorigincountry+"): At least 1 duplicate PO line - "+xlineno+;
					chr(13)+chr(13)+"Please note, all other POs in this file have been processed."
				do form dartmail2 With "mwinter@fmiint.com, rebecca.h.ma@tollgroup.com",xFrom,"AEO 850 Translation Error"," ","",xmessage,"A"

				xpostr=""
				**cleaner to go to ST element and loop here rather than run thru the rest of this loop - mvw 06/28/13
*!*					locate for left(field1,2)="SE" while !eof()
*!*					xslncnt=xslncnt+1
				locate for left(field1,2)="ST" while !eof()
				loop
			else
				xslncnt=1
			endif

			xprevlineno=xlineno

			xstyle=alltrim(substr(field1,at("*",field1,7)+1,at("*",field1,8)-(at("*",field1,7)+1)))
			xqty=alltrim(substr(field1,at("*",field1,2)+1,at("*",field1,3)-(at("*",field1,2)+1)))
			xsku=alltrim(substr(field1,at("*",field1,11)+1,at("*",field1,12)-(at("*",field1,11)+1)))
			if occurs("*",field1)=11
				xsku=right(alltrim(field1),len(alltrim(field1))-at("*",field1,11))
			endif

			xunitprice="0.0000"

			**new field in PO113 for vendor number - mvw 08/29/12
			xvendornum=alltrim(substr(field1,at("*",field1,13)+1,at("*",field1,14)-(at("*",field1,13)+1)))
			if occurs("*",field1)=13
				xvendornum=right(alltrim(field1),len(alltrim(field1))-at("*",field1,13))
			endif

			**new field in PO115 for fish/wildlife flag - mvw 08/29/12
			xwildlife=alltrim(substr(field1,at("*",field1,15)+1,at("*",field1,16)-(at("*",field1,15)+1)))
			if occurs("*",field1)=15
				xwildlife=right(alltrim(field1),len(alltrim(field1))-at("*",field1,15))
			endif

			**cannot send a value of "N" to a custom flag field in ICON as there is reserved logic to handle the value "N", per Andi 09/03/12
			**change it to "NO"
			xwildlife=iif(upper(xwildlife)="N","NO",xwildlife)

		case left(field1,3)="PID"
			if occurs("*",field1)>5
				xdescription=alltrim(substr(field1,at("*",field1,5)+1,at("*",field1,6)-(at("*",field1,5)+1)))
			else
				xdescription=right(alltrim(field1),len(alltrim(field1))-at("*",field1,5))
			endif

		case left(field1,3)="FOB"
			**valid ICON inco terms
			**EXW	EX WORKS
			**FCA	FREE CARRIER
			**FAS	FREE ALONGSIDE SHIP
			**FOB	FREE ON BOARD
			**CFR	COST AND FREIGHT
			**CIF	COST INSURANCE AND FREIGHT
			**CPT	CARRIAGE PAID TO
			**CIP	CARRIAGE AND INSURANCE PAID TO
			**DAF	DELIVERED AT FRONTIER
			**DES	DELIVERED EX SHIP
			**DEQ	DELIVERED EX QUAY
			**DDU	DELIVERED DUTY UNPAID
			**DDP	DELIVERED DUTY PAID
			**PPD	PREPAID
			**DAP	DELIVERY AT PLACE
			**COL	COLLECT

			xincoterms=right(alltrim(field1),len(alltrim(field1))-at("*",field1,5))

			**if invalid inco term, set to default - mvw 10/11/12
			if !inlist(xincoterms,"EXW","FCA","FAS","FOB","CFR","CIF","CPT","CIP","DAF","DES","DEQ","DDU","DDP","PPD","DAP","COL")
				xincoterms="FOB"
			endif

		case left(field1,3)="DTM" and substr(field1,5,3)="061" and xlevel="I"
			xreqdt=right(alltrim(field1),len(alltrim(field1))-at("*",field1,2))

			**check that its a valid date (within 10 years), if not enter as today's date
			if !between(ctod(substr(xreqdt,5,2)+"/"+substr(xreqdt,7,2)+"/"+left(xreqdt,4)),gomonth(date(),-(10*12)),gomonth(date(),10*12))
				xreqdt=transform(year(date()))+"-"+padl(transform(month(date())),2,"0")+"-"+padl(transform(day(date())),2,"0")
			else
				xreqdt=left(xreqdt,4)+"-"+substr(xreqdt,5,2)+"-"+substr(xreqdt,7,2)
			endif

		case left(field1,3)="TD5"
			**sent as A, S or J (motor), only account for A or S bc thats all icon accepts
			xshipvia=right(alltrim(field1),len(alltrim(field1))-at("*",field1,4))
			xshipvia=iif(xshipvia="A","AIR","SEA")

		case left(field1,2)="N1" and substr(field1,4,2)="SU"
			xsuppliercode=right(alltrim(field1),len(alltrim(field1))-at("*",field1,4))
			xsuppliername=alltrim(substr(field1,at("*",field1,2)+1,at("*",field1,3)-(at("*",field1,2)+1)))

			skip
			store "" to xsupplieraddr1, xsupplieraddr2
			if left(field1,2)="N3" &&should have N3 but if not, leave blank
				if occurs("*",field1)=1
					xsupplieraddr1=right(alltrim(field1),len(alltrim(field1))-at("*",field1,1))
				else
					xsupplieraddr1=alltrim(substr(field1,at("*",field1,1)+1,at("*",field1,2)-(at("*",field1,1)+1)))
					xsupplieraddr2=right(alltrim(field1),len(alltrim(field1))-at("*",field1,2))
				endif
				skip
			endif

			if left(field1,2)="N3" &&if 2nd N3, skip past
				skip
			endif

			store "" to xsuppliercity, xsupplierstate, xsupplierzip, xsuppliercountry, xfobloccode
			if left(field1,2)#"N4"
				skip -1 &&if for some reason not provided, skip back bc there is a skip at end of loop
			else
				xsuppliercity=alltrim(substr(field1,at("*",field1,1)+1,at("*",field1,2)-(at("*",field1,1)+1)))
				**if N4 comes only with city, need to use the right() function below - applies to other fields below as well
				if occurs("*",field1)=1
					xsuppliercity=right(alltrim(field1),len(alltrim(field1))-at("*",field1,1))
				endif

				xsupplierstate=alltrim(substr(field1,at("*",field1,2)+1,at("*",field1,3)-(at("*",field1,2)+1)))
				if occurs("*",field1)=2
					xsupplierstate=right(alltrim(field1),len(alltrim(field1))-at("*",field1,2))
				endif
				xsupplierstate=iif(inlist(xsupplierstate,"N/","N\","NA"),"",xsupplierstate)

				xsupplierzip=left(alltrim(substr(field1,at("*",field1,3)+1,at("*",field1,4)-(at("*",field1,3)+1))),5)
				if occurs("*",field1)=3
					xsupplierzip=left(right(alltrim(field1),len(alltrim(field1))-at("*",field1,3)),5)
				endif
				xsupplierzip=iif(inlist(upper(xsupplierzip),"N/A","NA","N\A"),"",xsupplierzip)

				xsuppliercountry=alltrim(substr(field1,at("*",field1,4)+1,at("*",field1,5)-(at("*",field1,4)+1)))
				if occurs("*",field1)=4
					xsuppliercountry=right(alltrim(field1),len(alltrim(field1))-at("*",field1,4))
				endif

				select unloccode
				locate for name=xsuppliercity and country=xsuppliercountry
				do case
				case found()
					xfobloccode=alltrim(country)+alltrim(unloccode)

				**As per Chris Lind, we're only getting Bangladesh (BD) freight now, if city not found default to main port CHITTAGONG
				case xsuppliercountry="BD"
					xfobloccode="BDCGP"
				**otherwise default to GUANGZHOU, CHINA
				otherwise
					xfobloccode="CNXSA"
				endcase

				select x850

				**change to 3-digit iso country (unloccode.isocountry) - mvw 12/07/11
				=seek(xsuppliercountry,"unloccode","country")
				xsuppliercountry=unloccode.isocountry
			endif

		**REF*HS should come before SLN, just a safeguard
		case (left(field1,3)="REF" and substr(field1,5,2)="HS") or left(field1,3)="SLN"
			**do while loop for multiple sln segs per po line
			**xslncnt reset now done at PO1 seg in case of need to combine identical line numbers for po - mvw 02/14/12
			*xslncnt=1
			do while .t.
				**xpartattrib1, xpartattrib2 not used for AEO
				store "" to xpartattrib1, xpartattrib2

				**predefine xpartattrib3-xpartattrib6, there are only 4 place hoilders for HTS numbers
				store "" to xpartattrib3, xpartattrib4, xpartattrib5, xpartattrib6
				if (left(field1,3)="REF" and substr(field1,5,2)="HS")
					xcnt=3
					do while (left(field1,3)="REF" and substr(field1,5,2)="HS")
						xvar="xpartattrib"+transform(xcnt)
						&xvar=right(alltrim(field1),len(alltrim(field1))-at("*",field1,2))
						xcnt=xcnt+1
						skip
					enddo
				endif

				**moved xhdrstr definition here from the top of the PO1 case because xshipvia isn't defined until after PO1 in the TD5 seg
				xhdrstr=alltrim(xponum)+"|"+; &&A
					"|"+; &&column(s) B empty
					xaction+"|"+; &&C
					xtollcustcode+"|"+;&&D
					xsuppliercode+"|"+; &&E
					xsuppliername+"|"+; &&F
					xsupplieraddr1+"|"+; &&G
					xsupplieraddr2+"|"+; &&H
					xsuppliercity+"|"+; &&I
					xsupplierstate+"|"+; &&J
					xsuppliercountry+"|"+; &&K
					xsupplierzip+"|"+; &&L
					"||"+; &&column(s) M-N empty
					xstartdt+"|"+; &&O - placed start date here per andi - 04/18/12
					"||"+; &&column(s) P-Q empty
					xorderdt+"|"+; &&R
					"|"+; &&column(s) S empty
					xcurrency+"|"+; &&T
					"|"+; &&column(s) U empty
					xincoterms+"|"+; &&V
					"|"+; &&column(s) W empty
					xshipvia+"|"+; &&X
					"||"+; &&column(s) Y-Z is empty
					"|"+; &&AA - removed xstartdt, moved to "O" per andi - mvw 04/18/12
					"|"+; &&column(s) AB is empty
					xexwreqdt+"|"+; &&AC
					"|||||||||"+; &&column(s) AD-AL empty
					xfobloccode+"|"+; &&AM
					xdelivport+"|"+; &&AN
					xdept+"|"+; &&AO
					xbuyername+"|"+; &&AP
					xbuyercode+"|"+; &&AQ
					xpriorityhold+"|"+; &&AR - added mvw 08/29/12
					xredvendor+"|"+; &&AS - added mvw 08/29/12
					"||||||||||||"+; &&column(s) AR-BE empty
					"|"+; &&BF - moved buyer name to AP per Andi - mvw 04/17/12
					"|"+; &&BG - moved buyer code to AQ per Andi - mvw 04/17/12
					"||" &&column(s) BH-BI empty


				**only add po line if the qty>0, AE sends cancelled lines with a 0 qty - mvw 05/09/12
				**only send BANGLADESH pos, thats all Toll handles at this point - mvw 10/03/12
				**updated to send files for countries Philippines (PH), Sri Lanka (LK), Pakistan (PK) and Kenya (KE) per Rebecca - mvw 04/09/13
				**send ALL pos (even if not bangladesh) for test files - mvw 11/13/12
				if (inlist(xorigincountry,"BD","BGD","PH","PHL","LK","LKA","PK","PAK","KE","KEN") or xtest) and val(xqty)>0
					**removed the "-" from the lineno as ICON could not accept it. Now line number "001" will go as "0011" 
					**  for the 1st SLN, "0012" for the 2nd and so on. The right-most character will need to be stripped 
					**  for the 856 going back to PVH - mvw 01/12/12
					*xlineno+"-"+transform(xslncnt)+"|" &&BJ
					**no need to add slncnt to po line as there should only be 1 sln per po1 seg
					*xlineno+padl(transform(xslncnt),2,"0")+"|" &&BJ

					**update xpostr now, not xfilestr... only add to xfilestr once a po is complete in case of errors (dline dups, etc) within a po - mvw 01/16/13
					xpostr=xpostr+xhdrstr+;
						xlineno+"|"+; &&BJ
						"||"+; &&column(s) BK-BL empty
						xstyle+"|"+; &&BM
						xdescription+"|"+; &&BN
						transform(xqty)+"|"+; &&BO
						"|||"+; &&column(s) BP-BR empty
						transform(xunitprice)+"|"+; &&BS
						"|"+; &&column(s) BT empty
						xaction+"|"+; &&BU
						xreqdt+"|"+; &&BV
						xsku+"|"+; &&BW
						"|"+; &&BX - empty, only need 2 attributes style and sku
						"|"+; &&BY - empty, had held the 1st HTS # - mvw 02/10/12
						xsku+"|"+; &&BZ - copied sku to BZ, moved xpartattrib3 to CA, etc per Andi - mvw 04/17/12
						xpartattrib3+"|"+; &&CA
						xpartattrib4+"|"+; &&CB
						xpartattrib5+"|"+; &&CC
						xpartattrib6+"|"+; &&CD
						"||||||||||"+; &&column(s) CE-CN empty
						xvendornum+"|"+; &&CO - added mvw 08/29/12
						xwildlife+"|"+; &&CP - added mvw 08/29/12
						"||||||"+; &&column(s) CQ-CV empty
						xorigincountry+"|"+; &&CW
						"||||||||"+;&&column(s) CX-DE empty
						xdelivport+"|"+; &&DF
						xshiptocode+"|"+; &&DG
						xshiptoname+"|"+; &&DH
						xshiptoaddr1+"|"+; &&DI
						xshiptoaddr2+"|"+; &&DJ
						xshiptocity+"|"+; &&DK
						xshiptostate+"|"+; &&DL
						xshiptocountry+"|"+; &&DM
						xshiptozip+; &&DN
						chr(13)+chr(10)
				endif

				**segment should now be SLN, skip thru it bc we need no info from it
				**only skip if it is SLN seg, seeing files missing SLN which is a problem - mvw 01/18/13
				if left(field1,3)="SLN"
					skip
				endif

				if !((left(field1,3)="REF" and substr(field1,5,2)="HS") or left(field1,3)="SLN")
					exit
				endif

				xslncnt=xslncnt+1
			enddo

			if xerror
				exit
			endif

			skip -1

		case left(field1,3)="CTT"
		case left(field1,3)="SE"
			**add xpostr to xfilestr once a po is complete in case of errors (line dups, etc) within a po - mvw 01/16/13
			xfilestr=xfilestr+xpostr
		endcase

		select x850
		skip
	enddo

	select x850
	locate

	**if xfilestr only contains header info, something went wrong! - mvw 07/05/12
	**it may now be that all the pos in this file were Non-Bangladesh pos, which we do not send... dont treat as error, just delete file and move on - mvw 10/03/12
*!*		if len(xfilestr)=xhdrlen
*!*			xerror=.t.
*!*		endif

	if !xerror
		**if pos from countries we do business with exist (ie, Bangladesh (list of countries above)), send file... else just delete the file - mvw 10/03/12
		if len(xfilestr)>xhdrlen
			do while .t.
				set date ymd
				xoutfile="PO_TGFDI_"+strtran(strtran(strtran(ttoc(datetime()),":","")," ",""),"/")+xtollcustcode+".txt"
				set date american

				xarchiveoutfile=xoutdir+"archive\"+xoutfile
				if !file(xarchiveoutfile)
					exit
				endif
			enddo

			xoutfile=xoutdir+xoutfile
			strtofile(xfilestr,xoutfile)
			strtofile(xfilestr,xarchiveoutfile)

			xfilecreated=.t.
		endif

		copy file &xfilename to &xarchiveinfile
		delete file &xfilename
	endif
endscan

if xfilecreated
	**create an ftpjob
	use f:\edirouting\ftpjobs in 0
	xjob=iif(xtest,"AEO-TEST-850-TO-ICON","AEO-850-TO-ICON")
	insert into ftpjobs (jobname, userid, jobtime) values (xjob,"AEO850",datetime())
	use in ftpjobs
endif

use in x850
use in xfilelist
use in unloccode
