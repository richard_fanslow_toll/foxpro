*!*  IF (lTesting AND DATETIME()>DATETIME(2016,04,06,21,00,00)) OR INLIST(cMBOL,'04907316521118745','04907316521086112','04907316521086129')
*!*  	RETURN
*!*  ENDIF

IF lTesting
	RETURN
ENDIF

*ASSERT .f.
IF !EMPTY(cMBOL)
	lcQuery = [select * from edioutlog where accountid = ]+TRANSFORM(nAcctNum)+[ and mbol = ']+cMBOL+[']
	xsqlexec(lcQuery,"p1",,"stuff")
	LOCATE
	IF !EOF()
	IF USED("p1")
		USE IN "p1"
	endif
		RETURN
	ENDIF
	IF USED("p1")
		USE IN p1
	ENDIF
ENDIF

lcQuery = [select * from edioutlog where accountid = ]+TRANSFORM(nAcctNum)+[ and isanum = ']+cISA_Num+[']
xsqlexec(lcQuery,"p1",,"stuff")

STORE "" TO m.confirmdt,m.confirmtm,m.comfirmtm  && Last variable added to cover incorrect spelling in SQL table.

lNormalASN = .F.
LOCATE
IF EOF()
	USE IN p1
	useca("edioutlog","stuff")  && Creates updatable ASN/943 cursor

	SELECT edioutlog
	SCATTER MEMVAR MEMO BLANK
	m.eoutlogid = dygenpk("edioutlog","whall")
	m.isanum     =  cISA_Num
	m.accountid  = nAcctNum
	m.office     = cOffice
	m.mod        = cMod
	m.editype    = cEDIType
	m.fileformat =  IIF(nAcctNum = 6059,"CSV","EDI")
	m.wo_nums    = cWO_NumList
	m.mbol       = cMBOL
	m.bol        = cBOL
	m.parcel     = lParcelType
	m.createdt   = DATETIME()
	m.confirmtm  = empty2nul(m.confirmtm)
	m.comfirmtm  = empty2nul(m.comfirmtm)  && This variable added to cover incorrect spelling in SQL table.
	m.filepath   = lcPath
	m.comments   = "945 CREATED"
	m.filename   = cFilenameShort
	IF !EMPTY(cMBOL)
		IF !EMPTY(cSubBOLDetail)
			m.detail = cSubBOLDetail
			m.bol = ""
		ELSE
			m.detail = ""
		ENDIF
	ELSE
		m.detail = ""
	ENDIF
	m.edidata = ""
	INSERT INTO edioutlog FROM MEMVAR

	TRY

		IF (EMPTY(cMBOL) OR !EMPTY(cSubBOLDetail)) AND !INLIST(nAcctNum,4610,4694)
			APPEND MEMO edidata FROM &cFilenameArch
		ELSE
			APPEND MEMO edidata FROM &cFilenameHold
		ENDIF

		SELECT edioutlog
		SCATTER MEMVAR MEMO
		INSERT INTO F:\wh\edidata\edioutlogdbf FROM MEMVAR

		tu("edioutlog")
		USE IN edioutlog
		lNormalASN = .T.

	CATCH TO oErr
		IF !lNormalASN
			ASSERT .F. MESSAGE "In Catch section of asn_data_out..."
			SET STEP ON
			tsubject2 = "EDIOUTLOG Upload [asn_data_out] Error ("+TRANSFORM(oErr.ERRORNO)+") at "+TTOC(DATETIME())
			tsendto2  = "joe.bianchi@tollgroup.com"
			tcc2 = "paul.gaidis@tollgroup.com"
			tfrom2 = "Toll EDI Operations <toll-edi-ops@tollgroup.com>"
			STORE "" TO tattach2
			tmessage = "EDIOUTLOG Value Insert Error, Mod "+cMod+", Account # "+ALLTRIM(STR(nAcctNum))
			lcSourceMachine = SYS(0)
			lcSourceProgram = "asn_out_data"

			tmessage =tmessage+ "Try/CATCH Exeception Message:"+CHR(13)+;
				[  Error: ] + STR(oErr.ERRORNO) +CHR(13)+;
				[  LineNo: ] + STR(oErr.LINENO) +CHR(13)+;
				[  Message: ] + oErr.MESSAGE +CHR(13)+;
				[  Procedure: ] + oErr.PROCEDURE +CHR(13)+;
				[  Details: ] + oErr.DETAILS +CHR(13)+;
				[  StackLevel: ] + STR(oErr.STACKLEVEL) +CHR(13)+;
				[  LineContents: ] + oErr.LINECONTENTS+CHR(13)+;
				[  UserValue: ] + oErr.USERVALUE+CHR(13)+;
				[  Computer:  ] +lcSourceMachine+CHR(13)+;
				[  Program:   ] +lcSourceProgram
			DO FORM m:\dev\frm\dartmail2 WITH tsendto2,tfrom2,tsubject2,tcc2,tattach2,tmessage2,"A"
		ENDIF
	ENDTRY
ENDIF
