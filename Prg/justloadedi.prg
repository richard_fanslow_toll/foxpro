For i=1 To Alen(a856)
  Scatter Memvar Blank
  xend = At(delimchar,a856[i])
  xbeg=At(delimchar,a856[i])+2

  If !xend >1
    m.segment=a856[i]
    Insert Into x856 From Memvar
    Loop
  Else
    m.segment=Left(a856[i],xbeg-3)
  Endif

*		WAIT WINDOW AT 10,10 "Importing record# "+STR(i)+"      Segment: "+m.segment NOWAIT

  For j=2 To Occurs(delimchar,a856[i])
    jj=Ltrim(Str(j-1))
    xend=At(delimchar,a856[i],j)
    m.f&jj=Substr(a856[i],xbeg-1,xend-xbeg+1)
    m.sf&jj = xend-xbeg+1
    xbeg=xend+2
  Endfor

  jj=Ltrim(Str(j-1))
  m.f&jj=Substr(a856[i],xend+1)
  m.sf&jj = Len(Substr(a856[i],xend+1))

  Insert Into x856 From Memvar
Endfor
