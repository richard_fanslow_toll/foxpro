*!* This is a generic form import program for Merkury transfer sheets, etc.

IF lTesting
	SET STEP ON
	lTestMail = .T.
ENDIF

SELECT xinwolog
SET ORDER TO inwologid
LOCATE
STORE RECCOUNT() TO lnCount
WAIT WINDOW AT 10,10 "Container lines to load ...........  "+STR(lnCount) NOWAIT TIMEOUT 1

nUploadcount = 0
cRecs = ALLTRIM(STR(lnCount))

*ASSERT .F. MESSAGE "At XINWOLOG Import loop...debug"
cCtrString = PADR("WO NUM",10)+PADR("CONTAINER",15)+PADR("CTNS",6)+"PRS"+CHR(13)

SCAN  && Currently in XINWOLOG table
	cContainer = ALLT(xinwolog.CONTAINER)
	cCtnQty = ALLT(STR(xinwolog.plinqty))
	WAIT "AT XINWOLOG RECORD "+ALLTRIM(STR(RECNO()))+" OF "+cRecs WINDOW NOWAIT NOCLEAR

	csq1 = [select * from inwolog where container = ']+cContainer+[' and accountid = ]+TRANSFORM(6561)
	xsqlexec(csq1,"piw",,"wh")
	LOCATE
	IF !EOF()
		WAIT WINDOW "Container "+cContainer+" already exists in INWOLOG...looping" TIMEOUT 6
		USE IN piw
		RETURN
	ELSE
		USE IN piw
	ENDIF

	DO CASE
		CASE INLIST(cOffice,"I","N")
			lcWarehouse = "NJ1"
		CASE cOffice = "M"
			lcWarehouse = "FL1"
		OTHERWISE
			lcWarehouse = "LA1"
	ENDCASE

	SELECT xinwolog
	SCATTER MEMVAR MEMO

	m.accountid = nAcctNum
	ninwologid = dygenpk("inwolog",cWhseMod)
	nwo_num = dygenpk("wonum",cWhseMod)
	m.wo_num = nwo_num
	m.wo_date = DATE()
	cWO_Num = ALLT(STR(m.wo_num))
	m.acctname = "MERKURY INNOVATIONS, LLC"

	insertinto("inwolog","wh",.T.)
	SET STEP ON 

	nUploadcount = nUploadcount + 1

	SELECT xpl
	SCAN FOR inwologid = xinwolog.inwologid
		SCATTER MEMVAR MEMO
		m.wo_num = inwolog.wo_num
		m.accountid = inwolog.accountid
		m.inwologid=inwolog.inwologid
		m.date_rcvd = DATE()
		insertinto("pl","wh",.T.)
	ENDSCAN
ENDSCAN

*!* Added below 03.04.2015 per Darren
SELECT inwolog
STORE "" TO xtruckwonum, xtruckseal
DO "m:\dev\prg\whtruckwonum.prg" WITH inwolog.wo_num, inwolog.REFERENCE, inwolog.CONTAINER, inwolog.accountid
REPLACE inwolog.truckwonum WITH xtruckwonum, inwolog.seal WITH xtruckseal IN inwolog

cCtrString = ""
SELECT pl
SCAN FOR wo_num = inwolog.wo_num
	IF !pl.units
		cCtrString = cCtrString+PADR(cWO_Num,10)+PADR(ALLTRIM(inwolog.CONTAINER),15)+"Cartons: "+pl.STYLE+pl.PACK+TRANSFORM(totqty)+"   PO: "+pl.cayset +CHR(13)
	ELSE
		cCtrString = cCtrString+PADR(cWO_Num,10)+PADR(ALLTRIM(inwolog.CONTAINER),15)+"Units  : "+pl.STYLE+PACK+TRANSFORM(totqty) +"   PO: "+pl.cayset +CHR(13)
	ENDIF
ENDSCAN

SELECT pl
COUNT TO xnumskus FOR !units AND wo_num=inwolog.wo_num

SELECT inwolog
SCATTER MEMVAR MEMO BLANK
SELECT pl
SCATTER MEMVAR MEMO BLANK

WAIT CLEAR

WAIT WINDOW AT 10,10 "Container records loaded...........  "+STR(nUploadcount) TIMEOUT 2

WAIT CLEAR

IF nUploadcount > 0
	tu("inwolog")
	tu("pl")

*	SET STEP ON
	tsubject= "Inbound WO(s) created for "+cCustname

	IF lTesting
		tsubject= tsubject+" (TEST) at "+TTOC(DATETIME())
	ELSE
		DO CASE
			CASE cOffice = "C"
				tsubject = tsubject+"(CA) at "+TTOC(DATETIME())
			CASE cOffice = "M"
				tsubject = tsubject+"(FL) at "+TTOC(DATETIME())
			CASE cOffice = "I"
				tsubject = tsubject+"(NJ) at "+TTOC(DATETIME())
			OTHERWISE
				tsubject = tsubject+"(UNK) at "+TTOC(DATETIME())
		ENDCASE
	ENDIF
	tattach = ""
	tmessage = cCtrString
	tmessage = tmessage + CHR(13) + CHR(13) + "From generic file: "+cFilename
	IF lTesting
		tmessage = tmessage + CHR(13) + "Data is in F:\WHP\WHDATA"
	ENDIF
	tFrom ="Toll WMS Operations <toll-edi-ops@tollgroup.com>"
	IF lDoMail
		DO FORM m:\dev\frm\dartmail2 WITH tsendto,tFrom,tsubject,tcc,tattach,tmessage,"A"
	ENDIF
	cCtrString = ""
	WAIT cCustname+" Inbound Import Process Complete for file "+cFilename WINDOW TIMEOUT 2
ENDIF

RETURN
