*!* INTRADECO 940

PARAMETERS cOfficeUsed

CLOSE DATABASES ALL

*!* Set and initialize public variables
PUBLIC xFile,archivefile,cXdate1,dXdate2,cOffice,lTesting,cUseDir,cCustName,nTotPT,cMailName
PUBLIC cDelimiter,EmailCommentStr,LogCommentStr,nAcctNum,cAcctNum,lLoop,lTestImport,lCons
PUBLIC chgdate,ptid,ptctr,nPTQty,lTestUploaddet,cOfficename,cConsignee,cStoreNum,cTotPT
PUBLIC cPickticket_num_start,cPickticket_num_end,EmailCommentStr,cLoadID,lcPath,cUseFolder
PUBLIC tsendto,tcc,lcArchivepath,nXPTQty,NormalExit,cMessage,fa997file,addproc,cTransfer,cMod,cISA_Num
PUBLIC tsendtoerr,tccerr,tsendtotest,tcctest,lBrowFiles,lOverridebusy,cFilename,lLoadSQL,nFileSize
PUBLIC ARRAY a856(1)

lTesting = .f.
lTestImport = lTesting
lOverridebusy = lTesting
*lOverridebusy = .T.
lBrowfiles = lTesting && Set to true to brow XPT/XPTDET files (in development only)
*lBrowfiles = .t.

DO m:\dev\prg\_setvars WITH lTesting

IF lTesting
	CLOSE DATABASES ALL
	CLEAR
	cOfficeUsed = "I"
	lTestUploaddet = .T.
ELSE
	lTesting = .F.
	lTestUploaddet = .F.
ENDIF

cOffice = IIF(INLIST(cOfficeUsed,"2","5"),"C",cOfficeUsed)
cMod = IIF(cOffice = "C","5",cOffice)
gOffice = cMod
lLoadSQL = .t.

cOfficename = ICASE(cOffice = "I","New Jersey",cOffice = "C","San Pedro",cOffice = "X","Carson","Florida")
STORE "" TO EmailCommentStr,cLoadID,cConsignee
nAcctNum = IIF(cOffice = "I",6237,5154)
CaptionStr = IIF(cOffice = "I","IVORY","INTRADECO")+" EDI 940 PROCESS"
STORE CaptionStr TO _SCREEN.CAPTION

NormalExit = .F.
lCons = .F.
cMessage = " "
cTransfer = "940-INTRADECO-EDI-"

TRY
	IF !lTesting
		_SCREEN.WINDOWSTATE=1
	ENDIF
	cCustName = "INTRADECO"
	cMailName = "Intradeco"
	cAlias = cCustName
	cUseDir = "m:\dev\prg\"+cCustName+"940_"

	xFile = ""
	cXdate1 = ""
	dXdate2 = DATE()

	cAcctNum = ALLTRIM(STR(nAcctNum))
	LogCommentStr = ""

	SELECT 0
	USE F:\3pl\DATA\mailmaster ALIAS mm SHARED

	LOCATE FOR mm.accountid = nAcctnum AND mm.office = cOffice ;
		AND mm.edi_type = "940" and mm.acctname = IIF(cOffice = "I","IVORY-EDI","INTRADECO-EDI")
	IF FOUND()
		STORE TRIM(mm.acctname) TO cAccountName
		STORE TRIM(mm.basepath) TO lcPath
		STORE TRIM(mm.archpath) TO lcArchivepath
		STORE JUSTFNAME(TRIM(mm.fmask)) TO cFilemask
		IF lTesting
			LOCATE
			LOCATE FOR mm.edi_type = "MISC" AND mm.taskname = "GENERAL"
			STORE TRIM(IIF(mm.use_alt,mm.sendtoalt,mm.sendto)) TO tsendto
			STORE TRIM(IIF(mm.use_alt,mm.ccalt,mm.cc)) TO tcc
		ELSE
			STORE TRIM(IIF(mm.use_alt,mm.sendtoalt,mm.sendto)) TO tsendto
			STORE TRIM(IIF(mm.use_alt,mm.ccalt,mm.cc)) TO tcc
		ENDIF
		LOCATE
		LOCATE FOR mm.edi_type = "MISC" AND mm.taskname = "GENERAL"
		STORE TRIM(IIF(mm.use_alt,mm.sendtoalt,mm.sendto)) TO tsendtoerr
		STORE TRIM(IIF(mm.use_alt,mm.ccalt,mm.cc)) TO tccerr
		STORE TRIM(IIF(mm.use_alt,mm.sendtoalt,mm.sendto)) TO tsendtotest
		STORE TRIM(IIF(mm.use_alt,mm.ccalt,mm.cc)) TO tcctest
	ELSE
		WAIT WINDOW AT 10,10  "No parameters set for this acct# "+cAcctnum+"  ---> Office "+cOffice TIMEOUT 2
		USE IN mm
		NormalExit = .F.
		THROW
	ENDIF
	USE IN mm

	IF lTesting
		lcPath = TRIM(lcPath)+"TEST\"
		cUseFolder = "F:\WHP\WHDATA\"
	ELSE
		SELECT 0
		USE F:\edirouting\ftpsetup SHARED
		LOCATE FOR ftpsetup.transfer = cTransfer
		IF ftpsetup.chkbusy = .T. AND !lOverridebusy
			WAIT WINDOW "File is already processing...exiting" TIMEOUT 2
			NormalExit = .T.
			THROW
		ENDIF
		REPLACE chkbusy WITH .T.,trig_time WITH DATETIME() FOR ftpsetup.transfer = cTransfer
		USE IN ftpsetup

		xReturn = "XXX"
		DO m:\dev\prg\wf_alt WITH cOffice,nAcctNum
		cUseFolder = UPPER(xReturn)
	ENDIF

	WAIT CLEAR
	DO (cUseDir+"PROCESS")

	SELECT 0
	USE F:\edirouting\ftpsetup SHARED
	REPLACE chkbusy WITH .F. FOR ftpsetup.transfer = cTransfer
	CLOSE DATABASES ALL

CATCH TO oErr
	IF !NormalExit
		ASSERT .F. MESSAGE "In Error Catch"
		tsubject = cMailName+" 940 Upload Error at "+TTOC(DATETIME())
		tattach  = ""
		tsendto  = tsendtoerr
		tcc = tccerr
		tmessage = cMailName+" 940 Upload ERROR"

		DO CASE
			CASE "MISSING STYLE"$UPPER(cMessage)
				tmessage = tmessage+CHR(13)+cMessage
				SELECT 0
				USE F:\3pl\DATA\mailmaster ALIAS mm
				LOCATE FOR edi_type = "MISC" AND mm.taskname = "INSTYLEERR"
				tcc = IIF(mm.use_alt,mm.ccalt,mm.cc)
				USE IN mm

			CASE !EMPTY(cMessage)
				tmessage = tmessage+CHR(13)+cMessage

			OTHERWISE
				lcSourceMachine = SYS(0)
				lcSourceProgram = SYS(16)
				tmessage =tmessage+ "Try/CATCH Exeception Message:"+CHR(13)+;
					[  Error: ] + STR(oErr.ERRORNO) +CHR(13)+;
					[  Procedure: ] + oErr.PROCEDURE +CHR(13)+;
					[  LineNo: ] + STR(oErr.LINENO) +CHR(13)+;
					[  Message: ] + oErr.MESSAGE +CHR(13)+;
					[  Details: ] + oErr.DETAILS +CHR(13)+;
					[  StackLevel: ] + STR(oErr.STACKLEVEL) +CHR(13)+;
					[  LineContents: ] + oErr.LINECONTENTS+CHR(13)+;
					[  UserValue: ] + oErr.USERVALUE+CHR(13)+;
					[  Computer:  ] +lcSourceMachine+CHR(13)+;
					[  940 file:  ] +xFile+CHR(13)+;
					[  Program:   ] +lcSourceProgram
		ENDCASE

		tattach  = ""
		tfrom    ="FMI EDI Processing Center <fmi-edi-ops@fmiint.com>"
		DO FORM m:\dev\frm\dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
		COPY FILE &xFile TO &archivefile

		IF FILE(archivefile)
			DELETE FILE &xFile
		ENDIF
	ELSE
		WAIT WINDOW AT 10,10 "Normal Exit " TIMEOUT 2
	ENDIF
FINALLY
	CLOSE DATABASES ALL
	ON ERROR
ENDTRY

