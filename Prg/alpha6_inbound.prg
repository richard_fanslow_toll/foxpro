CLOSE DATA ALL
CLEAR
RELEASE ALL
PUBLIC lTesting,tattach,tsendto,tcc,tfrom,cOffice,cMod,NormalExit,lBrowfiles,cTransfer,xfile,tsendtoerr

TRY
	_setvars(.T.)
	lTesting = .F.
	lOverridebusy = lTesting
	_setvars(lTesting)
	NormalExit = .F.
	lBrowfiles = lTesting
	lBrowfiles = .f.
	lOverridebusy = .T.

	ASSERT .F. MESSAGE "At start of Alpha6 PL processing"

	IF !lTesting
		SELECT 0
		USE F:\edirouting\ftpsetup SHARED
		cTransfer = "PL-ALPHA6"

		LOCATE FOR ftpsetup.transfer = cTransfer
		IF ftpsetup.chkbusy AND !lOverridebusy
			WAIT WINDOW "Process is flagged busy...returning" TIMEOUT 3
			NormalExit = .T.
			THROW
		ELSE
			REPLACE chkbusy WITH .T.,trig_time WITH DATETIME()  ;
				FOR ftpsetup.transfer = cTransfer ;
				IN ftpsetup
			USE IN ftpsetup
		ENDIF

	ENDIF

	IF USED('INWOLOG')
		USE IN inwolog
	ENDIF
	IF USED('XINWOLOG')
		USE IN xinwolog
	ENDIF
	IF USED('PL')
		USE IN pl
	ENDIF
	IF USED('XPL')
		USE IN xpl
	ENDIF

	nAcctNum = 5726
	cOffice = "N"
	cMod = "I"
	gOffice = cMod
	gMasterOffice = cOffice

	tfrom = "Toll EDI Operations <toll-edi-ops@tollgroup.com>"
	STORE " " TO tattach,tsendto,tcc

*	_SCREEN.WINDOWSTATE= IIF(lTesting OR lOverridebusy,2,1)

	SELECT 0
	USE F:\3pl\DATA\mailmaster ALIAS mm SHARED
	LOCATE FOR mm.edi_type = "PL" AND mm.accountid = nAcctNum AND mm.office = cOffice
	IF FOUND()
		STORE TRIM(mm.acctname) TO cCustname
		STORE TRIM(mm.basepath) TO lcPath
		STORE TRIM(mm.archpath) TO lcArchivepath
		_SCREEN.CAPTION = ALLT(mm.scaption)
*!*			STORE mm.testflag 	   TO lTest
		STORE TRIM(IIF(mm.use_alt,mm.sendtoalt,mm.sendto)) TO tsendto
		STORE TRIM(IIF(mm.use_alt,mm.ccalt,mm.cc)) TO tcc
		LOCATE
		LOCATE FOR mm.edi_type = "MISC" AND mm.taskname = "JOETEST"
		STORE TRIM(IIF(mm.use_alt,mm.sendtoalt,mm.sendto)) TO tsendtotest
		STORE TRIM(IIF(mm.use_alt,mm.ccalt,mm.cc)) TO tcctest
		LOCATE
		LOCATE FOR mm.edi_type = "MISC" AND mm.taskname = "GENERAL"
		STORE TRIM(IIF(mm.use_alt,mm.sendtoalt,mm.sendto)) TO tsendtoerr
		STORE TRIM(IIF(mm.use_alt,mm.ccalt,mm.cc)) TO tccerr
		USE IN mm
	ELSE
		USE IN mm
		WAIT WINDOW AT 10,10  "No parameters set for this acct# "+ALLTRIM(STR(nAcctnum))+"  ---> Office "+lcOffice TIMEOUT 2
		RELEASE ALL
		NormalExit = .F.
		THROW
	ENDIF

	IF lTesting
		cUseFolder = "F:\WHP\WHDATA\"
		USE (cUseFolder+"inwolog") IN 0 ALIAS inwolog
		SELECT * FROM inwolog WHERE .F. INTO CURSOR xinwolog READWRITE
		USE (cUseFolder+"pl") IN 0 ALIAS pl
		SELECT * FROM pl WHERE .F. INTO CURSOR xpl READWRITE
	ELSE
		xsqlexec("select * from inwolog where .f.","xinwolog",,"wh")
		useca("inwolog","wh")

		IF lTesting
			USE (cUseFolder+"pl") IN 0 ALIAS pl
		ELSE
			xsqlexec("select * from pl where .f.","xpl",,"wh")
			useca("pl","wh")
		ENDIF
	ENDIF

	CD &lcPath
	cfilenamexx = (lcPath+"temp1.xls")
	IF FILE(cfilenamexx)
		DELETE FILE [&cFilenamexx]
	ENDIF
	len1 = ADIR(ary1,"*.xls")

	IF len1 = 0
		WAIT WINDOW "No files to process...exiting" TIMEOUT 2
		NormalExit = .T.
		THROW
	ENDIF

	cFilename = ALLTRIM(ary1[1,1])
	xfile = (lcPath+cFilename)
	cArchiveFile = (lcArchivepath+cFilename)

	COPY FILE [&xfile] TO [&cArchiveFile]

	CREATE CURSOR intake (brokerref c(15),CONTAINER c(11),SIZE c(15),seal c(15),DEST c(20),shipper c(20),consignee c(30),ctns N(10),TYPE c(5),ponum c(15),STYLE c(20),ID c(10),tqty N(10))

	oExcel = CREATEOBJECT("Excel.Application")
	oWorkbook = oExcel.Workbooks.OPEN(xfile)
	WAIT WINDOW "Now saving file "+cFilename NOWAIT NOCLEAR
* oWorkbook.SAVEAS(cfilenamexx,20) && re-saves Excel sheet as text file
	oWorkbook.SAVEAS(cfilenamexx,39)  && re-saves Excel sheet as XL95/97 file
	WAIT CLEAR
	WAIT WINDOW "File save complete...continuing" TIMEOUT 1
	oWorkbook.CLOSE(.T.)
	oExcel.QUIT()
	RELEASE oExcel

	SET STEP ON
	SELECT intake
	DELETE all
	APPEND FROM [&cfilenamexx] TYPE XL5
	BLANK FIELDS ponum FOR ponum = '"'
	DELETE FOR EMPTY(intake.tqty) AND EMPTY(intake.ctns)
	LOCATE
	IF lTesting OR lBrowfiles
		BROWSE
	ENDIF
	IF FILE(cfilenamexx)
		DELETE FILE [&cFilenamexx]
	ENDIF
	IF FILE(xfile)
		DELETE  FILE [&xfile]
	ENDIF

	SELECT xinwolog
	SCATTER MEMVAR MEMO
	SELECT xpl
	SCATTER MEMVAR MEMO

	STORE 0 TO m.inwologid,m.plid,m.wo_num,npo,qty,totqty
	STORE DATE() TO m.wo_date,m.date_rcvd
	m.accountid = nAcctNum
	m.office = cOffice
	m.mod = cMod
	m.addby = "TGF-PROC"
	m.adddt = DATETIME()
	m.addproc = "ALPHA6INBD"

	SELECT intake
	SCAN WHILE !EMPTY(STYLE)
		SCATTER MEMVAR
		IF !EMPTY(intake.brokerref)
			m.inwologid = m.inwologid+1
			m.wo_num = m.wo_num+1
			m.comments = "FILENAME*"+cFilename
			INSERT INTO xinwolog FROM MEMVAR
		ENDIF
		IF !EMPTY(m.ponum) AND occurs("PONUM*"+m.ponum,xinwolog.comments)=0
			REPLACE xinwolog.comments WITH xinwolog.comments+CHR(13)+"PONUM*"+m.ponum IN xinwolog
			RELEASE m.ponum
		ENDIF

		REPLACE xinwolog.plinqty WITH xinwolog.plinqty+m.ctns IN xinwolog
		REPLACE xinwolog.plunitsinqty WITH xinwolog.plunitsinqty+m.tqty IN xinwolog

		m.plid = m.plid+1
		npo = npo+1
		m.units = .F.
		m.totqty = m.ctns
		m.pack = TRANSFORM(m.tqty/m.ctns)
		m.po = TRANSFORM(npo)
		m.echo = "FILENAME*"+cFilename
		m.id = ""
		INSERT INTO xpl FROM MEMVAR

		m.units = .T.
		m.totqty = m.tqty
		m.pack = "1"
		INSERT INTO xpl FROM MEMVAR

	ENDSCAN

	IF lBrowfiles OR lTesting
		SELECT xinwolog
		LOCATE
		BROWSE
		SELECT xpl
		LOCATE
		BROWSE
		IF lTesting
*			CANCEL
		ENDIF
	ENDIF

	cWhse = IIF(lTesting,"whp","whi")
	SELECT xinwolog

	SCAN
		SCATTER MEMVAR MEMO
		m.wo_num = dygenpk("WONUM",cWhse)
		insertinto("inwolog","wh",.T.)

		SELECT xpl
		SCAN FOR xpl.inwologid = xinwolog.inwologid
			SCATTER FIELDS EXCEPT inwologid,wo_num MEMO MEMVAR
			IF lTesting
				m.plid = dygenpk("PL",cWhse)
				INSERT INTO pl FROM MEMVAR
			ELSE
				insertinto("pl","wh",.T.)
			ENDIF
		ENDSCAN
	ENDSCAN

	goodmail()

	IF !lTesting
		tu("inwolog")
	ENDIF
	IF !lTesting
		tu("pl")
	ENDIF

	IF !lTesting
		SELECT 0
		USE F:\edirouting\ftpsetup SHARED
		cTransfer = "PL-ALPHA6"
		REPLACE chkbusy WITH .F.	FOR ftpsetup.transfer = cTransfer IN ftpsetup
		USE IN ftpsetup
	ENDIF

	WAIT WINDOW "Alpha6 PL Upload process complete...exiting" TIMEOUT 2
	NormalExit = .T.

CATCH TO oErr
	IF NormalExit = .F.
		SET STEP ON
		ASSERT .F. MESSAGE "In Catch section..."
		tsubject = "Alpha6 PL Upload Error ("+TRANSFORM(oErr.ERRORNO)+") at "+TTOC(DATETIME())
		tsendto  = tsendtoerr
		tcc = tccerr
		tmessage = "Alpha6 PL Upload Error..... Please fix me........!"
		lcSourceMachine = SYS(0)
		lcSourceProgram = "from alpha6_inbound"

		tmessage =tmessage+ "Try/CATCH Exeception Message:"+CHR(13)+;
			[  Error: ] + STR(oErr.ERRORNO) +CHR(13)+;
			[  LineNo: ] + STR(oErr.LINENO) +CHR(13)+;
			[  Message: ] + oErr.MESSAGE +CHR(13)+;
			[  Procedure: ] + oErr.PROCEDURE +CHR(13)+;
			[  Details: ] + oErr.DETAILS +CHR(13)+;
			[  StackLevel: ] + STR(oErr.STACKLEVEL) +CHR(13)+;
			[  LineContents: ] + oErr.LINECONTENTS+CHR(13)+;
			[  UserValue: ] + oErr.USERVALUE+CHR(13)+;
			[  Computer:  ] +lcSourceMachine+CHR(13)+;
			[  940 file:  ] +xfile+CHR(13)+;
			[  Program:   ] +lcSourceProgram

		DO FORM m:\dev\frm\dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
	ELSE
		WAIT WINDOW AT 10,10 "Normal Exit " TIMEOUT 2
	ENDIF
FINALLY
	ON ERROR
	SET STATUS BAR ON
	CLOSE DATABASES ALL
ENDTRY

*************************
PROCEDURE goodmail
*************************
tsubject= "TGF "+cCustname+" Packing List Upload: " +TTOC(DATETIME())
tattach = ""
tmessage = "Packing List uploaded for "+cCustname+CHR(13)+"From File: "+cFilename+CHR(13)
tfrom ="TGF EDI Operations <toll-edi-ops@tollgroup.com>"
IF lTesting
	tmessage = tmessage+CHR(13)+"*TEST DATA*"
ENDIF
DO FORM m:\dev\frm\dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
