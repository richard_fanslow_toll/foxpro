* This program identifies ship_ref (PT's) that have shipped and are not in the 945 file that is in edi_trigger
utilsetup("MJ_945_PT_NOT_IN_OB")
SET EXCLUSIVE OFF 
SET DELETED ON 
SET TALK OFF  
SET SAFETY OFF   &&Allows for overwrite of existing files

close databases all


xindir="f:\ftpusers\mj_wholesale\wms_outbound\945out\archive\"
xdate=date()-3


xfilecnt=Adir(afiles,xindir+"*.txt")
if xfilecnt=0
	return
endif

create cursor xfilelist (file c(100), x c(10), moddt C(10), modtm c(10), moddttm t(1))
append from array afiles
replace all moddttm with ctot(moddt+" "+modtm)
SET STEP ON 
use f:\3pl\data\edi_trigger in 0
select space(80) as ffile, * from edi_trigger where edi_type='945 '   and INLIST(accountid,6303,6304,6543) AND office='J' and when_proc>=xdate AND fin_status !='GSI FILE-CREATED'  into cursor xedi readwrite
use in edi_trigger

scan
	xpt="*"+alltrim(xedi.ship_ref)+"*"

	xfile=right(xedi.file945,len(xedi.file945)-rat("\",xedi.file945))
	xinfile=xindir+xfile
	xfs=filetostr(xinfile)
	if xpt$xfs
		replace ffile with xinfile in xedi
	else
		select xfilelist
		scan for moddttm>=xdate
			xfile=alltrim(xfilelist.file)
			xinfile=xindir+xfile
			xfs=filetostr(xinfile)

			if xpt$xfs
				replace ffile with xinfile in xedi
				exit
			endif
		endscan
	endif
endscan
SET STEP ON

*browse for empty(ffile)

select * FROM xedi WHERE  empty(ffile) AND bol !='04070086303654871' INTO CURSOR temp22
*copy to c:\tt.xls type xls for empty(ffile)
SELECT temp22
 
	If Reccount() > 0 
		export TO "C:\MJ_945_PT_NOT_IN_OB_FILE"  TYPE xls
		tsendto = "tmarg@fmiint.com"
		tattach = "C:\MJ_945_PT_NOT_IN_OB_FILE.xls" 
		tcc =""                                                                                                                               
		tFrom ="Toll EDI System Operations <fmi-transload-ops@fmiint.com>"                                
		tmessage = "MJ_945 PT_NOT_IN_OB  MJ 945 pickticket not in 0B file:  "+Ttoc(Datetime())        
		tSubject = "MJ 945 pickticket not in 0B file"                                                           
		Do Form m:\dev\frm\dartmail2 With tsendto,tFrom,tSubject, tcc,tattach,tmessage  ,"A"     
			
	ELSE 
*		export TO "S:\MarcJacobsData\Reports\InventorySyncFiles\OB_OSnD\NO_pre_invoice_osnd_exist_" + TTOC(DATE()-1, 0)  TYPE xls
		tsendto = "tmarg@fmiint.com"
		tattach = "" 
		tcc =""                                                                                                                               
		tFrom ="Toll EDI System Operations <fmi-transload-ops@fmiint.com>"                                
		tmessage = "NO MJ 945 picktickets in ob file are missing:  "+Ttoc(Datetime())        
		tSubject = "NO MJ 945 picktickets in ob file are missing:"                                                           
		Do Form m:\dev\frm\dartmail2 With tsendto,tFrom,tSubject, tcc,tattach,tmessage  ,"A"   

	ENDIF
close data all 

schedupdate()
_screen.Caption=gscreencaption
on error

