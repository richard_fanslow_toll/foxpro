************************************************************************
* ANF 856OUT Program: 11.10.15, Joe
************************************************************************
PARAMETERS nWO_Num,cOffice
CLEAR

*cTrailer = "657638"
cTrailer = "JBHU998625"
cSCAC= "JBHU"

TRY
	CLOSETABLES()
	PUBLIC ARRAY thisarray(1)
	PUBLIC nFilenum, tfile, tshipid,cstring,slevel,plevel,lnInterControlnum,lcInterctrlNum,gsNum,stNum,gn856Ctr,lTesting,nTotqty,nMatchQty
	PUBLIC nTotCtnQty,nTotCtnWt,nTotCuFt,cStreetAddressFull,cPO_Num,cOrigPO_Num,shipmentleveltotal,orderleveltotal
	PUBLIC equipleveltotal,dtmail,cRefString,cfd,NormalExit,cErrMsg
	PUBLIC cMod,cMBOL,cEDIType,lParcelType,cFilenameShort,cFilenameOut,cFilenameArch,lcPath,cWO_NumList,cISA_Num

	lTesting = .F.

	DO m:\dev\prg\_setvars WITH lTesting
	IF lTesting
		CLOSE DATA ALL
		ON ERROR DEBUG
	ENDIF

	dWO_date = DATE()

	ON ESCAPE CANCEL
	nAcctNum = 6576
	cMod = cOffice
	cMBOL = ""
	lParcelType = .f.
	cEDIType = "856"
	
	NormalExit = .F.

	lDoShipmentLevel = .T.
	lDoEquipLevel = .T.
	lUseBarcodes = .F.
	dShipDate = IIF(lTesting,DTOS(DATE()),DTOS(dWO_date))
	tTime = LEFT(STRTRAN(TIME(),":",""),4)
	cRefString = ""

	IF lTesting
		USE F:\anf\DATA\anfdetail.DBF IN 0 ALIAS wolog
		USE m:\dev\wodata\manifest IN 0 ALIAS manifest && Will be using Manifest for certain info, where manifest.orig_wo = wolog.wo_num
	ELSE
		USE F:\wo\wodata\manifest IN 0 ALIAS manifest && Will be using Manifest for certain info, where manifest.orig_wo = wolog.wo_num
		USE F:\wo\wodata\wolog IN 0 ALIAS wolog
	ENDIF

	USE F:\edirouting\SFTPJOBS IN 0 ALIAS SFTPJOBS
	USE F:\3pl\DATA\serial\anf_serial IN 0 ALIAS anf_serial

	lUPS = .F.
	lINWO = .F.
	cINWOString = ""
	cErrMsg = ""

	lTestMail = lTesting
	lFilesOut = !lTesting
	lEmail = .T.
	lWrapFile = .T.

	nManCount = 0
	STORE "*" TO cfd

	IF TYPE('nWO_Num') = "L"
		IF lTesting
			nWO_Num = 1661936
		ELSE
			DO ediupdate WITH "MISSING WO",.T.
			THROW
		ENDIF
	ENDIF
	c1 = 'TLP6502190'
	cWO_Num = ALLTRIM(STR(nWO_Num))
	cBOL = cWO_Num
	cWO_NumList = cWO_Num
	
	lEmail = .T.

	xsqlexec("select * from account where inactive=0","account",,"qq")
	INDEX ON accountid TAG accountid
	SET ORDER TO

	IF(SEEK(nAcctNum,"account","accountid"))
		cGroupName = ALLTRIM(account.acctname)
		WAIT WINDOW "Group Name: "+cGroupName TIMEOUT 2
	ELSE
		USE IN account
		DO ediupdate WITH "MISSING ACCOUNT #",.T.
		THROW
	ENDIF

	WAIT CLEAR

	dtmail = TTOC(DATETIME())
	cMailName = "Abercrombie & Fitch"
	tfrom = "TOLL Warehouse Operations <toll-warehouse-ops@tollgroup.com>"
	STORE " " TO tattach,tsendto,tcc

	SELECT 0
	USE F:\3pl\DATA\mailmaster ALIAS mm SHARED
	LOCATE FOR mm.edi_type = "O856" AND mm.accountid = nAcctNum
	IF FOUND()
		STORE TRIM(mm.basepath) TO lcPath
		STORE TRIM(mm.holdpath) TO lcHoldPath
		STORE TRIM(mm.archpath) TO lcArchivepath
		_SCREEN.CAPTION = ALLT(mm.scaption)
		IF !lTesting
			LOCATE
			STORE TRIM(IIF(mm.use_alt,mm.sendtoalt,mm.sendto)) TO tsendto
			STORE TRIM(IIF(mm.use_alt,mm.ccalt,mm.cc)) TO tcc
		ELSE
			LOCATE
			LOCATE FOR mm.edi_type = "MISC" AND mm.taskname = "GENERAL"
			STORE TRIM(IIF(mm.use_alt,mm.sendtoalt,mm.sendto)) TO tsendto,tsendtoerr
			STORE TRIM(IIF(mm.use_alt,mm.ccalt,mm.cc)) TO tcc,tccerr
		ENDIF
		tsendtoerr = tsendto
		tccerr = tcc
		USE IN mm
	ELSE
		USE IN mm
		WAIT WINDOW AT 10,10  "No parameters set for this acct# "+ALLTRIM(STR(Acctnum))+"  ---> Office "+cOffice TIMEOUT 2
		RELEASE ALL
		NormalExit = .F.
		THROW
	ENDIF

	STORE "" TO fstring
	STORE 0 TO shiplevel,gn856Ctr,stNum,lnCTTCount,lnSECount
	STORE 0 TO shipmentleveltotal,orderleveltotal,equipleveltotal
	STORE 0 TO hlcount2,hlpack,spcntp,hlsegcnt,segcnt,spcnt,spcnt2,hlitem,spcnti,lc_hlcount,a,lincnt, totlincnt
	STORE 0 TO sesegcnt,segcnt,nTotqty,nMatchQty,nTotCtnQty,nTotCtnWt,nTotCuFt

	STORE "" TO workordersprocessed,summaryinfo,lc_856num,lc_cnum
	STORE "" TO cShiptoName,cShiptoAddr1,cShiptoAddr2,cShiptoCity,cShiptoState,cShiptoZip,cStreetAddressFull

* top level counter for unique HL segment counts
	STORE 0 TO hlctr

* initiate overall counters
	STORE 0 TO shipmentlevelcount,orderlevelcount
	STORE 0 TO equiplevelcount

* initiate session overall counters
	STORE 0 TO thisshipmentlevelcount,thisorderlevelcount
	STORE 0 TO thisequiplevelcount

* initiate the value of the curent count for remembering the Parent HL Count
	STORE 0 TO currentshipmentlevelcount,currentorderlevelcount
	STORE 0 TO currentequiplevelcount

	STORE .F. TO llEditUpdate

	lcstring  = ""
	lincnt = 1
*******************************************************************************************
	jtimec = TIME()
	STORE SUBSTR(jtimec,1,2) TO j1
	STORE SUBSTR(jtimec,4,2) TO j2
	STORE SUBSTR(jtimec,7,2) TO j3
	lcTime = j1+j2
	dShortDate = RIGHT(DTOS(DATE()),6)
	dLongDate = DTOS(DATE())
***************************
*** Build Header Record ***
***************************
*!* For the GS Segment
	tsendid     = 'TGFSCS'
	tsendidlong = PADR(tsendid,15," ")
	tsendqual   = 'ZZ'
******************************
	trecid      = 'ANFTLP'
	trecidlong  = PADR(trecid,15," ")
	trecqual  = 'ZZ'

*******************************************************************************************
	WAIT WINDOW "Setting up ANF Outbound 856........." NOWAIT

	cFilenameHold = (lcHoldPath+"856ANF"+TTOC(DATETIME(),1)+".EDI")
	nFilenum = FCREATE(cFilenameHold)
	cFileStem = JUSTFNAME(cFilenameHold)
	cFilenameShort = cFileStem
	cFilenameOut = (lcPath+cFileStem)
	cFilenameArch = (lcArchivepath+cFileStem)

**********************************************
********  S H I P M E N T  L E V E L *********
**********************************************

	IF lTesting
*  SET STEP ON
	ENDIF

	IF nFilenum < 0
		=FCLOSE(nFilenum)
		CLOSE ALL
		THROW
	ENDIF

	WriteHeader()

	SELECT manifest
  =Seek(nWO_Num,"manifest","wo_num")
  If Found("manifest")
    tTime= Strtran(Substr(getmemodata("suppdata","COMPLETEDT"),12,6),":","")
    cScac = Alltrim(manifest.carrier)
  Else
    cScac = "JBHU"
  Endif  

  SELECT manifest
	LOCATE

	SUM pl_qty TO nTotCtnQty FOR manifest.wo_num = nWO_Num AND accountid = nAcctNum
	LOCATE
	SUM totalkgs TO nTotCtnWt FOR manifest.wo_num = nWO_Num AND accountid = nAcctNum
	LOCATE
	SUM cbm TO nTotCtnCube FOR manifest.wo_num = nWO_Num AND accountid = nAcctNum
	LOCATE

	WAIT WINDOW "Now scanning ANF Detail" NOWAIT

	SCAN FOR manifest.wo_num = nWO_Num
		cBSN = ALLTRIM(STR(manifest.wo_num))
		alength = ALINES(asupp,manifest.suppdata,.T.,CHR(13))

		IF lDoShipmentLevel
			DO shipmentheader WITH IIF(lTesting,cBSN,ALLTRIM(STR(nWO_Num)))
			STORE 2 TO segcnt
			shipment_level()  && write out the shipment level info to the flat file
			lDoShipmentLevel = .F.
		ENDIF

********** E Q U I P M E N T    L E V E L ****************************
		IF lDoEquipLevel
			equip_level()  && write out the shipment level info to the flat file
			lDoEquipLevel = .F.
		ENDIF

********** O R D E R   L E V E L ****************************

		cPO_Num  =  ALLTRIM(manifest.po_num)
		IF !(cPO_Num$cRefString)
			cRefString = IIF(EMPTY(cRefString),cPO_Num,cRefString+CHR(13)+cPO_Num)
		ENDIF
		WAIT WINDOW "Using PO# "+cPO_Num NOWAIT

*		IF pl_qty != 0
			order_level()
*		ENDIF
	ENDSCAN

	SELECT manifest
	lcCtr = ALLTRIM(STR(totlincnt))
	lincnt = 1
	lc856Ctr = ALLTRIM(STR(gn856Ctr))
	lcSegCtr = ALLTRIM(STR(segcnt+1))

	STORE "SE*"+lcSegCtr+"*"+stNum TO cstring
	fputstring(nFilenum,cstring)

	STORE "" TO workordersprocessed
	STORE 0 TO thisshipmentlevelcount,thisorderlevelcount
	STORE 0 TO thisequiplevelcount

	lc856Ctr = ALLTRIM(STR(gn856Ctr))
	STORE "GE*"+lc856Ctr+"*"+gsNum TO cstring
	fputstring(nFilenum,cstring)

	STORE "IEA*1*"+lcInterctrlNum TO cstring
	fputstring(nFilenum,cstring)


	SET CENTURY OFF
	SET DATE TO american

	IF lTesting
		cShipMsg  =  '  Shipment (PO level) count: '
		cOrderMsg= '  Order (UPC level) count: '
		cItemMsg  = '  Item (Style level) count: '
		cPackMsg  = '  Pack (Carton level) count: '
		cShipTot   = ALLTRIM(STR(shipmentleveltotal))
		cEquipTot   = ALLTRIM(STR(equipleveltotal))
		cOrderTot = ALLTRIM(STR(orderleveltotal))
	ENDIF

*!* Now transfers files to correct output folders
	=FCLOSE(nFilenum)
	COPY FILE &cFilenameHold TO &cFilenameArch

set step On 

	IF lFilesOut
		COPY FILE &cFilenameHold TO &cFilenameOut
		DELETE FILE &cFilenameHold
	ENDIF

	DO ediupdate WITH "ANF 856 CREATED",.F.
	WAIT CLEAR
	WAIT WINDOW "ANF 856 Process complete..." TIMEOUT 2
	NormalExit = .T.
	GOODMAIL()  && this proc send out the email alert
	
	*!* asn_out_data()()

CATCH TO oErr
	IF NormalExit = .F.
		ASSERT .F. MESSAGE "In Catch section..."
		SET STEP ON
		tsubject = cMailName+" 856 Upload Error ("+TRANSFORM(oErr.ERRORNO)+") at "+TTOC(DATETIME())
		tsendto  = tsendtoerr
		tcc = tccerr
		tmessage = cMailName+" 856 Upload Error..... Please fix me........!"
		lcSourceMachine = SYS(0)
		lcSourceProgram = "from edipoller_out-ANF 856"

		tmessage =tmessage+ "Try/CATCH Exeception Message:"+CHR(13)+;
			[  Error: ] + STR(oErr.ERRORNO) +CHR(13)+;
			[  LineNo: ] + STR(oErr.LINENO) +CHR(13)+;
			[  Message: ] + oErr.MESSAGE +CHR(13)+;
			[  Procedure: ] + oErr.PROCEDURE +CHR(13)

		IF !EMPTY(cErrMsg)
			tmessage =tmessage+CHR(13)+"Error found: "+cErrMsg
		ELSE
			tmessage =tmessage+[  Details: ] + oErr.DETAILS +CHR(13)+;
				[  StackLevel: ] + STR(oErr.STACKLEVEL) +CHR(13)+;
				[  LineContents: ] + oErr.LINECONTENTS+CHR(13)+;
				[  UserValue: ] + oErr.USERVALUE+CHR(13)+;
				[  Computer:  ] +lcSourceMachine+CHR(13)+;
				[  856 file:  ] +cFileStem+CHR(13)+;
				[  Program:   ] +lcSourceProgram
		ENDIF

		DO FORM m:\dev\frm\dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
	ELSE
		WAIT WINDOW AT 10,10 "Normal Exit " TIMEOUT 2
	ENDIF
FINALLY
	ON ERROR
*	SET STATUS BAR ON
ENDTRY

******************************************************
* O R D E R    L E V E L
******************************************************
PROC order_level
	PARAMETER lcDelloc
	orderleveltotal = orderleveltotal + 1
	hlctr = hlctr +1
	orderlevelcount = orderlevelcount+1
	thisorderlevelcount = thisorderlevelcount+1
	lc_hlcount = getnum(hlctr)  && overall HL counter
	lc_parentcount = getnum(currentequiplevelcount)  && the parent
	currentorderlevelcount = hlctr

	STORE "HL*"+ALLTRIM(lc_hlcount)+"*"+ALLTRIM(lc_parentcount)+"*O" TO cstring
	fputstring(nFilenum,cstring)
	segcnt=segcnt+1
	hlsegcnt=hlsegcnt+1

** PRF (PO#, as Outdet.style field)
	STORE "PRF*"+ALLTRIM(cPO_Num) TO cstring
	fputstring(nFilenum,cstring)
	segcnt=segcnt+1

	llDoUnits = .T.

	IF manifest.rcv_qty != manifest.pl_qty
		llDoUnits = .F.
		if manifest.pl_qty=0
			lnFactor = 1.00
		else
			lcPack = getmemodata("suppdata","PACK")
			lnPack = VAL(ALLTRIM(lcPack))
			llDoUnits = .F.
			lnFactor = (manifest.rcv_qty*lnPack)/manifest.units
			lnUnits = lnPack*rcv_qty
		endif
	ELSE
		lnFactor = 1.00
		lnUnits = manifest.units
	ENDIF

	STORE "MEA"+cfd+cfd+"PRQ"+cfd+ALLTRIM(STR(manifest.quantity))+cfd+"CT" TO cstring
	fputstring(nFilenum,cstring)
	segcnt=segcnt+1

	IF llDoUnits
		STORE "MEA"+cfd+cfd+"QUR"+cfd+ALLTRIM(STR(lnUnits))+cfd+"EA" TO cstring
		fputstring(nFilenum,cstring)
		segcnt=segcnt+1
	ENDIF
		
		nUseWt = 0
		nUseWt = IIF(manifest.totalkgs = 0,(manifest.weight/2.2046),manifest.totalkgs)
		STORE "MEA"+cfd+cfd+"N"+cfd+ALLTRIM(STR(nUseWt*lnFactor))+cfd+"KG" TO cstring
		fputstring(nFilenum,cstring)
		segcnt=segcnt+1

		STORE "MEA"+cfd+cfd+"VOL"+cfd+ALLTRIM(STR(manifest.cbm*lnFactor))+cfd+"CR" TO cstring
		fputstring(nFilenum,cstring)
		segcnt=segcnt+1

	STORE "REF"+cfd+"SI"+cfd+ALLTRIM(manifest.hawb) TO cstring
	fputstring(nFilenum,cstring)
	segcnt=segcnt+1

ENDPROC

*******************************************
*************END OF ORDER LEVEL************
*******************************************

******************************************************
* E Q U I P M E N T  L E V E L
******************************************************
PROC equip_level
	PARAMETER lcDelloc
	equipleveltotal = equipleveltotal + 1
	hlctr = hlctr +1
	equiplevelcount = equiplevelcount+1
	thisequiplevelcount = thisequiplevelcount+1
	lc_hlcount = getnum(hlctr)  && overall HL counter
	lc_parentcount = getnum(currentshipmentlevelcount)  && the parent
	currentequiplevelcount = hlctr
	STORE "HL*"+ALLTRIM(lc_hlcount)+"*"+ALLTRIM(lc_parentcount)+"*E" TO cstring
	fputstring(nFilenum,cstring)
	segcnt=segcnt+1
	hlsegcnt=hlsegcnt+1

	STORE "MEA"+cfd+cfd+"ZZZ"+cfd+ALLTRIM(STR(nTotCtnQty))+cfd+"CT" TO cstring  && Tot cartons for WO#
	fputstring(nFilenum,cstring)
	segcnt=segcnt+1

	STORE "MEA"+cfd+cfd+"PRQ"+cfd+ALLTRIM(STR(nTotCtnQty))+cfd+"CT" TO cstring  && Tot cartons for WO#
	fputstring(nFilenum,cstring)
	segcnt=segcnt+1

	STORE "MEA"+cfd+cfd+"VOL"+cfd+ALLTRIM(STR(nTotCtnCube))+cfd+"CR" TO cstring  && Tot volume for WO#
	fputstring(nFilenum,cstring)
	segcnt=segcnt+1

	STORE "MEA"+cfd+cfd+"N"+cfd+ALLTRIM(STR(nTotCtnWt))+cfd+"KG" TO cstring  && Tot weight for WO#
	fputstring(nFilenum,cstring)
	segcnt=segcnt+1

&&  STORE "TD3"+cfd+"TC"+cfd+cSCAC+cfd+cTrailer+cfd+"G"+cfd+ALLTRIM(STR(nTotCtnWt))+cfd+"KG"+cfd+cfd+cfd+ALLTRIM(wolog.seal) TO cstring  &&??

&& need to link up with the Seal Number

	DO CASE
		CASE  LEN(ALLTRIM(manifest.trailer)) > 4
			lcTrailerPrefix = SUBSTR(ALLTRIM(manifest.trailer),1,4)
			lcTrailer = SUBSTR(ALLTRIM(manifest.trailer),5)

		CASE  LEN(ALLTRIM(manifest.trailer))<= 4
			lcTrailerPrefix = SUBSTR(ALLTRIM(manifest.trailer),1,2)
			lcTrailer = SUBSTR(ALLTRIM(manifest.trailer),3)
	ENDCASE

*!*    If Len(Alltrim(manifest.trailer)) > 10
*!*      lcTrailerPrefix = Substr(Alltrim(manifest.trailer),1,4)
*!*      lcTrailer = Substr(Alltrim(manifest.trailer),5)
*!*    Else
*!*      lcTrailerPrefix = ""
*!*      lcTrailer = Alltrim(manifest.trailer)
*!*    Endif


	STORE "TD3"+cfd+"TC"+cfd+lcTrailerPrefix+cfd+ALLTRIM(lcTrailer)+cfd+"G"+cfd+ALLTRIM(STR(nTotCtnWt))+cfd+"KG"+cfd+cfd+cfd+ALLTRIM(manifest.seal) TO cstring  &&??
	fputstring(nFilenum,cstring)
	segcnt=segcnt+1

*!*    cHandleCode = segmentget(@asupp,"HANDLING",alength)
*!*    STORE "TD4"+cfd+cHandleCode TO cstring  &&??
*!*    fputstring(nFilenum,cstring)
*!*    segcnt=segcnt+1

*  cShipper = segmentget(@asupp,"VENDOR",alength)
* cShipCode = segmentget(@asupp,"VENDORNUM",alength)

	**adjusted to handle the possibility of shipping from CARSON II as opposed to SP - mvw 03/05/17
	if coffice="N"
		cShipper = "TOLL TRANSLOAD - CARTERET NJ"
	else
		if seek(manifest.wo_num,"wolog","wo_num")
			xfromcs=wolog.fromcs
		else
			xsqlexec("select * from fxwolog where wo_num = "+transform(manifest.wo_num),,,'fx')
			xfromcs=fxwolog.origin
			use in fxwolog
		endif

		cShipper = "TOLL TRANSLOAD "+IIF(xfromcs="CARSON","- CARSON CA","- SAN PEDRO CA")
	endif

	cShipCode = IIF(cOffice = "C","TOLL1","TOLL2")

	STORE "N1"+cfd+"LP"+cfd+cShipper+cfd+"ZZ"+cfd+cShipCode TO cstring  &&??
	fputstring(nFilenum,cstring)
	segcnt=segcnt+1

ENDPROC

*******************************************
*************END OF EQUIPMENT LEVEL************
*******************************************

******************************************************
* S H I P M E N T     L E V E L
******************************************************
PROC shipment_level
	shipmentleveltotal = shipmentleveltotal + 1
* reset all at each shipment level
	STORE 0 TO shipmentlevelcount
	STORE 0 TO orderlevelcount
	STORE 0 TO packlevelcount
	STORE 0 TO itemlevelcount
	STORE 0 TO hlctr

	shipmentlevelcount = shipmentlevelcount +1
	thisshipmentlevelcount = thisshipmentlevelcount +1
	hlctr = hlctr +1
	currentshipmentlevelcount = hlctr
	lc_hlcount = ALLTRIM(getnum(hlctr))
	STORE "HL*"+lc_hlcount+cfd+cfd+"S" TO cstring
	fputstring(nFilenum,cstring)
	segcnt=segcnt+1
	hlsegcnt=hlsegcnt+1

*  cBSNPrefix = LEFT(ALLTRIM(wolog.bsn),4)
*  STORE "TD5"+cfd+cfd+"2"+cfd+cBSNPrefix+cfd+ALLTRIM(wolog.TYPE) TO cstring

&&IIF(lTesting,"PASC",ALLTRIM(manifest.carrier))

  If Inlist(cSCAC,"HJBI","SCDS","ANSH")
		lcMode ="R"
	ELSE
		lcMode ="M"
	ENDIF

	STORE "TD5"+cfd+cfd+"2"+cfd+cSCAC+cfd+lcMode TO cstring
	fputstring(nFilenum,cstring)
	segcnt=segcnt+1

	STORE "REF*MA*"+ALLTRIM(TRANSFORM(nWO_Num)) TO cstring  && FMI O/B WO
	fputstring(nFilenum,cstring)
	segcnt=segcnt+1

	dDTMDt = DTOS(IIF(lTesting,DATE(),manifest.wo_date))
	STORE "DTM*111*"+dDTMDt+cfd+tTime TO cstring
	fputstring(nFilenum,cstring)
	segcnt=segcnt+1

	STORE "N1"+cfd+"SJ"+cfd+"TOLL GLOBAL FORWARDING"+cfd+"ZZ"+cfd+"TOLL" TO cstring
	fputstring(nFilenum,cstring)
	segcnt=segcnt+1

ENDPROC
********************************************
********** END OF SHIPMENT LEVEL************
********************************************

******************************************************
PROCEDURE shipmentheader
******************************************************
	PARAMETERS tshipid

	gn856Ctr = gn856Ctr +1
	STORE "ST"+cfd+"856"+cfd+stNum TO cstring
	fputstring(nFilenum,cstring)

	STORE "BSN"+cfd+"04"+cfd+ALLTRIM(TRANSFORM(nWO_Num))+cfd+ALLTRIM(dShipDate)+cfd+tTime TO cstring
	fputstring(nFilenum,cstring)

	totlincnt = 0

ENDPROC
******************************************************
PROCEDURE WriteHeader
******************************************************
* get the next unique control number
	SELECT anf_serial
	gsNum = ALLTRIM(STR(anf_serial.grpseqnum))
	stNum = PADL(gsNum,9,"0")

	REPLACE anf_serial.grpseqnum WITH anf_serial.grpseqnum + 1 IN anf_serial
	lnInterControlnum = anf_serial.seqnum
	REPLACE anf_serial.seqnum WITH anf_serial.seqnum + 1 IN anf_serial
	lcInterctrlNum = PADL(ALLTRIM(STR(lnInterControlnum)),9,"0")
	cISA_Num = lcInterctrlNum
	
*  write out the 856 header
	cISA_Type = IIF(lTesting,"T","P")

	STORE "ISA"+cfd+"00"+cfd+"          "+cfd+"00"+cfd+"          "+cfd+tsendqual+cfd+tsendidlong+cfd+trecqual+cfd+trecidlong+cfd+dShortDate+cfd+lcTime+;
		cfd+"U"+cfd+"00401"+cfd+lcInterctrlNum +cfd+"0"+cfd+cISA_Type+cfd+">" TO cstring
	fputstring(nFilenum,cstring)

	STORE "GS"+cfd+"SH"+cfd+ALLTRIM(tsendid)+cfd+trecid+cfd+dLongDate+cfd+lcTime+;
		cfd+ALLTRIM(gsNum)+cfd+"X"+cfd+"004010" TO cstring
	fputstring(nFilenum,cstring)


ENDPROC

******************************************************
FUNCTION getnum
******************************************************
	PARAM plcount
	fidnum = IIF(BETWEEN(plcount,1,9),SPACE(11)+LTRIM(STR(plcount)),;
		IIF(BETWEEN(plcount,10,99),SPACE(10)+LTRIM(STR(plcount)),;
		IIF(BETWEEN(plcount,100,999),SPACE(9)+LTRIM(STR(plcount)),;
		IIF(BETWEEN(plcount,1000,9999),SPACE(8)+LTRIM(STR(plcount)),;
		IIF(BETWEEN(plcount,10000,99999),SPACE(7)+LTRIM(STR(plcount))," ")))))
	RETURN fidnum

******************************************************
PROCEDURE fputstring
******************************************************
	PARAM filehandle,filedata
	IF lWrapFile
		FPUTS(filehandle,filedata)
	ELSE
		FWRITE(filehandle,filedata)
	ENDIF
ENDPROC

******************************************************
PROCEDURE CheckEmpty
******************************************************
	PARAMETER lcDataValue
	RETURN IIF(EMPTY(lcDataValue),"UNKNOWN",lcDataValue)
ENDPROC

******************************************************
PROCEDURE RevertTables
******************************************************
	TABLEREVERT(.T.,"controlnum")
	TABLEREVERT(.T.,"a856info")
	TABLEREVERT(.T.,"bkdnWO")
ENDPROC

******************************************************
PROCEDURE ediupdate
******************************************************
	PARAMETERS cMsgStr,lIsError

	IF !USED('edi_trigger')
		USE F:\3pl\DATA\edi_trigger IN 0
	ENDIF

	IF lIsError
		WAIT WINDOW "Error: "+cMsgStr TIMEOUT 5
		ASSERT .F. MESSAGE "In EDIUPDATE - ERROR"
	ENDIF

	SELECT edi_trigger
	LOCATE
	IF lIsError
		REPLACE processed WITH .T.,proc856 WITH .F.,file856 WITH "",;
			fin_status WITH cStatus,errorflag WITH .T. ;
			FOR edi_trigger.wo_num = nWO_Num AND accountid = nAcctNum
		=FCLOSE(nFilenum)
		ERASE &cFilenameHold
	ELSE
		REPLACE processed WITH .T.,proc856 WITH .T.,file856 WITH cFilenameHold,;
			fin_status WITH "856 CREATED",errorflag WITH .F.,when_proc WITH DATETIME() ;
			FOR edi_trigger.wo_num = nWO_Num AND accountid = nAcctNum
	ENDIF

	IF lIsError
*!* Create eMail error message
		tsubject = "ANF 856 Error in  WO# "+TRIM(cWO_Num)
		tattach = " "
		tmessage = "ANF 856 Processing for WO# "+cWO_Num+" (Office: "+cOffice+") produced this error: "+cStatus
		tmessage = tmessage+CHR(13)+"Reset EDI_TRIGGER to re-run."

		IF lEmail
			DO FORM m:\dev\frm\dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
		ENDIF
	ENDIF
ENDPROC

******************************************************
PROCEDURE CLOSETABLES
******************************************************

	IF USED('manifest')
		USE IN manifest
	ENDIF
	IF USED('wolog')
		USE IN wolog
	ENDIF
	IF USED('anf_serial')
		USE IN anf_serial
	ENDIF
	IF USED('account')
		USE IN account
	ENDIF
ENDPROC

****************************
PROCEDURE segmentget
****************************

	PARAMETER thisarray,lcKey,nLength

	FOR i = 1 TO nLength
		IF i > nLength
			EXIT
		ENDIF
		lnEnd= AT("*",thisarray[i])
		IF lnEnd > 0
			lcThisKey =TRIM(SUBSTR(thisarray[i],1,lnEnd-1))
			IF OCCURS(lcKey,lcThisKey)>0
				RETURN SUBSTR(thisarray[i],lnEnd+1)
				i = 1
			ENDIF
		ENDIF
	ENDFOR

	RETURN ""

****************************
PROCEDURE GOODMAIL
****************************

*!* Create eMail confirmation message
	tsubject = "ANF 856 EDI File from TGF"
	tattach = " "
	tmessage = "ANF 856 Outbound EDI Info from TGF, for Outbound WO# "+cWO_Num+CHR(13)
	tmessage = tmessage + "File name: "+cFileStem+CHR(13)
	tmessage = tmessage + "containing these POs :"+CHR(13)+CHR(13)
	tmessage = tmessage + cRefString+CHR(13)+CHR(13)
	tmessage = tmessage +"has been created and placed into folder "+IIF(lTesting,"856-Staging","856OUT")+CHR(13)+CHR(13)
	IF !lTestMail AND !lTesting
		tmessage = tmessage + "If you have any questions, please eMail or call 732-750-9000 x217."
	ENDIF
	IF lTesting
		tmessage = tmessage + CHR(13)+"This is TEST DATA only..."
	ENDIF
	IF lEmail
		DO FORM m:\dev\frm\dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
	ENDIF

	INSERT INTO SFTPJOBS (jobname) VALUES ("ANF_PUT_TO_AMBERROAD")
	USE IN SFTPJOBS
	WAIT CLEAR
	WAIT WINDOW cGroupName+" 856 EDI File output complete" TIMEOUT 1
	CLOSETABLES()
ENDPROC