set step on

public xfile

lcPath ="f:\ftpusers\kg-ca\856in\"

lnNum = ADIR(tarray,lcPath+"*.*")
IF lnNum = 0
  WAIT WINDOW "No files found...exiting" TIMEOUT 2
  CLOSE DATA ALL
  schedupdate()
  _SCREEN.CAPTION="Kurt Geiger Packing List Upload.............."
  ON ERROR
  RETURN
Else
 For thisfile = 1  To lnNum
  cfilename = ALLTRIM(tarray[thisfile,1])
  xfile = lcpath+cfilename
  loadfile()
  next 
Endif


******************************************************************
Procedure loadfile 
Create Cursor x943 (;
whseid char(4),;
status char(1),;
rcvdate char(8),;
ibd char(12),;
ponum char(9),;
supplier char(9),;
suppname char(30),;
style char(10),;
prodname char(30),;
upc char(13),;
qty char(10))

Select x943
Set Step On 

append from &xfile Type csv
endproc