*!* m:\dev\prg\rowone940_main.prg

PARAMETERS cOfficeIn

*runack("MODERNSHOE940")

PUBLIC nAcctNum,cDelimiter,nRun_num,cTranslateOption,cWhichFile,cOffice,cCustname,cPropername,cStorenum,lOK,cMod,cISA_Num
PUBLIC cFilter,nRunID,lComplete,lTesting,m.UCCNUMBER,lEmail,lTestImport,lTestRun,emailcommentstr,cUseFolder,nFileSize
PUBLIC ptid,ptctr,ptqty,xfile,cfilename,nUploadCount,pickticket_num_start,pickticket_num_end,nFileCount,units,cUCCNumber
PUBLIC lcPath,lcArchivepath,tsendto,tcc,NormalExit,lTestmail,LogCommentStr,lPick,lPrepack,tsendtoerr,tccerr,lImported,lLoadSQL
PUBLIC m.addby,m.adddt,m.addproc,cTransfer,tfrom,tattach,thisfile,tsendtotest,tcctest,cISAFiles
PUBLIC ARRAY a856(1)
CLOSE DATABASES ALL

TRY
	NormalExit = .F.

	lTesting = .f.
	lTestImport = lTesting
	lTestRun = lTesting
	lTestmail = lTesting
	lOverridebusy = lTesting
*	lOverridebusy = .t.

	DO m:\dev\prg\_setvars WITH lTesting

	IF VARTYPE(cOfficeIn)="L"
		cOfficeIn = "C"
	ENDIF
	cCustname = "ROWONE"
	cMailName = "ROW ONE"
	cUseName = "ROWONE"
	cPropername = PROPER(cMailName)

	_SCREEN.CAPTION = cMailName+" 940 Process"
	_SCREEN.WINDOWSTATE = IIF(lTesting,2,1)

	ASSERT .F. MESSAGE "At start of Row One 940 processing"
	IF !lTesting
	SELECT 0
	USE F:\edirouting\ftpsetup SHARED
	cTransfer = "940-ROWONE"
	LOCATE FOR ftpsetup.transfer = cTransfer AND ftpsetup.TYPE = "GET"
	IF ftpsetup.chkbusy AND !lOverridebusy
		WAIT WINDOW "Process is flagged busy...returning" TIMEOUT 3
		NormalExit = .T.
		THROW
	ELSE
		REPLACE chkbusy WITH .T.,trig_time WITH DATETIME() ;
			FOR ftpsetup.TYPE = "GET" ;
			AND ftpsetup.transfer = cTransfer ;
			IN ftpsetup
		USE IN ftpsetup
	endif
	ENDIF

	lEmail = .T.
	LogCommentStr = ""
	nAcctNum  = 6468
	cISAFiles = ""
	nPTQtyRec = 0
	STORE cOfficeIn TO cOffice
	cMod = IIF(cOffice = "C","5",cOffice)
	gOffice = cMod
	IF lTesting
	gOffice = "P"
	cMod = "P"
	gMasterOffice = "P"
	endif
	lLoadSQL = .t.
SET STEP ON 

	tfrom = "Toll EDI Operations <toll-edi-ops@tollgroup.com>"
	STORE " " TO tattach,tsendto,tcc

	SELECT 0
	USE F:\3pl\DATA\mailmaster ALIAS mm SHARED
	LOCATE FOR mm.edi_type = "940" AND mm.accountid = nAcctNum AND mm.office = cOffice
	IF FOUND()
*		STORE TRIM(mm.acctname) TO cCustname
		STORE TRIM(mm.basepath) TO lcPath
		STORE TRIM(mm.archpath) TO lcArchivepath
		_SCREEN.CAPTION = ALLT(mm.scaption)
*!*			STORE mm.testflag 	   TO lTest
		STORE TRIM(IIF(mm.use_alt,mm.sendtoalt,mm.sendto)) TO tsendto
		STORE TRIM(IIF(mm.use_alt,mm.ccalt,mm.cc)) TO tcc
		LOCATE
		LOCATE FOR mm.edi_type = "MISC" AND mm.taskname = "GENERAL"
		STORE TRIM(IIF(mm.use_alt,mm.sendtoalt,mm.sendto)) TO tsendtotest
		STORE TRIM(IIF(mm.use_alt,mm.ccalt,mm.cc)) TO tcctest
		tsendtoerr = tsendtotest
		tccerr = tcctest
		USE IN mm
	ELSE
		USE IN mm
		WAIT WINDOW AT 10,10  "No parameters set for this acct# "+ALLTRIM(STR(Acctnum))+"  ---> Office "+lcOffice TIMEOUT 2
		RELEASE ALL
		NormalExit = .F.
		THROW
	ENDIF
	lcPath = IIF(lTesting,lcPath+"test\",lcPath)

	CLEAR
	WAIT WINDOW "Now setting up "+cPropername+" 940 process..." TIMEOUT 2

	DO m:\dev\prg\createx856a
	SELECT x856

	cDelimiter = "*"
	cTranslateOption = "NOTILDE"
*	cTranslateOption = IIF(DATETIME()<DATETIME(2016,07,19,13,00,00),"NOTILDE","MODERNSHOE")
	cfile = ""

	dXdate1 = ""
	dXxdate2 = DATE()
	nFileCount = 0

	nRun_num = 999
	lnRunID = nRun_num

	xReturn="XXX"
	DO m:\dev\prg\wf_alt WITH cOffice,nAcctNum
	cUseFolder=xReturn

	DO ("m:\dev\PRG\"+cUseName+"940_PROCESS")

	SELECT 0
	USE F:\edirouting\ftpsetup SHARED
	REPLACE chkbusy WITH .F. ;
		FOR ftpsetup.TYPE = "GET" ;
		AND ftpsetup.transfer = cTransfer ;
		IN ftpsetup
	NormalExit = .T.

CATCH TO oErr
	IF NormalExit = .F.
		ASSERT .F. MESSAGE "In Catch section..."
		tsubject = "Row One 940 Upload Error ("+TRANSFORM(oErr.ERRORNO)+") at "+TTOC(DATETIME())
		tsendto  = tsendtoerr
		tcc = tccerr
		tmessage = "Row One 940 Upload Error..... Please fix me........!"
		lcSourceMachine = SYS(0)
		lcSourceProgram = "from rowone940 group"

		tmessage =tmessage+ "Try/CATCH Exeception Message:"+CHR(13)+;
			[  Error: ] + STR(oErr.ERRORNO) +CHR(13)+;
			[  LineNo: ] + STR(oErr.LINENO) +CHR(13)+;
			[  Message: ] + oErr.MESSAGE +CHR(13)+;
			[  Procedure: ] + oErr.PROCEDURE +CHR(13)+;
			[  Details: ] + oErr.DETAILS +CHR(13)+;
			[  StackLevel: ] + STR(oErr.STACKLEVEL) +CHR(13)+;
			[  LineContents: ] + oErr.LINECONTENTS+CHR(13)+;
			[  UserValue: ] + oErr.USERVALUE+CHR(13)+;
			[  Computer:  ] +lcSourceMachine+CHR(13)+;
			[  940 file:  ] +xfile+CHR(13)+;
			[  Program:   ] +lcSourceProgram

		DO FORM m:\dev\frm\dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
	ELSE
		WAIT WINDOW AT 10,10 "Normal Exit " TIMEOUT 2
	ENDIF
FINALLY
	ON ERROR
	SET STATUS BAR ON
	CLOSE DATABASES ALL
ENDTRY
