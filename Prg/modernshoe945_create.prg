*!* MODERN SHOE 945 All Pack Types Creation Program
*!* Creation Date: 02.26.2010, Joe

PARAMETERS cBOL,nAcctNum,cOffice
PUBLIC ARRAY thisarray(1)
PUBLIC c_CntrlNum,c_GrpCntrlNum,cReturned,cWO_Num,cWO_NumStr,nWO_Num1,lMSFilesOut,lPrepack
PUBLIC nUnitsum,nCtnCount,tsendto,tcc,tsendtoerr,tccerr,lTesting,lTestinput,nPTCount
PUBLIC nOrigSeq,nOrigGrpSeq,cProgname,cISA_Num,cRefBOL,cMailName,lHighline,lAlaguna,cTestBOL
PUBLIC cMod,cMBOL,cEDIType,lParcelType,cFilenameShort,cFilenameOut,cFilenameArch,lcPath,cWO_NumList,cISA_Num

WAIT WINDOW "Modern Shoe Group 945 Process" TIMEOUT 1

lTesting   = .f.  && Set to .t. for testing
lTestinput = .f. &&  Set to .t. for test input files only!
lEmail = .f. && !lTesting

DO m:\dev\prg\_setvars WITH lTesting
SET DELETED ON
cProgname = "modernshoe945_create"
IF lTesting
	ASSERT .F. MESSAGE "At beginning of Modernshoe 945 creation"
ENDIF
SET ESCAPE ON
ON ESCAPE CANCEL
*ON ERROR debug
lIsError = .F.
lDoCatch = .T.
cShip_ref = ""
lCloseOutput = .T.
STORE 0 TO nFilenum,nOrigSeq,nOrigGrpSeq
cCarrierType = "M" && Default as "Motor Freight"
ccustname = ""
cWO_NumStr = ""
cWO_NumList = ""
cStatus=""
cRefBOL = '9999'
lHighline = .F.
lAlaguna = .F.

TRY
	lFederated = .F.
	cPPName = "ModernShoe"
	tfrom = "TGF Warehouse Operations <warehouse-ops@tollgroup.com>"
	tmessage=""
	tcc = ""
	lDoBOLGEN = .F.

	lISAFlag = .T.
	lSTFlag = .T.
	nLoadid = 1
	lSQLMail = .F.
	lDoCompare = IIF(lTesting OR lTestinput,.F.,.T.)

	lDoCompare = .F.

	IF VARTYPE(cBOL) = "L"
		IF !lTesting AND !lTestinput && AND DATE()<>{^2006-10-20}
			WAIT WINDOW "Office not provided...terminating" TIMEOUT 3
			lCloseOutput = .F.
			DO ediupdate WITH "No OFFICE",.T.
			THROW
		ELSE
			CLOSE DATABASES ALL
			IF !USED('edi_trigger')
				cEDIFolder = "F:\3PL\DATA\"
				USE (cEDIFolder+"edi_trigger") IN 0 ALIAS edi_trigger
			ENDIF
			DO m:\dev\prg\lookups
*!* TEST DATA AREA
			cBOL = "1ZX133V00255010011"
			cOffice = "C"
			cTime = DATETIME()
			lFedEx = .F.
			nAcctNum = 5910
		ENDIF
	ENDIF


	cBOL=TRIM(cBOL)
	lOverflow = IIF(cBOL='04907315864913864',.T.,.F.)
	cTestBOL = '1ZX5677488738912'
	cOffice = IIF(cOffice= "I","N",cOffice)
	cMod = IIF(cOffice = "C","2","I")
	cMBOL = ""
	cEDIType = "945"
	lParcelType = .F.
	gOffice = cMod
	gMasterOffice = cOffice

	CREATE CURSOR temp945 (accountid i,isa_num c(10),st_num c(10),wo_num i,bol_no c(20),ship_ref c(20),filename c(50),filedate T)

	swc_cutctns(cBOL)
	cSCAC = ""


	IF USED('OUTSHIP')
		USE IN outship
	ENDIF

	csq1 = [select * from outship where accountid in (]+gtkohighlineaccounts+[) and office = ']+cOffice+[' and bol_no = ']+cBOL+[']
	xsqlexec(csq1,,,"wh")
	SELECT outship

	IF RECCOUNT() = 0
		SET STEP ON
		cErrMsg = "MISS BOL "+cBOL
		DO ediupdate WITH cErrMsg,.T.
		THROW
	ENDIF

	INDEX ON bol_no TAG bol_no
	INDEX ON ship_ref TAG ship_ref
	INDEX ON wo_num TAG wo_num

	IF SEEK(cBOL,'outship','bol_no')
		cSCAC = TRIM(outship.scac)
	ELSE
		SET STEP ON
		DO ediupdate WITH "BAD SCAC",.T.
		THROW
	ENDIF

	SELECT DISTINCT ship_ref FROM outship WHERE bol_no = cBOL INTO CURSOR temp1ms
	SELECT temp1ms
	SELECT 0
	CREATE CURSOR ovpts (outshipid N(10))
	SELECT ovpts
	INDEX ON outshipid TAG outshipid

	SELECT temp1ms
	SCAN
		cShip_ref = ALLTRIM(temp1ms.ship_ref)
		SELECT outship
		SCAN FOR outship.ship_ref = cShip_ref
			IF "OV"$outship.ship_ref
				INSERT INTO ovpts (outshipid) VALUES (outship.outshipid)
				WAIT WINDOW "Overflow PT Found at Ship_ref: "+ALLTRIM(outship.ship_ref) NOWAIT
				EXIT
			ENDIF
		ENDSCAN
	ENDSCAN
	USE IN temp1ms
	cRefBOL = IIF(lOverflow,cBOL,"9999")

	SELECT bol_no,wo_num ;
		FROM outship WHERE bol_no = cBOL ;
		AND INLIST(accountid,&gtkohighlineaccounts) ;
		GROUP BY 1,2 ;
		ORDER BY 1,2 ;
		INTO CURSOR sqldc
	LOCATE
	SCAN
		nWO_Num = sqldc.wo_num
		IF lDoCompare
			cRetMsg = "X"
			DO m:\dev\prg\sqldata-COMPARE WITH "",cBOL,nWO_Num,cOffice,nAcctNum,cPPName
			IF cRetMsg<>"OK"
				lCloseOutput = .T.
				cWO_Num = ALLTRIM(STR(nWO_Num1))
				DO ediupdate WITH "QTY COMPARE ERROR",.T.
				THROW
			ENDIF
		ENDIF
	ENDSCAN

	IF USED('parcel_carriers')
		USE IN parcel_carriers
	ENDIF
	IF USED('carriers')
		USE IN carriers
	ENDIF
	USE F:\3pl\DATA\parcel_carriers IN 0 ALIAS parcel_carriers

	lParcelType = IIF(SEEK(cSCAC,'parcel_carriers','scac'),.T.,.F.)
	cCarrierType = IIF(lParcelType,"U","M")
	lParcelType = IIF(cBOL = '1Z096FW50395029984',.F.,lParcelType)
	lParcelType = IIF(cBOL = outship.keyrec,.f.,lParcelType)
	USE IN parcel_carriers

	IF lParcelType AND LEN(ALLTRIM(cBOL)) = 17
		DO ediupdate WITH "PARCEL WITH WRONG BOL#",.T.
		THROW
	ENDIF

	IF TYPE("nWO_Num")<> "N"
		lCloseOutput = .T.
		DO ediupdate WITH "BAD WO#",.T.
		THROW
	ENDIF
	cWO_Num = ALLTRIM(STR(nWO_Num))

	lTestMail = lTesting && Sends mail to Joe only
	lMSFilesOut = !lTesting && If true, copies output to FTP folder (default = .t.)
	lStandalone = lTesting

*!*		lTestMail = .T.
*!*		lMSFilesOut = .F.

	STORE "LB" TO cWeightUnit
	lPrepack = .T.

	WAIT WINDOW "Now preparing tables and variables" NOWAIT NOCLEAR

*!* SET CUSTOMER CONSTANTS
	ccustname = "MODERNSHOE"  && Customer Identifier
	cX12 = "004050"  && X12 Standards Set used
	cMailName = "Modern Shoe"
	SELECT 0
	USE F:\3pl\DATA\mailmaster ALIAS mm SHARED
	IF lTesting OR lTestMail
		LOCATE FOR mm.edi_type = "MISC" AND mm.taskname = "GENERAL"
		lUseAlt = mm.use_alt
		tsendto = IIF(lUseAlt,mm.sendtoalt,mm.sendto)
		tcc = IIF(lUseAlt,mm.ccalt,mm.cc)
		tsendtoerr = tsendto
		tccerr = tcc
	ELSE
		LOCATE FOR mm.edi_type = "945" AND mm.office = "C" AND mm.accountid = 4752
		IF !FOUND()
			DO ediupdate WITH "MAILMASTER ACCT PROBLEM",.T.
			THROW
		ENDIF
		lUseAlt = mm.use_alt
		tsendto = IIF(lUseAlt,mm.sendtoalt,mm.sendto)
		tcc = IIF(lUseAlt,mm.ccalt,mm.cc)
		LOCATE
		LOCATE FOR office = 'X' AND accountid = 9999
		lUseAlt = mm.use_alt
		tsendtoerr = IIF(lUseAlt,mm.sendtoalt,mm.sendto)
		tccerr = IIF(lUseAlt,mm.ccalt,mm.cc)
	ENDIF
	USE IN mm

*!* SET OTHER CONSTANTS
	DO CASE
		CASE cOffice = "M"
			cCustLoc =  "FL"
			cFMIWarehouse = ""
			cCustPrefix = "945"+"f"
			cDivision = "Florida"
			cFolder = "WHM"
			cSF_Addr1  = "11400 NW 32ND AVE"
			cSF_CSZ    = "MIAMI+FL+33167"

		CASE cOffice = "N"
			cCustLoc =  "NJ"
			cFMIWarehouse = ""
			cCustPrefix = "945"+"j"
			cDivision = "New Jersey"
			cFolder = "WHN"
			cSF_Addr1  = "800 FEDERAL BLVD"
			cSF_CSZ    =  "CARTERET+NJ+07008"

		OTHERWISE
			cCustLoc =  "CA"
			cFMIWarehouse = ""
			cCustPrefix = "945"+"c"
			cDivision = "California"
			cFolder = "WHC"
			cSF_Addr1  = "450 WESTMONT DRIVE"
			cSF_CSZ    = "SAN PEDRO+CA+90731"
	ENDCASE
	cCustFolder = UPPER(ccustname)
	lNonEDI = .F.  && Assumes not a Non-EDI 945
	STORE "" TO lcKey
	STORE 0 TO alength,nLength
	cTime = DATETIME()
	cDelTime = SUBSTR(TTOC(cTime,1),9,4)
	cApptNum = ""

*!* SET OTHER CONSTANTS
	dt1 = TTOC(DATETIME(),1)
	dtmail = TTOC(DATETIME())
	dt2 = DATETIME()
	cString = ""

	csendqual = "ZZ"
	csendid = "FMI"
	crecqual = "12"
	crecid = "6173337475"

	cfd = "*"
	csegd = CHR(133)
	nSegCtr = 0
	cterminator = ">" && Used at end of ISA segment
	cdate = DTOS(DATE())
	cTruncDate = RIGHT(cdate,6)
	cTruncTime = SUBSTR(TTOC(DATETIME(),1),9,4)
	cfiledate = cdate+cTruncTime
	cOrig = "J"
	nSTCount = 0
	cStyle = ""
	cPTString = ""
	nTotCtnWt = 0
	nTotCtnCount = 0
	nUnitsum = 0
	cTargetStyle = ""

** PAD ID Codes
	csendidlong = PADR(csendid,15," ")
	crecidlong = PADR(crecid,15," ")

	lcPath = ("f:\ftpusers\"+cCustFolder+"\945OUT\")
	cFilenameHold = ("f:\ftpusers\"+cCustFolder+"\945-Staging\"+cCustPrefix+dt1+".txt")
	cFilenameShort = JUSTFNAME(cFilenameHold)
	cFilenameOut = (lcPath+cFilenameShort)
	cFilenameArch = ("f:\ftpusers\"+cCustFolder+"\945OUT\945archive\"+cFilenameShort)
	nFilenum = FCREATE(cFilenameHold)

	IF USED('SHIPMENT')
		USE IN shipment
	ENDIF

	IF USED('PACKAGE')
		USE IN package
	ENDIF

	gOffice=cOffice

	dDate = DATE()-5
	csq1 = [select * from shipment where right(rtrim(str(accountid)),4) in (]+gtkohighlineaccounts+[) and shipdate >= {]+DTOC(dDate)+[}]
	xsqlexec(csq1,,,"wh")
	INDEX ON shipmentid TAG shipmentid
	INDEX ON pickticket TAG pickticket

	SELECT shipment
	LOCATE
	IF RECCOUNT("shipment") > 0
		xjfilter="shipmentid in ("
		SCAN
			nShipmentId = shipment.shipmentid
			xsqlexec("select * from package where shipmentid="+TRANSFORM(nShipmentId),,,"wh")

			IF RECCOUNT("package")>0
				xjfilter=xjfilter+TRANSFORM(shipmentid)+","
			ELSE
				xjfilter="1=0"
			ENDIF
		ENDSCAN
		xjfilter=LEFT(xjfilter,LEN(xjfilter)-1)+")"

		xsqlexec("select * from package where "+xjfilter,,,"wh")
	ELSE
		xsqlexec("select * from package where .f.",,,"wh")
	ENDIF
	SELECT package
	INDEX ON shipmentid TAG shipmentid
	SET ORDER TO TAG shipmentid
	
	SELECT outship
	IF !SEEK(cBOL,'outship','bol_no')
		WAIT WINDOW "BOL not found in OUTSHIP" TIMEOUT 2
		DO ediupdate WITH "BOL NOT FOUND",.T.
		THROW
	ENDIF

	IF DATE() > {^2015-09-15}
		SELECT dcnum FROM outship WHERE outship.bol_no = cBOL GROUP BY 1 INTO CURSOR tempx
		LOCATE

		IF EOF()
			DO ediupdate WITH "EMPTY DC/STORE#",.T.
			THROW
		ENDIF

		IF EMPTY(tempx.dcnum)
			USE IN tempx
			SELECT storenum FROM outship WHERE outship.bol_no = cBOL GROUP BY 1 INTO CURSOR tempx
			IF EOF()
				DO ediupdate WITH "EMPTY DC/STORE#",.T.
				THROW
			ENDIF

			IF EMPTY(tempx.storenum)
				DO ediupdate WITH "EMPTY DC/STORE#",.T.
				THROW
			ELSE
				IF RECCOUNT('tempx')  > 1 AND cBOL<>"04907314677854548"
					DO ediupdate WITH "MULTIPLE DC #'s",.T.
					THROW
				ENDIF
			ENDIF
		ENDIF

		IF USED('tempx')
			USE IN tempx
		ENDIF
	ENDIF

	IF USED('OUTDET')
		USE IN outdet
	ENDIF

	selectoutdet()
	SELECT outdet
	INDEX ON outdetid TAG outdetid

&& Added code blocks to the following to overcome BOL vs. WO selection issues, 12.23.2005
	SELECT outship
	LOCATE
	IF USED("sqlwo")
		USE IN sqlwo
	ENDIF
	DELETE FILE "F:\3pl\DATA\sqlwo.dbf"
	SELECT bol_no,wo_num ;
		FROM outship ;
		WHERE bol_no = cBOL ;
		AND INLIST(accountid,&gtkohighlineaccounts) ;
		GROUP BY 1,2 ;
		ORDER BY 1,2 ;
		INTO DBF F:\3pl\DATA\sqlwo
	USE IN sqlwo
	SELECT 0
	USE F:\3pl\DATA\sqlwo ALIAS sqlwo
	IF lTesting
		LOCATE
		BROWSE
	ENDIF

	cRetMsg = ""

	DO m:\dev\prg\sqlconnect_bol  WITH nAcctNum,ccustname,cPPName,.T.,cOffice

	IF cRetMsg<>"OK"
		DO ediupdate WITH "SQLCONN: "+cRetMsg,.T.
		THROW
	ENDIF

	SELECT vmodernshoepp
*	brow

	SELECT wo_num FROM vmodernshoepp GROUP BY 1 INTO CURSOR temprunid1
	SELECT runid FROM vmodernshoepp GROUP BY 1 INTO CURSOR temprunid2
	SELECT temprunid1
	STORE RECCOUNT() TO nWORecs
	SELECT temprunid2
	STORE RECCOUNT() TO nRIDRecs

	IF nWORecs#nRIDRecs
		DO ediupdate WITH "MULTIPLE RUNIDS",.T.
		THROW
	ENDIF
	USE IN temprunid1
	USE IN temprunid2

	IF !USED("scacs")
		xsqlexec("select * from scac","scacs",,"wh")
		INDEX ON scac TAG scac
	ENDIF

	SELECT outship
	LOCATE
	WAIT CLEAR
	WAIT WINDOW "Now creating Header information" NOWAIT NOCLEAR

*!* HEADER LEVEL EDI DATA
	DO num_incr_isa

	cISA_Num = PADL(c_CntrlNum,9,"0")
	nISA_Num = INT(VAL(cISA_Num))

	IF lTesting
		cISACode = "T"
	ELSE
		cISACode = "P"
	ENDIF

	SELECT outship
	IF SEEK(cBOL,"outship","bol_no")
		IF "CONAME*HIGHLINE"$outship.shipins OR INLIST(outship.accountid,6668,6669)
			lHighline = .T.
			cMailName = "Highline"
			SELECT 0
			USE F:\3pl\DATA\mailmaster ALIAS mm
			LOCATE FOR mm.edi_type = "945" AND mm.office = "C" AND mm.accountid = 5864
			IF !FOUND()
				DO ediupdate WITH "MAILMASTER HIGHLINE ACCT PROBLEM",.T.
				THROW
			ENDIF
			tsendto = IIF(mm.use_alt,mm.sendtoalt,mm.sendto)
			tcc = IIF(mm.use_alt,mm.ccalt,mm.cc)
			USE IN mm
		ENDIF

		IF "CONAME*NEW ALAGUNA"$outship.shipins AND !BETWEEN(outship.accountid,6668,6671)
			lAlaguna = .T.
			cMailName = "New Alaguna"
			SELECT 0
			USE F:\3pl\DATA\mailmaster ALIAS mm
			LOCATE FOR mm.edi_type = "945" AND mm.office = "C" AND mm.accountid = 6414
			IF !FOUND()
				DO ediupdate WITH "MAILMASTER NEW ALAGUNA ACCT PROBLEM",.T.
				THROW
			ENDIF
			tsendto = IIF(mm.use_alt,mm.sendtoalt,mm.sendto)
			tcc = IIF(mm.use_alt,mm.ccalt,mm.cc)
			USE IN mm
		ENDIF
	ENDIF
	crecidlong = PADR(crecid,15," ")

	STORE "ISA"+cfd+"00"+cfd+"          "+cfd+"00"+cfd+"          "+cfd+csendqual+cfd+csendidlong+cfd+;
		crecqual+cfd+crecidlong+cfd+cTruncDate+cfd+cTruncTime+cfd+"U"+cfd+"00401"+cfd+;
		cISA_Num+cfd+"0"+cfd+cISACode+cfd+cterminator+csegd TO cString
	DO cstringbreak

	STORE "GS"+cfd+"SW"+cfd+csendid+cfd+crecid+cfd+cdate+cfd+cTruncTime+cfd+c_CntrlNum+;
		cfd+"X"+cfd+cX12+csegd TO cString
	DO cstringbreak

	SELECT outship
	LOCATE

	IF !SEEK(nWO_Num,'outship','wo_num')
		WAIT CLEAR
		WAIT WINDOW "Invalid Work Order Number - Not Found in OUTSHIP!" TIMEOUT 2
		DO ediupdate WITH "WO "+TRANSFORM(nWO_Num)+" NOT FOUND",.T.
		THROW
	ENDIF

	IF !lTesting
		IF EMPTY(cBOL)  && May be empty if triggered from PT screen, or FedEx shipment
			WAIT CLEAR
			WAIT WINDOW "MISSING Bill of Lading Number!" TIMEOUT 2
			DO ediupdate WITH "BOL# EMPTY",.T.
			THROW
		ELSE
			IF !SEEK(PADR(TRIM(cBOL),20),"outship","bol_no")
				WAIT CLEAR
				WAIT WINDOW "Invalid Bill of Lading Number - Not Found!" TIMEOUT 2
				DO ediupdate WITH "BOL# NOT FOUND",.T.
				THROW
			ENDIF
		ENDIF
	ENDIF

*************************************************************************
*1	PTIDSOURCE/OUTSHIP RECORD MAIN LOOP
*	Loops through individual OUTSHIP lines for BOL#
*************************************************************************
	WAIT CLEAR
	WAIT WINDOW "Now creating BOL#-based information..." NOWAIT NOCLEAR

	SELECT outship
	
	IF !lTesting AND outship.bol_no # cTestBOL
		COUNT TO N FOR outship.bol_no = cBOL AND !EMPTYnul(del_date)
	ELSE
		COUNT TO N FOR outship.bol_no = cBOL
	ENDIF

	IF N=0
		DO ediupdate WITH "INCOMP BOL",.T.
		THROW
	ENDIF

	LOCATE
	cMissDel = ""

	IF cBOL = cTestBOL && DS Test BOL
		oscanstr = "outship.bol_no = PADR(cBOL,20) AND INLIST(outship.accountid,&gtkohighlineaccounts)"
	ELSE
		oscanstr = "outship.bol_no = PADR(cBOL,20) AND !EMPTYnul(outship.del_date) AND INLIST(outship.accountid,&gtkohighlineaccounts)"
	ENDIF

	SELECT outship
	nPTCount = 0
	SCAN FOR &oscanstr
		IF "OV"$outship.ship_ref
			LOOP
		ENDIF
		lPrepack = !outship.picknpack

		lAmazon = IIF("AMAZON"$outship.consignee,.T.,.F.)
		lZappo = IIF("ZAPPO"$outship.consignee,.T.,.F.)
		SCATTER MEMVAR MEMO
		IF ("NOSEND945"$outship.shipins)
			DO ediupdate WITH "JILDOR NO 945",.F.
			THROW
		ENDIF

		nWO_Num = m.wo_num
		cWO_Num = ALLTRIM(STR(nWO_Num))
		nAcctNum = m.accountid
		IF !(cWO_Num$cWO_NumStr)
			cWO_NumStr = IIF(EMPTY(cWO_NumStr),cWO_Num,cWO_NumStr+","+cWO_Num)
		ENDIF
		IF !(cWO_Num$cWO_NumList)
			cWO_NumList = IIF(EMPTY(cWO_NumList),cWO_Num,cWO_NumList+CHR(13)+cWO_Num)
		ENDIF

		cPO_Num = ALLTRIM(m.cnee_ref)
		cShip_ref = ALLTRIM(m.ship_ref)
		nPTCount = nPTCount+1

		IF lParcelType
			nid = 0
			SELECT ucc,100 AS xid FROM vmodernshoepp WHERE vmodernshoepp.ship_ref = cShip_ref GROUP BY 1 INTO CURSOR tempuccs READWRITE
			SELECT tempuccs
			LOCATE
			SCAN
				nid = nid + 1
				REPLACE tempuccs.xid WITH nid IN tempuccs
			ENDSCAN
		ENDIF

		SELECT outship
*!* Added this code to trap miscounts in OUTDET/SQL\
		WAIT WINDOW "Now summing outdet/SQL by outshipid" NOWAIT

		IF lPrepack
			IF SEEK(outship.outshipid,'ovpts','outshipid') OR lOverflow
				SET DELETED OFF
				cRefBOL = cBOL
			ELSE
				SET DELETED ON
			ENDIF
			SELECT outdet
			IF SET("Deleted") = "OFF"
				SUM origqty TO nUnitTot1 ;
					FOR !units ;
					AND outdet.outshipid = outship.outshipid ;
					AND outdet.wo_num = nWO_Num
			ELSE
				SUM totqty TO nUnitTot1 ;
					FOR !units ;
					AND outdet.outshipid = outship.outshipid ;
					AND outdet.wo_num = nWO_Num
			ENDIF
			SELECT vmodernshoepp
			IF INLIST(nWO_Num,5049591)
				SUM (totqty/INT(VAL(PACK))) TO nUnitTot2 FOR vmodernshoepp.outshipid = outship.outshipid
			ELSE
				COUNT TO nUnitTot2 FOR vmodernshoepp.outshipid = outship.outshipid AND vmodernshoepp.totqty > 0
			ENDIF

			IF nUnitTot1<>nUnitTot2
				SET STEP ON
				ASSERT .F. MESSAGE "At SQL UNITQTY ERR"
				DO ediupdate WITH "SQL UNITQTY ERR-OSID "+TRANSFORM(outship.outshipid),.T.
				THROW
			ENDIF
		ELSE
*			ASSERT .f. MESSAGE "At OSID Qty scanning main loop"
			SELECT outdet
			SUM totqty TO nUnitTot1 ;
				FOR units ;
				AND outdet.outshipid = outship.outshipid ;
				AND outdet.wo_num = nWO_Num
			SELECT vmodernshoepp
			SUM totqty TO nUnitTot2 ;
				FOR vmodernshoepp.outshipid = outship.outshipid ;
				AND vmodernshoepp.totqty > 0
			IF nUnitTot1<>nUnitTot2
				ASSERT .F. MESSAGE "At SQL TOTQTY ERR"
				DO ediupdate WITH "SQL TOTQTY ERR-PIK",.T.
				THROW
			ENDIF
		ENDIF
		SELECT outdet
		SET ORDER TO outdetid
		LOCATE

*!* End code addition

		IF ("PENNEY"$UPPER(outship.consignee) OR "KMART"$UPPER(outship.consignee) OR "K-MART"$UPPER(outship.consignee) OR "AMAZON"$UPPER(outship.consignee) OR "ZAPPO"$UPPER(outship.consignee))
			lApptFlag = .T.
		ELSE
			lApptFlag = .F.
		ENDIF
		IF lTestinput
			ddel_date = DATE()
			cApptNum = "99999"
			dapptdate = DATE()
		ELSE
			ddel_date = outship.del_date
			IF EMPTY(ddel_date)
				IF cBOL = cTestBOL
					ddel_date = DATE()
				ELSE
					cMissDel = IIF(EMPTY(cMissDel),"The following PTs had no Delivery Dates:"+CHR(13)+TRIM(cShip_ref),cMissDel+CHR(13)+TRIM(cShip_ref))
				ENDIF
			ENDIF
			cApptNum = ALLTRIM(outship.appt_num)

			IF EMPTY(cApptNum) AND !lParcelType && Penney/KMart Appt Number check
				IF (!(lApptFlag) OR (DATE()={^2006-05-18} AND cBOL = "04907314677812395"))
					cApptNum = ""
				ELSE
					DO ediupdate WITH "EMPTY APPT #",.T.
					THROW
				ENDIF
			ENDIF
			dapptdate = outship.appt

			IF EMPTY(dapptdate)
				IF cBOL = cTestBOL
					dapptdate = DATE()
				ELSE
					dapptdate = outship.del_date
				ENDIF
			ENDIF
		ENDIF

		IF ALLTRIM(outship.SForCSZ) = ","
			BLANK FIELDS outship.SForCSZ NEXT 1 IN outship
		ENDIF

		alength = ALINES(apt,outship.shipins,.T.,CHR(13))

		cTRNum = ""
		cPRONum = ""

		IF (("WALMART"$outship.consignee) OR ("WAL-MART"$outship.consignee) ;
				OR ("WAL MART"$outship.consignee) OR ("AMAZON"$outship.consignee) OR ("ZAPPO"$outship.consignee) OR m.scac = "ABFS")
			cKeyRec = ALLTRIM(outship.keyrec)
			cKeyRec = TRIM(STRTRAN(cKeyRec,"#",""))
			cPRONum = TRIM(STRTRAN(cKeyRec,"PR",""))
			cPRONum =  IIF(ISALPHA(SUBSTR(cPRONum,LEN(cPRONum),1)),LEFT(cPRONum,LEN(cPRONum)-1),cPRONum)

			IF EMPTY(cPRONum)
				IF lAmazon OR lZappo
					DO ediupdate WITH "REQUIRED-EMPTY PRO #",.T.
					THROW
				ENDIF

				IF lParcelType
					cPRONum = ALLTRIM(outship.bol_no)
				ENDIF
			ENDIF
		ENDIF

		IF cBOL = "99911111111111111"
			cShip_ref = ALLTRIM(STRTRAN(cShip_ref,"OVER",""))
		ENDIF

		nOutshipid = m.outshipid
		IF lParcelType
			cPTString = IIF(EMPTY(cPTString),m.consignee+" "+cBOL+" "+STRTRAN(cShip_ref,"DS",""),cPTString+CHR(13)+m.consignee+" "+cBOL+" "+STRTRAN(cShip_ref,"DS",""))
		ELSE
			cPTString = IIF(EMPTY(cPTString),m.consignee+" "+STRTRAN(cShip_ref,"DS",""),cPTString+CHR(13)+m.consignee+" "+STRTRAN(cShip_ref,"DS",""))
		ENDIF
		m.CSZ = TRIM(m.CSZ)
		DO CASE
			CASE m.CSZ = 'HAYWARD ,CA 94545'
				m.CSZ = 'HAYWARD, CA 94545'
			CASE m.CSZ = "CITY OF INDUSTRY ,CA 91745"
				m.CSZ = "CITY OF INDUSTRY, CA 91745"
			CASE m.CSZ = ","
				DO ediupdate WITH "MISSING CSZ",.T.
				THROW
			CASE !(", "$m.CSZ)
				DO ediupdate WITH "UNSPACED COMMA IN CSZ",.T.
				THROW
		ENDCASE

		IF EMPTY(M.CSZ)
			WAIT CLEAR
			WAIT WINDOW "No SHIP-TO City/State/ZIP info...exiting" TIMEOUT 2
			DO ediupdate WITH "NO CSZ ADDRESS INFO",.T.
			THROW
		ENDIF
		m.CSZ = ALLTRIM(STRTRAN(m.CSZ,"  "," "))
		nSpaces = OCCURS(" ",ALLTRIM(m.CSZ))
		IF nSpaces = 0
			WAIT WINDOW "SHIP-TO CSZ Segment Error" TIMEOUT 3
			DO ediupdate WITH "SHIP-TO CSZ ERR: "+cShip_ref,.T.
			THROW
		ELSE
			nCommaPos = AT(",",m.CSZ)
			nLastSpace = AT(" ",m.CSZ,nSpaces)
			IF ISALPHA(SUBSTR(TRIM(m.CSZ),AT(" ",m.CSZ,nSpaces-1)+1,2))
				cCity = LEFT(TRIM(m.CSZ),AT(",",m.CSZ)-1)
				cState = SUBSTR(m.CSZ,nCommaPos+2,2)
				cZip = TRIM(SUBSTR(m.CSZ,nLastSpace+1))
			ELSE
				WAIT CLEAR
				WAIT WINDOW "NOT ALPHA: "+SUBSTR(TRIM(m.CSZ),AT(" ",m.CSZ,nSpaces-1)+1,2) TIMEOUT 3
				cCity = LEFT(TRIM(m.CSZ),AT(",",m.CSZ)-1)
				cState = SUBSTR(TRIM(m.CSZ),AT(" ",m.CSZ,nSpaces-2)+1,2)
				cZip = TRIM(SUBSTR(m.CSZ,nLastSpace+1))
			ENDIF
		ENDIF

		STORE "" TO cSForCity,cSForState,cSForZip
		cStoreName = segmentget(@apt,"STORENAME",alength)
		IF !lFederated
			IF !EMPTY(M.SForCSZ)
				m.SForCSZ = ALLTRIM(STRTRAN(m.SForCSZ,"  "," "))
				nSpaces = OCCURS(" ",ALLTRIM(m.SForCSZ))
				IF nSpaces = 0
					m.SForCSZ = STRTRAN(m.SForCSZ,",","")
					cSForCity = ALLTRIM(m.SForCSZ)
					cSForState = ""
					cSForZip = ""
				ELSE
					nCommaPos = AT(",",m.SForCSZ)
					nLastSpace = AT(" ",m.SForCSZ,nSpaces)
					nMinusSpaces = IIF(nSpaces=1,0,1)
					IF ISALPHA(SUBSTR(TRIM(m.SForCSZ),AT(" ",TRIM(m.SForCSZ),nSpaces-nMinusSpaces)+1,2))
						cSForCity = LEFT(TRIM(m.SForCSZ),AT(",",m.SForCSZ)-1)
						cSForState = SUBSTR(m.SForCSZ,nCommaPos+2,2)
						cSForZip = ALLTRIM(m.SForCSZ)
						IF ISALPHA(cSForZip)
							cSForZip = ""
						ENDIF
					ELSE
						WAIT CLEAR
						WAIT WINDOW "NOT ALPHA: "+SUBSTR(TRIM(m.SForCSZ),AT(" ",m.SForCSZ,nSpaces-1)+1,2) TIMEOUT 3
						cSForCity = LEFT(TRIM(m.SForCSZ),AT(",",m.SForCSZ)-1)
						cSForState = SUBSTR(TRIM(m.SForCSZ),AT(" ",m.SForCSZ,nSpaces-2)+1,2)
						cSForZip = ALLTRIM(m.SForCSZ)
						IF ISALPHA(cSForZip)
							cSForZip = ""
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ELSE
			STORE "" TO cCity,cState,cZip
		ENDIF


		DO num_incr_st
		WAIT CLEAR
		WAIT WINDOW "Now creating Line Item information" NOWAIT NOCLEAR

		INSERT INTO temp945 (accountid,isa_num,st_num,wo_num,bol_no,ship_ref,filename,filedate) ;
			VALUES (m.accountid,c_CntrlNum,c_GrpCntrlNum,m.wo_num,m.bol_no,m.ship_ref,cFilenameShort,dt2)

		STORE "ST"+cfd+"945"+cfd+PADL(c_GrpCntrlNum,9,"0")+csegd TO cString
		DO cstringbreak
		nSTCount = nSTCount + 1
		nSegCtr = 1

		cShip_refW06 =STRTRAN(cShip_ref,"DS","")
		cShip_refW06 =STRTRAN(cShip_refW06,"D","")

		STORE "W06"+cfd+"N"+cfd+cShip_refW06+cfd+cdate+cfd+TRIM(cWO_Num)+cfd+cfd+cPO_Num+csegd TO cString
		DO cstringbreak
		nSegCtr = nSegCtr + 1
		nCtnNumber = 1  && Seed carton sequence count

		cCountry = segmentget(@apt,"COUNTRY",alength)
		cCountry = ALLT(cCountry)
		IF EMPTY(cCountry)
			cCountry = "US"
		ENDIF

		cStoreNum = segmentget(@apt,"STORENUM",alength)
		cCountry = segmentget(@apt,"COUNTRY",alength)
		IF EMPTY(cCountry)
			cCountry = ALLTRIM(m.country)
		ENDIF
		IF EMPTY(cCountry)
			STORE "N1"+cfd+"ST"+cfd+ALLTRIM(m.consignee)+cfd+"92"+cfd+cStoreNum+cfd+"US"+csegd TO cString
		ELSE
			STORE "N1"+cfd+"ST"+cfd+ALLTRIM(m.consignee)+cfd+"92"+cfd+cStoreNum+cfd+cCountry+csegd TO cString
		ENDIF
		DO cstringbreak
		nSegCtr = nSegCtr + 1
		IF EMPTY(M.ADDRESS2)
			STORE "N3"+cfd+ALLTRIM(M.ADDRESS)+csegd TO cString
		ELSE
			STORE "N3"+cfd+ALLTRIM(M.ADDRESS)+cfd+ALLTRIM(M.ADDRESS2)+csegd TO cString
		ENDIF
		DO cstringbreak
		nSegCtr = nSegCtr + 1
		IF !EMPTY(cSForState)
			STORE "N4"+cfd+cSForCity+cfd+cSForState+cfd+cSForZip+cfd+"US"+csegd TO cString
		ELSE
			STORE "N4"+cfd+cSForCity+csegd TO cString
		ENDIF
		DO cstringbreak
		nSegCtr = nSegCtr + 1

		IF !EMPTY(ALLTRIM(m.shipfor))
			STORE "N1"+cfd+"Z7"+cfd+ALLTRIM(m.shipfor)+cfd+"92"+cfd+ALLTRIM(m.sforstore)+csegd TO cString
			DO cstringbreak
			nSegCtr = nSegCtr + 1
			IF EMPTY(M.ADDRESS2)
				STORE "N3"+cfd+ALLTRIM(M.SFORADDR1)+csegd TO cString
			ELSE
				STORE "N3"+cfd+ALLTRIM(M.SFORADDR1)+cfd+ALLTRIM(M.SFORADDR2)+csegd TO cString
			ENDIF
			DO cstringbreak
			nSegCtr = nSegCtr + 1
			IF !EMPTY(cSForState)
				STORE "N4"+cfd+cSForCity+cfd+cSForState+cfd+cSForZip+cfd+"US"+csegd TO cString
			ELSE
				STORE "N4"+cfd+cSForCity+csegd TO cString
			ENDIF
			DO cstringbreak
			nSegCtr = nSegCtr + 1
		ENDIF

		cSOSite = ""
		cSOSite = ALLT(UPPER(segmentget(@apt,"SOSITE",alength)))
		IF !EMPTY(cSOSite)
		STORE "N1"+cfd+"SO"+cfd+cSOSite+csegd TO cString
		DO cstringbreak
		nSegCtr = nSegCtr + 1
		endif
		
		cBOLUse = IIF(lAmazon AND lParcelType,"PSU"+TRANSFORM(nAcctNum)+PADL(cShip_ref,10,'0'),cBOL)
		STORE "N9"+cfd+"BM"+cfd+TRIM(cBOLUse)+csegd TO cString
		DO cstringbreak
		nSegCtr = nSegCtr + 1

		STORE "N9"+cfd+"DP"+cfd+TRIM(outship.dept)+csegd TO cString
		DO cstringbreak
		nSegCtr = nSegCtr + 1

		IF lParcelType
			cTrkNumber = ALLTRIM(outship.bol_no)
			STORE "N9"+cfd+"TN"+cfd+TRIM(cTrkNumber)+csegd TO cString
			DO cstringbreak
			nSegCtr = nSegCtr + 1
		ENDIF

		IF !EMPTY(cApptNum) AND ("AMAZON"$outship.consignee) OR ("ZAPPO"$outship.consignee) OR("ZAPPO"$outship.consignee) OR ("6PM"$outship.consignee) OR ("DILLARD"$outship.consignee AND !lParcelType)
			IF LEFT(cApptNum,4) = "DISP"
				cApptNum1 = ""
				nApptLen = LEN(cApptNum)
				FOR xx = 1 TO nApptLen
					IF !ISDIGIT(SUBSTR(cApptNum,xx,1))
						LOOP
					ELSE
						cApptNum1 = cApptNum1+SUBSTR(cApptNum,xx,1)
					ENDIF
				ENDFOR
				cApptNum = cApptNum1
			ENDIF
			STORE "N9"+cfd+"P8"+cfd+TRIM(cApptNum)+csegd TO cString
			DO cstringbreak
			nSegCtr = nSegCtr + 1
		ENDIF

		IF "PIPERLIME"$UPPER(outship.consignee) OR "THE GAP"$UPPER(outship.consignee)
			ASSERT .F. MESSAGE "At PIPERLIME/GAP point - debug"
			cLoadID2 = ALLTRIM(outship.appt_num)
			IF EMPTY(cLoadID2)
				DO ediupdate WITH "MISS LOADID",.T.
			ELSE
				cLoadID = ""

				FOR rr = 1 TO LEN(cLoadID2)
					cLoadChar = SUBSTR(cLoadID2,rr,1)
					IF ISDIGIT(cLoadChar)
						cLoadID = cLoadID+cLoadChar
					ENDIF
				ENDFOR

				STORE "N9"+cfd+"P8"+cfd+cLoadID+csegd TO cString
				DO cstringbreak
				nSegCtr = nSegCtr + 1
			ENDIF
		ENDIF

		cCompany = ""
		cCompany = ALLT(UPPER(segmentget(@apt,"COMPANY",alength)))
		IF LEN(cCompany) > 3
			cCompany = ICASE(cCompany="HIGHLINE","100",cCompany="NEW ALAGUNA","005",cCompany="NEW HIGHLINE",cCompany = "HOLT","100","001")
		ENDIF
		STORE "N9"+cfd+"CO"+cfd+TRIM(cCompany)+csegd TO cString
		DO cstringbreak
		nSegCtr = nSegCtr + 1

*!*			cMessage1 = ""
*!*			cMessage1 = segmentget(@apt,"MSG1",alength)
*!*			IF !EMPTY(cMessage1)
*!*			STORE "N9"+cfd+"MSG"+cfd+TRIM(cMessage1)+csegd TO cString
*!*			DO cstringbreak
*!*			nSegCtr = nSegCtr + 1
*!*			endif

*!*			cMessage2 = ""
*!*			cMessage2 = segmentget(@apt,"MSG2",alength)
*!*			IF !EMPTY(cMessage2)
*!*			STORE "N9"+cfd+"MSG"+cfd+TRIM(cMessage2)+csegd TO cString
*!*			DO cstringbreak
*!*			nSegCtr = nSegCtr + 1
*!*			endif

*!*			cMessage3 = ""
*!*			cMessage3 = segmentget(@apt,"MSG3",alength)
*!*			IF !EMPTY(cMessage3)
*!*			STORE "N9"+cfd+"MSG"+cfd+TRIM(cMessage3)+csegd TO cString
*!*			DO cstringbreak
*!*			nSegCtr = nSegCtr + 1
*!*			endif

		cWarehouse = segmentget(@apt,"WAREHOUSE",alength)
		IF EMPTY(cWarehouse)
			DO ediupdate WITH "MISS WHSE CODE",.T.
		ENDIF
		STORE "N9"+cfd+"WH"+cfd+TRIM(cWarehouse)+csegd TO cString
		DO cstringbreak
		nSegCtr = nSegCtr + 1

		ddel_date = IIF(cBOL = cTestBOL,DATE(),outship.del_date)
		cdel_date = DTOS(ddel_date)
		IF EMPTY(ddel_date)
			DO ediupdate WITH "MISSING DEL_DATE",.T.
		ENDIF

		STORE "G62"+cfd+"11"+cfd+cdel_date+csegd TO cString  && Ship date
		DO cstringbreak
		nSegCtr = nSegCtr + 1

		IF lTesting AND EMPTY(m.scac)
			STORE "RDWY" TO m.scac
		ELSE
			IF !EMPTY(TRIM(outship.scac))
				STORE outship.scac TO m.scac
				IF m.scac = "FGC"
					STORE "FDEG" TO m.scac
				ENDIF
			ELSE
				IF (UPPER(LEFT(outship.SHIP_VIA,3)) = "UPS") OR (lParcelType = .T. AND EMPTY(outship.scac))
					REPLACE outship.scac WITH "UPSN" IN outship
					STORE outship.scac TO m.scac
				ELSE
					DO ediupdate WITH "MISSING SCAC",.T.
					THROW
				ENDIF
			ENDIF
		ENDIF

		SELECT outship

		IF !EMPTY(cPRONum)
			STORE "W27"+cfd+cCarrierType+cfd+TRIM(m.scac)+cfd+cPRONum+csegd TO cString
		ELSE
			STORE "W27"+cfd+cCarrierType+cfd+TRIM(m.scac)+csegd TO cString
		ENDIF
		DO cstringbreak
		nSegCtr = nSegCtr + 1

		IF lParcelType
			IF lTesting
				ASSERT .F. MESSAGE "At UPS freight calculation"
			ENDIF
			cFreight = STRTRAN(segmentget(@apt,"UPSCHARGE",alength),".","") && Added per Xperia, 9/6/06, Joe
			IF VAL(cFreight)=0  && Added to double-check freight against UPS files, 04.16.2009, Joe
				SELECT shipment
				IF SEEK(cShip_ref,"shipment","pickticket")
					SELECT package
					SUM package.pkgcharge TO nFreight FOR package.shipmentid=shipment.shipmentid
					cFreight = STRTRAN(ALLTRIM(STR(nFreight,10,2)),".","")
				ENDIF
			ENDIF
			STORE "G72"+cfd+"ITA"+cfd+"15"+REPLICATE(cfd,6)+ALLT(cFreight)+csegd TO cString
			DO cstringbreak
			nSegCtr = nSegCtr + 1

			IF lTesting
*				SET STEP ON
			ENDIF
			nid = 0
			SELECT 100 AS xid,trknumber FROM package WHERE package.shipmentid=shipment.shipmentid INTO CURSOR package2 READWRITE
			SELECT package2
			LOCATE
			SCAN
				nid = nid + 1
				REPLACE package2.xid WITH nid IN package2
			ENDSCAN
			RELEASE nid
			SELECT tempuccs.ucc,package2.trknumber FROM tempuccs,package2 WHERE tempuccs.xid = package2.xid INTO CURSOR package1
			IF lTesting
*				BROWSE
			ENDIF

		ENDIF

*************************************************************************
*2	DETAIL LOOP - MATCHING SHIP_REF FIELD (FROM OUTSHIP)
*************************************************************************
*		ASSERT .F. MESSAGE "In Detail Loop"
		STORE "LB" TO cWeightUnit
		WAIT CLEAR
		WAIT WINDOW "Now creating Detail information "+cShip_ref NOWAIT NOCLEAR
		SELECT vmodernshoepp
		LOCATE
		SELECT outdet
		SET ORDER TO TAG outdetid
		IF cBOL = cRefBOL
			SET DELETED OFF
		ELSE
			SET DELETED ON
		ENDIF
		SELECT vmodernshoepp
		SET RELATION TO outdetid INTO outdet

		LOCATE
		LOCATE FOR ship_ref = TRIM(cShip_ref) AND outshipid = nOutshipid
		IF !FOUND()
			IF !lTesting
				WAIT WINDOW "PT "+cShip_ref+" NOT FOUND in VMODERNSHOEPP...ABORTING" TIMEOUT 2
				IF !lTesting
					lSQLMail = .T.
				ENDIF
				DO ediupdate WITH "MISS PT-SQL: "+cShip_ref,.T.
				THROW
			ELSE
				LOOP
			ENDIF
		ENDIF

		scanstr = "vmodernshoepp.ship_ref = cShip_ref and vmodernshoepp.outshipid = nOutshipid"
		SELECT vmodernshoepp
		LOCATE
		COUNT TO nUCCCount FOR &scanstr

		LOCATE
		LOCATE FOR &scanstr
		cCartonNum= "XXX"

		DO WHILE &scanstr

			IF vmodernshoepp.totqty < 1
				SKIP 1 IN vmodernshoepp
				LOOP
			ENDIF

			lDoManSegment = .T.
			lDoPALSegment = .T.
			alength = ALINES(apt,outdet.printstuff,.T.,CHR(13))
			lSkipBack = .T.

			STORE "LX"+cfd+ALLTRIM(STR(nCtnNumber))+csegd TO cString   && Carton Seq. # (up to Carton total)
			DO cstringbreak
			nSegCtr = nSegCtr + 1

			IF TRIM(vmodernshoepp.ucc) <> cCartonNum
				STORE TRIM(vmodernshoepp.ucc) TO cCartonNum
			ENDIF

			DO WHILE vmodernshoepp.ucc = cCartonNum
				cUCCNumber = vmodernshoepp.ucc
				IF EMPTY(cUCCNumber) OR ISNULL(cUCCNumber)
					WAIT CLEAR
					WAIT WINDOW "Empty UCC Number in vmodernshoepp "+cShip_ref TIMEOUT 2
					lSQLMail = .T.
					DO ediupdate WITH "EMPTY UCC# in "+cShip_ref,.T.
					THROW
				ENDIF
				cUCCNumber = TRIM(STRTRAN(TRIM(cUCCNumber)," ",""))

				IF lDoManSegment
					lDoManSegment = .F.

					STORE "MAN"+cfd+"GM"+cfd+TRIM(cUCCNumber)+csegd TO cString  && SCC-14 Number (Wal-mart spec)
					DO cstringbreak
					nSegCtr = nSegCtr + 1
					nTotCtnCount = nTotCtnCount + 1

					IF EMPTY(outdet.ctnwt) OR outdet.ctnwt = 0
						cCtnWt = ALLTRIM(STR(CEILING(outship.weight/outship.ctnqty)))
						IF (EMPTY(cCtnWt) OR INT(VAL(cCtnWt)) = 0) AND outdet.totqty >0
							IF lTesting
								cCtnWt = '5'
							ELSE
								DO ediupdate WITH "WEIGHT ERR: PT "+cShip_ref,.T.
								THROW
							ENDIF
						ENDIF
						nTotCtnWt = nTotCtnWt + INT(VAL(cCtnWt))
					ELSE
						STORE ALLTRIM(STR(outdet.ctnwt)) TO cCtnWt
						nTotCtnWt = nTotCtnWt + outdet.ctnwt
					ENDIF

				ENDIF

				WAIT WINDOW "OD OUTDETID: "+TRANSFORM(outdet.outdetid)+CHR(13)+"V- OUTDETID: "+TRANSFORM(vmodernshoepp.outdetid) NOWAIT
				cColor = TRIM(outdet.COLOR)
				cSize = TRIM(outdet.ID)
				cUPC = TRIM(outdet.upc)
				IF (ISNULL(cUPC) OR EMPTY(cUPC))
					cUPC = TRIM(vmodernshoepp.upc)
				ENDIF
				cStyle = TRIM(outdet.STYLE)
				cItemNum = TRIM(outdet.custsku)
				cLineNum = TRIM(outdet.linenum)
*				nShipDetQty = outdet.totqty
				nODID = outdet.outdetid

				IF EMPTY(cLineNum)
					ASSERT .F. MESSAGE "In LINENUM error"
					SET STEP ON
					DO ediupdate WITH "EMPTY LINE #s-ODID "+TRANSFORM(nODID),.T.
					THROW
				ENDIF

				nShipDetQty = vmodernshoepp.totqty
				IF ISNULL(nShipDetQty)
					nShipDetQty = outdet.totqty
				ENDIF
				nUnitsum = nUnitsum + nShipDetQty

				nOrigDetQty = vmodernshoepp.qty
				IF ISNULL(nOrigDetQty)
					nOrigDetQty = nShipDetQty
				ENDIF

				IF lDoPALSegment
					STORE "PAL"+REPLICATE(cfd,11)+cCtnWt+cfd+cWeightUnit+csegd TO cString
					DO cstringbreak
					nSegCtr = nSegCtr + 1
					lDoPALSegment = .F.
				ENDIF

*!* Changed the following to utilize original 940 unit codes from Printstuff field
				cUnitCode = TRIM(segmentget(@apt,"UNITCODE",alength))
				IF EMPTY(cUnitCode)
					cUnitCode = "EA"
				ENDIF

				IF nOrigDetQty = nShipDetQty
					nShipStat = "CL"
				ELSE
					nShipStat = "PR"
				ENDIF

				STORE "W12"+cfd+nShipStat+cfd+ALLTRIM(STR(nOrigDetQty))+cfd+ALLTRIM(STR(nShipDetQty))+cfd+;
					ALLTRIM(STR(nOrigDetQty-nShipDetQty))+cfd+cUnitCode+cfd+cfd+"VN"+cfd+TRIM(cStyle)+cfd+cfd+cCtnWt+cfd+"FR"+cfd+"L"+csegd TO cString
				DO cstringbreak
				nSegCtr = nSegCtr + 1

				cShipStyle = ""
				cShipStyle = TRIM(segmentget(@apt,"SHIPSTYLE",alength))
				STORE "G69"+cfd+cShipStyle+csegd TO cString
				DO cstringbreak
				nSegCtr = nSegCtr + 1

				cLineNum = ALLTRIM(outdet.linenum)
				STORE "N9"+cfd+"LI"+cfd+cLineNum+csegd TO cString
				DO cstringbreak
				nSegCtr = nSegCtr + 1

				IF lParcelType
					SELECT package1
					LOCATE FOR package1.ucc = cUCCNumber
					cTrkNumber = ALLT(package1.trknumber)
					STORE "N9"+cfd+"P8"+cfd+cTrkNumber+csegd TO cString   && Individual tracking numbers
					DO cstringbreak
					nSegCtr = nSegCtr + 1
					IF !EOF('package1')
						SKIP 1 IN package1
					ENDIF
					SELECT vmodernshoepp
				ENDIF

				cUPC = TRIM(outdet.upc)
				STORE "N9"+cfd+"UP"+cfd+cUPC+csegd TO cString
				DO cstringbreak
				nSegCtr = nSegCtr + 1

*!*					The following was added by request of Byron Navarro at Modern Shoe, 05.20.2015 (uncomment when approved)
				IF lTesting
					cCtnVol = ALLTRIM(STR((outship.cuft/outship.ctnqty),10,2))
					STORE "MEA"+cfd+"AV"+cfd+"VOL"+cfd+cCtnVol+cfd+"CF"+csegd TO cString
					DO cstringbreak
					nSegCtr = nSegCtr + 1

					cCtnWt = ALLTRIM(STR((outship.weight/outship.ctnqty),10,2))
					STORE "MEA"+cfd+"AV"+cfd+"WT"+cfd+cCtnWt+cfd+"LB"+csegd TO cString
					DO cstringbreak
					nSegCtr = nSegCtr + 1

				ENDIF

				SKIP 1 IN vmodernshoepp
			ENDDO
			nCtnNumber = nCtnNumber + 1
			lSkipBack = .T.

*			ASSERT .F. MESSAGE "At end of detail looping-debug"
		ENDDO

*************************************************************************
*2^	END DETAIL MATCHING SHIP_REF LOOP
*************************************************************************
		WAIT CLEAR
		WAIT WINDOW "Now creating Section Closure information" NOWAIT NOCLEAR
		STORE "W03"+cfd+ALLTRIM(STR(nUnitsum))+cfd+ALLTRIM(STR(nTotCtnWt))+cfd+;
			cWeightUnit+cfd+ALLTRIM(STR(outship.cuft,10,2))+cfd+"CF"+cfd+ALLTRIM(STR(nTotCtnCount))+cfd+"CT"+csegd TO cString   && Units sum, Weight sum, carton count

		nTotCtnWt = 0
		nTotCtnCount = 0
		nUnitsum = 0
		FPUTS(nFilenum,cString)

		STORE  "SE"+cfd+ALLTRIM(STR(nSegCtr+2))+cfd+PADL(c_GrpCntrlNum,9,"0")+csegd TO cString
		FPUTS(nFilenum,cString)

		SELECT outship
		WAIT CLEAR
	ENDSCAN


*************************************************************************
*1^	END OUTSHIP MAIN LOOP
*************************************************************************

	DO close945
	=FCLOSE(nFilenum)
*	ASSERT .F. MESSAGE "At end of main loop...debug here"
	IF !lTesting
		SELECT edi_trigger
		DO ediupdate WITH "945 CREATED",.F.
		SELECT edi_trigger
		LOCATE

		IF !USED("ftpedilog")
			SELECT 0
			USE F:\edirouting\ftpedilog ALIAS ftpedilog
			INSERT INTO ftpedilog (TRANSFER,ftpdate,filename,acct_name,TYPE) VALUES ("945-"+ccustname+"-"+cCustLoc,dt2,cFilenameHold,UPPER(ccustname),"945")
			USE IN ftpedilog
		ENDIF
	ENDIF

	WAIT CLEAR
*	WAIT WINDOW cCustFolder+" 945 Process complete..." TIMEOUT 1

*!* Create eMail confirmation message
	cOutFolder = "FMI"+cCustLoc

	tsubject = cMailName+" 945 EDI File from TGF as of "+dtmail+" ("+cCustLoc+")"
	tattach = " "
	tmessage = "945 EDI Info from TGF, file "+cFilenameShort+CHR(13)
	tmessage = tmessage + "Division "+cDivision+", BOL# "+TRIM(cBOL)+CHR(13)
	tmessage = tmessage + "For Work Orders: "+cWO_NumStr+","+CHR(13)
	tmessage = tmessage + "containing these "+TRANSFORM(nPTCount)+" picktickets:"+CHR(13)
	tmessage = tmessage + cPTString + CHR(13)
	tmessage = tmessage +"has been created and will be transmitted ASAP."+CHR(13)+CHR(13)
	IF !EMPTY(cMissDel)
		tmessage = tmessage+CHR(13)+CHR(13)+cMissDel+CHR(13)+CHR(13)
	ENDIF
	tmessage = tmessage + "If you have any questions, please eMail or call 732-750-9000 x217."

	IF lEmail
		DO FORM m:\dev\frm\dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
	ENDIF
	nPTCount = 0

	WAIT CLEAR
	WAIT WINDOW cMailName+" 945 EDI File output complete" TIMEOUT 1

*!* Transfers files to correct output folders
	COPY FILE [&cFilenameHold] TO [&cFilenameArch]
	IF lMSFilesOut AND !lTesting
		COPY FILE [&cFilenameHold] TO [&cFilenameOut]
		DELETE FILE [&cFilenameHold]
		SELECT temp945
		COPY TO "f:\3pl\data\temp945ms.dbf"
		USE IN temp945
		SELECT 0
		USE "f:\3pl\data\temp945ms.dbf" ALIAS temp945ms
		SELECT 0
		USE "f:\3pl\data\pts_sent945.dbf" ALIAS pts_sent945
		APPEND FROM "f:\3pl\data\temp945ms.dbf"
		USE IN pts_sent945
		USE IN temp945ms
		DELETE FILE "f:\3pl\data\temp945ms.dbf"
	ENDIF
	lDoCatch = .F.

	*!* asn_out_data()()
	RELEASE ALL LIKE c_CntrlNum,c_GrpCntrlNum

CATCH TO oErr
	IF lDoCatch
		ASSERT .F. MESSAGE "In error CATCH..."
		SET STEP ON
		lEmail = .F.
		DO ediupdate WITH ALLTRIM(oErr.MESSAGE),.T.
		tsubject = ccustname+" Error ("+TRANSFORM(oErr.ERRORNO)+") at "+TTOC(DATETIME())
		tattach  = ""
		tsendto  = tsendtoerr
		tcc = tccerr

		tmessage = ccustname+" Error processing "+CHR(13)

		tmessage =tmessage+ "Try/CATCH Exeception Message:"+CHR(13)+;
			[  Error: ] + STR(oErr.ERRORNO) +CHR(13)+;
			[  LineNo: ] + STR(oErr.LINENO) +CHR(13)+;
			[  Message: ] + oErr.MESSAGE +CHR(13)+;
			[  Procedure: ] + oErr.PROCEDURE +CHR(13)+;
			[  Details: ] + oErr.DETAILS +CHR(13)+;
			[  StackLevel: ] + STR(oErr.STACKLEVEL) +CHR(13)+;
			[  LineContents: ] + oErr.LINECONTENTS+CHR(13)+;
			[  UserValue: ] + oErr.USERVALUE
		tmessage = tmessage + CHR(13)+"PROGRAM: "+cProgname
		IF !EMPTY(cStatus)
			tmessage = tmessage + CHR(13)+"Error: "+cStatus
		ENDIF
		tsubject = "945 EDI Poller Error at "+TTOC(DATETIME())
		tattach  = ""
		tfrom    ="TGF EDI 945 Poller Operations <transload-ops@tollgroup.com>"
		DO FORM m:\dev\frm\dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
	ENDIF
ENDTRY

*** END OF CODE BODY

****************************
PROCEDURE close945
****************************
** Footer Creation

	STORE  "GE"+cfd+ALLTRIM(STR(nSTCount))+cfd+c_CntrlNum+csegd TO cString
	FPUTS(nFilenum,cString)

	STORE  "IEA"+cfd+"1"+cfd+PADL(c_CntrlNum,9,"0")+csegd TO cString
	FPUTS(nFilenum,cString)

	RETURN

****************************
PROCEDURE num_incr_isa
****************************

	IF !USED("serfile")
		USE ("f:\3pl\data\serial\"+ccustname+"945_serial") IN 0 ALIAS serfile
	ENDIF
	SELECT serfile
	IF lTesting AND lISAFlag
		ASSERT .F. MESSAGE "At group seqnum...debug"
		nOrigSeq = serfile.seqnum
		lISAFlag = .F.
	ENDIF
	nISA_Num = serfile.seqnum
	c_CntrlNum = ALLTRIM(STR(serfile.seqnum))
	REPLACE serfile.seqnum WITH serfile.seqnum + 1 IN serfile
	SELECT outship
	RETURN

****************************
PROCEDURE num_incr_st
****************************

	IF !USED("serfile")
		USE ("f:\3pl\data\serial\"+ccustname+""945_serial") IN 0 ALIAS serfile
	ENDIF
	SELECT serfile
	IF lTesting AND lSTFlag
		nOrigGrpSeq = serfile.grpseqnum
		lSTFlag = .F.
	ENDIF
	c_GrpCntrlNum = ALLTRIM(STR(serfile.grpseqnum))
	REPLACE serfile.grpseqnum WITH serfile.grpseqnum + 1 IN serfile
	SELECT outship
	RETURN


****************************
PROCEDURE segmentget
****************************

	PARAMETER thisarray,lcKey,nLength

	FOR i = 1 TO nLength
		IF i > nLength
			EXIT
		ENDIF
		lnEnd= AT("*",thisarray[i])
		IF lnEnd > 0
			lcThisKey =TRIM(SUBSTR(thisarray[i],1,lnEnd-1))
			IF OCCURS(lcKey,lcThisKey)>0
				RETURN SUBSTR(thisarray[i],lnEnd+1)
				i = 1
			ENDIF
		ENDIF
	ENDFOR

	RETURN ""

****************************
PROCEDURE ediupdate
****************************
	PARAMETER cStatus,lIsError
*	lDoCatch = IIF(lIsError,.T.,.F.)
	lDoCatch = .F.

	IF !lTesting
		SELECT edi_trigger
		IF !lIsError
			IF cStatus = "JILDOR NO 945"
				REPLACE processed WITH .T.,proc945 WITH .F.,file945 WITH "",comments WITH comments+CHR(13)+"No 945 per Highline";
					fin_status WITH "NO 945 NEEDED",errorflag WITH .F.,when_proc WITH DATETIME() ;
					FOR edi_trigger.bol = cBOL AND INLIST(accountid,&gtkohighlineaccounts)
			ELSE
				REPLACE processed WITH .T.,proc945 WITH .T.,file945 WITH cFilenameHold,isa_num WITH cISA_Num,;
					fin_status WITH "945 CREATED",errorflag WITH .F.,when_proc WITH DATETIME() ;
					FOR edi_trigger.bol = cBOL AND INLIST(accountid,&gtkohighlineaccounts)
			ENDIF
		ELSE
			REPLACE processed WITH .T.,proc945 WITH .F.,file945 WITH "",;
				fin_status WITH cStatus,errorflag WITH .T. ;
				FOR edi_trigger.bol = cBOL AND INLIST(accountid,&gtkohighlineaccounts)
			IF lCloseOutput
				=FCLOSE(nFilenum)
				ERASE &cFilenameHold
			ENDIF
		ENDIF
	ENDIF


	IF lIsError AND lEmail && AND cStatus # "SQL ERROR"
		tsubject = "945 Error in MODERN SHOE BOL "+TRIM(cBOL)+"(At PT "+cShip_ref+")"
		tattach = " "
		tsendto = ALLTRIM(tsendtoerr)
		tcc = ALLTRIM(tccerr)
		tmessage = "945 Processing for BOL# "+cBOL+", WO# "+cWO_Num+" produced this error: (Office: "+cOffice+"): "+cStatus+CHR(13)+"Check EDI_TRIGGER and re-run"
		IF "TOTQTY ERR"$cStatus
			tmessage = tmessage + CHR(13) + "At OUTSHIPID: "+ALLTRIM(STR(m.outshipid))
		ENDIF
		DO FORM m:\dev\frm\dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
	ENDIF

	IF USED('outship')
		USE IN outship
	ENDIF
	IF USED('outdet')
		USE IN outdet
	ENDIF
	IF USED('shipment')
		USE IN shipment
	ENDIF
	IF USED('package')
		USE IN package
	ENDIF

	IF USED('serfile')
		SELECT serfile
		IF lTesting
			IF nOrigSeq > 0
				REPLACE serfile.seqnum WITH nOrigSeq
			ENDIF
			IF nOrigGrpSeq > 0
				REPLACE serfile.grpseqnum WITH nOrigGrpSeq
			ENDIF
		ENDIF
		USE IN serfile
	ENDIF
	IF USED('scacs')
		USE IN scacs
	ENDIF
	IF USED('mm')
		USE IN mm
	ENDIF
	IF USED('tempx')
		USE IN tempx
	ENDIF
	IF USED('vmodernshoepp')
		USE IN vmodernshoepp
	ENDIF

	IF !lTesting
		SELECT edi_trigger
		LOCATE
	ENDIF
	IF lIsError
	ENDIF
	RETURN

****************************
PROCEDURE cstringbreak
****************************
	cLen = LEN(ALLTRIM(cString))

	FPUTS(nFilenum,cString)
	RETURN