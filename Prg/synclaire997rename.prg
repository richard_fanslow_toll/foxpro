DO m:\dev\prg\_setvars WITH .T.
CD "F:\FTPUSERS\Synclaire\997Out\hold\"
len1 = ADIR(ary997,"*.edi")
IF len1 = 0
	WAIT WINDOW "No 997s to process..." TIMEOUT 1
	CLOSE DATABASES ALL
	RETURN
ENDIF

lTesting = .f. && Change to .F. for production 997 creation.

_SCREEN.CAPTION = "Synclaire 997 Rename Process"
_SCREEN.WINDOWSTATE = IIF(lTesting,2,1)
cFileExt = ".edi"
cString = ""

FOR uy = 1 TO len1
	cFilenameIn = ALLTRIM(ary997[uy])
	stringin = FILETOSTR(cFilenameIn)
	stringin = STRTRAN(stringin,"~","~"+CHR(13))
	STRTOFILE(stringin,"WFile.997")
	DELETE FILE [&cFilenameIn]
	CREATE CURSOR temp1 (f1 c(254))
	SELECT temp1
	APPEND FROM WFile.997 TYPE SDF
	DELETE FOR EMPTY(ALLTRIM(temp1.f1))
	DELETE FILE WFile.997
	LOCATE
	ASSERT .F.
	nRecs = RECCOUNT()
	LOCATE
	IF lTesting
		SET STEP ON
	ENDIF

	lDoISA = .T.
	SCAN
		cF1 = ALLTRIM(f1)
		IF cF1 = "ISA"
			IF lDoISA
				lDoISA = .F.
				cTimeString = TTOC(DATETIME(),1)
				cOutfilename = "997sync"+cTimeString+cFileExt
				cArchiveFile = LOWER("F:\FTPUSERS\Synclaire\997Out\archive\"+cOutfilename)
				cOutfile = LOWER("F:\FTPUSERS\Synclaire\997Out\"+cOutfilename)
				cTestOutfile = LOWER("F:\FTPUSERS\Synclaire\997OUT\testout\"+cOutfilename)
				cProdOutfile = LOWER("F:\FTPUSERS\Synclaire\945OUT\"+cOutfilename)
				nFnum = FCREATE(cOutfile)
			ENDIF
		ENDIF

		FPUTS(nFnum,cF1)

	ENDSCAN
	SET STEP ON
	FCLOSE(nFnum)
	WAIT WINDOW "Process complete for "+cOutfilename TIME 1
	COPY FILE [&cOutfile] TO [&cArchiveFile]
	IF lTesting
		COPY FILE [&cOutfile] TO [&cTestOutfile]
	ELSE
		COPY FILE [&cOutfile] TO [&cProdOutfile]
		DELETE FILE [&cOutfile]
	ENDIF
ENDFOR

CLOSE ALL
CLEAR ALL
RELEASE ALL

RETURN
