**This script identifies when a 945 has not been created for a PT.  Therefore MJ has not invoiced that PT
* runs on the first of the month

* mb 1/2/2018: 2nd half of process which would send email to Maria Estrella was crashing; per Chris Malcolm I commented that part out as we believe it is no longer needed.
*
* build EXE in F:\3PL\

utilsetup("MJ_VAS")
set exclusive off
set deleted on
set talk off
set safety off

with _screen
	.autocenter = .t.
	.windowstate = 0
	.borderstyle = 1
	.width = 320
	.height = 210
	.top = 200
	.left = 100
	.closable = .t.
	.maxbutton = .t.
	.minbutton = .t.
	.scrollbars = 0
	.caption = "MJ_VAS"
endwith

goffice='J'
useca("inwolog","wh",,,"mod='"+goffice+"' and  accountid in (6303,6325,6543) and confirmdt>{"+dtoc(date()-90)+"}")
useca("indet","wh",,,"mod='"+goffice+"' and  accountid in (6303,6325,6543) and date_rcvd>{"+dtoc(date()-90)+"}")

xmonth=month(date())-1
xcmonth=cmonth(date())
xyear=year(date())
if xmonth=0
	xmonth=12
	xyear=year(date())-1
else
endif

xenddt=date()-1
xstartdt=xenddt-day(xenddt)+1

*set step on

select a.accountid,acct_ref, dtoc(confirmdt) as confirmdt,seal as vendor, style,color,id,totqty, space(5) as div from inwolog a , indet b where a.inwologid=b.inwologid  and month(confirmdt)=xmonth and year(confirmdt)=xyear and !zreturns and reference !='CANCELE' and a.accountid!=6453 into cursor t1 readwrite
upcmastsql(6303)
select a.*,info from t1 a left join upcmast b on a.style=b.style and a.color=b.color and a.id=b.id and b.accountid=6303 into cursor c1 readwrite

replace all div with getmemodata("info","DIV")
select * from c1 where inlist(div,'CW ','CEY','MEY','MWT','CM ','CMS','LM','FR','LS') into cursor c2 readwrite
select accountid,acct_ref as journal, confirmdt, vendor, div, sum(totqty) as qty from c2 group by 1,2,3,4,5 into cursor c3 readwrite


if reccount() > 0
	* Changed this to xl5 due to problems Chris is having opening the xls files. 2/2/2018 MB
	*export to "S:\MarcJacobsData\temp\mj_vas_ib1"  type xls
	export to "S:\MarcJacobsData\temp\mj_vas_ib1"  type xl5

	* MB: 1/2/2018 per CM
	*tsendto = "tmarg@fmiint.com,jim.killen@tollgroup.com"
	tsendto = "Chris.Malcolm@tollgroup.com, Marty.Cuniff@tollgroup.com, Bill.Marsh@tollgroup.com"
	tattach = "S:\MarcJacobsData\temp\mj_vas_ib1.xls"
	tcc ="mbennett@fmiint.com"
	tfrom ="Toll/FMI EDI System Operations <fmi-transload-ops@fmiint.com>"
	tmessage = "MJ VAS IB for   "+cmonth(date()-10)
	tsubject = "MJ VAS IB for   "+cmonth(date()-10)
	do form m:\dev\frm\dartmail2 with tsendto,tfrom,tsubject, tcc,tattach,tmessage  ,"A"
else
	* MB: 1/2/2018 per CM
	*tsendto = "tmarg@fmiint.com,jim.killen@tollgroup.com"
	tsendto = "Chris.Malcolm@tollgroup.com, Marty.Cuniff@tollgroup.com, Bill.Marsh@tollgroup.com"
	tattach = ""
	tcc ="mbennett@fmiint.com"
	tfrom ="Toll/FMI EDI System Operations <fmi-transload-ops@fmiint.com>"
	tmessage = "NO MJ VAS IB Exist for   "+cmonth(date()-10)
	tsubject = "NO MJ VAS IB Exist for   "+cmonth(date()-10)
	do form m:\dev\frm\dartmail2 with tsendto,tfrom,tsubject, tcc,tattach,tmessage  ,"A"
endif


*!*	set step on

*!*	xsqlexec("select del_date, outship.wo_num as wo_num, consignee, qty, totqty, upc, outship.outshipid from outship, outdet " + ;
*!*		"where outship.outshipid=outdet.outshipid and outship.mod='J' and outship.accountid=6303 and between(del_date,{"+dtoc(xstartdt)+"},{"+dtoc(xenddt)+"}) " + ;
*!*		"and ((consignee='NORD' and csz='CEDAR RAPIDS')	or (consignee='NORD' and dcnum='0569') or consignee='ZAPPOS')","c1",,"wh")

*!*	xsqlexec("select del_date, outship.wo_num as wo_num, consignee, qty from outship where outship.mod='J' and outship.accountid=6303 and between(del_date,{"+dtoc(xstartdt)+"},{"+dtoc(xenddt)+"}) " + ;
*!*		"and (inlist(consignee,'BLOOMINGDALE','NEIMAN MARCUS DIRECT','NET-A-PORTER','ZAPPOS.COM') or (consignee='NEIMAN MARCUS' and sforstore='079') " + ;
*!*		" or (consignee like '%NORDS%' and dcnum='0599'))","nj2",,"wh")

*!*	select a.*,space(3) as div, info from c1 a left join upcmast b on a.upc=b.upc and accountid=6303 into cursor c2 readwrite
*!*	replace all div with getmemodata("info","DIV") in c2
*!*	select * from c2 where inlist(div,'MWS','MWF') into cursor c3 readwrite
*!*	select del_date,wo_num,consignee,sum(totqty) as qty from c3 group by 1,2,3 into cursor nj1 readwrite

*!*	xsqlexec("select del_date, outship.wo_num as wo_num, consignee, qty, totqty, upc, outship.outshipid from outship, outdet " + ;
*!*		"where outship.outshipid=outdet.outshipid and outship.mod='L' and outship.accountid=6303 and between(del_date,{"+dtoc(xstartdt)+"},{"+dtoc(xenddt)+"}) " + ;
*!*		"and ((consignee='NORD' and csz='CEDAR RAPIDS')	or (consignee='NORD' and dcnum='0569') or consignee='ZAPPOS')","c1",,"wh")

*!*	xsqlexec("select del_date, outship.wo_num as wo_num, consignee, qty from outship where outship.mod='L' and outship.accountid=6303 and between(del_date,{"+dtoc(xstartdt)+"},{"+dtoc(xenddt)+"}) " + ;
*!*		"and (inlist(consignee,'BLOOMINGDALE','NEIMAN MARCUS DIRECT','NET-A-PORTER','ZAPPOS.COM') or (consignee='NEIMAN MARCUS' and sforstore='079') " + ;
*!*		" or (consignee like '%NORDS%' and dcnum='0599'))","nj2",,"wh")

*!*	select * from nj1 union select * from nj2 into cursor nj3 readwrite
*!*	select * from ml1 union select * from ml2 into cursor ml3 readwrite
*!*	select * from ml3 union select * from nj3 into cursor c1 readwrite
*!*	set step on

*!*	if reccount() > 0
*!*		export to "S:\MarcJacobsData\temp\mj_vas"  type xls

*!*		* MB: 1/2/2018 per CM
*!*		*tsendto = "todd.margolin@tollgroup.com,Maria.Estrella@Tollgroup.com"
*!*		tsendto = "Maria.Estrella@Tollgroup.com"
*!*		tattach = "S:\MarcJacobsData\temp\mj_vas.xls"
*!*		tcc =""
*!*		tfrom ="Toll/FMI EDI System Operations <fmi-transload-ops@fmiint.com>"
*!*		tmessage = "MJ VAS for   "+cmonth(date()-10)
*!*		tsubject = "MJ VAS for   "+cmonth(date()-10)
*!*		do form m:\dev\frm\dartmail2 with tsendto,tfrom,tsubject, tcc,tattach,tmessage  ,"A"
*!*	else
*!*		* MB: 1/2/2018 per CM
*!*		*tsendto = "tmarg@fmiint.com,Maria.Estrella@Tollgroup.com"
*!*		tsendto = "Maria.Estrella@Tollgroup.com"
*!*		tattach = ""
*!*		tcc =""
*!*		tfrom ="Toll/FMI EDI System Operations <fmi-transload-ops@fmiint.com>"
*!*		tmessage = "NO MJ VAS Exist for   "+cmonth(date()-10)
*!*		tsubject = "NO MJ VAS Exist for   "+cmonth(date()-10)
*!*		do form m:\dev\frm\dartmail2 with tsendto,tfrom,tsubject, tcc,tattach,tmessage  ,"A"
*!*	endif

close data all
schedupdate()
_screen.caption=gscreencaption
