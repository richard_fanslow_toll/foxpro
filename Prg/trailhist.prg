lparameters xtrailer, xupdate, xmonths, xignorewonum, xfrom, xforce, xremovefromdrop


if empty(xtrailer)
	return .f.
endif

if empty(xfrom)
	xfrom="?"
endif

xtrailer=left(padr(xtrailer,8),8)

if !seek(xtrailer,"trailer","trail_num")	&& not one of our trailers
	return .f.
endif

if xupdate
	if !empty(trailer.dispmsg)
		if x3winmsg("Trailer "+trim(trailer.trail_num)+" - "+trailer.dispmsg+crlf+crlf+"entered by "+username(trailer.dispmsgenterby)+" on "+dtoc(trailer.dispmsgdt) + ;
			crlf+crlf+crlf+crlf+"Would you like to remove this message?"+crlf,,"Y|N")="YES"
				replace dispmsg with "", dispmsgenterby with "", dispmsgdt with {} in trailer
		endif
	endif
endif

*if !xforce and !empty(trailer.qcserialno)	&& we know where the qualcomm-equipped trailers are - removed dy 4/10/14
*	return .f.
*endif

if empty(xmonths)
	xmonths=6
endif

select ;
	trailer, action, iif(action="RT",.t.,.f.) as bobtail, startloc, droploc, endloc, puloc, retloc, delloc, ;
	wo_num, accountid, acctname, 2 as leg, dispatched as sortdisp, {} as sortdel, ;
	dispatched, iif(!empty(time_in),dispatched,{}) as delivered, time_in, yard_hold ;
	from daily ;
	where trailer=xtrailer ;
	and action#"  " ;
	and dispatched>gomonth(date(),-xmonths) ;
union select ;
	trailer, action, iif(action="RT",.t.,.f.) as bobtail, startloc, droploc, endloc, puloc, retloc, delloc, ;
	wo_num, accountid, acctname, 2 as leg, dispatched as sortdisp, {} as sortdel, ;
	dispatched, dispatched as delivered, time_in, yard_hold ;
	from tdaily ;
	where trailer=xtrailer ;
	and action#"  " ;
	and dispatched>gomonth(date(),-xmonths) ;
	into cursor xtrailhist readwrite

select xtrailhist
scan 
	xrecno=recno()
	
	if between(wo_num,50000,80000)
		replace action with "OB"
	endif

	do case
	case !emptynul(delivered)
		replace sortdel with delivered
	case action="SH"
		replace sortdel with dispatched
	case !emptynul(yard_hold)
		replace sortdel with yard_hold
	otherwise
		replace sortdel with {12/31/2099}
	endcase
	
	if action="SH"
		replace startloc with "SPRINGFIELD GARDENS, NY", endloc with "CARTERET, NJ"
	endif
	if action="RT"
		replace endloc with delloc
	endif
	
	if empty(endloc)
		if action="OB"
			if seek(transform(wo_num),"outbound","del_mfst")
				if seek(outbound.outboundid,"delivery","outboundid")
					replace startloc with "CARTERET, NJ", endloc with delivery.deliverto
				endif
			endif
		else
			if !empty(retloc)
				replace endloc with retloc
			else
				if !empty(puloc)
					replace endloc with puloc
				else
					=seek(wo_num,"wolog","wo_num")
					if !empty(wolog.retloc)
						replace endloc with wolog.retloc
					else
						if !empty(wolog.toloc)
* this is using the wrong pudlocs it s/b wopudlocs  --- dy 11/7/15 revisit some day
*							if gfxsql
*								xquery="select city, state from pudlocs where accountid="+transform(wolog.accountid)+" and dash="+transform(wolog.toloc)
*								xsqlexec(xquery,"xsqlpudlocs")
*								replace endloc with dispcsz(xsqlpudlocs.city, xsqlpudlocs.state)
*							else
*								=seek(str(wolog.accountid,4)+str(wolog.toloc,4),"pudlocs","punum")
*								replace endloc with dispcsz(pudlocs.city, pudlocs.state)
*							endif
						else
							=seek(wolog.accountid,"account","accountid")
							replace endloc with dispcsz(account.city, account.state)
						endif
					endif
				endif
			endif
		endif
	endif

	if !empty(droploc)
		xendloc=endloc
		replace endloc with droploc
		scatter memvar
		m.startloc=m.endloc
		m.droploc=""
		m.endloc=xendloc
		m.leg=3
		insert into xtrailhist from memvar
	endif
	
	go (xrecno)
endscan

*

xquery="select * from fxtrips where triptrailer='"+xtrailer+"' and wo_date>'"+dtoc(gomonth(date(),-xmonths))+"' and dvrcompany<>'B'"
xsqlexec(xquery,"xtrailfxtrips",,"fx")

scan 
	xfxtripsid=fxtripsid

	m.action="EX"
	m.retloc=""
	m.trailer=xtrailer
	m.bobtail=deadhead
	m.startloc=puloc
	m.endloc=delloc
	m.wo_num=wo_num
	m.acctname=acctname(accountid)
	m.dispatched=dispatched
	m.delivered=delivered
	m.time_in=delvtime
	
	if !empty(m.dispatched)
		m.leg=2
		m.sortdisp=dispatched
		m.sortdel=iif(!empty(m.delivered),m.delivered,iif(m.dispatched<date()-10,m.dispatched+1,{12/31/2099}))
	else
		if !empty(m.delivered)
			m.dispatched=m.delivered-1
		else
			if leg=1
				m.leg=1
				locate for wo_num=m.wo_num and leg=2
				if deadhead
					locate for wo_num=m.wo_num and !deadhead
				endif
				m.sortdisp=dispatched
				m.sortdel=iif(!emptynul(delivered),delivered,{12/31/2099})
				m.dispatched=dispatched
				m.delivered=delivered
				m.time_in=delvtime
			else
				xleg=leg
				m.leg=3
				locate for wo_num=m.wo_num and leg=xleg-1
				if deadhead
					locate for wo_num=m.wo_num and !deadhead
				endif
				m.sortdisp=dispatched
				m.sortdel=iif(!emptynul(delivered),delivered,{12/31/2099})
				m.dispatched=dispatched
				m.delivered=delivered
				m.time_in=delvtime
			endif
		endif
	endif				

	try
		insert into xtrailhist from memvar
	catch
	endtry

	select xtrailfxtrips
	locate for fxtripsid=xfxtripsid
endscan

*

select xtrailhist
index on dtos(sortdel)+dtos(sortdisp)+chr2time(time_in)+transform(leg) tag zorder descending

if !empty(xignorewonum)
	delete for wo_num=xignorewonum
endif

locate for !emptynul(dispatched)
scatter memvar
m.location=endloc
m.droptime=time_in

select wo_num, action, accountid, acctname, endloc+space(10) as location, dispatched, delivered, time_in as droptime ;
	from xtrailhist ;
	where .f. ;
	into cursor xtrailcurr readwrite

insert into xtrailcurr from memvar

*

if xupdate
	if empty(xtrailcurr.location)
*		email("Dyoung@fmiint.com","INFO: xtrailcurr.location blank in trailhist", ;
		xtrailer+"*"+crlf+xfrom+crlf+transform(xmonths)+crlf+transform(reccount("xtrailhist"))+crlf+transform(reccount("xtrailcurr")),,,,.t.,,,,,.t.,,.t.)
	else
		if seek(xtrailer,"trailer","trail_num")
			replace wo_num with xtrailcurr.wo_num, ;
					accountid with xtrailcurr.accountid, ;
					acctname with xtrailcurr.acctname, ;
					location with xtrailcurr.location, ;
					dispatched with iif(xtrailcurr.delivered={},xtrailcurr.dispatched,{}), ;
					dropped with xtrailcurr.delivered, ;
					droptime with xtrailcurr.droptime, ;
					action with iif(xtrailcurr.delivered={},xtrailcurr.action,"") in trailer
		endif
	endif
endif

if xremovefromdrop
	if seek(xtrailer,"drop","trailer")
		delete in drop
	endif
endif