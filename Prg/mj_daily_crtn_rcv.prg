* This program
utilsetup("MJ_DAILY_CRTN_RCV")
set exclusive off
set deleted on
set talk off
set safety off   &&Allows for overwrite of existing files

close databases all

useca('mjraret','wh',.t.)
*!*	If !Used("mj_ra_return")
*!*	  use F:\wh\mj_ra_return.dbf IN 0
*!*	ENDIF

* Changed from all to last 180 days - Chad 3/23/2018
xsqlexec("select * from cartret where CreateDt >= getdate() - 180","mj_carton_returns",,"wh")

* returns_deleted_cartons.xls

if !used("mj_deleted_cartons")
	use f:\whj\whdata\mj_deleted_cartons.dbf in 0
endif

set deleted off
select distinct carton_id from mj_carton_returns where createdt >= date()-10 into cursor t2 readwrite
set deleted on

select distinct carton_id from mj_carton_returns where createdt >=date()-10 into cursor t1 readwrite
select * from t1 full join t2 on t1.carton_id = t2.carton_id into cursor c1 readwrite

select carton_id_b as carton_id from c1 where isnull(carton_id_a) into cursor c2 readwrite

select * from c2 left join mj_deleted_cartons b on c2.carton_id = b.carton_id into cursor c3 readwrite
select carton_id_a as carton_id from c3 where isnull(carton_id_b) into cursor c4 readwrite
select carton_id_a as carton_id from c3 into cursor c5 readwrite
use in mj_deleted_cartons
select c5
copy to f:\whj\whdata\mj_deleted_cartons.dbf
select c4
set step on
if reccount() > 0
	export to "S:\MarcJacobsData\TEMP\returns_deleted_cartons"  type xls
	tsendto = "v.gil@marcjacobs.com,Laudy.Minyety@tollgroup.com,bill.marsh@tollgroup.com,M.deLaCruz@marcjacobs.com"
	tattach = "S:\MarcJacobsData\TEMP\returns_deleted_cartons.xls"
	tcc =""
	tfrom ="Toll EDI System Operations <fmi-transload-ops@fmiint.com>"
	tmessage = "MJ RETURNS DELETED CARTONS:  "+ttoc(date()-1)
	tsubject = "MJ RETURNS DELETED CARTONS"
	do form m:\dev\frm\dartmail2 with tsendto,tfrom,tsubject, tcc,tattach,tmessage  ,"A"

else
*		export TO "S:\MarcJacobsData\Reports\InventorySyncFiles\OB_OSnD\NO_pre_invoice_osnd_exist_" + TTOC(DATE()-1, 0)  TYPE xls
	tsendto = "v.gil@marcjacobs.com,Laudy.Minyety@tollgroup.com,bill.marsh@tollgroup.com,M.deLaCruz@marcjacobs.com"
	tattach = ""
	tcc = ""
	tfrom ="Toll EDI System Operations <fmi-transload-ops@fmiint.com>"
	tmessage = "NO MJ RETURNS DELETED CARTONS:  "+ttoc(date()-1)
	tsubject = "NO MJ RETURNS DELETED CARTONS"
	do form m:\dev\frm\dartmail2 with tsendto,tfrom,tsubject, tcc,tattach,tmessage  ,"A"

endif




* Daily_Carton_rcv

*Wait window at 10,10 "Performing the first query...." nowait
select distinct ra, consignee from mjraret into cursor tempRA readwrite
*select * from mj_carton_returns where (createdt) >= (date()-1) and (createdt) != (date()) into cursor temp11 readwrite
select * from mj_carton_returns where createdt >= date()-1 and createdt != date() into cursor temp11 readwrite	&& Get yesterday
select * from temp11 a left join tempRA b on a.ra = b.ra and !empty(b.consignee) into cursor temp12 readwrite	&& Get Consignee
select dtoc(createDt) as date_rcvd, ra_a as rma, consignee, space(10) as consignee_address, unit_qty, carton_qty, carton_id from temp12 into cursor temp13 readwrite
*select * from temp13 where (substr(rma,1,1) != '2' and substr(rma,7,1)!=' ') and  substr(rma,1,3) != 'GSI' into cursor temp14 readwrite
	* Vanessa Gill @ MJ wants Retail as well. Changing Temp14 to include and distinguish. Chad 4/5/2018
select *, iif(substr(rma,1,1) = '2','Retail','Wholesale') as Outlet from temp13 where substr(rma,1,3) != 'GSI' into cursor temp14 readwrite
replace rma with substr(rma,4,15) for inlist(rma,'10-6','10-7') in temp14

select temp14
if reccount() > 0
	export to "S:\MarcJacobsData\TEMP\daily_carton_rcv"  type xls
	tsendto = "v.gil@marcjacobs.com,Laudy.Minyety@tollgroup.com,bill.marsh@tollgroup.com,M.deLaCruz@marcjacobs.com"
	tattach = "S:\MarcJacobsData\TEMP\daily_carton_rcv.xls"
	tcc =""
	tfrom ="Toll EDI System Operations <fmi-transload-ops@fmiint.com>"
	tmessage = "MJ DAILY CARTON RECEIVING:  "+ttoc(date()-1)
	tsubject = "MJ DAILY CARTON RECEIVING"
	do form m:\dev\frm\dartmail2 with tsendto,tfrom,tsubject, tcc,tattach,tmessage  ,"A"

else
	tsendto = "v.gil@marcjacobs.com,Laudy.Minyety@tollgroup.com,bill.marsh@tollgroup.com,M.deLaCruz@marcjacobs.com"
	tattach = ""
	tcc =""
	tfrom ="Toll EDI System Operations <fmi-transload-ops@fmiint.com>"
	tmessage = "NO MJ DAILY CARTON RECEIVING:  "+ttoc(date()-1)
	tsubject = "NO MJ DAILY CARTON RECEIVING"
	do form m:\dev\frm\dartmail2 with tsendto,tfrom,tsubject, tcc,tattach,tmessage  ,"A"

endif

goffice='J'
useca("inwolog","wh",,,"mod='"+goffice+"' and  accountid in (6303, 6325,6543)")

***This shows return carton status upto 5 days after carton is processed
select distinct ra,consignee from mjraret into cursor tempra readwrite
select * from mj_carton_returns where createdt > {11/16/2014} into cursor temp11 readwrite
select a.*,wo_num,axdt from temp11 a left join inwolog b on a.carton_id=b.unbilledcomment into cursor temp11a readwrite

use in inwolog
goffice='L'

useca("inwolog","wh",,,"mod='"+goffice+"' and  accountid in (6303,6325,6543) and archived=0")

select a.*,b.wo_num,b.axdt as laxdt from temp11a a left join inwolog b on a.carton_id=b.unbilledcomment into cursor temp11b readwrite
replace wo_num_a with wo_num_b for isnull(wo_num_a) and !isnull(wo_num_b) in temp11b
replace axdt with laxdt for isnull(axdt) and !isnull(laxdt) in temp11b
delete from temp11b where axdt<date()-6 and !emptynul(axdt)

select * from temp11b a left join  tempra b on a.ra=b.ra and !empty(consignee) into cursor temp12 readwrite
select dtoc(createdt) as date_rcvd,ra_a as rma,claim,consignee,space(10) as consignee_address,unit_qty, carton_qty,carton_id, space(20) as status,refurb_loc,cartonloc, wo_num_a as wo_num from temp12 into cursor temp13 readwrite
select * from temp13 where (substr(rma,1,1)!='2' and substr(rma,7,1)!=' ') and substr(rma,1,3)!='JAM' and substr(rma,1,3)!='GSI'  into cursor temp14

xsqlexec("select carton_id, inwonum from cmtrans","xdy",,"wh")
select carton_id ,inwonum from xdy group by 1,2 into cursor cmtrans2 readwrite

select * from temp14 a left join cmtrans2 b on a.carton_id=b.carton_id into cursor temp15 readwrite

replace status with 'SCANNED' for !isnull(carton_id_b)
replace status with 'CARTON RCVD' for isnull(carton_id_b)
replace status with 'IN REFURB' for isnull(carton_id_b) and !empty(refurb_loc)
replace status with 'TO BE SCANNED TO PL' for isnull(carton_id_b) and !empty(cartonloc)
replace status with 'WO CREATED' for !isnull(inwonum) and (inwonum)!=0
select a.*,confirmdt,axdt,brokerref from temp15 a left join inwolog b on a.inwonum=b.wo_num into cursor temp16 readwrite
replace status with 'WO CONFIRMED' for !isnull(confirmdt)
replace status with 'AX INPUT CREATED' for !isnull(axdt)
replace status with 'NO AX INPUT' for brokerref='NO A'
delete for axdt <date()-6
select date_rcvd, rma,claim, consignee, consignee_address, unit_qty,carton_qty,carton_id_a as carton_id,status,wo_num from temp16 into cursor temp17 readwrite
replace rma with substr(rma,4,15)  for inlist(rma,'10-6','10-7')  in temp17

select temp17
if reccount() > 0
	export to "S:\MarcJacobsData\Reports\mjdashboard\returns_carton_status"  type xls
	tsendto = "tmarg@fmiint.com,v.gil@marcjacobs.com,M.deLaCruz@marcjacobs.com,RETURN.DEPT@marcjacobs.com,Laudy.Minyety@tollgroup.com,bill.marsh@tollgroup.com"
	tattach = "S:\MarcJacobsData\Reports\mjdashboard\returns_carton_status.xls"
	tcc =""
	tfrom ="Toll EDI System Operations <fmi-transload-ops@fmiint.com>"
	tmessage = "MJ RETURNS CARTON STATUS:  "+ttoc(date()-1) +"        mj_daily_crtn_rcv"
	tsubject = "MJ RETURNS CARTON STATUS"
	do form m:\dev\frm\dartmail2 with tsendto,tfrom,tsubject, tcc,tattach,tmessage  ,"A"

else
*!*	*		export TO "S:\MarcJacobsData\Reports\InventorySyncFiles\OB_OSnD\NO_pre_invoice_osnd_exist_" + TTOC(DATE()-1, 0)  TYPE xls
*!*			tsendto = "tmarg@fmiint.com,Tolltemp8@marcjacobs.com,O.BATISTA@marcjacobs.com"
*!*			tattach = ""
*!*			tcc =""
*!*			tFrom ="Toll EDI System Operations <fmi-transload-ops@fmiint.com>"
*!*			tmessage = "NO MJ DAILY CARTON RECEIVING:  "+Ttoc(Date()-1)
*!*			tSubject = "NO MJ DAILY CARTON RECEIVING"
*!*			Do Form m:\dev\frm\dartmail2 With tsendto,tFrom,tSubject, tcc,tattach,tmessage  ,"A"

endif

***********************************START ALL RETURNS

goffice='J'

useca("inwolog","wh",,,"mod='"+goffice+"' and  accountid in (6303, 6325,6543) and archived=0")

***This shows return carton status upto 5 days after carton is processed
select distinct ra from mjraret into cursor tempra readwrite
select * from mj_carton_returns where createdt > {10/16/2015} into cursor temp11 readwrite
select * from temp11 a left join  tempra b on a.ra=b.ra  into cursor temp12 readwrite
select TTOD(createdt) as date_rcvd,ra_a as rma,unit_qty, carton_qty,carton_id, space(20) as status,refurb_loc,cartonloc from temp12 into cursor temp13 readwrite
**SELECT * FROM temp13 WHERE SUBSTR(rMa,1,1)!='10-'   AND SUBSTR(rMa,1,3)!='JAM' INTO CURSOR temp14

xsqlexec("select carton_id, inwonum from cmtrans","xdy",,"wh")
select carton_id ,inwonum from xdy group by 1,2 into cursor cmtrans2 readwrite

select * from temp13 a left join cmtrans2 b on a.carton_id=b.carton_id into cursor temp15 readwrite

replace status with 'SCANNED' for !isnull(carton_id_b)
replace status with 'CARTON RCVD' for isnull(carton_id_b)
replace status with 'IN REFURB' for isnull(carton_id_b) and !empty(refurb_loc)
replace status with 'TO BE SCANNED TO PL' for isnull(carton_id_b) and !empty(cartonloc)
replace status with 'WO CREATED' for !isnull(inwonum) and (inwonum)!=0
select a.*,confirmdt,axdt,brokerref from temp15 a left join inwolog b on a.inwonum=b.wo_num   into cursor temp16nj readwrite
select *, ltrim(str(inwonum)) as wonum from temp16nj into cursor temp16nj readwrite
delete from temp16nj where substr(wonum,1,1)='6'
if used("INWOLOG")
	use in inwolog
endif

goffice='L'
useca("inwolog","wh",,,"mod='"+goffice+"' and  accountid in (6303, 6325,6543)")

select a.*,confirmdt,axdt,brokerref from temp15 a left join inwolog b on a.inwonum=b.wo_num   into cursor temp16ml readwrite
select *, ltrim(str(inwonum)) as wonum from temp16ml into cursor temp16ml readwrite
delete from temp16ml where substr(wonum,1,1)='3'
select * from temp16nj union select * from temp16ml into cursor temp16 readwrite
replace status with 'WO CONFIRMED' for !isnull(confirmdt)
replace status with 'SENT TO AX FILE' for axdt>{01/01/2015}
replace status with 'NO AX INPUT' for brokerref='NO A'
delete for axdt <date()-6
select date_rcvd, rma,  unit_qty,carton_qty,carton_id_a as carton_id,status from temp16 ORDER BY 1 into cursor temp17w
**SELECT * FROM TEMP17 UNION ALL SELECT * FROM TEMP17W INTO CURSOR TEMP17C READWRITE
set step on
select temp17w
if reccount() > 0
	export to "S:\MarcJacobsData\Reports\mjdashboard\returns_carton_status_all"  type xls
*!*			tsendto =""&&arg@fmiint.com,l.lopez@marcjacobs.com,E.SKRZYNIARZ@marcjacobs.com,RETURN.DEPT@marcjacobs.com,J.JIMENEZ@marcjacobs.com,Laudy.Minyety@tollgroup.com,bill.marsh@tollgroup.com,M.deLaCruz@marcjacobs.com"
*!*			tattach = "S:\MarcJacobsData\TEMP\returns_carton_status_retail.xls"
*!*			tcc =""
*!*			tFrom ="Toll EDI System Operations <fmi-transload-ops@fmiint.com>"
*!*			tmessage = "MJ RETURNS CARTON STATUS RETAIL:  "+Ttoc(Date()-1)
*!*			tSubject = "MJ RETURNS CARTON STATUS RETAIL"
*!*			Do Form m:\dev\frm\dartmail2 With tsendto,tFrom,tSubject, tcc,tattach,tmessage  ,"A"

else
endif


********************************


delete file s:\marcjacobsdata\temp\mj_return_details.xls
delete file s:\marcjacobsdata\temp\mj_return_details.xlsx
useca('mjraret','wh',.t.)
*!*	If !Used("mj_ra_return")
*!*	use F:\wh\mj_ra_return.dbf IN 0
*!*	ENDIF
if !used("mj_carton_returns")
	use f:\wh\mj_carton_returns in 0
endif

xsqlexec("select invloc, accountid, upc, style, color, id, qty, carton_id from cmtrans " + ;
	"where scantime>={1/1/2017} and wo_created=1 and inlist(accountid,6303,6325) and invloc='10-'","xdy",,"wh")

select invloc as ra, accountid, upc, style, color, id, sum(qty) as qty, carton_id from xdy group by 1,2,3,4,5,6,8 into cursor t1

select date_rcvd,a.*,claim,track_num, addby as whse from t1 a left join mj_carton_returns b on a.carton_id=b.carton_id into cursor c1 readwrite
select a.*,consignee from c1 a left join mjraret b on a.ra=b.ra into cursor c3 readwrite
select date_rcvd,ra,claim,consignee, accountid as account, upc,style,color,id,qty,carton_id,track_num, whse from c3 into cursor c4 readwrite
replace whse with 'NJ' for empty(whse) in c4
replace ra with substr(ra,4,15)  for inlist(ra,'10-6','10-7')  in c4
copy to s:\marcjacobsdata\temp\mj_return_details.xls xls
oexcel=createobject("excel.application")
oexcel.visible=.f.
oworkbook=oexcel.workbooks.open("S:\MarcJacobsData\TEMP\mj_return_details.xls")
oworkbook.saveas("S:\MarcJacobsData\TEMP\mj_return_details.xlsx",51)
oexcel.quit()
release oexcel
select c4
if reccount() > 0
	tsendto = "tmarg@fmiint.com,v.gil@marcjacobs.com,M.deLaCruz@marcjacobs.com,R.WALSH@marcjacobs.com,J.VALENCIA@marcjacobs.com,Laudy.Minyety@tollgroup.com,bill.marsh@tollgroup.com"
	tattach = "S:\MarcJacobsData\TEMP\mj_return_details.xlsx"
	tcc =""
	tfrom ="Toll EDI System Operations <fmi-transload-ops@fmiint.com>"
	tmessage = "MJ RETURNS DETAIL:  "+ttoc(date()-1)
	tsubject = "MJ RETURNS DETAIL"
	do form m:\dev\frm\dartmail2 with tsendto,tfrom,tsubject, tcc,tattach,tmessage  ,"A"

else
*!*	*		export TO "S:\MarcJacobsData\Reports\InventorySyncFiles\OB_OSnD\NO_pre_invoice_osnd_exist_" + TTOC(DATE()-1, 0)  TYPE xls
*!*			tsendto = "tmarg@fmiint.com,Tolltemp8@marcjacobs.com,O.BATISTA@marcjacobs.com"
*!*			tattach = ""
*!*			tcc =""
*!*			tFrom ="Toll EDI System Operations <fmi-transload-ops@fmiint.com>"
*!*			tmessage = "NO MJ DAILY CARTON RECEIVING:  "+Ttoc(Date()-1)
*!*			tSubject = "NO MJ DAILY CARTON RECEIVING"
*!*			Do Form m:\dev\frm\dartmail2 With tsendto,tFrom,tSubject, tcc,tattach,tmessage  ,"A"

endif



wait window at 10,10 "all done...." timeout 2


schedupdate()
_screen.caption=gscreencaption
on error

