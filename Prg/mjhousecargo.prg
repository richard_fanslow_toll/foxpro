* House Cargo report for Marc Jacobs

utilsetup("MJHOUSECARGO")

if holiday(date())
	return
endif

guserid="AUTO"
gwipbywo=.f.
gwipbybl=.f.
xpnpacct=.f.
xwip=.t.
xtitle="House Cargo"
goffname="CARTERET"

xsqlexec("select * from account where inactive=0","account",,"qq")
index on accountid tag accountid
set order to

goffice="J"

xaccountid=6303
xacctname="MARC JACOBS WHOLESALE"
xoffice="N"

xacctfilter="accountid=6303"

do intlrpt 

rpt2pdf("intlrpt","h:\fox\housecargo.pdf")

tsendto="m.colucci@marcjacobs.com, logisticsintern2@marcjacobs.com, d.diamond@marcjacobs.com, m.valentine@marcjacobs.com, m.ricco@marcjacobs.com, j.bognanni@marcjacobs.com"
*tsendto="dyoung@fmiint.com"
tattach = "h:\fox\housecargo.pdf"
tfrom ="TGF Operations <support@fmiint.com>"
tmessage = "see attached files"
tsubject = "House Cargo report"

Do Form dartmail2 With tsendto,tFrom,tSubject," ",tattach,tmessage,"A"

schedupdate()

_screen.Caption=gscreencaption
on error
