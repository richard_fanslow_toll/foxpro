lparameters xauto

if date()={2/8/17}
	xstartdt={12/1/16}
	xenddt={12/10/17}
endif

do lookups
close databases all 

xsqlexec("select * from account where inactive=0","account",,"qq")
index on accountid tag accountid
set order to

xsqlexec("select * from whoffice",,,"wh")

useca("cargo","stuff",.t.)

m.zmonth=dt2month(xstartdt)

select whoffice
scan for !test
	xoffice=whoffice.office
	goffice=xoffice
	m.office=whoffice.rateoffice
	ginvacct=whoffice.wmsaccounts
	
	xsqlexec("select *	from adj where mod='"+goffice+"' and between(adjdt,{"+dtoc(xstartdt)+"},{"+dtoc(xenddt)+"})",,,"wh")

	if xauto
		xinvenfilter=".t."
		xsqlexec("select * from inven where mod='"+goffice+"'",,,"wh")
		index on accountid tag accountid
		set order to
	else
		xinvenfilter=".t."
		xsqlexec("select * from eominven where zmonth='"+dt2month(xstartdt)+"' and half=.f. and mod='"+goffice+"'","inven",,"wh")
	endif

	select account
	scan for &ginvacct 
		m.accountid=accountid
		m.acctname=acctname
		wait window "Processing Office "+xoffice+", Account "+m.acctname nowait

		xsqlexec("select units, totqty, totcube, pack, inwolog.accountid from inwolog, indet " + ;
			"where inwolog.inwologid=indet.inwologid and inwolog.accountid="+transform(m.accountid)+" " + ;
			"and inwolog.mod='"+goffice+"' and between(date_rcvd,{"+dtoc(xstartdt)+"},{"+dtoc(xenddt)+"}) " + ;
			"and cyclecount=0 and ppadj=0 and physinv=0 and dsap=0","xdytemp",,"wh")

		select sum(iif(!units,totqty,0)) as ctnsin, ;
			sum(iif(!units,totcube,0)) as cubein, ;
			sum(iif(!units,val(pack)*totqty,0)) as packin, ;
			sum(iif(units,totqty,0)) as gohin, ;
			sum(iif(between(val(pack),1,5) and !units,totqty,0)) as pack1, ;
			sum(iif(between(val(pack),6,11) and !units,totqty,0)) as pack6, ;
			sum(iif(between(val(pack),12,17) and !units,totqty,0)) as pack12, ;
			sum(iif(between(val(pack),18,23) and !units,totqty,0)) as pack18, ;
			sum(iif(between(val(pack),24,35) and !units,totqty,0)) as pack24, ;
			sum(iif(between(val(pack),36,47) and !units,totqty,0)) as pack36, ;
			sum(iif(val(pack)=48 and !units,totqty,0)) as pack48, ;
			sum(iif(val(pack)>48 and !units,totqty,0)) as packover ;
			from xdytemp group by accountid into cursor xtemp

		scatter memvar
		if m.ctnsin>0
			m.avgpack=round(m.packin/m.ctnsin,1)
		else
			m.avgpack=0
		endif

		xsqlexec("select style, color, id from inwolog, indet " + ;
			"where inwolog.inwologid=indet.inwologid and inwolog.accountid="+transform(m.accountid)+" " + ;
			"and between(date_rcvd,{"+dtoc(xstartdt)+"},{"+dtoc(xenddt)+"}) " + ;
			"and inwolog.mod='"+goffice+"' and zreturns=0 and returntostock=0 and cyclecount=0 and ppadj=0 " + ;
			"and unbilled=0 and sp=0 and physinv=0 and dsap=0","xdytemp",,"wh")

		select style, color, id from xdytemp group by style, color, id into cursor xtemp

		m.numstyin=reccount("xtemp")
*

		xsqlexec("select outship.accountid, units, totqty from outship, outdet " + ;
			"where outship.outshipid=outdet.outshipid " + ;
			"and outship.mod='"+goffice+"' and outship.accountid="+transform(m.accountid)+" " + ;
			"and between(outship.pulleddt,{"+dtoc(xstartdt)+"},{"+dtoc(xenddt)+"})","xdytemp",,"wh")

		select sum(iif(!units,totqty,0)) as ctnsout, sum(iif(units,totqty,0)) as gohout ;
			from xdytemp group by accountid into cursor xtemp
		
		scatter memvar

		select sum(-totqty) as unitspulled ;
			from adj ;
			where between(adjdt,xstartdt,xenddt) ;
			and accountid=m.accountid ;
 			and inlist(comment,"UNITS PULLED","LEFTOVER UNITS") ;
			group by accountid ;
 			into cursor xtemp
 			
 		m.gohout=m.gohout+xtemp.unitspulled

		select sum(-totqty) as ctnspulled ;
			from adj ;
			where between(adjdt,xstartdt,xenddt) ;
			and accountid=m.accountid ;
 			and inlist(comment,"PICKED FROM WAVE") ;
			group by accountid ;
 			into cursor xtemp
 			
 		m.ctnsout=m.ctnsout+xtemp.ctnspulled
 		
		xsqlexec("select inwolog.accountid, units, totqty from inwolog, indet where inwolog.inwologid=indet.inwologid " + ;
			"and inwolog.accountid="+transform(m.accountid)+" and between(date_rcvd,{"+dtoc(xstartdt)+"},{"+dtoc(xenddt)+"}) " + ;
			"and inwolog.ppadj=1","xdytemp",,"wh")

		select sum(iif(!units,totqty,0)) as ppctnsin, sum(iif(units,totqty,0)) as ppgohin ;
			from xdytemp group by accountid into cursor xtemp
		
		m.ctnsout=m.ctnsout-xtemp.ppctnsin
 		m.gohout=m.gohout-xtemp.ppgohin

*
		select sum(-totqty) as ctnsrep ;
			from adj ;
			where between(adjdt,xstartdt,xenddt) ;
			and accountid=m.accountid ;
 			and inlist(comment,"P&P CARTONS CONVERTED TO UNITS","AUTO-REPLENISHMENT ON INBOUND CONFIRMATION") ;
			group by accountid ;
 			into cursor xtemp
 			
 		scatter memvar

*
		select ;
			sum(totqty) as ctnsum, ;
			count(style) as stylecount ;
			from inven ;
			where accountid=m.accountid ;
			and &xinvenfilter ;
			and !units ;
			and !pack="1 " ;
			and totqty>0 ;
			group by accountid ;
			into cursor xtemp

		m.numstyles=stylecount
		if stylecount=0
			m.avgstyctn=0
		else
			m.avgstyctn = min(round(ctnsum/stylecount,0),999999)
		endif
*
		select ;
			sum(iif(!units,totqty,0)) as ctnsinv, ;
			sum(iif(!units,totcube,0)) as cubeinv, ;
			sum(iif(units,totqty,0)) as gohinv ;
			from inven ;
			where accountid=m.accountid ;
			and &xinvenfilter ;
			group by accountid ;
			into cursor xtemp

		scatter memvar
*

		m.avgcuftin = iif(m.ctnsin>0,m.cubein/m.ctnsin,0)
		m.avgcuftinv = iif(m.ctnsinv>0,m.cubeinv/m.ctnsinv,0)

		if right(m.zmonth,2)#"12"		
			if left(m.zmonth,2)="01"
				xmonth="12"+transform(val(right(m.zmonth,2))-1)
			else
				xmonth=transform(val(left(m.zmonth,2))-1,"@L 99")+right(m.zmonth,2)
			endif

			select cargo
			locate for accountid=m.accountid and zmonth=xmonth and office=m.office
			if found()
				xprevctnsinv=cargo.ctnsinv
				xprevgohinv=cargo.gohinv
			else
				xprevctnsinv=0
				xprevgohinv=0
			endif
					
			m.ctnsadj = -(m.ctnsin-m.ctnsout-m.ctnsrep-m.ctnsinv+xprevctnsinv)
			m.gohadj = -(m.gohin-m.gohout-m.gohinv+xprevgohinv)
		endif

		select cargo
		locate for office=m.office and zmonth=dt2month(xstartdt) and accountid=m.accountid

		if m.ctnsin#0 or m.gohin#0 or m.ctnsout#0 or m.gohout#0 or m.ctnsinv#0 or m.gohinv#0 
			if !found("cargo")
				insertinto("cargo","ex",.t.)
			else
				gather memvar fields ctnsin, ctnsout, ctnsrep, ctnsadj, ctnsinv, ;
					gohin, gohout, gohadj, gohinv, ;
					pack1, pack6, pack12, pack18, pack24, pack36, pack48, packover, ;
					avgpack, numstyin, numstyles, avgstyctn, avgcuftin, avgcuftinv
			endif
		else
			select cargo
			locate for office=m.office and zmonth=dt2month(xstartdt) and accountid=m.accountid
			if found("cargo")
				delete in cargo
			endif
		endif		
	endscan

	if used("inwolog")
		use in inwolog
		use in indet
	endif

	use in inven
	use in adj
endscan

tu("cargo")