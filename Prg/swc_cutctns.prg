PARAMETERS cBOLin
PUBLIC cUCC2

lSetDel = SET("Deleted")
IF lSetDel = "OFF"
	SET DELETED ON
ENDIF

WAIT WINDOW "Now creating SWC Records cursor for BOL "+cBOLin NOWAIT
CREATE CURSOR cutuccsin (wo_num N(10),ucc c(20))  && Temp cursor

llDoNew = .F.
IF llDoNew
**************************************************************************
	lcQuery = "select style,uccs from swcdet,swcstyle where swcdet.swcdetid = swcstyle.swcdetid and bol_no='"+cBOLin+"'"
	xsqlexec(lcQuery,"swcs",,"wh")
**************************************************************************
ENDIF

gMasterOffice = cOffice
gOffice = cMod

xsqlexec("select * from swcdet where bol_no='"+cBOLin+"'","xswcdet",,"wh")

IF !EOF()
	xjfilter="swcdetid in ("
	SELECT xswcdet
	SCAN
		xjfilter=xjfilter+TRANSFORM(swcdetid)+","
	ENDSCAN
	xjfilter=LEFT(xjfilter,LEN(xjfilter)-1)+")"
ELSE
	xjfilter="1=0"
ENDIF

xsqlexec("select * from swcstyle where "+xjfilter,,,"wh")

SELECT * FROM xswcdet,swcstyle WHERE xswcdet.swcdetid = swcstyle.swcdetid INTO CURSOR swcs READWRITE
USE IN xswcdet

*
SELECT swcs

LOCATE
SCAN
	xx= MEMLINES(uccs)
	IF xx >= 1
		ALINES(aa,uccs,1)
		FOR i =1 TO ALEN(aa,1)
			INSERT INTO cutuccsin (wo_num,ucc) VALUES (swcs.wo_num_a,aa[i])
		ENDFOR
	ENDIF
ENDSCAN

SELECT * ;
	FROM cutuccsin ;
	WHERE ISDIGIT(ucc) ;
	GROUP BY wo_num,ucc ;
	INTO CURSOR cutuccs READWRITE

nCutUCCs = RECCOUNT()
SELECT cutuccs
LOCATE
*SET STEP ON
SCAN
	cUCC1 = ALLTRIM(cutuccs.ucc)
	IF LEN(cUCC1) = 19
		DO m:\dev\prg\checkdigitcalc WITH cUCC1
		REPLACE cutuccs.ucc WITH cUCC2 IN cutuccs NEXT 1
	ENDIF
ENDSCAN
INDEX ON ucc TAG ucc
LOCATE

IF !EOF()
	WAIT WINDOW "Now cutting UCCs for BOL "+cBOLin Nowait &&TIMEOUT 1
	DO "m:\dev\prg\main_removectns-sql" WITH cOffice
ELSE
	WAIT WINDOW "No UCCs to cut for BOL "+cBOLin Nowait && TIMEOUT 1
ENDIF
USE IN cutuccsin
WAIT WINDOW "SWC Records cursor for BOL "+cBOLin+"...all processed." NOWAIT

SET DELETED &lSetDel

RETURN
