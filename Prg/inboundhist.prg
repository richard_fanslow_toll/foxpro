PARAMETERS cProcTag

IF VARTYPE(cProcTag) = "C"
	cProcTag = UPPER(cProcTag)
ENDIF

DO CASE
CASE cProcTag = "P"
	RELEASE nPTQtyRec
	PUBLIC nPTQtyRec  && creates public variable to be used in the ediinlog subroutine
	SELECT xinwolog
	SET DELETED OFF
	SUM xinwolog.plinqty TO nPTQtyRec
	SET DELETED ON
	LOCATE

	SELECT 0
	useca("pthist","stuff")  && Creates updatable PTHIST cursor

	SELECT xinwolog
	LOCATE

	SCAN
		cxinwologPT = ALLTRIM(xinwolog.wo_num)
		xaccountid = xinwolog.accountid
		sq1 = [select * from pthist where accountid = ]+TRANSFORM(xaccountid)+[ and isanum = ']+cISA_Num+[' and pt = ']+cxinwologPT+[']
		xsqlexec(sq1,"p99",,"stuff")
		SELECT p99
		IF RECCOUNT("p99") > 0
			LOOP
		ENDIF
		SELECT inwolog
		LOCATE FOR inwolog.accountid = xinwolog.accountid AND inwolog.ship_ref = xinwolog.ship_ref
		IF FOUND()
			SCATTER FIELDS ship_ref,consignee,accountid,qty,ptid MEMVAR
			nPTQtyRec = nPTQtyRec+1
			SELECT pl
			SUM totqty TO lnplqty FOR pl.inwologid= inwologid.inwologid
			COUNT FOR pl.inwologid= inwologid.inwologid TO lnItemqty

			lcWarehouse = ICASE(INLIST(cOffice,"I","J"),"NJ",cOffice = "M","FL",cOffice = "L","ML",cOffice = "X","CR",cOffice = "Y","C2",INLIST(cOffice,"K","S"),"KY","SP")

			SELECT pthist
			SCATTER MEMVAR MEMO BLANK
			lnTryDYGenpk = 1
			llDYGenpk = .F.
			DO WHILE lnTryDYGenpk < 10 AND llDYGenpk= .F.
				TRY
					m.pthistid   = dygenpk("pthist","WHALL")
				CATCH
					lnTryDYGenpk=lnTryDYGenpk+1
					INKEY(0.1)
				FINALLY
					llDYGenpk  = .T.
				ENDTRY
			ENDDO
			IF lnTryDYGenpk =10
				cErrorMessage ="Error DYGenPK in PTHIST......."
				THROW
			ENDIF
			m.pt = pt.ship_ref
			m.acct_name  = cCustname
			m.accountid  = xinwolog.accountid
			m.mod        = cMod
			m.office     = cOffice
			m.filename   = cfilename
			m.consignee  = pt.consignee
			m.warehouse  = lcWarehouse
			m.qty        = inwolog.plinqty
			m.detqty     = lnptqty
			m.detitems   = lnItemqty
			m.dateloaded = DATE()
			m.timeloaded = DATETIME()
			m.isanum     = cISA_Num
			m.confirmdt  = DATETIME()
			m.confirmed  = .T.
			m.comments   = "UPLOAD GOOD"
			INSERT INTO pthist FROM MEMVAR
		ENDIF
	ENDSCAN
	tu("pthist")
	USE IN pthist
*		ENDIF
	SET DELETED ON
	WAIT WINDOW "PT TOT: "+ALLTRIM(STR(nPTQtyRec)) NOWAIT

CASE cProcTag = "E"

	lcQuery = [select * from ediinlog where accountid = ]+TRANSFORM(nAcctNum)+[ and isanum = ']+cISA_Num+[']
	xsqlexec(lcQuery,"p2",,"stuff")
	IF RECCOUNT("p2")=0
		USE IN p2
		useca("ediinlog","stuff")  && Creates updatable EDIINLOG cursor

		LogCommentStr = "Pick tickets uploaded"
		SELECT ediinlog
		SCATTER MEMVAR MEMO BLANK
		lnTryDYGenpk = 1
		llDYGenpk = .F.
		DO WHILE lnTryDYGenpk < 10 AND llDYGenpk= .F.
			TRY
				m.ediinlogid = dygenpk("ediinlog","WHALL")
			CATCH
				lnTryDYGenpk=lnTryDYGenpk+1
				INKEY(0.1)
			FINALLY
				llDYGenpk  = .T.
			ENDTRY
		ENDDO
		IF lnTryDYGenpk =10
			cErrorMessage ="Error DYGenPK in PTHIST......."
			THROW
		ENDIF
*    m.ediinlogid = dygenpk("ediinlog","WHALL")
		m.acct_name  = cCustname
		m.accountid  = nAcctNum
		m.mod = cMod
		m.office = cOffice
		m.filepath = lcPath
		m.filename = cfilename
		m.size = nFileSize
		m.FTIME = DATE()
		m.FDATETIME = DATETIME() &&tarray(thisfile,4)
		m.TYPE = "940"
		m.qty =  nPTQtyRec
		m.isanum  =  cISA_Num
		m.uploadtime = DATETIME()
		m.comments   = LogCommentStr
		m.edidata    = ""
		INSERT INTO ediinlog FROM MEMVAR
		APPEND MEMO edidata FROM [&xfile]
		tu("ediinlog")
		USE IN ediinlog
	ENDIF
OTHERWISE
*!* Can put an error here if needed
ENDCASE
nPTQtyRec = 0

RETURN
