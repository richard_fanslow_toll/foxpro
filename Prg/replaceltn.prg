
FOR accts = 1 TO 2
	CLOSE DATA ALL
	lookups()
	*!* Added a qualifier for empty BOLs to the next xsqlexec selections in order to skip those shipments which have not yet been assigned a tracking number. Joe, 02.20.2018
	IF accts = 1
		xsqlexec("select * from outship where accountid = 6521 and bol_no <> ' ' and del_date > {"+DTOC(DATE()-5)+"}",,,"wh")
	ELSE
		xsqlexec("select * from outship where accountid in ("+gbillabongaccts+") and bol_no <> ' ' and del_date > {"+DTOC(DATE()-5)+"}",,,"wh")
	ENDIF

	SELECT outship
	INDEX ON ship_ref TAG ship_ref

	SELECT ship_ref,ship_via,scac ;
		FROM outship ;
		WHERE (INLIST(outship.scac,"PRRM","SPAR","SPRI","SPAD","SFIR") OR LEFT(ALLTRIM(outship.ship_via),4) = "USPS");
		AND !"LONGTRKNUM"$outship.shipins ;
		INTO DBF "c:\tempfox\tempsync.dbf"

	IF USED('tempsync')
		USE IN tempsync
	ENDIF

	USE c:\tempfox\tempsync ALIAS tempsync
	SELECT tempsync
	LOCATE

	SCAN
		cShip_ref = ALLTRIM(tempsync.ship_ref)
		CLEAR
*		?cShip_ref
		IF accts = 1
			xsqlexec("select * from shipment where right(rtrim(str(accountid)),4) in  ("+gbcnyrecaccounts+") and pickticket = '"+cShip_ref+"'",,,"wh")
		ELSE
			xsqlexec("select * from shipment where right(rtrim(str(accountid)),4) in  ("+gbillabongaccts+") and pickticket = '"+cShip_ref+"'",,,"wh")
		ENDIF

		IF RECCOUNT('shipment')>0
			nSHID = shipment.shipmentid
			xsqlexec("select * from package where shipmentid = "+TRANSFORM(nSHID),,,"wh")
			=SEEK(cShip_ref,"outship","ship_ref")
			IF FOUND()
				SELECT outship
				IF !"LONGTRKNUM*"$outship.shipins
*				BROWSE FIELDS outship.ship_ref,outship.shipins
					WAIT WINDOW "Now correcting PT # "+cShip_ref NOWAIT
					cTrknumber = IIF(EMPTY(ALLT(package.uspstrknumber)) AND LEN(ALLTRIM(package.trknumber))=22,package.trknumber,package.uspstrknumber)
					cShipins = outship.shipins
					cShipins2 = cShipins+CHR(13)+'LONGTRKNUM*'+ALLTRIM(cTrknumber)
					IF accts = 1
						xsqlexec("update outship set shipins = '"+cShipins2+"' where accountid = 6521 and ship_ref = '"+cShip_ref+"'",,,"wh")
					ELSE
						xsqlexec("update outship set shipins = '"+cShipins2+"' where accountid in ("+gbillabongaccts+") and ship_ref = '"+cShip_ref+"'",,,"wh")
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		SELECT tempsync
	ENDSCAN
ENDFOR

USE IN outship

WAIT WINDOW "Long Trknumber Processing complete" TIMEOUT 2
