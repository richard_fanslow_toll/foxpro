* SHC_RPTS_01
*
* Create a series of daily reports for SHC.
*
* specc'ed by Chris Malcolm.
*
*
* EXE = F:\UTIL\SHCREPORTS\SHC_RPTS_01.EXE


runack("SHC_RPTS_01")

LOCAL lnError, lcProcessName, loSHC_RPTS_01

DECLARE INTEGER CreateMutex IN Win32API ;
	STRING @lpMutexAttributes, ;
	STRING bInitialOwner, ;
	STRING @lpName

DECLARE INTEGER GetLastError IN Win32API

* if running in EXE check to see if already running
IF _VFP.STARTMODE > 0 THEN
	lcProcessName = "SHC_RPTS_01"
	CreateMutex(0,CHR(1),@lcProcessName)
	lnError = GetLastError()
	IF lnError = 183
		WAIT WINDOW TIMEOUT 10 "Lifefactory Inventory, Build QTY & Schedule process is already running..."
		RETURN .F.
	ENDIF
ENDIF

utilsetup("SHC_RPTS_01")


loSHC_RPTS_01 = CREATEOBJECT('SHC_RPTS_01')
loSHC_RPTS_01.MAIN()

schedupdate()

CLOSE DATABASES ALL
RETURN

#DEFINE LOGIT 1
#DEFINE WAITIT 2
#DEFINE NOWAITIT 4
#DEFINE SENDIT 8
#DEFINE CRLF CHR(13) + CHR(10)
#DEFINE LINEFEED CHR(10)
#DEFINE RETURN_DATA_MANDATORY .F.
#DEFINE RETURN_DATA_NOT_MANDATORY .T.
#DEFINE DAILY_OT_MULTIPLIER 1.5
#DEFINE DAILY_OT20_MULTIPLIER 2.0
#DEFINE xlContinuous 1
#DEFINE xlMedium -4138
#DEFINE xlDiagonalDown 5
#DEFINE xlDiagonalUp 6
#DEFINE xlInsideHorizontal 12
#DEFINE xlInsideVertical 11
#DEFINE xlThick 4
#DEFINE xlThin 2
#DEFINE xlNone -4142
#DEFINE xlEdgeBottom 9
#DEFINE xlEdgeLeft 7
#DEFINE xlEdgeRight 10
#DEFINE xlEdgeTop 8

DEFINE CLASS SHC_RPTS_01 AS CUSTOM

	cProcessName = 'SHC_RPTS_01'

	lTestMode = .F.  && .F. FOR NORMAL OPERATION, .T. TO SUPPRESS ALL BUT DEVELOPER EMAILS AND CHANGE INPUT TABLE, ETC.

	lAutoYield = .F.

	cStartTime = TTOC(DATETIME())

	* processing properties
	cProcessDesc = "SHC Reports 01 process"
	nAccountID = 5837
	cOffice = 'N'
	
	* file/folder properties
	cReportFolder = "F:\UTIL\SHCREPORTS\REPORTS\"
	nFileHandle = 0
	cFiletoSaveAs = ""

	* object properties
	oExcel = NULL
	oWorkbook = NULL

	* date properties
	dtNow = DATETIME()

	dToday = DATE()

	cToday = DTOC(DATE())
	cFileDateTime = ''

	* wait window properties
	nWaitWindowTimeout = 2
	lWaitWindowIsOn = .T.

	* logfile properties
	lLoggingIsOn = .T.
	cLogFile = 'F:\UTIL\SHCREPORTS\LOGFILES\SHC_RPTS_01_LOG.TXT'

	* INTERNAL email properties
	lSendInternalEmailIsOn = .T.
	cFrom = 'Mark.Bennett@Tollgroup.com'
	cSendTo = 'mbennett@fmiint.com'
	cCC = ''
	cSubject = 'SHC Reports Process for ' + TRANSFORM(DATE())
	cAttach = ''
	cBodyText = ''
	cTopBodyText = ''

	cCOMPUTERNAME = ''
	cUSERNAME = ''

	FUNCTION INIT
		IF NOT DODEFAULT()
			RETURN .F.
		ENDIF
		WITH THIS
			CLEAR
			*SET RESOURCE OFF
			CLOSE DATA
			SET CENTURY ON
			SET DATE AMERICAN
			SET HOURS TO 24
			SET ANSI ON
			SET TALK OFF
			SET DELETED ON
			SET CONSOLE OFF
			SET EXCLUSIVE OFF
			SET SAFETY OFF
			SET EXACT OFF
			SET STATUS BAR ON
			SET SYSMENU OFF
			SET ENGINEBEHAVIOR 70
			_VFP.AUTOYIELD = .lAutoYield
			.cCOMPUTERNAME = GETENV("COMPUTERNAME")
			.cUSERNAME = GETENV("USERNAME")
			IF .lTestMode THEN
				.cSendTo = 'mbennett@fmiint.com'
				.cCC = ''
				.cLogFile = 'F:\UTIL\SHCREPORTS\SHC_RPTS_01_LOG_TESTMODE.TXT'
			ENDIF
			.lLoggingIsOn = .lLoggingIsOn AND NOT EMPTY(.cLogFile)
		ENDWITH
	ENDFUNC


	FUNCTION DESTROY
		WITH THIS
			IF .lLoggingIsOn  THEN
				SET ALTERNATE OFF
				SET ALTERNATE TO
			ENDIF
		ENDWITH
		DODEFAULT()
	ENDFUNC


	FUNCTION MAIN

		WITH THIS
			LOCAL lnNumberOfErrors

			TRY
				lnNumberOfErrors = 0

				IF .lLoggingIsOn THEN
					SET ALTERNATE TO (.cLogFile) ADDITIVE
					SET ALTERNATE ON
				ENDIF

				.cSubject = .cProcessDesc + ' for ' + DTOC(.dToday)

				.TrackProgress(.cProcessDesc + ' started....', LOGIT+SENDIT+NOWAITIT)
				.TrackProgress('COMPUTERNAME = ' + .cCOMPUTERNAME, LOGIT+SENDIT)
				.TrackProgress('USERNAME = ' + .cUSERNAME, LOGIT+SENDIT)
				.TrackProgress('PROJECT = SHC_RPTS_01', LOGIT+SENDIT)
				IF .lTestMode THEN
					.TrackProgress('====>> TEST MODE !!!!', LOGIT+SENDIT)
				ENDIF

				.cFileDateTime = PADL(YEAR(.dToday),4,"0") + PADL(MONTH(.dToday),2,"0") + PADL(DAY(.dToday),2,"0") + STRTRAN(SUBSTR(TIME(),1,5),":","")

				.oExcel = CREATEOBJECT("Excel.Application")
				.oExcel.displayalerts = .F.



				USE F:\WO\WODATA\DAILY IN 0
				USE F:\WO\WODATA\TDAILY IN 0
				USE F:\WO\WODATA\WOLOG IN 0
				USE F:\WO\WODATA\AVAILLOG IN 0
				USE F:\WO\WODATA\DETAIL IN 0
				USE F:\WO\WODATA\MANIFEST IN 0

				xsqlexec("select * from pudlocs",,,"wo")
				index on accountid tag accountid
				set order to

				xsqlexec("select * from account where inactive=0","account",,"qq")
				index on accountid tag accountid
				set order to

				.AccountActivityReport()

				.ContainersArrivedReport()

				*.ContainersUnloadedReport()


				WAIT CLEAR

				* this goes at end of processing
				.oExcel.QUIT()
				.oExcel = NULL


				.TrackProgress(.cProcessDesc + ' ended normally....', LOGIT+SENDIT+NOWAITIT)

			CATCH TO loError

				.TrackProgress('There was an error.',LOGIT+SENDIT)
				.TrackProgress('The Error # is: ' + TRANSFORM(loError.ERRORNO), LOGIT+SENDIT)
				.TrackProgress('The Error Message is: ' + TRANSFORM(loError.MESSAGE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred in Program: ' + TRANSFORM(loError.PROCEDURE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred at line #: ' + TRANSFORM(loError.LINENO), LOGIT+SENDIT)
				lnNumberOfErrors = lnNumberOfErrors + 1
				IF TYPE('.oExcel') = 'O' AND NOT ISNULL(.oExcel) THEN
					.oExcel.QUIT()
				ENDIF

			ENDTRY

			WAIT CLEAR
			***************** INTERNAL email results ******************************
			.TrackProgress('About to send status email.',LOGIT)
			.TrackProgress('==================================================================================================================', SENDIT)
			.TrackProgress('The logfile is: ' + .cLogFile, LOGIT+SENDIT)
			.TrackProgress(.cProcessDesc + ' started: ' + .cStartTime, LOGIT+SENDIT)
			.TrackProgress(.cProcessDesc + ' finished: ' + TTOC(DATETIME()), LOGIT+SENDIT)

			IF .lSendInternalEmailIsOn THEN
				* try to trap error from not having dartmail dll's registered on user's pc...
				TRY
					.cBodyText = .cTopBodyText + CRLF + .cBodyText

					DO FORM dartmail2 WITH .cSendTo,.cFrom,.cSubject,.cCC,.cAttach,.cBodyText,"A"
					.TrackProgress('Sent status email.',LOGIT)
				CATCH TO loError
					.TrackProgress('There was an error sending the status email.',LOGIT)
					.TrackProgress('The Error # is: ' + TRANSFORM(loError.ERRORNO), LOGIT)
					.TrackProgress('The Error Message is: ' + TRANSFORM(loError.MESSAGE), LOGIT)
					.TrackProgress('The Error occurred in Program: ' + TRANSFORM(loError.PROCEDURE), LOGIT)
					.TrackProgress('The Error occurred at line #: ' + TRANSFORM(loError.LINENO), LOGIT)
					lnNumberOfErrors = lnNumberOfErrors + 1
				ENDTRY

			ELSE
				.TrackProgress('Did not send status email: THIS.lSendInternalEmailIsOn = FALSE.',LOGIT)
			ENDIF

		ENDWITH
		RETURN
	ENDFUNC && main


	PROCEDURE AccountActivityReport
		WITH THIS

			*!*	3.  Subject:  Outbound Trailers: Loaded and/or Picked up
			*!*	"	NJ Trucking Reports-1    Account Activity - From Previous Day, WO Type: Domestic

			* NOTE: code for this report was copied from OCEANRPT.PRG 10/13/15 MB

			LOCAL lnAccountID, ldToday, lcFileDateTime, ld8DaysAgo, lcFiletoSaveAs, cFilter, oWorkbook			
			LOCAL lcSendTo, lcFrom, lcSubject, lcCC, lcAttach, lcBodyText 

			STORE "" TO CLBLFIELD1, CTXTFIELD1, XADDITFLDS

			.TrackProgress('Creating Account Activity report; please wait....',LOGIT+SENDIT+NOWAITIT)

			lnAccountID = .nAccountID

			ldToday = .dToday
			ld8DaysAgo = ldToday - 8


			cFilter = "w.accountid = lnAccountID AND BETWEEN(w.WO_DATE, ld8DaysAgo, ldToday) AND w.TYPE = 'D' AND w.OFFICE = '" + .cOffice + "'"
*!*				cFilter = "(w.accountid = " + ALLTRIM(STR(lnAccountID,4,0)) + ;
*!*					") AND BETWEEN(w.WO_DATE, {" + DTOC(ld8DaysAgo) + "}, {" + DTOC(ldToday) + "}) AND (w.TYPE = 'D') AND (w.OFFICE = '" + .cOffice + "')"

			.TrackProgress('cFilter = ' + cFilter,LOGIT+SENDIT)
			*w.accountid = xaccountid AND BETWEEN(w.WO_DATE, xstartdt, xenddt) AND w.OFFICE = xOffice AND w.TYPE = 'D'

			*!*	if latest &&only show 8 day window for p/u's and d/l's
			*!*		cfilter = cfilter + " AND (EMPTY(w.PICKEDUP) OR w.PICKEDUP>ld8DaysAgo)"+;
			*!*			" AND (EMPTY(w.DELIVERED) OR w.DELIVERED>ld8DaysAgo)"
			*!*	endif

			SELECT a.* FROM wolog w JOIN availlog a ON a.wo_num = w.wo_num ;
				WHERE &cFilter AND !ISNULL(a.wo_num) ;
				INTO CURSOR csrtemp

			SELECT * FROM csrtemp a ;
				WHERE a.called = (SELECT MAX(called) FROM availlog a2 WHERE a2.wo_num = a.wo_num) ;
				GROUP BY wo_num ;
				INTO CURSOR csravail

			ctxtfield1 = IIF(!EMPTY(ctxtfield1), ctxtfield1, 'Iif(w.type = "A", Iif(IsNull(W.FWDR), "", LEFT(W.FWDR, 15)), '+;
				'Iif(w.type = "D", IIF(w.SIZE = "CONSOL", PADR("Delivery",15), PADR("Pick Up",15)), LEFT(w.VESSEL, 15)))')

			SELECT w.*, {} AS ftexp, det.delloc AS det_delloc, ;
				IIF(w.TYPE = 'A' AND !EMPTY(w.pickedup), LEFT(DTOC(w.pickedup),5), LEFT(DTOC(a.called),5)) AS called, ;
				IIF(w.TYPE = 'A' AND !EMPTY(w.pickedup), PADR("AVAILABLE",30), PADR(a.remarks,30)) AS xstatus, ;
				IIF(!EMPTY(clblfield1), clblfield1, IIF(w.TYPE = "A", PADR("Forwarder",10), ;
				IIF(w.TYPE = "D", PADR("Type",10), PADR("Vessel",10)))) AS lbl_field1, ;
				&ctxtfield1 AS txt_field1, ;
				IIF(w.accountid=6420,PADR("Cust"+CHR(13)+"Ref No",15),IIF(INLIST(w.accountid,&gJonesAcctList,1747) AND w.TYPE#"D",PADR(CHR(13)+"Hot",15),IIF(w.TYPE = "D", PADR(CHR(13)+"Ref No",15), PADR("Broker"+CHR(13)+"Ref No",15)))) AS lbl_field2, ;
				IIF(w.accountid=6420,PADR(w.cust_ref,15),IIF(w.TYPE = "D", PADR(w.brokref,15), PADR(w.brokref,15))) AS txt_field2, ;
				IIF(w.TYPE = "A", PADR("Master AWB",10), IIF(w.TYPE = 'D', PADR("From Loc",10), PADR("Container",10))) AS lbl_field3, ;
				IIF(w.TYPE = "A", PADR(w.awb,20), IIF(w.TYPE = 'D', SPACE(20), ;
				PADR(dispcont(w.CONTAINER),20))) AS txt_field3, ;
				IIF(w.TYPE = "D", PADR("Origin",15), PADR("P/U From",15)) AS lbl_field4, ;
				IIF(w.TYPE = "D", SPACE(30), PADR(w.puloc,30)) AS txt_field4, ;
				PADR(ALLTRIM(acct.address)+", "+ALLTRIM(acct.city)+", "+ALLTRIM(acct.state),50) AS destination, ;
				PADR(ALLTRIM(acct.city),30) AS delcity, ;
				{} AS dldisp, {} AS mfstdeliv &xadditflds;
				FROM wolog w ;
				LEFT JOIN csravail a ON a.wo_num = w.wo_num ;
				LEFT JOIN DETAIL det ON det.wo_num = w.wo_num ;
				LEFT JOIN account acct ON acct.accountid = w.accountid ;
				WHERE &cFilter ;
				GROUP BY w.wo_num ;
				ORDER BY w.TYPE, w.wo_num ;
				INTO CURSOR temp READWRITE
				
*!*				SELECT * FROM WOLOG W INTO CURSOR CURTEST WHERE &cFilter
*!*				SELECT CURTEST
*!*				BROW

			SELECT temp
			SCAN
				SELECT MAX(ftexpires) AS ftexp FROM availlog WHERE wo_num = temp.wo_num INTO CURSOR xxtemp
				IF !EOF()
					REPLACE ftexp WITH xxtemp.ftexp IN temp
				ENDIF
				USE IN xxtemp

				SELECT daily
				LOCATE FOR wo_num = temp.wo_num AND dispatched=DATE() AND action='PU' AND !undel AND emptynul(pickedup)
				IF FOUND()
					REPLACE xstatus WITH "P/U IN PROGRESS" IN temp
				ENDIF

				SELECT daily
				LOCATE FOR wo_num = temp.wo_num AND action='DL' AND !undel
				IF FOUND()
					REPLACE dldisp WITH daily.dispatched IN temp
				ENDIF

				IF SEEK(temp.wo_num,"manifest","orig_wo")
					REPLACE mfstdeliv WITH manifest.delivered IN temp
				ENDIF

			ENDSCAN

			SELECT temp
			SCAN FOR TYPE = 'D'
				SELECT pudlocs
				LOCATE FOR accountid = temp.accountid AND dash = temp.toloc
				IF FOUND()
					REPLACE destination WITH ALLTRIM(pudlocs.address)+", "+ALLTRIM(pudlocs.city)+", "+ALLTRIM(pudlocs.state), ;
						delcity WITH pudlocs.city ;
						IN temp
				ENDIF

				LOCATE FOR accountid = temp.accountid AND dash = temp.fromloc
				IF FOUND()
					REPLACE temp.txt_field3 WITH pudlocs.acct_name, temp.txt_field4 WITH pudlocs.city
				ENDIF
			ENDSCAN

			SELECT temp
			LOCATE

			* Following is based on code in OCEANRPT.SCX  CMDPRINT.ZZCREATEXLS()

			.cFiletoSaveAs = .cReportFolder + "ACCT_ACTIVITY_" + .cFileDateTime + ".XLS"
			*lcFiletoSaveAs = .cFiletoSaveAs


			* ADDED column for container, per Chris Malcolm 10/9/2015 MB

			SELECT wo_num, wo_date, txt_field1, txt_field2, txt_field3, SIZE, quantity, qty_type, CONTAINER, called, xstatus, eta, ftexp, appt, appttm, pickedup, delivered, ;
				returned, txt_field4 FROM temp INTO CURSOR csrtemp


			SELECT csrtemp
			COPY TO (.cFiletoSaveAs) TYPE XL5


			oWorkbook = .oExcel.workbooks.OPEN(.cFiletoSaveAs)
			oWorkbook.worksheets[1].NAME = "Activity"

			oWorkbook.worksheets[1].RANGE("A1"+":S1").horizontalalignment = -4108
			oWorkbook.worksheets[1].RANGE("A1"+":S1").FONT.bold = .T.

			oWorkbook.worksheets[1].RANGE("A1") = "W/O No."
			oWorkbook.worksheets[1].RANGE("B1") = "Date"
			oWorkbook.worksheets[1].RANGE("C1") = ""
			oWorkbook.worksheets[1].RANGE("D1") = ""
			oWorkbook.worksheets[1].RANGE("E1") = ""
			oWorkbook.worksheets[1].RANGE("F1") = ""
			oWorkbook.worksheets[1].RANGE("G1") = "Qty"
			oWorkbook.worksheets[1].RANGE("H1") = "Type"

			oWorkbook.worksheets[1].RANGE("I1") = "Container"

			oWorkbook.worksheets[1].RANGE("J1") = "Called"
			oWorkbook.worksheets[1].RANGE("K1") = "Status"
			oWorkbook.worksheets[1].RANGE("L1") = "ETA"
			oWorkbook.worksheets[1].RANGE("M1") = "F/T Exp"
			oWorkbook.worksheets[1].RANGE("N1") = "Appt"
			oWorkbook.worksheets[1].RANGE("O1") = "Appt Tm"
			oWorkbook.worksheets[1].RANGE("P1") = "P/U Date"
			oWorkbook.worksheets[1].RANGE("Q1") = "Delivered"
			oWorkbook.worksheets[1].RANGE("R1") = "Returned"
			oWorkbook.worksheets[1].RANGE("S1") = ""

			oWorkbook.SAVE()
			oWorkbook.CLOSE()
			
			IF USED('csrtemp') THEN
				USE IN csrtemp
			ENDIF
			
			IF USED('csravail') THEN
				USE IN csravail
			ENDIF
			
			IF USED('xxtemp') THEN
				USE IN xxtemp
			ENDIF
			
			IF USED('temp') THEN
				USE IN temp
			ENDIF

			IF FILE(.cFiletoSaveAs) THEN
			
				.TrackProgress('Successfully created: ' + .cFiletoSaveAs, LOGIT+SENDIT)
				
				lcAttach = .cFiletoSaveAs

				IF .lTestMode THEN
					lcSendTo = 'mbennett@fmiint.com'
					lcCC = ''
				ELSE
					lcSendTo = 'russellrivers@gmail.com, charles.derr@searshc.com, leandra.murchison@tollgroup.com, olga.cruz@tollgroup.com, chris.malcolm@tollgroup.com'
					*lcSendTo = 'chris.malcolm@tollgroup.com'
					lcCC = 'mbennett@fmiint.com'
				ENDIF
				
				lcFrom = 'mark.bennett@tollgroup.com'
				lcSubject = 'Outbound Trailers: Loaded and/or Picked up report for ' + TRANSFORM(DATE())
				lcBodyText = 'Attached report reflects Work Order dates ' + TRANSFORM(ld8DaysAgo) + ' through ' + TRANSFORM(ldToday)
				
				DO FORM dartmail2 WITH lcSendTo,lcFrom,lcSubject,lcCC,lcAttach,lcBodyText,"A"
				
				.TrackProgress('Sent Account Activity email.',LOGIT)
				
			ELSE
			
				.TrackProgress('!!! ERROR: there was a problem creating: ' + .cFiletoSaveAs, LOGIT+SENDIT)
				
			ENDIF


		ENDWITH
		RETURN
	ENDPROC


	PROCEDURE ContainersArrivedReport
		WITH THIS
			*!*	1. Subject: Containers Arrived
			*!*	"	NJ Trucking Reports-1:   Daily Activity Report - Load Type: Picked up by Date for Previous Day

			* based on code in form 'Activity'

			.TrackProgress('Creating Containers Arrived report; please wait....',LOGIT+SENDIT+NOWAITIT)

			LOCAL ldToday, ld8DaysAgo, ldYesterday

			ldToday = .dToday
			ldYesterday = ldToday - 1
			ld8DaysAgo = ldToday - 8

			.cFiletoSaveAs = .cReportFolder + "CONTAINERS_ARRIVED_" + .cFileDateTime + ".XLS"

			xstartdt = ldYesterday
			xenddt = ldYesterday

			xfilter="between(pickedup,xstartdt,xenddt) and action='PU'"
			xorderby="pickedup"
			xdatedesc="Picked Up"

			xaccountid = .nAccountID

			if !empty(xaccountid)
				xfilter=xfilter+iif(empty(xfilter),"", " and ")+"accountid=xaccountid"
			endif

			goffice = .cOffice

			SELECT &xorderby AS DATE, PADR(IIF(entry="A",dispawb(awb),IIF(entry="D",trailer,dispcont(CONTAINER))),12) AS REFERENCE, * FROM tdaily WHERE &xfilter AND office=goffice ;
				INTO CURSOR xtemp READWRITE

			SELECT &xorderby AS DATE, PADR(IIF(entry="A",dispawb(awb),IIF(entry="D",trailer,dispcont(CONTAINER))),12) AS REFERENCE, * FROM daily WHERE &xfilter AND office=goffice;
				INTO CURSOR xtemp2

			SCAN
				SCATTER MEMVAR
				INSERT INTO xtemp FROM MEMVAR
			ENDSCAN

			xorderby=xorderby+",wo_num"

			SELECT action, DATE, dispatched, disptime(dis_time) AS dis_time, disptime(time_in) AS time_in, ;
				REFERENCE, acctname, wo_num FROM xtemp ORDER BY &xorderby INTO CURSOR xrpt READWRITE

			USE IN xtemp
			USE IN xtemp2

			SELECT xrpt
			COPY TO (.cFiletoSaveAs) TYPE XL5

			IF FILE(.cFiletoSaveAs) THEN
			
				.TrackProgress('Successfully created: ' + .cFiletoSaveAs, LOGIT+SENDIT)
				
				lcAttach = .cFiletoSaveAs

				IF .lTestMode THEN
					lcSendTo = 'mbennett@fmiint.com'
					lcCC = ''
				ELSE
					lcSendTo = 'russellrivers@gmail.com, charles.derr@searshc.com, leandra.murchison@tollgroup.com, olga.cruz@tollgroup.com, chris.malcolm@tollgroup.com'
					*lcSendTo = 'chris.malcolm@tollgroup.com'
					lcCC = 'mbennett@fmiint.com'
				ENDIF
				
				lcFrom = 'mark.bennett@tollgroup.com'
				lcSubject = 'Containers Arrived report for ' + TRANSFORM(DATE())
				lcBodyText = 'Attached report reflects Pick Up date ' + TRANSFORM(ldYesterday) && + ' through ' + TRANSFORM(ldToday)
				
				DO FORM dartmail2 WITH lcSendTo,lcFrom,lcSubject,lcCC,lcAttach,lcBodyText,"A"
				
				.TrackProgress('Sent Account Activity email.',LOGIT)
				
			ELSE
			
				.TrackProgress('!!! ERROR: there was a problem creating: ' + .cFiletoSaveAs, LOGIT+SENDIT)
				
			ENDIF

		ENDWITH
		RETURN
	ENDPROC  &&  ContainersArrivedReport


	PROCEDURE ContainersUnloadedReport
		WITH THIS

			*!*	2. Subject: Containers Unloaded
			*!*	"	NJ Warehousing:           Inbound Receiving: Receiving Work Log: Confirmed by Previous Day  


		ENDWITH
		RETURN
	ENDPROC



*!*		FUNCTION GetNonWeekendCountOfDays
*!*			LPARAMETERS tdStartDate, tdEndDate

*!*			LOCAL lnCount, ldDate
*!*			lnCount = 0
*!*			ldDate = tdStartDate

*!*			DO WHILE ldDate <= tdEndDate
*!*				* if a date is not on the weekend, AND IT IS NOT A HOLIDAY, add it to the count
*!*				IF NOT INLIST(DOW(ldDate,1),1,7) THEN
*!*					SELECT CURHOLIDAYS
*!*					LOCATE FOR HOLIDAY = ldDate
*!*					IF FOUND() THEN
*!*						.TrackProgress('Holiday not counted when averaging: ' + TRANSFORM(ldDate),LOGIT+SENDIT)
*!*					ELSE
*!*						lnCount = lnCount + 1
*!*					ENDIF
*!*				ENDIF
*!*				ldDate = ldDate + 1
*!*			ENDDO

*!*			RETURN lnCount
*!*		ENDFUNC


*!*		PROCEDURE CrossHatchSelection
*!*			*!*		    .oExcel.Selection.Borders(xlDiagonalDown).LineStyle = xlNone
*!*			*!*		    .oExcel.Selection.Borders(xlDiagonalUp).LineStyle = xlNone
*!*			WITH .oExcel.SELECTION.BORDERS(xlEdgeLeft)
*!*				.LineStyle = xlContinuous
*!*				.ColorIndex = 0
*!*				.TintAndShade = 0
*!*				.Weight = xlThin
*!*			ENDWITH
*!*			WITH .oExcel.SELECTION.BORDERS(xlEdgeTop)
*!*				.LineStyle = xlContinuous
*!*				.ColorIndex = 0
*!*				.TintAndShade = 0
*!*				.Weight = xlThin
*!*			ENDWITH
*!*			WITH .oExcel.SELECTION.BORDERS(xlEdgeBottom)
*!*				.LineStyle = xlContinuous
*!*				.ColorIndex = 0
*!*				.TintAndShade = 0
*!*				.Weight = xlThin
*!*			ENDWITH
*!*			WITH .oExcel.SELECTION.BORDERS(xlEdgeRight)
*!*				.LineStyle = xlContinuous
*!*				.ColorIndex = 0
*!*				.TintAndShade = 0
*!*				.Weight = xlThin
*!*			ENDWITH
*!*			WITH .oExcel.SELECTION.BORDERS(xlInsideVertical)
*!*				.LineStyle = xlContinuous
*!*				.ColorIndex = 0
*!*				.TintAndShade = 0
*!*				.Weight = xlThin
*!*			ENDWITH
*!*			WITH .oExcel.SELECTION.BORDERS(xlInsideHorizontal)
*!*				.LineStyle = xlContinuous
*!*				.ColorIndex = 0
*!*				.TintAndShade = 0
*!*				.Weight = xlThin
*!*			ENDWITH
*!*		ENDPROC  && CrossHatchSelection


	PROCEDURE TrackProgress
		* do any combination of Wait Window, writing to logfile, and adding to body of email,
		* based on nFlags parameter.
		LPARAMETERS tcExpression, tnFlags
		WITH THIS
			IF BITAND(tnFlags,LOGIT) = LOGIT THEN
				IF .lLoggingIsOn THEN
					?
					? .cProcessName + "  " + TTOC(DATETIME()) + ": " + tcExpression
				ENDIF
			ENDIF
			IF .lWaitWindowIsOn THEN
				IF BITAND(tnFlags,NOWAITIT) = NOWAITIT THEN
					WAIT WINDOW tcExpression NOWAIT
				ENDIF
				IF BITAND(tnFlags,WAITIT) = WAITIT THEN
					WAIT WINDOW tcExpression TIMEOUT .nWaitWindowTimeout
				ENDIF
			ENDIF
			IF BITAND(tnFlags,SENDIT) = SENDIT THEN
				IF .lSendInternalEmailIsOn THEN
					.cBodyText = .cBodyText + tcExpression + CRLF + CRLF
				ENDIF
			ENDIF
		ENDWITH
	ENDPROC  &&  TrackProgress


ENDDEFINE