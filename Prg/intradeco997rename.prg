CLOSE DATA ALL

WAIT WINDOW "" TIMEOUT 2
SELECT 0
USE F:\edirouting\FTPSETUP ALIAS FTPSETUP SHARED
LOCATE FOR FTPSETUP.TRANSFER = "997-INTRADECO-RENAME"
IF FTPSETUP.chkbusy
	WAIT WINDOW 'Rename Process Is Busy...try again later' TIMEOUT 2
	CLOSE DATABASES ALL
	CANCEL
	RETURN
ELSE
	REPLACE FTPSETUP.chkbusy WITH .T. FOR FTPSETUP.TRANSFER = "997-INTRADECO-RENAME" IN FTPSETUP
ENDIF


cInPath = "F:\FTPUSERS\Intradeco-TollUse\997out\"
cArchPath = "F:\FTPUSERS\Intradeco-TollUse\997out\archive\"
CD &cInPath
cOutPath = "F:\FTPUSERS\Intradeco\OUT\"

len1 = ADIR(ary1,"*.edi")

IF len1 < 1
	WAIT WINDOW "No files to process...exiting" TIMEOUT 2
	CLOSE DATA ALL
	RETURN
ENDIF

FOR i = 1 TO len1
	datestr = TTOC(DATETIME(),1)
	cFilenameIn = (cInPath+ALLTRIM(ary1[i,1]))
	cFilestem = "997INT"+datestr+".edi"
	cFilenameX = (cInPath+cFilestem)
	cFilenameOut = (cOutPath+cFilestem)
	cFilenameArch = (cArchPath+cFilestem)
	RENAME [&cFilenameIn] TO [&cFilenameX]
	COPY FILE [&cFilenameX] TO [&cFilenameArch]
	COPY FILE [&cFilenameX] TO [&cFilenameOut]
	DELETE FILE [&cFilenameX]
	WAIT WINDOW "" TIMEOUT 2
ENDFOR

SELECT FTPSETUP
REPLACE FTPSETUP.chkbusy WITH .F. FOR FTPSETUP.TRANSFER = "997-INTRADECO-RENAME" IN FTPSETUP

WAIT WINDOW "All 997 renaming complete" TIMEOUT 2
