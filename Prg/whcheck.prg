* Checks for errors in the warehousing data
 
lparameter xoffice, xauto, xaccountid, xhistory

if xhistory
	garchive=.t.
endif

if empty(xoffice) or (type("gldor")="L" and !gldor)
	xoffice="K"	&& <-- must be changed if running with test data
	xdir="m:\dev\whdata\"
else
	xoffice=upper(xoffice)
	if xoffice="K"
		xdir="f:\whk\whdata\"
	else
		xdir=wf(xoffice)
	endif
endif

xsqlexec("select * from whoffice","whoffice2",,"wh")
index on office tag office
set order to

=seek(xoffice,"whoffice2","office")
xmasteroffice=whoffice2.rateoffice
use in whoffice2

xinwolog=.t.
xoutwolog=.t.
xinven=.t. 
xalloc=.f.	&& dy 10/6/16

if date()={2/18/17}
	xinwolog=.f.
	xoutwolog=.t. 
	xinven=.f. 
	xalloc=.f. 
endif

close data all
set deleted on
set talk off
set strictdate to 0

xpnpaccount=pnpacct(xoffice)
xoutwologpnpaccount=strtran(xpnpaccount,"accountid","outwolog.accountid")

goffice=xoffice

if !empty(xaccountid)
	xcheckfilter="accountid="+transform(xaccountid)
else
	xcheckfilter=".t."
endif

xsqlexec("select * from inwolog where mod='"+goffice+"' and "+xcheckfilter,,,"wh")
index on inwologid tag inwologid
set order to
xsqlexec("select * from indet where mod='"+goffice+"' and "+xcheckfilter,,,"wh")
index on inwologid tag inwologid
set order to
xsqlexec("select * from inloc where mod='"+goffice+"' and "+xcheckfilter,,,"wh")
index on inwologid tag inwologid
set order to
xsqlexec("select * from pl where mod='"+goffice+"' and "+xcheckfilter,,,"wh")
index on inwologid tag inwologid
set order to

xsqlexec("select * from outwolog where mod='"+goffice+"' and "+xcheckfilter,,,"wh")
index on outwologid tag outwologid
set order to
xsqlexec("select * from outship where mod='"+goffice+"' and "+xcheckfilter,,,"wh")
index on outwologid tag outwologid
set order to
xsqlexec("select * from outdet where mod='"+goffice+"' and "+xcheckfilter,,,"wh")
index on outwologid tag outwologid
set order to
xsqlexec("select * from outloc where mod='"+goffice+"' and "+xcheckfilter,,,"wh")
index on outwologid tag outwologid
set order to

xsqlexec("select * from inven where mod='"+goffice+"'",,,"wh")
xsqlexec("select * from invenloc where mod='"+goffice+"'",,,"wh")
index on invenid tag invenid
set order to

xsqlexec("select * from whseloc where office='"+xmasteroffice+"'",,,"wh")
index on whseloc tag whseloc
set order to

create cursor xcheck ( ;
	fixed l, ;
	file c(7), ;
	id i, ;
	wo_num n(7), ;
	data c(12), ;
	amount n(13,2), ;
	oldamt n(13,2), ;
	accountid i, ;
	units l, ;
	style c(20), ;
	color c(10), ;
	idd c(10), ;
	pack c(10))

if xinwolog
	i=0
	select inwolog
	scan for !archived and !(xoffice="I" and inlist(accountid,5451,5633,5726,5742,5447,5778,6242,6252,6385,6462,6674,6603,5318,6669)) ;
	and !(xoffice="L" and inlist(accountid,5633)) ;
	and !(xoffice="M" and inlist(accountid,5742)) ;
	and !inlist(accountid,4859,4999,6114,6261,6137,6135,6179,6321,6416,6417,6418,6438,6304,6682,6651)
		i=i+1
		if int(i/1000)=i/1000
			wait wind "inwolog - "+trans(i) nowait
		endif

		select pl
		sum totqty to xx for inwologid=inwolog.inwologid and !units
		if inwolog.plinqty#xx
			we("PL",inwolog.inwologid,inwolog.wo_num,"PLINQTY",xx,inwolog.plinqty,inwolog.accountid)
		endif
		sum totqty to xx for inwologid=inwolog.inwologid and units
		if inwolog.plunitsinqty#xx
			we("PL",inwolog.inwologid,inwolog.wo_num,"PLUNITSINQTY",xx,inwolog.plunitsinqty,inwolog.accountid)
		endif
		sum totqty to xx for inwologid=inwolog.inwologid and !emptynul(confirmdt) and (!&xpnpaccount or !units) and accountid#1
		if inwolog.ploutqty#xx
			we("PL",inwolog.inwologid,inwolog.wo_num,"PLOUTQTY",xx,inwolog.ploutqty,inwolog.accountid)
		endif
		
		select indet
		sum totqty, removeqty to xx, yy for inwologid=inwolog.inwologid and !units
		if inwolog.quantity#xx
			we("INDET",inwolog.inwologid,inwolog.wo_num,"QUANTITY",xx,inwolog.quantity,inwolog.accountid)
		endif
		if inwolog.removeqty#yy
			we("INDET",inwolog.inwologid,inwolog.wo_num,"REMOVEQTY",yy,inwolog.removeqty,inwolog.accountid)
		endif
		
		sum totqty, removeqty to xx, yy for inwologid=inwolog.inwologid and units
		if inwolog.unitsinqty#xx
			we("INDET",inwolog.inwologid,inwolog.wo_num,"UNITSINQTY",xx,inwolog.unitsinqty,inwolog.accountid)
		endif
		if inwolog.unitsoutqty#yy
			we("INDET",inwolog.inwologid,inwolog.wo_num,"UNITSOUTQTY",yy,inwolog.unitsoutqty,inwolog.accountid)
		endif

		if inwolog.cycleinout#"O"
			sum totwt, totcube to xx, yy for inwologid=inwolog.inwologid and !units
			if inwolog.weight#xx
				we("INDET",inwolog.inwologid,inwolog.wo_num,"WEIGHT",xx,inwolog.weight,inwolog.accountid)
			endif
			if inwolog.cuft#yy
				we("INDET",inwolog.inwologid,inwolog.wo_num,"CUFT",yy,inwolog.cuft,inwolog.accountid)
			endif
		endif
				
		select inloc
		sum locqty, removeqty to xx, yy for inwologid=inwolog.inwologid
		if inwolog.unitsinqty+inwolog.quantity#xx
			we("INLOC",inwolog.inwologid,inwolog.wo_num,"QUANTITY",xx,inwolog.unitsinqty+inwolog.quantity,inwolog.accountid)
		endif
		if inwolog.removeqty+inwolog.unitsoutqty#yy
			we("INLOC",inwolog.inwologid,inwolog.wo_num,"REMOVEQTY",yy,inwolog.removeqty+inwolog.unitsoutqty,inwolog.accountid)
		endif
	endscan
endif

if xoutwolog
	i=0
	select outwolog
	
	scan for !archived and !splitpt and !(wo_date<{3/1/06} and picknpack) ;
	and !inlist(accountid,6261,5694,6137,6416,6417,6418,6438,6097,6414,1285,6321,6304,6603,5318,6669) ;
	and !(xoffice="I" and inlist(accountid,5451,5633,5726,5742,5447,5778,6242,6252,6385,6462,6674,6682)) ;
	and !(xoffice="L" and inlist(accountid,5633)) and !(xoffice="M" and inlist(accountid,5742)) and !inlist(accountid,4859,4999,6114,6135,6179,6664) ;
	and !inlist(wo_num,5119706,5272186,5272196,7140265,7724930,7724931,7724932)
	
		if date()={2/18/17} and wo_num#5273742
			loop
		endif

		i=i+1
		if int(i/1000)=i/1000
			wait wind "outwolog - "+trans(i) nowait
		endif

		select outship
		sum qty, ctnqty, weight, cuft to ww,xx,yy,zz for outwologid=outwolog.outwologid
		if outwolog.quantity#ww
			we("OUTSHIP",outwolog.outwologid,outwolog.wo_num,"QUANTITY",ww,outwolog.quantity,outwolog.accountid)
		endif
		if outwolog.ctnqty#xx && and !outwolog.picknpack
			we("OUTSHIP",outwolog.outwologid,outwolog.wo_num,"CTNQTY",xx,outwolog.ctnqty,outwolog.accountid)
		endif
		if outwolog.weight#yy && and !outwolog.picknpack
			we("OUTSHIP",outwolog.outwologid,outwolog.wo_num,"WEIGHT",yy,outwolog.weight,outwolog.accountid)
		endif
		if outwolog.cuft#zz && and !outwolog.picknpack
			we("OUTSHIP",outwolog.outwologid,outwolog.wo_num,"CUFT",zz,outwolog.cuft,outwolog.accountid)
		endif
		
		select outdet
		do case
		case !outwolog.picknpack and &xoutwologpnpaccount	&& outwolog.accountid=6532
			sum totqty to xx for outwologid=outwolog.outwologid and units
			sum totwt, totcube to yy,zz for outwologid=outwolog.outwologid and !units
		case outwolog.picknpack 
			sum totqty, totwt, totcube to xx,yy,zz for outwologid=outwolog.outwologid and units
		otherwise
			sum totqty, totwt, totcube to xx,yy,zz for outwologid=outwolog.outwologid and !units
		endcase
		
		if outwolog.quantity#xx and !&xoutwologpnpaccount	&& emove this someday dy 1/31/15
			we("OUTDET",outwolog.outwologid,outwolog.wo_num,"QUANTITY",xx,outwolog.quantity,outwolog.accountid)
		endif
		if outwolog.weight#yy and !outwolog.picknpack and &xoutwologpnpaccount	&& !outwolog.accountid=6532
			select outship
			locate for wo_num=outwolog.wo_num and masterpack
			if !found()
				we("OUTDET",outwolog.outwologid,outwolog.wo_num,"WEIGHT",yy,outwolog.weight,outwolog.accountid)
			endif
		endif
		if outwolog.cuft#zz and !outwolog.picknpack
			we("OUTDET",outwolog.outwologid,outwolog.wo_num,"CUFT",zz,outwolog.cuft,outwolog.accountid)
		endif
		if outwolog.weight#yy and !outwolog.picknpack
			select outship
			locate for wo_num=outwolog.wo_num and masterpack
			if !found()
				we("OUTDET",outwolog.outwologid,outwolog.wo_num,"WEIGHT",yy,outwolog.weight,outwolog.accountid)
			endif
		endif
		if outwolog.cuft#zz and !outwolog.picknpack
			we("OUTDET",outwolog.outwologid,outwolog.wo_num,"CUFT",zz,outwolog.cuft,outwolog.accountid)
		endif
		
		select outwolog
		if outwolog.pulled and !outwolog.picknpack and !&xpnpaccount and !inlist(accountid,1,5383)
			select outloc
			sum locqty to xx for outwologid=outwolog.outwologid
			if outwolog.quantity#xx
				we("OUTLOC",outwolog.outwologid,outwolog.wo_num,"QTY",xx,outwolog.quantity,outwolog.accountid)
			endif
		endif
	endscan
endif

if xinven
	i=0
	select inven
	scan
		i=i+1
		if int(i/1000)=i/1000
			wait wind "inven - "+trans(i) nowait
		endif

		select invenloc
		sum locqty to xx for invenid=inven.invenid
		if inven.totqty#xx
			replace totqty with xx, availqty with inven.totqty-inven.allocqty-inven.holdqty in inven
			we("INVEN",inven.invenid,0,"QTY",xx,inven.totqty,inven.accountid,inven.style,inven.color,inven.id,inven.pack,inven.units)
		endif
	endscan	
	
	tu("inven")				
endif

if xalloc

	set talk on
	set notify on

	wait wind "creating xinven" nowait

	select invenid, accountid, units, style, color, id, pack, ;
		totqty, holdqty, allocqty, availqty, 00000000 as outqty ;
		from inven ;
		into cursor xinven readwrite
		
	index on str(accountid,4)+trans(units)+style+color+id+pack tag match

	wait wind "creating xoutdet" nowait
		
	select outwolog.accountid, outdet.units, outdet.style, outdet.color, outdet.id, outdet.pack, ;
		sum(iif(outwolog.pulled,0,outdet.totqty)) as outqty ;
		from outwolog, outdet;
		where outwolog.outwologid=outdet.outwologid ;
		group by 1,2,3,4,5,6 ;
		into cursor xoutdet

	scan 
		if seek(str(accountid,4)+trans(units)+style+color+id+pack,"xinven","match")
			replace outqty with xoutdet.outqty in xinven
		else
			scatter memvar
			insert into xinven from memvar
		endif
	endscan

	set talk off
	set notify off

	i=0
	select xinven
	scan
		i=i+1
		if int(i/10)=i/10
			wait wind "alloc/avail - "+trans(i) nowait
		endif

		xx=xinven.outqty
		
		if !xinven.units
			xsqlexec("select * from pprep where mod='"+xoffice+"' and movedt={} and accountid="+transform(xinven.accountid)+" and style='"+xinven.style+"' and color='"+xinven.color+"' and id='"+xinven.id+"' and pack='"+xinven.pack+"'",,,"wh")
			sum totqty to pprepqty 
			xx=xx+pprepqty
		else
			xsqlexec("select * from putback where mod='"+xoffice+"' and movedt={} and accountid="+transform(xinven.accountid)+" and style='"+xinven.style+"' and color='"+xinven.color+"' and id='"+xinven.id+"' and pack='"+xinven.pack+"'",,,"wh")
			sum unitqty to putbackqty
			xx=xx+putbackqty
		endif
		if xinven.allocqty#xx
			we("ALLOC",xinven.invenid,0,"QTY",xx,xinven.allocqty,xinven.accountid,xinven.style,xinven.color,xinven.id,xinven.pack,xinven.units)
		endif

		if xinven.totqty-xinven.holdqty-xinven.allocqty#xinven.availqty
			we("INVQTY",xinven.invenid,0,"AVAILQTY",xinven.totqty-xinven.holdqty-xinven.allocqty,xinven.availqty,xinven.accountid,xinven.style,xinven.color,xinven.id,xinven.pack)
		endif
	endscan
endif

select xcheck
copy to ("f:\auto\xcheck"+xoffice)

if !xauto
	browse
endif


****
proc we

lparameter m.file, m.id, m.wo_num, m.data, m.amount, m.oldamt, xaccountid, xstyle, xcolor, xid, xpack, m.units

m.accountid=iif(empty(xaccountid),0,xaccountid)
m.style=iif(empty(xstyle),'',xstyle)
m.color=iif(empty(xcolor),'',xcolor)
m.idd=iif(empty(xid),'',xid)
m.pack=iif(empty(xpack),'',xpack)
insert into xcheck from memvar

****
*!*	procedure ispnp

*!*	lparameters xaccountid

*!*	if !empty(xaccountid)
*!*		=seek(xaccountid,"account","accountid")
*!*		xpicknpack=account.pnp&gmasteroffice
*!*	else
*!*		xpicknpack=.f.
*!*	endif

*!*	return xpicknpack


