*!* TKO PnP Breakdown. Creates alternating non-unit/unit lines, per Maria E., 02.21.2017
*!* Note: In every other account, PREPACKs have this arrangement, while PnP get a single unit line in Detail

SELECT intake

m.accountid = 5865
STORE 0 TO m.plid,m.length,m.width,m.depth,m.ctncube,m.totcube,m.ctnwt,m.totwt,npo
STORE .F. TO m.wodelete,m.archived,m.flag

SELECT distinct f2 FROM intake INTO CURSOR tempf2 WHERE RECNO()>nRec1 AND !EMPTY(f1) 
nplinqty = RECCOUNT()
IF lTesting
WAIT WINDOW "Number of distinct counted records: "+TRANSFORM(nplinqty) TIMEOUT 30
endif
replace xinwolog.plinqty WITH nplinqty IN xinwolog
USE IN tempf2

SELECT f1 AS ponum,COUNT(f2) AS ctnnum,COUNT(f2) AS Totqty, f3 AS STYLE, f4 AS po1, f5 as color,f6 AS ID, f9 AS PACK ;
	FROM intake WHERE RECNO()>nRec1 AND !EMPTY(f1) ;
	GROUP BY f1,f3,f4,f5,f6,f9 ;
	INTO CURSOR tempintake

IF lTesting
	locate
	BROWSE
*	SET STEP ON
ENDIF

SELECT tempintake
LOCATE
nCtnNum = 0
cCarton = ""

SCAN
		lAddPL = .T.
		SCATTER MEMVAR
		m.units = .F.
		nTotqty = m.Totqty
		nPack = INT(VAL(m.pack))
*!*			IF lAddPL
*!*				lAddPL = .F.
*!*				REPLACE xinwolog.plinqty WITH xinwolog.plinqty+nTotqty IN xinwolog
*!*			ENDIF
		REPLACE xinwolog.plunitsinqty WITH xinwolog.plunitsinqty+(nTotqty*nPack) IN xinwolog
		m.ponum = ALLTRIM(UPPER(m.ponum))
		m.style = ALLTRIM(UPPER(m.style))
		IF !EMPTY(m.po1)
			m.style = m.style+"/"+ALLTRIM(m.po1)
		ENDIF
		m.color = ALLTRIM(UPPER(m.color))
		m.id = STRTRAN(ALLTRIM(UPPER(m.id))," ","")
		m.echo = "PONUM*"+m.ponum+CHR(13)+"ORIGSTYLE*"+m.style+CHR(13)+"ORIGCOLOR*"+m.color+CHR(13)+"ORIGSIZE*"+m.id
		npo = npo+1
		m.po = TRANSFORM(npo)
		m.plid = m.plid+1
		INSERT INTO xpl FROM MEMVAR

		SELECT xpl
		SCATTER FIELDS EXCEPT units,pack MEMVAR MEMO 
		m.Totqty  = nPack*nTotqty
		m.units = .T.
		m.plid = m.plid+1
		m.addby = "TOLLPROC"
		m.adddt = DATETIME()
		m.addproc = "TKOPL"
		INSERT INTO xpl FROM MEMVAR
		REPLACE PACK WITH "1" NEXT 1 IN xpl
ENDSCAN

