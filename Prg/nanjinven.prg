parameters xaccountid, lwebsite, limportable, xoffice

set exclusive off
set safety off

goffice=whbldg(xoffice,xaccountid)

xsqlexec("select * from inven where mod='"+goffice+"'",,,"wh")

do case
case xaccountid=4694
	select * from inven where accountid=xaccountid and !units and totqty > 0 into cursor temp order by style,color,id
otherwise
	select * from inven where accountid=xaccountid and units and totqty > 0 into cursor temp order by style,color,id
endcase

if lwebsite
	cfile = "C:\TEMPFOX\"+transform(reports.unique_id)+".XLS"
else
	cfile = "C:\TEMPFOX\"+alltrim(acctname(xaccountid))+"_INVENTORY.XLS"
	if file(cfile)
		delete file ["]+cfile+["]
	endif
endif

if limportable
	copy fields style, color, id, pack, totqty to &cfile type xls
else
	do case
	case inlist(xaccountid,4610)
		select style,color from temp into cursor styles readwrite group by style,color
	case inlist(xaccountid,4859)
		select style from temp into cursor styles readwrite group by style
	otherwise
		select style,color from temp into cursor styles readwrite group by style,color
	endcase

	select styles
	delete for empty(style)

	create cursor xinven ( ;
		style  c(20), ;
		color  c(10), ;
		id     c(10), ;
		size1  c(10),;
		size1q n(6),;
		size2 c(10),;
		size2q n(6),;
		size3  c(10),;
		size3q n(6),;
		size4  c(10),;
		size4q n(6),;
		size5  c(10),;
		size5q n(6),;
		size6  c(10),;
		size6q n(6),;
		size7  c(10),;
		size7q n(6),;
		size8  c(10),;
		size8q n(6),;
		size9  c(10),;
		size9q n(6),;
		size10  c(10),;
		size10q n(6),;
		size11  c(10),;
		size11q n(6),;
		size12  c(10),;
		size12q n(6),;
		size13  c(10),;
		size13q n(6),;
		size14  c(10),;
		size14q n(6),;
		size15  c(10),;
		size15q n(6),;
		size16  c(10),;
		size16q n(6),;
		size17  c(10),;
		size17q n(6),;
		size18  c(10),;
		size18q n(6),;
		size19  c(10),;
		size19q n(6),;
		size20  c(10),;
		size20q n(6),;
		size21  c(10),;
		size21q n(6),;
		size22  c(10),;
		size22q n(6),;
		size23  c(10),;
		size23q n(6),;
		size24  c(10),;
		size24q n(6),;
		size25  c(10),;
		size25q n(6),;
		size26  c(10),;
		size26q n(6),;
		size27  c(10),;
		size27q n(6),;
		size28  c(10),;
		size28q n(6),;
		size29  c(10),;
		size29q n(6),;
		size30  c(10),;
		size30q n(6),;
		size31  c(10),;
		size31q n(6),;
		size32  c(10),;
		size32q n(6),;
		size33  c(10),;
		size33q n(6),;
		size34  c(10),;
		size34q n(6),;
		size35  c(10),;
		size35q n(6),;
		size36  c(10),;
		size36q n(6),;
		size37  c(10),;
		size37q n(6),;
		size38  c(10),;
		size38q n(6),;
		size39  c(10),;
		size39q n(6),;
		size40  c(10),;
		size40q n(6),;
		size41  c(10),;
		size41q n(6),;
		size42  c(10),;
		size42q n(6),;
		size43  c(10),;
		size43q n(6),;
		size44  c(10),;
		size44q n(6),;
		size45  c(10),;
		size45q n(6))

	select styles
	goto top
	scan
		select xinven
		append blank
		replace xinven.style with styles.style in xinven

		if inlist(xaccountid,4610)
			replace xinven.color with styles.color in xinven
			select id,totqty,val(id) as sizeval from temp where temp.style= xinven.style and temp.color =xinven.color into cursor sizes readwrite
		endif

		if inlist(xaccountid,4859)
			select color,totqty,val(color) as sizeval from temp where temp.style= xinven.style into cursor sizes readwrite
		endif

		specialsizes = .f.
		select sizes
		scan
			if inlist(right(alltrim(id),1),"A","P","T","M")
				specialsizes = .t.
			endif
		endscan

		if specialsizes and inlist(xaccountid,4610)
			index on right(alltrim(id),1)+str(sizeval) tag sz ascending
		else
			index on sizeval tag sz ascending
		endif
		set order to sz
		goto top
		i=1

		scan
			stylefield= "size"+alltrim(str(i))
			qtyfield= "size"+alltrim(str(i))+"Q"
			if inlist(xaccountid,4610)
				replace xinven.&stylefield with sizes.id
			endif
			if inlist(xaccountid,4859)
				replace xinven.&stylefield with sizes.color
			endif

			replace xinven.&qtyfield with sizes.totqty
			i=i+1
		endscan

	endscan

	* CREATE SPREADSHEET

	oexcel=createobject("excel.application")
	oexcel.displayalerts = .f.
	oworkbook=oexcel.workbooks.add()
	osheet1=oworkbook.worksheets[1]

	with oexcel.activesheet.columns[1]
		.columnwidth =.columnwidth+6
	endwith

	for i = 3 to 35
		with oexcel.activesheet.columns[i]
			.columnwidth =.columnwidth-3
		endwith
	next i

	with osheet1

		.range("A1").value = acctname(xaccountid)
		.range("A2").value = ""
		.range("A3").value = ""
		.range("A3").value = "Generated on "+trans(date())

		select xinven
		goto top

		i=6
		scan
			srow=trans(i)
			qrow=trans(i+1)

if xinven.style='2M071320020250S'
set step on 
endif

			.range("A"+srow).value="'"+xinven.style
			.range("B"+srow).value=xinven.color
			.range("A"+srow).interior.color = rgb(192,192,192)
			.range("B"+srow).interior.color = rgb(192,192,192)


			.range("A"+srow).font.bold=.t.
			.range("B"+srow).font.bold=.t.

			.range("A"+srow).borders(7).linestyle=1
			.range("A"+srow).borders(8).linestyle=1
			.range("A"+srow).borders(9).linestyle=1
			.range("A"+srow).borders(10).linestyle=1

			.range("A"+srow).borders(7).weight=-4138
			.range("A"+srow).borders(8).weight=-4138
			.range("A"+srow).borders(9).weight=-4138
			.range("A"+srow).borders(10).weight=-4138

			.range("B"+srow).borders(7).linestyle=1
			.range("B"+srow).borders(8).linestyle=1
			.range("B"+srow).borders(9).linestyle=1
			.range("B"+srow).borders(10).linestyle=1

			.range("B"+srow).borders(7).weight=-4138
			.range("B"+srow).borders(8).weight=-4138
			.range("B"+srow).borders(9).weight=-4138
			.range("B"+srow).borders(10).weight=-4138

			lcformula = "=Sum(C"+qrow+":GG"+qrow+")"
			.range("A"+qrow).formula= lcformula &&=Sum(D+&qrow:GG+&qrow)"

			ncolasc1 = 32 &&space
			ncolasc2 = 67 &&col C
			for ncol = 1 to 45
				ccol = transform(ncol)
				cxlscol = alltrim(chr(ncolasc1))+alltrim(chr(ncolasc2))

				.range(cxlscol+srow).value=xinven.size&ccol
				if !empty(xinven.size&ccol.q)
					.range(cxlscol+qrow).value=xinven.size&ccol.q
					.range(cxlscol+srow).interior.color = rgb(192,192,192)
				endif

				ncolasc2 = ncolasc2+1
				if ncolasc2 > 90
					ncolasc2 = 65
					ncolasc1 = 65
				endif
			endfor

			i=i+4
		endscan
	endwith

	oworkbook.saveas(cfile)
	oexcel.quit()
	release oworkbook, oexcel, osheet1

	use in styles
	use in xinven
endif


if lwebsite
	select pdf_file
	insert into pdf_file (unique_id) values (reports.unique_id)
	**in order to avoid "VFPOLEDB" "provider ran out of memory" errors for large files (>2mb) no longer move file 
	**  thru linked server, just copy file directly to \\iis2\apps\CargoTrackingWeb\reports\ - mvw 03/08/13
	*append memo memo from &cfile overwrite
	copy file "&cfile" to \\iis2\apps\CargoTrackingWeb\reports\*.*
	replace memo with "FILE COPIED TO REPORT FOLDER" in pdf_file
	wait clear

	use in inven
	use in temp

	if used("sizes")
		use in sizes
	endif
else
	close data all
	wait window at 10,10 "Complete" timeout 1
endif
