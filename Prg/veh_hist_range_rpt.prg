LPARAMETERS tcStartingVehno, tcEndingVehno, tcReportMode, tcVehicleType, tlActive, tdstart, tdend, tcMaintCodes, tcRepairCodes, tcDivision

#DEFINE CRLF CHR(13) + CHR(10)
LOCAL lcCentury, lnYearsInService, lnTotOil, lnTotParts, lnTotTires, lcVehYear, lcVEHNO, lcTABLE, lcStartingVehno
LOCAL lnStartingVehno, lnEndingVehno, lcVEHNOWHERE, lnTotMileage, lcOutputFile, lcVehicleType, lcSQL, lcExact, lcANSI
LOCAL ARRAY laOrdhdr(1)

PRIVATE pnAvgYears, pnAvgHours, pnAvgMileage, pnAvgCost, pnAvgCostPerMile, pnTotCost, pnTotMileage, pnNumberOfVehicles
PRIVATE pcFilters
STORE 0 TO pnAvgYears, pnAvgHours, pnAvgMileage, pnAvgCost, pnAvgCostPerMile, pnTotCost, pnTotMileage, pnNumberOfVehicles
pcFilters = "Maintenance Types in " + tcMaintCodes + "' Repair Causes in " + tcRepairCodes



lcVehicleType = UPPER(ALLTRIM(tcVehicleType))

lcCentury = SET('CENTURY')

SET CENTURY ON
SET SAFETY OFF

* these will be used in SELECTS for trucks & trailers
lnStartingVehno = VAL(tcStartingVehno)
lnEndingVehno = VAL(tcEndingVehno)

IF !USED('ordhdr')
	USE SH!ordhdr IN 0
ENDIF
IF !USED('repdetl')
	USE SH!repdetl IN 0
ENDIF
IF !USED('tiredetl')
	USE SH!tiredetl IN 0
ENDIF
IF !USED('oildetl')
	USE SH!oildetl IN 0
ENDIF
IF USED('temp') THEN
	USE IN temp
ENDIF
IF USED('temp2') THEN
	USE IN temp2
ENDIF

* create temp2 cursor which drives report
SELECT * FROM ordhdr WHERE .F. INTO CURSOR temp2 READWRITE

ALTER TABLE temp2 ADD COLUMN start_mile N(10)
ALTER TABLE temp2 ADD COLUMN end_mile   N(10)
ALTER TABLE temp2 ADD COLUMN totmileage N(10)
ALTER TABLE temp2 ADD COLUMN st_date    d
ALTER TABLE temp2 ADD COLUMN end_date   d
ALTER TABLE temp2 ADD COLUMN service    N(4,2)
ALTER TABLE temp2 ADD COLUMN oilsvc     N(4)
ALTER TABLE temp2 ADD COLUMN repairsvc  N(4)
ALTER TABLE temp2 ADD COLUMN tiresvc    N(4)
ALTER TABLE temp2 ADD COLUMN pmsvc      N(4)
ALTER TABLE temp2 ADD COLUMN wsvc       N(4)
ALTER TABLE temp2 ADD COLUMN washsvc    N(4)

* added these to count Rep Cause types for JO 4/17/13 MB
ALTER TABLE temp2 ADD COLUMN rother     N(4)
ALTER TABLE temp2 ADD COLUMN raccident  N(4)
ALTER TABLE temp2 ADD COLUMN rdamage    N(4)

ALTER TABLE temp2 ADD COLUMN vehtype    C(8)
ALTER TABLE temp2 ADD COLUMN vehyear    C(4)

INDEX ON VEHNO TAG VEHNO
INDEX ON MAINTTYPE TAG MAINTTYPE
INDEX ON REPCAUSE TAG REPCAUSE

* open THE vehicle master which we will need to get the vehicle year
DO CASE
	CASE UPPER(ALLTRIM(tcVehicleType)) == "TRUCK"
		IF NOT USED('TRUCKS')
			USE SH!TRUCKS IN 0 ALIAS TRUCKS
		ENDIF
	CASE UPPER(ALLTRIM(tcVehicleType)) == "TRAILER"
		IF NOT USED('TRAILER')
			USE SH!TRAILER IN 0 ALIAS TRAILER
		ENDIF
	CASE UPPER(ALLTRIM(tcVehicleType)) == "MISC"  && "MISC" = FORKLIFTS
		IF NOT USED('VEHMAST')
			USE SH!VEHMAST IN 0 ALIAS VEHMAST
		ENDIF
	OTHERWISE
		* nothing
ENDCASE


************************************************************
************************************************************
* get cursor of vehicle #s to drive multi-vehicle report

WAIT WINDOW NOWAIT "Retrieving list of " + tcVehicleType + " #s...."

IF USED('CURVEHNORANGE') THEN
	USE IN CURVEHNORANGE
ENDIF

IF USED('CURMAXMILE') THEN
	USE IN CURMAXMILE
ENDIF

* determine vehno field name and table name, etc., for following SELECTs
DO CASE
	CASE UPPER(ALLTRIM(tcVehicleType)) == "TRUCK"
		lcVEHNO = "PADR(ALLTRIM(TRUCK_NUM),6,' ')"  && needed because TRUCK_NUM is c(8) 
		lcTABLE = "TRUCKS"
		* treat vehno's as numeric in Select
		lcVEHNOWHERE = "VAL(TRUCK_NUM)"
		lcStartingVehno = "lnStartingVehno"
		lcEndingVehno = "lnEndingVehno"
	CASE UPPER(ALLTRIM(tcVehicleType)) == "TRAILER"
*!*			lcVEHNO = "PADR(ALLTRIM(TRAIL_NUM),6,' ')"  && needed because TRAIL_NUM is c(8) 
*!*			lcTABLE = "TRAILER"
*!*			* treat vehno's as numeric in Select
*!*			lcVEHNOWHERE = "VAL(TRAIL_NUM)"
*!*			lcStartingVehno = "lnStartingVehno"
*!*			lcEndingVehno = "lnEndingVehno"
		
		* revised to treat as char, because some trailer #s have leading alpha chars 11/20/14 mb
		lcVEHNO = "TRAIL_NUM"  
		lcTABLE = "TRAILER"
		lcVEHNOWHERE = "ALLTRIM(TRAIL_NUM)"
		lcStartingVehno = "ALLTRIM(tcStartingVehno)"
		lcEndingVehno = "ALLTRIM(tcEndingVehno)"
		
	CASE UPPER(ALLTRIM(tcVehicleType)) == "MISC"
		lcVEHNO = "VEHNO"
		lcTABLE = "VEHMAST"
		* treat vehno's as character in Select
		lcVEHNOWHERE = "ALLTRIM(VEHNO)"
		lcStartingVehno = "ALLTRIM(tcStartingVehno)"
		lcEndingVehno = "ALLTRIM(tcEndingVehno)"
	OTHERWISE
		* nothing
ENDCASE

lcANSI = SET('ANSI')
lcExact = SET('EXACT')
SET EXACT ON
SET ANSI OFF
*!*	IF UPPER(ALLTRIM(tcVehicleType)) == "MISC" THEN
*!*		* FORKLIFT #S ARE ALPHANUMERIC, SO WE NEED EXACT COMPARISONS
*!*		SET EXACT ON
*!*	ENDIF
	
DO CASE
	CASE NOT EMPTY(tcDivision)
		SELECT DISTINCT &lcVEHNO AS TCVEHNO ;
			FROM &lcTABLE ;
			INTO CURSOR CURVEHNORANGE ;
			WHERE DIVISION = tcDivision ;
			AND NOT EMPTY(&lcVEHNO) ;
			AND ACTIVE = tlActive ;
			ORDER BY 1 ;
			READWRITE
	CASE (NOT EMPTY(tcStartingVehno)) AND EMPTY(tcEndingVehno)
		* GET JUST THE STARTING VEHNO
		SELECT DISTINCT &lcVEHNO AS TCVEHNO ;
			FROM &lcTABLE ;
			INTO CURSOR CURVEHNORANGE ;
			WHERE &lcVEHNOWHERE == &lcStartingVehno ;
			AND NOT EMPTY(&lcVEHNO) ;
			AND ACTIVE = tlActive ;
			READWRITE
	CASE (NOT EMPTY(tcStartingVehno)) AND (NOT EMPTY(tcEndingVehno))
		* GET STARTING/ENDING RANGE OF VEHNOs
		SELECT DISTINCT &lcVEHNO AS TCVEHNO ;
			FROM &lcTABLE ;
			INTO CURSOR CURVEHNORANGE ;
			WHERE &lcVEHNOWHERE >= &lcStartingVehno ;
			AND &lcVEHNOWHERE <= &lcEndingVehno ;
			AND NOT EMPTY(&lcVEHNO) ;
			AND ACTIVE = tlActive ;
			ORDER BY 1 ;
			READWRITE
	OTHERWISE
		* Nothing; other combinations are invalid
		SET CENTURY &lcCentury
		USE IN temp2
		RETURN
ENDCASE

SET EXACT &lcExact
SET ANSI &lcANSI


IF (NOT USED('CURVEHNORANGE')) OR EOF('CURVEHNORANGE') THEN
	=MESSAGEBOX("No Work Orders were found for the range of vehicles.",0+16,"Vehicle History Report")
	SET CENTURY &lcCentury
	USE IN temp2
	RETURN
ENDIF

IF NOT (tcReportMode = "ONELINE") THEN
	IF EMPTY(tdstart) AND RECCOUNT('curvehnorange')>20 THEN
		strmsg="Because no date range was entered, the report must be limited to a 20 vehicle range... Only the first 20 will be reported."
		MESSAGEBOX(strmsg,48,"Vehicle History Report")
		SELECT CURVEHNORANGE
		DELETE FOR RECNO()>20
	ENDIF
ENDIF

SELECT CURVEHNORANGE
SCAN
	* determine vehicle year from master vehicle tables
	lcVehYear = "?"
	DO CASE
		CASE LOWER(ALLTRIM(tcVehicleType)) == "truck"
			SELECT TRUCKS
			LOCATE FOR truck_num = CURVEHNORANGE.TCVEHNO
			IF FOUND() THEN
				lcVehYear = TRUCKS.YEAR
			ENDIF
		CASE LOWER(ALLTRIM(tcVehicleType)) == "trailer"
			SELECT TRAILER
			LOCATE FOR trail_num = CURVEHNORANGE.TCVEHNO
			IF FOUND() THEN
				lcVehYear = TRAILER.YEAR
			ENDIF
		CASE LOWER(ALLTRIM(tcVehicleType)) == "misc"
			SELECT VEHMAST
			LOCATE FOR VEHNO = CURVEHNORANGE.TCVEHNO
			IF FOUND() THEN
				lcVehYear = VEHMAST.YEAR
			ENDIF
	ENDCASE

	IF SEEK(CURVEHNORANGE.TCVEHNO,"ordhdr","vehno")
		*cfilter = IIF(EMPTY(tdstart),""," and between(crdate,tdstart,tdend)")
		cfilter = IIF(EMPTY(tdstart)," AND INLIST(MAINTTYPE," + tcMaintCodes + ") AND INLIST(REPCAUSE," + tcRepairCodes + ")"," AND BETWEEN(crdate,tdstart,tdend) AND INLIST(MAINTTYPE," + tcMaintCodes + ") AND INLIST(REPCAUSE," + tcRepairCodes + ")")
			
		cfilterSQL = IIF(EMPTY(tdstart)," AND MAINTTYPE IN (" + tcMaintCodes + ") AND REPCAUSE IN (" + tcRepairCodes + ")", ;
			" AND BETWEEN(crdate,tdstart,tdend) AND MAINTTYPE IN (" + tcMaintCodes + ") AND REPCAUSE IN (" + tcRepairCodes + ")")
			
		*lcMaintRepFilter = "MAINTTYPE IN " + tcMaintCodes + " AND REPCAUSE IN " + tcRepairCodes

		SELECT ordhdr
		*SCAN FOR VEHNO = CURVEHNORANGE.TCVEHNO &cfilter
		SCAN FOR (TYPE = lcVehicleType) AND (VEHNO = CURVEHNORANGE.TCVEHNO) &cfilter 
			WAIT WINDOW "Processing "+tcVehicleType+" #: "+ALLTRIM(CURVEHNORANGE.TCVEHNO)+", Work Order: "+TRANS(ordno) NOWAIT

			SCATTER MEMVAR
			INSERT INTO temp2 FROM MEMVAR

			SELECT oildetl
			SUM (oilqty * unitprice) TO lnTotOil FOR ordno = ordhdr.ordno

			SELECT repdetl
			SUM (repqty * unitprice) TO lnTotParts FOR ordno = ordhdr.ordno

			SELECT tiredetl
			SUM (tireqty * unitprice) TO lnTotTires FOR ordno = ordhdr.ordno

			REPLACE tirecost WITH lnTotTires, partcost WITH lnTotParts, oilcost WITH lnTotOil IN temp2
		ENDSCAN
		
*!*		SELECT temp2
*!*		brow

*!*			SELECT VEHNO, crdate, mileage FROM ordhdr WHERE VEHNO = CURVEHNORANGE.TCVEHNO ORDER BY crdate ;
*!*				INTO CURSOR curorders READWRITE


		************************************************************************************
		* get start/end crdates *not* filtered by date range.
		LOCAL lnMaxCRDate, lnMinCRDate
		
		IF USED('CURMINMAXCRDATE') THEN
			USE IN CURMINMAXCRDATE
		ENDIF
		
		SELECT MIN(CRDATE) AS MINCRDATE, MAX(CRDATE) AS MAXCRDATE FROM ordhdr ;
			WHERE (TYPE = lcVehicleType) AND (VEHNO = CURVEHNORANGE.TCVEHNO) ;
			INTO CURSOR CURMINMAXCRDATE
			
		lnMaxCRDate = CURMINMAXCRDATE.MAXCRDATE
		lnMinCRDate = CURMINMAXCRDATE.MINCRDATE 
		************************************************************************************
		
		* get mileage filtered by date range, if any.
		SELECT VEHNO, crdate, mileage FROM ordhdr ;
			WHERE (TYPE = lcVehicleType) AND (VEHNO = CURVEHNORANGE.TCVEHNO) &cfilterSQL ;
			ORDER BY crdate ;
			INTO CURSOR curorders READWRITE

		* finding most recent WO to get ending mileage
		GOTO BOTTOM IN curorders
		*REPLACE ALL end_mile WITH curorders.mileage, end_date WITH curorders.crdate FOR VEHNO = CURVEHNORANGE.TCVEHNO IN temp2
		REPLACE ALL end_mile WITH curorders.mileage, end_date WITH lnMaxCRDate FOR VEHNO = CURVEHNORANGE.TCVEHNO IN temp2
		
*!*			* ADDED this to get true Max mileage, to help in cases where the most recent WO had not entered mileage
*!*	*!*			SELECT MAX(mileage) AS MAXMILEAGE FROM ordhdr WHERE VEHNO = CURVEHNORANGE.TCVEHNO INTO CURSOR CURMAXMILE
*!*			
*!*			SELECT MAX(mileage) AS MAXMILEAGE FROM ordhdr ;
*!*				WHERE (TYPE = lcVehicleType) AND (VEHNO = CURVEHNORANGE.TCVEHNO) ;
*!*				INTO CURSOR CURMAXMILE
*!*			
*!*			REPLACE ALL end_mile WITH CURMAXMILE.MAXMILEAGE ;
*!*				FOR (VEHNO = CURVEHNORANGE.TCVEHNO) AND (CURMAXMILE.MAXMILEAGE > temp2.end_mile) IN temp2

		* finding oldest WO to get beginning mileage
		GOTO TOP IN curorders
		SELECT temp2
		*REPLACE ALL start_mile WITH curorders.mileage, st_date WITH curorders.crdate FOR VEHNO = CURVEHNORANGE.TCVEHNO IN temp2
		REPLACE ALL start_mile WITH curorders.mileage, st_date WITH lnMinCRDate FOR VEHNO = CURVEHNORANGE.TCVEHNO IN temp2
		*SKIP -1
		
*!*			SELECT temp2
*!*			LOCATE
*!*			lnYearsInService = (temp2.end_date - temp2.st_date)/365
*!*			REPLACE ALL temp2.service WITH lnYearsInService FOR VEHNO = CURVEHNORANGE.TCVEHNO IN temp2
		
		SELECT temp2
		SCAN FOR VEHNO = CURVEHNORANGE.TCVEHNO
			lnYearsInService = (temp2.end_date - temp2.st_date)/365
			REPLACE temp2.service WITH lnYearsInService IN temp2
		ENDSCAN

		SELECT temp2
		COUNT FOR (MAINTTYPE = "P") AND (VEHNO = CURVEHNORANGE.TCVEHNO) TO pmService
		COUNT FOR (MAINTTYPE = "O") AND (VEHNO = CURVEHNORANGE.TCVEHNO) TO oilService
		COUNT FOR (MAINTTYPE = "R") AND (VEHNO = CURVEHNORANGE.TCVEHNO) TO repairService
		COUNT FOR (MAINTTYPE = "T") AND (VEHNO = CURVEHNORANGE.TCVEHNO) TO tireService
		STORE 0 TO wartyService
		COUNT FOR (MAINTTYPE = "W") AND (VEHNO = CURVEHNORANGE.TCVEHNO) TO WashService
		
		COUNT FOR (REPCAUSE = "A") AND (VEHNO = CURVEHNORANGE.TCVEHNO) TO totraccident
		COUNT FOR (REPCAUSE = "D") AND (VEHNO = CURVEHNORANGE.TCVEHNO) TO totrdamage
		COUNT FOR (REPCAUSE = "O") AND (VEHNO = CURVEHNORANGE.TCVEHNO) TO totrother
		
		LOCATE



*!*	ALTER TABLE temp2 ADD COLUMN rother     N(4)
*!*	ALTER TABLE temp2 ADD COLUMN raccident    N(4)
*!*	ALTER TABLE temp2 ADD COLUMN rdamage     N(4)


		REPLACE ALL temp2.oilsvc WITH oilService, ;
			temp2.repairsvc WITH repairService, ;
			temp2.tiresvc   WITH tireService, ;
			temp2.wsvc      WITH wartyService, ;
			temp2.pmsvc     WITH pmService, ;
			temp2.washsvc   WITH WashService, ;
			temp2.raccident WITH totraccident, ;
			temp2.rdamage   WITH totrdamage, ;
			temp2.rother    WITH totrother, ;
			temp2.vehyear   WITH lcVehYear ;
			FOR VEHNO = CURVEHNORANGE.TCVEHNO ;
			IN temp2

		USE IN curorders
	ENDIF  &&  found()
ENDSCAN  && of curvehnorange

SELECT temp2
REPLACE ALL vehtype WITH tcVehicleType
LOCATE

* added 8/22/07 MB
SELECT temp2
SCAN
	lnTotMileage = temp2.end_mile - temp2.start_mile
	IF lnTotMileage < 1 THEN
		lnTotMileage = 1
	ENDIF
	REPLACE temp2.totmileage WITH lnTotMileage
ENDSCAN

IF USED('CURSTATS') THEN
	USE IN CURSTATS
ENDIF

*!*	IF UPPER(ALLTRIM(GETENV("COMPUTERNAME"))) = "MBENNETT" THEN
* select various stats that Jimmy O'Neill wants into a different cursor;
* they will display on summary page of the Summary report.
SELECT ;
	VEHNO, ;
	MAX(SERVICE) AS SERVICE, ;
	MAX(VEHHOURS) AS VEHHOURS, ;
	MAX(TOTMILEAGE) AS TOTMILEAGE, ;
	SUM(TOTLCOST+TIRECOST+OILCOST+PARTCOST+OUTSCOST) AS TOTCOST ;
	FROM TEMP2 ;
	INTO CURSOR CURSTATS ;
	GROUP BY VEHNO ;
	ORDER BY VEHNO		

SELECT CURSTATS
pnNumberOfVehicles = RECCOUNT('CURSTATS')
AVERAGE CURSTATS.SERVICE TO pnAvgYears
*AVERAGE CURSTATS.VEHHOURS TO pnAvgHours
AVERAGE CURSTATS.TOTMILEAGE TO pnAvgMileage
AVERAGE CURSTATS.TOTCOST TO pnAvgCost
SUM CURSTATS.TOTCOST TO pnTotCost
SUM CURSTATS.TOTMILEAGE TO pnTotMileage
IF pnTotMileage > 0 THEN
	pnAvgCostPerMile = pnTotCost / pnTotMileage
ENDIF

IF USED('CURSTATS') THEN
	USE IN CURSTATS
ENDIF

*!*	ENDIF



* order correctly
SELECT * FROM temp2 ORDER BY VEHNO, ordno INTO CURSOR temp

WAIT CLEAR

IF EOF('temp') THEN
	=MESSAGEBOX("No data was found for the range of vehicles.",0+16,"Vehicle History Report")
ELSE
	KEYBOARD '{CTRL+F10}' CLEAR

	*!*		IF tcReportMode = "FULL"
	*!*			report form veh_hist_range_rpt preview noconsole
	*!*		else
	*!*			report form veh_hist_range_rpt_summary preview noconsole
	*!*		ENDIF

	DO CASE
		CASE tcReportMode = "FULL"
		
			REPORT FORM veh_hist_range_rpt PREVIEW NOCONSOLE

			lcOutputFile = "F:\SHOP\ALLOCATIONREPORTS\VEHICLE_HISTORY_DETAIL_" + ;
				tcStartingVehno + "_" + tcEndingVehno + "_" + IIF(tlActive,"_ACTIVE_","_INACTIVE_") + ;
				STRTRAN(DTOC(DATE()),"/","") + ".XLS"
				
			SELECT TEMP	
			COPY TO (lcOutputFile) XL5	
			
		CASE tcReportMode = "SUMMARY"
		
			REPORT FORM veh_hist_range_rpt_summary PREVIEW NOCONSOLE

			lcOutputFile = "F:\SHOP\ALLOCATIONREPORTS\VEHICLE_HISTORY_SUMMARY_" + ;
				tcStartingVehno + "_" + tcEndingVehno + "_" + IIF(tlActive,"_ACTIVE_","_INACTIVE_") + ;
				STRTRAN(DTOC(DATE()),"/","") + ".XLS"
				
			SELECT TEMP	
			COPY TO (lcOutputFile) XL5	
			
		OTHERWISE  && tcReportMode = "ONELINE"
		
			USE IN temp
			SELECT ;
				VEHNO, ;
				VEHYEAR, ;
				VEHTYPE, ;
				ST_DATE, ;
				END_DATE, ;
				MAX(SERVICE) AS SERVICE, ;
				MAX(MILEAGE) AS MILEAGE, ;
				MAX(VEHHOURS) AS VEHHOURS, ;
				MAX(START_MILE) AS START_MILE, ;
				MAX(END_MILE) AS END_MILE, ;
				MAX(TOTMILEAGE) AS TOTMILEAGE, ;
				SUM(TOTLCOST) AS TOTAL_LABOR, ;
				SUM(TIRECOST) AS TOTAL_TIRES, ;
				SUM(OILCOST) AS TOTAL_OIL, ;
				SUM(PARTCOST) AS TOTAL_PARTS, ;
				SUM(OUTSCOST) AS TOTAL_OUTSCOST ;
				FROM TEMP2 ;
				INTO CURSOR TEMP ;
				GROUP BY VEHNO, VEHYEAR, VEHTYPE, ST_DATE, END_DATE ;
				ORDER BY VEHNO
				
			SELECT TEMP			
			REPORT FORM veh_hist_range_rpt_oneline PREVIEW NOCONSOLE
			
			* slightly different select to drive excel report
			USE IN temp
			SELECT ;
				VEHNO, ;
				VEHYEAR, ;
				VEHTYPE, ;
				ST_DATE, ;
				END_DATE, ;
				MAX(SERVICE) AS SERVICE, ;
				MAX(MILEAGE) AS MILEAGE, ;
				MAX(VEHHOURS) AS VEHHOURS, ;
				MAX(START_MILE) AS START_MILE, ;
				MAX(END_MILE) AS END_MILE, ;
				MAX(TOTMILEAGE) AS TOTMILEAGE, ;
				SUM(TOTLCOST+TIRECOST+OILCOST+PARTCOST+OUTSCOST) AS TOTAL_COST, ;
				(SUM(TOTLCOST+TIRECOST+OILCOST+PARTCOST+OUTSCOST))/(MAX(TOTMILEAGE)) AS PERMILECST ;
				FROM TEMP2 ;
				INTO CURSOR TEMP ;
				GROUP BY VEHNO, VEHYEAR, VEHTYPE, ST_DATE, END_DATE ;
				ORDER BY VEHNO

			lcOutputFile = "F:\SHOP\MILEAGEREPORTS\CPM_" + ;
				tcStartingVehno + "_" + tcEndingVehno + "_" + IIF(tlActive,"_ACTIVE_","_INACTIVE_") + ;
				STRTRAN(DTOC(DATE()),"/","") + ".XLS"
				
			SELECT TEMP	
			COPY TO (lcOutputFile) XL5	
			
			=MESSAGEBOX("An Excel version of this report has been saved at: " + lcOutputFile,0+64,"Cost Per Mile Report")

	ENDCASE

ENDIF  &&  eof('temp')

USE IN temp
USE IN temp2
USE IN CURVEHNORANGE

SET CENTURY &lcCentury
RETURN
