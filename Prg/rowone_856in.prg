*!* ROWONE_856IN.PRG...derived from TJX_856in.prg

PUBLIC xfile,asn_origin,TranslateOption,lcScreenCaption,cfilename,lTesting,NormalExit
PUBLIC cCustname,cMod,lcPath,cfilename,cTransfer,goffice
PUBLIC nFileSize,cISA_Num,LogCommentStr
ON ERROR

&& addin filter_x856 to remove non 856 type edi data

TRY
	lTesting = .F.
	IF !lTesting
		runack("ROWONEASN")
	ELSE
		CLOSE DATA ALL
	ENDIF

	lOverridebusy = .f.
	NormalExit = .F.

	_setvars(lTesting)
	gsystemmodule='ROWONE_856IN'

**need to have these fields for the EDILOG SQL store
	xfile = ""
	cfilename =""
	nAcctNum = 6468
	cCustname = "ROWONE"
	cMod = "5"
	cOffice = "C"
	gMasterOffice = cOffice
	gOffice = cMod
	LogCommentStr="856 upload"

	IF lTesting
		cOffice = "P"
		cMod = "P"
		gOffice = "P"
		gMasterOffice = "P"
	ENDIF
	cWhseMod = LOWER("wh"+cMod)

	cTransfer = "856-ROWONE"
	IF !lTesting
		SELECT 0
		USE F:\edirouting\FTPSETUP SHARED
		LOCATE FOR FTPSETUP.TRANSFER = cTransfer
		IF FOUND() AND FTPSETUP.chkbusy = .T. AND !lOverridebusy
			WAIT WINDOW "Process is busy...exiting" TIMEOUT 2
			NormalExit = .T.
			THROW
		ELSE
			REPLACE chkbusy WITH .T.,trig_time WITH DATETIME() FOR FTPSETUP.TRANSFER = cTransfer
			USE IN FTPSETUP
		ENDIF
	ENDIF

	IF USED("inwolog")
		USE IN inwolog
	ENDIF
	IF USED("pl")
		USE IN pl
	ENDIF

	xsqlexec("select * from inwolog where .f.","xinwolog",,cWhseMod)
	useca("inwolog","wh")

	IF lTesting
		USE F:\whp\whdata\pl  IN 0 ALIAS pl
		SELECT * FROM pl WHERE .F. INTO CURSOR xpl READWRITE
	ELSE
		xsqlexec("select * from pl where .f.","xpl",,cWhseMod)
		useca("pl","wh")
	ENDIF

	lcScreenCaption = "Row One ASN Uploader Process... Processing File: "
	_SCREEN.CAPTION = lcScreenCaption

	WAIT WINDOW AT 10,10 " Now uploading Row One 856's........." TIMEOUT 2
	TranslateOption = "CR_TILDE" &&"NONE"
	asn_origin = "ROWONE"

	DO createx856

*Set Default To "f:\jones856\asnin\"
	lcPath  = "f:\FTPUSERS\ROWONE\856in\"
	lcPath = IIF(lTesting, lcPath+"test\",lcPath)
	lcArchivePath = "f:\FTPUSERS\ROWONE\856in\archive\"
	lc997Path = "f:\FTPUSERS\ROWONE\997trans\"

	WAIT WINDOW AT 10,10  "Processing Row One 856s............." TIMEOUT 1

	DELIMITER = "*"

	xsqlexec("select * from dellocs",,,"wo")
	index on str(accountid,4)+location tag acct_loc
	set order to

	DO ProcessROWONEASN

	IF !lTesting
		SELECT 0
		USE F:\edirouting\FTPSETUP SHARED
		REPLACE chkbusy WITH .F. FOR FTPSETUP.TRANSFER = cTransfer
		USE IN FTPSETUP
	ENDIF

CATCH TO oErr
	IF !NormalExit
		SET STEP ON
		tsubject = " Row One 856 Upload Error ("+TRANSFORM(oErr.ERRORNO)+") at "+TTOC(DATETIME())
		tattach  = ""
		tmessage = " Row One 856 Upload Error"+CHR(13)
		lcSourceMachine = SYS(0)
		lcSourceProgram = SYS(16)

		tmessage =tmessage+ "Try/CATCH Exeception Message:"+CHR(13)+;
			[  Error: ] + STR(oErr.ERRORNO) +CHR(13)+;
			[  Procedure: ] + oErr.PROCEDURE +CHR(13)+;
			[  LineNo: ] + STR(oErr.LINENO) +CHR(13)+;
			[  Message: ] + oErr.MESSAGE +CHR(13)+;
			[  Details: ] + oErr.DETAILS +CHR(13)+;
			[  StackLevel: ] + STR(oErr.STACKLEVEL) +CHR(13)+;
			[  LineContents: ] + oErr.LINECONTENTS+CHR(13)+;
			[  UserValue: ] + oErr.USERVALUE+CHR(13)+;
			[  Computer:  ] +lcSourceMachine+CHR(13)+;
			[  940 file:  ] +cfilename+CHR(13)+;
			[  Program:   ] +lcSourceProgram

		tcc="paul.gaidis@tollgroup.com"
		tsendto  = "joe.bianchi@tollgroup.com"
		tFrom    ="TGF Corporate Communication Department <tgf-transload-ops@tollgroup.com>"
		tmessage = tmessage + CHR(13) + CHR(13) + cxErrMsg
		tattach  = ""
		DO FORM m:\dev\FRM\dartmail2 WITH tsendto,tFrom,tsubject,tcc,tattach,tmessage,"A"
	ENDIF
FINALLY
	SET STATUS BAR ON
	CLOSE DATABASES ALL
	ON ERROR
ENDTRY

***********************************************************************************************************

***********************************************************************************************************
PROCEDURE ProcessROWONEASN
*	SET STEP ON
lnNum = ADIR(tarray,(lcPath+"*.*"))

IF lnNum >= 1
	FOR thisfile = 1  TO lnNum
		xfile = lcPath+tarray[thisfile,1]  &&+"."
		archivefile = lcArchivePath+tarray[thisfile,1]  &&+"."
		to997file = lc997Path+tarray[thisfile,1]
		cfilename =tarray[thisfile,1]  &&+"."
		nFileSize = VAL(TRANSFORM(tarray(thisfile,2)))
		WAIT WINDOW "Importing file: "+xfile NOWAIT
		DO createx856a
		DO loadedifile WITH xfile,"*",TranslateOption,"TILDE"
		SELECT x856
		IF lTesting
*	BROWSE
		ENDIF

*DO loadedifile WITH xfile,"*",TranslateOption,"JONES856"
		DO ImportROWONEASN
		IF !lTesting
			DO StoreASNData
		ENDIF
		IF !lTesting
			COPY FILE [&xfile] TO [&to997file]
		ENDIF
		COPY FILE [&xfile] TO [&archivefile]
		IF FILE(archivefile) AND !lTesting
			DELETE FILE [&xfile]
		ENDIF
	NEXT thisfile
ELSE
	IF !lTesting
		schedupdate()
	ENDIF
	WAIT WINDOW AT 10,10  "No Row One 856's to Import" TIMEOUT 1
	NormalExit = .T.
	THROW
ENDIF

IF !lTesting
	schedupdate()
ENDIF

ENDPROC

******************************************************************************************************************
PROCEDURE ImportROWONEASN

TRY
	SELECT xinwolog
	SCATTER MEMVAR MEMO BLANK
	SELECT xpl
	SCATTER MEMVAR MEMO BLANK

	STORE 0 TO m.inwologid,m.plid
	m.addby = "ROWON856"
	m.adddt = DATETIME()
	m.addproc = "ROWONE856"

	m.acctname = 'ROW ONE BRANDS, INC.'
	m.accountid = nAcctNum
	cSource = IIF(lTesting,"whp",cWhseMod)
	cContainer = "XYZ"

	SET CENTURY ON
	SET DATE TO YMD

	SELECT x856
	GOTO TOP

	lnCurrentTripNumber =0
	lcHLLevel = ""
	lcVendorCode = ""
	lcCurrentHAWB = ""
	m.hdrsuppdata = ""

	POs_added = 0
	ShipmentNumber = 1
	ThisShipment = 0
	origawb = ""
	lcCurrentShipID = ""
	lcCurrentArrival = ""
	LAFreight = .F.

	SELECT x856
	SET FILTER TO
	LOCATE

	DO filter_x856 WITH "SH"

	SELECT x856
	SET FILTER TO
	LOCATE

	lcHLLevel = ""
	DO WHILE !EOF("x856")
		DO WHILE !INLIST(lcHLLevel,"S","O","E","I")
			WAIT WINDOW "Now processing Header Loop" NOWAIT

			IF TRIM(x856.segment) = "ISA"
				m.isa = ALLTRIM(x856.f13)
				cISA_Num = ALLTRIM(x856.f13)
				m.dateloaded = DATETIME()
				m.filename   = UPPER(xfile)
				cComments = ""
				cComments = "FILENAME*"+cfilename
				SELECT x856
				SKIP 1 IN x856
				LOOP
			ENDIF

			IF INLIST(x856.segment,"GS","ST","DTM")
				SELECT x856
				SKIP 1 IN x856
				IF AT("HL",x856.segment)>0
					lcHLLevel = ALLTRIM(x856.f3)
				ENDIF
				LOOP
			ENDIF

*    If Trim(x856.segment="BSN" && and alltrim(x856.f1) = "05"  && must delete then add new info
			IF INLIST(TRIM(x856.segment),"BSN","BGN") && and alltrim(x856.f1) = "05"  && must delete then add new info
				cComments2 = ""
				cComments = cComments+"SHIPID*"+ALLTRIM(x856.f2)
				cComments = cComments+CHR(13)+"HAWB*"+ALLTRIM(x856.f2)
				cComments = cComments+CHR(13)+"BSN*"+ALLTRIM(x856.f2)
				SELECT x856
				SKIP 1 IN x856
				IF AT("HL",x856.segment)>0
					lcHLLevel = ALLTRIM(x856.f3)
				ENDIF
				LOOP
			ENDIF
		ENDDO   && end if header complete

		IF lcHLLevel = "S"
			lELoop = .F.
			SELECT x856
			m.dateloaded = DATETIME()
			m.filename   = UPPER(xfile)
			DO WHILE lcHLLevel = "S"
				DO CASE
				CASE TRIM(x856.segment) = "TD1"
					m.plinqty   = VAL(ALLTRIM(x856.f2))
					m.weight    = VAL(ALLTRIM(x856.f7))
					m.cuft  = VAL(ALLTRIM(x856.f9))

				CASE TRIM(x856.segment) = "TD5"
					m.scac = ""
					m.scac  = UPPER(ALLTRIM(x856.f3))
					IF !EMPTY(m.scac)
						cComments = cComments+CHR(13)+"SCAC*"+m.scac
					ENDIF
					m.Type = ICASE(ALLTRIM(x856.f4) = "S","O",ALLTRIM(x856.f4) = "A","A","D")
					IF !EMPTY(m.Type)
						cComments = cComments+CHR(13)+"TYPE*"+m.Type
					ENDIF

				CASE TRIM(x856.segment) = "REF" AND ALLTRIM(x856.f1) = "BM"
					cComments = cComments+CHR(13)+"BOL*"+TRIM(x856.f2)

				CASE TRIM(x856.segment) = "REF" AND UPPER(TRIM(x856.f1)) = "CN"
					m.brokerref = ALLTRIM(x856.f2)
					cComments = cComments+CHR(13)+"BROKREF*"+TRIM(x856.f2)

				CASE TRIM(x856.segment) = "V1"
					m.vessel = UPPER(ALLTRIM(x856.f2))
					m.voyagenum = UPPER(ALLTRIM(x856.f4))
					cComments = cComments+CHR(13)+"VESSEL*"+TRIM(x856.f2)
					cComments = cComments+CHR(13)+"VOYAGENUM*"+TRIM(x856.f4)
				ENDCASE

				SELECT x856
				SKIP 1 IN x856
				IF TRIM(x856.segment) = "HL"
					lcHLLevel = TRIM(x856.f3)
				ENDIF
			ENDDO
		ENDIF   && end if HL S

		IF lcHLLevel = "E"
			SET STEP ON
			IF lELoop
				REPLACE xinwolog.comments WITH cComments+CHR(13)+cComments2 IN xinwolog NEXT 1
			ENDIF
			DO WHILE lcHLLevel = "E"
				IF TRIM(x856.segment) = "TD3"
					m.container = TRIM(x856.f2) +TRIM(x856.f3)
					IF m.container # cContainer
						lDoNewInwo = .T.
						cContainer = m.container
					ELSE
						lDoNewInwo = .F.
					ENDIF
					IF lDoNewInwo
						m.wo_date = DATE()
						m.inwologid = m.inwologid+1
						m.wo_num = m.inwologid
						INSERT INTO xinwolog FROM MEMVAR
						lDoNewInwo = .F.
					ENDIF
					cComments2 = ""
					cComments2 = "EQUIPTYPE*"+TRIM(x856.f1)
					m.seal = TRIM(x856.f9)
				ENDIF

				SELECT x856
				SKIP 1 IN x856
				IF TRIM(x856.segment) = "HL"
					lcHLLevel = TRIM(x856.f3)
				ENDIF
			ENDDO
		ENDIF  && end if HL E

		IF lcHLLevel = "O"
*				SET STEP ON
			DO WHILE lcHLLevel = "O"
				IF TRIM(x856.segment) = "PRF"
					IF EMPTY(cComments2)
						cComments2 = "PONUM*"+TRIM(x856.f1)
					ELSE
						cComments2 = cComments2+CHR(13)+"PONUM*"+TRIM(x856.f1)
					ENDIF
					SKIP 1 IN x856
				ELSE
					SKIP 1 IN x856
				ENDIF
				IF TRIM(x856.segment) = "HL"
					lcHLLevel = TRIM(x856.f3)
					SKIP 1 IN x856
				ENDIF
			ENDDO
		ENDIF  && end if HL O

		IF lcHLLevel = "I"
			IF !lELoop
				REPLACE xinwolog.comments WITH cComments+CHR(13)+cComments2 IN xinwolog NEXT 1
			ENDIF
*				SET STEP ON
			DO WHILE lcHLLevel = "I"
				IF TRIM(x856.segment) = "LIN"
					m.style = ALLTRIM(UPPER(x856.f3))
					m.color = ALLTRIM(UPPER(x856.f5))
					m.id = ALLTRIM(UPPER(x856.f11))
				ENDIF

				IF TRIM(x856.segment) = "SN1"  && UOM and quantities
					m.totqty = INT(VAL(x856.f4))
					REPLACE xinwolog.plunitsinqty WITH xinwolog.plunitsinqty+m.totqty IN xinwolog
					m.pack = '1'
					m.units = .T.
					SELECT x856
					SKIP 1 IN x856
				ENDIF

				IF TRIM(x856.segment) = "REF"  AND TRIM(x856.f1) = "P1"  && UPC Code
					m.echo = "UPC*"+TRIM(x856.f2)
					m.rcvd_date = DATE()
					m.plid = m.plid+1

					INSERT INTO xpl FROM MEMVAR
				ENDIF

				SELECT x856
				IF !EOF("x856")
					SKIP 1 IN x856
					IF TRIM(x856.segment) = "HL"
						lcHLLevel = TRIM(x856.f3)
					ENDIF
				ELSE
					EXIT
				ENDIF
			ENDDO &&!eof("x856")
		ENDIF  && end if HL I
	ENDDO
	cComments = ""

	IF lTesting
		SELECT xinwolog
		LOCATE
		BROWSE
		SELECT xpl
		LOCATE
		BROWSE
	ENDIF
	NormalExit = .T.

	WAIT WINDOW "Now inserting records into production tables" TIMEOUT 2
	SET STEP ON

	SELECT xinwolog
	SCAN
		SCATTER MEMVAR MEMO
		m.wo_num = dygenpk("WONUM",cSource)
		insertinto("inwolog","wh",.T.)

		SELECT xpl
		SCAN FOR xpl.inwologid = xinwolog.inwologid
			SCATTER FIELDS EXCEPT inwologid,wo_num MEMVAR MEMO
			m.inwologid = inwolog.inwologid
			m.wo_num = inwolog.wo_num
			IF lTesting
			m.inwologid = nInwologid
			m.wo_num = nWO_num
				m.plid = dygenpk("PL",cSource)
				INSERT INTO pl FROM MEMVAR
			ELSE
				insertinto("pl","wh",.T.)
			ENDIF
		ENDSCAN
	ENDSCAN

	SELECT inwolog
*	BROWSE
	SELECT pl
*	BROWSE

	SET STEP ON
	tu("inwolog")
	IF !lTesting
		tu("pl")
	ENDIF

	NormalExit = .T.

CATCH TO oErr
	IF !NormalExit
		tsubject = "URGENT: ROW ONE ASN Uplaod Error ("+TRANSFORM(oErr.ERRORNO)+") at "+TTOC(DATETIME())
		tmessage = "URGENT: This must be fixed and the file must be reimported or it could cause duplicate data"+CHR(13)+CHR(13)+"ROW ONE ASN Upload Error processing"

		tmessage =tmessage+ "Try/CATCH Exeception Message:"+CHR(13)+;
			[  Error: ] + STR(oErr.ERRORNO) +CHR(13)+;
			[  LineNo: ] + STR(oErr.LINENO) +CHR(13)+;
			[  Message: ] + oErr.MESSAGE +CHR(13)+;
			[  Procedure: ] + oErr.PROCEDURE +CHR(13)+;
			[  Details: ] + oErr.DETAILS +CHR(13)+;
			[  StackLevel: ] + STR(oErr.STACKLEVEL) +CHR(13)+;
			[  LineContents: ] + oErr.LINECONTENTS+CHR(13)+;
			[  UserValue: ] + oErr.USERVALUE+CHR(13)+;
			[  ASN Filename: ]+xfile

		tattach  = ""
		tcc=""
		tsendto  = "joe.bianchi@tollgroup.com"
		tFrom    ="TGF Corporate Communication Department <tgf-transload-ops@tollgroup.com>"
		DO FORM dartmail2 WITH tsendto,tFrom,tsubject,tcc,tattach,tmessage,"A"
	ENDIF

ENDTRY

ENDPROC


**********************************************************************************************************************
PROCEDURE StoreASNData
**********************************************************************************************************************
lcQuery = [select * from ediinlog where accountid = ]+TRANSFORM(nAcctNum)+[ and isanum = ']+cISA_Num+[']
xsqlexec(lcQuery,"p1",,"stuff")
IF RECCOUNT()=0
	USE IN p1
	useca("ediinlog","stuff")  && Creates updatable PTHIST cursor

*      LogCommentStr = "Pick tickets uploaded"
	SELECT ediinlog
	SCATTER MEMVAR MEMO
	m.ediinlogid = dygenpk("ediinlog","WHALL")
	m.acct_name  = cCustname
	m.accountid  = nAcctNum
	m.mod        = cMod
	m.office     = cOffice
	m.filepath   = lcPath
	m.filename   = cfilename
	m.size       = nFileSize
	m.FTIME      = DATE()
	m.FDATETIME  = DATETIME() &&tarray(thisfile,4)
	m.Type       = "856"
	m.qty        = 0
	m.isanum     =  cISA_Num
	m.uploadtime = DATETIME()
	m.comments   = LogCommentStr
	m.edidata    = ""
	INSERT INTO ediinlog FROM MEMVAR
	APPEND MEMO edidata FROM &xfile
	tu("ediinlog")
ENDIF
**********************************************************************************************************************
