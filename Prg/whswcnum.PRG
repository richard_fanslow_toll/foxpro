* fills in SWC # in outship
* runs 
* 	1) when an outbound WO is created in picktick 
*  	2) when the PTs are added/removed in bl or outwo

lparameters xfrom, xunused, xswctypechanged, xasktoredo

* fullpart means don't change SWC's after the 754 has updated address info
* if !xswctypechanged and (goffice="M" or outwolog.swctype="S" or (outwolog.cartonized and !inlist(outwolog.accountid,5102,1285,5453)) or outwolog.fullpart) and outwolog.comments#"SWC" 	
* changed dy 3/8/08 - we have to redo the SWCs even if the 754 has updated addess info
*    it's all been staged together, then you're shipping it on different BLs - the floor must know about this
*
* or (outwolog.cartonized and !inlist(outwolog.accountid,5102,1285,5453)) ---  removed dy 3/10/08

*if (!xswctypechanged and outwolog.swctype="S") or outwolog.comments="SWC" 	&& dy 4/5/10 - don't check if SWC type is "separated"
if outwolog.swctype="S" or outwolog.comments="SWC" 	
	return .f.
endif

if inlist(guserid,"PAULG","DYOUNG") and date()={10/30/17}
	return .f.
endif

*

if xfrom#"OUTWO"
	xoutship="outship"
	xoutshipfilter="wo_num=outwolog.wo_num"
else
	xoutship="voutship"
	xoutshipfilter=".t."
endif

*

select ship_ref, swcnum+swcover as origswc, space(7) as swcnum, consignee, address, address2, bol_no, combgroup, cnee_ref, ship_via, notonwip ;
	from (xoutship) where .f. into cursor xswc readwrite

select (xoutship)
scan for &xoutshipfilter
	scatter memvar
	m.origswc=m.swcnum+m.swcover
	m.swcnum=""
	insert into xswc from memvar
endscan

select ship_ref, swcnum+swcover as origswc, space(7) as swcnum, consignee, address, address2, bol_no, combgroup, cnee_ref, ship_via, notonwip ;
	from (xoutship) where .f. into cursor xnormal readwrite

index on bol_no+cnee_ref tag cnee_ref
index on bol_no+ship_via+cnee_ref tag blcarpo
index on bol_no tag bol_no
index on consignee+address+address2+bol_no tag conaddbol
index on consignee+address+address2+bol_no+ship_via tag conaddsv
index on str(combgroup,2)+bol_no tag groupbl
index on combgroup tag combgroup

*

replace address with name, name with "" in (xoutship) for empty(address) and !empty(name) and &xoutshipfilter

do case
case outwolog.swctype="A"	&& one per WO
	xswcnum=padl(trans(dygenpk("swcnum","wh"+tumod(outwolog.accountid))),7,"0")
	select (xoutship)
	scan for &xoutshipfilter
		replace swcnum with xswcnum in xswc for ship_ref=&xoutship..ship_ref
		insert into xnormal (swcnum,ship_ref) values (xswcnum,&xoutship..ship_ref)
	endscan
	
case outwolog.swctype="P"	&& one per PT
	select (xoutship)
	scan for &xoutshipfilter
		xswcnum=padl(trans(dygenpk("swcnum","wh"+tumod(outwolog.accountid))),7,"0")
		replace swcnum with xswcnum in xswc for ship_ref=&xoutship..ship_ref
		insert into xnormal (swcnum,ship_ref) values (xswcnum,&xoutship..ship_ref)
	endscan
	
case outwolog.swctype="O"	&& one per PO
	select (xoutship)
	scan for &xoutshipfilter
		if &xoutship..consignee="SEARS" and goffice="L"
			xsearsmiraloma=.t.
			xcnee_ref=substr(&xoutship..cnee_ref,5)
		else
			xsearsmiraloma=.f.
			xcnee_ref=&xoutship..cnee_ref
		endif
		
		if !seek(bol_no+xcnee_ref,"xnormal","cnee_ref")
			xswcnum=padl(trans(dygenpk("swcnum","wh"+tumod(outwolog.accountid))),7,"0")
			insert into xnormal (swcnum,bol_no,cnee_ref) values (xswcnum,&xoutship..bol_no,xcnee_ref)
		endif
	endscan

	select xnormal
	scan		
		if xsearsmiraloma
			replace swcnum with xnormal.swcnum in xswc for bol_no=xnormal.bol_no and substr(cnee_ref,5)=trim(xnormal.cnee_ref)
		else
			replace swcnum with xnormal.swcnum in xswc for bol_no=xnormal.bol_no and cnee_ref=xnormal.cnee_ref
		endif
	endscan
	
case outwolog.swctype="C"	&& one per PO/Carrier
	select (xoutship)
	scan for &xoutshipfilter
		if !seek(bol_no+ship_via+cnee_ref,"xnormal","blcarpo")
			xswcnum=padl(trans(dygenpk("swcnum","wh"+tumod(outwolog.accountid))),7,"0")
			insert into xnormal (swcnum,bol_no,ship_via,cnee_ref) values (xswcnum,&xoutship..bol_no,&xoutship..ship_via,&xoutship..cnee_ref)
		endif
	endscan

	select xnormal
	scan
		replace swcnum with xnormal.swcnum in xswc for bol_no=xnormal.bol_no and ship_via=xnormal.ship_via and cnee_ref=xnormal.cnee_ref
	endscan
	
case inlist(outwolog.swctype,"W","B")	&& one per BL (or one for entire WO - different BL's can't have the same SWC #)
	select (xoutship)
	scan for &xoutshipfilter
		if !seek(bol_no,"xnormal","bol_no")
			xswcnum=padl(trans(dygenpk("swcnum","wh"+tumod(outwolog.accountid))),7,"0")
			insert into xnormal (swcnum,bol_no) values (xswcnum,&xoutship..bol_no)
		endif
	endscan

	select xnormal
	scan
		replace swcnum with xnormal.swcnum in xswc for bol_no=xnormal.bol_no
	endscan

case outwolog.swctype="G"	&& one per consignee/dc (merge groups)
	select (xoutship)
	scan for &xoutshipfilter
		if !seek(consignee+address+address2+bol_no,"xnormal","conaddbol")
			xswcnum=padl(trans(dygenpk("swcnum","wh"+tumod(outwolog.accountid))),7,"0")
			insert into xnormal (swcnum,consignee,address,address2,bol_no) values (xswcnum,&xoutship..consignee,&xoutship..address,&xoutship..address2,&xoutship..bol_no)
		endif
	endscan

	select xnormal
	scan
		replace swcnum with xnormal.swcnum in xswc for bol_no=xnormal.bol_no and consignee=xnormal.consignee and address=xnormal.address and address2=xnormal.address2
	endscan

case outwolog.swctype="Y"	&& one per combined group
	select (xoutship)
	scan for &xoutshipfilter
		if !seek(combgroup,"xnormal","combgroup")
			xswcnum=padl(trans(dygenpk("swcnum","wh"+tumod(outwolog.accountid))),7,"0")
			insert into xnormal (swcnum,combgroup) values (xswcnum,&xoutship..combgroup)
		endif
	endscan

	select xnormal
	scan
		replace swcnum with xnormal.swcnum in xswc for combgroup=xnormal.combgroup
	endscan

case outwolog.swctype="Z"	&& one per consignee/dc/carrier
	select (xoutship)
	scan for &xoutshipfilter
		if !seek(consignee+address+address2+bol_no+ship_via,"xnormal","conaddsv")
			xswcnum=padl(trans(dygenpk("swcnum","wh"+tumod(outwolog.accountid))),7,"0")
			insert into xnormal (swcnum,consignee,address,address2,bol_no,ship_via) values (xswcnum,&xoutship..consignee,&xoutship..address,&xoutship..address2,&xoutship..bol_no,&xoutship..ship_via)
		endif
	endscan

	select xnormal
	scan
		replace swcnum with xnormal.swcnum in xswc for ship_via=xnormal.ship_via and bol_no=xnormal.bol_no and consignee=xnormal.consignee and address=xnormal.address and address2=xnormal.address2
	endscan

otherwise	&& one per consignee/dc (respect groups)
	select (xoutship)
	scan for &xoutshipfilter
		if combinedpt
			if !seek(str(combgroup,2)+bol_no,"xnormal","groupbl")
				xswcnum=padl(trans(dygenpk("swcnum","wh"+tumod(outwolog.accountid))),7,"0")
				insert into xnormal (swcnum,combgroup,bol_no) values (xswcnum,&xoutship..combgroup,&xoutship..bol_no)
			endif
		else
			if !seek(consignee+address+address2+bol_no,"xnormal","conaddbol")
				xswcnum=padl(trans(dygenpk("swcnum","wh"+tumod(outwolog.accountid))),7,"0")
				insert into xnormal (swcnum,consignee,address,address2,bol_no) values (xswcnum,&xoutship..consignee,&xoutship..address,&xoutship..address2,&xoutship..bol_no)
			endif
		endif
	endscan

	select xnormal
	scan
		if !empty(combgroup)
			replace swcnum with xnormal.swcnum in xswc for combgroup=xnormal.combgroup and bol_no=xnormal.bol_no
		else
			replace swcnum with xnormal.swcnum in xswc for consignee=xnormal.consignee and address=xnormal.address and address2=xnormal.address2 and bol_no=xnormal.bol_no
		endif
	endscan
endcase

xredoswc=.f.
xnewswcnum=.f.
xbloverride=.f.

if !xbloverride
	select (xoutship)
	locate for empty(swcnum) and &xoutshipfilter
	if found()
		xredoswc=.t.
		xnewswcnum=.t.
	else

		xbladdfilter=".t."
		if xfrom="BLADD"
			xbladdfilter="bol_no#'"+bl.bol_no+"'"
		endif
		
		select swcnum from xswc with (buffering=.t.) ;
			where !inlist(bol_no,"NOTHING SHIPPED","VARIOUS","PSU","PSF","1Z","783") and !notonwip and &xbladdfilter ;
			and (!inlist(ship_via,&xsmallpackagecarriers) or inlist(ship_via,&xnotsmallpackagecarriers) or inlist(outwolog.accountid,&gbillabongaccts)) ;
			group by 1 into cursor itsokiftheyareallthesame

		if reccount("itsokiftheyareallthesame")>1 or xswctypechanged

			okiwillfixit=.f.
			
			select xswc
			scan for !inlist(bol_no,"NOTHING SHIPPED","VARIOUS","PSU","PSF","1Z","783") and !notonwip and !okiwillfixit and !"OV"$ship_ref
				xship_ref=ship_ref
				xorigswc=origswc
				xswcnum=swcnum

				locate for ((origswc=xorigswc and !swcnum==xswcnum) or (swcnum=xswcnum and !origswc==xorigswc)) ;
					and !inlist(bol_no,"NOTHING SHIPPED","VARIOUS") and !notonwip and !"OV"$ship_ref and !empty(xswcnum) ;
					and (!inlist(ship_via,&xsmallpackagecarriers) or inlist(ship_via,&xnotsmallpackagecarriers) or inlist(outwolog.accountid,&gbillabongaccts))
					
				if found() and outwolog.wo_num#7702741
					assert .f.
					do case
					case outwolog.staged && and (outwolog.printed or outwolog.cartonized)  removed dy 3/30/12
						if outwolog.fullpart
							xerrmsg=crlf+crlf+"This probably happened because the 754 updated the address information."
						else
							xerrmsg=crlf+crlf+"Check the address for pick ticket "+trim(xswc.ship_ref)+"."
						endif
						xerrmsg1=crlf+crlf+"You can fix this by clicking the Recalc button on the Outbound WO screen, just be aware that the cartons may have already been labeled."
						do case
						case xfrom="BLADD"
							assert .f.
							x3winmsg("You can't create this BL because it would require a change to the SWC #s on work order "+transform(outwolog.wo_num)+" but it has already been added to the Staging screen and the labels have been printed."+xerrmsg+xerrmsg1)
							info(outwolog.wo_num,,outwolog.accountid,,,,,.t.,"Attempt to create a BL that would change SWC's on a staged & printed WO")
							xswcproblem=.t.
							return .f.
						case xfrom="BLDELETE"
							assert .f.
							x3winmsg("You can't remove this PT because it would require a change to the SWC #s on work order "+transform(outwolog.wo_num)+" but it has already been added to the Staging screen and the labels have been printed."+xerrmsg+xerrmsg1)
							info(outwolog.wo_num,,outwolog.accountid,,,,,.t.,"Attempt to delete a PT on a BL that would change SWC's on a staged & printed WO")
							xswcproblem=.t.
							return .f.
						case xasktoredo and outwolog.printed
							if x3winmsg("The SWC numbers need to be changed but the staging labels have already been printed."+xerrmsg+crlf+crlf+"Do you want to redo the staging?  If YES, then you may have to relabel all the cartons.",,"Y|N")="YES"
								okiwillfixit=.t.
								xredoswc=.t.
							else
								xswcproblem=.t.
								return .f.
							endif
						case xasktoredo and outwolog.cartonized
							if x3winmsg("The SWC numbers need to be changed but the work order has been cartonized."+xerrmsg+crlf+crlf+"Do you want to redo the staging?  If YES, then you will have to recartonize the work order.",,"Y|N")="YES"
								okiwillfixit=.t.
								xredoswc=.t.
							else
								xswcproblem=.t.
								return .f.
							endif
						case outwolog.cartonized
							assert .f.
							x3winmsg("You can't change this work order because it has been cartonized."+xerrmsg)
							xswcproblem=.t.
							return .f.
						otherwise
							assert .f.
							x3winmsg("You can't change this work order because it has already been added to the Staging screen."+xerrmsg+xerrmsg1)
							xswcproblem=.t.
							return .f.
						endcase

*		removed dy 4/12/07 - no longer running whswcnum if WO has been cartonized - restored dy 8/5/11 - removed dy 8/9/11
*!*						case outwolog.cartonized
*!*							email("Dyoung@fmiint.com","Attempt to change SWC's on cartonized WO "+transform(outwolog.wo_num),,,,,.t.,,,,,.t.,,.t.)
*!*							x3winmsg("You can't change the SWC #s on work order "+transform(outwolog.wo_num)+" because it has already been cartonized")
*!*							info(outwolog.wo_num,,outwolog.accountid,,,,,.t.,"Attempt to change SWC's on a cartonized WO")
*!*							xswcproblem=.t.
*!*							return .f.

					otherwise
						xredoswc=.t.
						exit
					endcase
				endif
				locate for ship_ref=xship_ref
			endscan
		endif
	endif
endif

if xredoswc
	if !xnewswcnum
		info(,outwolog.wo_num,,outwolog.accountid,,,,,,"SWC numbers changed")
	endif
	
	select xnormal
	scan 
		do case
		case outwolog.swctype="A"	&& one per WO
			replace swcnum with xnormal.swcnum in (xoutship) for ship_ref=xnormal.ship_ref and &xoutshipfilter

		case outwolog.swctype="P"	&& one per PT
			replace swcnum with xnormal.swcnum in (xoutship) for ship_ref=xnormal.ship_ref and &xoutshipfilter
			
		case outwolog.swctype="O"	&& one per PO
			if xsearsmiraloma
				replace swcnum with xnormal.swcnum in (xoutship) for bol_no=xnormal.bol_no and substr(cnee_ref,5)=trim(xnormal.cnee_ref) and &xoutshipfilter
			else
				replace swcnum with xnormal.swcnum in (xoutship) for bol_no=xnormal.bol_no and cnee_ref=xnormal.cnee_ref and &xoutshipfilter
			endif
			
		case outwolog.swctype="C"	&& one per PO/Carrier
			replace swcnum with xnormal.swcnum in (xoutship) for bol_no=xnormal.bol_no and ship_via=xnormal.ship_via and cnee_ref=xnormal.cnee_ref and &xoutshipfilter
			
		case inlist(outwolog.swctype,"B","W")	&& one per BL or entire WO
			replace swcnum with xnormal.swcnum in (xoutship) for bol_no=xnormal.bol_no and &xoutshipfilter

		case outwolog.swctype="G"	&& one per consignee/dc (merge groups)
			replace swcnum with xnormal.swcnum in (xoutship) for bol_no=xnormal.bol_no and consignee=xnormal.consignee and address=xnormal.address and address2=xnormal.address2 and &xoutshipfilter

		case outwolog.swctype="Z"	&& one per consignee/dc/carrier
			replace swcnum with xnormal.swcnum in (xoutship) for ship_via=xnormal.ship_via and bol_no=xnormal.bol_no and consignee=xnormal.consignee and address=xnormal.address and address2=xnormal.address2 and &xoutshipfilter

		case outwolog.swctype="Y"	&& one per combined group
			replace swcnum with xnormal.swcnum in (xoutship) for combgroup=xnormal.combgroup and &xoutshipfilter

		otherwise	&& one per consignee/dc (respect groups)
			if !empty(combgroup)
				replace swcnum with xnormal.swcnum in (xoutship) for combgroup=xnormal.combgroup and bol_no=xnormal.bol_no and &xoutshipfilter
			else
				replace swcnum with xnormal.swcnum in (xoutship) for consignee=xnormal.consignee and address=xnormal.address and address2=xnormal.address2 and bol_no=xnormal.bol_no and &xoutshipfilter
			endif
		endcase
	endscan

	if outwolog.staged
		replace swcdt with qdate(), ;
				printed with .f., ;
				completed with .f., ;
				labelqty with 0, ;
				cutqty with 0, ;
				leftqty with 0, ;
				stageqty with 0, ;
				stageleftq with 0 in outwolog

		if xfrom="STAGING"
			delete all in vswcdet
			delete all in vswcloc
			delete all in vswcstyle
		else
			select swcdet
			scan for outwologid=outwolog.outwologid
				delete in swcdet
			endscan
			select swcloc
			scan for outwologid=outwolog.outwologid
				delete in swcloc
			endscan
			select swcstyle
			scan for outwologid=outwolog.outwologid
				delete in swcstyle 
			endscan
		endif
		
		swcload(xfrom,outwolog.picknpack)
	endif
endif

if xfrom="OUTWO"
	go top in voutship
endif

return xredoswc