PARAMETER lcOffice,acct_num, xtesting

utilsetup("SENDINVENTORY")
set step on 

TRY

	IF VARTYPE(acct_num)="L"
		acct_num = "4610"
	ENDIF

*Set Step On
	CLOSE DATABASES ALL
	CLOSE TABLES ALL
	SET EngineBehavior 70
	SET EXCLUSIVE OFF
	SET SAFETY OFF
	SET DELETED ON
	SET ASSERTS OFF
	SET tablevalidate TO 0
	tfrom = "FMI Warehouse Operations <fmi-warehouse-ops@fmiint.com>"
	tattach = " "

*!*	if !xtesting and set("default")#"M"
*!*		_screen.Caption="SENDINVENTORY"
*!*		gsystemmodule="SENDINVENTORY"
*!*		guserid="???"
*!*		on error do fmerror with error(), program(), lineno(1), sys(16),, .t.
*!*	endif

	acct_num = VAL(acct_num)

	IF FILE("c:\tempfox\fo2.Dbf")
		DELETE FILE c:\tempfox\fo2.DBF
	ENDIF


	FOR lnOffice = 1 TO 2
*!*		SET NOTIFY ON
*!*		SET TALK ON
*!*		SET STATUS ON

		DO CASE
			CASE lnOffice = 1
				lcOffice = "N"
			CASE lnOffice = 2
				lcOffice = "C"
		ENDCASE

		lcWHPath = wf(lcOffice,acct_num)
		STORE .F. TO testing
		tcc = ""

		DO CASE

			CASE acct_num = 4610
				AcctList = "4610,4694"
				lcFilename = "NANJING_"+lcOffice+"_"+TTOC(DATETIME(),1)+".846"
				lcFilename2 = "NANJING_"+lcOffice+"_inproc_"+TTOC(DATETIME(),1)+".846"
				tsendto = 'pgaidis@fmiint.com'
		ENDCASE


		lcdate = DTOS(DATE())
		lctruncdate = RIGHT(lcdate,6)
		lctime = LEFT(TIME(DATE()),2)+SUBSTR(TIME(DATE()),4,2)
		lcfiledate = lcdate+lctime
		lcQtyRec = "1"
		lcOrig = "J"

* removed the "!SP" in the query to take into account the freight for SP that will be shipped

		SELECT * FROM &lcWHPath.outdet INTO CURSOR tempdet READWRITE WHERE .F.
		SELECT outdet
		SET ORDER TO outshipid
		SELECT * FROM &lcWHPath.outship WHERE INLIST(accountid,&AcctList) AND EMPTY(del_date) AND !notonwip AND qty !=0 AND ctnqty!=0 ORDER BY wo_num  INTO CURSOR inprocess READWRITE
		SELECT * FROM &lcWHPath.PROJECT WHERE INLIST(accountid,&AcctList) INTO CURSOR spwos READWRITE

		SET TALK OFF
		SET NOTIFY OFF
		SET STATUS OFF

		ASSERT .F.
		SELECT inprocess  && inprocess is a cursor of all outship records where they are not yet delivered and also excluding "notonwip"
* Delete For sp=.T.  && dont need any outship records that are related to special projects  / removed 1/21 we need to account for SP that are to be shipped
		GOTO TOP

		IF INLIST(acct_num,&AcctList)
			SELECT spwos
			SCAN
				SELECT inprocess
				WAIT WINDOW AT 10,10 "Looking for WO "+TRANSFORM(spwos.wo_num) NOWAIT
				LOCATE FOR inprocess.wo_num = spwos.wo_num
				IF FOUND()
					SELECT inprocess
					DELETE FOR inprocess.wo_num = spwos.wo_num
				ENDIF
			ENDSCAN
		ENDIF

		SELECT inprocess
		GOTO TOP
		lnRec= RECCOUNT()
		i=1
		SCAN
			WAIT WINDOW AT 10,10 "Working on "+STR(i)+"  of "+STR(lnRec) NOWAIT
			SELECT * FROM outdet WHERE outdet.outshipid= inprocess.outshipid INTO CURSOR ttindet
			SELECT ttindet
			SCAN
				SELECT ttindet
				SCATTER MEMVAR
				SELECT tempdet
				APPEND BLANK
				GATHER MEMVAR
			ENDSCAN
			i=i+1
		ENDSCAN

		SELECT tempdet

		SELECT *,0000 AS availqty, 000000000 AS PickedQty,00000000 AS outwo FROM tempdet GROUP BY STYLE,COLOR,ID,PACK INTO CURSOR items READWRITE

		SELECT items
		SCAN
			SELECT tempdet
			SUM totqty FOR tempdet.STYLE = items.STYLE AND tempdet.COLOR=items.COLOR AND tempdet.PACK=items.PACK TO lntotqty
			REPLACE items.PickedQty WITH lntotqty IN items

* added the totqty set to 0 ..... this was tested by PFG 2/23/2006 only effects items that are no longer in inventory but still
* on some WIP... or in outdet records
			REPLACE items.totqty WITH 0 IN items

		ENDSCAN

		SELECT * ,0000000 AS PickedQty,0000000 AS SPQty FROM &lcWHPath.inven WHERE INLIST(accountid,&AcctList) AND totqty >0 INTO CURSOR inventory READWRITE

*!*	 If lnOffice = 1
*!*	    SELECT * ,0000000 AS PickedQty,0000000 AS SPQty FROM m:\temp\njinven.dbf WHERE INLIST(accountid,&AcctList) AND totqty >0 INTO CURSOR inventory READWRITE
*!*	 EndIf
*!*	 
*!*	 If lnOffice = 2
*!*	   SELECT * ,0000000 AS PickedQty,0000000 AS SPQty FROM m:\temp\inven.dbf WHERE INLIST(accountid,&AcctList) AND totqty >0 INTO CURSOR inventory READWRITE
*!*	 endif

		SELECT inventory
		INDEX ON STYLE+COLOR+ID+PACK TAG upc
		SET ORDER TO upc

		DELETE FOR availqty =0 AND holdqty=0
		SELECT inventory
		GOTO TOP

		SELECT items
		SELECT * FROM items INTO ARRAY itemsarray

		SELECT inventory

		SELECT items
		SCAN
			SCATTER MEMVAR
			m.style = items.STYLE
			m.color = items.COLOR
			m.id    = items.ID
			m.pack  = items.PACK
			m.PickedQty = items.PickedQty
			SELECT inventory
			SEEK m.style+m.color+m.id+m.pack
			IF !FOUND()
				APPEND BLANK
				GATHER MEMVAR
			ELSE
				REPLACE PickedQty WITH m.PickedQty IN inventory
			ENDIF
		ENDSCAN

		IF INLIST(acct_num,&AcctList)  && now add in the special project inventory that is not yet delivered

			SELECT PROJECT &&Use F:\whc\whdata\Project In 0

			SELECT ;
				outdet.wo_num, ;
				STYLE, ;
				COLOR, ;
				ID, ;
				PACK, ;
				SUM(totqty) AS SPQty ;
				FROM outdet, PROJECT ;
				WHERE outdet.wo_num=PROJECT.wo_num ;
				AND INLIST(PROJECT.accountid,&AcctList) ;
				AND EMPTY(completeddt) ;
				GROUP BY 1,2,3,4,5 ;
				INTO CURSOR xrpt READWRITE

			SELECT xrpt
			SCAN
				IF !SEEK(xrpt.wo_num,"project","wo_num")
					SELECT xrpt
					DELETE
				ENDIF
			ENDSCAN

			SELECT xrpt
			SCAN
				SELECT xrpt
				SCATTER MEMVAR
				m.style = xrpt.STYLE
				m.color = xrpt.COLOR
				m.id    = xrpt.ID
				m.pack  = xrpt.PACK
				m.SPQty = xrpt.SPQty
				SELECT inventory
				SEEK m.style+m.color+m.id+m.pack
				IF !FOUND()
					APPEND BLANK
					GATHER MEMVAR
				ELSE
					REPLACE xrpt.SPQty WITH xrpt.SPQty + m.SPQty IN inventory
				ENDIF
			ENDSCAN
		ENDIF

		SELECT inventory

		DELETE FOR accountid = 4694 AND units
		GOTO TOP

		DELETE FOR accountid = 4610 AND !units

		IF lnOffice = 2
			SELECT Space(10) as nanjingacct, accountid,units,STYLE,COLOR,ID,PACK,totqty,holdqty,allocqty,availqty,PickedQty,00000000 AS SPQty,00000 AS totalqty, SPACE(8) AS office,SPACE(20) AS oldstyle,;
				SPACE(25) AS oldcolor,SPACE(10) AS oldsize,SPACE(25) AS DESC, SPACE(25) AS custsku, SPACE(10) AS invtype ;
				FROM inventory  INTO CURSOR finaloutput2 READWRITE

			SELECT finaloutput2
			REPLACE ALL office WITH "    C   "
			REPLACE ALL invtype WITH "PREPACK" FOR units = .F.
			REPLACE ALL invtype WITH "UNITPICK" FOR units = .T.
			REPLACE ALL totalqty WITH holdqty+availqty+PickedQty
		ENDIF

		IF lnOffice = 1
			SELECT Space(10) as nanjingacct, accountid,units,STYLE,COLOR,ID,PACK,totqty,holdqty,allocqty,availqty,PickedQty,00000000 AS SPQty,00000 AS totalqty,SPACE(8) AS office,SPACE(20) AS oldstyle,;
				SPACE(25) AS oldcolor,SPACE(10) AS oldsize,SPACE(25) AS DESC, SPACE(25) AS custsku, SPACE(10) AS invtype ;
				FROM inventory INTO CURSOR finaloutput1 READWRITE

			SELECT finaloutput1
			REPLACE ALL office WITH "   N    "
			REPLACE ALL invtype WITH "PREPACK" FOR units = .F.
			REPLACE ALL invtype WITH "UNITPICK" FOR units = .T.
			REPLACE ALL totalqty WITH holdqty+availqty+PickedQty
		ENDIF

		USE IN outship
		USE IN outdet
		USE IN PROJECT
		*USE IN inven

	NEXT lnOffice


	SELECT finaloutput2
	COPY TO c:\tempfox\fo2.DBF

	SELECT finaloutput1
	APPEND FROM c:\tempfox\fo2.DBF


    Select finaloutput1
    replace all nanjingacct with "1"
    goto top
    replace all nanjingacct with "10" for InList(Substr(style,1,2),"MD","GL","TO","BL")
    
    goto top
    
	USE F:\nanjing\nanjingstylemaster IN 0

	SELECT finaloutput1
	GOTO TOP
	SCAN
		SELECT nanjingstylemaster
		LOCATE FOR ALLTRIM(newstyle) = ALLTRIM(finaloutput1.STYLE)
		IF FOUND()
			SELECT finaloutput1
			REPLACE oldstyle WITH nanjingstylemaster.STYLE
			REPLACE oldcolor WITH nanjingstylemaster.COLOR
			REPLACE oldsize  WITH nanjingstylemaster.SIZE
			REPLACE DESC     WITH nanjingstylemaster.DESC
			REPLACE custsku  WITH nanjingstylemaster.vendorsku
		ENDIF

	ENDSCAN




*******************************************************************************************************************************************************

	DO CASE

		CASE acct_num = 4610
			xlsfilename = JUSTFNAME(lcFilename)
			xlsfilename= "f:\ftpusers\NANJING\846out\"+xlsfilename+".xls"
*Export to &xlsfilename fields style,color,id,pack,availqty,pickedqty type xls
			ASSERT .F. MESSAGE "At copy to XLS for pickpack"

			COPY FIELDS NanjingAcct,office,STYLE,COLOR,ID,PACK,availqty,PickedQty,holdqty,SPQty,totalqty,custsku,DESC,invtype ;
				TO "f:\ftpusers\NANJING\846out\NANJING.xls" TYPE XLS

			tmessage = "Attached is the inventory file for Nanjing goods.............."+CHR(13)+CHR(13)+;
				"This email is generated via an automated process"+CHR(13)+"For changes in content or distribution please contact: Paul Gaidis @ 732-750-9000x143 "
			tsubject= "Nanjing LA & NJ Inventory Sent: " +TTOC(DATETIME())

	ENDCASE

	tsendto = 'pgaidis@fmiint.com'
	tsendto = 'jason@nanjingusa.com,cary@nanjingusa.com'

	tattach ="f:\ftpusers\NANJING\846out\NANJING.xls"

	IF !testing
		DO FORM m:\dev\frm\dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
	ENDIF

CATCH TO oErr
	SET STEP ON
	ptError = .T.
	tsubject = "846 Inventory Update Error ("+TRANSFORM(oErr.ERRORNO)+")at "+TTOC(DATETIME())
	tattach  = ""
	tsendto  = "pgaidis@fmiint.com,joe.bianchi@tollgroup.com"
	tfrom    ="FMI WMS EDI System Operations <fmi-transload-ops@fmiint.com>"
	tcc =""
	tmessage = "Error processing an 846 Update "+CHR(13)
	tmessage =tmessage+ "Try/CATCH Exeception Message:"+CHR(13)+;
		[  Error: ] + STR(oErr.ERRORNO) +CHR(13)+;
		[  LineNo: ] + STR(oErr.LINENO) +CHR(13)+;
		[  Message: ] + oErr.MESSAGE +CHR(13)+;
		[  Procedure: ] + oErr.PROCEDURE +CHR(13)+;
		[  Details: ] + oErr.DETAILS +CHR(13)+;
		[  StackLevel: ] + STR(oErr.STACKLEVEL) +CHR(13)+;
		[  LineContents: ] + oErr.LINECONTENTS+CHR(13)+;
		[  UserValue: ] + oErr.USERVALUE+CHR(13)
	DO FORM dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"

ENDTRY

schedupdate()