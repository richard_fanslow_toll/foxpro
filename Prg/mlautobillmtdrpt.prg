*******************************************************************************************************
* Process ML Auto pm data to produce a month-to-date summary of handling charges which will be invoiced at the beginning of next month.
* Meant to be run every Monday night, through prior day (i.e. Sunday).
*
* EXE = F:\UTIL\MLAUTOBILLING\MTDREPORT.EXE
*******************************************************************************************************
*!*	DO DateRange WITH "KMART ALL"
*!*	*DO DateRange WITH "SEARS ALL"
*!*	DO DateRange WITH "FOOTLOCKER"

*!*	RETURN

utilsetup("MLAUTOBILLMTDRPT")

* disabled sears/kmart after Jan 2017

*DO MainMTD WITH "KMART ALL"

*DO MainMTD WITH "SEARS ALL"

*DO MainMTD WITH "DSG"

DO MainMTD WITH "RACK ROOM ALL"

DO MainMTD WITH "OFF BROADWAY ALL"

schedupdate()

RETURN


PROCEDURE MainMTD
	LPARAMETERS tcClient
	LOCAL loSearsProc, ldToday, ldFrom, ldTo, llGenerateInvoice, llUsePrepackForManualUnitCounting, lcClient, lnToday
	TRY
		lcClient = UPPER(ALLTRIM(tcClient))

		ldToday = DATE()
		*ldToday = {^2016-03-01}

		lnToday = DAY(ldToday)

		IF lnToday < 3 THEN
		*IF lnToday < 4 THEN
			* summarize entire prior month
			ldFrom = GOMONTH((ldToday - lnToday + 1),-1) && first day of prior month
			ldTo = ldToday - lnToday && last day of prior month
		ELSE
			* summarize current month to date
			ldFrom = ldToday - lnToday + 1 && 1st day of current month
			ldTo = ldToday - 1 && yesterday
		ENDIF

		* parameters to set in class
		llGenerateInvoice = .F.
		llUsePrepackForManualUnitCounting = .T.

		* load main processing class
		SET PROCEDURE TO M:\DEV\PRG\SEARSCLASS
		loSearsProc = CREATEOBJECT('SEARS')
		loSearsProc.SetSummaryOnly() && this forces a summary Excel sheet to be generated, not the pdf billing
		loSearsProc.SetUsePrepackForManualUnitCountingMode( llUsePrepackForManualUnitCounting )
*!*			loSearsProc.SetTestMode()

		IF loSearsProc.SetClient(lcClient) THEN
			loSearsProc.MakeBillForWeek(ldFrom, ldTo, llGenerateInvoice)
		ENDIF

	CATCH
	ENDTRY
	CLOSE DATABASES ALL

	RETURN
ENDPROC


PROCEDURE DateRange
	LPARAMETERS tcClient
	LOCAL loSearsProc, ldToday, ldFrom, ldTo, llGenerateInvoice, llUsePrepackForManualUnitCounting, lcClient, lnToday
	TRY
		lcClient = UPPER(ALLTRIM(tcClient))

		ldToday = DATE()

		lnToday = DAY(ldToday)

		* define date range
		ldFrom = {^2013-05-01}
		ldTo = {^2013-05-31}
		
		* parameters to set in class
		llGenerateInvoice = .F.
		llUsePrepackForManualUnitCounting = .T.

		* load main processing class
		SET PROCEDURE TO M:\DEV\PRG\SEARSCLASS
		loSearsProc = CREATEOBJECT('SEARS')
		loSearsProc.SetSummaryOnly() && this forces a summary Excel sheet to be generated, not the pdf billing
		loSearsProc.SetUsePrepackForManualUnitCountingMode( llUsePrepackForManualUnitCounting )

		IF loSearsProc.SetClient(lcClient) THEN
			loSearsProc.MakeBillForWeek(ldFrom, ldTo, llGenerateInvoice)
		ENDIF

	CATCH
	ENDTRY
	CLOSE DATABASES ALL

	RETURN
ENDPROC