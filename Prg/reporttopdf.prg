*****************************************************************************
* ReportToPDF.prg
*
* Call this when you want to a FOX Report to go to a PDF file
*
* Step#1 - the name of the report to run                 -  like -> STORE mfstr+ "BEALLS2" TO lcReportName
* Step#2 - where to place the file without the extension -  like -> STORE "C:\TEMPFOX\MFST293555" TO lcOutputFileName
* Step#3 = where ReportOK is a variable created in the calling program
*
* DO ReportToPDF WITH lcReportName,lcOutputFileName,ReportOK
*
*****************************************************************************
Parameter lcReportName,lcOutputFileName,ReportOK

STORE lcOutputFileName+[.ps]  TO lcPostScriptFileName
STORE lcOutputFileName+[.pdf] TO lcOutputFileName

IF !FILE(lcReportName+[.frx])
	=MESSAGEBOX("Report file not found..("+lcReportName+[.frx]+")...Call MIS",48,"PDF Emailer Application")
	STORE .F. TO ReportOK
	return ReportOK
ENDIF

*****************************************************************************
* now clear out the printer information from the tag fields in the first record
*****************************************************************************
lc_alias = ALIAS()

*!*	SELECT 0
*!*	*IF UPPER(lcReportName) <> "C:\TEMPFOX\JUNK"
*!*	USE lcReportName+[.frx] ALIAS TEMP_REPORT
*!*	REPLACE TAG WITH ""
*!*	REPLACE TAG2 WITH ""
*!*	USE IN TEMP_REPORT
*!*	*endif

SELECT &lc_alias

*****************************************************************************
* If the AMYUNI .DLL has been registered, use it
* Otherwise, use Acrobat
*****************************************************************************
OldAmyuni = .F.
NewAmyUni = .F.
*Wait window at 10,10 "We will now try to determine which Amyuni version.............." timeout 1

h_error=on("error")
on error xerror=.t.
xerror=.f.

*Wait window at 10,10 "Is it the new Amyuni version..............?" timeout 1
zzpdf=createobject("CDINTFEX.CDINTFEX")
If xerror = .F.
  on error &h_error
  NewAmyuni = .T.
endif

If NewAmyuni = .F.
  xerror=.f.
*  Wait window at 10,10 "Is it the old Amyuni version..............?" timeout 1
  zzPdf=CreateObject("CDINTF.CDINTF")
  on error &h_error
  If xerror = .f.
    OldAmyuni = .T.
  endif
EndIf

lcWhichOS = OS()
DO case 
  Case OldAmyuni = .T.
		zzPdf.DriverInit("Amyuni PDF")
		zzPdf.FileNameOptions = 1 + 2
		zzPdf.DefaultFileName = lcOutputFileName

		set printer to name "Amyuni PDF"

		REPORT FORM (lcReportName) NOEJECT NOCONSOLE TO PRINTER
		Set Printer to default
		
		** these lines added for the Amyuni printer on terminal server 2000
	    STORE "" TO lcRptFile
	    DO justfname WITH lcOutputFileName,lcRptFile
	    lcTotalFilename = "E:\DOCUMENTS AND SETTINGS\ADMINISTRATOR\TEMPFOX\"+lcRptFile
	*    =MESSAGEBOX("just press OK..."+lcTotalFilename+"  "+lcOutputFileName,48,"PDF Emailer Application")
		
	    if file(lcTotalFilename)
	       copy file "E:\DOCUMENTS AND SETTINGS\ADMINISTRATOR\TEMPFOX\"+lcRptFile to (lcOutputFileName)
	    endif
	    
	    lcTotalFilename = "E:\DOCUMENTS AND SETTINGS\ADMINISTRATOR.NJI\TEMPFOX\"+lcRptFile
	    if file(lcTotalFilename)
	       copy file "E:\DOCUMENTS AND SETTINGS\ADMINISTRATOR.NJI\TEMPFOX\"+lcRptFile to (lcOutputFileName)
	    endif

  Case NewAmyuni
*	xspot="1"
*	zzpdf=createobject("CDINTFEX.CDINTFEX")
	xspot="2"
	zzpdf.driverinit("Amyuni PDF")
	xspot="3"
	zzpdf.defaultfilename = lcoutputfilename
	xspot="4"
	zzPdf.EnablePrinter("FMI International, LLC","07EFCDAB01000100623D96E70BCA3B68B46DA2DCA944A0EDD64F5E74834A4E6FB79B506FF685751CE497E011B7D2357C6F97D3E9C1D001A21BDA22D827CDE4EBAA72C918EE30FD7047742684C89A3D8E87FF835A531360B988A89C17C7B8B2A56689F7E7E02856")
	xspot="5"
	zzpdf.filenameoptions = 1 + 2
	xspot="6"
	set printer to name "Amyuni PDF"
	xspot="7"
	report form (lcReportName) noeject noconsole to printer
	xspot="8"
	set printer to name (set("printer",2))
	xspot="9"
	
	if !file(lcOutputFileName)
		xspot="10"
		if file("d:\documents and settings\administrator.nji\"+strtran(upper(xoutputfilename),"C:\",""))
			xspot="11"
			copy file "d:\documents and settings\administrator.nji\"+strtran(upper(lcOutputFilename),"C:\","") to (lcOutputFilename)
		endif
	endif
  
 
  Case INLIST(lcWhichOS,"Windows NT 4.00","Windows NT 5.00","Windows 5.00","Windows 5.01")
		SET PRINTER TO NAME "ACROBAT PDFWriter"
		REPORT FORM (lcReportName) TO FILE (lcPostScriptFileName) NOCONSOLE
		Set Printer to default

		*-- PDFWriter creates two files .ps and .ps.pdf
		IF !FILE(lcPostScriptFileName)
			=MESSAGEBOX("PostScript File not created by the PDFWriter Driver...Call MIS",48,"PDF Emailer Application")
			STORE .F. TO ReportOK
			return ReportOK
		ENDIF
		ERASE (lcPostScriptFileName)
		lcPostScriptFileName = lcPostScriptFileName+[.pdf]

		*-- See that we have a valid file
		lbFileCreated = FILE(lcPostScriptFileName)

		IF lbFileCreated
			*-- Move the temp file to the actual file name by renaming
			IF FILE(lcOutputFileName)
				ERASE (lcOutputFileName)
			ENDIF

			RENAME (lcPostScriptFileName) TO (lcOutputFileName)
			ERASE (lcPostScriptFileName) && Yves
		ELSE
			=MESSAGEBOX("PDF File not created properly....No Email will be sent.....Call MIS",48,"PDF Emailer Application")
			STORE .F. TO ReportOK
			return ReportOK
		ENDIF

		IF ATC("PDF.PDF",UPPER(lcOutputFileName)) !=0
			=MESSAGEBOX("Output file "+lcOutputFileName+" has duplicate PDF.PDF in the name.....Call MIS",48,"PDF Emailer Application")
			STORE .F. TO ReportOK
			return ReportOK
		ENDIF

  *****************************************************************************
  Otherwise   && -- its Windows95 and it works in different manner
		IF FILE("C:\TEMPFOX\TEMP.PDF")
			DELETE FILE "C:\TEMPFOX\TEMP.PDF"
		ENDIF

		SET PRINTER TO NAME "PDF"
		REPORT FORM (lcReportName) TO PRINTER  NOCONSOLE
		Set Printer to default

		IF !FILE("C:\TEMPFOX\TEMP.PDF")
			=MESSAGEBOX("PDF File not created properly....No Email will be sent.....Call MIS",48,"PDF Emailer Application")
			STORE .F. TO ReportOK
			return ReportOK
		ELSE
			IF FILE(lcOutputFileName)
				ERASE (lcOutputFileName)
			ENDIF
			COPY FILE "C:\TEMPFOX\TEMP.PDF" TO (lcOutputFileName)
			DELETE FILE "C:\TEMPFOX\TEMP.PDF"
		ENDIF

		IF ATC("PDF.PDF",UPPER(lcOutputFileName)) !=0
			=MESSAGEBOX("Output file "+lcOutputFileName+" has duplicate PDF.PDF in the name.....Call MIS",48,"PDF Emailer Application")
			STORE .F. TO ReportOK
			return ReportOK
		ENDIF
EndCase

STORE .T. TO ReportOK
RETURN ReportOK
*****************************************************************************
