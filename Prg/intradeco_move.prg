CLOSE DATA ALL

DO m:\dev\prg\_setvars WITH .T.
lTesting = .f.

IF lTesting
	SET STEP ON
ENDIF

CD F:\ftpusers\Intradeco\IN\
len1 = ADIR(ary1,"*.edi")
FOR i = 1 TO len1
	cFilename = ALLTRIM(ary1[i,1])
	DO m:\dev\prg\createx856a
	DO m:\dev\prg\loadedifile WITH cFilename,"*","NOTILDE","INTRADECO"

	STORE .F. TO l940,l943,l997

	LOCATE
	STORE .f. TO l940,l943,l997
	LOCATE FOR TRIM(x856.segment) = "GS"
	IF FOUND()
		DO CASE
			CASE TRIM(x856.f1) = "AR"
				l943 = .T.
			CASE TRIM(x856.f1) = "FA"
				l997 = .T.
			OTHERWISE
				l940 = .T.
		ENDCASE
		ELSE
		DELETE FILE [&cFilename]
		loop
	ENDIF

	IF l940 OR l943
		cGroup = 'NJ'
		LOCATE
		LOCATE FOR TRIM(x856.segment) = "N9" AND TRIM(x856.f1) = "LU"
		IF FOUND()
			cGroup = ICASE(TRIM(x856.f2)="N","NJ",TRIM(x856.f2)="L","CA",TRIM(x856.f2)="R","CA","FL")  && Dropped Carson
		ELSE
			cGroup = "FL"
		ENDIF
		
		LOCATE
		LOCATE FOR TRIM(x856.segment) = "N9" AND TRIM(x856.f1) = "AR"
		IF FOUND()
			IF l943
				IF x856.f2 = "6237"  OR cGroup = "NJ" && Ivory
					cFilenameout = "943IVI"+TTOC(DATETIME(),1)+".edi"
					cOutfolder = ("F:\0-Packinglist\Ivory-NJ\")
				ELSE  && Intradeco
					cFilenameout = "943IN"+LEFT(cGroup,1)+TTOC(DATETIME(),1)+".edi"
					cOutfolder = ("F:\0-Packinglist\Intradeco-"+cGroup+"\")
				ENDIF
				cOutfile = (cOutfolder+cFilenameout)
				COPY FILE [&cFilename] TO [&cOutfile]
			ELSE
				DO CASE
					CASE x856.f2 = "6237NJ" && Ivory
						cFilenameout = "940IVI"+TTOC(DATETIME(),1)+".edi"
						cOutfolder = ("F:\FTPUSERS\Ivory-NJ\")
					CASE x856.f2 = "5154CA"
						cFilenameout = "940INC"+TTOC(DATETIME(),1)+".edi"
						cOutfolder = ("F:\FTPUSERS\Intradeco-CA\")
					CASE x856.f2 = "5154CR"
						cFilenameout = "940INR"+TTOC(DATETIME(),1)+".edi"
						cOutfolder = ("F:\FTPUSERS\Intradeco-CR\")
					OTHERWISE
						cFilenameout = "940INF"+TTOC(DATETIME(),1)+".edi"
						cOutfolder = ("F:\FTPUSERS\Intradeco-FL\")
				ENDCASE
				cOutfile = (cOutfolder+"940in\"+cFilenameout)
			ENDIF
			COPY FILE [&cFilename] TO [&cOutfile]
			IF !lTesting
				DELETE FILE &cFilename
			ENDIF
			LOOP
		ENDIF
	ELSE
		SELECT x856
		IF FOUND()
			cGroup = "NJ"
			cFile997out = "997"+TTOC(DATETIME(),1)+".edi"
			cOutfolder = ICASE(cGroup = "NJ","F:\FTPUSERS\Ivory-NJ\",cGroup = "FL","F:\FTPUSERS\Intradeco-FL\",cGroup = "CR","F:\FTPUSERS\Intradeco-CA\","F:\FTPUSERS\Intradeco-CA\")
			fa997file = (cOutfolder+"997in\"+cFile997out)
			WAIT WINDOW "This is a 997 file...Archiving and returning" TIMEOUT 2
*			COPY FILE [&cfile997out] TO [&fa997file]
			COPY FILE [&cfilename] TO [&fa997file]
			IF !lTesting
				DELETE FILE &cFilename
			ENDIF
		ENDIF
	ENDIF
ENDFOR

CLOSE DATA ALL
RETURN
