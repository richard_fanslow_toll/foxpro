* Create list of "delete" Pay Code transactions for Salaried employees.
* To identify where self-service employees may be deleting Vac/Sick from old timecards to increase their balances.
* EXEs go in F:\UTIL\KRONOS\
LOCAL loKronosPayCodeAuditReport

loKronosPayCodeAuditReport = CREATEOBJECT('KronosPayCodeAuditReport')
loKronosPayCodeAuditReport.MAIN()
CLOSE DATABASES ALL
RETURN

#DEFINE LOGIT 1
#DEFINE WAITIT 2
#DEFINE NOWAITIT 4
#DEFINE SENDIT 8
#DEFINE CRLF CHR(13) + CHR(10)
#DEFINE LF CHR(10)
#DEFINE RETURN_DATA_MANDATORY .F.
#DEFINE RETURN_DATA_NOT_MANDATORY .T.
#DEFINE DAILY_OT_MULTIPLIER 1.5

DEFINE CLASS KronosPayCodeAuditReport AS CUSTOM

	cProcessName = 'KronosPayCodeAuditReport'

	lTestMode = .T.  && .F. FOR NORMAL OPERATION, .T. TO SUPPRESS ALL BUT DEVELOPER EMAILS AND CHANGE INPUT TABLE, ETC.

	lAutoYield = .T.

	cStartTime = TTOC(DATETIME())
	
	* connection properties
	nSQLHandle = 0

	* file properties
	nFileHandle = 0

	* date properties
	dtNow = DATETIME()

	dToday = DATE()
	* to force different pay weeks
	*dToday = {^2009-05-26}

	cToday = DTOC(DATE())
	
	* wait window properties
	nWaitWindowTimeout = 2
	lWaitWindowIsOn = .T.

	* logfile properties
	lLoggingIsOn = .T.
	cLogFile = 'F:\UTIL\KRONOS\Logfiles\KronosPayCodeAuditReport_log.txt'

	* INTERNAL email properties
	lSendInternalEmailIsOn = .T.
	cFrom = 'FMI Corporate <fmicorporate@fmiint.com>'
	cSendTo = 'Mark Bennett <mbennett@fmiint.com>'
	cCC = ''
	cSubject = ''
	cAttach = ''
	cBodyText = ''

	cCOMPUTERNAME = ''
	cUSERNAME = ''

	FUNCTION INIT
		IF NOT DODEFAULT()
			RETURN .F.
		ENDIF
		WITH THIS
			CLEAR
			*SET RESOURCE OFF
			CLOSE DATA
			SET CENTURY ON
			SET DATE AMERICAN
			SET HOURS TO 24
			SET ANSI ON
			SET TALK OFF
			SET DELETED ON
			SET CONSOLE OFF
			SET EXCLUSIVE OFF
			SET SAFETY OFF
			SET EXACT OFF
			SET STATUS BAR ON
			SET SYSMENU OFF
			SET ENGINEBEHAVIOR 70
			_VFP.AUTOYIELD = .lAutoYield
			.cCOMPUTERNAME = GETENV("COMPUTERNAME")
			.cUSERNAME = GETENV("USERNAME")
			IF .lTestMode THEN
				.cSendTo = 'Mark Bennett <mbennett@fmiint.com>'
				.cLogFile = 'F:\UTIL\KRONOS\Logfiles\KronosPayCodeAuditReport_log_TESTMODE.txt'
			ENDIF
			.lLoggingIsOn = .lLoggingIsOn AND NOT EMPTY(.cLogFile)
			IF .lLoggingIsOn THEN
				SET ALTERNATE TO (.cLogFile) ADDITIVE
				SET ALTERNATE ON
			ENDIF
		ENDWITH
	ENDFUNC


	FUNCTION DESTROY
		WITH THIS
			IF .lLoggingIsOn  THEN
				SET ALTERNATE OFF
				SET ALTERNATE TO
			ENDIF
		ENDWITH
		DODEFAULT()
	ENDFUNC


	FUNCTION MAIN
		WITH THIS
			LOCAL lnNumberOfErrors, lcSQL, ldJan1st, lcAfterDateSQL
			LOCAL llRetval1, llRetval2

			TRY
				lnNumberOfErrors = 0
				ldToday = .dToday

				*.TrackProgress('==================================================================================================================', LOGIT+SENDIT)
				.TrackProgress('project = KRONOSTRREPORT', LOGIT+SENDIT)
				IF .lTestMode THEN
					.TrackProgress('KRONOS PayCode Audit process started....', LOGIT+SENDIT)
					.TrackProgress('COMPUTERNAME = ' + .cCOMPUTERNAME, LOGIT+SENDIT)
					.TrackProgress('USERNAME = ' + .cUSERNAME, LOGIT+SENDIT)
					.TrackProgress('                        TEST MODE                        ', LOGIT+SENDIT)
				ELSE
					.TrackProgress('KRONOS PayCode Audit process started....', LOGIT)
					.TrackProgress('COMPUTERNAME = ' + .cCOMPUTERNAME, LOGIT)
					.TrackProgress('USERNAME = ' + .cUSERNAME, LOGIT)
				ENDIF

				.cSubject = 'Kronos Pay Code Audit Report for: ' + TRANSFORM(ldToday)
				
				ldJan1st = {^2011-01-01}
				lcAfterDateSQL = "{ts '" + TRANSFORM(YEAR(ldJan1st)) + "-" + PADL(MONTH(ldJan1st),2,"0") + "-" + PADL(DAY(ldJan1st),2,"0") + " 00:00:00.0'}"

				IF USED('CURAUDIT') THEN
					USE IN CURAUDIT
				ENDIF
				IF USED('CURAUDIT2') THEN
					USE IN CURAUDIT2
				ENDIF
				
				* main SQL
				lcSQL = ;
					" SELECT DISTINCT " + ;
					" ENTEREDONDTM AS EDITDATE, " + ;
					" EVENTDATE AS CARDDATE, " + ;
					" 0000 AS DAYSDIFF, " + ;
					" PERSONFULLNAME AS EMPLOYEE, " + ; 
					" PERSONNUM AS FILE_NUM, " + ;
					" TRCACTIONTYPEID AS TYPEID, " + ; 
					" PAYCODENAME AS PAYCODE, " + ; 
					" CLIENTUSERNAME AS CUSER, " + ;
					" TIMEINSECONDS AS NSECS, " + ;
					" TIMESHEETITEMTYPE AS ITEMTYPE, " + ;
					" CLIENT " + ;
					" FROM VP_TIMECARDAUDIT " + ;
					" WHERE EVENTDATE >= " + lcAfterDateSQL + ;
					" AND PERSONNUM IN " + ;
					" (SELECT PERSONNUM FROM VP_EMPLOYEEV42 WHERE HOMELABORLEVELNM1 IN ('AXA','ZXU')) " + ;
					" AND ENTEREDONDTM > EVENTDATE " + ;
					" AND TIMESHEETITEMTYPE = 'PayCodeEdit' " + ;
					" AND TRCACTIONTYPEID = 5 " + ;
					" AND FUNCTIONCODE = 'E' " + ;
					" ORDER BY ENTEREDONDTM DESC "
					
				IF .lTestMode THEN
					.TrackProgress('', LOGIT+SENDIT)
					.TrackProgress('lcSQL =' + lcSQL, LOGIT+SENDIT)
				ENDIF

				.nSQLHandle = SQLCONNECT("WTK2","sa","kr0n1tes")

				IF .nSQLHandle > 0 THEN

						IF .ExecSQL(lcSQL, 'CURAUDIT', RETURN_DATA_MANDATORY) THEN						
							IF USED('CURAUDIT') AND NOT EOF('CURAUDIT') THEN
								SELECT CURAUDIT
								SCAN
									REPLACE CURAUDIT.DAYSDIFF WITH TTOD(EDITDATE) - TTOD(CARDDATE)
								ENDSCAN
								
								SELECT TTOD(EDITDATE) AS EDITDAY, TTOD(CARDDATE) AS CARDDAY, * FROM CURAUDIT INTO CURSOR CURAUDIT2 ORDER BY EDITDATE DESC
								
								SELECT CURAUDIT2
								*BROWSE
								COPY TO C:\TEMPFOX\PCAUDIT.XLS XL5 FOR (DAYSDIFF > 3)
								.cAttach = "C:\TEMPFOX\PCAUDIT.XLS"
							ENDIF													
						ENDIF			
				ELSE
					* connection error
					.TrackProgress('Unable to connect to WTK System.', LOGIT+SENDIT)
				ENDIF   &&  .nSQLHandle > 0
				CLOSE DATABASES ALL
				
			CATCH TO loError

				.TrackProgress('There was an error.',LOGIT+SENDIT)
				.TrackProgress('The Error # is: ' + TRANSFORM(loError.ERRORNO), LOGIT+SENDIT)
				.TrackProgress('The Error Message is: ' + TRANSFORM(loError.MESSAGE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred in Program: ' + TRANSFORM(loError.PROCEDURE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred at line #: ' + TRANSFORM(loError.LINENO), LOGIT+SENDIT)
				lnNumberOfErrors = lnNumberOfErrors + 1
				IF TYPE('oExcel') = 'O' AND NOT ISNULL(oExcel) THEN
					oExcel.QUIT()
				ENDIF
				CLOSE DATA
				CLOSE ALL

			ENDTRY

			CLOSE DATA
			WAIT CLEAR
			***************** INTERNAL email results ******************************
			.TrackProgress('About to send status email.',LOGIT)
			.TrackProgress('==================================================================================================================', SENDIT)
			.TrackProgress('The logfile is: ' + .cLogFile, SENDIT)
			.TrackProgress('KRONOS PayCode Audit process started: ' + .cStartTime, LOGIT+SENDIT)
			.TrackProgress('KRONOS PayCode Audit process finished: ' + TTOC(DATETIME()), LOGIT+SENDIT)

			IF .lSendInternalEmailIsOn THEN
				* try to trap error from not having dartmail dll's registered on user's pc...
				TRY
					DO FORM dartmail2 WITH .cSendTo,.cFrom,.cSubject,.cCC,.cAttach,.cBodyText,"A"
					.TrackProgress('Sent status email.',LOGIT)
				CATCH TO loError
					*=MESSAGEBOX('There was an error sending the status email. Please contact Mark Bennett.',0+64,"KRONOS Temp Time")
					.TrackProgress('There was an error sending the status email.',LOGIT)
					.TrackProgress('The Error # is: ' + TRANSFORM(loError.ERRORNO), LOGIT)
					.TrackProgress('The Error Message is: ' + TRANSFORM(loError.MESSAGE), LOGIT)
					.TrackProgress('The Error occurred in Program: ' + TRANSFORM(loError.PROCEDURE), LOGIT)
					.TrackProgress('The Error occurred at line #: ' + TRANSFORM(loError.LINENO), LOGIT)
					lnNumberOfErrors = lnNumberOfErrors + 1
					CLOSE DATA
				ENDTRY

			ELSE
				.TrackProgress('Did not send status email: THIS.lSendInternalEmailIsOn = FALSE.',LOGIT)
			ENDIF

		ENDWITH
		RETURN
	ENDFUNC && main
	

	FUNCTION GetSQLStartDateTimeString
		* returns a timestamp string for use in SQL queries
		LPARAMETERS tdDate
		LOCAL lcSetDate, lcSQLDateTimeString
		lcSetDate = SET('DATE')
		SET DATE YMD
		lcSQLDateTimeString   = "{ts '" + TRANSFORM(YEAR(tdDate)) + "-" + PADL(MONTH(tdDate),2,"0") + "-" + PADL(DAY(tdDate),2,"0") + " 00:00:00.0'}"
		SET DATE &lcSetDate.
		RETURN lcSQLDateTimeString
	ENDFUNC
	

	FUNCTION GetSQLEndDateTimeString
		* returns a timestamp string for use in SQL queries
		LPARAMETERS tdDate
		LOCAL lcSetDate, lcSQLDateTimeString
		lcSetDate = SET('DATE')
		SET DATE YMD
		lcSQLDateTimeString = "{ts '" + TRANSFORM(YEAR(tdDate)) + "-" + PADL(MONTH(tdDate),2,"0") + "-" + PADL(DAY(tdDate),2,"0") + " 23:59:59.9'}"
		SET DATE &lcSetDate.
		RETURN lcSQLDateTimeString
	ENDFUNC
	

	FUNCTION ExecSQL
		LPARAMETERS tcSQL, tcCursorName, tlNoDataReturnedIsOkay
		LOCAL llRetval, lnResult
		WITH THIS
			* close target cursor if it's open
			IF USED(tcCursorName)
				USE IN (tcCursorName)
			ENDIF
			WAIT WINDOW "Fetching remote data; please wait..." NOWAIT
			lnResult = SQLEXEC(.nSQLHandle, tcSQL, tcCursorName)
			llRetval = ( lnResult > 0 )
			IF llRetval THEN
				* see if any data came back
				IF NOT tlNoDataReturnedIsOkay THEN
					IF (NOT USED(tcCursorName)) OR EOF(tcCursorName) THEN
						llRetval = .F.
						.TrackProgress('No data was returned by this query: ' +CRLF + tcSQL, LOGIT+SENDIT)
					ENDIF
				ENDIF
			ELSE
				.TrackProgress("Result Code [" + TRANSFORM(lnResult) + "] returned by this query: " + tcSQL, LOGIT+SENDIT)
			ENDIF
			WAIT CLEAR
			RETURN llRetval
		ENDWITH
	ENDFUNC


	PROCEDURE TrackProgress
		* do any combination of Wait Window, writing to logfile, and adding to body of email,
		* based on nFlags parameter.
		LPARAMETERS tcExpression, tnFlags
		WITH THIS
			IF BITAND(tnFlags,LOGIT) = LOGIT THEN
				IF .lLoggingIsOn THEN
					?
					? .cProcessName + "  " + TTOC(DATETIME()) + ": " + tcExpression
				ENDIF
			ENDIF
			IF .lWaitWindowIsOn THEN
				IF BITAND(tnFlags,NOWAITIT) = NOWAITIT THEN
					WAIT WINDOW tcExpression NOWAIT
				ENDIF
				IF BITAND(tnFlags,WAITIT) = WAITIT THEN
					WAIT WINDOW tcExpression TIMEOUT .nWaitWindowTimeout
				ENDIF
			ENDIF
			IF BITAND(tnFlags,SENDIT) = SENDIT THEN
				IF .lSendInternalEmailIsOn THEN
					.cBodyText = .cBodyText + tcExpression + CRLF + CRLF
				ENDIF
			ENDIF
		ENDWITH
	ENDPROC  &&  TrackProgress
	

ENDDEFINE
