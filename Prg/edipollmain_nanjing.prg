CLOSE DATABASES ALL
CLEAR ALL
RELEASE ALL
WITH _SCREEN
	.AUTOCENTER = .T.
	.WINDOWSTATE = 0
	.BORDERSTYLE = 1
	.WIDTH = 260
	.HEIGHT = 210
	.TOP = 290
	.LEFT = 660
	.CLOSABLE = .F.
	.MAXBUTTON = .F.
	.MINBUTTON = .F.
	.SCROLLBARS = 0
	.CAPTION = "NANJING POLLER - 945 ONLY"
EndWith
SET REPROCESS TO 500

Public tFrom,lDoManUC,lcCloseOutput
tfrom = "TGF Warehouse Operations <tgf-warehouse-ops@fmiint.com>"
DO m:\dev\prg\lookups

TRY
	PUBLIC lSkipError,LNEWPTSTRUC
	lNewPTstruc = .t.
	lSkipError = .f.
	DO m:\dev\prg\_setvars
	SELECT 0
	USE F:\3pl\DATA\edipollertrip SHARED ALIAS enan
	REPLACE enan.nanjing WITH .f.
	USE IN enan

	SELECT 0
	USE F:\3pl\DATA\last945time SHARED ALIAS nlast
	REPLACE checkflag WITH .T. FOR poller = "NANJING"
	REPLACE tries WITH 0 FOR poller = "NANJING"
	USE IN nlast

	SET STATUS BAR OFF
	SET SYSMENU OFF
	ON ESCAPE CANCEL
	DO FORM m:\dev\frm\edi_jobhandler_nanjing
CATCH TO oErr
	IF !lSkipError
	ASSERT .F. MESSAGE "AT CATCH SECTION"
	SELECT 0
	USE F:\3pl\DATA\mailmaster ALIAS mm
	LOCATE
	LOCATE FOR mm.office = 'X' AND mm.accountid = 9999
	tsendtoerr = IIF(mm.use_alt,sendtoalt,mm.sendto)
	tccerr = IIF(mm.use_alt,mm.ccalt,mm.cc)
	USE IN mm
	tsubject = "Nanjing EDI Poller Error ("+TRANSFORM(oErr.ERRORNO)+") at "+TTOC(DATETIME())
	tattach  = ""
	tsendto  = tsendtoerr
	tcc = tccerr
	tmessage = "Nanjing 945 Poller Major Error..... Please fix me........!"
	lcSourceMachine = SYS(0)

	tmessage =tmessage+ "Try/CATCH Exeception Message:"+CHR(13)+;
		[  Error: ] + STR(oErr.ERRORNO) +CHR(13)+;
		[  LineNo: ] + STR(oErr.LINENO) +CHR(13)+;
		[  Message: ] + oErr.MESSAGE +CHR(13)+;
		[  Procedure: ] + oErr.PROCEDURE +CHR(13)+;
		[  Details: ] + oErr.DETAILS +CHR(13)+;
		[  StackLevel: ] + STR(oErr.STACKLEVEL) +CHR(13)+;
		[  LineContents: ] + oErr.LINECONTENTS+CHR(13)+;
		[  UserValue: ] + oErr.USERVALUE+CHR(13)+;
		[  Computer:  ] +lcSourceMachine+CHR(13)
	DO FORM m:\dev\FRM\dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
	endif
	SELECT 0
	USE F:\3pl\DATA\edipollertrip SHARED ALIAS enan
	REPLACE enan.nanjing WITH .T.
FINALLY
	ON error
	CLOSE DATABASES ALL
	set status bar ON
ENDTRY
