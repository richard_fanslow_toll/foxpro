* import S3 styles, from Tom Delaney 8/21/2017
* programmed by MB
* build exe as F:\UTIL\S3\S3STYLEIMPORT.EXE
*
* xsqlexec("select * from account where inactive=0","account",,"qq")
* 
* 1/16/2018 MB: had to make several changes both to the spreadsheet and the code to support new spreadsheet format they sent on 1/16/2018 - it differed from previous spreadsheet.
* In particular they mixed numeric and char values in the ID/size columnn.
*
* revised for different columns and fewer header lines 1/23/2018 MB
*
* modified 2/20/2018 MB for file from Paul Gaidis.
*
* modified to auto-load with new format from Yaakov @ fxny 2/23/2018 MB

LOCAL lTestMode
lTestMode = .F.

guserid = "STYLEIMP"

IF NOT lTestMode THEN
	utilsetup("S3STYLEIMPORT")
ENDIF

DECLARE INTEGER CreateMutex IN Win32API ;
	STRING @lpMutexAttributes, ;
	STRING bInitialOwner, ;
	STRING @lpName

DECLARE INTEGER GetLastError IN Win32API

* if running in EXE check to see if already running
IF _VFP.STARTMODE > 0 THEN
	lcProcessName = "S3STYLEIMPORT"
	CreateMutex(0,CHR(1),@lcProcessName)
	lnError = GetLastError()
	IF lnError = 183
		WAIT WINDOW TIMEOUT 5 "This program is already running..." 
		IF NOT lTestMode THEN
			schedupdate()
		ENDIF
		RETURN .F.
	ENDIF
ENDIF

loS3STYLEIMPORT = CREATEOBJECT('S3STYLEIMPORT') 
loS3STYLEIMPORT.MAIN( lTestMode )

IF NOT lTestMode THEN
	schedupdate()
ENDIF

CLOSE DATABASES ALL
RETURN

#DEFINE LOGIT 1
#DEFINE WAITIT 2
#DEFINE NOWAITIT 4
#DEFINE SENDIT 8
#DEFINE CRLF CHR(13) + CHR(10)
#DEFINE RETURN_DATA_MANDATORY .F.
#DEFINE RETURN_DATA_NOT_MANDATORY .T.
#DEFINE CSVFORMAT 6
#DEFINE WRITE_ONLY_UNBUFFERED 11
#DEFINE COMMA ","

DEFINE CLASS S3STYLEIMPORT AS CUSTOM

	cProcessName = 'S3STYLEIMPORT'

	lTestMode = .F.  && .F. FOR NORMAL OPERATION, .T. TO SUPPRESS ALL BUT DEVELOPER EMAILS AND CHANGE INPUT TABLE, ETC.

	dToday = DATE()

	lAutoYield = .T.

	cStartTime = TTOC(DATETIME())

	* connection properties
	nSQLHandle = 0

	* folder properties

	cInputFolder1 = ''
	cArchiveFolder = ''
	cOrigFolder = ''	

*!*		* data properties
*!*		cUPCMastTable = ''
	
	nAccountID = 6803  
	cAddProc = "S3STYLE"
	tADDDT = DATETIME()
	lPNP = .F.
	cDESCRIP = ''
	cSTYLE = ''
	nUPCMASTID = 0
	cUPC = ''
	nWEIGHT = 0
	nCUBE = 0
	nUIC = 0
	cINFO = ''
	cColor = ''
	cID = ''
	
	* processing properties
	lDeleteFoundRecs = .F.  && if .T. then when we find a key match we will delete existing rec and insert the new one. If .F. we will simply not insert when we find a new match.
	nRow = 0

	* Excel properties
	oExcel = NULL

	* wait window properties
	nWaitWindowTimeout = 2
	lWaitWindowIsOn = .T.

	* logfile properties
	lLoggingIsOn = .T.
	cLogFile = 'F:\UTIL\S3\STYLES\LOGFILES\S3STYLEIMPORT_log.txt'

	* INTERNAL email properties
	lSendInternalEmailIsOn = .T.
	cFrom = 'TGF Corporate <fmicorporate@fmiint.com>'
	cSendTo = 'mbennett@fmiint.com'
	cCC = ''
	cSubject = 'S3 Style Import for ' + TRANSFORM(DATETIME())
	cAttach = ''
	cBodyText = ''
	cMGRSendTo = ''
	cMGRFrom = ''
	cMGRSubject = ''
	cMGRCC = ''
	cMGRAttach = ''
	cMGRBodyText = ''

	cCOMPUTERNAME = ''
	cUSERNAME = ''


	FUNCTION INIT
		IF NOT DODEFAULT()
			RETURN .F.
		ENDIF
		WITH THIS
			*SET RESOURCE OFF
			CLOSE DATA
			SET CENTURY ON
			*SET DATE AMERICAN
			*SET HOURS TO 24
			SET DECIMALS TO 2
			SET ANSI ON
			SET TALK OFF
			SET DELETED ON
			SET CONSOLE OFF
			SET EXCLUSIVE OFF
			SET SAFETY OFF
			SET EXACT OFF
			SET STATUS BAR ON
			SET SYSMENU OFF
			SET ENGINEBEHAVIOR 70
			SET MULTILOCKS ON
			_VFP.AUTOYIELD = .lAutoYield
			.cCOMPUTERNAME = GETENV("COMPUTERNAME")
			.cUSERNAME = GETENV("USERNAME")
			.lLoggingIsOn = .lLoggingIsOn AND NOT EMPTY(.cLogFile)
		ENDWITH
	ENDFUNC


	FUNCTION DESTROY
		WITH THIS
			IF .lLoggingIsOn  THEN
				SET ALTERNATE OFF
				SET ALTERNATE TO
			ENDIF
		ENDWITH
		IF TYPE('.oExcel') = 'O' AND NOT ISNULL(.oExcel) THEN
			.oExcel.QUIT()
		ENDIF
		DODEFAULT()
	ENDFUNC


	FUNCTION MAIN
		LPARAMETERS tlTestMode
		WITH THIS
			LOCAL lnNumberOfErrors, lnAccountID, lnNumFiles1, lnNumFiles2, laFiles[1,5]

			TRY

				lnNumberOfErrors = 0
				
				.cStartTime = TTOC(DATETIME())

				.lTestMode = tlTestMode

				IF .lTestMode THEN
					.cLogFile = 'F:\UTIL\S3\STYLES\LOGFILES\S3STYLEIMPORT_log_TESTMODE.txt'
					.cSendTo = 'mbennett@fmiint.com'
				ENDIF
				
				IF .lLoggingIsOn THEN
					SET ALTERNATE TO (.cLogFile) ADDITIVE
					SET ALTERNATE ON
				ENDIF
				
				IF .lTestMode THEN
					.cInputFolder1 = 'F:\UTIL\S3\STYLES\TESTINPUT\'
					.cArchiveFolder = 'F:\UTIL\S3\STYLES\TESTINPUT\ARCHIVED\'
					.cOrigFolder = 'F:\UTIL\S3\STYLES\TESTINPUT\ORIG\'
				ELSE
					.cInputFolder1 = 'F:\FTPUSERS\S3\STYLEMASTER\'
					.cArchiveFolder = 'F:\FTPUSERS\S3\STYLEMASTER\ARCHIVED\'
					.cOrigFolder = 'F:\FTPUSERS\S3\STYLEMASTER\ORIG\'	
				ENDIF				

				.TrackProgress('S3 Style Import process started....', LOGIT+SENDIT)
				.TrackProgress('COMPUTERNAME = ' + .cCOMPUTERNAME, LOGIT+SENDIT)
				.TrackProgress('USERNAME = ' + .cUSERNAME, LOGIT+SENDIT)
				.TrackProgress('PROJECT = S3STYLEIMPORT', LOGIT+SENDIT)
				IF .lTestMode THEN
					.TrackProgress('====>TEST MODE', LOGIT+SENDIT)
				ENDIF
				.TrackProgress('.cInputFolder1 = ' + .cInputFolder1, LOGIT+SENDIT)
				.TrackProgress('.cArchiveFolder = ' + .cArchiveFolder, LOGIT+SENDIT)
				.TrackProgress('.cOrigFolder = ' + .cOrigFolder, LOGIT+SENDIT)
				
				* OPEN Excel
				.oExcel = CREATEOBJECT("excel.application")
				.oExcel.displayalerts = .F.
				.oExcel.VISIBLE = .F.
				
				* for sql
				useca("upcmast","wh",,,"select * from upcmast where accountid = " + ALLTRIM(STR(.nAccountID,4,0)) + " order by upc",,"upcmastsql")
				SELECT "upcmastsql"
				GO bottom
			
				* process first source folder
				.ProcessFolder( .cInputFolder1, .cArchiveFolder, .cOrigFolder )
				
				*.UpdateMeasures( .cInputFolder1, .cArchiveFolder, .cOrigFolder )

				CLOSE DATABASES ALL
				
				.oExcel.Quit()
				
				* should no longer be necessary since UPC is now part of the unique key when adding or updating 2/23/2018 MB
				*!*	IF NOT .lTestMode THEN
				*!*		.FixDupeUPCs()
				*!*	ENDIF

				.TrackProgress('S3 Style Import process ended normally.', LOGIT+SENDIT)

			CATCH TO loError

				.TrackProgress('There was an error.',LOGIT+SENDIT)
				.TrackProgress('The Error # is: ' + TRANSFORM(loError.ERRORNO), LOGIT+SENDIT)
				.TrackProgress('The Error Message is: ' + TRANSFORM(loError.MESSAGE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred in Program: ' + TRANSFORM(loError.PROCEDURE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred at line #: ' + TRANSFORM(loError.LINENO), LOGIT+SENDIT)
				.TrackProgress('SPREADSHEET ROW = ' + TRANSFORM(.nRow), LOGIT+SENDIT)
				lnNumberOfErrors = lnNumberOfErrors + 1
				.lSendInternalEmailIsOn = .T.
				IF TYPE('.oExcel') = 'O' AND NOT ISNULL(.oExcel) THEN
					.oExcel.QUIT()
				ENDIF

			ENDTRY

			WAIT CLEAR
			***************** INTERNAL email results ******************************
			.TrackProgress('About to send status email.',LOGIT+SENDIT)
			.TrackProgress('The logfile is: ' + .cLogFile, LOGIT+SENDIT)
			.TrackProgress('S3 Style Import process started: ' + .cStartTime, LOGIT+SENDIT)
			.TrackProgress('S3 Style Import process finished: ' + TTOC(DATETIME()), LOGIT+SENDIT)

			IF .lSendInternalEmailIsOn THEN
				DO FORM dartmail2 WITH .cSendTo,.cFrom,.cSubject,.cCC,.cAttach,.cBodyText,"A"
				.TrackProgress('Sent status email.',LOGIT)
			ELSE
				.TrackProgress('Did not send status email: THIS.lSendInternalEmailIsOn = FALSE.',LOGIT+SENDIT)
			ENDIF

		ENDWITH
		RETURN
	ENDFUNC && main


	PROCEDURE ProcessFolder
		LPARAMETERS tcInputFolder, tcArchivedFolder, tcOrigFolder
		WITH THIS
			LOCAL laFiles[1,5], laFilesSorted[1,6], lnNumFiles, lnCurrentFile, lcSourceFile, lcArchivedFile, lcOrigFile
			LOCAL lnHandle, lcStr, lnCtr, oWorkbook, oWorksheet, lnMaxRow, lnRow, lcRow, lnDupeDelCtr, lnDupeCtr
			LOCAL llArchived, lnColumnF, lcColumnD, lnLength, lnWidth, lnHeight
			LOCAL lcGTINString, j, lnNumLines, lcInfoLine, lcType

			
			PRIVATE	m.UPCMASTID, m.ACCOUNTID, m.STYLE, m.PNP, m.DESCRIP, m.UPC, m.WEIGHT, m.CUBE, m.UIC, m.INFO, m.COLOR, m.ID
			STORE 0 TO m.UPCMASTID, m.WEIGHT, m.CUBE, m.UIC
			STORE '' TO m.STYLE, m.DESCRIP, m.UPC, m.INFO, m.COLOR, m.ID
			STORE .T. TO m.PNP

			m.ACCOUNTID = .nAccountID
			

			************************************************************************
			*** for sql
			PRIVATE m.ADDPROC, m.UPDPROC, m.ADDBY, m.ADDDT, m.UPDATEBY, m.UPDATEDT
			STORE .cAddProc TO m.ADDPROC, m.UPDPROC, m.ADDBY, m.UPDATEBY
			STORE .tADDDT TO m.ADDDT, m.UPDATEDT
			************************************************************************

			* 3/5/2018 MB
			*lnNumFiles = ADIR(laFiles,(tcInputFolder + "*.xls")) 
			lnNumFiles = ADIR(laFiles,(tcInputFolder + "*.xlsx")) 

			IF lnNumFiles > 0 THEN

				* sort file list by date/time
				.SortArrayByDateTime(@laFiles, @laFilesSorted)

				* send an email only if files were found
				.lSendInternalEmailIsOn = .T.

				* for auto-load just load one file per process run - if there are multiple files the folder poller will call the process again 2/23/2018 MB
				*FOR lnCurrentFile = 1 TO lnNumFiles
				FOR lnCurrentFile = 1 TO 1

					lcSourceFile = tcInputFolder + laFilesSorted[lnCurrentFile,1]
					lcArchivedFile = tcArchivedFolder + laFilesSorted[lnCurrentFile,1]
					lcOrigFile = tcOrigFolder + laFilesSorted[lnCurrentFile,1]

					.TrackProgress('lcSourceFile = ' + lcSourceFile,LOGIT+SENDIT)
					.TrackProgress('lcArchivedFile = ' + lcArchivedFile,LOGIT+SENDIT)
					.TrackProgress('lcOrigFile = ' + lcOrigFile,LOGIT+SENDIT)					
					
					* move source file into ORIG folder so that, if an error causes the program to abort, the folderscan process does not keep reprocessing the bad source file in an infinite loop
					
					* if the file already exists in the orig folder, make sure it is not read-only.
					llArchived = FILE(lcOrigFile)
					IF llArchived THEN
						RUN ATTRIB -R &lcOrigFile.
						.TrackProgress('   Used ATTRIB -R for re-archiving : ' + lcOrigFile,LOGIT+SENDIT)
					ENDIF

					* archive the source file
					COPY FILE (lcSourceFile) TO (lcOrigFile)

					* always delete the source file
					*IF FILE(lcOrigFile) THEN
						DELETE FILE (lcSourceFile)
					*ENDIF
					
					
					* then process from the file once it is in the ORIG folder
					oWorkbook = .oExcel.workbooks.OPEN(lcOrigFile)
					oWorksheet = oWorkbook.Worksheets[1]  

					lnMaxRow = 19999
					
					lnCtr = 0
					lnDupeCtr = 0
					lnDupeDelCtr = 0
					
					*SET STEP ON 
					 					

					FOR lnRow = 2 TO lnMaxRow   && header decreased to 1 line 1/23/2018 MB
					
						.nRow = lnRow
					
						WAIT WINDOW NOWAIT 'Processing Spreadsheet Row: ' + TRANSFORM(lnRow)
						
						lcRow = ALLTRIM(STR(lnRow))
						
*!*							**************************************************************************************
*!*							M.STYLE = NVL(OWORKSHEET.RANGE("A"+LCROW).VALUE,'')
*!*							
*!*							M.DESCRIP = NVL(OWORKSHEET.RANGE("D"+LCROW).VALUE,'')
*!*																		
*!*							M.COLOR = NVL(OWORKSHEET.RANGE("B"+LCROW).VALUE,'')											
*!*							
*!*							* THEY ARE MIXING CHAR AND NUMERIC ID VALUES SO MUST TEST FOR TYPE 1/16/201 MB
*!*							M.ID = OWORKSHEET.RANGE("C"+LCROW).VALUE
*!*							IF TYPE('M.ID') == 'N' THEN 											
*!*								M.ID = ALLTRIM(STR(M.ID,4,1))
*!*							ELSE
*!*								M.ID = ALLTRIM(M.ID)	
*!*							ENDIF									
*!*							
*!*							*M.UPC = NVL(OWORKSHEET.RANGE("E"+LCROW).VALUE,0)
*!*							*M.UPC =INT(M.UPC)
*!*							*M.UPC = STR(M.UPC,12,0)
*!*							M.UPC = NVL(OWORKSHEET.RANGE("E"+LCROW).VALUE,'')
*!*							M.UPC = ALLTRIM(M.UPC)
*!*							* THEY HAVE SENT SOME UPCS PREFIXED BY CHR(160), WHICH ALLTRIM DOES NOT REMOVE. TRY TO REMOVE IT. 1/25/2018 MB
*!*							M.UPC = STRTRAN(M.UPC,CHR(160),'')
*!*							**************************************************************************************
						
						* lately the spreadsheets they send seem to alternate between 2 formats - either the one above or the one below. 
						* I've gotten both in the same day from Alma Navarro. 1/25/2018 MB
						
						* MB: below modfied for new 2/23/2018 format from Yaakov.
						**************************************************************************************
						m.UPC = NVL(oWorksheet.RANGE("A"+lcRow).VALUE,0)
						IF TYPE('m.UPC') == 'N' THEN 
							m.UPC = STR(m.UPC,20,0) && not working changed 3/5/2018 MB
							*m.UPC = STR(m.UPC,12,0)
						ENDIF									
						m.UPC = ALLTRIM(m.UPC)
						
						*      or
						*m.UPC = NVL(oWorksheet.RANGE("A"+lcRow).VALUE,'')
						*m.UPC = ALLTRIM(m.UPC)
						* they have sent some UPCs prefixed by chr(160), which alltrim does not remove. Try to remove it. 1/25/2018 MB
						*m.UPC = STRTRAN(m.UPC,CHR(160),'')
						
						m.STYLE = NVL(oWorksheet.RANGE("B"+lcRow).VALUE,'')
						* they are now mixing char and numeric STYLE values so must test for type 1/26/2018 MB
						IF TYPE('m.STYLE') == 'N' THEN 											
							m.STYLE = STR(m.STYLE,20,0)
						ENDIF									
						m.STYLE = ALLTRIM(m.STYLE)	
						
						* to support '#N/A' values they sometimes give 2/23/2018 MB
						m.DESCRIP = oWorksheet.RANGE("C"+lcRow).VALUE  &&& NOTE: getting errors on this line if Excel has '#N/A' as a value!
						lcType = TYPE('m.DESCRIP')
						IF NOT (lcType == 'C') THEN
							m.DESCRIP = SPACE(10)
						ENDIF
																	
						m.COLOR = NVL(oWorksheet.RANGE("D"+lcRow).VALUE,'')											
						
						* they are mixing char and numeric ID values so must test for type 1/16/201 MB
						m.ID = oWorksheet.RANGE("E"+lcRow).VALUE
						IF TYPE('m.ID') == 'N' THEN 											
							m.ID = ALLTRIM(STR(m.ID,4,1))
						ELSE
							m.ID = ALLTRIM(m.ID)	
						ENDIF									
						**************************************************************************************
						
						* remove the embedded dashes in the UPC field
						*m.UPC = STRTRAN(m.UPC,"-","")										
						
						IF ( ISNULL(m.STYLE) OR EMPTY(m.STYLE) ) THEN
							* we are past the last data row! exit the loop...
							.TrackProgress('====> Empty Style encountered at row: ' + lcRow,LOGIT+SENDIT)
							EXIT FOR
						ENDIF
						
						m.PNP = .T.
						
						m.STYLE = PADR(ALLTRIM(m.STYLE),20,' ')

						m.UPC = PADR(ALLTRIM(m.UPC),14,' ') && added 1/16/2018 MB because upc comparisons were failing

						*****************************************************************
						* for sql, save memvars gotten from Excel spreadsheet to properties for later recovery
						.lPNP = m.PNP
						.cDESCRIP = m.DESCRIP
						.cSTYLE = m.STYLE
						.cUPC = m.UPC
						*.nWEIGHT = m.WEIGHT
						*.nCUBE = m.CUBE
						*.nUIC = m.UIC
						*.cINFO = m.INFO
						.cColor = m.COLOR
						.cID = m.ID
						*****************************************************************
						
						SELECT upcmastsql

						LOCATE FOR (ACCOUNTID = m.ACCOUNTID) ;
							AND (UPC == m.UPC) 
							
						IF FOUND() THEN						
								
							REPLACE upcmastsql.PNP WITH m.PNP, upcmastsql.DESCRIP WITH m.DESCRIP, ;
								upcmastsql.STYLE WITH m.STYLE, upcmastsql.COLOR WITH m.COLOR, upcmastsql.ID WITH m.ID, ;
								upcmastsql.UPDPROC WITH m.UPDPROC, upcmastsql.UPDATEBY WITH m.UPDATEBY, upcmastsql.UPDATEDT WITH m.UPDATEDT IN upcmastsql
								
							lnDupeCtr = lnDupeCtr + 1
						ELSE
							SELECT upcmastsql
							SCATTER MEMVAR MEMO BLANK

							* restore memvars from properties
							STORE .cAddProc TO m.ADDPROC, m.UPDPROC, m.ADDBY, m.UPDATEBY 
							STORE .tADDDT TO m.ADDDT, m.UPDATEDT
							
							m.PNP = .lPNP
							m.DESCRIP = .cDESCRIP
							m.STYLE = .cSTYLE
							m.UPC = .cUPC
							m.ACCOUNTID = .nAccountID
							*m.WEIGHT = .nWEIGHT
							*m.CUBE = .nCUBE 
							*m.UIC = .nUIC
							*m.INFO = .cINFO 
							m.COLOR = .cColor
							m.ID = .cID
							
							IF .lTestMode THEN
								m.UPCMASTID = 0
							ELSE					
								m.upcmastid=sqlgenpk("upcmast","wh")
							ENDIF
							
							INSERT INTO upcmastsql FROM MEMVAR
							lnCtr = lnCtr + 1
						ENDIF
						*****************************************************************	

						
					ENDFOR
					
					oWorkbook.close()
					
					IF .lTestMode THEN
						SELECT upcmastsql
						BROWSE
						.TrackProgress('TEST MODE: did not TU(upcmastsql)',LOGIT+SENDIT)
					ELSE					
						* tableupate SQL
						IF tu("upcmastsql") THEN
							.TrackProgress('SUCCESS: TU(upcmastsql) returned .T.!',LOGIT+SENDIT)
						ELSE
							* ERROR on table update
							.TrackProgress('ERROR: TU(upcmastsql) returned .F.!',LOGIT+SENDIT)
							=AERROR(UPCERR)
							IF TYPE("UPCERR")#"U" THEN 
								.TrackProgress('Error Description = ' + UPCERR[1,2],LOGIT+SENDIT)
							ENDIF
						ENDIF
					ENDIF

					*.TrackProgress(TRANSFORM(lnCtr ) + ' INSERTS were made from ' + lcSourceFile,LOGIT+SENDIT)
					.TrackProgress(TRANSFORM(lnCtr ) + ' INSERTS were made from ' + lcOrigFile,LOGIT+SENDIT)
					*.TrackProgress(TRANSFORM(lnDupeDelCtr) + ' found records were deleted ',LOGIT+SENDIT)
					.TrackProgress(TRANSFORM(lnDupeCtr) + ' found records were updated ',LOGIT+SENDIT)

					* if the file already exists in the archive folder, make sure it is not read-only.
					* this is to prevent errors we get copying into archive on a resend of an already-archived file.
					* Necessary because the files in the archive folders were sometimes marked as read-only (not sure why)
					llArchived = FILE(lcArchivedFile)
					IF llArchived THEN
						RUN ATTRIB -R &lcArchivedFile.
						*.TrackProgress('   Used ATTRIB -R for re-archiving : ' + lcSourceFile,LOGIT+SENDIT)
						.TrackProgress('   Used ATTRIB -R for re-archiving : ' + lcOrigFile,LOGIT+SENDIT)
					ENDIF

					* archive the source file
					*COPY FILE (lcSourceFile) TO (lcArchivedFile)
					COPY FILE (lcOrigFile) TO (lcArchivedFile)

					* delete the source file
					IF FILE(lcArchivedFile) THEN
						*DELETE FILE (lcSourceFile)
						DELETE FILE (lcOrigFile)
					ENDIF
			
					WAIT CLEAR

				ENDFOR && lnCurrentFile = 1 TO lnNumFiles
			ELSE
				.TrackProgress('Found no files in the S3 source folder', LOGIT+SENDIT)
			ENDIF

		ENDWITH
		RETURN
	ENDPROC  &&  ProcessFolder
	
	
	PROCEDURE FixDupeUPCs
		WITH THIS
				LOCAL lnUpcmastid, lnUPCsBlanked
				
				lnUPCsBlanked = 0
				
				useca("upcmast","wh",,,"select * from upcmast where accountid = " + ALLTRIM(STR(.nAccountID,4,0)),,"upcmastsql")
				SELECT upcmastsql
				GO bottom
				
				SELECT UPC, COUNT(*) AS CNT ;
					FROM upcmastsql ;
					INTO CURSOR CURDUPES ;
					WHERE NOT EMPTYNUL(UPC) ;
					GROUP BY UPC ;
					ORDER BY UPC ;
					HAVING CNT > 1

*!*					SELECT CURDUPES
*!*					BROWSE
								
				IF USED('CURDUPES') AND NOT EOF('CURDUPES') THEN
					SELECT CURDUPES
					SCAN
						SELECT upcmastsql
						LOCATE FOR UPC == CURDUPES.UPC
						IF FOUND() THEN
							lnUpcmastid = upcmastsql.upcmastid
							SELECT upcmastsql
							SCAN FOR (UPC == CURDUPES.UPC) AND (NOT upcmastid = lnUpcmastid)	
								REPLACE upcmastsql.UPC WITH "" IN upcmastsql
								lnUPCsBlanked = lnUPCsBlanked + 1
							ENDSCAN
						ENDIF
					ENDSCAN
				ENDIF
				
				.TrackProgress('in FixDupeUPCs() # UPCs Blanked = ' + TRANSFORM(lnUPCsBlanked),LOGIT+SENDIT)
					
				IF tu("upcmastsql") THEN
					.TrackProgress('SUCCESS: TU(upcmastsql) in FixDupeUPCs() returned .T.!',LOGIT+SENDIT)
				ELSE
					* ERROR on table update
					.TrackProgress('ERROR: TU(upcmastsql) in FixDupeUPCs() returned .F.!',LOGIT+SENDIT)
					=AERROR(UPCERR)
					IF TYPE("UPCERR")#"U" THEN 
						.TrackProgress('Error Description = ' + UPCERR[1,2],LOGIT+SENDIT)
					ENDIF
				ENDIF
						
		ENDWITH
		RETURN 
	ENDPROC  && FixDupeUPCs


	PROCEDURE TrackProgress
		* do any combination of Wait Window, writing to logfile, and adding to body of email,
		* based on nFlags parameter.
		LPARAMETERS tcExpression, tnFlags
		WITH THIS
			IF BITAND(tnFlags,LOGIT) = LOGIT THEN
				IF .lLoggingIsOn THEN
					?
					? .cProcessName + "  " + TTOC(DATETIME()) + ": " + tcExpression
				ENDIF
			ENDIF
			IF .lWaitWindowIsOn THEN
				IF BITAND(tnFlags,NOWAITIT) = NOWAITIT THEN
					WAIT WINDOW tcExpression NOWAIT
				ENDIF
				IF BITAND(tnFlags,WAITIT) = WAITIT THEN
					WAIT WINDOW tcExpression TIMEOUT .nWaitWindowTimeout
				ENDIF
			ENDIF
			IF BITAND(tnFlags,SENDIT) = SENDIT THEN
				IF .lSendInternalEmailIsOn THEN
					.cBodyText = .cBodyText + tcExpression + CRLF + CRLF
				ENDIF
			ENDIF
		ENDWITH
	ENDPROC  &&  TrackProgress


	PROCEDURE SortArrayByDateTime
		* expects taInArray[] to be the result of an ADIR()
		LPARAMETERS taInArray, taOutArray
		EXTERNAL ARRAY taInArray, taOutArray
		LOCAL lnRows, lnCols, i, j
		* extending taInArray[] to process the files in date/time order
		lnRows = ALEN(taInArray,1)
		lnCols = ALEN(taInArray,2)
		* create taOutArray with one more column
		DIMENSION taOutArray[lnRows,lnCols + 1]
		* and fill it with taInArray columns AND (filedate + filetime) in new column
		* ( ACOPY() doesn't preserve elements properly because of different # of columns )
		FOR i = 1 TO lnRows
			FOR j = 1 TO lnCols
				taOutArray[i,j] = taInArray[i,j]
			ENDFOR
			taOutArray[i,lnCols + 1] = DTOS(taInArray[i,3]) + "_" + taInArray[i,4]
		ENDFOR
		* sort taOutArray on date/time column
		=ASORT(taOutArray,6)
	ENDPROC

ENDDEFINE
