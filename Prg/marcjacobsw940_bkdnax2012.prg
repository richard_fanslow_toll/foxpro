*!* m:\dev\prg\marcjacobsw940_bkdn.prg, 11.29.2011, Joe
lCheckStyle = .T.
lPrepack = .F.
lPick = .T.
lJCPenney = .F.
lDoImport = .T.

STORE "" TO m.isa_num,m.id,cStylecheck,cSTStoreNum,cDesc
ptctr = 0

lBrowfiles = lTesting
*lBrowfiles = .t.

SELECT x856
COUNT FOR TRIM(x856.segment) = "ST" TO nPTQty
cSegCnt = ALLTRIM(STR(nPTQty))
WAIT WINDOW "There are "+cSegCnt+" picktickets in file "+cFilename TIMEOUT WAITTIME

LOCATE
IF lTesting
	WAIT WINDOW "Now in 940 Breakdown"+CHR(13)+"RUNNING IN TEST MODE..." TIMEOUT WAITTIME
	m.ship_via = "WILL CALL CUST"
ELSE
	WAIT WINDOW "Now in 940 Breakdown" TIMEOUT 2
ENDIF
WAIT "There are "+cSegCnt+" P/Ts in this file" WINDOW TIMEOUT WAITTIME

SELECT x856
LOCATE

*!* Constants
m.acct_name = cCustname && Set in start prg

&& nAcctNum = 6303, this set in the startup
*  Cros reference for AX2012
*  6303 = 1000 - Wholesale
*  6304 = 1200 - Retail
*  6305 = 1320 - ECOMM
*  6306 = supplies no equivalent
*  6320 = 1310 - Partnet Stores
*  6321 = 1221 - UK
*  6322 = 1222 - France
*  6323 = 1223 - Italy
*  6324 = 1250 - Retail Damages
*  6325 = 1150 - Wholesale Damages
*  6364 = 1400 - Online Sale

*
m.acct_num = nAcctNum
m.sf_addr1 = "72 SPRING STREET"
m.sf_addr2 = "7TH FLOOR"
m.sf_csz = "NEW YORK, NY 10012"

*!*	IF !USED("uploaddet")
*!*		IF lTesting
*!*			USE F:\ediwhse\test\uploaddet IN 0 ALIAS uploaddet
*!*		ELSE
*!*			USE F:\ediwhse\DATA\uploaddet IN 0 SHARED ALIAS uploaddet
*!*		ENDIF
*!*	ENDIF

SELECT xpt
DELETE ALL
SELECT xptdet
DELETE ALL

SELECT x856
SET FILTER TO
LOCATE

ptidctr    =0
ptdetidctr =0


DO WHILE !EOF("x856")
	cSegment = TRIM(x856.segment)
	cCode = TRIM(x856.f1)

	IF TRIM(x856.segment) = "ISA"
		m.isa_num = ALLTRIM(x856.f13)
		cISA_Num = ALLTRIM(x856.f13)
		SELECT x856
		SKIP
		LOOP
	ENDIF

	IF TRIM(x856.segment) = "GS"
		SELECT x856
		SKIP
		LOOP
	ENDIF

	IF TRIM(x856.segment) = "ST" AND TRIM(x856.f1) = "940"
		SELECT x856
		SKIP
		LOOP
	ENDIF

	IF TRIM(x856.segment) = "W05"  && new shipment
		cShip_ref = ALLTRIM(x856.f5) && PG from F2 to F5 1/12/2012
		cOrderNumber=ALLTRIM(x856.f2) && the MJ Order Number, stored in shipins
		lDoInsert = .F.
		WAIT WINDOW AT 10,10 "Now processing PT# "+ cShip_ref NOWAIT
		SELECT xpt
		nDetCnt = 0
		ptdetctr = 0
		ptidctr  = ptidctr    +1
		m.pack = "1"
		APPEND BLANK
		STORE 0 TO pt_total_eaches
		STORE 0 TO pt_total_cartons
		ptctr = ptctr +1
		REPLACE xpt.ptid WITH ptidctr IN xpt
		STORE ptctr TO m.ptid
		REPLACE xpt.goh WITH .T. IN xpt
		REPLACE xpt.accountid  WITH m.acct_num IN xpt
		REPLACE xpt.ptdate     WITH DATE() IN xpt
		REPLACE xpt.ship_ref   WITH cShip_ref IN xpt  && Pick Ticket
		m.pt = ALLTRIM(x856.f2)
		IF lTesting
*      REPLACE xpt.cacctnum   with "X999" IN xpt
		ENDIF
		REPLACE xpt.cnee_ref WITH UPPER(ALLTRIM(x856.f3)) IN xpt
		REPLACE xpt.shipins WITH IIF(EMPTY(xpt.shipins),"ORDERNUMBER*"+cOrderNumber,xpt.shipins+CHR(13)+"ORDERNUMBER*"+cOrderNumber) IN xpt
		SELECT x856
		SKIP
		LOOP
	ENDIF

*!*	  If Trim(x856.segment) = "N1" And Trim(x856.f1) = "SF" && SHIP FROM, NEW AX2012
*!*	    Do Case
*!*	    Case Alltrim(x856.f4) ="1000" && AX2012 acct# for Wholsale
*!*	      nAcctNum = 6303
*!*	      m.acct_name = "MARC JACOBS WHOLESALE"
*!*	      m.acct_num = nAcctNum
*!*	      Replace xpt.accountid  With m.acct_num In xpt
*!*	    Case Alltrim(x856.f4) ="1200" && AX2012 acct# for Retail
*!*	      nAcctNum = 6304
*!*	      m.acct_name = "MARC JACOBS RETAIL"
*!*	      m.acct_num = nAcctNum
*!*	      Replace xpt.accountid  With m.acct_num In xpt
*!*	    Case Alltrim(x856.f4) ="1320" && AX2012 acct# for ECOMM
*!*	      nAcctNum = 6305
*!*	      m.acct_name = "MARC JACOBS ECOMM"
*!*	      m.acct_num = nAcctNum
*!*	      Replace xpt.accountid  With m.acct_num In xpt
*!*	    Case Alltrim(x856.f4) ="1310" && AX2012 acct# for Partner Stores
*!*	      nAcctNum = 6320
*!*	      m.acct_name = "MARC JACOBS PARTNER STORES"
*!*	      m.acct_num = nAcctNum
*!*	      Replace xpt.accountid  With m.acct_num In xpt
*!*	    Case Alltrim(x856.f4) ="1221" && AX2012 acct# for UK-Retail
*!*	      nAcctNum = 6321
*!*	      m.acct_name = "MARC JACOBS UK-RETAIL"
*!*	      m.acct_num = nAcctNum
*!*	      Replace xpt.accountid  With m.acct_num In xpt
*!*	    Case Alltrim(x856.f4) ="1222" && AX2012 acct# for FR-Retail
*!*	      nAcctNum = 6322
*!*	      m.acct_name = "MARC JACOBS FR-RETAIL"
*!*	      m.acct_num = nAcctNum
*!*	      Replace xpt.accountid  With m.acct_num In xpt
*!*	    Case Alltrim(x856.f4) ="1223" && AX2012 acct# for IT-Retail
*!*	      nAcctNum = 6323
*!*	      m.acct_name = "MARC JACOBS IT-RETAIL"
*!*	      m.acct_num = nAcctNum
*!*	      Replace xpt.accountid  With m.acct_num In xpt
*!*	    Case Alltrim(x856.f4) ="1250" && AX2012 acct# for Retail DMG
*!*	      nAcctNum = 6324
*!*	      m.acct_name = "MARC JACOBS RETAIL DMG"
*!*	      m.acct_num = nAcctNum
*!*	      Replace xpt.accountid  With m.acct_num In xpt
*!*	    Case Alltrim(x856.f4) ="1151" && AX2012 acct# for Wholesale DMG
*!*	      nAcctNum = 6325
*!*	      m.acct_name = "MARC JACOBS WHOLESALE DMG"
*!*	      m.acct_num = nAcctNum
*!*	      Replace xpt.accountid  With m.acct_num In xpt
*!*	    Case Alltrim(x856.f4) ="1400" && AX2012 acct# for Online sale
*!*	      nAcctNum = 6364
*!*	      m.acct_name = "MARC JACOBS ONLINE SALE"
*!*	      m.acct_num = nAcctNum
*!*	      Replace xpt.accountid  With m.acct_num In xpt
*!*	    Case Alltrim(x856.f4) ="1600" && AX2012 acct# for Press Samples
*!*	      nAcctNum = 6543
*!*	      m.acct_name = "MARC JACOBS PRESS SAMPLE"
*!*	      m.acct_num = nAcctNum
*!*	      Replace xpt.accountid  With m.acct_num In xpt
*!*	    Case Alltrim(x856.f4) ="1001" && AX2012 acct# for Wholesale CA added 02/11/15 TM
*!*	    SET STEP ON
*!*	      nAcctNum = 6303
*!*	      m.acct_name = "MARC JACOBS WHOLESALE"
*!*	      m.acct_num = nAcctNum
*!*	      Replace xpt.accountid  With m.acct_num In xpt
*!*	          Case Alltrim(x856.f4) ="1152" && AX2012 acct# for Wholesale CA added 12/09/15 TM
*!*	      nAcctNum = 6325
*!*	      m.acct_name = "MARC JACOBS WHOLESALE DMG"
*!*	      m.acct_num = nAcctNum
*!*	      Replace xpt.accountid  With m.acct_num In xpt
*!*	    Otherwise
*!*	      errmsg = "INVALID ACCOUNT NUMBER......."
*!*	      Throw
*!*	    Endcase
*!*	    Select x856
*!*	    Skip 4 In x856
*!*	    Loop
*!*	  ENDIF

	IF TRIM(x856.segment) = "N1" AND TRIM(x856.f1) = "SF" && SHIP FROM, NEW AX2012
		DO CASE
			CASE ALLTRIM(x856.f4) ="10100" && AX2012 acct# for Wholsale
				nAcctNum = 6303
				m.acct_name = "MARC JACOBS WHOLESALE"
				m.acct_num = nAcctNum
				REPLACE xpt.accountid  WITH m.acct_num IN xpt
			CASE ALLTRIM(x856.f4) ="10180" && AX2012 acct# for Wholesale DMG
				nAcctNum = 6325
				m.acct_name = "MARC JACOBS WHOLESALE DMG"
				m.acct_num = nAcctNum
				REPLACE xpt.accountid  WITH m.acct_num IN xpt
			CASE ALLTRIM(x856.f4) ="10190" && AX2012 acct# for Press Samples
				nAcctNum = 6543
				m.acct_name = "MARC JACOBS PRESS SAMPLE"
				m.acct_num = nAcctNum
				REPLACE xpt.accountid  WITH m.acct_num IN xpt
			CASE ALLTRIM(x856.f4) ="11100" && AX2012 acct# for Wholesale CA added 02/11/15 TM
				nAcctNum = 6303
				m.acct_name = "MARC JACOBS WHOLESALE"
				m.acct_num = nAcctNum
				REPLACE xpt.accountid  WITH m.acct_num IN xpt
			CASE ALLTRIM(x856.f4) ="11180" && AX2012 acct# for Wholesale CA added 12/09/15 TM
				nAcctNum = 6325
				m.acct_name = "MARC JACOBS WHOLESALE DMG"
				m.acct_num = nAcctNum
				REPLACE xpt.accountid  WITH m.acct_num IN xpt
			CASE ALLTRIM(x856.f4) ="11190" && AX2012 acct# for Press Samples
				nAcctNum = 6543
				m.acct_name = "MARC JACOBS PRESS SAMPLE"
				m.acct_num = nAcctNum
			OTHERWISE
				errmsg = "INVALID ACCOUNT NUMBER......."
				THROW
		ENDCASE
		SELECT x856
		SKIP 4 IN x856
		LOOP
	ENDIF

	IF TRIM(x856.segment) = "N1" AND TRIM(x856.f1) = "BT" && SHIP FROM, NEW AX2012
		SELECT x856
		SKIP 2
		LOOP
	ENDIF

	IF TRIM(x856.segment) = "N1" AND TRIM(x856.f1) = "ST" && Ship-to data
*!*			IF !EMPTY(m.isa_num)
*!*				SELECT uploaddet
*!*				APPEND BLANK
*!*				REPLACE uploaddet.office    WITH cOffice
*!*				REPLACE uploaddet.cust_name WITH UPPER(TRIM(x856.f2))
*!*				REPLACE uploaddet.acct_num  WITH m.acct_num
*!*				REPLACE uploaddet.DATE      WITH DATETIME()
*!*				REPLACE uploaddet.isa_num   WITH m.isa_num
*!*				REPLACE uploaddet.startpt   WITH xpt.ship_ref
*!*				REPLACE uploaddet.runid     WITH nRunID
*!*			ENDIF

		SELECT xpt
		cConsignee = UPPER(TRIM(x856.f2))
		IF "MARC JACOBS INTERNATIONAL"$cConsignee
			cConsignee = "MJI "+SUBSTR(TRIM(x856.f2),27)
		ENDIF

		m.st_name = TRIM(x856.f2)
		REPLACE xpt.consignee WITH cConsignee IN xpt
		REPLACE xpt.NAME      WITH cConsignee IN xpt  && added this as Consignee will be over written by the N9*CU*new name value

		cStoreNum = TRIM(x856.f4)
		cSTStoreNum = TRIM(x856.f4)
*    ASSERT .f. MESSAGE "At store number conversion"
		IF LEN(cStoreNum)>5
			REPLACE xpt.storenum  WITH VAL(RIGHT(cStoreNum,5)) IN xpt
		ELSE
			REPLACE xpt.storenum  WITH VAL(cStoreNum) IN xpt
		ENDIF
		REPLACE xpt.dcnum  WITH cStoreNum IN xpt
		REPLACE xpt.shipins WITH IIF(EMPTY(xpt.shipins),"STORENUM*"+cStoreNum,xpt.shipins+CHR(13)+"STORENUM*"+cStoreNum) IN xpt

		SKIP 1 IN x856
		IF TRIM(x856.segment) = "N3"  && address info
			m.st_addr1 = UPPER(TRIM(x856.f1))
			IF LEN(m.st_addr1)>30
				cSt_addr1ext = SUBSTR(m.st_addr1,21)
				REPLACE xpt.shipins WITH ;
					IIF(EMPTY(xpt.shipins),"ST_ADDR1EXT*"+cSt_addr1ext,;
					xpt.shipins+CHR(13)+"ST_ADDR1EXT*"+cSt_addr1ext)
				m.st_addr1 = LEFT(m.st_addr1,30)
			ENDIF
			m.st_addr2 = UPPER(TRIM(x856.f2))
			IF LEN(m.st_addr2)>30
				cSt_addr2ext = SUBSTR(m.st_addr2,21)
				REPLACE xpt.shipins WITH ;
					IIF(EMPTY(xpt.shipins),"ST_ADDR2EXT*"+cSt_addr2ext,;
					xpt.shipins+CHR(13)+"ST_ADDR2EXT*"+cSt_addr2ext)
				m.st_addr2 = LEFT(m.st_addr2,30)
			ENDIF
			REPLACE xpt.address  WITH m.st_addr1
			REPLACE xpt.address2 WITH m.st_addr2
		ENDIF
		SKIP 1 IN x856
		IF TRIM(x856.segment) = "N3"  && 2nd line address info
			m.st_addr2 = UPPER(TRIM(x856.f1))
			IF LEN(m.st_addr2)>30
				cSt_addr2ext = SUBSTR(m.st_addr2,21)
				REPLACE xpt.shipins WITH ;
					IIF(EMPTY(xpt.shipins),"ST_ADDR2EXT*"+cSt_addr2ext,;
					xpt.shipins+CHR(13)+"ST_ADDR2EXT*"+cSt_addr2ext)
				m.st_addr2 = LEFT(m.st_addr2,30)
			ENDIF
			SKIP 1 IN x856
		ENDIF

		IF TRIM(x856.segment) = "N4"  && address info
			cZipcode = TRIM(x856.f3)
			cCity = LEFT(UPPER(TRIM(x856.f1)),20)
			cState = UPPER(TRIM(x856.f2))
			cCountry = ALLTRIM(x856.f4)
			REPLACE xpt.shipins WITH ;
				IIF(EMPTY(xpt.shipins),;
				"CITY*"+cCity+CHR(13)+"STATE*"+cState+CHR(13)+"ZIPCODE*"+cZipcode+CHR(13)+"COUNTRY*"+cCountry,;
				xpt.shipins+CHR(13)+"CITY*"+cCity+CHR(13)+"STATE*"+cState+CHR(13)+"ZIPCODE*"+cZipcode+CHR(13)+"COUNTRY*"+cCountry)
			m.st_csz = cCity+", "+cState+" "+cZipcode
			REPLACE xpt.csz WITH m.st_csz
		ENDIF
		SELECT x856
		SKIP
		LOOP
	ENDIF

	IF TRIM(x856.segment) = "N1" AND TRIM(x856.f1) = "Z7"
		SELECT xpt
		REPLACE xpt.shipfor WITH ALLTRIM(x856.f2)
		cSFStoreNum = TRIM(x856.f4)
		REPLACE xpt.sforstore  WITH ALLTRIM(cSFStoreNum)
		REPLACE xpt.storenum  WITH VAL(RIGHT(ALLTRIM(cSFStoreNum),5))
		SKIP 1 IN x856
		IF TRIM(x856.segment) = "N3"  && address info
			m.sforaddr1 = UPPER(TRIM(x856.f1))
			m.sforaddr2 = UPPER(TRIM(x856.f2))
			REPLACE xpt.sforaddr1 WITH m.sforaddr1
			REPLACE xpt.sforaddr2 WITH m.sforaddr2
		ELSE
			LOOP
		ENDIF
		SKIP 1 IN x856
		IF TRIM(x856.segment) = "N3"  && address info
			m.sforaddr2 = UPPER(TRIM(x856.f1))
			REPLACE xpt.sforaddr2 WITH m.sforaddr2
			SKIP 1 IN x856
		ENDIF
		IF TRIM(x856.segment) = "N4"  && address info
			cSforZipcode = TRIM(x856.f3)
			cSforCity = LEFT(UPPER(TRIM(x856.f1)),20)
			cSforState = UPPER(TRIM(x856.f2))
			cSforCountry = ALLTRIM(x856.f4)
			m.sforcsz = UPPER(TRIM(x856.f1))+", "+UPPER(TRIM(x856.f2))+" "+TRIM(x856.f3)
			REPLACE xpt.sforcsz WITH m.sforcsz
		ENDIF
		REPLACE shipins WITH ;
			IIF(EMPTY(xpt.shipins),;
			"SFORCITY*"+cCity+CHR(13)+"SFORSTATE*"+cSforState+CHR(13)+"SFORZIPCODE*"+cSforZipcode+CHR(13)+;
			"SFORCOUNTRY*"+cSforCountry,;
			xpt.shipins+CHR(13)+"SFORCITY*"+cSforCity+CHR(13)+"SFORSTATE*"+cSforState+CHR(13)+"SFORZIPCODE*"+;
			cSforZipcode+CHR(13)+"SFORCOUNTRY*"+cSforCountry)
		SELECT x856
		SKIP
		LOOP
	ENDIF

	IF TRIM(x856.segment) = "N9"
		DO CASE
			CASE TRIM(x856.f1) = "DP"
				cDept = ALLTRIM(x856.f2)
				cDeptname = "DEPTNAME*"+ALLTRIM(x856.f3)
				REPLACE xpt.dept WITH cDept IN xpt
				REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+cDeptname IN xpt

			CASE TRIM(x856.f1) = "6P"
				cGrp =ALLTRIM(x856.f2)
				cGrpNum = "GROUP*"+ALLTRIM(x856.f2)
				REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+cGrpNum IN xpt

			CASE TRIM(x856.f1) = "PG" && Group Number for SAKS for label added 5/30/14 TM
				cGrpNum = "GRPNUM*"+ALLTRIM(x856.f2)
				REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+cGrpNum IN xpt

			CASE TRIM(x856.f1) = "SAC"
				cSAC =ALLTRIM(x856.f2)
				cSACcode = "SAC*"+ALLTRIM(x856.f2)
				REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+cSACcode IN xpt

			CASE TRIM(x856.f1) = "IA" && Internal Cust. Acct. ID
				cVendNum =ALLTRIM(x856.f2)
				REPLACE xpt.vendor_num WITH cVendNum IN xpt

			CASE TRIM(x856.f1) = "DV"
				cDivCode = "DIVCODE*"+ALLTRIM(x856.f2)
				REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+cDivCode IN xpt
				REPLACE xpt.batch_num WITH ALLTRIM(x856.f2) IN xpt

			CASE TRIM(x856.f1) = "CU"
				cCacctnum = ALLTRIM(x856.f2)
				REPLACE xpt.cacctnum WITH cCacctnum IN xpt
				cConsignee = UPPER(TRIM(x856.f3))  && added this update 2/7/2012 PG

				IF !"MARC JACOBS"$cConsignee  && on retail orders keep the original shipto
					m.st_name = TRIM(x856.f2)
					REPLACE xpt.consignee WITH cConsignee IN xpt
				ENDIF

			CASE TRIM(x856.f1) = "TM"
				cTerms = ALLTRIM(x856.f2)
				cTermsDesc = "TERMS*"+ALLTRIM(x856.f3)
				REPLACE xpt.terms WITH cTerms IN xpt
				REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+cTermsDesc IN xpt

			CASE TRIM(x856.f1) = "3P"
				cTPA = "TPA*"+ALLTRIM(x856.f2)
				REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+cTPA IN xpt

			CASE TRIM(x856.f1) = "VAT"
				cVAT = "VAT*"+ALLTRIM(x856.f2)
				REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+cVAT IN xpt

			CASE TRIM(x856.f1) = "TR"
				cTR = "TERMSCODE*"+ALLTRIM(x856.f2)
				REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+cTR IN xpt

			CASE TRIM(x856.f1) = "MR"
				cGN = "GROUPNAME*"+ALLTRIM(x856.f2)
				REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+cGN IN xpt

			CASE TRIM(x856.f1) = "CR"
				lcCR =  "RPRO*"+ALLTRIM(x856.f2)
				lcRetailStore = SUBSTR(ALLTRIM(x856.f2),3,3)
				REPLACE storenum WITH VAL(lcRetailStore) IN xpt
				lcRPRO_Subsidiary = SUBSTR(ALLTRIM(x856.f2),1,2)
				REPLACE dcnum WITH lcRPRO_Subsidiary IN xpt
				REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+lcCR IN xpt
				REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"RPROSUB*"+lcRPRO_Subsidiary IN xpt

		ENDCASE
		SELECT x856
		SKIP
		LOOP
	ENDIF

	IF TRIM(x856.segment) = "G62" AND (TRIM(x856.f1) == "10") && Start date
		lcDate = ALLTRIM(x856.f2)
		lxDate = SUBSTR(lcDate,5,2)+"/"+SUBSTR(lcDate,7,2)+"/"+LEFT(lcDate,4)
		ldDate = CTOD(lxDate)
		REPLACE xpt.START WITH ldDate IN xpt
		SELECT x856
		SKIP
		LOOP
	ENDIF

	IF TRIM(x856.segment) = "G62" AND TRIM(x856.f1) == "01" && Cancel date
		lcDate = ALLTRIM(x856.f2)
		lxDate = SUBSTR(lcDate,5,2)+"/"+SUBSTR(lcDate,7,2)+"/"+LEFT(lcDate,4)
		ldDate = CTOD(lxDate)
		REPLACE xpt.CANCEL WITH ldDate IN xpt
		SELECT x856
		SKIP
		LOOP
	ENDIF

	IF TRIM(x856.segment) = "NTE"  && Notes
		DO CASE
			CASE TRIM(x856.f1) = "EDI"
				REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"FOB*"+ALLT(x856.f2) IN xpt
			CASE TRIM(x856.f1) = "DEL"
				REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"DELIVINST*"+ALLT(x856.f2) IN xpt
			CASE TRIM(x856.f1) = "PKG"
				REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"PKGINST*"+ALLT(x856.f2) IN xpt
			CASE TRIM(x856.f1) = "SPH"
				REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"SPHANDLE*"+ALLT(x856.f2) IN xpt
			CASE TRIM(x856.f1) = "WHI"
				REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"SPINS*"+ALLT(x856.f2) IN xpt
			CASE TRIM(x856.f1) = "GFT"
				REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"GIFTMSG*"+ALLT(x856.f2) IN xpt
			CASE TRIM(x856.f1) = "PRO"
				REPLACE xpt.keyrec WITH "PR"+ALLT(x856.f2) IN xpt
			CASE TRIM(x856.f1) = "MBO"
				REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"MBOL*"+ALLT(x856.f2)
			CASE TRIM(x856.f1) = "BOL"
				REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"BOL*"+ALLT(x856.f2)
			CASE TRIM(x856.f1) = "PDN"
				REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"PROMO*"+ALLT(x856.f2)
			CASE TRIM(x856.f1) = "REF"
				REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"REFNUMPO*"+ALLT(x856.f2)
			CASE TRIM(x856.f1) = "ZZZ"
				REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"REFNUMOTH*"+ALLT(x856.f2)
			CASE TRIM(x856.f1) = "MRT"
				REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"EDIPOTYPE*"+ALLT(x856.f2)
			CASE TRIM(x856.f1) = "STT"
				REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"STORETYPE*"+ALLT(x856.f2)
			CASE TRIM(x856.f1) = "BWV"
				REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"BCWAVENUM*"+ALLT(x856.f2)
			CASE TRIM(x856.f1) = "BOS"
				REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"BCORDERSOURCE*"+ALLT(x856.f2)
			CASE TRIM(x856.f1) = "CAC"
				REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"DBA*"+ALLT(x856.f2)
		ENDCASE
		SELECT x856
		SKIP
		LOOP
	ENDIF


	IF TRIM(x856.segment) = "W66"  && Shipper Information
		cShipcode = UPPER(ALLTRIM(x856.f2))
		cShip_via = UPPER(ALLTRIM(x856.f5))
		cSCAC = ALLTRIM(x856.f10)
		REPLACE xpt.shipins  WITH xpt.shipins+CHR(13)+"SHIPCODE*"+cShipcode IN xpt
		REPLACE xpt.scac     WITH cSCAC IN xpt
		REPLACE xpt.ship_via WITH cShip_via IN xpt
		SELECT x856
		SKIP
		LOOP
	ENDIF

	IF TRIM(x856.segment) = "LX" && start of PT detail, stay here until this PT is complete
		SKIP 1 IN x856

		DO WHILE ALLTRIM(x856.segment) != "SE"
			cSegment = TRIM(x856.segment)
			cCode = TRIM(x856.f1)
			IF TRIM(x856.segment) = "W01" && Destination Quantity
				m.totqty = VAL(ALLTRIM(x856.f1))
				cUnitstype = "UNITSTYPE*"+ALLTRIM(x856.f2)

				STORE 1 TO nCasePack

				m.upc = ""
				IF ALLTRIM(x856.f4) = "UP"
					STORE ALLTRIM(x856.f5) TO m.upc
				ENDIF

				IF ALLTRIM(x856.f6) = "VN"
					STORE ALLTRIM(x856.f7) TO m.style
				ENDIF
				SELECT x856
				SKIP 1
				LOOP
			ENDIF

			IF TRIM(x856.segment) = "G69"
				cDesc = "DESC*"+ALLTRIM(x856.f1)
				SELECT x856
				SKIP 3
				LOOP
			ENDIF

			IF TRIM(x856.segment) = "N9" AND TRIM(x856.f1) = "VC"
				cColor = ALLTRIM(x856.f2)
				SELECT x856
				SKIP 1
				LOOP
			ENDIF

			IF TRIM(x856.segment) = "N9" AND TRIM(x856.f1) = "SZ"
				cID = ALLTRIM(x856.f2)

** after this segment do an insert into Xpt
				SELECT xptdet
				APPEND BLANK
				ptdetidctr = ptdetidctr +1
				REPLACE xptdet.ptid       WITH ptidctr    IN xptdet
				REPLACE xptdet.ptdetid    WITH ptdetidctr IN xptdet
				REPLACE xptdet.accountid  WITH xpt.accountid IN xptdet
				REPLACE xptdet.units      WITH .T.        IN xptdet
				REPLACE xptdet.STYLE      WITH m.style    IN xptdet
				REPLACE xptdet.COLOR      WITH cColor     IN xptdet
				REPLACE xptdet.ID         WITH cID        IN xptdet
				REPLACE xptdet.PACK       WITH "1"        IN xptdet
				REPLACE xptdet.custsku    WITH m.upc      IN xptdet
				REPLACE xptdet.casepack   WITH 1          IN xptdet
				REPLACE xptdet.upc        WITH m.upc      IN xptdet
				REPLACE xptdet.totqty     WITH m.totqty   IN xptdet
				REPLACE xptdet.origqty    WITH xptdet.totqty IN xptdet
				REPLACE xptdet.printstuff WITH xptdet.printstuff+CHR(13)+cDesc IN xptdet
				REPLACE xptdet.printstuff WITH xptdet.printstuff +CHR(13)+"COLORDESC*"+cColor IN xptdet
				REPLACE xptdet.printstuff WITH xptdet.printstuff +CHR(13)+cUnitstype IN xptdet

				SELECT x856
				SKIP 1
				LOOP
			ENDIF

			IF ALLTRIM(x856.segment)="LX" && start of new PT detail, use same header
				SKIP 1 IN x856
				LOOP
			ENDIF

			IF TRIM(x856.segment) = "W76"  && Total carton quantity
				nCtnCount = INT(VAL(ALLTRIM(x856.f1)))
				REPLACE xpt.qty WITH nCtnCount IN xpt
				REPLACE xpt.origqty WITH xpt.qty IN xpt
				IF lPick
					nCtnCount = 0
				ENDIF
				SELECT xptdet
			ENDIF
 
			SELECT xptdet
			SKIP 1 IN x856
		ENDDO
		SELECT xptdet
	ENDIF

	SELECT x856
	IF !EOF()
		SKIP 1 IN x856
	ENDIF
ENDDO

*!* Added this block to create dupe-detail error file and mail
SELECT xpt
SELECT ptid,upc,STYLE,COUNT(upc) AS cnt1 ;
	FROM xptdet ;
	GROUP BY ptid,upc ;
	INTO CURSOR tempdet1 ;
	HAVING cnt1 > 1
LOCATE
IF !EOF()
	SELECT a.ship_ref AS pickticket,b.STYLE,b.upc,b.cnt1 AS linecount ;
		FROM xpt a,tempdet1 b ;
		WHERE a.ptid = b.ptid ;
		INTO CURSOR tempdupes
	LOCATE

	cExcelDupes = "c:\tempfox\dupedetail.xls"
	COPY TO [&cExcelDupes] TYPE XL5
	USE IN tempdet1
	USE IN tempdupes
	IF !lTesting
		SELECT 0
		USE F:\3pl\DATA\mailmaster ALIAS mm
		LOCATE FOR GROUP = "MARCJACOBS" AND taskname = 'MJPLERROR'
		STORE TRIM(IIF(mm.use_alt,mm.sendtoalt,mm.sendto)) TO tsendto
		STORE TRIM(IIF(mm.use_alt,mm.ccalt,mm.cc)) TO tcc
		USE IN mm
	ELSE
		tsendto = tsendtoerr
		tcc = tccerr
	ENDIF

	tsendto ="pgaidis@fmiint.com"
	tcc = ""

	tsubject = "Error: Marc Jacobs Duplicate Detail Lines"
	tmessage = "Marc Jacobs (W) 940 Upload Error!"
	tmessage = tmessage+CHR(13)+"The attached Excel file contains duplicate line items by PT"
	tmessage = tmessage+CHR(13)+"From file "+cFilename
	tattach  = cExcelDupes
	tfrom    ="Toll EDI Processing Center <toll-edi-ops@tollgroup.com>"
	DO FORM m:\dev\frm\dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
	lDoImport = .T.
ENDIF
*!* End added code block

IF lDoImport
	rollup()
	DO m:\dev\prg\setuppercase940
	lBrowfiles = .F.
	IF lBrowfiles
		SELECT xpt
		LOCATE
		BROWSE
		SELECT xptdet
		BROWSE
	ENDIF

	WAIT cCustname+" Breakdown Round complete..." WINDOW TIMEOUT WAITTIME

	SELECT x856
	IF USED("PT")
		USE IN pt
	ENDIF
	IF USED("PTDET")
		USE IN ptdet
	ENDIF
	WAIT CLEAR
	lDoImport = .T.
ENDIF
RETURN

******************************
PROCEDURE rollup
******************************
	ASSERT .F. MESSAGE "In rollup procedure"
	nChangedRecs = 0

	SELECT xptdet
	SET DELETED ON
	LOCATE
	IF lTesting
		WAIT WINDOW "Browsing XPTDET pre-rollup" TIMEOUT 2
		BROW
	ENDIF
	cMsgStr = "PT/OSID/ODIDs changed: "
	SCAN FOR !DELETED()
		SCATTER MEMVAR
		nRecno = RECNO()
		LOCATE FOR xptdet.ptid = m.ptid ;
			AND xptdet.STYLE = m.style ;
			AND xptdet.COLOR = m.color ;
			AND xptdet.ID = m.id ;
			AND xptdet.PACK = m.pack ;
			AND xptdet.upc = m.upc ;
			AND RECNO('xptdet') > nRecno
		IF FOUND()
			SELECT xpt
			LOCATE FOR xpt.ptid = m.ptid
			IF !FOUND()
				WAIT WINDOW "PROBLEM!"
			ENDIF
			cShip_ref = ALLT(xpt.ship_ref)
			IF nChangedRecs > 10
				cMsgStr = "More than 10 records rolled up"
			ELSE
				cMsgStr = cMsgStr+CHR(13)+cShip_ref+" "+TRANSFORM(m.ptid)+" "+TRANSFORM(m.ptdetid)
			ENDIF
			nChangedRecs = nChangedRecs+1
			SELECT xptdet
			nTotqty1 = xptdet.totqty
			DELETE NEXT 1 IN xptdet
		ENDIF
		IF !EOF()
			GO nRecno
			REPLACE xptdet.totqty WITH xptdet.totqty+nTotqty1 IN xptdet
			REPLACE xptdet.origqty WITH xptdet.totqty IN xptdet
		ELSE
			GO nRecno
		ENDIF
	ENDSCAN

	IF nChangedRecs > 0
		WAIT WINDOW cMsgStr TIMEOUT 2
		WAIT WINDOW "There were "+TRANS(nChangedRecs)+" records rolled up" TIMEOUT 1
	ELSE
		WAIT WINDOW "There were NO records rolled up" TIMEOUT 1
	ENDIF

ENDPROC

