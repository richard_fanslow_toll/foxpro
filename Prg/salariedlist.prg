* this report is designed to be run at the beginning of each week
* It will give Lucille the list of current salaried employees

* export department list from ADP into Foxpro (for use by revtime.scx)

runack("SALARIEDLIST")

LOCAL loSalariedList
loSalariedList = CREATEOBJECT('SalariedList')
loSalariedList.MAIN()
CLOSE DATABASES ALL
RETURN

#DEFINE LOGIT 1
#DEFINE WAITIT 2
#DEFINE NOWAITIT 4
#DEFINE SENDIT 8
#DEFINE CRLF CHR(13) + CHR(10)
#DEFINE RETURN_DATA_MANDATORY .F.
#DEFINE RETURN_DATA_NOT_MANDATORY .T.
#DEFINE CSVFORMAT 6
#DEFINE WRITE_ONLY_UNBUFFERED 11
#DEFINE COMMA ","

DEFINE CLASS SalariedList AS CUSTOM

	cProcessName = 'SalariedList'

	lTestMode = .F.  && .F. FOR NORMAL OPERATION, .T. TO SUPPRESS ALL BUT DEVELOPER EMAILS AND CHANGE INPUT TABLE, ETC.

	lAutoYield = .T.

	cStartTime = TTOC(DATETIME())
	
	dToday = DATE()

	
	* connection properties
	nSQLHandle = 0

	* wait window properties
	nWaitWindowTimeout = 2
	lWaitWindowIsOn = .T.

	* logfile properties
	lLoggingIsOn = .T.
	cLogFile = 'f:\util\hr\salariedlist\Logfiles\SalariedList_log.txt'

	* INTERNAL email properties
	lSendInternalEmailIsOn = .T.
	cFrom = 'TGF Corporate <fmicorporate@fmiint.com>'
	cSendTo = 'lucille.waldrip@tollgroup.com'
	cCC = 'Mark Bennett <mbennett@fmiint.com>'
	cSubject = 'SALARIED LISTS, Results for: ' + TTOC(DATETIME())
	cAttach = ''
	cBodyText = ''

	cCOMPUTERNAME = ''
	cUSERNAME = ''

	FUNCTION INIT
		IF NOT DODEFAULT()
			RETURN .F.
		ENDIF
		WITH THIS
			*SET RESOURCE OFF
			CLOSE DATA
			SET CENTURY OFF
			SET DATE AMERICAN
			SET HOURS TO 24
			SET ANSI ON
			SET TALK OFF
			SET DELETED ON
			SET CONSOLE OFF
			SET EXCLUSIVE OFF
			SET SAFETY OFF
			SET EXACT OFF
			SET STATUS BAR OFF
			SET SYSMENU OFF
			SET ENGINEBEHAVIOR 70
			_VFP.AUTOYIELD = .lAutoYield
			.cCOMPUTERNAME = GETENV("COMPUTERNAME")
			.cUSERNAME = GETENV("USERNAME")
			IF .lTestMode THEN
				.cSendTo = 'Mark Bennett <mbennett@fmiint.com>'
				.cLogFile = 'f:\util\hr\salariedlist\Logfiles\SalariedList_log_TESTMODE.txt'
			ENDIF
			.lLoggingIsOn = .lLoggingIsOn AND NOT EMPTY(.cLogFile)
			IF .lLoggingIsOn THEN
				SET ALTERNATE TO (.cLogFile) ADDITIVE
				SET ALTERNATE ON
			ENDIF
		ENDWITH
	ENDFUNC


	FUNCTION DESTROY
		WITH THIS
			IF .lLoggingIsOn  THEN
				SET ALTERNATE OFF
				SET ALTERNATE TO
			ENDIF
			SET STATUS BAR ON
		ENDWITH
		DODEFAULT()
	ENDFUNC


	FUNCTION MAIN
		WITH THIS
			LOCAL lnNumberOfErrors, lcSQL, loError, llNewRecord, lcInsert, lcErrorMsg
			LOCAL lcNewFoxProDepartmentTable, lcOldFoxProDepartmentTable, lcDivnameCompanyLookupTable
			LOCAL lcDeptWhere, lcOutputFile, lnRecs, lcComputer, ldToday, lcToday
			TRY
				lnNumberOfErrors = 0
				
				lcComputer = .cCOMPUTERNAME

				.TrackProgress('', LOGIT)
				.TrackProgress('', LOGIT)
				.TrackProgress('=========================================================', LOGIT+SENDIT)
				.TrackProgress('SALARIED LIST process started....', LOGIT+SENDIT+NOWAITIT)
				.TrackProgress('COMPUTERNAME = ' + .cCOMPUTERNAME, LOGIT+SENDIT)
				.TrackProgress('USERNAME = ' + .cUSERNAME, LOGIT+SENDIT)
				.TrackProgress('=========================================================', LOGIT+SENDIT)
				IF .lTestMode THEN
					.TrackProgress('', LOGIT)
					.TrackProgress('=========================================================', LOGIT+SENDIT)
					.TrackProgress('                        TEST MODE                        ', LOGIT+SENDIT)
					.TrackProgress('=========================================================', LOGIT+SENDIT)
				ENDIF
				
				ldToday = .dToday
				lcToday = STRTRAN( TRANSFORM( ldToday ), "/","-" )

				* get ss#s from ADP
				OPEN DATABASE F:\UTIL\ADPCONNECTION\CONNECT1

				.nSQLHandle = SQLCONNECT("PCPAYWIN","MBENNETT","VP737A")

				IF .nSQLHandle > 0 THEN

				lcSQL = ;
					"SELECT " + ;
					" COMPANYCODE AS ADP_COMP, " + ;
					" {fn IFNULL({fn LEFT(HOMEDEPARTMENT,2)},'??')} AS DIVISION, " + ;
					" {fn IFNULL({fn SUBSTRING(HOMEDEPARTMENT,3,4)},'????')} AS DEPT, " + ;
					" {fn IFNULL(FILE#,'0000')} AS EMP_NUM, " + ;
					" {fn IFNULL(NAME,'')} AS EMPLOYEE " + ;
					" FROM REPORTS.V_EMPLOYEE WHERE STATUS <> 'T' " + ;
					" AND COMPANYCODE IN ('E88','E89') "


*!*	SELECT adp_comp, division, dept, emp_num, employee ;
*!*		FROM F:\HR\employee ;
*!*		INTO CURSOR curSalaried ;
*!*		WHERE adp_comp = "ZXU" ;
*!*		AND STATUS <> 'T' ;
*!*		ORDER BY adp_comp, division, dept, emp_num

					IF USED('SQLCURSOR1') THEN
						USE IN SQLCURSOR1
					ENDIF

					IF .ExecSQL(lcSQL, 'SQLCURSOR1', RETURN_DATA_MANDATORY) THEN

						IF NOT USED('SQLCURSOR1') OR EOF('SQLCURSOR1') THEN
							.TrackProgress('There were no ADP Departments to export!', LOGIT+SENDIT+NOWAITIT)
						ELSE

							************************* MAIN PROCESSING FOLLOWS ***********************************




							************************* END OF MAIN PROCESSING ***********************************

						ENDIF   &&  NOT USED('SQLCURSOR1') OR EOF('SQLCURSOR1')

						*CLOSE DATABASES ALL
					ENDIF  &&  .ExecSQL(lcSQL, 'SQLCURSOR1', RETURN_DATA_MANDATORY)

					.TrackProgress('', LOGIT+SENDIT)
					.TrackProgress('UPDATE FOXPRO DEPARTMENTS process ended normally.', LOGIT+SENDIT+NOWAITIT)

				ELSE
					* connection error
					.TrackProgress('Unable to connect to ADP Payroll System.', LOGIT+SENDIT)
				ENDIF   &&  .nSQLHandle > 0

			CATCH TO loError

				.TrackProgress('There was an error.',LOGIT+SENDIT)
				.TrackProgress('The Error # is: ' + TRANSFORM(loError.ErrorNo), LOGIT+SENDIT)
				.TrackProgress('The Error Message is: ' + TRANSFORM(loError.MESSAGE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred in Program: ' + TRANSFORM(loError.PROCEDURE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred at line #: ' + TRANSFORM(loError.LINENO), LOGIT+SENDIT)
				lnNumberOfErrors = lnNumberOfErrors + 1
				CLOSE DATA

			ENDTRY

			CLOSE DATA
			WAIT CLEAR
			***************** INTERNAL email results ******************************
			.TrackProgress('About to send status email.',LOGIT)
			.TrackProgress('The logfile is: ' + .cLogFile, SENDIT)
			.TrackProgress('SALARIED LIST process started: ' + .cStartTime, LOGIT+SENDIT)
			.TrackProgress('SALARIED LIST DEPARTMENTS process finished: ' + TTOC(DATETIME()), LOGIT+SENDIT)


			IF .lSendInternalEmailIsOn THEN
				* try to trap error from not having dartmail dll's registered on user's pc...
				TRY
					* E88
					IF USED('curSalaried') THEN
						USE IN curSalaried
					ENDIF

					lcOutputFile = "F:\UTIL\HR\SALARIEDLIST\REPORTS\SALARIED_LIST_E88_"  + lcToday +  ".XLS"
					
					IF FILE(lcOutputFile) THEN
						DELETE FILE (lcOutputFile)
					ENDIF
					
					SELECT * FROM SQLCURSOR1 INTO CURSOR curSalaried WHERE ADP_COMP = 'E88' ORDER BY 1, 2, 3, 4
					
					SELECT curSalaried
					lnRecs = RECCOUNT()
					COPY TO (lcOutputFile) XL5

					.cSubject = "List of E88 Salaried Employees, count = " + TRANSFORM(lnRecs)
					.cAttach = lcOutputFile
					.cBodyText = "The attached spreadsheet lists all current employees for company = 'E88.'" + CRLF + CRLF + ;
						"( sent from: " + lcComputer + " )"
					DO FORM dartmail2 WITH .cSendTo,.cFrom,.cSubject,.cCC,.cAttach,.cBodyText,"A"
					.TrackProgress('Sent status email.',LOGIT)
					
					* E89
					IF USED('curSalaried') THEN
						USE IN curSalaried
					ENDIF

					lcOutputFile = "F:\UTIL\HR\SALARIEDLIST\REPORTS\SALARIED_LIST_E89_"  + lcToday +  ".XLS"
					
					IF FILE(lcOutputFile) THEN
						DELETE FILE (lcOutputFile)
					ENDIF
					
					SELECT * FROM SQLCURSOR1 INTO CURSOR curSalaried WHERE ADP_COMP = 'E89' ORDER BY 1, 2, 3, 4
					
					SELECT curSalaried
					lnRecs = RECCOUNT()
					COPY TO (lcOutputFile) XL5

					.cSubject = "List of E89 Salaried Employees, count = " + TRANSFORM(lnRecs)
					.cAttach = lcOutputFile
					.cBodyText = "The attached spreadsheet lists all current employees for company = 'E89.'" + CRLF + CRLF + ;
						"( sent from: " + lcComputer + " )"
					DO FORM dartmail2 WITH .cSendTo,.cFrom,.cSubject,.cCC,.cAttach,.cBodyText,"A"
					.TrackProgress('Sent status email.',LOGIT)
					
				CATCH TO loError
					*=MESSAGEBOX('There was an error sending the status email. Please contact Mark Bennett.',0+64,"EXPORT ADP DEPARTMENT LIST TO FOXPRO")
					.TrackProgress('There was an error sending the status email.',LOGIT)
					.TrackProgress('The Error # is: ' + TRANSFORM(loError.ErrorNo), LOGIT)
					.TrackProgress('The Error Message is: ' + TRANSFORM(loError.MESSAGE), LOGIT)
					.TrackProgress('The Error occurred in Program: ' + TRANSFORM(loError.PROCEDURE), LOGIT)
					.TrackProgress('The Error occurred at line #: ' + TRANSFORM(loError.LINENO), LOGIT)
					lnNumberOfErrors = lnNumberOfErrors + 1
					CLOSE DATA
				ENDTRY
			ELSE
				.TrackProgress('Did not send status email: THIS.lSendInternalEmailIsOn = FALSE.',LOGIT)
			ENDIF
			
			CLOSE DATABASES ALL

		ENDWITH
		RETURN
	ENDFUNC && main


	FUNCTION ExecSQL
		LPARAMETERS tcSQL, tcCursorName, tlNoDataReturnedIsOkay
		LOCAL llRetval, lnResult
		WITH THIS
			* close target cursor if it's open
			IF USED(tcCursorName)
				USE IN (tcCursorName)
			ENDIF
			WAIT WINDOW "Fetching remote data; please wait..." NOWAIT
			lnResult = sqlexec(.nSQLHandle, tcSQL, tcCursorName)
			llRetval = ( lnResult > 0 )
			IF llRetval THEN
				* see if any data came back
				IF NOT tlNoDataReturnedIsOkay THEN
					IF (NOT USED(tcCursorName)) OR EOF(tcCursorName) THEN
						llRetval = .F.
						.TrackProgress('No data was returned by this query: ' +CRLF + tcSQL, LOGIT+SENDIT)
					ENDIF
				ENDIF
			ELSE
				.TrackProgress("Result Code [" + TRANSFORM(lnResult) + "] returned by this query: " + tcSQL, LOGIT+SENDIT)
			ENDIF
			WAIT CLEAR
			RETURN llRetval
		ENDWITH
	ENDFUNC


	PROCEDURE TrackProgress
		* do any combination of Wait Window, writing to logfile, and adding to body of email,
		* based on nFlags parameter.
		LPARAMETERS tcExpression, tnFlags
		WITH THIS
			IF BITAND(tnFlags,LOGIT) = LOGIT THEN
				IF .lLoggingIsOn THEN
					?
					? .cProcessName + "  " + TTOC(DATETIME()) + ": " + tcExpression
				ENDIF
			ENDIF
			IF .lWaitWindowIsOn THEN
				IF BITAND(tnFlags,NOWAITIT) = NOWAITIT THEN
					WAIT WINDOW tcExpression NOWAIT
				ENDIF
				IF BITAND(tnFlags,WAITIT) = WAITIT THEN
					WAIT WINDOW tcExpression TIMEOUT .nWaitWindowTimeout
				ENDIF
			ENDIF
			IF BITAND(tnFlags,SENDIT) = SENDIT THEN
				IF .lSendInternalEmailIsOn THEN
					.cBodyText = .cBodyText + tcExpression + CRLF + CRLF
				ENDIF
			ENDIF
		ENDWITH
	ENDPROC  &&  TrackProgress

ENDDEFINE







*!*	***********************
*!*	#DEFINE CRLF CHR(13) + CHR(10)

*!*	SET EXCLUSIVE OFF
*!*	SET SAFETY OFF
*!*	SET DELETED ON

*!*	LOCAL 

*!*	lcComputer = GETENV("COMPUTERNAME")

*!*	* ZXU

*!*	IF USED('curSalaried') THEN
*!*		USE IN curSalaried
*!*	ENDIF

*!*	IF FILE(lcOutputFile) THEN
*!*		DELETE FILE (lcOutputFile)
*!*	ENDIF

*!*	SELECT adp_comp, division, dept, emp_num, employee ;
*!*		FROM F:\HR\employee ;
*!*		INTO CURSOR curSalaried ;
*!*		WHERE adp_comp = "ZXU" ;
*!*		AND STATUS <> 'T' ;
*!*		ORDER BY adp_comp, division, dept, emp_num

*!*	SELECT curSalaried
*!*	lnRecs = RECCOUNT()
*!*	COPY TO (lcOutputFile) XL5

*!*	lcSubject = "List of Salaried Employees, count = " + TRANSFORM(lnRecs)
*!*	lcCC = "Mark Bennett <mbennett@fmiint.com>"
*!*	lcAttach = lcOutputFile
*!*	lcBodyText = "The attached spreadsheet lists all current employees for company = 'ZXU.'" + CRLF + CRLF + ;
*!*		"( sent from: " + lcComputer + " )"

*!*	DO FORM F:\MAIL\FORMS\dartmail2 WITH lcSendTo, lcFrom, lcSubject, lcCC, lcAttach, lcBodyText, "A"

*!*	***********************************************************************************************************
*!*	* AXA
*!*	lcOutputFile = "F:\UTIL\HR\SALARIEDLIST\REPORTS\SALARIED_LIST_AXA.XLS"

*!*	IF USED('curSalaried') THEN
*!*		USE IN curSalaried
*!*	ENDIF

*!*	IF FILE(lcOutputFile) THEN
*!*		DELETE FILE (lcOutputFile)
*!*	ENDIF

*!*	SELECT adp_comp, division, dept, emp_num, employee ;
*!*		FROM F:\HR\employee ;
*!*		INTO CURSOR curSalaried ;
*!*		WHERE adp_comp = "AXA" ;
*!*		AND STATUS <> 'T' ;
*!*		ORDER BY adp_comp, division, dept, emp_num

*!*	SELECT curSalaried
*!*	lnRecs = RECCOUNT()
*!*	COPY TO (lcOutputFile) XL5

*!*	lcSendTo = "Lucille Waldrip <lwaldrip@fmiint.com>"
*!*	*lcSendTo = "Mark Bennett <mbennett@fmiint.com>"
*!*	lcFrom = "FMI Corporate <fmicorporate@fmiint.com>"
*!*	lcSubject = "List of Salaried Employees, count = " + TRANSFORM(lnRecs)
*!*	lcCC = "Mark Bennett <mbennett@fmiint.com>"
*!*	lcAttach = lcOutputFile
*!*	lcBodyText = "The attached spreadsheet lists all current employees for company = 'AXA.'" + CRLF + CRLF + ;
*!*		"( sent from: " + lcComputer + " )"

*!*	DO FORM F:\MAIL\FORMS\dartmail2 WITH lcSendTo, lcFrom, lcSubject, lcCC, lcAttach, lcBodyText, "A"


*!*	CLOSE ALL
