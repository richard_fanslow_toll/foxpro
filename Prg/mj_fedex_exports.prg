* This program extracts previous days fedex exports
utilsetup("MJ_FEDEX_EXPORTS")
SET EXCLUSIVE OFF 
SET DELETED ON 
SET TALK OFF  
SET SAFETY OFF   &&Allows for overwrite of existing files
WITH _SCREEN	
	.AUTOCENTER = .T.
	.WINDOWSTATE = 0
	.BORDERSTYLE = 1
	.WIDTH = 320
	.HEIGHT = 210
	.TOP = 290
	.LEFT = 110
	.CLOSABLE = .T.
	.MAXBUTTON = .t.
	.MINBUTTON = .T.
	.SCROLLBARS = 0
	.CAPTION = "MJ_FEDEX_EXPORTS"
ENDWITH	

close databases all
schedupdate()

goffice="J"

xsqlexec("select del_date, accountid, ship_ref from outship where mod='J' and wo_date>{"+dtoc(date()-60)+"}",,,"wh")
index on ship_ref tag ship_ref
set order to

If !Used("shipment")
	xsqlexec("select * from shipment where mod='"+goffice+"' and inlist(country,'250','380','826') and carrier='FDX'",,,"wh")
ENDIF
If !Used("package")
	xsqlexec("select * from package where mod='"+goffice+"'",,,"wh")
ENDIF
If !Used("account")
xsqlexec("select * from account where inactive=0","account",,"qq")
index on accountid tag accountid
set order to
endif

SET STEP ON 
*Wait window at 10,10 "Performing the first query...." nowait
SELECT TRKNUMBER,WO_NUM,PICKTICKET,COMPANY,ADDRESS,ADDRESS2,CITY,STATE,ZIP FROM SHIPMENT A,;
 PACKAGE B WHERE A.SHIPMENTID=B.SHIPMENTID AND INLIST(COUNTRY,'250','380','826') AND ;
 CARRIER ='FDX' INTO CURSOR TEMP96
 
SELECT del_date, accountid, SPACE(30) as acctn, temp96.* FROM temp96 LEFT JOIN outship ON pickticket=ship_ref INTO CURSOR temp97 READWRITE
SELECT * FROM temp97 WHERE del_date=(date()-1) INTO CURSOR temp98 READWRITE
SELECT acctname, temp98.* FROM temp98 LEFT JOIN account ON temp98.accountid=account.accountid INTO CURSOR temp99 READWRITE 
 
	If Reccount() > 0 
		export TO "S:\MarcJacobsData\temp\fedex_exports"  TYPE xls
		tsendto = "v.lopez@marcjacobs.com,tmarg@fmiint.com"
		tattach = "S:\MarcJacobsData\temp\fedex_exports.xls" 
		tcc =""                                                                                                                               
		tFrom ="Toll EDI System Operations <fmi-transload-ops@fmiint.com>"                                
		tmessage = "Fedex Exports complete:  "+Ttoc(Datetime())        
		tSubject = "Fedex Exports complete"                                                           
		Do Form m:\dev\frm\dartmail2 With tsendto,tFrom,tSubject, tcc,tattach,tmessage  ,"A"     
			
	ELSE 
		tsendto = "v.lopez@marcjacobs.com,tmarg@fmiint.com"
		tattach = "" 
		tcc =""                                                                                                                               
		tFrom ="Toll EDI System Operations <fmi-transload-ops@fmiint.com>"                                
		tmessage = "NO fedex exports for:  "+Ttoc(Datetime())        
		tSubject = "NO fedex exports exists"                                                           
		Do Form m:\dev\frm\dartmail2 With tsendto,tFrom,tSubject, tcc,tattach,tmessage  ,"A"   

	ENDIF


Wait window at 10,10 "all done...." timeout 2

_Screen.Caption=gscreencaption