* PROCESS NAME:  SYNCLAIRE_OUTBOUND_RPT
*
* DESCRIPTION: reports miscellaneous data from OUTBOUND work orders for prior month
*
* Supports Synclaire and BCNY, based on passed parameter 'S' or 'B'
*
* build EXE as: F:\AUTO\SYNCLAIRE_OUTBOUND_RPT.EXE
*
* s.b. scheduled to run on day 1 of each month
*
* Adapted to run as auto exe MB 12/08/2017 - old code written by PG is at bottom, commented out.

LPARAMETERS tcClientCode

LOCAL loSYNCLAIRE_OUTBOUND_RPT, llTestMode

llTestMode = .F.

IF NOT llTestMode THEN
	utilsetup("SYNCLAIRE_OUTBOUND_RPT")
ENDIF

loSYNCLAIRE_OUTBOUND_RPT = CREATEOBJECT('SYNCLAIRE_OUTBOUND_RPT')

loSYNCLAIRE_OUTBOUND_RPT.MAIN( llTestMode, tcClientCode )

CLOSE DATABASES ALL
RETURN

#DEFINE LOGIT 1
#DEFINE WAITIT 2
#DEFINE NOWAITIT 4
#DEFINE SENDIT 8
#DEFINE CRLF CHR(13) + CHR(10)
#DEFINE LF CHR(10)
#DEFINE RETURN_DATA_MANDATORY .F.
#DEFINE RETURN_DATA_NOT_MANDATORY .T.

DEFINE CLASS SYNCLAIRE_OUTBOUND_RPT AS CUSTOM

	cProcessName = 'SYNCLAIRE_OUTBOUND_RPT'

	lTestMode = .F.  && .F. FOR NORMAL OPERATION, .T. TO SUPPRESS ALL BUT DEVELOPER EMAILS AND CHANGE INPUT TABLE, ETC.

	lAutoYield = .T.

	cStartTime = TTOC(DATETIME())

	* date properties
	dtNow = DATETIME()

	dToday = DATE()

	* wait window properties
	nWaitWindowTimeout = 2
	lWaitWindowIsOn = .T.

	* logfile properties
	lLoggingIsOn = .T.
	cLogFile = 'F:\UTIL\3PL\Logfiles\SYNCLAIRE_OUTBOUND_RPT_log.txt'

	* folder properties

	* process flow properties
	cClientCode = ''
	cClientName = ''
	nAccountID = 0

	* INTERNAL email properties
	lSendInternalEmailIsOn = .T.
	cFrom = "TGF EDI Ops <fmicorporate@fmiint.com>"
	cSendTo = 'mbennett@fmiint.com'
	cCC = ''
	cSubject = ''
	cAttach = ''
	cBodyText = ''

	cCOMPUTERNAME = ''
	cUSERNAME = ''

	FUNCTION INIT
		IF NOT DODEFAULT()
			RETURN .F.
		ENDIF
		WITH THIS
			CLEAR
			IF _VFP.STARTMODE > 0 THEN
				SET RESOURCE OFF
			ENDIF
			CLOSE DATA
			SET CENTURY ON
			SET DATE AMERICAN
			SET HOURS TO 24
			SET ANSI ON
			SET TALK OFF
			SET DELETED ON
			SET CONSOLE OFF
			SET EXCLUSIVE OFF
			SET SAFETY OFF
			SET EXACT OFF
			SET STATUS BAR ON
			SET SYSMENU OFF
			SET ENGINEBEHAVIOR 70
			_VFP.AUTOYIELD = .lAutoYield
			.cCOMPUTERNAME = UPPER(ALLTRIM(GETENV("COMPUTERNAME")))
			.cUSERNAME = GETENV("USERNAME")
			IF .lTestMode THEN
				.cSendTo = 'mbennett@fmiint.com'
				.cLogFile = 'F:\UTIL\3PL\Logfiles\SYNCLAIRE_OUTBOUND_RPT_log_TESTMODE.txt'
			ENDIF
			.lLoggingIsOn = .lLoggingIsOn AND NOT EMPTY(.cLogFile)
			IF .lLoggingIsOn THEN
				SET ALTERNATE TO (.cLogFile) ADDITIVE
				SET ALTERNATE ON
			ENDIF
			.cSubject = .cProcessName + ' process for: ' + DTOC(DATE())
		ENDWITH
	ENDFUNC


	FUNCTION DESTROY
		WITH THIS
			IF .lLoggingIsOn  THEN
				SET ALTERNATE OFF
				SET ALTERNATE TO
			ENDIF
		ENDWITH
		DODEFAULT()
	ENDFUNC


	FUNCTION MAIN
		LPARAMETERS tlTestMode, tcClientCode
		WITH THIS
			LOCAL i, lnNumberOfErrors, lcOutputFile, ldToday, ldStartDate, ldEndDate, lcOutputFolder, lcDSNLess, lnHandle, lcServer, lcSqlPassword, lnOK
			LOCAL lnCount, lcQuery

			TRY
				lnNumberOfErrors = 0

				.TrackProgress(.cProcessName + " process started....", LOGIT+SENDIT+NOWAITIT)
				.TrackProgress('COMPUTERNAME = ' + .cCOMPUTERNAME, LOGIT+SENDIT)
				.TrackProgress('USERNAME = ' + .cUSERNAME, LOGIT+SENDIT)
				.TrackProgress('PROJECT = SYNCLAIRE_OUTBOUND_RPT', LOGIT+SENDIT)

				.lTestMode = tlTestMode

				IF .lTestMode THEN
					.TrackProgress('====> TEST MODE', LOGIT+SENDIT)
				ENDIF

				.cClientCode = UPPER(ALLTRIM(tcClientCode))

				DO CASE
					CASE .cClientCode == 'B'
						.nAccountID = 6221
						.cClientName = 'BCNY'
						IF .lTestMode THEN
							lcOutputFolder = 'F:\UTIL\SYNCLAIRE\TESTREPORTS\'
						ELSE
							lcOutputFolder = 'F:\FTPUSERS\BCNY\OUTBOUNDREPORT\'
						ENDIF
					CASE .cClientCode == 'S'
						.nAccountID = 6521
						.cClientName = 'SYNCLAIRE'
						IF .lTestMode THEN
							lcOutputFolder = 'F:\UTIL\SYNCLAIRE\TESTREPORTS\'
						ELSE
							lcOutputFolder = 'F:\FTPUSERS\SYNCLAIRE\OUTBOUNDREPORT\'
						ENDIF
				ENDCASE

				.TrackProgress('.cClientName = ' + .cClientName, LOGIT+SENDIT)

				ldToday = DATE()

				ldEndDate = ldToday - DAY(ldToday)

				ldStartDate = GOMONTH(ldEndDate + 1, -1)

				.TrackProgress('ldStartDate = ' + DTOC(ldStartDate), LOGIT+SENDIT)

				.TrackProgress('ldEndDate = ' + DTOC(ldEndDate), LOGIT+SENDIT)

				.cSubject = .cProcessName + ' process for ' + .cClientName + ' for : ' + DTOC(ldToday)

				lcOutputFile = lcOutputFolder + .cClientName + "_OUTBOUND_REPORT_" + DTOS(ldStartDate) + ".CSV"  && Synclaire has too many rows for XLS

				.TrackProgress('lcOutputFile = ' + lcOutputFile, LOGIT+SENDIT)

				**************************************************************************************************

				DO m:\dev\prg\lookups
				SET DECIMALS TO 0

				PUBLIC gsystemmodule, goffice
				gsystemmodule ="wh"
				goffice = "2"

				USE F:\wh\sqlpassword IN 0
				lcSqlPassword = ALLTRIM(sqlpassword.PASSWORD)
				USE IN sqlpassword

				lcServer = "tgfnjsql01"
				SQLSETPROP(0,'DispLogin',3)
				SQLSETPROP(0,"dispwarnings",.F.)

				lcDSNLess = "driver=SQL Server;server=&lcServer;uid=SA;pwd=&lcSqlPassword;DATABASE=PICKPACK"
				lnHandle = SQLSTRINGCONNECT(m.lcDSNLess,.T.)
				IF lnHandle<=0 THEN&& bailout
					.TrackProgress('====> ERROR: Could not make connection to tgfnjsql01 SQL server: ' + lcDSNLess, LOGIT+SENDIT)
					THROW
				ENDIF

				*
				xsqlexec("select wo_num,ship_ref,del_date,consignee,ctnqty,qty,cuft,outshipid from outship where accountid = " + TRANSFORM(.nAccountID),,,"wh")

				xsqlexec("select outshipid,outdetid,upc,totqty from outdet where accountid = " + TRANSFORM(.nAccountID),,,"wh")

				*set step On
				CREATE CURSOR outdata (;
					del_date d,;
					outshipid INT,;
					consignee CHAR(30),;
					ship_ref  CHAR(20),;
					UPC CHAR(13),;
					ucc CHAR(20),;
					ctnqty N(6),;
					qty N(6),;
					qtypercase CHAR(6),;
					cuft N(6,2))

				SELECT outship
				INDEX ON del_date TAG del
				INDEX ON outshipid TAG outshipid
				INDEX ON MONTH(del_date) TAG mdel
				
				SELECT outdet
				INDEX ON outshipid TAG outshipid


				SELECT outship
				SET ORDER TO del
				lnCount = 0

				*SCAN FOR !ISNULL(del_date) AND MONTH(del_date) = 11
				SCAN FOR !ISNULL(del_date) AND (del_date >= ldStartDate) AND (del_date <= ldEndDate)
					SELECT outship
					WAIT WINDOW AT 10,10 "doing PT: " + outship.ship_ref+" dated: " + DTOS(outship.del_date)+ "  " + TRANSFORM(lnCount) NOWAIT
					SCATTER MEMVAR
					INSERT INTO outdata FROM MEMVAR
					SELECT outdet
					SCAN FOR outshipid = outship.outshipid
						SCATTER MEMVAR FIELDS UPC,totqty
						INSERT INTO outdata (ship_ref,UPC,qty,del_date,consignee,outshipid) VALUES;
							(outship.ship_ref,outdet.UPC,outdet.totqty,outship.del_date,outship.consignee,outship.outshipid)
					ENDSCAN
					m.UPC =""

					if uesql()
						xsqlexec("select sum(totqty) as lnunits, ucc from cartons where ship_ref='"+outship.ship_ref+"' group by ucc","result",,"pickpack")
					else
						lcQuery = "select Sum(totqty) as lnunits,UCC from cartons where ship_ref = '" + outship.ship_ref + " ' group by ucc"
						lnOK = SQLEXEC(lnHandle,lcQuery,"result")
					endif

					IF RECCOUNT("result") > 0 THEN
						SELECT result
						SCAN
							SCATTER MEMVAR FIELDS ucc,lnunits
							INSERT INTO outdata (ship_ref,ucc,qtypercase,del_date,consignee,outshipid) VALUES;
								(outship.ship_ref, result.ucc, TRANSFORM(result.lnunits),outship.del_date,outship.consignee,outship.outshipid)

						ENDSCAN
						m.ucc =""
						m.qtypercase =""
					ENDIF
					lnCount = lnCount+1
				ENDSCAN


				SELECT outdata
				COPY TO (lcOutputFile) CSV

				IF FILE(lcOutputFile) THEN
					.TrackProgress('=====>  Output file created: ' + lcOutputFile, LOGIT+SENDIT+NOWAITIT)
				ELSE
					.TrackProgress('=====>  ERROR: output file not found: ' + lcOutputFile, LOGIT+SENDIT+NOWAITIT)
					THROW
				ENDIF

				IF NOT .lTestMode THEN
					schedupdate()
				ENDIF

				**************************************************************************************************

				.TrackProgress(.cProcessName + ' process ended normally', LOGIT+SENDIT+NOWAITIT)

			CATCH TO loError

				.TrackProgress('There was an error.',LOGIT+SENDIT)
				.TrackProgress('The Error # is: ' + TRANSFORM(loError.ERRORNO), LOGIT+SENDIT)
				.TrackProgress('The Error Message is: ' + TRANSFORM(loError.MESSAGE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred in Program: ' + TRANSFORM(loError.PROCEDURE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred at line #: ' + TRANSFORM(loError.LINENO), LOGIT+SENDIT)
				lnNumberOfErrors = lnNumberOfErrors + 1
				IF TYPE('oExcel') = 'O' AND NOT ISNULL(oExcel) THEN
					oExcel.QUIT()
				ENDIF
				CLOSE DATA

			ENDTRY

			CLOSE DATA
			WAIT CLEAR
			***************** INTERNAL email results ******************************
			.TrackProgress('About to send status email.',LOGIT)
			.TrackProgress('--', SENDIT)
			.TrackProgress('The logfile is: ' + .cLogFile, SENDIT)
			.TrackProgress(.cProcessName + ' process started: ' + .cStartTime, LOGIT+SENDIT)
			.TrackProgress(.cProcessName + ' process finished: ' + TTOC(DATETIME()), LOGIT+SENDIT)

			IF .lSendInternalEmailIsOn THEN
				* try to trap error from not having dartmail dll's registered on user's pc...
				TRY
					DO FORM dartmail2 WITH .cSendTo,.cFrom,.cSubject,.cCC,.cAttach,.cBodyText,"A"
					.TrackProgress('Sent status email.',LOGIT)
				CATCH TO loError
					*=MESSAGEBOX('There was an error sending the status email. Please contact Mark Bennett.',0+64,"KRONOS Temp Time")
					.TrackProgress('There was an error sending the status email.',LOGIT)
					.TrackProgress('The Error # is: ' + TRANSFORM(loError.ERRORNO), LOGIT)
					.TrackProgress('The Error Message is: ' + TRANSFORM(loError.MESSAGE), LOGIT)
					.TrackProgress('The Error occurred in Program: ' + TRANSFORM(loError.PROCEDURE), LOGIT)
					.TrackProgress('The Error occurred at line #: ' + TRANSFORM(loError.LINENO), LOGIT)
					lnNumberOfErrors = lnNumberOfErrors + 1
				ENDTRY

			ELSE
				.TrackProgress('Did not send status email: THIS.lSendInternalEmailIsOn = FALSE.',LOGIT)
			ENDIF

		ENDWITH
		RETURN
	ENDFUNC && main


	PROCEDURE TrackProgress
		* do any combination of Wait Window, writing to logfile, and adding to body of email,
		* based on nFlags parameter.
		LPARAMETERS tcExpression, tnFlags
		WITH THIS
			IF BITAND(tnFlags,LOGIT) = LOGIT THEN
				IF .lLoggingIsOn THEN
					?
					? .cProcessName + "  " + TTOC(DATETIME()) + ": " + tcExpression
				ENDIF
			ENDIF
			IF .lWaitWindowIsOn THEN
				IF BITAND(tnFlags,NOWAITIT) = NOWAITIT THEN
					WAIT WINDOW tcExpression NOWAIT
				ENDIF
				IF BITAND(tnFlags,WAITIT) = WAITIT THEN
					WAIT WINDOW tcExpression TIMEOUT .nWaitWindowTimeout
				ENDIF
			ENDIF
			IF BITAND(tnFlags,SENDIT) = SENDIT THEN
				IF .lSendInternalEmailIsOn THEN
					.cBodyText = .cBodyText + tcExpression + CRLF + CRLF
				ENDIF
			ENDIF
		ENDWITH
	ENDPROC  &&  TrackProgress

ENDDEFINE
