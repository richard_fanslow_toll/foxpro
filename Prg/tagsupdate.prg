close data all 
Set Step On 
Use h:\fox\mj_both in 0
Use h:\fox\mj_toll In 0


Select mj_both
Scan
  Select mj_toll
  Locate for style = mj_both.mj_style and color = mj_both.mj_color and id = mj_both.mj_id
  If Found()
    replace mj_both.toll_total with mj_toll.sum_qty
  EndIf   

EndScan


*********************************************************************************

close data all 
Create Cursor pixmaster (;
  type c(5),;
  code c(6),;
  div c(5),;
  style Char(20),;
  color char(10),;
  label c(20),;
  dim c(10),;
  size char(10),;
  upc char(20),;
  qty n(10),;
  adj1 c(10),;
  transdt c(10),;
  location c(10),;
  shipnum c(10),;
  ponum char(5),;
  adj2  char(10),;
  whseloc char(10))

*Append From h:\fox\test.txt delimited with character "|"
Append From h:\fox\tollpixfile.txt type csv
*Append From h:\fox\mjorig.csv type csv

*********************************************************************************


close data all 
Create Cursor invenmaster (;
  sku Char(20),;
  upc char(20),;
  style char(20),;
  desc char(50),;
  color char(10),;
  id char(10),;
  tag char(4),;
  qty char(6))

*Append From h:\fox\test.txt delimited with character "|"
Append From h:\fox\itemsummarywh.csv type csv
*Append From h:\fox\mjorig.csv type csv


Set Step On
Close Data All
Set DELETED on
Use F:\whj\whdata\WHSELOC In 0
Use F:\wh\TAGS In 0

Select Substr(WHSELOC,1,3) As WHROW,Count(1) As LOCCTR From WHSELOC Group By WHROW Into Cursor whselocs

Select Substr(WHSELOC,1,3) As Row,Count(1) As TAGCTR, 0000 As sysctr,0000 As Diff From TAGS Group By Row Into Cursor taglocs Readwrite


*USE H:\FOX\WHSELOCS In 0
*USE H:\FOX\TAGLOCS IN 0

Select taglocs
Scan
  Select whselocs
  Locate For whselocs.WHROW = taglocs.Row
  If Found()
    Replace taglocs.sysctr With whselocs.LOCCTR
  Else
    Replace taglocs.sysctr With 999999
  Endif

  Replace taglocs.Diff With taglocs.sysctr - taglocs.TAGCTR
Endscan
Browse
Export to h:\fox\unscanned_tags.xls type xls
Return

************************************************************************************************


Close Data All
Use F:\whj\whdata\WHSELOC In 0
Use F:\wh\TAGS In 0
Create Cursor locs (;
  rowvalue Char(3))

Insert Into locs (rowvalue) Values ("2AA")
Insert Into locs (rowvalue) Values ("2BB")
Insert Into locs (rowvalue) Values ("2CC")
Insert Into locs (rowvalue) Values ("2DD")
Insert Into locs (rowvalue) Values ("2EE")
Insert Into locs (rowvalue) Values ("2FF")
Insert Into locs (rowvalue) Values ("2GG")
Insert Into locs (rowvalue) Values ("2H1")
Insert Into locs (rowvalue) Values ("2H2")
Insert Into locs (rowvalue) Values ("2H3")
Insert Into locs (rowvalue) Values ("2H4")
Insert Into locs (rowvalue) Values ("2H5")
Insert Into locs (rowvalue) Values ("2HH")
Insert Into locs (rowvalue) Values ("2II")
Insert Into locs (rowvalue) Values ("2JJ")
Insert Into locs (rowvalue) Values ("2KK")
Insert Into locs (rowvalue) Values ("2LL")
Insert Into locs (rowvalue) Values ("2MM")
Insert Into locs (rowvalue) Values ("2NN")
Insert Into locs (rowvalue) Values ("2OO")
Insert Into locs (rowvalue) Values ("2PP")
Insert Into locs (rowvalue) Values ("2QQ")
Insert Into locs (rowvalue) Values ("2RR")
Insert Into locs (rowvalue) Values ("2S1")
Insert Into locs (rowvalue) Values ("2S2")
Insert Into locs (rowvalue) Values ("2S3")
Insert Into locs (rowvalue) Values ("2S4")
Insert Into locs (rowvalue) Values ("2SS")
Insert Into locs (rowvalue) Values ("2TT")
Insert Into locs (rowvalue) Values ("2UU")
Insert Into locs (rowvalue) Values ("2VA")
Insert Into locs (rowvalue) Values ("2VB")
Insert Into locs (rowvalue) Values ("2VC")
Insert Into locs (rowvalue) Values ("2VD")
Insert Into locs (rowvalue) Values ("2VV")
Insert Into locs (rowvalue) Values ("2WW")
Insert Into locs (rowvalue) Values ("2XX")
Insert Into locs (rowvalue) Values ("2YY")
Insert Into locs (rowvalue) Values ("2ZA")
Insert Into locs (rowvalue) Values ("2ZZ")
Insert Into locs (rowvalue) Values ("CAN")
Insert Into locs (rowvalue) Values ("CLG")
Insert Into locs (rowvalue) Values ("EMP")
Insert Into locs (rowvalue) Values ("NSC")
Insert Into locs (rowvalue) Values ("RET")
Insert Into locs (rowvalue) Values ("RGO")
Insert Into locs (rowvalue) Values ("RTG")
Insert Into locs (rowvalue) Values ("STA")




Select WHSELOC
Select WHSELOC,Space(10) As scantag From WHSELOC Where .F. Order By WHSELOC Into Cursor locsout Readwrite

Select locs
Scan

  rowvalue = locs.rowvalue
  Select WHSELOC
  Select WHSELOC,Space(10) As scantag From WHSELOC Where Substr(WHSELOC,1,3)= rowvalue Order By WHSELOC Into Cursor syslocs Readwrite

  Select TAGS
  Select * From TAGS Where Substr(WHSELOC,1,3)= rowvalue Order By WHSELOC Into Cursor taglocs

  Select syslocs
  Scan
    Select taglocs
    Locate For taglocs.WHSELOC = syslocs.WHSELOC
    If Found()
      Replace syslocs.scantag With taglocs.tagloc
    Else
      Replace syslocs.scantag With "NA"
    Endif

  Endscan
  Select syslocs
  Set Filter To Empty(scantag)
  Copy To h:\fox\syslocs
  Select locsout
  Append From h:\fox\syslocs
Endscan

Select locsout
filename = "h:\fox\unlinked.xls type xls"
Export To &filename
**************************************************************************************

close data all
*Set Step On 
*Use h:\fox\itemsummary In 0
*Use h:\fox\invenmaster In 0

Use h:\fox\mj_gave_us alias invenmaster In 0
Use f:\wh\tags In 0

Select invenmaster

Scan

  Select tags
  Locate for Alltrim(tags.tagloc) = Alltrim( invenmaster.tag)
  If Found()
    replace  invenmaster.whseloc with tags.whseloc
  Else
    replace  invenmaster.whseloc with "NA" in  invenmaster
  EndIf   

EndScan

Select tag FROM  invenmaster where whseloc="NA" order by tag group by tag
Copy To h:\fox\unlinked

*********************************************************************************

Set Escape on
Set Talk off
close data all
*Set Step On 
Use h:\fox\itemsummary In 0

goffice="J"

Use f:\wh\tags In 0

Select itemsummary
replace all wisqty with 0
replace all wmsqty with 0


Select whseloc from itemsummary where whseloc != "NA" group by whseloc into cursor wislocs 
totcount = Reccount("wislocs")
ii=1

Select wislocs
Scan
Wait window at 10,10 "Doing "+Transform(ii)+  "  out of "+Transform(totcount) nowait
ii=ii+1
	xsqlexec("select locqty from invenloc where mod='"+goffice+"' and whseloc='"+Alltrim(wislocs.whseloc)+"'",,,"wh")
  	sum locqty to tt for Alltrim(invenloc.whseloc) = Alltrim(wislocs.whseloc)
  
  Select itemsummary
  replace wmsqty with tt for itemsummary.whseloc = wislocs.whseloc

  Select itemsummary
  sum qty to tt for itemsummary.whseloc = wislocs.whseloc
  Select itemsummary
  replace wisqty with tt for itemsummary.whseloc = wislocs.whseloc
  
endscan
******************************************************************************************
close data all 
useca("phys","wh")

Use h:\fox\invenmaster In 0
*Set Step On 
Select invenmaster

select phys
scatter memvar memo blank

m.accountid = 6304
m.date_rcvd = Date()
m.units        = .t.
m.pack        = "1"
m.manual     = .t.
m.updateby  = "PAULG"
m.updatedt   = Date()
m.addby      = "PAULG"
m.adddt       = date() 
m.addproc   = "MANUAL"
m.office="N"
m.mod="J"

Scan
  m.tagno       = Val(invenmaster.tag)
  m.whseloc   = invenmaster.whseloc
  m.style        = invenmaster.style
  m.color        = invenmaster.color
  m.id             = invenmaster.id
  m.totqty       = invenmaster.qty
  m.physid     = dygenpk("phys","whall")
  Insert into phys from memvar
EndScan

tu("phys")  
  
  
*********************************************************************************************  
  
Set Talk off
close data all
Set Step On 
Use h:\fox\itemsummary In 0
Use h:\fox\unlinked In 0

Select itemsummary
Select * from itemsummary where .f. into cursor notyet readwrite

Select unlinked
Goto top
Scan
  Select itemsummary
  Scan for tag = unlinked.tag
    Select itemsummary
    Scatter memvar
    Select notyet
    Append Blank
    Gather memvar
  EndScan
EndScan
Select NOTYET
Export To h:\fox\unlinked_items.xls type xls
************************************************************************************