
*!* Intradeco 846 (Inventory Reporting) Creation
*!* Derived from the Nautica version, 02.09.16, Joe

PUBLIC cWhse,cFilename,cFilenameOut,cFilenameHold,tsendto,tcc,tsendtoerr,tccerr,tsendtotest,tcctest,lIsError,lJoeMail
PUBLIC leMail,tfrom,lOverrideXcheck,lHoldFile,NormalExit,cFolderOffice,cWhseRef
CLOSE DATA ALL
CLEAR ALL
RELEASE ALL


TRY
	lTesting = .F. && If TRUE, disables certain functions
	DO m:\dev\prg\_setvars WITH lTesting
	lUsePL = .F.
	lIsError = .F.
	cErrMsg = ""
	lOverrideXcheck = .T. && If true, doesn't check ASN carton count against barcode scans.
	lHoldFile = lTesting && If true, keeps output file in 846HOLD folder, otherwise into OUT
	leMail = .F.  && If .t., sends mail to WAREHOUSES...default is FALSE, they don't want mail for these per Maria E., 03.02.2016
	NormalExit = .F.
	tfrom = "TGF Warehouse Operations <warehouse-ops@fmiint.com>"

	FOR whseused = 1 TO 2
		closefiles()
		nAcctNum = 5154
		cOffice = ICASE(whseused = 1,"5",whseused = 2, "X")
		goffice = cOffice

		cWhse = ICASE(whseused = 1,"SAN PEDRO",whseused = 2, "CARSON")
		cWhseRef = ICASE(whseused = 1,"US30",whseused = 2, "US15",whseused = 3,"US21","US40")
		cFolderOffice = IIF(cOffice = "5","C",cOffice)
		cCustName = "INTRADECO"
		cWaitTime = IIF(lTesting,2,1)

		WAIT WINDOW "Now Processing "+cCustName+", Warehouse "+cWhse+"...Round "+ALLTRIM(STR(whseused))+" of 4" TIMEOUT cWaitTime
		WAIT WINDOW "Now Processing "+cCustName+", Warehouse "+cWhse+"...Round "+ALLTRIM(STR(whseused))+" of 4" NOWAIT

		DIMENSION thisarray(1)
		cSuffix = ""
		cIntUsage = IIF(lTesting,"T","P")

		xReturn = "XXX"
		DO m:\dev\prg\wf_alt WITH cFolderOffice,nAcctNum
		IF xReturn = "XXX"
			WAIT WINDOW "No Bldg. # Provided...closing" TIMEOUT 3
			RETURN
		ENDIF
		cUseFolder = IIF(lTesting,"f:\whp\whdata\",UPPER(xReturn))

		nSTSets = 0
		nFilenum = 0
		STORE "" TO c_CntrlNum,c_GrpCntrlNum

*!* SET CUSTOMER CONSTANTS
		cMailName = PROPER("INTRADECO") && Proper name for eMail, etc.
		cZLAddress = "9500 NW 108TH AVE"
		cZLCity = "MIAMI"
		cZLState = "FL"
		cZLZip = "33178"

		cCustPrefix = "IN" && Customer Prefix (for output file names)
		cCustPrefix = cCustPrefix+cFolderOffice

*!* SET OTHER CONSTANTS
		cX12 = "004010"  && X12 Standards Set used
		cX12ISA = LEFT(cX12,5)
		nCtnNumber = 1
		cString = ""
		csendqual = "ZZ"
		csendid = "TOLLLOGISTICS"
		crecqual = "12"
		crecid = "3052648888"  && Recipient EDI address
		cfd = "*"  && Field/element delimiter
		csegd = "~" && Line/segment delimiter
		nSegCtr = 0
		cterminator = ">" && Used at end of ISA segment
		cdate = DTOS(DATE())
		ctruncdate = RIGHT(cdate,6)
		ctime = LEFT(TIME(DATE()),2)+SUBSTR(TIME(DATE()),4,2)
		cfiledate = cdate+ctime
		cOrig = "J"
		cdate = DTOS(DATE())
		ctruncdate = RIGHT(cdate,6)
		ctime = LEFT(TIME(DATE()),2)+SUBSTR(TIME(DATE()),4,2)
		dt1 = TTOC(DATETIME(),1)
		dt2 = DATETIME()
		dtmail = TTOC(DATETIME())

** PAD ID Codes
		csendidlong = PADR(csendid,15," ")
		crecidlong = PADR(crecid,15," ")

*!* Serial number incrementing table
		IF USED("serfile")
			USE IN serfile
		ENDIF

		SELECT 0
		USE F:\3pl\DATA\mailmaster ALIAS mm
		LOCATE FOR mm.edi_type = "846" AND mm.office= cFolderOffice AND mm.accountid = nAcctNum
		tsendto = IIF(mm.use_alt,mm.sendtoalt,mm.sendto)
		tcc = IIF(mm.use_alt,mm.ccalt,mm.cc)
		cHoldPath = ALLTRIM(mm.holdpath)
		cArchivePath = ALLTRIM(mm.archpath)
		cOutPath = ALLTRIM(mm.basepath)
		LOCATE
		LOCATE FOR mm.edi_type = 'MISC' AND mm.taskname = "GENERAL"
		tsendtoerr = IIF(mm.use_alt,mm.sendtoalt,mm.sendto)
		tccerr = IIF(mm.use_alt,mm.ccalt,mm.cc)
		tsendtotest = IIF(mm.use_alt,mm.sendtoalt,mm.sendto)
		tcctest = IIF(mm.use_alt,mm.ccalt,mm.cc)
		USE IN mm

		tsendto = IIF(lTesting,tsendtotest,tsendto)
		tcc = IIF(lTesting,tcctest,tcc)

		cFilestem = cCustPrefix+dt1+".txt"

		IF lTesting
*SET STEP ON
		ENDIF

		cFilenameHold = (cHoldPath+cFilestem) && Holds file here until complete and ready to send
		cFilenameOut= (cOutPath+cFilestem)
		cFilenameArchive = (cArchivePath+cFilestem)
		nFilenum = FCREATE(cFilenameHold)

		USE ("F:\3pl\data\serial\intradeco_846_serial") IN 0 ALIAS serfile

		xsqlexec("select * from inven where mod='"+goffice+"' and inlist(accountid,5154,6237) and totqty>0",,,"wh")

		IF USED('detail')
			USE IN DETAIL
		endif

    IF USED('outdet')
      USE IN outdet
    endif
		
	xsqlexec("select * from outship where accountid in (5154,6237) and wo_date > {"+DTOC({^2016-01-01})+"}",,,"wh")
    xsqlexec("select * from outdet where accountid in (5154,6237) and wo_date > {"+DTOC({^2016-01-01})+"}",,,"wh")

*  	USE ("F:\WH"+cOffice+"\whdata\outdet") IN 0 ALIAS outdet

	USE F:\3pl\DATA\edi_trigger IN 0 ALIAS edi_trigger
**added !cancelrts 1/12/17 TMARG

		SELECT outship.wo_num,outdet.upc,outdet.STYLE,outdet.PACK,totqty, totqty*VAL(PACK) AS total_units ;
			FROM outship LEFT OUTER JOIN outdet ;
			ON outdet.outshipid = outship.outshipid ;
			WHERE INLIST(outship.accountid,5154) AND emptynul(del_date) AND pulled AND notonwip AND !sp AND !cancelrts;
			AND outship.wo_date > {^2016-01-01} ;
			INTO CURSOR wip_items READWRITE

		SELECT wip_items
		IF lTesting
			BROWSE
		ENDIF

		DELETE FOR EMPTY(wip_items.STYLE)
		LOCATE
		IF lTesting
			LOCATE
*			BROWSE TIMEOUT 5
		ENDIF

		nRCnt = 0
		nISACount = 0
		nSTCount = 1
		nSECount = 1

		SELECT STYLE,PACK,totqty,0000 AS eachqty ;
			FROM inven ;
			WHERE INLIST(accountid,5154) ;
			AND totqty>0 ;
			INTO CURSOR items1 READWRITE
		SELECT items1
		LOCATE
		SCAN
			REPLACE eachqty WITH totqty*VAL(TRANSFORM(PACK))
		ENDSCAN

		SELECT STYLE,SPACE(12) AS upc, SUM(eachqty) AS totqty,0000 AS wipqty ;
			FROM items1 ;
			INTO CURSOR items READWRITE ;
			GROUP BY STYLE

		SELECT wip_items
		SCAN
			SELECT items
			LOCATE FOR ALLTRIM(items.STYLE) = ALLTRIM(wip_items.STYLE)
			IF FOUND()
				REPLACE items.totqty WITH items.totqty + wip_items.total_units
				REPLACE items.wipqty WITH wip_items.total_units
			ELSE
				INSERT INTO items (STYLE,totqty) VALUES (wip_items.STYLE,wip_items.total_units)
				REPLACE items.wipqty WITH wip_items.total_units
			ENDIF
		ENDSCAN
		SELECT items

		SELECT STYLE,SUM(totqty) AS newqty ;
			FROM items ;
			INTO CURSOR newitems ;
			GROUP BY STYLE

		IF RECCOUNT('newitems') = 0
			WAIT WINDOW "Warehouse "+cWhse+" has no inventory. Report skipped." TIMEOUT 2
			LOOP
		ENDIF

		DO num_incr
		STORE "ISA"+cfd+"00"+cfd+"          "+cfd+"00"+cfd+"          "+cfd+csendqual+cfd+csendidlong+cfd+;
			crecqual+cfd+crecidlong+cfd+ctruncdate+cfd+ctime+cfd+"U"+cfd+cX12ISA+cfd+;
			PADL(c_CntrlNum,9,"0")+cfd+"0"+cfd+cIntUsage+cfd+cterminator+csegd TO cString
		DO cstringbreak

		STORE "GS"+cfd+"IB"+cfd+csendid+cfd+crecid+cfd+cdate+cfd+ctime+cfd+c_CntrlNum+;
			cfd+"X"+cfd+cX12+csegd TO cString
		DO cstringbreak

		nSegCtr = 0
		nISACount = 1
		cDTime = RIGHT(TTOC(DATETIME(),1),6)
		cDTOS = DTOS(DATE())

		IF nSECount = 1
			STORE "ST"+cfd+"846"+cfd+PADL(c_GrpCntrlNum,9,"0")+csegd TO cString
			DO cstringbreak
			nSegCtr = nSegCtr + 1

			STORE "BIA"+cfd+"00"+cfd+"SI"+cfd+PADL(c_GrpCntrlNum,9,"0")+cfd+cdate+csegd TO cString
			DO cstringbreak
			nSegCtr = nSegCtr + 1
			nSECount = 0

			STORE "DTM"+cfd+"007"+cfd+cdate+cfd+RIGHT(dt1,6)+csegd TO cString
			DO cstringbreak
			nSegCtr = nSegCtr + 1
			nSECount = 0

			STORE "REF"+cfd+"WH"+cfd+cWhseRef+csegd TO cString
			DO cstringbreak
			nSegCtr = nSegCtr + 1
			nSECount = 0
		ENDIF

		SELECT newitems
		lineCtr = 1
		LOCATE
		SCAN
*			STORE "LIN"+cfd+ALLTRIM(STR(lineCtr))+cfd+"UP"+cfd+newitems.upc+csegd TO cString
			STORE "LIN"+cfd+ALLTRIM(STR(lineCtr))+cfd+"SK"+cfd+ALLTRIM(newitems.STYLE)+csegd TO cString
			lineCtr=lineCtr+1

			DO cstringbreak
			nSegCtr = nSegCtr + 1

			STORE "QTY"+cfd+"26"+cfd+ALLTRIM(TRANSFORM(newitems.newqty))+csegd TO cString  && 26 = Total Inventory
			DO cstringbreak
			nSegCtr = nSegCtr + 1

			STORE "QTY"+cfd+"RJ"+cfd+ALLTRIM(TRANSFORM(newitems.newqty))+csegd TO cString  && RJ = Quantity on Shelf (?)
			DO cstringbreak
			nSegCtr = nSegCtr + 1

			STORE "QTY"+cfd+"QH"+cfd+"0"+csegd TO cString  && QH = Quantity on Hold (?)
			DO cstringbreak
			nSegCtr = nSegCtr + 1

			STORE "QTY"+cfd+"74"+cfd+"0"+cfd+"CT"+csegd TO cString  && Added to satisfy EDI requirement
			DO cstringbreak
			nSegCtr = nSegCtr + 1

			STORE "QTY"+cfd+"63"+cfd+"0"+cfd+"CT"+csegd TO cString  && Added to satisfy EDI requirement
			DO cstringbreak
			nSegCtr = nSegCtr + 1

		ENDSCAN

		nSTCount = 0

		STORE "CTT"+cfd+ALLTRIM(TRANSFORM(lineCtr))+csegd TO cString
		DO cstringbreak
		nSegCtr = nSegCtr + 1

		STORE  "SE"+cfd+ALLTRIM(STR(nSegCtr+1))+cfd+PADL(c_GrpCntrlNum,9,"0")+csegd TO cString
		DO cstringbreak
		nSegCtr = 0
		nSTSets = nSTSets + 1

		STORE  "GE"+cfd+ALLTRIM(STR(nSTSets))+cfd+c_CntrlNum+csegd TO cString
		DO cstringbreak

		STORE  "IEA"+cfd+"1"+cfd+PADL(c_CntrlNum,9,"0")+csegd TO cString
		DO cstringbreak

		=FCLOSE(nFilenum)

		IF !lTesting
			IF !USED("ftpedilog")
				SELECT 0
				USE F:\edirouting\ftpedilog ALIAS ftpedilog
				INSERT INTO ftpedilog (ftpdate,filename,acct_name,TYPE) VALUES (dt2,cFilestem,"cCustname","944")
				USE IN ftpedilog
			ENDIF
			SELECT edi_trigger
		ENDIF

		WAIT WINDOW "846 Creation process complete:"+CHR(13)+cFilestem+", Warehouse: "+cWhse TIMEOUT 1
		goodmail()

		COPY FILE [&cFilenameHold] TO [&cFilenameArchive]
		IF !lTesting AND !lHoldFile
			COPY FILE [&cFilenameHold] TO [&cFilenameOut]
			DELETE FILE [&cFilenameHold]
		ENDIF
		NormalExit = .T.
		closefiles()
	ENDFOR

*!* Had to add a PUSH bloc because Intradeco has no FTP retrieval capabilities. Joe, 03.07.2016
	IF USED('ftpjobs')
		USE IN ftpjobs
	ENDIF

	USE F:\edirouting\ftpjobs IN 0 ALIAS ftpjobs
**we have been overloaded with these triggers and they are VERY slow to connect. Limit to 3 active triggers- mvw 08/13/14
	cUseProc='945-INTRADECO-PUT'
	SELECT CNT(1) AS CNT FROM ftpjobs WHERE jobname=cUseProc INTO CURSOR xtemp
	IF xtemp.CNT<=3
		INSERT INTO ftpjobs (jobname,USERID,jobtime,terminated,COMPLETE) VALUES (cUseProc,"paulg",DATETIME(),.F.,.F.)
	ENDIF
	USE IN ftpjobs

	WAIT WINDOW "All Intradeco/Ivory 846 files processed...exiting" TIMEOUT 2

CATCH TO oErr
	IF !NormalExit
		WAIT WINDOW "At error catch block" TIMEOUT 1
		SET STEP ON
		tsubject = "846 Error in "+cCustName+", Office: "+cOffice
		tattach = " "
		tsendto = tsendtoerr
		tcc = tccerr

		tmessage = cCustName+" Error processing "+CHR(13)
		tmessage =tmessage+ "Try/CATCH Exeception Message:"+CHR(13)+;
			[  Error: ] + STR(oErr.ERRORNO) +CHR(13)+;
			[  Procedure: ] + oErr.PROCEDURE +CHR(13)+;
			[  LineNo: ] + STR(oErr.LINENO) +CHR(13)+;
			[  Message: ] + oErr.MESSAGE +CHR(13)+;
			[  Details: ] + oErr.DETAILS +CHR(13)+;
			[  StackLevel: ] + STR(oErr.STACKLEVEL) +CHR(13)+;
			[  LineContents: ] + oErr.LINECONTENTS+CHR(13)+;
			[  UserValue: ] + oErr.USERVALUE
	ENDIF
FINALLY
	SET STATUS BAR ON
	CLOSE DATA ALL
	ON ERROR
ENDTRY

************************************************************************
*!* END OF MAIN PROGRAM
************************************************************************

****************************
PROCEDURE cstringbreak
****************************
	IF lTesting
		FPUTS(nFilenum,cString)
	ELSE
		FWRITE(nFilenum,cString)
	ENDIF
ENDPROC

****************************
PROCEDURE num_incr
****************************
	SELECT serfile
	REPLACE serfile.seqnum WITH serfile.seqnum + 1 IN serfile
	REPLACE serfile.grpseqnum WITH serfile.grpseqnum + 1 IN serfile
	c_CntrlNum = ALLTRIM(STR(serfile.seqnum))
	c_GrpCntrlNum = ALLTRIM(STR(serfile.grpseqnum))
ENDPROC

****************************
PROCEDURE closefiles
****************************
	IF USED("serial")
		USE IN serial
	ENDIF
	IF USED("inven")
		USE IN inven
	ENDIF
	IF USED("outship")
		USE IN outship
	ENDIF
	IF USED("outdet")
		USE IN outdet
	ENDIF
	IF USED("edi_trigger")
		USE IN edi_trigger
	ENDIF
	IF USED("detail")
		USE IN DETAIL
	ENDIF
ENDPROC

*****************************
PROCEDURE goodmail
*****************************
	IF leMail
		tattach = ""
		tsubject = "846 Update Done, "+TTOC(DATETIME())
		DO FORM m:\dev\frm\dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
	ENDIF
ENDPROC
