* SFTP upload Rackroom EPDI files to Fedex

* EXE = F:\UTIL\FTPSTUFF\SFTP_RR_EPDI.EXE

*  SFTP_RR_EPDI

********************************************************************************
**** Must be run from a server with CuteFTP9 installed, such as FTP1 !!!!!!!****
********************************************************************************


*!*	runack("SFTP_RR_EPDI")

LOCAL lnError, lcProcessName, loSFTP_RR_EPDI

DECLARE INTEGER CreateMutex IN Win32API ;
	STRING @lpMutexAttributes, ;
	STRING bInitialOwner, ;
	STRING @lpName

DECLARE INTEGER GetLastError IN Win32API

* if running in EXE check to see if already running
IF _VFP.STARTMODE > 0 THEN
	lcProcessName = "SFTP_RR_EPDI"
	CreateMutex(0,CHR(1),@lcProcessName)
	lnError = GetLastError()
	IF lnError = 183
		WAIT WINDOW TIMEOUT 10 "Rack Room EPDI to Fedex process is already running..."
		RETURN .F.
	ENDIF
ENDIF

*!*	utilsetup("SFTP_RR_EPDI")


loSFTP_RR_EPDI = CREATEOBJECT('SFTP_RR_EPDI')
loSFTP_RR_EPDI.MAIN()

*!*	schedupdate()

CLOSE DATABASES ALL
RETURN

#DEFINE LOGIT 1
#DEFINE WAITIT 2
#DEFINE NOWAITIT 4
#DEFINE SENDIT 8
#DEFINE CRLF CHR(13) + CHR(10)
#DEFINE LF CHR(10)
#DEFINE RETURN_DATA_MANDATORY .F.
#DEFINE RETURN_DATA_NOT_MANDATORY .T.
#DEFINE DAILY_OT_MULTIPLIER 1.5
#DEFINE DAILY_OT20_MULTIPLIER 2.0
#DEFINE xlEdgeBottom 9
#DEFINE xlContinuous 1
#DEFINE xlMedium -4138

DEFINE CLASS SFTP_RR_EPDI AS CUSTOM

	cProcessName = 'SFTP_RR_EPDI'

	lTestMode = .T.  && .F. FOR NORMAL OPERATION, .T. TO SUPPRESS ALL BUT DEVELOPER EMAILS AND CHANGE INPUT TABLE, ETC.

	lAutoYield = .T.

	cStartTime = TTOC(DATETIME())
	
	* TRANSFER ENGINE PROPERTIES
	oTransferEngine = NULL
	
	* date properties
	dtNow = DATETIME()

	dToday = DATE()

	cToday = DTOC(DATE())
	
	* file/folder props
	cRootFileName = ''
	cLocalFolder = ''
	cArchiveFolder = ''
	cRemoteFolder = ''
	
	* error props
	nNumberOfErrors = 0

	* wait window properties
	nWaitWindowTimeout = 2
	lWaitWindowIsOn = .T.

	* logfile properties
	lLoggingIsOn = .T.
	cLogFile = 'F:\UTIL\FTPSTUFF\LOGFILES\SFTP_RR_EPDI_LOG.TXT'

	* INTERNAL email properties
	lSendInternalEmailIsOn = .T.
	cFrom = 'Mark.Bennett@Tollgroup.com'
	cSendTo = 'mbennett@fmiint.com'
	cCC = ''
	cSubject = 'EPDI to Fedex SFTP process'
	cAttach = ''
	cBodyText = ''
	cTopBodyText = ''

	cCOMPUTERNAME = ''
	cUSERNAME = ''

	FUNCTION INIT
		IF NOT DODEFAULT()
			RETURN .F.
		ENDIF
		WITH THIS
			CLEAR
			*SET RESOURCE OFF
			CLOSE DATA
			SET CENTURY ON
			SET DATE AMERICAN
			SET HOURS TO 24
			SET ANSI ON
			SET TALK OFF
			SET DELETED ON
			SET CONSOLE OFF
			SET EXCLUSIVE OFF
			SET SAFETY OFF
			SET EXACT OFF
			SET STATUS BAR ON
			SET SYSMENU OFF
			SET ENGINEBEHAVIOR 70
			_VFP.AUTOYIELD = .lAutoYield
			.cCOMPUTERNAME = GETENV("COMPUTERNAME")
			.cUSERNAME = GETENV("USERNAME")
			IF .lTestMode THEN
				.cSendTo = 'mbennett@fmiint.com'
				.cCC = ''
				.cLogFile = 'F:\UTIL\FTPSTUFF\LOGFILES\SFTP_RR_EPDI_LOG_TESTMODE.TXT'
			ENDIF
			.lLoggingIsOn = .lLoggingIsOn AND NOT EMPTY(.cLogFile)
		ENDWITH
	ENDFUNC


	FUNCTION DESTROY
		WITH THIS
			IF .lLoggingIsOn  THEN
				SET ALTERNATE OFF
				SET ALTERNATE TO
			ENDIF
		ENDWITH
		DODEFAULT()
	ENDFUNC


	FUNCTION MAIN
		WITH THIS
			LOCAL lnRetVal, laFiles[1,5], laFilesSorted[1,6], lnNumFiles, lnCurrentFile, lcFile, lcArchiveFile, lcMessage

			TRY
				.nNumberOfErrors = 0

				IF .lLoggingIsOn THEN
					SET ALTERNATE TO (.cLogFile) ADDITIVE
					SET ALTERNATE ON
				ENDIF

				.TrackProgress('Rack Room EPDI to Fedex process started....', LOGIT+SENDIT+NOWAITIT)
				.TrackProgress('COMPUTERNAME = ' + .cCOMPUTERNAME, LOGIT+SENDIT)
				.TrackProgress('USERNAME = ' + .cUSERNAME, LOGIT+SENDIT)
				.TrackProgress('PROJECT = SFTP_RR_EPDI', LOGIT+SENDIT)
				IF .lTestMode THEN
					.TrackProgress('                        TEST MODE                        ', LOGIT+SENDIT)
				ENDIF
				
				.cSubject = 'EPDI to Fedex SFTP process, ' + TRANSFORM(DATETIME())

				.cLocalFolder   = 'F:\FTPUSERS\RACKROOM\EPDI_OUT\'
				.cArchiveFolder = 'F:\FTPUSERS\RACKROOM\EPDI_OUT\ARCHIVE\'
				.cRemoteFolder  = '/GOFEDEXASYNC/EPDIT/'   && Test
				*.cRemoteFolder  = '/GOFEDEXASYNC/EPDI/'   && production
				
					lnNumFiles = ADIR(laFiles,.cLocalFolder + "EP*.*")

					IF lnNumFiles > 0 THEN
					
						*Create a connection object and assign properties
						.oTransferEngine = CREATEOBJECT("CuteFTPPro.TEConnection")
						*.oTransferEngine.protocol = "FTP"  
						.oTransferEngine.protocol = "SFTP"  && this enables secure ftp with secure shell
						.oTransferEngine.useproxy = "BOTH"
						.oTransferEngine.port = 60022
						
						.oTransferEngine.HOST     = 'prod.ec.fedex.com'
						.oTransferEngine.login    = "RACKROOM"
						.oTransferEngine.PASSWORD = "Orange14"
						

						.TrackProgress('Protocol = ' + .oTransferEngine.protocol, LOGIT+SENDIT)
						.TrackProgress('Host = ' + .oTransferEngine.HOST, LOGIT+SENDIT)
						

						.TrackProgress('Now Connecting to ===> ' + .oTransferEngine.HOST, LOGIT+SENDIT)
						.oTransferEngine.CONNECT()

						lcMessage = .oTransferEngine.WAIT(-1,120)

						IF NOT EMPTY(lcMessage) THEN
							.TrackProgress('Connection message = ' + lcMessage, LOGIT+SENDIT)
						ENDIF

						IF .oTransferEngine.isconnected >= 0
							.TrackProgress('ERROR: Could not connect to: ' + .oTransferEngine.HOST, LOGIT+SENDIT)
							THROW
						ELSE
							.TrackProgress('Now connected to: ' + .oTransferEngine.HOST, LOGIT+SENDIT)
						ENDIF

						.oTransferEngine.localfolder = .cLocalFolder
						.TrackProgress('Local Folder = ' + .oTransferEngine.localfolder, LOGIT+SENDIT)
						
						IF NOT EMPTY(.cRemoteFolder) THEN
						
							.oTransferEngine.remotefolder = .cRemoteFolder						
							.TrackProgress('Remote Folder = ' + .oTransferEngine.remotefolder, LOGIT+SENDIT)
							
							lnRetVal = .oTransferEngine.remoteexists(.oTransferEngine.remotefolder)
							IF lnRetVal >= 0 THEN
								.TrackProgress('ERROR: Remote folder [' + .oTransferEngine.remotefolder + '] does not exist!' , LOGIT+SENDIT)
								THROW
							ENDIF
							
						ENDIF

						* we've connected okay if we got to here, proceed to upload files.
						
						* sort file list by date/time
						.SortArrayByDateTime(@laFiles, @laFilesSorted)

						FOR lnCurrentFile = 1 TO lnNumFiles
				
							.cRootFileName = laFiles[lnCurrentFile,1]
							lcFile = .cLocalFolder + .cRootFileName
							lcArchiveFile = .cArchiveFolder + laFiles[lnCurrentFile,1]
				
							.UploadFile( .cRootFileName, lcFile, lcArchiveFile )
				
						ENDFOR  &&  lnCurrentFile = 1 TO lnNumFiles

						.oTransferEngine.CLOSE()
				
					ELSE

						.TrackProgress('No EPDI files to upload!',LOGIT+SENDIT)
						.cSubject = "NO FILES FOUND - " + .cSubject
						
						* don't send internal email ?
						*.lSendInternalEmailIsOn = .F.
						
					ENDIF && lnNumFiles > 0
												

				.TrackProgress('EPDI to Fedex SFTP process ended normally....', LOGIT+SENDIT+NOWAITIT)

			CATCH TO loError

				.TrackProgress('There was an error.',LOGIT+SENDIT)
				.TrackProgress('The Error # is: ' + TRANSFORM(loError.ERRORNO), LOGIT+SENDIT)
				.TrackProgress('The Error Message is: ' + TRANSFORM(loError.MESSAGE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred in Program: ' + TRANSFORM(loError.PROCEDURE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred at line #: ' + TRANSFORM(loError.LINENO), LOGIT+SENDIT)
				.nNumberOfErrors = .nNumberOfErrors + 1
				IF TYPE('.oTransferEngine') = 'O' AND NOT ISNULL(.oTransferEngine) THEN
					.oTransferEngine.CLOSE()
				ENDIF

			ENDTRY

			CLOSE DATA
			WAIT CLEAR
			
			IF .nNumberOfErrors > 0 THEN
				.cSubject = '(ERRORS) ' + .cSubject
			ENDIF
			
			***************** INTERNAL email results ******************************
			.TrackProgress('About to send status email.',LOGIT)
			.TrackProgress('==================================================================================================================', SENDIT)
			.TrackProgress('The logfile is: ' + .cLogFile, SENDIT)
			.TrackProgress('Rack Room EPDI to Fedex process started: ' + .cStartTime, LOGIT+SENDIT)
			.TrackProgress('Rack Room EPDI to Fedex process finished: ' + TTOC(DATETIME()), LOGIT+SENDIT)

			IF .lSendInternalEmailIsOn THEN
				* try to trap error from not having dartmail dll's registered on user's pc...
				TRY
					.cBodyText = .cTopBodyText + CRLF + .cBodyText

					DO FORM dartmail2 WITH .cSendTo,.cFrom,.cSubject,.cCC,.cAttach,.cBodyText,"A"
					.TrackProgress('Sent status email.',LOGIT)
				CATCH TO loError
					*=MESSAGEBOX('There was an error sending the status email. Please contact Mark Bennett.',0+64,"KRONOS Temp Time")
					.TrackProgress('There was an error sending the status email.',LOGIT)
					.TrackProgress('The Error # is: ' + TRANSFORM(loError.ERRORNO), LOGIT)
					.TrackProgress('The Error Message is: ' + TRANSFORM(loError.MESSAGE), LOGIT)
					.TrackProgress('The Error occurred in Program: ' + TRANSFORM(loError.PROCEDURE), LOGIT)
					.TrackProgress('The Error occurred at line #: ' + TRANSFORM(loError.LINENO), LOGIT)
					.nNumberOfErrors = .nNumberOfErrors + 1
					CLOSE DATA
				ENDTRY

			ELSE
				.TrackProgress('Did not send status email: THIS.lSendInternalEmailIsOn = FALSE.',LOGIT)
			ENDIF

		ENDWITH
		RETURN
	ENDFUNC && main
	
	
	PROCEDURE UploadFile
		LPARAMETERS tcRootFileName, tcFile, tcArchiveFile
		WITH THIS
			LOCAL lcMessage, lnRetVal

				.TrackProgress("============================================================",LOGIT+SENDIT)
				.TrackProgress("==> Uploading file: " + tcFile,LOGIT+SENDIT+NOWAITIT)
					
				lnRetVal = .oTransferEngine.remoteexists(tcRootFileName)
				IF lnRetVal < 0 THEN
					.TrackProgress(tcRootFileName + ' already exists in remote folder - deleting it before upload!' , LOGIT+SENDIT)
					* delete it
					.oTransferEngine.remoteremove(tcRootFileName)
				ELSE
					.TrackProgress(tcRootFileName + ' does NOT exist in remote folder!' , LOGIT+SENDIT)
				ENDIF
				
					
				.oTransferEngine.upload(tcFile)

				lcMessage = .oTransferEngine.WAIT(-1,60)

				.TrackProgress('File Transfer message = ' + lcMessage, LOGIT+SENDIT)
				
				IF NOT ("FINISHED" $ UPPER(lcMessage)) THEN
					.TrackProgress('=====> WARNING: unexpected file transfer message - check the upload!', LOGIT+SENDIT)
					THROW
				ENDIF
				
				* archive the file we sent
				COPY FILE (tcFile) TO (tcArchiveFile)
				IF FILE(tcArchiveFile) THEN
					DELETE FILE (tcFile)
				ELSE
					.TrackProgress('**** ERROR creating local archived file: ' + tcArchiveFile, LOGIT+SENDIT)
					.nNumberOfErrors = .nNumberOfErrors + 1
				ENDIF
				
				IF FILE(tcFile) THEN
					.TrackProgress('**** ERROR in the archiving; did not delete local file: ' + tcFile, LOGIT+SENDIT)
					.nNumberOfErrors = .nNumberOfErrors + 1
				ENDIF

	
		ENDWITH
		RETURN
	ENDPROC


	PROCEDURE TrackProgress
		* do any combination of Wait Window, writing to logfile, and adding to body of email,
		* based on nFlags parameter.
		LPARAMETERS tcExpression, tnFlags
		WITH THIS
			IF BITAND(tnFlags,LOGIT) = LOGIT THEN
				IF .lLoggingIsOn THEN
					?
					? .cProcessName + "  " + TTOC(DATETIME()) + ": " + tcExpression
				ENDIF
			ENDIF
			IF .lWaitWindowIsOn THEN
				IF BITAND(tnFlags,NOWAITIT) = NOWAITIT THEN
					WAIT WINDOW tcExpression NOWAIT
				ENDIF
				IF BITAND(tnFlags,WAITIT) = WAITIT THEN
					WAIT WINDOW tcExpression TIMEOUT .nWaitWindowTimeout
				ENDIF
			ENDIF
			IF BITAND(tnFlags,SENDIT) = SENDIT THEN
				IF .lSendInternalEmailIsOn THEN
					.cBodyText = .cBodyText + tcExpression + CRLF + CRLF
				ENDIF
			ENDIF
		ENDWITH
	ENDPROC  &&  TrackProgress


	PROCEDURE SortArrayByDateTime
		* expects taInArray[] to be the result of an ADIR()
		LPARAMETERS taInArray, taOutArray
		EXTERNAL ARRAY taInArray, taOutArray
		LOCAL lnRows, lnCols, i, j
		* extending taInArray[] to process the files in date/time order
		lnRows = ALEN(taInArray,1)
		lnCols = ALEN(taInArray,2)
		* create taOutArray with one more column
		DIMENSION taOutArray[lnRows,lnCols + 1]
		* and fill it with taInArray columns AND (filedate + filetime) in new column
		* ( ACOPY() doesn't preserve elements properly because of different # of columns )
		FOR i = 1 TO lnRows
			FOR j = 1 TO lnCols
				taOutArray[i,j] = taInArray[i,j]
			ENDFOR
			taOutArray[i,lnCols + 1] = DTOS(taInArray[i,3]) + "_" + taInArray[i,4]
		ENDFOR
		* sort taOutArray on date/time column
		=ASORT(taOutArray,6)
	ENDPROC


	PROCEDURE SortArrayByFileName
		* expects taInArray[] to be the result of an ADIR()
		LPARAMETERS taInArray, taOutArray
		EXTERNAL ARRAY taInArray, taOutArray
		LOCAL lnRows, lnCols, i, j
		* extending taInArray[] to process the files in date/time order
		lnRows = ALEN(taInArray,1)
		lnCols = ALEN(taInArray,2)
		* create taOutArray with one more column
		DIMENSION taOutArray[lnRows,lnCols + 1]
		* and fill it with taInArray columns AND (filedate + filetime) in new column
		* ( ACOPY() doesn't preserve elements properly because of different # of columns )
		FOR i = 1 TO lnRows
			FOR j = 1 TO lnCols
				taOutArray[i,j] = taInArray[i,j]
			ENDFOR
			taOutArray[i,lnCols + 1] = DTOS(taInArray[i,3]) + "_" + taInArray[i,4]
		ENDFOR
		* sort taOutArray on date/time column
		=ASORT(taOutArray,6)
	ENDPROC


ENDDEFINE
