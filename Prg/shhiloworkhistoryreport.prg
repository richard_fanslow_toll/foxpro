LPARAMETERS tcCostCenter, tdStartDate, tdEndDate

LOCAL lcWhere, lcCentury, ldStartDate, ldEndDate, lcCostCenter

PRIVATE  pcCostCenter, pcDateRange

ldStartDate = tdStartDate
ldEndDate = tdEndDate
lcCostCenter = UPPER(ALLTRIM(tcCostCenter))
pcCostCenter = lcCostCenter

IF lcCostCenter == "ALL" THEN
	lcWhere = ""
ELSE
	lcWhere = " AND A.DIVISION = '" + lcCostCenter + "' "
ENDIF

IF (NOT EMPTY(ldStartDate)) AND (NOT EMPTY(ldEndDate)) THEN
	lcWhere = lcWhere + " AND (B.CRDATE >= ldStartDate) AND (B.CRDATE <= ldEndDate) "
	pcDateRange = " For Work Orders from " + TRANSFORM(ldStartDate) + " thru " + TRANSFORM(ldEndDate)
ELSE
	pcDateRange = ""
ENDIF

IF USED('CURHILO') THEN
	USE IN CURHILO
ENDIF

WAIT WINDOW NOWAIT "Retrieving data - please wait..."

SELECT ;
	A.DIVISION, ;
	A.VEHNO, ;
	A.MODEL, ;
	A.MODELNO, ;
	B.CRDATE, ;
	B.ORDNO, ;
	B.MECHNAME, ;
	B.WORKDESC ;
	FROM F:\SHOP\SHDATA\VEHMAST A ;
	INNER JOIN F:\SHOP\SHDATA\ORDHDR B ;
	ON B.VEHNO = A.VEHNO ;
	INTO CURSOR CURHILO ;
	WHERE NOT EMPTY(A.VEHNO) ;
	&lcWhere. ;
	ORDER BY 1, 2, 5

WAIT CLEAR

lcCentury = SET('CENTURY')
SET CENTURY ON

IF (NOT USED('CURHILO')) OR EOF('CURHILO') THEN
	=MESSAGEBOX("No matching work orders were found!")
ELSE
	SELECT CURHILO
	GOTO TOP
	KEYBOARD '{CTRL+F10}' CLEAR
	REPORT FORM shhiloworkhistoryreport PREVIEW NOCONSOLE
ENDIF

IF USED('CURHILO') THEN
	USE IN CURHILO
ENDIF

SET CENTURY &lcCentury.

RETURN