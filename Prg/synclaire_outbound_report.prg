*Parameters xacct

*xacct = 6521
xacct = 6221
do m:\dev\prg\lookups

do _setvars

set decimals to 0

set safety off
public gsystemmodule

gsystemmodule ="wh"

runack("SYNCLAIRE_OUTBOUND")

goffice="2"

wait window at 10,10 "Synclaire outbound Processing..........." nowait

use f:\wh\sqlpassword in 0
gsqlpassword = alltrim(sqlpassword.password)
use in sqlpassword

cserver = "tgfnjsql01"
sqlsetprop(0,'DispLogin',3)
sqlsetprop(0,"dispwarnings",.f.)

lcdsnless="driver=SQL Server;server=&cServer;uid=SA;pwd=&gsqlpassword;DATABASE=PICKPACK"
xxxhandle=sqlstringconnect(m.lcdsnless,.t.)
if xxxhandle<=0 && bailout
	wait window at 6,2 "No Data Connection.."+chr(13)+" Call MIS...."
	lcquery = [Could not make connection to tgfnjsql01 SQL server]+chr(13)+"   "+lcdsnless
	throw
	return
endif

*
xsqlexec("select wo_num,ship_ref,del_date,consignee,ctnqty,qty,cuft,outshipid from outship where accountid = " + transform(xacct),,,"wh")

xsqlexec("select outshipid,outdetid,upc,totqty from outdet where accountid = " + transform(xacct),,,"wh")

*set step On
create cursor outdata (;
del_date d,;
outshipid int,;
consignee char(30),;
ship_ref  char(20),;
upc char(13),;
ucc char(20),;
ctnqty n(6),;
qty n(6),;
qtypercase char(6),;
cuft n(6,2))

select outship
index on del_date tag del
select outship
index on outshipid tag outshipid
index on month(del_date) tag mdel


select outship
set order to del
lncount = 0

scan for !isnull(del_date) and month(del_date) = 11
	select outship
	wait window at 10,10 "doing PT: "+outship.ship_ref+" dated: "+dtos(outship.del_date)+"  "+ transform(lncount) nowait
	scatter memvar
	insert into outdata from memvar
	select outdet
	scan for outshipid = outship.outshipid
		scatter memvar fields upc,totqty
		insert into outdata (ship_ref,upc,qty,del_date,consignee,outshipid) values;
		(outship.ship_ref,outdet.upc,outdet.totqty,outship.del_date,outship.consignee,outship.outshipid)
	endscan
	m.upc =""

	if usesql()
		xsqlexec("select sum(totqty) as lnunits, ucc from cartons where ship_ref='"+outship.ship_ref+"' group by ucc","result",,"pickpack")
	else
		lcquery ="select Sum(totqty) as lnunits,UCC from cartons where ship_ref = '"+outship.ship_ref+" ' group by ucc"
		nok = sqlexec(xxxhandle,lcquery,"result")
	endif

	if reccount("result") > 0
		select result
		scan
			scatter memvar fields ucc,lnunits
			insert into outdata (ship_ref,ucc,qtypercase,del_date,consignee,outshipid) values;
			(outship.ship_ref, result.ucc, transform(result.lnunits),outship.del_date,outship.consignee,outship.outshipid)

		endscan
		m.ucc =""
		m.qtypercase =""
	endif w
	lncount = lncount+1
endscan


select outdata

if xacct = 6521
	copy to f:\ftpusers\synclaire\outboundreport\synclaire_outbound_nov2017.csv csv
else
	copy to f:\ftpusers\bcny\outboundreport\bcny_outbound_nov2017.csv csv
endif

?chr(7)

wait clear

return


*!*	Copy To h:\fox\temp2

*!*	Select temp
*!*	Append From h:\fox\temp2

*!*	Select * From temp Order By invdt,invdetid Into Cursor fdata
*!*	Select fdata

*!*	If xacct = 6521
*!*	  *Copy To F:\ftpusers\synclaire\invoicedata\synclaire_billing.Csv Csv
*!*	  Copy To F:\ftpusers\synclaire\invoicedata\synclaire_billing2.Csv Csv
*!*	Else
*!*	  Copy To F:\ftpusers\synclaire\invoicedata\bcny_billing.Csv Csv
*!*	Endif

*!*	Wait Window At 10,10 "Synclaire Billing Process Complete.............." Timeout 2

*!*	tsubject = "Synclaire Billing file created at: "+Ttoc(Datetime())
*!*	tsendto  = "pgaidis@fmiint.com,tmarg@fmiint.com"
*!*	tcc = ""
*!*	tattach=""
*!*	tfrom = "Toll/FMI EDI Operations <toll-edi-ops@tollgroup.com>"
*!*	tmessage = tsubject
*!*	Do Form m:\dev\frm\dartmail2 With tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"

*!*	schedupdate()

*!*	Return
*******************************************8888
