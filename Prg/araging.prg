* ARAGING

* populate AR Aging table daily, for Rob Petti/Qlikview. Based on report in the araging form.

* build exe as F:\UTIL\ARAGING\ARAGING.EXE

LOCAL lTestMode
lTestMode = .F.


IF NOT lTestMode THEN
	utilsetup("ARAGING")
ENDIF

DECLARE INTEGER CreateMutex IN Win32API ;
	STRING @lpMutexAttributes, ;
	STRING bInitialOwner, ;
	STRING @lpName

DECLARE INTEGER GetLastError IN Win32API

* if running in EXE check to see if already running
IF _VFP.STARTMODE > 0 THEN
	lcProcessName = "ARAGING"
	CreateMutex(0,CHR(1),@lcProcessName)
	lnError = GetLastError()
	IF lnError = 183
		WAIT WINDOW TIMEOUT 5 "This program is already running..."
		IF NOT lTestMode THEN
			schedupdate()
		ENDIF
		RETURN .F.
	ENDIF
ENDIF

loARAGING = CREATEOBJECT('ARAGING')
loARAGING.MAIN( lTestMode )

IF NOT lTestMode THEN
	schedupdate()
ENDIF

CLOSE DATABASES ALL
RETURN

#DEFINE LOGIT 1
#DEFINE WAITIT 2
#DEFINE NOWAITIT 4
#DEFINE SENDIT 8
#DEFINE CRLF CHR(13) + CHR(10)
#DEFINE RETURN_DATA_MANDATORY .F.
#DEFINE RETURN_DATA_NOT_MANDATORY .T.
#DEFINE CSVFORMAT 6
#DEFINE WRITE_ONLY_UNBUFFERED 11
#DEFINE COMMA ","

DEFINE CLASS ARAGING AS CUSTOM

	cProcessName = 'ARAGING'

	lTestMode = .F.  && .F. FOR NORMAL OPERATION, .T. TO SUPPRESS ALL BUT DEVELOPER EMAILS AND CHANGE INPUT TABLE, ETC.

	dToday = DATE()

	lAutoYield = .T.

	cStartTime = TTOC(DATETIME())

	* connection properties
	nSQLHandle = 0


	* data properties

	* processing properties

	* Excel properties
	oExcel = NULL

	* wait window properties
	nWaitWindowTimeout = 2
	lWaitWindowIsOn = .T.

	* logfile properties
	lLoggingIsOn = .T.
	cLogFile = 'F:\UTIL\ARIAT\LOGFILES\ARAGING_log.txt'

	* INTERNAL email properties
	lSendInternalEmailIsOn = .T.
	cFrom = 'mark.bennett@tollgroup.com'
	cSendTo = 'mbennett@fmiint.com'
	*cSendTo = 'mbennett@fmiint.com'
	cCC = ''
	cSubject = 'AR Aging Process for ' + TRANSFORM(DATETIME())
	cAttach = ''
	cBodyText = ''
	cMGRSendTo = ''
	cMGRFrom = ''
	cMGRSubject = ''
	cMGRCC = ''
	cMGRAttach = ''
	cMGRBodyText = ''

	cCOMPUTERNAME = ''
	cUSERNAME = ''


	FUNCTION INIT
		IF NOT DODEFAULT()
			RETURN .F.
		ENDIF
		WITH THIS
		
			SET STRICTDATE TO 0
			*SET RESOURCE OFF
			CLOSE DATA
			SET CENTURY ON
			*SET DATE AMERICAN
			*SET HOURS TO 24
			SET DECIMALS TO 2
			SET ANSI ON
			SET TALK OFF
			SET DELETED ON
			SET CONSOLE OFF
			SET EXCLUSIVE OFF
			SET SAFETY OFF
			SET EXACT OFF
			SET STATUS BAR ON
			SET SYSMENU OFF
			SET ENGINEBEHAVIOR 70
			SET MULTILOCKS ON
			_VFP.AUTOYIELD = .lAutoYield
			.cCOMPUTERNAME = GETENV("COMPUTERNAME")
			.cUSERNAME = GETENV("USERNAME")
			IF .lTestMode THEN
				.cLogFile = 'F:\UTIL\ARAGING\LOGFILES\ARAGING_log_TESTMODE.txt'
				.cSendTo = 'mbennett@fmiint.com'
			ENDIF
			.lLoggingIsOn = .lLoggingIsOn AND NOT EMPTY(.cLogFile)
			IF .lLoggingIsOn THEN
				SET ALTERNATE TO (.cLogFile) ADDITIVE
				SET ALTERNATE ON
			ENDIF
		ENDWITH
	ENDFUNC


	FUNCTION DESTROY
		WITH THIS
			IF .lLoggingIsOn  THEN
				SET ALTERNATE OFF
				SET ALTERNATE TO
			ENDIF
		ENDWITH
		IF TYPE('.oExcel') = 'O' AND NOT ISNULL(.oExcel) THEN
			.oExcel.QUIT()
		ENDIF
		DODEFAULT()
	ENDFUNC


	FUNCTION MAIN
		LPARAMETERS tlTestMode
		WITH THIS
			LOCAL lnNumberOfErrors
			TRY
				lnNumberOfErrors = 0

				.lTestMode = tlTestMode

				.cStartTime = TTOC(DATETIME())

				.TrackProgress('AR Aging process started....', LOGIT+SENDIT)
				.TrackProgress('COMPUTERNAME = ' + .cCOMPUTERNAME, LOGIT+SENDIT)
				.TrackProgress('USERNAME = ' + .cUSERNAME, LOGIT+SENDIT)
				.TrackProgress('PROJECT = ARAGING', LOGIT+SENDIT)
				IF .lTestMode THEN
					.TrackProgress('                        TEST MODE                        ', LOGIT+SENDIT)
				ENDIF
				****************
				*OPEN DATABASE F:\WATUSI\ARDATA\AR

				xsqlexec("select * from account where inactive=0","account",,"qq")
				index on accountid tag accountid
				set order to

				xsqlexec("select * from acctbill","acctbill",,"qq")
				index on billtoid tag billtoid
				index on accountid tag accountid
				index on STR(billtoid,4)+STR(accountid,4) tag billacct
				set order to
				
				xsqlexec("select * from billto","billto",,"qq")
				index on billtoid tag billtoid
				set order to

				xsqlexec("select * from salesman","salesman",,"qq")
				index on salesmanid tag salesmanid
				index on salesman tag salesman
				set order to

				* added 04/27/2017 MB because division table is no longer in FoxPro
				xsqlexec("select * from division",,,"ar")
				index on company tag company
				set order to

				xsqlexec("select groupname, acctgrpid from acctgrp order by 1","xacctgrp",,"ar")
				xsqlexec("select * from acctdet",,,"ar")

				select division+" - "+shortname as divshort, company from division order by 1 into cursor xrpt 
				select * from xrpt into cursor xdivno where .f. readwrite

				insert into xdivno (divshort, company) values ("TRUCKING", "t")
				insert into xdivno (divshort, company) values ("WAREHOUSING", "w")

				select xrpt
				scan 
					insert into xdivno (divshort, company) values (xrpt.divshort, xrpt.company)
				ENDSCAN

				*USE F:\WATUSI\ARDATA\division IN 0  && 04/27/2017

				xsqlexec("select * from farhdr",,,"ar")

				PUBLIC gaccountid
				PUBLIC gemail
				PUBLIC gover45
				PUBLIC gover90
				PUBLIC gijoe
				PUBLIC gbycompany
				PUBLIC gcompany
				PUBLIC gasofdt
				PUBLIC gwilmington
				PUBLIC gsanfran
				PUBLIC g331
				PUBLIC gplatinumok


				gaccountid=0
				gemail=.F.
				gover90=.F.
				gijoe=.F.
				gbycompany=.F.
				gcompany=''
				gasofdt=DATE()
				*gwilmington=gwilmonly
				g331=.F.

				gplatinumok = .F.  && ????
				gwilmington= .F.
				gover45 = .T.

				*!*	IF gijoe AND (gover45 OR gover90)
				*!*		x3winmsg("Joe's Special can't be used with over 45 or over 90")
				*!*		RETURN
				*!*	ENDIF

				WAIT WINDOW "Please Wait..." NOWAIT

				IF EMPTY(gaccountid)
					taccountid="%"
				ELSE
					taccountid=platkey(gaccountid,.T.)
				ENDIF
				tbilltoid="%"

				*!*	IF gplatinumok
				*!*		SELECT farhdr
				*!*		IF EMPTY(gaccountid)
				*!*			SET FILTER TO
				*!*		ELSE
				*!*			SET FILTER TO shiptokey=taccountid
				*!*		ENDIF
				*!*		REQUERY("arhdr_cs")
				*!*		REQUERY("arselect")
				*!*	ENDIF

				goffsetsince={}
				arcalc(!gplatinumok, gasofdt)

				*!*	IF gwilmington
				*!*		xinlist=""
				*!*		SELECT account
				*!*		COUNT TO xnumwo FOR wilmington
				*!*		SCAN FOR wilmington
				*!*			xinlist=xinlist+TRANSFORM(accountid)+","
				*!*		ENDSCAN

				*!*		xnuminlists=INT((xnumwo-1)/25)+1
				*!*		xfilter="("
				*!*		FOR i=1 TO xnuminlists
				*!*			xdy=""
				*!*			FOR j=1 TO MIN(25,xnumwo-(i-1)*25)
				*!*				xdy=xdy+GETWORDNUM(xinlist,(i-1)*25+j,",")+","
				*!*			NEXT
				*!*			xfilter=xfilter+"inlist(accountid,"+LEFT(xdy,LEN(xdy)-1)+") or "
				*!*		NEXT
				*!*		xfilter=LEFT(xfilter,LEN(xfilter)-4)+")"

				*!*		DELETE IN xrpt FOR !&xfilter
				*!*	ENDIF

				*!*	IF gsanfran
				*!*		xinlist=""
				*!*		SELECT account
				*!*		COUNT TO xnumwo FOR sanfran
				*!*		SCAN FOR sanfran
				*!*			xinlist=xinlist+TRANSFORM(accountid)+","
				*!*		ENDSCAN

				*!*		xnuminlists=INT((xnumwo-1)/25)+1
				*!*		xfilter="("
				*!*		FOR i=1 TO xnuminlists
				*!*			xdy=""
				*!*			FOR j=1 TO MIN(25,xnumwo-(i-1)*25)
				*!*				xdy=xdy+GETWORDNUM(xinlist,(i-1)*25+j,",")+","
				*!*			NEXT
				*!*			xfilter=xfilter+"inlist(accountid,"+LEFT(xdy,LEN(xdy)-1)+") or "
				*!*		NEXT
				*!*		xfilter=LEFT(xfilter,LEN(xfilter)-4)+")"

				*!*		DELETE IN xrpt FOR !&xfilter
				*!*	ENDIF

				xfilter=".t."

				*!*	IF g331
				*!*		SELECT accountid, billtoid, docnum, invnum, docdt, origdocdt, lastactdt, wo_num, ;
				*!*			SUM(amt1) AS amt1, SUM(amt2) AS amt2, SUM(amt3) AS amt3, SUM(amt4) AS amt4, ;
				*!*			amt5, amt6, amt7, amt8, amt9, amt10, SUM(paid) AS paid, SUM(owed) AS owed ;
				*!*			FROM xrpt ;
				*!*			WHERE origdocdt<={3/31} ;
				*!*			GROUP BY accountid, billtoid, invnum ;
				*!*			INTO CURSOR xrpt
				*!*	ENDIF

				*!*	IF gbycompany OR !EMPTY(gcompany)

				*!*		SELECT xrpt
				*!*		*	delete for amt1=0 and amt2=0 and amt3=0 and amt4=0

				*!*		arcomp(.T.)

				*!*		DO CASE
				*!*			CASE gcompany="t" AND !EMPTY(gasofdt)
				*!*				xfilter='inlist(company,"X","T","J","^","V","U")'
				*!*			CASE gcompany="w" AND !EMPTY(gasofdt)
				*!*				xfilter='!inlist(company,"X","T","J","^","V","U")'
				*!*			CASE !EMPTY(gcompany) AND !EMPTY(gasofdt)
				*!*				xfilter="company=gcompany and docdt<=gasofdt"
				*!*			CASE gcompany="t"
				*!*				xfilter='inlist(company,"X","T","J","^","V","U")'
				*!*			CASE gcompany="w"
				*!*				xfilter='!inlist(company,"X","T","J","^","V","U")'
				*!*			CASE !EMPTY(gcompany)
				*!*				xfilter="company=gcompany"
				*!*			OTHERWISE
				*!*				xfilter=".t."
				*!*		ENDCASE

				*!*		IF INLIST(gcompany,"t","w")
				*!*			xgroupby="1,2"
				*!*		ELSE
				*!*			xgroupby="4,1,2"
				*!*		ENDIF

				*!*		SELECT ;
				*!*			PADR(acctname(accountid),30) AS acctname, ;
				*!*			PADR(billname(billtoid),30) AS billname, ;
				*!*			PADR(compname(company),40) AS compname, ;
				*!*			company, ;
				*!*			docdt, ;
				*!*			accountid, ;
				*!*			billtoid, ;
				*!*			SPACE(20) AS salesman, ;
				*!*			0000000 AS credlimit, ;
				*!*			SUM(amt1) AS amt1, ;
				*!*			SUM(amt2) AS amt2, ;
				*!*			SUM(amt3) AS amt3, ;
				*!*			SUM(amt4) AS amt4, ;
				*!*			SUM(amt5) AS amt5, ;
				*!*			SUM(amt6) AS amt6, ;
				*!*			SUM(amt7) AS amt7, ;
				*!*			SUM(amt8) AS amt8, ;
				*!*			SUM(amt9) AS amt9, ;
				*!*			SUM(amt10) AS amt10 ;
				*!*			FROM xtemp ;
				*!*			WHERE &xfilter ;
				*!*			GROUP BY &xgroupby ;
				*!*			ORDER BY &xgroupby ;
				*!*			INTO CURSOR xaging READWRITE

				*!*		DO CASE
				*!*			CASE gcompany="t"
				*!*				REPLACE ALL compname WITH "TRUCKING", company WITH "t" IN xaging
				*!*			CASE gcompany="w"
				*!*				REPLACE ALL compname WITH "WAREHOUSING", company WITH "W" IN xaging
				*!*		ENDCASE

				*!*	ELSE
				
					IF !EMPTY(gasofdt)
						xfilter="docdt<=gasofdt"
					ELSE
						xfilter=".t."
					ENDIF

					SELECT ;
						PADR(acctname(accountid),30) AS acctname, ;
						PADR(billname(billtoid),30) AS billname, ;
						accountid, ;
						billtoid, ;
						SPACE(20) AS salesman, ;
						'' AS company, ;
						SPACE(40) AS compname, ;
						docdt, ;
						0000000 AS credlimit, ;
						SUM(amt1) AS amt1, ;
						SUM(amt2) AS amt2, ;
						SUM(amt3) AS amt3, ;
						SUM(amt4) AS amt4, ;
						SUM(amt5) AS amt5, ;
						SUM(amt6) AS amt6, ;
						SUM(amt7) AS amt7, ;
						SUM(amt8) AS amt8, ;
						SUM(amt9) AS amt9, ;
						SUM(amt10) AS amt10 ;
						FROM xrpt ;
						WHERE &xfilter ;
						GROUP BY 1,2 ;
						ORDER BY 1,2 ;
						INTO CURSOR xaging READWRITE
						
				*!*	ENDIF

				DO CASE
					CASE gover45
						xrange1="45-90 Days"
						xrange2="91-120 Days"
						xrange3="121-150 Days"
						xrange4="151-180 Days"
						xrange5="> 181 Days"
						xrange6=""
						xrange7=""
					CASE gover90
						xrange1="91-120 Days"
						xrange2="121-150 Days"
						xrange3="151-180 Days"
						xrange4="181-210 Days"
						xrange5="> 210 Days"
						xrange6=""
						xrange7=""
					CASE gijoe
						xrange1="0-30 Days"
						xrange2="31-60"
						xrange3="61-90"
						xrange4="91-120"
						xrange5="121-150"
						xrange6="151-180"
						xrange7="181-210"
						xrange8="211-240"
						xrange9="241-270"
						xrange10=">270"
					OTHERWISE
						xrange1="0-30 Days"
						xrange2="31-60 Days"
						xrange3="61-90 Days"
						xrange4="> 90 Days"
				ENDCASE

				SET FILTER TO !(amt1=0 AND amt2=0 AND amt3=0 AND amt4=0 AND amt5=0 AND amt6=0 AND amt7=0 AND amt8=0 AND amt9=0 AND amt10=0) && and acctname#"CAYSET"

				DO CASE
					CASE gover90 OR gover45
						grptname="aragingov90"
					CASE gijoe
						grptname="aragingall"

						SELECT xaging
						SCAN
							=SEEK(accountid,"account","accountid")
							REPLACE salesman WITH salesman(account.salesmanid) IN xaging
							=SEEK(billtoid,"billto","billtoid")
							REPLACE credlimit WITH billto.credlimit IN xaging
						ENDSCAN
				ENDCASE

				*!*	if thisform.chkpmbase8.Value
				*!*		select acctname, accountid, billname, salesman, company, compname, docdt, credlimit, ;
				*!*			sum(amt1) as amt1, sum(amt2) as amt2, sum(amt3) as amt3, sum(amt4) as amt4, sum(amt5) as amt5, ;
				*!*			sum(amt6) as amt6, sum(amt7) as amt7, sum(amt8) as amt8, sum(amt9) as amt9, sum(amt10) as amt10 ;
				*!*			from xaging group by accountid into cursor xaging2
				*!*	endif

				*!*	IF gwilmington
				*!*		REPLACE ALL compname WITH "WILMINGTON YARD"
				*!*	ENDIF
				*!*	IF g331
				*!*		REPLACE ALL compname WITH "INVOICES 3/31 & EARLIER"
				*!*	ENDIF

				*this.zzprintreport()
				

				* replace all data in AGINGO45 with xaging
				
				*first delete all recs in aging045
				xsqlexec("delete from agingo45 where 1=1",,,"ar")
				
				* then open as CA for inserts into
				useca("agingo45","ar",,,"select * from agingo45 where 1 = 0",,"agingo45")

				SELECT xaging
				SCAN
					m.asofdate=gasofdt
					m.accountid=xaging.accountid
					m.billtoid=xaging.billtoid
					m.acctname=xaging.acctname
					m.billname=xaging.billname
					m.AMT45_90=xaging.amt1
					m.AMT91_120=xaging.amt2
					m.AMT121_150=xaging.amt3
					m.AMT151_180=xaging.amt4
					m.AMT181_PLUS=xaging.amt5
					m.TOTAL=xaging.amt1 + xaging.amt2 + xaging.amt3 + xaging.amt4 + xaging.amt5
					insertinto("agingo45","ar",.t.)
				endscan
				
				*tu("agingo45")

				IF .lTestMode THEN
					SELECT agingo45
					BROWSE
					.TrackProgress('TEST MODE: did not TU(agingo45)',LOGIT+SENDIT)
				ELSE					
					* tableupate SQL
					IF tu("agingo45") THEN
						.TrackProgress('SUCCESS: TU(agingo45) returned .T.!',LOGIT+SENDIT)
					ELSE
						* ERROR on table update
						.TrackProgress('ERROR: TU(agingo45) returned .F.!',LOGIT+SENDIT)
						=AERROR(UPCERR)
						IF TYPE("UPCERR")#"U" THEN 
							.TrackProgress('Error Description = ' + UPCERR[1,2],LOGIT+SENDIT)
						ENDIF
					ENDIF
				ENDIF

				*!*	SELECT AGINGO45
				*!*	browse

				*COPY TO c:\a\aging.xls xl5

				WAIT CLEAR

				***************

				.TrackProgress('AR Aging process ended normally.', LOGIT+SENDIT)

			CATCH TO loError

				.TrackProgress('There was an error.',LOGIT+SENDIT)
				.TrackProgress('The Error # is: ' + TRANSFORM(loError.ERRORNO), LOGIT+SENDIT)
				.TrackProgress('The Error Message is: ' + TRANSFORM(loError.MESSAGE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred in Program: ' + TRANSFORM(loError.PROCEDURE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred at line #: ' + TRANSFORM(loError.LINENO), LOGIT+SENDIT)
				*.TrackProgress('SPREADSHEET ROW = ' + TRANSFORM(.nRow), LOGIT+SENDIT)
				lnNumberOfErrors = lnNumberOfErrors + 1
				.lSendInternalEmailIsOn = .T.
				IF TYPE('.oExcel') = 'O' AND NOT ISNULL(.oExcel) THEN
					.oExcel.QUIT()
				ENDIF

			ENDTRY

			WAIT CLEAR
			***************** INTERNAL email results ******************************
			.TrackProgress('About to send status email.',LOGIT+SENDIT)
			.TrackProgress('The logfile is: ' + .cLogFile, LOGIT+SENDIT)
			.TrackProgress('AR Aging process started: ' + .cStartTime, LOGIT+SENDIT)
			.TrackProgress('AR Aging process finished: ' + TTOC(DATETIME()), LOGIT+SENDIT)

			IF .lSendInternalEmailIsOn THEN
				DO FORM dartmail2 WITH .cSendTo,.cFrom,.cSubject,.cCC,.cAttach,.cBodyText,"A"
				.TrackProgress('Sent status email.',LOGIT)
			ELSE
				.TrackProgress('Did not send status email: THIS.lSendInternalEmailIsOn = FALSE.',LOGIT+SENDIT)
			ENDIF

		ENDWITH
		RETURN
	ENDFUNC && main




	PROCEDURE TrackProgress
		* do any combination of Wait Window, writing to logfile, and adding to body of email,
		* based on nFlags parameter.
		LPARAMETERS tcExpression, tnFlags
		WITH THIS
			IF BITAND(tnFlags,LOGIT) = LOGIT THEN
				IF .lLoggingIsOn THEN
					?
					? .cProcessName + "  " + TTOC(DATETIME()) + ": " + tcExpression
				ENDIF
			ENDIF
			IF .lWaitWindowIsOn THEN
				IF BITAND(tnFlags,NOWAITIT) = NOWAITIT THEN
					WAIT WINDOW tcExpression NOWAIT
				ENDIF
				IF BITAND(tnFlags,WAITIT) = WAITIT THEN
					WAIT WINDOW tcExpression TIMEOUT .nWaitWindowTimeout
				ENDIF
			ENDIF
			IF BITAND(tnFlags,SENDIT) = SENDIT THEN
				IF .lSendInternalEmailIsOn THEN
					.cBodyText = .cBodyText + tcExpression + CRLF + CRLF
				ENDIF
			ENDIF
		ENDWITH
	ENDPROC  &&  TrackProgress


	PROCEDURE SortArrayByDateTime
		* expects taInArray[] to be the result of an ADIR()
		LPARAMETERS taInArray, taOutArray
		EXTERNAL ARRAY taInArray, taOutArray
		LOCAL lnRows, lnCols, i, j
		* extending taInArray[] to process the files in date/time order
		lnRows = ALEN(taInArray,1)
		lnCols = ALEN(taInArray,2)
		* create taOutArray with one more column
		DIMENSION taOutArray[lnRows,lnCols + 1]
		* and fill it with taInArray columns AND (filedate + filetime) in new column
		* ( ACOPY() doesn't preserve elements properly because of different # of columns )
		FOR i = 1 TO lnRows
			FOR j = 1 TO lnCols
				taOutArray[i,j] = taInArray[i,j]
			ENDFOR
			taOutArray[i,lnCols + 1] = DTOS(taInArray[i,3]) + "_" + taInArray[i,4]
		ENDFOR
		* sort taOutArray on date/time column
		=ASORT(taOutArray,6)
	ENDPROC

ENDDEFINE
