* Create spreadsheet of the last punch date for Temp associates for a particular temp agency.
*
* MB 4/19/2018
*
* Requested by Yvonne Ortiz from Chartwell SP; agency code X 4/19/2018.


LPARAMETERS tcTempCo

utilsetup("KRONOSGETLASTPUNCHES")

LOCAL loKRONOSGETLASTPUNCHES, lcTempCo


loKRONOSGETLASTPUNCHES = CREATEOBJECT('KRONOSGETLASTPUNCHES')
IF TYPE('tcTempCo') = "L" THEN
	tcTempCo = "?"
ENDIF
lcTempCo = ALLTRIM(tcTempCo)
loKRONOSGETLASTPUNCHES.SetTempCo( lcTempCo )
loKRONOSGETLASTPUNCHES.MAIN()

schedupdate()

CLOSE DATABASES ALL

RETURN

#DEFINE LOGIT 1
#DEFINE WAITIT 2
#DEFINE NOWAITIT 4
#DEFINE SENDIT 8
#DEFINE CRLF CHR(13) + CHR(10)
#DEFINE LF CHR(10)
#DEFINE RETURN_DATA_MANDATORY .F.
#DEFINE RETURN_DATA_NOT_MANDATORY .T.

DEFINE CLASS KRONOSGETLASTPUNCHES AS CUSTOM

	cProcessName = 'KRONOSGETLASTPUNCHES'

	lTestMode = .F.  && .F. FOR NORMAL OPERATION, .T. TO SUPPRESS ALL BUT DEVELOPER EMAILS AND CHANGE INPUT TABLE, ETC.

	* which payroll server, and associated connections, to hit
	lUsePAYROLL2Server = .T.

	lAutoYield = .T.

	cStartTime = TTOC(DATETIME())

	* connection properties
	nSQLHandle = 0

	* file properties
	nFileHandle = 0


	* report filter properties
	cTempCo = ""
	cTempCoName = ""

	* date properties
	dtNow = DATETIME()

	dToday = DATE()

	* wait window properties
	nWaitWindowTimeout = 2
	lWaitWindowIsOn = .T.

	* logfile properties
	lLoggingIsOn = .T.
	cLogFile = 'F:\UTIL\KRONOS\Logfiles\KRONOSGETLASTPUNCHES_log.txt'

	* INTERNAL email properties
	lSendInternalEmailIsOn = .T.
	cFrom = 'mark.bennett@tollgroup.com'
	cSendTo = 'mbennett@fmiint.com'
	cCC = ''
	cSubject = 'Kronos Last Punches Report for: ' + DTOC(DATE())
	cAttach = ''
	cBodyText = ''

	cCOMPUTERNAME = ''
	cUSERNAME = ''

	FUNCTION INIT
		IF NOT DODEFAULT()
			RETURN .F.
		ENDIF
		WITH THIS
			CLEAR
			*SET RESOURCE OFF
			CLOSE DATA
			SET CENTURY ON
			SET DATE AMERICAN
			SET HOURS TO 24
			SET ANSI ON
			SET TALK OFF
			SET DELETED ON
			SET CONSOLE OFF
			SET EXCLUSIVE OFF
			SET SAFETY OFF
			SET EXACT OFF
			SET STATUS BAR ON
			SET SYSMENU OFF
			SET ENGINEBEHAVIOR 70
			_VFP.AUTOYIELD = .lAutoYield
			.cCOMPUTERNAME = GETENV("COMPUTERNAME")
			.cUSERNAME = GETENV("USERNAME")
			IF .lTestMode THEN
				.cSendTo = 'mbennett@fmiint.com'
				.cLogFile = 'F:\UTIL\KRONOS\Logfiles\KRONOSGETLASTPUNCHES_log_TESTMODE.txt'
			ENDIF
			.lLoggingIsOn = .lLoggingIsOn AND NOT EMPTY(.cLogFile)
		ENDWITH
	ENDFUNC


	FUNCTION DESTROY
		WITH THIS
			IF .lLoggingIsOn  THEN
				SET ALTERNATE OFF
				SET ALTERNATE TO
			ENDIF
		ENDWITH
		DODEFAULT()
	ENDFUNC


	FUNCTION MAIN
		WITH THIS
			LOCAL lnNumberOfErrors, lcSQL, lcSQL2, lcSQL3, lcSQL4, loError, llValid
			LOCAL lcXLFileName, lcTempCoWhere, lcTempCoWhere2, lcTempCoWhere3, lcTestModeWhere, lcTestModeWhere2
			LOCAL oExcel, oWorkbook, oWorksheet, lnRow, lcRow
			LOCAL lcColsToProcess, lnColumn, lcBoldColumn, i, j, lcBoldRow, luCellValue, lcTargetDirectory, lcFiletoSaveAs
			LOCAL lcFileDate, lcTempCo, lcTitle, llSaveAgain, lcFileDate
			LOCAL lcSendTo, lcCC, lcBodyText, ld14DaysAgo, lcTopBodytext, lt14DaysAgo
			LOCAL lcSQLActivePersons, lcPersonnum

			TRY
				lnNumberOfErrors = 0

				IF .lLoggingIsOn THEN
					SET ALTERNATE TO (.cLogFile) ADDITIVE
					SET ALTERNATE ON
				ENDIF

				lcTopBodytext = ""

				.TrackProgress('Kronos Last Punches Report process started....', LOGIT+SENDIT+NOWAITIT)
				.TrackProgress('COMPUTERNAME = ' + .cCOMPUTERNAME, LOGIT+SENDIT)
				.TrackProgress('USERNAME = ' + .cUSERNAME, LOGIT+SENDIT)
				.TrackProgress('PROJECT = KRONOSGETLASTPUNCHES', LOGIT+SENDIT)
				IF .lTestMode THEN
					.TrackProgress('                        TEST MODE                        ', LOGIT+SENDIT)
				ENDIF

				IF EMPTY(.cTempCoName) THEN
					.TrackProgress('Missing or Invalid Temp CO Parameter!', LOGIT+SENDIT)
					THROW
				ENDIF

				IF USED('CUR_ALL') THEN
					USE IN CUR_ALL
				ENDIF

				IF USED('CUR_ALL2') THEN
					USE IN CUR_ALL2
				ENDIF
				IF USED('CUR_ALL3') THEN
					USE IN CUR_ALL3
				ENDIF

				.cSubject = 'Last Punches Report for ' + .cTempCoName + ' ' +  DTOC(.dToday)

				* filter by TempCo if there was a passed TempCo parameters
				lcTempCoWhere = " AND (A.LABORLEVELNAME5 = '" + .cTempCo + "') "
				lcTempCoWhere2 = " AND (HOMELABORLEVELNM5 = '" + .cTempCo + "') "
				lcTempCoWhere3 = " AND (LABORLEVELNAME5 = '" + .cTempCo + "') "

				SET TEXTMERGE ON
				TEXT TO	lcSQLActivePersons NOSHOW

SELECT
PERSONFULLNAME AS NAME,
PERSONNUM,
HOMELABORACCTNAME AS LABRACCT,
EMPLOYMENTSTATUS AS STATUS
FROM VP_EMPLOYEEV42
WHERE HOMELABORLEVELNM1 = 'TMP'
<<lcTempCoWhere2>>
ORDER BY PERSONFULLNAME

				ENDTEXT
				SET TEXTMERGE OFF


				*IF .lTestMode THEN
				.TrackProgress('', LOGIT+SENDIT)
				.TrackProgress('lcSQLActivePersons =' + lcSQLActivePersons, LOGIT+SENDIT)
				*ENDIF

				.TrackProgress('Connecting to WTK System....', LOGIT+SENDIT)

				WAIT WINDOW NOWAIT "Preparing data..."

				.nSQLHandle = SQLCONNECT("WTK2","sa","kr0n1tes")

				IF .nSQLHandle > 0 THEN

					IF .ExecSQL(lcSQLActivePersons, 'CUR_ALL', RETURN_DATA_MANDATORY, .T.) THEN

						IF USED('CUR_ALL') AND NOT EOF('CUR_ALL') THEN


							SELECT NAME, PERSONNUM, LABRACCT, STATUS, {} AS LASTPUNCH ;
								FROM CUR_ALL ;
								INTO CURSOR CUR_ALL2 ;
								READWRITE

							*SELECT CUR_ALL2
							*BROWSE

							*THROW

							* LOOP THRU ACTIVE ASSOCIATES AND RETRIEVE THE LATEST PAY CODE DATE FOR EACH
							SELECT CUR_ALL2
							SCAN

								WAIT WINDOW NOWAIT CUR_ALL2.NAME

								lcPersonnum = CUR_ALL2.PERSONNUM

								SET TEXTMERGE ON
								TEXT TO	lcSQLMax NOSHOW

SELECT MAX(APPLYDATE) AS APPLYDATE
FROM VP_TOTALS
WHERE (PERSONNUM = '<<lcPersonnum>>') AND (PAYCODETYPE = 'P') AND (TIMEINSECONDS > 0)

								ENDTEXT
								SET TEXTMERGE OFF

								IF USED('CURMAXAPPLY') THEN
									USE IN CURMAXAPPLY
								ENDIF

								IF .ExecSQL(lcSQLMax, 'CURMAXAPPLY', RETURN_DATA_MANDATORY, .F.) THEN

									IF USED('CURMAXAPPLY') AND NOT EOF('CURMAXAPPLY') THEN
										REPLACE CUR_ALL2.LASTPUNCH WITH NVL(CURMAXAPPLY.APPLYDATE,{}) IN CUR_ALL2
									ELSE
										REPLACE CUR_ALL2.LASTPUNCH WITH {} IN CUR_ALL2
									ENDIF

								ENDIF

							ENDSCAN

							WAIT CLEAR



							*BROWSE

							*!*							SELECT * ;
							*!*								FROM CUR_ALL2 ;
							*!*								INTO CURSOR CUR_ALL3 ;
							*!*								WHERE NOT EMPTY(LASTPUNCH ) ;
							*!*								ORDER BY NAME

							*!*							SELECT CUR_ALL3
							*!*							BROWSE

							lcXLFileName = 'F:\UTIL\KRONOS\TEMPTIMEREPORTS\' + .cTempCo + '_LASTPUNCHES.XLS'

							SELECT CUR_ALL2
							COPY TO (lcXLFileName) XL5

							IF FILE(lcXLFileName) THEN
								.cAttach = lcXLFileName
								.TrackProgress('Kronos Last Punches Report process ended normally.', LOGIT+SENDIT+NOWAITIT)
							ELSE
								.TrackProgress('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!', LOGIT+SENDIT)
								.TrackProgress("ERROR: There was a problem creating File : " + lcXLFileName, LOGIT+SENDIT)
								.TrackProgress('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!', LOGIT+SENDIT)
							ENDIF
						ELSE
							lcTopBodytext = "There was no data."
						ENDIF  && USED('CUR_ALL')

						.cBodyText = lcTopBodytext + CRLF + CRLF + "======= report log follows ========" + CRLF + CRLF + .cBodyText

					ENDIF  &&  .ExecSQL(...)

					CLOSE DATABASES ALL
				ELSE
					* connection error
					.TrackProgress('Unable to connect to WTK System.', LOGIT+SENDIT)
				ENDIF   &&  .nSQLHandle > 0

			CATCH TO loError

				.TrackProgress('There was an error.',LOGIT+SENDIT)
				.TrackProgress('The Error # is: ' + TRANSFORM(loError.ERRORNO), LOGIT+SENDIT)
				.TrackProgress('The Error Message is: ' + TRANSFORM(loError.MESSAGE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred in Program: ' + TRANSFORM(loError.PROCEDURE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred at line #: ' + TRANSFORM(loError.LINENO), LOGIT+SENDIT)
				lnNumberOfErrors = lnNumberOfErrors + 1
				IF TYPE('oExcel') = 'O' AND NOT ISNULL(oExcel) THEN
					oExcel.QUIT()
				ENDIF
				CLOSE DATA
				CLOSE ALL

			ENDTRY

			CLOSE DATA
			WAIT CLEAR
			***************** INTERNAL email results ******************************
			.TrackProgress('About to send status email.',LOGIT)
			.TrackProgress('==================================================================================================================', SENDIT)
			.TrackProgress('The logfile is: ' + .cLogFile, SENDIT)
			.TrackProgress('Kronos Last Punches Report process started: ' + .cStartTime, LOGIT+SENDIT)
			.TrackProgress('Kronos Last Punches Report process finished: ' + TTOC(DATETIME()), LOGIT+SENDIT)

			IF .lSendInternalEmailIsOn THEN
				* try to trap error from not having dartmail dll's registered on user's pc...

				TRY
					DO FORM dartmail2 WITH .cSendTo,.cFrom,.cSubject,.cCC,.cAttach,.cBodyText,"A"
					.TrackProgress('Sent status email.',LOGIT)
				CATCH TO loError
					*=MESSAGEBOX('There was an error sending the status email. Please contact Mark Bennett.',0+64,"KRONOS Temp Time")
					.TrackProgress('There was an error sending the status email.',LOGIT)
					.TrackProgress('The Error # is: ' + TRANSFORM(loError.ERRORNO), LOGIT)
					.TrackProgress('The Error Message is: ' + TRANSFORM(loError.MESSAGE), LOGIT)
					.TrackProgress('The Error occurred in Program: ' + TRANSFORM(loError.PROCEDURE), LOGIT)
					.TrackProgress('The Error occurred at line #: ' + TRANSFORM(loError.LINENO), LOGIT)
					lnNumberOfErrors = lnNumberOfErrors + 1
					CLOSE DATA
				ENDTRY

			ELSE
				.TrackProgress('Did not send status email: THIS.lSendInternalEmailIsOn = FALSE.',LOGIT)
			ENDIF

		ENDWITH
		RETURN
	ENDFUNC && main


	FUNCTION ExecSQL
		LPARAMETERS tcSQL, tcCursorName, tlNoDataReturnedIsOkay, tlLogSQLErrorInEmail
		LOCAL llRetval, lnResult
		WITH THIS
			* close target cursor if it's open
			IF USED(tcCursorName)
				USE IN (tcCursorName)
			ENDIF
			WAIT WINDOW "Fetching remote data; please wait..." NOWAIT
			lnResult = SQLEXEC(.nSQLHandle, tcSQL, tcCursorName)
			llRetval = ( lnResult > 0 )
			IF llRetval THEN
				* see if any data came back
				IF NOT tlNoDataReturnedIsOkay THEN
					IF (NOT USED(tcCursorName)) OR EOF(tcCursorName) THEN
						llRetval = .F.
						.TrackProgress('No data was returned by this query: ' +CRLF + tcSQL, LOGIT+SENDIT)
					ENDIF
				ENDIF
			ELSE
				IF tlLogSQLErrorInEmail THEN
					.TrackProgress("Result Code [" + TRANSFORM(lnResult) + "] returned by this query: " + tcSQL, LOGIT+SENDIT)
				ENDIF
			ENDIF
			WAIT CLEAR
			RETURN llRetval
		ENDWITH
	ENDFUNC


	PROCEDURE TrackProgress
		* do any combination of Wait Window, writing to logfile, and adding to body of email,
		* based on nFlags parameter.
		LPARAMETERS tcExpression, tnFlags
		WITH THIS
			IF BITAND(tnFlags,LOGIT) = LOGIT THEN
				IF .lLoggingIsOn THEN
					?
					? .cProcessName + "  " + TTOC(DATETIME()) + ": " + tcExpression
				ENDIF
			ENDIF
			IF .lWaitWindowIsOn THEN
				IF BITAND(tnFlags,NOWAITIT) = NOWAITIT THEN
					WAIT WINDOW tcExpression NOWAIT
				ENDIF
				IF BITAND(tnFlags,WAITIT) = WAITIT THEN
					WAIT WINDOW tcExpression TIMEOUT .nWaitWindowTimeout
				ENDIF
			ENDIF
			IF BITAND(tnFlags,SENDIT) = SENDIT THEN
				IF .lSendInternalEmailIsOn THEN
					.cBodyText = .cBodyText + tcExpression + CRLF + CRLF
				ENDIF
			ENDIF
		ENDWITH
	ENDPROC  &&  TrackProgress


	PROCEDURE SetTempCo
		LPARAMETERS tcTempCo
		WITH THIS
			DO CASE
				CASE tcTempCo = 'A'
					.cTempCo = tcTempCo
					.cTempCoName = 'Tri-State SP '
					.TrackProgress('Set TempCo = ' + tcTempCo,LOGIT+SENDIT)
					.TrackProgress('Set TempCoName = ' + .cTempCoName,LOGIT+SENDIT)
				CASE tcTempCo = 'B'
					.cTempCo = tcTempCo
					.cTempCoName = 'Select Staffing ML '
					.TrackProgress('Set TempCo = ' + tcTempCo,LOGIT+SENDIT)
					.TrackProgress('Set TempCoName = ' + .cTempCoName,LOGIT+SENDIT)
				CASE tcTempCo = 'D'
					.cTempCo = tcTempCo
					.cTempCoName = 'Quality NJ'
					.TrackProgress('Set TempCo = ' + tcTempCo,LOGIT+SENDIT)
					.TrackProgress('Set TempCoName = ' + .cTempCoName,LOGIT+SENDIT)
				CASE tcTempCo = 'J'
					.cTempCo = tcTempCo
					.cTempCoName = 'Diamond Staffing - NJ'
					.TrackProgress('Set TempCo = ' + tcTempCo,LOGIT+SENDIT)
					.TrackProgress('Set TempCoName = ' + .cTempCoName,LOGIT+SENDIT)
				CASE tcTempCo = 'K'
					.cTempCo = tcTempCo
					.cTempCoName = 'Tri-State ML'
					.TrackProgress('Set TempCo = ' + tcTempCo,LOGIT+SENDIT)
					.TrackProgress('Set TempCoName = ' + .cTempCoName,LOGIT+SENDIT)
				CASE tcTempCo = 'L'
					.cTempCo = tcTempCo
					.cTempCoName = 'CRSCO - Louisville'
					.TrackProgress('Set TempCo = ' + tcTempCo,LOGIT+SENDIT)
					.TrackProgress('Set TempCoName = ' + .cTempCoName,LOGIT+SENDIT)
				CASE tcTempCo = 'N'
					.cTempCo = tcTempCo
					.cTempCoName = 'LaborReady - Louisville'
					.TrackProgress('Set TempCo = ' + tcTempCo,LOGIT+SENDIT)
					.TrackProgress('Set TempCoName = ' + .cTempCoName,LOGIT+SENDIT)
				CASE tcTempCo = 'O'
					.cTempCo = tcTempCo
					.cTempCoName = 'Express Staffing - Louisville'
					.TrackProgress('Set TempCo = ' + tcTempCo,LOGIT+SENDIT)
					.TrackProgress('Set TempCoName = ' + .cTempCoName,LOGIT+SENDIT)
				CASE tcTempCo = 'P'
					.cTempCo = tcTempCo
					.cTempCoName = 'Packaging & More LLC - Louisville'
					.TrackProgress('Set TempCo = ' + tcTempCo,LOGIT+SENDIT)
					.TrackProgress('Set TempCoName = ' + .cTempCoName,LOGIT+SENDIT)
				CASE tcTempCo == 'Q'
					.cTempCo = tcTempCo
					.cTempCoName = 'Cor-Tech - Louisville'
					.TrackProgress('Set TempCo = ' + tcTempCo,LOGIT+SENDIT)
					.TrackProgress('Set TempCoName = ' + .cTempCoName,LOGIT+SENDIT)
				CASE tcTempCo == 'R'
					.cTempCo = tcTempCo
					.cTempCoName = 'Staffing Solutions - SP'
					.TrackProgress('Set TempCo = ' + tcTempCo,LOGIT+SENDIT)
					.TrackProgress('Set TempCoName = ' + .cTempCoName,LOGIT+SENDIT)
				CASE INLIST(tcTempCo,'S')
					.cTempCo = tcTempCo
					.cTempCoName = 'Kamran Staffing - ML'
					.TrackProgress('Set TempCo = ' + tcTempCo,LOGIT+SENDIT)
					.TrackProgress('Set TempCoName = ' + .cTempCoName,LOGIT+SENDIT)
				CASE tcTempCo == 'T'
					.cTempCo = tcTempCo
					.cTempCoName = 'Staffing Solutions - ML'
					.TrackProgress('Set TempCo = ' + tcTempCo,LOGIT+SENDIT)
					.TrackProgress('Set TempCoName = ' + .cTempCoName,LOGIT+SENDIT)
				CASE tcTempCo == 'U'
					.cTempCo = tcTempCo
					.cTempCoName = 'Simplified Labor Staffing Solutions - Carson'
					.TrackProgress('Set TempCo = ' + tcTempCo,LOGIT+SENDIT)
					.TrackProgress('Set TempCoName = ' + .cTempCoName,LOGIT+SENDIT)
				CASE tcTempCo == 'V'
					.cTempCo = tcTempCo
					.cTempCoName = 'Simplified Labor Staffing Solutions - Carson 2'
					.TrackProgress('Set TempCo = ' + tcTempCo,LOGIT+SENDIT)
					.TrackProgress('Set TempCoName = ' + .cTempCoName,LOGIT+SENDIT)
				CASE tcTempCo == 'X'
					.cTempCo = tcTempCo
					.cTempCoName = 'Chartwell Staffing Solutions - San Pedro'
					.TrackProgress('Set TempCo = ' + tcTempCo,LOGIT+SENDIT)
					.TrackProgress('Set TempCoName = ' + .cTempCoName,LOGIT+SENDIT)
				CASE tcTempCo == 'Y'
					.cTempCo = tcTempCo
					.cTempCoName = 'Simplified Labor Staffing Solutions - Mira Loma'
					.TrackProgress('Set TempCo = ' + tcTempCo,LOGIT+SENDIT)
					.TrackProgress('Set TempCoName = ' + .cTempCoName,LOGIT+SENDIT)
				OTHERWISE
					.TrackProgress('INVALID TempCo = ' + tcTempCo,LOGIT+SENDIT)
					.cTempCoName = ''
			ENDCASE
		ENDWITH
	ENDPROC


ENDDEFINE
