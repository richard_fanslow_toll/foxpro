*!* NANJING USA 945 PickPack  UPS Creation Program
*!* Creation Date: 09.30.2005 by Joe from the standard P/P program

PARAMETERS cBOL,nWO_Num,cShip_ref,cOffice,lPTFlag
lMismatch = .F.

PUBLIC ARRAY thisarray(1)
PUBLIC cCustname,nUnitSum,cTrk,cUCC,cFileNameShort,lDoSQL,nOrigSeq,nOrigGrpSeq,lTestInput,cUsefolder,lTesting
PUBLIC cMod,cMBOL,cFilenameOut,cFilenameArch,cWO_NumList,lcPath,cEDIType,cISA_Num,cProgName,lDoCatch
PUBLIC cErrMsg,lNanFilesOut,lOverflow
cProgName = "nanjing945pp_create_ups.prg"

tfrom = "Toll EDI Operations <toll-edi-ops@tollgroup.com>"
nRec1 = 9999999
lDoSQL = .T.

lTesting = .F.
lTestInput = .F.

DO m:\dev\prg\_setvars WITH lTesting
ON ESCAPE CANCEL

lIsError = .F.
lDoCatch = .T.
lDoManUC = .F.
cUCC = "XXX"
cTrk = " "
cErrMsg = ""
IF !lPTFlag
	cShip_ref = ""
ENDIF

STORE "" TO cWO_NumList
lCloseOutput = .T.
nFilenum = 0
nOrigGrpSeq = 0
nOrigSeq = 0
TRY
	WAIT "" TIMEOUT 1
	nAcctNum = 4610
	cPPName = "Nanjing"

	lISAFlag = .T.
	lSTFlag = .T.
	lCloseOutput = .T.
	lOverflow = .f.

	IF TYPE("cOffice") = "L"
		IF !lTesting
			WAIT WINDOW "Office not provided...terminating" TIMEOUT 3
			lCloseOutput = .F.
			cErrMsg = "No OFFICE"
			DO ediupdate WITH cErrMsg,.T.
			THROW
		ELSE
			CLOSE DATABASES ALL
			cOffice = "C"
			nWO_Num = 4021410
			cBOL = "927564570311044"
		ENDIF
	ENDIF

	cMBOL = ""
	cEDIType = "945"
	lParcelType = .T.
	cMod = IIF(cOffice = "C","1",cOffice)
	goffice = cMod
	gMasterOffice = IIF(cOffice = "I","N",cOffice)
	cBOL = ALLTRIM(cBOL)

	CREATE CURSOR qtycompare (ship_ref c(10), osqty i, ppqty i,pctdiff i)
	CREATE CURSOR temp945 (accountid i,isa_num c(10),st_num c(10),wo_num i,bol_no c(20),ship_ref c(20),filename c(50),filedate T)

	IF TYPE("nWO_Num")<> "N"
		lCloseOutput = .F.
		cErrMsg = "BAD WO#"
		DO ediupdate WITH cErrMsg,.T.
		THROW
	ENDIF
	cWO_Num = ALLTRIM(STR(nWO_Num))
	cWO_NumList = cWO_Num
	cPadWO_Num = PADR(cWO_Num,10)
	lEmail = .T.
	lTestMail = IIF(lTesting,.T.,.F.) && Sends mail to Joe only
	lNanFilesOut = IIF(lTesting,.F.,.T.) && If true, copies output to FTP folder (default = .t.)
	lStandalone = IIF(lTesting,.T.,.F.)
	STORE "LB" TO cWeightUnit
	lPrepack = .F.
	lPick = .T.
	cUseFolder = "F:\WHY\WHDATA\"
	IF lTestInput
		cUsefolder = "F:\WHP\WHDATA\"
	ENDIF
	DO m:\dev\prg\swc_cutctns WITH cBOL

*!*	lNanFilesout = .F.
*!*	lTestMail = .T.

	IF !lTesting
		IF !USED('nanjing_wohold')
			USE F:\3pl\DATA\nanjing_wohold IN 0
		ENDIF

		nWO_numOld=nanjing_wohold.wo_num
		IF nWO_Num = nWO_numOld
			lDoSQL = .F.
		ELSE
			REPLACE wo_num WITH nWO_Num IN nanjing_wohold
		ENDIF
	ENDIF

	lSkipCompare= IIF(nAcctNum = 4694 AND INLIST(cOffice,"I","N"),.T.,.F.)
	lSkipCompare = .T.
	IF lSkipCompare OR !lDoSQL
		WAIT WINDOW "Skipping Labels/Cartons Comparison..." TIMEOUT 1
	ELSE
		cRetMsg = "X"
		DO m:\dev\prg\sqldata-COMPARENAN WITH cUsefolder,cBOL,nWO_Num,cOffice,nAcctNum,cPPName
		IF cRetMsg<>"OK"
			lCloseOutput = .F.
			cErrMsg = "SQL ERROR"
			DO ediupdate WITH cErrMsg,.T.
			THROW
		ENDIF
	ENDIF

	PUBLIC c_CntrlNum,c_GrpCntrlNum,cReturned
	WAIT WINDOW "Now preparing tables and variables" NOWAIT NOCLEAR

*!* SET CUSTOMER CONSTANTS
	cCustname = "NANJING"  && Customer Identifier
	cX12 = "004050"  && X12 Standards Set used
	cMailName = "Nanjing USA"
	SELECT 0
	USE F:\3pl\DATA\mailmaster ALIAS mm
	LOCATE FOR edi_type = '945' AND mm.office = cOffice AND mm.accountid = nAcctNum
	tsendto = IIF(mm.use_alt,sendtoalt,mm.sendto)
	tcc = IIF(mm.use_alt,mm.ccalt,mm.cc)
	LOCATE
	LOCATE FOR mm.office = 'X' AND mm.accountid = 9999
	tsendtoerr = IIF(mm.use_alt,sendtoalt,mm.sendto)
	tccerr = IIF(mm.use_alt,mm.ccalt,mm.cc)
	LOCATE
	LOCATE FOR mm.edi_type = 'MISC' AND mm.taskname = 'GENERAL'
	tsendtotest = IIF(mm.use_alt,sendtoalt,mm.sendto)
	tcctest = IIF(mm.use_alt,mm.ccalt,mm.cc)
	LOCATE
	LOCATE FOR mm.edi_type = 'MISC' AND mm.taskname = 'NANQTYERR'
	tsendtoqty = IIF(mm.use_alt,sendtoalt,mm.sendto)
	tccqty = IIF(mm.use_alt,mm.ccalt,mm.cc)

*!* SET OTHER CONSTANTS
	DO CASE
	CASE cOffice = "M"
		cCustLoc =  "FL"
		cFMIWarehouse = ""
		cCustPrefix = "945"+"f"
		cDivision = "Florida"
*		cFolder = "WHM"
		cSF_Addr1  = "11400 NW 32ND AVE"
		cSF_CSZ    = "MIAMI*FL*33167"

	CASE cOffice = "N"
		cCustLoc =  "NJ"
		cFMIWarehouse = ""
		cCustPrefix = "945"+"j"
		cDivision = "New Jersey"
*		cFolder = "WHN"
		cSF_Addr1  = "800 FEDERAL BLVD"
		cSF_CSZ    =  "CARTERET*NJ*07008"

	OTHERWISE
		cCustLoc =  "C2"
		cFMIWarehouse = ""
		cCustPrefix = "945"+"y"
		cDivision = "Carson2"
*		cFolder = "WHC"
		cSF_Addr1  = "1000 E 223RD ST"
		cSF_CSZ    = "CARSON*CA*90745"
	ENDCASE
	cCustFolder = UPPER(cCustname)  && +"-"+cCustLoc
	lNonEDI = .F.  && Assumes not a Non-EDI 945
	lDoPAL = .T.
	STORE "" TO lcKey
	STORE 0 TO alength,nLength

*!* SET OTHER CONSTANTS
	dt1 = TTOC(DATETIME(),1)
	dtmail = TTOC(DATETIME())
	dt2 = DATETIME()
	cString = ""

	csendqual = "ZZ"
	csendid = "FMIF"
	crecqual = "01"
	crecid = "654351030"

	cfd = CHR(0x07)
	csegd = "~"
	nSegCtr = 0
	cterminator = ">" && Used at end of ISA segment
	cdate = DTOS(DATE())
	ctruncdate = RIGHT(cdate,6)
	ctime = LEFT(TIME(DATE()),2)+SUBSTR(TIME(DATE()),4,2)
	cfiledate = cdate+ctime
	cOrig = "J"
	nSTCount = 0
	cStyle = ""
	cPTString = ""
	nTotCtnWt = 0
	nTotCtnCount = 0
	nUnitSum = 0
	cTargetStyle = ""

** PAD ID Codes
	csendidlong = PADR(csendid,15," ")
	crecidlong = PADR(crecid,15," ")

	lcPath = ("f:\ftpusers\"+cCustFolder+"\945OUT\")
	cFilenameHold = ("f:\ftpusers\"+cCustFolder+"\945-Staging\"+cCustPrefix+dt1+".txt")
	cFileNameShort = JUSTFNAME(cFilenameHold)
	cFilenameArch = UPPER("f:\ftpusers\"+cCustFolder+"\945OUT\945ARCHIVE\"+cFileNameShort)
	cFilenameOut = (lcPath+cFileNameShort)
	IF lTesting
	cFilenameOut = (cFilenameOut+"test\")
	endif
	nFilenum = FCREATE(cFilenameHold)

	IF USED('outship')
		USE IN outship
	ENDIF
	IF USED('outdet')
		USE IN outdet
	ENDIF

	xsqlexec("select * from outship where accountid = "+TRANSFORM(nAcctNum)+" and bol_no = '"+cBOL+"'",,,"wh")
	INDEX on bol_no TAG bol_no
	INDEX on outshipid TAG outshipid
	INDEX on wo_num TAG wo_num

	selectoutdet()
	SELECT outdet
	INDEX ON outdetid TAG outdetid

	IF USED('SHIPMENT')
		USE IN shipment
	ENDIF

	csq1 = [select * from shipment where accountid = ]+TRANSFORM(nAcctNum)
	xsqlexec(csq1,,,"wh")
	INDEX ON shipmentid TAG shipmentid
	INDEX ON pickticket TAG pickticket

	IF USED('PACKAGE')
		USE IN package
	ENDIF

	csq1 = [select * from package where accountid = ]+TRANSFORM(nAcctNum)
	xsqlexec(csq1,,,"wh")
	INDEX ON shipmentid TAG shipmentid
	SET ORDER TO TAG shipmentid

	IF lDoSQL
		cRetMsg = "X"
		DO m:\dev\prg\sqlconnectnanjing_wo  WITH nWO_Num,nAcctNum,cCustname,cPPName,.T.,cOffice
		IF cRetMsg<>"OK"
			cErrMsg = cRetMsg
			DO ediupdate WITH cErrMsg,.T.
			THROW
		ENDIF
	ENDIF

*!* Added this code block to remove zero-qty cartons from SQL cursor
	SELECT ucc,SUM(totqty) AS uccqty FROM vnanjingpp GROUP BY 1 INTO CURSOR tempchk HAVING uccqty=0
	SELECT tempchk
	LOCATE
	IF !EOF()
		SCAN
			STORE ALLT(ucc) TO cUCC
			SELECT vnanjingpp
			LOCATE
			DELETE FOR ucc = cUCC
		ENDSCAN
	ENDIF
	USE IN tempchk

	CREATE CURSOR tempqtyerr (ship_ref c(20),ctnqty i,totqty i,cqty i,uqty i)

	SELECT ship_ref,outshipid,ctnqty,IIF("PREPACK-REP"$outship.shipins,.T.,.F.) AS PRR FROM outship WHERE bol_no = cBOL INTO CURSOR out1
	SELECT a.*,INT(VAL(b.PACK)) AS npack,b.totqty AS rawqty,b.origqty,0 AS totqty2 ;
		FROM out1 a LEFT JOIN outdet b ;
		ON a.outshipid = b.outshipid ;
		INTO CURSOR out2

	SELECT ship_ref,ctnqty,SUM(rawqty) AS qty ;
		FROM out2 ;
		GROUP BY 1 ;
		INTO CURSOR temptotals

	SELECT temptotals
	SCAN
		cSRUse = ALLTRIM(temptotals.ship_ref)
		SELECT vnanjingpp
		SUM totqty TO m.uqty FOR vnanjingpp.ship_ref = PADR(ALLTRIM(cSRUse),20)
		SELECT 0
		SELECT DISTINCT(ucc) FROM vnanjingpp WHERE vnanjingpp.ship_ref = PADR(ALLTRIM(cSRUse),20) INTO CURSOR tempxyz
		m.cqty = RECCOUNT()
		USE IN tempxyz
		IF (uqty # temptotals.qty) OR (cqty # temptotals.ctnqty)
			INSERT INTO tempqtyerr (ship_ref,ctnqty,totqty,cqty,uqty) VALUES (cSRUse,temptotals.ctnqty,temptotals.qty,m.cqty,m.uqty)
		ENDIF
	ENDSCAN

	SELECT tempqtyerr
	LOCATE
	IF !EOF()
		SET STEP ON
		totqtyerr()
		cErrMsg = "OD/CZN QTY DISCREP."
		DO ediupdate WITH cErrMsg,.T.
		THROW
	ENDIF

	SELECT vnanjingpp
	SET RELATION TO outdetid INTO outdet

	IF !USED("scacs")
		xsqlexec("select * from scac","scacs",,"wh")
		INDEX ON scac TAG scac
	ENDIF

	SELECT outship
	LOCATE
	WAIT CLEAR
	WAIT WINDOW "Now creating Header information" NOWAIT NOCLEAR

*!* HEADER LEVEL EDI DATA
	DO num_incr_isa

	cISA_Num = PADL(c_CntrlNum,9,"0")
	nISA_Num = INT(VAL(cISA_Num))

	STORE "ISA"+cfd+"00"+cfd+"          "+cfd+"00"+cfd+"          "+cfd+csendqual+cfd+csendidlong+cfd+;
		crecqual+cfd+crecidlong+cfd+ctruncdate+cfd+ctime+cfd+"U"+cfd+"00401"+cfd+;
		cISA_Num+cfd+"0"+cfd+"P"+cfd+cterminator+csegd TO cString
	DO cstringbreak

	STORE "GS"+cfd+"SW"+cfd+csendid+cfd+crecid+cfd+cdate+cfd+ctime+cfd+c_CntrlNum+;
		cfd+"X"+cfd+cX12+csegd TO cString
	DO cstringbreak

	SELECT outship
	LOCATE
	IF !SEEK(nWO_Num,"outship","wo_num")
		SET STEP ON
		WAIT CLEAR
		WAIT WINDOW "Invalid Work Order Number - Not Found in OUTSHIP!" TIMEOUT 2
		cErrMsg = "WO# NOT FOUND"
		DO ediupdate WITH cErrMsg,.T.
		THROW
	ENDIF
	lJCPenney = IIF("PENNEY"$outship.consignee,.T.,.F.)
	lOutlet = IIF("OUTLET"$outship.consignee,.T.,.F.)

	LOCATE
	IF !lTesting
		IF EMPTY(cBOL)  && May be empty if triggered from PT screen, or FedEx shipment
			WAIT CLEAR
			WAIT WINDOW "MISSING Bill of Lading Number!" TIMEOUT 2
			cErrMsg = "BOL# EMPTY"
			DO ediupdate WITH cErrMsg,.T.
			THROW
		ELSE
			IF !SEEK(PADR(TRIM(cBOL),20),"outship","bol_no")
				WAIT CLEAR
				WAIT WINDOW "Invalid Bill of Lading Number - Not Found!" TIMEOUT 2
				cErrMsg = "BOL# NOT FOUND"
				DO ediupdate WITH cErrMsg,.T.
				THROW
			ENDIF
		ENDIF
	ENDIF

	SELECT outship
	SET ORDER TO
	LOCATE && in OUTSHIP

*************************************************************************
*1	PTIDSOURCE/OUTSHIP RECORD MAIN LOOP
*	Loops through individual OUTSHIP lines for BOL#
*************************************************************************

	WAIT CLEAR
	WAIT WINDOW "Now creating WO#-based Pick/Pack information..." NOWAIT
*	ASSERT .F. MESSAGE "AT OUTSHIP SCAN"
	IF lTesting
		scanstr = "outship.wo_num = nWO_Num and INLIST(outship.accountid,4610,4694)"
	ELSE
		scanstr = "outship.wo_num = nWO_Num and outship.bol_no = cBOL"
	ENDIF
	SELECT outship
	SCAN FOR &scanstr
		WAIT WINDOW "" TIMEOUT 2
		cBOL2 = ALLTRIM(outship.bol_no)
		IF EMPTY(cBOL2)  && May be empty if triggered from PT screen, or FedEx shipment
			IF !lTesting
				WAIT CLEAR
				WAIT WINDOW "MISSING BOL# at PT# "+cShip_ref+"!" NOWAIT
				SELECT edi_trigger
				REPLACE fin_status WITH "MISSING BOL#",processed WITH .T.,errorflag WITH .T.  FOR edi_trigger.wo_num = nWO_Num ;
					AND edi_trigger.ship_ref = cShip_ref
				SELECT edi_trigger
			ENDIF
			LOOP
		ENDIF

		IF EMPTYnul(outship.del_date)
			cErrMsg = "EMPTY DEL_DATE"
			DO ediupdate WITH cErrMsg,.T.
			THROW
		ENDIF

		IF !lTesting
			IF SEEK(cBOL2,'edi_trigger','bol')
				IF edi_trigger.wo_num = outship.wo_num AND edi_trigger.processed
					LOOP
				ENDIF
			ENDIF
		ENDIF

		SCATTER MEMVAR MEMO
		lWalMart = IIF(("WALMART"$UPPER(m.consignee)) OR ("WAL-MART"$UPPER(m.consignee)),.T.,.F.)

		nOSQty = outship.qty
		IF nOSQty = 0
			LOOP
		ENDIF

		cBOL = TRIM(outship.bol_no)

		alength = ALINES(apt,outship.shipins,.T.,CHR(13))
		nOutshipID = outship.outshipid
		nTotCtnCount = m.qty
		cPO_Num = ALLTRIM(m.cnee_ref)
		cShip_ref = ALLTRIM(m.ship_ref)
		IF " OV"$cShip_ref
			LOOP
		ENDIF

		cFreight = "0.00"
		IF lTestInput
			cFreight = "25.50"
		ELSE
			SELECT shipment
			ASSERT .F. MESSAGE "At Shipment UPS number scan"
			LOCATE
			IF lTesting
				LOCATE FOR INLIST(shipment.accountid,4610,4694,9999) AND shipment.pickticket = cShip_ref AND shipment.wo_num = nWO_Num
			ELSE
				LOCATE FOR INLIST(shipment.accountid,nAcctNum,9999) AND shipment.pickticket = cShip_ref AND shipment.wo_num = nWO_Num
			ENDIF
			IF FOUND()
				SELECT package
				SUM pkgcharge TO nFreight FOR package.shipmentid = shipment.shipmentid
				cFreight = ALLTRIM(STR(nFreight,10,2))
				SELECT trknumber FROM package WHERE package.shipmentid = shipment.shipmentid INTO CURSOR temptrknumber
			ELSE
				cErrMsg = "PT MISS IN SHIPMENT"
				DO ediupdate WITH cErrMsg,.T.
				THROW
			ENDIF
			SELECT outship
		ENDIF

		SELECT outship

		IF "!!"$cShip_ref
			cBaseShip_ref = LEFT(cShip_ref,AT("!",cShip_ref,1)-1)
			cShip_ref2 = LEFT(cShip_ref,AT("!",cShip_ref,1)-4)
			nSuffix = INT(VAL(RIGHT(cBaseShip_ref,3)))
			DO CASE
			CASE ("!!D"$cShip_ref)
				cSuffix = PADL(ALLTRIM(STR(nSuffix+4)),3,'0')
			CASE ("!!C"$cShip_ref)
				cSuffix = PADL(ALLTRIM(STR(nSuffix+3)),3,'0')
			CASE ("!!B"$cShip_ref)
				cSuffix = PADL(ALLTRIM(STR(nSuffix+2)),3,'0')
			OTHERWISE
				cSuffix = PADL(ALLTRIM(STR(nSuffix+1)),3,'0')
			ENDCASE
			cShip_refedi = cShip_ref2+cSuffix
		ELSE
			cShip_refedi = cShip_ref
		ENDIF

		nOutshipID = m.outshipid
		cPTString = IIF(EMPTY(cPTString),m.consignee+" "+cShip_ref+" "+cBOL,cPTString+CHR(13)+m.consignee+" "+cShip_ref+" "+cBOL)
		m.CSZ = TRIM(m.CSZ)

		IF EMPTY(M.CSZ)
			WAIT CLEAR
			WAIT WINDOW "No City/State/ZIP info...exiting" TIMEOUT 2
			cErrMsg = "BAD ADDRESS INFO"
			DO ediupdate WITH cErrMsg,.T.
			THROW
		ENDIF
		cCity = segmentget(@apt,"CITY",alength)
		cState = segmentget(@apt,"STATE",alength)
		cZip = segmentget(@apt,"ZIPCODE",alength)

		IF EMPTY(cCity) OR EMPTY(cState) OR EMPTY(cZip)
			m.CSZ = ALLTRIM(STRTRAN(m.CSZ,"  "," "))
			nSpaces = OCCURS(" ",ALLTRIM(m.CSZ))
			nCommaPos = AT(",",m.CSZ)
			nLastSpace = AT(" ",m.CSZ,nSpaces)

			IF ISALPHA(SUBSTR(TRIM(m.CSZ),AT(" ",m.CSZ,nSpaces-1)+1,2))
				cCity = LEFT(TRIM(m.CSZ),AT(",",m.CSZ)-1)
				cState = SUBSTR(m.CSZ,nCommaPos+2,2)
				cZip = TRIM(SUBSTR(m.CSZ,nLastSpace+1))
			ELSE
				WAIT CLEAR
				WAIT WINDOW "NOT ALPHA: "+SUBSTR(TRIM(m.CSZ),AT(" ",m.CSZ,nSpaces-1)+1,2) TIMEOUT 3
				cCity = LEFT(TRIM(m.CSZ),AT(",",m.CSZ)-1)
				cState = SUBSTR(TRIM(m.CSZ),AT(" ",m.CSZ,nSpaces-2)+1,2)
				cZip = TRIM(SUBSTR(m.CSZ,nLastSpace+1))
			ENDIF
		ENDIF
		nCtnNumber = 1  && Begin sequence count

		DO num_incr_st
		WAIT CLEAR
		WAIT WINDOW "Now creating Line Item information" NOWAIT NOCLEAR

		INSERT INTO temp945 (accountid,isa_num,st_num,wo_num,bol_no,ship_ref,filename,filedate) ;
			VALUES (m.accountid,c_CntrlNum,c_GrpCntrlNum,m.wo_num,m.bol_no,m.ship_ref,cFileNameShort,dt2)

		STORE "ST"+cfd+"945"+cfd+PADL(c_GrpCntrlNum,9,"0")+csegd TO cString
		DO cstringbreak
		nSTCount = nSTCount + 1
		nSegCtr = 1

		IF LEFT(cBOL,2)="1Z"
			IF lJCPenney
				cShipmentID = ALLTRIM(cBOL)
			ELSE
				cShipmentID = ALLTRIM(SUBSTR(cBOL,8))
			ENDIF
		ELSE
			cShipmentID = (RIGHT(ALLTRIM(cWO_Num),6)+RIGHT(ALLTRIM(cShip_ref),4))
		ENDIF

		STORE "W06"+cfd+"N"+cfd+cShip_refedi+cfd+cdate+cfd+TRIM(cShipmentID)+cfd+TRIM(cWO_Num)+cfd+cPO_Num+csegd TO cString
		DO cstringbreak
		nSegCtr = nSegCtr + 1

		cStoreNum = segmentget(@apt,"STORENUM",alength)
		IF EMPTY(cStoreNum)
			cStoreNum = ALLTRIM(m.dcnum)
		ENDIF
		STORE "N1"+cfd+"ST"+cfd+ALLTRIM(m.consignee)+cfd+"92"+cfd+cStoreNum+csegd TO cString
		DO cstringbreak
		nSegCtr = nSegCtr + 1

		IF lJCPenney
			STORE "N1"+cfd+"Z7"+cfd+ALLTRIM(m.shipfor)+cfd+"92"+cfd+ALLTRIM(m.sforstore)+csegd TO cString
			DO cstringbreak
			nSegCtr = nSegCtr + 1
		ENDIF

		STORE "N9"+cfd+"BM"+cfd+TRIM(cBOL)+csegd TO cString
		DO cstringbreak
		nSegCtr = nSegCtr + 1

		cMR = segmentget(@apt,"MR",alength)
		IF lWalMart AND cMR#"0073"
			lDoManUC = .T.
		ENDIF
		STORE "N9"+cfd+"MR"+cfd+TRIM(cMR)+csegd TO cString
		DO cstringbreak
		nSegCtr = nSegCtr + 1

		cInvoice = IIF(EMPTY(ALLTRIM(m.keyrec)),ALLTRIM(m.ship_ref),ALLTRIM(m.keyrec))
		STORE "N9"+cfd+"CN"+cfd+TRIM(cInvoice)+csegd TO cString
		DO cstringbreak
		nSegCtr = nSegCtr + 1

		STORE "N9"+cfd+"DP"+cfd+TRIM(outship.dept)+csegd TO cString
		DO cstringbreak
		nSegCtr = nSegCtr + 1

		STORE "N9"+cfd+"IA"+cfd+TRIM(outship.vendor_num)+csegd TO cString
		DO cstringbreak
		nSegCtr = nSegCtr + 1

		IF lTesting
			ddel_date = DATE()
			dapptnum = "99999"
			dapptdate = DATE()
		ELSE
			ddel_date = outship.del_date
			IF EMPTY(ddel_date)
				cErrMsg = "MISSING DELDATE"
				DO ediupdate WITH cErrMsg,.T.
				THROW
			ENDIF

			dapptnum = IIF(EMPTY(outship.appt_num),"UNK",TRIM(outship.appt_num))
			dapptdate = outship.appt
			IF EMPTY(dapptdate)
				dapptdate = outship.del_date
			ENDIF
		ENDIF
		STORE "N9"+cfd+"AO"+cfd+dapptnum+csegd TO cString
		DO cstringbreak
		nSegCtr = nSegCtr + 1

		cUseTime = "153000"
*!*			STORE "G62"+cfd+"70"+cfd+DTOS(dapptdate)+cfd+"3"+cfd+cUseTime+csegd TO cString  && Ship date
*!*			DO cstringbreak
*!*			nSegCtr = nSegCtr + 1

		STORE "G62"+cfd+"11"+cfd+DTOS(ddel_date)+cfd+"9"+cfd+cUseTime+csegd TO cString  && Ship date
		DO cstringbreak
		nSegCtr = nSegCtr + 1

		SELECT scacs
		IF lTesting
			STORE "TEST" TO m.scac
		ELSE
			IF !EMPTY(TRIM(outship.scac))
				STORE outship.scac TO m.scac
			ELSE
				IF UPPER(LEFT(outship.SHIP_VIA,3)) = "UPS" AND EMPTY(outship.scac)
					REPLACE outship.scac WITH "UPSN" IN outship
				ELSE
					cErrMsg = "MISSING SCAC"
					DO ediupdate WITH cErrMsg,.T.
					THROW
				ENDIF
			ENDIF
		ENDIF

		SELECT outship

		cShipType="U"
		STORE "W27"+cfd+cShipType+cfd+TRIM(m.scac)+cfd+TRIM(m.SHIP_VIA)+csegd TO cString
		DO cstringbreak
		nSegCtr = nSegCtr + 1

		IF !EMPTY(cFreight) AND !lWalMart
			STORE "G72"+cfd+"516"+cfd+"15"+REPLICATE(cfd,6)+ALLT(cFreight)+csegd TO cString
			DO cstringbreak
			nSegCtr = nSegCtr + 1
			cFreight = ""
		ENDIF

*************************************************************************
*2	DEtAIL LOOP - MATCHING SHIP_REF FIELD (FROM OUTSHIP)
*************************************************************************
		WAIT CLEAR
		WAIT WINDOW "Now creating Detail information "+cShip_ref NOWAIT NOCLEAR
		SELECT vnanjingpp
		SUM totqty TO nPPQty FOR ship_ref = TRIM(cShip_ref) AND outshipid = nOutshipID
		INSERT INTO qtycompare (ship_ref,osqty,ppqty) VALUES (cShip_ref,nOSQty,nPPQty)
		SELECT qtycompare
*	brow
		GO BOTT
		nPctdiff = INT(ABS((osqty-ppqty)/osqty)*100)
		REPLACE pctdiff WITH nPctdiff
		STORE 0 TO nOSQty,nPPQty,nPctdiff
		SELECT vnanjingpp
		LOCATE
		LOCATE FOR vnanjingpp.ship_ref = TRIM(cShip_ref) AND vnanjingpp.outshipid = nOutshipID
		IF !FOUND()
			WAIT WINDOW "PT "+cShip_ref+" NOT FOUND in VNANJINGPP...ABORTING" TIMEOUT 2
			cErrMsg = "MISS PT: "+cShip_ref
			DO ediupdate WITH cErrMsg,.T.
			THROW
		ENDIF

*!* Added this code to trap unmatched OUTDETIDs in OUTDET, Joe 11.22.2005
		SCAN FOR ship_ref = TRIM(cShip_ref) AND outshipid = nOutshipID
			IF EMPTY(outdet.outdetid)
				WAIT WINDOW "OUTDETID "+TRANSFORM(vnanjingpp.outdetid)+" NOT FOUND IN OUTDET!...CLOSING" TIMEOUT 3
				cErrMsg = "MISS OD-ID: "+TRANSFORM(vnanjingpp.outdetid)
				DO ediupdate WITH cErrMsg,.T.
				THROW
			ENDIF
		ENDSCAN

		scanstr2 = "vnanjingpp.ship_ref = cShip_ref and vnanjingpp.outshipid = nOutshipid"
		SELECT vnanjingpp
		LOCATE FOR &scanstr2
		cCartonNum= "XXX"
*	SCAN FOR &scanstr
		DO WHILE &scanstr2
*!*			lSkipBack = .T.

			IF vnanjingpp.totqty = 0
				SKIP 1 IN vnanjingpp
				LOOP
			ENDIF

			IF vnanjingpp.ucc = 'CUTS'
				SKIP 1 IN vnanjingpp
				LOOP
			ENDIF

			IF TRIM(vnanjingpp.ucc) <> cCartonNum
				STORE TRIM(vnanjingpp.ucc) TO cCartonNum
			ENDIF

			STORE "LX"+cfd+ALLTRIM(STR(nCtnNumber))+csegd TO cString   && Carton Seq. # (up to Carton total)
			lDoManSegment = .T.
			DO cstringbreak
			nSegCtr = nSegCtr + 1

			DO WHILE vnanjingpp.ucc = cCartonNum
				cSCCNumber = TRIM(vnanjingpp.scc214)
				IF (ISNULL(cSCCNumber) OR EMPTY(cSCCNumber) )
					STORE "00"+LEFT(vnanjingpp.upc,6)+ALLTRIM(STR(vnanjingpp.carton_id))+TRANSFORM(INT(RAND(0)*10)) TO cSCCNumber
				ENDIF
				IF "MEIJER"$outship.consignee
					cSCCNumber = TRIM(vnanjingpp.ucc)
				ENDIF
				cSCCNumber = TRIM(STRTRAN(TRIM(cSCCNumber)," ",""))

				IF lDoManSegment
					cUCC = ALLTRIM(vnanjingpp.ucc)
					SELECT temptrknumber
					cTrk = ALLTRIM(temptrknumber.trknumber)
					IF !EOF()
						SKIP 1 IN temptrknumber
					ENDIF
					SELECT vnanjingpp
					IF !EMPTY(cUCC) AND cUCC # "XXX"
						lDoManSegment = .F.
						IF !EMPTY(cTrk)
							STORE "MAN"+cfd+"GM"+cfd+TRIM(cUCC)+cfd+"CP"+cTrk+csegd TO cString  && UCC+Tracking Number
						ELSE
							STORE "MAN"+cfd+"GM"+cfd+TRIM(cUCC)+csegd TO cString  && UCC Number (Wal-mart DSDC only)
						ENDIF
						DO cstringbreak
						nSegCtr = nSegCtr + 1
						nDetQty = INT(VAL(outdet.PACK))
					ELSE
						WAIT CLEAR
						WAIT WINDOW "Empty UCC Number in vNanjingPP "+cShip_ref TIMEOUT 2
						IF lTesting
							CLOSE DATABASES ALL
							=FCLOSE(nFilenum)
							ERASE &cFilenameHold
							THROW
						ELSE
							cErrMsg = "EMPTY UCC# in "+cShip_ref
							DO ediupdate WITH cErrMsg,.T.
							=FCLOSE(nFilenum)
							ERASE &cFilenameHold
							THROW
						ENDIF
					ENDIF
				ENDIF

				SELECT outdet
				cColor = ""
				IF nAcctNum = 4694
					IF "ORIG_COLOR*"$outdet.printstuff
						valreturn("orig_color")
						STORE cOrig_color TO cColor
					ENDIF
				ELSE
					cColor = TRIM(outdet.COLOR)
				ENDIF
				IF EMPTY(ALLTRIM(cColor))
					IF "ORIG_COLOR*"$outdet.printstuff
						valreturn("orig_color")
						STORE cOrig_color TO cColor
					ELSE
						cErrMsg = "MISS COLOR in "+cShip_ref
						DO ediupdate WITH cErrMsg,.T.
						=FCLOSE(nFilenum)
						ERASE &cFilenameHold
						THROW
					ENDIF
				ENDIF

				cSize = ""
				IF "ORIG_ID*"$outdet.printstuff
					valreturn("orig_id")
					STORE cOrig_id TO cSize
				ELSE
					IF EMPTY(TRIM(cSize))
						STORE TRIM(outdet.ID) TO cSize
					ENDIF
				ENDIF

				cStyle = ""
				IF "ORIG_STYLE*"$outdet.printstuff
					valreturn("orig_style")
					STORE cOrig_style TO cStyle
				ELSE
					IF EMPTY(cStyle)
						cStyle = TRIM(outdet.STYLE)
					ENDIF
				ENDIF
				valreturn("PRINTSTYLE")

				cUPC = TRIM(outdet.upc)
				IF "MEIJER"$outship.consignee
					cStyle = outdet.STYLE
				ENDIF
				cItemNum = TRIM(outdet.custsku)

				IF lOutlet OR lJCPenney
					IF lOutlet
						cStyle = TRIM(outdet.custsku)
						cItemNum = ""
					ENDIF

*!*						IF outdet.size_scale = "ADD*ONLY" && Added 01.25.07 to correct style data
*!*							IF lOutlet
*!*								cStyle = cStyle+"*ONLY"
*!*							ELSE
*!*								cItemNum = cItemNum + "*ONLY"
*!*							ENDIF
*!*						ENDIF
				ENDIF

				nDetQty = IIF(nAcctNum = 4694,1,vnanjingpp.totqty)
*				nOrigQty = IIF(nAcctNum = 4694,1,outdet.origqty)
				nOrigQty = IIF(nAcctNum = 4694,1,vnanjingpp.qty)

				IF lTesting AND EMPTY(outdet.ctnwt)
					cCtnWt = "2"
					nTotCtnWt = nTotCtnWt + 2
				ELSE
					STORE ALLTRIM(STR(outdet.ctnwt)) TO cCtnWt
					nTotCtnWt = nTotCtnWt + outdet.ctnwt
				ENDIF

				IF EMPTY(cCtnWt) OR outdet.ctnwt=0
					nCtnWt = outship.weight/outship.ctnqty
					cCtnWt = ALLTRIM(STR(nCtnWt))
					nTotCtnWt = nTotCtnWt + nCtnWt
					IF EMPTY(cCtnWt)
						cErrMsg = "MISS CTNWT "+TRANSFORM(vnanjingpp.outdetid)
						DO ediupdate WITH cErrMsg,.T.
						=FCLOSE(nFilenum)
						ERASE &cFilenameHold
						THROW
					ENDIF
				ENDIF

				valreturn("UNITSTYPE")
				valreturn("W0104")
				valreturn("W0106")
				valreturn("W0115")

				IF EMPTY(cUnitstype)
					cUnitstype = "EA"
				ENDIF

				IF EMPTY(cW0104)
					cW0104 = "IN"
				ENDIF

				IF EMPTY(cW0106)
					cW0106 = "UP"
				ENDIF

				IF EMPTY(cW0115)
					cW0115 = "VN"
				ENDIF

				IF nOrigQty=nDetQty
					nShipType = "CL"
				ELSE
					nShipType = "PR"
				ENDIF

				IF nDetQty>0
					nUnitSum = nUnitSum + nDetQty
					STORE "W12"+cfd+nShipType+cfd+ALLTRIM(STR(nOrigQty))+cfd+ALLTRIM(STR(nDetQty))+cfd+;
						ALLTRIM(STR(nOrigQty-nDetQty))+cfd+"EA"+cfd+cUPC+cfd+cW0104+cfd+TRIM(cItemNum)+;
						cfd+cfd+cCtnWt+cfd+cWeightUnit+REPLICATE(cfd,6)+cW0106+cfd+cUPC+;
						REPLICATE(cfd,3)+cW0115+cfd+cStyle+csegd TO cString  && Eaches/Style
					DO cstringbreak
					nSegCtr = nSegCtr + 1

					IF !EMPTY(cColor)
						STORE "N9"+cfd+"VCL"+cfd+cColor+csegd TO cString
						DO cstringbreak
						nSegCtr = nSegCtr + 1
					ENDIF

					IF nAcctNum = 4610
						IF !EMPTY(cSize)
							STORE "N9"+cfd+"VSZ"+cfd+cSize+csegd TO cString
							DO cstringbreak
							nSegCtr = nSegCtr + 1
						ENDIF
					ENDIF
				ENDIF

				SELECT vnanjingpp
				IF !EOF()
					SKIP 1 IN vnanjingpp
				ELSE
					EXIT
				ENDIF
			ENDDO
*!*			lSkipBack = .T.
			nCtnNumber = nCtnNumber + 1
		ENDDO

*************************************************************************
*2^	END DETAIL MATCHING SHIP_REF LOOP
*************************************************************************
		WAIT CLEAR
		WAIT WINDOW "Now creating Section Closure information" NOWAIT NOCLEAR
		STORE "W03"+cfd+ALLTRIM(STR(nUnitSum))+cfd+ALLTRIM(STR(nTotCtnWt))+cfd+;
			cWeightUnit+csegd TO cString   && Units sum, Weight sum, carton count

		nTotCtnWt = 0
		nTotCtnCount = 0
		nUnitSum = 0
		FPUTS(nFilenum,cString)

		STORE  "SE"+cfd+ALLTRIM(STR(nSegCtr+2))+cfd+PADL(c_GrpCntrlNum,9,"0")+csegd TO cString
		FPUTS(nFilenum,cString)

		WAIT CLEAR
	ENDSCAN

*************************************************************************
*1^	END OUTSHIP MAIN LOOP
*************************************************************************
	DO close945
	=FCLOSE(nFilenum)

	ASSERT .F. MESSAGE "Post-closure point...debug"

	lMismatch = .F.
	SELECT qtycompare
	LOCATE
	IF !EOF()
		SUM qtycompare.osqty TO sum_os
		SUM qtycompare.ppqty TO sum_pp
		IF sum_pp <> sum_os
			lMismatch = .T.
			lEmail = .F.
			lNanFilesOut = .F.
			countmail()
			cErrMsg = "COUNT ERR "+cBOL
			DO ediupdate WITH cErrMsg,.T.
			=FCLOSE(nFilenum)
			ERASE &cFilenameHold
			THROW
		ENDIF
	ENDIF

	IF !lTesting
		SELECT edi_trigger
		IF lMismatch
			DO ediupdate WITH "945 COUNT OFF",.T.
		ELSE
			DO ediupdate WITH "945 CREATED",.F.
		ENDIF
		SELECT edi_trigger
		LOCATE

		IF !USED("ftpedilog")
			SELECT 0
			USE F:\edirouting\ftpedilog ALIAS ftpedilog
			INSERT INTO ftpedilog (TRANSFER,ftpdate,filename,acct_name,TYPE) VALUES ("945-"+cCustname+"-"+cCustLoc,dt2,cFilenameHold,UPPER(cCustname),"945")
			USE IN ftpedilog
		ENDIF
	ENDIF

	WAIT CLEAR
	WAIT WINDOW cCustFolder+" 945 Process complete..." TIMEOUT 1

*!* Create eMail confirmation message
	IF lTestMail
		tsendto = tsendtotest
		tcc = tcctest
	ENDIF

	cOutFolder = "FMI"+cCustLoc

	tsubject = cMailName+" 945 EDI File from TGF as of "+dtmail+" ("+cCustLoc+")"
	tattach = " "
	tmessage = "945 EDI Info from TGF for "+cDivision+", UPS WO# "+TRIM(cWO_Num)+CHR(13)
	tmessage = tmessage + "(File: "+cFileNameShort+")"+CHR(13)
	tmessage = tmessage + "containing these picktickets:"+CHR(13)
	tmessage = tmessage + cPTString + CHR(13)
	IF lMismatch
		tmessage = tmessage + "has a quantity mismatch and is being held pending verification." + CHR(13)
		tmessage = tmessage + "We will notify you of final status shortly."
	ELSE
		tmessage = tmessage + "has been created and will be transmitted ASAP."+CHR(13)+CHR(13)
		tmessage = tmessage + "If you have any questions, please eMail or call 732-750-9000 x217."
	ENDIF
	tmessage = tmessage+CHR(13)+"Run from prog: "+cProgName

	IF lEmail
		DO FORM m:\dev\frm\dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
	ENDIF

	RELEASE ALL LIKE c_CntrlNum,c_GrpCntrlNum
	WAIT CLEAR
	IF lPTFlag
		WAIT WINDOW cMailName+" 945 EDI File output complete"+CHR(13)+"for PT# "+TRIM(cShip_ref) TIMEOUT 1
	ELSE
		WAIT WINDOW cMailName+" 945 EDI File output complete"+CHR(13)+"for BOL# "+TRIM(cBOL) TIMEOUT 1
	ENDIF

*!* Transfers files to correct output folders
	IF lNanFilesOut
		COPY FILE &cFilenameHold TO &cFilenameOut
		DELETE FILE &cFilenameHold
	ENDIF
	
	IF !lTesting
		SELECT temp945
		COPY TO "f:\3pl\data\temp945a.dbf"
		USE IN temp945
		SELECT 0
		USE "f:\3pl\data\temp945a.dbf" ALIAS temp945a
		SELECT 0
		USE "f:\3pl\data\pts_sent945.dbf" ALIAS pts_sent945
		APPEND FROM "f:\3pl\data\temp945a.dbf"
		USE IN pts_sent945
		USE IN temp945a
		DELETE FILE "f:\3pl\data\temp945a.dbf"
	ENDIF

	IF !lTesting
*		asn_out_data()
	ENDIF

CATCH TO oErr
	IF lDoCatch
		SET STEP ON
		lEmail = .F.
		IF EMPTY(cErrMsg)
			DO ediupdate WITH "ERRHAND ERROR",.T.
			tsubject = cCustname+" Error ("+TRANSFORM(oErr.ERRORNO)+") at "+TTOC(DATETIME())
			tattach  = ""
			tsendto  = IIF(lTesting,tsendtotest,tsendtoerr)
			tcc = IIF(lTesting,tcctest,tccerr)

			tmessage = cCustname+" Error processing "+CHR(13)

			tmessage =tmessage+ "Try/CATCH Exeception Message:"+CHR(13)+;
				[  Error: ] + STR(oErr.ERRORNO) +CHR(13)+;
				[  LineNo: ] + STR(oErr.LINENO) +CHR(13)+;
				[  Message: ] + oErr.MESSAGE +CHR(13)+;
				[  Procedure: ] + oErr.PROCEDURE +CHR(13)+;
				[  Details: ] + oErr.DETAILS +CHR(13)+;
				[  StackLevel: ] + STR(oErr.STACKLEVEL) +CHR(13)+;
				[  LineContents: ] + oErr.LINECONTENTS+CHR(13)+;
				[  UserValue: ] + oErr.USERVALUE

			tsubject = "945 EDI Poller Error at "+TTOC(DATETIME())
			tattach  = ""
			tfrom = "Toll EDI Operations <toll-edi-ops@tollgroup.com>"
			DO FORM m:\dev\frm\dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
		ENDIF
	ENDIF
FINALLY
*	RELEASE ALL LIKE t*
ENDTRY


*** END OF CODE BODY

****************************
PROCEDURE close945
****************************
** Footer Creation

STORE  "GE"+cfd+ALLTRIM(STR(nSTCount))+cfd+c_CntrlNum+csegd TO cString
FPUTS(nFilenum,cString)

STORE  "IEA"+cfd+"1"+cfd+PADL(c_CntrlNum,9,"0")+csegd TO cString
FPUTS(nFilenum,cString)

RETURN

****************************
PROCEDURE num_incr_isa
****************************

IF !USED("serfile")
	USE ("f:\3pl\data\serial\"+cCustname+"945_serial") IN 0 ALIAS serfile
ENDIF
SELECT serfile
IF lTesting AND lISAFlag
	nOrigSeq = serfile.seqnum
	lISAFlag = .F.
ENDIF
nISA_Num = serfile.seqnum
c_CntrlNum = ALLTRIM(STR(serfile.seqnum))
REPLACE serfile.seqnum WITH serfile.seqnum + 1 IN serfile
SELECT outship
RETURN

****************************
PROCEDURE num_incr_st
****************************

IF !USED("serfile")
	USE ("f:\3pl\data\serial\"+cCustname+"945_serial") IN 0 ALIAS serfile
ENDIF
SELECT serfile
IF lTesting AND lSTFlag
	nOrigGrpSeq = serfile.grpseqnum
	lSTFlag = .F.
ENDIF
c_GrpCntrlNum = ALLTRIM(STR(serfile.grpseqnum))
REPLACE serfile.grpseqnum WITH serfile.grpseqnum + 1 IN serfile
SELECT outship
RETURN


****************************
PROCEDURE segmentget
****************************

PARAMETER thisarray,lcKey,nLength

FOR i = 1 TO nLength
	IF i > nLength
		EXIT
	ENDIF
	lnEnd= AT("*",thisarray[i])
	IF lnEnd > 0
		lcThisKey =TRIM(SUBSTR(thisarray[i],1,lnEnd-1))
		IF OCCURS(lcKey,lcThisKey)>0
			RETURN SUBSTR(thisarray[i],lnEnd+1)
			i = 1
		ENDIF
	ENDIF
ENDFOR

RETURN ""

****************************
PROCEDURE ediupdate
****************************
PARAMETER cStatus,lIsError
lDoCatch = .F.

&&
IF !lTesting
	SELECT edi_trigger
	IF !lIsError
		IF lPTFlag
			REPLACE processed WITH .T.,proc945 WITH .T.,file945 WITH cFilenameHold,;
				fin_status WITH cStatus,errorflag WITH .F.,when_proc WITH DATETIME() ;
				FOR edi_trigger.ship_ref = cShip_ref AND accountid = nAcctNum AND !processed
		ELSE
			REPLACE processed WITH .T.,proc945 WITH .T.,file945 WITH cFilenameHold,;
				fin_status WITH cStatus,errorflag WITH .F.,when_proc WITH DATETIME() ;
				FOR accountid = nAcctNum AND edi_trigger.BOL = cBOL AND !processed
		ENDIF
	ELSE
		IF lPTFlag
			REPLACE processed WITH .T.,proc945 WITH .F.,file945 WITH "",;
				fin_status WITH cStatus,errorflag WITH .T. ;
				FOR edi_trigger.ship_ref = cShip_ref AND accountid = nAcctNum AND !processed
		ELSE
			REPLACE processed WITH .T.,proc945 WITH .F.,file945 WITH "",;
				fin_status WITH cStatus,errorflag WITH .T. ;
				FOR accountid = nAcctNum AND edi_trigger.BOL = cBOL AND !processed
		ENDIF
		IF lCloseOutput
			=FCLOSE(nFilenum)
			IF !lMismatch
				ERASE &cFilenameHold
			ENDIF
		ENDIF
	ENDIF
ENDIF

IF lIsError AND lEmail
	tsubject = "945 Error in NANJING/EA BOL "+TRIM(cBOL)+" (At PT "+cShip_ref+")"
	tattach = " "
	tsendto = tsendtoerr
	tcc = tccerr

	tmessage = "Generated from program: nanjing945pp_create_ups, within EDI Outbound Poller"
	tmessage = tmessage + CHR(13) + "945 Processing for WO# "+cWO_Num+" produced this error: (Office: "+cOffice+"): "+cStatus+CHR(13)+"Check EDI_TRIGGER and re-run"

	IF cStatus = "MISS PT"
		tmessage = tmessage + CHR(13) + "This error means that this PT exists in OUTSHIP, but not in the Pickpack table. Please add this WO to SQL."
	ENDIF

	IF TIME(DATE())>"20:00:00" OR INLIST(DOW(DATE()),1,7)
*		tcc = tcc+",jsb1956@yahoo.com"
	ENDIF
	DO FORM m:\dev\frm\dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
ENDIF

IF USED('outship')
	USE IN outship
ENDIF
IF USED('outdet')
	USE IN outdet
ENDIF

IF USED('serfile')
	SELECT serfile
	IF lTestInput
		REPLACE serfile.seqnum WITH nOrigSeq
		REPLACE serfile.grpseqnum WITH nOrigGrpSeq
	ENDIF
	USE IN serfile
ENDIF

IF USED('scacs')
	USE IN scacs
ENDIF

IF USED('mm')
	USE IN mm
ENDIF

IF !lTesting
	SELECT edi_trigger
	LOCATE
ENDIF
IF lIsError
ENDIF
RETURN

****************************
PROCEDURE cstringbreak
****************************
cLen = LEN(ALLTRIM(cString))

FPUTS(nFilenum,cString)
RETURN

****************************
PROCEDURE valreturn
****************************
PARAMETERS cIdentifier
cReturned = "c"+cIdentifier
RELEASE ALL LIKE &cReturned
PUBLIC &cReturned
nGetline = ATCLINE(cIdentifier,outdet.printstuff)
dataline = MLINE(outdet.printstuff,nGetline)
STORE SUBSTR(dataline,AT("*",dataline)+1) TO &cReturned
RETURN &cReturned
ENDPROC

****************************
PROCEDURE countmail
****************************
SELECT qtycompare
LOCATE
tmessage = "SCAN PACK REPORT NOT RUN!"
cString = PADR("WO #",10)+PADR("PICKTICKET",15)+PADR("O/S QTY",10)+PADR("P/P QTY",10)+PADR("QTY DIFF",10)+PADR("PCT DIFF",10)
tmessage = tmessage + CHR(13) + cString
IF !EOF()
	ASSERT .F. MESSAGE "In QTY Compare error"
	SCAN
		cShip_ref = PADR(qtycompare.ship_ref,15)
		cOSQty =  PADR(ALLTRIM(STR(qtycompare.osqty)) ,10)
		cPPqty = PADR(ALLTRIM(STR(qtycompare.ppqty)) ,10)
		cQtyDiff = PADR(ALLTRIM(STR(qtycompare.ppqty-qtycompare.osqty)) ,10)
		cPctDiff = PADR(ALLTRIM(STR(qtycompare.pctdiff)) ,10)
		IF cOSQty<>cPPqty
			tmessage = tmessage + CHR(13) + cPadWO_Num + cShip_ref + cOSQty + cPPqty + cQtyDiff + cPctDiff
		ENDIF
	ENDSCAN

	tsubject = "945 Count for NANJING/EA DSDC BOL "+TRIM(cBOL)
	tattach = " "
	tsendto = tsendtoerr
	tcc = tccerr
	DO FORM m:\dev\frm\dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
ENDIF
ENDPROC

****************************
PROCEDURE totqtyerr
****************************
SET STEP ON
tsubject = "Cartons/Units Discrepancy, Outbound vs. Cartons Data"
tmessage = "The following PT(s) had unmatched counts between the outbound ('O') and cartonization ('C') data:"+CHR(13)+CHR(13)
tmessage = tmessage+PADR("PT#",20)+PADR("O.Ctns",10)+PADR("C.Ctns",10)+PADR("O.Units",10)+PADR("C.Units",10)+CHR(13)
SELECT tempqtyerr
SCAN
	cSRUse = ALLTRIM(tempqtyerr.ship_ref)
	cnctnqty = STR(tempqtyerr.ctnqty)
	cncqty = STR(tempqtyerr.cqty)
	cntotqty = STR(tempqtyerr.totqty)
	cnuqty = STR(tempqtyerr.uqty)
	DO CASE
	CASE (ctnqty # cqty) AND (totqty # uqty)
		cErrorLine = "Both Carton and Unit totals don't match"
	CASE (ctnqty # cqty)
		cErrorLine = "Only Carton totals don't match"
	CASE (totqty # uqty)
		cErrorLine = "Only Unit totals don't match"
	ENDCASE
	tmessage = tmessage+PADR(cSRUse,20)+PADR(cnctnqty,10)+PADR(cncqty,10)+PADR(cntotqty,10)+PADR(cnuqty,10)+" "+cErrorLine+CHR(13)
ENDSCAN

tmessage = tmessage+CHR(13)+"Please contact Toll I.T./EDI IMMEDIATELY to confirm which numbers are correct."
tsendto = IIF(lTesting,tsendtotest,tsendtoqty)
tcc = IIF(lTesting,tcctest,tccqty)
tattach = ""
DO FORM m:\dev\frm\dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
USE IN tempqtyerr
ENDPROC
