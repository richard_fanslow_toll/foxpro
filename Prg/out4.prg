close databases all
set deleted on

goffice='5'

use ("g:\moved2sql\wh"+goffice+"\outship") in 0 alias outshipold
use ("g:\moved2sql\wh"+goffice+"\outdet") in 0 alias outdetold

* sp3

*useca("labels","pickpack_sql5",,,"select * from labels where accountid=6612")


**use the 2nd cursor adapter for SP3 bc it has a different table structure
*useca("cartons","pickpack_sql5",,,"select * from cartons where accountid=6612")
useca("cartons","pickpack_sp3",,,"between(insdttm,{7/28/17},{8/1/17}) and (office='"+goffice+"' or inlist(accountid,6182,5154,6468))","cartons_sp3")

? reccount()

xdy=0

select cartons
scan
	xdy=xdy+1
	if xdy%1000=0
		wait window xdy nowait
		tu("cartons")
	endif
	
	if !seek(outshipid,"outshipold","outshipid")
		loop
	endif
	if !seek(outdetid,"outdetold","outdetid")
		loop
	endif
	
	if xsqlexec("select outwologid, outshipid, accountid, ship_ref from outship where accountid="+transform(outshipold.accountid)+" and ship_ref='"+outshipold.ship_ref+"'",,,"wh") = 0
		loop
	endif

	if xsqlexec("select outdetid from outdet where accountid="+transform(outshipold.accountid)+" and outshipid="+transform(outship.outshipid)+" and style='"+outdetold.style+"' and color='"+outdetold.color+"' and id='"+outdetold.id+"' and pack='"+outdetold.pack+"' and units="+iif(outdetold.units,"1","0"),,,"wh") = 0
		loop
	endif
	
	replace outshipid with outship.outshipid, outdetid with outdet.outdetid in cartons
endscan
	
tu("cartons")

gunshot()

email("Dyoung@fmiint.com","cartons",,,,,.t.,,,,,.t.,,.t.)
