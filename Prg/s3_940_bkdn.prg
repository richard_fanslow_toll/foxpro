*!* m:\dev\prg\s3_940_bkdn.prg

CLEAR
lCheckStyle = .T.
lPrepack = .F.  && Inventory to be kept as Prepack

cUCCNumber = ""
cUsePack = ""
lcUCCSeed = 1

SELECT x856
COUNT FOR TRIM(segment) = "ST" TO nSegCnt
cSegCnt = ALLTRIM(STR(nSegCnt))
SELECT x856
LOCATE

ptctr = 0

WAIT WINDOW "Now in "+cMailName+" 940 Breakdown" NOWAIT

WAIT "There are "+cSegCnt+" P/Ts in this file" WINDOW NOWAIT
SELECT x856
LOCATE

*!* Constants
m.acct_name = "S3 HOLDING, LLC"
STORE nAcctNum TO m.accountid
m.careof = " "
m.sf_addr1 = "1 W. 34TH STREET. 11TH FLOOR"
m.sf_addr2 = ""
m.sf_csz = "NEW YORK, NY 10001"

m.printed = .F.
m.print_by = ""
m.printdate = DATE()
*!* End constants

SELECT xpt
DELETE ALL
SELECT xptdet
DELETE ALL

STORE 0 TO pt_total_eaches
STORE 0 TO pt_total_cartons
STORE 0 TO nCtnCount

SELECT x856
SET FILTER TO
LOCATE

STORE "" TO cisa_num

WAIT WINDOW "NOW IN INITAL HEADER BREAKDOWN" TIMEOUT 1
WAIT WINDOW "NOW IN INITAL HEADER BREAKDOWN" NOWAIT
*ASSERT .F. MESSAGE "At header bkdn...debug"
DO WHILE !EOF("x856")
	cSegment = TRIM(x856.segment)
	cCode = TRIM(x856.f1)

	IF TRIM(x856.segment) = "ISA"
		cisa_num = ALLTRIM(x856.f13)
		CurrentISANum = ALLTRIM(x856.f13)
		WAIT WINDOW "This is a "+cMailName+" PT upload" NOWAIT
		ptctr = 0
		SELECT x856
		SKIP
		LOOP
	ENDIF

	IF TRIM(x856.segment) = "GS"
		cgs_num = ALLTRIM(x856.f6)
		lcCurrentGroupNum = ALLTRIM(x856.f6)
		SELECT x856
		SKIP
		LOOP
	ENDIF

	IF TRIM(x856.segment) = "ST" AND TRIM(x856.f1) = "940"
		IF !lTesting
			SELECT x856
			SKIP
			LOOP
		ENDIF
	ENDIF

	IF TRIM(x856.segment) = "W05"  && new shipment
		nCtnCount = 0
		m.style = ""
		SELECT xpt
		nDetCnt = 0
		APPEND BLANK

		REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"FILENAME*"+cFilename IN xpt
		REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"CONAME*S3-"+cGroup
		REPLACE xpt.qty WITH 0 IN xpt
		REPLACE xpt.origqty WITH 0 IN xpt
		REPLACE xpt.office WITH cOffice IN xpt
		REPLACE xpt.MOD WITH cMod IN xpt
		STORE 0 TO pt_total_eaches
		STORE 0 TO pt_total_cartons
		ptctr = ptctr +1
		WAIT WINDOW AT 10,10 "Now processing PT# "+ ALLTRIM(x856.f2)+" Number "+CHR(13)+TRANSFORM(ptctr)+" of "+cSegCnt NOWAIT

		REPLACE xpt.ptid       WITH ptctr IN xpt
		REPLACE xpt.accountid  WITH nAcctNum IN xpt
		REPLACE xpt.ptdate     WITH dFileDate IN xpt
		m.ship_ref = ALLTRIM(x856.f2)
		cShip_ref = ALLTRIM(x856.f2)
		REPLACE ship_ref   WITH m.ship_ref IN xpt  && Pick Ticket
		m.pt = ALLTRIM(x856.f2)
		REPLACE cnee_ref   WITH ALLTRIM(x856.f3) IN xpt  && PO
		SELECT x856
		SKIP
		LOOP
	ENDIF

	IF TRIM(x856.segment) = "N1" AND TRIM(x856.f1) = "ST" && Ship-to data

		cConsignee = UPPER(ALLTRIM(x856.f2))
		SELECT xpt
		m.st_name = cConsignee
		REPLACE xpt.consignee WITH  cConsignee IN xpt

		cStoreNum = ALLTRIM(x856.f4)
		IF EMPTY(xpt.shipins)
			REPLACE xpt.shipins WITH "STORENUM*"+cStoreNum IN xpt
		ELSE
			REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"STORENUM*"+cStoreNum IN xpt
		ENDIF
		IF EMPTY(xpt.dcnum)
			REPLACE xpt.dcnum WITH cStoreNum IN xpt
		ENDIF
		IF LEN(cStoreNum)>5
			REPLACE xpt.storenum  WITH VAL(RIGHT(cStoreNum,5)) IN xpt
		ELSE
			REPLACE xpt.storenum  WITH VAL(cStoreNum) IN xpt
		ENDIF

		SKIP 1 IN x856
		DO CASE
			CASE TRIM(x856.segment) = "N2"  && name ext. info
				m.name = UPPER(ALLTRIM(x856.f1))
				REPLACE xpt.NAME WITH m.name IN xpt
				SKIP 1 IN x856
				IF TRIM(x856.segment) = "N3"  && address info
					m.st_addr1 = UPPER(ALLTRIM(x856.f1))
					REPLACE xpt.address  WITH m.st_addr1 IN xpt
					m.st_addr2 = UPPER(ALLTRIM(x856.f2))
					REPLACE xpt.address2 WITH m.st_addr2 IN xpt
				ENDIF
				SKIP 1 IN x856
				IF TRIM(x856.segment) = "N4"  && address info
					m.zipbar = UPPER(ALLTRIM(x856.f3))
					m.st_csz = UPPER(ALLTRIM(x856.f1))+", "+UPPER(ALLTRIM(x856.f2))+" "+TRIM(x856.f3)
					REPLACE xpt.csz WITH m.st_csz IN xpt
					REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"COUNTRY*"+ALLTRIM(x856.f4) IN xpt
				ENDIF
			CASE TRIM(x856.segment) = "N3"  && address info
				m.st_addr1 = UPPER(ALLTRIM(x856.f1))
				REPLACE xpt.address  WITH m.st_addr1 IN xpt
				m.st_addr2 = UPPER(ALLTRIM(x856.f2))
				REPLACE xpt.address2 WITH m.st_addr2 IN xpt
				SKIP 1 IN x856
				IF TRIM(x856.segment) = "N4"  && address info
					m.zipbar = UPPER(ALLTRIM(x856.f3))
					m.st_csz = UPPER(TRIM(x856.f1))+", "+UPPER(TRIM(x856.f2))+" "+TRIM(x856.f3)
					REPLACE xpt.csz WITH m.st_csz IN xpt
					REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"COUNTRY*"+ALLTRIM(x856.f4) IN xpt
				ENDIF
			OTHERWISE
				WAIT WINDOW "UNKNOWN ADDRESS SEGMENT" TIMEOUT 2
				THROW
		ENDCASE
		SELECT x856
		SKIP
		LOOP
	ENDIF


	IF TRIM(x856.segment) = "PER"  && Personal contact
		IF INLIST(x856.f1,"AJ","RE")
			REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"CONTACT*"+UPPER(ALLTRIM(x856.f2)) IN xpt
		ENDIF
		IF x856.f3 = "TE"
			REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"PHONE*"+UPPER(ALLTRIM(x856.f4)) IN xpt
		ENDIF
		IF INLIST(x856.f5,"EA","EM")
			IF x856.f5 = "EA"
				REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"EDI_CONTACT*"+UPPER(ALLTRIM(x856.f6)) IN xpt
			ELSE
				REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"EMAIL*"+UPPER(ALLTRIM(x856.f6)) IN xpt
			ENDIF
		ENDIF
		SELECT x856
		SKIP
		LOOP
	ENDIF

	IF TRIM(x856.segment) = "N9"  && Additional info segments
		DO CASE
			CASE x856.f1 = "DP"
				REPLACE xpt.dept WITH ALLTRIM(x856.f1) IN xpt
			CASE x856.f1 = "CO"
				REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"SALESORDER*"+UPPER(ALLTRIM(x856.f2)) IN xpt
			CASE x856.f1 = "14"
				REPLACE xpt.batch_num WITH UPPER(ALLTRIM(x856.f2)) IN xpt
				IF !EMPTY(ALLTRIM(x856.f2))
					REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"MASTACCT*"+UPPER(ALLTRIM(x856.f2)) IN xpt
				ENDIF
			CASE x856.f1 = "ST"
				IF !EMPTY(ALLTRIM(x856.f2))
					REPLACE xpt.storenum WITH INT(VAL(ALLTRIM(x856.f2))) IN xpt
				ENDIF
		ENDCASE
		SELECT x856
		SKIP
		LOOP
	ENDIF

	IF TRIM(x856.segment) = "G62" AND TRIM(x856.f1) == "37"  && start date
		lcDate = ALLTRIM(x856.f2)
		lxDate = SUBSTR(lcDate,5,2)+"/"+SUBSTR(lcDate,7,2)+"/"+LEFT(lcDate,4)
		ldDate = CTOD(lxDate)
		STORE ldDate TO dStart
		REPLACE xpt.START WITH ldDate IN xpt
		SELECT x856
		SKIP
		LOOP
	ENDIF

	IF TRIM(x856.segment) = "G62" AND TRIM(x856.f1) == "38"  && cancel date
		lcDate = ALLTRIM(x856.f2)
		lxDate = SUBSTR(lcDate,5,2)+"/"+SUBSTR(lcDate,7,2)+"/"+LEFT(lcDate,4)
		ldDate = CTOD(lxDate)
		STORE ldDate TO dCancel
		REPLACE xpt.CANCEL WITH ldDate IN xpt
		SELECT x856
		SKIP
		LOOP
	ENDIF

	IF TRIM(x856.segment) = "W66"
		REPLACE terms WITH UPPER(ALLTRIM(x856.f1)) IN xpt
		REPLACE carrcode WITH UPPER(ALLTRIM(x856.f5)) IN xpt
		cSCAC = UPPER(ALLTRIM(x856.f10))
		REPLACE SCAC WITH cSCAC IN xpt

		cShip_via = UPPER(ALLTRIM(x856.f5))
		REPLACE ship_via WITH cShip_via IN xpt
		DO CASE
			CASE cShip_via = UPPER('FedexGroundDisabled')
				cShip_via = UPPER('Fedex Ground')
			CASE cShip_via = UPPER('FedexHomeDeliverydisabled')
				cShip_via = UPPER('Fedex Home Delivery')
			CASE cShip_via = UPPER('FedexGroundorHomedeliverydisabled')
				cShip_via = UPPER('Fedex Home Delivery')
			CASE cShip_via = UPPER('FedEx2Day')
				cShip_via = UPPER('FedEx 2Day')
		ENDCASE

		REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"SHIP-VIA*"+cShip_via IN xpt
		REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"SENTSCAC*"+cSCAC IN xpt
		SELECT x856
		SKIP
		LOOP
	ENDIF

	IF TRIM(x856.segment) = "LX" && start of PT detail, stay here until this PT is complete
*		ASSERT .F. MESSAGE cShip_ref+" Detail, Type = "+IIF(lPrepack,"Prepack","Pickpack")+"...debug"
		m.units = IIF(lPrepack,.F.,.T.)

		WAIT "NOW IN DETAIL LOOP PROCESSING" WINDOW NOWAIT

		SELECT xptdet
		CALCULATE MAX(ptdetid) TO m.ptdetid
		m.ptdetid = m.ptdetid + 1

		SELECT xptdet
		SKIP 1 IN x856
		lnLinenum=1
		DO WHILE ALLTRIM(x856.segment) != "SE"
			cSegment = TRIM(x856.segment)
			cCode = TRIM(x856.f1)
			IF TRIM(x856.segment) = "W01" && Destination Quantity
				SELECT xptdet
				APPEND BLANK
				REPLACE xptdet.office WITH xpt.office IN xptdet
				REPLACE xptdet.MOD WITH xpt.MOD IN xptdet
				REPLACE xptdet.ptid WITH xpt.ptid IN xptdet
				REPLACE xptdet.ptdetid WITH m.ptdetid IN xptdet
				REPLACE xptdet.units WITH m.units IN xptdet
				REPLACE xptdet.linenum WITH ALLTRIM(STR(lnLinenum)) IN xptdet
				lnLinenum = lnLinenum + 1
				m.ptdetid = m.ptdetid + 1
				REPLACE xptdet.accountid WITH xpt.accountid IN xptdet
				cUsePack = "1"
				REPLACE xptdet.PACK WITH cUsePack IN xptdet
				m.casepack = INT(VAL(cUsePack))
				REPLACE xptdet.casepack WITH m.casepack IN xptdet

				REPLACE xptdet.totqty WITH INT(VAL(x856.f1)) IN xptdet
				REPLACE xpt.qty WITH xpt.qty + xptdet.totqty IN xpt
				REPLACE xpt.origqty WITH xpt.qty IN xpt
				REPLACE xptdet.origqty WITH xptdet.totqty IN xptdet

				REPLACE xptdet.printstuff WITH "FILENAME*"+cFilename IN xpt
				REPLACE xptdet.printstuff WITH xptdet.printstuff+CHR(13)+"CONAME*S3-"+cGroup
				REPLACE xptdet.printstuff WITH xptdet.printstuff+CHR(13)+"UOM*"+ALLTRIM(x856.f2) IN xptdet

				IF ALLTRIM(x856.f4) = "IN"
					cStyle = UPPER(ALLTRIM(x856.f5))
*					cStyle = LEFT(cStyle,AT("-",cStyle,2)-1)
					REPLACE xptdet.STYLE WITH cStyle IN xptdet
*					REPLACE xptdet.custsku WITH cStyle IN xptdet
				ENDIF

				IF ALLTRIM(x856.f6) = "UP"
					REPLACE xptdet.upc WITH ALLTRIM(x856.f7) IN xptdet
				ENDIF

				IF ALLTRIM(x856.f15) = "VN"
					cCustsku = UPPER(ALLTRIM(x856.f16))
*!*						IF OCCURS('-',STYLE)>1
*!*							cStyle = LEFT(cStyle,AT("-",cStyle,2)-1)
*!*						ENDIF
*!*						REPLACE xptdet.STYLE WITH cStyle IN xptdet
*!*						REPLACE xptdet.STYLE WITH cStyle IN xptdet
					REPLACE xptdet.custsku WITH cCustsku IN xptdet
				ENDIF
			ENDIF

			IF TRIM(x856.segment) = "N9" AND TRIM(x856.f1) = "CL"  && color
				REPLACE xptdet.COLOR WITH UPPER(ALLTRIM(x856.f2)) IN xptdet
			ENDIF

			IF TRIM(x856.segment) = "N9" AND TRIM(x856.f1) = "SZ"  && size
				cSize = UPPER(ALLTRIM(x856.f2))
				IF !"."$cSize
					cSize = cSize+".0"
				ENDIF

				REPLACE xptdet.ID WITH cSize IN xptdet
			ENDIF

			IF TRIM(x856.segment) = "N9" AND TRIM(x856.f1) = "LT"  && color
				REPLACE xptdet.STYLE WITH ALLTRIM(xptdet.STYLE)+"/"+UPPER(ALLTRIM(x856.f2)) IN xptdet
			ENDIF

			IF TRIM(x856.segment) = "G69" && desc
				cPrtstr = "DESC*"+UPPER(TRIM(x856.f1))
				REPLACE xptdet.printstuff WITH xptdet.printstuff+CHR(13)+cPrtstr IN xptdet
			ENDIF

			SELECT x856
			SKIP 1 IN x856
		ENDDO

		SELECT xptdet
	ENDIF

	SELECT x856
	IF !EOF()
		SKIP 1 IN x856
	ENDIF
ENDDO

SELECT xptdet

Select xptdet
set Step On
Do rollup_ptdet_units

WAIT WINDOW "At end of ROLLUP procedure" NOWAIT

IF lBrowfiles
	WAIT WINDOW "If tables are incorrect, CANCEL when XPTDET window opens"+CHR(13)+"Otherwise, will load PT table" TIMEOUT 2
	SELECT xpt
	LOCATE
	BROWSE
	SELECT xptdet
	LOCATE
	BROWSE
	IF lTesting
*		CANCEL
	ENDIF
ENDIF

WAIT WINDOW cCustname+" Breakdown Round complete..." NOWAIT
SELECT x856
IF USED("PT")
	USE IN pt
ENDIF
IF USED("PTDET")
	USE IN ptdet
ENDIF
WAIT CLEAR
RETURN

*************************************************************************************************
*!* Error Mail procedure - Cancel Before Start Date
*************************************************************************************************
PROCEDURE errordates

	IF lEmail
		tsendto = tsendtotest
		tcc =  tcctest
		cMailLoc = IIF(cOffice = "C","West",IIF(cOffice = "N","New Jersey","Florida"))
		tsubject= "TGF "+cMailLoc+" "+cCustname+" Pickticket Error: " +TTOC(DATETIME())
		tmessage = "File: "+cFilename+CHR(13)+" has Cancel date(s) which precede Start date(s)"
		tmessage = tmessage + "File needs to be checked and possibly resubmitted."
		DO FORM m:\dev\frm\dartmail2 WITH tsendto,tFrom,tsubject,tcc,tattach,tmessage,"A"
	ENDIF
ENDPROC
*************************************************************************************************

