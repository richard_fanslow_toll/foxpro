* Report all vehicles which have not had FHWA Inspection within last 335 days.
* EXCLUDED FORKLIFTS per Jimmy O'Neill 04/10/06 MB

PUBLIC gHeader
gHeader = "TGF"

LOCAL ldToday, ld335DaysAgo, ldInspectDate, llHadInspection, lcComment, lnDays
ldToday = DATE()
ld335DaysAgo = ldToday - 335
lcComment = ''

IF USED('curInspect') THEN
	USE IN curInspect
ENDIF
IF USED('curInspectFinal') THEN
	USE IN curInspectFinal
ENDIF

CREATE CURSOR curInspect ( vehno C(8), lastinspct D, COMMENT C(50), vehtype C(8), nDays i, nDaysDesc i, padvehno C(10) )

IF NOT USED('INSPECTS') THEN
	USE SH!INSPECTS IN 0 ALIAS INSPECTS
ENDIF
SELECT INSPECTS
*SET ORDER TO INSP_DATE  && activate when index is added

IF NOT USED('TRUCKS') THEN
	USE SH!TRUCKS IN 0 ALIAS TRUCKS
ENDIF

SELECT TRUCKS
SCAN FOR ACTIVE
	WAIT WINDOW "Checking Truck # " + TRUCKS.TRUCK_NUM NOWAIT
	llHadInspection = .F.
	ldInspectDate = CTOD('')
	SELECT INSPECTS
	SCAN FOR equip_no = ALLTRIM(TRUCKS.TRUCK_NUM)
		ldInspectDate = INSPECTS.insp_date
		IF ldInspectDate > ld335DaysAgo THEN
			llHadInspection = .T.
			EXIT
		ENDIF
	ENDSCAN
	IF NOT llHadInspection THEN
		IF EMPTY(ldInspectDate) THEN
			lnDays = 999999
			lcComment = "No record of FHWA Inspection"
		ELSE
			lnDays = ldToday - ldInspectDate
			lcComment = TRANSFORM(lnDays) + " days since last FHWA Inspection"
		ENDIF
		SELECT curInspect
		APPEND BLANK
		REPLACE vehno WITH TRUCKS.TRUCK_NUM, ;
			padvehno WITH PADL(ALLTRIM(TRUCKS.TRUCK_NUM),10,"0"), ;
			lastinspct WITH ldInspectDate, ;
			COMMENT WITH lcComment, ;
			nDays WITH lnDays, ;
			nDaysDesc WITH (100000 - nDays), ;
			vehtype WITH "TRUCK"
	ENDIF
ENDSCAN
WAIT CLEAR

**********
* TRAILERS
IF NOT USED('TRAILER') THEN
	USE SH!TRAILER IN 0 ALIAS TRAILER
ENDIF

SELECT TRAILER
SCAN FOR ACTIVE
	WAIT WINDOW "Checking Trailer # " + TRAILER.TRAIL_NUM NOWAIT
	llHadInspection = .F.
	ldInspectDate = CTOD('')
	SELECT INSPECTS
	SCAN FOR equip_no = ALLTRIM(TRAILER.TRAIL_NUM)
		ldInspectDate = INSPECTS.insp_date
		IF ldInspectDate > ld335DaysAgo THEN
			llHadInspection = .T.
			EXIT
		ENDIF
	ENDSCAN
	IF NOT llHadInspection THEN
		IF EMPTY(ldInspectDate) THEN
			lnDays = 999999
			lcComment = "No record of FHWA Inspection"
		ELSE
			lnDays = ldToday - ldInspectDate
			lcComment = TRANSFORM(lnDays) + " days since last FHWA Inspection"
		ENDIF
		SELECT curInspect
		APPEND BLANK
		REPLACE vehno WITH TRAILER.TRAIL_NUM, ;
			padvehno WITH PADL(ALLTRIM(TRAILER.TRAIL_NUM),10,"0"), ;
			lastinspct WITH ldInspectDate, ;
			COMMENT WITH lcComment, ;
			nDays WITH lnDays, ;
			nDaysDesc WITH (100000 - nDays), ;
			vehtype WITH "TRAILER"
	ENDIF
ENDSCAN
WAIT CLEAR


*!*	* FORKLIFTS
*!*	IF NOT USED('VEHMAST') THEN
*!*		USE SH!VEHMAST IN 0 ALIAS VEHMAST
*!*	ENDIF

*!*	SELECT VEHMAST
*!*	SCAN FOR ACTIVE
*!*		WAIT WINDOW "Checking Forklift # " + VEHMAST.VEHNO NOWAIT
*!*		llHadInspection = .F.
*!*		ldInspectDate = CTOD('')
*!*		SELECT INSPECTS
*!*		SCAN FOR equip_no = ALLTRIM(VEHMAST.VEHNO)
*!*			ldInspectDate = INSPECTS.insp_date
*!*			IF ldInspectDate > ld335DaysAgo THEN
*!*				llHadInspection = .T.
*!*				EXIT
*!*			ENDIF
*!*		ENDSCAN
*!*		IF NOT llHadInspection THEN
*!*			IF EMPTY(ldInspectDate) THEN
*!*				lnDays = 999999
*!*				lcComment = "No record of FHWA Inspection"
*!*			ELSE
*!*				lnDays = ldToday - ldInspectDate
*!*				lcComment = TRANSFORM(lnDays) + " days since last FHWA Inspection"
*!*			ENDIF
*!*			SELECT curInspect
*!*			APPEND BLANK
*!*			REPLACE vehno WITH VEHMAST.VEHNO, ;
*!*				padvehno WITH PADL(ALLTRIM(VEHMAST.VEHNO),10,"0"), ;
*!*				lastinspct WITH ldInspectDate, ;
*!*				COMMENT WITH lcComment, ;
*!*				nDays WITH lnDays, ;
*!*				nDaysDesc WITH (100000 - nDays), ;
*!*				vehtype WITH "FORKLIFT"
*!*		ENDIF
*!*	ENDSCAN
*!*	WAIT CLEAR

*prepare & run report
SELECT * FROM curInspect INTO CURSOR curInspectFinal ORDER BY vehtype, nDaysDesc, padvehno

SELECT curInspectFinal
GOTO TOP
*BROWSE
KEYBOARD "{ctrl-f10}"
REPORT FORM SHINSPECT PREVIEW NOCONSOLE

IF USED('curInspect') THEN
	USE IN curInspect
ENDIF
IF USED('curInspectFinal') THEN
	USE IN curInspectFinal
ENDIF
