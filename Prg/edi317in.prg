utilsetup("EDI317")
Close Data All

Public lTesting
Public Array a856(1)

lTesting = .F.
Do m:\dev\prg\_setvars With lTesting

use f:\wo\wodata\wogenpk again in 0 alias generatepk
Use f:\wo\wodata\wolog In 0
Select * From wolog Where .f. Into Cursor twolog readwrite

*set Step On

If !lTesting
  lcPath = 'F:\FTPUSERS\TJMAX\317IN\'
  lcArchivePath = 'F:\FTPUSERS\TJMAX\317IN\archive\'
  tsendto="PGAIDIS@FMIINT.COM"
  tcc="PGAIDIS@FMIINT.COM"
Else
  lcPath = 'F:\FTPUSERS\TJMAX\317IN\'
  lcArchivePath = 'F:\FTPUSERS\TJMAX\317IN\archive\'
  tsendto="PGAIDIS@FMIINT.COM"
  tcc=""
Endif

Cd &lcPath
len1 = Adir(ary1,"*.*")
If len1 = 0
  Wait Window "No files found...exiting" Timeout 2
  Close Data All
  schedupdate()
  _Screen.Caption=gscreencaption
  On Error
  Return
Endif

For Thisfile = 1 To len1
  cFilename = Alltrim(ary1[thisfile,1])
  xfile = lcPath+cFilename
  Do m:\dev\prg\createx856a
  Do m:\dev\prg\loadedifile With xfile,"*","TILDE"

  m.addby = "FILE"+Alltrim(Transform(thisfile))
  m.adddt = Datetime()
  m.addproc = "FILE"+Alltrim(Transform(thisfile))  &&"ARIAT832"
  m.accountid = 6565

  Select x856
  Goto Top
  Scan
    Do Case
    Case Trim(x856.segment) = "ISA"
      cisa_num = Alltrim(x856.f13)
 
    Case Trim(x856.segment) = "GS"
      cgs_num = Alltrim(x856.f6)
 
    Case Trim(x856.segment) = "ST"
*      Insert Into ackdata (groupnum,isanum,transnum,edicode,accountid,loaddt,loadtime,filename) Values (lcCurrentGroupNum,cisa_num,Alltrim(x856.f2),"SC",6532,Date(),Datetime(),xfile)
 
    Case Trim(x856.segment) = "N1" And Alltrim(x856.f1) = "BR"
    
    Case Trim(x856.segment) = "N1" And Alltrim(x856.f1) = "SH"

    Case Trim(x856.segment) = "N1" And Alltrim(x856.f1) = "I3"

    Case Trim(x856.segment) = "G62" And Alltrim(x856.f1) = "17"

    Case Trim(x856.segment) = "G62" And Alltrim(x856.f1) = "80"


    Case Trim(x856.segment) = "N9" And Alltrim(x856.f1) = "OC"
      m.container = Alltrim(x856.f2)
      m.size      = Alltrim(x856.f3)
    
    Case Trim(x856.segment) = "N9" And Alltrim(x856.f1) = "ABS"
      m.vessel = Alltrim(x856.f2)
    
    Case Trim(x856.segment) = "TD5" And Alltrim(x856.f1) = "A"
      m.ssco = Alltrim(x856.f5)

    Endcase

  Endscan

Set Step On
  Do createworkorder
  
  cLoadFile = (lcPath+cFilename)
  cArchiveFile = (lcArchivePath+cFilename)
  Copy File [&cLoadFile] To [&cArchiveFile]
*  Delete File [&cLoadFile]

Endfor

Set Step On
************************************** END   AX2012
tattach = ""
tFrom ="Toll EDI System Operations <toll-edi-ops@tollgroup.com>"
tmessage = "Ariat Stylemaster updated at: "+Ttoc(Datetime())+Chr(13)+;
Chr(13)+lcAddedStr+Chr(13)+;
Chr(13)+lcUpdateStr

tSubject = "Ariat Stylemaster Update"
Do Form m:\dev\frm\dartmail2 With tsendto,tFrom,tSubject,tcc,tattach,tmessage,"A"
Wait Window "All 832 Files finished processing...exiting" Timeout 2

Close Data All

schedupdate()
*_Screen.Caption=gscreencaption
On Error

****************************************************************
Procedure CreateWorkOrder

  private m.wologid, m.wo_num, m.wo_date, m.office

   Select generatepk
   Locate for GPK_PK = "WOLOG"
   replace gpk_currentnumber with (gpk_currentnumber + 1) in generatepk
   m.wologid = generatepk.gpk_currentnumber
 
   select generatepk
   locate for gpk_pk = "WONUM"
   replace gpk_currentnumber with (gpk_currentnumber + 1) in generatepk
   m.wo_num = generatepk.gpk_currentnumber

   * don't carry these variables over from calling prog -- just reset
   m.accountid = 6565
   m.acctname = "TJX COMPANIES INC."
   m.wo_date = date()
   m.updproc = ""
   m.updatedt = {}
   m.updateby = ""
   m.adddt = datetime()
   m.addproc = "EDI317"
   m.addby   = "EDI317"

   m.wotype = "OCEAN FULL"
   m.office = "C"
   m.type = 'O'
   m.quantity = 1
   m.billtoid = 6565
   m.toloc    = 1  && to loc #1 for biglots = tremont pa dc
   m.tocs     = "SAN PEDRO, CA"
   m.qty_type = "CARTONS"
   m.fromloc  = 0
   m.toloc    = 2
   m.puloc    = ""
   m.retloc   = ""
   m.remarks  = ""
   m.notes    = ""
   m.wostatus = "NOT AVAILABLE"
   m.priority = 5

   insert into twolog from memvar
   
   && OK now lets see if there is any asn data for this container
   
   
