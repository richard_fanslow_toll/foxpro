* Find most current mm.exe
set step on

declare aexe[1]
adir("aexe","f:\3pl\proj\labelprint*.exe")
asort(aexe,1)

exedt=aexe(1,3)
exetime=aexe(1,4)
exenum=aexe(1,1)
for i=2 to alen(aexe,1)
  if (aexe(i,3)>exedt or (aexe(i,3)=exedt and aexe(i,4)>exetime)) and left(upper(aexe(i,1)),5)#"MMRUN"
    exedt=aexe(i,3)
    exetime=aexe(i,4)
    exenum=aexe(i,1)
  endif
next

wshshell=createobject("wscript.shell")
exenum= "f:\3pl\proj\"+exenum
wshshell.run(exenum)


