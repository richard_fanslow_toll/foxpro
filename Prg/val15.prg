* validate WMS data during the day - every 30 minutes

lparameters xskipreports

utilsetup("VAL15")

if holiday(date())
	return
endif

store "" to guserid, gprocess
gdevelopment=.f.
xjobno=0
xsystem="VAL15"

external array xdbf
 
use f:\auto\VAL15 exclusive
zap
use

* 

valset("check for invenloc with hold=.t. and empty(holdtype)")

select whoffice
scan for !test and !novalid
	xoffice=whoffice.office
	xmasteroffice=whoffice.rateoffice
	goffice=xoffice
	xfolder=whoffice.folder+"\whdata\"

	useca("inven","wh",,,"mod='"+goffice+"'")
	useca("invenloc","wh",,,"mod='"+goffice+"' and hold=1 and empty(holdtype)")	

	select invenid, accountid, units, style, color, id, pack, whseloc, locqty, updateby, updatedt, updproc ;
		from invenloc where .f. into cursor xinvenloc readwrite

	select * from invenloc where hold and empty(holdtype) into cursor xbadhold
	
*!*		scan
*!*			scatter memvar
*!*			insert into xinvenloc from memvar
*!*			
*!*			replace hold with .f. in invenloc for invenlocid=xbadhold.invenlocid
*!*			
*!*			select inven
*!*			locate for invenid=xbadhold.invenid

*!*			fixinven(.t.,,.t.)
*!*		endscan

	select xinvenloc
	count to xcnt

	if xcnt>0
		copy to ("f:\auto\xnoholdtype"+xoffice)
		email("Dyoung@fmiint.com","VAL "+transform(xcnt)+" hold=.t. and empty(holdtype) for "+xoffice,"xnoholdtype"+xoffice,,,,.t.,,,,,.t.)
*		email("Dyoung@fmiint.com","VAL (fixed) "+transform(xcnt)+" hold=.t. and empty(holdtype) for "+xoffice,"xnoholdtype"+xoffice,,,,.t.,,,,,.t.)
	endif

	if used("indet")
		use in indet
		use in inloc
	endif
	
	use in inven
	use in invenloc
endscan

*

valset("check for mismatched SWCLOC/BLLOADs")

select whoffice
scan for !test and !novalid and wireless 
	xoffice=whoffice.office
	goffice=xoffice
	xfolder=whoffice.folder+"\whdata\"

	xsqlexec("select * from bl where mod='"+goffice+"' and bldate>={"+dtoc(date()-14)+"} and started=1",,,"wh")
	xsqlexec("select * from blload where 1=0",,,"wh")
	
	select .f. as fixed, blid, space(20) as bol_no, {} as bldate, 0000 as accountid, swcnum, swcover, palletnum, ;
		00000 as swclocqty, 00000 as blloadqty, ;
		space(7) as swclocloc, space(7) as blloadloc ;
		from blload where .f. into cursor xswcload readwrite

	index on swcnum+swcover+str(palletnum,3) tag zorder

	select bl
	scan for bldate>=date()-14 and started
		m.blid=bl.blid
		m.bol_no=bl.bol_no
		m.bldate=bl.bldate
		m.accountid=bl.accountid
		m.blloadqty=0
		m.blloadloc=''

		xsqlexec("select * from swcdet where bol_no='"+bl.bol_no+"' and adddt>{"+dtoc(date()-90)+"}",,,"wh")
					
		select swcdet
		scan for bol_no=bl.bol_no
			m.swcnum=swcdet.swcnum
			m.swcover=swcdet.swcover
			xsqlexec("select * from swcloc where mod='"+xoffice+"' and swcdetid="+transform(swcdet.swcdetid)+" and locqty#0",,,"wh")
			scan
				m.palletnum=swcloc.palletnum
				m.swclocqty=swcloc.locqty
				m.swclocloc=iif(!empty(swcloc.stageloc),swcloc.stageloc,swcloc.labelloc)
				insert into xswcload from memvar
			endscan
		endscan

		m.swclocqty=0
		m.swclocloc=''
	
		xsqlexec("select * from blload where mod='"+xoffice+"' and blid="+transform(m.blid),,,"wh")
		scan
			m.swcnum=blload.swcnum
			m.swcover=blload.swcover
			m.palletnum=blload.palletnum
			m.blloadqty=blload.ctnqty
			m.blloadloc=blload.whseloc
			if seek(m.swcnum+m.swcover+str(m.palletnum,3),"xswcload","zorder")
				replace blloadqty with m.blloadqty, blloadloc with m.blloadloc in xswcload
			else
				insert into xswcload from memvar
			endif
		endscan
	endscan

	select xswcload
	delete for !((blloadqty#swclocqty or blloadloc#swclocloc) and swclocloc#"MP")
	delete for inlist(swcnum,"0842163","0843004","0840780","0842189","0842206","0842215")
	delete for inlist(bol_no,"01904270000060449","04907316182390450","08447020001308823","04907316182404881","08447020001308779","08447020001308762","08447020001308915","08447020001308908")
	delete for inlist(bol_no,"08447020001310239","08447020001310246","08447020001310277","08447020001310253","04907316521343147","04907316521374899","04907316803257292","04907316521384904")
	if date()<{5/1/18}
		delete for accountid=6561
	endif
	
	count to aaa

	if aaa>0
		xfixed=0
		scan 
			if !empty(xswcload.swclocloc)
				xsqlexec("select * from blload where mod='"+xoffice+"' and swcnum='"+xswcload.swcnum+"' and palletnum="+transform(xswcload.palletnum),,,"wh")

				if reccount()#0
					xtally = xsqlexec("update blload set whseloc='"+xswcload.swclocloc+"', ctnqty="+transform(xswcload.swclocqty)+" " + ;
						"where mod='"+xoffice+"' and swcnum='"+xswcload.swcnum+"' and palletnum="+transform(xswcload.palletnum),,,"wh")
						
					if xtally>0
						replace fixed with .t. in xswcload
						xfixed=xfixed+1
					endif
				endif
			endif
		endscan
		
		copy to ("f:\auto\xswcload"+xoffice)
		email("Dyoung@fmiint.com","VAL "+iif(aaa=xfixed,"(fixed) ","")+transform(aaa)+" mismatched SWCLOC/BLLOAD's, office "+xoffice,"xswcload"+xoffice,,,,.t.,,,,,.t.)
	endif
	
	use in bl
	use in blload
	if used("swcdet")
		use in swcdet
	endif
endscan

*

valset("check for empty entry field in daily for export loads")

use f:\wo\wodata\daily

select * from daily where action="LD" and empty(entry) into cursor xrpt

count to xcnt	
if xcnt>0
	copy to ("f:\auto\xldentry")
	email("Dyoung@fmiint.com","VAL "+transform(xcnt)+" empty entry field in daily for export loads","xldentry",,,,.t.,,,,,.t.)
endif

*

valset("fix labeled & staged dates")

select whoffice
scan for !test and !novalid and wireless
	xoffice=whoffice.office
	goffice=whoffice.office
	xfolder=whoffice.folder+"\whdata\"

	xsqlexec("select * from swcdet where mod='"+goffice+"' and adddt>{"+dtoc(date()-10)+"}",,,"wh")
	
	select " " as type, {} as swcdt, swcdetid, swcnum, space(20) as ship_ref, wo_num, accountid from swcdet where .f. into cursor xswcdet readwrite

	select swcdet
	scan for adddt>date()-10
		xsqlexec("select * from outwolog where wo_num="+transform(swcdet.wo_num),,,"wh")

		if swcdet.detlabelqty>0	and xoffice#"J"	&& outship.labelqty s/b filled in when labeling starts
			useca("outship","wh",,,"outwologid="+transform(swcdet.outwologid)+" and swcnum+swcover='"+swcdet.swcnum+swcdet.swcover+"' and labeled={} and ctnqty#0")
			
			select outship
			scan for outwologid=swcdet.outwologid and swcnum+swcover=swcdet.swcnum+swcdet.swcover and emptynul(labeled) and ctnqty#0
				select swcdet
				scatter memvar
				m.type="L"
				m.swcdt=iif(!emptynul(outship.picked),outship.picked,iif(!emptynul(outwolog.swcdt),outwolog.swcdt,max(outship.wo_date,date()-1)))
				m.ship_ref=outship.ship_ref
				insert into xswcdet from memvar
				replace labeled with m.swcdt in outship
			endscan
		endif

		if swcdet.detlabelqty>0 and swcdet.detstageleftqty<=0	&& outship.stageqty s/b filled in when staging is finished
			useca("outship","wh",,,"outwologid="+transform(swcdet.outwologid)+" and swcnum+swcover='"+swcdet.swcnum+swcdet.swcover+"' and staged={} and ctnqty#0")

			select outship
			scan for outwologid=swcdet.outwologid and swcnum+swcover=swcdet.swcnum+swcdet.swcover and emptynul(staged) and ctnqty#0
				select swcdet
				scatter memvar
				m.type="S"
				m.swcdt=nul2empty(outship.labeled)
				m.ship_ref=outship.ship_ref
				insert into xswcdet from memvar
				replace staged with m.swcdt in outship
			endscan
		endif
	endscan

	select xswcdet
	count to xcnt

	if xcnt>0
		copy to ("f:\auto\xlabstage"+xoffice)
*		email("Dyoung@fmiint.com","VAL (fixed) "+transform(xcnt)+" missing label/stage dates for "+xoffice,"xlabstage"+xoffice,,,,.t.,,,,,.t.)
		tu("outship")
	endif

	use in swcdet
	if used("outship")
		use in outship
	endif
	if used("outwolog")
		use in outwolog
	endif
endscan

*

valset("check for duplicate UCCs in swcscan")

xsqlexec("select ucc, mod, zdate from swcscan where zdate={"+dtoc(date())+"}","xtemp",,"wh")

select ucc, mod, zdate from xtemp group by 1,2 having count(1)>1 into cursor xrpt	
	
if reccount()>0
	copy to ("f:\auto\xswcscan")
	email("Dyoung@fmiint.com","VAL "+transform(reccount())+" duplicate UCCs in swcscan","xswcscan",,,,.t.,,,,,.t.)
endif

*

valset("fix mismatched bl's in bldet/outship")

select whoffice
scan for !test and !novalid
	xoffice=whoffice.office
	goffice=xoffice
	xfolder=whoffice.folder+"\whdata\"
	
	xsqlexec("select * from bl where mod='"+goffice+"' and adddt>={"+dtoc(date()-10)+"} and empty(mblnum) and bl.manual=0",,,"wh")
	
	xsqlexec("select * from bldet where mod='"+goffice+"' and adddt>{"+dtoc(date()-40)+"}",,,"wh")
	index on blid tag blid
	set order to
	
	create cursor xrpt (blid i, bol_no c(20), bldate d, accountid i, ship_via c(30), ship_ref c(20), combinedpt l, bldetqty n(6), outshipqty n(6))
	index on str(accountid,4)+ship_ref tag zorder

	select bl
	scan for adddt>=date()-10 and empty(mblnum) and !bl.manual ;
		and !inlist(bol_no,"04907316521922748","04907316521570123") ;
		and !(accountid=6097 and inlist(consignee,"BED","BB")) and !inlist(ship_via,"UPS","FEDEX","FED EX")
	
		m.blid=bl.blid
		m.bol_no=bl.bol_no
		m.bldate=bl.bldate
		m.ship_via=bl.ship_via
		m.outshipqty=0
			
		select bldet
		scan for blid=m.blid
			m.accountid=bldet.accountid
			m.ship_ref=bldet.ship_ref
			m.combinedpt=bldet.combinedpt
			m.bldetqty=bldet.detqty
			insert into xrpt from memvar
		endscan

		m.bldetqty=0
		
		xsqlexec("select * from outship where mod='"+goffice+"' and bol_no='"+m.bol_no+"' and wo_date>{"+dtoc(date()-200)+"}",,,"wh")
		
		select outship
		scan for bol_no=m.bol_no and wo_date>date()-300
			m.accountid=outship.accountid
			m.ship_ref=outship.ship_ref
			m.combinedpt=outship.combinedpt
			m.outshipqty=outship.ctnqty
			if m.combinedpt and seek(str(m.accountid,4)+outship.combpt,"xrpt","zorder")
				replace outshipqty with xrpt.outshipqty+m.outshipqty in xrpt
			else
				if seek(str(m.accountid,4)+m.ship_ref,"xrpt","zorder")
					replace outshipqty with xrpt.outshipqty+m.outshipqty in xrpt
				else
					insert into xrpt from memvar
				endif
			endif
		endscan
	endscan

	select xrpt
	delete for inlist(ship_ref,"3181238-B           ","3181238-A %U        ","025013","6103228-A %C        ","EJ102313L-B_10QHG7/Y","0001548896","001239EL17")
	delete for inlist(bol_no,"04331826218349237","04331826218177809","04331826218197289","04331826218197302")
	delete for inlist(bol_no,"04331826218894423","04331826218915197","04331826218922553","04331826218934266","04331826218958378","04331826218008264","04907316182131206")
	delete for inlist(bol_no,"04331826218026633","04331826218026641","04331826218026541","04331826218055992","04331826218059679","04907316182072356","04331826218080628")
	delete for inlist(SHIP_REF,"995670EL17","995671EL17","995672EL17","997610BB17","048607BB17","096267BB17")
	delete for inlist(ship_via,"DHL","TRADE DIRECT","ONTRAC")
	delete for bldate<date()-270
	count for bldetqty#outshipqty to aa
	
	if aa>0
		use in bl
		use in bldet
		
		select xrpt
		scan for bldetqty#outshipqty
			useca("bl","wh",,,"blid="+transform(xrpt.blid))
			useca("bldet","wh",,,"blid="+transform(xrpt.blid))

			scan
				if !combinedpt
					if xsqlexec("select * from outship where accountid="+transform(bldet.accountid)+" and ship_ref='"+bldet.ship_ref+"'",,,"wh") > 0
						if outship.ctnqty#bldet.detqty 
							replace detqty with outship.ctnqty in bldet
						endif
						if round(outship.weight,0)#bldet.detwt and outship.weight#0
							replace detwt with iif(bldet.detwt>0 and bldet.detwt<.5,1,round(outship.weight,0)) in bldet
						endif
						if outship.cuft#bldet.detcube and outship.cuft#0
							replace detcube with outship.cuft in bldet
						endif
					endif
				else
					if xsqlexec("select * from outship where accountid="+transform(bldet.accountid)+" and combpt='"+bldet.ship_ref+"'",,,"wh") > 0
						sum ctnqty, iif(weight>0 and weight<.5,1,round(weight,0)), cuft to xctnqty, xweight, xcuft for accountid=bldet.accountid and combpt=bldet.ship_ref
						
						if xctnqty#bldet.detqty 
							replace detqty with xctnqty in bldet
						endif
						if round(xweight,0)#bldet.detwt and xweight#0
							replace detwt with iif(bldet.detwt>0 and bldet.detwt<.5,1,round(xweight,0)) in bldet
						endif
						if xcuft#bldet.detcube and xcuft#0
							replace detcube with xcuft in bldet
						endif
					endif
				endif
			endscan
			
			select bldet
			calculate sum(detqty), sum(detwt), sum(detcube) to ww,yy,zz

			if ww#bl.totqty
				replace totqty with ww in bl
			endif
			if yy#bl.totwt
				replace totwt with yy in bl
			endif
			if zz#bl.totcube 
				replace totcube with zz in bl
			endif
			
			tu("bl")
			tu("bldet")
		endscan
		
		copy to ("f:\auto\xblmis"+xoffice) for bldetqty#outshipqty
		email("Dyoung@fmiint.com","VAL (fixed) "+transform(aa)+" mismatched bl's in bldet/outship, office "+xoffice,"xblmis"+xoffice,,,,.t.,,,,,.t.)
	endif

	use in bl
	use in bldet
	if used("outship")
		use in outship
	endif
endscan

*

valset("fix locqty#detlabelqty in swcdet/swcloc")

select whoffice
scan for !test and !novalid and wireless
	xoffice=whoffice.office
	goffice=xoffice
	xfolder=whoffice.folder+"\whdata\"

	xsqlexec("select * from outwolog where mod='"+goffice+"' and swcdt>{"+dtoc(date()-10)+"} and swcsort=1",,,"wh")
	xsqlexec("select * from swcdet where 1=0",,,"wh")

	select swcdetid, wo_num, accountid, swcnum, 00000 as locqty, detlabelqty from swcdet where .f. into cursor xrpt readwrite

	select outwolog
	scan for swcdt>date()-10 and swcsort
		xsqlexec("select * from swcdet where mod='"+goffice+"' and outwologid="+transform(outwolog.outwologid),,,"wh")
		scan
			xsqlexec("select * from swcloc where mod='"+xoffice+"' and swcdetid="+transform(swcdet.swcdetid),,,"wh")
			sum locqty to aa 
			if aa#swcdet.detlabelqty
				select swcdet
				m.locqty=aa
				scatter memvar
				insert into xrpt from memvar
				xsqlexec("update swcdet set detlabelqty="+transform(aa)+", detleftqty=detexpqty-detlabelqty-detcutqty-notonwipqty " + ;
					"where swcdetid="+transform(swcdet.swcdetid),,,"wh")
			endif
		endscan
	endscan
	
	select xrpt
	if reccount()>0
		copy to ("f:\auto\xswcloclabelqty"+xoffice)
*		email("Dyoung@fmiint.com","VAL (fixed) "+transform(reccount())+" locqty#detlabelqty in swcdet/swcloc for "+xoffice,"xswcloclabelqty"+xoffice,,,,.t.,,,,,.t.)
	endif

	use in outwolog
	use in swcdet
	if used("swcloc")
		use in swcloc
	endif
endscan

*

valset("check for two un-closed swcloc's")

select whoffice
scan for !test and !novalid and wireless and inlist(office,"J","7")
	xoffice=whoffice.office
	goffice=xoffice
	xfolder=whoffice.folder+"\whdata\"

	xsqlexec("select * from outwolog where mod='"+goffice+"' and swcdt>{"+dtoc(date()-10)+"} and swcsort=1",,,"wh")
	xsqlexec("select * from swcdet where 1=0",,,"wh")

	select swcdetid, wo_num, accountid, swcnum from swcdet where .f. into cursor xrpt readwrite
	
	select outwolog
	scan for swcdt>date()-10 and swcsort
		xsqlexec("select * from swcdet where mod='"+goffice+"' and outwologid="+transform(outwolog.outwologid),,,"wh")
		scan
			xsqlexec("select * from swcloc where mod='"+xoffice+"' and swcdetid="+transform(swcdet.swcdetid)+" and closed=0",,,"wh")
			count to aa
			if aa>1
				select swcdet
				m.locqty=aa
				scatter memvar
				insert into xrpt from memvar
			endif
		endscan
	endscan
	
	select xrpt
	if reccount()>0
		copy to ("f:\auto\xswc2unclosed"+xoffice)
		email("Dyoung@fmiint.com","VAL "+transform(reccount())+" two un-closed swcloc's for "+xoffice,"xswc2unclosed"+xoffice,,,,.t.,,,,,.t.)
	endif

	use in outwolog
	use in swcdet
	if used("swcloc")
		use in swcloc
	endif
endscan

*

valset("check for missing chassis in drop")

use f:\wo\wodata\wolog in 0
use f:\wo\wodata\tdaily in 0
use f:\wo\wodata\drop in 0

select * from drop where .f. into cursor xrpt readwrite

select drop
scan for !empty(container) and empty(chassis)
	select tdaily
	locate for wo_num=drop.wo_num and !empty(chassis)
	if found()
		select drop
		scatter memvar
		insert into xrpt from memvar
		
		replace chassis with tdaily.chassis in drop
		replace chassis with tdaily.chassis in wolog for wo_num=drop.wo_num
	endif
endscan

select xrpt
count to aa

if aa>0
	select xrpt
	copy to f:\auto\xdropchassis
	email("Dyoung@fmiint.com","VAL (fixed) "+transform(aa)+" missing chassis in drop","xdropchassis",,,,.t.,,,,,.t.)
endif

* 

valset("check for multiple P&P cartons for one wo/group")

xsqlexec("select * from account where inactive=0","account",,"qq")
index on accountid tag accountid
set order to

select whoffice
scan for !test and !novalid and picknpack
	xoffice=whoffice.office
	goffice=xoffice
	xfolder=whoffice.folder+"\whdata\"
	xpnpaccount=pnpacct(xoffice)

	useca("indet","wh",,,xpnpaccount+" and mod='"+goffice+"' and date_rcvd>{"+dtoc(date()-7)+"}")

	select * from indet where &xpnpaccount and !units and date_rcvd>date()-7 ;
		group by wo_num, po having count(wo_num)>1 into cursor xdupppctn

*!*		scan 
*!*			xmaxpo=maxpo(xdupppctn.wo_num)
*!*			xctn=0
*!*			
*!*			select indet
*!*			scan for wo_num=xdupppctn.wo_num and po=xdupppctn.po
*!*				if !units
*!*					xctn=xctn+1
*!*				endif
*!*				if xctn>1
*!*					replace po with xmaxpo in indet
*!*				endif
*!*			endscan
*!*		endscan

	if reccount("xdupppctn")>0
*		tu("indet")
		
		select xdupppctn
		copy to ("f:\auto\xdupppctn"+xoffice)
		email("Dyoung@fmiint.com","VAL "+transform(_tally)+" multiple P&P cartons for one wo/group for "+xoffice,"xdupppctn"+xoffice,,,,.t.,,,,,.t.)
	endif
	
	use in indet
endscan

*

valset("check for extra styles in indet for Synclaire")

goffice='2'

xsqlexec("select * from indet where inlist(accountid,6521,6561) and !empty(po) and date_rcvd={"+dtoc(date())+"}","xdytemp",,"wh")
select * from xdytemp group by wo_num, po having count(1)=3 into cursor xrpt

if reccount()>0
	copy to ("f:\auto\x2xuindet")
	email("Dyoung@fmiint.com","VAL "+transform(reccount())+" extra styles in indet for 2XU","x2xuindet",,,,.t.,,,,,.t.)
endif

*

valset("check for extra styles in indet for 2XU")

goffice='Y'

xsqlexec("select * from indet where inlist(accountid,6665) and !empty(po) and date_rcvd={"+dtoc(date())+"}","xdytemp",,"wh")
select * from xdytemp group by wo_num, po having count(1)=3 into cursor xrpt

if reccount()>0
	copy to ("f:\auto\x2xuindet")
	email("Dyoung@fmiint.com","VAL "+transform(reccount())+" extra styles in indet for 2XU","x2xuindet",,,,.t.,,,,,.t.)
endif

*

use f:\auto\val15
go bottom
replace zendtime with datetime()
replace all seconds with zendtime-zstarttime
use 

schedupdate()

_screen.Caption=gscreencaption
on error
