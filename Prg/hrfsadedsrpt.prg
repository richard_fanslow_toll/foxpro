* HRFSADEDSRPT.PRG
* produce WEEKLY FSA DEDUCTIONS spreadsheet - run each Thursday for that Friday's pay....

* Build EXE as F:\UTIL\HR\F:\UTIL\HR\FSA\HRFSADEDSRPT.exe
LOCAL loFSADEDSREPORT

runack("HRFSADEDSRPT")

utilsetup("HRFSADEDSRPT")

loFSADEDSREPORT = CREATEOBJECT('FSADEDSREPORT')

loFSADEDSREPORT.MAIN()

schedupdate()

CLOSE DATABASES ALL
RETURN


#DEFINE LOGIT 1
#DEFINE WAITIT 2
#DEFINE NOWAITIT 4
#DEFINE SENDIT 8
#DEFINE CRLF CHR(13) + CHR(10)
#DEFINE RETURN_DATA_MANDATORY .F.
#DEFINE RETURN_DATA_NOT_MANDATORY .T.
#DEFINE CSVFORMAT 6
#DEFINE WRITE_ONLY_UNBUFFERED 11
#DEFINE COMMA ","
#DEFINE xlEdgeBottom 9
#DEFINE xlContinuous 1
#DEFINE xlMedium -4138
#DEFINE xlFONT_RED -16776961

DEFINE CLASS FSADEDSREPORT AS CUSTOM

	cProcessName = 'FSADEDSREPORT'

	lTestMode = .F.  && .F. FOR NORMAL OPERATION, .T. TO SUPPRESS ALL BUT DEVELOPER EMAILS AND CHANGE INPUT TABLE, ETC.

	dToday = DATE()
	*dToday = {^2014-01-01}

	lAutoYield = .T.

	cStartTime = TTOC(DATETIME())

	* connection properties
	nSQLHandle = 0

	* wait window properties
	nWaitWindowTimeout = 2
	lWaitWindowIsOn = .T.

	* logfile properties
	lLoggingIsOn = .T.
	cLogFile = 'F:\UTIL\HR\FSA\LOGFILES\FSADEDS_Report_log.txt'

	* INTERNAL email properties
	lSendInternalEmailIsOn = .T.
	cFrom = 'Mark.Bennett@Tollgroup.com'
	cSendTo = 'marie.freiberger@tollgroup.com, Lauren.Klaver@Tollgroup.com'
	cCC = 'mbennett@fmiint.com'
	cSubject = ''
	cAttach = ''
	cBodyText = ''

	cCOMPUTERNAME = ''
	cUSERNAME = ''


	FUNCTION INIT
		IF NOT DODEFAULT()
			RETURN .F.
		ENDIF
		WITH THIS
			*SET RESOURCE OFF
			CLOSE DATA
			SET DECIMALS TO 4
			SET CENTURY ON
			SET DATE AMERICAN
			SET HOURS TO 24
			SET ANSI ON
			SET TALK OFF
			SET DELETED ON
			SET CONSOLE OFF
			SET EXCLUSIVE OFF
			SET SAFETY OFF
			SET EXACT OFF
			SET STATUS BAR ON
			SET SYSMENU OFF
			SET ENGINEBEHAVIOR 70
			_VFP.AUTOYIELD = .lAutoYield
			.cCOMPUTERNAME = GETENV("COMPUTERNAME")
			.cUSERNAME = GETENV("USERNAME")
			IF .lTestMode THEN
				.cLogFile = 'F:\UTIL\HR\FSA\LOGFILES\FSADEDS_Report_log_TESTMODE.txt'
				.cSendTo = 'mbennett@fmiint.com'
			ENDIF
			.lLoggingIsOn = .lLoggingIsOn AND NOT EMPTY(.cLogFile)
			IF .lLoggingIsOn THEN
				SET ALTERNATE TO (.cLogFile) ADDITIVE
				SET ALTERNATE ON
			ENDIF
			SET PROCEDURE TO VALIDATIONS ADDITIVE
		ENDWITH
	ENDFUNC


	FUNCTION DESTROY
		WITH THIS
			IF .lLoggingIsOn  THEN
				SET ALTERNATE OFF
				SET ALTERNATE TO
			ENDIF
		ENDWITH
		DODEFAULT()
	ENDFUNC


	FUNCTION MAIN
		WITH THIS
			LOCAL lnNumberOfErrors, lcSQL, lcSQL2, loError, ldToday
			LOCAL lcFiletoSaveAs, lcSpreadsheetTemplate
			LOCAL ldFromDate, ldToDate, lcADPFromDate, lcADPToDate, ldTermCheckDate, lcADPTermCheckDate
			LOCAL lcDateSuffix, lnTotalFSA, ldEligDate, ldFYBeginDate
			LOCAL i, oExcel, oWorkbook, oWorksheet, lnStartRow, lcStartRow, lnRow, lcRow
			LOCAL lcFirstName, lcNewFirstName, lcLastName, lnSpacePos, lcMI, lnMaxAmount

			TRY
				lnNumberOfErrors = 0

				.TrackProgress("FSA DEDS REPORT process started....", LOGIT+SENDIT+NOWAITIT)
				.TrackProgress('COMPUTERNAME = ' + .cCOMPUTERNAME, LOGIT+SENDIT)
				.TrackProgress('USERNAME = ' + .cUSERNAME, LOGIT+SENDIT)
				.TrackProgress('PROJECT = HRFSADEDSRPT', LOGIT+SENDIT)
				IF .lTestMode THEN
					.TrackProgress('                        TEST MODE                        ', LOGIT+SENDIT)
				ENDIF

				* determine prior Saturday
				ldFromDate = .dToday
				DO WHILE DOW(ldFromDate,1) <> 7
					ldFromDate = ldFromDate - 1
				ENDDO
				ldToDate = ldFromDate + 6
				
*!*	******************************************
*!*	** activate to force specific date range
*!*	ldFromDate = {^2013-10-05}
*!*	ldToDate = {^2013-10-11}
*!*	******************************************

				ldTermCheckDate = ldFromDate - 7

				lcDateSuffix = DTOC(ldFromDate) + " - " + DTOC(ldToDate)
				.cSubject = 'FSA DEDS REPORT for ' + lcDateSuffix


				lcADPFromDate = "DATE'" + TRANSFORM(YEAR(ldFromDate)) + "-" + PADL(MONTH(ldFromDate),2,'0') + "-" + PADL(DAY(ldFromDate),2,'0') + "'"
				lcADPToDate = "DATE'" + TRANSFORM(YEAR(ldToDate)) + "-" + PADL(MONTH(ldToDate),2,'0') + "-" + PADL(DAY(ldToDate),2,'0') + "'"
				lcADPTermCheckDate = "DATE'" + TRANSFORM(YEAR(ldTermCheckDate)) + "-" + PADL(MONTH(ldTermCheckDate),2,'0') + "-" + PADL(DAY(ldTermCheckDate),2,'0') + "'"

				lcSpreadsheetTemplate = 'F:\UTIL\HR\FSA\TEMPLATES\FSA TEMPLATE.XLS'
				lcFiletoSaveAs = 'F:\UTIL\HR\FSA\REPORTS\TGFSTAFFING' + STRTRAN(lcDateSuffix,"/","") + '.XLS'
				.TrackProgress('Creating spreadsheet: ' + lcFiletoSaveAs, LOGIT+SENDIT+NOWAITIT)


				* delete output file if it already exists...
				IF FILE(lcFiletoSaveAs) THEN
					DELETE FILE (lcFiletoSaveAs)
				ENDIF

				* now connect to ADP...

				.nSQLHandle = SQLCONNECT("PCPAYWIN","MBENNETT","VP737A")

				IF .nSQLHandle > 0 THEN

					lcWhere = ""
					
* THIS FOLLOWING SQL GETS ACTIVE EMPLOYEES WHO HAVE AN ACTIVE FSA DEDUCTION

					SET TEXTMERGE ON
					TEXT TO	lcSQL NOSHOW
SELECT
DISTINCT
837131 AS CONTROL,
10 AS SUFFIX,
0 AS ACCOUNT,
A.SOCIALSECURITY# AS SSN,
A.LASTNAME,
A.FIRSTNAME,
' ' AS MI,
A.NAME,
{FN CONCAT(A.STREETLINE1,{FN CONCAT(' ',{FN IFNULL(A.STREETLINE2,'')})})} AS STREET1,
' ' AS STREET2,
A.CITY,
A.STATE,
A.ZIPCODE,
' ' AS ZIPEXT,
A.GENDER,
A.BIRTHDATE,
DATE'2012-07-01' AS OPDATE,
A.STATUS,
DATE'2012-07-01' AS STATDATE,
A.CUSTAREA2,
00000.00 AS MAXAMOUNT,
DATE'2012-07-01' AS MAXDATE,
1 AS PRODCODE,
' ' AS ERCONTRIB,
(SELECT {FN IFNULL(SUM(CHECKVIEWDEDAMT),0.00)} FROM REPORTS.V_CHK_VW_DEDUCTION
 WHERE FILE#=A.FILE#
 AND CHECKVIEWPAYDATE >= <<lcADPFromDate>>
 AND CHECKVIEWPAYDATE <= <<lcADPToDate>>
 AND CHECKVIEWDEDCODE = '1') AS EECONTRIB,
{FN IFNULL((SELECT DEDUCTIONAMOUNT FROM REPORTS.V_DEDUCTIONS
WHERE DEDUCTIONCODE IN ('1') AND FILE# = A.FILE# AND STATUS = 'A'),0.00)} AS FSAAMT,
A.COMPANYCODE AS ADP_COMP,
A.HIREDATE,
A.TERMINATIONDATE,
{FN IFNULL((SELECT DEDUCTIONCODE FROM REPORTS.V_DEDUCTIONS 
WHERE DEDUCTIONCODE IN ('G','H') AND FILE# = A.FILE# AND STATUS = 'A'),' ')} AS HEALTHCODE,
{FN IFNULL((SELECT DEDUCTIONCODE FROM REPORTS.V_DEDUCTIONS 
WHERE DEDUCTIONCODE IN ('3') AND FILE# = A.FILE# AND STATUS = 'A'),' ')} AS VISIONCODE
FROM REPORTS.V_EMPLOYEE A
WHERE ( A.STATUS <> 'T' )
AND A.NAME IN
(SELECT NAME FROM REPORTS.V_DEDUCTIONS
 WHERE STATUS <> 'T'
 AND DEDUCTIONCODE = '1')
ORDER BY NAME
					ENDTEXT
					SET TEXTMERGE OFF

* THIS FOLLOWING SQL GETS TERMINATED EMPLOYEES WHO HAVE A TERMINATED FSA DEDUCTION AND NO ACTIVE FSA DEDUCTION
* MODIFIED TO ONLY INCLUDE SOMEONE WHO HAD AN FSA DEDUCTION IN THE PAY PERIOD 12/12/12 MB PER LAUREN
					SET TEXTMERGE ON
					TEXT TO	lcSQL2 NOSHOW
SELECT
DISTINCT
837131 AS CONTROL,
10 AS SUFFIX,
0 AS ACCOUNT,
A.SOCIALSECURITY# AS SSN,
A.LASTNAME,
A.FIRSTNAME,
' ' AS MI,
A.NAME,
{FN CONCAT(A.STREETLINE1,{FN CONCAT(' ',{FN IFNULL(A.STREETLINE2,'')})})} AS STREET1,
' ' AS STREET2,
A.CITY,
A.STATE,
A.ZIPCODE,
' ' AS ZIPEXT,
A.GENDER,
A.BIRTHDATE,
DATE'2012-07-01' AS OPDATE,
A.STATUS,
DATE'2012-07-01' AS STATDATE,
A.CUSTAREA2,
00000.00 AS MAXAMOUNT,
DATE'2012-07-01' AS MAXDATE,
1 AS PRODCODE,
' ' AS ERCONTRIB,
(SELECT {FN IFNULL(SUM(CHECKVIEWDEDAMT),0.00)} FROM REPORTS.V_CHK_VW_DEDUCTION
 WHERE FILE#=A.FILE#
 AND CHECKVIEWPAYDATE >= <<lcADPFromDate>>
 AND CHECKVIEWPAYDATE <= <<lcADPToDate>>
 AND CHECKVIEWDEDCODE = '1') AS EECONTRIB,
{FN IFNULL((SELECT DEDUCTIONAMOUNT FROM REPORTS.V_DEDUCTIONS
WHERE DEDUCTIONCODE IN ('1') AND FILE# = A.FILE# AND STATUS = 'A'),0.00)} AS FSAAMT,
A.COMPANYCODE AS ADP_COMP,
A.HIREDATE,
A.TERMINATIONDATE,
{FN IFNULL((SELECT DEDUCTIONCODE FROM REPORTS.V_DEDUCTIONS 
WHERE DEDUCTIONCODE IN ('G','H') AND FILE# = A.FILE# AND STATUS = 'A'),' ')} AS HEALTHCODE,
{FN IFNULL((SELECT DEDUCTIONCODE FROM REPORTS.V_DEDUCTIONS 
WHERE DEDUCTIONCODE IN ('3') AND FILE# = A.FILE# AND STATUS = 'A'),' ')} AS VISIONCODE
FROM REPORTS.V_EMPLOYEE A
WHERE ( A.STATUS = 'T' )
AND A.NAME IN
(SELECT NAME FROM REPORTS.V_CHK_VW_DEDUCTION
 WHERE CHECKVIEWPAYDATE >= <<lcADPFromDate>>
 AND CHECKVIEWPAYDATE <= <<lcADPToDate>>
 AND CHECKVIEWDEDCODE = '1')
AND A.COMPANYCODE IN ('E87','E88','E89')
ORDER BY NAME
					ENDTEXT
					SET TEXTMERGE OFF

*!*	SELECT
*!*	DISTINCT
*!*	837131 AS CONTROL,
*!*	10 AS SUFFIX,
*!*	0 AS ACCOUNT,
*!*	A.SOCIALSECURITY# AS SSN,
*!*	A.LASTNAME,
*!*	A.FIRSTNAME,
*!*	' ' AS MI,
*!*	{FN CONCAT(A.STREETLINE1,{FN CONCAT(' ',{FN IFNULL(A.STREETLINE2,'')})})} AS STREET1,
*!*	' ' AS STREET2,
*!*	A.CITY,
*!*	A.STATE,
*!*	A.ZIPCODE,
*!*	' ' AS ZIPEXT,
*!*	A.GENDER,
*!*	A.BIRTHDATE,
*!*	DATE'2012-07-01' AS OPDATE,
*!*	A.STATUS,
*!*	DATE'2012-07-01' AS STATDATE,
*!*	A.CUSTAREA2,
*!*	00000.00 AS MAXAMOUNT,
*!*	DATE'2012-07-01' AS MAXDATE,
*!*	1 AS PRODCODE,
*!*	' ' AS ERCONTRIB,
*!*	(SELECT {FN IFNULL(SUM(CHECKVIEWDEDAMT),0.00)} FROM REPORTS.V_CHK_VW_DEDUCTION
*!*	 WHERE FILE#=A.FILE#
*!*	 AND CHECKVIEWPAYDATE >= <<lcADPFromDate>>
*!*	 AND CHECKVIEWPAYDATE <= <<lcADPToDate>>
*!*	 AND CHECKVIEWDEDCODE = '1') AS EECONTRIB,
*!*	{FN IFNULL((SELECT DEDUCTIONAMOUNT FROM REPORTS.V_DEDUCTIONS
*!*	WHERE DEDUCTIONCODE IN ('1') AND FILE# = A.FILE# AND STATUS = 'A'),0.00)} AS FSAAMT,
*!*	A.COMPANYCODE AS ADP_COMP,
*!*	A.HIREDATE,
*!*	A.TERMINATIONDATE,
*!*	{FN IFNULL((SELECT DEDUCTIONCODE FROM REPORTS.V_DEDUCTIONS 
*!*	WHERE DEDUCTIONCODE IN ('G','H') AND FILE# = A.FILE# AND STATUS = 'A'),' ')} AS HEALTHCODE,
*!*	{FN IFNULL((SELECT DEDUCTIONCODE FROM REPORTS.V_DEDUCTIONS 
*!*	WHERE DEDUCTIONCODE IN ('3') AND FILE# = A.FILE# AND STATUS = 'A'),' ')} AS VISIONCODE
*!*	FROM REPORTS.V_EMPLOYEE A
*!*	WHERE ( A.STATUS = 'T' )
*!*	AND A.NAME IN
*!*	(SELECT NAME FROM REPORTS.V_DEDUCTIONS
*!*	 WHERE STATUS = 'T'
*!*	 AND DEDUCTIONCODE = '1')
*!*	AND A.NAME NOT IN
*!*	(SELECT NAME FROM REPORTS.V_DEDUCTIONS
*!*	 WHERE STATUS <> 'T'
*!*	 AND DEDUCTIONCODE = '1')
*!*	ORDER BY NAME
*!*						ENDTEXT
*!*						SET TEXTMERGE OFF

*!*						SET TEXTMERGE ON
*!*						TEXT TO	lcSQL NOSHOW
*!*	SELECT
*!*	DISTINCT
*!*	837131 AS CONTROL,
*!*	10 AS SUFFIX,
*!*	0 AS ACCOUNT,
*!*	A.SOCIALSECURITY# AS SSN,
*!*	A.LASTNAME,
*!*	A.FIRSTNAME,
*!*	' ' AS MI,
*!*	{FN CONCAT(A.STREETLINE1,{FN CONCAT(' ',{FN IFNULL(A.STREETLINE2,'')})})} AS STREET1,
*!*	' ' AS STREET2,
*!*	A.CITY,
*!*	A.STATE,
*!*	A.ZIPCODE,
*!*	' ' AS ZIPEXT,
*!*	A.GENDER,
*!*	A.BIRTHDATE,
*!*	DATE'2012-07-01' AS OPDATE,
*!*	A.STATUS,
*!*	DATE'2012-07-01' AS STATDATE,
*!*	0 AS MAXAMOUNT,
*!*	DATE'2012-07-01' AS MAXDATE,
*!*	1 AS PRODCODE,
*!*	' ' AS ERCONTRIB,
*!*	(SELECT {FN IFNULL(SUM(CHECKVIEWDEDAMT),0.00)} FROM REPORTS.V_CHK_VW_DEDUCTION
*!*	 WHERE FILE#=A.FILE#
*!*	 AND CHECKVIEWPAYDATE >= <<lcADPFromDate>>
*!*	 AND CHECKVIEWPAYDATE <= <<lcADPToDate>>
*!*	 AND CHECKVIEWDEDCODE = '1') AS EECONTRIB,
*!*	{FN IFNULL((SELECT DEDUCTIONAMOUNT FROM REPORTS.V_DEDUCTIONS
*!*	WHERE DEDUCTIONCODE IN ('1') AND FILE# = A.FILE# AND STATUS = 'A'),0.00)} AS FSAAMT,
*!*	A.COMPANYCODE AS ADP_COMP,
*!*	A.HIREDATE,
*!*	A.TERMINATIONDATE,
*!*	{FN IFNULL((SELECT DEDUCTIONCODE FROM REPORTS.V_DEDUCTIONS 
*!*	WHERE DEDUCTIONCODE IN ('G','H') AND FILE# = A.FILE# AND STATUS = 'A'),' ')} AS HEALTHCODE,
*!*	{FN IFNULL((SELECT DEDUCTIONCODE FROM REPORTS.V_DEDUCTIONS 
*!*	WHERE DEDUCTIONCODE IN ('3') AND FILE# = A.FILE# AND STATUS = 'A'),' ')} AS VISIONCODE
*!*	FROM REPORTS.V_EMPLOYEE A
*!*	WHERE  ( ( A.STATUS <> 'T' ) OR ( A.STATUS = 'T' AND A.TERMINATIONDATE >= <<lcADPTermCheckDate>> ) )
*!*	AND A.NAME IN
*!*	(SELECT NAME FROM REPORTS.V_DEDUCTIONS
*!*	 WHERE STATUS <> 'T'
*!*	 AND DEDUCTIONCODE = '1')
*!*	ORDER BY NAME
*!*						ENDTEXT
*!*						SET TEXTMERGE OFF


*!*	WHERE  ( ( A.STATUS <> 'T' ) OR ( A.STATUS = 'T' AND A.TERMINATIONDATE >= <<lcADPTermCheckDate>> ) )
*!*	AND A.NAME IN
*!*	(SELECT NAME FROM REPORTS.V_CHK_VW_DEDUCTION
*!*	 WHERE CHECKVIEWPAYDATE >= <<lcADPFromDate>>
*!*	 AND CHECKVIEWPAYDATE <= <<lcADPToDate>>
*!*	 AND CHECKVIEWDEDCODE = '1')
*!*	ORDER BY NAME

*!*	WHERE  
*!*	(  (A.STATUS <> 'T') OR 
*!*	   ( (A.STATUS = 'T') AND 
*!*	     (A.NAME NOT IN ( SELECT NAME FROM REPORTS.V_EMPLOYEE WHERE STATUS = 'A' )) AND
*!*	     (A.TERMINATIONDATE >= <<lcADPTermCheckDate>>)
*!*	   )
*!*	 )


					IF USED('CURFSAPRETERMS') THEN
						USE IN CURFSAPRETERMS
					ENDIF
					IF USED('CURFSAPRE') THEN
						USE IN CURFSAPRE
					ENDIF
					IF USED('CURFSA') THEN
						USE IN CURFSA
					ENDIF

					IF .lTestMode THEN
						.TrackProgress('lcSQL = ' + lcSQL, LOGIT+SENDIT)
						.TrackProgress('lcSQL2 = ' + lcSQL2, LOGIT+SENDIT)
					ENDIF

					IF .ExecSQL(lcSQL, 'CURFSAPRE', RETURN_DATA_MANDATORY) AND ;
						.ExecSQL(lcSQL2, 'CURFSAPRETERMS', RETURN_DATA_NOT_MANDATORY) THEN

						*!*								.ExecSQL(lcSQL2, 'CURHOURSPRE', RETURN_DATA_MANDATORY) AND ;
						
*!*							SELECT CURFSAPRE
*!*							BROWSE
*!*							SELECT CURFSAPRETERMS
*!*							BROWSE
*!*							
*!*							THROW
						
						IF USED('CURFSAPRE') AND NOT EOF('CURFSAPRE') THEN
						
						IF USED('CURFSAPRETERMS') AND NOT EOF('CURFSAPRETERMS') THEN
							* add these to CURFSAPRE - ONLY IF NOT ALREADY IN THERE!
							
							IF USED('CURFSAPRETERMS2') THEN
								USE IN CURFSAPRETERMS2
							ENDIF
							
							SELECT * ;
								FROM CURFSAPRETERMS ;
								INTO CURSOR CURFSAPRETERMS2 ;
								WHERE NAME NOT IN ( SELECT NAME FROM CURFSAPRE) ;
								ORDER BY NAME
							
							IF USED('CURFSAPRETERMS2') AND NOT EOF('CURFSAPRETERMS2') THEN
								SELECT CURFSAPRETERMS2
								SCAN
									SCATTER MEMVAR
									INSERT INTO CURFSAPRE FROM MEMVAR
								ENDSCAN
								USE IN CURFSAPRETERMS2
							ENDIF && USED('CURFSAPRETERMS2')

							USE IN CURFSAPRETERMS

						ENDIF && USED('CURFSAPRETERMS')

*!*	SELECT CURFSAPRE
*!*	BROWSE
*!*	THROW

*SET STEP ON 
						
						SELECT CURFSAPRE
						SCAN
						
							*REPLACE CURFSAPRE.MAXAMOUNT WITH IIF(CURFSAPRE.ADP_COMP='E87',52*CURFSAPRE.FSAAMT,26*CURFSAPRE.FSAAMT)
							lnMaxAmount = VAL(CURFSAPRE.CUSTAREA2)
							IF lnMaxAmount > 0 THEN
								REPLACE CURFSAPRE.MAXAMOUNT WITH lnMaxAmount	
							ELSE
							
							ENDIF
							* POPULATE DATE FIELDS
							
							ldEligDate = .Get4MonthDate( TTOD( CURFSAPRE.HIREDATE ) )
							IF ldEligDate < {^2012-07-01} THEN
								ldEligDate = {^2012-07-01}
							ENDIF
							
							
							DO CASE
								CASE BETWEEN(ldFromDate,{^2012-07-01},{^2013-06-30})
									ldFYBeginDate = {^2012-07-01}
								CASE BETWEEN(ldFromDate,{^2013-07-01},{^2014-06-30})
									ldFYBeginDate = {^2013-07-01}
								CASE BETWEEN(ldFromDate,{^2014-07-01},{^2015-06-30})
									ldFYBeginDate = {^2014-07-01}
								CASE BETWEEN(ldFromDate,{^2015-07-01},{^2016-06-30})
									ldFYBeginDate = {^2015-07-01}
								CASE BETWEEN(ldFromDate,{^2016-07-01},{^2017-06-30})
									ldFYBeginDate = {^2016-07-01}
								OTHERWISE
									THROW							
							ENDCASE
							
							* per Lauren, relace all 3 dates with eligibility date if it's later than what is already in OPDATE
							IF ldEligDate > ldFYBeginDate then
								REPLACE CURFSAPRE.OPDATE WITH ldEligDate, CURFSAPRE.STATDATE WITH ldEligDate, CURFSAPRE.MAXDATE WITH ldEligDate IN CURFSAPRE
							ELSE
								REPLACE CURFSAPRE.OPDATE WITH ldFYBeginDate, CURFSAPRE.STATDATE WITH ldFYBeginDate, CURFSAPRE.MAXDATE WITH ldFYBeginDate IN CURFSAPRE
							ENDIF

							* for Terminated, maybe change Stat Date
							IF (CURFSAPRE.STATUS = 'T') AND (NOT ISNULL(CURFSAPRE.TERMINATIONDATE)) AND (CURFSAPRE.TERMINATIONDATE > CURFSAPRE.STATDATE) THEN
								REPLACE CURFSAPRE.STATDATE WITH CURFSAPRE.TERMINATIONDATE IN CURFSAPRE
							ENDIF
							
							*!*	IF (CURFSAPRE.STATUS = 'T') AND (CURFSAPRE.TERMINATIONDATE > CURFSAPRE.STATDATE) THEN
							*!*		* TERM STATUS
							*!*		REPLACE CURFSAPRE.STATDATE WITH CURFSAPRE.TERMINATIONDATE IN CURFSAPRE
							*!*	ELSE
							*!*		* ACTIVE STATUS
							*!*		IF (CURFSAPRE.HIREDATE > CURFSAPRE.STATDATE) THEN
							*!*			REPLACE CURFSAPRE.STATDATE WITH CURFSAPRE.HIREDATE IN CURFSAPRE
							*!*		ENDIF
							*!*	ENDIF
							
							* CHECK WITH LAUREN FOR BETTER STATDATE LOGIC FOR ACTIVE NEW HIRES ABOVE!!!!							
							
							* remove middle initial if there is one and place in its own field
							lcFirstName = ALLTRIM(CURFSAPRE.FIRSTNAME)
							lnSpacePos = AT(' ',lcFirstName)
							IF lnSpacePos > 0 THEN
								* replace First name with everything to the left of the space
								lcNewFirstName = LEFT(lcFirstName, lnSpacePos - 1)
								REPLACE CURFSAPRE.FIRSTNAME WITH lcNewFirstName IN CURFSAPRE
								
								* replace MI with 1st char of everything to the right of the space
								lcMI = UPPER(LEFT(ALLTRIM(SUBSTR(lcFirstName, lnSpacePos)),1))
								IF NOT EMPTY(lcMI) THEN
									REPLACE CURFSAPRE.MI WITH lcMI IN CURFSAPRE
								ENDIF
							ENDIF	
							
						ENDSCAN
						

*!*							SELECT CURFSAPRE
*!*							BROWSE
						
						SELECT * FROM CURFSAPRE INTO CURSOR CURFSA ORDER BY LASTNAME READWRITE
						
						SELECT CURFSA
						SUM EECONTRIB TO lnTotalFSA

						oExcel = CREATEOBJECT("excel.application")
						oExcel.displayalerts = .F.
						oExcel.VISIBLE = .F.
						oWorkbook = oExcel.workbooks.OPEN(lcSpreadsheetTemplate)
						oWorkbook.SAVEAS(lcFiletoSaveAs)
						oWorksheet = oWorkbook.Worksheets[1]

						oWorksheet.RANGE("B2").VALUE = ldToDate
						oWorksheet.RANGE("D2").VALUE = lnTotalFSA

						lnRow = 3

						SELECT CURFSA
						SCAN
							lnRow = lnRow + 1
							lcRow = ALLTRIM(STR(lnRow))
							oWorksheet.RANGE("A"+lcRow).VALUE = CURFSA.CONTROL
							oWorksheet.RANGE("B"+lcRow).VALUE = CURFSA.SUFFIX
							oWorksheet.RANGE("C"+lcRow).VALUE = CURFSA.ACCOUNT
							oWorksheet.RANGE("D"+lcRow).VALUE = CURFSA.SSN
							oWorksheet.RANGE("E"+lcRow).VALUE = CURFSA.LASTNAME
							oWorksheet.RANGE("F"+lcRow).VALUE = CURFSA.FIRSTNAME
							oWorksheet.RANGE("G"+lcRow).VALUE = CURFSA.MI
							oWorksheet.RANGE("H"+lcRow).VALUE = CURFSA.STREET1
							oWorksheet.RANGE("I"+lcRow).VALUE = CURFSA.STREET2
							oWorksheet.RANGE("J"+lcRow).VALUE = CURFSA.CITY
							oWorksheet.RANGE("K"+lcRow).VALUE = CURFSA.STATE
							oWorksheet.RANGE("L"+lcRow).VALUE = CURFSA.ZIPCODE
							oWorksheet.RANGE("M"+lcRow).VALUE = CURFSA.ZIPEXT
							oWorksheet.RANGE("N"+lcRow).VALUE = CURFSA.GENDER
							oWorksheet.RANGE("O"+lcRow).VALUE = TTOD(CURFSA.BIRTHDATE)
							oWorksheet.RANGE("P"+lcRow).VALUE = TTOD(CURFSA.OPDATE)
							oWorksheet.RANGE("Q"+lcRow).VALUE = CURFSA.STATUS
							oWorksheet.RANGE("R"+lcRow).VALUE = TTOD(CURFSA.STATDATE)
							oWorksheet.RANGE("S"+lcRow).VALUE = CURFSA.MAXAMOUNT
							oWorksheet.RANGE("T"+lcRow).VALUE = TTOD(CURFSA.MAXDATE)
							oWorksheet.RANGE("U"+lcRow).VALUE = CURFSA.PRODCODE
							oWorksheet.RANGE("V"+lcRow).VALUE = CURFSA.ERCONTRIB
							oWorksheet.RANGE("W"+lcRow).VALUE = CURFSA.EECONTRIB
							oWorksheet.RANGE("AD"+lcRow).VALUE = IIF(EMPTY(CURFSA.HEALTHCODE)," ","POS")
							oWorksheet.RANGE("AG"+lcRow).VALUE = IIF(EMPTY(CURFSA.VISIONCODE)," ","VISION")
						ENDSCAN
*!*	837131 AS CONTROL,
*!*	10 AS SUFFIX,
*!*	0 AS ACCOUNT,
*!*	SSN,
*!*	A.LASTNAME,
*!*	A.FIRSTNAME,
*!*	' ' AS MI,
*!*	STREET1,
*!*	' ' AS STREET2,
*!*	A.CITY,
*!*	A.STATE,
*!*	A.ZIPCODE,
*!*	' ' AS ZIPEXT,
*!*	A.GENDER,
*!*	A.BIRTHDATE,
*!*	DATE'2012-07-01' AS OPDATE,
*!*	A.STATUS,
*!*	DATE'2012-07-01' AS STATDATE,
*!*	0 AS MAXAMOUNT,
*!*	DATE'2012-07-01' AS MAXDATE,
*!*	1 AS PRODCODE,
*!*	' ' AS ERCONTRIB,
*!*	EECONTRIB,
						IF TYPE('oWorkbook') = "O" THEN
							oWorkbook.SAVE()
							oWorkbook = NULL
						ENDIF
						IF TYPE('oExcel') = "O" THEN
							oExcel.QUIT()
							oExcel = NULL
						ENDIF


						IF FILE(lcFiletoSaveAs) THEN
							.cAttach = lcFiletoSaveAs
							.cBodyText = "See attached report." + CRLF + CRLF + "<<REPORT LOG FOLLOWS>>" + CRLF + .cBodyText
						ELSE
							.TrackProgress("ERROR attaching " + lcFiletoSaveAs, LOGIT+SENDIT+NOWAITIT)
						ENDIF

						ELSE
							.TrackProgress('No paydata was found for the date range!', LOGIT+SENDIT)
						ENDIF  &&  USED('CURFSAPRE') AND NOT EOF('CURFSAPRE')


						=SQLDISCONNECT(.nSQLHandle)

					ENDIF  &&  .ExecSQL(lcSQL, 'SQLCURSOR1', RETURN_DATA_MANDATORY)
				ELSE
					* connection error
					.TrackProgress('Unable to connect to ADP Payroll System.', LOGIT+SENDIT)
				ENDIF   &&  .nSQLHandle > 0

				.TrackProgress("FSA DEDS REPORT process ended normally.", LOGIT+SENDIT+NOWAITIT)


			CATCH TO loError

				.TrackProgress('There was an error.',LOGIT+SENDIT)
				.TrackProgress('The Error # is: ' + TRANSFORM(loError.ERRORNO), LOGIT+SENDIT)
				.TrackProgress('The Error Message is: ' + TRANSFORM(loError.MESSAGE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred in Program: ' + TRANSFORM(loError.PROCEDURE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred at line #: ' + TRANSFORM(loError.LINENO), LOGIT+SENDIT)
				lnNumberOfErrors = lnNumberOfErrors + 1

				IF TYPE('oWorkbook') = "O" AND NOT ISNULL(oWorkbook) THEN
					oWorkbook.SAVE()
				ENDIF
				IF TYPE('oExcel') = "O" AND NOT ISNULL(oExcel) THEN
					oExcel.QUIT()
				ENDIF
				
				CLOSE DATABASES ALL

			ENDTRY

			WAIT CLEAR
			***************** INTERNAL email results ******************************

*!*				IF (lnNumberOfErrors = 0) THEN
*!*					* SAVE AND QUIT EXCEL
*!*					IF TYPE('oWorkbook') = "O" THEN
*!*						oWorkbook.SAVE()
*!*						oWorkbook = NULL
*!*					ENDIF
*!*					IF TYPE('oExcel') = "O" THEN
*!*						oExcel.QUIT()
*!*						oExcel = NULL
*!*					ENDIF
*!*				ENDIF

			.TrackProgress('About to send status email.',LOGIT)
			.TrackProgress('The logfile is: ' + .cLogFile, SENDIT)
			.TrackProgress("FSA DEDS REPORT process started: " + .cStartTime, LOGIT+SENDIT)
			.TrackProgress("FSA DEDS REPORT process finished: " + TTOC(DATETIME()), LOGIT+SENDIT)

			IF .lSendInternalEmailIsOn THEN
				DO FORM dartmail2 WITH .cSendTo,.cFrom,.cSubject,.cCC,.cAttach,.cBodyText,"A"
				.TrackProgress('Sent status email.',LOGIT)
			ELSE
				.TrackProgress('Did not send status email: THIS.lSendInternalEmailIsOn = FALSE.',LOGIT)
			ENDIF 

		ENDWITH
		RETURN
	ENDFUNC && main


	FUNCTION ExecSQL
		LPARAMETERS tcSQL, tcCursorName, tlNoDataReturnedIsOkay
		LOCAL llRetval, lnResult
		WITH THIS
			* close target cursor if it's open
			IF USED(tcCursorName)
				USE IN (tcCursorName)
			ENDIF
			WAIT WINDOW "Fetching remote data; please wait..." NOWAIT
			lnResult = SQLEXEC(.nSQLHandle, tcSQL, tcCursorName)
			llRetval = ( lnResult > 0 )
			IF llRetval THEN
				* see if any data came back
				IF NOT tlNoDataReturnedIsOkay THEN
					IF (NOT USED(tcCursorName)) OR EOF(tcCursorName) THEN
						llRetval = .F.
						.TrackProgress('No data was returned by this query: ' +CRLF + tcSQL, LOGIT+SENDIT)
						.cSendTo = 'Mark Bennett <mbennett@fmiint.com>'
					ENDIF
				ENDIF
			ELSE
				.TrackProgress("Result Code [" + TRANSFORM(lnResult) + "] returned by this query: " + tcSQL, LOGIT+SENDIT)
				.cSendTo = 'Mark Bennett <mbennett@fmiint.com>'
			ENDIF
			WAIT CLEAR
			RETURN llRetval
		ENDWITH
	ENDFUNC


	PROCEDURE TrackProgress
		* do any combination of Wait Window, writing to logfile, and adding to body of email,
		* based on nFlags parameter.
		LPARAMETERS tcExpression, tnFlags
		WITH THIS
			IF BITAND(tnFlags,LOGIT) = LOGIT THEN
				IF .lLoggingIsOn THEN
					?
					? .cProcessName + "  " + TTOC(DATETIME()) + ": " + tcExpression
				ENDIF
			ENDIF
			IF .lWaitWindowIsOn THEN
				IF BITAND(tnFlags,NOWAITIT) = NOWAITIT THEN
					WAIT WINDOW tcExpression NOWAIT
				ENDIF
				IF BITAND(tnFlags,WAITIT) = WAITIT THEN
					WAIT WINDOW tcExpression TIMEOUT .nWaitWindowTimeout
				ENDIF
			ENDIF
			IF BITAND(tnFlags,SENDIT) = SENDIT THEN
				IF .lSendInternalEmailIsOn THEN
					.cBodyText = .cBodyText + tcExpression + CRLF + CRLF
				ENDIF
			ENDIF
		ENDWITH
	ENDPROC  &&  TrackProgress


	FUNCTION GetSQLStartDateTimeString
		* returns a timestamp string for use in SQL queries
		LPARAMETERS tdDate
		LOCAL lcSetDate, lcSQLDateTimeString
		lcSetDate = SET('DATE')
		SET DATE YMD
		lcSQLDateTimeString   = "{ts '" + TRANSFORM(YEAR(tdDate)) + "-" + PADL(MONTH(tdDate),2,"0") + "-" + PADL(DAY(tdDate),2,"0") + " 00:00:00.0'}"
		SET DATE &lcSetDate.
		RETURN lcSQLDateTimeString
	ENDFUNC


	FUNCTION GetSQLEndDateTimeString
		* returns a timestamp string for use in SQL queries
		LPARAMETERS tdDate
		LOCAL lcSetDate, lcSQLDateTimeString
		lcSetDate = SET('DATE')
		SET DATE YMD
		lcSQLDateTimeString = "{ts '" + TRANSFORM(YEAR(tdDate)) + "-" + PADL(MONTH(tdDate),2,"0") + "-" + PADL(DAY(tdDate),2,"0") + " 23:59:59.9'}"
		SET DATE &lcSetDate.
		RETURN lcSQLDateTimeString
	ENDFUNC
	
	
	FUNCTION Get4MonthDate
		* returns FSA eligibility date based on passed hiredate
		LPARAMETERS tcHireDate		
		LOCAL ldEligDate
				
		*tcHireDate = {^2011-03-30}	
		
		* new date is the first of the 4th month they have worked
			
		*!*	IF DAY(tcHireDate) = 1 THEN
		*!*		* just add 4 months to hire date
		*!*		ldEligDate = GOMONTH(tcHireDate,4)
		*!*	ELSE
		*!*		* go to the first of the next month
		*!*		ldEligDate = GOMONTH(tcHireDate,1)
		*!*		ldEligDate = ldEligDate - DAY(ldEligDate) + 1
		*!*		* then add 4 months
		*!*		ldEligDate = GOMONTH(ldEligDate,4)
		*!*	ENDIF	
		
		* go to 1st of month hired and add 4 months...
		ldEligDate = tcHireDate - DAY(tcHireDate) + 1
		* then add 4 months
		ldEligDate = GOMONTH(ldEligDate,4)
		
		*WAIT WINDOW TRANSFORM(ldEligDate)
		
		RETURN ldEligDate		
	ENDFUNC


ENDDEFINE
