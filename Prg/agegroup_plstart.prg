PARAMETERS cOfficeIn

runack("AGEGROUP_PLBREAKOUT")

CLOSE DATABASES ALL
IF VARTYPE(cOfficeIn)='L'
	cOfficeIn = "N"
ENDIF
PUBLIC cOffice,nAcctNum,cState,cMailname,cFTPOffice,lDoInbound,lNormalExit

TRY
	DO m:\dev\prg\_setvars
	lNormalExit = .F.
	cOfficeIn = UPPER(cOfficeIn)
	_SCREEN.WINDOWSTATE=1
	STORE cOfficeIn TO cOffice
	nAcctNum = 1285
	cMailname = "AgeGroup(PL)"
	lDoInbound = .T.

	cFTPOffice = IIF(cOffice="C","CA","NJ")
	WAIT WINDOW "Now breaking out "+cFTPOffice+" Excel Packing List document" TIMEOUT 1

	SELECT 0
	USE F:\edirouting\FTPSETUP SHARED
	REPLACE chkbusy WITH .T.,trig_time WITH DATETIME()  FOR UPPER(FTPSETUP.TRANSFER) = UPPER("AgeGroup "+cFTPOffice+" PL")
	USE IN FTPSETUP

	DO m:\dev\prg\agegroup_plbreakout WITH cOffice

	SELECT 0
	USE F:\edirouting\FTPSETUP SHARED
	REPLACE CHKBUSY WITH .F. FOR UPPER(FTPSETUP.TRANSFER) = UPPER("AgeGroup "+cFTPOffice+" PL")
	USE IN FTPSETUP

	ASSERT .F. MESSAGE "At DoInbound...stop here to prevent WMSInbound"
	IF lDoInbound
		WAIT WINDOW "Now running WMS Inbound process" TIMEOUT 1
		wmsinboundexe="F:\3PL\PROJ\wmsinbound.EXE"
		DO (wmsinboundexe) WITH cOffice,nAcctNum
	ENDIF

CATCH TO oErr
	IF lNormalExit = .F.
		ASSERT .F. MESSAGE "In Error Catch"
		tsubject = cMailname+" PL Process Error ("+TRANSFORM(oErr.ERRORNO)+") at "+TTOC(DATETIME())
		tattach  = ""
		SELECT 0
		USE f:\3pl\data\mailmaster ALIAS mm shared
		LOCATE FOR edi_type = "MISC" AND taskname = "GENERAL"
		tsendto = IIF(mm.use_alt,mm.sendtoalt,mm.sendto)
		tcc = IIF(mm.use_alt,mm.ccalt,mm.cc)
		USE IN mm
		tmessage = cMailname+" PL Process Error.....Please fix me........!"
		tmessage = tmessage+CHR(13)+cMessage
		lcSourceMachine = SYS(0)
		lcSourceProgram = SYS(16)

		tmessage =tmessage+ "Try/CATCH Exeception Message:"+CHR(13)+;
			[  Error: ] + STR(oErr.ERRORNO) +CHR(13)+;
			[  LineNo: ] + STR(oErr.LINENO) +CHR(13)+;
			[  Message: ] + oErr.MESSAGE +CHR(13)+;
			[  Procedure: ] + oErr.PROCEDURE +CHR(13)+;
			[  Details: ] + oErr.DETAILS +CHR(13)+;
			[  LineContents: ] + oErr.LINECONTENTS+CHR(13)+;
			[  UserValue: ] + oErr.USERVALUE+CHR(13)+;
			[  Computer:  ] +lcSourceMachine+CHR(13)+;
			[  PL file:  ] +xFile+CHR(13)+;
			[  Program:   ] +lcSourceProgram

		tattach  = ""
		tfrom    ="TGF EDI Processing Center <toll-edi-ops@tollgroup.com>"
		DO FORM m:\dev\FRM\dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
	ELSE
		WAIT WINDOW AT 10,10 "Normal Exit " TIMEOUT 2
	ENDIF
FINALLY
	WAIT WINDOW "All AgeGroup processes complete...exiting" TIMEOUT 1
	CLOSE DATABASES ALL
ENDTRY

