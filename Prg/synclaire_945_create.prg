*!* Synclaire 945 (Whse. Shipping Advice) Creation Program
*!* Creation Date: 06.18.2014 by Joe, modded by Paul (from BCNY 945 prog)

PARAMETERS cBOL,cOffice,nAcctNum,cShip_ref,lParcelLong
** try to define all variables that may be included in the "Catch Email in case of an error
** that is not the result of a deliberate Throw.......

RELEASE m.groupnum,m.transnum,m.isanum,m.edicode,m.ship_ref,m.processed,m.accountid,m.loaddt,m.loadtime,m.filename,m.fafilename
PUBLIC ARRAY thisarray(1)
PUBLIC cMBOL,nWO_num,cWO_Num,cWO_NumStr,cWO_NumList,cWO_NumOld,cCustname,nUnitSum,cFileNameShort,lDoSQLConnect,cErrMsg,lTesting,cCuFt,lTestSplits,cTrailerNumber,cUPC,cMod,lDoCartons
PUBLIC lBCNYFilesout,lTestmail,lParcelType,lTestinput,lDoCompare,cTimeMsg,cBOLTest,cISA_Num,tccerr,lBrowfiles,lcPath,lcArchivePath,lcHoldPath,cShip_SCAC,cTerms,lTPAmazon,cEDIType,lScanpack,lUseSQLBL
PUBLIC m.groupnum,m.transnum,m.isanum,m.edicode,m.ship_ref,m.processed,m.accountid,m.loaddt,m.loadtime,m.filename,m.fafilename,cFilenameOut,lDoSQLRun,lOverflow,lDoOV  && For 997 ack, per Todd, 08.08.2014


*SET STEP ON 

lTesting = .F.
lTestinput =  .F.
lTestSplits = .F.

lDoSQLConnect = .T.
lDoCompare = !lTesting
lDoCompare = IIF(lTestSplits,.F.,lDoCompare) && If true, sends email (will not send if running tests of split PTs)
*lDoCompare = .f.
lBrowfiles = lTesting
lPGTesting = .F.
lDoSQLRun = .T.
tccerr=""
cErrMsg ="TOP LEVEL"
cTimeMsg = IIF(lTesting,"TIMEOUT 2","TIMEOUT 0.01")

cCustname ="SYNCLAIRE"
lDoCatch = .T.
lEmail = .T.
lTestmail = lTesting && Sends mail to Joe only
*lTestmail = .T. && Sends mail to Joe only
lTestmail = IIF(lTestSplits,.T.,lTestmail) && If true, sends test email to Joe only (will send if running tests of split PTs)
lBCNYFilesout = .T. && If true, copies output to FTP folder (default = .t.)
lBCNYFilesout = IIF(lTestSplits,.F.,lBCNYFilesout) && If true, copies output to FTP folder (will not move files if running tests of split PTs)
cMBOL = ""
nOSQty = 0
cEDIType = "945"
lDoOV=.f.

*Set Coverage To h:\fox\syndata.txt

StartTime = DATETIME()

STORE "" TO lcPath,lcHoldPath,lcArchivePath,tsendtotest,tsendtoerr,tcctest

DO m:\dev\prg\_setvars WITH lTesting
IF VARTYPE(nAcctNum) # "N"
	nAcctNum = 6521
ENDIF
m.accountid = nAcctNum
m.edicode = "SW"
m.fafilename = ""
cUPC = ""
cTerms = ""
IF VARTYPE(cOffice) # "C"
	cOffice = "C"
ENDIF
cMod = IIF(cOffice = "C","2",cOffice)
goffice = cMod
gMasterOffice = cOffice

lUseSQLBL = .T.

TRY
	STORE 0 TO  nFilenum,nSCCSuffix,nOrigSeq,nOrigGrpSeq
	STORE "" TO cWO_NumStr,cWO_Num,cWO_NumList
	IF !lParcelLong
		STORE "" TO cShip_ref
	ENDIF
	STORE "XXX" TO cWO_NumOld

	lIsError = .F.
	lDoCatch = .T.
	lCloseOutput = .T.

	cPPName = "SYNCLAIRE"
	lISAFlag = .T.
	lSTFlag = .T.
	lJCPenney = .F.
	lCloseOutput = .T.
	lParcelType = .F.
	tfrom = "Toll EDI Operations <toll-edi-ops@tollgroup.com>"

	IF lTesting OR VARTYPE(cBOL) # "C"
		WAIT WIND "Running as a TEST" TIMEOUT 2
		CLOSE DATABASES ALL
		DO m:\dev\prg\lookups
		cBOL = '04917526521003850'
		cShip_ref = '9524388-A'
		cBOLTest = cBOL
	ENDIF

	IF EMPTY(cBOL) AND lTesting
		WAIT WINDOW "Empty BOL#" TIMEOUT 2
	ENDIF
	cBOL = ALLTRIM(cBOL)
	lOverflow = IIF(cBOL = '999',.T.,.F.)
	lDoCartons = IIF(DATETIME()<DATETIME(2018,01,23,21,55,00),.F.,.T.)

*!* SET CUSTOMER CONSTANTS
	cCustname = "SYNCLAIRE"  && Customer Identifier
	cX12 = "004010"  && X12 Standards Set used
	cMailName = "Synclaire"

	SELECT 0
	SELECT * FROM "f:\3pl\data\ackdatashell" WHERE .F. INTO CURSOR tempack READWRITE

	SELECT 0
	CREATE CURSOR temp945 (accountid i,isa_num c(10),st_num c(10),wo_num i,bol_no c(20),;
		ship_ref c(20),filename c(50),filedate T)

*!*	lBCNYFilesout = .F.
*!*	lTestMail = .T.

	STORE "L" TO cWeightUnit
	lPrepack = .F.
	lPick = !lPrepack
	cUsefolder = "F:\wh2\whdata\"
	IF lTestinput
		cUsefolder = "F:\WHP\WHDATA\"
	ENDIF

	DO m:\dev\prg\swc_cutctns WITH cBOL

	WAIT WINDOW "at the outship query..............." NOWAIT

	IF USED("outship")
		USE IN outship
	ENDIF

	IF lParcelLong
		csq1 = [select * from outship where accountid = ]+TRANSFORM(nAcctNum)+[ and office = ']+cOffice+[' and ship_ref = ']+cShip_ref+[']
	ELSE
		csq1 = [select * from outship where accountid = ]+TRANSFORM(nAcctNum)+[ and office = ']+cOffice+[' and bol_no = ']+cBOL+[']
	ENDIF
	
	xsqlexec(csq1,,,"wh")
	SELECT outship
	IF RECCOUNT() = 0
		cErrMsg = "MISS BOL "+cBOL
		DO ediupdate WITH cErrMsg,.T.
		THROW
	ENDIF

	COUNT TO n1
	COUNT TO n2 FOR "OV"$outship.ship_ref
	IF n1 = n2
		REPLACE processed WITH .T.,proc945 WITH .T.,file945 WITH "NO 945 FILE - OVERFLOW";
			fin_status WITH "NO 945 PRODUCED - OV", errorflag WITH .F.,when_proc WITH DATETIME() ;
			FOR EDI_TRIGGER.bol = cBOL AND EDI_TRIGGER.ship_ref = cShip_ref AND INLIST(accountid,6520,6521,6522)
		lDoCatch = .F.
		THROW
	ENDIF

	LOCATE
	INDEX ON bol_no TAG bol_no
	INDEX ON wo_num TAG wo_num
	INDEX ON ship_ref TAG ship_ref

	lScanpack = .F.

	SELECT outship

	COUNT TO nCntBOL FOR outship.bol_no = cBOL
	LOCATE
	nSMU = 0


	cScanParam = IIF(lParcelLong,"ship_ref = cShip_ref","bol_no = cBOL")
	SCAN FOR &cScanParam
		lScanpack = IIF("PROCESSMODE*SCANPACK"$outship.shipins,.T.,.F.)
		IF "SMU"$outship.ship_ref
			nSMU = nSMU+1
		ENDIF
	ENDSCAN

*!* Added this code to trap unmatched OUTDETIDs in OUTDET, Joe 11.22.2005
	IF lParcelLong
		=SEEK(cShip_ref,'outship','ship_ref')
	ELSE
		=SEEK(cBOL,'outship','bol_no')
	ENDIF
	nWO_num = outship.wo_num

*	lDoCompare = IIF(lScanpack,.F.,lDoCompare)
	lDoCompare = .F.

	IF nCntBOL = nSMU && All-SMU shipment...no 945 goes out, added 01.28.2015
*		cErrMsg = "SMU Orders"
*		DO ediupdate WITH cErrMsg,.F.
*		THROW
	ENDIF

	IF USED('OUTDET')
		USE IN outdet
	ENDIF

	selectoutdet()
	SELECT outdet
	INDEX ON outdetid TAG outdetid

	IF USED('SHIPMENT')
		USE IN shipment
	ENDIF

	IF USED('PACKAGE')
		USE IN package
	ENDIF

	xjfilter="shipmentid in ("
	SELECT outship
	SCAN
		xsqlexec("select * from shipment where pickticket = '"+outship.ship_ref+"'",,,"wh")
		IF RECCOUNT("shipment") > 0
			SELECT shipment
			SCAN
				xjfilter=xjfilter+TRANSFORM(shipmentid)+","
			ENDSCAN
		ENDIF
	ENDSCAN

	SELECT shipment
	INDEX ON shipmentid TAG shipmentid
	INDEX ON pickticket TAG pickticket
	LOCATE

	IF RECCOUNT("shipment") > 0
		xjfilter=LEFT(xjfilter,LEN(xjfilter)-1)+")"
		xsqlexec("select * from package where "+xjfilter,,,"wh")
	ELSE
		xsqlexec("select * from package where .f.",,,"wh")
	ENDIF

*!*  	dDate = DATE()-3
	SELECT package
	INDEX ON shipmentid TAG shipmentid
	SET ORDER TO TAG shipmentid


***************************************************************************************************************************************
** will add in the precursor verification at the pick ticket level,
** to eliminate the "by work order" requirement
** If an outdet error is detected a record is placed into the ScanPoller
** The Scapoller will then perform the WMS update and the next time the EDI is triggered they should be good.
***************************************************************************************************************************************
	lldo_new_check = .F.

	IF lldo_new_check
		cRetMsg = ""

		DO m:\dev\prg\get_sql_ptdata  WITH 6521,cBOL
		IF cRetMsg<>"OK"
			cErrMsg = cRetMsg
			DO ediupdate WITH cErrMsg,.T.
			THROW
		ENDIF

		SELECT results
		LOCATE

************************************************************************
* here lets do a pre-check on all the PTs fo a QTY check
************************************************************************
		llAllQtysOK = .T.
		cErrMsg ="Pick Tickets with Sql.totqty vs Outdet.totqty errors on shipment: "+cBOL+CHR(13)+"-------------------------------------------------------------------"+CHR(13)
		IF lParcelLong
			SELECT ship_ref,wo_num,outshipid,shipins FROM outship WHERE outship.ship_ref = cShip_ref INTO CURSOR outshipids
		ELSE
			SELECT ship_ref,wo_num,outshipid,shipins FROM outship WHERE outship.bol_no= cBOL INTO CURSOR outshipids
		ENDIF
		SELECT outshipids
		llPrepackErrorFlag = .F.
		SCAN
			llPType = ICASE("PICKPACK"$outshipids.shipins,"PICKPACK","SCANPACK"$outshipids.shipins,"SCANPACK","PREPACK"$outshipids.shipins,"PREPACK","UNK")

			SELECT outdet
			SET ORDER TO

			IF INLIST(llPType,"PICKPACK","SCANPACK")
				SUM totqty TO nUnitTot1 FOR  units AND outdet.outshipid = outshipids.outshipid
			ELSE
				SUM totqty TO nUnitTot1 FOR !units AND outdet.outshipid = outshipids.outshipid
			ENDIF

			SELECT results
			SUM Oqty TO nUnitTot2 FOR results.outshipid = outshipids.outshipid
			IF nUnitTot1<>nUnitTot2
				SET STEP ON
				IF llPType = "PREPACK"
					llPrepackErrorFlag = .T.
				ENDIF
				SELECT results
				LOCATE FOR results.outshipid = outshipids.outshipid
				cErrMsg = cErrMsg+"Pick Ticket: "+ALLTRIM(outshipids.ship_ref)+" -("+llPType+")  Outdet totqty = "+PADR(ALLTRIM(TRANSFORM(nUnitTot1)),6," ")+"  SQL Cartons TotQty:"+PADR(ALLTRIM(TRANSFORM(nUnitTot2)),6," ")+CHR(13)
				llAllQtysOK = .F.
				IF llPType = "PICKPACK"  && add in a WMS update trigger, but only for PICPACK type pick tickets
* removed dy 7/21/16 no longer done
*					IF !USED("wmsupdttgr")
*						USE F:\3pl\DATA\wmsupdttgr IN 0
*					ENDIF
*					SELECT wmsupdttgr
*					LOCATE FOR !processed AND ship_ref=outshipids.ship_ref
*					IF !FOUND()
*						INSERT INTO wmsupdttgr (accountid,ship_ref,wo_num,trig_time,trig_date,MOD,office) VALUES (6521,outshipids.ship_ref,outshipids.wo_num,DATETIME(),DATE(),"2","C")
*					ENDIF
				ENDIF
			ENDIF
		ENDSCAN

		IF llAllQtysOK = .F.
			tsendto ="pgaidis@fmiint.com,joe.bianchi@tollgroup.com"
			tcc="Maria.Estrella@Tollgroup.com"
			tsubject ="Synclaire Outdet/SQL Qty Errors on BOL/Trk#: "+cBOL+" at "+TTOC(DATETIME())
			tattach=""
			IF llPrepackErrorFlag = .T.
				cErrMsg = cErrMsg+CHR(13)+CHR(13)+" --- Prepack Qty Error, check for scanned UCC labels  -- "
			ENDIF
			tmessage = cErrMsg
			DO FORM m:\dev\frm\dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"

			cErrMsg= "SQL/OTDET QTY ERR"
			DO ediupdate WITH cErrMsg,.T.
			THROW
		ENDIF

	ENDIF
***************************************************************************************************************************************
** end of new precursor check
***************************************************************************************************************************************
	WAIT WINDOW "at the sql data compare............." NOWAIT

	IF !lDoCompare
		WAIT WINDOW "Skipping Labels/Cartons Comparison..." &cTimeMsg
	ELSE
		WAIT WINDOW "Performing Labels/Cartons Comparison..." &cTimeMsg
		cRetMsg = "X"
		DO m:\dev\prg\sqldata-COMPARE_bcny WITH cUsefolder,cBOL,nWO_num,cOffice,nAcctNum,cPPName
		IF cRetMsg<>"OK"
			lCloseOutput = .F.
			cErrMsg = "SQL ERROR "+cRetMsg
			DO ediupdate WITH cErrMsg,.T.
			THROW
		ENDIF
	ENDIF

	PUBLIC c_CntrlNum,c_GrpCntrlNum,cReturned
	WAIT WINDOW "Now preparing tables and variables" NOWAIT NOCLEAR

	IF USED('mm')
		USE IN mm
	ENDIF
	USE F:\3pl\DATA\mailmaster ALIAS mm IN 0
	SELECT mm
	LOCATE FOR edi_type = '945' AND mm.office = cOffice AND mm.accountid = 6521 AND taskname # "SYNCLAIRE-EDI-MBOL"
	tsendto = IIF(mm.use_alt,sendtoalt,mm.sendto)
	tcc = IIF(mm.use_alt,mm.ccalt,mm.cc)
	lcPath = ALLTRIM(mm.basepath)
	lcHoldPath = ALLTRIM(mm.holdpath)
	lcArchivePath = ALLTRIM(mm.archpath)
	LOCATE
	LOCATE FOR mm.edi_type = 'MISC' AND mm.taskname = 'GENERAL'
	tsendtotest = IIF(mm.use_alt,sendtoalt,mm.sendto)
	tcctest = IIF(mm.use_alt,mm.ccalt,mm.cc)
	tsendtoerr = IIF(mm.use_alt,sendtoalt,mm.sendto)
	tccerr = IIF(mm.use_alt,mm.ccalt,mm.cc)
	LOCATE
	LOCATE FOR mm.edi_type = 'MISC' AND mm.taskname = "940DUPES"  && Changed from "TODDMAIL" after changes were apparently made.
	tsendtotodd = IIF(mm.use_alt,sendtoalt,mm.sendto)
	tcctodd = IIF(mm.use_alt,mm.ccalt,mm.cc)
	USE IN mm


*!* SET OTHER CONSTANTS
	DO CASE
		CASE cOffice = "M"
			cCustLoc =  "FL"
			cFMIWarehouse = ""
			cCustPrefix = "945bw"+"f"
			cDivision = "Florida"
			cSF_Addr1  = "11400 NW 32ND AVE"
			cSF_CSZ    = "MIAMI*FL*33167"

		CASE cOffice = "N"
			cCustLoc =  "NJ"
			cFMIWarehouse = ""
			cCustPrefix = "945bw"+"j"
			cDivision = "New Jersey"
			cSF_Addr1  = "800 FEDERAL BLVD"
			cSF_CSZ    =  "CARTERET*NJ*07008"

		OTHERWISE
			cCustLoc =  "CA"
			cFMIWarehouse = ""
			cCustPrefix = "945bw"+"c"
			cDivision = "California"
			cSF_Addr1  = "450 WESTMONT DRIVE"
			cSF_CSZ    = "SAN PEDRO*CA*90731"
	ENDCASE

	IF !USED("edi_trigger")
		USE F:\3pl\DATA\EDI_TRIGGER IN 0
	ENDIF

	IF USED('ackdata')
		USE IN ackdata
	ENDIF

	useca("ackdata","wh")

	IF USED('bcny_scac_conv')   && Added per Todd to substitute alternate SCACs for certain consignees, 08.18.2014
		USE IN bcny_scac_conv
	ENDIF
	USE (cUsefolder+"bcny_scac_conv")	 IN 0 ALIAS bcny_scac_conv

	SELECT outship
	IF !SEEK(cBOL,'outship','bol_no')
		SET STEP ON
		cErrMsg = "MISS BOL "+cBOL
		DO ediupdate WITH cErrMsg,.T.
		THROW
	ENDIF

	cCustFolder = UPPER(cCustname)  && +"-"+cCustLoc
	lNonEDI = .F.  && Assumes not a Non-EDI 945
	lDoPAL = .T.
	STORE "" TO lcKey
	STORE 0 TO alength,nLength

*!* SET OTHER CONSTANTS
	dt1 = TTOC(DATETIME(),1)
	dtmail = TTOC(DATETIME())
	dt2 = DATETIME()
	m.loaddt  = DATE()
	m.loadtime = dt2

	cString = ""

	csendqual = "ZZ"
	IF lTesting = .T.
		csendid = "TGF-US-TEST"
	ELSE
		csendid = "TGF-US-"+ICASE(cOffice = "I","NJ",cOffice = "M","FL","SP")
	ENDIF

	crecqual = "01"
	crecid = "087607334"

	cfd = "*"
	csegd = "^"
	nSegCtr = 0
	cterminator = ">" && Used at end of ISA segment
	cdate = DTOS(DATE())
	ctruncdate = RIGHT(cdate,6)
	ctime = LEFT(TIME(DATE()),2)+SUBSTR(TIME(DATE()),4,2)
	cfiledate = cdate+ctime
	cOrig = "J"
	nSTCount = 0
	cStyle = ""
	cPTString = ""
	nTotCtnWt = 0
	nTotCtnCount = 0
	nUnitSum = 0
	cTargetStyle = ""

** PAD ID Codes
	csendidlong = PADR(csendid,15," ")
	crecidlong = PADR(crecid,15," ")

	cFileNameShort = (cCustPrefix+dt1+".edi")
	cFilenameHold = (lcHoldPath+cFileNameShort)
	cFilenameArch = (lcArchivePath+cFileNameShort)
	cFilenameOut = (lcPath+cFileNameShort)
	m.filename = cFilenameOut
	cFilename = cFilenameHold
	nFilenum = FCREATE(cFilenameHold)

	IF !USED("parcel_carriers")
		USE F:\3pl\DATA\parcel_carriers IN 0 ALIAS parcel_carriers
	ENDIF
	IF !USED('OUTSHIP')
		USE (cUsefolder+"OUTSHIP") IN 0 ALIAS outship
	ENDIF

	IF lParcelLong
		=SEEK(cShip_ref,'outship','ship_ref')
	ELSE
		=SEEK(cBOL,'outship','bol_no')
	ENDIF
	nWO_num = outship.wo_num
	cShip_SCAC = IIF(ALLTRIM(outship.scac)="UPSZ","UPSN",ALLTRIM(outship.scac))  && Changed per Maria Estrella, 03.20.2014
	lParcelType = IIF(SEEK(cShip_SCAC,'parcel_carriers','scac'),.T.,.F.)

	IF !lParcelType
		DO m:\dev\prg\overflowpt_xcheck WITH cBOL
	ENDIF

	SELECT outship
	IF lTesting
		SET STEP ON
	ENDIF
	IF lParcelType
		LOCATE
		cScanParam = IIF(lParcelLong,"ship_ref = cShip_ref","bol_no = cBOL")
		SCAN FOR &cScanParam
			cShip_ref = outship.ship_ref
			alength = ALINES(apt,outship.shipins,.T.,CHR(13))
			cLTN = segmentget(@apt,"LONGTRKNUM",alength)
			IF INLIST(outship.scac,"USPS","PRRM","SPAR","SFIR","SPRI","SPAD")  OR LEFT(ALLTRIM(outship.ship_via),4) = "USPS" && USPS parcels
				IF EMPTY(ALLTRIM(cLTN))
					SET STEP ON
					lParcelLong = .F.
					cErrMsg = "MISS LONGTRKNUM FOR USPS SHIPMENT"  && Will error the entire BOL.
					DO ediupdate WITH cErrMsg,.T.
					THROW
				ENDIF
			ENDIF
		ENDSCAN
	ENDIF

	=SEEK(cBOL,'outship','bol_no')
	lDoCP = .F.  && Determination of whether or not to add a CP tracking number element to the MAN segment, per Maria & Synclaire, 10.30.2017. Joe
	IF UPPER(outship.consignee) = "RACK ROOM" OR UPPER(outship.consignee) = "OFF BROADWAY"
		lDoCP = .T.
	ENDIF

	lPrepaidParcel = IIF("PREPAID"$UPPER(outship.ship_via),.T.,.F.)
	lFedex = IIF(SEEK(cShip_SCAC,'parcel_carriers','scac') AND parcel_carriers.fedex,.T.,.F.)
	IF lFedex
		IF outship.consignee = "STAGE" AND outship.scac = "FSP"
			cShip_SCAC = "FXSP"
		ELSE
			cShip_SCAC = "FDEG"
		ENDIF
	ENDIF

	lSkipUPSWO = .F.
*!*		lSkipUPSWO = IIF(lParcelType AND "~"$outship.ship_ref,.T.,.F.)  && Should not operate on the new "%" splits, which will process independently now. Per Paul. 08.05.2014, Joe
*!*		STORE 0 TO m.unitswo,m.cartonswo
*!*		IF lSkipUPSWO
*!*			SCAN FOR outship.bol_no = cBOL
*!*				IF "~C"$outship.ship_ref
*!*					m.cartonswo = outship.wo_num
*!*				ELSE
*!*					m.unitswo = outship.wo_num
*!*				ENDIF
*!*			ENDSCAN
*!*			WAIT WINDOW "C WO "+ALLT(STR(m.cartonswo))+", U WO "+ALLT(STR(m.unitswo)) TIMEOUT 2
*!*		ENDIF

	SELECT outship
	LOCATE
	IF lParcelLong
		=SEEK(cShip_ref,'outship','ship_ref')
	ELSE
		=SEEK(cBOL,'outship','bol_no')
	ENDIF

	lDoSQLUPS = .T.
	IF lParcelType
		IF !lSkipUPSWO
			IF USED('upswo')
				USE IN upswo
			ENDIF
			SELECT 0
			USE F:\3pl\DATA\upswo && Table of UPS WO#'s for back reference
			LOCATE
			LOCATE FOR upswo.accountid = nAcctNum
			IF !FOUND()
				IF lTesting
					REPLACE upswo.wo_num WITH 0 IN upswo
				ELSE
					INSERT INTO upswo (accountid,wo_num) VALUES (nAcctNum,nWO_num)
				ENDIF
			ELSE

				IF lDoSQLUPS
					REPLACE upswo.wo_num WITH nWO_num IN upswo NEXT 1
				ENDIF
			ENDIF
			USE IN upswo
		ELSE
			STORE .F. TO lFoundCarton, lFoundUnit
			IF USED('spwos')
				USE IN spwos
			ENDIF
			SELECT 0
			USE  F:\3pl\DATA\splits_wos ALIAS spwos EXCLUSIVE
			IF lTesting
				ZAP
			ENDIF
			LOCATE
			IF EOF()
				lDoSQLUPS = .T.
			ELSE
				LOCATE FOR m.cartonswo = spwos.splitwo
				IF FOUND()
					lFoundCarton = .T.
				ENDIF
				LOCATE FOR m.unitswo = spwos.splitwo
				IF FOUND()
					lFoundUnit = .T.
				ENDIF
				lDoSQLUPS = IIF(!lFoundUnit  OR !lFoundCarton,.T.,.F.)
			ENDIF
			IF lDoSQLUPS
				SELECT spwos
				ZAP
				INSERT INTO spwos (splitwo) VALUES (m.cartonswo)
				INSERT INTO spwos (splitwo) VALUES (m.unitswo)
*!*					locate
*!*					brow
			ENDIF
		ENDIF
	ENDIF
	USE IN parcel_carriers

	IF USED('BL')
		USE IN BL
	ENDIF
	IF lUseSQLBL
		useca("bl","wh")
	ELSE
		USE (cUsefolder+"BL") IN 0 ALIAS BL
	ENDIF

*!*		IF !lTesting AND lParcelType
*!*			IF USED('sync_wohold')
*!*				USE IN sync_wohold
*!*			ENDIF
*!*			SELECT 0
*!*			USE F:\3pl\DATA\sync_wohold
*!*			nWO_numOld = sync_wohold.wo_num
*!*			IF nWO_num = nWO_numOld
*!*				lDoSQLRun = .F.
*!*			ELSE
*!*				REPLACE wo_num WITH nWO_num IN sync_wohold
*!*				lDoSQLRun = .T.
*!*			ENDIF
*!*		ENDIF

	IF lDoSQLConnect
		IF lDoSQLUPS
			SELECT outship
			IF USED("sqlwobcny")
				USE IN sqlwobcny
			ENDIF
			DELETE FILE "F:\3pl\DATA\sqlwobcny.dbf"
*!*				IF lSkipUPSWO
*!*	*				SET STEP ON
*!*					SELECT splitwo AS wo_num ;
*!*						FROM spwos ;
*!*						ORDER BY 1 ;
*!*						INTO DBF F:\3pl\DATA\sqlwobcny
*!*				ELSE
			IF USED("sqlptbcny")
				USE IN sqlptbcny
			ENDIF

			IF FILE("F:\3pl\DATA\sqlptbcny.dbf")
				DELETE FILE "F:\3pl\DATA\sqlptbcny.dbf"
			ENDIF
			CREATE TABLE F:\3pl\DATA\sqlptbcny.DBF (ship_ref c(20),scanpack L)
			SELECT outship
			cScanStrSQL = IIF(lParcelLong, "outship.ship_ref = cShip_ref","outship.bol_no = PADR(cBOL,20)")
			SCAN FOR &cScanStrSQL
*			SCAN FOR outship.bol_no = PADR(cBOL,20)	&& Added this replacement block to create a PT list with scanpack indicator included, Joe, 08.16.2016
				m.ship_ref = outship.ship_ref
				alength = ALINES(apt,outship.shipins,.T.,CHR(13))
				cSP = segmentget(@apt,"PROCESSMODE",alength)
				m.scanpack = IIF(cSP = "SCANPACK",.T.,.F.)
				INSERT INTO sqlptbcny FROM MEMVAR
			ENDSCAN
*WAIT WINDOW "SQL PT Table complete..." NOWAIT

			SELECT sqlptbcny

			IF (lTesting OR lBrowfiles)
				BROWSE
			ENDIF
			cRetMsg = "X"

			lDoSQLRun =.T.
			IF lDoSQLRun
				DO m:\dev\prg\sqlconnect_bol_synclaire  WITH nAcctNum,cCustname,cPPName,.T.,cOffice,.F.,.F.,lDoCartons && Amended for UCC number sequencing
				IF cRetMsg<>"OK"
					cErrMsg = cRetMsg
					DO ediupdate WITH cErrMsg,.T.
					THROW
				ENDIF

				SELECT vsynclairepp
				LOCATE
				IF lTesting AND lBrowfiles
					BROWSE
					SET STEP ON
				ENDIF

*				COPY TO ('h:\fox\synclairewo.dbf')
				LOCATE
			ELSE
				USE ('h:\fox\synclairewo') IN 0 ALIAS wofile  && If a parcel and the WO# has not changed, reuse the SQL cursor data
				SELECT * FROM wofile INTO CURSOR vsynclairepp READWRITE
				USE IN wofile
			ENDIF
		ENDIF
	ENDIF

*!* Added this code block to remove zero-qty cartons from SQL cursor
	SELECT ucc,SUM(totqty) AS uccqty FROM vsynclairepp GROUP BY 1 INTO CURSOR tempchk HAVING uccqty=0
	SELECT tempchk
	LOCATE
	IF (DATETIME() < DATETIME(2014,09,19,20,15,00))
		BROWSE
	ENDIF
	IF !EOF()
		SCAN
			STORE ALLT(ucc) TO cUCC
			SELECT vsynclairepp
			LOCATE
			DELETE FOR ucc = cUCC
		ENDSCAN
	ENDIF
	USE IN tempchk

	SELECT vsynclairepp
	SET RELATION TO outdetid INTO outdet

*!*		IF !USED("scacs")
*!*			WAIT WINDOW
*!*			xsqlexec("select * from scac","scacs",,"wh")
*!*			INDEX ON scac TAG scac
*!*		ENDIF
	IF !USED("parcel_carriers")
		USE F:\3pl\DATA\parcel_carriers IN 0 ALIAS parcel_carriers
	ENDIF
	SELECT outship
	LOCATE
	WAIT CLEAR


************************************************************************************************************************
	WAIT WINDOW "Now creating Header information" NOWAIT NOCLEAR

*!* HEADER LEVEL EDI DATA
	DO num_incr_isa

	cISA_Num = PADL(c_CntrlNum,9,"0")
	nISA_Num = INT(VAL(cISA_Num))
	m.isanum = cISA_Num
	m.groupnum = c_CntrlNum

	STORE "ISA"+cfd+"00"+cfd+"          "+cfd+"00"+cfd+"          "+cfd+csendqual+cfd+csendidlong+cfd+;
		crecqual+cfd+crecidlong+cfd+ctruncdate+cfd+ctime+cfd+"U"+cfd+"00401"+cfd+;
		cISA_Num+cfd+"0"+cfd+IIF(lTesting,"T","P")+cfd+cterminator+csegd TO cString
	DO cstringbreak

	STORE "GS"+cfd+"SW"+cfd+csendid+cfd+crecid+cfd+cdate+cfd+ctime+cfd+c_CntrlNum+;
		cfd+"X"+cfd+cX12+csegd TO cString
	DO cstringbreak

*************************************************************************
*1  PTIDSOURCE/OUTSHIP RECORD MAIN LOOP
*  Loops through individual OUTSHIP lines for BOL#
*************************************************************************

	WAIT CLEAR
	WAIT WINDOW "Now creating BOL#-based information..." NOWAIT NOCLEAR

	nPTCount = 0
	lSplitPT = .F.
	cShip_refLast = "XXXX"

*	lParcelLong = .f.
	SELECT outship
	cScanStr = IIF(lParcelLong,"outship.ship_ref = cShip_ref and INLIST(accountid,&gbcnyrecaccounts)","outship.bol_no = cBOL")
	SCAN FOR &cScanStr	&& Added this replacement block to create a PT list with scanpack indicator included, Joe, 08.16.2016
*	SCAN FOR BOL_NO = cBOL &&And accountid = nAcctNum
		IF outship.accountid = 6522 OR outship.ctnqty = 0
			LOOP
		ENDIF
		IF 'SMU'$outship.ship_ref
*			LOOP
		ENDIF

		SCATTER MEMVAR MEMO
		alength = ALINES(apt,outship.shipins,.T.,CHR(13))

		cConsignee = ALLTRIM(outship.consignee)
		lAmazon = IIF("AMAZON"$outship.consignee,.T.,.F.)
		lZappos = IIF("ZAPPO"$outship.consignee,.T.,.F.)
		lSaks = IIF("SAKS.COM"$outship.consignee,.T.,.F.)
		cTPInfo = UPPER(segmentget(@apt,"TPINFO",alength))
		lTPAmazon = IIF("AMAZON"$cTPInfo,.T.,.F.)
		RELEASE cTPInfo

		IF lAmazon
			WAIT WINDOW "This is an Amazon shipment" TIMEOUT 2
		ENDIF

		cConsignee = ALLTRIM(outship.consignee)
		lSCACConv = IIF("KOHLS.COM"$cConsignee OR "BELK.COM"$cConsignee OR "QVC"$cConsignee OR "TOYS"$cConsignee OR "BBB"$cConsignee OR "BB&B"$cConsignee OR ("BED"$cConsignee AND "BATH"$cConsignee),.T.,.F.)
		IF lSCACConv  && If one of the selected SCAC conversion consignees...
			cSCAINFO = ALLTRIM(segmentget(@apt,"SCAINFO",alength)) && Added this section per Todd, 08.18.2014
			IF "~"$cSCAINFO
				cSCAINFO = LEFT(cSCAINFO, AT("~",cSCAINFO)-1)
			ENDIF
			cShip_SCAC = ALLTRIM(cSCAINFO)
			IF lSCACConv AND LEFT(cSCAINFO,3) = "UNS"
				SELECT bcny_scac_conv
				LOCATE FOR bcny_scac_conv.scac_940 = cSCAINFO AND bcny_scac_conv.scac = ALLTRIM(outship.scac)
				IF FOUND()
					cShip_SCAC = ALLTRIM(bcny_scac_conv.scac_945)
				ELSE  && Will send a notification mail to Todd if the SCAC is unmatched
					cErrMsg = "NO CONVERSION SCAC"
					SET STEP ON
					DO ediupdate WITH cErrMsg,.T.
					THROW
				ENDIF
			ENDIF
		ENDIF

		nOutshipid = m.outshipid
		m.ship_ref = ALLTRIM(m.ship_ref)
		cShip_ref = ALLTRIM(m.ship_ref)
		m.processed = .F.
		IF lTestSplits
			cShip_ref = STRTRAN(ALLTRIM(cShip_ref),"~","%")
		ENDIF
		lSplitPT = IIF("~"$cShip_ref,.T.,.F.)

		IF lSplitPT
			cPTbase = ALLTRIM(LEFT(ALLTRIM(cShip_ref),AT("~",cShip_ref,1)-1))
			nrec1 = RECNO()
			cRefSR = IIF("~C"$cShip_ref,cPTbase+" ~U",cPTbase+" ~C")
			LOCATE
			LOCATE FOR outship.ship_ref = cRefSR  AND outship.bol_no = cBOL
			IF !FOUND()
				cErrMsg = "SPLIT PT - DIFF BOLs"
				DO ediupdate WITH cErrMsg,.T.
				THROW
			ENDIF
			RELEASE cPTbase,cRefSR
			GO nrec1
		ENDIF

		IF (" OV"$cShip_ref) OR (RIGHT(ALLTRIM(cShip_ref),1)=".")
			LOOP
		ENDIF
		lJCPenney= IIF("PENNEY"$outship.consignee,.T.,.F.)
		lWalMart = IIF(("WALMART"$UPPER(m.consignee)) OR ("WAL-MART"$UPPER(m.consignee)),.T.,.F.)
		lSpencer = IIF("SPENCER"$outship.consignee,.T.,.F.)
		lTarget = IIF("TARGET"$outship.consignee,.T.,.F.)
		lGap = IIF("GAP "$outship.consignee OR outship.consignee = "THE GAP ",.T.,.F.)
		lKmart = IIF("KMART"$outship.consignee OR "K-MART"$outship.consignee,.T.,.F.)
		lToys= IIF("TOYS"$outship.consignee,.T.,.F.)
		lKS= IIF("KIDSSHOES"$outship.consignee,.T.,.F.)
		lAmazonMWS = IIF("AMAZON"$UPPER(outship.consignee) AND "MWS"$UPPER(outship.consignee) AND "D2C"$UPPER(outship.consignee),.T.,.F.)
		lZero= IIF("ZEROTIE"$UPPER(outship.consignee),.T.,.F.)
		nRec = RECNO()
		IF lToys
			SUM cuft TO nCuFt FOR bol_no = cBOL
			cCuFt = ALLTRIM(STR(CEILING(nCuFt)))
			RELEASE nCuFt
			GO nRec
		ENDIF
		cShip_SCAC = IIF(LEFT(ALLTRIM(outship.scac),2)="UP" AND lTarget,"UPSN",ALLTRIM(outship.scac))  && Changed per Todd M, 10.07.2016, amended to include all UP prefix SCACs
		cShip_SCAC = IIF(lSaks AND outship.scac = "FHOM","FEDH",cShip_SCAC)
		cShip_refUse = IIF(lSplitPT,ALLTRIM(LEFT(cShip_ref,AT("~",ALLTRIM(cShip_ref))-1)),cShip_ref)
		lNewPT = IIF(cShip_refLast = cShip_refUse,.F.,.T.)
		STORE cShip_refUse TO cShip_refLast

		SELECT outship

		IF !lSplitPT OR (lSplitPT AND lNewPT) && We only want to process the PT header once for each split PT root-number.
			nCtnNumber = 1  && Begin sequence count
			lDoFirstRound = .T.

			IF lWalMart
				WAIT WINDOW "This is a Wal-Mart order" &cTimeMsg
			ENDIF
			ddel_date = outship.del_date
			IF EMPTY(ddel_date) AND notonwip AND !lTesting
				LOOP
			ENDIF
			nOSQty = m.qty
			IF nOSQty = 0
				IF !lSplitPT
					LOOP
				ELSE
					cErrMsg = "EMPTY OS QTY IN SPLIT PT"
					DO ediupdate WITH cErrMsg,.T.
					THROW
				ENDIF
			ENDIF

			nTotCtnCount = m.qty
			cPO_Num = ALLTRIM(m.cnee_ref)
			nPTCount = nPTCount + 1

			nWO_num = outship.wo_num
			cWO_Num = ALLTRIM(STR(nWO_num))
			cPadWO_Num = PADR(cWO_Num,10)
			IF TRIM(cWO_Num) <> TRIM(cWO_NumOld)
				STORE cWO_Num TO cWO_NumOld
				IF EMPTY(cWO_NumStr)
					STORE cWO_Num TO cWO_NumStr,cWO_NumList
				ELSE
					IF !(cWO_Num$cWO_NumStr)
						cWO_NumStr = (cWO_NumStr+", "+cWO_Num)
					ENDIF
					IF !(cWO_Num$cWO_NumList)
						cWO_NumList = (cWO_NumList+CHR(13)+cWO_Num)
					ENDIF
				ENDIF
			ENDIF

			cPTString = IIF(EMPTY(cPTString),m.consignee+" "+cShip_refUse,cPTString+CHR(13)+m.consignee+" "+cShip_refUse)

			DO num_incr_st
			WAIT CLEAR
			WAIT WINDOW "Now creating Line Item information" NOWAIT NOCLEAR

			INSERT INTO temp945 (accountid,isa_num,st_num,wo_num,bol_no,ship_ref,filename,filedate) ;
				VALUES (m.accountid,c_CntrlNum,c_GrpCntrlNum,m.wo_num,m.bol_no,cShip_refUse,cFileNameShort,dt2)

			m.transnum = PADL(c_GrpCntrlNum,9,"0")
			STORE "ST"+cfd+"945"+cfd+m.transnum+csegd TO cString

			insertinto("ackdata","wh",.T.)

			IF lTesting
				nTotCtnWt = 0
				nTotCtnCount = 0
				nUnitSum = 0
			ENDIF
			DO cstringbreak
			nSTCount = nSTCount + 1
			nSegCtr = 1

			cShipmentID = ""
			cShipmentID = segmentget(@apt,"LONGTRKNUM",alength)
			IF EMPTY(cShipmentID)
				cShipmentID = TRIM(cBOL)
			ENDIF
			cRELEASENUM = segmentget(@apt,"RELEASENUM",alength)
			cW0504 = segmentget(@apt,"W0504",alength)
			STORE "W06"+cfd+"N"+cfd+cShip_refUse+cfd+cdate+cfd+TRIM(cShipmentID)+cfd+TRIM(cWO_Num)+cfd+cPO_Num+IIF(!EMPTY(cRELEASENUM),cfd+cRELEASENUM,"")+ICASE(!EMPTY(cRELEASENUM) AND !EMPTY(cW0504),cfd+cW0504+csegd,EMPTY(cRELEASENUM) AND !EMPTY(cW0504),REPLICATE(cfd,2)+cW0504+csegd,csegd) TO cString
			DO cstringbreak
			nSegCtr = nSegCtr + 1

			cDUNSQUAL = segmentget(@apt,"DUNSQUAL",alength)
			cDUNSQUAL = IIF(EMPTY(cDUNSQUAL),"9",cDUNSQUAL)
			cDUNSNumber = segmentget(@apt,"DUNSNUMBER",alength)
			cDUNSNumber = IIF(EMPTY(cDUNSNumber),ALLTRIM(outship.duns),cDUNSNumber)
			IF EMPTY(ALLTRIM(outship.duns))
				STORE "N1"+cfd+"SF"+cfd+"BCNY"+csegd TO cString
			ELSE
				STORE "N1"+cfd+"SF"+cfd+"BCNY"+cfd+cDUNSQUAL+cfd+cDUNSNumber+csegd TO cString
			ENDIF
			DO cstringbreak
			nSegCtr = nSegCtr + 1

			STORE "N3"+cfd+"400-450 WESTMONT DRIVE"+csegd TO cString
			DO cstringbreak
			nSegCtr = nSegCtr + 1

			STORE "N4"+cfd+"SAN PEDRO"+cfd+"CA"+cfd+"90731"+csegd TO cString
			DO cstringbreak
			nSegCtr = nSegCtr + 1

			STORE "" TO cBT,cBTDC,cBYName,cBYNum

			cDCNum = ""
			cDCNum = ALLT(segmentget(@apt,"STORENUM",alength))
			cDCNum = IIF((cDCNum # m.dcnum) OR (EMPTY(cDCNum)),ALLTRIM(m.dcnum),cDCNum)

			m.CSZ = TRIM(outship.CSZ)
			IF EMPTY(ALLT(STRTRAN(M.CSZ,",","")))
				WAIT CLEAR
				WAIT WINDOW "No SHIP-TO City/State/ZIP info...exiting" TIMEOUT 2
				cErrMsg = "NO CSZ INFO"
				DO ediupdate WITH cErrMsg,.T.
				THROW
			ENDIF
			IF !(", "$m.CSZ)
				m.CSZ = ALLTRIM(STRTRAN(m.CSZ,",",", "))
			ENDIF
			m.CSZ = ALLTRIM(STRTRAN(m.CSZ,"  "," "))
			cCity = ALLT(LEFT(TRIM(m.CSZ),AT(",",m.CSZ)-1))
			cStateZip = ALLT(SUBSTR(TRIM(m.CSZ),AT(",",m.CSZ)+1))

			cState = ALLT(LEFT(cStateZip,2))
			cOrigState = segmentget(@apt,"ORIGSTATE",alength)
			cState = IIF(EMPTY(cOrigState),cState,cOrigState)
			cZip = ALLT(SUBSTR(cStateZip,3))

			cSHIPTO = ALLTRIM(segmentget(@apt,"SHIPTO",alength))
			cSHIPTO = IIF(EMPTY(cSHIPTO),ALLTRIM(outship.consignee),cSHIPTO)
			cSHIPTO = STRTRAN(cSHIPTO,"<>","#")
			STORE "N1"+cfd+"ST"+cfd+cSHIPTO+IIF(!EMPTY(cDCNum),+cfd+"92"+cfd+cDCNum,"")+csegd TO cString
			DO cstringbreak
			nSegCtr = nSegCtr + 1

			STORE "N3"+cfd+TRIM(outship.address)+IIF(!EMPTY(outship.address2),cfd+ALLT(outship.address2),"")+csegd TO cString
			DO cstringbreak
			nSegCtr = nSegCtr + 1

			STORE "N4"+cfd+cCity+cfd+cState+cfd+cZip+csegd TO cString
			DO cstringbreak
			nSegCtr = nSegCtr + 1

			IF !EMPTY(m.shipfor)
				STORE "" TO cSForCity,cSForState,cSForZip
				m.SForCSZ = ALLTRIM(STRTRAN(m.SForCSZ,"  "," "))
				nSpaces = OCCURS(" ",ALLTRIM(m.SForCSZ))
				IF nSpaces = 0
					m.SForCSZ = STRTRAN(m.SForCSZ,",","")
					cSForCity = ALLTRIM(m.SForCSZ)
					cSForState = ""
					cSForZip = ""
				ELSE
					nCommaPos = AT(",",m.SForCSZ)
					nLastSpace = AT(" ",m.SForCSZ,nSpaces)
					nMinusSpaces = IIF(nSpaces=1,0,1)
					IF ISALPHA(SUBSTR(TRIM(m.SForCSZ),AT(" ",TRIM(m.SForCSZ),nSpaces-nMinusSpaces)+1,2))
						cSForCity = LEFT(TRIM(m.SForCSZ),AT(",",m.SForCSZ)-1)
						cSForState = SUBSTR(m.SForCSZ,nCommaPos+2,2)
						cSForZip = TRIM(RIGHT(TRIM(m.SForCSZ),5))
						IF ISALPHA(cSForZip)
							cSForZip = ""
						ENDIF
					ELSE
						WAIT CLEAR
						WAIT WINDOW "NOT ALPHA: "+SUBSTR(TRIM(m.SForCSZ),AT(" ",m.SForCSZ,nSpaces-1)+1,2) &cTimeMsg
						cSForCity = LEFT(TRIM(m.SForCSZ),AT(",",m.SForCSZ)-1)
						cSForState = SUBSTR(TRIM(m.SForCSZ),AT(" ",m.SForCSZ,nSpaces-2)+1,2)
						cSForZip = TRIM(RIGHT(TRIM(m.SForCSZ),5))
						IF ISALPHA(cSForZip)
							cSForZip = ""
						ENDIF
					ENDIF
				ENDIF

				IF EMPTY(ALLTRIM(m.sforstore))
					STORE "N1"+cfd+"Z7"+cfd+ALLTRIM(m.shipfor)+csegd TO cString
				ELSE
					STORE "N1"+cfd+"Z7"+cfd+ALLTRIM(m.shipfor)+cfd+"92"+cfd+ALLTRIM(m.sforstore)+csegd TO cString
				ENDIF
				DO cstringbreak
				nSegCtr = nSegCtr + 1
				STORE "N3"+cfd+ALLTRIM(m.sforaddr1)+csegd TO cString
				DO cstringbreak
				nSegCtr = nSegCtr + 1
				STORE "N4"+cfd+ALLTRIM(cSForCity)+cfd+ALLTRIM(cSForState)+cfd+ALLTRIM(cSForZip)+csegd TO cString
				DO cstringbreak
				nSegCtr = nSegCtr + 1
			ENDIF

			cBYName = segmentget(@apt,"BYNAME",alength)
			cBYName = STRTRAN(cBYName,"<>","#")
			cBYNum = segmentget(@apt,"N1-BY",alength)
			IF !lTestinput
				IF (EMPTY(cBYName) AND EMPTY(cBYNum))
*
				ELSE
					STORE "N1"+cfd+"BY"+cfd+cBYName+cfd+"92"+cfd+cBYNum+csegd TO cString
					DO cstringbreak
					nSegCtr = nSegCtr + 1
				ENDIF
			ENDIF

** Ok, now in the header N9 segments
** Now if the shipment is a Smartpost (Fedex-->USPS) then we need to get Ref4 from the package table

			DO CASE
				CASE lParcelType AND outship.scac="FSP" && here we look for REF4 in the pachage table, this is the USPS trk# for a Smartpost shipment
					SELECT shipment
					LOCATE FOR accountid = 6521 AND pickticket = outship.ship_ref
					IF FOUND()
						=SEEK(shipment.shipmentid,"package","shipmentid")
						IF FOUND("package")
							IF !EMPTY(package.ref4)
								STORE "N9"+cfd+"BM"+cfd+SUBSTR(ALLTRIM(package.ref4),9)+csegd TO cString
							ELSE
								STORE "N9"+cfd+"BM"+cfd+ALLTRIM(cShipmentID)+csegd TO cString
							ENDIF
						ELSE
							STORE "N9"+cfd+"BM"+cfd+ALLTRIM(cShipmentID)+csegd TO cString
						ENDIF
					ELSE
						STORE "N9"+cfd+"BM"+cfd+ALLTRIM(cShipmentID)+csegd TO cString
					ENDIF

				CASE lParcelType AND "PENNEY"$UPPER(m.consignee)
					STORE "N9"+cfd+"BM"+cfd+"0"+csegd TO cString
				OTHERWISE
					STORE "N9"+cfd+"BM"+cfd+ALLTRIM(cShipmentID)+csegd TO cString
			ENDCASE

			DO cstringbreak
			nSegCtr = nSegCtr + 1

			IF lToys AND lParcelType
				STORE "N9"+cfd+"CN"+cfd+TRIM(cBOL)+csegd TO cString
				DO cstringbreak
				nSegCtr = nSegCtr + 1
			ENDIF

			cDept = ALLT(outship.dept)
			IF EMPTY(cDept)
				cDept = segmentget(@apt,"DEPT",alength)
				cDept = STRTRAN(cDept,"<>","#")
			ENDIF
			IF !EMPTY(cDept)
				STORE "N9"+cfd+"DP"+cfd+TRIM(outship.dept)+csegd TO cString
				DO cstringbreak
				nSegCtr = nSegCtr + 1
			ENDIF

			cVendnum = ""
			cVendnum = segmentget(@apt,"VENDORNUM",alength) && Added for 10+ character vendor numbers
			cVendnum = STRTRAN(cVendnum,"<>","#")
			IF EMPTY(cVendnum)
				cVendnum = TRIM(outship.vendor_num)
			ENDIF
			IF !EMPTY(cVendnum)
				STORE "N9"+cfd+"IA"+cfd+cVendnum+csegd TO cString
				DO cstringbreak
				nSegCtr = nSegCtr + 1
			ENDIF

			cSNNum = segmentget(@apt,"SNNUM",alength)
			cSNNum = STRTRAN(cSNNum,"<>","#")
			IF !EMPTY(cSNNum)
				STORE "N9"+cfd+"SN"+cfd+TRIM(cSNNum)+csegd TO cString
				DO cstringbreak
				nSegCtr = nSegCtr + 1
			ENDIF

			cPKNum = segmentget(@apt,"PKNUM",alength)
			IF !EMPTY(cPKNum)
				STORE "N9"+cfd+"PK"+cfd+TRIM(cPKNum)+csegd TO cString
				DO cstringbreak
				nSegCtr = nSegCtr + 1
			ENDIF

			cVNInfo = segmentget(@apt,"VNINFO",alength)
			cVNInfo  = STRTRAN(cVNInfo,"<>","#")
			IF !EMPTY(cVNInfo)
				STORE "N9"+cfd+"VN"+cfd+TRIM(cVNInfo)+csegd TO cString
				DO cstringbreak
				nSegCtr = nSegCtr + 1
			ENDIF

			cVINum = segmentget(@apt,"VINUM",alength)
			IF !EMPTY(cVINum)
				STORE "N9"+cfd+"VI"+cfd+TRIM(cVINum)+csegd TO cString
				DO cstringbreak
				nSegCtr = nSegCtr + 1
			ENDIF

			cTPInfo = segmentget(@apt,"TPINFO",alength)
			cTPInfo  = STRTRAN(cTPInfo,"<>","#")
			IF !EMPTY(cTPInfo)
				STORE "N9"+cfd+"TP"+cfd+TRIM(cTPInfo)+csegd TO cString
				DO cstringbreak
				nSegCtr = nSegCtr + 1
			ENDIF

			cSCAINFO = segmentget(@apt,"SCAINFO",alength)
			cSCAINFO  = STRTRAN(cSCAINFO,"<>","#")
			IF !EMPTY(cSCAINFO)
				STORE "N9"+cfd+"SCA"+cfd+TRIM(cSCAINFO)+csegd TO cString
				DO cstringbreak
				nSegCtr = nSegCtr + 1
			ENDIF

			cSTInfo = segmentget(@apt,"STINFO",alength)
			cSTInfo  = STRTRAN(cSTInfo,"<>","#")
			IF !EMPTY(cSTInfo)
				STORE "N9"+cfd+"ST"+cfd+TRIM(cSTInfo)+csegd TO cString
				DO cstringbreak
				nSegCtr = nSegCtr + 1
			ENDIF

			cWHInfo = segmentget(@apt,"WHINFO",alength)
			cWHInfo  = STRTRAN(cWHInfo,"<>","#")
			IF !EMPTY(cWHInfo)
				STORE "N9"+cfd+"WH"+cfd+TRIM(cWHInfo)+csegd TO cString
				DO cstringbreak
				nSegCtr = nSegCtr + 1
			ENDIF

			cSMU = segmentget(@apt,"SMU",alength)
			cSMU = STRTRAN(cSMU,"<>","#")
			IF !EMPTY(cSMU)
				STORE "N9"+cfd+"SMU"+cfd+TRIM(cSMU)+csegd TO cString
				DO cstringbreak
				nSegCtr = nSegCtr + 1
			ENDIF

			cATInfo = segmentget(@apt,"ATINFO",alength)
			cATInfo  = STRTRAN(cATInfo,"<>","#")
			IF !EMPTY(cATInfo)
				STORE "N9"+cfd+"AT"+cfd+TRIM(cATInfo)+csegd TO cString
				DO cstringbreak
				nSegCtr = nSegCtr + 1
			ENDIF

			cEDIInfo = segmentget(@apt,"EDI-INFO",alength)
			cEDIInfo  = STRTRAN(cEDIInfo,"<>","#")
			IF !EMPTY(cEDIInfo)
				STORE "N9"+cfd+"TI"+cfd+TRIM(cEDIInfo)+csegd TO cString
				DO cstringbreak
				nSegCtr = nSegCtr + 1
			ENDIF

			termsgen() && Gets terms type
			IF lParcelType
				cTerms = "CC"
				cTerms = IIF(lPrepaidParcel,"PP","CC")
			ENDIF

*			IF (!lParcelType OR (lParcelType AND cTerms = "CC" AND lTPAmazon) OR (lParcelType AND lZappos)) AND !lPrepaidParcel AND !lKS AND !lAmazonMWS AND !lZero
			IF (!lParcelType OR (lParcelType AND lZappos)) AND !lAmazonMWS AND !lKS AND !lZero
				cLoadID = ALLTRIM(outship.appt_num)
				IF EMPTY(cLoadID)
					IF (outship.consignee = "AP-" OR outship.consignee = "AP -")
						cLoadID = DTOS(DATE())
					ELSE
						cErrMsg = "MISS LOADID "+cShip_refUse
						DO ediupdate WITH cErrMsg,.T.
						THROW
					ENDIF
				ENDIF
				STORE "N9"+cfd+"LO"+cfd+TRIM(cLoadID)+csegd TO cString
				DO cstringbreak
				nSegCtr = nSegCtr + 1
			ELSE
				IF lToys
					cLoadID = "NA"
					STORE "N9"+cfd+"LO"+cfd+TRIM(cLoadID)+csegd TO cString
					DO cstringbreak
					nSegCtr = nSegCtr + 1
				ELSE
					IF (lAmazon  OR lTPAmazon) AND !lPrepaidParcel && Added this per Wei, 2014.08.07, rev. 2014.10.16
						cLoadID = ALLTRIM(outship.appt_num)
						IF EMPTY(cLoadID)
							cLoadID = ALLTRIM(cBOL)
							IF EMPTY(cLoadID)
								cErrMsg = "MISS LOADID "+cShip_refUse
								DO ediupdate WITH cErrMsg,.T.
								THROW
							ENDIF
						ELSE
							STORE "N9"+cfd+"LO"+cfd+TRIM(cLoadID)+csegd TO cString
							DO cstringbreak
							nSegCtr = nSegCtr + 1
						ENDIF
					ENDIF
				ENDIF
			ENDIF

			SELECT outship

			IF EMPTYnul(outship.del_date)
				IF lJCPenney
					cErrMsg = "MISSING DELDATE"
					DO ediupdate WITH cErrMsg,.T.
					THROW
				ELSE
					ddel_date = outship.appt
				ENDIF
			ELSE
				ddel_date = outship.del_date
				IF EMPTY(ddel_date)
					IF lTesting
						ddel_date = DATE()
					ELSE
						SELECT EDI_TRIGGER
						LOCATE
						LOCATE FOR accountid = nAcctNum AND ship_ref = cShip_ref AND bol = cBOL AND edi_type = "945"
						IF FOUND()
							REPLACE fin_status WITH "MISSING DELDATE",processed WITH .T.,errorflag WITH .T.
							tsubject = "945 Error in BCNY/EA BOL "+TRIM(cBOL)+" (At PT "+cShip_ref+")"
							tattach = " "
							tsendto = tsendtoerr
							tcc = tccerr
							tmessage = "945 Processing for WO# "+TRIM(cWO_Num)+" produced this error: (Office: "+cOffice+"): "+cStatus+CHR(13)+"Please notify Joe of actual status for reprocessing ASAP."
							DO FORM m:\dev\frm\dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
						ENDIF
						LOOP
					ENDIF
				ENDIF

				dapptdate = outship.appt
				IF EMPTY(dapptdate)
					dapptdate = outship.del_date
				ENDIF

			ENDIF

			IF EMPTYnul(outship.CANCEL)
				IF outship.consignee = "KIDSSHOES.COM"
					REPLACE outship.CANCEL WITH (outship.START+5) IN outship NEXT 1
				ELSE
					REPLACE outship.CANCEL WITH DATE()+2 IN outship NEXT 1
				ENDIF
			ENDIF

			IF EMPTYnul(outship.del_date) OR EMPTYnul(outship.START)
				cErrMsg = "EMPTY "+ICASE(EMPTYnul(outship.del_date),"DELDATE",EMPTYnul(outship.START),"START","CANCEL")
				DO ediupdate WITH cErrMsg,.T.
				THROW
			ENDIF

			cUseTime = "153000"
			STORE "G62"+cfd+"11"+cfd+DTOS(del_date)+cfd+"9"+cfd+cUseTime+csegd TO cString  && Ship date
			DO cstringbreak
			nSegCtr = nSegCtr + 1

			cUseTime = "153000"
			STORE "G62"+cfd+"67"+cfd+DTOS(del_date)+cfd+"9"+cfd+cUseTime+csegd TO cString  && Sched ship date
			DO cstringbreak
			nSegCtr = nSegCtr + 1

			STORE "G62"+cfd+"10"+cfd+DTOS(outship.START)+csegd TO cString  && Ship date
			DO cstringbreak
			nSegCtr = nSegCtr + 1

			alength = ALINES(apt,outship.shipins,.T.,CHR(13))
			cPODate = segmentget(@apt,"PODATE",alength)
			IF !EMPTY(cPODate)
				STORE "G62"+cfd+"04"+cfd+cPODate+csegd TO cString  && Ship date
				DO cstringbreak
				nSegCtr = nSegCtr + 1
			ENDIF

			cDNLDate = segmentget(@apt,"DNLDATE",alength)
			IF !EMPTY(cDNLDate)
				STORE "G62"+cfd+"54"+cfd++DTOS(del_date)+csegd TO cString  && Ship date
				DO cstringbreak
				nSegCtr = nSegCtr + 1
			ENDIF

			SELECT outship

			cShipType=IIF(lParcelType,"U","M")
			cTerms = IIF(lParcelType,"CC",cTerms)
*!* Added the following SEGMENTGET of ship_via info to send back what was sent on the 940, in order to overcome changes made when SCAC is added in the Parcel system. Joe, 03.01.2018
			cShip_via = segmentget(@apt,"SPINS1",alength)
			IF "SHIP VIA:"$cShip_via
				cShip_via = ALLTRIM(SUBSTR(cShip_via,AT(":",cShip_via)+1))
			ELSE
				cShip_via = ALLTRIM(outship.ship_via)
			ENDIF
			DO CASE
				CASE EMPTY(cTerms) AND EMPTY(cTrailerNumber)
					STORE "W27"+cfd+cShipType+cfd+ALLTRIM(cShip_SCAC)+cfd+ALLTRIM(cShip_via)+csegd TO cString
				CASE !EMPTY(cTerms) AND EMPTY(cTrailerNumber)
					STORE "W27"+cfd+cShipType+cfd+ALLTRIM(cShip_SCAC)+cfd+ALLTRIM(cShip_via)+cfd+ALLTRIM(cTerms)+csegd TO cString
				OTHERWISE
					STORE "W27"+cfd+cShipType+cfd+ALLTRIM(cShip_SCAC)+cfd+ALLTRIM(cShip_via)+cfd+ALLTRIM(cTerms)+cfd+cfd+cfd+ALLTRIM(cTrailerNumber)+csegd TO cString
			ENDCASE
			DO cstringbreak
			nSegCtr = nSegCtr + 1

			#IF 0
				IF !lTestinput AND lParcelType
					SELECT shipment
					=SEEK(cShip_ref,"shipment","pickticket")
					WAIT WINDOW "PT running: "+ALLTRIM(shipment.pickticket) NOWAIT
					SELECT shipmentid,uspstrknumber,trknumber,ucc,ref4 FROM package INTO CURSOR package1 READWRITE
					SELECT shipment
					SCAN FOR shipment.pickticket = cShip_ref
						SELECT shipmentid,uspstrknumber,trknumber,ucc,ref4 FROM package WHERE package.shipmentid=shipment.shipmentid INTO CURSOR package2
						SELECT package1
						APPEND FROM DBF('package2')
					ENDSCAN
					LOCATE
				ENDIF
			#ENDIF

		ENDIF

*************************************************************************
*2  DETAIL LOOP - MATCHING SHIP_REF FIELD (FROM OUTSHIP)
*************************************************************************

*!*	?StartTime
*!*	endtime = Datetime()
*!*	?endtime

		WAIT CLEAR
		WAIT WINDOW "Now creating Detail information "+cShip_ref NOWAIT NOCLEAR
		SELECT vsynclairepp
		LOCATE
		LOCATE FOR ship_ref = TRIM(cShip_ref) AND outshipid = nOutshipid
		IF !FOUND()
			SET STEP ON
			IF !lTesting
				WAIT WINDOW "PT "+cShip_ref+" NOT FOUND in vsynclairepp...ABORTING" &cTimeMsg
				IF !lTesting
					cErrMsg = "MISS PT: "+cShip_ref+" "+ALLTRIM(TRANSFORM(nOutshipid))
					DO ediupdate WITH cErrMsg,.T.
				ENDIF
				=FCLOSE(nFilenum)
				ERASE &cFilename
				THROW
			ELSE
				LOOP
			ENDIF
		ENDIF

		IF lOverflow && Added this to automatically set deleted OFF for overflows
			SET DELETED OFF
		ENDIF

		SELECT vsynclairepp
		SCAN FOR vsynclairepp.ship_ref = TRIM(cShip_ref) AND vsynclairepp.outshipid = nOutshipid AND vsynclairepp.totqty#0
			IF EMPTY(outdet.outdetid)
				SET STEP ON
				WAIT WINDOW "OUTDETID "+TRANSFORM(vsynclairepp.outdetid)+" NOT FOUND IN OUTDET!...CLOSING" &cTimeMsg
				cErrMsg = "MISS ODID/WO: "+TRANSFORM(vsynclairepp.outdetid)+"/"+TRANSFORM(vsynclairepp.wo_num)
				DO ediupdate WITH cErrMsg,.T.
				=FCLOSE(nFilenum)
				ERASE &cFilename
				THROW
			ENDIF
		ENDSCAN

		SCANSTR = "vsynclairepp.ship_ref = cShip_ref and vsynclairepp.outshipid = nOutshipid and !DELETED()"

		SELECT vsynclairepp
		LOCATE FOR &SCANSTR
		cCartonNum= "XXX"
		cUCC="XXX"

		DO WHILE &SCANSTR
			lSkipBack = .T.

			IF vsynclairepp.totqty = 0 OR vsynclairepp.ucc = 'CUTS'
				SKIP 1 IN vsynclairepp
				LOOP
			ENDIF

			IF vsynclairepp.ucc = 'CUTS'
				SKIP 1 IN vsynclairepp
				LOOP
			ENDIF

			IF TRIM(vsynclairepp.ucc) <> cCartonNum
				STORE TRIM(vsynclairepp.ucc) TO cCartonNum
			ENDIF

			STORE "LX"+cfd+ALLTRIM(STR(nCtnNumber))+csegd TO cString   && Carton Seq. # (up to Carton total)
			lDoManSegment = .T.
			DO cstringbreak
			nSegCtr = nSegCtr + 1

			cDoString = "vsynclairepp.ucc = cCartonNum"
			DO WHILE &cDoString
				IF vsynclairepp.totqty = 0
					SKIP 1 IN vsynclairepp
					LOOP
				ENDIF

				IF lDoManSegment
					cUCC = TRIM(vsynclairepp.ucc)
					cTrkNumber = ""
					IF lParcelType AND lDoCP
						IF lTestinput
							cTrkNumber = ALLTRIM(outship.bol_no)
						ELSE
							IF lTesting
								SET STEP ON
							ENDIF
							SELECT package1
							WAIT WINDOW ALLTRIM(STR(RECCOUNT())) NOWAIT
							COUNT TO nUCCx FOR !EMPTY(package1.ucc)
							COUNT TO nRef4x FOR !EMPTY(package1.ref4)
							lNoCount = IIF((nUCCx = 0 AND nRef4x = 0),.T.,.F.)
							RELEASE nUCCx,nRef4x
							LOCATE
							IF lNoCount
								GO nCtnNumber
								cTrkNumber = ALLTRIM(package1.trknumber)
							ELSE
								LOCATE FOR  package1.ref4 = cUCC
								IF !FOUND()
									LOCATE FOR  package1.ucc = cUCC
									IF FOUND()
										cTrkNumber = ALLTRIM(package1.trknumber)
									ELSE
										=SEEK(cShip_ref,"shipment","pickticket")
										LOCATE FOR package1.shipmentid = shipment.shipmentid
										IF FOUND()
											cTrkNumber = ALLTRIM(package1.trknumber)
										ELSE
											DO ediupdate WITH "Missing TRK # at "+cShip_ref,.T.
											THROW
										ENDIF
									ENDIF
								ELSE
									cTrkNumber = ALLTRIM(package1.trknumber)
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					nCtnNumber = nCtnNumber + 1

					IF !EMPTY(cUCC) AND cUCC # "XXX"
						lDoManSegment = .F.
						cPRONum = IIF(lTesting,"999888777",ALLTRIM(outship.keyrec))
						IF lToys AND !EMPTY(cPRONum)
							STORE "MAN"+cfd+"GM"+cfd+TRIM(cUCC)+cfd+cfd+"ZZ"+cfd+cPRONum+csegd TO cString  && UCC Number (Wal-mart DSDC only)
						ELSE
							IF 	lParcelType
								IF EMPTY(cTrkNumber)
									cTrkNumber = cBOL
								ENDIF
								STORE "MAN"+cfd+"GM"+cfd+TRIM(cUCC)+cfd+cfd+"CP"+cfd+cTrkNumber+csegd TO cString
							ELSE
								STORE "MAN"+cfd+"GM"+cfd+TRIM(cUCC)+csegd TO cString  && UCC Number (Wal-mart DSDC only)
							ENDIF
						ENDIF
						DO cstringbreak
						nSegCtr = nSegCtr + 1
** now write out the saved inner and pack info that was uploaded from the 940, if we have the data........
						alength = ALINES(apt,outdet.printstuff,.T.,CHR(13))
						cPalPack  = ALLTRIM(segmentget(@apt,"W20_1",alength))
						cPalInner = ALLTRIM(segmentget(@apt,"W20_2",alength))
						cOrigWt = ALLTRIM(segmentget(@apt,"ORIG_WT",alength))
						cUOW = IIF(EMPTY(cOrigWt),"",ALLTRIM(segmentget(@apt,"ORIG_UOW",alength)))
						cOrigVol = ALLTRIM(segmentget(@apt,"ORIG_VOL",alength))
						cUOV = IIF(EMPTY(cOrigVol),"",ALLTRIM(segmentget(@apt,"ORIG_UOV",alength)))
*						STORE "PAL"+cfd+cfd+cfd+cfd+ALLTRIM(cPalPack)+cfd+cfd+cfd+cfd+cfd+cfd+cfd+cOrigWt+cfd+cUOW+cfd+cOrigVol+cfd+cUOV+cfd+cfd+ALLTRIM(cPalInner)+csegd TO cString
*						DO cstringbreak
*						nSegCtr = nSegCtr + 1
					ELSE
						WAIT CLEAR
						WAIT WINDOW "Empty UCC Number in vsynclairepp "+cShip_ref &cTimeMsg
						IF lTesting
							ASSERT .F. MESSAGE "EMPTY UCC...DEBUG"
							CLOSE DATABASES ALL
							=FCLOSE(nFilenum)
							ERASE &cFilename
							THROW
						ELSE
							cErrMsg = "EMPTY UCC# in "+cShip_ref
							DO ediupdate WITH cErrMsg,.T.
							=FCLOSE(nFilenum)
							ERASE &cFilename
							THROW
						ENDIF
					ENDIF
				ENDIF

				alength = ALINES(aptdet,outdet.printstuff,.T.)
				lEndAtSubqty = .T.

				FOR rrr = 1 TO alength
					arystr = ALLT(SUBSTR(UPPER(ALLTRIM(aptdet[rrr])),1))
					IF LEFT(ALLT(arystr),5) = "SIZE" &&AND !(">"$arystr))
						lEndAtSubqty = .F.
						EXIT
					ENDIF
				ENDFOR

				IF lTesting
					WAIT WINDOW "Length of APTDET: "+ALLTRIM(STR(alength)) NOWAIT
					WAIT CLEAR
				ENDIF
				cUPCCode = segmentget(@aptdet,"UPCCODE",alength)
				cColor = TRIM(outdet.COLOR)
				cColor = IIF(cColor = ALLTRIM(outship.cnee_ref),"",cColor)
				STORE ALLTRIM(outdet.ID) TO cSize
				cStyle = TRIM(outdet.STYLE)
				cUPC = IIF(EMPTY(ALLT(outdet.upc)),TRIM(vsynclairepp.upc),TRIM(outdet.upc))
				cGTIN = ""
				IF LEN(ALLTRIM(cUPC)) # 12
					IF cUPCCode = "UK"
						cGTIN = TRIM(outdet.upc)
					ENDIF
					cUPC = ""
				ENDIF
				cStyle = ALLTRIM(outdet.STYLE)

				cItemNum = segmentget(@aptdet,"CUSTSKU",alength)
				IF EMPTY(ALLT(cItemNum))
					cItemNum = ALLTRIM(outdet.custsku)
				ENDIF
				nDetQty = vsynclairepp.totqty
				nOrigQty = vsynclairepp.qty

				IF EMPTY(cItemNum) AND !lSplitPT
					SET STEP ON
					cErrMsg = "MISS ItNum, Style/ODID "+cStyle+"/"+ALLTRIM(STR(outdet.outdetid))
					DO ediupdate WITH cErrMsg,.T.
					=FCLOSE(nFilenum)
					ERASE &cFilename
					THROW
				ENDIF

				cUOM = segmentget(@aptdet,"UOM",alength)
				IF EMPTY(cUOM)
					cUOM = segmentget(@aptdet,"UNITCODE",alength)
				ENDIF
				lPrepack = IIF(cUOM = "EA",.F.,.T.)

				IF lTesting AND EMPTY(outdet.ctnwt)
					cCtnWt = IIF(lPrepack,"5","2")
					nTotCtnWt = nTotCtnWt + INT(VAL(cCtnWt))
				ELSE
					STORE ALLTRIM(STR(outdet.ctnwt)) TO cCtnWt
					nTotCtnWt = nTotCtnWt + outdet.ctnwt

					IF EMPTY(cCtnWt) OR outdet.ctnwt=0
						nCtnWt = outship.weight/outship.ctnqty
						cCtnWt = ALLTRIM(STR(nCtnWt))
						nTotCtnWt = nTotCtnWt + nCtnWt
						IF EMPTY(cCtnWt)
							cErrMsg = "MISS CTNWT "+TRANSFORM(vsynclairepp.outdetid)
							DO ediupdate WITH cErrMsg,.T.
							=FCLOSE(nFilenum)
							ERASE &cFilename
							THROW
						ENDIF
					ENDIF
				ENDIF

				IF nOrigQty=nDetQty
					nShipType = "CL"
				ELSE
					nShipType = "PR"
				ENDIF

				IF nDetQty>0
					lG69 = .F.
					nUnitSum = nUnitSum + nDetQty
					cUPC = IIF(lGap,"",cUPC)
					nOriqqty = INT(VAL(segmentget(@aptdet,"ORIGQTY",alength)))
					IF lPrepack AND ("SUBUPC"$outdet.printstuff OR "FULLDET"$outdet.printstuff)
						STORE "W12"+cfd+nShipType+cfd+ALLTRIM(STR(nOrigQty))+cfd+ALLTRIM(STR(nDetQty))+cfd+;
							ALLTRIM(STR(nOrigQty-nDetQty))+cfd+cUOM+cfd+cUPC+cfd+cUOM+cfd+cUPC+REPLICATE(cfd,2)+cCtnWt+cfd+"G"+cfd+cWeightUnit+csegd TO cString  && Eaches/Style
						DO cstringbreak
						nSegCtr = nSegCtr + 1
						cDesc = segmentget(@aptdet,"DESC",alength)
						IF !EMPTY(cDesc)
							lG69 = .T.
							STORE "G69"+cfd+cDesc+csegd TO cString
							DO cstringbreak
							nSegCtr = nSegCtr + 1
						ENDIF

						lenarydet = ALEN(aptdet,1)

						IF "FULLDET"$outdet.printstuff
							FOR irq = 1 TO lenarydet
								arystr = SUBSTR(UPPER(ALLTRIM(aptdet[irq])),1)
								IF "FULLDET"$arystr
									cSubCmt = ALLTRIM(SUBSTR(ALLTRIM(arystr),AT("*",arystr,1)+1))
									IF RIGHT(ALLTRIM(cSubCmt),4) = "****"
										cSubCmt = LEFT(cSubCmt,AT("*",cSubCmt,4)-1)
									ENDIF
									STORE cSubCmt+csegd TO cString
									DO cstringbreak
									nSegCtr = nSegCtr + 1
									RELEASE cSubCmt
								ELSE
									LOOP
								ENDIF
							ENDFOR
						ELSE
							CREATE CURSOR tempdetail (STYLE c(20),COLOR c(20),SIZE c(15),subqty i)
							FOR irq = 1 TO lenarydet
								arystr = SUBSTR(UPPER(ALLTRIM(aptdet[irq])),1)
								DO CASE
									CASE EMPTY(arystr)
										LOOP
									CASE LEFT(arystr,6)= "SUBUPC"
										cSubUPC = SUBSTR(ALLTRIM(arystr),AT("*",arystr)+1)
									CASE LEFT(arystr,6) = "SUBQTY"  && Write to cString at this point
										cSubqty = SUBSTR(ALLTRIM(arystr),AT("*",arystr)+1)
										IF lEndAtSubqty
											STORE "N9"+cfd+"UP"+cfd+cSubUPC+cfd+cSubqty+csegd TO cString
											DO cstringbreak
											nSegCtr = nSegCtr + 1
											LOOP
										ENDIF
									CASE LEFT(arystr,5) = "STYLE"
										cStyle = SUBSTR(ALLTRIM(arystr),AT("*",arystr)+1)
										cStyle = IIF(">"$cStyle,"",cStyle)
									CASE LEFT(arystr,5)= "COLOR"
										cColor = SUBSTR(ALLTRIM(arystr),AT("*",arystr)+1)
									CASE LEFT(arystr,4) = "SIZE"
										cSize = SUBSTR(ALLTRIM(arystr),AT("*",arystr)+1)
										cSize = IIF(">"$cSize,"",cSize)
										IF !lEndAtSubqty
											IF EMPTY(cSize)
												STORE "N9"+cfd+"UP"+cfd+cSubUPC+cfd+cSubqty+IIF(!EMPTY(cStyle),REPLICATE(cfd,4)+"IN>"+cStyle+">CL>"+cColor+csegd,csegd) TO cString
											ELSE
												STORE "N9"+cfd+"UP"+cfd+cSubUPC+cfd+cSubqty+IIF(!EMPTY(cStyle),REPLICATE(cfd,4)+"IN>"+cStyle+">CL>"+cColor+">SZ>"+cSize+csegd,csegd) TO cString
											ENDIF
											DO cstringbreak
											nSegCtr = nSegCtr + 1
										ENDIF
								ENDCASE
							ENDFOR
						ENDIF
					ELSE
						cPKPack = ""
						cPKPack = segmentget(@aptdet,"PKPACK",alength)
						cStyle = segmentget(@aptdet,"ORIGSTYLE",alength)
						IF cUPCCode = "UK" AND EMPTY(cUPC) AND !EMPTY(cGTIN) && GTIN data
							STORE "W12"+cfd+nShipType+cfd+ALLTRIM(STR(nOrigQty))+cfd+ALLTRIM(STR(nDetQty))+cfd+;
								ALLTRIM(STR(nOrigQty-nDetQty))+cfd+cUOM+cfd+cUPC+cfd+IIF(!EMPTY(cStyle),"VN","")+cfd+TRIM(cStyle)+;
								cfd+cfd+cCtnWt+cfd+"G"+cfd+cWeightUnit+REPLICATE(cfd,5)+IIF(!EMPTY(cItemNum),"IN","")+cfd+ALLTRIM(cItemNum)+cfd+cfd+cfd+cUPCCode+cfd+cGTIN+csegd TO cString  && Eaches/Style
						ELSE
							STORE "W12"+cfd+nShipType+cfd+ALLTRIM(STR(nOrigQty))+cfd+ALLTRIM(STR(nDetQty))+cfd+;
								ALLTRIM(STR(nOrigQty-nDetQty))+cfd+cUOM+cfd+cUPC+cfd+IIF(!EMPTY(cStyle),"VN","")+cfd+TRIM(cStyle)+;
								cfd+IIF(!EMPTY(cPKPack),ALLTRIM(outdet.PACK),"")+cfd+cCtnWt+cfd+"G"+cfd+cWeightUnit+REPLICATE(cfd,5)+IIF(!EMPTY(cItemNum),"IN","")+cfd+ALLTRIM(cItemNum)+csegd TO cString  && Eaches/Style
						ENDIF
						DO cstringbreak
						nSegCtr = nSegCtr + 1
						cDesc = segmentget(@aptdet,"DESC",alength)
						IF !EMPTY(cDesc) AND !lG69
							STORE "G69"+cfd+cDesc+csegd TO cString
							DO cstringbreak
							nSegCtr = nSegCtr + 1
						ELSE
							g69 = .F.
						ENDIF

					ENDIF

					cColor = segmentget(@aptdet,"ORIGCOLOR",alength)
					IF !EMPTY(cColor)
						STORE "N9"+cfd+"CL"+cfd+cColor+csegd TO cString
						DO cstringbreak
						nSegCtr = nSegCtr + 1
					ENDIF

					IF "%C"$cShip_ref
						cItemNum = segmentget(@aptdet,"ORIGSTYLE",alength)
						WAIT WINDOW cItemNum NOWAIT
					ENDIF
					IF !EMPTY(cItemNum)
						STORE "N9"+cfd+"IN"+cfd+cItemNum+csegd TO cString
						DO cstringbreak
						nSegCtr = nSegCtr + 1
					ENDIF

					cSize = segmentget(@aptdet,"ORIGSIZE",alength)
					IF !EMPTY(cSize)
						STORE "N9"+cfd+"SZ"+cfd+cSize+csegd TO cString
						DO cstringbreak
						nSegCtr = nSegCtr + 1
					ENDIF

					cBuyerNum = segmentget(@aptdet,"BUYERSIZE",alength)
					IF !EMPTY(cBuyerNum)
						STORE "N9"+cfd+"IZ"+cfd+cBuyerNum+csegd TO cString
						DO cstringbreak
						nSegCtr = nSegCtr + 1
					ENDIF

					IF cUOM = "AS"
						cInnerpack =  ""
						cInnerpack = segmentget(@aptdet,"INNERPACK",alength)
						IF !EMPTY(cInnerpack)
							STORE "QTY"+cfd+"38"+cfd+cInnerpack+cfd+cUOM+csegd TO cString
							DO cstringbreak
							nSegCtr = nSegCtr + 1
						ENDIF
					ENDIF
				ENDIF

				SKIP 1 IN vsynclairepp
			ENDDO
			lSkipBack = .T.
*			nCtnNumber = nCtnNumber + 1
		ENDDO

		IF lNewPT
			IF !lDoFirstRound
				LOOP
			ELSE
				lDoFirstRound = .F.
				IF !lSplitPT
					EndSEloop()
				ENDIF
			ENDIF
		ELSE
			EndSEloop()
		ENDIF

*************************************************************************
*2^  END DETAIL MATCHING SHIP_REF LOOP
*************************************************************************
		SELECT outship
		WAIT CLEAR
	ENDSCAN

*************************************************************************
*1^  END OUTSHIP MAIN LOOP
*************************************************************************

	DO close945
	=FCLOSE(nFilenum)

	tu("ackdata")

	USE IN tempack
	USE IN ackdata

	IF !lTesting
		SELECT EDI_TRIGGER
		DO ediupdate WITH "945 CREATED",.F.

		IF !USED("ftpedilog")
			SELECT 0
			USE F:\edirouting\ftpedilog ALIAS ftpedilog
			INSERT INTO ftpedilog (TRANSFER,ftpdate,filename,acct_name,TYPE) VALUES ("945-"+cCustname+"-"+cCustLoc,dt2,cFilename,UPPER(cCustname),"945")
			USE IN ftpedilog
		ENDIF
	ENDIF

	WAIT CLEAR
	WAIT WINDOW cCustFolder+" 945 Process complete..." NOWAIT &&TIMEOUT 1

*!* Create eMail confirmation message
	IF lTestmail
		tsendto = tsendtotest
		tcc = tcctest
	ENDIF

	cPTCount = ALLTRIM(STR(nPTCount))
	nPTCount = 0
	tsubject = cMailName+" 945 EDI File from TGF as of "+dtmail+" ("+cCustLoc+")"
	tattach = " "
	tmessage = "945 EDI Info from TGF for BOL# "+TRIM(cBOL)+CHR(13)
	tmessage = tmessage + "(File: "+cFileNameShort+")"+CHR(13)
	tmessage = tmessage + "(WO# "+cWO_NumStr+"), containing these "+cPTCount+" picktickets:"+CHR(13)
	tmessage = tmessage + cPTString + CHR(13)
	tmessage = tmessage + "has been created and will be transmitted ASAP."+CHR(13)+CHR(13)
	tmessage = tmessage + "If you have any questions, please eMail or call 732-750-9000 x217."

	IF lEmail
		DO FORM m:\dev\frm\dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
	ENDIF

	cFilenameArchive = UPPER("f:\ftpusers\"+cCustFolder+"\945OUT\945ARCHIVE\"+cCustPrefix+dt1+".txt")

	RELEASE ALL LIKE c_CntrlNum,c_GrpCntrlNum
	WAIT CLEAR
	WAIT WINDOW cMailName+" 945 EDI File output complete"+CHR(13)+"for BOL# "+TRIM(cBOL) &cTimeMsg
*!* Transfers files to correct output folders
	COPY FILE [&cFilenameHold] TO [&cFilenameArch]

**********************************************************************************************88
	IF lBCNYFilesout

		COPY FILE [&cFilenameHold] TO [&cFilenameOut]
		DELETE FILE [&cFilenameHold]
		SELECT temp945
		COPY TO "f:\3pl\data\temp945a.dbf"
		USE IN temp945
		SELECT 0
		USE "f:\3pl\data\temp945a.dbf" ALIAS temp945a
		SELECT 0
		USE "f:\3pl\data\pts_sent945.dbf" ALIAS pts_sent945
		APPEND FROM "f:\3pl\data\temp945a.dbf"
		USE IN pts_sent945
		USE IN temp945a
		DELETE FILE "f:\3pl\data\temp945a.dbf"
	ENDIF

*	asn_out_data()

	IF USED('parcel_carriers')
		USE IN parcel_carriers
	ENDIF
	WAIT WINDOW "Synclaire standard 945 process complete... Time: "+TRANSFORM(DATETIME()-StartTime)+" seconds" NOWAIT

	endtime=DATETIME()

CATCH TO oErr
	IF lDoCatch && AND !lBypass
		SET STEP ON
		lEmail = .F.
		lCloseOutput = .F.


		IF cErrMsg # "TOP LEVEL"
			DO ediupdate WITH cErrMsg,.T.
		ELSE
			DO ediupdate WITH ALLT(oErr.MESSAGE),.T.
		ENDIF


		tsubject = cCustname+" Error ("+TRANSFORM(oErr.ERRORNO)+") at "+TTOC(DATETIME())
		tattach  = ""
		tsendto = IIF(lTesting,tsendtotest,tsendtoerr)
		tcc= IIF(lTesting,tcctest,tccerr)

		IF lPGTesting
			tsendto="pgaidis@fmiint.com"
			tcc=""
		ENDIF

		tmessage = cCustname+" Error processing "+CHR(13)
		tmessage =tmessage+ "Try/CATCH Exeception Message:"+CHR(13)+;
			[  Error: ] + STR(oErr.ERRORNO) +CHR(13)+;
			[  LineNo: ] + STR(oErr.LINENO) +CHR(13)+;
			[  Message: ] + oErr.MESSAGE +CHR(13)+;
			[  Procedure: ] + oErr.PROCEDURE +CHR(13)+;
			[  Details: ] + oErr.DETAILS +CHR(13)+;
			[  StackLevel: ] + STR(oErr.STACKLEVEL) +CHR(13)+;
			[  LineContents: ] + oErr.LINECONTENTS+CHR(13)+;
			[  UserValue: ] + oErr.USERVALUE

		tsubject = "945 EDI Poller Error at "+TTOC(DATETIME())
		tattach  = ""
		tfrom    ="TGF EDI 945 Poller Operations <toll-transload-ops@tollgroup.com>"
		DO FORM m:\dev\frm\dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
	ENDIF
FINALLY
*RELEASE tsendto,tfrom,tsubject,tcc,tattach,tmessage
	WAIT CLEAR
	SET DELETED ON
	ON ERROR
ENDTRY


*** END OF CODE BODY

****************************
PROCEDURE close945
****************************
** Footer Creation

	STORE  "GE"+cfd+ALLTRIM(STR(nSTCount))+cfd+c_CntrlNum+csegd TO cString
*!*		IF lTesting
*!*			FPUTS(nFilenum,cString)
*!*		ELSE
	FWRITE(nFilenum,cString)
*!*		ENDIF

	STORE  "IEA"+cfd+"1"+cfd+PADL(c_CntrlNum,9,"0")+csegd TO cString
*!*		IF lTesting
*!*			FPUTS(nFilenum,cString)
*!*		ELSE
	FWRITE(nFilenum,cString)
*!*		ENDIF

	RETURN

****************************
PROCEDURE num_incr_isa
****************************

	IF !USED("serfile")
		USE ("f:\3pl\data\serial\BCNY-Synclaire945_serial") IN 0 ALIAS serfile
	ENDIF
	SELECT serfile
	IF lTesting AND lISAFlag
		nOrigSeq = serfile.seqnum
		lISAFlag = .F.
	ENDIF
	nISA_Num = serfile.seqnum
	c_CntrlNum = ALLTRIM(STR(serfile.seqnum))
	REPLACE serfile.seqnum WITH serfile.seqnum + 1 IN serfile
	SELECT outship
	RETURN

****************************
PROCEDURE num_incr_st
****************************

	IF !USED("serfile")
		USE ("f:\3pl\data\serial\"+cCustname+"945_serial") IN 0 ALIAS serfile
	ENDIF
	SELECT serfile
	IF lTesting AND lSTFlag
		nOrigGrpSeq = serfile.grpseqnum
		lSTFlag = .F.
	ENDIF
	c_GrpCntrlNum = ALLTRIM(STR(serfile.grpseqnum))
	REPLACE serfile.grpseqnum WITH serfile.grpseqnum + 1 IN serfile
	SELECT outship
	RETURN


****************************
PROCEDURE segmentget
****************************

	PARAMETER thisarray,lcKey,nLength

	FOR i = 1 TO nLength
		IF i > nLength
			EXIT
		ENDIF
		lnEnd= AT("*",thisarray[i])
		IF lnEnd > 0
			lcThisKey =TRIM(SUBSTR(thisarray[i],1,lnEnd-1))
			IF OCCURS(lcKey,lcThisKey)>0
				RETURN SUBSTR(thisarray[i],lnEnd+1)
				i = 1
			ENDIF
		ENDIF
	ENDFOR

	RETURN ""

****************************
PROCEDURE ediupdate
****************************
	PARAMETER cStatus,lIsError
	lDoCatch = .F.

	IF cStatus = "MISS LOADID"
		tcc = "maria.estrella@tollgroup.com,alma.navarro@tollgroup.com"
	ENDIF

	WAIT WINDOW  "In EDIUpdate" NOWAIT
	IF !lTesting AND !lTestSplits
		SELECT EDI_TRIGGER
		nRec = RECNO()
		LOCATE
		IF !lIsError
			IF lParcelLong
				REPLACE processed WITH .T.,proc945 WITH .T.,file945 WITH cFilename,;
					fin_status WITH cStatus,errorflag WITH .F.,when_proc WITH DATETIME() ;
					isa_num WITH cISA_Num ;
					FOR EDI_TRIGGER.bol = cBOL AND EDI_TRIGGER.ship_ref = cShip_ref AND INLIST(accountid,6520,6521,6522)
			ELSE
				REPLACE processed WITH .T.,proc945 WITH .T.,file945 WITH cFilename,;
					fin_status WITH cStatus,errorflag WITH .F.,when_proc WITH DATETIME() ;
					isa_num WITH cISA_Num ;
					FOR EDI_TRIGGER.bol = cBOL AND INLIST(accountid,6520,6521,6522)
			ENDIF
		ELSE
			IF lParcelLong
				REPLACE processed WITH .T.,proc945 WITH .F.,file945 WITH "", ;
					fin_status WITH cStatus,errorflag WITH .T. ;
					isa_num WITH cISA_Num ;
					FOR EDI_TRIGGER.bol = cBOL AND EDI_TRIGGER.ship_ref = cShip_ref AND INLIST(accountid,6520,6521,6522)
			ELSE
				REPLACE processed WITH .T.,proc945 WITH .F.,file945 WITH "", ;
					fin_status WITH cStatus,errorflag WITH .T. ;
					isa_num WITH cISA_Num ;
					FOR EDI_TRIGGER.bol = cBOL AND INLIST(accountid,6520,6521,6522)
			ENDIF
			WAIT WINDOW "" TIMEOUT 1
		ENDIF
		IF lCloseOutput
			=FCLOSE(nFilenum)
		ENDIF
	ENDIF

	IF lTestSplits
		REPLACE processed WITH .T. FOR EDI_TRIGGER.bol = cBOL AND accountid = nAcctNum
	ENDIF

	IF lIsError AND lEmail
		tsubject = "945 Error in SYNCLAIRE BOL "+TRIM(cBOL)+" (At PT "+cShip_ref+")"
		tattach = " "
		tsendto = tsendtoerr
		tcc = tccerr

		tmessage = "Generated from program: synclaire_945_create, within EDI BCNY Poller"
		tmessage = tmessage + CHR(13) + "945 Processing for BOL# "+TRIM(cBOL)+"(WO# "+cWO_Num+"), produced this error: (Office: "+cOffice+"): "+cStatus+CHR(13)+"Check EDI_TRIGGER and re-run"
		IF cStatus = "MISS PT"
			tmessage = tmessage + CHR(13) + "This error means that this PT exists in OUTSHIP, but not in the Pickpack table. Please add this WO to SQL."
		ENDIF
		IF lPGTesting
			tsendto="pgaidis@fmiint.com,TMARG@FMIINT.COM"
			tcc=""
		ENDIF

		DO FORM m:\dev\frm\dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
	ENDIF
	IF lIsError AND cStatus = "NO CONVERSION SCAC"
		tsubject = "SCAC Conversion Error for SYNCLAIRE BOL "+TRIM(cBOL)+" (At PT "+cShip_ref+")"
		tmessage = "The BOL/PT noted has no match in the 'bcny_scac_conv' conversion table for outship.SCAC = "+cShip_SCAC
		tmessage = tmessage+CHR(13)+"Please add this SCAC to the table and retrigger the BOL."
		tattach = " "
		tsendto = tsendtotodd
		tcc = tcctodd
		DO FORM m:\dev\frm\dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
	ENDIF

	IF USED('outship')
		USE IN outship
	ENDIF
	IF USED('outdet')
		USE IN outdet
	ENDIF

	IF USED('serfile')
		SELECT serfile
		IF lTesting
			REPLACE serfile.seqnum WITH nOrigSeq
			REPLACE serfile.grpseqnum WITH nOrigGrpSeq
		ENDIF
		USE IN serfile
	ENDIF

	IF USED('scacs')
		USE IN scacs
	ENDIF

	IF USED('mm')
		USE IN mm
	ENDIF

	IF !lTesting
		SELECT EDI_TRIGGER
		LOCATE
	ENDIF
ENDPROC

****************************
PROCEDURE cstringbreak
****************************
	cLen = LEN(ALLTRIM(cString))
*!*		IF lTesting
*!*			FPUTS(nFilenum,cString)
*!*		ELSE
	FWRITE(nFilenum,cString)
*!*		ENDIF
ENDPROC

****************************
PROCEDURE EndSEloop
****************************
	WAIT CLEAR
	WAIT WINDOW "Now creating Section Closure information" NOWAIT NOCLEAR
	IF lToys
		STORE "W03"+cfd+ALLTRIM(STR(nUnitSum))+cfd+ALLTRIM(STR(nTotCtnWt))+cfd+;
			"LB"+cfd+cCuFt+cfd+"CF"+csegd TO cString   && Units sum, Weight sum, Volume sum, carton count
	ELSE
		STORE "W03"+cfd+ALLTRIM(STR(nUnitSum))+cfd+ALLTRIM(STR(nTotCtnWt))+cfd+;
			"LB"+csegd TO cString   && Units sum, Weight sum, carton count
	ENDIF
	FWRITE(nFilenum,cString)

	STORE  "SE"+cfd+ALLTRIM(STR(nSegCtr+2))+cfd+PADL(c_GrpCntrlNum,9,"0")+csegd TO cString
	FWRITE(nFilenum,cString)
	nTotCtnWt = 0
	nTotCtnCount = 0
	nUnitSum = 0
ENDPROC

*****************************************************
PROCEDURE termsgen
*****************************************************
	cBOL = ALLTRIM(outship.bol_no)
	cShipType      =""
	cTrailerNumber =""
	cProNumber     =""
	cTerms         =""

	IF lParcelType
		cTrackNum = cBOL  && UPS Tracking Number
	ENDIF

	IF lTPAmazon
		cTerms = UPPER(segmentget(@apt,"PMT_TYPE",alength))
	ELSE
		IF USED('BL')
			USE IN BL
		ENDIF
		cTerms = "PP"
		IF !lTesting
			IF lUseSQLBL
				csq1 = [select * from bl where bol_no = ']+cBOL+[']
				xsqlexec(csq1,"bl1",,"wh")
				SELECT bl1
				LOCATE
				IF !EOF()
					cShip_SCAC    = IIF(!EMPTY(bl1.scac),bl1.scac,cShip_SCAC)
					cTrailerNumber= LEFT(ALLTRIM(bl1.trailer),10)
					cProNumber    = bl1.pronumber
					DO CASE
						CASE bl1.terms ="1"
							cTerms = "PP"
						CASE bl1.terms ="2"
							cTerms = "CC"
						OTHERWISE
							cTerms = ""
					ENDCASE
				ELSE
					IF !lParcelType
						cErrMsg = "MISSING BL Data"
						DO ediupdate WITH cErrMsg,.T.
						THROW
					ENDIF
				ENDIF
			ELSE
				USE (cUsefolder+"BL") IN 0 ALIAS BL
				SELECT BL
				LOCATE FOR bol_no = cBOL
				IF FOUND()
					cShip_SCAC    = IIF(!EMPTY(BL.scac) AND !lSCACConv,BL.scac,cShip_SCAC)
					cTrailerNumber= LEFT(ALLTRIM(BL.trailer),10)
					cProNumber    = BL.pronumber
					lCC  = .F.
					DO CASE
						CASE BL.terms ="1"
							cTerms = "PP"
						CASE BL.terms ="2"
							cTerms = "CC"
							lCC  = .T.
						OTHERWISE
							cTerms = ""
					ENDCASE
				ELSE
					IF !lParcelType
						cErrMsg = "MISSING BL Data"
						DO ediupdate WITH cErrMsg,.T.
						THROW
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF

	IF USED('BL')
		USE IN BL
	ENDIF
ENDPROC
