* essentialbrands832
* style import for a .csv file, not an edi 832 format
* author: Joe Bianchi? Possibly modified by Todd Margolin.
*
* built EXE as F:\3PL\PROJ\ESSBRANDS832.EXE 1/30/2018 MB to automate the style import.
* added whsql.vcx 1/30/2018 MB
*
* 4/24/2018 MB: changed fmiint.com to toll email.

#DEFINE CRLF CHR(13) + CHR(10)   && added 1/30/2018 MB


utilsetup("ESSBRANDS832")

*close data all
LOCAL llTesting

llTesting = .F.
*llTesting = .T.

SET SAFETY OFF

*set step on
zuserid=upper(getenv("USERNAME"))
lcpath = 'F:\FTPUSERS\ESSENTIALBRANDS\832IN\'
lcarchivepath = 'F:\FTPUSERS\ESSENTIALBRANDS\832IN\ARCHIVE\'
useca("upcmast","wh",,,"select * from upcmast where  accountid in (6769)")

len1 = adir(ary1,lcpath+"*.*")
if len1 = 0
	wait window "No files found...exiting" timeout 2
	close data all
	schedupdate()
	_screen.caption=gscreencaption
	on error
	return
endif
for i = 1 to len1
	cfilename = alltrim(ary1[i,1])
	xfile = lcpath+cfilename
	carchivefile = (lcarchivepath+cfilename)
	cloadfile = (lcpath+cfilename)
	copy file [&cLoadFile] to [&cArchiveFile]


	create cursor eb832 (company c(4), division c(4), season c(7),sku100 c(10), sku c(10),  sku100desc c(50), sku200 c(10), sku200desc c(50), sku300 c(10), sku300desc c(20), sku400 c(10), ;
		sku400desc1 c(30), sku400desc2 c(20), nrf c(10) , ppkunit n(4), cartonunit n(4) , sizescale c(5), size1 c(20), upc c(14), uom1 c(5),  wprice n(7,2), rprice n(7,2), cat c(5), group1 n(10), porh c(6),accountid i(4), info m(4), descrip c(50))


	tfile=alltrim(ary1[i,1])
	tfilepath=lcpath+alltrim(ary1[i,1])
	appe from "&tfilepath" type csv
	*set step on
	delete file "&tfilepath"
	delete for company='COMP'
	select  *, alltrim(sku100)+alltrim(sku200) as style, sku300 as color, sku400 as id, wprice as price, space(2) as uom from eb832 into cursor eb832_2 readwrite
************************************sku400 conversion if *N *************************************  look at size1
	replace uom with 'EA' for uom1='EA'
	replace uom with 'CA' for uom1 !='EA'
	replace accountid with 6769 for accountid=0
	
	* 10/11/2017 mb: ADDED THIS TO HANDLE THE 'OS' IN SIZE1 ROWS WHICH HAVE NOTHING IN THE CARTONUNIT COLUMN
	REPLACE CARTONUNIT WITH 1 FOR CARTONUNIT = 0
	
	
	store "" to m.info
	*set step on
	select eb832_2
	scan
*!*			scatter memvar
*!*			m.accountid=6769
*!*		endscan
*!*		scan
		m.info = "COMPANY**"+allt(upper(eb832_2.company))
		m.info = m.info+chr(13)+"DIVISION*"+allt(eb832_2.division)
		m.info = m.info+chr(13)+"SEASON*"+allt(eb832_2.season)
		m.info = m.info+chr(13)+"SKU100*"+trans(eb832_2.sku100)
		m.info = m.info+chr(13)+"SKU*"+allt(eb832_2.sku)
		m.info = m.info+chr(13)+"SKU100DESC*"+trans(eb832_2.sku100desc)
		m.info = m.info+chr(13)+"SKU200*"+transform(eb832_2.sku200)
		m.info = m.info+chr(13)+"SKU200DESC*"+transform(eb832_2.sku200desc)
		m.info = m.info+chr(13)+"SKU300*"+transform(eb832_2.sku300)
		m.info = m.info+chr(13)+"SKU300DESC*"+transform(eb832_2.sku300desc)
		m.info = m.info+chr(13)+"SKU400*"+transform(eb832_2.sku400)
		m.info = m.info+chr(13)+"SKU400DESC1*"+transform(eb832_2.sku400desc1)
		m.info = m.info+chr(13)+"SKU400DESC2*"+transform(eb832_2.sku400desc2)
		m.info = m.info+chr(13)+"NRF*"+(eb832_2.nrf)
		
		**********************************************************************************************************************************
		* 10/10/2017 MB - after discussing with Joe and Chris, use the CARTONUNIT value for PPKUNIT also, if CARTONUNIT value is >. 
		* Because PPKUNIT*1 was not allowing Chris to change the pack in the WMS; we think putting the larger # will work for ESS Brands.
		*m.info = m.info+chr(13)+"PPKUNIT*"+transform(eb832_2.ppkunit)
		IF eb832_2.cartonunit > eb832_2.ppkunit then
			m.info = m.info+chr(13)+"PPKUNIT*"+transform(eb832_2.cartonunit)
		ELSE
			m.info = m.info+chr(13)+"PPKUNIT*"+transform(eb832_2.ppkunit)
		ENDIF
		**********************************************************************************************************************************
		
		m.info = m.info+chr(13)+"CARTONUNIT*"+transform(eb832_2.cartonunit)
		m.info = m.info+chr(13)+"SIZESCALE*"+(eb832_2.sizescale)
		m.info = m.info+chr(13)+"SIZE1*"+(eb832_2.size1)
		m.info = m.info+chr(13)+"UOM1*"+(eb832_2.uom1)
		m.info = m.info+chr(13)+"CAT*"+(eb832_2.cat)
		m.info = m.info+chr(13)+"GROUP*"+transform(eb832_2.group1)
		m.info = m.info+chr(13)+"PORH*"+(eb832_2.porh)
		replace eb832_2.info with m.info in eb832_2
		replace descrip with sku100desc in eb832_2
	endscan
	replace id with size1 for id='*N'


	useca("upcmast","wh",,,"select * from upcmast where accountid = 6769")
	xsqlexec("select * from upcmast where accountid= 6769","xupcmast",,"wh")
	select xupcmast
	index on upc tag upc
	set order to

	*set step on
	select upcmast
	scatter memvar memo blank
	select eb832_2
	recctr=0
	scan
		scatter memvar memo
		recctr = recctr +1
		wait window at 10,10 "Checking Record # "+transform(recctr) nowait
		m.adddt=datetime()
		m.updatedt=datetime()
*		m.addby=zuserid
*		m.updateby=zuserid
		m.addproc='EB832'
		m.updproc='EB832'
		m.accountid=6769
		m.pnp = .T.
		if !empty(upc)

			if !seek(alltrim(eb832_2.upc),"xupcmast","upc")
				m.upcmastid=sqlgenpk("upcmast","wh")
				insert into upcmast from memvar
				release M.upcmastid
			else
				select upcmast
				locate for alltrim(upc)=alltrim(eb832_2.upc)
				if found() then
					replace style with eb832_2.style, ;
						color with alltrim(eb832_2.color), ;
						id with alltrim(eb832_2.id), ;
						rprice with (eb832_2.rprice), ;
						uom with eb832_2.uom, ;
						descrip with eb832_2.sku100desc, ;
						price with eb832_2.price, ;
						pnp with .T., ;
						info with eb832_2.info, ;
						updatedt with datetime() in upcmast
				endif
			endif
		endif
	endscan
	*set step on
	IF llTesting then
		SELECT upcmast
		BROWSE
	ELSE
		IF tu("upcmast") then
			WAIT WINDOW TIMEOUT 5 'TU() was successful'
		ELSE
			WAIT WINDOW TIMEOUT 5 '===> TU() failed!'
			tsendto = "mark.bennett@tollgroup.com"
			tattach = ""
			tcc =""
			tfrom ="toll-edi-ops@tollgroup.com"
			tmessage = "===> TU() failed!"
			tsubject = "Essential Brands Stylemaster TU() failed!"
			do form m:\dev\frm\dartmail2 with tsendto,tfrom,tsubject, tcc,tattach,tmessage  ,"A"
		ENDIF
	ENDIF
	
	select eb832_2
	if reccount() > 0
		export to "S:\Agegroup\temp\eb_style_master"  type xls
		IF llTesting then
			tsendto = "mark.bennett@tollgroup.com"
		else
			tsendto = "mark.bennett@tollgroup.com,ed@agegroupltd.com"
		endif
		tattach = "S:\Agegroup\temp\eb_style_master.xls"
		tcc =""
		tfrom ="Toll EDI System Operations <toll-edi-ops@tollgroup.com>"
		* added filename that was loaded 1/30/2018 MB
		*tmessage = "Essential Brands Stylemaster add/update_:"+ttoc(datetime()) 
		tmessage = "Essential Brands Stylemaster add/update: " + ttoc(datetime()) + CRLF + CRLF + "Loaded file " + cfilename		
		tsubject = "Essential Brands Stylemaster add/update"
		do form m:\dev\frm\dartmail2 with tsendto,tfrom,tsubject, tcc,tattach,tmessage  ,"A"

	else
	endif

endfor
close databases all
