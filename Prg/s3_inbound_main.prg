*!* S3 Inbound project, 01.24.2018, Joe

lTesting = .t.
lBrowfiles = lTesting

*SET STEP ON 
lcPath = "f:\ftpusers\s3\inboundpl\"
CD &lcPath
lcArchivePath = (lcPath+"archive\")

len1 = ADIR(ary1,"*.xlsx")
IF len1 = 0
	WAIT WINDOW "No S3 inbound files to process...exiting" TIMEOUT 2
	CLOSE DATA ALL
	CANCEL
ENDIF

FOR thisfile = 1 TO len1
	IF USED('tempmain')
		USE IN tempmain
	ENDIF

	cfilenamexx = (lcPath+"temp1.txt")
	IF FILE(cfilenamexx)
		DELETE FILE [&cFilenamexx]
	ENDIF

	cFilename = ALLTRIM(ary1[1,1])
	xfile = (lcPath+cFilename)
	cArchiveFile = (lcArchivePath+cFilename)

	COPY FILE [&xfile] TO [&cArchiveFile]

	CREATE CURSOR tempmain (a c(50), ;
		b c(30), ;
		c c(30), ;
		d c(50), ;
		e c(30), ;
		F c(30), ;
		g c(30), ;
		h c(30), ;
		i c(30), ;
		j c(30), ;
		k c(30), ;
		l c(30), ;
		m c(30), ;
		N c(30), ;
		o c(30), ;
		p c(30), ;
		q c(30), ;
		r c(30), ;
		s c(30), ;
		T c(30), ;
		u c(30), ;
		v c(30), ;
		w c(30), ;
		x c(30), ;
		Y c(30), ;
		z c(30), ;
		aa c(30), ;
		ab c(30))

	oExcel = CREATEOBJECT("Excel.Application")
	oWorkbook = oExcel.Workbooks.OPEN(xfile)
	WAIT WINDOW "Now saving file "+cFilename NOWAIT NOCLEAR
	oWorkbook.SAVEAS(cfilenamexx,-4158) && re-saves Excel sheet as Windows current text version file
	*oWorkbook.SAVEAS(cfilenamexx,20) && re-saves Excel sheet as text file
	*oWorkbook.SAVEAS(cfilenamexx,23) && re-saves Excel sheet as Windows CSV file
	*oWorkbook.SAVEAS(cfilenamexx,39)  && re-saves Excel sheet as XL95/97 file
	*oWorkbook.SAVEAS(cfilenamexx,56)  && re-saves Excel sheet as more XL8 file
	WAIT CLEAR
	WAIT WINDOW "File save complete...continuing" TIMEOUT 1
	oWorkbook.CLOSE(.T.)
	oExcel.QUIT()
	RELEASE oExcel

*	SET STEP ON
	SELECT tempmain
	DELETE ALL
	APPEND FROM [&cfilenamexx] TYPE DELIMITED WITH tab
*	APPEND FROM [&cfilenamexx] TYPE xl5
	LOCATE
	BROWSE
	cancel

	DO m:\dev\prg\s3_inbound_bkdn
ENDFOR

