

*******************************************************************************************************
* Import/process the 754 files from Regatta/J.C Penney - Courtaulds - Success
*
* The initial purpose is to update the Ship_Via field in Outship tables with the SCAC name
* gotten by looking up the SCAC code in the 754 edi files. We can use RRC field from
* the 754 edi files to join to the outship tables' RRC_NUM field for Updating Ship_Via.
*
* RRC = Routing Request Control number
* SCAC = Standard Carrier Alpha Code
*
* NOTE:  build exe ====> F:\EDIWHSE\PROJ\PROC754.EXE
*
*******************************************************************************************************

LPARAMETERS tcCompanyCode

runack("PROC754")

LOCAL loProcess754
*SET STEP ON
loProcess754 = CREATEOBJECT('MarkProcess754')
* the createobject should fail if Init() cannot access the log file,
* which means that the process is already running!
IF TYPE("loProcess754") = "O" AND (NOT ISNULL(loProcess754)) THEN
	loProcess754.SetCompanyCode(tcCompanyCode)
	loProcess754.MAIN()
ENDIF
RETURN

#DEFINE LOGIT 1
#DEFINE WAITIT 2
#DEFINE NOWAITIT 4
#DEFINE SENDIT 8
#DEFINE CRLF CHR(13) + CHR(10)
#DEFINE AGEGROUPCODE "AGEGROUP"
#DEFINE AGEGROUPNJCODE "AGEGROUPNJ"
#DEFINE REGATTACODE "K"
#DEFINE SARALEECODE "S"
#DEFINE SUCCESSCODE "U"
#DEFINE NANJINGCODE "N"
#DEFINE MORETMLCODE "MORETML"
#DEFINE MORETFLCODE "MORETFL"
#DEFINE MORETSPCODE "MORETSP"
#DEFINE TKONJCODE "TKONJ"
#DEFINE TKOCACODE "TKOCA"
#DEFINE TKOFLCODE "TKOFL"
#DEFINE PENJCODE "PENJ"
#DEFINE PECACODE "PECA"
#DEFINE PEFLCODE "PEFL"
#DEFINE DODENIMCACODE "DODENIMCA"
#DEFINE DODENIMNJCODE "DODENIMNJ"
#DEFINE JQUEENCACODE "JQUEENCA"
#DEFINE BIOWORLDCODE "BIOWORLD"
#DEFINE SAMSUNGCODE "SAMSUNG"

#DEFINE SUCCESSDELIMITER CHR(0x7F)

DEFINE CLASS MarkProcess754 AS CUSTOM

	cProcessName = 'MarkProcess754'

	lTestMode = .F.  && .F. FOR NORMAL OPERATION, .T. TO SUPPRESS ALL BUT DEVELOPER EMAILS AND CHANGE INPUT PATH, ETC.

	* file format properties
	lMoretFlatFileFormat = .T.  && THIS NOW GETS TESTED AND SET APPROPRIATELY FURTHER DOWN IN PROCESSING 3/15/11 M.B.
	lMatchRRCNums = .T.

	lAutoYield = .T.

	cStartTime = TTOC(DATETIME())

	* path properties -- defaults are for Regatta, will be changed if SaraLee is being processed
	cInPath = 'F:\FTPUSERS\Regatta\754\'
	cArchivePath = 'F:\FTPUSERS\Regatta\754\754 Archive\'
	cOriginalsPath = 'F:\FTPUSERS\Regatta\754\754 Originals\'

	cCompanyCode = ''
	* Note: companycode is passed in as a parameter to the EXE, specified in FTPSETUP.DBF.
	* company code key
	* [K] = Regatta/Kids International
	* [S] = Sara Lee
	* [U] = Success
	* [N] = Nanjing
	* [MORETML] = Moret Mira Loma
	
	cFLSendTo = ''
	cCASendTo = ''
	cNYSendTo = ''
	cMLSendTo = ''
	cTUGSendTo = ''

	*cSCACTable = 'f:\whn\whdata\scac'   && 10/17/2006 per Darren
	*cSCACTable = 'f:\wh\scac'

	cJCPDCTable = 'F:\3PL\DATA\JCPDCS'

	cNanjingTranslatePath = 'F:\FTPUSERS\NANJING\940TRANSLATE\'

	* swcnum / loadid properties
	lSwcNumLoadIDProblem = .F.
	cSwcNumLoadIDProblemList = ""
	
	* props used in Main & called procs
	cBLTable = ''

	* wait window properties
	nWaitWindowTimeout = 2
	lWaitWindowIsOn = .T.
	cWaitHeader = ""

	* logfile properties
	lLoggingIsOn = .T.
	cLogFile = 'F:\EDIRouting\Logfiles\Process754_log.txt'

	* PDF report properties
	cPDFFilename = "c:\tempfox\754_Routing_Report"
	*cPDFReportFRX = "M:\DEV\RPT\754rpt"
	cPDFReportFRX = "754rpt"
	
	* processing properties 
	nWoDateDaysBack = 40  && when scanning outship, how far back to look (or if in sql, how far back to select) MAY NEED TO MAKE THIS SMALLER IF THERE ARE SQL PERFORMANCE ISSUES.
	cMod = ""  && the mod currently being processed
	
	cCOMPUTERNAME = ''

	* INTERNAL email properties
	lSendInternalEmailIsOn = .T.
	cFrom = 'TGF Corporate <fmicorporate@fmiint.com>'
	cSendTo = 'pgaidis@fmiint.com, joe.bianchi@tollgroup.com, jrocio@fmiint.com, mbennett@fmiint.com'
	cCC = ''
	cSubject = '754 Import Process Results for: ' + TTOC(DATETIME())
	cAttach = ''
	cBodyText = ''

	* EXTERNAL email properties - these are defaults;
	*some like cClientSendTo & cClientAttach are region-specific
	lSendExternalEmailIsOn = .T.
	cClientFrom = 'TGF Corporate <fmicorporate@fmiint.com>'
	cClientSendTo = ''
	cClientCC = 'mbennett@fmiint.com'
	cClientSubject = '754 EDI Shipment Routing Report for: ' + TTOC(DATETIME())
	cClientAttach = ''
	cClientBodyText = 'FYI, see attached report.'

	* data validation properties
	lx754DataErrors = .F.
	lRecsNotFound = .F.
	lOtherErrors = .F.

	* 754 file properties
	dCreationDate = CTOD("")

	* WH office Table properties
	cWHOfficeTable = 'F:\WH\WHOFFICE'

	* arrays
	DIMENSION aOutShipTables(3,8)


	FUNCTION INIT
		IF NOT DODEFAULT()
			RETURN .F.
		ENDIF
		WITH THIS
			*SET RESOURCE OFF
			SET CENTURY ON
			SET ANSI ON
			SET TALK OFF
			SET DELETED ON
			SET CONSOLE OFF
			SET EXCLUSIVE OFF
			SET SAFETY OFF
			SET EXACT OFF
			SET STATUS BAR OFF
			SET SYSMENU OFF
			SET ENGINEBEHAVIOR 70
			SET MULTILOCKS ON  && needed by x3genpk
			_VFP.AUTOYIELD = .lAutoYield
			_SCREEN.CAPTION = "Process 754s"
			.cCOMPUTERNAME = GETENV("COMPUTERNAME")
			LOCAL llError
			llError = .F.
			IF .lTestMode THEN
				*.lSendExternalEmailIsOn = .F.
				.cInPath = 'F:\FTPUSERS\Moret-SP\754 Test\'
				.cSendTo = 'mbennett@fmiint.com'
				.cLogFile = 'F:\EDIRouting\Logfiles\Process754_log_TESTMODE.txt'
				.cNYSendTo = 'mbennett@fmiint.com'
				.cFLSendTo = 'mbennett@fmiint.com'
				.cCASendTo = 'mbennett@fmiint.com'
				.cMLSendTo = 'mbennett@fmiint.com'
				.cTUGSendTo = 'mbennett@fmiint.com'
				.cClientCC = 'mbennett@fmiint.com'
			ENDIF
			.lLoggingIsOn = .lLoggingIsOn AND NOT EMPTY(.cLogFile)
			IF .lLoggingIsOn THEN
				TRY
					SET ALTERNATE TO (.cLogFile) ADDITIVE
					SET ALTERNATE ON
				CATCH
					llError = .T.
					WAIT WINDOW TIMEOUT 1 "Could not access the logfile."
				ENDTRY
			ENDIF
			* don't instantiate object if we couldn't access the logfile,
			* which means the process is already running.
			IF llError THEN
				RETURN .F.
			ENDIF
		ENDWITH
	ENDFUNC


	FUNCTION DESTROY
		WITH THIS
			IF .lLoggingIsOn  THEN
				SET ALTERNATE OFF
				SET ALTERNATE TO
			ENDIF
		ENDWITH
		DODEFAULT()
	ENDFUNC


	FUNCTION MAIN

		LOCAL lc754Table, aOutShipTables[1,2], lnNumFiles, laFiles[1,5], laFilesSorted[1,6], i, j, k, l
		LOCAL lcArchivedFile, loError, lcOutShipTable, lnTally, lcOutShipDB, llPDFReportWasCreated, lc754File
		LOCAL lcPlace, lcPDFFilename, lcShipViaTable, lcCompanyCode, lcASNCode, lcApptTable, lcGENERATEPK
		LOCAL lcTestString, lcJCPDCTable, lnStoreNum, lcOriginalFile, lcDCNum, lcOutwologTable, lnApptCallID
		LOCAL lcNanjingTranslateFile, lcFieldDelimiter, lcShipIns, lcTestStringBL, lcEOLChar, lcFileString
		LOCAL lcMod, lcSQL, ln754Count, ldOldestWoDate, lcSQLFMTOldestWoDate, lcUpdateSQL
		
		PUBLIC goffice

		WITH THIS

			TRY

				PUBLIC gUserid, gProcess
				STORE "PROC754" TO gUserid, gProcess

				lcCompanyCode = .cCompanyCode

				lcJCPDCTable = .cJCPDCTable
				
				*SET STEP ON

				IF INLIST(lcCompanyCode,SARALEECODE,SUCCESSCODE,NANJINGCODE,MORETFLCODE,MORETMLCODE,MORETSPCODE,TKONJCODE,TKOCACODE,TKOFLCODE) OR ;
					INLIST(lcCompanyCode,PENJCODE,PECACODE,PEFLCODE,DODENIMCACODE,DODENIMNJCODE,JQUEENCACODE,AGEGROUPCODE,BIOWORLDCODE,SAMSUNGCODE) OR ;
					INLIST(lcCompanyCode,AGEGROUPNJCODE) THEN
					* open JCPenney DCs table
					USE (lcJCPDCTable) IN 0 AGAIN ALIAS JCPDCS
				ENDIF
				
				ldOldestWoDate = DATE() - .nWoDateDaysBack				
				.TrackProgress('ldOldestWoDate = ' + TRANSFORM(ldOldestWoDate), LOGIT+SENDIT)
				
				lcSQLFMTOldestWoDate = "{" + DTOC(ldOldestWoDate) + "}"
				.TrackProgress('lcSQLFMTOldestWoDate = ' + TRANSFORM(lcSQLFMTOldestWoDate), LOGIT+SENDIT)

				.SetUpShipTablesArray()

				* get files to process
				SET DEFAULT TO (.cInPath)

				DO CASE
*!*						CASE INLIST(lcCompanyCode,MORETFLCODE,MORETMLCODE) AND .lMoretFlatFileFormat
*!*							lnNumFiles = ADIR(laFiles,"H754*.*")
*!*						CASE INLIST(lcCompanyCode,MORETFLCODE,MORETMLCODE) AND (NOT .lMoretFlatFileFormat)
*!*							lnNumFiles = ADIR(laFiles,"*.edi")
					CASE INLIST(lcCompanyCode,AGEGROUPCODE,AGEGROUPNJCODE,SAMSUNGCODE)
						lnNumFiles = ADIR(laFiles,"*.EDI")
					CASE INLIST(lcCompanyCode,BIOWORLDCODE,MORETFLCODE,MORETMLCODE,MORETSPCODE) && added BIOWORLDCODE 6/7/2016 because they no longer end their files in .txt
						lnNumFiles = ADIR(laFiles,"*.*")
					CASE INLIST(lcCompanyCode,TKONJCODE,TKOCACODE,TKOFLCODE)
						lnNumFiles = ADIR(laFiles,"*.001")
					CASE INLIST(lcCompanyCode,DODENIMCACODE,DODENIMNJCODE)
						lnNumFiles = ADIR(laFiles,"*.edi")
					CASE INLIST(lcCompanyCode,JQUEENCACODE)
						lnNumFiles = ADIR(laFiles,"*.754")
					OTHERWISE
						lnNumFiles = ADIR(laFiles,"*.txt")
				ENDCASE

				IF lnNumFiles > 0
				
					* new code below 2/18/2016
					useca("apptcall","wh")  && grab apptcall with no records; later will blank memvars so we can insert from memvars without null errors

					.TrackProgress('Found ' + TRANSFORM(lnNumFiles) + ' files; starting 754 process.', LOGIT+SENDIT)

					* sort file list by date/time
					.SortArrayByDateTime(@laFiles, @laFilesSorted)

					* create cursor that holds bad SCAC codes
					.CreateSCACCodeCursor()

					* create cursor that holds bad SCAC names
					.CreateSCACNameCursor()

					* create cursor 'y754' that accumulates data for report phase
					.CreateY754Cursor()

					* create cursor for determining whether any swcnum has more than one associated loadid
					.CreateLoadIDCursor()

					* now process the files
					FOR i = 1 TO lnNumFiles
						lc754File = .cInPath + laFilesSorted[i,1]
						lcArchivedFile = .cArchivePath + laFilesSorted[i,1]
						lcOriginalFile = .cOriginalsPath + laFilesSorted[i,1]
						lcNanjingTranslateFile = .cNanjingTranslatePath + laFilesSorted[i,1]

						IF FILE(lc754File) THEN

							.cWaitHeader = "Input file: " + lc754File

							**********************************************************************************************
							**********************************************************************************************

							* Copy the file to the 'original' folder
							COPY FILE (lc754File) TO (lcOriginalFile)

							* Copy the file to the 'archived' folder
							COPY FILE (lc754File) TO (lcArchivedFile)

							* if Nanjing, copy to the translate folder, per PG 4/16/07 MB
							IF lcCompanyCode = NANJINGCODE THEN
								COPY FILE (lc754File) TO (lcNanjingTranslateFile)
							ENDIF

							* Delete file FROM 754In folder so it doesn't keep triggering new 754 procs while this one is running.
							IF FILE(lcOriginalFile) THEN

								DELETE FILE (lc754File)

								* And from here on, we will only use the copy in (lcOriginalFile)
				
								* if Moret, determine whether flat file format or edi format
								IF INLIST(lcCompanyCode,MORETFLCODE,MORETMLCODE,MORETSPCODE) THEN
									lcFileString = UPPER(LEFT(FILETOSTR(lcOriginalFile),255))
									IF (LEFT(lcFileString,3) = "ISA") AND ("ST" $ lcFileString) AND ("754" $ lcFileString) THEN
										* EDI format
										.lMoretFlatFileFormat = .F.
										.TrackProgress('Moret File Format = EDI', LOGIT+SENDIT)
									ELSE
										* not EDI, must be flat file
										.lMoretFlatFileFormat = .T.
										.TrackProgress('Moret File Format = FLAT', LOGIT+SENDIT)
									ENDIF
								ENDIF

								**********************************************************************************************
								**********************************************************************************************

								**********************************************************************************************
								**********************************************************************************************
								* examine 4th character of 1st line of file and make this the field delimiter
								* Per Paul G., 3/20/08 MB.
								lcFieldDelimiter = .GetFieldDelimiter(lcOriginalFile)
								lcEOLChar = .GetEOLChar(lcOriginalFile)

								**********************************************************************************************
								**********************************************************************************************

								DO CASE
									CASE INLIST(lcCompanyCode,AGEGROUPCODE,AGEGROUPNJCODE,BIOWORLDCODE,SAMSUNGCODE)
										* use generic conversion of EOL char to CR/LF
										IF .ConvertEOLCharToCRLF(lcOriginalFile,lcEOLChar) THEN
											* nothing
										ELSE
											.TrackProgress('!!!**** Error converting EOL char to CR/LF in file: ' + lcOriginalFile, LOGIT+SENDIT)
											.lOtherErrors = .T.
										ENDIF
									CASE INLIST(lcCompanyCode,SUCCESSCODE)
										IF .ConvertForSuccess(lcOriginalFile) THEN
											.TrackProgress('Converting B1h to 15h for file: ' + lcOriginalFile, LOGIT+SENDIT)
										ELSE
											.TrackProgress('!!!**** Error converting B1h in file: ' + lcOriginalFile, LOGIT+SENDIT)
											.lOtherErrors = .T.
										ENDIF
									CASE INLIST(lcCompanyCode,MORETFLCODE,MORETMLCODE,MORETSPCODE) AND (NOT .lMoretFlatFileFormat)
										IF .ConvertForMoret(lcOriginalFile,lcEOLChar) THEN
											* nothing
										ELSE
											.TrackProgress('!!!**** Error converting Moret file: ' + lcOriginalFile, LOGIT+SENDIT)
											.lOtherErrors = .T.
										ENDIF
									CASE INLIST(lcCompanyCode,JQUEENCACODE) 
										IF .ConvertForJQUEEN(lcOriginalFile) THEN
											.TrackProgress('Converting 85h to Line Feeds for file: ' + lcOriginalFile, LOGIT+SENDIT)
										ELSE
											.TrackProgress('!!!**** Error converting 85h in file: ' + lcOriginalFile, LOGIT+SENDIT)
											.lOtherErrors = .T.
										ENDIF
									CASE INLIST(lcCompanyCode,SARALEECODE)
										* convert CHR(13) TO CHR(13) + CHR(10) in 754 file if Sara Lee
										IF .ADDLINEFEEDS(lcOriginalFile) THEN
											.TrackProgress('Added Line Feeds to file: ' + lcOriginalFile, LOGIT+SENDIT)
										ELSE
											.TrackProgress('!!!**** Error adding Line Feeds to file: ' + lcOriginalFile, LOGIT+SENDIT)
											.lOtherErrors = .T.
										ENDIF
									CASE INLIST(lcCompanyCode,NANJINGCODE)
										* Convert 85h to Line Feeds if Nanjing
										IF .ConvertForNanjing(lcOriginalFile) THEN
											.TrackProgress('Doing ConvertForNanjing() for file: ' + lcOriginalFile, LOGIT+SENDIT)
										ELSE
											.TrackProgress('!!!**** Error doing ConvertForNanjing() in file: ' + lcOriginalFile, LOGIT+SENDIT)
											.lOtherErrors = .T.
										ENDIF

									CASE INLIST(lcCompanyCode,PENJCODE,PECACODE,PEFLCODE)
										IF .ConvertForPerryEllis(lcOriginalFile) THEN
											.TrackProgress('Did ConvertForPerryEllis() for file: ' + lcOriginalFile, LOGIT+SENDIT)
										ELSE
											.TrackProgress('!!!**** Error doing ConvertForPerryEllis() in file: ' + lcOriginalFile, LOGIT+SENDIT)
											.lOtherErrors = .T.
										ENDIF
										
									CASE INLIST(lcCompanyCode,TKONJCODE,TKOCACODE,TKOFLCODE)
										IF .ConvertForTKO(lcOriginalFile) THEN
											.TrackProgress('Did ConvertForTKO() for file: ' + lcOriginalFile, LOGIT+SENDIT)
										ELSE
											.TrackProgress('!!!**** Error doing ConvertForTKO() in file: ' + lcOriginalFile, LOGIT+SENDIT)
											.lOtherErrors = .T.
										ENDIF
										
									CASE INLIST(lcCompanyCode,DODENIMCACODE,DODENIMNJCODE)
										IF .ConvertForDODENIM(lcOriginalFile) THEN
											.TrackProgress('Did ConvertForDODENIM() for file: ' + lcOriginalFile, LOGIT+SENDIT)
										ELSE
											.TrackProgress('!!!**** Error doing ConvertForDODENIM() in file: ' + lcOriginalFile, LOGIT+SENDIT)
											.lOtherErrors = .T.
										ENDIF
										
									OTHERWISE
										*nothing
								ENDCASE

								IF USED('x856')
									USE IN x856
								ENDIF
								IF USED('x754')
									USE IN x754
								ENDIF


								DO CASE
									CASE INLIST(lcCompanyCode,MORETFLCODE,MORETMLCODE,MORETSPCODE) AND .lMoretFlatFileFormat
										* nothing to do -- new format 754 will not be loaded into x856 array
										.TrackProgress('New Moret file format -- skipping x856 array processing', LOGIT)
									OTHERWISE
										.TrackProgress('About to create x856 array & cursor', LOGIT)
										* create x856 array & cursor
										DO M:\DEV\prg\createx856
										.TrackProgress('About to LOAD 754 file into cursor x856', LOGIT)
								ENDCASE

								DO CASE
									CASE INLIST(lcCompanyCode,SUCCESSCODE)
										*DO M:\DEV\prg\loadedifile WITH lcOriginalFile, SUCCESSDELIMITER, "SUCCESS", "UNK"
										DO M:\DEV\prg\loadedifile WITH lcOriginalFile, lcFieldDelimiter, "SUCCESS", "UNK"
									CASE INLIST(lcCompanyCode,MORETFLCODE,MORETMLCODE,MORETSPCODE) AND .lMoretFlatFileFormat
										.TrackProgress('New Moret file format -- skipping loadedifile processing', LOGIT)
									OTHERWISE
										* LOAD 754 file into cursor x856
										* use the determined field delimiter per PG 3/20/08 MB
										*DO M:\DEV\prg\loadedifile WITH lcOriginalFile, CHR(7), "NONE", "UNK"
										DO M:\DEV\prg\loadedifile WITH lcOriginalFile, lcFieldDelimiter, "NONE", "UNK"
								ENDCASE
								
*!*	SELECT X856
*!*	BROWSE

								.TrackProgress('About to create 754 cursor with RRC_Num, SCAC, and SCAC Name fields', LOGIT)
								* create 754 cursor with RRC_Num, SCAC, and SCAC Name fields

								* create file-specific data cursor x754
								.CreateX754Cursor()
								SELECT x754

								*DO F:\ediwhse\prg\LOAD754 WITH .cSCACTable
								DO CASE
									CASE INLIST(lcCompanyCode,MORETFLCODE,MORETMLCODE,MORETSPCODE) AND .lMoretFlatFileFormat
										.LoadMoretFlatFileIntoX754Cursor( lcOriginalFile )
									OTHERWISE
										.Load754intoCursor()
								ENDCASE
								
*!*									IF .lTestMode THEN
*!*										SELECT x754
*!*										COPY TO C:\TEMPFOX\X754.XLS XL5
*!*										THROW
*!*									ENDIF

								*!*	                IF INLIST(lcCompanyCode,MORETMLCODE) AND .lMoretFlatFileFormat THEN
								*!*	                  CLOSE DATABASES ALL
								*!*	                  X = 2 * .F.
								*!*	                ENDIF

								*!*			SELECT x754
								*!*			GOTO TOP
								*!*			BROWSE

								.TrackProgress('754 File Creation Date = [' + DTOC(.dCreationDate) + "]", LOGIT)

								* update Ship_via in the Outship Tables
								* !! CURRENTLY WE ARE UPDATING ALL REGIONAL TABLES
								* !! BECAUSE WE DON'T KNOW WHICH EACH 754 RRC_NUM/PO_NUM pair APPLIES TO.
								* Should not be a problem as long as RRC_NUM/PO_NUM pairs are unique across all tables.

								*!*	IF .lTestMode THEN
								*!*		SET STEP ON
								*!*	ENDIF
								
								
								.TrackProgress('About to update Ship_via in the Outship Tables', LOGIT)
								FOR k = 1 TO ALEN(.aOutShipTables,1)
									lcOutShipTable = .aOutShipTables[k,1]
									lcOutShipDB = .aOutShipTables[k,2]
									lcPlace = .aOutShipTables[k,3]
									*!*										lcShipViaTable = .aOutShipTables[k,5]
									lcApptTable = .aOutShipTables[k,6]
									lcGENERATEPK = .aOutShipTables[k,7]
									lcOutwologTable = .aOutShipTables[k,8]
									.cBLTable = .aOutShipTables[k,10]

									lcMod = UPPER(ALLTRIM(.aOutShipTables( k, 12)))
									.cMod = lcMod
									goffice = lcMod
									.TrackProgress('Curent Mod = ' + .cMod, LOGIT+SENDIT)
									
									**************************************************************
									* block added 9/12/2016 MB
									* close most aliases that were opened in the prior loop iteration; they will be reopened for the current db
									IF USED('GENERATEPK') THEN
										USE IN GENERATEPK
									ENDIF
									IF USED('Outwolog') THEN
										USE IN Outwolog
									ENDIF
									IF USED('bltable') THEN
										USE IN bltable
									ENDIF
									IF USED('OutShipTable') THEN
										USE IN OutShipTable
									ENDIF
									**********************************************************

									* open db for table to be updated
									OPEN DATABASE (lcOutShipDB) SHARED

									*!*										* open shipvia table
									*!*										USE (lcShipViaTable) IN 0 AGAIN ALIAS ShipViaTable


									* open GenPk table, needed for inserting into Apptcall.dbf
									USE (lcGENERATEPK) IN 0 AGAIN ALIAS GENERATEPK
									
									** open Appointment table, 09/10/04 MB per Juan
									** SQL version now opened once at beginning of processing 9/12/2016 MB
									*USE (lcApptTable) IN 0 AGAIN ALIAS AppointmentTable

									*!*	* update Outship table with info from x754 AND ALSO TAKE INFO FROM OUTSHIP TABLE FOR REPORT
									*!*	USE (.cBLTable) IN 0 AGAIN ALIAS BLTable
									* BL now in SQL
									*SET STEP ON 

									lcSQL = "select blid, mod, scac, ship_via, bol_no from bl where mod='" + lcMod + "'"
									.TrackProgress('BL lcSQL = ' + lcSQL, LOGIT+SENDIT)
									xsqlexec(lcSQL,"bltable",,"wh")
									SELECT bltable
									INDEX ON bol_no TAG bol_no

 									
									* update Outship table with info from x754 AND ALSO TAKE INFO FROM OUTSHIP TABLE FOR REPORT
									* select from sql outship for mod and recent wo_dates
									* result needs to be indexed, so we will *not* use a Cursor Adaptor,
									* and we will later update the table using sql Update commands.
									*xsqlexec("select * from outship where mod='" + .cMod + "' and wo_date>{" + DTOC(ldOldestWoDate) + "}","OutShipTable",,"wh")
									xsqlexec("select outshipid, wo_date, bol_no, rrc_num, cnee_ref, ship_ref, dcnum, shipins, swcnum, outwologid, accountid, ctnqty, wo_num, consignee, appt_time from outship where mod='" + .cMod + "' and wo_date>{" + DTOC(ldOldestWoDate) + "}","OutShipTable",,"wh")
										
									SELECT OutShipTable
									INDEX ON rrc_num TAG rrc_num
									INDEX ON cnee_ref TAG cnee_ref
									INDEX ON bol_no TAG bol_no
									INDEX ON LEFT(ship_ref,6) TAG ship_ref
									INDEX ON accountid TAG accountid
									set order to
									
									SELECT OutShipTable


									lnTally = 0
									SELECT x754
									
									****************************************************************************
									** added block 9/12/2016
									COUNT TO ln754Count FOR EMPTY(placecode)
									.TrackProgress('ln754Count = ' + TRANSFORM(ln754Count), LOGIT+SENDIT)
									IF ln754Count = 0 THEN
										* we are done; we don't want/need to keep checking data for all of the remaining offices
										EXIT FOR
									ENDIF

									SELECT x754
									****************************************************************************
								
*!*	SELECT x754
*!*	BROWSE

									********************************************************************************************************
									* 11/19/03 MB
									* scan for empty placecode will speed processing because placecode is filled
									* inside scan when a hit is found. Best improvement (#recs * 1 scan) is when all recs hit in 1st
									* outship table scanned, so entire 2nd and 3rd scans are eliminated. Conversely, would see no improvement
									* if all recs hit in last outship table scanned (#recs * 3 scans). But on average would expect to
									* see (#recs * 2 scans), which is 1/3 faster than unconditional scan.
									*SCAN
									SET EXACT OFF
									SCAN FOR EMPTY(placecode)
										********************************************************************************************************
										.TrackProgress('Processing RRC_NUM: ' + x754.rrc_num + ' in ' + .aOutShipTables[k,1], NOWAITIT)

										IF .x754RecordDataIsOkay() THEN

											SELECT OutShipTable

											DO CASE
												CASE INLIST(lcCompanyCode,MORETFLCODE,MORETMLCODE,MORETSPCODE) AND .lMoretFlatFileFormat

													* for moret flat file, they do not provide po_num, so try to match by rrc_num and pick ticket (ship_ref)
													*SCAN FOR (wo_date > DATE()-30) AND OutShipTable.rrc_num = x754.rrc_num AND OutShipTable.ship_ref = x754.ship_ref

													* Only look at left 6 chars of Ship_ref per Paul G. 10/31/08 -- because we have a FMI-generated suffix after
													* the first 6 chars that client is not sending in 754
													SCAN FOR (wo_date > ldOldestWoDate) AND OutShipTable.rrc_num = x754.rrc_num ;
															AND LEFT(OutShipTable.ship_ref,6) = ALLTRIM(x754.ship_ref)

														*!*	* special processing per Paul G 05/12/08 for when rrc-nums get messed up.
														* try to match by wo and rrc
														*SCAN FOR (wo_date > DATE()-30) AND INLIST(wo_num,8024687) AND OutShipTable.rrc_num = x754.rrc_num
														
														* try to match by wo and left(ship_ref,6)
														*SCAN FOR (wo_date > DATE()-30) AND INLIST(wo_num,8024687) AND LEFT(OutShipTable.ship_ref,6) = ALLTRIM(x754.ship_ref)
														
														* AND LEFT(OutShipTable.ship_ref,6) = ALLTRIM(x754.ship_ref) ;
														  &&	AND CANCEL = {^2008-11-11}

														* DO SARA LEE CUSTOM PROCESSING --
														* CHANGE OUTSHIP DC NUM & ADDRESS IF NEW DC NUM IS DIFFERENT FROM OUTSHIP

														.TrackProgress("", LOGIT+SENDIT+NOWAITIT)
														.TrackProgress("SSNM processing Outship, RRC = " + TRANSFORM(OutShipTable.rrc_num) + ", ShipRef = " + TRANSFORM(OutShipTable.ship_ref), LOGIT+SENDIT+NOWAITIT)

														* Update Outship table
														*lnStoreNum = VAL(x754.storenum)
														lcDCNum = PADR(ALLTRIM(x754.storenum),10,' ')

														*IF (OutShipTable.storenum = lnStoreNum) AND (OutShipTable.dcnum = lcDCNum) THEN
														IF (OutShipTable.dcnum = lcDCNum) THEN
															* no change -- nothing to do
															.TrackProgress('Outship already had 754 DC Num = ' + lcDCNum, LOGIT+SENDIT+NOWAITIT)
														ELSE
															* the DC changed, update it and its address fields in Outship table
															* first look up the new storenum in JCPDC table
															SELECT JCPDCS
															* 9/28/07 MB PER PAUL/CHRIS; NOW USING CHAR DCNUM FOR THIS SEARCH
															*LOCATE FOR storenum = lnStoreNum
															* added replace of outship.consignee 4/1/08 MB per Paul.
															LOCATE FOR ALLTRIM(dcnum) == ALLTRIM(lcDCNum)
															IF FOUND() THEN
																IF .lTestMode THEN
																	lcTestString = 'TEST MODE: would update ' + lcOutShipTable + ' with: ' + CRLF + ;
																		' dcnum, storenum = ' + lcDCNum + ;
																		' consignee, name = ' + ALLTRIM(JCPDCS.NAME) + ;
																		'  address = ' + TRANSFORM(JCPDCS.address) + ;
																		'  csz = ' + ;
																		(ALLTRIM(JCPDCS.CITY) + ", " + ALLTRIM(JCPDCS.STATE) + " " + ALLTRIM(JCPDCS.ZIP))

																ELSE
																	* update outship table.
																	* added replaces of outship.name, outship.storenum per PG 4/9/08 MB
																	lcShipIns = ALLTRIM(OutShipTable.shipins)
																																		
																	.UpdateSQLOutshipWithDCInfo(lcDCNum, lcShipIns)

																	lcTestString = 'Updated ' + lcOutShipTable + ' with: ' + CRLF + ;
																		' dcnum, storenum = ' + lcDCNum + ;
																		' consignee, name = ' + ALLTRIM(JCPDCS.NAME) + ;
																		'  address = ' + TRANSFORM(JCPDCS.address) + ;
																		'  csz = ' + ;
																		(ALLTRIM(JCPDCS.CITY) + ", " + ALLTRIM(JCPDCS.STATE) + " " + ALLTRIM(JCPDCS.ZIP))
																ENDIF

																.TrackProgress(lcTestString, LOGIT+SENDIT)
															ELSE
																.TrackProgress("!!*** ERROR: new DC Num " + lcDCNum + ;
																	' not found in ' + lcJCPDCTable, LOGIT+SENDIT+NOWAITIT)
																	.lOtherErrors = .T.
															ENDIF  && FOUND()

															SELECT JCPDCS
															LOCATE
														ENDIF  &&  (OutShipTable.dcnum = lcDCNum)

														* Update Outship table with appointment info
														* do not replace ASN codes per Juan 01/09/06 MB
														IF .lTestMode THEN
															lcTestString = 'TEST MODE: would update ' + lcOutShipTable + ' with: ' + CRLF + ;
																' ship_via = ' + TRANSFORM(x754.scacname) +  ;
																'  scac = ' + TRANSFORM(x754.scaccode) +  ;
																'  CID = ' + TRANSFORM(x754.LoadID) + ;
																'  BATCH_NUM = ' + TRANSFORM(x754.LoadID) + ;
																'  APPT = ' + TRANSFORM(x754.PkupDate)

														ELSE
*!*																IF usesqloutwo(.cMod) THEN
*!*																
*!*																	.UpdateSQLOutshipWithApptInfo()
*!*																	
*!*																ELSE
*!*																	* foxpro
*!*																	REPLACE OutShipTable.ship_via WITH x754.scacname, ;
*!*																		OutShipTable.scac WITH x754.scaccode, ;
*!*																		OutShipTable.CID WITH x754.LoadID, ;
*!*																		OutShipTable.BATCH_NUM WITH x754.LoadID, ;
*!*																		OutShipTable.APPT WITH x754.PkupDate, ;
*!*																		OutShipTable.CALLED WITH .dCreationDate, ;
*!*																		OutShipTable.APPT_NUM WITH x754.LoadID, ;
*!*																		OutShipTable.APPTREMARKS WITH "754 Received" ;
*!*																		IN OutShipTable
*!*																ENDIF

*!*																lcTestString = 'Updated ' + lcOutShipTable + ' with: ' + CRLF + ;
*!*																	' ship_via = ' + TRANSFORM(x754.scacname) +  ;
*!*																	'  scac = ' + TRANSFORM(x754.scaccode) +  ;
*!*																	'  CID = ' + TRANSFORM(x754.LoadID) + ;
*!*																	'  BATCH_NUM = ' + TRANSFORM(x754.LoadID) + ;
*!*																	'  APPT = ' + TRANSFORM(x754.PkupDate)
														ENDIF
																
														.UpdateBLTableSCAC(lcMod)
																
														.TrackLoadIDs(OutShipTable.wo_num, OutShipTable.swcnum, x754.LoadID, OutShipTable.ship_ref)
														.TrackProgress(lcTestString, LOGIT+SENDIT+NOWAITIT)

														**************************************************************************************************
														* update Outwolog per Darren 3/27/06 MB
														* Applies to all customers.
																	
														.UpdateSQLOutwolog()

														**************************************************************************************************

														* Add record to Appointment table
														* NOTE: not inserting appt_time because that's not populated in JC Penney or Saralee 754 files,
														* field = 754.pkuptime.
														* Added insert of outwologid, wo_num, accountid 3/9/06 MB per Darren
														IF .lTestMode THEN
															.TrackProgress('TEST MODE: would add rec to Appt Table here.', LOGIT+NOWAITIT)
														ELSE
														
															* must modify for SQL apptcall per Darren 2/16/2016														
														
															* old code
															*!*	lnApptCallID = genpk("apptcall","wh")

															*!*	INSERT INTO AppointmentTable ( ;
															*!*		ApptCallID, ;
															*!*		outwologid, ;
															*!*		wo_num, ;
															*!*		accountid, ;
															*!*		outshipid, ;
															*!*		CALLED, ;
															*!*		APPT, ;
															*!*		APPT_NUM, ;
															*!*		remarks, ;
															*!*		addby, ;
															*!*		adddt) ;
															*!*		VALUES ( ;
															*!*		lnApptCallID, ;
															*!*		OutShipTable.outwologid, ;
															*!*		OutShipTable.wo_num, ;
															*!*		OutShipTable.accountid, ;
															*!*		OutShipTable.outshipid, ;
															*!*		.dCreationDate, ;
															*!*		x754.PkupDate, ;
															*!*		x754.LoadID, ;
															*!*		"754 Received", ;
															*!*		"PROC754", ;
															*!*		DATETIME())
																
																
*!*																* new code supporting sql apptcall below 2/18/2016
*!*																SELECT apptcall
*!*																SCATTER MEMVAR MEMO BLANK
*!*																
*!*																m.outwologid = OutShipTable.outwologid
*!*																m.wo_num = OutShipTable.wo_num
*!*																m.accountid = OutShipTable.accountid
*!*																m.outshipid = OutShipTable.outshipid
*!*																m.CALLED = .dCreationDate
*!*																m.APPT = x754.PkupDate
*!*																m.APPT_NUM = x754.LoadID
*!*																m.remarks = "754 Received"
*!*																m.addby = "PROC754"
*!*																m.addproc = "PROC754"
*!*																m.adddt = DATETIME()
*!*																*m.updatedt = DATETIME()
*!*																m.Office = .aOutShipTables( k, 11)  && i.e. Rate Office in whoffice
*!*																m.Mod = .aOutShipTables( k, 12)  && i.e. office in whoffice

*!*																m.ApptCallID = dygenpk("apptcall","whall")
*!*																
*!*																INSERT INTO APPTCALL FROM MEMVAR
*!*																
*!*																.TrackProgress('Did SQL INSERT into APPTCALL for wo_num = ' + TRANSFORM(m.wo_num) + ', APPT_NUM = ' + TRANSFORM(m.APPT_NUM), LOGIT+SENDIT)
																
														ENDIF

														* Accumulate data in x754/y754 for the reporting phase
														* We're putting the Place Code into x754/y754 so we know which
														* regional outship table this record updated. Later that will drive
														* the data in the reports we send to each regional contact.
														REPLACE x754.qty WITH OutShipTable.ctnqty, ;
															x754.pt WITH OutShipTable.ship_ref, ;
															x754.cnee_ref WITH OutShipTable.cnee_ref, ;
															x754.wo_num WITH OutShipTable.wo_num, ;
															x754.wo_date WITH OutShipTable.wo_date, ;
															x754.consignee WITH OutShipTable.consignee, ;
															x754.ship_ref WITH OutShipTable.ship_ref, ;
															x754.placecode WITH lcPlace ;
															IN x754

														lnTally = lnTally + 1

														* Create y754 record. There can be many y754 records per single x754 record.
														SELECT x754
														SCATTER MEMVAR
														SELECT y754
														APPEND BLANK
														GATHER MEMVAR
													ENDSCAN  && inner outship scan
													
												CASE INLIST(lcCompanyCode,SAMSUNGCODE)

													* we don't have rc_nums in outship tables for samsung, so PG suggested also checking accountid
													SCAN FOR (wo_date > ldOldestWoDate) AND OutShipTable.accountid = 6652 AND OutShipTable.cnee_ref = x754.po_num
													
														* REGATTA is no longer a client.
														*!*	lcASNCode = SPACE(2)
														*!*	DO CASE
														*!*	CASE INLIST(lcCompanyCode,REGATTACODE)

														*!*	* populate outship.ASNcode from the regional shipvia table
														*!*	SELECT ShipViaTable
														*!*	* look for code by matching SCACnames, for particular company.
														*!*	LOCATE FOR ShipViaTable.account == lcCompanyCode AND ShipViaTable.shipvia == x754.scacname
														*!*	IF FOUND() THEN
														*!*		lcASNCode = ALLTRIM(ShipViaTable.CODE)
														*!*	ELSE
														*!*		.RememberBadSCACName(lcCompanyCode, x754.scacname, lcShipViaTable)
														*!*	ENDIF
														*!*	* doing a locate here because when not found(), being at eof() was causing
														*!*	* the subsequent replace into x754 to fail! 12/17/03 MB.
														*!*	SELECT ShipViaTable
														*!*	LOCATE

														*!*	* Update Outship table
														*!*	* also replace SCAC and ASN codes per Juan 11/14/03 MB
														*!*	* Also replace CID with LoadID per Darren 06/09/04 MB
														*!*	IF .lTestMode THEN
														*!*		lcTestString = 'TEST MODE: would update ' + lcOutShipTable + ' with: ' + CRLF + ;
														*!*			' ship_via = ' + TRANSFORM(x754.scacname) +  ;
														*!*			'  scac = ' + TRANSFORM(x754.scaccode) +  ;
														*!*			'  ASNcode = ' + TRANSFORM(lcASNCode) +  ;
														*!*			'  CID = ' + TRANSFORM(x754.LoadID) + ;
														*!*			'  BATCH_NUM = ' + TRANSFORM(x754.LoadID) + ;
														*!*			'  APPT = ' + TRANSFORM(x754.PkupDate)

														*!*	ELSE
														*!*		REPLACE OutShipTable.ship_via WITH x754.scacname, ;
														*!*			OutShipTable.scac WITH x754.scaccode, ;
														*!*			OutShipTable.ASNcode WITH lcASNCode, ;
														*!*			OutShipTable.CID WITH x754.LoadID, ;
														*!*			OutShipTable.BATCH_NUM WITH x754.LoadID, ;
														*!*			OutShipTable.APPT WITH x754.PkupDate, ;
														*!*			OutShipTable.CALLED WITH .dCreationDate, ;
														*!*			OutShipTable.APPT_TIME WITH x754.PkupTime, ;
														*!*			OutShipTable.APPT_NUM WITH x754.LoadID, ;
														*!*			OutShipTable.APPTREMARKS WITH "754 Received" ;
														*!*			IN OutShipTable

														*!*		lcTestString = 'Updated ' + lcOutShipTable + ' with: ' + CRLF + ;
														*!*			' ship_via = ' + TRANSFORM(x754.scacname) +  ;
														*!*			'  scac = ' + TRANSFORM(x754.scaccode) +  ;
														*!*			'  ASNcode = ' + TRANSFORM(lcASNCode) +  ;
														*!*			'  CID = ' + TRANSFORM(x754.LoadID) + ;
														*!*			'  BATCH_NUM = ' + TRANSFORM(x754.LoadID) + ;
														*!*			'  APPT = ' + TRANSFORM(x754.PkupDate)
														*!*	ENDIF

														*!*	.UpdateBLTableSCAC(lcMod)

														*!*	.TrackLoadIDs(OutShipTable.wo_num, OutShipTable.swcnum, x754.LoadID, OutShipTable.ship_ref)
														*!*	.TrackProgress(lcTestString, LOGIT+SENDIT+NOWAITIT)

																****** NEW ***************
																* PER PAUL G., HAVING SUCCESS DO SAME CUSTOM PROCESSING AS SARA LEE 7/11/06 MB
																* Do not replace Outship Storenum per Paul 7/12/06 MB
*!*																CASE (INLIST(lcCompanyCode,SARALEECODE,SUCCESSCODE,NANJINGCODE,MORETFLCODE,MORETMLCODE,MORETSPCODE,TKONJCODE,TKOCACODE,TKOFLCODE) OR ;
*!*																		INLIST(lcCompanyCode,PENJCODE,PECACODE,PEFLCODE,DODENIMCACODE,DODENIMNJCODE,JQUEENCACODE,AGEGROUPCODE,AGEGROUPNJCODE,BIOWORLDCODE,SAMSUNGCODE))
														* DO SARA LEE CUSTOM PROCESSING --
														* CHANGE OUTSHIP DC NUM & ADDRESS IF NEW DC NUM IS DIFFERENT FROM OUTSHIP

*!*															.TrackProgress("", LOGIT+SENDIT+NOWAITIT)
*!*															.TrackProgress("SSNM processing Outship, RRC = " + TRANSFORM(OutShipTable.rrc_num) + ", PO = " + TRANSFORM(OutShipTable.cnee_ref), LOGIT+SENDIT+NOWAITIT)

														* Update Outship table
														*lnStoreNum = VAL(x754.storenum)
														lcDCNum = PADR(ALLTRIM(x754.storenum),10,' ')

														*IF (OutShipTable.storenum = lnStoreNum) AND (OutShipTable.dcnum = lcDCNum) THEN
														IF (OutShipTable.dcnum = lcDCNum) THEN
															* no change -- nothing to do
															.TrackProgress('Outship already had 754 DC Num = ' + lcDCNum, LOGIT+SENDIT+NOWAITIT)
														ELSE
															* the DC changed, update it and its address fields in Outship table.
															* first look up the new storenum in JCPDC table
															SELECT JCPDCS
															* 9/28/07 MB PER PAUL/CHRIS; NOW USING CHAR DCNUM FOR THIS SEARCH
															*LOCATE FOR storenum = lnStoreNum
															* added replace of outship.consignee 4/1/08 MB per Paul.
															LOCATE FOR ALLTRIM(dcnum) == ALLTRIM(lcDCNum)
															IF FOUND() THEN
																IF .lTestMode THEN
																	lcTestString = 'TEST MODE: would update ' + lcOutShipTable + ' with: ' + CRLF + ;
																		' dcnum, storenum = ' + lcDCNum + ;
																		' consignee, name = ' + ALLTRIM(JCPDCS.NAME) + ;
																		'  address = ' + TRANSFORM(JCPDCS.address) + ;
																		'  csz = ' + ;
																		(ALLTRIM(JCPDCS.CITY) + ", " + ALLTRIM(JCPDCS.STATE) + " " + ALLTRIM(JCPDCS.ZIP))

																ELSE
																	* update Shipping Instructions with new storenum per PG and JB 7/16/08 MB.
																	lcShipIns = ALLTRIM(OutShipTable.shipins)
																	lcShipIns = STRTRAN(lcShipIns,("STORENUM*"+ALLTRIM(OutShipTable.dcnum)),("STORENUM*"+ALLTRIM(lcDCNum)))
																	
																	
																	.UpdateSQLOutshipWithDCInfo(lcDCNum, lcShipIns)

																	lcTestString = 'Updated ' + lcOutShipTable + ' with: ' + CRLF + ;
																		' dcnum, storenum = ' + lcDCNum + ;
																		' consignee, name = ' + ALLTRIM(JCPDCS.NAME) + ;
																		'  address = ' + TRANSFORM(JCPDCS.address) + ;
																		'  csz = ' + ;
																		(ALLTRIM(JCPDCS.CITY) + ", " + ALLTRIM(JCPDCS.STATE) + " " + ALLTRIM(JCPDCS.ZIP))
																ENDIF

																.TrackProgress(lcTestString, LOGIT+SENDIT+NOWAITIT)
															ELSE
																.TrackProgress("!!*** ERROR: new DC Num " + lcDCNum + ;
																	' not found in ' + lcJCPDCTable, LOGIT+SENDIT+NOWAITIT)
																	.lOtherErrors = .T.
															ENDIF  && FOUND()

															SELECT JCPDCS
															LOCATE
														ENDIF  &&  (OutShipTable.dcnum = lcDCNum)

														* Update Outship table with other elements not having to do with storenum/dcnum.
														* do not replace ASN codes per Juan 01/09/06 MB
														IF .lTestMode THEN
															lcTestString = 'TEST MODE: would update ' + lcOutShipTable + ' with: ' + CRLF + ;
																' ship_via = ' + TRANSFORM(x754.scacname) +  ;
																'  scac = ' + TRANSFORM(x754.scaccode) +  ;
																'  CID = ' + TRANSFORM(x754.LoadID) + ;
																'  BATCH_NUM = ' + TRANSFORM(x754.LoadID) + ;
																'  APPT = ' + TRANSFORM(x754.PkupDate)

														ELSE
*!*																IF usesqloutwo(.cMod) THEN
*!*																
*!*																	.UpdateSQLOutshipWithApptInfo()
*!*																	
*!*																ELSE
*!*																	* foxpro
*!*																	REPLACE OutShipTable.ship_via WITH x754.scacname, ;
*!*																		OutShipTable.scac WITH x754.scaccode, ;
*!*																		OutShipTable.CID WITH x754.LoadID, ;
*!*																		OutShipTable.BATCH_NUM WITH x754.LoadID, ;
*!*																		OutShipTable.APPT WITH x754.PkupDate, ;
*!*																		OutShipTable.CALLED WITH .dCreationDate, ;
*!*																		OutShipTable.APPT_TIME WITH x754.PkupTime, ;
*!*																		OutShipTable.APPT_NUM WITH x754.LoadID, ;
*!*																		OutShipTable.APPTREMARKS WITH "754 Received" ;
*!*																		IN OutShipTable
*!*																ENDIF

*!*																lcTestString = 'Updated ' + lcOutShipTable + ' with: ' + CRLF + ;
*!*																	' ship_via = ' + TRANSFORM(x754.scacname) +  ;
*!*																	'  scac = ' + TRANSFORM(x754.scaccode) +  ;
*!*																	'  CID = ' + TRANSFORM(x754.LoadID) + ;
*!*																	'  BATCH_NUM = ' + TRANSFORM(x754.LoadID) + ;
*!*																	'  APPT = ' + TRANSFORM(x754.PkupDate)
														ENDIF
														
														.UpdateBLTableSCAC(lcMod)

														.TrackLoadIDs(OutShipTable.wo_num, OutShipTable.swcnum, x754.LoadID, OutShipTable.ship_ref)
														.TrackProgress(lcTestString, LOGIT+SENDIT+NOWAITIT)


*!*																OTHERWISE
*!*																	* nothing
*!*															ENDCASE

														**************************************************************************************************
														* update Outwolog per Darren 3/27/06 MB
														* Applies to all customers.
																	
														.UpdateSQLOutwolog()

														**************************************************************************************************

														* Add record to Appointment table
														* NOTE: not inserting appt_time because that's not populated in JC Penney or Saralee 754 files,
														* field = 754.pkuptime.
														* Added insert of outwologid, wo_num, accountid 3/9/06 MB per Darren
														IF .lTestMode THEN
															.TrackProgress('TEST MODE: would add rec to Appt Table here.', LOGIT+NOWAITIT)
														ELSE
															*old code 2/18/2016
															*!*	lnApptCallID = genpk("apptcall","wh")
															*!*	INSERT INTO AppointmentTable ( ;
															*!*		ApptCallID, ;
															*!*		outwologid, ;
															*!*		wo_num, ;
															*!*		accountid, ;
															*!*		outshipid, ;
															*!*		CALLED, ;
															*!*		APPT, ;
															*!*		APPT_NUM, ;
															*!*		remarks, ;
															*!*		addby, ;
															*!*		adddt) ;
															*!*		VALUES ( ;
															*!*		lnApptCallID, ;
															*!*		OutShipTable.outwologid, ;
															*!*		OutShipTable.wo_num, ;
															*!*		OutShipTable.accountid, ;
															*!*		OutShipTable.outshipid, ;
															*!*		.dCreationDate, ;
															*!*		x754.PkupDate, ;
															*!*		x754.LoadID, ;
															*!*		"754 Received", ;
															*!*		"PROC754", ;
															*!*		DATETIME())

*!*																* new code supporting sql apptcall below 2/18/2016
*!*																SELECT apptcall
*!*																SCATTER MEMVAR MEMO BLANK
*!*																
*!*																m.outwologid = OutShipTable.outwologid
*!*																m.wo_num = OutShipTable.wo_num
*!*																m.accountid = OutShipTable.accountid
*!*																m.outshipid = OutShipTable.outshipid
*!*																m.CALLED = .dCreationDate
*!*																m.APPT = x754.PkupDate
*!*																m.APPT_NUM = x754.LoadID
*!*																m.remarks = "754 Received"
*!*																m.addby = "PROC754"
*!*																m.adddt = DATETIME()
*!*																m.addproc = "PROC754"
*!*																*m.updatedt = DATETIME()
*!*																m.Office = .aOutShipTables( k, 11)  && i.e. Rate Office in whoffice
*!*																m.Mod = .aOutShipTables( k, 12)  && i.e. office in whoffice

*!*																m.ApptCallID = dygenpk("apptcall","whall")
*!*																
*!*																INSERT INTO APPTCALL FROM MEMVAR
*!*																
*!*																.TrackProgress('Did SQL INSERT into APPTCALL for wo_num = ' + TRANSFORM(m.wo_num) + ', APPT_NUM = ' + TRANSFORM(m.APPT_NUM), LOGIT+SENDIT)

														ENDIF 

														* Accumulate data in x754/y754 for the reporting phase
														* We're putting the Place Code into x754/y754 so we know which
														* regional outship table this record updated. Later that will drive
														* the data in the reports we send to each regional contact.
														REPLACE x754.qty WITH OutShipTable.ctnqty, ;
															x754.pt WITH OutShipTable.ship_ref, ;
															x754.cnee_ref WITH OutShipTable.cnee_ref, ;
															x754.wo_num WITH OutShipTable.wo_num, ;
															x754.wo_date WITH OutShipTable.wo_date, ;
															x754.consignee WITH OutShipTable.consignee, ;
															x754.ship_ref WITH OutShipTable.ship_ref, ;
															x754.placecode WITH lcPlace

														lnTally = lnTally + 1

														* Create y754 record. There can be many y754 records per single x754 record.
														SELECT x754
														SCATTER MEMVAR
														SELECT y754
														APPEND BLANK
														GATHER MEMVAR
													ENDSCAN  && inner outship scan
													
												OTHERWISE 
													* standard case; try to match rrc AND po_num
													*****SCAN FOR (wo_date > DATE()-90) AND OutShipTable.rrc_num = x754.rrc_num AND OutShipTable.cnee_ref = x754.po_num
													
													SCAN FOR (wo_date > ldOldestWoDate) AND OutShipTable.rrc_num = x754.rrc_num AND OutShipTable.cnee_ref = x754.po_num
													
					
					* special cases for bad data			
					
				*SCAN FOR (wo_date > ldOldestWoDate) AND OutShipTable.rrc_num = x754.rrc_num && ===> some RECENT MORET-SP FILES ARE NOT SENDING PONUMS
				
					
				*SCAN FOR (wo_date > ldOldestWoDate) AND OutShipTable.cnee_ref = x754.po_num AND INLIST(ACCOUNTID,5836,5865)  && ===> TKO PROVIDES NO RRCS; TRY TO MATCH BY PoNUM & ACCOUNTID PER JUAN 4/21/2016
				
				
				*SCAN FOR (wo_date > ldOldestWoDate) AND INLIST(wo_num,7144318,7144391)
					
					
					*SCAN FOR (wo_date > ldOldestWoDate) AND OutShipTable.cnee_ref = x754.po_num AND INLIST(wo_num,7119912,7119955)
														
					*SCAN FOR (wo_date > ldOldestWoDate) AND OutShipTable.rrc_num = x754.rrc_num AND INLIST(wo_num,6178820)  && ===> RECENT mORET-SP FILES ARE NOT SENDING PONUMS
														
					*SCAN FOR (wo_date > ldOldestWoDate) AND OutShipTable.cnee_ref = x754.po_num 
																											
					*!*	SCAN FOR (wo_date > DATE()-6) AND (OutShipTable.cnee_ref = x754.po_num) AND INLIST(wo_num,3153468)

					*!*															* special processing per Paul G 05/12/08 for when rrc-nums get messed up.
					*!*	SCAN FOR (wo_date > DATE()-20) AND OutShipTable.cnee_ref = x754.po_num AND ;
					*!*	INLIST(wo_num,4015927)

														* below was way too slow because not rushmore optimizable - also hung my pc!
														*!*	 SCAN FOR ALLTRIM(OutShipTable.rrc_num) == ALLTRIM(x754.rrc_num) AND ; 
														*!*	 ALLTRIM(OutShipTable.cnee_ref) == ALLTRIM(x754.po_num)

														lcASNCode = SPACE(2)
														DO CASE
															*!*	CASE INLIST(lcCompanyCode,REGATTACODE)

															*!*		* populate outship.ASNcode from the regional shipvia table
															*!*		SELECT ShipViaTable
															*!*		* look for code by matching SCACnames, for particular company.
															*!*		LOCATE FOR ShipViaTable.account == lcCompanyCode AND ShipViaTable.shipvia == x754.scacname
															*!*		IF FOUND() THEN
															*!*			lcASNCode = ALLTRIM(ShipViaTable.CODE)
															*!*		ELSE
															*!*			.RememberBadSCACName(lcCompanyCode, x754.scacname, lcShipViaTable)
															*!*		ENDIF
															*!*		* doing a locate here because when not found(), being at eof() was causing
															*!*		* the subsequent replace into x754 to fail! 12/17/03 MB.
															*!*		SELECT ShipViaTable
															*!*		LOCATE

															*!*		* Update Outship table
															*!*		* also replace SCAC and ASN codes per Juan 11/14/03 MB
															*!*		* Also replace CID with LoadID per Darren 06/09/04 MB
															*!*		IF .lTestMode THEN
															*!*			lcTestString = 'TEST MODE: would update ' + lcOutShipTable + ' with: ' + CRLF + ;
															*!*				' ship_via = ' + TRANSFORM(x754.scacname) +  ;
															*!*				'  scac = ' + TRANSFORM(x754.scaccode) +  ;
															*!*				'  ASNcode = ' + TRANSFORM(lcASNCode) +  ;
															*!*				'  CID = ' + TRANSFORM(x754.LoadID) + ;
															*!*				'  BATCH_NUM = ' + TRANSFORM(x754.LoadID) + ;
															*!*				'  APPT = ' + TRANSFORM(x754.PkupDate)

															*!*		ELSE
															*!*			REPLACE OutShipTable.ship_via WITH x754.scacname, ;
															*!*				OutShipTable.scac WITH x754.scaccode, ;
															*!*				OutShipTable.ASNcode WITH lcASNCode, ;
															*!*				OutShipTable.CID WITH x754.LoadID, ;
															*!*				OutShipTable.BATCH_NUM WITH x754.LoadID, ;
															*!*				OutShipTable.APPT WITH x754.PkupDate, ;
															*!*				OutShipTable.CALLED WITH .dCreationDate, ;
															*!*				OutShipTable.APPT_TIME WITH x754.PkupTime, ;
															*!*				OutShipTable.APPT_NUM WITH x754.LoadID, ;
															*!*				OutShipTable.APPTREMARKS WITH "754 Received" ;
															*!*				IN OutShipTable

															*!*			lcTestString = 'Updated ' + lcOutShipTable + ' with: ' + CRLF + ;
															*!*				' ship_via = ' + TRANSFORM(x754.scacname) +  ;
															*!*				'  scac = ' + TRANSFORM(x754.scaccode) +  ;
															*!*				'  ASNcode = ' + TRANSFORM(lcASNCode) +  ;
															*!*				'  CID = ' + TRANSFORM(x754.LoadID) + ;
															*!*				'  BATCH_NUM = ' + TRANSFORM(x754.LoadID) + ;
															*!*				'  APPT = ' + TRANSFORM(x754.PkupDate)
															*!*		ENDIF
															*!*		
															*!*		.UpdateBLTableSCAC(lcMod)

															*!*		.TrackLoadIDs(OutShipTable.wo_num, OutShipTable.swcnum, x754.LoadID, OutShipTable.ship_ref)
															*!*		.TrackProgress(lcTestString, LOGIT+SENDIT+NOWAITIT)

																****** NEW ***************
																* PER PAUL G., HAVING SUCCESS DO SAME CUSTOM PROCESSING AS SARA LEE 7/11/06 MB
																* Do not replace Outship Storenum per Paul 7/12/06 MB
															CASE (INLIST(lcCompanyCode,SARALEECODE,SUCCESSCODE,NANJINGCODE,MORETFLCODE,MORETMLCODE,MORETSPCODE,TKONJCODE,TKOCACODE,TKOFLCODE) OR ;
																	INLIST(lcCompanyCode,PENJCODE,PECACODE,PEFLCODE,DODENIMCACODE,DODENIMNJCODE,JQUEENCACODE,AGEGROUPCODE,AGEGROUPNJCODE,BIOWORLDCODE))
																* DO SARA LEE CUSTOM PROCESSING --
																* CHANGE OUTSHIP DC NUM & ADDRESS IF NEW DC NUM IS DIFFERENT FROM OUTSHIP

																.TrackProgress("", LOGIT+SENDIT+NOWAITIT)
																.TrackProgress("SSNM processing Outship, RRC = " + TRANSFORM(OutShipTable.rrc_num) + ", PO = " + TRANSFORM(OutShipTable.cnee_ref), LOGIT+SENDIT+NOWAITIT)

																* Update Outship table
																*lnStoreNum = VAL(x754.storenum)
																lcDCNum = PADR(ALLTRIM(x754.storenum),10,' ')

																*IF (OutShipTable.storenum = lnStoreNum) AND (OutShipTable.dcnum = lcDCNum) THEN
																IF (OutShipTable.dcnum = lcDCNum) THEN
																	* no change -- nothing to do
																	.TrackProgress('Outship already had 754 DC Num = ' + lcDCNum, LOGIT+SENDIT+NOWAITIT)
																ELSE
																	* the DC changed, update it and its address fields in Outship table.
																	* first look up the new storenum in JCPDC table
																	SELECT JCPDCS
																	* 9/28/07 MB PER PAUL/CHRIS; NOW USING CHAR DCNUM FOR THIS SEARCH
																	*LOCATE FOR storenum = lnStoreNum
																	* added replace of outship.consignee 4/1/08 MB per Paul.
																	LOCATE FOR ALLTRIM(dcnum) == ALLTRIM(lcDCNum)
																	IF FOUND() THEN
																		IF .lTestMode THEN
																			lcTestString = 'TEST MODE: would update ' + lcOutShipTable + ' with: ' + CRLF + ;
																				' dcnum, storenum = ' + lcDCNum + ;
																				' consignee, name = ' + ALLTRIM(JCPDCS.NAME) + ;
																				'  address = ' + TRANSFORM(JCPDCS.address) + ;
																				'  csz = ' + ;
																				(ALLTRIM(JCPDCS.CITY) + ", " + ALLTRIM(JCPDCS.STATE) + " " + ALLTRIM(JCPDCS.ZIP))

																		ELSE
																			* update Shipping Instructions with new storenum per PG and JB 7/16/08 MB.
																			lcShipIns = ALLTRIM(OutShipTable.shipins)
																			lcShipIns = STRTRAN(lcShipIns,("STORENUM*"+ALLTRIM(OutShipTable.dcnum)),("STORENUM*"+ALLTRIM(lcDCNum)))
																																					
																			.UpdateSQLOutshipWithDCInfo(lcDCNum, lcShipIns)

																			lcTestString = 'Updated ' + lcOutShipTable + ' with: ' + CRLF + ;
																				' dcnum, storenum = ' + lcDCNum + ;
																				' consignee, name = ' + ALLTRIM(JCPDCS.NAME) + ;
																				'  address = ' + TRANSFORM(JCPDCS.address) + ;
																				'  csz = ' + ;
																				(ALLTRIM(JCPDCS.CITY) + ", " + ALLTRIM(JCPDCS.STATE) + " " + ALLTRIM(JCPDCS.ZIP))
																		ENDIF

																		.TrackProgress(lcTestString, LOGIT+SENDIT+NOWAITIT)
																	ELSE
																		.TrackProgress("!!*** ERROR: new DC Num " + lcDCNum + ;
																			' not found in ' + lcJCPDCTable, LOGIT+SENDIT+NOWAITIT)
																			.lOtherErrors = .T.
																	ENDIF  && FOUND()

																	SELECT JCPDCS
																	LOCATE
																ENDIF  &&  (OutShipTable.dcnum = lcDCNum)

																* Update Outship table with other elements not having to do with storenum/dcnum.
																* do not replace ASN codes per Juan 01/09/06 MB
																IF .lTestMode THEN
																	lcTestString = 'TEST MODE: would update ' + lcOutShipTable + ' with: ' + CRLF + ;
																		' ship_via = ' + TRANSFORM(x754.scacname) +  ;
																		'  scac = ' + TRANSFORM(x754.scaccode) +  ;
																		'  CID = ' + TRANSFORM(x754.LoadID) + ;
																		'  BATCH_NUM = ' + TRANSFORM(x754.LoadID) + ;
																		'  APPT = ' + TRANSFORM(x754.PkupDate)

																ELSE
*!*																		IF usesqloutwo(.cMod) THEN
*!*																		
*!*																			.UpdateSQLOutshipWithApptInfo()
*!*																			
*!*																		ELSE
*!*																			* foxpro
*!*																			REPLACE OutShipTable.ship_via WITH x754.scacname, ;
*!*																				OutShipTable.scac WITH x754.scaccode, ;
*!*																				OutShipTable.CID WITH x754.LoadID, ;
*!*																				OutShipTable.BATCH_NUM WITH x754.LoadID, ;
*!*																				OutShipTable.APPT WITH x754.PkupDate, ;
*!*																				OutShipTable.CALLED WITH .dCreationDate, ;
*!*																				OutShipTable.APPT_TIME WITH x754.PkupTime, ;
*!*																				OutShipTable.APPT_NUM WITH x754.LoadID, ;
*!*																				OutShipTable.APPTREMARKS WITH "754 Received" ;
*!*																				IN OutShipTable
*!*																		ENDIF

*!*																		lcTestString = 'Updated ' + lcOutShipTable + ' with: ' + CRLF + ;
*!*																			' ship_via = ' + TRANSFORM(x754.scacname) +  ;
*!*																			'  scac = ' + TRANSFORM(x754.scaccode) +  ;
*!*																			'  CID = ' + TRANSFORM(x754.LoadID) + ;
*!*																			'  BATCH_NUM = ' + TRANSFORM(x754.LoadID) + ;
*!*																			'  APPT = ' + TRANSFORM(x754.PkupDate)
																ENDIF
																
																.UpdateBLTableSCAC(lcMod)

																.TrackLoadIDs(OutShipTable.wo_num, OutShipTable.swcnum, x754.LoadID, OutShipTable.ship_ref)
																.TrackProgress(lcTestString, LOGIT+SENDIT+NOWAITIT)


															OTHERWISE
																* nothing
														ENDCASE

														**************************************************************************************************
														* update Outwolog per Darren 3/27/06 MB
														* Applies to all customers.
																	
														.UpdateSQLOutwolog()

														**************************************************************************************************

														* Add record to Appointment table
														* NOTE: not inserting appt_time because that's not populated in JC Penney or Saralee 754 files,
														* field = 754.pkuptime.
														* Added insert of outwologid, wo_num, accountid 3/9/06 MB per Darren
														IF .lTestMode THEN
															.TrackProgress('TEST MODE: would add rec to Appt Table here.', LOGIT+NOWAITIT)
														ELSE
															*old code 2/18/2016
															*!*	lnApptCallID = genpk("apptcall","wh")
															*!*	INSERT INTO AppointmentTable ( ;
															*!*		ApptCallID, ;
															*!*		outwologid, ;
															*!*		wo_num, ;
															*!*		accountid, ;
															*!*		outshipid, ;
															*!*		CALLED, ;
															*!*		APPT, ;
															*!*		APPT_NUM, ;
															*!*		remarks, ;
															*!*		addby, ;
															*!*		adddt) ;
															*!*		VALUES ( ;
															*!*		lnApptCallID, ;
															*!*		OutShipTable.outwologid, ;
															*!*		OutShipTable.wo_num, ;
															*!*		OutShipTable.accountid, ;
															*!*		OutShipTable.outshipid, ;
															*!*		.dCreationDate, ;
															*!*		x754.PkupDate, ;
															*!*		x754.LoadID, ;
															*!*		"754 Received", ;
															*!*		"PROC754", ;
															*!*		DATETIME())

*!*																* new code supporting sql apptcall below 2/18/2016
*!*																SELECT apptcall
*!*																SCATTER MEMVAR MEMO BLANK
*!*																
*!*																m.outwologid = OutShipTable.outwologid
*!*																m.wo_num = OutShipTable.wo_num
*!*																m.accountid = OutShipTable.accountid
*!*																m.outshipid = OutShipTable.outshipid
*!*																m.CALLED = .dCreationDate
*!*																m.APPT = x754.PkupDate
*!*																m.APPT_NUM = x754.LoadID
*!*																m.remarks = "754 Received"
*!*																m.addby = "PROC754"
*!*																m.adddt = DATETIME()
*!*																m.addproc = "PROC754"
*!*																*m.updatedt = DATETIME()
*!*																m.Office = .aOutShipTables( k, 11)  && i.e. Rate Office in whoffice
*!*																m.Mod = .aOutShipTables( k, 12)  && i.e. office in whoffice

*!*																m.ApptCallID = dygenpk("apptcall","whall")
*!*																
*!*																INSERT INTO APPTCALL FROM MEMVAR
*!*																
*!*																.TrackProgress('Did SQL INSERT into APPTCALL for wo_num = ' + TRANSFORM(m.wo_num) + ', APPT_NUM = ' + TRANSFORM(m.APPT_NUM), LOGIT+SENDIT)

														ENDIF
														
														SELECT OutShipTable

														* Accumulate data in x754/y754 for the reporting phase
														* We're putting the Place Code into x754/y754 so we know which
														* regional outship table this record updated. Later that will drive
														* the data in the reports we send to each regional contact.
														REPLACE x754.qty WITH OutShipTable.ctnqty, ;
															x754.pt WITH OutShipTable.ship_ref, ;
															x754.cnee_ref WITH OutShipTable.cnee_ref, ;
															x754.wo_num WITH OutShipTable.wo_num, ;
															x754.wo_date WITH OutShipTable.wo_date, ;
															x754.consignee WITH OutShipTable.consignee, ;
															x754.ship_ref WITH OutShipTable.ship_ref, ;
															x754.placecode WITH lcPlace

														lnTally = lnTally + 1

														* Create y754 record. There can be many y754 records per single x754 record.
														SELECT x754
														SCATTER MEMVAR
														SELECT y754
														APPEND BLANK
														GATHER MEMVAR
													ENDSCAN  && inner outship scan
											ENDCASE
											********************** end new ************************
										ENDIF  &&  .x754RecordDataIsOkay()
									ENDSCAN  &&  FOR EMPTY(placecode)

									*!*	SELECT AppointmentTable
									*!*	BROW


									*!*									IF USED('JCPDCS') THEN
									*!*										USE IN JCPDCS
									*!*									ENDIF
									
									* now closing these tables before reopening early on in the k = 1 TO ALEN(.aOutShipTables) loop 9/12/2016 MB
*!*										USE IN OutShipTable
*!*										USE IN bltable
*!*										*!*										USE IN ShipViaTable
*!*										USE IN Outwolog
*!*										*USE IN AppointmentTable
*!*										USE IN GENERATEPK
*!*										CLOSE DATABASES

									.TrackProgress("Processing " + lcOriginalFile + ", updated " + TRANSFORM(lnTally) + " records in " + .aOutShipTables[k,1], LOGIT+SENDIT)
								ENDFOR  &&  k = 1 TO ALEN(.aOutShipTables)
								.TrackProgress("------------------------------", LOGIT+SENDIT)

								* list any RRC_NUM/PO_NUMs that couldn't be found in any outship table in internal email.
								.LogRecsNotFound()

							ELSE
								.TrackProgress('!!!**** Error copying ' + lc754File + ' to ' + lcOriginalFile, LOGIT+SENDIT)
								.lOtherErrors = .T.
							ENDIF  &&  FILE(lcOriginalFile)

						ENDIF  &&  FILE(lc754File)

					ENDFOR  &&  i = 1 TO lnNumFiles

					* tableupdate sql apptcall 2/18/2016
					IF USED('APPTCALL') AND (RECCOUNT('APPTCALL') > 0) THEN 
						*!*	IF NOT tu("APPTCALL") THEN
						*!*		=AERROR(APPTCALLERR)
						*!*		IF TYPE("APPTCALLERR")#"U" THEN && and inlist(aerrq[1,1],1526,1542,1543)
						*!*			.TrackProgress('APPTCALL TABLEUPDATE ERROR : ' + APPTCALLERR[1,2],LOGIT+SENDIT)
						*!*		ELSE
						*!*			.TrackProgress('APPTCALL TABLEUPDATE ERROR : TU() returned .F.',LOGIT+SENDIT)
						*!*		ENDIF
						*!*		THROW
						*!*	ENDIF
						* tableupate SQL
						IF tu("APPTCALL") THEN
							.TrackProgress('=======> SUCCESS: TU(APPTCALL) returned .T.!',LOGIT+SENDIT)
						ELSE
							* ERROR on table update
							.TrackProgress('=======> ERROR: TU(APPTCALL) returned .F.!',LOGIT+SENDIT)
							.lOtherErrors = .T.
							=AERROR(UPCERR)
							IF TYPE("UPCERR")#"U" THEN 
								.TrackProgress('Error Description = ' + UPCERR[1,2],LOGIT+SENDIT)
							ENDIF
						ENDIF
					ELSE
						.TrackProgress('=======> Did NOT call TU(): APPTCALL not used or had no records!',LOGIT+SENDIT)
					ENDIF


					IF USED('APPTCALL') THEN
						USE IN APPTCALL
					ENDIF

					.TrackProgress("Processed " + TRANSFORM(lnNumFiles) + " '754' files", LOGIT+SENDIT)
					WAIT CLEAR
				ELSE
					.TrackProgress('No 754 files to upload!', LOGIT+WAITIT+SENDIT)
				ENDIF  &&  lnNumFiles > 0

*!*	SELECT y754
*!*	browse						
*!*	SET STEP ON 

				* list SCAC codes we couldn't find in SCAC table
				.LogBadSCACCodes()

				* list SCAC Names we couldn't find in ship via tables
				.LogBadSCACNames()

				* ANALYZE LOADID VS SWCNUM
				.AnalyzeLoadIDs()

				.TrackProgress('754 process ended normally.', LOGIT+SENDIT)

			CATCH TO loError

				.TrackProgress('There was an error.',LOGIT+SENDIT)
				.TrackProgress('The Error # is: ' + TRANSFORM(loError.ERRORNO), LOGIT+SENDIT)
				.TrackProgress('The Error Message is: ' + TRANSFORM(loError.MESSAGE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred in Program: ' + TRANSFORM(loError.PROCEDURE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred at line #: ' + TRANSFORM(loError.LINENO), LOGIT+SENDIT)
				.cSendTo = 'mbennett@fmiint.com'
				.cCC = ''

			FINALLY

			ENDTRY

			WAIT CLEAR


			******************************************************************************
			* This block can't be before the ENDTRY because REPORTTOPDF.PRG has its own
			* error handling which would be overridden.
			IF USED('y754') THEN
				* loop to process all regions in y754 cursor
				FOR l = 1 TO ALEN(.aOutShipTables,1)
					lcPlace = .aOutShipTables[l,3]
					* there should be data in y754 to report on.
					* Prepare PDF report for client - filter on PlaceCode so each client
					* only sees the data for their region.
					SELECT y754

					*!*						DELETE TAG ALL
					*INDEX ON (storenum + STR(wo_num)) TAG stores FOR placecode == lcPlace
					*INDEX ON (storenum + loadid + STR(wo_num) + po_num) TAG stores FOR placecode == lcPlace
					INDEX ON (storenum + LoadID + consignee + STR(wo_num) + po_num) TAG stores FOR ALLTRIM(placecode) == ALLTRIM(lcPlace)
					SET ORDER TO stores
					GOTO TOP
					IF NOT EOF()
						* create PDF report
						SET CENTURY OFF  && FOR XX/XX/XX FORMAT DATES ON REPORT
						llPDFReportWasCreated = .F.
						*DO REPORTTOPDF WITH (.cPDFReportFRX), (.cPDFFilename + "_" + ALLTRIM(lcPlace)), llPDFReportWasCreated
						*DO M:\DEV\PRG\REPORTTOPDF WITH (.cPDFReportFRX), (.cPDFFilename + "_" + ALLTRIM(lcPlace)), llPDFReportWasCreated
						*DO M:\DEV\PRG\RPT2PDF WITH (.cPDFReportFRX), (.cPDFFilename + "_" + ALLTRIM(lcPlace)), llPDFReportWasCreated
						DO RPT2PDF_NOTRIES WITH (.cPDFReportFRX), (.cPDFFilename + "_" + ALLTRIM(lcPlace)), llPDFReportWasCreated

						IF NOT llPDFReportWasCreated THEN
							.TrackProgress('ERROR - PDF file not created',LOGIT+WAITIT+SENDIT)
							.lOtherErrors = .T.
						ENDIF
						SET PRINTER TO DEFAULT
						
						SET CENTURY ON

						* Email PDF report to client
						IF .lSendExternalEmailIsOn THEN
							lcPDFFilename = .cPDFFilename + "_" + ALLTRIM(lcPlace) + ".pdf"

							IF .lx754DataErrors AND NOT .lTestMode THEN
								.cClientSubject = '754 EDI Shipment Routing Report for: ' + TTOC(DATETIME()) + ' (THERE WERE ERRORS)'
							ENDIF

							* 'Send To' regular Region-specific recipient
							.cClientSendTo = .aOutShipTables[l,4]


							IF FILE(lcPDFFilename) THEN
								.cClientAttach = lcPDFFilename
								.TrackProgress('About to send Client email for region: ' + ALLTRIM(lcPlace) + ' to: ' + .cClientSendTo, SENDIT+LOGIT)
								DO FORM dartmail2 WITH ;
									.cClientSendTo,.cClientFrom,.cClientSubject,.cClientCC,.cClientAttach,.cClientBodyText,"A"
								.TrackProgress('Sent Client email.',SENDIT+LOGIT)
							ELSE
								.cClientAttach = ''
								.TrackProgress('ERROR - Did not find PDF file: ' + lcPDFFilename, SENDIT+LOGIT)
							ENDIF
						ELSE
							.TrackProgress('lSendExternalEmailIsOn is .F. - Did not send Client email.', SENDIT+LOGIT)
						ENDIF
					ELSE
						.TrackProgress('There were no y754 records for region: ' + ALLTRIM(lcPlace), SENDIT+LOGIT)
					ENDIF && NOT EOF()
				ENDFOR && l = 1 TO ALEN(.aOutShipTables,1)
			ELSE
				.TrackProgress('Cursor y754 not found. Did not make PDF or send Client email.', SENDIT+LOGIT)
			ENDIF  &&  USED('y754')

			***************** INTERNAL email results ******************************
			.TrackProgress('About to send status email.',LOGIT)

			* document any problem with swcnum vs loadid
			IF .lSwcNumLoadIDProblem THEN
				.cBodyText = '**********************************************************************' + CRLF + ;
					'ATTENTION: one or more SWC #s had more than one Load ID! List follows:' + CRLF + CRLF + ;
					.cSwcNumLoadIDProblemList + ;
					'**********************************************************************' + CRLF + CRLF + .cBodyText

			ENDIF

			IF .lx754DataErrors THEN
				.TrackProgress('WARNING: encountered invalid x754 data! Please review the logfile for details!', SENDIT)
			ENDIF  &&  .lx754DataErrors
			
			* added 7/6/2017 MB to better draw attention to errors in big emails
			IF (.lOtherErrors OR .lx754DataErrors) THEN  
				.cSubject = .cSubject + ' ====> THERE WERE ERRORS!' 
			ENDIF

			.TrackProgress('The logfile is: ' + .cLogFile, SENDIT)
			.TrackProgress('754 import process started: ' + .cStartTime, LOGIT+SENDIT)
			.TrackProgress('754 import process finished: ' + TTOC(DATETIME()), LOGIT+SENDIT)
			IF .lSendInternalEmailIsOn THEN
				DO FORM dartmail2 WITH .cSendTo,.cFrom,.cSubject,.cCC,.cAttach,.cBodyText,"A"
				.TrackProgress('Sent status email.',LOGIT)
			ELSE
				.TrackProgress('Did not send status email: THIS.lSendInternalEmailIsOn = FALSE.',LOGIT)
			ENDIF

			*!*		lSwcNumLoadIDProblem = .F.
			*!*		cSwcNumLoadIDProblemList = ""


			CLOSE ALL

		ENDWITH  &&  THIS

		RETURN
	ENDFUNC && main


	PROCEDURE UpdateSQLOutwolog
		WITH THIS
			LOCAL lcUpdateSQL, llRetval

			* execute a sql update to update outshiptable. outwologid is primary key, so use that in the WHERE clause.
			lcUpdateSQL = "update outwolog set fullpart=1 where outwologid=" + TRANSFORM(OutShipTable.outwologid)
				
			llRetval = xsqlexec(lcUpdateSQL,,,"wh")
			
			.TrackProgress('llRetval = ' + TRANSFORM(llRetval) + ' for lcUpdateSQL = ' + lcUpdateSQL, LOGIT+SENDIT)
		
		ENDWITH
		RETURN
	ENDPROC && UpdateSQLOutwolog


	PROCEDURE UpdateSQLOutshipWithDCInfo
		LPARAMETERS tcDCNum, tcShipIns
		WITH THIS
			LOCAL lcUpdateSQL, llRetval, llDontReformatQuery
			llDontReformatQuery = .T.

			* execute a sql update to update outshiptable. Outshipid is primary key, so use that in the WHERE clause.
			lcUpdateSQL = ;
				"update outship set dcnum='" + tcDCNum + "'," + ;
				"storenum=" + tcDCNum + "," + ;
				"shipins='" + tcShipIns + "'," + ;
				"address='" + ALLTRIM(JCPDCS.address) + "'," + ;
				"consignee='" + ALLTRIM(JCPDCS.NAME) + "'," + ;
				"name='" + ALLTRIM(JCPDCS.NAME) + "'," + ;
				"csz='" + ALLTRIM(JCPDCS.CITY) + ", " + ALLTRIM(JCPDCS.STATE) + " " + ALLTRIM(JCPDCS.ZIP) + "' " + ;
				"where outshipid=" + TRANSFORM(OutShipTable.outshipid)
				
			llRetval = xsqlexec(lcUpdateSQL,,,"wh",,,,,,llDontReformatQuery)  && needed to add llDontReformatQuery param 10/30/2017
			
			.TrackProgress('llRetval = ' + TRANSFORM(llRetval) + ' for lcUpdateSQL = ' + lcUpdateSQL, LOGIT+SENDIT)
		
		ENDWITH
		RETURN
	ENDPROC && UpdateSQLOutshipWithDCInfo


	PROCEDURE UpdateSQLOutshipWithApptInfo
		WITH THIS
			LOCAL lcUpdateSQL, lcPkupTime, llRetval
			
			RETURN
			
			* don't change pick up time if it was not in the 754
			IF NOT EMPTYNUL(x754.PkupTime) THEN
				lcPkupTime = x754.PkupTime
			ELSE
				lcPkupTime = OutShipTable.appt_time
			ENDIF

			* execute a sql update to update outshiptable. Outshipid is primary key, so use that in the WHERE clause.
			lcUpdateSQL = ;
				"update outship set ship_via='" + x754.scacname + "'," + ;
				"scac='" + x754.scaccode + "'," + ;
				"cid='" + x754.LoadID + "'," + ;
				"batch_num='" + x754.LoadID + "'," + ;
				"appt={" + DTOC(x754.PkupDate) + "}," + ;
				"called={" + DTOC(.dCreationDate) + "}," + ;
				"appt_time='" + lcPkupTime + "'," + ;
				"appt_num='" + x754.LoadID + "'," + ;
				"apptremarks='754 Received' " + ;
				"where outshipid=" + TRANSFORM(OutShipTable.outshipid)

			*.TrackProgress('lcUpdateSQL = ' + lcUpdateSQL, LOGIT+SENDIT)
				
			llRetval = xsqlexec(lcUpdateSQL,,,"wh")
			
			.TrackProgress('llRetval = ' + TRANSFORM(llRetval) + ' for lcUpdateSQL = ' + lcUpdateSQL, LOGIT+SENDIT)
		
		ENDWITH
		RETURN
	ENDPROC && UpdateSQLOutshipWithApptInfo


	PROCEDURE UpdateBLTableSCAC
		LPARAMETERS tcMod
		WITH THIS
			* update BL table per Darren 1/10/11 MB
			LOCAL lcTestStringBL, lcMod, lcSQL, luRetVal
			lcMod = tcMod

			IF NOT EMPTY(OutShipTable.bol_no) THEN
				IF SEEK(OutShipTable.bol_no,"bltable","bol_no") THEN
					IF .lTestMode THEN
						lcTestStringBL = 'BL Processing: would update ' + .cBLTable + ' with: ship_via = ' + TRANSFORM(x754.scacname) +  ;
							'  scac = ' + TRANSFORM(x754.scaccode)
					ELSE
						* some BLs now in SQL 9/8/2016 MB
						
						*REPLACE BLTable.ship_via WITH x754.scacname, BLTable.scac WITH x754.scaccode IN BLTable
						
						*lcMod = UPPER(ALLTRIM(bltable.mod))
						goffice = lcMod

						* UPDATE the sql table
				        
						*lcSQL = "update bl set ship_via='" + ALLTRIM(x754.scacname) + "' where blid=" + TRANSFORM(bltable.BLID)
						lcSQL = "update bl set ship_via='" + ALLTRIM(x754.scacname) + "', scac='" + ALLTRIM(x754.scaccode) + "' where blid=" + TRANSFORM(bltable.BLID)  
						.TrackProgress('Update BL lcSQL = ' + lcSQL, LOGIT+SENDIT)
						
						luRetVal = xsqlexec(lcSQL,,,"wh")
						.TrackProgress('RetVal for SQL Update of BL = ' + TRANSFORM(luRetVal), LOGIT+SENDIT)


						lcTestStringBL = 'BL Processing: Updated ' + .cBLTable + ' with: ship_via = ' + TRANSFORM(x754.scacname) +  ;
							'  scac = ' + TRANSFORM(x754.scaccode)
					ENDIF  && .lTestMode
				ELSE
					lcTestStringBL =  'BL Processing: bol_no: ' + OutShipTable.bol_no + ' not found in ' + .cBLTable
				ENDIF  && SEEK()
			ELSE
				lcTestStringBL =  'BL Processing: Empty OutShipTable.bol_no -- no BL updates'
			ENDIF && NOT EMPTY(OutShipTable.bol_no)
			.TrackProgress(lcTestStringBL, LOGIT+SENDIT+NOWAITIT)
		ENDWITH
		RETURN
	ENDPROC && UpdateBLTableSCAC


	PROCEDURE TrackLoadIDs
		* whenever a loadid is updated, track following outship fields in a cursor: wo_num, swcnum, loadid, ship_ref.
		* BUT don't save/track this info if the swcnum is blank, per Paul & Juan 4/10/08 MB
		LPARAMETERS tnWo_num, tcSwcnum, tcLoadid, tcShip_ref
		WITH THIS
			IF NOT EMPTY(tcSwcnum) THEN
				INSERT INTO CurLoadIDs ( ;
					wo_num, ;
					swcnum, ;
					LoadID, ;
					ship_ref) ;
					VALUES ( ;
					tnWo_num, ;
					tcSwcnum, ;
					tcLoadid, ;
					tcShip_ref)
			ENDIF
		ENDWITH
	ENDPROC



	PROCEDURE AnalyzeLoadIDs
		WITH THIS
			IF USED('CurLoadIDs') AND NOT EOF('CurLoadIDs') THEN
				*SELECT CurLoadIDs
				*SET ORDER TO swcnum
				*BROW

				* determine if any swcnum has more than one loadid

				* first get unique pairs
				SELECT DISTINCT swcnum, LoadID ;
					FROM CurLoadIDs ;
					INTO CURSOR CurDistinct ;
					ORDER BY swcnum, LoadID

				* then count on swcnum
				SELECT swcnum, COUNT(*) AS CNT ;
					FROM CurDistinct ;
					INTO CURSOR CurCount ;
					GROUP BY swcnum ;
					ORDER BY swcnum

				* now list any swcnums that had count > 1
				SELECT * ;
					FROM CurLoadIDs ;
					INTO CURSOR CurProblem ;
					WHERE swcnum IN ;
					(SELECT swcnum FROM CurCount WHERE CNT > 1) ;
					ORDER BY swcnum, LoadID

				IF USED('CurProblem') AND NOT EOF('CurProblem') THEN
					* we detected a problem; set flag
					.lSwcNumLoadIDProblem = .T.
					* build a string of the problem records
					.cSwcNumLoadIDProblemList = "SWCNUM   LOADID                WO_NUM   SHIP_REF" + CRLF + ;
						"-------  --------------------  -------  --------------------"
					SELECT CurProblem
					SCAN
						.cSwcNumLoadIDProblemList = .cSwcNumLoadIDProblemList + CRLF + ;
							CurProblem.swcnum + "  " + ;
							CurProblem.LoadID + "  " + ;
							PADL(CurProblem.wo_num,7,"0") + "  " + ;
							CurProblem.ship_ref
					ENDSCAN
				ENDIF
			ENDIF  &&  USED('CurLoadIDs') AND NOT EOF('CurLoadIDs')
		ENDWITH
	ENDPROC



	***********************************************************************************************************************
	PROCEDURE CreateLoadIDCursor
		* this cursor holds info for determining whether any swcnum has more than one loadid
		CREATE CURSOR CurLoadIDs (wo_num N(7,0), swcnum c(7), LoadID c(20), ship_ref c(20))
		INDEX ON wo_num TAG wo_num
		INDEX ON swcnum TAG swcnum
		INDEX ON LoadID TAG LoadID
		INDEX ON ship_ref TAG ship_ref
	ENDPROC


	PROCEDURE LoadMoretFlatFileIntoX754Cursor()
		LPARAMETERS tcOriginalFile
		WITH THIS
			LOCAL lcFlatFile, lnNumLines, i, lcLine, lcRRCFirstChar
			LOCAL rrc_num, ship_ref, LoadID, scaccode, scacname, storenum, PkupDate, detrecno, shipfrom
			LOCAL ARRAY laFlatFile(1,1)

			* load flat file into array
			lcFlatFile = FILETOSTR(tcOriginalFile)
			*!*			lcOutstr=STRTRAN(lcOutstr,CHR(13),"")
			*!*			lcOutstr=STRTRAN(lcOutstr,CHR(10),"")
			*!*			lcOutstr=STRTRAN(lcOutstr,"~",CHR(13))
			lnNumLines = ALINES(laFlatFile,lcFlatFile)

			* parse file in array to memvars
			FOR i = 1 TO lnNumLines

				lcLine = laFlatFile[i]
				m.rrc_num = SUBSTR(lcLine,9,10)
				m.ship_ref = SUBSTR(lcLine,19,6)
				m.LoadID = SUBSTR(lcLine,25,30)
				m.scaccode = SUBSTR(lcLine,55,4)
				
*!*					* added 6/15/2011 MB to force a particular SCAC code
*!*					IF NOT EMPTY(.cForcedSCACCode) THEN
*!*						m.scaccode = .cForcedSCACCode
*!*					ENDIF
				
				m.storenum = SUBSTR(lcLine,59,6)
				m.PkupDate = SUBSTR(lcLine,59,6)
				m.PkupDate = CTOD(SUBSTR(lcLine,69,2)+"/"+SUBSTR(lcLine,71,2)+"/"+SUBSTR(lcLine,65,4))

				* since shipfrom is not provided, base it on the first digit of rrc#.
				* Per PG, CA Moret rrc's begin with 5, NJ Moret rrc's begin with 4
				lcRRCFirstChar = LEFT(m.rrc_num,1)
				DO CASE
					CASE lcRRCFirstChar = "5"
						m.shipfrom = "MIRA LOMA, CA  917523244"
					CASE lcRRCFirstChar = "4"
						m.shipfrom = "CARTERET, NJ  070081098"
					CASE lcRRCFirstChar = "1"
						m.shipfrom = "MIAMI, FL 33182"
					OTHERWISE
						m.shipfrom = "???"
				ENDCASE

				m.detrecno = i

				* get SCAC NAME FROM CODE
				xsqlexec("select dispname from scac where scac='"+m.scaccode+"'","scac",,"wh")
				m.scacname=scac.dispname
				
				IF EMPTY(m.scacname) THEN
					.RememberBadSCACCode(m.scaccode)
				ENDIF
				*SELECT scactable
				*LOCATE

				* populate x754 cursor
				INSERT INTO x754 FROM MEMVAR

			ENDFOR  &&  i = 1 TO lnNumLines

			*SELECT x754
			*BROWSE

		ENDWITH
	ENDPROC  && LoadMoretFlatFileIntoX754Cursor()

	***********************************************************************************************************************
	PROCEDURE Load754intoCursor
		* Get info needed to update Outship Tables (rrc_num & scacname),
		* other fields are for the 754 report sent to the client.
		WITH THIS
			LOCAL llNewRec, lcPkupDate, lcTempdate, ldPkupDate, lnHoldRec, lcThisRRC, lcCreationDate
			LOCAL lnHour, llTimeIsPM, scaccode, scacname, lcPreviousSegment
			LOCAL lcSHIPFROM, lcEDIType

			*USE (.cSCACTable) IN 0 AGAIN ALIAS scactable
			lcPreviousSegment = ''
			llNewRec = .F.
			* BGN segment, note date in field 3 in YYYYMMDD format:
			*!*	BGN00200406020111495300009200000000000000000000000000002004060201114953RJ

			*!*	DETAIL DATA LOOKS LIKE (with extra LFs removed):
			*!*	LX1
			*!*	L110689080044820031112024026RRCLTIS
			*!*	L1198519322PUA
			*!*	BLRDINL
			*!*	OID39908161
			*!*	G626920031112
			*!*	N1ST9294417
			*!*	LX7
			*!*	L110689080334320031112024063RRCLTIS
			*!*	L1198519322PUA
			*!*	BLRDINL
			*!*	OID26212001
			*!*	OID39910295
			*!*	G626920031112
			*!*	N1ST9294417

			*!*				SELECT x856
			*!*				SET FILTER TO segment = "N1"
			*!*				locate
			*!*				brow


			SELECT x856
			SCAN
				STORE '' TO m.scaccode, m.scacname
				
*!*					* added 6/15/2011 MB to force a particular SCAC code
*!*					* Needed here in adddition to the BLR CASE below to handle when there are no BLR segments.
*!*					IF NOT EMPTY(.cForcedSCACCode) THEN
*!*						m.scaccode = .cForcedSCACCode
*!*					ENDIF
				
				DO CASE
						* get 754 file creation date from BGN segment, to be used in adding appointment records, per Juan 06/10/04 MB
					CASE ALLTRIM(x856.segment) = "BGN"
						lcCreationDate = ALLTRIM(x856.f3)
						THIS.dCreationDate = CTOD(SUBSTR(lcCreationDate,5,2) + "/" + SUBSTR(lcCreationDate,7,2) + "/" + LEFT(lcCreationDate,4))
					CASE ALLTRIM(x856.segment) = "ST"
					
						* added 3/10/11 to handle J.Queen 997s that are appearing in 754 folder.
						lcEDIType = UPPER(ALLTRIM(x856.f1))
						IF NOT (lcEDIType == '754') THEN
							.cSendTo = 'mbennett@fmiint.com'
							.cCC = ''
							.cClientSendTo = ''
							.cClientCC = ''
							.lSendExternalEmailIsOn = .F.
							.TrackProgress("*****************************************", LOGIT+SENDIT)
							.TrackProgress("Not a 754! EDI Type = " + lcEDIType, LOGIT+SENDIT)
							.TrackProgress("*****************************************", LOGIT+SENDIT)
							* close this cursor to prevent client report/email from going out...
							IF USED('y754') THEN
								USE IN y754
							ENDIF
							THROW
						ENDIF
					
						SELECT x754
						APPEND BLANK
						REPLACE detrecno WITH RECNO()
						llNewRec = .F.  && 11/18/03 to prevent blank rec when we hit another ST segment.

					CASE ALLTRIM(x856.segment) == "N4"
						REPLACE x754.shipfrom WITH ALLTRIM(x856.f1) + ", " + ;
							ALLTRIM(x856.f2)+ "  " + ALLTRIM(x856.f3)

					CASE ALLTRIM(x856.segment) == "LX"
						* The 1st time we encounter seg LX,
						* we do not need to append blank because this info can go
						* into the record we appended initially.
						* BUT...if we encounter this again, we do want to append
						* because we're in a different set of detail records.
						* Hence the llNewRec flag which was initialized to .F. for Segment ST
						IF llNewRec THEN
							SELECT x754
							APPEND BLANK
							REPLACE detrecno WITH RECNO()
						ELSE
							* set flag so that we'll append new rec every time we get here from now on
							llNewRec = .T.
						ENDIF
						REPLACE x754.lxnum WITH VAL(ALLTRIM(x856.f1))  && 11/18/03 mb

					CASE ALLTRIM(x856.segment) == "L11" AND ALLTRIM(x856.f2) == "RRC"
						REPLACE x754.rrc_num WITH x856.f1

					CASE ALLTRIM(x856.segment) == "L11" AND ALLTRIM(x856.f2) == "PUA"
						REPLACE x754.LoadID WITH x856.f1

					CASE ALLTRIM(x856.segment) == "L11" AND ALLTRIM(x856.f2) == "BM" AND INLIST(.cCompanyCode,TKONJCODE,TKOCACODE,TKOFLCODE)
						* TKO seems to be giving us loadid in non-standard segment, per Juan 4/21/2016
						REPLACE x754.LoadID WITH x856.f1

					CASE ALLTRIM(x856.segment) == "BLR"
						* put scac code and scac name into x754
						m.scaccode = ALLTRIM(x856.f1)
				
*!*							* added 6/15/2011 MB to force a particular SCAC code
*!*							IF NOT EMPTY(.cForcedSCACCode) THEN
*!*								m.scaccode = .cForcedSCACCode
*!*							ENDIF
				
						xsqlexec("select dispname from scac where scac='"+m.scaccode+"'","scac",,"wh")
						m.scacname=scac.dispname
						***********************************
						* 11/17/03 MB
						IF EMPTY(m.scacname) THEN
							.RememberBadSCACCode(m.scaccode)
						ENDIF
						*SELECT scactable
						*LOCATE
						***********************************
						REPLACE x754.scaccode WITH m.scaccode, ;
							x754.scacname WITH m.scacname

					CASE ALLTRIM(x856.segment) == "OID"
						* There can be multiple of these records per detail section,
						* so we need to append a new rec if we just did one of these.
						* NOTE: because the segments with pickupdate and storenum come AFTER
						* the OID segment, only the last of multiple PO_NUM recs for a RRC_NUM in x754
						* will have pickupdate and storenum populated. There is code later on
						* to back fill the blank fields correctly.
						IF lcPreviousSegment == "OID" THEN
							* add new record
							SELECT x754
							SCATTER MEMVAR
							APPEND BLANK
							GATHER MEMVAR
							REPLACE x754.po_num WITH x856.f2, detrecno WITH RECNO()

						ELSE
							REPLACE x754.po_num WITH x856.f2
						ENDIF

					CASE ALLTRIM(x856.segment) == "G62"
						lcPkupDate = x856.f2
						lcTempdate = SUBSTR(lcPkupDate,5,2)+"/"+SUBSTR(lcPkupDate,7,2)+"/"+SUBSTR(lcPkupDate,1,4)
						ldPkupDate = CTOD(lcTempdate)
						IF VAL(SUBSTR(x856.f4,1,2)) > 12
							lnHour = VAL(SUBSTR(x856.f4,1,2)) - 12
							llTimeIsPM = .T.
						ELSE
							lnHour = VAL(SUBSTR(x856.f4,1,2))
							llTimeIsPM=.F.
						ENDIF
						*!*							REPLACE x754.PkupDate WITH ldPkupDate, ;
						*!*								x754.pkuptime WITH ALLTRIM(STR(lnHour))+":"+SUBSTR(x856.f4,3,2)+" "+IIF(llTimeIsPM,"pm","am")
						REPLACE x754.PkupDate WITH ldPkupDate, ;
							x754.PkupTime WITH ALLTRIM(STR(lnHour))+SUBSTR(x856.f4,3,2)+" "+IIF(llTimeIsPM,"P","A")

					CASE ALLTRIM(x856.segment) == "N1" AND ALLTRIM(x856.f1) == "ST"
						REPLACE x754.storenum WITH x856.f4 IN x754

					OTHERWISE
						* ignore the segment
				ENDCASE

				* "remember" the segment for next pass
				IF NOT EMPTY(ALLTRIM(x856.segment)) THEN
					lcPreviousSegment = ALLTRIM(x856.segment)
				ENDIF

			ENDSCAN

			*USE IN scactable

			*!*	SELECT x754
			*!*	GOTO TOP
			*!*	BROWSE
			*!*				QUIT

			*!*				* go back and fill in any blank pickupdates and storenums
			*!*				SELECT x754
			*!*				GOTO TOP
			*!*				SCAN
			*!*					SELECT x754
			*!*					STORE RECNO() TO lnHoldRec
			*!*					IF EMPTY(x754.PkupDate)
			*!*						STORE x754.rrc_num TO lcThisRRC
			*!*						IF USED('temp754')
			*!*							USE IN temp754
			*!*						ENDIF
			*!*						SELECT * FROM x754 WHERE x754.rrc_num == lcThisRRC AND !EMPTY(x754.PkupDate) ;
			*!*							INTO CURSOR temp754
			*!*
			*!*						SELECT temp754
			*!*						LOCATE
			*!*
			*!*						SELECT x754
			*!*						REPLACE ALL x754.PkupDate WITH temp754.PkupDate, ;
			*!*							x754.PkupTime WITH temp754.PkupTime, ;
			*!*							x754.storenum WITH temp754.storenum ;
			*!*							FOR x754.rrc_num = lcThisRRC ;
			*!*							IN x754
			*!*					ENDIF
			*!*					SELECT x754
			*!*					GOTO lnHoldRec
			*!*				ENDSCAN

			* go back and fill in any blank pickupdates and storenums
			IF USED('temp754')
				USE IN temp754
			ENDIF


			* PKUPTIME??
			SELECT DISTINCT rrc_num, PkupDate, storenum ;
				FROM x754 ;
				INTO CURSOR temp754 ;
				WHERE NOT EMPTY(PkupDate) AND NOT EMPTY(storenum)

			*!*				SELECT temp754
			*!*				BROWSE

			SELECT temp754
			SCAN
				*!*					SELECT x754
				STORE temp754.rrc_num TO lcThisRRC
				REPLACE ALL x754.PkupDate WITH temp754.PkupDate, ;
					x754.storenum WITH temp754.storenum ;
					FOR x754.rrc_num == lcThisRRC ;
					AND ( EMPTY(x754.PkupDate) OR EMPTY(x754.storenum) ) ;
					IN x754
			ENDSCAN

			IF USED('temp754')
				USE IN temp754
			ENDIF

			*!*	SELECT x754
			*!*	GOTO TOP
			*!*	BROWSE


			*********************************************************************************
			* 1/30/06 MB to handle problem with blank shipfrom on report
			* go back and fill in blank ship froms (only the first one was being populated)
			lcSHIPFROM = ""
			SELECT x754
			SCAN
				IF NOT EMPTY(x754.shipfrom) THEN
					* remember it
					lcSHIPFROM = x754.shipfrom
				ELSE
					* it was empty, replace it if we have a NON-EMPTY shipfrom variable
					IF NOT EMPTY(lcSHIPFROM) THEN
						REPLACE x754.shipfrom WITH lcSHIPFROM
					ENDIF
				ENDIF
			ENDSCAN
			*********************************************************************************

			SELECT x754
			INDEX ON placecode TAG placecode
			INDEX ON rrc_num TAG rrc_num
		ENDWITH
	ENDPROC  &&  Load754


	***********************************************************************************************************************
	PROCEDURE LogBadSCACCodes
		* list any SCACCodes that couldn't be found in SCAC table.
		LOCAL lcBadSCACCodes, lnBadSCACCodes
		IF USED('curSCACCodesNotFound') THEN
			SELECT curSCACCodesNotFound
			GOTO TOP
			lnBadSCACCodes = RECCOUNT()
			lcBadSCACCodes = ''
			SCAN
				IF EMPTY(lcBadSCACCodes) THEN
					lcBadSCACCodes = "WARNING: " + TRANSFORM(lnBadSCACCodes) + " SCAC Codes could not be found in the SCAC table." + CRLF + CRLF
					lcBadSCACCodes = lcBadSCACCodes + "SCAC Code" + CRLF
					lcBadSCACCodes = lcBadSCACCodes + "---------" + CRLF
				ENDIF
				lcBadSCACCodes = lcBadSCACCodes + curSCACCodesNotFound.scaccode + CRLF
			ENDSCAN
			.TrackProgress(lcBadSCACCodes, LOGIT+SENDIT)
		ENDIF  &&  USED('curSCACCodesNotFound') THEN
	ENDPROC
	***********************************************************************************************************************
	PROCEDURE LogBadSCACNames
		* list any SCAC Names that couldn't be found in ship via table.
		LOCAL lcBadSCACNames, lnBadSCACNames
		IF USED('curSCACNamesNotFound') THEN
			SELECT curSCACNamesNotFound
			GOTO TOP
			lnBadSCACNames = RECCOUNT()
			lcBadSCACNames = ''
			SCAN
				IF EMPTY(lcBadSCACNames) THEN
					lcBadSCACNames = "WARNING: " + TRANSFORM(lnBadSCACNames) + " SCAC Names could not be found in the ship via tables." + CRLF + CRLF
					lcBadSCACNames = lcBadSCACNames + "Company Code     SCAC Name                           Ship Via Table" + CRLF
					lcBadSCACNames = lcBadSCACNames + "------------     -------------------------------     -----------------------------------" + CRLF
				ENDIF
				lcBadSCACNames = lcBadSCACNames + PADR(curSCACNamesNotFound.cCompany,17) + curSCACNamesNotFound.scacname + SPACE(5) + ;
					ALLTRIM(curSCACNamesNotFound.cShipVia) + CRLF
			ENDSCAN
			.TrackProgress(lcBadSCACNames, LOGIT+SENDIT)
		ENDIF  &&  USED('curSCACNamesNotFound') THEN
	ENDPROC


	***********************************************************************************************************************
	PROCEDURE LogRecsNotFound
		* list any RRC_NUM/PO_NUMs or RRC_NUM/SHIP_REFs that couldn't be found in any outship table in internal email.
		LOCAL lcRecsNotFound, lnRecsNotFound
		WITH THIS
			IF USED('x754') THEN
				lcRecsNotFound = ''
				SELECT x754
				COUNT FOR EMPTY(placecode) TO lnRecsNotFound
				DO CASE
					CASE INLIST(.cCompanyCode,MORETFLCODE,MORETMLCODE,MORETSPCODE) AND .lMoretFlatFileFormat
						* handle new Moret flat file format
						*!*					DELETE TAG ALL
						INDEX ON (rrc_num + ship_ref) TAG rrc_ship FOR EMPTY(placecode)
						SET ORDER TO rrc_ship
						GOTO TOP
						SCAN
							IF EMPTY(lcRecsNotFound) THEN
								lcRecsNotFound = "ERROR: " + TRANSFORM(lnRecsNotFound) + " RRC_NUM/SHIP_REF pairs could not be found in any outship table." + CRLF + CRLF
								lcRecsNotFound = lcRecsNotFound + "RRC#                          SHIP_REF#" + CRLF
								lcRecsNotFound = lcRecsNotFound + "-------------------------     --------------------" + CRLF
							ENDIF
							lcRecsNotFound = lcRecsNotFound + x754.rrc_num + SPACE(5) + x754.ship_ref + CRLF
						ENDSCAN
					OTHERWISE
						* standard case
						*!*					DELETE TAG ALL
						INDEX ON (rrc_num + po_num) TAG rrc_po FOR EMPTY(placecode)
						SET ORDER TO rrc_po
						GOTO TOP
						SCAN
							IF EMPTY(lcRecsNotFound) THEN
								lcRecsNotFound = "ERROR: " + TRANSFORM(lnRecsNotFound) + " RRC_NUM/PO_NUM pairs could not be found in any outship table." + CRLF + CRLF
								lcRecsNotFound = lcRecsNotFound + "RRC#                          PO#" + CRLF
								lcRecsNotFound = lcRecsNotFound + "-------------------------     --------------------" + CRLF
							ENDIF
							lcRecsNotFound = lcRecsNotFound + x754.rrc_num + SPACE(5) + x754.po_num + CRLF
						ENDSCAN
				ENDCASE
				.TrackProgress(lcRecsNotFound, LOGIT+SENDIT)
				.lRecsNotFound = NOT EMPTY(lcRecsNotFound)
			ENDIF  &&  USED('x754') THEN
		ENDWITH
	ENDPROC


	***********************************************************************************************************************
	FUNCTION x754RecordDataIsOkay
		LOCAL llRetVal, lcMessage
		llRetVal = .T.
		WITH THIS
			IF EMPTY(x754.rrc_num)
				llRetVal = .F.
				lcMessage = "INVALID DATA: Empty RRC_NUM in x754 LX #: " + TRANSFORM(x754.lxnum)
				.TrackProgress(lcMessage, LOGIT+SENDIT)
			ENDIF
			IF EMPTY(x754.scacname)
				llRetVal = .F.
				lcMessage = "INVALID DATA: Empty SCACNAME in x754 for RRC_NUM: " + x754.rrc_num
				.TrackProgress(lcMessage, LOGIT+SENDIT)
			ENDIF
			IF NOT llRetVal THEN
				.lx754DataErrors = .T.
			ENDIF
		ENDWITH
		RETURN llRetVal
	ENDFUNC


	*!*		FUNCTION ADDLINEFEEDS
	*!*			LPARAMETERS tc754File, tcSaraLeeOriginalFile
	*!*			LOCAL llRetVal, lcFileString, lnBytesWritten
	*!*			llRetVal = .T.
	*!*			* first, back up original file before we mess with it
	*!*			COPY FILE (tc754File) TO (tcSaraLeeOriginalFile)
	*!*			* NOW PROCESS
	*!*			* load
	*!*			lcFileString = FILETOSTR(tc754File)
	*!*			* convert, adding the missing LFs
	*!*			lcFileString = STRTRAN(lcFileString,CHR(13),CHR(13)+CHR(10))
	*!*			* rewrite file
	*!*			lnBytesWritten = STRTOFILE(lcFileString,tc754File)
	*!*			IF lnBytesWritten < 1 THEN
	*!*				llRetVal = .F.
	*!*			ENDIF
	*!*			.TrackProgress('In ADDLINEFEEDS(), for file ' + tc754File + ' bytes written = ' + TRANSFORM(lnBytesWritten), LOGIT)
	*!*			RETURN llRetVal
	*!*		ENDFUNC


	FUNCTION GetEOLChar
		* PG says that the EOL CHAR is always the 106th character on the 1st line. 3/20/08 MB.
		LPARAMETERS tcOriginalFile
		LOCAL lcEOLChar, lcFileString
		lcFileString = FILETOSTR(tcOriginalFile)
		lcEOLChar = SUBSTR(lcFileString,106,1)
		.TrackProgress('In GetEOLChar(), for file ' + tcOriginalFile + ' EOL Char = ' + lcEOLChar + " CHR(" + TRANSFORM(ASC(lcEOLChar)) + ")", LOGIT+SENDIT)
		RETURN lcEOLChar
	ENDFUNC


	FUNCTION GetFieldDelimiter
		* PG says that the field delimiter is always the 4th character on the 1st line. 3/20/08 MB.
		LPARAMETERS tcOriginalFile
		LOCAL lcFieldDelimiter, lcFileString
		lcFileString = FILETOSTR(tcOriginalFile)
		lcFieldDelimiter = SUBSTR(lcFileString,4,1)
		.TrackProgress('In GetFieldDelimiter(), for file ' + tcOriginalFile + ' Field Delimiter = ' + lcFieldDelimiter + " CHR(" + TRANSFORM(ASC(lcFieldDelimiter)) + ")", LOGIT+SENDIT)
		RETURN lcFieldDelimiter
	ENDFUNC

	FUNCTION ConvertForSuccess
		LPARAMETERS tc754File
		LOCAL llRetVal, lcFileString, lnBytesWritten
		llRetVal = .T.
		* load
		lcFileString = FILETOSTR(tc754File)
		* convert, changing chr(197) to nulls
		lcFileString = STRTRAN(lcFileString,CHR(0xC5),CHR(0x15))
		*lcFileString = STRTRAN(lcFileString,CHR(0x7E),CHR(13)+CHR(10))
		* rewrite file
		lnBytesWritten = STRTOFILE(lcFileString,tc754File)
		IF lnBytesWritten < 1 THEN
			llRetVal = .F.
		ENDIF
		.TrackProgress('In ConvertForSuccess(), for file ' + tc754File + ' bytes written = ' + TRANSFORM(lnBytesWritten), LOGIT)
		RETURN llRetVal
	ENDFUNC


	***********************************************************************************************************************
	FUNCTION ADDLINEFEEDS
		LPARAMETERS tc754File
		LOCAL llRetVal, lcFileString, lnBytesWritten
		llRetVal = .T.
		* load
		lcFileString = FILETOSTR(tc754File)
		* convert, adding the missing LFs
		lcFileString = STRTRAN(lcFileString,CHR(13),CHR(13)+CHR(10))
		* rewrite file
		lnBytesWritten = STRTOFILE(lcFileString,tc754File)
		IF lnBytesWritten < 1 THEN
			llRetVal = .F.
		ENDIF
		.TrackProgress('In ADDLINEFEEDS(), for file ' + tc754File + ' bytes written = ' + TRANSFORM(lnBytesWritten), LOGIT)
		RETURN llRetVal
	ENDFUNC
	***********************************************************************************************************************
	FUNCTION ConvertForNanjing
		LPARAMETERS tc754File
		LOCAL llRetVal, lcFileString, lnBytesWritten
		llRetVal = .T.
		* load
		lcFileString = FILETOSTR(tc754File)
		* convert, adding the missing LFs
		*lcFileString = STRTRAN(lcFileString,CHR(0x85),CHR(13)+CHR(10))
		* do for hex 73 what we used to do for hex 85 AND convert hex 7F to hex 07 per Paul 4/5/07 MB
		lcFileString = STRTRAN(lcFileString,CHR(0x7E),CHR(13)+CHR(10))
		*lcFileString = STRTRAN(lcFileString,CHR(0x7F),CHR(0x07))
		* rewrite file
		lnBytesWritten = STRTOFILE(lcFileString,tc754File)
		IF lnBytesWritten < 1 THEN
			llRetVal = .F.
		ENDIF
		.TrackProgress('In ConvertForNanjing(), for file ' + tc754File + ' bytes written = ' + TRANSFORM(lnBytesWritten), LOGIT)
		RETURN llRetVal
	ENDFUNC
	***********************************************************************************************************************
	FUNCTION ConvertForMoret
		LPARAMETERS tc754File, tcEOLChar
		LOCAL llRetVal, lcFileString, lnBytesWritten
		llRetVal = .T.
		* load
		lcFileString = FILETOSTR(tc754File)
		lcFileString = STRTRAN(lcFileString,tcEOLChar,CHR(13)+CHR(10))
		* rewrite file
		lnBytesWritten = STRTOFILE(lcFileString,tc754File)
		IF lnBytesWritten < 1 THEN
			llRetVal = .F.
		ENDIF
		.TrackProgress('In ConvertForMoret(), for file ' + tc754File + ' bytes written = ' + TRANSFORM(lnBytesWritten), LOGIT)
		RETURN llRetVal
	ENDFUNC
	***********************************************************************************************************************
	FUNCTION ConvertEOLCharToCRLF
		LPARAMETERS tc754File, tcEOLChar
		LOCAL llRetVal, lcFileString, lnBytesWritten
		llRetVal = .T.
		* load
		lcFileString = FILETOSTR(tc754File)
		lcFileString = STRTRAN(lcFileString,tcEOLChar,CHR(13)+CHR(10))
		* rewrite file
		lnBytesWritten = STRTOFILE(lcFileString,tc754File)
		IF lnBytesWritten < 1 THEN
			llRetVal = .F.
		ENDIF
		.TrackProgress('In ConvertEOLCharToCRLF(), for file ' + tc754File + ' bytes written = ' + TRANSFORM(lnBytesWritten), LOGIT)
		RETURN llRetVal
	ENDFUNC
	***********************************************************************************************************************
	FUNCTION ConvertForJQUEEN
		LPARAMETERS tc754File
		LOCAL llRetVal, lcFileString, lnBytesWritten
		llRetVal = .T.
		* load
		lcFileString = FILETOSTR(tc754File)
		lcFileString = STRTRAN(lcFileString,CHR(0x85),CHR(7)+CHR(13)+CHR(10))
		* rewrite file
		lnBytesWritten = STRTOFILE(lcFileString,tc754File)
		IF lnBytesWritten < 1 THEN
			llRetVal = .F.
		ENDIF
		.TrackProgress('In ConvertForMoret(), for file ' + tc754File + ' bytes written = ' + TRANSFORM(lnBytesWritten), LOGIT)
		RETURN llRetVal
	ENDFUNC
	***********************************************************************************************************************
	FUNCTION ConvertForPerryEllis
		* remove chr(133), i.e. 85 hex
		LPARAMETERS tc754File
		LOCAL llRetVal, lcFileString, lnBytesWritten
		llRetVal = .T.
		* load
		lcFileString = FILETOSTR(tc754File)
		lcFileString = STRTRAN(lcFileString,CHR(0x85),"")
		* rewrite file
		lnBytesWritten = STRTOFILE(lcFileString,tc754File)
		IF lnBytesWritten < 1 THEN
			llRetVal = .F.
		ENDIF
		.TrackProgress('In ConvertForPerryEllis(), for file ' + tc754File + ' bytes written = ' + TRANSFORM(lnBytesWritten), LOGIT)
		RETURN llRetVal
	ENDFUNC
	***********************************************************************************************************************
	***********************************************************************************************************************
	FUNCTION ConvertForTKO
		* remove chr(21), i.e. 15 hex
		LPARAMETERS tc754File
		LOCAL llRetVal, lcFileString, lnBytesWritten
		llRetVal = .T.
		* load
		lcFileString = FILETOSTR(tc754File)
		lcFileString = STRTRAN(lcFileString,CHR(0x15),"")
		* rewrite file
		lnBytesWritten = STRTOFILE(lcFileString,tc754File)
		IF lnBytesWritten < 1 THEN
			llRetVal = .F.
		ENDIF
		.TrackProgress('In ConvertForTKO(), for file ' + tc754File + ' bytes written = ' + TRANSFORM(lnBytesWritten), LOGIT)
		RETURN llRetVal
	ENDFUNC
	***********************************************************************************************************************
	***********************************************************************************************************************
	FUNCTION ConvertForDODENIM
		LPARAMETERS tc754File
		* COONVERT hex 85 to CRLF
		LOCAL llRetVal, lcFileString, lnBytesWritten
		llRetVal = .T.
		* load
		lcFileString = FILETOSTR(tc754File)
		lcFileString = STRTRAN(lcFileString,CHR(0x85),CHR(13)+CHR(10))
		* rewrite file
		lnBytesWritten = STRTOFILE(lcFileString,tc754File)
		IF lnBytesWritten < 1 THEN
			llRetVal = .F.
		ENDIF
		.TrackProgress('In ConvertForDODENIM(), for file ' + tc754File + ' bytes written = ' + TRANSFORM(lnBytesWritten), LOGIT)
		RETURN llRetVal
	ENDFUNC
	***********************************************************************************************************************
	***********************************************************************************************************************
	PROCEDURE TrackProgress
		* do any combination of Wait Window, writing to logfile, and adding to body of email,
		* based on nFlags parameter.
		LPARAMETERS tcExpression, tnFlags
		LOCAL lcWaitHeader, lcWaitExpression
		WITH THIS
			lcWaitHeader = .cWaitHeader
			IF BITAND(tnFlags,LOGIT) = LOGIT THEN
				IF .lLoggingIsOn THEN
					?
					? .cProcessName + "  " + TTOC(DATETIME()) + ": " + tcExpression
				ENDIF
			ENDIF
			IF .lWaitWindowIsOn THEN
				lcWaitExpression = tcExpression
				IF LEN(lcWaitExpression) > 150 THEN
					lcWaitExpression = LEFT(lcWaitExpression,150)
				ENDIF
				* identify file being processed in all Wait Windows
				IF BITAND(tnFlags,NOWAITIT) = NOWAITIT THEN
					*WAIT WINDOW tcExpression NOWAIT
					WAIT WINDOW (lcWaitHeader + CRLF + lcWaitExpression) NOWAIT
				ENDIF
				IF BITAND(tnFlags,WAITIT) = WAITIT THEN
					*WAIT WINDOW tcExpression TIMEOUT .nWaitWindowTimeout
					WAIT WINDOW (lcWaitHeader + CRLF + lcWaitExpression) TIMEOUT .nWaitWindowTimeout
				ENDIF
			ENDIF
			IF BITAND(tnFlags,SENDIT) = SENDIT THEN
				IF .lSendInternalEmailIsOn THEN
					.cBodyText = .cBodyText + tcExpression + CRLF + CRLF
				ENDIF
			ENDIF
		ENDWITH
	ENDPROC  &&  TrackProgress


	***********************************************************************************************************************
	PROCEDURE SortArrayByDateTime
		* expects taInArray[] to be the result of an ADIR()
		LPARAMETERS taInArray, taOutArray
		EXTERNAL ARRAY taInArray, taOutArray
		LOCAL lnRows, lnCols, i, j
		* extending taInArray[] to process the files in date/time order
		lnRows = ALEN(taInArray,1)
		lnCols = ALEN(taInArray,2)
		* create taOutArray with one more column
		DIMENSION taOutArray[lnRows,lnCols + 1]
		* and fill it with taInArray columns AND (filedate + filetime) in new column
		* ( ACOPY() doesn't preserve elements properly because of different # of columns )
		FOR i = 1 TO lnRows
			FOR j = 1 TO lnCols
				taOutArray[i,j] = taInArray[i,j]
			ENDFOR
			taOutArray[i,lnCols + 1] = DTOS(taInArray[i,3]) + "_" + taInArray[i,4]
		ENDFOR
		* sort taOutArray on date/time column
		=ASORT(taOutArray,6)
	ENDPROC


	***********************************************************************************************************************
	PROCEDURE SetUpShipTablesArray()

		** rewritten to support table-based offices/regions per Darren 10/24/06 MB

		* about the Outship array:
		* column 1 holds the respective regional Outship table names
		* column 2 holds their database container names
		* column 3 holds the respective regional place codes, or State Codes
		* column 4 holds the email 'send to' contact for the respective region/place.
		* column 5 holds the respective regional shipvia table names
		* column 6 holds the respective regional apptcall table names
		* column 7 holds the respective regional Genpk table names
		* column 8 holds the respective regional Outwolog table names
		* column 9 holds the respective regional Office Code, like N, M, C, 1, 2, L
		* column 10 holds the respective regional BL tables names
		* column 11 holds the rateoffice from whoffice

		LOCAL lnRows, i, lcOffice, lcSendTo, lcFolder, lcPlaceCode, lcRateOffice

		WITH THIS

			* redimension the array based on number of non-test offices in F:\WH\WHOFFICE

			IF USED('CURWHOFFICE') THEN
				USE IN CURWHOFFICE
			ENDIF

			SELECT * ;
				FROM (.cWHOfficeTable) ;
				INTO CURSOR CURWHOFFICE ;
				WHERE (NOT TEST) ;
				ORDER BY OFFICE

*!*					WHERE (NOT TEST) OR UPPER(ALLTRIM(OFFICE)) = "P" ;


			lnRows = RECCOUNT('CURWHOFFICE')
			DIMENSION .aOutShipTables(lnRows,12)

			IF USED('WHOFFICE') THEN
				USE IN WHOFFICE
			ENDIF

			*!*				SELECT CURWHOFFICE
			*!*				BROWSE


*SET STEP ON 
			* derive table locations, etc. based on WHOFFICE information
			i = 0
			SELECT CURWHOFFICE
			SCAN FOR (NOT TEST) &&  OR UPPER(ALLTRIM(OFFICE)) = "P"
				i = i + 1

				lcOffice = UPPER(ALLTRIM(CURWHOFFICE.OFFICE))
				lcFolder = UPPER(ALLTRIM(CURWHOFFICE.FOLDER))
				lcRateOffice = UPPER(ALLTRIM(CURWHOFFICE.RATEOFFICE))
				lcMod = lcOffice

				* outship table
				.aOutShipTables( i, 1) = lcFolder + "\WHDATA\OUTSHIP"

				* outship DB
				.aOutShipTables( i, 2) = lcFolder + "\WHDATA\WH"

				* Place Code
				*!*	DO CASE
				*!*		CASE lcOffice == "N"
				*!*			lcPlaceCode = "NJ------"
				*!*		CASE lcOffice == "M"
				*!*			lcPlaceCode = "FL------"
				*!*		CASE lcOffice == "C"
				*!*			lcPlaceCode = "CA------"
				*!*		CASE lcOffice == "1"	&& SP Mod A
				*!*			lcPlaceCode = "SP-MOD-A"
				*!*		CASE lcOffice == "2"	&& SP MOD D
				*!*			lcPlaceCode = "SP-MOD-D"
				*!*		CASE lcOffice == "3"	&& TUG
				*!*			lcPlaceCode = "TUG-----"
				*!*		CASE lcOffice == "L"	&& Mira Loma
				*!*			lcPlaceCode = "ML------"
				*!*		CASE lcOffice == "5"	&& SP MOD C
				*!*			lcPlaceCode = "SP-MOD-C"
				*!*		CASE lcOffice == "P"	&& SP TEST MOD 
				*!*			lcPlaceCode = "SP-MOD-X"
				*!*		OTHERWISE
				*!*			lcPlaceCode = "--------"
				*!*	ENDCASE
				lcPlaceCode = ALLTRIM(CURWHOFFICE.DESCRIP)
				.aOutShipTables( i, 3) = lcPlaceCode

				* Email Send TOs
				*!*	DO CASE
				*!*		CASE lcOffice == "N"
				*!*			lcSendTo = .cNYSendTo
				*!*		CASE lcOffice == "M"
				*!*			lcSendTo = .cFLSendTo
				*!*		CASE lcOffice == "C"
				*!*			lcSendTo = .cCASendTo
				*!*		CASE lcOffice == "1"
				*!*			lcSendTo = .cCASendTo
				*!*		CASE lcOffice == "2"
				*!*			lcSendTo = .cCASendTo
				*!*		CASE lcOffice == "3"
				*!*			lcSendTo = .cTUGSendTo
				*!*		CASE lcOffice == "L"
				*!*			lcSendTo = .cMLSendTo
				*!*		CASE lcOffice == "5"
				*!*			lcSendTo = .cCASendTo
				*!*		CASE lcOffice == "P"
				*!*			lcSendTo = .cCASendTo
				*!*		OTHERWISE
				*!*			lcSendTo = 'mbennett@fmiint.com'
				*!*	ENDCASE
				DO CASE
					*CASE lcRateOffice == "C"
					CASE INLIST(lcRateOffice,"C","X","O","Y","Z")
						lcSendTo = .cCASendTo
					CASE lcRateOffice == "L"
						lcSendTo = .cMLSendTo
					CASE lcRateOffice == "M"
						lcSendTo = .cFLSendTo
					CASE lcRateOffice == "N"
						lcSendTo = .cNYSendTo
					OTHERWISE
						.TrackProgress('lcSendTo = ' + lcSendTo, LOGIT+SENDIT)
						lcSendTo = 'mbennett@fmiint.com'
				ENDCASE				
				
				.aOutShipTables( i, 4) = lcSendTo

				** NOT CURRENTLY USED OR DEFINED 10/23/06 MB
				*!*	* Ship Via tables
				*!*	.aOutShipTables( i, 5) = lcFolder + "\WHDATA\SHIPVIA"

				* Appointment Tables
				.aOutShipTables( i, 6) = lcFolder + "\WHDATA\APPTCALL"

				* Framework GenPK table
				.aOutShipTables( i, 7) = lcFolder + "\WHDATA\WHGENPK"

				* OutWoLog tables
				.aOutShipTables( i, 8) = lcFolder + "\WHDATA\OUTWOLOG"

				* Office
				.aOutShipTables( i, 9) = lcOffice

				* OutWoLog tables
				.aOutShipTables( i, 10) = lcFolder + "\WHDATA\BL"
				
				* added 11 and 12 to support apptcall going to sql 2/18/2016 MB
				.aOutShipTables( i, 11) = lcRateOffice				
				.aOutShipTables( i, 12) = lcMod

			ENDSCAN
			
			IF USED('CURWHOFFICE') THEN
				USE IN CURWHOFFICE
			ENDIF
		ENDWITH
	ENDPROC

	***********************************************************************************************************************
	PROCEDURE CreateX754Cursor
		* x754 & y754 cursors should have identical structures.
		* x754 holds data specific to each 754 file being processed.
		* y754 is used to accumulate x754 data for all files processed, for the reporting phase.
		* changed STORENUM from c(5) TO C(10) 9/28/07 mb
		CREATE CURSOR x754 ( ;
			wo_num    N(8) ,;
			wo_date   d    ,;
			PkupTime  c(15),;
			PkupDate  d    ,;
			consignee c(35),;
			shipfrom  c(30),;
			ship_ref  c(20),;
			storenum  c(10) ,;
			rrc_num   c(25),;
			po_num    c(20),;
			cnee_ref  c(20),;
			scaccode  c(4) ,;
			scacname  c(31),;
			placecode c(20) ,;
			rrcdocid  c(15),;
			LoadID    c(15),;
			qty       N(6) ,;
			detrecno  i    ,;
			lxnum     i    ,;
			pt        c(10),;
			hll c(1))
	ENDPROC


	***********************************************************************************************************************
	PROCEDURE CreateY754Cursor
		* x754 & y754 cursors should have identical structures.
		* x754 holds data specific to each 754 file being processed.
		* y754 is used to accumulate x754 data for all files processed, for the reporting phase.
		* changed STORENUM from c(5) TO C(10) 9/28/07 mb
		CREATE CURSOR y754 ( ;
			wo_num    N(8) ,;
			wo_date   d    ,;
			PkupTime  c(15),;
			PkupDate  d    ,;
			consignee c(35),;
			shipfrom  c(30),;
			ship_ref  c(20),;
			storenum  c(10) ,;
			rrc_num   c(25),;
			po_num    c(20),;
			cnee_ref  c(20),;
			scaccode  c(4) ,;
			scacname  c(31),;
			placecode c(20) ,;
			rrcdocid  c(15),;
			LoadID    c(15),;
			qty       N(6) ,;
			detrecno  i    ,;
			lxnum     i    ,;
			pt        c(10),;
			hll c(1))
	ENDPROC

	***********************************************************************************************************************
	PROCEDURE CreateSCACCodeCursor
		* this cursor holds SCAC codes we couldn't find
		CREATE CURSOR curSCACCodesNotFound (scaccode c(4))
		INDEX ON scaccode TAG scaccode
		SET ORDER TO scaccode
	ENDPROC

	***********************************************************************************************************************
	PROCEDURE CreateSCACNameCursor
		* this cursor holds SCAC names we couldn't find
		CREATE CURSOR curSCACNamesNotFound (cCompany c(1), scacname c(31), cShipVia M)
		INDEX ON cCompany TAG cCompany
		INDEX ON scacname TAG scacname
	ENDPROC

	***********************************************************************************************************************
	PROCEDURE RememberBadSCACCode
		* build list of unique SCACCODES that we couldn't find the names for.
		LPARAMETERS tcSCACCODE
		LOCAL lnSelect
		lnSelect = SELECT()
		SELECT curSCACCodesNotFound
		SEEK tcSCACCODE
		IF NOT FOUND() THEN
			APPEND BLANK
			REPLACE scaccode WITH tcSCACCODE
		ENDIF
		SELECT (lnSelect)
	ENDPROC
	***********************************************************************************************************************
	PROCEDURE RememberBadSCACName
		* build list of unique SCAC Names that we couldn't find the ASN Codes for in the ShipVia tables.
		LPARAMETERS tcCompanyCode, tcScacname, tcShipViaTable
		LOCAL lnSelect
		lnSelect = SELECT()
		SELECT curSCACNamesNotFound
		LOCATE FOR cCompany == tcCompanyCode AND scacname == tcScacname
		IF NOT FOUND() THEN
			APPEND BLANK
			REPLACE cCompany WITH tcCompanyCode, scacname WITH tcScacname, cShipVia WITH tcShipViaTable
		ENDIF
		SELECT (lnSelect)
	ENDPROC
	***********************************************************************************************************************
	PROCEDURE SetCompanyCode
		LPARAMETERS tcCompanyCode
		LOCAL lcType
		lcType = TYPE('tcCompanyCode')
		WITH THIS
			.TrackProgress("---------------------------- New 754 Process Started ---------------------------", LOGIT+SENDIT)
			.TrackProgress('COMPUTERNAME = ' + .cCOMPUTERNAME, LOGIT+SENDIT)
			.TrackProgress('PROJECT = proc754', LOGIT+SENDIT)
			IF .lTestMode THEN
				.TrackProgress("----------------------------        TEST MODE        ---------------------------", LOGIT+SENDIT)
			ENDIF
			DO CASE
				CASE lcType = 'L'
					.TrackProgress("Empty or no Company Code parameter passed", LOGIT+SENDIT)
				CASE lcType = 'C'
					.cCompanyCode = tcCompanyCode
					.TrackProgress("Company Code parameter passed = [" + tcCompanyCode + "]", LOGIT+SENDIT)
					* change company-specific properties here (they default to cCompanyCode = REGATTACODE)
					DO CASE
						CASE .cCompanyCode = AGEGROUPNJCODE
							.cInPath = 'F:\FTPUSERS\AGEGROUP-NJ\754IN\'
							.cArchivePath = 'F:\FTPUSERS\AGEGROUP-NJ\754 ARCHIVE\'
							.cOriginalsPath = 'F:\FTPUSERS\AGEGROUP-NJ\754 ORIGINAL\'
							IF .lTestMode THEN
								.cInPath = 'F:\FTPUSERS\AGEGROUP-NJ\754 TEST\'
							ELSE
								.cFLSendTo = 'Joe.Abbate@Tollgroup.com, karen.zelaya@Tollgroup.com, patricia.zelaya@tollgroup.com,  njrouting@fmiint.com, ed@agegroupltd.com, dawn@agegroup.com, Arnold@agegroup.com'
								.cCASendTo = 'myrnaly.rivera@Tollgroup.com, njrouting@fmiint.com, ed@agegroupltd.com, dawn@agegroup.com, Arnold@agegroup.com, Flerida.donado@tollgroup.com, Andrea.Rico@tollgroup.com, Maria.estrella@tollgroup.com'
								.cNYSendTo = 'CMalcolm@fmiint.com, myrnaly.rivera@Tollgroup.com, njrouting@fmiint.com, ed@agegroupltd.com, dawn@agegroup.com, Arnold@agegroup.com'
								.cMLSendTo = 'myrnaly.rivera@Tollgroup.com, njrouting@fmiint.com, ed@agegroupltd.com, dawn@agegroup.com, Arnold@agegroup.com, Flerida.donado@tollgroup.com, Andrea.Rico@tollgroup.com, Maria.estrella@tollgroup.com'
								.cTUGSendTo = 'myrnaly.rivera@Tollgroup.com, njrouting@fmiint.com, ed@agegroupltd.com, dawn@agegroup.com, Arnold@agegroup.com'
							ENDIF
						CASE .cCompanyCode = AGEGROUPCODE
							.cInPath = 'F:\FTPUSERS\AGEGROUP\754IN\'
							.cArchivePath = 'F:\FTPUSERS\AGEGROUP\754 Archive\'
							.cOriginalsPath = 'F:\FTPUSERS\AGEGROUP\754 Original\'
							IF .lTestMode THEN
								.cInPath = 'F:\FTPUSERS\AGEGROUP\754 Test\'
							ELSE
								.cFLSendTo = 'Joe.Abbate@Tollgroup.com, karen.zelaya@Tollgroup.com, patricia.zelaya@tollgroup.com, Andy.lynn@ronlynn.com, ed@agegroup.com, arnold@agegroup.com, dawn@agegroup.com, howard@agegroup.com, ed@agegroupltd.com'
								.cCASendTo = 'Andy.lynn@ronlynn.com, ed@agegroup.com, arnold@agegroup.com, dawn@agegroup.com, howard@agegroup.com, ed@agegroupltd.com, Flerida.donado@tollgroup.com, Andrea.Rico@tollgroup.com, Maria.estrella@tollgroup.com'
								.cNYSendTo = 'CMalcolm@fmiint.com, myrnaly.rivera@tollgroup.com, Claudine.blenke@tollgroup.com, Andy.lynn@ronlynn.com, ed@agegroup.com, arnold@agegroup.com, dawn@agegroup.com, howard@agegroup.com, jkillen@fmiint.com, ed@agegroupltd.com'
								.cMLSendTo = 'Andy.lynn@ronlynn.com, ed@agegroup.com, arnold@agegroup.com, dawn@agegroup.com, howard@agegroup.com, ed@agegroupltd.com, Flerida.donado@tollgroup.com, Andrea.Rico@tollgroup.com, Maria.estrella@tollgroup.com'
								.cTUGSendTo = 'pgaidis@fmiint.com, Andy.lynn@ronlynn.com, ed@agegroup.com, arnold@agegroup.com, dawn@agegroup.com, howard@agegroup.com, jkillen@fmiint.com, ed@agegroupltd.com'
							ENDIF
						CASE .cCompanyCode = BIOWORLDCODE
							.cInPath = 'F:\FTPUSERS\BIOWORLD\754IN\'
							.cArchivePath = 'F:\FTPUSERS\BIOWORLD\754IN\ARCHIVED\'
							.cOriginalsPath = 'F:\FTPUSERS\BIOWORLD\754IN\ORIGINAL\'
							IF .lTestMode THEN
								.cInPath = 'F:\FTPUSERS\BIOWORLD\754IN\754TEST\'
							ELSE
								.cFLSendTo = 'mark.bennett@tollgroup.com'
								.cCASendTo = 'mark.bennett@tollgroup.com'
								.cNYSendTo = 'mark.bennett@tollgroup.com'
								.cMLSendTo = 'mark.bennett@tollgroup.com'
								.cTUGSendTo = 'mark.bennett@tollgroup.com'
							ENDIF
						CASE .cCompanyCode = PECACODE
							.cInPath = 'F:\FTPUSERS\PerryEllis-CA\754IN\'
							.cArchivePath = 'F:\FTPUSERS\PerryEllis-CA\754 Archive\'
							.cOriginalsPath = 'F:\FTPUSERS\PerryEllis-CA\754 Original\'
							IF .lTestMode THEN
								.cInPath = 'F:\FTPUSERS\PerryEllis-CA\754 Test\'
							ELSE
								.cFLSendTo = 'Joe.Abbate@Tollgroup.com, karen.zelaya@Tollgroup.com, patricia.zelaya@tollgroup.com, pgaidis@fmiint.com, MICHAEL.FERNANDEZ@pery.com'
								.cCASendTo = 'pgaidis@fmiint.com,MICHAEL.FERNANDEZ@pery.com, fdonato@fmiint.com'
								.cNYSendTo = 'pgaidis@fmiint.com,MICHAEL.FERNANDEZ@pery.com'
								.cMLSendTo = 'pgaidis@fmiint.com,MICHAEL.FERNANDEZ@pery.com'
								.cTUGSendTo = 'pgaidis@fmiint.com,MICHAEL.FERNANDEZ@pery.com'
							ENDIF
						CASE .cCompanyCode = PEFLCODE
							.cInPath = 'F:\FTPUSERS\PerryEllis-FL\754IN\'
							.cArchivePath = 'F:\FTPUSERS\PerryEllis-FL\754 Archive\'
							.cOriginalsPath = 'F:\FTPUSERS\PerryEllis-FL\754 Original\'
							IF .lTestMode THEN
								.cInPath = 'F:\FTPUSERS\PerryEllis-FL\754 Test\'
							ELSE
								.cFLSendTo = 'Joe.Abbate@Tollgroup.com, karen.zelaya@Tollgroup.com, patricia.zelaya@tollgroup.com, pgaidis@fmiint.com,MICHAEL.FERNANDEZ@pery.com'
								.cCASendTo = 'pgaidis@fmiint.com,MICHAEL.FERNANDEZ@pery.com, fdonato@fmiint.com'
								.cNYSendTo = 'pgaidis@fmiint.com,MICHAEL.FERNANDEZ@pery.com'
								.cMLSendTo = 'pgaidis@fmiint.com,MICHAEL.FERNANDEZ@pery.com'
								.cTUGSendTo = 'pgaidis@fmiint.com,MICHAEL.FERNANDEZ@pery.com'
							ENDIF
						CASE .cCompanyCode = PENJCODE
							.cInPath = 'F:\FTPUSERS\PerryEllis-NJ\754IN\'
							.cArchivePath = 'F:\FTPUSERS\PerryEllis-NJ\754 Archive\'
							.cOriginalsPath = 'F:\FTPUSERS\PerryEllis-NJ\754 Original\'
							IF .lTestMode THEN
								.cInPath = 'F:\FTPUSERS\PerryEllis-NJ\754 Test\'
							ELSE
								.cFLSendTo = 'Joe.Abbate@Tollgroup.com, karen.zelaya@Tollgroup.com, patricia.zelaya@tollgroup.com, pgaidis@fmiint.com,MICHAEL.FERNANDEZ@pery.com'
								.cCASendTo = 'pgaidis@fmiint.com,MICHAEL.FERNANDEZ@pery.com, fdonato@fmiint.com'
								.cNYSendTo = 'pgaidis@fmiint.com,MICHAEL.FERNANDEZ@pery.com'
								.cMLSendTo = 'pgaidis@fmiint.com,MICHAEL.FERNANDEZ@pery.com'
								.cTUGSendTo = 'pgaidis@fmiint.com,MICHAEL.FERNANDEZ@pery.com'
							ENDIF
						CASE .cCompanyCode = SAMSUNGCODE
							.cInPath = 'F:\FTPUSERS\SAMSUNG\754IN\'
							.cArchivePath = 'F:\FTPUSERS\SAMSUNG\754IN\ARCHIVED\'
							.cOriginalsPath = 'F:\FTPUSERS\SAMSUNG\754IN\ORIGINAL\'
							IF .lTestMode THEN
								.cInPath = 'F:\FTPUSERS\SAMSUNG\754IN\754TEST\'
							ELSE
								*****  NEED CLIENT CONTACT BELOW?
								.cFLSendTo = 'pgaidis@fmiint.com, Maria.Estrella@Tollgroup.com, Flerida.donado@tollgroup.com'
								.cCASendTo = 'pgaidis@fmiint.com, Maria.Estrella@Tollgroup.com, Flerida.donado@tollgroup.com'
								.cNYSendTo = 'pgaidis@fmiint.com, Maria.Estrella@Tollgroup.com, Flerida.donado@tollgroup.com'
								.cMLSendTo = 'pgaidis@fmiint.com, Maria.Estrella@Tollgroup.com, Flerida.donado@tollgroup.com'
								.cTUGSendTo = 'pgaidis@fmiint.com, Maria.Estrella@Tollgroup.com, Flerida.donado@tollgroup.com'
								
								* , Hak.Ong@tollgroup.com
							ENDIF
						CASE .cCompanyCode = TKOCACODE
							.cInPath = 'F:\FTPUSERS\TKO-CA\754IN\'
							.cArchivePath = 'F:\FTPUSERS\TKO-CA\754 Archive\'
							.cOriginalsPath = 'F:\FTPUSERS\TKO-CA\754 Original\'
							IF .lTestMode THEN
								.cInPath = 'F:\FTPUSERS\TKO-CA\754 Test\'
							ELSE
								.cFLSendTo = 'Joe.Abbate@Tollgroup.com, karen.zelaya@Tollgroup.com, patricia.zelaya@tollgroup.com, pgaidis@fmiint.com, Lillian@TKO-FL.com, Raiza@TKO-FL.com, sylvia@tko-fl.com, jkillen@fmiint.com'
								.cCASendTo = 'pgaidis@fmiint.com, Lillian@TKO-FL.com, Raiza@TKO-FL.com, sylvia@tko-fl.com, jkillen@fmiint.com, MLRodriguez@fmiint.com'
								.cNYSendTo = 'pgaidis@fmiint.com, Lillian@TKO-FL.com, Raiza@TKO-FL.com, sylvia@tko-fl.com, jkillen@fmiint.com, CMalcolm@fmiint.com, imorello@fmiint.com'
								.cMLSendTo = 'pgaidis@fmiint.com, Lillian@TKO-FL.com, Raiza@TKO-FL.com, sylvia@tko-fl.com, jkillen@fmiint.com'
								.cTUGSendTo = 'pgaidis@fmiint.com, Lillian@TKO-FL.com, Raiza@TKO-FL.com, sylvia@tko-fl.com, jkillen@fmiint.com'
							ENDIF
						CASE .cCompanyCode = TKOFLCODE
							.cInPath = 'F:\FTPUSERS\TKO-FL\754IN\'
							.cArchivePath = 'F:\FTPUSERS\TKO-FL\754 Archive\'
							.cOriginalsPath = 'F:\FTPUSERS\TKO-FL\754 Original\'
							IF .lTestMode THEN
								.cInPath = 'F:\FTPUSERS\TKO-FL\754 Test\'
							ELSE
								.cFLSendTo = 'Joe.Abbate@Tollgroup.com, karen.zelaya@Tollgroup.com, patricia.zelaya@tollgroup.com, pgaidis@fmiint.com, Lillian@TKO-FL.com, Raiza@TKO-FL.com, sylvia@tko-fl.com, jkillen@fmiint.com'
								.cCASendTo = 'pgaidis@fmiint.com, Lillian@TKO-FL.com, Raiza@TKO-FL.com, sylvia@tko-fl.com, jkillen@fmiint.com, MLRodriguez@fmiint.com'
								.cNYSendTo = 'pgaidis@fmiint.com, Lillian@TKO-FL.com, Raiza@TKO-FL.com, sylvia@tko-fl.com, jkillen@fmiint.com, CMalcolm@fmiint.com, imorello@fmiint.com'
								.cMLSendTo = 'pgaidis@fmiint.com, Lillian@TKO-FL.com, Raiza@TKO-FL.com, sylvia@tko-fl.com, jkillen@fmiint.com'
								.cTUGSendTo = 'pgaidis@fmiint.com, Lillian@TKO-FL.com, Raiza@TKO-FL.com, sylvia@tko-fl.com, jkillen@fmiint.com'
							ENDIF
						CASE .cCompanyCode = TKONJCODE
							.cInPath = 'F:\FTPUSERS\TKO-NJ\754IN\'
							.cArchivePath = 'F:\FTPUSERS\TKO-NJ\754 Archive\'
							.cOriginalsPath = 'F:\FTPUSERS\TKO-NJ\754 Original\'
							IF .lTestMode THEN
								.cInPath = 'F:\FTPUSERS\TKO-NJ\754 Test\'
							ELSE
								.cFLSendTo = 'Joe.Abbate@Tollgroup.com, karen.zelaya@Tollgroup.com, patricia.zelaya@tollgroup.com, pgaidis@fmiint.com, Lillian@TKO-FL.com, Raiza@TKO-FL.com, sylvia@tko-fl.com, jkillen@fmiint.com'
								.cCASendTo = 'pgaidis@fmiint.com, Lillian@TKO-FL.com, Raiza@TKO-FL.com, sylvia@tko-fl.com, jkillen@fmiint.com, MLRodriguez@fmiint.com'
								.cNYSendTo = 'pgaidis@fmiint.com, Lillian@TKO-FL.com, Raiza@TKO-FL.com, sylvia@tko-fl.com, jkillen@fmiint.com, CMalcolm@fmiint.com, imorello@fmiint.com'
								.cMLSendTo = 'pgaidis@fmiint.com, Lillian@TKO-FL.com, Raiza@TKO-FL.com, sylvia@tko-fl.com, jkillen@fmiint.com'
								.cTUGSendTo = 'pgaidis@fmiint.com, Lillian@TKO-FL.com, Raiza@TKO-FL.com, sylvia@tko-fl.com, jkillen@fmiint.com'
							ENDIF
						CASE .cCompanyCode = MORETFLCODE
							.cInPath = 'F:\FTPUSERS\MORET-FL\754IN\'
							.cArchivePath = 'F:\FTPUSERS\MORET-FL\754 Archive\'
							.cOriginalsPath = 'F:\FTPUSERS\MORET-FL\754 Original\'
							IF .lTestMode THEN
								.cInPath = 'F:\FTPUSERS\MORET-FL\754 Test\'
							ELSE
								.cFLSendTo = 'Joe.Abbate@Tollgroup.com, karen.zelaya@Tollgroup.com, patricia.zelaya@tollgroup.com, pgaidis@fmiint.com, avner@moret.com, lupe@moret.com'
								.cCASendTo = 'pgaidis@fmiint.com, avner@moret.com, lupe@moret.com, SPRouting'
								.cNYSendTo = 'pgaidis@fmiint.com, CMalcolm@fmiint.com, avner@moret.com, lupe@moret.com'
								.cMLSendTo = 'pgaidis@fmiint.com, Joni.Golding@Tollgroup.com, avner@moret.com, lupe@moret.com'
								.cTUGSendTo = 'pgaidis@fmiint.com, avner@moret.com, lupe@moret.com'
							ENDIF
						CASE .cCompanyCode = MORETMLCODE
							.cInPath = 'F:\FTPUSERS\MORET-ML\754IN\'
							.cArchivePath = 'F:\FTPUSERS\MORET-ML\754 Archive\'
							.cOriginalsPath = 'F:\FTPUSERS\MORET-ML\754 Original\'
							IF .lTestMode THEN
								.cInPath = 'F:\FTPUSERS\MORET-ML\754 Test\'
							ELSE
								.cFLSendTo = 'Joe.Abbate@Tollgroup.com, karen.zelaya@Tollgroup.com, patricia.zelaya@tollgroup.com, pgaidis@fmiint.com, avner@moret.com, lupe@moret.com'
								.cCASendTo = 'pgaidis@fmiint.com, avner@moret.com, lupe@moret.com, SPRouting'
								.cNYSendTo = 'pgaidis@fmiint.com, CMalcolm@fmiint.com, avner@moret.com, lupe@moret.com'
								.cMLSendTo = 'pgaidis@fmiint.com, Joni.Golding@Tollgroup.com, avner@moret.com, lupe@moret.com'
								.cTUGSendTo = 'pgaidis@fmiint.com, avner@moret.com, lupe@moret.com'
							ENDIF
						CASE .cCompanyCode = MORETSPCODE
							.cInPath = 'F:\FTPUSERS\MORET-SP\754IN\'
							.cArchivePath = 'F:\FTPUSERS\MORET-SP\754 Archive\'
							.cOriginalsPath = 'F:\FTPUSERS\MORET-SP\754 Original\'
							IF .lTestMode THEN
								.cInPath = 'F:\FTPUSERS\MORET-SP\754 Test\'
							ELSE
								.cFLSendTo = 'Joe.Abbate@Tollgroup.com, karen.zelaya@Tollgroup.com, patricia.zelaya@tollgroup.com, pgaidis@fmiint.com, avner@moret.com, lupe@moret.com'
								.cCASendTo = 'pgaidis@fmiint.com, avner@moret.com, lupe@moret.com, SPRouting'
								.cNYSendTo = 'pgaidis@fmiint.com, CMalcolm@fmiint.com, avner@moret.com, lupe@moret.com'
								.cMLSendTo = 'pgaidis@fmiint.com, Joni.Golding@Tollgroup.com, avner@moret.com, lupe@moret.com'
								.cTUGSendTo = 'pgaidis@fmiint.com, avner@moret.com, lupe@moret.com'
								
*!*									.cFLSendTo = ''
*!*									.cCASendTo = ''
*!*									.cNYSendTo = ''
*!*									.cMLSendTo = ''
*!*									.cTUGSendTo = ''
								
							ENDIF
						CASE .cCompanyCode = SARALEECODE
							.cInPath = 'F:\FTPUSERS\Courtaulds\754IN\'
							.cArchivePath = 'F:\FTPUSERS\Courtaulds\754IN\754 Archive\'
							.cOriginalsPath = 'F:\FTPUSERS\Courtaulds\754IN\754 Originals\'
							IF .lTestMode THEN
								.cInPath = 'F:\FTPUSERS\Courtaulds\754IN\marktest\'
							ELSE
								.cFLSendTo = 'Joe.Abbate@Tollgroup.com, karen.zelaya@Tollgroup.com, patricia.zelaya@tollgroup.com, pgaidis@fmiint.com'
								.cCASendTo = 'Tom.anderson@saralee.com, jkillen@fmiint.com, pgaidis@fmiint.com, ccasillas@fmiint.com, fdonato@fmiint.com'
								.cNYSendTo = 'pgaidis@fmiint.com'
								.cMLSendTo = 'pgaidis@fmiint.com, Joni.Golding@Tollgroup.com'
								.cTUGSendTo = 'pgaidis@fmiint.com'
							ENDIF
						CASE .cCompanyCode = SUCCESSCODE
							.cInPath = 'F:\FTPUSERS\SUCCESS\754IN\'
							.cArchivePath = 'F:\FTPUSERS\SUCCESS\ARCHIVE\'
							.cOriginalsPath = 'F:\FTPUSERS\SUCCESS\ORIGINALS\'
							IF .lTestMode THEN
								.cInPath = 'F:\FTPUSERS\SUCCESS\754IN\marktest\'
							ELSE
								.cFLSendTo = 'Joe.Abbate@Tollgroup.com, karen.zelaya@Tollgroup.com, patricia.zelaya@tollgroup.com, jkillen@fmiint.com, pgaidis@fmiint.com, Steve.Golub@successappl.com, veronica.zamora@successappl.com'
								.cCASendTo = 'jarana@fmiint.com, mlrodriguez@fmiint.com, jkillen@fmiint.com, pgaidis@fmiint.com, Steve.Golub@successappl.com, veronica.zamora@successappl.com'
								.cNYSendTo = 'CMalcolm@fmiint.com, CBlenke@fmiint.com, jkillen@fmiint.com, pgaidis@fmiint.com, Steve.Golub@successappl.com, lschwartz@fmiint.com, veronica.zamora@successappl.com'
								.cMLSendTo = 'pgaidis@fmiint.com, Joni.Golding@Tollgroup.com'
								.cTUGSendTo = 'pgaidis@fmiint.com'
							ENDIF
						CASE .cCompanyCode = NANJINGCODE
							.cInPath = 'F:\FTPUSERS\NANJING\754IN\'
							.cArchivePath = 'F:\FTPUSERS\NANJING\754IN\ARCHIVE\'
							.cOriginalsPath = 'F:\FTPUSERS\NANJING\754IN\ORIGINALS\'
							IF .lTestMode THEN
								.cInPath = 'F:\FTPUSERS\NANJING\754IN\marktest\'
							ELSE
								.cFLSendTo = 'Joe.Abbate@Tollgroup.com, karen.zelaya@Tollgroup.com, patricia.zelaya@tollgroup.com, jkillen@fmiint.com, pgaidis@fmiint.com'
								.cCASendTo = 'Laura.Romo@tollgroup.com, pgaidis@fmiint.com, Flerida.Donado@tollgroup.com, Steven.BALBAS@tollgroup.com'
								.cNYSendTo = 'CMalcolm@fmiint.com, CBlenke@fmiint.com, jkillen@fmiint.com, pgaidis@fmiint.com'
								.cMLSendTo = 'pgaidis@fmiint.com, Joni.Golding@Tollgroup.com'
								.cTUGSendTo = 'pgaidis@fmiint.com'
							ENDIF
						CASE .cCompanyCode = DODENIMCACODE
							.cInPath = 'F:\FTPUSERS\DODENIM-CA\754IN\'
							.cArchivePath = 'F:\FTPUSERS\DODENIM-CA\754IN\ARCHIVE\'
							.cOriginalsPath = 'F:\FTPUSERS\DODENIM-CA\754IN\ORIGINALS\'
							IF .lTestMode THEN
								.cInPath = 'F:\FTPUSERS\DODENIM-CA\754IN\MARKTEST\'
							ELSE
								.cFLSendTo = 'Joe.Abbate@Tollgroup.com, karen.zelaya@Tollgroup.com, patricia.zelaya@tollgroup.com, jkillen@fmiint.com, pgaidis@fmiint.com, angela@dodenim.com'
								.cCASendTo = 'jkillen@fmiint.com, pgaidis@fmiint.com, jarana@fmiint.com, mlrodriguez@fmiint.com, angela@dodenim.com'
								.cNYSendTo = 'CMalcolm@fmiint.com, CBlenke@fmiint.com, jkillen@fmiint.com, pgaidis@fmiint.com, angela@dodenim.com'
								.cMLSendTo = 'pgaidis@fmiint.com, Joni.Golding@Tollgroup.com, angela@dodenim.com'
								.cTUGSendTo = 'pgaidis@fmiint.com'
							ENDIF
						CASE .cCompanyCode = DODENIMNJCODE
							.cInPath = 'F:\FTPUSERS\DODENIM-NJ\754IN\'
							.cArchivePath = 'F:\FTPUSERS\DODENIM-NJ\754IN\ARCHIVE\'
							.cOriginalsPath = 'F:\FTPUSERS\DODENIM-NJ\754IN\ORIGINALS\'
							IF .lTestMode THEN
								.cInPath = 'F:\FTPUSERS\DODENIM-NJ\754IN\MARKTEST\'
							ELSE
								.cFLSendTo = 'Joe.Abbate@Tollgroup.com, karen.zelaya@Tollgroup.com, patricia.zelaya@tollgroup.com, jkillen@fmiint.com, pgaidis@fmiint.com, angela@dodenim.com'
								.cCASendTo = 'jkillen@fmiint.com, pgaidis@fmiint.com, jarana@fmiint.com, mlrodriguez@fmiint.com, angela@dodenim.com'
								.cNYSendTo = 'CMalcolm@fmiint.com, CBlenke@fmiint.com, jkillen@fmiint.com, pgaidis@fmiint.com, angela@dodenim.com'
								.cMLSendTo = 'pgaidis@fmiint.com, Joni.Golding@Tollgroup.com, angela@dodenim.com'
								.cTUGSendTo = 'pgaidis@fmiint.com'
							ENDIF
						CASE .cCompanyCode = JQUEENCACODE
							.cInPath = 'F:\FTPUSERS\JQUEEN-CA\754IN\'
							.cArchivePath = 'F:\FTPUSERS\JQUEEN-CA\754IN\754 ARCHIVE\'
							.cOriginalsPath = 'F:\FTPUSERS\JQUEEN-CA\754IN\754 ORIGINALS\'
							IF .lTestMode THEN
								.cInPath = 'F:\FTPUSERS\JQUEEN-CA\754IN\MARKTEST\'
							ELSE
								.cFLSendTo = 'Joe.Abbate@Tollgroup.com, karen.zelaya@Tollgroup.com, patricia.zelaya@tollgroup.com, jkillen@fmiint.com, pgaidis@fmiint.com, tony@jqueen-ny.com, john@jqueen-ny.com'
								.cCASendTo = 'jkillen@fmiint.com, pgaidis@fmiint.com, jarana@fmiint.com, mlrodriguez@fmiint.com, fdonato@fmiint.com, tflores@fmiint.com, tony@jqueen-ny.com, john@jqueen-ny.com'
								.cNYSendTo = 'CMalcolm@fmiint.com, CBlenke@fmiint.com, jkillen@fmiint.com, pgaidis@fmiint.com, tony@jqueen-ny.com, john@jqueen-ny.com'
								.cMLSendTo = 'pgaidis@fmiint.com, Joni.Golding@Tollgroup.com, tony@jqueen-ny.com, john@jqueen-ny.com'
								.cTUGSendTo = 'pgaidis@fmiint.com, tony@jqueen-ny.com, john@jqueen-ny.com'
							ENDIF
						OTHERWISE
							.TrackProgress("In SetCompanyCode, unexpected company code: " + .cCompanyCode, LOGIT+SENDIT)
					ENDCASE
				OTHERWISE
					.TrackProgress("INVALID COMPANY CODE PARAMETER: type = " + lcType, LOGIT+SENDIT)
			ENDCASE
		ENDWITH
	ENDPROC


ENDDEFINE
