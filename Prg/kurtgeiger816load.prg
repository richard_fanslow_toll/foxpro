set step on

public xfile

lcPath ="f:\ftpusers\kg-ca\816\"

lnNum = ADIR(tarray,lcPath+"*.*")
IF lnNum = 0
  WAIT WINDOW "No files found...exiting" TIMEOUT 2
  CLOSE DATA ALL
  schedupdate()
  _SCREEN.CAPTION="Kurt Geiger Packing List Upload.............."
  ON ERROR
  RETURN
Else
 For thisfile = 1  To lnNum
  cfilename = ALLTRIM(tarray[thisfile,1])
  xfile = lcpath+cfilename
  loadfile()
  next 
Endif


******************************************************************
Procedure loadfile 
Create Cursor x816 (;
status char(1),;
custid char(10),;
custname char(40),;
storeid  char(10),;
storename char(40),;
staddr1 char(40),;
staddr2 char(40),;
staddr3 char(40),;
staddr4 char(40),;
stcntry char(40),;
stzip char(10))

Select x816
Set Step On 

append from &xfile Type csv
endproc