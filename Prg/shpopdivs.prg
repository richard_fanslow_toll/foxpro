* populate new 2-digit Division field based on Company code
*RETURN

SET TALK ON

* LIVE
USE F:\shop\shdata\trucks
REPLACE ALL DIVISION WITH ShGetDiv(company) FOR EMPTY(DIVISION)

USE F:\shop\shdata\trailer
REPLACE ALL DIVISION WITH ShGetDiv(company) FOR EMPTY(DIVISION)

USE F:\shop\shdata\VEHMAST
REPLACE ALL DIVISION WITH ShGetDiv(company) FOR EMPTY(DIVISION)

*!*	* DEV
*!*	USE M:\DEV\shdata\trucks
*!*	REPLACE ALL DIVISION WITH GetDiv(company) FOR EMPTY(DIVISION)


*!*	USE M:\DEV\shdata\trailer
*!*	REPLACE ALL DIVISION WITH GetDiv(company) FOR EMPTY(DIVISION)


*!*	USE M:\DEV\shdata\VEHMAST
*!*	REPLACE ALL DIVISION WITH GetDiv(company) FOR EMPTY(DIVISION)


*CLOSE DATABASES

RETURN

FUNCTION ShGetDiv
	LPARAMETERS tcCompanyCode
	LOCAL lcCompanyCode, lcDivCode
	lcCompanyCode = UPPER(ALLTRIM(tcCompanyCode))
	DO CASE
		CASE lcCompanyCode = "I"
			lcDivCode = "04"
		CASE lcCompanyCode = "J"
			lcDivCode = "06"
		CASE lcCompanyCode = "L"
			lcDivCode = "50"
		CASE lcCompanyCode = "M"
			lcDivCode = "60"
		CASE lcCompanyCode = "S"
			lcDivCode = "60"
		CASE lcCompanyCode = "T"
			lcDivCode = "03"
		CASE lcCompanyCode = "W"
			lcDivCode = "05"
		CASE lcCompanyCode = "X"
			lcDivCode = "02"
		OTHERWISE
			lcDivCode = "  "
	ENDCASE
	RETURN lcDivCode
ENDFUNC
