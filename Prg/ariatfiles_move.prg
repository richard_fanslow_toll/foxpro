PUBLIC cfilename,cfileoutname,cAriatTransfer,normalexit
CLOSE DATA ALL
DO M:\DEV\PRG\_SETVARS WITH .T.
normalexit = .F.
cCustName = "ARIAT"


TRY
	lTesting = .F.
	lOverrideBusy = lTesting
	lOverrideBusy = .T.
	cAriatTransfer = "ARIAT-MOVE"
*	SET STEP ON
	IF !lTesting
		SELECT 0
		USE F:\edirouting\ftpsetup SHARED
		LOCATE FOR ftpsetup.transfer = cAriatTransfer
    If !Found()
      WAIT WINDOW "Process not found in FTPSetup.........exiting" TIMEOUT 2
      normalexit = .T.
      THROW
    Endif 
		IF ftpsetup.chkbusy = .T. AND !lOverrideBusy
			WAIT WINDOW "File is processing...exiting" TIMEOUT 2
			normalexit = .T.
			THROW
		ENDIF
		REPLACE chkbusy WITH .T. FOR ftpsetup.transfer = cAriatTransfer
		USE IN ftpsetup
	ENDIF

	ASSERT .F. MESSAGE "In  mail data subroutine"
	SELECT 0
	USE F:\3pl\DATA\mailmaster ALIAS mm SHARED
	LOCATE FOR mm.edi_type = "MISC" AND mm.taskname = "GENERAL"
	STORE TRIM(IIF(mm.use_alt,mm.sendtoalt,mm.sendto)) TO tsendtoerr
	STORE TRIM(IIF(mm.use_alt,mm.ccalt,mm.cc)) TO tccerr
	USE IN mm

	FOR xyz = 0 TO 1  && dont do the test channel anymore  PG 03/29/2017
		IF xyz = 1
			cDirIn = "f:\ftpusers\ariat\in\"
		ELSE
			cDirIn = "f:\ftpusers\ariat-test\in\"
		ENDIF

		CD &cDirIn

		_SCREEN.CAPTION="Ariat File Move Process"
		_SCREEN.WINDOWSTATE=IIF(lTesting,2,1)

		nFound = ADIR(ary1,cDirIn+"*")
		IF nFound = 0
			CLOSE DATABASES ALL
			WAIT WINDOW "No EDI files found to process...exiting" TIMEOUT 2
			loop
		ENDIF

		len1 = ALEN(ary1,1)

		FOR xxi = 1 TO len1
			ASSERT .F. MESSAGE "In move loop"
			cARfilename = ALLT(ary1[xxi,1])
			xfile = (cDirIn+cARfilename)

			WAIT WINDOW "Processing file# "+ALLTRIM(TRANSFORM(xxi))+ "of "+ALLTRIM(TRANSFORM(xxi))+"  File(s)  "+ cARfilename NOWAIT
			DO M:\DEV\PRG\createx856a
			DO M:\DEV\PRG\loadedifile WITH xfile,"*","TILDE","ARIAT"
			SELECT x856
			LOCATE FOR x856.segment = "GS"
			cFiletype = ICASE(x856.f1 = 'SC','832',x856.f1 = 'SH','856',x856.f1 = 'SW','940',x856.f1 = 'FA','997','940')  && Types 832/997/940/856  PG added 856 1/15/2017

			cDirXfer = ("F:\ftpusers\ariat\940xfer\")
			cFileXferName = (cDirXfer+cARfilename)
			DO CASE
			CASE cFiletype = '997'
				cDirOut = ("F:\ftpusers\ariat\997IN\")
				cfileoutname = (cDirOut+cARfilename)
				cfileoutname = cfileoutname+TTOC(DATETIME(),1)
				COPY FILE [&xfile] TO [&cFileOutName]
				IF FILE(cfileoutname)
					IF FILE(xfile)
						DELETE FILE [&xfile]
					ENDIF
				ENDIF
				LOOP
			CASE cFiletype = '832'
				cDirOut = ("F:\ftpusers\ariat\stylemaster\")
			CASE cFiletype = '940'
				cDirOut = ("F:\ftpusers\ariat\940IN\")
			CASE cFiletype = '856'
				cDirOut = ("F:\ftpusers\ariat\856IN\")
			ENDCASE

			cfileoutname = (cDirOut+cARfilename)
			COPY FILE [&xfile] TO [&cFileOutName]
			COPY FILE [&xfile] TO [&cFileXferName]  && Added 08.08.2011 to send 997s for these
			IF FILE(cfileoutname)
				DELETE FILE [&xfile]
			ENDIF
*  Loop  && removed as it seems like it moves one file at a time

		ENDFOR
	ENDFOR

	WAIT WINDOW "Ariat file move process complete" TIMEOUT 1
	SELECT 0
	USE F:\edirouting\ftpsetup SHARED
	REPLACE chkbusy WITH .F. FOR ftpsetup.transfer = cAriatTransfer

	WAIT CLEAR
	normalexit = .T.

CATCH TO oErr
	IF !normalexit
		ASSERT .F. MESSAGE "At CATCH..."
		SET STEP ON

		tsubject = cCustName+" File Move (KY) Upload Error ("+TRANSFORM(oErr.ERRORNO)+") at "+TTOC(DATETIME())
		tattach  = ""
		tsendto  = tsendtoerr
		tcc = tccerr
		tmessage = cCustName+" File Move (KY) Upload Error..... Please fix me........!"+CHR(13)
		lcSourceMachine = SYS(0)
		lcSourceProgram = SYS(16)

		tmessage =tmessage+ "Try/CATCH Exeception Message:"+CHR(13)+;
			[  Message: ] + oErr.MESSAGE +CHR(13)+;
			[  Procedure: ] + oErr.PROCEDURE +CHR(13)+;
			[  LineNo: ] + STR(oErr.LINENO) +CHR(13)+;
			[  Error: ] + STR(oErr.ERRORNO) +CHR(13)+;
			[  Details: ] + oErr.DETAILS +CHR(13)+;
			[  StackLevel: ] + STR(oErr.STACKLEVEL) +CHR(13)+;
			[  LineContents: ] + oErr.LINECONTENTS+CHR(13)+;
			[  UserValue: ] + oErr.USERVALUE+CHR(13)+;
			[  Computer:  ] +lcSourceMachine+CHR(13)+;
			[  940 file:  ] +xfile+CHR(13)+;
			[  Program:   ] +lcSourceProgram

		tattach  = ""
		tFrom ="TGF EDI Operations <toll-edi-ops@tollgroup.com>"
		DO FORM M:\DEV\frm\dartmail2 WITH tsendto,tFrom,tsubject,tcc,tattach,tmessage,"A"
	ELSE
		WAIT WINDOW AT 10,10 "Normal Exit " TIMEOUT 2
	ENDIF
FINALLY
	CLOSE DATABASES ALL
ENDTRY
