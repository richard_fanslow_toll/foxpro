*!* Complete revision of Parcel trigger program to work with unified SQL database.
*!* Completed 12.22.2017, Joe

TRY
	CLEAR
	PUBLIC nAccountid,nAcctNum,cBOL,nAccountName,cUseFolder,cLocName,cOffice,nDaysback,cUserMessage,cShip_ref,lMoret,cAcctGrp,lAriat
	PUBLIC cPTString,cPTMail,cMissSCACs,cSCACString,lSendmail,tFrom,tCC,cGroup,cPTSWC,dDate,tSendto,lAmazon,guserid,cMod

	lTesting = .T.
	_setvars(lTesting)
	IF !lTesting
		utilsetup("PARCEL945TRIGGER")
	ENDIF
	_setvars(lTesting)
	lTestInput = .F.
	lTestmail = lTesting
	lTestLoad = lTesting
	lSendmail = !lTesting
	lOverridebusy = lTesting
	lKeepScreen = lTesting
	guserid = "JOEB"
	lDoFinish = .T.  && Default = .t., if .f., will not allow the post-process reports and early trigger process to run
	NormalExit = .F.
	lAmazon = .F.
	tFrom ="Toll EDI  Operations <toll-edi-ops@tollgroup.com>"

*!*  lKeepScreen = .T.
*!*  lDoFinish = .f.
*!*	 lOverridebusy = .T.

	_SCREEN.WINDOWSTATE=IIF(lTesting OR lOverridebusy OR lKeepScreen,2,1)
	_SCREEN.CAPTION = "EDI 945 PARCEL Trigger-All Accounts"

	IF !lTesting
		SELECT 0
		USE F:\edirouting\FTPSETUP SHARED
		LOCATE FOR FTPSETUP.TRANSFER = "TRIG-PARCEL945"
		IF FTPSETUP.chkbusy = .T. AND !lOverridebusy
			WAIT WINDOW 'TRIG Busy...try again later' TIMEOUT 1
			NormalExit = .T.
			CLOSE DATABASES ALL
			THROW
		ENDIF
		REPLACE chkbusy WITH .T.,trig_time WITH DATETIME() FOR FTPSETUP.TRANSFER = "TRIG-PARCEL945"

		IF !lOverridebusy
			LOCATE FOR FTPSETUP.TRANSFER = "TRIG-PARCEL945"
			IF FTPSETUP.folderchk = .F. AND !lOverridebusy
				WAIT WINDOW 'TRIG Stopped-No Folderchk flag' TIMEOUT 1
				CLOSE DATABASES ALL
				THROW
			ENDIF
		ENDIF
		USE IN FTPSETUP
	ENDIF

	DO m:\dev\prg\_setvars WITH .T.
	lookups()
*	replaceltn()  && Added this to ensure that current Synclaire long trk numbers are populated

	CLOSE DATABASES ALL

	SELECT 0
	USE F:\3pl\DATA\mailmaster ALIAS mm
	LOCATE
	LOCATE FOR mm.edi_type = 'MISC' AND mm.taskname = "GENERAL"
	tSendto = IIF(mm.use_alt,mm.sendtoalt,mm.sendto)
	tCC = IIF(mm.use_alt,mm.ccalt,mm.cc)
	USE IN mm

	NormalExit = .F.

	cUserMessage = ""
	IF USED('edi_trigger')
		USE IN edi_trigger
	ENDIF

	cEDIFolder = IIF(lTesting,"C:\TEMPFOX\","F:\3PL\DATA\")
	IF lTesting
		WAIT WINDOW "This will be run into the TEST edi_trigger table" TIMEOUT 1
	ENDIF
	USE (cEDIFolder+"edi_trigger") IN 0 ALIAS edi_trigger SHARED

	USE F:\3pl\DATA\parcel_carriers IN 0 ALIAS parcels ORDER TAG scac NOUPDATE
	USE F:\3pl\DATA\trigger945groups IN 0 ALIAS triggers NOUPDATE
	xsqlexec("select * from account",,,"qq")

	nDaysback = IIF(INLIST(DOW(DATE()),1,7),10,7)
	nDaysback = 20
	dDate = DATE()-nDaysback

	csq1 = [select * from shipment where 1=0]
	xsqlexec(csq1,,,"wh")

	csq1 = [select * from swcdet where adddt  >= {]+DTOC(dDate)+[}]
	xsqlexec(csq1,,,"wh")

	WAIT WINDOW "Selecting OUTSHIP records" NOWAIT NOCLEAR
	csq1 = [select accountid,mod,consignee,del_date,ship_ref,cnee_ref,scac from outship where 1=0]
	xsqlexec(csq1,"osfreightnobol",,"wh")

*!* here we gather all outship records with an empty BOL but a del_date is there and within the applicable window.
*!* can we just select the few fields we actually use rather than "*"
** some one else puts in the del_date

	csq1 = [select consignee,mod,office,accountid,bol_no,swcnum,del_date,ship_ref,cnee_ref,wo_num,keyrec,ship_via,scac,salesrep,0 as matched from outship where bol_no = '  ' and scac <> '  ' and del_date >= {]+DTOC(dDate)+[} order by accountid,mod]
	xsqlexec(csq1,"outshipmast",,"wh")

	SELECT outshipmast
	DELETE FOR accountid = 6751 && TJM...may be removed in future.
	LOCATE
	IF lTesting
		BROWSE
	ENDIF

*!* the outshipmast cursor will have all the target pick tickets that will be part of the processing
*!* little by little we need to filter out those pick tickets that are not candidates for triggering
*!* then we remove any trucking SCACs from the outshipmast table
	nct = 0
	SCAN
		IF !SEEK(outshipmast.scac,'parcels','scac')  && Trucking SCAC shipments with no BOL#
			SCATTER FIELDS accountid,MOD,consignee,del_date,ship_ref,cnee_ref,scac MEMVAR
			INSERT INTO osfreightnobol FROM MEMVAR
			nct =	nct +1
			DELETE NEXT 1 IN outshipmast
		ENDIF
	ENDSCAN
	WAIT WINDOW "There were "+TRANSFORM(nct)+" non-UPS SCAC records removed" TIMEOUT 2
	WAIT CLEAR
	RELEASE nct
	IF lTesting
		SELECT osfreightnobol
*!*			BROWSE
	ENDIF

*!* what does this next loop accomoplish ---
*!* It selects Shipment records only matching the outship extract PTs.
*!* This has to happen here, because another section uses this data later on
*!* shipment has accountids that are special, the right 4 digits are like a base account and the left digits are derivatives of the base.

	SELECT outshipmast
	LOCATE
	WAIT WINDOW "Selecting SHIPMENT records: number of records = "+TRANSFORM(RECCOUNT("outshipmast")) NOWAIT NOCLEAR

	SCAN
		nAcctMast = outshipmast.accountid
		cSRMast = PADR(ALLTRIM(outshipmast.ship_ref),20)
		cWO_Num=outshipmast.wo_num

		csq1 = [select * from shipment where (right(rtrim(str(accountid)),4) = ]+TRANSFORM(nAcctMast)+[ or accountid = 9999) and wo_num = "+TRANSFORM(nWO_Num)+" and pickticket = ']+cSRMast+[']
		xsqlexec(csq1,"shipment2",,"wh")
		IF RECCOUNT('shipment2')>0
			SELECT outshipmast
			REPLACE outshipmast.matched WITH 1 NEXT 1 IN outshipmast
		ENDIF
		SELECT shipment
		APPEND FROM DBF('shipment2')
		USE IN shipment2
		SELECT outshipmast
	ENDSCAN


	RELEASE cSRMast
	IF lTesting
		SELECT shipment
*		BROW
	ENDIF


*!* a little more explain
*!* There are two types of numbers stored in the that we always know are parcel tracking numbers, the 1Z UPS and the D for EMSY shipments.
*!*	This populates the BOL# field for those and removes them from further processing
*!* EMSY is Ontrac new small PKG carrier

	WAIT WINDOW "Filling BOL# field for UPS/EMSY numbers in Keyrec" NOWAIT NOCLEAR
	SELECT outshipmast
	LOCATE

	nct = 0
	SCAN
		IF ((LEFT(outshipmast.keyrec,2) = '1Z') OR (LEFT(outshipmast.keyrec,1) = 'D' AND LEN(ALLTRIM(outshipmast.keyrec))>10 AND outshipmast.scac = "EMSY"))
			nct = nct+1
			cBN6 = RIGHT(ALLTRIM(outshipmast.keyrec),6)
			cSR = outshipmast.ship_ref
			nAcctNum = outshipmast.accountid
			cMod = outshipmast.MOD
			xsqlexec("update outship set bol_no = keyrec, blnum6 = '"+cBN6+"' where accountid = "+TRANSFORM(nAcctNum)+" and mod = '"+cMod+"' and ship_ref = '"+cSR+"'",,,"wh")
			DELETE NEXT 1 IN outshipmast
		ENDIF
	ENDSCAN
	WAIT WINDOW "There were "+TRANSFORM(nct)+" UPS records filled in" TIMEOUT 2
	RELEASE nct,cSR,nAcctNum,cMod


*!* a little more explain ---
*!* This section performs a SCAC conversion for certain account/scac combinations per the customers,
*!* but it also preloads Moret AmazonFBA shipments with a "Pseudo-BOL", per Lupe and Liana.

	SELECT outshipmast
	LOCATE
	WAIT WINDOW "Converting/replacing select SCAC codes in OUTSHIP"+CHR(13)+"and deleting certain non-945 PTs" NOWAIT NOCLEAR
	SCAN  && Looks for SCACs to convert
		cSR = outshipmast.ship_ref
		nAcctNum = outshipmast.accountid
		cMod = outshipmast.MOD
		cSCAC = outshipmast.scac
		STORE .F. TO lAmazon,lAmazonFBA

		lLoop = .F.
*!* what are we checking --- Checks if an AmazonFBA shipment for Moret, also for Consignee = "Sample", which are removed from processing
		consigneecheck()
		IF lLoop = .T.
			LOOP
		ENDIF

		lLoop = .F.
		scaccheck()
		IF lLoop = .T.
			LOOP
		ENDIF

		DO CASE

*!* need verbage on the special conditions for Moret ---
&&  AmazonFBA can't be processed by Moret with a standard tracking number, per Lupe, so this creates one

			CASE  moretaccount("outshipmast.accountid")
				IF lAmazonFBA AND !lAmazon  && Pseudo-BOL exception used for the rare Moret Amazon FBA shipments
					cBOLRepl = "PSP"+PADL(ALLTRIM(cSR),14,"0")
					cBN6 = RIGHT(ALLTRIM(cBOLRepl),6)
					xsqlexec("update outship set bol_no = '"+cBOLRepl+"' where accountid = "+cAccountid+" and mod = '"+cMod+"' and ship_ref = '"+cShip_ref+"'",,,"wh")
					xsqlexec("update outship set blnum6 = '"+cBN6+"' where accountid = "+cAccountid+" and mod = '"+cMod+"' and ship_ref = '"+cShip_ref+"'",,,"wh")
					RELEASE cBN6,cBOLRepl
					DELETE NEXT 1 IN outshipmast
					xsqlexec("select * from swcdet where mod='"+cWhse+"' and swcnum='"+cSWCNum+"'","xswcdet",,"wh")
					IF RECCOUNT('xswcdet') > 0
						xsqlexec("update swcdet set bol_no='"+cBOLRepl+"', blnum6= '"+cBN6+"' where mod='"+cMod+"' and swcnum='"+cSWCNum+"'",,,"wh")
					ENDIF
					LOOP
				ENDIF

				cReplSCAC = ''  && this case just for Moret ? why ? --- Moret is the only customer that specified conditional replacement of these SCACs
				DO CASE
					CASE outshipmast.ship_via = "DHL"
						cReplSCAC = "DHLC"
					CASE outshipmast.ship_via = "UPS" OR outshipmast.ship_via = "U.P.S" OR outshipmast.ship_via = "UNITED PARCEL"
						cReplSCAC = "UPSN"
					CASE outshipmast.ship_via = "FEDEX" OR outshipmast.ship_via = "FED EX" OR outshipmast.ship_via = "FEDERAL EXPRESS"
						cReplSCAC = "FDEG"
					CASE outshipmast.scac = 'UPGC'
						cReplSCAC = "UPSL"
					CASE outshipmast.scac = 'FGC'
						cReplSCAC = "FDEG"
				ENDCASE
				IF !EMPTY(cReplSCAC)
					xsqlexec("update outship set scac = '"+cReplSCAC+"' where ship_ref = '"+cSR+"' and mod = '"+cMod+"' and accountid = "+TRANSFORM(nAcctNum),,,"wh")
				ENDIF

*!* need verbage on these special conditions --- Courtaulds specified replacement of this SCAC, if used
			CASE  (INLIST(outshipmast.accountid,&gcourtauldsaccounts) OR INLIST(outshipmast.accountid,&gnauticaaccounts))  && Miami accounts
				IF cSCAC = 'UPGC'
					cReplSCAC = "UPSL"
					xsqlexec("update outship set scac = '"+cReplSCAC+"' where ship_ref = '"+cSR+"' and mod = '"+cMod+"' and accountid = "+TRANSFORM(nAcctNum),,,"wh")
				ENDIF

*!* need verbage on these special conditions --- Highline specified replacement of this SCAC, if used
			CASE  INLIST(outshipmast.accountid,&gtkohighlineaccounts) && Highline accounts
				IF cSCAC = 'FEG'
					cReplSCAC = "FDEG"
					xsqlexec("update outship set scac = '"+cReplSCAC+"' where ship_ref = '"+cSR+"' and mod = '"+cMod+"' and accountid = "+TRANSFORM(nAcctNum),,,"wh")
				ENDIF
		ENDCASE

		RELEASE cReplSCAC,cSR,cAc
	ENDSCAN

*!* what is the purpose of this cursor  --- It consolidates Shipment/Package info to just what's needed to populate tracking number in the bol_no field,
*!* and provides a scannable data set for use in the summary email

	CREATE CURSOR shiptrack (shipmentid i, pickticket c(20),trknumber c(22),uspstrknumber c(22),pkgcharge N(8,2))

	WAIT WINDOW "Checking SHIPMENT records, matching to PACKAGE" NOWAIT NOCLEAR
	SELECT shipment
	LOCATE
	IF RECCOUNT()=0
		parcelmail()  && Added here in case no Shipment matches to outshipmast
		WAIT WINDOW "No shipment records found for any outship PTs...closing" TIMEOUT 2
		CLOSE DATA ALL
		NormalExit = .T.
		THROW
	ELSE
*!* thought we were going to do one shipment/package select at the beginning using shipmentid for a single select of packages and not building a "shipmentid in"
*!* No, that was never my idea. This selects only the package records that match the Shipment records which in turn matched the outstanding parcel PTs, because it's
*!* possible that certain Shipments may go back farther than the 20 day selection limit.

		WAIT WINDOW "Selecting PACKAGE records" NOWAIT NOCLEAR
		SELECT shipment
		LOCATE
		IF lTesting
*			SET STEP ON
		ENDIF
*!* Changed filter accrual to draw from Shipment, removed parcel SQL select until the end
		xjfilter="shipmentid in ("
		SCAN
			nShipmentId = shipment.shipmentid
			xjfilter=xjfilter+TRANSFORM(shipment.shipmentid)+","
		ENDSCAN
		xjfilter=LEFT(xjfilter,LEN(xjfilter)-1)+")"
		xsqlexec("select * from package where "+xjfilter,,,"wh")

		SCAN
			SCATTER FIELDS shipmentid,pickticket MEMVAR
			SELECT package
			LOCATE FOR package.shipmentid = shipment.shipmentid
			IF FOUND()
				SCATTER FIELDS trknumber,uspstrknumber MEMVAR
				trknumber = LEFT(ALLTRIM(trknumber),20)
				SUM(package.pkgcharge) TO m.pkgcharge FOR package.shipmentid = shipment.shipmentid
				IF EMPTY(m.pkgcharge)
					m.pkgcharge = 0
				ENDIF
				INSERT INTO shiptrack FROM MEMVAR
				RELEASE m.pkgcharge
			ENDIF
		ENDSCAN
	ENDIF

&& at this point explain what the cursor has and what this data will be used for  --- Has four elements, pickticket, tracking number, USPS trk number and package charges
*!* Reason for cursor creation is given above

&& need more verbage of this big scan loop
*!* OK, IF there are records in the distilled shiptrack cursor, this scans that  because only the records contained it in are relevant to the outstanding Outship parcel records,
*!* by PT. This is where the BOL_NO field is actually populated with the trknumber, the uspstrknumber (if present) and UPS charges populate the Shipins memo field,
*!* and the blnum6 is populated.

	SELECT shiptrack
	LOCATE
	IF lTesting
		BROWSE
*		SET STEP ON
	ENDIF
	WAIT CLEAR
	SCAN  && Running through shiptrack cursor
		cSR = ALLTRIM(shiptrack.pickticket)
		IF "~"$cSR && Split PT
			STORE ALLTRIM(STRTRAN(cSR,"~","")) TO cSR

			IF EMPTY(shiptrack.trknumber)
				cTrkMessage = cGroup+": No Tracking # for PT "+ALLTRIM(cShip_ref)+", SHP/ID "+ALLTRIM(STR(shipment.shipmentid))+" in Package"
				WAIT WINDOW cTrkMessage TIMEOUT 1
				MissTrkNumMail()
				WAIT WINDOW "No trk.number for PT# "+cSR TIMEOUT 1
				LOOP
			ELSE
				cBN6 = RIGHT(ALLTRIM(shiptrack.trknumber),6)
				SELECT outshipmast
				LOCATE
				LOCATE FOR outshipmast.ship_ref = cSR
				cMod = outshipmast.MOD
				nAcctNum = outshipmast.accountid
				cSWCNum =  outshipmast.swcnum

*!* Gather OUTSHIP info for current PT to check SHIPINS field
				xsqlexec("select * from outship where accountid = "+TRANSFORM(outshipmast.accountid)+" and mod = '"+ALLTRIM(outship.MOD)+"' and ship_ref = '"+PADR(ALLTRIM(outship.ship_ref),20)+"'",,,"wh")
				csh = outship.shipins

*!* Update LTN and package charges as needed
				IF !EMPTY(shiptrack.uspstrknumber) AND LEN(ALLTRIM(shiptrack.uspstrknumber))=22 AND !"LONGTRKNUM*"$csh
					csh = csh+CHR(13)+"LONGTRKNUM*"+ALLTRIM(shiptrack.uspstrknumber)
				ENDIF
				IF !("UPSCHARGE*"$outship.shipins) AND shiptrack.pkgcharge > 0
					csh = csh +CHR(13)+"UPSCHARGE*"+ALLTRIM(STR(nUPSCharge,8,2))
				ENDIF

				IF !lTesting
					xsqlexec("update outship set shipins = '"+csh+"' where ship_ref = '"+cSR+"' and mod = '"+cMod+"' and accountid = "+TRANSFORM(nAcctNum),,,"wh")
				ELSE
					WAIT WINDOW "Outship SHIPINS would be updated with LONGTRKNUM* here" TIMEOUT 2
				ENDIF
			ENDIF

*!* Insert TRKNUMBER into OUTSHIP (if trknumber exists in PACKAGE)
			IF !lTesting
				xsqlexec("update outship set bol_no = '"+LEFT(ALLTRIM(shiptrack.trknumber),20)+"' where ship_ref = '"+cSR+"' and mod = '"+cMod+"' and accountid = "+TRANSFORM(nAcctNum),,,"wh")
				xsqlexec("update outship set blnum6 = '"+cBN6+"' where ship_ref = '"+cSR+"' and mod = '"+cMod+"' and accountid = "+TRANSFORM(nAcctNum),,,"wh")

				xsqlexec("select * from swcdet where mod='"+cMod+"' and swcnum='"+cSWCNum+"'","xswcdet",,"wh")
				IF RECCOUNT()#0
					xsqlexec("update swcdet set bol_no='"+LEFT(ALLTRIM(shiptrack.trknumber),20)+"', blnum6='"+cBN6+"' where mod='"+cMod+"' and swcnum='"+cSWCNum+"'",,,"wh")
				ENDIF
				USE IN xswcdet
			ELSE
				WAIT WINDOW "Outship BOL_NO would be updated with TRKNUMBER here" TIMEOUT 2
			ENDIF
			DELETE NEXT 1 IN outshipmast
		ENDIF
	ENDSCAN

&& need verbage on the summary of what this code has accomplished --- Population of outship bol_no, blnum6 and shipins fields with required info for parcel triggering.


	IF lTesting
*		SET STEP ON
		CLOSE DATA ALL
		CANCEL
	ENDIF

CATCH TO oErr
	IF !NormalExit
		SET STEP ON
		ASSERT .F. MESSAGE "At CATCH..."
		tsubject = "Parcel 945 Trigger "+cOffice+" Upload Error ("+TRANSFORM(oErr.ERRORNO)+") at "+TTOC(DATETIME())
		tattach  = ""
		tmessage = "Parcel 945 Trigger "+cOffice+" Process Error"
		lcSourceMachine = SYS(0)
		lcSourceProgram = SYS(16)

		tmessage = tmessage+ "Try/CATCH Exeception Message:"+CHR(13)+;
			[  Error: ] + STR(oErr.ERRORNO) +CHR(13)+;
			[  LineNo: ] + STR(oErr.LINENO) +CHR(13)+;
			[  Message: ] + oErr.MESSAGE +CHR(13)+;
			[  Procedure: ] + oErr.PROCEDURE +CHR(13)+;
			[  Details: ] + oErr.DETAILS +CHR(13)+;
			[  StackLevel: ] + STR(oErr.STACKLEVEL) +CHR(13)+;
			[  LineContents: ] + oErr.LINECONTENTS+CHR(13)+;
			[  UserValue: ] + oErr.USERVALUE+CHR(13)+;
			[  Computer:  ] +lcSourceMachine+CHR(13)+;
			[  Program:   ] +lcSourceProgram

		tmessage = tmessage + CHR(13) + cUserMessage

		tattach = ""
		tCC = IIF(lAriat,"paul.gaidis@tollgroup.com,juan.rocio@tollgroup.com","")
		tFrom = "TGF EDI Processing Center <toll-edi-ops@tollgroup.com>"
		DO FORM m:\dev\frm\dartmail2 WITH tSendto,tFrom,tsubject,tCC,tattach,tmessage,"A"
	ELSE
		WAIT WINDOW AT 10,10 "Normal Exit " TIMEOUT 2
	ENDIF
FINALLY
	WAIT WINDOW "Finished processing all accounts/locations...exiting"  TIMEOUT 2
	WAIT CLEAR
	IF USED('FTPSETUP')
		USE IN FTPSETUP
	ENDIF
	IF !lTesting
		schedupdate()
		SELECT 0
		USE F:\edirouting\FTPSETUP SHARED
		REPLACE FTPSETUP.chkbusy WITH .F. FOR FTPSETUP.TRANSFER = "TRIG-PARCEL945"
	ENDIF
	CLOSE DATA ALL
ENDTRY

********************************************************************************
PROCEDURE consigneecheck
********************************************************************************
	cConsignee = ALLTRIM(outshipmast.consignee)
	STORE .F. TO lAmazon,lAmazonFBA
	DO CASE
		CASE cConsignee = "AMAZON FBA"
			lAmazonFBA = .T.
		CASE "AMAZON"$cConsignee
			lAmazon = .T.
	ENDCASE

	DO CASE
		CASE cConsignee = "SAMPLE " ;
				AND cConsignee#"SAMPLER STORES" ;
				AND INLIST(outshipmast.accountid,&gsamsungaccounts)
			DELETE NEXT 1 IN outshipmast
			lLoop = .T.
		CASE "SAMPLE"$cConsignee ;
				AND !INLIST(outshipmast.accountid,6803,6561,6532) ;
				AND (INLIST(outshipmast.accountid,&gsamsungaccounts) ;
				OR INLIST(outshipmast.accountid,&gtkohighlineaccounts)) ;
				AND !("FRENCH CONNECTION"$cConsignee)
			DELETE NEXT 1 IN outshipmast
			lLoop = .T.
		CASE (cSR = "SAMPLE" OR "DAMAGE"$cSR)
			DELETE NEXT 1 IN outshipmast
			lLoop = .T.
	ENDCASE
ENDPROC

********************************************************************************
PROCEDURE scaccheck
********************************************************************************
	WAIT WINDOW "In SCAC crosscheck" NOWAIT
	cSCAC = outshipmast.scac
	STORE outshipmast.swcnum TO cSWCNum
	lParcelRecs = .T.
	DO CASE
		CASE INLIST(nAcctNum,&gmerkuryaccounts) AND (cSCAC = "MIMK")  && Re-added 04.21.2015, Joe
			cbn2="MIMK"+cShip_refDel
			cbl6 = RIGHT(TRIM(cbn2),6)
			xsqlexec("update outship set bol_no = '"+cbn2+"' where accountid in ("+gmerkuryaccounts+") and mod = '"+cMod+"' and ship_ref = '"+cSR+"'",,,"wh")
			xsqlexec("update outship set blnum6 = '"+cbl6+"' where accountid in ("+gmerkuryaccounts+") and mod = '"+cMod+"' and ship_ref = '"+cSR+"'",,,"wh")
			RELEASE cbn2,cbl6
			lLoop = .T.
		CASE INLIST(nAcctNum,&gBioworldAccounts) AND ("AMAZON"$outshipmast.consignee OR "MODERN LIVING"$outshipmast.consignee)
			IF SEEK(cShip_refDel,'outship','ship_ref')
				cbn2="AMZ"+cShip_refDel
				cbl6 = RIGHT(TRIM(cbn2),6)
				xsqlexec("update outship set bol_no = '"+cbn2+"' where accountid in ("+gBioworldAccounts+") and mod = '"+cMod+"' and ship_ref = '"+cSR+"'",,,"wh")
				xsqlexec("update outship set blnum6 = '"+cbl6+"' where accountid in ("+gBioworldAccounts+") and mod = '"+cMod+"' and ship_ref = '"+cSR+"'",,,"wh")
				RELEASE cbn2,cbl6
				lLoop = .T.
			ENDIF
	ENDCASE
ENDPROC

********************************************************************************
*********************** END MAIN PROGRAM CODE ******************************
********************************************************************************

****************************
PROCEDURE parcelmail
****************************
	tsubject= " 945 Parcel Trigger Master Issues List"
	tattach = ""
	tmessage = "During 945 triggering, the following issues were noted:"+CHR(13)+CHR(13)
	cMailList = ""

	IF RECCOUNT('osfreightnobol')>0
		cTruckHeader = "The following PTs have a trucking SCAC and del. date, but no BOL#."+CHR(13)
		cTruckHeader = cTruckHeader+CHR(13)+PADR("Acct",6)+PADR("PT",22)+"SCAC"+CHR(13)
		cMailList = cTruckHeader
		SELECT osfreightnobol  && Freight-SCAC shipments w/o BOL#
		LOCATE
		SCAN
			cMailList = cMailList+PADR(ALLTRIM(STR(osfreightnobol.accountid)),6)+PADR(ALLTRIM(osfreightnobol.ship_ref),22)+osfreightnobol.scac+CHR(13)
		ENDSCAN
	ENDIF

	IF RECCOUNT('outshipmast')>0
		cMissShipments = ""
		cMissShipments = CHR(13)+"The following PTs have a parcel SCAC and del. date, but no records in the Shipment table."
		cMissShipments = cMissShipments+CHR(13)+"1) Those with Keyrec data (verify any trk. number matches):"
		cMissShipments = cMissShipments+CHR(13)+PADR("Acct",6)+PADR("PT",25)+PADR("Keyrec",22)+"SCAC"+CHR(13)
		cMailList = cMailList+cMissShipments+CHR(13)
		SELECT outshipmast
		COUNT TO nempties FOR EMPTY(outshipmast.keyrec)
		LOCATE
		SCAN FOR outshipmast.matched=0 AND !EMPTY(outshipmast.keyrec)
			cMailList = cMailList+PADR(ALLTRIM(STR(outshipmast.accountid)),6)+PADR(ALLTRIM(outshipmast.ship_ref),25)+PADR(ALLTRIM(outshipmast.keyrec),22)+outshipmast.scac+CHR(13)
		ENDSCAN
		IF nempties > 0  && Empty keyrec records
			cMissShipments = ""
			cMissShipments = CHR(13)+"2) Those without Keyrec data (get trk. numbers from warehouse):"
			cMissShipments = cMissShipments+CHR(13)+PADR("Acct",6)+PADR("PT",22)+"SCAC"+CHR(13)
			cMailList = cMailList+cMissShipments+CHR(13)
			SELECT outshipmast
			LOCATE
			SCAN FOR outshipmast.matched = 0 AND EMPTY(outshipmast.keyrec)
				cMailList = cMailList+PADR(ALLTRIM(STR(outshipmast.accountid)),6)+PADR(ALLTRIM(outshipmast.ship_ref),22)+outshipmast.scac+CHR(13)
			ENDSCAN
		ENDIF
	ENDIF

	IF RECCOUNT('shiptrack')>0
		SELECT shiptrack
		cEmptyTrack = ""
		cEmptyTrack = CHR(13)+"The following PTs have record(s) in the Shipment table, but no tracking numbers entered."
		cEmptyTrack = cEmptyTrack+CHR(13)+PADR("Acct#",6)+PADR("PT#",22)+CHR(13)
		cMailList = cMailList+cEmptyTrack+CHR(13)
		SCAN
			cMailList= cMailList+PADR(ALLTRIM(STR(shiptrack.accountid)),6)+ALLTRIM(shiptrack.pickticket)+CHR(13)
		ENDSCAN

	ENDIF

	IF lTesting
		CLEAR
		SET MEMOWIDTH TO 80
		?cMailList
	ELSE
		tmessage =	tmessage+CHR(13)+CHR(13)+cMailList
		DO FORM m:\dev\frm\dartmail2 WITH tsendtotrk,tFrom,tsubject,tCCtrk,tattach,tmessage,"A"
	ENDIF
	cMailList = ""
ENDPROC
