lparameters xview

if xview
	xinvoice="vinvoice"
	xinvdet="vinvdet"
	xinvdetfilter=".t."
else
	xinvoice="invoice"
	xinvdet="invdet"
	xinvdetfilter="invoiceid=invoice.invoiceid"
endif

xsqlexec("delete from prorate where invnum='"+&xinvoice..invnum+"'",,,"ar")

select (xinvoice)
scatter memvar fields accountid, acctname, invnum, invdt, wo_num

xdate=&xinvoice..profromdt
xnumdays=0
do while .t.
	if (!inlist(dow(xdate),7,1) and !holiday(xdate)) or xdate=&xinvoice..invdt
		xnumdays=xnumdays+1
	endif
	xdate=xdate+1
	if xdate>&xinvoice..protodt
		exit
	endif
enddo

select (xinvdet)
scan for !main and invdetamt#0 and &xinvdetfilter
	scatter memvar fields company, glcode

	if between(&xinvoice..invdt,&xinvoice..profromdt,&xinvoice..protodt)
		xprorateamt=round(invdetamt/xnumdays,2)
		xfudge1=xprorateamt*xnumdays-invdetamt
		xfudge2=0
	else
		xprorateamt=round(invdetamt/xnumdays,2)
		xfudge2=xprorateamt*xnumdays-invdetamt
		m.prorateamt=-invdetamt
		m.proratedt=&xinvoice..invdt
		insertinto("prorate","ar",.t.)
	endif

	xdate=&xinvoice..profromdt
	do while .t.
		if (!inlist(dow(xdate),7,1) and !holiday(xdate)) or xdate=&xinvoice..invdt
			m.proratedt=xdate
			if &xinvoice..invdt=xdate
				m.prorateamt=xprorateamt-&xinvdet..invdetamt-xfudge1
			else
				m.prorateamt=xprorateamt-xfudge2
				xfudge2=0
			endif
			insertinto("prorate","ar",.t.)
		endif

		xdate=xdate+1
		if xdate>&xinvoice..protodt
			exit
		endif
	enddo
endscan
