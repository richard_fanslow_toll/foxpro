* find errors in the development environment 

lparameters xnobackup

utilsetup("VAL7")

store "" to guserid, gprocess
gdevelopment=.f.
xjobno=0
xsystem="VAL7"

use f:\auto\val7 exclusive
zap
use

use f:\auto\deverr exclusive
zap
use

**

valset("delete _ref project files")

adir(aref,"m:\dev\proj\*.*")

for i=1 to alen(aref,1)
	if "_REF"$upper(aref[i,1])
		delete file ("m:\dev\proj\"+aref[i,1])
	endif
next

**

valset("delete err & bak files in proj")

adir(aref,"m:\dev\proj\*.*")

for i=1 to alen(aref,1)
	if ".ERR"$upper(aref[i,1]) or ".BAK"$upper(aref[i,1])
		delete file ("m:\dev\proj\"+aref[i,1])
	endif
next

**

valset("delete err files in prg")

adir(aref,"m:\dev\prg\*.*")

for i=1 to alen(aref,1)
	if ".ERR"$upper(aref[i,1])
		delete file ("m:\dev\prg\"+aref[i,1])
	endif
next

**

valset("move prg bak files")

adir(aref,"m:\dev\prg\*.*")

for i=1 to alen(aref,1)
	if ".BAK"$upper(aref[i,1])
		copy file ("m:\dev\prg\"+aref[i,1]) to ("m:\bak\prgbak\"+aref[i,1])
		delete file ("m:\dev\prg\"+aref[i,1])
	endif
next

**

valset("find project master file errors")

use f:\auto\deverr in 0
useca("project","bt",.t.)

select project
scan for empty(exefolder) 
	insert into deverr (problem, filename, developer) values ("missing exe location", project.projname, project.developer1)
endscan

select project
scan for empty(developer1)
	insert into deverr (problem, filename) values ("missing developer name",project.projname)
endscan

**

valset("find fxp with no prg")

use f:\auto\deverr in 0

adir(aref,"m:\dev\prg\*.fxp")

for i=1 to alen(aref,1)
	if !file("m:\dev\prg\"+strtran(upper(aref[i,1]),".FXP","")+".prg")
		insert into deverr (problem, filename) values ("fxp with no prg",aref[i,1])
	endif
next

** removed dy 11/24/14 could be useful some day

*!*	valset("find prg with no fxp")

*!*	use f:\auto\deverr in 0

*!*	adir(aref,"m:\dev\prg\*.prg")

*!*	for i=1 to alen(aref,1)
*!*		if !file("m:\dev\prg\"+strtran(upper(aref[i,1]),".PRG","")+".fxp")
*!*			insert into deverr (problem, filename) values ("prg with no fxp",aref[i,1])
*!*		endif
*!*	next

**

valset("find incorrect file types")

use f:\auto\deverr in 0

adir(aref,"m:\dev\frm\*.*")

for i=1 to alen(aref,1)
	xextension=upper(right(trim(aref[i,1]),3))
	if !inlist(xextension,"SCX","SCT","DEP","DLL") and right(xextension,2)#".H"
		insert into deverr (problem, filename) values ("incorrect file type in m:\dev\frm",aref[i,1])
	endif
next

adir(aref,"m:\dev\lib\*.*")

for i=1 to alen(aref,1)
	xextension=upper(right(trim(aref[i,1]),3))
	if !inlist(xextension,"VCX","VCT","FLL") and right(xextension,2)#".H"
		insert into deverr (problem, filename) values ("incorrect file type in m:\dev\lib",aref[i,1])
	endif
next

adir(aref,"m:\dev\mnu\*.*")

for i=1 to alen(aref,1)
	xextension=upper(right(trim(aref[i,1]),3))
	if !inlist(xextension,"MNX","MPR","MPX","MNT") and right(xextension,2)#".H"
		insert into deverr (problem, filename) values ("incorrect file type in m:\dev\mnu",aref[i,1])
	endif
next

adir(aref,"m:\dev\prg\*.*")

for i=1 to alen(aref,1)
	xextension=upper(right(trim(aref[i,1]),3))
	if !inlist(xextension,"PRG","FXP") and right(xextension,2)#".H"
		insert into deverr (problem, filename) values ("incorrect file type in m:\dev\prg",aref[i,1])
	endif
next

adir(aref,"m:\dev\proj\*.*")

for i=1 to alen(aref,1)
	xextension=upper(right(trim(aref[i,1]),3))
	if !inlist(xextension,"PJT","PJX") and right(xextension,2)#".H"
		insert into deverr (problem, filename) values ("incorrect file type in m:\dev\proj",aref[i,1])
	endif
next

adir(aref,"m:\dev\rpt\*.*")

for i=1 to alen(aref,1)
	xextension=upper(right(trim(aref[i,1]),3))
	if !inlist(xextension,"FRX","FRT","LBT","LBX") and right(xextension,2)#".H"
		insert into deverr (problem, filename) values ("incorrect file type in m:\dev\rpt",aref[i,1])
	endif
next

**

valset("find projects not in master list")

useca("project","bt",.t.)
use f:\auto\deverr in 0

adir(aref,"m:\dev\proj\*.pjx")

for i=1 to alen(aref,1)
	select project
	locate for projname=padr(strtran(aref[i,1],".PJX",""),24)
	if !found()
		insert into deverr (problem, filename) values ("project not found on master list",aref[i,1])
	endif
next

*

valset("find projects in master list but not in m:\dev\proj")

useca("project","bt",.t.)
use f:\auto\deverr in 0

adir(aref,"m:\dev\proj\*.pjx")

select project
scan for !empty(projname) and !notfox
	if !file("m:\dev\proj\"+trim(projname)+".pjx")
		insert into deverr (problem, filename) values ("project in master list not found in m:\dev\proj",project.projname)
	endif
endscan

* removed dy 11/24/14 - restire this when I have time

*!*	valset("find VMP classes in non-framework projects")

*!*	use f:\auto\deverr in 0
*!*	use f:\bugtrack\btdata\project in 0

*!*	=adir(aproj,"m:\dev\proj\*.pjx")

*!*	for i=1 to alen(aproj,1)
*!*		if !inlist(aproj[i,1],"EDI_REPORTS.PJX","HHPP.PJX","KPI.PJX","MENU.PJX","MJSCANRET.PJX","INITPUT.PJX","MJRETPROC.PJX","WAVEPICK.PJX","EDIIMPORT.PJX","MJ_CARTON_RETURN")
*!*			try
*!*				use ("m:\dev\proj\"+aproj[i,1]) alias xproj
*!*				if seek(strtran(aproj[i,1],".PJX",""),"project","projname") and !project.framework
*!*					locate for type="V" and "xlib"$name
*!*					if found()
*!*						insert into deverr (problem, filename) values ("VMP class in non-framework project",strtran(aproj[i,1],".PJX",""))
*!*					endif
*!*				endif
*!*			catch
*!*				insert into deverr (problem, filename) values ("could not open project - VMP class in project",aproj[i,1])
*!*			endtry
*!*		endif
*!*	next

**

valset("find forms with incorrect frmname")

use f:\auto\deverr in 0

=adir(afrm,"m:\dev\frm\*.scx")

for i=1 to alen(afrm,1)
	if !"SQL"$afrm[i,1]
		try	
			use ("m:\dev\frm\"+afrm[i,1])
			locate for class="frm"
			if found()
				xx="frm"+strtran(lower(afrm[i,1]),".scx","")
				locate for class="frm" and xx$lower(properties)
				if !found()
					insert into deverr (problem, filename) values ("frmname property incorrect",afrm[i,1])
				endif
			endif
		catch
			insert into deverr (problem, filename) values ("could not open form - incorrect frmname check",afrm[i,1])
		endtry
	endif
next

**

valset("find files that are in the DEV directory but not included in any projects")

use f:\auto\deverr in 0

create cursor missfile (zproj c(32), zkey c(15), ztype c(1))

create cursor source (name c(32), type c(1), lastedit d, unusedrpt l, count n(3), ;
	proj1 c(25), proj2 c(25), proj3 c(25), proj4 c(25), proj5 c(25), proj6 c(25), proj7 c(25), proj8 c(25), proj9 c(25), proj10 c(25), ;
	proj11 c(25), proj12 c(25), proj13 c(25), proj14 c(25), proj15 c(25), proj16 c(25), proj17 c(25), proj18 c(25), proj19 c(25), proj20 c(25), ;
	proj21 c(25), proj22 c(25), proj23 c(25), proj24 c(25), proj25 c(25), proj26 c(25), proj27 c(25), proj28 c(25), proj29 c(25), proj30 c(25), ;
	proj31 c(25), proj32 c(25), proj33 c(25), proj34 c(25), proj35 c(25), proj36 c(25), proj37 c(25), proj38 c(25), proj39 c(25), proj40 c(25), ;
	proj41 c(25), proj42 c(25), proj43 c(25), proj44 c(25), proj45 c(25), proj46 c(25), proj47 c(25), proj48 c(25), proj49 c(25), proj50 c(25), ;
	proj51 c(25), proj52 c(25), proj53 c(25), proj54 c(25), proj55 c(25), proj56 c(25), proj57 c(25), proj58 c(25), proj59 c(25), proj60 c(25), ;
	proj61 c(25), proj62 c(25), proj63 c(25), proj64 c(25), proj65 c(25), proj66 c(25), proj67 c(25), proj68 c(25), proj69 c(25), proj70 c(25), ;
	proj71 c(25), proj72 c(25), proj73 c(25), proj74 c(25), proj75 c(25), proj76 c(25), proj77 c(25), proj78 c(25), proj79 c(25), proj80 c(25), ;
	proj81 c(25), proj82 c(25), proj83 c(25), proj84 c(25), proj85 c(25), proj86 c(25), proj87 c(25), proj88 c(25), proj89 c(25), proj90 c(25), ;
	proj91 c(25), proj92 c(25), proj93 c(25), proj94 c(25), proj95 c(25), proj96 c(25), proj97 c(25), proj98 c(25), proj99 c(25), proj100 c(25))

index on name+type tag nametype

=adir(aprg,"m:\dev\prg\*.prg")
=adir(afrm,"m:\dev\frm\*.scx")
=adir(arpt,"m:\dev\rpt\*.frx")
=adir(amnu,"m:\dev\mnu\*.mnx")
=adir(alib,"m:\dev\lib\*.vcx")
=adir(aproj,"m:\dev\proj\*.pjx")

for i=1 to alen(aprg,1)
	m.name=strtran(aprg[i,1],".PRG","")
	m.lastedit=aprg[i,3]
	m.type="P"
	insert into source from memvar
next
for i=1 to alen(afrm,1)
	m.name=strtran(afrm[i,1],".SCX","")
	m.lastedit=afrm[i,3]
	m.type="K"
	insert into source from memvar
next
for i=1 to alen(arpt,1)
	m.name=strtran(arpt[i,1],".FRX","")
	m.lastedit=arpt[i,3]
	m.type="R"
	insert into source from memvar
next
for i=1 to alen(amnu,1)
	m.name=strtran(amnu[i,1],".MNX","")
	m.lastedit=amnu[i,3]
	m.type="M"
	insert into source from memvar
next
for i=1 to alen(alib,1)
	m.name=strtran(alib[i,1],".VCX","")
	m.lastedit=alib[i,3]
	m.type="V"
	insert into source from memvar
next

select 0
for i=1 to alen(aproj,1)
	try
		use ("m:\dev\proj\"+aproj[i,1]) alias xproj
		scan for inlist(type,"P","K","R","M","V") and !inlist(key,"X1","X2","X3","X5","X6","X7","X8","XX","MSGSVC ","NOHOT ","WCONNECT ","CODEBLOCKCLASS ","WCVISUAL ","GET856 ","SHELL ","LORUN ","STAR ","STARTUP ","WWIPSTUFF")
			if !seek(left(key,32)+type,"source","nametype")
				if !inlist(aproj[i,1],"INVOICE","WEB")
					insert into missfile (zproj, zkey, ztype) values (aproj[i,1], xproj.key, xproj.type)
				endif
			else
				try
					replace count with count+1 in source
					ii=trans(source.count)
					replace proj&ii with strtran(aproj[i,1],".PJX","") in source
				catch
				endtry
			endif
		endscan	
	catch
		insert into deverr (problem, filename) values ("could not open project - files not included check",aproj[i,1])
	endtry
next

select source
copy to f:\auto\source
scan for empty(proj1) and !"ZZ"$name
	xext=iif(source.type="P",".PRG",iif(source.type="K",".SCX",iif(source.type="R",".FRX",iif(source.type="M",".MNX",iif(source.type="V",".VCX","")))))
*	if source.lastedit<date()-60
		insert into deverr (problem, filename, lastedit) values ("file not found in any project",trim(source.name)+xext,source.lastedit)
*	endif
endscan

**  sadly this is probably not feasible  dy 11/6/15

*!*	valset("find unused framework reports")

*!*	use f:\auto\deverr in 0
*!*	use f:\common\rpt in 0
*!*	use f:\auto\source in 0
*!*	use f:\bugtrack\btdata\project in 0

*!*	select rptname, count(1) as xcnt from rpt where zdatetime>gomonth(date(),-12) group by 1 into cursor xrpt
*!*	index on rptname tag rptname

*!*	select source
*!*	scan for type="R" and !empty(proj1) and lastedit<date()-180
*!*		if !seek(left(name,15),"xrpt","rptname")
*!*			if seek(source.proj1,"project","projname") and project.framework
*!*				replace unusedrpt with .t. in source
*!*				insert into deverr (problem, filename) values ("unused report",source.name)
*!*			endif
*!*		endif
*!*	endscan

**

valset("find files that are in located outside of the development environment")

use f:\auto\deverr in 0

=adir(aproj,"m:\dev\proj\*.pjx")

for i=1 to alen(aproj,1)
	if !inlist(aproj[i,1],"LOOKUPINFILES","MARCJACOBS_VARIOUS","IN7_UTILS","INSERT_SFTPJOBS","LABELPRINT","TIMESTARFTP")
		try
			use ("m:\dev\proj\"+aproj[i,1]) alias xproj
			
			scan for (name="..\..\" and !inlist(name,"..\..\vmp2005","..\..\dev","..\..\..\vmp2005\","..\..\..\dev")) ;
			or (name#"..\" and type#"H" and !inlist(name,"m:\vmp2005\","m:\dev","c:\program files\microsoft visual foxpro 9") and !".db"$name and !".txt"$name and !"foxtools"$name and !"IN7"$name) ;
			or (upper(name)="..\"+trim(key) and inlist(type,"P","K","R","M","V")) ;
			or name="m:\bak"
				if name#"..\wconnect\wconnect.prg" and !".pdf"$name

					insert into deverr (problem, filename) values ("file located outside development environment",strtran(aproj[i,1],".PJX","")+" - "+strtran(xproj.name,chr(10),""))
				endif
			endscan	
		catch
			insert into deverr (problem, filename) values ("could not open project - files outside dev check",aproj[i,1])
		endtry
	endif
next

**

valset("find FXP files in projects")

use f:\auto\deverr in 0

=adir(aproj,"m:\dev\proj\*.pjx")

for i=1 to alen(aproj,1)
	try
		use ("m:\dev\proj\"+aproj[i,1]) alias xproj
		scan for ".FXP"$upper(name)
			insert into deverr (problem, filename) values ("FXP file in project",strtran(aproj[i,1],".PJX","")+" - "+strtran(xproj.name,chr(10),""))
		endscan	
	catch
		insert into deverr (problem, filename) values ("could not open project - find FXP file in project",aproj[i,1])
	endtry
next

**

valset("find reports that have pictures on other drives")

use f:\auto\deverr in 0

=adir(arpt,"m:\dev\rpt\*.frx")

for i=1 to alen(arpt,1)
	try
		use ("m:\dev\rpt\"+arpt[i,1]) alias xrpt
		locate for "F:"$upper(picture)
		if found()
			insert into deverr (problem, filename) values ("report with picture on the F drive",arpt[i,1])
		else
			locate for "C:"$upper(picture)
			if found()
				insert into deverr (problem, filename) values ("report with picture on the C drive",arpt[i,1])
			endif
		endif
	catch
		insert into deverr (problem, filename) values ("could not open report - pictures on drives check",arpt[i,1])
	endtry
next

**

valset("find forms that use class libraries outside of the development environment")

use f:\auto\deverr in 0

=adir(ascx,"m:\dev\frm\*.scx")

for i=1 to alen(ascx,1)
	try
		use ("m:\dev\frm\"+ascx[i,1]) alias xfrm
		locate for (!empty(classloc) and classloc#"..\" and classloc#"c:\program files\microsoft visual foxpro 9") or "\bak"$classloc
		if found()
			insert into deverr (problem, filename) values ("form uses class library outside of development",ascx[i,1])
		endif
	catch
		insert into deverr (problem, filename) values ("could not open form - class library check",ascx[i,1])
	endtry
next

**	don't bother dy 11/7/08

*valset("find development files on F drive")

*use deverr in 0

*filefind("F",,[".PRG",".SCX",".VCX",".FRX",".PJX",".MNU"],"development file found on F drive")

** don't bother dy 3/10/09

*!*	valset("find duplicate project found on M drive")

*!*	use f:\auto\deverr in 0
*!*	use f:\bugtrack\btdata\project in 0

*!*	filefind("M",,[".PJX"],"duplicate project found on M drive")

*!*	select deverr
*!*	scan for problem="duplicate project found on M drive"
*!*		if !seek(trim(strtran(substr(filename,at("\",filename,occurs("\",filename))+1),".PJX",""))+" ","project","projname")
*!*			delete in deverr
*!*		endif
*!*	endscan

*!*	**

valset("check for table on F drive")

use f:\auto\deverr in 0

adir(afrm,"m:\dev\frm\*.scx")

for i=1 to alen(afrm,1)
	if !inlist(afrm[i,1],"POSTPOST","SCANUAPO","UASCAN","INBOUNDAUDIT")
		try
			use "m:\dev\frm\"+afrm[i,1]
			locate for class="cursor" and "F:"$upper(properties) ;
				and !"F:\WH\"$upper(properties) ;
				and !"F:\EDIROUTING\"$upper(properties) ;
				and !"F:\MM\"$upper(properties) ;
				and !"F:\MAIN\USERPWRD"$upper(properties) ;
				and !"F:\MAIN\OFFICE"$upper(properties) ;
				and !"F:\TRANSLOAD"$upper(properties) ;
				and !"F:\EFREIGHT\"$upper(properties) ;
				and !"F:\JONES856\"$upper(properties) ;
				and !"F:\GUESS\"$upper(properties) ;
				and !"F:\TRANSLOAD\TLDATA"$upper(properties) ;
				and !"F:\SYSPOLLER\REPORTS"$upper(properties) ;
				and !"F:\AUTO\HOLIDAYS"$upper(properties) ;
				and !"F:\3PL"$upper(properties) ;
				and !"F:\VF"$upper(properties) ;
				and !"ARCONFIG"$upper(properties) ;
				and !"PRCONFIG"$upper(properties) ;
				and !"COMMON"$upper(properties) ;
				and !"DROPTRACK"$upper(properties) ;
				and !"BIGLOTS"$upper(properties) ;
				and !"F:\UTIL\LF"$upper(properties) ;
				and !"F:\UTIL\LOADTENDER"$upper(properties) ;
				and !"F:\WATUSI\ARDATA\DELWO"$upper(properties) ;
				and !"F:\WATUSI\ARDATA\DELWO"$upper(properties)
			
			if found()
				insert into deverr (problem, filename) values ("form's dataenvironment points to a table on F:",afrm[i,1])
			endif
		catch
		endtry
	endif
next

**

use f:\auto\val7
go bottom
replace zendtime with datetime()
replace all seconds with zendtime-zstarttime
use 

wait clear
close databases all
use f:\auto\deverr
count to xcnt
if xcnt>0
	email("Dyoung@fmiint.com","VAL "+transform(xcnt)+" problems in the development environment","deverr",,,,.t.,,,,,.t.)
endif

schedupdate()

_screen.Caption=gscreencaption
on error

gunshot()