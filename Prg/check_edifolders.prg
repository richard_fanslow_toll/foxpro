utilsetup("INCOMINGFOLDERCHECK")

DO m:\dev\prg\_setvars WITH .T.
*ASSERT .F.
PUBLIC normalexit,lTesting,lCheckTriggers,cFoldername,tcc,tsendto,tsendtoerr,tccerr
normalexit = .T.
CLOSE DATA ALL
DO m:\dev\prg\lookups

lTesting = .F.
_SCREEN.WINDOWSTATE = IIF(lTesting,2,1)
_SCREEN.CAPTION = "TGF FOLDER CHECK (incomingfoldercheck.exe)"
cUserMessage = ""
lCheckFolders = .T.  && Normally true


IF (INLIST(DOW(DATE()),1,7) AND TIME() > "08:15:00") OR (BETWEEN(TIME(),"06:00:00","13:00:00")) && OR (BETWEEN(TIME(),"00:00:05","15:00:00")) && AND INLIST(accountid,6303,6304))
	lCheckTriggers = .F.
ELSE
	lCheckTriggers = .T.
ENDIF

*SET STEP ON
TRY
	DO m:\dev\prg\_setvars
	USE F:\3pl\DATA\mailmaster ALIAS mm SHARED
	LOCATE FOR mm.edi_type = "MISC" AND mm.taskname = "GENERAL"
	tsendtoerr = IIF(mm.use_alt,mm.sendtoalt,mm.sendto)
	tccerr = IIF(mm.use_alt,mm.ccalt,mm.cc)
	USE IN mm

	xsqlexec("select * from account where inactive=0","account",,"qq")
	INDEX ON accountid TAG accountid
	SET ORDER TO

	IF lCheckFolders
		SELECT 0
		USE F:\3pl\DATA\checkedfiles EXCLUSIVE
		DELETE FOR checkedfiles.FILEDATE<(DATE()-5)
		PACK
		USE IN checkedfiles
		USE F:\3pl\DATA\checkedfiles ALIAS ckfiles SHARED
		SELECT 0
		USE F:\3pl\DATA\checkedfolders ALIAS ckfolders ORDER TAG foldername
		dtScantime = DATETIME()
		dtScantime = dtScantime+(60*32)
		IF !lTesting
			WAIT WINDOW "Now scanning folders, please wait" TIMEOUT 1
			WAIT WINDOW "Now scanning folders, please wait" NOWAIT
		ENDIF
		SCAN FOR ckfolders.docheck
			SCATTER MEMVAR FIELDS foldername
			WAIT WINDOW "Now checking folder "+ALLTRIM(foldername) TIMEOUT 1
			cFoldername = ALLT(m.foldername)
			IF cFoldername = "F:\0-PACKINGLIST\MORET-ALL\"
				DELETE FILE (cFoldername+"*.dbf")
			ENDIF
			IF cFoldername = "F:\0-PICKTICKETS\INTRADECO-FL\"
				DELETE FILE (cFoldername+"*.csv")
			ENDIF
			IF lTesting
				? cFoldername
			ENDIF
			IF !DIRECTORY(cFoldername)
				SET STEP ON
				WAIT WINDOW "Directory "+cFoldername+" does not exist...looping" TIMEOUT 2
				DELETE NEXT 1 IN ckfolders
				LOOP
			ENDIF

			CD [&cFoldername]
			IF ADIR(ary1,'*.*')>0
				len1 = ALEN(ary1,1)
				cUserMessage = "Folders containing files: "+CHR(13)
				cUserMessage = cUserMessage + CHR(13) + cFoldername
			ELSE
				RELEASE ALL LIKE ary1
				LOOP
			ENDIF
			FOR yy = 1 TO len1
				m.filename = ary1[yy,1]
				m.FILEDATE = ary1[yy,3]
				m.filetime = ary1[yy,4]
				SELECT ckfiles
				LOCATE FOR ckfiles.foldername = m.foldername AND ;
					ckfiles.filename = m.filename
				IF FOUND()
					IF ckfiles.checktime <= dtScantime+(60*30)
						errormail()
						RELEASE ALL LIKE ary1
						normalexit = .T.
						THROW
					ENDIF
				ELSE
					m.checktime = dtScantime
					INSERT INTO ckfiles FROM MEMVAR
				ENDIF
			ENDFOR
			RELEASE ALL LIKE ary1
		ENDSCAN
		WAIT WINDOW "All folders processed..." TIMEOUT 2
		IF !EMPTY(cUserMessage)
			errormail()
		ENDIF

		SELECT ckfiles

		IF lTesting
			LOCATE
			IF EOF()
				WAIT WINDOW "No pending files in any scanned folder" TIMEOUT 2
				BROWSE
			ENDIF
		ENDIF
	ENDIF

*!* Now checking EDI_TRIGGER for any error records
	IF lCheckTriggers
		IF USED('edi_trigger')
			USE IN edi_trigger
		ENDIF
		cEDIFolder = "F:\3PL\DATA\"
		USE (cEDIFolder+"edi_trigger") IN 0 ALIAS edi_trigger

		SELECT a.edi_type,TTOC(a.trig_time) AS trig_time,STR(a.accountid,4) AS cAcctNum,b.acctname,a.bol;
			FROM edi_trigger a,account b ;
			WHERE a.ERRORFLAG = .T. ;
			AND a.fin_status # "NO 945" ;
			AND a.edi_type # "944X" ;
			AND ((DATETIME()-a.trig_time)/3600) > 0.5 ;
			AND a.accountid = b.accountid ;
			GROUP BY a.accountid,a.bol ;
			INTO CURSOR tempedi1 READWRITE
*			AND TTOD(a.trig_time) >= DATE()-12 ;

		LOCATE
*		BROW
		IF !EOF()
			tcc = ALLTRIM(tccerr)
			IF !("gaidis"$tcc)
				LOCATE FOR INLIST(cAcctNum,&gmjacctlist)
				IF FOUND()
					tcc = IIF(EMPTY(ALLT(tcc)),"pgaidis@fmiint.com,tmarg@fmiint.com",ALLT(tcc)+","+"pgaidis@fmiint.com,tmarg@fmiint.com")
				ENDIF
			ENDIF
			cString1 = PADR("TYPE",6)+PADR("OFFICE",8)+PADR("TRIG TIME",22)+PADR("ACCT#",8)+;
				PADR("ACCT NAME",35)+"BOL #"+CHR(13)
			LOCATE
			SCAN
				cString1 = cString1+CHR(13)+PADR(TRIM(edi_type),6)+PADR(office,8)+PADR(trig_time,22)+PADR(cAcctNum,8)+;
					PADR(TRIM(acctname),35)+bol
			ENDSCAN

			tsendto = ALLTRIM(tsendtoerr)
			tsubject = "EDI_Trigger Table Errors"
			tattach = ""
			tfrom = "TGF EDI Operations <toll-edi-ops@tollgroup.com>"
			tmessage = "The EDI_TRIGGER table lists the following BOLs with errorflag = TRUE"
			tmessage = tmessage+CHR(13)+CHR(13)+cString1+CHR(13)+CHR(13)+"Check and correct."
			SET STEP ON
			DO FORM m:\dev\frm\dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
		ENDIF
		normalexit = .T.
		THROW
	ENDIF
	CLOSE DATA ALL

	IF (BETWEEN(TIME(),"08:00:00","20:00:00") AND INLIST(MINUTE(DATETIME()),15,30,45))
		USE F:\edirouting\ftpsetup.DBF
		replace chkbusy WITH .f. FOR folderchk AND chkbusy AND ftpsetup.transfer # "TRIG" AND (DATETIME()-ftpsetup.trig_time > 850) 
		USE
	ENDIF

	IF (BETWEEN(TIME(),"04:00:00","08:00:00") AND INLIST(MINUTE(DATETIME()),15,30,45))
		USE F:\edirouting\ftpsetup.DBF
		replace chkbusy WITH .f. FOR folderchk AND chkbusy
		USE
	endif
CATCH TO oErr
	IF !normalexit
		ASSERT .F. MESSAGE "AT CATCH SECTION"
		tsubject = "EDI FOLDER ERROR ("+TRANSFORM(oErr.ERRORNO)+") at "+TTOC(DATETIME())
		tattach  = ""
		tsendto  = tsendtoerr
		tcc      = tccerr
		tmessage = "IncomingFolderCheck.pjx Error!"
		tmessage = tmessage+CHR(13)+cFoldername
		lcSourceMachine = SYS(0)
		lcSourceProgram = SYS(16)

		tmessage =tmessage+ "Try/CATCH Exeception Message:"+CHR(13)+;
			[  Error: ] + STR(oErr.ERRORNO) +CHR(13)+;
			[  LineNo: ] + STR(oErr.LINENO) +CHR(13)+;
			[  Message: ] + oErr.MESSAGE +CHR(13)+;
			[  Procedure: ] + oErr.PROCEDURE +CHR(13)+;
			[  Details: ] + oErr.DETAILS +CHR(13)+;
			[  LineContents: ] + oErr.LINECONTENTS+CHR(13)+;
			[  UserValue: ] + oErr.USERVALUE+CHR(13)+;
			[  Computer:  ] +lcSourceMachine+CHR(13)+;
			[  Program:   ] +lcSourceProgram
		tmessage = tmessage + CHR(13) + CHR(13) + cUserMessage
		tattach  = ""
		tfrom    ="TGF EDI Processing Center <toll-edi-ops@tollgroup.com>"
		DO FORM m:\dev\FRM\dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
	ENDIF
FINALLY
	ON ERROR
	CLOSE ALL
ENDTRY

schedupdate()

**********************
PROCEDURE errormail
**********************
	tsendto = tsendtoerr
	tcc = tccerr
	tsubject = "Unprocessed Files in Folders"
	tattach = ""
	tfrom = "TGF EDI Operations <toll-edi-ops@tollgroup.com>"

	tmessage = cUserMessage
	tmessage = tmessage + CHR(13) + CHR(13) + "Check folder(s) and run process manually if needed."
	DO FORM m:\dev\frm\dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"

