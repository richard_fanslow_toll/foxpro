Parameters cRunType,nWO_Num,cInitType

Public c_CntrlNum,c_GrpCntrlNum,cWO_Num,cGroupName,cDivision,cCustName,cFilename,div214,cST_CSZ
Public tsendto,tcc,tsendtoerr,tccerr,cFin_Status,lDoCatch
 
*cRunType = "AB"  && Appt Date, ETA
*cRunType = "AV"  && Available for Delivery
*cRunType = "AF"  && Depart Pickup Location
*cRunType = "X1"  && Dropoped at DC

cProcType = ""

Do case
  Case cRuntype = "AB"
    cProcType = "Appt Date, ETA"
  Case cRuntype = "AV"
    cProcType = "Available"
  Case cRuntype = "AF"
    cProcType = "Departing for Tremont"
  Case cRuntype = "X1"
    cProcType = "Dropped at Tremont"
  otherwise  
    cProcType = "UNK"
EndCase 

cOffice = "N"

cFilename = ""
lTesting = .F.
lFilesOut = !lTesting
lEmail = !lTesting
lUseTestData = .F.

*close data all 


On Escape Cancel
lFClose = .T.
lIsError = .F.
lDoCatch = .T.
cShip_ref = ""
cST_CSZ = ""
cFin_Status = ""
nFilenum = 0
Set Hours To 24

*ASSERT .f.
Try
  cSystemName = "UNASSIGNED"
  nAcctNum = 4717
  cWO_Num = ""
  lDoError = .F.

  cDivision = "New Jersey"
  cOffCity = "CARTERET"
  cOffState = "NJ"

  cWO_Num = Alltrim(Str(nWO_Num))

  Dimension thisarray(1)
  cCustName = "BIGLOTS"
  cMailName = "BIGLOTS"
  cCustFolder = "BIGLOTS"
  dtmail = Ttoc(Datetime())
  dt2 = Datetime()
  cCustPrefix = cCustName+"214"+"_"
  dt1 = Ttoc(Datetime(),1)

  Select 0
  Use F:\3pl\Data\mailmaster Alias mm Shared
  Locate For mm.edi_type = "214" and mm.accountid = nAcctNum 
    _Screen.Caption = Alltrim(mm.scaption)
  tsendto = Iif(mm.use_alt,mm.sendtoalt,mm.sendto)
  tcc = Iif(mm.use_alt,mm.ccalt,mm.cc)

  cFilename = (ALLTRIM(mm.holdpath)+cCustPrefix+dt1+".txt")
  cFilenameShort = Justfname(cFilename)
  cFilename2 = (Alltrim(mm.archpath)+cFilenameShort)
  cFilename3 = (Alltrim(mm.basepath)+cFilenameShort)

  Locate
  Locate For mm.office = 'X' And mm.accountid = 9999
  tsendtoerr = Iif(mm.use_alt,mm.sendtoalt,mm.sendto)
  tccerr = Iif(mm.use_alt,mm.ccalt,mm.cc)

  Use In mm

  tfrom = "TGF Warehouse Operations <fmi-warehouse-ops@fmiint.com>"
  tattach = ""

  Select 0
  Use

 nFilenum = Fcreate(cFilename)

Select 0
Use F:\wo\wodata\wolog Alias wolog Order Tag wo_num Shared Noupdate

  cfd = "*"  && Field delimiter
  csegd = "~"  && Segment delimiter
  cterminator = ">"  && Used at end of ISA segment

  Select wolog
  =Seek(nWO_Num,"WOLOG","WO_NUM")

  If !Found("wolog")
    cFin_Status = "MISSING WO-WOLOG: "+Alltrim(Str(nWO_Num))
    Throw
  Endif
  
  csendqual = "02"  && Sender qualifier
  csendid = "FMIT"  && Sender ID code
  crecqual = "ZZ"  && Recip qualifier
  crecid = "BLASI"   && Recip ID Code

  cDate = Dtos(Date())
  cTruncDate = Right(cDate,6)
  cTruncTime = Substr(Ttoc(Datetime(),1),9,4)
  cfiledate = cDate+cTruncTime
  csendidlong = Padr(csendid,15," ")
  crecidlong = Padr(crecid,15," ")
  cString = ""

  nSTCount = 0
  nSegCtr = 0
  nLXNum = 1

  Store "" To cEquipNum,cHAWB

  Wait Clear
  Wait Window "Now creating Trucking WO#-based 214.." Nowait

  If lTesting
    cISACode = "T"
  Else
    cISACode = "P"
  Endif

  Do num_incr_isa

  cISA_Num = Padl(c_CntrlNum,9,"0")

  Store "ISA"+cfd+"00"+cfd+"          "+cfd+"00"+cfd+"          "+cfd+csendqual+cfd+csendidlong+cfd+;
    crecqual+cfd+crecidlong+cfd+cTruncDate+cfd+cTruncTime+cfd+"^"+cfd+"00401"+cfd+;
    cISA_Num+cfd+"0"+cfd+cISACode+cfd+":"+csegd To cString  && +cfd+cterminator
  Do cstringbreak

  Store "GS"+cfd+"QM"+cfd+csendid+cfd+crecid+cfd+cDate+cfd+cTruncTime+cfd+c_CntrlNum+;
    cfd+"X"+cfd+"004010"+csegd To cString
  Do cstringbreak
  Do num_incr_st

  Store "ST"+cfd+"214"+cfd+Padl(c_GrpCntrlNum,9,"0")+csegd To cString
  Do cstringbreak
  nSTCount = nSTCount + 1
  nSegCtr = nSegCtr + 1
  cRefID = Allt(wolog.brokref)

  Store "B10"+cfd+Alltrim(Str(torun.wo_num))+cfd+Alltrim(wolog.apptnum)+cfd+csendid+csegd To cString
  Do cstringbreak
  nSegCtr = nSegCtr + 1

*!* Now in line loop
  Store "LX"+cfd+Alltrim(Str(nLXNum))+csegd To cString  && Size

  Do cstringbreak
  nSegCtr = nSegCtr + 1
  nLXNum = nLXNum + 1

  cShipStatus = Alltrim(cRunType)

 *cRunType = "AB"  && Appt Date, ETA
 *cRunType = "AV"  && Available for Delivery
 *cRunType = "AF"  && Depart Pickup Location
 *cRunType = "X1"  && Dropoped at DC

Set Step On 
  Do case
    case cRunType = "AB" and cInitType = "INIT"  && Appt Date, ETA
      cStatusDate = Date()+6
      cStatusTime = Strtran(Left(Ttoc(cStatusDate,2),5),":","")
      cStatusDate = Dtoc(cStatusDate,1)
      Store "AT7"+cfd+cfd+cfd+"AB"+cfd+"NS"+cfd+cStatusDate+cfd+cStatusTime+cfd+"LT"+csegd To cString
    case cRunType = "AB" and cInitType != "INIT"  &&  Appt Date, ETA
      cStatusDate = Date()+3
      cStatusTime = Strtran(Left(Ttoc(cStatusDate,2),5),":","")
      cStatusDate = Dtoc(cStatusDate,1)
      Store "AT7"+cfd+cfd+cfd+"AB"+cfd+"NS"+cfd+cStatusDate+cfd+cStatusTime+cfd+"LT"+csegd To cString
    case cRunType = "AV"  && Available for Delivery 
      cStatusDate = Date()+3
      Whichday =Dow(cStatusDate) 
      Do case
        Case Whichday=6
         cStatusDate=cStatusDate+2
        Case Whichday=7
         cStatusDate=cStatusDate+3
      EndCase    
      cStatusTime = Strtran(Left(Ttoc(cStatusDate,2),5),":","")
      cStatusDate = Dtoc(cStatusDate,1)
      Store "AT7"+cfd+cfd+cfd+"AB"+cfd+"NS"+cfd+cStatusDate+cfd+cStatusTime+cfd+"LT"+csegd To cString
    case cRunType = "AF"  && Depart Pickup Location
      cStatusDate = torun.trig_time 
      cStatusTime = Strtran(Left(Ttoc(torun.trig_time,2),5),":","")
      cStatusDate = Dtoc(cStatusDate,1)
      Store "AT7"+cfd+"AF"+cfd+"NS"+cfd+cfd+cfd+cStatusDate+cfd+cStatusTime+cfd+"LT"+csegd To cString
    case cRunType = "X1"  && Dropoped at DC
      cStatusDate = torun.trig_time 
      cStatusTime = Strtran(Left(Ttoc(torun.trig_time,2),5),":","")
      cStatusDate = Dtoc(cStatusDate,1)
      Store "AT7"+cfd+"X1"+cfd+"NS"+cfd+cfd+cfd+cStatusDate+cfd+cStatusTime+cfd+"LT"+csegd To cString
   EndCase 

  Do cstringbreak
  nSegCtr = nSegCtr + 1
    
  Store "MS2"+cfd+Substr(Alltrim(wolog.container),1,4)+cfd+Substr(Alltrim(wolog.container),5)+csegd To cString
  Do cstringbreak
  nSegCtr = nSegCtr + 1
 
*!*    If Empty(wolog.delivered)
*!*      cStatusDate = Dtoc(Date(),1)
*!*    Else
*!*      cStatusDate = Dtoc(wolog.delivered,1)
*!*    Endif
*!*    cStatusTime = Strtran(Left(Ttoc(wolog.updatedt,2),5),":","")

*!*    If lTesting And Empty(wolog.updatedt)
*!*      cStatusTime = "1530"
*!*    Endif


*!* Finish processing
  Do close214
  If nFilenum>0
    =Fclose(nFilenum)
  Endif

  Assert .F. Message "At file closure"
  
  If !lTesting
    Do ediupdate With "214 CREATED",.F.,.F.
  EndIf   


  Copy File [&cFilename] To [&cFilename2]
  If lFilesOut
    Copy File &cFilename To &cFilename3
    Delete File &cFilename
  Endif

  If !lTesting
    If !Used("ftpedilog")
      Select 0
      Use F:\edirouting\ftpedilog Alias ftpedilog
      Insert Into ftpedilog (TRANSFER,ftpdate,filename,acct_name,Type) Values ("214-"+cCustName,dt2,cFilename,Upper(cCustName),"214")
      Use In ftpedilog
    Endif
  Endif

  If lTesting
    tsubject = "Big Lots 214 *TEST* EDI from TGF as of "+dtmail
  Else
    tsubject = "Big Lots 214 EDI from TGF as of "+dtmail
  Endif

  tmessage = "214 ("+cProcType+") EDI Info from TGF-CA, Truck WO# "+cWO_Num+Chr(10)
  tmessage = tmessage + "for "+cMailName+Chr(13)
  tmessage = tmessage +cRunType+" has been created."+Chr(10)+"(Filename: "+cFilenameShort+")"
*!*  +CHR(10)+CHR(10)
*!*  tmessage = tmessage + "If you have any questions, please eMail or call 732-750-9000 x217."
  If lTesting
    tmessage = tmessage + Chr(10)+Chr(10)+"This is a TEST RUN..."
  Endif

  If lEmail
*    Do Form dartmail2 With tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
  Endif

  Select 0
  Use F:\edirouting\ftpjobs
*  Insert Into ftpjobs (jobname,Userid,jobtime) Values ("214-BIGLOTS","PAULG",Datetime())

  Release All Like c_CntrlNum,c_GrpCntrlNum
  Release All Like nOrigSeq,nOrigGrpSeq
  Wait Clear
  Wait Window cMailName+" 214 "+cProcType+" EDI File output complete" At 20,60 nowait &&Timeout 2

  closefiles()

Catch To oErr
Set Step On 
  If lDoCatch
    lEmail = .T.
    If Empty(cFin_Status)
      cFin_Status = "ERRHAND ERROR"
    Endif
    Do ediupdate With cFin_Status,.T.
    tsubject = cCustName+" Error ("+Transform(oErr.ErrorNo)+") at "+Ttoc(Datetime())
    tattach  = ""
    tsendto  = "PGAIDIS@fmiint.com"
    tmessage = cCustName+" Error processing "+cRunType+Chr(13)+cFin_Status+Chr(13)

    tmessage =tmessage+ "Try/CATCH Exeception Message:"+Chr(13)+;
      [  Error: ] + Str(oErr.ErrorNo) +Chr(13)+;
      [  LineNo: ] + Str(oErr.Lineno) +Chr(13)+;
      [  Message: ] + oErr.Message +Chr(13)+;
      [  Procedure: ] + oErr.Procedure +Chr(13)+;
      [  Details: ] + oErr.Details +Chr(13)+;
      [  StackLevel: ] + Str(oErr.StackLevel) +Chr(13)+;
      [  LineContents: ] + oErr.LineContents+Chr(13)+;
      [  UserValue: ] + oErr.UserValue

 
    tsubject = "BIG LOTS EDI Processing Error at "+Ttoc(Datetime())
    tattach  = ""
    tcc=""
    tfrom    ="TGF Big Lots EDI Processing Error <fmi-transload-ops@fmiint.com>"
    Do Form dartmail2 With tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
    lEmail = .F.
  Endif
Finally
  Set Hours To 12
  Fclose(nFilenum)
  closefiles()
Endtry

&& END OF MAIN CODE SECTION



****************************
Procedure num_incr_isa
****************************

If !Used("serfile")
  Use ("f:\3pl\data\serial\"+cCustName+"_serial") In 0 Alias serfile
Endif
Select serfile
If lTesting
  nOrigSeq = serfile.seqnum
  nOrigGrpSeq = serfile.grpseqnum
  lISAFlag = .F.
Endif
nISA_Num = serfile.seqnum
c_CntrlNum = Alltrim(Str(serfile.seqnum))
Replace serfile.seqnum With serfile.seqnum + 1 In serfile
Select wolog
Endproc

****************************
Procedure num_incr_st
****************************

If !Used("serfile")
  Use ("f:\3pl\data\serial\"+cCustName+"_serial") In 0 Alias serfile
Endif
Select serfile
If lTesting
  nOrigSeq = serfile.seqnum
  nOrigGrpSeq = serfile.grpseqnum
  lSTFlag = .F.
Endif
c_GrpCntrlNum = Alltrim(Str(serfile.grpseqnum))
Replace serfile.grpseqnum With serfile.grpseqnum + 1 In serfile
Select wolog
Endproc

****************************
Procedure segmentget
****************************

Parameter thisarray,lcKey,nLength

For i = 1 To nLength
  If i > nLength
    Exit
  Endif
  lnEnd= At("*",thisarray[i])
  If lnEnd > 0
    lcThisKey =Trim(Substr(thisarray[i],1,lnEnd-1))
    If Occurs(lcKey,lcThisKey)>0
      Return Substr(thisarray[i],lnEnd+1)
      i = 1
    Endif
  Endif
Endfor

Return ""

****************************
Procedure close214
****************************
Store  "SE"+cfd+Alltrim(Str(nSegCtr+1))+cfd+Padl(c_GrpCntrlNum,9,"0")+csegd To cString
Do cstringbreak

Store  "GE"+cfd+Alltrim(Str(nSTCount))+cfd+c_CntrlNum+csegd To cString
Do cstringbreak

Store  "IEA"+cfd+"1"+cfd+Padl(c_CntrlNum,9,"0")+csegd To cString
Do cstringbreak

Return

****************************
Procedure cstringbreak
****************************
cLen = Len(Alltrim(cString))
Fputs(nFilenum,cString)
Return



**************************
Procedure ediupdate
**************************
Parameters cFin_Status, lIsError,lFClose
cRunVar1 = cRunType+"214"
cRunVar2 = "214"+cRunType

If !lTesting
  Select edi_trigger
  Locate
  If lIsError
    Replace edi_trigger.processed With .T.,edi_trigger.created With .F.,;
      edi_trigger.fin_status With cFin_Status,edi_trigger.errorflag With .T.;
      edi_trigger.when_proc With Datetime() ;
      FOR edi_trigger.wo_num = nWO_Num And Inlist(edi_type,cRunVar1,cRunVar2)
    If nFilenum>0
      =Fclose(nFilenum)
    Endif
    If File(cFilename)
      Delete File &cFilename
    Endif
    closefiles()
  Else
    Replace edi_trigger.processed With .T.,edi_trigger.created With .T.,proc214 With .T.;
      edi_trigger.when_proc With Datetime(),;
      edi_trigger.fin_status With cRunType+"214 CREATED";
      edi_trigger.errorflag With .F.,file214 With cFilename ;
      FOR edi_trigger.wo_num = nWO_Num
  Endif
Endif
Endproc

*********************
Procedure closefiles
*********************
If Used('ftpedilog')
  Use In ftpedilog
Endif
If Used('serfile')
  Use In serfile
Endif
If Used('wolog')
  Use In wolog
Endif
If Used('xref')
  Use In xref
Endif
If Used('ftpjobs')
  Use In ftpjobs
Endif
Endproc
