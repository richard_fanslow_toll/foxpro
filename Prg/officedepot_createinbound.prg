*
* Take the data from the 856 just uploaded and create an Inbound Work Order
*
*
*Set Step On 
DO m:\dev\prg\_setvars WITH .T.
&&Set Step On 
lTesting = .f.

naccountid = 6494
m.accountid = 6494

Select * From inwolog Where .F. Into Cursor xinwolog Readwrite
Select * From pl      Where .F. Into Cursor xpl Readwrite

Set EngineBehavior 70
Select bol,store,Count(1) as ctnqty From xdetail Into Cursor temp Group By bol,store

Select temp

m.plid =0
m.inwologid =0

m.inwologid = m.inwologid + 1

Select xdetail
Goto top
Scatter memvar
m.accountid = 6494
m.acctname ="OFFICE DEPOT"
m.comments = "ASN UPLOAD: "+lcFilename+Chr(13)+"CTR "+m.Container+Chr(13)

Insert Into xinwolog From Memvar

Replace xinwolog.acct_ref   With m.bol
Replace xinwolog.brokerref  With m.shipid
Replace xinwolog.Container  With m.Container

Select temp
Goto Top
firstloop = .T.
ctnsum =0
plidvalue = 1

Scan
  Select xpl
  Scatter Memvar Blank Memo
  m.inwologid = xinwolog.inwologid
  m.plid   = plidvalue
  m.units  = .F.
  m.totqty = temp.ctnqty
  ctnsum   = ctnsum + m.totqty
  m.style  = alltrim(temp.bol)+"-"+temp.store
  m.color  = ""
  m.pack   = "1"
  m.accountid = 6494
  Insert Into xpl From Memvar
  Replace xinwolog.plunitsinqty With xinwolog.plunitsinqty+(temp.ctnqty*Val(Transform(Pack))) In xinwolog
  Replace xinwolog.plinqty With ctnsum In xinwolog
  Replace xinwolog.plopenqty With ctnsum In xinwolog
  plidvalue = plidvalue +1
Endscan

**************  OK now the Import  **************************888

Select xinwolog
Locate

Scan  && Currently in XINWOLOG table
  cContainer = Allt(xinwolog.Container)
  cCtnQty = Allt(Str(xinwolog.plinqty))
  cUnitQty = Allt(Str(xinwolog.plunitsinqty))

  Select inwolog
  cOffice = "C"

  Select xinwolog
  Scatter Memvar Memo
  Select inwolog
  m.inwologid = dygenpk("INWOLOG","wh8")
  m.wo_num = dygenpk("WONUM","wh8")
  cWO_Num = Allt(Str(m.wo_num))
  m.wo_date = Date()

  insertinto("inwolog","wh",.T.)
  tu("inwolog")

  Select xpl
  Scan For inwologid = xinwolog.inwologid
    Scatter Memvar Memo
    m.inwologid=inwolog.inwologid
    m.wo_num = inwolog.wo_num
    
   	insertinto("pl","wh",.t.)
  Endscan
	tu("pl")

Endscan

Select xdetail
Scan
  Select xdetail
  replace xdetail.inwonum With  Val(cWO_Num)
  replace xdetail.style With alltrim(xdetail.bol)+"-"+xdetail.store
  Scatter Memvar fields except ctnuccid

*!*	  Select whgenpk		dy 12/26/17
*!*	  Locate For gpk_pk = "CTNUCC"
*!*	  Replace gpk_currentnumber With gpk_currentnumber + 1
*!*	  m.ctnuccid = gpk_currentnumber

  m.ctnuccid = sqlgenpk("ctnucc","wh")

  Select ctnucc
  Append Blank
  Gather memvar
Endscan





