************************************************************************
* Started out as 856 FILES TO send to QIVA
* AUTHOR--:PG
* DATE----:05/01/2002
* Modified much
************************************************************************
**Modification (mvw 12/14/10): We will now run a similar method 
**	ninewest856out.prg for ninewest wos found in asnwo.dbf
************************************************************************
*
do setenvi

utilsetup("JAG856OUT")

Set Century On
Set Date To ymd
Set Decimal To 0

Close Data All
Store Chr(07) To segterminator
Store "~" To segterminator

Store "" To fstring
Store 0 To shiplevel

Public tfile, tshipid,cstring,slevel,plevel,lnInterControlnum,lcInterctrlNum,stNum,gn856Ctr
Public lcCode,lcOrigin,lcDischarge, lcDocrcpt

lcCode = "UNKN"
lcOrigin= "UNK"
lcDischarge= "UNK"
lcDocrcpt = "UNK"

store 0 to gn856ctr,stnum,lncttcount,lnsecount
store 0 to hlcount2,hlpack,spcntp,hlsegcnt,segcnt,spcnt,spcnt2,hlitem,spcnti,lc_hlcount,a,lincnt,totlincnt,sesegcnt,segcnt

Store "" To workordersprocessed,summaryinfo,lc_856num,lc_cnum

* top level counter for unique HL segment counts
Store 0 To hlctr

* use these to store overall counters
Store 0 To shipmentlevelcount,orderlevelcount,packlevelcount,itemlevelcount

* use these to store overall counters
Store 0 To thisshipmentlevelcount,thisorderlevelcount,thispacklevelcount,thisitemlevelcount

* used to store the value of the curent count for remembering the Parent HL Count
Store 0 To currentshipmentlevelcount,currentorderlevelcount,currentpacklevelcount,currentitemlevelcount

Store .F. To llEditUpdate

lcstring  = ""
lincnt = 1
*******************************************************************************************
jtimec = Time()
Store Substr(jtimec,1,2) To j1
Store Substr(jtimec,4,2) To j2
Store Substr(jtimec,7,2) To j3
lcTime = j1+j2
tday = Day(Date())
Store Str(tday,2,0)     To tday1
Store Dtoc(Date())      To hdate
Store Substr(hdate,6,2) To tmon
Store Substr(hdate,9,2) To tday
Store Substr(hdate,1,2) To tcent
Store Substr(hdate,3,2) To tyr
Store Substr(hdate,1,4) To twholeyr
Store Substr(hdate,1,2) To d4
Store Substr(hdate,3,2) To d1
Store Substr(hdate,6,2) To d2
Store Substr(hdate,9,2) To d3

*******************************************************************************************
Store 0 To EDI_testing

use f:\wo\wodata\manifest in 0
use f:\jones856\data\controlnum in 0
use f:\jones856\data\asnwo in 0
use f:\wo\wodata\detail in 0
use f:\jones856\jonesdetail in 0
use f:\ninewest856\nwestdetail in 0

xsqlexec("select * from dellocs",,,"wo")
index on str(accountid,4)+location tag acct_loc
set order to

*tsendid    = Padr('FMIF',12," ")
tsendid    = Padr('LOGNET',12," ")
tsendcode  = '2122441111'

tqualcode  = Padr('2122441111',15," ")
tqualtype  = 'ZZ'

Wait Window "Setting up ASNWO........." Nowait

store .f. to xtriggerjag, xtriggernwd

Select asnwo
Scan For !processed and process
	lnWonum = wo_num

	**xoutdir is changed in ninewest856out.prg to place files in the 9west dir if needed, so needs to be reset here - mvw 02/02/11
	xoutdir="f:\ftpusers\jag\856out\"
*	xoutdir='c:\xls\'

	**new method to handle ninewest 856s - mvw 12/14/10
	**removed separate process for 9west as all their data now comes into jonesdetail, not nwestdetail. They are handled as same acct now in lognet - mvw 11/19/12
*!*		if asnwo.accountid=1747
*!*			do ninewest856out
*!*			loop
*!*		endif

	do while .t.
		tfile = "JAGJA2FMI"+Alltrim(Str(Year(Date())))+Padl(Alltrim(Str(Month(Date()))),2,"0")+;
			PADL(Alltrim(Str(Day(Date()))),2,"0")+Padl(Alltrim(Str(Hour(Datetime()))),2,"0")+;
			padl(Alltrim(Str(Minute(Datetime()))),2,"0")+Padl(Alltrim(Str(Sec(Datetime()))),2,"0")+".856"
		tfile=xoutdir+tfile
		if !file(tfile)
			exit
		endif
	enddo

	xfilestr=""

	Wait Window "856 Processing for Work Order "+transform(lnWonum)+"..." Nowait

	WriteHeader()
	xsqlexec("select * from fxwolog where wo_num="+transform(lnwonum),"xfxwolog",,"fx")

	xerror=.f.
	store "" to xerrormsg, xmemomsg &&may be updated in shipment_level() if error occurs

	select * from manifest where wo_num = lnwonum order by orig_wo,hawb,po_num into cursor tmfst READWRITE
	**if been deleted from manifest since trigger created - mvw 11/12/13
	if eof()
		xerror=.t.
		xmemomsg="No data in manifest.dbf for this wo"
	endif

	shipmentheader(transform(lnWonum))

	Store 3 To segcnt
	xretrylater=.f.

	if xerror or !shipment_level()
		if !emptynul(xerrormsg)
			tsubject="JAG 856 Creation Error at "+Ttoc(Datetime())
			tmessage=xerrormsg

			tattach=""
			tcc=""
			tsendto="mwinter@fmiint.com"
			tFrom="TGF Corporate Communication Department <fmi-transload-ops@fmiint.com>"
			Do Form dartmail2 With tsendto,tFrom,tsubject,tcc,tattach,tmessage,"A"
		endif

		replace runattempt with datetime(), info with xmemomsg in asnwo
		if !xretrylater
			replace process with .f. in asnwo
		endif
	else
		xerror=.f.

		**xemptyfile: b/c we now ignore any mfst rec with no match in jdetail, it is possible that a file might be created with
		**  no detail info (no mfst recs had a match), if so we need to delete the file and send an email alert - mvw 08/14/08
		xemptyfile=.t.
		xallibdsexist=.t.
		select tmfst
		locate

		do while !eof()
			**changed to process LEI shipments as per Dan Lewis - mvw 04/29/09
			**changed to process GV shipments as per Mary Jane - mvw 01/08/10
*!*				if delloc='TP' or inlist(type,'A','D') or inlist(accountid,62,3544,5632)
			**changed to send all shiopments - no restrictions - per dan lewis - 01/21/11 mvw
*!*				if (inlist(delloc,'TP',"EDISON") or inlist(type,'A','D') or inlist(accountid,3544))
			**added back the air freight exclusion - as per dan lewis - mvw 04/07/11
			**exclude all pos that already have ibd's assigned (!emptynul(arrival)) as per Jeffrey Stein - mvw 11/18/11
			**exclude lines where hawb="xxxxx-1" as they are split pos with both goh and cartons - mvw 06/01/12
			**removed air freight exclusion for 858/210 per Dan Lewsi - mvw 11/19/12
*!*				if type='A' or !emptynul(arrival) or inlist(right(alltrim(hawb),2),"-1","-2")
			**removed emptynul(arrival) filter because we are entering ibds prior to closing manifests! not sure why 
			**  we excluded them anyway.... see if we have any issues sending everything - mvw 12/05/12
			*if !emptynul(arrival) or inlist(right(alltrim(hawb),2),"-1","-2")
			if inlist(right(alltrim(hawb),2),"-1","-2","-3")
				skip
				loop
			endif

			xallibdsexist=.f.

**commented out until we get verification from MJ that this will work - eliminating all po/style combinations that 
**were on earlier trips... seemed to be more instances of this than actually caused problems on their end - 03/11/09
*!*				**if po/style has been loaded onto another trailer (regardless of orig_wo) that has already 
*!*				**  had an asn sent, ignore this record - mvw 3/10/09
*!*				**	- may need to compare only po (as opposed to po/style)??
*!*				xrecno=recno("asnwo")
*!*				select * from manifest ;
*!*					where str(accountid,4)+po_num=str(tmfst.accountid,4)+tmfst.po_num and ;
*!*					wo_date>tmfst.wo_date-30 and style=tmfst.style and wo_num#tmfst.wo_num ;
*!*					into cursor xtemp
*!*				xskip=.f.
*!*				scan
*!*					select asnwo
*!*					locate for wo_num=xtemp.wo_num
*!*					if found() and recno()<xrecno
*!*						xskip=.t.
*!*						exit
*!*					endif
*!*				endscan

*!*				goto xrecno in asnwo

*!*				use in xtemp

*!*				select tmfst
*!*				if xskip
*!*					skip
*!*					loop
*!*				endif
*!*				**end po/style skip


			xorigwo=orig_wo
			xhawb=hawb
			lcThisPO=po_num
			
			**need to create item recs for all recs in jonesdetail for suits - mvw 05/09/08
			select jonesdetail
			**if hawb is keyed in by a user, they may include a 4-digit prefix that will not be included in jonesdetail's data - mvw 05/20/08
	*		xhawbfilter='(hawb=xhawb or hawb="'+right(xhawb,len(xhawb)-4)+space(4)+'" or right(hawb,len(xhawb)-4) = "'+left(xhawb,len(xhawb)-4)+'")'
			xhawbfilter='hawb=xhawb'

			locate for ctrprfx+container=tmfst.container and (awb=left(tmfst.awb,13) or awb=dispawb(alltrim(tmfst.awb))) and po_num=tmfst.po_num and &xhawbfilter and ;
				(style=tmfst.style or sapstyle=tmfst.style)
			if !found()
				xhawbfilter='(hawb=xhawb or (ssco#"MAEU" and right(hawb,len(tmfst.hawb)-4)+space(4) = xhawb) or (ssco="MAEU" and hawb = right(xhawb,len(tmfst.hawb)-4)+space(4)))'
				locate for ctrprfx+container=tmfst.container and (awb=left(tmfst.awb,13) or awb=dispawb(alltrim(tmfst.awb))) and po_num=tmfst.po_num and &xhawbfilter and ;
					(style=tmfst.style or sapstyle=tmfst.style)
				if !found()
					select tmfst
					skip
					loop
				endif
			endif

			**changed to handle both prepack and non-prepack suits, both are wrapped up in the FMI system, 
			**  but need to explode to show all line items in the 856 - mvw 06/13/08
			**changed to also include ZPPK Prepacks as well - mvw 07/30/09
*			xsuit = qty_type='GOH' and "**materialtype: zsut"$lower(info) and "**prepack: true"$lower(info)
*			xsuit = qty_type='GOH' and "**materialtype: zsut"$lower(info)
			xsuit = ((qty_type='GOH' and "**materialtype: zsut"$lower(info)) or "**materialtype: zppk"$lower(info))

			**removed below loop and combined here - PREPACK and non-PREPACK has same code - mvw 07/30/09
*			if xsuit and !("**prepack: true"$lower(info))
			if xsuit
				select * from tmfst where orig_wo=xorigwo and hawb=xhawb and po_num=lcThisPO into cursor xtemp
				if reccount()>1
					**auto delete extra recs for suits - mvw 12/19/08
					xrecno=recno("tmfst")
					delete for orig_wo=xorigwo and hawb=xhawb and po_num=lcThisPO and manifestid#xtemp.manifestid in tmfst
					go xrecno in tmfst
					use in xtemp
				endif
				select jonesdetail
			endif

			**can remove once multiple prepack codes per po is accounted for - mvw 06/24/08
*!*				if xsuit and "**prepack: true"$lower(info)
*!*					select * from tmfst where orig_wo=xorigwo and hawb=xhawb and po_num=lcThisPO into cursor xtemp
*!*					if reccount()>1
*!*						**auto delete extra recs for suits - mvw 12/19/08
*!*						xrecno=recno("tmfst")
*!*						delete for orig_wo=xorigwo and hawb=xhawb and po_num=lcThisPO and manifestid#xtemp.manifestid in tmfst
*!*						go xrecno in tmfst
*!*						use in xtemp
*!*					endif
*!*					select jonesdetail
*!*				endif
			******
			xemptyfile=.f.

		    order_level(tmfst.po_num) &&no record pointer movement of either tmfst or jonesdetail

			select tmfst
			do while orig_wo=xorigwo and hawb=xhawb and po_num=lcThisPO
				cfilter='ctrprfx+container=tmfst.container and (awb=left(tmfst.awb,13) or awb=dispawb(alltrim(tmfst.awb))) and po_num=tmfst.po_num and '+xhawbfilter
				if !xsuit
					cfilter=cfilter+' and (style=tmfst.style or sapstyle=tmfst.style)'
				endif

				select jonesdetail
				scan for &cfilter
					item_level()
				endscan

				select tmfst
				skip
			enddo
		enddo

		do case
		case !xerror and xemptyfile &&no mfst recs had a match in jonesdetail
			if xallibdsexist
				xmsg="IBDs exist for all pos on this trip"
			else
				xmsg="No matching Jonesdetail.dbf records for any pos on this trip"
			endif
			xmsg=xmsg+"; no file will be sent for trip "+transform(lnWonum)+" ("+transform(datetime())+")"

			tsubject = "JAG 856 Creation Error at "+Ttoc(Datetime())
			tmessage = xmsg+chr(13)+chr(13)+"FILE WAS NOT CREATED!"

			tattach  = ""
			tcc=""
			tsendto  = "mwinter@fmiint.com"
			tFrom    ="TGF Corporate Communication Department <fmi-transload-ops@fmiint.com>"
			Do Form dartmail2 With tsendto,tFrom,tsubject,tcc,tattach,tmessage,"A"

			replace process with .f., runattempt with datetime(), info with info+chr(13)+xmsg+chr(13) in asnwo
			xerror=.t.

		case !xerror
			Store "CTT*"+Alltrim(Str(lnCTTCount,6,0))+segterminator To cstring
			xfilestr=xfilestr+cstring

			lcCtr = Alltrim(Str(totlincnt))
			lincnt = 1
			Store 0 To lnCTTCount
			lc856Ctr = Alltrim(Str(gn856Ctr))
			lcSegCtr = Alltrim(Str(segcnt+1))

			cstring ="SE*"+lcSegCtr+"*"+stNum+Padl(lc856Ctr,4,"0")+segterminator
			xfilestr=xfilestr+cstring

			**process the next work order in the table
			Select asnwo
			Replace asnwo.processed With .T. In asnwo
			Replace asnwo.Process   With .F. In asnwo
			Replace asnwo.date_proc  With Datetime() In asnwo
			summaryinfo = ;
			    "-----------------------------------------------------------------------------"+Chr(13)+;
			    '856 file name....'+"---> "+lc_cnum+Chr(13)+;
			    '   created.......'+Dtoc(Date())+Chr(13)+;
			    '   file.....: '+tfile+Chr(13)+;
			    '-----------------------------------------------------------------------------'+Chr(13)+;
			    'shipment level count.....'+Str(thisshipmentlevelcount)+Chr(13)+;
			    '   order level count.....'+Str(thisorderlevelcount)+Chr(13)+;
			    '    pack level count.....'+Str(thispacklevelcount)+Chr(13)+;
			    '    item level count.....'+Str(thisitemlevelcount)+Chr(13)+;
			    '-----------------------------------------------------------------------------'+Chr(13)
			Replace asnwo.Info With summaryinfo+workordersprocessed

			lc856Ctr = Alltrim(Str(gn856Ctr))
			Store "GE*"+lc856Ctr+"*"+stNum+segterminator To cstring
			xfilestr=xfilestr+cstring

			Store "IEA*1*"+lcInterctrlNum+segterminator To cstring
			xfilestr=xfilestr+cstring

			strtofile(xfilestr,tfile)

			xtriggerjag=.t.
		endcase
	endif &&shipment_level()

	store "" to workordersprocessed
	store 0 to thisshipmentlevelcount,thisorderlevelcount,thispacklevelcount,thisitemlevelcount
endscan

**create trigger record if file(s) created - mvw 12/20/10
if xtriggerjag
	insert into f:\edirouting\ftpjobs (jobname,userid,jobtime) values ("JAG-SAP-TOLOGNET","FTPPROCESS",datetime())
endif
if xtriggernwd
	insert into f:\edirouting\ftpjobs (jobname,userid,jobtime) values ("JAG-NWD-TOLOGNET","FTPPROCESS",datetime())
endif

set century off
set date to american

schedupdate()

return






********************************************************************************************************************
* P A C K     L E V E L
********************************************************************************************************************
Proc pack_level
	hlctr = hlctr +1
	packlevelcount = packlevelcount+1
	thispacklevelcount = thispacklevelcount+1
	currentpacklevelcount = hlctr
	lc_hlcount = Alltrim(getnum(hlctr))								&& overall HL counter
	lc_parentcount = Alltrim(getnum(currentitemlevelcount))				&& the parent

	Store "HL*"+lc_hlcount+"*"+lc_parentcount+"*"+"P"+segterminator To cstring
	xfilestr=xfilestr+cstring
	****************************************************************************************************************
	segcnt=segcnt+1
	hlsegcnt=hlsegcnt+1

	* count the line items for the CTT segment counter
	lnCTTCount = lnCTTCount +1

	If tlbcdata.bc1 = "?"
	  Store "SN1**00*EA"+segterminator To cstring
	Else
	  Store "SN1**"+Alltrim(tlbcdata.bc1)+"*EA"+segterminator To cstring
	Endif
	xfilestr=xfilestr+cstring
	segcnt=segcnt+1

	** MAN

	If tlbcdata.bc3 = "?"
	  cRet2Alias = Iif(emptynul(Alias()), "", "select "+Alias())
	  cOnError = On("error")
	  On Error Retry
	  Wait "Attempting to open bclog exclusively.... will retry automatically." Window Nowait Noclear
	  Select 0
	  Use F:\nautica\Data\bclog Excl
	  Wait Clear
	  On Error &cOnError

	  nSeqNum = seqNum
	  Replace seqNum With seqNum+1
	  Use In bcLog

	  &cRet2Alias
	  Store "MAN*GM*0000999999"+Padl(Alltrim(Str(nSeqNum,9,0)),9,"0")+"9"+segterminator To cstring
	Else
	  Store "MAN*GM*"+Alltrim(tlbcdata.bc3)+segterminator To cstring
	Endif

	xfilestr=xfilestr+cstring
	segcnt=segcnt+1
endproc &&pack_level


********************************************************************************************************************
* I T E M     L E V E L
********************************************************************************************************************
Proc item_level
	lincnt = lincnt + 1
	hlctr = hlctr +1
	itemlevelcount = itemlevelcount+1
	thisitemlevelcount = thisitemlevelcount+1
	currentitemlevelcount = hlctr
	lc_hlcount = Alltrim(getnum(hlctr)) 						&& overall HL counter
	lc_parentcount = Alltrim(getnum(currentorderlevelcount))	&& the parent

	Store "HL*"+lc_hlcount+"*"+lc_parentcount+"*I"+segterminator To cstring
	xfilestr=xfilestr+cstring
	segcnt=segcnt+1
	hlsegcnt=hlsegcnt+1


	**send back sapstyle (as opposed to material number which is held in style field) - mvw 05/07/08
*	Store "LIN**IN*"+Alltrim(tmfst.style)+segterminator To cstring
	Store "LIN**IN*"+Alltrim(iif(emptynul(jonesdetail.sapstyle),tmfst.style,jonesdetail.sapstyle))+segterminator To cstring

	xfilestr=xfilestr+cstring
	segcnt=segcnt+1

	**changed to reflect the p/l qty as opposed to ship qty - mvw 05/06/08
	*Store "SN1**"+Transform(tmfst.units)+"*EA*"+transform(jonesdetail.units)+"*EA"+segterminator To cstring
	Store "SN1**"+transform(jonesdetail.units)+"*EA*"+transform(jonesdetail.units)+"*EA"+segterminator To cstring
	xfilestr=xfilestr+cstring
	segcnt=segcnt+1


	**added 2 segments here for item qty and weight, previously we had just sent total qtys & weights for a po - mvw 07/08/08
	Store "MEA*CT*SQ*"+iif(jonesdetail.qty_type="GOH","0",transform(jonesdetail.pl_qty))+"*CT"+segterminator To cstring
	xfilestr=xfilestr+cstring
	segcnt=segcnt+1

	Store "MEA*WT*G*"+transform(jonesdetail.totalkgs)+"*KG"+segterminator To cstring
	xfilestr=xfilestr+cstring
	segcnt=segcnt+1
	**end 07/08/08 updt

	lcdocrcpt = jonesdetail.docrcpt
	Store "REF*RV*"+Alltrim(lcdocrcpt)+segterminator to cstring
	xfilestr=xfilestr+cstring
	segcnt=segcnt+1

	Store "N1*SF*ORIGIN"+segterminator To cstring
	xfilestr=xfilestr+cstring
	segcnt=segcnt+1

	Store "N4****"+left(Alltrim(jonesdetail.origin),2)+"*UN*"+Alltrim(jonesdetail.origin)+segterminator To cstring
	xfilestr=xfilestr+cstring
	segcnt=segcnt+1

	Store "N1*ST*DESTINITION"+segterminator To cstring
	xfilestr=xfilestr+cstring
	segcnt=segcnt+1

	**changed to dest (was discharge) - swapped the 2 for reporting purposes - mvw 03/29/11
	xdest=iif(!emptynul(jonesdetail.dest),jonesdetail.dest,jonesdetail.discharge)
	Store "N4****US*UN*"+Alltrim(xdest)+segterminator To cstring
	xfilestr=xfilestr+cstring
	segcnt=segcnt+1
endproc &&item_level


********************************************************************************************************************
* O R D E R    L E V E L
********************************************************************************************************************
Proc order_level
Parameter lcDelloc

	hlctr = hlctr +1
	orderlevelcount = orderlevelcount+1
	thisorderlevelcount = thisorderlevelcount+1
	lc_hlcount = getnum(hlctr)								&& overall HL counter
	lc_parentcount = getnum(currentshipmentlevelcount)				&& the parent
	currentorderlevelcount = hlctr

	Store "HL*"+Alltrim(lc_hlcount)+"*"+Alltrim(lc_parentcount)+"*O"+segterminator To cstring
	xfilestr=xfilestr+cstring
	********************************************************************************************************************************
	segcnt=segcnt+1
	hlsegcnt=hlsegcnt+1

	** PRF
	**strip the "-1" if we add manually to separate goh/cartons on 1 hawb - mvw 04/30/12
	*Store "PRF*"+Alltrim(tmfst.po_num)+"****"+(Alltrim(jonesdetail.hawb)+segterminator To cstring
	Store "PRF*"+Alltrim(tmfst.po_num)+"****"+(Alltrim(strtran(jonesdetail.hawb,"-1","")))+segterminator To cstring
	xfilestr=xfilestr+cstring
	segcnt=segcnt+1

	**NEED TD1 SEGMENT HERE TOTAL CTNS/WEIGHT/CUBE FOR PO
	Do Case
	Case  tmfst.qty_type = "GOH"
		ctype="HRB"
	Case  tmfst.Type = "LOOSE"
		ctype="LSE"
	Otherwise
		ctype="CTN"
	Endcase

	**changed to reflect the p/l qty as opposed to ship qty - mvw 05/06/08
*	Store "TD1*"+ctype+"*"+Alltrim(Str(tmfst.quantity))+"****G*"+Alltrim(Str(tmfst.weight))+"*LB*"+segterminator To cstring
*	Store "TD1*"+ctype+"*"+Alltrim(Str(tmfst.pl_qty))+"****G*"+Alltrim(Str(tmfst.weight))+"*LB*"+segterminator To cstring
	**changed to get total pl_qty and weight for this po - mvw 05/07/08
	**need to get plweight from detail - and cannot simply sum all of detail for this po because if some do not ship (never received, etc)
	cselect=iif(emptynul(alias()), "", "select "+alias())
	xponum=tmfst.po_num
	xhawb=tmfst.hawb
	xorigwo=tmfst.orig_wo
	store 0 to xtotqty,xtotwt
	select sum(m.pl_qty) as totqty, sum(d.plweight) as totwt from tmfst m left join detail d on d.detailid=m.detailid ;
		where m.orig_wo=xorigwo and m.hawb=xhawb and m.po_num=xponum into cursor xtemp
	&cselect
	Store "TD1*"+ctype+"*"+Alltrim(Str(xtemp.totqty))+"****G*"+Alltrim(Str(xtemp.totwt))+"*LB*"+segterminator To cstring
	use in xtemp

	xfilestr=xfilestr+cstring
	segcnt=segcnt+1

	**record pointer in jonesdetail has been moved prior to call to this proc - mvw 05/20/08
	*=Seek(tmfst.container,"jonesdetail","ctr")
	If !eof("jonesdetail")
	  lcCode = jonesdetail.code
	  lcOrigin= jonesdetail.origin
	  lcDischarge= jonesdetail.discharge
	Else
	  lcCode = "UNK"
	  lcOrigin= "UNK"
	  lcDischarge= "UNK"
	EndIf

	If emptynul(lcCode)
	  lcCode = "UNKN"
	EndIf   

	Do Case
	Case  tmfst.Type = "O"
	  Store "TD3*CN*"+Substr(tmfst.CONTAINER,1,4)+"*"+Substr(tmfst.Container,5,10)+"*******"+Alltrim(lcCode)+segterminator To cstring
	Case  tmfst.Type = "A"
	  Store "REF*AW*"+alltrim(tmfst.awb)+segterminator To cstring
	Case  tmfst.Type = "D"
	  Store "TD3*CN*"+Substr(tmfst.container,1,4)+"*"+Substr(tmfst.Container,5)+"*******"+Alltrim(lcCode)+segterminator To cstring
	Endcase

	xfilestr=xfilestr+cstring
	segcnt=segcnt+1

	Set Date To ymd
	Set Cent On

	Set Date To Mdy
	Set Cent Off
endproc &&order_level


********************************************************************************************************************
* S H I P M E N T     L E V E L
********************************************************************************************************************
Proc shipment_level
	* reset all at each shipment level
	Store 0 To shipmentlevelcount
	Store 0 To orderlevelcount
	Store 0 To packlevelcount
	Store 0 To itemlevelcount
	Store 0 To hlctr

	shipmentlevelcount = shipmentlevelcount +1
	thisshipmentlevelcount = thisshipmentlevelcount +1
	hlctr = hlctr +1
	currentshipmentlevelcount = hlctr
	**currentshipmentlevelcount = shipmentlevelcount
	lc_hlcount = Alltrim(getnum(hlctr))
	Store "HL*"+lc_hlcount+"**S"+segterminator To cstring
	xfilestr=xfilestr+cstring
	********************************************************************************************************************
	segcnt=segcnt+1
	hlsegcnt=hlsegcnt+1

	**TD1 NEED TOTAL CTNS/WEIGHT/CUBE FOR TRIP

	** TD5
	&&&
	Store "TD5*B*2*FMIX*M*FMI INC."+segterminator To cstring
	xfilestr=xfilestr+cstring
	segcnt=segcnt+1

	** TD3
	&&&
	
	xsqlexec("select * from fxwolog where wo_num="+transform(lnwonum),"xfxwolog",,"fx")

	do case
	case reccount("xfxwolog")=0 and emptynul(tmfst.trailer)
		**if not found in express and emptynul(manifest.trailer), error out - mvw 02/14/11
		xerrormsg="Error in trip "+transform(lnWonum)+": Trailer number empty in manifest.dbf, file not created. Will retry file creation on next run."
		xmemomsg="**Trailer number empty in manifest.dbf, will retry**"+chr(13)
		xretrylater=.t.
		return .f.

**begin sending trips moved in ocean container, per Dan Lewis - mvw 05/22/13
*!*		case !found() and len(alltrim(tmfst.trailer))>6
*!*			**DO NOT send trips that have been loaded onto a ctr (vs a trailer), as per dan lewis - mvw 02/16/11
*!*			xerrormsg="" &&left blank so no error email is sent
*!*			xmemomsg="**Container used for transport (manifest.trailer), No file required**"+chr(13)
*!*			xretrylater=.f.
*!*			return .f.

	**we started sending trips moved in an ocean ctr but still do not want to send wos that are manifested on themselves per Dan L 
	**  (because those should be straight loads from pier to DC, no transload involved) - mvw 08/16/13
	case reccount("xfxwolog")=0 and tmfst.wo_num=tmfst.orig_wo
		xerrormsg="" &&left blank so no error email is sent
		xmemomsg="**WO was manifested on itself (wo_num = orig_wo), No file required**"+chr(13)
		xretrylater=.f.
		return .f.

	case reccount("xfxwolog")=0
		**if not found in express, use the manifest trailer number (FMIUNK causes issues for jones) - mvw 02/14/11
*		lcTrailerNumber= "FMIUNK"
		**remove dashes and middle spaces from trailer - mvw 08/8/13
		lcTrailerNumber=strtran(strtran(tmfst.trailer,"-")," ")

	case reccount("xfxwolog")#0 and emptynul(xfxwolog.trailer)
		xerrormsg="Error in trip "+transform(lnWonum)+": Trailer number empty in fxwolog.dbf, file not created. Will retry file creation on next run."
		xmemomsg="**Trailer number empty in fxwolog.dbf, will retry**"+chr(13)
		xretrylater=.t.
		return .f.
	otherwise
		lcTrailerNumber= xfxwolog.trailer
	Endcase

	**for trips transported in the original container, need to put ctr prefix in TD02 and ctr # in TD03. 
	**  Otherwise, TD02 = "FMIX" and trailer is placed in TD03 - mvw 05/09/13
	if len(alltrim(lctrailernumber))>6
		Store "TD3*CN*"+left(lctrailernumber,4)+"*"+Alltrim(substr(lctrailernumber,5,len(lctrailernumber)-4))+"*******P5GP"+segterminator To cstring
	else
		**changed to put actual manifest.carrier code in TD302 element if valid (!empty() and len()=4) per Dan L - mvw 08/02/13
		**changed back per Nithya, carrier code now goes in the custom "TRUCKER" field - mvw 08/12/13
		Store "TD3*CN*FMIX*"+Alltrim(lcTrailerNumber)+"*******P5GP"+segterminator To cstring
*!*			xcarrier=iif(emptynul(tmfst.carrier) or len(alltrim(tmfst.carrier))#4 or tmfst.carrier="FMI","FMIX",alltrim(tmfst.carrier))
*!*			Store "TD3*CN*"+xcarrier+"*"+Alltrim(lcTrailerNumber)+"*******P5GP"+segterminator To cstring
	endif
	xfilestr=xfilestr+cstring
	segcnt=segcnt+1

	**added 3 new custom fields (Carrier OBL, Trucker and Cntr Complete) per Dan Lewis - mvw 01/02/13
	Store "REF*ZZ*GDX OPT:CARRIER OBL*"+transform(lnWonum)+segterminator To cstring
	xfilestr=xfilestr+cstring
	segcnt=segcnt+1

	**changed to put actual manifest.carrier code if valid (!empty() and len()=4) per Dan L / Nithya - mvw 08/12/13
*!*		Store "REF*ZZ*GDX OPT:TRUCKER*FMI"+segterminator To cstring
	xcarrier=iif(emptynul(tmfst.carrier) or len(alltrim(tmfst.carrier))#4 or tmfst.carrier="FMI","FMI",alltrim(tmfst.carrier))
	Store "REF*ZZ*GDX OPT:TRUCKER*"+xcarrier+segterminator To cstring
	xfilestr=xfilestr+cstring
	segcnt=segcnt+1

	Store "REF*ZZ*GDX OPT:CNTR COMPLETE*YES"+segterminator To cstring
	xfilestr=xfilestr+cstring
	segcnt=segcnt+1
	**added - mvw 01/02/12


	**changed to send lnwonum as local trips would always get a 0 in this element - mvw 08/03/11
*	Store "REF*BM*"+Alltrim(Str(fxwolog.WO_NUM))+"*FMI TRIP NUMBER"+segterminator To cstring  && FMI TRIP NO
	Store "REF*BM*"+transform(lnwonum)+"*FMI TRIP NUMBER"+segterminator To cstring  && FMI TRIP NO

	xfilestr=xfilestr+cstring
	segcnt=segcnt+1

	Store "REF*WS*NONE*DC"+segterminator To cstring  && hardcoded as per direction from lognet
	xfilestr=xfilestr+cstring
	segcnt=segcnt+1

	Store "R4*R*UN*US LAX**US"+segterminator To cstring && hardcoded as per direction from lognet
	xfilestr=xfilestr+cstring
	segcnt=segcnt+1


	** DTM the date pickup appt date
	** get the date out of FX wolog
	* might need some work here on trips that are not express trips....................................................
	* as trucking WOs do not have del appt and pu appt

	Set Date To ymd
	Set Cent On

	xsqlexec("select * from fxwolog where wo_num="+transform(lnwonum),"xfxwolog",,"fx")

	store {} to ld_dateshipped, ld_delappt, ld_delivered
	if reccount("xfxwolog")#0
	  xsqlexec("select * from fxtrips where fxwologid="+transform(xfxwolog.fxwologid)+" and deadhead=0","xfxtrips",,"fx")
	  
	  goto bottom
	  ld_dateshipped = xfxtrips.pickedup
	  ld_delappt     = xfxtrips.delappt
	  ld_delivered   = xfxtrips.delivered
	Endif

	ld_dateshipped = iif(emptynul(ld_dateshipped),tmfst.stripped,ld_dateshipped)
	ld_delappt = iif(emptynul(ld_delappt),Date(),ld_delappt)
	ld_delivered = iif(emptynul(ld_delivered),Date(),ld_delivered)

	do case
	case tmfst.stripped>ld_dateshipped
	**just take the later date to pass lognet's checks - mvw 06/24/08
*!*			xerrormsg="Error in trip "+transform(tmfst.wo_num)+": Strip date (manifest.stripped) shows as after the delivered start date (fxtrips.pickedup), file not created."
*!*			return .f.
		ld_dateshipped=tmfst.stripped
	case ld_dateshipped>ld_delappt
	**just take the later date to pass lognet's checks - mvw 06/24/08
*!*			xerrormsg="Error in trip "+transform(tmfst.wo_num)+": Delivery start date (fxtrips.pickedup) shows as after the estimated delivery date (fxtrips.delappt), file not created."
*!*			return .f.
		ld_delappt=ld_dateshipped
	endcase

	Store "DTM*019*"+Alltrim(Strtran(Dtoc(tmfst.stripped),'/',''))+"***UN*STRIPPED"+segterminator To cstring
	xfilestr=xfilestr+cstring
	segcnt=segcnt+1
	
	Store "DTM*370*"+Alltrim(Strtran(Dtoc(ld_dateshipped),'/',''))+"***UN*DEL START"+segterminator To cstring
	xfilestr=xfilestr+cstring
	segcnt=segcnt+1

	Store "DTM*035*"+Alltrim(Strtran(Dtoc(ld_delappt),'/',''))+"***UN*EST DEL"+segterminator To cstring
	xfilestr=xfilestr+cstring
	segcnt=segcnt+1

	** N1--SHIP FROM
	Store "N1*SF*FMI INC.*ZZ*FMIX"+segterminator To cstring
	xfilestr=xfilestr+cstring
	segcnt=segcnt+1

	Store "N3*350 WESTMOUNT DRIVE"+segterminator To cstring
	xfilestr=xfilestr+cstring
	segcnt=segcnt+1

	Store "N4*SAN PEDRO*CA*90731"+segterminator To cstring
	xfilestr=xfilestr+cstring
	segcnt=segcnt+1

	** N1--SHIP TO
	&&&
	Store "N1*ST*JONES APPAREL GROUP*92*"+Alltrim(tmfst.delloc)+segterminator To cstring
	xfilestr=xfilestr+cstring
	segcnt=segcnt+1

	Select dellocs
	Locate For Alltrim(location) = Alltrim(tmfst.delloc)
	If Found()
	  lcAddr = iif(!emptynul(dellocs.address),dellocs.address,iif(!emptynul(dellocs.acct_name2),dellocs.acct_name2,"UNK"))
	  lcCity = dellocs.city
	  lcState = dellocs.state
	  lcZip = dellocs.zip
	Else
	  lcAddr = "UNK"
	  lcCity = "UNK"
	  lcState = "UNK"
	  lcZip  = "UNK"
	Endif

	Store "N3*"+Alltrim(lcAddr)+segterminator To cstring
	xfilestr=xfilestr+cstring
	segcnt=segcnt+1

	Store "N4*"+Alltrim(lcCity)+"*"+Alltrim(lcState)+"*"+Alltrim(lcZip)+segterminator To cstring
	xfilestr=xfilestr+cstring
	segcnt=segcnt+1

	Set Date To Mdy
	Set Cent Off

	Return
endproc &&shipment_level


********************************************************************************************************
Procedure shipmentheader
Parameters tshipid

	gn856Ctr = gn856Ctr +1
	Store "ST*856*"+stNum+Padl(Alltrim(Str(gn856Ctr)),4,"0")+segterminator To cstring
	xfilestr=xfilestr+cstring

	**changed to send trip number as the transaction id as per dan lewis (jones/lognet) - mvw 5/12/10
	tshipid = transform(lnWonum)
*!*		Select controlnum
*!*		If llEditUpdate = .T.
*!*		  lc_cnum = Padl(Alltrim(Str(controlnum.editbsn)),5,"0")
*!*		  Replace controlnum.editbsn With controlnum.editbsn + 1
*!*		Else
*!*		  lc_cnum = Padl(Alltrim(Str(controlnum.bsnseg)),5,"0")
*!*		  Replace controlnum.bsnseg With controlnum.bsnseg + 1
*!*		Endif

*!*		tshipid = Padr('FMILA'+Substr(lc_cnum,1,25),30)
	**end transaction id update - mvw 5/12/10

	If llEditUpdate = .T.
	  Store "BSN*05*"+Alltrim(tshipid)+"*"+d4+d1+d2+d3+"*"+j1+j2+"*0001*OR"+segterminator To cstring
	Else
	  Store "BSN*00*"+Alltrim(tshipid)+"*"+d4+d1+d2+d3+"*"+j1+j2+"*0001*OR"+segterminator To cstring
	Endif
	xfilestr=xfilestr+cstring

	totlincnt = 0
endproc &&shipmentheader


********************************************************************************************************
Procedure WriteHeader
	* get the next unique control number
	Select controlnum
	stNum = Alltrim(Str(controlnum.cntrl856))
	lc_856num =stNum
	Replace controlnum.cntrl856 With controlnum.cntrl856 + 1
	lnInterControlnum = controlnum.interctrl
	Replace controlnum.interctrl With controlnum.interctrl + 1
	lcInterctrlNum = Padl(Alltrim(Str(lnInterControlnum)),9,"0")

	*  write out the 214 header

	*Store "ISA*00*          *00*          *ZZ*FMIF           *12*"+tqualcode+"*"+tyr+tmon+tday+"*"+lcTime+"*U*00401*"+lcInterctrlNum +"*0*P*;"+">"+segterminator to cstring
	Store "ISA*00*          *00*          *ZZ*FMIF           *12*"+tqualcode+"*"+tyr+tmon+tday+"*"+lcTime+"*U*00401*"+lcInterctrlNum +"*0*P*;"+segterminator To cstring
	xfilestr=xfilestr+cstring

	Store "GS*SH*"+Alltrim(tsendid)+"*"+Alltrim(tsendcode)+"*"+twholeyr+tmon+tday+"*"+lcTime+"*"+stNum+"*X*004030"+segterminator To cstring
	xfilestr=xfilestr+cstring
Endproc


********************************************************************************************************
Function getnum
Param plcount
	fidnum = Iif(Between(plcount,1,9),Space(11)+Ltrim(Str(plcount)),;
		IIF(Between(plcount,10,99),Space(10)+Ltrim(Str(plcount)),;
		IIF(Between(plcount,100,999),Space(9)+Ltrim(Str(plcount)),;
		IIF(Between(plcount,1000,9999),Space(8)+Ltrim(Str(plcount)),;
		IIF(Between(plcount,10000,99999),Space(7)+Ltrim(Str(plcount))," ")))))
Return fidnum


********************************************************************************************************
Proc fputstring
Param filehandle,filedata
	Fwrite(filehandle,filedata)
Endproc


********************************************************************************************************
Proc CheckEmpty
Parameter lcDataValue
	Return Iif(emptynul(lcDataValue),"UNKNOWN",lcDataValue)
Endproc


********************************************************************************************************
Proc RevertTables
	Tablerevert(.T.,"controlnum")
	Tablerevert(.T.,"asnwo")
	Tablerevert(.T.,"a856info")
	Tablerevert(.T.,"bkdnWO")
Endproc
********************************************************************************************************
