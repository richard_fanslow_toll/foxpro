utilsetup("MJ_FIX_CORRUPT_DATA")
with _screen
	.autocenter = .t.
	.windowstate = 0
	.borderstyle = 1
	.width = 320
	.height = 210
	.top = 290
	.left = 110
	.closable = .t.
	.maxbutton = .t.
	.minbutton = .t.
	.scrollbars = 0
	.caption = "MJ_FIX_CORRUPT_DATA"
endwith
set exclusive off
set deleted on
set talk off
set safety off

useca("outship","wh",,,"wo_date>{"+dtoc(date()-2)+"}")

select * from outship ;
	where "�" $name or "�" $consignee or "�" $address or "�" $address2 or "�" $csz ;
	into cursor corruptouts
	
select corruptouts
if reccount() > 0
	export to "S:\MarcJacobsData\mj_fix_corrupt_data\mj_corrupt_data_before_" + ttoc(datetime(), 1)  type xls
	set step on
	select  outship
	replace name with strtran(name,"YOR�","YORK")  for "YOR�" $name
	replace name with strtran(name,"�","")  for "�" $name
	replace consignee with strtran(consignee,"YOR�","YORK")  for "YOR�" $consignee
	replace consignee with strtran(consignee,"�","")  for "�" $consignee
	replace address with strtran(address,"YOR�","YORK")  for "YOR�" $address
	replace address with strtran(address,"�","")  for "�" $address
	replace address2 with strtran(address2,"YOR�","YORK")  for "YOR�" $address2
	replace address2 with strtran(address2,"�","")  for "�" $address2
	replace csz with strtran(csz,"YOR�","YORK")  for "YOR�" $csz
	replace csz with strtran(csz,"�","")  for "�" $csz
		
	tu("outship")

	tsendto = "tmarg@fmiint.com"
	tattach = ""
	tcc =""
	tfrom ="Toll EDI System Operations <fmi-transload-ops@fmiint.com>"
	tmessage = "mj_fix_corrupt_data_"+ttoc(datetime())
	tsubject = "mj_fix_corrupt_data_executed"
	do form m:\dev\frm\dartmail2 with tsendto,tfrom,tsubject, tcc,tattach,tmessage  ,"A"
else
*!*	  tsendto = "tmarg@fmiint.com"
*!*	  tattach = ""
*!*	  tcc =""
*!*	  tFrom ="Toll EDI System Operations <fmi-transload-ops@fmiint.com>"
*!*	  tmessage = "NO_mj_corrupt_data_"+Ttoc(Datetime())
*!*	  tSubject = "NO_mj_corrupt_data_executed"
*!*	  Do Form m:\dev\frm\dartmail2 With tsendto,tFrom,tSubject, tcc,tattach,tmessage  ,"A"
endif

close data all
schedupdate()
_screen.caption=gscreencaption


********************************************************************************************************************



*!*	Select * From upcmast Where Inlist(accountid,6321,6322,6323) And Empty(upc) ;
*!*	  INTO Cursor TEMP
*!*	If Reccount() > 0
*!*	  Export To "S:\MarcJacobsData\missing_upc\missing_upc_before_" + Ttoc(Datetime(), 1)  Type Xls
*!*	  tsendto = "tmarg@fmiint.com"
*!*	  tattach = ""
*!*	  tcc =""
*!*	  tFrom ="Toll EDI System Operations <fmi-transload-ops@fmiint.com>"
*!*	  tmessage = "missing_upc_update_executed_"+Ttoc(Datetime())
*!*	  tSubject = "missing_upc_update_executed"
*!*	  Do Form m:\dev\frm\dartmail2 With tsendto,tFrom,tSubject, tcc,tattach,tmessage  ,"A"

*!*	Endif

*!*	Select TEMP
*!*	Scan
*!*	  alength = Alines(aupc,TEMP.Info,.T.,Chr(13))
*!*	  lcdiv = Alltrim(segmentget(@aupc,"ITEM",alength))
*!*	  Select upcmast
*!*	  Locate For upcmastid = TEMP.upcmastid
*!*	  If Found()
*!*	    Replace upc With lcdiv In upcmast
*!*	    UPDATE upcmast;
*!*	    set updateby = 'MISSUPC' , updatedt = DATETIME() WHERE upcmastid = TEMP.upcmastid
*!*	  Else
*!*	    =Messagebox("WTF",0,"UPC Updater")
*!*	  Endif

*!*	Endscan

*!*		  SELECT * FROM  F:\wh\upcmast.Dbf where updateby ='MISSUPC' AND !EMPTY(upc) ;
*!*		  AND TTOD(UPDATEDT) = TTOD(DATETIME()) INTO CURSOR afteru
*!*	SELECT afteru
*!*	 If Reccount() > 0
*!*	  export TO "S:\MarcJacobsData\missing_upc\\missing_upc_after_" + TTOC(DATETIME(), 1)  TYPE xls
*!*	      tsendto = "tmarg@fmiint.com"
*!*	      tattach = ""
*!*	      tcc =""
*!*	      tFrom ="Toll EDI System Operations <fmi-transload-ops@fmiint.com>"
*!*	      tmessage = "missing_upc_update_executed_"+Ttoc(Datetime())
*!*	      tSubject = "missing_upc_update_executed"
*!*	      Do Form m:\dev\frm\dartmail2 With tsendto,tFrom,tSubject, tcc,tattach,tmessage  ,"A"
*!*	ELSE
*!*	    tsendto = "tmarg@fmiint.com"
*!*	    tattach = ""
*!*	    tcc =""
*!*	    tFrom ="Toll EDI System Operations <fmi-transload-ops@fmiint.com>"
*!*	    tmessage = "NO_missing_upc_to_update_"+Ttoc(Datetime())
*!*	    tSubject = "NO_missing_upc_to_update"
*!*	    Do Form m:\dev\frm\dartmail2 With tsendto,tFrom,tSubject, tcc,tattach,tmessage  ,"A"
*!*	ENDIF
*!*	Wait Window At 10,10 "all done...." Timeout 2
*!*	Close Data All
*!*	schedupdate()
*!*	_Screen.Caption=gscreencaption
*!*	On Error

*!*	** end of program **

*!*	****************************
*!*	Procedure segmentget
*!*	****************************
*!*	Parameter thisarray,lcKey,nLength

*!*	For i = 1 To nLength
*!*	  If i > nLength
*!*	    Exit
*!*	  Endif
*!*	  lnEnd= At("*",thisarray[i])
*!*	  If lnEnd > 0 And lnEnd <= nLength
*!*	    lcThisKey =Trim(Substr(thisarray[i],1,lnEnd-1))
*!*	    If Occurs(lcKey,lcThisKey)>0
*!*	      Return Substr(thisarray[i],lnEnd+1)
*!*	      i = 1
*!*	    Endif
*!*	  Endif
*!*	Endfor
*!*	Return ""
