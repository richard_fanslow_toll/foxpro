lparameters xinadjchange, xwhseloc, xaccountid, xunits, xstyle, xcolor, xid, xpack

if xinadjchange>0    && qty was un-cut so add back removeqty
	xinadjchange = indetremove(xinadjchange, xaccountid, xunits, xstyle, xcolor, xid, xpack, xwhseloc)
	
	if xinadjchange>0
		adjremove(xinadjchange, xaccountid, xunits, xstyle, xcolor, xid, xpack, xwhseloc)
	endif
	
else     && qty was cut so decrease removeqty

	xinchange = -xinadjchange

	xinchange = indetbackin(xinchange, xaccountid, xunits, xstyle, xcolor, xid, xpack, xwhseloc)
	
	if xinchange>0
		adjbackin(xinadjchange, xaccountid, xunits, xstyle, xcolor, xid, xpack, xwhseloc)
	endif
endif