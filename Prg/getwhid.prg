lparameters xxtable, xfrom

if empty(xfrom)
	xfrom=""
endif

if xfrom="AUTO"
	if !used("genpk"+xoffice)
		use ("f:\wh"+xoffice+"\whdata\whgenpk") alias ("genpk"+xoffice) in 0 again
	endif
	select ("genpk"+xoffice)
	locate for gpk_pk=upper(xxtable)
	replace gpk_currentnumber with gpk_currentnumber+1
	return gpk_currentnumber
else
	return genpk(xxtable,"wh")
endif
