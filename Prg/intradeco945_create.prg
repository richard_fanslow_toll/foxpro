*!* Intradeco 945 (Whse. Shipping Advice) Creation PROGRAM
*!* Creation Date: 08.01.2012 by Joe (Derived from YellSteel945_create program)

PARAMETERS cOffice,cBOL,nAcctNum
WAIT WINDOW "Now at start of Intradeco 945 process" TIMEOUT 5

PUBLIC ARRAY thisarray(1)
PUBLIC c_CntrlNum,c_GrpCntrlNum,cReturned,cWO_Num,cWO_NumStr,nWO_Num1,lTesting,tsendtotest,tcctect,lTestinput,lPick,lPrepack
PUBLIC lEmail,dDateTimeCal,nFilenum,nWO_Xcheck,nOutshipid,cCharge,lDoWM,cProgname,ccustname,cCheckShip_ref,lOverflow
PUBLIC cCity,cState,cZip,nOrigSeq,nOrigGrpSeq,tsendto,tcc,tsendtoerr,tccerr,lFirstLoop,lJCPenney,cErrMsg,lDoIntraFilesOut,tfrom
PUBLIC cMod,cMBOL,cFilenameShort,cFilenameOut,cFilenameArch,lParcelType,cWO_NumList,lcPath,cEDIType,cISA_Num

lTesting   = .F.  && Set to .t. for testing
lTestinput = .F.  &&  Set to .t. for test input files only!

lEmail = .T.
lTestMail = lTesting && Sends mail to Joe only
lDoIntraFilesOut = !lTesting && If true, copies output to FTP folder (default = .t.)
lStandalone = lTesting

DO m:\dev\prg\_setvars WITH lTesting
IF lTesting
	CLOSE DATABASES ALL
	SET STATUS BAR ON
	_SCREEN.WINDOWSTATE=2
ENDIF

cProgname = "intradeco945_create"

lPick = .T.
lPrepack = .F.
lIsError = .F.
lDoCatch = .T.
cShip_ref = ""
cErrMsg = "TOP LEVEL"
lCloseOutput = .T.
nFilenum = 0
cCharge = "0.00"
ccustname = ""
cWO_NumStr = ""
cWO_NumList = ""
cString = ""
nPTCtnTot = 0
cShipMethodPymt = "PP"  && "prepaid", default.
lFirstLoop = .T.
cCheckShip_ref = '0080686705'
lOverflow = .f.

TRY
	nOrigSeq = 0
	nOrigGrpSeq = 0
	cPPName = "Intradeco"
	ASSERT .F. MESSAGE "At the start of "+cPPName+" 945 process..."
	tfrom = "Toll/FMI EDI Operations <toll-edi-ops@tollgroup.com>"
	tmessage=""
	tcc = ""
	lDoBOLGEN = .F.
	lSamples = .F.
	cBOL1 = ""
	lISAFlag = .T.
	lSTFlag = .T.
	nLoadid = 1
	lSQLMail = .F.
*	cOffice = "I"

	IF lTesting OR lTestinput
		CLOSE DATABASES ALL
		DO m:\dev\prg\lookups
		SELECT 0
		IF !USED('edi_trigger')
			cEDIFolder = "F:\3PL\DATA\"
			USE (cEDIFolder+"edi_trigger") IN 0 ALIAS edi_trigger
		ENDIF
		nAcctNum = 5154
		cOffice = "M"
		cBOL = "04331725154922750"
		lFedEx = .F.
	ENDIF

	cBOL=TRIM(cBOL)
	cMBOL = ""
	cMod = IIF(cOffice = "C","5",cOffice)
	gOffice = cMod
	gMasterOffice = cOffice
	cEDIType = "945"
	lParcelType = .F.

	CREATE CURSOR temp945 (accountid i,isa_num c(10),st_num c(10),wo_num i,bol_no c(20),ship_ref c(20),filename c(50),filedate T)

	DO m:\dev\prg\swc_cutctns_gen WITH cBOL

*!* lTestMail = .T.
*!*	lDoIntraFilesOut = .F.

	STORE "LB" TO cWeightUnit
	cfd = "*"
	csegd = "~"
	nSegCtr = 0
	cterminator = ">" && Used at end of ISA segment

	WAIT WINDOW "Now preparing tables and variables" NOWAIT NOCLEAR

*!* SET CUSTOMER CONSTANTS
	ccustname = IIF(nAcctNum = 6237,"IVORY","INTRADECO")  && Customer Identifier
	cX12 = "004030"  && X12 Standards Set used
	cMailName = "Intradeco"
	SELECT 0
	USE F:\3pl\DATA\mailmaster ALIAS mm
	LOCATE FOR mm.edi_type = '945' AND mm.office = cOffice AND mm.accountid = nAcctNum
	tsendto = IIF(mm.use_alt,mm.sendtoalt,mm.sendto)
	tcc = IIF(mm.use_alt,mm.ccalt,mm.cc)
	lcOutPath = ALLTRIM(mm.basepath)
	lcArchPath = ALLTRIM(mm.archpath)
	lcHoldPath = ALLTRIM(mm.holdpath)

	LOCATE
	LOCATE FOR mm.edi_type = 'MISC' AND mm.taskname = "GENERAL"
	tsendtotest = IIF(mm.use_alt,mm.sendtoalt,mm.sendto)
	tcctest = IIF(mm.use_alt,mm.ccalt,mm.cc)
	tsendtoerr = IIF(mm.use_alt,mm.sendtoalt,mm.sendto)
	tccerr = IIF(mm.use_alt,mm.ccalt,mm.cc)
	USE IN mm

*!* SET OTHER CONSTANTS
	cSF_Name = "FMI INTERNATIONAL"
	DO CASE
		CASE cOffice = "I"
			cCustLoc =  "NJ"
			cFMIWarehouse = ""
			cCustPrefix = "945j"
			cDivision = "New Jersey"
			cSF_Addr1  = "800 FEDERAL BLVD"
			cSF_CSZ    =  "CARTERET"+cfd+"NJ"+cfd+"07008"

		CASE cOffice = "X"
			cCustLoc =  "CR"
			cFMIWarehouse = ""
			cCustPrefix = "945r"
			cDivision = "Carson"
			cSF_Addr1  = "2000 E CARSON ST"
			cSF_CSZ    = "CARSON"+cfd+"CA"+cfd+"90745"

		CASE cOffice = "M"
			cCustLoc =  "FL"
			cFMIWarehouse = ""
			cCustPrefix = "945f"
			cDivision = "Florida"
			cSF_Addr1  = "11400 NW 32ND AVE"
			cSF_CSZ    = "MIAMI"+cfd+"FL"+cfd+"33167"

		OTHERWISE
			cCustLoc =  "SP"
			cFMIWarehouse = ""
			cCustPrefix = "945c"
			cDivision = "San Pedro"
			cSF_Addr1  = "450 WESTMONT DRIVE"
			cSF_CSZ    = "SAN PEDRO"+cfd+"CA"+cfd+"90731"
	ENDCASE
	IF lTesting
		cCustPrefix = "945t"
	ENDIF

*	cCustOutFolder = "INTRADECO-FL"
	cCustFolder = UPPER(ccustname)+"-"+cCustLoc

	STORE "" TO lcKey
	STORE 0 TO alength,nLength
	cTime = TIME()
	cDelTime = STRTRAN(LEFT(cTime,5),":","")
	cCarrierType = "M" && Default as "Motor Freight"
	dapptnum = ""

*!* SET OTHER CONSTANTS
	IF INLIST(cOffice,'C','1','2','X')
		dDateTimeCal = (DATETIME()-(3*3600))
	ELSE
		dDateTimeCal = DATETIME()
	ENDIF
	dt1 = TTOC(dDateTimeCal,1)
	dtmail = TTOC(dDateTimeCal)
	dt2 = dDateTimeCal
	cString = ""

	csendqual = "ZZ"
	csendid = "TOLLLOGISTICS"
	crecqual = "12"
	crecid = "3052648888"

	cdate = DTOS(DATE())
	cTruncDate = RIGHT(cdate,6)
	cTruncTime = cDelTime
	cfiledate = cdate+cTruncTime
	cOrig = "J"
	nSTCount = 0
	cStyle = ""
	cPTString = ""
	nTotCtnWt = 0
	nTotCtnCount = 0
	nUnitSum = 0
	cTargetStyle = ""

** PAD ID Codes
	csendidlong = PADR(csendid,15," ")
	crecidlong = PADR(crecid,15," ")

	lcPath = lcOutPath
	cFilenameHold = (lcHoldPath+cCustPrefix+dt1+".txt")
	cFilenameShort = JUSTFNAME(cFilenameHold)
	cFilenameOut = (lcOutPath+cFilenameShort)
	cFilenameArch = (lcArchPath+cFilenameShort)
	nFilenum = FCREATE(cFilenameHold)

	IF USED('OUTWOLOG')
		USE IN outwolog
	ENDIF

	IF USED('OUTSHIP')
		USE IN outship
	ENDIF

	csq1 = [select * from outship where accountid = ]+TRANSFORM(nAcctNum)+[ and office = ']+cOffice+[' and bol_no = ']+cBOL+[']
	xsqlexec(csq1,,,"wh")
	IF RECCOUNT() = 0
		cErrMsg = "MISS BOL "+cBOL
		DO ediupdate WITH cErrMsg,.T.
		THROW
	ENDIF
	INDEX ON bol_no TAG bol_no
	LOCATE
	nWO_Num = outship.wo_num

	csq1 = [select * from outwolog where accountid = ]+TRANSFORM(nAcctNum)+[ and wo_num = ]+TRANSFORM(nWO_Num)
	xsqlexec(csq1,,,"wh")
	IF RECCOUNT() = 0
		cErrMsg = "MISS OUTWOLOG REC FOR WO# "+cWO_Num
		DO ediupdate WITH cErrMsg,.T.
		THROW
	ENDIF
	SELECT outwolog
	INDEX ON wo_num TAG wo_num
	IF SEEK(nWO_Num,"outwolog","wo_num")
		lPick = outwolog.picknpack
		lPrepack = IIF(lPick,.F.,.T.)
	ENDIF
	USE IN outwolog

	SELECT outshipid ;
		FROM outship ;
		WHERE bol_no = cBOL ;
		AND accountid = nAcctNum ;
		INTO CURSOR tempsr
	SELECT tempsr
	WAIT WINDOW "There are "+ALLTRIM(STR(RECCOUNT()))+" outshipid's to check" NOWAIT

	SELECT outship
	IF !SEEK(cBOL,'outship','bol_no')
		SET STEP ON
		WAIT WINDOW "BOL not found in OUTSHIP" TIMEOUT 2
		cErrMsg = "BOL NOT FOUND"
		DO ediupdate WITH cErrMsg,.T.
		THROW
	ENDIF

	IF USED('OUTDET')
		USE IN outdet
	ENDIF

	selectoutdet()
	SELECT outdet
	INDEX ON outdetid TAG outdetid

	IF !USED("scacs")
		xsqlexec("select * from scac","scacs",,"wh")
		INDEX ON scac TAG scac
	ENDIF

	IF !lTestinput
		IF USED('SHIPMENT')
			USE IN shipment
		ENDIF

		gOffice=cOffice

		dDate = DATE()-25
		csq1 = [select * from shipment where right(rtrim(str(accountid)),4) in (]+gintradecoaccts+[) and shipdate >= {]+DTOC(dDate)+[}]
		xsqlexec(csq1,,,"wh")
		INDEX ON shipmentid TAG shipmentid
		INDEX ON pickticket TAG pickticket

		IF USED('PACKAGE')
			USE IN package
		ENDIF

		SELECT shipment
		LOCATE
		IF RECCOUNT("shipment") > 0
			xjfilter="shipmentid in ("
			SCAN
				nShipmentId = shipment.shipmentid
				xsqlexec("select * from package where shipmentid="+TRANSFORM(nShipmentId),,,"wh")

				IF RECCOUNT("package")>0
					xjfilter=xjfilter+TRANSFORM(shipmentid)+","
				ELSE
					xjfilter="1=0"
				ENDIF
			ENDSCAN
			xjfilter=LEFT(xjfilter,LEN(xjfilter)-1)+")"

			xsqlexec("select * from package where "+xjfilter,,,"wh")
		ELSE
			xsqlexec("select * from package where .f.",,,"wh")
		ENDIF
		SELECT package
		INDEX ON shipmentid TAG shipmentid
		SET ORDER TO TAG shipmentid

		SELECT shipment
		SET RELATION TO shipmentid INTO package
	ENDIF

&& Added code blocks to the following to overcome BOL vs. WO selection issues, 12.23.2005
	cSQL="tgfnjsql01"
	
	SELECT outship
	LOCATE
	IF USED("sqlwo")
		USE IN sqlwo
	ENDIF
	IF FILE("F:\3pl\DATA\sqlwo.dbf")
		DELETE FILE "F:\3pl\DATA\sqlwo.dbf"
	ENDIF
	SELECT bol_no,wo_num ;
		FROM outship ;
		WHERE bol_no == cBOL ;
		AND accountid = nAcctNum ;
		GROUP BY 1,2 ;
		ORDER BY 1,2 ;
		INTO DBF F:\3pl\DATA\sqlwo
	USE IN sqlwo
	SELECT 0
	USE F:\3pl\DATA\sqlwo ALIAS sqlwo
	IF lTesting
*		BROWSE
	ENDIF

	ASSERT .F. MESSAGE "At SQL CONNECT"
	cRetMsg = ""
	DO m:\dev\prg\sqlconnect_bol  WITH nAcctNum,ccustname,cPPName,.T.,cOffice,.T.
	IF cRetMsg<>"OK"
		cErrMsg = cRetMsg
		DO ediupdate WITH cRetMsg,.T.
		THROW
	ENDIF
	SELECT vintradecopp
	LOCATE
	IF lTesting
*!*			BROWSE
*!*			ASSERT .F.
	ENDIF
	IF RECCOUNT()=0
		WAIT WINDOW "SQL select data is EMPTY...error!" TIMEOUT 2
		cErrMsg = "NO SQL DATA"
		DO ediupdate WITH cErrMsg,.T.
		THROW
	ENDIF

	SELECT outship
	LOCATE
	WAIT CLEAR
	WAIT WINDOW "Now creating Header information" NOWAIT NOCLEAR

*!* HEADER LEVEL EDI DATA
	DO num_incr_isa

	cISA_Num = PADL(c_CntrlNum,9,"0")
	nISA_Num = INT(VAL(cISA_Num))

	IF lTesting
		cISACode = "T"
	ELSE
		cISACode = "P"
	ENDIF

	STORE "ISA"+cfd+"00"+cfd+"          "+cfd+"00"+cfd+"          "+cfd+csendqual+cfd+csendidlong+cfd+;
		crecqual+cfd+crecidlong+cfd+cTruncDate+cfd+cTruncTime+cfd+"U"+cfd+"00401"+cfd+;
		cISA_Num+cfd+"0"+cfd+cISACode+cfd+cterminator+csegd TO cString
	DO cstringbreak

	STORE "GS"+cfd+"SW"+cfd+csendid+cfd+crecid+cfd+cdate+cfd+cTruncTime+cfd+c_CntrlNum+;
		cfd+"X"+cfd+cX12+csegd TO cString
	DO cstringbreak

	SELECT outship
	LOCATE

	IF !lTesting
		IF EMPTY(cBOL)  && May be empty if triggered from PT screen, or FedEx shipment
			WAIT CLEAR
			WAIT WINDOW "MISSING Bill of Lading Number!" TIMEOUT 2
			cErrMsg = "BOL# EMPTY"
			DO ediupdate WITH cErrMsg,.T.
			THROW
		ELSE
			IF !SEEK(PADR(TRIM(cBOL),20),"outship","bol_no")
				WAIT CLEAR
				WAIT WINDOW "Invalid Bill of Lading Number - Not Found!" TIMEOUT 2
				cErrMsg = "BOL# NOT FOUND"
				DO ediupdate WITH cErrMsg,.T.
				THROW
			ENDIF
		ENDIF
	ENDIF

*************************************************************************
*1	PTIDSOURCE/OUTSHIP RECORD MAIN LOOP
*	Loops through individual OUTSHIP lines for BOL#
*************************************************************************
	WAIT CLEAR
	WAIT WINDOW "Now creating BOL#-based information..." NOWAIT NOCLEAR

	SELECT outship
	SET ORDER TO
	IF DATETIME() < DATETIME(2012,04,24,20,20,00)
		COUNT TO N FOR outship.bol_no = cBOL
	ELSE
		COUNT TO N FOR outship.bol_no = cBOL AND !EMPTYnul(del_date)
	ENDIF

	IF N=0
		cErrMsg = "INCOMPLETE BOL"
		DO ediupdate WITH cErrMsg,.T.
		THROW
	ENDIF

	LOCATE
	cMissDel = ""

	IF DATETIME() < DATETIME(2012,04,24,20,20,00)
		oscanstr = "outship.accountid = nAcctNum and outship.bol_no = cBOL"
	ELSE
		oscanstr = "outship.accountid = nAcctNum and outship.bol_no = cBOL AND !EMPTYnul(outship.del_date)"
	ENDIF

	SELECT outship
	SET DELETED ON
*	SET STEP ON
	SCAN FOR &oscanstr
		SCATTER MEMVAR MEMO
		IF "OV"$m.ship_ref
			LOOP
		ENDIF
		cShip_ref = ALLTRIM(outship.ship_ref)
		IF cShip_ref = cCheckShip_ref
			SET DELETED OFF
		ELSE
			SET DELETED ON
		ENDIF
		cPO_Num = ALLTRIM(outship.cnee_ref)
		nWO_Num = outship.wo_num
		cWO_Num = TRANSFORM(nWO_Num)
		nCtnNumber = 1  && Seed carton sequence count

		IF VARTYPE(nWO_Num) # "N"
			lCloseOutput = .T.
			cErrMsg = "NON-NUMERIC WO"
			DO ediupdate WITH cErrMsg,.T.
			THROW
		ENDIF

		DO num_incr_st
		STORE "ST"+cfd+"945"+cfd+PADL(c_GrpCntrlNum,9,"0")+csegd TO cString
		DO cstringbreak
		nSTCount = nSTCount + 1
		nSegCtr = 1

		STORE "W06"+cfd+"N"+cfd+cShip_ref+cfd+cdate+cfd+TRIM(cBOL)+cfd+TRIM(cWO_Num)+cfd+cPO_Num+csegd TO cString
		DO cstringbreak
		nSegCtr = nSegCtr + 1

		IF USED('parcel_carriers')
			USE IN parcel_carriers
		ENDIF
		USE F:\3pl\DATA\parcel_carriers IN 0 ALIAS parcel_carriers
		lParcelType = IIF(SEEK(m.scac,"parcel_carriers","scac"),.T.,.F.)
		USE IN parcel_carriers
		SELECT outship
		lParcelType = IIF(outship.keyrec = cBOL,.F.,lParcelType)

		lJCPenney = IIF("PENNEY"$outship.consignee,.T.,.F.)
		alength = ALINES(apt,outship.shipins,.T.,CHR(13))
		nOutshipid = m.outshipid

		cShip_ref = ALLTRIM(m.ship_ref)
		cPTString = IIF(EMPTY(cPTString),m.consignee+" "+cShip_ref,cPTString+CHR(13)+m.consignee+" "+cShip_ref)
		IF !(cWO_Num$cWO_NumList)
			cWO_NumList = IIF(EMPTY(cWO_NumList),cWO_Num,cWO_NumList+CHR(13)+cWO_Num)
		ENDIF
		IF !lParcelType
			cTrackNum = ALLT(m.keyrec)  && ProNum if available
			cCharge = "0.00"
			IF !(cWO_Num$cWO_NumStr)
				cWO_NumStr = IIF(EMPTY(cWO_NumStr),cWO_Num,cWO_NumStr+","+cWO_Num)
			ENDIF
		ELSE
			cTrackNum = ALLT(outship.bol_no)  && UPS Tracking Number
			cWO_NumStr = IIF(EMPTY(cWO_NumStr),cTrackNum,cWO_NumStr+","+cTrackNum)
			nCharge = 0
			SELECT shipment
			LOCATE FOR shipment.accountid = nAcctNum AND shipment.pickticket = PADR(cShip_ref,20)
			IF !FOUND()
				LOCATE FOR shipment.accountid = 9999 AND shipment.pickticket = PADR(cShip_ref,20)
				IF !FOUND()
					cErrMsg = "MISS UPS REC "+cShip_ref
					DO ediupdate WITH cErrMsg,.T.
					THROW
				ELSE
					lSkipCharges = IIF(INLIST(shipment.billing,"COL","STP","B3P","C/B","BRC"),.T.,.F.)
					SELECT package
					LOCATE
					SUM pkgcharge TO nCharge FOR package.shipmentid = shipment.shipmentid
					cCharge = ALLT(STR(nCharge,8,2))
				ENDIF
			ELSE
				lSkipCharges = IIF(INLIST(shipment.billing,"COL","STP","B3P","C/B","BRC"),.T.,.F.)
				SELECT package
				LOCATE
				SUM pkgcharge TO nCharge FOR package.shipmentid = shipment.shipmentid
				cCharge = ALLT(STR(nCharge,8,2))
			ENDIF
			IF nCharge = 0 AND !INLIST(m.scac,"UPSL","UPCG","FDXG","FGC","FEG") AND !lSkipCharges
				IF lTesting
					cCharge = "25.50"
				ELSE
					cErrMsg = "UPS-EMPTY CHARGE"
					ASSERT .F. MESSAGE cErrMsg
					DO ediupdate WITH cErrMsg,.T.
					THROW
				ENDIF
			ENDIF
		ENDIF

		nTotCtnCount = m.qty
		cPO_Num = ALLTRIM(m.cnee_ref)
		cShip_ref = ALLTRIM(m.ship_ref)

*!* Added this code to trap miscounts in OUTDET Units
		IF lPrepack
			SELECT outdet
			SET ORDER TO
*			SET STEP ON
			IF SET("Deleted")="ON"
				SUM totqty TO nCtnTot1 FOR !units AND outdet.outshipid = outship.outshipid
			ELSE
				SUM origqty TO nCtnTot1 FOR !units AND outdet.outshipid = outship.outshipid
			ENDIF

*!*	Check carton count
*			ASSERT .F. MESSAGE "In outdet/SQL tot check"
			SELECT distinct ucc ;
				FROM vintradecopp ;
				WHERE vintradecopp.outshipid = outship.outshipid ;
				AND vintradecopp.totqty > 0 ;
				INTO CURSOR tempsqlx
			SELECT tempsqlx
			SET STEP ON 
			nCtnTot2 = RECCOUNT()
			USE IN tempsqlx
			IF nCtnTot1<>nCtnTot2
				cErrMsg = "SQL CTNQTY ERR, OSID: "+TRANSFORM(outship.outshipid)
				SET STEP ON
				DO ediupdate WITH cErrMsg,.T.
				THROW
			ENDIF
		ELSE
			SELECT outdet
			SUM totqty TO nUnitTot1 FOR outdet.units=.T. AND outdet.outshipid = outship.outshipid

			SELECT vintradecopp
			SUM totqty TO nUnitTot2 FOR vintradecopp.outshipid = outship.outshipid
			IF nUnitTot1<>nUnitTot2
				cErrMsg = "SQL UNITQTY ERR, OSID: "+TRANSFORM(outship.outshipid)
				ASSERT .F. MESSAGE cErrMsg
				SET STEP ON
				DO ediupdate WITH cErrMsg,.T.
				THROW
			ENDIF
		ENDIF

		SELECT outdet
		SET ORDER TO outdetid
		LOCATE

*!* End code addition

		IF (("PENNEY"$UPPER(outship.consignee)) OR ("KMART"$UPPER(outship.consignee)) OR ("K-MART"$UPPER(outship.consignee)))
			lApptFlag = .T.
		ELSE
			lApptFlag = .F.
		ENDIF

		IF DATETIME() < DATETIME(2012,04,24,20,20,00)
			ddel_date = DATE()+1
		ELSE
			ddel_date = outship.del_date
		ENDIF

		IF lTestinput
			IF EMPTY(ddel_date)
				ddel_date = DATE()
			ENDIF
			dapptnum = "99999"
			dapptdate = DATE()
		ELSE
			IF EMPTY(ddel_date)
				cMissDel = IIF(EMPTY(cMissDel),"The following PTs had no Delivery Dates:"+CHR(13)+TRIM(cShip_ref),cMissDel+CHR(13)+TRIM(cShip_ref))
			ENDIF
			dapptnum = ALLTRIM(outship.appt_num)

			IF EMPTY(dapptnum) AND !lParcelType && Penney/KMart Appt Number check
				IF !(lApptFlag)
					dapptnum = ""
				ELSE
					cErrMsg = "EMPTY APPT #"
					DO ediupdate WITH cErrMsg,.T.
					THROW
				ENDIF
			ENDIF
			dapptdate = outship.appt

			IF EMPTY(dapptdate)
				dapptdate = outship.del_date
			ENDIF
		ENDIF

		IF !(cWO_Num$cWO_NumStr)
			cWO_NumStr = IIF(EMPTY(cWO_NumStr),cWO_Num,cWO_NumStr+","+cWO_Num)
		ENDIF

		IF ALLTRIM(outship.SForCSZ) = ","
			BLANK FIELDS outship.SForCSZ NEXT 1 IN outship
		ENDIF

		cTRNum = ""
		cPRONum = ""

		IF lFirstLoop
			m.CSZ = TRIM(m.CSZ)
			IF EMPTY(ALLT(STRTRAN(M.CSZ,",","")))
				WAIT CLEAR
				WAIT WINDOW "No SHIP-TO City/State/ZIP info...exiting" TIMEOUT 2
				cErrMsg = "NO CSZ INFO"
				DO ediupdate WITH cErrMsg,.T.
				THROW
			ENDIF
			IF !(", "$m.CSZ)
				m.CSZ = ALLTRIM(STRTRAN(m.CSZ,",",", "))
			ENDIF
			m.CSZ = ALLTRIM(STRTRAN(m.CSZ,"  "," "))
			cCity = ALLT(LEFT(TRIM(m.CSZ),AT(",",m.CSZ)-1))
			cStateZip = ALLT(SUBSTR(TRIM(m.CSZ),AT(",",m.CSZ)+1))
			cState = ALLT(LEFT(cStateZip,2))
			cZip = ALLT(SUBSTR(cStateZip,3))

			STORE "" TO cSForCity,cSForState,cSForZip
			m.SForCSZ = ALLTRIM(STRTRAN(m.SForCSZ,"  "," "))
			nSpaces = OCCURS(" ",ALLTRIM(m.SForCSZ))
			IF nSpaces = 0
				m.SForCSZ = STRTRAN(m.SForCSZ,",","")
				cSForCity = ALLTRIM(m.SForCSZ)
				cSForState = ""
				cSForZip = ""
			ELSE
				nCommaPos = AT(",",m.SForCSZ)
				nLastSpace = AT(" ",m.SForCSZ,nSpaces)
				nMinusSpaces = IIF(nSpaces=1,0,1)
				IF ISALPHA(SUBSTR(TRIM(m.SForCSZ),AT(" ",TRIM(m.SForCSZ),nSpaces-nMinusSpaces)+1,2))
					cSForCity = LEFT(TRIM(m.SForCSZ),AT(",",m.SForCSZ)-1)
					cSForState = SUBSTR(m.SForCSZ,nCommaPos+2,2)
					cSForZip = TRIM(RIGHT(TRIM(m.SForCSZ),5))
					IF ISALPHA(cSForZip)
						cSForZip = ""
					ENDIF
				ELSE
					WAIT CLEAR
					WAIT WINDOW "NOT ALPHA: "+SUBSTR(TRIM(m.SForCSZ),AT(" ",m.SForCSZ,nSpaces-1)+1,2) TIMEOUT 3
					cSForCity = LEFT(TRIM(m.SForCSZ),AT(",",m.SForCSZ)-1)
					cSForState = SUBSTR(TRIM(m.SForCSZ),AT(" ",m.SForCSZ,nSpaces-2)+1,2)
					cSForZip = TRIM(RIGHT(TRIM(m.SForCSZ),5))
					IF ISALPHA(cSForZip)
						cSForZip = ""
					ENDIF
				ENDIF
			ENDIF

			WAIT CLEAR
			WAIT WINDOW "Now creating Line Item information" NOWAIT NOCLEAR

			INSERT INTO temp945 (accountid,isa_num,st_num,wo_num,bol_no,ship_ref,filename,filedate) ;
				VALUES (m.accountid,c_CntrlNum,c_GrpCntrlNum,m.wo_num,m.bol_no,m.ship_ref,cFilenameShort,dt2)

			STORE "N1"+cfd+"SF"+cfd+ALLTRIM(cSF_Name)+cfd+"91"+cfd+"01"+csegd TO cString
			DO cstringbreak
			nSegCtr = nSegCtr + 1

			STORE "N3"+cfd+cSF_Addr1+csegd TO cString
			DO cstringbreak
			nSegCtr = nSegCtr + 1

			STORE "N4"+cfd+cSF_CSZ+csegd TO cString
			DO cstringbreak
			nSegCtr = nSegCtr + 1

*		ASSERT .F. MESSAGE "At Ship-to store...debug"
			cStoreNum = segmentget(@apt,"STORENUM",alength)
			IF EMPTY(cStoreNum)
				cStoreNum = m.dcnum
			ENDIF

			IF EMPTY(cStoreNum)
				STORE "N1"+cfd+"ST"+cfd+ALLTRIM(m.consignee)+csegd TO cString
			ELSE
				STORE "N1"+cfd+"ST"+cfd+ALLTRIM(m.consignee)+cfd+"92"+cfd+cStoreNum+csegd TO cString
			ENDIF
			DO cstringbreak
			nSegCtr = nSegCtr + 1

			cCountry = segmentget(@apt,"COUNTRY",alength)
			cCountry = ALLT(cCountry)
			IF EMPTY(cCountry)
				cCountry = "USA"
			ENDIF

			IF EMPTY(ALLTRIM(outship.address2))
				STORE "N3"+cfd+TRIM(outship.address)+csegd TO cString
			ELSE
				STORE "N3"+cfd+TRIM(outship.address)+cfd+TRIM(outship.address2)+csegd TO cString
			ENDIF
			DO cstringbreak
			nSegCtr = nSegCtr + 1

			STORE "N4"+cfd+cCity+cfd+cState+cfd+cZip+cfd+cCountry+csegd TO cString
			DO cstringbreak
			nSegCtr = nSegCtr + 1

			IF !EMPTY(m.shipfor)
				cSFStoreNum = segmentget(@apt,"SFSTORENUM",alength)
				IF EMPTY(cSFStoreNum)
					cSFStoreNum = ALLTRIM(m.sforstore)
				ENDIF
				IF EMPTY(ALLTRIM(m.sforstore))
					STORE "N1"+cfd+"Z7"+cfd+ALLTRIM(m.shipfor)+csegd TO cString
				ELSE
					STORE "N1"+cfd+"Z7"+cfd+ALLTRIM(m.shipfor)+cfd+"92"+cfd+cSFStoreNum+csegd TO cString
				ENDIF
				DO cstringbreak
				nSegCtr = nSegCtr + 1

				IF EMPTY(ALLTRIM(m.sforaddr2))
					STORE "N3"+cfd+TRIM(m.sforaddr1)+csegd TO cString
				ELSE
					STORE "N3"+cfd+TRIM(m.sforaddr1)+cfd+TRIM(m.sforaddr2)+csegd TO cString
				ENDIF
				DO cstringbreak
				nSegCtr = nSegCtr + 1

				IF !EMPTY(cSForState)
					STORE "N4"+cfd+cSForCity+cfd+cSForState+cfd+cSForZip+cfd+"USA"+csegd TO cString
				ELSE
					STORE "N4"+cfd+cSForCity+csegd TO cString
				ENDIF
				DO cstringbreak
				nSegCtr = nSegCtr + 1
			ENDIF

			cLoadID = ALLTRIM(m.appt_num)
			IF EMPTY(cLoadID)
				cErrMsg = "MISS APPT #"
				DO ediupdate WITH cErrMsg,.T.
				THROW
			ELSE
				STORE "N9"+cfd+"LD"+cfd+cLoadID+csegd TO cString
				DO cstringbreak
				nSegCtr = nSegCtr + 1
			ENDIF

			IF lJCPenney
				IF !lParcelType
					IF !EMPTY(ALLTRIM(m.batch_num))
						STORE "N9"+cfd+"P8"+cfd+ALLTRIM(m.batch_num)+csegd TO cString
						DO cstringbreak
						nSegCtr = nSegCtr + 1
					ELSE
						cErrMsg = "JCPENNEY-NO PUA"
						DO ediupdate WITH cErrMsg,.T.
						THROW
					ENDIF
				ENDIF
			ENDIF

			STORE "G62"+cfd+"11"+cfd+TRIM(DTOS(ddel_date))+cfd+"D"+cfd+cDelTime+csegd TO cString  && Ship date/time
			DO cstringbreak
			nSegCtr = nSegCtr + 1

			SELECT scacs
			IF lTesting AND (EMPTY(m.scac) OR EMPTY(m.ship_via))
				STORE "RDWY" TO m.scac
				STORE "ROADWAY" TO m.ship_via
			ELSE
				IF !EMPTY(TRIM(outship.scac))
					STORE ALLTRIM(outship.scac) TO m.scac
					m.scac = IIF(INLIST(m.scac,"FED","FEG"),"FDEG",m.scac)
					m.scac = IIF(INLIST(m.scac,"UPSL","UPSN","UPSS","UPGC"),"UPG",m.scac)
					m.scac = IIF(INLIST(m.scac,"UPWZ","UPOZ"),"UP2D",m.scac)
					lFedEx = .F.
					SELECT scacs
					IF SEEK(m.scac,"scacs","scac")
						cShipMethodPymt = IIF("COLLECT"$UPPER(scacs.NAME) OR "COLLECT"$UPPER(scacs.DISPNAME),"CC","PP")
						IF ("FEDERAL EXPRESS"$scacs.NAME) OR ("FEDEX"$scacs.NAME) OR ("FED EX"$scacs.NAME)
							lFedEx = .T.
							SELECT 0
							USE F:\3pl\DATA\parcel_carriers ALIAS parcels
							IF SEEK(m.scac,"parcels","scac")
								IF "FEDEX GROUND"$parcels.NAME
									m.scac = "FDEG"
								ENDIF
							ENDIF
							USE IN parcels
						ENDIF
						SELECT outship

						IF lParcelType OR lFedEx
							cCarrierType = "U" && Parcel as UPS or FedEx
						ENDIF
					ELSE
						WAIT CLEAR
						cErrMsg = "MISSING SCAC"
						DO ediupdate WITH cErrMsg,.T.
						THROW
					ENDIF
				ENDIF
			ENDIF

			SELECT outship

			IF !EMPTY(cTRNum)
				STORE "W27"+cfd+cCarrierType+cfd+TRIM(m.scac)+cfd+TRIM(m.ship_via)+cfd+;
					IIF(lParcelType,cShipMethodPymt,"")+cfd+"TL"+;
					REPLICATE(cfd,2)+cTRNum+csegd TO cString
			ELSE
				STORE "W27"+cfd+cCarrierType+cfd+TRIM(m.scac)+cfd+TRIM(m.ship_via)+;
					IIF(lParcelType,cfd+cShipMethodPymt,"")+csegd TO cString
			ENDIF

			IF !EMPTY(cString)
				DO cstringbreak
				nSegCtr = nSegCtr + 1
			ENDIF

*!*				IF lParcelType AND !lSkipCharges
*!*					STORE "G72"+cfd+"516"+cfd+"15"+REPLICATE(cfd,6)+ALLT(cCharge)+csegd TO cString
*!*					DO cstringbreak
*!*					nSegCtr = nSegCtr + 1
*!*				ENDIF

*			lFirstLoop = .F.

		ENDIF

*************************************************************************
*2	DETAIL LOOP - MATCHING SHIP_REF FIELD (FROM OUTSHIP)
*************************************************************************
		WAIT CLEAR
		WAIT WINDOW "Now creating Detail information "+cShip_ref NOWAIT NOCLEAR
		ASSERT .F. MESSAGE "In detail loop...DEBUG"
		SELECT vintradecopp
		SELECT outdet
		SET ORDER TO TAG outdetid
		SELECT vintradecopp
		SET RELATION TO outdetid INTO outdet

		SELECT vintradecopp
		LOCATE
*!*			BROWSE FIELDS wo_num,ship_ref,outshipid
		LOCATE FOR vintradecopp.ship_ref = TRIM(cShip_ref) AND vintradecopp.outshipid = nOutshipid
		IF !FOUND()
			IF !lTesting
				WAIT WINDOW "PT "+cShip_ref+" NOT FOUND in vintradecopp...ABORTING" TIMEOUT 2
				IF !lTesting
					lSQLMail = .T.
				ENDIF
				cErrMsg = "MISSING PT-SQL "+cShip_ref
				DO ediupdate WITH cErrMsg,.T.
				THROW
			ELSE
				LOOP
			ENDIF
		ENDIF

		scanstr = "vintradecopp.ship_ref = cShip_ref and vintradecopp.outshipid = nOutshipid"

		SELECT vintradecopp
		LOCATE
		LOCATE FOR &scanstr
		cUCC= "XXX"
		DO WHILE &scanstr

			IF TRIM(vintradecopp.ucc) = 'CUTS' && OR vintradecopp.totqty = 0
				IF !EOF()
					SKIP 1 IN vintradecopp
					LOOP
				ELSE
					EXIT
				ENDIF
			ENDIF

			IF TRIM(vintradecopp.ucc) <> cUCC
				STORE TRIM(vintradecopp.ucc) TO cUCC
				WAIT WINDOW "UCC: "+cUCC NOWAIT
			ENDIF

			STORE "LX"+cfd+ALLTRIM(STR(nCtnNumber))+csegd TO cString   && Carton Seq. # (up to Carton total)
			lDoPALSegment = .F.
			DO cstringbreak
			nSegCtr = nSegCtr + 1

			lDoManSegment = .T.
			DO WHILE vintradecopp.ucc = cUCC
				cDesc = ""
*				ASSERT .F. MESSAGE "At carton scan"
				SELECT outdet
				alength = ALINES(aptdet,outdet.printstuff,.T.,CHR(13))
				cUCCNumber = vintradecopp.ucc
				cUCCNumber = ALLTRIM(STRTRAN(TRIM(cUCCNumber)," ",""))

				IF lDoManSegment
					lDoManSegment = .F.
					nPTCtnTot =  nPTCtnTot + 1
*					cLinenum = TRIM(segmentget(@aptdet,"LINENUM",alength))
					cLinenum = ALLTRIM(outdet.linenum)
					STORE "MAN"+cfd+"GM"+cfd+REPLICATE("0",20)+cfd+cfd+"L"+cfd+cLinenum+csegd TO cString  && SCC-14 Number (Wal-mart spec)
					DO cstringbreak
					nSegCtr = nSegCtr + 1
*!*						IF lParcelType
*!*							STORE "MAN"+cfd+"CP"+cfd+TRIM(m.bol_no)+csegd TO cString  && SCC-14 Number (Wal-mart spec)
*!*							DO cstringbreak
*!*							nSegCtr = nSegCtr + 1
*!*						ENDIF
				ENDIF

				nShipDetQty = INT(VAL(outdet.PACK))
				nUnitSum = nUnitSum + nShipDetQty

				IF EMPTY(outdet.ctnwt) OR outdet.ctnwt = 0
					cCtnWt = ALLTRIM(STR(CEILING(outship.weight/outship.ctnqty)))
					IF lTesting
						cCtnWt = "3"
					ELSE
						IF EMPTY(cCtnWt) OR INT(VAL(cCtnWt)) = 0
							IF lParcelType
								cErrMsg = "WEIGHT ERR: PT "+cShip_ref
								DO ediupdate WITH cErrMsg,.T.
								THROW
							ELSE
								cCtnWt = 5
							ENDIF
						ENDIF
					ENDIF
				ENDIF

				cColor = UPPER(TRIM(outdet.COLOR))
				cSize = UPPER(TRIM(outdet.ID))
				cUPC = TRIM(outdet.upc)
				IF (ISNULL(cUPC) OR EMPTY(cUPC))
					cUPC = TRIM(vintradecopp.upc)
				ENDIF
				cItemNum = TRIM(outdet.custsku)
				cStyle = TRIM(outdet.STYLE)
				IF EMPTY(cStyle)
					WAIT WINDOW "Empty style in "+cShip_ref TIMEOUT 2
				ENDIF

				nShipDetQty = vintradecopp.totqty  && Using totqty vs. pack per Paul, 03.10.2010
*!*					IF ISNULL(nShipDetQty) OR EMPTY(nShipDetQty) OR nShipDetQty = 0
*!*						nShipDetQty = outdet.totqty
*!*						IF nShipDetQty = 0
*!*							SKIP 1 IN vintradecopp
*!*							LOOP
*!*						ENDIF
*!*					ENDIF

*				nOrigDetQty = INT(VAL(vintradecopp.PACK))
				nOrigDetQty = vintradecopp.totqty  && Using totqty vs. pack per Paul, 03.10.2010
*!*					IF ISNULL(nOrigDetQty) OR nOrigDetQty = 0
*!*						nOrigDetQty = nShipDetQty
*!*					ENDIF

*!* Changed the following to utilize original 940 unit codes from Printstuff field
				cUnitCode = TRIM(segmentget(@aptdet,"UNITCODE",alength))
				IF EMPTY(cUnitCode)
					cUnitCode = "CT"
				ENDIF

				IF nOrigDetQty = nShipDetQty
					nShipStat = "CC"
				ELSE
					nShipStat = "CP"
				ENDIF

&& Changed W12 to include ship_ref, removed from W06.
				STORE "W12"+cfd+nShipStat+cfd+ALLTRIM(STR(nOrigDetQty))+cfd+;
					ALLTRIM(STR(nShipDetQty))+cfd+ALLTRIM(STR(nOrigDetQty-nShipDetQty))+cfd+;
					cUnitCode+cfd+ALLTRIM(cUPC)+cfd+"IN"+cfd+cStyle+cfd+cfd+ALLTRIM(STR(ctnwt,4,1))+cfd+"L"+cfd+"L"+REPLICATE(cfd,9)+"VN"+cfd+cItemNum+csegd TO cString
				DO cstringbreak
				nSegCtr = nSegCtr + 1

*				ASSERT .f. MESSAGE "AT G69 LOOP"
				cDesc = ALLTRIM(segmentget(@aptdet,"DESC",alength))
				IF !EMPTY(cDesc)
					STORE "G69"+cfd+cDesc+csegd TO cString
					DO cstringbreak
					nSegCtr = nSegCtr + 1
				ENDIF

				SELECT vintradecopp

				lDoManSegment = .F.
				SKIP 1 IN vintradecopp
			ENDDO

			nCtnNumber = nCtnNumber + 1
		ENDDO

*************************************************************************
*2^	END DETAIL MATCHING SHIP_REF LOOP
*************************************************************************
		WAIT CLEAR
		SELECT outship
		closesegs()
	ENDSCAN


*************************************************************************
*1^	END OUTSHIP MAIN LOOP
*************************************************************************
	ASSERT .F. MESSAGE "At close of outship loop"

	DO close945
	=FCLOSE(nFilenum)
	IF !lTesting
		SELECT edi_trigger
		DO ediupdate WITH "945 CREATED",.F.
		SELECT edi_trigger
		LOCATE
*!*		This section has been superseded by AS2 connection as of 08.25.2017, per Mark B.
*!*
*!*			IF USED('ftpjobs')
*!*				USE IN ftpjobs
*!*			ENDIF

*!*			USE F:\edirouting\ftpjobs IN 0 ALIAS ftpjobs
*!*			cUseProc='945-INTRADECO-PUT'
*!*			SELECT CNT(1) AS CNT FROM ftpjobs WHERE jobname=cUseProc INTO CURSOR xtemp
*!*			IF xtemp.CNT<=3
*!*				INSERT INTO ftpjobs (jobname,USERID,jobtime,terminated,COMPLETE) VALUES (cUseProc,"paulg",DATETIME(),.F.,.F.)
*!*			ENDIF
*!*			USE IN ftpjobs

		IF !USED("ftpedilog")
			SELECT 0
			USE F:\edirouting\ftpedilog ALIAS ftpedilog
			INSERT INTO ftpedilog (TRANSFER,ftpdate,filename,acct_name,TYPE) VALUES ("945-"+ccustname+"-"+cCustLoc,dt2,cFilenameArch,UPPER(ccustname),"945")
			USE IN ftpedilog
		ENDIF
	ENDIF

	WAIT CLEAR
	WAIT WINDOW cCustFolder+" 945 Process complete..." NOWAIT

*!* Create eMail confirmation message
	IF lTestMail
		tsendto = tsendtotest
		tcc = tcctest
	ENDIF

	tsubject = cMailName+" 945 EDI File from FMI as of "+dtmail+" ("+cCustLoc+")"
	tattach = " "
	tmessage = "945 EDI Info from FMI, file "+cFilenameShort+CHR(13)
	tmessage = tmessage + "Division "+cDivision+", BOL# "+TRIM(cBOL)+CHR(13)
	tmessage = tmessage + "Work Orders: "+cWO_NumStr+","+CHR(13)
	tmessage = tmessage + "containing these picktickets:"+CHR(13)+CHR(13)
	tmessage = tmessage + cPTString + CHR(13)+CHR(13)
	tmessage = tmessage +"has been created and will be transmitted ASAP."+CHR(13)+CHR(13)
	IF !EMPTY(cMissDel)
		tmessage = tmessage+CHR(13)+CHR(13)+cMissDel+CHR(13)+CHR(13)
	ENDIF
	IF lTesting OR lTestinput
		tmessage = tmessage + "This is a TEST 945"
	ELSE
		tmessage = tmessage + "If you have any questions, please eMail or call 732-750-9000 x217."
	ENDIF

	IF lEmail
		DO FORM m:\dev\frm\dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
	ENDIF

	RELEASE ALL LIKE c_CntrlNum,c_GrpCntrlNum
	WAIT CLEAR
	WAIT WINDOW cMailName+" 945 EDI File output complete" TIMEOUT 1

*!* Transfers files to correct output folders
	COPY FILE [&cFilenameHold] TO [&cFilenameArch]
	WAIT WINDOW "" TIMEOUT 1

	IF lDoIntraFilesOut AND !lTesting
		COPY FILE [&cFilenameHold] TO [&cFilenameOut]
		DELETE FILE [&cFilenameHold]
		SELECT temp945
		COPY TO "f:\3pl\data\temp945int.dbf"
		USE IN temp945
		SELECT 0
		USE "f:\3pl\data\temp945int.dbf" ALIAS temp945int
		SELECT 0
		USE "f:\3pl\data\pts_sent945.dbf" ALIAS pts_sent945
		APPEND FROM "f:\3pl\data\temp945int.dbf"
		USE IN pts_sent945
		USE IN temp945int
		DELETE FILE "f:\3pl\data\temp945int.dbf"
	ENDIF

	*!* asn_out_data()()

CATCH TO oErr
	IF lDoCatch
		ASSERT .F. MESSAGE "In Error CATCH"
		SET STEP ON
*		lEmail = .F.
		DO ediupdate WITH IIF(EMPTY(cErrMsg),ALLTRIM(oErr.MESSAGE),cErrMsg),.T.
		tsubject = ccustname+" Error ("+TRANSFORM(oErr.ERRORNO)+") at "+TTOC(DATETIME())
		tattach  = ""
		IF lTesting
			tsendto  = tsendtotest
			tcc = tcctest
		ELSE
			tsendto  = tsendtoerr
			tcc = tccerr
		ENDIF

		tmessage = ccustname+" Error processing "+CHR(13)

		tmessage =tmessage+ "Try/CATCH Exeception Message:"+CHR(13)+;
			[  Error: ] + STR(oErr.ERRORNO) +CHR(13)+;
			[  LineNo: ] + STR(oErr.LINENO) +CHR(13)+;
			[  Message: ] + oErr.MESSAGE +CHR(13)+;
			[  Procedure: ] + oErr.PROCEDURE +CHR(13)+;
			[  Details: ] + oErr.DETAILS +CHR(13)+;
			[  StackLevel: ] + STR(oErr.STACKLEVEL) +CHR(13)+;
			[  LineContents: ] + oErr.LINECONTENTS+CHR(13)+;
			[  UserValue: ] + oErr.USERVALUE
		tmessage =tmessage+CHR(13)+cProgname
		tmessage =tmessage+CHR(13)+CHR(13)+"ERROR: "+cErrMsg

		tsubject = "945 EDI Poller Error at "+TTOC(DATETIME())
		tattach  = ""
		tcc=""
		DO FORM m:\dev\frm\dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
		IF nFilenum > 0
			FCLOSE(nFilenum)
		ENDIF
	ENDIF
FINALLY
	IF USED('OUTSHIP')
		USE IN outship
	ENDIF
	IF USED('OUTDET')
		USE IN outdet
	ENDIF
	IF USED('SHIPMENT')
		USE IN shipment
	ENDIF
	IF USED('PACKAGE')
		USE IN package
	ENDIF
	IF USED('SERFILE')
		USE IN serfile
	ENDIF
	IF USED('OUTWOLOG')
		USE IN outwolog
	ENDIF

	ON ERROR
	WAIT CLEAR

ENDTRY

*** END OF CODE BODY

****************************
PROCEDURE closesegs
****************************
	cTotCtnWt = ALLTRIM(STR(nTotCtnWt,10,1))
	STORE "W03"+cfd+ALLTRIM(STR(nPTCtnTot))+cfd+cTotCtnWt+cfd+;
		cWeightUnit+csegd TO cString   && Units sum, Weight sum, carton count
	nSegCtr = nSegCtr + 1

	nPTCtnTot = 0
	nTotCtnWt = 0
	nTotCtnCount = 0
	nUnitSum = 0
	FPUTS(nFilenum,cString)

	STORE  "SE"+cfd+ALLTRIM(STR(nSegCtr+1))+cfd+PADL(c_GrpCntrlNum,9,"0")+csegd TO cString
	FPUTS(nFilenum,cString)

ENDPROC

****************************
PROCEDURE close945
****************************
** Footer Creation

	STORE  "GE"+cfd+ALLTRIM(STR(nSTCount))+cfd+c_CntrlNum+csegd TO cString
	FPUTS(nFilenum,cString)

	STORE  "IEA"+cfd+"1"+cfd+PADL(c_CntrlNum,9,"0")+csegd TO cString
	FPUTS(nFilenum,cString)

	RETURN

****************************
PROCEDURE num_incr_isa
****************************

	IF !USED("serfile")
		USE ("f:\3pl\data\serial\"+ccustname+"945_serial") IN 0 ALIAS serfile
	ENDIF
	SELECT serfile
	IF lTesting AND lISAFlag
		nOrigSeq = serfile.seqnum
		lISAFlag = .F.
	ENDIF
	nISA_Num = serfile.seqnum
	c_CntrlNum = ALLTRIM(STR(serfile.seqnum))
	REPLACE serfile.seqnum WITH serfile.seqnum + 1 IN serfile
	SELECT outship
	RETURN

****************************
PROCEDURE num_incr_st
****************************

	IF !USED("serfile")
		USE ("f:\3pl\data\serial\"+ccustname+""945_serial") IN 0 ALIAS serfile
	ENDIF
	SELECT serfile
	IF lTesting AND lSTFlag
		nOrigGrpSeq = serfile.grpseqnum
		lSTFlag = .F.
	ENDIF
	c_GrpCntrlNum = ALLTRIM(STR(serfile.grpseqnum))
	REPLACE serfile.grpseqnum WITH serfile.grpseqnum + 1 IN serfile
	SELECT outship
	RETURN


****************************
PROCEDURE segmentget
****************************

	PARAMETER thisarray,lcKey,nLength

	FOR i = 1 TO nLength
		IF i > nLength
			EXIT
		ENDIF
		lnEnd= AT("*",thisarray[i])
		IF lnEnd > 0
			lcThisKey =TRIM(SUBSTR(thisarray[i],1,lnEnd-1))
			IF OCCURS(lcKey,lcThisKey)>0
				RETURN SUBSTR(thisarray[i],lnEnd+1)
				i = 1
			ENDIF
		ENDIF
	ENDFOR

	RETURN ""

****************************
PROCEDURE ediupdate
****************************
	PARAMETER cStatus,lIsError
	lDoCatch = .F.

	IF lIsError
*		SET STEP ON
	ENDIF

	IF !lTesting
		SELECT edi_trigger
		nRec = RECNO()
		IF !lIsError
			REPLACE processed WITH .T.,proc945 WITH .T.,file945 WITH cFilenameArch,isa_num WITH cISA_Num,;
				fin_status WITH "945 CREATED",errorflag WITH .F.,when_proc WITH DATETIME() ;
				FOR edi_trigger.bol = cBOL AND accountid = nAcctNum

*			xsqlexec("update edi_trigger set processed=1, proc945=1, file945='"+cFilenameArch+"', " + ;
				"isa_num='"+cISA_Num+"', fin_status='945 CREATED', errorflag=0, when_proc={"+TTOC(DATETIME())+"} " + ;
				"where bol='"+cBOL+"' and accountid="+TRANSFORM(nAcctNum),,,"stuff")

		ELSE
			REPLACE processed WITH .T.,proc945 WITH .F.,file945 WITH "",;
				fin_status WITH cStatus,errorflag WITH .T. ;
				FOR edi_trigger.bol = cBOL AND accountid = nAcctNum

*			xsqlexec("update edi_trigger set processed=1, proc945=0,file945='', " + ;
				"fin_status='"+cStatus+"', errorflag=1 " + ;
				"where bol='"+cBOL+"' and accountid="+TRANSFORM(nAcctNum),,,"stuff")

			IF lCloseOutput
				=FCLOSE(nFilenum)
				ERASE &cFilename
			ENDIF
		ENDIF
	ENDIF

	IF lIsError AND lEmail AND cStatus<>"SQL ERROR"
		tsubject = "945 Error in "+cMailName+" BOL "+TRIM(cBOL)+"(At PT "+cShip_ref+")"
		tattach = " "
		IF lTesting
			tsendto = tsendtotest
			tcc = tcctest
		ELSE
			tsendto = tsendtoerr
			tcc = tccerr
		ENDIF
		tmessage = "945 Processing for BOL# "+cBOL+", WO# "+cWO_Num+" produced this error: (Office: "+cOffice+"): "+cStatus+CHR(13)+"Check EDI_TRIGGER and re-run"
		IF "TOTQTY ERR"$cStatus
			tmessage = tmessage + CHR(13) + "At OUTSHIPID: "+ALLTRIM(STR(m.outshipid))
		ENDIF
		DO FORM m:\dev\frm\dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
	ENDIF

	IF USED('outship')
		USE IN outship
	ENDIF
	IF USED('outdet')
		USE IN outdet
	ENDIF

	IF USED('serfile')
		SELECT serfile
		IF lTesting
			REPLACE serfile.seqnum WITH nOrigSeq
			REPLACE serfile.grpseqnum WITH nOrigGrpSeq
		ENDIF
		USE IN serfile
	ENDIF
	IF USED('scacs')
		USE IN scacs
	ENDIF
	IF USED('mm')
		USE IN mm
	ENDIF
	IF USED('tempx')
		USE IN tempx
	ENDIF
	IF !lTesting
		SELECT edi_trigger
		LOCATE
	ENDIF

ENDPROC

****************************
PROCEDURE cstringbreak
****************************
	cLen = LEN(ALLTRIM(cString))
	IF ISNULL(cString) OR EMPTY(cString)
		ASSERT .F. MESSAGE "At cStringBreak procedure empty field...>>DEBUG<<"
	ENDIF
	FPUTS(nFilenum,cString)
	cString = ""
ENDPROC


****************************
PROCEDURE cszbreak
****************************
	cCSZ = ALLT(m.CSZ)

	FOR ii = 5 TO 2 STEP -1
		cCSZ = STRTRAN(cCSZ,SPACE(ii),SPACE(1))
	ENDFOR
	cCSZ = STRTRAN(cCSZ,",","")
	len1 = LEN(ALLT(cCSZ))
	nSpaces = OCCURS(" ",cCSZ)

	IF nSpaces<2
		cErrMsg = "BAD CSZ INFO"
		DO ediupdate WITH cErrMsg,.T.
		THROW
	ENDIF

*	ASSERT .F. MESSAGE "In CSZ Breakout"
	FOR ii = nSpaces TO 1 STEP -1
		ii1 = ALLT(STR(ii))
		DO CASE
			CASE ii = nSpaces
				nPOS = AT(" ",cCSZ,ii)
				cZip = ALLT(SUBSTR(cCSZ,nPOS))
*				WAIT WINDOW "ZIP: "+cZip TIMEOUT 1
				nEndState = nPOS
			CASE ii = (nSpaces - 1)
				nPOS = AT(" ",cCSZ,ii)
				cState = ALLT(SUBSTR(cCSZ,nPOS,nEndState-nPOS))
*				WAIT WINDOW "STATE: "+cState TIMEOUT 1
				IF nSpaces = 2
					cCity = ALLT(LEFT(cCSZ,nPOS))
*					WAIT WINDOW "CITY: "+cCity TIMEOUT 1
					EXIT
				ENDIF
			OTHERWISE
				nPOS = AT(" ",cCSZ,ii)
				cCity = ALLT(LEFT(cCSZ,nPOS))
*				WAIT WINDOW "CITY: "+cCity TIMEOUT 2
		ENDCASE
	ENDFOR
ENDPROC