*!* BILLABONG944_CREATE.PRG
PARAMETERS nwo_num,nAcctNum
DO m:\dev\prg\_setvars
PUBLIC lxfer944,cwhse,cfilenamehold,cfilenameout,tsendto,tcc,tsendtoerr,tccerr,liserror,ljoemail,losd
PUBLIC lemail,tfrom,cwhsemod,goffice,gmasteroffice,ltesting

tfrom = "Toll EDI Operations <toll-edi-ops@tollgroup.com>"

ltesting = .f. && If TRUE, disables certain functions
ltestinput = .F.
lusepl = .F.
liserror = .F.
DO m:\dev\prg\lookups

*nAcctNum = 6744
coffice = "L"

IF VARTYPE(nwo_num) # "N" AND ltesting
	SET STATUS BAR ON
	nwo_num = 7729886
	nAcctNum = 6744
	lusepl = .T.
	CLOSE DATABASES ALL
ENDIF

ccontainer = ""

IF ltestinput
	cusefolder = "F:\WHP\WHDATA\"
ELSE
	xreturn = "XXX"
	xreturn = wf(coffice,nAcctNum)
	IF xreturn = "XXX"
		WAIT WINDOW "No Bldg. # Provided...closing" TIMEOUT 3
		DO ediupdate WITH .T.,"NO BLDG #"
		RETURN
	ENDIF
	cusefolder = UPPER(xreturn)
ENDIF

IF ltesting
	cintusage = "T"
ELSE
	cintusage = "P"
ENDIF

SELECT 0
USE F:\3pl\DATA\mailmaster ALIAS mm SHARED
LOCATE FOR mm.accountid = nAcctNum ;
	AND mm.office = coffice ;
	AND mm.edi_type = "944"
IF !FOUND()
	ASSERT .F. MESSAGE "At missing Office/Acct"
	WAIT WINDOW "Office/Loc/Acct not found!" TIMEOUT 2
	DO ediupdate WITH .T.,"ACCT/LOC !FOUND"
	normalexit = .F.
	THROW
ENDIF
STORE TRIM(mm.acctname) TO ccustname
STORE TRIM(mm.basepath) TO lcoutpath
STORE TRIM(mm.holdpath) TO lcholdpath
STORE TRIM(mm.archpath) TO lcarchivepath
lusealt = mm.use_alt
tsendto = IIF(lusealt,mm.sendtoalt,mm.sendto)
tcc = IIF(lusealt,mm.ccalt,mm.cc)
LOCATE
LOCATE FOR mm.edi_type = "MISC" AND mm.taskname = "GENERAL"
lusealt = mm.use_alt
tsendtoerr = IIF(lusealt,mm.sendtoalt,mm.sendto)
tccerr = IIF(lusealt,mm.ccalt,mm.cc)
tsendtotest = IIF(lusealt,mm.sendtoalt,mm.sendto)
tcctest = IIF(lusealt,mm.ccalt,mm.cc)
USE IN mm

IF ltesting
	tsendto = tsendtotest
	tcc = tcctest
ENDIF

DIMENSION thisarray(1)
csuffix = ""
DO CASE
	CASE INLIST(coffice,"I","N")
		cwhse = "CARTERET"
	CASE coffice = "M"
		cwhse = "MIAMI"
	OTHERWISE
		cwhse = "MIRA LOMA"
ENDCASE

cmod = IIF(coffice = "C","2",coffice)
goffice = cmod
gmasteroffie = coffice
cwhsemod = LOWER("wh"+cmod)
gsystemmodule = UPPER(cwhsemod)

IF VARTYPE(nwo_num) <> "N"  OR EMPTY(nwo_num) && If WO is not numeric or equals Zero
	WAIT WINDOW "No WO# Provided...closing" TIMEOUT 3
	DO ediupdate WITH .T.,"NO WO_NUM"
	RETURN
ENDIF

losd = .F.
osdcheck()

loverridexcheck = .T. && If true, doesn't check ASN carton count against barcode scans.
lholdfile = .F. && If true, keeps output file in 944HOLD folder, otherwise into 944OUT
lemail = !ltesting  && If .t., sends mail to WAREHOUSES

cbarmiss = ""
casn = ""
cupc = ""
nstsets = 0
cspecchar = .F.
xferupdate = .T.
nfilenum = 0
lbackup = .F.
loverage = .F.
ljoemail = .T.
lcopyca = .T.
cerrmsg = ""
liserror = .T.
lxfer944 = .F.
ctrk_wo = ""
nctntotal = 0

cwo_num = ALLTRIM(STR(nwo_num))

IF !ltesting
	IF !USED("edi_xfer")
		USE F:\edirouting\DATA\edi_xfer IN 0 ALIAS edi_xfer ORDER TAG inven_wo
		SELECT edi_xfer
		SET FILTER TO edi_type = "944" AND accountid = nAcctNum
		IF SEEK(nwo_num)
			WAIT WINDOW "Work Order "+cwo_num+" already created and logged in EDI_XFER" TIMEOUT 2
			IF MESSAGEBOX("944 for Work Order "+cwo_num+" already created...Overwrite Date?",4+32+256,"WO# FOUND",8000) = 7 &&No
				USE IN edi_xfer
				DO ediupdate WITH .T.,"DUPLICATE WO#"
				RETURN
			ELSE
				xferupdate = .F.
			ENDIF
		ENDIF
	ENDIF
ENDIF


CREATE CURSOR temp1 (wo_num N(6), ;
	po_num c(5), ;
	STYLE c(20), ;
	expected i, ;
	actual i)

*!* SET CUSTOMER CONSTANTS
ccustname = "BILLABONG"  && Customer Identifier (for folders, etc.)
cmailname = PROPER(ccustname)  && Proper name for eMail, etc.
czladdress = "117 WATERWORKS WAY"
czlcity = "IRVINE"
czlstate = "CA"
czlzip = "92618"
ccustprefix = "BBG" && Customer Prefix (for output file names)
cx12 = "004010"  && X12 Standards Set used
crecid = IIF(ltesting,"BBGTOLLTST","BBGTOLLPRD")  && Recipient EDI address

dt1 = TTOC(DATETIME(),1)
dt2 = DATETIME()
dtmail = TTOC(DATETIME())

cfilestem = ccustprefix+dt1+".944"
cfilenamehold = (lcholdpath+cfilestem) && Holds file here until complete and ready to send
cfilenameout = (lcoutpath+cfilestem)
cfilenamearchive = (lcarchivepath+cfilestem)

STORE "" TO c_cntrlnum,c_grpcntrlnum
nfilenum = FCREATE(cfilenamehold)

*!* SET OTHER CONSTANTS
nctnnumber = 1
cstring = ""
csendqual = "ZZ"
csendid= IIF(ltesting,"TOLLTEST","TGFUSA")
crecqual = "ZZ"
cfd = "|"  && Field/element delimiter
csegd = "~" && Line/segment delimiter
nsegctr = 0
cterminator = ">" && Used at end of ISA segment
cdate = DTOS(DATE())
ctruncdate = RIGHT(cdate,6)
ctime = LEFT(TIME(DATE()),2)+SUBSTR(TIME(DATE()),4,2)
cfiledate = cdate+ctime
corig = "N"
cdate = DTOS(DATE())
ctruncdate = RIGHT(cdate,6)
ctime = LEFT(TIME(DATE()),2)+SUBSTR(TIME(DATE()),4,2)
cqtyrec = "1"
cctypeused = ""
nqtyexpected = 0
nctndamaged = 0
nctnshortage = 0
nctnexpected = 0  && From INWOLOG count
nctnactual = 0  && From TLDATA\Barcodes count
cmissingctn = "Missing Cartons: "+CHR(13)
cpo = ""
cstyle = ""
npoexpected = 0
npoactual = 0

** PAD ID Codes
csendidlong = PADR(csendid,15," ")
crecidlong = PADR(crecid,15," ")

IF USED('account')
	USE IN account
ENDIF

xsqlexec("select * from account where inactive=0","account",,"qq")
INDEX ON accountid TAG accountid
SET ORDER TO

*!* Serial number incrementing table
IF USED("serfile")
	USE IN serfile
ENDIF
USE ("F:\3pl\data\serial\"+ccustname+"944_serial") IN 0 ALIAS serfile

*!* Trigger Data
IF !USED('edi_trigger')
	cedifolder = "F:\3PL\DATA\"
	USE (cedifolder+"edi_trigger") IN 0 ALIAS edi_trigger ORDER TAG wo_num
ENDIF

*!* Lookup in table (parent)
IF USED('inwolog')
	USE IN inwolog
ENDIF

csq1 = [select * from inwolog where accountid in (6718,6744,6747) and wo_num = ]+TRANSFORM(nwo_num)
xsqlexec(csq1,,,"wh")
SELECT inwolog
INDEX ON inwologid TAG inwologid
LOCATE
IF ltesting
*BROWSE
ENDIF

*!* Lookup in table (xref for PO's)
IF USED('PL')
	USE IN pl
ENDIF
IF USED('XPL')
	USE IN xpl
ENDIF
IF USED('INDET')
	USE IN indet
ENDIF

*!* NOTE: The changes here were requested by Todd, 08.22.2017, in order to more correctly trap PO#
*!* The first SQL select takes from PL but uses alias of INDET to avoid having to make dozens of code changes
csq1 = [select * from pl where accountid in (6718,6744,6747) and wo_num = ]+TRANSFORM(nwo_num) 
xsqlexec(csq1,"indet",,"wh")
SELECT indet
INDEX ON inwologid TAG inwologid

*!* Added this section to correct PO# in added INDET detail lines. Joe, 06.07.2017 *!*
******JOE look here if you are using tempind2 to populate indet with the PONUM, then I would think here
*!*	csq1 = [select * from pl where accountid in (6718,6744,6747) and wo_num = ]+TRANSFORM(nwo_num)
*!*	xsqlexec(csq1,"pl",,"wh")
*!*	SELECT style,color, SPACE(12) as ponum,echo FROM pl INTO CURSOR vpl1 READWRITE
*!*	replace all ponum with getmemodata("echo","PONUM")  IN vpl1
*!*	SELECT distinct style, color, MAX(ponum) as ponum FROM vpl1 GROUP BY style,color INTO CURSOR vpl2 READWRITE

*!*	IF you don't get a hit on style/color look below next, else NA
*!*	SELECT distinct style, MAX(ponum) as ponum FROM vpl1 GROUP BY style INTO CURSOR vpl3 READWRITE
******JOE

CREATE CURSOR tempind (ponum c(10),STYLE c(20),COLOR c(10),qty i)
SELECT indet
LOCATE
SCAN FOR !EMPTY(ALLTRIM(indet.ECHO))
	cstyle = ALLTRIM(indet.STYLE)
	ccolor = ALLTRIM(indet.COLOR)
	nqty = indet.totqty
	alendet = ALINES(apl,indet.ECHO,.T.,CHR(13))
	FOR detscan = 1 TO alendet
		ccharline = ALLTRIM(apl[detscan])
		IF ccharline = "PONUM"
			cponum = ALLTRIM(SUBSTR(ccharline,AT("*",ccharline)+1))
			INSERT INTO tempind  (ponum,STYLE,COLOR,qty) VALUES (cponum,cstyle,ccolor,nqty)
		ENDIF
	ENDFOR
ENDSCAN
SELECT tempind
IF ltesting
	BROWSE
ENDIF

SELECT STYLE,COLOR,MAX(ponum) AS ponum FROM tempind GROUP BY 1,2 INTO CURSOR tempind1
IF ltesting
	BROWSE
ENDIF
**TMARG   SELECT STYLE,SPACE(20) AS COLOR, MAX(ponum) AS ponum FROM tempind1 WHERE EMPTY(tempind1.ponum) GROUP BY 1 INTO CURSOR tempind2
SELECT STYLE,SPACE(20) AS COLOR, MAX(ponum) AS ponum FROM tempind1 GROUP BY 1 INTO CURSOR tempind2
IF ltesting
	BROWSE
ENDIF

USE IN tempind

*!* Now that the records have been extracted from PL, we switch back to an INDET cursor to insert PO and other data for the 944 
USE IN indet
csq1 = [select * from indet where accountid in (6718,6744,6747) and wo_num = ]+TRANSFORM(nwo_num)
xsqlexec(csq1,"indet",,"wh")
SELECT indet
INDEX ON inwologid TAG inwologid

SELECT indet
SCAN FOR !units AND EMPTY(ALLTRIM(indet.ECHO))
	SELECT tempind1
	LOCATE FOR PADR(tempind1.STYLE,20) = indet.STYLE AND PADR(tempind1.COLOR,10) = indet.COLOR
	IF FOUND()
		REPLACE indet.ECHO WITH "PONUM*"+ALLTRIM(tempind1.ponum)+CHR(13)+"PACK*"+ALLTRIM(indet.PACK) NEXT 1 IN indet
	ELSE
		SELECT tempind2
		LOCATE FOR PADR(tempind2.STYLE,20) = indet.STYLE
		IF FOUND()
			REPLACE indet.ECHO WITH "PONUM*"+ALLTRIM(tempind2.ponum)+CHR(13)+"PACK*"+ALLTRIM(indet.PACK) NEXT 1 IN indet
		ELSE
			REPLACE indet.ECHO WITH "PONUM*NA"+CHR(13)+"PACK*"+ALLTRIM(indet.PACK) NEXT 1 IN indet
		ENDIF
	ENDIF
	cecho = ALLTRIM(indet.ECHO)
	SKIP 1 IN indet
	IF EMPTY(ALLTRIM(indet.ECHO))
		REPLACE indet.ECHO WITH cecho NEXT 1 IN indet
	ENDIF
ENDSCAN

SELECT indet
LOCATE
IF ltesting
	BROW
	SET STEP ON
ENDIF

COUNT TO n1 FOR EMPTY(ALLTRIM(indet.ECHO))

IF n1>0
	WAIT WINDOW "There are "+TRANSFORM(n1)+" detail records with empty Echo fields"+CHR(13)+"Filling these now" TIMEOUT 2
	SCAN FOR EMPTY(ALLTRIM(ECHO)) AND !units
		REPLACE indet.ECHO WITH "PONUM*NA"+CHR(13)+"PACK*"+ALLTRIM(indet.PACK) NEXT 1 IN indet
		cecho = ALLTRIM(indet.ECHO)
		SKIP 1 IN indet
		IF EMPTY(ALLTRIM(indet.ECHO))
			REPLACE indet.ECHO WITH cecho IN indet NEXT 1
		ENDIF
	ENDSCAN
ELSE
	WAIT WINDOW "There are NO detail records with empty Echo fields" TIMEOUT 2
ENDIF

USE IN tempind2
RELEASE n1
*!* End added code *!*

SELECT indet
LOCATE
IF ltesting
	BROWSE FOR !units
ENDIF

nrcnt = 0
nisacount = 0
nstcount = 1
nsecount = 1

SELECT inwolog
WAIT WINDOW "" TIMEOUT 2

LOCATE FOR inwolog.wo_num = nwo_num
IF !FOUND()
	SET STEP ON
	WAIT WINDOW "WO# "+cwo_num+" not found in INWOLOG...Terminating" TIMEOUT 3
	IF !ltesting
		lcopyca = .T.
		DO ediupdate WITH .T.,"MISS INWOLOG WO"
		RETURN
	ENDIF
ENDIF

ntrk_wo = INT(VAL(inwolog.comments))  && Extracts Trucking WO# from INWOLOG
ctrk_wo = ALLTRIM(STR(ntrk_wo))
ccontainer = IIF(!EMPTY(inwolog.CONTAINER),TRIM(inwolog.CONTAINER),TRIM(inwolog.REFERENCE)) && Extracts Container ID from INWOLOG
IF EMPTY(TRIM(ccontainer))
  ccontainer='MISSING'
 ENDIF
IF EMPTY(TRIM(ccontainer))
	WAIT WINDOW "There is no container attached to "+cwo_num TIMEOUT 3
	lcopychris = .T.
	DO ediupdate WITH .T.,"NO CONTAINER"
	RETURN
ENDIF

SELECT indet
IF !ltesting
	csq1 = [select * from indet where wo_num = ]+TRANSFORM(nwo_num)
	xsqlexec(csq1,"tempwo1",,"wh")
	SELECT tempwo1
	LOCATE
	IF RECCOUNT()=0
		waitstr = "WO# "+cwo_num+" Not Found in Inbd Detail Table..."+CHR(13)+;
			"WO has not been confirmed...Terminating"
		WAIT WINDOW waitstr TIMEOUT 3
		DO ediupdate WITH .T.,"WO NOT CONF. IN INDET"
		RETURN
	ENDIF
	USE IN tempwo1
ENDIF

SELECT inwolog
cacctname = TRIM(inwolog.acctname)
cconfirmdate = DTOS(TTOD(inwolog.confirmdt))
cconfirmdate = IIF(EMPTY(cconfirmdate) AND ltesting,DTOS(DATE()),cconfirmdate)

ccontainer = TRIM(inwolog.CONTAINER)
cctrtype = IIF(SUBSTR(ccontainer,4,1)="U","SQ","AW")
cacct_ref = TRIM(inwolog.acct_ref)
alenhead = ALINES(apthead,inwolog.comments,.T.,CHR(13))

detlinescreate()

IF nisacount = 0
	DO num_incr
	STORE "ISA"+cfd+"00"+cfd+"          "+cfd+"00"+cfd+"          "+cfd+csendqual+cfd+csendidlong+cfd+;
		crecqual+cfd+crecidlong+cfd+ctruncdate+cfd+ctime+cfd+"U"+cfd+"00401"+cfd+;
		PADL(c_cntrlnum,9,"0")+cfd+"0"+cfd+cintusage+cfd+cterminator+csegd TO cstring
	DO cstringbreak

	STORE "GS"+cfd+"RE"+cfd+csendid+cfd+crecid+cfd+cdate+cfd+ctime+cfd+c_cntrlnum+;
		cfd+"X"+cfd+cx12+csegd TO cstring
	DO cstringbreak

	nsegctr = 0
	nisacount = 1
ENDIF

SELECT temppodet
LOCATE
SCAN

	cponum = ALLTRIM(temppodet.ponum)

	STORE "ST"+cfd+"944"+cfd+PADL(c_grpcntrlnum,9,"0")+csegd TO cstring
	DO cstringbreak
	nsegctr = nsegctr + 1

	IF ltesting AND EMPTY(cacct_ref)
		cacct_ref = "9999999"
	ENDIF
	STORE "W17"+cfd+corig+cfd+cdate+cfd+IIF(EMPTY(cacct_ref),cwo_num,cacct_ref)+REPLICATE(cfd,5)+cponum+cfd+"000000"+csegd TO cstring
	DO cstringbreak
	nsegctr = nsegctr + 1

	cconame = ALLTRIM(segmentget(@apthead,"CONAME",alenhead))
	cconame = IIF(EMPTY(cconame),UPPER(cmailname),cconame)
	STORE "N1"+cfd+"SE"+cfd+UPPER(cconame)+cfd+"93"+cfd+cwhse+csegd TO cstring
	DO cstringbreak
	nsegctr = nsegctr + 1
***Added 5/11/17 TMARG  Added
	cregion = ALLTRIM(segmentget(@apthead,"REGION",alenhead))
	cregion = IIF(EMPTY(cregion),"NO REGION",cregion)
	STORE "N1"+cfd+"UD"+cfd+UPPER(cregion)+csegd TO cstring
	DO cstringbreak
	nsegctr = nsegctr + 1

	cbrand = TRIM(segmentget(@apthead,"BRAND",alenhead))
	STORE "N9"+cfd+"3L"+cfd+cbrand+csegd TO cstring  && Used for either AWB or container type
	DO cstringbreak
	nsegctr = nsegctr + 1

	STORE "N9"+cfd+"D2"+cfd+cwo_num+csegd TO cstring  && Added Division info per Joe S.
	DO cstringbreak
	nsegctr = nsegctr + 1

	STORE "G62"+cfd+"19"+cfd+cconfirmdate+csegd TO cstring
	DO cstringbreak
	nsegctr = nsegctr + 1


	STORE 0 TO ntotctnwt,ntotctncube
	SELECT * FROM tempindet INTO CURSOR vtempindet READWRITE
	SELECT * FROM vtempindet INTO CURSOR tempindet READWRITE
	SELECT tempindet
	LOCATE
	IF ltesting
*	BROWSE
	ENDIF
	SCAN FOR tempindet.ponum = PADR(cponum,10) && FOR indet.inwologid = inwolog.inwologid AND units...adding padding 05.17.2017, Joe, per Todd.
		alendet = ALINES(apl,indet.ECHO,.T.,CHR(13))
		IF '-' $(tempindet.STYLE)
			pos1=ATC('-',tempindet.STYLE)
			REPLACE tempindet.STYLE WITH  SUBSTR(tempindet.STYLE,1,pos1-1)
		ENDIF
		cstyle = ALLTRIM(tempindet.STYLE)
		ccolor = ALLTRIM(tempindet.COLOR)
		cid = ALLTRIM(tempindet.ID)
		xsqlexec("select upc from upcmast where style = '"+cstyle+"' and color = '"+ccolor+"' and id = '"+cid+"'","tempupcmast",,"wh") && Changed UPC retrieval per Todd. Joe,  05.31.2017
		cupc = ALLTRIM(tempupcmast.upc)
		ASSERT .F. MESSAGE "At ctn wt/cube calcs"
		nctnwt = IIF(ltesting,1.4,(tempindet.ctnwt/2.202))
		ntotctnwt = ntotctnwt+nctnwt
		nctncube = IIF(ltesting,0.06,(tempindet.ctncube*0.0283))
		ntotctncube = ntotctncube+nctncube
****TMARG
**		STORE "W07"+cfd+ALLTRIM(tempindet.PACK)+cfd+"UN"+cfd+cfd+"UP"+cfd+cUPC+csegd TO cString
		STORE "W07"+cfd+ALLTRIM(STR(tempindet.totqty))+cfd+"UN"+cfd+cfd+"UP"+cfd+cupc+csegd TO cstring
		DO cstringbreak
		nsegctr = nsegctr + 1

		STORE "W20"+REPLICATE(cfd,4)+ALLTRIM(STR(nctnwt,6,1))+cfd+"FR"+cfd+"K"+REPLICATE(cfd,2)+ALLTRIM(STR(nctncube,6,2))+cfd+"CR"+csegd TO cstring
		DO cstringbreak
		nsegctr = nsegctr + 1

	ENDSCAN

	SUM ctns TO nctns FOR tempindet.ponum = PADR(cponum,10)
	SUM totqty TO nunits FOR tempindet.ponum = PADR(cponum,10)

*!*		SELECT indet
*!*		SUM totqty TO nCtns FOR !units AND indet.wo_num = nWO_Num
*!*		SUM totqty TO nUnits FOR units AND indet.wo_num = nWO_Num

	cctns = ALLTRIM(STR(nctns))
	cunits = ALLTRIM(STR(nunits))

	WAIT WINDOW "END OF SCAN/CHECK PHASE ROUND..." NOWAIT

	stclose()
ENDSCAN

close944()
lxfer944 = .T.
=FCLOSE(nfilenum)

IF USED('serfile')
	USE IN serfile
ENDIF

IF !ltesting
	IF !USED("ftpedilog")
		SELECT 0
		USE F:\edirouting\ftpedilog ALIAS ftpedilog
		INSERT INTO ftpedilog (ftpdate,filename,acct_name,TYPE) VALUES (dt2,cfilenamehold,"cCustname","944")
		USE IN ftpedilog
	ENDIF
ENDIF

SELECT edi_trigger

WAIT "944 Creation process complete:"+CHR(13)+cfilenamehold WINDOW AT 45,60 TIMEOUT 3
*	DO F:\polorl\prg\total944.prg WITH lXfer944,cFilenameHold  && Checks expected vs. actual totals
cfin = "944 FILE "+cfilenameout+" CREATED WITHOUT ERRORS"
COPY FILE &cfilenamehold TO &cfilenamearchive
IF !lholdfile AND !ltesting
	COPY FILE &cfilenamehold TO &cfilenameout
	ERASE &cfilenamehold
ENDIF

IF !ltesting
	SELECT edi_trigger
	LOCATE
	REPLACE edi_trigger.processed WITH .T.,edi_trigger.created WITH .T.,edi_trigger.when_proc WITH DATETIME(),;
		edi_trigger.fin_status WITH "944 CREATED",edi_trigger.errorflag WITH .F.,file944crt WITH cfilenamehold ;
		FOR edi_trigger.wo_num = nwo_num AND edi_trigger.accountid = nAcctNum AND edi_trigger.edi_type = "944"
	LOCATE
ENDIF
liserror = .F.

IF lemail
	joemail()
ENDIF
RELEASE ALL LIKE T*
closedata()

************************************************************************
*!* END OF MAIN PROGRAM
************************************************************************

****************************
PROCEDURE stclose
****************************
	ctotctnwt = ALLTRIM(STR(ntotctnwt,10,2))
	ctotctncube = ALLTRIM(STR(ntotctncube,10,2))
	nctnshortage = 0
	IF nctnshortage < 0
		WAIT CLEAR
		loverage = .T.
		tsendto = tsendtoerr
		tcc = tccerr
		tsubject = cmailname+" 944 EDI File OVERAGE, WO "+cwo_num
		tattach = " "
		tmessage = "PO "+cponumold+" OVERAGE (More cartons found than expected for a PO)! Check EDI File immediately!"
		tmessage = tmessage + "Expected: "+TRANSFORM(nctnexpected)+", Actual: "+TRANSFORM(nctnactual)
		tfrom = "Toll EDI Operations <toll-edi-ops@tollgroup.com>"
		DO FORM m:\dev\frm\dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
	ENDIF

	STORE "W14"+cfd+cunits+cfd+cctns+cfd+"0"+cfd+ctotctncube+cfd+ctotctnwt+csegd TO cstring
	DO cstringbreak
	nsegctr = nsegctr + 1
	STORE 0 TO ntotctnwt,ntotctncube

	STORE  "SE"+cfd+ALLTRIM(STR(nsegctr+1))+cfd+PADL(c_grpcntrlnum,9,"0")+csegd TO cstring
	FWRITE(nfilenum,cstring)
	nstcount = 1
	nsecount = 1
	nsegctr = 0
	nstsets = nstsets + 1

	SELECT serfile
	REPLACE serfile.grpseqnum WITH serfile.grpseqnum + 1
	c_grpcntrlnum = ALLTRIM(STR(serfile.grpseqnum))
*? c_GrpCntrlNum
	SELECT indet
	lxcount = 1
	STORE 0 TO nctnexpected,nctnactual,nctndamaged,nctnshortage
	RETURN

****************************
PROCEDURE close944
****************************
** Footer Creation

	STORE  "GE"+cfd+ALLTRIM(STR(nstsets))+cfd+c_cntrlnum+csegd TO cstring
	FWRITE(nfilenum,cstring)

	STORE  "IEA"+cfd+"1"+cfd+PADL(c_cntrlnum,9,"0")+csegd TO cstring
	FWRITE(nfilenum,cstring)

	RETURN

****************************
PROCEDURE cstringbreak
****************************
	FWRITE(nfilenum,cstring)
	RETURN

****************************
PROCEDURE num_incr
****************************
	SELECT serfile
	REPLACE serfile.seqnum WITH serfile.seqnum + 1 IN serfile
	REPLACE serfile.grpseqnum WITH serfile.grpseqnum + 1 IN serfile
	c_cntrlnum = ALLTRIM(STR(serfile.seqnum))
	c_grpcntrlnum = ALLTRIM(STR(serfile.grpseqnum))
	RETURN

****************************
PROCEDURE ediupdate
****************************
	PARAMETERS liserror,cfin
	IF cfin = "NO PO DATA IN ECHO"
		cfin = "NO 944 POSSIBLE: 'Echo' field empty"
		archivefile = ""
	ENDIF

	IF !ltesting
		SELECT edi_trigger
		LOCATE
		REPLACE processed WITH .T.,created WITH .F.,fin_status WITH cfin,errorflag WITH .F. ;
			FOR edi_trigger.wo_num = nwo_num AND edi_trigger.accountid = nAcctNum AND edi_trigger.edi_type = "944"
		LOCATE
		IF !liserror
			REPLACE when_proc WITH DATETIME(),file944crt WITH archivefile ;
				FOR edi_trigger.wo_num = nwo_num ;
				AND edi_trigger.accountid = nAcctNum AND edi_trigger.edi_type = "944"
		ELSE
			REPLACE processed WITH .T.,created WITH .F.,fin_status WITH cfin,errorflag WITH .T. ;
				edi_trigger.file944crt WITH "" ;
				FOR edi_trigger.wo_num = nwo_num AND edi_trigger.accountid = nAcctNum ;
				AND edi_trigger.edi_type = "944"
		ENDIF
		IF ljoemail AND lemail
			joemail()
		ENDIF
	ENDIF
	closeout()
ENDPROC

****************************
PROCEDURE closeout
****************************
	IF !EMPTY(nfilenum)
		=FCLOSE(nfilenum)
		ERASE &cfilenamehold
	ENDIF
	IF USED('barcodes')
		SELECT barcodes
		SET FILTER TO
		USE IN barcodes
	ENDIF
	closedata()
ENDPROC

****************************
PROCEDURE closedata
****************************
	IF USED('barcodes')
		SELECT barcodes
		SET FILTER TO
		USE IN barcodes
	ENDIF
	IF USED('edi_xfer')
		USE IN edi_xfer
	ENDIF
	IF USED('csrBCdata')
		USE IN csrbcdata
	ENDIF
	IF USED('detail')
		USE IN DETAIL
	ENDIF
	IF USED('indet')
		USE IN indet
	ENDIF
	IF USED('inwolog')
		USE IN inwolog
	ENDIF
	RELEASE ALL LIKE ccontainer
	WAIT CLEAR
	_SCREEN.CAPTION = "INBOUND POLLER - EDI"
ENDPROC

****************************
PROCEDURE joemail
****************************
	IF liserror OR cfin = "NO PO DATA IN ECHO"
		tsubject = cmailname+" EDI ERROR (944), Inv. WO"+cwo_num
		IF cfin = "NO PO DATA IN ECHO"
			SELECT 0
			USE F:\3pl\DATA\mailmaster ALIAS mm
			LOCATE FOR mm.edi_type = "MISC" AND mm.taskname = "MODSHOEPODATA"
			lusealt= IIF(mm.use_alt,.T.,.F.)
			tsendto = IIF(lusealt,sendtoalt,sendto)
			tcc = IIF(lusealt,ccalt,cc)
			USE IN mm
		ENDIF
	ELSE
		tsubject = cmailname+" EDI FILE (944) Created, Inv. WO"+cwo_num
	ENDIF

	tattach = " "
	IF (ctrk_wo = "0" OR EMPTY(ctrk_wo))
		cthistrk_wo = "MISSING"
	ELSE
		cthistrk_wo = ctrk_wo
	ENDIF
	tmessage = "Inbd. WO #: "+cwo_num+CHR(13)
	cacctname = IIF(SEEK(nAcctNum,'account','accountid'),ALLTRIM(account.acctname),"")
	tmessage = tmessage+CHR(13)+"Acct ID: "+TRANSFORM(nAcctNum)+", Account: "+cacctname

	IF EMPTY(cerrmsg)
		tmessage = tmessage+CHR(13)+cfin
	ELSE
		tmessage = tmessage+CHR(13)+cerrmsg
	ENDIF
	IF cfin = "NO PO DATA IN ECHO"
		tmessage = tmessage + CHR(13)+CHR(13)+"Container/Airbill: "+ccontainer
	ENDIF

	DO FORM m:\dev\frm\dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
	IF !liserror
*		transitionmail()
	ENDIF
ENDPROC


****************************
PROCEDURE transitionmail
****************************
	RETURN

	tsendto = tsendtotest
	tcc = tcctest
	tfrom = "Toll EDI Operations <toll-edi-ops@tollgroup.com>"
	tsubject = ccustname+" 944 output ready"
	tmessage = "A 944 for WO# "+cwo_num+"was created as"
	tmessage = tmessage + "f:\ftpusers\"+ccustname+"\944OUT\transition\prl"+dt1+".944."
	tmessage = tmessage + "Check file and drop into 944OUT folder if correctly created."

	DO FORM m:\dev\frm\dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
ENDPROC

****************************
PROCEDURE osdcheck
****************************

	SELECT 0
	csq1 = [select * from pl where inlist(accountid,]+gtkohighlineaccounts+[)]
	xsqlexec(csq1,,,"wh")

	IF !USED("indet")
		csq1 = [select * from indet where inlist(accountid,]+gtkohighlineaccounts+[)]
		xsqlexec(csq1,,,"wh")
	ENDIF

	SELECT * FROM pl WHERE wo_num=nwo_num INTO CURSOR xpl
	INDEX ON TRANS(units)+STYLE+COLOR+ID+PACK TAG match
	USE IN pl

	SELECT * FROM indet WHERE wo_num=nwo_num INTO CURSOR xindet
	INDEX ON TRANS(units)+STYLE+COLOR+ID+PACK TAG match
	USE IN indet

	CREATE CURSOR xrpt (date_rcvd d, units l, STYLE c(20), COLOR c(10), ID c(10), PACK c(10), plqty N(6), indetqty N(6), diffqty N(7), DIMEN c(10), ctncube N(6,2), ctnwt N(4,0))
	INDEX ON TRANS(units)+STYLE+COLOR+ID+PACK TAG match

	SELECT xpl
	SCAN
		SCATTER MEMVAR FIELDS units, STYLE, COLOR, ID, PACK, ctncube, ctnwt

		DO CASE
			CASE !SEEK(TRANS(xpl.units)+xpl.STYLE+xpl.COLOR+xpl.ID+xpl.PACK,"xindet","match")
				m.indetqty=0
			CASE xindet.totqty#xpl.totqty
				SELECT xindet
				m.indetqty=xindet.totqty
			OTHERWISE
				LOOP
		ENDCASE

		m.dimen=TRANS(LENGTH)+"x"+TRANS(WIDTH)+"x"+TRANS(depth)
		m.plqty=xpl.totqty
		m.diffqty=m.indetqty-m.plqty

		IF SEEK(m.style+m.color+m.id+m.pack,"xrpt","match")
			REPLACE plqty WITH xrpt.xplqty+m.plqty, indetqty WITH xrpt.indetqty+m.indetqty IN xrpt
		ELSE
			INSERT INTO xrpt FROM MEMVAR
		ENDIF
	ENDSCAN

	SELECT xindet
	SCAN
		IF !SEEK(TRANS(xindet.units)+xindet.STYLE+xindet.COLOR+xindet.ID+xindet.PACK,"xpl","match")
			m.plqty=0
			m.indetqty=xindet.totqty

			SCATTER MEMVAR FIELDS units, STYLE, COLOR, ID, PACK, ctncube, ctnwt, date_rcvd
			m.dimen=TRANS(LENGTH)+"x"+TRANS(WIDTH)+"x"+TRANS(depth)
			m.diffqty=m.indetqty-m.plqty

			IF SEEK(m.style+m.color+m.id+m.pack,"xrpt","match")
				REPLACE xrpt.plqty WITH xrpt.plqty+m.plqty, indetqty WITH xrpt.indetqty+m.indetqty IN xrpt
			ELSE
				INSERT INTO xrpt FROM MEMVAR
			ENDIF
		ENDIF
	ENDSCAN

	losd = IIF(RECCOUNT("xrpt")#0,.T.,.F.)
	USE IN xpl
	USE IN xindet
	USE IN xrpt
ENDPROC

*****************************
PROCEDURE detlinescreate
*****************************
	SELECT SPACE(10) AS ponum,SPACE(12) AS upc,wo_num,STYLE,COLOR,ID,totqty,PACK,000 AS ctns,ctnwt,ctncube FROM indet WHERE .F. INTO CURSOR tempindet2 READWRITE
	SELECT indet
	IF ltesting
*
	ENDIF
	SCAN FOR indet.wo_num = nwo_num AND units
		SCATTER MEMO MEMVAR
		alendet = ALINES(apl,indet.ECHO,.T.,CHR(13))
		m.upc = TRIM(segmentget(@apl,"UPC",alendet))
		m.pack = TRIM(segmentget(@apl,"PACK",alendet))
		m.ctns = IIF(EMPTY(m.pack),1,(m.totqty/INT(VAL(m.pack))))
		m.pack = IIF(EMPTY(m.pack),TRANSFORM(m.totqty),m.pack)
		SKIP 1 IN indet
		m.ctncube = indet.totcube
		m.ctnwt = indet.totwt
		SKIP -1 IN indet
		FOR detscan = 1 TO alendet
			ccharline = ALLTRIM(apl[detscan])
			IF ccharline = "PONUM"
				m.ponum = ALLTRIM(SUBSTR(ccharline,AT("*",ccharline)+1))
				INSERT INTO tempindet2 FROM MEMVAR
			ENDIF
*		m.ponum = "NA"
		ENDFOR
	ENDSCAN

	SELECT tempindet2
	LOCATE

	SELECT * FROM tempindet2 ORDER BY ponum,upc INTO CURSOR tempindet
	SELECT DISTINCT ALLTRIM(PADR(ponum,10)) AS ponum FROM tempindet2 ORDER BY ponum INTO CURSOR temppodet

*!*	IF ltesting OR ltestinput
*!*		SELECT temppodet
*!*		LOCATE
*!*		BROWSE
*!*		SELECT tempindet
*!*		LOCATE
*!*		BROWSE
*!*		SET STEP ON
*!*	ENDIF
	IF ltesting
*	CANCEL
	ENDIF

****************************
PROCEDURE segmentget
****************************

	PARAMETER thisarray,lckey,nlength

	FOR i = 1 TO nlength
		IF i > nlength
			EXIT
		ENDIF
		lnend= AT("*",thisarray[i])
		IF lnend > 0
			lcthiskey =TRIM(SUBSTR(thisarray[i],1,lnend-1))
			IF OCCURS(lckey,lcthiskey)>0
				RETURN SUBSTR(thisarray[i],lnend+1)
				i = 1
			ENDIF
		ENDIF
	ENDFOR

	RETURN ""
ENDPROC

