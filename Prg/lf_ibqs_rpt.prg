* LF_IBQS_RPT
*
* Create a series of daily reports for Lifefactory.
*
* specc'ed by Chris Malcolm and PG
*
* First report will be "Inventory Build Qty and Schedule"
*
* 2nd will be "Material Inventory and Usage"
*
* more after those are working
*
* EXE = F:\UTIL\LF\LF_IBQS_RPT.EXE


runack("LF_IBQS_RPT")

LOCAL lnError, lcProcessName, loLF_IBQS_RPT

DECLARE INTEGER CreateMutex IN Win32API ;
	STRING @lpMutexAttributes, ;
	STRING bInitialOwner, ;
	STRING @lpName

DECLARE INTEGER GetLastError IN Win32API

* if running in EXE check to see if already running
IF _VFP.STARTMODE > 0 THEN
	lcProcessName = "LF_IBQS_RPT"
	CreateMutex(0,CHR(1),@lcProcessName)
	lnError = GetLastError()
	IF lnError = 183
		WAIT WINDOW TIMEOUT 10 "Lifefactory Inventory, Build QTY & Schedule process is already running..."
		RETURN .F.
	ENDIF
ENDIF

utilsetup("LF_IBQS_RPT")


loLF_IBQS_RPT = CREATEOBJECT('LF_IBQS_RPT')

*set step on 
loLF_IBQS_RPT.MAIN()

schedupdate()

CLOSE DATABASES ALL
RETURN

#DEFINE LOGIT 1
#DEFINE WAITIT 2
#DEFINE NOWAITIT 4
#DEFINE SENDIT 8
#DEFINE CRLF CHR(13) + CHR(10)
#DEFINE LINEFEED CHR(10)
#DEFINE RETURN_DATA_MANDATORY .F.
#DEFINE RETURN_DATA_NOT_MANDATORY .T.
#DEFINE DAILY_OT_MULTIPLIER 1.5
#DEFINE DAILY_OT20_MULTIPLIER 2.0
#DEFINE xlContinuous 1
#DEFINE xlMedium -4138
#DEFINE xlDiagonalDown 5
#DEFINE xlDiagonalUp 6
#DEFINE xlInsideHorizontal 12
#DEFINE xlInsideVertical 11
#DEFINE xlThick 4
#DEFINE xlThin 2
#DEFINE xlNone -4142
#DEFINE xlEdgeBottom 9
#DEFINE xlEdgeLeft 7
#DEFINE xlEdgeRight 10
#DEFINE xlEdgeTop 8

DEFINE CLASS LF_IBQS_RPT AS CUSTOM

	cProcessName = 'LF_IBQS_RPT'

	lTestMode = .F.  && .F. FOR NORMAL OPERATION, .T. TO SUPPRESS ALL BUT DEVELOPER EMAILS AND CHANGE INPUT TABLE, ETC.

	lAutoYield = .F.

	cStartTime = TTOC(DATETIME())

	* processing properties
	cProcessDesc = "Lifefactory Inventory, Build Qty & Schedule process"
	nAccountID = 6034

	* object properties
	oExcel = NULL
	oWorkbook = NULL
	osheet1 = NULL

	* file properties
	nFileHandle = 0
	cFiletoSaveAs = ""

	* date properties
	dtNow = DATETIME()

	dToday = DATE()
	*dToday = {^2017-11-18}

	cToday = DTOC(DATE())

	* wait window properties
	nWaitWindowTimeout = 2
	lWaitWindowIsOn = .T.

	* logfile properties
	lLoggingIsOn = .T.
	cLogFile = 'F:\UTIL\LF\LOGFILES\LF_IBQS_RPT_LOG.TXT'

	* INTERNAL email properties
	lSendInternalEmailIsOn = .T.
	cFrom = 'Mark.Bennett@Tollgroup.com'

	cSendTo = 'ethan@lifefactory.com, vlad@lifefactory.com, greg@lifefactory.com, leandra.murchison@tollgroup.com, chris.malcolm@tollgroup.com'
*	cSendTo = 'dyoung@fmiint.com'
	cCC = 'mbennett@fmiint.com'
	cSubject = ''
	cAttach = ''
	cBodyText = ''
	cTopBodyText = '' 

	cCOMPUTERNAME = ''
	cUSERNAME = ''

	FUNCTION INIT
		IF NOT DODEFAULT()
			RETURN .F.
		ENDIF
		WITH THIS
			CLEAR
			*SET RESOURCE OFF
			CLOSE DATA
			SET CENTURY ON
			SET DATE AMERICAN
			SET HOURS TO 24
			SET ANSI ON
			SET TALK OFF
			SET DELETED ON
			SET CONSOLE OFF
			SET EXCLUSIVE OFF
			SET SAFETY OFF
			SET EXACT OFF
			SET STATUS BAR ON
			SET SYSMENU OFF
			SET ENGINEBEHAVIOR 70
			_VFP.AUTOYIELD = .lAutoYield
			.cCOMPUTERNAME = GETENV("COMPUTERNAME")
			.cUSERNAME = GETENV("USERNAME")
			IF .lTestMode THEN
				.cSendTo = 'mbennett@fmiint.com'
				.cCC = ''
				.cLogFile = 'F:\UTIL\LF\LF_IBQS_RPT_LOG_TESTMODE.TXT'
			ENDIF
			.lLoggingIsOn = .lLoggingIsOn AND NOT EMPTY(.cLogFile)
		ENDWITH
	ENDFUNC


	FUNCTION DESTROY
		WITH THIS
			IF .lLoggingIsOn  THEN
				SET ALTERNATE OFF
				SET ALTERNATE TO
			ENDIF
		ENDWITH
		DODEFAULT()
	ENDFUNC


	FUNCTION MAIN

		WITH THIS
			LOCAL lnNumberOfErrors, lnAccountID, ldToday, lcFileDateTime, ld14DaysAgo
			LOCAL lcFiletoSaveAs, lcTemplateFile, lnTotQty, lcAssemblyFile, lcTabName, loWorkbookA, loSheetA
			LOCAL lnCurrentMonth, lnCurrentYear, lnLastPopulatedDate, i, ii, ldLastPopulatedDate
			
			PUBLIC goffice
						
			TRY
				lnNumberOfErrors = 0

				IF .lLoggingIsOn THEN
					SET ALTERNATE TO (.cLogFile) ADDITIVE
					SET ALTERNATE ON
				ENDIF

				.cSubject = .cProcessDesc + ' for ' + DTOC(.dToday)

				.TrackProgress(.cProcessDesc + ' started....', LOGIT+SENDIT+NOWAITIT)
				.TrackProgress('COMPUTERNAME = ' + .cCOMPUTERNAME, LOGIT+SENDIT)
				.TrackProgress('USERNAME = ' + .cUSERNAME, LOGIT+SENDIT)
				.TrackProgress('PROJECT = LF_IBQS_RPT', LOGIT+SENDIT)
				IF .lTestMode THEN
					.TrackProgress('                        TEST MODE                        ', LOGIT+SENDIT)
				ENDIF

				goffice = "N"
				
				* activate when we want usesqlinwo() to return .T.
				*PUBLIC gldor
				*gldor = .F.

				.TrackProgress('Processing Inventory Build Qty and Schedule Tab...', LOGIT+SENDIT+NOWAITIT)

				lnAccountID = .nAccountID

				ldToday = .dToday

				lcFileDateTime = PADL(YEAR(ldToday),4,"0") + PADL(MONTH(ldToday),2,"0") + PADL(DAY(ldToday),2,"0") + STRTRAN(SUBSTR(TIME(),1,5),":","")

				*lcOutputIBQS_CSV = "F:\UTIL\LF\REPORTS\IBQS_" + lcFileDateTime + ".CSV"
				.cFiletoSaveAs = "F:\UTIL\LF\REPORTS\IBQS_" + lcFileDateTime + ".XLS"
				lcFiletoSaveAs = .cFiletoSaveAs


				* copy the template file to the output file
				lcTemplateFile = "F:\UTIL\LF\TEMPLATES\LF_IBQS_ TEMPLATE.xls"

				COPY FILE(lcTemplateFile) TO (lcFiletoSaveAs)
*SET STEP ON 

				OPEN DATABASE F:\WHI\WHDATA\WH
				
*				goffice = "N"


				xsqlexec("select * from outwolog where mod='I' and accountid=6034","outwolog",,"wh")
				xsqlexec("select * from outship where mod='I' and accountid=6034","outship",,"wh")
				xsqlexec("select * from outdet where mod='I' and accountid=6034","outdet",,"wh")
				
				SELECT outwolog
				INDEX ON wo_num TAG wo_num
				set order to
								
				*!*					USE F:\WHI\WHDATA\outwolog IN 0 SHARED
				*!*					USE F:\WHI\WHDATA\outship IN 0 SHARED
				*!*					USE F:\WHI\WHDATA\outdet IN 0 SHARED
				
				xsqlexec("select * from inven where mod='I' and accountid=6034",,,"wh")
				xsqlexec("select * from indet where mod='I' and accountid=6034","indet",,"wh")
				xsqlexec("select * from inwolog where mod='I' and accountid=6034","inwolog",,"wh")
				
				*USE F:\WHI\WHDATA\pt IN 0 SHARED

				xsqlexec("select * from comphead where mod='I' and accountid=6034","comphead",,"wh")
				xsqlexec("select * from compon where mod='I' and accountid=6034","compon",,"wh")
				xsqlexec("select * from project where mod='I' and accountid=6034","project",,"wh")
				xsqlexec("select * from projdet where mod='I' and accountid=6034","projdet",,"wh")
				xsqlexec("select * from pt where mod='I' and accountid=6034","pt",,"wh")

				upcmastsql(lnAccountID)

				SELECT ACCOUNTID, STYLE, COLOR, DESCRIP FROM UPCMAST INTO CURSOR CURLFSTYLES WHERE ACCOUNTID = lnAccountID ORDER BY STYLE, COLOR
*set step on 
				*!*					SELECT CURLFSTYLES
				*!*					BROWSE

				IF USED('UPCMAST') THEN
					USE IN UPCMAST
				ENDIF

				*!*	* DATA FROM OUTWOLOG, OUTCOMP -- NOTONWIP = .T.?
				*!*	SELECT A.WO_DATE, A.WO_NUM, A.QUANTITY, A.PICKNPACK, A.PULLED, A.PULLEDDT, A.PULLEDTIME, B.STYLE,B.WO_NUM, B.ACCOUNTID, B.QTY ;
				*!*	FROM OUTWOLOG A	;
				*!*	LEFT OUTER JOIN OUTCOMP B ;
				*!*	ON B.OUTWOLOGID = A.OUTWOLOGID ;
				*!*	INTO CURSOR CUROUTWOLOG ;
				*!*	WHERE A.ACCOUNTID = lnAccountID AND A.SP AND A.NOTONWIP

				*!*	SELECT CUROUTWOLOG
				*!*	BROWSE
				*!*	SELECT CUROUTWOLOG
				*!*	COPY TO C:\A\SPOUTWOLOG.XLS XL5

				*!*	* uncompleted special projects

				*!*	select outdet.wo_num, units, style, color, id, pack, sum(totqty) as spqty ;
				*!*	  from outdet, project ;
				*!*	  where outdet.wo_num=project.wo_num ;
				*!*	  and project.accountid = lnAccountID ;
				*!*	  and empty(completeddt) ;
				*!*	  group by 1,2,3,4,5,6 ;
				*!*	  into cursor xsp
				*!*
				*!*	SELECT xsp
				*!*	BROWSE

				*!*	SELECT xsp
				*!*	COPY TO c:\a\lf_sp.xls xl5

				*!*	THROW

				* code below is from compon form, zzcreatereport(); only change was to hardcode the accountid to lnAccountID, i.e.6034
				SELECT ;
					comphead.ACCOUNTID, ;
					comphead.STYLE AS masterstyle, ;
					00000000000 AS canmake, ;
					comphead.compgrp, ;
					COMPON.STYLE, ;
					COMPON.compqty, ;
					COMPON.noinven, ;
					.F. AS sub, ;
					000000 AS availqty, ;
					000000 AS buildableqty, ;
					000000 AS totavailqty ;
					FROM comphead, COMPON ;
					WHERE comphead.compheadid=COMPON.compheadid ;
					AND comphead.ACCOUNTID = lnAccountID ;
					ORDER BY compgrp, masterstyle ;
					INTO CURSOR xrpt READWRITE

				SELECT xrpt
				SCAN
					SELECT inven
					LOCATE FOR units AND ACCOUNTID=xrpt.ACCOUNTID AND STYLE=xrpt.STYLE AND COLOR#"DAMAGE" AND availqty>0
					IF FOUND()
						REPLACE availqty WITH inven.availqty IN xrpt
					ENDIF
				ENDSCAN

				SELECT masterstyle FROM xrpt GROUP BY 1 INTO CURSOR xtemp
				SCAN
					xcanmake=99999
					SELECT xrpt
					SCAN FOR masterstyle=xtemp.masterstyle AND !noinven AND compqty#0
						xcanmake = MIN(xcanmake,INT(availqty/compqty))
					ENDSCAN
					REPLACE canmake WITH xcanmake IN xrpt FOR masterstyle=xtemp.masterstyle
				ENDSCAN

				SELECT * FROM xrpt INTO CURSOR xrpt2

				SELECT xrpt
				SCAN
					SELECT xrpt2
					LOCATE FOR masterstyle=xrpt.STYLE
					IF FOUND()
						REPLACE buildableqty WITH xrpt2.canmake, sub WITH .T. IN xrpt
					ENDIF
				ENDSCAN

				REPLACE ALL totavailqty WITH availqty+buildableqty IN xrpt

				SELECT masterstyle FROM xrpt WHERE buildableqty>0 GROUP BY 1 INTO CURSOR xtemp
				SCAN
					xcanmake=99999
					SELECT xrpt
					SCAN FOR masterstyle=xtemp.masterstyle AND !noinven AND compqty#0
						xcanmake = MIN(xcanmake,INT(totavailqty/compqty))
					ENDSCAN
					REPLACE canmake WITH xcanmake IN xrpt FOR masterstyle=xtemp.masterstyle
				ENDSCAN

				SELECT * FROM xrpt INTO CURSOR CURCOMPON ORDER BY STYLE

				*!*					SELECT CURCOMPON
				*!*					COPY TO C:\A\LFCOMPON.XLS XL5

				USE IN xrpt
				USE IN xtemp

				* below is from Lifefactory assemblies report; form = lifebuild.
				*select projectid, projno, custrefno, wo_num, putwonum from project where accountid=6034 and completeddt={} into cursor xtemp

				*!*					select projectid, projno, custrefno, wo_num, putwonum, queuedt, startdt, completebydt, completeddt from project ;
				*!*						where accountid = lnAccountID and completeddt = {} into cursor xtemp

				* per PG, instead of filtering by blank complete date, try reporting on start dates going back 2 weeks and see what we get.
				ld14DaysAgo = .dToday - 14

				SELECT projectid, projno, custrefno, wo_num, putwonum, queuedt, startdt, completebydt, completeddt FROM PROJECT ;
					WHERE ACCOUNTID = lnAccountID AND ((startdt >= ld14DaysAgo) OR (completeddt = {})) INTO CURSOR xtemp

				SELECT *, SPACE(254) AS jobdesc, SPACE(10) AS STYLE, 000000 AS totqty FROM xtemp WHERE .F. INTO CURSOR xrpt READWRITE

				SELECT xtemp
				*set step on
				SCAN
					SCATTER MEMVAR
					m.jobdesc=""
					SELECT projdet
					SCAN FOR projectid=xtemp.projectid
						IF !EMPTY(m.jobdesc)
							m.jobdesc=m.jobdesc+CRLF
						ENDIF
						m.jobdesc=m.jobdesc+TRIM(projdet.DESCRIPTION)
					ENDSCAN

					IF !EMPTY(m.putwonum)
						xsqlexec("select style, totqty from pl where wo_num="+transform(m.putwonum),,,"wh")
						scan 
							m.style=style
							m.totqty=totqty
							insert into xrpt from memvar
						endscan
					ELSE
						=SEEK(m.wo_num,"outwolog","wo_num")
						xsqlexec("select * from outcomp where outwologid="+transform(outwolog.outwologid),"outcomp",,"wh")
						scan
							m.style=STYLE
							m.totqty=qty
							INSERT INTO xrpt FROM MEMVAR
						ENDSCAN
					ENDIF
				ENDSCAN

				SELECT * FROM xrpt INTO CURSOR xrpt2 ORDER BY STYLE

				*!*					select xrpt2
				*!*					COPY TO C:\A\LFASSEMBLY.XLS XL5

				* join to COMPON to get avail qty
				*!*					SELECT B.MASTERSTYLE, B.STYLE, A.JOBDESC, NVL(B.AVAILQTY,-1) AS AVAILQTY, A.TOTQTY AS BUILDQTY, A.QUEUEDT, A.STARTDT, A.COMPLETEBYDT, A.COMPLETEDDT ;
				*!*					FROM xrpt A	;
				*!*					LEFT OUTER JOIN CURCOMPON B ;
				*!*					ON ALLTRIM(B.MASTERSTYLE) == ALLTRIM(A.STYLE) ;
				*!*					INTO CURSOR CURIBQS ;
				*!*					WHERE (NOT B.NOINVEN) ;
				*!*					ORDER BY B.MASTERSTYLE, B.STYLE ;
				*!*					READWRITE

				SELECT B.masterstyle, SPACE(10) AS COLOR, SPACE(50) AS DESCRIP, NVL(B.availqty,-1) AS availqty, A.totqty AS BUILDQTY, A.queuedt, A.startdt, A.completebydt, A.completeddt ;
					FROM xrpt A	;
					LEFT OUTER JOIN CURCOMPON B ;
					ON ALLTRIM(B.masterstyle) == ALLTRIM(A.STYLE) ;
					INTO CURSOR CURIBQS ;
					WHERE (NOT B.noinven) ;
					ORDER BY B.masterstyle, B.STYLE ;
					READWRITE

				* populate color and descrip
				SELECT CURIBQS
				SCAN
					SELECT CURLFSTYLES
					LOCATE FOR ALLTRIM(STYLE) == ALLTRIM(CURIBQS.masterstyle)
					IF FOUND() THEN
						REPLACE CURIBQS.COLOR WITH CURLFSTYLES.COLOR, CURIBQS.DESCRIP WITH CURLFSTYLES.DESCRIP IN CURLFSTYLES
					ENDIF
				ENDSCAN

				* populate Column D - Avail Qty differently than the way we got it already
				SELECT CURIBQS
				SCAN
					SELECT inven
					SUM totqty FOR (ACCOUNTID = lnAccountID) AND (ALLTRIM(STYLE) == ALLTRIM(CURIBQS.masterstyle)) TO lnTotQty
					LOCATE
					REPLACE CURIBQS.availqty WITH lnTotQty IN CURIBQS
				ENDSCAN

				* remove duplicate records per Chris 4/29/15
				SELECT DISTINCT * ;
					FROM CURIBQS ;
					INTO CURSOR CURIBQS2 ;
					ORDER BY masterstyle



				* populate copy of template file
				.oExcel = CREATEOBJECT("Excel.Application")
				.oExcel.VISIBLE = .F.
				.oExcel.displayalerts = .F.

				.oWorkbook = .oExcel.workbooks.OPEN(lcFiletoSaveAs)
				.osheet1 = .oWorkbook.worksheets[1]
				.osheet1.SELECT()
				xsheet = ".osheet1"
				i = 1
				ii=TRANSFORM(i)

				SELECT CURIBQS2
				SCAN
					i = i + 1
					ii=TRANSFORM(i)
					exceltext("A"+ii,CURIBQS2.masterstyle)
					exceltext("B"+ii,CURIBQS2.COLOR)
					exceltext("C"+ii,CURIBQS2.DESCRIP)
					exceltext("D"+ii,CURIBQS2.availqty)
					exceltext("E"+ii,CURIBQS2.BUILDQTY)
					exceltext("F"+ii,CURIBQS2.queuedt)
					exceltext("G"+ii,CURIBQS2.startdt)
					exceltext("H"+ii,CURIBQS2.completeddt)
				ENDSCAN


				.osheet1.RANGE("A2:H"+ii).SELECT

				.CrossHatchSelection()

				* "unselect" selection
				.osheet1.RANGE("I1").SELECT
				.oExcel.CutCopyMode = .F.

				.oWorkbook.SAVE()




				**** Material Inventory and Usage Report
				*!*	The 2nd Tab Material Inventory & Usage
				*!*	"	This is just the current inventory for components only (any style that does not start with a number)
				*!*	"	Inventory = Total Qty, Need for Builds = Allocated, Balance Inventory = Avail Qty
				*!*	"	Just change our names to the heading name Lifefactory wants

				.TrackProgress('Processing Material Inventory and Usage Tab...', LOGIT+SENDIT+NOWAITIT)

				SELECT STYLE, totqty, allocqty, availqty, SPACE(50) AS DESCRIP ;
					FROM inven ;
					INTO CURSOR CURMIAU ;
					WHERE (ACCOUNTID = lnAccountID) AND units AND (NOT totqty = 0) ;
					AND (NOT INLIST(UPPER(ALLTRIM(COLOR)),'DMG','DAMAGE','DAMAGED','DAMAGES')) ;
					AND (NOT INLIST(UPPER(ALLTRIM(ID)),'DMG','DAMAGE','DAMAGED','DAMAGES')) ;
					AND (NOT INLIST(UPPER(ALLTRIM(COLOR)),'AZ','EU')) ;
					AND ISALPHA(STYLE) ;
					ORDER BY STYLE ;
					READWRITE

				* populate descrip
				SELECT CURMIAU
				SCAN
					SELECT CURLFSTYLES
					LOCATE FOR ALLTRIM(STYLE) == ALLTRIM(CURMIAU.STYLE)
					IF FOUND() THEN
						REPLACE CURMIAU.DESCRIP WITH CURLFSTYLES.DESCRIP IN CURMIAU
					ENDIF
				ENDSCAN


				*!*	*!*					SELECT STYLE, COUNT(*) AS CNT ;
				*!*	*!*					FROM INVEN ;
				*!*	*!*					INTO CURSOR CURMIAU ;
				*!*	*!*					WHERE (accountid = 6034) AND UNITS AND (NOT totqty = 0) ;
				*!*	*!*					AND (NOT INLIST(UPPER(ALLTRIM(COLOR)),'DMG','DAMAGE','DAMAGED','DAMAGES')) ;
				*!*	*!*					AND (NOT INLIST(UPPER(ALLTRIM(ID)),'DMG','DAMAGE','DAMAGED','DAMAGES')) ;
				*!*	*!*					GROUP BY style ;
				*!*	*!*					ORDER BY style
				*!*
				*!*					SELECT CURMIAU
				*!*					BROWSE

				* move to tab 2 for this report
				.osheet1 = .oWorkbook.worksheets[2]
				.osheet1.SELECT()
				xsheet = ".osheet1"
				i = 1

				SELECT CURMIAU
				SCAN
					i = i + 1
					ii=TRANSFORM(i)
					exceltext("A"+ii,CURMIAU.STYLE)
					exceltext("B"+ii,CURMIAU.DESCRIP)
					exceltext("C"+ii,CURMIAU.totqty)
					exceltext("D"+ii,CURMIAU.allocqty)
					exceltext("E"+ii,CURMIAU.availqty)
				ENDSCAN


				.osheet1.RANGE("A2:E"+ii).SELECT

				.CrossHatchSelection()

				* "unselect" selection
				.osheet1.RANGE("F1").SELECT
				.oExcel.CutCopyMode = .F.

				.oWorkbook.SAVE()

				*!*					.InventoryAdjustmentReport()
				* 3rd tab


				*!*					.HeldOrderListReport()
				* 4th tab



				****    Order WIP Report
				* 5th Tab:
				* per Chris: Order WIP:  Work in Progress filtered to Non-SP in the format Lifefactory has requested

				.TrackProgress('Processing Order WIP Tab...', LOGIT+SENDIT+NOWAITIT)

				************************************************************************************************************
				* CODE BELOW IS BASED ON LIFEWIP.PRG by Darren

				SELECT ship_ref, SPACE(15) AS STATUS, wo_num, wo_date, consignee, cnee_ref, ship_via, ctnqty, qty AS unitqty, ;
					START, CANCEL, called, appt, appt_time, appt_num, apptremarks, del_date, pulled, picked, labeled, staged, truckloaddt, ptdate ;
					FROM outship ;
					WHERE (ACCOUNTID = lnAccountID) AND (EMPTYnul(del_date) OR del_date>DATE()-30) AND !notonwip ;
					INTO CURSOR CURORDERWIP READWRITE

				SELECT CURORDERWIP

				REPLACE ALL CURORDERWIP.STATUS WITH ;
					IIF(!pulled,"NOT PULLED",IIF(!EMPTYnul(del_date),"DELIVERED",IIF(!EMPTYnul(truckloaddt),"LOADED",IIF(!EMPTYnul(staged),"STAGED",IIF(!EMPTYnul(labeled),"LABELED",IIF(!EMPTYnul(picked),"PICKED","PULLED")))))) ;
					IN CURORDERWIP

				SELECT ship_ref, "UNALLOCATED" AS STATUS, consignee, cnee_ref, ship_via, qty AS unitqty, START, CANCEL, ptdate ;
					FROM PT ;
					WHERE (ACCOUNTID = lnAccountID) ;
					ORDER BY ship_ref INTO CURSOR xrpt2

				SELECT xrpt2
				SCAN
					SCATTER MEMVAR
					IF ISNULL(m.cancel) then
						m.cancel = {}
					ENDIF
					IF ISNULL(m.start) then
						m.start = {}
					ENDIF
					INSERT INTO CURORDERWIP FROM MEMVAR
				ENDSCAN
				************************************************************************************************************


				*!*	SELECT A.SHIP_REF, SPACE(20) AS STATUS, A.WO_NUM, A.CONSIGNEE, A.CNEE_REF, A.SHIP_VIA, ;
				*!*	(SELECT SUM(TOTQTY) FROM OUTDET WHERE UNITS AND (ACCOUNTID = lnAccountID) AND (OUTSHIPID = A.OUTSHIPID)) AS UNITQTY, ;
				*!*	A.PTDATE, A.WO_DATE, A.PULLED, A.STAGED, A.DEL_DATE, A.START, A.CANCEL ;
				*!*	FROM OUTSHIP A ;
				*!*	INTO CURSOR CURORDERWIP ;
				*!*	WHERE A.ACCOUNTID = lnAccountID ;
				*!*	AND EMPTY(A.DEL_DATE) ;
				*!*	AND !A.NOTONWIP ;
				*!*	AND !A.SP ;
				*!*	READWRITE

				*AND QTY#0 ;

*!*					SELECT CURORDERWIP
*!*					BROWSE

				* move to tab 5 for this report
				.osheet1 = .oWorkbook.worksheets[5]
				.osheet1.SELECT()
				xsheet = ".osheet1"
				i = 1

				SELECT CURORDERWIP
				SCAN
					i = i + 1
					ii=TRANSFORM(i)
					exceltext("A"+ii,CURORDERWIP.ship_ref)
					exceltext("B"+ii,CURORDERWIP.STATUS)
					exceltext("C"+ii,CURORDERWIP.wo_num)
					exceltext("D"+ii,CURORDERWIP.consignee)
					exceltext("E"+ii,CURORDERWIP.cnee_ref)
					exceltext("F"+ii,CURORDERWIP.ship_via)
					exceltext("G"+ii,CURORDERWIP.unitqty)
					exceltext("H"+ii,CURORDERWIP.ptdate)
					exceltext("I"+ii,CURORDERWIP.wo_date)
					exceltext("J"+ii,CURORDERWIP.pulled)
					exceltext("K"+ii,CURORDERWIP.staged)
					exceltext("L"+ii,CURORDERWIP.del_date)
					exceltext("M"+ii,CURORDERWIP.START)
					exceltext("N"+ii,CURORDERWIP.CANCEL)
				ENDSCAN 


				.osheet1.RANGE("A2:N"+ii).SELECT

				.CrossHatchSelection()

				* "unselect" selection
				.osheet1.RANGE("O1").SELECT
				.oExcel.CutCopyMode = .F.

				.oWorkbook.SAVE()






				*************************************************************************************************************************
				****	Current Assembly Status Report  -  copy from the current month tab in a spreadsheet that Chris Maintains
				* 6th tab

				.TrackProgress('Processing Current Assembly Status Tab...', LOGIT+SENDIT+NOWAITIT)


				* access target tab in report spreadsheet
				.osheet1 = .oWorkbook.worksheets( "CURRENT ASSEMBLY STATUS" )

				* Open Chris's spreadsheet
				lcAssemblyFile = 'S:\Chris Malcolm\Lifefactory Assembly Totals.xls'

				* construct tab name for current month
				lcTabName = PROPER(CMONTH(ldToday)) + " " + TRANSFORM(YEAR(ldToday))


				* open spreadsheet and access the right tab
				loWorkbookA = .oExcel.workbooks.OPEN(lcAssemblyFile)
				loSheetA = loWorkbookA.worksheets( lcTabName )

				* copy the assembly worksheet to the report spreadsheet
				loSheetA.RANGE("A1:Z200").COPY(.osheet1.RANGE("A1"))

				loWorkbookA.CLOSE()


				*************************************************************************************************************************
				*************************************************************************************************************************
				*************************************************************************************************************************
				*************************************************************************************************************************
				*************************************************************************************************************************
				* Current Shipment Status 
				* 7th tab
				
				* populate a cursor of holiday dates (weekdays that the CTT office is closed)
				* to be used by GetNonWeekendCountOfDays()
				
				CREATE CURSOR CURHOLIDAYS( holiday d )
				
				*!*	INSERT INTO CURHOLIDAYS (holiday) VALUES ({^2015-07-03})
				*!*	INSERT INTO CURHOLIDAYS (holiday) VALUES ({^2015-09-07})
				*!*	INSERT INTO CURHOLIDAYS (holiday) VALUES ({^2015-11-26})
				*!*	INSERT INTO CURHOLIDAYS (holiday) VALUES ({^2015-11-27})
				*!*	INSERT INTO CURHOLIDAYS (holiday) VALUES ({^2015-12-25})

				*!*	* 2016
				*!*	INSERT INTO CURHOLIDAYS (holiday) VALUES ({^2016-01-01})				
				*!*	INSERT INTO CURHOLIDAYS (holiday) VALUES ({^2016-02-15})
				*!*	INSERT INTO CURHOLIDAYS (holiday) VALUES ({^2016-03-25})
				*!*	INSERT INTO CURHOLIDAYS (holiday) VALUES ({^2016-05-30})
				*!*	INSERT INTO CURHOLIDAYS (holiday) VALUES ({^2016-07-04})
				*!*	INSERT INTO CURHOLIDAYS (holiday) VALUES ({^2016-09-05})
				*!*	INSERT INTO CURHOLIDAYS (holiday) VALUES ({^2016-11-24})
				*!*	INSERT INTO CURHOLIDAYS (holiday) VALUES ({^2016-11-25})
				*!*	INSERT INTO CURHOLIDAYS (holiday) VALUES ({^2016-12-25})
				*!*	INSERT INTO CURHOLIDAYS (holiday) VALUES ({^2016-12-26})

				*!*	* 2017
				*!*	INSERT INTO CURHOLIDAYS (holiday) VALUES ({^2017-01-02})				
				*!*	INSERT INTO CURHOLIDAYS (holiday) VALUES ({^2017-02-20})
				*!*	INSERT INTO CURHOLIDAYS (holiday) VALUES ({^2017-04-14})
				*!*	INSERT INTO CURHOLIDAYS (holiday) VALUES ({^2017-05-29})
				*!*	INSERT INTO CURHOLIDAYS (holiday) VALUES ({^2017-07-04})
				*!*	INSERT INTO CURHOLIDAYS (holiday) VALUES ({^2017-09-04})
				*!*	INSERT INTO CURHOLIDAYS (holiday) VALUES ({^2017-11-23})
				*!*	INSERT INTO CURHOLIDAYS (holiday) VALUES ({^2017-11-24})
				*!*	INSERT INTO CURHOLIDAYS (holiday) VALUES ({^2017-12-25})
				
				USE F:\AUTO\HOLIDAYS IN 0
				SELECT HOLIDAYS
				SCAN
					m.HOLIDAY = HOLIDAYS.ZDATE
					INSERT INTO CURHOLIDAYS FROM MEMVAR
				ENDSCAN
				
				IF USED('HOLIDAYS') THEN
					USE IN HOLIDAYS
				ENDIF
				
*!*	SELECT CURHOLIDAYS
*!*	BROWSE
*!*	THROW
				
				lnCurrentMonth = MONTH(ldToday)
				lnCurrentYear = YEAR(ldToday)
				
				IF USED('CURSHIPSTAT') THEN
					USE IN CURSHIPSTAT
				ENDIF
				IF USED('CURSHIPDATE') THEN
					USE IN CURSHIPDATE
				ENDIF

				USE F:\UTIL\LF\DATA\DAILYSHIP.DBF AGAIN IN 0 ALIAS DAILYSHIP
				
				* get latest populated date in current month, FOR USE IN CALCULATING DAILY AVERAGE later
				SELECT MAX(SHIPDATE) AS MAXDATE ;
				FROM DAILYSHIP ;
				INTO CURSOR CURSHIPDATE ;
				WHERE MONTH(SHIPDATE) = lnCurrentMonth ;
				AND YEAR(SHIPDATE) = lnCurrentYear ;
				AND NOT EMPTY(UPDATEDT)
				
				lnLastPopulatedDate = 1
				IF USED('CURSHIPDATE') AND (NOT EOF('CURSHIPDATE')) THEN
					IF NOT ISNULL(CURSHIPDATE.MAXDATE) THEN
						lnLastPopulatedDate = DAY(CURSHIPDATE.MAXDATE)
						ldLastPopulatedDate = CURSHIPDATE.MAXDATE
					ENDIF
				ENDIF
				.TrackProgress('ldLastPopulatedDate = ' + TRANSFORM(ldLastPopulatedDate), LOGIT+SENDIT+NOWAITIT)
				.TrackProgress('lnLastPopulatedDate = ' + TRANSFORM(lnLastPopulatedDate), LOGIT+SENDIT+NOWAITIT)

				
				
				SELECT SHIPDATE, DUNITQTY, PUNITQTY, LSUNITQTY, UWUNITQTY, BLOGUNITS, PCENTSHPD ;
				FROM DAILYSHIP ;
				INTO CURSOR CURSHIPSTAT ;
				WHERE MONTH(SHIPDATE) = lnCurrentMonth ;
				AND YEAR(SHIPDATE) = lnCurrentYear ;
				ORDER BY SHIPDATE


				* move to tab 7 for this report
				.osheet1 = .oWorkbook.worksheets[7]
				.osheet1.SELECT()
				
				.osheet1.RANGE("B2").VALUE = PROPER(CMONTH(ldToday)) + " " + TRANSFORM(YEAR(ldToday))
				
				xsheet = ".osheet1"
				i = 4

				SELECT CURSHIPSTAT
				SCAN
					i = i + 1
					ii=TRANSFORM(i)
					exceltext("A"+ii,CURSHIPSTAT.SHIPDATE)
					exceltext("B"+ii,CURSHIPSTAT.DUNITQTY)
					exceltext("C"+ii,CURSHIPSTAT.PUNITQTY)
					exceltext("D"+ii,CURSHIPSTAT.LSUNITQTY)
					exceltext("E"+ii,CURSHIPSTAT.UWUNITQTY)
					exceltext("F"+ii,CURSHIPSTAT.BLOGUNITS)
					exceltext("G"+ii,CURSHIPSTAT.PCENTSHPD)
				ENDSCAN 


				.osheet1.RANGE("A2:G"+ii).SELECT

				.CrossHatchSelection()

				* "unselect" selection
				.osheet1.RANGE("D1").SELECT
				.oExcel.CutCopyMode = .F.
				
				* POPULATE statistics grid
				* NOTE: this section modified 6/18/2015 MB to make non-backlog averages based on worked days (i.e. not counting weekends) per Chris Malcolm.
				
				LOCAL ldLastDayOfMonth, ldFirstDayOfMonth
				LOCAL lnDWeek1, lnPWeek1, lnLWeek1, lnUWeek1, lnBWeek1, lnBWeek1AVG
				LOCAL lnDWeek2, lnPWeek2, lnLWeek2, lnUWeek2, lnBWeek2, lnBWeek2AVG
				LOCAL lnDWeek3, lnPWeek3, lnLWeek3, lnUWeek3, lnBWeek3, lnBWeek3AVG
				LOCAL lnDWeek4, lnPWeek4, lnLWeek4, lnUWeek4, lnBWeek4, lnBWeek4AVG
				LOCAL ldWeek1, ldWeek2, ldWeek3, ldWeek4, lcYear, lcMonth
				LOCAL lnWorkedDayCountMTD
				LOCAL lnWorkedDayCountWeek1, lnWorkedDayCountWeek2, lnWorkedDayCountWeek3, lnWorkedDayCountWeek4
				
				ldFirstDayOfMonth = ldToday - DAY(ldToday) + 1
				ldLastDayOfMonth = GOMONTH(ldFirstDayOfMonth,+1) - 1
				.TrackProgress('ldFirstDayOfMonth = ' + TRANSFORM(ldFirstDayOfMonth), LOGIT+SENDIT)
				.TrackProgress('ldLastDayOfMonth = ' + TRANSFORM(ldLastDayOfMonth), LOGIT+SENDIT)
				
				lcYear = ALLTRIM(TRANSFORM(INT(YEAR(ldToday))))
				lcMonth = ALLTRIM(TRANSFORM(INT(MONTH(ldToday))))
				lcMonth = PADL(lcMonth,2,"0") 
				ldWeek1 = CTOD(lcMonth + "/01/" + lcYear)
				ldWeek2 = CTOD(lcMonth + "/08/" + lcYear)
				ldWeek3 = CTOD(lcMonth + "/15/" + lcYear)
				ldWeek4 = CTOD(lcMonth + "/22/" + lcYear)
				.TrackProgress('ldWeek1 = ' + TRANSFORM(ldWeek1), LOGIT+SENDIT)
				.TrackProgress('ldWeek2 = ' + TRANSFORM(ldWeek2), LOGIT+SENDIT)
				.TrackProgress('ldWeek3 = ' + TRANSFORM(ldWeek3), LOGIT+SENDIT)
				.TrackProgress('ldWeek4 = ' + TRANSFORM(ldWeek4), LOGIT+SENDIT)
				
				SELECT CURSHIPSTAT
				SUM DUNITQTY TO lnDWeek1 FOR BETWEEN(SHIPDATE,ldWeek1,ldWeek2-1)
				SUM DUNITQTY TO lnDWeek2 FOR BETWEEN(SHIPDATE,ldWeek2,ldWeek3-1)
				SUM DUNITQTY TO lnDWeek3 FOR BETWEEN(SHIPDATE,ldWeek3,ldWeek4-1)
				SUM DUNITQTY TO lnDWeek4 FOR BETWEEN(SHIPDATE,ldWeek4,ldLastDayOfMonth)
				
				SUM PUNITQTY TO lnPWeek1 FOR BETWEEN(SHIPDATE,ldWeek1,ldWeek2-1)
				SUM PUNITQTY TO lnPWeek2 FOR BETWEEN(SHIPDATE,ldWeek2,ldWeek3-1)
				SUM PUNITQTY TO lnPWeek3 FOR BETWEEN(SHIPDATE,ldWeek3,ldWeek4-1)
				SUM PUNITQTY TO lnPWeek4 FOR BETWEEN(SHIPDATE,ldWeek4,ldLastDayOfMonth)
				
				SUM LSUNITQTY TO lnLWeek1 FOR BETWEEN(SHIPDATE,ldWeek1,ldWeek2-1)
				SUM LSUNITQTY TO lnLWeek2 FOR BETWEEN(SHIPDATE,ldWeek2,ldWeek3-1)
				SUM LSUNITQTY TO lnLWeek3 FOR BETWEEN(SHIPDATE,ldWeek3,ldWeek4-1)
				SUM LSUNITQTY TO lnLWeek4 FOR BETWEEN(SHIPDATE,ldWeek4,ldLastDayOfMonth)
				
				SUM UWUNITQTY TO lnUWeek1 FOR BETWEEN(SHIPDATE,ldWeek1,ldWeek2-1)
				SUM UWUNITQTY TO lnUWeek2 FOR BETWEEN(SHIPDATE,ldWeek2,ldWeek3-1)
				SUM UWUNITQTY TO lnUWeek3 FOR BETWEEN(SHIPDATE,ldWeek3,ldWeek4-1)
				SUM UWUNITQTY TO lnUWeek4 FOR BETWEEN(SHIPDATE,ldWeek4,ldLastDayOfMonth)
				
				SUM BLOGUNITS TO lnBWeek1 FOR BETWEEN(SHIPDATE,ldWeek1,ldWeek2-1)
				SUM BLOGUNITS TO lnBWeek2 FOR BETWEEN(SHIPDATE,ldWeek2,ldWeek3-1)
				SUM BLOGUNITS TO lnBWeek3 FOR BETWEEN(SHIPDATE,ldWeek3,ldWeek4-1)
				SUM BLOGUNITS TO lnBWeek4 FOR BETWEEN(SHIPDATE,ldWeek4,ldLastDayOfMonth)
				

				lnWorkedDayCountMTD = .GetNonWeekendCountOfDays(ldWeek1, ldLastPopulatedDate)
				.TrackProgress('lnWorkedDayCountMTD = ' + TRANSFORM(lnWorkedDayCountMTD), LOGIT+SENDIT)

				SELECT CURSHIPSTAT
				
				exceltext("J6",lnPWeek1)
				exceltext("J7",lnPWeek2)
				exceltext("J8",lnPWeek3)
				exceltext("J9",lnPWeek4)
				exceltext("J11",lnPWeek1+lnPWeek2+lnPWeek3+lnPWeek4)
				exceltext("J12",(lnPWeek1+lnPWeek2+lnPWeek3+lnPWeek4)/4) 
				*exceltext("J13",(lnPWeek1+lnPWeek2+lnPWeek3+lnPWeek4)/lnLastPopulatedDate)  
				exceltext("J13",(lnPWeek1+lnPWeek2+lnPWeek3+lnPWeek4)/lnWorkedDayCountMTD)  
				
				exceltext("K6",lnLWeek1)
				exceltext("K7",lnLWeek2)
				exceltext("K8",lnLWeek3)
				exceltext("K9",lnLWeek4)
				exceltext("K11",lnLWeek1+lnLWeek2+lnLWeek3+lnLWeek4)
				exceltext("K12",(lnLWeek1+lnLWeek2+lnLWeek3+lnLWeek4)/4) 
				*exceltext("K13",(lnLWeek1+lnLWeek2+lnLWeek3+lnLWeek4)/lnLastPopulatedDate)  
				exceltext("K13",(lnLWeek1+lnLWeek2+lnLWeek3+lnLWeek4)/lnWorkedDayCountMTD)  
				
				exceltext("L6",lnDWeek1)
				exceltext("L7",lnDWeek2)
				exceltext("L8",lnDWeek3)
				exceltext("L9",lnDWeek4)
				exceltext("L11",lnDWeek1+lnDWeek2+lnDWeek3+lnDWeek4)
				exceltext("L12",(lnDWeek1+lnDWeek2+lnDWeek3+lnDWeek4)/4) 
				*exceltext("L13",(lnDWeek1+lnDWeek2+lnDWeek3+lnDWeek4)/lnLastPopulatedDate)  
				exceltext("L13",(lnDWeek1+lnDWeek2+lnDWeek3+lnDWeek4)/lnWorkedDayCountMTD)  
				
				
				SELECT CURSHIPSTAT
				AVERAGE BLOGUNITS TO lnBWeek1AVG FOR BETWEEN(SHIPDATE,ldWeek1,ldWeek2-1)
				AVERAGE BLOGUNITS TO lnBWeek2AVG FOR BETWEEN(SHIPDATE,ldWeek2,ldWeek3-1)
				AVERAGE BLOGUNITS TO lnBWeek3AVG FOR BETWEEN(SHIPDATE,ldWeek3,ldWeek4-1)
				AVERAGE BLOGUNITS TO lnBWeek4AVG FOR BETWEEN(SHIPDATE,ldWeek4,ldLastDayOfMonth)

				* below would make backlog averages based only on worked days, which for now is not what we want.
				*!*	lnWorkedDayCountWeek1 = .GetNonWeekendCountOfDays(ldWeek1,ldWeek2-1)
				*!*	lnWorkedDayCountWeek2 = .GetNonWeekendCountOfDays(ldWeek2,ldWeek3-1)
				*!*	lnWorkedDayCountWeek3 = .GetNonWeekendCountOfDays(ldWeek3,ldWeek4-1)
				*!*	lnWorkedDayCountWeek4 = .GetNonWeekendCountOfDays(ldWeek4,ldLastDayOfMonth)

				*!*	.TrackProgress('lnWorkedDayCountWeek1 = ' + TRANSFORM(lnWorkedDayCountWeek1), LOGIT+SENDIT)
				*!*	.TrackProgress('lnWorkedDayCountWeek2 = ' + TRANSFORM(lnWorkedDayCountWeek2), LOGIT+SENDIT)
				*!*	.TrackProgress('lnWorkedDayCountWeek3 = ' + TRANSFORM(lnWorkedDayCountWeek3), LOGIT+SENDIT)
				*!*	.TrackProgress('lnWorkedDayCountWeek4 = ' + TRANSFORM(lnWorkedDayCountWeek4), LOGIT+SENDIT)

				*!*	lnBWeek1AVG = lnBWeek1/lnWorkedDayCountWeek1
				*!*	lnBWeek2AVG = lnBWeek2/lnWorkedDayCountWeek2
				*!*	lnBWeek3AVG = lnBWeek3/lnWorkedDayCountWeek3
				*!*	lnBWeek4AVG = lnBWeek4/lnWorkedDayCountWeek4

				exceltext("M6",lnBWeek1AVG)
				exceltext("M7",lnBWeek2AVG)
				exceltext("M8",lnBWeek3AVG)
				exceltext("M9",lnBWeek4AVG)
				*exceltext("M11",lnBWeek1+lnBWeek2+lnBWeek3+lnBWeek4)
				exceltext("M12",(lnBWeek1AVG+lnBWeek2AVG+lnBWeek3AVG+lnBWeek4AVG)/4) 
				exceltext("M13",(lnBWeek1+lnBWeek2+lnBWeek3+lnBWeek4)/lnLastPopulatedDate)  
				*exceltext("M13",(lnBWeek1+lnBWeek2+lnBWeek3+lnBWeek4)/lnWorkedDayCountMTD)  

				.oWorkbook.SAVE()

				IF USED('CURSHIPSTAT') THEN
					USE IN CURSHIPSTAT
				ENDIF
				
				IF USED('DAILYSHIP') THEN
					USE IN DAILYSHIP
				ENDIF



				*************************************************************************************************************************
				* make the 1st tab active before saving
				.osheet1 = .oWorkbook.worksheets[1]
				.osheet1.SELECT()

				.oWorkbook.SAVE()

				WAIT CLEAR

				* this goes at end of processing 
				.oWorkbook.CLOSE()
				.oExcel.QUIT()
				.oExcel = NULL

				IF FILE(lcFiletoSaveAs) THEN
					.TrackProgress('Successfully created: ' + lcFiletoSaveAs, LOGIT+SENDIT)
					.cAttach = lcFiletoSaveAs
				ELSE
					.TrackProgress('!!! ERROR: there was a problem creating: ' + lcFiletoSaveAs, LOGIT+SENDIT)
				ENDIF


				.TrackProgress(.cProcessDesc + ' ended normally....', LOGIT+SENDIT+NOWAITIT)

			CATCH TO loError

				.TrackProgress('There was an error.',LOGIT+SENDIT)
				.TrackProgress('The Error # is: ' + TRANSFORM(loError.ERRORNO), LOGIT+SENDIT)
				.TrackProgress('The Error Message is: ' + TRANSFORM(loError.MESSAGE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred in Program: ' + TRANSFORM(loError.PROCEDURE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred at line #: ' + TRANSFORM(loError.LINENO), LOGIT+SENDIT)
				lnNumberOfErrors = lnNumberOfErrors + 1
				IF TYPE('.oExcel') = 'O' AND NOT ISNULL(.oExcel) THEN
					.oExcel.QUIT()
				ENDIF

			ENDTRY

			WAIT CLEAR
			***************** INTERNAL email results ******************************
			.TrackProgress('About to send status email.',LOGIT)
			.TrackProgress('==================================================================================================================', SENDIT)
			.TrackProgress('The logfile is: ' + .cLogFile, LOGIT+SENDIT)
			.TrackProgress(.cProcessDesc + ' started: ' + .cStartTime, LOGIT+SENDIT)
			.TrackProgress(.cProcessDesc + ' finished: ' + TTOC(DATETIME()), LOGIT+SENDIT)

			IF .lSendInternalEmailIsOn THEN
				* try to trap error from not having dartmail dll's registered on user's pc...
				TRY
					.cBodyText = .cTopBodyText + CRLF + .cBodyText

					DO FORM dartmail2 WITH .cSendTo,.cFrom,.cSubject,.cCC,.cAttach,.cBodyText,"A"
					.TrackProgress('Sent status email.',LOGIT)
				CATCH TO loError
					.TrackProgress('There was an error sending the status email.',LOGIT)
					.TrackProgress('The Error # is: ' + TRANSFORM(loError.ERRORNO), LOGIT)
					.TrackProgress('The Error Message is: ' + TRANSFORM(loError.MESSAGE), LOGIT)
					.TrackProgress('The Error occurred in Program: ' + TRANSFORM(loError.PROCEDURE), LOGIT)
					.TrackProgress('The Error occurred at line #: ' + TRANSFORM(loError.LINENO), LOGIT)
					lnNumberOfErrors = lnNumberOfErrors + 1
				ENDTRY

			ELSE
				.TrackProgress('Did not send status email: THIS.lSendInternalEmailIsOn = FALSE.',LOGIT)
			ENDIF

		ENDWITH
		RETURN
	ENDFUNC && main
	
	
	FUNCTION GetNonWeekendCountOfDays
		LPARAMETERS tdStartDate, tdEndDate

		LOCAL lnCount, ldDate
		lnCount = 0
		ldDate = tdStartDate
		
		DO WHILE ldDate <= tdEndDate
			* if a date is not on the weekend, AND IT IS NOT A HOLIDAY, add it to the count
			IF NOT INLIST(DOW(ldDate,1),1,7) THEN
				SELECT CURHOLIDAYS
				LOCATE FOR HOLIDAY = ldDate
				IF FOUND() THEN
					.TrackProgress('Holiday not counted when averaging: ' + TRANSFORM(ldDate),LOGIT+SENDIT)
				ELSE
					lnCount = lnCount + 1
				ENDIF
			ENDIF
			ldDate = ldDate + 1
		ENDDO

		RETURN lnCount
	ENDFUNC


	PROCEDURE CrossHatchSelection
		*!*		    .oExcel.Selection.Borders(xlDiagonalDown).LineStyle = xlNone
		*!*		    .oExcel.Selection.Borders(xlDiagonalUp).LineStyle = xlNone
		WITH .oExcel.SELECTION.BORDERS(xlEdgeLeft)
			.LineStyle = xlContinuous
			.ColorIndex = 0
			.TintAndShade = 0
			.Weight = xlThin
		ENDWITH
		WITH .oExcel.SELECTION.BORDERS(xlEdgeTop)
			.LineStyle = xlContinuous
			.ColorIndex = 0
			.TintAndShade = 0
			.Weight = xlThin
		ENDWITH
		WITH .oExcel.SELECTION.BORDERS(xlEdgeBottom)
			.LineStyle = xlContinuous
			.ColorIndex = 0
			.TintAndShade = 0
			.Weight = xlThin
		ENDWITH
		WITH .oExcel.SELECTION.BORDERS(xlEdgeRight)
			.LineStyle = xlContinuous
			.ColorIndex = 0
			.TintAndShade = 0
			.Weight = xlThin
		ENDWITH
		WITH .oExcel.SELECTION.BORDERS(xlInsideVertical)
			.LineStyle = xlContinuous
			.ColorIndex = 0
			.TintAndShade = 0
			.Weight = xlThin
		ENDWITH
		WITH .oExcel.SELECTION.BORDERS(xlInsideHorizontal)
			.LineStyle = xlContinuous
			.ColorIndex = 0
			.TintAndShade = 0
			.Weight = xlThin
		ENDWITH
	ENDPROC  && CrossHatchSelection


	PROCEDURE TrackProgress
		* do any combination of Wait Window, writing to logfile, and adding to body of email,
		* based on nFlags parameter.
		LPARAMETERS tcExpression, tnFlags
		WITH THIS
			IF BITAND(tnFlags,LOGIT) = LOGIT THEN
				IF .lLoggingIsOn THEN
					?
					? .cProcessName + "  " + TTOC(DATETIME()) + ": " + tcExpression
				ENDIF
			ENDIF
			IF .lWaitWindowIsOn THEN
				IF BITAND(tnFlags,NOWAITIT) = NOWAITIT THEN
					WAIT WINDOW tcExpression NOWAIT
				ENDIF
				IF BITAND(tnFlags,WAITIT) = WAITIT THEN
					WAIT WINDOW tcExpression TIMEOUT .nWaitWindowTimeout
				ENDIF
			ENDIF
			IF BITAND(tnFlags,SENDIT) = SENDIT THEN
				IF .lSendInternalEmailIsOn THEN
					.cBodyText = .cBodyText + tcExpression + CRLF + CRLF
				ENDIF
			ENDIF
		ENDWITH
	ENDPROC  &&  TrackProgress


ENDDEFINE
