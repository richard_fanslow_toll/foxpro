Close Data All
SET EXCLUSIVE OFF
SET SAFETY OFF
SET DELETED on
ctransfer='997-ARIAT'

lcDELIMITER = "*"
lcTranslateOption ="ART997"
delimchar = lcDELIMITER
lcTranOpt= lcTranslateOption

SET STEP ON 


Do m:\dev\prg\createx856

*******************************************************************

lcpath="F:\FTPUSERS\Ariat\997in\"
lcarchivepath="F:\FTPUSERS\Ariat\997in\archive\"

Cd &lcpath

lnNum = Adir(tarray,"F:\FTPUSERS\Ariat\997in\*.*")
SET STEP ON 
If lnNum = 0
     Wait Window At 10,10 "    No ARIAT 997's to import.............." Timeout 2
*!*	     Use F:\edirouting\ftpsetup Shared
*!*	     Locate For ftpsetup.transfer =  cTransfer
*!*	     If Found()
*!*	          Replace chkbusy With .F. For ftpsetup.transfer = cTransfer
*!*	     Endif
*!*	     NormalExit = .T.

 *    Throw
Endif

For thisfile = 1  To lnNum
* Archivename = Ttoc(Datetime(),1)
     xfile = lcpath+tarray[thisfile,1]+"."

*Xfile = lcPath+Allt(tarray[thisfile,1])
     !Attrib -R &xfile  && Removes read-only flag from file to allow deletion

     archivefile = lcarchivepath+tarray[thisfile,1]
     lcFilename = tarray[thisfile,1]
     Wait Window "Importing file: "+xfile Nowait
     If File(xfile)
* load the file into the 856 array
SET STEP ON 
          Select x856
          Zap
          Do m:\dev\prg\loadedifile With xfile,delimchar,lcTranOpt,"ART997" &&"NAUTICA"
          lProcessOK = .F.
         
          Do m:\dev\prg\ariat_997_bkdn With xfile  && do the bkdn and reconile the 997 into ACKDATA

          If lProcessOK
               Copy File &xfile To &archivefile
               If File(archivefile)
                    Delete File &xfile
               Endif
          Endif
     ENDIF
     
Next thisfile
SET STEP ON 
&&&& now run through and look for errors
Do m:\dev\prg\997_resend_cleanup WITH 6532

 Do m:\dev\prg\ariat_997_status_check  && do the bkdn and reconile the 997 into ACKDATA

