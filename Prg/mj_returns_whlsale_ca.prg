utilsetup("MJ_RETURNS_WHLSALE")
Set Exclusive Off
Set Deleted On
Set Talk Off
Set Safety Off 
*!*	WITH _SCREEN	
*!*		.AUTOCENTER = .T.
*!*		.WINDOWSTATE = 0
*!*		.BORDERSTYLE = 1
*!*		.WIDTH = 320
*!*		.HEIGHT = 210
*!*		.TOP = 290
*!*		.LEFT = 110
*!*		.CLOSABLE = .F.
*!*		.MAXBUTTON = .F.
*!*		.MINBUTTON = .F.
*!*		.SCROLLBARS = 0
*!*		.CAPTION = "MJ_RETURNS_WHLSALE"
*!*	ENDWITH	

*!*	If !Used("cmtrans")
*!*	  Use f:\3pl\data\cmtrans.Dbf In 0
*!*	ENDIF
*!*	If !Used("inwolog")
*!*	 use f:\whl\whdata\inwolog.Dbf In 0
*!*	ENDIF
*!*	If !Used("pl")
*!*	  use f:\whl\whdata\pl.Dbf In 0
*!*	ENDIF
*!*	If !Used("indet")
*!*	  use f:\whl\whdata\indet.Dbf In 0
*!*	ENDIF
*!*	If !Used("invenloc")
*!*	 use f:\whl\whdata\invenloc.Dbf In 0
*!*	ENDIF

 
*!*	*****Missing WO for returns (PL scanned) 
*!*	SELECT distinct trolly FROM cmtrans WHERE !wo_created AND scantime>{10/17/2014} and trolly!='ML' INTO CURSOR temp60
*!*	If Reccount() > 0
*!*	EXPORT TO S:\MarcJacobsData\TEMP\SCANNED_RETURNS_WO_NOT_CREATED XLS
*!*	  tsendto = "tmarg@fmiint.com,Cesar.Lozada@tollgroup.com,yenny.garcia@tollgroup.com"
*!*	  tattach = "S:\MarcJacobsData\TEMP\SCANNED_RETURNS_WO_NOT_CREATED.XLS"
*!*	  tcc =""
*!*	  tFrom ="Toll EDI System Operations <fmi-transload-ops@fmiint.com>"
*!*	  tmessage = "Scanned returns WO not created"
*!*	  tSubject = "Scanned returns WO not created"
*!*	  Do Form m:\dev\frm\dartmail2 With tsendto,tFrom,tSubject, tcc,tattach,tmessage  ,"A"
*!*	ELSE 
*!*	*!*	  tsendto = "tmarg@fmiint.com,yenny.garcia@tollgroup.com"
*!*	*!*	  tattach = ""
*!*	*!*	  tcc =""
*!*	*!*	  tFrom ="Toll EDI System Operations <fmi-transload-ops@fmiint.com>"
*!*	*!*	  tmessage = "MJ_RETURNS_WHLSALE  Wholesale returns scanned pallet wo not created_DO NOT EXIST "+Ttoc(Datetime())
*!*	*!*	  tSubject = "NO Wholesale returns scanned pallet wo not created EXIST "
*!*	*!*	  Do Form m:\dev\frm\dartmail2 With tsendto,tFrom,tSubject, tcc,tattach,tmessage  ,"A"
*!*	ENDIF




*!*	****Invalid PL entered
*!*	SELECT * FROM cmtrans where  TROLLY!='PL'  and trolly!='ML' AND scantime>{10/17/2014}  INTO CURSOR temp23
*!*	SELECT temp23
*!*	IF reccount() > 0
*!*	EXPORT TO S:\MarcJacobsData\TEMP\invalid_PL xls
*!*	  tsendto = "tmarg@fmiint.com,yenny.garcia@tollgroup.com"
*!*	  tattach = "S:\MarcJacobsData\TEMP\invalid_PL.xls"
*!*	  tcc =""
*!*	  tFrom ="Toll EDI System Operations <fmi-transload-ops@fmiint.com>"
*!*	  tmessage = "MJ RETURNS  Invalid PL in cmtrans"
*!*	  tSubject = "Invalid PL in cmtrans" 
*!*	  Do Form m:\dev\frm\dartmail2 With tsendto,tFrom,tSubject, tcc,tattach,tmessage  ,"A"
*!*	ELSE 

*!*	ENDIF


*!*	***Multiple accounts exist on a PL
*!*	SELECT distinct trolly,inwonum from cmtrans WHERE  !EMPTY(inwonum) and scantime>{10/17/2014}INTO CURSOR temp44
*!*	SELECT CNT(1) as count , trolly FROM temp44 GROUP BY trolly INTO CURSOR temp45 READWRITE
*!*	SELECT trolly FROM temp45 WHERE count!=1 INTO CURSOR temp46 READWRITE
*!*	IF reccount() > 0
*!*	EXPORT TO S:\MarcJacobsData\TEMP\Multiple_accts_exist_on_pl xls
*!*	  tsendto = "tmarg@fmiint.com,yenny.garcia@tollgroup.com"
*!*	  tattach = "S:\MarcJacobsData\TEMP\multplie_accts_exist_on_pl.xls"
*!*	  tcc =""
*!*	  tFrom ="Toll EDI System Operations <fmi-transload-ops@fmiint.com>"
*!*	  tmessage = "Multiple accounts exist on a PL, DO NOT create WO"
*!*	  tSubject = "Multiple accounts exist on a PL, DO NOT create WO" 
*!*	  Do Form m:\dev\frm\dartmail2 With tsendto,tFrom,tSubject, tcc,tattach,tmessage  ,"A"
*!*	ELSE 
*!*	  tsendto = "yenny.garcia@tollgroup.com"
*!*	  tattach = ""
*!*	  tcc =""
*!*	  tFrom ="Toll EDI System Operations <fmi-transload-ops@fmiint.com>"
*!*	  tmessage = "NO Invalid PL  account combo exists EXIST "+Ttoc(Datetime())
*!*	  tSubject = "NO Invalid PL  account combo exists" 
*!*	  Do Form m:\dev\frm\dartmail2 With tsendto,tFrom,tSubject, tcc,tattach,tmessage  ,"A"
*!*		ENDIF

*!*	***** identifies multiple RA's on a PL
*!*	SELECT distinct trolly,invloc from cmtrans where scantime>{10/17/2014} INTO CURSOR temp80
*!*	SELECT CNT(1) as count , trolly FROM temp80 GROUP BY trolly INTO CURSOR temp81 READWRITE
*!*	SELECT trolly FROM temp81 WHERE count!=1 INTO CURSOR temp82 READWRITE
*!*	IF reccount() > 0
*!*	EXPORT TO S:\MarcJacobsData\TEMP\multiple_RAs_exist_on_PL xls
*!*	  tsendto = "tmarg@fmiint.com,yenny.garcia@tollgroup.com"
*!*	  tattach = "S:\MarcJacobsData\TEMP\multiple_RAs_exist_on_PL.xls"
*!*	  tcc =""
*!*	  tFrom ="Toll EDI System Operations <fmi-transload-ops@fmiint.com>"
*!*	  tmessage = "Multiple RA's on a PL exist, DO NOT create WO"
*!*	  tSubject = "Multiple RA's on a PL exist, DO NOT create WO" 
*!*	  Do Form m:\dev\frm\dartmail2 With tsendto,tFrom,tSubject, tcc,tattach,tmessage  ,"A"
*!*	ELSE 
*!*	  tsendto = "yenny.garcia@tollgroup.com"
*!*	  tattach = ""
*!*	  tcc =""
*!*	  tFrom ="Toll EDI System Operations <fmi-transload-ops@fmiint.com>"
*!*	  tmessage = "NO Multiple RA's on a PL exist "+Ttoc(Datetime())
*!*	  tSubject = "NO Multiple RA's on a PL exist" 
*!*	  Do Form m:\dev\frm\dartmail2 With tsendto,tFrom,tSubject, tcc,tattach,tmessage  ,"A"
*!*		ENDIF


*!*	****identifies returns that need to be PUT AWAY to returns rack
*!*	SELECT accountid, style,color,id,whseloc, locqty, adddt FROM invenloc where WHSELOC='PL'  AND LOCQTY!=0 INTO CURSOR put1 READWRITE ORDER BY whseloc
*!*	SELECT put1
*!*	IF reccount() > 0
*!*	EXPORT TO S:\MarcJacobsData\TEMP\scanned_returns_to_put_away xls
*!*	  tsendto = "tmarg@fmiint.com,yenny.garcia@tollgroup.com"
*!*	  tattach = "S:\MarcJacobsData\TEMP\scanned_returns_to_put_away.xls"
*!*	  tcc =""
*!*	  tFrom ="Toll EDI System Operations <fmi-transload-ops@fmiint.com>"
*!*	  tmessage = "Scanned returns to be put away exist"
*!*	  tSubject = "Scanned returns to be put away exist" 
*!*	  Do Form m:\dev\frm\dartmail2 With tsendto,tFrom,tSubject, tcc,tattach,tmessage  ,"A"
*!*	ELSE 
*!*	*!*	  tsendto = "tmarg@fmiint.com,yenny.garcia@tollgroup.com"
*!*	*!*	  tattach = ""
*!*	*!*	  tcc =""
*!*	*!*	  tFrom ="Toll EDI System Operations <fmi-transload-ops@fmiint.com>"
*!*	*!*	  tmessage = "NO Scanned returns to be put away exist "+Ttoc(Datetime())
*!*	*!*	  tSubject = "NO Scanned returns to be put away exist" 
*!*	*!*	  Do Form m:\dev\frm\dartmail2 With tsendto,tFrom,tSubject, tcc,tattach,tmessage  ,"A"
*!*	ENDIF


*!*	****identifies returns that need to be PUT AWAY to general population
*!*	SELECT accountid, style,color,id,whseloc, locqty, adddt FROM invenloc where WHSELOC='RETPUT' AND INLIST(accountid,6303,6325) AND LOCQTY!=0 INTO CURSOR put2 READWRITE ORDER BY whseloc
*!*	SELECT put2
*!*	IF reccount() > 0
*!*	EXPORT TO S:\MarcJacobsData\TEMP\returns_from_racks_to_put_away xls
*!*	  tsendto = "tmarg@fmiint.com,yenny.garcia@tollgroup.com"
*!*	  tattach = "S:\MarcJacobsData\TEMP\returns_from_racks_to_put_away.xls"
*!*	  tcc =""
*!*	  tFrom ="Toll EDI System Operations <fmi-transload-ops@fmiint.com>"
*!*	  tmessage = "Returns from racks to be put away to general population exist"
*!*	  tSubject = "Returns from racks to be put away to general population exist" 
*!*	  Do Form m:\dev\frm\dartmail2 With tsendto,tFrom,tSubject, tcc,tattach,tmessage  ,"A"
*!*	ELSE 
*!*	*!*	  tsendto = "tmarg@fmiint.com,yenny.garcia@tollgroup.com"
*!*	*!*	  tattach = ""
*!*	*!*	  tcc =""
*!*	*!*	  tFrom ="Toll EDI System Operations <fmi-transload-ops@fmiint.com>"
*!*	*!*	  tmessage = "NO Returns from racks to be put away to general population exist "+Ttoc(Datetime())
*!*	*!*	  tSubject = "NO Returns from racks to be put away to general population exist" 
*!*	*!*	  Do Form m:\dev\frm\dartmail2 With tsendto,tFrom,tSubject, tcc,tattach,tmessage  ,"A"
*!*	ENDIF













 Close Data All
schedupdate()
_Screen.Caption=gscreencaption