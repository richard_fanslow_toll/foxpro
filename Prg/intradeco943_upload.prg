*!* INTRADECO 943_CREATE.PRG
PARAMETERS cOffice

PUBLIC lXfer943,cFilename,tsendto,tcc,tsendtoerr,tccerr,tsendtotest,tcctest,lIsError,nOrigAcctNum,nDivLoops,cInFolder,cWhse,nRecLU,lTesting
PUBLIC lDoMail,tfrom,tattach,cxErrMsg,NormalExit,nAcctNum,Acctname,lPick,lBrowFiles,cTransfer,cArchivePath,lcPath,cContainer,cAcct_ref,cMod,cWhseMod
PUBLIC lDoSQL,gMasterOffice
PUBLIC ARRAY a856(1)

CLOSE DATABASES ALL

IF VARTYPE(cOffice) # "C"
cOffice = "C"
ENDIF

lTesting = .F. && If TRUE, disables certain functions
lOverrideBusy = lTesting
lOverrideBusy = .T.

DO m:\dev\prg\_setvars WITH lTesting
ON ERROR THROW

*SET STEP ON

IF lTesting
	WAIT WINDOW "This is a "+IIF(lTesting,"TEST","PRODUCTION")+" upload" TIMEOUT 2
ENDIF

TRY
	DO m:\dev\prg\lookups
	tfrom = "Toll/FMI EDI Operations <toll-edi-ops@tollgroup.com>"
	tattach = " "
	cOffice = IIF(lTesting,"I",cOffice)
	cMod = IIF(cOffice = "C","5",cOffice)
	gOffice = cMod
	gMasterOffice = cOffice
	cWhseMod = LOWER("wh"+cMod)

	lDoSQL = .t.

	assignofficelocs()  && Added to begin trap of multiple locs on one 943, 06.26.2013, Joe

	lTestmail = lTesting
	lBrowFiles = lTesting
	lIsError = .F.
	lDoMail = .T.  && If .t., sends mail
	NormalExit = .F.
	lPick = .F.
	nDivLoops = 0
	nRecLU = 0
	cMasterComments = ""
	cContainer = ""
	cReference = ""

	nAcctNum = IIF(INLIST(cOffice,"M","C","X"),5154,6237)  && All NJ inbounds  are Ivory 6237
	cxErrMsg = ""
	cCustName = "INTRADECO"  && Customer Identifier (for folders, etc.)
	cMailName = "Intradeco"  && Proper name for eMail, etc.

	cTransfer = IIF(cOffice = "I","943-IVORY-NJ","943-INTRADECO-")
	SELECT 0
	USE F:\edirouting\ftpsetup
	LOCATE FOR ftpsetup.transfer = cTransfer
	IF ftpsetup.chkbusy AND !lTesting AND !lOverrideBusy
		WAIT WINDOW "Process is busy...exiting" TIMEOUT 2
		CLOSE DATA ALL
		NormalExit = .T.
		THROW
	ENDIF
	REPLACE chkbusy WITH .T.,trig_time WITH DATETIME()  FOR transfer = cTransfer
	USE IN ftpsetup

	setuprcvmail(cOffice)
	lcPath = IIF(lTesting,lcPath+"test\",lcPath)
	IF !lTesting
		DELETE FILE (lcPath+'*.*)
	ENDIF
	DELETE FILE (lcPath+'foxuser.*')
	CD [&lcPath]
	len1 = ADIR(ary1,"*.*")
	IF len1 = 0
		WAIT WINDOW "There are no 943 files to process...exiting" TIMEOUT 2
		CLOSE DATABASES ALL
		NormalExit = .T.
*		SET STEP ON
		THROW
	ENDIF
	WAIT WINDOW "There are "+TRANSFORM(len1)+" 943 files to process" TIMEOUT 2

	FOR kk = 1 TO len1
		cContainer = ""
		cAcct_ref = ""
		cFilename = ALLTRIM(ary1[kk,1])
		cArchiveFile  = (cArchivePath+cFilename)
		dFiledate = ary1[kk,3]
		xfile = lcPath+cFilename
		DO m:\dev\prg\createx856a
		loadedifile(xfile,"*","NOTILDE",cCustName)
		SELECT x856
		LOCATE
		LOCATE FOR x856.segment = "GS" AND x856.f1 = "AR"
		IF !FOUND()
			SET STEP ON
			cxErrMsg = cFilename+" is NOT a 943 document"
			errormail()
		ENDIF

		IF cOffice = "Y"  && If not a NJ 943, using "X" as a placeholder
			SELECT ALLTRIM(f2) AS whseloc ;
				FROM x856 ;
				WHERE x856.segment = "N9" AND x856.f1 = "LU" ;
				INTO CURSOR templu1
			SELECT whseloc,RECNO() AS recnum ;
				FROM templu1 ;
				INTO CURSOR templu2

			nRecLU = RECCOUNT('templu2')
			USE IN templu1
			USE IN templu2
		ELSE
			nRecLU = 1
		ENDIF

		SELECT x856
		WAIT WINDOW "This is a "+cWhse+" inbound file" TIMEOUT 1

		IF lTesting
			cUseFolder = "F:\WHP\WHDATA\"
		ELSE
			xReturn = "XXX"
			DO m:\dev\prg\wf_alt WITH cOffice,nAcctNum
			IF xReturn = "XXX"
				SET STEP ON
				cxErrMsg = "NO WHSE FOUND IN WMS"
				errormail()
				RETURN
			ENDIF
			cUseFolder = UPPER(xReturn)
		ENDIF

		useca("inrcv","wh")  && an updateable cursor with all existing records


*		IF USED('whgenpk')
*			USE IN whgenpk
*		ENDIF
		IF USED('inwolog')
			USE IN inwolog
		ENDIF
		IF USED('pl')
			USE IN pl
		ENDIF

*		USE (cUseFolder+"WHGENPK") IN 0 ALIAS whgenpk
*		USE (cUseFolder+"INRCV") IN 0 ALIAS inrcv

		useca("inwolog","wh")
		xsqlexec("select * from inwolog where .f.","xinwolog",,"wh")

		useca("pl","wh")
		xsqlexec("select * from pl where .f.","xpl",,"wh")

		xsqlexec("select * from account where inactive=0","account",,"qq")
		INDEX ON accountid TAG accountid
		SET ORDER TO

		SELECT xinwolog
		SCATTER MEMVAR BLANK
		=SEEK(nAcctNum,'account','accountid')
		m.Acctname = account.Acctname
		m.accountid = account.accountid
		SELECT pl
		STORE "FILE943*"+cFilename TO m.comments
		STORE m.comments+CHR(13)+"FILETIME*"+TTOC(DATETIME()) TO m.comments
		STORE 0 TO m.inwologid,m.plid,npo
		STORE "" TO m.echo,m.po
		STORE "FMI-PROC" TO m.addby
		STORE DATETIME() TO m.adddt
		STORE "INTRA943" TO m.addproc
		STORE DATE() TO m.wo_date,m.date_rcvd

		SELECT x856
		LOCATE
		m.inwologid = 0
		m.plid = 0
		SCAN
			DO CASE
				CASE INLIST(x856.segment,"ISA","GS","ST","IEA")
					LOOP

				CASE x856.segment = "W06"
					m.inwologid = m.inwologid +1
					m.acct_ref = ALLT(x856.f4)
					STORE m.acct_ref TO cAcct_ref
					m.comments = m.comments+CHR(13)+"PO*"+ALLT(x856.f6)

				CASE x856.segment = "N9" AND x856.f1 = "AR"
					m.reference = ALLT(x856.f2)

				CASE x856.segment = "N9" AND x856.f1 = "TRA"
					m.brokerref = ALLT(x856.f2)
					INSERT INTO xinwolog FROM MEMVAR

				CASE x856.segment = "W04"
					m.plid = m.plid + 1
					m.totqty = INT(VAL(x856.f1))
					REPLACE xinwolog.plinqty WITH xinwolog.plinqty+m.totqty IN xinwolog
					m.units = IIF(x856.f2="CT",.F.,.T.)
					m.echo = "UPC*"+ALLT(x856.f3)
					m.style = IIF(x856.f4 = "ST",ALLT(x856.f5),"")
					m.echo = m.echo+CHR(13)+"LINENUM*"+IIF(x856.f6 = "LN",ALLT(x856.f7),"")

				CASE x856.segment = "W20"
					m.pack = IIF(cOffice = "M",ALLT(x856.f1),"1")
					nPack = INT(VAL(m.pack))
					INSERT INTO xpl FROM MEMVAR
					REPLACE xinwolog.plunitsinqty WITH xinwolog.plinqty*nPack IN xinwolog

				CASE x856.segment = "W27"
					IF x856.f1= "O"
						cContainer = ALLT(x856.f6)+ALLT(x856.f7)
						REPLACE xinwolog.CONTAINER WITH cContainer IN xinwolog
					ELSE
						cReference = ALLT(x856.f7)
						IF EMPTY(cReference)
							cReference = ALLT(x856.f6)
						ENDIF
						REPLACE xinwolog.REFERENCE WITH cReference IN xinwolog
					ENDIF

			ENDCASE
		ENDSCAN

		IF lTesting
			SELECT xpl
			BROWSE FOR INLIST(STYLE,"WM6659DRTMM","WM6659DRTMXL","WM6659DRTML","WM6659DRTM2XL")
		ENDIF
		xplrollup()
		IF lTesting
			SELECT xinwolog
			DELETE FOR plinqty = 0
			LOCATE
			BROWSE
			SELECT xpl
			DELETE FOR totqty = 0
			LOCATE
			BROWSE
		ENDIF

		SELECT xinwolog
		LOCATE
		SCAN
			xAcct_Num =  xinwolog.accountid
			xAcct_ref = xinwolog.acct_ref
			xContainer = xinwolog.CONTAINER
			WAIT WINDOW "AT XINWOLOG RECORD "+ALLTRIM(STR(RECNO())) NOWAIT NOCLEAR
			csq1 = [select * from inwolog where accountid = ]+ALLTRIM(STR(xAcct_Num))+[ and container = ']+xContainer+[' and acct_ref = ']+xAcct_ref+[']
			xsqlexec(csq1,"i1",,"wh")
			IF RECCOUNT("i1")>0
				cWO = TRANSFORM(i1.wo_num)
				dupemail(cWO)
				SET STEP ON
				ninwologid = xinwolog.inwologid
				DELETE FOR xinwolog.inwologid = ninwologid
				SELECT xpl
				DELETE FOR xpl.inwologid = ninwologid
				SELECT xinwolog
				WAIT WINDOW "This set of values already exists in the INWOLOG table...looping" TIMEOUT 2
				LOOP
			ENDIF
		ENDSCAN
		nUploadCount = 0

		SELECT xinwolog
		LOCATE
		WAIT WINDOW "Currently in "+UPPER(IIF(lTesting,"test","production"))+" mode, base folder: "+cUseFolder TIMEOUT 2
		SET STEP ON
		SCAN  FOR !DELETED() && Scanning xinwolog here
			SCATTER MEMVAR MEMO
			cPLCtns = ALLTRIM(STR(m.plinqty))
			cPLUnits = ALLTRIM(STR(m.plunitsinqty))
			nWO_num  = dygenpk("wonum",cWhseMod)

			cWO_Num = ALLTRIM(STR(nWO_num))
			ninwologid = dygenpk("inwolog",cWhseMod)

			m.wo_num = nWO_num
			SET ORDER TO
			insertinto("inwolog","wh",.T.)
			nUploadCount = nUploadCount+1

			SELECT xpl
			SCAN FOR inwologid=xinwolog.inwologid
				SCATTER FIELDS EXCEPT inwologid MEMVAR MEMO
				m.wo_num = nWO_num
				m.inwologid = inwolog.inwologid
				insertinto("pl","wh",.T.)
			ENDSCAN
			goodmail()
		ENDSCAN

		IF nUploadCount = 0
			SET STEP ON
			IF FILE(cFilename)
				COPY FILE [&cFilename] TO [&cArchivefile]
				IF FILE(cArchiveFile) AND !lTesting
					DELETE FILE [&cFilename]
				ENDIF
			ENDIF
			EXIT
		ENDIF

*		SELECT whgenpk
		cUploadCount = ALLTRIM(STR(nUploadCount))
		WAIT WINDOW "Intradeco 943 file "+cFilename+" processed."+CHR(13)+"Header records loaded: "+cUploadCount TIMEOUT 1
		IF FILE(cFilename)
			COPY FILE [&cFilename] TO [&cArchivefile]
			IF FILE(cArchiveFile) AND !lTesting
				DELETE FILE [&cFilename]
			ENDIF
		ENDIF

		cWO_Num = TRANSFORM(nWO_num)
		SELECT xinwolog
		cPLCtns = TRANSFORM(xinwolog.plinqty)
		cPLUnits = TRANSFORM(xinwolog.plunitsinqty)

		SELECT inwolog

		IF !EMPTY(inwolog.CONTAINER)
			SELECT pl
			COUNT TO xnumskus FOR !units AND wo_num=inwolog.wo_num

			xsqlexec("select * from inrcv where container='"+inwolog.CONTAINER+"' and status#'CONFIRMED'","xinrcv",,"wh")
			SELECT xinrcv
			IF RECCOUNT() > 0
				xsqlexec("update inrcv set inwonum="+TRANSFORM(inwolog.wo_num)+" where container='"+inwolog.CONTAINER+"' and status#'CONFIRMED'",,,"wh")
			ELSE
				SELECT inrcv
				SCATTER MEMVAR MEMO BLANK
				m.mod = cMod
				m.office = cOffice
				m.inrcvid=dygenpk("inrcv","whall")  && new get the ID value
				m.inwonum=inwolog.wo_num
				m.accountid=inwolog.accountid
				m.Acctname=inwolog.Acctname
				m.container=inwolog.CONTAINER
				m.status="NOT AVAILABLE"
				m.skus=xnumskus
				m.cartons=inwolog.plinqty
				m.newdt=DATE()
				m.addfrom="IN"
				m.confirmdt=empty2nul(m.confirmdt)
				m.newdt=empty2nul(m.newdt)
				m.dydt=empty2nul(m.dydt)
				m.firstavaildt=empty2nul(m.firstavaildt)
				m.apptdt=empty2nul(m.apptdt)
				m.pickedup=empty2nul(m.pickedup)

				INSERT INTO inrcv FROM MEMVAR
				tu("inrcv")
			ENDIF

		ENDIF

	ENDFOR
	tu("inwolog")
	tu("pl")

	WAIT WINDOW "All Intradeco 943s processed...exiting" TIMEOUT 2

	closeftpsetup()

	NormalExit = .T.

CATCH TO oErr
	IF NormalExit = .F.
		ASSERT .F. MESSAGE "AT CATCH SECTION"
		SET STEP ON
		tsubject = cCustName+" 943 Upload Error ("+TRANSFORM(oErr.ERRORNO)+") at "+TTOC(DATETIME())
		tattach  = ""
		tsendto  = tsendtoerr
		tcc      = tccerr
		tmessage = cCustName+" 943 Upload Error"
		lcSourceMachine = SYS(0)
		lcSourceProgram = SYS(16)

		IF EMPTY(cxErrMsg)
			tmessage =tmessage+ "Try/CATCH Exeception Message:"+CHR(13)+;
				[  Error: ] + STR(oErr.ERRORNO) +CHR(13)+;
				[  Procedure: ] + oErr.PROCEDURE +CHR(13)+;
				[  LineNo: ] + STR(oErr.LINENO) +CHR(13)+;
				[  Message: ] + oErr.MESSAGE +CHR(13)+;
				[  Details: ] + oErr.DETAILS +CHR(13)+;
				[  StackLevel: ] + STR(oErr.STACKLEVEL) +CHR(13)+;
				[  LineContents: ] + oErr.LINECONTENTS+CHR(13)+;
				[  UserValue: ] + oErr.USERVALUE+CHR(13)+;
				[  Computer:  ] +lcSourceMachine+CHR(13)+;
				[  943 file:  ] +cFilename+CHR(13)+;
				[  Program:   ] +lcSourceProgram
		ELSE
			tmessage = tmessage + CHR(13) + CHR(13) + cxErrMsg
		ENDIF
		tattach  = ""
		tfrom    ="FMI EDI Processing Center <fmi-transload-ops@fmiint.com>"
		DO FORM m:\dev\FRM\dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
	ENDIF
FINALLY
	SET STATUS BAR ON
	DELETE FILE (lcPath+'*.*')
	CLOSE DATABASES ALL
	ON ERROR
ENDTRY


****************************
PROCEDURE goodmail
****************************
	tsubject = cMailName+" EDI FILE (943) Processed, Inv. WO"+cWO_Num
	tmessage = REPLICATE("-",90)+CHR(13)
	tmessage = tmessage+"Packinglist upload run from machine: "+SYS(0)+CHR(13)
	tmessage = tmessage+REPLICATE("-",90)+CHR(13)
	tmessage = tmessage+"Inbound Workorders created for "+ALLTRIM(m.Acctname)+", Loc: "+cWhse+CHR(13)
	tmessage = tmessage+IIF(!EMPTY(cContainer),"Container: "+cContainer+CHR(13),"AWB :"+cReference)+CHR(13)
	tmessage = tmessage+"Data from File : "+cFilename+CHR(13)
	tmessage = tmessage+REPLICATE("-",90)+CHR(13)
	tmessage = tmessage+"Acct. Ref: "+cAcct_ref+", WO# "+cWO_Num+SPACE(2)+"CTNS: "+cPLCtns+SPACE(2)+"UNITS: "+cPLUnits

	IF lDoMail
		DO FORM m:\dev\frm\dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
	ENDIF
ENDPROC

****************************
PROCEDURE dupemail
****************************
	PARAMETERS cWO
	tsubject = cMailName+" EDI FILE (943): Duplicate"
	tmessage = REPLICATE("-",90)+CHR(13)
	tmessage = tmessage+"Data from File : "+cFilename+CHR(13)
	tmessage = tmessage+"For Account ID: "+TRANSFORM(nAcctNum)
	tmessage = tmessage+ ", Container "+cContainer
	tmessage = tmessage+ ", Acct. Reference "+cAcct_ref+CHR(13)
	tmessage = tmessage+"already exists in the Inbound WO table, in Work Order "+cWO
	tmessage = tmessage+CHR(13)+REPLICATE("-",90)+CHR(13)

	IF lTesting
		SET STEP ON
	ENDIF

	IF lDoMail
		DO FORM m:\dev\frm\dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
	ENDIF
ENDPROC

****************************
PROCEDURE errormail
****************************
	SET STEP ON
	tsubject = cMailName+" EDI ERROR (943)"
	tsendto = tsendtoerr
	tcc = tccerr
	tmessage = "File: "+cFilename
	tmessage = tmessage+CHR(13)+cxErrMsg

	DO FORM m:\dev\frm\dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
	NormalExit = .T.
	SET STEP ON
	THROW
ENDPROC

****************************
PROCEDURE setuprcvmail
****************************
	PARAMETERS cOffice
*	ASSERT .f. MESSAGE "In Mail/Folder selection...debug"
	SELECT 0
	USE F:\3pl\DATA\mailmaster ALIAS mm
	LOCATE FOR mm.edi_type = "943" AND mm.GROUP = "INTRADECO" AND mm.office = cOffice
	IF !FOUND()
		SET STEP ON
		cxErrMsg = "Office/Loc/Acct not in Mailmaster!"
		errormail()
	ENDIF
	STORE TRIM(mm.basepath) TO lcPath
	STORE TRIM(mm.archpath) TO cArchivePath
	lUseAlt = mm.use_alt
	STORE IIF(lUseAlt,TRIM(mm.sendtoalt),TRIM(mm.sendto)) TO tsendto
	STORE IIF(lUseAlt,TRIM(mm.ccalt),TRIM(mm.cc)) TO tcc
	LOCATE
	LOCATE FOR mm.edi_type = "MISC" AND mm.taskname = "GENERAL"
	lUseAlt = mm.use_alt
	STORE IIF(lUseAlt,TRIM(mm.sendtoalt),TRIM(mm.sendto)) TO tsendtotest
	STORE IIF(lUseAlt,TRIM(mm.ccalt),TRIM(mm.cc)) TO tcctest
	STORE IIF(lUseAlt,TRIM(mm.sendtoalt),TRIM(mm.sendto)) TO tsendtoerr
	STORE IIF(lUseAlt,TRIM(mm.ccalt),TRIM(mm.cc)) TO tccerr
	IF lTesting OR lTestmail
		tsendto = tsendtotest
		tcc = tcctest
	ENDIF
	USE IN mm
ENDPROC

***************************************
PROCEDURE closeftpsetup
***************************************
	SELECT 0
	USE F:\edirouting\ftpsetup
	REPLACE chkbusy WITH .F. FOR transfer = cTransfer
	USE IN ftpsetup
ENDPROC

***************************************
PROCEDURE assignofficelocs
***************************************
	cOffice=IIF(INLIST(cOffice,"1","2","7"),"C",cOffice)
	cWhseLoc = ICASE(cOffice = "M","FL",cOffice = "I","NJ",cOffice = "X","CR","CA")
	cWhse = ICASE(cOffice = "M","MIAMI",cOffice = "I","CARTERET",cOffice = "X","CARSON","SAN PEDRO")
ENDPROC

***************************************
PROCEDURE xplrollup  && Added 02.20.2014, Joe, per Chris M.
***************************************
	SET STEP ON
	SELECT xpl
	SELECT inwologid,STYLE,COUNT(STYLE) AS cnt1 FROM xpl GROUP BY 1,2 INTO CURSOR tempxpl
	SELECT tempxpl
	LOCATE
	IF lTesting
		BROWSE FOR cnt1>1
	ENDIF
	LOCATE
	SCAN FOR tempxpl.cnt1>1
		cStyle1 = ALLTRIM(tempxpl.STYLE)
		nIWID = tempxpl.inwologid
		SELECT xpl
		LOCATE FOR inwologid = nIWID AND STYLE = cStyle1
		nrec1 = RECNO()
		REPLACE xpl.ECHO WITH xpl.ECHO+CHR(13)+"QTY*"+ALLT(STR(xpl.totqty)) IN xpl
		SCAN FOR !DELETED() AND inwologid = nIWID AND STYLE = cStyle1 AND RECNO()>nrec1
			nrec2 = RECNO()
			SCATTER FIELDS ECHO MEMO MEMVAR
			nTotqty = xpl.totqty
			GO nrec1
			IF !"MULTILINES"$xpl.ECHO
				REPLACE xpl.ECHO WITH "MULTILINES"+CHR(13)+xpl.ECHO IN xpl
			ENDIF
			REPLACE xpl.ECHO WITH xpl.ECHO+CHR(13)+m.echo IN xpl
			REPLACE xpl.ECHO WITH xpl.ECHO+CHR(13)+"QTY*"+ALLT(STR(nTotqty)) IN xpl
			REPLACE xpl.totqty WITH xpl.totqty+nTotqty IN xpl
			GO nrec2
			DELETE NEXT 1 IN xpl
		ENDSCAN
		STORE "" TO m.echo
	ENDSCAN
ENDPROC
