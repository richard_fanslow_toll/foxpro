* Sends Ariat the USPS postage balance on the 1st and 15th

utilsetup("ARIATPOSTAGE")

xx=filetostr("o:\elspostage.log")
xx=val(right(strtran(xx,":",""),8))
xto="Janine.Grossmann@Ariat.Com; Cori.DalPorto@Ariat.Com; Gerri.Cooper@Ariat.Com"
*xto="darren.young@tollgroup.com"
xcc="Tony.Sutherland@tollgroup.com; Samantha.Mccurdy@tollgroup.com; Cheri.Foster@tollgroup.com"

emailx(xto,xcc,"Remaining Ariat USPS postage is "+transform(xx))

schedupdate()

on error
