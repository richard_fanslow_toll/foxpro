**dailyStats.prg
**
**print wo and scan details for all daily sorter activity
**
Parameters lAuto, nSysId, cSysDesc, dStartDt, dEndDt, cAcctFilter, cAcctName, lEmail, cEmailTo

If lAuto
  USE f:\transload\tldata\barcodes In 0
  Use f:\wo\wodata\detail In 0
EndIf

Select wo_num from barcodes where scantm >= dStartDt and scantm < dEndDt+1 and sysid = nSysId ;
	group by wo_num into cursor csrWonums

Select d.* from csrWonums c left join detail d on d.wo_num = c.wo_num into cursor csrDailyStats

Select c.*, Sum(Iif(!b.duplicate, 1, 0)) as bcCnt, Sum(Iif(b.manualScan, 1, 0)) as msCnt, ;
	   Min(scanTm) as startTm, Max(scanTm) as endTm ;
	from csrDailyStats c left join barcodes b on b.detailid = c.detailid ;
	group by c.detailid order by c.wo_num into cursor csrDailyStats



*****Start Report Writing
SET MARGIN TO 0

lsTimeString = "Run Date: "+Dtoc(Date())+"  Run Time: "+Ttoc(Datetime(),2)+"     "+chr(13)+chr(13)

Store "" TO lsCompanyName

lsReportName = "Daily Conveyor Statistics By Work Order"
lsGroupByString = lsReportName+Chr(13)+"Account: "+Iif(Empty(cAcctFilter), "All Accounts", cAcctName)+;
	Chr(13)+"Dates: "+Dtoc(dStartDt)+" - "+Dtoc(dEndDt)

CREATE CURSOR c_Report (cr_SortBy I, cr_AcctName C(50), cr_GroupBy C(250), ;
						cr_CO_Name C(50), cr_RptName C(200), ;
						cr_FooterName C(200), cr_Header M, cr_Details M)

lsHeader = chr(13)+"W/O No.  Account          PO No.      Style            Del Loc            P/L   Rcvd   Ship   Scan   Manual   Diff"+chr(13)

**insert header info
INSERT INTO c_Report (cr_SortBy, cr_GroupBy, cr_AcctName, cr_Header, ;
					  cr_CO_Name, cr_RptName, cr_FooterName, cr_Details) ;
	   VALUES (0, ;
			   lsTimeString+lsGroupByString, ;
			   "", ;
			   lsHeader, ;
			   lsCompanyName, ;
			   lsReportName, ;
			   lsGroupByString, ;
			   "")

**insert detail records
Select csrDailyStats
Do while !Eof()
	Store 0 to nRcvd, nScan
	Store Dtot({}) to tStartTm, tEndTm

	Store wo_num to nWonum
	Do while wo_num = nWonum
		lsDetails = padr(Alltrim(Str(wo_num)),7)+Space(2)+Left(&cacctnamefld,15)+Space(2)+Left(po_num, 15)+space(2)+Left(style, 15)+;
					space(2)+Iif(IsNull(delloc),Space(10),Padr(delloc,10))+Space(7)+Str(pl_qty,5,0)+;
					Space(2)+Str(rcv_qty,5,0)+Space(2)+Str(quantity,5,0)+Space(2)+Str(bcCnt,5,0)+Space(4)+Str(msCnt,5,0)+Space(3)+Str(bcCnt-rcv_qty,4)

		INSERT INTO c_Report (cr_SortBy, cr_GroupBy, cr_AcctName, cr_CO_Name, ;
							  cr_RptName, cr_FooterName, cr_Header, cr_Details) ;
			   VALUES (1, lsTimeString+lsGroupByString, "", lsCompanyName, ;
					   lsReportName, lsGroupByString, lsHeader, lsDetails)

		nRcvd = nRcvd+rcv_qty
		nScan = nScan+bcCnt

		If !IsNull(startTm) &&no scans for this po/style (detailId)
			tStartTm = Iif(Empty(tStartTm), startTm, Min(tStartTm, startTm))
			tEndTm = Max(tEndTm, endTm)
		EndIf
		Skip
	EndDo

	lsDetails = REPLICATE("-", 10)
	Insert INTO c_Report (cr_SortBy, cr_GroupBy, cr_AcctName, cr_CO_Name, ;
						  cr_RptName, cr_FooterName, cr_Header, cr_Details) ;
		   VALUES (1, lsTimeString+lsGroupByString, "", lsCompanyName, ;
				   lsReportName, lsGroupByString, lsHeader, lsDetails)

	lsDetails = Space(9)+"Scanned: "+Ttoc(tStartTm)+" - "+Ttoc(tEndTm)+Space(17)+Str(nRcvd,6,0)+Space(8)+Str(nScan,6,0)+Space(12)+Str(nScan-nRcvd,4)+Chr(13)+Chr(13)
	INSERT INTO c_Report (cr_SortBy, cr_GroupBy, cr_AcctName, cr_CO_Name, ;
						  cr_RptName, cr_FooterName, cr_Header, cr_Details) ;
		   VALUES (1, lsTimeString+lsGroupByString, "", lsCompanyName, ;
				   lsReportName, lsGroupByString, lsHeader, lsDetails)
EndDo

lsDetails = REPLICATE("-", 10)
Insert INTO c_Report (cr_SortBy, cr_GroupBy, cr_AcctName, cr_CO_Name, ;
					  cr_RptName, cr_FooterName, cr_Header, cr_Details) ;
	   VALUES (1, lsTimeString+lsGroupByString, "", lsCompanyName, ;
			   lsReportName, lsGroupByString, lsHeader, lsDetails)
Insert INTO c_Report (cr_SortBy, cr_GroupBy, cr_AcctName, cr_CO_Name, ;
					  cr_RptName, cr_FooterName, cr_Header, cr_Details) ;
	   VALUES (1, lsTimeString+lsGroupByString, "", lsCompanyName, ;
			   lsReportName, lsGroupByString, lsHeader, lsDetails)

Select csrDailyStats
Sum rcv_qty, bcCnt to nRcvdTot, nScanTot

lsDetails = "Grand Totals:"+Space(63)+Str(nRcvdTot,8,0)+Space(6)+Str(nScanTot,8,0)+Space(11)+Str(nScanTot-nRcvdTot,5)
INSERT INTO c_Report (cr_SortBy, cr_GroupBy, cr_AcctName, cr_CO_Name, ;
					  cr_RptName, cr_FooterName, cr_Header, cr_Details) ;
	   VALUES (1, lsTimeString+lsGroupByString, "", lsCompanyName, ;
			   lsReportName, lsGroupByString, lsHeader, lsDetails)


cFileName = "det"+SUBSTR(SYS(2015), 3, 10)


&&print report
SELECT c_Report

If !lAuto
	Set Printer to default
	REPORT FORM PORTRAIT PREVIEW
EndIf


If lEmail
	cOutFileName = "DailyStats"
	lFileCreated = .f.
	DO frxToPdf with .t., "portrait", "h:\pdf\", cOutFileName

	If !lFileCreated
		If !lAuto
			strMsg = "PDF was not created. File will not be sent."
			=MessageBox(strMsg, 16, gcMsgTitle)
		EndIf
	Else
		cTo = cEmailTo
		cCc = ' '
		cSubject = "Daily Sorter Stats By W/O: "+Dtoc(dStartDt)+"-"+Dtoc(dEndDt)+" ("+cSysDesc+")"
		cAttach = "h:\pdf\"+cOutFileName
		cFrom ="TGF WMS Operations <fmicorporate@fmiint.com>"
		cMessage = cSubject
		DO FORM dartmail WITH cTo,cFrom,cSubject,cCc,cAttach,cMessage,Iif(lAuto, "A", "")
	EndIf
EndIf

If lAuto
  Close Databases all
EndIf
