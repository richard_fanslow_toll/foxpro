set step on

public xfile

lcPath ="f:\ftpusers\kg-ca\940\"

lnNum = ADIR(tarray,lcPath+"*.*")
IF lnNum = 0
  WAIT WINDOW "No files found...exiting" TIMEOUT 2
  CLOSE DATA ALL
  schedupdate()
  _SCREEN.CAPTION="Kurt Geiger pick ticket Upload.............."
  ON ERROR
  RETURN
Else
 For thisfile = 1  To lnNum
  cfilename = ALLTRIM(tarray[thisfile,1])
  xfile = lcpath+cfilename
  loadfile()
  next 
Endif


******************************************************************
Procedure loadfile 
Create Cursor x940 (;
whseid char(4),;
ship_ref char(9),;
createdt char(8),;
storenum char(4),;
style char(10),;
prodname char(30),;
color char(10),;
material char(30),;
size char(6),;
upc char(13),;
qty char(7),;
ponum char(30),;
custpt char(10),;
custid char(10),;
custname char(40),;
branch char(10),;
braddr1 char(40),;
braddr2 char(40),;
braddr3 char(40),;
braddr4 char(40),;
brcntry char(40),;
brzip char(10))

Select x940
Set Step On 

append from &xfile Type csv
endproc