*!* m:\dev\prg\ariat_bkdn.prg
tFrom ="Toll EDI Operations <toll-edi-ops@tollgroup.com>"
CLEAR
lCheckStyle = .T.
lPrepack = .F.
m.color=""
cUCCNumber = ""
cUsePack = ""
lcUCCSeed = 1

goffice="K"
gmasteroffice="K"

*close data all
If !lTesting
  useca("ackdata","wh")
Endif 

CREATE CURSOR tempsr1 (ship_ref c(20))
SELECT x856
LOCATE FOR x856.segment = "ISA"
cisa_num = ALLTRIM(x856.f13)

SCAN FOR x856.segment = "W05"
	m.ship_ref = ALLTRIM(x856.f2)
	INSERT INTO tempsr1 FROM MEMVAR
ENDSCAN
cSegCnt = ALLTRIM(STR(RECCOUNT('tempsr1')))
SELECT ship_ref,COUNT(ship_ref) AS cnt1 FROM tempsr1 GROUP BY ship_ref  INTO CURSOR tempsr2 HAVING cnt1 > 1
USE IN tempsr1
SELECT tempsr2

LOCATE
IF !EOF()
	COPY TO h:\fox\ariatdups.xls TYPE xl5
	COPY FILE [&xfile] TO ("F:\FTPUSERS\Ariat\940IN\hold\"+cFilename)
	DELETE FILE [&xfile]
	cErrMsg = "Duplicate PT #'s in 940...needs resending"
	THROW
ENDIF


*COUNT FOR TRIM(segment) = "ST" TO nSegCnt
*cSegCnt = ALLTRIM(STR(nSegCnt))
LOCATE

ptctr = 0

WAIT WINDOW "Now in 940 Breakdown" &waittime

WAIT Window "There are "+cSegCnt+" P/Ts in this file" &waittime
SELECT x856
LOCATE

*!* Constants
m.userid = "ARI940PROC"
m.insdttm = DATETIME()
m.accountid = 6532
nacct_num = 6532

m.acct_name = "ARIAT"
STORE nacct_num TO m.acct_num
m.careof = " "
m.sf_addr1 = "3242 WHIPPLE RD"
m.sf_csz = "UNION CITY, CA 94587"
*!* End constants

*!*	IF !USED("isa_num")
*!*		IF lTesting
*!*			USE F:\3pl\test\isa_num IN 0 ALIAS isa_num
*!*		ELSE
*!*			USE F:\3pl\DATA\isa_num IN 0 ALIAS isa_num
*!*		ENDIF
*!*	ENDIF

*!*	IF !USED("uploaddet")
*!*		IF lTesting
*!*			USE F:\ediwhse\test\uploaddet IN 0 ALIAS uploaddet
*!*		ELSE
*!*			USE F:\ediwhse\DATA\uploaddet IN 0 SHARED ALIAS uploaddet
*!*		ENDIF
*!*	ENDIF

SELECT xpt
DELETE ALL
SELECT xptdet
DELETE ALL

STORE 0 TO pt_total_eaches,pt_total_cartons,nCtnCount,nptid,nptdetid,m.weight
STORE 1 TO m.carton_id

SELECT x856
SET FILTER TO
LOCATE

IF lTesting
*  SET STEP ON
ENDIF

STORE "" TO cisa_num

lcCurrentGroupNum = ""

WAIT WINDOW "NOW IN INITAL HEADER BREAKDOWN" &waittime
WAIT WINDOW "NOW IN INITAL HEADER BREAKDOWN" NOWAIT
nptid = 0
DO WHILE !EOF("x856")
	cSegment = TRIM(x856.segment)
	cCode = TRIM(x856.f1)

	DO CASE
		CASE TRIM(x856.segment) = "ISA"
			cisa_num = ALLTRIM(x856.f13)
			WAIT WINDOW "This is a "+cProperName+" upload." &waittime
			ptctr = 0
			SELECT x856
			SKIP
			LOOP

		CASE TRIM(x856.segment) = "GS"
			cgs_num = ALLTRIM(x856.f6)
			lcCurrentGroupNum = ALLTRIM(x856.f6)
*!*				SELECT isa_num
*!*				LOCATE FOR isa_num.isa_num = cisa_num ;
*!*					AND isa_num.acct_num = nacct_num
*!*				IF !FOUND()
*!*					APPEND BLANK
*!*					REPLACE isa_num.isa_num WITH cisa_num IN isa_num
*!*					REPLACE isa_num.acct_num WITH nacct_num IN isa_num
*!*					REPLACE isa_num.office WITH cOffice IN isa_num
*!*					REPLACE isa_num.TYPE WITH "940" IN isa_num
*!*					REPLACE isa_num.ack WITH .F. IN isa_num
*!*					REPLACE isa_num.dateloaded WITH DATETIME() IN isa_num
*!*					REPLACE isa_num.filename WITH UPPER(TRIM(xfile)) IN isa_num
*!*				ENDIF
			SELECT x856
			SKIP
			LOOP

		CASE TRIM(x856.segment) = "ST" AND TRIM(x856.f1) = "940"
			IF !lTesting
				lnTryAck = 1
				llACKUpdate = .F.
				lntry =1

				m.groupnum=lcCurrentGroupNum
				m.isanum=cisa_num
				m.transnum=x856.f2
				m.edicode="OW"
				m.accountid=6532
				m.loaddt=date()
				m.loadtime=datetime()
				m.filename=xfile
			If !ltesting
       	insertinto("ackdata","wh",.t.)
      endif
				
				IF lnTryAck=10
					cErrorMessage ="Error updating ACKDATA......."
					THROW
				ENDIF
			ENDIF
			SELECT x856
			SKIP
			LOOP

		CASE TRIM(x856.segment) = "W05"  && new shipment
			lnShipInsCtr = 1
			nCtnCount = 0
			m.style = ""
      If !lTesting
  			REPLACE ackdata.ship_ref WITH ALLTRIM(x856.f2) IN ackdata
      endif
			SELECT xpt
			nDetCnt = 0
			APPEND BLANK

			REPLACE xpt.shipins WITH "FILENAME*"+cFilename IN xpt
			REPLACE xpt.qty WITH 0 IN xpt
			REPLACE xpt.origqty WITH 0 IN xpt
			STORE 0 TO pt_total_eaches
			STORE 0 TO pt_total_cartons
			nptid = nptid +1
			WAIT WINDOW AT 10,10 "Now processing PT# "+TRANSFORM(nptid)+" Number "+CHR(13)+TRANSFORM(ptctr)+" of "+cSegCnt NOWAIT

			REPLACE xpt.ptid       WITH nptid IN xpt
			REPLACE xpt.accountid  WITH nacct_num IN xpt
*    REPLACE xpt.office     WITH cOffice && "N"
			REPLACE xpt.ptdate     WITH DATE() IN xpt
			m.ship_ref = ALLTRIM(x856.f2)
*      m.ship_ref = ALLTRIM(x856.f2)+"A"
			m.cnee_ref = ALLTRIM(x856.f3)
			cship_ref = ALLTRIM(x856.f2)
			REPLACE xpt.ship_ref   WITH m.ship_ref IN xpt  && Pick Ticket
			REPLACE xpt.cnee_ref   WITH m.cnee_ref IN xpt  && PO Number
			m.pt = ALLTRIM(x856.f2)
			IF lTesting
*				REPLACE xpt.cacctnum   WITH "999" IN xpt
			ENDIF
			SELECT x856
			SKIP
			LOOP

		CASE TRIM(x856.segment) = "N1" AND TRIM(x856.f1) = "ST" && Ship-to data
*!*				#IF 0
*!*					SELECT uploaddet
*!*					LOCATE
*!*					IF !EMPTY(cisa_num)
*!*						APPEND BLANK
*!*						REPLACE uploaddet.office    WITH cOffice IN uploaddet
*!*						REPLACE uploaddet.cust_name WITH UPPER(TRIM(x856.f2)) IN uploaddet
*!*						REPLACE uploaddet.acct_num  WITH nacct_num IN uploaddet
*!*						REPLACE uploaddet.DATE      WITH DATETIME() IN uploaddet
*!*						REPLACE uploaddet.isa_num   WITH cisa_num IN uploaddet
*!*						REPLACE uploaddet.startpt   WITH xpt.ship_ref IN uploaddet
*!*						REPLACE uploaddet.runid     WITH lnRunID IN uploaddet
*!*					ENDIF
*!*				#ENDIF

			SELECT xpt
			m.consignee = UPPER(ALLTRIM(x856.f2))
			m.consignee = STRTRAN(m.consignee," - "," ")
			m.consignee = STRTRAN(m.consignee,"#","")

			REPLACE xpt.consignee WITH m.consignee IN xpt
			REPLACE xpt.dcnum WITH ALLTRIM(x856.f4) IN xpt

			SELECT x856
			SKIP 1 IN x856

			IF TRIM(x856.segment) = "N2"  && address info
				REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"N2ADDR*"+UPPER(ALLTRIM(x856.f1)) IN xpt
				REPLACE xpt.NAME    WITH UPPER(ALLTRIM(x856.f1)) IN xpt
				SELECT x856
				SKIP 1 IN x856
				IF TRIM(x856.segment) = "N3"  && address info
					REPLACE xpt.address  WITH UPPER(ALLTRIM(x856.f1)) IN xpt
					REPLACE xpt.address2 WITH UPPER(ALLTRIM(x856.f2)) IN xpt
				ENDIF
				SELECT x856
				SKIP 1 IN x856
				IF TRIM(x856.segment) = "N4"  && address info
					REPLACE xpt.residential WITH .F. IN xpt
					m.CSZ = UPPER(TRIM(x856.f1))+", "+UPPER(TRIM(x856.f2))+" "+TRIM(x856.f3)
					REPLACE xpt.CSZ WITH m.CSZ IN xpt
					REPLACE xpt.country WITH ALLTRIM(x856.f4) IN xpt
					IF TRIM(x856.f5) = "ZZ"
						IF TRIM(x856.f6) = "Y"
							REPLACE xpt.residential WITH .T. IN xpt
						ENDIF
					ENDIF
				ENDIF
			ELSE
				IF TRIM(x856.segment) = "N3"  && address info
					REPLACE xpt.address  WITH UPPER(ALLTRIM(x856.f1)) IN xpt
					REPLACE xpt.address2 WITH UPPER(ALLTRIM(x856.f2)) IN xpt
				ENDIF
				SELECT x856
				SKIP 1 IN x856
				IF TRIM(x856.segment) = "N4"  && address info
					REPLACE xpt.residential WITH .F. IN xpt
					m.CSZ = UPPER(TRIM(x856.f1))+", "+UPPER(TRIM(x856.f2))+" "+TRIM(x856.f3)
					REPLACE xpt.CSZ WITH m.CSZ IN xpt
					REPLACE xpt.country WITH ALLTRIM(x856.f4) IN xpt
					IF TRIM(x856.f5) = "ZZ"
						IF TRIM(x856.f6) = "Y"
							REPLACE xpt.residential WITH .T. IN xpt
						ENDIF
					ENDIF
				ENDIF
			ENDIF

			SELECT x856
			SKIP 1 IN x856
			LOOP

		CASE TRIM(x856.segment) = "N1" AND TRIM(x856.f1) = "BT" && Bill to data
			SELECT xpt
			REPLACE xpt.shipins   WITH xpt.shipins+CHR(13)+"BILLTONAME*"+UPPER(STRTRAN(ALLTRIM(x856.f2),"'","`")) IN xpt
			REPLACE xpt.shipins   WITH xpt.shipins+CHR(13)+"BILLTOCODE*"+ALLTRIM(x856.f4) IN xpt
			SELECT x856
			SKIP 1 IN x856

			IF TRIM(x856.segment) = "N2"  && address info
				REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"BILLTON2*"+UPPER(STRTRAN(ALLTRIM(x856.f1),"'","`")) IN xpt
				SELECT x856
				SKIP 1 IN x856
				IF TRIM(x856.segment) = "N3"  && address info
					REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"BILLTOADDR1*"+UPPER(ALLTRIM(x856.f1)) IN xpt
					REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"BILLTOADDR2*"+UPPER(STRTRAN(ALLTRIM(x856.f2),"'","`")) IN xpt
				ENDIF
				SELECT x856
				SKIP 1 IN x856
				IF TRIM(x856.segment) = "N4"  && address info
					REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"BILLTOCSZ*"+UPPER(TRIM(x856.f1))+", "+UPPER(TRIM(x856.f2))+" "+TRIM(x856.f3) IN xpt
					REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"BILLTOCNTRY*"+UPPER(TRIM(x856.f4)) IN xpt
				ENDIF
			ELSE
				IF TRIM(x856.segment) = "N3"  && address info
					REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"BILLTOADDR1*"+UPPER(ALLTRIM(x856.f1)) IN xpt
					REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"BILLTOADDR2*"+UPPER(STRTRAN(ALLTRIM(x856.f2),"'","`")) IN xpt
				ENDIF
				SELECT x856
				SKIP 1 IN x856
				IF TRIM(x856.segment) = "N4"  && address info
					REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"BILLTOCSZ*"+UPPER(TRIM(x856.f1))+", "+UPPER(TRIM(x856.f2))+" "+TRIM(x856.f3) IN xpt
					REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"BILLTOCNTRY*"+UPPER(TRIM(x856.f4)) IN xpt
				ENDIF
			ENDIF

			SELECT x856
			SKIP 1 IN x856
			LOOP

		CASE TRIM(x856.segment) = "N1" AND TRIM(x856.f1) = "BY" && Ship-for data
			IF lTesting
			ENDIF

			SELECT xpt
			REPLACE xpt.shipfor WITH UPPER(ALLTRIM(x856.f2)) IN xpt
			REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"STORENAME*"+ALLTRIM(x856.f2) IN xpt
			cStoreNum = ALLTRIM(x856.f4)
			REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"STORENUM*"+cStoreNum IN xpt
			nStoreNum = INT(VAL(cStoreNum))
			IF !nStoreNum > 99999
				REPLACE xpt.storenum  WITH nStoreNum IN xpt
			ENDIF
			
			REPLACE xpt.sforstore WITH ALLTRIM(x856.f4) IN xpt
			IF "TRACTOR"$xpt.shipfor
				REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"TSCXDOCK*TRUE" IN xpt
			ENDIF
			SKIP 1 IN x856
			IF TRIM(x856.segment) = "N2"  && name/address info
				REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"N2ADDR*"+UPPER(STRTRAN(ALLTRIM(x856.f1),"'","`")) IN xpt
				SELECT x856
				SKIP 1 IN x856
				IF TRIM(x856.segment) = "N3"  && address info
					REPLACE xpt.sforaddr1  WITH UPPER(ALLTRIM(x856.f1)) IN xpt
					REPLACE xpt.sforaddr2 WITH UPPER(ALLTRIM(x856.f2)) IN xpt
				ENDIF
				SELECT x856
				SKIP 1 IN x856
				IF TRIM(x856.segment) = "N4"  && address info
					m.sforCSZ = UPPER(TRIM(x856.f1))+", "+UPPER(TRIM(x856.f2))+" "+TRIM(x856.f3)
					REPLACE xpt.sforCSZ WITH m.CSZ IN xpt
				ENDIF
			ELSE
				IF TRIM(x856.segment) = "N3"  && address info
					REPLACE xpt.sforaddr1  WITH UPPER(ALLTRIM(x856.f1)) IN xpt
					REPLACE xpt.sforaddr2 WITH UPPER(ALLTRIM(x856.f2)) IN xpt
				ENDIF
				SELECT x856
				SKIP 1 IN x856
				IF TRIM(x856.segment) = "N4"  && address info
					m.sforCSZ = UPPER(TRIM(x856.f1))+", "+UPPER(TRIM(x856.f2))+" "+TRIM(x856.f3)
					REPLACE xpt.sforCSZ WITH m.sforCSZ IN xpt
				ENDIF
			ENDIF

			SELECT x856
			SKIP
			LOOP

		CASE TRIM(x856.segment) = "N9" AND TRIM(x856.f1) = "KK"  && 940type
			c940Type = ""
			c940Type = "940TYPE*"+IIF(TRIM(x856.f2)="LF","REGULAR","STO")
			REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+c940Type IN xpt
			SELECT x856
			SKIP
			LOOP

		CASE TRIM(x856.segment) = "N9" AND TRIM(x856.f1) = "DP"  && customer dept
			STORE "" TO m.dept,m.deptname
			m.dept = ALLTRIM(x856.f2)
			m.deptname = ALLTRIM(x856.f3)
			REPLACE xpt.dept WITH m.dept IN xpt
			REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"DEPTNAME*"+m.deptname IN xpt
			SELECT x856
			SKIP
			LOOP

		CASE TRIM(x856.segment) = "N9" AND TRIM(x856.f1) = "ST"  && store name for labels
			REPLACE xpt.shipfor WITH ALLTRIM(x856.f2)
			cStorename = "STORENAME*"+ALLTRIM(x856.f2)
			REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+cStorename IN xpt
			SELECT x856
			SKIP
			LOOP

		CASE TRIM(x856.segment) = "N9" AND ALLTRIM(x856.f1) = "CO"
			REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"SALESORDER*"+UPPER(ALLTRIM(x856.f2)) IN xpt
			REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"CUSTORDER*"+UPPER(ALLTRIM(x856.f3)) IN xpt
			SELECT x856
			SKIP
			LOOP

		CASE TRIM(x856.segment) = "N9" AND TRIM(x856.f1) = "14"  && Master Acct #
			REPLACE xpt.cacctnum WITH ALLTRIM(x856.f2)IN xpt
			SELECT x856
			SKIP
			LOOP

		CASE TRIM(x856.segment) = "N9" AND TRIM(x856.f1) = "IA"  && Vendor number
			REPLACE xpt.vendor_num WITH ALLTRIM(x856.f2)IN xpt
			SELECT x856
			SKIP
			LOOP

		CASE TRIM(x856.segment) = "N9" AND TRIM(x856.f1) = "12"  && Billing Acct #
			REPLACE xpt.upsacctnum WITH ALLTRIM(x856.f2) IN  xpt
			REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"BILLACCT*"+ALLT(x856.f2) IN xpt
			SELECT x856
			SKIP
			LOOP

		CASE TRIM(x856.segment) = "N9" AND TRIM(x856.f1) = "FOB"  && customer dept
			cFOB = ALLTRIM(x856.f2)
			REPLACE xpt.div WITH cFOB IN xpt
			REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"FOB*"+cFOB IN xpt
			SELECT x856
			SKIP
			LOOP

		CASE TRIM(x856.segment) = "N9" AND TRIM(x856.f1) = "XE"  && PO #
			IF ALLTRIM(x856.f2) = "01"
				REPLACE xpt.batch_num WITH "RUSH" IN  xpt
				REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"PRIORITY*RUSH" IN xpt
			ENDIF
			IF ALLTRIM(x856.f2) = "02"
				REPLACE xpt.batch_num WITH "DTOC " IN  xpt
				REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"PRIORITY*DTOC" IN xpt
			ENDIF
			IF ALLTRIM(x856.f2) = "03"
				REPLACE xpt.batch_num WITH "STD " IN  xpt
				REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"PRIORITY*STD" IN xpt
			ENDIF

			SELECT x856
			SKIP
			LOOP

		CASE TRIM(x856.segment) = "G62" AND TRIM(x856.f1) == "37"  && start date
			lcDate = ALLTRIM(x856.f2)
			lxDate = SUBSTR(lcDate,5,2)+"/"+SUBSTR(lcDate,7,2)+"/"+LEFT(lcDate,4)
			ldDate = CTOD(lxDate)
			STORE ldDate TO dStart
			REPLACE xpt.START WITH ldDate IN xpt
			SELECT x856
			SKIP
			LOOP

		CASE TRIM(x856.segment) = "G62" AND TRIM(x856.f1) == "38"  && cancel date
			lcDate = ALLTRIM(x856.f2)
			lxDate = SUBSTR(lcDate,5,2)+"/"+SUBSTR(lcDate,7,2)+"/"+LEFT(lcDate,4)
			ldDate = CTOD(lxDate)
			STORE ldDate TO dCancel
			REPLACE xpt.CANCEL WITH ldDate IN xpt
			SELECT x856
			SKIP
			LOOP

		CASE TRIM(x856.segment) = "N9" AND ALLTRIM(x856.f1) = "KK"
			REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"KK*"+ALLTRIM(x856.f2) IN xpt
			SELECT x856
			SKIP
			LOOP

		CASE TRIM(x856.segment) = "N9" AND ALLTRIM(x856.f1) = "PO"
			REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"N9PO*"+ALLTRIM(x856.f2) IN xpt
			SELECT x856
			SKIP
			LOOP

		CASE TRIM(x856.segment) = "NTE" AND TRIM(x856.f1) = "WHI"
			lnShipInsCtr=1
			DO WHILE TRIM(x856.segment) = "NTE" AND TRIM(x856.f1) = "WHI"
				REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"SPINS"+ALLTRIM(TRANSFORM(lnShipInsCtr))+"*"+UPPER(ALLTRIM(x856.f2)) IN xpt
				SKIP 1 IN x856
			ENDDO
			SELECT x856
*      SKIP
			LOOP

		CASE TRIM(x856.segment) = "W66"
			if xsqlexec("select * from altscac where accountid=6532 and shipmode='"+alltrim(x856.f5)+"'",,,"wh") # 0
  				cship_via = alltrim(x856.f5)

  				replace xpt.ship_via  with altscac.shipname in xpt
  				replace xpt.scac      with altscac.scaccode in xpt
  				replace xpt.carrcode  with altscac.shipmode in xpt
  				replace xpt.rout_code with altscac.shipmode in xpt
  				replace xpt.shipins   with xpt.shipins+chr(13)+"SHIPSCAC*"+upper(alltrim(altscac.scaccode)) in xpt
  				replace xpt.shipins   with xpt.shipins+chr(13)+"PKGCODE*"+upper(alltrim(altscac.search)) in xpt
  				replace xpt.shipins   with xpt.shipins+chr(13)+"PKGPAY*"+upper(alltrim(altscac.paycode)) in xpt
  				replace xpt.shipins   with xpt.shipins+chr(13)+"PKGTYPE*"+upper(alltrim(altscac.paytype)) in xpt
  			else
				REPLACE xpt.ship_via WITH "CALL FOR ROUTING" IN xpt
				REPLACE xpt.scac     WITH "????" IN xpt
			endif
			
			SELECT x856
			SKIP
			LOOP

********** added 2/9/2017 TMARG for ACADEMY and DICK'S if store=0 or empty, replace with DCNUM
replace xpt.storenum WITH VAL(xpt.dcnum) FOR INLIST(UPPER(xpt.consignee),'ACADEMY','DICK')  AND (ALLTRIM(transform(xpt.storenum))=''  OR xpt.storenum='0')

			WAIT WINDOW "NOW IN DETAIL LOOP PROCESSING" NOWAIT

		CASE TRIM(x856.segment) = "LX"
			m.units = IIF(lPrepack,.F.,.T.)
			SELECT xptdet
			SKIP 1 IN x856

			lnVasCtr = 0
			lcVASStr = ""

			DO WHILE ALLTRIM(x856.segment) != "SE"
				cSegment = TRIM(x856.segment)
				cCode = TRIM(x856.f1)

				IF TRIM(x856.segment) = "MAN" AND TRIM(x856.f1) = "GM"
					m.ucc = ALLT(x856.f2)
				ENDIF

				IF TRIM(x856.segment) = "W01" && Destination Quantity
					nptdetid = nptdetid + 1
					SELECT xptdet
					APPEND BLANK
					REPLACE xptdet.ptid WITH xpt.ptid IN xptdet
					REPLACE xptdet.ptdetid WITH nptdetid IN xptdet
					REPLACE xptdet.units WITH m.units IN xptdet
					REPLACE xptdet.accountid WITH xpt.accountid IN xptdet
					REPLACE xptdet.printstuff WITH "UOM*"+ALLT(x856.f2) IN xptdet
					IF lPrepack
						cUsePack = ALLT(STR(INT(VAL(x856.f12))))
						IF EMPTY(cUsePack)
							WAIT WINDOW "Empty cUsePack at PT"+cship_ref TIMEOUT 3
							NormalExit = .F.
							errormail1()
							USE IN x856
							lOK = .F.
							RETURN
						ENDIF
						IF cUsePack == "1"
							cUsePack = ALLT(x856.f1)
						ENDIF
					ELSE
						cUsePack = "1"
					ENDIF
					STORE cUsePack TO m.pack

					IF ALLTRIM(x856.f4) = "VN"
						m.style = ALLTRIM(x856.f5)
						m.upc = ALLTRIM(x856.f3)

						IF upcmastsql(,,,,m.upc)
							m.color = upcmast.COLOR
						ELSE
							m.color = "UNK"
						ENDIF
					ENDIF

					IF ALLTRIM(x856.f15) = "IN"  && Added 06.06.2013
						cRetailSKU = ALLTRIM(x856.f16)
						IF EMPTY(cRetailSKU)
							WAIT WINDOW "NO Retail SKU" &waittime&& 
						ENDIF
						REPLACE xptdet.printstuff WITH xptdet.printstuff+CHR(13)+"RETAILSKU*"+cRetailSKU IN xptdet
						REPLACE xptdet.custsku WITH cRetailSKU IN xptdet
					ENDIF

					REPLACE xptdet.STYLE WITH m.style IN xptdet
					REPLACE xptdet.COLOR WITH m.color IN xptdet
					REPLACE xptdet.upc   WITH m.upc IN xptdet
					REPLACE xptdet.PACK  WITH cUsePack IN xptdet
					m.casepack = INT(VAL(cUsePack))
					REPLACE xptdet.casepack WITH m.casepack IN xptdet
					m.divvalue = INT(VAL(ALLTRIM(x856.f1)))
					IF lPrepack AND m.divvalue = 1
						m.divvalue = INT(VAL(cUsePack))
					ENDIF
					IF m.divvalue = 0 AND !lPrepack
						errormail1()
						USE IN x856
						lOK = .F.
						RETURN
					ENDIF

					IF !lPrepack  && Pickpack
						m.totqty = INT(VAL(x856.f1))
						REPLACE xptdet.totqty WITH m.totqty IN xptdet
					ELSE
						m.totqty = (m.divvalue/m.casepack)
						REPLACE xptdet.totqty WITH m.totqty IN xptdet
					ENDIF

					REPLACE xptdet.origqty WITH xptdet.totqty IN xptdet
					REPLACE xpt.qty WITH xpt.qty + xptdet.totqty IN xpt
					REPLACE xpt.origqty WITH xpt.qty IN xpt
					REPLACE xptdet.printstuff WITH xptdet.printstuff+CHR(13)+"UNITCODE*"+ALLTRIM(x856.f2)
				ENDIF

				IF TRIM(x856.segment) = "G69" && shipstyle
					cPrtstr = "DESC*"+UPPER(TRIM(x856.f1))
					IF !("DESC"$xptdet.printstuff)
						REPLACE xptdet.printstuff WITH xptdet.printstuff+CHR(13)+cPrtstr IN xptdet
					ENDIF
				ENDIF

				IF TRIM(x856.segment) = "N9" AND ALLTRIM(x856.f1) = "LI"
					REPLACE xptdet.linenum WITH ALLTRIM(x856.f2) IN xptdet
				ENDIF

				IF TRIM(x856.segment) = "N9" AND ALLTRIM(x856.f1) = "PRT" AND ALLTRIM(x856.f2) = "B"
					REPLACE xptdet.ID WITH "SECONDS" IN xptdet
				ENDIF

				IF TRIM(x856.segment) = "AMT" AND ALLTRIM(x856.f1) = "LI"
					cPrtstr = "AMT*"+UPPER(TRIM(x856.f2))
					REPLACE xptdet.printstuff WITH xptdet.printstuff+CHR(13)+cPrtstr IN xptdet
				ENDIF

				IF TRIM(x856.segment) = "N9" AND ALLTRIM(x856.f1) = "VCL"
					lcColor = ALLTRIM(x856.f2)
					REPLACE xptdet.printstuff WITH xptdet.printstuff+CHR(13)+"COLOR*"+ALLTRIM(lcColor) IN xptdet
				ENDIF

				IF TRIM(x856.segment) = "N9" AND ALLTRIM(x856.f1) = "VSZ"
				  If ALLTRIM(x856.f2) ="NA"
             m.id = ""
          else
           	m.id = ALLTRIM(x856.f2)
          Endif 
					REPLACE xptdet.ID WITH m.id IN xptdet
				ENDIF

				IF TRIM(x856.segment) = "N9" AND ALLTRIM(x856.f1) = "VDM"
          If ALLTRIM(x856.f2) ="NA"
             m.id = ""
          else
    				m.id = m.id+" "+ALLTRIM(x856.f2)
          Endif 
					REPLACE xptdet.ID WITH m.id IN xptdet
				ENDIF

				IF TRIM(x856.segment) = "N9"  AND ALLTRIM(x856.f1) = "ZZ"
					lnVasCtr = lnVasCtr+1
					ThisVAS = ALLTRIM(x856.f2)
					DO CASE
						CASE ThisVAS = "V01"
							REPLACE xpt.v01 WITH .T. IN xpt
							IF !"CONTENT LABEL"$xpt.shipins
								REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"CONTENT LABEL" IN xpt
							ENDIF
						CASE ThisVAS = "V02"
							REPLACE xpt.v02 WITH .T. IN xpt
							IF !"VASCODE*CARTON LABELS"$xpt.shipins
								REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"VASCODE*CARTON LABELS" IN xpt
							ENDIF
						CASE ThisVAS = "V03"
							REPLACE xpt.v03 WITH .T. IN xpt
						CASE ThisVAS = "V04"
							REPLACE xpt.v04 WITH .T. IN xpt
							IF !"VASCODE*PRODUCT LABELS"$xpt.shipins
								REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"VASCODE*PRODUCT LABELS" IN xpt
							ENDIF
						CASE ThisVAS = "V05"
							REPLACE xpt.v05 WITH .T. IN xpt
*!*              CASE ThisVAS = "V06"
*!*                REPLACE xpt.v06 WITH .T. IN xpt
						CASE ThisVAS = "V07"
							REPLACE xpt.v07 WITH .T. IN xpt
						CASE ThisVAS = "V08"
							REPLACE xpt.v08 WITH .T. IN xpt
						CASE ThisVAS = "V09"
							REPLACE xpt.v09 WITH .T. IN xpt
						CASE ThisVAS = "V10"
							REPLACE xpt.v10 WITH .T. IN xpt
						CASE ThisVAS = "V11"
							REPLACE xpt.v11 WITH .T. IN xpt
							IF !"VASCODE*EVENT SPECIAL"$xpt.shipins
								REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"VASCODE*EVENT SPECIAL" IN xpt
							ENDIF
						CASE ThisVAS = "V12"
							REPLACE xpt.v12 WITH .T. IN xpt
							IF !"VASCODE*MUSICAL"$xpt.shipins
								REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"VASCODE*MUSICAL" IN xpt
							ENDIF
						CASE ThisVAS = "V13"
							REPLACE xpt.v13 WITH .T. IN xpt
						CASE ThisVAS = "V14"
							REPLACE xpt.v14 WITH .T. IN xpt
						CASE ThisVAS = "V15"
							REPLACE xpt.v15 WITH .T. IN xpt
						CASE ThisVAS = "V16"
							REPLACE xpt.v16 WITH .T. IN xpt
						CASE ThisVAS = "V17"
							REPLACE xpt.v17 WITH .T. IN xpt
						CASE ThisVAS = "V18"
							REPLACE xpt.v18 WITH .T. IN xpt
						CASE ThisVAS = "V19"
							REPLACE xpt.v19 WITH .T. IN xpt
						CASE ThisVAS = "V20"
							REPLACE xpt.v20 WITH .T. IN xpt
						CASE ThisVAS = "V21"
							REPLACE xpt.v21 WITH .T. IN xpt
							IF !"VASCODE*APPLYCTNSTICKER"$xpt.shipins
								REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"VASCODE*APPLYCTNSTICKER" IN xpt
							ENDIF
						CASE ThisVAS = "V22"
							REPLACE xpt.v22 WITH .T. IN xpt
						CASE ThisVAS = "V23"
							REPLACE xpt.v23 WITH .T. IN xpt
						CASE ThisVAS = "V24"
							REPLACE xpt.v24 WITH .T. IN xpt
						CASE ThisVAS = "V25"
							REPLACE xpt.v25 WITH .T. IN xpt
						CASE ThisVAS = "V26"
							REPLACE xpt.v26 WITH .T. IN xpt
						CASE ThisVAS = "V27"
							REPLACE xpt.v27 WITH .T. IN xpt
						CASE ThisVAS = "V28"
							REPLACE xpt.v28 WITH .T. IN xpt
						CASE ThisVAS = "V28"
							REPLACE xpt.v28 WITH .T. IN xpt
						CASE ThisVAS = "V29"
							REPLACE xpt.v29 WITH .T. IN xpt
						CASE ThisVAS = "V30"
							REPLACE xpt.v30 WITH .T. IN xpt
						CASE ThisVAS = "V31"
							REPLACE xpt.v31 WITH .T. IN xpt
						CASE ThisVAS = "V32"
							REPLACE xpt.v32 WITH .T. IN xpt
						CASE ThisVAS = "V33"
							REPLACE xpt.v33 WITH .T. IN xpt
							IF !upper("RMVE PbagAPPW/Hanger")$xpt.shipins
								REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+UPPER("RMVE PbagAPPW") IN xpt
							ENDIF
						CASE ThisVAS = "V35"
							REPLACE xpt.v35 WITH .T. IN xpt
							IF !upper("UPC labels facing up")$xpt.shipins
								REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+UPPER("UPC labels facing up") IN xpt
							ENDIF
						CASE ThisVAS = "V36"
							REPLACE xpt.v36 WITH .T. IN xpt
							IF !upper("Manual ASN")$xpt.shipins
								REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+UPPER("Manual ASN") IN xpt
							ENDIF
						CASE ThisVAS = "V37"
							REPLACE xpt.v37 WITH .T. IN xpt
						CASE ThisVAS = "V38"
							REPLACE xpt.v38 WITH .T. IN xpt
							IF !upper("No Filler in Carton")$xpt.shipins
								REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+UPPER("No Filler in Carton") IN xpt
							ENDIF
						CASE ThisVAS = "V39"
							REPLACE xpt.v39 WITH .T. IN xpt
							IF !upper("UCC128 Front of Box")$xpt.shipins
								REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+UPPER("UCC128 Front of Box") IN xpt
							ENDIF
						CASE ThisVAS = "V41"
							REPLACE xpt.v41 WITH .T. IN xpt
						CASE ThisVAS = "V50"
							REPLACE xpt.v50 WITH .T. IN xpt
							IF !upper("No cartons over 50 lbs")$xpt.shipins
								REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+UPPER("No cartons over 50 lbs") IN xpt
							ENDIF
					ENDCASE

					REPLACE xptdet.printstuff WITH xptdet.printstuff+CHR(13)+"VAS"+ALLTRIM(TRANSFORM(lnVasCtr))+"*"+ALLTRIM(x856.f2) IN xptdet
					IF lnVasCtr = 1
						IF !"VASFLAG*TRUE"$xpt.shipins
							REPLACE xpt.vas WITH .T.
							REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"VASFLAG*TRUE" IN xpt
						ENDIF
						lcVASStr = ALLTRIM(x856.f2)
						REPLACE xpt.vas WITH .T.
					ELSE
						lcVASStr= lcVASStr +","+ALLTRIM(x856.f2)
						REPLACE xpt.vas WITH .T.
					ENDIF
				ENDIF

				SKIP 1 IN x856

				IF ALLTRIM(x856.segment) = "LX"
					IF !EMPTY(lcVASStr)
						REPLACE xptdet.printstuff WITH xptdet.printstuff+CHR(13)+"ALLVAS*"+ALLTRIM(lcVASStr) IN xptdet
					ENDIF
					lnVasCtr = 0
					lcVASStr = ""
				ENDIF
			ENDDO
			m.shipins =""
			lnVasCtr = 0
			lcVASStr = ""

			SELECT xptdet
	ENDCASE

	SELECT x856
	IF !EOF()
		SKIP 1 IN x856
	ENDIF
ENDDO

If !lTesting
  tu("ackdata")
Endif 

SELECT xptdet
rollup()

WAIT WINDOW "At end of ROLLUP procedure"  &waittime&& TIMEOUT 2

*********** added by PG 09/23/2014, somehow carcode gets set to "UPS", this should fix it
SELECT xpt
GOTO TOP
REPLACE ALL carrcode WITH rout_code

DO m:\dev\prg\setuppercase940

**need to create a product type indicator in shipins if one does not already exist - mvw 07/23/14
SELECT xpt
SCAN FOR !("PRODUCT TYPE*"$shipins)
	xallapparel=.T.
	xallfootwear=.T.
	SELECT xptdet
	SCAN FOR xptdet.ptid=xpt.ptid
		upcmastsql(xpt.accountid,,,,xptdet.upc)
		xallapparel=IIF(upcmast.itemtype="APP",xallapparel,.F.)
		xallfootwear=IIF(upcmast.itemtype="FOOT",xallfootwear,.F.)
		IF !xallapparel AND !xallfootwear
			EXIT
		ENDIF
	ENDSCAN
	xproducttype="PRODUCT TYPE*"+IIF(xallapparel,"ALL APPAREL",IIF(xallfootwear,"ALL FOOTWEAR","MIXED"))
	REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+xproducttype IN xpt
ENDSCAN

IF lBrowfiles
	WAIT WINDOW "If tables are incorrect, CANCEL when XPTDET window opens"+CHR(13)+"Otherwise, will load PT table"  &waittime&& TIMEOUT 2
	SELECT xpt
	LOCATE
	BROWSE && TIMEOUT 10
	SELECT xptdet
	LOCATE
	BROWSE
ENDIF

IF lreload
	SELECT xpt
	DELETE FOR !INLIST(xpt.ship_ref,"0082571867","0082571891","0082571961","0082571975","0082572034")
	SELECT xpt
	BROWSE

ENDIF


&& Added the following to attempt to trap PT records with missing detail
SELECT xpt
SCAN
	SELECT xptdet
	LOCATE FOR xptdet.ptid = xpt.ptid
	IF !FOUND()
		SET STEP ON
		tsubjectqq = "Ariat 940 did not load all PT Detail, File "+cFilename
		tattach  = ""
		tsendtoqq  = tsendtoerr
		tccqq = tccerr
		tmessageqq = "File "+xfile+" was missing PT detail during the upload process...reload this file locally"
		tmessageqq = tmessageqq+CHR(13)+"(Error occurred at "+TTOC(DATETIME())
		tFrom ="Toll EDI Operations <toll-edi-ops@tollgroup.com>"
		DO FORM m:\dev\frm\dartmail2 WITH tsendtoqq,tFrom,tsubjectqq,tccqq,tattach,tmessageqq,"A"
		lOK = .F.
		RETURN
	ENDIF
ENDSCAN


*!*  If some_deleted
*!*    tsubjectsd = "Ariat 940 Records deleted at "+Ttoc(Datetime())
*!*    tattach  = ""
*!*    tsendtosd  = "pgaidis@fmiint.com"
*!*    tccsd = ""
*!*    tmessagesd = "Picktickets deleted: "+deleted_pts
*!*    tFrom ="Toll EDI Operations <toll-edi-ops@tollgroup.com>"
*!*    Do Form m:\dev\frm\dartmail2 With tsendtosd,tFrom,tsubjectsd,tccsd,tattach,tmessagesd,"A"
*!*  Endif

WAIT WINDOW cCustname+" Breakdown Round complete..." &waittime&& TIMEOUT 2
SELECT x856
WAIT CLEAR




*************************************************************************************************
*!* Error Mail procedure - W01*12 Segment
*************************************************************************************************
PROCEDURE errormail1

	IF lEmail
		SELECT 0
		USE F:\3pl\DATA\mailmaster ALIAS mm SHARED
		LOCATE FOR mm.edi_type = "MISC" AND mm.taskname = "MODSHOEERR"
		STORE TRIM(IIF(mm.use_alt,mm.sendtoalt,mm.sendto)) TO tsendto
		STORE TRIM(IIF(mm.use_alt,mm.ccalt,mm.cc)) TO tcc
		USE IN mm
		cMailLoc = "Louisville, Kentucky"
		tsubject= "TGF "+cMailLoc+" "+cCustname+" Pickticket Error: " +TTOC(DATETIME())
		tmessage = "File: "+cFilename+CHR(13)+" is missing W01*12 segment(s), causing a divide by zero error."
		tmessage = tmessage + "Please re-create and resend this file."
		DO FORM m:\dev\frm\dartmail2 WITH tsendto,tFrom,tsubject,tcc,tattach,tmessage,"A"
	ENDIF
ENDPROC

*************************************************************************************************
*!* Error Mail procedure - only for QVC UPC issues
*************************************************************************************************
PROCEDURE errormail_qvc
	PARAMETERS lcstyle

*  tsendto ="pgaidis@fmiint.com,juan@fmiint.com,maria.estrella@tollgroup.com,joe.bianchi@tollgroup.com"
	tsendto ="pgaidis@fmiint.com"
	tcc=""
	tattach  = ""
	tsubject= "Ariat QVC UPC Issue Pickticket Error: " +TTOC(DATETIME())
	tmessage = "Missing UPC for :"+lcstyle+CHR(13)
	tmessage = tmessage + "Please make corrections and reload file: "+cFilename
	DO FORM m:\dev\frm\dartmail2 WITH tsendto,tFrom,tsubject,tcc,tattach,tmessage,"A"

ENDPROC
*************************************************************************************************
*!* Error Mail procedure - Cancel Before Start Date
*************************************************************************************************
PROCEDURE errordates

	IF lEmail
		tsendto = tsendtotest
		tcc =  tcctest
		tattach  = ""
		cMailLoc = "Louisville, Kentucky"
		tsubject= "TGF "+cMailLoc+" "+cCustname+" Pickticket Error: " +TTOC(DATETIME())
		tmessage = "File: "+cFilename+CHR(13)+" has Cancel date(s) which precede Start date(s)"
		tmessage = tmessage + "File needs to be checked and possibly resubmitted."
		DO FORM m:\dev\frm\dartmail2 WITH tsendto,tFrom,tsubject,tcc,tattach,tmessage,"A"
	ENDIF
ENDPROC

*****************************************************************************************************
PROCEDURE rollup
*****************************************************************************************************
	IF lTesting
*SET STEP ON
	ENDIF
	IF !lTesting
	ASSERT .F. MESSAGE "In rollup procedure"
	endif
	nChangedRecs = 0

	SELECT xptdet
	SET DELETED ON
	LOCATE
	IF lTesting
*    WAIT WINDOW "Browsing XPTDET pre-rollup" TIMEOUT 2
*    BROW
	ENDIF
	cMsgStr = "PT/OSID/ODIDs changed: "
	SCAN FOR !DELETED()
		SCATTER MEMVAR
		nRecno = RECNO()
		COUNT TO nLoops FOR xptdet.ptid = m.ptid 	AND xptdet.STYLE = m.style AND xptdet.COLOR = m.color ;
			AND xptdet.ID = m.id AND xptdet.PACK = m.pack AND xptdet.upc = m.upc AND RECNO('xptdet') > nRecno
		GO nRecno
		IF nLoops = 0
			LOOP
		ENDIF

		IF EOF()
			EXIT
		ENDIF

		lcUPC = ""
		nTotqty =0
		lcLineNum = ""
		lcRolledupString =""

		SELECT xptdet
		GO nRecno
		REPLACE xptdet.printstuff WITH xptdet.printstuff+CHR(13)+"ORIGQTY*"+ALLTRIM(TRANSFORM(xptdet.totqty))+CHR(13)+"ORIGUPC*"+lcUPC IN xptdet

		FOR tt = 1 TO nLoops
			LOCATE FOR xptdet.ptid = m.ptid ;
				AND xptdet.STYLE = m.style ;
				AND xptdet.COLOR = m.color ;
				AND xptdet.ID = m.id ;
				AND xptdet.PACK = m.pack ;
				AND xptdet.upc = m.upc ;
				AND RECNO('xptdet') > nRecno
			IF FOUND()
				SELECT xpt
				LOCATE FOR xpt.ptid = m.ptid
				IF !FOUND()
					WAIT WINDOW "PROBLEM!"
				ENDIF
				cship_ref = ALLT(xpt.ship_ref)
				IF nChangedRecs > 10
					cMsgStr = "More than 10 records rolled up"
				ELSE
					cMsgStr = cMsgStr+CHR(13)+cship_ref+" "+TRANSFORM(m.ptid)+" "+TRANSFORM(m.ptdetid)
				ENDIF
				nChangedRecs = nChangedRecs+1
				SELECT xptdet
*        alength = ALINES(aptdet,xptdet.printstuff,.T.,CHR(13))
*        cStoredUCC = ALLTRIM(segmentget(@aptdet,"UPC",alength))
				lcUPC     = xptdet.upc
				nTotqty   = xptdet.totqty
				lcLineNum = xptdet.linenum
*      lcLoopLine= alltrim(Transform(tt))
				lcRolledupString = CHR(13)+CHR(13)+"SUBLINE*"+lcLineNum+CHR(13)+"SUBQTY*"+TRANSFORM(nTotqty)
				DELETE NEXT 1 IN xptdet
			ENDIF
			GO nRecno
			REPLACE xptdet.printstuff WITH xptdet.printstuff+CHR(13)+lcRolledupString IN  xptdet
			REPLACE xptdet.totqty WITH xptdet.totqty+nTotqty IN xptdet
			REPLACE xptdet.origqty WITH xptdet.totqty IN xptdet
		ENDFOR

		SELECT xptdet
	ENDSCAN
	IF lTesting
*    SET DELETED OFF
*    BROWSE
*    SET DELETED ON
	ENDIF

	IF nChangedRecs > 0
		WAIT WINDOW cMsgStr  &waittime&& TIMEOUT 2
		WAIT WINDOW "There were "+TRANS(nChangedRecs)+" records rolled up" &waittime&& TIMEOUT 1
	ELSE
		WAIT WINDOW "There were NO records rolled up" &waittime&&  TIMEOUT 1
	ENDIF

ENDPROC

*****************************************************************************************************
PROCEDURE segmentget
*****************************************************************************************************

	PARAMETER thisarray,lcKey,nLength

	FOR i = 1 TO nLength
		IF i > nLength
			EXIT
		ENDIF
		lnEnd= AT("*",thisarray[i])
		IF lnEnd > 0
			lcThisKey =TRIM(SUBSTR(thisarray[i],1,lnEnd-1))
			IF OCCURS(lcKey,lcThisKey)>0
				RETURN SUBSTR(thisarray[i],lnEnd+1)
				i = 1
			ENDIF
		ENDIF
	ENDFOR

	RETURN ""
