*!* m:\dev\prg\merkury940_main.prg
PARAMETERS cOfficeIn

PUBLIC nAcctNum,cDelimiter,nRun_num,cTranslateOption,cWhichFile,cOffice,cCustname,cPropername,cStorenum,lOK
PUBLIC cFilter,nRunID,lComplete,lTesting,m.UCCNUMBER,lEmail,lTestImport,emailcommentstr,cUseFolder,lBrowfiles,cMod,cISA_Num
PUBLIC ptid,ptctr,ptqty,xfile,cfilename,nUploadCount,pickticket_num_start,pickticket_num_end,nFileCount,units,cUCCNumber
PUBLIC lcPath,lcArchivepath,NormalExit,lTestmail,LogCommentStr,lPick,lPrepack,lImported,lWalmartTest
PUBLIC m.addby,m.adddt,m.addproc,cTransfer,lHighline,cISA_Num,thisfile,lLoadSQL,nFileSize
PUBLIC cErrMsg,cCO
Public tsendto,tcc,tsendtoerr,tccerr,tsendtotest,tcctest,tsendtoalt,tccalt,tsendtopt,tccpt,tfrom,tattach,guserid
Store "" To tsendto,tcc,tsendtoerr, tccerr,tsendtotest,tcctest,tsendtoalt,tccalt,tsendtopt,tccpt,tfrom,tattach,guserid,gmasteroffice
Public llEmailTest
*On Error do errhandler


PUBLIC ARRAY a856(1)
PUBLIC ARRAY thisarray(1)

guserid = "EDIUSER"
gmasteroffice = "C"

CLOSE DATABASES ALL
NormalExit = .F.

cErrMsg="None yet"
*   test()    && To get into test mode: 
*   test(.t.) && get into live mode: 

_screen.caption="Merkury 940 Uploadder: start time: "+Ttoc(Datetime())

llEmailTest = .f.

TRY
	lTesting = .f.
	lTestImport = lTesting
	lTestmail = lTesting
	lBrowfiles = lTesting
	lOverridebusy = lTesting
	lBrowfiles = .F.
*	lWalmartTest = .T.
	lOverridebusy = .T.


	_SCREEN.WINDOWSTATE= IIF(lTesting OR lOverridebusy,2,1)

	DO m:\dev\prg\_setvars WITH lTesting

	ASSERT .F. MESSAGE "At start of Merkury 940 processing"
	SELECT 0
	USE F:\edirouting\ftpsetup SHARED
	cTransfer = "940-MERKURY"
	LOCATE FOR ftpsetup.transfer = cTransfer AND ftpsetup.TYPE = "GET"
	IF ftpsetup.chkbusy AND !lOverridebusy
		WAIT WINDOW "Process is flagged busy...returning" TIMEOUT 1
		NormalExit = .T.
		THROW
	ELSE
		SELECT ftpsetup
		LOCATE FOR ftpsetup.transfer = cTransfer
		IF FOUND()
			REPLACE chkbusy WITH .T.,trig_time WITH DATETIME()
			USE IN ftpsetup
		ELSE
			WAIT WINDOW "Error in FTPSetup...returning" TIMEOUT 1
			USE IN ftpsetup
			NormalExit = .T.
			THROW
		ENDIF
	ENDIF

	IF VARTYPE(cOfficeIn) # "C"
		cOfficeIn = "C"
	ENDIF
	cCustname = "MERKURY"
	nAcctNum  = 6561
	cMod = IIF(cOfficeIn = "C","2","I")
	gOffice = cMod
	lLoadSQL = .T.

  useca("info","wh")  && setup for error logging

	lEmail = .T.
	LogCommentStr = ""

	STORE cOfficeIn TO cOffice
	tfrom = "Toll EDI Operations <toll-edi-ops@tollgroup.com>"
	STORE " " TO tattach,tsendto,tcc

	SELECT 0
	USE F:\3pl\DATA\mailmaster ALIAS mm SHARED
	LOCATE FOR mm.edi_type = "940" AND mm.accountid = nAcctNum AND mm.office = cOffice
	IF FOUND()
		STORE TRIM(mm.acctname) TO cCustname
		STORE TRIM(mm.basepath) TO lcPath
		STORE TRIM(mm.archpath) TO lcArchivepath
		_SCREEN.CAPTION = ALLT(mm.scaption)
*!*			STORE mm.testflag 	   TO lTest
		STORE TRIM(IIF(mm.use_alt,mm.sendtoalt,mm.sendto)) TO tsendto
		STORE TRIM(IIF(mm.use_alt,mm.ccalt,mm.cc))         TO tcc
		LOCATE

		LOCATE FOR mm.edi_type = "MISC" AND mm.taskname = "GENERAL"

		STORE TRIM(IIF(mm.use_alt,mm.sendtoalt,mm.sendto)) TO tsendtotest
		STORE TRIM(IIF(mm.use_alt,mm.ccalt,mm.cc))         TO tcctest
		tsendtoerr = tsendtotest
		tccerr     = tcctest

		LOCATE FOR mm.edi_type = "MISC" AND mm.GROUP = "MERKURY" AND mm.taskname = "DUPPTNOTICE"

		STORE TRIM(IIF(mm.use_alt,mm.sendtoalt,mm.sendto)) TO tsendtopt
		STORE TRIM(IIF(mm.use_alt,mm.ccalt,mm.cc))         TO tccpt

		USE IN mm
	ELSE
		USE IN mm
		WAIT WINDOW AT 10,10  "No parameters set for this acct# "+ALLTRIM(STR(Acctnum))+"  ---> Office "+lcOffice TIMEOUT 2
		RELEASE ALL
		NormalExit = .F.
		THROW
	ENDIF
	lcPath = IIF(lTesting,lcPath+"test\",lcPath)

	cUseName = "MERKURY"
	cPropername = PROPER(cUseName)

	_SCREEN.CAPTION = cPropername+" 940 Process"
	CLEAR
	WAIT WINDOW "Now setting up "+cPropername+" 940 process..." TIMEOUT 2

	DO m:\dev\prg\createx856a
	SELECT x856

	cDelimiter = "*"
	cTranslateOption = "TILDE"
	cfile = ""

	dXdate1 = ""
	dXxdate2 = DATE()
	nFileCount = 0

	nRun_num = 999
	lnRunID = nRun_num

*set step On 
	IF lTesting
		cUseFolder = "F:\WHP\WHDATA\"
	ELSE
		xReturn="XXX"
		DO m:\dev\prg\wf_alt WITH cOffice,nAcctNum
		cUseFolder=xReturn
	ENDIF

	DO ("m:\dev\PRG\MERKURY940_PROCESS")

	SELECT 0
	USE F:\edirouting\ftpsetup SHARED
	REPLACE chkbusy WITH .F. ;
		FOR ftpsetup.TYPE = "GET" ;
		AND ftpsetup.transfer = cTransfer ;
		IN ftpsetup
	NormalExit = .T.

CATCH TO oErr
	IF NormalExit = .F.
		ASSERT .F. MESSAGE "In Catch section..."
		tsubject = "Merkury 940 Upload Error ("+TRANSFORM(oErr.ERRORNO)+") at "+TTOC(DATETIME())
		tsendto  = "pgaidis@fmiint.com"    &&tsendtoerr
		tcc      = "pgaidis@fmiint.com"  &&tccerr
		tmessage = "Merkury 940 Upload Error..... Please fix me........!"
		lcSourceMachine = SYS(0)
		lcSourceProgram = "from merkury940 group"

		tmessage =tmessage+ "Try/CATCH Exeception Message:"+CHR(13)+;
			[  Error: ] + STR(oErr.ERRORNO) +CHR(13)+;
			[  LineNo: ] + STR(oErr.LINENO) +CHR(13)+;
			[  Message: ] + oErr.MESSAGE +CHR(13)+;
			[  Procedure: ] + oErr.PROCEDURE +CHR(13)

		IF !EMPTY(cErrMsg)
			tmessage =tmessage+CHR(13)+"Error found: "+cErrMsg
		ELSE
			tmessage =tmessage+[  Details: ] + oErr.DETAILS +CHR(13)+;
				[  StackLevel: ] + STR(oErr.STACKLEVEL) +CHR(13)+;
				[  LineContents: ] + oErr.LINECONTENTS+CHR(13)+;
				[  UserValue: ] + oErr.USERVALUE+CHR(13)+;
				[  Computer:  ] +lcSourceMachine+CHR(13)+;
				[  940 file:  ] +xfile+CHR(13)+;
				[  Program:   ] +lcSourceProgram
		ENDIF

		DO FORM m:\dev\frm\dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
	ELSE
		WAIT WINDOW AT 10,10 "Normal Exit " TIMEOUT 2
	ENDIF
FINALLY
	ON ERROR
	SET STATUS BAR ON
	CLOSE DATABASES ALL
ENDTRY
