PARAMETERS cOffice,nAcctNum
PUBLIC m.accountid,lTesting,NormalExit,cUseFolder,tsendto,tsendtoerr,tcc,tccerr,cTransfer,lDoMail,cCustName,lSync,cErrMsg,cMod,lDoSQL,cWhseMod,gOffice
*!* Derived from the genericinbound program
CLOSE DATABASES ALL
*SET STEP ON
TRY
	DO m:\dev\prg\lookups
	lTesting = .F.
	lBrowfiles = lTesting
	NormalExit = .F.
	DO m:\dev\prg\_setvars WITH .T.
	lDoMail = .T.
*	lBrowfiles = .T.
	cErrMsg = ""
	
	_screen.WindowState = IIF(lTesting,2,1)

	SET SAFETY off	
	IF lTesting
		WAIT WINDOW "Running as TEST" TIMEOUT 2
		cUseFolder = "F:\WHP\WHDATA\"
		nAcctNum = 6521
		cOffice = "C"
	ELSE
		IF VARTYPE(nAcctNum) # "N"
			IF VARTYPE(nAcctNum) = "L"
				nAcctNum = 6221
				cOffice = "C"
			ELSE
				nAcctNum = INT(VAL(nAcctNum))
			ENDIF
		ENDIF
		cUseFolder = "F:\WH2\WHDATA\"
		WAIT WINDOW "Using folder "+cUseFolder TIMEOUT 2
	ENDIF

	m.accountid = nAcctNum
	cMod = "2"
	cWhseMod = "wh"+cMod
	gOffice = cMod
	gMasterOffice = cOffice


	lDoSQL = .T.

	SELECT 0
	USE F:\edirouting\FTPSETUP SHARED
	cTransfer = "PL-BCNY"   && Flag all BCNY PL uploads as busy to avoid conflict
	REPLACE chkbusy WITH .T.,trig_time WITH DATETIME()  FOR FTPSETUP.TRANSFER = cTransfer
	USE IN FTPSETUP

*	USE (cUseFolder+"whgenpk") IN 0 ALIAS whgenpk

	useca("inwolog","wh")

	IF lDoSQL
		useca("pl","wh")
	ELSE
		USE (cUseFolder+"pl") IN 0 ALIAS pl
	ENDIF

	USE F:\wo\wodata\wolog IN 0 ALIAS wolog
	useca("inrcv","wh")  && creates INRCV updatable cursor

	xsqlexec("select * from account where inactive=0","account",,"qq")
	INDEX ON accountid TAG accountid
	SET ORDER TO

	cCustName = ""
	cPnp = ""
	lCtnAcct = .T.

	SELECT 0  && Mailmaster is run at this point, after proper account ID is determined
	USE F:\3pl\DATA\mailmaster ALIAS mm
	LOCATE FOR mm.accountid = nAcctNum AND mm.office = cOffice AND mm.edi_type = "PL"
	IF FOUND()
		STORE TRIM(mm.AcctName) TO lcAccountname
		STORE TRIM(mm.basepath) TO lcPath
			tsendto = IIF(mm.use_alt,sendtoalt,sendto)
			tcc = IIF(mm.use_alt,ccalt,cc)
		IF lTesting
			WAIT WINDOW "File path is "+lcPath TIMEOUT 2
		ENDIF
		STORE TRIM(mm.archpath) TO lcArchPath
		STORE TRIM(mm.scaption) TO _SCREEN.CAPTION
		IF lTesting
			LOCATE
			LOCATE FOR edi_type = "MISC" AND taskname = "JOETEST"
			tsendto = IIF(mm.use_alt,sendtoalt,sendto)
			tcc = IIF(mm.use_alt,ccalt,cc)
		ENDIF
		LOCATE
		LOCATE FOR edi_type = "MISC" AND taskname = "GENERAL"
		tsendtoerr = IIF(mm.use_alt,sendtoalt,sendto)
		tccterr = IIF(mm.use_alt,ccalt,cc)
		USE IN mm
	ELSE
		SET STEP ON
		WAIT WINDOW AT 10,10  "No parameters set for this acct# "+ALLTRIM(STR(nAcctNum))+"  ---> Office "+lcOffice TIMEOUT 5
		THROW
	ENDIF

	cInfolder = lcPath
	cArchfolder = lcArchPath
	CD [&cInfolder]
	len1= ADIR(ary1,"*.xls")


	FOR ee = 1 TO len1
		xsqlexec("select * from inwolog where .f.","xinwolog",,"wh")

		SELECT xinwolog
		SCATTER MEMVAR MEMO
		INDEX ON inwologid TAG inwologid

		IF lDoSQL
			xsqlexec("select * from pl where .f.","xpl",,"wh")
		ELSE
			SELECT * FROM pl WHERE .F. INTO CURSOR xpl READWRITE
		ENDIF
		SELECT xpl
		SCATTER MEMVAR MEMO

		SELECT 0
		CREATE CURSOR intake (f1 c(50),f2 c(30),f3 c(30),f4 c(30),f5 c(30),f6 c(30),f7 c(30),f8 c(30))
		cfilenamexx = (cInfolder+"temp1.txt")
		IF FILE(cfilenamexx)
			DELETE FILE [&cfilenamexx]
		ENDIF
		cfilename = ALLTRIM(ary1[ee,1])
		WAIT WINDOW "Processing file "+IIF(nAcctNum=6521,"SYNCLAIRE", "BCNY")+" "+cfilename TIMEOUT 2
		cfullname = (cInfolder+cfilename)
		cArchfilename = (cArchfolder+cfilename)
		COPY FILE [&cfullname] TO [&carchfilename]
		oExcel = CREATEOBJECT("Excel.Application")
		oWorkbook = oExcel.Workbooks.OPEN(cfullname)
		WAIT WINDOW "Now saving file "+cfilename NOWAIT NOCLEAR
		SET SAFETY OFF
		oWorkbook.SAVEAS(cfilenamexx,20)  && or 39 for xl5...re-saves Excel sheet as XL95/97 file
		WAIT CLEAR
		WAIT WINDOW "File save complete...continuing" TIMEOUT 1
		oWorkbook.CLOSE(.T.)
		oExcel.QUIT()
		RELEASE oExcel
		IF !lTesting
			DELETE FILE [&cfullname]
		ENDIF

		SELECT intake
		APPEND FROM [&cfilenamexx] TYPE DELIMITED WITH TAB && XL8
		LOCATE
		IF lTesting OR lBrowfiles
			BROWSE TIMEOUT 30
		ENDIF
		GO BOTT
		DO WHILE EMPTY(intake.f1)
			SKIP -1
		ENDDO
		nRec1 = RECNO('intake')
		DELETE FOR RECNO()>nRec1

		LOCATE FOR UPPER(intake.f1) = "ACCOUNT"
		lSync = IIF(ALLTRIM(intake.f2)="BCNY",.F.,.T.)
		nAcctNum = IIF(lSync,6521,6221)

		SELECT account
		=SEEK(nAcctNum,'account','accountid')
		cPnp = "pnp"+IIF(cOffice="I","N",cOffice)
		IF FOUND()
			cCustName = ALLTRIM((account.AcctName))
			lCtnAcct = IIF(!lSync,.T.,!(account.&cPnp))
		ELSE
			cCustName = "NA"
			lCtnAcct = .T.
		ENDIF

		IF cCustName = "NA"
			WAIT WINDOW "Houston, we have a problem..." TIMEOUT 2
			THROW
		ENDIF

		IF lTesting
			SELECT intake
			LOCATE
			BROWSE

		ENDIF
		m.wo_date = DATE()
		m.date_rcvd = DATE()
		m.picknpack = .F.
		m.wo_num = 1
		m.inwologid = 1
		m.plid = 0
		m.AcctName = cCustName
		m.addby = "TOLLPROC"
		m.adddt = DATETIME()
		m.addproc = IIF(lSync,"SYNCINB","BCNYINB")

		m.comments = "FILENAME*"+cfilename
		SELECT intake
		LOCATE
		LOCATE FOR UPPER(intake.f1) = "CONTAINER"
		m.container = UPPER(ALLTRIM(intake.f2))
		IF EMPTY(m.container)
			LOCATE FOR UPPER(intake.f1) = "AIR WAYBILL"
			m.container = UPPER(ALLTRIM(intake.f2))
			IF EMPTY(m.container)
				WAIT WINDOW "No Container or Reference data" TIMEOUT 10
				CLOSE DATA ALL
				THROW
			ENDIF
		ENDIF

		WAIT WINDOW "Container: "+m.container TIMEOUT 2

		LOCATE
		LOCATE FOR UPPER(intake.f1) = "BRANDS"
		m.comments = m.comments+CHR(13)+"BRANDS*"+ALLTRIM(intake.f2)
		INSERT INTO xinwolog FROM MEMVAR

		SELECT intake
		LOCATE
		LOCATE FOR "CARTON RANGE"$UPPER(intake.f1)
		IF !FOUND()
			WAIT WINDOW "CARTON RANGE cell not found!" TIMEOUT 10
			CLOSE DATA ALL
			THROW
		ENDIF
		REPLACE intake.f5 WITH "gqty",intake.f6 WITH "Totqty",intake.f3 WITH "Color",intake.f8 WITH "Pack" IN intake NEXT 1
		nRec1 = RECNO()
		SCAN FOR RECNO()>nRec1
			IF "-"$intake.f1
				nDigit1 = INT(VAL(LEFT(ALLTRIM(intake.f1),AT("-",intake.f1)-1)))
				nDigit2 = INT(VAL(SUBSTR(ALLTRIM(intake.f1),AT("-",intake.f1)+1)))
				REPLACE intake.f6 WITH TRANSFORM(nDigit2-nDigit1+1)  && totqty
				IF INT(VAL(intake.f6))<1 && negative number result
					CLOSE DATA ALL
					cErrMsg = "Negative carton range (tot.qty. is negative)"
					THROW
				ENDIF


			ELSE
				REPLACE intake.f6 WITH ALLT(intake.f1)  && totqty
			ENDIF
			REPLACE intake.f8 WITH ALLT(f5) && ALLTRIM(TRANS(INT(VAL(intake.f5))/INT(VAL(f6))))  && Pack
		ENDSCAN

		SELECT IIF(ISALPHA(ALLTRIM(f4)),"",ALLTRIM(f4)) AS COLOR, f2 AS STYLE,f3 AS cayset,INT(VAL(f6)) AS ctnqty,INT(VAL(f5)) AS unitqty,ALLT(f8) AS cpack ;
			FROM intake ;
			WHERE RECNO()>nRec1 ;
			INTO CURSOR temppl READWRITE

		SELECT temppl
		LOCATE
		m.plid = 0
		npo = 0
		SCAN
			SCATTER MEMVAR
			m.pack = cpack
			m.units = .F.
			m.totqty = m.ctnqty
			REPLACE xinwolog.plinqty WITH xinwolog.plinqty+m.ctnqty IN xinwolog
			REPLACE xinwolog.plunitsinqty WITH IIF(lCtnAcct,0,xinwolog.plinqty) IN xinwolog
			m.echo = "FILENAME*"+cfilename
			m.style = ALLTRIM(m.style)
			SELECT inrcv
			SCATTER MEMVAR MEMO BLANK

*!*	Removed the following per Maria E, 03.09.2015
*!*				IF !lSync
*!*					STORE ALLTRIM(m.color) TO  m.cayset
*!*					m.color = ""
*!*				ELSE
			m.color = ALLTRIM(m.color)
*!*				ENDIF

			m.id = ALLTRIM(m.id)
			npo = npo+1
			m.po = TRANSFORM(npo)
			m.plid = m.plid+1
			INSERT INTO xpl FROM MEMVAR
			m.totqty  = INT(VAL(cpack))*m.ctnqty
			m.pack = '1'
			m.units = .T.
			m.plid = m.plid+1
			INSERT INTO xpl FROM MEMVAR
		ENDSCAN

		IF lTesting AND lBrowfiles
			SELECT xinwolog
			LOCATE
			BROWSE
			SELECT xpl
			LOCATE
			BROWSE
		ENDIF

		DO m:\dev\prg\bcnyinbound_import

		SELECT pl
		COUNT TO xnumskus FOR !units AND wo_num=inwolog.wo_num

		xsqlexec("select * from inrcv where container='"+inwolog.CONTAINER+"' and status#'CONFIRMED'","xinrcv",,"wh")
		SELECT xinrcv
		IF RECCOUNT() > 0
			xsqlexec("update inrcv set inwonum="+TRANSFORM(inwolog.wo_num)+" where container='"+inwolog.CONTAINER+"' and status#'CONFIRMED'",,,"wh")
		ELSE
			SELECT inrcv
			SCATTER MEMVAR MEMO BLANK
			m.mod = cMod
			m.office = cOffice
			m.inrcvid=dygenpk("inrcv","whall")  && new get the ID value
			m.inwonum=inwolog.wo_num
			m.accountid=inwolog.accountid
			m.AcctName=inwolog.AcctName
			m.container=inwolog.CONTAINER
			m.status="NOT AVAILABLE"
			m.skus=xnumskus
			m.cartons=inwolog.plinqty
			m.newdt=DATE()
			m.addfrom="IN"
			m.confirmdt=empty2nul(m.confirmdt)
			m.newdt=empty2nul(m.newdt)
			m.dydt=empty2nul(m.dydt)
			m.firstavaildt=empty2nul(m.firstavaildt)
			m.apptdt=empty2nul(m.apptdt)
			m.pickedup=empty2nul(m.pickedup)

			INSERT INTO inrcv FROM MEMVAR
			tu("inrcv")
		ENDIF

		DELETE FILE [&cfilenamexx]

	ENDFOR
	NormalExit = .T.

CATCH TO oErr
	IF !NormalExit
		SET STEP ON
		lnRec = 0
		ptError = .T.
		tsubject = "Inbound Upload Error ("+TRANSFORM(oErr.ERRORNO)+") for "+cCustName+" at "+TTOC(DATETIME())+"  on file "+cfilename
		tattach  = ""
		tmessage = "Error processing Inbound file,GENERIC INBOUND.... Filename: "+cfilename+CHR(13)
		IF EMPTY(cErrMsg)
			tmessage = tmessage+CHR(13)+" Error at record "+TRANSFORM(lnRec)+CHR(13)+CHR(13)
			tmessage =tmessage+ "Try/CATCH Exeception Message:"+CHR(13)+;
				[  Error: ] + STR(oErr.ERRORNO) +CHR(13)+;
				[  LineNo: ] + STR(oErr.LINENO) +CHR(13)+;
				[  Message: ] + oErr.MESSAGE +CHR(13)+;
				[  Procedure: ] + oErr.PROCEDURE +CHR(13)+;
				[  Details: ] + oErr.DETAILS +CHR(13)+;
				[  StackLevel: ] + STR(oErr.STACKLEVEL) +CHR(13)+;
				[  LineContents: ] + oErr.LINECONTENTS+CHR(13)+;
				[  UserValue: ] + oErr.USERVALUE+CHR(13)+;
				[  Machine Name: ] + SYS(0)+CHR(13)+;
				[  Error Message :]+CHR(13)
		ELSE
			tmessage = tmessage+CHR(13)+"Specific Error: "+cErrMsg
		ENDIF

		tsubject = "Inbound WO Error for "+cCustName+" at "+TTOC(DATETIME())+"  on file "+cfilename
		tattach  = ""
		tsendto = tsendtoerr
		tcc = tccerr

		tFrom    ="Toll WMS Inbound EDI Operations <toll-edi-ops@tollgroup.com>"
		DO FORM m:\dev\frm\dartmail2 WITH tsendto,tFrom,tsubject,tcc,tattach,tmessage,"A"
		lCreateErr = .T.
	ENDIF
FINALLY

	WAIT WINDOW "All "+cCustName+" Inbound Complete...Exiting" TIMEOUT 2
	SELECT 0
	USE F:\edirouting\FTPSETUP SHARED
	REPLACE chkbusy WITH .F. FOR FTPSETUP.TRANSFER = cTransfer
	USE IN FTPSETUP
	SET LIBRARY TO
	CLOSE DATABASES ALL
ENDTRY
