lNewPT = .T.

cOutState = IIF(cOfficeUsed = "C","CA",IIF(cOfficeUsed="N","NJ","FL"))
lcFA997Path   = ("f:\ftpusers\tko997out\940xfer\")
lc754inpath   = ("f:\ftpusers\tko-ca\754in\")
lcIN997Path   = ("f:\ftpusers\_997in\tko\")

*CD &lcPath
set step On 


waitstr = "Processing records for "+cMailName
WAIT WINDOW waitstr TIMEOUT 2

IF lNewPack
	lnNum = ADIR(tarray,lcPath+"*.x12")
ELSE
	lnNum = ADIR(tarray,cFilemask)
ENDIF

IF lnNum = 0
	WAIT WINDOW AT 10,20 "No "+cMailName+" 940's to import for "+cOfficename TIMEOUT 3
	USE F:\edirouting\FTPSETUP SHARED
	cTransfer = "940-TKO"
	REPLACE chkbusy WITH .F. FOR FTPSETUP.TRANSFER = cTransfer
	USE IN FTPSETUP
	NormalExit = .T.
	THROW
ENDIF

*ASSERT .F. MESSAGE "At opening of Sams file"
USE "f:\walmart\samsdcs.dbf" IN 0 ALIAS samsdcs

ASSERT .F. MESSAGE "At File process loop"
FOR thisfile = 1  TO lnNum
	lLoop = .F.
	cFilename = ALLTRIM(tarray[thisfile,1])
	nFileSize = VAL(TRANSFORM(tarray(thisfile,2)))
	xfile = lcPath+cFilename
***TMARG  for testing comment below
	archivefile = lcArchivePath+cFilename
	fa997file = lcFA997Path+cFilename
	in997file = lcIN997Path+cFilename
	c754file = lc754inpath+cFilename

	IF LOWER(JUSTEXT(cFilename)) = "txt" AND !lTesting
		COPY FILE [&xfile] TO [&c754file]
		DELETE FILE [&xfile]
		LOOP
	ENDIF

	IF !lTesting
		COPY FILE [&xfile] TO [&fa997file]
	ENDIF

	WAIT WINDOW "Importing file: "+xfile TIMEOUT 2
*	ASSERT .F. MESSAGE "In process prg., at file type check"
	IF FILE(xfile)
		RELEASE ALL LIKE APT
		RELEASE ALL LIKE APTDET
		CREATE CURSOR tempzzz (field1 c(254))
		APPEND FROM [&xfile] TYPE SDF
		LOCATE
		cDelimiter = SUBSTR(field1,4,1)
		IF EMPTY(cDelimiter)
			cMessage = "Delimiter is bad"
			WAIT WINDOW cMessage TIMEOUT 2
			NormalExit = .T.
			THROW
		ENDIF
		USE IN tempzzz

		DO m:\dev\prg\createx856a
		SELECT x856
		ASSERT .F. MESSAGE "In x856 population...debug"
		APPEND FROM [&xfile] TYPE DELIMITED WITH "" WITH CHARACTER &cDelimiter
		LOCATE

		SKIP
		IF ALLTRIM(x856.segment)="GS" AND ALLTRIM(x856.f1) = "FA"
			WAIT WINDOW "This is a 997 ACK file...deleting" TIMEOUT 2
			DELETE FILE [&xfile]
			LOOP
		ENDIF

		IF ALLTRIM(x856.segment)="GS" AND ALLTRIM(x856.f1) = "RG"
			WAIT WINDOW "This is a 754 file...moving" TIMEOUT 2
			COPY FILE [&xfile] TO [&c754file]
			DELETE FILE [&xfile]
			LOOP
		ENDIF

		IF ALLTRIM(x856.segment)="GS" AND ALLTRIM(x856.f1) # "OW"
			WAIT WINDOW "This is an Acknowledgement file...moving" TIMEOUT 2
			COPY FILE [&xfile] TO [&in997file]
			DELETE FILE [&xfile]
			LOOP
		ENDIF
		LOCATE

		IF lNewPack
			LOCATE FOR ALLTRIM(x856.segment)="N9" AND ALLTRIM(x856.f1) = "PKG"
			IF FOUND()
				lPrepack = .T.
				lPick = .F.
			ELSE
				lPrepack = .F.
				lPick = .T.
			ENDIF
		ENDIF

		lMB = .F.  && Michael B. orders for NJ; may be adapted for SP English Laundry
		SET STEP ON
		SELECT x856
		LOCATE FOR ALLTRIM(x856.segment)="W01"
		IF !FOUND()
		ELSE
			IF LEFT(ALLTRIM(x856.f5),1) = "M" AND INLIST(cOffice,"N","I")
				lMB = .T.
				lPrepack = .F.
				lPick = .T.
			ENDIF
		ENDIF
		LOCATE

		DO create_pt_cursors WITH cUseFolder && Added to prep for SQL Server transition

		SELECT xpt
		INDEX ON ship_ref TAG ship_ref
		INDEX ON ptid TAG ptid
		SET ORDER TO ship_ref
*!* End of added code

		DO (cUseDir+"BKDN") WITH cFilename
		IF lLoop
			LOOP
		ENDIF
		
		cCustnameTemp = cCustname
		cCustname = IIF(lMB,cCustname+"-MB",cCustname) 
		set step on 
    DO m:\dev\prg\all940_import
		cCustname = cCustnameTemp
	ENDIF

*	release_ptvars()

NEXT thisfile

&& now clean up the 940in archive files, delete for the upto the last 10 days
deletefile(lcArchivePath,10)

WAIT CLEAR
WAIT WINDOW "ALL "+cMailName+" 940 files finished processing for: "+cOfficename+"..." TIMEOUT 3

*************************************************************************************************
