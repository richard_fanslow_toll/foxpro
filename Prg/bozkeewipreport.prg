* BOZKEEWIPREPORT.PRG
* programmer: Mark Bennett
* Automate Bozkee wip report
* For: Maria Estrella
*
* build EXE as F:\UTIL\BOZKEE\BOZKEEWIPREPORT.EXE

LOCAL lnError, lcProcessName, loBOZKEEWIPREPORT, lTestMode

lTestMode = .F.

IF NOT lTestMode THEN
	runack("BOZKEEWIPREPORT")
ENDIF

DECLARE INTEGER CreateMutex IN Win32API ;
	STRING @lpMutexAttributes, ;
	STRING bInitialOwner, ;
	STRING @lpName

DECLARE INTEGER GetLastError IN Win32API

* if running in EXE check to see if already running
IF _VFP.STARTMODE > 0 THEN
	lcProcessName = "BOZKEEWIPREPORT"
	CreateMutex(0,CHR(1),@lcProcessName)
	lnError = GetLastError()
	IF lnError = 183
		WAIT WINDOW TIMEOUT 10 "Process is already running..."
		RETURN .F.
	ENDIF
ENDIF

utilsetup("BOZKEEWIPREPORT")

loBOZKEEWIPREPORT = CREATEOBJECT('BOZKEEWIPREPORT')
loBOZKEEWIPREPORT.MAIN( lTestMode )

IF NOT lTestMode THEN
	schedupdate()
ENDIF

CLOSE DATABASES ALL
RETURN

#DEFINE LOGIT 1
#DEFINE WAITIT 2
#DEFINE NOWAITIT 4
#DEFINE SENDIT 8
#DEFINE CRLF CHR(13) + CHR(10)
#DEFINE LF CHR(10)
#DEFINE RETURN_DATA_MANDATORY .F.
#DEFINE RETURN_DATA_NOT_MANDATORY .T.

DEFINE CLASS BOZKEEWIPREPORT AS CUSTOM

	cProcessName = 'BOZKEEWIPREPORT'

	lTestMode = .F.  && .F. FOR NORMAL OPERATION, .T. TO SUPPRESS ALL BUT DEVELOPER EMAILS AND CHANGE INPUT TABLE, ETC.

	lAutoYield = .T.
	cStartTime = TTOC(DATETIME())

	* connection properties
	nSQLHandle = 0

	* file properties
	nFileHandle = 0

	* date properties
	dtNow = DATETIME()

	dToday = DATE()

	* processing props
	nMonth = 0
	nYear = 0
	cMod = 'E'
	nAccountID = 6262
	cReportPath = 'F:\UTIL\BOZKEE\REPORTS\'

	* wait window properties
	nWaitWindowTimeout = 2
	lWaitWindowIsOn = .T.

	* logfile properties
	lLoggingIsOn = .T.
	cLogFile = 'F:\UTIL\BOZKEE\LOGFILES\BOZKEEWIPREPORT_log.txt'

	* INTERNAL email properties
	lSendInternalEmailIsOn = .T.
	cFrom = 'TGF Operations <fmi-transload-ops@fmiint.com>'
	cSendTo = 'Sprouting@fmiint.com'
	cCC = 'mbennett@fmiint.com'
	cSubject = 'Bozkee WIP Report for: ' + DTOC(DATE())
	cAttach = ''
	cBodyText = 'See attached report.'

	cCOMPUTERNAME = ''
	cUSERNAME = ''

	FUNCTION INIT
		IF NOT DODEFAULT()
			RETURN .F.
		ENDIF
		WITH THIS
			CLEAR
			*SET RESOURCE OFF
			CLOSE DATA
			SET CENTURY ON
			SET DATE AMERICAN
			SET HOURS TO 24
			SET ANSI ON
			SET TALK OFF
			SET DELETED ON
			SET CONSOLE OFF
			SET EXCLUSIVE OFF
			SET SAFETY OFF
			SET EXACT OFF
			SET STATUS BAR ON
			SET SYSMENU OFF
			SET ENGINEBEHAVIOR 70
			_VFP.AUTOYIELD = .lAutoYield
			.cCOMPUTERNAME = GETENV("COMPUTERNAME")
			.cUSERNAME = GETENV("USERNAME")
			IF .lTestMode THEN
				.cSendTo = 'mbennett@fmiint.com'
				.cLogFile = 'F:\UTIL\BOZKEE\Logfiles\BOZKEEWIPREPORT_log_TESTMODE.txt'
			ENDIF
			.lLoggingIsOn = .lLoggingIsOn AND NOT EMPTY(.cLogFile)
			IF .lLoggingIsOn THEN
				SET ALTERNATE TO (.cLogFile) ADDITIVE
				SET ALTERNATE ON
			ENDIF
		ENDWITH
	ENDFUNC


	FUNCTION DESTROY
		WITH THIS
			IF .lLoggingIsOn  THEN
				SET ALTERNATE OFF
				SET ALTERNATE TO
			ENDIF
		ENDWITH
		DODEFAULT()
	ENDFUNC


	FUNCTION MAIN
		LPARAMETERS tlTestMode

		WITH THIS

			.lTestMode = tlTestMode

			LOCAL lcXLSfolder, lnNumberOfErrors, ldToday, lcReportFile

			lnNumberOfErrors = 0

			TRY
				* NOTE: code below was based on Darren's Lifewip.prg.

				ldToday = .dToday


				IF holiday(ldToday) THEN
					.TrackProgress('Holiday - not running', LOGIT+SENDIT)
					THROW
				ENDIF

				guserid="AUTO"
				gwipbywo=.F.
				gwipbybl=.F.
				xpnpacct=.F.
				xacctname = "BOZKEE (001)"
				xwip=.T.
				xtitle="Work in Progress"

				xsqlexec("select * from account where inactive=0","account",,"qq")
				INDEX ON accountid TAG accountid
				SET ORDER TO

				xsqlexec("select * from outship where accountid=" + ALLTRIM(STR(.nAccountID)),"outship",,"wh")

				goffice="7"
				xsqlexec("select * from pt where mod='E' and accountid=" + ALLTRIM(STR(.nAccountID)),,,"wh",,,,,,,.T.)

				SELECT ship_ref, SPACE(15) AS STATUS, wo_num, wo_date, consignee, cnee_ref, ship_via, ctnqty, qty AS unitqty, ;
					START, CANCEL, called, appt, appt_time, appt_num, apptremarks, del_date, pulled, picked, labeled, staged, truckloaddt, ptdate ;
					FROM outship ;
					WHERE accountid=.nAccountID AND (emptynul(del_date) OR del_date>(ldToday-30)) AND !notonwip ;
					INTO CURSOR xrpt READWRITE

				REPLACE ALL STATUS WITH IIF(!pulled,"NOT PULLED",IIF(!emptynul(del_date),"DELIVERED",IIF(!emptynul(truckloaddt),"LOADED",IIF(!emptynul(staged),"STAGED",IIF(!emptynul(labeled),"LABELED",IIF(!emptynul(picked),"PICKED","PULLED"))))))

				SELECT ship_ref, "UNALLOCATED" AS STATUS, consignee, cnee_ref, ship_via, qty AS unitqty, START, CANCEL, ptdate ;
					FROM pt ;
					WHERE accountid=.nAccountID ;
					ORDER BY ship_ref INTO CURSOR xrpt2

				SCAN
					SCATTER MEMVAR
					INSERT INTO xrpt FROM MEMVAR
				ENDSCAN

				lcReportFile = .cReportPath + "BOZKEE_WIP_" + DTOS(ldToday) + ".XLS"

				SELECT xrpt
				INDEX ON ship_ref TAG ship_ref
				COPY FIELDS ship_ref, STATUS, wo_num, consignee, cnee_ref, ship_via, ctnqty, unitqty, ptdate, wo_date, pulled, staged, del_date, START, CANCEL TO (lcReportFile) XL5


				IF FILE(lcReportFile) THEN
					.cAttach = lcReportFile
				ELSE
					.TrackProgress('===> There was an error attaching report file: ' + lcReportFile, LOGIT+SENDIT)
				ENDIF

				.TrackProgress('Process ' + .cProcessName + ' ended normally.', LOGIT+SENDIT)

			CATCH TO loError

				.TrackProgress('There was an error.',LOGIT+SENDIT)
				.TrackProgress('The Error # is: ' + TRANSFORM(loError.ERRORNO), LOGIT+SENDIT)
				.TrackProgress('The Error Message is: ' + TRANSFORM(loError.MESSAGE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred in Program: ' + TRANSFORM(loError.PROCEDURE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred at line #: ' + TRANSFORM(loError.LINENO), LOGIT+SENDIT)
				lnNumberOfErrors = lnNumberOfErrors + 1

			ENDTRY

			CLOSE DATA
			WAIT CLEAR
			***************** INTERNAL email results ******************************
			.TrackProgress('About to send status email.',LOGIT)
			.TrackProgress('==================================================================================================================', SENDIT)
			.TrackProgress('The logfile is: ' + .cLogFile, SENDIT)
			.TrackProgress(.cProcessName + ' process started: ' + .cStartTime, LOGIT+SENDIT)
			.TrackProgress(.cProcessName + ' process finished: ' + TTOC(DATETIME()), LOGIT+SENDIT)

			IF .lSendInternalEmailIsOn THEN
				* try to trap error from not having dartmail dll's registered on user's pc...
				TRY
					DO FORM dartmail2 WITH .cSendTo,.cFrom,.cSubject,.cCC,.cAttach,.cBodyText,"A"
					.TrackProgress('Sent status email.',LOGIT)
				CATCH TO loError
					.TrackProgress('There was an error sending the status email.',LOGIT)
					.TrackProgress('The Error # is: ' + TRANSFORM(loError.ERRORNO), LOGIT)
					.TrackProgress('The Error Message is: ' + TRANSFORM(loError.MESSAGE), LOGIT)
					.TrackProgress('The Error occurred in Program: ' + TRANSFORM(loError.PROCEDURE), LOGIT)
					.TrackProgress('The Error occurred at line #: ' + TRANSFORM(loError.LINENO), LOGIT)
					lnNumberOfErrors = lnNumberOfErrors + 1
				ENDTRY

			ELSE
				.TrackProgress('Did not send status email: THIS.lSendInternalEmailIsOn = FALSE.',LOGIT)
			ENDIF

		ENDWITH
		RETURN
	ENDFUNC && main


	PROCEDURE TrackProgress
		* do any combination of Wait Window, writing to logfile, and adding to body of email,
		* based on nFlags parameter.
		LPARAMETERS tcExpression, tnFlags
		WITH THIS
			IF BITAND(tnFlags,LOGIT) = LOGIT THEN
				IF .lLoggingIsOn THEN
					?
					? .cProcessName + "  " + TTOC(DATETIME()) + ": " + tcExpression
				ENDIF
			ENDIF
			IF .lWaitWindowIsOn THEN
				IF BITAND(tnFlags,NOWAITIT) = NOWAITIT THEN
					WAIT WINDOW tcExpression NOWAIT
				ENDIF
				IF BITAND(tnFlags,WAITIT) = WAITIT THEN
					WAIT WINDOW tcExpression TIMEOUT .nWaitWindowTimeout
				ENDIF
			ENDIF
			IF BITAND(tnFlags,SENDIT) = SENDIT THEN
				IF .lSendInternalEmailIsOn THEN
					.cBodyText = .cBodyText + tcExpression + CRLF + CRLF
				ENDIF
			ENDIF
		ENDWITH
	ENDPROC  &&  TrackProgress


ENDDEFINE




