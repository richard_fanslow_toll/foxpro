**Marc Jacobs "Dashboard" KPI Spreadsheet creation
parameters xstartdt, xenddt, xshowxls

**remove RETAIL SUPPLIES from gmjacctlist, dont want kpi to reflect
gmjacctlist=strtran(gmjacctlist,",6453","")

if empty(xstartdt)
	xstartdt=gomonth(ctod(left(dtoc(date()),2)+"/01/"+right(dtoc(date()),2)),-1)
	xenddt=gomonth(xstartdt,1)-1
endif

xzmonth=left(dtoc(xstartdt),2)+right(dtoc(xstartdt),2)
xmonth=left(dtoc(xstartdt),2)+"/"+right(dtoc(xstartdt),2)
xmonfld="M"+transform(month(xstartdt))

goffice="J"

xsqlexec("select * from invenloc where mod='"+goffice+"'",,,"wh")
index on whseloc tag whseloc
set order to

xsqlexec("select * from cargo",,,"stuff")
xsqlexec("select * from sales",,,"ar")

use f:\kpi\kpidata\mjloc in 0

if !used("holidays")
	use f:\hr\hrdata\holidays in 0
endif

**create the cursor that will be sent to XLS
create cursor xkpi (item n(2,0), title c(40), kpidesc c(100), kpi c(20))

insert into xkpi (item, title) values (1, "Total DC Productivity")
insert into xkpi (item, title) values (2, "Total DC CPU")
insert into xkpi (item, title) values (3, "DC Turn Time")
insert into xkpi (item, title) values (4, "Total DC Fixed CPU")
insert into xkpi (item, title) values (5, "Total DC Variable CPU")
insert into xkpi (item, title) values (6, "Total Units Received")
**removed per Todd!
*insert into xkpi (item, title) values (9, "Pick Face Location Utilization")
insert into xkpi (item, title) values (10, "DC Storage Utilization")
insert into xkpi (item, title) values (11, "Units On Hand")
insert into xkpi (item, title) values (12, "Inventory Days")
insert into xkpi (item, title) values (13, "Working Days")
insert into xkpi (item, title) values (15, "SKUs On Hand")
insert into xkpi (item, title) values (17, "Total Units Picked & Packed")
insert into xkpi (item, title) values (20, "Total Units Shipped")
insert into xkpi (item, title) values (21, "Shipping Productivity")
insert into xkpi (item, title) values (24, "Total Units Produced")

**included variable charge gl codes: Handling (13), Pick & Pack (31), Supplemental Labor (62)
xvarglcodes="'13','31','62'"
**included fixed charge gl codes: Storage (14)
xfixglcodes="'14'"

select cargo
**no longer get shipped totals from cargo as cargo.gohout is based on pulleddt not del_date - mvw 10/2/13
*sum gohin, gohout to xgohin, xgohout for zmonth=xzmonth and inlist(accountid,&gmjacctlist)
sum gohin to xgohin for zmonth=xzmonth and inlist(accountid,&gmjacctlist)

xsqlexec("select qty from outship where mod='J' and between(del_date,{"+dtoc(xstartdt)+"},{"+dtoc(xenddt)+"}) " + ;
	"and inlist(accountid,"+gmjacctlist+") and sp=0 and notonwip=0",,,"wh")

sum qty to xgohout 

replace kpidesc with "Units Shipped: "+alltrim(transform(xgohout,"999,999,999,999")) in xkpi for item=1
replace kpidesc with "Units Shipped: "+alltrim(transform(xgohout,"999,999,999,999")) in xkpi for item=20
replace kpidesc with "Units Received: "+alltrim(transform(xgohin,"999,999,999,999")), kpi with alltrim(transform(xgohin,"999,999,999,999")) in xkpi for item=6

**need total, fixed and variable costs for items 2, 4 & 5
**no longer use invoice/invdet bc doesnt take credit memos into account, use ar.sales
*!*	select sum(iif(inlist(glcode,&xvarglcodes),invdetamt,0.00)) as varinvamt, sum(iif(inlist(glcode,&xfixglcodes),invdetamt,0.00)) as fixinvamt ;
*!*		from invdet d left join invoice i on i.invoiceid=d.invoiceid ;
*!*		where between(xinvdt,xstartdt,xenddt) and inlist(xaccountid,&gmjacctlist) and !empty(invdetamt) and !split and !void ;
*!*		into cursor xinvtot


select sum(iif(inlist(glcode,&xvarglcodes),&xmonfld,0.00)) as varinvamt, sum(iif(inlist(glcode,&xfixglcodes),&xmonfld,0.00)) as fixinvamt ;
	from sales where inlist(accountid,&gmjacctlist) and year=transform(year(xstartdt)) and compdesc="M JACOBS" ;
	into cursor xinvtot

xvarinvamt=varinvamt
xfixinvamt=fixinvamt
use in xinvtot

replace kpidesc with "Units Shipped: "+alltrim(transform(xgohout,"999,999,999,999"))+", Total Invoiced (All): $"+alltrim(transform(xvarinvamt+xfixinvamt,"999,999,999,999.99")), ;
	kpi with "$"+transform((xvarinvamt+xfixinvamt)/xgohout) in xkpi for item=2
replace kpidesc with "Units Shipped: "+alltrim(transform(xgohout,"999,999,999,999"))+", Total Invoiced (Fixed): $"+alltrim(transform(xfixinvamt,"999,999,999,999.99")), ;
	kpi with "$"+transform(xfixinvamt/xgohout) in xkpi for item=4
replace kpidesc with "Units Shipped: "+alltrim(transform(xgohout,"999,999,999,999"))+", Total Invoiced (Variable): $"+alltrim(transform(xvarinvamt,"999,999,999,999.99")), ;
	kpi with "$"+transform(xvarinvamt/xgohout) in xkpi for item=5


**need avg days for allocated to shipped for pts delivered in the month
**scan thru outship recs summing (totqty * totdays) where totdays = del_date-accepteddt. divide that total by total units shipped
**NOTE: cursor must be named "csrdetail" to use kpichgdate()

xsqlexec("select * from outship where between(del_date,{"+dtoc(xstartdt)+"},{"+dtoc(xenddt)+"} " + ;
	"and inlist(accountid,"+gmjacctlist+") and sp=0 and notonwip=0",,,"wh")

select {} as accepteddt, 000 as totdays, * from outship into cursor csrdetail readwrite

**NOTE: if you are updating entire cursor in kpichgdate, must be at the top of the cursor as it does a do while !eof()
=kpichgdate("start", -3, .t.) &&change start date to start - 3 bus. days

**acceptedDt is the latest of ptDate, wo_date and (start - 3 bus. days) - start changed back 3 days above
replace all accepteddt with iif(ptdate>start, iif(ptdate>wo_date, ptdate, wo_date), iif(start>wo_date, start, wo_date)), ;
	totdays with iif(del_date-accepteddt<=0,0,(del_date-accepteddt))

sum totdays*qty, qty to xtotal, xtotqty

xoctfile="h:\fox\mjkpidash"+xzmonth+"-oct.xls"
copy fields accountid, wo_num, ship_ref, start, ptdate, wo_date, accepteddt, del_date, qty, totdays to &xoctfile type xl5

use in csrdetail

replace kpidesc with "Allocated to Shipped (Avg Days Weighted on Unit Qty):"+transform(xtotal/xtotqty),	kpi with transform(xtotal/xtotqty) in xkpi for item=3


**need total used locations vs total locations (all valid locs in mjloc in kpidata created by steve sykes) for item 10
select m.whseloc, sum(locqty) as totqty from mjloc m left join invenloc l on m.whseloc=l.whseloc into cursor xtemp group by m.whseloc
count to xused for totqty>0
replace kpidesc with "Total Locs: "+transform(reccount("xtemp"))+", Total Used Locs: "+transform(xused), kpi with transform(xused/reccount("xtemp")) in xkpi for item=10
use in xtemp

**need to calculate units on hand at the end of month - eominven + eomundel (wip) totals - for item 11

xsqlexec("select * from eominven where zmonth='"+xzmonth+"' and inlist(accountid,"+gmjacctlist+") and units=.t. and half=.f. and mod='J'",,,"wh")
sum totqty to xinvenqty 

xsqlexec("select * from eomundel where zmonth='"+xzmonth+"' and inlist(accountid,"+gmjacctlist+") and units=.t. and half=.f. and mod='J'",,,"wh")
sum totqty to xwipqty
set step on 
replace kpidesc with "Inven Units: "+alltrim(transform(xinvenqty,"999,999,999,999"))+", Wip Units: "+alltrim(transform(xwipqty,"999,999,999,999")), ;
	kpi with alltrim(transform(xinvenqty+xwipqty,"999,999,999,999")) in xkpi for item=11

**need to calculate (units on hand / (shipped units last 3 months * 4)) * 365 - for item 12

xsqlexec("select qty from outship where between(del_date,{"+dtoc(gomonth(xstartdt,-2))+"},{"+dtoc(xenddt)+"}) " + ;
	"and inlist(accountid,"+gmjacctlist+") and sp=0 and notonwip=0",,,"wh")

sum qty to xgohout2

xkpi=((xinvenqty+xwipqty)/(xgohout2*4))*365
replace kpidesc with "Units On Hand: "+alltrim(transform(xinvenqty+xwipqty,"999,999,999,999"))+", Shipped Last 3 Months: "+alltrim(transform(xgohout2,"999,999,999,999")), ;
	kpi with alltrim(transform(xkpi,"999,999.99")) in xkpi for item=12

**need to calculate number of working days (days in month - weekends & holidays for item 13
**calculate date differential into dateDiff - make necessary adjustments for weekends & FMI holidays
create cursor xtemp (startdt d(8), enddt d(8), diff n(5,0))
insert into xtemp (startdt, enddt) values (xstartdt, xenddt)
DO kpicalcdiff with "startdt", "enddt", "diff"
locate
xworkdays=xtemp.diff
use in xtemp

replace kpidesc with "Working Days (Days in month minus weekends/holidays): "+transform(xworkdays), kpi with transform(xworkdays) in xkpi for item=13

**need skus on hand (eodinven.skucnt) for last day of month for item 15
**NOTE: loop to find last day run in the month, in case the process didnt run for a few days

xsqlexec("select * from eodinven where eoddt>{"+dtoc(date()-7)+"} and mod='J'","eodinven",,"wh")

select eodinven
xdate=xenddt
xfound=.f.
do while xdate>=xstartdt
	locate for eoddt=xdate
	if found()
		xfound=.t.
		exit
	endif
	xdate=xdate-1
enddo

if xfound
	sum skucnt to xskucnt for eoddt=xdate and inlist(accountid,&gmjacctlist)
	replace kpidesc with "SKU Count: "+alltrim(transform(xskucnt,"999,999,999,999")), kpi with alltrim(transform(xskucnt,"999,999,999,999")) in xkpi for item=15
endif

**need total units picked for items 17 & 24

xsqlexec("select qty from outship where picked>={"+dtoc(xstartdt)+"} and picked<={"+dtoc(xenddt)+"} " + ;
	"and inlist(accountid,"+gmjacctlist+") and sp=0 and notonwip=0",,,"wh")

select sum(qty) as totqty from outship into cursor xtemp

replace kpidesc with "Units Picked & Packed: "+alltrim(transform(xtemp.totqty,"999,999,999,999")), kpi with alltrim(transform(xtemp.totqty,"999,999,999,999")) in xkpi for item=17
replace kpidesc with "Units Packed: "+alltrim(transform(xtemp.totqty,"999,999,999,999")), kpi with alltrim(transform(xtemp.totqty,"999,999,999,999")) in xkpi for item=24
use in xtemp

xfile="h:\fox\mjkpidash"+xzmonth+".xls"
select xkpi
copy fields title, kpidesc, kpi to &xfile type xl5

if xshowxls
	public oexcel
	oexcel = createobject("excel.application")

	oworkbook = oexcel.workbooks.open(xfile)
	oexcel.visible = .t.
	release oworkbook, oexcel
else
	xto="mwinter@fmiint.com,mike.winter@tollgroup.com,tmarg@fmiint.com,steven.sykes@tollgroup.com"
	xcc=""

	xattach=xfile+","+xoctfile
	xfrom="TOLL SYSTEM <tollsystem@toll.com>"
	xsubject="MJ KPI Dashboard - "+xmonth
	xmessage="Attached: KPI Dashboard File & Order Cycle Time (Staged to Shipped) Detail File"
	do form dartmail2 with xto,xfrom,xsubject,xcc,xattach,xmessage,"A"
endif

set database to WH
close databases
set database to EX
close databases
set database to AR
close databases

use in mjloc

**should be open before call
*use in holidays
