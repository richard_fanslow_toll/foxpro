Parameters llOpen210,llClose210

*!* Created 2017.1.26, PG


*  for LOS  --> TGFJ
*  for SDG  --> SDIE

lTesting = glBBB_210_Testing

Select curInvoice

If "LOS"$Upper(curInvoice.filename)
  lcSCAC = "TGFJ"
  lcPoolID = "33456"
Endif

If "SDG"$Upper(curInvoice.filename)
  lcSCAC = "TGFJ"
  lcPoolID = "2636"
Endif


Public cWO_Num,cGroupName,cCustName,div214,cST_CSZ,cPrgName
Public cFin_status,cString,nHandle
Dimension thisarray(1)

cPrgName  = "BED BATH 210"

*DO m:\dev\prg\_setvars WITH lTesting
On Escape Cancel

*!*	If lTesting
*!*	  Set Step On
*!*	Endif

Store "" To cShip_ref,cST_CSZ,cMacysnet,cTripid,cLeg,cFileInfo,cFin_status
lIsError = .F.
lDoCatch = .T.
lEmail = .T.
lDoError = .F.
lFilesOut = .T.
nAcctNum = 6695
cOffice = "I"

tfrom = "TOLL EDI Operations <tgf-express-ops@tollgroup.com>"
Select 0
Use F:\3pl\Data\mailmaster Alias mm
Locate For mm.edi_type = "MISC" And mm.taskname = "GENERAL"
tsendto = Iif(mm.use_alt,mm.ccalt,mm.cc)  && Reversed here to put CC into SENDTO
tcc = Iif(mm.use_alt,mm.sendtoalt,mm.sendto)
Use In mm
tattach = ""

cCustName = "BEDBATH"
cCustFolder = "BEDBATH"
cCustPrefix = "BEDBATH"+Lower(cOffice)
dtmail = Ttoc(Datetime())
dt2 = Datetime()
cMailName = Proper("BEDBATH")

IF glBBB_210_Reload THEN
	lc210path = "F:\FTPUSERS\BEDBATH\REINVOICES\210OUT\"
ELSE
	IF lTesting THEN
		lc210path = "F:\FTPUSERS\BEDBATH\TESTDATAIN\TEST210OUT\"
	ELSE
		* REVISED 11/21/2017 so that the 210s are created in PRE210OUT folder; then moved to 210OUT	only on successful completion of the entire process.
		*lc210path = "f:\ftpusers\BEDBATH\210out\"
		lc210path = "F:\FTPUSERS\Bedbath\PRE210OUT\"
	ENDIF
ENDIF

Cd &lc210path

cterminator = ">"  && Used at end of ISA segment
cfd = "*"  && Field delimiter
csegd = Chr(13) && CR
csendqual = "02"  && Sender qualifier
csendid = "TGFUSA"  && Sender ID code
crecqual = "ZZ"  && Recip qualifier
crecid = "BBA0444"   && Recip ID Code, production (05.11.2010)

cString = ""

nLXNum = 1

cISACode = Iif(lTesting,"T","P")

Wait Clear
Wait Window "Now creating Bed Bath 210 information..." Nowait

If llOpen210  && open the 210 enevelope

  cDate = Dtos(Date())
  cTruncDate = Right(cDate,6)
  cTruncTime = Substr(Ttoc(Datetime(),1),9,4)
  cfiledate = cDate+cTruncTime
  csendidlong = Padr(csendid,15," ")
  crecidlong = Padr(crecid,15," ")

  dt1 = Ttoc(Datetime(),1)
  xmaxbbb = sqlgenpk("BBB-"+Transform(Date()),"wh")
  m.c210FileName = ("TGFJ"+dt1+"."+Padl(xmaxbbb,3,"0"))
  c210FilenameShort = Justfname(cFilename)
  c210Archive = ("f:\ftpusers\"+cCustFolder+"\210OUT\archive\"+m.c210FileName)
  *c210Filename3 = ("f:\ftpusers\"+cCustFolder+"\210OUT\"+c210Filename)
  nHandle = Fcreate(m.c210FileName)

  nSTCount = 0

*SET STEP ON
  Do num_incr_isa
  cISA_Num = Padl(c_CntrlNum,9,"0")

  Store "ISA"+cfd+"00"+cfd+"          "+cfd+"00"+cfd+"          "+cfd+csendqual+cfd+csendidlong+cfd+;
  crecqual+cfd+crecidlong+cfd+cTruncDate+cfd+cTruncTime+cfd+"U"+cfd+"00200"+cfd+;
  cISA_Num+cfd+"0"+cfd+cISACode+cfd+cterminator+csegd To cString
  Do cstringbreak

  Store "GS"+cfd+"QM"+cfd+csendid+cfd+crecid+cfd+cDate+cfd+cTruncTime+cfd+c_CntrlNum+;
  cfd+"X"+cfd+"004010VICS"+csegd To cString
  Do cstringbreak

Endif

nSegCtr = 0
Do num_incr_st
Store "ST"+cfd+"210"+cfd+Padl(c_GrpCntrlNum,9,"0")+csegd To cString
Do cstringbreak
nSTCount = nSTCount + 1
nSegCtr = nSegCtr + 1
Set Century On
Set Date YMD


Store "B3"+cfd+"B"+cfd+Alltrim(lcInvoiceNum)+cfd+Alltrim(curInvoice.manifest)+cfd+"CC"+cfd+cfd+Dtos(Date())+cfd+Alltrim(Transform(curInvoice.amount*100))+cfd+cfd+Dtos(Date())+cfd+"003"+cfd+Alltrim(lcSCAC)+cfd+Dtos(Date())+csegd To cString
Do cstringbreak
nSegCtr = nSegCtr + 1

Store "N1"+cfd+"BT"+cfd+"BERMAN BLAKE ASSOCIATES INC."+cfd+"25"+cfd+lcPoolID+csegd To cString
Do cstringbreak
nSegCtr = nSegCtr + 1

Store "N3"+cfd+"210 CROSSWAYS PARK DRIVE"+csegd To cString
Do cstringbreak
nSegCtr = nSegCtr + 1

Store "N4"+cfd+"WODDBURY"+cfd+"NY"+cfd+"11797"+cfd+"USA"+csegd To cString
Do cstringbreak
nSegCtr = nSegCtr + 1

Store "N1"+cfd+Iif(curInvoice.Type = "IN","CN","SH")+cfd+"TOLL GLOBAL FORWARDING"+cfd+"25"+cfd+lcPoolID+csegd To cString
Do cstringbreak
nSegCtr = nSegCtr + 1

Store "N3"+cfd+"3355 DULLES DRIVE"+csegd To cString
Do cstringbreak
nSegCtr = nSegCtr + 1

Store "N4"+cfd+"MIRA LOMA"+cfd+"CA"+cfd+"91752"+csegd To cString
Do cstringbreak
nSegCtr = nSegCtr + 1


Store "N1"+cfd+Iif(curInvoice.Type = "IN","SH","CN")+cfd+Alltrim(CurInvoice.shipname)+csegd To cString
Do cstringbreak
nSegCtr = nSegCtr + 1

Store "N3"+cfd+Alltrim(CurInvoice.addr1)+csegd To cString
Do cstringbreak
nSegCtr = nSegCtr + 1

If Len(alltrim(Alltrim(CurInvoice.addr2))) > 1
  Store "N3"+cfd+Alltrim(CurInvoice.addr2)+csegd To cString
  Do cstringbreak
  nSegCtr = nSegCtr + 1
Endif 

Store "N4"+cfd+Alltrim(CurInvoice.city)+cfd+Alltrim(CurInvoice.state)+cfd+Alltrim(CurInvoice.zip)+csegd To cString
Do cstringbreak
nSegCtr = nSegCtr + 1


*N1*CN*LAS VEGAS LA EXPRESS C/O BBB~
*N3*1000 S. CUCAMONGA AVE.~
*N4*ONTARIO*CA*91761*USA~

Store "N9"+cfd+"MB"+cfd+Alltrim(curInvoice.manifest)+csegd To cString
Do cstringbreak
nSegCtr = nSegCtr + 1

Store "N9"+cfd+"ST"+cfd+Alltrim(lcPoolID)+csegd To cString
Do cstringbreak
nSegCtr = nSegCtr + 1

Store "N7"+cfd+cfd+Alltrim(curInvoice.trailer)+csegd To cString
Do cstringbreak
nSegCtr = nSegCtr + 1
**Freight Charges

xxi = 1  && do this for the number of different FAreight class types in the summarized data set

Store "LX"+cfd+Alltrim(Transform(xxi))+csegd To cString
Do cstringbreak
nSegCtr = nSegCtr + 1

Store "L5"+cfd+Alltrim(Transform(xxi))+cfd+"CARTON HANDLING"+csegd To cString
Do cstringbreak
nSegCtr = nSegCtr + 1

** HIN or HOUT
Store "L0"+cfd+Alltrim(Transform(xxi))+cfd+Alltrim(Transform(Str(curInvoice.rate,4,2)))+cfd+"EA"+cfd+Alltrim(Transform(curInvoice.weight))+cfd+"N"+cfd+cfd+cfd+Alltrim(Transform(curInvoice.qty))+cfd+"CTN"+csegd To cString
Do cstringbreak
nSegCtr = nSegCtr + 1

xxi = 2  && do this for the number of different FAreight class types in the summarized data set

Store "LX"+cfd+Alltrim(Transform(xxi))+csegd To cString
Do cstringbreak
nSegCtr = nSegCtr + 1

*!*  STORE "L5"+cfd+alltrim(Transform(xxi))+cfd+cfd+"CARTONS FAK"+cfd+cfd+"N"+csegd TO cString
*!*  DO cstringbreak
*!*  nSegCtr = nSegCtr + 1
If curInvoice.Type = "IN"
  lcXtype ="HNI"
Else
  lcXtype ="HNO"
Endif

&&L1*4*19**1884****FUE~

Store "L1"+cfd+Alltrim(Transform(xxi))+cfd+Alltrim(Transform(Str(curInvoice.rate,4,2)))+cfd+"CO"+cfd+Alltrim(Transform(curInvoice.amount*100))+cfd+cfd+cfd+cfd+Alltrim(lcXtype)+cfd+cfd+cfd+cfd+lcXtype+csegd To cString
Do cstringbreak
nSegCtr = nSegCtr + 1


&& L3*19391*N***43751******1028~

Store "L3"+cfd+Alltrim(Transform(curInvoice.weight))+cfd+"N"+cfd+cfd+cfd+Alltrim(Transform(curInvoice.amount*100))+cfd+cfd+cfd+cfd+cfd+cfd+Alltrim(Transform(curInvoice.qty))+csegd To cString
Do cstringbreak
nSegCtr = nSegCtr + 1


Store "SE"+cfd+Alltrim(Str(nSegCtr+1))+cfd+Padl(c_GrpCntrlNum,9,"0")+csegd To cString
Do cstringbreak


***************************************************************************************************************************8
If llClose210  && close the enevelope
  Store  "GE"+cfd+Alltrim(Str(nSTCount))+cfd+c_CntrlNum+csegd To cString
  Do cstringbreak
  Store  "IEA"+cfd+"1"+cfd+Padl(c_CntrlNum,9,"0")+csegd To cString
  Do cstringbreak
  lClosedFile=Fclose(nHandle)
Endif

Return
*********************************************************************************************************************
Procedure cstringbreak
****************************
*  ASSERT .f. MESSAGE "In cStringBreak proc"
cLen = Len(Alltrim(cString))
Fputs(nHandle,cString)
Return

****************************
Procedure num_incr_isa
****************************

If !Used("serfile")
  Use ("F:\3pl\data\serial\bedbath_210_serial") In 0 Alias serfile
Endif
Select serfile
If lTesting
  nOrigSeq = serfile.seqnum
  nOrigGrpSeq = serfile.grpseqnum
  lISAFlag = .F.
Endif
nISA_Num = serfile.seqnum
c_CntrlNum = Alltrim(Str(serfile.seqnum))
Replace serfile.seqnum With serfile.seqnum + 1 In serfile
Endproc

****************************
Procedure num_incr_st
****************************

If !Used("serfile")
  Use ("F:\3pl\data\serial\bedbath_210_serial") In 0 Alias serfile
Endif
Select serfile
If lTesting
  nOrigSeq = serfile.seqnum
  nOrigGrpSeq = serfile.grpseqnum
  lSTFlag = .F.
Endif
c_GrpCntrlNum = Alltrim(Str(serfile.grpseqnum))
Replace serfile.grpseqnum With serfile.grpseqnum + 1 In serfile
Endproc

*********************************************************************************

*!*  If lClosedFile And lTesting
*!*    Wait Window "LL File closed successfully" Timeout 1
*!*  *    MODIFY FILE &cFilename
*!*    Release nHandle
*!*  Endif

*!*  Do ediupdate With "210 CREATED",.F.
*!*  *SET STEP ON
*!*  If lFilesOut
*!*    Copy File &c210Filename To &c210Filename2
*!*    Copy File &c210Filename To &c210Filename3
*!*    Delete File &c210Filename
*!*  Endif
*!*  If !lTesting
*!*    If !Used("ftpedilog")
*!*      Select 0
*!*      Use F:\edirouting\ftpedilog Alias ftpedilog
*!*      Insert Into ftpedilog (TRANSFER,ftpdate,filename,acct_name,Type) ;
*!*      VALUES ("214-"+cCustName,dt2,cFilename,Upper(cCustName),"214")
*!*      Use In ftpedilog
*!*    Endif
*!*  Endif

*!*  **create an ftpjob if this is the last edi_trigger rec for an east penn 210 - mvw 02/06/14
*!*  Select edi_trigger
*!*  xrecno=Recno()
*!*  Locate For accountid=6293 And edi_type="210" And !processed While !Eof()

*!*  If !Found()
*!*    xused=Used("ftpjobs")
*!*    If !xused
*!*      Use F:\edirouting\ftpjobs In 0
*!*    Endif
*!*    Insert Into ftpjobs (jobname, Userid, jobtime) Values ("210-EASTPENN","AUTO",Datetime())
*!*    If !xused
*!*      Use In ftpjobs
*!*    Endif
*!*  Endif
*!*  Select edi_trigger
*!*  Locate

*!*  *!* Finish processing
*!*  If lTesting
*!*    tsubject = "EAST PENN 210 *TEST* EDI from TGF as of "+dtmail
*!*  Else
*!*    tsubject = "EAST PENN 210 EDI from TGF as of "+dtmail
*!*  Endif

*!*  tmessage = cMailName+" 214 EDI FILE (TGF trucking WO# "+cWO_Num+")"
*!*  tmessage = tmessage+Chr(13)+"has been created for the following:"+Chr(13)
*!*  tmessage = tmessage+Chr(13)+cFileInfo
*!*  If lTesting
*!*    tmessage = tmessage + Chr(13)+Chr(13)+"This is a TEST RUN..."
*!*  Endif

*!*  If lEmail
*!*    Do Form dartmail2 With tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
*!*  Endif
*!*  Release All Like c_CntrlNum,c_GrpCntrlNum
*!*  Release All Like nOrigSeq,nOrigGrpSeq
*!*  Wait Clear
*!*  Wait Window cMailName+" 210 EDI File output complete" At 20,60 Timeout 2
*!*  closefiles()

*!*  *!*  CATCH TO oErr
*!*  *!*    IF lDoCatch
*!*  *!*      ASSERT .F. MESSAGE "In CATCH...debug"
*!*  *!*      lEmail = .T.

*!*  *!*      tattach  = ""
*!*  *!*      tmessage = cCustName+" Error processing "+CHR(13)
*!*  *!*      tmessage = tmessage+TRIM(PROGRAM())+CHR(13)

*!*  *!*      tmessage =tmessage+ "Try/CATCH Exeception Message:"+CHR(13)+;
*!*  *!*        [  Error: ] + STR(oErr.ERRORNO) +CHR(13)+;
*!*  *!*        [  LineNo: ] + STR(oErr.LINENO) +CHR(13)+;
*!*  *!*        [  Message: ] + oErr.MESSAGE +CHR(13)+;
*!*  *!*        [  Procedure: ] + oErr.PROCEDURE +CHR(13)+;
*!*  *!*        [  Details: ] + oErr.DETAILS +CHR(13)+;
*!*  *!*        [  StackLevel: ] + STR(oErr.STACKLEVEL) +CHR(13)+;
*!*  *!*        [  LineContents: ] + oErr.LINECONTENTS+CHR(13)+;
*!*  *!*        [  UserValue: ] + oErr.USERVALUE

*!*  *!*      tsubject = "214 Process Error at "+TTOC(DATETIME())
*!*  *!*      tattach  = ""
*!*  *!*      tccerr="joe.bianchi@tollgroup.com"
*!*  *!*      tsendtoerr ="pgaidis@fmiint.com,mwinter@fmiint.com"
*!*  *!*      tfrom    ="TGF EDI 210 Process Ops <toll-edi-ops@tollgroup.com>"
*!*  *!*      DO FORM dartmail2 WITH tsendtoerr,tfrom,tsubject,tccerr,tattach,tmessage,"A"
*!*  *!*      lEmail = .F.
*!*  *!*    ENDIF
*!*  *!*  FINALLY
*!*  *!*    closefiles()
*!*  *!*  ENDTRY

*!*  && END OF MAIN CODE SECTION

*!*  **************************
*!*  Procedure ediupdate
*!*  **************************
*!*  Parameters cFin_status,lIsError
*!*  *  ASSERT .F. MESSAGE "In EDIUPDATE"
*!*  *  SET STEP ON
*!*  lDoCatch = .F.
*!*  Select edi_trigger
*!*  Locate
*!*  If lIsError
*!*    Set Step On
*!*    Replace edi_trigger.processed With .T.,edi_trigger.created With .F.,;
*!*    edi_trigger.fin_status With cFin_status,edi_trigger.errorflag With .T.;
*!*    edi_trigger.when_proc With Datetime() ;
*!*    FOR edi_trigger.bol = cInv_Num And edi_trigger.accountid = 6293 ;
*!*    AND edi_trigger.edi_type ="210 "
*!*    closefiles()
*!*    If cFin_status # "TOTAL INVOICE AMOUNT DOESNT MATCH DETAIL"  && No need for Joe & Paul to receive this notice
*!*      errormail(cFin_status)
*!*    Endif
*!*    =Fclose(nHandle)
*!*    If File(cFilename)
*!*      Delete File [&cFilename]
*!*    Endif
*!*    Throw
*!*  Else
*!*    Replace edi_trigger.processed With .T.,edi_trigger.created With .T.,;
*!*    edi_trigger.when_proc With Datetime(),;
*!*    edi_trigger.fin_status With "210 CREATED";
*!*    edi_trigger.errorflag With .F.,file214 With cFilename ;
*!*    FOR edi_trigger.bol = cInv_Num And edi_trigger.accountid = 6293 ;
*!*    AND edi_trigger.edi_type ="210 "
*!*  Endif
*!*  Endproc

*!*  ****************************
*!*  Procedure errormail
*!*  ****************************
*!*  Parameters cFin_status
*!*  Set Step On
*!*  lDoCatch = .F.

*!*  Assert .F. Message "At Errormail...Debug"
*!*  tsubject = cCustName+" 210 EDI File Error"
*!*  tmessage = "210 EDI File Error, WO# "+cWO_Num+Chr(13)
*!*  tmessage = tmessage+Chr(13)+Chr(13)+"Error: "+cFin_status

*!*  If lTesting
*!*    tmessage = tmessage + Chr(13)+Chr(13)+"This is a TEST run."
*!*  Endif
*!*  If lEmail
*!*    Do Form dartmail2 With tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
*!*  Endif
*!*  Endproc

*!*  *********************
*!*  Procedure closefiles
*!*  *********************
*!*  If Used('ftpedilog')
*!*    Use In ftpedilog
*!*  Endif
*!*  If Used('serfile')
*!*    Use In serfile
*!*  Endif
*!*  If Used('fedcodes')
*!*    Use In FEDCODES
*!*  Endif
*!*  If Used('account')
*!*    Use In account
*!*  Endif
*!*  If Used('xref')
*!*    Use In xref
*!*  Endif
*!*  Endproc


*!*  ****************************
*!*  Procedure segmentget
*!*  ****************************

*!*  Parameter thisarray,lcKey,nLength

*!*  For i = 1 To nLength
*!*    If i > nLength
*!*      Exit
*!*    Endif
*!*    lnEnd= At("*",thisarray[i])
*!*    If lnEnd > 0
*!*      lcThisKey =Trim(Substr(thisarray[i],1,lnEnd-1))
*!*      If Occurs(lcKey,lcThisKey)>0
*!*        Return Substr(thisarray[i],lnEnd+1)
*!*        i = 1
*!*      Endif
*!*    Endif
*!*  Endfor

*!*  Return ""

*!*  ****************************
*!*  Procedure close214
*!*  ****************************
*!*  Store  "SE"+cfd+Alltrim(Str(nSegCtr+1))+cfd+Padl(c_GrpCntrlNum,9,"0")+csegd To cString
*!*  Do cstringbreak

*!*  Store  "GE"+cfd+Alltrim(Str(nSTCount))+cfd+c_CntrlNum+csegd To cString
*!*  Do cstringbreak

*!*  Store  "IEA"+cfd+"1"+cfd+Padl(c_CntrlNum,9,"0")+csegd To cString
*!*  Do cstringbreak

*!*  Endproc


