Parameters lcASN

Public c_CntrlNum,c_GrpCntrlNum,cWO_Num,cGroupName,cDivision,cCustName,cFilename,div214,cST_CSZ
Public tsendto,tcc,tsendtoerr,tccerr,cFin_Status,lDoCatch,lDoJfilesout

lTesting = .F.
lEmail = !lTesting

*Close Data All

Do m:\dev\prg\_setvars With lTesting

If lTesting
  Close Data All
Endif

lDofilesout=.T.

cCustName="OfficeDepot"
cFilename = ""
lFClose = .T.
lIsError = .F.
lDoCatch = .T.
cShip_ref = ""
cST_CSZ = ""
cFin_Status = ""
nFilenum = 0
dt1	 = Ttoc(Datetime(),1)
cFin_Status = "UNEXPECTED"

If Empty(lcASN)
  Messagebox("Need some parameters",0,"Office Depot Receiving")
  return
Endif 

*ASSERT .f.
Try
  cSystemName = "UNASSIGNED"
  nAcctNum = 6494
  cWO_Num = ""
  lDoError = .F.

  xReturn = "XX"
  If !lTesting
    Do m:\dev\prg\wf_alt With "C",6494
    cUseFolder = Upper(xReturn)
  Else
    cUseFolder = "f:\whp\whdata\"
  Endif

  If !Used("ctnucc")
    Use (cUseFolder+"ctnucc") In 0  Shared
  Endif   

  Select ctnucc
  Locate For container = lcASN
  If !Found()
    Messagebox("Cant locate this ASN in the database",0,"Office Depot Receipt Gemeration")
    cFin_Status = "Cant find ASN: "+lcASN
    throw
  Else
    nWo_num = ctnucc.inwonum
  Endif 
  
  Select Store From ctnucc Where inwonum = nWO_Num  Into Cursor instores Group By Store

  Select 0
  Use F:\3pl\Data\mailmaster Alias mm Shared
  Locate For edi_type = "861" And accountid = nAcctNum
  Scatter Memvar
  _Screen.Caption = Alltrim(mm.scaption)
  tsendto = Iif(mm.use_alt,mm.sendtoalt,mm.sendto)
  tcc = Iif(mm.use_alt,mm.ccalt,mm.cc)

  cFilename      = Alltrim(mm.holdpath)+("ODRCV_"+Alltrim(lcASN)+"_"+dt1+".EDI")
  cFilenameShort = Justfname(cFilename)
  cFilenameArch  = (Alltrim(mm.archpath)+cFilenameShort)
  cFilenameOut   = (Alltrim(mm.basepath)+cFilenameShort)
  Locate
  Locate For mm.edi_type = "MISC" And mm.taskname = "GENERAL"
  tsendtoerr = Iif(mm.use_alt,mm.sendtoalt,mm.sendto)
  tccerr = Iif(mm.use_alt,mm.ccalt,mm.cc)
  Use In mm

  tfrom = "TGF Warehouse Operations <warehouse-ops@fmiint.com>"
  tattach = ""

  Select 0
  nFilenum = Fcreate(cFilename)
  If !nFilenum > 0
    cFin_Status = "Cant Create file"
    Throw
  Endif

  cfd = "*"  && Field delimiter
  csegd = ""  && Segment delimiter
  cterminator = ">"  && Used at end of ISA segment

  csendqual = "ZZ"  && Sender qualifier
  csendid = "TGF"   && Sender ID code
  crecqual = "ZZ"   && Recip qualifier
  If lTesting
    crecid = "1ODAS2"   && Recip ID Code
  Else
    crecid = "OFFICEDEPOT"   && Recip ID Code
  Endif
  cDate = Dtos(Date())
  cTruncDate = Right(cDate,6)
  cTruncTime = Substr(Ttoc(Datetime(),1),9,4)
  cfiledate = cDate+cTruncTime
  csendidlong = Padr(csendid,15," ")
  crecidlong = Padr(crecid,15," ")
  cString = ""

  nSTCount = 0
  nSegCtr = 0
  nLXNum = 1

  Store "" To cEquipNum,cB10
  lnOverallTotalCartons=0
  
  Wait Clear
  Wait Window "Now creating an Office Depot 861.........." Nowait

  cISACode = Iif(lTesting,"T","P")

  Do num_incr_isa
  cISA_Num = Padl(c_CntrlNum,9,"0")

  Store "ISA"+cfd+"00"+cfd+"          "+cfd+"00"+cfd+"          "+cfd+csendqual+cfd+csendidlong+cfd+;
  crecqual+cfd+crecidlong+cfd+cTruncDate+cfd+cTruncTime+cfd+"U"+cfd+"00401"+cfd+;
  cISA_Num+cfd+"0"+cfd+cISACode+cfd+":"+csegd To cString  && +cfd+cterminator
  Do cstringbreak

  Store "GS"+cfd+"RC"+cfd+csendid+cfd+crecid+cfd+cDate+cfd+cTruncTime+cfd+c_CntrlNum+;
  cfd+"X"+cfd+"004010"+csegd To cString
  Do cstringbreak

  Do num_incr_st

  Store "ST"+cfd+"861"+cfd+Padl(c_GrpCntrlNum,9,"0")+csegd To cString
  Do cstringbreak
  nSegCtr = nSegCtr + 1

  Select instores
  Scan

    Select * From ctnucc Where inwonum = nWO_Num And Store = instores.Store And totqty =1 Into Cursor cartons
    lnCtnCount = Reccount("Cartons")

    Select * From ctnucc Where inwonum = nWO_Num And Store = instores.Store And Empty(bol) And totqty =1 Into Cursor overagecartons
    lnCtnCountOverage = Reccount("overagecartons")

    Select * From ctnucc Where inwonum = nWO_Num And Store = instores.Store And cartonstatus="01" And totqty =1 Into Cursor damagecartons
    lnCtnCountDamage = Reccount("damagecartons")

    lnOverallTotalCartons = lnOverallTotalCartons+lnCtnCount

    nSTCount = nSTCount + 1
    cRefID = "BROKERREF"

&& 00003 = Pooler BOL #
    Store "BRA"+cfd+Alltrim(cartons.bol)+cfd+cDate+cfd+"00"+cfd+"1"+csegd To cString
    Do cstringbreak
    nSegCtr = nSegCtr + 1


    Store "DTM"+cfd+"242"+cfd+cDate+cfd+cTruncTime+csegd To cString
    Do cstringbreak
    nSegCtr = nSegCtr + 1

    Store "DTM"+cfd+"243"+cfd+cDate+cfd+cTruncTime+csegd To cString
    Do cstringbreak
    nSegCtr = nSegCtr + 1

    Store "TD1"+cfd+"CTN"+cfd+Alltrim(Transform(lnCtnCount))+csegd To cString  && number od Ctns on the BOL
    Do cstringbreak
    nSegCtr = nSegCtr + 1

    Store "TD5"+cfd+cfd+cfd+cfd+"M"+csegd To cString  && Motor
    Do cstringbreak
    nSegCtr = nSegCtr + 1

    Store "TD3"+cfd+"TL"+cfd+cfd+Alltrim(cartons.Container)+csegd To cString  && Motor
    Do cstringbreak
    nSegCtr = nSegCtr + 1

    Store "N1"+cfd+"WH"+cfd+cfd+"91"+cfd+"3061"+csegd To cString  && DC
    Do cstringbreak
    nSegCtr = nSegCtr + 1

    Store "N1"+cfd+"7P"+cfd+cfd+"91"+cfd+"306100002"+csegd To cString  && Pool Agent
    Do cstringbreak
    nSegCtr = nSegCtr + 1

    Store "N1"+cfd+"SN"+cfd+cfd+"91"+cfd+Alltrim(Transform(cartons.Store))+csegd To cString  && Store number
    Do cstringbreak
    nSegCtr = nSegCtr + 1

    lnThisStoreCtns =0  && OK, lets count the Ctns for this store
    Select cartons
    Scan
      lnThisStoreCtns = lnThisStoreCtns +1
    Endscan

    Store "RCD"+cfd+Alltrim(Transform(nSegCtr))+cfd+cfd+cfd+cfd+cfd+Alltrim(Transform(lnThisStoreCtns-lnCtnCountDamage))+cfd+"CT"+cfd+"07"+csegd To cString  && one ctn short
    Do cstringbreak
    nSegCtr = nSegCtr + 1

    If lnCtnCountOverage > 0
      Store "RCD"+cfd+Alltrim(Transform(nSegCtr))+cfd+cfd+cfd+cfd+cfd+Alltrim(Transform(lnCtnCountOverage))+cfd+"CT"+cfd+"03"+csegd To cString  && one ctn short
      Do cstringbreak
      nSegCtr = nSegCtr + 1
    Endif   

    If lnCtnCountDamage > 0
      Store "RCD"+cfd+Alltrim(Transform(nSegCtr))+cfd+cfd+cfd+cfd+cfd+Alltrim(Transform(lnCtnCountDamage))+cfd+"CT"+cfd+"01"+csegd To cString  && one ctn short
      Do cstringbreak
      nSegCtr = nSegCtr + 1
    Endif   

    Select cartons
    Locate For inwonum =nWO_Num
    If Found()
      Store "REF"+cfd+"BM"+cfd+Alltrim(cartons.bol)+csegd To cString  && Store number
    Else
      Store "REF"+cfd+"BM"+cfd+"UNK"+csegd To cString  && Store number
    Endif
    Do cstringbreak
    nSegCtr = nSegCtr + 1

*    Store "REF"+cfd+"BM"+cfd+"40204907316494988901"+csegd To cString  && Store number

    Select cartons
    Scan

      Store "MAN"+cfd+"AI"+cfd+cartons.ucc+csegd To cString  && Ctn numbers
      Do cstringbreak
      nSegCtr = nSegCtr + 1

    Endscan

&& closing segments
  Endscan

  Store  "SE"+cfd+Alltrim(Str(nSegCtr+1))+cfd+Padl(c_GrpCntrlNum,9,"0")+csegd To cString
  Do cstringbreak
  Store  "GE"+cfd+"1"+cfd+c_CntrlNum+csegd To cString
  Do cstringbreak
  Store  "IEA"+cfd+"1"+cfd+Padl(c_CntrlNum,9,"0")+csegd To cString
  Do cstringbreak

  If nFilenum>0
    =Fclose(nFilenum)
  Endif

  If !lTesting
    If !Used("edi_trigger")
      use f:\3pl\data\edi_trigger In 0
    Endif    
    Insert Into edi_trigger (edi_type,accountid,office,processed,errorflag,fin_status,trig_time,when_proc,bol,consignee,trig_from,file945,outshipid) Values ;
                           ("861",6494,"C",.t.,.f.,"861 Receipt",Datetime(),Datetime(),Transform(nWO_num),"Office Depot","ODSystem",cFilename,lnOverallTotalCartons)
  Endif

  Copy File &cFilename To &cFilenameArch

  If lDofilesout
    Copy File &cFilename To &cFilenameOut
    Delete File &cFilename
  Endif

  && close this ASN as after its been received we should not receive any more cartons
  
  Select ctnucc
  replace asnstatus With "C" , rcv861sent With .t., rcv861file With Justfname(cFilename) For container = lcASN 
  
  tsubject = "Office Depot 861 EDI from Toll "+ttoc(Datetime())
  tmessage = "861-EDI Info from TGF-CA  for Office Depot has been created."+Chr(13)+"Filename: "+cFilename+Chr(13)+;
  "Total of "+Transform(lnOverallTotalCartons)+" Cartons"
  If lEmail
    Do Form dartmail2 With tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
  Endif

  Wait Clear
  Wait Window " EDI File output complete" At 20,60 Nowait Timeout 3


Catch To oErr
  Set Step On
  If lDoCatch
    lEmail = .T.

    tsubject = cCustName+" Error ("+Transform(oErr.ErrorNo)+") at "+Ttoc(Datetime())
    tattach  = ""
    tsendto  = "pgaidis@fmiint.com"
    tmessage = ""

    tmessage =tmessage+ "Try/CATCH Exeception Message:"+Chr(13)+;
    [  Error: ] + Str(oErr.ErrorNo) +Chr(13)+;
    [  LineNo: ] + Str(oErr.Lineno) +Chr(13)+;
    [  Message: ] + oErr.Message +Chr(13)+;
    [  Procedure: ] + oErr.Procedure +Chr(13)+;
    [  Details: ] + oErr.Details +Chr(13)+;
    [  StackLevel: ] + Str(oErr.StackLevel) +Chr(13)+;
    [  LineContents: ] + oErr.LineContents+Chr(13)+;
    [  UserValue: ] + oErr.UserValue

    tmessage = tmessage+Chr(13)+cFin_Status

    tsubject = "Office Depot 861 Receipt Creator Error at "+Ttoc(Datetime())
    tattach  = ""
    tcc=""
    tfrom    ="TGF EDI Operations <transload-ops@fmiint.com>"
    Do Form dartmail2 With tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
    lEmail = .F.
  Endif
Finally
  Set Hours To 12
  Fclose(nFilenum)
Endtry

&& END OF MAIN CODE SECTION



****************************
Procedure num_incr_isa
****************************

If !Used("serfile")
  Use ("f:\3pl\data\serial\"+cCustName+"_serial") In 0 Alias serfile
Endif

Select serfile
If lTesting
  nOrigSeq = serfile.seqnum
  nOrigGrpSeq = serfile.grpseqnum
  lISAFlag = .F.
Endif

nISA_Num = serfile.seqnum
c_CntrlNum = Alltrim(Str(serfile.seqnum))
Replace serfile.seqnum With serfile.seqnum + 1 In serfile
Use In serfile

Endproc

****************************
Procedure num_incr_st
****************************

If !Used("serfile")
  Use ("f:\3pl\data\serial\"+cCustName+"_serial") In 0 Alias serfile
Endif

Select serfile
If lTesting
  nOrigSeq = serfile.seqnum
  nOrigGrpSeq = serfile.grpseqnum
  lSTFlag = .F.
Endif

c_GrpCntrlNum = Alltrim(Str(serfile.grpseqnum))
Replace serfile.grpseqnum With serfile.grpseqnum + 1 In serfile
Use In serfile

Endproc

****************************
Procedure segmentget
****************************

Parameter thisarray,lcKey,nLength

For i = 1 To nLength
  If i > nLength
    Exit
  Endif
  lnEnd= At("*",thisarray[i])
  If lnEnd > 0
    lcThisKey =Trim(Substr(thisarray[i],1,lnEnd-1))
    If Occurs(lcKey,lcThisKey)>0
      Return Substr(thisarray[i],lnEnd+1)
      i = 1
    Endif
  Endif
Endfor

Return ""

****************************
Procedure cstringbreak
****************************
cLen = Len(Alltrim(cString))
Fputs(nFilenum,cString)
Return
******************************************************************************



