* KRONOS ON PREMISES RPT

* exe = F:\UTIL\KRONOS\KRONOSONPREMISESRPT.EXE
* 
* added Agencies F & G - Chartwell KY and Chartwell NJ 02/02/2018 MB
*
* removed William Schiele from emails 4/2/2018 MB
*
* 4/24/2018 MB: changed fmiint.com emails to Toll emails.


LPARAMETERS tcTempCo

IF TYPE('tcTempCo') = "L" THEN
	tcTempCo = "?"
ENDIF

ON ERROR DO errHandlerKTTR WITH ERROR( ), MESSAGE( ), MESSAGE(1), PROGRAM( ), LINENO( ), tcTempCo, tcMode

runack("KRONOSONPREMISESRPT")

LOCAL lnError, lcProcessName

DECLARE INTEGER CreateMutex IN Win32API ;
	STRING @lpMutexAttributes, ;
	STRING bInitialOwner, ;
	STRING @lpName

DECLARE INTEGER GetLastError IN WIN32API 

* if running in EXE check to see if already running
IF _VFP.STARTMODE > 0 THEN
	lcProcessName = "KRONOSONPREMISESRPT"
	CreateMutex(0,CHR(1),@lcProcessName)
	lnError = GetLastError()
	IF lnError = 183
		WAIT WINDOW TIMEOUT 10 "KRONOS TIME REPORT process is already running..."
		RETURN .F.
	ENDIF
ENDIF

_SCREEN.CAPTION = "KRONOSONPREMISESRPT"

utilsetup("KRONOSONPREMISESRPT")

LOCAL loKRONOSONPREMISESRPT, lcTempCo

loKRONOSONPREMISESRPT = CREATEOBJECT('KRONOSONPREMISESRPT')

lcTempCo = UPPER(ALLTRIM(tcTempCo))

loKRONOSONPREMISESRPT.MAIN( lcTempCo )


schedupdate()

CLOSE DATABASES ALL

WAIT WINDOW TIMEOUT 60  && TO FORCE THIS PROCESS TO RUN FOR > 1 MINUTE, I THINK IF IT RUNS WITHIN 1 MINUTE STM RUNS IT TWICE

RETURN

#DEFINE LOGIT 1
#DEFINE WAITIT 2
#DEFINE NOWAITIT 4
#DEFINE SENDIT 8
#DEFINE NOCRLF 16
#DEFINE CRLF CHR(13) + CHR(10)
#DEFINE LF CHR(10)
#DEFINE RETURN_DATA_MANDATORY .F.
#DEFINE RETURN_DATA_NOT_MANDATORY .T.
#DEFINE DAILY_OT_MULTIPLIER 1.5
#DEFINE DAILY_OT20_MULTIPLIER 2.0
#DEFINE xlEdgeBottom 9
#DEFINE xlContinuous 1
#DEFINE xlMedium -4138
#DEFINE TAB CHR(9)

DEFINE CLASS KRONOSONPREMISESRPT AS CUSTOM

	cProcessName = 'KRONOSONPREMISESRPT'

	lTestMode = .F.  && .F. FOR NORMAL OPERATION, .T. TO SUPPRESS ALL BUT DEVELOPER EMAILS AND CHANGE INPUT TABLE, ETC.

	lAutoYield = .T.

	* date properties
	dtNow = DATETIME()

	dToday = DATE()
	*dToday = {^2015-05-12}

	cStartTime = TTOC(DATETIME())

	* connection properties
	nSQLHandle = 0

	* sql/report filter properties
	cTempCoWhere = ""
	cTempCoWhere2 = ""
	cTempCo = ""
	cTempCoName = ""
	cMode = ""


	cToday = DTOC(DATE())

	* wait window properties
	nWaitWindowTimeout = 2
	lWaitWindowIsOn = .T.

	* logfile properties
	lLoggingIsOn = .T.
	cLogFile = 'F:\UTIL\KRONOS\Logfiles\KRONOSONPREMISESRPT_log.txt'

	* INTERNAL email properties
	lSendInternalEmailIsOn = .T.
	cFrom = 'mark.bennett@tollgroup.com'
	cSendTo = 'mark.bennett@tollgroup.com'
	cCC = ''
	cSubject = 'Kronos On Premises Report'
	cAttach = ''
	cBodyText = ''
	cTopBodyText = ''

	cCOMPUTERNAME = ''
	cUSERNAME = ''

	FUNCTION INIT
		IF NOT DODEFAULT()
			RETURN .F.
		ENDIF
		WITH THIS
			CLEAR
			*SET RESOURCE OFF
			CLOSE DATA
			SET CENTURY ON
			SET DATE YMD
			SET HOURS TO 24
			SET ANSI ON
			SET TALK OFF
			SET DELETED ON
			SET CONSOLE OFF
			SET EXCLUSIVE OFF
			SET SAFETY OFF
			SET EXACT OFF
			SET STATUS BAR ON
			SET SYSMENU OFF
			SET ENGINEBEHAVIOR 70
			SET DECIMALS TO 0

			SET FIXED ON

			_VFP.AUTOYIELD = .lAutoYield
			.cCOMPUTERNAME = GETENV("COMPUTERNAME")
			.cUSERNAME = GETENV("USERNAME")
		ENDWITH
	ENDFUNC


	FUNCTION DESTROY
		WITH THIS
			IF .lLoggingIsOn  THEN
				SET ALTERNATE OFF
				SET ALTERNATE TO
			ENDIF
		ENDWITH
		DODEFAULT()
	ENDFUNC


	FUNCTION MAIN
		LPARAMETERS tcTempCo
		WITH THIS
			LOCAL lnNumberOfErrors, lcSQL, lcSQL2, lcSQL3, lcSQL4, lcSQL5, lcSQL6, lcSQL7, loError
			LOCAL lcXLFileName, ldStartDate, ldEndDate, lcSQLStartDate, lcSQLEndDate, ldDate, lcTempCoWhere, lcTempCo, lcMode

			ON ERROR

			TRY
				lnNumberOfErrors = 0

				*.TrackProgress('==================================================================================================================', LOGIT+SENDIT)
				.TrackProgress('KRONOS On Premises process started....', LOGIT+SENDIT)
				.TrackProgress('COMPUTERNAME = ' + .cCOMPUTERNAME, LOGIT+SENDIT)
				.TrackProgress('USERNAME = ' + .cUSERNAME, LOGIT+SENDIT)
				.TrackProgress('PROJECT = KRONOSONPREMISESRPT', LOGIT+SENDIT)
				.TrackProgress('Temp Co = ' + LEFT(tcTempCo,1), LOGIT+SENDIT)
				IF .lTestMode THEN
					.TrackProgress('                        TEST MODE                        ', LOGIT+SENDIT)
				ENDIF

				IF LEN( tcTempCo ) = 1 THEN
				
					lcTempCo = tcTempCo
					lcMode = ""
				ELSE
				
					lcTempCo = LEFT(tcTempCo,1)
					lcMode = SUBSTR(tcTempCo,2)
					.TrackProgress('Mode = ' + lcMode, LOGIT+SENDIT)
					
					IF NOT INLIST(lcMode,"DIV52","SHIFT1","SHIFT2") THEN
						.TrackProgress('Invalid Mode Parameter in the TempCo!', LOGIT+SENDIT)
						THROW
					ELSE
						.cMode = lcMode
					ENDIF
					
				ENDIF				

				* set temp agency from parameter
				.SetTempCo( lcTempCo )

				IF EMPTY(.cTempCoName) THEN
					.TrackProgress('Missing or Invalid Temp CO Parameter!', LOGIT+SENDIT)
					THROW
				ENDIF
				
				.cSubject = 'On Premises Report - ' + .cTempCoName + ' as of ' + TRANSFORM(DATETIME()) + ' Eastern Time'

				ldToday = .dToday
				.cToday = DTOC(.dToday)

				ldStartDate = ldToday
				ldEndDate = ldToday + 1

				lcSQLStartDate = "'" + STRTRAN(TRANSFORM(ldStartDate),"/","") + "'"
				lcSQLEndDate = "'" + STRTRAN(TRANSFORM(ldEndDate),"/","") + "'"
				
				lcXLFileName = "F:\UTIL\Kronos\TEMPTIMEREPORTS\ONPREMISES_" + STRTRAN(lcSQLStartDate,"'","") + ".XLS"

				IF USED('CURPUNCHES') THEN
					USE IN CURPUNCHES
				ENDIF

				IF USED('CURPUNCHESPRE') THEN
					USE IN CURPUNCHESPRE
				ENDIF

				* define filters / emails by passed tempco and mode parameters
				DO CASE
					CASE .cTempCo == 'D'  && QUALITY NJ
						.cTempCoWhere =  " AND (LABORLEVELNAME1 = 'TMP') AND (LABORLEVELNAME5 = '" + .cTempCo + "') "
						DO CASE
							CASE .cMode == "SHIFT2"
								.cSendTo = 'arturo.reyes@tollgroup.com, victor.roche@tollgroup.com, Jim.Lake@tollgroup.com'
								.cCC = 'mark.bennett@tollgroup.com, sueoqualitytemps@outlook.com, toll@chartwellstaff.com'
							OTHERWISE
								.cSendTo = 'Bill.Marsh@tollgroup.com, Jim.Lake@tollgroup.com'
								.cCC = 'mark.bennett@tollgroup.com, sueoqualitytemps@outlook.com, toll@chartwellstaff.com'
						ENDCASE
					CASE .cTempCo == 'F'  && CHARTWELL KY
						.cTempCoWhere =  " AND (LABORLEVELNAME1 = 'TMP') AND (LABORLEVELNAME5 = '" + .cTempCo + "') "
						DO CASE
							CASE .cMode == "SHIFT2"
								.cSendTo = 'Robin.Ragg@tollgroup.com, toll@chartwellstaff.com'
								.cCC = 'mark.bennett@tollgroup.com'
							OTHERWISE
								.cSendTo = 'Robin.Ragg@tollgroup.com, toll@chartwellstaff.com'
								.cCC = 'mark.bennett@tollgroup.com'
						ENDCASE
					CASE .cTempCo == 'G'  && CHARTWELL NJ
						.cTempCoWhere =  " AND (LABORLEVELNAME1 = 'TMP') AND (LABORLEVELNAME5 = '" + .cTempCo + "') "
						DO CASE
							CASE .cMode == "SHIFT2"
								.cSendTo = 'arturo.reyes@tollgroup.com, victor.roche@tollgroup.com, Jim.Lake@tollgroup.com'
								.cCC = 'mark.bennett@tollgroup.com, toll@chartwellstaff.com'
							OTHERWISE
								.cSendTo = 'Bill.Marsh@tollgroup.com, Jim.Lake@tollgroup.com'
								.cCC = 'mark.bennett@tollgroup.com, toll@chartwellstaff.com'
						ENDCASE
					CASE .cTempCo == 'R'
						DO CASE
							CASE .cMode == "DIV52"
								.cTempCoWhere =  " AND (LABORLEVELNAME1 = 'TMP') AND (LABORLEVELNAME2 = '52') AND (LABORLEVELNAME5 = '" + .cTempCo + "') "
								.cSubject = 'On Premises Report - ' + .cTempCoName + ' - DIV 52 as of ' + TRANSFORM(DATETIME())
								.cSendTo = 'mark.bennett@tollgroup.com'
								.cCC = 'mark.bennett@tollgroup.com'
							CASE .cMode == "SHIFT1"
								.cTempCoWhere =  " AND (LABORLEVELNAME1 = 'TMP') AND (LABORLEVELNAME5 = '" + .cTempCo + "') "
								.cSubject = 'On Premises Report - ' + .cTempCoName + ' as of ' + TRANSFORM(DATETIME())
								.cSendTo = 'malvarez@thessg.com, jcano@thessg.com'
								.cCC = 'mark.bennett@tollgroup.com'
							CASE .cMode == "SHIFT2"
								.cTempCoWhere =  " AND (LABORLEVELNAME1 = 'TMP') AND (LABORLEVELNAME5 = '" + .cTempCo + "') "
								.cSubject = 'On Premises Report - ' + .cTempCoName + ' as of ' + TRANSFORM(DATETIME())
								.cSendTo = 'malvarez@thessg.com, jcano@thessg.com'
								.cCC = 'mark.bennett@tollgroup.com'
							OTHERWISE
								.cTempCoWhere =  " AND (LABORLEVELNAME1 = 'TMP') AND (LABORLEVELNAME5 = '" + .cTempCo + "') "
								.cSubject = 'On Premises Report - ' + .cTempCoName + ' as of ' + TRANSFORM(DATETIME())
								.cSendTo = 'mark.bennett@tollgroup.com'
								.cCC = 'mark.bennett@tollgroup.com'
						ENDCASE
					CASE .cTempCo == 'X'   && Chartwell SP
						DO CASE
							CASE .cMode == "DIV52"
								.cTempCoWhere =  " AND (LABORLEVELNAME1 = 'TMP') AND (LABORLEVELNAME2 = '52') AND (LABORLEVELNAME5 = '" + .cTempCo + "') "
								.cSubject = 'On Premises Report - ' + .cTempCoName + ' - DIV 52 as of ' + TRANSFORM(DATETIME())
								.cSendTo = 'mark.bennett@tollgroup.com'
								.cCC = 'mark.bennett@tollgroup.com'
							CASE .cMode == "SHIFT1"
								.cTempCoWhere =  " AND (LABORLEVELNAME1 = 'TMP') AND (LABORLEVELNAME5 = '" + .cTempCo + "') "
								.cSubject = 'On Premises Report - ' + .cTempCoName + ' as of ' + TRANSFORM(DATETIME())
								.cSendTo = 'toll@chartwellstaff.com'
								.cCC = 'mark.bennett@tollgroup.com'
							CASE .cMode == "SHIFT2"
								.cTempCoWhere =  " AND (LABORLEVELNAME1 = 'TMP') AND (LABORLEVELNAME5 = '" + .cTempCo + "') "
								.cSubject = 'On Premises Report - ' + .cTempCoName + ' as of ' + TRANSFORM(DATETIME())
								.cSendTo = 'toll@chartwellstaff.com'
								.cCC = 'mark.bennett@tollgroup.com'
							OTHERWISE
								.cTempCoWhere =  " AND (LABORLEVELNAME1 = 'TMP') AND (LABORLEVELNAME5 = '" + .cTempCo + "') "
								.cSubject = 'On Premises Report - ' + .cTempCoName + ' as of ' + TRANSFORM(DATETIME())
								.cSendTo = 'mark.bennett@tollgroup.com'
								.cCC = 'mark.bennett@tollgroup.com'
						ENDCASE
					CASE .cTempCo == 'Y'   && Simplified
						DO CASE
							CASE .cMode == "SHIFT1"
								.cTempCoWhere =  " AND (LABORLEVELNAME1 = 'TMP') AND (LABORLEVELNAME5 = '" + .cTempCo + "') "
								.cSubject = 'On Premises Report - ' + .cTempCoName + ' as of ' + TRANSFORM(DATETIME())
								.cSendTo = 'Sandra.Senbol@yourstaffingfirm.com, maria.sanchez@yourstaffingfirm.com'
								.cCC = 'mark.bennett@tollgroup.com'
							CASE .cMode == "SHIFT2"
								.cTempCoWhere =  " AND (LABORLEVELNAME1 = 'TMP') AND (LABORLEVELNAME5 = '" + .cTempCo + "') "
								.cSubject = 'On Premises Report - ' + .cTempCoName + ' as of ' + TRANSFORM(DATETIME())
								.cSendTo = 'Sandra.Senbol@yourstaffingfirm.com, maria.sanchez@yourstaffingfirm.com'
								.cCC = 'mark.bennett@tollgroup.com'
							CASE .cMode == "SHIFT3"
								.cTempCoWhere =  " AND (LABORLEVELNAME1 = 'TMP') AND (LABORLEVELNAME5 = '" + .cTempCo + "') "
								.cSubject = 'On Premises Report - ' + .cTempCoName + ' as of ' + TRANSFORM(DATETIME())
								.cSendTo = 'Sandra.Senbol@yourstaffingfirm.com, maria.sanchez@yourstaffingfirm.com'
								.cCC = 'mark.bennett@tollgroup.com'
							OTHERWISE
								.cTempCoWhere =  " AND (LABORLEVELNAME1 = 'TMP') AND (LABORLEVELNAME5 = '" + .cTempCo + "') "
								.cSubject = 'On Premises Report - ' + .cTempCoName + ' as of ' + TRANSFORM(DATETIME())
								.cSendTo = 'mark.bennett@tollgroup.com'
								.cCC = 'mark.bennett@tollgroup.com'
						ENDCASE
					OTHERWISE
						* NOTHING  
				ENDCASE
				
				IF .lTestMode THEN
					.cSendTo = 'mark.bennett@tollgroup.com'
					.cCC = ""
				ENDIF

				lcTempCoWhere = .cTempCoWhere

				* the select below gets temps where there is an in punch for today but no out punch; that should mean they are On Premises

				SET TEXTMERGE ON

				TEXT TO	lcSQL NOSHOW


SELECT
PERSONFULLNAME AS NAME,
LABORLEVELNAME2 AS DIVISION,
LABORLEVELNAME3 AS DEPT,
INPUNCHDTM,
OUTPUNCHDTM
FROM VP_TIMESHTPUNCHV42
WHERE (NOT {fn IFNULL(INPUNCHDTM,'')} = '')
AND ({fn IFNULL(OUTPUNCHDTM,'')} = '')
AND (EVENTDATE >= <<lcSQLStartDate>>)
AND (EVENTDATE < <<lcSQLEndDate>>)
<<lcTempCoWhere>>
ORDER BY PERSONFULLNAME, EVENTDATE, INPUNCHDTM


				ENDTEXT

				SET TEXTMERGE OFF

				*!*	AND LABORLEVELNAME1 = 'TMP'
				*!*

				IF .lTestMode THEN
					.TrackProgress('', LOGIT+SENDIT)
					.TrackProgress('lcSQL =' + lcSQL, LOGIT+SENDIT)
				ENDIF

				.nSQLHandle = SQLCONNECT("WTK2","sa","kr0n1tes")

				IF .nSQLHandle > 0 THEN

					IF .ExecSQL(lcSQL, 'CURPUNCHESPRE', RETURN_DATA_NOT_MANDATORY) THEN

						IF USED('CURPUNCHESPRE') AND NOT EOF('CURPUNCHESPRE') THEN

							SELECT CURPUNCHESPRE
							COPY TO (lcXLFileName) XL5
							.cAttach = lcXLFileName
							
							.cTopBodyText = "These associates are On Premises: they have In Punches with no matching Out Punches."

						ELSE
							.TrackProgress("There are no In Punches without matching out punches to report!", LOGIT+SENDIT)
							.cTopBodyText = "No associates are On Premises: there are no In Punches without matching Out Punches to report!"
						ENDIF


					ENDIF  &&  .ExecSQL(lcSQL

				ELSE
					* connection error
					.TrackProgress('Unable to connect to WTK System.', LOGIT+SENDIT)
				ENDIF   &&  .nSQLHandle > 0

				.TrackProgress('KRONOS On Premises process ended normally.', LOGIT+SENDIT+NOWAITIT)
				
				.cBodyText = .cTopBodyText + CRLF+ CRLF + "<<report log follows>>" + CRLF + CRLF + .cBodyText


			CATCH TO loError

				.TrackProgress('There was an error.',LOGIT+SENDIT)
				.TrackProgress('The Error # is: ' + TRANSFORM(loError.ERRORNO), LOGIT+SENDIT)
				.TrackProgress('The Error Message is: ' + TRANSFORM(loError.MESSAGE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred in Program: ' + TRANSFORM(loError.PROCEDURE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred at line #: ' + TRANSFORM(loError.LINENO), LOGIT+SENDIT)
				lnNumberOfErrors = lnNumberOfErrors + 1
				IF TYPE('oExcel') = 'O' AND NOT ISNULL(oExcel) THEN
					oExcel.QUIT()
				ENDIF
				*CLOSE ALL

			ENDTRY

			CLOSE DATA
			WAIT CLEAR
			***************** INTERNAL email results ******************************
			.TrackProgress('About to send status email.',LOGIT)
			.TrackProgress('==================================================================================================================', SENDIT)
			.TrackProgress('The logfile is: ' + .cLogFile, SENDIT)
			.TrackProgress('KRONOS On Premises process started: ' + .cStartTime, LOGIT+SENDIT)
			.TrackProgress('KRONOS On Premises process finished: ' + TTOC(DATETIME()), LOGIT+SENDIT)

			IF .lSendInternalEmailIsOn THEN
				* try to trap error from not having dartmail dll's registered on user's pc...
				TRY
					DO FORM dartmail2 WITH .cSendTo,.cFrom,.cSubject,.cCC,.cAttach,.cBodyText,"A"
					.TrackProgress('Sent status email.',LOGIT)
				CATCH TO loError
					*=MESSAGEBOX('There was an error sending the status email. Please contact Mark Bennett.',0+64,"KRONOS Temp Time")
					.TrackProgress('There was an error sending the status email.',LOGIT)
					.TrackProgress('The Error # is: ' + TRANSFORM(loError.ERRORNO), LOGIT)
					.TrackProgress('The Error Message is: ' + TRANSFORM(loError.MESSAGE), LOGIT)
					.TrackProgress('The Error occurred in Program: ' + TRANSFORM(loError.PROCEDURE), LOGIT)
					.TrackProgress('The Error occurred at line #: ' + TRANSFORM(loError.LINENO), LOGIT)
					lnNumberOfErrors = lnNumberOfErrors + 1
				ENDTRY

			ELSE
				.TrackProgress('Did not send status email: THIS.lSendInternalEmailIsOn = FALSE.',LOGIT)
			ENDIF

		ENDWITH
		RETURN
	ENDFUNC && main


	FUNCTION ExecSQL
		LPARAMETERS tcSQL, tcCursorName, tlNoDataReturnedIsOkay
		LOCAL llRetVal, lnResult
		WITH THIS
			* close target cursor if it's open
			IF USED(tcCursorName)
				USE IN (tcCursorName)
			ENDIF
			WAIT WINDOW "Fetching remote data; please wait..." NOWAIT
			lnResult = SQLEXEC(.nSQLHandle, tcSQL, tcCursorName)
			llRetVal = ( lnResult > 0 )
			IF llRetVal THEN
				* see if any data came back
				IF NOT tlNoDataReturnedIsOkay THEN
					IF (NOT USED(tcCursorName)) OR EOF(tcCursorName) THEN
						llRetVal = .F.
						.TrackProgress('No data was returned by this query: ' +CRLF + tcSQL, LOGIT+SENDIT)
					ENDIF
				ENDIF
			ELSE
				.TrackProgress("Result Code [" + TRANSFORM(lnResult) + "] returned by this query: " + tcSQL, LOGIT+SENDIT)
			ENDIF
			WAIT CLEAR
			RETURN llRetVal
		ENDWITH
	ENDFUNC


	PROCEDURE TrackProgress
		* do any combination of Wait Window, writing to logfile, and adding to body of email,
		* based on nFlags parameter.
		LPARAMETERS tcExpression, tnFlags
		LOCAL lcCRLF
		WITH THIS
			IF BITAND(tnFlags,NOCRLF) = NOCRLF THEN
				lcCRLF = ""
			ELSE
				lcCRLF = CRLF
			ENDIF
			IF BITAND(tnFlags,LOGIT) = LOGIT THEN
				IF .lLoggingIsOn THEN
					?
					? .cProcessName + "  " + TTOC(DATETIME()) + ": " + tcExpression
				ENDIF
			ENDIF
			IF .lWaitWindowIsOn THEN
				IF BITAND(tnFlags,NOWAITIT) = NOWAITIT THEN
					WAIT WINDOW tcExpression NOWAIT
				ENDIF
				IF BITAND(tnFlags,WAITIT) = WAITIT THEN
					WAIT WINDOW tcExpression TIMEOUT .nWaitWindowTimeout
				ENDIF
			ENDIF
			IF BITAND(tnFlags,SENDIT) = SENDIT THEN
				IF .lSendInternalEmailIsOn THEN
					.cBodyText = .cBodyText + tcExpression + CRLF + lcCRLF
				ENDIF
			ENDIF
		ENDWITH
	ENDPROC  &&  TrackProgress


	PROCEDURE SetTempCo
		LPARAMETERS tcTempCo
		WITH THIS
			DO CASE
				CASE tcTempCo == 'D'
					.cTempCo = tcTempCo
					.cTempCoName = 'Quality Temps NJ'
					IF .lTestMode THEN
						.TrackProgress('Set TempCo = ' + tcTempCo,LOGIT+SENDIT)
						.TrackProgress('Set TempCoName = ' + .cTempCoName,LOGIT+SENDIT)
					ELSE
						.TrackProgress('Set TempCo = ' + tcTempCo,LOGIT)
						.TrackProgress('Set TempCoName = ' + .cTempCoName,LOGIT)
					ENDIF
				CASE tcTempCo == 'G'
					.cTempCo = tcTempCo
					.cTempCoName = 'Chartwell Temps NJ'
					IF .lTestMode THEN
						.TrackProgress('Set TempCo = ' + tcTempCo,LOGIT+SENDIT)
						.TrackProgress('Set TempCoName = ' + .cTempCoName,LOGIT+SENDIT)
					ELSE
						.TrackProgress('Set TempCo = ' + tcTempCo,LOGIT)
						.TrackProgress('Set TempCoName = ' + .cTempCoName,LOGIT)
					ENDIF
				CASE tcTempCo == 'Q'
					.cTempCo = tcTempCo
					.cTempCoName = 'Cor-Tech - Louisville'
					IF .lTestMode THEN
						.TrackProgress('Set TempCo = ' + tcTempCo,LOGIT+SENDIT)
						.TrackProgress('Set TempCoName = ' + .cTempCoName,LOGIT+SENDIT)
					ELSE
						.TrackProgress('Set TempCo = ' + tcTempCo,LOGIT)
						.TrackProgress('Set TempCoName = ' + .cTempCoName,LOGIT)
					ENDIF
				CASE tcTempCo == 'R'
					.cTempCo = tcTempCo
					.cTempCoName = 'Staffing Solutions - SP'
					IF .lTestMode THEN
						.TrackProgress('Set TempCo = ' + tcTempCo,LOGIT+SENDIT)
						.TrackProgress('Set TempCoName = ' + .cTempCoName,LOGIT+SENDIT)
					ELSE
						.TrackProgress('Set TempCo = ' + tcTempCo,LOGIT)
						.TrackProgress('Set TempCoName = ' + .cTempCoName,LOGIT)
					ENDIF
				CASE tcTempCo == 'T'
					.cTempCo = tcTempCo
					.cTempCoName = 'Staffing Solutions - ML'
					IF .lTestMode THEN
						.TrackProgress('Set TempCo = ' + tcTempCo,LOGIT+SENDIT)
						.TrackProgress('Set TempCoName = ' + .cTempCoName,LOGIT+SENDIT)
					ELSE
						.TrackProgress('Set TempCo = ' + tcTempCo,LOGIT)
						.TrackProgress('Set TempCoName = ' + .cTempCoName,LOGIT)
					ENDIF
				CASE tcTempCo == 'U'
					.cTempCo = tcTempCo
					.cTempCoName = 'Staffing Solutions - Carson'
					IF .lTestMode THEN
						.TrackProgress('Set TempCo = ' + tcTempCo,LOGIT+SENDIT)
						.TrackProgress('Set TempCoName = ' + .cTempCoName,LOGIT+SENDIT)
					ELSE
						.TrackProgress('Set TempCo = ' + tcTempCo,LOGIT)
						.TrackProgress('Set TempCoName = ' + .cTempCoName,LOGIT)
					ENDIF
				CASE tcTempCo == 'V'
					.cTempCo = tcTempCo
					.cTempCoName = 'Staffing Solutions - Carson 2'
					IF .lTestMode THEN
						.TrackProgress('Set TempCo = ' + tcTempCo,LOGIT+SENDIT)
						.TrackProgress('Set TempCoName = ' + .cTempCoName,LOGIT+SENDIT)
					ELSE
						.TrackProgress('Set TempCo = ' + tcTempCo,LOGIT)
						.TrackProgress('Set TempCoName = ' + .cTempCoName,LOGIT)
					ENDIF
				CASE tcTempCo == 'X'
					.cTempCo = tcTempCo
					.cTempCoName = 'Chartwell Staffing Solutions - San Pedro'
					IF .lTestMode THEN
						.TrackProgress('Set TempCo = ' + tcTempCo,LOGIT+SENDIT)
						.TrackProgress('Set TempCoName = ' + .cTempCoName,LOGIT+SENDIT)
					ELSE
						.TrackProgress('Set TempCo = ' + tcTempCo,LOGIT)
						.TrackProgress('Set TempCoName = ' + .cTempCoName,LOGIT)
					ENDIF
				CASE tcTempCo == 'Y'
					.cTempCo = tcTempCo
					.cTempCoName = 'Simplified Labor Staffing Solutions - Mira Loma'
					IF .lTestMode THEN
						.TrackProgress('Set TempCo = ' + tcTempCo,LOGIT+SENDIT)
						.TrackProgress('Set TempCoName = ' + .cTempCoName,LOGIT+SENDIT)
					ELSE
						.TrackProgress('Set TempCo = ' + tcTempCo,LOGIT)
						.TrackProgress('Set TempCoName = ' + .cTempCoName,LOGIT)
					ENDIF
				OTHERWISE
					.TrackProgress('INVALID TempCo = ' + tcTempCo,LOGIT+SENDIT)
					.cTempCoName = ''
			ENDCASE

			* make logfiles company-specific so that report run times can overlap
			IF .lTestMode THEN
				.cLogFile = 'F:\UTIL\KRONOS\Logfiles\KRONOSONPREMISESRPT_' + .cTempCoName + "_"  + .cCOMPUTERNAME + '_log_TESTMODE.txt'
			ELSE
				.cLogFile = 'F:\UTIL\KRONOS\Logfiles\KRONOSONPREMISESRPT_' + + .cTempCoName + "_" + .cCOMPUTERNAME + '_log.txt'
			ENDIF
			.lLoggingIsOn = .lLoggingIsOn AND NOT EMPTY(.cLogFile)
			IF .lLoggingIsOn THEN
				SET ALTERNATE TO (.cLogFile) ADDITIVE
				SET ALTERNATE ON
			ENDIF
		ENDWITH
	ENDPROC  &&  SetTempCo




	PROCEDURE errHandlerKTTR
		PARAMETER merror, MESS, mess1, mprog, mlineno, tcTempCo, tcMode
		LOCAL lcSendTo,lcFrom,lcSubject,lcCC,lcAttach,lcBodyText
		CLEAR

		lcSendTo = 'mark.bennett@tollgroup.com'
		lcFrom = 'fmicorporate@fmiint.com'
		lcSubject = 'Early error in KRONOSONPREMISESRPT'
		lcCC = ''
		lcAttach = ''
		lcBodyText = 'Error number: ' + LTRIM(STR(merror)) + CRLF + ;
			'Error message: ' + MESS + CRLF + ;
			'Line of code with error: ' + mess1 + CRLF + ;
			'Line number of error: ' + LTRIM(STR(mlineno)) + CRLF + ;
			'Program with error: ' + mprog + CRLF + ;
			'tcTempCo = ' + tcTempCo + CRLF + ;
			'tcMode = ' + tcMode

		DO FORM dartmail2 WITH lcSendTo,lcFrom,lcSubject,lcCC,lcAttach,lcBodyText,"A"

		QUIT

	ENDPROC
	
ENDDEFINE
