* Open appointments for Marc Jacobs

utilsetup("MJAPPT")

if holiday(date())
	return
endif

guserid="AUTO"

goffice="J"

xsqlexec("select * from account where inactive=0","account",,"qq")
index on accountid tag accountid
set order to

xsqlexec("select accountid, ship_ref, wo_num, wo_date, consignee, cnee_ref, ship_via, ctnqty, " + ;
	"start, cancel, called, appt, appt_time, appt_num, apptremarks, pulleddt, picked, labeled, staged " + ;
	"from outship where (accountid=6404 or (accountid=6303 and cnee_ref='INFA')) " + ;
	"and appt#{} and del_date={} and notonwip=0","xdytemp",,"wh")
	
select padr(acctname(accountid),30), ship_ref, wo_num, wo_date, consignee, cnee_ref, ship_via, ctnqty, ;
	start, cancel, called, appt, appt_time, appt_num, apptremarks, pulleddt, picked, labeled, staged, "   " as code ;
	from xdytemp into cursor xrpt readwrite
	
scan 
	replace code with mjwipdiv()
endscan
set step on 
xfilename="h:\fox\mj.xls"

copy to (xfilename) xls

tattach=xfilename
tsendto="d.moe@marcjacobs.com, e.sullivan@marcjacobs.com, l.lopez@marcjacobs.com"
tcc="m.ricco@marcjacobs.com, kfloeck@fmiint.com"
tFrom ="TGF EDI <fmi-transload-ops@fmiint.com>"
tmessage = "See attached file"
tSubject = "Open Appointments"

Do Form dartmail2 With tsendto,tFrom,tSubject," ",tattach,tmessage,"A"

*

use in account

schedupdate()

_screen.Caption=gscreencaption
on error