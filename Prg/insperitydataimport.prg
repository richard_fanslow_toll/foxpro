*InsperityDataImport.PRG
* process various data files from Insperity and TimeStar
* also does some reporting.

* Build EXE as F:\UTIL\INSPERITY\INSPERITYDATAIMPORT.EXE 
*
* 4/24/2018 MB: changed fmiint.com to Toll emails.

LPARAMETERS tcMode

LOCAL loINSPERITYDATAIMPORT, lcMode

IF EMPTY(tcMode) THEN
	lcMode = "?"
ELSE
	lcMode = UPPER(ALLTRIM(tcMode))
ENDIF

runack("INSPERITYDATAIMPORT")

utilsetup("INSPERITYDATAIMPORT")

loINSPERITYDATAIMPORT = CREATEOBJECT('INSPERITYDATAIMPORT')

loINSPERITYDATAIMPORT.MAIN( lcMode )

schedupdate()

CLOSE DATABASES ALL
RETURN


#DEFINE LOGIT 1
#DEFINE WAITIT 2
#DEFINE NOWAITIT 4
#DEFINE SENDIT 8
#DEFINE CRLF CHR(13) + CHR(10)
#DEFINE RETURN_DATA_MANDATORY .F.
#DEFINE RETURN_DATA_NOT_MANDATORY .T.
#DEFINE CSVFORMAT 6
#DEFINE WRITE_ONLY_UNBUFFERED 11
#DEFINE COMMA ","
#DEFINE xlEdgeBottom 9
#DEFINE xlContinuous 1
#DEFINE xlMedium -4138
#DEFINE xlFONT_RED -16776961

DEFINE CLASS INSPERITYDATAIMPORT AS CUSTOM

	cProcessName = 'INSPERITYDATAIMPORT'

	lTestMode = .F.  && .F. FOR NORMAL OPERATION, .T. TO SUPPRESS ALL BUT DEVELOPER EMAILS AND CHANGE INPUT TABLE, ETC.

	dToday = DATE()

	lAutoYield = .T.

	cStartTime = TTOC(DATETIME())

	* processing properties
	lAppendTimeData = .T.    &&  when .F. entire table will be zapped before loading. When .T. new data will be appended to existing.
	cMode = ''
	cTempWorksiteCode = ''

	* connection properties
	nSQLHandle = 0

	* Excel properties
	oExcel = NULL

	* wait window properties
	nWaitWindowTimeout = 2
	lWaitWindowIsOn = .T.

	* logfile properties
	lLoggingIsOn = .T.
	cLogFile = 'F:\UTIL\INSPERITY\LOGFILES\INSPERITYDATAIMPORT_log.txt'

	* INTERNAL email properties
	lSendInternalEmailIsOn = .T.
	cFrom = 'Mark.Bennett@Tollgroup.com'
	cSendTo = 'mark.bennett@tollgroup.com'
	*cSendTo = 'Mark.Bennett@Tollgroup.com'
	cCC = ''
	cSubject = ''
	cAttach = ''
	cBodyText = ''
	cPunchMessages = ''

	cCOMPUTERNAME = ''
	cUSERNAME = ''


	FUNCTION INIT
		IF NOT DODEFAULT()
			RETURN .F.
		ENDIF
		WITH THIS
			*SET RESOURCE OFF
			CLOSE DATA
			SET DECIMALS TO 4
			SET CENTURY ON
			SET DATE AMERICAN
			SET HOURS TO 24
			SET ANSI ON
			SET TALK OFF
			SET DELETED ON
			SET CONSOLE OFF
			SET EXCLUSIVE OFF
			SET SAFETY OFF
			SET EXACT OFF
			SET STATUS BAR ON
			SET SYSMENU OFF
			SET ENGINEBEHAVIOR 70
			_VFP.AUTOYIELD = .lAutoYield
			.cCOMPUTERNAME = GETENV("COMPUTERNAME")
			.cUSERNAME = GETENV("USERNAME")
			IF .lTestMode THEN
				.cLogFile = 'F:\UTIL\INSPERITY\LOGFILES\INSPERITYDATAIMPORT_log_TESTMODE.txt'
				.cSendTo = 'mark.bennett@tollgroup.com'
				.cCC = ''
			ENDIF
			.lLoggingIsOn = .lLoggingIsOn AND NOT EMPTY(.cLogFile)
			IF .lLoggingIsOn THEN
				SET ALTERNATE TO (.cLogFile) ADDITIVE
				SET ALTERNATE ON
			ENDIF
			SET PROCEDURE TO VALIDATIONS ADDITIVE
		ENDWITH
	ENDFUNC


	FUNCTION DESTROY
		WITH THIS
			IF .lLoggingIsOn  THEN
				SET ALTERNATE OFF
				SET ALTERNATE TO
			ENDIF
		ENDWITH
		IF TYPE('.oExcel') = 'O' AND NOT ISNULL(.oExcel) THEN
			.oExcel.QUIT()
		ENDIF
		DODEFAULT()
	ENDFUNC


	FUNCTION MAIN
		LPARAMETERS tcMode
		WITH THIS
			LOCAL lnNumberOfErrors, lcSQL, lcSQL2, loError, lcDateSuffix
			LOCAL lcFiletoSaveAs, lcSpreadsheetTemplate, ldWorkDate
			LOCAL i, oExcel, oWorkbook, oWorksheet, lnStartRow, lcStartRow, lnRow, lcRow

			TRY
				lnNumberOfErrors = 0

				.TrackProgress("INSPERITY DATA IMPORT process started....", LOGIT+SENDIT+NOWAITIT)
				.TrackProgress('COMPUTERNAME = ' + .cCOMPUTERNAME, LOGIT+SENDIT)
				.TrackProgress('USERNAME = ' + .cUSERNAME, LOGIT+SENDIT)
				.TrackProgress('PROJECT = INSPERITYDATAIMPORT', LOGIT+SENDIT)
				.TrackProgress('LOAD TYPE = ' + tcMode, LOGIT+SENDIT)

				IF .lTestMode THEN
					.TrackProgress('                        TEST MODE                        ', LOGIT+SENDIT)
				ENDIF

				lcDateSuffix = DTOC(DATE())

				IF NOT INLIST(tcMode,"401K","ACC","ADDDRIVERTOFOXPRO","CHECKDEPTTRANS","CHECKDIVSDEPTS","CHECKDRIVERS","CSVTIME","CSVTIMEDRIVER","EMP","EMP-TERMS","TODDPRFORECAST") ;
						AND NOT INLIST(tcMode,"GPAYCALLOW","JOBCOSTING","JOBCOSTING2","LIABILITY","LOADDRIVERPUNCHES","LOADEMAIL","PAY-FOR-HEADCOUNTS","UTIL1","WAGES","XLSTIME") ;
						AND NOT INLIST(tcMode,"XLSTIME-ONEDAY","CVSTEMP-SP2","CVSTEMP-CA1","CVSTEMP-CA2","CVSTEMP-CA4","CVSTEMP-CA3","POPULATEDRIVERSS_NUMS") THEN
					.TrackProgress('ERROR: unexpected Load Type!', LOGIT+SENDIT)
					THROW
				ENDIF

				.cMode = tcMode

				.cSubject = 'INSPERITY DATA IMPORT: ' + .cMode + ' for ' + lcDateSuffix

				* OPEN Excel
				.oExcel = CREATEOBJECT("excel.application")
				.oExcel.displayalerts = .F.
				.oExcel.VISIBLE = .F.

				DO CASE
*!*						CASE .cMode == "POPULATEDRIVERSS_NUMS"
*!*							.PopulateDriverSS_Nums()
					CASE .cMode == "CVSTEMP-SP2"
						* san pedro office
						.cTempWorksiteCode = 'SP2'
						.LoadTempTimeCSV()
					CASE .cMode == "CVSTEMP-CA1"
						* carson office
						.cTempWorksiteCode = 'CA1'
						.LoadTempTimeCSV()
					CASE .cMode == "CVSTEMP-CA2"
						* carson 2 office - check code!!!!!!!
						.cTempWorksiteCode = 'CA2'
						.LoadTempTimeCSV()
					CASE .cMode == "CVSTEMP-CA4"
						* mira loma office
						.cTempWorksiteCode = 'CA4'
						.LoadTempTimeCSV()
					CASE .cMode == "CVSTEMP-CA3"
						* mira loma 2 office - check code!!!!!!!
						.cTempWorksiteCode = 'CA3'
						.LoadTempTimeCSV()
					CASE .cMode == "TODDPRFORECAST"
						.NEWPRFORECAST()
					CASE .cMode == "401K"
						.Load401kDataXLS()
					CASE .cMode == "ACC"
						.LoadAccrualsData()
					CASE .cMode == "ADDDRIVERTOFOXPRO"
						.AddDriverToFoxPro()
					CASE .cMode == "CHECKDEPTTRANS"
						.CheckDeptTrans()
					CASE .cMode == "CHECKDIVSDEPTS"
						.CheckDivsDepts()
					CASE .cMode == "CHECKDRIVERS"
						.CheckDrivers()
					CASE .cMode == "CSVTIME"
						*THROW
						* this loads the csv time data we d/l via ftp twice daily
						* run this load late each night
						*.GetTimeDataCSV()  && old format
						.GetTimeDataCSV2()  && new format with in and out punches as of 8/21/14.
					CASE .cMode == "CSVTIMEDRIVER" 
						*THROW
						IF DAY(DATE()) = 1 THEN
							* we don't run this in auto mode on the first of the month because the month-to-date data we ftp'd from Insperity is missing most of the prior day's punches.
							* So on the first Mark will need to manually download from current day through the entire prior month before we can do LoadDriverPunches()
							.TrackProgress("1st of month: not running!", LOGIT+SENDIT)
							THROW
						ENDIF	
						* this loads the csv time data we d/l via ftp twice daily
						* Run this load in early-morning each day so it can load the west coast driver punches that came in as late as 7am EST.
						.GetTimeDataCSV2()  && new format with in and out punches as of 8/21/14.
						ldWorkDate = DATE() - 1  && default to yesterday
						*ldWorkDate = {^2018-04-11}
						.LoadDriverPunches( ldWorkDate )
						*.PopulateDriverSS_Nums()
					CASE .cMode == "EMP"
						.GetBasicEEInfo()
						.CheckDrivers()
						.CheckDeptTrans()
						.CheckDivsDepts()
					CASE .cMode == "EMP-TERMS" 
						.GetTermEEInfo()
*!*						CASE .cMode == "JOBCOSTING"
*!*							.MakeJobCostingSheet()
					CASE .cMode == "GPAYCALLOW"
						.GetGrossPayAndCarAllowance()
					CASE .cMode == "JOBCOSTING2" 
						.MakeJobCostingSheet2()
*!*						CASE .cMode == "LIABILITY"
*!*							.GetAccrualsLiability()
					CASE .cMode == "LOADDRIVERPUNCHES" 
						ldWorkDate = DATE() - 1  && default to yesterday   
						*ldWorkDate = {^2018-03-10}
						.LoadDriverPunches( ldWorkDate ) 
					CASE .cMode == "LOADEMAIL"
						.LoadEmailAddresses()
					CASE .cMode == "PAY-FOR-HEADCOUNTS" 
						.LoadPayDetailForHeadcounts()
					CASE .cMode == "UTIL1" 
						.AddDivColumn()
					CASE .cMode == "WAGES"
						USE F:\UTIL\TIMESTAR\DATA\TIMEDATA IN 0 ALIAS TIMEDATA
						.PopulateWageAmt()
					CASE .cMode == "XLSTIME"
						* this loads the 'data export - payroll' format from TimeStar report generator
						.GetTimeDataXLS() 
*!*						CASE .cMode == "XLSTIME-ONEDAY"
*!*							* this loads the 'data export - payroll' format from TimeStar report generator
*!*							* ONLY FOR ONE DAY - TYPICALLY THE LAST DAY OF THE MONTH THAT JUST ENDED
*!*							* DO THE EXPORT FOR THAT DAY ONLY BEFOR CALLING THIS
*!*							.GetTimeDataXLSOneDay() 
					OTHERWISE
						.TrackProgress("ERROR: unexpected mode parameter: " + .cMode, LOGIT+SENDIT+NOWAITIT)
						THROW
				ENDCASE

				.TrackProgress("INSPERITY DATA IMPORT process ended normally.", LOGIT+SENDIT+NOWAITIT)

			CATCH TO loError

				.TrackProgress('There was an error.',LOGIT+SENDIT)
				.TrackProgress('The Error # is: ' + TRANSFORM(loError.ERRORNO), LOGIT+SENDIT)
				.TrackProgress('The Error Message is: ' + TRANSFORM(loError.MESSAGE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred in Program: ' + TRANSFORM(loError.PROCEDURE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred at line #: ' + TRANSFORM(loError.LINENO), LOGIT+SENDIT)
				lnNumberOfErrors = lnNumberOfErrors + 1

				IF TYPE('.oExcel') = 'O' AND NOT ISNULL(.oExcel) THEN
					.oExcel.QUIT()
				ENDIF
				CLOSE DATABASES ALL

			ENDTRY

			WAIT CLEAR
			***************** INTERNAL email results ******************************


			.TrackProgress('About to send status email.',LOGIT)
			.TrackProgress('The logfile is: ' + .cLogFile, SENDIT)
			.TrackProgress("INSPERITY DATA IMPORT process started: " + .cStartTime, LOGIT+SENDIT)
			.TrackProgress("INSPERITY DATA IMPORT process finished: " + TTOC(DATETIME()), LOGIT+SENDIT)

			IF .lSendInternalEmailIsOn THEN
				DO FORM dartmail2 WITH .cSendTo,.cFrom,.cSubject,.cCC,.cAttach,.cBodyText,"A"
				.TrackProgress('Sent status email.',LOGIT)
			ELSE
				.TrackProgress('Did not send status email: THIS.lSendInternalEmailIsOn = FALSE.',LOGIT)
			ENDIF

		ENDWITH
		RETURN
	ENDFUNC && main
	
	
	PROCEDURE CheckDivsDepts
		* REPORT ON DIV OR DEPT MISMATCHES BETWEEN DATA IN TIMESTAR AND DATA IN ESC
		LOCAL lcOutputfile
		lcOutputfile = 'F:\UTIL\TIMESTAR\REPORTS\DIV_DEPT_MISMATCHES_' + DTOS(DATE()) + '.XLS'
		
		USE F:\UTIL\TIMESTAR\DATA\EEINFO AGAIN IN 0 ALIAS EEINFO 
		
		SELECT NAME, INSPID, DIVISION, DEPT, SPLIT2, SPLIT3 ;
			FROM EEINFO ;
			INTO CURSOR CURMISMATCHES ;
			WHERE (STATUS = 'A') AND ((DIVISION <> SPLIT2) OR (DEPT <> SPLIT3)) ;
			ORDER BY NAME
		
		IF USED('CURMISMATCHES') AND NOT EOF('CURMISMATCHES') THEN
			SELECT CURMISMATCHES
			COPY TO (lcOutputfile) XL5
			.AddAttachment(lcOutputfile)
		ENDIF
		
		IF USED('EEINFO') THEN
			USE IN EEINFO
		ENDIF
		IF USED('CURMISMATCHES') THEN
			USE IN CURMISMATCHES
		ENDIF
		
		RETURN
	ENDPROC


	PROCEDURE LoadTempTimeCSV

		* load temp time data from Staffing Solutions Group

		LOCAL lnMaxRow, lcRow, lnRow, oWorkbook, oWorksheet, lcSourceFile, lnRowsProcessed, lcMode, lcBusUnit, lcWorkDate, ldWorkDate
		LOCAL lcArchivedFile, lnCSVMonth, lnCSVYear, lcPayStatsFile, lnMUFACTOR

		PRIVATE m.FILE_NUM, m.NAME, m.WORKDATE, m.LUNCHMINS, m.PAY_TYPE, m.TOTHOURS, m.BUSUNIT, m.DIVISION, m.DEPT, m.WORKSITE, m.GLACCTNO
		PRIVATE m.COLLAR, m.TOTDOLLARS, m.TOLLCLASS, m.COMPANY, m.INPUNCHNM, m.INPUNCHDT, m.OUTPUNCHNM, m.OUTPUNCHDT

		WITH THIS

			DO CASE
				CASE .cTempWorksiteCode == 'SP2'
				CASE .cTempWorksiteCode == 'CA1'
				CASE .cTempWorksiteCode == 'CA2'
				CASE .cTempWorksiteCode == 'CA4'
				CASE .cTempWorksiteCode == 'CA3'
				OTHERWISE
					.TrackProgress('Undefned temp worksite code in LoadTempTimeCSV()', LOGIT+SENDIT)
					THROW
			ENDCASE
			
			
			* MAKE FILENAMES LOADED BASED ON WORKSITE IN CASE ABOVE !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
			lcSourceFile   = 'F:\FTPUSERS\SSG\IN\YTD_HOURS_DATA.CSV'
			lcArchivedFile = 'F:\FTPUSERS\SSG\IN\ARCHIVED\YTD_HOURS_DATA_' + DTOS(DATE()) + '.CSV'

			.TrackProgress('Source File = ' + lcSourceFile, LOGIT+SENDIT)

			.TrackProgress('Archived File = ' + lcArchivedFile, LOGIT+SENDIT)

			IF NOT FILE(lcSourceFile) THEN
				.TrackProgress('=====> ERROR: Source File ' + lcSourceFile + ' was not found!', LOGIT+SENDIT)
				THROW
			ENDIF

			USE F:\UTIL\ADPREPORTS\DATA\DEPTTRANS.DBF IN 0 ALIAS DEPTTRANS

			* OPEN THE TIMEDATA INFO TABLE
			IF USED('TIMEDATA') THEN
				USE IN TIMEDATA
			ENDIF

			IF USED('CURCSVPRE') THEN
				USE IN CURCSVPRE
			ENDIF

			IF USED('CURCSV') THEN
				USE IN CURCSV
			ENDIF

			*USE F:\UTIL\TIMESTAR\TESTDATA\TTIMEDATA IN 0 ALIAS TIMEDATA EXCLUSIVE   && test data

			USE F:\UTIL\TIMESTAR\DATA\TEMPTIME IN 0 ALIAS TIMEDATA EXCLUSIVE		&& live data


			SET HOURS TO 24

			CREATE CURSOR CURCSVPRE ( FILE_NUM C(7), NAME C(30), WORKDATEDT T, PAY_TYPE C(20), TOTHOURS N(5,2), TOTDOLLARS Y, COMPANY C(7), BUSUNITC C(1), ;
				DIVISION C(2), DEPT C(4), WORKSITE C(3), GLACCTNO C(4), COLLAR C(1), TOLLCLASS C(30), WORKDATE D, BUSUNIT C(5), ;
				INPUNCHNM C(3), INPUNCHDT T, OUTPUNCHNM C(3), OUTPUNCHDT T )

			SET DATE YMD

			.TrackProgress('Appending from CSV file: ' + lcSourceFile, LOGIT+SENDIT+NOWAITIT)

			SELECT CURCSVPRE
			APPEND FROM (lcSourceFile) CSV

			* examine 1st record to see which month we are loading
			SELECT CURCSVPRE
			GOTO TOP
			lnCSVMonth = MONTH( CURCSVPRE.WORKDATEDT )
			lnCSVYear = YEAR( CURCSVPRE.WORKDATEDT )

			.TrackProgress('=====> Reloading Month: ' + TRANSFORM(lnCSVMonth) + '/' + TRANSFORM(lnCSVYear), LOGIT+SENDIT+NOWAITIT)

			*THROW

			* delete the existing data for current month
			SELECT TIMEDATA
			DELETE FOR (MONTH(WORKDATE) = lnCSVMonth) AND (YEAR(WORKDATE) = lnCSVYear)
			PACK

			* SORT CURCSV BY DATE WITHIN NAME
			SELECT * FROM CURCSVPRE INTO CURSOR CURCSV ORDER BY NAME, WORKDATEDT READWRITE

			IF USED('CURCSVPRE') THEN
				USE IN CURCSVPRE
			ENDIF


			lcBusUnit = 'SCS'
			
			SELECT CURCSV
			SCAN
				*!*	DO CASE
				*!*		CASE CURCSV.BUSUNITC = 'A'
				*!*			lcBusUnit = 'ADMIN'
				*!*		CASE CURCSV.BUSUNITC = 'F'
				*!*			lcBusUnit = 'FF'
				*!*		CASE CURCSV.BUSUNITC = 'S'
				*!*			lcBusUnit = 'SCS'
				*!*		OTHERWISE
				*!*			lcBusUnit = '???'
				*!*	ENDCASE
				*lcWorkDate = STRTRAN(ALLTRIM( CURCSV.WORKDATEC ),"-","/")
				ldWorkDate = TTOD(CURCSV.WORKDATEDT)
				
				DO CASE
					CASE BETWEEN(ldWorkDate,{^2016-01-01},{^2099-12-31})
						lnMUFACTOR = 1.28
					OTHERWISE
						lnMUFACTOR = 1.28
				ENDCASE

				REPLACE CURCSV.BUSUNIT WITH 'SCS', ;
					CURCSV.WORKDATE WITH ldWorkDate, ;
					CURCSV.PAY_TYPE WITH UPPER(CURCSV.PAY_TYPE), ;
					CURCSV.ADP_COMP WITH 'TMP', ;
					CURCSV.WORKSITE WITH .cTempWorksiteCode, ;
					CURCSV.MUFACTOR WITH lnMUFACTOR ;
					IN CURCSV

			ENDSCAN
			*!*	BROWSE
			*!*	THROW

			* append the new data for current month
			SET DATE MDY

			lnRowsProcessed = 0

			SELECT CURCSV
			SCAN

				SCATTER MEMVAR

				* make any adjustments to memvars here...
				m.COMPANY = INT(VAL(m.COMPANY))


				INSERT INTO TIMEDATA FROM MEMVAR

				lnRowsProcessed = lnRowsProcessed + 1
				WAIT WINDOW NOWAIT 'Processing Time Data Spreadsheet Row: ' + TRANSFORM(lnRowsProcessed)

			ENDSCAN

			WAIT CLEAR

			.TrackProgress("In GetTimeDataCSV, lnRowsProcessed = " + TRANSFORM(lnRowsProcessed), LOGIT+SENDIT)

			* populate calculated fields that correspond to old kronos/adp values
			SELECT TIMEDATA
			SCAN FOR (MONTH(WORKDATE) = lnCSVMonth) AND (YEAR(WORKDATE) = lnCSVYear)

				*!*	IF TIMEDATA.COMPANY = 3273300 THEN
				*!*		REPLACE TIMEDATA.ADP_COMP WITH 'E88' IN TIMEDATA && SALARIED
				*!*	ELSE
				*!*		REPLACE TIMEDATA.ADP_COMP WITH 'E87' IN TIMEDATA && HOURLY
				*!*	ENDIF

				*!*	DO CASE
				*!*		CASE TIMEDATA.BUSUNIT = 'A'
				*!*			REPLACE TIMEDATA.BUSUNIT WITH 'ADM' IN TIMEDATA
				*!*		CASE TIMEDATA.BUSUNIT = 'F'
				*!*			REPLACE TIMEDATA.BUSUNIT WITH 'FF ' IN TIMEDATA
				*!*		CASE TIMEDATA.BUSUNIT = 'S'
				*!*			REPLACE TIMEDATA.BUSUNIT WITH 'SCS' IN TIMEDATA 
				*!*		OTHERWISE
				*!*			* NOTHING
				*!*	ENDCASE

				DO CASE
					CASE INLIST(ALLTRIM(TIMEDATA.PAY_TYPE),'REGULAR','GUARANTEE','LNCHPEN')
						REPLACE TIMEDATA.REGHOURS WITH TIMEDATA.TOTHOURS IN TIMEDATA
					CASE INLIST(ALLTRIM(TIMEDATA.PAY_TYPE),'OVERTIME','RETRO-OVT','WRKDHOL')
						REPLACE TIMEDATA.OTHOURS WITH TIMEDATA.TOTHOURS IN TIMEDATA
					CASE INLIST(ALLTRIM(TIMEDATA.PAY_TYPE),'DOUBLE')
						REPLACE TIMEDATA.DTHOURS WITH TIMEDATA.TOTHOURS IN TIMEDATA
					OTHERWISE
						REPLACE TIMEDATA.CODEHOURS WITH TIMEDATA.TOTHOURS IN TIMEDATA
				ENDCASE

*!*					* DELETE BAD DATA
*!*					* SARA SUPERVISOR, ETC.
*!*					IF VAL(TIMEDATA.FILE_NUM) < 10 THEN
*!*						DELETE IN TIMEDATA
*!*					ENDIF

*!*					* BAD DEPT CODES
*!*					IF VAL(TIMEDATA.DEPT) = 0 THEN
*!*						DELETE IN TIMEDATA
*!*					ENDIF


				* fill in missing tollclass INFO, from DEPT lookup
				SELECT DEPTTRANS
				LOCATE FOR DEPT = TIMEDATA.DEPT
				IF FOUND() THEN
					REPLACE TIMEDATA.TOLLCLASS WITH DEPTTRANS.HCFTECODE, TIMEDATA.TCDESC WITH DEPTTRANS.HCFTEDESC IN TIMEDATA
				ENDIF
				***********************************************************************************************


			ENDSCAN

			*SELECT TIMEDATA
			*BROWSE

			lcPayStatsFile = 'F:\UTIL\TIMESTAR\REPORTS\TEMP_PAY_STATS_' + DTOS(DATE()) + '.XLS'

			IF USED('CURPAYTYPES') THEN
				USE IN CURPAYTYPES
			ENDIF

			SELECT PAY_TYPE, COUNT(*) AS COUNT ;
				FROM CURCSV INTO CURSOR CURPAYTYPES ;
				GROUP BY 1 ORDER BY 1

			SELECT CURPAYTYPES
			COPY TO (lcPayStatsFile) XL5
			*.cAttach = lcPayStatsFile
			.AddAttachment(lcPayStatsFile)



			IF USED('CURCSV') THEN
				USE IN CURCSV
			ENDIF

			IF USED('TIMEDATA') THEN
				USE IN TIMEDATA
			ENDIF

			* archive the source file
			COPY FILE (lcSourceFile) TO (lcArchivedFile)

			IF FILE(lcArchivedFile) THEN
				.TrackProgress('Source file was archived to: ' + lcArchivedFile, LOGIT+SENDIT)
				DELETE FILE (lcSourceFile)
			ELSE
				.TrackProgress('ERROR archiving Source file [' + lcSourceFile + '] to: ' + lcArchivedFile, LOGIT+SENDIT)
			ENDIF

		ENDWITH
		RETURN
	ENDPROC  && LoadTempTimeCSV




	PROCEDURE LoadEmailAddresses
		* add email addresses to EEINFO...

		WITH THIS

			LOCAL lnMaxRow, lcRow, lnRow, oWorkbook, oWorksheet, lcSourceFile, lnRowsProcessed
		
			USE F:\UTIL\TIMESTAR\DATA\EEINFO IN 0 ALIAS EEINFO

			lcSourceFile = 'F:\UTIL\TIMESTAR\REPORTS\BASICEEINFO\EMAILINFO.XLS'

			.TrackProgress('Source File = ' + lcSourceFile, LOGIT+SENDIT)

			oWorkbook = .oExcel.workbooks.OPEN(lcSourceFile)

			oWorksheet = oWorkbook.Worksheets[1]
			
			lnRowsProcessed = 0

			lnMaxRow = 1000

			FOR lnRow = 2 TO lnMaxRow

				WAIT WINDOW NOWAIT 'Processing EMAIL ADDRESSES Data Spreadsheet Row: ' + TRANSFORM(lnRow)

				lcRow = ALLTRIM(STR(lnRow))
				
				m.INSPID = NVL(oWorksheet.RANGE("D"+lcRow).VALUE,0)
				
				* WE'RE DONE IF INSPID WAS NULL
				IF (TYPE('m.INSPID') <> 'N') OR (m.INSPID = 0) THEN
					EXIT FOR
				ENDIF
				
				m.EMAIL = NVL(oWorksheet.RANGE("C"+lcRow).VALUE,' ')	
				
				SELECT EEINFO
				LOCATE FOR INSPID = m.INSPID
				IF FOUND() THEN
					REPLACE EEINFO.EMAIL WITH m.EMAIL IN EEINFO
				ENDIF
				
				lnRowsProcessed = lnRowsProcessed + 1

			ENDFOR

			oWorkbook.CLOSE()

			SELECT EEINFO
			BROW				
			
			USE IN EEINFO
			
			.TrackProgress("In LoadEmailAddresses, lnRowsProcessed = " + TRANSFORM(lnRowsProcessed), LOGIT+SENDIT)

		ENDWITH
		RETURN
	ENDPROC  && LoadEmailAddresses



	PROCEDURE LoadPayDetailForHeadcounts
	
		* This load will support doing monthly headcounts for Ken, based on those who received checks.
		
		* 1 - run ESC report 'detail by date for headcounts' in my saved reports, save as F:\UTIL\INSPERITY\ESCREPORTS\PAYROLLINFO\DETAIL BY DATE FOR HEADCOUNTS.XLS
		

		LOCAL lnMaxRow, lcRow, lnRow, oWorkbook, oWorksheet, lcSourceFile, lnRowsProcessed

		PRIVATE m.INSPID, m.EMPLOYEE, m.GROSSPAY, m.WKDSTATE, m.PAYDATE

		WITH THIS


			* CLEAR OUT THE Paydet data TABLE

			USE F:\UTIL\INSPERITY\DATA\PAYDET IN 0 ALIAS PAYDET EXCLUSIVE 
			DELETE ALL
			PACK			
		

			lcSourceFile = 'F:\UTIL\INSPERITY\ESCREPORTS\PAYROLLINFO\DETAIL BY DATE FOR HEADCOUNTS.XLS'

			.TrackProgress('Source File = ' + lcSourceFile, LOGIT+SENDIT)

			oWorkbook = .oExcel.workbooks.OPEN(lcSourceFile)

			oWorksheet = oWorkbook.Worksheets[1]
			
			lnRowsProcessed = 0

			lnMaxRow = 15000

			FOR lnRow = 8 TO lnMaxRow

				WAIT WINDOW NOWAIT 'Processing DETAIL BY DATE FOR HEADCOUNTS.XLS Data Spreadsheet Row: ' + TRANSFORM(lnRow)

				STORE "" TO m.EMPLOYEE, m.WKDSTATE
				STORE 0 TO m.INSPID, m.GROSSPAY
				STORE {} TO m.PAYDATE

				lcRow = ALLTRIM(STR(lnRow))
				
				m.INSPID = NVL(oWorksheet.RANGE("A"+lcRow).VALUE,0)
				
				* WE'RE DONE IF WE ENCOUNTER THE WORD 'IMPORTANT' IN COL A
				IF (TYPE('m.INSPID') = 'C') AND ("IMPORTANT" $ m.INSPID) THEN
					EXIT FOR
				ENDIF				
				
				* Otherwise SKIP the spreadsheet row if m.INSPID is not numeric and > 0
				IF (TYPE('m.INSPID') <> 'N') OR (m.INSPID <= 0) THEN
					LOOP
				ENDIF
				
				m.EMPLOYEE = oWorksheet.RANGE("B"+lcRow).VALUE				
				
				m.GROSSPAY = NVL(oWorksheet.RANGE("C"+lcRow).VALUE,0)
				
				m.WKDSTATE = ALLTRIM(NVL(oWorksheet.RANGE("D"+lcRow).VALUE,''))

				m.PAYDATE = NVL(oWorksheet.RANGE("E"+lcRow).VALUE,{})

				lnRowsProcessed = lnRowsProcessed + 1

				INSERT INTO PAYDET FROM MEMVAR

			ENDFOR

			oWorkbook.CLOSE()
			
			
			* populate cmonth, cyear, division, busunit fields
			USE F:\UTIL\TIMESTAR\DATA\EEINFO IN 0 ALIAS EEINFO 
			
			SELECT PAYDET
			SCAN
				SELECT EEINFO 
				LOCATE FOR INSPID = PAYDET.INSPID
				IF FOUND() THEN
					REPLACE PAYDET.DIVISION WITH EEINFO.DIVISION IN PAYDET
				ENDIF			
			ENDSCAN
			
			SELECT PAYDET
			SCAN
				REPLACE PAYDET.NMONTH WITH MONTH(PAYDET.PAYDATE), PAYDET.NYEAR WITH YEAR(PAYDET.PAYDATE) IN PAYDET
			ENDSCAN

			.TrackProgress("In LoadPayDetailForHeadcounts, lnRowsProcessed = " + TRANSFORM(lnRowsProcessed), LOGIT+SENDIT)
			
			SELECT PAYDET
			BROWSE

		ENDWITH
		RETURN
	ENDPROC  && LoadPayDetailForHeadcounts

	PROCEDURE NEWPRFORECAST
		* LOAD DATA FOR PR FORECASTING FOR TODD

		* run ESC report 'pay-carallow- by wkdstate' in my saved reports for 1 month period, save as F:\UTIL\INSPERITY\ESCREPORTS\payrollinfo\new_pay_carallow_by_wkdstate.xls
		
		
		* adjust maxrow values based on reports

		LOCAL lnMaxRow, lcRow, lnRow, oWorkbook, oWorksheet, lcSourceFile, lnRowsProcessed

		PRIVATE m.INSPID, m.EMPLOYEE, m.GROSSPAY, m.CARALLOW, m.WKDSTATE

		WITH THIS


			* CLEAR OUT THE GPAYALLOW data TABLE

			USE F:\UTIL\INSPERITY\DATA\GPAYALLOW IN 0 ALIAS GPAYALLOW EXCLUSIVE
			DELETE ALL
			PACK			
		
				
			lnRowsProcessed = 0

			lcSourceFile = 'F:\UTIL\INSPERITY\ESCREPORTS\PAYROLLINFO\NEW_PAY_CARALLOW_BY_WKDSTATE.XLS'

			.TrackProgress('Source File = ' + lcSourceFile, LOGIT+SENDIT)
			
			IF FILE(lcSourceFile) THEN

				oWorkbook = .oExcel.workbooks.OPEN(lcSourceFile)

				oWorksheet = oWorkbook.Worksheets[1]

				lnMaxRow = 649

				FOR lnRow = 14 TO lnMaxRow

					WAIT WINDOW NOWAIT 'Processing PAY_CARALLOW_BY_WKDSTATE Data Spreadsheet Row: ' + TRANSFORM(lnRow)

					STORE "" TO m.EMPLOYEE, m.WKDSTATE
					STORE 0 TO m.INSPID, m.GROSSPAY, m.CARALLOW

					lcRow = ALLTRIM(STR(lnRow))

					
					m.EMPLOYEE = NVL(oWorksheet.RANGE("A"+lcRow).VALUE,'')

					* WE'RE DONE IF WE ENCOUNTER THE WORD 'IMPORTANT' IN COL A
					IF (TYPE('m.EMPLOYEE') = 'C') AND ("IMPORTANT" $ m.EMPLOYEE) THEN
						EXIT FOR
					ENDIF				

					
					m.INSPID = NVL(oWorksheet.RANGE("B"+lcRow).VALUE,0)				
					
					* Otherwise SKIP the spreadsheet row if m.INSPID is not numeric and > 0
					IF (TYPE('m.INSPID') <> 'N') OR (m.INSPID <= 0) THEN
						LOOP
					ENDIF
					
					m.WKDSTATE = ALLTRIM(NVL(oWorksheet.RANGE("C"+lcRow).VALUE,''))
					
					m.CARALLOW = NVL(oWorksheet.RANGE("U"+lcRow).VALUE,0)
					
					m.GROSSPAY = NVL(oWorksheet.RANGE("AN"+lcRow).VALUE,0)

					INSERT INTO GPAYALLOW FROM MEMVAR
					
					lnRowsProcessed = lnRowsProcessed + 1

				ENDFOR

				oWorkbook.CLOSE()				

			ENDIF && FILE(lcSourceFile) THEN

			.TrackProgress("In NEWPRFORECAST, lnRowsProcessed = " + TRANSFORM(lnRowsProcessed), LOGIT+SENDIT)
			
			* populate division, dept, deptdesc, business unit  --- no mastercomp
			USE F:\UTIL\TIMESTAR\DATA\EEINFO IN 0 ALIAS EEINFO 
			
			SELECT GPAYALLOW
			SCAN
				SELECT EEINFO 
				LOCATE FOR INSPID = GPAYALLOW.INSPID
				IF FOUND() THEN
					REPLACE GPAYALLOW.DIVISION WITH EEINFO.DIVISION, GPAYALLOW.BUSUNIT WITH EEINFO.BUSUNIT, GPAYALLOW.DEPT WITH EEINFO.DEPT, GPAYALLOW.DEPTDESC WITH EEINFO.DEPTDESC IN GPAYALLOW
				ENDIF			
			ENDSCAN
			
			* REVERSE SIGN ON CAR ALLOWANCE
			SELECT GPAYALLOW
			SCAN
				REPLACE GPAYALLOW.CARALLOW WITH (-1 * GPAYALLOW.CARALLOW) IN GPAYALLOW
			ENDSCAN
			
			* add data source
			SELECT GPAYALLOW
			SCAN
				REPLACE GPAYALLOW.datasource WITH 'INSPERITY' IN GPAYALLOW
			ENDSCAN
			

*!*				SET PROCEDURE TO VALIDATIONS
*!*				SELECT GPAYALLOW
*!*				REPLACE ALL GPAYALLOW.MASTERCOMP WITH GetCompanyFromStringDivision(GPAYALLOW.DIVISION) IN GPAYALLOW


			SELECT GPAYALLOW
			BROWSE
			
			SELECT GPAYALLOW
			COPY TO C:\A\PRFORECAST_JAN_2016.XLS XL5
			

		ENDWITH
		RETURN
	
	ENDPROC  &&  NEWPRFORECAST


	PROCEDURE GetGrossPayAndCarAllowance
	
		* This load will support getting data for Todd's KPMG reoport which breaks down grosspay and car allow by worked state.
		
		* It will load 2 excel reports which must be run for the same date range.

		* 1 - run ESC report 'grosspay by wkdstate' in my saved reports, save as F:\UTIL\INSPERITY\ESCREPORTS\payrollinfo\grosspay_by_wkdstate.xls

		* 2 - run ESC report 'pay-carallow- by wkdstate' in my saved reports, save as F:\UTIL\INSPERITY\ESCREPORTS\payrollinfo\pay_carallow_by_wkdstate.xls
		
		* NOTE: just load grosspay_by_wkdstate if you want to run a headcount report
		
		* adjust maxrow values based on reports

		LOCAL lnMaxRow, lcRow, lnRow, oWorkbook, oWorksheet, lcSourceFile, lnRowsProcessed

		PRIVATE m.INSPID, m.EMPLOYEE, m.GROSSPAY, m.CARALLOW, m.WKDSTATE

		WITH THIS


			* CLEAR OUT THE GPAYALLOW data TABLE

			USE F:\UTIL\INSPERITY\DATA\GPAYALLOW IN 0 ALIAS GPAYALLOW EXCLUSIVE
			DELETE ALL
			PACK			
		

			lcSourceFile = 'F:\UTIL\INSPERITY\ESCREPORTS\PAYROLLINFO\GROSSPAY_BY_WKDSTATE.XLSX'

			.TrackProgress('Source File = ' + lcSourceFile, LOGIT+SENDIT)

			oWorkbook = .oExcel.workbooks.OPEN(lcSourceFile)

			oWorksheet = oWorkbook.Worksheets[1]
			
			lnRowsProcessed = 0

			lnMaxRow = 2300

			FOR lnRow = 10 TO lnMaxRow

				WAIT WINDOW NOWAIT 'Processing GROSSPAY_BY_WKDSTATE Data Spreadsheet Row: ' + TRANSFORM(lnRow)

				STORE "" TO m.EMPLOYEE, m.WKDSTATE
				STORE 0 TO m.INSPID, m.GROSSPAY, m.CARALLOW

				lcRow = ALLTRIM(STR(lnRow))
				
				m.INSPID = NVL(oWorksheet.RANGE("A"+lcRow).VALUE,0)
				
				* WE'RE DONE IF WE ENCOUNTER THE WORD 'IMPORTANT' IN COL A
				IF (TYPE('m.INSPID') = 'C') AND ("IMPORTANT" $ m.INSPID) THEN
					EXIT FOR
				ENDIF				
				
				* Otherwise SKIP the spreadsheet row if m.INSPID is not numeric and > 0
				IF (TYPE('m.INSPID') <> 'N') OR (m.INSPID <= 0) THEN
					LOOP
				ENDIF
				
				m.EMPLOYEE = oWorksheet.RANGE("B"+lcRow).VALUE				
				
				m.GROSSPAY = NVL(oWorksheet.RANGE("C"+lcRow).VALUE,0)
				
				m.WKDSTATE = ALLTRIM(NVL(oWorksheet.RANGE("D"+lcRow).VALUE,''))

				INSERT INTO GPAYALLOW FROM MEMVAR

				lnRowsProcessed = lnRowsProcessed + 1

			ENDFOR

			oWorkbook.CLOSE()
			
*!*				SELECT GPAYALLOW
*!*				BROWSE

			.TrackProgress("In GetGrossPayAndCarAllowance, lnRowsProcessed = " + TRANSFORM(lnRowsProcessed), LOGIT+SENDIT)
			
			
			

			lcSourceFile = 'F:\UTIL\INSPERITY\ESCREPORTS\PAYROLLINFO\PAY_CARALLOW_BY_WKDSTATE.XLSX'

			.TrackProgress('Source File = ' + lcSourceFile, LOGIT+SENDIT)
			
			IF FILE(lcSourceFile) THEN

				oWorkbook = .oExcel.workbooks.OPEN(lcSourceFile)

				oWorksheet = oWorkbook.Worksheets[1]
				
				lnRowsProcessed = 0

				lnMaxRow = 792

				FOR lnRow = 14 TO lnMaxRow

					WAIT WINDOW NOWAIT 'Processing PAY_CARALLOW_BY_WKDSTATE Data Spreadsheet Row: ' + TRANSFORM(lnRow)

					STORE "" TO m.EMPLOYEE, m.WKDSTATE
					STORE 0 TO m.INSPID, m.GROSSPAY, m.CARALLOW

					lcRow = ALLTRIM(STR(lnRow))

					
					m.EMPLOYEE = NVL(oWorksheet.RANGE("A"+lcRow).VALUE,'')

					* WE'RE DONE IF WE ENCOUNTER THE WORD 'IMPORTANT' IN COL A
					IF (TYPE('m.EMPLOYEE') = 'C') AND ("IMPORTANT" $ m.EMPLOYEE) THEN
						EXIT FOR
					ENDIF				

					
					m.INSPID = NVL(oWorksheet.RANGE("B"+lcRow).VALUE,0)				
					
					* Otherwise SKIP the spreadsheet row if m.INSPID is not numeric and > 0
					IF (TYPE('m.INSPID') <> 'N') OR (m.INSPID <= 0) THEN
						LOOP
					ENDIF
					
					m.WKDSTATE = ALLTRIM(NVL(oWorksheet.RANGE("C"+lcRow).VALUE,''))
					
					m.CARALLOW = NVL(oWorksheet.RANGE("AA"+lcRow).VALUE,0)

					* update car allow for each person
					SELECT GPAYALLOW 
					LOCATE FOR INSPID = m.INSPID
					IF FOUND() THEN
						REPLACE GPAYALLOW.CARALLOW WITH m.CARALLOW IN GPAYALLOW 
					ELSE
						* person's inspid not found - insert new record
						INSERT INTO GPAYALLOW FROM MEMVAR
					ENDIF
					
					lnRowsProcessed = lnRowsProcessed + 1

				ENDFOR

				oWorkbook.CLOSE()
				
*!*					* update dept and deptdesc in gpayallow fro eeinfo
*!*					USE f:\util\timestar\data\eeinfo.dbf IN 0 ALIAS eeinfo
*!*					
*!*					SELECT GPAYALLOW 
*!*					SCAN
*!*						SELECT eeinfo
*!*						LOCATE FOR INSPID = GPAYALLOW.INSPID
*!*						IF FOUND() THEN
*!*							REPLACE GPALLOW.DEPT WITH EEINFO.DEPT, GPALLOW.DEPTDESC WITH EEINFO.DEPTDESC IN GPAYALLOW
*!*						ENDIF				
*!*					ENDSCAN
				

				.TrackProgress("In GetGrossPayAndCarAllowance, lnRowsProcessed = " + TRANSFORM(lnRowsProcessed), LOGIT+SENDIT)
			ENDIF && FILE(lcSourceFile) THEN
			
			* populate division, business unit  --- no mastercomp
			USE F:\UTIL\TIMESTAR\DATA\EEINFO IN 0 ALIAS EEINFO 
			
			SELECT GPAYALLOW
			SCAN
				SELECT EEINFO 
				LOCATE FOR INSPID = GPAYALLOW.INSPID
				IF FOUND() THEN
					REPLACE GPAYALLOW.DIVISION WITH EEINFO.DIVISION, GPAYALLOW.BUSUNIT WITH EEINFO.BUSUNIT IN GPAYALLOW
				ENDIF			
			ENDSCAN
			
			* REVERSE SIGN ON CAR ALLOWANCE
			SELECT GPAYALLOW
			SCAN
				REPLACE GPAYALLOW.CARALLOW WITH (-1 * GPAYALLOW.CARALLOW) IN GPAYALLOW
			ENDSCAN
			
			* add data source
			SELECT GPAYALLOW
			SCAN
				REPLACE GPAYALLOW.datasource WITH 'INSPERITY' IN GPAYALLOW
			ENDSCAN
			

*!*				SET PROCEDURE TO VALIDATIONS
*!*				SELECT GPAYALLOW
*!*				REPLACE ALL GPAYALLOW.MASTERCOMP WITH GetCompanyFromStringDivision(GPAYALLOW.DIVISION) IN GPAYALLOW


			SELECT GPAYALLOW
			BROWSE
			

		ENDWITH
		RETURN
	ENDPROC  && GetGrossPayAndCarAllowance



	PROCEDURE Load401kDataXLS

		* this loads the '401k Report' in My Saved Reports in Insperity ESC.
		
		* the files to be loaded should be saved in:  F:\UTIL\INSPERITY\ESCREPORTS\401kinfo\

		LOCAL lnMaxRow, lcRow, lnRow, oWorkbook, oWorksheet, lcSourceFile, lnRowsProcessed

		PRIVATE m.MONTH, m.YEAR, m.NAME, m.AMT_K, m.CATCHUP_K, m.AMT_R, m.CATCHUP_R, m.COMPMATCH

		WITH THIS


			* OPEN THE 401k data TABLE
			IF USED('DATA401K') THEN
				USE IN DATA401K
			ENDIF

			USE F:\UTIL\TIMESTAR\DATA\EEINFO AGAIN IN 0 ALIAS EEINFO
			
			USE F:\UTIL\TIMESTAR\DATA\DATA401K IN 0 ALIAS DATA401K EXCLUSIVE
			

			*lcSourceFile = 'F:\UTIL\TIMESTAR\REPORTS\TIMEDATA\DATA EXPORT - PAYROLL.XLS'
			lcSourceFile = GETFILE('xls','Get a 401k file')

			.TrackProgress('Source File = ' + lcSourceFile, LOGIT+SENDIT)

			oWorkbook = .oExcel.workbooks.OPEN(lcSourceFile)

			oWorksheet = oWorkbook.Worksheets[1]
			
			LOCAL lcReportDateRange, lcToDate, ldToDate
			lcReportDateRange = ALLTRIM(oWorksheet.RANGE("A4").VALUE)
			lcToDate = RIGHT(lcReportDateRange,10)
			ldToDate = CTOD(lcToDate)
			m.MONTH = MONTH(ldToDate)
			m.YEAR = YEAR(ldToDate)

			SELECT DATA401K
			DELETE ALL  && FOR (month = m.MONTH) AND (year = m.YEAR) IN DATA401K
			PACK

			lnRowsProcessed = 0

			lnMaxRow = 240

			FOR lnRow = 11 TO lnMaxRow

				WAIT WINDOW NOWAIT 'Processing 401K Data Spreadsheet Row: ' + TRANSFORM(lnRow)

				STORE "" TO m.NAME
				STORE 0 TO m.AMT_K, m.CATCHUP_K, m.AMT_R, m.CATCHUP_R, m.COMPMATCH

				lcRow = ALLTRIM(STR(lnRow))
				
				m.INSPID = oWorksheet.RANGE("A"+lcRow).VALUE
				
				m.NAME = oWorksheet.RANGE("B"+lcRow).VALUE
				
				IF ( ISNULL(m.NAME) OR EMPTY(m.NAME) ) THEN
					* we are past the last data row of the section we are loading! exit the loop...
					EXIT FOR
				ENDIF
				
				m.AMT_K = NVL(oWorksheet.RANGE("C"+lcRow).VALUE,0)
				
				m.CATCHUP_K = NVL(oWorksheet.RANGE("D"+lcRow).VALUE,0)
				
				m.AMT_R = NVL(oWorksheet.RANGE("E"+lcRow).VALUE,0)
				
				m.CATCHUP_R = NVL(oWorksheet.RANGE("F"+lcRow).VALUE,0)
				
				m.COMPMATCH = NVL(oWorksheet.RANGE("G"+lcRow).VALUE,0)
				
				

				INSERT INTO DATA401K FROM MEMVAR

				lnRowsProcessed = lnRowsProcessed + 1

			ENDFOR

			oWorkbook.CLOSE()

			.TrackProgress("In Load401kDataXLS, lnRowsProcessed = " + TRANSFORM(lnRowsProcessed), LOGIT+SENDIT)

			
			* populate Division in DATA401K
			SELECT DATA401K
			SCAN
				SELECT EEINFO
				LOCATE FOR (INSPID = DATA401K.INSPID)
				IF FOUND() THEN
					REPLACE DATA401K.DIVISION WITH EEINFO.DIVISION IN DATA401K
				ELSE
					.TrackProgress('ERROR: Inspid not found in EEINFO = ' + TRANSFORM(DATA401K.INSPID), LOGIT+SENDIT)
				ENDIF			
			ENDSCAN
			
			SELECT DATA401K
			BROWSE

			IF USED('CURDIV') THEN
				USE IN CURDIV
			ENDIF
			
			SELECT MONTH, YEAR, DIVISION, SUM(COMPMATCH) AS COMPMATCH ;
			FROM DATA401K ;
			INTO CURSOR CURDIV ;
			GROUP BY 1, 2, 3 ;
			ORDER BY 3
			
			SELECT CURDIV
			COPY TO C:\A\COMPMATCH_Q1_2015.XLS XL5

			IF USED('CURDIV') THEN
				USE IN CURDIV
			ENDIF

			USE IN DATA401K

		ENDWITH
		RETURN
	ENDPROC  && Load401kDataXLS




	PROCEDURE AddDriverToFoxPro
		WITH THIS

			LOCAL lnPos

			CLOSE DATABASES ALL

			USE F:\UTIL\TIMESTAR\DATA\EEINFO AGAIN IN 0 ALIAS EEINFO
			USE F:\HR\HRDATA\EMPLOYEE IN 0 ALIAS EMPLOYEE

			*SET STEP ON

			SELECT EEINFO
			LOCATE FOR INSPID = 2397329
			IF FOUND() THEN
				SCATTER MEMVAR
				lnPos = AT(",",m.NAME) - 1
				m.LEGALLN = LEFT(m.NAME,lnPos)
				WAIT WINDOW 'LEGALLN = ' + m.LEGALLN

				SELECT EMPLOYEE
				LOCATE FOR INSPID == m.INSPID
				IF FOUND() THEN
					WAIT WINDOW "Inspid is already in FoxPro!"
				ELSE
					* add the employee
					m.COMPANY = IIF(m.DIVISION = '55','W','T')
					SELECT EMPLOYEE
					APPEND BLANK
					REPLACE EMPLOYEE WITH m.NAME, ACTIVE WITH .T., HIRE_DATE WITH m.HIREDATE, DIVISION WITH INT(VAL(m.DIVISION)), DEPT WITH INT(VAL(m.DEPT)), ;
						LEGALLN WITH m.LEGALLN, INSPID WITH m.INSPID, EMP_NUM WITH m.BADGENUM, COMPANY WITH m.COMPANY IN EMPLOYEE
				ENDIF

				SELECT EMPLOYEE
				BROWSE FOR INSPID == m.INSPID

			ENDIF


			CLOSE DATABASES ALL

		ENDWITH
	ENDPROC


	PROCEDURE CheckDeptTrans
		WITH THIS

			.TrackProgress('Running CheckDeptTrans()......',LOGIT+SENDIT)

			IF USED('EEINFO') THEN
				USE IN EEINFO
			ENDIF
			IF USED('DEPTTRANS') THEN
				USE IN DEPTTRANS
			ENDIF

			USE F:\UTIL\TIMESTAR\DATA\EEINFO AGAIN IN 0 ALIAS EEINFO
			USE F:\UTIL\ADPREPORTS\DATA\DEPTTRANS.DBF AGAIN IN 0 ALIAS DEPTTRANS

			SELECT EEINFO
			SCAN FOR STATUS <> 'T'
				SELECT DEPTTRANS
				LOCATE FOR DEPT == EEINFO.DEPT
				IF NOT FOUND() THEN
					.TrackProgress('ERROR on ' + ALLTRIM(EEINFO.NAME) + ': Dept not found in DEPTTRANS',LOGIT+SENDIT)
				ELSE
					* don't bother checking collartype any more, since HR says it no longer is fixed per department. 12/1/2017 MB
					*IF (COLLARTYPE <> EEINFO.COLLAR) OR (GLACCTNO <> EEINFO.GLACCTNO) OR (HCFTECODE <> EEINFO.TOLLCLASS) THEN
					IF (GLACCTNO <> EEINFO.GLACCTNO) OR (HCFTECODE <> EEINFO.TOLLCLASS) THEN
						.TrackProgress('ERROR: ' + ALLTRIM(EEINFO.NAME) + ',   Insperity ID# = ' + TRANSFORM(EEINFO.INSPID) + ',  Dept = ' + EEINFO.DEPT + '  ' + EEINFO.DEPTDESC,LOGIT+SENDIT)
						*!*	IF (COLLARTYPE <> EEINFO.COLLAR)  THEN
						*!*		.TrackProgress('     In TimeStar, Collar = ' + EEINFO.COLLAR,LOGIT+SENDIT)
						*!*		.TrackProgress('       Should be, Collar = ' + DEPTTRANS.COLLARTYPE,LOGIT+SENDIT)
						*!*	ENDIF
						IF  (GLACCTNO <> EEINFO.GLACCTNO) THEN
							.TrackProgress('     In TimeStar, GLACCTNO = ' + EEINFO.GLACCTNO,LOGIT+SENDIT)
							.TrackProgress('       Should be, GLACCTNO = ' + DEPTTRANS.GLACCTNO,LOGIT+SENDIT)
						ENDIF
						IF (HCFTECODE <> EEINFO.TOLLCLASS) THEN
							.TrackProgress('     In TimeStar, TOLLCLASS = ' + EEINFO.TOLLCLASS,LOGIT+SENDIT)
							.TrackProgress('       Should be, TOLLCLASS = ' + DEPTTRANS.HCFTECODE,LOGIT+SENDIT)
						ENDIF
					ENDIF
				ENDIF
			ENDSCAN

			IF USED('EEINFO') THEN
				USE IN EEINFO
			ENDIF
			IF USED('DEPTTRANS') THEN
				USE IN DEPTTRANS
			ENDIF

		ENDWITH
		RETURN
	ENDPROC  &&  CheckDeptTrans


	PROCEDURE CheckDrivers
		WITH THIS

			.TrackProgress('Running CheckDrivers()......',LOGIT+SENDIT)

			LOCAL lcDriverFile

			IF USED('EEINFO') THEN
				USE IN EEINFO
			ENDIF
			IF USED('CURDRIVERS') THEN
				USE IN CURDRIVERS
			ENDIF
			IF USED('EMPLOYEE') THEN
				USE IN EMPLOYEE
			ENDIF

			USE F:\UTIL\TIMESTAR\DATA\EEINFO AGAIN IN 0 ALIAS EEINFO
			USE F:\HR\HRDATA\EMPLOYEE AGAIN IN 0 ALIAS EMPLOYEE


			SELECT A.INSPID, A.NAME, A.BADGENUM, A.DIVISION, A.DEPT, A.STATUS, A.SUPERVISOR, NVL(B.LEGALLN,'') AS FOXLN, NVL(B.EMP_NUM,0) AS FOXNUM ;
				FROM EEINFO A ;
				LEFT OUTER JOIN EMPLOYEE B ;
				ON B.INSPID = A.INSPID ;
				INTO CURSOR CURDRIVERS ;
				WHERE A.STATUS <> 'T' ;
				AND A.DEPT = '0650' ;
				AND A.DIVISION <> '23' ;
				HAVING ( EMPTY(FOXLN) OR (FOXNUM = 0) ) ;
				ORDER BY A.NAME



			IF USED('CURDRIVERS') AND NOT EOF('CURDRIVERS') THEN
				lcDriverFile = 'F:\UTIL\TIMESTAR\REPORTS\CHECKDRIVERS_' + DTOS(DATE()) + '.XLS'
				SELECT CURDRIVERS
				COPY TO (lcDriverFile) XL5
				*.cAttach = .cAttach + IIF(EMPTY(.cAttach),"",",") + lcDriverFile
				.AddAttachment(lcDriverFile)
			ENDIF


			IF USED('EEINFO') THEN
				USE IN EEINFO
			ENDIF
			IF USED('CURDRIVERS') THEN
				USE IN CURDRIVERS
			ENDIF

		ENDWITH

		RETURN
	ENDPROC  && CheckDrivers


	PROCEDURE MakeJobCostingSheet
		* make sheet of job costing info for Natalie @ Insperity
		* for recent hires...
		WITH THIS
			LOCAL lnMonth, lcFile, lnYear
			lnMonth = MONTH(DATE())
			lnYear = YEAR(DATE())
			lcFile = "C:\A\TOLL_JOB_COSTING_" + DTOS(DATE()) + ".XLS"

			IF USED('EEINFO') THEN
				USE IN EEINFO
			ENDIF
			IF USED('CURCOSTING') THEN
				USE IN CURCOSTING
			ENDIF

			USE F:\UTIL\TIMESTAR\DATA\EEINFO IN 0 ALIAS EEINFO

			SELECT ;
				COMPANY, ;
				NAME, ;
				INSPID, ;
				DIVISION, ;
				DEPT, ;
				BUSUNIT, ;
				WORKSITE, ;
				GLACCTNO, ;
				COLLAR, ;
				TOLLCLASS, ;
				PTODATE ;
				FROM EEINFO ;
				INTO CURSOR CURCOSTING ;
				WHERE MONTH(HIREDATE) = lnMonth ;
				AND YEAR(HIREDATE) = lnYear ;
				ORDER BY NAME

			SELECT CURCOSTING
			COPY TO (lcFile) XL5

			.TrackProgress('Created Costing Sheet: ' + lcFile,LOGIT+SENDIT)

			IF USED('EEINFO') THEN
				USE IN EEINFO
			ENDIF
			IF USED('CURCOSTING') THEN
				USE IN CURCOSTING
			ENDIF

		ENDWITH
		RETURN
	ENDPROC  && MakeJobCostingSheet




	PROCEDURE MakeJobCostingSheet2
		* make sheet of job costing info for Natalie @ Insperity
		* for specific employees
		WITH THIS
			LOCAL lnMonth, lcFile, lnYear
			lnMonth = MONTH(DATE())
			lnYear = YEAR(DATE())
			lcFile = "C:\A\TOLL_JOB_COSTING_" + DTOS(DATE()) + ".XLS"

			IF USED('EEINFO') THEN
				USE IN EEINFO
			ENDIF
			IF USED('CURCOSTING') THEN
				USE IN CURCOSTING
			ENDIF

			USE F:\UTIL\TIMESTAR\DATA\EEINFO IN 0 ALIAS EEINFO

			*!*				SELECT ;
			*!*					COMPANY, ;
			*!*					NAME, ;
			*!*					INSPID, ;
			*!*					DIVISION, ;
			*!*					DEPT, ;
			*!*					BUSUNIT, ;
			*!*					WORKSITE, ;
			*!*					GLACCTNO, ;
			*!*					COLLAR, ;
			*!*					TOLLCLASS, ;
			*!*					PTODATE ;
			*!*					FROM EEINFO ;
			*!*					INTO CURSOR CURCOSTING ;
			*!*					WHERE INSPID IN ( 2385287, 2386794, 2386990 ) ;
			*!*					ORDER BY NAME

			SELECT ;
				COMPANY, ;
				NAME, ;
				INSPID, ;
				DIVISION, ;
				DEPT, ;
				BUSUNIT, ;
				WORKSITE, ;
				GLACCTNO, ;
				COLLAR, ;
				TOLLCLASS, ;
				PTODATE ;
				FROM EEINFO ;
				INTO CURSOR CURCOSTING ;
				WHERE FLAGGED ;
				ORDER BY NAME

			SELECT CURCOSTING
			COPY TO (lcFile) XL5

			.TrackProgress('Created Costing Sheet: ' + lcFile,LOGIT+SENDIT)

			IF USED('EEINFO') THEN
				USE IN EEINFO
			ENDIF
			IF USED('CURCOSTING') THEN
				USE IN CURCOSTING
			ENDIF

		ENDWITH
		RETURN
	ENDPROC  && MakeJobCostingSheet2


	PROCEDURE GetTimeDataCSV

		* load modified to support nightly month-to-date file from Insperity in .csv format

		LOCAL lnMaxRow, lcRow, lnRow, oWorkbook, oWorksheet, lcSourceFile, lnRowsProcessed, lcMode, lcBusUnit, lcWorkDate, ldWorkDate
		LOCAL lcArchivedFile, lnCSVMonth, lnCSVYear, lcPayStatsFile

		PRIVATE m.FILE_NUM, m.NAME, m.WORKDATE, m.LUNCHMINS, m.PAY_TYPE, m.TOTHOURS, m.BUSUNIT, m.DIVISION, m.DEPT, m.WORKSITE, m.GLACCTNO
		PRIVATE m.COLLAR, m.TOTDOLLARS, m.TOLLCLASS, m.COMPANY, m.INPUNCHNM, m.INPUNCHDT, m.OUTPUNCHNM, m.OUTPUNCHDT

		WITH THIS


			USE F:\UTIL\ADPREPORTS\DATA\DEPTTRANS.DBF IN 0 ALIAS DEPTTRANS

			* OPEN THE TIMEDATA INFO TABLE
			IF USED('TIMEDATA') THEN
				USE IN TIMEDATA
			ENDIF

			IF USED('CURCSVPRE') THEN
				USE IN CURCSVPRE
			ENDIF

			IF USED('CURCSV') THEN
				USE IN CURCSV
			ENDIF

			*USE F:\UTIL\TIMESTAR\TESTDATA\TTIMEDATA IN 0 ALIAS TIMEDATA EXCLUSIVE   && test data

			USE F:\UTIL\TIMESTAR\DATA\TIMEDATA IN 0 ALIAS TIMEDATA EXCLUSIVE		&& live data


			lcSourceFile   = 'F:\FTPUSERS\INSPERITY\IN\TIMEDATA\YTD_HOURS_DATA.CSV'
			lcArchivedFile = 'F:\FTPUSERS\INSPERITY\IN\TIMEDATA\ARCHIVED\YTD_HOURS_DATA_' + DTOS(DATE()) + '.CSV'

			.TrackProgress('Source File = ' + lcSourceFile, LOGIT+SENDIT)

			.TrackProgress('Archived File = ' + lcArchivedFile, LOGIT+SENDIT)

			IF NOT FILE(lcSourceFile) THEN
				.TrackProgress('=====> ERROR: Source File ' + lcSourceFile + ' was not found!', LOGIT+SENDIT)
				THROW
			ENDIF

			SET HOURS TO 24

			CREATE CURSOR CURCSVPRE ( FILE_NUM C(7), NAME C(30), WORKDATEDT T, PAY_TYPE C(20), TOTHOURS N(5,2), TOTDOLLARS Y, COMPANY C(7), BUSUNITC C(1), ;
				DIVISION C(2), DEPT C(4), WORKSITE C(3), GLACCTNO C(4), COLLAR C(1), TOLLCLASS C(30), WORKDATE D, BUSUNIT C(5), ;
				INPUNCHNM C(3), INPUNCHDT T, OUTPUNCHNM C(3), OUTPUNCHDT T )

			SET DATE YMD

			.TrackProgress('Appending from CSV file: ' + lcSourceFile, LOGIT+SENDIT+NOWAITIT)

			SELECT CURCSVPRE
			APPEND FROM (lcSourceFile) CSV

			* examine 1st record to see which month we are loading
			SELECT CURCSVPRE
			GOTO TOP
			lnCSVMonth = MONTH( CURCSVPRE.WORKDATEDT )
			lnCSVYear = YEAR( CURCSVPRE.WORKDATEDT )

			.TrackProgress('=====> Reloading Month: ' + TRANSFORM(lnCSVMonth) + '/' + TRANSFORM(lnCSVYear), LOGIT+SENDIT+NOWAITIT)

			*THROW

			* delete the existing data for current month
			SELECT TIMEDATA
			DELETE FOR (MONTH(WORKDATE) = lnCSVMonth) AND (YEAR(WORKDATE) = lnCSVYear)
			PACK

			* SORT CURCSV BY DATE WITHIN NAME
			SELECT * FROM CURCSVPRE INTO CURSOR CURCSV ORDER BY NAME, WORKDATEDT READWRITE

			IF USED('CURCSVPRE') THEN
				USE IN CURCSVPRE
			ENDIF


			SELECT CURCSV
			SCAN
				DO CASE
					CASE CURCSV.BUSUNITC = 'A'
						lcBusUnit = 'ADMIN'
					CASE CURCSV.BUSUNITC = 'F'
						lcBusUnit = 'FF'
					CASE CURCSV.BUSUNITC = 'S'
						lcBusUnit = 'SCS'
					OTHERWISE
						lcBusUnit = '???'
				ENDCASE
				*lcWorkDate = STRTRAN(ALLTRIM( CURCSV.WORKDATEC ),"-","/")
				ldWorkDate = TTOD( CURCSV.WORKDATEDT )
				REPLACE CURCSV.BUSUNIT WITH lcBusUnit, CURCSV.WORKDATE WITH ldWorkDate, CURCSV.PAY_TYPE WITH UPPER(CURCSV.PAY_TYPE)IN CURCSV

			ENDSCAN
			*!*	BROWSE
			*!*	THROW

			* append the new data for current month
			SET DATE MDY

			lnRowsProcessed = 0

			SELECT CURCSV
			SCAN

				SCATTER MEMVAR

				* make any adjustments to memvars here...
				m.COMPANY = INT(VAL(m.COMPANY))


				INSERT INTO TIMEDATA FROM MEMVAR

				lnRowsProcessed = lnRowsProcessed + 1
				WAIT WINDOW NOWAIT 'Processing Time Data Spreadsheet Row: ' + TRANSFORM(lnRowsProcessed)

			ENDSCAN

			WAIT CLEAR

			.TrackProgress("In GetTimeDataCSV, lnRowsProcessed = " + TRANSFORM(lnRowsProcessed), LOGIT+SENDIT)

			* populate calculated fields that correspond to old kronos/adp values
			SELECT TIMEDATA
			SCAN FOR (MONTH(WORKDATE) = lnCSVMonth) AND (YEAR(WORKDATE) = lnCSVYear)

				IF TIMEDATA.COMPANY = 3273300 THEN
					REPLACE TIMEDATA.ADP_COMP WITH 'E88' IN TIMEDATA && SALARIED
				ELSE
					REPLACE TIMEDATA.ADP_COMP WITH 'E87' IN TIMEDATA && HOURLY
				ENDIF

				DO CASE
					CASE TIMEDATA.BUSUNIT = 'A'
						REPLACE TIMEDATA.BUSUNIT WITH 'ADM' IN TIMEDATA
					CASE TIMEDATA.BUSUNIT = 'F'
						REPLACE TIMEDATA.BUSUNIT WITH 'FF ' IN TIMEDATA
					CASE TIMEDATA.BUSUNIT = 'S'
						REPLACE TIMEDATA.BUSUNIT WITH 'SCS' IN TIMEDATA 
					OTHERWISE
						* NOTHING
				ENDCASE

				DO CASE
					CASE INLIST(ALLTRIM(TIMEDATA.PAY_TYPE),'REGULAR','GUARANTEE','LNCHPEN')
						REPLACE TIMEDATA.REGHOURS WITH TIMEDATA.TOTHOURS IN TIMEDATA
					CASE INLIST(ALLTRIM(TIMEDATA.PAY_TYPE),'OVERTIME','RETRO-OVT','WRKDHOL')
						REPLACE TIMEDATA.OTHOURS WITH TIMEDATA.TOTHOURS IN TIMEDATA
					CASE INLIST(ALLTRIM(TIMEDATA.PAY_TYPE),'DOUBLE')
						REPLACE TIMEDATA.DTHOURS WITH TIMEDATA.TOTHOURS IN TIMEDATA
					OTHERWISE
						REPLACE TIMEDATA.CODEHOURS WITH TIMEDATA.TOTHOURS IN TIMEDATA
				ENDCASE

				* DELETE BAD DATA
				* SARA SUPERVISOR, ETC.
				IF VAL(TIMEDATA.FILE_NUM) < 10 THEN
					DELETE IN TIMEDATA
				ENDIF

				* BAD DEPT CODES
				IF VAL(TIMEDATA.DEPT) = 0 THEN
					DELETE IN TIMEDATA
				ENDIF


				*************** TAKE THIS OUT ONCE JOSH PROVIDES THE TOLLCLASS CODES **************************
				* fill in missing tollclass INFO, from DEPT lookup
				SELECT DEPTTRANS
				LOCATE FOR DEPT = TIMEDATA.DEPT
				IF FOUND() THEN
					REPLACE TIMEDATA.TOLLCLASS WITH DEPTTRANS.HCFTECODE, TIMEDATA.TCDESC WITH DEPTTRANS.HCFTEDESC IN TIMEDATA
				ENDIF
				***********************************************************************************************


			ENDSCAN

			.PopulateWageAmt()

			*SELECT TIMEDATA
			*BROWSE

			lcPayStatsFile = 'F:\UTIL\TIMESTAR\REPORTS\PAY_STATS_' + DTOS(DATE()) + '.XLS'

			IF USED('CURPAYTYPES') THEN
				USE IN CURPAYTYPES
			ENDIF

			SELECT PAY_TYPE, COUNT(*) AS COUNT ;
				FROM CURCSV INTO CURSOR CURPAYTYPES ;
				GROUP BY 1 ORDER BY 1

			SELECT CURPAYTYPES
			COPY TO (lcPayStatsFile) XL5
			*.cAttach = lcPayStatsFile
			.AddAttachment(lcPayStatsFile)



			IF USED('CURCSV') THEN
				USE IN CURCSV
			ENDIF

			IF USED('TIMEDATA') THEN
				USE IN TIMEDATA
			ENDIF

			* archive the source file
			COPY FILE (lcSourceFile) TO (lcArchivedFile)

			IF FILE(lcArchivedFile) THEN
				.TrackProgress('Source file was archived to: ' + lcArchivedFile, LOGIT+SENDIT)
				DELETE FILE (lcSourceFile)
			ELSE
				.TrackProgress('ERROR archiving Source file [' + lcSourceFile + '] to: ' + lcArchivedFile, LOGIT+SENDIT)
			ENDIF

		ENDWITH
		RETURN
	ENDPROC  && GetTimeDataCSV



	* modified for SQL 2/26/2018 MB
	PROCEDURE LoadDriverPunches
		LPARAMETERS tdWorkDate
		WITH THIS
			* Extract data from TIMEDATA for local drivers for passed date and insert into punch table.
			* this should be run right after the 7am daily automated time load

			LOCAL ldWorkDate, lcSendTo, lcFrom, lcSubject, lcCC, lcAttach, lcBodyText, lnInOutPunchDeltaSecs, lnInMinutes, lnOutMinutes
			LOCAL lcPunchMessage, lcSQLWorkdate
			*ldWorkDate = DATE() - 1
			ldWorkDate = tdWorkDate
			
			
			lcSQLWorkdate = "{" + TRANSFORM(YEAR(ldWorkDate)) + "-" + PADL(MONTH(ldWorkDate),2,'0')  + "-" + PADL(DAY(ldWorkDate),2,'0') + "}" 

			**********************************************************************************************************************************
			**********************************************************************************************************************************

			.TrackProgress('In  LoadDriverPunches(), loading punch data for ' + TRANSFORM(ldWorkDate), LOGIT+SENDIT)

			IF USED('PUNCH') THEN
				USE IN PUNCH
			ENDIF
			IF USED('EXCEPT') THEN
				USE IN EXCEPT
			ENDIF
			IF USED('DAY') THEN
				USE IN DAY
			ENDIF
			IF USED('EMPLOYEE') THEN
				USE IN EMPLOYEE
			ENDIF
			IF USED('TIMEDATA') THEN
				USE IN TIMEDATA
			ENDIF
			IF USED('CURPUNCHES') THEN
				USE IN CURPUNCHES
			ENDIF

			SET DELETED ON
			SET CENTURY ON
			SET DATE MDY
			SET HOURS TO 12

			*USE F:\WO\WODATA\PUNCH IN 0   && LIVE
			USECA("punch","wo",,,"select * from punch where zdate=" + lcSQLWorkdate)
						
			*USE F:\WO\WODATA\WOGENPK IN 0 ALIAS GENERATEPK && LIVE
			*USE F:\UTIL\TIMESTAR\TESTDATA\TESTPUNCH IN 0 ALIAS PUNCH   && TEST 

			*USE F:\WO\WODATA\EXCEPT IN 0  && not used 3/5/2018 MB
			*xsqlexec("select * from exceptx","except",,"wo")
			
			*USE F:\WO\WODATA\DAY IN 0  && not used 3/5/2018 MB
			*xsqlexec("select * from day","day",,"wo")
			
			USE F:\HR\HRDATA\EMPLOYEE IN 0
			USE F:\UTIL\TIMESTAR\DATA\TIMEDATA IN 0

			* NOTE: in Where below we are only including pay types associated with punches. So we can't ever get a record missing both in and out punches (I think).
			SELECT INT(VAL(FILE_NUM)) AS INSPID, NAME AS DRIVER, WORKDATE AS ZDATE, INPUNCHDT, OUTPUNCHDT, INPUNCHNM, OUTPUNCHNM, PAY_TYPE AS PAYTYPE, TOTHOURS AS HOURS ;
				FROM TIMEDATA ;
				INTO CURSOR CURPUNCHES ;
				WHERE WORKDATE = ldWorkDate ;
				AND DEPT = '0650' ;
				AND WORKSITE <> 'IL1' ;
				AND PAY_TYPE IN ("REGULAR","WRKDHOL","OVERTIME") ;
				ORDER BY NAME ;
				READWRITE

			IF (NOT USED('CURPUNCHES')) OR EOF('CURPUNCHES') THEN
			
				.TrackProgress('!!!!!!!! WARNING: In  LoadDriverPunches(), no time data found for ' + TRANSFORM(ldWorkDate), LOGIT+SENDIT)
				
			ELSE

*!*					SELECT CURPUNCHES
*!*					BROW

				* try to blank out the duplicate punches Timestar adds when there is a missing punch.
				* NOTE: when the system generates a punch, it uses the unrounded time of the real punch.
				* A rounded punch is one where the minutes = 00, 15, 30, or 45.
				* So if one punch is rounded and one is not rounded, the real punch is the rounded one, and we want to blank the unrounded one.
				* If both are rounded (i.e., someone actually punched on a 15 minute boundary) we're stuck again not knowing which is real.
				SELECT CURPUNCHES
				SCAN FOR (ZDATE = ldWorkDate) AND (NOT EMPTYNUL(CURPUNCHES.INPUNCHDT)) AND (NOT EMPTYNUL(CURPUNCHES.OUTPUNCHDT))
					lnInOutPunchDeltaSecs = CURPUNCHES.OUTPUNCHDT - CURPUNCHES.INPUNCHDT
					* NOTE: in some cases, such as when there is an in punch but no out punch in the time card, TimeStar enters an out punch = to the in punch.
					* (the Vice Versa logic also holds)
					* In these cases the punch in seems to be rounded, but the added punch out is not rounded.
					* in the timecard the added punch is in red, indicating it needs manual correction. But here we don't know that.
					* so if we get an in punch within 7 minutes of an out punch, look at the INPUNCHNM and OUTPUNCHNM fields to determine which punch to blank out.
					IF ABS(lnInOutPunchDeltaSecs) <= (7*60) THEN
						.TrackProgress('WARNING: Difference between In and Out punches = ' + TRANSFORM(lnInOutPunchDeltaSecs) + ' seconds!', LOGIT+SENDIT)						
						DO CASE
							CASE EMPTYNUL(CURPUNCHES.INPUNCHNM) AND (NOT EMPTYNUL(CURPUNCHES.OUTPUNCHNM))
								* the OUT punch was real; IN punch was generated by the system; blank the IN punch
								*.TrackProgress('WARNING: Blanking the In Punch for ' + ALLTRIM(CURPUNCHES.DRIVER) + ': Out = ' + TRANSFORM(CURPUNCHES.OUTPUNCHDT), LOGIT+SENDIT)
								lcPunchMessage = 'WARNING: Blanking the In Punch for ' + ALLTRIM(CURPUNCHES.DRIVER) + ': Out = ' + TRANSFORM(CURPUNCHES.OUTPUNCHDT)
								.TrackProgress(lcPunchMessage, LOGIT+SENDIT)
								.cPunchMessages = .cPunchMessages + lcPunchMessage + CRLF
								REPLACE INPUNCHDT WITH {} IN CURPUNCHES
							CASE EMPTYNUL(CURPUNCHES.OUTPUNCHNM) AND (NOT EMPTYNUL(CURPUNCHES.INPUNCHNM))
								* the IN punch was real; OUT punch was generated by the system; blank the OUT punch
								*.TrackProgress('WARNING: Blanking the Out Punch for ' + ALLTRIM(CURPUNCHES.DRIVER) + ': In = ' + TRANSFORM(CURPUNCHES.INPUNCHDT), LOGIT+SENDIT)
								lcPunchMessage = 'WARNING: Blanking the Out Punch for ' + ALLTRIM(CURPUNCHES.DRIVER) + ': In = ' + TRANSFORM(CURPUNCHES.INPUNCHDT)
								.TrackProgress(lcPunchMessage, LOGIT+SENDIT)
								.cPunchMessages = .cPunchMessages + lcPunchMessage + CRLF
								REPLACE OUTPUNCHDT WITH {} IN CURPUNCHES
							OTHERWISE
								* TRY comparison of the punch minutes, using 'rounded' logic.
								lnInMinutes = MINUTE(CURPUNCHES.INPUNCHDT)
								lnOutMinutes = MINUTE(CURPUNCHES.OUTPUNCHDT)
								DO CASE
									CASE INLIST(lnInMinutes,0,15,30,45) AND (NOT INLIST(lnOutMinutes,0,15,30,45))
										* the IN punch was real; OUT punch was generated by the system; blank the OUT punch
										*.TrackProgress('WARNING: Blanking the Out Punch for ' + ALLTRIM(CURPUNCHES.DRIVER) + ': In = ' + TRANSFORM(CURPUNCHES.INPUNCHDT), LOGIT+SENDIT)
										lcPunchMessage = 'WARNING: Blanking the Out Punch for ' + ALLTRIM(CURPUNCHES.DRIVER) + ': In = ' + TRANSFORM(CURPUNCHES.INPUNCHDT)
										.TrackProgress(lcPunchMessage, LOGIT+SENDIT)
										.cPunchMessages = .cPunchMessages + lcPunchMessage + CRLF
										REPLACE OUTPUNCHDT WITH {} IN CURPUNCHES
									CASE INLIST(lnOutMinutes,0,15,30,45) AND (NOT INLIST(lnInMinutes,0,15,30,45))
										* the OUT punch was real; IN punch was generated by the system; blank the IN punch
										*.TrackProgress('WARNING: Blanking the In Punch for ' + ALLTRIM(CURPUNCHES.DRIVER) + ': Out = ' + TRANSFORM(CURPUNCHES.OUTPUNCHDT), LOGIT+SENDIT)
										lcPunchMessage = 'WARNING: Blanking the In Punch for ' + ALLTRIM(CURPUNCHES.DRIVER) + ': Out = ' + TRANSFORM(CURPUNCHES.OUTPUNCHDT)
										.TrackProgress(lcPunchMessage, LOGIT+SENDIT)
										.cPunchMessages = .cPunchMessages + lcPunchMessage + CRLF
										REPLACE INPUNCHDT WITH {} IN CURPUNCHES
									OTHERWISE
										.TrackProgress("ERROR: Can't determine which punch to blank for " + ALLTRIM(CURPUNCHES.DRIVER) + ": In = " + TRANSFORM(CURPUNCHES.INPUNCHDT) + ": Out = " + TRANSFORM(CURPUNCHES.OUTPUNCHDT), LOGIT+SENDIT)
										* 07/25/2016: after discussing with Mike Drew; since this is mostly a problem for west coast night drivers, we will assume the in punch is
										* correct, and blank the out punch, which will be filled with IN + 12 hours later in this process.
										* this may not always be correct, but it will be better than not having the punch added to the Punches tables, the way it was before this change.
										*.TrackProgress('WARNING: Blanking the Out Punch for ' + ALLTRIM(CURPUNCHES.DRIVER) + ': In = ' + TRANSFORM(CURPUNCHES.INPUNCHDT), LOGIT+SENDIT)
										lcPunchMessage = "ERROR: Can't determine which punch to blank for " + ALLTRIM(CURPUNCHES.DRIVER) + ": In = " + TRANSFORM(CURPUNCHES.INPUNCHDT) + ": Out = " + TRANSFORM(CURPUNCHES.OUTPUNCHDT)
										lcPunchMessage = lcPunchMessage + CRLF + 'WARNING: Blanking the Out Punch for ' + ALLTRIM(CURPUNCHES.DRIVER) + ': In = ' + TRANSFORM(CURPUNCHES.INPUNCHDT)
										.TrackProgress(lcPunchMessage, LOGIT+SENDIT)
										.cPunchMessages = .cPunchMessages + lcPunchMessage + CRLF
										REPLACE OUTPUNCHDT WITH {} IN CURPUNCHES
								ENDCASE
						ENDCASE
					ENDIF
				ENDSCAN

				SELECT CURPUNCHES
				LOCATE				

				SELECT PUNCH
				DELETE FOR ZDATE=ldWorkDate IN PUNCH

				SELECT CURPUNCHES				
				SCAN FOR ZDATE=ldWorkDate
				
					SCATTER MEMVAR FIELDS DRIVER, ZDATE, INSPID
					STORE {} TO m.punchin, m.punchout

					SELECT EMPLOYEE
					LOCATE FOR INSPID=m.INSPID
					IF FOUND()
						m.driver=EMPLOYEE.EMPLOYEE
					ELSE
						LOOP
					ENDIF

					SELECT CURPUNCHES

					m.punchin = CURPUNCHES.INPUNCHDT

					*!*					IF !EMPTY(CURPUNCHES.INPUNCHDT)
					*!*						m.punchin=CTOT(SUBSTR(CURPUNCHES.INPUNCHDT,2,17))
					*!*						IF "P"$CURPUNCHES.INPUNCHDT AND SUBSTR(TTOC(m.punchin),12,2)#"12"
					*!*							m.punchin=m.punchin+43200
					*!*						ENDIF
					*!*					ENDIF

					m.punchout = CURPUNCHES.OUTPUNCHDT

					*!*					IF !EMPTY(CURPUNCHES.OUTPUNCHDT)
					*!*						m.punchout=CTOT(SUBSTR(CURPUNCHES.OUTPUNCHDT,2,17))
					*!*						IF "P"CURPUNCHES.OUTPUNCHDT AND SUBSTR(TTOC(m.punchout),12,2)#"12"
					*!*							m.punchout=m.punchout+43200
					*!*						ENDIF
					*!*					ENDIF

* 2/27/2018 MB
*!*						IF !SEEK(DTOS(m.ZDATE)+m.driver,"punch","datedriver")
*!*							*INSERT INTO PUNCH FROM MEMVAR
*!*						ELSE
*!*							IF !EMPTY(m.punchin)
*!*								REPLACE punchin WITH m.punchin IN PUNCH
*!*							ENDIF
*!*							IF !EMPTY(m.punchout)
*!*								REPLACE punchout WITH m.punchout IN PUNCH
*!*							ENDIF
*!*						ENDIF
					
					SELECT PUNCH
					LOCATE FOR (ZDATE=m.ZDATE) AND (DRIVER=m.driver)
					IF FOUND() THEN
						**IF !EMPTY(m.punchin)
						IF !EMPTYNUL(m.punchin)
							REPLACE punchin WITH m.punchin IN PUNCH
						ENDIF
						*IF !EMPTY(m.punchout)
						IF !EMPTYNUL(m.punchout)
							REPLACE punchout WITH m.punchout IN PUNCH
						ENDIF
					ELSE
						INSERTINTO("PUNCH","WO")
					ENDIF

					*IF INLIST(ALLTRIM(CURPUNCHES.paytype),"REGULAR","WRKDHOL","OVERTIME") THEN
					REPLACE HOURS WITH PUNCH.HOURS+CURPUNCHES.HOURS IN PUNCH
					*ENDIF

					SELECT EMPLOYEE
					LOCATE FOR INSPID=PUNCH.INSPID
					REPLACE office WITH IIF(EMPLOYEE.COMPANY="T","N","C") IN PUNCH

				ENDSCAN

				SELECT PUNCH
				*SCAN FOR ZDATE=ldWorkDate AND PUNCH.punchin=PUNCH.punchout AND !EMPTY(PUNCH.punchin)
				SCAN FOR (ZDATE = ldWorkDate) AND NOT EMPTYNUL(PUNCH.punchin)
					lnInOutPunchDeltaSecs = PUNCH.punchout - PUNCH.punchin
					* NOTE: in some cases, such as when there is an in punch but no out punch in the time card, TimeStar enters an out punch = to the in punch.
					* In these cases the punch in seems to be rounded, but the added punch out is not rounded.
					* in the timecard the added punch is in red, indicating it needs manual corection. But here we don't know that.
					* so if we get an in punch within 7 minutes of an out punch, assume the in punch was correct and blank out the out punch; later on it will be filled with (in punch +12 hours)
					IF ABS(lnInOutPunchDeltaSecs) <= (7*60) THEN
						.TrackProgress('WARNING: Difference between In and Out punches = ' + TRANSFORM(lnInOutPunchDeltaSecs) + ' seconds!', LOGIT+SENDIT)
						.TrackProgress('WARNING: Blanking the Out Punch for ' + PUNCH.DRIVER + ': ' + TRANSFORM(PUNCH.punchin), LOGIT+SENDIT)
						REPLACE punchout WITH {} IN PUNCH
					ENDIF
				ENDSCAN


				SELECT PUNCH
				SCAN FOR ZDATE=ldWorkDate AND (EMPTYNUL(punchin) OR EMPTYNUL(punchout))
					REPLACE HOURS WITH IIF(office="N",8,10) IN PUNCH

					DO CASE
						CASE EMPTYNUL(PUNCH.punchin) AND EMPTYNUL(PUNCH.punchout)

							*  NOTE: I don't think this case is possible, now that we are getting the data from Timestar.

							SELECT EMPLOYEE
							LOCATE FOR INSPID=PUNCH.INSPID

							IF FOUND() AND EMPLOYEE.daynight="2"
								REPLACE punchin WITH CTOT(DTOC(ZDATE)+" 12:00:00"), punchout WITH CTOT(DTOC(ZDATE+1)+" 06:00:00") IN PUNCH
							ELSE
								REPLACE punchin WITH CTOT(DTOC(ZDATE)+" 05:00:00"), punchout WITH CTOT(DTOC(ZDATE)+" 08:00:00") IN PUNCH
							ENDIF

						CASE EMPTYNUL(PUNCH.punchin)
							lcPunchMessage = '======>: Creating In Punch = Out Punch -12 Hours for ' + ALLTRIM(PUNCH.DRIVER) + ': Out was ' + TRANSFORM(PUNCH.PUNCHOUT)
							.TrackProgress(lcPunchMessage, LOGIT+SENDIT)
							.cPunchMessages = .cPunchMessages + lcPunchMessage + CRLF
							REPLACE punchin WITH punchout-43200, nopunchin WITH .T. IN PUNCH

						CASE EMPTYNUL(PUNCH.punchout)
							lcPunchMessage = '======>: Creating Out Punch = In Punch +12 Hours for ' + ALLTRIM(PUNCH.DRIVER) + ': In was ' + TRANSFORM(PUNCH.PUNCHIN)
							.TrackProgress(lcPunchMessage, LOGIT+SENDIT)
							.cPunchMessages = .cPunchMessages + lcPunchMessage + CRLF
							REPLACE punchout WITH punchin+43200, nopunchout WITH .T. IN PUNCH
					ENDCASE
				ENDSCAN
				
*!*					SELECT PUNCH
*!*					BROWSE
*!*					THROW
				
				* tableupate SQL
				IF tu("PUNCH") THEN
					.TrackProgress('SUCCESS: TU(PUNCH) returned .T.!',LOGIT+SENDIT)
				ELSE
					* ERROR on table update
					.TrackProgress('ERROR: TU(PUNCH) returned .F.!',LOGIT+SENDIT)
					=AERROR(UPCERR)
					IF TYPE("UPCERR")#"U" THEN 
						.TrackProgress('Error Description = ' + UPCERR[1,2],LOGIT+SENDIT)
					ENDIF
				ENDIF


				***  send an email saying punches have been loaded **********
				lcSendTo = 'mark.bennett@tollgroup.com'  &&, mike.drew@tollgroup.com'
				lcFrom = 'mark.bennett@tollgroup.com'
				lcSubject = 'Driver punch table has been loaded for ' + TRANSFORM(ldWorkDate)
				lcCC = ''
				lcBodyText =  .cPunchMessages  &&'' && 'You can now Add Drivers for ' + TRANSFORM(ldWorkDate) + ' on the Driver Moves screen. Make sure you have closed out that date first!'
				
				* attach spreadsheet of punches loaded
				lcAttach = 'F:\UTIL\INSPERITY\REPORTS\PUNCHES_LOADED_' + DTOS(DATE())+ '.XLS'
				SELECT * FROM PUNCH INTO CURSOR CURRESULTS WHERE ZDATE = ldWorkDate ORDER BY DRIVER, ZDATE
				SELECT CURRESULTS
				COPY TO (lcAttach) XL5
				
				DO FORM dartmail2 WITH lcSendTo,lcFrom,lcSubject,lcCC,lcAttach,lcBodyText,"A"

			ENDIF  &&  (NOT USED('CURPUNCHES')) OR EOF('CURPUNCHES')

			**********************************************************************************************************************************
			**********************************************************************************************************************************

			IF USED('PUNCH') THEN
				USE IN PUNCH
			ENDIF
			IF USED('EXCEPT') THEN
				USE IN EXCEPT
			ENDIF
			IF USED('DAY') THEN
				USE IN DAY
			ENDIF
			IF USED('EMPLOYEE') THEN
				USE IN EMPLOYEE
			ENDIF
			IF USED('TIMEDATA') THEN
				USE IN TIMEDATA
			ENDIF
			IF USED('CURPUNCHES') THEN
				USE IN CURPUNCHES
			ENDIF


		ENDWITH
		RETURN
	ENDPROC  && LoadDriverPunches



	PROCEDURE GetTimeDataCSV2

		* load modified to support nightly month-to-date file from Insperity in .csv format
		* Modified 6/24/14 to support 4 new punch info fields for Darren (m.INPUNCHNM, m.INPUNCHDT, m.OUTPUNCHNM, m.OUTPUNCHDT).
		
		* Modified 3/17/2015 to load an xls file, not a csv file, because they can no longer send csv files after their data center move.

		LOCAL lnMaxRow, lcRow, lnRow, oWorkbook, oWorksheet, lcSourceFile, lnRowsProcessed, lcMode, lcBusUnit, lcWorkDate, ldWorkDate
		LOCAL lcArchivedFile, lnCSVMonth, lnCSVYear, lcPayStatsFile, llTestMode

		PRIVATE m.FILE_NUM, m.NAME, m.WORKDATE, m.LUNCHMINS, m.PAY_TYPE, m.TOTHOURS, m.BUSUNIT, m.DIVISION, m.DEPT, m.WORKSITE, m.GLACCTNO
		PRIVATE m.COLLAR, m.TOTDOLLARS, m.TOLLCLASS, m.COMPANY, m.INPUNCHNM, m.INPUNCHDT, m.OUTPUNCHNM, m.OUTPUNCHDT

		WITH THIS

			llTestMode = .F.
			*llTestMode = .T.

			USE F:\UTIL\TIMESTAR\DATA\EEINFO IN 0 ALIAS EEINFO
			
			USE F:\UTIL\ADPREPORTS\DATA\DEPTTRANS.DBF IN 0 ALIAS DEPTTRANS

			IF USED('TIMEDATA') THEN
				USE IN TIMEDATA
			ENDIF

			IF USED('CURCSVPRE') THEN
				USE IN CURCSVPRE
			ENDIF

			IF USED('CURCSV') THEN
				USE IN CURCSV
			ENDIF

			IF llTestMode THEN

				USE F:\UTIL\TIMESTAR\TESTDATA\TESTTIMEDATA IN 0 ALIAS TIMEDATA EXCLUSIVE   && test data

				*lcSourceFile   = 'F:\UTIL\TIMESTAR\reports\TIMEDATA\YTD_HOURS_DATA.CSV'

				*lcArchivedFile = 'F:\UTIL\TIMESTAR\reports\TIMEDATA\ARCHIVED\YTD_HOURS_DATA_' + DTOS(DATE()) + '.CSV'
				
				lcSourceFile   = 'F:\FTPUSERS\INSPERITY\IN\TIMEDATA\YTD_HOURS_DATA.XLS'

				lcArchivedFile = 'F:\FTPUSERS\INSPERITY\IN\TIMEDATA\ARCHIVED\YTD_HOURS_DATA_' + STRTRAN(STRTRAN(STRTRAN(TTOC(DATETIME()),"/","_")," ","_"),":","_") + '.XLS'

			ELSE
				USE F:\UTIL\TIMESTAR\DATA\TIMEDATA IN 0 ALIAS TIMEDATA EXCLUSIVE		&& live data

				*lcSourceFile   = 'F:\FTPUSERS\INSPERITY\IN\TIMEDATA\YTD_HOURS_DATA.CSV'

				*lcArchivedFile = 'F:\FTPUSERS\INSPERITY\IN\TIMEDATA\ARCHIVED\YTD_HOURS_DATA_' + DTOS(DATE()) + '.CSV'

				lcSourceFile   = 'F:\FTPUSERS\INSPERITY\IN\TIMEDATA\YTD_HOURS_DATA.XLS'

				lcArchivedFile = 'F:\FTPUSERS\INSPERITY\IN\TIMEDATA\ARCHIVED\YTD_HOURS_DATA_' + STRTRAN(STRTRAN(STRTRAN(TTOC(DATETIME()),"/","_")," ","_"),":","_") + '.XLS'

			ENDIF

			.TrackProgress('Source File = ' + lcSourceFile, LOGIT+SENDIT)

			.TrackProgress('Archived File = ' + lcArchivedFile, LOGIT+SENDIT)

			IF NOT FILE(lcSourceFile) THEN
				.TrackProgress('=====> ERROR: Source File ' + lcSourceFile + ' was not found!', LOGIT+SENDIT)
				THROW
			ENDIF


			* archive the source file
			COPY FILE (lcSourceFile) TO (lcArchivedFile)


*!*				CREATE CURSOR CURCSVPRE ( FILE_NUM C(7), NAME C(30), WORKDATE T, PAY_TYPE C(20), INPUNCHNM C(3), INPUNCHDT T, OUTPUNCHNM C(3), OUTPUNCHDT T, ;
*!*					TOTHOURS N(5,2), TOTDOLLARS Y, COMPANY C(7), BUSUNITC C(1), DIVISION C(2), DEPT C(4), WORKSITE C(3), GLACCTNO C(4), COLLAR C(1), TOLLCLASS C(30), BUSUNIT C(5) )

			CREATE CURSOR CURCSVPRE ( FILE_NUM C(7), NAME C(30), WORKDATE D, PAY_TYPE C(20), INPUNCHNM C(3), INPUNCHDT T, OUTPUNCHNM C(3), OUTPUNCHDT T, ;
				TOTHOURS N(5,2), TOTDOLLARS Y, COMPANY C(7), BUSUNITC C(30), DIVISION C(2), DEPT C(4), WORKSITE C(3), GLACCTNO C(4), COLLAR C(1), TOLLCLASS C(30), BUSUNIT C(5) )

			*SET DATE YMD

			.TrackProgress('Appending from XLS file: ' + lcSourceFile, LOGIT+SENDIT+NOWAITIT)
			
			SET CENTURY ON
			SET DATE MDY

*!*				SELECT CURCSVPRE
*!*				*APPEND FROM (lcSourceFile) CSV
*!*				APPEND FROM (lcSourceFile) XL8

************************************************
			oWorkbook = .oExcel.workbooks.OPEN(lcSourceFile)

			oWorksheet = oWorkbook.Worksheets[1]
			
			lnRowsProcessed = 0

			lnMaxRow = 30000

			FOR lnRow = 2 TO lnMaxRow

				WAIT WINDOW NOWAIT 'Processing Time Data Spreadsheet Row: ' + TRANSFORM(lnRow)
				
			*.TrackProgress("In GetTimeDataCSV2, Time Data Spreadsheet Row = " + TRANSFORM(lnRow), LOGIT+SENDIT)

				lcRow = ALLTRIM(STR(lnRow))
				
				m.FILE_NUM = NVL(oWorksheet.RANGE("A"+lcRow).VALUE,'')
				
				IF EMPTY(m.FILE_NUM) THEN
					EXIT FOR
				ENDIF

				m.NAME = NVL(oWorksheet.RANGE("B"+lcRow).VALUE,'')
				m.WORKDATE = TTOD(NVL(oWorksheet.RANGE("C"+lcRow).VALUE,CTOT('')))
				m.PAY_TYPE = NVL(oWorksheet.RANGE("D"+lcRow).VALUE,'')
				m.INPUNCHNM = NVL(oWorksheet.RANGE("E"+lcRow).VALUE,'')

				m.INPUNCHDT1 = NVL(oWorksheet.RANGE("F"+lcRow).VALUE,'')
				m.INPUNCHDT2 = STRTRAN(m.INPUNCHDT1,'   ',' ')  && REMOVE  some extra blanks so this can be converted to a date-time expression
				m.INPUNCHDT = CTOT(m.INPUNCHDT2)
				
				m.OUTPUNCHNM = NVL(oWorksheet.RANGE("G"+lcRow).VALUE,'')
				
				m.OUTPUNCHDT1 = NVL(oWorksheet.RANGE("H"+lcRow).VALUE,'')
				m.OUTPUNCHDT2 = STRTRAN(m.OUTPUNCHDT1,'   ',' ')  && REMOVE  some extra blanks so this can be converted to a date-time expression
				m.OUTPUNCHDT = CTOT(m.OUTPUNCHDT2)
				
*!*					m.OUTPUNCHDT = NVL(STRTRAN(oWorksheet.RANGE("H"+lcRow).VALUE,'   ',' '),CTOT(''))
				
				m.TOTHOURS = NVL(oWorksheet.RANGE("I"+lcRow).VALUE,000.00)
				m.TOTDOLLARS = NVL(oWorksheet.RANGE("J"+lcRow).VALUE,000.00)
				m.COMPANY = NVL(oWorksheet.RANGE("K"+lcRow).VALUE,'')
				m.BUSUNITC = NVL(oWorksheet.RANGE("L"+lcRow).VALUE,'')
				m.DIVISION = NVL(oWorksheet.RANGE("M"+lcRow).VALUE,'')
				m.DEPT = NVL(oWorksheet.RANGE("N"+lcRow).VALUE,'')
				m.WORKSITE = NVL(oWorksheet.RANGE("O"+lcRow).VALUE,'')
				m.GLACCTNO = NVL(oWorksheet.RANGE("P"+lcRow).VALUE,'')
				m.COLLAR = NVL(oWorksheet.RANGE("Q"+lcRow).VALUE,'')
				m.TOLLCLASS = NVL(oWorksheet.RANGE("R"+lcRow).VALUE,'')
				
		*SET STEP ON 

				INSERT INTO CURCSVPRE FROM MEMVAR

				lnRowsProcessed = lnRowsProcessed + 1

			ENDFOR

			oWorkbook.CLOSE()
			
			
			
			* some org level fields may be missing in what was loaded - try to populate them from what's in the employee table
			SELECT CURCSVPRE
			SCAN FOR EMPTY(DEPT) 
				SELECT EEINFO
				LOCATE FOR INSPID = INT(VAL(CURCSVPRE.FILE_NUM))
				IF FOUND() THEN
					REPLACE ;
					CURCSVPRE.COMPANY   WITH ALLTRIM(TRANSFORM(EEINFO.COMPANY)), ;
					CURCSVPRE.BUSUNITC  WITH EEINFO.BUSUNIT, ;
					CURCSVPRE.DIVISION  WITH EEINFO.DIVISION, ;
					CURCSVPRE.DEPT      WITH EEINFO.DEPT, ;
					CURCSVPRE.WORKSITE  WITH EEINFO.WORKSITE, ;
					CURCSVPRE.GLACCTNO  WITH EEINFO.GLACCTNO, ;
					CURCSVPRE.COLLAR    WITH EEINFO.COLLAR, ;
					CURCSVPRE.TOLLCLASS WITH EEINFO.TCDESC ;
					IN CURCSVPRE
				ENDIF
			ENDSCAN

			IF USED('EEINFO') THEN
				USE IN EEINFO
			ENDIF
			
*!*	SELECT CURCSVPRE
*!*	BROW
*!*	THROW
			
			
*!*				SELECT GPAYALLOW
*!*				BROWSE

			.TrackProgress("In GetTimeDataCSV2, lnRowsImported = " + TRANSFORM(lnRowsProcessed), LOGIT+SENDIT)
************************************************




*!*	SELECT CURCSVPRE
*!*			BROWSE
*!*			THROW

			* examine 1st record to see which month we are loading
			SELECT CURCSVPRE
			GOTO TOP

			lnCSVMonth = MONTH( CURCSVPRE.WORKDATE )
			lnCSVYear = YEAR( CURCSVPRE.WORKDATE )

			.TrackProgress('=====> Reloading Month: ' + TRANSFORM(lnCSVMonth) + '/' + TRANSFORM(lnCSVYear), LOGIT+SENDIT+NOWAITIT)


			* delete the existing data for current month
			SELECT TIMEDATA
			DELETE FOR (MONTH(WORKDATE) = lnCSVMonth) AND (YEAR(WORKDATE) = lnCSVYear)
			PACK

			* SORT CURCSV BY DATE WITHIN NAME
			SELECT * FROM CURCSVPRE INTO CURSOR CURCSV ORDER BY NAME, WORKDATE READWRITE

			IF USED('CURCSVPRE') THEN
				USE IN CURCSVPRE
			ENDIF


			SELECT CURCSV
			SCAN
				DO CASE
					CASE UPPER(ALLTRIM(CURCSV.BUSUNITC)) = 'ADMIN'
						lcBusUnit = 'ADM'
					CASE UPPER(ALLTRIM(CURCSV.BUSUNITC)) = 'FREIGHT FORWARDING'
						lcBusUnit = 'FF'
					CASE UPPER(ALLTRIM(CURCSV.BUSUNITC)) = 'SUPPLY CHAIN SERVICES'
						lcBusUnit = 'SCS'
					CASE UPPER(ALLTRIM(CURCSV.BUSUNITC)) = 'GLOBAL LOGISTICS'
						lcBusUnit = 'GL'
					CASE UPPER(ALLTRIM(CURCSV.BUSUNITC)) = 'GLOBAL FORWARDING'
						lcBusUnit = 'GF'
					CASE UPPER(ALLTRIM(CURCSV.BUSUNITC)) = 'SHARED SERVICES'
						lcBusUnit = 'SS'
					OTHERWISE
						lcBusUnit = '???'
				ENDCASE

				REPLACE CURCSV.BUSUNIT WITH lcBusUnit, CURCSV.PAY_TYPE WITH UPPER(CURCSV.PAY_TYPE)IN CURCSV

			ENDSCAN

			* append the new data for current month
			SET DATE MDY

			lnRowsProcessed = 0

			SELECT CURCSV
			SCAN

				SCATTER MEMVAR

				* make any adjustments to memvars here...
				m.COMPANY = INT(VAL(m.COMPANY))

				INSERT INTO TIMEDATA FROM MEMVAR

				lnRowsProcessed = lnRowsProcessed + 1
				WAIT WINDOW NOWAIT 'Processing Time Data Spreadsheet Row: ' + TRANSFORM(lnRowsProcessed)

			ENDSCAN

			WAIT CLEAR

			.TrackProgress("In GetTimeDataCSV, lnRowsProcessed = " + TRANSFORM(lnRowsProcessed), LOGIT+SENDIT)

			* populate calculated fields that correspond to old kronos/adp values
			SELECT TIMEDATA
			SCAN FOR (MONTH(WORKDATE) = lnCSVMonth) AND (YEAR(WORKDATE) = lnCSVYear)

				IF TIMEDATA.COMPANY = 3273300 THEN
					REPLACE TIMEDATA.ADP_COMP WITH 'E88' IN TIMEDATA && SALARIED
				ELSE
					REPLACE TIMEDATA.ADP_COMP WITH 'E87' IN TIMEDATA && HOURLY
				ENDIF

*!*					DO CASE
*!*						CASE TIMEDATA.BUSUNIT = 'A'
*!*							REPLACE TIMEDATA.BUSUNIT WITH 'ADM' IN TIMEDATA
*!*						CASE TIMEDATA.BUSUNIT = 'F'
*!*							REPLACE TIMEDATA.BUSUNIT WITH 'FF ' IN TIMEDATA
*!*						CASE TIMEDATA.BUSUNIT = 'S'
*!*							REPLACE TIMEDATA.BUSUNIT WITH 'SCS' IN TIMEDATA
*!*						OTHERWISE
*!*							* NOTHING
*!*					ENDCASE

				DO CASE
					CASE INLIST(ALLTRIM(TIMEDATA.PAY_TYPE),'REGULAR','GUARANTEE','LNCHPEN')
						REPLACE TIMEDATA.REGHOURS WITH TIMEDATA.TOTHOURS IN TIMEDATA
					CASE INLIST(ALLTRIM(TIMEDATA.PAY_TYPE),'OVERTIME','RETRO-OVT','WRKDHOL')
						REPLACE TIMEDATA.OTHOURS WITH TIMEDATA.TOTHOURS IN TIMEDATA
					CASE INLIST(ALLTRIM(TIMEDATA.PAY_TYPE),'DOUBLE')
						REPLACE TIMEDATA.DTHOURS WITH TIMEDATA.TOTHOURS IN TIMEDATA
					OTHERWISE
						REPLACE TIMEDATA.CODEHOURS WITH TIMEDATA.TOTHOURS IN TIMEDATA
				ENDCASE

				* DELETE BAD DATA
				* SARA SUPERVISOR, ETC.
				IF VAL(TIMEDATA.FILE_NUM) < 10 THEN
					DELETE IN TIMEDATA
				ELSE
					* CHECK FOR BAD DEPT CODES
					IF VAL(TIMEDATA.DEPT) = 0 THEN
						* SOME DRIVER PUNCHES THAT WE WANT ARE COMING IN WITHOUT DEPT INFO, SO CHANGE THIS TO JUST LOG THE ERROR 1/20/15 MB.
						*DELETE IN TIMEDATA
						.TrackProgress('WARNING: missing DEPT for: ' + ALLTRIM(TIMEDATA.NAME) + ',    WORKDATE = ' + TRANSFORM(TIMEDATA.WORKDATE), LOGIT+SENDIT+NOWAITIT)
					ENDIF
				ENDIF


				* fill in missing tollclass INFO, from DEPT lookup
				SELECT DEPTTRANS
				LOCATE FOR DEPT = TIMEDATA.DEPT
				IF FOUND() THEN
					REPLACE TIMEDATA.TOLLCLASS WITH DEPTTRANS.HCFTECODE, TIMEDATA.TCDESC WITH DEPTTRANS.HCFTEDESC IN TIMEDATA
				ENDIF


			ENDSCAN

			.PopulateWageAmt()

			*SELECT TIMEDATA
			*BROWSE

			lcPayStatsFile = 'F:\UTIL\TIMESTAR\REPORTS\PAY_STATS_' + DTOS(DATE()) + '.XLS'

			IF USED('CURPAYTYPES') THEN
				USE IN CURPAYTYPES
			ENDIF

			SELECT PAY_TYPE, COUNT(*) AS COUNT ;
				FROM CURCSV INTO CURSOR CURPAYTYPES ;
				GROUP BY 1 ORDER BY 1

			SELECT CURPAYTYPES
			COPY TO (lcPayStatsFile) XL5
			*.cAttach = lcPayStatsFile
			.AddAttachment(lcPayStatsFile)


			IF USED('CURCSV') THEN
				USE IN CURCSV
			ENDIF

			IF USED('TIMEDATA') THEN
				USE IN TIMEDATA
			ENDIF

*!*				* archive the source file
*!*				COPY FILE (lcSourceFile) TO (lcArchivedFile)

			IF FILE(lcArchivedFile) THEN
				.TrackProgress('Source file was archived to: ' + lcArchivedFile, LOGIT+SENDIT)
				DELETE FILE (lcSourceFile)
			ELSE
				.TrackProgress('ERROR archiving Source file [' + lcSourceFile + '] to: ' + lcArchivedFile, LOGIT+SENDIT)
			ENDIF

		ENDWITH
		RETURN
	ENDPROC  && GetTimeDataCSV2



	PROCEDURE GetTimeDataXLS

		* this loads the 'data export - payroll' format from TimeStar report generator
		* IF YOU LOADMORE THAN ONE MONTH THIS WAY, BE SURE TO DELETE WHAT YOU WANT TO REPLACE BEFORE LOADING!

		LOCAL lnMaxRow, lcRow, lnRow, oWorkbook, oWorksheet, lcSourceFile, lnRowsProcessed, lcMode, ldWorkDate, lnCSVMonth, lnCSVYear

		PRIVATE m.FILE_NUM, m.NAME, m.WORKDATE, m.LUNCHMINS, m.PAY_TYPE, m.TOTHOURS, m.BUSUNIT, m.DIVISION, m.DEPT, m.WORKSITE, m.GLACCTNO
		PRIVATE m.COLLAR, m.INPUNCHNM, m.INPUNCHDT, m.OUTPUNCHNM, m.OUTPUNCHDT

		WITH THIS

			USE F:\UTIL\ADPREPORTS\DATA\DEPTTRANS.DBF IN 0 ALIAS DEPTTRANS

			IF .lAppendTimeData THEN
				lcMode = 'APPEND TO EXISTING'
			ELSE
				lcMode = 'PURGE ALL EXISTING'
			ENDIF
			.TrackProgress('TIMEDATA LOAD MODE = ' + lcMode, LOGIT+SENDIT)

			* OPEN THE TIMEDATA INFO TABLE
			IF USED('TIMEDATA') THEN
				USE IN TIMEDATA
			ENDIF

			USE F:\UTIL\TIMESTAR\DATA\TIMEDATA IN 0 ALIAS TIMEDATA EXCLUSIVE
			*USE F:\UTIL\TIMESTAR\TESTDATA\TESTTIMEDATA IN 0 ALIAS TIMEDATA EXCLUSIVE   && test data

			*!*				IF .lAppendTimeData THEN
			*!*					* nothing
			*!*				ELSE
			*!*					* ZAP TABLE
			*!*					SELECT TIMEDATA
			*!*					DELETE FOR YEAR(WORKDATE) = 2014
			*!*					PACK
			*!*				ENDIF

			*lcSourceFile = 'F:\UTIL\TIMESTAR\REPORTS\TIMEDATA\DATA EXPORT - PAYROLL.XLS'
			lcSourceFile = GETFILE('xls','Get a file, mode = ' + lcMode)

			.TrackProgress('Source File = ' + lcSourceFile, LOGIT+SENDIT)

			oWorkbook = .oExcel.workbooks.OPEN(lcSourceFile)

			oWorksheet = oWorkbook.Worksheets[1]


			********************************************************************************************************
			* get month/year so that we can delete it from timedata before loading
			ldWorkDate = oWorksheet.RANGE("C2").VALUE

			lnCSVMonth = MONTH( ldWorkDate )
			lnCSVYear = YEAR( ldWorkDate )

			.TrackProgress('=====> Reloading Month: ' + TRANSFORM(lnCSVMonth) + '/' + TRANSFORM(lnCSVYear), LOGIT+SENDIT+NOWAITIT)


			* delete the existing data for current month
			SELECT TIMEDATA
			DELETE FOR (MONTH(WORKDATE) = lnCSVMonth) AND (YEAR(WORKDATE) = lnCSVYear)
			PACK
			********************************************************************************************************

			lnRowsProcessed = 0

			lnMaxRow = 200000


			FOR lnRow = 2 TO lnMaxRow

				WAIT WINDOW NOWAIT 'Processing Time Data Spreadsheet Row: ' + TRANSFORM(lnRow)

				STORE "" TO m.FILE_NUM, m.NAME, m.PAY_TYPE, m.BUSUNIT, m.DIVISION, m.DEPT, m.WORKSITE, m.GLACCTNO, m.COLLAR, m.INPUNCHNM, m.OUTPUNCHNM
				STORE 0 TO m.LUNCHMINS, m.TOTHOURS
				STORE CTOD('') TO m.WORKDATE, m.INPUNCHDT, m.OUTPUNCHDT

				lcRow = ALLTRIM(STR(lnRow))
				m.FILE_NUM = ALLTRIM(oWorksheet.RANGE("A"+lcRow).VALUE)
				IF ( ISNULL(m.FILE_NUM) OR EMPTY(m.FILE_NUM) ) THEN
					* we are past the last data row! exit the loop...
					EXIT FOR
				ENDIF
				m.NAME = oWorksheet.RANGE("B"+lcRow).VALUE
				m.WORKDATE = oWorksheet.RANGE("C"+lcRow).VALUE

				m.INPUNCHNM = NVL(oWorksheet.RANGE("D"+lcRow).VALUE,'   ')
				m.INPUNCHDT = NVL(oWorksheet.RANGE("E"+lcRow).VALUE,CTOT(''))
				m.OUTPUNCHNM = NVL(oWorksheet.RANGE("G"+lcRow).VALUE,'   ')
				m.OUTPUNCHDT = NVL(oWorksheet.RANGE("H"+lcRow).VALUE,CTOT(''))

				m.LUNCHMINS = oWorksheet.RANGE("J"+lcRow).VALUE
				m.PAY_TYPE = UPPER(oWorksheet.RANGE("M"+lcRow).VALUE)
				m.TOTHOURS = oWorksheet.RANGE("S"+lcRow).VALUE
				m.COMPANY = INT(VAL(LEFT(oWorksheet.RANGE("Y"+lcRow).VALUE,7)))
				
				lcBusUnit = UPPER(ALLTRIM(oWorksheet.RANGE("Z"+lcRow).VALUE))
				
				DO CASE
					CASE lcBusUnit = 'ADMIN'
						 m.BUSUNIT = 'ADM'
					CASE lcBusUnit = 'FREIGHT FORWARDING'
						 m.BUSUNIT = 'FF'
					CASE lcBusUnit = 'SUPPLY CHAIN SERVICES'
						 m.BUSUNIT = 'SCS'
					CASE lcBusUnit = 'GLOBAL LOGISTICS'
						 m.BUSUNIT = 'GL'
					CASE lcBusUnit = 'GLOBAL FORWARDING'
						 m.BUSUNIT = 'GF'
					CASE lcBusUnit = 'SHARED SERVICES'
						 m.BUSUNIT = 'SS'						
					OTHERWISE
						 m.BUSUNIT = '???'						
				ENDCASE
				
				m.DIVISION = LEFT(oWorksheet.RANGE("AA"+lcRow).VALUE,2)
				m.DEPT = LEFT(oWorksheet.RANGE("AB"+lcRow).VALUE,4)
				m.WORKSITE = LEFT(oWorksheet.RANGE("AC"+lcRow).VALUE,3)
				m.GLACCTNO = LEFT(oWorksheet.RANGE("AD"+lcRow).VALUE,4)
				m.COLLAR = UPPER(LEFT(oWorksheet.RANGE("AE"+lcRow).VALUE,1))
				m.TCDESC = oWorksheet.RANGE("AF"+lcRow).VALUE

				* check data quality before inserting.				
				* DELETE BAD DATA: SARA SUPERVISOR, BLANK DEPT #S, ETC.
				IF (VAL(m.FILE_NUM) < 10) OR (VAL(m.DEPT) = 0) THEN
					* bad data, do not insert into TIMEDATA
				ELSE				
					INSERT INTO TIMEDATA FROM MEMVAR
				ENDIF

				lnRowsProcessed = lnRowsProcessed + 1

			ENDFOR

			oWorkbook.CLOSE()

			.TrackProgress("In GetTimeDataXLS, lnRowsProcessed = " + TRANSFORM(lnRowsProcessed), LOGIT+SENDIT)

			*SELECT TIMEDATA

			*BROWSE FOR VAL(FILE_NUM) < 10

			*BROWSE FOR VAL(TIMEDATA.DEPT) = 0

			* populate calculated fields that correspond to old kronos/adp values
			SELECT TIMEDATA
			SCAN

				IF TIMEDATA.COMPANY = 3273300 THEN
					REPLACE TIMEDATA.ADP_COMP WITH 'E88' IN TIMEDATA && SALARIED
				ELSE
					REPLACE TIMEDATA.ADP_COMP WITH 'E87' IN TIMEDATA && HOURLY
				ENDIF

*!*					DO CASE
*!*						CASE TIMEDATA.BUSUNIT = 'A'
*!*							REPLACE TIMEDATA.BUSUNIT WITH 'ADM' IN TIMEDATA
*!*						CASE TIMEDATA.BUSUNIT = 'F'
*!*							REPLACE TIMEDATA.BUSUNIT WITH 'FF ' IN TIMEDATA
*!*						CASE TIMEDATA.BUSUNIT = 'S'
*!*							REPLACE TIMEDATA.BUSUNIT WITH 'SCS' IN TIMEDATA
*!*						OTHERWISE
*!*							* NOTHING
*!*					ENDCASE

				DO CASE
					CASE INLIST(ALLTRIM(TIMEDATA.PAY_TYPE),'REGULAR','GUARANTEE','LNCHPEN')
						REPLACE TIMEDATA.REGHOURS WITH TIMEDATA.TOTHOURS IN TIMEDATA
					CASE INLIST(ALLTRIM(TIMEDATA.PAY_TYPE),'OVERTIME','RETRO-OVT','WRKDHOL')
						REPLACE TIMEDATA.OTHOURS WITH TIMEDATA.TOTHOURS IN TIMEDATA
					CASE INLIST(ALLTRIM(TIMEDATA.PAY_TYPE),'DOUBLE')
						REPLACE TIMEDATA.DTHOURS WITH TIMEDATA.TOTHOURS IN TIMEDATA
					OTHERWISE
						REPLACE TIMEDATA.CODEHOURS WITH TIMEDATA.TOTHOURS IN TIMEDATA
				ENDCASE



*!*					* DELETE BAD DATA
*!*					* SARA SUPERVISOR, ETC.
*!*					IF VAL(TIMEDATA.FILE_NUM) < 10 THEN
*!*						DELETE IN TIMEDATA
*!*					ENDIF

*!*					* BAD DEPT CODES
*!*					IF VAL(TIMEDATA.DEPT) = 0 THEN
*!*						DELETE IN TIMEDATA
*!*					ENDIF


				*************** TAKE THIS OUT ONCE JOSH PROVIDES THE TOLLCLASS CODES **************************
				* fill in missing tollclass INFO, from DEPT lookup
				SELECT DEPTTRANS
				LOCATE FOR DEPT = TIMEDATA.DEPT
				IF FOUND() THEN
					REPLACE TIMEDATA.TOLLCLASS WITH DEPTTRANS.HCFTECODE, TIMEDATA.TCDESC WITH DEPTTRANS.HCFTEDESC IN TIMEDATA
				ENDIF
				***********************************************************************************************


			ENDSCAN

			.PopulateWageAmt()

			SELECT TIMEDATA
			BROWSE

			USE IN TIMEDATA

		ENDWITH
		RETURN
	ENDPROC  && GetTimeDataXLS



*!*	* don't use this - too much work to maintain changes in this...MB 10/3/2017
*!*		PROCEDURE GetTimeDataXLSOneDay

*!*			* this loads the 'data export - payroll' format from TimeStar report generator
*!*			* THIS SHOULD ONLY BE USED TO LOAD one day - typically the last day of the month

*!*			LOCAL lnMaxRow, lcRow, lnRow, oWorkbook, oWorksheet, lcSourceFile, lnRowsProcessed, ldWorkDate, lnCSVMonth, lnCSVYear

*!*			PRIVATE m.FILE_NUM, m.NAME, m.WORKDATE, m.LUNCHMINS, m.PAY_TYPE, m.TOTHOURS, m.BUSUNIT, m.DIVISION, m.DEPT, m.WORKSITE, m.GLACCTNO
*!*			PRIVATE m.COLLAR, m.INPUNCHNM, m.INPUNCHDT, m.OUTPUNCHNM, m.OUTPUNCHDT

*!*			WITH THIS

*!*				USE F:\UTIL\ADPREPORTS\DATA\DEPTTRANS.DBF IN 0 ALIAS DEPTTRANS


*!*				* OPEN THE TIMEDATA INFO TABLE
*!*				IF USED('TIMEDATA') THEN
*!*					USE IN TIMEDATA
*!*				ENDIF

*!*				USE F:\UTIL\TIMESTAR\DATA\TIMEDATA IN 0 ALIAS TIMEDATA EXCLUSIVE
*!*				*USE F:\UTIL\TIMESTAR\TESTDATA\TESTTIMEDATA IN 0 ALIAS TIMEDATA EXCLUSIVE   && test data

*!*				lcSourceFile = GETFILE('xls','Get a file')

*!*				.TrackProgress('Source File = ' + lcSourceFile, LOGIT+SENDIT)

*!*				oWorkbook = .oExcel.workbooks.OPEN(lcSourceFile)

*!*				oWorksheet = oWorkbook.Worksheets[1]


*!*				********************************************************************************************************
*!*				* get date in exported spreadsheet so that we can delete it from timedata before loading
*!*				ldWorkDate = oWorksheet.RANGE("C2").VALUE
*!*				IF TYPE('ldWorkDate') = 'T' THEN
*!*					ldWorkDate = TTOD(ldWorkDate)
*!*				ENDIF
*!*				
*!*				.TrackProgress('type of ldWorkDate = ' + TYPE('ldWorkDate'), SENDIT+NOWAITIT)

*!*				.TrackProgress('=====> Reloading Date: ' + TRANSFORM(ldWorkDate), LOGIT+SENDIT+NOWAITIT)


*!*				* delete the existing data for current month
*!*				SELECT TIMEDATA
*!*				DELETE FOR (WORKDATE = ldWorkDate)
*!*				PACK
*!*				********************************************************************************************************

*!*				lnRowsProcessed = 0

*!*				lnMaxRow = 200000


*!*				FOR lnRow = 2 TO lnMaxRow

*!*					WAIT WINDOW NOWAIT 'Processing Time Data Spreadsheet Row: ' + TRANSFORM(lnRow)

*!*					STORE "" TO m.FILE_NUM, m.NAME, m.PAY_TYPE, m.BUSUNIT, m.DIVISION, m.DEPT, m.WORKSITE, m.GLACCTNO, m.COLLAR, m.INPUNCHNM, m.OUTPUNCHNM
*!*					STORE 0 TO m.LUNCHMINS, m.TOTHOURS
*!*					STORE CTOD('') TO m.WORKDATE, m.INPUNCHDT, m.OUTPUNCHDT

*!*					lcRow = ALLTRIM(STR(lnRow))
*!*					m.FILE_NUM = ALLTRIM(oWorksheet.RANGE("A"+lcRow).VALUE)
*!*					IF ( ISNULL(m.FILE_NUM) OR EMPTY(m.FILE_NUM) ) THEN
*!*						* we are past the last data row! exit the loop...
*!*						EXIT FOR
*!*					ENDIF
*!*					m.NAME = oWorksheet.RANGE("B"+lcRow).VALUE
*!*					m.WORKDATE = oWorksheet.RANGE("C"+lcRow).VALUE

*!*					m.INPUNCHNM = NVL(oWorksheet.RANGE("D"+lcRow).VALUE,'   ')
*!*					m.INPUNCHDT = NVL(oWorksheet.RANGE("E"+lcRow).VALUE,CTOT(''))
*!*					m.OUTPUNCHNM = NVL(oWorksheet.RANGE("G"+lcRow).VALUE,'   ')
*!*					m.OUTPUNCHDT = NVL(oWorksheet.RANGE("H"+lcRow).VALUE,CTOT(''))

*!*					m.LUNCHMINS = oWorksheet.RANGE("J"+lcRow).VALUE
*!*					m.PAY_TYPE = UPPER(oWorksheet.RANGE("M"+lcRow).VALUE)
*!*					m.TOTHOURS = oWorksheet.RANGE("S"+lcRow).VALUE
*!*					m.COMPANY = INT(VAL(LEFT(oWorksheet.RANGE("Y"+lcRow).VALUE,7)))
*!*					m.BUSUNIT = UPPER(LEFT(oWorksheet.RANGE("Z"+lcRow).VALUE,1))
*!*					m.DIVISION = LEFT(oWorksheet.RANGE("AA"+lcRow).VALUE,2)
*!*					m.DEPT = LEFT(oWorksheet.RANGE("AB"+lcRow).VALUE,4)
*!*					m.WORKSITE = LEFT(oWorksheet.RANGE("AC"+lcRow).VALUE,3)
*!*					m.GLACCTNO = LEFT(oWorksheet.RANGE("AD"+lcRow).VALUE,4)
*!*					m.COLLAR = UPPER(LEFT(oWorksheet.RANGE("AE"+lcRow).VALUE,1))
*!*					m.TCDESC = oWorksheet.RANGE("AF"+lcRow).VALUE

*!*					* check data quality before inserting.				
*!*					* DELETE BAD DATA: SARA SUPERVISOR, BLANK DEPT #S, ETC.
*!*					IF (VAL(m.FILE_NUM) < 10) OR (VAL(m.DEPT) = 0) THEN
*!*						* bad data, do not insert into TIMEDATA
*!*					ELSE				
*!*						INSERT INTO TIMEDATA FROM MEMVAR
*!*					ENDIF

*!*					lnRowsProcessed = lnRowsProcessed + 1

*!*				ENDFOR

*!*				oWorkbook.CLOSE()

*!*				.TrackProgress("In GetTimeDataXLSOneDay, lnRowsProcessed = " + TRANSFORM(lnRowsProcessed), LOGIT+SENDIT)

*!*				*SELECT TIMEDATA

*!*				*BROWSE FOR VAL(FILE_NUM) < 10

*!*				*BROWSE FOR VAL(TIMEDATA.DEPT) = 0

*!*				* populate calculated fields that correspond to old kronos/adp values
*!*				SELECT TIMEDATA
*!*				SCAN

*!*					IF TIMEDATA.COMPANY = 3273300 THEN
*!*						REPLACE TIMEDATA.ADP_COMP WITH 'E88' IN TIMEDATA && SALARIED
*!*					ELSE
*!*						REPLACE TIMEDATA.ADP_COMP WITH 'E87' IN TIMEDATA && HOURLY
*!*					ENDIF

*!*					DO CASE
*!*						CASE TIMEDATA.BUSUNIT = 'A'
*!*							REPLACE TIMEDATA.BUSUNIT WITH 'ADM' IN TIMEDATA
*!*						CASE TIMEDATA.BUSUNIT = 'F'
*!*							REPLACE TIMEDATA.BUSUNIT WITH 'FF ' IN TIMEDATA
*!*						CASE TIMEDATA.BUSUNIT = 'S'
*!*							REPLACE TIMEDATA.BUSUNIT WITH 'SCS' IN TIMEDATA
*!*						OTHERWISE
*!*							* NOTHING
*!*					ENDCASE

*!*					DO CASE
*!*						CASE INLIST(ALLTRIM(TIMEDATA.PAY_TYPE),'REGULAR','GUARANTEE','LNCHPEN')
*!*							REPLACE TIMEDATA.REGHOURS WITH TIMEDATA.TOTHOURS IN TIMEDATA
*!*						CASE INLIST(ALLTRIM(TIMEDATA.PAY_TYPE),'OVERTIME','RETRO-OVT','WRKDHOL')
*!*							REPLACE TIMEDATA.OTHOURS WITH TIMEDATA.TOTHOURS IN TIMEDATA
*!*						CASE INLIST(ALLTRIM(TIMEDATA.PAY_TYPE),'DOUBLE')
*!*							REPLACE TIMEDATA.DTHOURS WITH TIMEDATA.TOTHOURS IN TIMEDATA
*!*						OTHERWISE
*!*							REPLACE TIMEDATA.CODEHOURS WITH TIMEDATA.TOTHOURS IN TIMEDATA
*!*					ENDCASE



*!*	*!*					* DELETE BAD DATA
*!*	*!*					* SARA SUPERVISOR, ETC.
*!*	*!*					IF VAL(TIMEDATA.FILE_NUM) < 10 THEN
*!*	*!*						DELETE IN TIMEDATA
*!*	*!*					ENDIF

*!*	*!*					* BAD DEPT CODES
*!*	*!*					IF VAL(TIMEDATA.DEPT) = 0 THEN
*!*	*!*						DELETE IN TIMEDATA
*!*	*!*					ENDIF


*!*					*************** TAKE THIS OUT ONCE JOSH PROVIDES THE TOLLCLASS CODES **************************
*!*					* fill in missing tollclass INFO, from DEPT lookup
*!*					SELECT DEPTTRANS
*!*					LOCATE FOR DEPT = TIMEDATA.DEPT
*!*					IF FOUND() THEN
*!*						REPLACE TIMEDATA.TOLLCLASS WITH DEPTTRANS.HCFTECODE, TIMEDATA.TCDESC WITH DEPTTRANS.HCFTEDESC IN TIMEDATA
*!*					ENDIF
*!*					***********************************************************************************************


*!*				ENDSCAN

*!*				.PopulateWageAmt()

*!*				SELECT TIMEDATA
*!*				GO BOTT
*!*				BROWSE

*!*				USE IN TIMEDATA

*!*			ENDWITH
*!*			RETURN
*!*		ENDPROC  && GetTimeDataXLSOneDay



	PROCEDURE PopulateWageAmt
		* populate hourlyrate and wageamt fields in timedata
		* revised 4/9/14 to get time sensitive rates from SALHIST table instead of EEINFO table.
		* assumes TIMEDATA alias has been opened in calling program

		LOCAL lcFileNum, lnHourlyRate, lnWageAmt, lnRow, lnInspID, ldWorkDate, lcSalNotFoundFile

		WITH THIS

			.TrackProgress('Running PopulateWageAmt()......',LOGIT+SENDIT)

			IF NOT USED('SALHIST') THEN
				USE F:\UTIL\TIMESTAR\DATA\SALHIST IN 0 ALIAS SALHIST
			ENDIF

			lnRow = 0
			SELECT TIMEDATA
			*SCAN
			SCAN FOR EMPTY(WAGEAMT)
				lnRow = lnRow + 1
				WAIT WINDOW NOWAIT 'Populating Wageamt for TIMEDATA REC # ' + TRANSFORM(lnRow)
				lnInspID = INT(VAL(TIMEDATA.FILE_NUM))
				ldWorkDate = TIMEDATA.WORKDATE

				SELECT SALHIST
				LOCATE FOR (INSPID = lnInspID) AND BETWEEN(ldWorkDate,FROMDATE,TODATE)
				IF FOUND() THEN

					lnHourlyRate = SALHIST.HOURLYRATE

					lnWageAmt = (TIMEDATA.CODEHOURS * lnHourlyRate) + (TIMEDATA.REGHOURS * lnHourlyRate) + ;
						(TIMEDATA.OTHOURS * lnHourlyRate * 1.5) + (TIMEDATA.DTHOURS * lnHourlyRate * 2.0)

					REPLACE TIMEDATA.HOURLYRATE WITH lnHourlyRate, TIMEDATA.WAGEAMT WITH lnWageAmt, TIMEDATA.SALFOUND WITH .T. IN TIMEDATA

				ELSE

					REPLACE TIMEDATA.SALFOUND WITH .F. IN TIMEDATA

				ENDIF

			ENDSCAN
			WAIT CLEAR

			IF USED('CURSALNOTFOUND') THEN
				USE IN CURSALNOTFOUND
			ENDIF

			SELECT * FROM TIMEDATA INTO CURSOR CURSALNOTFOUND WHERE NOT SALFOUND

			IF USED('CURSALNOTFOUND') AND (NOT EOF('CURSALNOTFOUND')) THEN
				lcSalNotFoundFile = 'F:\UTIL\TIMESTAR\REPORTS\ERRORS\SALNOTFOUND.XLS'
				SELECT CURSALNOTFOUND
				COPY TO (lcSalNotFoundFile) XL5
				.TrackProgress("**** Some wages were not found - see attachment.", LOGIT+SENDIT)
				*.cAttach = .cAttach + IIF(EMPTY(.cAttach),"",";") + lcSalNotFoundFile
				.AddAttachment(lcSalNotFoundFile)
			ENDIF

		ENDWITH
	ENDPROC  && PopulateWageAmt




	PROCEDURE PopulateDriverSS_Nums
		* populate SS_NUM field in time data for local drivers, where it is blank.
		* wil be called after each daily time data load...

		LOCAL lcDriversNotFoundFile

		WITH THIS

			.TrackProgress('Running PopulateDriverSS_Nums()......',LOGIT+SENDIT)
			
			IF NOT USED('EMPLOYEE') THEN
				USE F:\HR\HRDATA\EMPLOYEE AGAIN IN 0 ALIAS EMPLOYEE
			ENDIF
			
			IF NOT USED('TIMEDATA') THEN
				USE F:\UTIL\TIMESTAR\DATA\TIMEDATA AGAIN IN 0 ALIAS TIMEDATA
			ENDIF
			
			*ON KEY LABEL F2 EXIT
						
*!*				* Loop thru local drivers who have both ss_num and inspid populated...NOT filtering by active for the first pass, will on subsequent passes
*!*				SELECT EMPLOYEE
*!*				SCAN FOR ACTIVE AND (DEPT = 650) AND (INSPID > 0) AND (NOT EMPTY(SS_NUM))
*!*				*SCAN FOR (DEPT = 650) AND (INSPID > 0) AND (NOT EMPTY(SS_NUM))
*!*				
*!*					* update ss_num where it is blank, for driver based on matching inspid
*!*					SELECT TIMEDATA
*!*					SCAN FOR EMPTY(SS_NUM) AND (VAL(FILE_NUM) = EMPLOYEE.INSPID)
*!*						REPLACE TIMEDATA.SS_NUM WITH EMPLOYEE.SS_NUM IN TIMEDATA
*!*					ENDSCAN
*!*				
*!*				ENDSCAN 
			
			* FILL IN MISSING SS_NUMS, FIRST LOOP CHECKS ACTIVE EMPLOYEES IN EMPLOYEE TABLE
			SELECT TIMEDATA
			SCAN FOR EMPTY(SS_NUM) AND (DEPT = '0650') AND (BUSUNIT = 'SCS') && AND (VAL(FILE_NUM) = EMPLOYEE.INSPID)
				SELECT EMPLOYEE
				LOCATE FOR ACTIVE AND (INSPID = VAL(TIMEDATA.FILE_NUM))
				IF FOUND() THEN
					REPLACE TIMEDATA.SS_NUM WITH EMPLOYEE.SS_NUM IN TIMEDATA
				ENDIF
			ENDSCAN
		
			* FILL IN ANY REMAINING MISSING SS_NUMS, 2ND LOOP CHECKS INACTIVE EMPLOYEES IN EMPLOYEE TABLE
			SELECT TIMEDATA
			SCAN FOR EMPTY(SS_NUM) AND (DEPT = '0650') AND (BUSUNIT = 'SCS') && AND (VAL(FILE_NUM) = EMPLOYEE.INSPID)
				SELECT EMPLOYEE
				LOCATE FOR (NOT ACTIVE) AND (INSPID = VAL(TIMEDATA.FILE_NUM))
				IF FOUND() THEN
					REPLACE TIMEDATA.SS_NUM WITH EMPLOYEE.SS_NUM IN TIMEDATA
				ENDIF
			ENDSCAN
			
			
*!*				SELECT TIMEDATA
*!*				COPY TO C:\A\BLANK_SS_NUM.XLS FOR (DEPT = '0650') AND EMPTY(SS_NUM) AND (BUSUNIT = 'SCS') AND ((REGHOURS  OTHOURS) > 0) XL5 

			IF USED('CURDRIVERSNOTFOUND') THEN
				USE IN CURDRIVERSNOTFOUND
			ENDIF

			SELECT * FROM TIMEDATA INTO CURSOR CURDRIVERSNOTFOUND WHERE (DEPT = '0650') AND EMPTY(SS_NUM) AND (BUSUNIT = 'SCS') AND ((REGHOURS + OTHOURS) > 0)

			IF USED('CURDRIVERSNOTFOUND') AND (NOT EOF('CURDRIVERSNOTFOUND')) THEN
				lcDriversNotFoundFile = 'F:\UTIL\TIMESTAR\REPORTS\ERRORS\DRIVERSNOTFOUND_SS_NUM.XLS'
				SELECT CURDRIVERSNOTFOUND
				COPY TO (lcDriversNotFoundFile) XL5
				.TrackProgress("**** Some Drivers were not found updating SS#s - see attachment.", LOGIT+SENDIT)
				*.cAttach = .cAttach + IIF(EMPTY(.cAttach),"",";") + lcSalNotFoundFile
				.AddAttachment(lcDriversNotFoundFile)
			ENDIF


		ENDWITH 
	ENDPROC  && PopulateDriverSS_Nums


	FUNCTION AddAttachment
		LPARAMETERS tcFilename
		WITH THIS
			LOCAL llRetVal
			llRetVal = .T.
			* add to attachments
			IF FILE(tcFilename) THEN
				IF EMPTY(.cAttach) THEN
					.cAttach = tcFilename
				ELSE
					.cAttach = .cAttach + ";" + tcFilename
				ENDIF
			ELSE
				.TrackProgress('!!!! ERROR: file [' + tcFilename + '] could not be added to list of attachments',LOGIT+WAITIT+SENDIT)
			ENDIF
		ENDWITH
		RETURN llRetVal
	ENDFUNC


	PROCEDURE GetBasicEEInfo

		LOCAL lnMaxRow, lcRow, lnRow, oWorkbook, oWorksheet, lcSourceFile, lnRowsProcessed, lnInspID, lcGender, lnHourlyRate, lnAnnualSalary, lcTimeZone
		LOCAL ldPTODate, lcSplit1, lcSplit2, lcSplit3, lcSplit4, ldFarFutureDate, ldToday, ldYesterday, lcBirthDay, lcStatus, lcWEmail, lcHEmail, lcWRKDSTATE, lcJobTitle

		PRIVATE m.STATUS, m.COMPANY, m.NAME, m.INSPID, m.GENDER, m.HOURLYRATE, m.BUSUNIT, m.BUSUNITDSC, m.DIVISION, m.DIVDESC, m.DEPT
		PRIVATE m.DEPTDESC, m.WORKSITE, m.WSITEDESC, m.GLACCTNO, m.COLLAR, m.TOLLCLASS, m.TCDESC, m.ANNSALARY, m.HIREDATE, m.TERMDATE, m.RATETYPE
		PRIVATE	m.STREET, m.CITY, m.STATE, m.ZIP, m.PHONE, m.SUPERVISOR, m.BADGENUM, m.PTODATE

		WITH THIS

			* ZAP THE BASIC EE INFO TABLE
			USE F:\UTIL\TIMESTAR\DATA\EEINFO IN 0 ALIAS EEINFO EXCLUSIVE
			SELECT EEINFO
			DELETE ALL
			PACK

			* OPEN LOOKUP ABLE TO FILL IN MISSING INFO
			USE F:\UTIL\ADPREPORTS\DATA\DEPTTRANS.DBF AGAIN IN 0 ALIAS DEPTTRANS


			lcSourceFile = 'F:\UTIL\TIMESTAR\REPORTS\BASICEEINFO\EEDATADUMP.XLS'

			oWorkbook = .oExcel.workbooks.OPEN(lcSourceFile)

			oWorksheet = oWorkbook.Worksheets[1]

			lnRowsProcessed = 0

			lnMaxRow = 2000


			FOR lnRow = 2 TO lnMaxRow

				WAIT WINDOW NOWAIT 'Processing Basic EE Info Spreadsheet Row: ' + TRANSFORM(lnRow)

				STORE "" TO m.NAME, m.BUSUNIT, m.BUSUNITDSC, m.DIVISION, m.DIVDESC, m.DEPT, m.DEPTDESC, m.WORKSITE, m.WSITEDESC, m.GLACCTNO, m.COLLAR, m.TOLLCLASS, m.TCDESC, m.STATUS
				STORE "" TO m.RATETYPE, m.STREET, m.CITY, m.STATE, m.ZIP, m.PHONE, m.SUPERVISOR
				STORE "U" TO m.GENDER
				STORE 0 TO m.COMPANY, m.INSPID, m.HOURLYRATE, m.ANNSALARY, m.BADGENUM
				STORE CTOD('') TO m.HIREDATE, m.TERMDATE, m.PTODATE


				lcRow = ALLTRIM(STR(lnRow))

				m.INSPID = oWorksheet.RANGE("A"+lcRow).VALUE

				* If the companyfield is empty in the current row, we assume we are past the last row with data in it.
				IF ( ISNULL(m.INSPID) OR EMPTY(m.INSPID) ) THEN
					* we are past the last data row! exit the loop...
					EXIT FOR
				ENDIF

				m.NAME = ALLTRIM(oWorksheet.RANGE("G"+lcRow).VALUE) + ", " + ALLTRIM(oWorksheet.RANGE("H"+lcRow).VALUE) + " " + ALLTRIM(NVL(oWorksheet.RANGE("I"+lcRow).VALUE,""))

				m.STATUS = UPPER(ALLTRIM(oWorksheet.RANGE("J"+lcRow).VALUE))
				DO CASE
					CASE m.STATUS = "HIRED"
						m.STATUS = "A"
					CASE m.STATUS = "TERM"
						m.STATUS = "T"
					OTHERWISE
						m.STATUS = "L"
						*m.STATUS = UPPER(LEFT(m.STATUS,1))
				ENDCASE

				m.TERMDATE = oWorksheet.RANGE("L"+lcRow).VALUE
				IF m.TERMDATE = {^2079-06-06} THEN
					m.TERMDATE = CTOD('')
				ENDIF

				m.HIREDATE = oWorksheet.RANGE("N"+lcRow).VALUE

				m.RATETYPE = LEFT(oWorksheet.RANGE("M"+lcRow).VALUE,1)

				m.COMPANY = oWorksheet.RANGE("T"+lcRow).VALUE

				m.BUSUNIT = NVL(oWorksheet.RANGE("W"+lcRow).VALUE,'')


				m.DIVISION = PADL(ALLTRIM(TRANSFORM(NVL(oWorksheet.RANGE("Z"+lcRow).VALUE,''))),2,'0')

				m.DEPT = PADL(ALLTRIM(TRANSFORM(NVL(oWorksheet.RANGE("AC"+lcRow).VALUE,''))),4,'0')

				m.WORKSITE = NVL(oWorksheet.RANGE("AF"+lcRow).VALUE,'')

				m.GLACCTNO = ALLTRIM(TRANSFORM(NVL(oWorksheet.RANGE("AI"+lcRow).VALUE,0)))

				m.COLLAR = NVL(oWorksheet.RANGE("AL"+lcRow).VALUE,'')

				m.TOLLCLASS = NVL(oWorksheet.RANGE("AO"+lcRow).VALUE,'')

				m.STREET = NVL(oWorksheet.RANGE("O"+lcRow).VALUE,' ')


				m.CITY = NVL(oWorksheet.RANGE("P"+lcRow).VALUE,' ')

				m.STATE = NVL(oWorksheet.RANGE("Q"+lcRow).VALUE,' ')

				m.ZIP = ALLTRIM(TRANSFORM(NVL(oWorksheet.RANGE("R"+lcRow).VALUE,0)))
				IF (LEN(m.ZIP) = 4) OR (LEN(m.ZIP) = 8) THEN
					* initial zero was lost - add it
					m.ZIP = '0' + m.ZIP
				ENDIF

				m.PHONE = NVL(oWorksheet.RANGE("S"+lcRow).VALUE,' ')

				m.SUPERVISOR = 	ALLTRIM(NVL(oWorksheet.RANGE("AS"+lcRow).VALUE,' ')) + ' ' + ALLTRIM(NVL(oWorksheet.RANGE("AR"+lcRow).VALUE,' '))

				m.BADGENUM = NVL(oWorksheet.RANGE("D"+lcRow).VALUE,0)

				m.PTODATE = oWorksheet.RANGE("AX"+lcRow).VALUE
				IF m.PTODATE = {^2079-06-06} THEN
					m.PTODATE = CTOD('')
				ENDIF


				INSERT INTO EEINFO FROM MEMVAR

				lnRowsProcessed = lnRowsProcessed + 1

			ENDFOR

			oWorkbook.CLOSE()

			.TrackProgress("In GetBasicEEInfo, lnRowsProcessed = " + TRANSFORM(lnRowsProcessed), LOGIT+SENDIT)


			* delete test/bogus employees
			SELECT EEINFO
			SCAN

				IF EEINFO.INSPID = 1 THEN
					DELETE
				ENDIF

			ENDSCAN

			* LOG AND fill in missing data
			SELECT EEINFO
			SCAN

				IF EMPTY(EEINFO.WORKSITE) THEN
					.TrackProgress("ERROR: empty WORKSITE for " + EEINFO.NAME, LOGIT+SENDIT)
				ENDIF

				IF EEINFO.COMPANY = 3273300 THEN
					REPLACE EEINFO.ADP_COMP WITH 'E88' IN EEINFO && SALARIED
				ELSE
					REPLACE EEINFO.ADP_COMP WITH 'E87' IN EEINFO && HOURLY
				ENDIF

				SELECT DEPTTRANS
				LOCATE FOR DEPTTRANS.DEPT = EEINFO.DEPT
				IF FOUND() THEN

					* this always needs to be populated
					REPLACE EEINFO.DEPTDESC WITH DEPTTRANS.DESC IN EEINFO

					IF EMPTY(EEINFO.TOLLCLASS) THEN
						* replace code and description
						.TrackProgress("WARNING: filled empty TOLLCLASS for " + EEINFO.NAME, LOGIT+SENDIT)
						REPLACE EEINFO.TOLLCLASS WITH DEPTTRANS.HCFTECODE, EEINFO.TCDESC WITH DEPTTRANS.HCFTEDESC IN EEINFO
					ELSE
						* just replace the desc, which is always missing
						REPLACE EEINFO.TCDESC WITH DEPTTRANS.HCFTEDESC IN EEINFO
					ENDIF

					IF EMPTY(EEINFO.GLACCTNO) THEN
						.TrackProgress("WARNING: filled empty GLACCTNO for " + EEINFO.NAME, LOGIT+SENDIT)
						REPLACE EEINFO.GLACCTNO WITH DEPTTRANS.GLACCTNO IN EEINFO
					ENDIF

					IF EMPTY(EEINFO.COLLAR) THEN
						.TrackProgress("WARNING: filled empty COLLAR for " + EEINFO.NAME, LOGIT+SENDIT)
						REPLACE EEINFO.COLLAR WITH DEPTTRANS.COLLARTYPE IN EEINFO
					ENDIF

				ENDIF

			ENDSCAN



			************************************************************
			************************************************************
			************************************************************
			*** look in other spreadsheet(s) to fill in data we could not get from timestar, such as Gender & Hourly Rate & STATUS

			lcSourceFile = 'F:\UTIL\INSPERITY\ESCREPORTS\BASICEEINFO\BASICEEINFO.XLSX'

			oWorkbook = .oExcel.workbooks.OPEN(lcSourceFile)

			oWorksheet = oWorkbook.Worksheets[1]

			lnRowsProcessed = 0

			lnMaxRow = 2000

			FOR lnRow = 8 TO lnMaxRow

				WAIT WINDOW NOWAIT 'Getting other Payroll EE Info, Spreadsheet Row: ' + TRANSFORM(lnRow)

				*!*					IF lnRow = 626 THEN
				*!*						SET STEP ON
				*!*					ENDIF

				lcRow = ALLTRIM(STR(lnRow))

				lnInspID = oWorksheet.RANGE("C"+lcRow).VALUE

				* If the companyfield is empty in the current row, we assume we are past the last row with data in it.
				IF ( ISNULL(lnInspID) OR EMPTY(lnInspID) ) THEN
					* we are past the last data row! exit the loop...
					EXIT FOR
				ENDIF

				lcGender = oWorksheet.RANGE("D"+lcRow).VALUE
				lcGender = NVL(lcGender,'U')
				lcGender = UPPER(LEFT(lcGender,1))

				lnHourlyRate = NVL(oWorksheet.RANGE("E"+lcRow).VALUE,0.00)
				*lnHourlyRate = 0.00

				* PTODATE is not gotten from the prior employee info spreadsheet, the one from TimeStar 12/21/2017 MB
				*ldPTODate = NVL(oWorksheet.RANGE("H"+lcRow).VALUE,CTOD(''))

				* the following are to compare against what we already have from Timestar...
				lcSplit1 = NVL(oWorksheet.RANGE("I"+lcRow).VALUE,SPACE(3))  && Bus Unit in Payroll
				lcSplit2 = NVL(oWorksheet.RANGE("J"+lcRow).VALUE,SPACE(2))  && Division
				lcSplit3 = NVL(oWorksheet.RANGE("K"+lcRow).VALUE,SPACE(4))  && Dept
				lcSplit4 = NVL(oWorksheet.RANGE("L"+lcRow).VALUE,SPACE(3))  && worksite

				* more not available in TimeStar
				lcBirthDay = NVL(oWorksheet.RANGE("M"+lcRow).VALUE,SPACE(6))  && birthday

				lcStatus = NVL(oWorksheet.RANGE("N"+lcRow).VALUE,SPACE(1))  && status
				
				lcWEmail = NVL(oWorksheet.RANGE("O"+lcRow).VALUE,SPACE(1)) && work email
				
				lcHEmail = NVL(oWorksheet.RANGE("P"+lcRow).VALUE,SPACE(1)) && home email
				
				lcWRKDSTATE = NVL(oWorksheet.RANGE("Q"+lcRow).VALUE,SPACE(1)) && Worked state
				
				
				lnAnnualSalary = NVL(oWorksheet.RANGE("R"+lcRow).VALUE,0.00)
				*lnAnnualSalary = 0.00
				
				lcJobTitle = NVL(oWorksheet.RANGE("S"+lcRow).VALUE,SPACE(1))  && Job Title - added 8/21/2017 at Abby's request
				
*!*					IF lnInspID = 2866528 then
*!*						SET STEP ON 
*!*					endif

				* update the record in EEINFO table
				SELECT EEINFO
				LOCATE FOR INSPID = lnInspID
				IF FOUND() THEN
				
					IF EMPTY(lcWRKDSTATE) THEN
						lcWRKDSTATE2 = UPPER(LEFT(ALLTRIM(EEINFO.WORKSITE),2))
					ELSE
						lcWRKDSTATE2 = UPPER(ALLTRIM(lcWRKDSTATE))
					ENDIF
					
					* Derive time zone from worked state
					DO CASE
						CASE INLIST(lcWRKDSTATE2,'CA','WA')
							lcTimeZone = 'P'
						CASE INLIST(lcWRKDSTATE2,'IL','OH','TX')
							lcTimeZone = 'C'
						CASE INLIST(lcWRKDSTATE2,'AL','FL','GA','MA','MD','ME','NY','NJ','KY','PA','RI','TN')
							lcTimeZone = 'E'
						OTHERWISE
							lcTimeZone = '?'
					ENDCASE
				
				
					*!*	REPLACE EEINFO.GENDER WITH lcGender, ;
					*!*		EEINFO.PTODATE WITH ldPTODate, EEINFO.SPLIT1 WITH lcSplit1, EEINFO.SPLIT2 WITH lcSplit2, EEINFO.SPLIT3 WITH lcSplit3, EEINFO.SPLIT4 WITH lcSplit4, ;
					*!*		EEINFO.BIRTHDAY WITH lcBirthDay, EEINFO.STATUS WITH lcStatus, EEINFO.WEMAIL WITH lcWEmail, EEINFO.HEMAIL WITH lcHEmail, EEINFO.WRKDSTATE WITH lcWRKDSTATE ;
					*!*		IN EEINFO

					* PTODATE is not gotten from the prior employee info spreadsheet, the one from TimeStar 12/21/2017 MB
					*!*	REPLACE EEINFO.GENDER WITH lcGender, ;
					*!*		EEINFO.PTODATE WITH ldPTODate, EEINFO.SPLIT1 WITH lcSplit1, EEINFO.SPLIT2 WITH lcSplit2, EEINFO.SPLIT3 WITH lcSplit3, EEINFO.SPLIT4 WITH lcSplit4, ;
					*!*		EEINFO.BIRTHDAY WITH lcBirthDay, EEINFO.STATUS WITH lcStatus, EEINFO.WEMAIL WITH lcWEmail, EEINFO.HEMAIL WITH lcHEmail, EEINFO.WRKDSTATE WITH lcWRKDSTATE, ;
					*!*		EEINFO.HOURLYRATE WITH lnHourlyRate, EEINFO.ANNSALARY WITH lnAnnualSalary, EEINFO.JOBTITLE WITH lcJobTitle, EEINFO.TIMEZONE WITH lcTimeZone ;
					*!*		IN EEINFO

					REPLACE EEINFO.GENDER WITH lcGender, ;
						EEINFO.SPLIT1 WITH lcSplit1, EEINFO.SPLIT2 WITH lcSplit2, EEINFO.SPLIT3 WITH lcSplit3, EEINFO.SPLIT4 WITH lcSplit4, ;
						EEINFO.BIRTHDAY WITH lcBirthDay, EEINFO.STATUS WITH lcStatus, EEINFO.WEMAIL WITH lcWEmail, EEINFO.HEMAIL WITH lcHEmail, EEINFO.WRKDSTATE WITH lcWRKDSTATE, ;
						EEINFO.HOURLYRATE WITH lnHourlyRate, EEINFO.ANNSALARY WITH lnAnnualSalary, EEINFO.JOBTITLE WITH lcJobTitle, EEINFO.TIMEZONE WITH lcTimeZone ;
						IN EEINFO

				ENDIF

				lnRowsProcessed = lnRowsProcessed + 1

			ENDFOR

			oWorkbook.CLOSE()

			*ERASE (lcSourceFile)  && for privacy
			
			SELECT EEINFO
			BROWSE

			************************************************************
			************************************************************
			************************************************************
			* update new Salary history table
			.TrackProgress("*** Updating Salary history table....", LOGIT+SENDIT+NOWAITIT)

			ldToday = DATE()
			ldYesterday = ldToday - 1
			ldFarFutureDate = {^2099-12-31}
			ldInsperityStartDate = {^2013-12-29}

			IF NOT USED('SALHIST') THEN
				USE F:\UTIL\TIMESTAR\DATA\SALHIST IN 0 ALIAS SALHIST
			ENDIF

			SELECT EEINFO
			SCAN && FOR STATUS <> 'T'

				* we always search for a record with TODATE = ldFarFutureDate, which is always the 'latest' salary history record.
				SELECT SALHIST
				LOCATE FOR (INSPID = EEINFO.INSPID)	AND (TODATE = ldFarFutureDate)
				IF FOUND() THEN

					* if existing hourlyrate changed by more than 1 cent, insert a new sal hist record
					IF (ABS(EEINFO.HOURLYRATE - SALHIST.HOURLYRATE)) > .01 THEN

						.TrackProgress("==> Hourly Rate changed for " + ALLTRIM(EEINFO.NAME) + "  # " + TRANSFORM(EEINFO.INSPID), LOGIT+SENDIT+NOWAITIT)

						* handle case where we update more than once in the same day - probably only will happen in initial testing, but hey...
						IF SALHIST.FROMDATE = ldToday THEN
							* We've already added a change record today -
							* Just change the hourlyrate, since everything else should stay the same
							REPLACE SALHIST.HOURLYRATE WITH EEINFO.HOURLYRATE IN SALHIST

						ELSE
							* standard case.
							* first change the todate in current record to yesterday
							REPLACE SALHIST.TODATE WITH ldYesterday IN SALHIST

							* then insert the new record with today as fromdate
							INSERT INTO SALHIST (ADP_COMP, NAME, INSPID, HOURLYRATE, FROMDATE, TODATE) ;
								VALUES (EEINFO.ADP_COMP, EEINFO.NAME, EEINFO.INSPID, EEINFO.HOURLYRATE, ldToday, ldFarFutureDate)

						ENDIF  && SALHIST.FROMDATE = ldToday
					ENDIF &&  (ABS(EEINFO.HOURLYRATE - SALHIST.HOURLYRATE)) > .01

				ELSE

					WAIT WINDOW "Added initial Hourly Rate for " + ALLTRIM(EEINFO.NAME) + "  # " + TRANSFORM(EEINFO.INSPID) NOWAIT

					* insert inital record
					INSERT INTO SALHIST (ADP_COMP, NAME, INSPID, HOURLYRATE, FROMDATE, TODATE) ;
						VALUES (EEINFO.ADP_COMP, EEINFO.NAME, EEINFO.INSPID, EEINFO.HOURLYRATE, ldInsperityStartDate, ldFarFutureDate)

				ENDIF

			ENDSCAN

			WAIT CLEAR


			************************************************************
			************************************************************
			************************************************************

			USE IN EEINFO
			USE IN SALHIST

		ENDWITH
		RETURN
	ENDPROC  && GetBasicEEInfo



	PROCEDURE GetTermEEInfo
		* update term info in eeinfo

		WITH THIS

			LOCAL lcTermReason, ldTermDate, lcStatus, lnInspID

			USE F:\UTIL\TIMESTAR\DATA\EEINFO IN 0 ALIAS EEINFO

			lcSourceFile = 'F:\UTIL\INSPERITY\ESCREPORTS\TERMINFO\TERMINFO.XLS'

			oWorkbook = .oExcel.workbooks.OPEN(lcSourceFile)

			oWorksheet = oWorkbook.Worksheets[1]

			lnRowsProcessed = 0

			lnMaxRow = 2000

			FOR lnRow = 8 TO lnMaxRow

				WAIT WINDOW NOWAIT 'Processing Terminated EE Info Spreadsheet Row: ' + TRANSFORM(lnRow)

				lcRow = ALLTRIM(STR(lnRow))

				lnInspID = oWorksheet.RANGE("B"+lcRow).VALUE

				* If the insperity id is empty in the current row, we assume we are past the last row with data in it.
				IF ( ISNULL(lnInspID) OR EMPTY(lnInspID) ) THEN
					* we are past the last data row! exit the loop...
					EXIT FOR
				ENDIF

				lcStatus = UPPER(ALLTRIM(oWorksheet.RANGE("C"+lcRow).VALUE))

				ldTermDate = oWorksheet.RANGE("D"+lcRow).VALUE

				lcTermReason = UPPER(ALLTRIM(oWorksheet.RANGE("E"+lcRow).VALUE))

				IF lcStatus = "TERMINATED" THEN
					SELECT EEINFO
					LOCATE FOR INSPID = lnInspID
					IF FOUND() THEN
						IF EEINFO.STATUS <> 'T' THEN
							.TrackProgress("For " + ALLTRIM(EEINFO.NAME) + ", prior Status = " + EEINFO.STATUS, LOGIT+SENDIT+NOWAITIT)
						ENDIF
						* update term date and reason
						REPLACE EEINFO.TERMDATE WITH ldTermDate, EEINFO.TERMREASON WITH lcTermReason IN EEINFO
					ELSE
						.TrackProgress("ERROR: could not find " + ALLTRIM(EEINFO.NAME), LOGIT+SENDIT+NOWAITIT)
					ENDIF
				ENDIF

				lnRowsProcessed = lnRowsProcessed + 1

			ENDFOR

			oWorkbook.CLOSE()

			.TrackProgress("In GetTermEEInfo, lnRowsProcessed = " + TRANSFORM(lnRowsProcessed), LOGIT+SENDIT)
			************************************************************
			************************************************************
			************************************************************

			USE IN EEINFO

		ENDWITH
		RETURN
	ENDPROC  && GetTermEEInfo


	* moved to InsperityReports project
	*!*		PROCEDURE GetAccrualsLiability
	*!*			WITH THIS
	*!*
	*!*				LOCAL ldToday, ldJan1st, lnUntaken, lnLiability, lcOutputFile
	*!*				ldToday = DATE()
	*!*				ldJan1st = {^2014-01-01}
	*!*
	*!*				* OPEN THE ACCRUALS INFO TABLE
	*!*				IF USED('EEINFO') THEN
	*!*					USE IN EEINFO
	*!*				ENDIF
	*!*				IF USED('ACCRDATA') THEN
	*!*					USE IN ACCRDATA
	*!*				ENDIF
	*!*				IF USED('CURLIABILITY1') THEN
	*!*					USE IN CURLIABILITY1
	*!*				ENDIF
	*!*				IF USED('CURLIABILITY2') THEN
	*!*					USE IN CURLIABILITY2
	*!*				ENDIF

	*!*				USE F:\UTIL\TIMESTAR\DATA\ACCRDATA IN 0 ALIAS ACCRDATA SHARED

	*!*				USE F:\UTIL\TIMESTAR\DATA\EEINFO IN 0 ALIAS EEINFO SHARED

	*!*				* transcode	transname
	*!*				* AC		Accrual
	*!*				* AJ		Accrual Adjustment
	*!*				* CA		Carryover
	*!*				* CJ		Carryover Adjustment
	*!*				* EX		Expired
	*!*				* MA		Manual Accrual
	*!*				* MC		Manual Carryover
	*!*				* MT		Manual Taken
	*!*				* TA		Taken
	*!*
	*!*				SELECT ;
	*!*					INSPID, ;
	*!*					SUM(IIF(INLIST(TRANSCODE,'AC','CA','MA'),HOURS,0)) AS ACCRUED, ;
	*!*					SUM(IIF(INLIST(TRANSCODE,'AJ','MT','TA'),HOURS,0)) AS TAKEN ;
	*!*					FROM ACCRDATA ;
	*!*					INTO CURSOR CURLIABILITY1 ;
	*!*					WHERE BETWEEN(EFFDATE, ldJan1st, ldToday) ;
	*!*					AND ACCRCODE = 'VAC' ;
	*!*					GROUP BY INSPID ;
	*!*					ORDER BY INSPID
	*!*
	*!*	*!*				SELECT CURLIABILITY1
	*!*	*!*				BROWSE
	*!*
	*!*				SELECT A.*, B.NAME, B.HOURLYRATE, 0000.00 AS UNTAKEN, 0000000.00 AS LIABILITY ;
	*!*					FROM CURLIABILITY1 A ;
	*!*					LEFT OUTER JOIN EEINFO B ;
	*!*					ON B.INSPID = A.INSPID ;
	*!*					INTO CURSOR CURLIABILITY2 ;
	*!*					ORDER BY B.NAME ;
	*!*					READWRITE
	*!*
	*!*				SELECT CURLIABILITY2
	*!*				SCAN
	*!*					lnUntaken = CURLIABILITY2.ACCRUED  + CURLIABILITY2.TAKEN  && Adding here because Takens are negative in TimeStar, not positive
	*!*					lnLiability = lnUntaken * CURLIABILITY2.HOURLYRATE
	*!*					REPLACE CURLIABILITY2.UNTAKEN WITH lnUntaken, CURLIABILITY2.LIABILITY WITH 	lnLiability IN CURLIABILITY2
	*!*				ENDSCAN
	*!*
	*!*				lcOutputFile = 'F:\UTIL\TIMESTAR\REPORTS\VACLIABILITY_' + DTOS(ldToday) + '.XLS'
	*!*				SELECT CURLIABILITY2
	*!*				COPY TO (lcOutputFile) XL5
	*!*				.cAttach = lcOutputFile
	*!*
	*!*
	*!*				IF USED('EEINFO') THEN
	*!*					USE IN EEINFO
	*!*				ENDIF
	*!*				IF USED('ACCRDATA') THEN
	*!*					USE IN ACCRDATA
	*!*				ENDIF
	*!*				IF USED('CURLIABILITY1') THEN
	*!*					USE IN CURLIABILITY1
	*!*				ENDIF
	*!*				IF USED('CURLIABILITY2') THEN
	*!*					USE IN CURLIABILITY2
	*!*				ENDIF
	*!*
	*!*			ENDWITH
	*!*			RETURN
	*!*		ENDPROC  && GetAccrualsLiability



	PROCEDURE LoadAccrualsData
		* LOAD all TimeStar data on Sick and Vacation accruals; to drive Liability reports for Ken.
		* Derived from TimeStar report: Data Export-Accruals.

		LOCAL lnMaxRow, lcRow, lnRow, oWorkbook, oWorksheet, lcSourceFile, lnRowsProcessed, lcMode

		PRIVATE m.INSPID, m.NAME, m.ACCRCODE, m.EFFDATE, m.TRANSCODE, m.TRANSNAME, m.PENDING, m.HOURS

		WITH THIS

			SET DECIMALS TO 2

			* OPEN THE ACCRUALS INFO TABLE
			IF USED('ACCRDATA') THEN
				USE IN ACCRDATA
			ENDIF

			USE F:\UTIL\TIMESTAR\DATA\ACCRDATA IN 0 ALIAS ACCRDATA EXCLUSIVE

			* ZAP TABLE
			SELECT ACCRDATA
			DELETE ALL IN ACCRDATA
			PACK

			lcSourceFile = 'F:\UTIL\TIMESTAR\REPORTS\ACCRUALSDATA\ACCRUAL_EXPORT(1).XLS'

			.TrackProgress('Source File = ' + lcSourceFile, LOGIT+SENDIT)

			oWorkbook = .oExcel.workbooks.OPEN(lcSourceFile)

			oWorksheet = oWorkbook.Worksheets[1]

			lnRowsProcessed = 0

			lnMaxRow = 100000


			FOR lnRow = 2 TO lnMaxRow

				WAIT WINDOW NOWAIT 'Processing Accruals Data Spreadsheet Row: ' + TRANSFORM(lnRow)

				STORE "" TO m.NAME, m.ACCRCODE, m.TRANSCODE, m.TRANSNAME
				STORE 0 TO m.INSPID, m.HOURS
				STORE .F. TO m.PENDING
				STORE CTOD('') TO m.EFFDATE

				lcRow = ALLTRIM(STR(lnRow))

				*!*	SET STEP ON
				m.INSPID = oWorksheet.RANGE("A"+lcRow).VALUE
				IF ( ISNULL(m.INSPID) OR EMPTY(m.INSPID) ) THEN
					* we are past the last data row! exit the loop...
					EXIT FOR
				ENDIF
				m.INSPID = INT(VAL(ALLTRIM(m.INSPID)))

				m.NAME = ALLTRIM(NVL(oWorksheet.RANGE("B"+lcRow).VALUE,''))

				m.ACCRCODE = ALLTRIM(NVL(oWorksheet.RANGE("I"+lcRow).VALUE,''))

				m.EFFDATE = CTOD(NVL(oWorksheet.RANGE("J"+lcRow).VALUE,''))

				m.TRANSCODE = ALLTRIM(NVL(oWorksheet.RANGE("K"+lcRow).VALUE,''))

				m.TRANSNAME = ALLTRIM(NVL(oWorksheet.RANGE("L"+lcRow).VALUE,''))

				m.PENDING = NVL(oWorksheet.RANGE("M"+lcRow).VALUE,0)
				m.PENDING = (m.PENDING = 1)

				m.HOURS = NVL(oWorksheet.RANGE("N"+lcRow).VALUE,0)

				INSERT INTO ACCRDATA FROM MEMVAR

				lnRowsProcessed = lnRowsProcessed + 1

			ENDFOR

			oWorkbook.CLOSE()

			.TrackProgress("In LoadAccrualsData(), lnRowsProcessed = " + TRANSFORM(lnRowsProcessed), LOGIT+SENDIT)


			SELECT ACCRDATA

			BROWSE FOR INSPID < 10


			SELECT ACCRDATA
			BROWSE

			USE IN ACCRDATA

			SET DECIMALS TO 4

		ENDWITH
		RETURN
	ENDPROC  && LoadAccrualsData



	PROCEDURE GetPayrollInfo

		LOCAL laFiles[1,5], laFilesSorted[1,6], lnNumFiles, lcInputFolder, lcSourceFile, lnCurrentFile

		WITH THIS

			* ZAP THE PAYROLLINFO TABLE
			IF USED('PAYROLLINFO') THEN
				USE IN PAYROLLINFO
			ENDIF
			USE F:\UTIL\INSPERITY\DATA\PAYROLLINFO.DBF EXCLUSIVE IN 0 ALIAS PAYROLLINFO
			SELECT PAYROLLINFO
			DELETE ALL
			PACK

			* now load all files in the payroll info folder - there should be one per month

			lcInputFolder = 'F:\UTIL\INSPERITY\ESCREPORTS\PAYROLLINFO\'

			lnNumFiles = ADIR(laFiles,(lcInputFolder + 'PAYROLLINFO*.XLS'))

			IF lnNumFiles > 0 THEN

				* sort file list by date/time
				.SortArrayByDateTime(@laFiles, @laFilesSorted)

				FOR lnCurrentFile = 1 TO lnNumFiles

					lcSourceFile = lcInputFolder + laFilesSorted[lnCurrentFile,1]

					.ProcessPayrollInfoFile( lcSourceFile )

				ENDFOR

			ELSE
				.TrackProgress('Found no files in the Payroll Info folder', LOGIT+SENDIT)
			ENDIF

			* populate calculated fields that correspond to old kronos/adp values
			SELECT PAYROLLINFO
			SCAN

				IF PAYROLLINFO.CLIENTID = 3273300 THEN
					REPLACE PAYROLLINFO.ADP_COMP WITH 'E88' IN PAYROLLINFO && SALARIED
				ELSE
					REPLACE PAYROLLINFO.ADP_COMP WITH 'E87' IN PAYROLLINFO && HOURLY
				ENDIF

			ENDSCAN

			IF .lTestMode THEN
				SELECT PAYROLLINFO
				BROWSE
			ENDIF

			USE IN PAYROLLINFO

		ENDWITH
		RETURN
	ENDPROC  && GetPayrollInfo



	PROCEDURE ProcessPayrollInfoFile

		LPARAMETERS tcSourceFile

		LOCAL lnMaxRow, lcRow, lnRow, oWorkbook, oWorksheet, lcSourceFile, lnRowsProcessed

		WITH THIS

			lcSourceFile = tcSourceFile

			.TrackProgress('Loading Payroll Info File: ' + lcSourceFile, LOGIT+SENDIT)

			oWorkbook = .oExcel.workbooks.OPEN(lcSourceFile)

			oWorksheet = oWorkbook.Worksheets[1]

			lnRowsProcessed = 0

			lnMaxRow = 20000


			FOR lnRow = 8 TO lnMaxRow

				WAIT WINDOW NOWAIT 'Processing Payroll Info Spreadsheet Row: ' + TRANSFORM(lnRow)

				PRIVATE m.PAYDATE, m.NAME, m.CLIENTID, m.INSPID, m.STATUS, m.HOURLYRATE, m.GROSSPAY, m.NETPAY
				PRIVATE m.REGHOURS, m.REGPAY, m.OTHOURS, m.OTPAY, m.DTHOURS, m.DTPAY, m.BONUSPAY, m.COMMISSION
				PRIVATE m.SICKHOURS, m.SICKPAY, m.SPECPAY, m.VACHOURS, m.VACPAY, m.PTOHOURS, m.PTOPAY, m.CHECKNUM
				PRIVATE m.PAYROLLNUM, m.DIVISION, m.DIVNAME, m.BUSUNIT, m.DEPT, m.DEPTNAME, m.SPLIT2, m.SPLIT3
				PRIVATE m.OTHPTOHRS, m.OTHPTOPAY

				STORE "" TO m.NAME, m.STATUS, m.DIVISION, m.DIVNAME, m.BUSUNIT, m.DEPT, m.DEPTNAME, m.SPLIT2, m.SPLIT3

				STORE 0 TO m.CLIENTID, m.INSPID, m.HOURLYRATE, m.GROSSPAY, m.NETPAY, m.REGHOURS, m.REGPAY, m.OTHOURS, m.OTPAY
				STORE 0 TO m.DTHOURS, m.DTPAY, m.BONUSPAY, m.COMMISSION, m.SICKHOURS, m.SICKPAY, m.SPECPAY, m.VACHOURS, m.VACPAY
				STORE 0 TO m.PTOHOURS, m.PTOPAY, m.CHECKNUM, m.PAYROLLNUM, m.OTHPTOHRS, m.OTHPTOPAY

				STORE CTOD('') TO m.PAYDATE

				lcRow = ALLTRIM(STR(lnRow))

				m.PAYDATE = oWorksheet.RANGE("A"+lcRow).VALUE

				IF ( ISNULL(m.PAYDATE) OR EMPTY(m.PAYDATE) ) THEN
					* we are past the last data row! exit the loop...
					EXIT FOR
				ENDIF

				m.NAME = oWorksheet.RANGE("B"+lcRow).VALUE
				m.CLIENTID = oWorksheet.RANGE("C"+lcRow).VALUE
				m.INSPID = oWorksheet.RANGE("D"+lcRow).VALUE
				m.STATUS = LEFT(oWorksheet.RANGE("E"+lcRow).VALUE,1)
				m.HOURLYRATE = NVL(oWorksheet.RANGE("F"+lcRow).VALUE,0.00)
				m.GROSSPAY = NVL(oWorksheet.RANGE("G"+lcRow).VALUE,0.00)
				m.NETPAY = NVL(oWorksheet.RANGE("H"+lcRow).VALUE,0.00)
				m.REGHOURS = NVL(oWorksheet.RANGE("I"+lcRow).VALUE,0.00)
				m.REGPAY = NVL(oWorksheet.RANGE("J"+lcRow).VALUE,0.00)
				m.OTHOURS = NVL(oWorksheet.RANGE("K"+lcRow).VALUE,0.00)
				m.OTPAY = NVL(oWorksheet.RANGE("L"+lcRow).VALUE,0.00)
				m.DTHOURS = NVL(oWorksheet.RANGE("M"+lcRow).VALUE,0.00)
				m.DTPAY = NVL(oWorksheet.RANGE("N"+lcRow).VALUE,0.00)
				m.BONUSPAY = NVL(oWorksheet.RANGE("O"+lcRow).VALUE,0.00)
				m.COMMISSION = NVL(oWorksheet.RANGE("P"+lcRow).VALUE,0.00)
				m.SICKHOURS = NVL(oWorksheet.RANGE("Q"+lcRow).VALUE,0.00)
				m.SICKPAY = NVL(oWorksheet.RANGE("R"+lcRow).VALUE,0.00)
				m.SPECPAY = NVL(oWorksheet.RANGE("S"+lcRow).VALUE,0.00)
				m.VACHOURS = NVL(oWorksheet.RANGE("T"+lcRow).VALUE,0.00)
				m.VACPAY = NVL(oWorksheet.RANGE("U"+lcRow).VALUE,0.00)
				m.PTOHOURS = NVL(oWorksheet.RANGE("V"+lcRow).VALUE,0.00)
				m.PTOPAY = NVL(oWorksheet.RANGE("W"+lcRow).VALUE,0.00)
				m.CHECKNUM = NVL(oWorksheet.RANGE("X"+lcRow).VALUE,0.00)
				m.PAYROLLNUM = NVL(oWorksheet.RANGE("Y"+lcRow).VALUE,0.00)

				m.DIVISION = LEFT(NVL(oWorksheet.RANGE("Z"+lcRow).VALUE,'   '),2)
				m.DIVNAME = SUBSTR(NVL(oWorksheet.RANGE("Z"+lcRow).VALUE,'       '),6)

				m.DEPT = LEFT(NVL(oWorksheet.RANGE("AA"+lcRow).VALUE,'    '),4)
				m.DEPTNAME = NVL(oWorksheet.RANGE("AB"+lcRow).VALUE,' ')
				m.BUSUNIT = NVL(oWorksheet.RANGE("AC"+lcRow).VALUE,'   ')
				m.SPLIT2 = NVL(oWorksheet.RANGE("AD"+lcRow).VALUE,'  ')
				m.SPLIT3 = NVL(oWorksheet.RANGE("AE"+lcRow).VALUE,'    ')

				m.OTHPTOHRS = NVL(oWorksheet.RANGE("AJ"+lcRow).VALUE,0.00)
				m.OTHPTOPAY = NVL(oWorksheet.RANGE("AK"+lcRow).VALUE,0.00)

				INSERT INTO PAYROLLINFO FROM MEMVAR

				lnRowsProcessed = lnRowsProcessed + 1

			ENDFOR

			oWorkbook.CLOSE()

			.TrackProgress("Rows Processed = " + TRANSFORM(lnRowsProcessed), LOGIT+SENDIT)

		ENDWITH
		RETURN
	ENDPROC  && ProcessPayrollInfoFile



	FUNCTION ExecSQL
		LPARAMETERS tcSQL, tcCursorName, tlNoDataReturnedIsOkay
		LOCAL llRetVal, lnResult
		WITH THIS
			* close target cursor if it's open
			IF USED(tcCursorName)
				USE IN (tcCursorName)
			ENDIF
			WAIT WINDOW "Fetching remote data; please wait..." NOWAIT
			lnResult = SQLEXEC(.nSQLHandle, tcSQL, tcCursorName)
			llRetVal = ( lnResult > 0 )
			IF llRetVal THEN
				* see if any data came back
				IF NOT tlNoDataReturnedIsOkay THEN
					IF (NOT USED(tcCursorName)) OR EOF(tcCursorName) THEN
						llRetVal = .F.
						.TrackProgress('No data was returned by this query: ' +CRLF + tcSQL, LOGIT+SENDIT)
						.cSendTo = 'Mark Bennett <mark.bennett@tollgroup.com>'
					ENDIF
				ENDIF
			ELSE
				.TrackProgress("Result Code [" + TRANSFORM(lnResult) + "] returned by this query: " + tcSQL, LOGIT+SENDIT)
				.cSendTo = 'Mark Bennett <mark.bennett@tollgroup.com>'
			ENDIF
			WAIT CLEAR
			RETURN llRetVal
		ENDWITH
	ENDFUNC


	PROCEDURE TrackProgress
		* do any combination of Wait Window, writing to logfile, and adding to body of email,
		* based on nFlags parameter.
		LPARAMETERS tcExpression, tnFlags
		WITH THIS
			IF BITAND(tnFlags,LOGIT) = LOGIT THEN
				IF .lLoggingIsOn THEN
					?
					? .cProcessName + "  " + TTOC(DATETIME()) + ": " + tcExpression
				ENDIF
			ENDIF
			IF .lWaitWindowIsOn THEN
				IF BITAND(tnFlags,NOWAITIT) = NOWAITIT THEN
					WAIT WINDOW tcExpression NOWAIT
				ENDIF
				IF BITAND(tnFlags,WAITIT) = WAITIT THEN
					WAIT WINDOW tcExpression TIMEOUT .nWaitWindowTimeout
				ENDIF
			ENDIF
			IF BITAND(tnFlags,SENDIT) = SENDIT THEN
				IF .lSendInternalEmailIsOn THEN
					.cBodyText = .cBodyText + tcExpression + CRLF + CRLF
				ENDIF
			ENDIF
		ENDWITH
	ENDPROC  &&  TrackProgress


	FUNCTION GetSQLStartDateTimeString
		* returns a timestamp string for use in SQL queries
		LPARAMETERS tdDate
		LOCAL lcSetDate, lcSQLDateTimeString
		lcSetDate = SET('DATE')
		SET DATE YMD
		lcSQLDateTimeString   = "{ts '" + TRANSFORM(YEAR(tdDate)) + "-" + PADL(MONTH(tdDate),2,"0") + "-" + PADL(DAY(tdDate),2,"0") + " 00:00:00.0'}"
		SET DATE &lcSetDate.
		RETURN lcSQLDateTimeString
	ENDFUNC


	FUNCTION GetSQLEndDateTimeString
		* returns a timestamp string for use in SQL queries
		LPARAMETERS tdDate
		LOCAL lcSetDate, lcSQLDateTimeString
		lcSetDate = SET('DATE')
		SET DATE YMD
		lcSQLDateTimeString = "{ts '" + TRANSFORM(YEAR(tdDate)) + "-" + PADL(MONTH(tdDate),2,"0") + "-" + PADL(DAY(tdDate),2,"0") + " 23:59:59.9'}"
		SET DATE &lcSetDate.
		RETURN lcSQLDateTimeString
	ENDFUNC


	PROCEDURE SortArrayByDateTime
		* expects taInArray[] to be the result of an ADIR()
		LPARAMETERS taInArray, taOutArray
		EXTERNAL ARRAY taInArray, taOutArray
		LOCAL lnRows, lnCols, i, j
		* extending taInArray[] to process the files in date/time order
		lnRows = ALEN(taInArray,1)
		lnCols = ALEN(taInArray,2)
		* create taOutArray with one more column
		DIMENSION taOutArray[lnRows,lnCols + 1]
		* and fill it with taInArray columns AND (filedate + filetime) in new column
		* ( ACOPY() doesn't preserve elements properly because of different # of columns )
		FOR i = 1 TO lnRows
			FOR j = 1 TO lnCols
				taOutArray[i,j] = taInArray[i,j]
			ENDFOR
			taOutArray[i,lnCols + 1] = DTOS(taInArray[i,3]) + "_" + taInArray[i,4]
		ENDFOR
		* sort taOutArray on date/time column
		=ASORT(taOutArray,6)
	ENDPROC


ENDDEFINE




*!*		PROCEDURE GetPayDataForFTE
*!*
*!*			* THIS DOESN'T WORK BECAUSE THE ESC PAYROLL REPORTS CANNOT PROVIDE INFO FOR THE DRIVERS
*!*
*!*			LOCAL lnMaxRow, lcRow, lnRow, oWorkbook, oWorksheet, lcSourceFile, lnRowsProcessed
*!*			PRIVATE m.CLIENTID, m.PAYDATE, m.NAME, m.FILE_NUM, m.DIVISION, m.DEPT, m.COLLAR, m.TOLLCLASS
*!*
*!*			WITH THIS
*!*
*!*				* ZAP THE PAYDATAFORFTE TABLE
*!*				IF USED('PAYDATAFORFTE') THEN
*!*					USE IN PAYDATAFORFTE
*!*				ENDIF
*!*				USE F:\UTIL\INSPERITY\DATA\PAYDATAFORFTE IN 0 ALIAS PAYDATAFORFTE EXCLUSIVE
*!*				SELECT PAYDATAFORFTE
*!*				DELETE ALL
*!*				PACK
*!*
*!*			 	lcSourceFile = 'F:\UTIL\INSPERITY\ESCREPORTS\PAYROLL\PAYDATAFORFTE.XLS'
*!*
*!*				oWorkbook = .oExcel.workbooks.OPEN(lcSourceFile)
*!*
*!*				oWorksheet = oWorkbook.Worksheets[1]
*!*
*!*				lnRowsProcessed = 0

*!*				lnMaxRow = 20000
*!*

*!*				FOR lnRow = 9 TO lnMaxRow

*!*					WAIT WINDOW NOWAIT 'Processing Time Data Spreadsheet Row: ' + TRANSFORM(lnRow)
*!*
*!*					STORE "" TO m.NAME, m.FILE_NUM, m.DIVISION, m.DEPT, m.COLLAR, m.TOLLCLASS
*!*					STORE 0 TO m.CLIENTID
*!*					STORE CTOD('') TO m.PAYDATE

*!*					lcRow = ALLTRIM(STR(lnRow))
*!*
*!*					m.CLIENTID = oWorksheet.RANGE("A"+lcRow).VALUE
*!*
*!*					IF ( ISNULL(m.CLIENTID) OR EMPTY(m.CLIENTID) ) THEN
*!*						* we are past the last data row! exit the loop...
*!*						EXIT FOR
*!*					ENDIF
*!*
*!*					m.PAYDATE = oWorksheet.RANGE("B"+lcRow).VALUE
*!*
*!*					m.NAME = oWorksheet.RANGE("C"+lcRow).VALUE

*!*					m.FILE_NUM = ALLTRIM(TRANSFORM(oWorksheet.RANGE("D"+lcRow).VALUE))

*!*					m.DIVISION = LEFT(NVL(oWorksheet.RANGE("E"+lcRow).VALUE,'  '),2)
*!*
*!*					m.DEPT = LEFT(NVL(oWorksheet.RANGE("F"+lcRow).VALUE,'    '),4)
*!*
*!*					m.COLLAR = UPPER(LEFT(NVL(oWorksheet.RANGE("G"+lcRow).VALUE,' '),1))
*!*
*!*					m.TOLLCLASS = LEFT(NVL(oWorksheet.RANGE("H"+lcRow).VALUE,'  '),2)

*!*					INSERT INTO PAYDATAFORFTE FROM MEMVAR
*!*
*!*					lnRowsProcessed = lnRowsProcessed + 1

*!*				ENDFOR

*!*				oWorkbook.CLOSE()

*!*				.TrackProgress("In GetPayDataForFTE, lnRowsProcessed = " + TRANSFORM(lnRowsProcessed), LOGIT+SENDIT)

*!*				* populate calculated fields that correspond to old kronos/adp values
*!*				SELECT PAYDATAFORFTE
*!*				SCAN
*!*
*!*					IF PAYDATAFORFTE.CLIENTID = 3273300 THEN
*!*						REPLACE PAYDATAFORFTE.ADP_COMP WITH 'E88' IN PAYDATAFORFTE && SALARIED
*!*					ELSE
*!*						REPLACE PAYDATAFORFTE.ADP_COMP WITH 'E87' IN PAYDATAFORFTE && HOURLY
*!*					ENDIF
*!*
*!*				ENDSCAN
*!*
*!*				SELECT PAYDATAFORFTE
*!*				BROWSE
*!*
*!*				USE IN PAYDATAFORFTE
*!*
*!*			ENDWITH
*!*			RETURN
*!*		ENDPROC  && GetPayDataForFTE
