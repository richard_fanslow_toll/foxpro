Parameters lcBol
set step On 

Public lcOrder,lcOrdDetailsAndCartons,lcCartonOut,gnBOL_CtnQty

ldCreatedatetime = Datetime()
*close data all

***************************** Check to see if we need to create this file **********************************
** OK first need to check to see if the items on this BOL require an ASN, only shipmenst going
** to the UA DCs are candidates for this file, otherwise only an event file is sent
** get the outship record
lcquery = [select outshipid,shipins from outship where accountid = 5687 and bol_no = ']+lcBol+[']
xsqlexec(lcquery,"xpotype",,"wh")

If Reccount("xpotype") = 0
  =Messagebox("POType search error!! call IT (1) ",0,"Under Armour Bypass ASN file Creation")
  Return
Endif

** then get an outdet record, need the IBD as the IBD is in the color field in outdet
lcquery = [select color from outdet where outshipid=]+Transform(xpotype.outshipid)
xsqlexec(lcquery,"xpotype",,"wh")

If Reccount("xpotype") = 0
  =Messagebox("POType search error!! call IT (2)",0,"Under Armour Bypass ASN file Creation")
  Return
Endif

** then look in ctnucc for that IBD,  IBD is stored in the reference field
lcquery = [select top 1 suppdata from ctnucc where reference = ']+Alltrim(xpotype.Color)+[']
xsqlexec(lcquery,"xpotype",,"wh")

** pull back the suppdata memo field, POTYPE is in this field
If Reccount("xpotype") = 0
  =Messagebox("POType search error!! call IT (3)",0,"Under Armour Bypass ASN file Creation")
  Return
Endif

** create this ASN file only for the 2 POTypes below........
Select xpotype
xlcPotype = getmemodata("suppdata","POTYPE")

If !Inlist(xlcPotype,"YCON","ZNB")
   do ua_outbound_bypass With lcBol
   return
Endif

***************************** OK, This BOL requires the ASN file  **********************************
lcquery = lcBol
lcquery = [select ship_ref,0000 as ctnqty,Space(4) as scac, Space(15) as trailer,Space(15) as ponum,Space(12) as ibd,Space(15) as seal,outshipid from outship where accountid = 5687 and bol_no = ']+lcBol+[']
xsqlexec(lcquery,"xpos",,"wh")

Select xpos
Scan
  Replace ship_ref With Substr(ship_ref,1,10) In xpos
Endscan

Select * From xpos Group By ship_ref Into Cursor xpos Readwrite


Select xpos
Scan
  lcquery = [select style,color from outdet where accountid = 5687 and outshipid = ]+Transform(xpos.outshipid)
  xsqlexec(lcquery,"xpodet",,"wh")
  Replace xpos.ibd   With xpodet.Color In xpos
  Replace xpos.ponum With xpodet.Style In xpos
Endscan

lcquery = [select scac,trailer,seal,container,totqty,blid from bl where accountid = 5687 and bol_no =']+lcBol+[']
xsqlexec(lcquery,"xbl",,"wh")

If Empty(xbl.scac) Or (Empty(xbl.trailer) And Empty(xbl.Container))
  =Messagebox("UA File cannot be created, need a SCAC and Trailer or container value......."+Chr(13)+"Operation Aborted.......",0,"UA Outbound File Creation")
  Return
Else
  lcScac = xbl.scac
  lcTrailer = xbl.trailer
Endif
***************************** OK now we create the file ********************************************************88
lcquery = [select * from bldet where accountid = 5687 and blid = ]+Transform(xbl.blid)
xsqlexec(lcquery,"xbldet",,"wh")

Select Sum(detqty) As ctnqty,Substr(ship_ref,1,10) As ponum From xbldet Into Cursor poqtys Group By 2

gnBOL_CtnQty = xbl.totqty

Select xpos
Replace All scac    With xbl.scac In xpos
Replace All trailer With xbl.trailer In xpos
If Empty(trailer)
  Replace All trailer With xbl.Container In xpos
Endif
Replace All seal    With xbl.seal In xpos

Select xpos
Scan
  Select poqtys
  Locate For Alltrim(ponum) = Alltrim(xpos.ponum)
  Replace xpos.ctnqty With poqtys.ctnqty In xpos
Endscan

Select Sum(ctnqty) As qty ,ibd From xpos Into Cursor ibdqty Group By ibd

*Scan outship For lcBol by

If !Used("uaoutbounds")
  Use F:\3pl\Data\uaoutbounds In 0
Endif


If !Used("ua_counter")
  Use F:\3PL\Data\UA_COUNTER In 0
Endif

lnCounter = UA_COUNTER.seqnum
Replace UA_COUNTER.seqnum With UA_COUNTER.seqnum+1 In UA_COUNTER

Select xpos
Go Top

** removed below by Juan, 02/13/2018....
*lcScac = xpos.scac
*lcTrailer = xpos.trailer

Select * From xpos Group By ibd Into Cursor just_ibds Readwrite

Select just_ibds
Scan
  Select ibdqty
  Locate For Alltrim(ibd) = Alltrim(just_ibds.ibd)
  Replace just_ibds.ctnqty With ibdqty.qty In just_ibds
Endscan

lnRun= 1

Select just_ibds
Go Top

*set step On
Scan
**  lcQuery = "select * from ctnucc where accountid = 5687 and reference ='"+ALLTRIM(xpos.ibd)+"'"

  lcquery = "select * from ctnucc where accountid = 5687 and reference ='"+Alltrim(just_ibds.ibd)+"' and ponum = '"+Alltrim(just_ibds.ponum)+"'"

**in case need to trigger again for a specific palletid - 2 other places need to be un commented
*lcQuery = "select * from ctnucc where accountid = 5687 and reference ='"+Alltrim(xibds.ibd)+"' and outwonum = "+Transform(Manifestnum)+" and palletid = 'PLT010693'"

  xsqlexec(lcquery,"xctn",,"wh")
  Select xctn
  lcASNNumber = getmemodata("suppdata","ASN")
  num1= Reccount("xctn")
* replace ctnqty With num1 In just_ibds

  Select xctn
  Go Top

  gcDocNumber = xctn.Reference

  lnNumCtns = num1
  lnNumCtns = Padl(Alltrim(Transform(lnNumCtns)),5,"0")
&& now for the header of the combined file

* Replace all the predefined data elements
  Set Date YMD
  Set Century On

  lcdate =Strtran(Dtoc(Date()),"/","")

  Select xctn
  Go Top
  lcShipToID = xctn.shipid
  lcShipFrom = Alltrim(whseloc)
***lnNumCtns = Padl(Alltrim(Transform(Reccount("xctn"))),5,"0")

**  lcDocnumber = xpos.ibd
  lcDocnumber = just_ibds.ibd

  lcHeader = Filetostr("f:\underarmour\xmltemplates\transload_bypass_detail_header.txt")

  lcHeader = Strtran(lcHeader,"<EXTDOCNUMBER>",Alltrim(Transform(lcDocnumber)))
  lcHeader = Strtran(lcHeader,"<DOCNUMBER>",Alltrim(Transform(lcDocnumber))+"_"+Alltrim(just_ibds.ibd) )
  lcHeader = Strtran(lcHeader,"<SUBBOL>",Alltrim(lcBol))
  lcHeader = Strtran(lcHeader,"<CARRIER>",Alltrim(lcScac))  && Needs to be a 4 digit SCAC code
  lcHeader = Strtran(lcHeader,"<TRAILER>",Alltrim(lcTrailer))
  lcHeader = Strtran(lcHeader,"<DOCTYPE>","IBDUPD")  && this for Bypass only
  lcHeader = Strtran(lcHeader,"<NUMOFCTNS>",Transform(just_ibds.ctnqty))
  lcHeader = Strtran(lcHeader,"<NUMBOLCTNS>",Transform(gnBOL_CtnQty))
  lcHeader = Strtran(lcHeader,"<SHIPFROMID>",Alltrim(lcShipFrom))
  lcHeader = Strtran(lcHeader,"<SHIPTOID>",Alltrim(lcShipToID))
  lcHeader = Strtran(lcHeader,"<SHIPDATE>",lcdate)
****************************************************************************************************************
  lcOutStr = lcHeader
  lcEndOrder = Filetostr("f:\underarmour\xmltemplates\transload_outbound_endorder.txt")


  lcOrder=""
  lcOrdDetailsAndCartons=""
  lcCartonOut =""

  m.podata =""

  Select xpos
  Scan For xpos.ibd = just_ibds.ibd
    Do transload_OrderData With Alltrim(just_ibds.ibd),Alltrim(xpos.ponum)
    xxorder = lcOrder
    lcOutStr = lcOutStr+lcOrder+lcOrdDetailsAndCartons
    lcOutStr = lcOutStr &&+lcEndOrder  && close the high level Order loop
  Endscan

  cDate = Dtos(Date())
  cTruncDate = Right(cDate,6)
  cTruncTime = Substr(Ttoc(Datetime(),1),9,6)
  cfiledate = cDate+cTruncTime
  lcFooter = Filetostr("f:\underarmour\xmltemplates\transload_outbound_footer.txt")

  lcOutStr = lcOutStr+lcFooter
  lnCtnQty = Occurs("<Carton>",lcOutStr)

  xxfilename = "F:\FTPUSERS\UNDERARMOUR\OUTBOUND\WMSSHP_"+Alltrim(gcDocNumber)+"_"+cfiledate+".XML"
*  xxfilename = "F:\FTPUSERS\UNDERARMOUR\OUTBOUND\SPECIAL\WMSSHP_"+ALLTRIM(gcDocNumber)+"_"+cfiledate+".XML"
  Strtofile(lcOutStr,Upper(xxfilename))

  Wait Window At 10,10 "Creating file: "+xxfilename Timeout 0.5

*****************************************************************************************************************
lnCtnQty = Occurs("<Carton>",lcOutStr)
lnPOQty = Occurs("<OrderDetails>",lcOutStr)
Wait Window At 10,10 "Creating file: "+xxfilename Timeout 1

Select uaoutbounds
m.bol = lcBol
m.scac = just_ibds.scac
m.seal = just_ibds.seal
m.ibd =Alltrim(gcDocNumber)
m.shipdt = Date()
m.shiptm = Datetime()
m.shipto = lcShipToID
m.shipfrom= lcShipFrom
m.qty =  num1
m.trailer =just_ibds.trailer
m.carrier = just_ibds.scac
m.filename = xxfilename
m.poqty =lnPOQty
m.ctnqty =lnCtnQty
m.adddt = ldCreatedatetime
m.podata = "CTNS*"+Transform(lnCtnQty)
Insert Into uaoutbounds From Memvar
lnRun = lnRun+1
Endscan

Select uaoutbounds
Sum ctnqty To bltotal For bol = lcBol And adddt = ldCreatedatetime

Replace bol_total With bltotal For bol = lcBol

Insert Into F:\edirouting\sftpjobs.Dbf (jobname, Userid) Values ('PUT_XML_TO_UNDERARMOUR','PGAIDIS')

*!*  USE IN xibds
*!*  USE IN manifest
*!*  USE IN uaoutbounds
*!*  USE IN sftpjobs

*!*  lIsError = .F.
*!*  ediupdate("XML CREATED",lIsError)

*********************************************************************************************************8
Procedure transload_OrderData

Parameters IBDnum,xxponum

m.podata = ""
lcquery = "select * from ctnucc where accountid = 5687 and reference ='"+Alltrim(IBDnum)+"'"+ " and ponum = '"+ xxponum+"'"

xsqlexec(lcquery,"xctn",,"wh")

**in case need to trigger again for a specific palletid - 2 other places need to be un commented
*xsqlexec("select * from ctnucc where accountid = 5687 and reference ='"+Alltrim(xibds.ibd)+"' and outwonum = "+Transform(Manifestnum)+" and palletid = 'PLT010693'","xctn",,"wh")

Select xctn

lcOrder = Filetostr("f:\underarmour\xmltemplates\transload_outbound_order.txt")
lcOrder = Strtran(lcOrder,"<IBDORDER>",xctn.Reference)
lcOrder = Strtran(lcOrder,"<ASNTYPE>",xctn.upc)
lcOrder = Strtran(lcOrder,"<CUSTOMERPO>",Alltrim(xxponum))

lcOrdDetailsAndCartons=""

Select xctn
*set step On
Select ponum,serialno As linenum From xctn  Where ponum In (Select ponum From xpos) Into Cursor thesepos Group By ponum,linenum

Select thesepos
Scan
  m.podata = m.podata+"PO*"+Alltrim(thesepos.ponum)+Chr(13)+"POLINE*"+thesepos.linenum+Chr(13)
  Select xctn
  Locate For ponum = thesepos.ponum And serialno = thesepos.linenum
  lcOrderDetails = xctn.orddetails
  lccarton = Filetostr("f:\underarmour\xmltemplates\transload_outbound_carton.txt")
  lcCartonOut = ""

****

*set step On
  Select xctn
  lnCtn = 0
  Scan For ponum = thesepos.ponum And serialno = thesepos.linenum
    lcCartonOut = lcCartonOut+xctn.ctndetails
    lnCtn=lnCtn+1
  Endscan
  m.podata = m.podata+"POQTY*"+Alltrim(Transform(lnCtn))+Chr(13)

  lcOrdDetailsAndCartons=lcOrdDetailsAndCartons+lcOrderDetails+Chr(13)+lcCartonOut
  lcOrderDetails=""
  lcCartonOut=""
Endscan

Endproc
***************************************************************************************************

