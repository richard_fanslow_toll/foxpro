PARAMETERS cOffice
*SET STEP ON 
cSrvr = "tgfnjsql01"
SELECT 0
USE F:\wh\sqlpassword
cSQLPass = ALLTRIM(sqlpassword.PASSWORD)
USE IN sqlpassword

lcDSNLess="driver=SQL Server;server=&csrvr;uid=SA;pwd=&cSQLPass;DATABASE=PICKPACK"
nHandle=SQLSTRINGCONNECT(lcDSNLess,.T.)
SQLSETPROP(nHandle,"DispLogin",3)

IF nHandle<1 && bailout
	WAIT WINDOW "Could not make SQL connection" TIMEOUT 3
	cxMessage = "NO SQL CONNECT"
	THROW
ENDIF

SELECT cutuccs
LOCATE
*BROWSE TIMEOUT 60
cWaitstr = "Removing "+TRANSFORM(RECCOUNT())+" records"
WAIT WINDOW cWaitstr TIMEOUT 2
*SET STEP ON 
scan
	if usesql()
		xsqlexec("update cartons set ucc='CUTS' where cartonnum='"+cucc+"' and wo_num="+cwo,,,"pickpack")
	else
		cUCC = ALLTRIM(cutuccs.ucc)
		cWO = TRANSFORM(cutuccs.wo_num)
		WAIT WINDOW "Now updating carton: "+cUCC NOWAIT
		lcQ1 = [update dbo.cartons]
		lcQ2 = [ SET UCC = 'CUTS' ]
		lcQ3 = [ WHERE CARTONNUM = ]
		lcQ4 = " '&cUCC' "
		lcQ5 = [ AND WO_NUM = ]
		lcQ6 = " &cWO "
		lcsql = lcQ1+lcQ2+lcQ3+lcQ4+lcQ5+lcQ6

		llSuccess=SQLEXEC(nHandle,lcsql)
		IF llSuccess<1  && no records 0 indicates no records and 1 indicates records found
			ASSERT .F. MESSAGE "At NO RECORDS statement in SQLConnect"
			WAIT WINDOW "No "+cCustName+" "+cOffice+" records found" TIMEOUT 2
		endif
	endif

	SQLCANCEL(nHandle)
	SQLDISCONNECT(0)
ENDSCAN

WAIT WINDOW "SQL UCC update complete...exiting" TIMEOUT 2
RETURN
