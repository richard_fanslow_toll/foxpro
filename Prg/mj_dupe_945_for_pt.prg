* This program identifies when a PT has multiple BOL's attached to it.  Usually caused when a bol is pulled back by shipping and the PT is assigned a new PT.
* 945 is then sent to MJ for both (W to R, 945 and vouchers/slips)
utilsetup("MJ_DUPE_945_FOR_PT")
SET EXCLUSIVE OFF 
SET DELETED ON 
SET TALK OFF  
SET SAFETY OFF   &&Allows for overwrite of existing files

close databases all


If !Used("edi_trigger")
  use f:\3pl\data\edi_trigger SHARED IN 0 
ENDIF

SELECT ship_ref, CNT(1) FROM edi_trigger WHERE edi_type='945 ' AND accountid=6303 AND when_proc >{09/01/2014} GROUP BY 1 INTO CURSOR temp99 READWRITE
SELECT * FROM temp99 WHERE cnt_exp_2 >1 INTO CURSOR temp98 READWRITE
SELECT * FROM temp98 t LEFT JOIN edi_trigger e ON t.ship_ref=e.ship_ref AND E.ACCOUNTID=6303 INTO CURSOR temp55 READWRITE
SELECT distinct ship_ref_a as ship_ref, MAX(when_proc) FROM temp55 GROUP BY 1 INTO CURSOR temp56
SELECT ship_ref FROM temp56 WHERE max_when_proc > {01/01/2014} into CURSOR temp57
SELECT * FROM temp57 a LEFT JOIN edi_trigger b ON a.ship_ref=b.ship_ref AND b.accountid=6303 INTO CURSOR temp58
 
	If Reccount() > 0 
**		export TO "S:\MarcJacobsData\TEMP\GSI_RETURN _" +Ttoc(Datetime())   TYPE xls
		export TO "S:\MarcJacobsData\TEMP\mj_dupe_945_for_pt"  TYPE xls
		tsendto = "tmarg@fmiint.com"
		tattach = "S:\MarcJacobsData\TEMP\\mj_dupe_945_for_pt.xls" 
		tcc =""                                                                                                                               
		tFrom ="Toll EDI System Operations <fmi-transload-ops@fmiint.com>"                                
		tmessage = "DUPLICATE 945 for PT exists:  "+Ttoc(Datetime())        
		tSubject = "DUPLICATE 945 for PT exists"                                                           
		Do Form m:\dev\frm\dartmail2 With tsendto,tFrom,tSubject, tcc,tattach,tmessage  ,"A"     
			
	ELSE 
*!*		
*!*			tsendto = "tmarg@fmiint.com"
*!*			tattach = "" 
*!*			tcc =""                                                                                                                               
*!*			tFrom ="Toll EDI System Operations <fmi-transload-ops@fmiint.com>"                                
*!*			tmessage = "NO DUPLICATE 945 for PT exists  "+Ttoc(Datetime())        
*!*			tSubject = "NO DUPLICATE 945 for PT exists"                                                           
*!*			Do Form m:\dev\frm\dartmail2 With tsendto,tFrom,tSubject, tcc,tattach,tmessage  ,"A"   

	ENDIF
close data all 

Wait window at 10,10 "all done...." timeout 2


schedupdate()
_screen.Caption=gscreencaption
on error