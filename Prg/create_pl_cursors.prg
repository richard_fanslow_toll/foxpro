PARAMETERS cUseFolder,llSQLTesting

&&    llSqlTesting = .f.  && not ready for prime time yet

IF USED("inwolog")
USE IN inwolog
endif
IF USED("pl")
USE IN pl
endif

IF llSQLTesting
	xsqlexec("select * from inwolog where .f.","xinwolog",,"wh")
	SELECT xinwolog
	xsqlexec("select * from pl where .f.","xpl",,"wh")
	SELECT xpl
ELSE
*!* Set up temp structure for PL
	USE (cUseFolder+"pl") IN 0 SHARED NOUPDATE
	SELECT ptdet
	SELECT * FROM pl WHERE .F. INTO CURSOR xpl READWRITE
	USE IN pl

*!* Set up temp structure for INWOLOG
	USE (cUseFolder+"inwolog") IN 0 SHARED NOUPDATE
	SELECT inwolog
	SELECT * FROM inwolog WHERE .F. INTO CURSOR xinwolog READWRITE
	SELECT xiwolog
	INDEX ON inwologid TAG inwologid
	USE IN inwolog

ENDIF

SELECT xinwolog
