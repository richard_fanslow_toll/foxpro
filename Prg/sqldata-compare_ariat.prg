*!* This program cross-checks the record counts for
*!* the SQL LABELS and CARTONS tables by BOL/PT
*!* to address a prior Courtaulds SQL problem

PARAMETERS cUseFolder,cBOL,nWO_Num,cOffice,nAcctNum,cPPName,lSQL3,lMoret
PUBLIC cFileType1,lLabelRecs
SET TALK OFF
SET ASSERTS ON
SET ESCAPE ON
ON ESCAPE CANCEL

*!* nDoVar = 2, will change WO#
*!* nDoVar = 3, will change Carton count
nDoVar = 0
lError = .F.
lLabelRecs = .F.
*ASSERT .F. MESSAGE "At SQL-COMPARE BOL selection"
goffice = cOffice

*SET STEP ON 
IF USED('outship')
USE IN outship
ENDIF

xsqlexec("select * from outship where bol_no = '"+cBOL+"' and accountid = "+TRANSFORM(nAcctNum),,,"wh")

SELECT bol_no,ship_ref ;
	FROM outship WHERE bol_no = cBOL ;
	AND accountid = nAcctNum ;
	AND !EMPTY(bol_no) ;
	and !EMPTYnul(del_date) ;
	GROUP BY 1,2 ;
	ORDER BY 1,2 ;
	INTO DBF F:\3pl\DATA\sqlptcomp
IF lTesting
	*	BROWSE
ENDIF

cFileOutName1 = "L"+TRIM(cPPName)
cFileOutName2 = "C"+TRIM(cPPName)
nAcct = ALLTRIM(STR(nAcctNum))
cSQL = "tgfnjsql01"

SELECT 0
USE F:\wh\sqlpassword
cSQLPass = ALLTRIM(sqlpassword.PASSWORD)
USE IN sqlpassword

lcDSNLess="driver=SQL Server;server=tgfnjsql01;uid=SA;pwd=&cSQLPass;DATABASE=PICKPACK"

nHandle=SQLSTRINGCONNECT(m.lcDSNLess,.T.)
SQLSETPROP(nHandle,"DispLogin",3)
WAIT WINDOW "Now processing "+cPPName+" SQL view...please wait" NOWAIT

IF nHandle=0 && bailout
	WAIT WINDOW "Could not make SQL connection" TIMEOUT 3
	closefiles()
	cRetMsg = "NO SQL CONNECT"
	RETURN cRetMsg
ENDIF

*!* Scans through all PTs within the OUTSHIP BOL#
IF lTesting
*SET STEP ON
endif 
SELECT sqlptcomp
LOCATE
IF lTesting
*BROWSE
endif
SCAN
	cPT1 = ALLTRIM(sqlptcomp.ship_ref)
	lcQ3 = " '&cPT1' "
	WAIT WINDOW "Now scanning PT# "+cPT1 NOWAIT

	if usesql()
		xsqlexec("select * from labels where ship_ref='"+cpt1+"'",cFileOutName1,,"pickpack")
		llsuccess=1
	else
		lcQ1=[SELECT * FROM dbo.labels Labels]
		lcQ2 = [ WHERE Labels.ship_ref = ]
		lcsql = lcQ1+lcQ2+lcQ3
		llSuccess=SQLEXEC(nHandle,lcsql,cFileOutName1)
	endif
	
	successrun("L")

	IF lError
		IF lLabelRecs
			cRetMsg = "LR"
		ELSE
			cRetMsg = "SQL ERROR-LBL"
		ENDIF
		RETURN cRetMsg
	ENDIF

	if usesql()
		xsqlexec("select * from cartons where ship_ref='"+cpt1+"'",cFileOutName2,,"pickpack")
		llsuccess=1
	else
		lcQ1 = [SELECT * FROM dbo.cartons Cartons]
		lcQ2 = [ WHERE Cartons.ship_ref = ]
		lcsql = lcQ1+lcQ2+lcQ3
		llSuccess=SQLEXEC(nHandle,lcsql,cFileOutName2)
	endif
	
	successrun("C")

	IF lError
		cRetMsg = "SQL ERROR-CTN"
		RETURN cRetMsg
	ENDIF

	SELECT &cFileOutName1 && Labels
	COUNT TO nLabels
	cCountLabels = ALLTRIM(STR(nLabels))
	SELECT &cFileOutName2 && Cartons
	COUNT TO nCartons
	IF nDoVar = 3 AND lTesting
		nCartons = nCartons - 5
	ENDIF
	cCountCartons = ALLTRIM(STR(nCartons))

	IF nLabels <> nCartons && Mismatched count
		IF lSQL3 AND (cOffice = "N" AND nAcctNum=5446 OR cOffice = "C" AND lMoret) AND nLabels = 0
			WAIT WINDOW "Correct zero-label COUNT(MORET SQL3 data)" NOWAIT
		ELSE
			errormail(1)
			cRetMsg = "LBL/CTN UNEQUAL"
			RETURN cRetMsg
		ENDIF
	ENDIF
ENDSCAN

WAIT WINDOW "Labels and Cartons had equal record counts...continuing" NOWAIT

SQLCANCEL(nHandle)
SQLDISCONNECT(nHandle)
closefiles()

cRetMsg = "OK"
RETURN cRetMsg

**********************
PROCEDURE successrun
	PARAMETERS cFileType1
	cFileWithError = IIF(cFileType1="C","CARTONS","LABELS")
	DO CASE
		CASE INLIST(llSuccess,0,-1)  && no records 0 indicates no records and 1 indicates records found
			WAIT WINDOW "No SQL Connection (sqldata-compare)" TIMEOUT 3
			errormail(2)
			RETURN
		CASE RECCOUNT()=0
			IF lSQL3 AND (cOffice = "C" AND lMoret OR cOffice = "N" AND INLIST(nAcctNum,5446))
				WAIT WINDOW "No records correctly found in LABELS - MORET" NOWAIT
			ELSE
				ASSERT .F. MESSAGE "Zero Record Count flagged in "+cFileWithError+"...debug"
				WAIT WINDOW "No "+cPPName+" records found for this pt# "+ALLTRIM(cPT1) TIMEOUT 3
				errormail(3)
				RETURN
			ENDIF
		CASE RECCOUNT()>0
			IF lSQL3 AND cOffice = "C" AND lMoret AND cFileType1 = "L"
				WAIT WINDOW cPPName+" LABELS records were found for this PT# "+ALLTRIM(cPT1) TIMEOUT 3
				lLabelRecs = .T.
				errormail(4)
				RETURN
			ENDIF
	ENDCASE
ENDPROC

**********************
PROCEDURE errormail
	PARAMETERS nType
	lError = .T.
	DO CASE
		CASE nType = 1
			tmessage = "The SQL counts (Labels vs. Cartons) did not match for this BOL."
			tmessage = tmessage + CHR(13) +;
				"Count at PT# "+cPT1+": "+cCountLabels+" (Labels) <> "+cCountCartons+" (Cartons)."
			tmessage = tmessage + CHR(13) + "Office: "+cOffice+", Account: "+ALLTRIM(STR(nAcctNum))
		CASE nType = 2
			tmessage = "SQL could not properly connect while processing PT "+cPT1
			tmessage = tmessage + CHR(13) + "Office: "+cOffice+", Account: "+ALLTRIM(STR(nAcctNum))
		CASE nType = 3
			tmessage = "No records were found in the SQL data for this BOL at PT# "+cPT1
			tmessage = tmessage + CHR(13) + "Office: "+cOffice+", Account: "+ALLTRIM(STR(nAcctNum))
			tmessage = tmessage + CHR(13) + "Error occurred in "+cFileWithError
		OTHERWISE
			tmessage = "Records were found in the MORET SQL LABELS for this BOL at PT# "+cPT1
			tmessage = tmessage + CHR(13) + "Office: "+cOffice+", Account: "+ALLTRIM(STR(nAcctNum))
			tmessage = tmessage + CHR(13) + "Error occurred in "+cFileWithError
			tmessage = tmessage + CHR(13) + "THIS ERROR MUST BE CORRECTED IMMEDIATELY."
			stopmoretwo()
	ENDCASE
	IF nType = 2
		tmessage = tmessage + CHR(13) + "Processing stopped at PT# "+cPT1
	ENDIF

	assert .f. MESSAGE "At Compare Mailing process"
	tfrom = "TGF Warehouse Operations <warehouse-ops@fmiint.com>"
	tsubject = "SQL ERROR(Labels-Cartons) at BOL#: "+cBOL+", PT# "+ALLTRIM(cPT1)
	USE F:\3pl\DATA\mailmaster ALIAS mm IN 0
	SELECT mm
	LOCATE FOR mm.accountid = 9997 AND mm.taskname = "ADDMIKE"
	tsendto = IIF(mm.use_alt,mm.sendtoalt,mm.sendto)
	tcc = IIF(mm.use_alt,mm.ccalt,mm.cc)
	USE IN mm

	tattach = " "
	DO FORM m:\dev\frm\dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
	closefiles()
	SQLCANCEL(nHandle)
	SQLDISCONNECT(nHandle)
ENDPROC

**********************
PROCEDURE closefiles
	IF USED(cFileOutName1)
		USE IN &cFileOutName1
	ENDIF
	IF USED(cFileOutName2)
		USE IN &cFileOutName2
	ENDIF
	IF USED('sqlptcomp')
		USE IN sqlptcomp
	ENDIF
	RETURN

	**********************
PROCEDURE stopmoretwo
	IF !lTesting
		SELECT edi_trigger
		LOCATE
		REPLACE processed WITH .T.,errorflag WITH .T.,fin_status WITH "RECS IN LABELS" ;
			FOR edi_trigger.wo_num = nWO_Num AND edi_trigger.accountid = nAcctNum
		LOCATE
	ENDIF
ENDPROC
