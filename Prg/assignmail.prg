PARAMETERS cEDI_type,cOffice,nAcctNum
*!* i.e.-"940","C",4610
*!* Used to assign mail and folder info to EDI processes

IF VARTYPE(cEDI_type) = "N"
	cEDI_type = ALLTRIM(STR(cEDI_type))
ENDIF

SELECT 0
USE F:\3pl\DATA\mailmaster ALIAS mm SHARED
LOCATE FOR edi_type = cEDI_type ;
	AND mm.office = cOffice ;
	AND mm.accountid = nAcctNum
IF FOUND()
	IF !EMPTY(mm.acctname)
		STORE TRIM(mm.acctname) TO cAccountName
	ENDIF
	IF !EMPTY(mm.basepath)
		STORE TRIM(mm.basepath) TO lcPath
	ENDIF
	IF !EMPTY(mm.archpath)
		STORE TRIM(mm.archpath) TO lcArchivePath
	ENDIF
	tsendto = IIF(lUseAlt,mm.sendtoalt,mm.sendto)
	tcc = IIF(lUseAlt,mm.ccalt,mm.cc)
	STORE TRIM(mm.scaption) TO thiscaption
	STORE mm.CtnAcct        TO lCartonAcct
	LOCATE
	LOCATE FOR (mm.accountid = 9999) AND (mm.office = "X")
	lUseAlt = mm.use_alt
	tsendtoerr = IIF(lUseAlt,mm.sendtoalt,mm.sendto)
	tccerr = IIF(lUseAlt,mm.ccalt,mm.cc)
	USE IN mm
	LOCATE
	LOCATE FOR mm.edi_type = "MISC" AND mm.taskname = "GENERAL"
	lUseAlt = mm.use_alt
	tsendtotest = IIF(lUseAlt,mm.sendtoalt,mm.sendto)
	tcctest = IIF(lUseAlt,mm.ccalt,mm.cc)
	USE IN mm
ELSE
	WAIT WINDOW "Error: Mail info not found" TIMEOUT 2
	LOCATE
	LOCATE FOR mm.accountid = 9999 AND mm.office = "X"
	lUseAlt = mm.use_alt
	tsendto = IIF(lUseAlt,mm.sendtoalt,mm.sendto)
	tcc = IIF(lUseAlt,mm.ccalt,mm.cc)
	USE IN mm
	tattach = ""
	tFrom ="TGF EDI Operations <toll-edi-ops@tollgroup.com>"
	tsubject= "TGF EDI Mail Assignment Error - "+TTOC(DATETIME())
	tmessage = "EDI Type "+cEDI_type+", Office "+cOffice+", Acct# "+nAcctNum+CHR(13)
	tmessage = tmessage + "Not found in F:\3PL\DATA\MAILMASTER.DBF"+CHR(13)
	tmessage = tmessage + "Please check and correct table."
	DO FORM m:\dev\frm\dartmail2 WITH tsendto,tFrom,tsubject,tcc,tattach,tmessage,"A"
	NormalExit = .F.
	THROW
ENDIF
