* this report is designed to be run at the beginning of the month
* It will tell HR which employees have their resident alien statuses expiring Next month (or earlier)

runack("RA")

#DEFINE CRLF CHR(13) + CHR(10)

SET EXCLUSIVE OFF
SET SAFETY OFF
SET DELETED ON

IF USED('curAlien') THEN
	USE IN curAlien
ENDIF

IF FILE("F:\UTIL\HR\RSDNTALIEN\REPORTS\RSDTNALIEN.XLS") THEN
	DELETE FILE F:\UTIL\HR\RSDNTALIEN\REPORTS\RSDNTALIEN.XLS
ENDIF

* get 1st of (this month + 2)
LOCAL ldToday, lcMonth, lcYear, ldFirstOfThisMonthPlus2, lcComputer
ldToday = DATE()
lcComputer = GETENV("COMPUTERNAME")


*!*	** for testing
*!*	ldToday = {^2004-06-01}

lcMonth = PADL(MONTH(ldToday),2,"0")
lcYear = TRANSFORM(YEAR(ldToday))
ldFirstOfThisMonthPlus2 = GOMONTH(CTOD(lcMonth + "/01/" + lcYear),2)

SELECT employee, emp_num, ss_num, rsalienexp ;
	FROM F:\HR\hrdata\employee ;
	INTO CURSOR curAlien ;
	WHERE ACTIVE ;
	AND NOT EMPTY(rsalienexp) ;
	AND rsalienexp < ldFirstOfThisMonthPlus2 ;
	ORDER BY rsalienexp

*!*	SELECT curAlien
*!*	BROWSE
SELECT curAlien
GOTO TOP
COPY TO F:\UTIL\HR\RSDNTALIEN\REPORTS\RSDNTALIEN.XLS XL5

*lcSendTo = "Marilyn Hecht <mhecht@fmiint.com>"
*lcSendTo = "Elsie Rosado <ERosado@fmiint.com>"
*lcSendTo = "Luz Aguado <LAguado@fmiint.com>"
*lcSendTo = "MarieDeSaye@fmiint.com"
lcSendTo = "mbennett@fmiint.com"

lcFrom = "TGF Corporate <fmicorporate@fmiint.com>"
lcSubject = "Resident Alien Expiration Dates"
lcCC = "Mark Bennett <mbennett@fmiint.com>"
lcAttach = "F:\UTIL\HR\RSDNTALIEN\REPORTS\RSDNTALIEN.XLS"
lcBodyText = "The attached spreadsheet lists employees whose Resident Alien status expires before " + TRANSFORM(ldFirstOfThisMonthPlus2) + "." + CRLF + ;
+ CRLF + "You will need to manually widen the spreadsheet columns to see all of the data." + CRLF + CRLF + "This email sent from computer: " + lcComputer
DO FORM dartmail2 WITH lcSendTo, lcFrom, lcSubject, lcCC, lcAttach, lcBodyText, "A"

IF USED('curAlien') THEN
	USE IN curAlien
ENDIF
IF USED('employee') THEN
	USE IN employee
ENDIF
