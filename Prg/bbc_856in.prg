Parameters nAcctnum

Public xfile,asn_origin,TranslateOption,lcScreenCaption,etadate,tsendto,tcc,tsendtoerr,tccerr,cCustname,cMod,lcPath,cfilename,lDoSQL,cWhse
Public nFileSize,cISA_Num,xfile,LogCommentStr,m.acctname,m.accountid,lcPath,lcArchivepath

runack("BBCASN")

nAcctNum = 6757
Do setparams With nAcctnum


xfile = ""
etadate=""
lcTempID = ""

Close Data All
&& these variables help with the EDIINLog store in SQL
cOffice="L"
cMod="L"

gOffice = cOffice
gMasterOffice = cOffice
guserid = "BBC856"
cCustname=m.acctname 
cfilename=""
nFileSize = 0
cISA_Num=""
xfile =""
LogCommentStr =""
&&----------------------------------------------------

ltesting = .F.
lOverridebusy = .t.
gMasterOffice ="L"

Do m:\dev\prg\_setvars With ltesting
On Error Do fmerror With Error(), Program(), Lineno(1), Sys(16),, .T.

lcScreenCaption = "BBC 856 ASN Uploader Process... Processing File: "
_Screen.Caption = lcScreenCaption

Wait Window At 10,10 " Now uploading BBC 856's........." Timeout 1
TranslateOption = "CR_TILDE" &&"NONE"
asn_origin = "BBC"

Select 0
Use F:\edirouting\FTPSETUP Shared
Locate For FTPSETUP.TRANSFER = "BBC-856IN"
If Found()
  If chkbusy And !lOverridebusy
    Wait Window "BBC 856 Process is busy...exiting" Timeout 2
    NormalExit = .T.
    Return
  Endif
Endif
Replace chkbusy With .T.,trig_time With Datetime()  For FTPSETUP.TRANSFER = "ARIAT-856IN"
Use In FTPSETUP

*Assert .F. Message "At MM Config load"
Select 0
Use F:\3pl\Data\mailmaster Alias mm Shared
Locate For mm.accountid = 6757 And edi_type = "856"
If Found()
 * Store Trim(mm.basepath) To lcPath
 * Store Trim(mm.archpath) To lcArchivePath
  tsendto = Iif(mm.use_alt,Trim(mm.sendtoalt),Trim(mm.sendto))
  tcc = Iif(mm.use_alt,Trim(mm.ccalt),Trim(mm.cc))
  Store Trim(mm.scaption) To thiscaption
  Store mm.testflag      To lTest
  _Screen.Caption = thiscaption
  Locate For (mm.edi_type = "MISC") And (mm.taskname = "GENERAL")
  If ltesting
    tsendto = Iif(mm.use_alt,Trim(mm.sendtoalt),Trim(mm.sendto))
    tcc = Iif(mm.use_alt,Trim(mm.ccalt),Trim(mm.cc))
  Endif
  tsendtoerr = Iif(mm.use_alt,Trim(mm.sendtoalt),Trim(mm.sendto))
  tccerr = Iif(mm.use_alt,Trim(mm.ccalt),Trim(mm.cc))
  Use In mm
Else
  Wait Window At 10,10  "No parameters set for this acct# "+Alltrim(Str(nAcctNum))+"  ---> Office "+cOffice Timeout 2
  Use In mm
  NormalExit = .T.
  Throw
Endif

*Do createx856

*Set Step On
Wait Window At 10,10  "Processing BBC 856s............." Timeout 1


useca("pl","wh")
xsqlexec("select * from pl where .f.","xpl",,"wh")
xsqlexec("select * from pl where .f.","tpl",,"wh")

useca("inwolog","wh")
xsqlexec("select * from inwolog where .f.","xinwolog",,"wh")
xsqlexec("select * from inwolog where .f.","tinwo",,"wh")

useca("ctnucc","wh")
xsqlexec("select * from ctnucc where 1=0","tctnucc",,"wh")

Delimiter = "*"
Do ProcessBBCasn

Select 0
Use F:\edirouting\FTPSETUP Shared
Replace chkbusy With .F. For FTPSETUP.TRANSFER = "ARIAT-856IN"
Use In FTPSETUP

***********************************************************************************************************
Procedure ProcessBBCasn

*lnNum = Adir(tarray,"f:\ftpusers\ariat-asns\in\*.*")
Do case


Endcase 
lnNum = Adir(tarray,lcPath+"*.*")

If ltesting
  lcPath = "f:\ftpusers\BBC-test\asnin\"
  lnNum = Adir(tarray,"f:\ftpusers\BBC-test\asnin\*.*")
Endif


set step on

If lnNum > 0
  For thisfile = 1  To lnNum
    xfile = lcPath+tarray[thisfile,1]  &&+"."
    cfilename =tarray[thisfile,1]  &&+"."
    archivefile = lcArchivePath+tarray[thisfile,1]  &&+"."
    nFileSize = Val(Transform(tarray(thisfile,2)))
    Wait Window "Importing file: "+xfile Nowait
    Do import_x12 With xfile  && created this as some sizes have a asterisk
*    Do loadedifile With xfile,"*",TranslateOption,"JONES856"

*!*        SELECT x856
*!*        LOCATE FOR x856.segment = 'GS'
*!*        IF x856.f1 = "FA"
*!*          WAIT WINDOW "This is a 997 FA file...moving to correct folder" TIMEOUT 2
*!*          cfile997in = ("F:\ftpusers\ariat-asns\997in\"+ALLTRIM(JUSTFNAME(xfile)))
*!*          COPY FILE [&xfile] TO [&cfile997in]
*!*          DELETE FILE [&xfile]
*!*          LOOP
*!*        ENDIF
    Do ImportBBCasn
    Copy File &xfile To &archivefile
    If File(archivefile)
      Delete File &xfile
    Endif
  Next thisfile
Else
  Wait Window  "No 856's to Import" Timeout 1
Endif

Endproc

******************************************************************************************************************
Procedure ImportBBCasn
******************************************************************************************************************

&&Try
lnPrfCtr = 0

Set Century On
Set Date To YMD

Select x856
Goto Top

lcHLLevel = ""
lcVendorCode = ""
lcCurrentHAWB = ""

testing=.F.

POs_added = 0
ShipmentNumber = 1
ThisShipment = 0
origawb = ""
lcCurrentShipID = ""
lcCurrentArrival = ""
LAFreight = .F.

** here we clear out asn data already loaded from this file
Select x856
Set Filter To
Goto Top
m.suppdata = ""
m.awb =""
m.asnloaddttm = Datetime()
lcCurrentPO = ""

Do While !Eof("x856")
  Wait Window "At the outer loop " Nowait

  If Trim(x856.segment) = "ISA"
    cISA_Num=Alltrim(x856.f13)
    m.isa   = Alltrim(x856.f13)
    m.dateloaded = Datetime()
    m.asnfile    = Upper(Justfname(xfile))
    Select x856
    Skip 1 In x856
    Loop
  Endif

  If Inlist(x856.segment,"GS","ST")
    Select x856
    Skip 1 In x856
    Loop
  Endif

  If Trim(x856.segment) = "BSN"&& and alltrim(x856.f1) = "05"  && must delete then add new info
    m.shipid = Alltrim(x856.f2)
    m.bsn    = Alltrim(x856.f2)
    Select x856
    Skip 1 In x856
    Loop
  Endif

  If At("HL",x856.segment)>0
    lcHLLevel = Alltrim(x856.f3)
  Endif

  If lcHLLevel = "S"
    Select tctnucc
    Scatter Memvar Memo Blank Fields Except isa,bsn,asnfile,hawb,shipid
    Select x856
    m.loaddt = Date()
    m.adddt = Datetime()
    addby = "BBC856"
    addproc ="TGFAUTO"
    m.dateloaded = Datetime()
    m.filename   = Upper(xfile)
    Do While lcHLLevel = "S"
      If Trim(x856.segment) = "REF" And Alltrim(x856.f1) ="OC"
        m.reference = Alltrim(x856.f2)
       endif 

      If Trim(x856.segment) = "TD3"
        m.container = Trim(x856.f2) +Trim(x856.f3)
        m.prefix    = Trim(x856.f2)
        m.ctrnum    = Trim(x856.f3)
        m.seal      = Trim(x856.f9)
        m.equiptype = Trim(x856.f1)
      Endif

      If Trim(x856.segment) = "TD1"
        m.qty     = Val((Alltrim(x856.f2)))
        m.suppdata = m.suppdata +Chr(13)+"TOTCTNS*"+Alltrim(x856.f2)
        m.weight  = Val((Alltrim(x856.f7)))
        m.suppdata = m.suppdata +Chr(13)+"TOTWEIGHT*"+Alltrim(x856.f7)
        m.cube    = Val((Alltrim(x856.f9)))
        m.suppdata = m.suppdata +Chr(13)+"TOTCUBE*"+Alltrim(x856.f9)
      Endif

      If Trim(x856.segment) = "TD5"
        If Alltrim(x856.f7) = "PA"
          m.destport = Alltrim(x856.f8)
        Endif
        If Alltrim(x856.f7) = "OR"
          m.originport =  Alltrim(x856.f8)
        Endif
      Endif

      If Trim(x856.segment) = "REF" And Upper(Trim(x856.f1)) = "BL"
        m.bol = Alltrim(x856.f2)
      Endif

      If Trim(x856.segment) = "REF" And Upper(Trim(x856.f1)) = "AW"
        m.awb = Alltrim(x856.f2)
      Endif

      If Trim(x856.segment) = "REF" And Upper(Trim(x856.f1)) = "PK"
        m.packinglistnumber = Alltrim(x856.f2)
      Endif

      If Trim(x856.segment) = "DTM" And Alltrim(x856.f1)= "056"
        m.documentdate = Alltrim(x856.f2)  &&Substr(Alltrim(x856.f2),5,2)+"/"+Substr(Alltrim(x856.f2),7,2)+"/"+Substr(Alltrim(x856.f2),1,4)\
        etadate= Substr(Alltrim(x856.f2),5,2)+"/"+Substr(Alltrim(x856.f2),7,2)+"/"+Substr(Alltrim(x856.f2),1,4)
      Endif

      Select x856
      Skip 1 In x856
      If Trim(x856.segment) = "HL"
        lcHLLevel = Trim(x856.f3)
      Endif
    Enddo
  Endif   && end if HL S

  If lcHLLevel = "E"
    Do While lcHLLevel = "E"
      If Trim(x856.segment) = "TD3"
        m.container = Trim(x856.f2) +Trim(x856.f3)
        m.seal = Trim(x856.f9)
        m.equiptype = Trim(x856.f1)
        m.seal = Trim(x856.f9)
      Endif
      Select x856
      Skip 1 In x856
      If Trim(x856.segment) = "HL"
        lcHLLevel = Trim(x856.f3)
      Endif
    Enddo
  Endif

  If lcHLLevel = "O"
    Do While lcHLLevel = "O"
      If Trim(x856.segment) = "PRF"
        lcCurrentPO = Alltrim(x856.f1)
        m.ponum    =  Alltrim(x856.f1)
        m.podate   =  Alltrim(x856.f4)
      Endif
      Select x856
      Skip 1 In x856
      If Trim(x856.segment) = "HL"
        lcHLLevel = Trim(x856.f3)
      Else
        Loop
      Endif
    Enddo
  Endif

  If lcHLLevel = "P"
    Do While lcHLLevel = "P"
      If Trim(x856.segment) = "MAN"
        lnPrfCtr = 0
        m.ucc = Alltrim(x856.f2)
      Endif

      Select x856
      If !Eof("x856")
        Skip 1 In x856
      Else
        Exit
      Endif
      If Trim(x856.segment) = "HL"
        lcHLLevel = Trim(x856.f3)
      Endif
    Enddo
  Endif

  If lcHLLevel = "I"
    Do While lcHLLevel = "I"
      If Trim(x856.segment) = "LIN"
        m.asnline = Alltrim(x856.f1)
        m.style   = Alltrim(x856.f3)
        m.color   = Alltrim(x856.f9)
        m.id      = Alltrim(x856.f7)
        m.upc     = Alltrim(x856.f5)
      Endif
      If Trim(x856.segment) = "SN1"
        m.totqty = Val(Alltrim(x856.f2))
        m.pack   = Alltrim(x856.f2)
        m.uom    = Alltrim(x856.f3)
      Endif
      If Trim(x856.segment) = "REF" And Alltrim(x856.f1) = "LI"
        m.poline = Alltrim(x856.f2)
      Endif
      If Trim(x856.segment) = "PRF" 
        m.custpo = Alltrim(x856.f1)
      Endif
      If Trim(x856.segment) = "REF" And Alltrim(x856.f1) = "WS"
        Select tctnucc
        m.office = "L"
        m.mod ="L"
        Select tctnucc
        m.suppdata =                   "PREFIX*"+m.prefix
        m.suppdata =m.suppdata+Chr(13)+"CTRNUM*"+m.ctrnum
        m.suppdata =m.suppdata+Chr(13)+"PODATE*"+m.podate
        m.suppdata =m.suppdata+Chr(13)+"ASNLINE*"+m.asnline
        m.suppdata =m.suppdata+Chr(13)+"POLINE*"+m.poline
        m.suppdata =m.suppdata+Chr(13)+"CUSTPO*"+m.custpo
        m.suppdata =m.suppdata+Chr(13)+"UOM*"+m.uom
        m.ponum = lcCurrentPO
        Append Blank
        m.asnloaddttm = Datetime()
        Gather Memvar Memo
        m.suppdata = ""
      Endif
      Select x856
      If !Eof("x856")
        Skip 1 In x856
      Else
        Exit
      Endif
      If Trim(x856.segment) = "HL"
        lcHLLevel = Trim(x856.f3)
      Endif
    Enddo

    If Trim(x856.segment) = "PRF"
      lnPrfCtr = lnPrfCtr +1
      m.ponum =  Alltrim(x856.f1)
      Select tctnucc
 *     m.acctname ="ARIAT"
 *     m.accountid = 6532
      m.office = "K"
      Skip 1 In x856
      If (x856.f3) = "P" Or Alltrim(x856.segment) ="CTT"  && after the last PO loop is the ending CTT segment
        Skip -1 In x856
        If !lnPrfCtr > 1
          m.id =""
          lcTempID = ""
        Endif
      Else
        m.id = lcTempID
        Skip -1 In x856
      Endif

      Append Blank
      Gather Memvar Memo
      m.suppdata = ""
    Endif

    Select x856
    If !Eof("x856")
      Skip 1 In x856
    Else
      Exit
    Endif
    If Trim(x856.segment) = "HL"
      lcHLLevel = Trim(x856.f3)
    Endif
  Endif

  If Eof("x856")
    Exit
  Endif
Enddo &&!eof("x856")


Set Step On

Select tctnucc
Copy To h:\fox\tctnucc

LogCommentStr = LogCommentStr+Alltrim(m.container)+"-"+Alltrim(m.bol)+"-"+m.awb+"-"+m.shipid

*DO
*asn_in_data()

Do StoreASNData

Do CreateInbound

Do Import_Data

LogCommentStr = ""
m.awb=""

Endproc

**********************************************************************************************************************
Procedure CreateInbound
m.plid =0
m.inwologid =0

Select tctnucc

Select ucc,Style,Color,Id,Container,bol,asnfile,shipid,Sum(totqty) As inqty,Space(20)as poline,Space(20)as UPC,;
      Space(20) ponum From tctnucc Group By ucc,Style,Color,id,Container,bol, asnfile Into Cursor tempinb Readwrite
Select tempinb
Scan
  Select tctnucc
  Locate For ucc = tempinb.ucc 
  thisPOLine = getmemodata("suppdata","POLINE")
  replace poline With thisPOLine    In tempinb
  replace upc    With tctnucc.upc   In tempinb
  replace ponum  With tctnucc.ponum In tempinb
endscan

-
Select ucc,Count(*) As Cnt From tempinb Group By ucc Having Cnt>1 Into Cursor tempmixed Readwrite && these are mixed cartons

Select tempmixed
Scan
  Select tempinb
  Locate For tempmixed.ucc = tempinb.ucc
  If Found()
    Delete For tempmixed.ucc = tempinb.ucc In tempinb
  Endif
Endscan

Select Style,Color,Id,upc,ponum,poline,Sum(inqty),Count(*) As Cnt From tempinb Group By Style,Color,Id,inqty Into Cursor tempfullctns Readwrite

Select tempinb
Go Top

Scatter Memvar
m.container = tempinb.Container
*m.reference = tempinb.bol
m.brokerref = tempinb.shipid
m.comments = "ASN Upload"+Chr(13)+"ASN FILE: "+tempinb.asnfile

m.inwologid = m.inwologid + 1
Insert Into xinwolog From Memvar

groupctr = 1
Select tempfullctns
Locate
Scan
  Scatter Memvar
  m.units  = .F.
  m.id = ""
  m.totqty = tempfullctns.Cnt
  m.style  = tempfullctns.Style
  m.color  = tempfullctns.Color
  m.id     = tempfullctns.id
*  Set Step On
  m.pack   = Alltrim(Transform(tempfullctns.sum_inqty/tempfullctns.Cnt))
  m.po     =  Transform(groupctr)
  m.cayset =  tempfullctns.ponum
  m.echo = "UPC*"+tempfullctns.upc+Chr(13)+"PONUM*"+tempfullctns.ponum+Chr(13)+"LINENUM*"+tempfullctns.poline
  m.plid = m.plid + 1
  Insert Into xpl From Memvar
*  replace xpl.echo With "UPC*"+tempfullctns.upc+Chr(13)+"PONUM*"+tempfullctns.ponum+Chr(13)+"LINENUM*"+tempfullctns.poline In xpl
  m.units  = .T.
  m.totqty = tempfullctns.sum_inqty
  m.style  = tempfullctns.Style
  m.color  = tempfullctns.Color
  m.id     = tempfullctns.id
  m.pack   = "1"
  m.po = Transform(groupctr)
  m.plid = m.plid + 1
  Insert Into xpl From Memvar
  groupctr = groupctr +1
Endscan

Select tempinb
Recall All
m.plid = m.plid

Select tempmixed
Goto Top
Scan
  Select tempinb
  Select * From tempinb Where tempinb.ucc = tempmixed.ucc Into Cursor temp Readwrite
  Select temp
  Sum inqty To ctnpack
  Select temp
  Go Top
  Scatter Memvar
  m.units  = .F.
  m.totqty = 1
  m.po = Transform(groupctr)
  m.color  = m.id
  m.pack   = Alltrim(Transform(ctnpack))
  m.po = Transform(groupctr)
  Insert Into xpl From Memvar
  Replace Id With "" In xpl

  Select temp
  Scan
    m.units  = .T.
    m.totqty = temp.inqty
    m.style  = temp.Style
    m.color  = temp.Color
    m.id     = temp.id
    m.pack   = "1"
    m.po = Transform(groupctr)
    m.plid = m.plid + 1
    Insert Into xpl From Memvar
    Replace Id With "" In xpl
  Endscan
  groupctr = groupctr +1
Endscan

*Set Step On

Select xpl
Sum totqty To totunits For units = .T.
Sum totqty To totctns For units = .F.

Select xinwolog
Go Top
Replace xinwolog.plunitsinqty With totunits In xinwolog
Replace xinwolog.plinqty      With totctns In xinwolog

*Set Step On
Endproc
**********************************************************************************************************************
Procedure Import_Data

set step on

Select xinwolog
Locate

Scan  && Currently in XINWOLOG table
  Select inwolog
  cOffice = "L"
  Select xinwolog
  Scatter Memvar Memo
  nWO_num  = dygenpk("wonum","whl")
  m.wo_num = nwo_num
  m.wo_date = Date()
  m.addproc ="BBC856"
  m.accountid = nAcctNum
  cWO_Num = Allt(Str(m.wo_num))

  insertinto("inwolog","wh",.T.)
  tu("inwolog")
Endscan

Select xpl
Scan
  Scatter Memvar Memo
  m.mod = cMod
  m.office = cOffice
  m.inwologid = inwolog.inwologid
  m.accountid = nAcctNum
  m.wo_num = nwo_num
  insertinto("pl","wh",.T.)
Endscan
tu("pl")

&& OK, now take the records in tctnucc and move them into the production table

Select tctnucc
Scan
  Scatter Memvar Memo
  m.inwonum = inwolog.wo_num
  insertinto("ctnucc","wh",.T.)
Endscan

tu("ctnucc")

* Deanna Nelson <Deanna.Nelson@tollgroup.com>
* Cheri Foster <Cheri.Foster@tollgroup.com>
* Kimberly Sallee <kimberly.sallee@tollgroup.com>
* Jace Sipes <Jace.Sipes@tollgroup.com>

** OK now send the email

tfrom    ="TGF/ARIAT EDI Poller Operations <transload-ops@fmiint.com>"
tsubject = "BBC ASN Uploader WO#: "+Alltrim(Transform(inwolog.wo_num))+"  Container: "+inwolog.Container+ "  ETA: "+etadate
tattach =""
tmessage = "Inbound Created for Toll-Mira-Loma Warehouse"+Chr(13)
tmessage = tmessage +;
  "-------------------------------------------------"+Chr(13)+;
  "Work Order    :"+Alltrim(Transform(inwolog.wo_num))+Chr(13)+;
  "Container     :"+inwolog.Container+Chr(13)+;
  "Filename      : "+xfile+Chr(13)+;
  "ETA           : "+etadate+Chr(13)+;
  "BOL           : "+Transform(inwolog.Reference)+Chr(13)+;
  "Reference     : "+Transform(inwolog.brokerref)+Chr(13)+;
  "# of Cartons  : "+Alltrim(Transform(inwolog.plinqty))+Chr(13)+;
  "Containing    : "+Alltrim(Transform(inwolog.plunitsinqty))+" Units"

Do Form dartmail2 With tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"

Select xinwolog
Zap
Select xpl
Zap
Select tctnucc
Zap
Select x856
Zap
etadate = ""

Endproc

**********************************************************************************************************************
Procedure StoreASNData
**********************************************************************************************************************
lcQuery = [select * from ediinlog where accountid = ]+Transform(nAcctNum)+[ and isanum = ']+cISA_Num+[']
xsqlexec(lcQuery,"p1",,"stuff")
If Reccount()=0
  Use In p1
  useca("ediinlog","stuff")  && Creates updatable PTHIST cursor

*      LogCommentStr = "Pick tickets uploaded"
  Select ediinlog
  Scatter Memvar Memo
  m.ediinlogid = dygenpk("ediinlog","WHALL")
  m.acct_name  = cCustname
  m.accountid  = nAcctNum
  m.mod        = cMod
  m.office     = cOffice
  m.filepath   = lcPath
  m.filename   = cfilename
  m.size       = nFileSize
  m.FTIME      = Date()
  m.FDATETIME  = Datetime() &&tarray(thisfile,4)
  m.TYPE       = "856"
  m.qty        = 0
  m.isanum     =  cISA_Num
  m.uploadtime = Datetime()
  m.comments   = LogCommentStr
  m.edidata    = ""
  Insert Into ediinlog From Memvar
  Append Memo edidata From &xfile
  tu("ediinlog")
Endif
**********************************************************************************************************************
Procedure setparams

  parameter nAcctnum

Do case
  Case nAcctNum = 6757
   m.accountid = 6757
   m.acctname = "BBC/RW"
   lcPath        = "f:\ftpusers\BBCRW\856IN\"
   lcArchivePath = "f:\ftpusers\BBCRW\856IN\ARCHIVE\"

  Case nAcctNum = 6763
   m.accountid = 6763
   m.acctname = "BBC/CLUB FOOT"
   lcPath        = "f:\ftpusers\BBCCF\856IN\"
   lcArchivePath = "f:\ftpusers\BBCCF\856IN\ARCHIVE\"

  Case nAcctNum = 6761
   m.accountid = 6761
   m.acctname = "BBC INTERNATIONAL"
   lcPath        = "f:\ftpusers\BBC\856IN\"
   lcArchivePath = "f:\ftpusers\BBC\856IN\ARCHIVE\"

  Case nAcctNum = 6747
   m.accountid = 6747
   m.acctname = "BBC RVCA"
   lcPath        = "f:\ftpusers\BBCFY\856IN\"
   lcArchivePath = "f:\ftpusers\BBCFY\856IN\ARCHIVE\"
Endcase
 


Endproc 
**********************************************************************************************************************
