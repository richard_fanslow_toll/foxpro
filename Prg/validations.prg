* commonly-used data validation & related functions
 
FUNCTION IsValidNumericDivision
	LPARAMETERS tnDivision
	LOCAL llRetval
	llRetval = INLIST(tnDivision,0,1,2,3,4,5,6,7,11,12,14,32,50,51,52,53,54,55,56,57,58,59) ;
	OR INLIST(tnDivision,60,61,62,71,72,73,74,75,76,78,80,82,90,91,92) ;
	OR INLIST(tnDivision,10,20,21,22,23,24,25,26,27,28,29,30) ;
	OR INLIST(tnDivision,35,36,37,38,39)
	RETURN llRetval
ENDFUNC


FUNCTION GetMasterCompanyFromNumericDivision
	LPARAMETERS tnDivision
	LOCAL lcRetVal
	DO CASE
		CASE INLIST(tnDivision,20,21,22,23,24,25,26,27,28,29,30,31)
			lcRetVal = "TGF"
		CASE INLIST(tnDivision,35,36,37,38,39)
			lcRetVal = "GEN"
*!*			CASE INLIST(tnDivision,11,12,92)
*!*				lcRetVal = "SCM"
		CASE INLIST(tnDivision,61)
			lcRetVal = "Seamaster"
		CASE INLIST(tnDivision,62)
			lcRetVal = "MLI"
*!*			CASE INLIST(tnDivision,71,72,73,74,76,78)
*!*				lcRetVal = "TUG"
		CASE INLIST(tnDivision,11,12,92,71,72,73,74,76,78)
			lcRetVal = "OTI"
		CASE INLIST(tnDivision,80)
			lcRetVal = "AmerRussia"
		CASE INLIST(tnDivision,90)
			lcRetVal = "SMG"
		OTHERWISE
			lcRetVal = "FMI"
	ENDCASE
	RETURN lcRetVal
ENDFUNC

FUNCTION GetCompanyFromStringDivision
	* return Master Company from passed division (character format)
	LPARAMETERS tcDivision
	LOCAL lcRetVal
	DO CASE
		CASE INLIST(tcDivision,'20','21','22','23','24','25','26','27','28','29','30','31')
			lcRetVal = "TGF"
		CASE INLIST(tcDivision,'35','36','37','38','39')
			lcRetVal = "GEN"
*!*			CASE INLIST(tcDivision,'11','12','92')
*!*				lcRetVal = "SCM"
		CASE INLIST(tcDivision,'61')
			lcRetVal = "Seamaster"
		CASE INLIST(tcDivision,'62')
			lcRetVal = "MLI"
*!*			CASE INLIST(tcDivision,'71','72','73','74','76','78')
*!*				lcRetVal = "TUG"
		CASE INLIST(tcDivision,'11','12','92','71','72','73','74','76','78')
			lcRetVal = "OTI"
		CASE INLIST(tcDivision,'80')
			lcRetVal = "AmerRussia"
		CASE INLIST(tcDivision,'90')
			lcRetVal = "SMG"
		OTHERWISE
			lcRetVal = "FMI"
	ENDCASE
	RETURN lcRetVal
ENDFUNC


FUNCTION GetNewADPCompany
	LPARAMETERS tcOldCompany
	LOCAL lcNewCompany
	DO CASE
		CASE tcOldCompany = "AXA"
			lcNewCompany = "E89"
		CASE tcOldCompany = "SXI"
			lcNewCompany = "E87"
		CASE tcOldCompany = "ZXU"
			lcNewCompany = "E88"
		OTHERWISE
			lcNewCompany = tcOldCompany
	ENDCASE
	RETURN lcNewCompany
ENDFUNC


FUNCTION GetOldADPCompany
	LPARAMETERS tcNewCompany
	LOCAL lcOldCompany
	DO CASE
		CASE tcNewCompany = "E89"
			lcOldCompany = "AXA"
		CASE tcNewCompany = "E87"
			lcOldCompany = "SXI"
		CASE tcNewCompany= "E88"
			lcOldCompany= "ZXU"
		OTHERWISE
			lcOldCompany = tcNewCompany
	ENDCASE
	RETURN lcOldCompany
ENDFUNC


FUNCTION GetSUISDIFromState
	LPARAMETERS tcSiteState
	LOCAL lcSUISDI
	DO CASE
		CASE tcSiteState = "AL"
			lcSUISDI = "03"
		CASE tcSiteState = "CA"
			lcSUISDI = "75"
		CASE tcSiteState = "FL"
			lcSUISDI = "42"
		CASE tcSiteState = "GA"
			lcSUISDI = "23"
		CASE tcSiteState = "IL"
			lcSUISDI = "43"
		CASE tcSiteState = "MA"
			lcSUISDI = "21"
		CASE tcSiteState = "NC"
			lcSUISDI = "12"
		CASE tcSiteState = "NJ"
			lcSUISDI = "21"
		CASE tcSiteState = "NY"
			lcSUISDI = "19"
		CASE tcSiteState = "PA"
			lcSUISDI = "59"
		CASE tcSiteState = "SC"
			lcSUISDI = "35"
		CASE tcSiteState = "TN"
			lcSUISDI = "52"
		CASE tcSiteState = "TX"
			lcSUISDI = "53"
		CASE tcSiteState = "WV"
			lcSUISDI = "26"
		OTHERWISE
			lcSUISDI = ""
	ENDCASE
	RETURN lcSUISDI
ENDFUNC
