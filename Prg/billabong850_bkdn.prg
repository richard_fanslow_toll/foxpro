*!* m:\dev\prg\billabong850_bkdn.prg
clear
wait window "Now in BKDN Phase..." nowait

lcheckstyle = .t.
lpick = .f.
lprepack = .t.
m.units = .f.
store "" to cunitofmeas,m.linenum,cdesc,cinnerpack1,cinnerpack2,cinnerpack,cunitstype,cuom,corigwt,cuow,corigvol,cuov
store "" to lcstyle,lccolor,lcsize,lcqty,lcupc,lcsku,lcdesc
wait window "Now in 850 Breakdown" &message_timeout2

goffice = cmod
gmasteroffice = coffice
m.office = coffice
m.mod = cmod

if !ltesting
	useca("ackdata","wh")
endif

select x856
locate
count for trim(segment) = "ST" to nsegcnt
csegcnt = alltrim(str(nsegcnt))

wait window "There are "+csegcnt+" P/Ts in this file" &message_timeout2
select x856
locate

*!* Constants
m.acct_name = "BILLABONG"
m.acct_num = nacctnum
m.careof = " "
m.sf_addr1 = "117 WATERWORKS WAY"
m.sf_addr2 = ""
m.sf_csz = "IRVINE, CA 92618"
m.printed = .f.
m.print_by = ""
m.printdate = date()
m.custsku = ""
ljcpmail = .t.
*!* End constants

select xpt
delete all
select xptdet
delete all

store "" to m.isa_num,m.shipins,m.color,m.id
store 0 to ptctr,pt_total_eaches,pt_total_cartons,nctncount

create cursor brandlist1 (brand c(2), accountid n(4))
select x856
set filter to

*naftacheck()
locate

scan for (x856.segment = "REF" and x856.f1 = "3L")
	m.brand = alltrim(x856.f2)
	do case
	case alltrim(x856.f2) = "BB"
		m.accountid=6744
	case alltrim(x856.f2) = "RV"
		m.accountid=6747
	case alltrim(x856.f2) = "EL"
		m.accountid=6718
	case alltrim(x856.f2) = "CA"
		m.accountid=6744
	case alltrim(x856.f2) = "EC"
		m.accountid=6718
	case alltrim(x856.f2) = "CR"
		m.accountid=6747
	otherwise
		m.accountid=6744
	endcase
	insert into brandlist1 from memvar
endscan
select brandlist1

select * from brandlist1 group by 1,2 into cursor brandlist
use in brandlist1
locate
if ltesting
*	BROWSE
	nbrandrecs = reccount()
endif

select x856
locate
wait window "NOW IN INITIAL HEADER BREAKDOWN" &message_timeout2

ptidctr    =0
ptdetidctr =0
m.ptdetid = 0

nisa = 0  && To check for embedded FA envelopes.
ljcp = .f.
********************************************************************************************

do while !eof("x856")
	csegment = trim(x856.segment)
	ccode = trim(x856.f1)

	if trim(x856.segment) = "ISA"
		if nisa = 0
			nisa = 1
			m.isa_num = alltrim(x856.f13)
			cisa_num = alltrim(x856.f13)
			select x856
			skip
			loop
		endif
	endif

	if (trim(x856.segment) = "GS")
		cbrand = "XX"
		cgs_num = alltrim(x856.f6)
		lccurrentgroupnum = alltrim(x856.f6)
		select x856
		skip
		loop
	endif

	if trim(x856.segment) = "ST" and trim(x856.f1) = "850"
		lcpttype=""
		if !ltesting
			m.groupnum=lccurrentgroupnum
			m.isanum=cisa_num
			m.transnum=x856.f2
			m.edicode="PO"
			m.accountid=6718
			m.loaddt=date()
			m.loadtime=datetime()
			m.filename=xfile
			insertinto("ackdata","wh",.t.)
		else
		endif
		select x856
		skip
		loop
	endif


	if (trim(x856.segment) = "BEG")
		litd = .f.
		lltradedirectflag = .f.
		llcodflag = .f.

		if !ltesting
			replace ackdata.ship_ref with alltrim(x856.f2) in ackdata
		endif

		ldolineinsert = .f.
		ptdetctr = 0
		ptidctr  = ptidctr    +1
		ccustnotes = ""
		m.vendor_num = ""
		nctncount = 0
		nshcount = 1
		ndelcount = 1
		ndetcnt = 0
		select xpt
		append blank
		replace xpt.addproc with "BILL850" in xpt
		replace xpt.adddt with datetime() in xpt
		replace xpt.addby with "TOLLPROC" in xpt
		replace xpt.office with coffice in xpt
		replace xpt.mod with cmod in xpt
		replace xpt.accountid with nacctnum
		replace xpt.shipins with "FILENAME*"+cfilename in xpt
		replace xpt.qty with 0 in xpt
		replace xpt.origqty with 0 in xpt
		store 0 to pt_total_eaches
		store 0 to pt_total_cartons
		ptctr = ptctr +1
		wait window at 10,10 "Now processing PT# "+ alltrim(x856.f2)+" Number "+chr(13)+transform(ptctr)+" of "+csegcnt nowait
		replace xpt.ptid       with ptidctr in xpt
		replace xpt.ptdate     with date() in xpt
		m.ship_ref = alltrim(x856.f3)
		lsplitpt = .f.
		m.cnee_ref = alltrim(x856.f6)
		replace xpt.ship_ref   with m.ship_ref in xpt  && Pick Ticket
		replace xpt.cnee_ref with m.cnee_ref in xpt  && Pick Ticket
		replace xpt.shipins with xpt.shipins+chr(13)+"SALESORDER*"+alltrim(x856.f4) in xpt
		m.pt = alltrim(x856.f3)
		select x856
		skip 1
		loop
	endif

	if alltrim(x856.segment) = "REF"
		do case

		case alltrim(x856.f1) = "3L"
			cbrand = alltrim(x856.f2)
			do case
			case cbrand = "BB"
				m.accountid=6744
			case cbrand = "RV"
				m.accountid=6747
			case cbrand = "EL"
				m.accountid=6718
			case cbrand = "CA"
				m.accountid=6744
			case cbrand = "EC"
				m.accountid=6718
			case cbrand = "CR"
				m.accountid=6747
			otherwise
				m.accountid=6744
			endcase
			replace xpt.accountid with m.accountid in xpt
			replace xpt.shipins with xpt.shipins+chr(13)+"BRAND*"+cbrand in xpt
			replace xpt.shipins with xpt.shipins+chr(13)+"SALESORDER*"+alltrim(x856.f4) in xpt

		case alltrim(x856.f1) = "PHC" and alltrim(x856.f2)="CON"
			replace xpt.shipins with xpt.shipins+chr(13)+"PTTYPE*"+alltrim(x856.f2) in xpt
			lcpttype="CON"
			replace batch_num with alltrim(x856.f2) in xpt
		case alltrim(x856.f1) = "PHC" and alltrim(x856.f2)="WEB"
			replace xpt.shipins with xpt.shipins+chr(13)+"PTTYPE*"+alltrim(x856.f2) in xpt
			lcpttype="WEB"
			replace batch_num with alltrim(x856.f2) in xpt

		case alltrim(x856.f1) = "PHC" and alltrim(x856.f2)="PNP"
			replace xpt.shipins with xpt.shipins+chr(13)+"PTTYPE*"+alltrim(x856.f2) in xpt
			lcpttype="PNP"
			replace batch_num with alltrim(x856.f2) in xpt

		case alltrim(x856.f1) = "PHC" and alltrim(x856.f2)="ITX"
			replace xpt.shipins with xpt.shipins+chr(13)+"PTTYPE*"+alltrim(x856.f2) in xpt
			lcpttype="ITX"
			replace batch_num with alltrim(x856.f2) in xpt

		case alltrim(x856.f1) = "DP"
			replace xpt.shipins with xpt.shipins+chr(13)+"DEPT*"+alltrim(x856.f2) in xpt
			replace xpt.shipins with xpt.shipins+chr(13)+"FOB*"+alltrim(x856.f3) in xpt  &&&&& added TMARG 6/28/17 to capture Macy's dept name for label
			replace xpt.dept    with alltrim(x856.f2) in xpt

		case alltrim(x856.f1) = "AAO"
&& typical segment layout  REF|AAO|1Y750E
			replace xpt.shipins with xpt.shipins+chr(13)+"3RDPARTY*"+alltrim(x856.f2) in xpt
			replace xpt.upsacctnum with alltrim(x856.f2) in xpt

		case alltrim(x856.f1) = "ZA"
			replace xpt.shipins with xpt.shipins+chr(13)+"VENDNUM*"+alltrim(x856.f2) in xpt
			replace xpt.vendor_num with alltrim(x856.f2) in xpt
***TMARG changed 7/27/17  Trade direct flag is now set in TD5
*!*			case alltrim(x856.f1) = "ACD" and alltrim(x856.f2) ="XE"
*!*				lltradedirectflag = .t.
*!*				replace xpt.shipins with xpt.shipins+chr(13)+"TRADEDIRECT*YES" in xpt
		case alltrim(x856.f1) = "ACD"
			replace xpt.shipins with xpt.shipins+chr(13)+"ACD*"+alltrim(x856.f2) in xpt

		case alltrim(x856.f1) = "LV"
			replace xpt.shipins with xpt.shipins+chr(13)+"LVCODE*"+alltrim(x856.f2) in xpt
&& for Ref Customer 8
&& for Product Type for Zappos
&& for PromoCode for NexCom
&& for FOB for Macys or Coast Guard
&& for EventCode for REI

		case alltrim(x856.f1) = "I5"
			replace xpt.shipins with xpt.shipins+chr(13)+"INVCODE*"+alltrim(x856.f2) in xpt

		case alltrim(x856.f1) = "4C"
			replace xpt.shipins with xpt.shipins+chr(13)+"MARKFORSTORE*"+STRTRAN(upper(trim(x856.f2)),"'","`") in xpt

		case alltrim(x856.f1) = "ACB"
			replace xpt.shipins with xpt.shipins+chr(13)+"REGION*"+alltrim(x856.f2) in xpt

		case alltrim(x856.f1) = "TOC"
			replace xpt.shipins with xpt.shipins+chr(13)+"SHIPPINGCODE*"+alltrim(x856.f2) in xpt

		case alltrim(x856.f1) = "LU"
			replace xpt.shipins with xpt.shipins+chr(13)+"CANADALOCATION*"+alltrim(x856.f2) in xpt
			replace batch_num with "TD-"+alltrim(x856.f2) in xpt
		* moved sforstore from MARKFOR to EDISHIPTO  9/29/17 TMARG
		case alltrim(x856.f1) = "ST"
			replace xpt.shipins with xpt.shipins+chr(13)+"EDISHIPTO*"+alltrim(x856.f2) in xpt
			replace xpt.sforstore with alltrim(x856.f2) in xpt
		endcase

		select x856
		skip 1
		loop
	endif

	if alltrim(x856.segment) = "LIN"
		lcvasstr = ""
		if alltrim(x856.f2) = "SA"
			lcvasstr = lcvasstr+alltrim(x856.f3)+","
			replace xpt.v01 with .t.
			replace xpt.vas with .t.
		endif
		if alltrim(x856.f4) = "SB"
			lcvasstr = lcvasstr++alltrim(x856.f5)+","
			replace xpt.v02 with .t.
			replace xpt.vas with .t.
		endif
		if alltrim(x856.f6) = "SC"
			lcvasstr = lcvasstr++alltrim(x856.f7)+","
			replace xpt.v03 with .t.
			replace xpt.vas with .t.
		endif
		if alltrim(x856.f8) = "SD"
			lcvasstr = lcvasstr++alltrim(x856.f9)+","
			replace xpt.v04 with .t.
			replace xpt.vas with .t.
		endif
		if alltrim(x856.f10) = "SE"
			lcvasstr = lcvasstr++alltrim(x856.f11)+","
			replace xpt.v05 with .t.
			replace xpt.vas with .t.
		endif
		if alltrim(x856.f12) = "SF"
			lcvasstr = lcvasstr++alltrim(x856.f13)+","
			replace xpt.v06 with .t.
			replace xpt.vas with .t.
		endif
		if alltrim(x856.f14) = "SG"
			lcvasstr = lcvasstr++alltrim(x856.f15)+","
			replace xpt.v07 with .t.
			replace xpt.vas with .t.
		endif
		if alltrim(x856.f16) = "SH"
			lcvasstr = lcvasstr++alltrim(x856.f17)+","
			replace xpt.v08 with .t.
			replace xpt.vas with .t.
		endif
		if alltrim(x856.f18) = "SI"
			lcvasstr = lcvasstr++alltrim(x856.f19)+","
			replace xpt.v09 with .t.
			replace xpt.vas with .t.
		endif
		replace xpt.shipins with xpt.shipins+chr(13)+"VASSTR*"+lcvasstr in xpt
		replace xpt.shipins with xpt.shipins+chr(13)+"VASFLAG*TRUE" in xpt
		select x856
		skip 1
		loop
	endif

	if alltrim(x856.segment) = "TAX"
		do case
		case alltrim(x856.f2) = "GL"
			replace xpt.shipins with xpt.shipins+chr(13)+"CANADAGS*"+alltrim(x856.f3) in xpt
		case alltrim(x856.f2) = "PS"
			replace xpt.shipins with xpt.shipins+chr(13)+"CANADAPG*"+alltrim(x856.f3) in xpt
		case alltrim(x856.f2) = "BE"
			replace xpt.shipins with xpt.shipins+chr(13)+"CANADABE*"+alltrim(x856.f3) in xpt
		case alltrim(x856.f2) = "ZZ"
			replace xpt.shipins with xpt.shipins+chr(13)+"USTAX*"+alltrim(x856.f3) in xpt
		case alltrim(x856.f2) = "FR"
			replace xpt.shipins with xpt.shipins+chr(13)+"FREIGHTMARKUP*"+alltrim(x856.f3) in xpt
		endcase
		select x856
		skip 1
		loop
	endif

	if alltrim(x856.segment) = "SAC"
		replace xpt.shipins with xpt.shipins+chr(13)+"CODAMOUNT*"+alltrim(x856.f2) in xpt
		replace xpt.shipins with xpt.shipins+chr(13)+"AGENCYCODE*"+alltrim(x856.f4) in xpt
		replace xpt.shipins with xpt.shipins+chr(13)+"CODCHARGE*"+alltrim(x856.f5) in xpt
		replace xpt.shipins with xpt.shipins+chr(13)+"FREIGHTDISC*"+alltrim(x856.f7) in xpt
		replace xpt.shipins with xpt.shipins+chr(13)+"CARTONCHARGE*"+alltrim(x856.f8) in xpt
		select x856
		skip 1
		loop
	endif

	if alltrim(x856.segment) = "ITD"
		litd = .t.
		if inlist(alltrim(x856.f14),"S","U")
			if alltrim(x856.f14) = "S"
				replace xpt.shipins with xpt.shipins+chr(13)+"FUNDS*SECURE" in xpt
			else
				replace xpt.shipins with xpt.shipins+chr(13)+"FUNDS*UNSECURE" in xpt
			endif
			replace xpt.shipins with xpt.shipins+chr(13)+"CODFLAG*YES" in xpt
			llcodflag = .t.
			select x856
			skip 1
			loop
		endif
	endif

	if alltrim(x856.segment) = "DTM" and trim(x856.f1) == "037"  && Start date
		lcdate = alltrim(x856.f2)
		lxdate = substr(lcdate,5,2)+"/"+substr(lcdate,7,2)+"/"+left(lcdate,4)
		lddate = ctod(lxdate)
		replace xpt.start with lddate in xpt
		release lddate
		select x856
		skip
		loop
	endif

	if alltrim(x856.segment) = "DTM" and trim(x856.f1) == "038"  && cancel date
		lcdate = alltrim(x856.f2)
		lxdate = substr(lcdate,5,2)+"/"+substr(lcdate,7,2)+"/"+left(lcdate,4)
		lddate = ctod(lxdate)
		replace xpt.cancel with lddate in xpt
		release lddate
		select x856
		skip
		loop
	endif

	if alltrim(x856.segment) = "SI"

		if !empty(alltrim(x856.f2))
			replace xpt.shipins with xpt.shipins+chr(13)+"SHIPLBLCODE*"+alltrim(x856.f3) in xpt
		endif

		if !empty(alltrim(x856.f6))
			replace xpt.shipins with xpt.shipins+chr(13)+"PACKLISTCODE*"+alltrim(x856.f7) in xpt
		endif

		if !empty(alltrim(x856.f8))
			replace xpt.shipins with xpt.shipins+chr(13)+"PACKLISTLOCATION*"+alltrim(x856.f9) in xpt
		endif

		select x856
		skip
		loop
	endif

	if alltrim(x856.segment) = "TD5"
&& typical segment TD5�Z�2�UPSG�M�BS
		cscac = alltrim(x856.f3)
		cscac = icase(cscac = "STD" and !litd,"USTD",cscac = "STD" and litd,"UTDC",cscac)  && Added per Todd, 06.13.2017, Joe

*    	REPLACE xpt.scac     WITH ALLTRIM(x856.f3) IN xpt
		replace xpt.ship_via with alltrim(x856.f3) in xpt

		lcshipmode = alltrim(x856.f3)
		lcterms =""

		do case
		case inlist(lcshipmode,"STD")
			lltradedirectflag = .t.
			replace xpt.shipins with xpt.shipins+chr(13)+"TRADEDIRECT*YES" in xpt
			if llcodflag = .t.
				replace xpt.ship_via with "UPS TRADE DIRECT COD"
				replace xpt.scac     with "UTDC" in xpt  && indicates COD
			else
				replace xpt.ship_via with "UPS STANDARD TRADE DIRECT"
				replace xpt.scac     with "USTD" in xpt   && regular smallpkg consolidate to Canada
			endif
*** MAY NEED TO FLIP THE SCAC TO uttk if ther shipment flips to LTL over 500 lbs

		case inlist(lcshipmode,"SRG")
			replace xpt.ship_via with "SEE ROUTING GUIDE"

		case inlist(lcshipmode,"SRGT")
			lltradedirectflag = .t.
			replace xpt.shipins with xpt.shipins+chr(13)+"TRADEDIRECT*YES" in xpt
			replace xpt.ship_via with "SEE ROUTING TRADE DIRECT"

		case inlist(lcshipmode,"FDX2","FDXD")
			do case
			case inlist(alltrim(x856.f5),"BR","CC")
				replace xpt.ship_via with "FEDEX 2 DAY COLLECT", xpt.scac with "FDEN"
			case alltrim(x856.f5)="BTP"
				replace xpt.ship_via with "FEDEX 2 DAY THIRD PARTY", xpt.scac with "F2D3"
			otherwise
				replace xpt.ship_via with "FEDEX 2 DAY", xpt.scac with "FDXT"
			endcase
		case lcshipmode = "FDXS"
			do case
			case inlist(alltrim(x856.f5),"BR","CC")
				replace xpt.ship_via with "FEDEX EXP SAVER COLLECT", xpt.scac with "FENG"
			case alltrim(x856.f5)="BTP"
				replace xpt.ship_via with "FEDEX EXP SAVER THIRD PARTY", xpt.scac with "FXS3"
			otherwise
				replace xpt.ship_via with "FEDEX EXPRESS SAVER", xpt.scac with "FENT"
			endcase

		case lcshipmode = "FDEN"
			do case
			case inlist(alltrim(x856.f5),"BR","CC")
				replace xpt.ship_via with "FEDEX NEXT DAY COLLECT", xpt.scac with "FNDC"
			case alltrim(x856.f5)="BTP"
				replace xpt.ship_via with "FEDEX OVERNIGHT THIRD PARTY", xpt.scac with "FEX3"
			otherwise
				replace xpt.ship_via with "FEDEX NEXT DAY AIR", xpt.scac with "FEDX"
			endcase

**removed per Tom D, not record of it in Billabong's list - mvw 06/21/17
*!*				CASE lcShipMode = "FDXHD"
*!*					REPLACE xpt.ship_via WITH "FDX HOME DELIVERY", xpt.scac with "XXXX"

**removed per Tom D, not record of it in Billabong's list - mvw 06/21/17
*!*				CASE lcShipMode = "FDEG"
*!*					REPLACE xpt.ship_via WITH "FDX GROUND", xpt.scac with "XXXX"

**removed per Tom D, not record of it in Billabong's list - mvw 06/21/17
*!*				CASE lcShipMode = "FDXP"
*!*					REPLACE xpt.ship_via WITH "FDX INTL AIR PRIY", xpt.scac with "XXXX"

*!*        Case lcShipMode = "FDXE"
*!*          Replace xpt.ship_via With "FDX INTL GROUND", xpt.scac with "XXXX"

**removed per Tom D, not record of it in Billabong's list - mvw 06/21/17
*!*				CASE lcShipMode = "FDXIE"
*!*					REPLACE xpt.ship_via WITH "FDX INTL ECON", xpt.scac with "XXXX"

*!*        Case lcShipMode = "UPSR"
*!*          Replace xpt.ship_via With "UPS GROUND COMMERICAL", xpt.scac with "XXXX"

		case lcshipmode = "UPSN"
			do case
			case llcodflag
				replace xpt.ship_via with "UPS GROUND COD", xpt.scac with "UCOD"
			case inlist(alltrim(x856.f5),"BR","CC")
				replace xpt.ship_via with "UPS GROUND COLLECT", xpt.scac with "UPGC"
			case alltrim(x856.f5)="BTP"
				replace xpt.ship_via with "UPS 3RD PARTY - GROUND", xpt.scac with "UPSS"
			otherwise
				replace xpt.ship_via with "UPS GROUND PREPAID", xpt.scac with "UPSN"
			endcase

		case lcshipmode = "UPON"
			do case
			case inlist(alltrim(x856.f5),"BR","CC")
				replace xpt.ship_via with "UPS 3 DAY COLLECT", xpt.scac with "UPST"
			case alltrim(x856.f5)="BTP"
				replace xpt.ship_via with "UPS 3RD PARTY - 3 DAY", xpt.scac with "UPSO"
			otherwise
				replace xpt.ship_via with "UPS 3RD DAY", xpt.scac with "UPAC"
			endcase

		case lcshipmode = "UPRN"
			do case
			case inlist(alltrim(x856.f5),"BR","CC")
				replace xpt.ship_via with "UPS NEXT DAY COLLECT", xpt.scac with "UPSL"
			case alltrim(x856.f5)="BTP"
				replace xpt.ship_via with "UPS NEXT DAY THIRD PARTY", xpt.scac with "UN3P"
			otherwise
				replace xpt.ship_via with "UPS-NEXT DAY", xpt.scac with "UPND"
			endcase

		case lcshipmode = "UPBN"
			do case
			case inlist(alltrim(x856.f5),"BR","CC")
				replace xpt.ship_via with "UPS 2ND DAY COLLECT", xpt.scac with "UPTC"
			case alltrim(x856.f5)="BTP"
				replace xpt.ship_via with "UPS 2ND DAY 3RD PARTY", xpt.scac with "UPS2"
			otherwise
				replace xpt.ship_via with "UPS 2ND DAY", xpt.scac with "UPWZ"
			endcase
		endcase

		replace xpt.carrcode with lcshipmode in xpt
		replace xpt.shipins with xpt.shipins+chr(13)+"SMPKGBILLING*"+alltrim(x856.f5) in xpt
		replace xpt.shipins with xpt.shipins+chr(13)+"CARRCODE*"+alltrim(x856.f3) in xpt
		replace xpt.terms with alltrim(x856.f5) in xpt   && either BS-bill sender, BR-bill receiver, BTP bill 3rd party

		if lltradedirectflag = .t.
			replace xpt.ship_via with "TRADE DIRECT"+lcterms  in xpt
			replace batch_num with "STD" in xpt
			if llcodflag = .t.
				replace xpt.ship_via with alltrim(xpt.ship_via)+"-COD"  in xpt
				replace xpt.batch_num with alltrim(xpt.batch_num)+"COD"  in xpt
			endif
		endif

		select x856
		skip
		loop
	endif

	if trim(x856.segment) = "N1" and trim(x856.f1)="ST"
		cconsignee = STRTRAN(upper(trim(x856.f2)),"'","`") && sometimes the ship-to is a DC or Unit name So use the BT r BY for the Consignee
		replace xpt.shipins with xpt.shipins+chr(13)+"SHIPTO*"+cconsignee in xpt
		replace xpt.consignee with cconsignee in xpt
		replace xpt.shipins with xpt.shipins+chr(13)+"ACSNUM*"+alltrim(x856.f4) in xpt && Full-length DC Number
		replace xpt.cacctnum with alltrim(x856.f4) in xpt

		cdcnum = alltrim(x856.f4)
		replace xpt.shipins with xpt.shipins+chr(13)+"STORENUM*"+cdcnum in xpt && Full-length DC Number
		replace xpt.dcnum with right(cdcnum,10) in xpt

		skip 1 in x856
		if trim(x856.segment) = "N2"
			skip 1 in x856
		endif

		if trim(x856.segment) = "N3"  && address info
			m.st_addr1 = upper(trim(x856.f1))
			m.st_addr2 = upper(trim(x856.f2))
			replace xpt.address  with m.st_addr1 in xpt
			replace xpt.address2 with m.st_addr2 in xpt
		endif
		skip 1 in x856
		if trim(x856.segment) = "N4"  && address info
			if !empty(trim(x856.f3))
				m.zipbar = trim(x856.f3)
			endif
			if !empty(trim(x856.f2))
				m.st_csz = upper(trim(x856.f1))+", "+upper(trim(x856.f2))+" "+trim(x856.f3)
			else
				m.st_csz = upper(trim(x856.f1))
			endif
			replace xpt.csz with m.st_csz in xpt
			ccountry = allt(x856.f4)
			replace xpt.shipins with xpt.shipins+chr(13)+"COUNTRY*"+ccountry in xpt
			replace xpt.country with ccountry in xpt
		endif
		select x856
		skip
		loop
	endif

	if trim(x856.segment) = "N1" and trim(x856.f1)="BS"
		cbillto = STRTRAN(upper(trim(x856.f2)),"'","`")
		replace xpt.shipins with xpt.shipins+chr(13)+"BILLTO*"+cbillto in xpt
		replace xpt.shipins with xpt.shipins+chr(13)+"BILLTOCODE*"+STRTRAN(upper(trim(x856.f4)),"'","`") in xpt && Full-length DC Number

		skip 1 in x856
		if trim(x856.segment) = "N2"
			skip 1 in x856
		endif

		if trim(x856.segment) = "N3"  && address info
			billtoaddr1 = upper(trim(x856.f1))
			billtoaddr2 = upper(trim(x856.f2))
			replace xpt.shipins with xpt.shipins+chr(13)+"BILLTOADDR1*"+billtoaddr1 in xpt
			replace xpt.shipins with xpt.shipins+chr(13)+"BILLTOADDR2*"+billtoaddr2 in xpt
		endif
		skip 1 in x856
		if trim(x856.segment) = "N4"  && address info
			if !empty(trim(x856.f3))
				replace xpt.shipins with xpt.shipins+chr(13)+"BILLTOZIP*"+trim(x856.f3) in xpt
			endif
			if !empty(trim(x856.f2))
				billtocsz = upper(trim(x856.f1))+", "+upper(trim(x856.f2))+" "+trim(x856.f3)
			else
				billtocsz = upper(trim(x856.f1))
			endif
			replace xpt.shipins with xpt.shipins+chr(13)+"BILLTOCSZ*"+trim(billtocsz) in xpt
			ccountry = allt(x856.f4)
			replace xpt.shipins with xpt.shipins+chr(13)+"BILLTOCOUNTRY*"+ccountry in xpt
		endif
		select x856
		skip
		loop
	endif


	if trim(x856.segment) = "N1" and trim(x856.f1)="Z7"
		replace xpt.shipfor with upper(trim(x856.f2))
		replace xpt.shipins with xpt.shipins+chr(13)+"MARKFOR*"+STRTRAN(upper(trim(x856.f2)),"'","`")  in xpt

		skip 1 in x856
		if trim(x856.segment) = "N2"
			skip 1 in x856
		endif

		if trim(x856.segment) = "N3"  && address info
			m.st_addr1 = upper(trim(x856.f1))
			m.st_addr2 = upper(trim(x856.f2))
			replace xpt.sforaddr1  with m.st_addr1 in xpt
			replace xpt.sforaddr2  with m.st_addr2 in xpt
		endif
		skip 1 in x856
		if trim(x856.segment) = "N4"  && address info
			if !empty(trim(x856.f3))
				m.zipbar = trim(x856.f3)
			endif
			if !empty(trim(x856.f2))
				m.sforcsz = upper(trim(x856.f1))+", "+upper(trim(x856.f2))+" "+trim(x856.f3)
			else
				m.sforscz= upper(trim(x856.f1))
			endif
			replace xpt.sforcsz with m.sforcsz  in xpt
			ccountry = allt(x856.f4)
			replace xpt.shipins with xpt.shipins+chr(13)+"COUNTRY*"+ccountry in xpt
			replace xpt.country with ccountry in xpt
		endif
		select x856
		skip
		loop
	endif

	if trim(x856.segment) = "MSG"  && Warehouse handling notes
		m.sh = "SPINS"+allt(str(nshcount))+"*"+alltrim(x856.f2)  && ADDED THE ENUMERATION OF THE NOTES 4/17/2008
		nshcount = nshcount + 1
		replace xpt.shipins with xpt.shipins+chr(13)+m.sh in xpt
		select x856
		skip
		loop
	endif

	if trim(x856.segment) = "PO1" && start of PT detail, stay here until this PT is complete
		wait "NOW IN DETAIL LOOP PROCESSING" window nowait

		select xptdet
		m.ptid = xpt.ptid
		m.accountid = xpt.accountid
		m.ptdate = date()
		select x856
		nrecret = recno()
		do while alltrim(x856.segment) != "SE"
			skip 1 in x856
			if alltrim(x856.segment) = "CTT"
				if !"CTTLINES*"$xpt.shipins
					replace xpt.shipins with xpt.shipins+chr(13)+"CTTLINES*"+alltrim(x856.f1)+chr(13)+"CTTQTY*"+alltrim(x856.f2) in xpt
					exit
				endif
			endif
		enddo
		go nrecret

		do while alltrim(x856.segment) != "SE"
			do case
			case trim(x856.segment) = "PO1"
				select xpt
				scatter memvar memo blank fields except ptdetid,ptid,accountid,addby,adddt,addproc
				m.printstuff = ""
				m.style      = ""
				m.color      = ""
				m.price      = ""
				m.printstuff = "FILENAME*"+cfilename
				m.kidsflag   = ""
				m.taxrate    = ""
				m.taxonfrt   = .f.
				m.linenum = alltrim(x856.f1)

				cprepackqty = alltrim(x856.f2)
				lprepack = iif(empty(cprepackqty),.f.,.t.)
				m.totqty = iif(lprepack,int(val(cprepackqty)),0)
				m.origqty = m.totqty
				m.units = iif(lprepack,.f.,.t.)

				m.price = alltrim(x856.f4)  && keep price persistent till the next PO1 segment/
				m.printstuff = m.printstuff+chr(13)+"PRICE*"+m.price

				if alltrim(x856.f6) = "IN"
					m.style = alltrim(x856.f7)
				endif

				if alltrim(x856.f8) = "BO"
					m.color = alltrim(x856.f9)
				endif

				if inlist(alltrim(x856.f10),"D4","CU")
					m.style = alltrim(m.style)+"-"+alltrim(x856.f11)
				endif

				if alltrim(x856.f12) = "OT"
					m.printstuff = m.printstuff+chr(13)+"ACSPTNUMBER*"+alltrim(x856.f13)   &&Alltrim(x856.f4)
				endif

				if alltrim(x856.f12) = "CO"
					m.printstuff = m.printstuff+chr(13)+"CHILDPT*"+alltrim(x856.f15)   &&Alltrim(x856.f4)
				endif

				if alltrim(x856.f14) = "ON"
					m.printstuff = m.printstuff+chr(13)+"CUSTORDER*"+alltrim(x856.f15)   &&Alltrim(x856.f4)
				endif

				if alltrim(x856.f16) = "PO"
					m.printstuff = m.printstuff+chr(13)+"PONUMBER*"+alltrim(x856.f18)   &&Alltrim(x856.f4)
				endif

				if alltrim(x856.f18) = "PI"
					m.printstuff = m.printstuff+chr(13)+"PURCHITEM*"+alltrim(x856.f19)   &&Alltrim(x856.f4)
				endif

				if alltrim(x856.f20) = "PG"
					m.printstuff = m.printstuff+chr(13)+"CUSTPREPACK*"+alltrim(x856.f15)   &&Alltrim(x856.f4)
				endif

				m.pack = "1"
				m.casepack = 1
				if inlist(lcpttype,"CON","WEB")
					linecustord = alltrim(x856.f15)
					linecustpt = alltrim(x856.f13)
					linecustpo = alltrim(x856.f17)
				endif
				if alltrim(x856.f22) = "KI"
					m.printstuff = m.printstuff+chr(13)+"KIDS*"+alltrim(x856.f23)
				endif
				m.po1printstuff = m.printstuff  &&  svae this segment data to be propigated to the rest of possible records

			case trim(x856.segment) = "REF" and alltrim(x856.f1) = "T1" and alltrim(x856.f2) = "GST"
				m.printstuff = m.printstuff+chr(13)+"GSTTAXRATE*"+alltrim(x856.f3)

			case trim(x856.segment) = "REF" and alltrim(x856.f1) = "T1" and alltrim(x856.f2) = "HST"
				m.printstuff = m.printstuff+chr(13)+"HSTTAXRATE*"+alltrim(x856.f3)

			case trim(x856.segment) = "REF" and alltrim(x856.f1) = "T1" and alltrim(x856.f2) = "PST"
				m.printstuff = m.printstuff+chr(13)+"PSTTAXRATE*"+alltrim(x856.f3)

			case trim(x856.segment) = "REF" and alltrim(x856.f1) = "T2" and alltrim(x856.f2) = "GST"
				m.printstuff = m.printstuff+chr(13)+"GSTTAXRATE*"+alltrim(x856.f3)

			case trim(x856.segment) = "REF" and alltrim(x856.f1) = "T2" and alltrim(x856.f2) = "HST"
				m.printstuff = m.printstuff+chr(13)+"HSTTAXRATE*"+alltrim(x856.f3)

			case trim(x856.segment) = "REF" and alltrim(x856.f1) = "T2" and alltrim(x856.f2) = "PST"
				m.printstuff = m.printstuff+chr(13)+"PSTTAXRATE*"+alltrim(x856.f3)

			case alltrim(x856.segment) = "REF"  and alltrim(x856.f1) = "F9" and alltrim(x856.f2) = "Y"
				m.printstuff =  m.printstuff+chr(13)+"TAXONFREIGHT*"+alltrim(x856.f2)

			case trim(x856.segment) = "PO4"  && Prepack pack qty

				m.pack = alltrim(x856.f1)
				m.casepack = int(val(m.pack))
				cuom = alltrim(x856.f3)
				if !empty(alltrim(x856.f4))
					m.printstuff = m.printstuff+chr(13)+"INNERS*"+alltrim(x856.f4)
				endif

			case trim(x856.segment) = "SLN" && PnP Detail
*				SET STEP ON
				select x856
				cuom = iif(!lprepack,"EA",cuom)
				m.totqty = int(val(x856.f4))
				m.origqty = m.totqty
				m.printstuff = m.printstuff+chr(13)+"ORIGQTY*"+alltrim(x856.f4)
				m.printstuff = m.printstuff+chr(13)+"UOM*"+cuom
				if inlist(lcpttype,"CON","WEB")
					m.printstuff = m.printstuff+chr(13)+"LINECUSTORD*"+linecustord
					m.printstuff = m.printstuff+chr(13)+"LINECUSTPT*"+linecustpt
					m.printstuff = m.printstuff+chr(13)+"LINECUSTPO*"+linecustpo
				endif
				m.upc = ""
				m.id = ""
				if inlist(alltrim(x856.f9),"UP","EN")
					m.upc   = alltrim(x856.f10)
				endif
				if alltrim(x856.f11) = "IZ"
					m.id   = alltrim(x856.f12)
				endif


			case trim(x856.segment) = "SI"
				m.printstuff = m.printstuff+chr(13)+"CUSTDESC*"+alltrim(x856.f3)
				m.printstuff = m.printstuff+chr(13)+"CUSTSKU*"+alltrim(x856.f5)
				m.custsku = alltrim(x856.f5)
				m.printstuff = m.printstuff+chr(13)+"CUSTCOLOR*"+alltrim(x856.f7)

			case trim(x856.segment) = "TC2"
				m.printstuff = m.printstuff+chr(13)+"HTSCODE*"+alltrim(x856.f2)
				select xptdet
				m.ptdetid = m.ptdetid + 1
				m.addproc = "BILL850"
				m.adddt = datetime()
				m.addby = "TOLLPROC"
				m.office = coffice
				m.mod = cmod
				m.accountid = nacctnum
				insert into xptdet from memvar
        replace xptdet.ship_ref With xpt.ship_ref  In xptdet
				m.printstuff = ""
				m.printstuff = m.po1printstuff && save the memo data from the PO1 segment

				if lprepack
					scatter memvar memo
					m.ptdetid = m.ptdetid+1
					m.units = .t.
					ntotqty = m.totqty*m.casepack
					cpack = "1"
					ncasepack = 1
					append blank
					gather memvar memo
					replace xptdet.totqty with ntotqty,xptdet.pack with "1",xptdet.casepack with 1 next 1 in xptdet
				endif
				select x856
				skip 1 in x856
				if x856.segment= "PO1"
					m.printstuff = ""
					m.printstuff = "FILENAME*"+cfilename
				endif
				skip -1 in x856
			endcase
			skip 1 in x856
		enddo
		select xptdet
	endif

	select x856
	if !eof()
		skip 1 in x856
	endif
enddo

if !ltesting
	tu("ackdata")
endif

select xpt
locate
scan
	select xptdet
	detqty =0
	scan for xptdet.ptid = xpt.ptid
		detqty = detqty + xptdet.totqty
	endscan
	replace xpt.qty     with detqty in xpt
	replace xpt.origqty with detqty in xpt
endscan

select xptdet
do m:\dev\prg\setuppercase940

if lbrowfiles or ltesting
	wait window "Now browsing XPT/XPTDET files" &message_timeout2
	select xpt
	locate
	browse
	select xptdet
	locate
	browse
	if messagebox("Do you wish to continue?",4+16+256,"CONTINUE")=7
		cancel
		exit
	endif
endif

wait ccustname+" Breakdown Round complete..." window &message_timeout2

*set step On 
*Do bad_rollup_ptdet_units
Do rollup_ptdet_units
set step on

select x856
if used("PT")
	use in pt
endif
if used("PTDET")
	use in ptdet
endif
wait clear
return

********************************************************************************************
procedure naftacheck
****************************

create cursor tempchk (ship_ref c(20),style c(20),misscode l, missdata l, recfound l)
select x856
locate
scan for (x856.segment = "BEG") or (x856.f1 = "3L") or (x856.segment = "PO1") or (x856.segment = "SLN")
	do case
	case x856.segment = "BEG"
		m.ship_ref = alltrim(x856.f3)
	case x856.f1 = "3L"
		do case
		case alltrim(x856.f2) = "BB"
			nacctnum1=6744
		case alltrim(x856.f2) = "RV"
			nacctnum1=6747
		case alltrim(x856.f2) = "EL"
			nacctnum1=6718
		case alltrim(x856.f2) = "CA"
			nacctnum1=6718
		case alltrim(x856.f2) = "EC"
			nacctnum1=6718
		case alltrim(x856.f2) = "CR"
			nacctnum1=6718
		otherwise
			nacctnum1=6744
		endcase
	case x856.segment = "PO1"
		if x856.f6 = "IN"
			m.style = alltrim(x856.f7)
		endif
		if alltrim(x856.f8) = "BO"
			m.color = alltrim(x856.f9)
		endif
	case x856.segment = "SLN"
		if alltrim(x856.f11) = "IZ"
			m.id   = alltrim(x856.f12)
			xsqlexec("select * from upcmast where accountid = "+transform(nacctnum1)+" and style = '"+m.style+"' and color = '"+m.color+"' and id = '"+m.id+"'",,,"wh")
			m.recfound = iif(reccount() = 0,.f.,.t.)
			store .f. to lnafta,lhtsca,m.misscode,m.missdata
			store "" to cvalnafta,cvalhtsca
			if m.recfound
				alength = alines(aptinfo,upcmast.info,.t.,chr(13))
				lnafta = iif("NAFTA"$upcmast.info,.t.,.f.)
				lhtsca = iif("HTSCA"$upcmast.info,.t.,.f.)
				cvalnafta = alltrim(segmentget(@aptinfo,"NAFTA",alength))
				cvalhtsca = alltrim(segmentget(@aptinfo,"HTSCA",alength))
				m.misscode = iif(!lnafta or !lhtsca,.t.,.f.)
				m.missdata = iif(empty(cvalhtsca) or empty(cvalnafta),.t.,.f.)
			endif

			if m.misscode = .t. or m.missdata = .t. or m.recfound = .f.
				insert into tempchk from memvar
			endif
		endif
	endcase
endscan

select tempchk
if reccount()>0
	set step on
	tfrom ="TGF EDI Operations <toll-edi-ops@tollgroup.com>"
	tattachq = ""
	tsendtoq = iif(ltesting,"joe.bianchi@tollgroup.com","claudia@billabong-usa.com,sbredenkamp@elementbrand.com,jsalisbury@billabong-usa.com")
	tccq = iif(ltesting,"","joe.bianchi@tollgroup.com,todd.margolin@tollgroup.com")
	tsubjectq = "Missing NAFTA/HTSCA labels or data in Toll Style Master"
	tmessageq = "The following orders/styles have either missing NAFTA/HTSCA labels or empty data:"
	tmessageq = tmessageq+chr(13)+padr("ORDER#",25)+padr("STYLE",25)+"NO LABEL   "+"EMPTY   "+"NOT FOUND"+chr(13)
	select tempchk
	locate
	scan
		tmessageq = tmessageq+chr(13)+padr(alltrim(tempchk.ship_ref),25)+padr(alltrim(tempchk.style),25)+space(4)+transform(m.misscode,"Y")+space(8)+transform(m.missdata,"Y")+space(8)+transform(m.recfound,"Y")
	endscan
	do form m:\dev\frm\dartmail2 with tsendtoq,tfrom,tsubjectq,tccq,tattachq,tmessageq,"A"
else
	use in tempchk
endif

release nacctnum1,m.ship_ref,m.style,m.color,m.id,m.misscode,m.missdata,m.recfound
select x856
endproc

********************************************************************************************
procedure segmentget
****************************

parameter thisarray,lckey,nlength

for i = 1 to nlength
	if i > nlength
		exit
	endif
	lnend= at("*",thisarray[i])
	if lnend > 0
		lcthiskey =trim(substr(thisarray[i],1,lnend-1))
		if occurs(lckey,lcthiskey)>0
			return substr(thisarray[i],lnend+1)
			i = 1
		endif
	endif
endfor

return ""
********************************************************************************************
 