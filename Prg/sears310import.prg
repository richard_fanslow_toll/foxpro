************************************************************************
* Started out as 856 FILES TO send to QIVA
* AUTHOR--:PG
* DATE----:05/01/2002
* Modified much, now this takes in flat files and sends out 852,
* product activity data for Sears
************************************************************************
Do setenvi
Set Century On
Set Date To ymd
Set Decimal To 0

xoutdir="f:\ftpusers\searsedi\EdiDataOut\"

Close Data All
Store Chr(07) To segterminator
Store "~"+Chr(13)    To segterminator

*Store "" to segterminator

Store "" To fstring
Store 0 To shiplevel

Public t856, tfile, tshipid,cstring,slevel,plevel,lnInterControlnum,lcInterctrlNum,stNum,gn856Ctr
Public lcCode,lcOrigin,lcDischarge, lcDocrcpt

Store .F. To llEditUpdate

lcstring  = ""
lincnt = 1
******************************************************************************************
Wait Window "Open tables and setting up........." Nowait
*******************************************************************************************
Store 0 To EDI_testing
Use F:\searsedi\Data\controlnum In 0 Alias controlnum

********************************************************************************************************
**********************************************
*** Build Hierarchical Level *** SHIPMENT LEVEL
***********************************************
Wait Window "Setting up the cursors......." Nowait
lcpath ="f:\ftpusers\searsedi\RGTIdataIn\"
lcarchivepath ="f:\ftpusers\searsedi\RGTIdataIn\archive\"

Use F:\searsedi\Data\asn310 In 0

Select * from asn310 into cursor temp310 where .f. readwrite

Cd &lcpath

lnNum = Adir(tarray,"310*.txt")

If lnNum=0
  Messagebox("No Sears 310 Flat Files to import",16,"Sears EDI Formatter, Flat to 852...",1500)
  Return
Endif
If lnNum > 0
  For thisfile = 1  To lnNum
    xfile = lcpath+tarray[thisfile,1]
    archivefile = lcarchivepath+tarray[thisfile,1]
    Wait Window "Importing file: "+xfile Nowait
    lnHandle = Fopen(xfile)
    Do While !Feof(lnHandle)
      lcStr= Fgets(lnHandle,600)
      If Alltrim(Substr(lcStr,1,2)) = "FM"
        Select temp310
        Append Blank
        Replace partner    With Alltrim(Substr(lcStr,1,2))
        Replace hbol       With Alltrim(Substr(lcStr,169,16))
        Replace etd        With Alltrim(Substr(lcStr,196,8))
        Replace scac       With Alltrim(Substr(lcStr,204,4))
        Replace vessel     With Alltrim(Substr(lcStr,208,35))
        Replace voyage     With Alltrim(Substr(lcStr,243,10))
        Replace disdc      With Alltrim(Substr(lcStr,255,3))
        Replace port       With Alltrim(Substr(lcStr,262,35))
        Replace etadc      With Alltrim(Substr(lcStr,330,8))
        Replace vendor     With Alltrim(Substr(lcStr,356,35))
        Replace Container  With Alltrim(Substr(lcStr,391,15))
        Replace Size       With Alltrim(Substr(lcStr,406,3))
        Replace seal       With Alltrim(Substr(lcStr,416,15))
        Replace item       With Alltrim(Substr(lcStr,113,15))
        Replace wt         With Alltrim(Substr(lcStr,431,8))
        Replace cube       With Alltrim(Substr(lcStr,453,8))
        Replace ctnqty     With Alltrim(Substr(lcStr,464,5))
        Replace units      With Alltrim(Substr(lcStr,469,9))
        Replace fcr        With Alltrim(Substr(lcStr,481,15))
      Endif
    Enddo
    Fclose(lnHandle)

  Select * from temp310 where disdc = "FMS" group by hbol,container,disdc into cursor allhbols
    
  Select allhbols
  Scan
    Select allhbols
    Scatter memvar
    Select asn310
    Append Blank
    Gather memvar
  EndScan

&& here we shouldarchive and delete the files
    If !File(archivefile)
      Copy File [&xfile] To [&archivefile]
    Endif
*    If !lTestImport
*	   erase [&xfile]
*    Endif
  Next
Endif



