do case
case cstype="BOBTAIL"
	return "bobtail"
case cstype="DO CANCEL ADMIN FEE"
	return "cancel"
case cstype="CHASSIS MOVE"
	return "chas"
case cstype="STORAGE"
	return "storage"
case cstype="DELIVERY ATTEMPT"
	return "attdel"
case inlist(cstype,"DRIVER DETENTION","SEARS WAITING TIME")
	return "acctdet"
case cstype="HAZMAT"
	return "hazmat"
case cstype="PALLET"
	return "pallet"
case inlist(cstype,"RETURN TO YARD","PRE-PULL")
	return "dray"
case cstype="PICKUP ATTEMPT"
	return "attpu"
case cstype="SHUTTLE"
	return "shuttle"
case cstype="SORTING"
	return "carton"
case cstype="STOPOFF"
	return "stopoff"
case inlist(cstype,"SUMMIT CFT","SUMMIT PIER PASS")
	return "summit"
otherwise
	return "blank"
endcase
