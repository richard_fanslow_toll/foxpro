* import BBC  styles
* programmed by MB
* build exe as F:\UTIL\BBCSTYLES\BBC_888.EXE
*
* Adapted from PG's bbc_888.prg 7/10/2017 MB
*
* NOTE: added whsql.vcx ang gethandleca.prg to project for the EXE.
* 4/30/2018 MB: revised to allow each division to have its own log files, so different divisions can import styles at the same time

LPARAMETERS tcDivision

LOCAL lTestMode, lcDivision
lTestMode = .F.

lcDivision = UPPER(ALLTRIM(tcDivision))

guserid = "BBC_888"

IF NOT lTestMode THEN
	utilsetup("BBC_888")
ENDIF

DECLARE INTEGER CreateMutex IN Win32API ;
	STRING @lpMutexAttributes, ;
	STRING bInitialOwner, ;
	STRING @lpName

DECLARE INTEGER GetLastError IN Win32API

* if running in EXE check to see if already running
IF _VFP.STARTMODE > 0 THEN
	lcProcessName = "BBC_888"
	CreateMutex(0,CHR(1),@lcProcessName)
	lnError = GetLastError()
	IF lnError = 183
		WAIT WINDOW TIMEOUT 5 "This program is already running..."
		IF NOT lTestMode THEN
			schedupdate()
		ENDIF
		RETURN .F.
	ENDIF
ENDIF

loBBC_888 = CREATEOBJECT('BBC_888')
loBBC_888.MAIN( lTestMode, lcDivision )

IF NOT lTestMode THEN
	schedupdate()
ENDIF

CLOSE DATABASES ALL
RETURN

#DEFINE LOGIT 1
#DEFINE WAITIT 2
#DEFINE NOWAITIT 4
#DEFINE SENDIT 8
#DEFINE CRLF CHR(13) + CHR(10)
#DEFINE RETURN_DATA_MANDATORY .F.
#DEFINE RETURN_DATA_NOT_MANDATORY .T.
#DEFINE CSVFORMAT 6
#DEFINE WRITE_ONLY_UNBUFFERED 11
#DEFINE COMMA ","

DEFINE CLASS BBC_888 AS CUSTOM

	cProcessName = 'BBC_888'

	lTestMode = .T.  && .F. FOR NORMAL OPERATION, .T. TO SUPPRESS ALL BUT DEVELOPER EMAILS AND CHANGE INPUT TABLE, ETC.

	dToday = DATE()

	lAutoYield = .T.

	cStartTime = TTOC(DATETIME())

	* connection properties
	nSQLHandle = 0

	* folder properties

	cInputFolder1 = ''
	cArchiveFolder = ''
	cOrigFolder = ''	

*!*		* data properties
*!*		cUPCMastTable = ''
	
	nAccountID = 0   && FROM F:\SYSDATA\QQDATA\account.dbf

	cAddProc = "BBC_888"
	tADDDT = DATETIME()
	lPNP = .F.
	cDESCRIP = ''
	cSTYLE = ''
	nUPCMASTID = 0
	cDivision = ''
	cClientName = ''
	cUPC = ''
	nWEIGHT = 0
	nCUBE = 0
	nUIC = 0
	cINFO = ''
	cHTSCODE = ''
	cITEMTYPE = ''
	
	* processing properties
	lDeleteFoundRecs = .F.  && if .T. then when we find a key match we will delete existing rec and insert the new one. If .F. we will simply not insert when we find a new match.
	nRow = 0
	cStyle = ''
	nSkippedRecs = 0

	* Excel properties
	oExcel = NULL

	* wait window properties
	nWaitWindowTimeout = 2
	lWaitWindowIsOn = .T.

	* logfile properties
	lLoggingIsOn = .T.
	cLogFile = 'F:\UTIL\BBCSTYLES\STYLES\LOGFILES\BBC_888_log.txt'

	* INTERNAL email properties
	lSendInternalEmailIsOn = .T.
	cFrom = 'toll-edi-ops@tollgroup.com'
	cSendTo = 'doug.wachs@tollgroup.com'
	cCC = 'mbennett@fmiint.com, pgaidis@fmiint.com'
	cSubject = 'BBC_888 Style Import for ' + TRANSFORM(DATETIME())
	cAttach = ''
	cBodyText = ''
	cMGRSendTo = ''
	cMGRFrom = ''
	cMGRSubject = ''
	cMGRCC = ''
	cMGRAttach = ''
	cMGRBodyText = ''

	cCOMPUTERNAME = ''
	cUSERNAME = ''


	FUNCTION INIT
		IF NOT DODEFAULT()
			RETURN .F.
		ENDIF
		WITH THIS
			*SET RESOURCE OFF
			CLOSE DATA
			SET CENTURY ON
			*SET DATE AMERICAN
			*SET HOURS TO 24
			SET DECIMALS TO 2
			SET ANSI ON
			SET TALK OFF
			SET DELETED ON
			SET CONSOLE OFF
			SET EXCLUSIVE OFF
			SET SAFETY OFF
			SET EXACT OFF
			SET STATUS BAR ON
			SET SYSMENU OFF
			SET ENGINEBEHAVIOR 70
			SET MULTILOCKS ON
			_VFP.AUTOYIELD = .lAutoYield
			.cCOMPUTERNAME = GETENV("COMPUTERNAME")
			.cUSERNAME = GETENV("USERNAME")
			.lLoggingIsOn = .lLoggingIsOn AND NOT EMPTY(.cLogFile)
		ENDWITH
	ENDFUNC


	FUNCTION DESTROY
		WITH THIS
			IF .lLoggingIsOn  THEN
				SET ALTERNATE OFF
				SET ALTERNATE TO
			ENDIF
		ENDWITH
		*!*	IF TYPE('.oExcel') = 'O' AND NOT ISNULL(.oExcel) THEN
		*!*		.oExcel.QUIT()
		*!*	ENDIF
		DODEFAULT()
	ENDFUNC


	FUNCTION MAIN
		LPARAMETERS tlTestMode, tcDivision
		WITH THIS
			LOCAL lnNumberOfErrors

			TRY

				lnNumberOfErrors = 0
				
				.cStartTime = TTOC(DATETIME())

				.lTestMode = tlTestMode
				.cDivision = tcDivision

				IF .lTestMode THEN
					.cLogFile = 'F:\UTIL\BBCSTYLES\STYLES\LOGFILES\BBC_888_log_TESTMODE.txt'
					.cSendTo = 'mbennett@fmiint.com'
				ENDIF
				
				IF .lLoggingIsOn THEN
					SET ALTERNATE TO (.cLogFile) ADDITIVE
					SET ALTERNATE ON
				ENDIF

				.TrackProgress('BBC_888 Style Import process started....', LOGIT+SENDIT)
				.TrackProgress('COMPUTERNAME = ' + .cCOMPUTERNAME, LOGIT+SENDIT)
				.TrackProgress('USERNAME = ' + .cUSERNAME, LOGIT+SENDIT)
				.TrackProgress('PROJECT = BBC_888', LOGIT+SENDIT)
				
				* assign client name, processing folders and accountid based on passed parameter
				DO CASE
					CASE .cDivision == 'BASE'
						.nAccountID = 6761
						.cClientName = 'BBC Base Acct'
						.cInputFolder1 = 'F:\FTPUSERS\BBC\888IN\'
						.cArchiveFolder = 'F:\FTPUSERS\BBC\888IN\ARCHIVE\'
						.cOrigFolder = 'F:\FTPUSERS\BBC\888IN\ORIG\'
					CASE .cDivision == 'CF'
						.nAccountID = 6763
						.cClientName = 'BBC Club Foot'
						.cInputFolder1 = 'F:\FTPUSERS\BBCCF\888IN\'
						.cArchiveFolder = 'F:\FTPUSERS\BBCCF\888IN\ARCHIVE\'
						.cOrigFolder = 'F:\FTPUSERS\BBCCF\888IN\ORIG\'
					CASE .cDivision == 'FY'
						.nAccountID = 6759
						.cClientName = 'BBC Feiyue'
						.cInputFolder1 = 'F:\FTPUSERS\BBCFY\888IN\'
						.cArchiveFolder = 'F:\FTPUSERS\BBCFY\888IN\ARCHIVE\'
						.cOrigFolder = 'F:\FTPUSERS\BBCFY\888IN\ORIG\'	
					CASE .cDivision == 'RW'
						.nAccountID = 6757
						.cClientName = 'BBC Robert Wayne'
						.cInputFolder1 = 'F:\FTPUSERS\BBCRW\888IN\'
						.cArchiveFolder = 'F:\FTPUSERS\BBCRW\888IN\ARCHIVE\'
						.cOrigFolder = 'F:\FTPUSERS\BBCRW\888IN\ORIG\'	
					OTHERWISE
						* error- invalid division
						.TrackProgress('=====> ERROR; unexpected Division Parameter = ' + .cDivision, LOGIT+SENDIT)
						THROW
				ENDCASE
				
				* added 4/30/2018 MB SO THAT THE DIFFERENT DIVISIONS EACH HAVE THEIR OWN LOG FILES, ENABLING SIMUL RUN OF THEIR STYLE IMPORTS
				IF .lLoggingIsOn THEN
					.cLogFile = 'F:\UTIL\BBCSTYLES\STYLES\LOGFILES\BBC_888_' + .cDivision + '_log.txt'
					SET ALTERNATE OFF
					SET ALTERNATE TO (.cLogFile) ADDITIVE
					SET ALTERNATE ON
				ENDIF


				.cSubject = .cClientName + ' Style Import for ' + TRANSFORM(DATETIME())
				
				IF .lTestMode THEN
					.cInputFolder1 = 'F:\UTIL\BBCSTYLES\STYLES\TESTINPUT\'
					.cArchiveFolder = 'F:\UTIL\BBCSTYLES\STYLES\TESTINPUT\ARCHIVED\'
					.cOrigFolder = 'F:\UTIL\BBCSTYLES\STYLES\TESTINPUT\ORIG\'
				ENDIF
				IF .lTestMode THEN
					.TrackProgress('====> TEST MODE', LOGIT+SENDIT)
				ENDIF
				.TrackProgress('AccountID = ' + TRANSFORM(.nAccountID), LOGIT+SENDIT)
				.TrackProgress('.cInputFolder1 = ' + .cInputFolder1, LOGIT+SENDIT)
				.TrackProgress('.cArchiveFolder = ' + .cArchiveFolder, LOGIT+SENDIT)
				.TrackProgress('.cOrigFolder = ' + .cOrigFolder, LOGIT+SENDIT)

				.TrackProgress('Selecting existing styles...', LOGIT+SENDIT+NOWAITIT)
				
				useca("upcmast","wh",,,"select * from upcmast where accountid = " + ALLTRIM(STR(.nAccountID,4,0)),,"upcmastsql")
				SELECT upcmastsql
				GO bottom

				* the 888 parses will insert into TempUPCMast
				Select * ;
				  FROM upcmastsql ;
				  WHERE .F. ;
				  INTO Cursor TempUPCMast Readwrite

				.TrackProgress('Parsing 888 and importing styles...', LOGIT+SENDIT+NOWAITIT)
			
				* process first source folder
				.ProcessFolder( .cInputFolder1, .cArchiveFolder, .cOrigFolder )				

				CLOSE DATABASES ALL
				
				IF NOT .lTestMode THEN
					.FixDupeUPCs()
				ENDIF

				.TrackProgress('BBC_888 Style Import process ended normally.', LOGIT+SENDIT)

			CATCH TO loError

				.TrackProgress('There was an error.',LOGIT+SENDIT)
				.TrackProgress('The Error # is: ' + TRANSFORM(loError.ERRORNO), LOGIT+SENDIT)
				.TrackProgress('The Error Message is: ' + TRANSFORM(loError.MESSAGE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred in Program: ' + TRANSFORM(loError.PROCEDURE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred at line #: ' + TRANSFORM(loError.LINENO), LOGIT+SENDIT)
				.TrackProgress('SPREADSHEET ROW = ' + TRANSFORM(.nRow), LOGIT+SENDIT)
				.TrackProgress('SPREADSHEET STYLE = ' + .cStyle, LOGIT+SENDIT)
				lnNumberOfErrors = lnNumberOfErrors + 1
				.lSendInternalEmailIsOn = .T.
				*!*	IF TYPE('.oExcel') = 'O' AND NOT ISNULL(.oExcel) THEN
				*!*		.oExcel.QUIT()
				*!*	ENDIF

			ENDTRY   

			WAIT CLEAR
			***************** INTERNAL email results ******************************
			.TrackProgress('About to send status email.',LOGIT+SENDIT)
			.TrackProgress('The logfile is: ' + .cLogFile, LOGIT+SENDIT)
			.TrackProgress('BBC_888 Style Import process started: ' + .cStartTime, LOGIT+SENDIT)
			.TrackProgress('BBC_888 Style Import process finished: ' + TTOC(DATETIME()), LOGIT+SENDIT)

			IF .lSendInternalEmailIsOn THEN
				DO FORM dartmail2 WITH .cSendTo,.cFrom,.cSubject,.cCC,.cAttach,.cBodyText,"A"
				.TrackProgress('Sent status email.',LOGIT)
			ELSE
				.TrackProgress('Did not send status email: THIS.lSendInternalEmailIsOn = FALSE.',LOGIT+SENDIT)
			ENDIF

		ENDWITH
		RETURN
	ENDFUNC && main


	PROCEDURE ProcessFolder
		LPARAMETERS tcInputFolder, tcArchivedFolder, tcOrigFolder
		WITH THIS
			LOCAL laFiles[1,5], laFilesSorted[1,6], lnNumFiles, lnCurrentFile, lcSourceFile, lcArchivedFile, lcOrigFile
			LOCAL lnHandle, lcStr, lnCtr, oWorkbook, oWorksheet, lnMaxRow, lnRow, lcRow, lnDupeDelCtr, lnDupeCtr
			LOCAL llArchived, lnColumnF, lcColumnD, lnLength, lnWidth, lnHeight
			LOCAL lcSize, lcEAN, lcCompare, llUpdateIfFound, lnTotCount, llInsertAll

*!*				PRIVATE	m.ACCOUNTID, m.ADDPROC, m.PNP, m.DESCRIP, m.UPC, m.STYLE, m.WEIGHT, m.CUBE, m.UIC, m.INFO, m.HTSCODE, m.ITEMTYPE
*!*				PRIVATE m.ADDBY, m.UPDATEBY, m.ADDDT, m.UPDATEDT

*!*				m.ACCOUNTID = .nAccountID
*!*				m.ADDPROC = .cAddProc
*!*				m.ADDBY = .cAddProc

			PRIVATE	m.UPCMASTID, m.ACCOUNTID, m.STYLE, m.PNP, m.DESCRIP, m.UPC, m.WEIGHT, m.CUBE, m.UIC, m.INFO, m.HTSCODE, m.ITEMTYPE
			STORE 0 TO m.UPCMASTID, m.WEIGHT, m.CUBE, m.UIC
			STORE '' TO m.STYLE, m.DESCRIP, m.UPC, m.INFO, m.HTSCODE, m.ITEMTYPE
			STORE .T. TO m.PNP		

			************************************************************************
			*** for sql
			PRIVATE m.ADDPROC, m.UPDPROC, m.ADDBY, m.ADDDT, m.UPDATEBY, m.UPDATEDT
			STORE .cAddProc TO m.ADDPROC, m.UPDPROC, m.ADDBY, m.UPDATEBY
			STORE .tADDDT TO m.ADDDT, m.UPDATEDT
			************************************************************************

			lnNumFiles = ADIR(laFiles,(tcInputFolder + "*.*")) 

			IF lnNumFiles > 0 THEN

				.TrackProgress('=======> Found ' + TRANSFORM(lnNumFiles) + ' files!',LOGIT+SENDIT)

				* sort file list by date/time
				.SortArrayByDateTime(@laFiles, @laFilesSorted)

				* send an email only if files were found
				.lSendInternalEmailIsOn = .T.

				FOR lnCurrentFile = 1 TO lnNumFiles

					lcSourceFile = tcInputFolder + laFilesSorted[lnCurrentFile,1]
					lcArchivedFile = tcArchivedFolder + laFilesSorted[lnCurrentFile,1]
					lcOrigFile = tcOrigFolder + laFilesSorted[lnCurrentFile,1]

					.TrackProgress('lcSourceFile = ' + lcSourceFile,LOGIT+SENDIT)
					.TrackProgress('lcArchivedFile = ' + lcArchivedFile,LOGIT+SENDIT)
					.TrackProgress('lcOrigFile = ' + lcOrigFile,LOGIT+SENDIT)
					
					
					* move source file into ORIG folder so that, if an error causes the program to abort, the folderscan process does not keep reprocessing the bad source file in an infinite loop
					
					* if the file already exists in the orig folder, make sure it is not read-only.
					llArchived = FILE(lcOrigFile)
					IF llArchived THEN
						RUN ATTRIB -R &lcOrigFile.
						.TrackProgress('   Used ATTRIB -R for re-archiving : ' + lcOrigFile,LOGIT+SENDIT)
					ENDIF

					* archive the source file
					COPY FILE (lcSourceFile) TO (lcOrigFile)

					* always delete the source file
					*IF FILE(lcOrigFile) THEN
						DELETE FILE (lcSourceFile)
					*ENDIF
					
					* parse the 888 file into TempUPCMast cursor records...
					.CreateTempUPCMast( lcOrigFile )

					* if the file already exists in the archive folder, make sure it is not read-only.
					* this is to prevent errors we get copying into archive on a resend of an already-archived file.
					* Necessary because the files in the archive folders were sometimes marked as read-only (not sure why)
					llArchived = FILE(lcArchivedFile)
					IF llArchived THEN
						RUN ATTRIB -R &lcArchivedFile.
						*.TrackProgress('   Used ATTRIB -R for re-archiving : ' + lcSourceFile,LOGIT+SENDIT)
						.TrackProgress('   Used ATTRIB -R for re-archiving : ' + lcOrigFile,LOGIT+SENDIT)
					ENDIF

					* archive the source file
					*COPY FILE (lcSourceFile) TO (lcArchivedFile)
					COPY FILE (lcOrigFile) TO (lcArchivedFile)

					* delete the source file
					IF FILE(lcArchivedFile) THEN
						*DELETE FILE (lcSourceFile)
						DELETE FILE (lcOrigFile)
					ENDIF			

				ENDFOR && lnCurrentFile = 1 TO lnNumFiles
				
*!*					SELECT TempUPCMast
*!*					BROWSE

				STORE 0 TO lnDupeCtr, lnCtr
				
				SELECT TempUPCMast
				lnTotCount = RECCOUNT()

*!*	SELECT TempUPCMast				
*!*	COPY TO C:\A\BBC_TEMPUPC

				
				llInsertAll = .F.  && ======> set to .F. for normal behavior!
				
				SELECT TempUPCMast
				SCAN
				
					WAIT WINDOW NOWAIT 'TempUPCMast RECNO = ' + TRANSFORM(RECNO()) + ' of ' + TRANSFORM(lnTotCount)

					SCATTER MEMVAR MEMO 
					
					IF llInsertAll THEN
					
						* always insert; we would do this only if there is no pre-existing data 
						IF .lTestMode THEN
							m.UPCMASTID = 0
						ELSE
							m.upcmastid=sqlgenpk("upcmast","wh")
						ENDIF
						
						INSERT INTO upcmastsql FROM MEMVAR
						lnCtr = lnCtr + 1	
										
					ELSE
						* normal behavior; search to determine whether we're updating or inserting					
						SELECT upcmastsql
						
						* changed to use UPC as key value 2/7/2018 MB needed because it was not updating old versions of styles with style+color+id as the key
						
						*!*	LOCATE FOR (ACCOUNTID = m.ACCOUNTID) ;
						*!*		AND (STYLE == m.STYLE) ;
						*!*		AND (COLOR == m.COLOR) ;
						*!*		AND (ID == m.ID)
						
						LOCATE FOR (ACCOUNTID = m.ACCOUNTID) ;
							AND (UPC == m.UPC)
							
						IF FOUND() THEN
							REPLACE upcmastsql.PNP WITH m.PNP, upcmastsql.DESCRIP WITH m.DESCRIP, ;
								upcmastsql.COLOR WITH m.COLOR, upcmastsql.ID WITH m.ID, ;
								upcmastsql.UIC WITH m.UIC, upcmastsql.CUBE WITH m.CUBE, upcmastsql.WEIGHT WITH m.WEIGHT, upcmastsql.INFO WITH m.INFO, ;
								upcmastsql.HTSCODE WITH m.HTSCODE, upcmastsql.ITEMTYPE WITH m.ITEMTYPE, upcmastsql.PRICE WITH m.PRICE, ;
								upcmastsql.UPDPROC WITH m.UPDPROC, upcmastsql.UPDATEBY WITH m.UPDATEBY, upcmastsql.UPDATEDT WITH m.UPDATEDT IN upcmastsql
								
							lnDupeCtr = lnDupeCtr + 1
						ELSE
							
							IF .lTestMode THEN
								* DON'T WASTE IDS BECAUSE WE'RE NOT DOING A TABLE UPDATE
								m.UPCMASTID = 0
							ELSE
								m.upcmastid=sqlgenpk("upcmast","wh")
							ENDIF
							
							INSERT INTO upcmastsql FROM MEMVAR
							lnCtr = lnCtr + 1
						ENDIF
						
					ENDIF  && llInsertAll 
				
				ENDSCAN

				.TrackProgress('======> Beginning TU(upcmastsql)',LOGIT+SENDIT+NOWAITIT)
				
				IF .lTestMode THEN
					.TrackProgress('TEST MODE: did not TU(upcmastsql)',LOGIT+SENDIT)
					SELECT upcmastsql
					*COPY TO C:\A\BBC_UPCMAST
					*THROW
					BROWSE
				ELSE					
					* tableupate SQL
					IF tu("upcmastsql") THEN
						.TrackProgress('SUCCESS: TU(upcmastsql) returned .T.!',LOGIT+SENDIT)
					ELSE
						* ERROR on table update
						.TrackProgress('ERROR: TU(upcmastsql) returned .F.!',LOGIT+SENDIT)
						=AERROR(UPCERR)
						IF TYPE("UPCERR")#"U" THEN 
							.TrackProgress('Error Description = ' + UPCERR[1,2],LOGIT+SENDIT)
						ENDIF
					ENDIF
				ENDIF

				.TrackProgress(TRANSFORM(lnCtr) + ' INSERTS were made from ' + lcOrigFile,LOGIT+SENDIT)
				.TrackProgress(TRANSFORM(lnDupeCtr) + ' found records were updated',LOGIT+SENDIT)				
				.TrackProgress(TRANSFORM(lnCtr + lnDupeCtr) + ' total style records were processed',LOGIT+SENDIT)				
				
			ELSE
				.TrackProgress('Found no files in the BBC_888 source folder', LOGIT+SENDIT)
			ENDIF
			
			WAIT CLEAR

		ENDWITH
		RETURN
	ENDPROC  &&  ProcessFolder
	

	FUNCTION CreateTempUPCMast
		LPARAMETERS tcOrigFile
		WITH THIS
			LOCAL lnRecCount

			.TrackProgress('====> Parsing file ' + tcOrigFile, LOGIT+SENDIT)
		

			DO m:\dev\prg\createx856a
			DO m:\dev\prg\loadedifile WITH tcOrigFile,"|","TILDE"

			m.accountid = .nAccountID
			m.pnp = .T.
			m.uom = "EA"
			m.info = ""
			lcItemType =""

			SELECT x856
			SCAN
				DO CASE
					CASE TRIM(x856.segment) = "ISA"
						cisa_num = ALLTRIM(x856.f13)
					CASE TRIM(x856.segment) = "GS"
						cgs_num = ALLTRIM(x856.f6)
						lcCurrentGroupNum = ALLTRIM(x856.f6)
					CASE TRIM(x856.segment) = "ST" AND TRIM(x856.f1) = "888"
						m.groupnum=lcCurrentGroupNum
						m.isanum=cisa_num
						m.transnum=x856.f2
						m.edicode="OW"
						m.loaddt=DATE()
						m.loadtime=DATETIME()
						m.filename=tcOrigFile
						* insertinto("ackdata","wh",.t.)
					CASE x856.segment = "G39"
						m.itemtype = UPPER(ALLT(x856.f19))

						m.uic= VAL(ALLT(x856.f17))
						
						* added 7/14/2017 because UIC in sql can only be 3 digits, but we were getting values of 27684 for BBC Base Acct
						* this was causing numeric overflow errors that crashed the Table Update()
						IF m.uic > 999 THEN
							m.uic = 999
						ENDIF
						
						m.weight = VAL(ALLT(x856.f5))

						m.length = ALLT(x856.f12)
						m.width  = ALLT(x856.f10)
						m.depth  = ALLT(x856.f8)
						m.cube = (VAL(m.length)*VAL(m.width)*VAL(m.depth))/1728.0

					CASE x856.segment = "N9" AND ALLT(x856.f1)= "VA"
						m.style = UPPER(ALLT(x856.f2))

					CASE x856.segment = "N9" AND ALLT(x856.f1)= "CL"
						m.color = UPPER(ALLT(x856.f2))

					CASE x856.segment = "N9" AND ALLT(x856.f1)= "SZ"
						m.id = UPPER(ALLT(x856.f2))

					CASE x856.segment = "N9" AND ALLT(x856.f1)= "PR"
						m.price = VAL(ALLT(x856.f2))

					CASE x856.segment = "N9" AND ALLT(x856.f1)= "HHT"
						m.htscode = UPPER(ALLT(x856.f2))

					CASE x856.segment = "N9" AND ALLT(x856.f1)= "UP"
						m.upc = UPPER(ALLT(x856.f2))

					* must get bRAND info from N9*BR before inserting 07/28/2017 per PG
					*!*	CASE x856.segment = "N9" AND ALLT(x856.f1)= "LI"
					*!*		m.descrip = UPPER(ALLT(x856.f2))
					*!*		m.info = "LENGTH*"+m.length+CHR(13)+"WIDTH*"+m.width+CHR(13)+"DEPTH*"+m.depth
					*!*		INSERT INTO TempUPCMast FROM MEMVAR
					*!*		SCATTER MEMVAR MEMO BLANK
					*!*		m.info = ""
						
					CASE x856.segment = "N9" AND ALLT(x856.f1)= "LI"
						m.descrip = UPPER(ALLT(x856.f2))
						
					CASE x856.segment = "N9" AND ALLT(x856.f1)= "BR"
						m.brand = UPPER(ALLT(x856.f2))
						m.info = "LENGTH*"+m.length+CHR(13)+"WIDTH*"+m.width+CHR(13)+"DEPTH*"+m.depth+CHR(13)+"BRAND*"+m.brand
						INSERT INTO TempUPCMast FROM MEMVAR
						SCATTER MEMVAR MEMO BLANK
						m.info = ""

				ENDCASE

			ENDSCAN
			
			SELECT TempUPCMast
			lnRecCount = RECCOUNT()
			.TrackProgress('TempUPCMast Reccount = ' + TRANSFORM(lnRecCount), LOGIT+SENDIT)

		ENDWITH
		RETURN
	ENDPROC  &&  ProcessFolder


	PROCEDURE FixDupeUPCs
		WITH THIS
				LOCAL lnUpcmastid, lnReplaces
				
				useca("upcmast","wh",,,"select * from upcmast where accountid = " + ALLTRIM(STR(.nAccountID,4,0)),,"upcmastsql")
				SELECT upcmastsql
				GO bottom
				
				SELECT UPC, COUNT(*) AS CNT ;
					FROM upcmastsql ;
					INTO CURSOR CURDUPES ;
					WHERE NOT EMPTYNUL(UPC) ;
					GROUP BY UPC ;
					ORDER BY UPC ;
					HAVING CNT > 1

*!*					SELECT CURDUPES
*!*					BROWSE

				lnReplaces = 0
								
				SELECT CURDUPES
				SCAN
					SELECT upcmastsql
					LOCATE FOR UPC == CURDUPES.UPC
					IF FOUND() THEN
						lnUpcmastid = upcmastsql.upcmastid
						SELECT upcmastsql
						SCAN FOR (UPC == CURDUPES.UPC) AND (NOT upcmastid = lnUpcmastid)	
							REPLACE upcmastsql.UPC WITH "" IN upcmastsql
							lnReplaces = lnReplaces + 1
						ENDSCAN
					ENDIF
				ENDSCAN

				.TrackProgress('====> In FixDupeUPCs() # of duplicates blanked = ' + TRANSFORM(lnReplaces),LOGIT+SENDIT)
				
				IF (lnReplaces > 0) THEN
					IF tu("upcmastsql") THEN
						.TrackProgress('SUCCESS: TU(upcmastsql) in FixDupeUPCs() returned .T.!',LOGIT+SENDIT)
					ELSE
						* ERROR on table update
						.TrackProgress('ERROR: TU(upcmastsql) in FixDupeUPCs() returned .F.!',LOGIT+SENDIT)
						=AERROR(UPCERR)
						IF TYPE("UPCERR")#"U" THEN 
							.TrackProgress('Error Description = ' + UPCERR[1,2],LOGIT+SENDIT)
						ENDIF
					ENDIF
				ELSE
					.TrackProgress('TU(upcmastsql) in FixDupeUPCs() is not needed!',LOGIT+SENDIT)
				ENDIF				
		
		ENDWITH
		RETURN 
	ENDPROC  && CheckDupeUPCs
	
* dy 2/23/18
*!*		FUNCTION ExecSQL
*!*			LPARAMETERS tcSQL, tcCursorName, tlNoDataReturnedIsOkay
*!*			LOCAL llRetval, lnResult
*!*			WITH THIS
*!*				* close target cursor if it's open
*!*				IF USED(tcCursorName)
*!*					USE IN (tcCursorName)
*!*				ENDIF
*!*				WAIT WINDOW "Fetching remote data; please wait..." NOWAIT
*!*				lnResult = SQLEXEC(.nSQLHandle, tcSQL, tcCursorName)
*!*				llRetval = ( lnResult > 0 )
*!*				IF llRetval THEN
*!*					* see if any data came back
*!*					IF NOT tlNoDataReturnedIsOkay THEN
*!*						IF (NOT USED(tcCursorName)) OR EOF(tcCursorName) THEN
*!*							llRetval = .F.
*!*							.TrackProgress('No data was returned by this query: ' +CRLF + tcSQL, LOGIT+SENDIT)
*!*							.cSendTo = 'mbennett@fmiint.com'
*!*						ENDIF
*!*					ENDIF
*!*				ELSE
*!*					.TrackProgress("Result Code [" + TRANSFORM(lnResult) + "] returned by this query: " + tcSQL, LOGIT+SENDIT)
*!*					.cSendTo = 'mbennett@fmiint.com'
*!*				ENDIF
*!*				WAIT CLEAR
*!*				RETURN llRetval
*!*			ENDWITH
*!*		ENDFUNC


	PROCEDURE TrackProgress
		* do any combination of Wait Window, writing to logfile, and adding to body of email,
		* based on nFlags parameter.
		LPARAMETERS tcExpression, tnFlags
		WITH THIS
			IF BITAND(tnFlags,LOGIT) = LOGIT THEN
				IF .lLoggingIsOn THEN
					?
					? .cProcessName + "  " + TTOC(DATETIME()) + ": " + tcExpression
				ENDIF
			ENDIF
			IF .lWaitWindowIsOn THEN
				IF BITAND(tnFlags,NOWAITIT) = NOWAITIT THEN
					WAIT WINDOW tcExpression NOWAIT
				ENDIF
				IF BITAND(tnFlags,WAITIT) = WAITIT THEN
					WAIT WINDOW tcExpression TIMEOUT .nWaitWindowTimeout
				ENDIF
			ENDIF
			IF BITAND(tnFlags,SENDIT) = SENDIT THEN
				IF .lSendInternalEmailIsOn THEN
					.cBodyText = .cBodyText + tcExpression + CRLF + CRLF
				ENDIF
			ENDIF
		ENDWITH
	ENDPROC  &&  TrackProgress


	PROCEDURE SortArrayByDateTime
		* expects taInArray[] to be the result of an ADIR()
		LPARAMETERS taInArray, taOutArray
		EXTERNAL ARRAY taInArray, taOutArray
		LOCAL lnRows, lnCols, i, j
		* extending taInArray[] to process the files in date/time order
		lnRows = ALEN(taInArray,1)
		lnCols = ALEN(taInArray,2)
		* create taOutArray with one more column
		DIMENSION taOutArray[lnRows,lnCols + 1]
		* and fill it with taInArray columns AND (filedate + filetime) in new column
		* ( ACOPY() doesn't preserve elements properly because of different # of columns )
		FOR i = 1 TO lnRows
			FOR j = 1 TO lnCols
				taOutArray[i,j] = taInArray[i,j]
			ENDFOR
			taOutArray[i,lnCols + 1] = DTOS(taInArray[i,3]) + "_" + taInArray[i,4]
		ENDFOR
		* sort taOutArray on date/time column
		=ASORT(taOutArray,6)
	ENDPROC

ENDDEFINE
