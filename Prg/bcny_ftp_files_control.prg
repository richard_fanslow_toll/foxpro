**This scriptwill identify files not FTP'd successfully
utilsetup("BCNY_FTP_FILES_CONTROL")
Set Exclusive Off
Set Deleted On
Set Talk Off
Set Safety Off 
WITH _SCREEN	
	.AUTOCENTER = .T.
	.WINDOWSTATE = 0
	.BORDERSTYLE = 1
	.WIDTH = 320
	.HEIGHT = 210
	.TOP = 290
	.LEFT = 110
	.CLOSABLE = .T.
	.MAXBUTTON = .t.
	.MINBUTTON = .T.
	.SCROLLBARS = 0
	.CAPTION = "BCNY_FTP_FILES_CONTROL"
ENDWITH	

_screen.WindowState=1

If !Used("edi_trigger")
  use f:\3pl\data\edi_trigger shared In 0
ENDIF

*xsqlexec("select * from ftplog where dd>={"+dtoc(DATE()-10)+"}",,,"wh")

If !Used("ftplog")
  use f:\wh\ftplog shared In 0
ENDIF
SELECT * FROM ftplog WHERE dd>=DATE()-10 INTO CURSOR bcftplog
USE IN ftplog

SELECT edi_trigger
SELECT file945, SPACE(30) as trig945 FROM edi_trigger where INLIST(accountid,6521) AND edi_type='945 ' AND WHEN_proc > DATE()-4;
and ship_ref != '8351778-A ' AND WHEN_proc<datetime()-4000  into cursor lookup945 readwrite
USE IN edi_trigger
SELECT  lookup945
scan
pos1=ATC('945b',file945)
replace trig945 with substr(file945,pos1,36) 
ENDSCAN
REPLACE trig945 WITH FILE945 FOR FILE945='_TOLLSP'

SET STEP ON 
select distinct trig945 from lookup945 where !EMPTY(trig945) into cursor lookup945_2 READWRITE 
replace trig945 WITH LOWER(trig945) FOR !EMPTY(trig945) IN lookup945_2
*!*	COPY FILE \\app1\fmisys\FTPLogs\*.txt TO \\app1\fmisys\FTPLogs\copy\ftpcopybcny.txt
*!*	CREATE CURSOR ftplogtmb (logdata C(200))
*!*	APPE from \\app1\fmisys\FTPLogs\copy\ftpcopybcny.txt TYPE DELI
SET STEP ON 
Select *, SPACE(40) as file945  FROM bcftplog WHERE 'Sent file' $logdata AND 'synclaire\945out\' $logdata AND '.edi successfully' $logdata AND !'_tollsp'  $(logdata) INTO CURSOR log945a READWRITE
scan
pos1=ATC('out\945',logdata)
replace file945 with substr(logdata,pos1+4,24) 
ENDSCAN

Select *, SPACE(40) as file945  FROM bcftplog WHERE 'Sent file' $logdata AND 'synclaire\945out\' $logdata AND '.edi successfully' $logdata AND '_tollsp'  $(logdata) INTO CURSOR log945b READWRITE
scan
pos1=ATC('_to',logdata)
replace file945 with substr(logdata,pos1,30) 
ENDSCAN

SELECT * FROM log945a UNION ALL SELECT * FROM log945b INTO CURSOR log945 READWRITE 
SELECT * FROM lookup945_2  left JOIN log945 ON trig945  = file945   INTO CURSOR compare945 readwrite
SELECT trig945 FROM compare945 where ISNULL(file945) INTO CURSOR missing945 readwrite
*!*DELETE file \\app1\fmisys\FTPLogs\copy\ftpcopybcny.txt
SET STEP ON 

SELECT missing945

If Reccount() > 0
  Export To "S:\BCNY\945_FTP_controls"  Type Xls
  tsendto = "todd.margolin@tollgroup.com"
  tattach = "S:\BCNY\945_FTP_controls.xls"
  tcc =""
  tFrom ="Toll EDI System Operations <fmi-transload-ops@fmiint.com>"
  tmessage = "BCNY missing 945s from FTPlog "+Ttoc(Datetime())
  tSubject = "BCNY missing 945s from FTPlog"
  Do Form m:\dev\frm\dartmail2 With tsendto,tFrom,tSubject, tcc,tattach,tmessage  ,"A"
ELSE 
  tsendto = "tmarg@fmiint.com"
  tattach = ""
  tcc =""
  tFrom ="Toll EDI System Operations <fmi-transload-ops@fmiint.com>"
  tmessage = "NO_BCNY missing successful 945 FTP "+Ttoc(Datetime())
  tSubject = "NO_BCNY missing successful 945 FTP_exist"
  Do Form m:\dev\frm\dartmail2 With tsendto,tFrom,tSubject, tcc,tattach,tmessage  ,"A"
ENDIF



Close Data All
schedupdate()
_Screen.Caption=gscreencaption