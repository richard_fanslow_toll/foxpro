*tloadStatus
*11/10/03
*

parameters lwebsite, lavailable

*xoffice="C"
*xaccountid=5687

xacctname=acctname(xaccountid)

use f:\wo\wodata\daily in 0
use f:\wo\wodata\wolog in 0
use f:\wo\wodata\detail in 0
use f:\wo\wodata\manifest in 0

xsqlexec("select * from pudlocs",,,"wo")
index on accountid tag accountid
set order to

cfilter = "w.accountid = xaccountid and w.OFFICE = xOffice AND w.type = 'O' AND Between(W.WO_DATE, Date()-90, Date())"
*!*	SELECT a.* FROM wolog w left JOIN availlog a ON a.WO_NUM = w.WO_NUM ;
*!*		WHERE &cFilter and !Empty(a.avail) and !ISNULL(a.wo_num) ;
*!*	  INTO CURSOR csrTemp

xsqlexec("select * from availlog where accountid="+transform(xaccountid)+" " + ;
	"and office='"+xoffice+"' and called>={"+dtoc(date()-90)+"}",,,"wo")
index on wo_num tag wo_num
set order to
	
select a.* from wolog w left join availlog a on a.wo_num = w.wo_num ;
	where &cfilter and !isnull(a.wo_num) ;
	into cursor csrtemp

select * from csrtemp a where a.called = (select max(called) from availlog a2 where a2.wo_num = a.wo_num) ;
	group by wo_num ;
	into cursor csravail

**if want only availabel frt, need to see avail rec, else NULL
store iif(lavailable, "!empty(a.avail) AND !IsNull(a.avail)", "(empty(a.avail) OR IsNull(a.avail))") to cavailfltr

select w.*, {} as ftexp, ;
	iif(w.type='A' and !empty(w.pickedup), left(dtoc(w.pickedup),5), left(dtoc(a.called),5)) as called, iif(!isnull(d.wo_num), padr("P/U In Progress",30), ;
	iif(w.type='A' and !empty(w.pickedup), padr("Available",30), padr(a.remarks,30))) as status, ;
	iif(w.type="A", padr("Forwarder",10), iif(w.type = 'D', padr("Type",10), padr("Vessel",10))) as lbl_field1, ;
	iif(w.type="A", left(w.fwdr, 15), iif(w.type = 'D', ;
	iif(w.size="CONSOL", padr("Delivery",15), padr("Pick Up",15)), left(w.vessel, 15))) as txt_field1, ;
	iif(w.type="D", padr(chr(13)+"Ref No",15), padr("Broker"+chr(13)+"Ref No",15)) as lbl_field2, ;
	iif(w.type="D", padr(w.brokref,15), padr(w.brokref,15)) as txt_field2, ;
	iif(w.type="A", padr("Master AWB",10), iif(w.type = 'D', padr("From Loc",10),	padr("Container",10))) as lbl_field3, ;
	iif(w.type="A", padr(dispawb(w.awb),20), iif(w.type = 'D', padr(p.acct_name,20), padr(dispcont(w.container),20))) as txt_field3, ;
	iif(w.type="D", padr("Origin",15), padr("P/U From",15)) as lbl_field4, ;
	iif(w.type="D", padr(p.city,12), padr(w.puloc,12)) as txt_field4 ;
	from wolog w ;
	left join csravail a on a.wo_num = w.wo_num ;
	left join daily d on d.wo_num = w.wo_num and d.dispatched=date() and d.action='PU' and !d.undel and empty(d.pickedup) ;
	left join detail det on det.wo_num = w.wo_num ;
	left join manifest mfst on mfst.orig_wo = w.wo_num ;
	left join pudlocs p on str(p.accountid,4)+str(p.dash,4) = str(w.accountid,4)+str(w.fromloc,4) ;
	where &cfilter and &cavailfltr and !isnull(det.wo_num) and isnull(mfst.wo_num) ; &&only frt that is not mfst'ed
	group by w.wo_num ;
	order by w.type, w.wo_num ;
	into cursor temp

=seek(xoffice,"offices","locale")
goffcity=offices.city

store "All "+iif(lavailable, "Available, Unshipped", "Unavailable")+" Freight" to creporttitle
store "Ocean Transload Status Report" to creportname

select temp
if lwebsite &&create .pdf file put into MEMO field of PDF_FILE.DBF
	loadwebpdf("OCEANRPT")
endif

use in temp
use in daily
use in availlog
use in wolog
use in detail
use in manifest
use in pudlocs
