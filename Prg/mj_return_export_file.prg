utilsetup("MJ_RETURN_EXPORT_FILE")
set exclusive off
set deleted on
set talk off
set safety off

schedupdate()
with _screen
	.autocenter = .t.
	.windowstate = 0
	.borderstyle = 1
	.width = 320
	.height = 210
	.top = 290
	.left = 110
	.closable = .t.
	.maxbutton = .t.
	.minbutton = .t.
	.scrollbars = 0
	.caption = "MJ_RETURN_EXPORT_FILE"
endwith
**********************************START NJ
close databases all
goffice='J'
useca("inwolog","wh",,,"mod='"+goffice+"' and  accountid in (6303,6325,6543) and confirmdt>{"+dtoc(date()-10)+"}")
useca("indet","wh",,,"mod='"+goffice+"' and  accountid in (6303,6325,6543) and date_rcvd>{"+dtoc(date()-10)+"}")
 

upcmastsql (6303)
select a.wo_num,substr(unbilledcomment,1,2)+'/'+substr(unbilledcomment,3,2)+'/20'+substr(unbilledcomment,5,2)as phy_rcpt_dt,ttod(confirmdt) as wms_rcpt_dt,;
	000000 as whse, a.accountid as wms_loc,acctname,reference as ra,unbilledcomment as cartonid, space(4) as shp_car, alltrim(style)+'-'+alltrim(color) as item, id as size, totqty,;
	style,color from inwolog a , indet b where  inlist(a.accountid,6303,6325,6543) and zreturns and a.inwologid=b.inwologid and container='PL' and emptynul(axdt) and !emptynul(confirmdt) into cursor n1 readwrite
replace all whse with 10100 for wms_loc=6303 in n1
replace all whse with 10180 for wms_loc=6325 in n1
replace all whse with 10190 for wms_loc=6543 in n1
select distinct wo_num from n1 into cursor n2 readwrite
select inwolog
scan for inlist(accountid,6303,6325,6543) and zreturns
	select n2
	locate for n2.wo_num=inwolog.wo_num
	if found("n2")
		replace axdt with date()  in inwolog
	endif   
endscan
	
tu('inwolog')

**********************************START ML
use in inwolog
use in indet
goffice='L'
useca("inwolog","wh",,,"mod='"+goffice+"' and  accountid in (6303,6325,6543) and confirmdt>{"+dtoc(date()-90)+"}")
useca("indet","wh",,,"mod='"+goffice+"' and  accountid in (6303,6325,6543) and date_rcvd>{"+dtoc(date()-90)+"}")

useca("cartret","wh",.t.,,,,"mj_carton_returns")

useca('mjraret','wh',.t.)

select a.wo_num,substr(unbilledcomment,1,2)+'/'+substr(unbilledcomment,3,2)+'/20'+substr(unbilledcomment,5,2)as phy_rcpt_dt,ttod(confirmdt) as wms_rcpt_dt,;
	000000 as whse, a.accountid as wms_loc,acctname,reference as ra,unbilledcomment as cartonid, space(4) as shp_car, alltrim(style)+'-'+alltrim(color) as item, id as size, totqty,;
	style,color from inwolog a , indet b where inlist(a.accountid,6303,6325,6543) and zreturns  and a.inwologid=b.inwologid and container='ML' and emptynul(axdt) and !emptynul(confirmdt) into cursor m1 readwrite
replace all whse with 11100 for wms_loc=6303 in m1
replace all whse with 11180 for wms_loc=6325 in m1
replace all whse with 11190 for wms_loc=6543 in m1
select distinct wo_num from m1 into cursor m2 readwrite

select inwolog
scan for inlist(accountid,6303,6325,6543) and zreturns
	select m2
	locate for m2.wo_num=inwolog.wo_num
	if found("m2")
		replace axdt with date()  in inwolog
	endif   
	endscan

	tu('inwolog')

	select * from n1 union all select * from m1  into cursor t1 readwrite

	select wo_num,phy_rcpt_dt, wms_rcpt_dt, whse, wms_loc,acctname,ra,cartonid,shp_car,upc,item,size,totqty from t1 a left join upcmast b on a.style=b.style and a.color=b.color and a.size=b.id and b.accountid=6303 into cursor c1 readwrite

*!*	If !Used("mj_ra_return")
*!*		Use F:\wh\mj_ra_return.Dbf In 0
*!*	Endif

*********outslip
	set step on
	select * from c1 where  substr(ra,1,1)='2' and substr(ra,7,1)=' '  and substr(ra,6,1)!=' '   into cursor vout readwrite
	select wo_num,phy_rcpt_dt, wms_rcpt_dt, whse, wms_loc,acctname, cust_acct, b.claim as ra, b.ra as cust_ref,a.cartonid,shp_car,upc,item,size,totqty from vout a left join mjraret b on a.ra=b.ra ;
		into cursor vout2 readwrite

*********NON outslip  10-  RA
	select * from  c1 where substr(ra,1,3)='10-'  and !(inlist(substr(ra,4,1),'6','7')  and substr(ra,14,1)=' '  ) into cursor nonout10 readwrite
	select wo_num,phy_rcpt_dt, wms_rcpt_dt, whse, wms_loc,acctname, cust_acct, a.ra,a.cartonid,shp_car,upc,item,size,totqty from  nonout10 a left join mjraret b on a.ra=b.ra ;
		into cursor nonout10_2 readwrite
	select wo_num,phy_rcpt_dt, wms_rcpt_dt, whse, wms_loc,acctname, cust_acct, a.ra, b.claim,a.cartonid,shp_car,upc,item,size,totqty from  nonout10_2 a left join mj_carton_returns b on a.ra=b.ra ;
		and a.cartonid=b.carton_id into cursor nonout10_3 readwrite

*********NON outslip  6 or 7 RA
	select * from  c1 where substr(ra,1,3)='10-'  and (inlist(substr(ra,4,1),'6','7')  and substr(ra,14,1)=' '  )  into cursor nonout67 readwrite
	select wo_num,phy_rcpt_dt, wms_rcpt_dt, whse, wms_loc,acctname, cust_acct, a.ra,a.cartonid,shp_car,upc,item,size,totqty from  nonout67 a left join mjraret b on a.ra=b.ra ;
		into cursor nonout67_2 readwrite
	select wo_num,phy_rcpt_dt,wms_rcpt_dt, whse, wms_loc,acctname, cust_acct, a.ra, b.claim,a.cartonid,shp_car,upc,item,size,totqty from  nonout67_2 a left join mj_carton_returns b on alltrim(a.ra)=alltrim(b.ra) ;
		and a.cartonid=b.carton_id into cursor nonout67_3 readwrite
	select wo_num,phy_rcpt_dt,wms_rcpt_dt, whse, wms_loc,acctname, cust_acct, substr(ra,4,9) as ra, claim, cartonid,shp_car,upc,item,size,totqty from  nonout67_3 into cursor nonout67_4 readwrite

	select * from vout2 union select * from nonout10_3 union select * from nonout67_4 into cursor final readwrite

	set step on

copy to "S:\MarcJacobsData\Reports\Returns_inventory\mjreturns"  type csv
copy to "S:\MarcJacobsData\Reports\Returns_inventory\archive\mjreturns"+ttoc(datetime(),1)+".csv"  type csv


	select final
	if reccount() > 0
*EXPORT TO "S:\MarcJacobsData\Reports\Returns_inventory\RETURNS_FILE_LOAD_FOR_AX_"+ttoc(datetime(),1)  TYPE xls
		*tsendto = "A.AOUEILLE@marcjacobs.com,I.CASTILLO@marcjacobs.com,V.Gil@marcjacobs.com, r.walsh@marcjacobs.com,O.BATISTA@marcjacobs.com, j.valencia@marcjacobs.com,e.barnes@marcjacobs.com"
		tsendto = "MJTollReturns@marcjacobs.com"
		tattach = "S:\MarcJacobsData\Reports\Returns_inventory\mjreturns.csv"
		*tcc ="chad.linville@tollgroup.com,E.SKRZYNIARZ@marcjacobs.com,L.LOPEZ@marcjacobs.com,M.deLaCruz@marcjacobs.com"
		tcc = ""
		tfrom ="Toll EDI System Operations <fmi-transload-ops@fmiint.com>"
		tmessage = "MJ returns file load for AX created"+ttoc(datetime()) +"             from mj_return_export_file.prg"
		tsubject = "MJ returns file load for AX created"
		do form m:\dev\frm\dartmail2 with tsendto,tfrom,tsubject, tcc,tattach,tmessage  ,"A"
	else
*EXPORT TO "S:\MarcJacobsData\Reports\Returns_inventory\NO_RETURNS_FILE_LOAD_FOR_AX_"+ttoc(datetime(),1)  TYPE xls
		*tsendto =  "A.AOUEILLE@marcjacobs.com,I.CASTILLO@marcjacobs.com,V.Gil@marcjacobs.com, r.walsh@marcjacobs.com,O.BATISTA@marcjacobs.com, j.valencia@marcjacobs.com,e.barnes@marcjacobs.com"
		tsendto = "MJTollReturns@marcjacobs.com"
		tattach = ""
		*tcc ="chad.linville@tollgroup.com,E.SKRZYNIARZ@marcjacobs.com,L.LOPEZ@marcjacobs.com,M.deLaCruz@marcjacobs.com"
		tcc = ""
		tfrom ="Toll EDI System Operations <fmi-transload-ops@fmiint.com>"
		tmessage = "NO MJ returns file load for AX created "+ttoc(datetime())+"             from mj_return_export_file.prg"
		tsubject = "NO MJ returns file load for AX created"
		do form m:\dev\frm\dartmail2 with tsendto,tfrom,tsubject, tcc,tattach,tmessage  ,"A"
	endif

	select *, space(5) as whs_rtl from final into cursor final2 readwrite
	replace whs_rtl with 'RTL' for cust_ref='2'
	replace whs_rtl with 'WHLS' for cust_ref !='2'

	if reccount() > 0
*EXPORT TO S:\MarcJacobsData\Reports\mjdashboard\mjkpireturns xls
		export to  "S:\MarcJacobsData\Reports\Returns_inventory\mjreturns"  type xls
		*tsendto = "E.SKRZYNIARZ@marcjacobs.com,L.LOPEZ@marcjacobs.com,l.sharma@marcjacobs.com,e.barnes@marcjacobs.com,B.Liu@marcjacobs.com,S.price@marcjacobs.com"
		tsendto = "MJTollReturns@marcjacobs.com"
		tattach = "S:\MarcJacobsData\Reports\Returns_inventory\mjreturns.XLS"
		*tcc ="chad.linville@tollgroup.com"
		tcc = ""
		tfrom ="Toll EDI System Operations <fmi-transload-ops@fmiint.com>"
		tmessage = "MJ returns file for RPro created"+ttoc(datetime()) +"             from mj_return_export_file.prg"
		tsubject = "MJ returns file for RPro created"
		do form m:\dev\frm\dartmail2 with tsendto,tfrom,tsubject, tcc,tattach,tmessage  ,"A"
	else
		*tsendto =  "E.SKRZYNIARZ@marcjacobs.com,L.LOPEZ@marcjacobs.com,l.sharma@marcjacobs.com,e.barnes@marcjacobs.com,B.Liu@marcjacobs.com,S.price@marcjacobs.com"
		tsendto = "MJTollReturns@marcjacobs.com"
		tattach = ""
		*tcc ="chad.linville@tollgroup.com"
		tcc = ""
		tfrom ="Toll EDI System Operations <fmi-transload-ops@fmiint.com>"
		tmessage = "NO MJ returns file for RPro created "+ttoc(datetime())+"             from mj_return_export_file.prg"
		tsubject = "NO MJ returns file for RPro created"
		do form m:\dev\frm\dartmail2 with tsendto,tfrom,tsubject, tcc,tattach,tmessage  ,"A"
	endif

	close data all

	_screen.caption=gscreencaption
	on error
