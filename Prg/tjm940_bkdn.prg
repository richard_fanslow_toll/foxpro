SELECT intake
locate

SELECT DISTINCT ship_ref FROM intake INTO CURSOR temppt
m.ptid = 0
m.ptdetid = 0

SELECT temppt
LOCATE
SCAN
	cShip_ref = ALLTRIM(temppt.ship_ref)

	SELECT intake
	SUM intake.totqty TO m.qty FOR intake.ship_ref = cShip_ref
	LOCATE FOR intake.ship_ref = cShip_ref

	SCATTER MEMVAR
	m.ptid = m.ptid + 1
	m.origqty = m.qty
	m.shipins = "FILENAME*"+cFilename
	m.office = cOffice
	m.mod = cMod
	m.addby = "TJM940"
	m.adddt = DATETIME()
	m.addproc = "TJM940"
	m.accountid =  nAcctNum
	m.ptdate = DATE()
	INSERT INTO xpt FROM MEMVAR

	SCAN FOR intake.ship_ref = cShip_ref
		SCATTER MEMVAR
		m.origqty = m.totqty
		m.ptid = xpt.ptid
		m.ptdetid = m.ptdetid + 1
		m.addby = "TJM940"
		m.adddt = DATETIME()
		m.addproc = "TJM940"
		m.office = cOffice
		m.mod = cMod
		m.accountid =  nAcctNum
		m.units = .t.
		m.printstuff = "FILENAME*"+cFilename+CHR(13)+"DESC*"+ALLTRIM(m.desc)
		INSERT INTO xptdet FROM MEMVAR
	ENDSCAN

ENDSCAN

IF lTesting && OR DATETIME()<DATETIME(2017,11,22,15,00,00)
	SELECT xpt
	LOCATE
	BROWSE
	SELECT xptdet
	LOCATE
	BROWSE
	CANCEL
Endif

*!*	Select xpt
*!*	Delete For ptid = 1
*!*	Select xptdet
*!*	Delete For ptid = 1

RETURN
