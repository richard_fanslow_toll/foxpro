lparameter lcoffice, xaccountid, xtest

lcoffice ="Y"
goffice = "Y"
xaccountid = "6665"
close data all

do m:\dev\prg\_setvars with .f.

utilsetup("2XU_846")

xscreencaption=_screen.caption
_screen.caption="SENDINVENTORY for Account: "+xaccountid+" and Office: "+lcoffice

guserid="SENDINVENTORY"
gprocess=guserid
gdevelopment=.f.

xmod="Y"

**changed to use sqlexec to cut time in 1/2 - mvw 03/28/18
*!*	useca("upcmast","wh",,,"select * from upcmast where accountid = 6665")

*!*	select upcmast

*!*	WAIT WINDOW "Copying upcmast records to H:\Fox" NOWAIT NOCLEAR
*!*	copy to h:\fox\tempupcmast
*!*	use in upcmast

*!*	WAIT WINDOW  "upcmast copying complete..." TIMEOUT 5

*!*	use h:\fox\tempupcmast in 0 alias upcmast exclusive
*!*	select upcmast
*!*	index on style+color+id tag style
*!*	set order to

wait "Getting style master data..." window nowait noclear
xsqlexec("select * from upcmast where accountid=6665",,,"wh")

wait "Indexing style master data..." window nowait noclear
index on style+color+id tag style
set order to

wait clear
**end changes - mvw 03/28/18

xaccountid = val(xaccountid)
lcwhpath="F:\WHY\whdata\"

if !xtest
else
	use f:\tmptmp\paulg\inven in 0
	use f:\tmptmp\paulg\outship in 0
	use f:\tmptmp\paulg\outdet in 0
	use f:\tmptmp\paulg\project in 0
endif

* get inventory

xsqlexec("select units, Style, Color, Id, cast(' ' as char(15)) as upc, availqty, allocqty, holdqty, " + ;
	"cast(0 as numeric(7)) as pickedqty, cast(0 as numeric(7)) as spqty " + ;
	"from inven where accountid=6665 and units=1","inventory",,"wh")

index on transform(units)+style+color+id tag match

* inprocess is a cursor of all outship records where they are not yet delivered and also excluding "notonwip"

xsqlexec("select * from outship where mod='"+goffice+"' and accountid=6665 " + ;
	"and del_date={} and notonwip=0 and qty#0 order by wo_num","inprocess",,"wh")

xsqlexec("select units, style, color, id, pack, totqty from outship, outdet " + ;
	"where outship.outshipid=outdet.outshipid " + ;
	"and outship.mod='"+goffice+"' and outship.accountid=6665 " + ;
	"and del_date={} and notonwip=0 and qty#0","xdy",,"wh") 
	
select units, style, color, id, pack, sum(totqty) as pickedqty ;
	from xdy group by units, style, color, id, pack into cursor xunshipped

select xunshipped
scan
	=seek(transform(units)+style+color+id,"inventory","match")
	replace pickedqty with xunshipped.pickedqty in inventory
endscan

xsqlexec("select style, color, id, totqty from outship, outdet where outship.outshipid=outdet.outshipid " + ;
	"and outship.mod='"+goffice+"' and outship.accountid=6665 and del_date#{}","xdy",,"wh")
	
select style, color, id, space(15) as upc, sum(totqty) as tqty from xdy group by style, color, id order by tqty desc into cursor shippedsofar readwrite

select shippedsofar
scan
	select upcmast
	=seek(shippedsofar.style+shippedsofar.color+shippedsofar.id,"upcmast","style")
	if found("upcmast")
		replace shippedsofar.upc with upcmast.upc
	else
		replace shippedsofar.upc with "UNK"
	endif
endscan

select shippedsofar
lcfilename = "2XU_SHIPPED_"+ttoc(datetime(),1)
xlsfilename = justfname(lcfilename)
xlsfilename2= "f:\ftpusers\2XU\846out\"+xlsfilename+".csv"

copy to &xlsfilename2 fields upc,style,color,id,tqty csv

select inventory
delete for availqty=0 and pickedqty=0 and holdqty=0 and spqty=0

select inventory
scan
	select upcmast
	=seek(inventory.style+inventory.color+inventory.id,"upcmast","style")
	if found("upcmast")
		replace inventory.upc with upcmast.upc
	else
		replace inventory.upc with "UNK"
	endif
endscan

gunshot()

lcfilename = "2XU_"+lcoffice+"_"+ttoc(datetime(),1)
lcfilename2 = "2XU_"+lcoffice+"_inproc_"+ttoc(datetime(),1)
tsendto = 'pgaidis@fmiint.com,maria.estrella@tollgroup.com,mike.winter@tollgroup.com,sara.molini@2xu.com,scott.leydin@2xu.com,matt.adelgais@2xu.com'

xlsfilename = justfname(lcfilename)
xlsfilename= "f:\ftpusers\2XU\846out\"+xlsfilename+".csv"
copy to &xlsfilename fields upc,style,color,id, availqty,pickedqty csv

tmessage = "Updated inventory file in the 2XU FTP Site.............."+chr(13)+chr(13)+;
"This email is generated via an automated process"+chr(13)+"For changes in content or distribution please contact: Paul Gaidis @ 732-750-9000x143 "
tsubject= "2XU Inventory Sent: " +ttoc(datetime())
export to ("f:\inven"+transform(xaccountid)) fields style,color,id,availqty,pickedqty type xls

tattach = xlsfilename+";"+xlsfilename2


tfrom ="TGF Corporate <TGFSupplyChain@fmiint.com>"

if !empty(tattach)
	do form dartmail2 with tsendto,tfrom,tsubject,"",tattach,tmessage,"A"
endif

schedupdate()

on error
close data all 
DELETE FILE h:\fox\tempupcmast.*
_screen.caption=xscreencaption
