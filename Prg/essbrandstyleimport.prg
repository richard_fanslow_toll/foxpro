* import Essential Brands styles, from Tom Delaney 8/31/2017
* programmed by MB
* build exe as F:\UTIL\ESSENTIALBRANDS\ESSBRANDSTYLEIMPORT.EXE
*
* xsqlexec("select * from account where inactive=0","account",,"qq")

LOCAL lTestMode
lTestMode = .T.

guserid = "STYLEIMP"

IF NOT lTestMode THEN
	utilsetup("ESSBRANDSTYLEIMPORT")
ENDIF

DECLARE INTEGER CreateMutex IN Win32API ;
	STRING @lpMutexAttributes, ;
	STRING bInitialOwner, ;
	STRING @lpName

DECLARE INTEGER GetLastError IN Win32API

* if running in EXE check to see if already running
IF _VFP.STARTMODE > 0 THEN
	lcProcessName = "ESSBRANDSTYLEIMPORT"
	CreateMutex(0,CHR(1),@lcProcessName)
	lnError = GetLastError()
	IF lnError = 183
		WAIT WINDOW TIMEOUT 5 "This program is already running..." 
		IF NOT lTestMode THEN
			schedupdate()
		ENDIF
		RETURN .F.
	ENDIF
ENDIF

loESSBRANDSTYLEIMPORT = CREATEOBJECT('ESSBRANDSTYLEIMPORT') 
loESSBRANDSTYLEIMPORT.MAIN( lTestMode )

IF NOT lTestMode THEN
	schedupdate()
ENDIF

CLOSE DATABASES ALL
RETURN

#DEFINE LOGIT 1
#DEFINE WAITIT 2
#DEFINE NOWAITIT 4
#DEFINE SENDIT 8
#DEFINE CRLF CHR(13) + CHR(10)
#DEFINE RETURN_DATA_MANDATORY .F.
#DEFINE RETURN_DATA_NOT_MANDATORY .T.
#DEFINE CSVFORMAT 6
#DEFINE WRITE_ONLY_UNBUFFERED 11
#DEFINE COMMA ","

DEFINE CLASS ESSBRANDSTYLEIMPORT AS CUSTOM

	cProcessName = 'ESSBRANDSTYLEIMPORT'

	lTestMode = .T.  && .F. FOR NORMAL OPERATION, .T. TO SUPPRESS ALL BUT DEVELOPER EMAILS AND CHANGE INPUT TABLE, ETC.

	dToday = DATE()

	lAutoYield = .T.

	cStartTime = TTOC(DATETIME())

	* connection properties
	nSQLHandle = 0

	* folder properties

	cInputFolder1 = ''
	cArchiveFolder = ''
	cOrigFolder = ''	

*!*		* data properties
*!*		cUPCMastTable = ''
	
	nAccountID = 6769  
	cAddProc = "ESSBRAND"
	tADDDT = DATETIME()
	lPNP = .F.
	cDESCRIP = ''
	cSTYLE = ''
	nUPCMASTID = 0
	cUPC = ''
	nWEIGHT = 0
	nCUBE = 0
	nUIC = 0
	cINFO = ''
	cColor = ''
	cID = ''
	cUOM = ''
	nPrice = 0.00
	
	* processing properties
	lDeleteFoundRecs = .F.  && if .T. then when we find a key match we will delete existing rec and insert the new one. If .F. we will simply not insert when we find a new match.
	nRow = 0

	* Excel properties
	oExcel = NULL

	* wait window properties
	nWaitWindowTimeout = 2
	lWaitWindowIsOn = .T.

	* logfile properties
	lLoggingIsOn = .T.
	cLogFile = 'F:\UTIL\ESSENTIALBRANDS\STYLES\LOGFILES\ESSBRANDSTYLEIMPORT_log.txt'

	* INTERNAL email properties
	lSendInternalEmailIsOn = .T.
	cFrom = 'TGF Corporate <fmicorporate@fmiint.com>'
	cSendTo = 'mbennett@fmiint.com'
	cCC = ''
	cSubject = 'Essential Brands Style Import for ' + TRANSFORM(DATETIME())
	cAttach = ''
	cBodyText = ''
	cMGRSendTo = ''
	cMGRFrom = ''
	cMGRSubject = ''
	cMGRCC = ''
	cMGRAttach = ''
	cMGRBodyText = ''

	cCOMPUTERNAME = ''
	cUSERNAME = ''


	FUNCTION INIT
		IF NOT DODEFAULT()
			RETURN .F.
		ENDIF
		WITH THIS
			*SET RESOURCE OFF
			CLOSE DATA
			SET CENTURY ON
			*SET DATE AMERICAN
			*SET HOURS TO 24
			SET DECIMALS TO 2
			SET ANSI ON
			SET TALK OFF
			SET DELETED ON
			SET CONSOLE OFF
			SET EXCLUSIVE OFF
			SET SAFETY OFF
			SET EXACT OFF
			SET STATUS BAR ON
			SET SYSMENU OFF
			SET ENGINEBEHAVIOR 70
			SET MULTILOCKS ON
			_VFP.AUTOYIELD = .lAutoYield
			.cCOMPUTERNAME = GETENV("COMPUTERNAME")
			.cUSERNAME = GETENV("USERNAME")
			.lLoggingIsOn = .lLoggingIsOn AND NOT EMPTY(.cLogFile)
		ENDWITH
	ENDFUNC


	FUNCTION DESTROY
		WITH THIS
			IF .lLoggingIsOn  THEN
				SET ALTERNATE OFF
				SET ALTERNATE TO
			ENDIF
		ENDWITH
		IF TYPE('.oExcel') = 'O' AND NOT ISNULL(.oExcel) THEN
			.oExcel.QUIT()
		ENDIF
		DODEFAULT()
	ENDFUNC


	FUNCTION MAIN
		LPARAMETERS tlTestMode
		WITH THIS
			LOCAL lnNumberOfErrors, lnAccountID, lnNumFiles1, lnNumFiles2, laFiles[1,5]

			TRY

				lnNumberOfErrors = 0
				
				.cStartTime = TTOC(DATETIME())

				.lTestMode = tlTestMode

				IF .lTestMode THEN
					.cLogFile = 'F:\UTIL\ESSENTIALBRANDS\STYLES\LOGFILES\ESSBRANDSTYLEIMPORT_log_TESTMODE.txt'
					.cSendTo = 'mbennett@fmiint.com'
				ENDIF
				
				IF .lLoggingIsOn THEN
					SET ALTERNATE TO (.cLogFile) ADDITIVE
					SET ALTERNATE ON
				ENDIF
				
				IF .lTestMode THEN
					.cInputFolder1 = 'F:\UTIL\ESSENTIALBRANDS\STYLES\TESTINPUT\'
					.cArchiveFolder = 'F:\UTIL\ESSENTIALBRANDS\STYLES\TESTINPUT\ARCHIVED\'
					.cOrigFolder = 'F:\UTIL\ESSENTIALBRANDS\STYLES\TESTINPUT\ORIG\'
				ELSE
					.cInputFolder1 = 'F:\FTPUSERS\ESSENTIAL BRANDS\STYLEMASTER\'
					.cArchiveFolder = 'F:\FTPUSERS\ESSENTIAL BRANDS\STYLEMASTER\ARCHIVED\'
					.cOrigFolder = 'F:\FTPUSERS\ESSENTIAL BRANDS\STYLEMASTER\ORIG\'	
				ENDIF				

				.TrackProgress('Essential Brands Style Import process started....', LOGIT+SENDIT)
				.TrackProgress('COMPUTERNAME = ' + .cCOMPUTERNAME, LOGIT+SENDIT)
				.TrackProgress('USERNAME = ' + .cUSERNAME, LOGIT+SENDIT)
				.TrackProgress('PROJECT = ESSBRANDSTYLEIMPORT', LOGIT+SENDIT)
				IF .lTestMode THEN
					.TrackProgress('====>TEST MODE', LOGIT+SENDIT)
				ENDIF
				.TrackProgress('.cInputFolder1 = ' + .cInputFolder1, LOGIT+SENDIT)
				.TrackProgress('.cArchiveFolder = ' + .cArchiveFolder, LOGIT+SENDIT)
				.TrackProgress('.cOrigFolder = ' + .cOrigFolder, LOGIT+SENDIT)
				
				* OPEN Excel
				.oExcel = CREATEOBJECT("excel.application")
				.oExcel.displayalerts = .F.
				.oExcel.VISIBLE = .F.

				*USE (.cUPCMastTable) IN 0 ALIAS UPCMAST				
				
				* for sql
				useca("upcmast","wh",,,"select * from upcmast where accountid = " + ALLTRIM(STR(.nAccountID,4,0)),,"upcmastsql")
				SELECT "upcmastsql"
				GO bottom
			
				* process first source folder
				.ProcessFolder( .cInputFolder1, .cArchiveFolder, .cOrigFolder )
				
*!*					SELECT UPCMAST
*!*					BROWSE

				*.UpdateMeasures( .cInputFolder1, .cArchiveFolder, .cOrigFolder )

				CLOSE DATABASES ALL
				
				.oExcel.Quit()
				
				IF NOT .lTestMode THEN
					.FixDupeUPCs()
				ENDIF

				.TrackProgress('Essential Brands Style Import process ended normally.', LOGIT+SENDIT)

			CATCH TO loError

				.TrackProgress('There was an error.',LOGIT+SENDIT)
				.TrackProgress('The Error # is: ' + TRANSFORM(loError.ERRORNO), LOGIT+SENDIT)
				.TrackProgress('The Error Message is: ' + TRANSFORM(loError.MESSAGE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred in Program: ' + TRANSFORM(loError.PROCEDURE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred at line #: ' + TRANSFORM(loError.LINENO), LOGIT+SENDIT)
				.TrackProgress('SPREADSHEET ROW = ' + TRANSFORM(.nRow), LOGIT+SENDIT)
				lnNumberOfErrors = lnNumberOfErrors + 1
				.lSendInternalEmailIsOn = .T.
				IF TYPE('.oExcel') = 'O' AND NOT ISNULL(.oExcel) THEN
					.oExcel.QUIT()
				ENDIF

			ENDTRY

			WAIT CLEAR
			***************** INTERNAL email results ******************************
			.TrackProgress('About to send status email.',LOGIT+SENDIT)
			.TrackProgress('The logfile is: ' + .cLogFile, LOGIT+SENDIT)
			.TrackProgress('Essential Brands Style Import process started: ' + .cStartTime, LOGIT+SENDIT)
			.TrackProgress('Essential Brands Style Import process finished: ' + TTOC(DATETIME()), LOGIT+SENDIT)

			IF .lSendInternalEmailIsOn THEN
				DO FORM dartmail2 WITH .cSendTo,.cFrom,.cSubject,.cCC,.cAttach,.cBodyText,"A"
				.TrackProgress('Sent status email.',LOGIT)
			ELSE
				.TrackProgress('Did not send status email: THIS.lSendInternalEmailIsOn = FALSE.',LOGIT+SENDIT)
			ENDIF

		ENDWITH
		RETURN
	ENDFUNC && main


	PROCEDURE ProcessFolder
		LPARAMETERS tcInputFolder, tcArchivedFolder, tcOrigFolder
		WITH THIS
			LOCAL laFiles[1,5], laFilesSorted[1,6], lnNumFiles, lnCurrentFile, lcSourceFile, lcArchivedFile, lcOrigFile
			LOCAL lnHandle, lcStr, lnCtr, oWorkbook, oWorksheet, lnMaxRow, lnRow, lcRow, lnDupeDelCtr, lnDupeCtr
			LOCAL llArchived, lnColumnF, lcColumnD, lnLength, lnWidth, lnHeight
			LOCAL lcGTINString, j, lnNumLines, lcInfoLine

			
			PRIVATE	m.UPCMASTID, m.ACCOUNTID, m.STYLE, m.PNP, m.DESCRIP, m.UPC, m.WEIGHT, m.CUBE, m.UIC, m.INFO, m.COLOR, m.ID, m.UOM, m.PRICE
			STORE 0 TO m.UPCMASTID, m.WEIGHT, m.CUBE, m.UIC, m.PRICE
			STORE '' TO m.STYLE, m.DESCRIP, m.UPC, m.INFO, m.COLOR, m.ID, m.UOM
			STORE .T. TO m.PNP

			m.ACCOUNTID = .nAccountID
			

			************************************************************************
			*** for sql
			PRIVATE m.ADDPROC, m.UPDPROC, m.ADDBY, m.ADDDT, m.UPDATEBY, m.UPDATEDT
			STORE .cAddProc TO m.ADDPROC, m.UPDPROC, m.ADDBY, m.UPDATEBY
			STORE .tADDDT TO m.ADDDT, m.UPDATEDT
			************************************************************************

			* 4/21/2017 MB
			*lnNumFiles = ADIR(laFiles,(tcInputFolder + "dims*.xls")) 
			lnNumFiles = ADIR(laFiles,(tcInputFolder + "Item*.csv")) 

			IF lnNumFiles > 0 THEN

				* sort file list by date/time
				.SortArrayByDateTime(@laFiles, @laFilesSorted)

				* send an email only if files were found
				.lSendInternalEmailIsOn = .T.

				FOR lnCurrentFile = 1 TO lnNumFiles

					lcSourceFile = tcInputFolder + laFilesSorted[lnCurrentFile,1]
					lcArchivedFile = tcArchivedFolder + laFilesSorted[lnCurrentFile,1]
					lcOrigFile = tcOrigFolder + laFilesSorted[lnCurrentFile,1]

					.TrackProgress('lcSourceFile = ' + lcSourceFile,LOGIT+SENDIT)
					.TrackProgress('lcArchivedFile = ' + lcArchivedFile,LOGIT+SENDIT)
					.TrackProgress('lcOrigFile = ' + lcOrigFile,LOGIT+SENDIT)
					
					
					* move source file into ORIG folder so that, if an error causes the program to abort, the folderscan process does not keep reprocessing the bad source file in an infinite loop
					
					* if the file already exists in the orig folder, make sure it is not read-only.
					llArchived = FILE(lcOrigFile)
					IF llArchived THEN
						RUN ATTRIB -R &lcOrigFile.
						.TrackProgress('   Used ATTRIB -R for re-archiving : ' + lcOrigFile,LOGIT+SENDIT)
					ENDIF

					* archive the source file
					COPY FILE (lcSourceFile) TO (lcOrigFile)

					* always delete the source file
					*IF FILE(lcOrigFile) THEN
						DELETE FILE (lcSourceFile)
					*ENDIF
					
					
					* then process from the file once it is in the ORIG folder
					oWorkbook = .oExcel.workbooks.OPEN(lcOrigFile)
					oWorksheet = oWorkbook.Worksheets[1]  

					lnMaxRow = 999
					
					lnCtr = 0
					lnDupeCtr = 0
					lnDupeDelCtr = 0
					 					

					FOR lnRow = 2 TO lnMaxRow   && header increased to 3 lines in 2nd file for 6/15/2017
					
						.nRow = lnRow
					
						WAIT WINDOW NOWAIT 'Processing Spreadsheet Row: ' + TRANSFORM(lnRow)
						
						lcRow = ALLTRIM(STR(lnRow))
						
						
						m.STYLE = ALLTRIM(NVL(oWorksheet.RANGE("D"+lcRow).VALUE,'')) + ALLTRIM(NVL(oWorksheet.RANGE("G"+lcRow).VALUE,''))
						
						IF ( ISNULL(m.STYLE) OR EMPTY(m.STYLE) ) THEN
							* we are past the last data row! exit the loop...
							.TrackProgress('====> Empty Style encountered at row: ' + lcRow,LOGIT+SENDIT)
							EXIT FOR
						ENDIF
						
						m.STYLE = PADR(m.STYLE,20,' ')						
						
						m.DESCRIP = NVL(oWorksheet.RANGE("F"+lcRow).VALUE,'')
																	
						m.COLOR = NVL(oWorksheet.RANGE("I"+lcRow).VALUE,0)											
						m.COLOR =INT(m.COLOR )
						m.COLOR = ALLTRIM(STR(m.COLOR,3,0))
						m.COLOR = PADL(m.COLOR,3,'0')
																	
						lcCOLORDESC = NVL(oWorksheet.RANGE("J"+lcRow).VALUE,'')	
						m.INFO = 'COLORDESC*' + lcCOLORDESC										
																	
						m.ID = ALLTRIM(NVL(oWorksheet.RANGE("K"+lcRow).VALUE,''))
						IF UPPER(ALLTRIM(m.ID)) = '*N' THEN
							m.ID = ALLTRIM(NVL(oWorksheet.RANGE("R"+lcRow).VALUE,''))
						ENDIF											
*!*							m.ID = STR(m.ID,4,1)										

						m.UOM = NVL(oWorksheet.RANGE("T"+lcRow).VALUE,'')
						IF UPPER(ALLTRIM(m.UOM)) <> 'EA' THEN
							m.UOM = 'CA'
						ENDIF											
						
						m.UPC = NVL(oWorksheet.RANGE("S"+lcRow).VALUE,0)
						m.UPC =INT(m.UPC)
						m.UPC = STR(m.UPC,12,0)

						m.PRICE = NVL(oWorksheet.RANGE("U"+lcRow).VALUE,0.00)
						
						
						m.PNP = .F.
						

						*****************************************************************
						* for sql, save memvars gotten from Excel spreadsheet to properties for later recovery
						.lPNP = m.PNP
						.cDESCRIP = m.DESCRIP
						.cSTYLE = m.STYLE
						.cUPC = m.UPC
						*.nWEIGHT = m.WEIGHT
						*.nCUBE = m.CUBE
						*.nUIC = m.UIC
						.cINFO = m.INFO
						.cColor = m.COLOR
						.cID = m.ID
						.cUOM = m.UOM
						.nPRICE = m.PRICE
						*****************************************************************
						
						SELECT upcmastsql

						LOCATE FOR (ACCOUNTID = m.ACCOUNTID) ;
							AND (UPC = m.UPC)
							
						IF FOUND() THEN						
								
							REPLACE upcmastsql.PNP WITH m.PNP, upcmastsql.DESCRIP WITH m.DESCRIP, upcmastsql.INFO WITH m.INFO, upcmastsql.STYLE WITH m.STYLE, ;
								upcmastsql.COLOR WITH m.COLOR, upcmastsql.ID WITH m.ID, upcmastsql.UOM WITH m.UOM, upcmastsql.PRICE WITH m.PRICE, ;
								upcmastsql.UPDPROC WITH m.UPDPROC, upcmastsql.UPDATEBY WITH m.UPDATEBY, upcmastsql.UPDATEDT WITH m.UPDATEDT IN upcmastsql
								
							lnDupeCtr = lnDupeCtr + 1
						ELSE
							SELECT upcmastsql
							SCATTER MEMVAR MEMO BLANK

							* restore memvars from properties
							STORE .cAddProc TO m.ADDPROC, m.UPDPROC, m.ADDBY, m.UPDATEBY 
							STORE .tADDDT TO m.ADDDT, m.UPDATEDT
							
							m.PNP = .lPNP
							m.DESCRIP = .cDESCRIP
							m.STYLE = .cSTYLE
							m.UPC = .cUPC
							m.ACCOUNTID = .nAccountID
							*m.WEIGHT = .nWEIGHT
							*m.CUBE = .nCUBE 
							*m.UIC = .nUIC
							m.INFO = .cINFO 
							m.COLOR = .cColor
							m.ID = .cID
							m.UOM = .cUOM
							m.PRICE = .nPRICE
							
							IF .lTestMode THEN
								m.UPCMASTID = 0
							ELSE					
								m.UPCMASTID = dygenpk("upcmast","whall")
							ENDIF
							
							INSERT INTO upcmastsql FROM MEMVAR
							lnCtr = lnCtr + 1
						ENDIF
						*****************************************************************	

						
					ENDFOR
					
					oWorkbook.close()
					
					IF .lTestMode THEN
						SELECT upcmastsql
						BROWSE
						.TrackProgress('TEST MODE: did not TU(upcmastsql)',LOGIT+SENDIT)
					ELSE					
						* tableupate SQL
						IF tu("upcmastsql") THEN
							.TrackProgress('SUCCESS: TU(upcmastsql) returned .T.!',LOGIT+SENDIT)
						ELSE
							* ERROR on table update
							.TrackProgress('ERROR: TU(upcmastsql) returned .F.!',LOGIT+SENDIT)
							=AERROR(UPCERR)
							IF TYPE("UPCERR")#"U" THEN 
								.TrackProgress('Error Description = ' + UPCERR[1,2],LOGIT+SENDIT)
							ENDIF
						ENDIF
					ENDIF

					.TrackProgress(TRANSFORM(lnCtr ) + ' INSERTS were made from ' + lcOrigFile,LOGIT+SENDIT)
					.TrackProgress(TRANSFORM(lnDupeCtr) + ' found records were updated ',LOGIT+SENDIT)

					* if the file already exists in the archive folder, make sure it is not read-only.
					* this is to prevent errors we get copying into archive on a resend of an already-archived file.
					* Necessary because the files in the archive folders were sometimes marked as read-only (not sure why)
					llArchived = FILE(lcArchivedFile)
					IF llArchived THEN
						RUN ATTRIB -R &lcArchivedFile.
						*.TrackProgress('   Used ATTRIB -R for re-archiving : ' + lcSourceFile,LOGIT+SENDIT)
						.TrackProgress('   Used ATTRIB -R for re-archiving : ' + lcOrigFile,LOGIT+SENDIT)
					ENDIF

					* archive the source file
					*COPY FILE (lcSourceFile) TO (lcArchivedFile)
					COPY FILE (lcOrigFile) TO (lcArchivedFile)

					* delete the source file
					IF FILE(lcArchivedFile) THEN
						*DELETE FILE (lcSourceFile)
						DELETE FILE (lcOrigFile)
					ENDIF
			
					WAIT CLEAR

				ENDFOR && lnCurrentFile = 1 TO lnNumFiles
			ELSE
				.TrackProgress('Found no files in the Essential Brands source folder', LOGIT+SENDIT)
			ENDIF

		ENDWITH
		RETURN
	ENDPROC  &&  ProcessFolder
	
	
	PROCEDURE FixDupeUPCs
		WITH THIS
				LOCAL lnUpcmastid, lnUPCsBlanked
				
				lnUPCsBlanked = 0
				
				useca("upcmast","wh",,,"select * from upcmast where accountid = " + ALLTRIM(STR(.nAccountID,4,0)),,"upcmastsql")
				SELECT upcmastsql
				GO bottom
				
				SELECT UPC, COUNT(*) AS CNT ;
					FROM upcmastsql ;
					INTO CURSOR CURDUPES ;
					WHERE NOT EMPTYNUL(UPC) ;
					GROUP BY UPC ;
					ORDER BY UPC ;
					HAVING CNT > 1

*!*					SELECT CURDUPES
*!*					BROWSE
								
				IF USED('CURDUPES') AND NOT EOF('CURDUPES') THEN
					SELECT CURDUPES
					SCAN
						SELECT upcmastsql
						LOCATE FOR UPC == CURDUPES.UPC
						IF FOUND() THEN
							lnUpcmastid = upcmastsql.upcmastid
							SELECT upcmastsql
							SCAN FOR (UPC == CURDUPES.UPC) AND (NOT upcmastid = lnUpcmastid)	
								REPLACE upcmastsql.UPC WITH "" IN upcmastsql
								lnUPCsBlanked = lnUPCsBlanked + 1
							ENDSCAN
						ENDIF
					ENDSCAN
				ENDIF
				
				.TrackProgress('in FixDupeUPCs() # UPCs Blanked = ' + TRANSFORM(lnUPCsBlanked),LOGIT+SENDIT)
					
				IF tu("upcmastsql") THEN
					.TrackProgress('SUCCESS: TU(upcmastsql) in FixDupeUPCs() returned .T.!',LOGIT+SENDIT)
				ELSE
					* ERROR on table update
					.TrackProgress('ERROR: TU(upcmastsql) in FixDupeUPCs() returned .F.!',LOGIT+SENDIT)
					=AERROR(UPCERR)
					IF TYPE("UPCERR")#"U" THEN 
						.TrackProgress('Error Description = ' + UPCERR[1,2],LOGIT+SENDIT)
					ENDIF
				ENDIF
						
		ENDWITH
		RETURN 
	ENDPROC  && FixDupeUPCs


	FUNCTION ExecSQL
		LPARAMETERS tcSQL, tcCursorName, tlNoDataReturnedIsOkay
		LOCAL llRetval, lnResult
		WITH THIS
			* close target cursor if it's open
			IF USED(tcCursorName)
				USE IN (tcCursorName)
			ENDIF
			WAIT WINDOW "Fetching remote data; please wait..." NOWAIT
			lnResult = SQLEXEC(.nSQLHandle, tcSQL, tcCursorName)
			llRetval = ( lnResult > 0 )
			IF llRetval THEN
				* see if any data came back
				IF NOT tlNoDataReturnedIsOkay THEN
					IF (NOT USED(tcCursorName)) OR EOF(tcCursorName) THEN
						llRetval = .F.
						.TrackProgress('No data was returned by this query: ' +CRLF + tcSQL, LOGIT+SENDIT)
						.cSendTo = 'mbennett@fmiint.com'
					ENDIF
				ENDIF
			ELSE
				.TrackProgress("Result Code [" + TRANSFORM(lnResult) + "] returned by this query: " + tcSQL, LOGIT+SENDIT)
				.cSendTo = 'mbennett@fmiint.com'
			ENDIF
			WAIT CLEAR
			RETURN llRetval
		ENDWITH
	ENDFUNC


	PROCEDURE TrackProgress
		* do any combination of Wait Window, writing to logfile, and adding to body of email,
		* based on nFlags parameter.
		LPARAMETERS tcExpression, tnFlags
		WITH THIS
			IF BITAND(tnFlags,LOGIT) = LOGIT THEN
				IF .lLoggingIsOn THEN
					?
					? .cProcessName + "  " + TTOC(DATETIME()) + ": " + tcExpression
				ENDIF
			ENDIF
			IF .lWaitWindowIsOn THEN
				IF BITAND(tnFlags,NOWAITIT) = NOWAITIT THEN
					WAIT WINDOW tcExpression NOWAIT
				ENDIF
				IF BITAND(tnFlags,WAITIT) = WAITIT THEN
					WAIT WINDOW tcExpression TIMEOUT .nWaitWindowTimeout
				ENDIF
			ENDIF
			IF BITAND(tnFlags,SENDIT) = SENDIT THEN
				IF .lSendInternalEmailIsOn THEN
					.cBodyText = .cBodyText + tcExpression + CRLF + CRLF
				ENDIF
			ENDIF
		ENDWITH
	ENDPROC  &&  TrackProgress


	PROCEDURE SortArrayByDateTime
		* expects taInArray[] to be the result of an ADIR()
		LPARAMETERS taInArray, taOutArray
		EXTERNAL ARRAY taInArray, taOutArray
		LOCAL lnRows, lnCols, i, j
		* extending taInArray[] to process the files in date/time order
		lnRows = ALEN(taInArray,1)
		lnCols = ALEN(taInArray,2)
		* create taOutArray with one more column
		DIMENSION taOutArray[lnRows,lnCols + 1]
		* and fill it with taInArray columns AND (filedate + filetime) in new column
		* ( ACOPY() doesn't preserve elements properly because of different # of columns )
		FOR i = 1 TO lnRows
			FOR j = 1 TO lnCols
				taOutArray[i,j] = taInArray[i,j]
			ENDFOR
			taOutArray[i,lnCols + 1] = DTOS(taInArray[i,3]) + "_" + taInArray[i,4]
		ENDFOR
		* sort taOutArray on date/time column
		=ASORT(taOutArray,6)
	ENDPROC

ENDDEFINE
