* Report all trailers which have not had PM service* (i.e., a WO with mainttype = P) within last 90 days.

PUBLIC gHeader
gHeader = "TGF"

LOCAL ldToday, ld90DaysAgo, ldLatestServiceDate, llHadService, lnCurrentMileage, lcComment, lnDays

ldToday = DATE()
ld90DaysAgo = ldToday - 90
lcComment = ''

IF USED('curPMService') THEN
	USE IN curPMService
ENDIF
IF USED('curPMServiceFinal') THEN
	USE IN curPMServiceFinal
ENDIF

IF NOT USED('ORDHDR') THEN
	USE SH!ORDHDR IN 0 ALIAS ORDHDR
ENDIF
SELECT ORDHDR
SET ORDER TO CRDATE

IF NOT USED('TRAILER') THEN
	USE SH!TRAILER IN 0 ALIAS TRAILER
ENDIF

** new code 9/5/07 MB
WAIT WINDOW "Checking Trailers...." NOWAIT

SELECT "TRAILER" AS VEHTYPE, ;
	VEHNO, ;
	SPACE(50) AS COMMENT, ;
	SPACE(10) AS PADVEHNO, ;
	0000000 AS NDAYS, ;
	0000000 AS NDAYSDESC, ;
	MAX(CRDATE) AS LASTSVCE ;
	FROM ORDHDR ;
	WHERE INLIST(UPPER(MAINTTYPE),'P') AND (TYPE = 'TRAILER') ;
	AND VEHNO IN ( SELECT TRAIL_NUM AS VEHNO FROM TRAILER WHERE ACTIVE ) ;
	AND NOT EMPTY(VEHNO) ;
	GROUP BY 1,2,3,4,5,6 ;
	UNION ALL ;
	SELECT "TRAILER" AS VEHTYPE, ;
	TRAIL_NUM AS VEHNO, ;
	SPACE(50) AS COMMENT, ;
	SPACE(10) AS PADVEHNO, ;
	0000000 AS NDAYS, ;
	0000000 AS NDAYSDESC, ;
	{} AS LASTSVCE ;
	FROM TRAILER ;
	WHERE ACTIVE ;
	AND NOT EMPTY(TRAIL_NUM) ;
	AND ( TRAIL_NUM NOT IN ( SELECT VEHNO AS TRAIL_NUM FROM ORDHDR WHERE INLIST(UPPER(MAINTTYPE),'P') AND (TYPE = 'TRAILER') ) ) ;
	INTO CURSOR curPMService ;
	READWRITE ;

SELECT curPMService
SCAN
	IF EMPTY(curPMService.LASTSVCE) THEN
		lnDays = 999999
		lcComment = "Never had PM service"
	ELSE
		lnDays = ldToday - curPMService.LASTSVCE
		lcComment = TRANSFORM(lnDays) + " days since last PM service"
	ENDIF
	REPLACE curPMService.NDAYS WITH lnDays, ;
		curPMService.NDAYSDESC WITH (1000000 - lnDays), ;
		curPMService.COMMENT WITH lcComment, ;
		curPMService.PADVEHNO WITH PADL(ALLTRIM(curPMService.VEHNO),10,"0")
ENDSCAN

*prepare & run report
SELECT * ;
	FROM curPMService ;
	INTO CURSOR curPMServiceFinal ;
	WHERE NDAYS > 90 ;
	ORDER BY VEHTYPE, NDAYSDESC, PADVEHNO

WAIT CLEAR

IF USED('curPMServiceFinal') AND NOT EOF('curPMServiceFinal') THEN
	SELECT curPMServiceFinal
	GOTO TOP
	*BROWSE
	KEYBOARD "{ctrl-f10}"
	REPORT FORM SHPMSERVICE PREVIEW NOCONSOLE
ELSE
	X3WINMSG("There are no Trailers which have not had PM service within last 90 days.")
ENDIF

IF USED('curPMService') THEN
	USE IN curPMService
ENDIF

IF USED('curPMServiceFinal') THEN
	USE IN curPMServiceFinal
ENDIF
