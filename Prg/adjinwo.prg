* changes a location for a style on inbound WOs for a P&P account
* we can't create adj records for cartons for P&P accounts so the location must be changed in inloc

* used in inven.scx if location changed
* used in pppulllxe if replenishment dropped to a carton location - rare
* used in pulllxe if pick from a different location
* used in whconsollxe.scx and whlocchglxe.scx
* used in hhlocchgmj

lparameters zaccountid, zunits, zstyle, zcolor, zid, zpack, zfromwhseloc, ztowhseloc, xfrominven

xsqlexec("select * from indet where "+gmodfilter+" and accountid="+transform(zaccountid)+" and units="+transform(zunits)+" and " + ;
	"style='"+zstyle+"' and color='"+zcolor+"' and id='"+zid+"' and pack='"+zpack+"' and remainqty>0","xindet",,"wh")

scan for xdychange>0
	xsqlexec("select * from inloc where indetid="+transform(xindet.indetid)+" and remainqty>0 and whseloc='"+zfromwhseloc+"'","xinloc",,"wh")

	scan for xdychange>0
		xinlocid=inlocid

		if xdychange>=xinloc.remainqty
			xmoved=xinloc.remainqty
			xdychange=xdychange-xinloc.remainqty

			if xinloc.removeqty>0
				scatter memvar

				addsql("update inloc set locqty=removeqty, remainqty=0 where inlocid="+transform(xinloc.inlocid),"wh")

				m.locqty=xmoved
				m.removeqty=0
				m.remainqty=xmoved
				m.whseloc=ztowhseloc
				if type("xpalletid")="C"
					m.palletid=xpalletid
				endif
				insertinto("inloc","wh",.t.)
			else
				addsql("update inloc set whseloc='"+ztowhseloc+"' where inlocid="+transform(xinloc.inlocid),"wh")
			endif
		else
			xmoved=xdychange
			xdychange=0

			scatter memvar

			addsql("update inloc set locqty=locqty-"+transform(xmoved)+", remainqty=remainqty-"+transform(xmoved)+" where inlocid="+transform(xinloc.inlocid),"wh")

			m.locqty=xmoved
			m.removeqty=0
			m.remainqty=xmoved
			m.whseloc=ztowhseloc
			insertinto("inloc","wh",.t.)
		endif

		select inloc
		locate for inlocid=xinlocid

		m.comment=transform(xmoved)+iif(zunits," UNIT"," CARTON")+iif(xmoved>1,"S","")+" MOVED FROM "+trim(zfromwhseloc)+" TO "+ztowhseloc
		m.inadj=.f.
		m.remainqty=0
		m.removeqty=0
		m.totqty=0
		m.adjdt=qdate()
		m.offset=.t.
		m.addby=''
		m.adddt={}

		m.whseloc=zfromwhseloc
		insertinto("adj","wh",.t.)

		m.whseloc=ztowhseloc
		insertinto("adj","wh",.t.)

		if !xfrominven
			if seek(str(zaccountid,4)+trans(zunits)+zstyle+zcolor+zid+zpack+zfromwhseloc,"invenloc","matchwh")
				xonhold=invenloc.hold
				replace locqty with invenloc.locqty-xmoved in invenloc
			else
				xonhold=.f.
			endif

			if seek(str(zaccountid,4)+trans(zunits)+zstyle+zcolor+zid+zpack,"inven","match")
				if seek(str(zaccountid,4)+trans(zunits)+zstyle+zcolor+zid+zpack+ztowhseloc,"invenloc","matchwh")

					replace locqty with invenloc.locqty+xmoved in invenloc

					do case
					case (invenloc.hold and !xonhold) or holdloc(ztowhseloc)
						replace holdqty with inven.holdqty+xmoved, availqty with inven.totqty-inven.allocqty-inven.holdqty in inven
						pnphold(xmoved)
					case !invenloc.hold and xonhold
						replace holdqty with inven.holdqty-xmoved, availqty with inven.totqty-inven.allocqty-inven.holdqty in inven
						pnphold(-xmoved)
					endcase

				else
					if holdloc(ztowhseloc) and !xonhold
						replace holdqty with inven.holdqty+xmoved, availqty with inven.totqty-inven.allocqty-inven.holdqty in inven
						if seek(str(zaccountid,4)+trans(zunits)+zstyle+zcolor+zid+zpack+zfromwhseloc,"invenloc","matchwh")
							pnphold(xmoved)
						endif
					endif
					if !holdloc(ztowhseloc) and xonhold
						replace holdqty with inven.holdqty-xmoved, availqty with inven.totqty-inven.allocqty-inven.holdqty in inven
						if seek(str(zaccountid,4)+trans(zunits)+zstyle+zcolor+zid+zpack+zfromwhseloc,"invenloc","matchwh")
							pnphold(-xmoved,.t.)
						endif
					endif

					m.invenid=inven.invenid
					m.whseloc=ztowhseloc
					m.locqty=xmoved
					m.hold=holdloc(ztowhseloc)		&& (xonhold or holdloc(ztowhseloc))  changed dy 6/28/11 per Ed K
					m.holdtype=gholdtype

					if m.invenid=0
						email("Dyoung@fmiint.com","INFO: invenid=0 when inserting invenloc B",,,,,.t.,,,,,.t.,,.t.)
					endif

					insertinto("invenloc","wh",.t.)
				endif
			endif
		endif
	endscan
endscan
