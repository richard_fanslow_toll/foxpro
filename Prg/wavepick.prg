* repulls wave short picks 
* this is a standalone process that runs every 10 minutes

utilsetup("WAVEPICK")

guserid="WAVEPICK"
gprocess="WAVEPICK"

xsqlexec("select * from account where inactive=0","account",,"qq")
index on accountid tag accountid
set order to


*per darren, commented out all calls to noinven - mvw 09/18/17
*use f:\auto\noinven in 0

xsqlexec("select * from whoffice",,,"wh")

set classlib to wave additive
cuswavecode=createobject("wavecode")

zz="NO"

userv("vwavepick","whall")

xsqlexec("select * from outdet where 1=0",,,"wh")

select whoffice
**all for SP and KY - mvw 09/23/15
*scan for !test and inlist(rateoffice,"C","K") and wireless and office="K"
**added for NJ - mvw 09/15/16
scan for !test and inlist(rateoffice,"K","N","L") and wireless
	gmasteroffice=whoffice.rateoffice
	goffice=whoffice.office
	xoffice=whoffice.office
	xmasteroffice=whoffice.rateoffice
	xfolder=whoffice.folder+"\whdata\"
	gmodfilter="mod='"+goffice+"'"	&& dy 12/30/17
	
	useca("wave","wh",,,"mod='"+goffice+"' and wavedt>{"+dtoc(date()-5)+"}")

	select wave
	locate for wavedt>date()-5
	if found()
		xerror=.f.

		try
			xstep=1
			
			if type("gldor")="L" and !gldor
				use whdata\wosemi in 0
*				use whdata\outdet in 0
*				use whdata\outloc in 0
				use whdata\inven in 0
				use whdata\invenloc in 0
				use whdata\inwolog in 0
				use whdata\indet in 0
				use whdata\inloc in 0
				use whdata\adj in 0
			else
*				if usesqloutwo()
*					useca("outdet","wh")
*					useca("outloc","wh")
*				else
*					use (xfolder+"outdet") in 0
*					use (xfolder+"outloc") in 0
*				endif
				
				userv("vinvenall","whall","inven",.t.)
				select inven
				cursorsetprop("buffering",3)
				index on invenid tag invenid
				index on str(accountid,4)+trans(units)+style+color+id+pack tag match
				set order to
				cursorsetprop("buffering",5)

				userv("vinvenlocall","whall","invenloc",.t.)
				select invenloc
				cursorsetprop("buffering",3)
				index on invenid tag invenid
				index on invenlocid tag invenlocid
				index on str(accountid,4)+trans(units)+style+color+id+pack+whseloc tag matchwh
				set order to
				cursorsetprop("buffering",5)				
				
				useca("adj","wh")
				useca("wosemi","wh",.t.)
			endif

			xstep=2

			useca("pprep","wh",,,"mod='"+goffice+"' and movedt={} and loadeddt={}")

			xstep=3

			xsqlexec("select * from whseloc where office='"+xmasteroffice+"'","whseloc",,"wh")
			index on whseloc tag whseloc
			set order to
			
			xsqlexec("select * from upcloc where mod='"+xmasteroffice+"'","upcloc",,"wh")
			index on style tag style
			index on upc tag upc
			index on str(accountid,4)+style+color+id tag stylecolid
			index on whseloc tag whseloc
			set order to

			xstep=4

			select style, color, id, pack, pickqty from vwavepick where .f. into cursor xwavepick readwrite
			select outdetid, accountid, units, style, color, id, pack, totqty as origqty, totqty, 00000 as reloadqty from outdet where .f. into cursor xoutdet readwrite

			index on style+color+id tag stylecolid
			select style, color, id, 0000 as shortqty from outdet where .f. into cursor xshort readwrite

			xstep=5

*per darren, commented out all calls to noinven - mvw 09/18/17
*			create cursor xnoinven (wavenum n(5), accountid i, acctname c(30), style c(20), color c(10), id c(10), shortqty n(5))

			create cursor xrem (wo_num n(7), po c(10), totqty n(6), removeqty n(6))
			index on str(wo_num,7)+po tag wopo

			cursorsetprop("Buffering",5,"wave")
			cursorsetprop("Buffering",5,"vwavepick")
*			cursorsetprop("Buffering",5,"outdet")
*			cursorsetprop("Buffering",5,"outloc")
			
		catch
			aerror(aerrq)
	    	email("mwinter@fmiint.com","Wavepick Error: Tables closed before buffering could be set! Step "+transform(xstep),transform(aerrq[1,1])+chr(13)+chr(10)+aerrq[1,2],,,,.t.,,,,,.t.)
			xerror=.t.
		endtry

		if xerror
			return
		endif

		select wave
		scan for wavedt>=date()-5 and wavenum=  70017
			**don't look at waves that have been manually finished - mvw 09/28/15
			xsqlexec("select * from wavepick where wavenum="+transform(wave.wavenum)+" and movedt#{} and driver='FINISH'",,,"wh")
			if found()
				loop
			endif

			**in case finished but auto generated instruction had been skipped already and no open picks existed at time of finish - mvw 10/14/15
			xsqlexec("select * from info where wavenum="+transform(wave.wavenum)+" and message='Wave Picking finished'",,,"wh")
			if reccount()#0
				loop
			endif

			xsqlexec("select * from wavepick where wavenum="+transform(wave.wavenum)+" and movedt#{} and repulled=0 and added=0 and driver#'FINISH'",,,"wh")
			locate for (actunitqty#0 and actpickqty*val(pack)#actunitqty) or pickqty#actpickqty
			if !found()
				loop
			endif

			**only add if there has been wavepick activity on this wave in the last day - mvw 09/25/15
			locate for movedt>=date()-1
			if !found()
				loop
			endif

			if xsqlexec("select * from wosemi where mod='"+goffice+"' and zform='W'","xwosemi",,"wh") # 0
				if xwosemi.userid#guserid 
					loop
				else
					xsqlexec("delete from wosemi where wosemiid="+transform(xwosemi.wosemiid),,,"wh")
				endif
			endif

			m.zform="W"
			m.wo_num=wave.wavenum
			m.accountid=wave.accountid
			m.userid=guserid
			m.zdatetime=datetime()
			insertinto("wosemi","wh",.t.)
			tu("wosemi")

			insert into wosemi (zform, wo_num, accountid, userid, zdatetime) values ("W", wave.wavenum, wave.accountid, guserid, datetime())

			zz="YES"
			twaveid=wave.waveid

			requery("vwavepick")

			zap in xwavepick
			zap in xoutdet

			select vwavepick
			scan for ((actunitqty#0 and actpickqty*val(pack)#actunitqty) or pickqty#actpickqty) and !emptynul(movedt) and !repulled and !added and driver#"FINISH"
				scatter memvar fields accountid, style, color, id
				m.units=.t.
				m.pack="1"
				
				if actunitqty#0
					m.totqty=pickqty*val(pack)-actunitqty
				else
					m.totqty=(pickqty-actpickqty)*val(pack)
				endif

				if seek(style+color+id,"xoutdet","stylecolid")
					replace totqty with xoutdet.totqty+m.totqty in xoutdet
					replace origqty with xoutdet.totqty in xoutdet
				else
					m.origqty=m.totqty
					insert into xoutdet from memvar
				endif
			endscan

			select xoutdet
			scan
				select vwavepick
				sum (pickqty*val(pack)) to xyz1 ;
					for style=xoutdet.style and color=xoutdet.color and id=xoutdet.id and emptynul(movedt) and (repulled or added)
				sum iif(actunitqty#0,actunitqty,actpickqty*val(pack)) to xyz2 ;
					for style=xoutdet.style and color=xoutdet.color and id=xoutdet.id and !emptynul(movedt) and (repulled or added)
				xyz=xyz1+xyz2

				if xyz>0
					if xyz>=xoutdet.totqty
						replace totqty with 0 in xoutdet
					else
						replace totqty with xoutdet.totqty-xyz in xoutdet
					endif
				endif

				select inven
				locate for accountid=wave.accountid and units and style=xoutdet.style and color=xoutdet.color and id=xoutdet.id
				if !found
					replace totqty with 0 in xoutdet
				else
					if inven.availqty<xoutdet.totqty
						replace totqty with inven.availqty in xoutdet
					endif
				endif
			endscan

			m.repulled=.t.
			xwavepickadded=.f.

			if !cuswavecode.zzpull(.t.)
*			if !wavepull(.t.,.t.)
				set step on 
				tablerevert(.t.,"wave")
				tablerevert(.t.,"vwavepick")
*				if usesqloutwo()
*				else
*					tablerevert(.t.,"outdet")
*					tablerevert(.t.,"outloc")
*				endif
				
				tablerevert(.t.,"inven")
				tablerevert(.t.,"invenloc")
				tablerevert(.t.,"adj")
				email("Dyoung@fmiint.com;mwinter@fmiint.com","INFO: unable to repick wave "+wave.wavenum+" in wavepickshort",,,,,.t.,,,,,.t.)
				delete in wosemi
				tu("wosemi")
				loop
			endif

			replace repull with .f. in wave
			
			do case
			case xwavepickadded
				replace pickdt with {}, packdt with {} in wave

			case !xwavepickadded and wave.accountid=6532 and wave.pulldt#{} and wave.pickdt={}
				replace pickdt with date() in wave
			endcase

			select xwavepick
			scan
				=seek(style+color+id,"xoutdet","stylecolid")
				replace reloadqty with xoutdet.reloadqty+xwavepick.pickqty*val(xwavepick.pack) in xoutdet

				**for JQUEEN Direct To Label, need to update the comments to either "DIRECT TO LABEL" or "PUT SYSTEM" - mvw 03/11/14
				if wave.accountid=6097
					select vwavepick
					locate for style=xwavepick.style and color=xwavepick.color and id=xwavepick.id and (pickqty#actpickqty or actunitqty#0) and !emptynul(movedt)
					xcomment=iif(eof(),"PUT SYSTEM",vwavepick.comment)
					replace comment with xcomment for style=xwavepick.style and color=xwavepick.color and id=xwavepick.id and emptynul(movedt) and emptynul(comment)
				endif
			endscan

*			select xoutdet
*			locate for origqty#reloadqty
*			if found()
*				copy to ("f:\auto\wavepickshort-"+transform(wave.wavenum))
*			endif
			
*per darren, commented out all calls to noinven - mvw 09/18/17
*!*				select xoutdet
*!*				scan for origqty#reloadqty
*!*					if !seek(str(wave.wavenum,4)+str(xoutdet.accountid,4)+xoutdet.style+xoutdet.color+xoutdet.id,"noinven","stylecolid")
*!*						scatter memvar
*!*						m.wavenum=wave.wavenum
*!*						m.acctname=acctname(m.accountid)
*!*						m.shortqty=m.origqty-m.reloadqty
*!*						m.zdatetime=datetime()
*!*						insert into noinven from memvar
*!*						if gmasteroffice="C"
*!*							insert into xnoinven from memvar
*!*						endif
*!*					endif
*!*				endscan

			if xwavepickadded
				tu("wave")
				tu("vwavepick")
*				tu("outdet")
*				tu("outloc")
				tu("inven")
				tu("invenloc")
				tu("indet")
				tu("inloc")
				tu("adj")
				
				indetadjupdate()
				addsqlrun()		&& updates adj records that need to be changed with sqlexec's
			endif

			delete in wosemi
			tu("wosemi")
		endscan

*		use in vwavepick
		use in wosemi
*		use in outdet
*		use in outloc
		use in inven
		use in invenloc
		use in adj
		use in upcloc
		use in pprep
		use in whseloc
		if used("generatepk")
			use in generatepk
		endif
	endif

	use in wave
endscan

use in whoffice
use in account
use in vwavepick

*per darren, commented out all calls to noinven - mvw 09/18/17
*!*	use in noinven

*!*	if used("xnoinven") and reccount("xnoinven")>0
*!*		select xnoinven
*!*		copy to h:\fox\noinven.xls xls
*!*		emailx("hak.ong@tollgroup.com; Felix.Saracay@tollgroup.com; Marvin.Perez@tollgroup.com",,"Not enough inventory available to repick for styles that were wave picked short","Message delivered by the automated wave repicker",,"h:\fox\noinven.xls")
*!*	endif

try
	schedupdate()
catch
endtry

_screen.caption=gscreencaption
on error

wait clear
