*!* m:\dev\prg\yellsteel940edi_process.prg

CD &lcpath

LogCommentStr = ""
cDelimiter = "*"
cTranslateOption = "NONE"

delimchar = cDelimiter
lcTranOpt= cTranslateOption

lnNum = ADIR(tarray,"*.txt")

IF lnNum = 0
	WAIT WINDOW AT 10,10 "      No "+cCustname+" 940s to import     " TIMEOUT 3
	flagreset()
	normalexit = .T.
	THROW
ENDIF

c997indir = "F:\ftpusers\YellSteel-NJ\997in\"
FOR thisfile = 1  TO lnNum
	STORE "" TO xfile,cFilename
	STORE 0 TO nFileSize
	Xfile = lcpath+tarray[thisfile,1] && +"."
	cfilename = ALLTRIM(LOWER(tarray[thisfile,1]))
	nFileSize = VAL(TRANSFORM(tarray(thisfile,2)))
	cString1 = FILETOSTR(Xfile)
	Archivefile  = (lcArchivePath+cfilename)
	
	SET SAFETY off

	IF "~GS"$cString1
		lcTranOpt = "TILDE"
	ENDIF
	RELEASE cString1

	WAIT WINDOW "Importing file: "+cfilename NOWAIT
	DO m:\dev\prg\loadedifile WITH Xfile,delimchar,lcTranOpt,cCustname

	SELECT x856
	LOCATE


	IF USED('yell1')
		USE IN yell1
	ENDIF

	LOCATE FOR x856.segment = "W05"
*!*		IF FOUND()
*!*			IF LEFT(ALLTRIM(x856.f3),3) = "015" AND cOffice # "C"
*!*				WAIT WINDOW "This is a CA 940 file...moving" TIMEOUT 1
*!*				c940OutFile = ("f:\0-picktickets\yellsteel-ca\"+cfilename)
*!*				COPY FILE [&xfile] TO [&c940Outfile]
*!*				DELETE FILE [&xfile]
*!*				LOOP
*!*			ENDIF
*!*		ENDIF
	

	SELECT x856
	LOCATE FOR x856.segment = "GS"
	IF TRIM(x856.f1)="FA" && Acknowledgement File
		WAIT WINDOW "This is an inbound 997 file...moving" TIMEOUT 1
		c997infile = c997indir+cfilename
		COPY FILE [&xfile] TO [&c997infile]
		DELETE FILE [&xfile]
		LOOP
	ENDIF
	c997Outfile = ("f:\ftpusers\yellsteel-nj\940xfer\"+cfilename)
	COPY FILE [&xfile] TO [&c997Outfile]

	LOCATE
*!* Added to verify correct 940 structure, 10.06.2009
	ASSERT .F. MESSAGE "At SEG HDR counts...debug"
	COUNT TO nISA FOR x856.segment = "ISA"
	COUNT TO nIEA FOR x856.segment = "IEA"
	COUNT TO nGS FOR x856.segment = "GS"
	COUNT TO nGE FOR x856.segment = "GE"
	COUNT TO nST FOR x856.segment = "ST"
	COUNT TO nSE FOR x856.segment = "SE"
	IF (nISA#nIEA) OR (nGS#nGE) OR (nST#nSE)
		cWinMsg = "There is a Segment (Loop) discrepancy"
		ASSERT .F. MESSAGE cWinMsg
		normalexit = .F.
		WAIT WINDOW cWinMsg TIMEOUT 3
		THROW
	ENDIF

	LOCATE

	DO create_pt_cursors WITH cUseFolder && Added to prep for SQL Server transition

	DO ("m:\dev\prg\"+cCustname+"940edi_bkdn")

	DO m:\dev\prg\all940_import
*	DO ("m:\dev\prg\"+cCustname+"940edi_import")
	
NEXT thisfile

&& now clean up the 940in archive files, delete for the upto the last 10 days
IF !lTesting
deletefile(lcarchivepath,20)
endif

WAIT CLEAR
WAIT "Entire "+cCustname+" 940 Process Now Complete" WINDOW TIMEOUT 3

*******************
PROCEDURE flagreset
*******************
	IF USED('ftpsetup')
		USE IN FTPSETUP
	ENDIF
	SELECT 0
	USE F:\edirouting\FTPSETUP SHARED
	REPLACE CHKBUSY WITH .F. FOR FTPSETUP.TRANSFER = cTransfer
	USE IN FTPSETUP
ENDPROC
