* AP CHECK SCAN REPORT
* List FMI and SMG checks in prior month that have not had info scanned.


LOCAL loAPCheckScanReportsProcess

loAPCheckScanReportsProcess = CREATEOBJECT('APCheckScanReportsProcess')
loAPCheckScanReportsProcess.MAIN()
CLOSE DATABASES ALL
RETURN

#DEFINE LOGIT 1
#DEFINE WAITIT 2
#DEFINE NOWAITIT 4
#DEFINE SENDIT 8
#DEFINE CR CHR(13)
#DEFINE LF CHR(10)
#DEFINE CRLF CHR(13) + CHR(10)
#DEFINE RETURN_DATA_MANDATORY .F.
#DEFINE RETURN_DATA_NOT_MANDATORY .T.
#DEFINE CSVFORMAT 6
#DEFINE WRITE_ONLY_UNBUFFERED 11
#DEFINE COMMA ","

DEFINE CLASS APCheckScanReportsProcess AS CUSTOM

	cProcessName = 'APCheckScanReportsProcess'

	lTestMode = .T.  && .F. FOR NORMAL OPERATION, .T. TO SUPPRESS ALL BUT DEVELOPER EMAILS AND CHANGE INPUT TABLE, ETC.

	lAutoYield = .T.

	* date time props
	dToday = DATE()
	*dToday = {^2010-11-05}

	cStartTime = TTOC(DATETIME())

	* company - account properties
	
	* choose 1 of the following:
	*cCompany = "TOLL-FMI"
	cCompany = "TOLL-SMG"

	cAcctnumber = ''
	cDsnName = ''
	nSQLHandle = -1
	cCASHACCTKEY = ''
	cCompKey = ''

	* wait window properties
	nWaitWindowTimeout = 2
	lWaitWindowIsOn = .T.

	* logfile properties
	lLoggingIsOn = .T.
	cLogFile = 'F:\UTIL\APCHECKSCANS\LOGFILES\APCheckScanReportsProcess_log.txt'

	* INTERNAL email properties
	lSendInternalEmailIsOn = .T.
	cFrom = 'FMI Corporate <fmicorporate@fmiint.com>'
	cSendTo = 'mbennett@fmiint.com'
	cCC = 'mbennett@fmiint.com'
	cSubject = ''
	cAttach = ''
	cBodyText = ''

	cCOMPUTERNAME = ''
	cUSERNAME = ''


	FUNCTION INIT
		IF NOT DODEFAULT()
			RETURN .F.
		ENDIF
		WITH THIS
			*SET RESOURCE OFF
			CLOSE DATA
			DO setenvi
			SET CENTURY ON
			SET DATE TO ymd
			SET DECIMAL TO 0
			SET TALK OFF
			SET DELETED ON
			SET CONSOLE OFF
			SET EXCLUSIVE OFF
			SET SAFETY OFF
			SET EXACT OFF
			SET STATUS BAR ON
			SET SYSMENU OFF
			SET ENGINEBEHAVIOR 70
			_VFP.AUTOYIELD = .lAutoYield
			.cCOMPUTERNAME = GETENV("COMPUTERNAME")
			.cUSERNAME = GETENV("USERNAME")
			IF .lTestMode THEN
				.cLogFile = 'F:\UTIL\APCHECKSCANS\LOGFILES\APCheckScanReportsProcess_LOG_TESTMODE.txt'
				.cSendTo = 'mbennett@fmiint.com'
				.cCC = ''
			ENDIF
			.lLoggingIsOn = .lLoggingIsOn AND NOT EMPTY(.cLogFile)
			IF .lLoggingIsOn THEN
				SET ALTERNATE TO (.cLogFile) ADDITIVE
				SET ALTERNATE ON
			ENDIF
		ENDWITH
	ENDFUNC


	FUNCTION DESTROY
		WITH THIS
			IF .lLoggingIsOn  THEN
				SET ALTERNATE OFF
				SET ALTERNATE TO
			ENDIF
		ENDWITH
		DODEFAULT()
	ENDFUNC


	FUNCTION MAIN
		WITH THIS

			LOCAL lnNumberOfErrors, ldToday, ldToDate, ldFromDate, lcMonthYear, lcOutputFile, lcDSN
			LOCAL lcSQLStartdate, lcSQLEnddate, lnResult, lcCompKey

			TRY

				lnNumberOfErrors = 0

				.TrackProgress('', LOGIT)
				.TrackProgress('', LOGIT)
				.TrackProgress('=========================================================', LOGIT+SENDIT)
				.TrackProgress('AP Check Scans Reports Process process started....', LOGIT+SENDIT+NOWAITIT)
				.TrackProgress('COMPUTERNAME = ' + .cCOMPUTERNAME, LOGIT+SENDIT)
				.TrackProgress('USERNAME = ' + .cUSERNAME, LOGIT+SENDIT)
				.TrackProgress('=========================================================', LOGIT+SENDIT)
				IF .lTestMode THEN
					.TrackProgress('                        TEST MODE                        ', LOGIT+SENDIT)
					.TrackProgress('=========================================================', LOGIT+SENDIT)
				ENDIF
				.TrackProgress('', LOGIT)

				ldToday = .dToday
				ldToDate = ldToday - DAY(ldToday)  && this is last day of prior month
				ldFromDate = GOMONTH(ldToDate + 1,-1) && this is first day of prior month

				*!*	**** to force other date range	**************************************************
				*!*	ldFromDate = {^2010-01-01}
				*!*	ldToDate   = {^2010-09-30}
				*!*	**********************************************************************************

				lcMonthYear = PROPER(CMONTH(ldFromDate)) + " " + TRANSFORM(YEAR(ldFromDate))

				.cSubject = 'AP Checks without Scans for ' + .cCompany + ' for: ' + lcMonthYear
				lcOutputFile = "F:\UTIL\APCHECKSCANS\REPORTS\APCHECKSCAN" + .cCompany + lcMonthYear + ".XLS"


				DO CASE
					CASE .cCompany == "TOLL-FMI"
						.cAcctnumber = "38824494"
						.cDsnName = "PLATINUM-408FMI"
						.cCASHACCTKEY = '1010000000'
						.cCompKey = "FMI"
					CASE .cCompany == "TOLL-SMG"
						.cAcctnumber = "38824339"
						.cDsnName = "PLATINUM-408SMG"
						.cCASHACCTKEY = '1010000090'
						.cCompKey = "SMG"
					OTHERWISE
						.TrackProgress('Unexpected .cCompany!', LOGIT+SENDIT)
						THROW
				ENDCASE

				lcDSN = "DSN="+ALLTRIM(.cDsnName)
				.nSQLHandle = SQLSTRINGCONNECT(lcDSN,.T.)

				.TrackProgress('.nSQLHandle = ' + TRANSFORM(.nSQLHandle) + ' after SQLSTRINGCONNECT!', LOGIT+SENDIT)

				IF .nSQLHandle < 1
					THROW
				ENDIF

				lcSQLStartdate = ['] + .platinumdate( ldFromDate ) + [']
				lcSQLEnddate = ['] + .platinumdate( ldToDate ) + [']

				lcSql = ;
					"SELECT APTRAN.DOCUMENTNUMBER AS CHECKNUM, APTRAN.DOCUMENTDATE AS DOCDATE, " + ;
					" APTRAN.CASHACCTKEY AS ACCTKEY,  APTRAN.TRANSACTIONTYPE AS TRANSTYPE, APTRAN.RECDATE " + ;
					" FROM APTRAN " + ;
					" WHERE (APTRAN.TRANSACTIONTYPE='P' OR APTRAN.TRANSACTIONTYPE='V') AND APTRAN.DOCUMENTNUMBER<>'0000' " + ;
					" AND APTRAN.CASHACCTKEY='" + .cCASHACCTKEY + "'" + ;
					" AND APTRAN.RECDATE BETWEEN " + lcSQLStartdate + " AND " + lcSQLEnddate + ;
					" ORDER BY APTRAN.RECDATE "

				.TrackProgress('lcSql = ' + lcSql,LOGIT+SENDIT)
				
				IF USED('checkreg') THEN
					USE IN checkreg
				ENDIF
				
				IF USED('CURCHECKREG2') THEN
					USE IN CURCHECKREG2
				ENDIF
				
				IF USED('CURNOTSCANNED') THEN
					USE IN CURNOTSCANNED
				ENDIF


				IF .ExecSQL(lcSql,'checkreg',RETURN_DATA_MANDATORY) THEN
				
*!*						SELECT checkreg
*!*						BROWSE

					lcCompKey = .cCompKey

					SELECT DISTINCT PADL(ALLTRIM(CHECKNUM),6,'0') AS CHECKNUM, ;
						lcCompKey AS COMPANY ;
						FROM checkreg ;
						INTO CURSOR CURCHECKREG2 ;
						ORDER BY 1
						
*!*						SELECT CURCHECKREG2
*!*						BROWSE

					SELECT CHECKNUM ;
						FROM CURCHECKREG2 ;
						INTO CURSOR CURNOTSCANNED ;
						WHERE CHECKNUM NOT IN (SELECT CHECKNUM FROM F:\WATUSI\ARDATA\apscan WHERE PDFLOADED) ;
						ORDER BY CHECKNUM						
					
						
					SELECT CURNOTSCANNED
					COPY TO (lcOutputFile) XL5
					
					.cAttach = lcOutputFile
				
				ENDIF				


				=SQLDISCONNECT(.nSQLHandle)
				.TrackProgress('AP Check Scans Reports Process process ended normally!',LOGIT+SENDIT)

			CATCH TO loError

				.TrackProgress('There was an error.',LOGIT+SENDIT)
				.TrackProgress('The Error # is: ' + TRANSFORM(loError.ERRORNO), LOGIT+SENDIT)
				.TrackProgress('The Error Message is: ' + TRANSFORM(loError.MESSAGE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred in Program: ' + TRANSFORM(loError.PROCEDURE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred at line #: ' + TRANSFORM(loError.LINENO), LOGIT+SENDIT)
				lnNumberOfErrors = lnNumberOfErrors + 1
				CLOSE DATA

			ENDTRY


			CLOSE DATA
			CLOSE ALL
			WAIT CLEAR
			***************** INTERNAL email results ******************************
			.TrackProgress('About to send status email.',LOGIT)
			.TrackProgress('The logfile is: ' + .cLogFile, SENDIT)
			.TrackProgress('AP Check Scans Reports Process process started: ' + .cStartTime, LOGIT+SENDIT)
			.TrackProgress('AP Check Scans Reports Process process finished: ' + TTOC(DATETIME()), LOGIT+SENDIT)

			IF .lSendInternalEmailIsOn THEN
				DO FORM dartmail2 WITH .cSendTo,.cFrom,.cSubject,.cCC,.cAttach,.cBodyText,"A"
				.TrackProgress('Sent status email.',LOGIT)
			ELSE
				.TrackProgress('Did not send status email: THIS.lSendInternalEmailIsOn = FALSE.',LOGIT)
			ENDIF

		ENDWITH
		RETURN
	ENDFUNC && main


	FUNCTION platinumdate
		LPARAMETERS tDate
		LOCAL lcYear, lcMONTH, lcDay
		** get year
		lcYear = ALLTRIM(STR(YEAR(tDate)))

		** get month
		lcMONTH = IIF(LEN(ALLTRIM(STR(MONTH(tDate))))=1,"0"+ALLTRIM(STR(MONTH(tDate))),ALLTRIM(STR(MONTH(tDate))))

		** get day
		lcDay = IIF(LEN(ALLTRIM(STR(DAY(tDate))))=1,"0"+ALLTRIM(STR(DAY(tDate))),ALLTRIM(STR(DAY(tDate))))

		lcRetval = lcYear + "-" + lcMONTH + "-" + lcDay

		RETURN lcRetval
	ENDFUNC


	FUNCTION ExecSQL
		LPARAMETERS tcSQL, tcCursorName, tlNoDataReturnedIsOkay
		LOCAL llRetval, lnResult
		WITH THIS
			* close target cursor if it's open
			IF USED(tcCursorName)
				USE IN (tcCursorName)
			ENDIF
			WAIT WINDOW "Fetching remote data; please wait..." NOWAIT
			lnResult = SQLEXEC(.nSQLHandle, tcSQL, tcCursorName)
			llRetval = ( lnResult > 0 )
			IF llRetval THEN
				* see if any data came back
				IF NOT tlNoDataReturnedIsOkay THEN
					IF (NOT USED(tcCursorName)) OR EOF(tcCursorName) THEN
						llRetval = .F.
						.TrackProgress('No data was returned by this query: ' +CRLF + tcSQL, LOGIT+SENDIT)
					ENDIF
				ENDIF
			ELSE
				.TrackProgress("Result Code [" + TRANSFORM(lnResult) + "] returned by this query: " + tcSQL, LOGIT+SENDIT)
			ENDIF
			WAIT CLEAR
			RETURN llRetval
		ENDWITH
	ENDFUNC


	PROCEDURE TrackProgress
		* do any combination of Wait Window, writing to logfile, and adding to body of email,
		* based on nFlags parameter.
		LPARAMETERS tcExpression, tnFlags
		WITH THIS
			IF BITAND(tnFlags,LOGIT) = LOGIT THEN
				IF .lLoggingIsOn THEN
					?
					? .cProcessName + "  " + TTOC(DATETIME()) + ": " + tcExpression
				ENDIF
			ENDIF
			IF .lWaitWindowIsOn THEN
				IF BITAND(tnFlags,NOWAITIT) = NOWAITIT THEN
					WAIT WINDOW tcExpression NOWAIT
				ENDIF
				IF BITAND(tnFlags,WAITIT) = WAITIT THEN
					WAIT WINDOW tcExpression TIMEOUT .nWaitWindowTimeout
				ENDIF
			ENDIF
			IF BITAND(tnFlags,SENDIT) = SENDIT THEN
				IF .lSendInternalEmailIsOn THEN
					.cBodyText = .cBodyText + tcExpression + CRLF + CRLF
				ENDIF
			ENDIF
		ENDWITH
	ENDPROC  &&  TrackProgress

ENDDEFINE
