* Report all vehicles which have not had FHWA service
* (i.e., a WO with mainttype = I) within last 335 days.

* Added parameter specifies either Trailers or Trucks 
*
* 06/11/2015 per Gary Fechtenburg, if Trailer, removed the virtual trailers from the report.

LPARAMETERS tcVehType

PUBLIC gHeader
gHeader = "TGF"

LOCAL ldToday, ldLatestServiceDate, llHadService, lnCurrentMileage, lcComment, lnDays
LOCAL llTrucks, llTrailers

STORE .F. TO llTrucks, llTrailers

IF ALLTRIM(UPPER(tcVehType)) = "TRAILER" THEN
    llTrailers = .T.
ENDIF

IF ALLTRIM(UPPER(tcVehType)) = "TRUCK" THEN
    llTrucks = .T.
ENDIF

ldToday = DATE()
lcComment = ''

IF USED('curFHWAService') THEN
    USE IN curFHWAService
ENDIF
IF USED('curFHWAServiceFinal') THEN
    USE IN curFHWAServiceFinal
ENDIF

*CREATE CURSOR curFHWAService ( vehno C(8), lastsvce D, COMMENT C(50), vehtype C(8), nDays i, nDaysDesc i, padvehno C(10) )

IF NOT USED('ORDHDR') THEN
    USE SH!ORDHDR IN 0 ALIAS ORDHDR
ENDIF
SELECT ORDHDR
SET ORDER TO CRDATE

IF llTrucks THEN

    IF NOT USED('TRUCKS') THEN
        USE SH!TRUCKS IN 0 ALIAS TRUCKS
    ENDIF

    WAIT WINDOW "Checking Trucks...." NOWAIT

	SELECT "TRUCK" AS VEHTYPE, ;
		VEHNO, ;
		SPACE(50) AS COMMENT, ;
		SPACE(10) AS PADVEHNO, ;
		0000000 AS NDAYS, ;
		0000000 AS NDAYSDESC, ;
		MAX(crdate) AS LASTSVCE ;
		FROM ORDHDR ;
		WHERE INLIST(UPPER(MAINTTYPE),'I') AND (TYPE = 'TRUCK') ;
		AND VEHNO IN ( SELECT TRUCK_NUM AS VEHNO FROM TRUCKS WHERE ACTIVE ) ;
		AND NOT EMPTY(VEHNO) ;
		GROUP BY 1,2,3,4,5,6 ;
	UNION ALL ;
	SELECT "TRUCK" AS VEHTYPE, ;
		TRUCK_NUM AS VEHNO, ;
		SPACE(50) AS COMMENT, ;
		SPACE(10) AS PADVEHNO, ;
		0000000 AS NDAYS, ;
		0000000 AS NDAYSDESC, ;
		{} AS LASTSVCE ;
		FROM TRUCKS ;
		WHERE ACTIVE ;
		AND NOT EMPTY(TRUCK_NUM) ;
		AND ( TRUCK_NUM NOT IN ( SELECT VEHNO AS TRUCK_NUM FROM ORDHDR WHERE INLIST(UPPER(MAINTTYPE),'I') AND (TYPE = 'TRUCK') ) ) ;
	INTO CURSOR curFHWAService ;
	READWRITE		
		
	SELECT curFHWAService
	SCAN
        IF EMPTY(curFHWAService.LASTSVCE) THEN
            lnDays = 999999
            lcComment = "Never had FHWA inspection"
        ELSE
            lnDays = ldToday - curFHWAService.LASTSVCE
            lcComment = TRANSFORM(lnDays) + " days since last FHWA inspection"
        ENDIF
        REPLACE curFHWAService.NDAYS WITH lnDays, ;
            curFHWAService.NDAYSDESC WITH (1000000 - lnDays), ;
        	curFHWAService.COMMENT WITH lcComment, ;
        	curFHWAService.PADVEHNO WITH PADL(ALLTRIM(curFHWAService.VEHNO),10,"0")	
	ENDSCAN
	
ENDIF && llTrucks


IF llTrailers THEN

    IF NOT USED('TRAILER') THEN
        USE SH!TRAILER IN 0 ALIAS TRAILER
    ENDIF

	WAIT WINDOW "Checking Trailers...." NOWAIT

	SELECT "TRAILER" AS VEHTYPE, ;
		VEHNO, ;
		SPACE(50) AS COMMENT, ;
		SPACE(10) AS PADVEHNO, ;
		0000000 AS NDAYS, ;
		0000000 AS NDAYSDESC, ;
		MAX(CRDATE) AS LASTSVCE ;
		FROM ORDHDR ;
		WHERE INLIST(UPPER(MAINTTYPE),'I') AND (TYPE = 'TRAILER') ;
		AND VEHNO IN ( SELECT TRAIL_NUM AS VEHNO FROM TRAILER WHERE ACTIVE ) ;
		AND VEHNO NOT IN ('1701','1702','1703','1704','1705','1706','1707','1732','1750','1752','1756','1757','1758','1703T','1755T') ;
		AND NOT EMPTY(VEHNO) ;
		GROUP BY 1,2,3,4,5,6 ;
		UNION ALL ;
	SELECT "TRAILER" AS VEHTYPE, ;
		TRAIL_NUM AS VEHNO, ;
		SPACE(50) AS COMMENT, ;
		SPACE(10) AS PADVEHNO, ;
		0000000 AS NDAYS, ;
		0000000 AS NDAYSDESC, ;
		{} AS LASTSVCE ;
		FROM TRAILER ;
		WHERE ACTIVE ;
		AND NOT EMPTY(TRAIL_NUM) ;
		AND ( TRAIL_NUM NOT IN ( SELECT VEHNO AS TRAIL_NUM FROM ORDHDR WHERE INLIST(UPPER(MAINTTYPE),'I') AND (TYPE = 'TRAILER') ) ) ;
		AND TRAIL_NUM NOT IN ('1701','1702','1703','1704','1705','1706','1707','1732','1750','1752','1756','1757','1758','1703T','1755T') ;
	INTO CURSOR curFHWAService ;
	READWRITE
		
		
	SELECT curFHWAService
	SCAN
        IF EMPTY(curFHWAService.LASTSVCE) THEN
            lnDays = 999999
            lcComment = "Never had FHWA inspection"
        ELSE
            lnDays = ldToday - curFHWAService.LASTSVCE
            lcComment = TRANSFORM(lnDays) + " days since last FHWA inspection"
        ENDIF
        REPLACE curFHWAService.NDAYS WITH lnDays, ;
            curFHWAService.NDAYSDESC WITH (1000000 - lnDays), ;
        	curFHWAService.COMMENT WITH lcComment, ;
        	curFHWAService.PADVEHNO WITH PADL(ALLTRIM(curFHWAService.VEHNO),10,"0")	
	ENDSCAN
	
ENDIF  && llTrailers

    
*prepare & run report
SELECT * ;
	FROM curFHWAService ;
	INTO CURSOR curFHWAServiceFinal ;
	WHERE NDAYS > 335 ;
	ORDER BY vehtype, nDaysDesc, padvehno

WAIT CLEAR

IF USED('curFHWAServiceFinal') AND NOT EOF('curFHWAServiceFinal') THEN
	SELECT curFHWAServiceFinal
	GOTO TOP
	*BROWSE
	KEYBOARD "{ctrl-f10}"
	REPORT FORM SHFHWASERVICE PREVIEW NOCONSOLE
ELSE
	X3WINMSG("There are no vehicles which have not had FHWA inspection within last 335 days.")
ENDIF

IF USED('curFHWAService') THEN
    USE IN curFHWAService
ENDIF

IF USED('curFHWAServiceFinal') THEN
    USE IN curFHWAServiceFinal
ENDIF

RETURN

*!*	IF llForklifts THEN
*!*	    * FORKLIFTS
*!*	    IF NOT USED('VEHMAST') THEN
*!*	        USE SH!VEHMAST IN 0 ALIAS VEHMAST
*!*	    ENDIF   
*!*	    
*!*	    	** new code 9/5/07 MB
*!*	    WAIT WINDOW "Checking Trailers...." NOWAIT

*!*		SELECT "FORKLIFT" AS VEHTYPE, ;
*!*			VEHNO, ;
*!*			SPACE(50) AS COMMENT, ;
*!*			SPACE(10) AS PADVEHNO, ;
*!*			0000000 AS NDAYS, ;
*!*			0000000 AS NDAYSDESC, ;
*!*			MAX(crdate) AS LASTSVCE ;
*!*			FROM ORDHDR ;
*!*			WHERE INLIST(UPPER(MAINTTYPE),'O','P') AND (TYPE = 'MISC') ;
*!*			AND VEHNO IN ( SELECT VEHNO FROM VEHMAST WHERE ACTIVE ) ;
*!*			AND NOT EMPTY(VEHNO) ;
*!*			GROUP BY 1,2,3,4,5,6 ;
*!*		UNION ALL ;
*!*		SELECT "FORKLIFT" AS VEHTYPE, ;
*!*			VEHNO, ;
*!*			SPACE(50) AS COMMENT, ;
*!*			SPACE(10) AS PADVEHNO, ;
*!*			0000000 AS NDAYS, ;
*!*			0000000 AS NDAYSDESC, ;
*!*			{} AS LASTSVCE ;
*!*			FROM VEHMAST ;
*!*			WHERE ACTIVE ;
*!*			AND NOT EMPTY(VEHNO) ;
*!*			AND ( VEHNO NOT IN ( SELECT VEHNO FROM ORDHDR WHERE INLIST(UPPER(MAINTTYPE),'I') AND (TYPE = 'MISC') ) ) ;
*!*		INTO CURSOR curFHWAService ;
*!*		READWRITE ;

