PARAMETERS nWO_Num,nAcctNum,cCustName,cPPName,lUCC,cOffice
IF VARTYPE(cOffice) # "C"
	cOffice = "C"
ENDIF
SET ASSERTS ON
ASSERT .f. MESSAGE "At beginning SQL WO for Nanjing"

cFileOutName = "V"+TRIM(cPPName)+"pp"
nAcct = ALLTRIM(STR(nAcctNum))
cSQL = "tgfnjsql01"

SELECT 0
USE f:\wh\sqlpassword
cSQLPass = ALLTRIM(sqlpassword.password)
USE IN sqlpassword
*cSqlPass = "B%g23!7#$"

lcDSNLess="driver=SQL Server;server=tgfnjsql01;uid=SA;pwd=&cSQLPass;DATABASE=PICKPACK"

nHandle=SQLSTRINGCONNECT(m.lcDSNLess,.T.)
SQLSETPROP(nHandle,"DispLogin",3)
WAIT WINDOW "Now processing "+cCustName+" SQL view...please wait" NOWAIT
IF nHandle=0 && bailout
	WAIT WINDOW "Could not make SQL connection" TIMEOUT 1
	cRetMsg = "NO SQL CONNECT"
	RETURN cRetMsg
ENDIF

IF DATETIME()<DATETIME(2017,08,24,19,00,00)
SET STEP ON 
ENDIF

nWo   = ALLTRIM(STR(nWO_Num))

*!* Changed select to drop accountid requirement, 12.30.2005
WAIT CLEAR
IF DATETIME() < DATETIME(2018,03,27,22,30,00) OR lTesting
		
	lcsql=[SELECT * FROM dbo.labels Labels]
	lcQ2 = [ WHERE Labels.wo_num = ]
	lcQ3 = "&nWo "
	IF lUCC
		lcQ6 = [ order by Labels.ship_ref,Labels.outshipid,Labels.ucc]
	ELSE
		lcQ6 = [ order by Labels.ship_ref,Labels.outshipid,Labels.CartonNum]
	ENDIF

	lcsql = lcsql+lcQ2+lcQ3+lcQ6
else
	if usesql()
		if lucc
			xorderby="order by ship_ref, outshipid, ucc"
		else
			xorderby="order by ship_ref, outshipid, cartonnum"
		endif
		
		xsqlexec("select * from cartons where wo_num="+nwo+" and ucc#'CUTS' "+xorderby,cFileOutName,,"pickpack")

	else
		lcsql=[SELECT * FROM dbo.cartons Cartons]
		lcQ2 = [ WHERE Cartons.wo_num = ]
		lcQ3 = "&nWo "
		IF lUCC
			lcQ6 = [ order by Cartons.ship_ref,Cartons.outshipid,Cartons.ucc]
		ELSE
			lcQ6 = [ order by Cartons.ship_ref,Cartons.outshipid,Cartons.CartonNum]
		ENDIF

		lcsql = lcsql+lcQ2+lcQ3+lcQ6
	endif
ENDIF

IF USED('sqlwonanjing')
	USE IN sqlwonanjing
ENDIF

if usesql()
else
	llSuccess=SQLEXEC(nHandle,lcsql,cFileOutName)

	IF llSuccess=0  && no records 0 indicates no records and 1 indicates records found
		WAIT WINDOW "No "+cCustName+" records found" TIMEOUT 1
		cRetMsg = "NO SQL CONNECT"
		RETURN cRetMsg
	ENDIF
endif

GO BOTT
WAIT CLEAR
*WAIT "" TIMEOUT 3
SQLCANCEL(nHandle)
SQLDISCONNECT(nHandle)

cRetMsg = "OK"
RETURN cRetMsg
