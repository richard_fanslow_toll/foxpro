PARAMETERS cFilename

SELECT x856
SET FILTER TO

SELECT x856
DO WHILE !EOF("x856")

	cSegment = TRIM(x856.segment)
	cF1 = TRIM(x856.f1)

	IF INLIST(TRIM(x856.segment),"ISA","GS","ST")
		SELECT x856
		IF !EOF()
			SKIP 1 IN x856
			LOOP
		ENDIF
	ENDIF

	IF TRIM(x856.segment) = "W05"  && new shipment
	ENDIF

	IF TRIM(x856.segment) = "GE"
	ENDIF

	IF TRIM(x856.segment) = "N9" AND TRIM(x856.f1) = "UC" && Cust UCC#
		REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"UCC*"+ALLTRIM(x856.f2)
	ENDIF

	IF TRIM(x856.segment) = "N9" AND TRIM(x856.f1) = "IA" && Vendor Num.
		REPLACE xpt.vendor_num WITH ALLTRIM(x856.f2) IN xpt
		cDUNS =  ALLTRIM(x856.f3)
		IF !EMPTY(cDUNS)
			REPLACE xpt.DUNS WITH cDUNS IN xpt
		ENDIF
	ENDIF

	IF TRIM(x856.segment) = "N9" AND TRIM(x856.f1) = "CN" && C/Acct Num.
		REPLACE xpt.cacctnum WITH ALLTRIM(x856.f2) IN xpt
	ENDIF

	IF TRIM(x856.segment) = "G62" AND TRIM(x856.f1) == "37" && Start Date
		cXdate1 = TRIM(x856.f2)
		datestrconversion()
		REPLACE xpt.START WITH dxdate2 IN xpt
	ENDIF

	IF TRIM(x856.segment) = "G62" AND TRIM(x856.f1) == "38" && Cancel Date

	ENDIF

	IF !EOF()
		SKIP 1 IN x856
	ENDIF
ENDDO

WAIT cMailName+" BKDN process complete for file "+xfile WINDOW TIMEOUT 2
RETURN

******************************
PROCEDURE datestrconversion
******************************
	cXdate1 = TRIM(cXdate1)
	dxdate2 = CTOD(SUBSTR(cXdate1,5,2)+"/"+RIGHT(cXdate1,2)+"/"+LEFT(cXdate1,4))
ENDPROC

