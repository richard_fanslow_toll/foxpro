Parameters acctnum

lnacctnum= Val(Transform(acctnum))
lcWHPath = wf("M",4859)

OPEN DATABASE &lcWHPath.wh.dbc shared

Use &lcWHPath.inven In 0

xsqlexec("select * from account where inactive=0","account",,"qq")
index on accountid tag accountid
set order to

Set EngineBehavior 70

Select * from inven where accountid= lnacctnum and units AND TOTQTY > 0 into cursor temp order by style,color,id

Select style from temp into cursor styles readwrite group by style

Select styles
Delete For Empty(style)


* PART I - avail vs. pickup

create cursor xinven ( ;
	style  c(20), ;
	color  c(10), ;
	id     c(10), ;
	size1  c(10),;
	size1Q n(6),;
	size2 c(10),;
	size2Q n(6),;
	size3  c(10),;
	size3Q n(6),;
	size4  c(10),;
	size4Q n(6),;
	size5  c(10),;
	size5Q n(6),;
	size6  c(10),;
	size6Q n(6),;
	size7  c(10),;
	size7Q n(6),;
	size8  c(10),;
	size8Q n(6),;
	size9  c(10),;
	size9Q n(6),;
	size10  c(10),;
	size10Q n(6),;
	size11  c(10),;
	size11Q n(6),;
	size12  c(10),;
	size12Q n(6),;
	size13  c(10),;
	size13Q n(6),;
	size14  c(10),;
	size14Q n(6),;
	size15  c(10),;
	size15Q n(6),;
	size16  c(10),;
	size16Q n(6),;
	size17  c(10),;
	size17Q n(6),;
	size18  c(10),;
	size18Q n(6),;
	size19  c(10),;
	size19Q n(6),;
	size20  c(10),;
	size20Q n(6),;
	size21  c(10),;
	size21Q n(6),;
	size22  c(10),;
	size22Q n(6),;
	size23  c(10),;
	size23Q n(6),;
	size24  c(10),;
	size24Q n(6),;
	size25  c(10),;
	size25Q n(6),;
	size26  c(10),;
	size26Q n(6),;
	size27  c(10),;
	size27Q n(6),;
	size28  c(10),;
	size28Q n(6),;
	size29  c(10),;
	size29Q n(6),;
	size30  c(10),;
	size30Q n(6),;
	size31  c(10),;
	size31Q n(6),;
	size32  c(10),;
	size32Q n(6),;
	size33  c(10),;
	size33Q n(6),;
	size34  c(10),;
	size34Q n(6),;
	size35  c(10),;
	size35Q n(6),;
	size36  c(10),;
	size36Q n(6))

Select styles
Goto top
Scan
 Select xinven
 Append Blank
 replace xinven.style with styles.style in xinven
* replace xinven.color with styles.color in xinven
 
 Select color,totqty,Val(id) as sizeval from temp where temp.style= xinven.style into cursor sizes readwrite

 Select sizes
 Index on sizeval tag sz ascending
 Set order to sz
 
 Goto top
 i=1
 scan
   stylefield= "size"+alltrim(str(i))
   qtyfield= "size"+alltrim(str(i))+"Q"
   replace xinven.&stylefield with sizes.color
   replace xinven.&qtyfield with sizes.totqty
   i=i+1      
 EndScan  

EndScan 



* CREATE SPREADSHEET

filename = "C:\TEMPFOX\"+Alltrim(acctname(lnacctnum))+"_INVENTORY.XLS"
If File(filename)
  Delete File ["]+filename+["]
endif  
  

oexcel=createobject("excel.application")
oworkbook=oexcel.workbooks.add()
osheet1=oworkbook.worksheets[1]

With oExcel.activesheet.columns[1]
  .columnwidth =.columnwidth+6
endwith

For i = 3 to 35
  With oExcel.activesheet.columns[i]
    .columnwidth =.columnwidth-3
  endwith
Next i


with osheet1

.range("A1").value = acctname(lnacctnum)
.range("A2").value = ""
.range("A3").value = ""
.range("A3").value = "Generated on "+trans(date())

Select xinven
Goto top

i=6
scan
	srow=trans(i)
	qrow=trans(i+1)
	
	
	.range("A"+srow).value="'"+Transform(xinven.style)
*	.range("C"+srow).value=xinven.color
    .range("A"+srow).interior.color = Rgb(192,192,192)
    .range("B"+srow).interior.color = Rgb(192,192,192)
   
    
    .range("A"+srow).font.bold=.t.
    .range("B"+srow).font.bold=.t.
    
    .range("A"+srow).borders(7).linestyle=1
    .range("A"+srow).borders(8).linestyle=1
    .range("A"+srow).borders(9).linestyle=1
    .range("A"+srow).borders(10).linestyle=1
    
    .range("A"+srow).borders(7).weight=-4138
    .range("A"+srow).borders(8).weight=-4138
    .range("A"+srow).borders(9).weight=-4138
    .range("A"+srow).borders(10).weight=-4138

    .range("B"+srow).borders(7).linestyle=1
    .range("B"+srow).borders(8).linestyle=1
    .range("B"+srow).borders(9).linestyle=1
    .range("B"+srow).borders(10).linestyle=1
    
    .range("B"+srow).borders(7).weight=-4138
    .range("B"+srow).borders(8).weight=-4138
    .range("B"+srow).borders(9).weight=-4138
    .range("B"+srow).borders(10).weight=-4138
   
	lcformula = "=Sum(C"+qrow+":GG"+qrow+")"
    .range("A"+qrow).formula= lcFormula &&=Sum(D+&qrow:GG+&qrow)"
 
	.range("C"+srow).value=xinven.size1
	If !Empty(xinven.size1)
	  .range("C"+qrow).value=xinven.size1Q
	  .range("C"+srow).interior.color = Rgb(192,192,192)
	EndIf   
	
	.range("D"+srow).value=xinven.size2
	If !Empty(xinven.size2)
	  .range("D"+qrow).value=xinven.size2Q
	  .range("D"+srow).interior.color = Rgb(192,192,192)
	endif  

	.range("E"+srow).value=xinven.size3
	If !Empty(xinven.size3)
	  .range("E"+qrow).value=xinven.size3Q
	  .range("E"+srow).interior.color = Rgb(192,192,192)
    EndIf 
     
	.range("F"+srow).value=xinven.size4
	If !Empty(xinven.size4)
  	  .range("F"+qrow).value=xinven.size4Q
	  .range("F"+srow).interior.color = Rgb(192,192,192)
    EndIf 
    
	.range("G"+srow).value=xinven.size5
	If !Empty(xinven.size5)
  	  .range("G"+qrow).value=xinven.size5Q
	  .range("G"+srow).interior.color = Rgb(192,192,192)
    EndIf 
    
	.range("H"+srow).value=xinven.size6
	If !Empty(xinven.size6)
  	  .range("H"+qrow).value=xinven.size6Q
	  .range("H"+srow).interior.color = Rgb(192,192,192)
    EndIf 
   
	.range("I"+srow).value=xinven.size7
	If !Empty(xinven.size7)
  	  .range("I"+qrow).value=xinven.size7Q
	  .range("I"+srow).interior.color = Rgb(192,192,192)
    EndIf 
    
	.range("J"+srow).value=xinven.size8
	If !Empty(xinven.size8)
  	  .range("J"+qrow).value=xinven.size8Q
	  .range("J"+srow).interior.color = Rgb(192,192,192)
    EndIf 
    
	.range("K"+srow).value=xinven.size9
	If !Empty(xinven.size9)
  	  .range("K"+qrow).value=xinven.size9Q
	  .range("K"+srow).interior.color = Rgb(192,192,192)
    EndIf 

	.range("L"+srow).value=xinven.size10
	If !Empty(xinven.size10)
  	  .range("L"+qrow).value=xinven.size10Q
	  .range("L"+srow).interior.color = Rgb(192,192,192)
    EndIf 

	.range("M"+srow).value=xinven.size11
	If !Empty(xinven.size11)
  	  .range("M"+qrow).value=xinven.size11Q
	  .range("M"+srow).interior.color = Rgb(192,192,192)
    EndIf 

	.range("N"+srow).value=xinven.size12
	If !Empty(xinven.size12)
  	  .range("N"+qrow).value=xinven.size12Q
	  .range("N"+srow).interior.color = Rgb(192,192,192)
    EndIf 

	.range("O"+srow).value=xinven.size13
	If !Empty(xinven.size13)
  	  .range("O"+qrow).value=xinven.size13Q
	  .range("O"+srow).interior.color = Rgb(192,192,192)
    EndIf 

	.range("P"+srow).value=xinven.size14
	If !Empty(xinven.size14)
  	  .range("P"+qrow).value=xinven.size14Q
	  .range("P"+srow).interior.color = Rgb(192,192,192)
    EndIf 

	.range("Q"+srow).value=xinven.size15
	If !Empty(xinven.size15)
  	  .range("Q"+qrow).value=xinven.size15Q
	  .range("Q"+srow).interior.color = Rgb(192,192,192)
    EndIf 

	.range("R"+srow).value=xinven.size16
	If !Empty(xinven.size16)
  	  .range("R"+qrow).value=xinven.size16Q
	  .range("R"+srow).interior.color = Rgb(192,192,192)
    EndIf 

	.range("S"+srow).value=xinven.size17
	If !Empty(xinven.size17)
  	  .range("S"+qrow).value=xinven.size17Q
	  .range("S"+srow).interior.color = Rgb(192,192,192)
    EndIf 

	.range("T"+srow).value=xinven.size18
	If !Empty(xinven.size18)
  	  .range("T"+qrow).value=xinven.size18Q
	  .range("T"+srow).interior.color = Rgb(192,192,192)
    EndIf 

	.range("U"+srow).value=xinven.size19
	If !Empty(xinven.size19)
  	  .range("U"+qrow).value=xinven.size19Q
	  .range("U"+srow).interior.color = Rgb(192,192,192)
    EndIf 

	.range("V"+srow).value=xinven.size20
	If !Empty(xinven.size20)
  	  .range("V"+qrow).value=xinven.size20Q
	  .range("V"+srow).interior.color = Rgb(192,192,192)
    EndIf 

	.range("W"+srow).value=xinven.size21
	If !Empty(xinven.size21)
  	  .range("W"+qrow).value=xinven.size21Q
	  .range("W"+srow).interior.color = Rgb(192,192,192)
    EndIf 

	.range("X"+srow).value=xinven.size22
	If !Empty(xinven.size22)
  	  .range("X"+qrow).value=xinven.size22Q
	  .range("X"+srow).interior.color = Rgb(192,192,192)
    EndIf 

	.range("Y"+srow).value=xinven.size23
	If !Empty(xinven.size23)
  	  .range("Y"+qrow).value=xinven.size23Q
	  .range("Y"+srow).interior.color = Rgb(192,192,192)
    EndIf 

	.range("Z"+srow).value=xinven.size24
	If !Empty(xinven.size24)
  	  .range("Z"+qrow).value=xinven.size24Q
	  .range("Z"+srow).interior.color = Rgb(192,192,192)
    EndIf 

	.range("AA"+srow).value=xinven.size25
	If !Empty(xinven.size25)
  	  .range("AA"+qrow).value=xinven.size25Q
	  .range("AA"+srow).interior.color = Rgb(192,192,192)
    EndIf 

	.range("BB"+srow).value=xinven.size26
	If !Empty(xinven.size26)
  	  .range("BB"+qrow).value=xinven.size26Q
	  .range("BB"+srow).interior.color = Rgb(192,192,192)
    EndIf 

	.range("CC"+srow).value=xinven.size27
	If !Empty(xinven.size27)
  	  .range("CC"+qrow).value=xinven.size27Q
	  .range("CC"+srow).interior.color = Rgb(192,192,192)
    EndIf 

	.range("DD"+srow).value=xinven.size28
	If !Empty(xinven.size28)
  	  .range("DD"+qrow).value=xinven.size28Q
	  .range("DD"+srow).interior.color = Rgb(192,192,192)
    EndIf 

	.range("EE"+srow).value=xinven.size29
	If !Empty(xinven.size29)
  	  .range("EE"+qrow).value=xinven.size29Q
	  .range("EE"+srow).interior.color = Rgb(192,192,192)
    EndIf 

	.range("FF"+srow).value=xinven.size30
	If !Empty(xinven.size30)
  	  .range("FF"+qrow).value=xinven.size30Q
	  .range("FF"+srow).interior.color = Rgb(192,192,192)
    EndIf 


    i=i+4
endscan
endwith
oworkbook.saveas(filename)
oexcel.quit()
Release oexcel
close data all 

Wait window at 10,10 "Complete" timeout 1
********************************************************************************************************
procedure acctname

lparameter xaccountid

if seek(xaccountid,"account","accountid")
  return trim(account.acctname)
else
  return ""
endif



************************************************************
