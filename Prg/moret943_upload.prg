*!* MORET 943_CREATE.PRG
CLOSE DATABASES ALL
CLEAR ALL
RELEASE ALL
DO m:\dev\prg\lookups

lTesting = .f. && If TRUE, disables certain functions
lOverrideBusy = lTesting
*lOverrideBusy = .T.
DO m:\dev\prg\_setvars WITH lTesting
IF lTesting
	WAIT WINDOW "This is a "+IIF(lTesting,"TEST","PRODUCTION")+" upload" TIMEOUT 2
ENDIF

TRY
	PUBLIC lXfer943,cWhse,cFilename,cFilename2,tsendto,tcc,tsendtoerr,tccerr,lIsError,nOrigAcctNum,nDivLoops,cInFolder,cMod,m.office,lDoDQL
	PUBLIC lDoMail,tfrom,tattach,cxErrMsg,NormalExit,nAcctNum,Acctname,lMoretSend,cOffice,lPick,lBrowFiles,cTransfer,cMasterComments,ninwologid
	_SCREEN.CAPTION = "Moret 943 Upload Process"
	_SCREEN.WINDOWSTATE=IIF(lTesting,2,0)

	tfrom = "TGF EDI Operations <toll-edi-ops@fmiint.com>"
	tattach = " "
	cOffice = "C"  && Seed only

	lTestmail = lTesting
	lBrowFiles = .F.
	lIsError = .F.
	lDoMail = .T.  && If .t., sends mail
	lMoretSend = .F.
	NormalExit = .F.
	lPick = .F.
	nDivLoops = 0
	cMasterComments = ""
	nOrigAcctNum = 5446
	nAcctNum = nOrigAcctNum
	cxErrMsg = ""
	cCustName = "MORET"  && Customer Identifier (for folders, etc.)
	cMailName = "Moret"  && Proper name for eMail, etc.

	cTransfer = "943-MORET-ALL"
	SELECT 0
	USE F:\edirouting\ftpsetup
	LOCATE FOR ftpsetup.transfer = cTransfer
	IF !lTesting
		IF ftpsetup.chkbusy AND !lOverrideBusy
			WAIT WINDOW "Process is busy...exiting" TIMEOUT 2
			CLOSE DATA ALL
			NormalExit = .T.
			THROW
		ENDIF
		REPLACE chkbusy WITH .T.,trig_time WITH DATETIME()  FOR transfer = cTransfer
		USE IN ftpsetup
	ENDIF

	SET STEP ON
	xsqlexec("select * from account where inactive=0","account",,"qq")
	INDEX ON accountid TAG accountid
	SET ORDER TO

	USE F:\3pl\DATA\moretedidivs.DBF IN 0 ALIAS mdivs
	USE "f:\wo\wodata\wolog" IN 0 ALIAS wolog
	useca("inrcv","wh")

	SET STEP ON
	cInFolder = ("F:\0-Packinglist\Moret-All\943_process\")
	cInFolder = IIF(lTesting,cInFolder+"test\",cInFolder)
	DELETE FILE (cInFolder+'*.txt')
	DELETE FILE (cInFolder+'foxuser.*')
	CD [&cInFolder]
	len1 = ADIR(ary1,"*.edi")
	IF len1 = 0
		WAIT WINDOW "There are no 943 files to process...exiting" TIMEOUT 2
		CLOSE DATABASES ALL
		NormalExit = .T.
*		SET STEP ON
		THROW
	ENDIF
	WAIT WINDOW "There are "+TRANSFORM(len1)+" 943 files to process" TIMEOUT 2

	FOR kk = 1 TO len1
*	FOR kk = 1 TO 1
		cFilename = ALLTRIM(ary1[kk,1])
		cCoNum = LEFT(cFilename,2)
		dFiledate = ary1[kk,3]
		cFilename2 = JUSTSTEM(cFilename)+"2.TXT"
		cString = FILETOSTR(cFilename)
		cString = STRTRAN(cString,"|","*")
		cString = STRTRAN(cString,"~",CHR(13))
		STRTOFILE(cString,cFilename2)

		SELECT 0
		CREATE CURSOR tempstyles (STYLE c(20),COLOR c(10),FMITYPE c(3),SMTYPE c(3),FOUND l)
		CREATE CURSOR divisions (divcode c(3),accountid N(5))
		DO m:\dev\prg\createx856a
		SELECT x856
		APPEND FROM &cFilename2 TYPE DELIMITED WITH CHARACTER "*"
		LOCATE
		LOCATE FOR x856.segment = "GS" AND x856.f1 = "PO"
		IF !FOUND()
			cxErrMsg = cFilename+" is NOT a 943 document"
			errormail()
		ENDIF

		LOCATE
		SCAN FOR x856.segment = "N9"  AND x856.f1 = "DV"
			cDiv = ALLTRIM(x856.f2)
			SELECT mdivs
			LOCATE FOR conum = cCoNum AND divcode = cDiv
			IF !FOUND()
				cxErrMsg = "Bad Co#/DivCode"
				errormail()
			ELSE
				nAcctNum = mdivs.accountid
				IF EMPTY(nAcctNum)
					cxErrMsg = "Missing Co#/Acct ID"
					errormail()
				ENDIF
			ENDIF

			IF nAcctNum = 5741  && added to avoid creating separate work orders for different JMI Kids divisions
				cDiv = '002'
			ENDIF

			SELECT divisions
			LOCATE FOR divisions.accountid = nAcctNum AND divisions.divcode = cDiv
			IF !FOUND()
				INSERT INTO divisions (divcode,accountid) VALUES (cDiv,nAcctNum)
			ENDIF
		ENDSCAN

		SELECT divisions
		IF cCoNum = '01'
			nDivLoops = RECCOUNT()
		ELSE
			nDivLoops = 1
		ENDIF
		LOCATE
		IF lTesting
			BROWSE
		ENDIF

		SELECT x856
		FOR uu = 1 TO 1

			LOCATE FOR x856.segment = "N1" AND x856.f1 = "WH"
			DO CASE
			CASE x856.f4 = "FMIFL"
				cWhseLoc = "FL"
				cWhse = "MIAMI"
				cOffice = "M"
				cMod = cOffice
			CASE x856.f4 = "FMINJ"
				cWhseLoc = "NJ"
				cWhse = "CARTERET"
				cOffice = "I"
				cMod = cOffice
			CASE x856.f4 = "FMIML"
				cWhseLoc = "ML"
				cWhse = "MIRA LOMA"
				cOffice = "L"
				cMod = cOffice
			CASE UPPER(x856.f4) = "TOLLO"  && Ontario no longer being used, per Liana, 12.11.2015
				cWhseLoc = "CR"
				cWhse = "CARSON"
				cOffice = "X"
				cMod = cOffice
			OTHERWISE
				cWhseLoc = "SP"
				cWhse = "SAN PEDRO"
				cOffice = "C"
				cMod = "7"
			ENDCASE

			WAIT WINDOW "This is a "+cWhse+" inbound file" TIMEOUT 2

			cOffice=IIF(INLIST(cOffice,"1","2","7"),"C",cOffice)
			gOffice = cMod
			gMasteroffice = cOffice
			cWhseMod = LOWER("wh"+cMod)

			cArchiveFolder = IIF(lTesting,("F:\0-Packinglist\Moret-All\943_process\temparchive\"),("F:\FTPUSERS\MORET\943in\archive\"))
			cArchiveFile = (cArchiveFolder+cFilename)
			cHoldFolder = "F:\0-Packinglist\Moret-All\943_process\hold\"
			cHoldFile = (cHoldFolder+cFilename)
			setuprcvmail(cOffice)

			IF lTesting
				cUseFolder = "F:\WHP\WHDATA\"
			ELSE
				xReturn = "XXX"
*SET STEP ON
				DO m:\dev\prg\wf_alt WITH cOffice,nAcctNum
				IF xReturn = "XXX"
					cxErrMsg = "NO WHSE FOUND IN WMS"
					errormail()
					RETURN
				ENDIF
				cUseFolder = UPPER(xReturn)
			ENDIF
			IF USED('inwolog')
				USE IN inwolog
			ENDIF
			IF USED('pl')
				USE IN pl
			ENDIF

			useca("inwolog","wh")
			xsqlexec("select * from inwolog where .f.","xinwolog",,"wh")

			useca("pl","wh")
			xsqlexec("select * from pl where .f.","xpl",,"wh")
			SELECT xpl
			SCATTER MEMVAR MEMO BLANK


			SELECT xinwolog
			SCATTER MEMVAR BLANK
			STORE "FILE943*"+cFilename TO m.comments
			STORE m.comments+CHR(13)+"FILETIME*"+TTOC(DATETIME()) TO m.comments
			STORE 0 TO m.inwologid,m.plid,npo
			STORE "" TO m.echo,m.po
			STORE "MORET943" TO m.addby
			STORE DATETIME() TO m.adddt
			STORE "MORET943" TO m.addproc
			STORE DATE() TO m.wo_date,m.date_rcvd

			SELECT divisions
			GO uu
			nAcctNum = divisions.accountid
			m.accountid = nAcctNum
			IF SEEK(nAcctNum,'account','accountid')
				m.Acctname = account.Acctname
			ENDIF
			cDiv = ALLTRIM(divisions.divcode)

			SELECT x856
*!* Scan for PnP pack of 1 errors
			cStyleList = ""
			SCAN
				DO CASE
				CASE x856.segment = "W06"
					cAcct_ref = ALLTRIM(x856.f2)
				CASE x856.segment = "W27"
					cContainer = UPPER(ALLTRIM(x856.f6)+ALLTRIM(x856.f7))
				CASE x856.segment = "W04"
					IF INLIST(ALLTRIM(x856.f2),"PG","EA")
						cStyle1 = UPPER(ALLTRIM(x856.f5))
						SKIP 4 IN x856
						IF x856.segment = 'N9' AND x856.f1 = 'PY'
							IF INT(VAL(x856.f2)) = 1
								cStyleList = IIF(EMPTY(cStyleList),cStyle1,cStyleList+CHR(13)+cStyle1)
							ENDIF
						ENDIF
					ENDIF
				ENDCASE
			ENDSCAN

			IF !EMPTY(cStyleList)
				IF FILE(cFilename)
					COPY FILE [&cFilename] TO [&cHoldfile]
					IF FILE(cHoldFile) AND !lTesting
						DELETE FILE [&cFilename]
					ENDIF
				ENDIF
				packmail()
				closeftpsetup()
				NormalExit = .T.
				THROW
			ENDIF

			IF lTesting
*				SET STEP ON
			ENDIF
			nInstIncr = 0

			SCAN
				DO CASE
				CASE INLIST(x856.segment,"ISA","GS","ST","N1")
					LOOP
				CASE x856.segment = "W06"
					m.inwologid = m.inwologid + 1
					m.acct_ref = ALLTRIM(x856.f2)
					STORE m.acct_ref TO cAcct_ref
					LOOP
				CASE x856.segment = "N9" AND x856.f1 = "ABS"
					m.reference = ALLTRIM(x856.f2)
					IF EMPTY(m.reference)
						LOOP
					ENDIF

				CASE x856.segment = "N9" AND x856.f1 = "ZZ"
*						SET STEP ON
					nInstIncr = nInstIncr+1
					cCommentStr = "INST"+ALLTRIM(STR(nInstIncr))+"*"+ALLTRIM(x856.f2)
*!*							IF EMPTY(cMasterComments)
*!*								cMasterComments = cCommentStr
*!*							ELSE
*!*								cMasterComments = cMasterComments+CHR(13)+cCommentStr
*!*							ENDIF
					cCommentStr = ""

				CASE x856.segment = "W27"
					IF !EMPTY(cMasterComments)
						cMasterComments = cMasterComments+CHR(13)
					ENDIF
					m.container = UPPER(ALLTRIM(x856.f6)+ALLTRIM(x856.f7))
					STORE m.container TO cContainer
*						DISPLAY MEMORY TO FILE "c:\tempfox\mem.xls"
					INSERT INTO xinwolog FROM MEMVAR
					LOOP
				CASE x856.segment = "W04"
					m.totqty = INT(VAL(x856.f1))
					STORE m.totqty TO nTotQty

					lPick = IIF(INLIST(ALLTRIM(x856.f2),"PG","EA"),.T.,.F.)

					m.echo = "PACKTYPE*"+IIF(lPick,"PIK","PRE")
					cUPC = ALLTRIM(x856.f3)
					m.echo = m.echo+CHR(13)+"UPC*"+cUPC
					m.style = UPPER(ALLTRIM(x856.f5))

					cID = ""
					cColor = ""

					SKIP 5 IN x856
					IF x856.f1 # "DV"
						cxErrMsg = "NO WHSE FOUND IN WMS"
						errormail()
					ENDIF

					IF cCoNum = '01'
						lBreakLoop = IIF(x856.f2 = cDiv OR (INLIST(x856.f2,'002','003','004') AND INLIST(cDiv,'002','003','004')),.F.,.T.)
						IF lBreakLoop AND lTesting
							WAIT WINDOW "New Div Code: "+ALLTRIM(x856.f2) TIMEOUT 2
						ENDIF
					ELSE
						lBreakLoop = .F.
					ENDIF

					SELECT x856
					SKIP -5 IN x856
					IF lBreakLoop
						SELECT xinwolog
						SCATTER MEMVAR MEMO
						SKIP 1 IN divisions
						cDiv = ALLTRIM(divisions.divcode)
						m.accountid = divisions.accountid
						IF SEEK(m.accountid,'account','accountid')
							m.Acctname = account.Acctname
						ENDIF
						m.inwologid = m.inwologid+1
						STORE 0 TO m.plinqty,m.plunitsinqty
						APPEND BLANK
						GATHER MEMVAR MEMO
						SKIP -1 IN x856
						LOOP
					ENDIF

					IF "-"$m.style
						cColor = UPPER(ALLTRIM(SUBSTR(m.style,AT("-",m.style)+1)))
						IF !EMPTY(cColor)
							m.echo = m.echo+CHR(13)+"COLOR*"+cColor
						ENDIF
						cStyle = UPPER(PADR(ALLTRIM(LEFT(m.style,AT("-",m.style)-1)),20))
					ELSE
						cStyle = UPPER(ALLTRIM(m.style))
					ENDIF
					LOOP
				CASE x856.segment = "G69"
					m.echo = m.echo+CHR(13)+"DESC*"+UPPER(ALLTRIM(x856.f1))
					LOOP
				CASE x856.segment = "N9"
					DO CASE
					CASE x856.f1 = "EA"  && Pack qty
						m.pack = IIF(lPick,ALLTRIM(x856.f2),"1")
						STORE m.pack TO cPack
						m.casepack = INT(VAL(cPack))
						m.oldpack = m.casepack
					CASE x856.f1 = "PO"
						m.echo = m.echo+CHR(13)+"PONUM*"+ALLTRIM(x856.f2)
						IF !"PONUM*"$xinwolog.comments
							REPLACE xinwolog.comments WITH xinwolog.comments+CHR(13)+"PONUM*"+ALLTRIM(x856.f2)
						ENDIF
					CASE x856.f1 = "PY"  && Inner cartons/polybags
						IF !lPick
							m.pack = "1"  && cPack  Changed per Ed Kurowski, 04.25.2012
							m.casepack = INT(VAL(m.pack))
							m.units = .F.
							cInners = ALLTRIM(x856.f2)
							nInners = INT(VAL(cInners))
							npo = npo+1
							m.po = ALLTRIM(STR(npo))
							m.plid = m.plid + 1
							REPLACE xinwolog.plunitsinqty WITH xinwolog.plunitsinqty + m.totqty
							REPLACE xinwolog.plinqty WITH xinwolog.plinqty + m.totqty
							INSERT INTO xpl FROM MEMVAR
							IF nInners > 1
								IF !("Style "+cStyle)$cMasterComments
									IF EMPTY(cMasterComments)
										cMasterComments = "WHINST*Style "+cStyle+": "+cInners+" inner cartons per Master carton"
									ELSE
										cMasterComments = cMasterComments+CHR(13)+"WHINST*Style "+cStyle+": "+cInners+" inner cartons per Master carton"
									ENDIF
								ENDIF
							ENDIF
							m.units = .T.
							m.plid = m.plid + 1
							INSERT INTO xpl FROM MEMVAR
							IF nInners > 1
								IF !("Style "+cStyle)$cMasterComments
									IF EMPTY(cMasterComments)
										cMasterComments = "WHINST*Style "+cStyle+": "+cInners+" inner cartons per Master carton"
									ELSE
										cMasterComments = cMasterComments+CHR(13)+"WHINST*Style "+cStyle+": "+cInners+" inner cartons per Master carton"
									ENDIF
								ENDIF
							ENDIF
						ELSE
							cInners = ALLTRIM(x856.f2)
							nInners = INT(VAL(cInners))
							m.pack = cInners
							m.casepack = INT(VAL(m.pack))
							nDivPack = nTotQty/nInners
							IF nDivPack < 1
								SET STEP ON
								IF FILE(cFilename)
									COPY FILE [&cFilename] TO [&cHoldfile]
									IF FILE(cHoldFile) AND !lTesting
										DELETE FILE [&cFilename]
									ENDIF
								ENDIF
								divpackmail()
								closeftpsetup()
								NormalExit = .T.
								THROW
							ENDIF

							nOVPack = MOD(nTotQty,nInners)
							lOVPack = IIF (nOVPack > 0,.T.,.F.)
							IF lOVPack
								m.totqty = FLOOR(nTotQty/nInners)
								IF m.totqty = 0
									m.totqty = 1
								ENDIF
							ELSE
								IF BETWEEN((nTotQty/nInners),0,.999)
									m.totqty = 1
								ELSE
									m.totqty = (nTotQty/nInners)
								ENDIF
							ENDIF
							m.units = .F.
*									m.totqty = nTotQty/nInners
*									m.totqty = nTotQty/m.oldpack
							REPLACE xinwolog.plinqty WITH xinwolog.plinqty + m.totqty IN xinwolog
							npo = npo+1
							m.po = ALLTRIM(STR(npo))
							m.plid = m.plid + 1
							INSERT INTO xpl FROM MEMVAR
							IF nInners > 1
								IF !("Style "+cStyle)$cMasterComments
									IF EMPTY(cMasterComments)
										cMasterComments = "WHINST*Style "+cStyle+": "+cInners+" inner cartons per Master carton"
									ELSE
										cMasterComments = cMasterComments+CHR(13)+"WHINST*Style "+cStyle+": "+cInners+" inner cartons per Master carton"
									ENDIF
								ENDIF
							ENDIF
							m.units = .T.
							m.pack = "1"
							m.casepack = INT(VAL(m.pack))
							m.plid = m.plid + 1

							m.totqty = IIF(lOVPack,nTotQty-nOVPack,nTotQty)
							REPLACE xinwolog.plunitsinqty WITH xinwolog.plunitsinqty + m.totqty IN xinwolog
							INSERT INTO xpl FROM MEMVAR
							IF nInners > 1
								IF !("Style "+cStyle)$cMasterComments
									IF EMPTY(cMasterComments)
										cMasterComments = "WHINST*Style "+cStyle+": "+cInners+" inner cartons per Master carton"
									ELSE
										cMasterComments = cMasterComments+CHR(13)+"WHINST*Style "+cStyle+": "+cInners+" inner cartons per Master carton"
									ENDIF
								ENDIF
							ENDIF

							IF lOVPack AND lPick
								m.totqty = 1
								m.pack = TRANS(nOVPack)
								m.units = .F.
								m.plid = m.plid + 1
								npo = npo+1
								m.po = ALLTRIM(STR(npo))
								REPLACE xinwolog.plinqty WITH xinwolog.plinqty + m.totqty IN xinwolog
								INSERT INTO xpl FROM MEMVAR
								m.units = .T.
								m.pack = "1"
								m.plid = m.plid + 1
								m.totqty = nOVPack
								REPLACE xinwolog.plunitsinqty WITH xinwolog.plunitsinqty + m.totqty IN xinwolog
								INSERT INTO xpl FROM MEMVAR
							ENDIF

						ENDIF
					ENDCASE

				CASE x856.segment = "W20"
					m.ctnwt = VAL(x856.f4)
					m.ctncube = VAL(x856.f8)
					IF lPick
						SELECT xpl
						SKIP -1 IN xpl
						REPLACE xpl.ctnwt WITH m.ctnwt NEXT 1
						REPLACE xpl.ctncube WITH m.ctncube NEXT 1
						REPLACE xinwolog.weight WITH xinwolog.weight + m.ctnwt IN xinwolog
						REPLACE xinwolog.cuft WITH xinwolog.cuft + m.ctncube IN xinwolog
						SKIP 1 IN xpl
					ENDIF
					REPLACE xpl.ctnwt WITH m.ctnwt NEXT 1
					REPLACE xpl.ctncube WITH m.ctncube NEXT 1
					REPLACE xinwolog.weight WITH xinwolog.weight + m.ctnwt IN xinwolog
					REPLACE xinwolog.cuft WITH xinwolog.cuft + m.ctncube IN xinwolog
					LOOP
				CASE x856.segment = "GE"
					IF !EMPTY(cMasterComments) AND cWhse#"ONTARIO"
						REPLACE xinwolog.printcomments WITH xinwolog.printcomments+CHR(13)+xinwolog.comments+CHR(13)+cMasterComments
					ENDIF
					EXIT
				ENDCASE
			ENDSCAN

			IF !lTesting
				SELECT xinwolog
				LOCATE
				SCAN
					xAcct_Num =  xinwolog.accountid
					xContainer = ALLTRIM(xinwolog.CONTAINER)
					xAcct_ref = ALLTRIM(xinwolog.acct_ref)
					WAIT WINDOW "AT XINWOLOG RECORD "+ALLTRIM(STR(RECNO())) NOWAIT NOCLEAR
					csq1 = [select * from inwolog where 	accountid = ]+ALLTRIM(STR(xAcct_Num))+[ and container = ']+xContainer+[' and acct_ref = ']+xAcct_ref+[']
					xsqlexec(csq1,"p1",,"wh")
					LOCATE
					IF !EOF()
						SET STEP ON
						cWO = TRANSFORM(inwolog.wo_num)
						dupemail(cWO)
						SELECT xinwolog
						ninwologid = xinwolog.inwologid
						DELETE NEXT 1
						SELECT xpl
						DELETE FOR xpl.inwologid = ninwologid
						SELECT xinwolog
						WAIT WINDOW "This set of values already exists in the INWOLOG table...looping" TIMEOUT 2
						LOOP
					ENDIF
				ENDSCAN
			ENDIF

			SELECT STYLE,PACK,1 AS CNT FROM xpl WHERE !units INTO CURSOR xpl1
			SELECT STYLE,PACK,SUM(CNT) AS cnt1 FROM xpl1 GROUP BY 1,2 INTO CURSOR xpl2
			LOCATE

			SCAN FOR cnt1 > 1
				SCATTER MEMVAR
				m.style = UPPER(m.style)
				SELECT xpl
				LOCATE FOR xpl.STYLE = m.style AND xpl.PACK = m.pack AND !units
				IF EOF()
					EXIT
				ENDIF
				nRec = RECNO()
				nCtns = 0
				nUnits = 0
				SCAN FOR xpl.STYLE = m.style AND xpl.PACK = m.pack AND !units AND RECNO() > nRec AND !DELETED()
					nCtns = nCtns + xpl.totqty
					SKIP 1 IN xpl
					nUnits = nUnits + xpl.totqty
					SKIP -1 IN xpl
					DELETE NEXT 2 IN xpl
				ENDSCAN
				SELECT xpl
				GO nRec
				REPLACE xpl.totqty WITH xpl.totqty+nCtns IN xpl
				SKIP 1 IN xpl
				REPLACE xpl.totqty WITH xpl.totqty+nUnits IN xpl
			ENDSCAN

			IF lTesting
				WAIT WINDOW "Checking XPL deleted recs" TIMEOUT 3
				SELECT xpl
				LOCATE
				SET DELETED OFF
*				Browse
				SET DELETED ON

				SELECT xinwolog
				LOCATE
				BROWSE
				SELECT xpl
				LOCATE
				BROWSE
				cancel
			ENDIF


			nUploadCount = 0

			SELECT xinwolog
			LOCATE
			WAIT WINDOW "Currently in "+UPPER(IIF(lTesting,"test","production"))+" mode, base folder: "+cUseFolder TIMEOUT 2
*			SET STEP ON
			SCAN  FOR !DELETED() && Scanning xinwolog here
				SCATTER MEMVAR MEMO
				cPLCtns = ALLTRIM(STR(m.plinqty))
				cPLUnits = ALLTRIM(STR(m.plunitsinqty))
				nWO_num = dygenpk("wonum",cWhseMod)
				m.wo_num = nWO_num
				cWO_Num = ALLTRIM(STR(nWO_num))

				WAIT WINDOW "WO# is "+cWO_Num TIMEOUT 2

				SELECT inwolog

				m.mod = cMod
				m.office = cOffice
				insertinto("inwolog","wh",.T.)
				nUploadCount = nUploadCount+1

				SELECT xpl
				SCAN FOR inwologid=xinwolog.inwologid
					SCATTER FIELDS EXCEPT inwologid MEMVAR MEMO
					m.wo_num = nWO_num
					m.office = cOffice
					m.mod = cMod
					insertinto("pl","wh",.T.)
				ENDSCAN
				goodmail()
			ENDSCAN

			IF nUploadCount = 0
				SET STEP ON
				IF FILE(cFilename)
					COPY FILE [&cFilename] TO [&cArchivefile]
					IF FILE(cArchiveFile) AND !lTesting
						DELETE FILE [&cFilename]
					ENDIF
				ENDIF
				EXIT
			ENDIF

			STORE "" TO xtruckwonum, xtruckseal
			DO "m:\dev\prg\whtruckwonum.prg" WITH inwolog.wo_num, inwolog.REFERENCE, inwolog.CONTAINER, inwolog.accountid
			REPLACE truckwonum WITH xtruckwonum, seal WITH xtruckseal IN inwolog
			RELEASE xtruckwonum, xtruckseal

			cUploadCount = ALLTRIM(STR(nUploadCount))
			WAIT WINDOW "Moret 943 file "+cFilename+" processed."+CHR(13)+"Header records loaded: "+cUploadCount TIMEOUT 1
			IF FILE(cFilename)
				COPY FILE [&cFilename] TO [&cArchivefile]
				IF FILE(cArchiveFile) AND !lTesting
					DELETE FILE [&cFilename]
				ENDIF
			ENDIF

			SELECT pl
			COUNT TO xnumskus FOR !units AND wo_num=inwolog.wo_num

			xsqlexec("select * from inrcv where container='"+inwolog.CONTAINER+"' and status#'CONFIRMED'","xinrcv",,"wh")
			SELECT xinrcv
			IF RECCOUNT() > 0
				xsqlexec("update inrcv set inwonum="+TRANSFORM(inwolog.wo_num)+" where container='"+inwolog.CONTAINER+"' and status#'CONFIRMED'",,,"wh")
			ELSE
				SELECT inrcv
				SCATTER MEMVAR MEMO BLANK
				m.mod = cMod
				m.office = cOffice
				m.inrcvid=dygenpk("inrcv","whall")  && new get the ID value
				m.inwonum=inwolog.wo_num
				m.accountid=inwolog.accountid
				m.Acctname=inwolog.Acctname
				m.container=inwolog.CONTAINER
				m.status="NOT AVAILABLE"
				m.skus=xnumskus
				m.cartons=inwolog.plinqty
				m.newdt=DATE()
				m.addfrom="IN"
				m.confirmdt=empty2nul(m.confirmdt)
				m.newdt=empty2nul(m.newdt)
				m.dydt=empty2nul(m.dydt)
				m.firstavaildt=empty2nul(m.firstavaildt)
				m.apptdt=empty2nul(m.apptdt)
				m.pickedup=empty2nul(m.pickedup)

				INSERT INTO inrcv FROM MEMVAR
				tu("inrcv")
			ENDIF

			tu("inwolog")
			tu("pl")

			IF lTesting
				SELECT inwolog
				GO BOTTOM
				BROWSE TIMEOUT 5
				SELECT pl
				GO BOTTOM
				BROWSE TIMEOUT 5
			ENDIF
		ENDFOR

	ENDFOR
	WAIT WINDOW "All Moret 943s processed...exiting" TIMEOUT 2

	closeftpsetup()

	NormalExit = .T.

CATCH TO oErr
	IF NormalExit = .F.
		ASSERT .F. MESSAGE "AT CATCH SECTION"
		tsubject = cCustName+" 943 Upload Error ("+TRANSFORM(oErr.ERRORNO)+") at "+TTOC(DATETIME())
		tattach  = ""
		tsendto  = tsendtoerr
		tcc      = tccerr
		tmessage = cCustName+" 943 Upload Error"
		lcSourceMachine = SYS(0)
		lcSourceProgram = SYS(16)

		IF EMPTY(cxErrMsg)
			tmessage =tmessage+ "Try/CATCH Exeception Message:"+CHR(13)+;
				[  Error: ] + STR(oErr.ERRORNO) +CHR(13)+;
				[  Procedure: ] + oErr.PROCEDURE +CHR(13)+;
				[  LineNo: ] + STR(oErr.LINENO) +CHR(13)+;
				[  Message: ] + oErr.MESSAGE +CHR(13)+;
				[  Details: ] + oErr.DETAILS +CHR(13)+;
				[  StackLevel: ] + STR(oErr.STACKLEVEL) +CHR(13)+;
				[  LineContents: ] + oErr.LINECONTENTS+CHR(13)+;
				[  UserValue: ] + oErr.USERVALUE+CHR(13)+;
				[  Computer:  ] +lcSourceMachine+CHR(13)+;
				[  940 file:  ] +cFilename+CHR(13)+;
				[  Program:   ] +lcSourceProgram
		ELSE
			tmessage = tmessage + CHR(13) + CHR(13) + cxErrMsg
		ENDIF
		tattach  = ""
		tfrom    ="TGF EDI Processing Center <transload-ops@fmiint.com>"
		DO FORM m:\dev\FRM\dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
	ENDIF
FINALLY
	DELETE FILE (cInFolder+'*.txt')
	DELETE FILE (cInFolder+'foxuser.*')
	SET LIBRARY TO
	CLOSE DATABASES ALL
	ON ERROR
ENDTRY


****************************
PROCEDURE goodmail
****************************
tsubject = cMailName+" EDI FILE (943) Processed, Inv. WO "+cWO_Num
tmessage = REPLICATE("-",90)+CHR(13)
tmessage = tmessage+"Packinglist upload run from machine: "+SYS(0)+CHR(13)
tmessage = tmessage+REPLICATE("-",90)+CHR(13)
tmessage = tmessage+"Inbound Workorders created for "+ALLTRIM(m.Acctname)+", Warehouse: "+cWhse+CHR(13)
tmessage = tmessage+"Data from File : "+cFilename+CHR(13)
tmessage = tmessage+REPLICATE("-",90)+CHR(13)
tmessage = tmessage+"Acct. Ref: "+cAcct_ref+SPACE(2)+"Container: "+cContainer+SPACE(2)+"WO# "+cWO_Num+SPACE(2)+"CTNS: "+cPLCtns+SPACE(2)+"UNITS: "+cPLUnits

IF lDoMail
	DO FORM m:\dev\frm\dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
ENDIF
ENDPROC

********************************
PROCEDURE divpackmail
********************************
tsubject = cMailName+" EDI FILE (943): PnP Calc. Ctn. Qty. of 0"
tmessage = REPLICATE("-",90)+CHR(13)
tmessage = tmessage+"Packinglist upload run from machine: "+SYS(0)+CHR(13)
tmessage = tmessage+REPLICATE("-",90)+CHR(13)
tmessage = tmessage+"Data from File : "+cFilename+CHR(13)
tmessage = tmessage+"Contains PnP items with a calculated carton quantity [Units/Pack] of ZERO."
tmessage = tmessage+CHR(13)+"Please correct and re-send."+CHR(13)
tmessage = tmessage+"Reference#: "+TRIM(m.acct_ref)+", Container: "+cContainer+CHR(13)
tmessage = tmessage+"Style: "+TRIM(m.style)
tcc = IIF(EMPTY(ALLTRIM(tcc)),'lupe@moret.com',ALLTRIM(tcc)+',lupe@moret.com')

IF lDoMail
	DO FORM m:\dev\frm\dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
ENDIF
ENDPROC

****************************
PROCEDURE packmail
****************************
tsubject = cMailName+" EDI FILE (943): PnP Pack of 1"
tmessage = REPLICATE("-",90)+CHR(13)
tmessage = tmessage+"Packinglist upload run from machine: "+SYS(0)+CHR(13)
tmessage = tmessage+REPLICATE("-",90)+CHR(13)
tmessage = tmessage+"Data from File : "+cFilename+CHR(13)
tmessage = tmessage+"Contains PnP items with a pack qty (N9*PY) of 1. This is only allowed for prepacks. Please correct and re-send."+CHR(13)
tmessage = tmessage+"Reference#: "+TRIM(cAcct_ref)+", Container: "+cContainer+CHR(13)+CHR(13)
IF !EMPTY(cStyleList)
	tmessage = tmessage+"Affected styles:"+CHR(13)+cStyleList
ENDIF
IF !lTesting
	tcc = IIF(EMPTY(ALLTRIM(tcc)),'lupe@moret.com',ALLTRIM(tcc)+',lupe@moret.com')
ENDIF

IF lDoMail
	DO FORM m:\dev\frm\dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
ENDIF
ENDPROC

****************************
PROCEDURE dupemail
****************************
PARAMETERS cWO
tsubject = cMailName+" EDI FILE (943): Duplicate"
tmessage = REPLICATE("-",90)+CHR(13)
tmessage = tmessage+"Data from File : "+cFilename+CHR(13)
tmessage = tmessage+"For Account ID: "+TRANSFORM(nAcctNum)
tmessage = tmessage+ ", Container "+cContainer
tmessage = tmessage+ ", Acct. Reference "+cAcct_ref+CHR(13)
tmessage = tmessage+"already exists in the Inbound WO table, in Work Order "+cWO
tmessage = tmessage+CHR(13)+REPLICATE("-",90)+CHR(13)

IF lTesting
	SET STEP ON
ENDIF

IF lDoMail
	DO FORM m:\dev\frm\dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
ENDIF
ENDPROC

****************************
PROCEDURE errormail
****************************
setuprcvmail(cOffice)
tsubject = cMailName+" EDI ERROR (943)"
tsendto = tsendtoerr
tcc = tccerr
IF lMoretSend AND !lTesting
	tcc      = tcc+",FMIConfirmation@moret.com"  && Amended 2/27/08, Joe
ENDIF
tmessage = "File: "+cFilename
tmessage = tmessage+CHR(13)+cxErrMsg

DO FORM m:\dev\frm\dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
NormalExit = .T.
SET STEP ON
THROW
ENDPROC

****************************
PROCEDURE setuprcvmail
****************************
PARAMETERS cOffice
*	ASSERT .f. MESSAGE "In Mail/Folder selection...debug"
SELECT 0
USE F:\3pl\DATA\mailmaster ALIAS mm
LOCATE FOR mm.edi_type = "943" AND mm.GROUP = "MORET" AND mm.office = cOffice
IF !FOUND()
	cxErrMsg = "Office/Loc/Acct not in Mailmaster!"
	errormail()
ENDIF
STORE TRIM(mm.archpath) TO cArchivePath
lUseAlt = mm.use_alt
STORE IIF(lUseAlt,TRIM(mm.sendtoalt),TRIM(mm.sendto)) TO tsendto
STORE IIF(lUseAlt,TRIM(mm.ccalt),TRIM(mm.cc)) TO tcc
LOCATE
LOCATE FOR mm.edi_type = "MISC" AND mm.taskname = "GENERAL"
lUseAlt = mm.use_alt
STORE IIF(lUseAlt,TRIM(mm.sendtoalt),TRIM(mm.sendto)) TO tsendtotest
STORE IIF(lUseAlt,TRIM(mm.ccalt),TRIM(mm.cc)) TO tcctest
STORE IIF(lUseAlt,TRIM(mm.sendtoalt),TRIM(mm.sendto)) TO tsendtoerr
STORE IIF(lUseAlt,TRIM(mm.ccalt),TRIM(mm.cc)) TO tccerr
IF lTesting OR lTestmail
	tsendto = tsendtotest
	tcc = tcctest
ENDIF
USE IN mm

cArchiveFile  = (cArchivePath+cFilename)
ENDPROC

*********************************
PROCEDURE closeftpsetup
*********************************
SELECT 0
USE F:\edirouting\ftpsetup
REPLACE chkbusy WITH .F. FOR transfer = cTransfer
USE IN ftpsetup
ENDPROC
