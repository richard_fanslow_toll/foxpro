* offsets XRACK location with real location
* called from hhlocchgmj.scx, hhlocchgmjwrist.scx, inven.scx, scanrpt.scx, val11.prg

lparameters xmakemsg, xfrom, xxinvenid

if empty(xfrom)
	xfrom=""
endif

if !empty(xfrom) and xfrom#"AUTO2"
	xxinvenloc="invenloc"
else
	xxinvenloc="vinvenloc"
endif

**if no "xrack" cursor was created, need to create now and populate - mvw 08/18/15
if !used("xrack")

*	email("mwinter@fmiint.com","Cursor xrack created in offsetxrack.prg ("+guserid+")",,,,,.t.,,,,,.t.)

	**make sure whseloc is open in each calling form
	select accountid, units, style, color, id, pack, whseloc, locqty, space(7) as oldwhseloc from &xxinvenloc where .f. into cursor xrack readwrite

	select &xxinvenloc
	locate for invenid=xxinvenid and inlist(whseloc,"XRACK","ZRACK") and locqty<0
	if found()
		xxrackqty=locqty
		scan for invenid=xxinvenid and !inlist(whseloc,"XRACK","RACK","BSTAT","PSTAT","PFLOW","CYCLEU","CYCLEZ","CUT") and !hold and locqty>0 and xxrackqty<0
			**for Ariat only look at the slot location not decking or baker shelves - mvw 08/18/15
			if accountid=6532 and seek(&xxinvenloc..whseloc,"whseloc","whseloc") and whseloc.loctype#"SCAN PICK"
				loop
			endif

			scatter memvar
			m.locqty=min(-xxrackqty,&xxinvenloc..locqty)
			insert into xrack from memvar
			xxrackqty=xxrackqty+&xxinvenloc..locqty
		endscan
	endif
endif

select xrack
scan
	xqty=xrack.locqty
	if xmakemsg
		xmsg=xmsg+"XRACK location was offset with "+transform(xqty)+" units of style "+whstyle(.t.,xrack.style,xrack.color,xrack.id,xrack.pack)+crlf
	endif
	
	select (xxinvenloc)
	locate for accountid=xrack.accountid and units and style=xrack.style and color=xrack.color and id=xrack.id and pack=xrack.pack and whseloc="XRACK"
	if found()
		if &xxinvenloc..locqty=-xqty
			delete in (xxinvenloc)
		else
			replace locqty with &xxinvenloc..locqty+xqty in (xxinvenloc)
		endif
	endif
	locate for accountid=xrack.accountid and units and style=xrack.style and color=xrack.color and id=xrack.id and pack=xrack.pack and whseloc=xrack.whseloc
	if found()
		if &xxinvenloc..locqty=xqty
			delete in (xxinvenloc)
		else
			replace locqty with &xxinvenloc..locqty-xqty in (xxinvenloc)
		endif
	endif

	if usesqloutwo()	&&  don't adjust outloc just create adj records		dy 2/21/17
		m.accountid=xrack.accountid
		m.adjdt=date()
		m.offset=.t.
		m.comment="OFFSET XRACK"
		m.units=.t.
		m.style=xrack.style
		m.color=xrack.color
		m.id=xrack.id
		m.pack="1"
		m.totqty=-xqty
		m.removeqty=0
		m.remainqty=m.totqty
		m.inadj=(m.totqty>0)
		m.whseloc=xrack.whseloc
		
		insertinto("adj","wh",.t.)
		
		m.totqty=xqty
		m.remainqty=m.totqty
		m.inadj=(m.totqty>0)
		m.whseloc="XRACK"

		insertinto("adj","wh",.t.)

		xinchange=xqty

		adjremove(xinchange, xrack.accountid, xrack.units, xrack.style, xrack.color, xrack.id, xrack.pack, xrack.whseloc)

	else	&& dbf	
		select outloc.outlocid ;
			from outdet, outloc ;
			with (buffering=.t.) ;
			where outdet.outdetid=outloc.outdetid ;
			and whseloc="XRACK" ;
			and outdet.accountid=xrack.accountid ;
			and style=xrack.style and color=xrack.color and id=xrack.id and pack=xrack.pack ;
			into cursor xrackloc

		if reccount("xrackloc")>0
			scan for xqty>0
				=seek(xrackloc.outlocid,"outloc","outlocid")
				select outloc
				scatter memvar
				
				if xqty>=outloc.locqty
					replace whseloc with xrack.whseloc in outloc
					xinchange=outloc.locqty
					xqty=xqty-outloc.locqty
					=seek(xrackloc.outlocid,"outloc","outlocid")
					select outloc
					scatter memvar
				else
					replace locqty with outloc.locqty-xqty in outloc
					m.whseloc=xrack.whseloc 
					m.locqty=xqty
					m.addby=""
					m.adddt={}
					if xfrom="AUTO"
						m.outlocid=autopk()
					else
						m.outlocid=genpk("outloc","wh")
					endif
					insert into outloc from memvar
					xinchange=xqty
					xqty=0
				endif

				xinchange = indetremove(xinchange, xrack.accountid, xrack.units, xrack.style, xrack.color, xrack.id, xrack.pack, xrack.whseloc,,,.t.)

				if xinchange>0
					adjremove(xinchange, xrack.accountid, xrack.units, xrack.style, xrack.color, xrack.id, xrack.pack, xrack.whseloc)
				endif
			endscan
			
		else
			m.adjdt=date()
			m.offset=.t.
			m.comment="OFFSET XRACK"
			m.units=.t.
			m.style=xrack.style
			m.color=xrack.color
			m.id=xrack.id
			m.pack="1"
			m.totqty=-xqty
			m.removeqty=0
			m.remainqty=m.totqty
			m.inadj=(m.totqty>0)
			m.whseloc=xrack.whseloc
			
			insertinto("adj","wh",.t.)
			
			m.totqty=xqty
			m.remainqty=m.totqty
			m.inadj=(m.totqty>0)
			m.whseloc="XRACK"

			insertinto("adj","wh",.t.)

			xinchange=xqty

			adjremove(xinchange, xrack.accountid, xrack.units, xrack.style, xrack.color, xrack.id, xrack.pack, xrack.whseloc)
		endif
	endif
endscan
