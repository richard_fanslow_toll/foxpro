Public xfile,asn_origin,TranslateOption,lcScreenCaption,cfilename,ltesting
Public cCustname,cMod,lcPath,cfilename
Public nFileSize,cISA_Num,LogCommentStr

&& addin filter_x856 to remove non 856 type edi data

runack("TJXASN")
xfile = ""
Set Safety Off
Set Exclusive Off
Set Deleted On
Set enginebehavior 70
set multilocks on
Close Data All

gsystemmodule='TJX_856IN'

ltesting = .F.

lcScreenCaption = "tjx ASN Uploader Process... Processing File: "
_Screen.Caption = lcScreenCaption

Wait Window At 10,10 " Now uploading TJX 856's........." Timeout 2
TranslateOption = "CR_TILDE" &&"NONE"
asn_origin = "TJX"

Do createx856

*Set Default To "f:\jones856\asnin\"
lcPath        = "f:\FTPUSERS\TJMAX\856in\"
lcArchivePath = "f:\FTPUSERS\TJMAX\856in\archive\"
lc997Path = "f:\FTPUSERS\TJMAX\997trans\"

Wait Window At 10,10  "Processing TJX 856s............." Timeout 1

**need to have these fields for the EDILOG SQL store
cfilename =""
nAcctNum = 6565
cCustname = "TJX"
cMod = "X"
cOffice = "C"
LogCommentStr="856 upload"


Delimiter = "*"

xsqlexec("select * from dellocs",,,"wo")
index on str(accountid,4)+location tag acct_loc
set order to

Use F:\tjmax\Data\tjxdetail In 0
Select * From tjxdetail Where .F. Into Cursor asndata Readwrite

Select asndata
Scatter Memvar Memo Blank

Do ProcessTJXASN

***********************************************************************************************************

***********************************************************************************************************
Procedure ProcessTJXASN

lnNum = Adir(tarray,"f:\ftpusers\tjmax\856in\*.*")

*lnNum = Adir(tarray,"f:\ftpusers\tjmax\856in\test\*.*")
*lcpath = "f:\ftpusers\tjmax\856in\test\"

If lnNum=0
  schedupdate()
  Return
Endif

If lnNum >= 1
  For thisfile = 1  To lnNum
    xfile = lcPath+tarray[thisfile,1]  &&+"."
    archivefile = lcArchivePath+tarray[thisfile,1]  &&+"."
    to997file = lc997Path+tarray[thisfile,1]
    cfilename =tarray[thisfile,1]  &&+"."
    nFileSize = Val(Transform(tarray(thisfile,2)))
    Wait Window "Importing file: "+xfile Nowait
    Do loadedifile With xfile,"*",TranslateOption,"JONES856"

    Do ImportTJXASN
    Do StoreASNData
    Copy File [&xfile] To [&to997file]
    Copy File [&xfile] To [&archivefile]
    If File(archivefile)
      Delete File [&xfile]
    Endif
  Next thisfile
Else
  Wait Window At 10,10  "          No 856's to Import          " Timeout 1
Endif

schedupdate()

Endproc
******************************************************************************************************************

******************************************************************************************************************
Procedure ImportTJXASN

Try

  Set Century On
  Set Date To YMD

  Select x856
  Goto Top

  lnCurrentTripNumber =0
  lcHLLevel = ""
  lcVendorCode = ""
  lcCurrentHAWB = ""
  m.hdrsuppdata = ""

  POs_added = 0
  ShipmentNumber = 1
  ThisShipment = 0
  origawb = ""
  lcCurrentShipID = ""
  lcCurrentArrival = ""
  LAFreight = .F.

** here we clear out asn data already loaded from this file
  Select tjxdetail
  Set Filter To
  Set Order To filename
  Seek Alltrim(Upper(xfile))
**added to delete on reimport of file if not already loaded into the fmi system - mvw 02/13/07
  Delete While filename=Alltrim(Upper(xfile)) For Empty(wo_num)

  Select x856
  Set Filter To
  Goto Top

  Do filter_x856 With "SH"

  Select x856
  Set Filter To
  Goto Top

  Do While !Eof("x856")
    Wait Window "At the outer loop " Nowait

    If Trim(x856.segment) = "ISA"
      m.isa = Alltrim(x856.f13)
      cISA_Num = Alltrim(x856.f13)
      m.dateloaded = Datetime()
      m.filename   = Upper(xfile)
      Select x856
      Skip 1 In x856
      Loop
    Endif

    If Inlist(x856.segment,"GS","ST")
      Select x856
      Skip 1 In x856
      Loop
    Endif

*    If Trim(x856.segment="BSN" && and alltrim(x856.f1) = "05"  && must delete then add new info
    If Inlist(Trim(x856.segment),"BSN","BGN") && and alltrim(x856.f1) = "05"  && must delete then add new info
      m.shipid = Alltrim(x856.f2)
      m.hawb   = Alltrim(x856.f2)
      m.bsn    = Alltrim(x856.f2)
      Select x856
      Skip 1 In x856
      Loop
    Endif

    If At("HL",x856.segment)>0
      lcHLLevel = Alltrim(x856.f3)
    Endif

    If lcHLLevel = "S"
      Select asndata
      Scatter Memvar Memo Blank Fields Except isa,bsn,filename,hawb
      m.hdrsuppdata  = ""

      Select x856
      m.dateloaded = Datetime()
      m.filename   = Upper(xfile)
      Do While lcHLLevel = "S"
        If Trim(x856.segment) = "TD5"
          m.scac  = Upper(Alltrim(x856.f3))
          If Alltrim(x856.f4) = "S"
            m.Type = "O"
          Endif
          If Alltrim(x856.f4) = "A"
            m.Type = "A"
          Endif
          If Alltrim(x856.f4) = "M"
            m.Type = "D"
          Endif
        Endif
        If Trim(x856.segment) = "REF" And Upper(Trim(x856.f1)) = "8S"
          m.brokref = Alltrim(x856.f2)
          m.hdrsuppdata = m.hdrsuppdata+Chr(13)+"BROKREF*"+Trim(x856.f2)
        Endif

        If Trim(x856.segment) = "DTM" And Alltrim(x856.f1)= "370"
          m.saildate = Ctod(Substr(Alltrim(x856.f2),5,2)+"/"+Substr(Alltrim(x856.f2),7,2)+"/"+Substr(Alltrim(x856.f2),1,4))
          m.hdrsuppdata = m.hdrsuppdata+Chr(13)+"SAILDATE*"+Trim(x856.f2)
        Endif

        If Trim(x856.segment) = "N1" And Upper(Trim(x856.f1)) = "FF"
          lcFreightForwarder = Alltrim(x856.f2)
          lcFreightForwarderCode = Alltrim(x856.f4)
        Endif

        If Trim(x856.segment) = "V1"
          m.vessel = Upper(Alltrim(x856.f2))
          m.voyagenum = Upper(Alltrim(x856.f4))
          m.hdrsuppdata = m.hdrsuppdata+Chr(13)+"VESSEL*"+Trim(x856.f2)
          m.hdrsuppdata = m.hdrsuppdata+Chr(13)+"VOYAGENUM*"+Trim(x856.f4)
          m.hdrdata= m.hdrsuppdata
        Endif

        Select x856
        Skip 1 In x856
        If Trim(x856.segment) = "HL"
          lcHLLevel = Trim(x856.f3)
        Endif
      Enddo
    Endif   && end if HL S

    If lcHLLevel = "E"
      Do While lcHLLevel = "E"
*!*          If Trim(x856.segment) = "HL" And Alltrim(x856.f3) != "E"
*!*            lcHLLevel = Alltrim(x856.f3)
*!*            Exit
*!*          Endif
        If Trim(x856.segment) = "TD1"
          Do Case
          Case Alltrim(x856.f1) ="CTN"
            m.qty_type = "CARTONS"
          Endcase
          m.totqty   = Val(Alltrim(x856.f2))
          m.totwt    = Val(Alltrim(x856.f7))
          m.totcube  = Val(Alltrim(x856.f9))
        Endif

        If Trim(x856.segment) = "TD3"
          m.container = Trim(x856.f2) +Trim(x856.f3)
          m.equiptype =Trim(x856.f1)
          m.seal = Trim(x856.f9)
** port of loading
        Endif

        If Trim(x856.segment) = "TD5" And Upper(Trim(x856.f7)) = "PA"
** port of loading
        Endif

        If Trim(x856.segment) = "TD5" And Upper(Trim(x856.f7)) = "PE"
** port of entry
        Endif

        If Trim(x856.segment) = "DTM" And Alltrim(x856.f1)= "371"
          m.arrivedate =  Ctod(Substr(Alltrim(x856.f2),5,2)+"/"+Substr(Alltrim(x856.f2),7,2)+"/"+Substr(Alltrim(x856.f2),1,4))
        Endif

        If Trim(x856.segment) = "N1" And Alltrim(x856.f1)= "DS"
          m.div =  Alltrim(x856.f4)
        Endif

        Select x856
        Skip 1 In x856
        If Trim(x856.segment) = "HL"
          lcHLLevel = Trim(x856.f3)
        Endif
      Enddo
    Endif

    If lcHLLevel = "O"
      Do While lcHLLevel = "O"
        If Trim(x856.segment) = "PRF"
          m.po_num =  Alltrim(x856.f1)
        Endif
        If Trim(x856.segment) = "TD1" And Alltrim(x856.f1) = "CTN"
          m.pl_qty =  Val(Alltrim(x856.f2))
          m.cbm    =  Val(Alltrim(x856.f9))
        Endif
        If Trim(x856.segment) = "TD1" And Alltrim(x856.f1) = "PLT"
          m.suppdata = m.suppdata +Chr(13)+"PLTCOUNT*"+Alltrim(x856.f2)
        Endif
        If Trim(x856.segment) = "REF" And Alltrim(x856.f1) = "BL"
          m.suppdata = m.suppdata+Chr(13)+"BOL*"+Trim(x856.f2)
        Endif
        If Trim(x856.segment) = "REF" And Alltrim(x856.f1) = "19"
          m.ChainIndicator =  Alltrim(x856.f2)
          m.whse_loc = Alltrim(x856.f2)
          Do Case
          Case Alltrim(x856.f2)="08"
            m.label = "TJMAXX/NEWTON BUYING CORP"
          Case Alltrim(x856.f2)="10"
            m.label = "MARSHALLS"
          Case Alltrim(x856.f2)="19"
            m.label = "AJ WRIGHT"
          Case Alltrim(x856.f2)="28"
            m.label = "HOME GOODS"
          Case Alltrim(x856.f2)="04"
            m.label = "WINNERS"
          Case Alltrim(x856.f2)="05"
            m.label = "TKMAXX"
          Case Alltrim(x856.f2)="24"
            m.label = "HOMESENSE"
          Otherwise
            m.label = "UNKNOWN"
          Endcase
          m.suppdata = m.suppdata +Chr(13)+"CHAIN*"+Trim(x856.f2)
        Endif
        If Trim(x856.segment) = "REF" And Alltrim(x856.f1) = "DP"
          m.dept =  Alltrim(x856.f2)
        Endif
        If Trim(x856.segment) = "REF" And Alltrim(x856.f3) = "ZZ" And Alltrim(x856.f4) = "Y"
          m.loose =  Alltrim(x856.f1)
        Endif
        If Trim(x856.segment) = "REF" And Alltrim(x856.f3) = "ZU"  And Alltrim(x856.f4) = "Y"
          m.pocrossover =  .T.
          m.suppdata = m.suppdata +Chr(13)+"POCROSSOVER*"+"Y"
        Endif
        If Trim(x856.segment) = "N1" And Alltrim(x856.f1) = "VN"
          m.vendor =  Alltrim(x856.f2)
          m.vendornum =  Alltrim(x856.f4)
          m.suppdata = m.suppdata +Chr(13)+"VENDOR*"+Alltrim(x856.f2)
          m.suppdata = m.suppdata +Chr(13)+"VENDORNUM*"+Alltrim(x856.f4)
        Endif
        If Trim(x856.segment) = "N3" And Alltrim(x856.f3) = "?"
        Endif
        If Trim(x856.segment) = "N3" And Alltrim(x856.f3) = "IND"
        Endif
        If Trim(x856.segment) = "N4"
*       Set Step On
        Endif
        If Trim(x856.segment) = "N1" And Alltrim(x856.f1) = "ST"
*          Set Step On
          m.div =  Right(Alltrim(x856.f4),3)
          If m.div ="884"
*          Set Step On
          Endif
          Select dellocs
          Locate For accountid = 6565 And div = m.div

          If Found()
            m.label = dellocs.Label
            m.style = dellocs.city
            m.delloc= Alltrim(dellocs.location)
          Else
            m.label = "UNK"
            m.style = "UNK"
            m.delloc= "UNK"
          Endif

          Select asndata
          m.acctname ="TJX COMPANIES INC."
          m.accountid = 6565
          m.office = "C"
          Append Blank
          m.suppdata = m.hdrsuppdata+Chr(13)+m.suppdata
          m.suppdata = m.suppdata+Chr(13)+"SEAL*"+Trim(m.seal)
          m.suppdata = m.suppdata+Chr(13)+"EQUIPTYPE*"+Trim(m.equiptype)
          Gather Memvar Memo
          m.suppdata  = ""
        Endif
        If Trim(x856.segment) = "HL"
          lcHLLevel = Trim(x856.f3)
        Endif

        Select x856
        If !Eof("x856")
          Skip 1 In x856
        Else
          Exit
        Endif
      Enddo
    Endif

    If Eof()
      Exit
    Endif
  Enddo &&!eof("x856")

  Set Step On

    Select asndata
    Goto Top
    Scan
      Select asndata
      Scatter Memvar Memo
      Release m.detailid
      m.detailid = Reccount("tjxdetail")
      m.dateloaded = Datetime()
      Insert Into tjxdetail From Memvar
    Endscan

Catch To oErr
  tsubject = "URGENT: TJX ASN Uplaod Error ("+Transform(oErr.ErrorNo)+") at "+Ttoc(Datetime())
  tmessage = "URGENT: This must be fixed and the file must be reimported or it could cause duplicate data"+Chr(13)+Chr(13)+"TJX ASN Upload Error processing"

  tmessage =tmessage+ "Try/CATCH Exeception Message:"+Chr(13)+;
  [  Error: ] + Str(oErr.ErrorNo) +Chr(13)+;
  [  LineNo: ] + Str(oErr.Lineno) +Chr(13)+;
  [  Message: ] + oErr.Message +Chr(13)+;
  [  Procedure: ] + oErr.Procedure +Chr(13)+;
  [  Details: ] + oErr.Details +Chr(13)+;
  [  StackLevel: ] + Str(oErr.StackLevel) +Chr(13)+;
  [  LineContents: ] + oErr.LineContents+Chr(13)+;
  [  UserValue: ] + oErr.UserValue+Chr(13)+;
  [  ASN Filename: ]+xfile

  tattach  = ""
  tcc=""
  tsendto  = "pgaidis@fmiint.com"
  tFrom    ="TGF Corporate Communication Department <tgf-transload-ops@tollgroup.com>"
  Do Form dartmail2 With tsendto,tFrom,tsubject,tcc,tattach,tmessage,"A"
Endtry

Endproc


**********************************************************************************************************************
Procedure StoreASNData
**********************************************************************************************************************
lcQuery = [select * from ediinlog where accountid = ]+Transform(nAcctNum)+[ and isanum = ']+cISA_Num+[']
xsqlexec(lcQuery,"p1",,"stuff")
If Reccount()=0
  Use In p1
  useca("ediinlog","stuff")  && Creates updatable PTHIST cursor

*      LogCommentStr = "Pick tickets uploaded"
  Select ediinlog
  Scatter Memvar Memo
  m.ediinlogid = dygenpk("ediinlog","WHALL")
  m.acct_name  = cCustname
  m.accountid  = nAcctNum
  m.mod        = cMod
  m.office     = cOffice
  m.filepath   = lcPath
  m.filename   = cfilename
  m.size       = nFileSize
  m.FTIME      = Date()
  m.FDATETIME  = Datetime() &&tarray(thisfile,4)
  m.Type       = "856"
  m.qty        = 0
  m.isanum     =  cISA_Num
  m.uploadtime = Datetime()
  m.comments   = LogCommentStr
  m.edidata    = ""
  Insert Into ediinlog From Memvar
  Append Memo edidata From &xfile
  tu("ediinlog")
Endif
**********************************************************************************************************************


**************************************************************************************
