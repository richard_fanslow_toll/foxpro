*!* BBC 943_CREATE.PRG
Parameters  nAcctNum

Public lXfer943,cFilename,tsendto,tcc,tsendtoerr,tccerr,tsendtotest,tcctest,lIsError,nOrigAcctNum,nDivLoops,cInFolder,cWhse,nRecLU,lTesting
Public lDoMail,tfrom,tattach,cxErrMsg,NormalExit,Acctname,lPick,lBrowFiles,cTransfer,cArchivePath,lcPath,lcContainer,cAcct_ref,cMod,cWhseMod
Public lDoSQL,gMasterOffice,goffice,cOffice,cBrokerref
Public Array a856(1)

Close Databases All
Clear
_Screen.Caption = "BBC 943 UPLOAD"
cOffice = "L"

lTesting = .f. && If TRUE, disables certain functions
lOverrideBusy = lTesting
lOverrideBusy = .f.

Do m:\dev\prg\_setvars With lTesting
*ON ERROR THROW

Try
  Do m:\dev\prg\lookups
  tfrom = "Toll/FMI EDI Operations <toll-edi-ops@tollgroup.com>"
  tattach = " "
  cOffice = "L"
  cMod = "L"
  goffice = cMod
  gMasterOffice = cOffice
  cWhseMod = Lower("wh"+cMod)

  lDoSQL = .T.

  lTestmail = lTesting
  lBrowFiles = lTesting
  lIsError = .F.
  lDoMail = .T.  && If .t., sends mail
  NormalExit = .F.
  lPick = .F.
  nDivLoops = 0
  nRecLU = 0
  cMasterComments = ""
  lcContainer = ""
  lcReference = ""

  nAcctNum = Iif(Vartype(nAcctNum) # "N",6757,nAcctNum)  && bbc rw
  cxErrMsg = ""
  cCustName = "BBC RW"  && Customer Identifier (for folders, etc.)
  cMailName = "RW"  && Proper name for eMail, etc.

  cTransfer ="943BBCRW"
  Select 0
  Use F:\edirouting\ftpsetup
  Locate For ftpsetup.transfer = cTransfer
  If ftpsetup.chkbusy And !lTesting And !lOverrideBusy
    Wait Window "Process is busy...exiting" Timeout 2
    Close Data All
    NormalExit = .T.
    Throw
  Endif
  Replace chkbusy With .T.,trig_time With Datetime()  For transfer = cTransfer
  Use In ftpsetup

  setuprcvmail(cOffice)

  If !lTesting
    xpath = "f:\ftpusers\bbcrw\943in\"
    cArchivePath = "f:\ftpusers\bbcrw\943in\archive\"
  Else
    xpath = "f:\ftpusers\bbcrw\943intest\"
    cArchivePath = "f:\ftpusers\bbcrw\943intest\archive\"
  Endif

  Delete File (xpath+"fox*.*")
  len1 = Adir(ary1,xpath+"*")
  If len1 = 0
    Wait Window "There are no 943 files to process...exiting" Timeout 2
    Close Databases All
    NormalExit = .T.
    Throw
  Endif
  Wait Window "There are "+Transform(len1)+" 943 files to process" Nowait &&TIMEOUT 2


  For kk = 1 To len1
    cContainer = ""
    cAcct_ref = ""
    cFilename = Alltrim(ary1[kk,1])
    cArchiveFile  = (cArchivePath+cFilename)
    dFiledate = ary1[kk,3]
    xfile = xpath+cFilename
    Copy File [&xfile] To [&cArchivefile]

    xstr1 = Filetostr(xfile)  && Added to trap empty files
    If Empty(Alltrim(xstr1))
      Release xstr1
      Delete File [&xfile]
      Loop
    Endif

    Do m:\dev\prg\createx856a

    loadedifile(xfile,"*","PIPE3","BBC")
    Select x856
    Locate
    Locate For x856.segment = "GS" And x856.f1 = "AR"
    If !Found()
      Set Step On
      cxErrMsg = cFilename+" is NOT a 943 document"
      errormail()
    Endif

    Select x856
    Set Deleted Off
    Go Bott
    Recall
    Set Deleted On
    Select x856
    Go Top
    If lTesting
      cUseFolder = "F:\WHP\WHDATA\"
    Else
      xReturn = "XXX"
      Do m:\dev\prg\wf_alt With cOffice,nAcctNum
      If xReturn = "XXX"
        Set Step On
        cxErrMsg = "NO WHSE FOUND IN WMS"
        errormail()
        Return
      Endif
      cUseFolder = Upper(xReturn)
    Endif

    useca("inwolog","wh")
    xsqlexec("select * from inwolog where .f.","xinwolog",,"wh")

    useca("pl","wh")
    xsqlexec("select * from pl where .f.","xpl",,"wh")

    Store 0 To xinwologid,xplid,xnpo

    xsqlexec("select * from account where inactive=0","account",,"qq")
    Index On accountid Tag accountid
    Set Order To
    =Seek(nAcctNum,'account','accountid')
    m.Acctname = account.Acctname
    m.accountid = account.accountid

    useca("inrcv","wh")

    Select x856
    Do While !Eof("x856")
      Do Case
      Case Inlist(x856.segment,"GS","ISA")
        m.container =""
        Skip 1 In x856
        Loop

      Case x856.segment = "ST"
        Skip 1 In x856
        Loop

      Case x856.segment = "W06"
        Skip 2 In x856
        If x856.segment != "W27"
          errmgs = "NO W27 segment, invalid 943 format.........."
          Throw
        Endif
        lcContainer =  Alltrim(x856.f5)
        Select xinwolog
        Locate For Container = lcContainer
        If !Found()
          Select xinwolog
          Scatter Memvar Blank
          m.accountid = nAcctNum
          Store "FILE943*"+cFilename To m.comments
          Store m.comments+Chr(13)+"FILETIME*"+Ttoc(Datetime()) To m.comments
          Store "" To m.echo,m.po
          Store "FMI-PROC" To m.addby
          Store Datetime() To m.adddt
          Store "BBC943" To m.addproc
          Store Date() To m.wo_date,m.date_rcvd
          m.container = lcContainer
          xinwologid = xinwologid+1
          m.inwologid = xinwologid
          Insert Into xinwolog From Memvar

          m.container =""
          llReturn = .F.
          Select x856
          Skip -2 In x856
          Replace acct_ref  With Allt(x856.f4) In xinwolog
          Replace Reference With Allt(x856.f5) In xinwolog
          Store Allt(x856.f4)  To cAcct_ref
          Skip 1 In x856
          Loop
        Else
          Loop
&& not a new container so point to the current one
        Endif


&& OK here we need to skip ahead to get the container number
&& added here by PG 01/10/2018

*!*            SELECT xinwolog
*!*            LOCATE FOR inwolog.CONTAINER = m.container
*!*            IF FOUND()
*!*            ELSE
*!*              m.inwologid = m.inwologid + 1
*!*              INSERT INTO xinwolog FROM MEMVAR
*!*  *          close data all
*!*            ENDIF
*!*            SELECT x856

      Case x856.segment = "N9" And x856.f1 = "LI"
        cBrokerref = ""
        llReturn = .T.
        Replace comments With "FILE943*"+cFilename In xinwolog
        Replace comments With comments+Chr(13)+"FILETIME*"+Ttoc(Datetime()) In xinwolog

      Case x856.segment = "W27"
        If !Allt(x856.f5)$xinwolog.comments
          Replace comments With "CONTAINER*"+Alltrim(x856.f5)+Chr(13)+m.comments In xinwolog
        Endif

      Case x856.segment = "W04"
        lcEachType = Alltrim(x856.f2)
        m.plid = xplid
        xplid = xplid +1
        m.totqty = Int(Val(x856.f1))
        Replace xinwolog.plinqty With xinwolog.plinqty+m.totqty In xinwolog
        m.units = Iif(x856.f2="CT",.F.,.T.)
        m.echo = "UPC*"+Allt(x856.f3)
        m.style = Iif(x856.f4 = "ST",Allt(x856.f5),"")

        Skip 1 In x856
        If Alltrim(x856.segment) = "N9" And Alltrim(x856.f1) = "LN"
          m.linenum =  Allt(x856.f2)
          m.echo = m.echo+Chr(13)+"LINENUM*"+Alltrim(m.linenum)
        Endif

        Skip 1 In x856
        If Alltrim(x856.segment) = "N9" And Alltrim(x856.f1) = "CL"
          m.color =  Allt(x856.f2)
        Endif

        Skip 1 In x856
        If Alltrim(x856.segment) = "N9" And Alltrim(x856.f1) = "CD"
          m.echo = m.echo+Chr(13)+"DESC*"+Alltrim(x856.f2)
        Endif

        Skip 1 In x856
        If Alltrim(x856.segment) = "N9" And Alltrim(x856.f1) = "SZ"
          m.id =  Allt(x856.f2)
        Endif

        Skip 1 In x856
        If Alltrim(x856.segment) = "N9" And Alltrim(x856.f1) = "BR"
          m.echo = m.echo+Chr(13)+"PO*"+Alltrim(x856.f2)
          m.cayset = Alltrim(x856.f2)
        Endif

        Skip 1 In x856
        If Alltrim(x856.segment) = "W20"
          m.pack= Strtran(Alltrim(x856.f1),"0","")
          lnPackval= Int(Val(x856.f1))
          If lnPackval < 1
            errmsg = "W20-01 is zero/no value"
            Throw
          Endif
        Endif

        Do Case
        Case m.totqty%lnPackval = 0 And m.totqty >= lnPackval And lcEachType = "EA"
          m.po = Transform(xnpo)
          m.totqty = m.totqty /lnPackval
          m.units = .F.
          m.accountid = nAcctNum
          m.plid = xplid
          Insert Into xpl From Memvar
          Replace xpl.inwologid With xinwolog.inwologid
*              REPLACE xpl.ECHO WITH xpl.ECHO+CHR(13)+ALLTRIM(m.ponum)+"*"+TRANSFORM(m.totqty) IN xpl

          m.units = .T.
          m.pack="1"
          m.totqty = m.totqty*lnPackval
          m.plid = xplid
          xplid = xplid +1
          m.accountid = nAcctNum
          Insert Into xpl From Memvar
          Replace xpl.inwologid With xinwolog.inwologid

        Case m.totqty<lnPackval  And lcEachType = "EA"
          lnThisQty = m.totqty
          m.po = Transform(xnpo)
          m.pack= Transform(m.totqty)  && was m.totqty
          m.totqty = 1
          m.units = .F.
          Insert Into xpl From Memvar
          Replace xpl.inwologid With xinwolog.inwologid
*          REPLACE xpl.ECHO WITH xpl.ECHO+CHR(13)+ALLTRIM(m.ponum)+"*"+TRANSFORM(m.totqty) IN xpl
          m.units = .T.
          m.pack="1"
          m.totqty = lnThisQty
          m.plid = xplid
          xplid = xplid +1
          Insert Into xpl From Memvar
          Replace xpl.inwologid With xinwolog.inwologid

        Case m.totqty>lnPackval And m.totqty%lnPackval != 0   And lcEachType = "EA" && at least one carton with a residual carton
          m.po = Transform(Val(m.po)+1)
          orig_eaches = m.totqty
          m.totqty = Int(m.totqty /lnPackval)
          lnleftover = orig_eaches-(m.totqty*lnPackval)

          m.units = .F.
          Insert Into xpl From Memvar  && #1, CTN
          Replace xpl.inwologid With xinwolog.inwologid
*    REPLACE xpl.ECHO WITH xpl.ECHO+CHR(13)+ALLTRIM(m.ponum)+"*"+TRANSFORM(m.totqty) IN xpl
          m.units = .T.
          m.pack="1"
          m.totqty = m.totqty*lnPackval
          m.plid = xplid
          xplid = xplid +1

          Insert Into xpl From Memvar   && #1, UNITS
          Replace xpl.inwologid With xinwolog.inwologid
*      REPLACE xpl.ECHO WITH xpl.ECHO+CHR(13)+ALLTRIM(m.ponum)+"*"+TRANSFORM(m.totqty) IN xpl

          m.pack = Alltrim(Transform(lnleftover))
          m.totqty = 1
          m.plid = m.plid + 1
          m.units = .F.
          m.po = Transform(Val(m.po)+1)

          Insert Into xpl From Memvar  && #2, CTN, RESIDUAL
          Replace xpl.inwologid With xinwolog.inwologid

*        REPLACE xpl.ECHO WITH xpl.ECHO+CHR(13)+ALLTRIM(m.ponum)+"*"+TRANSFORM(m.totqty) IN xpl
          m.units = .T.
          m.pack="1"
          m.totqty = lnleftover
          m.plid = xplid
          xplid = xplid +1
          Insert Into xpl From Memvar     && #2, UNITS, RESIDUAL
          Replace xpl.inwologid With xinwolog.inwologid
*          REPLACE xpl.ECHO WITH xpl.ECHO+CHR(13)+ALLTRIM(m.ponum)+"*"+TRANSFORM(m.totqty) IN xpl

        Case lcEachType = "CT"
          m.po = Transform(Val(m.po)+1)
          m.totqty = m.totqty && the qty is the actual carton qty,
          m.units = .F.
          Insert Into xpl From Memvar
          Replace xpl.inwologid With xinwolog.inwologid
*            REPLACE xpl.ECHO WITH xpl.ECHO+CHR(13)+ALLTRIM(m.ponum)+"*"+TRANSFORM(m.totqty) IN xpl
          m.units = .T.
          m.pack="1"
          m.totqty = m.totqty*lnPackval
          m.plid = xplid
          xplid = xplid +1
          Insert Into xpl From Memvar
          If xpl.inwologid = 0
            Replace xpl.inwologid With 1 In xpl
          Endif
        Endcase

      Case x856.segment = "IEA"
        If lTesting
          Select xpl
        Endif

        Select xinwolog
        Scan
          Select xpl
          Sum totqty To lnctnsin For xpl.inwologid = xinwolog.inwologid And units= .F.
          Replace plinqty With lnctnsin In xinwolog

          Sum totqty To lnunitsin For xpl.inwologid = xinwolog.inwologid And units= .T.
          Replace plunitsinqty With lnunitsin In xinwolog

        Endscan

        xplrollup()
        inwolog_enter()
        Select xinwolog
        Zap
        Select xpl
        zap

      Endcase
      If !Eof('x856')
        Skip 1 In x856
      Else
        Set Step On
        Exit
      Endif
    Enddo
  DELETE FILE [&xfile]
  Endfor

  Wait Window "All BBC 943s processed...exiting" Timeout 2

  closeftpsetup()

  NormalExit = .T.

Catch To oErr
  If NormalExit = .F.
    Assert .F. Message "AT CATCH SECTION"
    Set Step On
    tsubject = cCustName+" 943 Upload Error ("+Transform(oErr.ErrorNo)+") at "+Ttoc(Datetime())
    tattach  = ""
    tsendto  = tsendtoerr
    tcc      = tccerr
    tmessage = cCustName+" 943 Upload Error"
    lcSourceMachine = Sys(0)
    lcSourceProgram = Sys(16)

    If Empty(cxErrMsg)
      tmessage =tmessage+ "Try/CATCH Exeception Message:"+Chr(13)+;
        [  Error: ] + Str(oErr.ErrorNo) +Chr(13)+;
        [  Procedure: ] + oErr.Procedure +Chr(13)+;
        [  LineNo: ] + Str(oErr.Lineno) +Chr(13)+;
        [  Message: ] + oErr.Message +Chr(13)+;
        [  Details: ] + oErr.Details +Chr(13)+;
        [  StackLevel: ] + Str(oErr.StackLevel) +Chr(13)+;
        [  LineContents: ] + oErr.LineContents+Chr(13)+;
        [  UserValue: ] + oErr.UserValue+Chr(13)+;
        [  Computer:  ] +lcSourceMachine+Chr(13)+;
        [  943 file:  ] +cFilename+Chr(13)+;
        [  Program:   ] +lcSourceProgram
    Else
      tmessage = tmessage + Chr(13) + Chr(13) + cxErrMsg
    Endif
    tattach  = ""
    tfrom    ="FMI EDI Processing Center <fmi-transload-ops@fmiint.com>"
    Do Form m:\dev\FRM\dartmail2 With tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
  Endif
Finally
  Set Status Bar On
  Close Databases All
  On Error
Endtry

*********************************************************************************************************************
****************************
Procedure goodmail
****************************
tsubject = cMailName+" EDI FILE (943) Processed, Inv. WO"+cWO_Num
tmessage = Replicate("-",90)+Chr(13)
tmessage = tmessage+"Packinglist upload run from machine: "+Sys(0)+Chr(13)
tmessage = tmessage+Replicate("-",90)+Chr(13)
tmessage = tmessage+"Inbound Workorders created for "+Alltrim(m.Acctname)+", Loc: Mira Loma"+Chr(13)++Chr(13)
tmessage = tmessage+"Container: "+lcContainer+Chr(13)+Chr(13)
tmessage = tmessage+"Data from File : "+cFilename+Chr(13)
tmessage = tmessage+Replicate("-",90)+Chr(13)
tmessage = tmessage+"Acct. Ref: "+cAcct_ref+",  Brok. Ref: "+cBrokerref+", WO# "+cWO_Num+Space(2)+"CTNS: "+cPLCtns+Space(2)+"UNITS: "+cPLUnits

tsendto = tsendto+",paul.gaidis@tollgroup.com"

If lDoMail
  Do Form m:\dev\frm\dartmail2 With tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
Endif
Endproc
*********************************************************************************************************************
Procedure dupemail
****************************
Parameters cWO
tsubject = cMailName+" EDI FILE (943): Duplicate"
tmessage = Replicate("-",90)+Chr(13)
tmessage = tmessage+"Data from File : "+cFilename+Chr(13)
tmessage = tmessage+"For Account ID: "+Transform(nAcctNum)
tmessage = tmessage+ ", Container "+cContainer
tmessage = tmessage+ ", Acct. Reference "+cAcct_ref+Chr(13)
tmessage = tmessage+"already exists in the Inbound WO table, in Work Order "+cWO
tmessage = tmessage+Chr(13)+Replicate("-",90)+Chr(13)

tsendto= tsendtoerr
tcc = tccerr

tsendto = "pgaidis@fmiint.com"

If lDoMail
  Do Form m:\dev\frm\dartmail2 With tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
Endif
Endproc

*********************************************************************************************************************
Procedure errormail
****************************
Set Step On
tsubject = cMailName+" EDI ERROR (943)"
tsendto = tsendtoerr
tcc = tccerr
tmessage = "File: "+cFilename
tmessage = tmessage+Chr(13)+cxErrMsg

Do Form m:\dev\frm\dartmail2 With tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
NormalExit = .T.
Throw
Endproc

****************************
Procedure setuprcvmail
****************************
Parameters cOffice
*  ASSERT .f. MESSAGE "In Mail/Folder selection...debug"
Select 0
Use F:\3pl\Data\mailmaster Alias mm
Locate For mm.edi_type = "943" And accountid = 6757
If !Found()
  Set Step On
  cxErrMsg = "Office/Loc/Acct not in Mailmaster!"
  errormail()
Endif
Store Trim(mm.basepath) To lcPath
Store Trim(mm.archpath) To cArchivePath
lUseAlt = mm.use_alt
Store Iif(lUseAlt,Trim(mm.sendtoalt),Trim(mm.sendto)) To tsendto
Store Iif(lUseAlt,Trim(mm.ccalt),Trim(mm.cc)) To tcc
Locate
Locate For mm.edi_type = "MISC" And mm.taskname = "GENERAL"
lUseAlt = mm.use_alt
Store Iif(lUseAlt,Trim(mm.sendtoalt),Trim(mm.sendto)) To tsendtotest
Store Iif(lUseAlt,Trim(mm.ccalt),Trim(mm.cc)) To tcctest
Store Iif(lUseAlt,Trim(mm.sendtoalt),Trim(mm.sendto)) To tsendtoerr
Store Iif(lUseAlt,Trim(mm.ccalt),Trim(mm.cc)) To tccerr
If lTesting Or lTestmail
  tsendto = tsendtotest
  tcc = tcctest
Endif
Use In mm
Endproc

***************************************
Procedure closeftpsetup
***************************************
Select 0
Use F:\edirouting\ftpsetup
Replace chkbusy With .F. For transfer = cTransfer
Use In ftpsetup
Endproc

***************************************
Procedure assignofficelocs
***************************************
cOffice=Iif(Inlist(cOffice,"1","2","7"),"C",cOffice)
cWhseLoc = Icase(cOffice = "M","FL",cOffice = "I","NJ",cOffice = "X","CR","CA")
cWhse = Icase(cOffice = "M","MIAMI",cOffice = "I","CARTERET",cOffice = "X","CARSON","SAN PEDRO")
Endproc

***************************************
Procedure xplrollup  && Added 02.20.2014, Joe, per Chris M.
***************************************
Select xpl
*Select inwologid,Style,Count(Style) As cnt1 From xpl Group By 1,2 Into Cursor tempxpl

Select inwologid,Style,Color,Id,Pack, Count(1) As cnt1 From xpl Where !units Group By inwologid,Style,Color,Id,Pack Into Cursor tempxpl
Select tempxpl
Locate
If lTesting
  Browse For cnt1>1
Endif
Locate

Scan For tempxpl.cnt1>1
  cStyle1 = Alltrim(tempxpl.Style)
  Select xpl
  Locate For xpl.inwologid = tempxpl.inwologid And xpl.Style = tempxpl.Style And xpl.Color = tempxpl.Color And xpl.Id = tempxpl.Id And xpl.Pack = tempxpl.Pack
  nrec1 = Recno()
  Replace xpl.Echo With xpl.Echo+Chr(13)+"QTY*"+Allt(Str(xpl.totqty)) In xpl

  Scan For !Deleted() And  xpl.Style = tempxpl.Style And xpl.Color = tempxpl.Color And xpl.Id = tempxpl.Id And xpl.Pack = tempxpl.Pack And Recno()>nrec1
    nrec2 = Recno()
    Scatter Fields Echo Memo Memvar
    nTotqty = xpl.totqty
    Go nrec1
    If !"MULTILINES"$xpl.Echo
      Replace xpl.Echo With "MULTILINES"+Chr(13)+xpl.Echo In xpl
    Endif
    Replace xpl.Echo With xpl.Echo+Chr(13)+m.echo In xpl
    Replace xpl.Echo With xpl.Echo+Chr(13)+"QTY*"+Allt(Str(nTotqty)) In xpl
    Replace xpl.totqty With xpl.totqty+nTotqty In xpl
    lnPack = Val(xpl.Pack )
    lnStyleTotal = xpl.totqty
    Skip 1 In xpl
    Replace xpl.totqty With lnStyleTotal*lnPack  In xpl
    Go nrec2
    Delete Next 2 In xpl
  Endscan

  Store "" To m.echo
Endscan
Endproc

***************************************************************************************************************
Procedure inwolog_enter
***************************************************************************************************************
Select xinwolog
Locate
lExitIE = .F.
Scan
  xAcct_Num =  xinwolog.accountid
  xAcct_ref = xinwolog.acct_ref
  xContainer = xinwolog.Container
  xBrokref = xinwolog.brokerref
  Wait Window "AT XINWOLOG RECORD "+Alltrim(Str(Recno())) Nowait Noclear
  csq1 = [select * from inwolog where accountid = ]+Alltrim(Str(xAcct_Num))+[ and container = ']+xContainer+[']+[ and acct_ref = ']+xAcct_ref+[']+[ and brokerref = ']+xBrokref+[']
  xsqlexec(csq1,"i1",,"wh")

  If Reccount("i1")>0
    cWO = Transform(i1.wo_num)
    If Datetime()>Datetime(2017,09,14,13,15,00)
      dupemail(cWO)
    Endif
    ninwologid = xinwolog.inwologid
    Delete For xinwolog.inwologid = ninwologid
    Select xpl
    Delete For xpl.inwologid = ninwologid
    Select xinwolog
    Wait Window "This set of values already exists in the INWOLOG table...looping" Nowait
    lExitIE = .T.
    Loop
  Endif
Endscan
If lExitIE
  Return
Endif

nUploadCount = 0

Select xinwolog
Locate
Wait Window "Currently in "+Upper(Iif(lTesting,"test","production"))+" mode, base folder: "+cUseFolder Timeout 2

Scan  For !Deleted() && Scanning xinwolog here
  Scatter Memvar Memo
  cPLCtns = Alltrim(Str(m.plinqty))
  cPLUnits = Alltrim(Str(m.plunitsinqty))
  nWO_num  = dygenpk("wonum","whl")
  cWO_Num = Alltrim(Str(nWO_num))
  m.wo_num = nWO_num
  Set Order To
  m.mod = cMod
  m.office = cOffice
  m.Acctname = "RW FOOTWEAR"
  insertinto("inwolog","wh",.T.)
  nUploadCount = nUploadCount+1

  Select xpl
  Scan For inwologid=xinwolog.inwologid
    Scatter Fields Except inwologid Memvar Memo
    m.wo_num = nWO_num
    m.inwologid = inwolog.inwologid
    m.mod = cMod
    m.office = cOffice
    insertinto("pl","wh",.T.)
  Endscan

*!*    If lTesting
*!*      Select inwolog
*!*      Locate
*!*      Browse
*!*      Select pl
*!*      Locate
*!*      Browse
*!*      Set Step On
*!*    Endif

  tu("inwolog")
  tu("pl")

  goodmail()
Endscan



#If 0
  cWO_Num = Transform(nWO_num)
  Select xinwolog
  cPLCtns = Transform(xinwolog.plinqty)
  cPLUnits = Transform(xinwolog.plunitsinqty)

  Select inwolog

  If !Empty(inwolog.Container)
    Select pl
    Count To xnumskus For !units And wo_num=inwolog.wo_num

    xsqlexec("select * from inrcv where container='"+inwolog.Container+"' and status#'CONFIRMED'","xinrcv",,"wh")
    Select xinrcv
    If Reccount() > 0
      xsqlexec("update inrcv set inwonum="+Transform(inwolog.wo_num)+" where container='"+inwolog.Container+"' and status#'CONFIRMED'",,,"wh")
    Else
      Select inrcv
      Scatter Memvar Memo Blank
      m.mod = cMod
      m.office = cOffice
      m.inrcvid=dygenpk("inrcv","whall")  && new get the ID value
      m.inwonum=inwolog.wo_num
      m.accountid=inwolog.accountid
      m.Acctname=inwolog.Acctname
      m.container=inwolog.Container
      m.status="NOT AVAILABLE"
      m.skus=xnumskus
      m.cartons=inwolog.plinqty
      m.newdt=Date()
      m.addfrom="IN"
      m.confirmdt=empty2nul(m.confirmdt)
      m.newdt=empty2nul(m.newdt)
      m.dydt=empty2nul(m.dydt)
      m.firstavaildt=empty2nul(m.firstavaildt)
      m.apptdt=empty2nul(m.apptdt)
      m.pickedup=empty2nul(m.pickedup)

      Insert Into inrcv From Memvar
*      tu("inrcv")
    Endif
  Endif
#Endif

Endproc
