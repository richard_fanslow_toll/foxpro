* create headcount by div/dept/jobclass report for Ken -- for last 6 months or so.

* Build EXE as F:\UTIL\ADPREPORTS\kenbkdn????.exe

LOCAL loKenBkdnReportTrending
loKenBkdnReportTrending = CREATEOBJECT('KenBkdnReportTrending')
loKenBkdnReportTrending.MAIN()
CLOSE DATABASES ALL
RETURN

#DEFINE LOGIT 1
#DEFINE WAITIT 2
#DEFINE NOWAITIT 4
#DEFINE SENDIT 8
#DEFINE CRLF CHR(13) + CHR(10)
#DEFINE RETURN_DATA_MANDATORY .F.
#DEFINE RETURN_DATA_NOT_MANDATORY .T.
#DEFINE CSVFORMAT 6
#DEFINE WRITE_ONLY_UNBUFFERED 11
#DEFINE COMMA ","

DEFINE CLASS KenBkdnReportTrending AS CUSTOM

	cProcessName = 'KenBkdnReportTrending'

	lTestMode = .T.  && .F. FOR NORMAL OPERATION, .T. TO SUPPRESS ALL BUT DEVELOPER EMAILS AND CHANGE INPUT TABLE, ETC.

	dToday = DATE()

	lAutoYield = .T.

	cStartTime = TTOC(DATETIME())

	* report properties
	lShowSalaries = .T.

	* connection properties
	nSQLHandle = 0

	* wait window properties
	nWaitWindowTimeout = 2
	lWaitWindowIsOn = .T.

	* logfile properties
	lLoggingIsOn = .T.
	cLogFile = 'F:\UTIL\ADPREPORTS\LOGFILES\KENBKDN_REPORT_log.txt'

	* INTERNAL email properties
	lSendInternalEmailIsOn = .T.
	cFrom = 'TGF Corporate <fmicorporate@fmiint.com>'
	cSendTo = 'Ken Kausner <kkausner@fmiint.com>, Lucille Waldrip <lwaldrip@fmiint.com>, Stephanie Kochanski <skochanski@fmiint.com>, Rebecca Selawsky <rselawsky@fmiint.com>'
	cCC = 'Mark Bennett <mbennett@fmiint.com>'
	cSubject = 'Employee Breakdown Report for ' + DTOC(DATE())
	cAttach = ''
	cBodyText = ''

	cCOMPUTERNAME = ''
	cUSERNAME = ''


	FUNCTION INIT
		IF NOT DODEFAULT()
			RETURN .F.
		ENDIF
		WITH THIS
			*SET RESOURCE OFF
			CLOSE DATA
			SET CENTURY ON
			SET DATE AMERICAN
			SET HOURS TO 24
			SET ANSI ON
			SET TALK OFF
			SET DELETED ON
			SET CONSOLE OFF
			SET EXCLUSIVE OFF
			SET SAFETY OFF
			SET EXACT OFF
			SET STATUS BAR ON
			SET SYSMENU OFF
			SET ENGINEBEHAVIOR 70
			_VFP.AUTOYIELD = .lAutoYield
			.cCOMPUTERNAME = GETENV("COMPUTERNAME")
			.cUSERNAME = GETENV("USERNAME")
			IF .lTestMode THEN
				.cLogFile = 'F:\UTIL\ADPREPORTS\LOGFILES\KENBKDN_REPORT_log_TESTMODE.txt'
				.cSendTo = 'Mark Bennett <mbennett@fmiint.com>'
			ENDIF
			.lLoggingIsOn = .lLoggingIsOn AND NOT EMPTY(.cLogFile)
			IF .lLoggingIsOn THEN
				SET ALTERNATE TO (.cLogFile) ADDITIVE
				SET ALTERNATE ON
			ENDIF
		ENDWITH
	ENDFUNC


	FUNCTION DESTROY
		WITH THIS
			IF .lLoggingIsOn  THEN
				SET ALTERNATE OFF
				SET ALTERNATE TO
			ENDIF
		ENDWITH
		DODEFAULT()
	ENDFUNC


	FUNCTION MAIN
		WITH THIS
			LOCAL lnNumberOfErrors, lcSQL, lcSQL2, lcSQL3, loError, ldToday
			LOCAL lcFiletoSaveAs, lcSpreadsheetTemplate, lcGrandTotalRow, lcWhere
			LOCAL oExcel, oWorkbook, oWorksheet
			TRY
				lnNumberOfErrors = 0

				.TrackProgress('', LOGIT)
				.TrackProgress('', LOGIT)
				.TrackProgress('=========================================================', LOGIT+SENDIT)
				.TrackProgress("Employee Breakdown REPORT process started....", LOGIT+SENDIT+NOWAITIT)
				.TrackProgress('COMPUTERNAME = ' + .cCOMPUTERNAME, LOGIT+SENDIT)
				.TrackProgress('USERNAME = ' + .cUSERNAME, LOGIT+SENDIT)
				.TrackProgress('=========================================================', LOGIT+SENDIT)
				IF .lTestMode THEN
					.TrackProgress('                        TEST MODE                        ', LOGIT+SENDIT)
					.TrackProgress('=========================================================', LOGIT+SENDIT)
				ENDIF
				.TrackProgress('', LOGIT)

				ldToday = .dToday

				DO CASE
					CASE .lTestMode
						.cSendTo = 'Mark Bennett <mbennett@fmiint.com>'
					CASE .lShowSalaries
						.cSendTo = 'Lucille Waldrip <lwaldrip@fmiint.com>, Stephanie Kochanski <skochanski@fmiint.com>, Rebecca Selawsky <rselawsky@fmiint.com>'
					OTHERWISE
						.cSendTo = 'Ken Kausner <kkausner@fmiint.com>'
				ENDCASE
				
				SELECT DATE, ADP_COMP, DIVISION, DEPARTMENT, SPACE(30) AS DEPTDESC, COUNT(*) AS HEADCNT ;
					FROM f:\util\snapshots\adp\snapall.dbf ;
					INTO CURSOR CURTRENDING ;
					WHERE (STATUS <> 'T') AND DAY(DATE)=15 ;
					AND YEAR(DATE) = 2009 ;
					GROUP BY 1,2,3,4, 5 ;
					ORDER BY 1,2,3,4 ;
					READWRITE
			
*!*	SELECT CURTRENDING
*!*	BROW				

				* get ss#s from ADP
				OPEN DATABASE F:\UTIL\ADPCONNECTION\CONNECT1

				.nSQLHandle = SQLCONNECT("PCPAYWIN","MBENNETT","VP737A")

				IF .nSQLHandle > 0 THEN

					lcWhere = ""
					*lcWhere = " AND (A.CUSTAREA3 = 'NJ1' OR A.CUSTAREA3 = 'NJ2' OR A.CUSTAREA3 = 'NJ3' OR A.CUSTAREA3 = 'NJ4')"
					*lcWhere = " AND (A.CUSTAREA3 = 'SP1' OR A.CUSTAREA3 = 'SP2')"
					*lcWhere = " AND {fn LEFT(A.HOMEDEPARTMENT,2)} IN ('07')"
					
					lcSQL = ;
						"SELECT " + ;
						" COMPANYCODE, " + ;
						" DEPARTMENT, " + ;
						" DESCRIPTION " + ;
						" FROM REPORTS.V_DEPARTMENT " + ;
						" WHERE COMPANYCODE IN ('E87','E88','E89') " + ;
						" ORDER BY COMPANYCODE, DEPARTMENT "

*!*						lcSQL = ;
*!*							"SELECT " + ;
*!*							" {fn LEFT(A.HOMEDEPARTMENT,2)} AS DIVISION, " + ;
*!*							" {fn SUBSTRING(A.HOMEDEPARTMENT,3,4)} AS DEPT, " + ;
*!*							" B.DESCRIPTION AS DEPTDESC, " + ;
*!*							" A.NAME, " + ;
*!*							" A.FILE# AS FILE_NUM, " + ;
*!*							" A.RATETYPE, " + ;
*!*							" {fn IFNULL(A.RATE1AMT,0.00)} AS RATE1AMT, " + ;
*!*							" {fn IFNULL(A.ANNUALSALARY,0.00)} AS ANNSALARY, " + ;
*!*							" (SELECT {fn IFNULL(MAX(DEDUCTIONAMOUNT),0.00)} FROM REPORTS.V_DEDUCTIONS WHERE DEDUCTIONCODE = 'E' AND NAME = A.NAME AND COMPANYCODE IN ('E87','E88','E89')) AS CARALLOW " + ;
*!*							" FROM REPORTS.V_EMPLOYEE A, " + ;
*!*							" REPORTS.V_DEPARTMENT B " + ;
*!*							" WHERE B.COMPANYCODE(+) = A.COMPANYCODE " + ;
*!*							" AND B.DEPARTMENT(+) = A.HOMEDEPARTMENT " + ;
*!*							" AND A.COMPANYCODE IN ('E87','E88','E89') " + ;
*!*							" AND A.STATUS <> 'T' " + ;
*!*							lcWhere + ;
*!*							" ORDER BY 1, 2, 4 "

					IF .lTestMode THEN
						.TrackProgress('                        TEST MODE                        ', LOGIT+SENDIT)
						.TrackProgress('lcSQL =' + lcSQL, LOGIT+SENDIT)
					ENDIF

					IF USED('CURDEPTS') THEN
						USE IN CURDEPTS
					ENDIF

					IF .ExecSQL(lcSQL, 'CURDEPTS', RETURN_DATA_MANDATORY) THEN

*!*	SELECT CURDEPTS
*!*	BROWSE
						SELECT CURTRENDING
						SCAN
							SELECT CURDEPTS
							LOCATE FOR ALLTRIM(DEPARTMENT) == (CURTRENDING.DIVISION + CURTRENDING.DEPARTMENT)
							IF FOUND() THEN
								REPLACE CURTRENDING.DEPTDESC WITH CURDEPTS.DESCRIPTION
							ENDIF
						ENDSCAN
						
SELECT CURTRENDING
COPY TO C:\A\KENBKDN_TRENDING.XLS XL5
THROW

						**************************************************************************************************************
						**************************************************************************************************************
						** setup file names
						IF .lShowSalaries THEN
							lcSpreadsheetTemplate = "F:\UTIL\ADPREPORTS\KENBKDN_TEMPLATE_SALARIES.XLS"
							lcFiletoSaveAs = "F:\UTIL\ADPREPORTS\REPORTS\Employee Breakdown Report (with Salaries) for " + STRTRAN(DTOC(ldToday),"/","-") + ".XLS"
						ELSE
							lcSpreadsheetTemplate = "F:\UTIL\ADPREPORTS\KENBKDN_TEMPLATE.XLS"
							lcFiletoSaveAs = "F:\UTIL\ADPREPORTS\REPORTS\Employee Breakdown Report for " + STRTRAN(DTOC(ldToday),"/","-") + ".XLS"
						ENDIF


						**************************************************************************************************************
						**************************************************************************************************************

						IF FILE(lcFiletoSaveAs) THEN
							DELETE FILE (lcFiletoSaveAs)
						ENDIF


						***********************************************************************************
						***********************************************************************************
						** output to Excel spreadsheet
						.TrackProgress('Creating spreadsheet: ' + lcFiletoSaveAs, LOGIT+SENDIT+NOWAITIT)

						oExcel = CREATEOBJECT("excel.application")
						oExcel.VISIBLE = .F.
						oWorkbook = oExcel.workbooks.OPEN(lcSpreadsheetTemplate)
						oWorkbook.SAVEAS(lcFiletoSaveAs)

						oWorksheet = oWorkbook.Worksheets[1]
						oWorksheet.RANGE("A5","AT2000").ClearContents()
						oWorksheet.RANGE("A1").VALUE = "Summit Global Logistics Staffing Report for " + TRANSFORM(ldToday)

						LOCAL lnStartRow, lnRow, lnEndRow, lnRowsProcessed, lcStartRow, lcEndRow, lcPrevDiv, lcPrevDept
						LOCAL lnDivCount, lnDeptCount

						lnStartRow = 5
						lcStartRow = ALLTRIM(STR(lnStartRow))

						*!*							lnEndRow = lnStartRow - 1
						lnRow = lnStartRow - 1

						STORE 0 TO lnDivCount, lnDeptCount

						SELECT SQLCURSOR1
						LOCATE

						* init break check variables
						lcPrevDiv = SQLCURSOR1.DIVISION
						lcPrevDept = SQLCURSOR1.DEPT

						SCAN

							IF (SQLCURSOR1.DIVISION <> lcPrevDiv) OR (SQLCURSOR1.DEPT <> lcPrevDept) THEN
								* break in div or dept -- insert subtotals line
								lnRow = lnRow + 1
								lnEndRow = lnRow
								lcRow = ALLTRIM(STR(lnRow))
							ENDIF
							IF (SQLCURSOR1.DEPT <> lcPrevDept) THEN
								*  insert DEPT subtotals line
								oWorksheet.RANGE("I"+lcRow).VALUE = lnDeptCount
								* reset counter
								lnDeptCount = 0
							ENDIF
							IF (SQLCURSOR1.DIVISION <> lcPrevDiv) THEN
								*  insert DIVISION subtotals line
								oWorksheet.RANGE("J"+lcRow).VALUE = lnDivCount
								* reset counter
								lnDivCount = 0
							ENDIF

							lnRow = lnRow + 1
							*!*								lnEndRow = lnRow
							lcRow = ALLTRIM(STR(lnRow))

							* INCREMENT SUBTOTALS COUNTERS
							lnDivCount = lnDivCount + 1
							lnDeptCount = lnDeptCount + 1

							oWorksheet.RANGE("A"+lcRow).VALUE = "'" + SQLCURSOR1.DIVISION
							oWorksheet.RANGE("B"+lcRow).VALUE = "'" + SQLCURSOR1.DEPT
							oWorksheet.RANGE("C"+lcRow).VALUE = "'" + SQLCURSOR1.DEPTDESC
							oWorksheet.RANGE("D"+lcRow).VALUE = "'" + SQLCURSOR1.NAME
							oWorksheet.RANGE("E"+lcRow).VALUE = "'" + SQLCURSOR1.RATETYPE

							IF .lShowSalaries THEN
								oWorksheet.RANGE("F"+lcRow).VALUE = SQLCURSOR1.RATE1AMT
								oWorksheet.RANGE("G"+lcRow).VALUE = SQLCURSOR1.ANNSALARY
								oWorksheet.RANGE("H"+lcRow).VALUE = SQLCURSOR1.CARALLOW
							ENDIF

							*!*								IF .lTestMode THEN
							*!*									oWorksheet.RANGE("I"+lcRow).VALUE = SQLCURSOR1.FILE_NUM
							*!*								ENDIF

							* 'remember' prev div/depts
							lcPrevDiv = SQLCURSOR1.DIVISION
							lcPrevDept = SQLCURSOR1.DEPT

						ENDSCAN

						*!*							* populate header counts, sums, etc.
						*!*							lnRowsProcessed = lnEndRow - lnStartRow + 1
						*!*							lcEndRow = ALLTRIM(STR(lnEndRow))

						* final subtotals line
						lnRow = lnRow + 1
						*!*							lnEndRow = lnRow
						lcRow = ALLTRIM(STR(lnRow))

						*  insert DEPT subtotals line
						oWorksheet.RANGE("I"+lcRow).VALUE = lnDeptCount
						*  insert DIVISION subtotals line
						oWorksheet.RANGE("J"+lcRow).VALUE = lnDivCount

						lcGrandTotalRow = ALLTRIM(STR(lnRow + 2))

						*!*	Grand Total for DivCount column
						oWorksheet.RANGE("D" + lcGrandTotalRow).VALUE = "Grand Total Headcount:"
						oWorksheet.RANGE("J" + lcGrandTotalRow).VALUE = "=SUM(J5:J" + lcRow + ")"
						oWorksheet.RANGE("D" + lcGrandTotalRow,"J" + lcGrandTotalRow).FONT.bold = .T.

						* SAVE AND QUIT EXCEL
						oWorkbook.SAVE()
						oExcel.QUIT()

						***********************************************************************************
						***********************************************************************************


						IF FILE(lcFiletoSaveAs) THEN
							* attach output file to email
							.cAttach = lcFiletoSaveAs
							.cBodyText = "See attached Employee Breakdown report." + ;
								CRLF + CRLF + "(do not reply - this is an automated report)" + ;
								CRLF + CRLF + "<report log follows>" + ;
								CRLF + .cBodyText
						ELSE
							.TrackProgress('ERROR: unable to email spreadsheet: ' + lcFiletoSaveAs, LOGIT+SENDIT+NOWAITIT)
						ENDIF

						CLOSE DATABASES ALL
					ENDIF  &&  .ExecSQL(lcSQL, 'SQLCURSOR1', RETURN_DATA_MANDATORY)

					.TrackProgress("Employee Breakdown REPORT process ended normally.", LOGIT+SENDIT+NOWAITIT)
				ELSE
					* connection error
					.TrackProgress('Unable to connect to ADP Payroll System.', LOGIT+SENDIT)
				ENDIF   &&  .nSQLHandle > 0

			CATCH TO loError

				.TrackProgress('There was an error.',LOGIT+SENDIT)
				.TrackProgress('The Error # is: ' + TRANSFORM(loError.ERRORNO), LOGIT+SENDIT)
				.TrackProgress('The Error Message is: ' + TRANSFORM(loError.MESSAGE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred in Program: ' + TRANSFORM(loError.PROCEDURE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred at line #: ' + TRANSFORM(loError.LINENO), LOGIT+SENDIT)
				lnNumberOfErrors = lnNumberOfErrors + 1
				IF TYPE('oWorkbook') = "O" THEN
					oWorkbook.SAVE()
				ENDIF
				IF TYPE('oExcel') = "O" THEN
					oExcel.QUIT()
				ENDIF
				CLOSE DATABASES ALL

			ENDTRY

			CLOSE DATA
			WAIT CLEAR
			***************** INTERNAL email results ******************************
			.TrackProgress('About to send status email.',LOGIT)
			.TrackProgress('The logfile is: ' + .cLogFile, SENDIT)
			.TrackProgress("Employee Breakdown REPORT process started: " + .cStartTime, LOGIT+SENDIT)
			.TrackProgress("Employee Breakdown REPORT process finished: " + TTOC(DATETIME()), LOGIT+SENDIT)

			IF .lSendInternalEmailIsOn THEN
				DO FORM dartmail2 WITH .cSendTo,.cFrom,.cSubject,.cCC,.cAttach,.cBodyText,"A"
				.TrackProgress('Sent status email.',LOGIT)
			ELSE
				.TrackProgress('Did not send status email: THIS.lSendInternalEmailIsOn = FALSE.',LOGIT)
			ENDIF

		ENDWITH
		RETURN
	ENDFUNC && main


	FUNCTION ExecSQL
		LPARAMETERS tcSQL, tcCursorName, tlNoDataReturnedIsOkay
		LOCAL llRetval, lnResult
		WITH THIS
			* close target cursor if it's open
			IF USED(tcCursorName)
				USE IN (tcCursorName)
			ENDIF
			WAIT WINDOW "Fetching remote data; please wait..." NOWAIT
			lnResult = SQLEXEC(.nSQLHandle, tcSQL, tcCursorName)
			llRetval = ( lnResult > 0 )
			IF llRetval THEN
				* see if any data came back
				IF NOT tlNoDataReturnedIsOkay THEN
					IF (NOT USED(tcCursorName)) OR EOF(tcCursorName) THEN
						llRetval = .F.
						.TrackProgress('No data was returned by this query: ' +CRLF + tcSQL, LOGIT+SENDIT)
						.cSendTo = 'Mark Bennett <mbennett@fmiint.com>'
					ENDIF
				ENDIF
			ELSE
				.TrackProgress("Result Code [" + TRANSFORM(lnResult) + "] returned by this query: " + tcSQL, LOGIT+SENDIT)
				.cSendTo = 'Mark Bennett <mbennett@fmiint.com>'
			ENDIF
			WAIT CLEAR
			RETURN llRetval
		ENDWITH
	ENDFUNC


	PROCEDURE TrackProgress
		* do any combination of Wait Window, writing to logfile, and adding to body of email,
		* based on nFlags parameter.
		LPARAMETERS tcExpression, tnFlags
		WITH THIS
			IF BITAND(tnFlags,LOGIT) = LOGIT THEN
				IF .lLoggingIsOn THEN
					?
					? .cProcessName + "  " + TTOC(DATETIME()) + ": " + tcExpression
				ENDIF
			ENDIF
			IF .lWaitWindowIsOn THEN
				IF BITAND(tnFlags,NOWAITIT) = NOWAITIT THEN
					WAIT WINDOW tcExpression NOWAIT
				ENDIF
				IF BITAND(tnFlags,WAITIT) = WAITIT THEN
					WAIT WINDOW tcExpression TIMEOUT .nWaitWindowTimeout
				ENDIF
			ENDIF
			IF BITAND(tnFlags,SENDIT) = SENDIT THEN
				IF .lSendInternalEmailIsOn THEN
					.cBodyText = .cBodyText + tcExpression + CRLF + CRLF
				ENDIF
			ENDIF
		ENDWITH
	ENDPROC  &&  TrackProgress

ENDDEFINE

