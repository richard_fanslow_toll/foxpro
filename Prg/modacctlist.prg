lparameters xdatabase, xmod, xxtable

if empty(xmod)
	xmod=""
endif
if empty(xxtable)
	xxtable=""
else
	xxtable=xxtable+"."
endif

xmodacctlist=""

do case
case xdatabase="N" 
	select account
	scan for !inactive and njmod=xmod and njinven
		xmodacctlist=xmodacctlist+transform(accountid)+","
	endscan
case xdatabase="C" and xmod="1"
	select account
	scan for !inactive and building="4" and cainven
		xmodacctlist=xmodacctlist+transform(accountid)+","
	endscan
case xdatabase="C" and xmod="2"
	select account
	scan for !inactive and building="7" and cainven
		xmodacctlist=xmodacctlist+transform(accountid)+","
	endscan
case xdatabase="C" and xmod="5"
	select account
	scan for !inactive and building="6" and cainven
		xmodacctlist=xmodacctlist+transform(accountid)+","
	endscan
case xdatabase="C" and xmod="6"
	select account
	scan for !inactive and building="5" and cainven
		xmodacctlist=xmodacctlist+transform(accountid)+","
	endscan
case xdatabase="C" and xmod="7"
	select account
	scan for !inactive and building="0" and cainven
		xmodacctlist=xmodacctlist+transform(accountid)+","
	endscan
case xdatabase="C" and xmod="8"
	select account
	scan for !inactive and building="1" and cainven
		xmodacctlist=xmodacctlist+transform(accountid)+","
	endscan
endcase

if empty(xmodacctlist)
	return ".t."
else
	xmodacctlist="inlist("+xxtable+"accountid,"+left(xmodacctlist,len(xmodacctlist)-1)+")"
endif

return xmodacctlist