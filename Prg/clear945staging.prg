close data all
clear all
set safety off

use f:\3pl\data\checkstagingfolders alias csf

scan
	cfoldername = alltrim(csf.foldername)
	try		&& dy 12/5/17
		cd &cfoldername
		wait window cfoldername nowait
		delete file *.*
	catch
	endtry
endscan

close data all
wait window "Processing complete...exiting" timeout 2
set safety on
return