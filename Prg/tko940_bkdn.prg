PARAMETERS cFilename

WAIT WINDOW "In BKDN prg...debug" TIMEOUT 2

lDoColor = .T.
lDropShip = .F.
lLX = .F.
lnEaches = 0
nRepQty = 0
lPick = .T.
lPullByCtn = .F.
cBillTo = ""
lPackQty = 0
m.isa_num = ""
m.printstuff = ""
eMailCommentStr = ""
cPackStr = ""
cPackgroup = ""

SELECT x856
SET FILTER TO
COUNT FOR TRIM(segment) = "W05" TO nSegCnt
cSegCnt = ALLTRIM(STR(nSegCnt))
WAIT "There are "+cSegCnt+" P/Ts in this file" WINDOW TIMEOUT 2
nPTqty = nSegCnt
nTotPT = 1
SELECT x856
LOCATE

*!*	IF !USED("uploaddet")
*!*		IF lTesting OR lTestUploaddet
*!*			USE F:\ediwhse\test\uploaddet IN 0 ALIAS uploaddet
*!*		ELSE
*!*			USE F:\ediwhse\DATA\uploaddet IN 0 SHARED ALIAS uploaddet
*!*		ENDIF
*!*	ENDIF

SELECT xpt
DELETE ALL
SELECT xptdet
DELETE ALL

STORE 0 TO pt_total_cartons
STORE 0 TO nCtnCount
STORE 0 TO ptctr
STORE 0 TO nTotWt,nTotCube
cStoreNum = ""

SELECT x856
LOCATE

m.PTDETID=0

DO WHILE !EOF("x856")
	cSegment = TRIM(x856.segment)
	cF1 = TRIM(x856.f1)

	IF INLIST(TRIM(x856.segment),"GS","ST")
		SELECT x856
		IF !EOF()
			SKIP 1 IN x856
			LOOP
		ENDIF
	ENDIF

	IF TRIM(x856.segment) = "ISA"
		cISA_Num = ALLTRIM(x856.f13)
		SKIP 1 IN x856
		LOOP
	ENDIF

	IF TRIM(x856.segment) = "W05"  && new shipment
*    nAcctNum = 5865
		llBJ_VMI_Flag = .F.
		lAsst = .F.
		lPick = .T.
		m.units = .T.
		SELECT xpt
		APPEND BLANK
		STORE 0 TO pt_total_cartons
		ptctr = ptctr +1
		nTotqty = 0
		REPLACE xpt.addby WITH "FMI PROC" IN xpt
		REPLACE xpt.adddt WITH DATETIME() IN xpt
		REPLACE xpt.ptid       WITH ptctr IN xpt
		REPLACE xpt.ptdate     WITH DATE() IN xpt
		REPLACE xpt.accountid WITH nAcctNum IN xpt
		cShip_ref = ALLTRIM(x856.f2)
		cCNEE_ref = UPPER(ALLTRIM(x856.f3))
		REPLACE xpt.ship_ref WITH cShip_ref IN xpt && Pickticket
		REPLACE xpt.cnee_ref WITH cCNEE_ref IN xpt  && Customer PO
		WAIT CLEAR
		waitstr = "Now processing PT # "+cShip_ref+CHR(13)+"FILE: "+cFilename
		WAIT WINDOW AT 10,10  waitstr NOWAIT
	ENDIF

	IF TRIM(x856.segment) = "GE"
		IF lTesting
			CLEAR
		ENDIF
	ENDIF

	IF TRIM(x856.segment) = "N1" AND INLIST(TRIM(x856.f1),"ST","CN")
		cStoreNum = UPPER(ALLTRIM(x856.f4))
		IF EMPTY(cStoreNum)
*      ASSERT .F. MESSAGE "At Store Number insertion"
		ENDIF
		cConsignee = UPPER(ALLT(x856.f2))
		cConsignee = STRTRAN(cConsignee,'"','') && Removes double-quotes (2.1.2010, per Darren)
		cConsignee = STRTRAN(cConsignee,"''","") && Removes pairs of single-quotes
		lSams = IIF("SAMS"$cConsignee OR "SAM'S"$cConsignee,.T.,.F.)
		lSears = IIF("SEARS"$cConsignee OR "KMART"$cConsignee OR "K-MART"$cConsignee,.T.,.F.)
		IF lTesting
*Set Step On
		ENDIF
		IF lNewPT
			lNewPT = .F.
		ELSE
			IF !(cShip_ref$eMailCommentStr)
				nTotPT = nTotPT+1
			ENDIF
		ENDIF
		cDCNum = ""
		IF EMPTY(xpt.shipins)
			REPLACE xpt.shipins WITH "STORENUM*"+cStoreNum IN xpt  && DC or Store Number
		ELSE
			REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"STORENUM*"+cStoreNum IN xpt  && DC or Store Number
		ENDIF

		IF LEN(cStoreNum)>=13
			IF SEEK(cStoreNum,'samsdcs','gln')
				cStoreNum = ALLTRIM(samsdcs.dcnum)
			ELSE
				cStoreNum = "9999"
			ENDIF
			SELECT x856
		ENDIF
		REPLACE xpt.dcnum WITH cStoreNum IN xpt
		REPLACE xpt.storenum WITH INT(VAL(RIGHT(ALLTRIM(cStoreNum),5))) IN xpt
		REPLACE xpt.consignee WITH cConsignee IN xpt NEXT 1
		REPLACE xpt.NAME WITH xpt.consignee IN xpt NEXT 1

		SKIP 1 IN x856
		DO CASE
			CASE TRIM(x856.segment) = "N2"  && address info
				REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"STNAME2*"+UPPER(ALLTRIM(x856.f1)) IN xpt
				SKIP 1 IN x856
				IF TRIM(x856.segment) = "N3"  && address info
					REPLACE xpt.address WITH UPPER(TRIM(x856.f1)) IN xpt
					REPLACE xpt.address2  WITH UPPER(TRIM(x856.f2)) IN xpt
				ELSE
					SET STEP ON
					cMessage = "Missing Ship-To Address Info, PT# "+cShip_ref
					WAIT WINDOW cMessage TIMEOUT 2
					NormalExit = .F.
					THROW
				ENDIF

			CASE TRIM(x856.segment) = "N3"  && address info
				REPLACE xpt.address WITH UPPER(TRIM(x856.f1)) IN xpt
				REPLACE xpt.address2  WITH UPPER(TRIM(x856.f2)) IN xpt
			OTHERWISE
				SET STEP ON
				cMessage = "Missing Ship-To Address Info, PT# "+cShip_ref
				WAIT WINDOW cMessage TIMEOUT 2
				NormalExit = .F.
				THROW
		ENDCASE

		SKIP 1 IN x856
		IF TRIM(x856.segment) = "N4"  && address info
			c1city = TRIM(x856.f1)
			c1state= TRIM(x856.f2)
			c1zip = TRIM(x856.f3)
			IF EMPTY(c1city) OR EMPTY(c1state)
				SET STEP ON

				cMessage = "Missing Ship-To Address Info, PT# "+cShip_ref
				WAIT WINDOW cMessage TIMEOUT 2
				NormalExit = .F.
				THROW
			ENDIF
			cCSZ = c1city+", "+c1state+" "+c1zip
			REPLACE xpt.csz WITH UPPER(cCSZ) IN xpt
		ELSE
			cMessage = "Missing Ship-To CSZ Info, PT# "+cShip_ref
			WAIT WINDOW cMessage TIMEOUT 2
			NormalExit = .F.
			THROW
		ENDIF
	ENDIF

	IF (TRIM(x856.segment) = "N1" AND TRIM(x856.f1) = "Z7")
		STORE UPPER(TRIM(x856.f2)) TO cBillTo
		REPLACE xpt.shipfor WITH cBillTo IN xpt
		cSFStoreNum = UPPER(ALLTRIM(x856.f4))
		REPLACE xpt.sforstore WITH cSFStoreNum IN xpt
		REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"SFSTORENUM*"+cSFStoreNum IN xpt  && DC or Store Number
		SKIP 1 IN x856
		DO CASE
			CASE TRIM(x856.segment) = "N2"  && address info
				REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"SFNAME2*"+UPPER(ALLTRIM(x856.f1)) IN xpt
				SKIP 1 IN x856
				IF TRIM(x856.segment) = "N3"  && address info
					REPLACE xpt.sforaddr1 WITH UPPER(TRIM(x856.f1)) IN xpt
					REPLACE xpt.sforaddr2  WITH UPPER(TRIM(x856.f2)) IN xpt
				ELSE
					cMessage = "Missing Mark-For Address Info after N2, PT# "+cShip_ref
					SET STEP ON
					WAIT WINDOW cMessage TIMEOUT 2
					NormalExit = .F.
					THROW
				ENDIF
			CASE TRIM(x856.segment) = "N3"  && address info
				REPLACE xpt.sforaddr1 WITH UPPER(TRIM(x856.f1)) IN xpt
				REPLACE xpt.sforaddr2  WITH UPPER(TRIM(x856.f2)) IN xpt
			OTHERWISE
				SET STEP ON
				cMessage = "Missing Mark-For Address Info after N1, PT# "+cShip_ref
				WAIT WINDOW cMessage TIMEOUT 2
				NormalExit = .F.
				THROW
		ENDCASE
		SKIP 1 IN x856
		IF TRIM(x856.segment) = "N4"  && address info
			c1city = TRIM(x856.f1)
			c1state= TRIM(x856.f2)
			c1zip = TRIM(x856.f3)
			IF EMPTY(c1city) OR EMPTY(c1state)
				SET STEP ON
				cMessage = "Missing Mark-for Address Info (CSZ) PT# "+cShip_ref
				WAIT WINDOW cMessage TIMEOUT 2
				NormalExit = .F.
				THROW
			ENDIF

			csforCSZ = c1city+", "+c1state+" "+c1zip
			REPLACE xpt.sforcsz WITH UPPER(csforCSZ) IN xpt
		ELSE
			ASSERT .F. MESSAGE "Missing Ship-For CSZ, PT# "+cShip_ref
			cMessage = "Missing Ship-For CSZ Info, PT# "+cShip_ref
			WAIT WINDOW cMessage TIMEOUT 2
			NormalExit = .F.
			THROW
		ENDIF
	ENDIF

	IF (TRIM(x856.segment) = "N1" AND TRIM(x856.f1) = "BT")
		IF lTesting
*Set Step On
		ENDIF
		cBilltoField = ALLTRIM(x856.f2)
		lLord = IIF(("LORD"$cBilltoField AND "TAYLOR"$cBilltoField) OR ("L&T"$cBilltoField),.T.,.F.)
		IF "DROP"$cBilltoField AND "SHIP"$cBilltoField AND lLord
			lDropShip = .T.
			REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"DROPSHIP"IN xpt  && DC or Store Number
		ENDIF
	ENDIF

	IF TRIM(x856.segment) = "N9" AND TRIM(x856.f1) = "DP" && Dept Num.
		cDept = TRIM(x856.f2)
		REPLACE xpt.dept WITH cDept IN xpt
	ENDIF

	IF TRIM(x856.segment) = "N9" AND TRIM(x856.f1) = "MR" && Dept Num.
		REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"MR*"+ALLTRIM(x856.f2) IN xpt
	ENDIF

	IF TRIM(x856.segment) = "N9" AND TRIM(x856.f1) = "CT" && Carton Code
		REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"CTNCODE*"+ALLTRIM(x856.f2) IN xpt
	ENDIF

	IF TRIM(x856.segment) = "N9" AND TRIM(x856.f1) = "FOB" && Dept Num.
		REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"FOB*"+ALLTRIM(x856.f2) IN xpt
	ENDIF

	IF TRIM(x856.segment) = "N9" AND TRIM(x856.f1) = "PH" && TMW Ship Mode Code
*Set Step On
		nSMCode = INT(VAL(x856.f2))
		USE F:\3pl\DATA\TMWCodes IN 0 ALIAS TMW
		IF SEEK(nSMCode,'TMW','SMCODE')
			cSMDesc = ALLTRIM(TMW.DESC)
		ENDIF
		USE IN TMW
		SELECT xpt
		REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"SMCODE*"+ALLTRIM(STR(nSMCode)) IN xpt
		REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"SMDESC*"+ALLTRIM(cSMDesc) IN xpt
	ENDIF

	IF TRIM(x856.segment) = "N9" AND TRIM(x856.f1) = "SU" && Label Type Code
		REPLACE xpt.cacctnum WITH ALLTRIM(x856.f2)  && added here 02/03/2017 PG && Removed underscore, which bombed out the prg. Joe, 02.06.17
		REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"LABEL*"+ALLTRIM(x856.f2) IN xpt
		IF TRIM(x856.f3) = "LO002"  && Drop ship code
			REPLACE xpt.ship_ref WITH ALLTRIM(xpt.ship_ref)="-DS" IN xpt
		ENDIF
		IF  ALLTRIM(x856.f2) = "BJ002"
			llBJ_VMI_Flag = .T.  && no PO needs to be appended after the style
		ENDIF

	ENDIF

	IF TRIM(x856.segment) = "N9" AND TRIM(x856.f1) = "IA" && Vendor Num.
		IF ALLTRIM(x856.f2) = "276238620" && Wal-Mart vendor number
			IF SEEK(cStoreNum,"samsdcs","gln")
				REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"GLN*"+ALLTRIM(cStoreNum) IN xpt
				cDCNum = ALLTRIM(samsdcs.dcnum)
				REPLACE xpt.dcnum WITH cDCNum IN xpt
				REPLACE xpt.storenum WITH INT(VAL(RIGHT(cDCNum,5))) IN xpt
				REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"STORENUM*"+cDCNum IN xpt  && DC or Store Number
			ELSE
				REPLACE xpt.dcnum WITH cStoreNum IN xpt
				REPLACE xpt.storenum WITH INT(VAL(RIGHT(cStoreNum,5))) IN xpt
				REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"STORENUM*"+cStoreNum IN xpt  && DC or Store Number
			ENDIF
		ENDIF
		cVendNum = RIGHT(ALLTRIM(x856.f2),10)
		REPLACE xpt.vendor_num WITH cVendNum IN xpt
	ENDIF

	IF TRIM(x856.segment) = "G62" AND TRIM(x856.f1) == "04" && Ship Date
		cXdate1 = TRIM(x856.f2)
		datestrconversion()
		REPLACE xpt.START WITH dxdate2 IN xpt
	ENDIF

	IF TRIM(x856.segment) = "G62" AND TRIM(x856.f1) == "02" && Cancel Date
		cXdate1 = TRIM(x856.f2)
		datestrconversion()
		REPLACE xpt.CANCEL WITH dxdate2 IN xpt
	ENDIF

	IF TRIM(x856.segment) = "NTE"  && Notes
*    ASSERT .F. MESSAGE "At delivery instructions"
		IF ALLTRIM(x856.f1) = "WHI"
			IF !EMPTY(ALLTRIM(x856.f2))
				IF x856.f2 = "ASSORTMENT"
					lAsst = .T.
					warehouseinfo()
				ELSE
					REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"WHSEINST*"+ALLTRIM(x856.f2) IN xpt
				ENDIF
			ENDIF
		ENDIF

		IF ALLTRIM(x856.f1) = "DEL"
			IF !EMPTY(ALLTRIM(x856.f2))
				REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"DELIVINST*"+ALLTRIM(x856.f2) IN xpt
			ENDIF
		ENDIF

	ENDIF

	IF TRIM(x856.segment) = "W66"  && Shipping info
		REPLACE xpt.ship_via WITH ALLTRIM(x856.f5)
		REPLACE xpt.scac WITH ALLTRIM(x856.f10)
	ENDIF

*********************************************
*!* XPTDET Update Section
*********************************************

	lLoopSeg = "LX"
	IF TRIM(x856.segment) = lLoopSeg && start of PT detail, stay here until this PT is complete
		IF lTesting
*Set Step On
		ENDIF
		IF lLord AND lDropShip
			REPLACE xpt.scac WITH "FEDX" IN xpt
		ENDIF

		SELECT x856

		lPick = .T.
		nCurrRec = RECNO()
		DO WHILE .T.
			SKIP 1 IN x856
			IF INLIST(x856.segment,"LX","W76")
				EXIT
			ENDIF
			IF x856.segment = "N9" AND x856.f1 = "PKG"
				lPick = .F.
				EXIT
			ENDIF
		ENDDO
		GO nCurrRec
		lLX = .T.

		DO WHILE TRIM(x856.segment) != "SE"
			cxSegment = TRIM(x856.segment)
			cxField01 = TRIM(x856.f1)

			IF TRIM(x856.segment) = "LX"
				SKIP 1 IN x856
				LOOP
			ENDIF

			lUseNumPack = .F.
			IF TRIM(x856.segment) = "W01"
				cUnitCode = ALLTRIM(x856.f2)
				lUseNumPack = IIF(cUnitCode = "CA",.T.,lUseNumPack)
				m.printstuff = ""
				m.printstuff = IIF(EMPTY(m.printstuff),"UNITCODE*"+cUnitCode,m.printstuff+CHR(13)+"UNITCODE*"+cUnitCode)
				cPackgroup = ""
				m.upc = ALLTRIM(x856.f7)
				m.style = ALLT(x856.f5)
				IF "-"$m.style
*!*            ASSERT .F. MESSAGE "At style with dash...debug"
*!*            m.style = LEFT(TRIM(m.style),AT("-",m.style)-1)
				ENDIF

				cStylePack = ""
				DO CASE
					CASE (!lPick OR lSams)
						cStylePack = '1'
						npack = 1
						IF !lPick
							m.units = .F.
							m.printstuff = IIF(EMPTY(m.printstuff),"PREPACK",m.printstuff+CHR(13)+"PREPACK")
							m.printstuff = m.printstuff+CHR(13)+"ORIGSTYLE"
							IF lMB
								REPLACE xpt.accountid WITH 6705 IN xpt
							ELSE
								REPLACE xpt.accountid WITH 5836 IN xpt
							ENDIF
							REPLACE xpt.goh WITH .F. IN xpt
						ELSE
							m.units = .T.
							npack = 1
							m.printstuff = IIF(EMPTY(m.printstuff),"PICKPACK",m.printstuff+CHR(13)+"PICKPACK")
							m.printstuff = m.printstuff+CHR(13)+"ORIGSTYLE"
							IF lMB
								REPLACE xpt.accountid WITH 6705 IN xpt
							ELSE
								REPLACE xpt.accountid WITH 5865 IN xpt
							ENDIF
							REPLACE xpt.goh WITH .T. IN xpt
						ENDIF
					CASE lPick
						m.units = .T.
						npack = 1
						m.printstuff = IIF(EMPTY(m.printstuff),"PICKPACK",m.printstuff+CHR(13)+"PICKPACK")
						m.printstuff = m.printstuff+CHR(13)+"ORIGSTYLE"
						IF lMB
							REPLACE xpt.accountid WITH 6705 IN xpt
						ELSE
							REPLACE xpt.accountid WITH 5865 IN xpt
						ENDIF
						REPLACE xpt.goh WITH .T. IN xpt
				ENDCASE

&& ,"SUIT MART"  Removed 11.23.2015 per Wendy @ TKO
				IF llBJ_VMI_Flag = .F.  && style never needs to be added as a suffix when BJs-VMI N9*SU = "BJ002"
					IF INLIST(xpt.consignee,"VAN HEUSEN","PHILLIPS- VAN HEUSEN","CALVIN KLEIN","IZOD","GH BASS") ;
							OR INLIST(xpt.consignee,"BJ","CHRISTOPHER","C&B") OR ("KARAN"$xpt.consignee) OR;
							("DKNY"$xpt.consignee) OR ("HILFIGER"$xpt.consignee) OR (xpt.consignee = "CINTAS")

						IF (!lPick) OR (xpt.consignee = "CINTAS")
							IF !("/"$m.style)
								IF INLIST(xpt.consignee,"CINTAS","BEALL","BJ")
									m.style = ALLTRIM(m.style)+"/"+ALLTRIM(cCNEE_ref)
								ELSE
									m.style = ALLTRIM(m.style)+"/"+RIGHT(ALLTRIM(cCNEE_ref),5)
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF

				cPack = ALLTRIM(STR(npack))
				m.totqty = INT(VAL(x856.f1))
				IF lPick
					nTotqty = nTotqty + xptdet.totqty
				ENDIF
				m.origqty = m.totqty
				IF npack = 0
					ASSERT .F. MESSAGE "At Prepack TOTQTY calc...debug!"
				ENDIF

				m.printstuff = IIF(EMPTY(m.printstuff),"UNITTYPE*"+TRIM(x856.f2),m.printstuff+CHR(13)+"UNITTYPE*"+TRIM(x856.f2))
				SELECT xptdet
				APPEND BLANK
				m.PTDETID=m.PTDETID+1
				CLEAR
				IF EMPTY(m.upc)
					cMessage = "Empty UPC code, PT# "+cShip_ref
					WAIT WINDOW cMessage TIMEOUT 2
					NormalExit = .F.
					THROW
				ENDIF
				GATHER MEMVAR MEMO

				REPLACE xptdet.printstuff WITH xptdet.printstuff+CHR(13)+"ORIGQTY*"+TRANSFORM(xptdet.origqty) IN xptdet
*        REPLACE xptdet.totqty WITH 0, xptdet.origqty WITH 0 IN xptdet
				REPLACE xptdet.addby WITH "FMI PROC" IN xptdet
				REPLACE xptdet.adddt WITH DATETIME() IN xptdet
				REPLACE xptdet.ptid      WITH xpt.ptid IN xptdet
				REPLACE xptdet.accountid WITH xpt.accountid IN xptdet
				REPLACE xptdet.PACK WITH cPack IN xptdet
				REPLACE xptdet.casepack WITH npack IN xptdet
			ENDIF
			SELECT x856

			IF TRIM(x856.segment) = "G69"
				REPLACE xptdet.printstuff WITH xptdet.printstuff+CHR(13)+"MASTSTYLEDESC*"+TRIM(x856.f1) IN xptdet
			ENDIF

			IF TRIM(x856.segment) = "N9" AND ALLTRIM(x856.f1) = "PKG"
				REPLACE xptdet.units WITH .F. IN xptdet
				REPLACE xptdet.accountid WITH 5836 IN xptdet
				REPLACE xpt.accountid WITH 5836 IN xpt
				cCode = ALLTRIM(x856.f2)
				REPLACE xptdet.ID WITH cCode IN xptdet
				cPackgroup = "PACKGROUP*SOLIDPACK"
				IF ISALPHA(cCode)
					cPackgroup = "PACKGROUP*CASEPACK"
					lUseNumPack = .T.
					cPackStr=""
					FOR ss = 1 TO LEN(ALLTRIM(cCode))
						cAdd = SUBSTR(cCode,ss,1)
						cPackStr = IIF(ISDIGIT(cAdd),cPackStr+cAdd,cPackStr)
					ENDFOR
					npack = INT(VAL(cPackStr))
					RELEASE cPackStr,ss,cAdd
				ELSE
					npack = INT(VAL(x856.f3))
				ENDIF
				IF !cPackgroup$xpt.shipins
					REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+cPackgroup IN xpt
				ENDIF
				REPLACE xptdet.PACK WITH ALLTRIM(STR(npack)) IN xptdet
				REPLACE xptdet.casepack WITH npack IN xptdet
				REPLACE xptdet.totqty WITH IIF(lUseNumPack,xptdet.totqty,(xptdet.totqty/npack)) IN xptdet
				IF !lPick
					nTotqty = nTotqty + xptdet.totqty
				ENDIF
				REPLACE xptdet.origqty WITH xptdet.totqty IN xptdet
			ELSE
				IF lPick
					IF !"PKG*MUSICAL"$xpt.shipins
						REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"PKG*MUSICAL" IN xpt
					ENDIF
					REPLACE xptdet.printstuff WITH xptdet.printstuff+CHR(13)+"PKG*"+ALLTRIM(x856.f2) IN xptdet
					REPLACE xptdet.printstuff WITH xptdet.printstuff+CHR(13)+"QTY*"+ALLTRIM(x856.f3) IN xptdet
				ENDIF
			ENDIF

			IF TRIM(x856.segment) = "N9" AND ALLTRIM(x856.f1) = "CL"
				cColor = ""
				REPLACE xptdet.printstuff WITH xptdet.printstuff+CHR(13)+"ORIGCOLOR*"+ALLTRIM(x856.f2) IN xptdet
				cColor = ALLTRIM(x856.f2)
				IF LEFT(cColor,1)="0" AND SUBSTR(cColor,4,1) = "-"
					cColor = ALLTRIM(SUBSTR(cColor,5))  && Added the strtrans to resolve a color name issue, 12.27.2011
				ENDIF
				REPLACE xptdet.COLOR WITH cColor IN xptdet
			ENDIF

			IF TRIM(x856.segment) = "N9" AND ALLTRIM(x856.f1) = "SZ" AND ALLTRIM(x856.f2)#"QTY" && changed per Fred at TKO, 01.05.2012
				REPLACE xptdet.ID WITH ALLTRIM(x856.f2) IN xptdet
			ENDIF

* added these temporarily to upload Miami PT with solids that needed color and size.

			IF TRIM(x856.segment) = "N9" AND ALLTRIM(x856.f1) = "SK"
				REPLACE xptdet.custsku WITH ALLTRIM(x856.f2) IN xptdet
			ENDIF

			IF TRIM(x856.segment) = "W76"
				SELECT xptdet
				GO BOTT
				npack = 0
				lnEaches = 0
				nTotqty = INT(VAL(x856.f1))
				REPLACE xpt.qty WITH nTotqty IN xpt
				REPLACE xpt.origqty WITH xpt.qty IN xpt
				lLX = .F.
				REPLACE xpt.EACHES WITH INT(VAL(TRIM(x856.f1))) IN xpt
				STORE 0 TO nTotWt,nTotCube,pt_total_cartons
				REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+cPackgroup IN xpt
				cPackgroup = ""
			ENDIF

			SELECT xptdet
			IF xptdet.ptid = 0
				DELETE IN xptdet
			ENDIF

			SELECT x856
			IF !EOF()
				SKIP 1 IN x856
			ENDIF
		ENDDO  && SEGMENT NOW = 'SE'
	ENDIF
	IF !EOF()
		SKIP 1 IN x856
	ENDIF
ENDDO

DO m:\dev\prg\setuppercase940

WAIT CLEAR
IF lBrowfiles
	WAIT WINDOW "Now displaying XPT/XPTDET cursors..."+CHR(13)+"CANCEL at XPTDET if desired." TIMEOUT 2
	SELECT xpt
	SET ORDER TO
	LOCATE
	BROWSE TIMEOUT 10
	SELECT xptdet
	SET ORDER TO
	LOCATE
	BROWSE TIMEOUT 10
	IF MESSAGEBOX("Do you wish to continue?",4+32+256,"Browse Files Box")<>6
		CANCEL
	ENDIF
ENDIF

WAIT CLEAR
WAIT cMailName+" BKDN process complete for file "+xfile WINDOW TIMEOUT 2
lLX = .F.
lnEaches = 0
nRepQty = 0
m.isa_num = ""
SELECT x856
cTotPT = ALLTRIM(STR(nTotPT))
lNewPT = .T.
RETURN

******************************
PROCEDURE datestrconversion
******************************
	cXdate1 = TRIM(cXdate1)
	dxdate2 = CTOD(SUBSTR(cXdate1,5,2)+"/"+RIGHT(cXdate1,2)+"/"+LEFT(cXdate1,4))
ENDPROC

******************************
PROCEDURE warehouseinfo
******************************
	cOutfilex = "f:\3pl\data\tempwhsex.txt"
	CREATE CURSOR whsein (a c(50),b c(50),c c(50),d c(50),e c(50),F c(50))
	CREATE CURSOR whseinfo (maststyle c(20),mastupc c(20),subdesc c(30),subcolor c(20),;
		subsize c(10),subpack c(5),subupc c(30),subsku c(20))
	SELECT x856
	SCAN WHILE ALLTRIM(x856.segment) = "NTE" AND ALLTRIM(x856.f1) = "WHI"
		cWhseStr = ALLTRIM(x856.f2)
		STRTOFILE(cWhseStr,cOutfilex,0)
		SELECT whsein
		APPEND FROM &cOutfilex TYPE DELIMITED WITH CHARACTER "%"
		SELECT x856
	ENDSCAN
	SELECT whsein
	SCAN
		IF UPPER(whsein.a) = "ASSORTMENT"
			m.maststyle = ALLTRIM(whsein.b)
			m.mastupc = ALLTRIM(whsein.c)
		ELSE
			m.subdesc = ALLTRIM(whsein.a)
			m.subcolor = ALLTRIM(whsein.b)
			m.subsize = ALLTRIM(whsein.c)
			m.subpack = ALLTRIM(whsein.d)
			m.subupc = ALLTRIM(whsein.e)
			m.subsku = ALLTRIM(whsein.F)
			INSERT INTO whseinfo FROM MEMVAR
		ENDIF
	ENDSCAN
	DELETE FILE &cOutfilex
	USE IN whsein
	SELECT x856
ENDPROC

******************************
PROCEDURE pswhseinsert
******************************
	PARAMETERS cStyle1,cUPC1
	SELECT whseinfo
	LOCATE
	SCAN FOR whseinfo.maststyle = cStyle1 AND whseinfo.mastupc = cUPC1
		m.printstuff = IIF(EMPTY(m.printstuff),"SUBCUSTSKU*"+whseinfo.subsku,m.printstuff+CHR(13)+"SUBCUSTSKU*"+whseinfo.subsku)
		m.printstuff = m.printstuff+CHR(13)+"SUBSTYLEDESC*"+whseinfo.subdesc
		m.printstuff = m.printstuff+CHR(13)+"SUBPACK*"+whseinfo.subpack
		m.printstuff = m.printstuff+CHR(13)+"SUBUPC*"+whseinfo.subupc
		m.printstuff = m.printstuff+CHR(13)+"SUBCOLOR*"+whseinfo.subcolor
		m.printstuff = m.printstuff+CHR(13)+"SUBSIZE*"+whseinfo.subsize+CHR(13)
	ENDSCAN
	SELECT x856
ENDPROC
