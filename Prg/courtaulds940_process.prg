*!* m:\dev\prg\courtaulds940_process.prg

CD &lcpath

LogCommentStr = ""
delimchar = cDelimiter
lcTranOpt= cTranslateOption

lnNum = ADIR(tarray,"*.txt")

IF lnNum = 0
	WAIT WINDOW AT 10,10 "      No "+cCustname+" 940s to import     " TIMEOUT 3
	NormalExit = .T.
	THROW
ENDIF

FOR thisfile = 1  TO lnNum
	Xfile = lcpath+tarray[thisfile,1] && +"."
	cfilename = ALLTRIM(LOWER(tarray[thisfile,1]))
	nFileSize = VAL(TRANSFORM(tarray(thisfile,2)))
	c997File = ("f:\ftpusers\courtaulds\997out\"+cfilename)
	COPY FILE [&xfile] TO [&c997file]
	WAIT "" TIMEOUT 5
	WAIT WINDOW "Importing file: "+cfilename NOWAIT
	DO m:\dev\prg\loadedifile WITH Xfile,delimchar,lcTranOpt,cCustname

	SELECT x856
	LOCATE

	DO create_pt_cursors WITH cUseFolder,lLoadSQL  && Added to prep for SQL Server transition, replaces old cursor creation
	
	DO ("m:\dev\prg\"+cCustname+"940_bkdn")
	
	DO m:\dev\prg\all940_import

*	release_ptvars()
NEXT thisfile

&& now clean up the 940in archive files, delete for the upto the last 10 days
deletefile(lcarchivepath,IIF(DATE()<{^2016-05-01},20,10))

WAIT CLEAR

WAIT "Entire "+cCustname+" 940 Process Now Complete" WINDOW TIMEOUT 3
