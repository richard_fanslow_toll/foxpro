*!* Billabong 846 CREATOR

*Parameters nacctnum

cOffice ="L"

Public cWhse,cFilename,cFilename2,tsendto,tcc,tsendtoerr,tccerr,tsendtotest,tcctest,lIsError,lJoeMail
Public leMail,tfrom,lDoCatch
Close Data All
lTesting = .F. && If TRUE, disables certain functions
lUsePL = .F.
lIsError = .F.
On Error Throw
nAcctNum = 6718
cOffice = "L"
lDoCatch  = .T.
cErrMsg = ""
goffice=cOffice
If lTesting
	cIntUsage = "T"
Else
	cIntUsage = "P"
Endif

Do m:\dev\prg\_setvars With .T.

tfrom = "TGF Warehouse Operations <warehouse-ops@fmiint.com>"
*Set STEP ON
Try

	cOfficeUsed = cOffice
	xReturn = "XXX"
	Do m:\dev\prg\wf_alt With cOffice,nAcctNum
	If xReturn = "XXX"
		Wait Window "No Bldg. # Provided...closing" Timeout 3
		Return
	Endif

	If lTesting
		cUseFolder = "f:\whp\"
	Else
		cUseFolder = Upper(xReturn)
	Endif

	Dimension thisarray(1)
	cSuffix = ""
	cWhse = "MIRALOMA"

	lOverrideXcheck = .T. && If true, doesn't check ASN carton count against barcode scans.
	lHoldFile = .T. && If true, keeps output file in 846HOLD folder, otherwise into OUT
	leMail = .T.  && If .t., sends mail to WAREHOUSES

	cBarMiss = ""
	cASN = ""
	cUPC = ""
	nSTSets = 0
	cSpecChar = .F.
	XFERUPDATE = .T.
	nFilenum = 0
	lBackup = .F.
	lOverage = .F.
	lJoeMail = .T.
	lCopyCA = .T.
	cErrMsg = ""
	lIsError = .T.
	lXfer944 = .F.
	cTrk_WO = ""
	nCTNTotal = 0

*!* SET CUSTOMER CONSTANTS
	cCustName = "BILLABONG"  && Customer Identifier (for folders, etc.)
	cMailName = "BILLABONG "  && Proper name for eMail, etc.
	cZLAddress = ""
	cZLCity = ""
	cZLState = ""
	cZLZip = ""

	cCustPrefix = "BILLABONG-" && Customer Prefix (for output file names)
	cX12 = "005010"  && X12 Standards Set used
	crecid = "BBGTOLLPRD"  && Recipient EDI address

	dt1 = Ttoc(Datetime(),1)
	dt2 = Datetime()
	dtmail = Ttoc(Datetime())

	cFilestem = cCustPrefix+dt1+".txt"
	cFilename = "f:\ftpusers\billabong\846hold\"+cFilestem && Holds file here until complete and ready to send
	cFilename2 = "f:\ftpusers\billabong\846out\"+cFilestem
	cFilenameOUT= "f:\ftpusers\billabong\out\"+cFilestem
	cArchiveFile = "f:\ftpusers\billabong\846out\archive\"+cFilestem

	Store "" To c_CntrlNum,c_GrpCntrlNum
	nFilenum = Fcreate(cFilename)

set step On 


*!* SET OTHER CONSTANTS
	nCtnNumber = 1
	cString = ""
	csendqual = "ZZ"
	csendid = "TGFUSA"
	crecqual = "ZZ"
	cfd = "*"  && Field/element delimiter
	csegd = "~" && Line/segment delimiter
	nSegCtr = 0
	cterminator = ">" && Used at end of ISA segment
	cdate = Dtos(Date())
	ctruncdate = Right(cdate,6)
	ctime = Left(Time(Date()),2)+Substr(Time(Date()),4,2)
	cfiledate = cdate+ctime
	cOrig = "J"
	cdate = Dtos(Date())
	ctruncdate = Right(cdate,6)
	ctime = Left(Time(Date()),2)+Substr(Time(Date()),4,2)

** PAD ID Codes
	csendidlong = Padr(csendid,15," ")
	crecidlong = Padr(crecid,15," ")

*!* Serial number incrementing table
	If Used("serfile")
		Use In serfile
	Endif

Use ("F:\3pl\data\serial\billabong_846_serial.dbf") In 0 Alias serfile

*close data all 

xsqlexec("select * from upcmast where  accountid in (6718,6744,6747)",,,"wh")
xsqlexec("select * from inven where  accountid in (6718,6719,6720,6744,6747)",,,"wh")

**Use f:\whl\whdata\inven In 0

Select style,color,id,Space(13) as upc,Space(20) as brand, SPACE(2) as brandcode, totqty From inven Where INLIST(accountid, 6718,6744,6747) And units Into Cursor tinven ORDER BY accountid readwrite
SELECT tinven 
scan
		If '-' $(tinven.Style)
				pos1=Atc('-', tinven.Style)
				Replace  tinven.Style With  Substr(tinven.Style,1,pos1-1)
		Endif
endscan

Select tinven
xxii = Reccount("tinven")
xi=1
Scan
  Wait Window At 10,10 "Now updating record "+Transform(xi) +"  out of "+Transform(xxii)+" records" nowait

  Select upcmast
  locate for style= tinven.style and color= tinven.color and id= tinven.id
  If Found()
    replace tinven.upc With upcmast.upc
    replace tinven.brand With getmemodata("info","BRANDDESC")
    replace tinven.brandcode With getmemodata("info","BRANDCODE")
  Else

  Endif 
  xi=xi+1
Endscan 


* inprocess is a cursor of all outship records where they are not yet delivered and also excluding "notonwip"

*xsqlexec("select * from outship where accountid in (6744,6718) " + ;
  "and del_date={} and notonwip=0 and qty#0 order by wo_num","inprocess",,"wh")

xsqlexec("select units, style, color, id, upc, pack, totqty from outship, outdet " + ;
  "where outship.outshipid=outdet.outshipid " + ;
  "and outship.accountid in (6744,6718) " + ;
  "and del_date={} and notonwip=0 and qty#0","xdy",,"wh") 
  
select units, style, color, id, pack, sum(totqty) as pickedqty ;
  from xdy group by units, style, color, id, pack into cursor xunshipped


*!*  xsqlexec("select * from inven where mod='L' and accountid = 6718 ",,,"wh")
*!*  xsqlexec("select * from upcmast where  accountid in (6718,6719,6720)",,,"wh")
*!*  SELECT style,color,id,upc,info,SPACE(5) as brand FROM upcmast INTO CURSOR vupcmast READWRITE
*!*  replace all BRAND with getmemodata("info","BRANDDESC")
*!*  	Select wip_items
*!*  	Scan
*!*  		Select items
*!*  		Locate For Alltrim(items.upc) = Alltrim(wip_items.upc)
*!*  		If Found()
*!*  			Replace items.totqty With items.totqty + wip_items.total_units
*!*  			Replace items.wipqty With wip_items.total_units
*!*  		Else
*!*  			Insert Into items (upc,totqty) Values (wip_items.upc,wip_items.total_units)
*!*  			Replace items.wipqty With wip_items.total_units
*!*  		Endif
*!*  	Endscan


*!*  	Set Step On
*!*  	Select upc,Sum(totqty) As newqty From items Into Cursor newitems Group By upc

nSECount = 1

	Do num_incr
	Store "ISA"+cfd+"00"+cfd+"          "+cfd+"00"+cfd+"          "+cfd+csendqual+cfd+csendidlong+cfd+;
		crecqual+cfd+crecidlong+cfd+ctruncdate+cfd+ctime+cfd+"U"+cfd+"00501"+cfd+;
		PADL(c_CntrlNum,9,"0")+cfd+"0"+cfd+cIntUsage+cfd+cterminator+csegd To cString
	Do cstringbreak

	Store "GS"+cfd+"IB"+cfd+csendid+cfd+crecid+cfd+cdate+cfd+ctime+cfd+c_CntrlNum+;
		cfd+"X"+cfd+cX12+csegd To cString
	Do cstringbreak

	nSegCtr = 0
	nISACount = 1

	If nSECount = 1
		Store "ST"+cfd+"846"+cfd+Padl(c_GrpCntrlNum,9,"0")+csegd To cString
		Do cstringbreak
		nSegCtr = nSegCtr + 1

		Store "BIA"+cfd+"08"+cfd+"MM"+cfd+"FMM"+cfd+cdate+csegd To cString
		Do cstringbreak
		nSegCtr = nSegCtr + 1
		nSECount = 0
	Endif

	Select tinven
	lineCtr = 1

	Scan &&FOR indet.inwologid = inwolog.inwologid AND units
*    STORE "LX"+cfd+ALLTRIM(TRANSFORM(LineCtr))+csegd TO cString
*    DO cstringbreak
*    nSegCtr = nSegCtr + 1

		Store "LIN"+cfd+cfd+"UP"+cfd+ALLTRIM(tinven.upc)+csegd To cString
		Do cstringbreak
		nSegCtr = nSegCtr + 1

		Store "REF"+cfd+"3L"+cfd+Alltrim(Transform(tinven.brandcode))+csegd To cString
		Do cstringbreak
		nSegCtr = nSegCtr + 1

    xwipqty =0
    Select xdy
    Locate For upc = tinven.upc

    If Found("xdy")
       xwipqty = xdy.totqty
*      Store "QTY"+cfd+"14"+cfd+Alltrim(Transform(xdy.totqty))+csegd To cString
*      Do cstringbreak
*      nSegCtr = nSegCtr + 1
    Endif 
*    lineCtr = lineCtr +1

  	Store "QTY"+cfd+"17"+cfd+Alltrim(Transform(tinven.totqty+xwipqty))+csegd To cString
		Do cstringbreak
		nSegCtr = nSegCtr + 1

	Endscan

	Wait Window "END OF SCAN/CHECK PHASE ROUND..." Nowait

	nSTCount = 0

	Store "CTT"+cfd+Alltrim(Transform(lineCtr))+csegd To cString
	Do cstringbreak
	nSegCtr = nSegCtr + 1

	Store  "SE"+cfd+Alltrim(Str(nSegCtr+1))+cfd+Padl(c_GrpCntrlNum,9,"0")+csegd To cString
	Do cstringbreak
	nSegCtr = 0
	nSTSets = nSTSets + 1

	Store  "GE"+cfd+Alltrim(Str(nSTSets))+cfd+c_CntrlNum+csegd To cString
	Do cstringbreak

	Store  "IEA"+cfd+"1"+cfd+Padl(c_CntrlNum,9,"0")+csegd To cString
	Do cstringbreak

	=Fclose(nFilenum)


set step On 
	If !lTesting
		If !Used("ftpedilog")
			Select 0
			Use F:\edirouting\ftpedilog Alias ftpedilog
			Insert Into ftpedilog (ftpdate,filename,acct_name,Type) Values (dt2,cFilename,"cCustname","944")
			Use In ftpedilog
		ENDIF
		use f:\3pl\data\edi_trigger SHARED IN 0
		Select edi_trigger
	Endif

	Wait "846 Creation process complete:"+Chr(13)+cFilename Window At 45,60 Timeout 3
	cFin = "846 FILE "+cFilename2+" CREATED WITHOUT ERRORS"

*  Set Step On
	Copy File &cFilename To &cFilename2
	Copy File &cFilename To &cFilenameOUT

	If !lHoldFile And !lTesting
		Erase &cFilename
	Endif

	lDoCatch = .F.

Catch To oErr
	If lDoCatch And Empty(Alltrim(cErrMsg))
		Wait Window "At error catch block" Timeout 1
		Set Step On
		tsubject = "846 Error in "+cCustName
		tattach = " "
		tsendto = tsendtoerr
		tcc = tccerr

		tmessage = cCustName+" Error processing "+Chr(13)
		tmessage =tmessage+ "Try/CATCH Exeception Message:"+Chr(13)+;
			[  Error: ] + Str(oErr.ErrorNo) +Chr(13)+;
			[  Procedure: ] + oErr.Procedure +Chr(13)+;
			[  LineNo: ] + Str(oErr.Lineno) +Chr(13)+;
			[  Message: ] + oErr.Message +Chr(13)+;
			[  Details: ] + oErr.Details +Chr(13)+;
			[  StackLevel: ] + Str(oErr.StackLevel) +Chr(13)+;
			[  LineContents: ] + oErr.LineContents+Chr(13)+;
			[  UserValue: ] + oErr.UserValue
	Endif
Endtry

************************************************************************
*!* END OF MAIN PROGRAM
************************************************************************

****************************
Procedure close846
****************************
** Footer Creation
Return
****************************
Procedure cstringbreak
****************************
*  FPUTS(nFilenum,cString)
Fwrite(nFilenum,cString)
Return

****************************
Procedure num_incr
****************************
Select serfile
Replace serfile.seqnum With serfile.seqnum + 1 In serfile
Replace serfile.grpseqnum With serfile.grpseqnum + 1 In serfile
c_CntrlNum = Alltrim(Str(serfile.seqnum))
c_GrpCntrlNum = Alltrim(Str(serfile.grpseqnum))
Return

************************************************************************************************
