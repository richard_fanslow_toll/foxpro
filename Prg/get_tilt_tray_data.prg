parameters xwavenum

xsetdate=set("Date")
xsethours=set("Hours")
set Date YMD
set hours to 24

create cursor xdata (field1 c(64))

xsqlexec("select * from wavewo where wavenum = "+transform(xwavenum),"xwavewo",,"wh")
if eof("xwavewo")
	messagebox("Wave not found! Wave activation cancelled.",16,"Error")
else
	xerror=.f.

	select xwavewo
	scan
		xsqlexec("select * from labels where wo_num = "+transform(xwavewo.wo_num),,,"pickpack_sql5")
		if eof("labels")
			messagebox("Could not find cartonization data for wo "+transform(xwavewo.wo_num)+"! Wave activation cancelled.",16,"Error")
			xerror=.t.
			exit
		endif

		select labels
		scan
*			m.field1=Strtran(dtoc(Date()),"/","")+Padl(Transform(xwavenum),10,"0")+ucc+Padr(Alltrim(upc),14," ")+Padl(Alltrim(Transform(totqty)),7,"0")+"0000001"
			m.field1=Padl(Transform(xwavenum),10,"0")+ucc+Padr(Alltrim(upc),14," ")+Padl(Alltrim(Transform(totqty)),7,"0")+"0000001"
			insert into xdata from memvar
		endscan
		use in labels
	endscan
	
	if !xerror
		select xdata
		append blank
		replace field1 With "ZZZZ"+Padl(Alltrim(Transform(Reccount("xdata"))),7,"0")

		xdatetime = Strtran(Strtran(Strtran(ttoc(Datetime()),"/","")," ",""),":","")
		xfilename = "BCwave.txt."+xdatetime
		Copy To f:\ftpusers\bbc\tilttray_in\&xfilename Delimited With ""
		Copy To f:\ftpusers\bbc\tilttray_in\archive\&xfilename Delimited With ""
	endif
endif

use in xwavewo
use in xdata

set date &xsetdate
set hours to &xsethours



*!*	set Date YMD
*!*	lnThiswo_num = 6509792
*!*	Select ttwaveid
*!*	If ttwaveid.wavedate != Date()
*!*	  replace ttwaveid.wavedate With Date()
*!*	  replace ttwaveid.waveid With 1
*!*	Else
*!*	  replace waveid With waveid +1 
*!*	Endif 
*!*	xwave = ttwaveid.waveid 

*!*	useca("labels","pickpack_sql5",,,"select * from labels where wo_num = "+Transform(lnThiswo_num))
*!*	Select ucc,totqty,upc From labels Into Cursor xtray
*!*	Select xtray
*!*	Select Strtran(dtoc(Date()),"/","")+Padl(Transform(xwave),2,"0")+ucc+Padr(Alltrim(upc),14," ")+Padl(Alltrim(Transform(totqty)),7,"0")+"0000001" ;
*!*	From xtray Into Cursor temp readwrite

*!*	If !Used("ttwaveid")
*!*	  Use f:\ftpusers\data\ttwaveid In 0
*!*	Endif

*!*	Select temp
*!*	Append Blank
*!*	replace exp_1 With "ZZZZ"+Padl(Alltrim(Transform(Reccount("temp"))),7,"0")

*!*	xfilename = +Strtran(ttoc(Datetime()),"/","")
*!*	xfilename = Strtran(xfilename," ","")
*!*	xfilename = "f:\ftpusers\bbc\tilttray_in\BCwave"+Transform(lnThiswo_num)+".txt."+Strtran(xfilename,":","")
*!*	Copy To &xfilename Delimited With ""
