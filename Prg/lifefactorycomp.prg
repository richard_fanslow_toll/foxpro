* Lifefactory component & inventory check reports

utilsetup("LIFEFACTORYCOMP")

if holiday(date())
	return
endif

goffice="I"

xsqlexec("select * from inven where accountid=6034",,,"wh")
index on style tag style
set order to

xsqlexec("select * from pt where accountid=6304",,,"wh")
xsqlexec("select * from ptdet where accountid=6304",,,"wh")
index on ptid tag ptid
set order to

xsqlexec("select * from comphead where mod='I'","comphead",,"wh")
xsqlexec("select * from compon where mod='I'","compon",,"wh")

* component report

select ;
	comphead.style as masterstyle, ;
	00000000000 as canmake, ;
	comphead.compgrp, ;
	compon.style, ;
	000000 as baseqty, ;
	compon.compqty, ;
	compon.noinven, ;
	.f. as sub, ;
	000000 as availqty, ;
	000000 as buildableqty, ;
	000000 as totavailqty ;
	from comphead, compon ;
	where comphead.compheadid=compon.compheadid ;
	and comphead.accountid=6034 ;
	order by compgrp, masterstyle ;
	into cursor xrpt readwrite

select xrpt
scan 
	select inven
	locate for units and style=xrpt.style and color#"DAMAGE" and availqty>0
	if found()
		replace availqty with inven.availqty in xrpt
	endif
endscan

set step on 
select xrpt
scan for style = "110000"
	select inven
	locate for units and style=xrpt.style and color#"DAMAGE"
	if found()
		replace baseqty with inven.fillamt in xrpt
	endif
endscan

select masterstyle from xrpt group by 1 into cursor xtemp
scan 
	xcanmake=9999999
	select xrpt
	scan for masterstyle=xtemp.masterstyle and !noinven and compqty#0
		xcanmake = min(xcanmake,int(availqty/compqty))
	endscan
	assert xcanmake#9999999
	replace canmake with xcanmake in xrpt for masterstyle=xtemp.masterstyle
endscan

select * from xrpt into cursor xrpt2

select xrpt
scan 
	select xrpt2
	locate for masterstyle=xrpt.style
	if found()
		replace buildableqty with xrpt2.canmake, sub with .t. in xrpt
	endif
endscan

replace all totavailqty with availqty+buildableqty in xrpt

select masterstyle from xrpt where buildableqty>0 group by 1 into cursor xtemp
scan 
	xcanmake=99999
	select xrpt
	scan for masterstyle=xtemp.masterstyle and !noinven and compqty#0
		xcanmake = min(xcanmake,int(totavailqty/compqty))
	endscan
	replace canmake with xcanmake in xrpt for masterstyle=xtemp.masterstyle
endscan

select xrpt
set step on 
delete for noinven
copy to f:\ftpusers\lifefactory\reports\component.csv csv

* inventory check report

create cursor xrpt ( ;
	style c(20), ;
	color c(10), ;
	id c(10), ;
	onhandqty n(6), ;
	needqty n(6), ;
	shortqty n(6))

index on style+color+id tag match

m.shortqty=0
tt=0

select pt
scan for accountid=6034
	xptid=pt.ptid
	xcuft=0
	xweight=0

	select ptdet
	scan for ptid=xptid
		scatter memvar memo
		m.needqty=m.totqty

		if seek(m.style+m.color+m.id,"xrpt","match")
			replace needqty with xrpt.needqty+m.needqty in xrpt
		else
			m.onhandqty=0
			select inven
			locate for units=m.units and style=m.style and color=m.color and id=m.id and pack=m.pack
			m.onhandqty=inven.availqty
			insert into xrpt from memvar
		endif
	endscan
endscan

replace shortqty with needqty-onhandqty for needqty>onhandqty in xrpt

m.needqty=0

select inven
scan for accountid=6034
	if !seek(style+color+id,"xrpt","match")
		m.style=inven.style
		m.color=inven.color
		m.id=inven.id
		m.onhandqty=inven.totqty
		insert into xrpt from memvar
	endif
endscan

select xrpt
set step on 
copy to f:\ftpusers\lifefactory\reports\invcheck.csv csv

*!*	*

*!*	tsendto="al@lifefactory.com, chip@lifefactory.com"
*!*	*tsendto="dyoung@fmiint.com"
*!*	tattach = "h:\fox\component.csv,h:\fox\invcheck.csv"
*!*	tfrom ="FMI Grand Poo-Bah <support@fmiint.com>"
*!*	tmessage = "see attached files........"
*!*	tsubject = "FMI Component & Inventory Check reports"
*!*	do form dartmail2 with tsendto,tfrom,tsubject," ",tattach,tmessage,"A"

schedupdate()

_screen.caption=gscreencaption
on error
