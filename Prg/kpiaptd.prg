**Time from container Appointment to container delivery
**  - Full ocean containers Only
**  - Times based on wolog.appt (or if empty(appt), availlog.avail+3) Vs. wolog.pickedUp
**
Wait window "Gathering data for Appointment To Delivered Spreadsheet..." nowait noclear

dstartdt=xstartdt
denddt=xenddt

use f:\wo\wodata\wolog in 0

if empty(xaccountid)
	xacctfilter=".t."
else
	xacctfilter="accountid="+transform(xaccountid)
endif

xsqlexec("select wo_num, called, avail, remarks from availlog where "+xacctfilter+" and office='"+goffice+"'",,,"wo")
index on wo_num tag wo_num
set order to

Select w.*, w.accountid as acctNumFld, acctname as acctNameFld, Max(called) as lastCall, ;
	   called, Iif((emptynul(avail) and emptynul(called)), wo_date, Iif(Emptynul(avail), called, avail)) as avail ;
	from wolog w ;
		left join availlog a on w.wo_num = a.wo_num ;
	group by w.wo_num, a.called, a.avail, a.remarks ;
	order by w.acctname, w.wo_date ;
	where &xacctfilter and Between(w.delivered, xstartdt, xenddt) and w.office = goffice and type # "D" ;
  into cursor csrdetail

**availType: 0=avail, 1=wo_date, 2=last call date
Select c.*, Iif(IsNull(avail), 1, Iif(Empty(avail), 2, 0)) as availType, 00 as DateDiff ;
	from csrdetail c ;
		left join account a on a.accountid = c.accountid ;
	group by c.wo_num order by c.acctNameFld, c.wo_date ;
	where (IsNull(called) or called = lastCall) ;
  into cursor csrDetail readWrite

**for big lots (4717), if empty(appt), use avail+3 as is being sent in the 214
if xaccountid=4717
	**if available+3 is saturday or sunday, make it the monday
	replace all appt with iif(!emptynul(appt),appt,iif(!inlist(dow(avail+3),6,7),avail+3,iif(dow(avail+3)=6,avail+5,avail+4)))
endif

**calculate date differential into dateDiff - make necessary adjustments for weekends & FMI holidays
DO kpicalcDiff with "appt", "delivered", "dateDiff"

Locate
If Eof()
	strMsg = "No data found."
	=MessageBox(strMsg, 48, "Appointment To Delivered")
Else
	Wait window "Creating Appointment To Delivered Spreadsheet..." nowait noclear

	public oexcel
	oexcel = createobject("excel.application")

	oWorkbook=""
	DO xlsTimeGeneric with "", "delivered", "1", "", "Appointment To Delivered", "Delivered Date Vs. Appointment", ;
		" - Date range entered reflects container delivered dates.", "", "''", .t., .t.

	If lDetail
		Select csrTotals
		nWorksheet = 2
		Scan
			store tempfox+substr(sys(2015), 3, 10)+".xls" to ctmpfile
			Select *, Iif(type="A", awb, container) as field1 from csrDetail ;
				where Year(delivered) = csrTotals.year and Month(delivered) = csrTotals.month into cursor csrDet
			Copy to &cTmpFile fields acctNameFld, wo_num, type, field1, wo_date, appt, delivered, dateDiff type xl5

			DO xlsTimeGenDetail with cTmpFile, nWorkSheet, "H"

			nWorksheet = nWorksheet+1
			use in csrdet
		EndScan
	EndIf

	oWorkbook.Worksheets[1].range("A1").activate()
	oWorkbook.Save()
	oExcel.visible=.t.

	RELEASE oWorkbook, oExcel
EndIf


USE in wolog
USE in csrDetail
If Used("csrTotals")
  USE in csrTotals
EndIf