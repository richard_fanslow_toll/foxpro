if date()={11/28/17}
	close databases all
	set deleted on
	xsqlexec("select * from whoffice",,,"wh")
	xeomfilter1="half=.f."
	xeomfilter2=".t."
	xhalf=.f.
	xzmonth=dt2month(date()+8)
endif

xoffice=whoffice.office
goffice=xoffice
xmasteroffice=whoffice.rateoffice
gmasteroffice=whoffice.rateoffice
? xoffice

*

if xoffice="I"	&& dy 11/28/17 per Chris M

	wait window "Loading EOMUNDEL ("+xoffice+")..." nowait

	useca("eomundel","wh",,,,,,,,,,,,.t.)

	select eomundel
	scatter memvar memo blank
	m.date_rcvd=date()-1
	m.half=xhalf
	m.zmonth=xzmonth

	i=0

	xsqlexec("select * from outship where accountid=5726 and mod='"+goffice+"' and pulled=1 and notonwip=0 and del_date={} and "+xeomfilter2,,,"wh")
	
	scan
		i=i+1
		if i%1000=0
			tu("eomundel")
		endif

		xsqlexec("select * from outdet where mod='"+goffice+"' and outshipid="+transform(outship.outshipid)+" " + ;
			"and totqty>0 and (units=0 or "+transform(outship.picknpack)+")",,,"wh")
			
		scan
			scatter memvar
			select eomundel
			locate for zmonth=m.zmonth and accountid=m.accountid and units=m.units and style=m.style and color=m.color and id=m.id and pack=m.pack and half=m.half
			if !found()
				insertinto("eomundel","wh",.t.)
			else
				replace totqty with eomundel.totqty+m.totqty in eomundel
			endif
		endscan
	endscan

	tu("eomundel")
	
	use in eomundel
endif

*

wait window "Loading EOMINVEN ("+xoffice+")..." nowait

useca("eominven","wh")

xsqlexec("select * from inven where mod='"+goffice+"' and totqty>0",,,"wh")

select eominven
scatter memvar memo blank
m.half=xhalf
m.zmonth=xzmonth
m.office=xmasteroffice
m.mod=xoffice

i=0

select inven
scan for totqty>0 and &xeomfilter2
	i=i+1
	if i%1000=0
		tu("eominven")
	endif
	
	scatter memvar
	insertinto("eominven","wh",.t.)
endscan

? tu("eominven")

use in eominven
use in inven

*

wait window "Loading EOMINDET ("+xoffice+")..." nowait

useca("eomindet","wh")

xsqlexec("select * from indet where mod='"+goffice+"' and remainqty>0 and "+xeomfilter2,,,"wh")

select eomindet
scatter memvar memo blank
m.half=xhalf
m.zmonth=xzmonth
m.office=xmasteroffice
m.mod=xoffice

i=0

select indet
scan for remainqty>0 and &xeomfilter2
	i=i+1
	if i%1000=0
		tu("eomindet")
	endif
	
	scatter memvar
	m.totcube=min(99999,m.totcube)
	insertinto("eomindet","wh",.t.)
endscan

? tu("eomindet")

use in eomindet
use in indet

*

wait window "Loading EOMADJX ("+xoffice+")..." nowait

useca("eomadjx","wh")
 
xsqlexec("select * from adj where remainqty>0 and "+xeomfilter2+" and mod='"+xoffice+"'",,,"wh")

select eomadjx
scatter memvar memo blank
m.half=xhalf
m.zmonth=xzmonth
m.office=xmasteroffice
m.mod=xoffice

i=0

select adj
scan for inadj and remainqty>0 and &xeomfilter2
	i=i+1
	if i%1000=0
		wait window "Loading EOMADJX ("+xoffice+")  "+transform(i) nowait
		tu("eomadjx")
	endif
	
	scatter memvar
	insertinto("eomadjx","wh",.t.)
endscan

? tu("eomadjx")

use in eomadjx
use in adj

*

wait window "Loading EOMPT ("+xoffice+")..." nowait

useca("eompt","wh")

select eompt
scatter memvar memo blank
m.half=xhalf
m.zmonth=xzmonth

i=0

xsqlexec("select * from outship where mod='"+goffice+"' and pulled=1 and notonwip=0 and del_date={} and "+xeomfilter2+" and ctnqty#0",,,"wh")

scan
	i=i+1
	if i%1000=0
		tu("eompt")
	endif
	
	scatter memvar fiel accountid, outshipid, ship_ref, ctnqty, cuft
	m.unitqty=qty
	insertinto("eompt","wh",.t.)
endscan

? tu("eompt")

use in eompt

*

? "done"

email("Dyoung@fmiint.com","INFO: eominven is done for "+goffice,,,,,.t.,,,,,.t.,,.t.)

gunshot()