* Daily Receiving Log report for San Pedro
*
* 07/26/2017 MB
*
* Report on month-to-date per ME.
*
* per Erica Corea / Maria Estrella, run twice daily at 9am and 3pm Eastern time. 6am and 12pm west coast time.
*
* EXE = F:\AUTO\DAILYRECEIVINGLOGRPT.EXE
*
* added Gene Roebuck to email 2/22/2018 MB
*
* 4/25/2018 MB: changed my fmiint.com email to my Toll email.
*
* 4/30/2018 MB: added Taylor PeddyCord to the emails for Carson 2
*

LPARAMETERS tcOffice

LOCAL lcOffice	&& Optional parameter for Location
IF EMPTY(tcOffice) THEN
	lcOffice = 'C'	&& Default to San Pedro
ELSE
	lcOffice = UPPER(ALLTRIM(tcOffice))
	* Y is Carson2
ENDIF

runack("DAILYRECEIVINGLOGRPT")

IF HOLIDAY(DATE()) THEN
	schedupdate()
	RETURN
ENDIF

LOCAL lnError, lcProcessName, loDAILYRECEIVINGLOGRPT

DECLARE INTEGER CreateMutex IN Win32API ;
	STRING @lpMutexAttributes, ;
	STRING bInitialOwner, ;
	STRING @lpName

DECLARE INTEGER GetLastError IN Win32API

* if running in EXE check to see if already running
IF _VFP.STARTMODE > 0 THEN
	lcProcessName = "DAILYRECEIVINGLOGRPT"
	CreateMutex(0,CHR(1),@lcProcessName)
	lnError = GetLastError()
	IF lnError = 183
		WAIT WINDOW TIMEOUT 10 "DAILYRECEIVINGLOGRPT..."
		RETURN .F.
	ENDIF
ENDIF

utilsetup("DAILYRECEIVINGLOGRPT")


loDAILYRECEIVINGLOGRPT = CREATEOBJECT('DAILYRECEIVINGLOGRPT')
*loDAILYRECEIVINGLOGRPT.MAIN()
loDAILYRECEIVINGLOGRPT.MAIN(lcOffice) && Passing office to Main. -Chad 2/23/18

schedupdate()

CLOSE DATABASES ALL
RETURN

#DEFINE LOGIT 1
#DEFINE WAITIT 2
#DEFINE NOWAITIT 4
#DEFINE SENDIT 8
#DEFINE CRLF CHR(13) + CHR(10)
#DEFINE LINEFEED CHR(10)
#DEFINE RETURN_DATA_MANDATORY .F.
#DEFINE RETURN_DATA_NOT_MANDATORY .T.

DEFINE CLASS DAILYRECEIVINGLOGRPT AS CUSTOM

	cProcessName = 'DAILYRECEIVINGLOGRPT'

	lTestMode = .F.  && .F. FOR NORMAL OPERATION, .T. TO SUPPRESS ALL BUT DEVELOPER EMAILS AND CHANGE INPUT TABLE, ETC.

	lAutoYield = .T.

	cStartTime = TTOC(DATETIME())

	* processing properties
	cProcessDesc = "Month-To-Date Receiving Log Report"

	* date properties
	dtNow = DATETIME()

	dToday = DATE()

	cToday = DTOC(DATE())
	
	cOffice = lcOffice
	
	* Excel properties
	oExcel = NULL

	* wait window properties
	nWaitWindowTimeout = 2
	lWaitWindowIsOn = .T.

	* logfile properties
	lLoggingIsOn = .T.
	cLogFile = 'F:\UTIL\TRUCKING\LOGFILES\DAILYRECEIVINGLOGRPT_LOG.TXT'

	* INTERNAL email properties
	lSendInternalEmailIsOn = .T.
	cFrom = 'toll-edi-ops@tollgroup.com'
	cSendTo = 'mark.bennett@tollgroup.com'
	cCC = 'mark.bennett@tollgroup.com'
	cSubject = ''
	cAttach = ''
	cBodyText = ''
	cTopBodyText = 'See attached receiving log report.'

	cCOMPUTERNAME = ''
	cUSERNAME = ''

	FUNCTION INIT
		IF NOT DODEFAULT()
			RETURN .F.
		ENDIF
		WITH THIS
			CLEAR
			*SET RESOURCE OFF
			CLOSE DATA
			SET CENTURY ON
			SET DATE AMERICAN
			SET HOURS TO 24
			SET ANSI ON
			SET TALK OFF
			SET DELETED ON
			SET CONSOLE OFF
			SET EXCLUSIVE OFF
			SET SAFETY OFF
			SET EXACT OFF
			SET STATUS BAR ON
			SET SYSMENU OFF
			SET ENGINEBEHAVIOR 70
			_VFP.AUTOYIELD = .lAutoYield
			.cCOMPUTERNAME = GETENV("COMPUTERNAME")
			.cUSERNAME = GETENV("USERNAME")
			
			IF .lTestMode THEN	&& Override SendTo
				.cSendTo = 'mark.bennett@tollgroup.com'
				.cCC = ''
				.cLogFile = 'F:\UTIL\TRUCKING\DAILYRECEIVINGLOGRPT_LOG_TESTMODE.TXT'
			ENDIF
			.lLoggingIsOn = .lLoggingIsOn AND NOT EMPTY(.cLogFile)
		ENDWITH
	ENDFUNC


	FUNCTION DESTROY
		WITH THIS
			IF .lLoggingIsOn  THEN
				SET ALTERNATE OFF
				SET ALTERNATE TO
			ENDIF
			IF TYPE('.oExcel') = 'O' AND NOT ISNULL(.oExcel) THEN
				.oExcel.QUIT()
			ENDIF
		ENDWITH
		DODEFAULT()
	ENDFUNC


	FUNCTION MAIN
		LParameters cOffice

		WITH THIS
			LOCAL lnNumberOfErrors, ldToday, lcOutputFile, lcTemplateFile, loWorkbook, loWorksheet, lnRow, lcRow, ldFirstOfMonth
			
			TRY
				lnNumberOfErrors = 0
				.cOffice = cOffice

				IF .lLoggingIsOn THEN
					SET ALTERNATE TO (.cLogFile) ADDITIVE
					SET ALTERNATE ON
				ENDIF

				.cSubject = .cProcessDesc + ' for ' + TTOC(DATETIME())

				.TrackProgress(.cProcessDesc + ' started....', LOGIT+SENDIT+NOWAITIT)
				.TrackProgress('COMPUTERNAME = ' + .cCOMPUTERNAME, LOGIT+SENDIT)
				.TrackProgress('USERNAME = ' + .cUSERNAME, LOGIT+SENDIT)
				.TrackProgress('PROJECT = DAILYRECEIVINGLOGRPT', LOGIT+SENDIT)
				IF .lTestMode THEN
					.TrackProgress('======> TEST MODE <========', LOGIT+SENDIT)
				ENDIF
				
				.cCC = 'mark.bennett@tollgroup.com'
				DO CASE
					CASE .cOffice = 'Y'	&& Carson 2
						.cSendTo = 'Maria.Estrella@tollgroup.com;ivan.salinas@tollgroup.com;victor.suarez@tollgroup.com;esperanza.ruiz@tollgroup.com;daniela.delacruz@tollgroup.com;rocio.recinos@tollgroup.com'
						.cSendTo = .cSendTo + ';nancy.ramirez@tollgroup.com;jacqueline.rivera@tollgroup.com;flerida.donado@tollgroup.com;Laura.Romo@tollgroup.com;taylor.peddycord@tollgroup.com'
					OTHERWISE	&& All Others
						.cSendTo = 'Gene.Roebuck@tollgroup.com;Erica.Corea@tollgroup.com;Louie.Ybarra@tollgroup.com;Jorge.Ocampo@tollgroup.com;Yesenia.Valencia@tollgroup.com;alma.navarro@tollgroup.com'
				ENDCASE
				DO CASE
					CASE .lTestMode 	&& Override SendTo and CC
						.cSendTo = 'mark.bennett@tollgroup.com'
						.cCC = ''
				ENDCASE
				
				ldToday = .dToday
				
				ldFirstOfMonth = ldToday - DAY(ldToday) + 1
				
				
				USE F:\WO\WODATA\WOLOG IN 0
			
				*xsqlexec("select office, mod, acctname, inwonum, ltwonum, container, status,skus, cartons, newdt from inrcv where office = 'C' AND newdt = {" + DTOC(ldToday) + "}","inrcv",,"wh")
				*xsqlexec("select office, mod, acctname, inwonum, ltwonum, container, status,skus, cartons, newdt from inrcv where office = 'C' AND newdt >= {" + DTOC(ldFirstOfMonth) + "}","inrcv",,"wh")
				*2/27/18-Chad
				cOtherCriteria = ".T."
				DO CASE
					CASE .cOffice = "Y"	&& If for Carson2, only get 2XU, Age Group, Nanjin PNP, Nanjing PPK, Steel Series, TJM
						cOtherCriteria = "AccountID IN (6665,1285,4610,4694,6612,6751)"
				ENDCASE
				
				xsqlexec("select office, mod, acctname, inwonum, ltwonum, container, status,skus, cartons, newdt from inrcv "+;
				"where office = '" + .cOffice + "' AND newdt >= {" + DTOC(ldFirstOfMonth) + "} and " + cOtherCriteria,"inrcv",,"wh")
				
				SELECT *, {} AS PICKEDUP ;
				FROM inrcv ;
				INTO CURSOR inrcv2 ;
				ORDER BY newdt, ACCTNAME, MOD ;
				READWRITE
				
				* look up In Yard Date (pickedup date?) in Wolog
				SELECT inrcv2
				SCAN FOR NOT EMPTY(ltwonum)
					SELECT WOLOG
					LOCATE FOR WO_NUM = inrcv2.ltwonum
					IF FOUND() THEN
						IF NOT ISNULL(WOLOG.pickedup) THEN
							REPLACE inrcv2.pickedup WITH WOLOG.pickedup IN inrcv2
						ENDIF
					ENDIF
				ENDSCAN
				
				lcTemplateFile = 'F:\UTIL\TRUCKING\TEMPLATES\receiving_log_template.xls'
				SET DATE YMD
				lcOutputFile = 'F:\UTIL\TRUCKING\REPORTS\RECEIVING_LOG_' + STRTRAN(STRTRAN(STRTRAN(TTOC(DATETIME()),"/")," "),":") + '.XLS'
												
				* OPEN Excel
				.oExcel = CREATEOBJECT("excel.application")
				.oExcel.displayalerts = .F.
				.oExcel.VISIBLE = .F.

				loWorkbook = .oExcel.workbooks.OPEN(lcTemplateFile)
				loWorkbook.SAVEAS(lcOutputFile)
				loWorksheet = loWorkbook.worksheets[1]
				loWorksheet.SELECT()
				*SET STEP ON 
				
				lnRow = 1
				SELECT inrcv2
				SCAN
					lnRow = lnRow + 1
					lcRow = LTRIM(STR(lnRow))
					loWorksheet.RANGE("A" + lcRow).VALUE = inrcv2.office
					loWorksheet.RANGE("B" + lcRow).VALUE = inrcv2.mod
					loWorksheet.RANGE("C" + lcRow).VALUE = inrcv2.acctname
					loWorksheet.RANGE("D" + lcRow).VALUE = inrcv2.inwonum
					loWorksheet.RANGE("E" + lcRow).VALUE = inrcv2.ltwonum
					loWorksheet.RANGE("F" + lcRow).VALUE = "'" + inrcv2.container
					loWorksheet.RANGE("G" + lcRow).VALUE = inrcv2.status
					loWorksheet.RANGE("H" + lcRow).VALUE = inrcv2.skus
					loWorksheet.RANGE("I" + lcRow).VALUE = inrcv2.cartons
					loWorksheet.RANGE("J" + lcRow).VALUE = inrcv2.newdt
					IF NOT EMPTYNUL(inrcv2.pickedup) THEN
						loWorksheet.RANGE("K" + lcRow).VALUE = inrcv2.pickedup
					ENDIF				
				ENDSCAN

				loWorkbook.SAVE()	
				
				.oExcel.Quit()

				.cAttach = lcOutputFile

				.TrackProgress(.cProcessDesc + ' ended normally....', LOGIT+SENDIT+NOWAITIT)

			CATCH TO loError

				.TrackProgress('There was an error.',LOGIT+SENDIT)
				.TrackProgress('The Error # is: ' + TRANSFORM(loError.ERRORNO), LOGIT+SENDIT)
				.TrackProgress('The Error Message is: ' + TRANSFORM(loError.MESSAGE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred in Program: ' + TRANSFORM(loError.PROCEDURE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred at line #: ' + TRANSFORM(loError.LINENO), LOGIT+SENDIT)
				lnNumberOfErrors = lnNumberOfErrors + 1
				IF TYPE('.oExcel') = 'O' AND NOT ISNULL(.oExcel) THEN
					.oExcel.QUIT()
				ENDIF

			ENDTRY

			WAIT CLEAR
			***************** INTERNAL email results ******************************
			.TrackProgress('About to send status email.',LOGIT)
			.TrackProgress('==================================================================================================================', SENDIT)
			.TrackProgress('The logfile is: ' + .cLogFile, LOGIT+SENDIT)
			.TrackProgress(.cProcessDesc + ' started: ' + .cStartTime, LOGIT+SENDIT)
			.TrackProgress(.cProcessDesc + ' finished: ' + TTOC(DATETIME()), LOGIT+SENDIT)

			IF .lSendInternalEmailIsOn THEN
				* try to trap error from not having dartmail dll's registered on user's pc...
				TRY
					.cBodyText = .cTopBodyText + CRLF + CRLF + "------------------------------------" + CRLF + .cBodyText
					
					emailx(.cSendTo,.cCC,.cSubject,.cBodyText,,.cAttach)
					
					*DO FORM dartmail2 WITH .cSendTo,.cFrom,.cSubject,.cCC,.cAttach,.cBodyText,"A"
					.TrackProgress('Sent status email.',LOGIT)
				CATCH TO loError
					.TrackProgress('There was an error sending the status email.',LOGIT)
					.TrackProgress('The Error # is: ' + TRANSFORM(loError.ERRORNO), LOGIT)
					.TrackProgress('The Error Message is: ' + TRANSFORM(loError.MESSAGE), LOGIT)
					.TrackProgress('The Error occurred in Program: ' + TRANSFORM(loError.PROCEDURE), LOGIT)
					.TrackProgress('The Error occurred at line #: ' + TRANSFORM(loError.LINENO), LOGIT)
					lnNumberOfErrors = lnNumberOfErrors + 1
				ENDTRY

			ELSE
				.TrackProgress('Did not send status email: THIS.lSendInternalEmailIsOn = FALSE.',LOGIT)
			ENDIF

		ENDWITH
		RETURN
	ENDFUNC && main
	
	
		
	
	PROCEDURE TrackProgress
		* do any combination of Wait Window, writing to logfile, and adding to body of email,
		* based on nFlags parameter.
		LPARAMETERS tcExpression, tnFlags
		WITH THIS
			IF BITAND(tnFlags,LOGIT) = LOGIT THEN
				IF .lLoggingIsOn THEN
					?
					? .cProcessName + "  " + TTOC(DATETIME()) + ": " + tcExpression
				ENDIF
			ENDIF
			IF .lWaitWindowIsOn THEN
				IF BITAND(tnFlags,NOWAITIT) = NOWAITIT THEN
					WAIT WINDOW tcExpression NOWAIT
				ENDIF
				IF BITAND(tnFlags,WAITIT) = WAITIT THEN
					WAIT WINDOW tcExpression TIMEOUT .nWaitWindowTimeout
				ENDIF
			ENDIF
			IF BITAND(tnFlags,SENDIT) = SENDIT THEN
				IF .lSendInternalEmailIsOn THEN
					.cBodyText = .cBodyText + tcExpression + CRLF + CRLF
				ENDIF
			ENDIF
		ENDWITH
	ENDPROC  &&  TrackProgress
	

ENDDEFINE
