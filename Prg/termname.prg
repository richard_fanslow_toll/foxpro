lparameter xterminal 

if !used("terminal")
	xsqlexec("select * from terminal",,,"wo")
	index on terminal tag terminal
	set order to
endif

if seek(left(xterminal,23),"terminal","terminal")
	return trim(terminal.city)
else
	return Space(30)
endif