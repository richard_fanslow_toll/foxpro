utilsetup("BBC_UPS_BACKFILL")
SET EXCLUSIVE OFF 
SET DELETED ON 
SET TALK OFF  
SET SAFETY OFF   &&Allows for overwrite of existing files

close databases all
goffice="L"
gmasteroffice='L'
useca("shipment","wh",,,"select * from shipment where accountid in (6757,16757,26757)and adddt >={"+dtoc(date()-10)+"}")
useca("package","wh",,,"select * from package where accountid in (6757,16757,26757)and adddt >={"+dtoc(date()-10)+"}")
*useca("ufclose","wh",,,"select * from ufclose where accountid in (6757,16757,26757) and adddt >={"+dtoc(date()-10)+"}")
SET STEP ON 
use f:\whl\whdata\outship IN 0
SELECT a.carrier,a.accountid,pickticket,shipdate, trknumber,pkgnum FROM shipment a , package b WHERE a.shipmentid=b.shipmentid INTO CURSOR c1 READWRITE
DELETE FOR EMPTY(trknumber)
SELECT * FROM c1 WHERE pkgnum=1 INTO CURSOR c1a READWRITE
SELECT carrier,accountid, pickticket,shipdate, MAX(trknumber) as bol FROM c1a GROUP BY 1,2,3,4 INTO CURSOR c2a READWRITE
SELECT  * FROM  c2a a LEFT JOIN outship b ON pickticket=ship_ref INTO CURSOR c3 READWRITE
SELECT * FROM c3 WHERE EMPTY(del_date) INTO CURSOR c5 READWRITE
*SELECT * FROM c5 a LEFT JOIN ufclose b ON a.accountid_a = b.accountid AND a.shipdate=b.closedt AND a.carrier=b.carrier INTO CURSOR c6 READWRITE
SELECT * FROM c5 INTO CURSOR c6 readwrite
*DELETE FOR ISNULL(acctcode) IN c6
SET STEP ON[ 
select c6
scan
select outship
locate for ship_ref=c6.pickticket
if found("outship")
replace  bol_no with c6.bol in outship 
replace del_date with c6.shipdate in outship 
replace blenterby with "BACK" in outship 
endif
ENDSCAN

SELECT c6
if reccount() > 0
	export to "F:\FTPUSERS\BBCRW\reports\backfill"  type xl5
	tsendto = "tmarg@fmiint.com,todd.margolin@tollgroup.com"
	tattach = "F:\FTPUSERS\BBCRW\reports\backfill.xls"
	tcc =""
	tfrom ="Toll EDI System Operations <fmi-transload-ops@fmiint.com>"
	tmessage = "BBC backfill:  "+dtoc(date()-1)
	tsubject = "BBC backfill"
	do form m:\dev\frm\dartmail2 with tsendto,tfrom,tsubject, tcc,tattach,tmessage  ,"A"
ELSE
	tsendto = "tmarg@fmiint.com,todd.margolin@tollgroup.com"
	tattach = ""
	tcc =""
	tfrom ="Toll EDI System Operations <fmi-transload-ops@fmiint.com>"
	tmessage = "NO BBC backfill:  "+dtoc(date()-1)
	tsubject = "NO BBC backfill"
	do form m:\dev\frm\dartmail2 with tsendto,tfrom,tsubject, tcc,tattach,tmessage  ,"A"
endif


schedupdate()
_screen.Caption=gscreencaption
on error

