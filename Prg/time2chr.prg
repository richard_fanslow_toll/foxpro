lparameter xtime, xoldstyle

xhh=left(xtime,2)
xmm=substr(xtime,4,2)

do case
case xhh="12"
	xreturn = xhh+xmm+"P"
case xhh="00"
	xreturn = "12"+xmm+"A"
case xhh<"12"
	xreturn = xhh+xmm+"A"
otherwise
	xreturn = trans(val(xhh)-12,"@L 99")+xmm+"P"
endcase

if xoldstyle
	xreturn = left(xreturn,2)+":"+lower(substr(xreturn,3))
endif

return xreturn