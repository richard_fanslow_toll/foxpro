PARAMETERS lnAcctnum,llIssuesOnly,llToMeOnly

utilsetup("CHECK945")

guserid="check945"
gsystemmodule="check945"

*!*	IF INLIST(DOW(DATE()),1,7)
*!*		WAIT WINDOW "Weekend...no reports run" TIMEOUT 2
*!*		RETURN
*!*	ENDIF

CLOSE DATA ALL
IF EMPTY(lnAcctnum)
	WAIT WINDOW "No account ID parameter" TIMEOUT 2
	RETURN
ENDIF

DO m:\dev\prg\_setvars WITH .T.

ON ESCAPE CANCEL
ON ERROR
*ON ERROR DO fmerror WITH ERROR(), PROGRAM(), LINENO(1), SYS(16),, .T.
SET HOURS TO 24
PUBLIC tcc
lookups()

lLoadSQL = .T.
lTesting = .f.


tFrom ="TGF EDI System Operations <toll-transload-ops@fmiint.com>"
tcc = " "

*gsystemmodule="PL"

SELECT 0

xsqlexec("select * from account where inactive=0","account",,"qq")
INDEX ON accountid TAG accountid
SET ORDER TO

IF SEEK(lnAcctnum,'account','accountid')
	cAcctName = ALLTRIM(account.acctname)
	WAIT WINDOW "Checking account "+cAcctName TIMEOUT 2
ENDIF
USE IN account

* like 4677,t,t

IF VARTYPE(llIssuesOnly) = "U"
	llIssuesOnly = .F.
ELSE
	IF VARTYPE(llIssuesOnly) = "C"
		IF UPPER(llIssuesOnly) = "T"
			llIssuesOnly = .T.
		ELSE
			llIssuesOnly = .F.
		ENDIF
	ENDIF
ENDIF

IF VARTYPE(llToMeOnly) = "U"
	llToMeOnly = .F.
ELSE
	IF VARTYPE(llToMeOnly) = "C"
		IF UPPER(llToMeOnly) = "T"
			llToMeOnly = .T.
		ELSE
			llToMeOnly = .F.
		ENDIF
	ENDIF
ENDIF

thisuser=getuserid()

IF llToMeOnly
	DO CASE
		CASE UPPER(thisuser) = "JBIANCHI"
			tsendto = "joe.bianchi@tollgroup.com"
		CASE UPPER(thisuser) = "PAULG"
			tsendto = "pgaidis@fmiint.com"
		OTHERWISE
			tsendto = "pgaidis@fmiint.com"
	ENDCASE
	WAIT WINDOW AT 10,10 "Report going to "+tsendto TIMEOUT 1
ELSE
	WAIT WINDOW AT 10,10 "Report going to default addresses" TIMEOUT 1
ENDIF

cCoreSendGroup = "pgaidis@fmiint.com,joe.bianchi@tollgroup.com"
lnAcctnum= VAL(TRANSFORM(lnAcctnum))
doemail = .T.

DO CASE
	CASE lnAcctnum = 4610
		accountnumber = 4610
		accountnumber2 = 4694  && 4694
		accountnumber3 = 4610  && 4694
	CASE lnAcctnum = 4859
		accountnumber = 4859
		accountnumber2 = 4859
		accountnumber3 = 4859
	CASE lnAcctnum = 4752
		accountnumber = 4752
		accountnumber2 = 5318
		accountnumber3 = 4752
	CASE lnAcctnum = 4677
		accountnumber = 4677
		accountnumber2 = 4677
		accountnumber3 = 4677
	CASE lnAcctnum = 4650
		accountnumber = 4650
		accountnumber2 = 5023
		accountnumber3 = 4650
	CASE lnAcctnum = 5383
		accountnumber = 5383
		accountnumber2 = 5383
		accountnumber3 = 5383
	CASE lnAcctnum = 5452
		accountnumber = 5452
		accountnumber2 = 5452
		accountnumber3 = 5452
	CASE lnAcctnum = 5446
		accountnumber = 5446
		accountnumber2 = 5446
		accountnumber3 = 5446
ENDCASE

exceptionsonly = .T.

DO CASE
	CASE INLIST(accountnumber,&gnanjingaccounts)
		cOffice="Y"
		goffice="Y"
		lcWHPath="Y"
		gUseAcct1 = gnanjingaccounts
		tsubject= "Nanjing 945-EDI Status Report"
		lcAcctList = "4610,4694"
		lcRptTitle ="Nanjing USA - 940/945-EDI Status Report"
		IF llToMeOnly = .F.
			tsendto = cCoreSendGroup  && +",cary@nanjingusa.com"
		ENDIF
	CASE INLIST(accountnumber,&gCourtauldsaccounts)
		cOffice="M"
		goffice="M"
		lcWHPath="M"
		gUseAcct1 = gCourtauldsaccounts
		tsubject= "Courtaulds 940/945-EDI Status Report"
		lcAcctList = "4677"
		lcRptTitle ="Courtaulds - 940/945-EDI Status Report"
		IF llToMeOnly = .F.
			tsendto = cCoreSendGroup
		ENDIF
	CASE INLIST(accountnumber,&gtkohighlineaccounts)
		cOffice="C"
		goffice="2"
		lcWHPath="2"
		gUseAcct1 = gtkohighlineaccounts
		tsubject= "Modern Shoe 940/945-EDI Status Report"
		lcRptTitle ="Modern Shoe - 940/945-EDI Status Report"
		lcAcctList = gtkohighlineaccounts
		IF llToMeOnly = .F.
			tsendto = cCoreSendGroup+",RJ@highlineunited.com,byron@modernshoe.net,linda@modernshoe.net,sarah@highlineunited.com,donna@modernshoe.net."
		ENDIF
ENDCASE


SET EngineBehavior 70

*cOffice = IIF(accountnumber = 4677,"M","C")	&& fixed dy 3/27/17
*lcWHPath = wf(cOffice,accountnumber)
cUseFolder = "f:\wh"+goffice+"\whdata"

xsqlexec("select * from outship where mod = '"+goffice+"' and accountid = "+TRANSFORM(accountnumber)+" and wo_date >= {"+DTOC(DATE()-10)+"}",,,"wh")
INDEX on STR(accountid,4)+ship_ref TAG acctpt
DELETE FOR ISNULL(outship.del_date)

DO create_pt_cursors WITH cUseFolder,lLoadSQL && Added to prep for SQL Server transition

IF lLoadSQL
	lcQuery = [select * from pt where mod=']+goffice+[' and accountid in (]+gUseAcct1+[)]
	xsqlexec(lcQuery,,,"wh")
ELSE
	USE cUseFolder+"PT" IN 0 ALIAS pt
ENDIF
lntime = 60

*!*	IF !USED('pthist')
*!*		cEDIFolder = "F:\3PL\DATA\"
*!*		USE (cEDIFolder+"PTHIST") IN 0 ALIAS pthist
*!*	ENDIF

SET CENTURY ON
cAcctNum = ALLTRIM(STR(accountnumber))
SET DATE YMD
dDate = DATE()-10
dDate2 = STRTRAN(DTOC(dDate),"/","-")
lcQuery = [select * from pthist where mod=']+goffice+[' and pthist.accountid in (]+gUseAcct1+[) and pthist.dateloaded > ']+dDate2+[']
xsqlexec(lcQuery,,,"stuff")
SET CENTURY OFF

SELECT pthist

SELECT *, {  /  /  } AS d_date,{  /  /  } AS p_date,{  /  /  } AS l_date,{  /  /  } AS c_date,;
	{  /  /  } AS a_date,"                   " AS bol, "    " AS scaccode, 0000000 AS wo_num,{  /  /  } AS t_date,;
	"                                           " AS file945,{  /  /  } AS trigdate,{  /  /  } AS b_date,;
	{  /  /  } AS s_date,"                   " AS edistat,{  /  /  } AS proc_date, 00000 AS unitqty, 0000 AS ctnqty;
	FROM pthist ;
	WHERE INLIST(accountid,&lcAcctList) ;
	AND  !INLIST(edistatus,"945 CREATED","CANCELED","NOTHING SHIPPED","MANUALLY REMOVED","NO 945 POSSIBLE") ;
	GROUP BY pt ;
	INTO CURSOR pts READWRITE

*BROWSE

IF !USED('edi_trigger')
	cEDIFolder = "F:\3PL\DATA\"
	USE (cEDIFolder+"edi_trigger") IN 0 ALIAS edi_trigger
ENDIF
SELECT edi_trigger
SET ORDER TO acct_pt  &&SHIP_REF   && SHIP_REF

SELECT pts
SCAN
	WAIT WINDOW AT 10,10 "Checking "+pt NOWAIT
	SELECT edi_trigger
	SEEK ALLTRIM(STR(accountnumber))+ALLTRIM(pts.pt)
	IF !FOUND()
		SEEK STR(accountnumber2,4,0)+pts.pt
	ENDIF
	IF !FOUND()
		SEEK STR(accountnumber3,4,0)+pts.pt
	ENDIF

	IF FOUND()
		REPLACE pts.confirmed WITH .T.
		lcfname = JUSTFNAME(edi_trigger.file945)
		REPLACE pts.file945  WITH lcfname
		REPLACE pts.trigdate  WITH edi_trigger.trig_time

		REPLACE pts.proc_date WITH edi_trigger.when_proc
		REPLACE pts.edistat  WITH edi_trigger.fin_status

		SELECT pthist
		LOCATE FOR accountid = INT(VAL(STR(accountnumber2,4,0))) AND pthist.pt = pts.pt
		IF !FOUND()
			LOCATE FOR accountid = INT(VAL(STR(accountnumber2,4,0))) AND pthist.pt = pts.pt
		ENDIF
		IF !FOUND()
			LOCATE FOR accountid = INT(VAL(STR(accountnumber3,4,0))) AND pthist.pt = pts.pt
		ENDIF
		IF FOUND()
			DO WHILE INLIST(pthist.accountid,&lcAcctList) AND pthist.pt=pts.pt
				REPLACE pthist.edistatus WITH edi_trigger.fin_status IN pthist
				SKIP 1 IN pthist
			ENDDO
		ENDIF

		SELECT outship
		SEEK STR(accountnumber,4,0)+pts.pt
		IF !FOUND()
			SEEK STR(accountnumber2,4,0)+pts.pt
		ENDIF
		IF !FOUND()
			SEEK STR(accountnumber3,4,0)+pts.pt
		ENDIF

		IF FOUND()
			REPLACE pts.wo_num WITH wo_num
			REPLACE pts.d_date WITH nul2empty(del_date)
			REPLACE pts.p_date WITH nul2empty(picked)
			REPLACE pts.l_date WITH nul2empty(labeled)
			REPLACE pts.s_date WITH nul2empty(staged)
			REPLACE pts.a_date WITH nul2empty(appt)
			REPLACE pts.b_date WITH nul2empty(CANCEL)
			REPLACE pts.bol    WITH bol_no
			REPLACE pts.scaccode WITH scac
			REPLACE pts.unitqty  WITH qty
			REPLACE pts.ctnqty   WITH ctnqty
			IF EMPTY(pts.d_date)
				REPLACE pts.edistat WITH "WO Not Complete"
			ENDIF
		ELSE
			SELECT pt
			LOCATE FOR accountid = accountnumber AND pt.ship_ref = pts.pt
			IF !FOUND()
				LOCATE FOR accountid = accountnumber2 AND pt.ship_ref = pts.pt
			ENDIF
			IF !FOUND()
				LOCATE FOR accountid = accountnumber3 AND pt.ship_ref = pts.pt
			ENDIF
			IF FOUND()
				REPLACE pts.unitqty  WITH pt.qty
			ENDIF
		ENDIF
	ELSE
		SELECT outship
		SEEK STR(accountnumber,4,0)+pts.pt
		IF !FOUND()
			SEEK STR(accountnumber2,4,0)+pts.pt
		ENDIF
		IF !FOUND()
			SEEK STR(accountnumber3,4,0)+pts.pt
		ENDIF
		IF FOUND()
			SET STEP ON 
			REPLACE pts.wo_num WITH outship.wo_num
			REPLACE pts.d_date WITH nul2empty(outship.del_date)
			REPLACE pts.p_date WITH nul2empty(outship.picked)
			REPLACE pts.l_date WITH nul2empty(outship.labeled)
			REPLACE pts.s_date WITH nul2empty(outship.staged)
			REPLACE pts.a_date WITH nul2empty(outship.appt)
			REPLACE pts.b_date WITH nul2empty(outship.CANCEL)
			REPLACE pts.bol    WITH outship.bol_no
			REPLACE pts.scaccode WITH outship.scac
			REPLACE pts.unitqty  WITH outship.qty
			REPLACE pts.ctnqty   WITH outship.ctnqty
			IF pts.unitqty = pts.ctnqty
				REPLACE pts.ctnqty   WITH outship.ctnqty
				REPLACE pts.edistat WITH "WO Not Processed"
			ELSE
				IF EMPTY(pts.d_date)
					REPLACE pts.edistat WITH "WO Not Complete"
				ELSE

				ENDIF
			ENDIF
		ELSE
			SELECT pt
			LOCATE FOR accountid = accountnumber AND pt.ship_ref = pts.pt
			IF !FOUND()
				LOCATE FOR accountid = accountnumber2 AND pt.ship_ref = pts.pt
			ENDIF
			IF !FOUND()
				LOCATE FOR accountid = accountnumber3 AND pt.ship_ref = pts.pt
			ENDIF
			IF FOUND()
				REPLACE pts.unitqty WITH pt.qty IN pts
				REPLACE pts.edistat WITH "On PT Screen" IN pts
				REPLACE pts.b_date  WITH nul2empty(pt.CANCEL) IN pts
			ELSE
				REPLACE pts.edistat WITH "Not in System" IN pts
			ENDIF
		ENDIF
	ENDIF
	lcfname = JUSTFNAME(pts.filename)
	REPLACE pts.filename WITH lcfname

ENDSCAN

SELECT pts
INDEX ON pt TAG pt
SET ORDER TO pt


SELECT edi_trigger
SET ORDER TO acct_pt

SELECT pthist

SELECT pts
LOCATE

SCAN
	WAIT WINDOW AT 10,10 "Checking "+pt NOWAIT

	SELECT edi_trigger
	SEEK ALLTRIM(STR(accountnumber))+ALLTRIM(pts.pt)
	IF !FOUND()
		SEEK STR(accountnumber2,4,0)+pts.pt
	ENDIF
	IF !FOUND()
		SEEK STR(accountnumber3,4,0)+pts.pt
	ENDIF

	IF FOUND()
		SELECT pthist
		LOCATE FOR accountid = accountnumber AND pthist.pt = pts.pt
		IF !FOUND()
			LOCATE FOR accountid = accountnumber2 AND pthist.pt = pts.pt
		ENDIF
		IF !FOUND()
			LOCATE FOR accountid = accountnumber3 AND pthist.pt = pts.pt
		ENDIF
		IF FOUND()
			REPLACE pthist.edistatus WITH edi_trigger.fin_status IN pthist
			WAIT WINDOW AT 10,10 "Updating "+pt NOWAIT
		ENDIF
	ENDIF

	IF pts.unitqty =0 AND pts.ctnqty =0
		SELECT pthist
		LOCATE FOR accountid = accountnumber AND pthist.pt = pts.pt
		IF !FOUND()
			LOCATE FOR accountid = accountnumber2 AND pthist.pt = pts.pt
		ENDIF
		IF !FOUND()
			LOCATE FOR accountid = accountnumber3 AND pthist.pt = pts.pt
		ENDIF
		IF FOUND()
			DO WHILE pthist.accountid=accountnumber AND pthist.pt=pts.pt
				REPLACE pthist.edistatus WITH "NOTHING SHIPPED"
				SKIP 1 IN pthist
			ENDDO
		ENDIF
	ENDIF
ENDSCAN

SELECT pts

WAIT WINDOW AT 10,10 "Deleting where 945 was created............" NOWAIT
DELETE FOR edistat= "MANUALLY REMOVED"

WAIT WINDOW AT 10,10 "Deleting where 945 can't be created............" NOWAIT
DELETE FOR edistat= "NO 945 POSSIBLE"

IF exceptionsonly = .T. AND llIssuesOnly=.T.
	DELETE FOR (unitqty =0 AND ctnqty =0)
ENDIF

IF exceptionsonly = .T.
	WAIT WINDOW AT 10,10 "Deleting where 945 was created............" NOWAIT
	DELETE FOR edistat="945 CREATED"
ENDIF

IF exceptionsonly = .T. AND llIssuesOnly= .F.
	WAIT WINDOW AT 10,10 "Deleting where Nothing was shipped..........." NOWAIT
	DELETE FOR edistat= "NOTHING SHIPPED"
	WAIT WINDOW AT 10,10 "Deleting PTS not in system..........." NOWAIT
	DELETE FOR edistat= "Not in System"
ENDIF

IF exceptionsonly = .T. AND llIssuesOnly = .T.
	WAIT WINDOW AT 10,10 "Deleting PTs only on the PT Screen............" NOWAIT
	DELETE FOR edistat= "On PT Screen"
ENDIF

SELECT pts
SCAN
	WAIT WINDOW AT 10,10 "Checking SCAC Issues.............." NOWAIT
	IF !EMPTY(d_date) AND EMPTY(pts.trigdate)
		REPLACE pts.edistat WITH "No Trig.Ck SCAC"
	ENDIF
ENDSCAN

IF llIssuesOnly = .T.
	WAIT WINDOW AT 10,10 "Deleting non-issues.............." NOWAIT
	DELETE FOR edistat= "WO Not Complete"
	DELETE FOR edistat= "WO Not Processed"
ENDIF

SELECT pts
LOCATE
IF EOF()
	WAIT WINDOW "No open 945s for Account "+ALLTRIM(STR(lnAcctnum))+"...exiting" TIMEOUT 2
	No945Records()
	set date american
	schedupdate()
	CLOSE DATABASES ALL
	RETURN
ENDIF

COPY TO c:\tempfox\ptstatus.XLS TYPE XLS
report_date= DATE()

tu('pthist')


IF doemail = .T.
	lcReportname = "nanjingrpt"
	lcFilename = "c:\tempfox\940_945_Status_Report"
	STORE  .F. TO REPORTOK

	DO rpt2pdf WITH lcReportname,lcFilename,REPORTOK
	TRY
		tattach = lcFilename
		lcSourceMachine = SYS(0)
		tmessage = "See attached report file"+CHR(13)+" This report was generated from machine: "+lcSourceMachine
		DO FORM dartmail2 WITH tsendto,tFrom,tsubject," ",tattach,tmessage,"A"

		IF accountnumber = 4610 AND !llToMeOnly
			tattach = "c:\tempfox\ptstatus.xls"
			tmessage = "See attached report file in XLS format "+CHR(13)+" This report was generated from machine: "+lcSourceMachine
			SELECT 0
			USE F:\3pl\DATA\mailmaster ALIAS mm
			LOCATE FOR edi_type = "MISC" AND mm.taskname = "NANCHECK945"
			tsendto = IIF(mm.use_alt,sendtoalt,sendto)
			tcc = IIF(mm.use_alt,ccalt,cc)
			USE IN mm
			DO FORM dartmail2 WITH tsendto,tFrom,tsubject," ",tattach,tmessage,"A"
		ENDIF
	CATCH
		tattach = ""
		tmessage = "Problem with the 945 check routine on"+lcSourceMachine
		SELECT 0
		USE F:\3pl\DATA\mailmaster ALIAS mm
		LOCATE FOR mm.edi_type = "MISC" AND taskname = "GENERAL"
		tsendto = ALLTRIM(IIF(mm.use_alt,sendtoalt,sendto))
		tcc = ALLTRIM(IIF(mm.use_alt,ccalt,cc))
		USE IN mm
		DO FORM dartmail2 WITH tsendto,tFrom,tsubject,tcc,tattach,tmessage,"A"
	ENDTRY
ELSE
	REPORT FORM nanjingrpt PREVIEW
ENDIF

set date american
schedupdate()


********************************************************************************************************
PROCEDURE getuserid
	LOCAL lcusername,lres
	DECLARE INTEGER GetUserName IN advapi32 STRING@, INTEGER@

	lcusername = REPLICATE(CHR(0),255)
	lres = GetUserName(@lcusername,255)

	IF lres # 0 THEN
		RETURN LEFT(lcusername,AT(CHR(0),lcusername)-1)
*    RETURN LEFT(lcusername,AT(CHR(0),lcusername)-1)
	ELSE
		RETURN "UNK"
	ENDIF
ENDPROC
** "End of  for" getuserid



********************************************************************************************************
PROCEDURE No945Records
	DO CASE
		CASE INLIST(accountnumber,4610,4694)
			tcc = " "
			cCustomer = "Nanjing"
		CASE INLIST(accountnumber,4677)
			cCustomer = "Courtaulds"
		CASE INLIST(accountnumber,4752,5318)
			tcc = "RJ@highlineunited.com,byron@modernshoe.net,linda@modernshoe.net,sarah@highlineunited.com,donna@modernshoe.net."
			cCustomer = "Modern Shoe"
	ENDCASE
	SELECT 0
	USE F:\3pl\DATA\mailmaster ALIAS mm
	LOCATE FOR mm.edi_type = "MISC" AND mm.taskname = "GENERAL"
	tsendto = ALLTRIM(IIF(mm.use_alt,mm.sendtoalt,mm.sendto))
	tcc = ALLTRIM(IIF(mm.use_alt,mm.ccalt,mm.cc))
	USE IN mm
	tsubject = "No Open 945s for Customer: "+cCustomer
	tattach = ""
	tmessage = "There are no open 945s for this account, no PDF produced"
	DO FORM dartmail2 WITH tsendto,tFrom,tsubject,tcc,tattach,tmessage,"A"
ENDPROC
