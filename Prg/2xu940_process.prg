*!* m:\dev\prg\ariat940_process.prg

lcUpErrPath = "F:\FTPUSERS\2xu\940IN\hold\"

LogCommentStr = ""

delimchar = "*"
lcTranOpt= "TILDE"

set step On 
CD &lcPath

ll940test = .f.

If ll940test
  xpath = "f:\ftpusers\ariat-test\940in\"
  lnNum = Adir(tarray,xpath+"*.*")
Else
  lnNum = Adir(tarray,lcPath+"*.*")
Endif

lnNum = ADIR(tarray,"*.*")
IF lnNum = 0
	WAIT WINDOW AT 10,10 "      No "+cCustname+" 940s to import     " TIMEOUT 3
	NormalExit = .T.
	THROW
ENDIF

FOR thisfile = 1  TO lnNum
  If ll940test
    xfile = xpath+tarray[thisfile,1] && +"."
  Else
    xfile = lcPath+tarray[thisfile,1] && +"."
  Endif

	cfilename = LOWER(tarray[thisfile,1])
	nFileSize = VAL(TRANSFORM(tarray(thisfile,2)))
	dFileDate = tarray[thisfile,3]
	cFileTime = tarray[thisfile,4]

	archivefile  = (lcArchivePath+cfilename)
	!ATTRIB -R [&xfile]

*!* Added per Juan, from the Steel Series 940 process, Joe, 04.27.2017
	STORE .F. TO lNextDay,lMonday,l4Day
	IF cFileTime > "12:30:00"  && This is the cutoff time for date changes to process
		USE F:\hr\hrdata\holidays IN 0
		SELECT * FROM holidays WHERE YEAR(holidays.holidaydt) = YEAR(DATE()) INTO CURSOR tempholidays
		SELECT tempholidays
		INDEX ON holidaydt TAG holidaydt
		USE IN holidays

		SELECT tempholidays
		LOCATE FOR holidaydt = dFileDate
		IF FOUND()
			DO CASE
				CASE DOW(dFileDate) = 6
					lMonday = .T.
				CASE  MONTH(dFileDate) = 11
					IF DOW(dFileDate) = 5
						l4Day =  .T.
					ELSE
						lMonday = .T.
					ENDIF
				OTHERWISE
					lNextDay = .T.
			ENDCASE
		ELSE
			DO CASE && Processing for PTDATE calculation
				CASE (INLIST(DOW(dFileDate),7,1)) OR (DOW(dFileDate) = 6 AND cFileTime>="15:00:00")
					lMonday = .T.  && Will make PTDATE the following Monday for all Sat.-Sun file dates.
				CASE (!INLIST(DOW(dFileDate),6,7,1)) AND (cFileTime>="15:00:00")
					lNextDay= .T.  && Will make PTDATE the following day for non-weekend
			ENDCASE
		ENDIF
	ENDIF
*!* End of addition per Juan

	WAIT WINDOW "Importing file: "+cfilename NOWAIT
	DO m:\dev\prg\loadedifile WITH xfile,delimchar,lcTranOpt,cCustname

	SELECT x856
	LOCATE
	LOCATE FOR TRIM(x856.segment) = "GS" AND TRIM(x856.f1) = "FA"
	IF FOUND()
		_997file  = ("f:\ftpusers\2xu\997in\"+cfilename)
		COPY FILE [&xfile] TO [&_997file]
		DELETE FILE [&xfile]
		LOOP
	ENDIF

	cxferfile = ("F:\FTPUSERS\2XU\940xfer\"+cfilename)
	COPY FILE [&xfile] TO [&cxferfile]
	
	LOCATE

	IF lTesting
		cUsefolderOld = cUseFolder
		cUseFolder = "f:\whk\whdata\"
	ENDIF

	DO create_pt_cursors WITH cUseFolder,lLoadSQL && Added to prep for SQL Server transition

	IF lTesting
		cUseFolder = cUsefolderOld
		RELEASE cUsefolderOld
	ENDIF

	lOK = .T.

	DO ("m:\dev\prg\"+cUsename+"940_bkdn")
	IF lOK && If no divide-by-zero error
*		DO ("m:\dev\prg\"+cUsename+"940_import")
		DO ("m:\dev\prg\all940_import")
	ELSE
		lcUpErrFile = lcUpErrPath+cfilename
		COPY FILE [&xfile] TO [&lcuperrfile]
		DELETE FILE [&xfile]
	ENDIF

ENDFOR
WAIT WINDOW "All "+cCustname+" pickticket files uploaded" TIMEOUT 2

&& now clean up the 940in archive files, delete for the upto the last 10 days
**deletefile(lcArchivePath,IIF(DATE()<{^2016-05-01},20,20))

WAIT CLEAR

