*!* ARIAT 945 (Whse. Shipping Advice) SCANPACK Creation Program
*!* Creation Date: 08.12.2014 by Joe (Derived from Ariat945_create program)

parameters cbol,cship_ref
*WAIT WINDOW "Now at start of Ariat Scanpack 945 process" TIMEOUT 10
*SET STEP ON
public array thisarray(1)
public c_cntrlnum,c_grpcntrlnum,creturned,cwo_num,cwo_numstr,nwo_num1,ltesting,ltestinput,lamazon,lusepackagedata,ndecrementqty,lcpath,lcarchivepath
public lemail,ddatetimecal,nfilenum,lscanpack,nwo_xcheck,ldototqtycheck,ctrknumber,cerrmsg,ccustprefix,ntotctnwt,nloops,nodetqty,ldocompare,loverflow
public ccity,cstate,czip,norifgseq,noriggrpseq,tsendto,tcc,tsendtoerr,tccerr,cprogname,cisa_num,cchkdate,laddcharges,clinenumsd,nsqlqty,m.linenum,ldoscan,cpronum
public nlength,nwidth,ndepth,nwo_num,ldoariatfilesout,tsendtotest,tcctest,nacctnum1,nacctnum2,lusebol,ctrknumberdet,ltestprod,cmatchucc,lmastersubdetailinbol
public cmod,cmbol,cfilenameshort,cfilenameout,cfilenamearch,lparceltype,cwo_numlist,ceditype,cisa_num,ldosql,lloadsqlbl,ldobtcharges
public nfilenum
wait window "" timeout 1

coffice = "K"
cmod = coffice
cmbol = ""
lparceltype = .f.
ceditype = "945"
gmasteroffice= coffice
goffice = coffice
ldosql = .t.
lloadsqlbl = .t.
ncharge =0.0
m.linenum = ""
loverflow = .f.
cbolfill = ""

cerrmsg = "EARLY ERROR"

select 0
use f:\wh\sqlpassword
csqlpass = alltrim(sqlpassword.password)
use in sqlpassword

*!*	OPEN DATABASE m:\dev\wh
*!*	set database to wh
*!*	scacenter()

* to do testing, set the next 2 options to .t. and hard code in the bol later down in the code

ltesting   = .f. && Set to .t. for full testing
ltestinput =  .f.  &&  Set to .t. to use test input files only!
ltestprod = .f. && Allows testing using triggers and pre-delivered production data
ldocompare = !ltestinput && Cross-checks SQL totals in Labels vs Cartons

ldoariatfilesout = !ltesting
ldoscan = !ltesting
lemail = .t.  && Default = .t.
ltestmail = iif(ltesting or ltestinput,.t.,.f.) && Sends mail to Joe only

ldoariatfilesout = .f.
lTestMail = .t.

do m:\dev\prg\_setvars with ltesting

cprogname = "ariat945_create"
lusebol = .f.
lusepackagedata = .t.

if ltesting
	close databases all
endif

clear
ldototqtycheck = .t.  && In general, this should not be changed.
lpick = .f.
lprepack = .f.
ljcp = .f.
liserror = .f.
ldocatch = .t.
cship_ref = ""
lcloseoutput = .t.
nfilenum = 0
nwo_num = 0
ccustname = ""
store "" to cwo_numstr,cwo_numlist
ctrknumberdet = ""
cstring = ""
laddcharges = .f.

wait window "At the start of ARIAT 945 (all types) process..." nowait
*!* Note: For Wal-mart testing, be sure that there is a BOL and DEL_DATE filled in on the test OUTSHIP table,
*!* and that it matches the test data below

try
	if ltesting
		close databases all
		do m:\dev\prg\lookups
		if !used('edi_trigger')
			cedifolder = "F:\3PL\DATA\"
			use (cedifolder+"edi_trigger") in 0 alias edi_trigger
		endif

*!* TEST DATA AREA*
		cbol =  "08865320000113834"
		ctime = datetime()
	endif

	lfederated = .f.
	nacctnum = 6532
	nacctnum1 = 16532
	nacctnum2 = 26532
	norigseq = 0
	noriggrpseq = 0
	cppname = "Ariat"
	tfrom = "Toll EDI Operations <toll-edi-ops@tollgroup.com>"
	tmessage=""
	tcc = ""
	ldobolgen = .f.
	lsamples = .f.
	cbol1 = ""
	lisaflag = .t.
	lstflag = .t.
	nloadid = 1
	lsqlmail = .f.
	lfedex = .f.
	cupc = ""

	create cursor temp945 (accountid i,isa_num c(10),st_num c(10),wo_num i,bol_no c(20),ship_ref c(20),filename c(50),filedate t)
	cbol=trim(cbol)
	cretmsg = "X"

*********************
*!* Don't change these flags until testing is complete!!!
*!*		lTestMail = .T.
*!*		lDoAriatFilesOut = .F.
*********************

	store "LB" to cweightunit
	store "CF" to cvolunit
	cfd = "*"
	csegd = "~"
	nsegctr = 0
	cterminator = ">" && Used at end of ISA segment

	wait window "Now preparing tables and variables" nowait noclear

*!* SET CUSTOMER CONSTANTS
	ccustname = "ARIAT"  && Customer Identifier
	cx12 = "004010"  && X12 Standards Set used
	cmailname = "Ariat"
	select 0

	if used('mm')
		use in mm
	endif

	use f:\3pl\data\mailmaster alias mm
	locate for mm.edi_type = "945" and mm.office = coffice and mm.accountid = nacctnum
	tsendto = iif(mm.use_alt,mm.sendtoalt,mm.sendto)
	tcc = iif(mm.use_alt,mm.ccalt,mm.cc)
	lcpath = allt(mm.basepath)
	lcarchivepath = allt(mm.archpath)
	lcholdpath = allt(mm.holdpath)
	locate
	locate for mm.edi_type = 'MISC' and mm.taskname = "GENERAL"
	tsendtoerr = iif(mm.use_alt,mm.sendtoalt,mm.sendto)
	tccerr = iif(mm.use_alt,mm.ccalt,mm.cc)
	tsendtotest = iif(mm.use_alt,mm.sendtoalt,mm.sendto)
	tcctest = iif(mm.use_alt,mm.ccalt,mm.cc)
	use in mm
	tcc = iif(ltesting,"",tcc)

*!* SET OTHER CONSTANTS
	ccustloc =  "KY"
	cfmiwarehouse = ""
	ccustprefix = "945k"
	cdivision = "Kentucky"
	cfolder = "WHK"
	csf_addr1  = "7501 WINSTEAD DRIVE"
	csf_csz    = "LOUISVILLE"+cfd+"KY"+cfd+"40258"
	ccustfolder = upper(ccustname)

	lnonedi = .f.  && Assumes not a Non-EDI 945
	store "" to lckey
	store 0 to alength,nlength
	ctime = datetime()
	cdeltime = substr(ttoc(ctime,1),9,4)
	ccarriertype = "M" && Default as "Motor Freight"
	dapptnum = ""

*!* SET OTHER CONSTANTS
	if inlist(coffice,'C','1','2')
		ddatetimecal = (datetime()-(3*3600))
	else
		ddatetimecal = datetime()
	endif
	dt1 = ttoc(ddatetimecal,1)
	dtmail = ttoc(ddatetimecal)
	dt2 = ddatetimecal
	cstring = ""

	csendqual = "ZZ"
	csendid = "TGFPROD940"  && this will go to TGFPROD940 when in production
	csendidlong = padr(csendid,15," ")
*!* Receiver variables are at the ISA loop (noted 10.01.2010)

	cdate = dtos(date())
	ctruncdate = right(cdate,6)
	ctrunctime = substr(ttoc(ddatetimecal,1),9,4)
	cfiledate = cdate+ctrunctime
	corig = "J"
	nstcount = 0
	cstyle = ""
	cptstring = ""
	ntotctnwt = 0
	ntotctnvol = 0
	ntotctncount = 0
	nunitsum = 0
	ctargetstyle = ""

	do m:\dev\prg\swc_cutctns_gen with cbol

** PAD ID Codes
	ddatetimecal = datetime()
	dt1 = ttoc(ddatetimecal,1)
	cfilenamehold = (lcholdpath+ccustprefix+dt1+".txt")
	cfilenameshort = justfname(cfilenamehold)
	lcpath = iif(ltesting,"f:\ftpusers\ariat-test\945-staging\",lcpath)

	if ltesting &&And lTestInput
		cfilenameout = (lcpath+cfilenameshort)  && Ariat's direct 945OUT folder
	else
		cfilenameout = (lcpath+cfilenameshort)
		cfilenamearch = (lcarchivepath+cfilenameshort)
	endif

	nfilenum = fcreate(cfilenamehold)
*	ASSERT .F. MESSAGE "At Ariat SQL scan"

	csql="tgfnjsql01"

	lcdsnless="driver=SQL Server;server=&cSQL;uid=SA;pwd=&cSQLPass;DATABASE=PICKPACK"
	nhandle=sqlstringconnect(m.lcdsnless,.t.)
	sqlsetprop(nhandle,"DispLogin",3)
	wait window "Now processing ARIAT PT SQL select...please wait" nowait

	if nhandle=0 && bailout
		wait window "Could not make SQL connection" timeout 3
		cerrmsg = "NO SQL CONNECT"
		do ediupdate with cerrmsg,.t.
		throw
	endif

	if !ltestinput
		if used('OUTWOLOG')
			use in outwolog
		endif
	endif

	if used('ackdata')
		use in ackdata
	endif

	useca("ackdata","wh")

	if used("scacs")
		use in scacs
	endif
	use ("F:\3pl\DATA\parcel_carriers") in 0 alias scacs order tag scac

	if used('OUTSHIP')
		use in outship
	endif

	csq1 = [select * from outship where accountid = ]+transform(nacctnum)+[ and office = ']+coffice+[' and bol_no = ']+cbol+[']
	xsqlexec(csq1,,,"wh")
	if reccount() = 0
		cerrmsg = "MISS BOL "+cbol
		do ediupdate with cerrmsg,.t.
		throw
	endif

	index on bol_no tag bol_no

	select outship
	=seek(cbol,'outship','bol_no')
	nwo_num = outship.wo_num
	xxlcpt  =outship.ship_ref

	if seek(outship.scac,"scacs","scac")
		lparceltype = .t.
	endif

	lscanpack = iif("SCANPACK"$outship.shipins,.t.,.f.)
	ldoscan = iif(lscanpack,.t.,.f.)

	if !ltesting
*		ASSERT .f. MESSAGE "At WO Hold section"
		if used('ariat_wohold')
			use in ariat_wohold
		endif
		select 0
		use f:\3pl\data\ariat_wohold
		nwo_numold = ariat_wohold.wo_num
		if nwo_num = nwo_numold
			ldosql = .f.
		else
			replace wo_num with nwo_num in ariat_wohold
		endif
	endif
	ldosql = .t.
	if outship.carrcode = "UPSGCC"  && UPSGCC indicates that this is a Consolidated to Canada, so we need to load in the tracking numbers
*		WAIT WINDOW "" TIMEOUT 1

		run /n  f:\util\ariat\ariat_trk_ucc_import.exe
*		WAIT WINDOW "" TIMEOUT 1

*******************************************************************************************
		select ship_ref from outship where bol_no = cbol into cursor temppts
		select temppts
		lcerrorstr = ""
		llexporterror = .f.

		scan
			if usesql()
				xsqlexec("select distinct ucc from cartons where ship_ref = '"+temppts.ship_ref+"' and accountid=6532","vart",,"pickpack")
			else
				ctemp = "VART"
				lcsql=[select distinct ucc from cartons where ship_ref = ']+temppts.ship_ref+[' and accountid = 6532]
				llsuccess=sqlexec(nhandle,lcsql,ctemp)
				if llsuccess=0  && no records 0 indicates no records and 1 indicates records found
					cerrmsg = "NO SQL CONNECT AT WORLDEASE CHECK "
					do ediupdate with cerrmsg,.t.
					throw
				endif
			endif
			
			if xsqlexec("select * from ctnucc where mod='K' and ucc='"+vart.ucc+"'",,,"wh") = 0
				lcerrorstr = lcerrorstr +chr(13)+"no data for delivery: "+temppts.ship_ref+"looking for ucc "+ vart.ucc
				llexporterror=.t.
			else
				if empty(ctnucc.reference) or empty(ctnucc.serialno) && ctnucc needs the small pakg charge--> ctnucc.reference and the trknumber --> ctnucc.serialno
					lcerrorstr = lcerrorstr +chr(13)+temppts.ship_ref+"  UCC: "+ucc+"  Trk#: "+ctnucc.serialno+"  PkgChg: "+ctnucc.reference
					llexporterror=.t.
				endif
			endif
		endscan

		if llexporterror
			set step on
			tsubject = "ARIAT Worldease Export Error: "+trim(cbol)
			tsendto = "cheri.foster@tollgroup.com,tony.sutherland@tollgroup.com,michael.sanders@tollgroup.com"
			tcc = "pgaidis@fmiint.com,joe.bianchi@tollgroup.com,doug.wachs@tollgroup.com"
			tmessage = "Worldease Export data issues for following deliveries:"+chr(13)+lcerrorstr
			tattach =""
			do form m:\dev\frm\dartmail2 with tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
			cerrmsg = "ERR-WRLDESE UCC/PT "+alltrim(vart.ucc)+"-"+alltrim(temppts.ship_ref)
			do ediupdate with cerrmsg,.t.
			throw
		endif
	endif
*******************************************************************************************
	if used('OUTDET')
		use in outdet
	endif

	selectoutdet()
	select outdet
	index on outdetid tag outdetid

	if used('BL')
		use in bl
	endif
	if lloadsqlbl and !ltestinput
		csq1 = [select * from BL where bol_no = ']+cbol+[']
		xsqlexec(csq1,,,"wh")
		select bl
		index on bol_no tag bol_no
	else
		use f:\whk\whdata\bl in 0 alias bl
	endif

	if ldoscan
		wait window "This is a SCANPACK Account..."+chr(13)+"Must confirm No LABELS in SQL" nowait

		select wo_num,outshipid,shipins ;
		from outship ;
		where bol_no = padr(cbol,20) ;
		and accountid = nacctnum ;
		into cursor tempsr
		select tempsr
		locate
		wait window "There are "+alltrim(str(reccount()))+" outshipid's to check" nowait

		select tempsr
		lcleansql = .t.

		scan for "SCANPACK"$shipins  && only verify the actual scanpacked pts, pts can be mixed on a BOL
			if useca()
				xsqlexec("select * from labels where outshipid="+transform(tempsr.outshipid)+" and accountid=6532","vart",,"pickpack")
				lcq3 = " &cOSID "
				lcq4 = [ AND Labels.accountid = ]
				lcq5 = "6532"
				lcsql = lcq1+lcq2+lcq3+lcq4+lcq5

			else
				store alltrim(str(tempsr.outshipid)) to cosid
				ctemp = "VART"
				lcq1 = [SELECT * FROM dbo.labels Labels]
				lcq2 = [ WHERE Labels.outshipid = ]
				lcq3 = " &cOSID "
				lcq4 = [ AND Labels.accountid = ]
				lcq5 = "6532"
				lcsql = lcq1+lcq2+lcq3+lcq4+lcq5
				llsuccess=sqlexec(nhandle,lcsql,ctemp)
				if llsuccess=0  && no records 0 indicates no records and 1 indicates records found
					cerrmsg = "NO SQL CONNECT"
					do ediupdate with cerrmsg,.t.
					throw
				endif
			endif
				
			select vart
			if reccount() = 0
				wait window "No "+ccustname+" records found in LABELS for OSID "+cosid+" OK...proceeding" nowait
			else
				messagebox("RECORDS EXIST IN SQL LABELS"+chr(13)+"FOR OSID "+cosid+chr(13)+"CAN'T PROCEED",16,"SQL LABEL RECORD TRAP",4000)
				lcleansql = .f.
				assert .f. message "SQL Labels records found"
				cerrmsg = "RECS FOUND IN SQL LABELS"
				do ediupdate with cerrmsg,.t.
				throw
			endif
		endscan
		wait window "All Outshipids within this BOL are scanned...proceeding" nowait
		sqlcancel(nhandle)
		use in vart
	endif

	select outship
	cbolx = padr(cbol,20)

	if seek(cbolx,'outship','bol_no')
		lsyms = iif("SYMS"$outship.consignee,.t.,.f.)
		lamazon = iif("AMAZON"$outship.consignee or "GOLDEN STATE"$outship.consignee,.t.,.f.)
		lariateurope = iif("ARIAT"$outship.consignee and "EUROPE"$outship.consignee,.t.,.f.)
		ldocompare = iif("SCANPACK"$outship.shipins,.f.,ldocompare)
	else
		wait window "BOL not found in OUTSHIP" timeout 2
		cerrmsg = "BOL NOT FOUND"
		do ediupdate with cerrmsg,.t.
		throw
	endif

	if used('SHIPMENT')
		use in shipment
	endif

	if used('PACKAGE')
		use in package
	endif

*!*		csq1 = [select * from shipment where right(rtrim(str(accountid)),4) = ]+TRANSFORM(nAcctNum)+[ and pickticket =']+xxlcPT+[']
*!*		xsqlexec(csq1,,,"wh")

	xsqlexec("select * from shipment where 1=0",,,"wh")  && Sets up an empty cursor
	select outship && temp cursor with all PTs on the BOL
	scan
		csr = alltrim(outship.ship_ref)
		xsqlexec("select * from shipment where right(rtrim(str(accountid)),4) =6532 and pickticket = '"+csr+"'","sm1",,"wh")
		select shipment
		append from dbf('sm1')
		use in sm1
	endscan
	release csr

	select shipment
	index on shipmentid tag shipmentid
	index on pickticket tag pickticket

*!*		csq1 = [select * from package where accountid = ]+TRANSFORM(nAcctNum)+[ and shipmentid =]+TRANSFORM(shipment.shipmentid)
*!*		xsqlexec(csq1,,,"wh")
*!*		INDEX ON shipmentid TAG shipmentid
*!*		INDEX ON ucc TAG ucc
*!*		SET ORDER TO TAG shipmentid
	locate
	if reccount("shipment") > 0
		xjfilter="shipmentid in ("
		scan
			nshipmentid = shipment.shipmentid
			xsqlexec("select * from package where  right(rtrim(str(accountid)),4) = "+transform(nacctnum)+"and shipmentid="+transform(nshipmentid),,,"wh")

			if reccount("package")>0
				xjfilter=xjfilter+transform(shipmentid)+","
			else
				xjfilter="1=0"
			endif
		endscan
		xjfilter=left(xjfilter,len(xjfilter)-1)+")"

		xsqlexec("select * from package where "+xjfilter,,,"wh")
	else
		xsqlexec("select * from package where .f.",,,"wh")
	endif
	select package
	index on shipmentid tag shipmentid
	index on ucc tag ucc
	set order to tag shipmentid

	select shipment
	set relation to shipmentid into package

&& Added code blocks to the following to overcome BOL vs. WO selection issues, 12.23.2005

	wait window "Skipping Labels/Cartons Comparison..." nowait

	if ldosql  && If using the same WO# for multiple parcel shipments, this saves a lot of time.
		select outship
		locate
		if used("sqlpt")
			use in sqlpt
		endif
		delete file "F:\3pl\DATA\sqlpt.dbf"
		select bol_no,ship_ref,outshipid ;
		from outship ;
		where bol_no = padr(cbol,20) ;
		and accountid = nacctnum ;
		group by 1,2 ;
		order by 1,2 ;
		into dbf f:\3pl\data\sqlpt
		use in sqlpt
		use f:\3pl\data\sqlpt in 0 alias sqlpt

		cretmsg = ""

		do m:\dev\prg\sqlconnectariat_bol  with nacctnum,ccustname,cppname,.t.,coffice
		if cretmsg<>"OK"
			cerrmsg = cretmsg
			do ediupdate with cerrmsg,.t.
			throw
		endif
	endif

	select variatpp
	locate
************************************************************************
* here lets do a pre-check on all the PTs fo a QTY check
************************************************************************
	llallqtysok = .t.
	cerrmsg ="Pick Tickets with Sql.totqty vs Outdet.totqty errors on shipment: "+cbol+chr(13)+replicate("-",86)+chr(13)
	select ship_ref,outshipid,shipins,wo_num from outship where outship.bol_no= cbol into cursor outshipids
	select outshipids
	scan
		select outdet
		set order to
		sum totqty to nunittot1 for  units and outdet.outshipid = outshipids.outshipid
		select variatpp
		if ltesting
*			BROWSE
		endif

		sum totqty to nunittot2 for variatpp.outshipid = outshipids.outshipid
		if nunittot1<>nunittot2
			set step on
			llptype="UNK"
			do case
			case "PICKPACK"$outshipids.shipins
				llptype = "PICKPACK"
			case "SCANPACK"$outshipids.shipins
				llptype = "SCANPACK"
			case "PREPACK"$outshipids.shipins
				llptype = "PREPACK "
			endcase
			select variatpp
			locate for variatpp.outshipid = outshipids.outshipid
			cerrmsg = cerrmsg+"Pick Ticket: "+alltrim(outshipids.ship_ref)+" -("+llptype+")  Outdet totqty = "+padr(alltrim(transform(nunittot1)),6," ")+"  SQL Cartons TotQty:"+padr(alltrim(transform(nunittot2)),6," ")+chr(13)
			llallqtysok = .f.
		endif
	endscan

	if llallqtysok = .f.

		if lemail
			select 0
			use f:\3pl\data\mailmaster alias mm

			if coffice = "K"
				locate for mm.edi_type = "MISC" and mm.taskname = "SHORTERROR"
			else
				locate for mm.edi_type = "MISC" and mm.taskname = "GRPERROR"
			endif
			tsendto = iif(mm.use_alt,mm.sendtoalt,mm.sendto)
			tcc = iif(mm.use_alt,mm.ccalt,mm.cc)
			use in mm
			tsubject ="Ariat Outdet/SQL Qty Errors on BOL/Trk#: "+cbol+" at "+ttoc(datetime())
			tattach=""
			tmessage = cerrmsg
			do form m:\dev\frm\dartmail2 with tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
		endif

		m.bol_no = cbol
		m.ship_ref = alltrim(outship.ship_ref)
		m.addproc = "ARIAT940"
		csq1 = [select * from info where bol_no = ']+cbol+[' and ship_ref = ']+m.ship_ref+[' and addproc = ']+m.addproc+[']
		xsqlexec(csq1,"par1",,"wh")
		locate
		if eof("par1")
			use in par1
			useca("info","wh")	&& changed dy 12/27/17
			guserid="ARIATNEW"
			info("ARIAT_NEW_945",,,nacctnum,outship.ship_ref,,outship.cnee_ref,,,"SQL Qty shortage - Record(s) corrected")
			tu("info")

			cerrmsg= "SQL/OUTDET QTY ERR "+alltrim(outship.ship_ref)
			set step on
			do ediupdate with cerrmsg,.t.
			throw
		endif
		release csq1
		if used("par1")
			use in par1
		endif

	endif

*************************************************************************

	if ltesting
*	BROWSE TIMEOUT 30
	endif

	use f:\wh\sqlpassword in 0
	gsqlpassword = alltrim(sqlpassword.password)
	use in sqlpassword

	cserver = "tgfnjsql01"
	sqlsetprop(0,'DispLogin',3)
	sqlsetprop(0,"dispwarnings",.f.)

	lcdsnless="driver=SQL Server;server=&cServer;uid=SA;pwd=&gsqlpassword;DATABASE=PICKPACK"
	nhandle=sqlstringconnect(m.lcdsnless,.t.)
	if nhandle<=0 && bailout
		wait window at 6,2 "No Data Connection.."+chr(13)+" Call MIS...."
		lcquery = [Could not make connection to sp3 SQL server]+chr(13)+"   "+lcdsnless
		throw
		return
	endif

	select outship
	locate
	wait clear
	wait window "Now creating Header information" nowait noclear


*********************************************************************************************************************
* here we check to see if the BOL has multiple pick tickets, if so we need to send a 945 file per pick ticket
* even in the case of a trucking BOL with many picktickets
*********************************************************************************************************************
*  Select ship_ref From outship Where bol_no = cBol Into Cursor xPTs
*  Select xPts
*  Scan

*!* HEADER LEVEL EDI DATA
	if !ltesting
		do num_incr_isa
	else
		c_cntrlnum = "1"
	endif

	cisa_num = padl(c_cntrlnum,9,"0")
	nisa_num = int(val(cisa_num))
	cisacode = iif(ltesting or ltestinput,"T","P")
	cisacode = "P"

	select outship
	if seek(cbol,'outship','bol_no')
		alength = alines(apt,outship.shipins,.t.,chr(13))
		cxshipid = segmentget(@apt,"SHIPID945",alength)
	endif

	crecqual = "01"
	crecid = "789995313"
	crecidlong = padr(crecid,15," ")

**************************************************************************8

****************** START WRITING OUT THE 945 FILE ****************************

	store "ISA"+cfd+"00"+cfd+"          "+cfd+"00"+cfd+"          "+cfd+csendqual+cfd+csendidlong+cfd+;
	crecqual+cfd+crecidlong+cfd+ctruncdate+cfd+ctrunctime+cfd+"U"+cfd+"00401"+cfd+;
	cisa_num+cfd+"0"+cfd+cisacode+cfd+cterminator+csegd to cstring
	do cstringbreak

	store "GS"+cfd+"SW"+cfd+csendid+cfd+crecid+cfd+cdate+cfd+ctrunctime+cfd+c_cntrlnum+;
	cfd+"X"+cfd+cx12+csegd to cstring
	do cstringbreak

	select outship
*	SET ORDER TO TAG wo_num
	locate

	if !ltesting
		if empty(cbol)  && May be empty if triggered from PT screen, or FedEx shipment
			wait clear
			wait window "MISSING Bill of Lading Number!" timeout 2
			cerrmsg = "BOL# EMPTY"
			do ediupdate with cerrmsg,.t.
			throw
		else
			if !seek(padr(trim(cbol),20),"outship","bol_no")
				wait clear
				wait window "Invalid Bill of Lading Number - Not Found!" timeout 2
				cerrmsg = "BOL# NOT FOUND"
				do ediupdate with cerrmsg,.t.
				throw
			endif
		endif
	endif

*************************************************************************
*1	PTIDSOURCE/OUTSHIP RECORD MAIN LOOP
*	Loops through individual OUTSHIP lines for BOL#
*************************************************************************
	wait clear
	wait window "Now creating BOL#-based information..." nowait noclear

	select outship
	set order to
	if ltesting or ltestprod
		count to n for outship.bol_no = padr(cbol,20)
	else
		count to n for outship.bol_no = padr(cbol,20) and !emptynul(del_date)
	endif
	if n=0
		cerrmsg = "INCOMP BOL"
		do ediupdate with cerrmsg,.t.
		throw
	endif

	locate
	cmissdel = ""

	if ltesting or ltestprod
		if cbol = "EMPTY"
			oscanstr = "outship.bol_no = PADR(cBOL,20)"
		else
			oscanstr = "outship.bol_no = PADR(cBOL,20) and ctnqty>0"
		endif
	else
		oscanstr = "outship.bol_no = PADR(cBOL,20) AND !EMPTYnul(del_date) and ctnqty>0"
	endif


	select 0
	select * from "f:\3pl\data\ackdatashell" where .f. into cursor tempack readwrite

	select outship
	nwo_xcheck = 0
	dimension aryamz(1)
	
	SET STEP ON
	scan for &oscanstr &&outship.ship_ref = xPTs.ship_ref
		cship_ref = alltrim(outship.ship_ref)
		lscanpack = iif("SCANPACK"$outship.shipins,.t.,.f.)

		scatter fields wo_num,scac,ship_via memvar
		if empty(outship.scac)
			m.scac = segmentget(@apt,"SHIPSCAC",alength)
			replace outship.scac with m.scac in outship next 1
			if ltestinput and date() = {^2014-12-01}
				m.scac = "UPSS"
			endif
		endif
		if alltrim(outship.scac) = "FSP" && Added to prevent issues with Fedex Smartpost SCAC, per Cori @ Ariat, 07.21.2017, Joe
			replace outship.scac with "FXSP" in outship next 1
		endif


		lconsolfreight = .f.
		lworldeasefreight = .f.

******** look here

		select scacs
		if inlist(padr(alltrim(outship.carrcode),10),"UPSGCC","UPSFCO","UPSFFF","UPSFTP","UPSF") or cbol = '1Z89RA010303768895'
			if outship.carrcode = "UPSGCC"
				lworldeasefreight = .t.
				xsqlexec("select * from ctnucc where mod='K' and ship_ref='"+outship.ship_ref+"'",,,"wh")

				if found()
					lcworldeasefreightcharge = alltrim(ctnucc.reference)  && this is the charge from Worldease
				else
					lcworldeasefreightcharge = "UNK"
					tsubject = "ARIAT Worldease Export Error: "+trim(cbol)
					tsendto = "cheri.foster@tollgroup.com,tony.sutherland@tollgroup.com,michael.sanders@tollgroup.com"
					tcc = "pgaidis@fmiint.com,joe.bianchi@tollgroup.com,tmarg@fmiint.com"
					tmessage = "Worldease Export data issues for following delivery:"+chr(13)+alltrim(outship.ship_ref)+chr(13)+" Can't find this delivery in the Worldease data...."
					tattach =""
					do form m:\dev\frm\dartmail2 with tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
					cerrmsg = "WORLEASE DATA PT LOOKUP ERROR"
					do ediupdate with cerrmsg,.t.
					throw
				endif
* Select =Seek(tuccs.ucc,"ctnucc","ship_ref")
*!*	        If Found("ctnucc")
*!*	          lcWorldEaseFreightCharge = Alltrim(ctnucc.Reference)
*!*	        Else
*          lcWorldEaseFreightCharge = "UNK"
*!*	        Endif
			endif
			wait window "This is a UPS CONSOLIDATION shipment, running as type = 'Freight'" nowait
			lconsolfreight = .t.
			lparceltype = .f.
		else
			if !empty(trim(outship.scac))
				store trim(outship.scac) to m.scac
				m.scac = iif(m.scac = "FEDZ","FDEN",m.scac)
				m.scac = iif(m.scac = "FDXG","FDEG",m.scac)
				m.scac = iif(m.scac = "UPWZ","UPSN",m.scac)

				if xsqlexec("select * from altscac where accountid=6532 and scaccode='"+m.scac+"'",,,"wh") # 0

**added UPSGCC filter to exclude that carrcode from UPS processing, will be processed manually - no data in shipment/package - mvw 05/27/14
					if inlist(m.scac,"UPSN","USPS")
						if inlist(cship_ref,"0083853250")  && Put in PT # for those parcel shipments processed w/o labels, etc.
							lparceltype = .f.
							lfedex = .f.
						else
							if outship.carrcode = "XXXXXUPSGCC"
								cerrmsg = "NO 945 NEEDED - UPSGCC"
								do ediupdate with cerrmsg,.f.
								throw

							else
								lparceltype = .t.
								lfedex = .f.
							endif
						endif
					endif
					if inlist(m.scac,"FDEN","FDEG","FXSP")
						if outship.carrcode ="FDXIFE"
							lfedex   = .f.
							lparceltype = .f.
						else
							lfedex   = .t.
							lparceltype = .t.
						endif
					endif
				endif

				lparceltype = iif(alltrim(outship.keyrec) == alltrim(outship.bol_no),.f.,lparceltype)

				select outship

				if lparceltype
					ccarriertype = "U" && Parcel as UPS or FedEx
				endif
			else
				wait clear
				cerrmsg = "MISSING SCAC"
				do ediupdate with cerrmsg,.t.
				throw
			endif
		endif

		lusepackagedata = iif(empty(outship.amazon) and lparceltype,.t.,.f.)
		cship_ref = alltrim(outship.ship_ref)
		ccarrcode = alltrim(outship.carrcode)
		ccarrcode = iif(ccarrcode = "FDXGCO","FDXGC",ccarrcode)
		ccarrcode = iif(ccarrcode = "FSP","FXSP",ccarrcode)

		if lparceltype
			if xsqlexec("select * from altscac where accountid=6532 and shipmode='"+ccarrcode+"'",,,"wh") = 0
				cerrmsg = "BAD CARRCODE: "+ccarrcode
				do ediupdate with cerrmsg,.t.
				throw
			else
				laddcharges = altscac.frton945
			endif
		endif

		select outship
		if lparceltype
			cproups = cbol
		endif
*		ASSERT .F. MESSAGE "Debug here!"
		select outship
		scatter memvar memo
		if " OV"$m.ship_ref or "!!"$m.ship_ref
			loop
		endif

		nwo_num = outship.wo_num
		cwo_num = alltrim(str(nwo_num))
		lbloom = iif(outship.consignee = "BLOOMINGDALE",.t.,.f.)

		if !(cwo_num$cwo_numstr)
			cwo_numstr = iif(empty(cwo_numstr),cwo_num,cwo_numstr+","+cwo_num)
		endif
		if !(cwo_num$cwo_numlist)
			cwo_numlist = iif(empty(cwo_numlist),cwo_num,cwo_numlist+chr(13)+cwo_num)
		endif

		if ltestinput
			lpick = .t.
		else
			xsqlexec("select * from outwolog where wo_num  = "+transform(m.wo_num),,,"wh")
			lpick = iif(outwolog.picknpack,.t.,.f.)
			lprepack = iif(lpick,.f.,.t.)
		endif

		ntotctncount = m.qty

		cpo_num = left(alltrim(m.cnee_ref),22)
		if empty(cpo_num)
			cpo_num = "NA"
		endif

*!* Added this code to trap miscounts in OUTDET Units

		if ldototqtycheck && lPrepack AND
			select outdet
			set order to
			sum totqty to nunittot1 for  units and outdet.outshipid = outship.outshipid
			if lprepack
				sum (val(pack)*totqty) to nunittot2 for !units and outdet.outshipid = outship.outshipid
				if nunittot1<>nunittot2
					set step on
					cerrmsg = "OUTDET INTERNAL TOTQTY ERR"
					=fclose(nfilenum)
					do ediupdate with cerrmsg,.t.
					throw
				endif
			endif
			select variatpp
			sum totqty to nunittot2 for variatpp.outshipid = outship.outshipid
			if nunittot1<>nunittot2
				set step on
				select variatpp
				locate for variatpp.outshipid = outship.outshipid
				cerrmsg = "SQL/OTDET QTY ERR: PT "+alltrim(variatpp.ship_ref)
				do ediupdate with cerrmsg,.t.
				=fclose(nfilenum)
				throw
			endif
		endif
		select outdet
		set order to && outdetid
		locate

*!* End code addition

		if (("PENNEY"$upper(outship.consignee)) or ("KMART"$upper(outship.consignee)) or ("K-MART"$upper(outship.consignee))) and !lparceltype
			lapptflag = .t.
		else
			lapptflag = .f.
		endif

		if ltestinput
			ddel_date = date()
			dapptnum = "99999"
			dapptdate = date()
		else
			ddel_date = outship.del_date
			if empty(ddel_date)
				if ltesting or ltestprod
					ddel_date = date()
				else
					cmissdel = iif(empty(cmissdel),"The following PTs had no Delivery Dates:"+chr(13)+trim(cship_ref),cmissdel+chr(13)+trim(cship_ref))
				endif
			endif
			dapptnum = alltrim(outship.appt_num)
			if empty(dapptnum) and ltestprod
				dapptnum = str(int(rand(seconds())*(10^7)),7)
			endif

			if empty(dapptnum) and (("PENNEY"$outship.consignee) or ("KMART"$outship.consignee) or ("K-MART"$outship.consignee))  && Penney/KMart Appt Number check
				if !lapptflag
					dapptnum = ""
				else
					cerrmsg = "EMPTY APPT #"
					do ediupdate with cerrmsg,.t.
					throw
				endif
			endif

			dapptdate = outship.appt
			if empty(dapptdate)
				dapptdate = iif(ltestprod,date(),outship.del_date)
			endif
		endif

		alength = alines(apt,outship.shipins,.t.,chr(13))

** update this logic to err out when Amazon........PG 01/08/2018
		do case
		case lariateurope
			cpronum = "OCEAN"
		case lamazon
			if empty(outship.keyrec)
				cerrmsg = "NO PRO#/ARN# FOR AMAZON"
				do ediupdate with cerrmsg,.t.
			endif
			cpronum = alltrim(outship.keyrec)
		case !lparceltype
			cpronum = allt(outship.keyrec)
		otherwise
			cpronum = "NONE"
		endcase
	*!* Added to prevent catastrophic failure to upload 945 due to missing PRO#.  03/30/2018, Joe
			IF EMPTY(cProNum) AND !lParcelType
				cerrmsg = "Missing PRO# "+alltrim(outship.ship_ref)
				do ediupdate with cerrmsg,.t.
				=fclose(nfilenum)
				THROW
			ENDIF 
	*!* End added PRO# code			

*!*  		IF !lAriatEurope
*!*  			IF lAmazon
*!*  				cPRONum = ALLTRIM(outship.keyrec)
*!*  			ELSE
*!*  				cPRONum = ALLT(outship.appt_num)
*!*  				IF EMPTY(cPRONum) AND !lParcelType
*!*  					cPRONum = ALLT(outship.keyrec)
*!*  					IF EMPTY(cPRONum) AND !lParcelType
*!*  						cPRONum = "NONE"
*!*  					ENDIF
*!*  				ENDIF
*!*  			ENDIF
*!*  		ELSE
*!*  			cPRONum = "OCEAN"
*!*  		ENDIF

		noutshipid = m.outshipid
		cptstring = iif(empty(cptstring),m.consignee+" "+cship_ref,cptstring+chr(13)+m.consignee+" "+cship_ref)

		m.csz = trim(m.csz)
		if empty(allt(strtran(M.csz,",","")))
			wait clear
			wait window "No SHIP-TO City/State/ZIP info...exiting" timeout 2
			cerrmsg = "NO CSZ INFO"
			do ediupdate with cerrmsg,.t.
			throw
		endif
		if !(", "$m.csz)
			m.csz = alltrim(strtran(m.csz,",",", "))
		endif
		m.csz = alltrim(strtran(m.csz,"  "," "))
		ccity = allt(left(trim(m.csz),at(",",m.csz)-1))
		cstatezip = allt(substr(trim(m.csz),at(",",m.csz)+1))

		cstate = allt(left(cstatezip,2))
		corigstate = segmentget(@apt,"ORIGSTATE",alength)
		cstate = iif(empty(corigstate),cstate,corigstate)

		czip = allt(substr(cstatezip,3))

		lnosforaddress = .f.
		if (empty(strtran(alltrim(outship.sforcsz),",","")) or empty(alltrim(outship.sforaddr1)))
			blank fields outship.sforcsz next 1 in outship
			m.sforcsz = ""
			lnosforaddress = .t.
		endif

		if !ltesting
			do num_incr_st
		else
			c_grpcntrlnum  = "1"
		endif
		wait clear
		wait window "Now creating Line Item information" nowait noclear

		insert into temp945 (accountid,isa_num,st_num,wo_num,bol_no,ship_ref,filename,filedate) ;
		values (m.accountid,c_cntrlnum,c_grpcntrlnum,m.wo_num,m.bol_no,m.ship_ref,cfilenameshort,dt2)
******** added 1/9/17 TMARG to capture 997 data

		m.groupnum=c_cntrlnum
		m.isanum=cisa_num
		m.transnum=padl(c_grpcntrlnum,9,"0")
		m.edicode="SW"
		m.accountid=m.accountid
		m.loaddt=date()
		m.loadtime=datetime()
		m.filename=cfilenameshort
		m.ship_ref=m.ship_ref
		insertinto("ackdata","wh",.t.)
		tu("ackdata")

		store "ST"+cfd+"945"+cfd+padl(c_grpcntrlnum,9,"0")+csegd to cstring
		do cstringbreak
		nstcount = nstcount + 1
		nsegctr = 1
		cintacct = ""
		insert into tempack from memvar


		store "W06"+cfd+"N"+cfd+cship_ref+cfd+cdate+cfd+trim(cship_ref)+cfd+trim(cwo_num)+cfd+cpo_num+csegd to cstring
		do cstringbreak
		nsegctr = nsegctr + 1

		nctnnumber = 1  && Seed carton sequence count

		cstorenum = alltrim(m.dcnum)

		if empty(cstorenum)
			store "N1"+cfd+"ST"+cfd+alltrim(m.consignee)+csegd to cstring
		else
			store "N1"+cfd+"ST"+cfd+alltrim(m.consignee)+cfd+"92"+cfd+cstorenum+csegd to cstring
		endif
		do cstringbreak
		nsegctr = nsegctr + 1

		if !empty(m.sforstore)
			store "N1"+cfd+"BY"+cfd+alltrim(m.shipfor)+cfd+"91"+cfd+trim(m.sforstore)+csegd to cstring
			do cstringbreak
			nsegctr = nsegctr + 1
		endif

		store "N1"+cfd+"SF"+cfd+"TGF"+cfd+"91"+cfd+"01"+csegd to cstring
		do cstringbreak
		nsegctr = nsegctr + 1

		store "N3"+cfd+trim(csf_addr1)+csegd to cstring
		do cstringbreak
		nsegctr = nsegctr + 1

		store "N4"+cfd+csf_csz+cfd+"USA"+csegd to cstring
		do cstringbreak
		nsegctr = nsegctr + 1

		if lparceltype
			if cbol # "EMPTY"
				lusepackagedata = .f.
				if !lamazon
					if ltesting and ltestinput
						ncharge = 25.70
					else
						cupstrack = ""
						nstrseek = padr(allt(cship_ref),20)
						select shipment
						locate for shipment.pickticket = nstrseek and inlist(shipment.accountid,nacctnum,nacctnum1,nacctnum2,9999)
						if !found()
							cerrmsg = "NO FIND IN SHIPMENT, PT "+alltrim(cship_ref)
							do ediupdate with cerrmsg,.t.
							throw
						else
							sum shipment.charge to ncharge ;
							for shipment.pickticket = nstrseek ;
							and inlist(shipment.accountid,nacctnum,nacctnum1,nacctnum2,9999)
						endif

						lusepackagedata = .t.
						select package
						select trknumber,ucc as ucc,pkgweight,length,width,depth ;
						from package ;
						where .f. ;
						into cursor packagedata readw
						select shipment
						locate
						ns1 = 1
						ns = transform(ns1)
						scan for shipment.pickticket = nstrseek and inlist(shipment.accountid,nacctnum,nacctnum1,nacctnum2,9999)
							select trknumber,ucc as ucc,pkgweight,length,width,depth ;
							from package ;
							where package.shipmentid = shipment.shipmentid ;
							into cursor packagedata&ns
							select packagedata
							append from dbf('PACKAGEDATA'+ns)
							use in packagedata&ns
						endscan
					endif
				else
					lenary1 = memlines(outship.amazon)
					if lenary1 < 1
						if empty(cbol)
							cerrmsg = "EMPTY 'AMAZON' FIELD"
							do ediupdate with cerrmsg,.t.
							throw
						else
							if cbol = "AMZ"
								nstrseek = padr(allt(cship_ref),20)
								select shipment
								locate for shipment.pickticket = nstrseek and inlist(shipment.accountid,nacctnum,nacctnum1,nacctnum2,9999)
								if !found()
									set step on
									cerrmsg = "MISS PT IN SHIPMENT "+cship_ref
									do ediupdate with cerrmsg,.t.
									throw
								else
									sum shipment.charge for shipment.pickticket = nstrseek and inlist(shipment.accountid,nacctnum,nacctnum1,nacctnum2,9999) to ncharge
								endif

								lusepackagedata = .t.
								lusebol = .f.
								select package
								locate for package.shipmentid = shipment.shipmentid
								if !found()
									set step on
									cerrmsg = "NO MATCH IN PACKAGE "+cship_ref
									do ediupdate with cerrmsg,.t.
									throw
								endif

								select trknumber,ucc ;
								from package ;
								where .f. ;
								into cursor packagedata readw
								select shipment
								locate
								ns1 = 1
								ns = transform(ns1)
								ncharge= 0.0
								scan for shipment.pickticket = nstrseek and inlist(shipment.accountid,nacctnum,nacctnum1,nacctnum2,9999)
									select trknumber,ucc ;
									from package ;
									where package.shipmentid = shipment.shipmentid ;
									into cursor packagedata&ns
									select packagedata
									append from dbf('PACKAGEDATA'+ns)
									use in packagedata&ns
								endscan
							else
								lusepackagedata = .f.
								lusebol = .t.
								ctrknumber =  cbol
							endif
						endif
					else
						alines(aryamz,outship.amazon)
						lusebol = .t.
						ctrknumber =  cbol
					endif
				endif
			else
				create cursor packagedata (trknumber c(20),ucc c(20))
				ncharge = 20.5
				for wwq = 1 to outship.ctnqty
					cucc = "00008102000005"+padl(right(allt(str(rand(seconds())*250000)),6),6,"0")
					if lfedex
						ctrknumber = "977712345"+padl(right(allt(str(rand(seconds())*250000)),6),6,"0")
					else
						ctrknumber = "1ZX123A12356"+padl(right(allt(str(rand(seconds())*250000)),6),6,"0")
					endif
					insert into packagedata (trknumber,ucc) values (ctrknumber,cucc)
				endfor
			endif
		endif
*!*			IF cBOL = "EMPTY"
*!*				lUseBOL = .T.
*!*				cTrkNumber =  cBOL
*!*			ENDIF

		ccharge = alltrim(str(ncharge*100,10))

		if lusebol
			ctrknumber =  cbol
		endif

		if lamazon and lparceltype and empty(ctrknumber)
			cerrmsg = "EMPTY AMZ Track# "+cship_ref
			do ediupdate with cerrmsg,.t.
			throw
		endif

		cbolfill = iif((lparceltype and !lamazon),cbol,cpronum)  && on a ParcelType the cBol would be the tracking number; changed to PRO# for all Amazon shipments
		cbolfill = iif(lconsolfreight,cbol,cbolfill)  && if lConsolFreight is .T. then its a consolidate to Canada,
&& N9*BM would be the LTL BOL and the individual ctns would be UPS tracking number from Worldease import

		if empty(cbolfill)
		else
			store "N9"+cfd+"BM"+cfd+cbolfill+csegd to cstring  && on a regular or domestic small pkg shipment both N9*BM and N9*CN are the small pkg tracking number
			do cstringbreak
			nsegctr = nsegctr + 1
		endif

		store "N9"+cfd+"CN"+cfd+cbol+csegd to cstring  && Added 05.09.2014, per Daniel at Ariat.
		do cstringbreak                                && On small pkg this is a  tracking number
		nsegctr = nsegctr + 1

		csalesorder = segmentget(@apt,"SALESORDER",alength)
		store "N9"+cfd+"CO"+cfd+csalesorder+csegd to cstring
		do cstringbreak
		nsegctr = nsegctr + 1

		cbillto = segmentget(@apt,"BILLTONAME",alength)  && Added to pull Dillard or Amazon from bill-to info
		cbilltocode = segmentget(@apt,"BILLTOCODE",alength)  && Added to
		ldobtcharges = iif(inlist(cbilltocode,"20004210","20005422"),.f.,.t.)

		lldillards = .f.
		llarn= .f.

		if "DILLARD"$outship.consignee or "DILLARD"$upper(cbillto)
			lldillards = .t.
			store "N9"+cfd+"LO"+cfd+iif(lamazon,cpronum,allt(outship.appt_num))+csegd to cstring  && Changed to use PRO# if Amazon type shipment.
			do cstringbreak
			nsegctr = nsegctr + 1
		endif

		if ("AMAZON"$outship.consignee or "AMAZON"$upper(cbillto)) and !("MARKETPLACE"$upper(cbillto))
			llarn= .t.
			camazonroutcode = allt(outship.appt_num)
			camazonroutcode = iif(empty(camazonroutcode),alltrim(outship.keyrec),camazonroutcode)
			llallnumeric=.t.
			for lndigctr = 1 to len(camazonroutcode)
				if !isdigit(substr(camazonroutcode,lndigctr,1))
					llallnumeric = .f.
				endif
			next
			if (empty(camazonroutcode) or len(camazonroutcode)!= 10 or !llallnumeric)
				set step on
				cerrmsg = "EMPTY/BAD AMZ ARN Number "+cship_ref
				do ediupdate with cerrmsg,.t.
				throw
			endif
			store "N9"+cfd+"LO"+cfd+allt(camazonroutcode)+csegd to cstring
			do cstringbreak
			nsegctr = nsegctr + 1
		endif

		if [DICK'S SPORTING GOODS]$outship.consignee
			llcscode= .t.
			lccscode = allt(outship.appt_num)
			if substr(lccscode,1,2)!="CS"
				llcscode= .f.
			endif
			if (empty(lccscode) or len(lccscode) #10 or !llcscode) and lccscode # "NOLOADNUMBER"
				cerrmsg = "EMPTY/BAD DICKS Routing Code "+cship_ref
				do ediupdate with cerrmsg,.t.
				throw
			endif
			store "N9"+cfd+"LO"+cfd+allt(lccscode)+csegd to cstring
			do cstringbreak
			nsegctr = nsegctr + 1
		endif

		if lldillards = .f. and llarn= .f. and !empty(outship.appt_num)
			store "N9"+cfd+"LO"+cfd+allt(outship.appt_num)+csegd to cstring
			do cstringbreak
			nsegctr = nsegctr + 1
		endif

		store "G62"+cfd+"11"+cfd+trim(dtos(ddel_date))+cfd+"D"+cfd+cdeltime+csegd to cstring  && Ship date/time
		do cstringbreak
		nsegctr = nsegctr + 1

		llsmallpkg = .f.

		if xsqlexec("select * from altscac where accountid=6532 and scaccode='"+outship.scac+"'",,,"wh") # 0 ;
			and altscac.smallpkg
			llsmallpkg = .t.
		else
			cbolx = padr(cbol,20)
			if seek(cbolx,'bl','bol_no')
				do case
				case bl.terms = "1"
					lcterms = "PP"
				case bl.terms = "2"
					lcterms = "CC"
				case bl.terms = "3"
					lcterms = "TP"
				otherwise
					lcterms = "PP"
				endcase
			else
				lcterms = "PP"
			endif
		endif

		if empty(m.carrcode) and !llsmallpkg
			select ariat_carriers
			locate for alltrim(ariat_carriers.scac) = alltrim(outship.scac) and alltrim(ariat_carriers.terms)= alltrim(lcterms)
			if found()
				m.carrcode = ariat_carriers.vendor
			else
				locate for alltrim(ariat_carriers.scac) = alltrim(outship.scac)
				if found()
					m.carrcode = ariat_carriers.vendor
				else
					cerrmsg = "Ariat SCAC is not valid....->> "+outship.scac+"....at BOL...."+cbol+"  at PT "+cship_ref
					do ediupdate with cerrmsg,.t.
					throw
				endif
			endif
		endif

		store "W27"+cfd+ccarriertype+cfd+trim(m.scac)+cfd+trim(m.carrcode)+csegd to cstring  && Changed per Paul, 05.05.2014
		do cstringbreak
		nsegctr = nsegctr + 1


		if lworldeasefreight = .t. and !empty(lcworldeasefreightcharge)
			store "G72"+cfd+"504"+cfd+"06"+replicate(cfd,6)+allt(lcworldeasefreightcharge)+csegd to cstring
			do cstringbreak
			nsegctr = nsegctr + 1
		endif

		if lparceltype and lworldeasefreight = .f. and ldobtcharges
			if laddcharges
				if inlist(alltrim(m.carrcode),"UPSG","UPSGR")  && special 15% discount from the Malvern published rates, PG 03/06/2015
					ccharge = str(round(val(alltrim(ccharge))*0.85,0),6,0)
				endif
				store "G72"+cfd+"504"+cfd+"06"+replicate(cfd,6)+allt(ccharge)+csegd to cstring
				do cstringbreak
				nsegctr = nsegctr + 1
			endif
		endif

*************************************************************************
*	DETAIL LOOP - MATCHING SHIP_REF FIELD (FROM OUTSHIP)
*************************************************************************
		wait clear
		wait window "Now creating Detail information "+cship_ref nowait noclear


		create cursor upcdata (;
		ship_ref char(20),;
		outshipid int,;
		outdetid int,;
		upc char(12),;
		style char(20),;
		color char(10),;
		mainline char(15),;
		origqty  n(6),;
		linenum  char(15),;
		lineqty n(6),;
		remainqty n(6))

		lcpt = cship_ref

**  query by pick ticket

		if ltesting
			select ucc,upc,style,color,pack,sum(totqty) as pqty from variatpp where ship_ref=lcpt and ucc # 'CUTS' and accountid = 6532  group by ucc,upc,style,color,pack into cursor tempdata
		else
			if usesql()
				xsqlexec("select ucc, upc, style, color, pack, sum(totqty) as pqty from cartons where ship_ref='"+lcpt+"' " + ;
					"and ucc#'CUTS' and accountid=6532 group by ucc, upc, style, color, pack","tempdata",,"pickpack")
			else
				lcquery  = [select ucc,upc,style,color,pack,sum(totqty) as pqty from dbo.cartons where ship_ref=']+lcpt+[' and ucc != 'CUTS' and accountid = 6532  group by ucc,upc,style,color,pack]
				if sqlexec(nhandle,lcquery,"tempdata")#1
					wait window at 6,2 "Error Cartons SQL query.........." timeout 1
					sqldisconnect(nhandle)
				endif
			endif
		endif

		select tempdata
		if ltesting
*		BROWSE
		endif
		select distinct ucc from tempdata into cursor tuccs

		select outship

		locate for padr(alltrim(lcpt),20)=outship.ship_ref
		thisoutshipid = outship.outshipid

		select style,color,upc,printstuff,outshipid,outdetid,units,linenum,sum(origqty) as mlqty from outdet where  units and outdet.outshipid = thisoutshipid group by style,color,upc  into cursor toutdet

		select toutdet
		scan for toutdet.outshipid = thisoutshipid and units
&& OK insert the first main line data record
			insert into upcdata (ship_ref,outshipid,outdetid,upc,style,color,mainline,origqty) values (lcpt,thisoutshipid,toutdet.outdetid,toutdet.upc,toutdet.style,toutdet.color,toutdet.linenum,toutdet.mlqty)
			thismainline = toutdet.linenum

			alines(a, printstuff)
			llsubline = .f.
			lnsubqty=0

			for i=1 to alen(a)
				if a[i] = "SUBLINE"
					llsubline = .t.
					lnlinestarts =atc("*",a[i])
					thisline=  substr(a[i],lnlinestarts+1)
					i=i+1
					lnqtystarts =atc("*",a[i])
					thisqty=  substr(a[i],lnqtystarts+1)
					lnsubqty = lnsubqty +val(thisqty)
					insert into upcdata (ship_ref,outshipid,outdetid,upc,style,color,linenum,lineqty) values (lcpt,thisoutshipid,toutdet.outdetid,toutdet.upc,toutdet.style,toutdet.color,thisline,val(thisqty))
				endif
			next i

			if llsubline
				mainlineqty = toutdet.mlqty - lnsubqty
				insert into upcdata (ship_ref,outshipid,outdetid,upc,style,color,linenum,lineqty) values (lcpt,thisoutshipid,toutdet.outdetid,toutdet.upc,outdet.style,toutdet.color,toutdet.linenum,mainlineqty)
			else
				insert into upcdata (ship_ref,outshipid,outdetid,upc,style,color,linenum,lineqty) values (lcpt,thisoutshipid,toutdet.outdetid,toutdet.upc,outdet.style,toutdet.color,toutdet.linenum,toutdet.mlqty)
			endif
		endscan
*!*			ELSE

*!*				RETURN

*!*			ENDIF

*** now the UPCdata cursor has the linenum and qty data, we scan through UCCs a =and simply decrement according to the SQL Carton/detail/totqty value
		select upcdata
		replace all remainqty with lineqty for lineqty > 0
		if ltesting
*		BROWSE
		endif


		if ltesting
*			SET STEP ON
*			lcQuery  = [select ucc,upc,style,color,pack,sum(totqty) as pqty from dbo.labels where ship_ref=']+lcPT+[' and ucc != 'CUTS' and accountid = 6532  group by ucc,upc,style,color,pack]
			select ucc,upc,style,color,pack,sum(totqty) as pqty from variatpp where ship_ref=lcpt and ucc # 'CUTS' and accountid = 6532  group by ucc,upc,style,color,pack into cursor tempdata
		else
			if usesql()
				xsqlexec("select ucc, upc, style, color, pack, sum(totqty) as pqty from cartons where ship_ref='"+lcpt+"' " + ;
					"and ucc#'CUTS' and accountid=6532 group by ucc, upc, style, color, pack","tempdata",,"pickpack")
			else
				lcquery  = [select ucc,upc,style,color,pack,sum(totqty) as pqty from dbo.cartons where ship_ref=']+lcpt+[' and ucc != 'CUTS' and accountid = 6532  group by ucc,upc,style,color,pack]
				if sqlexec(nhandle,lcquery,"tempdata")#1
					wait window at 6,2 "Error Cartons SQL query.........." timeout 1
					sqldisconnect(nhandle)
				endif
			endif
		endif

		select tempdata
		select distinct ucc from tempdata where ucc != 'CUTS' into cursor tuccs  && no CUTS ever, these are scanpack shorts
		if ltesting
			locate
*			BROWSE
		endif

		lxctr =0

		select tuccs
		scan

			select distinct upc from tempdata where ucc = tuccs.ucc into cursor tupcs

			select tupcs
			scan

				lxctr = lxctr +1

				select tempdata
				scan for tempdata.ucc = tuccs.ucc and tempdata.upc = tupcs.upc

					thisupcqty = tempdata.pqty

					do while thisupcqty !=0 && here we will completely ignoe any UPC value where totqty =0, no worry about "NOQTY" type cartons

						select upcdata
						locate for alltrim(upcdata.upc) = alltrim(tupcs.upc) and remainqty >0
						if found()
							do case
							case upcdata.remainqty >= thisupcqty
								thispackqty = thisupcqty
								replace upcdata.remainqty with upcdata.remainqty - thisupcqty
								thisupcqty = 0
								thislinenum = upcdata.linenum
							case upcdata.remainqty < thisupcqty  && looks like this line is now depleted
								thisupcqty = thisupcqty - upcdata.remainqty
								thispackqty = upcdata.remainqty
								replace upcdata.remainqty with 0
								thislinenum = upcdata.linenum
							endcase
						else
							set step on
							cerrmsg = "ODET/CTNS QY MTCH AT "+cship_ref+" and UPC "+alltrim(tupcs.upc)
							do ediupdate with cerrmsg,.t.
							throw
						endif

						store "LX"+cfd+alltrim(str(lxctr))+csegd to cstring   && Carton Seq. # (up to Carton total)
						do cstringbreak
						nsegctr = nsegctr + 1

						store "MAN"+cfd+"GM"+cfd+tuccs.ucc+csegd to cstring
						do cstringbreak
						nsegctr = nsegctr + 1

						store "W12"+cfd+"CL"+cfd+cfd+alltrim(str(thispackqty))+cfd+cfd+"EA"+cfd+alltrim(tempdata.upc)+replicate(cfd,4)+"5.0"+cfd+"G"+cfd+"L"+csegd to cstring
						do cstringbreak
						nsegctr = nsegctr + 1
						nunitsum = nunitsum  +thispackqty  && just added this

						store "N9"+cfd+"LI"+cfd+alltrim(thislinenum)+csegd to cstring
						do cstringbreak
						nsegctr = nsegctr + 1

						do case
						case lparceltype and lworldeasefreight = .f.
							if seek(tuccs.ucc,"package","ucc")
								lctrknumber = alltrim(package.trknumber)
								lctrknumber = iif(empty(lctrknumber),cbolfill,lctrknumber)
							else
								lctrknumber = "UNK"
							endif

						case lworldeasefreight = .t.
							if xsqlexec("select * from ctnucc where mod='K' and ucc='"+tuccs.ucc+"'",,,"wh") # 0
								lctrknumber = alltrim(ctnucc.serialno)
								lctrknumber = iif(empty(lctrknumber),cbolfill,lctrknumber)
							else
								lctrknumber = "UNK"
							endif

						otherwise
							lctrknumber = cbolfill
						endcase

*!*							IF lParcelType And lWorldeaseFreight = .f.
*!*								=SEEK(tuccs.ucc,"package","ucc")
*!*								IF FOUND("package")
*!*									lcTrknumber = ALLTRIM(package.trknumber)
*!*									lcTrknumber = IIF(EMPTY(lcTrknumber),cBOLFill,lcTrknumber)
*!*								ELSE
*!*									lcTrknumber = "UNK"
*!*								ENDIF
*!*							ELSE
*!*								lcTrknumber = cBOLFill
*!*							ENDIF

						store "N9"+cfd+"TR"+cfd+alltrim(lctrknumber)+csegd to cstring
						do cstringbreak
						nsegctr = nsegctr + 1

					enddo  && do each UPC/line number

				endscan  && tempdata.ucc = tuccs.ucc And tempdata.upc = tupcs.upc, scan through the UPCs in this cartons

			endscan  && scan through the UCC cartons on this pick ticket

		endscan  && toutdet.outshipid = thisoutshipid And units, scan throuh this pick ticket


&& now we need to scan through the UPC/line detail cursor  an look for any UPC/line num
*    If lTesting
		lxctr = lxctr +1
		select upcdata
		scan for upcdata.lineqty=upcdata.remainqty and !empty(upcdata.lineqty)
			store "LX"+cfd+alltrim(str(lxctr))+csegd to cstring   && Carton Seq. # (up to Carton total)
			do cstringbreak
			nsegctr = nsegctr + 1

			store "MAN"+cfd+"GM"+cfd+"NOQTY"+csegd to cstring
			do cstringbreak
			nsegctr = nsegctr + 1

			store "W12"+cfd+"CL"+cfd+cfd+"0"+cfd+cfd+"EA"+cfd+alltrim(upcdata.upc)+replicate(cfd,4)+"0.0"+cfd+"G"+cfd+"L"+csegd to cstring
			do cstringbreak
			nsegctr = nsegctr + 1

			store "N9"+cfd+"LI"+cfd+alltrim(upcdata.linenum)+csegd to cstring
			do cstringbreak
			nsegctr = nsegctr + 1

			store "N9"+cfd+"TR"+cfd+"NONE"+csegd to cstring
			do cstringbreak
			nsegctr = nsegctr + 1
			lxctr = lxctr +1
		endscan
*   Endif
** scan for

*************************************************************************
*2^	END DETAIL MATCHING SHIP_REF LOOP
*************************************************************************
		wait clear
		wait window "Now creating Section Closure information" nowait noclear
		if ltesting
		endif

		if lparceltype and ltesting
			ntotctnwt = iif(empty(ntotctnwt) or isnull(ntotctnwt),5,ntotctnwt)
			store "W03"+cfd+alltrim(str(nunitsum))+cfd+alltrim(str(ntotctnwt))+cfd+;
			cweightunit+cfd+alltrim(str(ceiling(ntotctnvol)))+cfd+cvolunit+csegd to cstring
		else-
*			nTotCtnWt = IIF(ISNULL(nTotCtnWt) AND csendid = "TGFTEST",25,nTotCtnWt)
			ntotctnwt = int(outship.weight) && Added on 05.21.2014 per Paul to replace ^
			store "W03"+cfd+alltrim(str(nunitsum))+cfd+alltrim(str(ntotctnwt))+cfd+;
			cweightunit+csegd to cstring
		endif
		fputs(nfilenum,cstring)

		ntotctnwt = 0
		ntotctnvol = 0
		ntotctncount = 0
		nunitsum = 0
		store  "SE"+cfd+alltrim(str(nsegctr+2))+cfd+padl(c_grpcntrlnum,9,"0")+csegd to cstring
		fputs(nfilenum,cstring)

		select outship
		wait clear
	endscan


*************************************************************************
*1^	END OUTSHIP MAIN LOOP
*************************************************************************

	do close945
	=fclose(nfilenum)
	sqldisconnect(nhandle)
	select tempack
	if ltesting
		locate
		brow
		set step on
	endif
**************commented out 1/9/17 TMARG
*!*		SELECT ackdata
*!*		APPEND FROM DBF('tempack')
	use in tempack
	use in ackdata


	if !ltesting
		select edi_trigger
		do ediupdate with "945 CREATED",.f.
		select edi_trigger
		locate

		if !used("ftpedilog")
			select 0
			use f:\edirouting\ftpedilog alias ftpedilog
			insert into ftpedilog (transfer,ftpdate,filename,acct_name,type) values ("945-"+ccustname+"-"+ccustloc,dt2,cfilenamehold,upper(ccustname),"945")
			use in ftpedilog
		endif
	endif

	wait clear
	wait window ccustfolder+" 945 Process complete..." nowait

*!* Create eMail confirmation message
	if ltestmail
		select 0
		use f:\3pl\data\mailmaster alias mm
		locate for mm.edi_type = "MISC" and mm.taskname = "GENERAL"
		tsendto = iif(mm.use_alt,mm.sendtoalt,mm.sendto)
		tcc = "" && Iif(mm.use_alt,mm.ccalt,mm.cc)
		use in mm
	endif

	tsubject = cmailname+iif(lscanpack," (S-P)","")+" 945 EDI File from TGF as of "+dtmail+" ("+ccustloc+")"+iif(ltesting," *TEST*","")
	tattach = " "
	tmessage = "(S-P) 945 EDI Info from TGF, file "+cfilenameshort+chr(13)
	tmessage = tmessage + "Division "+cdivision+", BOL# "+trim(cbol)+chr(13)
	tmessage = tmessage + "Work Orders: "+cwo_numstr+","+chr(13)
	tmessage = tmessage + "containing these picktickets:"+chr(13)+chr(13)
	tmessage = tmessage + cptstring + chr(13)+chr(13)
	tmessage = tmessage +"has been created and will be transmitted ASAP."+chr(13)+chr(13)
	if !empty(cmissdel)
		tmessage = tmessage+chr(13)+chr(13)+cmissdel+chr(13)+chr(13)
	endif
	tmessage = tmessage + "If you have any questions, please eMail or call 732-750-9000 x217."
	if lemail
		do form m:\dev\frm\dartmail2 with tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
	endif

	wait clear
	wait window cmailname+" 945 EDI File output complete" at 20,60 nowait

*!* Transfers files to correct output folders
	if !ltesting
		copy file [&cFilenameHold] to [&cFilenameArch]
	endif

	set step on
	if ldoariatfilesout && AND !lTesting
		copy file [&cFilenameHold] to [&cFilenameOut]
		delete file [&cFilenameHold]
		select temp945
		copy to "f:\3pl\data\temp945a.dbf"
		select 0
		use "f:\3pl\data\temp945a.dbf" alias temp945a
		select 0
		use "f:\3pl\data\pts_sent945.dbf" alias pts_sent945
		append from "f:\3pl\data\temp945a.dbf"
		use in pts_sent945
		use in temp945a
		delete file "f:\3pl\data\temp945a.dbf"
	endif

	if !ltesting
*!*		asn_out_data()
	endif

	release all like c_cntrlnum,c_grpcntrlnum

	use in temp945

catch to oerr
	set step on
	if ldocatch
		wait clear
*		ASSERT .F. MESSAGE "In Error CATCH"
		if cerrmsg != "EARLY ERROR"

*		lEmail = .F.
			do ediupdate with iif(empty(cerrmsg),"ERRHAND ERROR",cerrmsg),.t.

		endif
		tsubject = ccustname+" Error ("+transform(oerr.errorno)+") at "+ttoc(datetime())
		tattach  = ""
		if ltesting
			tsendto  = tsendtotest
			tcc = tcctest
		else
			tsendto  = tsendtoerr
			tcc = tccerr
		endif

		tmessage = ccustname+" Error processing "+chr(13)

		tmessage =tmessage+ "Try/CATCH Exeception Message:"+chr(13)+;
		[  Error: ] + str(oerr.errorno) +chr(13)+;
		[  LineNo: ] + str(oerr.lineno) +chr(13)+;
		[  Message: ] + oerr.message +chr(13)+;
		[  Procedure: ] + oerr.procedure +chr(13)+;
		[  Details: ] + oerr.details +chr(13)+;
		[  StackLevel: ] + str(oerr.stacklevel) +chr(13)+;
		[  LineContents: ] + oerr.linecontents+chr(13)+;
		[  UserValue: ] + oerr.uservalue

		tsubject = "945 EDI Poller Error at "+ttoc(datetime())
		tattach  = ""
		tcc=""
		do form m:\dev\frm\dartmail2 with tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
		fclose(nfilenum)
		sqldisconnect(nhandle)
	endif

finally
	if used('OUTSHIP')
		use in outship
	endif
	if used('OUTDET')
		use in outdet
	endif
	if used('SHIPMENT')
		use in shipment
	endif
	if used('PACKAGE')
		use in package
	endif
	if used('SERFILE')
		use in serfile
	endif
	if used('OUTWOLOG')
		use in outwolog
	endif

	wait clear

endtry

*** END OF CODE BODY



****************************
procedure close945
****************************
** Footer Creation

store  "GE"+cfd+alltrim(str(nstcount))+cfd+c_cntrlnum+csegd to cstring
fputs(nfilenum,cstring)

store  "IEA"+cfd+"1"+cfd+padl(c_cntrlnum,9,"0")+csegd to cstring
fputs(nfilenum,cstring)

return

****************************
procedure num_incr_isa
****************************

if !used("serfile")
	use ("f:\3pl\data\serial\"+ccustname+"945_serial") in 0 alias serfile
endif
select serfile
if ltesting and lisaflag
	norigseq = serfile.seqnum
	lisaflag = .f.
endif
nisa_num = serfile.seqnum
c_cntrlnum = alltrim(str(serfile.seqnum))
replace serfile.seqnum with serfile.seqnum + 1 in serfile
select outship
return

****************************
procedure num_incr_st
****************************

if !used("serfile")
	use ("f:\3pl\data\serial\"+ccustname+"945_serial") in 0 alias serfile
endif
select serfile
if ltesting and lstflag
	noriggrpseq = serfile.grpseqnum
	lstflag = .f.
endif
c_grpcntrlnum = alltrim(str(serfile.grpseqnum))
replace serfile.grpseqnum with serfile.grpseqnum + 1 in serfile
select outship
endproc

****************************
procedure segmentget
****************************

parameter thisarray,lckey,nlength

for i = 1 to nlength
	if i > nlength
		exit
	endif
	lnend= at("*",thisarray[i])
	if lnend > 0
		lcthiskey =trim(substr(thisarray[i],1,lnend-1))
		if occurs(lckey,lcthiskey)>0
			return substr(thisarray[i],lnend+1)
			i = 1
		endif
	endif
endfor

return ""

****************************
procedure ediupdate
****************************
parameter cstatus,liserror
ldocatch = .f.

if !ltesting
	select edi_trigger
	nrec = recno()
	locate
	if cstatus = "NO 945 NEEDED - UPSGCC"  && Manually processed, per Mike Winter
		replace processed with .t.,proc945 with .t.,file945 with cfilenamehold,isa_num with cisa_num,;
		fin_status with cstatus,errorflag with .f.,when_proc with datetime() ;
		for edi_trigger.bol = cbol and accountid = nacctnum
	else
		if !liserror
			replace processed with .t.,proc945 with .t.,file945 with cfilenamehold,isa_num with cisa_num,;
			fin_status with "945 CREATED",errorflag with .f.,when_proc with datetime() ;
			for edi_trigger.bol = cbol and accountid = nacctnum
		else
			replace processed with .t.,proc945 with .f.,file945 with cfilenamehold,;
			fin_status with cstatus,errorflag with .t. ;
			for edi_trigger.bol = cbol and accountid = nacctnum
			if lcloseoutput
				=fclose(nfilenum)
				erase &cfilenamehold
			endif
		endif
	endif
endif

set step on
if liserror and lemail and cstatus<>"SQL ERROR"
	tsubject = "945 Error in ARIAT BOL "+trim(cbol)+"(At PT "+cship_ref+")"
	tattach = " "
	tsendto = tsendtoerr
	tcc = tccerr
	tmessage = "945 Processing for BOL# "+cbol+", WO# "+cwo_num+" produced this error: (Office: "+coffice+"): "+cstatus+chr(13)+;
	iif(cstatus = "BAD BOL#, PT #  ","Please add tracking number to Amazon field or email to Joe","Check EDI_TRIGGER and re-run")
	if "TOTQTY ERR"$cstatus
		tmessage = tmessage + chr(13) + "At OUTSHIPID: "+alltrim(str(m.outshipid))
	endif
	do form m:\dev\frm\dartmail2 with tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
endif

if used('outship')
	use in outship
endif
if used('outdet')
	use in outdet
endif

if used('serfile')
	select serfile
	if ltesting
		replace serfile.seqnum with norigseq
		replace serfile.grpseqnum with noriggrpseq
	endif
	use in serfile
endif
if used('scacs')
	use in scacs
endif
if used('mm')
	use in mm
endif
if used('tempx')
	use in tempx
endif
if used('vbug')
	use in vbug
endif
if !ltesting
	select edi_trigger
	locate
endif

endproc

****************************
procedure cstringbreak
****************************
clen = len(alltrim(cstring))
if isnull(cstring)
	assert .f. message "At cStringBreak procedure"
endif
fputs(nfilenum,cstring)
endproc


****************************
procedure cszbreak
****************************
ccsz = allt(m.csz)

for ii = 5 to 2 step -1
	ccsz = strtran(ccsz,space(ii),space(1))
endfor
ccsz = strtran(ccsz,",","")
len1 = len(allt(ccsz))
nspaces = occurs(" ",ccsz)

if nspaces<2
	cerrmsg = "BAD CSZ INFO"
	do ediupdate with cerrmsg,.t.
	throw
endif

*	ASSERT .F. MESSAGE "In CSZ Breakout"
for ii = nspaces to 1 step -1
	ii1 = allt(str(ii))
	do case
	case ii = nspaces
		npos = at(" ",ccsz,ii)
		czip = allt(substr(ccsz,npos))
		nendstate = npos
	case ii = (nspaces - 1)
		npos = at(" ",ccsz,ii)
		cstate = allt(substr(ccsz,npos,nendstate-npos))
		if nspaces = 2
			ccity = allt(left(ccsz,npos))
			exit
		endif
	otherwise
		npos = at(" ",ccsz,ii)
		ccity = allt(left(ccsz,npos))
*				WAIT WINDOW "CITY: "+cCity TIMEOUT 2
	endcase
endfor
endproc

*******************************************************************************************************
*!*	PROCEDURE scacenter
*!*	USE wh.scacenter ALIAS scacenter
*!*	replace scacenterdt WITH DATETIME(),scacenterby WITH IIF(TYPE("guserid")="C" AND !EMPTY(guserid),guserid,"???")
*!*	endproc
