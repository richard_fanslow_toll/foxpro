CLOSE DATABASES ALL

DO M:\DEV\PRG\_SETVARS WITH .T.
lTesting = .F.
_SCREEN.WINDOWSTATE=IIF(lTesting,2,1)
SET STEP ON 

SELECT 0
USE "f:\edirouting\ftpsetup"
REPLACE chkbusy WITH .T. FOR transfer = "997_317-TJX-RENAME" IN ftpsetup

WAIT WINDOW "Now renaming TJX 997/317 output files" NOWAIT

CD "F:\ftpusers\TJMAX\997-317RENAME\"

cFilename = "F:\ftpusers\TJMAX\997-317RENAME\997-317.edi"
cOutname = ("997TJX317"+TTOC(DATETIME(),1)+".edi")
IF !FILE(cFilename)
	WAIT WINDOW "No TJX 997/317 files to output" TIMEOUT 1
	CLOSE DATA ALL
	RETURN
ELSE
	WAIT WINDOW "TJX 997/317 file processing" TIMEOUT 1
ENDIF
cOutfolder = IIF(lTesting,"f:\ftpusers\tjmax\testout\","F:\FTPUSERS\TJMAX\EXPEDITORS\OUT\")

cOutfile = (cOutfolder+cOutname)
cArchivefile = ("F:\FTPUSERS\TJMAX\997-317rename\archive\"+cOutname)

cString = FILETOSTR(cFilename)
STRTOFILE(cString,cArchivefile)
STRTOFILE(cString,cOutfile)

IF FILE(cFilename)
	DELETE FILE [&cFilename]
ENDIF

SELECT ftpsetup
REPLACE chkbusy WITH .F. FOR transfer = "997_317-TJX-RENAME" IN ftpsetup

CLOSE DATABASES ALL
RETURN
