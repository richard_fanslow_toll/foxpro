* validate WMS data - runs on Sunday
 
utilsetup("VAL13")

store "" to guserid, gprocess
gdevelopment=.f.
xjobno=0
xsystem="VAL13"

external array xdbf

use f:\auto\val13 exclusive
zap
use

*

valset("remove palletid from inloc for RACK locations")

release guserid

select whoffice
scan for !test and !novalid and wireless
	xoffice=whoffice.office
	goffice=whoffice.office
	xfolder=whoffice.folder+"\whdata\"

	xsqlexec("select * from inloc where mod='"+goffice+"' and whseloc='RACK' and !empty(palletid)",,,"wh")

	replace palletid with "" in inloc for whseloc="RACK" and !empty(palletid)

	use in inloc
endscan

*

valset("check for tables with a deleted record #1")

set deleted off
dimension xdbf[1]

create cursor xdelrec (office c(1), ztable c(15), packed l)
select 2

select whoffice
scan for !test and !novalid and !deleted()
	xoffice=whoffice.office
	goffice=xoffice
	xfolder=whoffice.folder+"\whdata\"
	m.office=xoffice

	adir("xdbf",xfolder+"*.dbf")

	for i=1 to alen(xdbf,1)
		select 0
		try
			use (xfolder+xdbf[i,1])

			if deleted() and !inlist(xdbf[i,1],"WOSEMI","PT","PTDET","PICKS","ALREADYPICKED","TOBEPICKED")
				m.ztable=strtran(xdbf[i,1],".DBF","")
				insert into xdelrec from memvar
				
				try 
					use (xfolder+xdbf[i,1]) exclusive
					pack
					replace packed with .t. in xdelrec
				catch
				endtry
			endif		
		catch
		endtry
		
		use
	next
endscan

select xdelrec
if reccount("xdelrec")>0
	copy to f:\auto\xdelrec
	count for packed
	xfixed=iif(_tally=reccount()," (fixed)","")
	email("Dyoung@fmiint.com","VAL"+xfixed+" "+transform(reccount("xdelrec"))+" tables with a deleted record #1","xdelrec",,,,.t.,,,,,.t.)
endif

*

use f:\auto\val13
go bottom
replace zendtime with datetime()
replace all seconds with zendtime-zstarttime
use 

schedupdate()

_screen.Caption=gscreencaption
on error
