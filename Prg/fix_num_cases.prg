*RETURN
CLOSE DATABASES ALL

* for particular RCN, change num_cases to 1, or -1 if it was negative

SET DELETED ON
SET EXCLUSIVE OFF
SET TALK OFF

WAIT WINDOW NOWAIT "Fixing numcases...."

LOCAL lnReplaces, lnReplaces2, lnRCN

*lnRCN = 81285

* 1st do RCs
lnReplaces = 0
USE F:\FOOTSTAR\PMDATA\RCRCHIST
*SCAN FOR (rcn = lnRCN) AND (NOT (num_cases = 1))
SCAN FOR (NOT (num_cases = 1))
	DO CASE
		CASE num_cases < -1
			REPLACE num_cases WITH -1
			lnReplaces = lnReplaces + 1
		CASE num_cases = 0
			REPLACE num_cases WITH 1
			lnReplaces = lnReplaces + 1
		CASE num_cases > 1
			REPLACE num_cases WITH 1
			lnReplaces = lnReplaces + 1
		OTHERWISE
			*nothing
	ENDCASE

ENDSCAN

CLOSE DATABASES ALL


*do RZs
lnReplaces2 = 0
USE F:\FOOTSTAR\PMDATA\RCRZHIST
*SCAN FOR (rcn = lnRCN) AND (NOT (num_cases = 1))
SCAN FOR (NOT (num_cases = 1))
	DO CASE
		CASE num_cases < -1
			REPLACE num_cases WITH -1
			lnReplaces2 = lnReplaces2 + 1
		CASE num_cases = 0
			REPLACE num_cases WITH 1
			lnReplaces2 = lnReplaces2 + 1
		CASE num_cases > 1
			REPLACE num_cases WITH 1
			lnReplaces2 = lnReplaces2 + 1
		OTHERWISE
			*nothing
	ENDCASE

ENDSCAN

CLOSE DATABASES ALL

MESSAGEBOX("# of RC adjustments = " + TRANSFORM(lnReplaces))
MESSAGEBOX("# of RZ adjustments = " + TRANSFORM(lnReplaces2))

WAIT clear

*SELECT ttod(tdatetime), rcn, count(*), avg(num_cases), sum(num_cases) FROM F:\FOOTSTAR\PMDATA\RCRCHIST.DBF where num_cases > 1 GROUP BY 1, 2



RETURN

