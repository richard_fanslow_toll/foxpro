* run GREAT WEST Payroll Data Interchange FOR LATEST ADP PAYDATE
* modified 7/16/07 to be run AFTER a paydate, usually running on Wednesday for prior Friday, per Karen.
* This change was made to properly reflect corrections to 401k deductions made after the actual paydate.

* build EXE as F:\UTIL\GREATWEST\GREATWESTPDI.EXE

runack("GREATWESTPDI")

LOCAL loGreatWestPDI
loGreatWestPDI = CREATEOBJECT('GreatWestPDI')
loGreatWestPDI.MAIN()
CLOSE DATABASES ALL
RETURN

#DEFINE LOGIT 1
#DEFINE WAITIT 2
#DEFINE NOWAITIT 4
#DEFINE SENDIT 8
#DEFINE CRLF CHR(13) + CHR(10)
#DEFINE RETURN_DATA_MANDATORY .F.
#DEFINE RETURN_DATA_NOT_MANDATORY .T.
#DEFINE CSVFORMAT 6
#DEFINE WRITE_ONLY_UNBUFFERED 11
#DEFINE COMMA ","

DEFINE CLASS GreatWestPDI AS CUSTOM

	cProcessName = 'GreatWestPDI'

	lTestMode = .F.  && .F. FOR NORMAL OPERATION, .T. TO SUPPRESS ALL BUT DEVELOPER EMAILS AND CHANGE INPUT TABLE, ETC.

	* special processing properties
	**********************************************
	lUseDateRange = .F.
	* the following only apply if above prop is .T.
	cRangeStartDate = "2007-01-01"
	cRangeEndDate   = "2007-12-31"
	**********************************************
	lGetMissingDemoGraphicsFromFoxPro = .T.

	dToday = DATE()
	* forcing to a particular date will make report think that is the run date -- remember it should be AFTER the paydate you want to report on
	*dToday = {^2009-04-08}

	lAutoYield = .T.

	cStartTime = TTOC(DATETIME())

	* deduction properties
	cDedCode = ''

	* connection properties
	nSQLHandle = 0

	* output file properties
	nCurrentOutputCSVFileHandle = -1

	* wait window properties
	nWaitWindowTimeout = 2
	lWaitWindowIsOn = .T.

	* logfile properties
	lLoggingIsOn = .T.
	cLogFile = 'F:\UTIL\GreatWest\LOGFILES\GREAT_WEST_PDI_log.txt'

	* Deds K/O/N properties
	lGetOnlyLatestPaydate = .T.

	* INTERNAL email properties
	lSendInternalEmailIsOn = .T.
	cFrom = 'TGF Corporate <fmicorporate@fmiint.com>'
	cSendTo = 'Rebecca Selawsky <RSelawsky@fmiint.com>'
	cCC = 'Karen Waldrip <kwaldrip@fmiint.com>, Mark Bennett <mbennett@fmiint.com>'
	cSubject = 'Great West Payroll Data Interchange File'
	cAttach = ''
	cBodyText = ''

	cCOMPUTERNAME = ''
	cUSERNAME = ''


	FUNCTION INIT
		IF NOT DODEFAULT()
			RETURN .F.
		ENDIF
		WITH THIS
			*SET RESOURCE OFF
			CLOSE DATA
			SET CENTURY ON
			SET DATE AMERICAN
			SET HOURS TO 24
			SET ANSI ON
			SET TALK OFF
			SET DELETED ON
			SET CONSOLE OFF
			SET EXCLUSIVE OFF
			SET SAFETY OFF
			SET EXACT OFF
			SET STATUS BAR ON
			SET SYSMENU OFF
			SET ENGINEBEHAVIOR 70
			_VFP.AUTOYIELD = .lAutoYield
			.cCOMPUTERNAME = GETENV("COMPUTERNAME")
			.cUSERNAME = GETENV("USERNAME")
			IF .lTestMode THEN
				.cLogFile = 'F:\UTIL\GreatWest\LOGFILES\GREAT_WEST_PDI_log_TESTMODE.txt'
				.cSendTo = 'Mark Bennett <mbennett@fmiint.com>'
				.cCC = ''
			ENDIF
			.lLoggingIsOn = .lLoggingIsOn AND NOT EMPTY(.cLogFile)
			IF .lLoggingIsOn THEN
				SET ALTERNATE TO (.cLogFile) ADDITIVE
				SET ALTERNATE ON
			ENDIF
		ENDWITH
	ENDFUNC


	FUNCTION DESTROY
		WITH THIS
			IF .lLoggingIsOn  THEN
				SET ALTERNATE OFF
				SET ALTERNATE TO
			ENDIF
		ENDWITH
		DODEFAULT()
	ENDFUNC


	FUNCTION MAIN
		WITH THIS
			LOCAL lnNumberOfErrors, lcSQL, lcSQL2, lcSQL3, loError, lnPayMonth, lnPayYear, lnPayDay
			LOCAL ldEndingPaydate, lcFiletoSaveAs, lcSpreadsheetTemplate
			LOCAL oExcel, oWorkbook, oWorksheet
			LOCAL lcOutputFilename, lcOutputLine
			LOCAL lcPlanNumber, lcSSN, lcDivNum, lcLast, lcFirst, lcMiddle, lcSuffix, lcBirthDate, lcGender
			LOCAL lcMarital, lcAddress1, lcAddress2, lcCity, lcState, lcZip, lcHomePhone, lcWorkPhone, lcWorkPhoneExt, lcCountryCode
			LOCAL ldHireDate, lcTermDate, lcRehireDate, lcEEContribution, lcEmployerMatch, lcLoanRepayment, lcYTDHoursWorked, lcYTDTotCompensation, lcYTDPlanComp
			LOCAL lcPreContrib, lcHighComp, lcBlanks, lcOfficer, lcPartDate, lcEligibleCode

			TRY
				lnNumberOfErrors = 0

				.TrackProgress('', LOGIT)
				.TrackProgress('', LOGIT)
				.TrackProgress('=========================================================', LOGIT+SENDIT)
				.TrackProgress('GREAT WEST PDI process started....', LOGIT+SENDIT+NOWAITIT)
				.TrackProgress('COMPUTERNAME = ' + .cCOMPUTERNAME, LOGIT+SENDIT)
				.TrackProgress('USERNAME = ' + .cUSERNAME, LOGIT+SENDIT)
				.TrackProgress('=========================================================', LOGIT+SENDIT)
				IF .lTestMode THEN
					.TrackProgress('                        TEST MODE                        ', LOGIT+SENDIT)
					.TrackProgress('=========================================================', LOGIT+SENDIT)
				ENDIF
				IF .lUseDateRange THEN
					.TrackProgress('                     DATE-RANGE MODE                     ', LOGIT+SENDIT)
					.TrackProgress('=========================================================', LOGIT+SENDIT)
				ENDIF
				.TrackProgress('', LOGIT)

				* okay to proceed.
				*!*					lnPayMonth = MONTH(.dToday)
				*!*					lnPayYear = YEAR(.dToday)
				*!*					lnPayDay = DAY(.dToday)

				* determine most recent Friday (i.e. payday) date
				ldEndingPaydate = .dToday
				DO WHILE DOW(ldEndingPaydate) <> 6
					ldEndingPaydate = ldEndingPaydate - 1
				ENDDO

*!*	* Hardcode to handle early pay for 4th of july
*!*	ldEndingPaydate = {^2009-07-02}

				* construct month/year values for SELECT
				lnPayMonth = MONTH(ldEndingPaydate)
				lnPayYear = YEAR(ldEndingPaydate)
				lnPayDay = DAY(ldEndingPaydate)

				lcSQLPayDate = PADL(lnPayYear,4,"0") + "-" + PADL(lnPayMonth,2,"0") + "-" + PADL(lnPayDay,2,"0")

				* get ss#s from ADP
				OPEN DATABASE F:\UTIL\ADPCONNECTION\CONNECT1

				.nSQLHandle = SQLCONNECT("PCPAYWIN","MBENNETT","VP737A")

				IF .nSQLHandle > 0 THEN

					*!*							" AND (CHECKVIEWPAYDATE >= DATE '2005-01-01') " + ;
					*!*							" AND (CHECKVIEWPAYDATE <= DATE '2005-10-21') "

					IF .lUseDateRange THEN
						lcSQL = ;
							"SELECT " + ;
							" COMPANYCODE AS ADPCOMP, " + ;
							" NAME, " + ;
							" SOCIALSECURITY# AS SS_NUM, " + ;
							" CHECKVIEWPAYDATE AS PAYDATE, " + ;
							" CHECKVIEWDEDCODE AS DEDCODE, " + ;
							" CHECKVIEWDEDAMT AS DEDAMT, " + ;
							" FILE# AS FILENUM " + ;
							" FROM REPORTS.V_CHK_VW_DEDUCTION " + ;
							" WHERE ((CHECKVIEWDEDCODE = 'K') OR (CHECKVIEWDEDCODE = 'N')) " + ;
							" AND (CHECKVIEWPAYDATE >= DATE '" + .cRangeStartDate + "') " + ;
							" AND (CHECKVIEWPAYDATE <= DATE '" + .cRangeEndDate + "') "
					ELSE
						lcSQL = ;
							"SELECT " + ;
							" COMPANYCODE AS ADPCOMP, " + ;
							" NAME, " + ;
							" SOCIALSECURITY# AS SS_NUM, " + ;
							" CHECKVIEWPAYDATE AS PAYDATE, " + ;
							" CHECKVIEWDEDCODE AS DEDCODE, " + ;
							" CHECKVIEWDEDAMT AS DEDAMT, " + ;
							" FILE# AS FILENUM " + ;
							" FROM REPORTS.V_CHK_VW_DEDUCTION " + ;
							" WHERE ((CHECKVIEWDEDCODE = 'K') OR (CHECKVIEWDEDCODE = 'N')) " + ;
							" AND (CHECKVIEWPAYDATE = DATE '" + lcSQLPayDate + "') "
					ENDIF


					*!*								" AND {fn MONTH(CHECKVIEWPAYDATE)} = " + TRANSFORM(lnPayMonth) + ;
					*!*								" AND {fn YEAR(CHECKVIEWPAYDATE)} = " + TRANSFORM(lnPayYear)

					lcSQL2 = ;
						"SELECT " + ;
						" NAME, " + ;
						" LASTNAME, " + ;
						" FIRSTNAME, " + ;
						" SOCIALSECURITY# AS SS_NUM, " + ;
						" HOMEDEPARTMENT AS HOMEDEPT, " + ;
						" STREETLINE1 AS STREET1, " + ;
						" {fn IFNULL(STREETLINE2,' ')} AS STREET2, " + ;
						" CITY, " + ;
						" STATE, " + ;
						" ZIPCODE, " + ;
						" GENDER, " + ;
						" HIREDATE, " + ;
						" BIRTHDATE, " + ;
						" TERMINATIONDATE AS TERMDATE, " + ;
						" {FN IFNULL(ANNUALSALARY,0000000.00)} AS SALARY, " + ;
						" STATUS " + ;
						" FROM REPORTS.V_EMPLOYEE "

					IF .lUseDateRange THEN
						lcSQL3 = ;
							"SELECT " + ;
							" SOCIALSECURITY# AS SS_NUM, " + ;
							" CHECKVIEWPAYDATE AS PAYDATE, " + ;
							" {FN IFNULL(CHECKVIEWMEMOAMT,0.00)} AS K_MATCH " + ;
							" FROM REPORTS.V_CHK_VW_MEMO " + ;
							" WHERE (CHECKVIEWMEMOCD = 'K') " + ;
							" AND (CHECKVIEWPAYDATE >= DATE '" + .cRangeStartDate + "') " + ;
							" AND (CHECKVIEWPAYDATE <= DATE '" + .cRangeEndDate + "') "
					ELSE
						lcSQL3 = ;
							"SELECT " + ;
							" SOCIALSECURITY# AS SS_NUM, " + ;
							" CHECKVIEWPAYDATE AS PAYDATE, " + ;
							" {FN IFNULL(CHECKVIEWMEMOAMT,0.00)} AS K_MATCH " + ;
							" FROM REPORTS.V_CHK_VW_MEMO " + ;
							" WHERE (CHECKVIEWMEMOCD = 'K') " + ;
							" AND (CHECKVIEWPAYDATE = DATE '" + lcSQLPayDate + "') "
					ENDIF

					* COUNT # OF PAY WEEKS FOR SALARIED EE'S,
					* WILL BE USED TO CALCULATE TOTAL YTD HOURS
					IF .lUseDateRange THEN
						lcSQL4 = ;
							"SELECT " + ;
							" SOCIALSECURITY# AS SS_NUM, " + ;
							" COUNT(DISTINCT CHECKVIEWWEEK#) AS NUM_WEEKS " + ;
							" FROM REPORTS.V_CHK_VW_INFO " + ;
							" WHERE COMPANYCODE NOT IN ('SXI','E87') " + ;
							" AND (CHECKVIEWPAYDATE >= DATE '" + .cRangeStartDate + "') " + ;
							" AND (CHECKVIEWPAYDATE <= DATE '" + .cRangeEndDate + "') " + ;
							" GROUP BY SOCIALSECURITY# "
					ELSE
						lcSQL4 = ;
							"SELECT " + ;
							" SOCIALSECURITY# AS SS_NUM, " + ;
							" COUNT(DISTINCT CHECKVIEWWEEK#) AS NUM_WEEKS " + ;
							" FROM REPORTS.V_CHK_VW_INFO " + ;
							" WHERE {fn YEAR(CHECKVIEWPAYDATE)} = " + TRANSFORM(YEAR(ldEndingPaydate)) + ;
							" AND COMPANYCODE NOT IN ('SXI','E87') " + ;
							" GROUP BY SOCIALSECURITY# "
					ENDIF

					* count hours of various types for Hourly
					IF .lUseDateRange THEN
						lcSQL5 = ;
							"SELECT " + ;
							" SOCIALSECURITY# AS SS_NUM, " + ;
							" SUM({fn IFNULL(CHECKVIEWREGHOURS,0000.00)}) AS REG_HOURS, " + ;
							" SUM({fn IFNULL(CHECKVIEWOTHOURS,0000.00)}) AS OT_HOURS, " + ;
							" SUM({fn IFNULL(CHECKVIEWHOURSAMT,0000.00)}) AS OTHER_HRS " + ;
							" FROM REPORTS.V_CHK_VW_HOURS " + ;
							" WHERE COMPANYCODE IN ('SXI','E87') " + ;
							" AND (CHECKVIEWPAYDATE >= DATE '" + .cRangeStartDate + "') " + ;
							" AND (CHECKVIEWPAYDATE <= DATE '" + .cRangeEndDate + "') " + ;
							" GROUP BY SOCIALSECURITY# "
					ELSE
						lcSQL5 = ;
							"SELECT " + ;
							" SOCIALSECURITY# AS SS_NUM, " + ;
							" SUM({fn IFNULL(CHECKVIEWREGHOURS,0000.00)}) AS REG_HOURS, " + ;
							" SUM({fn IFNULL(CHECKVIEWOTHOURS,0000.00)}) AS OT_HOURS, " + ;
							" SUM({fn IFNULL(CHECKVIEWHOURSAMT,0000.00)}) AS OTHER_HRS " + ;
							" FROM REPORTS.V_CHK_VW_HOURS " + ;
							" WHERE {fn YEAR(CHECKVIEWPAYDATE)} = " + TRANSFORM(YEAR(ldEndingPaydate)) + ;
							" AND COMPANYCODE IN ('SXI','E87') " + ;
							" GROUP BY SOCIALSECURITY# "
					ENDIF

					IF .lUseDateRange THEN
						lcSQL6 = ;
							"SELECT " + ;
							" SOCIALSECURITY# AS SS_NUM, " + ;
							" SUM(CHECKVIEWGROSSPAYA) AS GROSSPAY, " + ;
							" COUNT(*) AS NUM_PAYS " + ;
							" FROM REPORTS.V_CHK_VW_INFO " + ;
							" WHERE (CHECKVIEWPAYDATE >= DATE '" + .cRangeStartDate + "') " + ;
							" AND (CHECKVIEWPAYDATE <= DATE '" + .cRangeEndDate + "') " + ;
							" GROUP BY SOCIALSECURITY# "
					ELSE
						lcSQL6 = ;
							"SELECT " + ;
							" SOCIALSECURITY# AS SS_NUM, " + ;
							" SUM(CHECKVIEWGROSSPAYA) AS GROSSPAY, " + ;
							" COUNT(*) AS NUM_PAYS " + ;
							" FROM REPORTS.V_CHK_VW_INFO " + ;
							" WHERE {fn YEAR(CHECKVIEWPAYDATE)} = " + TRANSFORM(YEAR(ldEndingPaydate)) + ;
							" GROUP BY SOCIALSECURITY# "
					ENDIF

					IF .lTestMode THEN
						.TrackProgress('                        TEST MODE                        ', LOGIT+SENDIT)
						.TrackProgress('lcSQL =' + lcSQL, LOGIT+SENDIT)
					ENDIF

					IF USED('SQLCURSOR1') THEN
						USE IN SQLCURSOR1
					ENDIF
					IF USED('CUREMPLOYEES') THEN
						USE IN CUREMPLOYEES
					ENDIF
					IF USED('CURKMATCH') THEN
						USE IN CURKMATCH
					ENDIF
					IF USED('CURYTDSALARY') THEN
						USE IN CURYTDSALARY
					ENDIF
					IF USED('CURYTDHOURLY') THEN
						USE IN CURYTDHOURLY
					ENDIF
					IF USED('CURYTDPAY') THEN
						USE IN CURYTDPAY
					ENDIF

					IF .ExecSQL(lcSQL, 'SQLCURSOR1', RETURN_DATA_MANDATORY) ;
							AND .ExecSQL(lcSQL2, 'CUREMPLOYEES', RETURN_DATA_MANDATORY) ;
							AND .ExecSQL(lcSQL3, 'CURKMATCH', RETURN_DATA_MANDATORY) ;
							AND .ExecSQL(lcSQL4, 'CURYTDSALARY', RETURN_DATA_MANDATORY) ;
							AND .ExecSQL(lcSQL5, 'CURYTDHOURLY', RETURN_DATA_MANDATORY) ;
							AND .ExecSQL(lcSQL6, 'CURYTDPAY', RETURN_DATA_MANDATORY) THEN

						*!*								SELECT SQLCURSOR1
						*!*								BROWSE

						IF USED('SQLCURSOR2') THEN
							USE IN SQLCURSOR2
						ENDIF

						* populate nulls
						SELECT CUREMPLOYEES
						SCAN
							IF ISNULL(CUREMPLOYEES.BIRTHDATE) THEN
								REPLACE CUREMPLOYEES.BIRTHDATE WITH {}
							ENDIF
							IF ISNULL(CUREMPLOYEES.HIREDATE) THEN
								REPLACE CUREMPLOYEES.HIREDATE WITH {}
							ENDIF
						ENDSCAN
						LOCATE

						** setup file names

						lcSpreadsheetTemplate = "M:\MarkB\templates\GREAT_WEST_TEMPLATE.XLS"

						IF .lUseDateRange THEN
							lcFiletoSaveAs = "F:\UTIL\GreatWest\REPORTS\MetLife PDI File for " + .cRangeStartDate + " thru " + .cRangeEndDate + ".XLS"
							.cSubject = 'Great West Payroll Data Interchange File, for Date Range: ' + .cRangeStartDate + ' thru ' + .cRangeEndDate
						ELSE
							lcFiletoSaveAs = "F:\UTIL\GreatWest\REPORTS\MetLife PDI File for " + STRTRAN(DTOC(ldEndingPaydate),"/","-") + ".XLS"
							.cSubject = 'Great West Weekly Report, for Pay Date: ' + DTOC(ldEndingPaydate)
						ENDIF

						IF FILE(lcFiletoSaveAs) THEN
							DELETE FILE (lcFiletoSaveAs)
						ENDIF


						* sum deductions by ss_num, date, paycode for latest date
						* (summing only because their *might* conceivably be more than one check per date,
						* and we don't want to miss anything in later lookups)
						IF USED('SQLCURSOR2') THEN
							USE IN SQLCURSOR2
						ENDIF

						SELECT ;
							SS_NUM, ;
							DEDCODE, ;
							SUM(DEDAMT) AS DEDAMT ;
							FROM SQLCURSOR1 ;
							INTO CURSOR SQLCURSOR2 ;
							GROUP BY 1, 2


						* roll up matches for latest paydate
						IF USED('CURKMATCH2') THEN
							USE IN CURKMATCH2
						ENDIF

						SELECT ;
							SS_NUM, ;
							SUM(K_MATCH) AS K_MATCH ;
							FROM CURKMATCH ;
							INTO CURSOR CURKMATCH2 ;
							GROUP BY 1

						*!*		SELECT CURKMATCH2
						*!*		BROWSE


						*!*							SELECT SQLCURSOR2
						*!*							GOTO TOP
						*!*							BROWSE

						* now create main report cursor of distinct ss#s from sqlcursor1
						IF USED('CURMETLIFE') THEN
							USE IN CURMETLIFE
						ENDIF

						SELECT ;
							SS_NUM, ;
							ADPCOMP, ;
							SPACE(30) AS NAME, ;
							SPACE(20) AS LASTNAME, ;
							SPACE(20) AS FIRSTNAME, ;
							0000.00 AS K_AMOUNT, ;
							0000.00 AS K_MATCH, ;
							0000.00 AS N_AMOUNT, ;
							00000000000.00 AS YTDPAY, ;
							00000000000.00 AS SALARY, ;
							000000 AS YTDHOURS, ;
							SPACE(2) AS DIVISION, ;
							SPACE(30) AS STREET1, ;
							SPACE(30) AS STREET2, ;
							SPACE(20) AS CITY, ;
							SPACE(2) AS STATE, ;
							SPACE(10) AS ZIPCODE, ;
							SPACE(1) AS GENDER, ;
							SPACE(1) AS STATUS, ;
							{} AS HIREDATE, ;
							{} AS BIRTHDATE, ;
							{} AS TERMDATE ;
							FROM SQLCURSOR1 ;
							INTO CURSOR CURMETLIFE ;
							GROUP BY SS_NUM ;
							READWRITE

						SELECT CURMETLIFE
						SCAN
							* get Ded K amount
							SELECT SQLCURSOR2
							LOCATE FOR (SS_NUM = CURMETLIFE.SS_NUM) AND (DEDCODE = 'K')
							IF FOUND() THEN
								REPLACE CURMETLIFE.K_AMOUNT WITH SQLCURSOR2.DEDAMT
							ENDIF
							LOCATE


							* get 401k Match amount
							SELECT CURKMATCH2
							LOCATE FOR (SS_NUM = CURMETLIFE.SS_NUM)
							IF FOUND() THEN
								REPLACE CURMETLIFE.K_MATCH WITH CURKMATCH2.K_MATCH
							ENDIF
							LOCATE


							* get Ded N amount (401k loan repayment)
							SELECT SQLCURSOR2
							LOCATE FOR (SS_NUM = CURMETLIFE.SS_NUM) AND (DEDCODE = 'N')
							IF FOUND() THEN
								REPLACE CURMETLIFE.N_AMOUNT WITH SQLCURSOR2.DEDAMT
							ENDIF
							LOCATE

							* GET YTD HOURS
							IF INLIST(CURMETLIFE.ADPCOMP,'SXI','E87') THEN
								* HOURLY
								SELECT CURYTDHOURLY
								LOCATE FOR (SS_NUM = CURMETLIFE.SS_NUM)
								IF FOUND() THEN
									REPLACE CURMETLIFE.YTDHOURS WITH CURYTDHOURLY.REG_HOURS + CURYTDHOURLY.OT_HOURS + CURYTDHOURLY.OTHER_HRS
								ENDIF
								LOCATE

							ELSE
								* SALARIED
								SELECT CURYTDSALARY
								LOCATE FOR (SS_NUM = CURMETLIFE.SS_NUM)
								IF FOUND() THEN
									REPLACE CURMETLIFE.YTDHOURS WITH (CURYTDSALARY.NUM_WEEKS * 80)
								ENDIF
								LOCATE
							ENDIF

							* GET YTD PAY
							SELECT CURYTDPAY
							LOCATE FOR (SS_NUM = CURMETLIFE.SS_NUM)
							IF FOUND() THEN
								REPLACE CURMETLIFE.YTDPAY WITH CURYTDPAY.GROSSPAY
							ENDIF
							LOCATE

							* get additional info from employee table:
							SELECT CUREMPLOYEES
							LOCATE FOR (SS_NUM = CURMETLIFE.SS_NUM)
							IF FOUND() THEN
								REPLACE ;
									CURMETLIFE.NAME WITH CUREMPLOYEES.NAME, ;
									CURMETLIFE.LASTNAME  WITH CUREMPLOYEES.LASTNAME, ;
									CURMETLIFE.FIRSTNAME WITH CUREMPLOYEES.FIRSTNAME, ;
									CURMETLIFE.DIVISION WITH LEFT(CUREMPLOYEES.HOMEDEPT,2), ;
									CURMETLIFE.STREET1 WITH CUREMPLOYEES.STREET1, ;
									CURMETLIFE.STREET2 WITH CUREMPLOYEES.STREET2, ;
									CURMETLIFE.CITY WITH CUREMPLOYEES.CITY, ;
									CURMETLIFE.STATE WITH CUREMPLOYEES.STATE, ;
									CURMETLIFE.ZIPCODE WITH CUREMPLOYEES.ZIPCODE, ;
									CURMETLIFE.GENDER WITH CUREMPLOYEES.GENDER, ;
									CURMETLIFE.STATUS WITH CUREMPLOYEES.STATUS, ;
									CURMETLIFE.SALARY WITH CUREMPLOYEES.SALARY, ;
									CURMETLIFE.HIREDATE WITH TTOD(CUREMPLOYEES.HIREDATE), ;
									CURMETLIFE.BIRTHDATE WITH TTOD(CUREMPLOYEES.BIRTHDATE)

								IF NOT ISNULL(CUREMPLOYEES.TERMDATE) THEN
									REPLACE CURMETLIFE.TERMDATE WITH TTOD(CUREMPLOYEES.TERMDATE)
								ENDIF
							ENDIF
							LOCATE
							
						ENDSCAN

						IF .lGetMissingDemoGraphicsFromFoxPro THEN
							* for old, purged ADP employees, get additional info from Foxpro employee table.
							IF USED('CURFOXEMPLOYEE') THEN
								USE IN CURFOXEMPLOYEE
							ENDIF
							
							SELECT ;
								ALLTRIM(TRANSFORM(DIVISION)) AS DIVISION, ;
								IIF(ACTIVE,"A","T") AS STATUS, ;
								EMPLOYEE AS NAME, ;
								STRTRAN(SS_NUM,"-","") AS SS_NUM, ;
								HIRE_DATE AS HIREDATE, ;
								TERM_DATE AS TERMDATE, ;
								ADDRESS AS STREET1, ;
								CITY, ;
								STATE, ;
								ZIP AS ZIPCODE, ;
								DOB AS BIRTHDATE, ;
								SEXTYPE AS GENDER ;
							FROM F:\HR\HRDATA\EMPLOYEE ;
							INTO CURSOR CURFOXEMPLOYEE ;
							ORDER BY 2,4
													
							SELECT CURMETLIFE
							SCAN FOR EMPTY(NAME)
								SELECT CURFOXEMPLOYEE
								GOTO TOP
								LOCATE FOR (SS_NUM = CURMETLIFE.SS_NUM)
								IF FOUND() THEN
									REPLACE ;
										CURMETLIFE.NAME WITH CURFOXEMPLOYEE.NAME, ;
										CURMETLIFE.DIVISION WITH CURFOXEMPLOYEE.DIVISION, ;
										CURMETLIFE.STREET1 WITH CURFOXEMPLOYEE.STREET1, ;
										CURMETLIFE.CITY WITH CURFOXEMPLOYEE.CITY, ;
										CURMETLIFE.STATE WITH CURFOXEMPLOYEE.STATE, ;
										CURMETLIFE.ZIPCODE WITH CURFOXEMPLOYEE.ZIPCODE, ;
										CURMETLIFE.GENDER WITH CURFOXEMPLOYEE.GENDER, ;
										CURMETLIFE.STATUS WITH CURFOXEMPLOYEE.STATUS, ;
										CURMETLIFE.HIREDATE WITH CURFOXEMPLOYEE.HIREDATE, ;
										CURMETLIFE.BIRTHDATE WITH CURFOXEMPLOYEE.BIRTHDATE

									IF NOT ISNULL(CURFOXEMPLOYEE.TERMDATE) THEN
										REPLACE CURMETLIFE.TERMDATE WITH CURFOXEMPLOYEE.TERMDATE
									ENDIF
								ENDIF
								LOCATE							
							ENDSCAN
						ENDIF


						*** SORT HERE !!!!
						IF USED('CURGWPDI') THEN
							USE IN CURGWPDI
						ENDIF
						SELECT * FROM CURMETLIFE INTO CURSOR CURGWPDI ORDER BY NAME

						***********************************************************************************
						***********************************************************************************
						** output to Excel spreadsheet
						.TrackProgress('Creating spreadsheet: ' + lcFiletoSaveAs, LOGIT+SENDIT+NOWAITIT)

						oExcel = CREATEOBJECT("excel.application")
						oExcel.VISIBLE = .F.
						oWorkbook = oExcel.workbooks.OPEN(lcSpreadsheetTemplate)
						oWorkbook.SAVEAS(lcFiletoSaveAs)

						oWorksheet = oWorkbook.Worksheets[1]
						oWorksheet.RANGE("A4","AT2000").ClearContents()

						LOCAL lnStartRow, lnRow, lnEndRow, lnRowsProcessed, lcStartRow, lcEndRow

						lnStartRow = 4
						lcStartRow = ALLTRIM(STR(lnStartRow))

						lnEndRow = lnStartRow - 1
						lnRow = lnStartRow - 1
						
*!*	SELECT CURGWPDI
*!*	COPY TO C:\A\INFO401K


						SELECT CURGWPDI
						SCAN
							lnRow = lnRow + 1
							lnEndRow = lnRow

							lcRow = ALLTRIM(STR(lnRow))

							oWorksheet.RANGE("A"+lcRow).VALUE = "D"
							*oWorksheet.RANGE("B"+lcRow).VALUE = "108343"
							oWorksheet.RANGE("B"+lcRow).VALUE = "455221-01"
							oWorksheet.RANGE("C"+lcRow).VALUE = "'" + CURGWPDI.SS_NUM
							oWorksheet.RANGE("D"+lcRow).VALUE = CURGWPDI.NAME
							oWorksheet.RANGE("E"+lcRow).VALUE = "**A"
							oWorksheet.RANGE("F"+lcRow).VALUE = CURGWPDI.K_AMOUNT
							oWorksheet.RANGE("G"+lcRow).VALUE = "**D"
							oWorksheet.RANGE("H"+lcRow).VALUE = CURGWPDI.K_MATCH

							* change some loan ids per Karen 3/24/06 MB
							*oWorksheet.RANGE("M"+lcRow).VALUE = "001"
							DO CASE
								CASE CURGWPDI.SS_NUM = '130542475'  && Manuel Gomez
									oWorksheet.RANGE("M"+lcRow).VALUE = "002"
								CASE CURGWPDI.SS_NUM = '611070558'  && Erica Nicholas
									oWorksheet.RANGE("M"+lcRow).VALUE = "002"
								OTHERWISE
									* default value
									oWorksheet.RANGE("M"+lcRow).VALUE = "001"
							ENDCASE

							oWorksheet.RANGE("N"+lcRow).VALUE = CURGWPDI.N_AMOUNT
							oWorksheet.RANGE("U"+lcRow).VALUE = CURGWPDI.STREET1
							oWorksheet.RANGE("V"+lcRow).VALUE = CURGWPDI.STREET2
							oWorksheet.RANGE("X"+lcRow).VALUE = CURGWPDI.CITY
							oWorksheet.RANGE("Y"+lcRow).VALUE = CURGWPDI.STATE
							oWorksheet.RANGE("Z"+lcRow).VALUE = "'" + CURGWPDI.ZIPCODE
							oWorksheet.RANGE("AA"+lcRow).VALUE = "USA"
							oWorksheet.RANGE("AB"+lcRow).VALUE = CURGWPDI.GENDER
							oWorksheet.RANGE("AC"+lcRow).VALUE = "'" + CURGWPDI.DIVISION
							IF EMPTY(CURGWPDI.BIRTHDATE) THEN
								oWorksheet.RANGE("AD"+lcRow).VALUE = "?"
							ELSE
								oWorksheet.RANGE("AD"+lcRow).VALUE = CURGWPDI.BIRTHDATE
							ENDIF
							*oWorksheet.RANGE("AE"+lcRow).VALUE = CURGWPDI.HIREDATE
							IF EMPTY(CURGWPDI.HIREDATE) THEN
								oWorksheet.RANGE("AE"+lcRow).VALUE = "?"
							ELSE
								oWorksheet.RANGE("AE"+lcRow).VALUE = CURGWPDI.HIREDATE
							ENDIF
							oWorksheet.RANGE("AH"+lcRow).VALUE = "'7"
							
							oWorksheet.RANGE("AJ"+lcRow).VALUE = CURGWPDI.YTDPAY

						ENDSCAN

						* populate header counts, sums, etc.
						lnRowsProcessed = lnEndRow - lnStartRow + 1
						lcEndRow = ALLTRIM(STR(lnEndRow))

						* rec count
						oWorksheet.RANGE("D2").VALUE = lnRowsProcessed

						* sum $ columns
						oWorksheet.RANGE("F2").VALUE = "=SUM(F4:F" + lcEndRow + ")"
						oWorksheet.RANGE("H2").VALUE = "=SUM(H4:H" + lcEndRow + ")"
						oWorksheet.RANGE("J2").VALUE = "=SUM(J4:J" + lcEndRow + ")"
						oWorksheet.RANGE("L2").VALUE = "=SUM(L4:L" + lcEndRow + ")"
						oWorksheet.RANGE("N2").VALUE = "=SUM(N4:N" + lcEndRow + ")"
						oWorksheet.RANGE("P2").VALUE = "=SUM(P4:P" + lcEndRow + ")"
						oWorksheet.RANGE("R2").VALUE = "=SUM(R4:R" + lcEndRow + ")"
						oWorksheet.RANGE("T2").VALUE = "=SUM(T4:T" + lcEndRow + ")"

						* paycheck date
						IF .lUseDateRange THEN
							oWorksheet.RANGE("V2").VALUE = "YTD thru " + .cRangeEndDate
						ELSE
							oWorksheet.RANGE("V2").VALUE = ldEndingPaydate
						ENDIF

						* SAVE AND QUIT EXCEL
						oWorkbook.SAVE()
						oExcel.QUIT()



						***********************************************************************************
						***********************************************************************************
						***********************************************************************************
						***********************************************************************************
						* Create PDI output csv file

						* open output file
						lcOutputFilename = "F:\UTIL\GreatWest\REPORTS\CSV PDI File for " + STRTRAN(DTOC(ldEndingPaydate),"/","-") + ".CSV"
						.nCurrentOutputCSVFileHandle = FCREATE(lcOutputFilename)
						.TrackProgress('nCurrentOutputCSVFileHandle = ' + TRANSFORM(.nCurrentOutputCSVFileHandle), LOGIT+SENDIT)
						IF .nCurrentOutputCSVFileHandle < 0 THEN
							.TrackProgress('***** Error on FCREATE of: ' + lcOutputFilename, LOGIT+SENDIT)
						ELSE
							.TrackProgress('FCREATEed: ' + lcOutputFilename, LOGIT+SENDIT)
						ENDIF


						* populate the output file
						SELECT CURGWPDI
						SCAN
							lcPlanNumber = "455221-01"
							lcSSN = CURGWPDI.SS_NUM
							lcDivNum = ""
							lcLast = CURGWPDI.LASTNAME
							lcFirst = CURGWPDI.FIRSTNAME
							lcMiddle = ""
							lcSuffix = ""
							lcBirthDate = CURGWPDI.BIRTHDATE
							lcGender = CURGWPDI.GENDER
							lcMarital = "??marital???"
							lcAddress1 = CURGWPDI.STREET1
							lcAddress2 = CURGWPDI.STREET2
							lcCity = CURGWPDI.CITY
							lcState = UPPER(CURGWPDI.STATE)
							lcZip = CURGWPDI.ZIPCODE
							lcHomePhone = ""
							lcWorkPhone = ""
							lcWorkPhoneExt = ""
							lcCountryCode = ""
							ldHireDate = CURGWPDI.HIREDATE
							IF CURGWPDI.STATUS = 'T' AND NOT ISNULL(CURGWPDI.TERMDATE) THEN
								lcTermDate = DTOC(CURGWPDI.TERMDATE)
							ELSE
								lcTermDate = ""
							ENDIF
							lcRehireDate = ""
							lcEEContribution = ALLTRIM(STR(CURGWPDI.K_AMOUNT,11,2))
							lcEmployerMatch = ALLTRIM(STR(CURGWPDI.K_MATCH,11,2))
							lcLoanRepayment = ALLTRIM(STR(CURGWPDI.N_AMOUNT,11,2))
							lcZeroContrib = ALLTRIM(STR(0.00,11,2))
							lcYTDHoursWorked = ALLTRIM(STR(INT(CURGWPDI.YTDHOURS)))
							lcYTDTotCompensation = ALLTRIM(STR(CURGWPDI.YTDPAY,11,2))
							lcYTDPlanComp = lcYTDTotCompensation
							lcPreContrib = ALLTRIM(STR(0.00,11,2))
							
							IF CURGWPDI.SALARY > 80000 THEN
								lcHighComp = "Y"
							ELSE
								lcHighComp = "N"
							ENDIF

							IF INLIST(CURGWPDI.ADPCOMP,'AXA','E89') THEN
								lcOfficer = "Y"
							ELSE
								lcOfficer = "N"
							ENDIF
							lcBlanks = ""
							lcPartDate = "?part date?"
							lcEligibleCode = "?elig code?"

							* Write Detail Line
							lcOutputLine = ;
								.CleanUpField( lcPlanNumber ) + "," + ;
								.CleanUpField( lcSSN ) + "," + ;
								.CleanUpField( lcLast ) + "," + ;
								.CleanUpField( lcFirst ) + "," + ;
								.CleanUpField( lcBirthDate ) + "," + ;
								.CleanUpField( lcGender ) + "," + ;
								.CleanUpField( lcAddress1 ) + "," + ;
								.CleanUpField( lcAddress2 ) + "," + ;
								.CleanUpField( lcCity ) + "," + ;
								.CleanUpField( lcState ) + "," + ;
								.CleanUpField( lcZip ) + "," + ;								
								.CleanUpField( ldHireDate ) + "," + ;
								.CleanUpField( lcTermDate ) + "," + ;
								.CleanUpField( ldEndingPaydate ) + "," + ;
								.CleanUpField( lcEEContribution ) + "," + ;
								.CleanUpField( lcEmployerMatch ) + "," + ;
								.CleanUpField( lcLoanRepayment ) + "," + ;
								.CleanUpField( lcZeroContrib ) + "," + ;
								.CleanUpField( lcZeroContrib ) + "," + ;
								.CleanUpField( lcZeroContrib ) + "," + ;
								.CleanUpField( lcZeroContrib ) + "," + ;
								.CleanUpField( lcZeroContrib ) + "," + ;
								.CleanUpField( lcYTDHoursWorked ) + "," + ;
								.CleanUpField( lcYTDTotCompensation ) + "," + ;
								.CleanUpField( lcYTDPlanComp ) + "," + ;
								.CleanUpField( lcHighComp ) + "," + ;
								.CleanUpField( lcOfficer ) 

							=FPUTS(.nCurrentOutputCSVFileHandle,lcOutputLine)
							
						ENDSCAN


						* close output file
						IF .nCurrentOutputCSVFileHandle > -1 THEN
							llRetval = FCLOSE(.nCurrentOutputCSVFileHandle)
						ENDIF


						***********************************************************************************
						***********************************************************************************
						***********************************************************************************
						***********************************************************************************


						IF FILE(lcFiletoSaveAs) THEN
							* attach xls output file to email
							.cAttach = lcFiletoSaveAs
							.cBodyText = "See attached Great West PDI Files." + ;
								CRLF + CRLF + "(do not reply - this is an automated report)" + ;
								CRLF + CRLF + "<report log follows>" + ;
								CRLF + .cBodyText
						ELSE
							.TrackProgress('ERROR: unable to email spreadsheet: ' + lcFiletoSaveAs, LOGIT+SENDIT+NOWAITIT)
						ENDIF
						
						IF FILE(lcOutputFilename) THEN
							* attach csv output file to email
							.cAttach = .cAttach + "," + lcOutputFilename
						ELSE
							.TrackProgress('ERROR: unable to email spreadsheet: ' + lcOutputFilename, LOGIT+SENDIT+NOWAITIT)
						ENDIF

						CLOSE DATABASES ALL
					ENDIF  &&  .ExecSQL(lcSQL, 'SQLCURSOR1', RETURN_DATA_MANDATORY)

					.TrackProgress('GREAT WEST PDI process ended normally.', LOGIT+SENDIT+NOWAITIT)
				ELSE
					* connection error
					.TrackProgress('Unable to connect to ADP Payroll System.', LOGIT+SENDIT)
				ENDIF   &&  .nSQLHandle > 0

			CATCH TO loError

				.TrackProgress('There was an error.',LOGIT+SENDIT)
				.TrackProgress('The Error # is: ' + TRANSFORM(loError.ERRORNO), LOGIT+SENDIT)
				.TrackProgress('The Error Message is: ' + TRANSFORM(loError.MESSAGE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred in Program: ' + TRANSFORM(loError.PROCEDURE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred at line #: ' + TRANSFORM(loError.LINENO), LOGIT+SENDIT)
				lnNumberOfErrors = lnNumberOfErrors + 1
				IF TYPE('oWorkbook') = "O" THEN
					oWorkbook.SAVE()
				ENDIF
				IF TYPE('oExcel') = "O" THEN
					oExcel.QUIT()
				ENDIF
				CLOSE DATABASES ALL

			ENDTRY

			CLOSE DATA
			WAIT CLEAR
			***************** INTERNAL email results ******************************
			.TrackProgress('About to send status email.',LOGIT)
			.TrackProgress('The logfile is: ' + .cLogFile, SENDIT)
			.TrackProgress('GREAT WEST PDI process started: ' + .cStartTime, LOGIT+SENDIT)
			.TrackProgress('GREAT WEST PDI process finished: ' + TTOC(DATETIME()), LOGIT+SENDIT)

			IF .lSendInternalEmailIsOn THEN
				DO FORM dartmail2 WITH .cSendTo,.cFrom,.cSubject,.cCC,.cAttach,.cBodyText,"A"
				.TrackProgress('Sent status email.',LOGIT)
			ELSE
				.TrackProgress('Did not send status email: THIS.lSendInternalEmailIsOn = FALSE.',LOGIT)
			ENDIF

		ENDWITH
		RETURN
	ENDFUNC && main


	FUNCTION CleanUpField
		* prepare field for inclusion in the string to be written with FPUTS().
		* especially critical to remove any commas, since that creates bogus column in CSV output file.
		LPARAMETERS tuValue
		LOCAL lcRetVal
		lcRetVal = STRTRAN(ALLTRIM(TRANSFORM(tuValue)),",","")
		RETURN lcRetVal
	ENDFUNC


	FUNCTION ExecSQL
		LPARAMETERS tcSQL, tcCursorName, tlNoDataReturnedIsOkay
		LOCAL llRetval, lnResult
		WITH THIS
			* close target cursor if it's open
			IF USED(tcCursorName)
				USE IN (tcCursorName)
			ENDIF
			WAIT WINDOW "Fetching remote data; please wait..." NOWAIT
			lnResult = SQLEXEC(.nSQLHandle, tcSQL, tcCursorName)
			llRetval = ( lnResult > 0 )
			IF llRetval THEN
				* see if any data came back
				IF NOT tlNoDataReturnedIsOkay THEN
					IF (NOT USED(tcCursorName)) OR EOF(tcCursorName) THEN
						llRetval = .F.
						.TrackProgress('No data was returned by this query: ' +CRLF + tcSQL, LOGIT+SENDIT)
						.cSendTo = 'Mark Bennett <mbennett@fmiint.com>'
					ENDIF
				ENDIF
			ELSE
				.TrackProgress("Result Code [" + TRANSFORM(lnResult) + "] returned by this query: " + tcSQL, LOGIT+SENDIT)
				.cSendTo = 'Mark Bennett <mbennett@fmiint.com>'
			ENDIF
			WAIT CLEAR
			RETURN llRetval
		ENDWITH
	ENDFUNC


	PROCEDURE TrackProgress
		* do any combination of Wait Window, writing to logfile, and adding to body of email,
		* based on nFlags parameter.
		LPARAMETERS tcExpression, tnFlags
		WITH THIS
			IF BITAND(tnFlags,LOGIT) = LOGIT THEN
				IF .lLoggingIsOn THEN
					?
					? .cProcessName + "  " + TTOC(DATETIME()) + ": " + tcExpression
				ENDIF
			ENDIF
			IF .lWaitWindowIsOn THEN
				IF BITAND(tnFlags,NOWAITIT) = NOWAITIT THEN
					WAIT WINDOW tcExpression NOWAIT
				ENDIF
				IF BITAND(tnFlags,WAITIT) = WAITIT THEN
					WAIT WINDOW tcExpression TIMEOUT .nWaitWindowTimeout
				ENDIF
			ENDIF
			IF BITAND(tnFlags,SENDIT) = SENDIT THEN
				IF .lSendInternalEmailIsOn THEN
					.cBodyText = .cBodyText + tcExpression + CRLF + CRLF
				ENDIF
			ENDIF
		ENDWITH
	ENDPROC  &&  TrackProgress

ENDDEFINE

