*!* m:\dev\prg\yellsteel940edi_bkdn.prg
CLEAR

lCheckStyle = .T.
lPrepack = .F.
lPick = .T.
lSolidPack = .F.
lJCP = .F.
units = .T.
ptctr = 0
m.isa_num = ""
m.color = ""
m.id = ""
cDCPI = ""
cType940 = ""

SELECT x856
COUNT FOR TRIM(segment) = "ST" TO nSegCnt
cSegCnt = ALLTRIM(STR(nSegCnt))

WAIT WINDOW "Now in "+cCustName+" 940 Breakdown" TIMEOUT 1
WAIT "There are "+cSegCnt+" P/Ts in this file" WINDOW TIMEOUT 1
SELECT x856
LOCATE

SELECT xpt
SCATTER MEMVAR blank
SELECT xptdet
SCATTER MEMVAR blank

*!* Constants
m.acct_name = "YELL STEEL"
m.acct_num = nAcctNum
m.careof = " "
m.sf_addr1 = "1382 VALENCIA AVE - SUITE I"
m.sf_csz = "TUSTIN, CA 92780"
m.printed = .F.
m.print_by = ""
m.printdate = DATE()
m.addby = "FMI-PROC"
m.adddt = DATETIME()
m.addproc = "YSTL940EDI"
*!* End constants

*!*	IF !USED("uploaddet")
*!*		IF lTesting
*!*			USE F:\ediwhse\test\uploaddet IN 0 ALIAS uploaddet
*!*		ELSE
*!*			USE F:\ediwhse\DATA\uploaddet IN 0 SHARED ALIAS uploaddet
*!*		ENDIF
*!*	ENDIF

SELECT xpt
DELETE ALL
SELECT xptdet
DELETE ALL

STORE 0 TO pt_total_eaches
STORE 0 TO pt_total_cartons
STORE 0 TO nCtnCount

SELECT x856
SET FILTER TO
LOCATE

WAIT WINDOW "NOW IN INITAL HEADER BREAKDOWN" TIMEOUT 1
WAIT WINDOW "NOW IN INITAL HEADER BREAKDOWN" NOWAIT
DO WHILE !EOF("x856")
	cSegment = TRIM(x856.segment)
	cCode = TRIM(x856.f1)

	IF INLIST(TRIM(x856.segment),"GS")
		SELECT x856
		SKIP
		LOOP
	ENDIF

	IF TRIM(x856.segment) = "ISA"
		cISA_Num = ""
		cISA_Num = ALLTRIM(x856.f13)
		SELECT x856
		SKIP
		LOOP
	ENDIF

	IF TRIM(x856.segment) = "ST" AND TRIM(x856.f1) = "940"
		SELECT x856
		SKIP
		LOOP
	ENDIF

	IF TRIM(x856.segment) = "W05"  && new shipment
		nCtnCount = 0
		SELECT xpt
		nDetCnt = 0
		APPEND BLANK
		REPLACE xpt.qty WITH 0 IN xpt
		REPLACE xpt.origqty WITH 0 IN xpt
		STORE 0 TO pt_total_eaches
		STORE 0 TO pt_total_cartons
		ptctr = ptctr +1
		WAIT WINDOW AT 10,10 "Now processing PT# "+ ALLTRIM(x856.f2)+" Number "+CHR(13)+TRANSFORM(ptctr)+" of "+cSegCnt NOWAIT

		REPLACE xpt.ptid       WITH ptctr
		REPLACE xpt.accountid  WITH m.acct_num
		REPLACE xpt.ptdate     WITH DATE()
		m.ship_ref = ALLTRIM(x856.f2)
		m.cnee_ref = ALLTRIM(x856.f3)
		REPLACE xpt.ship_ref   WITH m.ship_ref  && Pick Ticket
		REPLACE xpt.cnee_ref   WITH m.cnee_ref
		m.pt = ALLTRIM(x856.f2)
		IF lTesting
			REPLACE xpt.cacctnum   WITH "999"
		ENDIF
		SELECT x856
		SKIP
		LOOP
	ENDIF

	IF TRIM(x856.segment) = "N1" AND TRIM(x856.f1) = "ST" && Ship-to data
*!*			SELECT uploaddet
*!*			LOCATE
*!*			IF !EMPTY(M.isa_num)
*!*				APPEND BLANK
*!*				REPLACE uploaddet.office    WITH cOffice
*!*				REPLACE uploaddet.cust_name WITH UPPER(TRIM(x856.f2))
*!*				REPLACE uploaddet.acct_num  WITH m.acct_num
*!*				REPLACE uploaddet.DATE      WITH DATETIME()
*!*				REPLACE uploaddet.isa_num   WITH m.isa_num
*!*				REPLACE uploaddet.startpt   WITH xpt.ship_ref
*!*				REPLACE uploaddet.runid     WITH lnRunID
*!*			ENDIF

		SELECT xpt
		m.st_name = TRIM(x856.f2)
		REPLACE xpt.NAME WITH UPPER(m.st_name) IN xpt
		REPLACE xpt.consignee WITH UPPER(m.st_name) IN xpt

		lFederated = IIF("MACY"$UPPER(m.st_name),.T.,.F.)
		lJCP = IIF("PENNEY"$UPPER(m.st_name),.T.,.F.)

		cStoreNum = ALLTRIM(x856.f4)
		REPLACE xpt.shipins WITH IIF(EMPTY(xpt.shipins),cType940,xpt.shipins+CHR(13)+cType940) IN xpt
		ASSERT .F. MESSAGE "At Storenum insert into SHIPINS"
		REPLACE xpt.shipins WITH IIF(EMPTY(xpt.shipins),"STORENUM*"+cStoreNum,xpt.shipins+CHR(13)+"STORENUM*"+cStoreNum) IN xpt
		REPLACE xpt.storenum  WITH VAL(RIGHT(cStoreNum,5)) IN xpt
		REPLACE xpt.dcnum WITH cStoreNum IN xpt

		SKIP 1 IN x856
		IF TRIM(x856.segment) = "N3"  && address info
			m.st_addr1 = UPPER(TRIM(x856.f1))
			m.st_addr2 = UPPER(TRIM(x856.f2))
			REPLACE xpt.address  WITH m.st_addr1 IN xpt
			REPLACE xpt.address2 WITH m.st_addr2 IN xpt
		ENDIF
		SKIP 1 IN x856
		IF TRIM(x856.segment) = "N4"  && address info
			IF !EMPTY(TRIM(x856.f3))
				m.zipbar = TRIM(x856.f3)
			ENDIF
			IF !EMPTY(TRIM(x856.f2))
				m.st_csz = UPPER(TRIM(x856.f1))+", "+UPPER(TRIM(x856.f2))+" "+TRIM(x856.f3)
			ELSE
				m.st_csz = UPPER(TRIM(x856.f1))
			ENDIF
			REPLACE xpt.csz WITH m.st_csz IN xpt
		ENDIF
		SELECT x856
		SKIP
		LOOP
	ENDIF

	IF TRIM(x856.segment) = "N9" AND TRIM(x856.f1) = "DP"  && customer dept
		m.dept = ALLTRIM(x856.f2)
		REPLACE xpt.dept WITH m.dept IN xpt
		SELECT x856
		SKIP
		LOOP
	ENDIF

	IF TRIM(x856.segment) = "N9" AND TRIM(x856.f1) = "MR" && Dept Num.
		REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"MR*"+ALLTRIM(x856.f2) IN xpt
	ENDIF

	IF TRIM(x856.segment) = "G62" AND TRIM(x856.f1) == "37"  && start date
		lcDate = ALLTRIM(x856.f2)
		lxDate = SUBSTR(lcDate,5,2)+"/"+SUBSTR(lcDate,7,2)+"/"+LEFT(lcDate,4)
		ldDate = CTOD(lxDate)
		REPLACE xpt.START WITH ldDate IN xpt
		SELECT x856
		SKIP
		LOOP
	ENDIF

	IF TRIM(x856.segment) = "G62" AND TRIM(x856.f1) == "38"  && cancel date
		lcDate = ALLTRIM(x856.f2)
		lxDate = SUBSTR(lcDate,5,2)+"/"+SUBSTR(lcDate,7,2)+"/"+LEFT(lcDate,4)
		ldDate = CTOD(lxDate)
		REPLACE xpt.CANCEL WITH ldDate IN xpt
		SELECT x856
		SKIP
		LOOP
	ENDIF

	m.units = .F.
	m.ptdetid = 0
*!* At Detail Process
	IF TRIM(x856.segment) = "LX" && start of PT detail, stay here until this PT is complete
		*SET STEP ON
		WAIT "NOW IN DETAIL LOOP PROCESSING" WINDOW NOWAIT
		SELECT xpt
		GATHER MEMVAR FIELDS addby,adddt,addproc

		SELECT xptdet
		m.linenum = TRIM(x856.f1)

*!* Housekeeping

		SKIP 1 IN x856
		lPrepack = .F.
		DO WHILE ALLTRIM(x856.segment) != "SE"
			cSegment = TRIM(x856.segment)
			cCode = TRIM(x856.f1)

			DO CASE
				CASE TRIM(x856.segment) = "LX"
					m.linenum = TRIM(x856.f1)

				CASE TRIM(x856.segment) = "W01" && Destination Quantity
					cUnitCode = ALLT(x856.f2)
					lPrepack = IIF(INLIST(cUnitCode,"CS"),.T.,.F.)  && Replenishment check
					IF lPrepack
					cUnitCode = IIF(cUnitCode#"CS","RP","CS")
					endif
					m.custsku  = ""
					m.totqty   = INT(VAL(x856.f1))
					m.origqty  = m.totqty
					m.Style    = ALLT(x856.f5)
					m.upc = ALLT(x856.f7)
					m.pack     = "12"
					m.casepack = 12
					m.printstuff = ""
					IF !lPrepack AND !lTesting
						m.totqty   = INT(VAL(x856.f1))/12
						m.origqty  = INT(VAL(x856.f1))/12
					ENDIF
					REPLACE xpt.qty WITH xpt.qty + m.totqty IN xpt
					REPLACE xpt.origqty WITH xpt.qty IN xpt

				CASE TRIM(x856.segment) = "N9" AND TRIM(x856.f1) = "GTIN"  && GTIN
					m.printstuff = IIF(EMPTY(m.printstuff),m.printstuff+CHR(13)+"GTIN*"+ALLTRIM(x856.f2),"GTIN*"+ALLTRIM(x856.f2))

				CASE TRIM(x856.segment) = "N9" AND TRIM(x856.f1) = "VCL"  && color
*!*					m.color = ALLTRIM(x856.f2)

				CASE TRIM(x856.segment) = "N9" AND TRIM(x856.f1) = "VSZ"  && size
					cNumberSize = ALLT(x856.f2)
					m.printstuff = m.printstuff+CHR(13)+"NUMSIZE*"+cNumberSize

				CASE TRIM(x856.segment) = "N9" AND TRIM(x856.f1) = "SZ"  && size
*!*					m.id = ALLTRIM(x856.f2)

				CASE TRIM(x856.segment) = "N9" AND TRIM(x856.f1) = "PK"
					REPLACE xptdet.PACK WITH ALLTRIM(x856.f2) IN xptdet NEXT 1
					REPLACE xptdet.casepack WITH INT(VAL(xptdet.PACK)) IN xptdet NEXT 1

				CASE TRIM(x856.segment) = "G69"
					SELECT xpt
					SCATTER FIELDS ptid,accountid MEMVAR
					SELECT xptdet

					APPEND BLANK
					IF lTesting
						REPLACE xptdet.ship_ref WITH xpt.ship_ref IN xptdet
					ENDIF
					m.ptdetid = m.ptdetid + 1
					GATHER MEMVAR MEMO
					REPLACE xptdet.origqty WITH xptdet.totqty IN xptdet
					REPLACE xptdet.printstuff WITH xptdet.printstuff+CHR(13)+;
						"UNITCODE*"+cUnitCode+CHR(13)+"PACKTYPE*"+IIF(cUnitCode="CS","CASEPACK","REPLENISHMENT") IN xptdet
					SCATTER FIELDS ptid,ptdetid,accountid MEMVAR

				CASE TRIM(x856.segment) = "W20"
					REPLACE xptdet.PACK WITH ALLTRIM(x856.f1) IN xptdet NEXT 1
					REPLACE xptdet.casepack WITH INT(VAL(xptdet.PACK)) IN xptdet NEXT 1
					

			ENDCASE

			SELECT x856
			SKIP 1 IN x856
		ENDDO

		SELECT xptdet
	ENDIF

	IF TRIM(x856.segment) = "LX"
		m.linenum = TRIM(x856.f1)
	ENDIF

	SELECT x856
	IF !EOF()
		SKIP 1 IN x856
	ENDIF
ENDDO

DO m:\dev\prg\setuppercase940

IF lBrowfiles
	SELECT xpt
	LOCATE
	BROWSE
	SELECT xptdet
	LOCATE
	BROWSE
	IF MESSAGEBOX("Do you wish to continue?",4+16+256,"CONTINUE")=7
		CANCEL
		NormalExit = .T.
		THROW
	ENDIF
ENDIF


ASSERT .F. MESSAGE "At end of BKDN round (not SET STEP ON)...debug if needed"

WAIT cCustName+" Breakdown Round complete..." WINDOW TIMEOUT 2
SELECT x856
IF USED("PT")
	USE IN pt
ENDIF
IF USED("PTDET")
	USE IN ptdet
ENDIF
WAIT CLEAR

