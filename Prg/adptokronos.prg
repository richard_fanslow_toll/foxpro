* Initial export of employees from ADP into Kronos.

LOCAL loADPEmployeesToKronos
*!*	IF NOT INLIST(UPPER(ALLTRIM(GETENV("COMPUTERNAME"))),"MBENNETT","HR1") THEN
*!*		=MESSAGEBOX("The Employee Export must be run from Lucille's PC!",0+16,"Export FoxPro Employees")
*!*		RETURN
*!*	ENDIF
loADPEmployeesToKronos = CREATEOBJECT('ADPEmployeesToKronos')
loADPEmployeesToKronos.MAIN()
CLOSE DATABASES ALL
RETURN

#DEFINE LOGIT 1
#DEFINE WAITIT 2
#DEFINE NOWAITIT 4
#DEFINE SENDIT 8
#DEFINE CRLF CHR(13) + CHR(10)
#DEFINE RETURN_DATA_MANDATORY .F.
#DEFINE RETURN_DATA_NOT_MANDATORY .T.
#DEFINE CSVFORMAT 6
#DEFINE WRITE_ONLY_UNBUFFERED 11
#DEFINE COMMA ","
#DEFINE MIRA_LOMA_ACQUISITION_HIRE_DATE {^2004-07-21}
#DEFINE MIRA_LOMA_ACQUISITION_HIRE_DATE_TWO {^2004-07-19}
*!*	#DEFINE NOSHIFTCODE "NOSHFT"
#DEFINE LORIS_BADGE_NUM 1026
#DEFINE LUCILLES_BADGE_NUM 1018
#DEFINE LUCILLES_NAME "Waldrip, Lucille"

DEFINE CLASS ADPEmployeesToKronos AS CUSTOM

	cProcessName = 'ADPEmployeesToKronos'

	lTestMode = .F.  && .F. FOR NORMAL OPERATION, .T. TO SUPPRESS ALL BUT DEVELOPER EMAILS AND CHANGE INPUT TABLE, ETC.

	lAutoYield = .T.

	cStartTime = TTOC(DATETIME())

	* Process properties
	lDidInitialPut = .T.  && make this .F. for one-time export to Kronos; .T. thereafter!
	*cInitialExportDateODBCFormat = "{d '2007-01-07'}"   && after one-time export to Kronos set this = to the export date
	cInitialExportDateODBCFormat = "{d '2012-01-07'}"   && after one-time export to Kronos set this = to the export date

	lOnlyProcesshangesTo9999 = .F.

	* Data Table properties
	cFoxProEmployeeTable = "F:\HR\HRDATA\EMPLOYEE"

	* email properties
	cExportedList = ''

	* connection properties
	nSQLHandle = 0

	* file properties
	nFileHandle = 0

	* date / time properties
	dtNow = DATETIME()
	dToday = DATE()
	cToday = DTOC(DATE())
	dTomorrow = DATE() + 1
	cTime = TIME()

	* Effective Date logic properties
	lForcePriorPastSundayEffectiveDates = .F.  && set to .T. to force regardless of load datetime

	* wait window properties
	nWaitWindowTimeout = 2
	lWaitWindowIsOn = .T.

	* logfile properties
	lLoggingIsOn = .T.
	cLogFile = 'F:\UTIL\KRONOS\Logfiles\ADPEmployeesToKronos_log.txt'

	* INTERNAL email properties
	lSendInternalEmailIsOn = .T.
	cFrom = 'TGF Corporate <fmicorporate@fmiint.com>'
	cSendTo = 'Mark Bennett <mbennett@fmiint.com>'
	*cSendTo = 'Lucille Waldrip <lwaldrip@fmiint.com>, Lori Guiliano <lguiliano@fmiint.com>, Mark Bennett <mbennett@fmiint.com>'
	cCC = ''
	cSubject = 'ADP EMPLOYEES --> KRONOS, Results for: ' + TTOC(DATETIME())
	cAttach = ''
	cBodyText = ''

	cCOMPUTERNAME = ''
	cUSERNAME = ''

	FUNCTION INIT
		IF NOT DODEFAULT()
			RETURN .F.
		ENDIF
		WITH THIS
			CLEAR
			*SET RESOURCE OFF
			CLOSE DATA
			SET CENTURY ON
			SET DATE AMERICAN
			SET HOURS TO 24
			SET ANSI ON
			SET TALK OFF
			SET DELETED ON
			SET CONSOLE OFF
			SET EXCLUSIVE OFF
			SET SAFETY OFF
			SET EXACT OFF
			SET STATUS BAR ON
			SET SYSMENU OFF
			SET ENGINEBEHAVIOR 70
			_VFP.AUTOYIELD = .lAutoYield
			.cCOMPUTERNAME = GETENV("COMPUTERNAME")
			.cUSERNAME = GETENV("USERNAME")
			IF .lTestMode THEN
				.cSendTo = 'Mark Bennett <mbennett@fmiint.com>'
				.cLogFile = 'F:\UTIL\KRONOS\Logfiles\ADPEmployeesToKronos_log_TESTMODE.txt'
			ENDIF
			.lLoggingIsOn = .lLoggingIsOn AND NOT EMPTY(.cLogFile)
			IF .lLoggingIsOn THEN
				SET ALTERNATE TO (.cLogFile) ADDITIVE
				SET ALTERNATE ON
			ENDIF
			* re-init date properties after all SETTINGS
			.dtNow = DATETIME()
			.dToday = DATE()
			.cToday = DTOC(DATE())
			SET PROCEDURE TO M:\MARKB\PRG\VALIDATIONS ADDITIVE
		ENDWITH
	ENDFUNC


	FUNCTION DESTROY
		WITH THIS
			IF .lLoggingIsOn  THEN
				SET ALTERNATE OFF
				SET ALTERNATE TO
			ENDIF
		ENDWITH
		DODEFAULT()
	ENDFUNC


	FUNCTION MAIN
		WITH THIS
			LOCAL lnNumberOfErrors, lcSQL, lcSQL2, lcSQL3, loError, lcTopBodyText, ldBlankDate, ltBlankDateTime, llValid
			LOCAL ldHireDate, ldTempHireDate, ldNewDate, lcImport, lcPhone, lcNewStatus
			LOCAL lcPayRule, lcTimeZone, lcAccrualProfile, ldEmpEffDate, lcSalaryType, lcLicense
			LOCAL lcCSVFilename, lcEmployeeList, lnEmployeesSent, lcWhere, llMandatoryDataSwitch, lcAction
			LOCAL lcUsername, lcPWRD, lcDailyCSVFilename, lcNow, lcToday, lcDailyCSVFileRoot, llChanged, ldTomorrow
			LOCAL lnTRNumber, lcTRName, lcOldWorkSite, ldLatestPastSunday, ldPriorPastSunday, lcNewWorkSite
			LOCAL llRunningOnMonday, llRunningOnTuesday, llBefore8AM, lcNew9999

			TRY
				lnNumberOfErrors = 0
				lcTopBodyText = ''
				ldBlankDate = CTOD('')
				ltBlankDateTime = DTOT(ldBlankDate)
				*ltBlankBirthdate = DTOT({^1916-01-01})
				ldTomorrow = .dTomorrow
				ldToday = .dToday

				llRunningOnMonday = ( DOW( ldToday, 1) = 2 )

				llRunningOnTuesday = ( DOW( ldToday, 1) = 3 )

				llBefore8AM = ( VAL(LEFT(.cTime,2)) < 8 )

				lcNew9999 = ""

				.TrackProgress('cTime = ' + .cTime, LOGIT+SENDIT)
				.TrackProgress('Running On Monday = ' + TRANSFORM(llRunningOnMonday), LOGIT+SENDIT)
				.TrackProgress('Running On Tuesday = ' + TRANSFORM(llRunningOnTuesday), LOGIT+SENDIT)
				.TrackProgress('Before 8AM = ' + TRANSFORM(llBefore8AM), LOGIT+SENDIT)
				.TrackProgress('lOnlyProcesshangesTo9999 = ' + TRANSFORM(.lOnlyProcesshangesTo9999), LOGIT+SENDIT)

				* calc prior Sunday, which we will now use for effective dates for Not New RECORDS (i.e., updates)
				* instead of today's date. This is to try and prevent switching labor levels in the middle of a pay period,
				* which causes extra Totals by labor level on the time cards, and makes the TR reports harder to read.
				ldLatestPastSunday = ldToday
				DO WHILE DOW( ldLatestPastSunday, 1) <> 1
					ldLatestPastSunday = ldLatestPastSunday - 1
				ENDDO
				ldPriorPastSunday = ldLatestPastSunday - 7
				.TrackProgress('Prior Past Sunday = ' + TRANSFORM(ldPriorPastSunday), LOGIT+SENDIT)
				.TrackProgress('Latest Past Sunday = ' + TRANSFORM(ldLatestPastSunday), LOGIT+SENDIT)

				*.TrackProgress('', LOGIT)
				*.TrackProgress('', LOGIT)
				*.TrackProgress('==================================================================================================================', LOGIT+SENDIT)
				.TrackProgress('ADP EMPLOYEES --> KRONOS process started....', LOGIT+SENDIT+NOWAITIT)
				.TrackProgress('COMPUTERNAME = ' + .cCOMPUTERNAME, LOGIT+SENDIT)
				.TrackProgress('USERNAME = ' + .cUSERNAME, LOGIT+SENDIT)
				.TrackProgress('==================================================================================================================', LOGIT+SENDIT)
				IF .lTestMode THEN
					.TrackProgress('', LOGIT)
					.TrackProgress('==================================================================================================================', LOGIT+SENDIT)
					.TrackProgress('                        TEST MODE                        ', LOGIT+SENDIT)
					.TrackProgress('==================================================================================================================', LOGIT+SENDIT)
				ENDIF
				.TrackProgress('', LOGIT)

				* delete main output file early on so we now it's free when we FOPEN() it later...
				lcCSVFilename = "F:\UTIL\KRONOS\EDI\PEOPLEIMPORT.CSV"
				IF FILE(lcCSVFilename) THEN
					DELETE FILE (lcCSVFilename)
				ENDIF

				* open export history table
				USE F:\UTIL\KRONOS\DATA\EXPHIST IN 0 ALIAS EXPHIST

				***************************************************
				***************************************************
				** Extract current WTK employee data that we don't want changed when importing.
				** For example:
				*		DEVICE GROUP
				* 		CPAYRULE
				* 		CACRPROFIL
				* 		schedgrp
				* 		license
				* 		username
				* 		PASSWORD

				* Notes:
				* 	- schedule groups are date based, so we must do special processing to get the most recent one.
				*	- each employee can have multiple licenses, so we must do special processin to get the highest one.

				IF USED('CURWTKEMPLOYEE') THEN
					USE IN CURWTKEMPLOYEE
				ENDIF
				IF USED('CURWTKEMPLOYEE2') THEN
					USE IN CURWTKEMPLOYEE2
				ENDIF
				IF USED('CURLICENSES') THEN
					USE IN CURLICENSES
				ENDIF
				IF USED('CURLICENSES2') THEN
					USE IN CURLICENSES2
				ENDIF
				IF USED('CURSCHEDULES') THEN
					USE IN CURSCHEDULES
				ENDIF
				IF USED('CURSCHEDULES2') THEN
					USE IN CURSCHEDULES2
				ENDIF
				IF USED('RVWRLIST') THEN
					USE IN RVWRLIST
				ENDIF

				USE F:\UTIL\KRONOS\DATA\TIMERVWR.DBF AGAIN IN 0 ALIAS RVWRLIST
				*
				* main SQL for easy-to-get info
				lcSQL = ;
					" SELECT " + ;
					" B.FULLNM AS NAME, " + ;
					" B.PERSONNUM AS BADGE_NUM, " + ;
					" B.PERSONID AS PERSONID, " + ;
					" X.HOMELABORACCTNAME AS LABRACCT, " + ;
					" B.COMPANYHIREDTM AS HIREDATE, " + ;
					" A.EMPLOYEEID AS EMPLOYEEID, " + ;
					" A.DEVICEGROUPNM AS DEVICEGRP, " + ;
					" C.NAME AS CPAYRULE, " + ;
					" D.NAME AS CACRPROFIL, " + ;
					" '                              ' AS SCHEDGRP, " + ;
					" '                              ' AS LICENSE " + ;
					" FROM WTKEMPLOYEE A " + ;
					" JOIN PERSON B " + ;
					" ON B.PERSONID = A.PERSONID " + ;
					" JOIN VP_EMPLOYEEV42 X " + ;
					" ON X.PERSONID = A.PERSONID " + ;
					" JOIN PAYRULEIDS C " + ;
					" ON C.PAYRULEID = A.PAYRULEID " + ;
					" LEFT OUTER JOIN ACCRUALPROFILE D " + ;
					" ON D.ACCRUALPROFILEID = B.ACCRUALPROFILEID " + ;
					" WHERE A.WTKEMPLOYEEID > 0 " + ;
					" ORDER BY 1 "

				*!*						* Z.NAME
				*!*						" JOIN ACCRUALPROFILE Z " + ;
				*!*						" ON Z.ACCRUALPROFILEID = B.ACCRUALPROFILEID " + ;

				* SQL for License Lookup table
				lcSQL2 = ;
					" SELECT " + ;
					" A.PERSONID, " + ;
					" A.LICENSETYPEID AS LICENSEID, " + ;
					" B.SHORTNM AS LICENSE " + ;
					" FROM PRSNLICTYPEMM A " + ;
					" JOIN LICENSETYPE B " + ;
					" ON B.LICENSETYPEID = A.LICENSETYPEID " + ;
					" WHERE A.PERSONID > 0 " + ;
					" ORDER BY 1, 2 DESC "


				* SQL for Schedule Group Lookup Table
				lcSQL3 = ;
					" SELECT " + ;
					" A.EMPLOYEEID, " + ;
					" A.EFFECTIVEDTM AS EFFECTDATE, " + ;
					" B.SHORTNM AS SCHEDGRP " + ;
					" FROM EMPGRPSCHEDMM A " + ;
					" JOIN GROUPSCHEDULE B " + ;
					" ON B.GRPSCHEDULEID = A.GRPSCHEDULEID " + ;
					" ORDER BY 1, 2 DESC "

				.TrackProgress('Connecting to WTK System....', LOGIT+SENDIT)

				*.nSQLHandle = SQLCONNECT("KRONOSEDI","tkcsowner","tkcsowner")
				.nSQLHandle = SQLCONNECT("WTK2","sa","kr0n1tes")

				IF .nSQLHandle > 0 THEN

					IF .ExecSQL(lcSQL, 'CURWTKEMPLOYEE', RETURN_DATA_MANDATORY) ;
							AND .ExecSQL(lcSQL2, 'CURLICENSES', RETURN_DATA_MANDATORY) ;
							AND .ExecSQL(lcSQL3, 'CURSCHEDULES', RETURN_DATA_MANDATORY) ;
							THEN

						*!*							SELECT CURLICENSES
						*!*							BROWSE
						*!*							SELECT CURSCHEDULES
						*!*							BROWSE

						.TrackProgress('Downloading Kronos settings to Export History table....', LOGIT+SENDIT)

						* populate main cursor
						SELECT CURWTKEMPLOYEE
						SCAN

							* LICENSE
							SELECT CURLICENSES
							GOTO TOP
							LOCATE FOR PERSONID = CURWTKEMPLOYEE.PERSONID
							IF FOUND() THEN
								REPLACE CURWTKEMPLOYEE.LICENSE WITH .TranslateLicense(CURLICENSES.LICENSE)
							ENDIF
							LOCATE

							* SCHED GROUP
							SELECT CURSCHEDULES
							GOTO TOP
							LOCATE FOR EMPLOYEEID = CURWTKEMPLOYEE.EMPLOYEEID
							IF FOUND() THEN
								REPLACE CURWTKEMPLOYEE.SCHEDGRP WITH ALLTRIM(CURSCHEDULES.SCHEDGRP)
							ENDIF
							LOCATE


						ENDSCAN


						*!*							SELECT CURWTKEMPLOYEE
						*!*							GOTO TOP
						*!*							BROWSE


						*!*							RETURN

						* convert valid Badge_nums to ints (some are char in SQL)
						SELECT ;
							INT(VAL(BADGE_NUM)) AS BADGE_NUM, ;
							LABRACCT, ;
							PERSONID, ;
							TTOD(HIREDATE) AS HIREDATE, ;
							EMPLOYEEID, ;
							DEVICEGRP, ;
							CPAYRULE, ;
							NVL(CACRPROFIL,SPACE(50)) AS CACRPROFIL, ;
							SCHEDGRP, ;
							LICENSE ;
							FROM CURWTKEMPLOYEE ;
							INTO CURSOR CURWTKEMPLOYEE2 ;
							WHERE INT(VAL(BADGE_NUM)) > 0

						IF USED('CURWTKEMPLOYEE') THEN
							USE IN CURWTKEMPLOYEE
						ENDIF
						
						* update export history table
						SELECT EXPHIST
						SCAN
							SELECT CURWTKEMPLOYEE2
							LOCATE FOR BADGE_NUM = EXPHIST.BADGE_NUM
							IF FOUND() THEN
								IF (NOT ISNULL(CURWTKEMPLOYEE2.DEVICEGRP)) THEN
									REPLACE EXPHIST.DEVICEGRP WITH CURWTKEMPLOYEE2.DEVICEGRP IN EXPHIST
								ENDIF
								IF (NOT ISNULL(CURWTKEMPLOYEE2.SCHEDGRP)) THEN
									REPLACE EXPHIST.SCHEDGRP WITH CURWTKEMPLOYEE2.SCHEDGRP IN EXPHIST
								ENDIF
								IF (NOT ISNULL(CURWTKEMPLOYEE2.CPAYRULE)) THEN
									REPLACE EXPHIST.CPAYRULE WITH CURWTKEMPLOYEE2.CPAYRULE IN EXPHIST
								ENDIF
								IF (NOT ISNULL(CURWTKEMPLOYEE2.CACRPROFIL)) THEN
									REPLACE EXPHIST.CACRPROFIL WITH CURWTKEMPLOYEE2.CACRPROFIL IN EXPHIST
								ENDIF
								IF (NOT ISNULL(CURWTKEMPLOYEE2.LICENSE)) THEN
									REPLACE EXPHIST.LICENSE WITH CURWTKEMPLOYEE2.LICENSE IN EXPHIST
								ENDIF
								IF (NOT ISNULL(CURWTKEMPLOYEE2.HIREDATE)) THEN
									REPLACE EXPHIST.HIREDATE WITH CURWTKEMPLOYEE2.HIREDATE IN EXPHIST
								ENDIF
								IF (NOT ISNULL(CURWTKEMPLOYEE2.LABRACCT)) THEN
									REPLACE EXPHIST.LABRACCT WITH CURWTKEMPLOYEE2.LABRACCT IN EXPHIST
								ENDIF
							ENDIF
						ENDSCAN

					ENDIF  &&  .ExecSQL(lcSQL, 'CURWTKEMPLOYEE', RETURN_DATA_MANDATORY)

				ELSE
					* connection error
					.TrackProgress('Unable to connect to WTK System.', LOGIT+SENDIT)
				ENDIF   &&  .nSQLHandle > 0

				IF USED('CURWTKEMPLOYEE2') THEN
					USE IN CURWTKEMPLOYEE2
				ENDIF

*!*					CLOSE DATA

				SELECT EXPHIST
				LOCATE

				***************************************************
				***************************************************


				* get ss#s from ADP
				OPEN DATABASE F:\UTIL\ADPCONNECTION\CONNECT1

				.nSQLHandle = SQLCONNECT("PCPAYWIN","MBENNETT","VP737A")

				IF .nSQLHandle > 0 THEN

					* Base WHERE clause on whether it's the initial put or not.
					* (initial put will not include terminated statuses, but ongoing will)
					IF .lDidInitialPut THEN
						* this is an ongoing export - export (ALL NON-TERMINATED) + (TERMINATED AFTER DATE OF INITIAL PUT).
						* AND return data is not mandatory (i.e., no data returned will not trigger an error)
						*lcWhere = " WHERE {fn CONVERT(HIREDATE,SQL_DATE)} = " + .cInitialExportDateODBCFormat
						*lcWhere = ;
						*	" WHERE (STATUS <> 'T' AND {fn CONVERT(HIREDATE,SQL_DATE)} > " + .cInitialExportDateODBCFormat + ") OR " +
						lcWhere = ;
							" WHERE ((STATUS <> 'T') OR " + ;
							"       ((STATUS  = 'T') AND ({fn CONVERT(TERMINATIONDATE,SQL_DATE)} > " + .cInitialExportDateODBCFormat + ")))"
						*llMandatoryDataSwitch = RETURN_DATA_NOT_MANDATORY
						llMandatoryDataSwitch = RETURN_DATA_MANDATORY
						.TrackProgress('EXPORT MODE = ONGOING', LOGIT+SENDIT)
					ELSE
						* this is an initial put export - export all non-terminated employees
						* AND return data is mandatory (i.e., no data returned will trigger an error)
						lcWhere = " WHERE (STATUS <> 'T') "
						llMandatoryDataSwitch = RETURN_DATA_MANDATORY
						.TrackProgress('EXPORT MODE = INITIAL', LOGIT+SENDIT)
					ENDIF

					lcSQL = ;
						"SELECT " + ;
						" {fn IFNULL(STATUS,' ')} AS STATUS, " + ;
						" {fn IFNULL(AREACODEPHONE#,' ')} AS PHONE, " + ;
						" BIRTHDATE AS DOB, " + ;
						" {fn IFNULL(CITY,' ')} AS CITY, " + ;
						" {fn IFNULL(COMPANYCODE,'   ')} AS ADP_COMP, " + ;
						" {fn IFNULL(CUSTAREA1,' ')} AS CLASSIF, " + ;
						" {fn IFNULL(CUSTAREA2,' ')} AS GLACCT, " + ;
						" {fn IFNULL(CUSTAREA3,'   ')} AS WORKSITE, " + ;
						" {fn IFNULL(FILE#,0000)} AS FILE_NUM, " + ;
						" {fn IFNULL(FIRSTNAME,' ')} AS FIRST, " + ;
						" {fn IFNULL(LASTNAME,' ')} AS LAST, " + ;
						" {fn IFNULL(GENDER,' ')} AS SEXTYPE, " + ;
						" HIREDATE AS HIRE_DATE, " + ;
						" HIREDATE AS VHIRE_DATE, " + ;
						" {fn IFNULL(HOMEDEPARTMENT,'000000')} AS HOMEDEPARTMENT, " + ;
						" {fn IFNULL(NAME,' ')} AS EMPLOYEE, " + ;
						" {fn IFNULL(RATE1AMT,00000000.00)} AS HOURLYWAGE, " + ;
						" SOCIALSECURITY# AS SS_NUM, " + ;
						" {fn IFNULL(STATE,' ')} AS STATE, " + ;
						" {fn IFNULL(STREETLINE1,' ')} AS STREETLINE1, " + ;
						" {fn IFNULL(STREETLINE2,' ')} AS STREETLINE2, " + ;
						" {fn IFNULL(ZIPCODE,' ')} AS ZIP, " + ;
						" {fn IFNULL(STATEWRKDINCODE,'  ')} AS WKSTATE, " + ;
						" {fn IFNULL(ASSIGNEDSHIFT,'1')} AS SHIFT, " + ;
						" {fn IFNULL(EMPLOYEETYPE,'1018')} AS TIMERVWR " + ;
						" FROM REPORTS.V_EMPLOYEE " + ;
						lcWhere

					IF .lTestMode THEN
						.TrackProgress('lcSQL = ' + lcSQL, LOGIT+SENDIT)
					ENDIF

					IF USED('SQLCURSOR1') THEN
						USE IN SQLCURSOR1
					ENDIF

					*IF .ExecSQL(lcSQL, 'SQLCURSOR1', RETURN_DATA_MANDATORY) THEN
					IF .ExecSQL(lcSQL, 'SQLCURSOR1', llMandatoryDataSwitch) THEN

						IF USED('SQLCURSOR13') THEN
							USE IN SQLCURSOR13
						ENDIF
						IF USED('SQLCURSOR15') THEN
							USE IN SQLCURSOR15
						ENDIF

						m.WHEN = .dtNow

						.TrackProgress('Formatting ADP data....', LOGIT+NOWAITIT)

						* sort by BADGE_NUM + status DESCENDING so that Terminated Statuses come first. this should handle the case
						* where and employee switches ADP_COMP -- they would be terminated in the old one, active in the new one,
						* first a T status rec, then an A status rec.
						* NOTE: zeroing out HOURLYWAGE for companies other than SXI 08/12/05 MB
						SELECT ;
							(PADL(1000000 - INT(FILE_NUM),7,"0") + STATUS) AS CSORT, ;
							STATUS, ;
							INT(FILE_NUM) AS BADGE_NUM, ;
							INT(FILE_NUM) AS PERSONNUM, ;
							TTOD(NVL(DOB,ltBlankDateTime)) AS DOB, ;
							TTOD(NVL(HIRE_DATE,ltBlankDateTime)) AS HIRE_DATE, ;
							TTOD(NVL(HIRE_DATE,ltBlankDateTime)) AS PLAEFFDATE, ;
							TTOD(NVL(HIRE_DATE,ltBlankDateTime)) AS UASEFFDATE, ;
							"?" AS UASSTATUS, ;
							NVL(HIRE_DATE,ltBlankDateTime) AS BDGEFF_DT, ;
							INT(VAL(LEFT(HOMEDEPARTMENT,2))) AS DIVISION, ;
							INT(VAL(SUBSTR(HOMEDEPARTMENT,3))) AS DEPT, ;
							(LEFT(SS_NUM,3) + "-" + SUBSTR(SS_NUM,4,2) + "-" + SUBSTR(SS_NUM,6)) AS SS_NUM, ;
							UPPER((ALLTRIM(STREETLINE1) + " " + ALLTRIM(STREETLINE2))) AS ADDRESS, ;
							INT(VAL(GLACCT)) AS GLACCT, ;
							1 AS CLASSIF, ;
							(STRTRAN(PHONE," ","") + " ") AS PHONE, ;
							STRTRAN(UPPER(CITY),",","") AS CITY, ;
							LEFT(ADP_COMP,3) AS ADP_COMP, ;
							WORKSITE, ;
							UPPER(FIRST) AS FIRST, ;
							UPPER(LAST) AS LAST, ;
							SEXTYPE, ;
							UPPER(EMPLOYEE) AS EMPLOYEE, ;
							IIF(INLIST(LEFT(ADP_COMP,3),"SXI"),HOURLYWAGE,0000000.00) AS HOURLYWAGE, ;
							STATE, ;
							ZIP, ;
							LEFT(WKSTATE,2) AS WKSTATE, ;
							TTOD(NVL(HIRE_DATE,ltBlankDateTime)) AS VHIREDATE, ;
							TTOD(NVL(HIRE_DATE,ltBlankDateTime)) AS EMPEFFDATE, ;
							.T. AS LWTIMEKEEP, ;
							.F. AS LWEMPLOYEE, ;
							.F. AS LWMANAGER, ;
							.F. AS LPARTTIME, ;
							.F. AS LNEW9999, ;
							SPACE(50) AS CPAYRULE, ;
							"NO DEVICES " AS DEVICEGRP, ;
							SPACE(30) AS CSUPERVISR, ;
							0000 AS SPVSRBADGE, ;
							SPACE(5) AS CTIMEZONE, ;
							SPACE(40) AS CACRPROFIL, ;
							40 AS STDHOURS, ;
							"TRUE " AS BIOMETRIC, ;
							"HOME" AS ADDRTYPE, ;
							"HOME" AS PHONETYPE, ;
							"U.S.A" AS COUNTRY, ;
							SPACE(20) AS USERNAME, ;
							SPACE(20) AS PASSWORD, ;
							SPACE(20) AS LICENSE, ;
							SPACE(24) AS LABRACCT, ;
							"FALSE" AS CHGEPWRD, ;
							SPACE(1) AS FULLPART, ;
							"0800a-0500p M-F               " AS SCHEDGRP, ;
							SHIFT, ;
							TIMERVWR, ;
							.F. AS LNEW, ;
							.F. AS LEXPORTED, ;
							.T. AS VALID ;
							FROM SQLCURSOR1 ;
							INTO CURSOR SQLCURSOR13


						*	IIF(LEFT(ADP_COMP,3)='SXI',TIMERVWR,'1018') AS TIMERVWR, ;
						*!*								'1' AS SHIFT, ;
						*!*								'1018' AS TIMERVWR, ;

						* log Hire dates > today as errors
						SELECT SQLCURSOR13
						SCAN FOR (HIRE_DATE > ldToday)
							.TrackProgress('ERROR: ADP Employee [' + ALLTRIM(SQLCURSOR13.EMPLOYEE) + '], FILE# [' + ;
								TRANSFORM(SQLCURSOR13.BADGE_NUM) + '] ADP Status = [' + ALLTRIM(SQLCURSOR13.STATUS) + ;
								'] Hire Date > today: [' + DTOC(SQLCURSOR13.HIRE_DATE) + ']', LOGIT+SENDIT)
						ENDSCAN

						* filter out Hire dates > today
						SELECT * ;
							FROM SQLCURSOR13 ;
							INTO CURSOR SQLCURSOR15 ;
							WHERE NOT (HIRE_DATE > ldToday) ;
							ORDER BY EMPLOYEE

						IF USED('SQLCURSOR13') THEN
							USE IN SQLCURSOR13
						ENDIF


						*!*	SELECT SQLCURSOR15
						*!*	BROWSE
						*!*	SELECT SQLCURSOR15
						*!*	LOCATE
						*!*
						*!*	SET STEP ON

						IF USED('SQLCURSOR2') THEN
							USE IN SQLCURSOR2
						ENDIF

						* exclude Over The Road Drivers
						* ALLOWING THEM 1/8/07 MB
						*!*							SELECT * ;
						*!*								FROM SQLCURSOR15 ;
						*!*								INTO CURSOR SQLCURSOR2 ;
						*!*								WHERE NOT ( (ADP_COMP = 'SXI') AND (DIVISION = 2) AND (CLASSIF = 1) AND (HOURLYWAGE = 0.00) ) ;
						*!*								ORDER BY CSORT DESCENDING ;
						*!*								READWRITE
						SELECT * ;
							FROM SQLCURSOR15 ;
							INTO CURSOR SQLCURSOR2 ;
							ORDER BY CSORT DESCENDING ;
							READWRITE

						.TrackProgress('Adding fields from FoxPro....', LOGIT+NOWAITIT)
						* Make copy of employee table
						IF USED('CUREMPLOYEE') THEN
							USE IN CUREMPLOYEE
						ENDIF

						SELECT * FROM (.cFoxProEmployeeTable) INTO CURSOR CUREMPLOYEE READWRITE
						IF USED('EMPLOYEE') THEN
							USE IN EMPLOYEE
						ENDIF

						SELECT CUREMPLOYEE
						INDEX ON EMP_NUM TAG EMP_NUM
						*BROWSE

						* only scan for non-Term statuses; Terminated won't be in FoxPro so we can't get the extra info.
						* But Kronos shouldn't need additional data for ee's to be Terminated anyway.
						SELECT SQLCURSOR2
						SCAN FOR STATUS <> 'T'
							SELECT CUREMPLOYEE
							LOCATE FOR EMP_NUM = SQLCURSOR2.BADGE_NUM
							IF FOUND() THEN
								* populate extra fields needed by Kronos
								REPLACE SQLCURSOR2.VHIREDATE WITH CUREMPLOYEE.VHIRE_DATE, ;
									SQLCURSOR2.LPARTTIME WITH CUREMPLOYEE.PART_TIME
							ELSE
								* error: log and mark as invalid record
								LOCATE
								* 3/16/06 MB:  if not found, use default values instead of invalidating the import
								*REPLACE SQLCURSOR2.VALID WITH .F.
								*!*									REPLACE SQLCURSOR2.VHIREDATE WITH CUREMPLOYEE.HIRE_DATE, ;
								*!*										SQLCURSOR2.LPARTTIME WITH .F.
								REPLACE SQLCURSOR2.VHIREDATE WITH SQLCURSOR2.HIRE_DATE, ;
									SQLCURSOR2.LPARTTIME WITH .F.
								.TrackProgress('WARNING: ADP Employee [' + ALLTRIM(SQLCURSOR2.EMPLOYEE) + '], FILE# [' + ;
									TRANSFORM(SQLCURSOR2.BADGE_NUM) + '] ADP Status = [' + ALLTRIM(SQLCURSOR2.STATUS) + ;
									'] not found as ACTIVE in FoxPro', LOGIT+SENDIT)
							ENDIF

						ENDSCAN

						*!*							* open Managers table
						*!*							USE F:\UTIL\KRONOS\DATA\MANAGERS IN 0 EXCLUSIVE ALIAS MANAGERS
						*!*							SELECT MANAGERS
						*!*							DELETE TAG ALL
						*!*							INDEX ON FILE_NUM TAG FILE_NUM
						*!*							INDEX ON SUP_NUM TAG SUP_NUM

						*!*							* open employee/supervisors table
						*!*							USE F:\UTIL\KRONOS\DATA\EMPSV IN 0 EXCLUSIVE ALIAS EMPSV
						*!*							SELECT EMPSV
						*!*							DELETE TAG ALL
						*!*							INDEX ON FILE_NUM TAG FILE_NUM
						*!*							INDEX ON SUP_NUM TAG SUP_NUM

						*!*							* open shifts table
						*!*							USE F:\UTIL\KRONOS\DATA\SHIFTS IN 0 ALIAS SHIFTS
						*!*							SELECT SHIFTS
						*!*							LOCATE

						*!*							* open export history table
						*!*							USE F:\UTIL\KRONOS\DATA\EXPHIST IN 0 ALIAS EXPHIST

						*SET STEP ON

						.TrackProgress('Calculating Kronos fields....', LOGIT+NOWAITIT)
						lcEmployeeList = ""
						lnEmployeesSent = 0
						SELECT SQLCURSOR2
						SCAN

							* determine Pay Rule
							lcPayRule = ""
							DO CASE
								CASE (SQLCURSOR2.ADP_COMP = 'SXI') AND (SQLCURSOR2.DIVISION = 2) AND (SQLCURSOR2.CLASSIF = 1) AND (SQLCURSOR2.HOURLYWAGE = 0.00)
									lcPayRule = ALLTRIM(SQLCURSOR2.WORKSITE) + " OTR Drivers"
								OTHERWISE
									IF SQLCURSOR2.ADP_COMP = "SXI" THEN
										IF SQLCURSOR2.LPARTTIME THEN
											lcSalaryType = "PART-TIME"
										ELSE
											lcSalaryType = "HOURLY"
										ENDIF
									ELSE
										lcSalaryType = "SALARIED"
									ENDIF
									lcPayRule = ALLTRIM(SQLCURSOR2.WORKSITE) + " " + lcSalaryType
							ENDCASE

							*********************** 7/27/06 **********************************
							* WE NOW WANT TO USE THE PAY-FROM-SCHEDULE RULES FOR SALARIED, SO....
							IF SQLCURSOR2.ADP_COMP <> 'SXI' THEN
								lcPayRule = lcPayRule + " - PAY FROM SCHEDULE"
							ENDIF
							*********************** 7/27/06 **********************************

							REPLACE SQLCURSOR2.CPAYRULE WITH lcPayRule

							* determine DEVICE GROUP
							* As of 1/19/06 all of NJ is now rolled out (or will be shortly),
							* and are allowed to cross-punch, so put them in ALL DEVICES.
							DO CASE
								CASE SQLCURSOR2.ADP_COMP <> "SXI"
									* Salaried will not be enrolled on clocks so don't assign them to any device group
									lcDeviceGroup = "NO DEVICES "
								CASE (SQLCURSOR2.ADP_COMP = 'SXI') AND (SQLCURSOR2.DIVISION = 2) AND (SQLCURSOR2.CLASSIF = 1) AND (SQLCURSOR2.HOURLYWAGE = 0.00)
									* OTR Drivers will not be enrolled on clocks so don't assign them to any device group
									lcDeviceGroup = "NO DEVICES "
								CASE INLIST(UPPER(ALLTRIM(SQLCURSOR2.WORKSITE)),"FL1","FL2")
									lcDeviceGroup = "FL XPUNCH  "
								CASE INLIST(UPPER(ALLTRIM(SQLCURSOR2.WORKSITE)),"GA1")
									lcDeviceGroup = "GA XPUNCH  "
								CASE INLIST(UPPER(ALLTRIM(SQLCURSOR2.WORKSITE)),"CA4","CA5","CA6","CA7","SP1","SP2")
									lcDeviceGroup = "CA XPUNCH  "
								CASE INLIST(UPPER(ALLTRIM(SQLCURSOR2.WORKSITE)),"NC1")
									lcDeviceGroup = "NC XPUNCH  "
								CASE LEFT((UPPER(ALLTRIM(SQLCURSOR2.WORKSITE))),2) = "NJ"
									lcDeviceGroup = "NJ XPUNCH  "
								CASE LEFT((UPPER(ALLTRIM(SQLCURSOR2.WORKSITE))),2) = "NY"
									lcDeviceGroup = "NY XPUNCH  "
								CASE INLIST(UPPER(ALLTRIM(SQLCURSOR2.WORKSITE)),"SC1")
									lcDeviceGroup = "SC XPUNCH  "
								CASE INLIST(UPPER(ALLTRIM(SQLCURSOR2.WORKSITE)),"TX1")
									lcDeviceGroup = "TX XPUNCH  "
								OTHERWISE
									lcDeviceGroup = "NO DEVICES "
							ENDCASE
							REPLACE SQLCURSOR2.DEVICEGRP WITH lcDeviceGroup

							*!*								* get TIMERVWR
							*!*								SELECT EMPSV
							*!*								LOCATE FOR FILE_NUM = SQLCURSOR2.BADGE_NUM
							*!*								IF FOUND() THEN
							*!*									IF NOT EMPTY(EMPSV.SUP_NUM) THEN
							*!*										REPLACE SQLCURSOR2.TIMERVWR WITH PADL(EMPSV.SUP_NUM,4,"0")
							*!*									ENDIF
							*!*								ENDIF
							*!*								LOCATE

							*!*								* get  shift code
							*!*								SELECT SHIFTS
							*!*								IF EMPTY(SQLCURSOR2.SHIFT) OR LEFT(SQLCURSOR2.SHIFT,1) = "?" THEN
							*!*									REPLACE SQLCURSOR2.SHIFTCODE WITH NOSHIFTCODE
							*!*								ELSE
							*!*									LOCATE FOR (ALLTRIM(SHIFT) = ALLTRIM(SQLCURSOR2.SHIFT))
							*!*									IF FOUND() THEN
							*!*										REPLACE SQLCURSOR2.SHIFTCODE WITH SHIFTS.CODE
							*!*									ELSE
							*!*										LOCATE
							*!*										REPLACE SQLCURSOR2.SHIFTCODE WITH NOSHIFTCODE
							*!*									ENDIF
							*!*								ENDIF
							*!*								LOCATE

							* determine time zone
							DO CASE
								CASE INLIST(SQLCURSOR2.WKSTATE,"CA")
									lcTimeZone = "13004"   && PST
								OTHERWISE
									lcTimeZone = "13011"  && EST  && CST = 13007
							ENDCASE
							REPLACE SQLCURSOR2.CTIMEZONE WITH lcTimeZone

							* calc Employment Status Effective Date ( = Kronos Accruals Date )
							ldEmpEffDate = {}
							DO CASE
								CASE SQLCURSOR2.HIRE_DATE = MIRA_LOMA_ACQUISITION_HIRE_DATE
									* construct date = Jan 1st of vhire-date year.
									ldEmpEffDate = CTOD( "01/01/" + TRANSFORM(YEAR(SQLCURSOR2.VHIREDATE)))
								OTHERWISE
									ldEmpEffDate = SQLCURSOR2.VHIREDATE
							ENDCASE
							REPLACE SQLCURSOR2.EMPEFFDATE WITH ldEmpEffDate


							*********************************************************************************************************8
							*********************************************************************************************************8
							* calculate Accrual Profile
							lcAccrualProfile = ""
							* activate when Accruals are defined in Kronos.
							DO CASE
								CASE SQLCURSOR2.LPARTTIME
									* no accrual profile so they don't accrue anything
									lcAccrualProfile = ""
								CASE (SQLCURSOR2.ADP_COMP = 'SXI') AND (SQLCURSOR2.DIVISION = 2) AND (SQLCURSOR2.CLASSIF = 1) AND (SQLCURSOR2.HOURLYWAGE = 0.00)
									* EE is Over the road driver
									lcAccrualProfile = "OTR Driver Accruals"
								CASE (SQLCURSOR2.HIRE_DATE = MIRA_LOMA_ACQUISITION_HIRE_DATE) OR ;
										((SQLCURSOR2.HIRE_DATE = MIRA_LOMA_ACQUISITION_HIRE_DATE_TWO) AND (SQLCURSOR2.DIVISION = 50))
									* EE part of Mira Loma Acquisition
									IF GOMONTH(SQLCURSOR2.EMPEFFDATE,(5 * 12)) <= MIRA_LOMA_ACQUISITION_HIRE_DATE THEN
										* EE was here at least 5 years when ML was acquired: invoke ML Grandfathered Vacation clause
										lcAccrualProfile = "Mira Loma Grandfathered Accruals"
									ELSE
										lcAccrualProfile = "Standard Accruals"
									ENDIF
								CASE INLIST(SQLCURSOR2.DIVISION,6) OR INLIST(SQLCURSOR2.SS_NUM,"105-32-8424")
									* EE was part of Freight Management acquisition
									lcAccrualProfile = "Freight Management Accruals"
								OTHERWISE
									lcAccrualProfile = "Standard Accruals"
							ENDCASE
							REPLACE SQLCURSOR2.CACRPROFIL WITH lcAccrualProfile
							*********************************************************************************************************8
							*********************************************************************************************************8

							* determine License Name
							* We now want to give all Salaried the Workforce Employee license so they can manage own timecards 7/26/05 MB
							* If not already a manager (Managers can do their own timecards), set the proper flag for Employee license.
							IF SQLCURSOR2.ADP_COMP <> "SXI" THEN
								* EE IS SALARIED
								IF NOT SQLCURSOR2.LWMANAGER THEN
									REPLACE SQLCURSOR2.LWEMPLOYEE WITH .T.
								ENDIF
							ENDIF

							DO CASE
								CASE SQLCURSOR2.STATUS = "T"
									* TERMINATED - deactivate license!
									**lcLicense = ""
									* but for now don't deactivate because this is causing problems 3/14/06 MB
									lcLicense = "WORKFORCE TIMEKEEPER"
								CASE SQLCURSOR2.LWMANAGER
									lcLicense = "WORKFORCE MANAGER"
									*REPLACE SQLCURSOR2.UASSTATUS WITH "1"
								CASE SQLCURSOR2.LWEMPLOYEE
									lcLicense = "WORKFORCE EMPLOYEE"
									*REPLACE SQLCURSOR2.UASSTATUS WITH "0"
								OTHERWISE
									lcLicense = "WORKFORCE TIMEKEEPER"
									*REPLACE SQLCURSOR2.UASSTATUS WITH "0"
							ENDCASE
							REPLACE SQLCURSOR2.LICENSE WITH lcLicense, SQLCURSOR2.UASSTATUS WITH "1"

							* XXX Username and Password only apply to Workforce Employee and Manager licenses
							*IF SQLCURSOR2.LWMANAGER OR SQLCURSOR2.LWEMPLOYEE THEN
							* determine username/pwrd
							lcUsername = STRTRAN(STRTRAN(STRTRAN(LEFT(SQLCURSOR2.FIRST,1) + ALLTRIM(SQLCURSOR2.LAST)," ",""),".",""),"'","") + ;
								ALLTRIM(STR(SQLCURSOR2.BADGE_NUM))
							lcPWRD = STRTRAN(STRTRAN(STRTRAN(LEFT(SQLCURSOR2.FIRST,1) + ALLTRIM(SQLCURSOR2.LAST)," ",""),".",""),"'","") + ;
								ALLTRIM(STR(10000 - SQLCURSOR2.BADGE_NUM))
							REPLACE SQLCURSOR2.USERNAME WITH lcUsername, SQLCURSOR2.PASSWORD WITH lcPWRD
							*ENDIF

							* FULLTIME VS PARTTIME
							REPLACE SQLCURSOR2.FULLPART WITH IIF(SQLCURSOR2.LPARTTIME,"P","F")


							* create Primary Labor Account
							REPLACE SQLCURSOR2.LABRACCT WITH ;
								ALLTRIM(SQLCURSOR2.ADP_COMP) + "/" + ;
								PADL(SQLCURSOR2.DIVISION,2,"0") + "/" + ;
								PADL(SQLCURSOR2.DEPT,4,"0") + "/" + ;
								ALLTRIM(SQLCURSOR2.WORKSITE) + "/" + ;
								ALLTRIM(STR(SQLCURSOR2.CLASSIF)) + "/" + ;
								ALLTRIM(SQLCURSOR2.SHIFT) + "/" + ;
								ALLTRIM(SQLCURSOR2.TIMERVWR)

							* adjust certain values
							IF (SQLCURSOR2.DIVISION = 2) AND (SQLCURSOR2.CLASSIF) = 1 AND (SQLCURSOR2.HOURLYWAGE = 0.00) THEN
								REPLACE SQLCURSOR2.STDHOURS WITH 0, SQLCURSOR2.BIOMETRIC WITH "FALSE"
							ENDIF

							IF ALLTRIM(SQLCURSOR2.PHONE) = "()" THEN
								REPLACE SQLCURSOR2.PHONE WITH ""
							ENDIF


							********************************************************************
							********************************************************************
							* 8/18/06 MB: populate these fields with Time Reviewer info, to enable exception reporting

							* if no supervisor has been assigned, make it default to Lucille
							*IF SQLCURSOR2.SPVSRBADGE = 0 THEN
							REPLACE SQLCURSOR2.SPVSRBADGE WITH LUCILLES_BADGE_NUM, SQLCURSOR2.CSUPERVISR WITH LUCILLES_NAME
							*ENDIF
							*!*								lnTRNumber = INT(VAL(SQLCURSOR2.TIMERVWR))
							*!*								lcTRName = ""
							*!*
							*!*								* GET TR NAME from time reviewer table
							*!*								SELECT RVWRLIST
							*!*								LOCATE FOR TimeRvwr = SQLCURSOR2.TIMERVWR
							*!*								IF FOUND() THEN
							*!*									lcTRName = RVWRLIST.NAME
							*!*								ENDIF
							*!*								SELECT RVWRLIST
							*!*								LOCATE
							*!*
							*!*								REPLACE SQLCURSOR2.SPVSRBADGE WITH lnTRNumber, SQLCURSOR2.CSUPERVISR WITH lcTRName
							********************************************************************
							********************************************************************



							********************************************************************
							********************************************************************
							* determine if this employee needs to be sent to Kronos
							* We'll set LNEW = .T. if:
							* a) ee has never been sent before
							*  or
							* b) ee has a different status than before

							llNew = .F.
							llExported = .F.
							llChanged = .F.
							m.BADGE_NUM = SQLCURSOR2.BADGE_NUM
							m.ADP_COMP = SQLCURSOR2.ADP_COMP
							m.EMPLOYEE = SQLCURSOR2.EMPLOYEE
							m.STATUS = SQLCURSOR2.STATUS
							m.HOURLYWAGE = SQLCURSOR2.HOURLYWAGE
							m.LABRACCT = SQLCURSOR2.LABRACCT
							m.CPAYRULE = SQLCURSOR2.CPAYRULE
							m.CACRPROFIL = SQLCURSOR2.CACRPROFIL
							m.LICENSE = SQLCURSOR2.LICENSE
							m.SCHEDGRP = SQLCURSOR2.SCHEDGRP

							m.WHYXPORTED = ""
							m.LNEW9999 = .F.

							SELECT EXPHIST
							LOCATE FOR ;
								BADGE_NUM = SQLCURSOR2.BADGE_NUM ;
								AND ADP_COMP = SQLCURSOR2.ADP_COMP
							IF NOT FOUND() THEN
								llNew = .T.
								* never exported - add to history

								****************************************
								* new hire; force effdates = hiredate 11/15/06 mb
								REPLACE SQLCURSOR2.PLAEFFDATE WITH SQLCURSOR2.HIRE_DATE, ;
									SQLCURSOR2.UASEFFDATE WITH SQLCURSOR2.HIRE_DATE, ;
									SQLCURSOR2.BDGEFF_DT WITH SQLCURSOR2.HIRE_DATE
								****************************************

								m.WHYXPORTED = m.WHYXPORTED + "New Badge# / ADP_COMP combination.  "
								INSERT INTO EXPHIST FROM MEMVAR
							ELSE
								llExported = .T.
								IF EXPHIST.STATUS <> SQLCURSOR2.STATUS THEN
									* was exported previously, but status has changed
									*llNew = .T.
									llChanged = .T.
									m.WHYXPORTED = m.WHYXPORTED + "Status Change; Old = [" + ALLTRIM(EXPHIST.STATUS) + "] New = [" + ;
										ALLTRIM(SQLCURSOR2.STATUS) + "].  "

									**************************************************************
									**************************************************************
									* 10/6/2006 MB:
									* if employee was REHIRED (STATUS went from T to A for same file#), then
									* use the hire-date from exphist, not the new one coming in.
									* This is to preserve the original hire-date in Kronos for employees who are terminated then
									* rehired with the same file#.
									IF (EXPHIST.STATUS = 'T') AND (SQLCURSOR2.STATUS = 'A') THEN
										IF NOT EMPTY(EXPHIST.HIREDATE) THEN
											ldHireDate = EXPHIST.HIREDATE
											REPLACE SQLCURSOR2.HIRE_DATE WITH ldHireDate
											.TrackProgress('NOTE: ' + ALLTRIM(SQLCURSOR2.EMPLOYEE) + ;
												' rehired with same badge # -- using HireDate from Kronos: ' + TRANSFORM(ldHireDate), LOGIT+SENDIT)
										ENDIF
									ENDIF
									**************************************************************
									**************************************************************


								ENDIF
								IF EXPHIST.LABRACCT <> SQLCURSOR2.LABRACCT THEN
									*llNew = .T.
									llChanged = .T.
									m.WHYXPORTED = m.WHYXPORTED + "Labor Acct Change; Old = [" + ALLTRIM(EXPHIST.LABRACCT) + "] New = [" + ;
										ALLTRIM(SQLCURSOR2.LABRACCT) + "].  "


									* 8/31/07 mb, TO ONLY PROCESS CHANGES TO 9999 for time reviewer
									IF .lOnlyProcesshangesTo9999 THEN
										IF (RIGHT(EXPHIST.LABRACCT,4) <> RIGHT(SQLCURSOR2.LABRACCT,4)) AND (RIGHT(SQLCURSOR2.LABRACCT,4) = '9999') THEN
											REPLACE SQLCURSOR2.LNEW9999 WITH .T.
											lcNew9999 = lcNew9999 + SQLCURSOR2.ADP_COMP + " " + PADL(SQLCURSOR2.BADGE_NUM,4,"0") + "   " + LEFT(SQLCURSOR2.EMPLOYEE,30) + ;
												" " + SQLCURSOR2.STATUS + "   " + m.WHYXPORTED + CRLF
										ENDIF
									ENDIF


									*********************************************************************************************
									*********************************************************************************************
									* 10/23/06 MB
									**** if EE changed Worksite, log a warning to manually change their Pay Rule in Kronos.
									**** ( we don't want to oevrwrite an existing Pay Rule in this case; hence indicate manual change is necessary)
									lcOldWorkSite = SUBSTR(UPPER(ALLTRIM( EXPHIST.LABRACCT )),13,3)
									lcNewWorkSite = UPPER(ALLTRIM(SQLCURSOR2.WORKSITE))
									IF NOT (lcOldWorkSite == lcNewWorkSite) THEN
										.TrackProgress('!! ' + ALLTRIM(SQLCURSOR2.EMPLOYEE) + ;
											' had WORKSITE CHANGE, OLD = [' + lcOldWorkSite + '] NEW = [' + lcNewWorkSite + '] -- REVIEW PAY RULE & DEVICE GROUP IN KRONOS!!! ', LOGIT+SENDIT)
									ENDIF
									*********************************************************************************************
									*********************************************************************************************


									*!*										* update the status/date
									*!*										REPLACE EXPHIST.LABRACCT WITH m.LABRACCT, EXPHIST.WHEN WITH m.WHEN, EXPHIST.WHYXPORTED WITH m.WHYXPORTED
								ENDIF
								IF EXPHIST.HOURLYWAGE <> SQLCURSOR2.HOURLYWAGE THEN
									* was exported previously, but HOURLY RATE has changed
									*llNew = .T.
									llChanged = .T.
									m.WHYXPORTED = m.WHYXPORTED + "Wage Change; Old = [" + TRANSFORM(EXPHIST.HOURLYWAGE) + "] New = [" + ;
										TRANSFORM(SQLCURSOR2.HOURLYWAGE) + "].  "
									*!*										* update the status/date
									*!*										REPLACE EXPHIST.HOURLYWAGE WITH m.HOURLYWAGE, EXPHIST.WHEN WITH m.WHEN, EXPHIST.WHYXPORTED WITH m.WHYXPORTED
								ENDIF

								**************************************************************
								****** 10/03/05 MB ****************************************
								* since the employee was not new, replace certain values with what's in their EXP HISTORY table,
								* so that those Kronos settings are not lost on reimport.
								* but...if the exphist License is empty, which means a terminated employee, we don't want to
								* replace with that, because an employee going from term to Active needs a license to show up in kronos.
								IF EMPTY(EXPHIST.LICENSE) THEN
									* note absence of License in replace
									REPLACE ;
										SQLCURSOR2.DEVICEGRP WITH EXPHIST.DEVICEGRP, ;
										SQLCURSOR2.CPAYRULE WITH EXPHIST.CPAYRULE, ;
										SQLCURSOR2.CACRPROFIL WITH EXPHIST.CACRPROFIL, ;
										SQLCURSOR2.SCHEDGRP WITH EXPHIST.SCHEDGRP

									*!*										REPLACE ;
									*!*											SQLCURSOR2.CPAYRULE WITH EXPHIST.CPAYRULE, ;
									*!*											SQLCURSOR2.SCHEDGRP WITH EXPHIST.SCHEDGRP
								ELSE
									REPLACE ;
										SQLCURSOR2.DEVICEGRP WITH EXPHIST.DEVICEGRP, ;
										SQLCURSOR2.CPAYRULE WITH EXPHIST.CPAYRULE, ;
										SQLCURSOR2.CACRPROFIL WITH EXPHIST.CACRPROFIL, ;
										SQLCURSOR2.SCHEDGRP WITH EXPHIST.SCHEDGRP, ;
										SQLCURSOR2.LICENSE WITH EXPHIST.LICENSE

									*!*										REPLACE ;
									*!*											SQLCURSOR2.CPAYRULE WITH EXPHIST.CPAYRULE, ;
									*!*											SQLCURSOR2.SCHEDGRP WITH EXPHIST.SCHEDGRP, ;
									*!*											SQLCURSOR2.LICENSE WITH EXPHIST.LICENSE
								ENDIF
								**************************************************************
								**************************************************************

								*!*									* BUT ... if the employee is now terminated, blank out the license
								*!*									* regardless of what was in exphist table 10/5/06 MB
								*!*									IF SQLCURSOR2.STATUS = "T" THEN
								*!*										REPLACE SQLCURSOR2.LICENSE WITH ""
								*!*									ENDIF

								**************************************************************
								**************************************************************
								* 10/5/2006 MB:
								* if employee was not new, but something changed (status, for example), then
								* we do not want to continue using hire-date for these effective date fields;
								* we want to make the change effective TODAY.
								* This is to prevent errors loading into Kronos.
								IF llChanged THEN
									* Make change effective LAST SUNDAY, not today 10/24/06 MB
									*REPLACE SQLCURSOR2.PLAEFFDATE WITH ldToday, SQLCURSOR2.UASEFFDATE WITH ldToday
									* Revised 11/7/06 MB to use the previous past sunday (i.e., the Sunday starting previous pay period)
									* if any of the following is true:
									* 1) force flag 'lForcePriorPastSundayEffectiveDates' is set to True
									* 2) This is running anytime on a Monday (so it will change PLAccount for entire prior pay period)
									* 3) This is running on Tuesday before 8am
									* Otherwise we assume that the change was not meant to affect the current payroll being produced,
									* and we will use the Latest Past Sunday for the effective dates.
									IF .lForcePriorPastSundayEffectiveDates OR llRunningOnMonday OR ( llRunningOnTuesday AND llBefore8AM ) THEN
										*REPLACE SQLCURSOR2.PLAEFFDATE WITH ldPriorPastSunday, SQLCURSOR2.UASEFFDATE WITH ldPriorPastSunday
										REPLACE SQLCURSOR2.PLAEFFDATE WITH ldPriorPastSunday
									ELSE
										*REPLACE SQLCURSOR2.PLAEFFDATE WITH ldLatestPastSunday, SQLCURSOR2.UASEFFDATE WITH ldLatestPastSunday
										REPLACE SQLCURSOR2.PLAEFFDATE WITH ldLatestPastSunday
									ENDIF
								ENDIF
								**************************************************************
								**************************************************************

							ENDIF  &&  NOT FOUND()

							IF llChanged THEN
								llNew = .T.
								REPLACE EXPHIST.STATUS WITH m.STATUS, ;
									EXPHIST.LABRACCT WITH m.LABRACCT, ;
									EXPHIST.HOURLYWAGE WITH m.HOURLYWAGE, ;
									EXPHIST.WHEN WITH m.WHEN, ;
									EXPHIST.WHYXPORTED WITH m.WHYXPORTED
							ENDIF
							IF llNew THEN
								lcEmployeeList = lcEmployeeList + SQLCURSOR2.ADP_COMP + " " + PADL(SQLCURSOR2.BADGE_NUM,4,"0") + "   " + LEFT(SQLCURSOR2.EMPLOYEE,30) + ;
									" " + SQLCURSOR2.STATUS + "   " + m.WHYXPORTED + CRLF
								lnEmployeesSent = lnEmployeesSent + 1

								*!*	SELECT SQLCURSOR2
								*!*	BROW

							ENDIF

							SELECT EXPHIST
							LOCATE

							REPLACE SQLCURSOR2.LNEW WITH llNew, SQLCURSOR2.LEXPORTED WITH llExported
							********************************************************************
							********************************************************************


							* Other error checking/validations
							IF EMPTY(SQLCURSOR2.HIRE_DATE) THEN
								REPLACE SQLCURSOR2.VALID WITH .F.
								.TrackProgress('ERROR: ADP Employee [' + ALLTRIM(SQLCURSOR2.EMPLOYEE) + '], FILE# [' + ;
									TRANSFORM(SQLCURSOR2.BADGE_NUM) + '] ADP Status = [' + ALLTRIM(SQLCURSOR2.STATUS) + ;
									'] EMPTY HIRE DATE!', LOGIT+SENDIT)
							ENDIF

							IF EMPTY(SQLCURSOR2.DOB) THEN
								REPLACE SQLCURSOR2.VALID WITH .F.
								.TrackProgress('ERROR: ADP Employee [' + ALLTRIM(SQLCURSOR2.EMPLOYEE) + '], FILE# [' + ;
									TRANSFORM(SQLCURSOR2.BADGE_NUM) + '] ADP Status = [' + ALLTRIM(SQLCURSOR2.STATUS) + ;
									'] EMPTY DATE OF BIRTH!', LOGIT+SENDIT)
							ENDIF

							IF NOT INLIST(SQLCURSOR2.ADP_COMP,"ZXU","SXI","AXA","AM3") THEN
								REPLACE SQLCURSOR2.VALID WITH .F.
								.TrackProgress('ERROR: ADP Employee [' + ALLTRIM(SQLCURSOR2.EMPLOYEE) + '], FILE# [' + ;
									TRANSFORM(SQLCURSOR2.BADGE_NUM) + '] ADP Status = [' + ALLTRIM(SQLCURSOR2.STATUS) + ;
									'] invalid ADP_COMP: [' + SQLCURSOR2.ADP_COMP + ']', LOGIT+SENDIT)
							ENDIF
							*IF EMPTY(SQLCURSOR2.DIVISION) THEN
							*!*								IF NOT INLIST(SQLCURSOR2.DIVISION,0,1,2,3,4,5,6,7,11,32,50,52,53,54,55,56,57,58) AND ;
							*!*									NOT INLIST(SQLCURSOR2.DIVISION,60,61,62,71,72,73,74,75,76,80,90) THEN
							IF NOT IsValidNumericDivision(SQLCURSOR2.DIVISION) THEN
								REPLACE SQLCURSOR2.VALID WITH .F.
								.TrackProgress('ERROR: ADP Employee [' + ALLTRIM(SQLCURSOR2.EMPLOYEE) + '], FILE# [' + ;
									TRANSFORM(SQLCURSOR2.BADGE_NUM) + '] ADP Status = [' + ALLTRIM(SQLCURSOR2.STATUS) + ;
									'] invalid DIVISION: [' + TRANSFORM(SQLCURSOR2.DIVISION) + ']', LOGIT+SENDIT)
							ENDIF
							IF EMPTY(SQLCURSOR2.DEPT) THEN
								REPLACE SQLCURSOR2.VALID WITH .F.
								.TrackProgress('ERROR: ADP Employee [' + ALLTRIM(SQLCURSOR2.EMPLOYEE) + '], FILE# [' + ;
									TRANSFORM(SQLCURSOR2.BADGE_NUM) + '] ADP Status = [' + ALLTRIM(SQLCURSOR2.STATUS) + ;
									'] invalid DEPT: [' + TRANSFORM(SQLCURSOR2.DEPT) + ']', LOGIT+SENDIT)
							ENDIF
							IF NOT INLIST(SQLCURSOR2.WORKSITE,"CA4","CA5","CA6","CA7","FL1","FL2","GA1","NC1","NJ1","NJ2","NJ3","NJ4","NJ5") AND ;
									NOT INLIST(SQLCURSOR2.WORKSITE,"NY1","NY2","NY3","PA1","SC1","SP1","SP2","TN1","TX1") THEN
								REPLACE SQLCURSOR2.VALID WITH .F.
								.TrackProgress('ERROR: ADP Employee [' + ALLTRIM(SQLCURSOR2.EMPLOYEE) + '], FILE# [' + ;
									TRANSFORM(SQLCURSOR2.BADGE_NUM) + '] ADP Status = [' + ALLTRIM(SQLCURSOR2.STATUS) + ;
									'] invalid WORKSITE: [' + SQLCURSOR2.WORKSITE + ']', LOGIT+SENDIT)
							ENDIF
							IF EMPTY(SQLCURSOR2.CLASSIF) THEN
								REPLACE SQLCURSOR2.VALID WITH .F.
								.TrackProgress('ERROR: ADP Employee [' + ALLTRIM(SQLCURSOR2.EMPLOYEE) + '], FILE# [' + ;
									TRANSFORM(SQLCURSOR2.BADGE_NUM) + '] ADP Status = [' + ALLTRIM(SQLCURSOR2.STATUS) + ;
									'] invalid CLASSIF: [' + TRANSFORM(SQLCURSOR2.CLASSIF) + ']', LOGIT+SENDIT)
							ENDIF

							** Kronos cannot import employee who has hiredate in the future; log error in this case
							IF SQLCURSOR2.HIRE_DATE > .dToday THEN
								REPLACE SQLCURSOR2.VALID WITH .F.
								.TrackProgress('ERROR: ADP Employee [' + ALLTRIM(SQLCURSOR2.EMPLOYEE) + '], FILE# [' + ;
									TRANSFORM(SQLCURSOR2.BADGE_NUM) + '] ADP Status = [' + ALLTRIM(SQLCURSOR2.STATUS) + ;
									'] Kronos cannot import future hire date: ' + TRANSFORM(SQLCURSOR2.HIRE_DATE), LOGIT+SENDIT)
							ENDIF

							* if there was an error, blank out labracct field so rec will get processed again on next run,
							* when hopefully data probably will have been corrected.  03/30/06 mb
							IF NOT SQLCURSOR2.VALID THEN

								SELECT EXPHIST
								SCAN FOR (BADGE_NUM = SQLCURSOR2.BADGE_NUM) AND (TTOD(WHEN) = .dToday)
									REPLACE EXPHIST.LABRACCT WITH ""
								ENDSCAN

								SELECT EXPHIST
								LOCATE

							ENDIF


						ENDSCAN

						.TrackProgress('Updated Export History table.', LOGIT)

						* change to proper STATUS codes
						SELECT SQLCURSOR2
						SCAN
							DO CASE
								CASE STATUS = "A"
									lcNewStatus = "1"   &&& ACTIVE
								CASE STATUS = "T"
									lcNewStatus = "3"   &&& TERMINATED
								OTHERWISE
									lcNewStatus = "2"   &&& INACTIVE
							ENDCASE
							REPLACE SQLCURSOR2.STATUS WITH lcNewStatus
						ENDSCAN

						*SELECT CACRPROFIL FROM SQLCURSOR2 INTO CURSOR ACCRUALS GROUP BY 1 ORDER BY 1

						*SELECT SQLCURSOR2
						*BROWSE

						.TrackProgress('==================================================================================================================', LOGIT+SENDIT)
						IF lnEmployeesSent = 0 THEN
							.TrackProgress('There were no new or status-changed employees to Export!', LOGIT+SENDIT+NOWAITIT)
						ENDIF

						* create output files even if there were no records to send; in that case we'll just be writing out the header,
						* but at least Kronos import willl always see a file.
						SET DATE YMD
						lcNow = STRTRAN(TTOC(DATETIME()),"/","")
						lcNow = STRTRAN(lcNow," ","_")
						lcNow = STRTRAN(lcNow,":","")
						lcDailyCSVFileRoot = "PEOPLEIMPORT_" + lcNow + ".CSV"
						lcDailyCSVFilename = "F:\UTIL\KRONOS\EDI\DAILYBACKUPS\" + lcDailyCSVFileRoot

						*SET DATE YMD
						lcToday = DTOC(DATE())
						SET DATE AMERICAN

						IF FILE(lcDailyCSVFilename) THEN
							DELETE FILE (lcDailyCSVFilename)
						ENDIF

						.nFileHandle = FCREATE(lcCSVFilename)
						IF .nFileHandle > -1 THEN
							lcHeader = "hourlywage,devicegrp,cpayrule,csupervisr,spvsrbadge,ctimezone,plaeffdate,labracct,shift,badge_num," + ;
								"bdgeff_dt,empeffdate,status,cacrprofil,dob,stdhours,biometric,first,hire_date,last,personnum," + ;
								"license,city,addrtype,country,zip,state,address,phonetype,phone,uaseffdate,uasstatus,chgepwrd,username,password," + ;
								"schedgrp,importdate,importfile"

							=FPUTS(.nFileHandle,lcHeader)

							IF .lOnlyProcesshangesTo9999 THEN
								SELECT * ;
									FROM SQLCURSOR2 ;
									INTO CURSOR SQLCURSOR3 ;
									WHERE LNEW9999 ;
									ORDER BY CSORT DESCENDING
							ELSE
								SELECT * ;
									FROM SQLCURSOR2 ;
									INTO CURSOR SQLCURSOR3 ;
									ORDER BY CSORT DESCENDING ;
									READWRITE									
							ENDIF

	SELECT SQLCURSOR3
	BROWSE

							SELECT SQLCURSOR3
							SCAN FOR LNEW AND VALID
								lcLine = ;
									.CleanUpField(HOURLYWAGE) + "," + ;
									.CleanUpField(DEVICEGRP) + "," + ;
									.CleanUpField(CPAYRULE) + "," + ;
									'"' + ;
									ALLTRIM(CSUPERVISR) + ;
									'"' + ;
									"," + ;
									.CleanUpField(SPVSRBADGE) + "," + ;
									.CleanUpField(CTIMEZONE) + "," + ;
									.CleanUpField(PLAEFFDATE) + "," + ;
									.CleanUpField(LABRACCT) + "," + ;
									.CleanUpField(SHIFT) + "," + ;
									.CleanUpField(BADGE_NUM) + "," + ;
									.CleanUpField(BDGEFF_DT) + "," + ;
									.CleanUpField(EMPEFFDATE) + "," + ;
									.CleanUpField(STATUS) + "," + ;
									.CleanUpField(CACRPROFIL) + "," + ;
									.CleanUpField(DOB) + "," + ;
									.CleanUpField(STDHOURS) + "," + ;
									.CleanUpField(BIOMETRIC) + "," + ;
									.CleanUpField(FIRST) + "," + ;
									.CleanUpField(HIRE_DATE) + "," + ;
									.CleanUpField(LAST) + "," + ;
									.CleanUpField(PERSONNUM) + "," + ;
									.CleanUpField(LICENSE) + "," + ;
									.CleanUpField(CITY) + "," + ;
									.CleanUpField(ADDRTYPE) + "," + ;
									.CleanUpField(COUNTRY) + "," + ;
									.CleanUpField(ZIP) + "," + ;
									.CleanUpField(STATE) + "," + ;
									.CleanUpField(ADDRESS) + "," + ;
									.CleanUpField(PHONETYPE) + "," + ;
									.CleanUpField(PHONE) + "," + ;
									.CleanUpField(UASEFFDATE) + "," + ;
									.CleanUpField(UASSTATUS) + "," + ;
									.CleanUpField(CHGEPWRD) + "," + ;
									.CleanUpField(USERNAME) + "," + ;
									.CleanUpField(PASSWORD) + "," + ;
									.CleanUpField(SCHEDGRP) + "," + ;
									.CleanUpField(lcToday) + "," + ;
									.CleanUpField(lcDailyCSVFileRoot)

								=FPUTS(.nFileHandle,lcLine)

							ENDSCAN
						ELSE
							.TrackProgress('Error opening file for FWRITE(): ' + lcCSVFilename, LOGIT+SENDIT)
						ENDIF
						=FCLOSE(.nFileHandle)


						IF FILE(lcCSVFilename) THEN
							.TrackProgress('Exported ' + TRANSFORM(lnEmployeesSent) + ' employees to: ' + lcCSVFilename, LOGIT+SENDIT)

							IF .lDidInitialPut THEN
								lcAction = '!! ACTION ITEM: you must edit the new employees in Kronos to make sure they have the correct' + CRLF + ;
									'Pay Rule, Supervisor, Accruals Profile, License Type, etc.'
								.TrackProgress(lcAction, LOGIT+SENDIT)
							ENDIF

							* COPY DATE-SPECIFIC BACKUP FILE
							COPY FILE (lcCSVFilename) TO (lcDailyCSVFilename)

							IF FILE(lcDailyCSVFilename) THEN
								.TrackProgress('Made a backup of the Export File to: ' + lcDailyCSVFilename, LOGIT+SENDIT)
							ELSE
								.TrackProgress('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!', LOGIT+SENDIT)
								.TrackProgress('ERROR: There was a problem producing backup Export File: ' + lcDailyCSVFilename, LOGIT+SENDIT)
								.TrackProgress('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!', LOGIT+SENDIT)
							ENDIF

							.TrackProgress('Exported employees (ADP_COMP, File#, Name, Status, Reason for Export):' + CRLF + lcEmployeeList, LOGIT+SENDIT)
							.TrackProgress('ADP EMPLOYEES --> KRONOS process ended normally.', LOGIT+SENDIT+NOWAITIT)
						ELSE
							.TrackProgress('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!', LOGIT+SENDIT)
							.TrackProgress('ERROR: There was a problem producing Master Export File: ' + lcCSVFilename, LOGIT+SENDIT)
							.TrackProgress('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!', LOGIT+SENDIT)
						ENDIF

						.cBodyText = lcNew9999 + CRLF + .cBodyText

						CLOSE DATABASES ALL
					ENDIF  &&  .ExecSQL(lcSQL, 'SQLCURSOR1', RETURN_DATA_MANDATORY)

				ELSE
					* connection error
					.TrackProgress('Unable to connect to ADP Payroll System.', LOGIT+SENDIT)
				ENDIF   &&  .nSQLHandle > 0

			CATCH TO loError

				.TrackProgress('There was an error.',LOGIT+SENDIT)
				.TrackProgress('The Error # is: ' + TRANSFORM(loError.ERRORNO), LOGIT+SENDIT)
				.TrackProgress('The Error Message is: ' + TRANSFORM(loError.MESSAGE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred in Program: ' + TRANSFORM(loError.PROCEDURE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred at line #: ' + TRANSFORM(loError.LINENO), LOGIT+SENDIT)
				lnNumberOfErrors = lnNumberOfErrors + 1
				CLOSE DATA
				CLOSE ALL

			ENDTRY

			CLOSE DATA
			WAIT CLEAR
			***************** INTERNAL email results ******************************
			.TrackProgress('About to send status email.',LOGIT)
			.TrackProgress('==================================================================================================================', SENDIT)
			.TrackProgress('The logfile is: ' + .cLogFile, SENDIT)
			.TrackProgress('ADP EMPLOYEES --> KRONOS process started: ' + .cStartTime, LOGIT+SENDIT)
			.TrackProgress('ADP EMPLOYEES --> KRONOS process finished: ' + TTOC(DATETIME()), LOGIT+SENDIT)

			IF .lSendInternalEmailIsOn THEN
				* try to trap error from not having dartmail dll's registered on user's pc...
				TRY
					DO FORM M:\DEV\FRM\dartmail2 WITH .cSendTo,.cFrom,.cSubject,.cCC,.cAttach,.cBodyText,"A"
					.TrackProgress('Sent status email.',LOGIT)
				CATCH TO loError
					=MESSAGEBOX('There was an error sending the status email. Please contact Mark Bennett.',0+64,"ADP Employees --> Kronos")
					.TrackProgress('There was an error sending the status email.',LOGIT)
					.TrackProgress('The Error # is: ' + TRANSFORM(loError.ERRORNO), LOGIT)
					.TrackProgress('The Error Message is: ' + TRANSFORM(loError.MESSAGE), LOGIT)
					.TrackProgress('The Error occurred in Program: ' + TRANSFORM(loError.PROCEDURE), LOGIT)
					.TrackProgress('The Error occurred at line #: ' + TRANSFORM(loError.LINENO), LOGIT)
					lnNumberOfErrors = lnNumberOfErrors + 1
					CLOSE DATA
				ENDTRY
			ELSE
				.TrackProgress('Did not send status email: THIS.lSendInternalEmailIsOn = FALSE.',LOGIT)
			ENDIF

		ENDWITH
		RETURN
	ENDFUNC && main


	FUNCTION TranslateLicense
		LPARAMETERS tcLicense
		LOCAL lcLicense
		DO CASE
			CASE "MANAGER" $ UPPER(tcLicense)
				lcLicense = "WORKFORCE MANAGER"
			CASE "PROFESSIONAL" $ UPPER(tcLicense)
				lcLicense = "WORKFORCE EMPLOYEE"
			OTHERWISE
				lcLicense = "WORKFORCE TIMEKEEPER"
		ENDCASE
		RETURN lcLicense
	ENDFUNC


	FUNCTION CleanUpField
		* prepare field for inclusion in the string to be written with FPUTS().
		* especially critical to remove any commas, since that creates bogus column in CSV output file.
		LPARAMETERS tuValue
		LOCAL lcRetVal
		lcRetVal = STRTRAN(ALLTRIM(TRANSFORM(tuValue)),",","")
		RETURN lcRetVal
	ENDFUNC


	FUNCTION ExecSQL
		LPARAMETERS tcSQL, tcCursorName, tlNoDataReturnedIsOkay
		LOCAL llRetval, lnResult
		WITH THIS
			* close target cursor if it's open
			IF USED(tcCursorName)
				USE IN (tcCursorName)
			ENDIF
			WAIT WINDOW "Fetching remote data; please wait..." NOWAIT
			lnResult = SQLEXEC(.nSQLHandle, tcSQL, tcCursorName)
			llRetval = ( lnResult > 0 )
			IF llRetval THEN
				* see if any data came back
				IF NOT tlNoDataReturnedIsOkay THEN
					IF (NOT USED(tcCursorName)) OR EOF(tcCursorName) THEN
						llRetval = .F.
						.TrackProgress('No data was returned by this query: ' +CRLF + tcSQL, LOGIT+SENDIT)
					ENDIF
				ENDIF
			ELSE
				.TrackProgress("Result Code [" + TRANSFORM(lnResult) + "] returned by this query: " + tcSQL, LOGIT+SENDIT)
			ENDIF
			WAIT CLEAR
			RETURN llRetval
		ENDWITH
	ENDFUNC


	PROCEDURE TrackProgress
		* do any combination of Wait Window, writing to logfile, and adding to body of email,
		* based on nFlags parameter.
		LPARAMETERS tcExpression, tnFlags
		WITH THIS
			IF BITAND(tnFlags,LOGIT) = LOGIT THEN
				IF .lLoggingIsOn THEN
					?
					? .cProcessName + "  " + TTOC(DATETIME()) + ": " + tcExpression
				ENDIF
			ENDIF
			IF .lWaitWindowIsOn THEN
				IF BITAND(tnFlags,NOWAITIT) = NOWAITIT THEN
					WAIT WINDOW tcExpression NOWAIT
				ENDIF
				IF BITAND(tnFlags,WAITIT) = WAITIT THEN
					WAIT WINDOW tcExpression TIMEOUT .nWaitWindowTimeout
				ENDIF
			ENDIF
			IF BITAND(tnFlags,SENDIT) = SENDIT THEN
				IF .lSendInternalEmailIsOn THEN
					.cBodyText = .cBodyText + tcExpression + CRLF + CRLF
				ENDIF
			ENDIF
		ENDWITH
	ENDPROC  &&  TrackProgress

ENDDEFINE
