* WIP report for IDS

utilsetup("IDSREPORT")

if holiday(date())
	return
endif

use f:\wh2\whdata\outship in 0
use f:\wh2\whdata\swcdet in 0

* 

select ;
	ship_ref, ;
	consignee, ;
	ship_via, ;
	cnee_ref, ;
	000 as pallets, ;
	ctnqty, ;
	weight, ;
	cuft, ;
	padr(iif(!pulled,"NOT PULLED",iif(!emptynul(del_date),"DELIVERED",iif(!emptynul(truckloaddt),"LOADED",iif(!emptynul(staged),"STAGED",iif(!emptynul(labeled),"LABELED",iif(!emptynul(picked),"PICKED","PULLED")))))),10) as status, ;
	iif(!pulled,wo_date,iif(!emptynul(del_date),del_date,iif(!emptynul(truckloaddt),truckloaddt,iif(!emptynul(staged),staged,iif(!emptynul(labeled),labeled,iif(!emptynul(picked),picked,pulleddt)))))) as statusdt, ;
	swcnum, ;
	swcover ;
	from outship ;
	where accountid=6153 ;
	and (wo_date>date()-10 or emptynul(del_date)) ;
	and !notonwip ;
	and ctnqty>0 ;
	order by 1 ;
	into cursor xrpt readwrite
	
scan 
	if seek(swcnum+swcover,"swcdet","swcnumover")
		replace pallets with swcdet.numflags in xrpt
	endif
endscan

xheader="INTERNATIONAL DESIGN SOURCE WORK IN PROGRESS"
rpt2pdf("idswip","h:\fox\idswip.pdf")

tsendto="etosolini@sumlogint.com, Raico.chang@tollgroup.com"
tcc="" && "jkillen@fmiint.com, cmalcolm@fmiint.com"
tattach="h:\fox\idswip.pdf"
tFrom="TGF EDI System Operations <transload-ops@fmiint.com>"
tmessage=""
tSubject="TGF WIP"

Do Form dartmail2 With tsendto,tFrom,tSubject,tcc,tattach,tmessage,"A"

use in outship
use in swcdet

*

schedupdate()

_screen.Caption=gscreencaption
on error
