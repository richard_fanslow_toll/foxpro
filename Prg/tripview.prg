* creates vfxtrips view - MUST BE RUN IN VFP 8.0 !!

close databases all
set enginebehavior 70

*open data m:\dev\fxdata\fx
open data f:\express\fxdata\fx

CREATE SQL VIEW "VDISPATCH" AS SELECT ;
	fxtrips.fxwologid, ;
	fxtrips.fxtripsid, ;
	fxtrips.wo_num, ;
	fxtrips.wo_date, ;
	fxtrips.accountid, ;
	fxtrips.office, ;
	fxtrips.ownersid, ;
	fxtrips.deadhead, ;
	fxtrips.leg, ;
	fxtrips.quantity, ;
	fxtrips.qty_type, ;
	fxtrips.weight, ;
	fxtrips.traveled, ;
	fxtrips.fromloc, ;
	fxtrips.showfromloc, ;
	fxtrips.puloc, ;
	fxtrips.showpuloc, ;
	fxtrips.pustate, ;
	fxtrips.puname, ;
	fxtrips.puname2, ;
	fxtrips.puaddress, ;
	fxtrips.pucsz, ;
	fxtrips.puphone, ;
	fxtrips.pucontact, ;
	fxtrips.pu, ;
	fxtrips.origpu, ;
	fxtrips.puswitch, ;
	fxtrips.puappt, ;
	fxtrips.pickedup, ;
	fxtrips.picktime, ;
	fxtrips.puatime1, ;
	fxtrips.puatime, ;
	fxtrips.toloc, ;
	fxtrips.showtoloc, ;
	fxtrips.delloc, ;
	fxtrips.showdelloc, ;
	fxtrips.delstate, ;
	fxtrips.delname, ;
	fxtrips.delname2, ;
	fxtrips.deladdress, ;
	fxtrips.delcsz, ;
	fxtrips.delphone, ;
	fxtrips.delcontact, ;
	fxtrips.del, ;
	fxtrips.origdel, ;
	fxtrips.delswitch, ;
	fxtrips.delappt, ;
	fxtrips.delatime1, ;
	fxtrips.delatime, ;
	fxtrips.delivered, ;
	fxtrips.delvtime, ;
	fxtrips.dispatched, ;
	fxtrips.dis_time, ;
	fxtrips.dis_driver, ;
	fxtrips.dis_truck, ;
	fxtrips.dvr1ss, ;
	fxtrips.dvr2ss, ;
	fxtrips.dvrteam, ;
	fxtrips.dvrcompany, ;
	fxtrips.remarks, ;
	fxtrips.mfstunsent, ;
	fxtrips.triptrailer, ;
	fxtrips.posdt, ;
	fxtrips.postm, ;
	fxtrips.poscs, ;
	fxtrips.miles2dest, ;
	fxtrips.truckhr, ;
	fxtrips.truckmin, ;
	fxtrips.truckrev, ;
	fxtrips.delarrivedt, ;
	fxtrips.activeleg, ;
	fxtrips.latetime, ;
	fxtrips.tripltl, ;
	fxtrips.eastwest, ;
	fxtrips.hazmat, ;
	fxtrips.dychar, ;
	fxtrips.dylog, ;
	fxtrips.akmiles, ;
	fxtrips.almiles, ;
	fxtrips.armiles, ;
	fxtrips.azmiles, ;
	fxtrips.bcmiles, ;
	fxtrips.bjmiles, ;
	fxtrips.camiles, ;
	fxtrips.comiles, ;
	fxtrips.ctmiles, ;
	fxtrips.dcmiles, ;
	fxtrips.demiles, ;
	fxtrips.flmiles, ;
	fxtrips.gamiles, ;
	fxtrips.iamiles, ;
	fxtrips.idmiles, ;
	fxtrips.ilmiles, ;
	fxtrips.inmiles, ;
	fxtrips.ksmiles, ;
	fxtrips.kymiles, ;
	fxtrips.lamiles, ;
	fxtrips.mamiles, ;
	fxtrips.mdmiles, ;
	fxtrips.memiles, ;
	fxtrips.mimiles, ;
	fxtrips.mnmiles, ;
	fxtrips.momiles, ;
	fxtrips.msmiles, ;
	fxtrips.mtmiles, ;
	fxtrips.mxmiles, ;
	fxtrips.ncmiles, ;
	fxtrips.ndmiles, ;
	fxtrips.nemiles, ;
	fxtrips.nhmiles, ;
	fxtrips.njmiles, ;
	fxtrips.nmmiles, ;
	fxtrips.nvmiles, ;
	fxtrips.nymiles, ;
	fxtrips.ohmiles, ;
	fxtrips.okmiles, ;
	fxtrips.onmiles, ;
	fxtrips.ormiles, ;
	fxtrips.pamiles, ;
	fxtrips.pqmiles, ;
	fxtrips.qcmiles, ;
	fxtrips.rimiles, ;
	fxtrips.scmiles, ;
	fxtrips.sdmiles, ;
	fxtrips.tnmiles, ;
	fxtrips.txmiles, ;
	fxtrips.utmiles, ;
	fxtrips.vamiles, ;
	fxtrips.vtmiles, ;
	fxtrips.wamiles, ;
	fxtrips.wimiles, ;
	fxtrips.wvmiles, ;
	fxtrips.wymiles, ;
	fxtrips.ytmiles, ;
	fxtrips.homedt, ;
	fxtrips.homeloc, ;
	iif(emptynul(dispatched),"A ",iif(emptynul(pickedup),"PU","DL")) as action, ;
	fxtrips.triptrailer as trailer, ;
	acctname, ;
	triphot as hot, ;
	sanfran, ;
	"  " as origstate, ;
	.f. as diffdate, ;
	.f. as updatemark, ;
	.f. as mark ;
	from fx!fxtrips ;
	where delivered={} ;
	and !deadhead ;
	and &xfilter

*	from fx!fxtrips, fx!fxwolog ;
*	where fxtrips.fxwologid=fxwolog.fxwologid ;
*	fxwolog.trailer, ;
*	fxwolog.acctname, ;
*	fxwolog.hot, ;
*	fxwolog.sanfran, ;

DBSetProp('VDISPATCH', 'View', 'UpdateType', 1)
DBSetProp('VDISPATCH', 'View', 'WhereType', 1)
DBSetProp('VDISPATCH', 'View', 'FetchMemo', .T.)
DBSetProp('VDISPATCH', 'View', 'SendUpdates', .f.)
DBSetProp('VDISPATCH', 'View', 'UseMemoSize', 255)
DBSetProp('VDISPATCH', 'View', 'FetchSize', 100)
DBSetProp('VDISPATCH', 'View', 'MaxRecords', -1)
DBSetProp('VDISPATCH', 'View', 'Tables', 'fx!fxtrips')
DBSetProp('VDISPATCH', 'View', 'Prepared', .F.)
DBSetProp('VDISPATCH', 'View', 'CompareMemo', .T.)
DBSetProp('VDISPATCH', 'View', 'FetchAsNeeded', .F.)
DBSetProp('VDISPATCH', 'View', 'FetchSize', 100)
DBSetProp('VDISPATCH', 'View', 'Comment', "")
DBSetProp('VDISPATCH', 'View', 'BatchUpdateCount', 1)
DBSetProp('VDISPATCH', 'View', 'ShareConnection', .F.)

DBSetProp('VDISPATCH.fxtripsid', 'Field', 'KeyField', .T.)
DBSetProp('VDISPATCH.fxtripsid', 'Field', 'Updatable', .T.)

dbsetprop('VDISPATCH.leg', 'Field', 'RuleExpression', "setmark()")
dbsetprop('VDISPATCH.quantity', 'Field', 'RuleExpression', "setmark()")
dbsetprop('VDISPATCH.qty_type', 'Field', 'RuleExpression', "setmark()")
dbsetprop('VDISPATCH.weight', 'Field', 'RuleExpression', "setmark()")
dbsetprop('VDISPATCH.traveled', 'Field', 'RuleExpression', "setmark()")
dbsetprop('VDISPATCH.fromloc', 'Field', 'RuleExpression', "setmark()")
dbsetprop('VDISPATCH.showfromloc', 'Field', 'RuleExpression', "setmark()")
dbsetprop('VDISPATCH.puloc', 'Field', 'RuleExpression', "setmark()")
dbsetprop('VDISPATCH.showpuloc', 'Field', 'RuleExpression', "setmark()")
dbsetprop('VDISPATCH.pustate', 'Field', 'RuleExpression', "setmark()")
dbsetprop('VDISPATCH.puname', 'Field', 'RuleExpression', "setmark()")
dbsetprop('VDISPATCH.puname2', 'Field', 'RuleExpression', "setmark()")
dbsetprop('VDISPATCH.puaddress', 'Field', 'RuleExpression', "setmark()")
dbsetprop('VDISPATCH.pucsz', 'Field', 'RuleExpression', "setmark()")
dbsetprop('VDISPATCH.puphone', 'Field', 'RuleExpression', "setmark()")
dbsetprop('VDISPATCH.pucontact', 'Field', 'RuleExpression', "setmark()")
dbsetprop('VDISPATCH.pu', 'Field', 'RuleExpression', "setmark()")
dbsetprop('VDISPATCH.origpu', 'Field', 'RuleExpression', "setmark()")
dbsetprop('VDISPATCH.puswitch', 'Field', 'RuleExpression', "setmark()")
dbsetprop('VDISPATCH.puappt', 'Field', 'RuleExpression', "setmark()")
dbsetprop('VDISPATCH.puatime1', 'Field', 'RuleExpression', "setmark()")
dbsetprop('VDISPATCH.puatime', 'Field', 'RuleExpression', "setmark()")
dbsetprop('VDISPATCH.pickedup', 'Field', 'RuleExpression', "setmark()")
dbsetprop('VDISPATCH.picktime', 'Field', 'RuleExpression', "setmark()")
dbsetprop('VDISPATCH.toloc', 'Field', 'RuleExpression', "setmark()")
dbsetprop('VDISPATCH.showtoloc', 'Field', 'RuleExpression', "setmark()")
dbsetprop('VDISPATCH.delloc', 'Field', 'RuleExpression', "setmark()")
dbsetprop('VDISPATCH.showdelloc', 'Field', 'RuleExpression', "setmark()")
dbsetprop('VDISPATCH.delstate', 'Field', 'RuleExpression', "setmark()")
dbsetprop('VDISPATCH.delname', 'Field', 'RuleExpression', "setmark()")
dbsetprop('VDISPATCH.delname2', 'Field', 'RuleExpression', "setmark()")
dbsetprop('VDISPATCH.deladdress', 'Field', 'RuleExpression', "setmark()")
dbsetprop('VDISPATCH.delcsz', 'Field', 'RuleExpression', "setmark()")
dbsetprop('VDISPATCH.delphone', 'Field', 'RuleExpression', "setmark()")
dbsetprop('VDISPATCH.delcontact', 'Field', 'RuleExpression', "setmark()")
dbsetprop('VDISPATCH.del', 'Field', 'RuleExpression', "setmark()")
dbsetprop('VDISPATCH.origdel', 'Field', 'RuleExpression', "setmark()")
dbsetprop('VDISPATCH.delswitch', 'Field', 'RuleExpression', "setmark()")
dbsetprop('VDISPATCH.delappt', 'Field', 'RuleExpression', "setmark()")
dbsetprop('VDISPATCH.delatime1', 'Field', 'RuleExpression', "setmark()")
dbsetprop('VDISPATCH.delatime', 'Field', 'RuleExpression', "setmark()")
dbsetprop('VDISPATCH.delivered', 'Field', 'RuleExpression', "setmark()")
dbsetprop('VDISPATCH.delvtime', 'Field', 'RuleExpression', "setmark()")
dbsetprop('VDISPATCH.dispatched', 'Field', 'RuleExpression', "setmark()")
dbsetprop('VDISPATCH.dis_time', 'Field', 'RuleExpression', "setmark()")
dbsetprop('VDISPATCH.dis_driver', 'Field', 'RuleExpression', "setmark()")
dbsetprop('VDISPATCH.dis_truck', 'Field', 'RuleExpression', "setmark()")
dbsetprop('VDISPATCH.dvr1ss', 'Field', 'RuleExpression', "setmark()")
dbsetprop('VDISPATCH.dvr2ss', 'Field', 'RuleExpression', "setmark()")
dbsetprop('VDISPATCH.dvrteam', 'Field', 'RuleExpression', "setmark()")
dbsetprop('VDISPATCH.dvrcompany', 'Field', 'RuleExpression', "setmark()")
dbsetprop('VDISPATCH.remarks', 'Field', 'RuleExpression', "setmark()")
dbsetprop('VDISPATCH.mfstunsent', 'Field', 'RuleExpression', "setmark()")
dbsetprop('VDISPATCH.triptrailer', 'Field', 'RuleExpression', "setmark()")
dbsetprop('VDISPATCH.truckhr', 'Field', 'RuleExpression', "setmark()")
dbsetprop('VDISPATCH.truckmin', 'Field', 'RuleExpression', "setmark()")
dbsetprop('VDISPATCH.truckrev', 'Field', 'RuleExpression', "setmark()")
dbsetprop('VDISPATCH.activeleg', 'Field', 'RuleExpression', "setmark()")
dbsetprop('VDISPATCH.eastwest', 'Field', 'RuleExpression', "setmark()")
dbsetprop('VDISPATCH.tripltl', 'Field', 'RuleExpression', "setmark()")
dbsetprop('VDISPATCH.dychar', 'Field', 'RuleExpression', "setmark()")

close databases all
