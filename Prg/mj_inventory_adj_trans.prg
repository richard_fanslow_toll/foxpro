utilsetup("MJ_INVENTORY_ADJ_TRANS")
set exclusive off
set deleted on
set talk off
set safety off   &&Allows for overwrite of existing files

close databases all
**************************************START NJ

useca('adj','wh',,,"select * from adj where adjdt>={"+dtoc(date()-7)+"} and " + ;
		"inlist(accountid,6303,6304,6305,6320,6321,6322,6323,6324,6325,6543,6364) and offset=0 and flag=0")
SELECT adjid FROM adj INTO CURSOR markflag readwrite



	select accountid, adjdt,alltrim(style)+'-'+alltrim(color) as style,id,totqty,whseloc,comment,adjtype,adjid;
		from adj where comment !='PHYSICAL ADJUSTMENT'  and comment !='WAREHOUSE LOCATION'  and comment !='RETURN PUTAWAY' and adjtype !='LOCATION CHANGE'  ;
		AND office='N' AND MOD='J' into cursor temp45 readwrite
SET STEP ON 

select * from temp45 where totqty!=0 into cursor temp46 readwrite

replace all accountid with 10100 for accountid=6303 in temp46
replace all accountid with 10180 for accountid=6325 in temp46
replace all accountid with 10190 for accountid=6543 in temp46



**************************************START MIRA LOMA
	select accountid, adjdt,alltrim(style)+'-'+alltrim(color) as style,id,totqty,whseloc,comment,adjtype,adjid;
		from adj where   comment !='PHYSICAL ADJUSTMENT'  and comment !='WAREHOUSE LOCATION'  and comment !='RETURN PUTAWAY' and adjtype !='LOCATION CHANGE'  ;
		 AND office='L' AND MOD='L'  into cursor temp47 readwrite
		

	select * from temp47 where totqty!=0 into cursor temp48 readwrite

	replace all accountid with 11100 for accountid=6303 in temp48
	replace all accountid with 11180 for accountid=6325 in temp48
	replace all accountid with 11190 for accountid=6543 in temp48


	select * from temp46 union all select * from temp48 into cursor temp49 readwrite

	select temp49

	set step on
	if file ("S:\MarcJacobsData\Reports\InventorySyncFiles\InvenAdj\inventory_adj_trans.xls")
		delete file  s:\marcjacobsdata\reports\inventorysyncfiles\invenadj\inventory_adj_trans.xls
	else
	endif
	export to "S:\MarcJacobsData\Reports\InventorySyncFiles\InvenAdj\archive\Inventory_adj_trans_" +ttoc(datetime(),1) type xls
	export to s:\marcjacobsdata\reports\inventorysyncfiles\invenadj\inventory_adj_trans.xls type xls

	set step on
	if reccount() > 0
*		tsendto = "e.phillips@marcjacobs.com,c.cianciabella@marcjacobs.com,E.BARNES@marcjacobs.com,o.batista@marcjacobs.com,brian.mcglone@tollgroup.com,M.deLaCruz@marcjacobs.com,E.SKRZYNIARZ@marcjacobs.com"
**     tsendto =  "tmarg@fmiint.com"
		tsendto = "TollInventoryAdjustment@marcjacobs.com"
		tattach = "S:\MarcJacobsData\Reports\InventorySyncFiles\InvenAdj\Inventory_adj_trans.xls"
		tcc =""
		tfrom ="Toll EDI System Operations <fmi-transload-ops@fmiint.com>"
		tmessage = "Inventory Adjustment Transaction file created for  : "+dtoc (date()-1, 0)
		tsubject = "Inventory Adjustment Transaction file created successfully"
		do form m:\dev\frm\dartmail2 with tsendto,tfrom,tsubject, tcc,tattach,tmessage  ,"A"

	else
*		tsendto =  "e.phillips@marcjacobs.com,l.lopez@marcjacobs.com,c.cianciabella@marcjacobs.com,E.BARNES@marcjacobs.com,o.batista@marcjacobs.com,brian.mcglone@tollgroup.com,M.deLaCruz@marcjacobs.com,E.SKRZYNIARZ@marcjacobs.com"
**	tsendto =  "tmarg@fmiint.com"
		tsendto = "TollInventoryAdjustment@marcjacobs.com"
		tattach = ""
		tcc =""
		tfrom ="Toll EDI System Operations <fmi-transload-ops@fmiint.com>"
		tmessage = "No Inventory Adjustments exist  for  : "+dtoc (date()-1, 0)
		tsubject = "No Inventory Adjustments exist"
		do form m:\dev\frm\dartmail2 with tsendto,tfrom,tsubject, tcc,tattach,tmessage  ,"A"

	endif
Select markflag                                      
Scan                                            
SELECT adj                              
 LOCATE FOR adjid=adj.adjid    
 IF FOUND("adj")                            
 replace flag WITH .t. FOR adjid=adj.adjid    IN adj
 Else                                           
 EndIf                                          
ENDSCAN
tu('adj')

********************************START 365 VERSION
**************************************START NJ
close databases all
upcmastsql (6303)
goffice="J"
	xsqlexec("select * from adj where adjdt>={"+dtoc(date()-365)+"} and " + ;
		"inlist(accountid,6303,6304,6305,6320,6321,6322,6323,6324,6325,6543,6364) and offset=0",,,"wh")

	select accountid, adjdt,alltrim(style)+'-'+alltrim(color) as style,id,totqty,whseloc,comment,adjtype,adjid,style, color;
		from adj where comment !='PHYSICAL ADJUSTMENT'  and comment !='WAREHOUSE LOCATION'  and comment !='RETURN PUTAWAY' and adjtype !='LOCATION CHANGE' ;
		AND office='N' AND MOD='J'   into cursor temp45 readwrite


select * from temp45 where totqty!=0 into cursor temp46 readwrite

replace all accountid with 10100 for accountid=6303 in temp46
replace all accountid with 10180 for accountid=6325 in temp46
replace all accountid with 10190 for accountid=6543 in temp46



**************************************START MIRA LOMA
	select accountid, adjdt,alltrim(style)+'-'+alltrim(color) as style,id,totqty,whseloc,comment,adjtype,adjid,style, color ;
		from adj where comment !='PHYSICAL ADJUSTMENT'  and comment !='WAREHOUSE LOCATION'  and comment !='RETURN PUTAWAY' and adjtype !='LOCATION CHANGE'  ;
		 AND office='L' AND MOD='L' into cursor temp47 readwrite

	select * from temp47 where totqty!=0 into cursor temp48 readwrite

	replace all accountid with 11100 for accountid=6303 in temp48
	replace all accountid with 11180 for accountid=6325 in temp48
	replace all accountid with 11190 for accountid=6543 in temp48


	select * from temp46 union all select * from temp48 into cursor temp49 ORDER BY adjdt,accountid readwrite

	select a.*, upc,SPACE(10) as season, info FROM temp49 a LEFT JOIN upcmast b ON a.style_b=b.style AND a.color=b.color AND a.id=b.id INTO CURSOR c1 READWRITE 
replace all SEASON with getmemodata("info","SEASON")
SELECT accountid,adjdt,style_a as style,id,totqty,whseloc,comment,adjtype,adjid,upc,season FROM c1 INTO CURSOR c2 READWRITE
COPY TO "S:\MarcJacobsData\Reports\Cycle_count\adjustments" type csv
	set step on
*!*		if file ("S:\MarcJacobsData\Reports\InventorySyncFiles\InvenAdj\inventory_adj_trans.xls")
*!*			delete file  s:\marcjacobsdata\reports\inventorysyncfiles\invenadj\inventory_adj_trans.xls
*!*		else
*!*		endif
*!*		export to "S:\MarcJacobsData\Reports\InventorySyncFiles\InvenAdj\archive\Inventory_adj_trans_" +ttoc(datetime(),1) type xls
*!*		export to s:\marcjacobsdata\reports\inventorysyncfiles\invenadj\inventory_adj_trans.xls type xls


*!*	useca ("WHSELOC",'wh',,,'m-j')
*!*	use f:\whj\whdata\invenLOC IN 0
*!*	SELECT whseloc FROM whseloc WHERE whseloc !='PL' ORDER BY whseloc INTO CURSOR t1 READWRITE
*!*	SELECT WHSELOC ,SUM(LOCQTY) AS LOCQTY FROM INVENLOC WHERE LOCQTY!=0 GROUP BY WHSELOC INTO CURSOR V1 READWRITE
*!*	SELECT A.WHSELOC,LOCQTY , SPACE(5) AS INVENTORY FROM T1 A LEFT JOIN V1 B ON A.WHSELOC=B.WHSELOC INTO CURSOR CI1 READWRITE
*!*	REPLACE inventory WITH 'YES' FOR !ISNULL(LOCQTY)
*!*	REPLACE inventory WITH 'NO' FOR ISNULL(LOCQTY)
*!*	SELECT WHSELOC, INVENTORY FROM CI1 INTO CURSOR CI2 READWRITE

*!*	COPY TO "S:\MarcJacobsData\Reports\Cycle_count\whseloc" type csv



		schedupdate()
		_screen.caption=gscreencaption
		on error

****************************
procedure segmentget
****************************

parameter thisarray,lckey,nlength

for i = 1 to nlength
	if i > nlength
		exit
	endif
	lnend= at("*",thisarray[i])
	if lnend > 0
		lcthiskey =trim(substr(thisarray[i],1,lnend-1))
		if occurs(lckey,lcthiskey)>0
			return substr(thisarray[i],lnend+1)
			i = 1
		endif
	endif
endfor

return ""



schedupdate()
_screen.caption=gscreencaption
on error
