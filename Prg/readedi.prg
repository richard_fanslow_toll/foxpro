Parameters xfile

runack("READEDI")

Set Safety Off

xfiletype = Vartype(xfile)

If Inlist(Vartype(xfile),"U","L")
  xfile = Getfile('*')
Endif

Do Case
Case Empty(xfile)
  Wait Window "No file selected" Nowait
  Return .F.
Case !File(xfile)
  Wait Window (xfile+" does not exist")
  Return .F.
Endcase

Cd Justpath(xfile)

lcOutstr=Filetostr(xfile)

If Substr(lcOutstr,1,3) != "ISA"
  Wait Window At 10,10 "Not an EDI File !!.........."
  Return
Endif
delimchar = Substr(lcOutstr,4,1)
eolchar = Substr(lcOutstr,106,1)
If delimchar != "*"
  lcOutstr=Strtran(lcOutstr,delimchar,"*")
Endif

Do Case
Case eolchar = "^"
  lcOutstr=Strtran(lcOutstr,"^",Chr(13))
Case eolchar = Chr(28)
  lcOutstr=Strtran(lcOutstr,Chr(28),Chr(13))
Case eolchar = "~"
  lcOutstr=Strtran(lcOutstr,"~",Chr(13))
Case eolchar = "'"
  lcOutstr=Strtran(lcOutstr,"'",Chr(13))
Case eolchar = Chr(0x27)
  lcOutstr=Strtran(lcOutstr,Chr(0x27)," ")
Case eolchar = Chr(0x7)
  lcOutstr=Strtran(lcOutstr,Chr(0x07),Chr(13))
Case eolchar = Chr(0x85)
  lcOutstr=Strtran(lcOutstr,Chr(0x85),Chr(13))
Case eolchar = "`"
  lcOutstr=Strtran(lcOutstr,Chr(13),"")
  lcOutstr=Strtran(lcOutstr,Chr(10),"")
Otherwise
  lcOutstr=Strtran(lcOutstr,eolchar,Chr(13))
Endcase

llWin7=.F.

If !File("C:\Program Files\TextPad 4\TextPad.exe")
  If !File("C:\Program Files (x86)\TextPad 4\TextPad.exe"  )
    Messagebox([TextPad not installed as "C:\Program Files (x86)\TextPad 4\TextPad.exe"],0,"ReadEDI Editor")
    Return
  Else
    llWin7=.T.
  Endif
else
Endif

If llWin7
  If !Directory("h:\fox\")
    Strtofile(lcOutstr,"c:\tempfox\tempedi.txt")
    !/N C:\Program Files (x86)\TextPad 4\TextPad.exe "c:\tempfox\tempedi.txt"
  Else
    Strtofile(lcOutstr,"h:\fox\tempedi.txt")
    !/N C:\Program Files (x86)\TextPad 4\TextPad.exe "h:\fox\tempedi.txt"
  Endif
Else
  If !Directory("h:\fox\")
    Strtofile(lcOutstr,"c:\tempfox\tempedi.txt")
    !/N "C:\Program Files\TextPad 4\TextPad.exe" "c:\tempfox\tempedi.txt"
  Else
    Strtofile(lcOutstr,"h:\fox\tempedi.txt")
    !/N "C:\Program Files\TextPad 4\TextPad.exe" "h:\fox\tempedi.txt"
  Endif
Endif

*!*	If File("C:\Program Files\TextPad 4\TextPad.exe")
*!*	  !/N "C:\Program Files\TextPad 4\TextPad.exe" "h:\fox\tempedi.txt"
*!*	Else
*!*	  !/N "d:\Program Files\TextPad 4\TextPad.exe" "h:\fox\tempedi.txt"
*!*	endif

