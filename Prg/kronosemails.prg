* Email Kronos login info to everyone

#DEFINE CRLF CHR(13) + CHR(10)

LOCAL lcSendTo, lcFrom, lcSubject, lcCC, lcAttach, lcBodyText, llTimeStamper, lMode
LOCAL lnFileNum, lcUsername, lcPassword, lcFirstname, lcUser, llTestMode, llSpecialMode, llPopPasswords

llTestMode = .T.

llSpecialMode = llTestMode
llPopPasswords = llTestMode

lcFrom = "Mark.Bennett@tollgroup.com"
lcSubject = "Kronos Attendance"
lcCC = "mbennett@fmiint.com, lucille.waldrip@tollgroup.com, marie.freiberger@tollgroup.com"
lcAttach = ""

CLOSE DATABASES 
SET CENTURY ON
SET DATE AMERICAN
SET DECIMALS TO 3
SET HOURS TO 24
SET ANSI ON
SET TALK OFF
SET DELETED ON
SET CONSOLE OFF
SET EXCLUSIVE OFF
SET SAFETY OFF
SET EXACT OFF
SET STATUS BAR ON
SET SYSMENU OFF
SET ENGINEBEHAVIOR 70

USE F:\UTIL\Kronos\EMAILS\EMAILS ALIAS EMAILS IN 0 SHARED

*!*	IF llSpecialMode THEN
*!*		* send a special email which might be on any topic

*!*		SELECT EMAILS
*!*		SCAN FOR LSPECIAL

*!*		lcSendTo = ALLTRIM(EMAILS.CEMAIL)
*!*		IF EMPTY(lcSendTo) THEN
*!*			lcSendTo = "Mark Bennett <mbennett@fmiint.com>"
*!*		ENDIF
*!*		
*!*		lcFirstname = PROPER(ALLTRIM(EMAILS.CFIRST))
*!*		SET TEXTMERGE ON
*!*		TEXT TO lcBodyText
*!*	<<lcFirstname>>,

*!*	Please review your time cards in Kronos and make sure you have entered all Vacation and Sick time. 

*!*	IF you are having technical problems, let me know and we will make special arrangements for entering the data.

*!*	Thanks

*!*	Mark Bennett
*!*	Kronos System Administrator
*!*	732-750-9000 x 218	
*!*	MBennett@fmiint.com	
*!*		ENDTEXT
*!*		SET TEXTMERGE OFF
*!*			
*!*		IF llTestMode THEN
*!*			lcBodyText = "(TEST MODE: Email would go to:  " + lcSendTo + " )" + CRLF + CRLF + lcBodyText
*!*			lcSendTo = "Mark Bennett <mbennett@fmiint.com>"
*!*		ENDIF
*!*		
*!*		DO FORM dartmail2 WITH lcSendTo,lcFrom,lcSubject,lcCC,lcAttach,lcBodyText,"A"
*!*	ENDSCAN

*!*	ELSE
*!*		* send login info email

SELECT EMAILS
SCAN FOR LSETUP AND (NOT LSENT)

	lcSendTo = ALLTRIM(EMAILS.CEMAIL)
	IF EMPTY(lcSendTo) THEN
		lcSendTo = "mbennett@fmiint.com"
	ENDIF
	
	lcFirstname = PROPER(ALLTRIM(EMAILS.CFIRST))
	lcUser = UPPER(ALLTRIM(EMAILS.CUSER))
	lnFileNum = EMAILS.NFILENUM
	lcUsername = lcUser + TRANSFORM(lnFileNum)
	lcPassword = lcUser + TRANSFORM(10000 - lnFileNum)
	
	llTimeStamper = EMAILS.LTIMESTAMP

	IF llTimeStamper THEN
		lcAttach = "F:\UTIL\KRONOS\KRONOSWTK\TIMESTAMP AND TIMECARD EMPLOYEES\TIMESTAMP INSTRUCTIONS.doc"
		SET TEXTMERGE ON
		TEXT TO lcBodyText
<<lcFirstname>>,

Here is some information to help you use our Kronos timekeeping system to record your in/out times.

Your Kronos username is: <<lcUsername>>

Your Kronos password is: <<lcPassword>>

They should be entered in all caps, no spaces. Do not share them with anyone.

You will enter your username and password at http://payroll2/wfc/logon in Internet Explorer. 

NOTE: if you encounter Jave errors using the URL above, use this Java-less login instead:  

http://payroll2/wfc/applications/wtk/html/ess/logon.jsp

I have attached a word document showing how to do the timestamping; if you cannot open this document,
let me know and I can have Human Resources send you a printed copy.

Contact me if you have questions on any of this.

Thanks

Mark Bennett
Kronos System Administrator
732-750-9000 x 218
Mark.Bennett@tollgroup.com	
MBennett@fmiint.com	
ENDTEXT
		SET TEXTMERGE OFF
	ELSE
		* send instructions to typical self-service Salaried employee -- Hourly View.
		lcAttach = "F:\UTIL\KRONOS\KRONOSWTK\Training Documents\My Timecard.doc"
		SET TEXTMERGE ON
		TEXT TO lcBodyText
<<lcFirstname>>,

We are now implementing the Toll policy of having salaried employees document their attendance on a daily basis,
using the Kronos Timekeeper Software. Please follow the instructions presented below and begin tracking your
attendance as soon as possible.

Your Kronos username is: <<lcUsername>>

Your Kronos password is: <<lcPassword>>

They should be entered in all caps, no spaces. Do not share them with anyone. 



You will enter your username and password at http://payroll2/wfc/logon in Internet Explorer.

Notes:
1. Do this login when you are in the office; it will not work from home.
2. The first time you visit this URL you may be prompted to do a Java install of the Kronos software.
If so, you can do a "typical" install and choose the default prompts throughout. If you need help, contact 
your local IT staff.

ALSO NOTE: if you encounter Java errors using the URL above, use this Java-less login instead:  

http://payroll2/wfc/applications/wtk/html/ess/logon.jsp


I have attached a word document showing how to log in and use Kronos to document your work attendance;
if you cannot open this document, let me know and I can have Human Resources send you a printed copy.

Contact me if you have questions on any of this.

Thanks

Mark Bennett
Kronos System Administrator
732-750-9000 x 218	
Mark.Bennett@tollgroup.com	
MBennett@fmiint.com
		ENDTEXT
		SET TEXTMERGE OFF
	ENDIF	&& llTimeStamper

	
	IF llTestMode THEN
		lcBodyText = "(TEST MODE: Email would go to:  " + lcSendTo + "    cc: " + lcCC + ")" + CRLF + CRLF + lcBodyText
		lcSendTo = "mbennett@fmiint.com"
		lcCC = ""
	ENDIF
	
	DO FORM dartmail2 WITH lcSendTo,lcFrom,lcSubject,lcCC,lcAttach,lcBodyText,"A"
	
	REPLACE EMAILS.LSENT WITH .T.
	IF llPopPasswords THEN
		REPLACE cUsername WITH lcUsername, cPwrd WITH lcPassword
	ENDIF
ENDSCAN

*!*	ENDIF && llSpecialMode

CLOSE DATABASES
RETURN


PROCEDURE GetAddressList
	LOCAL lcList
	lcList = ""
	USE F:\UTIL\Kronos\EMAILS\EMAILS ALIAS EMAILS IN 0 SHARED
	SELECT EMAILS
	SCAN FOR Lsetup
	*SCAN
		IF EMPTY(lcList) THEN
			lcList = ALLTRIM(EMAILS.CEMAIL)
		ELSE
			lcList = lcList + ", " + ALLTRIM(EMAILS.CEMAIL)
		ENDIF
	ENDSCAN
	_cliptext = lcList
	CLOSE DATABASES 
	RETURN
ENDPROC
