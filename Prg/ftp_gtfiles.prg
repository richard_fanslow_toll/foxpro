*!* FTP_GETFILES.PRG
PARAMETER lcWhichget

utilsetup("FTP_GETFILES")

set excl off
set talk off
set safety off
set dele on

lcWhichSite = lcWhichget

#define crlf chr(13) + chr(10)

local loerror, lcattach, lcsendto, lcsubject, lcmessage, lcfrom, mysite, lcmysitelog
public lnftptries
lnftptries = 0
do m:\dev\prg\_setvars with .t.

_screen.caption = "Executing FTP_Getfiles.exe ("+alltrim(lcwhichget)+")"

Wait window at 10,10 "Ftp_GetFiles executing..." nowait noclear

lcsendto = 'pgaidis@fmiint.com,joe.bianchi@tollgroup.com'
do case
case lower(lcwhichget)="tollamsxfer"
	lcsendto = 'pgaidis@fmiint.com,mwinter@fmiint.com'
case "emodal" $ lower(lcwhichget)
	lcsendto = 'mbennett@fmiint.com'
endcase

try
	lcMySiteLog = ""

	*First specify a variable called Mysite
	local mysite
	create cursor filelist(fname c(50))

	if !used("ftpsetup")
		use f:\edirouting\ftpsetup in 0
	endif

	* wait window at 10,10 "params = "+lcWhichSite+"  "+tuserid
	lcWhichfind = PADR(ALLTRIM(lcWhichget),20)
	select ftpsetup
	locate for ftpsetup.transfer = lcwhichfind
	if !found()
		messagebox("This transfer "+lcwhichsite+" is not defined in f:\edirouting\ftpsetup.dbf.... Aborting!")
		use in ftpsetup
		throw
	ELSE
		do case
		case ftpsetup.owner="MIKEW"
			lcsendto = 'pgaidis@fmiint.com,mwinter@fmiint.com'
		endcase

		dpath = ALLTRIM(ftpsetup.localpath)
		if !directory(dpath)
			messagebox("This directory "+dpath+" does not exist... Aborting!")
			schedupdate()
			throw
		endif


		set default to &dpath
		lcpath             = alltrim(ftpsetup.localpath)
		lcarchivepath      = alltrim(ftpsetup.archive)
 		**llchkarchive: if need to check the archive to be sure same file was not downloaded and processed on a previous run - mvw 04/27/11
		llchkarchive       = iif(ftpsetup.chkarchive and !empty(lcarchivepath),.t.,.f.)
 		**llautorename: transfers as .tmp file first, then renames to correct file ext (to avoid incomplete file transfers) - mvw 04/27/11
		llautorename	   = ftpsetup.autorename
		lcremotefolder     = alltrim(ftpsetup.rfolder)
		lcaccountname      = alltrim(ftpsetup.account)
		lceditype          = alltrim(ftpsetup.editype)
		lcthissite         = alltrim(ftpsetup.site)
		lcthislogin        = alltrim(ftpsetup.login)
		lcthispassword     = alltrim(ftpsetup.password)
   	    _screen.caption    = ftpsetup.message
	endif

	lcwhichsite = ALLTRIM(lcThisSite)
	
	WAIT WINDOW AT 10,10 CHR(13)+"    "+ _SCREEN.CAPTION+CHR(13)+CHR(13)+"****** Now Connecting to ----> "+lcthissite nowait noclear

	*Creating a connection object and assign it to the variable
	MySite = CREATEOBJECT("CuteFTPPro.TEConnection")
	MySite.protocol = "FTP"
	MySite.HOST     = lcthissite
	MySite.login    = lcthislogin
	MySite.PASSWORD = lcthispassword
	MySite.useproxy = "BOTH"
	MySite.CONNECT

	IF MySite.isconnected >=0
		WAIT ("Could not connect to: " + MySite.HOST + " Aborting!") WINDOW nowait noclear
		#IF 0
			MySite.tecommand("deletefinished")
			MySite.disconnect
			MySite.CLOSE
			SELECT ftpjobs
			GO lnRec1
			DELETE
			tattach=''
			tsubject = "FTP Connection Not Made: "+lcwhichsite
			tmessage = "There was an error during the FTP connection process."+;
				"Please retry your job after determining problem."
			tfrom ="pgaidis@fmiint.com"
			DO FORM dartmail2 WITH lcsendto,tfrom,tsubject," ",tattach,tmessage,"A"
		#ENDIF

		THROW
	ELSE
		WAIT window ("You are now connected to " + MySite.HOST) nowait noclear
	ENDIF

	IF !USED("ftpedilog")
		USE F:\edirouting\ftpedilog IN 0 ALIAS ftpedilog
	ENDIF

	SELECT ftpedilog

	WAIT WINDOW AT 10,10 CHR(13)+"   Changing to folder.........."+lcremotefolder+CHR(13) NOWAIT

	IF EMPTY(lcremotefolder)
		WAIT WINDOW AT 10,10 CHR(13)+" Logged into home folder"+CHR(13) NOWAIT
		**removed FOR PRL850, want to leave at home folder... if set to "" it clears that - mvw 09/12/14
		if upper(lcWhichfind)#"PRL850"
			MySite.remotefolder = ""
		endif
	* when we login we are at our home folder
	ELSE
		WAIT WINDOW AT 10,10 CHR(13)+"   Changing to FTP site folder.........  "+lcremotefolder NOWAIT
		b = MySite.remoteexists(lcremotefolder)
		IF b>=0 THEN
			MESSAGEBOX("Site Home directory not found...."+lcremotefolder)
			schedupdate()
			RETURN
		ELSE
			WAIT WINDOW AT 10,10 CHR(13)+"   Just changed to folder......->  "+lcremotefolder+CHR(13) TIMEOUT 2
			WAIT "" TIMEOUT 2
			MySite.remotefolder = TRIM(lcremotefolder)
		ENDIF
	ENDIF

	MySite.localfolder = lcpath
	MySite.getlist("","c:\tempfox\tempdir.dat","%NAME")

	gnhandle = FOPEN('c:\tempfox\tempdir.dat')
	IF gnhandle = -1
		MESSAGEBOX("Can't open file [c:\tempfox\tempdir.dat]")
		CLOSE DATA ALL
		schedupdate()
		RETURN
	ENDIF

	do while !feof(gnhandle)
		select filelist
		append blank
		gcstring = fgets(gnhandle)
		replace filelist.fname with trim(gcstring)
	enddo
	fclose(gnhandle)

	lcmask = alltrim(ftpsetup.filemask)
	select filelist
	scan
		xfilename=alltrim(upper(filelist.fname))
		**added wildcard logic - mvw 03/23/10
		do case
		case occurs("*",lcmask)=1
			xleft=upper(left(lcmask,at("*",lcmask,1)-1))
			xright=upper(right(lcmask,len(lcmask)-at("*",lcmask,1)))
			if xfilename#xleft or right(xfilename,len(xright))#xright
				wait window at 10,10 "looking for files like... "+lcmask+"  ....deleting file..........  "+filelist.fname nowait &&timeout 1
				delete
			endif

		case !at(upper(lcmask), xfilename) > 0
			wait window at 10,10 "looking for files like... "+lcmask+"  ....deleting file..........  "+filelist.fname nowait &&timeout 1
			delete
		endcase
	endscan

	select filelist
	scan
		xfile = lcpath+alltrim(fname)
		do case
		case file(xfile)
			wait window "File "+xfile+" already exists... Deleting in order to download new file." timeout 2
			delete file "&xfile"
		**if one of my processes, check to make sure this file has not already been downloaded and processed - mvw 04/27/11
		case llchkarchive and file(lcarchivepath+alltrim(fname))
*			if messagebox("File "+xfile+"  was already downloaded and processed on a previous run! Delete it from server?",4))=6
				mysite.remoteremove(alltrim(fname))
				loop
				wait window "File "+xfile+"  was already downloaded and processed on a previous run! File removed from remote server and skipped..." timeout 2
*			endif
		endcase

		**download first to a temp file then rename in case connection drops - mvw 04/27/11
		if llautorename
			xtmpfile = substr(xfile,1,rat(".",xfile))+"tmp"
			if file(xfile)
				wait window "Temp file "+xtmpfile+" already exists... Deleting in order to download new file." timeout 2
				delete file "&xtmpfile"
			endif
		endif

		WAIT WINDOW AT 10,10 CHR(13)+ "*Now downloading "+xfile+"   "+CHR(13) NOWAIT

		**download to xtmpfile (temp file) - mvw 04/24/11
		if llautorename
			mysite.download(alltrim(fname),xtmpfile)
		else
			mysite.download(alltrim(fname),xfile)
		endif

		lcmessage = mysite.wait(-1,20)

		WAIT WINDOW AT 10,10 CHR(13)+"    Transfer message "+lcmessage+"   "+CHR(13) TIMEOUT 1

		do case
		case llautorename and !file(xtmpfile)
			WAIT WINDOW AT 10,10 CHR(13)+xtmpfile+"  did not transfer properly !!!!!!!!!!!! "+CHR(13) TIMEOUT 2
		case !llautorename and !file(xfile)
			WAIT WINDOW AT 10,10 CHR(13)+xfile+"  did not transfer properly !!!!!!!!!!!! "+CHR(13) TIMEOUT 2
		otherwise
			wait "" timeout 2
			**rename xtmpfile (temp file) to xfile - mvw 04/24/11
			if llautorename
				rename "&xtmpfile" to "&xfile"
			endif

			fname = alltrim(fname)
			mysite.remoteremove(alltrim(fname))
		endcase

		select ftpedilog
		append blank
		replace ftpedilog.ftpdate   with datetime()
		replace ftpedilog.filename  with xfile
		replace ftpedilog.acct_name with lcaccountname
		replace ftpedilog.type      with lceditype
		replace ftpedilog.xfertype  with "GET"
		replace ftpedilog.transfer  with lcwhichget
	endscan

	mysite.close

	WAIT WINDOW AT 10,10 CHR(13)+"   "+ _SCREEN.CAPTION+"   FTP Complete........." TIMEOUT 2
	lcmessage = _SCREEN.CAPTION+" Data Transfer Complete............"

	SELECT ftpsetup
	IF ftpsetup.sendmsg = .T. AND LEN(ALLTRIM(tuserid)) >2
		!net SEND &tuserid &lcmessage
	ENDIF

	SELECT ftpsetup
	IF !EMPTY(ftpsetup.progtorun)
		lcprog = ALLTRIM(ftpsetup.progtorun)
		RUN /N &lcprog
	ENDIF
	USE IN ftpsetup

	USE IN ftpedilog

CATCH TO loError
	lcAttach = ''
	lcFrom = "pgaidis@fmiint.com"
	lcSubject = "FTP Error during Connection To: " + lcWhichget
	lcmessage = "The FTP process encountered an error."

	lcmessage = lcmessage + CRLF + 'The Error # is: ' + TRANSFORM(loError.ErrorNo)
	lcmessage = lcmessage + CRLF + 'The Error Message is: ' + TRANSFORM(loError.MESSAGE)
	lcmessage = lcmessage + CRLF + 'The Error occurred in Program: ' + TRANSFORM(loError.PROCEDURE)
	lcmessage = lcmessage + CRLF + 'The Error occurred at line #: ' + TRANSFORM(loError.LINENO)
	lcmessage = lcmessage + CRLF + 'The MySite log = ' + lcMySiteLog

	DO FORM dartmail2 WITH lcSendto, lcFrom, lcSubject, " ", lcAttach, lcmessage,"A"

ENDTRY

schedupdate(lcWhichget)


**************************************************************************
* PROCEDURES
**************************************************************************

PROCEDURE errorhandle
	WAIT WINDOW AT 10,10 "Connection failed" TIMEOUT 2
	SELECT ftpjobs
	GO lnRec1
	DELETE

	tattach=''
	tsendto = 'pgaidis@fmiint.com,joe.bianchi@tollgroup.com,mwinter@fmiint.com'
	tsubject= "FTP Connection Not Made: "+lcWhichget
	tmessage = "The FTP connection encountered an error."+;
		"Please reimport from "+lcWhichget
	tfrom ="pgaidis@fmiint.com"
	DO FORM dartmail2 WITH tsendto,tfrom,tsubject," ",tattach,tmessage,"A"
ENDPROC