* Create spreadsheet of Hours info for NJ Drivers for the previous pay period.
* This is a version of the Summary (top) report on the wesdirpt form, customized for Mike DiVirgilio.

* run Tuesday pm for Mike DiVirgilio

* EXE = F:\UTIL\TRUCKING\NJDRIVERPRODUCTIONRPT.EXE
*
* Added support for CA drivers 6/2/2016 MB
* NOW EXPECTS 'N' OR 'C' AS A PARAMETER
*
* supported SQL Day/Daydet 03/06/2018 MB
*
* 4/24/2018 MB: changed fmiint.com emails to Toll emails; changed Paul Bobucky emails to Michael DiVirgilio.

LPARAMETERS tcOffice

LOCAL loNJDRIVERPRODUCTIONRPT, lcOffice

IF EMPTY(tcOffice) THEN
	lcOffice = 'N'
ELSE
	lcOffice = UPPER(ALLTRIM(tcOffice))
ENDIF

runack("NJDRIVERPRODUCTIONRPT")

utilsetup("NJDRIVERPRODUCTIONRPT")

loNJDRIVERPRODUCTIONRPT = CREATEOBJECT('NJDRIVERPRODUCTIONRPT')
loNJDRIVERPRODUCTIONRPT.MAIN(lcOffice)

=SchedUpdate()

CLOSE DATABASES ALL
RETURN

#DEFINE LOGIT 1
#DEFINE WAITIT 2
#DEFINE NOWAITIT 4
#DEFINE SENDIT 8
#DEFINE NOCRLF 16
#DEFINE CRLF CHR(13) + CHR(10)
#DEFINE LF CHR(10)
#DEFINE RETURN_DATA_MANDATORY .F.
#DEFINE RETURN_DATA_NOT_MANDATORY .T.
#DEFINE DAILY_OT_MULTIPLIER 1.5
#DEFINE DAILY_OT20_MULTIPLIER 2.0

DEFINE CLASS NJDRIVERPRODUCTIONRPT AS CUSTOM

	cProcessName = 'NJDRIVERPRODUCTIONRPT'

	lTestMode = .F.  && .F. FOR NORMAL OPERATION, .T. TO SUPPRESS ALL BUT DEVELOPER EMAILS AND CHANGE INPUT TABLE, ETC.

	* which payroll server, and associated connections, to hit
	lUsePAYROLL2Server = .T.

	lAutoYield = .T.

	cStartTime = TTOC(DATETIME())

	* connection properties
	nSQLHandle = 0

	* file properties
	nFileHandle = 0

	* sql/report filter properties
	cTempCoWhere = ""
	cTempCoWhere2 = ""
	cTempCo = ""
	cTempCoName = ""
	cExtraWhere = ""
	
	cExtraWhere2 = ""
	cOffice = ""
	cState = ""

	* date properties
	dtNow = DATETIME()

	dEndDate = {}
	dStartDate = {}

	dToday = DATE()
	* to force different pay weeks
	*dToday = {^2016-05-03}

	cToday = DTOC(DATE())

	* wait window properties
	nWaitWindowTimeout = 2
	lWaitWindowIsOn = .T.

	* logfile properties
	lLoggingIsOn = .T.
	cLogFile = 'F:\UTIL\TRUCKING\LOGFILES\NJDRIVERPRODUCTIONRPT_log.txt'

	* INTERNAL email properties
	lSendInternalEmailIsOn = .T.
	cFrom = 'mark.bennett@tollgroup.com'
	*cSendTo = 'mike.drew@tollgroup.com, Michael.Divirgilio@tollgroup.com'
	cSendTo = 'mark.bennett@tollgroup.com'
	cCC = ''
*!*		cSendTo = 'mark.bennett@tollgroup.com'
*!*		cCC = ''
	
	cSubject = ''
	cAttach = ''
	cBodyText = ''

	cCOMPUTERNAME = ''
	cUSERNAME = ''

	FUNCTION INIT
		IF NOT DODEFAULT()
			RETURN .F.
		ENDIF
		WITH THIS
			CLEAR
			*SET RESOURCE OFF
			CLOSE DATA
			SET CENTURY ON
			SET DATE YMD
			SET HOURS TO 24
			SET ANSI ON
			SET TALK OFF
			SET DELETED ON
			SET CONSOLE OFF
			SET EXCLUSIVE OFF
			SET SAFETY OFF
			SET EXACT OFF
			SET STATUS BAR ON
			SET SYSMENU OFF
			SET ENGINEBEHAVIOR 70
			SET DECIMALS TO 0
			_VFP.AUTOYIELD = .lAutoYield
			.cCOMPUTERNAME = GETENV("COMPUTERNAME")
			.cUSERNAME = GETENV("USERNAME")
			IF .lTestMode THEN
				.cSendTo = 'mark.bennett@tollgroup.com'
				.cCC = ''
				.cLogFile = 'F:\UTIL\TRUCKING\LOGFILES\NJDRIVERPRODUCTIONRPT_log_TESTMODE.txt'
			ENDIF
			.lLoggingIsOn = .lLoggingIsOn AND NOT EMPTY(.cLogFile)
			IF .lLoggingIsOn THEN
				SET ALTERNATE TO (.cLogFile) ADDITIVE
				SET ALTERNATE ON
			ENDIF
		ENDWITH
	ENDFUNC


	FUNCTION DESTROY
		WITH THIS
			IF .lLoggingIsOn  THEN
				SET ALTERNATE OFF
				SET ALTERNATE TO
			ENDIF
		ENDWITH
		DODEFAULT()
	ENDFUNC


	FUNCTION MAIN
		LPARAMETERS tcOffice
		WITH THIS
			LOCAL lnNumberOfErrors, lcSQL, lcSQL2, lcSQL3, lcSQL4, lcSQL5, loError, lcTopBodyText, llValid
			LOCAL lcXLFileName, ldStartDate, ldEndDate, lcStartDate, lcEndDate, ldDate
			LOCAL lcSQLStartDate, lcSQLEndDate, lcXLFileName, ldYesterday
			LOCAL oExcel, oWorkbook, oWorksheet, lnRow, lcRow, lnStartRow, lcStartRow, lnEndRow, lnPP, lcPP, lnNN, lcNN, lcJJ
			LOCAL lcColsToProcess, lnColumn, lcBoldColumn, i, j, lcBoldRow, luCellValue, lcTargetDirectory, ldPayDate, lcFiletoSaveAs
			LOCAL lcFileDate, lcDivision, lcTitle, llSaveAgain, lcPayCode, lcSpecialWhere
			LOCAL ii, lnTotPay, lnDaysToCount, lnAVG


			TRY
				lnNumberOfErrors = 0
				lcTopBodyText = ''

				.TrackProgress('NJ Driver Time Report process started....', LOGIT+SENDIT)
				.TrackProgress('COMPUTERNAME = ' + .cCOMPUTERNAME, LOGIT+SENDIT)
				.TrackProgress('USERNAME = ' + .cUSERNAME, LOGIT+SENDIT)
				.TrackProgress('Project =  NJDRIVERPRODUCTIONRPT', LOGIT+SENDIT)
				.TrackProgress('tcOffice = ' + tcOffice, LOGIT+SENDIT)
				IF .lTestMode THEN
					.TrackProgress('                        TEST MODE                        ', LOGIT+SENDIT)
				ENDIF
				
				IF INLIST(tcOffice,'N','C') THEN
					.cOffice = tcOffice
				ELSE
					.TrackProgress('ERROR: unexpected office parameter = ' + tcOffice, LOGIT+SENDIT)
					THROW
				ENDIF


				ldToday = .dToday

				ldTomorrow = ldToday + 1
				ldYesterday = ldToday - 1

				* calculate pay period start/end dates.
				* We go back from today until we hit a Saturday (DOW = 7) , which is the end day of the previous pay period.
				* then we subtract 6 days from that to get the prior Sunday (DOW = 1), which is the start day of the previous pay period.
				ldDate = ldToday
				DO WHILE DOW(ldDate,1) <> 7
					ldDate = ldDate - 1
				ENDDO
				ldEndDate = ldDate
				ldStartDate = ldDate - 6

*!*	*!*	***********************************
*!*	*!*	** force custom date ranges here
*!*	ldStartDate = {^2015-04-17}
*!*	ldEndDate = {^2015-05-29}
*!*	*!*	***********************************


				.dEndDate = ldEndDate
				.dStartDate = ldStartDate

				lcSpecialWhere = ""
				************************************************************

				SET DATE AMERICAN

				lcStartDate = STRTRAN(DTOC(ldStartDate),"/","-")
				lcEndDate = STRTRAN(DTOC(ldEndDate),"/","-")

				.cSubject = 'Driver Production for Week: ' + lcStartDate + " to " + lcEndDate
				
*!*					SET DATE YMD

*!*					*	 create "YYYYMMDD" safe date format for use in SQL query
*!*					* NOTE: for SQL we are going to use < EndDate ( not <= ), so we are going to add 1 to that date here for querying purposes.
*!*					lcSQLStartDate = STRTRAN(DTOC(ldStartDate),"/","")
*!*					lcSQLEndDate = STRTRAN(DTOC(ldEndDate + 1),"/","")
*!*					lcSQLToday = STRTRAN(DTOC(ldToday),"/","")
*!*					lcSQLTomorrow = STRTRAN(DTOC(ldTomorrow),"/","")

*!*					SET DATE AMERICAN
				* set up STATES, email recipients, etc.
				DO CASE
					CASE .cOffice = 'C'
						.cState = 'CA'
						.cSendTo = 'Michael.DiVirgilio@tollgroup.com, Cesar.Nevares@tollgroup.com'
						.cCC = 'mark.bennett@tollgroup.com'
					CASE .cOffice = 'N'
						.cState = 'NJ'
						.cSendTo = 'Michael.DiVirgilio@tollgroup.com, Mike.Drew@tollgroup.com, thomas.keaveney@tollgroup.com, Joe.Damato@tollgroup.com'
						.cCC = 'mark.bennett@tollgroup.com'
				ENDCASE		
						
				.cSubject = .cState + ' Driver Production for Week: ' + lcStartDate + " to " + lcEndDate

				* change to sql day/daydet 3/6/2018 MB
				*USE F:\WO\WODATA\DAY IN 0 ALIAS DAY
				*USE F:\WO\WODATA\DAYDET IN 0 ALIAS DAYDET				
				xsqlexec("select * from day","day",,"wo")
				xsqlexec("select * from daydet","daydet",,"wo")
				
				*USE F:\WO\WODATA\WESTXPAY IN 0 ALIAS WESTXPAY
				USE F:\HR\HRDATA\EMPLOYEE IN 0 ALIAS EMPLOYEE

				IF USED('CURDRIVERS') THEN
					USE IN CURDRIVERS
				ENDIF
				IF USED('CURDRIVERS2') THEN
					USE IN CURDRIVERS2
				ENDIF
				IF USED('CURDRIVERSPRE') THEN
					USE IN CURDRIVERSPRE
				ENDIF
				
*!*					*if goffice="N" THEN
*!*					xfilter2="!empty(approvedt)"
*!*					*case thisform.cbogetfmioo.value="FMI"
*!*					xfilter=xfilter+"company='T' and "

				* EXCLUDE REGIONAL DRIVERS, DO BY HARDCODED SS#S SINCE WE DON'T HAVE A TRUE DATA ELEMENT IDENTIFYING THEM
				*137-60-7790 = sasso
				*149-82-5657 = borges
				
							
				SELECT ;
					dis_driver, ;
					ss_num, ;
					zdate, ;
					leg, ;
					startloc, ;
					endloc, ;
					0000.00 AS extrapay, ;
					pay+xpay+waitpay AS totpay, ;
					approvedt ;
					FROM DAY, daydet ;
					WHERE DAY.dayid=daydet.dayid ;
					AND BETWEEN(zdate,ldStartDate,ldEndDate) ;
					AND office = .cOffice  ;
					AND company = "T" ;
					AND !EMPTYnul(approvedt) ;
					AND (NOT INLIST(ALLTRIM(employee.ss_num),"137-60-7790","149-82-5657")) ;
					ORDER BY dis_driver, zdate, leg ;
					INTO CURSOR CURDRIVERSPRE ;
					READWRITE
							
				* removed westxpay logic because Darren showed me there is no data in it more recent than 2013. 5/9/2016
				*!*	SELECT westxpay
				*!*	SCAN FOR BETWEEN(xpaydt,ldStartDate,ldEndDate) AND office = "N"
				*!*		m.dis_driver=westxpay.dis_driver
				*!*		SELECT employee
				*!*		LOCATE FOR employee=m.dis_driver AND (ACTIVE OR term_date>=ldStartDate-7) AND classif#51
				*!*		IF !FOUND()
				*!*			*email("Dyoung@fmiint.com","INFO: bad! driver not found in employee",m.dis_driver,,,,.t.,,,,,.t.,,.t.)
				*!*			.TrackProgress('ERROR! driver not found in employee table: ' + m.dis_driver, LOGIT+SENDIT)
				*!*		ELSE
				*!*			m.ss_num=employee.ss_num
				*!*			m.zdate=westxpay.xpaydt
				*!*			m.approvedt=DATE()
				*!*			m.startloc=westxpay.COMMENT
				*!*			m.endloc=""
				*!*			m.totpay= westxpay.extrapay
				*!*			INSERT INTO CURDRIVERSPRE FROM MEMVAR
				*!*		ENDIF
				*!*	ENDSCAN

				SELECT CURDRIVERSPRE
				INDEX ON dis_driver+DTOS(zdate)+STR(leg,2) TAG ZORDER

				CREATE CURSOR CURDRIVERS (dis_driver c(30), ss_num c(11), pay0 N(6,2), pay1 N(6,2), pay2 N(6,2), pay3 N(6,2), pay4 N(6,2), pay5 N(6,2), pay6 N(6,2), pay7 N(6,2), totpay N(6,2), avgpay N(6,2))
				INDEX ON dis_driver TAG dis_driver

				SELECT dis_driver, ss_num, zdate, SUM(totpay) AS pay FROM CURDRIVERSPRE GROUP BY 1,3 INTO CURSOR xtemp
				SCAN
					IF !BETWEEN(zdate,ldStartDate,ldEndDate)
						ii="0"
					ELSE
						ii=TRANSFORM(DOW(zdate))
					ENDIF
					IF !SEEK(dis_driver,"CURDRIVERS","dis_driver")
						INSERT INTO CURDRIVERS (dis_driver, ss_num) VALUES (xtemp.dis_driver, xtemp.ss_num)
					ENDIF
					REPLACE pay&ii WITH xtemp.pay IN CURDRIVERS
				ENDSCAN

				SELECT CURDRIVERS
				* per Mike DiV, only count mon-fri; calc average; and do not count 0$ days toward average calc
				*REPLACE ALL totpay WITH pay0+pay1+pay2+pay3+pay4+pay5+pay6+pay7
				SCAN
					lnTotPay = CURDRIVERS.pay2 + CURDRIVERS.pay3 + CURDRIVERS.pay4 + CURDRIVERS.pay5 + CURDRIVERS.pay6
					
					lnDaysToCount = 0
					lnDaysToCount = lnDaysToCount + IIF(CURDRIVERS.pay2 > 0,1,0)
					lnDaysToCount = lnDaysToCount + IIF(CURDRIVERS.pay3 > 0,1,0)
					lnDaysToCount = lnDaysToCount + IIF(CURDRIVERS.pay4 > 0,1,0)
					lnDaysToCount = lnDaysToCount + IIF(CURDRIVERS.pay5 > 0,1,0)
					lnDaysToCount = lnDaysToCount + IIF(CURDRIVERS.pay6 > 0,1,0)					
					
					IF lnDaysToCount > 0 THEN
						lnAVG = lnTotPay / lnDaysToCount
					ELSE
						lnAVG = 0
					ENDIF
					
					REPLACE CURDRIVERS.totpay WITH lnTotPay, CURDRIVERS.avgpay WITH lnAVG IN CURDRIVERS
				
				ENDSCAN
			
*!*					* EXCLUDE REGIONAL DRIVES - HOW, BY SS#?
*!*					SELECT CURDRIVER
*!*					SCAN
*!*					
*!*					ENDSCAN
				

*BROW

				WAIT WINDOW NOWAIT "Preparing data..."
				
				SELECT * FROM CURDRIVERS INTO CURSOR CURDRIVERS2 ORDER BY AVGPAY DESCENDING

				SELECT CURDRIVERS2
				GOTO TOP
				IF EOF() THEN
					.TrackProgress("There was no data to export!", LOGIT+SENDIT)
				ELSE
				
					lcFilePrefix = .cState + " Driver Production Week Ending"
					lcFileDate = DTOS(ldEndDate)

					lcTargetDirectory = "F:\UTIL\TRUCKING\REPORTS\" && + lcFileDate + "\"

					lcFiletoSaveAs = lcTargetDirectory + lcFilePrefix + " " + lcFileDate
					lcXLFileName = lcFiletoSaveAs + ".XLSX"

					* create primary, detail spreadsheet

					WAIT WINDOW NOWAIT "Opening Excel..."
					oExcel = CREATEOBJECT("excel.application")
					oExcel.VISIBLE = .F.
					oWorkbook = oExcel.workbooks.OPEN("F:\UTIL\TRUCKING\TEMPLATES\NJDRIVERPRODUCTIONRPT.XLSX")

					* if target directory exists, save there
					llSaveAgain = .F.
					IF DIRECTORY(lcTargetDirectory) THEN
						IF FILE(lcXLFileName) THEN
							DELETE FILE (lcXLFileName)
						ENDIF
						oWorkbook.SAVEAS(lcXLFileName)
						* set flag to save again after sheet is populated
						llSaveAgain = .T.
					ENDIF
					***********************************************************************************************************************
					***********************************************************************************************************************
					WAIT WINDOW NOWAIT "Building Driver Production spreadsheet..."


					oWorksheet = oWorkbook.Worksheets[1]
*!*						oWorksheet.RANGE("A" + lcStartRow,"P10000").clearcontents()

*!*						oWorksheet.RANGE("A" + lcStartRow,"P10000").FONT.SIZE = 10
*!*						oWorksheet.RANGE("A" + lcStartRow,"P10000").FONT.NAME = "Arial"
*!*						oWorksheet.RANGE("A" + lcStartRow,"P10000").FONT.bold = .F.

*!*						lcTitle = "NJ Driver Time" + LF + ;
*!*							" for Pay Date: " + DTOC(ldPayDate) + LF + ;
*!*							" Week of: " + lcStartDate + " - " + lcEndDate

					oWorksheet.RANGE("A1").VALUE = .cState + 'TRUCKING'

					oWorksheet.RANGE("C1").VALUE = ldStartDate
					oWorksheet.RANGE("C2").VALUE = ldEndDate
					
					oWorksheet.RANGE("B4").VALUE = CDOW(ldStartDate + 1)
					oWorksheet.RANGE("C4").VALUE = CDOW(ldStartDate + 2)
					oWorksheet.RANGE("D4").VALUE = CDOW(ldStartDate + 3)
					oWorksheet.RANGE("E4").VALUE = CDOW(ldStartDate + 4)
					oWorksheet.RANGE("F4").VALUE = CDOW(ldStartDate + 5)
					
					oWorksheet.RANGE("B5").VALUE = ldStartDate + 1
					oWorksheet.RANGE("C5").VALUE = ldStartDate + 2
					oWorksheet.RANGE("D5").VALUE = ldStartDate + 3
					oWorksheet.RANGE("E5").VALUE = ldStartDate + 4
					oWorksheet.RANGE("F5").VALUE = ldStartDate + 5
					
					lnRow = 5
					lnStartRow = lnRow + 1
					lcStartRow = ALLTRIM(STR(lnStartRow))
					
					* main scan/processing
					SELECT CURDRIVERS2
					SCAN
						* NO CHANGES; just advance a line
						lnRow = lnRow + 1

						lcRow = LTRIM(STR(lnRow))
						oWorksheet.RANGE("A" + lcRow).VALUE = CURDRIVERS2.dis_driver
						oWorksheet.RANGE("B" + lcRow).VALUE = CURDRIVERS2.pay2
						oWorksheet.RANGE("C" + lcRow).VALUE = CURDRIVERS2.pay3
						oWorksheet.RANGE("D" + lcRow).VALUE = CURDRIVERS2.pay4
						oWorksheet.RANGE("E" + lcRow).VALUE = CURDRIVERS2.pay5
						oWorksheet.RANGE("F" + lcRow).VALUE = CURDRIVERS2.pay6
						oWorksheet.RANGE("G" + lcRow).VALUE = CURDRIVERS2.totpay
						oWorksheet.RANGE("H" + lcRow).VALUE = CURDRIVERS2.avgpay

					ENDSCAN


					*oWorksheet.PageSetup.PrintArea = "$A$"  + lcStartRow + ":$F$" + lcRow

					*******************************************************************************************
					*******************************************************************************************

					* save again
					IF llSaveAgain THEN
						oWorkbook.SAVE()
					ENDIF
					oWorkbook.CLOSE()

					oExcel.QUIT()

					lcTopBodyText = "See attached report."


					IF FILE(lcXLFileName) THEN
						.cAttach = lcXLFileName
						.TrackProgress('Attached file : ' + lcXLFileName, LOGIT+SENDIT)
						.TrackProgress('Driver Production process ended normally.', LOGIT+SENDIT+NOWAITIT)
					ELSE
						.TrackProgress('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!', LOGIT+SENDIT)
						.TrackProgress("ERROR: There was a problem attaching file : " + lcXLFileName, LOGIT+SENDIT)
						.TrackProgress('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!', LOGIT+SENDIT)
					ENDIF

					IF .lTestMode THEN
						.cBodyText = lcTopBodyText + CRLF + CRLF + "<processing log follows>" + CRLF + CRLF + .cBodyText
					ELSE
						.cBodyText = lcTopBodyText + CRLF + CRLF + .cBodyText
					ENDIF

				ENDIF  && EOF() CURDRIVERS2


			CATCH TO loError

				.TrackProgress('There was an error.',LOGIT+SENDIT)
				.TrackProgress('The Error # is: ' + TRANSFORM(loError.ERRORNO), LOGIT+SENDIT)
				.TrackProgress('The Error Message is: ' + TRANSFORM(loError.MESSAGE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred in Program: ' + TRANSFORM(loError.PROCEDURE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred at line #: ' + TRANSFORM(loError.LINENO), LOGIT+SENDIT)
				lnNumberOfErrors = lnNumberOfErrors + 1
				IF TYPE('oExcel') = 'O' AND NOT ISNULL(oExcel) THEN
					oExcel.QUIT()
				ENDIF
				*CLOSE DATA
				*CLOSE ALL

			ENDTRY

			CLOSE DATA
			WAIT CLEAR
			***************** INTERNAL email results ******************************
			.TrackProgress('About to send status email.',LOGIT)
			.TrackProgress('==================================================================================================================', SENDIT)
			.TrackProgress('The logfile is: ' + .cLogFile, SENDIT)
			.TrackProgress('Driver Production process started: ' + .cStartTime, LOGIT+SENDIT)
			.TrackProgress('Driver Production process finished: ' + TTOC(DATETIME()), LOGIT+SENDIT)

			IF .lSendInternalEmailIsOn THEN
				* try to trap error from not having dartmail dll's registered on user's pc...
				TRY
					DO FORM dartmail2 WITH .cSendTo,.cFrom,.cSubject,.cCC,.cAttach,.cBodyText,"A"
					.TrackProgress('Sent status email.',LOGIT)
				CATCH TO loError
					*=MESSAGEBOX('There was an error sending the status email. Please contact Mark Bennett.',0+64,"Kronos NJ Driver Time")
					.TrackProgress('There was an error sending the status email.',LOGIT)
					.TrackProgress('The Error # is: ' + TRANSFORM(loError.ERRORNO), LOGIT)
					.TrackProgress('The Error Message is: ' + TRANSFORM(loError.MESSAGE), LOGIT)
					.TrackProgress('The Error occurred in Program: ' + TRANSFORM(loError.PROCEDURE), LOGIT)
					.TrackProgress('The Error occurred at line #: ' + TRANSFORM(loError.LINENO), LOGIT)
					lnNumberOfErrors = lnNumberOfErrors + 1
					*CLOSE DATA
				ENDTRY

			ELSE
				.TrackProgress('Did not send status email: THIS.lSendInternalEmailIsOn = FALSE.',LOGIT)
			ENDIF

		ENDWITH
		RETURN
	ENDFUNC && main


	PROCEDURE TrackProgress
		* do any combination of Wait Window, writing to logfile, and adding to body of email,
		* based on nFlags parameter.
		LPARAMETERS tcExpression, tnFlags
		LOCAL lcCRLF
		WITH THIS
			IF BITAND(tnFlags,NOCRLF) = NOCRLF THEN
				lcCRLF = ""
			ELSE
				lcCRLF = CRLF
			ENDIF
			IF BITAND(tnFlags,LOGIT) = LOGIT THEN
				IF .lLoggingIsOn THEN
					?
					? .cProcessName + "  " + TTOC(DATETIME()) + ": " + tcExpression
				ENDIF
			ENDIF
			IF .lWaitWindowIsOn THEN
				IF BITAND(tnFlags,NOWAITIT) = NOWAITIT THEN
					WAIT WINDOW tcExpression NOWAIT
				ENDIF
				IF BITAND(tnFlags,WAITIT) = WAITIT THEN
					WAIT WINDOW tcExpression TIMEOUT .nWaitWindowTimeout
				ENDIF
			ENDIF
			IF BITAND(tnFlags,SENDIT) = SENDIT THEN
				IF .lSendInternalEmailIsOn THEN
					.cBodyText = .cBodyText + tcExpression + CRLF + lcCRLF
				ENDIF
			ENDIF
		ENDWITH
	ENDPROC  &&  TrackProgress


*!*		FUNCTION CreatePDFReport
*!*			* create PDF report
*!*			LPARAMETERS tcRootPDFFilename, tcPDFReportFRX

*!*			WITH THIS

*!*				.TrackProgress('in CreatePDFReport tcRootPDFFilename=' + tcRootPDFFilename, LOGIT)
*!*				.TrackProgress('in CreatePDFReport tcPDFReportFRX=' + tcPDFReportFRX, LOGIT)

*!*				LOCAL llPDFReportWasCreated, lcRootPDFFilename, lcFullPDFFilename, lcPDFReportFRX
*!*				lcCentury = SET("CENTURY")

*!*				SET CENTURY OFF  && FOR XX/XX/XX FORMAT DATES ON REPORT
*!*				lcRootPDFFilename = tcRootPDFFilename
*!*				lcPDFReportFRX = tcPDFReportFRX

*!*				lcFullPDFFilename = lcRootPDFFilename + ".PDF"

*!*				llPDFReportWasCreated = .F.

*!*				DO M:\DEV\PRG\RPT2PDF WITH lcPDFReportFRX, lcRootPDFFilename, llPDFReportWasCreated

*!*				IF NOT llPDFReportWasCreated THEN
*!*					.TrackProgress('ERROR - PDF file [' + lcFullPDFFilename + '] was not created',LOGIT+WAITIT+SENDIT)
*!*				ENDIF

*!*				*!*				* keep track of pdf report files for emailing later
*!*				*!*				.AddAttachment(lcFullPDFFilename)

*!*				SET PRINTER TO DEFAULT
*!*				SET CENTURY &lcCentury
*!*			ENDWITH
*!*		ENDFUNC  &&  CreatePDFReport


ENDDEFINE
