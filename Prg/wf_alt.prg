* returns the folder for WMS data
*
* xaccountid is only needed if xoffice="C"
*
* if root=.f., returns f:\whx\whdata\
* if root=.t., returns f:\whx\

LPARAMETERS xoffice, xaccountid, xroot, xarchive
glivedata=.T.
xoffice=UPPER(xoffice)
LOCAL xh
IF xaccountid = 1080
	xreturn="f:\express\fxdata\"
	RETURN xreturn
ENDIF

IF TYPE("glivedata")="L" AND !glivedata
	xreturn="m:\dev\whdata\"

ELSE
	xh=IIF(xarchive,"h","")

	IF !USED("whoffice")
		xsqlexec("select * from whoffice",,,"wh")
		index on office tag office
		set order to
	ENDIF

	IF xoffice = "E"
		xreturn = "f:\wh7\whdata\"
		WAIT WINDOW xreturn TIMEOUT 1
		RETURN xreturn
	ENDIF
	IF xoffice = "P"
		xreturn = "f:\whp\whdata\"
		WAIT WINDOW xreturn TIMEOUT 1
		RETURN xreturn
	ENDIF

* end add

	IF !INLIST(xoffice,"I","C","K")
		=SEEK(xoffice,"whoffice","office")
		xreturn=whoffice.folder+"\whdata"+xh+"\"
		WAIT WINDOW xreturn NOWAIT
	ELSE
		IF EMPTY(xaccountid)
			WAIT WINDOW "Account # missing - system will take an ugly dump now" TIMEOUT 2
			RETURN
		ELSE
			xsqlexec("select * from account where inactive=0","lazyboy",,"qq")
			INDEX ON accountid TAG accountid
			SET ORDER TO
			
			IF SEEK(xaccountid,"lazyboy","accountid")
				DO CASE
					CASE xoffice="I" AND lazyboy.njmod="I"
						=SEEK("I","whoffice","office")
					CASE xoffice="I" AND lazyboy.njmod="J"
						=SEEK("J","whoffice","office")
					CASE xoffice="C" AND lazyboy.building="4"
						=SEEK("1","whoffice","office")
					CASE xoffice="C" AND INLIST(lazyboy.building,"7","8")
						=SEEK("2","whoffice","office")
					CASE xoffice="C" AND lazyboy.building="6"
						=SEEK("5","whoffice","office")
					CASE xoffice="C" AND lazyboy.building="5"
						=SEEK("6","whoffice","office")
					CASE xoffice="C" AND lazyboy.building="0"
						=SEEK("7","whoffice","office")
					CASE xoffice="C" AND lazyboy.building="Y"  &&& added TMARG 7/25/16 for 2XU
						=SEEK("Y","whoffice","office")
					CASE xoffice="C" AND lazyboy.building="1"
						=SEEK("8","whoffice","office")
					CASE xoffice="L" AND lazyboy.building="8"
						=SEEK("L","whoffice","office")
					CASE xoffice="K" AND lazyboy.kymod="K"
						=SEEK("K","whoffice","office")
					CASE xoffice="K" AND lazyboy.kymod="S"	&& steelxyz
						=SEEK("S","whoffice","office")
					OTHERWISE
						WAIT WINDOW "BUILDING NOT FOUND IN Account...terminating" TIMEOUT 1
						RETURN "XXX"
				ENDCASE
				xreturn=whoffice.folder+"\whdata"+xh+"\"
				WAIT WINDOW xreturn NOWAIT

			ELSE
				WAIT WINDOW "Account # "+TRANSFORM(xaccountid)+" not found - system will now crash" TIMEOUT 2
			ENDIF

			USE IN lazyboy
		ENDIF
	ENDIF
ENDIF

IF xroot
	IF TYPE("glivedata")="U" OR glivedata
		xreturn=STRTRAN(xreturn,"\whdata"+xh,"")
	ELSE
		TRY
			xreturn=STRTRAN(xreturn,"\whdata"+xh,"")
		CATCH
			xreturn="f:\wh"+goffice+"\"
		ENDTRY
	ENDIF
ENDIF
IF USED("whoffice")
	USE IN whoffice
ENDIF
WAIT CLEAR
RETURN xreturn
