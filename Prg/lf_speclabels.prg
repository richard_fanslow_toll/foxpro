* last run December 2016	dy 6/13/17
* does not work with SQL usesqloutwo()


CLOSE DATABASES ALL
Set Exclusive Off
Set Deleted On
Set Talk Off
Set Safety Off
With _Screen
	.AutoCenter = .T.
	.WindowState = 0
	.BorderStyle = 1
	.Width = 320
	.Height = 210
	.Top = 290
	.Left = 110
	.Closable = .T.
	.MaxButton = .T.
	.MinButton = .T.
	.ScrollBars = 0
	.Caption = "LF_SPECLABELS"
Endwith

goffice="I"

If !Used("labels")
	USE F:\LabelData\Lifefactory\labels In 0
Endif

Xpath='F:\FTPUSERS\LIFEFACTORY\PRINTLABEL\'
len1 = Adir(ary1,Xpath+"*.*")
If len1 = 0
	Wait Window "No files found...exiting" Timeout 2
	Close Data All

	On Error
	Return
Endif

For xrow = 1 To len1
*Copy File F:\FTPUSERS\LIFEFACTORY\PRINTLABEL\original\printlabel.xlsx To F:\FTPUSERS\LIFEFACTORY\PRINTLABEL\printlabel.csv
	Set Step On
CREATE CURSOR t1 (ship_ref c(20), line c(10), style C(20), supplier C(10), pack C(10), pcs N(10,0), gtin c(20))
	Tfile=Alltrim(ary1[xrow,1])
	tfilepath=Xpath+Alltrim(ary1[xrow,1])
	Appe From "&tfilepath" Type Deli
	DELETE FOR ship_ref='PT' In T1

*	Copy File F:\FTPUSERS\MJ_Wholesale\In\RA\&Tfile To F:\FTPUSERS\MJ_Wholesale\In\RA\archive\&Tfile
*	Delete File F:\FTPUSERS\MJ_Wholesale\In\RA\&Tfile

xsqlexec("select consignee as ship_to, name, address as staddr1 ,csz as stcsz, outshipid, cnee_ref as cust_po from outship where accountid=6034 and wo_date>{"+dtoc(date()-60)+"}",,,"wh")
index on ship_ref tag ship_ref
set order to
SELECT a.* ,outdetid, b.style, upc,totqty,printstuff,  SPACE(50) as desc, b.accountid, b.wo_num FROM t2 a LEFT JOIN outdet b ON a.outshipid=b.outshipid AND b.accountid=6034  INTO CURSOR t3 READWRITE

DELETE FOR STYLE_A != STYLE_B IN T3
replace all desc with getmemodata("printstuff","DESC")
*SELECT a.*,shipto, staddr1, stcsz, cust_po, pt,b.outdetid, b.totqty,n1,q1 FROM t3 a LEFT JOIN labels b ON a.ship_ref=b.pt INTO CURSOR t5 READWRITE
*DELETE for outdetid_a !=outdetid_b in t5
SELECT  ship_ref, SUM(TOTQTY) AS TOTCARTONS FROM T3 GROUP BY ship_ref INTO CURSOR T6 READWRITE
SELECT A.*,B.TOTCARTONS FROM T3 A LEFT JOIN T6 B ON A.SHIP_REF=B.SHIP_REF INTO CURSOR T7 READWRITE
select * from t7 where .f. into cursor t8 readwrite
select t7
scan
scatter memvar 
for i = 1 to totqty
	insert into t8 from memv
endfor
endscan
SELECT 10
COPY TO F:\LabelData\Lifefactory\SPECLABELS.DBF WITH CDX

ENDFOR
SET STEP ON 
DO FORM M:\DEV\FRM\lf_speclabels.scx
Close Data All

*_Screen.Caption=gscreencaption
