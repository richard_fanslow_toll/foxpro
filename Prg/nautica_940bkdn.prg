PARAMETERS cFilename

lLX = .F.
lnEaches = 0
nRepQty = 0
lPick = .F.
lPrepack = .T.
cBillTo = ""
lPackQty = 0
m.isa_num = ""
m.printstuff = ""
cVendNum = ""

SELECT x856
LOCATE

COUNT FOR TRIM(segment) = "W05" TO nSegCnt
cSegCnt = ALLTRIM(STR(nSegCnt))
WAIT "There are "+cSegCnt+" P/Ts in this file" WINDOW TIMEOUT 2
nPTqty = nSegCnt
SELECT x856
LOCATE

*!*	IF !USED("uploaddet")
*!*		IF lTesting OR lTestUploaddet
*!*			USE F:\3pl\test\uploaddet IN 0 ALIAS uploaddet
*!*		ELSE
*!*			USE F:\ediwhse\DATA\uploaddet IN 0 SHARED ALIAS uploaddet
*!*		ENDIF
*!*	ENDIF

SELECT xpt
DELETE ALL
SELECT xptdet
DELETE ALL

STORE 0 TO pt_total_cartons
STORE 0 TO nCtnCount
STORE 0 TO ptctr
STORE 0 TO nTotWt,nTotCube
cStoreNum = ""

SELECT x856
LOCATE

IF lTesting
	ASSERT .F. MESSAGE "At x856 Scan"
ENDIF
m.PTDETID=0
nXPTQty = 0

SELECT x856
LOCATE FOR TRIM(x856.segment) = "N1" AND INLIST(TRIM(x856.f1),"SF")
IF "KIPLING"$x856.f2  && flip the account# to Kipling
	nAcctNum = 6356
	STORE nAcctNum TO Acctnum
	cAcctNum = ALLTRIM(STR(nAcctNum))
ENDIF
LOCATE

DO WHILE !EOF("x856")
	cSegment = TRIM(x856.segment)
	cF1 = TRIM(x856.f1)

	IF INLIST(TRIM(x856.segment),"GS","ST")
		SELECT x856
		IF !EOF()
			SKIP 1 IN x856
			LOOP
		ENDIF
	ENDIF

	IF TRIM(x856.segment) = "ISA"
	cISA_Num = ALLTRIM(x856.f13)
	endif

	IF TRIM(x856.segment) = "W05"  && new shipment
		SELECT xpt
		APPEND BLANK
		STORE 0 TO pt_total_cartons
		nXPTQty = 0
		ptctr = ptctr +1
		REPLACE xpt.ptid       WITH ptctr IN xpt
		REPLACE xpt.ptdate     WITH DATE() IN xpt
		m.adddt = DATETIME()
		m.addby = "NAUT940"
		m.addproc = "NAUTICA940"
		REPLACE xpt.accountid WITH nAcctNum,xpt.adddt WITH m.adddt,xpt.addby WITH m.addby,xpt.addproc WITH m.addproc IN xpt
		cShip_ref = ALLTRIM(x856.f2)
		cCnee_ref = UPPER(ALLTRIM(x856.f3))
		REPLACE xpt.ship_ref WITH cShip_ref IN xpt && Pickticket
		REPLACE xpt.cnee_ref WITH cCnee_ref IN xpt  && Customer PO
		WAIT CLEAR
		waitstr = "Now processing PT # "+cShip_ref+CHR(13)+"FILE: "+cFilename
		WAIT WINDOW AT 10,10  waitstr NOWAIT
	ENDIF

	IF INLIST(TRIM(x856.segment),"GE","IEA")
		IF !EOF("x856")
			SKIP 1 IN x856
		ENDIF
		LOOP
	ENDIF

	IF TRIM(x856.segment) = "N1" AND TRIM(x856.f1)="ST"
		cDCNum      = UPPER(ALLTRIM(x856.f4))
		nStoreNum   = UPPER(ALLTRIM(x856.f4))
		cConsignee  = UPPER(ALLT(x856.f2))
		REPLACE xpt.dcnum    WITH cDCNum IN xpt
		REPLACE xpt.storenum WITH VAL(nStoreNum) IN xpt

		IF EMPTY(ALLTRIM(xpt.shipins))
			REPLACE xpt.shipins WITH "STORENUM*"+cDCNum IN xpt  && DC or Store Number
		ELSE
			IF !"STORENUM*"$xpt.shipins
				REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"STORENUM*"+cDCNum IN xpt  && DC or Store Number
			ENDIF
		ENDIF
		REPLACE xpt.consignee WITH cConsignee IN xpt
		REPLACE xpt.NAME      WITH xpt.consignee IN xpt
		SKIP 1 IN x856
		DO CASE
			CASE TRIM(x856.segment) = "N3"  && address info
				REPLACE xpt.address   WITH UPPER(ALLTRIM(x856.f1)) IN xpt
				REPLACE xpt.address2  WITH UPPER(ALLTRIM(x856.f2)) IN xpt
			OTHERWISE
				cMessage = "Missing Ship-To Address Info: PT "+cShip_ref
				SET STEP ON
				WAIT WINDOW cMessage TIMEOUT 2
				NormalExit = .F.
				THROW
		ENDCASE
		SKIP 1 IN x856
		IF TRIM(x856.segment) = "N4"  && address info
			cCSZ = TRIM(x856.f1)+", "+TRIM(x856.f2)+" "+TRIM(x856.f3)
			REPLACE xpt.csz WITH UPPER(cCSZ) IN xpt
		ELSE
			cMessage = "Missing Ship-To CSZ Info"
			WAIT WINDOW cMessage TIMEOUT 2
			NormalExit = .F.
			THROW
		ENDIF
	ENDIF

	IF TRIM(x856.segment) = "N1" AND TRIM(x856.f1) = "SF"
		REPLACE xpt.shipins WITH "SHIPFROM*"+UPPER(ALLTRIM(x856.f2)) IN xpt
	ENDIF

	IF TRIM(x856.segment) = "N1" AND TRIM(x856.f1)= "Z7"
		cShipFor   = UPPER(ALLTRIM(x856.f2))
		cSforStNum = UPPER(ALLTRIM(x856.f4))
		REPLACE xpt.sforstore WITH cSforStNum IN xpt
		REPLACE xpt.shipfor   WITH cShipFor   IN xpt
		SKIP 1 IN x856
		IF TRIM(x856.segment) = "N3"  && address info
			REPLACE xpt.sforaddr1  WITH UPPER(TRIM(x856.f1)) IN xpt
			REPLACE xpt.sforaddr2  WITH UPPER(TRIM(x856.f2)) IN xpt
		ELSE
			cMessage = "Missing Ship-For Address Info, PT "+cShip_ref
			SET STEP ON
			WAIT WINDOW cMessage TIMEOUT 2
			NormalExit = .F.
			THROW
		ENDIF
		SKIP 1 IN x856
		IF TRIM(x856.segment) = "N4"  && address info
			csforCSZ = TRIM(x856.f1)+", "+TRIM(x856.f2)+" "+TRIM(x856.f3)
			REPLACE xpt.sforcsz WITH UPPER(csforCSZ) IN xpt
		ELSE
			cMessage = "Missing Ship-To CSZ Info"
			WAIT WINDOW cMessage TIMEOUT 2
			NormalExit = .F.
			THROW
		ENDIF
	ENDIF
	IF TRIM(x856.segment) = "N9"
		DO CASE
			CASE TRIM(x856.f1) = "DP" && Department
				cDept = TRIM(x856.f2)
				REPLACE xpt.dept WITH cDept IN xpt

			CASE TRIM(x856.f1) = "ST"
				REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"SHIPTO*"+TRIM(x856.f2)

			CASE TRIM(x856.f1) = "FOB"
				REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"FOB*"+TRIM(x856.f2)

			CASE TRIM(x856.f1) = "CO"
				REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"CO*"+TRIM(x856.f2)

			CASE TRIM(x856.f1) = "98"
				REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"98*"+TRIM(x856.f2)

			CASE TRIM(x856.f1) = "14"
				REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"14*"+TRIM(x856.f2)
		ENDCASE
	ENDIF

	IF TRIM(x856.segment) = "G62" AND TRIM(x856.f1) == "37" && PO Date
		cXdate1 = TRIM(x856.f2)
		datestrconversion()
		REPLACE xpt.START WITH dxdate2 IN xpt
	ENDIF

	IF TRIM(x856.segment) = "G62" AND TRIM(x856.f1) == "38" && Cancel Date
		cXdate1 = TRIM(x856.f2)
		datestrconversion()
		REPLACE xpt.CANCEL WITH dxdate2 IN xpt
	ENDIF

*********************************************
*!* XPTDET Update Section
*********************************************

	lLoopSeg = "LX"
	IF TRIM(x856.segment) = lLoopSeg && start of PT detail, stay here until this PT is complete
*    ASSERT .F. MESSAGE "At LX loop segment"
		lLX = .T.

		DO WHILE TRIM(x856.segment) != "SE"
			cxSegment = TRIM(x856.segment)
			cxField01 = TRIM(x856.f1)

			IF TRIM(x856.segment) = "LX"
				SKIP 1 IN x856
				SELECT xptdet
				SCATTER MEMVAR MEMO BLANK
				LOOP
			ENDIF

			IF TRIM(x856.segment) = "G69"
				m.printstuff = "DESC*"+ALLTRIM(x856.f1)
			ENDIF

			IF TRIM(x856.segment) = "N9" AND TRIM(x856.f1)= "VCL"
				m.printstuff = m.printstuff+CHR(13)+"COLORDESC*"+ALLTRIM(x856.f3)
				m.color = ALLTRIM(x856.f2)
			ENDIF

			IF TRIM(x856.segment) = "N9" AND TRIM(x856.f1)= "VSZ"
				m.id = ALLTRIM(x856.f2)
			ENDIF

			IF TRIM(x856.segment) = "W01"
				m.totqty = INT(VAL(ALLTRIM(x856.f1)))
				m.origqty = m.totqty

				IF TRIM(x856.f15) = "VN"
					m.style = ALLTRIM(x856.f16)
				ENDIF

				IF TRIM(x856.f6) = "UP"
					m.upc = ALLTRIM(x856.f7)
				ENDIF
			ENDIF
			SELECT x856

			IF TRIM(x856.segment) = "W20"  && end of this pick ticket

				m.pack = ALLTRIM(TRANSFORM(VAL(ALLTRIM(x856.f1))/m.totqty))
				m.casepack = ALLTRIM(x856.f1)

				SELECT xptdet
				APPEND BLANK
				m.PTDETID=m.PTDETID+1
				GATHER MEMVAR MEMO
				REPLACE xptdet.addby     WITH "NAUT940" IN xptdet
				REPLACE xptdet.adddt     WITH DATETIME() IN xptdet
				REPLACE xptdet.ptid      WITH xpt.ptid   IN xptdet
				REPLACE xptdet.accountid WITH nAcctNum   IN xptdet
				REPLACE xptdet.ship_ref WITH xpt.ship_ref
				SELECT xptdet
				SUM totqty TO pttot FOR xptdet.ship_ref = xpt.ship_ref
				SELECT xpt
				REPLACE xpt.qty     WITH pttot IN xpt
				REPLACE xpt.origqty WITH pttot IN xpt
			ENDIF

			SELECT  x856
			IF !EOF()
				SKIP 1 IN x856
			ENDIF
		ENDDO   && SEGMENT NOW = 'SE'

	ENDIF
	IF !EOF("x856")
		SKIP 1 IN x856
	ENDIF
ENDDO

DO m:\dev\prg\setuppercase940

IF lBrowfiles
	SET STEP ON
	WAIT WINDOW "Now displaying XPT/XPTDET cursors..."+CHR(13)+"CANCEL at XPTDET if desired." TIMEOUT 2
	SELECT xpt
	SET ORDER TO
	LOCATE
	BROWSE
	SELECT xptdet
	SET ORDER TO
	LOCATE
	BROWSE
	IF MESSAGEBOX("Do you wish to continue?",4+32+256,"Browse Files Box") # 6
		CANCEL
	ENDIF
	SET STEP ON
ENDIF

WAIT CLEAR
WAIT cMailName+" BKDN process complete for file "+cFilename WINDOW TIMEOUT 2
lLX = .F.
lnEaches = 0
lPick = .F.
nRepQty = 0
m.isa_num = ""
SELECT x856
lNewPT = .T.
RETURN

******************************
PROCEDURE datestrconversion
******************************
	cXdate1 = TRIM(cXdate1)
	dxdate2 = CTOD(SUBSTR(cXdate1,5,2)+"/"+RIGHT(cXdate1,2)+"/"+LEFT(cXdate1,4))
	RETURN
