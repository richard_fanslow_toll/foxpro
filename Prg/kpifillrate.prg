**Fill Rate KPI

**Accuracy of warehouse pick
**  - Based on no. of cartons cut from wos b/c freight was lost? - all cuts are saved in cut.dbf
**  - Fill rate calculated on no. of cartons/goh actually shipped (based on staged date) vs. ;
**		# shipped + # of cuts
**

**define gofffice - added mvw 03/20/17
**xoffice=mod
goffice=xoffice

Wait window "Gathering data for Fill Rate Spreadsheet..." nowait noclear

cAcctFilter = Iif(Empty(cWhseAcct), "", iif(lower(cwhseacct)="inlist",strtran(cwhseacct,"inlist(","inlist(os."),"os."+cWhseAcct))

Select account
Locate for accountid = Val(cAcctNum)

If !Empty(cWhseAcct) and &cPnpFilter  &&only pick & pack accts
	xquery="select os.wo_num, os.wo_date, os.accountId, os.outShipId, os.ship_ref, od.totQty, "+;
		   "Iif(od.origQty>od.totQty, od.origQty-od.totQty, 000000) as cutQty, od.units, os.del_date as zDate "+;
		"from outShip os left join outDet od on od.outshipId = os.outshipId "+;
		"where "+cAcctFilter+" between(os.del_date,{"+transform(dStartDt)+"},{"+transform(dEndDt)+"}) and os.mod = '"+goffice+"'"
	xsqlexec(xquery,"csrsummary",,"wh",,,,,,,.t.)

	Select wo_num, wo_date, accountId, outShipId, ship_ref, ;
			sum(Iif(!units, totQty, 0)) as ctnQty, sum(Iif(units, totQty, 0)) as gohQty, ;
			sum(Iif(!units, cutQty, 0)) as ctnCut, sum(Iif(units, cutQty, 0)) as gohCut ;
		from csrSummary ;
		group by outShipId order by wo_num, ship_ref ;
	  into cursor csrDetail

else
	xsqlexec("select * from cut where mod='"+xoffice+"' and "+cWhseAcct+" between(cutDt,{"+dtoc(dStartDt)+"},{"+dtoc(dEndDt)+"})",,,"wh")
	
	xquery="select os.wo_num, os.wo_date, os.accountId, os.outShipId, os.ship_ref, od.totQty, "+;
		   "000000 as cutQty, od.units, os.del_date as zDate "+;
		"from outShip os left join outDet od on od.outshipId = os.outshipId "+;
		"where "+cAcctFilter+" between(os.del_date,{"+transform(dStartDt)+"},{"+transform(dEndDt)+"}) and os.mod = '"+goffice+"'"
	xsqlexec(xquery,"csrsummary",,"wh",,,,,,,.t.)

	select * from csrsummary ;
	  union all select wo_num, wo_date, accountId, outshipId, ship_ref, 000000 as totQty, cutQty, units, cutDt as zDate ;
	    from cut ;
	  into cursor csrSummary

	Select wo_num, wo_date, accountId, outShipId, ship_ref, ;
			sum(Iif(!units, totQty, 0)) as ctnQty, sum(Iif(units, totQty, 0)) as gohQty, ;
			sum(Iif(!units, cutQty, 0)) as ctnCut, sum(Iif(units, cutQty, 0)) as gohCut ;
		from csrSummary ;
		group by outShipId order by wo_num, ship_ref ;
	  into cursor csrDetail
EndIf

Locate
If Eof()
	strMsg = "No data found."
	=MessageBox(strMsg, 48, "Fill Rate")
Else
	Wait window "Creating Fill Rate Spreadsheet..." nowait noclear

	STORE tempfox+SUBSTR(SYS(2015), 3, 10)+".xls" TO cFile

	Store Iif(lDetail, "", "For .f.") to cFilter

	Copy To &cFile &cFilter type xl5

	oWorkbook = oExcel.Workbooks.Open(cFile)

	If lDetail
		oworkbook.worksheets[1].name = "Detail"
		oworkbook.worksheets[1].Range("A1:I"+Alltrim(Str(Min(Reccount()+1,65535)))).Columns.autoFit() &&65535 = max no of rows
		oworkbook.worksheets[1].Range("A1:I1").HorizontalAlignment = -4108
		oworkbook.worksheets[1].Range("A1:I1").Font.Bold = .t.

		oWorkSheet = oworkbook.worksheets.add(oworkbook.worksheets[1])
		oWorkSheet.activate()
	Else
		oworkbook.worksheets[1].Range("A1:H1").value = ""
	EndIf

	oworkbook.worksheets[1].name = "Summary"
	oworkbook.worksheets[1].columns[1].columnwidth = 25
	oworkbook.worksheets[1].range("A1:K1").merge()
	oworkbook.worksheets[1].range("A1") = "Key Performance Indicators (KPIs)"
	oworkbook.worksheets[1].Range("A1:K1").Font.Bold = .t.
	oworkbook.worksheets[1].Range("A1:K1").Font.Size = 14
	oworkbook.worksheets[1].Range("A1:K1").HorizontalAlignment = -4108
	oworkbook.worksheets[1].range("A2:K2").merge()
	oworkbook.worksheets[1].range("A2") = "Fill Rates ("+Dtoc(dStartDt)+" - "+Dtoc(dEndDt)+"): "+cAcctName+;
		" ("+Iif(cOffice="C","SP",Iif(cOffice="L","ML",Iif(cOffice="R","RI",Iif(cOffice="M", "FL", "NJ"))))+")"
	oworkbook.worksheets[1].Range("A2:K2").Font.Bold = .t.
	oworkbook.worksheets[1].Range("A2:K2").Font.Size = 12
	oworkbook.worksheets[1].Range("A2:K2").HorizontalAlignment = -4108
	oworkbook.worksheets[1].pageSetup.orientation = 2

	oworkbook.worksheets[1].range("B5:D5").merge()
	oworkbook.worksheets[1].range("B5") = "Cartons"
	oworkbook.worksheets[1].Range("B5:D5").Font.Bold = .t.
	oworkbook.worksheets[1].Range("B5:D5").HorizontalAlignment = -4108

	oworkbook.worksheets[1].range("F5:H5").merge()
	oworkbook.worksheets[1].range("F5") = "GOH"
	oworkbook.worksheets[1].Range("F5:H5").Font.Bold = .t.
	oworkbook.worksheets[1].Range("F5:H5").HorizontalAlignment = -4108

	oworkbook.worksheets[1].range("B6") = "Cut"
	oworkbook.worksheets[1].range("C6") = "Shipped"
	oworkbook.worksheets[1].range("D6") = "%"
	oworkbook.worksheets[1].range("F6") = "Cut"
	oworkbook.worksheets[1].range("G6") = "Shipped"
	oworkbook.worksheets[1].range("H6") = "%"
	oworkbook.worksheets[1].Range("B6:H6").HorizontalAlignment = -4108
	nRow = 7

	lweekly=!empty(cacctnum) and (inlist(val(cAcctNum),&gmoretacctlist) or inlist(val(cAcctNum),&gmoretacctlist2))
	Store Iif(Empty(cAcctNum), "s.accountId", iif(lweekly,"year,week","year,month")) to cGroupBy
	Store Iif(Empty(cAcctNum), "acctName", iif(lweekly,"year,week","year,month")) to cOrderBy
	Select sum(Iif(!units, totQty, 0)) as cqTot, sum(Iif(units, totQty, 0)) as gqTot, ;
		   sum(Iif(!units, cutQty, 0)) as ccTot, sum(Iif(units, cutQty, 0)) as gcTot, ;
		   acctName, (week(zdate)-week(dstartdt))+1 as week, Month(zDate) as month, Year(zDate) as year, zDate ;
		from csrSummary s left join account a on a.accountId = s.accountId ;
		group by &cGroupBy order by &cOrderBy ;
	  into cursor csrAcctTotals
	If Reccount() > 1
		Scan
		  oworkbook.worksheets[1].range("A"+Alltrim(Str(nRow))) = Iif(Empty(cAcctNum), acctName, iif(lweekly,"Week "+transform(week),Cmonth(zDate)))
		  oworkbook.worksheets[1].range("B"+Alltrim(Str(nRow))) = ccTot
		  oworkbook.worksheets[1].range("C"+Alltrim(Str(nRow))) = cqTot
*			  nRate = Iif(cqTot+ccTot = 0, 0, (cqTot/(cqTot+ccTot))*100)
		  nRate = Iif(cqTot+ccTot = 0, 0, (cqTot/(cqTot+ccTot)))
		  oworkbook.worksheets[1].range("D"+Alltrim(Str(nRow))) = Round(nRate,4)
		  oworkbook.worksheets[1].range("D"+Alltrim(Str(nRow))).numberFormat = "##0.00%"
		  oworkbook.worksheets[1].range("F"+Alltrim(Str(nRow))) = gcTot
		  oworkbook.worksheets[1].range("G"+Alltrim(Str(nRow))) = gqTot
*			  nRate = Iif(gqTot+gcTot = 0, 0, (gqTot/(gqTot+gcTot))*100)
		  nRate = Iif(gqTot+gcTot = 0, 0, (gqTot/(gqTot+gcTot)))
		  oworkbook.worksheets[1].range("H"+Alltrim(Str(nRow))) = Round(nRate,4)
		  oworkbook.worksheets[1].range("H"+Alltrim(Str(nRow))).numberFormat = "##0.00%"

		  nRow=nRow+1
		EndScan

		nRow=nRow+1
	EndIf

	Select csrDetail
	Sum ctnQty, gohQty, ctnCut, gohCut to nCtnQty, nGohQty, nCtnCut, nGohCut

	oworkbook.worksheets[1].range("A"+Alltrim(Str(nRow))+":H"+Alltrim(Str(nRow))).Font.Bold = .t.
	oworkbook.worksheets[1].range("A"+Alltrim(Str(nRow))) = "Totals"
	oworkbook.worksheets[1].range("B"+Alltrim(Str(nRow))) = nCtnCut
	oworkbook.worksheets[1].range("C"+Alltrim(Str(nRow))) = nCtnQty
*		nRate = Iif(nCtnQty+nCtnCut = 0, 0, (nCtnQty/(nCtnQty+nCtnCut))*100)
	nRate = Iif(nCtnQty+nCtnCut = 0, 0, (nCtnQty/(nCtnQty+nCtnCut)))
	oworkbook.worksheets[1].range("D"+Alltrim(Str(nRow))) = Round(nRate,4)
	oworkbook.worksheets[1].range("D"+Alltrim(Str(nRow))).numberFormat = "##0.00%"
	oworkbook.worksheets[1].range("F"+Alltrim(Str(nRow))) = nGohCut
	oworkbook.worksheets[1].range("G"+Alltrim(Str(nRow))) = nGohQty
*		nRate = Iif(nGohQty+nGohCut = 0, 0, (nGohQty/(nGohQty+nGohCut))*100)
	nRate = Iif(nGohQty+nGohCut = 0, 0, (nGohQty/(nGohQty+nGohCut)))
	oworkbook.worksheets[1].range("H"+Alltrim(Str(nRow))) = Round(nRate,4)
	oworkbook.worksheets[1].range("H"+Alltrim(Str(nRow))).numberFormat = "##0.00%"

	oWorkbook.Save()
*		oExcel.visible=.t.
EndIf

if used("outship")
	use in outship
endif
if used("outdet")
	use in outdet
endif
use in csrdetail
