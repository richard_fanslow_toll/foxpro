************************************************************************
* BUILD 856 FILES TO send to Nautica (Vanity Fair)
* AUTHOR--:PG
* REVISION DATE----:09/18/2006, Joe
* Direct-to-DC Shipments, breakout by BOL#/PO#
************************************************************************
PARAMETERS nWO_Num,cBOL,cOffice,nAcctNum
CLEAR

lTesting = .F.
DO m:\dev\prg\_setvars WITH lTesting

ON ESCAPE CANCEL
SET ASSERTS ON

lDoShipmentLevel = .T.
lUseBarcodes = .F.

PUBLIC nFilenum, tfile, tshipid,cstring,slevel,plevel,lnInterControlnum,lcInterctrlNum,stNum,gn856Ctr,lTesting,nTotqty,nMatchQty
PUBLIC nTotCtnQty,nTotCtnWt,nTotCuFt,cStreetAddressFull,cPO_Num,cOrigPO_Num,shipmentleveltotal,orderleveltotal
PUBLIC itemleveltotal,packleveltotal,dtmail,cRefString
PUBLIC cMod,cMBOL,cEDIType,lParcelType,cFilenameShort,cFilenameOut,cFilenameArch,lcPath,cWO_NumList,cISA_Num

lParcelType = .F.
lINWO = .F.
cINWOString = ""

lTestinput = .F.
lWorkFlag = .F.
lTestMail = .F.
lRunInternal = .T.

IF lTesting OR lTestinput
	lFilesOut = .F.
	lEmail = .F.
	lWrapFile = .T.
ELSE
	lFilesOut = .T.
	lEmail = .T.
	lWrapFile = .F.
ENDIF

nManCount = 0
STORE CHR(07) TO segterminator
IF TYPE('nWO_Num') = "L"
	IF lTesting OR lWorkFlag OR lRunInternal
		CLOSE DATABASES ALL
		nAcctNum = 687
		cOffice = "C"
		nWO_Num = 5000711
		cBOL = "07315170000000350"
	ELSE
		DO ediupdate WITH "MISSING WO",.T.
		RETURN
	ENDIF
ENDIF

cBOL = TRIM(cBOL)
cMBOL = ""
cEDIType = "856"

IF lTesting OR lTestinput
	cUseFolder = "F:\WHP\WHDATA\"
ELSE
	xReturn = "XXX"
	xReturn = wf(cOffice,nAcctNum)
	cUseFolder = UPPER(xReturn)
ENDIF

lFilesOut = .T.
lEmail = .T.

xsqlexec("select * from account where inactive=0","account",,"qq")
INDEX ON accountid TAG accountid
SET ORDER TO

IF(SEEK(nAcctNum,"account","accountid"))
	cGroupName = ALLTRIM(account.acctname)
	WAIT WINDOW "Group Name: "+cGroupName TIMEOUT 2
ELSE
	USE IN account
	DO ediupdate WITH "MISSING ACCOUNT #",.T.
	RETURN
ENDIF

xsqlexec("select * from inwolog where wo_num = "+ALLTRIM(TRANSFORM(nWO_Num)),"inwolog")

IF lUseBarcodes
	USE F:\transload\tldata\barcodes IN 0 ALIAS barcodes NOUPDATE ORDER TAG barcodeno
ENDIF

IF lUseBarcodes
	SELECT 0
	USE F:\Nautica\DATA\nbcodes ALIAS nbcodes
	WAIT WINDOW "Now updating NBCODES table with BARCODE Trk WO#s" NOWAIT
	UPDATE nbcodes ;
		SET nbcodes.trkwo = barcodes.wo_num ;
		WHERE nbcodes.barcode = barcodes.barcodeno ;
		AND EMPTY(nbcodes.barcode)

	SELECT trkwo FROM nbcodes GROUP BY 1 INTO CURSOR nbtrkwo
	SELECT nbtrkwo
	WAIT WINDOW "Now updating NBCODES table with INWOLOG WO#s" NOWAIT
	SCAN
		SELECT inwolog
		LOCATE FOR accountid = nAcctNum AND ALLTRIM(STR(nbtrkwo.trkwo))$truckwonum
		IF lTesting OR lTestinput
			REPLACE ALL nbcodes.inwo WITH 999999,nbcodes.indate WITH DATE() FOR nbcodes.trkwo = nbtrkwo.trkwo
			cINWOString = "999999"
		ELSE
			IF FOUND()
				SELECT nbcodes
				LOCATE
				REPLACE ALL nbcodes.inwo WITH inwolog.wo_num,nbcodes.indate WITH inwolog.wo_date FOR nbcodes.trkwo = nbtrkwo.trkwo
			ELSE
				ediupdate("MISS INWONUM",.T.)
				RETURN
			ENDIF
		ENDIF
		SELECT nbtrkwo
	ENDSCAN
	USE IN inwolog
	USE IN nbtrkwo
	USE IN nbcodes
ENDIF
WAIT CLEAR

SELECT 0
USE F:\Nautica\DATA\nbcodes ALIAS nbcodes NOUPDATE
LOCATE

xsqlexec("select * from outship where accountid ="+TRANSFORM(nAcctNum)+" and bol_no '"+cBOL+"'",,,"wh")

csq1 = [select distinct outshipid from outship]
xsqlexec(csq1,"oslist",,"wh")

IF RECCOUNT("oslist")>0
	LOCATE
	xjfilter="outshipid in ("
	SCAN
		xjfilter=xjfilter+TRANSFORM(outshipid)+","
	ENDSCAN
	xjfilter=LEFT(xjfilter,LEN(xjfilter)-1)+")"
ELSE
	xjfilter="1=0"
ENDIF

xsqlexec("select * from outdet where "+xjfilter,,,"wh")
USE IN oslist

USE F:\merch\DATA\controlnum IN 0 ALIAS controlnum

dtmail = TTOC(DATETIME())
cRefString = PADR("FMI WO#",15)+PADR("CUST PO#",20)
tsendto = "Marian_Meeks@vfc.com"
tfrom = "TOLL Warehouse Operations <toll-warehouse-ops@tollgroup.com>"
tcc = "joe.bianchi@tollgroup.com"

STORE "" TO fstring
STORE 0 TO shiplevel,gn856Ctr,stNum,lnCTTCount,lnSECount
STORE 0 TO shipmentleveltotal,orderleveltotal,itemleveltotal,packleveltotal
STORE 0 TO hlcount2,hlpack,spcntp,hlsegcnt,segcnt,spcnt,spcnt2,hlitem,spcnti,lc_hlcount,a,lincnt, totlincnt
STORE 0 TO sesegcnt,segcnt,nTotqty,nMatchQty,nTotCtnQty,nTotCtnWt,nTotCuFt

STORE "" TO workordersprocessed,summaryinfo,lc_856num,lc_cnum
STORE "" TO cShiptoName,cShiptoAddr1,cShiptoAddr2,cShiptoCity,cShiptoState,cShiptoZip,cStreetAddressFull

* top level counter for unique HL segment counts
STORE 0 TO hlctr

* initiate overall counters
STORE 0 TO shipmentlevelcount,orderlevelcount
STORE 0 TO packlevelcount,itemlevelcount

* initiate session overall counters
STORE 0 TO thisshipmentlevelcount,thisorderlevelcount
STORE 0 TO thispacklevelcount,thisitemlevelcount

* initiate the value of the curent count for remembering the Parent HL Count
STORE 0 TO currentshipmentlevelcount,currentorderlevelcount
STORE 0 TO currentpacklevelcount,currentitemlevelcount

STORE .F. TO llEditUpdate

lcstring  = ""
lincnt = 1
*******************************************************************************************
jtimec = TIME()
STORE SUBSTR(jtimec,1,2) TO j1
STORE SUBSTR(jtimec,4,2) TO j2
STORE SUBSTR(jtimec,7,2) TO j3
lcTime = j1+j2
dShortDate = RIGHT(DTOS(DATE()),6)
dLongDate = DTOS(DATE())
***************************
*** Build Header Record ***
***************************
*!* For the GS Segment
tsendid     = '2122452222'
tsendidlong = PADR(tsendid,15," ")
tsendqual   = '11'
******************************
trecid      = '2122441111'
trecidlong  = PADR(trecid,15," ")
trecqual  = '12'

*******************************************************************************************
WAIT WINDOW "Setting up Shipped 856........." NOWAIT

IF lWrapFile
	cFilenameHold = ("f:\ftpusers\NAUTICA\856-staging\856FMI"+TTOC(DATETIME(),1)+".EDI")
ELSE
	cFilenameHold = ("f:\ftpusers\NAUTICA\856-staging\856FMI"+TTOC(DATETIME(),1)+".EDI")
ENDIF
nFilenum = FCREATE(cFilenameHold)
cFileStem = JUSTFNAME(cFilenameHold)
cFilenameShort = cFileStem
lcPath = ("f:\ftpusers\NAUTICA\856out\")
cFilenameOut = (lcPath+cFileStem)
cFilenameArch = ("f:\ftpusers\NAUTICA\856out\archive\"+cFileStem)

SELECT outship
*SET ORDER TO TAG bol_no

**********************************************
********  S H I P M E N T  L E V E L *********
**********************************************

IF nFilenum < 0
	=FCLOSE(nFilenum)
	CLOSE ALL
	RETURN
ENDIF

WriteHeader()

SELECT outship
LOCATE
SELECT 0
SELECT cnee_ref,ctnqty,weight,cuft ;
	FROM outship ;
	WHERE bol_no = cBOL ;
	GROUP BY cnee_ref ;
	INTO CURSOR POCtns

SELECT outship
LOCATE
SUM ctnqty TO nTotCtnQty FOR bol_no = cBOL AND accountid = nAcctNum
LOCATE
SUM weight TO nTotCtnWt FOR bol_no = cBOL AND accountid = nAcctNum
LOCATE
SUM cuft TO nTotCuFt FOR bol_no = cBOL AND accountid = nAcctNum
LOCATE

WAIT WINDOW "Now scanning Outship" NOWAIT
SCAN FOR bol_no = cBOL
*!* Added to handle potential split PO shipments
	IF ("INWO*"$outship.shipins)
		lINWO = .T.
		cINWOString = ""
		ALINES(aryinwo,outship.shipins,1)
		len1 = ALEN(aryinwo,1)
		FOR z1 = 1 TO len1
			cAryString = ALLTRIM(aryinwo[z1])
			IF !"INWO*"$cAryString
				LOOP
			ENDIF
			cAryString=ALLTRIM(SUBSTR(cAryString,AT("*",cAryString)+1))
			cINWOString = IIF(EMPTY(cINWOString),cAryString,cINWOString+","+cAryString)
		ENDFOR
	ENDIF

	cShiptoName = TRIM(outship.CONSIGNEE)
	cShiptoAddr1 = TRIM(outship.address)
	nSpaces = OCCURS(" ",ALLTRIM(outship.csz))
	cShiptoCity = LEFT(outship.csz,AT(",",outship.csz,1)-1)
	cShiptoState = ALLTRIM(SUBSTR(outship.csz,AT(",",outship.csz,1)+2,2))
	cShiptoZip = ALLTRIM(SUBSTR(csz,AT(" ",outship.csz,nSpaces)+1))

	cStorenum = TRIM(outship.dcnum)
	IF EMPTY(cStorenum) AND !lTesting
		ediupdate("MISS STORENUM",.T.)
		RETURN
	ENDIF

	nWO_Num = outship.wo_num
	cWO_Num = ALLTRIM(STR(nWO_Num))
	cWO_NumList = IIF(EMPTY(cWO_NumList),cWO_Num,cWO_NumList+CHR(13)+cWO_Num)
	nOutshipid = outship.outshipid
	cShipdate = DTOS(outship.del_date)
	IF EMPTY(cShipdate)
		IF !lTesting AND DATE()>{^2007-11-13}
			ediupdate("MISS DEL_DATE",.T.)
			RETURN
		ELSE
			cShipdate = DTOS(DATE())
		ENDIF
	ENDIF

	cApptDate = DTOS(outship.appt)
	IF EMPTY(cApptDate)
		IF DATE()<{^2006-10-27}
			ediupdate("MISS APPT",.T.)
			RETURN
		ELSE
			cApptDate = cShipdate
		ENDIF
	ENDIF

	cAppt_num = ALLTRIM(outship.appt_num)
	cShip_ref = ALLTRIM(outship.ship_ref)
	cCNEE_Ref = ALLTRIM(outship.cnee_ref)
	cShipFor = ALLT(outship.shipfor)
	cSforstore =  ALLT(outship.sforstore)
	cRefString = cRefString+CHR(10)+PADR(cWO_Num,15)+PADR(cCNEE_Ref,20)
	SELECT outship
	nRec1 = RECNO()
	lSpecBOL = .F.
*	ASSERT .f. message "At outship to DET1 select"
	IF INLIST(TRIM(outship.bol_no),"07315170000000343","07315170000000350")
		lSpecBOL = .T.
		SELECT RIGHT(ALLTRIM(bol_no),3) AS bol,cnee_ref AS PO_Num,ship_ref,SUM(outship.cuft) AS POCube, ;
			SUM(outship.qty) AS POTotQty, SUM(outship.weight) AS POWeight ;
			FROM outship WHERE outship.outshipid = nOutshipid INTO CURSOR det1 READWRITE GROUP BY 1,2,3
	ELSE
		SELECT cnee_ref AS PO_Num,ship_ref,SUM(outship.cuft) AS POCube, ;
			SUM(outship.qty) AS POTotQty, SUM(outship.weight) AS POWeight ;
			FROM outship WHERE outship.outshipid = nOutshipid INTO CURSOR det1 READWRITE GROUP BY 1,2
	ENDIF
	SELECT outship
	GO nRec1

	SELECT det1
*	brow
	IF lTesting
		IF POCube=0
			REPLACE ALL POCube WITH (2*outship.qty)
		ENDIF
		IF POWeight=0
			REPLACE ALL POWeight WITH (3*outship.qty)
		ENDIF
	ENDIF
	LOCATE
	WAIT WINDOW "Now scanning DET1 cursor for measurements" NOWAIT
	COPY TO F:\3pl\data\nautica_det1
	SCAN  && Scanning DET1
*		ASSERT .F. MESSAGE "At DET1"
		IF lSpecBOL
			cBOLSnip = ALLTRIM(det1.bol)
			cShip_ref = ALLTRIM(det1.ship_ref)
		ELSE
			cShip_ref = (det1.ship_ref)
		ENDIF
		cOrigPO_Num = TRIM(det1.PO_Num)

*		WAIT WINDOW "Using PO # "+cOrigPO_num+" as Original PO"
		SELECT POCtns
		LOCATE FOR POCtns.cnee_ref = cOrigPO_Num
		IF FOUND()
			nPOCtnQty = POCtns.ctnqty
			nPOWeight = POCtns.weight
			nPOCube = POCtns.cuft
		ELSE
			WAIT WINDOW "POCtns CNEE_REF of "+cOrigPO_Num+" not found!"
		ENDIF

		SELECT nbcodes
		IF !lUseBarcodes
			lINWO = .F.
		ENDIF
		ASSERT .F. MESSAGE "At selection statement for NBCODES"
		DO CASE
			CASE lINWO
				scanstr = "nbcodes.cust_po = cOrigPO_Num and INLIST(nbcodes.INWO,&cINWOString)"
				WAIT WINDOW "Scanning for WO#'s "+cINWOString NOWAIT
			CASE lSpecBOL
				scanstr = "nbcodes.cust_po = cOrigPO_Num and ALLTRIM(nbcodes.bol) = cBOLSnip and nbcodes.shipmentid = cShip_ref"
			OTHERWISE
				scanstr = "nbcodes.cust_po = cOrigPO_Num and nbcodes.shipmentid = cShip_ref"
		ENDCASE

		LOCATE
		IF lUseBarcodes
			WAIT WINDOW "Now scanning NBCODES vs. TLDATA for complete barcoding" NOWAIT
			SCAN FOR &scanstr
				SELECT barcodes
				IF !SEEK(nbcodes.barcode)
					WAIT WINDOW "Missing Barcode in TLDATA: "+nbcodes.barcode TIMEOUT 2
				ENDIF
				SELECT nbcodes
			ENDSCAN
			LOCATE
		ENDIF

		SELECT bol,cust_po,Import_PO,upc,STYLE ;
			FROM nbcodes ;
			WHERE &scanstr ;
			INTO CURSOR temp1nt READWRITE ;
			GROUP BY 1,2,3,4
		IF lSpecBOL
			DELETE FOR EMPTY(ALLTRIM(bol))
		ENDIF
*		brow
		COPY TO F:\joeb\temp\temp1nt
*		WAIT WINDOW "Check temp1nt file in F:\Joeb\Temp"
		LOCATE
		WAIT WINDOW "Now Processing PO (Det Style)#... "+cOrigPO_Num NOWAIT
		SELECT nbcodes
		LOCATE

		IF lDoShipmentLevel
			shipmentheader(ALLTRIM(STR(nWO_Num)))
			SELECT nbcodes
			STORE 3 TO segcnt
			shipment_level()  && write out the shipment level info to the flat file
			lDoShipmentLevel = .F.
		ENDIF

********** O R D E R   L E V E L ****************************

		SELECT temp1nt
		LOCATE
		cPO_Numx = ""
		COPY TO m:\joeb\temp\nautica_temp1nt

		SCAN
			cPO_Num = ALLTRIM(temp1nt.cust_po)
			cImport_PO = ALLTRIM(temp1nt.Import_PO)
			IF lSpecBOL
				cBOLSnip = ALLTRIM(temp1nt.bol)
			ENDIF
			WAIT WINDOW "Using PO# "+cPO_Num NOWAIT

			IF lUseBarcodes
				scanstr = "nbcodes.cust_po = cPO_Num and INLIST(nbcodes.INWO,&cINWOString)"
			ELSE
*!*					IF DATE()>={^2007-10-05}
*!*					scanstr = "nbcodes.IMPORT_PO = cPO_Num"
*!*					ELSE
				IF lSpecBOL
					scanstr = "nbcodes.cust_po = cPO_Num AND nbcodes.bol = cBOLSnip AND nbcodes.shipmentid = cShip_ref"
				ELSE
					scanstr = "nbcodes.cust_po = cPO_Num AND nbcodes.shipmentid = cShip_ref"
				ENDIF
*!*					ENDIF
			ENDIF

			cUPC = temp1nt.upc
			cStyle = temp1nt.STYLE

			SELECT nbcodes
			SUM(INT(VAL(PACK))) TO nStyleQty FOR &scanstr AND ;
				nbcodes.upc = cUPC AND nbcodes.STYLE = cStyle

			LOCATE FOR &scanstr AND nbcodes.upc = cUPC

			IF cPO_Numx<> cPO_Num
				order_level(cPO_Num)
				STORE cPO_Num TO cPO_Numx
			ENDIF

			SUM INT(VAL(nbcodes.PACK)) TO nUPCQty FOR &scanstr AND nbcodes.upc = cUPC
			SELECT nbcodes
			LOCATE
			LOCATE FOR &scanstr AND nbcodes.upc = cUPC

***************** I T E M  L E V E L ****************************************
			item_level()
***************** P A C K  L E V E L ****************************************
			LOCATE FOR &scanstr AND nbcodes.upc = cUPC
			SCAN FOR &scanstr AND nbcodes.upc = cUPC
				pack_level()
			ENDSCAN
			SELECT temp1nt
		ENDSCAN
		SELECT det1
	ENDSCAN
ENDSCAN

SELECT outship
STORE "CTT*"+ALLTRIM(STR(lnCTTCount,6,0))+segterminator TO cstring
fputstring(nFilenum,cstring)
lcCtr = ALLTRIM(STR(totlincnt))
lincnt = 1
STORE 0 TO lnCTTCount
lc856Ctr = ALLTRIM(STR(gn856Ctr))
lcSegCtr = ALLTRIM(STR(segcnt+1))

STORE "SE*"+lcSegCtr+"*"+stNum+PADL(lc856Ctr,4,"0")+segterminator TO cstring
fputstring(nFilenum,cstring)

STORE "" TO workordersprocessed
STORE 0 TO thisshipmentlevelcount,thisorderlevelcount
STORE 0 TO thispacklevelcount,thisitemlevelcount

lc856Ctr = ALLTRIM(STR(gn856Ctr))
STORE "GE*"+lc856Ctr+"*"+stNum+segterminator TO cstring
fputstring(nFilenum,cstring)

STORE "IEA*1*"+lcInterctrlNum+segterminator TO cstring
fputstring(nFilenum,cstring)


SET CENTURY OFF
SET DATE TO american

IF lTesting
	cShipMsg  =  '  Shipment (PO level) count: '
	cOrderMsg= '  Order (UPC level) count: '
	cItemMsg  = '  Item (Style level) count: '
	cPackMsg  = '  Pack (Carton level) count: '
	cShipTot   = ALLTRIM(STR(shipmentleveltotal))
	cOrderTot = ALLTRIM(STR(orderleveltotal))
	cItemTot   = ALLTRIM(STR(itemleveltotal))
	cPackTot   = ALLTRIM(STR(packleveltotal))
*	MESSAGEBOX(REPLICATE('.',45)+CHR(13)+;
cShipMsg+cShipTot+CHR(13)+;
cOrderMsg+cOrderTot+CHR(13)+;
cItemMsg+cItemTot+CHR(13)+;
cPackMsg+cPackTot+CHR(13)+;
REPLICATE('.',45),48,'Nautica 856 Builder')
ENDIF
*WAIT WINDOW "Total Cartons added: "+ALLTRIM(STR(nManCount))

DO ediupdate WITH "856 CREATED",.F.
WAIT CLEAR
WAIT WINDOW "Nautica 856 Process complete..." TIMEOUT 2
asn_out_data()

RETURN

******************************************************
* P A C K     L E V E L
******************************************************
PROC pack_level
	hlctr = hlctr +1
	packleveltotal = packleveltotal + 1
	packlevelcount = packlevelcount+1
	thispacklevelcount = thispacklevelcount+1
	currentpacklevelcount = hlctr
	lc_hlcount = ALLTRIM(getnum(hlctr))								&& overall HL counter
	lc_parentcount = ALLTRIM(getnum(currentitemlevelcount))				&& the parent

	STORE "HL*"+lc_hlcount+"*"+lc_parentcount+"*"+"P"+segterminator TO cstring
	fputstring(nFilenum,cstring)
	segcnt=segcnt+1
	hlsegcnt=hlsegcnt+1

* count the line items for the CTT segment counter
	lnCTTCount = lnCTTCount +1

	STORE "SN1**"+ALLTRIM(nbcodes.PACK)+"*EA"+segterminator TO cstring
	fputstring(nFilenum,cstring)
	segcnt=segcnt+1

** MAN Segment (Carton barcode #)
	nManCount = nManCount+1
	STORE "MAN*GM*"+ALLTRIM(nbcodes.barcode)+segterminator TO cstring
	fputstring(nFilenum,cstring)
	segcnt=segcnt+1

ENDPROC

******************************************************
* I T E M     L E V E L
******************************************************
PROC item_level
	lincnt = lincnt + 1
	hlctr = hlctr +1
	itemleveltotal = itemleveltotal + 1
	itemlevelcount = itemlevelcount+1
	thisitemlevelcount = thisitemlevelcount+1
	currentitemlevelcount = hlctr
	lc_hlcount = ALLTRIM(getnum(hlctr)) 						&& overall HL counter
	lc_parentcount = ALLTRIM(getnum(currentorderlevelcount))		&& the parent


	STORE "HL*"+lc_hlcount+"*"+lc_parentcount+"*I"+segterminator TO cstring
	fputstring(nFilenum,cstring)
	segcnt=segcnt+1
	hlsegcnt=hlsegcnt+1

	STORE "LIN**UP*"+cUPC+"*VA*"+ALLTRIM(nbcodes.STYLE)+segterminator TO cstring
	fputstring(nFilenum,cstring)
	segcnt=segcnt+1

	STORE "SN1**"+ALLTRIM(STR(nStyleQty))+"*EA"+segterminator TO cstring
	fputstring(nFilenum,cstring)
	segcnt=segcnt+1

ENDPROC
******************************************************
* O R D E R    L E V E L
******************************************************
PROC order_level
	PARAMETER lcDelloc
	orderleveltotal = orderleveltotal + 1
	hlctr = hlctr +1
	orderlevelcount = orderlevelcount+1
	thisorderlevelcount = thisorderlevelcount+1
	lc_hlcount = getnum(hlctr)  && overall HL counter
	lc_parentcount = getnum(currentshipmentlevelcount)  && the parent
	currentorderlevelcount = hlctr

	STORE "HL*"+ALLTRIM(lc_hlcount)+"*"+ALLTRIM(lc_parentcount)+"*O"+segterminator TO cstring
	fputstring(nFilenum,cstring)
	segcnt=segcnt+1
	hlsegcnt=hlsegcnt+1

** PRF (PO#, as Outdet.style field)
	IF DATE()>={^2007-09-25}
		STORE "PRF*"+ALLTRIM(cImport_PO)+segterminator TO cstring
	ELSE
		STORE "PRF*"+ALLTRIM(cPO_Num)+segterminator TO cstring
	ENDIF
	fputstring(nFilenum,cstring)
	segcnt=segcnt+1

**TOTAL CTNS/WEIGHT/CUBE FOR PO

	STORE "TD1*CTN25*"+ALLTRIM(STR(nPOCtnQty))+"****G*"+ALLTRIM(STR(nPOWeight))+"*LB*"+;
		ALLTRIM(STR(nPOCube))+"*CO"+segterminator TO cstring
	fputstring(nFilenum,cstring)
	segcnt=segcnt+1

	IF DATE()>={^2007-09-25}
		STORE "REF*PO*"+ALLTRIM(cPO_Num)+segterminator TO cstring
	ELSE
		STORE "REF*PO*"+ALLTRIM(cImport_PO)+segterminator TO cstring
	ENDIF
	fputstring(nFilenum,cstring)
	segcnt=segcnt+1

ENDPROC

*******************************************
*************END OF ORDER LEVEL************
*******************************************

******************************************************
* S H I P M E N T     L E V E L
******************************************************
PROC shipment_level
	shipmentleveltotal = shipmentleveltotal + 1
* reset all at each shipment level
	STORE 0 TO shipmentlevelcount
	STORE 0 TO orderlevelcount
	STORE 0 TO packlevelcount
	STORE 0 TO itemlevelcount
	STORE 0 TO hlctr

	shipmentlevelcount = shipmentlevelcount +1
	thisshipmentlevelcount = thisshipmentlevelcount +1
	hlctr = hlctr +1
	currentshipmentlevelcount = hlctr
	lc_hlcount = ALLTRIM(getnum(hlctr))
	STORE "HL*"+lc_hlcount+"**S"+segterminator TO cstring
	fputstring(nFilenum,cstring)
	segcnt=segcnt+1
	hlsegcnt=hlsegcnt+1

** TD1 - (TOTAL CTNS/WEIGHT/CUBE)
	IF nTotCtnWt=0 AND lTesting
		nTotCtnWt = nTotCtnQty*3*outship.qty
		nTotCuFt = nTotCtnQty*2
	ENDIF
	STORE "TD1*CTN25*"+ALLTRIM(STR(nTotCtnQty))+"****G*"+ALLTRIM(STR(nTotCtnWt))+"*LB*"+ALLTRIM(STR(nTotCuFt))+;
		"*CO"+segterminator TO cstring
	fputstring(nFilenum,cstring)
	segcnt=segcnt+1

** TD5 - Hardcoded
	STORE "TD5*B*2*FMIE*M*FMI INTERNATIONAL"+segterminator TO cstring
	fputstring(nFilenum,cstring)
	segcnt=segcnt+1

** TD3 - Hardcoded until Trailer info is captured in WMS
	lcTrailerNumber= "FMIUNK"  &&  value must default to this
	STORE "TD3*CN**"+ALLTRIM(lcTrailerNumber)+segterminator TO cstring
	fputstring(nFilenum,cstring)
	segcnt=segcnt+1

	STORE "REF*BM*"+cBOL+"*FMI TRIP NUMBER"+segterminator   TO cstring  && FMI BOL#
	fputstring(nFilenum,cstring)
	segcnt=segcnt+1

	STORE "REF*AO*"+cAppt_num+"*LOAD ID"+segterminator   TO cstring  && FMI APPT#
	fputstring(nFilenum,cstring)
	segcnt=segcnt+1

	STORE "DTM*011*"+ALLTRIM(cShipdate)+segterminator TO cstring  && Ship Date (Del_date)
	fputstring(nFilenum,cstring)
	segcnt=segcnt+1

	STORE "DTM*371*"+ALLTRIM(cApptDate)+segterminator TO cstring  && Appt Date
	fputstring(nFilenum,cstring)
	segcnt=segcnt+1

** N1--SHIP FROM
	STORE "N1*SF*FMI INTERNATIONAL*ZZ*FMIE"+segterminator TO cstring
	fputstring(nFilenum,cstring)
	segcnt=segcnt+1

** N3--SHIP FROM ADDRESS
	STORE "N3*450 WESTMONT DRIVE"+segterminator TO cstring
	fputstring(nFilenum,cstring)
	segcnt=segcnt+1

** N4--SHIP FROM CSZ
	STORE "N4*SAN PEDRO*CA*90731"+segterminator TO cstring
	fputstring(nFilenum,cstring)
	segcnt=segcnt+1

** N1--SHIP TO
	STORE "N1*ST*"+cShiptoName+"*92*"+cStorenum+segterminator TO cstring
	fputstring(nFilenum,cstring)
	segcnt=segcnt+1

	cStreetAddressFull = IIF(EMPTY(cShiptoAddr2),cShiptoAddr1,cShiptoAddr1+"*"+cShiptoAddr2)
	STORE "N3*"+cStreetAddressFull+segterminator TO cstring
	fputstring(nFilenum,cstring)
	segcnt=segcnt+1

	STORE "N4*"+cShiptoCity+"*"+cShiptoState+"*"+cShiptoZip+segterminator TO cstring
	fputstring(nFilenum,cstring)
	segcnt=segcnt+1

	RETURN
ENDPROC
********************************************
********** END OF SHIPMENT LEVEL************
********************************************

******************************************************
PROCEDURE shipmentheader
******************************************************
	PARAMETERS tshipid

	gn856Ctr = gn856Ctr +1
	STORE "ST*856*"+stNum+PADL(ALLTRIM(STR(gn856Ctr)),4,"0")+segterminator TO cstring
	fputstring(nFilenum,cstring)

	SELECT controlnum
	IF llEditUpdate = .T.
		lc_cnum = PADL(ALLTRIM(STR(controlnum.editbsn)),5,"0")
		REPLACE controlnum.editbsn WITH controlnum.editbsn + 1
	ELSE
		lc_cnum = PADL(ALLTRIM(STR(controlnum.bsnseg)),5,"0")
		REPLACE controlnum.bsnseg WITH controlnum.bsnseg + 1
	ENDIF

	tshipid = PADR('FMILA'+SUBSTR(lc_cnum,1,25),30)

	IF llEditUpdate = .T.
		STORE "BSN*05*"+ALLTRIM(tshipid)+"*"+dLongDate+"*"+j1+j2+"*0002*OR"+segterminator TO cstring
	ELSE
		STORE "BSN*00*"+ALLTRIM(tshipid)+"*"+dLongDate+"*"+j1+j2+"*0002*OR"+segterminator TO cstring
	ENDIF
	fputstring(nFilenum,cstring)

	totlincnt = 0

ENDPROC
******************************************************
PROCEDURE WriteHeader
******************************************************
* get the next unique control number
	SELECT controlnum
	stNum = ALLTRIM(STR(controlnum.cntrl856))
	lc_856num =stNum
	REPLACE controlnum.cntrl856 WITH controlnum.cntrl856 + 1
	lnInterControlnum = controlnum.interctrl
	REPLACE controlnum.interctrl WITH controlnum.interctrl + 1
	lcInterctrlNum = PADL(ALLTRIM(STR(lnInterControlnum)),9,"0")
	cISA_Num = lcInterctrlNum

*  write out the 856 header
	IF lTesting
		cISA_Type = "T"
	ELSE
		cISA_Type = "P"
	ENDIF

	STORE "ISA*00*          *00*          *"+tsendqual+"*"+tsendidlong+"*"+trecqual+"*"+trecidlong+"*"+dShortDate+"*"+lcTime+;
		"*U*00403*"+lcInterctrlNum +"*0*"+cISA_Type+"*;"+segterminator TO cstring
	fputstring(nFilenum,cstring)

	STORE "GS*SH*"+tsendid+"*"+trecid+"*"+dLongDate+"*"+lcTime+;
		"*"+stNum+"*X*004030"+segterminator TO cstring
	fputstring(nFilenum,cstring)
ENDPROC

******************************************************
FUNCTION getnum
******************************************************
	PARAM plcount
	fidnum = IIF(BETWEEN(plcount,1,9),SPACE(11)+LTRIM(STR(plcount)),;
		IIF(BETWEEN(plcount,10,99),SPACE(10)+LTRIM(STR(plcount)),;
		IIF(BETWEEN(plcount,100,999),SPACE(9)+LTRIM(STR(plcount)),;
		IIF(BETWEEN(plcount,1000,9999),SPACE(8)+LTRIM(STR(plcount)),;
		IIF(BETWEEN(plcount,10000,99999),SPACE(7)+LTRIM(STR(plcount))," ")))))
	RETURN fidnum

******************************************************
PROCEDURE fputstring
******************************************************
	PARAM filehandle,filedata
	IF lWrapFile
		FPUTS(filehandle,filedata)
	ELSE
		FWRITE(filehandle,filedata)
	ENDIF
ENDPROC

******************************************************
PROCEDURE CheckEmpty
******************************************************
	PARAMETER lcDataValue
	RETURN IIF(EMPTY(lcDataValue),"UNKNOWN",lcDataValue)
ENDPROC

******************************************************
PROCEDURE RevertTables
******************************************************
	TABLEREVERT(.T.,"controlnum")
	TABLEREVERT(.T.,"a856info")
	TABLEREVERT(.T.,"bkdnWO")
ENDPROC

******************************************************
PROCEDURE ediupdate
******************************************************
	PARAMETERS cMsgStr,lIsError
	IF lIsError
		WAIT WINDOW "Error: "+cMsgStr TIMEOUT 5
		ASSERT .F. MESSAGE "In EDIUPDATE - ERROR"
	ENDIF

	IF USED('edi_trigger')
		SELECT edi_trigger
		LOCATE
		IF lIsError
			IF lParcelType
				REPLACE processed WITH .T.,proc856 WITH .F.,file856 WITH "",;
					fin_status WITH cStatus,errorflag WITH .T. ;
					FOR edi_trigger.wo_num = nWO_Num AND accountid = nAcctNum
			ELSE
				REPLACE processed WITH .T.,proc856 WITH .F.,file856 WITH "",;
					fin_status WITH cStatus,errorflag WITH .T. ;
					FOR edi_trigger.bol = cBOL AND accountid = nAcctNum
			ENDIF
			=FCLOSE(nFilenum)
			ERASE &cFilenameHold
		ELSE
			IF lParcelType
				REPLACE processed WITH .T.,proc856 WITH .T.,file856 WITH cFilenameHold,;
					fin_status WITH "856 CREATED",errorflag WITH .F.,when_proc WITH DATETIME() ;
					FOR edi_trigger.wo_num = nWO_Num AND accountid = nAcctNum
			ELSE
				REPLACE processed WITH .T.,proc856 WITH .T.,file856 WITH cFilenameHold,;
					fin_status WITH "856 CREATED",errorflag WITH .F.,when_proc WITH DATETIME() ;
					FOR edi_trigger.bol = cBOL AND accountid = nAcctNum
			ENDIF
		ENDIF
	ENDIF

	IF lIsError
*!* Create eMail error message
		IF lParcelType
			tsubject = "856 Error in "+cGroupName+" UPS WO# "+TRIM(cWO_Num)+"(At PT "+cShip_ref+")"
		ELSE
			tsubject = "856 Error in "+cGroupName+" BOL "+TRIM(cBOL)+"(At PT "+cShip_ref+")"
		ENDIF
		tattach = " "
		tsendto = "joe.bianchi@tollgroup.com"
		tcc = " "
		IF lParcelType
			tmessage = "856 Processing for UPS WO# "+cWO_Num+" (Office: "+cOffice+") produced this error: "+cStatus
		ELSE
			tmessage = "856 Processing for BOL# "+cBOL+", WO# "+cWO_Num+" (Office: "+cOffice+") produced this error: "+cStatus
		ENDIF

		IF "MISS PT-PICKPACK"$cStatus
			tmessage = CHR(10) +CHR(10)+ tmessage + CHR(10) + "Either the WO has not been moved from Labels to Cartons or the pickticket was never scanned. Please advise."
		ELSE
			tmessage = tmessage+CHR(10)+"Reset EDI_TRIGGER to re-run."
		ENDIF

		IF lEmail
			DO FORM m:\dev\frm\FORMS\dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
		ENDIF

	ELSE

		IF lTestMail
			tsendto = "joe.bianchi@tollgroup.com"
			tcc = " "
		ENDIF

*!* Create eMail confirmation message
		tsubject = "856 EDI File from TGF ("+cGroupName+")"
		tattach = " "
		IF lParcelType
			tmessage = "856 EDI Info from TGF for "+cGroupName+", UPS WO# "+TRIM(cWO_Num)+CHR(10)
		ELSE
			tmessage = "856 EDI Info from TGF, for division "+cGroupName+", BOL# "+TRIM(cBOL)+CHR(10)
		ENDIF
		tmessage = tmessage + "containing these orders :"+CHR(10)+CHR(10)
		tmessage = tmessage + cRefString+CHR(10)+CHR(10)
		tmessage = tmessage +"has been created and placed into folder 856OUT."+CHR(10)+CHR(10)
		IF !lTestMail AND !lTesting
			tmessage = tmessage + "If you have any questions, please eMail or call 732-750-9000 x217."
		ENDIF
		IF lTesting OR lTestinput
			tmessage = tmessage + CHR(10)+"This is TEST DATA only...Nautica: Disregard"
		ENDIF
		IF lWorkFlag
			tmessage = tmessage + CHR(10)+"This is a RESEND of the 856 DATA..."
		ENDIF
		IF lEmail
			DO FORM m:\dev\frm\dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
		ENDIF
	ENDIF

*!* Transfers files to correct output folders
	=FCLOSE(nFilenum)
*USE IN [&cFilenameArch]
	COPY FILE &cFilenameHold TO &cFilenameArch
	IF lFilesOut
		COPY FILE &cFilenameHold TO &cFilenameOut
		DELETE FILE &cFilenameHold
	ENDIF

	WAIT CLEAR
	WAIT WINDOW cGroupName+" 856 EDI File output complete" TIMEOUT 1

	CLOSETABLES()
	RETURN

******************************************************
PROCEDURE CLOSETABLES
******************************************************
	=FCLOSE(nFilenum)

	IF USED('outship')
		USE IN outship
	ENDIF
	IF USED('outdet')
		USE IN outdet
	ENDIF
	IF USED('controlnum')
		USE IN controlnum
	ENDIF
	IF USED('nbcodes')
		USE IN nbcodes
	ENDIF
	IF USED('account')
		USE IN account
	ENDIF
	IF USED('barcodes')
		USE IN barcodes
	ENDIF
ENDPROC

