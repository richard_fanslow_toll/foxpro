* make exe in F:\UTIL\MLAUTOBILLING\
* 
*******************************************************************************************************
* Process SEARS pm data to produce a bill.
*
*******************************************************************************************************
LPARAMETERS tcMode

*utilsetup("LOADSEARS")

* this will load ML billing data, WITHOUT deleting previously loaded data that fals within the date range for the new data.
* so this should only be used for special cases, as when Doug drops a set of records that were rejected in previous loads.

LOCAL loSEARSProc

_screen.Caption = "Loading ML Auto Bill Data..."

SET PROCEDURE TO M:\DEV\PRG\SEARSCLASS

loSEARSProc = CREATEOBJECT('SEARS')

IF NOT EMPTY(tcMode) AND UPPER(ALLTRIM(tcMode)) = "AUTO" THEN
	loSEARSProc.SetAutoMode()
ENDIF

loSEARSProc.SetClient("KMART FOOTWEAR")

* tell load not to delete any previously loaded data...
loSEARSProc.SetLoadingDeleteByDateRangeOff()

* go
loSEARSProc.Load()

*schedupdate()

RETURN
