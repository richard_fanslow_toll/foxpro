Procedure parse_po

done = .F.

Do While !done

  GotoTag("<Orders")

  GotoTag("<Order>")
  m.ponum=getfield(field1,[Order>],"<")

  GotoTag("<SoldTo")
  GotoTag("<ID")    && get Hawb
  m.soldtoID=getfield(field1,[ID>],"<")

  GotoTag("<ShipTo")
  GotoTag("<ID")    && get Hawb
  m.shiptoID=getfield(field1,[ID>],"<")


  GotoTag("<Name")    && get Hawb
  m.name=getfield(field1,[Name>],"<")

  GotoTag("<Name2")    && get Hawb
  m.name2=getfield(field1,[Name2>],"<")
  If m.name2 = "NA"
    m.name2=""
  endif  

  GotoTag("<Street")    && get Hawb
  m.street=getfield(field1,[Street>],"<")

  GotoTag("<Street2")    && get Hawb
  m.street2=getfield(field1,[Street2>],"<")
  If m.street2 = "NA"
    m.street2=""
  endif  
  
  GotoTag("<City")    && get Hawb
  m.city=getfield(field1,[City>],"<")

  GotoTag("<State")    && get Hawb
  m.state=getfield(field1,[State>],"<")

  GotoTag("<PostalCode")    && get Hawb
  m.zip=getfield(field1,[PostalCode>],"<")

  GotoTag([<Date DateQualifier="ReqDelDate">])
  lcDate=getfield(field1,[Date DateQualifier="ReqDelDate">],"<")
  lxDate = Substr(lcDate,5,2)+"/"+Substr(lcDate,7,2)+"/"+Left(lcDate,4)
  m.start = Ctod(lxDate)

  GotoTag([<Date DateQualifier="CancelDate">])
  lcDate=getfield(field1,[Date DateQualifier="CancelDate">],"<")
  lxDate = Substr(lcDate,5,2)+"/"+Substr(lcDate,7,2)+"/"+Left(lcDate,4)
  m.cancel = Ctod(lxDate)

  GotoTag("</Orders>","</Orders>")

  Skip 1 In temp1

  If temp1.field1 = "</ns0:UA_Order>"
    done = .T.
    Insert Into csrpodata From Memvar
  Else
    Insert Into csrpodata From Memvar
    Skip -1 In temp1
  Endif

Enddo

Select csrpodata
Scan
  Scatter Memvar
  Select podata
  Locate For podata.ponum = csrpodata.ponum
  If !Found()
    m.filename = xfile
    Insert Into podata From Memvar
  Else
   Select podata
   Delete For  podata.ponum = csrpodata.ponum
    m.filename = xfile
    Insert Into podata From Memvar
  endif 
Endscan

Select csrpodata
Zap

Endproc






*!*      GotoTag("<CustomerPO")
*!*      m.linenum=getfield(field1,[CustomerPO>],"<")
*!*      m.hdrdata =""
*!*      If m.doctype = "RESERVED"

*!*        GotoTag("<CustomerPO")    && get Hawb
*!*        m.custpo =getfield(field1,[CustomerPO>],"<")
*!*        m.hdrdata = m.hdrdata+Chr(13)+"CUSTPO*"+m.custpo

*!*        GotoTag("<ID")    && get Hawb
*!*        m.soldtoid =getfield(field1,[ID>],"<")
*!*        m.hdrdata = m.hdrdata+Chr(13)+"SOLDTOID*"+m.soldtoid

*!*        GotoTag("<Name")    && get Hawb
*!*        m.soldtoname =getfield(field1,[Name>],"<")
*!*        m.hdrdata = m.hdrdata+Chr(13)+"SOLDTONAME*"+m.soldtoname

*!*        GotoTag("<Street")    && get Hawb
*!*        m.soldtoaddr1 =getfield(field1,[Street>],"<")
*!*        m.hdrdata = m.hdrdata+Chr(13)+"SOLDTOADD1*"+m.soldtoaddr1

*!*        GotoTag("<Street2")    && get Hawb
*!*        m.soldtoaddr2 =getfield(field1,[Street2>],"<")
*!*        m.hdrdata = m.hdrdata+Chr(13)+"SOLDTOADD2*"+m.soldtoaddr2

*!*        GotoTag("<Street3")    && get Hawb
*!*        m.soldtoaddr3 =getfield(field1,[Street3>],"<")
*!*        m.hdrdata = m.hdrdata+Chr(13)+"SOLDTOADD3*"+m.soldtoaddr3

*!*        GotoTag("<City")    && get Hawb
*!*        m.soldtocity =getfield(field1,[City>],"<")
*!*        m.hdrdata = m.hdrdata+Chr(13)+"SOLDTOCITY*"+m.soldtocity

*!*        GotoTag("<State")    && get Hawb
*!*        m.soldtostate =getfield(field1,[State>],"<")
*!*        m.hdrdata = m.hdrdata+Chr(13)+"SOLDTOSTATE*"+m.soldtostate

*!*        GotoTag("<PostalCode")    && get Hawb
*!*        m.soldtozip =getfield(field1,[PostalCode>],"<")
*!*        m.hdrdata = m.hdrdata+Chr(13)+"SOLDTOZIP*"+m.soldtozip

*!*        GotoTag("<Email")    && get Hawb
*!*        m.soldtoemail =getfield(field1,[Email>],"<")
*!*        m.hdrdata = m.hdrdata+Chr(13)+"SOLDTOEMAIL*"+m.soldtoemail

*!*        GotoTag("<ExtPartnerID")    && ebd of the SOLD TO
*!*        m.extsoldtoid =getfield(field1,[ExtPartnerID>],"<")
*!*        m.hdrdata = m.hdrdata+Chr(13)+"SOLDTOEXTID*"+m.extsoldtoid

*!*        ******************************************************
*!*        GotoTag("<ID")    && SHIP TO
*!*        m.shiptoid =getfield(field1,[ID>],"<")
*!*        m.hdrdata = m.hdrdata+Chr(13)+"SHIPTOID*"+m.shiptoid

*!*        GotoTag("<Name")    && get Hawb
*!*        m.shiptoname =getfield(field1,[Name>],"<")
*!*        m.hdrdata = m.hdrdata+Chr(13)+"SHIPTONAME*"+m.shiptoname

*!*        GotoTag("<ExtPartnerID")    && get Hawb
*!*        m.shiptoid =getfield(field1,[ExtPartnerID>],"<")
*!*        m.hdrdata = m.hdrdata+Chr(13)+"SHIPTOID*"+m.shiptoid
*!*        **********************************************************
*!*
*!*        GotoTag("<ID")    && get Hawb
*!*        m.markforid =getfield(field1,[ID>],"<")
*!*        m.hdrdata = m.hdrdata+Chr(13)+"MARKFORID*"+m.markforid

*!*        GotoTag("<Name")    && get Hawb
*!*        m.markforname =getfield(field1,[Name>],"<")
*!*        m.hdrdata = m.hdrdata+Chr(13)+"MARKFORNAME*"+m.markforname

*!*        GotoTag("<ExtPartnerID")    && get Hawb
*!*        m.extmarkforid =getfield(field1,[ExtPartnerID>],"<")
*!*        m.hdrdata = m.hdrdata+Chr(13)+"MARKFOREXTID*"+m.extmarkforid


*!*        GotoTag("<Street")    && get Hawb
*!*        m.markforadd1 =getfield(field1,[Street>],"<")
*!*        m.hdrdata = m.hdrdata+Chr(13)+"MARKFORADD1*"+m.markforadd1

*!*        GotoTag("<Street2")    && get Hawb
*!*        m.markforadd2 =getfield(field1,[Street2>],"<")
*!*        m.hdrdata = m.hdrdata+Chr(13)+"MARKFORADD2*"+m.markforadd2

*!*        GotoTag("<Street3")    && get Hawb
*!*        m.markforadd3 =getfield(field1,[Street3>],"<")
*!*        m.hdrdata = m.hdrdata+Chr(13)+"MARKFORADD3*"+m.markforadd3

*!*        GotoTag("<City")    && get Hawb
*!*        m.markforcity =getfield(field1,[City>],"<")
*!*        m.hdrdata = m.hdrdata+Chr(13)+"MARKFORCITY*"+m.markforcity

*!*        GotoTag("<State")
*!*        m.markforstate =getfield(field1,[State>],"<")
*!*        m.hdrdata = m.hdrdata+Chr(13)+"MARKFORSTATE*"+m.markforstate

*!*        GotoTag("<PostalCode")    && get Hawb
*!*        m.markforzip =getfield(field1,[PostalCode>],"<")
*!*        m.hdrdata = m.hdrdata+Chr(13)+"MARKFORZIP*"+m.markforzip

*!*        GotoTag("<Email")    && get Hawb
*!*        m.markforemail =getfield(field1,[Email>],"<")
*!*        m.hdrdata = m.hdrdata+Chr(13)+"MARKFOREMAIL*"+m.markforemail

*************************************************************************************************************
Procedure parse_asn
GotoTag("<Header")

GotoTag("<DocNumber")
m.docnumber=getfield(field1,[DocNumber>],"<")
m.reference =m.docnumber

GotoTag("<ExtDocNumber")
m.docnumber=getfield(field1,[ExtDocNumber>],"<")

If DocVariant = "STANDARD"
  GotoTag([<DocType Variant="STANDARD"])    && get Hawb
  m.subbol=getfield(field1,[[<DocType Variant="RESERVED">],"<")
  m.hawb = m.subbol
Else
  GotoTag([<DocType Variant="RESERVED"])    && get Hawb
  m.subbol=getfield(field1,[[<DocType Variant="RESERVED">],"<")
  m.hawb = m.subbol
Endif

GotoTag("<SubBOL")    && get Hawb
m.subbol=getfield(field1,[SubBOL>],"<")
m.hawb = m.subbol

GotoTag("<Trailer")    && get Hawb
m.container=getfield(field1,[Trailer>],"<")

GotoTag("<TotalNumOfCartons")    && get Hawb
m.totcartons=getfield(field1,[TotalNumOfCartons>],"<")

GotoTag("<ShippedFrom")    && get Hawb
GotoTag("<ID")    && get Hawb
m.shipfrom=getfield(field1,[ID>],"<")

GotoTag("<Name")    && get Hawb
m.shipfromname=getfield(field1,[Name>],"<")

GotoTag("<ID")    && get Hawb
m.shipfromid=getfield(field1,[ID>],"<")

GotoTag([<Date DateQualifier="DeliveryDate"])    && get Hawb
m.deldate=getfield(field1,[<Date DateQualifier="DeliveryDate">],"<")

GotoTag("<MessageID")    && get Hawb
m.messageid=getfield(field1,[MessageID>],"<")

Skip 2 In temp1

llDone = .F.

Do While !llDone

  GotoTag("<Order")
  m.ordernum=getfield(field1,[Order>],"<")

  llOrderDetailsDone = .F.
  Do While !llOrderDetailsDone

    GotoTag("<OrderDetails")

    GotoTag("<LineNum")
    m.po_num=getfield(field1,[LineNum>],"<")

    GotoTag("<SKU")
    m.Doctype=getfield(field1,[SKU>],"<")

    GotoTag("<VariantArticle")
    m.Doctype=getfield(field1,[VariantArticle>],"<")

    GotoTag("<UPC")
    m.Doctype=getfield(field1,[UPC>],"<")

* GotoTag("<WMSizeCode")
* m.Doctype=getfield(field1,[WMSizeCode>],"<")

    GotoTag([<Quantity UOM])
    If "EA"$temp1.field1
      m.Doctype=getfield(field1,[Quantity UOM="EA">],"<")
    Else
      m.Doctype=getfield(field1,[Quantity UOM="PR">],"<")
    Endif

*     GotoTag([<Quantity UOM="EA"])
*      m.Doctype=getfield(field1,[Quantity UOM="EA">],"<")

    GotoTag("<StockCategory")
    m.Doctype=getfield(field1,[StockCategory>],"<")

    GotoTag("<SAPPurchOrderNum")
    m.Doctype=getfield(field1,[SAPPurchOrderNum>],"<")

    GotoTag("<SAPPurchOrderType")
    m.Doctype=getfield(field1,[SAPPurchOrderType>],"<")

    Skip 2 In temp1
    If "<OrderDetails>"$temp1.field1
&& another OrderDetails loop
    Else
      llOrderDetailsDone = .T.
      Skip -1 In temp1
    Endif
  Enddo
  llOrderDetailsDone = .F.

  llCartonFound = .T.
  m.pl_qty = 0
  m.plweight =0
  m.cbm = 0.0

  Do While llCartonFound

    GotoTag("<CartonID")    && get Hawb
    m.ucc=getfield(field1,[CartonID>],"<")
    m.pl_qty = m.pl_qty+1

    GotoTag([<Value type="GrossWeight"])    && get Hawb
    m.ctnwt=getfield(field1,[Value type="GrossWeight">],"<")
    m.plweight = m.plweight + Val(m.ctnwt)

    GotoTag([<Value type="Width"])    && get Hawb
    m.width=getfield(field1,[Value type="Width">],"<")

    GotoTag([<Value type="Height"])    && get Hawb
    m.height=getfield(field1,[Value type="Height">],"<")

    GotoTag([<Value type="Length"])    && get Hawb
    m.length=getfield(field1,[Value type="Length">],"<")
    m.cbm = m.cbm+(Val(m.width)*Val(m.height)*Val(Length))/1728

    m.suppdata = m.suppdata+"CTNWT*"+m.ctnwt+Chr(13)
    m.suppdata = m.suppdata+"CTNWIDTH*"+m.length+Chr(13)
    m.suppdata = m.suppdata+"CTNWIDTH*"+m.width+Chr(13)
    m.suppdata = m.suppdata+"CTNHEIGHT*"+m.height+Chr(13)


    llCartonDetails = .T.
    Do While llCartonDetails

      GotoTag("<SKU")    && get Hawb
      m.style=getfield(field1,[SKU>],"<")

      GotoTag("<VariantArticle")    && get Hawb
      m.var_ctn=getfield(field1,[VariantArticle>],"<")

      GotoTag("<UPC")    && get Hawb
      m.upc=getfield(field1,[UPC>],"<")

*      GotoTag("<WMSizeCode")    && get Hawb
*      m.id=getfield(field1,[WMSizeCode>],"<")


      GotoTag([<Quantity UOM])
      If "EA"$temp1.field1
        m.totqty=getfield(field1,[<Quantity UOM="EA">],"<")
      Else
        m.totqty=getfield(field1,[<Quantity UOM="PR">],"<")
      Endif

*   GotoTag([<Quantity UOM="EA"])    && get Hawb

      m.pack =Alltrim(Transform(Int(Val(m.totqty))))
      m.suppdata = m.suppdata+"QTY*"+m.pack+Chr(13)

      GotoTag("<Order")    && get Hawb
      m.ponum=getfield(field1,[Order>],"<")
      m.suppdata = m.suppdata+"ORDER*"+m.ponum+Chr(13)

      GotoTag("<OrderLine")    && get Hawb
      m.ctn_linenum=getfield(field1,[OrderLine>],"<")
      m.suppdata = m.suppdata+"LINE*"+m.ctn_linenum+Chr(13)

      Do getCtnRecord

      Skip 2 In temp1
      If temp1.field1 = "<CartonDetails>"
        llCartonDetails = .T.
      Else
        llCartonDetails = .F.
        Skip -1 In temp1
      Endif
    Enddo
    Skip 2 In temp1

    If temp1.field1 = "<Carton>"
      llCartonFound = .T.
    Else
      llCartonFound = .F.
      If temp1.field1 = "</Order>"
        Skip 2 In temp1
*          Set Step On
        If temp1.field1 = "<Order>"
          llOrderFound = .T.
          Do getRecord
        Else
          llOrderFound = .F.
          llDone = .T.
          Do getRecord
        Endif
      Else

      Endif
    Endif

    If llDone = .T.
      Exit
    Endif
  Enddo
Enddo

llDoSql=.T.

If llDoSql
  gmasteroffice ="N"
  goffice = "N"
  m.mod="I"
  useca("ctnucc","wh")
  Select uaucctemp
  Scan
    Select uaucctemp
    Scatter Memvar Memo
    insertinto("ctnucc","wh",.T.)
  Endscan
  tu("ctnucc")
  Return

Endif

*Do TransloadReceipt


Endproc

Procedure Events

 Select uaucctemp
 lnNumCtns = Reccount("uaucctemp")
 
 Select reference,ponum,getmemodata("suppdata","ASN") asnnum  From uaucctemp Group By reference, ponum Into Cursor tevents

 Select tevents
 Do ua_event With "2903",tevents.reference, tevents.ponum,0 
 Wait Window At 10,10 "xxxx" Timeout 1
 Do ua_event With "2904",tevents.reference, tevents.ponum,0 
 Wait Window At 10,10 "xxxx" Timeout 1

 Select tevents
 Scan
  Do ua_event With "1500",tevents.reference, tevents.ponum,1 
  Wait Window At 10,10 "xxxx" Timeout 1
 Endscan   

 Select tevents
 Scan
  Do ua_event With "1510",tevents.reference, tevents.ponum,2 
  Wait Window At 10,10 "xxxx" Timeout 1
Endscan   

Select tevents
Go top
Do ua_pod_event With "2800",tevents.reference,tevents.asnnum,4 


set step On 
 

endproc 

Procedure ASNReceipt
Parameters lcReceiptNum,lcTPM

Select shiphdr
Locate For tpmid = lcTPM
If Found()

Else
  Wait Window At 10,10 "Shipment data not on file for TPMID= "+lcTPM
Endif

Set Date YMD
lcTPMID= Strtran(Ttoc(Datetime()),"/","")
lcTPMID= Strtran(lcTPMID," ","")
lcTPMID= Strtran(lcTPMID,":","")

lcTStamp=Strtran(Ttoc(Datetime()),"/","-")
lcTStamp=Strtran(lcTStamp," ","T")

lcEventCode ="2094"

lcRCVStamp=Strtran(Ttoc(Datetime()-10000),"/","-")
lcRCVStamp=Strtran(lcRCVStamp," ","T")

* Read in the XML header template

lcHeader = Filetostr("f:\underarmour\rcpt_header.txt")

* Replace all the predefined data elements

lcHeader = Strtran(lcHeader,"TPMID",shiphdr.tpmid) &&lcTPMid
lcHeader = Strtran(lcHeader,"TSTAMP",lcTStamp)
lcHeader = Strtran(lcHeader,"EVENTCODE",lcEventCode)
lcHeader = Strtran(lcHeader,"SHIPMENTID",shiphdr.shipid)  &&lcShipID
lcHeader = Strtran(lcHeader,"MBOL","") &&lcMBOL
lcHeader = Strtran(lcHeader,"HBOL",shiphdr.BOL) &&lcBOL
lcHeader = Strtran(lcHeader,"PNUM","") && lcPro

lcHeader = Strtran(lcHeader,"CONTAINER",shiphdr.Container)  &&lcCtr

lcHeader = Strtran(lcHeader,"RECEIPTNUM",lcReceiptNum)
lcHeader = Strtran(lcHeader,"RCVSTAMP",lcRCVStamp)

* get the cartons related to this shipment
Select cartons

lcFilename = Strtran(lcTPMID,"-","")
lcFilename = Strtran(lcFilename,":","")
lcFilename = Strtran(lcFilename,"T","")
lcFilename = "c:\tempfox\"+"FMI_"+Alltrim(lcFilename)+".xml"

lnHandle2=Fcreate(lcFilename)

NumWrite = Fputs(lnHandle2,lcHeader)

Select cartons
Goto Top
Scan
  lcStr = [<LPN Id="]+Alltrim(cartons.ucc)+["  ReceiptIdRef="]+Alltrim(lcReceiptNum)+[">]
  NumWrite = Fputs(lnHandle2,lcStr)
  NumWrite = Fputs(lnHandle2,[</LPN>])
Endscan

* write out the XML footer elements

NumWrite = Fputs(lnHandle2,[</Destination>])
NumWrite = Fputs(lnHandle2,[</MANH_TPM_Shipment>])
NumWrite = Fputs(lnHandle2,[</Message>])
NumWrite = Fputs(lnHandle2,[</tXML>])
Fclose(lnHandle2)

Endproc
***********************************************************

*Do ASNReceipt with "6004604",lcTPMID
*************************************************************************************************************

Procedure parse_newasn
GotoTag("<Header")

GotoTag("<DocNumber")
m.docnumber=getfield(field1,[DocNumber>],"<")
m.reference =m.docnumber
gcDocNumber = m.docnumber

GotoTag("<ExtDocNumber")
m.docnumber=getfield(field1,[ExtDocNumber>],"<")

If DocVariant = "STANDARD"
  GotoTag([<DocType Variant="STANDARD"])    && get Hawb
  m.subbol=getfield(field1,[[<DocType Variant="RESERVED">],"<")
  m.hawb = m.subbol
Else
  GotoTag([<DocType Variant="RESERVED"])    && get Hawb
  m.subbol=getfield(field1,[[<DocType Variant="RESERVED">],"<")
  m.hawb = m.subbol
Endif

GotoTag("<SubBOL")    && get Hawb
m.subbol=getfield(field1,[SubBOL>],"<")
m.hawb = m.subbol

GotoTag("<Trailer")    && get Hawb
m.container=getfield(field1,[Trailer>],"<")

GotoTag("<TotalNumOfCartons")    && get Hawb
m.totcartons=getfield(field1,[TotalNumOfCartons>],"<")

GotoTag("<ShippedFrom")    && get Hawb
GotoTag("<ID")    && get Hawb
m.shipfrom=getfield(field1,[ID>],"<")

GotoTag("<Name")    && get Hawb
m.shipfromname=getfield(field1,[Name>],"<")

GotoTag("<ID")    && get Hawb
m.shipfromid=getfield(field1,[ID>],"<")

GotoTag([<Date DateQualifier="DeliveryDate"])    && get Hawb
m.deldate=getfield(field1,[<Date DateQualifier="DeliveryDate">],"<")

GotoTag("<MessageID")    && get Hawb
m.messageid=getfield(field1,[MessageID>],"<")

Skip 2 In temp1

llDone = .F.

Do While !llDone

  GotoTag("<Order")
  m.ordernum=getfield(field1,[Order>],"<")
  GotoTag("<ShipTo")
  GotoTag("<ID")
  m.shipid=getfield(field1,[ID>],"<")


********** roll up the Order Details Loop ************************************
  lcOrderDetailsStr = ""
  GotoTag("<OrderDetails")
  lcOrderDetailsStr = Alltrim( temp1.field1)
  lnORD_Start = Recno("temp1")

  llOrderDetailsDone = .F.
  Do While !llOrderDetailsDone
    Skip 1 In temp1
    If temp1.field1 ="</OrderDetails>"
      lcOrderDetailsStr = lcOrderDetailsStr+Chr(13)+Alltrim(temp1.field1)
      Skip 1 In temp1
      If temp1.field1 ="<OrderDetails>"
        lcOrderDetailsStr = lcOrderDetailsStr+Chr(13)+Alltrim(temp1.field1)
** there is another OrderDetail loop
      Else
        llOrderDetailsDone = .T.
        m.orddetails = lcOrderDetailsStr
        Skip -1 In temp1
      Endif
    Else
      lcOrderDetailsStr = lcOrderDetailsStr+Chr(13)+Alltrim(temp1.field1)
    Endif
  Enddo

********** roll up the Carton Loop ************************************
  lcCartonStr = "<Carton>"
  GotoTag("<Carton")  && really should be here already
  lccarton = Alltrim( temp1.field1)
  lnCtn_Start = Recno("temp1")
  lnCtn = 1
  llCartonDone = .F.
  Do While !llCartonDone
    Skip 1 In temp1
    If temp1.field1 ="</Carton>"
      lcCartonStr = lcCartonStr+Chr(13)+Alltrim(temp1.field1)
      Skip 1 In temp1
      If temp1.field1 ="<Carton>"
** write out this carton
        m.ctndetails = lcCartonStr
        Do getCtnRecord
        lcCartonStr = "<Carton>"
        lnCtn = lnCtn +1
      Else
        m.ctndetails = lcCartonStr
        Do getCtnRecord
        llCartonDone = .T.
        Skip -1 In temp1
      Endif
    Else
      lcCartonStr = lcCartonStr+Chr(13)+Alltrim(temp1.field1)

      If temp1.field1 = "<CartonID>"
        m.ucc=getfield(field1,[CartonID>],"<")
        m.pl_qty = m.pl_qty+1
      Endif

      If temp1.field1 =[<Value type="GrossWeight"]    && get Hawb
        m.ctnwt=getfield(field1,[Value type="GrossWeight">],"<")
        m.plweight = m.plweight + Val(m.ctnwt)
      Endif

      If temp1.field1 = [<Value type="Width"]    && get Hawb
        m.width=getfield(field1,[Value type="Width">],"<")
      Endif

      If temp1.field1 =[<Value type="Height"]    && get Hawb
        m.height=getfield(field1,[Value type="Height">],"<")
      Endif

      If temp1.field1 = [<Value type="Length"]  && get Hawb
        m.length=getfield(field1,[Value type="Length">],"<")
        m.cbm = m.cbm+(Val(m.width)*Val(m.height)*Val(Length))/1728
        m.suppdata = m.suppdata+"CTNWT*"+m.ctnwt+Chr(13)
        m.suppdata = m.suppdata+"CTNWIDTH*"+m.length+Chr(13)
        m.suppdata = m.suppdata+"CTNWIDTH*"+m.width+Chr(13)
        m.suppdata = m.suppdata+"CTNHEIGHT*"+m.height+Chr(13)
      Endif
    Endif
  Enddo


  llCartonFound = .T.
  m.pl_qty = 0
  m.plweight =0
  m.cbm = 0.0

  Skip 2 In temp1
  If temp1.field1 = "</Header>"
    llDone =.T.
  Endif

  If temp1.field1 = "<Order>"
    llOrderFound = .T.
    Do getRecord
    llOrderDetailsDone = .F.
  Endif

  If llDone = .T.
    Exit
  Endif

Enddo

gmasteroffice ="N"
goffice = "I"

Select uaucctemp
Scan
  Do Case
  Case "MIA"$xfile
    Replace whseloc With "TRSUS_MIA" In uaucctemp
  Case "NJ"$xfile
    Replace whseloc With "TRSUS_NJ" In uaucctemp
  Otherwise
    Replace whseloc With "TRSUS_UNK" In uaucctemp
  Endcase
Endscan

Scan
  Scatter Memvar Memo
  insertinto("ctnucc","wh",.T.)
Endscan

tu("ctnucc")

Select uaucctemp
*Do TransloadReceipt

Select uaucctemp
Zap

Return

Endproc
