******************************************************************************
* create_ship_package
*
*   This is called during the trigegring of 945s for small package shipments.
*
*   This function is called when the system sees that there are tracking numbers
*   in the Amazon memo field. These tracking numbers are a result of small package
*   shipping labels that were forwarded to us from the customer.
*
*   In order to minimize the effect on the 945 process, this function will create
*   shipment and package records so the 945 process can run in a more normal mode.
*
*   There may be some values in these new records to ensure that these reords are
*   part of small package closings and/or other reports in that they did not
*   originate from the internal small package systems.
*
*   This will also put the first tracking number into outship.bol_no and set
*   outship.keyrec to "CUSTOMERSUPPLIED"
*
*   PG 2/26/2018
******************************************************************************
PARAMETERS lnAccountid,lcShip_ref
NormalExit = .f.
** only for stand alone testing
*Close Data All
*test(.T.,.t.)

** only for stand alone testing, need to be set in the calling program
&& gmasteroffice ="L"
&& goffice ="L"
&& gmod = "L"

TRY

WAIT WINDOW "Now processing manual Amazon.field tracking numbers" NOWAIT NOCLEAR

IF USED('SHIPMENT')
	USE IN shipment
ENDIF

IF USED('PACKAGE')
	USE IN package
ENDIF

xsqlexec("select * from outship where del_date > {"+DTOC(DATE()-5)+"} and bol_no = ' ' order by office,mod,accountid,ship_ref","xamazon",,"wh")

SELECT xamazon
LOCATE
SCAN
	lcShip_ref = ALLTRIM(xamazon.ship_ref)
	lnAccountid = xamazon.accountid
	gMasterOffice = xamazon.office
	gOffice = xamazon.mod
	gMod = gOffice
*	WAIT WINDOW "Now scanning Office/Mod/PT# "+gMasterOffice+"/"+gMod+"/"+lcShip_ref NOWAIT 
	? "Now populating today's manual/cust. supplied tracking numbers"
		
	xsqlexec("select * from shipment where accountid = "+TRANSFORM(lnAccountid)+" and pickticket = '"+lcShip_ref+"'","ship1",,"wh")
	SELECT ship1
	IF RECCOUNT()>0
	USE IN ship1
	LOOP
	ENDIF
	
	USE IN ship1
	
	CREATE CURSOR csrUCCs (;
		ucc CHAR(20))

	SELECT xamazon

*xsqlexec("select * from outship where ship_ref='"+Alltrim(lcShip_ref)+"' and accountid = "+Transform(lnAccountid),"xamazon",,"wh")  && Removed to run a scan of outship extract instead

** grab the tracking numbers into an array
	alength = ALINES(aTraks,xamazon.amazon,.T.,CHR(13))
	IF LEN(aTraks[alength]) < 3
		alength = alength -1
	ENDIF

** first put a single record into shipment

	IF alength > 0 AND LEN(aTraks[1]) > 2
		useca("shipment","wh")
		useca("package","wh")
		SELECT xamazon
		SCATTER MEMVAR
		m.acctcode ="MANUAL"
		m.carrier ="MANUAL"
		m.service ="MAN"
		m.billing ="MAN"
		m.pickuprec="MANUAL"
		m.pickticket = ALLTRIM(lcShip_ref)
		m.shipdate = xamazon.del_date
		m.company = xamazon.consignee
		m.charge= 0.00
		m.updatedt = DATETIME()
		m.updateby = "945TRIG"
		m.updproc = "TGF-PROC"
		m.adddt = DATETIME()
		m.addby = "TGF-PROC"
		m.addproc = "945TRIG"
		insertinto("shipment","wh",.T.)

&& now grab the UCCs from Cartons
		xsqlexec("select ucc from cartons where ship_ref='"+ALLTRIM(lcShip_ref)+"' and accountid = "+TRANSFORM(lnAccountid)+ " group by ucc","xuccs",,"pickpack")

		IF RECCOUNT("xuccs")= 0
			FOR xucc = 1 TO alength
				INSERT INTO csrUCCs (ucc) VALUES ("UNK")
			NEXT
		ELSE
			SELECT ucc FROM xuccs INTO CURSOR csrUCCs GROUP BY ucc
			SELECT csrUCCs
			COPY TO ARRAY aUCCs
		ENDIF
		len1 = ALEN(aUCCs)
*		len1 = RECCOUNT('csrUCCs')
** then add in a record for each tracking number in the Amazon memo field

		FOR xxi = 1 TO len1
			m.shipmentid = shipment.shipmentid
			m.pkgnum = xxi
			m.pkgweight = 0.00
			m.length = 0
			m.width = 0
			m.depth = 0
			m.ref1= xamazon.cnee_ref
			m.ref2= xamazon.ship_ref
			m.updatedt = DATETIME()
			m.updproc = "TGF-PROC"
			m.updateby = "945TRIG"
			m.adddt = DATETIME()
			m.addby = "TGF-PROC"
			m.addproc = "945TRIG"
*			m.ucc = csrUCCs.ucc
			TRY
				m.ucc = aUCCs[xxi]
			CATCH
				m.ucc = aUCCs[1]
			ENDTRY
			TRY
			m.trknumber = STRTRAN(aTraks[xxi],CHR(10),"")  && remove the line feed character
			CATCH TO oErr2
			endtry
			pkgcharge =0
			m.packageid = sqlgenpk("package","wh")
			insertinto("package","wh",.T.)
		ENDFOR

		SELECT shipment
		tu("shipment")
		SELECT package
		tu("package")

		SELECT csrUCCs
		LOCATE

&& here we update the BOL with the first tracking number ,,, not sure if this is required
*  xsqlexec("update outship set bol_no = '"+Left(Alltrim(aTraks[1]),20)+"' where ship_ref='"+lcShip_ref+"' and accountid = "+Transform(lnAccountid),,,"wh")

		xsqlexec("update outship set keyrec = 'CUSTOMERSUPPLIED' where ship_ref='"+lcShip_ref+"' and accountid = "+TRANSFORM(lnAccountid),,,"wh")

*Else
*Return && no values in AMAZON memo field
	ENDIF
	IF USED('csrUCCs ')
		USE IN csrUCCs
	ENDIF
ENDSCAN

WAIT WINDOW "Amazon Shipment/Package tracking number insertion process complete" TIMEOUT 2

NormalExit = .t.

CATCH TO oErr
	IF !NormalExit
		SET STEP ON
		ASSERT .F. MESSAGE "At CATCH..."
		tsubject = "Create Ship Pkg Error ("+TRANSFORM(oErr.ERRORNO)+") at "+TTOC(DATETIME())
		tsendto= "joe.bianchi@tollgroup.com"
		tcc = ""
		tattach  = ""
		tmessage = "Create Ship Pkg Error"
		lcSourceMachine = SYS(0)
		lcSourceProgram = SYS(16)

		tmessage = tmessage+ "Try/CATCH Exeception Message:"+CHR(13)+;
			[  Error: ] + STR(oErr.ERRORNO) +CHR(13)+;
			[  LineNo: ] + STR(oErr.LINENO) +CHR(13)+;
			[  Message: ] + oErr.MESSAGE +CHR(13)+;
			[  Procedure: ] + oErr.PROCEDURE +CHR(13)+;
			[  Details: ] + oErr.DETAILS +CHR(13)+;
			[  StackLevel: ] + STR(oErr.STACKLEVEL) +CHR(13)+;
			[  LineContents: ] + oErr.LINECONTENTS+CHR(13)+;
			[  UserValue: ] + oErr.USERVALUE+CHR(13)+;
			[  Computer:  ] +lcSourceMachine+CHR(13)+;
			[  Program:   ] +lcSourceProgram

		tmessage = tmessage + CHR(13) + cUserMessage

		tattach = ""
		tCC = IIF(lAriat,"paul.gaidis@tollgroup.com,juan.rocio@tollgroup.com","")
		tFrom = "TGF EDI Processing Center <toll-edi-ops@tollgroup.com>"
		DO FORM m:\dev\frm\dartmail2 WITH tSendto,tFrom,tsubject,tCC,tattach,tmessage,"A"
	ELSE
		WAIT WINDOW AT 10,10 "Normal Exit " TIMEOUT 2
	ENDIF
FINALLY
	WAIT WINDOW "Finished processing all accounts/locations...exiting"  TIMEOUT 2
	WAIT CLEAR
	schedupdate()
	IF USED('FTPSETUP')
		USE IN FTPSETUP
	ENDIF
	SELECT 0
	USE F:\edirouting\FTPSETUP SHARED
	REPLACE FTPSETUP.chkbusy WITH .F. FOR FTPSETUP.TRANSFER = "TRIG-PARCEL945"
	CLOSE DATA ALL
ENDTRY
