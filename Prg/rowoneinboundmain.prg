PUBLIC m.accountid,NormalExit,cUseFolder,tsendto,tsendtoerr,tcc,tccerr,cTransfer,lDoMail,cCustName,lDoSQL,cMod
CLOSE DATABASES ALL

TRY

	lTesting = .F.
	lTestDest = .F.

	NormalExit = .F.
	DO m:\dev\prg\_setvars WITH .T.
	lDoMail = .T.

	_screen.WindowState=IIF(lTesting,1,0)
	
	cOffice = "C" && Mod C
	nAcctNum = 6468
	cTransfer = "PL-ROWONE"
	cMod = "5"
	gOffice = cOffice
	gMasterOffice = cOffice
	cWhseMod = LOWER("wh"+cMod)

	lDoSQL = .T.

	IF !lTesting
		SELECT 0
		USE F:\edirouting\FTPSETUP SHARED
		REPLACE chkbusy WITH .T. FOR FTPSETUP.TRANSFER = cTransfer
		USE IN FTPSETUP
	ENDIF

	SET STEP ON
	IF lTesting
		WAIT WINDOW "Running as TEST" TIMEOUT 2
		cUseFolder = "F:\WHP\WHDATA\"
	ELSE
		xReturn = "XXX"
		DO m:\dev\prg\wf_alt WITH cOffice,nAcctNum
		cUseFolder = UPPER(xReturn)
		WAIT WINDOW "Using folder "+cUseFolder TIMEOUT 2
	ENDIF

	SELECT 0
	USE F:\3pl\DATA\mailmaster ALIAS mm
	LOCATE FOR mm.accountid = nAcctNum AND mm.office = cOffice AND mm.edi_type = "PL"
	IF FOUND()
		STORE TRIM(mm.AcctName) TO lcAccountname
		STORE TRIM(mm.basepath) TO lcPath
		IF lTesting
			WAIT WINDOW "File path is "+lcPath TIMEOUT 2
		ENDIF
		STORE TRIM(mm.archpath) TO lcArchPath
		STORE TRIM(mm.scaption) TO _SCREEN.CAPTION
		IF lTesting
			LOCATE FOR edi_type = "MISC" AND taskname = "GENERAL"
		ENDIF
		tsendto = IIF(mm.use_alt,sendtoalt,sendto)
		tcc = IIF(mm.use_alt,ccalt,cc)
		LOCATE
		LOCATE FOR edi_type = "MISC" AND taskname = "GENERAL"
		tsendtoerr = IIF(mm.use_alt,sendtoalt,sendto)
		tccterr = IIF(mm.use_alt,ccalt,cc)
		USE IN mm
	ELSE
		WAIT WINDOW AT 10,10  "No parameters set for this acct# "+ALLTRIM(STR(nAcctNum))+"  ---> Office "+lcOffice TIMEOUT 5
		THROW
	ENDIF

	useca("inwolog","wh")
	
*!*		IF lDoSQL
		useca("pl","wh")
*!*		ELSE
*!*			USE (cUseFolder+"pl") IN 0 ALIAS pl
*!*		ENDIF

	xsqlexec("select * from account where inactive=0","account",,"qq")
	INDEX ON accountid TAG accountid
	SET ORDER TO
	cCustName = ""

*SET STEP ON
	SELECT account
	=SEEK(nAcctNum,'account','accountid')
	IF FOUND()
		cCustName = (account.AcctName)
	ELSE
		WAIT WINDOW "Houston, we have a problem..." TIMEOUT 2
		THROW
	ENDIF
	SET STEP ON
	cInfolder = lcPath
	cArchfolder = lcArchPath
	CD [&cInfolder]
	len1 = ADIR(ary1,"*.xls")

	cfilenamexx = (cInfolder+"temp1.xls")

	FOR ee = 1 TO len1
		SELECT inwolog
		SCATTER MEMVAR MEMO BLANK
		SELECT pl
		SCATTER MEMVAR MEMO BLANK

		xsqlexec("select * from inwolog where .f.","xinwolog",,"wh")

		SELECT xinwolog
		INDEX ON inwologid TAG inwologid

		IF lDoSQL
			xsqlexec("select * from pl where .f.","xpl",,"wh")
		ELSE
			SELECT * FROM pl WHERE .F. INTO CURSOR xpl READWRITE
		ENDIF

		SELECT 0

		CREATE CURSOR intake (f1 c(50),f2 c(30),f3 c(30),f4 c(30),f5 c(30),f6 c(30),f7 c(30),f8 c(30),f9 c(30),f10 c(30),f11 c(30))
		IF FILE(cfilenamexx)
			DELETE FILE [&cfilenamexx]
		ENDIF
		cfilename = ALLTRIM(ary1[ee,1])
		cfullname = (cInfolder+cfilename)
		cArchfilename = (cArchfolder+cfilename)
		COPY FILE [&cfullname] TO [&carchfilename]
		oExcel = CREATEOBJECT("Excel.Application")
		oWorkbook = oExcel.Workbooks.OPEN(cfullname)
		WAIT WINDOW "Now saving file "+cfilename NOWAIT NOCLEAR
		SET SAFETY OFF
		oWorkbook.SAVEAS(cfilenamexx,39)  && re-saves Excel sheet as XL95/97 file
		WAIT CLEAR
		WAIT WINDOW "File save complete...continuing" TIMEOUT 1
		oWorkbook.CLOSE(.T.)
		oExcel.QUIT()
		RELEASE oExcel

		SELECT intake
		APPEND FROM [&cfilenamexx] TYPE XL5
		LOCATE
		IF UPPER(ALLTRIM(intake.f1)) = "STYLE"
			DELETE NEXT 1 IN intake
		ENDIF

		GO BOTT
		DO WHILE EMPTY(intake.f1)
			SKIP -1
		ENDDO
		IF lTesting
			LOCATE
			BROWSE
		ENDIF
		m.accountid = nAcctNum
		m.wo_date = DATE()
		m.date_rcvd = DATE()
		m.picknpack = .F.
		m.wo_num = 1
		m.inwologid = 1
		m.plid = 0
		m.AcctName = cCustName
		m.addby = "INBPROC"
		m.adddt = DATETIME()
		m.addproc = "ROWONEINB"

		LOCATE
		IF lTestDest
			m.container = "INV2DEST"
		ELSE
			LOCATE FOR UPPER(intake.f1) = "CONTAINER"
			m.container = ALLTRIM(intake.f2)
		ENDIF
		m.comments = "FILENAME*"+cfilename
		
		INSERT INTO xinwolog FROM MEMVAR

		SELECT intake
		m.plid = 0
		npo = 0
		LOCATE
		SCAN FOR !EMPTY(intake.f1)
			m.units = IIF(intake.f5 = "U",.T.,.F.)
			m.totqty = INT(VAL(intake.f11))
			REPLACE xinwolog.plinqty WITH xinwolog.plinqty+m.totqty IN xinwolog
			REPLACE xinwolog.plunitsinqty WITH xinwolog.plinqty IN xinwolog
			m.style = ALLTRIM(intake.f1)
			m.color = ALLTRIM(intake.f2)
			m.id = ALLTRIM(intake.f3)
			m.pack = ALLTRIM(intake.f4)
			m.plid = m.plid+1
			INSERT INTO xpl FROM MEMVAR
		ENDSCAN

		IF lTesting
			SELECT xinwolog
			LOCATE
			BROWSE
			SELECT xpl
			LOCATE
			BROWSE
		ENDIF

		DO m:\dev\prg\rowoneinbound_import
		DELETE FILE [&cfilenamexx]
		IF !lTesting
			DELETE FILE [&cfullname]
		ENDIF

	ENDFOR
	NormalExit = .T.

CATCH TO oErr
	IF !NormalExit
		SET STEP ON
		lnRec = 0
		ptError = .T.
		tsubject = "Inbound Upload Error ("+TRANSFORM(oErr.ERRORNO)+") for "+cCustName+" at "+TTOC(DATETIME())+"  on file "+cfilename
		tattach  = ""
		tmessage = "Error processing Inbound file,GENERIC INBOUND.... Filename: "+cfilename+CHR(13)
		tmessage = tmessage+CHR(13)+" Error at record "+TRANSFORM(lnRec)+CHR(13)+CHR(13)
		tmessage =tmessage+ "Try/CATCH Exeception Message:"+CHR(13)+;
			[  Error: ] + STR(oErr.ERRORNO) +CHR(13)+;
			[  LineNo: ] + STR(oErr.LINENO) +CHR(13)+;
			[  Message: ] + oErr.MESSAGE +CHR(13)+;
			[  Procedure: ] + oErr.PROCEDURE +CHR(13)+;
			[  Details: ] + oErr.DETAILS +CHR(13)+;
			[  StackLevel: ] + STR(oErr.STACKLEVEL) +CHR(13)+;
			[  LineContents: ] + oErr.LINECONTENTS+CHR(13)+;
			[  UserValue: ] + oErr.USERVALUE+CHR(13)+;
			[  Machine Name: ] + SYS(0)+CHR(13)+;
			[  Error Message :]+CHR(13)

		tsubject = "Inbound WO Error for "+cCustName+" at "+TTOC(DATETIME())+"  on file "+cfilename
		tattach  = ""
		tsendto = tsendtoerr
		tcc = tccerr

		tFrom    ="Toll WMS Inbound EDI System Operations <toll-edi-ops@tollgroup.com>"
		DO FORM m:\dev\frm\dartmail2 WITH tsendto,tFrom,tsubject,tcc,tattach,tmessage,"A"
		lCreateErr = .T.
	ENDIF
FINALLY
	WAIT WINDOW "All "+cCustName+" Inbound Complete...Exiting" TIMEOUT 2
	SELECT 0
	USE F:\edirouting\FTPSETUP SHARED
	REPLACE chkbusy WITH .F. FOR FTPSETUP.TRANSFER = cTransfer
	USE IN FTPSETUP
	CLOSE DATABASES ALL
ENDTRY
