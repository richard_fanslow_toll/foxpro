*!* m:\dev\prg\merkury940_bkdn.prg
CLEAR
lCheckStyle = .T.
lPrepack = .F.  && Inventory to be kept as PnP
m.color=""
cUCCNumber = ""
cUsePack = ""
lcUCCSeed = 1

SELECT x856
COUNT FOR TRIM(segment) = "ST" TO nSegCnt
cSegCnt = ALLTRIM(STR(nSegCnt))
SELECT x856
LOCATE

ptctr = 0

WAIT WINDOW "Now in "+cMailName+" 940 Breakdown" NOWAIT

WAIT "There are "+cSegCnt+" P/Ts in this file" WINDOW NOWAIT
SELECT x856
LOCATE

*!* Constants
m.acct_name = "STEELSERIES N. AMERICA CORP"
STORE nAcctNum TO m.accountid
m.careof = " "
m.sf_addr1 = "656 W. RANDOLPH STREET"
m.sf_addr2 = "2 FLOOR EAST"
m.sf_csz = "CHICAGO, IL 60661"
m.printed = .F.
m.print_by = ""
m.printdate = DATE()
*!* End constants

SELECT xpt
DELETE ALL
SELECT xptdet
DELETE ALL

STORE 0 TO pt_total_eaches
STORE 0 TO pt_total_cartons
STORE 0 TO nCtnCount

SELECT x856
SET FILTER TO
LOCATE

STORE "" TO cisa_num

WAIT WINDOW "NOW IN INITAL HEADER BREAKDOWN" TIMEOUT 1
WAIT WINDOW "NOW IN INITAL HEADER BREAKDOWN" NOWAIT
*ASSERT .F. MESSAGE "At header bkdn...debug"
DO WHILE !EOF("x856")
	cSegment = TRIM(x856.segment)
	cCode = TRIM(x856.f1)

	IF TRIM(x856.segment) = "ISA"
		cisa_num = ALLTRIM(x856.f13)
		CurrentISANum = ALLTRIM(x856.f13)
		WAIT WINDOW "This is a "+cMailName+" PT upload" NOWAIT
		ptctr = 0
		SELECT x856
		SKIP
		LOOP
	ENDIF

	IF TRIM(x856.segment) = "GS"
		cgs_num = ALLTRIM(x856.f6)
		lcCurrentGroupNum = ALLTRIM(x856.f6)
		SELECT x856
		SKIP
		LOOP
	ENDIF

	IF TRIM(x856.segment) = "ST" AND TRIM(x856.f1) = "940"
		IF !lTesting
			SELECT x856
			SKIP
			LOOP
		ENDIF
	ENDIF

	IF TRIM(x856.segment) = "W05"  && new shipment
		nCtnCount = 0
		m.style = ""
		SELECT xpt
		nDetCnt = 0
		APPEND BLANK

		REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"FILENAME*"+cFilename IN xpt
		REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"CONAME*STEEL SERIES"
		REPLACE xpt.qty WITH 0 IN xpt
		REPLACE xpt.origqty WITH 0 IN xpt
		STORE 0 TO pt_total_eaches
		STORE 0 TO pt_total_cartons
		ptctr = ptctr +1
		WAIT WINDOW AT 10,10 "Now processing PT# "+ ALLTRIM(x856.f2)+" Number "+CHR(13)+TRANSFORM(ptctr)+" of "+cSegCnt NOWAIT

		REPLACE xpt.ptid       WITH ptctr IN xpt
		REPLACE xpt.accountid  WITH nAcctNum IN xpt
		DO CASE
			CASE lNextDay
				REPLACE xpt.ptdate     WITH dFileDate+1 IN xpt
			CASE lMonday
				DO CASE
					CASE DOW(dFileDate) = 6
						REPLACE xpt.ptdate     WITH dFileDate+3 IN xpt
					CASE DOW(dFileDate) = 7
						REPLACE xpt.ptdate     WITH dFileDate+2 IN xpt
					CASE DOW(dFileDate) = 1
						REPLACE xpt.ptdate     WITH dFileDate+1 IN xpt
				ENDCASE
			CASE l4Day
				REPLACE xpt.ptdate     WITH dFileDate+4 IN xpt
			OTHERWISE
				REPLACE xpt.ptdate     WITH dFileDate IN xpt
		ENDCASE
		m.ship_ref = ALLTRIM(x856.f2)
		cship_ref = ALLTRIM(x856.f2)
		cOrigOffice = cOffice
		cPTIndicator = LEFT(cship_ref,1)  && Added per Juan to cover orders now shipping from Carson II, 08.10.2017, Joe
		cOffice = IIF(!INLIST(cPTIndicator,"W","R"),"Y",cOffice)
		cMod = IIF(!INLIST(cPTIndicator,"W","R"),"Y",cMod)

		IF cOffice # cOrigOffice
			SELECT 0
			USE F:\3pl\DATA\mailmaster ALIAS mm SHARED
			LOCATE FOR mm.edi_type = "940" AND mm.accountid = nAcctNum AND mm.office = cOffice
			IF FOUND()
				STORE TRIM(IIF(mm.use_alt,mm.sendtoalt,mm.sendto)) TO tsendto
				STORE TRIM(IIF(mm.use_alt,mm.ccalt,mm.cc)) TO tcc
			ENDIF
			USE IN mm
		ENDIF
		REPLACE ship_ref   WITH m.ship_ref IN xpt  && Pick Ticket
		m.pt = ALLTRIM(x856.f2)
		REPLACE cnee_ref   WITH ALLTRIM(x856.f3) In xpt  && cust PO
		SELECT x856
		SKIP
		LOOP
	ENDIF

	IF TRIM(x856.segment) = "N1" AND TRIM(x856.f1) = "ST" && Ship-to data

		cConsignee = UPPER(ALLTRIM(x856.f2))
		SELECT xpt
		m.st_name = cConsignee
		REPLACE xpt.consignee WITH  cConsignee IN xpt

		cStoreNum = ALLTRIM(x856.f4)
		IF EMPTY(xpt.shipins)
			REPLACE xpt.shipins WITH "STORENUM*"+cStoreNum IN xpt
		ELSE
			REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"STORENUM*"+cStoreNum IN xpt
		ENDIF
		IF EMPTY(xpt.dcnum)
			REPLACE xpt.dcnum WITH cStoreNum IN xpt
		ENDIF
		IF LEN(cStoreNum)>5
			REPLACE xpt.storenum  WITH VAL(RIGHT(cStoreNum,5)) IN xpt
		ELSE
			REPLACE xpt.storenum  WITH VAL(cStoreNum) IN xpt
		ENDIF

		SKIP 1 IN x856
		DO CASE
			CASE TRIM(x856.segment) = "N2"  && name ext. info
				m.name = UPPER(ALLTRIM(x856.f1))
				REPLACE xpt.NAME WITH m.name IN xpt
				SKIP 1 IN x856
				IF TRIM(x856.segment) = "N3"  && address info
					m.st_addr1 = UPPER(ALLTRIM(x856.f1))
					REPLACE xpt.address  WITH m.st_addr1 IN xpt
					m.st_addr2 = UPPER(ALLTRIM(x856.f2))
					REPLACE xpt.address2 WITH m.st_addr2 IN xpt
				ENDIF
				SKIP 1 IN x856
				IF TRIM(x856.segment) = "N4"  && address info
					m.zipbar = UPPER(ALLTRIM(x856.f3))
					m.st_csz = UPPER(ALLTRIM(x856.f1))+", "+UPPER(ALLTRIM(x856.f2))+" "+TRIM(x856.f3)
					REPLACE xpt.csz WITH m.st_csz IN xpt
					REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"COUNTRY*"+ALLTRIM(x856.f4) IN xpt
				ENDIF
			CASE TRIM(x856.segment) = "N3"  && address info
				m.st_addr1 = UPPER(ALLTRIM(x856.f1))
				REPLACE xpt.address  WITH m.st_addr1 IN xpt
				m.st_addr2 = UPPER(ALLTRIM(x856.f2))
				REPLACE xpt.address2 WITH m.st_addr2 IN xpt
				SKIP 1 IN x856
				IF TRIM(x856.segment) = "N4"  && address info
					m.zipbar = UPPER(ALLTRIM(x856.f3))
					m.st_csz = UPPER(TRIM(x856.f1))+", "+UPPER(TRIM(x856.f2))+" "+TRIM(x856.f3)
					REPLACE xpt.csz WITH m.st_csz IN xpt
					REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"COUNTRY*"+ALLTRIM(x856.f4) IN xpt
				ENDIF
			OTHERWISE
				WAIT WINDOW "UNKNOWN ADDRESS SEGMENT" TIMEOUT 2
				THROW
		ENDCASE
		SELECT x856
		SKIP
		LOOP
	ENDIF

	IF TRIM(x856.segment) = "N1" AND INLIST(TRIM(x856.f1),"SD","MA","PF") && Returned data
		cNom = ALLTRIM(x856.f1)
		IF lTesting AND cNom = "SD"
*SET STEP ON
		ENDIF
		REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+cNom+"_N1*"+ALLTRIM(x856.f2) IN xpt
		SKIP 1 IN x856

		DO CASE
			CASE TRIM(x856.segment) = "N2"
				REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+cNom+"_N2*"+ALLTRIM(x856.f1) IN xpt
				SKIP 1 IN x856
				IF TRIM(x856.segment) = "N3"
					REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+cNom+"_N3*"+ALLTRIM(x856.f1)+"|"+ALLTRIM(x856.f2) IN xpt
					SKIP 1 IN x856
				ENDIF
				IF TRIM(x856.segment) = "N4"
					REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+cNom+"_N4*"+ALLTRIM(x856.f1)+"|"+ALLTRIM(x856.f2)+"|"+ALLTRIM(x856.f3) IN xpt
				ENDIF

			CASE TRIM(x856.segment) = "N3"
				REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+cNom+"_N3*"+ALLTRIM(x856.f1)+"|"+ALLTRIM(x856.f2) IN xpt
				SKIP 1 IN x856
				IF TRIM(x856.segment) = "N4"
					REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+cNom+"_N4*"+ALLTRIM(x856.f1)+"|"+ALLTRIM(x856.f2)+"|"+ALLTRIM(x856.f3) IN xpt
				ENDIF

			CASE TRIM(x856.segment) = "N4"
				REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+cNom+"_N4*"+ALLTRIM(x856.f1)+"|"+ALLTRIM(x856.f2)+"|"+ALLTRIM(x856.f3) IN xpt
		ENDCASE

		SELECT x856
		SKIP
		LOOP
	ENDIF

	IF TRIM(x856.segment) = "PER"  && Personal contact
		IF INLIST(x856.f1,"AJ","RE")
			REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"CONTACT*"+UPPER(ALLTRIM(x856.f2)) IN xpt
		ENDIF
		IF x856.f3 = "TE"
			REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"PHONE*"+UPPER(ALLTRIM(x856.f4)) IN xpt
		ENDIF
		IF INLIST(x856.f5,"EA","EM")
			IF x856.f5 = "EA"
				REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"EDI_CONTACT*"+UPPER(ALLTRIM(x856.f6)) IN xpt
			ELSE
				REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"EMAIL*"+UPPER(ALLTRIM(x856.f6)) IN xpt
			ENDIF
		ENDIF
		SELECT x856
		SKIP
		LOOP
	ENDIF

	IF TRIM(x856.segment) = "N9"  && Additional info segments
		DO CASE
			CASE x856.f1 = "06"
				REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"GROUPCODE*"+ALLTRIM(x856.f2) IN xpt
			CASE x856.f1 = "TY"
				REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"ORDERTYPE*"+ALLTRIM(x856.f2) IN xpt
				REPLACE xpt.batch_num WITH ALLTRIM(x856.f2) IN xpt
				IF ALLTRIM(x856.f2) = "WEB"
					REPLACE xpt.residential WITH .T. IN xpt
				ENDIF
			CASE x856.f1 = "ZZ" AND lTesting
				REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"TESTCHAR*"+ALLTRIM(x856.f2) IN xpt
		ENDCASE
	ENDIF

	IF TRIM(x856.segment) = "G62" AND TRIM(x856.f1) == "37"  && start date
		lcDate = ALLTRIM(x856.f2)
		lxDate = SUBSTR(lcDate,5,2)+"/"+SUBSTR(lcDate,7,2)+"/"+LEFT(lcDate,4)
		ldDate = CTOD(lxDate)
		STORE ldDate TO dStart
		REPLACE xpt.START WITH ldDate IN xpt
		SELECT x856
		SKIP
		LOOP
	ENDIF

	IF TRIM(x856.segment) = "G62" AND TRIM(x856.f1) == "38"  && cancel date
		lcDate = ALLTRIM(x856.f2)
		lxDate = SUBSTR(lcDate,5,2)+"/"+SUBSTR(lcDate,7,2)+"/"+LEFT(lcDate,4)
		ldDate = CTOD(lxDate)
		STORE ldDate TO dCancel
		REPLACE xpt.CANCEL WITH ldDate IN xpt
		SELECT x856
		SKIP
		LOOP
	ENDIF

	IF TRIM(x856.segment) = "W66"
		REPLACE xpt.terms WITH ALLTRIM(x856.f1) IN xpt
		REPLACE carrcode WITH ALLTRIM(x856.f5) IN xpt
		newcode = .T.
		IF newcode
			xshipcode=PADR(ALLTRIM(x856.f5),18)

			if xsqlexec("select * from altscac where accountid=6612 and shipmode='"+xshipcode+"'",,,"wh") # 0
				REPLACE xpt.ship_via WITH altscac.shipname IN xpt
				REPLACE xpt.scac     WITH altscac.scaccode IN xpt
			ELSE
				REPLACE xpt.ship_via WITH ALLTRIM(x856.f5) IN xpt
			ENDIF
		ENDIF

**commented out bc should always use steelseries_shipcodes above, newcode is always .t. - mvw 02/09/18
*!*	** Add code here to xref the x856.f2 transport type code against a table
*!*			IF newcode = .F.
*!*				DO CASE
*!*					CASE ALLTRIM(x856.f5) = "UPSG"
*!*						REPLACE xpt.ship_via WITH "UPS GND RESIDENTIAL" IN xpt
*!*					CASE ALLTRIM(x856.f5) = "UPS2"  && IF WEB THEN RESIDENTIAL
*!*						REPLACE xpt.ship_via WITH "UPS 2ND DAY RESIDENTIAL" IN xpt
*!*					CASE ALLTRIM(x856.f5) = "C10G01"
*!*						REPLACE xpt.ship_via WITH "CSA GROUND" IN xpt
*!*					CASE ALLTRIM(x856.f5) = "C03G01"
*!*						REPLACE xpt.ship_via WITH "CEVA LOGISTICS GND" IN xpt
*!*					CASE ALLTRIM(x856.f5) = "U02G01"
*!*						REPLACE xpt.ship_via WITH "UPS GND" IN xpt
*!*					CASE ALLTRIM(x856.f5) = "CUSTADV"
*!*						REPLACE xpt.ship_via WITH "CUSTOMER ADVICE" IN xpt
*!*					CASE ALLTRIM(x856.f5) = "DO603"
*!*						REPLACE xpt.ship_via WITH "DHL EXPRESS" IN xpt
*!*					CASE ALLTRIM(x856.f5) = "F01G01"
*!*						REPLACE xpt.ship_via WITH "FEDEX GND" IN xpt
*!*					CASE ALLTRIM(x856.f5) = "D04G01"
*!*						REPLACE xpt.ship_via WITH "DHE EXPRESS" IN xpt
*!*					CASE ALLTRIM(x856.f5) = "S12G01"
*!*						REPLACE xpt.ship_via WITH "SWIFT TRANSPERTATION" IN xpt
*!*					CASE ALLTRIM(x856.f5) = "Y0SG01"
*!*						REPLACE xpt.ship_via WITH "YRC GROUND" IN xpt
*!*					CASE ALLTRIM(x856.f5) = "CUSTPIK"
*!*						REPLACE xpt.ship_via WITH "CUSTOMER PICKUP" IN xpt
*!*					CASE ALLTRIM(x856.f5) = "UPSWX"
*!*						REPLACE xpt.ship_via WITH "UPS WORLDWIDE EXPRESS SAVER" IN xpt
*!*					CASE ALLTRIM(x856.f5) = "UPSWEDP"
*!*						REPLACE xpt.ship_via WITH "UPS WW EXPRESS SAVER DUTY PAID" IN xpt
*!*					OTHERWISE
*!*						REPLACE xpt.ship_via WITH ALLTRIM(x856.f5) IN xpt
*!*				ENDCASE
*!*			ENDIF

		SELECT x856
		SKIP
		LOOP
	ENDIF

	IF TRIM(x856.segment) = "LX" && start of PT detail, stay here until this PT is complete
		ASSERT .F. MESSAGE cship_ref+" Detail, Type = "+IIF(lPrepack,"Prepack","Pickpack")+"...debug"
		m.units = IIF(lPrepack,.F.,.T.)
		lnVasCtr = 0
		lcVASStr = ""

		WAIT "NOW IN DETAIL LOOP PROCESSING" WINDOW NOWAIT

		SELECT xptdet
		CALCULATE MAX(ptdetid) TO m.ptdetid
		m.ptdetid = m.ptdetid + 1

		SELECT xptdet
		SKIP 1 IN x856
		lnLinenum=1
		DO WHILE ALLTRIM(x856.segment) != "SE"
			cSegment = TRIM(x856.segment)
			cCode = TRIM(x856.f1)
			IF TRIM(x856.segment) = "W01" && Destination Quantity
				SELECT xptdet
				APPEND BLANK
				REPLACE xptdet.ptid WITH xpt.ptid IN xptdet
				REPLACE xptdet.ptdetid WITH m.ptdetid IN xptdet
				REPLACE xptdet.units WITH m.units IN xptdet
				REPLACE xptdet.linenum WITH ALLTRIM(STR(lnLinenum)) IN xptdet
				lnLinenum = lnLinenum + 1
				m.ptdetid = m.ptdetid + 1
				REPLACE xptdet.accountid WITH xpt.accountid IN xptdet
				cUsePack = "1"
				REPLACE xptdet.PACK WITH cUsePack IN xptdet
				m.casepack = INT(VAL(cUsePack))
				REPLACE xptdet.casepack WITH m.casepack IN xptdet

				IF !lPrepack  && Pickpack
					REPLACE xptdet.totqty WITH INT(VAL(x856.f1)) IN xptdet
				ENDIF
				REPLACE xpt.qty WITH xpt.qty + xptdet.totqty IN xpt
				REPLACE xpt.origqty WITH xpt.qty IN xpt
				REPLACE xptdet.origqty WITH xptdet.totqty IN xptdet

				IF EMPTY(TRIM(xptdet.printstuff))
					REPLACE xptdet.printstuff WITH "UOM*"+ALLTRIM(x856.f2) IN xptdet
				ELSE
					REPLACE xptdet.printstuff WITH xptdet.printstuff+CHR(13)+"UOM*"+ALLTRIM(x856.f2) IN xptdet
				ENDIF

				IF ALLTRIM(x856.f4) = "IN"
					REPLACE xptdet.STYLE WITH ALLTRIM(x856.f5) IN xptdet
				ENDIF

				IF ALLTRIM(x856.f6) = "UP"
					REPLACE xptdet.upc WITH ALLTRIM(x856.f7) IN xptdet
				ENDIF

				IF ALLTRIM(x856.f8) = "VN"
					REPLACE xptdet.shipstyle WITH ALLTRIM(x856.f9) IN xptdet
					REPLACE xptdet.custsku WITH ALLTRIM(x856.f9) IN xptdet
				ENDIF
			ENDIF

			IF TRIM(x856.segment) = "N9" AND TRIM(x856.f1) = "CO"  && customer order #
				cR2 = ALLTRIM(x856.f2)
				REPLACE xptdet.printstuff WITH xptdet.printstuff+CHR(13)+"CUSTORDER*"+cR2 IN xpt
			ENDIF

			IF TRIM(x856.segment) = "G69" && desc
				cPrtstr = "DESC*"+UPPER(TRIM(x856.f1))
				REPLACE xptdet.printstuff WITH xptdet.printstuff+CHR(13)+cPrtstr IN xptdet
			ENDIF

			IF TRIM(x856.segment) = "AMT"  && item cost
				cCost = ALLTRIM(x856.f2)
				REPLACE xptdet.printstuff WITH xptdet.printstuff+CHR(13)+"ITEMCOST*"+cCost IN xptdet
			ENDIF
&& AMT*1*93.70

			IF TRIM(x856.segment) = "N9"  AND ALLTRIM(x856.f1) = "ZZ"
				lnVasCtr = lnVasCtr+1
				ThisVAS = ALLTRIM(x856.f2)
				DO CASE
					CASE ThisVAS = "VA01"
						REPLACE xpt.v01 WITH .T. IN xpt
						IF !"COVER IMPORT MASTER CARTON LABEL ALL SIDES"$xpt.shipins
							REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"VASCODE*COVER IMPORT MASTER CARTON LABEL ALL SIDES" IN xpt
						ENDIF
					CASE ThisVAS = "VA02"
						REPLACE xpt.v02 WITH .T. IN xpt
						IF !"VASCODE*COVER IMPORT MASTER CARTON LABEL AND INNERS"$xpt.shipins
							REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"VASCODE*COVER IMPORT MASTER CARTON LABEL AND INNERS" IN xpt
						ENDIF
				ENDCASE
				REPLACE xptdet.printstuff WITH xptdet.printstuff+CHR(13)+"VAS"+ALLTRIM(TRANSFORM(lnVasCtr))+"*"+ALLTRIM(x856.f2) IN xptdet
				IF lnVasCtr = 1
					IF !"VASFLAG*TRUE"$xpt.shipins
						REPLACE xpt.vas WITH .T.
						REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"VASFLAG*TRUE" IN xpt
					ENDIF
					lcVASStr = ALLTRIM(x856.f2)
					REPLACE xpt.vas WITH .T.
				ELSE
					IF !ALLTRIM(x856.f2)$lcVASStr
						lcVASStr= lcVASStr +","+ALLTRIM(x856.f2)
					ENDIF
					REPLACE xpt.vas WITH .T.
				ENDIF
			ENDIF

			SELECT x856
			SKIP 1 IN x856
			IF ALLTRIM(x856.segment) = "SE"
				IF !EMPTY(lcVASStr)
					REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"ALLVAS*"+ALLTRIM(lcVASStr) IN xpt
				ENDIF
				lnVasCtr = 0
				lcVASStr = ""
			ENDIF

		ENDDO

		SELECT xptdet
	ENDIF

	SELECT x856
	IF !EOF()
		SKIP 1 IN x856
	ENDIF
ENDDO

SELECT xptdet

rollup()
WAIT WINDOW "At end of ROLLUP procedure" NOWAIT
*lbrowfiles = .t.

IF lBrowfiles
	WAIT WINDOW "If tables are incorrect, CANCEL when XPTDET window opens"+CHR(13)+"Otherwise, will load PT table" TIMEOUT 2
	SELECT xpt
	LOCATE
	BROWSE
	SELECT xptdet
	LOCATE
	BROWSE
*  cancel
ENDIF

SET STEP ON

WAIT WINDOW cCustname+" Breakdown Round complete..." NOWAIT
SELECT x856
IF USED("PT")
	USE IN pt
ENDIF
IF USED("PTDET")
	USE IN ptdet
ENDIF
WAIT CLEAR
RETURN

*************************************************************************************************
*!* Error Mail procedure - Cancel Before Start Date
*************************************************************************************************
PROCEDURE errordates

	IF lEmail
		tsendto = tsendtotest
		tcc =  tcctest
		cMailLoc = IIF(cOffice = "C","West",IIF(cOffice = "N","New Jersey","Florida"))
		tsubject= "TGF "+cMailLoc+" "+cCustname+" Pickticket Error: " +TTOC(DATETIME())
		tmessage = "File: "+cFilename+CHR(13)+" has Cancel date(s) which precede Start date(s)"
		tmessage = tmessage + "File needs to be checked and possibly resubmitted."
		DO FORM m:\dev\frm\dartmail2 WITH tsendto,tFrom,tsubject,tcc,tattach,tmessage,"A"
	ENDIF
ENDPROC

******************************
PROCEDURE rollup
******************************
	ASSERT .F. MESSAGE "In rollup procedure"
	nChangedRecs = 0
	SELECT xptdet
	SET DELETED ON
	LOCATE
	IF lTesting
*BROWSE
	ENDIF
	cMsgStr = "PT/OSID/ODIDs changed: "
	SCAN FOR !DELETED()
		SCATTER MEMVAR
		nRecno = RECNO()
		LOCATE FOR xptdet.ptid = m.ptid ;
			AND xptdet.STYLE = m.style ;
			AND xptdet.COLOR = m.color ;
			AND xptdet.ID = m.id ;
			AND xptdet.PACK = m.pack ;
			AND xptdet.upc = m.upc ;
			AND RECNO('xptdet') > nRecno
		IF FOUND()
			SELECT xpt
			LOCATE FOR xpt.ptid = m.ptid
			IF !FOUND()
				WAIT WINDOW "PROBLEM!"
			ENDIF
			cship_ref = ALLT(xpt.ship_ref)
			IF nChangedRecs > 10
				cMsgStr = "More than 10 records rolled up"
			ELSE
				cMsgStr = cMsgStr+CHR(13)+cship_ref+" "+TRANSFORM(m.ptid)+" "+TRANSFORM(m.ptdetid)
			ENDIF
			nChangedRecs = nChangedRecs+1
			SELECT xptdet
			nTotqty1 = xptdet.totqty
			DELETE NEXT 1 IN xptdet
		ENDIF
		IF !EOF()
			GO nRecno
			REPLACE xptdet.totqty WITH xptdet.totqty+nTotqty1 IN xptdet
			REPLACE xptdet.origqty WITH xptdet.totqty IN xptdet
		ELSE
			GO nRecno
		ENDIF
	ENDSCAN

	IF nChangedRecs > 0
		WAIT WINDOW cMsgStr TIMEOUT 1
		WAIT WINDOW "There were "+TRANS(nChangedRecs)+" records rolled up" TIMEOUT 1
	ELSE
		WAIT WINDOW "There were NO records rolled up" TIMEOUT 1
	ENDIF

ENDPROC
