* This program extracts return data for the month
utilsetup("MJ_RETURNS")
SET EXCLUSIVE OFF 
SET DELETED ON 
SET TALK OFF  
SET SAFETY OFF   &&Allows for overwrite of existing files
xyear=YEAR(DATE())
xmonth=month(date())
xcmonth=cmonth(Date())


*!*	if date()={12/1/13}
*!*		xmonth=month(date()-1)
*!*		xcmonth=cmonth(date()-1)
*!*	endif
close databases all
goffice='J'
useca("inwolog","wh",,,"mod='"+goffice+"' and  accountid in (6303,6325,6543) and confirmdt>{"+dtoc(date()-90)+"}")
useca("indet","wh",,,"mod='"+goffice+"' and  accountid in (6303,6325,6543) and date_rcvd>{"+dtoc(date()-90)+"}")

*!*	SELECT inwo.wo_num, inwo.accountid, inwo.confirmdt as date, ind.style, ind.color, ind.id, ind.totqty, brokerref as return_id,space(10) as RA;
*!*	 FROM inwolog inwo, indet ind WHERE inwo.wo_num=ind.wo_num AND returns and !empty(confirmdt) AND inwo.wo_num=3520822;
*!*	into cursor xtemp readwrite
SELECT SPACE(5) as division, inwo.wo_num, inwo.accountid, inwo.confirmdt as date, ind.style, ind.color, ind.id, Space(13) as upc, ind.totqty, brokerref as return_id,; 
CONTAINER,REFERENCE, ACCT_REF, space(50) as tt, space(50) as ttp , comments, printcomments; 
FROM inwolog inwo, indet ind WHERE inwo.wo_num=ind.wo_num AND zreturns and !emptynul(confirmdt) AND MONTH(date_rcvd)=xmonth AND YEAR(date_rcvd)=xyear AND ind.totqty>0 ;
into cursor xtemp readwrite

replace all tt with strtran(comments,chr(13),";")
replace all ttp with strtran(Printcomments,chr(13),";")


Select xtemp
Scan

IF upcmastsql(6303, xtemp.style,   xtemp.color,  xtemp.id )
    replace xtemp.upc with upcmast.upc in xtemp
    If Empty(upcmast.upc)
      alength = ALINES(aupc,upcmast.INFO,.T.,CHR(13))
      cUPC    = ALLTRIM(segmentget(@aupc,"UPC",alength))
      replace xtemp.upc with cUPC in xtemp
    EndIf   
  Else
    replace xtemp.upc with "UNK"
  EndIf   
  
************************begin division capture
    If Empty( xtemp.division)
      alength = ALINES(aupc,upcmast.INFO,.T.,CHR(13))
      lcdiv   = ALLTRIM(segmentget(@aupc,"DIV",alength))
      Replace division With lcdiv In xtemp
  Else
    replace xtemp.division with "UNK"
  EndIf   
************************end division capture
Endscan
set step on
*scan
*xtemp.wo_num=inwolog.wo_num
 *   replace RA with getmemodata("inwolog.comments","RA#:") in xtemp
*ENDSCAN
		If Reccount() > 0 
		export TO "S:\MarcJacobsData\Reports\EOMReports\Returns_" +xcmonth TYPE xls 
		tsendto = "tmarg@fmiint.com"
		tattach = "" 
		tcc =""                                                                                                                               
		tFrom ="Toll EDI System Operations <fmi-transload-ops@fmiint.com>"                                
		tmessage = "MJ_Returns_:"+Ttoc(Datetime())        
		tSubject = "MJ RETURNS REPORT COMPLETE"                                                           
		Do Form m:\dev\frm\dartmail2 With tsendto,tFrom,tSubject, tcc,tattach,tmessage  ,"A"     
			
	ELSE 
		export TO "S:\MarcJacobsData\Reports\EOMReports\no_returns_for_" + xcmonth TYPE xls
		tsendto = "tmarg@fmiint.com"
		tattach = "" 
		tcc =""                                                                                                                               
		tFrom ="Toll EDI System Operations <fmi-transload-ops@fmiint.com>"                                
		tmessage = "MJ_Returns_ "+Ttoc(Datetime())        
		tSubject = "MJ RETURNS REPORT COMPLETE"                                                           
		Do Form m:\dev\frm\dartmail2 With tsendto,tFrom,tSubject, tcc,tattach,tmessage  ,"A"   

	ENDIF
schedupdate()
_screen.Caption=gscreencaption
on error



****************************
PROCEDURE segmentget
****************************

  PARAMETER thisarray,lcKey,nLength

  FOR i = 1 TO nLength
    IF i > nLength
      EXIT
    ENDIF
    lnEnd= AT("*",thisarray[i])
    IF lnEnd > 0
      lcThisKey =TRIM(SUBSTR(thisarray[i],1,lnEnd-1))
      IF OCCURS(lcKey,lcThisKey)>0
        RETURN SUBSTR(thisarray[i],lnEnd+1)
        i = 1
      ENDIF
    ENDIF
  ENDFOR

  RETURN ""



schedupdate()
_screen.Caption=gscreencaption
on error
