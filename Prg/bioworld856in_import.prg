Select xinwolog
Set Order To inwologid
Locate
Store Reccount() To lnCount
Wait Window At 10,10 "Container lines to load ...........  "+Str(lnCount) Nowait Timeout 1

nUploadcount = 0
cRecs = Alltrim(Str(lnCount))

*ASSERT .F. MESSAGE "At XINWOLOG Import loop...debug"
tmessage = ""
cCtrString = ""
cCtrString = Padr("WO NUM",10)+Padr("CONTAINER",15)+Padr("CTNS",6)+"EACHES"+Chr(13)

set step on

Scan  && Currently in XINWOLOG table
  cContainer = Allt(xinwolog.Container)
  cCtnQty = Allt(Str(xinwolog.plinqty))
  cUnitQty = Allt(Str(xinwolog.plunitsinqty))

  Wait "AT XINWOLOG RECORD "+Alltrim(Str(Recno()))+" OF "+cRecs Window Nowait Noclear
  Select xinwolog
  Scatter Memvar Memo
  Select inwolog
  Set Order To

  nwo_num = dygenpk("wonum",cWhse)
  m.wo_num = nwo_num
  cWO_Num = Allt(Str(m.wo_num))
  cCtrString = cCtrString+Chr(13)+Padr(cWO_Num,10)+Padr(cContainer,15)+Padr(cCtnQty,6)+cUnitQty  && ,cUnitQty)

  If xgMod ="L"
    m.mod = "L"
    m.office = "L"
  Else
    m.mod = cMod
    m.office = cOffice
  Endif

  insertinto("inwolog","wh",.T.)
  If xgMod ="L"
    replace mod With "L" In inwolog
    replace office With "L" In inwolog
  endif


  nUploadcount = nUploadcount + 1

  Select xpl
  Scan For inwologid = xinwolog.inwologid
    Scatter Memvar Memo
    m.wo_num = nwo_num
    If xgMod ="L"
      m.mod = "L"
      m.office = "L"
    Else
      m.mod = cMod
      m.office = cOffice
    Endif
    m.inwologid = inwolog.inwologid
    insertinto("pl","wh",.T.)
    If xgMod ="L"
      replace mod With "L" In pl
      replace office With "L" In pl
    endif
  Endscan
Endscan
cVendorName = ""
Wait Clear

Wait Window At 10,10 "Container records loaded...........  "+Str(nUploadcount) Timeout 2

Wait Clear

If nUploadcount > 0
  tu("inwolog")
  tu("pl")

*  SET STEP ON
  tsubject= "Inbound WO(s) created for "+cMailname
  If lTesting
    tsubject= tsubject+" (TEST) at "+Ttoc(Datetime())
  Else
    Do Case
    Case m.office = "L"
      tsubject = tsubject+" (ML) at "+Ttoc(Datetime())
    Case cOffice = "C"
      tsubject = tsubject+" (SP) at "+Ttoc(Datetime())
    Case cOffice = "Y"
      tsubject = tsubject+" (C2) at "+Ttoc(Datetime())
    Case cOffice = "I"
      tsubject = tsubject+" (NJ) at "+Ttoc(Datetime())
    Otherwise
      tsubject = tsubject+" (UNK) at "+Ttoc(Datetime())
    Endcase
  Endif
  tattach = ""
  tmessage = cCtrString
  tmessage = tmessage + Chr(13) + Chr(13) + "From file: "+cFilename
  If lTesting
    tmessage = tmessage + Chr(13) + "Data is in F:\WHP\WHDATA"
  Endif
  tFrom ="Toll EDI Operations <toll-edi-ops@tollgroup.com>"
  If lDoMail
    Do Form m:\dev\frm\dartmail2 With tsendto,tFrom,tsubject,tcc,tattach,tmessage,"A"
  Endif
  Release cCtrString,tmessage
  Wait cMailname+" Inbound Import Process Complete for file "+cFilename Window Timeout 2
Endif

Return
