parameter whichaccount
SET EXCLUSIVE OFF 
SET DELETED ON 
SET TALK OFF  
SET SAFETY OFF
*whichaccount = 6303

whichaccount = val(alltrim(transform(whichaccount)))

close data all
do case
case whichaccount = 10100
	mwhseacct=6303
	maccountid=6303
	mmod='J'
case whichaccount = 10180
	mwhseacct=6325
	maccountid=6325
	mmode='J'
case whichaccount = 10190
	mwhseacct=6543
	maccountid=6543
	mmode='J'
case whichaccount = 11100
	mwhseacct=6303
	maccountid=6303
	mmod='L'
case whichaccount = 11180
	mwhseacct=6325
	maccountid=6325
	mmode='L'
case whichaccount = 11190
	mwhseacct=6543
	maccountid=6543
	mmode='L'
otherwise
	acctnam = "UNK"
endcase
*********CA
if inlist(whichaccount,11100,11180,11190)

	if !used("phys")
		xsqlexec("select * from phys where mod='L'",,,"wh")
	endif
	if !used("FROZ2017")
		use s:\marcjacobsdata\reports\inventorysyncfiles\bi\prod\frozen\froz2017ml.dbf in 0  alias froz2017
	endif

*!*		If !Used("FROZ2015")
*!*			Use H:\MJ\Inventory\Pi\FROZ2015ML.Dbf In 0 Alias FROZ2015
*!*		Endif
endif

*********NJ
if inlist(whichaccount,10100,10180,10190)
	if !used("phys")
		xsqlexec("select * from phys where mod='J'",,,"wh")
	endif
	if !used("FROZ2017")
		use s:\marcjacobsdata\reports\inventorysyncfiles\bi\prod\frozen\froz2017.dbf in 0
	endif

endif

upcmastsql(6303)


select whseacct, style,color,id,sum(locqty) as locqty from froz2017 where whseacct=mwhseacct group by 1,2,3,4 into cursor t1 readwrite
select accountid , style,color,id,sum(totqty) as final_count from phys where accountid=maccountid and zcount='F' group by 1,2,3,4 into cursor p1 readwrite
delete for final_count=0
select * from t1 a full join p1 b on a.whseacct=b.accountid and a.style=b.style and a.color=b.color and a.id=b.id into cursor t2 readwrite
replace whseacct with accountid for isnull(whseacct) in t2
replace style_a with style_b for isnull(style_a) in t2
replace color_a with color_b for isnull(color_a) in t2
replace id_a with id_b for isnull(id_a) in t2
replace final_count with 0 for isnull(final_count) in t2
select whseacct,style_a as style, color_a as color,id_a as size, locqty, final_count from t2 into cursor t3 readwrite
replace locqty with 0 for isnull(locqty) in t3
**************************************************************for frozen snapshot prior to PI for MJ
*replace final_count WITH 0 FOR final_count !=0  &&&  uncomment for frozen
select  whseacct, 000000 as axacct, space(18) as upc,style,color, size, space(4) as div,locqty as frozen,;
	final_count, locqty-final_count as variance from t3  into cursor t4 readwrite
select a.*, b.upc as upc1 , info from t4 a left join upcmast b on a.style=b.style and a.color=b.color and ;
	a.size=b.id and b.accountid=6303 into cursor c1 readwrite
replace upc1 with 'no upc' for isnull(upc1) in c1
replace all upc with upc1 in c1
replace all div with getmemodata("info","DIV")

replace axacct with 10100,  whseacct with 10100 for whichaccount =10100
replace axacct with 10180 , whseacct with 10180 for whichaccount =10180
replace axacct with 10190,  whseacct with 10190 for whichaccount =10190

replace axacct with 11100,  whseacct with 11100 for whichaccount =11100
replace axacct with 11180 , whseacct with 11180 for whichaccount =11180
replace axacct with 11190,  whseacct with 11190 for whichaccount =11190
*SET STEP ON
**for testing
*  REPLACE final_count WITH frozen in c1 for !ISNULL(style)

if whichaccount= 10100
	select whseacct, axacct, upc,style,color,size, div, frozen, final_count, variance from c1  into cursor c2 readwrite
	copy to "s:\marcjacobsdata\reports\inventorysyncfiles\BI\prod\PI_Sync_Toll_10100_"+ttoc(datetime(),1)  type csv
else
	if whichaccount= 10180
		select whseacct, axacct, upc,style,color,size, div, frozen, final_count, variance from c1  into cursor c2 readwrite
		copy to "s:\marcjacobsdata\reports\inventorysyncfiles\BI\prod\PI_Sync_Toll_10180_"+ttoc(datetime(),1)  type csv
	else
		if whichaccount=10190
			select whseacct, axacct, upc,style,color,size, div, frozen, final_count, variance from c1  into cursor c2 readwrite
			copy to "s:\marcjacobsdata\reports\inventorysyncfiles\BI\prod\PI_Sync_Toll_10190_"+ttoc(datetime(),1)  type csv
		else
			if whichaccount= 11100
				select whseacct, axacct, upc,style,color,size, div, frozen, final_count, variance from c1  into cursor c2 readwrite
				copy to "s:\marcjacobsdata\reports\inventorysyncfiles\BI\prod\PI_Sync_Toll_11100_"+ttoc(datetime(),1)  type csv
			else
				if whichaccount= 11180
					select whseacct, axacct, upc,style,color,size, div, frozen, final_count, variance from c1  into cursor c2 readwrite
					copy to "s:\marcjacobsdata\reports\inventorysyncfiles\BI\prod\PI_Sync_Toll_11180_"+ttoc(datetime(),1)  type csv
				else
					if whichaccount= 11190
						select whseacct, axacct, upc,style,color,size, div, frozen, final_count, variance from c1  into cursor c2 readwrite
						copy to "s:\marcjacobsdata\reports\inventorysyncfiles\BI\prod\PI_Sync_Toll_11190_"+ttoc(datetime(),1)  type csv
					else
					endif
				endif
			endif
		endif
	endif
endif


