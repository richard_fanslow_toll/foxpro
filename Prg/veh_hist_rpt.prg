LPARAMETERS tcVehno, tcReportMode, tcVehicleType
*ON ERROR

LOCAL lcCentury, lnYearsInService, lnTotOil, lnTotParts, lnTotTires
LOCAL ARRAY laOrdhdr(1)

PRIVATE pcVehicleYear

* determine vehicle year
DO CASE
	CASE UPPER(ALLTRIM(tcVehicleType)) == "TRUCK"
		pcVehicleYear = TRUCKS.YEAR
	CASE UPPER(ALLTRIM(tcVehicleType)) == "TRAILER"
		pcVehicleYear = TRAILER.YEAR
	CASE UPPER(ALLTRIM(tcVehicleType)) == "FORKLIFT"
		pcVehicleYear = VEHMAST.YEAR
	OTHERWISE
		pcVehicleYear = "?"
ENDCASE

lcCentury = SET('CENTURY')

SET CENTURY ON

IF NOT USED('ordhdr')
	USE SH!ordhdr IN 0
ENDIF

IF NOT USED('repdetl')
	USE SH!repdetl IN 0 ORDER repdate
ENDIF

IF NOT USED('tiredetl')
	USE SH!tiredetl IN 0 ORDER tiredate
ENDIF

IF NOT USED('oildetl')
	USE SH!oildetl IN 0 ORDER oildate
ENDIF

SELECT ordhdr
SET ORDER TO vehno
SEEK tcVehno
IF !FOUND()
	=MESSAGEBOX("Couldn't find this vehicle in the Work Orders ",0+48,"Fleet Maintenance")
	RETURN
ENDIF

IF USED('temp') THEN
	USE IN temp
ENDIF

SELECT ordhdr
=AFIELDS(laOrdhdr)
CREATE CURSOR temp FROM ARRAY laOrdhdr
ALTER TABLE temp ADD COLUMN start_mile N(10)
ALTER TABLE temp ADD COLUMN end_mile   N(10)
ALTER TABLE temp ADD COLUMN st_date    d
ALTER TABLE temp ADD COLUMN end_date   d
ALTER TABLE temp ADD COLUMN service    N(4,2)
ALTER TABLE temp ADD COLUMN oilsvc     N(4)
ALTER TABLE temp ADD COLUMN repairsvc  N(4)
ALTER TABLE temp ADD COLUMN tiresvc    N(4)
ALTER TABLE temp ADD COLUMN pmsvc      N(4)
ALTER TABLE temp ADD COLUMN wsvc       N(4)
ALTER TABLE temp ADD COLUMN washsvc    N(4)
ALTER TABLE temp ADD COLUMN vehtype    C(8)

SELECT ordhdr
SET ORDER TO vehord
SCAN FOR vehno = tcVehno
	WAIT WINDOW "Processing Work Order " + TRIM(STR(ordno)) NOWAIT
	SELECT ordhdr
	SCATTER MEMVAR
	INSERT INTO temp FROM MEMVAR
	STORE 0 TO lnTotOil, lnTotParts, lnTotTires

	SELECT oildetl
	SET ORDER TO ordno
	SEEK ordhdr.ordno
	IF FOUND()
		SCAN FOR ordno = ordhdr.ordno
			lnTotOil = lnTotOil + (oilqty * unitprice)
		ENDSCAN
	ENDIF

	SELECT repdetl
	SET ORDER TO ordno
	SEEK ordhdr.ordno
	IF FOUND()
		SCAN FOR ordno = ordhdr.ordno
			lnTotParts = lnTotParts + (repqty * unitprice)
		ENDSCAN
	ENDIF

	SELECT tiredetl
	SET ORDER TO ordno
	SEEK ordhdr.ordno
	IF FOUND()
		SCAN FOR ordno = ordhdr.ordno
			lnTotTires = lnTotTires + (tireqty * unitprice)
		ENDSCAN
	ENDIF

	REPLACE temp.tirecost WITH lnTotTires, temp.partcost WITH lnTotParts, temp.oilcost WITH lnTotOil IN temp

ENDSCAN

SELECT ordhdr
SEEK tcVehno

DO WHILE ordhdr.vehno = tcVehno
	SKIP 1
ENDDO

SKIP -1
SELECT temp
REPLACE ALL end_mile WITH ordhdr.mileage, end_date WITH ordhdr.crdate

SELECT ordhdr
SEEK tcVehno

SELECT temp
REPLACE ALL start_mile WITH ordhdr.mileage, st_date WITH ordhdr.crdate
SKIP -1
lnYearsInService = (temp.end_date - temp.st_date)/365
REPLACE ALL temp.service WITH lnYearsInService

SELECT temp
COUNT FOR mainttype = "P" TO pmService
COUNT FOR mainttype = "O" TO oilService
COUNT FOR mainttype = "R" TO repairService
COUNT FOR mainttype = "T" TO tireService
COUNT FOR mainttype = "W" TO wartyService
COUNT FOR mainttype = "C" TO WashService

REPLACE ALL temp.oilsvc WITH oilService, ;
	temp.repairsvc WITH repairService, ;
	temp.tiresvc   WITH tireService, ;
	temp.wsvc      WITH wartyService, ;
	temp.pmsvc     WITH pmService, ;
	temp.washsvc   WITH WashService

SELECT temp
REPLACE ALL vehtype WITH tcVehicleType
GOTO TOP

WAIT CLEAR
KEYBOARD '{CTRL+F10}' CLEAR
IF tcReportMode = "FULL"
	*REPORT FORM F:\SHOP\SHREPORTS\veh_hist_rpt PREVIEW NOCONSOLE
	REPORT FORM veh_hist_rpt PREVIEW NOCONSOLE
ENDIF
IF tcReportMode = "SUMMARY"
	*REPORT FORM F:\SHOP\SHREPORTS\veh_hist_rpt2 PREVIEW NOCONSOLE
	REPORT FORM veh_hist_rpt2 PREVIEW NOCONSOLE
ENDIF

SET CENTURY &lcCentury
RETURN
