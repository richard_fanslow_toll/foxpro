PARAMETERS nWO_Num,cOffice
PUBLIC c_CntrlNum,c_GrpCntrlNum,cWO_Num,cGroupName,cDivision,cCustName,cFilename,div214,cST_CSZ
PUBLIC tsendto,tcc,tsendtoerr,tccerr

cFilename = ""
lTesting = .f.
lFilesOut = .F.
lUseTestData = .T.
IF !USED('account')
	xsqlexec("select * from account where inactive=0","account",,"qq")
	index on accountid tag accountid
	set order to
ENDIF

ON ESCAPE CANCEL
ON ERROR THROW
lFClose = .T.
lIsError = .F.
lDoCatch = .T.
cShip_ref = ""
cST_CSZ = ""
nFilenum = 0

*ASSERT .f.
TRY
	cSystemName = "UNASSIGNED"
	nAcctNum = 5837
	cRunType = "P" && Pickup from pier
	cWO_Num = ""
	lEmail = .F.
	lDoError = .F.
	IF VARTYPE(nWO_Num)= "L"
		IF lTesting
			CLOSE DATABASES ALL
			lFilesOut = .F.
			nWO_Num = 1253262
			cOffice = "L"
			cRunType = "P"
			nTripId =  947942
		ELSE
			WAIT WINDOW "No params specified" TIMEOUT 2
			lDoCatch = .F.
			THROW
		ENDIF
	ENDIF

	cDivision = IIF(cOffice="C","California",IIF(cOffice="M","Miami","New Jersey"))
	cOffCity = IIF(cOffice="C","SAN PEDRO",IIF(cOffice="M","MIAMI","CARTERET"))
	cOffState = IIF(cOffice="C","CA",IIF(cOffice="M","FL","NJ"))

	xReturn = "XXX"
	DO m:\dev\prg\wf_alt WITH cOffice,nAcctNum
	IF EMPTY(ALLTRIM(xReturn))
		DO errormail WITH "NO WHSE"
		THROW
	ENDIF
	cUseFolder = UPPER(xReturn)

	cWO_Num = ALLTRIM(STR(nWO_Num))
	cProcType = "PICKUP"

	DIMENSION thisarray(1)

	tfrom = "TGF Warehouse Operations <warehouse-ops@fmiint.com>"
	tsendto = "joe.bianchi@tollgroup.com"
	tcc = ""
	tattach = ""
	STORE "" TO cST_CSZ,cST_Name,cST_Addr,cSF_CSZ,cSF_Name,cSF_Addr
	STORE "" TO cPUDLocCity,cPUDLocState,cPUDLocZip
	STORE "" TO cFMICity,cFMIState,cFMIZip

	cCustName = "SEARS"
	cMailName = "SEARS"
	cCustFolder = "SEARS"
	cCustPrefix = cCustName+"214"+LOWER(cRunType)+"_"+LOWER(cOffice)
	dt1 = TTOC(DATETIME(),1)
	dtmail = TTOC(DATETIME())
	dt2 = DATETIME()

	cFilename = ("f:\ftpusers\"+cCustFolder+"\214-Staging\"+cCustPrefix+dt1+".txt")
	cFilenameShort = JUSTFNAME(cFilename)
	cFilename2 = ("f:\ftpusers\"+cCustFolder+"\214OUT\archive\"+cFilenameShort)
	cFilename3 = ("f:\ftpusers\"+cCustFolder+"\214OUT\"+cFilenameShort)
	nFilenum = FCREATE(cFilename)

*!* Open the Tables

	xsqlexec("select * from pudlocs",,,"wo")
	index on accountid tag accountid
	index on str(accountid,4)+str(dash,4) tag punum
	set order to

	IF !USED('wolog')
		SELECT 0
		USE F:\wo\wodata\wolog ALIAS wolog ORDER TAG wo_num SHARED NOUPDATE
	ENDIF

	cfd = "*"  && Field delimiter
	csegd = "~"  && Segment delimiter
	cterminator = ">"  && Used at end of ISA segment

	SELECT wolog
	SEEK nWO_Num
	SCATTER FIELDS accountid,fromloc,toloc MEMVAR
	IF cRunType = "D"
		STORE m.toloc TO nLocCode
	ELSE
		STORE m.fromloc TO nLocCode
	ENDIF

	IF EMPTY(nLocCode)
		nLocCode = 4145
	ENDIF
	IF !SEEK(nWO_Num)
		DO errormail WITH "MISSWO"
		THROW
	ELSE
		SELECT pudlocs
		LOCATE
		LOCATE FOR pudlocs.accountid = m.accountid AND pudlocs.dash = nLocCode
		ASSERT .F. MESSAGE "At PUDLoc process...debug"
		IF FOUND()
			cPUDLocCity = UPPER(ALLTRIM(pudlocs.city))
			cPUDLocState = UPPER(ALLTRIM(pudlocs.state))
			cPUDLocZip = ALLTRIM(pudlocs.zip)
			IF cRunType = "D"
				cST_CSZ = cPUDLocCity+cfd+cPUDLocState+cfd+cPUDLocZip
				cST_Name = UPPER(ALLTRIM(pudlocs.acct_name))
				cST_Addr = UPPER(ALLTRIM(pudlocs.acct_name2))
			ELSE
				cSF_CSZ = cPUDLocCity+cfd+cPUDLocState+cfd+cPUDLocZip
				cSF_Name = UPPER(ALLTRIM(pudlocs.acct_name))
				cSF_Addr = UPPER(ALLTRIM(pudlocs.acct_name2))
			ENDIF
		ELSE
			DO errormail WITH "MISS PUDLOC INFO"
			THROW
		ENDIF
	ENDIF

	DO CASE
		CASE cOffice = "L"
			cFMIAddr = "3355 DULLES DR"
			cFMICity = "MIRA LOMA"
			cFMIState = "CA"
			cFMIZip = "91752"
		CASE cOffice = "C"
			cFMIAddr = "350-400 WESTMONT DR"
			cFMICity = "SAN PEDRO"
			cFMIState = "CA"
			cFMIZip = "90731"
		CASE cOffice = "N"
			cFMIAddr = "800 FEDERAL BLVD"
			cFMICity = "CARTERET"
			cFMIState = "NJ"
			cFMIZip = "07008"
		CASE cOffice = "M"
			cFMIAddr = "2100 NW 129TH AVE SUITE 100"
			cFMICity = "MIAMI"
			cFMIState = "FL"
			cFMIZip = "33182"
	ENDCASE

	IF cRunType = "D"
		STORE "TGF INC." TO cSF_Name
		STORE cFMIAddr TO cSF_Addr
		STORE cFMICity+cfd+cFMIState+cfd+cFMIZip TO cSF_CSZ
	ELSE
		STORE "TGF INC." TO cST_Name
		STORE cFMIAddr TO cST_Addr
		STORE cFMICity+cfd+cFMIState+cfd+cFMIZip TO cST_CSZ
	ENDIF

	csendqual = "02"  && Sender qualifier
	csendid = "FMIT"  && Sender ID code
	crecqual = "ZZ"  && Recip qualifier
	crecid = "CHINASHIP"   && Recip ID Code
	cDate = DTOS(DATE())
	cTruncDate = RIGHT(cDate,6)
	cTruncTime = SUBSTR(TTOC(DATETIME(),1),9,4)
	cfiledate = cDate+cTruncTime
	csendidlong = PADR(csendid,15," ")
	crecidlong = PADR(crecid,15," ")
	cString = ""

	nSTCount = 0
	nSegCtr = 0
	nLXNum = 1
	STORE "" TO cEquipNum,cHAWB

	WAIT CLEAR
	WAIT WINDOW "Now creating Trucking WO#-based 214 "+IIF(cRunType="D","Delivery","Pickup")+" information..." NOWAIT

	IF lTesting
		cISACode = "T"
	ELSE
		cISACode = "P"
	ENDIF

	DO num_incr_isa
	cISA_Num = PADL(c_CntrlNum,9,"0")
	STORE "ISA"+cfd+"00"+cfd+"          "+cfd+"00"+cfd+"          "+cfd+csendqual+cfd+csendidlong+cfd+;
		crecqual+cfd+crecidlong+cfd+cTruncDate+cfd+cTruncTime+cfd+"^"+cfd+"00401"+cfd+;
		cISA_Num+cfd+"0"+cfd+cISACode+cfd+":"+csegd TO cString  && +cfd+cterminator
	DO cstringbreak

	STORE "GS"+cfd+"QM"+cfd+csendid+cfd+crecid+cfd+cDate+cfd+cTruncTime+cfd+c_CntrlNum+;
		cfd+"X"+cfd+"004010"+csegd TO cString
	DO cstringbreak
	DO num_incr_st

	STORE "ST"+cfd+"214"+cfd+PADL(c_GrpCntrlNum,9,"0")+csegd TO cString
	DO cstringbreak
	nSTCount = nSTCount + 1
	nSegCtr = nSegCtr + 1

	SELECT wolog
	SEEK(nWO_Num)
	cRefID = ALLT(STR(wolog.wo_num))
	cBL = ALLTRIM(wolog.brokref)

	STORE "B10"+cfd+cRefID+cfd+cBL+cfd+csendid+csegd TO cString
	DO cstringbreak
	nSegCtr = nSegCtr + 1

	IF cRunType = "D"
		STORE "N1"+cfd+"SH"+cfd+cSF_Name+csegd TO cString
	ELSE
		STORE "N1"+cfd+"SH"+cfd+cSF_Name+cfd+"ZZ"+cfd+"USMIKFMII"+csegd TO cString
	ENDIF
	DO cstringbreak
	nSegCtr = nSegCtr + 1

	STORE "N3"+cfd+cSF_Addr+csegd TO cString
	DO cstringbreak
	nSegCtr = nSegCtr + 1

	STORE "N4"+cfd+cSF_CSZ+csegd TO cString
	DO cstringbreak
	nSegCtr = nSegCtr + 1

	IF cRunType = "D"
		STORE "N1"+cfd+"CN"+cfd+cST_Name+cfd+"ZZ"+cfd+"USMIKFMII"+csegd TO cString
	ELSE
		STORE "N1"+cfd+"CN"+cfd+cST_Name+csegd TO cString
	ENDIF
	DO cstringbreak
	nSegCtr = nSegCtr + 1

	STORE "N3"+cfd+cST_Addr+csegd TO cString
	DO cstringbreak
	nSegCtr = nSegCtr + 1

	STORE "N4"+cfd+cST_CSZ+csegd TO cString
	DO cstringbreak
	nSegCtr = nSegCtr + 1

*!* Now in line loop
	STORE "LX"+cfd+ALLTRIM(STR(nLXNum))+csegd TO cString  && Size
	DO cstringbreak
	nSegCtr = nSegCtr + 1
	nLXNum = nLXNum + 1

	cShipStatus = IIF(cRunType = "D","X3","X1")
	IF EMPTY(wolog.delivered)
		cStatusDate = DTOC(DATE(),1)
	ELSE
		cStatusDate = DTOC(wolog.delivered,1)
	ENDIF

	IF !EMPTY(wolog.updatedt)
		cStatusTime = STRTRAN(LEFT(TTOC(wolog.updatedt,2),5),":","")
	ELSE
		cStatusTime = STRTRAN(LEFT(TTOC(DATETIME()-(3*3600),2),5),":","")
	ENDIF
	IF lTesting
		cStatusTime = "1530"
	ENDIF

	STORE "AT7"+cfd+cShipStatus+cfd+"NS"+REPLICATE(cfd,3)+cStatusDate+cfd+cStatusTime+cfd+"LT"+csegd TO cString
	DO cstringbreak
	nSegCtr = nSegCtr + 1

	STORE "MS1"+cfd+cPUDLocCity+cfd+cPUDLocState+csegd TO cString
	DO cstringbreak
	nSegCtr = nSegCtr + 1

	CCtrPrefix = LEFT(wolog.CONTAINER,4)
	cCtrNumber = ALLTRIM(SUBSTR(wolog.CONTAINER,5))
	STORE "MS2"+cfd+CCtrPrefix+cfd+cCtrNumber+csegd TO cString
	DO cstringbreak
	nSegCtr = nSegCtr + 1

	STORE "L11"+cfd+cBL+cfd+"IC"+csegd TO cString  && added 9/27/06
	DO cstringbreak
	nSegCtr = nSegCtr + 1

*!* Finish processing
	DO close214
	IF nFilenum>0
		=FCLOSE(nFilenum)
	ENDIF
	DO ediupdate WITH "214 CREATED",.F.,.F.

	COPY FILE &cFilename TO &cFilename2
	IF lFilesOut
		COPY FILE &cFilename TO &cFilename3
		DELETE FILE &cFilename
	ENDIF

	IF !lTesting
		IF !USED("ftpedilog")
			SELECT 0
			USE F:\edirouting\ftpedilog ALIAS ftpedilog
			INSERT INTO ftpedilog (TRANSFER,ftpdate,filename,acct_name,TYPE) VALUES ("214-"+cCustName,dt2,cFilename,UPPER(cCustName),"214")
			USE IN ftpedilog
		ENDIF
	ENDIF

	IF lTesting
		tsubject = "VF 214 *TEST* EDI from TGF as of "+dtmail
	ELSE
		tsubject = "VF 214 EDI from TGF as of "+dtmail
	ENDIF

	tmessage = "214 ("+cProcType+") EDI Info from TGF-CA, Truck WO# "+cWO_Num+CHR(10)
	tmessage = tmessage + "for coalition "+cMailName
	tmessage = tmessage +" has been created."+CHR(10)+"(Filename: "+cFilenameShort+")"
*!*	+CHR(10)+CHR(10)
*!*	tmessage = tmessage + "If you have any questions, please eMail or call 732-750-9000 x217."
	IF lTesting
		tmessage = tmessage + CHR(10)+CHR(10)+"This is a TEST RUN..."
	ENDIF

	IF lEmail
		DO FORM dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
	ENDIF

	RELEASE ALL LIKE c_CntrlNum,c_GrpCntrlNum
	RELEASE ALL LIKE nOrigSeq,nOrigGrpSeq
	WAIT CLEAR
	WAIT WINDOW cMailName+" 214 "+cProcType+" EDI File output complete" AT 20,60 TIMEOUT 2

	closefiles()

CATCH TO oErr
	IF lDoCatch
		ASSERT .F. MESSAGE "In error catch"
		lEmail = .T.
		DO ediupdate WITH "ERRHAND ERROR",.T.
		tsubject = cCustName+" Error ("+TRANSFORM(oErr.ERRORNO)+") at "+TTOC(DATETIME())
		tattach  = ""
		tsendto  = "joe.bianchi@tollgroup.com"
		tmessage = cCustName+" Error processing "+CHR(13)

		tmessage =tmessage+ "Try/CATCH Exeception Message:"+CHR(13)+;
			[  Error: ] + STR(oErr.ERRORNO) +CHR(13)+;
			[  LineNo: ] + STR(oErr.LINENO) +CHR(13)+;
			[  Message: ] + oErr.MESSAGE +CHR(13)+;
			[  Procedure: ] + oErr.PROCEDURE +CHR(13)+;
			[  Details: ] + oErr.DETAILS +CHR(13)+;
			[  StackLevel: ] + STR(oErr.STACKLEVEL) +CHR(13)+;
			[  LineContents: ] + oErr.LINECONTENTS+CHR(13)+;
			[  UserValue: ] + oErr.USERVALUE

		tsubject = "945 EDI Poller Error at "+TTOC(DATETIME())
		tattach  = ""
		tcc=""
		tfrom    ="TGF EDI 945 Poller Operations <transload-ops@fmiint.com>"
		DO FORM dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
		lEmail = .F.
	ENDIF
FINALLY
	ON ERROR
	FCLOSE(nFilenum)
	RELEASE ALL LIKE t*
	closefiles()
ENDTRY

&& END OF MAIN CODE SECTION



****************************
PROCEDURE num_incr_isa
****************************

	IF !USED("serfile")
		USE ("F:\3pl\data\serial\"+cCustName+"214_serial") IN 0 ALIAS serfile
	ENDIF
	SELECT serfile
	IF lTesting
		nOrigSeq = serfile.seqnum
		nOrigGrpSeq = serfile.grpseqnum
		lISAFlag = .F.
	ENDIF
	nISA_Num = serfile.seqnum
	c_CntrlNum = ALLTRIM(STR(serfile.seqnum))
	REPLACE serfile.seqnum WITH serfile.seqnum + 1 IN serfile
	SELECT wolog
ENDPROC

****************************
PROCEDURE num_incr_st
****************************

	IF !USED("serfile")
		USE ("F:\3pl\data\serial\"+cCustName+"214_serial") IN 0 ALIAS serfile
	ENDIF
	SELECT serfile
	IF lTesting
		nOrigSeq = serfile.seqnum
		nOrigGrpSeq = serfile.grpseqnum
		lSTFlag = .F.
	ENDIF
	c_GrpCntrlNum = ALLTRIM(STR(serfile.grpseqnum))
	REPLACE serfile.grpseqnum WITH serfile.grpseqnum + 1 IN serfile
	SELECT wolog
ENDPROC

****************************
PROCEDURE segmentget
****************************

	PARAMETER thisarray,lcKey,nLength

	FOR i = 1 TO nLength
		IF i > nLength
			EXIT
		ENDIF
		lnEnd= AT("*",thisarray[i])
		IF lnEnd > 0
			lcThisKey =TRIM(SUBSTR(thisarray[i],1,lnEnd-1))
			IF OCCURS(lcKey,lcThisKey)>0
				RETURN SUBSTR(thisarray[i],lnEnd+1)
				i = 1
			ENDIF
		ENDIF
	ENDFOR

	RETURN ""

****************************
PROCEDURE close214
****************************
	STORE  "SE"+cfd+ALLTRIM(STR(nSegCtr+1))+cfd+PADL(c_GrpCntrlNum,9,"0")+csegd TO cString
	DO cstringbreak

	STORE  "GE"+cfd+ALLTRIM(STR(nSTCount))+cfd+c_CntrlNum+csegd TO cString
	DO cstringbreak

	STORE  "IEA"+cfd+"1"+cfd+PADL(c_CntrlNum,9,"0")+csegd TO cString
	DO cstringbreak

	RETURN

****************************
PROCEDURE cstringbreak
****************************
	cLen = LEN(ALLTRIM(cString))
	FPUTS(nFilenum,cString)
	RETURN


****************************
PROCEDURE errormail
****************************
	PARAMETERS msgtype
*ASSERT .f.
	lDoCatch = .F.

*ASSERT .F. MESSAGE "At Errormail...Debug"
	tsubject = cSystemName+" 214 EDI File Error"
	tmessage = cDivision+" 214 EDI Info, WO# "+cWO_Num+CHR(10)
	tsendto = "joe.bianchi@tollgroup.com"
	tcc = ""
	DO CASE
		CASE msgtype = "MISSWO"
			tmessage = tmessage + "WO# "+cWO_Num+" does not exist in F:\WO\WODATA\MANIFEST."
			DO ediupdate WITH msgtype,.T.,.F.
			IF lTesting
				lEmail = .F.
			ELSE
				tsendto = "jrocio@fmiint.com"
				tcc = "joe.bianchi@tollgroup.com"
			ENDIF
	ENDCASE

	IF lTesting
		tmessage = tmessage + CHR(10)+CHR(10)+"This is TEST DATA only..."
	ENDIF
	IF lEmail
		DO FORM dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
	ENDIF
ENDPROC

**************************
PROCEDURE ediupdate
**************************
	PARAMETERS cFin_status, lIsError,lFClose
	IF !lTesting
		SELECT edi_trigger
		LOCATE
		IF lIsError
			REPLACE edi_trigger.processed WITH .T.,edi_trigger.created WITH .F.,;
				edi_trigger.fin_status WITH cFin_status,edi_trigger.errorflag WITH .F.;
				edi_trigger.when_proc WITH DATETIME() ;
				FOR edi_trigger.wo_num = nWO_Num AND edi_type = cRunType+"214"
			IF nFilenum>0
				=FCLOSE(nFilenum)
			ENDIF
			IF FILE(cFilename)
				DELETE FILE &cFilename
			ENDIF
			closefiles()
		ELSE
			REPLACE edi_trigger.processed WITH .T.,edi_trigger.created WITH .T.,proc214 WITH .T.;
				edi_trigger.when_proc WITH DATETIME(),;
				edi_trigger.fin_status WITH cRunType+"214 CREATED";
				edi_trigger.errorflag WITH .F.,file214 WITH cFilename ;
				FOR edi_trigger.wo_num = nWO_Num AND edi_type = cRunType+"214"
		ENDIF
	ENDIF
ENDPROC

*********************
PROCEDURE closefiles
*********************
	IF USED('ftpedilog')
		USE IN ftpedilog
	ENDIF
	IF USED('serfile')
		USE IN serfile
	ENDIF
	IF USED('wolog')
		USE IN wolog
	ENDIF
	IF USED('account')
		USE IN account
	ENDIF
	IF USED('xref')
		USE IN xref
	ENDIF
	IF USED('pudlocs')
		USE IN pudlocs
	ENDIF
ENDPROC
