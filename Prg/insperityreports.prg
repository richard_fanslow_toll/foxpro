*INSPERITYREPORTS.PRG
*
* Author:	Mark Bennett
*
* process various reports from Insperity and TimeStar based on passed parameter
*
* Build EXE as F:\UTIL\INSPERITY\INSPERITYREPORTS.EXE
*
* added WoodDale prior day's hours report for Sean Danielle 01/11/2018 MB
*
* 3/7/2018 MB: modify to support sql day/daydet tables.
*
* 4/24/2018 MB: changed mbennett@fmiint.com to my Toll email.

LPARAMETERS tcReportType

LOCAL loINSPERITYREPORTS, lcReportType

IF EMPTY(tcReportType) THEN
	lcReportType = "?"
ELSE
	lcReportType = UPPER(ALLTRIM(tcReportType))
ENDIF

runack("INSPERITYREPORTS")

utilsetup("INSPERITYREPORTS")

loINSPERITYREPORTS = CREATEOBJECT('INSPERITYREPORTS')

loINSPERITYREPORTS.MAIN( lcReportType )

schedupdate()

CLOSE DATABASES ALL
RETURN


#DEFINE LOGIT 1
#DEFINE WAITIT 2
#DEFINE NOWAITIT 4
#DEFINE SENDIT 8
#DEFINE CRLF CHR(13) + CHR(10)
#DEFINE RETURN_DATA_MANDATORY .F.
#DEFINE RETURN_DATA_NOT_MANDATORY .T.
#DEFINE CSVFORMAT 6
#DEFINE WRITE_ONLY_UNBUFFERED 11
#DEFINE COMMA ","
#DEFINE xlEdgeBottom 9
#DEFINE xlContinuous 1
#DEFINE xlMedium -4138
#DEFINE xlFONT_RED -16776961

DEFINE CLASS INSPERITYREPORTS AS CUSTOM

	cProcessName = 'INSPERITYREPORTS'

	lTestMode = .T.  && .F. FOR NORMAL OPERATION, .T. TO SUPPRESS ALL BUT DEVELOPER EMAILS AND CHANGE INPUT TABLE, ETC.

	dToday = DATE()

	lAutoYield = .T.

	cStartTime = TTOC(DATETIME())

	* processing properties
	cReportType = ''

	* connection properties
	nSQLHandle = 0

	* Excel properties
	oExcel = NULL

	* wait window properties
	nWaitWindowTimeout = 2
	lWaitWindowIsOn = .T.

	* logfile properties
	lLoggingIsOn = .T.
	cLogFile = 'F:\UTIL\INSPERITY\LOGFILES\INSPERITYREPORTS_log.txt'

	* INTERNAL email properties
	lSendInternalEmailIsOn = .T.
	cFrom = 'Mark.Bennett@Tollgroup.com'
	cSendTo = 'mark.bennett@tollgroup.com'
	cCC = ''
	cSubject = ''
	cAttach = ''
	cBodyText = ''

	cCOMPUTERNAME = ''
	cUSERNAME = ''


	FUNCTION INIT
		IF NOT DODEFAULT()
			RETURN .F.
		ENDIF
		WITH THIS
			*SET RESOURCE OFF
			CLOSE DATA
			SET DECIMALS TO 4
			SET CENTURY ON
			SET DATE AMERICAN
			SET HOURS TO 24
			SET ANSI ON
			SET TALK OFF
			SET DELETED ON
			SET CONSOLE OFF
			SET EXCLUSIVE OFF
			SET SAFETY OFF
			SET EXACT OFF
			SET STATUS BAR ON
			SET SYSMENU OFF
			SET ENGINEBEHAVIOR 70
			_VFP.AUTOYIELD = .lAutoYield
			.cCOMPUTERNAME = GETENV("COMPUTERNAME")
			.cUSERNAME = GETENV("USERNAME")
			IF .lTestMode THEN
				.cLogFile = 'F:\UTIL\INSPERITY\LOGFILES\INSPERITYREPORTS_log_TESTMODE.txt'
				.cSendTo = 'mark.bennett@tollgroup.com'
				.cCC = ''
			ENDIF
			.lLoggingIsOn = .lLoggingIsOn AND NOT EMPTY(.cLogFile)
			IF .lLoggingIsOn THEN
				SET ALTERNATE TO (.cLogFile) ADDITIVE
				SET ALTERNATE ON
			ENDIF
			SET PROCEDURE TO VALIDATIONS ADDITIVE
		ENDWITH
	ENDFUNC


	FUNCTION DESTROY
		WITH THIS
			IF .lLoggingIsOn  THEN
				SET ALTERNATE OFF
				SET ALTERNATE TO
			ENDIF
		ENDWITH
		IF TYPE('.oExcel') = 'O' AND NOT ISNULL(.oExcel) THEN
			.oExcel.QUIT()
		ENDIF
		DODEFAULT()
	ENDFUNC


	FUNCTION MAIN
		LPARAMETERS tcReportType
		WITH THIS
			LOCAL lnNumberOfErrors, lcSQL, lcSQL2, loError, lcDateSuffix
			LOCAL lcFiletoSaveAs, lcSpreadsheetTemplate
			LOCAL i, oExcel, oWorkbook, oWorksheet, lnStartRow, lcStartRow, lnRow, lcRow
			LOCAL ldBeginDate, ldToday, ldEndDate, ldDate

			TRY
				lnNumberOfErrors = 0

				.TrackProgress("INSPERITY REPORTS process started....", LOGIT+SENDIT+NOWAITIT)
				.TrackProgress('COMPUTERNAME = ' + .cCOMPUTERNAME, LOGIT+SENDIT)
				.TrackProgress('USERNAME = ' + .cUSERNAME, LOGIT+SENDIT)
				.TrackProgress('PROJECT = INSPERITYREPORTS', LOGIT+SENDIT)
				.TrackProgress('REPORT TYPE = ' + tcReportType, LOGIT+SENDIT) 

				IF .lTestMode THEN
					.TrackProgress('                        TEST MODE                        ', LOGIT+SENDIT)
				ENDIF

				lcDateSuffix = DTOC(DATE())
				.cSubject = 'Insperity Report: ' + tcReportType + ' for ' + lcDateSuffix

				IF NOT INLIST(tcReportType,"DRIVER-TIME-BY-MONTH","VAC-EETAKINGS","PER-EETAKINGS","SCK-EETAKINGS","CADRIVERDAILYHOURSFORHECTOR") AND ;
					NOT INLIST(tcReportType,"DIV-50-OT","CADRIVER-PAYSUMMARY","DRIVER-PUNCHES","ELIG-401K","LIABILITY","NJ-OTR-WTD","PER-LIABILITY","SCK-LIABILITY","VAC-LIABILITY") AND ;
					NOT INLIST(tcReportType,"CADRIVER-SICKDAYS","CADRIVER-WEEKENDDAYSWORKED","CADRIVER-BADGES","CADRIVER-DAYSWORKED","GPAYCALLOW","SP2-BIRTHDAY-LIST","SP2-PHONE-LIST") AND ;
					NOT INLIST(tcReportType,"VAC-CARRYOVER-TAKEN","CADRIVER-CHECKHOURSBYWEEK","CADRIVER-HOURSBYDAY","PR-DATA-FORECASTER-KEN","EE-SALARY-INFO-FOR-KEN") AND ;
					NOT INLIST(tcReportType,"SP-TERMINATED-FOR-CARLINA","CADRIVERHOURSSUMMARYBYWEEK","CADRIVERHOURSALLFORLUCILLE","CADRIVER_OVERTIMESUMMARY","NJDRIVER_OVERTIMESUMMARY") AND ;
					NOT INLIST(tcReportType,"DRIVERTIMETOQLIKVIEW","DRIVERMOVESTOQLIKVIEW","DRIVERINFOTOQLIKVIEW","DRIVERPRODTOQLIKVIEW","OVERTIMEDUETOPTO","FFOVERTIMENOTDUETOPTO") AND ;
					NOT INLIST(tcReportType,"FLH-EETAKINGS","NY-FOR-LAUREN","NJ-FOR-JIM-KILLEN","WOODDALE-HRS") THEN
					.TrackProgress('ERROR: unexpected Report Type!', LOGIT+SENDIT)
					THROW
				ENDIF
				
				

				.cReportType = tcReportType

				* OPEN Excel
				.oExcel = CREATEOBJECT("excel.application")
				.oExcel.displayalerts = .F.
				.oExcel.VISIBLE = .F.

				* init various data properties likely to be used... 
				
				DO CASE
					CASE .cReportType == "WOODDALE-HRS"
						.WoodDaleHoursReport()
					CASE .cReportType == "NJ-FOR-JIM-KILLEN"
						.NJForJimKillen()
					CASE .cReportType == "NY-FOR-LAUREN"
						.NYForLauren()
					CASE .cReportType == "FFOVERTIMENOTDUETOPTO"
						.FFOvertimeNOTDueToPTO()
					CASE .cReportType == "OVERTIMEDUETOPTO"
						.OvertimeDueToPTO()
					CASE .cReportType == "DRIVERPRODTOQLIKVIEW"
					
						* DO ALL 3 DRIVER PROD EXPORTS...
					
						ldBeginDate = {^2016-01-01}
						* export will run weekly on wednesdays; end date should be the prior saturday, per Brian DeMarzo 11/16/2016.
						* We go back from today until we hit a Saturday (DOW = 7) , which is the end day of the previous Sunday-Saturday pay period.
						ldDate = .dToday
						DO WHILE DOW(ldDate,1) <> 7 
							ldDate = ldDate - 1
						ENDDO
						ldEndDate = ldDate
						
*!*	* to force different ranges
*!*	ldBeginDate = {^2015-01-01}
*!*	ldEndDate =   {^2016-11-30}
						
						.DriverInfoToQlikView()
						
						.DriverMovesToQlikView(ldBeginDate, ldEndDate)
						
						.DriverTimeToQlikView(ldBeginDate, ldEndDate)
						
					CASE .cReportType == "DRIVERINFOTOQLIKVIEW"
					
						* this export is always current info because the data in hr employee table is not time sensitive.
						
						.DriverInfoToQlikView()
						
					CASE .cReportType == "DRIVERMOVESTOQLIKVIEW"
					
						ldBeginDate = {^2016-01-01}
						* export will run weekly on wednesdays; end date should be the prior saturday, per Brian DeMarzo 11/16/2016.
						* We go back from today until we hit a Saturday (DOW = 7) , which is the end day of the previous Sunday-Saturday pay period.
						ldDate = .dToday
						DO WHILE DOW(ldDate,1) <> 7 
							ldDate = ldDate - 1
						ENDDO
						ldEndDate = ldDate
						
*!*	* to force different ranges
*!*	ldBeginDate = {^2016-01-01}
*!*	ldEndDate =   {^2016-10-31}

						.DriverMovesToQlikView(ldBeginDate, ldEndDate)

					CASE .cReportType == "DRIVERTIMETOQLIKVIEW"
					
						ldBeginDate = {^2016-01-01}
						* export will run weekly on wednesdays; end date should be the prior saturday, per Brian DeMarzo 11/16/2016.
						* We go back from today until we hit a Saturday (DOW = 7) , which is the end day of the previous Sunday-Saturday pay period.
						ldDate = .dToday
						DO WHILE DOW(ldDate,1) <> 7 
							ldDate = ldDate - 1
						ENDDO
						ldEndDate = ldDate
						
*!*	* to force different ranges
*!*	ldBeginDate = {^2016-01-01}
*!*	ldEndDate =   {^2016-10-31}
						
						.DriverTimeToQlikView(ldBeginDate, ldEndDate)

					CASE .cReportType == "NJDRIVER_OVERTIMESUMMARY"
						.NJDRIVER_OvertimeSummary()
					CASE .cReportType == "CADRIVER_OVERTIMESUMMARY"
						.CADRIVER_OvertimeSummary()
					CASE .cReportType == "CADRIVERHOURSALLFORLUCILLE" 
						.CADriverHoursAllForLucille()
					CASE .cReportType == "CADRIVERDAILYHOURSFORHECTOR" 
						.CADriverDailyHoursForHector()
					CASE .cReportType == "CADRIVERHOURSSUMMARYBYWEEK" 
						.CADriverHoursSummaryByWeek()
					CASE .cReportType == "SP-TERMINATED-FOR-CARLINA" 
						.SPTERMINATEDFORCARLINA()
					CASE .cReportType == "EE-SALARY-INFO-FOR-KEN" 
						.EESALARYINFOFORKEN()
					CASE .cReportType == "DRIVER-TIME-BY-MONTH"
						.GetDriverHoursByMonth( {^2014-01-01} )
						.GetDriverHoursByMonth( {^2014-02-01} )
						.GetDriverHoursByMonth( {^2014-03-01} )
						.GetDriverHoursByMonth( {^2014-04-01} )
						.GetDriverHoursByMonth( {^2014-05-01} )
						.GetDriverHoursByMonth( {^2014-06-01} )
						.GetDriverHoursByMonth( {^2014-07-01} )
						.GetDriverHoursByMonth( {^2014-08-01} )
						.GetDriverHoursByMonth( {^2014-09-01} )
						.GetDriverHoursByMonth( {^2014-10-01} )
						.GetDriverHoursByMonth( {^2014-11-01} )
						.GetDriverHoursByMonth( {^2014-12-01} )
						.GetDriverHoursByMonth( {^2015-01-01} )
						.GetDriverHoursByMonth( {^2015-02-01} )
						.GetDriverHoursByMonth( {^2015-03-01} )
						.GetDriverHoursByMonth( {^2015-04-01} )
					CASE .cReportType == "FLH-EETAKINGS"
						.EETakingsAndBalances("FLH")
					CASE .cReportType == "VAC-EETAKINGS"
						.EETakingsAndBalances("VAC")
					CASE .cReportType == "PER-EETAKINGS"
						.EETakingsAndBalances("PER")
					CASE .cReportType == "SCK-EETAKINGS"
						.EETakingsAndBalances("SCK")
					CASE .cReportType == "PR-DATA-FORECASTER-KEN"
						.PRDATAFORECASTERKEN()
					CASE .cReportType == "CADRIVER-HOURSBYDAY"
						.CADriverHoursByDayReport()
					CASE .cReportType == "CADRIVER-CHECKHOURSBYWEEK"
						.CheckCADriverHoursByWeek()
					CASE .cReportType == "CADRIVER-WEEKENDDAYSWORKED"
						.DriverWeekendDaysWorkedReport()
					CASE .cReportType == "CADRIVER-SICKDAYS"
						.DriverSickDaysReport()
					CASE .cReportType == "CADRIVER-DAYSWORKED"
						.DriverDaysWorkedReport()
					CASE .cReportType == "CADRIVER-PAYSUMMARY"
						.DriverPaySummaryReport()
					CASE .cReportType = "CADRIVER-BADGES"
						.CADriverBadgesReport()
					CASE .cReportType == "DIV-50-OT"
						.Div50_OvertimeReport()
					CASE .cReportType == "DRIVER-PUNCHES"
						.DriverPunchesReport()
					CASE .cReportType == "ELIG-401K"
						.EligibleFor401kReport()
					CASE .cReportType == "GPAYCALLOW"
						.GetGrossPayAndCarAllowance()
					CASE .cReportType == "LIST-OF_DRIVERS"
						.ListOfDrivers()
					CASE .cReportType == "NJ-OTR-WTD"
						.NJOTRWTDReport()
					CASE .cReportType == "PER-LIABILITY"
						.GetAccrualsLiability("PER")
					CASE .cReportType == "SCK-LIABILITY"
						.GetAccrualsLiability("SCK")
					CASE .cReportType == "SP2-BIRTHDAY-LIST"
						.SP2BirthdayList()
					CASE .cReportType == "SP2-PHONE-LIST"
						.SP2PhoneList()
					CASE .cReportType == "VAC-CARRYOVER-TAKEN"
						.GetVacCarryoverAndTaken()
					CASE .cReportType == "VAC-LIABILITY"
						.GetAccrualsLiability("VAC")
				ENDCASE

				.TrackProgress("INSPERITY REPORTS process ended normally.", LOGIT+SENDIT+NOWAITIT)

			CATCH TO loError

				.TrackProgress('There was an error.',LOGIT+SENDIT)
				.TrackProgress('The Error # is: ' + TRANSFORM(loError.ERRORNO), LOGIT+SENDIT)
				.TrackProgress('The Error Message is: ' + TRANSFORM(loError.MESSAGE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred in Program: ' + TRANSFORM(loError.PROCEDURE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred at line #: ' + TRANSFORM(loError.LINENO), LOGIT+SENDIT)
				lnNumberOfErrors = lnNumberOfErrors + 1

				IF TYPE('.oExcel') = 'O' AND NOT ISNULL(.oExcel) THEN
					.oExcel.QUIT()
				ENDIF
				CLOSE DATABASES ALL

			ENDTRY

			WAIT CLEAR
			***************** INTERNAL email results ******************************


			.TrackProgress('About to send status email.',LOGIT)
			.TrackProgress('The logfile is: ' + .cLogFile, SENDIT)
			.TrackProgress("INSPERITY REPORTS process started: " + .cStartTime, LOGIT+SENDIT)
			.TrackProgress("INSPERITY REPORTS process finished: " + TTOC(DATETIME()), LOGIT+SENDIT)

			IF .lSendInternalEmailIsOn THEN
				DO FORM dartmail2 WITH .cSendTo,.cFrom,.cSubject,.cCC,.cAttach,.cBodyText,"A"
				.TrackProgress('Sent status email.',LOGIT)
			ELSE
				.TrackProgress('Did not send status email: THIS.lSendInternalEmailIsOn = FALSE.',LOGIT)
			ENDIF

		ENDWITH
		RETURN
	ENDFUNC && main	


	* this procedure added 01/11/2018 MB
	PROCEDURE WoodDaleHoursReport
		WITH THIS
			* Prior Day Hours report for hourly WoodDale staff for Sean Danielle. 
			* Include all pay_types in the report per SD.

			* show Hours by Department
			* and
			* Hours by Employee within Department
			**************************************************
			** schedule it to run daily in the late morning.
			**************************************************
			
			
			LOCAL ldToday, ldYesterday, lcFiletoSaveAs
			LOCAL lnRow, lcRow, oWorkbook, oWorksheet1
			LOCAL lcSendToR, lcCCR, lcSubjectR, lcAttachR, lcBodyTextR

			ldToday = .dToday
			ldYesterday = ldToday - 1
			*ldYesterday = {^2018-01-13}  && to force particular date

			.cSubject = "WoodDale Hours for " + TRANSFORM(ldYesterday)
			
			lcSubjectR = .cSubject

			IF USED('DEPTTRANS') THEN
				USE IN DEPTTRANS
			ENDIF
			USE F:\UTIL\ADPREPORTS\DATA\DEPTTRANS.DBF IN 0 ALIAS DEPTTRANS

			IF USED('TIMEDATA') THEN
				USE IN TIMEDATA
			ENDIF
			USE F:\UTIL\TIMESTAR\DATA\TIMEDATA.DBF IN 0 ALIAS TIMEDATA

			IF USED('CUREMPLOYEE') THEN
				USE IN CUREMPLOYEE
			ENDIF

			IF USED('CURDEPT') THEN
				USE IN CURDEPT
			ENDIF

			* GET HOURS BY DEPT
			SELECT ;
				DEPT, ;
				SPACE(30) AS DEPTDESC, ;
				PAY_TYPE, ;
				SUM(TOTHOURS) AS HOURS ;
				FROM TIMEDATA ;
				INTO CURSOR CURDEPT ;
				WHERE WORKDATE = ldYesterday ;
				AND ADP_COMP = 'E87' ;
				AND WORKSITE = 'IL1' ;
				GROUP BY DEPT, DEPTDESC, PAY_TYPE ;
				ORDER BY DEPT, PAY_TYPE ;
				READWRITE

			* POPULATE DEPT DESCRIPTION
			SELECT CURDEPT
			SCAN
				SELECT DEPTTRANS
				LOCATE FOR ALLTRIM(DEPT) == ALLTRIM(CURDEPT.DEPT)
				IF FOUND() THEN
					REPLACE CURDEPT.DEPTDESC WITH DEPTTRANS.DESC
				ENDIF
			ENDSCAN

								
*!*	SELECT CURDEPT
*!*	BROWSE
*!*	THROW

			* get Hours by Employee with Dept
			SELECT ;
				NAME, ;
				DEPT, ;
				SPACE(30) AS DEPTDESC, ;
				PAY_TYPE, ;
				SUM(TOTHOURS) AS HOURS ;
				FROM TIMEDATA ;
				INTO CURSOR CUREMPLOYEE ;
				WHERE WORKDATE = ldYesterday ;
				AND ADP_COMP = 'E87' ;
				AND WORKSITE = 'IL1' ;
				GROUP BY NAME, DEPT, DEPTDESC, PAY_TYPE ;
				ORDER BY NAME, DEPT, PAY_TYPE ;
				READWRITE

			* POPULATE DEPT DESCRIPTION
			SELECT CUREMPLOYEE
			SCAN
				SELECT DEPTTRANS
				LOCATE FOR ALLTRIM(DEPT) == ALLTRIM(CUREMPLOYEE.DEPT)
				IF FOUND() THEN
					REPLACE CUREMPLOYEE.DEPTDESC WITH DEPTTRANS.DESC
				ENDIF
			ENDSCAN
			
*!*	SELECT CUREMPLOYEE
*!*	BROWSE
*!*	THROW


			** setup file names

			lcSpreadsheetTemplate = "F:\UTIL\INSPERITY\TEMPLATES\WOODDALE_HRS_TEMPLATE.XLS"

			lcFiletoSaveAs = "F:\UTIL\INSPERITY\REPORTS\Wood Dale Hours Report for " + STRTRAN(DTOC(ldYesterday),"/","-") + ".XLS"

			IF FILE(lcFiletoSaveAs) THEN
				DELETE FILE (lcFiletoSaveAs)
			ENDIF

			** output to Excel spreadsheet
			.TrackProgress('Creating spreadsheet: ' + lcFiletoSaveAs, LOGIT+SENDIT+NOWAITIT)

			oWorkbook = .oExcel.workbooks.OPEN(lcSpreadsheetTemplate)
			oWorkbook.SAVEAS(lcFiletoSaveAs)

			** 401k LIST IS WORKSHEET 1
			oWorksheet1 = oWorkbook.Worksheets[1]
			oWorksheet1.RANGE("A4","H200").ClearContents()
			oWorksheet1.RANGE("B1").VALUE = ldYesterday

			lnRow = 3

			SELECT CURDEPT
			SCAN
				lnRow = lnRow + 1
				lcRow = ALLTRIM(STR(lnRow))
				oWorksheet1.RANGE("A"+lcRow).VALUE = CURDEPT.DEPTDESC
				oWorksheet1.RANGE("B"+lcRow).VALUE = CURDEPT.PAY_TYPE
				oWorksheet1.RANGE("C"+lcRow).VALUE = CURDEPT.HOURS
			ENDSCAN

			lnRow = 3

			SELECT CUREMPLOYEE
			SCAN
				lnRow = lnRow + 1
				lcRow = ALLTRIM(STR(lnRow))
				oWorksheet1.RANGE("E"+lcRow).VALUE = CUREMPLOYEE.NAME
				oWorksheet1.RANGE("F"+lcRow).VALUE = CUREMPLOYEE.DEPTDESC				
				oWorksheet1.RANGE("G"+lcRow).VALUE = CUREMPLOYEE.PAY_TYPE
				oWorksheet1.RANGE("H"+lcRow).VALUE = CUREMPLOYEE.HOURS
			ENDSCAN

			oWorkbook.SAVE()
			.oExcel.QUIT()

			IF FILE(lcFileToSaveAs) THEN
				* attach output file to email
				lcAttachR = lcFileToSaveAs
				lcBodyTextR = "See attached hours report." + ;
					CRLF + CRLF + "(do not reply - this is an automated report)" + ;
					CRLF + CRLF + "<report log follows>" 
					
				lcSendToR = 'sean.daniele@tollgroup.com'
				lcCCR = 'mark.bennett@tollgroup.com'
			
				* send email...
				DO FORM dartmail2 WITH lcSendToR,.cFrom,lcSubjectR,lcCCR,lcAttachR,lcBodyTextR,"A"
				.TrackProgress('Sent CA Driver Daily Report email to ' + lcSendToR + '.',LOGIT+SENDIT)
					
			ELSE
				.TrackProgress('ERROR: unable to email spreadsheet: ' + lcFileToSaveAs, LOGIT+SENDIT+NOWAITIT)
			ENDIF


			*!*			IF .lTestMode THEN
			*!*				.cSendTo = 'mark.bennett@tollgroup.com'
			*!*				.cCC = ''
			*!*			ELSE
			*!*				.cSendTo = 'lauren.wojcik@Tollgroup.com'
			*!*				.cCC = 'mark.bennett@tollgroup.com'
			*!*			ENDIF

		ENDWITH
		RETURN
	ENDPROC  && WoodDaleHoursReport


	PROCEDURE NYForLauren
		* provide total hours and headcount just for NY office employees for a year
		* we'll calc these stats by calendar month...
		WITH THIS
		
			PRIVATE m.State, m.Date, m.FromDate, m.ToDate, m.Headcnt, m.Tothours
		
			CREATE CURSOR CURNYSTATS( STATE C(2), FROMDATE D, TODATE D, HEADCNT I, TOTHOURS N(8.3) )
			
			USE F:\UTIL\TIMESTAR\DATA\TIMEDATA IN 0 ALIAS TIMEDATA
			
			m.State = 'NY'
			
			m.Date = {^2016-01-01}
			
			DO WHILE m.Date <= {^2016-12-01}
				m.FromDate = m.Date
				m.ToDate = GOMONTH(m.FromDate,1) - 1
				
				SELECT ;
					COUNT(DISTINCT FILE_NUM) AS HEADCNT ;
					FROM TIMEDATA ;
					INTO CURSOR CURHEADCNT ;
					WHERE WORKDATE >= m.FromDate ;
					AND WORKDATE <= m.ToDate ;
					AND UPPER(ALLTRIM(WORKSITE)) == 'NY1'
					
				SELECT CURHEADCNT
				LOCATE
				m.Headcnt = CURHEADCNT.HEADCNT 
				

				SELECT SUM(CODEHOURS + REGHOURS + OTHOURS + DTHOURS) AS TOTHOURS ;
					FROM TIMEDATA ;
					INTO CURSOR CURTOTHOURS ;
					WHERE WORKDATE >= m.FromDate ;
					AND WORKDATE <= m.ToDate ;
					AND UPPER(ALLTRIM(WORKSITE)) == 'NY1'
					
				SELECT CURTOTHOURS
				LOCATE
				m.Tothours = CURTOTHOURS.TOTHOURS 

			
			
				INSERT INTO CURNYSTATS FROM MEMVAR
				
				m.Date = GOMONTH(m.Date,1)	
			
			ENDDO 
			
			SELECT CURNYSTATS
			COPY TO C:\A\CURNYSTATS.XLS XL5


		ENDWITH
		RETURN
	ENDFUNC && NYForLauren	


	PROCEDURE FFOvertimeNOTDueToPTO
	
		* report on Overtime pay NOT caused by PTO for Mariel Calello - FF employees only.
		* Only VAC, HOL and Personal PTO would generate OT.
		* Pay period is Sunday thru Saturday.
		
		* SO WE WILL COUNT OT & DT IN ANY WEEK THAT DID NOT HAVE PTO PLUS IN ANY WEEK WITH PTO ALL DT, AND ANY OT IN THE LAST DAY WORKED, WILL BE COUNTED	
	
		LOCAL ldStart, ldEnd, lcDetail, lcSummary, ldDate, ldPaydate, ldLastWorkDay, lcSummary		
	
		CLOSE DATABASES ALL
		
		USE F:\UTIL\TIMESTAR\DATA\TIMEDATA IN 0 ALIAS TIMEDATA

		SELECT WORKSITE, WORKDATE AS PPBEGIN, WORKDATE AS PPEND, WORKDATE AS PAYDATE, BUSUNIT, DIVISION, DEPT, NAME, ;
			0000.00 AS REGHOURS, ;
			0000.00 AS HOLHOURS, ;
			0000.00 AS VACHOURS, ;
			0000.00 AS PERHOURS, ;
			0000.00 AS OTHOURS, ;
			0000.00 AS OTWAGES, ;
			0000.00 AS DTWAGES, ;
			000 AS DAYSWITHOT,  ;
			000 AS DAYSWITHDT,  ;
			{} AS MAXDAY, ;
			000.00 AS LASTDAYOT, ;
			0000.00 AS LDOTWAGES, ;
			0000.00 AS DTHOURS, ;
			000.00 AS TOTOTHOURS, ;
			0000.00 AS TOTOTWAGES ;
			FROM TIMEDATA ;
			INTO CURSOR CURALL ;
			WHERE .F. ;
			READWRITE

		ldLastDate = {^2016-12-25} && last Sunday in report period
		*ldLastDate = {^2016-01-03} && last Sunday in report period
		ldDate = {^2015-12-27}  && start of 1st week paid in 2016
		DO WHILE ldDate <= ldLastDate 
			ldStart = ldDate  && start of pay period
			ldEnd = ldStart + 6  && end of pay period
			ldPaydate = ldEnd + 6  && paydate
			
			WAIT WINDOW NOWAIT 'Processing Pay Date ' + TRANSFORM(ldPaydate)
			
			SELECT WORKSITE, ldStart AS PPBEGIN, ldEnd AS PPEND, ldPaydate AS PAYDATE, BUSUNIT, DIVISION, DEPT, NAME, ;
				SUM(IIF(ALLTRIM(PAY_TYPE) == 'REGULAR',TOTHOURS,000.00)) AS REGHOURS, ;
				SUM(IIF(ALLTRIM(PAY_TYPE) == 'HOLIDAY',TOTHOURS,000.00)) AS HOLHOURS, ;
				SUM(IIF(ALLTRIM(PAY_TYPE) == 'VACATION',TOTHOURS,000.00)) AS VACHOURS, ;
				SUM(IIF(ALLTRIM(PAY_TYPE) == 'PERSONAL',TOTHOURS,000.00)) AS PERHOURS, ;
				SUM(IIF(ALLTRIM(PAY_TYPE) == 'OVERTIME',TOTHOURS,000.00)) AS OTHOURS, ;
				SUM(IIF(ALLTRIM(PAY_TYPE) == 'OVERTIME',WAGEAMT,0000.00)) AS OTWAGES, ;
				SUM(IIF(ALLTRIM(PAY_TYPE) == 'DOUBLETIME',WAGEAMT,0000.00)) AS DTWAGES, ;
				SUM(IIF(ALLTRIM(PAY_TYPE) == 'OVERTIME',1,0)) AS DAYSWITHOT, ;
				SUM(IIF(ALLTRIM(PAY_TYPE) == 'DOUBLETIME',1,0)) AS DAYSWITHDT, ;
				{} AS MAXDAY, ;
				000.00 AS LASTDAYOT, ;
				0000.00 AS LDOTWAGES, ;
				SUM(IIF(ALLTRIM(PAY_TYPE) == 'DOUBLETIME',TOTHOURS,000.00)) AS DTHOURS, ;
				000.00 AS TOTOTHOURS, ;
				0000.00 AS TOTOTWAGES ;
				FROM TIMEDATA ;
				INTO CURSOR CURWEEK ;
				WHERE ADP_COMP == 'E87' ;
				AND ALLTRIM(BUSUNIT) <> 'SCS' ;
				AND (NOT DIVISION IN ('01','03','92')) ;
				AND BETWEEN(WORKDATE,ldStart,ldEnd) ;
				GROUP BY 1,2,3,4,5,6,7,8 ;
				ORDER BY 1,2,8 ;
				READWRITE
			
			* FOR WEEKS THAT HAD BOTH PTO AND OVERTIME, CALC OT ON LAST DAY WORKED - WE'RE ASSUMING THAT ANY OT ON LAST DAY WORKED WAS AT LEAST PARTIALLY CAUSED BY PTO
			SELECT CURWEEK
			SCAN FOR ((VACHOURS + PERHOURS + HOLHOURS) > 0) AND ((OTHOURS + DTHOURS) > 0)
			
				* get last day *worked* in the week for the person - ind EXCLUDES incentive pay, etc.
				SELECT MAX(WORKDATE) AS WORKDATE ;
					FROM TIMEDATA ;
					INTO CURSOR CURMAX ;
					WHERE NAME == CURWEEK.NAME ;
					AND BETWEEN(WORKDATE,CURWEEK.PPBEGIN,CURWEEK.PPEND)	;
					AND ALLTRIM(INPUNCHNM) == 'IND'
			
				ldLastWorkDay = NVL(CURMAX.WORKDATE,{})				
				
				* ====> get ot for that day - WE WILL SUBTRACT THEM FROM THE WEEK'S TOTAL OT
				SELECT NVL(SUM(TOTHOURS),000.00) AS LASTDAYOT, NVL(SUM(WAGEAMT),0000.00) AS LDOTWAGES ;
					FROM TIMEDATA ;
					INTO CURSOR CURLASTDAYOT ;
					WHERE NAME == CURWEEK.NAME ;
					AND ALLTRIM(PAY_TYPE) == 'OVERTIME' ;
					AND WORKDATE = ldLastWorkDay 
					
					REPLACE CURWEEK.LASTDAYOT WITH (-1 * CURLASTDAYOT.LASTDAYOT), CURWEEK.LDOTWAGES WITH (-1 * CURLASTDAYOT.LDOTWAGES), CURWEEK.MAXDAY WITH ldLastWorkDay IN CURWEEK
					
			ENDSCAN


			* FOR WEEKS THAT HAD OT BUT NO PTO - WE WILL COUNT ALL OT HOURS, SO NO NEED TO CALC THE LAST DAY OT ADJUSTMENTS WE DID IN SCAN LOOP ABOVE

			
			SELECT CURWEEK
			SCAN FOR ((OTHOURS + DTHOURS) > 0)
				* APPLY ADJUSTMENTS SUBTRACTING LAST DAY OT
				REPLACE CURWEEK.TOTOTHOURS WITH (CURWEEK.OTHOURS + CURWEEK.LASTDAYOT), CURWEEK.TOTOTWAGES WITH (CURWEEK.OTWAGES + CURWEEK.LDOTWAGES + CURWEEK.DTWAGES) IN CURWEEK						
				SCATTER MEMVAR
				INSERT INTO CURALL FROM MEMVAR			
			ENDSCAN
			
			ldDate = ldDate + 7  && advance dates a week
		ENDDO
		
		WAIT CLEAR
		
*!*			SELECT CURALL
*!*			BROWSE
			
		* REORDER THE DATA
		SELECT * FROM CURALL ;
			INTO CURSOR CURALLFINAL ;
			ORDER BY WORKSITE, PPBEGIN, NAME		
		
		lcDetail = "C:\A\FM_OT_NOT_FROM_PTO_2016_DETAIL.XLS"
		SELECT CURALLFINAL
		COPY TO (lcDetail) XL5 && FOR TOTOTWAGES > 0
		
		* GROUP THE DATA BY WORKSITE
		SELECT WORKSITE, SUM(TOTOTHOURS) AS TOTOTHOURS, SUM(TOTOTWAGES) AS TOTOTWAGES ;
			FROM CURALLFINAL ;
			INTO CURSOR CURSUMMARY ;
			GROUP BY WORKSITE ;
			ORDER BY WORKSITE
		
		lcSummary = "C:\A\FM_OT_NOT_FROM_PTO_2016_SUMMARY.XLS"
		SELECT CURSUMMARY
		COPY TO (lcSummary) XL5 && FOR TOTOTWAGES > 0
		
		
		RETURN
	ENDPROC  && FFOvertimeNOTDueToPTO

	
	PROCEDURE OvertimeDueToPTO
	
		* report on Overtime pay caused by PTO for Rob Harrelson via Mariel Calello.
		* Only VAC and Personal PTO would generate OT.
		* Pay period is Sunday thru Saturday.
		
	
		LOCAL ldStart, ldEnd, lcDetail, lcSummary, ldDate, ldPaydate, ldLastWorkDay
		
	
		CLOSE DATABASES ALL
		
		USE F:\UTIL\TIMESTAR\DATA\TIMEDATA IN 0 ALIAS TIMEDATA

		SELECT WORKDATE AS PPBEGIN, WORKDATE AS PPEND, WORKDATE AS PAYDATE, DIVISION, DEPT, NAME, ;
			0000.00 AS REGHOURS, ;
			0000.00 AS HOLHOURS, ;
			0000.00 AS VACHOURS, ;
			0000.00 AS PERHOURS, ;
			0000.00 AS OTHOURS, ;
			0000.00 AS OTWAGES, ;
			000 AS DAYSWITHOT,  ;
			000.00 AS LASTDAYOT, ;
			0000.00 AS LDOTWAGES, ;
			{} AS MAXDAY ;
			FROM TIMEDATA ;
			INTO CURSOR CURALL ;
			WHERE .F. ;
			READWRITE

		ldLastDate = {^2016-12-25} && last Sunday in report period
		*ldLastDate = {^2016-01-03} && last Sunday in report period
		ldDate = {^2015-12-27}  && start of 1st week paid in 2016
		DO WHILE ldDate <= ldLastDate 
			ldStart = ldDate  && start of pay period
			ldEnd = ldStart + 6  && end of pay period
			ldPaydate = ldEnd + 6  && paydate
			
			WAIT WINDOW NOWAIT 'Processing Pay Date ' + TRANSFORM(ldPaydate)
			
			SELECT ldStart AS PPBEGIN, ldEnd AS PPEND, ldPaydate AS PAYDATE, DIVISION, DEPT, NAME, ;
				SUM(IIF(ALLTRIM(PAY_TYPE) == 'REGULAR',TOTHOURS,000.00)) AS REGHOURS, ;
				SUM(IIF(ALLTRIM(PAY_TYPE) == 'HOLIDAY',TOTHOURS,000.00)) AS HOLHOURS, ;
				SUM(IIF(ALLTRIM(PAY_TYPE) == 'VACATION',TOTHOURS,000.00)) AS VACHOURS, ;
				SUM(IIF(ALLTRIM(PAY_TYPE) == 'PERSONAL',TOTHOURS,000.00)) AS PERHOURS, ;
				SUM(IIF(ALLTRIM(PAY_TYPE) == 'OVERTIME',TOTHOURS,000.00)) AS OTHOURS, ;
				SUM(IIF(ALLTRIM(PAY_TYPE) == 'OVERTIME',WAGEAMT,0000.00)) AS OTWAGES, ;
				SUM(IIF(ALLTRIM(PAY_TYPE) == 'OVERTIME',1,0)) AS DAYSWITHOT, ;
				000.00 AS LASTDAYOT, ;
				0000.00 AS LDOTWAGES, ;
				{} AS MAXDAY ;
				FROM TIMEDATA ;
				INTO CURSOR CURWEEK ;
				WHERE ADP_COMP == 'E87' ;
				AND BETWEEN(WORKDATE,ldStart,ldEnd) ;
				GROUP BY 1,2,3,4,5,6 ;
				ORDER BY 1,6 ;
				READWRITE

			* 
*!*				SELECT CURWEEK
*!*				SCAN FOR ((VACHOURS + PERHOURS) > 0) AND (OTHOURS > 0)
*!*				
*!*					SELECT NVL(SUM(TOTHOURS),000.00) AS LASTDAYOT, NVL(SUM(WAGEAMT),0000.00) AS LDOTWAGES ;
*!*						FROM TIMEDATA ;
*!*						INTO CURSOR CURLASTDAYOT ;
*!*						WHERE NAME == CURWEEK.NAME ;
*!*						AND ALLTRIM(PAY_TYPE) == 'OVERTIME' ;
*!*						AND WORKDATE IN (SELECT MAX(WORKDATE) AS WORKDATE FROM TIMEDATA WHERE NAME == CURWEEK.NAME AND BETWEEN(WORKDATE,CURWEEK.PPBEGIN,CURWEEK.PPEND))	
*!*						
*!*						REPLACE CURWEEK.LASTDAYOT WITH CURLASTDAYOT.LASTDAYOT, CURWEEK.LDOTWAGES WITH CURLASTDAYOT.LDOTWAGES IN CURWEEK						
*!*				ENDSCAN

			* ===> add HOLIDAY below?
			SELECT CURWEEK
			SCAN FOR ((VACHOURS + PERHOURS) > 0) AND (OTHOURS > 0)
			
				* get last day *worked* in the week for the person - ind EXCLUDES incentive pay, etc.
				SELECT MAX(WORKDATE) AS WORKDATE ;
					FROM TIMEDATA ;
					INTO CURSOR CURMAX ;
					WHERE NAME == CURWEEK.NAME ;
					AND BETWEEN(WORKDATE,CURWEEK.PPBEGIN,CURWEEK.PPEND)	;
					AND ALLTRIM(INPUNCHNM) == 'IND'
			
				ldLastWorkDay = NVL(CURMAX.WORKDATE,{})				
				
				* get ot for that day
				SELECT NVL(SUM(TOTHOURS),000.00) AS LASTDAYOT, NVL(SUM(WAGEAMT),0000.00) AS LDOTWAGES ;
					FROM TIMEDATA ;
					INTO CURSOR CURLASTDAYOT ;
					WHERE NAME == CURWEEK.NAME ;
					AND ALLTRIM(PAY_TYPE) == 'OVERTIME' ;
					AND WORKDATE = ldLastWorkDay 
					
					REPLACE CURWEEK.LASTDAYOT WITH CURLASTDAYOT.LASTDAYOT, CURWEEK.LDOTWAGES WITH CURLASTDAYOT.LDOTWAGES, CURWEEK.MAXDAY WITH ldLastWorkDay IN CURWEEK						
			ENDSCAN


			
			SELECT CURWEEK
			SCAN FOR ((VACHOURS + PERHOURS) > 0) AND (OTHOURS > 0)
				SCATTER MEMVAR
				INSERT INTO CURALL FROM MEMVAR			
			ENDSCAN
			
			ldDate = ldDate + 7  && advance dates a week
		ENDDO
		
		WAIT CLEAR
		
*!*			SELECT CURALL
*!*			BROWSE
		
		
		
*!*					AND ALLTRIM(PAY_TYPE) == 'OVERTIME' ;
		

*!*	*!*			BROWSE
		
		lcDetail = "C:\A\OT_FROM_PTO_2016.XLS"
		SELECT CURALL
		COPY TO (lcDetail) XL5 FOR LDOTWAGES > 0
		
		lcDetail2 = "C:\A\ALL_OT_FROM_PTO_2016.XLS"
		SELECT CURALL
		COPY TO (lcDetail2) XL5 
		
		
		
		RETURN
	ENDPROC  && OvertimeDueToPTO

	
	PROCEDURE CADriverHoursAllForLucille
	
		LOCAL ldStart, ldEnd, lcDetail, lcSummary
		ldStart = {^2016-05-01}
		ldEnd = {^2016-05-31}
	
		CLOSE DATABASES ALL

		SELECT ldStart AS FROMDATE, ldEnd AS TODATE, DIVISION, DEPT, NAME, WORKDATE, PAY_TYPE, TOTHOURS ;
		FROM F:\UTIL\TIMESTAR\DATA\TIMEDATA ;
		INTO CURSOR CURHOURS ;
		WHERE BETWEEN(WORKDATE,ldStart,ldEnd) ;
		AND DIVISION = '55' ;
		AND DEPT  = '0650';
		AND NOT INLIST(ALLTRIM(PAY_TYPE),"UNPAID") ;
		ORDER BY DIVISION, DEPT, NAME, WORKDATE ;
		READWRITE

		* FOR EACH WORKDATE, POPULATE THE SATURDAY WITH THE FOLLOWING SATURDAY'S DATE I.E., THE END OF THE PAY PERIOD
*!*			SELECT CURHOURS
*!*			BROWSE
		
		lcDetail = "C:\A\CADRIVER_ALL_HOURS_DETAIL_" + DTOS(ldStart) + "-" + DTOS(ldEnd) + ".XLS"
		SELECT CURHOURS
		COPY TO (lcDetail) XL5
		
		SELECT FROMDATE, TODATE, NAME, SUM(TOTHOURS) AS TOTHOURS ;
			FROM CURHOURS ;
			INTO CURSOR CURSUMMARY ;
			GROUP BY 1,2,3 ;
			ORDER BY 3
			
*!*			SELECT CURSUMMARY
*!*			BROWSE
		
		lcSummary = "C:\A\CADRIVER_ALL_HOURS_SUMMARY_" + DTOS(ldStart) + "-" + DTOS(ldEnd) + ".XLS"
		SELECT CURSUMMARY
		COPY TO (lcSummary) XL5
		
		
		RETURN
	ENDPROC  && CADriverHoursAllForLucille
	
	
	PROCEDURE NJForJimKillen
	
		CLOSE DATABASES ALL
	
		LOCAL ldToday, ldStart, ldEnd, lcDetail, lcSummary
		LOCAL lcAttach, lcFrom, lcBodyText, lcSendTo, lcCC, lcSubject
		
		ldToday = DATE()
		ldEnd = ldToday - DAY(ldToday)
		ldStart = GOMONTH(ldEnd, -1) + 1

		SELECT ldStart AS FROMDATE, ldEnd AS TODATE, DIVISION, DEPT, NAME, SUM(TOTHOURS) AS ALLHOURS, SUM(IIF(ALLTRIM(PAY_TYPE)=="OVERTIME",TOTHOURS,000.00)) AS OTHOURS ;
		FROM F:\UTIL\TIMESTAR\DATA\TIMEDATA ;
		INTO CURSOR CURHOURS ;
		WHERE BETWEEN(WORKDATE,ldStart,ldEnd) ;
		AND DIVISION IN ('04','14') ;
		AND INLIST(ALLTRIM(PAY_TYPE),"REGULAR","OVERTIME","GUARANTEE","SALARY","WRKDHOL") ;
		GROUP BY DIVISION, DEPT, NAME ;
		ORDER BY DIVISION, DEPT, NAME ;
		READWRITE
		
		lcDetail = "F:\UTIL\INSPERITY\REPORTS\NJ_HOURS_DETAIL_" + DTOS(ldStart) + "-" + DTOS(ldEnd) + ".XLS"
		SELECT CURHOURS
		COPY TO (lcDetail) XL5
		
*!*	SELECT CURHOURS
*!*	BROWSE


		IF FILE(lcDetail) THEN
			* attach output file to email
			lcAttach = lcDetail
			lcFrom = 'mark.bennett@tollgroup.com'
			lcBodyText = "See attached NJ Employee Monthly Hours report." 
			lcSubject = "NJ Employee Monthly Hours for " + DTOC(ldStart) + "-" + DTOC(ldEnd)	
			lcSendTo = 'mark.bennett@tollgroup.com'
			lcCC = 'mark.bennett@tollgroup.com'
		
			* send email...
			DO FORM dartmail2 WITH lcSendTo,lcFrom,lcSubject,lcCC,lcAttach,lcBodyText,"A"
			.TrackProgress('Sent NJ Employee Monthly Hours report email to ' + lcSendTo + '.',LOGIT+SENDIT)
				
		ELSE
			.TrackProgress('====> ERROR: unable to email spreadsheet: ' + lcDetail, LOGIT+SENDIT+NOWAITIT)
		ENDIF

		
*!*			SELECT FROMDATE, TODATE, NAME, SUM(TOTHOURS) AS TOTHOURS ;
*!*				FROM CURHOURS ;
*!*				INTO CURSOR CURSUMMARY ;
*!*				GROUP BY 1,2,3 ;
*!*				ORDER BY 3
			
*!*	*!*			SELECT CURSUMMARY
*!*	*!*			BROWSE
*!*			
*!*			lcSummary = "C:\A\CADRIVER_ALL_HOURS_SUMMARY_" + DTOS(ldStart) + "-" + DTOS(ldEnd) + ".XLS"
*!*			SELECT CURSUMMARY
*!*			COPY TO (lcSummary) XL5
		
		
		RETURN
	ENDPROC  && NJForJimKillen


	PROCEDURE CADriverDailyHoursForHector
		WITH THIS

			* Report on date 2 days ago, so that yesterday's time card corrections are reflected.
			LOCAL ld2DaysAgo, lnTotDollars, lcSpreadsheetTemplate, lcFileToSaveAs
			LOCAL oWorkbook, oWorksheet1, lnTotDollarsForDay					
			LOCAL lcSendToR, lcCCR, lcSubjectR, lcAttachR, lcBodyTextR
			
			ld2DaysAgo = DATE() - 2
			*ld2DaysAgo = {^2016-05-07}

			USE F:\UTIL\TIMESTAR\DATA\EEINFO.DBF IN 0 ALIAS EEINFO SHARED

			USE F:\UTIL\TIMESTAR\DATA\TIMEDATA.DBF IN 0 ALIAS TIMEDATA SHARED

			IF USED('CURTIMEDATAHOURLY') THEN
				USE IN CURTIMEDATAHOURLY
			ENDIF
			IF USED('CURSUMMARY') THEN
				USE IN CURSUMMARY
			ENDIF

			SELECT ;
				WORKDATE, ;
				NAME, ;
				FILE_NUM, ;
				PAY_TYPE, ;
				TOTHOURS, ;
				TOTDOLLARS ;
				FROM TIMEDATA ;
				INTO CURSOR CURTIMEDATAHOURLY ;
				WHERE (WORKDATE = ld2DaysAgo) ;
				AND DIVISION = '55' ;
				AND DEPT  = '0650';
				AND (NOT INLIST(PAY_TYPE,'FMLA','UNPAID','SUSPENSION')) ;
				ORDER BY 1,2

			*!*				SELECT CURTIMEDATAHOURLY
			*!*				BROWSE

			SELECT WORKDATE, ;
				NAME, ;
				FILE_NUM, ;
				SUM(IIF(ALLTRIM(PAY_TYPE) = 'GUARANTEE',TOTHOURS,0.00)) AS GTDHOURS, ;
				SUM(IIF(ALLTRIM(PAY_TYPE) = 'REGULAR',TOTHOURS,0.00)) AS REGHOURS, ;
				SUM(IIF(ALLTRIM(PAY_TYPE) = 'OVERTIME',TOTHOURS,0.00)) AS OVTHOURS, ;
				SUM(IIF(ALLTRIM(PAY_TYPE) = 'INCENTIVE',TOTDOLLARS,0.00)) AS INCPAY, ;
				SUM(IIF(INLIST(ALLTRIM(PAY_TYPE),'VACATION','SICK','PERSONAL'),TOTHOURS,0.00)) AS PTOHOURS, ;
				00.00 AS BASEWAGE, ;
				00000.00 AS TOTDOLLARS ;
				FROM CURTIMEDATAHOURLY ;
				INTO CURSOR CURSUMMARY ;
				GROUP BY WORKDATE, NAME, FILE_NUM ;
				ORDER  BY WORKDATE, NAME ;
				READWRITE

			*!*				SELECT CURSUMMARY
			*!*				BROWSE


			*!*	'GUARANTEE'
			*!*	'REGULAR'
			*!*	'OVERTIME'
			*!*	'INCENTIVE',
			*!*	'VACATION','SICK','PERSONAL'

			* Populate base wages and calc totdollars
			SELECT CURSUMMARY
			SCAN
				SELECT EEINFO
				LOCATE FOR INSPID = VAL(CURSUMMARY.FILE_NUM)

				IF FOUND() THEN
					REPLACE CURSUMMARY.BASEWAGE WITH EEINFO.HOURLYRATE IN CURSUMMARY

					lnTotDollars = ;
						(CURSUMMARY.BASEWAGE * CURSUMMARY.GTDHOURS) + ;
						(CURSUMMARY.BASEWAGE * CURSUMMARY.REGHOURS) + ;
						(CURSUMMARY.BASEWAGE * CURSUMMARY.OVTHOURS * 1.5) + ;
						(CURSUMMARY.BASEWAGE * CURSUMMARY.PTOHOURS) + ;
						CURSUMMARY.INCPAY

					REPLACE CURSUMMARY.TOTDOLLARS WITH lnTotDollars IN CURSUMMARY
				ELSE
					.TrackProgress('===> ERROR: could not find base wage for ' + CURSUMMARY.NAME,LOGIT)
				ENDIF
			ENDSCAN
			LOCATE
			
			SELECT CURSUMMARY
			SUM CURSUMMARY.TOTDOLLARS TO lnTotDollarsForDay


			lcFileToSaveAs = 'F:\UTIL\INSPERITY\REPORTS\CADRIVER_' + DTOS(ld2DaysAgo) + '.XLS'

			lcSpreadsheetTemplate = "F:\UTIL\INSPERITY\TEMPLATES\CADRIVER_DAILY_TEMPLATE.XLS"

			IF FILE(lcFileToSaveAs) THEN
				DELETE FILE (lcFileToSaveAs)
			ENDIF

			** output to Excel spreadsheet
			.TrackProgress('Creating spreadsheet: ' + lcFileToSaveAs, LOGIT+SENDIT+NOWAITIT)

			oWorkbook = .oExcel.workbooks.OPEN(lcSpreadsheetTemplate)
			oWorkbook.SAVEAS(lcFileToSaveAs)

			oWorksheet1 = oWorkbook.Worksheets[1]
			oWorksheet1.RANGE("A4","J200").ClearContents()
			lcSubjectR = "CA Driver Daily Report for " + TRANSFORM(ld2DaysAgo)
			oWorksheet1.RANGE("A1").VALUE = lcSubjectR
			oWorksheet1.RANGE("G1").VALUE = lnTotDollarsForDay

			lnRow = 3

			SELECT CURSUMMARY
			SCAN
				lnRow = lnRow + 1
				lcRow = ALLTRIM(STR(lnRow))

				oWorksheet1.RANGE("A"+lcRow).VALUE = CURSUMMARY.WORKDATE
				oWorksheet1.RANGE("B"+lcRow).VALUE = CURSUMMARY.NAME
				oWorksheet1.RANGE("C"+lcRow).VALUE = "'" + CURSUMMARY.FILE_NUM
				oWorksheet1.RANGE("D"+lcRow).VALUE = CURSUMMARY.GTDHOURS
				oWorksheet1.RANGE("E"+lcRow).VALUE = CURSUMMARY.REGHOURS
				oWorksheet1.RANGE("F"+lcRow).VALUE = CURSUMMARY.OVTHOURS
				oWorksheet1.RANGE("G"+lcRow).VALUE = CURSUMMARY.INCPAY
				oWorksheet1.RANGE("H"+lcRow).VALUE = CURSUMMARY.PTOHOURS
				oWorksheet1.RANGE("I"+lcRow).VALUE = CURSUMMARY.BASEWAGE
				oWorksheet1.RANGE("J"+lcRow).VALUE = CURSUMMARY.TOTDOLLARS
			ENDSCAN

			oWorkbook.SAVE()
			.oExcel.QUIT()

			IF FILE(lcFileToSaveAs) THEN
				* attach output file to email
				lcAttachR = lcFileToSaveAs
				lcBodyTextR = "See attached CA Driver Daily report." 
					
				lcSendToR = 'cesar.nevares@tollgroup.com'
				lcCCR = 'mark.bennett@tollgroup.com'
			
				* send email...
				DO FORM dartmail2 WITH lcSendToR,.cFrom,lcSubjectR,lcCCR,lcAttachR,lcBodyTextR,"A"
				.TrackProgress('Sent CA Driver Daily Report email to ' + lcSendToR + '.',LOGIT+SENDIT)
					
			ELSE
				.TrackProgress('ERROR: unable to email spreadsheet: ' + lcFileToSaveAs, LOGIT+SENDIT+NOWAITIT)
			ENDIF

			RETURN
		ENDWITH

	ENDPROC  &&  CADriverDailyHoursForHector



	PROCEDURE CADriverHoursSummaryByWeek
	
		LOCAL ldSaturday, lnDayOfWeek
	
		CLOSE DATABASES ALL

		SELECT DIVISION, DEPT, NAME, WORKDATE, PAY_TYPE, TOTHOURS, {} AS WKENDING ;
		FROM F:\UTIL\TIMESTAR\DATA\TIMEDATA ;
		INTO CURSOR CURHOURS ;
		WHERE BETWEEN(WORKDATE,{^2016-08-01},{^2016-08-31}) ;
		AND DIVISION = '55' ;
		AND DEPT  = '0650';
		ORDER BY DIVISION, DEPT, NAME, WORKDATE ;
		READWRITE

		* FOR EACH WORKDATE, POPULATE THE SATURDAY WITH THE FOLLOWING SATURDAY'S DATE I.E., THE END OF THE PAY PERIOD
		SELECT CURHOURS
		SCAN
			lnDayOfWeek = DOW(CURHOURS.WORKDATE,1)  && SUNDAY = 1, SATURDAY = 7	
			ldSaturday = CURHOURS.WORKDATE + 7 - lnDayOfWeek
			REPLACE CURHOURS.WKENDING WITH ldSaturday IN CURHOURS
		ENDSCAN

		
*!*			SELECT CURHOURS
*!*			BROWSE
		
		SELECT NAME, WKENDING, PAY_TYPE, SUM(TOTHOURS) AS TOTHOURS ;
		FROM CURHOURS INTO CURSOR CURHOURS2 ;
		GROUP BY NAME, WKENDING, PAY_TYPE ;
		ORDER BY NAME, WKENDING, PAY_TYPE 
		
		SELECT NAME, WKENDING, SUM(TOTHOURS) AS TOTHOURS ;
		FROM CURHOURS INTO CURSOR CURTOTBYWEEK ;
		GROUP BY NAME, WKENDING ;
		ORDER BY NAME, WKENDING 
		
*!*			SELECT CURTOTBYWEEK
*!*			BROWSE
*!*			
*!*			SELECT CURHOURS2 
*!*			BROWSE

		SELECT CURHOURS2
		COPY TO C:\A\CADriverHoursSummaryByWeek-Unfiltered-2015.XLS XL5
		
		* FILTER FOR ONLY WEEKS THAT HAD 'GUARANTEE' PAY_TYPE AND HAD > 40 TOT HOURS IN THE WEEK
		SELECT A.NAME, A.WKENDING, A.PAY_TYPE, A.TOTHOURS ;
		FROM CURHOURS2 A ;
		INTO CURSOR CURHOURS3 ;
		WHERE EXISTS (SELECT * FROM CURHOURS2 WHERE (WKENDING = A.WKENDING) AND (NAME = A.NAME) AND (PAY_TYPE = 'GUARANTEE')) ;
		AND EXISTS (SELECT * FROM CURTOTBYWEEK WHERE (WKENDING = A.WKENDING) AND (NAME = A.NAME) AND (TOTHOURS > 40)) ;
		GROUP BY NAME, WKENDING, PAY_TYPE ;
		ORDER BY NAME, WKENDING, PAY_TYPE 
		
		SELECT CURHOURS3
		COPY TO C:\A\CADriverHoursSummaryByWeek-2015.XLS XL5
		
	
		RETURN
	ENDPROC  &&  CADriverHoursSummaryByWeek
	
	
	PROCEDURE GetDriverHoursByMonth
		LPARAMETERS tdStartDate
		* for Jack Drohan
		WITH THIS
		
			LOCAL ldFromDate, ldToDate, lc03DriversFile, lc55DriversFile, lcALLDriversFile
			
			ldFromDate = tdStartDate
			ldToDate = GOMONTH(ldFromDate,1) - 1
			
			.TrackProgress('Date Range = ' + TRANSFORM(ldFromDate) + " thru " + TRANSFORM(ldToDate),LOGIT+SENDIT)			
		
			IF USED('EEINFO') THEN
				USE IN EEINFO
			ENDIF
			IF USED('CURTIMEDATAPRE') THEN
				USE IN CURTIMEDATAPRE
			ENDIF
			IF USED('CURTIMEDATA') THEN
				USE IN CURTIMEDATA
			ENDIF
			IF USED('TIMEDATA') THEN
				USE IN TIMEDATA
			ENDIF

			USE F:\UTIL\TIMESTAR\DATA\EEINFO.DBF IN 0 ALIAS EEINFO

			USE F:\UTIL\TIMESTAR\DATA\TIMEDATA.DBF IN 0 ALIAS TIMEDATA

			* select only local drivers/switchers, no otr drivers, per Jack 
			SELECT ;
				NAME, ;
				WORKDATE AS PAYDATE, ;
				FILE_NUM, ;
				DIVISION, ;
				DEPT, ;
				(DIVISION + DEPT) AS HOMEDEPT, ;
				WORKSITE, ;
				ADP_COMP, ;
				'DRIVER' AS JOBDESC, ;
				(CODEHOURS + REGHOURS + OTHOURS + DTHOURS) AS HOURS ;
				FROM TIMEDATA ;
				INTO CURSOR CURTIMEDATAPRE ;
				WHERE WORKDATE >= ldFromDate ;
				AND WORKDATE <= ldToDate ;
				AND (DIVISION IN ('03','55')) ;
				AND (DEPT IN ('0650','0655','0660')) ;
				AND LEN(ALLTRIM(WORKSITE)) = 3 ;
				ORDER BY NAME, WORKDATE ;
				READWRITE

			* provide driver detail for Jack.
			lcALLDriversFile = "F:\UTIL\HR\OSHA\REPORTS\DRIVER-TIME-" + DTOS(ldFromDate) + "-" + DTOS(ldToDate) + ".XLS"
			lc03DriversFile = "F:\UTIL\HR\OSHA\REPORTS\DIV03_DRIVER-TIME-" + DTOS(ldFromDate) + "-" + DTOS(ldToDate) + ".XLS"
			lc55DriversFile = "F:\UTIL\HR\OSHA\REPORTS\DIV55_DRIVER-TIME-" + DTOS(ldFromDate) + "-" + DTOS(ldToDate) + ".XLS"

			IF USED('CURHOURS') THEN
				USE IN CURHOURS
			ENDIF

			SELECT DTOS(ldFromDate) + "-" + DTOS(ldToDate) AS DATERANGE, ;
				DIVISION, ;
				WORKSITE, ;
				NAME, ;
				JOBDESC, ;
				SUM(HOURS) AS WRKDHOURS ;
				FROM CURTIMEDATAPRE  ;
				INTO CURSOR CURHOURS ;
				GROUP BY 1, 2, 3, 4, 5 ;
				ORDER BY 1, 2, 3, 4

			SELECT CURHOURS
			COPY TO (lcALLDriversFile) XL5

			*.cAttach = .cAttach + "," + lcDriverDetailFile
			
			.cAttach = lcALLDriversFile

		ENDWITH
		RETURN
	ENDPROC && GetDriverHoursByMonth



	PROCEDURE EETakingsAndBalances
		LPARAMETERS tcACCRcode
		WITH THIS

			* Derived from TimeStar report: Data Export-Accruals.
			* BEFORE PROCEEDING: RUN THAT REPORT AND LOAD ACCRUALS DATA FOR THE WHOLE YEAR!

			LOCAL ldToday, ldJan1st, ldDec31st, ldTomorrow, lcOutputFile1, lcACCRcode, lcOutputFile2
			LOCAL lnTaken, lnToBeTaken, lnYearEndBal, lnCurBal, lnAdjusts, lnNegToDate, lcWhere, lnTotYearAutoAccruals, lnAccrToday

			ldJan1st = {^2018-01-01}
			
			ldDec31st = {^2018-12-31}
			
			*ldToday = DATE()  && the "as of" date
			ldToday = {^2018-03-31}  && the "as of" date
			
			ldTomorrow = ldToday + 1

			lcACCRcode = UPPER(ALLTRIM(tcACCRcode))

			* OPEN THE ACCRUALS INFO TABLE
			IF USED('EEINFO') THEN
				USE IN EEINFO
			ENDIF
			IF USED('ACCRDATA') THEN
				USE IN ACCRDATA
			ENDIF
			IF USED('CURLIABILITY1') THEN
				USE IN CURLIABILITY1
			ENDIF
			IF USED('CURLIABILITY2') THEN
				USE IN CURLIABILITY2
			ENDIF

			USE F:\UTIL\TIMESTAR\DATA\ACCRDATA IN 0 ALIAS ACCRDATA SHARED

			USE F:\UTIL\TIMESTAR\DATA\EEINFO IN 0 ALIAS EEINFO SHARED
			
			USE F:\UTIL\INSPERITY\DATA\WORKSITES IN 0 ALIAS WORKSITES SHARED

			* transcode	transname
			* AC		Accrual
			* AJ		Accrual Adjustment
			* CA		Carryover
			* CJ		Carryover Adjustment
			* EX		Expired
			* MA		Manual Accrual
			* MC		Manual Carryover
			* MT		Manual Taken
			* TA		Taken
			
			
			* get distinct ee list
			SELECT DISTINCT NAME, INSPID, ;
				0000.00 AS Taken, 0000.00 AS Adjusts, 0000.00 AS CurBal, 0000.00 AS ToBeTaken, 0000.00 AS YearEndBal,  0000.00 AS NegToDate, 0000.00 AS TOTYEARAC, 0000.00 AS ACCRTODAY ;
				FROM ACCRDATA ;
				INTO CURSOR CURLIABILITY1 ;
				ORDER BY NAME, INSPID ;
				READWRITE
			
			SELECT CURLIABILITY1
			SCAN
			
				SELECT ACCRDATA
				SUM HOURS TO lnNegToDate  FOR (INSPID = CURLIABILITY1.INSPID) AND (ACCRCODE = lcACCRcode) AND BETWEEN(EFFDATE,ldJan1st,ldToday) AND (HOURS < 0.0)
				SUM HOURS TO lnTaken      FOR (INSPID = CURLIABILITY1.INSPID) AND (ACCRCODE = lcACCRcode) AND BETWEEN(EFFDATE,ldJan1st,ldToday) AND INLIST(TRANSCODE,'MT','TA')
				SUM HOURS TO lnCurBal     FOR (INSPID = CURLIABILITY1.INSPID) AND (ACCRCODE = lcACCRcode) AND BETWEEN(EFFDATE,ldJan1st,ldToday)
				* ADDED field 8/17/2017 to help test the 8/16/2017 accruals rules changes implemented by Jordan Thomson of TimeStar. This totals JUST AUTO accruals entries.
				SUM HOURS TO lnAccrToday FOR (INSPID = CURLIABILITY1.INSPID) AND (ACCRCODE = lcACCRcode) AND (EFFDATE =ldToday) AND INLIST(TRANSCODE,'AC')
				
				lnAdjusts = lnNegToDate - lnTaken
				SUM HOURS TO lnYearEndBal FOR (INSPID = CURLIABILITY1.INSPID) AND (ACCRCODE = lcACCRcode) AND BETWEEN(EFFDATE,ldJan1st,ldDec31st) AND (NOT INLIST(TRANSCODE,'CJ','EX'))
				SUM HOURS TO lnToBeTaken  FOR (INSPID = CURLIABILITY1.INSPID) AND (ACCRCODE = lcACCRcode) AND BETWEEN(EFFDATE,ldTomorrow,ldDec31st) AND INLIST(TRANSCODE,'MT','TA')
				* lnTotYearAutoAccruals IS TOTAL SCHEDULED accrual for the year, not counting adjustments, carryovers, etc. 
				* IT WILL NOT WORK FOR DRIVERS BECAUSE THEIR ACCRUALS CHANGE MID-YEAR.
				SUM HOURS TO lnTotYearAutoAccruals FOR (INSPID = CURLIABILITY1.INSPID) AND (ACCRCODE = lcACCRcode) AND BETWEEN(EFFDATE,ldJan1st,ldDec31st) AND INLIST(TRANSCODE,'AC')
				LOCATE
				
				REPLACE ;
					CURLIABILITY1.Taken WITH lnTaken, ;
					CURLIABILITY1.Adjusts WITH lnAdjusts, ;
					CURLIABILITY1.CurBal WITH lnCurBal, ;
					CURLIABILITY1.ToBeTaken WITH lnToBeTaken, ;
					CURLIABILITY1.YearEndBal WITH lnYearEndBal, ;
					CURLIABILITY1.ACCRTODAY WITH lnAccrToday, ;
					CURLIABILITY1.TOTYEARAC WITH lnTotYearAutoAccruals, ;
					CURLIABILITY1.NegToDate WITH lnNegToDate ;
					IN CURLIABILITY1
					
					
			ENDSCAN
			
*!*				SELECT CURLIABILITY1
*!*				BROWSE

			lcWhere = ""  && used to define special filtering
			*lcWhere = "WHERE B.WORKSITE = 'SP2' AND (NOT (B.DIVISION='55' AND B.DEPT='0650')) "  && used to define special filtering
			*lcWhere = "WHERE B.WORKSITE = 'SP2' AND (B.DIVISION='55' AND B.DEPT='0650') "  && used to define special filtering
			*lcWhere = "AND C.WSTATE = 'CA' "
*!*					AND B.ADP_COMP <> 'E87' ;

				lcWhere = " WHERE B.STATUS <> 'T' "
			
*				WHERE B.DIVISION = '03' ;*
*				AND B.DEPT = '0650' ;

*!*				SELECT B.STATUS, B.WORKSITE, C.LOCNAME, lcACCRcode AS ACCRUAL, B.ADP_COMP, A.INSPID, B.HIREDATE, B.PTODATE, B.NAME, B.DIVISION, ;
*!*					A.Taken, A.Adjusts, A.ToBeTaken, A.CurBal, A.YearEndBal, A.TOTYEARAC ;
*!*					FROM CURLIABILITY1 A ;
*!*					LEFT OUTER JOIN EEINFO B ;
*!*					ON B.INSPID = A.INSPID ;
*!*					LEFT OUTER JOIN WORKSITES C ;
*!*					ON C.WWORKSITE = B.WORKSITE ;
*!*					INTO CURSOR CURLIABILITY2 ;
*!*					&lcWhere ;
*!*					ORDER BY B.STATUS, B.NAME ;
*!*					READWRITE

			SELECT B.STATUS, B.BUSUNIT, B.WORKSITE, B.WRKDSTATE, B.TIMEZONE, C.LOCNAME, lcACCRcode AS ACCRUAL, B.ADP_COMP, A.INSPID, B.HIREDATE, B.PTODATE, B.NAME, B.JOBTITLE, B.SUPERVISOR, B.DIVISION, ;
				A.Taken, A.Adjusts, A.CurBal, A.YearEndBal ;
				FROM CURLIABILITY1 A ;
				LEFT OUTER JOIN EEINFO B ;
				ON B.INSPID = A.INSPID ;
				LEFT OUTER JOIN WORKSITES C ;
				ON C.WWORKSITE = B.WORKSITE ;
				INTO CURSOR CURLIABILITY2 ;
				&lcWhere ;
				ORDER BY B.ADP_COMP, B.NAME ;
				READWRITE

			*lcOutputFile1 = 'F:\UTIL\TIMESTAR\REPORTS\' + lcACCRcode +'_EETakingsAndBalances_AS_OF_' + DTOS(ldToday) + '.XLS'
			lcOutputFile1 = 'C:\A\' + lcACCRcode +'_EETakingsAndBalances_AS_OF_' + DTOS(ldToday) + '.XLS'
			SELECT CURLIABILITY2
			COPY TO (lcOutputFile1) XL5 

			lcOutputFile2 = 'C:\A\HOURLY_' + lcACCRcode +'_EETakingsAndBalances_AS_OF_' + DTOS(ldToday) + '.XLS'
			SELECT CURLIABILITY2
			COPY TO (lcOutputFile2) XL5 FOR ADP_COMP = 'E87'

			lcOutputFile3 = 'C:\A\SALARIED_' + lcACCRcode +'_EETakingsAndBalances_AS_OF_' + DTOS(ldToday) + '.XLS'
			SELECT CURLIABILITY2
			COPY TO (lcOutputFile3) XL5 FOR ADP_COMP <> 'E87'
			
			lcOutputFileCA = 'C:\A\CA_' + lcACCRcode +'_EETakingsAndBalances_AS_OF_' + DTOS(ldToday) + '.XLS'
			lcOutputFileNonCA = 'C:\A\Non_CA_' + lcACCRcode +'_EETakingsAndBalances_AS_OF_' + DTOS(ldToday) + '.XLS'
			
			SELECT CURLIABILITY2
			COPY TO (lcOutputFileCA) XL5 FOR wrkdstate = 'CA'
			
			SELECT CURLIABILITY2
			COPY TO (lcOutputFileNonCA) XL5 FOR wrkdstate <> 'CA'
			
			
			.cAttach = lcOutputFile1 

			IF USED('EEINFO') THEN
				USE IN EEINFO
			ENDIF
			IF USED('ACCRDATA') THEN
				USE IN ACCRDATA
			ENDIF
			IF USED('CURLIABILITY1') THEN
				USE IN CURLIABILITY1
			ENDIF
			IF USED('CURLIABILITY2') THEN
				USE IN CURLIABILITY2
			ENDIF

		ENDWITH
		RETURN
	ENDPROC  && EETakingsAndBalances



	PROCEDURE PRDATAFORECASTERKEN

		USE F:\UTIL\TIMESTAR\DATA\EEINFO.DBF AGAIN IN 0 ALIAS EEINFO

		SELECT DIVISION, DEPT, NAME, INSPID AS EMP_NO, ANNSALARY AS SALARY, '0' AS MEDINS, '0' AS MOCARALLOW, '0' AS MO401K, STATE AS WORKSTATE, DEPTDESC, ;
		0000.00 AS HEALTH, 0000.00 AS CARALLOW, 0000.00 AS DED401KAMT ;
		FROM EEINFO ;
		INTO CURSOR FORECAST ;
		WHERE STATUS <> 'T' ;
		ORDER BY DIVISION, DEPT ;
		READWRITE

*!*			SELECT FORECAST
*!*			BROWSE


		*THEN IN FOXPRO: RENAME SQLCURSOR.DBF TO FORECAST.DBF AND RUN M:\MarkB\prg\HR_KEN_DATA_FORECAST.PRG

*!*			LOCAL lcMEDINS, lcControlChar

*!*			SELECT FORECAST
*!*			SCAN

*!*				lcControlChar = LEFT(FORECAST.REFJOB,1)
*!*				
*!*			*!*		IF (FORECAST.DED401KAMT + FORECAST.DED401KPCT) > 0 THEN
*!*			*!*			REPLACE FORECAST.MO401K WITH '401K'
*!*			*!*		ELSE
*!*			*!*			REPLACE FORECAST.MO401K WITH '0'
*!*			*!*		ENDIF
*!*				
*!*				IF FORECAST.CARALLOW > 0 THEN
*!*					REPLACE FORECAST.MOCARALLOW WITH 'CA' + TRANSFORM(INT(FORECAST.CARALLOW))
*!*				ELSE
*!*					REPLACE FORECAST.MOCARALLOW WITH '0'
*!*				ENDIF
*!*				
*!*				IF FORECAST.HEALTH > 0 THEN
*!*					DO CASE
*!*						CASE lcControlChar = '8'
*!*							lcMEDINS = 'MED GA'
*!*						OTHERWISE
*!*							lcMEDINS = 'MED OP'
*!*					ENDCASE
*!*				ELSE
*!*					lcMEDINS = '0'
*!*				ENDIF
*!*				REPLACE FORECAST.MEDINS WITH lcMEDINS
*!*				
*!*			ENDSCAN

*!*			*!*				CASE lcControlChar = '6' AND INLIST(FORECAST.DEPT,'02','03')
*!*			*!*					lcMEDINS = 'MED TK'

		SELECT FORECAST
		COPY TO C:\A\FORECAST_2015.XLS XL5

		CLOSE DATABASES ALL

		RETURN
	ENDPROC  &&  PRDATAFORECASTERKEN
	


	PROCEDURE EESALARYINFOFORKEN

		USE F:\UTIL\TIMESTAR\DATA\EEINFO.DBF AGAIN IN 0 ALIAS EEINFO

		SELECT DIVISION, DEPT, DEPTDESC, NAME, HIREDATE, ANNSALARY AS SALARY ;
		FROM EEINFO ;
		INTO CURSOR CURSALARYINFO ;
		WHERE STATUS <> 'T' ;
		ORDER BY DIVISION, DEPT, NAME ;
		READWRITE


		SELECT CURSALARYINFO
		BROWSE
		*COPY TO C:\A\CURSALARYINFO_2015-07-15.XLS XL5

		CLOSE DATABASES ALL

		RETURN
	ENDPROC  &&  PRDATAFORECASTERKEN
	
	
	PROCEDURE SPTERMINATEDFORCARLINA
	
		USE F:\UTIL\TIMESTAR\DATA\EEINFO.DBF AGAIN IN 0 ALIAS EEINFO
		
		SELECT DIVISION, DEPT, WORKSITE, NAME, STATUS, HIREDATE, TERMDATE ;
		FROM EEINFO ;
		INTO CURSOR CURTERMS ;
		WHERE STATUS = 'T' AND WORKSITE = 'SP2' ;
		ORDER BY 1, 2, 3
		
		SELECT CURTERMS
		COPY TO C:\A\SP_TERMS.XLS XL5
		
		CLOSE DATABASES ALL
		
		RETURN
		
	ENDPROC	&& SPTERMINATEDFORCARLINA
	
	
	PROCEDURE CADriverHoursByDayReport
		
		* all hours, not just worked, for carlina
		CLOSE DATABASES ALL
	
		USE F:\UTIL\TIMESTAR\DATA\EEINFO.DBF AGAIN IN 0 ALIAS EEINFO
	
		USE F:\UTIL\TIMESTAR\DATA\TIMEDATA AGAIN IN 0 ALIAS TIMEDATA
				
		* detail
		SELECT NAME, INT(VAL(FILE_NUM)) AS INSPID, WORKDATE, SUM(TOTHOURS) AS TOTHOURS ;
		FROM TIMEDATA ;
		INTO CURSOR CURDAYS ;
		WHERE (DIVISION = '55') AND (DEPT = '0650') ;
		AND WORKDATE >= {^2015-01-01} ;
		AND TOTHOURS > 0.00 ;
		GROUP BY NAME, FILE_NUM, WORKDATE ;
		ORDER BY NAME, WORKDATE
		
		SELECT CURDAYS 
		COPY TO C:\A\CAHOURSBYDAY_DETAIL.XLS XL5
		
		*!*	* ALSO SEE WHO DID NOT WORK...
		*!*	SELECT NAME FROM EEINFO ;
		*!*	WHERE (DIVISION = '55') AND (DEPT = '0650') ;
		*!*	AND STATUS <> 'T' ;
		*!*	AND INSPID NOT IN (SELECT INSPID FROM CURDAYS) ;
		*!*	INTO CURSOR CURDIDNOTWORK ;
		*!*	ORDER BY NAME

		*!*	SELECT CURDIDNOTWORK
		*!*	COPY TO C:\A\DID_NOT_WORK.XLS XL5



		*!*	* summary
		*!*	SELECT NAME, COUNT(DISTINCT WORKDATE) AS DAYSWORKED ;
		*!*	FROM CURDAYS ;
		*!*	INTO CURSOR CURDAYSSUM ;
		*!*	GROUP BY NAME ;
		*!*	ORDER BY NAME

		*!*	SELECT CURDAYSSUM 
		*!*	COPY TO C:\A\DAYSWORKED_SUMMARY.XLS XL5
			
		RETURN
	ENDPROC  &&  CADriverHoursByDayReport



	PROCEDURE GetVacCarryoverAndTaken
		WITH THIS

			* for Marie: show VAC carryover and taken, balance and $
			
			* BEFORE PROCEEDING: RUN THAT REPORT AND LOAD ACCRUALS DATA!

			LOCAL ldToday, ldJan1st, lnUntaken, lnLiability, lcOutputFile1, lcOutputFile2, lcOutputFile3, lcACCRcode

			ldToday = DATE()
			*ldToday = {^2017-01-29}  && the "as of" date

			ldJan1st = {^2017-01-01}

			lcACCRcode = "VAC"

			* OPEN THE ACCRUALS INFO TABLE
			IF USED('EEINFO') THEN
				USE IN EEINFO
			ENDIF
			IF USED('ACCRDATA') THEN
				USE IN ACCRDATA
			ENDIF
			IF USED('CURLIABILITY1') THEN
				USE IN CURLIABILITY1
			ENDIF
			IF USED('CURLIABILITY2') THEN
				USE IN CURLIABILITY2
			ENDIF

			USE F:\UTIL\TIMESTAR\DATA\ACCRDATA IN 0 ALIAS ACCRDATA SHARED

			USE F:\UTIL\TIMESTAR\DATA\EEINFO IN 0 ALIAS EEINFO SHARED

			* transcode	transname
			* AC		Accrual
			* AJ		Accrual Adjustment
			* CA		Carryover
			* CJ		Carryover Adjustment
			* EX		Expired
			* MA		Manual Accrual
			* MC		Manual Carryover
			* MT		Manual Taken
			* TA		Taken

			SELECT ;
				INSPID, ;
				SUM(IIF(INLIST(TRANSCODE,'CA'),HOURS,000.00)) AS CARRYOVER, ;
				SUM(IIF(INLIST(TRANSCODE,'MC'),HOURS,000.00)) AS CARRY_ADJ, ;
				SUM(IIF(INLIST(TRANSCODE,'TA'),HOURS,000.00)) AS TAKEN, ;
				SUM(IIF(INLIST(TRANSCODE,'MT'),HOURS,000.00)) AS TAKEN_ADJ ;
				FROM ACCRDATA ;
				INTO CURSOR CURLIABILITY1 ;
				WHERE BETWEEN(EFFDATE, ldJan1st, ldToday) ;
				AND ACCRCODE = lcACCRcode ;
				GROUP BY INSPID ;
				ORDER BY INSPID

			*!*				SELECT CURLIABILITY1
			*!*				BROWSE

			SELECT lcACCRcode AS ACCRUAL, B.ADP_COMP, A.INSPID, B.HIREDATE, B.PTODATE, B.NAME, B.DIVISION, B.WORKSITE, B.SUPERVISOR, B.HOURLYRATE, A.CARRYOVER, A.CARRY_ADJ, A.TAKEN, A.TAKEN_ADJ, 0000.00 AS BALANCE_HRS, 0000000.00 AS LIABILITY ;
				FROM CURLIABILITY1 A ;
				LEFT OUTER JOIN EEINFO B ;
				ON B.INSPID = A.INSPID ;
				INTO CURSOR CURLIABILITY2 ;
				WHERE B.STATUS <> 'T' ;
				ORDER BY B.NAME ;
				READWRITE

*!*					AND B.ADP_COMP <> 'E87' ;

*!*				SELECT CURLIABILITY2
*!*				SCAN
*!*					lnUntaken = CURLIABILITY2.ACCRUED_HRS + CURLIABILITY2.TAKEN_HRS && Adding here because Takens are negative in TimeStar, not positive
*!*					lnLiability = lnUntaken * CURLIABILITY2.HOURLYRATE
*!*					REPLACE CURLIABILITY2.BALANCE_HRS WITH lnUntaken, CURLIABILITY2.LIABILITY WITH 	lnLiability IN CURLIABILITY2
*!*				ENDSCAN

*!*				lcOutputFile1 = 'F:\UTIL\TIMESTAR\REPORTS\HOURLY_' + lcACCRcode +'_LIABILITY_THRU_' + DTOS(ldToday) + '.XLS'
*!*				SELECT CURLIABILITY2
*!*				COPY TO (lcOutputFile1) XL5 FOR ADP_COMP = 'E87'

*!*				lcOutputFile2 = 'F:\UTIL\TIMESTAR\REPORTS\SALARIED_' + lcACCRcode +'_LIABILITY_THRU_' + DTOS(ldToday) + '.XLS'
*!*				SELECT CURLIABILITY2
*!*				COPY TO (lcOutputFile2) XL5 FOR ADP_COMP <> 'E87'	
*!*				
*!*				.cAttach = lcOutputFile1 + "," + lcOutputFile2

			lcOutputFile3 = 'F:\UTIL\TIMESTAR\REPORTS\HOURLY_AND_SALARIED_' + lcACCRcode +'_CARRYOVER_AS_OF_' + DTOS(ldToday) + '.XLS'
			SELECT CURLIABILITY2
			COPY TO (lcOutputFile3) XL5 
			
			.cAttach = lcOutputFile3 
*!*				.cAttach = lcOutputFile1 + "," + lcOutputFile2 + "," + lcOutputFile3

			IF USED('EEINFO') THEN
				USE IN EEINFO
			ENDIF
			IF USED('ACCRDATA') THEN
				USE IN ACCRDATA
			ENDIF
			IF USED('CURLIABILITY1') THEN
				USE IN CURLIABILITY1
			ENDIF
			IF USED('CURLIABILITY2') THEN
				USE IN CURLIABILITY2
			ENDIF

		ENDWITH
		RETURN
	ENDPROC  && GetVacCarryoverAndTaken




	PROCEDURE GetDiv0203Hours
	
		CLOSE DATABASES ALL

		SELECT DIVISION, DEPT, NAME, WORKDATE, YEAR(WORKDATE) AS YEAR, PAY_TYPE, TOTHOURS ;
		FROM F:\UTIL\TIMESTAR\DATA\TIMEDATA ;
		INTO CURSOR CURHOURS ;
		WHERE BETWEEN(WORKDATE,{^2014-01-01},{^2014-12-31}) ;
		AND DIVISION IN ('02','03') ;
		AND DEPT IN ('0650','0655','0660') ;
		ORDER BY DIVISION, DEPT, NAME, WORKDATE
		
*!*			SELECT CURHOURS
*!*			BROWSE
		
		SELECT DIVISION, DEPT, NAME, YEAR, SUM(TOTHOURS) AS TOTHOURS ;
		FROM CURHOURS INTO CURSOR CURHOURS2 ;
		GROUP BY DIVISION, DEPT, NAME, YEAR ;
		ORDER BY DIVISION, DEPT, NAME, YEAR
		
*!*			SELECT CURHOURS2
*!*			BROWSE

		SELECT CURHOURS2
		COPY TO C:\A\DIV0203HOURS-2014.XLS XL5
	
		RETURN
	ENDPROC
	
	
	PROCEDURE GetGrossPayAndCarAllowance
	
		* roll up gross pay and carallow by worked state - for Todd, KPMG
		* Optionally combine with ADP data, if reporting before 2014
		
		
		CLOSE DATABASES ALL
	
		USE F:\UTIL\INSPERITY\DATA\GPAYALLOW IN 0 ALIAS GPAYALLOW
		
		
		* select all from GPAYALLOW - which has Insperity data - into a cursor, REVERSING THE SIGN FOR CARALLOW 
		
		SELECT * FROM GPAYALLOW INTO CURSOR CURGPAYALLOW READWRITE
		
		SELECT CURGPAYALLOW
		SCAN
			REPLACE CURGPAYALLOW.CARALLOW WITH (-1 * CURGPAYALLOW.CARALLOW) IN CURGPAYALLOW
		ENDSCAN
		
*!*			SELECT CURGPAYALLOW
*!*			BROW

*!*			* OPTIONALLY IMPORT DATA FROM ADP
*!*			SELECT CURGPAYALLOW
*!*			APPEND FROM C:\A\WCGROSSPAY
*!*			BROWSE

*!*			SELECT CURGPAYALLOW
*!*			APPEND FROM C:\A\WCCARALLOW
*!*			BROWSE

		* now roll up everything by workedstate		
				
		SELECT WKDSTATE, COUNT(*) AS HEADCNT, SUM(GROSSPAY) AS GROSSPAY, SUM(CARALLOW) AS CARALLOW ;
		FROM CURGPAYALLOW ;
		INTO CURSOR CURFINAL ;
		GROUP BY WKDSTATE ;
		ORDER BY WKDSTATE

		* now roll up everything by division		
				
		SELECT DIVISION, COUNT(*) AS HEADCNT, SUM(GROSSPAY) AS GROSSPAY, SUM(CARALLOW) AS CARALLOW ;
		FROM CURGPAYALLOW ;
		INTO CURSOR CURDIVS ;
		GROUP BY division ;
		ORDER BY division 
		
		
		
		SELECT CURFINAL 
		BROW

		SELECT CURFINAL 
		*COPY TO C:\A\WC_GROSSPAY_CARALLOW_BY_WKDSTATE_07012013-06302014.XLS XL5
		COPY TO C:\A\WC_GROSSPAY_CARALLOW_BY_WKDSTATE_07012015-03312016.XLS XL5

		SELECT CURDIVS
		COPY TO C:\A\WC_GROSSPAY_CARALLOW_BY_DIVISION_07012015-03312016.XLS XL5
		
		SELECT GPAYALLOW
		COPY TO C:\A\WC_GROSSPAY_CARALLOW_DETAIL_07012015-03312016.XLS XL5
		
	
		RETURN
	ENDPROC
	
	
	PROCEDURE DriverSickDaysReport
		
		* for carlina
		CLOSE DATABASES ALL
	
		USE F:\UTIL\TIMESTAR\DATA\TIMEDATA AGAIN IN 0 ALIAS TIMEDATA
				
		* detail
		SELECT NAME, WORKDATE, PAY_TYPE, TOTHOURS FROM TIMEDATA ;
		INTO CURSOR CURDAYS ;
		WHERE (DIVISION = '55') AND (DEPT = '0650') ;
		AND PAY_TYPE IN ('SICK') ;
		AND TOTHOURS > 0.0 ;
		ORDER BY NAME, WORKDATE, PAY_TYPE
		
		SELECT CURDAYS 
		COPY TO C:\A\CADRIVERSICKDAYS_2014_DETAIL.XLS XL5
		
		* summary
		SELECT NAME, SUM(TOTHOURS) AS SICKHOURS ;
		FROM CURDAYS ;
		INTO CURSOR CURDAYSSUM ;
		GROUP BY NAME ;
		ORDER BY NAME
		
		SELECT CURDAYSSUM 
		COPY TO C:\A\CADRIVERSICKDAYS_2014_SUMMARY.XLS XL5
		
	
		RETURN
	ENDPROC


	
	
	PROCEDURE ListOfDrivers
		
		* for carlina
		CLOSE DATABASES ALL
	
		USE F:\UTIL\TIMESTAR\DATA\EEINFO.DBF AGAIN IN 0 ALIAS EEINFO
		
		SELECT DEPTDESC, NAME, DIVISION, DEPT, STATUS FROM EEINFO INTO CURSOR CURDRIVERS ;
		WHERE DEPT IN ('0650','0655','0660') ;
		AND (STATUS <> 'T') ORDER BY DEPTDESC, NAME
		
		SELECT CURDRIVERS
		*BROW
		COPY TO C:\A\TIMESTAR_LIST_OF_DRIVERS.XLS XL5
		
	
		RETURN
	ENDPROC

	
	
	PROCEDURE DriverWeekendDaysWorkedReport
		
		* for carlina
		CLOSE DATABASES ALL
	
		*USE F:\UTIL\TIMESTAR\DATA\EEINFO.DBF AGAIN IN 0 ALIAS EEINFO
		USE F:\UTIL\TIMESTAR\DATA\TIMEDATA AGAIN IN 0 ALIAS TIMEDATA
				
		* detail
		SELECT NAME, WORKDATE, PADR(CDOW(WORKDATE),10,' ') AS DOW, PAY_TYPE, TOTHOURS ;
		FROM TIMEDATA ;
		INTO CURSOR CURDAYS ;
		WHERE (DIVISION = '55') AND (DEPT = '0650') ;
		AND PAY_TYPE IN ('GUARANTEE','OVERTIME','REGULAR') ;
		AND TOTHOURS > 0.0 ;
		AND DOW(WORKDATE,1) IN (1,7) ;
		ORDER BY NAME, WORKDATE, PAY_TYPE
		
		SELECT CURDAYS 
		COPY TO C:\A\WEEKEND_DAYSWORKED_DETAIL.XLS XL5
		
		* summary
		SELECT NAME, DOW, COUNT(DISTINCT WORKDATE) AS DAYSWORKED ;
		FROM CURDAYS ;
		INTO CURSOR CURDAYSSUM ;
		GROUP BY NAME, DOW ;
		ORDER BY NAME, DOW
		
		SELECT CURDAYSSUM 
		COPY TO C:\A\WEEKEND_DAYSWORKED_SUMMARY.XLS XL5
		
	
		RETURN
	ENDPROC


	PROCEDURE CheckCADriverHoursByWeek
	
		LOCAL ldStartDate, ldEndDate	
		
		* for carlina
		CLOSE DATABASES ALL
		
		CREATE CURSOR CURCHECKDRIVERS (DRIVER C(30), FROMDATE D, TODATE D, TOTHOURS N(5,2))
	
		USE F:\UTIL\TIMESTAR\DATA\EEINFO.DBF AGAIN IN 0 ALIAS EEINFO
	
		USE F:\UTIL\TIMESTAR\DATA\TIMEDATA AGAIN IN 0 ALIAS TIMEDATA
		
			
		SELECT EEINFO
		SCAN FOR (DIVISION = '55') AND (DEPT = '0650') AND (STATUS <> 'T')
		
			ldStartDate = {^2014-12-28}
			
			DO WHILE ldStartDate < {^2015-02-15}
			
				ldStartDate = ldStartDate + 7
				
				ldEndDate = ldStartDate + 6	
				
				IF USED('CURHOURS') THEN
					USE IN CURHOURS
				ENDIF
			
				SELECT NAME, SUM(TOTHOURS) AS TOTHOURS ;
				FROM TIMEDATA ;
				INTO CURSOR CURHOURS ;
				WHERE BETWEEN(WORKDATE,ldStartDate,ldEndDate) ;
				AND INT(VAL(FILE_NUM)) = EEINFO.INSPID ;
				GROUP BY 1
				
				IF CURHOURS.TOTHOURS < 40 THEN
				
					m.DRIVER = EEINFO.NAME
					m.FROMDATE = ldStartDate
					m.TODATE = ldEndDate
					m.TOTHOURS = CURHOURS.TOTHOURS
					
					INSERT INTO CURCHECKDRIVERS FROM MEMVAR
				
				ENDIF && CURHOURS.TOTHOURS < 40				
				
			ENDDO		
				
		ENDSCAN
		
		SELECT CURCHECKDRIVERS
		COPY TO C:\A\CA_DRIVER_WEEKLY_HOURS.XLS XL5
	
	
		RETURN
	ENDPROC  &&  CheckCADriverHoursByWeek
	
	
	PROCEDURE DriverDaysWorkedReport
		
		* for carlina
		CLOSE DATABASES ALL
	
		USE F:\UTIL\TIMESTAR\DATA\EEINFO.DBF AGAIN IN 0 ALIAS EEINFO
	
		*USE F:\UTIL\TIMESTAR\DATA\EEINFO.DBF AGAIN IN 0 ALIAS EEINFO
		USE F:\UTIL\TIMESTAR\DATA\TIMEDATA AGAIN IN 0 ALIAS TIMEDATA
				
		* detail
		SELECT NAME, INT(VAL(FILE_NUM)) AS INSPID, WORKDATE, PAY_TYPE ;
		FROM TIMEDATA ;
		INTO CURSOR CURDAYS ;
		WHERE (DIVISION = '55') AND (DEPT = '0650') ;
		AND PAY_TYPE IN ('GUARANTEE','OVERTIME','REGULAR') ;
		AND TOTHOURS > 0.0 ;
		AND WORKDATE >= {^2015-01-09} ;
		ORDER BY NAME, WORKDATE, PAY_TYPE
		
		SELECT CURDAYS 
		COPY TO C:\A\DAYSWORKED_DETAIL.XLS XL5
		
		* ALSO SEE WHO DID NOT WORK...
		SELECT NAME FROM EEINFO ;
		WHERE (DIVISION = '55') AND (DEPT = '0650') ;
		AND STATUS <> 'T' ;
		AND INSPID NOT IN (SELECT INSPID FROM CURDAYS) ;
		INTO CURSOR CURDIDNOTWORK ;
		ORDER BY NAME
		
		SELECT CURDIDNOTWORK
		COPY TO C:\A\DID_NOT_WORK.XLS XL5
		

		
		* summary
		SELECT NAME, COUNT(DISTINCT WORKDATE) AS DAYSWORKED ;
		FROM CURDAYS ;
		INTO CURSOR CURDAYSSUM ;
		GROUP BY NAME ;
		ORDER BY NAME
		
		SELECT CURDAYSSUM 
		COPY TO C:\A\DAYSWORKED_SUMMARY.XLS XL5
		
	
		RETURN
	ENDPROC
	
	
	PROCEDURE CADRIVER_OvertimeSummary
		* total ot hours for a date range for Maria Diaz
		
		LOCAL ldEndDate, ldStartDate, lcFileName 

		ldStartDate = {^2016-08-01}
		ldEndDate = {^2016-08-31}
		
		SELECT NAME, FILE_NUM, SUM(OTHOURS) AS OTHOURS ;
		FROM F:\UTIL\TIMESTAR\DATA\TIMEDATA ;
		INTO CURSOR CUROT ;
		WHERE BETWEEN(workdate,ldStartDate,ldEndDate) ;
		AND (division = '55') AND (dept = '0650') AND (othours > 0) ;
		GROUP BY 1,2 ;
		ORDER BY 1,2
		
		lcFileName  = 'C:\A\CADRIVER_OT_' + DTOS(ldStartDate) + '-' + + DTOS(ldEndDate) + '.XLS'
		
		SELECT CUROT
		COPY TO (lcFileName) xl5
		
		RETURN
	ENDPROC	
	
	
	PROCEDURE NJDRIVER_OvertimeSummary
		* total ot hours for a date range for Maria Diaz
		
		LOCAL ldEndDate, ldStartDate, lcFileName 

		ldStartDate = {^2016-08-01}
		ldEndDate = {^2016-08-31}
		
		SELECT NAME, FILE_NUM, SUM(OTHOURS) AS OTHOURS ;
		FROM F:\UTIL\TIMESTAR\DATA\TIMEDATA ;
		INTO CURSOR CUROT ;
		WHERE BETWEEN(workdate,ldStartDate,ldEndDate) ;
		AND (division = '03') AND (dept = '0650') AND (othours > 0) ;
		GROUP BY 1,2 ;
		ORDER BY 1,2
		
		lcFileName  = 'C:\A\NJDRIVER_OT_' + DTOS(ldStartDate) + '-' + + DTOS(ldEndDate) + '.XLS'
		
		SELECT CUROT
		COPY TO (lcFileName) xl5
		
		RETURN
	ENDPROC	
	
	
	PROCEDURE Div50_OvertimeReport
		* FOR JONI GOLDING
		
		LOCAL ldDate, ldEndDate, ldStartDate, lcFileName 
		
		ldDate = DATE()

		DO WHILE DOW(ldDate,1) <> 7
			ldDate = ldDate - 1
		ENDDO

		ldEndDate = ldDate
		ldStartDate = ldDate - 6

		
		SELECT NAME, FILE_NUM, WORKDATE, SUM(OTHOURS) AS OTHOURS ;
		FROM F:\UTIL\TIMESTAR\DATA\TIMEDATA ;
		INTO CURSOR CUROT ;
		WHERE BETWEEN(workdate,ldStartDate,ldEndDate) ;
		AND (division = '50') AND (othours > 0) ;
		GROUP BY 1,2,3 ;
		ORDER BY 1,2,3
		
		lcFileName  = 'C:\A\DIV50OT_' + DTOS(ldStartDate) + '-' + + DTOS(ldEndDate) + '.XLS'
		
		SELECT CUROT
		COPY TO (lcFileName) xl5
		
		RETURN
	ENDPROC	
	
	
	PROCEDURE CADriverBadgesReport
	
		* for Diana Landeros / Carlina	
		IF USED('curbadges') THEN
			USE IN curbadges
		ENDIF
		
		SELECT NAME, BADGENUM, HIREDATE, STATUS  ;
			FROM F:\UTIL\TIMESTAR\DATA\EEINFO ;
			INTO CURSOR curbadges ;
			WHERE DIVISION = '55' AND DEPT = '0650' ;
			ORDER BY NAME, BADGENUM
		
		SELECT curbadges
		COPY TO C:\A\CADRIVER_BADGE_NUMS.XLS XL5

		IF USED('curbadges') THEN
			USE IN curbadges
		ENDIF
	
		RETURN
	ENDPROC
	
	
	PROCEDURE SP2BirthdayList
	
		* for Carlina	
		IF USED('curbday') THEN
			USE IN curbday
		ENDIF
		
		SELECT BIRTHDAY, NAME  ;
			FROM F:\UTIL\TIMESTAR\DATA\EEINFO ;
			INTO CURSOR CURBDAY ;
			WHERE WORKSITE = 'SP2' ;
			AND STATUS <> 'T' ; 
			ORDER BY BIRTHDAY, NAME
		
		SELECT curbday
		COPY TO C:\A\SP2_BIRTHDAY_LIST.XLS XL5
	
		RETURN
	ENDPROC
	
	
	PROCEDURE SP2PhoneList
	
		* for Carlina	
		IF USED('curbday') THEN
			USE IN curbday
		ENDIF
		
		SELECT NAME, PHONE  ;
			FROM F:\UTIL\TIMESTAR\DATA\EEINFO ;
			INTO CURSOR CURBDAY ;
			WHERE WORKSITE = 'SP2' ;
			AND STATUS <> 'T' ; 
			ORDER BY NAME
		
		SELECT curbday
		COPY TO C:\A\SP2_PHONE_LIST.XLS XL5
	
		RETURN
	ENDPROC
	
	
	PROCEDURE DriverPaySummaryReport
	
		* for Carlina
	
		LOCAL ldStart, ldEnd
		
		ldStart = {^2014-01-01}
		ldEnd   = {^2014-12-31}
		
		IF USED('curpay') THEN
			USE IN curpay
		ENDIF
		
		SELECT NAME, PAY_TYPE, SUM(TOTHOURS) AS TOTHOURS ;
			FROM F:\UTIL\TIMESTAR\DATA\TIMEDATA ;
			INTO CURSOR CURPAY ;
			WHERE DIVISION = '55' AND DEPT = '0650' ;
			AND PAY_TYPE IN('SICK','PERSONAL','VACATION') ;
			AND BETWEEN(WORKDATE,ldStart,ldEnd) ; 
			GROUP BY NAME, PAY_TYPE ;
			ORDER BY NAME, PAY_TYPE
		
		SELECT curpay
		COPY TO C:\A\CADRIVER_HOURS_2014.XLS XL5
	
		RETURN
	ENDPROC


	PROCEDURE DriverPunchesReport
		WITH THIS
			* process month-to-date for local driver punches
			* get the data to Darren.
			LOCAL ldToday, lnCurrentMonth
			ldToday = .dToday
			lnCurrentMonth = MONTH(ldToday)
			
			IF USED('TIMEDATA') THEN
				USE IN TIMEDATA
			ENDIF
			
			IF USED('EMPLOYEE') THEN
				USE IN EMPLOYEE
			ENDIF
			
			IF USED('CURFINAL') THEN
				USE IN CURFINAL
			ENDIF
			
			IF USED('CURINS') THEN
				USE IN CURINS
			ENDIF
			
			IF USED('CUROUTS') THEN
				USE IN CUROUTS
			ENDIF
			
			*USE F:\UTIL\TIMESTAR\DATA\TIMEDATA IN 0 ALIAS TIMEDATA  && LIVE DATA
			
			USE F:\UTIL\TIMESTAR\TESTDATA\TTIMEDATA IN 0 ALIAS TIMEDATA  && TEST DATA
			
			USE F:\HR\HRDATA\EMPLOYEE IN 0 ALIAS EMPLOYEE
			
			* get in punches
			SELECT NAME, INT(VAL(FILE_NUM)) AS INSPID, WORKDATE, INPUNCHNM AS PUNCHTYPE, INPUNCHDT AS PUNCHDT ;
				FROM TIMEDATA ;
				INTO CURSOR CURINS ;
				WHERE DEPT = '0650' ;
				AND NOT EMPTY(INPUNCHDT) ;
				AND MONTH(WORKDATE) = lnCurrentMonth ;
				ORDER BY NAME, INPUNCHDT ;
				READWRITE
			
*!*		SELECT CURINS 
*!*		BROWSE

			
			* get out punches
			SELECT NAME, INT(VAL(FILE_NUM)) AS INSPID, WORKDATE, OUTPUNCHNM AS PUNCHTYPE, OUTPUNCHDT AS PUNCHDT ;
				FROM TIMEDATA ;
				INTO CURSOR CUROUTS ;
				WHERE DEPT = '0650' ;
				AND NOT EMPTY(OUTPUNCHDT) ;
				AND MONTH(WORKDATE) = lnCurrentMonth ;
				ORDER BY NAME, OUTPUNCHDT ;
				READWRITE
			
*!*		SELECT CUROUTS 
*!*		BROWSE
	
			* combine the cursors
			SELECT CURINS
			APPEND FROM DBF('CUROUTS')
			
			USE IN CUROUTS
			
			* SELECT INTO FINAL CURSOR, JOINING WITH EMPLOYEE TABLE TO GET SS#
			SELECT A.*, NVL(B.SS_NUM,' ') AS SS_NUM ;
				FROM CURINS A ;
				LEFT OUTER JOIN EMPLOYEE B ;
				ON B.INSPID = A.INSPID ;
				INTO CURSOR CURFINAL ;
				ORDER BY A.NAME, A.WORKDATE, A.PUNCHDT
			
			
	SELECT CURFINAL
	BROWSE


		ENDWITH
		RETURN
	ENDPROC  && DriverPunchesReport


	PROCEDURE EligibleFor401kReport
		WITH THIS
			* create "Eligible for Insurance" report for HR

			* Employees Hired 1/1/08 thru 1/31/08 would be eligible for insurance on 5/1/08  -- email should go out 3rd week in March
			* Employees Hired 2/1/08 thru 2/29/08 would be eligible for insurance on 6/1/08  -- email should go out 3rd week in April
			* etc.

			* The report needs need to be run/emailed the 3rd week of the 2ND month prior to when they are eligible
			
			**************************************************
			** schedule it to run on the 15th of each month.
			**************************************************
			
			
			LOCAL ldToday, ldEligible, lnTwoMonthsAgo, lnFiveMonthsAgo, lnHireMonth401k, lnHireYear401k, lcFiletoSaveAs
			LOCAL lnRow, lcRow, oWorkbook, oWorksheet1
			LOCAL lcSendToR, lcCCR, lcSubjectR, lcAttachR, lcBodyTextR

			ldToday = .dToday
			*ldToday = {^2014-11-15}  && to force particular date

			ldEligible = GOMONTH(( ldToday - DAY(ldToday) + 1), 2)
			.cSubject = "Employees Eligible for 401k on " + TRANSFORM(ldEligible)
			
			lcSubjectR = .cSubject

			* determine hire month/year we want to report on
			lnTwoMonthsAgo = GOMONTH(ldToday,-2)
			lnFiveMonthsAgo = GOMONTH(ldToday,-5)

			lnHireMonth401k = MONTH(lnFiveMonthsAgo)
			lnHireYear401k = YEAR(lnFiveMonthsAgo)

			IF USED('EEINFO') THEN
				USE IN EEINFO
			ENDIF
			USE F:\UTIL\TIMESTAR\DATA\EEINFO.DBF IN 0 ALIAS EEINFO

			IF USED('CUR401K') THEN
				USE IN CUR401K
			ENDIF

			IF USED('CUREEINFO') THEN
				USE IN CUREEINFO
			ENDIF

			SELECT ADP_COMP, DIVISION, DEPT, DEPTDESC, NAME, INSPID, HIREDATE, STREET, CITY, STATE, ZIP, BIRTHDAY, HOURLYRATE, ANNSALARY ;
				FROM EEINFO ;
				INTO CURSOR CUREEINFO ;
				WHERE STATUS <> 'T' ;
				ORDER BY HIREDATE
								
*!*	SELECT CUREEINFO
*!*	BROWSE
*!*	THROW

			SELECT * ;
				FROM CUREEINFO ;
				INTO CURSOR CUR401K ;
				WHERE MONTH(HIREDATE) = lnHireMonth401k ;
				AND YEAR(HIREDATE) = lnHireYear401k ;
				ORDER BY HIREDATE

			** setup file names

			lcSpreadsheetTemplate = "F:\UTIL\INSPERITY\TEMPLATES\ELIG_FOR_INS_TEMPLATE.XLS"

			lcFiletoSaveAs = "F:\UTIL\INSPERITY\REPORTS\401k Eligibility Report for " + STRTRAN(DTOC(ldToday),"/","-") + ".XLS"

			IF FILE(lcFiletoSaveAs) THEN
				DELETE FILE (lcFiletoSaveAs)
			ENDIF

			** output to Excel spreadsheet
			.TrackProgress('Creating spreadsheet: ' + lcFiletoSaveAs, LOGIT+SENDIT+NOWAITIT)

			oWorkbook = .oExcel.workbooks.OPEN(lcSpreadsheetTemplate)
			oWorkbook.SAVEAS(lcFiletoSaveAs)

			** 401k LIST IS WORKSHEET 1
			oWorksheet1 = oWorkbook.Worksheets[1]
			oWorksheet1.RANGE("A4","J100").ClearContents()
			lcSubject = "Employees Eligible for 401k on " + TRANSFORM(ldEligible)
			oWorksheet1.RANGE("A1").VALUE = lcSubject

			lnRow = 3

			SELECT CUR401K
			SCAN
				lnRow = lnRow + 1
				lcRow = ALLTRIM(STR(lnRow))

				oWorksheet1.RANGE("A"+lcRow).VALUE = CUR401K.NAME
				oWorksheet1.RANGE("B"+lcRow).VALUE = "'" + CUR401K.DIVISION
				oWorksheet1.RANGE("C"+lcRow).VALUE = CUR401K.STREET
				oWorksheet1.RANGE("D"+lcRow).VALUE = CUR401K.CITY
				oWorksheet1.RANGE("E"+lcRow).VALUE = CUR401K.STATE
				oWorksheet1.RANGE("F"+lcRow).VALUE = "'" + CUR401K.ZIP				
				oWorksheet1.RANGE("G"+lcRow).VALUE = CUR401K.BIRTHDAY
				oWorksheet1.RANGE("H"+lcRow).VALUE = CUR401K.HIREDATE
				oWorksheet1.RANGE("I"+lcRow).VALUE = CUR401K.DEPTDESC
				oWorksheet1.RANGE("J"+lcRow).VALUE = CUR401K.HOURLYRATE
			ENDSCAN

			oWorkbook.SAVE()
			.oExcel.QUIT()

*!*				IF FILE(lcFiletoSaveAs) THEN
*!*					* attach output file to email
*!*					.cAttach = lcFiletoSaveAs
*!*					.cBodyText = "See attached 401k Eligibility report." + ;
*!*						CRLF + CRLF + "(do not reply - this is an automated report)" + ;
*!*						CRLF + CRLF + "<report log follows>" + ;
*!*						CRLF + .cBodyText
*!*				ELSE
*!*					.TrackProgress('ERROR: unable to email spreadsheet: ' + lcFiletoSaveAs, LOGIT+SENDIT+NOWAITIT)
*!*				ENDIF			

			IF FILE(lcFileToSaveAs) THEN
				* attach output file to email
				lcAttachR = lcFileToSaveAs
				lcBodyTextR = "See attached 401k Eligibility report." + ;
					CRLF + CRLF + "(do not reply - this is an automated report)" + ;
					CRLF + CRLF + "<report log follows>" 
					
				lcSendToR = 'lauren.wojcik@tollgroup.com'
				lcCCR = 'mark.bennett@tollgroup.com'
			
				* send email...
				DO FORM dartmail2 WITH lcSendToR,.cFrom,lcSubjectR,lcCCR,lcAttachR,lcBodyTextR,"A"
				.TrackProgress('Sent CA Driver Daily Report email to ' + lcSendToR + '.',LOGIT+SENDIT)
					
			ELSE
				.TrackProgress('ERROR: unable to email spreadsheet: ' + lcFileToSaveAs, LOGIT+SENDIT+NOWAITIT)
			ENDIF


			*!*			IF .lTestMode THEN
			*!*				.cSendTo = 'mark.bennett@tollgroup.com'
			*!*				.cCC = ''
			*!*			ELSE
			*!*				.cSendTo = 'lauren.wojcik@Tollgroup.com'
			*!*				.cCC = 'mark.bennett@tollgroup.com'
			*!*			ENDIF

		ENDWITH
		RETURN
	ENDPROC  && EligibleFor401kReport


	PROCEDURE DriverTimeToQlikView
		LPARAMETERS tdBeginDate, tdEndDate
		.TrackProgress("Running DriverTimeToQlikView()....", LOGIT+SENDIT+NOWAITIT)
		WITH THIS
		
			LOCAL ldBeginDate, ldEndDate, lcFileName
			ldBeginDate = tdBeginDate
			ldEndDate = tdEndDate
			

			** add SS_NUM ?
			
			SELECT ADP_COMP, NAME, WORKDATE, INT(VAL(FILE_NUM)) AS FILE_NUM, (DIVISION + DEPT) AS HOMEDEPT,  ;
			SUM(OTHOURS) AS OTHOURS, SUM(REGHOURS) AS REGHOURS ;
			FROM F:\UTIL\TIMESTAR\DATA\TIMEDATA ;
			INTO CURSOR CURDRIVERS ;
			WHERE BETWEEN(WORKDATE, ldBeginDate, ldEndDate) ;
			AND INLIST(DIVISION,'03','55') AND (DEPT == '0650') AND ((OTHOURS + REGHOURS) > 0.00) ;
			GROUP BY 1,2,3,4,5 ;
			ORDER BY NAME, WORKDATE
			
			*SELECT CURDRIVERS
			*BROWSE
			
			*lcFileName = "S:\QLIKVIEW\MARKTEST\DRIVERSTUFF\DRIVER_TIME_" + DTOS(ldBeginDate) + "-" + DTOS(ldEndDate) + ".CSV"
					
			lcFileName = "S:\QLIKVIEW\MARKTEST\DRIVERSTUFF\DRIVER_TIME_YTD_" + ALLTRIM(STR(YEAR(ldBeginDate))) + ".CSV"		
			
			SELECT CURDRIVERS
			COPY TO (lcFileName) CSV

		ENDWITH
		RETURN
	ENDPROC  && DriverTimeToQlikView


	PROCEDURE DriverMovesToQlikView
		* DRIVERMOVESTOQLIKVIEW
		LPARAMETERS tdBeginDate, tdEndDate
		.TrackProgress("Running DriverMovesToQlikView()....", LOGIT+SENDIT+NOWAITIT)
		WITH THIS
		
			LOCAL ldBeginDate, ldEndDate, lcFileName
			ldBeginDate = tdBeginDate
			ldEndDate = tdEndDate
			
			*ldEndDate = GOMONTH(ldBeginDate,1) - 1	
			
*!*	* force different ranges
*!*	ldBeginDate = {^2016-01-01}
*!*	ldEndDate =   {^2016-10-31}
			
			*SET STEP ON 

*!*				B.DATEOUT, B.TIMEOUT, ;
	
			* 3/7/2018 MB - use sql day/daydet
			*!*	SELECT A.DIS_DRIVER AS NAME, A.ZDATE AS DATE, A.SS_NUM, ;
			*!*	IIF(B.DH OR INLIST(B.WOTYPE,"BOBTAIL","TRAILER MOVE","CONTAINER MOVE","CHASSIS"),.T.,.F.) AS DH, B.WOTYPE ;
			*!*	FROM F:\WO\WODATA\DAY A ;
			*!*	INNER JOIN ;
			*!*	F:\WO\WODATA\DAYDET B ;
			*!*	ON B.DAYID = A.DAYID ;
			*!*	INTO CURSOR CURMOVES ;
			*!*	WHERE BETWEEN(A.ZDATE,ldBeginDate,ldEndDate) ;
			*!*	AND (A.COMPANY = 'T') ;
			*!*	AND (NOT A.MILEBASED) ;
			*!*	ORDER BY 1, 2, 4
			
			xsqlexec("select * from day","day",,"wo")
			xsqlexec("select * from daydet","daydet",,"wo")

			SELECT A.DIS_DRIVER AS NAME, A.ZDATE AS DATE, A.SS_NUM, ;
			IIF(B.DH OR INLIST(B.WOTYPE,"BOBTAIL","TRAILER MOVE","CONTAINER MOVE","CHASSIS"),.T.,.F.) AS DH, B.WOTYPE ;
			FROM DAY A ;
			INNER JOIN ;
			DAYDET B ;
			ON B.DAYID = A.DAYID ;
			INTO CURSOR CURMOVES ;
			WHERE BETWEEN(A.ZDATE,ldBeginDate,ldEndDate) ;
			AND (A.COMPANY = 'T') ;
			AND (NOT A.MILEBASED) ;
			ORDER BY 1, 2, 4
			
			*SELECT CURMOVES
			*BROWSE
			
			*lcFileName = "S:\QLIKVIEW\MARKTEST\DRIVERSTUFF\DRIVER_MOVES_" + DTOS(ldBeginDate) + "-" + DTOS(ldEndDate) + ".CSV"		
			
			lcFileName = "S:\QLIKVIEW\MARKTEST\DRIVERSTUFF\DRIVER_MOVES_YTD_" + ALLTRIM(STR(YEAR(ldBeginDate))) + ".CSV"		
			
			SELECT CURMOVES
			COPY TO (lcFileName) CSV

		ENDWITH
		RETURN
	ENDPROC  && DriverMovesToQlikView


	PROCEDURE DriverInfoToQlikView
		.TrackProgress("Running DriverInfoToQlikView()....", LOGIT+SENDIT+NOWAITIT)
		WITH THIS
			* added field 'daynight' 10/19/2016
		
			LOCAL lcFileName
		
			* the purpose of this export is to allow QlikView to relate warehouse driver data, which uses SS#s to identify drivers,
			* with timestar driver data, which uses insperity inspid # (i.e. file_num). We enter both ss# and Insperity inspid into the foxPro hr table for drivers.
			*
			* This is why we filter by non-empty inspid below; old, inactive drivers will not have inspids.
			SELECT EMPLOYEE AS NAME, SS_NUM, INSPID AS FILE_NUM, (PADL(DIVISION,2,'0') + PADL(DEPT,4,'0')) AS HOMEDEPT, DAYNIGHT  ;
			FROM F:\HR\HRDATA\EMPLOYEE ;
			INTO CURSOR CURINFO ;
			WHERE (DEPT = 650) ;
			AND (NOT EMPTY(INSPID)) ;
			ORDER BY EMPLOYEE
			
			SELECT CURINFO
			*BROWSE
			
			lcFileName = "S:\QLIKVIEW\MARKTEST\DRIVERSTUFF\DRIVER_INFO.CSV"		
			
			SELECT CURINFO
			COPY TO (lcFileName) CSV

		ENDWITH
		RETURN
	ENDPROC  && DriverInfoToQlikView



*!*		PROCEDURE DriverMovesForEdTuso
*!*			WITH THIS
*!*			
*!*				LOCAL ldBeginDate, ldEndDate, lcFileName
*!*				
*!*				ldBeginDate = {^2016-01-01}
*!*				ldEndDate =   {^2016-09-30}
*!*				
*!*				*SET STEP ON 
*!*		
*!*				SELECT A.DIS_DRIVER AS NAME, A.ZDATE AS DATE, A.COMPANY, A.TRUCK, A.OFFICE, A.MILEBASED, ;
*!*				IIF(B.DH OR INLIST(B.WOTYPE,"BOBTAIL","TRAILER MOVE","CONTAINER MOVE","CHASSIS"),.T.,.F.) AS DH, ;
*!*				B.LEG, B.STARTLOC, B.ENDLOC, B.PULOC, B.RETLOC, B.WOTYPE, B.WO_NUM, B.ACCOUNTID, B.CONTAINER, B.ACTION ;
*!*				FROM F:\WO\WODATA\DAY A ;
*!*				INNER JOIN ;
*!*				F:\WO\WODATA\DAYDET B ;
*!*				ON B.DAYID = A.DAYID ;
*!*				INTO CURSOR CURMOVES ;
*!*				WHERE BETWEEN(A.ZDATE,ldBeginDate,ldEndDate) ;
*!*				ORDER BY 1, 2

*!*	*!*				AND (A.COMPANY = 'T') ;
*!*	*!*				AND (NOT A.MILEBASED) ;
*!*				
*!*				*SELECT CURMOVES
*!*				*BROWSE
*!*				
*!*			
*!*				lcFileName = "C:\A\DRIVER_MOVES_" + DTOS(ldBeginDate) + "-" + DTOS(ldEndDate) + ".CSV"		
*!*				
*!*				SELECT CURMOVES
*!*				COPY TO (lcFileName) CSV

*!*			ENDWITH
*!*			RETURN
*!*		ENDPROC  && 


	PROCEDURE GetAccrualsLiability
		LPARAMETERS tcACCRcode
		WITH THIS

			* Data comes from TimeStar report: Data Export-Accruals.
			* BEFORE PROCEEDING: 
			*    run TimeStar report 'Data Export - Accruals' in Excel format ytd at least as far as the As Of date (example 01/01/2016 thru 06/30/2016 for 6/30 as of date); 
			*    Save it as F:\UTIL\TIMESTAR\REPORTS\ACCRUALSDATA\accrual_export(1).xls
			*    then DO m:\dev\prg\insperitydataimport.prg WITH "ACC" to load the accruals data.

			LOCAL ldToday, ldJan1st, lnUntaken, lnLiability, lcOutputFile1, lcOutputFile2, lcOutputFile3, lcACCRcode

			ldToday = DATE()
			*ldToday = {^2017-03-31}  && the "as of" date

			ldJan1st = {^2017-01-01}

			lcACCRcode = UPPER(ALLTRIM(tcACCRcode))

			* OPEN THE ACCRUALS INFO TABLE
			IF USED('EEINFO') THEN
				USE IN EEINFO
			ENDIF
			IF USED('ACCRDATA') THEN
				USE IN ACCRDATA
			ENDIF
			IF USED('CURLIABILITY1') THEN
				USE IN CURLIABILITY1
			ENDIF
			IF USED('CURLIABILITY2') THEN
				USE IN CURLIABILITY2
			ENDIF

			USE F:\UTIL\TIMESTAR\DATA\ACCRDATA IN 0 ALIAS ACCRDATA SHARED

			USE F:\UTIL\TIMESTAR\DATA\EEINFO IN 0 ALIAS EEINFO SHARED

			* transcode	transname
			* AC		Accrual
			* AJ		Accrual Adjustment
			* CA		Carryover
			* CJ		Carryover Adjustment
			* EX		Expired
			* MA		Manual Accrual
			* MC		Manual Carryover
			* MT		Manual Taken
			* MJ		Manual Adjustment
			* TA		Taken
			
			
			* Added support for MJ trans below.
			SELECT ;
				INSPID, ;
				SUM(IIF(INLIST(TRANSCODE,'AC','CA','MA'),HOURS,0)) AS ACCRUED, ;
				SUM(IIF(INLIST(TRANSCODE,'AJ','MT','TA'),HOURS,0)) AS TAKEN, ;
				SUM(IIF(INLIST(TRANSCODE,'MJ'),HOURS,0)) AS ADJUSTS ;
				FROM ACCRDATA ;
				INTO CURSOR CURLIABILITY1 ;
				WHERE BETWEEN(EFFDATE, ldJan1st, ldToday) ;
				AND ACCRCODE = lcACCRcode ;
				GROUP BY INSPID ;
				ORDER BY INSPID

			*!*				SELECT CURLIABILITY1
			*!*				BROWSE

			SELECT lcACCRcode AS ACCRUAL, B.WORKSITE, B.ADP_COMP, A.INSPID, B.HIREDATE, B.PTODATE, B.NAME, B.DIVISION, A.ACCRUED AS ACCRUED_HRS, A.TAKEN AS TAKEN_HRS, ;
				A.ADJUSTS AS ADJ_HRS, ;
				0000.00 AS BALANCE_HRS, B.HOURLYRATE, 0000000.00 AS LIABILITY ;
				FROM CURLIABILITY1 A ;
				LEFT OUTER JOIN EEINFO B ;
				ON B.INSPID = A.INSPID ;
				INTO CURSOR CURLIABILITY2 ;
				WHERE B.STATUS <> 'T' ;
				ORDER BY B.NAME ;
				READWRITE

*!*					AND B.ADP_COMP <> 'E87' ;

			SELECT CURLIABILITY2
			SCAN
				*lnUntaken = CURLIABILITY2.ACCRUED_HRS + CURLIABILITY2.TAKEN_HRS && Adding here because Takens are negative in TimeStar, not positive
				lnUntaken = CURLIABILITY2.ACCRUED_HRS + CURLIABILITY2.TAKEN_HRS + CURLIABILITY2.ADJ_HRS && Adding Taken here because Takens are negative in TimeStar, not positive; adjustments could be either sign
				lnLiability = lnUntaken * CURLIABILITY2.HOURLYRATE
				REPLACE CURLIABILITY2.BALANCE_HRS WITH lnUntaken, CURLIABILITY2.LIABILITY WITH lnLiability IN CURLIABILITY2
			ENDSCAN

			lcOutputFile1 = 'F:\UTIL\TIMESTAR\REPORTS\HOURLY_' + lcACCRcode +'_LIABILITY_THRU_' + DTOS(ldToday) + '.XLS'
			SELECT CURLIABILITY2
			COPY TO (lcOutputFile1) XL5 FOR ADP_COMP = 'E87'

			lcOutputFile2 = 'F:\UTIL\TIMESTAR\REPORTS\SALARIED_' + lcACCRcode +'_LIABILITY_THRU_' + DTOS(ldToday) + '.XLS'
			SELECT CURLIABILITY2
			COPY TO (lcOutputFile2) XL5 FOR ADP_COMP <> 'E87'	
			
			.cAttach = lcOutputFile1 + "," + lcOutputFile2

			lcOutputFile3 = 'F:\UTIL\TIMESTAR\REPORTS\HOURLY_AND_SALARIED_' + lcACCRcode +'_LIABILITY_THRU_' + DTOS(ldToday) + '.XLS'
			SELECT CURLIABILITY2
			COPY TO (lcOutputFile3) XL5 
			
*!*				.cAttach = lcOutputFile1 
			.cAttach = lcOutputFile1 + "," + lcOutputFile2 + "," + lcOutputFile3

			IF USED('EEINFO') THEN
				USE IN EEINFO
			ENDIF
			IF USED('ACCRDATA') THEN
				USE IN ACCRDATA
			ENDIF
			IF USED('CURLIABILITY1') THEN
				USE IN CURLIABILITY1
			ENDIF
			IF USED('CURLIABILITY2') THEN
				USE IN CURLIABILITY2
			ENDIF

		ENDWITH
		RETURN
	ENDPROC  && GetAccrualsLiability


	FUNCTION ExecSQL
		LPARAMETERS tcSQL, tcCursorName, tlNoDataReturnedIsOkay
		LOCAL llRetval, lnResult
		WITH THIS
			* close target cursor if it's open
			IF USED(tcCursorName)
				USE IN (tcCursorName)
			ENDIF
			WAIT WINDOW "Fetching remote data; please wait..." NOWAIT
			lnResult = SQLEXEC(.nSQLHandle, tcSQL, tcCursorName)
			llRetval = ( lnResult > 0 )
			IF llRetval THEN
				* see if any data came back
				IF NOT tlNoDataReturnedIsOkay THEN
					IF (NOT USED(tcCursorName)) OR EOF(tcCursorName) THEN
						llRetval = .F.
						.TrackProgress('No data was returned by this query: ' +CRLF + tcSQL, LOGIT+SENDIT)
						.cSendTo = 'Mark Bennett <mark.bennett@tollgroup.com>'
					ENDIF
				ENDIF
			ELSE
				.TrackProgress("Result Code [" + TRANSFORM(lnResult) + "] returned by this query: " + tcSQL, LOGIT+SENDIT)
				.cSendTo = 'Mark Bennett <mark.bennett@tollgroup.com>'
			ENDIF
			WAIT CLEAR
			RETURN llRetval
		ENDWITH
	ENDFUNC


	PROCEDURE TrackProgress
		* do any combination of Wait Window, writing to logfile, and adding to body of email,
		* based on nFlags parameter.
		LPARAMETERS tcExpression, tnFlags
		WITH THIS
			IF BITAND(tnFlags,LOGIT) = LOGIT THEN
				IF .lLoggingIsOn THEN
					?
					? .cProcessName + "  " + TTOC(DATETIME()) + ": " + tcExpression
				ENDIF
			ENDIF
			IF .lWaitWindowIsOn THEN
				IF BITAND(tnFlags,NOWAITIT) = NOWAITIT THEN
					WAIT WINDOW tcExpression NOWAIT
				ENDIF
				IF BITAND(tnFlags,WAITIT) = WAITIT THEN
					WAIT WINDOW tcExpression TIMEOUT .nWaitWindowTimeout
				ENDIF
			ENDIF
			IF BITAND(tnFlags,SENDIT) = SENDIT THEN
				IF .lSendInternalEmailIsOn THEN
					.cBodyText = .cBodyText + tcExpression + CRLF + CRLF
				ENDIF
			ENDIF
		ENDWITH
	ENDPROC  &&  TrackProgress


	FUNCTION GetSQLStartDateTimeString
		* returns a timestamp string for use in SQL queries
		LPARAMETERS tdDate
		LOCAL lcSetDate, lcSQLDateTimeString
		lcSetDate = SET('DATE')
		SET DATE YMD
		lcSQLDateTimeString   = "{ts '" + TRANSFORM(YEAR(tdDate)) + "-" + PADL(MONTH(tdDate),2,"0") + "-" + PADL(DAY(tdDate),2,"0") + " 00:00:00.0'}"
		SET DATE &lcSetDate.
		RETURN lcSQLDateTimeString
	ENDFUNC


	FUNCTION GetSQLEndDateTimeString
		* returns a timestamp string for use in SQL queries
		LPARAMETERS tdDate
		LOCAL lcSetDate, lcSQLDateTimeString
		lcSetDate = SET('DATE')
		SET DATE YMD
		lcSQLDateTimeString = "{ts '" + TRANSFORM(YEAR(tdDate)) + "-" + PADL(MONTH(tdDate),2,"0") + "-" + PADL(DAY(tdDate),2,"0") + " 23:59:59.9'}"
		SET DATE &lcSetDate.
		RETURN lcSQLDateTimeString
	ENDFUNC


	PROCEDURE SortArrayByDateTime
		* expects taInArray[] to be the result of an ADIR()
		LPARAMETERS taInArray, taOutArray
		EXTERNAL ARRAY taInArray, taOutArray
		LOCAL lnRows, lnCols, i, j
		* extending taInArray[] to process the files in date/time order
		lnRows = ALEN(taInArray,1)
		lnCols = ALEN(taInArray,2)
		* create taOutArray with one more column
		DIMENSION taOutArray[lnRows,lnCols + 1]
		* and fill it with taInArray columns AND (filedate + filetime) in new column
		* ( ACOPY() doesn't preserve elements properly because of different # of columns )
		FOR i = 1 TO lnRows
			FOR j = 1 TO lnCols
				taOutArray[i,j] = taInArray[i,j]
			ENDFOR
			taOutArray[i,lnCols + 1] = DTOS(taInArray[i,3]) + "_" + taInArray[i,4]
		ENDFOR
		* sort taOutArray on date/time column
		=ASORT(taOutArray,6)
	ENDPROC


ENDDEFINE


*!*	* useful queries


*!*	*** for Abigail - laid off dEPT 655 driver's VAC taken from 11/1/2016 thru 12/9/2016
*!*	* SUMMARY
*!*	SELECT ;
*!*	'2016-11-01 THRU 2016-12-09' AS DATERANGE, ;
*!*	NAME, ;
*!*	SUM(CODEHOURS) AS VACHOURS ;
*!*	FROM f:\util\timestar\data\timedata.dbf ;
*!*	INTO CURSOR CURDIV655 ;
*!*	WHERE BETWEEN(WORKDATE,{^2016-11-01},{^2016-12-09}) ;
*!*	AND (DEPT = '0655') ;
*!*	AND (PAY_TYPE = 'VACATION') ;
*!*	GROUP BY NAME ;
*!*	ORDER BY NAME

*!*	SELECT CURDIV655
*!*	COPY TO C:\A\DEPT0655_DRIVER_INFO_SUMMARY.XLS XL5

*!*	* DETAIL
*!*	SELECT ;
*!*	'2016-11-01 THRU 2016-12-09' AS DATERANGE, ;
*!*	NAME, ;
*!*	WORKDATE, ;
*!*	CODEHOURS AS VACHOURS ;
*!*	FROM f:\util\timestar\data\timedata.dbf ;
*!*	INTO CURSOR CURDIV655 ;
*!*	WHERE BETWEEN(WORKDATE,{^2016-11-01},{^2016-12-09}) ;
*!*	AND (DEPT = '0655') ;
*!*	AND (PAY_TYPE = 'VACATION') ;
*!*	ORDER BY NAME

*!*	SELECT CURDIV655
*!*	COPY TO C:\A\DEPT0655_DRIVER_INFO_DETAIL.XLS XL5

*AND ALLTRIM(FILE_NUM) IN (SELECT ALLTRIM(STR(INSPID,10,0)) AS FILE_NUM FROM f:\util\timestar\data\eeinfo.dbf WHERE TERMDATE > {^2016-11-01}) ;


*!*	* for Darren for Mike DiV
*!*	SELECT name, workdate, division, dept, file_num, codehours, reghours, othours, dthours ;
*!*	FROM f:\util\timestar\data\timedata.dbf WHERE workdate < {^2014-02-01} AND division = '03' AND dept = '0650' ORDER BY name, workdate



*!*	* average ca driver hours per week jan 1-18 for carlina & rich pacheco
*!*	**** don't forget to load 12/29-31 time data !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
*!*	CLOSE DATABASES ALL
*!*	USE F:\UTIL\TIMESTAR\DATA\TIMEDATA IN 0 ALIAS TIMEDATA
*!*	USE F:\UTIL\TIMESTAR\DATA\EEINFO IN 0 ALIAS EEINFO

*!*	SELECT A.NAME, A.INSPID, ;
*!*	( SELECT SUM(REGHOURS) FROM TIMEDATA WHERE VAL(FILE_NUM) == A.INSPID AND WORKDATE >= {^2013-12-19} AND WORKDATE <= {^2014-01-04} ) AS REG0104, ;
*!*	( SELECT SUM(OTHOURS) FROM TIMEDATA WHERE VAL(FILE_NUM) == A.INSPID AND WORKDATE >= {^2013-12-19} AND WORKDATE <= {^2014-01-04} ) AS OT0104, ;
*!*	( SELECT SUM(CODEHOURS) FROM TIMEDATA WHERE VAL(FILE_NUM) == A.INSPID AND WORKDATE >= {^2013-12-19} AND WORKDATE <= {^2014-01-04} ) AS OTHER0104, ;
*!*	( SELECT SUM(REGHOURS) FROM TIMEDATA WHERE VAL(FILE_NUM) == A.INSPID AND WORKDATE >= {^2014-01-05} AND WORKDATE <= {^2014-01-11} ) AS REG0111, ;
*!*	( SELECT SUM(OTHOURS) FROM TIMEDATA WHERE VAL(FILE_NUM) == A.INSPID AND WORKDATE >= {^2014-01-05} AND WORKDATE <= {^2014-01-11} ) AS OT0111, ;
*!*	( SELECT SUM(CODEHOURS) FROM TIMEDATA WHERE VAL(FILE_NUM) == A.INSPID AND WORKDATE >= {^2014-01-05} AND WORKDATE <= {^2014-01-11} ) AS OTHER0111, ;
*!*	( SELECT SUM(REGHOURS) FROM TIMEDATA WHERE VAL(FILE_NUM) == A.INSPID AND WORKDATE >= {^2014-01-12} AND WORKDATE <= {^2014-01-18} ) AS REG0118, ;
*!*	( SELECT SUM(OTHOURS) FROM TIMEDATA WHERE VAL(FILE_NUM) == A.INSPID AND WORKDATE >= {^2014-01-12} AND WORKDATE <= {^2014-01-18} ) AS OT0118, ;
*!*	( SELECT SUM(CODEHOURS) FROM TIMEDATA WHERE VAL(FILE_NUM) == A.INSPID AND WORKDATE >= {^2014-01-12} AND WORKDATE <= {^2014-01-18} ) AS OTHER0118 ;
*!*	FROM EEINFO A ;
*!*	INTO CURSOR CURDRIVERS ;
*!*	WHERE A.DIVISION = '55' AND A.DEPT = '0650' ;
*!*	ORDER BY A.NAME

*!*	SELECT CURDRIVERS
*!*	COPY TO C:\A\CADRIVERJAN2014HOURS.XLS XL5



*!*	SELECT distinct supervisor from f:\util\timestar\data\eeinfo.dbf WHERE (status <> 'T') AND (RATETYPE <> 'S') ORDER BY SUPERVISOR
*!*	COPY TO C:\A\HOURY_TIMESTAR_SUPERVISORS.XLS XL5


*!*	SELECT ADP_COMP, NAME, WORKDATE, PAY_TYPE, TOTHOURS FROM f:\util\timestar\data\TIMEDATA WHERE PAY_TYPE IN ('VACATION','SICK') AND TOTHOURS = 0.00 ORDER BY NAME, WORKDATE
*!*	COPY TO C:\A\TIMESTAR_VAC_SICK_ZEROHOURS.XLS XL5


* sp2 terms for carlina
*!*	SELECT status, termdate, adp_comp, division, dept, name, inspid, worksite, hiredate, birthday FROM eeinfo WHERE worksite = 'SP2' AND STATUS = 'T' AND (TERMDATE >= {^2014-01-01} or EMPTY(termdate)) ORDER BY NAME
*!*	COPY TO c:\a\sp2terms_2014_insperity.xls xl5


* EMPLOYEE DID/DEPT INFO FOR ROB PETTI
* TRUCKING DIVS
*!*	SELECT adp_comp, WORKSITE, busunit, name, division, dept, deptdesc FROM eeinfo WHERE (status <> 'T') AND (BUSUNIT = 'SCS') AND WORKSITE IN ('CA4','NJ1','NJ2','SP2') AND DIVISION IN ('02','03','51','55') ORDER BY DIVISION, DEPT, NAME
* NON-TRUCKING DIVS
*!*	SELECT adp_comp, WORKSITE, busunit, name, division, dept, deptdesc FROM eeinfo WHERE (status <> 'T') AND (BUSUNIT = 'SCS') AND WORKSITE IN ('CA4','NJ1','NJ2','SP2')  AND DIVISION NOT IN ('02','03','51','55') ORDER BY DIVISION, DEPT, NAME

* ca driver worked hours for lucille
*SELECT NAME, WORKDATE,PAY_TYPE,TOTHOURS FROM timedata WHERE BETWEEN(workdate,{^2016-05-01},{^2016-05-07}) AND division = '55' AND dept = '0650' AND INLIST(pay_type,'REGULAR','OVERTIME','DOUBLE')

*SELECT NAME, SUM(TOTHOURS) as tothours FROM timedata WHERE BETWEEN(workdate,{^2016-05-01},{^2016-05-07}) AND division = '55' AND dept = '0650' AND INLIST(pay_type,'REGULAR','OVERTIME','DOUBLE') GROUP BY name

*!*	**************************************************************************************************
*!*	* INFO FOR ABIGAIL

*!*	CLOSE DATABASES ALL

*!*	USE F:\UTIL\TIMESTAR\DATA\TIMEDATA.DBF

*!*	* LOOKING FOR REG HOURS > 40
*!*	*SELECT NAME, SUM(REGHOURS) AS REGHOURS FROM TIMEDATA WHERE BETWEEN(WORKDATE,{^2017-01-29},{^2017-02-04}) AND ADP_COMP = 'E87' and pay_type ='REGULAR' GROUP BY 1 having SUM(REGHOURS) > 40
*!*	*SELECT NAME, SUM(REGHOURS) AS REGHOURS FROM TIMEDATA WHERE BETWEEN(WORKDATE,{^2017-01-22},{^2017-01-28}) AND ADP_COMP = 'E87' and pay_type ='REGULAR' GROUP BY 1 having SUM(REGHOURS) > 40

*!*	* Oscar Torres
*!*	SELECT NAME, WORKDATE, DIVISION, DEPT, CODEHOURS, OTHOURS, DTHOURS, REGHOURS, HOURLYRATE, WAGEAMT ;
*!*	FROM TIMEDATA ;
*!*	INTO CURSOR TEMP1 ;
*!*	WHERE FILE_NUM = '2309497' ;
*!*	AND INLIST(WORKDATE,{^2016-09-15},{^2016-09-16},{^2016-09-19},{^2016-09-20},{^2016-09-21},{^2016-10-01},{^2016-10-06},{^2016-10-15}) ;
*!*	ORDER BY WORKDATE

*!*	*!*	SELECT TEMP1
*!*	*!*	BROWSE

*!*	* Abad Garach
*!*	SELECT NAME, WORKDATE, DIVISION, DEPT, CODEHOURS, OTHOURS, DTHOURS, REGHOURS, HOURLYRATE, WAGEAMT ;
*!*	FROM TIMEDATA ;
*!*	INTO CURSOR TEMP2 ;
*!*	WHERE FILE_NUM = '2309455' ;
*!*	AND INLIST(WORKDATE,{^2016-09-22},{^2016-09-23},{^2016-09-26},{^2016-09-27},{^2016-09-28},{^2016-09-29},{^2016-09-30},{^2016-10-03},{^2016-10-04},{^2016-10-05},{^2016-10-07},{^2016-10-10},{^2016-10-11},{^2016-10-12},{^2016-10-13},{^2016-10-14}) ;
*!*	ORDER BY WORKDATE

*!*	*!*	SELECT TEMP2
*!*	*!*	BROWSE

*!*	* Rene Noel
*!*	SELECT NAME, WORKDATE, DIVISION, DEPT, CODEHOURS, OTHOURS, DTHOURS, REGHOURS, HOURLYRATE, WAGEAMT ;
*!*	FROM TIMEDATA ;
*!*	INTO CURSOR TEMP3 ;
*!*	WHERE FILE_NUM = '2309472' ;
*!*	AND INLIST(WORKDATE,{^2016-10-17},{^2016-10-18},{^2016-10-19},{^2016-10-20},{^2016-10-21},{^2016-10-24},{^2016-10-25},{^2016-10-26},{^2016-10-27},{^2016-10-28},{^2016-10-31}) ;
*!*	ORDER BY WORKDATE

*!*	*!*	SELECT TEMP3
*!*	*!*	BROWSE

*!*	* Miguel Rivera
*!*	SELECT NAME, WORKDATE, DIVISION, DEPT, CODEHOURS, OTHOURS, DTHOURS, REGHOURS, HOURLYRATE, WAGEAMT ;
*!*	FROM TIMEDATA ;
*!*	INTO CURSOR TEMP4 ;
*!*	WHERE FILE_NUM = '2316939' ;
*!*	AND INLIST(WORKDATE,{^2016-10-17},{^2016-10-18},{^2016-10-19},{^2016-10-20},{^2016-10-21},{^2016-10-24},{^2016-10-25},{^2016-10-26},{^2016-10-27},{^2016-10-28},{^2016-10-31}) ;
*!*	ORDER BY WORKDATE ;
*!*	READWRITE

*!*	SELECT TEMP4
*!*	APPEND FROM DBF('TEMP1')
*!*	APPEND FROM DBF('TEMP2')
*!*	APPEND FROM DBF('TEMP3')

*!*	SELECT TEMP4
*!*	COPY TO C:\A\HOURS_INFO.XLS XL5
*!*	**************************************************************************************************

*!*	************** bad pto dates
*!*	USE f:\util\timestar\data\eeinfo.dbf AGAIN IN 0 ALIAS Eeinfo
*!*	BROWSE FOR status <> 'T' AND dept <> '0650' AND MONTH(PTODATE) <> 1
*!*	COPY TO C:\A\BAD_PTO_DATES.XLS XL5 FOR status <> 'T' AND dept <> '0650' AND MONTH(PTODATE) <> 1



