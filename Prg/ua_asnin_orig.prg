Set Deleted On
Set Escape On
Set enginebehavior 70
Set Safety Off

Close Data All

xrunall=.T.

xaccountid=5687
xacctname="UNDER ARMOUR"

lcpath="f:\ftpusers\underarmour\asnin\"
lcarchivepath="f:\ftpusers\underarmour\asnin\archive\"

If !xrunall
  lnnum=1
*  cfilename = alltrim(getfile())
  cfilename=Alltrim(lcarchivepath+"manh_tpm_shipment0004192734.xml ")
Else
  lnnum = Adir(tarray,lcpath+"*.xml")
  If lnnum = 0
    Return
  Endif
Endif


Use F:\underarmour\Data\uadetail In 0
Select * From uadetail Where .F. Into Cursor uatemp Readwrite
Select uatemp
Scatter Memvar Memo Blank


Use F:\underarmour\Data\ctnucc In 0 Alias uaucc
Select * From uaucc Where .F. Into Cursor uaucctemp Readwrite
Select uaucctemp
Scatter Memvar Memo Blank

Use In uaucc

llclosetables = .F.
*!*  If !Used("shiphdr")
*!*    Use F:\underarmour\Data\shiphdr In 0
*!*    Use F:\underarmour\Data\cartons In 0
*!*    llclosetables = .T.
*!*  Endif

*!*  Use F:\underarmour\Data\ShipMfst In 0

If !Used("dellocs")
  Use F:\wo\wodata\dellocs In 0
Endif

Select 200
Create Cursor temp1 (field1 c(254))
Select temp1

xdateloaded=Dtot({})

For thisfile = 1  To lnnum
  Xfile = lcpath+tarray[thisfile,1] && +"."
  cfilename = Alltrim(Lower(tarray[thisfile,1]))
  ctns=0

**issues with dateloaded being same for multiple files - mvw 11/30/10
  Do While xdateloaded=Datetime()
    Wait Timeout 1
  Enddo
  xdateloaded=Datetime()

  lcStr = Filetostr(Xfile)

  lcStr = Strtran(lcStr,"><",">"+Chr(13)+"<")

  Strtofile(lcStr,"h:\fox\text.xml")

  cchar=Chr(13)+Chr(10)

  Append From "h:\fox\text.xml" Type Delimited With Character &cchar
  Replace All field1 With Alltrim(field1)
  Locate

  Set Date ymd

set step On 
  Skip 1 In temp1 
  Do CASE
    Case "UA_Order"$field1
      Doctype = "PO" 
    Case "UA_Delivery"$field1
      Doctype = "ASN" 
   Otherwise
     Messagebox("Invalid XML type",0,"UA XML Upload")
     return
  Endcase 

  If Doctype = "PO"
    parse_po()
  endif

set step On 


  GotoTag("<Header")

  GotoTag("<DocNumber")
  m.docnumber=getfield(field1,[DocNumber>],"<")

  GotoTag("<MasterBOL")    && get Hawb
  m.mbol=getfield(field1,[MasterBOL>],"<")
  m.brokref = m.mbol

  GotoTag("<SubBOL")    && get Hawb
  m.subbol=getfield(field1,[SubBOL>],"<")
  m.hawb = m.subbol

  GotoTag("<Carrier")    && get Hawb
  m.carrier=getfield(field1,[Carrier>],"<")

  GotoTag("<Trailer")    && get Hawb
  m.container=getfield(field1,[Trailer>],"<")

  GotoTag("<TotalNumOfCartons")    && get Hawb
  m.totcartons=getfield(field1,[TotalNumOfCartons>],"<")

  GotoTag("<ShippedFrom")    && get Hawb
  GotoTag("<ID")    && get Hawb
  m.shipfrom=getfield(field1,[ID>],"<")

  GotoTag("<Name")    && get Hawb
  m.shipfromname=getfield(field1,[Name>],"<")

  GotoTag("<ID")    && get Hawb
  m.shipfromid=getfield(field1,[ID>],"<")

  GotoTag([<Date DateQualifier="DeliveryDate"])    && get Hawb
  m.deldate=getfield(field1,[<Date DateQualifier="DeliveryDate">],"<")

  GotoTag("<MessageID")    && get Hawb
  m.messageid=getfield(field1,[MessageID>],"<")

  llDone = .F.
  Do While !llDone

    GotoTag("<Order")
    m.po_num=getfield(field1,[Order>],"<")

    GotoTag("<Type")
    m.doctype=getfield(field1,[Type>],"<")

    GotoTag("<CustomerPO")
    m.linenum=getfield(field1,[CustomerPO>],"<")
    m.hdrdata =""
    If m.doctype = "RESERVED"

      GotoTag("<CustomerPO")    && get Hawb
      m.custpo =getfield(field1,[CustomerPO>],"<")
      m.hdrdata = m.hdrdata+Chr(13)+"CUSTPO*"+m.custpo

      GotoTag("<ID")    && get Hawb
      m.soldtoid =getfield(field1,[ID>],"<")
      m.hdrdata = m.hdrdata+Chr(13)+"SOLDTOID*"+m.soldtoid

      GotoTag("<Name")    && get Hawb
      m.soldtoname =getfield(field1,[Name>],"<")
      m.hdrdata = m.hdrdata+Chr(13)+"SOLDTONAME*"+m.soldtoname

      GotoTag("<Street")    && get Hawb
      m.soldtoaddr1 =getfield(field1,[Street>],"<")
      m.hdrdata = m.hdrdata+Chr(13)+"SOLDTOADD1*"+m.soldtoaddr1

      GotoTag("<Street2")    && get Hawb
      m.soldtoaddr2 =getfield(field1,[Street2>],"<")
      m.hdrdata = m.hdrdata+Chr(13)+"SOLDTOADD2*"+m.soldtoaddr2

      GotoTag("<Street3")    && get Hawb
      m.soldtoaddr3 =getfield(field1,[Street3>],"<")
      m.hdrdata = m.hdrdata+Chr(13)+"SOLDTOADD3*"+m.soldtoaddr3

      GotoTag("<City")    && get Hawb
      m.soldtocity =getfield(field1,[City>],"<")
      m.hdrdata = m.hdrdata+Chr(13)+"SOLDTOCITY*"+m.soldtocity

      GotoTag("<State")    && get Hawb
      m.soldtostate =getfield(field1,[State>],"<")
      m.hdrdata = m.hdrdata+Chr(13)+"SOLDTOSTATE*"+m.soldtostate

      GotoTag("<PostalCode")    && get Hawb
      m.soldtozip =getfield(field1,[PostalCode>],"<")
      m.hdrdata = m.hdrdata+Chr(13)+"SOLDTOZIP*"+m.soldtozip

      GotoTag("<Email")    && get Hawb
      m.soldtoemail =getfield(field1,[Email>],"<")
      m.hdrdata = m.hdrdata+Chr(13)+"SOLDTOEMAIL*"+m.soldtoemail

      GotoTag("<ExtPartnerID")    && get Hawb
      m.extsoldtoid =getfield(field1,[ExtPartnerID>],"<")
      m.hdrdata = m.hdrdata+Chr(13)+"SOLDTOEXTID*"+m.extsoldtoid

      GotoTag("<ID")    && get Hawb
      m.shiptoid =getfield(field1,[ID>],"<")
      m.hdrdata = m.hdrdata+Chr(13)+"SHIPTOID*"+m.shiptoid

      GotoTag("<Name")    && get Hawb
      m.shiptoname =getfield(field1,[Name>],"<")
      m.hdrdata = m.hdrdata+Chr(13)+"SHIPTONAME*"+m.shiptoname

      GotoTag("<ExtPartnerID")    && get Hawb
      m.shiptoid =getfield(field1,[ExtPartnerID>],"<")
      m.hdrdata = m.hdrdata+Chr(13)+"SHIPTOID*"+m.shiptoid

      GotoTag("<ID")    && get Hawb
      m.markforid =getfield(field1,[ID>],"<")
      m.hdrdata = m.hdrdata+Chr(13)+"MARKFORID*"+m.markforid

      GotoTag("<Name")    && get Hawb
      m.markforname =getfield(field1,[Name>],"<")
      m.hdrdata = m.hdrdata+Chr(13)+"MARKFORNAME*"+m.markforname

      GotoTag("<Street")    && get Hawb
      m.markforadd1 =getfield(field1,[Street>],"<")
      m.hdrdata = m.hdrdata+Chr(13)+"MARKFORADD1*"+m.markforadd1

      GotoTag("<Street2")    && get Hawb
      m.markforadd2 =getfield(field1,[Street2>],"<")
      m.hdrdata = m.hdrdata+Chr(13)+"MARKFORADD2*"+m.markforadd2

      GotoTag("<Street3")    && get Hawb
      m.markforadd3 =getfield(field1,[Street3>],"<")
      m.hdrdata = m.hdrdata+Chr(13)+"MARKFORADD3*"+m.markforadd3

      GotoTag("<City")    && get Hawb
      m.markforcity =getfield(field1,[City>],"<")
      m.hdrdata = m.hdrdata+Chr(13)+"MARKFORCITY*"+m.markforcity

      GotoTag("<State")
      m.markforstate =getfield(field1,[State>],"<")
      m.hdrdata = m.hdrdata+Chr(13)+"MARKFORSTATE*"+m.markforstate

      GotoTag("<PostalCode")    && get Hawb
      m.markforzip =getfield(field1,[PostalCode>],"<")
      m.hdrdata = m.hdrdata+Chr(13)+"MARKFORZIP*"+m.markforzip

      GotoTag("<Email")    && get Hawb
      m.markforemail =getfield(field1,[Email>],"<")
      m.hdrdata = m.hdrdata+Chr(13)+"MARKFOREMAIL*"+m.markforemail

      GotoTag("<ExtPartnerID")    && get Hawb
      m.extmarkforid =getfield(field1,[ExtPartnerID>],"<")
      m.hdrdata = m.hdrdata+Chr(13)+"MARKFOREXTID*"+m.extmarkforid

    Else
      GotoTag("<IncoTerm1")
      m.fob=getfield(field1,[IncoTerm1>],"<")

      GotoTag("<ShippingCondition")
      m.ShipCond=getfield(field1,[ShippingCondition>],"<")
    Endif

    Skip 1 In temp1

    llOrderDetailsFound = .T.
    Do While llOrderDetailsFound

      GotoTag("<LineNum")    && get Hawb
      m.linenum=getfield(field1,[LineNum>],"<")

      GotoTag("<SKU")    && get Hawb
      m.style=getfield(field1,[SKU>],"<")

      GotoTag("<VariantArticle")    && get Hawb
      m.sku_var=getfield(field1,[VariantArticle>],"<")

      GotoTag("<UPC")    && get Hawb
      m.upc=getfield(field1,[UPC>],"<")

      GotoTag("<WMSizeCode")    && get Hawb
      m.sizecode=getfield(field1,[WMSizeCode>],"<")

      GotoTag([<Quantity UOM="EA"])    && get Hawb
      m.uomqty=getfield(field1,[<Quantity UOM="EA">],"<")

      GotoTag("<StockCategory")    && get Hawb
      m.stockcat=getfield(field1,[StockCategory>],"<")

      GotoTag("<CountryOfOrigin")    && get Hawb
      m.coo=getfield(field1,[CountryOfOrigin>],"<")

      GotoTag("<Quality")    && get Hawb
      m.quality=getfield(field1,[Quality>],"<")

      GotoTag("<Kit")    && get Hawb
      m.kit=getfield(field1,[Kit>],"<")

      GotoTag("<AccountGroup")    && get Hawb
      m.acctgrp=getfield(field1,[AccountGroup>],"<")

      GotoTag("<Channel")    && get Hawb
      m.channel=getfield(field1,[Channel>],"<")

*!*      GotoTag("<CustomerDept")    && get Hawb
*!*      lcdata14=getfield(field1,[CustomerDept>],"<")

      GotoTag("<SAPPurchOrderNum")    && get Hawb
      m.sappurchnum=getfield(field1,[SAPPurchOrderNum>],"<")

      GotoTag("<SAPPurchOrderType")    && get Hawb
      m.sappoptype=getfield(field1,[SAPPurchOrderType>],"<")

      Skip 2 In temp1
      If temp1.field1 = "<OrderDetails>"
        llOrderDetailsFound = .T.
      Else
        llOrderDetailsFound = .F.
        Skip -1 In temp1
      Endif
    Enddo

    llCartonFound = .T.
    m.pl_qty = 0
    m.plweight =0
    m.cbm = 0.0

    Do While llCartonFound

      GotoTag("<CartonID")    && get Hawb
      m.ucc=getfield(field1,[CartonID>],"<")
      m.pl_qty = m.pl_qty+1

      GotoTag([<Value type="GrossWeight"])    && get Hawb
      m.ctnwt=getfield(field1,[Value type="GrossWeight">],"<")
      m.plweight = m.plweight + Val(m.ctnwt)

      GotoTag([<Value type="Width"])    && get Hawb
      m.width=getfield(field1,[Value type="Width">],"<")

      GotoTag([<Value type="Height"])    && get Hawb
      m.height=getfield(field1,[Value type="Height">],"<")

      GotoTag([<Value type="Length"])    && get Hawb
      m.length=getfield(field1,[Value type="Length">],"<")
      m.cbm = m.cbm+(Val(m.width)*Val(m.height)*Val(Length))/1728

      llCartonDetails = .T.
      Do While llCartonDetails

        GotoTag("<SKU")    && get Hawb
        m.sku=getfield(field1,[SKU>],"<")

        GotoTag("<VariantArticle")    && get Hawb
        m.var_ctn=getfield(field1,[VariantArticle>],"<")

        GotoTag("<UPC")    && get Hawb
        m.ctnupc=getfield(field1,[UPC>],"<")

        GotoTag("<WMSizeCode")    && get Hawb
        m.ctnsizecode=getfield(field1,[WMSizeCode>],"<")

        GotoTag([<Quantity UOM="EA"])    && get Hawb
        m.totqty=getfield(field1,[<Quantity UOM="EA">],"<")
        m.pack =Alltrim(Transform(Int(Val(m.totqty))))
    
        GotoTag("<Order")    && get Hawb
        m.ponum=getfield(field1,[Order>],"<")
       
        GotoTag("<OrderLine")    && get Hawb
        m.ctn_linenum=getfield(field1,[OrderLine>],"<")
  
        Do getCtnRecord
       
        Skip 2 In temp1
        If temp1.field1 = "<CartonDetails>"
          llCartonDetails = .T.
        Else
          llCartonDetails = .F.
          Skip -1 In temp1
        Endif
      Enddo
      Skip 2 In temp1

      If temp1.field1 = "<Carton>"
        llCartonFound = .T.
      Else
        llCartonFound = .F.
        If temp1.field1 = "</Order>"
          Skip 1 In temp1
          If temp1.field1 = "<Order>"
            llOrderFound = .T.
            Do getRecord
          Else
            llOrderFound = .F.
            llDone = .T.
            Do getRecord
          Endif
        Else

        Endif
        Skip -1 In temp1
      Endif
    Enddo

    If llDone = .T.
      Exit
    Endif
  Enddo

llDoSql=.f.

If llDoSql

  gmasteroffice ="N"
  goffice = "N"
  m.mod="I"
  useca("ctnucc","wh")

  Select uaucctemp
  Scan 
    Select uaucctemp
    Scatter Memvar memo
    insertinto("ctnucc","wh",.T.)
  Endscan

  tu("ctnucc")

  Return
  Set Step On

  endif 

Endfor

Use In temp1

*Do ASNReceipt with "6004604",lcTPMID
*************************************************************************************************************
Procedure Parse_PO

  GotoTag("<Orders")

  GotoTag("<Order>")
  m.ponum=getfield(field1,[Order>],"<")

  GotoTag("<SoldTo")
  GotoTag("<ID")    && get Hawb
  m.soldtoID=getfield(field1,[ID>],"<")

  GotoTag("<ShipTo")
  GotoTag("<ID")    && get Hawb
  m.shiptoID=getfield(field1,[ID>],"<")
 

  GotoTag([<Date DateQualifier="PODate">])   
  m.subbol=getfield(field1,[Date DateQualifier="PODate">],"<")

Endproc 
*************************************************************************************************************
Procedure ASNReceipt
Parameters lcReceiptNum,lcTPM

Select shiphdr
Locate For tpmid = lcTPM
If Found()

Else
  Wait Window At 10,10 "Shipment data not on file for TPMID= "+lcTPM
Endif

Set Date YMD
lcTPMID= Strtran(Ttoc(Datetime()),"/","")
lcTPMID= Strtran(lcTPMID," ","")
lcTPMID= Strtran(lcTPMID,":","")

lcTStamp=Strtran(Ttoc(Datetime()),"/","-")
lcTStamp=Strtran(lcTStamp," ","T")

lcEventCode ="2094"

lcRCVStamp=Strtran(Ttoc(Datetime()-10000),"/","-")
lcRCVStamp=Strtran(lcRCVStamp," ","T")

* Read in the XML header template

lcHeader = Filetostr("f:\underarmour\rcpt_header.txt")

* Replace all the predefined data elements

lcHeader = Strtran(lcHeader,"TPMID",shiphdr.tpmid) &&lcTPMid
lcHeader = Strtran(lcHeader,"TSTAMP",lcTStamp)
lcHeader = Strtran(lcHeader,"EVENTCODE",lcEventCode)
lcHeader = Strtran(lcHeader,"SHIPMENTID",shiphdr.shipid)  &&lcShipID
lcHeader = Strtran(lcHeader,"MBOL","") &&lcMBOL
lcHeader = Strtran(lcHeader,"HBOL",shiphdr.BOL) &&lcBOL
lcHeader = Strtran(lcHeader,"PNUM","") && lcPro

lcHeader = Strtran(lcHeader,"CONTAINER",shiphdr.Container)  &&lcCtr

lcHeader = Strtran(lcHeader,"RECEIPTNUM",lcReceiptNum)
lcHeader = Strtran(lcHeader,"RCVSTAMP",lcRCVStamp)

* get the cartons related to this shipment
Select cartons

lcFilename = Strtran(lcTPMID,"-","")
lcFilename = Strtran(lcFilename,":","")
lcFilename = Strtran(lcFilename,"T","")
lcFilename = "c:\tempfox\"+"FMI_"+Alltrim(lcFilename)+".xml"

lnHandle2=Fcreate(lcFilename)

NumWrite = Fputs(lnHandle2,lcHeader)

Select cartons
Goto Top
Scan
  lcStr = [<LPN Id="]+Alltrim(cartons.ucc)+["  ReceiptIdRef="]+Alltrim(lcReceiptNum)+[">]
  NumWrite = Fputs(lnHandle2,lcStr)
  NumWrite = Fputs(lnHandle2,[</LPN>])
Endscan

* write out the XML footer elements

NumWrite = Fputs(lnHandle2,[</Destination>])
NumWrite = Fputs(lnHandle2,[</MANH_TPM_Shipment>])
NumWrite = Fputs(lnHandle2,[</Message>])
NumWrite = Fputs(lnHandle2,[</tXML>])
Fclose(lnHandle2)

Endproc

**************************************************************************
Procedure getfield
Parameters sdata,match,enddata

lnstart = At(match,sdata)
firstquote = lnstart+Len(match)
cc = Substr(sdata,firstquote,1)
If Len(Alltrim(sdata)) = Len(Alltrim(match))+1
  Return "NA"
Endif

lnsecond = firstquote+1
Do While Substr(sdata,lnsecond,1)!=enddata
  lnsecond = lnsecond+1
Enddo

Return Substr(sdata,firstquote,lnsecond-(firstquote))

Endproc

**************************************************************************
Procedure GotoTag
Parameters tagname,stoptag
xtagfilter=Iif(Empty(stoptag),"","and FIELD1!= stoptag")
Do While field1!= tagname &xtagfilter
  Skip 1 In temp1
Enddo
Endproc
**************************************************************************
Procedure FindTag
Parameters tagname,stoptag
Do While field1!= tagname
  Skip 1 In temp1
Enddo
Endproc
**************************************************************************
Procedure getRecord
  Select uatemp
  Append Blank
  m.filename = Xfile
  m.dateloaded=Date()
  m.accountid= 6666
  m.qty_type="CARTONS"
  m.loose = .F.
  m.type = "O"
  m.uploadtm=Datetime()
  m.acctname ="UNDER ARMOUR"
  Gather Memvar Memo
  Select temp1
Endproc
**************************************************************************
Procedure getCtnRecord
  Select uaucctemp
  Append Blank
  m.asnfile = Justfname(Xfile)
  m.accountid= 6666
  m.loaddt = Date()
  Gather Memvar Memo
  Select temp1
Endproc
**************************************************************************


