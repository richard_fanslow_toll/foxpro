*!* m:\dev\prg\samsung_bkdn.prg
clear

lcheckstyle = .t.
lprepack = .f.
lpick = .t.
lsolidpack = .f.
ljcp = .f.
units = .t.
ptctr = 0
m.isa_num = ""
m.color=""

select x856
count for trim(segment) = "ST" to nsegcnt
csegcnt = alltrim(str(nsegcnt))

wait window "Now in 940 Breakdown" timeout 1
wait "There are "+csegcnt+" P/Ts in this file" window timeout 1
select x856
locate

*!* Constants
m.acct_name = "SAMSUNG"
m.accountid = nacctnum
m.careof = " "
m.sf_addr1 = " "
m.sf_csz = " "
m.printed = .f.
m.print_by = ""
m.printdate = date()
*!* End constants

*!*	if !used("uploaddet")
*!*		if ltesting
*!*			use f:\ediwhse\test\uploaddet in 0 alias uploaddet
*!*		else
*!*			use f:\ediwhse\data\uploaddet in 0 shared alias uploaddet
*!*		endif
*!*	endif

select xpt
delete all
select xptdet
delete all
scatter memvar memo blank

store 0 to pt_total_eaches,pt_total_cartons,nctncount

select x856
set filter to
locate

m.specialship =""

wait window "NOW IN INITAL HEADER BREAKDOWN" timeout 1
wait window "NOW IN INITAL HEADER BREAKDOWN" nowait
do while !eof("x856")
	csegment = trim(x856.segment)
	ccode = trim(x856.f1)

	if trim(x856.segment) = "ISA"
		m.isa_num = alltrim(x856.f13)
		store m.isa_num to cisa_num
		select x856
		skip
		loop
	endif

	if trim(x856.segment) = "GS"
		select x856
		skip
		loop
	endif

	if trim(x856.segment) = "ST" and trim(x856.f1) = "940"
		select x856
		skip
		loop
	endif

	if trim(x856.segment) = "W05"  && new shipment
		m.specialship = ""
		nctncount = 0
		select xpt
		ndetcnt = 0
		append blank
		replace xpt.qty with 0 in xpt
		replace xpt.origqty with 0 in xpt
*		REPLACE xpt.goh WITH .T. IN xpt
		store 0 to pt_total_eaches
		store 0 to pt_total_cartons
		ptctr = ptctr +1
		wait window at 10,10 "Now processing PT# "+ alltrim(x856.f2)+" Number "+chr(13)+transform(ptctr)+" of "+csegcnt nowait

		replace xpt.ptid       with ptctr in xpt
		replace xpt.accountid  with m.accountid in xpt
		replace xpt.ptdate     with date() in xpt
		cship_ref = alltrim(x856.f2)
		m.ship_ref = alltrim(x856.f2)
		m.cnee_ref = alltrim(x856.f3)
		replace xpt.ship_ref   with m.ship_ref  in xpt && Pick Ticket
		replace xpt.cnee_ref   with m.cnee_ref in xpt
		m.pt = alltrim(x856.f2)
		if ltesting
			replace xpt.cacctnum   with "999" in xpt
		endif
		select x856
		skip
		loop
	endif

	if trim(x856.segment) = "N1" and trim(x856.f1) = "DV" && Division
		cdivision = alltrim(x856.f4)
		replace xpt.shipins with "DIVNAME*"+alltrim(x856.f2)+chr(13)+"DIVCODE*"+cdivision in xpt
		nacctnum  = icase(cdivision = "16",6651,cdivision = "8",6650,cdivision = "22",6652,6649)
		m.accountid = nacctnum
		replace xpt.accountid  with m.accountid in xpt next 1
	endif

	if trim(x856.segment) = "N1" and trim(x856.f1) = "ST" && Ship-to data
*!*			select uploaddet
*!*			locate
*!*			if !empty(m.isa_num) and !ltesting
*!*				insert into uploaddet (office,cust_name,acct_num,date,isa_num,startpt,runid) ;
*!*					values (coffice,upper(trim(x856.f2)),m.accountid,datetime(),m.isa_num,xpt.ship_ref,lnrunid)
*!*			endif

		select xpt
		m.st_name = trim(x856.f2)
		replace xpt.name with upper(m.st_name) in xpt
		replace xpt.consignee with upper(m.st_name) in xpt
		cstorenum = alltrim(x856.f4)

		replace xpt.shipins with iif(empty(xpt.shipins),"FILENAME*"+cfilename,xpt.shipins+chr(13)+"FILENAME*"+cfilename) in xpt
		replace xpt.shipins with xpt.shipins+chr(13)+"STORENUM*"+cstorenum in xpt
		replace xpt.storenum  with val(right(cstorenum,5)) in xpt
		replace xpt.dcnum with cstorenum in xpt

		skip 1 in x856
		if trim(x856.segment) = "N3"  && address info
			m.st_addr1 = upper(trim(x856.f1))
			m.st_addr2 = upper(trim(x856.f2))
			replace xpt.address  with m.st_addr1 in xpt
			replace xpt.address2 with m.st_addr2 in xpt
		endif
		skip 1 in x856
		if trim(x856.segment) = "N4"  && address info
			if !empty(trim(x856.f3))
				m.zipbar = trim(x856.f3)
			endif
			if !empty(trim(x856.f2))
				m.st_csz = upper(trim(x856.f1))+", "+upper(trim(x856.f2))+" "+trim(x856.f3)
			else
				m.st_csz = upper(trim(x856.f1))
			endif
			replace xpt.csz with m.st_csz in xpt
		endif
		select x856
		skip
		loop
	endif

	if trim(x856.segment) = "N1" and inlist(trim(x856.f1),"BY") && Ship-for data
		select xpt
		replace xpt.shipins with xpt.shipins+chr(13)+"BYNAME*"+alltrim(x856.f3)+chr(13)+"BYNUM*"+alltrim(x856.f4) in xpt
		replace xpt.shipins with xpt.shipins+chr(13)+"BYSHIPFOR*"+alltrim(x856.f2) in xpt
		skip 1 in x856
		if trim(x856.segment) = "N3"  && address info
			replace xpt.shipins with xpt.shipins+chr(13)+"BYADDR1*"+alltrim(x856.f1) in xpt
			replace xpt.shipins with xpt.shipins+chr(13)+"BYADDR2*"+alltrim(x856.f2) in xpt
		endif
		skip 1 in x856
		if trim(x856.segment) = "N4"  && address info
			if !empty(trim(x856.f3))
				replace xpt.shipins with xpt.shipins+chr(13)+"BYZIP*"+alltrim(x856.f3) in xpt
				m.zipbar = trim(x856.f3)
			endif
			if !empty(trim(x856.f2))
				replace xpt.shipins with xpt.shipins+chr(13)+"BYCSZ*"+alltrim(x856.f1)+", "++alltrim(x856.f2)+" "++alltrim(x856.f3) in xpt
			endif
			select x856
			skip
			loop
		endif
	endif

	if trim(x856.segment) = "N1" and inlist(trim(x856.f1),"Z7","MA") && Ship-for data
		select xpt
		replace xpt.shipfor with upper(alltrim(x856.f2)) in xpt
		replace xpt.sforstore with alltrim(x856.f4) in xpt
		skip 1 in x856
		if trim(x856.segment) = "N3"  && address info
			replace xpt.shipins with xpt.shipins+chr(13)+"STORENAME*"+alltrim(x856.f1) in xpt
			m.sforaddr1 = upper(trim(x856.f1))
			m.sforaddr2 = upper(trim(x856.f2))
			replace xpt.sforaddr1 with m.sforaddr1 in xpt
			replace xpt.sforaddr2 with m.sforaddr2 in xpt
		endif
		skip 1 in x856
		if trim(x856.segment) = "N4"  && address info
			if !empty(trim(x856.f3))
				m.zipbar = trim(x856.f3)
			endif
			if !empty(trim(x856.f2))
				m.sforcsz = upper(trim(x856.f1))+", "+upper(trim(x856.f2))+" "+trim(x856.f3)
			else
				m.sforcsz = upper(trim(x856.f1))
			endif
			replace xpt.sforcsz with m.sforcsz in xpt
			select x856
			skip
			loop
		endif
	endif

	if trim(x856.segment) = "N1" and trim(x856.f1)="SD"
		m.specialship = trim(x856.f4)
		replace xpt.shipins with xpt.shipins+chr(13)+"SHIPSPECIAL*"+trim(x856.f2) in xpt
		if m.specialship="84329"
&& this special# comes from the N1*SD segment.........
			replace xpt.batch_num with "NORDDROP" in xpt
		endif
		select x856
		skip
		loop
	endif


	if trim(x856.segment) = "N9" and trim(x856.f1) = "DP"  && customer dept and name
		m.dept = alltrim(x856.f2)
		replace xpt.dept with m.dept in xpt
		replace xpt.shipins with xpt.shipins+chr(13)+"FOB*"+alltrim(x856.f3) in xpt
		select x856
		skip
		loop
	endif

	if trim(x856.segment) = "N9" and trim(x856.f1) = "VR"  && Vendor #
		m.vendor_num = alltrim(x856.f2)
		replace xpt.vendor_num with m.vendor_num in xpt
		select x856
		skip
		loop
	endif

	if trim(x856.segment) = "N9" and trim(x856.f1) = "14"  && acct num, JCP supplier number
		m.cacctnum  = alltrim(x856.f2)
		replace xpt.cacctnum  with m.cacctnum in xpt
		select x856
		skip
		loop
	endif

	if trim(x856.segment) = "N9" and trim(x856.f1) = "12"  && UPS Acct# #
		m.upsacctnum = alltrim(x856.f2)
		replace xpt.upsacctnum with m.upsacctnum in xpt
		replace xpt.shipins with xpt.shipins+chr(13)+"UPSACCTNUM*"+m.upsacctnum in xpt
		select x856
		skip
		loop
	endif

	if trim(x856.segment) = "N9" and trim(x856.f1) = "BL"
		ccustbol = "CUSTBOL*"+upper(alltrim(x856.f2))
		replace xpt.shipins with iif(empty(xpt.shipins),ccustbol,xpt.shipins+chr(13)+ccustbol) in xpt
		select x856
		skip
		loop
	endif

	if trim(x856.segment) = "N9" and trim(x856.f1) = "CC"
		cccnum = "CCNUM*"+upper(alltrim(x856.f2))
		replace xpt.shipins with iif(empty(xpt.shipins),cccnum,xpt.shipins+chr(13)+cccnum) in xpt
		select x856
		skip
		loop
	endif

*!*		IF TRIM(x856.segment) = "G61" && Ship dept. eMail
*!*			cShipStr = "CONTACT*"+UPPER(ALLTRIM(x856.f2))
*!*			REPLACE xpt.shipins WITH IIF(EMPTY(xpt.shipins),cShipStr,xpt.shipins+CHR(13)+cShipStr)
*!*			IF ALLTRIM(x856.f3) = "EM"
*!*				REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"EMAIL*"+UPPER(ALLTRIM(x856.f4))
*!*			ENDIF
*!*			SELECT x856
*!*			SKIP
*!*			LOOP
*!*		ENDIF

	if trim(x856.segment) = "N9" and trim(x856.f1) = "SV"  && Ship Via Text
		m.ship_via= alltrim(x856.f2)
		replace xpt.ship_via with  m.ship_via in xpt
		select x856
		skip
		loop
	endif


	if trim(x856.segment) = "G62" and trim(x856.f1) == "37"  && start date
		lcdate = alltrim(x856.f2)
		lxdate = substr(lcdate,5,2)+"/"+substr(lcdate,7,2)+"/"+left(lcdate,4)
		lddate = ctod(lxdate)
		replace xpt.start with lddate in xpt
		select x856
		skip
		loop
	endif

	if trim(x856.segment) = "G62" and trim(x856.f1) == "38"  && cancel date
		lcdate = alltrim(x856.f2)
		lxdate = substr(lcdate,5,2)+"/"+substr(lcdate,7,2)+"/"+left(lcdate,4)
		lddate = ctod(lxdate)
		replace xpt.cancel with lddate in xpt
		select x856
		skip
		loop
	endif

*!*		IF TRIM(x856.segment) = "NTE" AND INLIST(TRIM(x856.f1),"OTH","DEL")  && delivery instructions
*!*			REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"INST*"+UPPER(ALLTRIM(x856.f2))
*!*			SELECT x856
*!*			SKIP
*!*			LOOP
*!*		ENDIF

	if trim(x856.segment) = "W66" && Ship info
		m.terms = alltrim(x856.f1)
		replace xpt.terms with m.terms in xpt
*!*  		m.ship_via = ALLTRIM(x856.f5)
*!*  		REPLACE xpt.ship_via WITH m.ship_via IN xpt
		m.scac = alltrim(x856.f10)
		replace xpt.scac with m.scac in xpt
	endif


	if trim(x856.segment) = "LX" && start of PT detail, stay here until this PT is complete
		wait "NOW IN DETAIL LOOP PROCESSING" window nowait
		select xptdet
		m.linenum = trim(x856.f1)
		calculate max(ptdetid) to m.ptdetid
		m.ptdetid = m.ptdetid + 1
*		APPEND BLANK
		m.printstuff = ""
		m.ptid = ptctr
		skip 1 in x856

		llstoredthestorenum = .f.

		do while alltrim(x856.segment) != "SE"
			csegment = trim(x856.segment)
			ccode = trim(x856.f1)

			if trim(x856.segment) = "LX"
				m.linenum = trim(x856.f1)
			endif

			if trim(x856.segment) = "W01" && Destination Quantity
				m.printstuff = ""
				loldstyle = .f.
				store "" to m.style,m.id,m.color,m.pack,m.upc,m.custsku
				lcase = iif(x856.f2 = "CA",.t.,.f.)
				lpick = .t.
				m.units = lpick
				if empty(trim(m.printstuff))
					m.printstuff = "UNITCODE*"+alltrim(x856.f2)
				else
					m.printstuff = m.printstuff+chr(13)+"UNITCODE*"+alltrim(x856.f2)
				endif
				m.pack = "1"  && change pack to always "1" PG 4/19/2016
				m.casepack = int(val(m.pack))
				m.totqty   = int(val(x856.f1))
				m.custsku  = upper(alltrim(x856.f16))
*		set step on
				do case
				case alltrim(x856.f4) = "VA"
					store upper(alltrim(x856.f5)) to m.style
					if alltrim(x856.f8) = "VN"       && added TMARG 7/28/16 as this is in the VA.  Also mapped it to the shipstyle field
						m.shipstyle= upper(alltrim(x856.f9))
					endif
				case alltrim(x856.f4) = "IN"
					loldstyle = .t.
					store "ITEMNUM*"+upper(alltrim(x856.f5)) to citemnum
					m.printstuff = iif(empty(m.printstuff),citemnum,m.printstuff+chr(13)+citemnum)
					store upper(alltrim(x856.f5)) to citemnum
					if alltrim(x856.f8) = "VN"
						m.shipstyle= upper(alltrim(x856.f9))    && added TMARG 7/28/16 mapped it to the shipstyle field
						if m.specialship ="70720"
							m.printstuff = m.printstuff+"DCPI*"+cvendorsku+chr(13)
						endif
					endif
				otherwise
					tsubject= "Missing Style/Code in Samsung 940"
					tattach = ""
					tcc = allt(tccerr)
					tsendto = allt(tsendtoerr)
					tmessage = "File "+cfilename+" was missing a Style code segment in PT# "+cship_ref+", at LX segment "+transform(m.linenum)+". Rejected."
					do form dartmail2 with tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"

					archivefile  = ("F:\ftpusers\samsung\940in\archive\"+justfname(currfile))
					if !file(archivefile)
						copy file &xfile to &archivefile
						erase &xfile
					endif
					normalexit = .f.
					throw
				endcase

				if alltrim(x856.f6) = "UP"
					store upper(alltrim(x856.f7)) to m.upc
					if empty(m.upc)
						set step on
						tsubject= "Missing UPC Code in Samsung 940"
						tattach = ""
						tcc = tccerr
						tsendto = tsendtoerr
						tmessage = "File "+cfilename+" was missing a UPC code segment. Rejected."
						tfrom ="TGF EDI Operations <toll-edi-ops@tollgroup.com>"
						do form dartmail2 with tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"

						archivefile  = ("F:\0-picktickets\archive\"+ccustname+"-CA\"+justfname(currfile))
						if !file(archivefile)
							copy file &xfile to &archivefile
							erase &xfile
						endif
						normalexit = .f.
						throw
					endif
				endif

				if lpick
					store "1" to m.pack  && Added to resolve 'pack' issue per Paul, 11/16/05
				else
					store alltrim(transform(int(val(x856.f1)))) to m.pack
				endif

				scatter fields ptid,accountid memvar
			endif

			if trim(x856.segment) = "G69" && Description
				cprtstr = "DESC*"+upper(alltrim(x856.f1))
				m.printstuff = iif(empty(m.printstuff),cprtstr,m.printstuff+chr(13)+cprtstr)
			endif

			if trim(x856.segment) = "N9" and alltrim(x856.f1) = "VC"
				set step on
				ccolordesc = "COLORDESC*"+upper(alltrim(x856.f2))
				m.color = upper(alltrim(x856.f2))
				m.printstuff = iif(empty(m.printstuff),ccolordesc,m.printstuff+chr(13)+ccolordesc)
			endif


			if trim(x856.segment) = "N9" and inlist(alltrim(x856.f1),"ZZ","CL")
				set step on
				ccolorcode = "COLORCODE*"+upper(alltrim(x856.f2))
				m.printstuff = iif(empty(m.printstuff),ccolorcode,m.printstuff+chr(13)+ccolorcode)
*!*					if loldstyle
*!*						do insertdet
*!*					endif
			endif




			if (trim(x856.segment) = "N9" and alltrim(x856.f1) = "SZ")
				m.id = upper(alltrim(x856.f2)) && +IIF(LCASE,"-PREPK","")
******				do insertdet
			endif

			if trim(x856.segment) = "N9" and alltrim(x856.f1) = "IN"
				set step on
				cin = "IN*"+upper(alltrim(x856.f2))
*				m.color = upper(alltrim(x856.f2))
				m.printstuff = iif(empty(m.printstuff),cin,m.printstuff+chr(13)+cin)
				if empty(custsku)
					m.custsku=upper(alltrim(x856.f2))
				else
				endif
			endif

			if trim(x856.segment) = "N9" and alltrim(x856.f1) = "LT"
				set step on
				clt = "LT*"+upper(alltrim(x856.f2))
*				m.color = upper(alltrim(x856.f2))
				m.printstuff = iif(empty(m.printstuff),clt,m.printstuff+chr(13)+clt)
				do insertdet
			endif

			if !("STORENUM*"$m.printstuff)
				m.printstuff = m.printstuff+chr(13)+"STORENUM*"+cstorenum
			endif

			if trim(x856.segment) = "W76" && Ctn Qty
				replace xpt.origqty with xpt.qty in xpt
			endif

			skip 1 in x856
		enddo

		select xptdet
	endif

	if trim(x856.segment) = "LX"
		m.linenum = trim(x856.f1)
	endif

	select x856
	if !eof()
		skip 1 in x856
	endif
enddo

do m:\dev\prg\setuppercase940

set step on

if lbrowfiles
	select xpt
	locate
	browse
	select xptdet
	locate
	browse
	if messagebox("Do you wish to continue?",4+16+256,"CONTINUE")=7
		cancel
		normalexit = .t.
		throw
	endif
endif

wait ccustname+" Breakdown Round complete..." window timeout 2
if used('x856')
	use in x856
endif
if used("PT")
	use in pt
endif
if used("PTDET")
	use in ptdet
endif
wait clear

********************
procedure insertdet
********************
m.ptid = ptctr
m.accountid = xpt.accountid
if ndetcnt = 0
	ndetcnt = 1
	if ltesting
		m.ship_ref = xpt.ship_ref
	endif
	insert into xptdet from memvar
	replace xptdet.origqty with xptdet.totqty in xptdet
	replace xpt.qty with xpt.qty+xptdet.totqty in xpt
	replace xpt.origqty with xpt.qty in xpt
*GATHER MEMVAR FIELDS ptid,accountid,STYLE,COLOR,PACK,ID,sku_count,sku_bkdn,units,upc,custsku
	select xptdet
else
	if ltesting
		m.ship_ref = xpt.ship_ref
	endif
	if lprepack
		locate for xptdet.ptid = xpt.ptid and xptdet.style = padr(m.style,20) and xptdet.upc = m.upc
	else
		locate for xptdet.ptid = xpt.ptid and xptdet.style = padr(m.style,20) ;
			and xptdet.color = m.color and xptdet.id = m.id and xptdet.upc = m.upc
	endif
	if found()
		m.oldpack = m.pack
		replace xptdet.totqty  with (xptdet.totqty + m.totqty)
		replace xptdet.origqty with xptdet.totqty
		replace xpt.qty with xpt.qty+xptdet.totqty in xpt
		select xptdet
	else
		m.ptdetid = m.ptdetid + 1
		insert into xptdet from memvar
		replace xptdet.origqty with xptdet.totqty
		replace xpt.qty with xpt.qty+xptdet.totqty in xpt
	endif

	replace xptdet.ptdetid with m.ptdetid
	scatter fields ptid,ptdetid,accountid memvar
	select xptdet
endif
endproc
