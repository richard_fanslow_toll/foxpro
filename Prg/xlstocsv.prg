PARAMETERS xfilexls, xfilecsv

xfilexls = upper(xfilexls)
assert .f.
if vartype(xfilecsv) = "L"
xfilecsv = strtran(xfilexls,".XLS",".CSV")
endif

RELEASE ALL LIKE o*

WAIT WINDOW "Now processing file "+xfilexls NOWAIT
oExcel = CREATEOBJECT("excel.application")
oExcel.displayalerts = .F.
oWorkbook = oExcel.workbooks.OPEN(xfilexls)
oWorkbook.SAVEAS(xfilecsv,6) && .csv
oWorkbook.CLOSE()
oExcel.QUIT()
RELEASE oExcel

return