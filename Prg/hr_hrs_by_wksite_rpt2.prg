* monthly hours report for Diana Landeros - run 1st of each month against prior month
* revised to use Insperity/Timestar instead of ADP/Kronos 2/6/14 MB.


* Build EXE as F:\UTIL\HR\OSHA\HR_HRS_BY_WKSITE_RPT.EXE

LOCAL loHR_HRS_BY_WKSITE_RPT

runack("HR_HRS_BY_WKSITE_RPT")

utilsetup("HR_HRS_BY_WKSITE_RPT")


loHR_HRS_BY_WKSITE_RPT = CREATEOBJECT('HR_HRS_BY_WKSITE_RPT')

loHR_HRS_BY_WKSITE_RPT.MAIN()


schedupdate()

CLOSE DATABASES ALL
RETURN

#DEFINE LOGIT 1
#DEFINE WAITIT 2
#DEFINE NOWAITIT 4
#DEFINE SENDIT 8
#DEFINE CRLF CHR(13) + CHR(10)
#DEFINE RETURN_DATA_MANDATORY .F.
#DEFINE RETURN_DATA_NOT_MANDATORY .T.
#DEFINE CSVFORMAT 6
#DEFINE WRITE_ONLY_UNBUFFERED 11
#DEFINE COMMA ","
#DEFINE xlSolid 1
#DEFINE xlAutomatic -4105
#DEFINE xlThemeColorDark1 1

DEFINE CLASS HR_HRS_BY_WKSITE_RPT AS CUSTOM

	cProcessName = 'HR_HRS_BY_WKSITE_RPT'

	lTestMode = .F.  && .F. FOR NORMAL OPERATION, .T. TO SUPPRESS ALL BUT DEVELOPER EMAILS AND CHANGE INPUT TABLE, ETC.

	lAutoYield = .T.

	cToday = DTOC(DATE())

	cStartTime = TTOC(DATETIME())
	
	* connection properties
	nSQLHandle = 0
	oExcel = NULL

	* wait window properties
	nWaitWindowTimeout = 2
	lWaitWindowIsOn = .T.

	* logfile properties
	lLoggingIsOn = .T.
	cLogFile = 'F:\UTIL\HR\OSHA\LOGFILES\HOURS_Monthly_log.txt'

	* INTERNAL email properties
	lSendInternalEmailIsOn = .T.
	cFrom = 'mark.bennett@tollgroup.com'
	cSendTo = 'Mariel.Calello@tollgroup.com, Diana.Landeros@Tollgroup.com, abigail.melaika@tollgroup.com'
	cCC = 'mbennett@fmiint.com'
	cSubject = 'Monthly Employee Hours Report for ' + DTOC(DATE())
	cAttach = ''
	cBodyText = ''

	cCOMPUTERNAME = ''
	cUSERNAME = ''


	FUNCTION INIT
		IF NOT DODEFAULT()
			RETURN .F.
		ENDIF
		WITH THIS
			*SET RESOURCE OFF
			CLOSE DATA
			SET CENTURY ON
			SET DATE AMERICAN
			SET HOURS TO 24
			SET ANSI ON
			SET TALK OFF
			SET DELETED ON
			SET CONSOLE OFF
			SET EXCLUSIVE OFF
			SET SAFETY OFF
			SET EXACT ON
			SET STATUS BAR ON
			SET SYSMENU OFF
			SET ENGINEBEHAVIOR 70
			_VFP.AUTOYIELD = .lAutoYield
			.cCOMPUTERNAME = GETENV("COMPUTERNAME")
			.cUSERNAME = GETENV("USERNAME")
			IF .lTestMode THEN
				.cLogFile = 'F:\UTIL\HR\OSHA\LOGFILES\HOURS_Monthly_log_TESTMODE.txt'
				.cSendTo = 'mbennett@fmiint.com'
				.cCC = ''
			ENDIF
			.lLoggingIsOn = .lLoggingIsOn AND NOT EMPTY(.cLogFile)
			IF .lLoggingIsOn THEN
				SET ALTERNATE TO (.cLogFile) ADDITIVE
				SET ALTERNATE ON
			ENDIF
		ENDWITH
	ENDFUNC


	FUNCTION DESTROY
		WITH THIS
			IF .lLoggingIsOn  THEN
				SET ALTERNATE OFF
				SET ALTERNATE TO
			ENDIF
			IF TYPE('.oExcel') = 'O' AND NOT ISNULL(.oExcel) THEN
				.oExcel.QUIT()
			ENDIF
		ENDWITH
		DODEFAULT()
	ENDFUNC


	FUNCTION MAIN
		WITH THIS
			LOCAL lnNumberOfErrors, lcSQL, lcSQL2, lcSQL3, lcSQL4, loError, ldDate, ldFromDate, ldToDate, lcToDate, lcFromDate, lcDriverDetailFile, lcAllDetailFile
			LOCAL lcLastName, ldLastPaydate, lnLastFile_Num, lcHavingClause, lcPaydatesFile, lcJobDesc, lc02DriversFile, lc03DriversFile
			LOCAL lcOutputFileTDDrivers, lnNJ1Drivers, lnSP2Drivers, lnCA2Drivers, loWorkbook, loWorksheet, lnRow, lcRow
			
			TRY
				lnNumberOfErrors = 0

				.TrackProgress('Monthly Employee Hours Report process started....', LOGIT+SENDIT+NOWAITIT)
				.TrackProgress('COMPUTERNAME = ' + .cCOMPUTERNAME, LOGIT+SENDIT)
				.TrackProgress('USERNAME = ' + .cUSERNAME, LOGIT+SENDIT)
				.TrackProgress('PROJECT = HR_HRS_BY_WKSITE_RPT', LOGIT+SENDIT)
				IF .lTestMode THEN
					.TrackProgress('                        TEST MODE                        ', LOGIT+SENDIT)
				ENDIF

				* construct dates for prior month
				ldDate = DATE()

				* TO FORCE OTHER MONTHS
				*ldDate = {^2018-05-01}

				ldToDate = ldDate - DAY(ldDate)  && this is last day of prior month
				ldFromDate = GOMONTH(ldToDate + 1,-1)   && this is first day of prior month

*!*					* to force date ranges
*!*	ldFromDate = {^2017-01-01}
*!*	ldToDate = {^2017-12-31}


** QUERY BELOW GETS ONLY HOURLY

				IF USED('EEINFO') THEN
					USE IN EEINFO
				ENDIF
				IF USED('CURTIMEDATAPRE') THEN
					USE IN CURTIMEDATAPRE
				ENDIF
				IF USED('CURTIMEDATA') THEN
					USE IN CURTIMEDATA
				ENDIF
				IF USED('TIMEDATA') THEN
					USE IN TIMEDATA
				ENDIF
				IF USED('CURSALPRE') THEN
					USE IN CURSALPRE
				ENDIF				
				
				
				USE F:\UTIL\TIMESTAR\DATA\EEINFO.DBF IN 0 ALIAS EEINFO
				
				USE F:\UTIL\TIMESTAR\DATA\TIMEDATA.DBF IN 0 ALIAS TIMEDATA
				
*!*					SELECT ;
*!*						NAME, ;
*!*						WORKDATE AS PAYDATE, ;
*!*						FILE_NUM, ;
*!*						DEPT, ;
*!*						(DIVISION + DEPT) AS HOMEDEPT, ;
*!*						WORKSITE, ;
*!*						ADP_COMP, ;
*!*						'                    ' AS JOBDESC, ;
*!*						(REGHOURS + OTHOURS + DTHOURS) AS HOURS ;
*!*						FROM TIMEDATA ;
*!*						INTO CURSOR CURTIMEDATAPRE ;
*!*						WHERE ADP_COMP = 'E87' ;
*!*						AND WORKDATE >= ldFromDate ;
*!*						AND WORKDATE <= ldToDate ;
*!*						AND LEN(ALLTRIM(WORKSITE)) = 3 ;
*!*						ORDER BY NAME, WORKDATE ;
*!*						READWRITE
				
				* this select now includes Salaried AND CODEHOURS
				SELECT ;
					NAME, ;
					WORKDATE AS PAYDATE, ;
					FILE_NUM, ;
					DEPT, ;
					(DIVISION + DEPT) AS HOMEDEPT, ;
					WORKSITE, ;
					ADP_COMP, ;
					'                    ' AS JOBDESC, ;
					(CODEHOURS + REGHOURS + OTHOURS + DTHOURS) AS HOURS ;
					FROM TIMEDATA ;
					INTO CURSOR CURTIMEDATAPRE ;
					WHERE WORKDATE >= ldFromDate ;
					AND WORKDATE <= ldToDate ;
					AND (DIVISION + DEPT) <> '020655' ;
					AND LEN(ALLTRIM(WORKSITE)) = 3 ;
					ORDER BY NAME, WORKDATE ;
					READWRITE
					
*!*	SELECT CURTIMEDATAPRE 
*!*	BROWSE


*!*	** QUERY BELOW GETS ONLY SALARIED - 
*!*	** Salaried will simply get one record in the cursor, with 166 hours for the month.
*!*	SELECT ;
*!*		NAME, ;
*!*		ldFromDate AS PAYDATE, ;
*!*		ALLTRIM(TRANSFORM(INSPID)) AS FILE_NUM, ;
*!*		DEPT, ;
*!*		(DIVISION + DEPT) AS HOMEDEPT, ;
*!*		WORKSITE, ;
*!*		ADP_COMP, ;
*!*		'                    ' AS JOBDESC, ;
*!*		166.00 AS HOURS ;
*!*		FROM EEINFO ; 
*!*		INTO CURSOR CURSALPRE ;
*!*		WHERE ((ADP_COMP <> 'E87') OR ((DIVISION + DEPT) = '020655')) ;
*!*		AND STATUS <> 'T' ;
*!*		ORDER BY NAME ;
*!*		READWRITE


** QUERY BELOW GETS ONLY OTR DRIVERS - SALARIED NOW GOTTEN IN MAIN SELECT
SELECT ;
	NAME, ;
	ldFromDate AS PAYDATE, ;
	ALLTRIM(TRANSFORM(INSPID)) AS FILE_NUM, ;
	DEPT, ;
	(DIVISION + DEPT) AS HOMEDEPT, ;
	WORKSITE, ;
	ADP_COMP, ;
	'                    ' AS JOBDESC, ;
	195.00 AS HOURS ;
	FROM EEINFO ; 
	INTO CURSOR CURSALPRE ;
	WHERE ((DIVISION + DEPT) = '020655') ;
	AND STATUS = 'A' ;
	AND HIREDATE <= ldToDate ;
	ORDER BY NAME ;
	READWRITE

*!*	* regional drivers should also get 195 hours, but there is no way to identify them
*!*	SELECT CURSALPRE
*!*	SCAN FOR (HOMEDEPT = '020655')
*!*		REPLACE CURSALPRE.HOURS WITH 195 IN CURSALPRE
*!*	ENDSCAN

*!*	SELECT CURSALPRE
*!*	BROWSE
						
						* APPEND SALARIED INFO TO HOURLY INFO
						SELECT CURTIMEDATAPRE
						APPEND FROM DBF('CURSALPRE')

*!*	SELECT CURTIMEDATAPRE
*!*	BROWSE
*!*	THROW
						
						USE IN CURSALPRE
						
						* revised 4/4/2017 per Diana Landeros:
						* she only wants NJ1 and SP2 broken out by driver/non-driver job descriptions. 
						* All other worksites can keep job desc blank

						*!*	SELECT CURTIMEDATAPRE
						*!*	SCAN 
						*!*		DO CASE
						*!*			CASE INLIST(CURTIMEDATAPRE.DEPT,'0650','0655','0660')
						*!*				lcJobDesc = 'Driver'
						*!*			CASE INLIST(CURTIMEDATAPRE.DEPT,'0604','0620','0622','0623','0625','0630','0635','0640','0642') OR ;
						*!*					INLIST(CURTIMEDATAPRE.DEPT,'0643','0644','0645','0646','1603','1642','1643','1644')
						*!*				lcJobDesc = 'Warehouse'
						*!*			CASE INLIST(CURTIMEDATAPRE.DEPT,'0670','0680','1680','2680')
						*!*				lcJobDesc = 'Mechanic'
						*!*			OTHERWISE
						*!*				lcJobDesc = 'Clerical'
						*!*		ENDCASE							
						*!*		REPLACE CURTIMEDATAPRE.JOBDESC WITH lcJobDesc IN CURTIMEDATAPRE
						*!*	ENDSCAN
						
						* REVISED 5/3/2017 MB per Diana Landeros:
						* Union drivers moved from SP2 to CA2 in middle of April 2017, so I am adding Carson 2 to the special processing.
						* For April 2017 the union driver headcnts will be duplicated in both SP2 and CA2.
						* After April there should be no more duplication.
						
						
						***************************************************************************************************************************************************
						*** added this scan to change any SP2 worksites in the selected data to CA2; according to HR all drivers are now in CA2, but this may not be reflected in timestar.
						* this is just for this report; does not affect source time data.
						*
						SELECT CURTIMEDATAPRE
						*SCAN FOR BETWEEN(CURTIMEDATAPRE.PAYDATE,{^2017-05-01},{^2017-05-31}) AND INLIST(CURTIMEDATAPRE.WORKSITE,'SP2') AND INLIST(CURTIMEDATAPRE.DEPT,'0650','0655','0660')
						SCAN FOR (CURTIMEDATAPRE.PAYDATE >= {^2017-04-01}) AND INLIST(CURTIMEDATAPRE.WORKSITE,'SP2') AND INLIST(CURTIMEDATAPRE.DEPT,'0650','0655','0660')
							REPLACE CURTIMEDATAPRE.WORKSITE WITH 'CA2' IN CURTIMEDATAPRE
						ENDSCAN
						***************************************************************************************************************************************************
						

						SELECT CURTIMEDATAPRE
						*BROWSE FOR INLIST(CURTIMEDATAPRE.DEPT,'0650','0655','0660')
						*throw
						
						
						*SCAN FOR INLIST(WORKSITE,'NJ1','SP2')
						SCAN FOR INLIST(WORKSITE,'CA2','NJ1')
							DO CASE
								CASE INLIST(CURTIMEDATAPRE.DEPT,'0650','0655','0660')
									lcJobDesc = 'Driver'
								OTHERWISE
									lcJobDesc = 'Non-Driver'
							ENDCASE							
							REPLACE CURTIMEDATAPRE.JOBDESC WITH lcJobDesc IN CURTIMEDATAPRE
						ENDSCAN
						
						
						
						
*!*							
*!*						SELECT CURTIMEDATAPRE 
*!*						BROWSE FOR WORKSITE = 'NJ1' AND ALLTRIM(JOBDESC) = 'Mechanic'
*!*						THROW
						
						* provide driver detail for Jack.
						lcAllDetailFile = "F:\UTIL\HR\OSHA\REPORTS\ALLDETAIL-" + DTOS(ldFromDate) + "-" + DTOS(ldToDate) + ".XLS"
						
						lcDriverDetailFile = "F:\UTIL\HR\OSHA\REPORTS\DRIVERDETAIL-" + DTOS(ldFromDate) + "-" + DTOS(ldToDate) + ".XLS"
						
*!*							lc02DriversFile = "F:\UTIL\HR\OSHA\REPORTS\DIV02_DRIVERS-" + DTOS(ldFromDate) + "-" + DTOS(ldToDate) + ".XLS"
*!*							lc03DriversFile = "F:\UTIL\HR\OSHA\REPORTS\DIV03_DRIVERS-" + DTOS(ldFromDate) + "-" + DTOS(ldToDate) + ".XLS"
						
						lcOutputFileTDDrivers = "F:\UTIL\HR\OSHA\REPORTS\DRIVERS-IN-TIMEDATA" + DTOS(ldFromDate) + "-" + DTOS(ldToDate) + ".XLS"
						
						lcOutputFileEEINFODrivers = "F:\UTIL\HR\OSHA\REPORTS\DRIVERS-NOT-IN-TIMEDATA" + DTOS(ldFromDate) + "-" + DTOS(ldToDate) + ".XLS"
						
						SELECT CURTIMEDATAPRE 
						COPY TO (lcAllDetailFile) XL5
						COPY FIELDS NAME, FILE_NUM, PAYDATE, WORKSITE, JOBDESC, HOURS TO (lcDriverDetailFile) FOR UPPER(ALLTRIM(JOBDESC)) = "DRIVER" XL5
						
						*!*	IF USED('CURDIV02') THEN
						*!*		USE IN CURDIV02
						*!*	ENDIF

						*!*	IF USED('CURDIV03') THEN
						*!*		USE IN CURDIV03
						*!*	ENDIF

						*!*	SELECT WORKSITE, ;
						*!*		NAME, ;
						*!*		JOBDESC, ;
						*!*		SUM(HOURS) AS WRKDHOURS ;
						*!*		FROM CURTIMEDATAPRE ;
						*!*		INTO CURSOR CURDIV02 ;
						*!*		WHERE UPPER(ALLTRIM(JOBDESC)) = "DRIVER" ;
						*!*		AND LEFT(HOMEDEPT,2) = '02' ;
						*!*		GROUP BY 1, 2, 3 ;
						*!*		ORDER BY 1, 2

						*!*	SELECT WORKSITE, ;
						*!*		NAME, ;
						*!*		JOBDESC, ;
						*!*		SUM(HOURS) AS WRKDHOURS ;
						*!*		FROM CURTIMEDATAPRE  ;
						*!*		INTO CURSOR CURDIV03 ;
						*!*		WHERE UPPER(ALLTRIM(JOBDESC)) = "DRIVER" ;
						*!*		AND LEFT(HOMEDEPT,2) = '03' ;
						*!*		GROUP BY 1, 2, 3 ;
						*!*		ORDER BY 1, 2
						*!*		
						*!*	SELECT CURDIV02	
						*!*	COPY TO (lc02DriversFile) XL5
						*!*		
						*!*	SELECT CURDIV03	
						*!*	COPY TO (lc03DriversFile) XL5


						SELECT ;
							B.GROUP, ;
							A.WORKSITE, ;
							B.OSHANUM AS SITENUMBER, ;
							B.DESC, ;
							A.JOBDESC, ;
							COUNT(DISTINCT A.FILE_NUM) AS HEADCNT, ;
							SUM(A.HOURS) AS WRKDHOURS ;
							FROM CURTIMEDATAPRE A ;
							LEFT OUTER JOIN ;
							F:\UTIL\HR\DATA\WORKSITES B ;
							ON B.WORKSITE = A.WORKSITE ;
							INTO CURSOR CURTIMEDATA ;
							GROUP BY 1, 2, 3, 4, 5 ;
							ORDER BY 1, 4 ;
							READWRITE

						*!*									SUM(A.VACHRS + A.HOLHRS) AS VHHOURS ;
						
						* ADD row for SP3 wilmington yard if it's not there
						SELECT CURTIMEDATA
						LOCATE FOR UPPER(ALLTRIM(WORKSITE)) = 'SP3'
						IF NOT FOUND() THEN
							m.GROUP = 'TGL'
							m.WORKSITE = 'SP3'
							m.SITENUMBER = 'US0033'
							m.DESC = 'Wilmington Yard'
							m.JOBDESC = ''
							m.HEADCNT = 0
							m.WRKDHOURS = 0.00
							INSERT INTO CURTIMEDATA FROM MEMVAR
						ENDIF
						
						* change sitenumber for Carson 2 Drivers to US0037 Per Diana Landeros 10/23/2017 MB
						SELECT CURTIMEDATA
						LOCATE FOR (UPPER(ALLTRIM(WORKSITE)) = 'CA2') AND (UPPER(ALLTRIM(JOBDESC)) == 'DRIVER')
						IF FOUND() THEN
							REPLACE CURTIMEDATA.SITENUMBER WITH 'US0037' IN CURTIMEDATA
						ENDIF
						
						* change sitenumber for Carteret Drivers to US0036 Per Diana Landeros 10/23/2017 MB
						SELECT CURTIMEDATA
						LOCATE FOR (UPPER(ALLTRIM(WORKSITE)) = 'NJ1') AND (UPPER(ALLTRIM(JOBDESC)) == 'DRIVER')
						IF FOUND() THEN
							REPLACE CURTIMEDATA.SITENUMBER WITH 'US0036' IN CURTIMEDATA
						ENDIF


						lcOutputFile = "F:\UTIL\HR\OSHA\REPORTS\HOURSBYWORKSITE-" + DTOS(ldFromDate) + "-" + DTOS(ldToDate) + ".XLS"
						
						* determine active drivers who were not in the timedata
						* first get the names and filenums who were in the timedata into a cursor
						SELECT DISTINCT NAME, VAL(FILE_NUM) AS INSPID, WORKSITE ;
							FROM CURTIMEDATAPRE ;
							INTO CURSOR CURTIMEDATADRIVERS ;
							WHERE UPPER(ALLTRIM(JOBDESC)) = "DRIVER" ;
							ORDER BY NAME
							
*!*							SELECT CURTIMEDATADRIVERS 
*!*							BROWSE
						
						* then get NON-TERMINATED NJ1 and SP2 drivers from EEINFO into another cursor
						SELECT NAME, INSPID, WORKSITE, STATUS ;
							FROM EEINFO ;
							INTO CURSOR CUREEINFODRIVERS ;
							WHERE STATUS <> 'T' ;
							AND WORKSITE IN ('CA2','NJ1','SP2') ;
							AND DEPT IN ('0650','0655','0660') ;
							AND INSPID NOT IN (SELECT INSPID FROM CURTIMEDATADRIVERS) ;
							ORDER BY NAME

							
*!*							SELECT CUREEINFODRIVERS
*!*							BROWSE

						* Diana said adjusting the counts is not necessary 4/5/2017, so disabled below
						
						*!*	* COUNT THE DRIVERS IN CUREEINFODRIVERS AND UPDATE THE COUNTS BY WORKSITE FOR NJ1 AND SP2

						*!*	SELECT CUREEINFODRIVERS
						*!*	COUNT TO lnNJ1Drivers FOR WORKSITE = 'NJ1'
						*!*	COUNT TO lnSP2Drivers FOR WORKSITE = 'SP2'
						*!*	.TrackProgress('lnNJ1Drivers = ' + TRANSFORM(lnNJ1Drivers), LOGIT+SENDIT+NOWAITIT)
						*!*	.TrackProgress('lnSP2Drivers = ' + TRANSFORM(lnSP2Drivers), LOGIT+SENDIT+NOWAITIT)

						*!*	IF lnNJ1Drivers > 0 THEN
						*!*		SELECT CURTIMEDATA
						*!*		LOCATE FOR WORKSITE = 'NJ1' AND ALLTRIM(UPPER(JOBDESC)) = 'DRIVER'
						*!*		IF FOUND() THEN
						*!*			REPLACE CURTIMEDATA.HEADCNT WITH (CURTIMEDATA.HEADCNT + lnNJ1Drivers) IN CURTIMEDATA	
						*!*		ELSE
						*!*			.TrackProgress('=====> ERROR: could not locate NJ1 Driver row in CURTIMEDATA', LOGIT+SENDIT+NOWAITIT)
						*!*		ENDIF
						*!*	ENDIF

						*!*	IF lnSP2Drivers > 0 THEN
						*!*		SELECT CURTIMEDATA
						*!*		LOCATE FOR WORKSITE = 'SP2' AND ALLTRIM(UPPER(JOBDESC)) = 'DRIVER'
						*!*		IF FOUND() THEN
						*!*			REPLACE CURTIMEDATA.HEADCNT WITH (CURTIMEDATA.HEADCNT + lnSP2Drivers) IN CURTIMEDATA	
						*!*		ELSE
						*!*			.TrackProgress('=====> ERROR: could not locate SP2 Driver row in CURTIMEDATA', LOGIT+SENDIT+NOWAITIT)
						*!*		ENDIF
						*!*	ENDIF


						* OPEN Excel
						.oExcel = CREATEOBJECT("excel.application")
						.oExcel.displayalerts = .F.
						.oExcel.VISIBLE = .F.
						
						lcTemplateFile = 'F:\UTIL\HR\OSHA\TEMPLATES\HOURSBYWORKSITE-Template01.xls'

						loWorkbook = .oExcel.workbooks.OPEN(lcTemplateFile)
						loWorkbook.SAVEAS(lcOutputFile)
						loWorksheet = loWorkbook.worksheets[1]
						loWorksheet.SELECT()
						*SET STEP ON 
						
						loWorksheet.RANGE("A1").VALUE = 'Employee hours by Worksite for ' + DTOS(ldFromDate) + "-" + DTOS(ldToDate)
						
						lnRow = 3
						SELECT CURTIMEDATA
						SCAN
							lnRow = lnRow + 1
							lcRow = LTRIM(STR(lnRow))
							loWorksheet.RANGE("A" + lcRow).VALUE = CURTIMEDATA.group
							loWorksheet.RANGE("B" + lcRow).VALUE = CURTIMEDATA.worksite
							loWorksheet.RANGE("C" + lcRow).VALUE = CURTIMEDATA.sitenumber
							loWorksheet.RANGE("D" + lcRow).VALUE = CURTIMEDATA.desc
							loWorksheet.RANGE("E" + lcRow).VALUE = CURTIMEDATA.jobdesc
							loWorksheet.RANGE("F" + lcRow).VALUE = CURTIMEDATA.headcnt
							loWorksheet.RANGE("G" + lcRow).VALUE = CURTIMEDATA.wrkdhours
							
							IF MOD(lnRow,2) = 0 THEN							
							    loWorksheet.Range("A"+lcRow,"G"+lcRow).Select
							    With .oExcel.Selection.Interior
							        .Pattern = xlSolid
							        .PatternColorIndex = xlAutomatic
							        .ThemeColor = xlThemeColorDark1
							        .TintAndShade = -0.149998474074526
							        .PatternTintAndShade = 0
							    EndWith
						    ENDIF
							
						ENDSCAN
						
						loWorksheet.RANGE("G1").select

						loWorkbook.SAVE()	
						
						.oExcel.Quit()

						IF FILE(lcOutputFile) THEN
							* attach output file to email
							.cAttach = lcOutputFile
							.cBodyText = "See attached Monthly Employee Hours Report." + ;
								CRLF + CRLF + "(do not reply - this is an automated report)" + ;
								CRLF + CRLF + "<report log follows>" + ;
								CRLF + .cBodyText
								
								*!*	IF FILE(lcDriverDetailFile) THEN
								*!*		* attach output file to email
								*!*		.cAttach = .cAttach + "," + lcDriverDetailFile
								*!*	ELSE
								*!*		.TrackProgress('ERROR: unable to email spreadsheet: ' + lcDriverDetailFile, LOGIT+SENDIT+NOWAITIT)
								*!*	ENDIF

								*!*	IF FILE(lc02DriversFile) THEN
								*!*		* attach output file to email
								*!*		.cAttach = .cAttach + "," + lc02DriversFile
								*!*	ELSE
								*!*		.TrackProgress('ERROR: unable to email spreadsheet: ' + lc02DriversFile, LOGIT+SENDIT+NOWAITIT)
								*!*	ENDIF

								*!*	IF FILE(lc03DriversFile) THEN
								*!*		* attach output file to email
								*!*		.cAttach = .cAttach + "," + lc03DriversFile
								*!*	ELSE
								*!*		.TrackProgress('ERROR: unable to email spreadsheet: ' + lc03DriversFile, LOGIT+SENDIT+NOWAITIT)
								*!*	ENDIF
							
						ELSE
							.TrackProgress('ERROR: unable to email spreadsheet: ' + lcOutputFile, LOGIT+SENDIT+NOWAITIT)
						ENDIF
						

						SELECT CURTIMEDATADRIVERS 
						COPY TO (lcOutputFileTDDrivers) XL5
						.TrackProgress('Output to spreadsheet: ' + lcOutputFileTDDrivers, LOGIT+SENDIT+NOWAITIT)
						
						IF FILE(lcOutputFileTDDrivers) THEN
							* attach output file to email
							.cAttach = .cAttach + "," + lcOutputFileTDDrivers
						ELSE
							.TrackProgress('ERROR: unable to email spreadsheet: ' + lcOutputFileTDDrivers, LOGIT+SENDIT+NOWAITIT)
						ENDIF
						

						SELECT CUREEINFODRIVERS
						COPY TO (lcOutputFileEEINFODrivers) XL5
						.TrackProgress('Output to spreadsheet: ' + lcOutputFileEEINFODrivers, LOGIT+SENDIT+NOWAITIT)
						
						IF FILE(lcOutputFileEEINFODrivers) THEN
							* attach output file to email
							.cAttach = .cAttach + "," + lcOutputFileEEINFODrivers
						ELSE
							.TrackProgress('ERROR: unable to email spreadsheet: ' + lcOutputFileEEINFODrivers, LOGIT+SENDIT+NOWAITIT)
						ENDIF

						CLOSE DATABASES ALL

					.TrackProgress('Monthly Employee Hours Report process ended normally.', LOGIT+SENDIT+NOWAITIT)

			CATCH TO loError

				.TrackProgress('There was an error.',LOGIT+SENDIT)
				.TrackProgress('The Error # is: ' + TRANSFORM(loError.ERRORNO), LOGIT+SENDIT)
				.TrackProgress('The Error Message is: ' + TRANSFORM(loError.MESSAGE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred in Program: ' + TRANSFORM(loError.PROCEDURE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred at line #: ' + TRANSFORM(loError.LINENO), LOGIT+SENDIT)
				lnNumberOfErrors = lnNumberOfErrors + 1
				CLOSE DATA
				IF TYPE('.oExcel') = 'O' AND NOT ISNULL(.oExcel) THEN
					.oExcel.QUIT()
					.oExcel = NULL
				ENDIF

			ENDTRY

			CLOSE DATA
			WAIT CLEAR
			***************** INTERNAL email results ******************************
			.TrackProgress('About to send status email.',LOGIT)
			.TrackProgress('The logfile is: ' + .cLogFile, SENDIT)
			.TrackProgress('Monthly Employee Hours Report process started: ' + .cStartTime, LOGIT+SENDIT)
			.TrackProgress('Monthly Employee Hours Report process finished: ' + TTOC(DATETIME()), LOGIT+SENDIT)

			IF .lSendInternalEmailIsOn THEN
				DO FORM dartmail2 WITH .cSendTo,.cFrom,.cSubject,.cCC,.cAttach,.cBodyText,"A"
				.TrackProgress('Sent status email.',LOGIT)
			ELSE
				.TrackProgress('Did not send status email: THIS.lSendInternalEmailIsOn = FALSE.',LOGIT)
			ENDIF

		ENDWITH
		RETURN
	ENDFUNC && main


	FUNCTION ExecSQL
		LPARAMETERS tcSQL, tcCursorName, tlNoDataReturnedIsOkay
		LOCAL llRetval, lnResult
		WITH THIS
			* close target cursor if it's open
			IF USED(tcCursorName)
				USE IN (tcCursorName)
			ENDIF
			WAIT WINDOW "Fetching remote data; please wait..." NOWAIT
			lnResult = SQLEXEC(.nSQLHandle, tcSQL, tcCursorName)
			llRetval = ( lnResult > 0 )
			IF llRetval THEN
				* see if any data came back
				IF NOT tlNoDataReturnedIsOkay THEN
					IF (NOT USED(tcCursorName)) OR EOF(tcCursorName) THEN
						llRetval = .F.
						.TrackProgress('No data was returned by this query: ' +CRLF + tcSQL, LOGIT+SENDIT)
						.cSendTo = 'Mark Bennett <mbennett@fmiint.com>'
					ENDIF
				ENDIF
			ELSE
				.TrackProgress("Result Code [" + TRANSFORM(lnResult) + "] returned by this query: " + tcSQL, LOGIT+SENDIT)
				.cSendTo = 'Mark Bennett <mbennett@fmiint.com>'
			ENDIF
			WAIT CLEAR
			RETURN llRetval
		ENDWITH
	ENDFUNC


	PROCEDURE TrackProgress
		* do any combination of Wait Window, writing to logfile, and adding to body of email,
		* based on nFlags parameter.
		LPARAMETERS tcExpression, tnFlags
		WITH THIS
			IF BITAND(tnFlags,LOGIT) = LOGIT THEN
				IF .lLoggingIsOn THEN
					?
					? .cProcessName + "  " + TTOC(DATETIME()) + ": " + tcExpression
				ENDIF
			ENDIF
			IF .lWaitWindowIsOn THEN
				IF BITAND(tnFlags,NOWAITIT) = NOWAITIT THEN
					WAIT WINDOW tcExpression NOWAIT
				ENDIF
				IF BITAND(tnFlags,WAITIT) = WAITIT THEN
					WAIT WINDOW tcExpression TIMEOUT .nWaitWindowTimeout
				ENDIF
			ENDIF
			IF BITAND(tnFlags,SENDIT) = SENDIT THEN
				IF .lSendInternalEmailIsOn THEN
					.cBodyText = .cBodyText + tcExpression + CRLF + CRLF
				ENDIF
			ENDIF
		ENDWITH
	ENDPROC  &&  TrackProgress


ENDDEFINE
