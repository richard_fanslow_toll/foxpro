**inReceipt.prg
**
**Inbound Receipt of all freight taken into the whse for a given date range (style detail)
**

xsqlexec("select indet.*, inwolog.acctname, inwolog.container, inwolog.reference, inwolog.acct_ref " +;
	"from inwolog, indet where inwolog.inwologid=indet.inwologid and inwolog.accountid="+transform(xaccountid)+" "+ ;
	"and between(date_rcvd,{"+dtoc(xstartdt)+"},{"+dtoc(xenddt)+"}) order by inwolog.wo_num","csrwolog",,"wh")

creportname = "Inbound Receipt"

loadwebpdf("inReceipt")

use in csrwolog
