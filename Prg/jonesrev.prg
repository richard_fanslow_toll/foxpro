*This report tabulates tonnage and revenue information for the transload services for Jones and Nine West for one month.
*the report is based on Work Order date in WOLOG. (not invoice or manifest dates)

email("Dyoung@fmiint.com","jonesrev.prg runs",,,,,.t.,,,,,.t.,,.t.)

use f:\wo\wodata\wolog in 0
use f:\wo\wodata\manifest order orig_wo in 0

CREATE CURSOR TEMP (start d, end d, dc c(20), office c(1), GROUP C(3),event c(40),loose l, type c(1), ctrs n(10,0), cartons n(10,0), ctnwt n(10,0), ccbm n(10,0), goh n(10,0), suits n(10,0), ;
gwt n(10,0), gcbm n(10,0), lpuchg n(10,2), lfsc n(10,2), ldelchg n(10,2), ldelfsc n(10,2), tload n(10,2), lhaul n(10,2), lhfsc n(10,2), SORTING N(10,2),otherchg n(10,2))

STORE tstart TO startdate
STORE tend TO enddate
STORE MONTH(tstart) TO whichmonth
STORE YEAR(tstart) TO whichyear
STORE DTOC(startdate)+' - '+DTOC(enddate) TO tfortheperiod


*there are five DCs for Jones: Tennessee, Virginia, Texas, Gilbert and West Deptford (Nine West)

STORE 1 TO whichdc

DO WHILE whichdc<6
WAIT clear

			DO case
			CASE whichdc=1  &&TEXAS
			STORE 'AL' TO whichdelloc
			STORE 'SOC' TO whichdelloc2
			STORE 'EL PASO' TO whichdesc

WAIT "Collecting Data for El Paso....." WINDOW AT 20,60 nowait			

			CASE whichdc=2  &&TENN
			STORE 'TN' TO whichdelloc
			STORE 'TN' TO whichdelloc2
			STORE 'TENNESSEE' TO whichdesc
WAIT "Collecting Data for Tennessee....." WINDOW AT 20,60 nowait						

			CASE whichdc=3  &&VA
			STORE 'VA' TO whichdelloc
			STORE 'VA' TO whichdelloc2
			STORE 'VIRGINIA' TO whichdesc
WAIT "Collecting Data for Virginia....." WINDOW AT 20,60 nowait									

			CASE whichdc=4  &&Gilbert
			STORE 'TP' TO whichdelloc
			STORE 'TP' TO whichdelloc2
			STORE 'GILBERT' TO whichdesc
WAIT "Collecting Data for Gilbert....." WINDOW AT 20,60 nowait												


			CASE whichdc=5  &&Nine West
			STORE 'WD' TO whichdelloc
			STORE 'WD' TO whichdelloc2
			STORE 'WEST DEPTFORD' TO whichdesc
WAIT "Collecting Data for Nine West....." WINDOW AT 20,60 nowait												


			ENDCASE




*There are three Transload Ports of Discharge, New York, Los Angeles and Miami

STORE 1 TO whichoffice



DO WHILE whichoffice<4

*cofilter is used so we can add up both "J" and "T" billing for New York air shipments

				DO case
				CASE whichoffice=1
				STORE 'C' TO toffice
				STORE 'W' TO tcompany
				STORE "company='W'" TO cofilter
				CASE whichoffice=2
				STORE 'N' TO toffice
				STORE 'T' TO tcompany
				STORE "(company='T' or company='J' or company='I')" TO cofilter				
				CASE whichoffice=3
				STORE 'M' TO toffice
				STORE 'M' TO tcompany
				STORE "company='M'" TO cofilter								
				endcase


*There are two modes of transport: Air and Ocean
STORE 1 TO whichmode

DO WHILE whichmode<3

				IF whichmode=1
				STORE 'O' TO ttype
				ELSE
				STORE 'A' TO ttype
				ENDIF



STORE 1 TO looseorfull


*Within the Ocean Mode, there are two shipping conditions: Loose freight or Full Container, AIr freight has only Loose

DO WHILE looseorfull<3

			IF looseorfull=1
			STORE .F. TO tloose
			ELSE
			STORE .T. TO tloose
			endif
			IF TTYPE='A'
			STORE .T. TO TLOOSE
			STORE 2 TO LOOSEORFULL
			ENDIF


*Begin the search of the Work Order Log for the Division, Office, Mode and Type:

			SELECT wolog
			IF WHICHDC=5
			LOCATE FOR office=toffice AND TYPE=ttype AND accountid=1747 AND wo_date>=startdate  AND wo_date<=enddate AND loose=tloose AND !INLIST(billtoid,5339,86,4346,679,24)

			ELSE
			LOCATE FOR office=toffice AND TYPE=ttype AND INLIST(accountid,1,4244,3544,3132,62,3014,5632) AND wo_date>=startdate  AND wo_date<=enddate AND loose=tloose AND !INLIST(billtoid,5339,86,4346,679,24)
			ENDIF

			STORE 0 TO tqty,twt,tcbm,gqty,gwt,gcbm


			STORE 0 TO onedcnum,onedcctn,onedcwt,onedccbm,onedcgoh,onedcgohwt,onedcgohcbm
			STORE 0 TO onedc2datesnum,onedc2datesctn,onedc2dateswt,onedc2datescbm,onedc2datesgoh,onedc2datesgohwt,onedc2datesgohcbm
			STORE 0 TO onedconedatenum,onedconedatectn,onedconedatewt,onedconedatecbm,onedconedategoh,onedconedategohwt,onedconedategohcbm,onedcldelchg
			STORE 0 TO onedc3delchg,onedc3delfsc,onedc4delchg,onedc4delfsc
			STORE 0 TO mixeddcnum,mixeddcctn,mixeddcwt,mixeddccbm,mixeddcgoh,mixeddcgohwt,mixeddcgohcbm
			STORE 0 TO tt1pu,tt1lfsc,tt1tl,tt1lh,tt1lhfsc,tt1other,tsorting1,TT1SORTING,TT2SORTING,TT3SORTING,TT4SORTING
			STORE 0 TO tt2pu,tt2lfsc,tt2tl,tt2lh,tt2lhfsc,tt2other
			STORE 0 TO tt3pu,tt3lfsc,tt3tl,tt3lh,tt3lhfsc,tt3other
			STORE 0 TO tt4pu,tt4lfsc,tt4tl,tt4lh,tt4lhfsc,tt4other
			STORE 0 TO tsuits1,tsuits2,tsuits3,tsuits4

*Now check each work order for the specific DC in Manifest (except for Nine West New York which, FOR FULL CONTAINERS, is not manifested):
STORE .f. TO nineshpt

DO WHILE !EOF()
STORE 0 TO thiswt

			STORE wo_num TO two

			
			*Since Nine West New York shipments are not manifested, use the quantity and weight info from the WOLOG.			

			IF type='O' AND accountid=1747 AND office='N' AND !loose
			STORE RECNO() TO ninerecno
			STORE .t. TO nineshpt
			onedcctn=onedcctn+wolog.quantity 
			onedcwt=onedcwt+wolog.weight
			thiswt=wolog.weight
			allweight=wolog.weight
			thisweight=wolog.weight
			onedccbm=onedccbm+wolog.cbm
			STORE 3 TO tinvtype
			STORE .t. TO justonedc
			STORE .f. TO onestop
			STORE .f. TO onedate
			STORE .t. TO somethisdc
			
			
			ELSE  &&other than Nine West
			
						*justonedc=means the inbound shipment (or container) was going to a single DC no other location			
						STORE .t. TO justonedc,onestop,onedate



						SELECT manifest
						SEEK two
						
						

						STORE .F. TO SOMETHISDC
						*For Tennessee shipments we do two additional sorts, for two buildings there and for future and current cargo
						*onestop=not only to Tennessee but only to one building
						*onedate=not only to one building, but either current or future


						IF EOF() 
						SELECT wolog
						CONTINUE
						LOOP
						ENDIF

						
						*start the collection of data from manifest at this record
						STORE RECNO() TO trec
						
						
						STORE DELLOC TO TDELLOC
						
						


						IF tdelloc='WD'
						STORE 'WD' TO tdelloc
						ENDIF
						
						IF !tdelloc='TN'
						STORE .f. TO onestop
						STORE .f. TO onedate
						ENDIF

						STORE EVENTCODE TO TEVENTCODE
								DO WHILE orig_wo=TWO AND !EOF()
									IF DELLOC=WHICHDELLOC OR DELLOC=WHICHDELLOC2
									STORE .T. TO SOMETHISDC
									ENDIF
									IF whichdelloc='TN'
										IF !delloc=whichdelloc 
										STORE .f. TO onestop
										STORE .f. TO onedate
										STORE .f. TO justonedc
										endif
									else
										IF !delloc=whichdelloc or !DELLOC=tdelloc
										STORE .f. TO onestop
										STORE .f. TO onedate
										STORE .f. TO justonedc
										ENDIF
									endif

									IF delloc<>tdelloc
									STORE .f. TO onestop
									ENDIF

									IF eventcode<>teventcode 
									STORE .f. TO onedate
									ENDIF




								SKIP
								
								ENDDO

			
		ENDIF  &&either Nine West or not											

*somethisdc=if there is any of this DC in this work order begin the process of collecing the cargo and revenue data, otherwise skip it
IF SOMETHISDC 



IF !nineshpt

*for other than Nine West Data:
*collect the cargo data first
*The four categories are 
*for Tennessee: 			tinvtype=1 (when the inbound shipment is for one building (TN1 or TN2) and only current or future (not both)
*							tinvtype=2 (when the inbound shipment is mixed for two buildings or mixed future and current
*for all DCs				tinvtype=3 (when the inbound shipment has only one DC Destination DC (TN,VA,AL,Gilbert,WD)
*for all DCs 				tinvtype=4 (when the inbound shipment has mixed DCs)


*For tinvtype 4 (mixed DCs) you may need to prorate the Pickup charge to apply only to the portion for that DC, so this is done
*by obtaining the total weight of the inbound (allweight) and the weight of the specific DC (thiswt)

			STORE 0 TO allweight,thiswt
			

			
			GOTO trec
					DO WHILE orig_wo=TWO AND !EOF()
					allweight=allweight+weight
						DO case

						case justonedc AND onedate AND onestop

						thiswt=thiswt+weight
						STORE 1 TO tinvtype
						
							IF delloc=whichdelloc OR DELLOC=whichdelloc2 

							IF !qty_type='GOH'
							onedconedatectn=onedconedatectn+quantity
							onedconedatewt=onedconedatewt+weight
							onedconedatecbm=onedconedatecbm+cbm
							else
							onedconedategoh=onedconedategoh+quantity
							onedconedategohwt=onedconedategohwt+weight
							onedconedategohcbm=onedconedategohcbm+cbm
							ENDIF
							ENDIF


						case justonedc AND onestop AND !onedate

						thiswt=thiswt+weight										
						STORE 2 TO tinvtype
						IF delloc=whichdelloc OR DELLOC=whichdelloc2 

						IF !qty_type='GOH'
						onedc2datesctn=onedc2datesctn+quantity
						onedc2dateswt=onedc2dateswt+weight
						onedc2datescbm=onedc2datescbm+cbm
						else
						onedc2datesgoh=onedc2datesgoh+quantity
						onedc2datesgohwt=onedc2datesgohwt+weight
						onedc2datesgohcbm=onedc2datesgohcbm+cbm
						ENDIF
						ENDIF

						case justonedc
					
						
						STORE 3 TO tinvtype
						thiswt=thiswt+weight						

						IF delloc=whichdelloc OR DELLOC=whichdelloc2  

						IF !qty_type='GOH'
						onedcctn=onedcctn+quantity
						onedcwt=onedcwt+weight
						onedccbm=onedccbm+cbm
						
						ELSE
						onedcgoh=onedcgoh+quantity
						onedcgohwt=onedcgohwt+weight
						onedcgohcbm=onedcgohcbm+cbm
						ENDIF
						ENDIF

						OTHERWISE
					

						STORE 4 TO tinvtype
						

						IF delloc=whichdelloc OR DELLOC=whichdelloc2 

						IF !qty_type='GOH'
						mixeddcctn=mixeddcctn+quantity
						mixeddcwt=mixeddcwt+weight
						mixeddccbm=mixeddccbm+cbm
						thiswt=thiswt+weight
				
						ELSE
						mixeddcgoh=mixeddcgoh+quantity
						mixeddcgohwt=mixeddcgohwt+weight
						mixeddcgohcbm=mixeddcgohcbm+cbm
						thiswt=thiswt+weight
						ENDIF
						ENDIF




						ENDCASE

					SKIP
					ENDDO


	ENDIF  &&!nineshpt

				IF tinvtype=1
				onedconedatenum=onedconedatenum+1
				ENDIF
				IF tinvtype=2
				onedc2datesnum=onedc2datesnum+1
				ENDIF
				IF tinvtype=3

				onedcnum=onedcnum+1
				endif
				IF tinvtype=4
				
				mixeddcnum=mixeddcnum+1
				ENDIF

*Now you have the cargo data collected, gather the invoice data for this shipment:


				if xsqlexec("select * from invdet where xwo_num="+transform(two),,,"ar")=0
					xsqlexec("select * from invdet where wo_num="+transform(two),,,"ar")=0
				endif
								
*as we go through the records of the INVDET file, there are instances in which more than one line of FUEL are given for the Work ORder,
*but only one of them applies to this specific DC.  We set up two variables, tfind and tfind1 to a default of 'xxx' to cause them 
*to not be found unless it is established that the weight (tfind) or number of garments (tfind2) of the DC billed for Linehaul or local trucking, 
*is part of the description line of any Fuel Surcharge (otherwise the Fuel surcharges cannot be allocated to the specific DC.


				STORE 'xxx' TO tfind,tfind1


				STORE 0 TO countlbs,fsccountlbs
				STORE .f. TO foundlbs

*create an additional filter to block Gilbert shipments from getting mixed in the billing

				IF !WHICHDC=4
				STORE 'GILBERT' TO ADDFILTER
				ELSE
				STORE 'xxx' TO ADDFILTER
				ENDIF

*When no tfind or tfind1 is determined, the Percent (FSCPCNT) is used times the local pickup charges to get the local FSC
*to properly calcuate this percent, we capture the local pickup charges for the DC in the variables thislocalpuchg1,2,3,4

				STORE 0 TO fscpcnt, thislocalpuchg1,thislocalpuchg2,thislocalpuchg3,thislocalpuchg4



					STORE .F. TO FOUNDAMATCH
					DO WHILE (xwo_num=two OR wo_num=two) AND !EOF()
					STORE .f. TO skipthisrecord									

*this part of the loop calculates "Other" charges, if not local pickup, transload, linehaul or fuel surcharges
*****			

									IF tinvtype=1

										DO case
										CASE XACCOUNTID=1747 AND (AT('MAX',DESCRIPTION)>0 OR AT('PICK UP',DESCRIPTION)>0 OR AT('AIRPORT',DESCRIPTION)>0) AND GLCODE=' '
										CASE (company='X' OR company='F') AND glcode='29'
										CASE (company='X' OR company='F') AND !glcode='29'
										CASE &COFILTER AND glcode='15'
										CASE &cofilter AND glcode='29'
										CASE COMPANY=" " AND GLCODE=" " AND AT('FUEL',DESCRIPTION)>0
										CASE &cofilter AND (VAL(glcode)<11 OR VAL(glcode)=41 OR VAL(glcode)=42) AND !AT('PERMIT',description)>0 AND ;
											!AT('STORAGE',description)>0 AND !AT(ADDFILTER,DESCRIPTION)>0

										OTHERWISE
										IF glcode='34' 

										ELSE
										tt1other=tt1other+invdetamt

										ENDIF

										ENDCASE
									ENDIF

									IF tinvtype=2
										DO case
										CASE XACCOUNTID=1747 AND (AT('MAX',DESCRIPTION)>0 OR AT('PICK UP',DESCRIPTION)>0 OR AT('AIRPORT',DESCRIPTION)>0) AND GLCODE=' '										
										CASE (company='X' OR company='F') AND glcode='29'
										CASE (company='X' OR company='F') AND !glcode='29'
										CASE &cofilter AND glcode='15'
										CASE &cofilter AND glcode='29'
										CASE COMPANY=" " AND GLCODE=" " AND AT('FUEL',DESCRIPTION)>0										
										CASE &cofilter AND (VAL(glcode)<11 OR VAL(glcode)=41 OR VAL(glcode)=42) AND !AT('PERMIT',description)>0 AND ;
											!AT('STORAGE',description)>0 AND !AT(ADDFILTER,DESCRIPTION)>0
										OTHERWISE
										IF glcode='34'

										ELSE
										
										tt2other=tt2other+invdetamt
										ENDIF
										ENDCASE
									ENDIF

									IF tinvtype=3
										DO case
										CASE XACCOUNTID=1747 AND (AT('MAX',DESCRIPTION)>0 OR AT('PICK UP',DESCRIPTION)>0 OR AT('AIRPORT',DESCRIPTION)>0) AND GLCODE=' '										
										CASE (company='X' OR company='F') AND glcode='29'
										CASE (company='X' OR company='F') AND !glcode='29'
										CASE &cofilter AND glcode='15'
										CASE &cofilter AND glcode='29'
										CASE COMPANY=" " AND GLCODE=" " AND AT('FUEL',DESCRIPTION)>0										
										CASE &cofilter AND (VAL(glcode)<11 OR VAL(glcode)=41 OR VAL(glcode)=42) AND !AT('PERMIT',description)>0 AND ;
											!AT('STORAGE',description)>0 AND !AT(ADDFILTER,DESCRIPTION)>0
										OTHERWISE
										IF glcode='34'
										ELSE
										tt3other=tt3other+invdetamt
										ENDIF

										ENDCASE
									ENDIF

								IF tinvtype=4
									DO case
									CASE XACCOUNTID=1747 AND (AT('MAX',DESCRIPTION)>0 OR AT('PICK UP',DESCRIPTION)>0 OR AT('AIRPORT',DESCRIPTION)>0) AND GLCODE=' '									
									CASE (company='X' OR company='F') AND glcode='29'
									CASE (company='X' OR company='F') AND !glcode='29'
									CASE &cofilter AND glcode='15'
									CASE &cofilter AND glcode='29'
									CASE COMPANY=" " AND GLCODE=" " AND AT('FUEL',DESCRIPTION)>0									
									CASE &cofilter AND (VAL(glcode)<11 OR VAL(glcode)=41 OR VAL(glcode)=42) AND !AT('PERMIT',description)>0 AND ;
									!AT('STORAGE',description)>0 AND !AT(ADDFILTER,DESCRIPTION)>0
									OTHERWISE
									IF glcode='34'

									ELSE
																		
									tt4other=tt4other+invdetamt
									ENDIF

									ENDCASE
								ENDIF

***** &"Other" charges


				*capture the billing on the first line for GOH Air cans 
				IF (AT('LD3',description)>0 OR AT('M1',description)>0 OR AT('GOH BOX',description)>0) AND glcode='07'
											DO CASE
												CASE TINVTYPE=1
												tt1pu=tt1pu+invdetamt
												thislocalpuchg1=thislocalpuchg1+invdetamt
												CASE TINVTYPE=2
												tt2pu=tt2pu+invdetamt
												thislocalpuchg2=thislocalpuchg2+invdetamt
												CASE TINVTYPE=3
												tt3pu=tt3pu+invdetamt
												thislocalpuchg3=thislocalpuchg3+invdetamt
												CASE TINVTYPE=4
												*prorate the pickup charge and the local fuel surcharge for this category because a 
												*full container with mixed DCs cannot be fully charged to only one of the DC groups
												STORE thiswt/allweight TO thisproratepcnt
												tt4pu=tt4pu+(invdetamt*thisproratepcnt)
												thislocalpuchg4=thislocalpuchg4+invdetamt
												ENDCASE
				
				ENDIF
				
					
					
					
					DO case

							*IF YOU HAVE A DELIVERY LOCATION THAT IS NOT THE ONE YOU ARE TABULATING, SKIP down to the next one
							*****
							STORE .f. TO skipthisrecord
							CASE AT('DELIV',DESCRIPTION)>0 AND AT(WHICHDESC,DESCRIPTION)=0 AND INVDETAMT=0 AND !(TTYPE='A' AND WOLOG.ACCOUNTID=1747 AND WOLOG.OFFICE='N')
							STORE .t. TO skipthisrecord
							SKIP

							DO WHILE (XWO_NUM=TWO OR WO_NUM=TWO) AND AT('DELIV',DESCRIPTION)=0 AND AT('ARRIVAL',DESCRIPTION)=0 AND !EOF()
							SKIP
							ENDDO
							SKIP -1
							
							
							*****
			
			
							*if the first line of billing is for Local Pickup (for full containers where the pickup applies to all the DCs 
							*in the shipment), capture it here before the "Delivery to" records:
							CASE ((VAL(glcode)<11 OR VAL(glcode)=41 OR VAL(glcode)=42) AND (&cofilter AND !AT('PERMIT',description)>0 AND ;
							!AT('STORAGE',description)>0 AND !tloose AND !AT(ADDFILTER,DESCRIPTION)>0 AND !skipthisrecord)) OR (WOLOG.ACCOUNTID=1747 AND ;
							(VAL(glcode)<11 OR VAL(glcode)=41 OR VAL(glcode)=42) AND &cofilter)OR (WHICHDC=4 AND ;
							(VAL(glcode)<11 OR VAL(glcode)=41 OR VAL(glcode)=42) AND &cofilter AND tloose)

												DO CASE
												CASE TINVTYPE=1
												tt1pu=tt1pu+invdetamt
												thislocalpuchg1=thislocalpuchg1+invdetamt
												CASE TINVTYPE=2
												tt2pu=tt2pu+invdetamt
												thislocalpuchg2=thislocalpuchg2+invdetamt
												CASE TINVTYPE=3
												tt3pu=tt3pu+invdetamt
												thislocalpuchg3=thislocalpuchg3+invdetamt


												CASE TINVTYPE=4
												*prorate the pickup charge and the local fuel surcharge for this category because a 
												*full container with mixed DCs cannot be fully charged to only one of the DC groups
												STORE thiswt/allweight TO thisproratepcnt
												tt4pu=tt4pu+(invdetamt*thisproratepcnt)
												thislocalpuchg4=thislocalpuchg4+invdetamt
												ENDCASE

							*For shipments where only local pickup is billed (Gilbert), sorting is outside the "Deliver To" loop.
							
							IF AT('Sorting',description)>0
													DO CASE
													CASE TINVTYPE=1
													tt1SORTING=tt1SORTING+invdetamt
													CASE TINVTYPE=2
													tt2SORTING=tt2SORTING+invdetamt	
													CASE TINVTYPE=3
													tt3SORTING=tt3SORTING+invdetamt	
													CASE TINVTYPE=4
													tt4SORTING=tt4SORTING+invdetamt	
													ENDCASE
								ENDIF



							*In the INVDET file, when we find the specific part of the invoice that applies to this DC, begin a new loop until
							*you hit another DC or FUEL line

							CASE (AT('DELIV',DESCRIPTION)>0 AND AT(whichdesc,description)>0 AND INVDETAMT=0 AND !skipthisrecord) OR (XACCOUNTID=1747 AND TLOOSE AND TTYPE='A' AND (AT('MAX',DESCRIPTION)>0 OR AT('PICK UP',DESCRIPTION)>0 OR AT('AIRPORT',DESCRIPTION)>0))
								SKIP
								*****************
								DO WHILE (xwo_num=two OR wo_num=two) AND ((AT('DELIV',DESCRIPTION)=0 AND AT('FUEL',description)=0) OR (XACCOUNTID=1747 AND TLOOSE AND TTYPE='A' AND (AT('MAX',DESCRIPTION)>0 OR AT('PICK UP',DESCRIPTION)>0 OR AT('AIRPORT',DESCRIPTION)>0))) AND !EOF()
								
								
								IF (VAL(glcode)<11 OR VAL(glcode)=41 OR VAL(glcode)=42) AND &cofilter AND !AT('PERMIT',description)>0 AND ;
								!AT('STORAGE',description)>0 AND !AT(ADDFILTER,DESCRIPTION)>0   &&AND !xaccountid=1747

							*add the local pickup charge for this DC								
												DO CASE
												CASE TINVTYPE=1
												tt1pu=tt1pu+invdetamt
												thislocalpuchg1=thislocalpuchg1+invdetamt
												CASE TINVTYPE=2
												tt2pu=tt2pu+invdetamt
												thislocalpuchg2=thislocalpuchg2+invdetamt
												CASE TINVTYPE=3
												tt3pu=tt3pu+invdetamt
												thislocalpuchg3=thislocalpuchg3+invdetamt
												CASE TINVTYPE=4
												tt4pu=tt4pu+invdetamt
												thislocalpuchg4=thislocalpuchg4+invdetamt
												ENDCASE
								
								ENDIF
								
								*THE ABOVE LINE WILL WORK EXCEPT FOR New York Air Freight, which is split between JFK and Trucking and to 
								*do this there is a billing line first WITH EMPTY COMPANY
								IF (AT('FUEL',DESCRIPTION)>0 AND AT('LINEHAUL',DESCRIPTION)=0 AND TLOOSE AND TTYPE='A' AND TOFFICE='N' AND EMPTY(GLCODE) AND !AT('PERMIT',description)>0 AND ;
								!AT('STORAGE',description)>0 AND !AT(ADDFILTER,DESCRIPTION)>0) OR (XACCOUNTID=1747 AND TLOOSE AND TTYPE='A' AND &cofilter)
											IF (VAL(glcode)<11 OR VAL(glcode)=41 OR VAL(glcode)=42) AND &cofilter 
															DO CASE
															CASE TINVTYPE=1
															tt1pu=tt1pu+invdetamt
															thislocalpuchg1=thislocalpuchg1+invdetamt
															CASE TINVTYPE=2
															tt2pu=tt2pu+invdetamt
															thislocalpuchg2=thislocalpuchg2+invdetamt
															CASE TINVTYPE=3
															tt3pu=tt3pu+invdetamt
															thislocalpuchg3=thislocalpuchg3+invdetamt
															CASE TINVTYPE=4
															tt4pu=tt4pu+invdetamt
															thislocalpuchg4=thislocalpuchg4+invdetamt
															ENDCASE
											ENDIF
											SKIP
													IF (VAL(glcode)<11 OR VAL(glcode)=41 OR VAL(glcode)=42) AND &cofilter 
															DO CASE
															CASE TINVTYPE=1
															tt1pu=tt1pu+invdetamt
															thislocalpuchg1=thislocalpuchg1+invdetamt
															CASE TINVTYPE=2
															tt2pu=tt2pu+invdetamt
															thislocalpuchg2=thislocalpuchg2+invdetamt
															CASE TINVTYPE=3
															tt3pu=tt3pu+invdetamt
															thislocalpuchg3=thislocalpuchg3+invdetamt
															CASE TINVTYPE=4
															tt4pu=tt4pu+invdetamt
															thislocalpuchg4=thislocalpuchg4+invdetamt
															ENDCASE
											ENDIF														
											
								
								
								
								
								
								
								ENDIF
								
								
								
								
								
								
								

							*add the transload amount for this DC
								
								IF AT('TRANSLOAD',description)>0
													DO CASE
													CASE TINVTYPE=1
													tt1tl=tt1tl+invdetamt
													CASE TINVTYPE=2
													tt2tl=tt2tl+invdetamt	
													CASE TINVTYPE=3
													tt3tl=tt3tl+invdetamt	
													CASE TINVTYPE=4
													tt4tl=tt4tl+invdetamt	
													ENDCASE
								
								
								
									
								
								ENDIF

								IF AT('Sorting',description)>0
													DO CASE
													CASE TINVTYPE=1
													tt1SORTING=tt1SORTING+invdetamt
													CASE TINVTYPE=2
													tt2SORTING=tt2SORTING+invdetamt	
													CASE TINVTYPE=3
													tt3SORTING=tt3SORTING+invdetamt	
													CASE TINVTYPE=4
													tt4SORTING=tt4SORTING+invdetamt	
													ENDCASE
								ENDIF


								


						*if the line is billing linehaul, capture the weight or unit data for later use 

							IF AT('LINEHAUL',description)>0 &&AND AT('FUEL',description)=0
									DO case
									
									case AT('SUITS', DESCRIPTION)>0  AND tfind='xxx'
									STORE AT('SUITS',DESCRIPTION) TO tposition
									STORE ALLTRIM(SUBSTR(description,1,tposition-1)) TO tfind1
													DO case
													CASE tinvtype=1 AND AT('SUIT',description)>0
													tsuits1=tsuits1+VAL(tfind1)
													CASE tinvtype=2 AND AT('SUIT',description)>0
													tsuits2=tsuits2+VAL(tfind1)
													CASE tinvtype=3 AND AT('SUIT',description)>0
													tsuits3=tsuits3+VAL(tfind1)
													CASE tinvtype=4 AND AT('SUIT',description)>0
													tsuits4=tsuits4+VAL(tfind1)																																							
													endcase


									
									case AT('GOH',DESCRIPTION)>0 AND tfind='xxx'
									STORE AT('GOH',DESCRIPTION) TO tposition
									STORE ALLTRIM(SUBSTR(description,1,tposition-1)) TO tfind
									

									case AT('CTNS',DESCRIPTION)>0  AND tfind='xxx'
									STORE AT('LBS',DESCRIPTION) TO tposition
									STORE AT('LINEHAUL',DESCRIPTION)+9 TO tposition2
									STORE tposition-tposition2 TO tposition
									STORE ALLTRIM(SUBSTR(description,tposition2,tposition-1)) TO tfind
									

									case AT('SUITS', DESCRIPTION)>0  AND !tfind='xxx'
									STORE AT('SUITS',DESCRIPTION) TO tposition
									STORE VAL(ALLTRIM(SUBSTR(description,1,tposition-1))) TO tfind3
													DO case
													CASE tinvtype=1 AND AT('SUIT',description)>0
													tsuits1=tsuits1+VAL(tfind3)
													CASE tinvtype=2 AND AT('SUIT',description)>0
													tsuits2=tsuits2+VAL(tfind3)
													CASE tinvtype=3 AND AT('SUIT',description)>0
													tsuits3=tsuits3+VAL(tfind3)
													CASE tinvtype=4 AND AT('SUIT',description)>0
													tsuits4=tsuits4+VAL(tfind3)																																							
													endcase

									
									
									
									
									STORE ALLTRIM(STR(tfind3+VAL(tfind1))) TO tfind1
									STORE VAL(tfind1)+VAL(tfind) TO tfind
									STORE ALLTRIM(STR(tfind)) TO tfind
									

									case AT('GOH',DESCRIPTION)>0 AND !tfind='xxx'
									STORE AT('GOH',DESCRIPTION) TO tposition
									STORE VAL(ALLTRIM(SUBSTR(description,1,tposition-1))) TO tfind3
									STORE ALLTRIM(STR(tfind3+VAL(tfind))) TO tfind
									

									case AT('CTNS',DESCRIPTION)>0  AND !tfind='xxx'
									STORE AT('LBS',DESCRIPTION) TO tposition
									STORE AT('LINEHAUL',DESCRIPTION)+9 TO tposition2
									STORE tposition-tposition2 TO tposition
									STORE VAL(ALLTRIM(SUBSTR(description,tposition2,tposition-1))) TO tfind3
									STORE ALLTRIM(STR(tfind3+VAL(tfind))) TO tfind									
									


									
									ENDCASE
									
							ENDIF
								
						*add the linehaul amount for this DC
							IF AT('LINEHAUL',description)>0 AND AT('FUEL',description)=0
													DO case
													CASE TINVTYPE=1
													tt1lh=tt1lh+invdetamt
													CASE TINVTYPE=2
													tt2lh=tt2lh+invdetamt	
													CASE TINVTYPE=3
													tt3lh=tt3lh+invdetamt
													CASE TINVTYPE=4
													tt4lh=tt4lh+invdetamt
													ENDCASE
									

								ENDIF


							SKIP
							ENDDO
							SKIP -1
							STORE .t. TO skipthisrecord
							*****************
	
	
						ENDCASE

	
					**Fuel Surcharges are generally billed at the end of the various billings for multiple DCs so you are outside the DC loop now
			

					STORE "xx" TO TCOUNTLBS
						
						*Get the percent of fuel in case it is needed later

						IF AT('%',description)>0 AND AT('FUEL',description)>0 AND (&cofilter OR toffice='N') AND tloose  AND !skipthisrecord &&AND (tinvtype=3 OR tinvtype=4)
						
						STORE AT('-',description) TO tbeginmark
						STORE AT('%',description) TO tendmark
						STORE tendmark-tbeginmark TO numchars
						STORE VAL(SUBSTR(description,tbeginmark+1,numchars)) TO fscpcnt
						ENDIF
						
						*Local Fuel Surcharge for a full container is here
						*for tinvtype=4 (multiple DCs), prorate it
						IF AT('FUEL',description)>0 AND &cofilter AND !tloose AND !skipthisrecord
						STORE .t. TO foundlbs
										DO case
										CASE tinvtype=1
										tt1lfsc=tt1lfsc+invdetamt
										CASE tinvtype=2
										tt2lfsc=tt2lfsc+invdetamt
										CASE tinvtype=3
										tt3lfsc=tt3lfsc+invdetamt
										CASE tinvtype=4
					
										STORE thiswt/allweight TO thisproratepcnt
										tt4lfsc=tt4lfsc+(invdetamt*thisproratepcnt)
										ENDCASE
							
						ENDIF 
						
						
						
						*AIR FREIGHT FROM NEW YORK SPLITS THE LOCAL FUEL SURCHARGE BETWEEN JFK AND TRUCKING
						IF TLOOSE AND TTYPE='A' AND TOFFICE='N' AND EMPTY(GLCODE) AND AT('FUEL',DESCRIPTION)>0 AND AT('LINEHAUL',DESCRIPTION)=0 AND !skipthisrecord
						SKIP
								IF COMPANY='J' OR COMPANY='I' OR COMPANY='T'
								STORE .F. TO FOUNDLBS
								STORE .t. TO foundlbs
												DO case
												CASE tinvtype=1
												tt1lfsc=tt1lfsc+invdetamt
												CASE tinvtype=2
												tt2lfsc=tt2lfsc+invdetamt
												CASE tinvtype=3
												tt3lfsc=tt3lfsc+invdetamt
												CASE tinvtype=4
												ENDCASE					
								SKIP
										IF COMPANY='J' OR COMPANY='I' OR COMPANY='T'
														DO case
														CASE tinvtype=1
														tt1lfsc=tt1lfsc+invdetamt
														CASE tinvtype=2
														tt2lfsc=tt2lfsc+invdetamt
														CASE tinvtype=3
														tt3lfsc=tt3lfsc+invdetamt
														CASE tinvtype=4					
														ENDCASE
										
										ENDIF
								 										
								ENDIF										
						
			
						ENDIF
						
						
						*Local Fuel Surcharges  (ocean and air) are sometimes split by description of
						*pounds or garments and there can be multiple records for a single DC, so we have to add the pounds or units up (countlbs)
							IF AT('FUEL',description)>0 AND AT('LINEHAUL',description)=0 AND (AT('GOH',description)>0 OR AT('LBS',description)>0) AND !&cofilter AND !skipthisrecord
								STORE LEN(ALLTRIM(DESCRIPTION)) TO XX2
								STORE AT(')',DESCRIPTION)+1 TO XX1
								IF AT('GOH',DESCRIPTION)>0
								STORE AT('GOH',DESCRIPTION) TO XX2
								ENDIF
								IF AT('LBS',DESCRIPTION)>0
								STORE AT('LBS',DESCRIPTION) TO XX2
								ENDIF
								STORE XX2-XX1 TO XX3
						
								STORE VAL(SUBSTR(description,XX1,XX3)) TO tlbs
								countlbs=countlbs+tlbs
								STORE ALLTRIM(STR(COUNTLBS,10,0)) TO TCOUNTLBS
							ENDIF	

						

						*if we find a Local Fuel surcharge record for local fuel with the total pounds or units, capture it
						IF AT('FUEL',description)>0 AND AT('LINEHAUL',description)=0 AND (AT(tfind,description)>0 OR AT(tfind1,description)>0 OR AT(TCOUNTLBS,DESCRIPTION)>0 ) AND &cofilter AND tloose  AND !skipthisrecord
						STORE .t. TO foundlbs

										DO case
											CASE TINVTYPE=1
											tt1lfsc=tt1lfsc+invdetamt
											CASE TINVTYPE=2
											tt2lfsc=tt2lfsc+invdetamt
											CASE TINVTYPE=3
											tt3lfsc=tt3lfsc+invdetamt
											CASE TINVTYPE=4
											tt4lfsc=tt4lfsc+invdetamt
											ENDCASE
							
						ENDIF 

						*Here we capture Linehaul Fuel if it matches the weight or unit totals for tfind or tfind1
						*THIS IS WHERE THE LINEHAUL FUEL LBS OR UNITS MATCHES THE PREVIOUS RECORD (TAKEN FROM THE LINEHAUL LINE) FOR THAT DC
						
						
						
											
						*AT TIMES THERE ARE SEVERAL LINEHAUL FSC RECORDS THAT ADD UP TO THE TOTAL FOR TFIND OR TFIND2.  IN ORDER TO ADD THEM, YOU NEED
						*TO KEEP TRACK OF THEM BY WEIGHT, UNITS, AND INVDETAMT  - FOR THE FIRST TWO RECORDS, THEN THREE (AND THE SECOND/THIRD) ETC.
						*THE BEST WAY IS TO START A LOOP AT THE FIRST RECORD OF ALL THE LH FUEL SURCHARGES.
						*STARTING AT THE FIRST RECORD, BEGIN ADDING AND LOOK FOR A MATCH TO THE LAST RECORD.
						*THEN START AT THE SECOND RECORD AND REPEAT, UNTIL YOU HIT THE LAST RECORD.
						
						STORE 0 TO ADDLBS,ADDUNITS,ADDBILLING
						STORE RECNO() TO STARTREC

						STORE 'ZZZ' TO XTFIND,XTFIND1
						STORE IIF(TFIND='xxx',0,VAL(tfind))+IIF(tfind1='xxx',0,VAL(tfind1)) TO tfind4
						
						IF tfind4=0
						STORE "xxx" TO tfind4
						else
						STORE ALLTRIM(STR(tfind4)) TO tfind4
						ENDIF
						
						STORE .t. TO tfirst
						DO WHILE (AT('FUEL',description)>0 OR AT('DOE',description)>0) AND AT('LINEHAUL',description)>0 AND !&cofilter  AND !skipthisrecord AND !FOUNDAMATCH
						IF (AT('FUEL',description)>0 OR AT('DOE',description)>0) AND AT('LINEHAUL',description)>0 AND (AT(tfind,description)>0 OR AT(tfind1,description)>0 OR AT(tfind4,description)>0) AND !&cofilter  AND !skipthisrecord
						STORE .T. TO FOUNDAMATCH

											DO case
											CASE TINVTYPE=1
											tt1lhfsc=tt1lhfsc+invdetamt
											CASE TINVTYPE=2
											tt2lhfsc=tt2lhfsc+invdetamt
											CASE TINVTYPE=3
											tt3lhfsc=tt3lhfsc+invdetamt
											CASE TINVTYPE=4

											tt4lhfsc=tt4lhfsc+invdetamt
											ENDCASE
						EXIT					
						ENDIF
						ADDBILLING=ADDBILLING+INVDETAMT
						*NOW ADD THE LBS OR UNIT DATA TO THE RUNNING TOTAL BEFORE GOING TO THE NEXT RECORD
									DO case
									case AT('SUITS', DESCRIPTION)>0
									STORE AT('SUITS',DESCRIPTION) TO tposition
									STORE VAL(ALLTRIM(SUBSTR(description,1,tposition-1))) TO Xtfind
									ADDUNITS=ADDUNITS+XTFIND
									IF XTFIND>0
									STORE ALLTRIM(STR(ADDUNITS)) TO XTFIND	
									ELSE
									STORE 'ZZZ' TO XTFIND								
									ENDIF
									
									case AT('GOH',DESCRIPTION)>0
									STORE AT('GOH',DESCRIPTION) TO tposition
									STORE VAL(ALLTRIM(SUBSTR(description,1,tposition-1))) TO Xtfind
									ADDUNITS=ADDUNITS+XTFIND
									IF XTFIND>0
									STORE ALLTRIM(STR(ADDUNITS)) TO XTFIND
									ELSE
									STORE 'ZZZ' TO XTFIND								
									ENDIF

									case AT('LBS',DESCRIPTION)>0
									STORE AT('LBS',DESCRIPTION) TO tposition
									STORE AT(')',DESCRIPTION)+2 TO tposition2
									STORE tposition-tposition2 TO tposition
									STORE VAL(ALLTRIM(SUBSTR(description,tposition2,tposition-1))) TO Xtfind1
									ADDLBS=ADDLBS+XTFIND1
									IF XTFIND1>0
									STORE ALLTRIM(STR(ADDLBS)) TO XTFIND1
									ELSE
									STORE 'ZZZ' TO XTFIND1								
									ENDIF

									ENDCASE

						IF ((AT(Xtfind,description)>0 OR AT(Xtfind1,description)>0) OR (xtfind1=tfind OR xtfind=tfind1)) AND !tfirst
						STORE .T. TO FOUNDAMATCH

									DO case
											CASE TINVTYPE=1
											tt1lhfsc=tt1lhfsc+ADDBILLING
											CASE TINVTYPE=2
											tt2lhfsc=tt2lhfsc+ADDBILLING
											CASE TINVTYPE=3
											tt3lhfsc=tt3lhfsc+ADDBILLING
											CASE TINVTYPE=4
											tt4lhfsc=tt4lhfsc+ADDBILLING
										ENDCASE
						EXIT						
						ENDIF
						
						SKIP
						STORE .f. TO tfirst
						*IF YOU ARE PAST ALL THE FUEL SURCHARGE RECORDS WITHOUT A MATCH, BEGIN THE TALLY AGAIN STARTING AT THE SECOND RECORD, ETC.
						IF (AT('FUEL',description)=0 OR AT('DOE',description)=0) AND AT('LINEHAUL',description)=0 AND !FOUNDAMATCH
						GOTO STARTREC
						SKIP
						IF (AT('FUEL',description)>0 OR AT('DOE',description)>0) AND AT('LINEHAUL',description)>0 AND (AT(tfind,description)>0 OR AT(tfind1,description)>0) AND !&cofilter  AND !skipthisrecord						
						STORE 0 TO ADDLBS,ADDUNITS,ADDBILLING
						STORE RECNO() TO STARTREC
						STORE .F. TO FOUNDAMATCH
						ELSE
						EXIT
						ENDIF
						ENDIF
						
																		
						ENDDO
						

						




						

						

					SKIP
					ENDDO



*for New York Nine West Shipments, the above programs pickup the Local Pickup and FUel charged to Maersk (under acct 1747), but
*there is an additional autobilling of the same work orders with a group total that has to be added
IF nineshpt
SELECT invdet
SET ORDER TO wo_num
SEEK two
STORE invnum TO tinvnum
SET ORDER TO invnum
SEEK tinvnum
STORE 0 TO eachcontainer
DO WHILE invnum=tinvnum AND !EOF()
IF AT('MAERSK PD',description)>0 AND AT('/',description)>0 AND (glcode='41' OR GLCODE='05') AND invdetamt>0
STORE AT('MAERSK PD:',description)+11 TO startpos
STORE AT('/',description) TO endpos
STORE endpos-startpos TO tlength
STORE VAL(SUBSTR(description,startpos,tlength)) TO ninecontainers
STORE ROUND(invdetamt/ninecontainers,2) TO eachcontainer
tt3pu=tt3pu+eachcontainer

ENDIF

IF AT('MAERSK PD FUEL',description)>0 AND AT('/',description)>0 AND  glcode='29'  AND invdetamt>0
STORE AT('MAERSK PD FUEL',description)+19 TO startpos
STORE AT('/CONT',description) TO endpos
IF endpos=0
STORE 2 TO tlength
else
STORE endpos-startpos TO tlength
endif
STORE VAL(SUBSTR(description,startpos+1,tlength)) TO ninecontainers
STORE ROUND(invdetamt/ninecontainers,2) TO eachcontainerfsc
tt3lfsc=tt3lfsc+eachcontainerfsc

ENDIF

SKIP
ENDDO

ENDIF








*If no local fuel surcharges could be determined by adding up total weight and unit for the DC, use the percent to calculate
	IF !foundlbs AND fscpcnt>0

		DO case
		CASE tinvtype=1
		tt1lfsc=tt1lfsc+ ((fscpcnt*thislocalpuchg1)/100)
		CASE tinvtype=2
		tt2lfsc=tt2lfsc+ ((fscpcnt*thislocalpuchg2)/100)
		CASE tinvtype=3
		tt3lfsc=tt3lfsc+ ((fscpcnt*thislocalpuchg3)/100)
		CASE tinvtype=4
		tt4lfsc=tt4lfsc+ ((fscpcnt*thislocalpuchg4)/100)
		ENDCASE
	ENDIF  

ENDIF



*if the work order was billed for linehaul under a trip, capture it and prorate if necessary
SELECT manifest
SET ORDER TO orig_wo
SEEK two
COPY TO H:\fox\temp2 WHILE orig_wo=two
USE H:\fox\temp2 IN 0 EXCLUSIVE
SELECT temp2
DELETE FOR !delloc=whichdelloc OR !delloc=whichdelloc2
INDEX on wo_num TAG wo_num unique
GO top

DO WHILE !EOF()
STORE wo_num TO ttrip

	SELECT invdet
	SET ORDER TO xwo_num
	SEEK ttrip
		IF EOF()
		SET ORDER TO wo_num
		SEEK ttrip
		ENDIF
	IF !EOF()
	*here you found a trip
	*THE PRORATION OF THE LINEHAUL BACK TO THE TOTAL IS DONE BY WEIGHT
	*CAPTURE THE TOTAL WEIGHT OF THE TRIP, THEN THE WEIGHT OF THE PORTION FOR THIS DC

	SELECT MANIFEST
	SET ORDER TO WO_NUM
	SEEK TTRIP
	STORE 0 TO TRIPWT,DCWEIGHT
	DO WHILE WO_NUM=TTRIP AND !EOF()
	TRIPWT=TRIPWT+WEIGHT
	IF (DELLOC=WHICHDELLOC OR DELLOC=WHICHDELLOC2) AND orig_wo=two
	DCWEIGHT=DCWEIGHT+WEIGHT
	ENDIF
	SKIP
	ENDDO
	IF TRIPWT>0
	STORE DCWEIGHT/TRIPWT TO XXPCNT
	ELSE
	STORE 1 TO XXPCNT
	ENDIF
	SET ORDER TO ORIG_WO
	
	
	SELECT INVDET
	DO WHILE (xwo_num=ttrip OR wo_num=ttrip) AND !ttrip=two AND !EOF()
	DO CASE
	
	CASE AT('TRANSLOAD',DESCRIPTION)>0 
	
				DO CASE
				CASE TINVTYPE=1
				tt1tl=tt1tl+(invdetamt*XXPCNT)
				CASE TINVTYPE=2
				tt2tl=tt2tl+(invdetamt*XXPCNT)
				CASE TINVTYPE=3
				tt3tl=tt3tl+(invdetamt*XXPCNT)	
				CASE TINVTYPE=4
				tt4tl=tt4tl+(invdetamt*XXPCNT)
				ENDCASE

	
	CASE (AT('CHARGE',DESCRIPTION)>0 OR AT('TRAILER',DESCRIPTION)>0 OR AT('Linehaul',DESCRIPTION)>0 OR AT('LINEHAUL',DESCRIPTION)>0) AND AT('FUEL',DESCRIPTION)=0 AND AT('TRANSLOAD',DESCRIPTION)=0 AND AT('GILBERT',description)=0
				DO CASE
				CASE TINVTYPE=1
				tt1lh=tt1lh+(invdetamt*XXPCNT)
				CASE TINVTYPE=2
				tt2lh=tt2lh+(invdetamt*XXPCNT)	
				CASE TINVTYPE=3
				tt3lh=tt3lh+(invdetamt*XXPCNT)
				CASE TINVTYPE=4
				tt4lh=tt4lh+(invdetamt*XXPCNT)
				ENDCASE
	
	CASE (AT('FUEL',DESCRIPTION)>0 OR AT('DOE',DESCRIPTION)>0) 
				DO case
				CASE TINVTYPE=1
				tt1lhfsc=tt1lhfsc+(invdetamt*XXPCNT)
				CASE TINVTYPE=2
				tt2lhfsc=tt2lhfsc+(invdetamt*XXPCNT)
				CASE TINVTYPE=3
				tt3lhfsc=tt3lhfsc+(invdetamt*XXPCNT)
				CASE TINVTYPE=4

				tt4lhfsc=tt4lhfsc+(invdetamt*XXPCNT)
				ENDCASE

	
	OTHERWISE
	
	IF !(company='X' AND description='WO') and !COMPANY=' ' 

				DO case
				CASE TINVTYPE=1
				tt1other=tt1other+(invdetamt*XXPCNT)
				CASE TINVTYPE=2
				tt2other=tt2other+(invdetamt*XXPCNT)
				CASE TINVTYPE=3
				IF AT('GILBERT TRAILER RATE',DESCRIPTION)>0 OR AT('DELIVER TO GILBERT',DESCRIPTION)>0 OR AT('@ GILBERT',DESCRIPTION)>0 OR AT('GILBERT DEL',description)>0
				IF tripwt>0 AND dcweight>0
				onedc3delchg=onedc3delchg+(invdetamt*(dcweight/tripwt))
				ELSE
				onedc3delchg=onedc3delchg+invdetamt
				endif
				SKIP
					IF AT('FUEL',description)>0 AND (xwo_num=ttrip OR wo_num=ttrip)
					IF tripwt>0 AND dcweight>0					
					onedc3delfsc=onedc3delfsc+(invdetamt*(dcweight/tripwt))
					ELSE
					onedc3delfsc=onedc3delfsc+invdetamt
					endif
					ELSE
					skip-1
					ENDIF
				else

				tt3other=tt3other+(invdetamt*XXPCNT)

				endif
				CASE TINVTYPE=4
				IF AT('GILBERT TRAILER RATE',DESCRIPTION)>0 OR AT('DELIVER TO GILBERT',DESCRIPTION)>0 OR AT('@ GILBERT',DESCRIPTION)>0 OR AT('GILBERT DEL',description)>0
				IF tripwt>0 AND dcweight>0
				onedc4delchg=onedc4delchg+(invdetamt*(dcweight/tripwt))
				ELSE
				onedc4delchg=onedc4delchg+invdetamt
				endif
				SKIP
					IF AT('FUEL',description)>0 AND (xwo_num=ttrip OR wo_num=ttrip)
					IF tripwt>0 AND dcweight>0					
					onedc4delfsc=onedc4delfsc+(invdetamt*(dcweight/tripwt))
					ELSE
					onedc4delfsc=onedc4delfsc+invdetamt
					endif
					ELSE
					skip-1
					ENDIF
				else
				
				tt4other=tt4other+(invdetamt*XXPCNT)
				ENDIF
				
				ENDCASE
	
	ENDIF
	
	ENDCASE
	
	
	SKIP
	ENDDO
	
	ENDIF
	
	
	SELECT temp2
	SKIP
	ENDDO
	
USE IN TEMP2








SELECT wolog
CONTINUE
enddo









IF WHICHDC=2 AND onedconedatenum>0
SELECT temp
APPEND BLANK
replace start WITH startdate
replace end WITH enddate
replace office WITH toffice
REPLACE DC WITH WHICHDESC
REPLACE GROUP WITH IIF(WHICHDC<5,'JAG','NW')
replace event WITH 'All '+whichdelloc+' - ONE DC - One Event '
replace ctrs WITH onedconedatenum
replace cartons WITH onedconedatectn
replace ctnwt WITH onedconedatewt
replace ccbm WITH onedconedatecbm
replace goh WITH onedconedategoh
replace suits WITH tsuits1
replace gwt WITH onedconedategohwt
replace gcbm WITH onedconedategohcbm
replace lpuchg WITH tt1pu
replace lfsc WITH tt1lfsc
replace tload WITH tt1tl
replace lhaul WITH tt1lh
replace lhfsc WITH tt1lhfsc
replace otherchg WITH tt1other
replace loose WITH tloose
replace type WITH ttype
REPLACE SORTING WITH TT1SORTING
ENDIF

IF WHICHDC<6


		IF WHICHDC=2 AND onedc2datesnum>0
			SELECT TEMP
			APPEND BLANK
			replace start WITH startdate
			replace end WITH enddate
			replace office WITH toffice
			REPLACE DC WITH WHICHDESC
			REPLACE GROUP WITH IIF(WHICHDC<5,'JAG','NW')
			replace event WITH 'All '+whichdelloc+' - ONE DC - Multiple events '
			replace ctrs WITH onedc2datesnum
			replace cartons WITH onedc2datesctn
			replace ctnwt with onedc2dateswt
			replace ccbm WITH onedc2datescbm
			replace goh WITH onedc2datesgoh
			replace suits WITH tsuits2
			replace gwt WITH onedc2datesgohwt
			replace gcbm WITH onedc2datesgohcbm
			replace lpuchg WITH tt2pu
			replace lfsc WITH tt2lfsc
			replace tload WITH tt2tl
			replace lhaul WITH tt2lh
			replace lhfsc WITH tt2lhfsc
			replace otherchg WITH tt2other
			replace loose WITH tloose
			replace type WITH ttype
			REPLACE SORTING WITH TT2SORTING			
		ENDIF


		IF onedcnum>0
		SELECT TEMP
		APPEND BLANK
		replace start WITH startdate
		replace end WITH enddate
		replace office WITH toffice
		REPLACE DC WITH WHICHDESC
		REPLACE GROUP WITH IIF(WHICHDC<5,'JAG','NW')
		IF WHICHDC=2

		replace event WITH 'All '+whichdelloc+' - Mixed Bldgs/events '
		ELSE
		IF WHICHDELLOC='TP'
		replace event WITH 'All GILBERT'		
		ELSE
		replace event WITH 'All '+whichdelloc
		ENDIF
		ENDIF
		replace ctrs WITH onedcnum
		replace cartons WITH onedcctn
		replace ctnwt with onedcwt
		replace ccbm WITH onedccbm
		replace goh WITH onedcgoh
		replace suits WITH tsuits3
		replace gwt WITH onedcgohwt
		replace gcbm WITH onedcgohcbm
		replace lpuchg WITH tt3pu
		replace lfsc WITH tt3lfsc
		replace tload WITH tt3tl
		replace lhaul WITH tt3lh
		replace lhfsc WITH tt3lhfsc
		replace otherchg WITH tt3other
		replace loose WITH tloose
		replace type WITH ttype
		REPLACE SORTING WITH TT3SORTING	
		replace ldelchg WITH onedc3delchg
		replace ldelfsc WITH onedc3delfsc
		ENDIF
		
		IF mixeddcctn>0

		SELECT TEMP
		APPEND BLANK
		replace start WITH startdate
		replace end WITH enddate
		replace office WITH toffice
		REPLACE DC WITH WHICHDESC
		REPLACE GROUP WITH IIF(WHICHDC<5,'JAG','NW')
		IF WHICHDELLOC='TP'
		replace event WITH 'Mixed GILBERT and other destinations '		
		ELSE
		replace event WITH 'Mixed '+whichdelloc+' and other destinations '
		ENDIF
		replace ctrs WITH mixeddcnum
		replace cartons WITH mixeddcctn
		replace ctnwt with mixeddcwt
		replace ccbm WITH mixeddccbm
		replace goh WITH mixeddcgoh
		replace suits WITH tsuits4
		replace gwt WITH mixeddcgohwt
		replace gcbm WITH mixeddcgohcbm
		replace lpuchg WITH tt4pu
		replace lfsc WITH tt4lfsc
		replace tload WITH tt4tl
		replace lhaul WITH tt4lh
		replace lhfsc WITH tt4lhfsc
		replace otherchg WITH tt4other
		replace loose WITH tloose
		replace type WITH ttype
		REPLACE SORTING WITH TT4SORTING	
		replace ldelchg WITH onedc4delchg
		replace ldelfsc WITH onedc4delfsc
		
		

		endif

ENDIF  &&somethisdc



looseorfull=looseorfull+1
enddo

whichmode=whichmode+1
enddo




whichoffice=whichoffice+1
enddo
whichdc=whichdc+1
enddo

SELECT temp

WAIT clear


****************************************************
*****************************************************
*****************************************************
*****************************************************
*****************************************************
*now the excel spreadsheet


WAIT "Please wait while the Excel Spreadsheet is populated..." WINDOW AT 20,60 nowait

oexcel=createobject("excel.application")
oworkbook=oexcel.workbooks.open("f:\ex\exdata\FMI-JAG Report.xls")






STORE 2 TO tnum
DO WHILE tnum<5
STORE 16 TO i
	DO case
	CASE tnum=2
	osheet1=oworkbook.worksheets[2]
	STORE 'C' TO toffice

	CASE tnum=3
	osheet1=oworkbook.worksheets[3]
	STORE 'N' TO toffice

	CASE tnum=4
	osheet1=oworkbook.worksheets[4]
	STORE 'M' TO toffice

	ENDCASE

store 'osheet1' to whichsheet
STORE "C" TO tcolumn

with &whichsheet
.range("A4").value=tFORTHEPERIOD
endwith


*the Lane=TN,VA,TX,WD,Gilbert
STORE 1 TO tlane
	DO WHILE tlane<6
	DO case
	CASE tlane=1
	STORE 'WEST DEPTFORD' TO whichdc
	CASE tlane=2
	STORE 'TENNESSEE' TO whichdc
	CASE tlane=3
	STORE 'VIRGINIA' TO whichdc
	CASE tlane=4
	STORE 'EL PASO' TO whichdc	
	CASE tlane=5
	STORE 'GILBERT' TO whichdc	
	ENDCASE
	
	
	STORE 0 TO tsorting,tother	
	*ttype=Ocean or Air
	STORE 1 TO ttype
		DO WHILE ttype<3
		IF ttype=1
		STORE 'O' TO whichtype
		ELSE
		STORE 'A' TO whichtype
		ENDIF
		
		
		
		*looseorfull=Loose or Full container
		STORE 1 TO looseorfull
			DO WHILE looseorfull<3
			IF looseorfull=1
			STORE .F. TO whichloose
			ELSE
			STORE .t. TO whichloose
			endif
			
			
			*tinvtype=1,2,3,4

			STORE 1 TO tinvtype

			DO WHILE tinvtype<5
				DO case
				CASE tinvtype=1 
				LOCATE FOR office=toffice AND dc=whichdc AND loose=whichloose AND type=whichtype AND AT('and other destinations',event)>0								
				CASE tinvtype=2
				LOCATE FOR office=toffice AND dc=whichdc AND loose=whichloose AND type=whichtype AND (AT('Mixed Bldgs',event)>0	OR (AT('All',event)>0 AND !DC='TENN'))				
				CASE tinvtype=3 
				LOCATE FOR office=toffice AND dc=whichdc AND loose=whichloose AND type=whichtype AND AT('ONE DC - Multiple event',event)>0
				CASE tinvtype=4 
				LOCATE FOR office=toffice AND dc=whichdc AND loose=whichloose AND type=whichtype AND AT('ONE DC - One Event',event)>0
				ENDCASE
		
			tsorting=tsorting+sorting
			tother=tother+otherchg
		
					*run from column C to column U

					DO WHILE ASC(tcolumn)<85
					STORE whichsheet+'.range("'+tcolumn+TRANSFORM(i)+'").value' TO tfield
						DO case
						CASE ASC(tcolumn)=67
						&tfield=TRANSFORM(temp.ctrs)
						CASE ASC(tcolumn)=68
						&tfield=TRANSFORM(temp.cartons)
						CASE ASC(tcolumn)=69
						&tfield=TRANSFORM(temp.ctnwt)
						CASE ASC(tcolumn)=70
						&tfield=TRANSFORM(temp.ccbm)
						CASE ASC(tcolumn)=71
						&tfield=TRANSFORM(temp.goh)
						CASE ASC(tcolumn)=72
						&tfield=TRANSFORM(temp.gwt)
						CASE ASC(tcolumn)=73
						&tfield=TRANSFORM(temp.gcbm)
						CASE ASC(tcolumn)=74
						&tfield=TRANSFORM(temp.suits)
						CASE ASC(tcolumn)=76
						&tfield=TRANSFORM(temp.lpuchg)
						CASE ASC(tcolumn)=77
						&tfield=TRANSFORM(temp.lfsc)
						CASE ASC(tcolumn)=78
						&tfield=TRANSFORM(temp.ldelchg)
						CASE ASC(tcolumn)=79
						&tfield=TRANSFORM(temp.ldelfsc)
						CASE ASC(tcolumn)=80
						&tfield=TRANSFORM(temp.tload)
						CASE ASC(tcolumn)=81
						&tfield=TRANSFORM(temp.lhaul)
						CASE ASC(tcolumn)=82
						&tfield=TRANSFORM(temp.lhfsc)
						ENDCASE



					STORE ASC(tcolumn)+1 TO tasc
					IF tasc=75
					tasc=tasc+1
					ENDIF

					STORE CHR(tasc) TO tcolumn
					ENDDO  &&runs from column E to column T
					
		
					
					
					
					
					DO case
							
					CASE INLIST(i,16,19,22,25,26,34,35,36,39,40,41,44,45,46,49,50)
					i=i+1
					CASE INLIST(i,51,52,60,63,66,69,70,78,81,84,87,88,96,99,102,105,106)
					i=i+1
					otherwise
					i=i+2
					ENDCASE
					
					IF i=27 OR i=53 OR i=71 OR i=89 OR i=107
					STORE "S" TO tcolumn
					STORE whichsheet+'.range("'+tcolumn+TRANSFORM(i)+'").value' TO tfield
					&tfield=TRANSFORM(tsorting)
					STORE "T" TO tcolumn
					STORE whichsheet+'.range("'+tcolumn+TRANSFORM(i)+'").value' TO tfield					
					&tfield=TRANSFORM(tother)
					IF i=107
					i=i+5
					else
					i=i+7
					ENDIF
					
					ENDIF
					
					
				STORE "C" TO tcolumn
					*the next row on the page

				tinvtype=tinvtype+1
				IF tinvtype=3 AND !whichdc='TENNESSEE'
				EXIT
				ENDIF
				
				ENDDO
				
				
					
				
			looseorfull=looseorfull+1	
			ENDDO
		ttype=ttype+1	
		
		ENDDO
					
					
		
					
					
	tlane=tlane+1		
	ENDDO
tnum=tnum+1
ENDDO  &&TNUM



* SAVE
DELETE FILE "h:\fox\FMI-JAG Report.xls"
xfile="h:\fox\FMI-JAG Report.xls"

oworkbook.saveas(xfile)
CLEAR
WAIT clear
oexcel.visible=.t.


*******************************************************************************************************
USE IN temp
USE IN wolog 
USE IN invdet 
USE IN manifest 

