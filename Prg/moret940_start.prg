*!* MORET 940 (EDI VERSION)
*!* Transition to X12, 10.31.2009

PARAMETERS cOfficeUsed
CLOSE DATABASES ALL
*!* Set and initialize public variables
PUBLIC xFile,archivefile,cXdate1,dXdate2,cOffice,lTesting,cUseDir,cCustName,nTotPT,cMailName,nSegCnt
PUBLIC cDelimiter,cTranslateOption,EmailCommentStr,LogCommentStr,nAcctNum,cAcctNum,lLoop,lDoPkg,nxptpts,nptpts
PUBLIC chgdate,ptid,ptctr,nPTQty,lTesting,lTestUploaddet,cOfficename,cConsignee,cStoreNum,cTotPT,cFilename
PUBLIC cPickticket_num_start,cPickticket_num_end,EmailCommentStr,cLoadID,lcPath,cUseFolder,cShip_ref,tfrom,nFileSize
PUBLIC tsendto,tcc,tsendtotest,tcctest,lcArchivepath,nXPTQty,NormalExit,cFilemask,cOutState,lBrowFiles,cMod,cISA_Num
PUBLIC cMessage,fa997file,m.printstuff,lTestImport,cTransfer,cCoCode,cCoNum,cDiv,lMix,tsendtoerr,tccerr,lEmail,lLoadSQL
PUBLIC ARRAY a856(1)
SET EXCLUSIVE OFF
CaptionStr = "MORET 940 (EDI VERSION)"
STORE CaptionStr TO _SCREEN.CAPTION

TRY
	DO m:\dev\prg\_setvars WITH .T.

	lTesting = .f.
	lTestFileIn = lTesting  && If .t., uses file(s) in subfolder "EDITEST"
	lTestImport = lTesting
	lTestUploaddet = lTesting
	lOverRideBusy = lTesting
	lBrowFiles = lTesting
	lEmail = !lTesting

*	lOverRideBusy = .t.
*	lEmail = .F.
*	lBrowFiles = .t.
*	SET ASSERTS OFF

	_SCREEN.WINDOWSTATE=IIF(lTesting OR lOverRideBusy,2,1)

	nAcctNum = 5453
	cCoNum = '03'
	cOfficeUsed = IIF(VARTYPE(cOfficeUsed)="L","C",cOfficeUsed)
	cDiv = ""
	cFilename = ""
	STORE 0 TO nSegCnt,nxptpts,nptpts
	tfrom ="Toll EDI Operations <toll-edi-ops@tollgroup.com>"
	lMix = .F.

	SELECT 0
	USE F:\edirouting\ftpsetup SHARED
	cTransfer = "940-MORET"
	LOCATE FOR ftpsetup.transfer =  cTransfer
	IF FOUND()
		IF !lTesting AND !lOverRideBusy
			IF !ftpsetup.chkbusy
				REPLACE chkbusy WITH .T.,trig_time WITH DATETIME()  FOR ftpsetup.transfer = cTransfer
			ELSE
				WAIT WINDOW "Transfer locked...can't start new process" TIMEOUT 2
				NormalExit = .T.
				THROW
			ENDIF
		ELSE
			REPLACE chkbusy WITH .T. FOR ftpsetup.transfer = cTransfer
		ENDIF
		USE IN ftpsetup
	ENDIF

	cOfficeUsed = IIF(INLIST(cOfficeUsed,"N","I"),"I",cOfficeUsed)
	IF lTesting
		CLOSE DATABASES ALL
		CLEAR
		cOfficeUsed = "C"
		WAIT WINDOW "This is a OFFICE "+cOfficeUsed+" TEST upload into F:\WHP" TIMEOUT 2
	ELSE
		IF VARTYPE(cOfficeUsed) = "L"
			cOfficeUsed = "C"
		ENDIF
		cOfficeUsed = UPPER(cOfficeUsed)
	ENDIF

	STORE cOfficeUsed TO cOffice
	IF lTesting OR lTestImport
		cUseFolder = "F:\WHP\WHDATA\"
	ELSE
		xReturn = "XXX"
		DO m:\dev\prg\wf_alt WITH cOffice,nAcctNum
		cUseFolder = UPPER(xReturn)
	ENDIF

	DO CASE
		CASE cOffice = "I"
			cOutState = "-NJ"
			cOfficename = "New Jersey"
		CASE cOffice = "M"
			cOutState = "-FL"
			cOfficename = "Florida"
		CASE cOffice = "L"
			cOutState = "-ML"
			cOfficename = "Mira Loma"
		CASE cOffice = "O"
			cOutState = "-ON"
			cOfficename = "Ontario"
		CASE cOffice = "X"
			cOutState = "-CR"
			cOfficename = "Carson"
		OTHERWISE
			cOutState = "-SP"
			cOfficename = "San Pedro"
	ENDCASE
	cMod = IIF(cOffice = "C","7",cOffice)

	IF lTesting
		goffice ="P"
	ELSE
		goffice = cMod
	ENDIF

	lLoadSQL = .t.

	STORE "" TO EmailCommentStr,cLoadID,cConsignee
	NormalExit = .F.
	cMessage = " "

	m.adddt = DATETIME()
	m.addby = "FMI-PROC"
	m.addproc = "MORET940"
	cCustName = "MORET"
	cMailName = "Moret"
	cAlias = "MORET"
	cDelimiter = "*"
	cTranslateOption ="CR_TILDE"
	cUseDir = "m:\dev\prg\"+cCustName+"940_"

	xFile = ""
	cXdate1 = ""
	dXdate2 = DATE()

	cAcctNum = ALLTRIM(STR(nAcctNum))
	LogCommentStr = ""
	cShip_ref = ""

	ASSERT .F. MESSAGE "In  mail data subroutine"
	SELECT 0
	USE F:\3pl\DATA\mailmaster ALIAS mm SHARED
	LOCATE FOR mm.edi_type = "940" AND mm.GROUP = "MORET" AND mm.office = cOfficeUsed
	IF FOUND()
		STORE TRIM(mm.acctname) TO cAccountName
		STORE TRIM(mm.basepath) TO lcPath
		WAIT WINDOW "Path is: "+lcPath TIMEOUT 2
		STORE TRIM(mm.archpath) TO lcArchivepath
		cFilemask = "*.*"
		STORE TRIM(IIF(mm.use_alt,mm.sendtoalt,mm.sendto)) TO tsendto
		STORE TRIM(IIF(mm.use_alt,mm.ccalt,mm.cc)) TO tcc
		LOCATE
		LOCATE FOR mm.office = "X" AND mm.accountid = 9999
		STORE TRIM(IIF(mm.use_alt,mm.sendtoalt,mm.sendto)) TO tsendtoerr
		STORE TRIM(IIF(mm.use_alt,mm.ccalt,mm.cc)) TO tccerr
		LOCATE
		LOCATE FOR mm.edi_type = "MISC" AND mm.taskname = "GENERAL"
		STORE TRIM(IIF(mm.use_alt,mm.sendtoalt,mm.sendto)) TO tsendtotest
		STORE TRIM(IIF(mm.use_alt,mm.ccalt,mm.cc)) TO tcctest
		USE IN mm
	ELSE
		WAIT WINDOW AT 10,10  "No parameters set for this acct# "+ALLTRIM(STR(nAcctNum))+"  ---> Office "+cOffice TIMEOUT 2
		NormalExit = .T.
		THROW
	ENDIF

	IF lTestFileIn
		lcPath = lcPath+"EDITEST\"
*	cFilemask = (lcPath+"*.*")
	ENDIF

	WAIT CLEAR

	DO (cUseDir+"PROCESS")
	CLOSE DATABASES ALL
	NormalExit = .T.

	IF !lTesting
		SELECT 0
		USE F:\edirouting\ftpsetup SHARED
		REPLACE chkbusy WITH .F. FOR ftpsetup.transfer = cTransfer
		USE IN ftpsetup
	ENDIF

CATCH TO oErr
	IF NormalExit = .F.
		ASSERT .F. MESSAGE "In Error Catch"
		SET STEP ON
		tsendto = tsendtoerr
		tcc = tccerr
		tsubject = cMailName+" 940 Upload Error ("+TRANSFORM(oErr.ERRORNO)+") at "+TTOC(DATETIME())
		tattach  = ""
		tmessage = cMailName+" 940 Upload Error..... Please fix"
		tmessage = tmessage+CHR(13)+cMessage+CHR(13)+CHR(13)
		lcSourceMachine = SYS(0)
		lcSourceProgram = SYS(16)

		tmessage =tmessage+ "Try/CATCH Exeception Message:"+CHR(13)+;
			[  Error: ] + STR(oErr.ERRORNO) +CHR(13)+;
			[  LineNo: ] + STR(oErr.LINENO) +CHR(13)+;
			[  Message: ] + oErr.MESSAGE +CHR(13)+;
			[  Procedure: ] + oErr.PROCEDURE +CHR(13)+;
			[  Details: ] + oErr.DETAILS +CHR(13)+;
			[  StackLevel: ] + STR(oErr.STACKLEVEL) +CHR(13)+;
			[  LineContents: ] + oErr.LINECONTENTS+CHR(13)+;
			[  UserValue: ] + oErr.USERVALUE+CHR(13)+;
			[  Computer:  ] +lcSourceMachine+CHR(13)+;
			[  940 file:  ] +xFile+CHR(13)+;
			[  Program:   ] +lcSourceProgram

		tattach  = ""

		DO FORM m:\dev\FRM\dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
		COPY FILE &xFile TO &archivefile

		IF FILE(archivefile)
			DELETE FILE [&xFile]
		ENDIF
	ELSE
		WAIT WINDOW AT 10,10 "Normal Exit " TIMEOUT 2
*!*			SELECT 0
*!*			USE F:\edirouting\ftpsetup SHARED
*!*			REPLACE chkbusy WITH .F. FOR ftpsetup.transfer = cTransfer
*!*			USE IN ftpsetup
	ENDIF
FINALLY
	CLOSE DATABASES ALL
ENDTRY

