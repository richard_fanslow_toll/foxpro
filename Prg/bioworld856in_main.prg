**********************************************************************************
* Bioworld 856 intake program, derived from Paul's Nautica856inb project, 03.04.2013
***********************************************************************************
* The 856 is structured as
* Shipment
*   Order
*     Item
*        Pack
***********************************************************************************
PUBLIC lProcessOK,lTesting,gContainer,cOffice,nAccountid,cTransfer,NormalExit,cFilename,xfile,lBrowFiles,cMailName,lDoMail,cErrMsg,lDoMix,cVendorName,gOffice
PUBLIC lDoSQL,cWhse,xgMod

CLOSE DATABASES ALL
WAIT WINDOW AT 10,10  "Now setting up Bioworld 856 upload............" TIMEOUT 1
SET tablevalidate TO 0
_SCREEN.CAPTION = "BIOWORLD 856 PROCESS"

TRY
	lTesting = .f.
	lBrowFiles = lTesting
	lOverridebusy =  lTesting
	lDoMail = .T.
*	lBrowFiles = .t.
	lOverridebusy =  .T.
	_screen.WindowState=IIF(lTesting,0,1)	
	DO m:\dev\prg\_setvars WITH .T.
	ON ESCAPE CANCEL
	ON ERROR THROW


	cOffice = "C"
	cMod = IIF(cOffice = "C","5",cOffice)
	gOffice = cMod
	gMasterOffice = cOffice

	lDoSQL = .t.
	cWhse = LOWER("wh"+cMod)

	cOfficename = "Carson 2"
	nAcctNum = 6182
	cTransfer = "856-BIOWORLDIN-CA"
	cMailName = "Bioworld"
	cVendorName = ""
	lDoMix = .F.
	NormalExit = .F.

	IF !lTesting
		SELECT 0
		USE F:\edirouting\ftpsetup SHARED
		LOCATE FOR ftpsetup.transfer =  cTransfer
		IF FOUND()
			IF !lOverridebusy
				IF !ftpsetup.chkbusy
					REPLACE chkbusy WITH .T.,trig_time WITH DATETIME()  FOR ftpsetup.transfer = cTransfer
				ELSE
					WAIT WINDOW "Transfer locked...can't start new process" TIMEOUT 2
					NormalExit = .T.
					THROW
				ENDIF
			ELSE
				REPLACE chkbusy WITH .T. FOR ftpsetup.transfer = cTransfer
			ENDIF
			USE IN ftpsetup
		ENDIF
	ENDIF

	SELECT 0
	USE F:\3pl\DATA\mailmaster ALIAS mm SHARED
	LOCATE FOR mm.edi_type = "856" AND mm.office = cOffice AND mm.accountid = 6182
	IF FOUND()
		STORE TRIM(mm.acctname) TO cAccountName
		STORE TRIM(mm.basepath) TO lcPath
		STORE TRIM(mm.archpath) TO lcArchivepath
		STORE TRIM(IIF(mm.use_alt,mm.sendtoalt,mm.sendto)) TO tsendto
		STORE TRIM(IIF(mm.use_alt,mm.ccalt,mm.cc)) TO tcc
		STORE JUSTFNAME(TRIM(mm.fmask))    TO cFilemask
		STORE mm.CtnAcct        TO lCartonAcct
		LOCATE FOR mm.edi_type = "MISC" AND mm.taskname = "ERRMARIA"
		STORE TRIM(IIF(mm.use_alt,mm.sendtoalt,mm.sendto)) TO tsendtoerr
		STORE TRIM(IIF(mm.use_alt,mm.ccalt,mm.cc)) TO tccerr
		IF lTesting
			STORE TRIM(IIF(mm.use_alt,mm.sendtoalt,mm.sendto)) TO tsendto
			STORE TRIM(IIF(mm.use_alt,mm.ccalt,mm.cc)) TO tcc
		ENDIF
		tsubject= "Packinglist Upload for: "+cMailName+"-"+cOfficename+"  at "+TTOC(DATETIME())
	ELSE
		WAIT WINDOW AT 10,10  "No parameters set for this acct# "+ALLTRIM(STR(Acctnum))+"  ---> Office "+cOffice TIMEOUT 2
		THROW
	ENDIF
	USE IN mm

	xReturn = "XX"
	IF !lTesting
		DO m:\dev\prg\wf_alt WITH cOffice,nAcctNum
		cUseFolder = UPPER(xReturn)
	ELSE
		lcPath = STRTRAN(lcPath,"\856in\","\856test\")
		cUseFolder = "f:\whp\whdata\"
	ENDIF

*	USE (cUseFolder+"whgenpk") IN 0 ALIAS whgenpk
	useca("pl","wh")
	useca("inwolog","wh")

	IF lTesting
		SELECT inwolog
		DELETE FOR accountid = nAcctNum
		SELECT pl
		DELETE FOR accountid = nAcctNum
	ENDIF

	delimchar = "*"
	lcTranOpt= "TILDE"

	lnNum = ADIR(tarray,lcPath+"*.*")

	CD &lcPath
	DELETE FILE foxuser.*

	IF lnNum = 0
		WAIT WINDOW AT 10,10 "    No Bioworld 856's to import.............." TIMEOUT 2
		NormalExit = .T.
		THROW
	ELSE
		WAIT WINDOW AT 10,10 " OK need to import "+TRANSFORM(lnNum)+" File(s)........" TIMEOUT 1
	ENDIF

	FOR thisfile = 1  TO lnNum
		cFilename = tarray[thisfile,1]
		xfile = lcPath+cFilename
		archivefile = lcArchivepath+cFilename
		WAIT WINDOW "Importing file: "+xfile NOWAIT
		IF FILE(xfile)
			xsqlexec("select * from pl where .f.","xpl",,"wh")
			xsqlexec("select * from inwolog where .f.","xinwolog",,"wh")

			SELECT xinwolog
			SCATTER MEMVAR MEMO BLANK
			INDEX ON inwologid TAG inwologid
			SELECT xinwolog
			DELETE ALL
			SELECT xpl
			DELETE ALL

* load the file into the 856 array
			DO m:\dev\prg\createx856a
			SELECT x856
			DO m:\dev\prg\loadedifile WITH xfile,delimchar,lcTranOpt,"BIOWORLD"
			LOCATE

			lProcessOK = .F.
			DO m:\dev\prg\bioworld856in_bkdn WITH cFilename

			IF lProcessOK
				DO m:\dev\prg\bioworld856in_import
			ENDIF

			COPY FILE [&xfile] TO [&archivefile]
			IF lProcessOK AND !lTesting
				IF FILE(archivefile)
					DELETE FILE [&xfile]
				ENDIF
			ENDIF
		ENDIF
	NEXT thisfile

	IF !lTesting
		SELECT 0
		USE F:\edirouting\ftpsetup SHARED
		REPLACE chkbusy WITH .F. FOR ftpsetup.transfer = cTransfer
		USE IN ftpsetup
	ENDIF
	NormalExit = .T.

CATCH TO oErr
	IF NormalExit = .F.
		ASSERT .F. MESSAGE "In Error Catch"
		SET STEP ON
		tsendto = tsendtoerr
		tcc = tccerr
		tsubject = cMailName+" 856 Upload Error ("+TRANSFORM(oErr.ERRORNO)+") at "+TTOC(DATETIME())
		tmessage = cMailName+" 856 Upload Error..... Please fix"
		lcSourceMachine = SYS(0)
		lcSourceProgram = SYS(16)
		IF !EMPTY(cErrMsg)
			tmessage = tmessage+ cErrMsg
		ELSE
			tmessage =tmessage+ "Try/CATCH Exeception Message:"+CHR(13)+;
				[  Error: ] + STR(oErr.ERRORNO) +CHR(13)+;
				[  LineNo: ] + STR(oErr.LINENO) +CHR(13)+;
				[  Message: ] + oErr.MESSAGE +CHR(13)+;
				[  Procedure: ] + oErr.PROCEDURE +CHR(13)+;
				[  Details: ] + oErr.DETAILS +CHR(13)+;
				[  StackLevel: ] + STR(oErr.STACKLEVEL) +CHR(13)+;
				[  LineContents: ] + oErr.LINECONTENTS+CHR(13)+;
				[  UserValue: ] + oErr.USERVALUE+CHR(13)+;
				[  Computer:  ] +lcSourceMachine+CHR(13)+;
				[  856 file:  ] +xfile+CHR(13)+;
				[  Program:   ] +lcSourceProgram
		ENDIF
		tattach  =  " "
		tfrom    ="TOLL EDI Processing Center <toll-edi-ops@tollgroup.com>"
		DO FORM m:\dev\FRM\dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
	ELSE
		WAIT WINDOW AT 10,10 "Normal Exit " TIMEOUT 2
	ENDIF
FINALLY
	ON ERROR
	SET STATUS BAR ON
	CLOSE DATABASES ALL
ENDTRY

