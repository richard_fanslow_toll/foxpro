* gets the mod based on office and accountid for offices N and C
* created dy 3/18/18 to simplify process and fix bug
* exclude tables that do not have an accountid field
* the tables that are excluded have an office field but no mod

lparameters xgmcursor, xgmoffice, xgmaccountid

if !inlist(xgmoffice,"N","C") ;
or inlist(lower(xgmcursor),"ufcoll","vconsdet","cons","conspdf","ufthird","operator","scac","whseloc","modacct","prod","dedemail","ufaddbk","cnee","vupsacctcomm","vinvoice","vinvdet","invpdf","vupcmast","invoice","invdet","editrigger") ;
or vartype(accountid)#"N" or vartype(office)#"C" or vartype(mod)#"C" 	
	return
endif

xfailed=.f.

if !used("xgmmodacct")
	xgmselect=select()
	xsqlexec("select * from modacct","xgmmodacct",,"wh")
	index on office+str(accountid,4) tag offacct
	set order to
	select (xgmselect)
endif

xgmaccountid=xgmaccountid%10000

try
	if seek(xgmoffice+str(xgmaccountid,4),"xgmmodacct","offacct")
		xgmmod=xgmmodacct.mod
	else
		xfailed=.t.
		xgmmod=""
	endif
catch
	xfailed=.t.
endtry

if xfailed and !inlist(lower(xgmcursor),"locchg","info","ackdata","spinfo")		&& sometimes accountid = 0
	emailx("Dyoung@fmiint.com",,"INFO: xgmmod not set in getmod - "+xgmcursor,transform(xgmaccountid)+crlf+xgmoffice+crlf+xgmmod,.t.)
endif

xfailed=.f.
try
	if lower(xgmcursor)="edi_trigger"
		replace office with xgmmod in (xgmcursor)
	else
		replace mod with xgmmod in (xgmcursor)
	endif
catch
	xfailed=.t.
endtry

if xfailed
	emailx("Dyoung@fmiint.com",,"INFO: xgmmod not replaced in getmod - "+xgmcursor,,.t.)
endif
