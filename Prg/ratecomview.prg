* creates VRATECOMCONTROL view - MUST BE RUN IN VFP 8.0 !!

cd m:\dev\prg

set enginebehavior 70
set exclusive off
set safety off

if type("gmilitarytime")="U"
	public gmilitarytime
endif

close databases all
open database f:\watusi\ardata\ar
*open database m:\dev\ardata\ar

CREATE SQL VIEW "VRATECOMCONTROL" AS SELECT ;
	ratecom.rateheadid, ratecomid, acctname, zdate, ratecom.addby, type, comment ;
	FROM ar!ratehead, ar!ratecom ;
	where ratehead.rateheadid=ratecom.rateheadid and &gratecomfilter and !inactive

DBSetProp('VRATECOMCONTROL', 'View', 'UpdateType', 1)
DBSetProp('VRATECOMCONTROL', 'View', 'WhereType', 1)
DBSetProp('VRATECOMCONTROL', 'View', 'FetchMemo', .T.)
DBSetProp('VRATECOMCONTROL', 'View', 'SendUpdates', .T.)
DBSetProp('VRATECOMCONTROL', 'View', 'UseMemoSize', 255)
DBSetProp('VRATECOMCONTROL', 'View', 'FetchSize', 100)
DBSetProp('VRATECOMCONTROL', 'View', 'MaxRecords', -1)
DBSetProp('VRATECOMCONTROL', 'View', 'Tables', 'ar!ratecom')
DBSetProp('VRATECOMCONTROL', 'View', 'Prepared', .F.)
DBSetProp('VRATECOMCONTROL', 'View', 'CompareMemo', .T.)
DBSetProp('VRATECOMCONTROL', 'View', 'FetchAsNeeded', .F.)
DBSetProp('VRATECOMCONTROL', 'View', 'FetchSize', 100)
DBSetProp('VRATECOMCONTROL', 'View', 'Comment', "")
DBSetProp('VRATECOMCONTROL', 'View', 'BatchUpdateCount', 1)
DBSetProp('VRATECOMCONTROL', 'View', 'ShareConnection', .F.)

DBSetProp('VRATECOMCONTROL.ratecomid', 'Field', 'KeyField', .T.)
DBSetProp('VRATECOMCONTROL.ratecomid', 'Field', 'Updatable', .T.)
DBSetProp('VRATECOMCONTROL.rateheadid', 'Field', 'Updatable', .T.)

close databases all 

gunshot()