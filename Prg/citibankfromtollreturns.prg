
* process Positive Pay files sent from CitiBank to us.
* It should be called when files appear in F:\FTPUSERS\CITIBANKIN\FROMTOLL\

* Build EXE as F:\BOA\CITIBANKFROMTOLLRETURNS.exe
*
* 4/27/2018 MB: changed fmiint.com to toll email

LOCAL loCitiFromTollReturns, lnError, lcProcessName

utilsetup("CITIBANKFROMTOLLRETURNS")

DECLARE INTEGER CreateMutex IN Win32API ;
	STRING @lpMutexAttributes, ;
	STRING bInitialOwner, ;
	STRING @lpName

DECLARE INTEGER GetLastError IN Win32API

* if running in EXE check to see if already running
IF _VFP.STARTMODE > 0 THEN
	lcProcessName = "CitiFromTollReturns"
	CreateMutex(0,CHR(1),@lcProcessName)
	lnError = GetLastError()
	IF lnError = 183
		WAIT WINDOW TIMEOUT 5 "This program is already running..."
		schedupdate()
		RETURN .F.
	ENDIF
ENDIF

loCitiFromTollReturns = CREATEOBJECT('CitiFromTollReturns')
loCitiFromTollReturns.MAIN()

schedupdate()

CLOSE DATABASES ALL

RETURN

#DEFINE LOGIT 1
#DEFINE WAITIT 2
#DEFINE NOWAITIT 4
#DEFINE SENDIT 8
#DEFINE LF CHR(10)
#DEFINE CRLF CHR(13) + CHR(10)
#DEFINE RETURN_DATA_MANDATORY .F.
#DEFINE RETURN_DATA_NOT_MANDATORY .T.
#DEFINE CSVFORMAT 6
#DEFINE WRITE_ONLY_UNBUFFERED 11
#DEFINE COMMA ","

DEFINE CLASS CitiFromTollReturns AS CUSTOM

	cProcessName = 'CitiFromTollReturns'

	lTestMode = .F.  && .F. FOR NORMAL OPERATION, .T. TO SUPPRESS ALL BUT DEVELOPER EMAILS AND CHANGE INPUT TABLE, ETC.

	lAutoYield = .T.

	* date time props
	dToday = DATE()
	*dToday = {^2010-07-24}

	cStartTime = TTOC(DATETIME())

	* folder properties
	cInputFolder = ''
	cArchivedFolder = ''
	cConvertedFolder = ''
	cBAIFolder = ''
	c824Folder = ''
	
	cTestInputFolder = 'F:\FTPUSERS\CITIBANKIN\TOLLTEST\'
	cTestArchivedFolder = 'F:\FTPUSERS\CITIBANKIN\TOLLTEST\ARCHIVE\'
	cTestConvertedFolder = 'F:\FTPUSERS\CITIBANKIN\TOLLTEST\CONVERTED\'
	cTestBAIFolder = 'F:\FTPUSERS\CITIBANKIN\TOLLTEST\BAIFILES\'
	cTest824Folder = 'F:\FTPUSERS\CITIBANKIN\TOLLTEST\824FILES\'
	
	cProdInputFolder = 'F:\FTPUSERS\CITIBANKIN\TOLLPROD\'
	cProdArchivedFolder = 'F:\FTPUSERS\CITIBANKIN\TOLLPROD\ARCHIVE\'
	cProdConvertedFolder = 'F:\FTPUSERS\CITIBANKIN\TOLLPROD\CONVERTED\'
	cProdBAIFolder = 'F:\FTPUSERS\CITIBANKIN\TOLLPROD\BAIFILES\'
	cProd824Folder = 'F:\FTPUSERS\CITIBANKIN\TOLLPROD\824FILES\'

	* processing properties
	lArchiveFiles = .T.
	lConvertFiles = .T.
	cErrors = ''
	lBAIFileFound = .F.
	l824FileFound = .F.

	* connection properties
	nSQLHandle = 0

	* wait window properties
	nWaitWindowTimeout = 2
	lWaitWindowIsOn = .T.

	* logfile properties
	lLoggingIsOn = .T.
	cLogFile = 'F:\BOA\LOGFILES\CitiBank_From_Toll_RETURNS_log.txt'

	* INTERNAL email properties
	lSendInternalEmailIsOn = .T.
	cFrom = 'TGF Corporate <fmicorporate@fmiint.com>'
	cSendTo = 'mark.bennett@tollgroup.com'
	*cCC = 'pgaidis@fmiint.com'
	cCC = ''
	cSubject = ''
	cAttach = ''
	cBodyText = ''

	cCOMPUTERNAME = ''
	cUSERNAME = ''


	FUNCTION INIT
		IF NOT DODEFAULT()
			RETURN .F.
		ENDIF
		WITH THIS
			*SET RESOURCE OFF
			CLOSE DATA
			SET CENTURY ON
			SET DATE AMERICAN
			SET HOURS TO 24
			SET ANSI ON
			SET TALK OFF
			SET DELETED ON
			SET CONSOLE OFF
			SET EXCLUSIVE OFF
			SET SAFETY OFF
			SET EXACT OFF
			SET STATUS BAR ON
			SET SYSMENU OFF
			SET ENGINEBEHAVIOR 70
			_VFP.AUTOYIELD = .lAutoYield
			.cCOMPUTERNAME = GETENV("COMPUTERNAME")
			.cUSERNAME = GETENV("USERNAME")
			IF .lTestMode THEN
				.cLogFile = 'F:\BOA\LOGFILES\CitiBank_From_Toll_RETURNS_LOG_TESTMODE.txt'
				.cSendTo = 'mark.bennett@tollgroup.com'
				.cCC = ''
			ENDIF
			.lLoggingIsOn = .lLoggingIsOn AND NOT EMPTY(.cLogFile)
			IF .lLoggingIsOn THEN
				SET ALTERNATE TO (.cLogFile) ADDITIVE
				SET ALTERNATE ON
			ENDIF
		ENDWITH
	ENDFUNC


	FUNCTION DESTROY
		WITH THIS
			IF .lLoggingIsOn  THEN
				SET ALTERNATE OFF
				SET ALTERNATE TO
			ENDIF
		ENDWITH
		DODEFAULT()
	ENDFUNC


	PROCEDURE Trigger824FileFTPUpload
		* trigger the FTP upload process
		*INSERT INTO F:\edirouting\ftpjobs (jobname) VALUES ("CITI-824-TO-TGF-HK")
		.TrackProgress('Triggered FTP job CITI-824-TO-TGF-HK', LOGIT+SENDIT)
		RETURN
	ENDPROC


	PROCEDURE TriggerBAIFileFTPUpload
		* trigger the FTP upload process
		INSERT INTO F:\edirouting\ftpjobs (jobname) VALUES ("CITI-BAI-TO-TGF-HK")
		.TrackProgress('Triggered FTP job CITI-BAI-TO-TGF-HK', LOGIT+SENDIT)
		RETURN
	ENDPROC


	FUNCTION MAIN
		WITH THIS
			LOCAL lnNumberOfErrors, loError, ldToday, ldYesterday, lcCompany, lcYesterday
			LOCAL lcTransText, lcInputFolder, lcArchivedFolder, lnNumFiles, lcRootFileName
			LOCAL laFiles[1,5], laFilesSorted[1,6], i, lcFile, lcArchivedFile, llArchived
			LOCAL lcConvertedFolder, lcConvertedFile, lcStringOld, lcStringNew, lcFileToProcess
			LOCAL lcDateTimeStamp, ltDateTimeStamp, lcBAIFolder, lcBAIFolderFile, lc824Folder, lc824FolderFile

			TRY

				lnNumberOfErrors = 0
				
				IF .lTestMode THEN
					.cInputFolder = .cTestInputFolder
					.cArchivedFolder = .cTestArchivedFolder
					.cConvertedFolder = .cTestConvertedFolder
					.cBAIFolder = .cTestBAIFolder
					.c824Folder = .cTest824Folder
				ELSE
					.cInputFolder = .cProdInputFolder
					.cArchivedFolder = .cProdArchivedFolder
					.cConvertedFolder = .cProdConvertedFolder
					.cBAIFolder = .cProdBAIFolder
					.c824Folder = .cProd824Folder
				ENDIF

				.TrackProgress('CitiBank From Toll Return process started....', LOGIT+SENDIT+NOWAITIT)
				.TrackProgress('COMPUTERNAME = ' + .cCOMPUTERNAME, LOGIT+SENDIT)
				.TrackProgress('USERNAME = ' + .cUSERNAME, LOGIT+SENDIT)
				.TrackProgress('PROJECT = CITIBANKFROMTOLLRETURNS', LOGIT+SENDIT)
				IF .lTestMode THEN
					.TrackProgress('                        TEST MODE                        ', LOGIT+SENDIT)
				ENDIF
				.TrackProgress('.cInputFolder = ' + .cInputFolder, LOGIT+SENDIT)
				.TrackProgress('.cArchivedFolder = ' + .cArchivedFolder, LOGIT+SENDIT)
				.TrackProgress('.cConvertedFolder = ' + .cConvertedFolder, LOGIT+SENDIT)
				.TrackProgress('.cBAIFolder = ' + .cBAIFolder, LOGIT+SENDIT)
				.TrackProgress('.c824Folder = ' + .c824Folder, LOGIT+SENDIT)

				ldToday = .dToday
				ldYesterday = ldToday - 1
				lcYesterday = DTOC(ldYesterday)

				.cSubject = 'CitiBank From Toll Return Processing for ' + TRANSFORM(DATETIME())

				lcInputFolder = .cInputFolder
				lcArchivedFolder = .cArchivedFolder
				lcConvertedFolder = .cConvertedFolder
				lcBAIFolder = .cBAIFolder
				lc824Folder = .c824Folder

				* get list of files in input folder
				SET DEFAULT TO (lcInputFolder)
				lnNumFiles = ADIR(laFiles,"*.*")

				.TrackProgress(TRANSFORM(lnNumFiles) + ' files found in ' + lcInputFolder,LOGIT+SENDIT)

				IF lnNumFiles > 0 THEN

					* sort file list by date/time
					.SortArrayByDateTime(@laFiles, @laFilesSorted)

					FOR i = 1 TO lnNumFiles
						lcRootFileName = laFilesSorted[i,1]
						lcDateTimeStamp = ( DTOC(laFilesSorted[i,3]) + " " + laFilesSorted[i,4] )
						ltDateTimeStamp = CTOT( lcDateTimeStamp )
						lcFile = lcInputFolder + lcRootFileName
						lcArchivedFile = lcArchivedFolder + lcRootFileName
						lcConvertedFile = lcConvertedFolder + lcRootFileName
						lcBAIFolderFile = lcBAIFolder + lcRootFileName
						lc824FolderFile = lc824Folder + lcRootFileName

						* process each file
						.TrackProgress('Processing ' + lcFile + '....',LOGIT+SENDIT)

						IF .lConvertFiles THEN
							* take input file, convert LF rec delimiter to cr/lf
							lcStringOld = FILETOSTR(lcFile)
							lcStringNew = STRTRAN(lcStringOld,LF,CRLF)
							* and write out in converted folder
							=STRTOFILE(lcStringNew,UPPER(lcConvertedFile))

							IF FILE(lcConvertedFile) THEN
								.TrackProgress('   Converted ' + lcFile,LOGIT+SENDIT)
							ELSE
								.LOGERROR('   ERROR converting ' + lcFile)
							ENDIF
							* run processing against the converted file
							lcFileToProcess = lcConvertedFile
						ELSE
							* run processing against original, unconverted file
							lcFileToProcess = lcFile

						ENDIF && lConvertFiles

						* process each file
						DO CASE
						
							
							* moved VCA file processing to separate process:  citibankVCAreturns
							*
							*!*	CASE UPPER(LEFT(lcRootFileName,9)) = 'VCA_RECON'

							*!*		* VCA Reconciliation file from Citi -- copy to its own folder
							*!*		* then process
							*!*		.TrackProgress('===+==> Found VCA Reconciliation file: ' + lcRootFileName,LOGIT+SENDIT)
							*!*		llArchived = FILE(lcVCAReconcileFile)
							*!*		IF llArchived THEN
							*!*			RUN ATTRIB -R &lcVCAReconcileFile.
							*!*			.TrackProgress('   Used ATTRIB -R for re-moving : ' + lcFile,LOGIT+SENDIT)
							*!*		ENDIF

							*!*		* move file...
							*!*		COPY FILE (lcFile) TO (lcVCAReconcileFile)
							*!*		llArchived = FILE(lcVCAReconcileFile)
							*!*		IF llArchived THEN
							*!*			DELETE FILE (lcFile)
							*!*			.TrackProgress('   Moved ' + lcFile,LOGIT+SENDIT)
							*!*		ELSE
							*!*			.LOGERROR('   ERROR moving ' + lcFile)
							*!*		ENDIF
							*!*		
							*!*		*.ProcessVCAReconciliationFile(lcVCAReconcileFile, lcRootFileName, ltDateTimeStamp)


							*!*	CASE UPPER(LEFT(lcRootFileName,12)) = 'VCA_RESPONSE'

							*!*		* VCA response file from Citi -- copy to its own folder
							*!*		* then process
							*!*		.TrackProgress('======> Found VCA Response file: ' + lcRootFileName,LOGIT+SENDIT)
							*!*		llArchived = FILE(lcVCAResponseFile)
							*!*		IF llArchived THEN
							*!*			RUN ATTRIB -R &lcVCAResponseFile.
							*!*			.TrackProgress('   Used ATTRIB -R for re-moving : ' + lcFile,LOGIT+SENDIT)
							*!*		ENDIF

							*!*		* move file...
							*!*		COPY FILE (lcFile) TO (lcVCAResponseFile)
							*!*		llArchived = FILE(lcVCAResponseFile)
							*!*		IF llArchived THEN
							*!*			DELETE FILE (lcFile)
							*!*			.TrackProgress('   Moved ' + lcFile,LOGIT+SENDIT)
							*!*		ELSE
							*!*			.LOGERROR('   ERROR moving ' + lcFile)
							*!*		ENDIF
							*!*		
							*!*		*.ProcessVCAResponseFile(lcVCAResponseFile, lcRootFileName, ltDateTimeStamp)
						
							CASE UPPER(LEFT(lcRootFileName,3)) = 'ACK'
								.ProcessACKFile(lcFileToProcess, lcRootFileName, ltDateTimeStamp)
								
							CASE UPPER(LEFT(lcRootFileName,4)) = 'AACK'
								.ProcessACKFile(lcFileToProcess, lcRootFileName, ltDateTimeStamp)
								
							CASE UPPER(LEFT(lcRootFileName,3)) = 'RTN'
								.ProcessRTNFile(lcFileToProcess, lcRootFileName, ltDateTimeStamp)
								
							CASE UPPER(LEFT(lcRootFileName,21)) = 'TGF_AMERICAS_BANK_REC'
								* BAI return file from Citi -- copy to its own folder
								* will initiate FTP to Toll Aus after all other processing completes...
								.lBAIFileFound = .T.
								.TrackProgress('Found BAI file: ' + lcRootFileName,LOGIT+SENDIT)
								llArchived = FILE(lcBAIFolderFile)
								IF llArchived THEN
									RUN ATTRIB -R &lcBAIFolderFile.
									.TrackProgress('   Used ATTRIB -R for re-moving : ' + lcFile,LOGIT+SENDIT)
								ENDIF

								* move file...
								COPY FILE (lcFile) TO (lcBAIFolderFile)
								llArchived = FILE(lcBAIFolderFile)
								IF llArchived THEN
									DELETE FILE (lcFile)
									.TrackProgress('   Moved ' + lcFile,LOGIT+SENDIT)
								ELSE
									.LOGERROR('   ERROR moving ' + lcFile)
								ENDIF
								
							CASE UPPER(LEFT(lcRootFileName,10)) = 'TOLLAP1067' 
								* NOTHING
								* it's a log file for ACH acct 1067 out of Platinum NJ
								* no special processing; just archive it in next step
								
							CASE UPPER(LEFT(lcRootFileName,11)) == 'TOLL.EDI824'
								* 824/997 return file from Citi -- copy to its own folder
								* will initiate FTP to Toll Aus after all other processing completes...
								.l824FileFound = .T.
								.TrackProgress('Found 824 file: ' + lcRootFileName,LOGIT+SENDIT)
								llArchived = FILE(lc824FolderFile)
								IF llArchived THEN
									RUN ATTRIB -R &lc824FolderFile.
									.TrackProgress('   Used ATTRIB -R for re-moving : ' + lcFile,LOGIT+SENDIT)
								ENDIF

								* move file...
								COPY FILE (lcFile) TO (lc824FolderFile)
								llArchived = FILE(lc824FolderFile)
								IF llArchived THEN
									DELETE FILE (lcFile)
									.TrackProgress('   Moved ' + lcFile,LOGIT+SENDIT)
								ELSE
									.LOGERROR('   ERROR moving ' + lcFile)
								ENDIF
								
							OTHERWISE
								.LOGERROR('   ERROR - unexpected file type for ' + lcFile)
						ENDCASE

						* archive each file if not already moved
						IF .lArchiveFiles ;
							AND (NOT UPPER(LEFT(lcRootFileName,21)) = 'TGF_AMERICAS_BANK_REC') ;
							AND (NOT UPPER(LEFT(lcRootFileName,11)) == 'TOLL.EDI824') THEN

							* if the file already exists in the archive folder, make sure it is not read-only.
							* this is to prevent errors we get copying into archive on a resend of an already-archived file.
							* Necessary because the files in the archive folders were sometimes marked as read-only (not sure why)
							llArchived = FILE(lcArchivedFile)
							IF llArchived THEN
								RUN ATTRIB -R &lcArchivedFile.
								.TrackProgress('   Used ATTRIB -R for re-archiving : ' + lcFile,LOGIT+SENDIT)
							ENDIF

							* archive it...
							COPY FILE (lcFile) TO (lcArchivedFile)
							llArchived = FILE(lcArchivedFile)
							IF llArchived THEN
								DELETE FILE (lcFile)
								.TrackProgress('   Archived ' + lcFile,LOGIT+SENDIT)
							ELSE
								.LOGERROR('   ERROR archiving ' + lcFile)
							ENDIF
						ENDIF  && .lArchiveFiles

					ENDFOR  && i = 1 TO lnNumFiles

				ENDIF  &&  lnNumFiles > 0

				* send BAI files, if any, from archive folder...
				IF .lBAIFileFound THEN
					.TriggerBAIFileFTPUpload()
				ENDIF

				* send 824/997 files, if any, from archive folder...
				IF .l824FileFound THEN
					.Trigger824FileFTPUpload()
				ENDIF

				.TrackProgress('CitiBank From Toll Return process ended normally!',LOGIT+SENDIT)
				.cBodyText = .cErrors + CRLF + CRLF + .cBodyText

			CATCH TO loError

				.TrackProgress('There was an error.',LOGIT+SENDIT)
				.TrackProgress('The Error # is: ' + TRANSFORM(loError.ERRORNO), LOGIT+SENDIT)
				.TrackProgress('The Error Message is: ' + TRANSFORM(loError.MESSAGE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred in Program: ' + TRANSFORM(loError.PROCEDURE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred at line #: ' + TRANSFORM(loError.LINENO), LOGIT+SENDIT)
				lnNumberOfErrors = lnNumberOfErrors + 1
				CLOSE DATA

			ENDTRY

			CLOSE DATA
			WAIT CLEAR
			***************** INTERNAL email results ******************************
			.TrackProgress('About to send status email.',LOGIT)
			.TrackProgress('The logfile is: ' + .cLogFile, SENDIT)
			.TrackProgress('CitiBank From Toll Return process started: ' + .cStartTime, LOGIT+SENDIT)
			.TrackProgress('CitiBank From Toll Return process finished: ' + TTOC(DATETIME()), LOGIT+SENDIT)

			IF .lSendInternalEmailIsOn THEN
				DO FORM dartmail2 WITH .cSendTo,.cFrom,.cSubject,.cCC,.cAttach,.cBodyText,"A"
				.TrackProgress('Sent status email.',LOGIT)
			ELSE
				.TrackProgress('Did not send status email: THIS.lSendInternalEmailIsOn = FALSE.',LOGIT)
			ENDIF

		ENDWITH
		RETURN
	ENDFUNC && main


	PROCEDURE LOGERROR
		LPARAMETERS tcError
		WITH THIS
			.TrackProgress(tcError,LOGIT+SENDIT)
			.cErrors = .cErrors + ALLTRIM(tcError) + CRLF
		ENDWITH
	ENDPROC


	PROCEDURE ProcessRTNFile
		LPARAMETERS tcFileToProcess, tcRootFileName, ttDateTimeStamp
		* Update the CitiBank fields in TOLLRTNS table for matching Acct# / Check #
		PRIVATE m.citiprocdtm, m.checknum, m.acctnum, m.citiissuedt, m.citipaiddt, m.citichkamt, m.citimatchind, m.citirtnfile
		m.citiprocdtm = DATETIME()

		USE F:\BOA\DATA\TOLLRTNS IN 0 ALIAS TOLLRTNS

		CREATE CURSOR currtn (cLine C(100))
		SELECT currtn
		APPEND FROM &tcFileToProcess TYPE SDF

		* exclude Trailer record, which begins with 'T' from scan
		* removed payee per CitiBank 8/4/10 MB
		SELECT currtn
		SCAN FOR NOT INLIST( UPPER(LEFT(currtn.cLine,1)),'T','F')
			m.checknum = SUBSTR(currtn.cLine,3,10)
			m.acctnum = SUBSTR(currtn.cLine,18,10)
			m.citiissuedt = CTOD(SUBSTR(currtn.cLine,46,2)+"/"+SUBSTR(currtn.cLine,48,2)+"/20"+SUBSTR(currtn.cLine,50,2))
			m.citipaiddt = CTOD(SUBSTR(currtn.cLine,39,2)+"/"+SUBSTR(currtn.cLine,41,2)+"/20"+SUBSTR(currtn.cLine,43,2))
			m.citichkamt = VAL(SUBSTR(currtn.cLine,28,11))/100
			m.citimatchind = SUBSTR(currtn.cLine,45,1)
			m.citirtnfile = tcRootFileName
			m.citirtndtm = ttDateTimeStamp

			SELECT TOLLRTNS
			LOCATE FOR (acctnum == m.acctnum) AND (checknum == m.checknum)
			IF FOUND() THEN
				* update citi fields
				REPLACE TOLLRTNS.citiissuedt WITH m.citiissuedt, ;
					TOLLRTNS.citipaiddt WITH m.citipaiddt, ;
					TOLLRTNS.citichkamt WITH m.citichkamt, ;
					TOLLRTNS.citimatchind WITH m.citimatchind, ;
					TOLLRTNS.citirtnfile WITH m.citirtnfile
					
				.TrackProgress('      :) :) - updated TOLLRTNS for Acct# / Check #: [' + m.acctnum + '] / [' + m.checknum + ']',LOGIT+SENDIT)
			ELSE
				* could not find -- but not necessarily an error because we never processed the issuance files sent from Toll
				* just insert the data we have
				INSERT INTO TOLLRTNS FROM MEMVAR
				*.LOGERROR('      ERROR - could not find matching Acct# / Check #: [' + m.acctnum + '] / [' + m.checknum + ']')
			ENDIF
			
		ENDSCAN

		IF USED('TOLLRTNS') THEN
			USE IN TOLLRTNS
		ENDIF
		IF USED('currtn') THEN
			USE IN currtn
		ENDIF
		
		RETURN
	ENDPROC  && ProcessRTNFile


	PROCEDURE ProcessACKFile
		LPARAMETERS tcFileToProcess, tcRootFileName, ttDateTimeStamp
		LOCAL lnFileHandle, lcLine, lnNumSecsToGoBack, lnIID, lnTHPos
		PRIVATE m.cprefix, m.cacknfile, m.procackndtm, m.recvackndtm, m.namtackn, m.ctrlrmatch
		

		*!*	ACKN layout (after converted to have CRs):
		*!*	         STRT      STRT  END
		*!*	PROFILE  DATE      TIME  TIME     RECS SENT    RECS RECV        AMOUNT
		*!*	--------------------------------------------------------------------------------
		*!*	TH4339IR 08/23/10  09:34 09:34             0   0000000010    000000010.00

		lnFileHandle = FOPEN(tcFileToProcess)&& Open the file
		lcLine = FGETS(lnFileHandle,500)  && read line 1 (discard)
		
*SET STEP ON 
		
		* the ACKN files for Toll Aus. files seem to be formatted differently -- without line feeds, 
		* so we will only get one line in the converted file.
		* Look for certain values in this line to determine this -- 2175 is a Toll account mask
		lnTHPos = AT('TH2175',lcLine)
		IF lnTHPos > 0 THEN
			* the file is still only one line, even after conversion.
			* extract the portion which would equate to line 4 of normal processing
			lcLine = SUBSTR(lcLine,lnTHPos)  && = line 4 (to be parsed)
		ELSE
			* assume file converted into 4 lines, continue reading lines
			lcLine = FGETS(lnFileHandle)  && read line 2 (discard)
			lcLine = FGETS(lnFileHandle)  && read line 3 (discard)
			lcLine = FGETS(lnFileHandle)  && read line 4 (to be parsed)
		ENDIF
		=FCLOSE(lnFileHandle)  && Close the file
		
		m.cprefix = SUBSTR(tcRootFileName,5,4)
		m.cacknfile = tcRootFileName
		m.nrecsackn = VAL(SUBSTR(lcLine,48,10))
		m.namtackn = VAL(SUBSTR(lcLine,60,14))
		m.ctrlrmatch = SUBSTR(lcLine,79,1)
		m.procackndtm = DATETIME()
		m.recvackndtm = ttDateTimeStamp
		
		USE F:\BOA\DATA\TOLLACKS IN 0 ALIAS TOLLACKS

		* look in TOLLACKS for most recent record that matches the Prefix ;
		* and was sent within last XX seconds
		* and has not already been updated with ACKN info.
		lnNumSecsToGoBack = 60 * 60 * 24

		IF USED('CURTOLLACKS') THEN
			USE IN CURTOLLACKS
		ENDIF

		SELECT iid, cprefix, insdttm, ( ttDateTimeStamp - insdttm ) AS NSECSPAST ;
			FROM TOLLACKS ;
			INTO CURSOR CURTOLLACKS ;
			WHERE COMPLETED = 'YES' ;
			AND ( ttDateTimeStamp - insdttm ) <= lnNumSecsToGoBack ;
			AND ( ttDateTimeStamp - insdttm ) > 0 ;
			AND CPREFIX = m.cprefix ;
			AND SENTTO = 'CITIBANK' ;
			AND EMPTY(cacknfile) ;
			ORDER BY insdttm DESC

		IF USED('CURTOLLACKS') AND NOT EOF('CURTOLLACKS') THEN
		
			* get IID from top (most recent) record to use to update back into TOLLACKS
			SELECT CURTOLLACKS
			GOTO TOP
			lnIID = CURTOLLACKS.IID
			
			SELECT TOLLACKS
			LOCATE FOR iid = lnIID
			IF FOUND() THEN
				REPLACE TOLLACKS.cacknfile WITH m.cacknfile, ;
					TOLLACKS.nrecsackn WITH m.nrecsackn, ;
					TOLLACKS.namtackn WITH m.namtackn, ;
					TOLLACKS.ctrlrmatch WITH m.ctrlrmatch, ;
					TOLLACKS.procackndtm WITH m.procackndtm, ;
					TOLLACKS.recvackndtm WITH m.recvackndtm
					
				.TrackProgress('      :) :) - updated TOLLACKS rec for iid = ' + TRANSFORM(lnIID), LOGIT+SENDIT)
				
				IF ( TOLLACKS.nrecsackn <> TOLLACKS.nrecssent ) THEN
					.LOGERROR('      WARNING - # Recs Sent Mismatch in TOLLACKS rec for iid = ' + TRANSFORM(lnIID))
				ENDIF
				
				IF ( TOLLACKS.namtackn <> TOLLACKS.namtsent ) THEN
					.LOGERROR('      WARNING - $ Amount Mismatch in TOLLACKS rec for iid = ' + TRANSFORM(lnIID))
				ENDIF				
				
			ELSE
				*.LOGERROR('      ERROR - could not find matching TOLLACKS rec for iid = ' + TRANSFORM(lnIID))
				INSERT INTO TOLLACKS FROM MEMVAR
			ENDIF
		
		ELSE
			*.LOGERROR('      ERROR - could not find matching TOLLACKS rec for ACK file: ' + tcRootFileName)
			INSERT INTO TOLLACKS FROM MEMVAR
		ENDIF

		IF USED('TOLLACKS') THEN
			USE IN TOLLACKS
		ENDIF
		IF USED('CURTOLLACKS') THEN
			USE IN CURTOLLACKS
		ENDIF

		RETURN
	ENDPROC && ProcessACKFile



	FUNCTION ExecSQL
		LPARAMETERS tcSQL, tcCursorName, tlNoDataReturnedIsOkay
		LOCAL llRetval, lnResult
		WITH THIS
			* close target cursor if it's open
			IF USED(tcCursorName)
				USE IN (tcCursorName)
			ENDIF
			WAIT WINDOW "Fetching remote data; please wait..." NOWAIT
			lnResult = SQLEXEC(.nSQLHandle, tcSQL, tcCursorName)
			llRetval = ( lnResult > 0 )
			IF llRetval THEN
				* see if any data came back
				IF NOT tlNoDataReturnedIsOkay THEN
					IF (NOT USED(tcCursorName)) OR EOF(tcCursorName) THEN
						llRetval = .F.
						.TrackProgress('No data was returned by this query: ' +CRLF + tcSQL, LOGIT+SENDIT)
						.cSendTo = 'mark.bennett@tollgroup.com>'
					ENDIF
				ENDIF
			ELSE
				.TrackProgress("Result Code [" + TRANSFORM(lnResult) + "] returned by this query: " + tcSQL, LOGIT+SENDIT)
				.cSendTo = 'mark.bennett@tollgroup.com>'
			ENDIF
			WAIT CLEAR
			RETURN llRetval
		ENDWITH
	ENDFUNC


	PROCEDURE TrackProgress
		* do any combination of Wait Window, writing to logfile, and adding to body of email,
		* based on nFlags parameter.
		LPARAMETERS tcExpression, tnFlags
		WITH THIS
			IF BITAND(tnFlags,LOGIT) = LOGIT THEN
				IF .lLoggingIsOn THEN
					?
					? .cProcessName + "  " + TTOC(DATETIME()) + ": " + tcExpression
				ENDIF
			ENDIF
			IF .lWaitWindowIsOn THEN
				IF BITAND(tnFlags,NOWAITIT) = NOWAITIT THEN
					WAIT WINDOW tcExpression NOWAIT
				ENDIF
				IF BITAND(tnFlags,WAITIT) = WAITIT THEN
					WAIT WINDOW tcExpression TIMEOUT .nWaitWindowTimeout
				ENDIF
			ENDIF
			IF BITAND(tnFlags,SENDIT) = SENDIT THEN
				IF .lSendInternalEmailIsOn THEN
					.cBodyText = .cBodyText + tcExpression + CRLF + CRLF
				ENDIF
			ENDIF
		ENDWITH
	ENDPROC  &&  TrackProgress


	PROCEDURE SortArrayByDateTime
		* expects taInArray[] to be the result of an ADIR()
		LPARAMETERS taInArray, taOutArray
		EXTERNAL ARRAY taInArray, taOutArray
		LOCAL lnRows, lnCols, i, j
		* extending taInArray[] to process the files in date/time order
		lnRows = ALEN(taInArray,1)
		lnCols = ALEN(taInArray,2)
		* create taOutArray with one more column
		DIMENSION taOutArray[lnRows,lnCols + 1]
		* and fill it with taInArray columns AND (filedate + filetime) in new column
		* ( ACOPY() doesn't preserve elements properly because of different # of columns )
		FOR i = 1 TO lnRows
			FOR j = 1 TO lnCols
				taOutArray[i,j] = taInArray[i,j]
			ENDFOR
			taOutArray[i,lnCols + 1] = DTOS(taInArray[i,3]) + "_" + taInArray[i,4]
		ENDFOR
		* sort taOutArray on date/time column
		=ASORT(taOutArray,6)
	ENDPROC


ENDDEFINE
