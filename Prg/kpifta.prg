**Time from container pickup to returned to port
**  - Full ocean containers Only
**  - Times based on wolog.pickedup Vs. wolog.returned
**
Wait window "Gathering data for Free Time Allowance Spreadsheet..." nowait noclear

dstartdt=xstartdt
denddt=xenddt

use f:\wo\wodata\wolog in 0

xsqlexec("select * from acctbill","acctbill",,"qq")
index on billtoid tag billtoid
index on accountid tag accountid
index on STR(billtoid,4)+STR(accountid,4) tag billacct

**account is required for this report
xacctfilter="accountid="+transform(xaccountid)+" and "
Select *, accountid as acctNumFld, acctname as acctNameFld, space(len(acctname)) as billname, 00 as DateDiff, 00 as kpi ;
	from wolog ;
	where &xacctfilter delivered>=xstartdt-30 and Between(returned, xstartdt, xenddt) and office = goffice and type="O" and !loose ;
  into cursor csrdetail readwrite

scan
	=seek(str(csrdetail.billtoid,4)+str(csrdetail.accountid,4),"acctbill","billacct")
	replace billname with acctbill.billname in csrdetail
endscan

**eventually need a table to hold accountid, billtoid, free days bc it will differ pre account/billto for aeropostale
**if blank billtoid, all bilto's for that acct. if blank acct, for all other accts not liosted in table
do case
case xaccountid=4717 &&big lots
	replace all kpi with 14
case xaccountid=3975 &&aeropostale
	replace all kpi with iif(billtoid=999,14,14)
endcase

**calculate date differential into dateDiff - make necessary adjustments for weekends & FMI holidays
DO kpicalcDiff with "pickedup", "returned", "dateDiff"

Locate
If Eof()
	strMsg = "No data found."
	=MessageBox(strMsg, 48, "Free Time Allowance")
Else
	Wait window "Creating Free Time Allowance Spreadsheet..." nowait noclear

	public oexcel
	oexcel = createobject("excel.application")

	oWorkbook=""
	xxlsfile="kpiPassFailGeneric.xls"
	DO xlsTimeGeneric with "", "returned", "1", "", "Free Time Allowance", "Returned Date Vs. Pickup Date", ;
		" - Date range entered reflects container returned dates.", "", "''", .t., .t.

	If lDetail
		Select csrTotals
		nWorksheet = 2
		Scan
			store tempfox+substr(sys(2015), 3, 10)+".xls" to ctmpfile
			Select *, Iif(type="A", awb, container) as field1 from csrDetail ;
				where Year(returned) = csrTotals.year and Month(returned) = csrTotals.month into cursor csrDet
			Copy to &cTmpFile fields acctNameFld, billname, wo_num, type, field1, wo_date, pickedup, returned, dateDiff, kpi type xl5

			DO xlsTimeGenDetail with cTmpFile, nWorkSheet, "J"

			nWorksheet = nWorksheet+1
			use in csrdet
		EndScan
	EndIf

	oWorkbook.Worksheets[1].range("A1").activate()
	oWorkbook.Save()
	oExcel.visible=.t.

	RELEASE oWorkbook, oExcel
EndIf


use in wolog
use in acctbill
use in csrdetail
if used("csrtotals")
  use in csrtotals
endif
