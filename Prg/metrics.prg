* we have to stop supporting this mess
* too complicated to move into SQL  dy 11/8/16

Parameters tAcct, tAcctName, tOffice, tStart, tEnd, reportType

*STORE 77 TO TACCT
*STORE 'WORLD WIDE APPAREL' TO TACCTNAME
*STORE 'N' TO TOFFICE
*STORE {11/01/04} to TSTART
*STORE {01/31/06} TO tend
*STORE gomonth(TSTART,1)-1 TO tend
*STORE 3 TO REPORTTYPE

STORE 12-LEN(ALLTRIM(STR(TACCT))) TO TLEN
STORE ALLTRIM(STR(tacct)) TO xcustomerkey
STORE "" TO xreturn
STORE tacct TO xid

if xid>4095
	xreturn = right(transform(xid,"@0"),4)
	if padl(trans(val(xreturn)),4,"0") = xreturn
		xreturn = "0000"+xreturn
	endif
else
	xreturn = right(transform(xid,"@0"),3)
	if padl(trans(val(xreturn)),3,"0") = xreturn
		xreturn = "00000"+xreturn
	endif
endif

STORE xreturn TO XSHIPTOKEY

DO WHILE TLEN>0
STORE '0'+XCUSTOMERKEY TO XCUSTOMERKEY
TLEN=TLEN-1
ENDDO

Store Iif(Empty(tAcct), .t., .f.) to allaccounts
Store Iif(Empty(tAcct), "ALL ACCOUNTS", tAcctName) TO tAcctName

Open Database (wf(toffice,tacct)+"wh")
USE wh!indeth ORDER INWOLOGID ALIAS DETh IN 0
USE wh!inloch ORDER INDETID ALIAS LOCh IN 0
USE wh!outdeth order OUTSHIPID
COPY TO h:\fox\outdeth.dbf WITH cdx
USE IN outdeth
USE h:\fox\outdeth EXCLUSIVE ALIAS odeth iN 0
USE wh!outloch order OUTdetID ALIAS oloch IN 0
USE wh!outshiph IN 0

xmod=wf(toffice,tacct,,,,,,.t.)

**create a random filename to prevent overwriting of files
cFileName = "s:\metrics\"+SUBSTR(SYS(2015), 3, 10)+".xls"

xsqlexec("select * from arhdrh",,,"ar")
xsqlexec("select * from arlinh",,,"ar")
xsqlexec("select * from glchart",,,"ar")

xsqlexec("select * from account where inactive=0","account",,"qq")
index on accountid tag accountid
set order to

USE f:\wo\wodata\wolog IN 0
xsqlexec("select * from cargo",,,"stuff")
index on accountid tag accountid
index on zmonth tag zmonth
set order to

set step on


*get the total number of workdays within this period
STORE tstart TO TDATE
STORE 0 TO TOTNUMDAYS

DO WHILE TDATE<=TEND
	IF DOW(TDATE)>=2 AND DOW(TDATE)<=6
	TOTNUMDAYS=TOTNUMDAYS+1
	ENDIF
	TDATE=TDATE+1
ENDDO

*TAKE OUT HOLIDAYS 
IF totnumdays>360 AND totnumdays<370
  totnumdays=totnumdays-10
ENDIF


STORE DTOC(tstart)+"-"+DTOC(tend)+" ("+ALLTRIM(STR(TOTNUMDAYS))+" days)" TO datestring
DO CASE
  CASE toffice="C"
	STORE "San Pedro" TO tlocation
  CASE toffice='N'
	STORE "Carteret" TO tlocation
  CASE toffice='M'
	STORE "Miami" TO tlocation
  CASE toffice='Y'
	STORE "Carson2" TO tlocation
ENDCASE

IF InList(reporttype,1,3)
	WAIT "Compiling inbound data. This process could take a few minutes... "+;
		"The output Excel spreadsheet will appear on completion." WINDOW NOWAIT NOCLEAR

	USE wh!INWOLOG ORDER ACCOUNTID IN 0

	SELECT inwolog
	If !Empty(tAcct) and !Seek(tAcct)
	  strMsg = "No data was found for the selected account."
	  =MessageBox(strMsg, 16, "Metrics Report")
	  =closeData()
	  Return
	EndIf

	USE wh!indet ORDER INWOLOGID IN 0
	USE wh!inloc ORDER INDETID IN 0

	create cursor TEMP (ACCOUNTID N(10),UNLOADED D, QTY N(10,0), SKUS N(10,0),ctncube N(10,2),containers n(10,0),ctrqty n(10,0), maxskus n(10,0),NUMLOCS N(10,0))
	create cursor TEMP2 (ACCOUNTID N(10),UNLOADED D, STYLE C(10), COLOR C(10), ID C(10), PACK C(10),ctncube N(10,2),NUMLOCS N(10,0))
	create cursor TEMP4 (ACCOUNTID N(10),acctname c(30),containers N(10,0),ctrqty n(10,0), totskus n(10,0), maxskus n(10,0), airshpts n(10,0), airqty n(10,0), dshpts n(10,0), dqty n(10,0),NUMLOCS N(10,0))

	SELECT TEMP
	INDEX ON accountid TAG account
	INDEX ON DTOC(unloaded) TAG uniqueunl unique
	INDEX ON UNLOADED TAG UNLOADED

	SELECT TEMP2
	INDEX ON UNLOADED TAG UNLOAD2


	*START SEARCHING THE INWOLOG ONLY ONE MONTH BEFORE THE START DATE BY WO DATE
	*END SEARCHING THE INWOLOG ONE MONTH AFTER THE END DATE BY WO_DATE
	STORE MONTH(tstart) TO tmonthstart
	STORE YEAR(tstart) TO tyearstart
	STORE MONTH(tend) TO tmonthend
	STORE YEAR(tend) TO tyearend


	IF tmonthstart=1
		STORE 12 TO tmonthstart
		STORE tyearstart-1 TO tyearstart
	ELSE
		STORE tmonthstart-1 TO tmonthstart
	ENDIF

	IF tmonthend=12
		STORE 1 TO tmonthend
		STORE TYEAREND+1 TO TYEAREND
	ELSE
		STORE tmonthend+1 TO tmonthend
	ENDIF

	STORE ALLTRIM(STR(tmonthstart)) TO tmonthstart
	IF LEN(tmonthstart)=1
		STORE "0"+tmonthstart TO tmonthstart
	ENDIF

	STORE ALLTRIM(STR(tyearstart)) TO tyearstart
	STORE ALLTRIM(STR(tmonthend)) TO tmonthend
	IF LEN(tmonthend)=1
		STORE "0"+tmonthend TO tmonthend
	ENDIF
	STORE ALLTRIM(STR(tyearend)) TO tyearend

	STORE CTOD(tmonthstart+"/01/"+tyearstart) TO notbefore
	STORE CTOD(tmonthEND+"/01/"+tyearend) TO notafter

	IF ALLACCOUNTS
		Select inwolog
		Locate

		DO WHILE !EOF()
			IF WO_DATE<notbefore OR WO_DATE>notafter OR sp
				SKIP
				LOOP
			ENDIF

			STORE INWOLOGID TO tid
			STORE accountid TO TACCT
			SELECT indet
			SEEK TID
			COUNT FOR date_rcvd>=TSTART OR date_rcvd<=tend WHILE INWOLOGID=tid AND !EOF() TO GOAHEAD
			
			SELECT deth
			SEEK TID
			COUNT FOR date_rcvd>=TSTART OR date_rcvd<=tend WHILE INWOLOGID=tid AND !EOF() TO GOAHEAD2
			
			goahead=goahead+goahead2
			
			IF GOAHEAD=0
				SELECT inwoLOG
				SKIP
				LOOP
			ENDIF

			SELECT temp4
			LOCATE FOR accountid=TACCT
			IF EOF()
				APPEND BLANK
				replace accountid WITH TACCT

				SELECT account
				LOCATE FOR accountid=tacct
				STORE acctname TO Tempname

				SELECT temp4
				replace acctname WITH tempname
			ENDIF

			STORE .F. TO isocean,isair,isdomestic			
			SELECT wolog
			SET ORDER TO container
			SEEK ALLTRIM(inwolog.container)
			IF accountid=TACCT AND office=TOFFICE
				SELECT temp4
				REPLACE CONTAINERS WITH CONTAINERS+1
				STORE .T. TO isocean
			ELSE
				SELECT wolog
				SET ORDER TO awb
				*STRIP OUT ANY DASHES BEFORE PARSING
				STORE 1 TO strnum
				STORE ALLTRIM(inwolog.reference) TO airref,TREF
				IF AT('-',airref)>0						
					STORE "" TO tref
					DO WHILE AT('-',airref)>0
						IF !substr(airref,strnum,1)='-'
							tref=tref+subSTR(airref,strnum,1)
						ENDIF
						STORE LEN(airref) TO TLENGTH
					 	STORE SUBSTR(airref,2,tlength-1) TO airref
						strnum=strnum+1
					ENDDO
				endif
				
				STORE SUBSTR(tref,1,3)+'-'+SUBSTR(tref,4,4)+'-'+SUBSTR(tref,8,4) TO TREF

				SEEK tref
				IF accountid=TACCT AND office=TOFFICE
					SELECT temp4
					REPLACE AIRSHPTS WITH AIRSHPTS +1
					STORE .T. TO isair
				ELSE
					SELECT temp4
					REPLACE DSHPTS WITH DSHPTS+1
					STORE .T. TO isdomestic
				ENDIF
			ENDIF
												
			STORE 0 TO tempmaxskus
			SELECT inDET
			SEEK tid
			IF !EOF()
				DO WHILE INWOLOGID=tid AND !EOF()
					IF UNITS OR TOTQTY=0
						SKIP
						LOOP
					ENDIF
					STORE DATE_RCVD TO tdate
					IF tdate<tstart OR tdate>tend
						SKIP
						LOOP
					ENDIF
					STORE STYLE TO tstyle
					STORE COLOR TO tcolor
					STORE ID TO tid2
					STORE PACK TO tpack
					STORE TOTQTY TO tqty
					STORE CTNCUBE TO tcube
					STORE accountid TO tacct
					STORE INDETID TO tdetid
					tempmaxskus=tempmaxskus+1
					
					SELECT TEMP4
					DO CASE
					CASE isocean
						replace ctrqty WITH ctrqty+indet.totqty
					CASE isair
						replace airqty WITH airqty+indet.totqty
					CASE isdomestic
						replace dqty WITH dqty+indet.totqty
					EndCase
						
					SELECT TEMP

					SEEK tdate
					IF EOF() 
						APPEND BLANK
						REPLACE UNLOADED WITH tdate
						replace accountid WITH TACCT
					ENDIF
					DO WHILE unloaded=TDATE AND !EOF()
						IF accountid=TACCT
							exit
						ENDIF
						skip
					ENDDO
					IF !(accountid=TACCT AND unloaded=TDATE)
						APPEND BLANK
						REPLACE UNLOADED WITH tdate
						replace accountid WITH TACCT
					ENDIF

					REPLACE QTY WITH QTY+tqty
					SELECT inloc
					SEEK tdetid
					STORE 0 TO tnumpallets
					DO WHILE INDETID=tdetid AND !EOF()
						IF locqty*TCUBE>98
							STORE ROUND(((locqty*tcube)/98),0) TO TEMPNUM
							TNUMPALLETS=TNUMPALLETS+TEMPNUM
						else
							tnumpallets=tnumpallets+1
						endif
						SKIP
					ENDDO

					SELECT TEMP
					REPLACE NUMLOCS WITH NUMLOCS+tnumpallets
										
					SELECT TEMP2
					SEEK tdate
					IF EOF()
						APPEND BLANK
						REPLACE UNLOADED WITH tdate
						replace accountid WITH TACCT
						REPLACE STYLE WITH tstyle
						REPLACE COLOR WITH tcolor
						REPLACE ID WITH tid2
						REPLACE PACK WITH tpack
						REPLACE CTNCUBE WITH tcube
						SELECT TEMP
						REPLACE SKUS WITH SKUS+1
						REPLACE CTNCUBE WITH tcube
						SELECT TEMP2
					ENDIF

					DO WHILE UNLOADED=tdate AND !EOF()
						IF accountid=TACCT
							EXIT
						ENDIF
						skip
					ENDDO

					IF !(accountid=TACCT AND unloaded=TDATE)
					  APPEND BLANK
			 		  REPLACE UNLOADED WITH tdate
			 		  replace accountid WITH TACCT
					  REPLACE STYLE WITH tstyle
					  REPLACE COLOR WITH tcolor
					  REPLACE ID WITH tid2
					  REPLACE PACK WITH tpack

					  SELECT TEMP
					  REPLACE SKUS WITH SKUS+1
					  REPLACE CTNCUBE WITH ((tqty*tcube)+(QTY*ctncube))/(tqty+QTY)
					endif

					SELECT temp2
					SEEK TDATE
					DO WHILE UNLOADED=tdate AND !EOF()
					  IF accountid=TACCT AND STYLE=tstyle AND COLOR=tcolor AND ID=tid2 AND PACK=tpack
						  EXIT
					  ENDIF
					  SKIP
					ENDDO

					IF !(accountid=TACCT AND UNLOADED=tdate AND STYLE=tstyle AND COLOR=tcolor AND ID=tid2 AND PACK=tpack)
						APPEND BLANK
						REPLACE UNLOADED WITH tdate
						replace accountid WITH TACCT
						REPLACE STYLE WITH tstyle
						REPLACE COLOR WITH tcolor
						REPLACE ID WITH tid2
						REPLACE PACK WITH tpack
						SELECT TEMP
						REPLACE SKUS WITH SKUS+1
						REPLACE CTNCUBE WITH ((tqty*tcube)+(QTY*ctncube))/(tqty+QTY)
					  
						SELECT TEMP2
					ENDIF

					SELECT inDET
					SKIP
				ENDDO
			ELSE
				SELECT deth
				SEEK tid
				DO WHILE INWOLOGID=tid AND !EOF()
					IF UNITS OR TOTQTY=0
						SKIP
						LOOP
					ENDIF
					STORE DATE_RCVD TO tdate
					IF tdate<tstart OR tdate>tend
						SKIP
						LOOP
					ENDIF
					STORE STYLE TO tstyle
					STORE COLOR TO tcolor
					STORE ID TO tid2
					STORE PACK TO tpack
					STORE TOTQTY TO tqty
					STORE CTNCUBE TO tcube
					STORE accountid TO tacct
					STORE INDETID TO tdetid
					tempmaxskus=tempmaxskus+1
					
					SELECT TEMP4
					DO CASE
					CASE isocean
						replace ctrqty WITH ctrqty+deth.totqty
					CASE isair
						replace airqty WITH airqty+deth.totqty
					CASE isdomestic
						replace dqty WITH dqty+deth.totqty
					EndCase
						
					SELECT TEMP
					SEEK tdate
					IF EOF() 
						APPEND BLANK
						REPLACE UNLOADED WITH tdate
						replace accountid WITH TACCT
					ENDIF
					DO WHILE unloaded=TDATE AND !EOF()
						IF accountid=TACCT
							exit
						ENDIF
						skip
					ENDDO
					IF !(accountid=TACCT AND unloaded=TDATE)
						APPEND BLANK
						REPLACE UNLOADED WITH tdate
						replace accountid WITH TACCT
					ENDIF

					REPLACE QTY WITH QTY+tqty
					SELECT LOCh
					SEEK tdetid
					STORE 0 TO tnumpallets
					DO WHILE INDETID=tdetid AND !EOF()
						IF locqty*TCUBE>98
							STORE ROUND(((locqty*tcube)/98),0) TO TEMPNUM
							TNUMPALLETS=TNUMPALLETS+TEMPNUM
						else
							tnumpallets=tnumpallets+1
						endif
						SKIP
					ENDDO
					SELECT TEMP
					REPLACE NUMLOCS WITH NUMLOCS+tnumpallets
										
					SELECT TEMP2
					SEEK tdate
					IF EOF()
						APPEND BLANK
						REPLACE UNLOADED WITH tdate
						replace accountid WITH TACCT
						REPLACE STYLE WITH tstyle
						REPLACE COLOR WITH tcolor
						REPLACE ID WITH tid2
						REPLACE PACK WITH tpack
						REPLACE CTNCUBE WITH tcube
						SELECT TEMP
						REPLACE SKUS WITH SKUS+1
						REPLACE CTNCUBE WITH tcube
						SELECT TEMP2
					ENDIF

					DO WHILE UNLOADED=tdate AND !EOF()
						IF accountid=TACCT
							EXIT
						ENDIF
						skip
					ENDDO

					IF !(accountid=TACCT AND unloaded=TDATE)
					  APPEND BLANK
			 		  REPLACE UNLOADED WITH tdate
			 		  replace accountid WITH TACCT
					  REPLACE STYLE WITH tstyle
					  REPLACE COLOR WITH tcolor
					  REPLACE ID WITH tid2
					  REPLACE PACK WITH tpack

					  SELECT TEMP
					  REPLACE SKUS WITH SKUS+1
					  REPLACE CTNCUBE WITH ((tqty*tcube)+(QTY*ctncube))/(tqty+QTY)
					endif

					SELECT temp2
					SEEK TDATE
					DO WHILE UNLOADED=tdate AND !EOF()
					  IF accountid=TACCT AND STYLE=tstyle AND COLOR=tcolor AND ID=tid2 AND PACK=tpack
						  EXIT
					  ENDIF
					  SKIP
					ENDDO

					IF !(accountid=TACCT AND UNLOADED=tdate AND STYLE=tstyle AND COLOR=tcolor AND ID=tid2 AND PACK=tpack)
						APPEND BLANK
						REPLACE UNLOADED WITH tdate
						replace accountid WITH TACCT
						REPLACE STYLE WITH tstyle
						REPLACE COLOR WITH tcolor
						REPLACE ID WITH tid2
						REPLACE PACK WITH tpack
						SELECT TEMP
						REPLACE SKUS WITH SKUS+1
						REPLACE CTNCUBE WITH ((tqty*tcube)+(QTY*ctncube))/(tqty+QTY)
					  
						SELECT TEMP2
					ENDIF

					SELECT DETh
					SKIP
				ENDDO
			ENDIF

***************

			SELECT temp4
			IF tempmaxskus>maxskus
				replace maxskus WITH TEMPMAXSKUS
			ENDIF

			replace totskus WITH totskus+tempmaxskus
			SELECT inwoLOG
			SKIP
		ENDDO
	ELSE && ALLACCOUNTS
		SELECT inwolog
		Locate for accountId = tAcct

		STORE 0 TO Onumber,octns,anumber,actns,dnumber,dctns,omax,oskus
		DO WHILE ACCOUNTID=tacct AND !EOF()
			STORE .F. TO tisocean
			IF WO_DATE<notbefore OR WO_DATE>notafter OR sp
				SKIP
				LOOP
			ENDIF
			STORE INWOLOGID TO tid
			STORE .F. TO isocean,isair,isdomestic

			SELECT indet
			SEEK TID
			COUNT FOR date_rcvd>=TSTART OR date_rcvd<=tend WHILE INWOLOGID=tid AND !EOF() TO GOAHEAD
			
			SELECT deth
			SEEK TID
			COUNT FOR date_rcvd>=TSTART OR date_rcvd<=tend WHILE INWOLOGID=tid AND !EOF() TO GOAHEAD2
			goahead=goahead+goahead2
			
			IF GOAHEAD=0
				SELECT inwoLOG
				SKIP
				LOOP
			ENDIF

			SELECT wolog
			SET ORDER TO container
			SEEK ALLTRIM(inwolog.container)
			IF accountid=TACCT AND office=TOFFICE AND !EMPTY(inwolog.container)
				onumber=onumber+1
				STORE .T. TO isocean
			ELSE
				SELECT wolog
				SET ORDER TO awb
				*STRIP OUT ANY DASHES BEFORE PARSING
				STORE 1 TO strnum
				STORE ALLTRIM(inwolog.reference) TO airref,TREF
				IF AT('-',airref)>0						
					STORE "" TO tref			
					
					DO WHILE AT('-',airref)>0
						IF !substr(airref,strnum,1)='-'
							tref=tref+substr(airref,strnum,1)
						ENDIF
						STORE LEN(airref) TO TLENGTH
					 	STORE SUBSTR(airref,2,tlength-1) TO airref
						strnum=strnum+1
					ENDDO
				ENDIF
				
				STORE SUBSTR(tref,1,3)+'-'+SUBSTR(tref,4,4)+'-'+SUBSTR(tref,8,4) TO TREF
				SEEK tref
				IF accountid=TACCT AND office=TOFFICE AND !EMPTY(inwolog.reference)
					anumber=anumber+1
					STORE .T. TO isair
				ELSE
					dnumber=dnumber+1
					STORE .T. TO isdomestic
				ENDIF
			ENDIF

			SELECT inDET
			SEEK tid
	 		IF !EOF()
				STORE 0 TO TEMPMAXSKUS
				DO WHILE INWOLOGID=tid AND !EOF()
					IF UNITS
						SKIP
						LOOP
					ENDIF

					STORE DATE_RCVD TO tdate
					IF tdate<tstart OR tdate>tend
						SKIP
						LOOP
					ENDIF
					STORE STYLE TO tstyle
					STORE COLOR TO tcolor
					STORE ID TO tid2
					STORE PACK TO tpack
					STORE TOTQTY TO tqty
					STORE CTNCUBE TO tcube
					STORE INDETID TO tdetid
					tempmaxskus=tempmaxskus+1
					oskus=oskus+1
					DO CASE
					CASE isocean
						octns=octns+TOTQTY
					CASE isair
						Actns=Actns+TOTQTY
					CASE isdomestic
						Dctns=Dctns+TOTQTY
					endcase
											
					SELECT TEMP
					SEEK tdate
					IF EOF()
						APPEND BLANK
						REPLACE UNLOADED WITH tdate
						replace accountid WITH TACCT
					ENDIF

					REPLACE QTY WITH QTY+tqty

					SELECT inloc
					SEEK tdetid
					STORE 0 TO tnumpallets
					DO WHILE INDETID=tdetid AND !EOF()
						IF locqty*TCUBE>98
							STORE ROUND(((locqty*tcube)/98),0) TO TEMPNUM
							TNUMPALLETS=TNUMPALLETS+TEMPNUM
						else
							tnumpallets=tnumpallets+1
						endif

						SKIP
					ENDDO

					SELECT TEMP
					REPLACE NUMLOCS WITH NUMLOCS+tnumpallets
						
					SELECT TEMP2
					SEEK tdate
					IF EOF()
						APPEND BLANK
						REPLACE UNLOADED WITH tdate
						replace accountid WITH TACCT
						REPLACE STYLE WITH tstyle
						REPLACE COLOR WITH tcolor
						REPLACE ID WITH tid2
						REPLACE PACK WITH tpack
						REPLACE CTNCUBE WITH tcube
						SELECT TEMP
						REPLACE SKUS WITH SKUS+1
						REPLACE CTNCUBE WITH tcube
						SELECT TEMP2
					ENDIF

					DO WHILE UNLOADED=tdate AND !EOF()
						  IF STYLE=tstyle AND COLOR=tcolor AND ID=tid2 AND PACK=tpack
							  EXIT
						  ENDIF
						  SKIP
					ENDDO

					IF !(UNLOADED=tdate AND STYLE=tstyle AND COLOR=tcolor AND ID=tid2 AND PACK=tpack)
						APPEND BLANK
						REPLACE UNLOADED WITH tdate
						replace accountid WITH TACCT
						REPLACE STYLE WITH tstyle
						REPLACE COLOR WITH tcolor
						REPLACE ID WITH tid2
						REPLACE PACK WITH tpack
						SELECT TEMP
						REPLACE SKUS WITH SKUS+1
						IF tqty+QTY=0
						REPLACE CTNCUBE WITH 0
						ELSE
						REPLACE CTNCUBE WITH ((tqty*tcube)+(QTY*ctncube))/(tqty+QTY)
						ENDIF
					ENDIF

					SELECT inDET
					SKIP
				ENDDO
			ELSE
				SELECT deth	 
				SEEK tid
				STORE 0 TO TEMPMAXSKUS
				DO WHILE INWOLOGID=tid AND !EOF()
					IF UNITS
						SKIP
						LOOP
					ENDIF

					STORE DATE_RCVD TO tdate
					IF tdate<tstart OR tdate>tend
						SKIP
						LOOP
					ENDIF
					STORE STYLE TO tstyle
					STORE COLOR TO tcolor
					STORE ID TO tid2
					STORE PACK TO tpack
					STORE TOTQTY TO tqty
					STORE CTNCUBE TO tcube
					STORE INDETID TO tdetid
					tempmaxskus=tempmaxskus+1
					oskus=oskus+1
					DO CASE
					CASE isocean
						octns=octns+TOTQTY
					CASE isair
						Actns=Actns+TOTQTY
					CASE isdomestic
						Dctns=Dctns+TOTQTY
					endcase
											
					SELECT TEMP
					SEEK tdate
					IF EOF()
						APPEND BLANK
						REPLACE UNLOADED WITH tdate
						replace accountid WITH TACCT
					ENDIF

					REPLACE QTY WITH QTY+tqty

					SELECT LOCh
					SEEK tdetid
					STORE 0 TO tnumpallets
					DO WHILE INDETID=tdetid AND !EOF()
						IF locqty*TCUBE>98
							STORE ROUND(((locqty*tcube)/98),0) TO TEMPNUM
							TNUMPALLETS=TNUMPALLETS+TEMPNUM
						else
							tnumpallets=tnumpallets+1
						endif

						SKIP
					ENDDO

					SELECT TEMP
					REPLACE NUMLOCS WITH NUMLOCS+tnumpallets
						
					SELECT TEMP2
					SEEK tdate
					IF EOF()
						APPEND BLANK
						REPLACE UNLOADED WITH tdate
						replace accountid WITH TACCT
						REPLACE STYLE WITH tstyle
						REPLACE COLOR WITH tcolor
						REPLACE ID WITH tid2
						REPLACE PACK WITH tpack
						REPLACE CTNCUBE WITH tcube
						SELECT TEMP
						REPLACE SKUS WITH SKUS+1
						REPLACE CTNCUBE WITH tcube
						SELECT TEMP2
					ENDIF

					DO WHILE UNLOADED=tdate AND !EOF()
						  IF STYLE=tstyle AND COLOR=tcolor AND ID=tid2 AND PACK=tpack
							  EXIT
						  ENDIF
						  SKIP
					ENDDO

					IF !(UNLOADED=tdate AND STYLE=tstyle AND COLOR=tcolor AND ID=tid2 AND PACK=tpack)
						APPEND BLANK
						REPLACE UNLOADED WITH tdate
						replace accountid WITH TACCT
						REPLACE STYLE WITH tstyle
						REPLACE COLOR WITH tcolor
						REPLACE ID WITH tid2
						REPLACE PACK WITH tpack
						SELECT TEMP
						REPLACE SKUS WITH SKUS+1
						IF tqty+QTY=0
						REPLACE CTNCUBE WITH 0
						ELSE
						REPLACE CTNCUBE WITH ((tqty*tcube)+(QTY*ctncube))/(tqty+QTY)
						ENDIF
					ENDIF

					SELECT DETh
					SKIP
				ENDDO
			ENDIF

			IF TEMPMAXSKUS>omax
				STORE tempmaxskus TO OMAX
			endif

			SELECT inwoLOG
			SKIP
		ENDDO

		SELECT temp4
		APPEND BLANK
		replace accountid WITH TACCT
		replace acctname WITH TACCTNAME
		replace containers WITH onumber
		replace ctrqty WITH octns
		replace airshpts WITH anumber
		replace airqty WITH actns
		replace dshpts WITH dnumber
		replace dqty WITH dctns
		replace maxskus WITH omax
		replace totskus WITH oskus
	ENDIF &&ALLACCOUNTS

	COPY FILE s:\metrics\whserpt.xls TO &cFileName

	oexcel=createobject("excel.application")

	* open existing:
	oworkbook=oexcel.workbooks.open(cFileName)

	* point to worksheets:
	osheet1=oworkbook.worksheets[1]
	osheet1.range("B4").value=tacctname
	osheet1.range("B5").value=DATESTRING
	osheet1.range("B6").value=TLOCATION

	osheet1=oworkbook.worksheets[2]
	osheet1.range("B4").value=tacctname
	osheet1.range("B5").value=DATESTRING
	osheet1.range("B6").value=TLOCATION

	osheet1=oworkbook.worksheets[3]
	osheet1.range("B4").value=tacctname
	osheet1.range("B5").value=DATESTRING
	osheet1.range("B6").value=TLOCATION

	osheet1=oworkbook.worksheets[4]
	osheet1.range("B4").value=tacctname
	osheet1.range("B5").value=DATESTRING
	osheet1.range("B6").value=TLOCATION


	osheet1=oworkbook.worksheets[5]
	osheet1.range("B4").value=tacctname
	osheet1.range("B5").value=DATESTRING
	osheet1.range("B6").value=TLOCATION

	osheet1=oworkbook.worksheets[6]
	osheet1.range("B4").value=tacctname
	osheet1.range("B5").value=DATESTRING
	osheet1.range("B6").value=TLOCATION


	SELECT temp
	Locate

	Select * from temp group by unloaded into cursor temp30 readWrite
	DO WHILE !EOF()
		SELECT temp
		SUM qty,skus,qty*ctncube,NUMLOCS TO T1,t2,t3,t4 FOR unloaded=temp30.unloaded

		SELECT temp30
		replace qty WITH t1
		replace skus WITH t2
		replace ctncube WITH Iif(t1 = 0, 0, t3/t1)
		replace numlocs WITH T4

		SKIP
	enddo

	Locate

	STORE QTY TO gtotpeak
	STORE UNLOADED TO gtotpeakday
	DO WHILE !EOF()
		IF qty>GTOTPEAK
			STORE QTY TO GTOTPEAK
			STORE UNLOADED TO gtotpeakday
		ENDIF
		SKIP
	ENDDO

	*LOOK FOR THE PEAK TOTAL CARTONS UNLOADED IN ANY CALENDAR WEEK (DAYS 1-7)
	Locate
	STORE 0 TO GSUSPEAK
	STORE DATE() TO GPEAKSTART,GPEAKEND

	DO WHILE unloaded<GPEAKSTART+6 AND !EOF()
		IF DOW(unloaded)=2 
			EXIT
		ENDIF
		SKIP
	ENDDO

	STORE RECNO() TO TREC
	Locate

	SUM qty TO GSUSPEAK WHILE RECNO()<=TREC
	DO WHILE !EOF()
		STORE unloaded TO TDATE,GPEAKSTART
		STORE unloaded+6 TO GPEAKEND
		STORE 0 TO TEMPPEAK
		DO WHILE unloaded<(TDATE+7) and !EOF() 
			TEMPPEAK=TEMPPEAK+qty
			SKIP
			IF DOW(unloaded)=2
				EXIT
			endif
		enddo

		IF temppeak>gsuspeak
			STORE temppeak TO GSUSPEAK
		ENDIF
	enddo

	*The daily sheet [3]
	osheet1=oworkbook.worksheets[3]

	SELECT temp30
	Locate

	STORE 10 TO tnum
	DO WHILE !EOF()
		STORE ALLTRIM(STR(tnum)) TO anum

		osheet1.range("A"+anum).value = unloaded
		osheet1.range("C"+anum).value = QTY
		osheet1.range("E"+anum).value = SKUS
		osheet1.range("G"+anum).value = QTY/SKUS
		osheet1.range("I"+anum).value = CTNCUBE

		tnum=tnum+1
		SKIP
	ENDDO

	STORE TNUM TO SHEET3ENDROW

	*The Pallet Location Sheet [6]
	osheet1=oworkbook.worksheets[6]

	SELECT temp
	SET ORDER TO account
	Locate

	STORE 11 TO tnum
	STORE .f. to ENDFILE
	STORE 0 TO gx1,gx2,gx3,gx4
	DO WHILE !EOF()
		STORE accountid TO TACCT

		SELECT account
		LOCATE FOR accountid=TACCT
		STORE ACCTNAME TO xname

		SELECT TEMP
		STORE RECNO() TO TREC
		STORE 0 TO x1,x2,x3,x4,XPEAK
		DO WHILE ACCOUNTID=tacct AND !EOF()
			x1=x1+QTY
			x2=x2+NUMLOCS
			x3=x3+(CTNCUBE*QTY)
			x4=x4+1
			IF NUMLOCS>XPEAK
				STORE NUMLOCS TO xpeak
			ENDIF

			SKIP
		ENDDO
		GX1=GX1+X1
		GX2=GX2+X2
		GX3=GX3+X3
		GX4=GX4+X4
			
		STORE RECNO() TO ENDREC
		IF EOF()
			STORE .t. to ENDFILE
		ENDIF
							
		SET FILTER TO accountid=TACCT
		SET ORDER TO unloaded
		GO top
		STORE 0 TO SUStamt,gstart,gend

		GO top
		STORE 0 TO SaMT,SNUM
		STORE unloaded TO xstart

		DO WHILE unloaded<xSTART+6 AND !EOF()
			IF DOW(unloaded)=2 
				EXIT
			ENDIF
			SKIP
		ENDDO

		STORE RECNO() TO xrec
		GO top
		SUM NUMLOCS TO sustamt WHILE RECNO()<=xrec
		DO WHILE !EOF()
			STORE unloaded TO TDATE,xstart
			STORE unloaded+6 TO xend
			STORE 0 TO TEMPPEAK
			DO WHILE unloaded<(TDATE+7) and !EOF() 
				TEMPPEAK=TEMPPEAK+numlocs
				SKIP
				IF DOW(unloaded)=2
					EXIT
				endif
			enddo
			IF temppeak>sustamt
				STORE temppeak TO sustamt
			ENDIF
		ENDDO

		SET FILTER TO 
		SET ORDER TO account
		IF endfile
			GO bott
			SKIP
		else
			GOTO ENDREC
		endif

		STORE ALLTRIM(STR(tnum)) TO anum
		STORE "T"+anum TO value11

		osheet1.range("A"+anum).value = XNAME
		osheet1.range("B"+anum).value = X1
		osheet1.range("D"+anum).value = X2
		osheet1.range("F"+anum).value = X1/X2
		osheet1.range("H"+anum).value = X3/X1
		osheet1.range("J"+anum).value = X2/totnumdays
		osheet1.range("L"+anum).value = XPEAK
		osheet1.range("N"+anum).value = XPEAK/(X2/totnumdays)
		osheet1.range("P"+anum).value = SUSTAMT/5
		osheet1.range("R"+anum).value = (SUSTAMT/5)/((X2/totnumdays))

		tnum=tnum+1
	ENDDO


	SET FILTER TO 
	STORE 0 TO gxpeak
	SET ORDER TO UNLOADED
	Locate
	DO WHILE !EOF()
		STORE UNLOADED TO tdate
		SUM NUMLOCS TO xnum WHILE UNLOADED=tdate AND !EOF()
		IF XNUM>GXPEAK
			STORE XNUM TO gxpeak
		ENDIF
	ENDDO
	Locate
	****************************

	STORE 0 TO GXSUStamt,gXstart,gXend

	GO top
	STORE 0 TO SaMT,SNUM
	STORE unloaded TO xstart

	DO WHILE unloaded<xSTART+6 AND !EOF()
		IF DOW(unloaded)=2 
			EXIT
		ENDIF
		SKIP
	ENDDO

	STORE RECNO() TO xrec
	Locate
	SUM NUMLOCS TO GXsustamt WHILE RECNO()<=xrec
	DO WHILE !EOF()
		STORE unloaded TO TDATE,xstart
		STORE unloaded+6 TO xend
		STORE 0 TO TEMPPEAK
		DO WHILE unloaded<(TDATE+7) and !EOF() 
			TEMPPEAK=TEMPPEAK+numlocs
			SKIP
			IF DOW(unloaded)=2
				EXIT
			endif
		enddo
		IF temppeak>GXsustamt
			STORE temppeak TO GXsustamt
		ENDIF
	ENDDO
	**************************


	*The Monthly sheet [4] 
	SELECT temp30
	SUM qty,skus,qty*ctncube TO gmqty,gmskus,gmcube
	Locate

	STORE 10 TO tnum
	DO WHILE !EOF()
		STORE ALLTRIM(STR(tnum)) TO anum
		STORE MONTH(UNLOADED) TO tmonth
		STORE YEAR(UNLOADED) TO tyear
		STORE ALLTRIM(STR(tmonth)) TO cmonth
		STORE STR(YEAR(UNLOADED)) TO cyear
		IF LEN(CMONTH)=1
			STORE "0"+CMONTH TO CMONTH
		ENDIF
		STORE cmonth+'/'+cyear TO cdate

		STORE 0 TO mqty,mskus,mcube
		DO WHILE MONTH(UNLOADED)=tmonth AND YEAR(UNLOADED)=tyear AND !EOF()
			mqty=mqty+QTY
			mskus=mskus+SKUS
			mcube=mcube+QTY*CTNCUBE
			SKIP
		ENDDO

		osheet1=oworkbook.worksheets[4]
		osheet1.range("A"+anum).value = cdate
		osheet1.range("C"+anum).value = Mqty
		osheet1.range("E"+anum).value = Mskus
		osheet1.range("G"+anum).value = MQTY/MSKUS

		STORE mcube/mqty TO mcube
		osheet1.range("I"+anum).value = mcube

		tnum=tnum+1
	ENDDO

	STORE ALLTRIM(STR(tnum)) TO anum

	osheet1.range("A"+anum).value = "Totals"
	osheet1.range("C"+anum).value = gMqty
	osheet1.range("E"+anum).value = gMskus
	osheet1.range("G"+anum).value = gMQTY/gMSKUS

	STORE gmcube/gmqty TO gmcube
	osheet1.range("I"+anum).value = gmcube


	SELECT temp
	SET ORDER TO account
	SUM qty,skus,(ctncube*qty),numlocs TO GTOTQTY,gtotskus,gtotcube,gnumlocs
	Locate

	osheet1=oworkbook.worksheets[2]
	STORE 11 TO TNUM
	STORE .f. to ENDFILE
	DO WHILE !EOF()
		STORE accountid TO TACCT

		SELECT account
		LOCATE FOR accountid=TACCT
		STORE acctname TO TACCTNAME

		SELECT temp
		STORE RECNO() TO TREC
		SUM qty,skus,ctncube*qty TO T1,T2,T3 WHILE accountid=TACCT
		STORE RECNO() TO ENDREC

		IF EOF()
			STORE .t. to ENDFILE
		endif

		SET ORDER TO unloaded
		SET FILTER TO accountid=TACCT
		GO top

		*GET THE PEAK DAY FOR THIS ACCOUNT
		STORE 0 TO TPEAK
		STORE UNLOADED TO tpeakday
		DO WHILE accountid=TACCT AND !EOF()
			IF qty>TPEAK
				STORE qty TO TPEAK
				STORE UNLOADED TO tpeakday
			ENDIF
			SKIP
		ENDDO


		*GET THE SUSTAINED PEAK PERIOD AND DAY FOR THIS ACCOUNT
		Locate
		STORE 0 TO SUStamt
		STORE unloaded TO GSTART,GEND

		DO WHILE ACCOUNTID=tacct AND UNLOADED<gstart+6 AND !EOF()
			STORE unloaded TO GEND
			sustamt=sustamt+QTY
			SKIP
			IF DOW(unloaded)=2
				EXIT
			endif
		ENDDO

		DO WHILE ACCOUNTID=tacct AND !EOF()
			STORE unloaded TO TDATE,GSTART
			STORE unloaded+6 TO GEND
			STORE 0 TO TEMPPEAK
			DO WHILE ACCOUNTID=tacct AND unloaded<(TDATE+7) and !EOF() 
				TEMPPEAK=TEMPPEAK+qty
				SKIP
				IF DOW(unloaded)=2
					EXIT
				endif
			enddo

			IF temppeak>SUSTAMT
				STORE temppeak TO SUSTAMT
			ENDIF
		enddo

		SET ORDER TO account
		SET FILTER TO 
		IF !ENDFILE
			GOTO endrec
		ELSE
			GO bott
			SKIP
		ENDIF
			
		STORE T1/totnumdays TO TAVGCTNS
		STORE TPEAK/Tavgctns TO tPEAKTOAVG
		STORE t1/t2 TO T4
		STORE Iif(t1 = 0, 0, t3/t1) TO T3

		STORE ALLTRIM(STR(tnum)) TO anum
		STORE "X"+anum TO value13

		osheet1.range("A"+anum).value = TACCTNAME
		osheet1.range("B"+anum).value = T1
		osheet1.range("D"+anum).value = t2
		osheet1.range("F"+anum).value = t4
		osheet1.range("H"+anum).value = t3
		osheet1.range("J"+anum).value = tAVgctns
		osheet1.range("L"+anum).value = tpeak
		osheet1.range("N"+anum).value = tpeakDAY
		osheet1.range("P"+anum).value = tpeaktoavg
		osheet1.range("R"+anum).value = sustamt/5
		osheet1.range("T"+anum).value = DTOC(gstart)+'-'+DTOC(gend)
		osheet1.range("V"+anum).value = (sustamt/5)/tavgctns

		TNUM=TNUM+1
	ENDDO

	STORE ALLTRIM(STR(tnum)) TO anum
	STORE GTOTQTY/GTOTSKUS TO tavg
	STORE GTOTCUBE/GTOTQTY TO tavgcube
	STORE GTOTQTY/TOTNUMDAYS TO tavgctns
	STORE GTOTPEAK/TAVGCTNS TO tpeaktoavg

	IF allaccounts
		STORE "X"+anum TO value13

		osheet1.range("A"+anum).value = "TOTALS"
		osheet1.range("B"+anum).value = gtotqty
		osheet1.range("D"+anum).value = gtotskus
		osheet1.range("F"+anum).value = tavg
		osheet1.range("H"+anum).value = tAVGCUBE
		osheet1.range("J"+anum).value = tAVgctns
		osheet1.range("L"+anum).value = gtotpeak
		osheet1.range("N"+anum).value = gtotpeakDAY
		osheet1.range("P"+anum).value = tpeaktoavg
		osheet1.range("R"+anum).value = Gsuspeak/5
		osheet1.range("T"+anum).value = DTOC(Gpeakstart)+'-'+DTOC(gpeakend)
		osheet1.range("V"+anum).value = (gsuspeak/5)/tavgctns
	ENDIF


	*totals for worksheet3
	osheet1=oworkbook.worksheets[3]
	STORE ALLTRIM(STR(sheet3endrow)) TO anum

	osheet1.range("A"+anum).value = "Totals"
	osheet1.range("C"+anum).value = GTOTQTY
	osheet1.range("E"+anum).value = GTOTSKUS
	osheet1.range("G"+anum).value = GTOTQTY/GTOTSKUS
	osheet1.range("I"+anum).value = tavgcube


	STORE TNUM TO SHEET6ENDROW

	*totals for worksheet6
	IF allaccounts
		osheet1=oworkbook.worksheets[6]
		STORE ALLTRIM(STR(sheet6endrow)) TO anum

		STORE "T"+anum TO value11

		osheet1.range("A"+anum).value = "Totals"
		osheet1.range("B"+anum).value = GTOTQTY
		osheet1.range("D"+anum).value = Gnumlocs
		osheet1.range("F"+anum).value = GTOTQTY/Gnumlocs
		osheet1.range("H"+anum).value = tavgcube
		osheet1.range("J"+anum).value = gX2/totnumdays
		osheet1.range("L"+anum).value = gXPEAK
		osheet1.range("N"+anum).value = gXPEAK/(gX2/totnumdays)
		osheet1.range("P"+anum).value = gxSUSTAMT/5
		osheet1.range("R"+anum).value = (gxSUSTAMT/5)/((gX2/totnumdays))
	endif


	*CLOSE DATABASES all

	osheet1=oworkbook.worksheets[5]
	STORE 11 TO TNUM

	Select temp4
	Locate
	DO WHILE !EOF()
		STORE ALLTRIM(STR(tnum)) TO anum

		osheet1.range("A"+anum).value = acctname
		osheet1.range("C"+anum).value = containers
		osheet1.range("E"+anum).value = ctrqty
		osheet1.range("O"+anum).value = maxskus

		STORE containers+airshpts+dshpts TO ALLSHPTS
		osheet1.range("Q"+anum).value = totskus/allshpts
		osheet1.range("G"+anum).value = airshpts
		osheet1.range("I"+anum).value = airqty
		osheet1.range("K"+anum).value = dshpts
		osheet1.range("M"+anum).value = dqty

		tnum=tnum+1
		SKIP
	ENDDO

	IF ALLACCOUNTS
		STORE ALLTRIM(STR(tnum)) TO anum
		SUM containers,ctrqty,airshpts,airqty,dshpts,dqty,totskus TO T1,t2,t3,t4,t5,t6,t7

		osheet1.range("A"+anum).value = "TOTALS"
		osheet1.range("C"+anum).value = T1
		osheet1.range("E"+anum).value = T2

		STORE t1+t3+t5 TO ALLSHPTS
		osheet1.range("O"+anum).value = Totskus
		osheet1.range("Q"+anum).value = T7/ALLSHPTS
		osheet1.range("G"+anum).value = T3
		osheet1.range("I"+anum).value = T4
		osheet1.range("K"+anum).value = T5
		osheet1.range("M"+anum).value = T6
	ENDIF


	**** Begin Revenue
	Select temp30
	Locate

	create cursor temp31 ( ;
		accountid i, ;
		billtoid i, ;
		shiptoid c(8), ;
		company c(1), ;
		revacct c(10), ;
		wo_num c(6), ;
		invdate d, ;
		amount n(10,2), ;
		group c(14))

	Store Iif(allAccounts, "", "arhdrh.shiptokey=xshiptokey and ") to cAcctFilter

	select ;
		arhdrh.applyto, ;
		arhdrh.shiptokey, ;
		arhdrh.customerkey, ;
		arhdrh.customerponumber, ;
		arlinh.locationkey, ;
		arlinh.revenueacctkey, ;
		arhdrh.invoicedate, ;
		arlinh.unitprice*arlinh.qtyordered as invamt ;
		from arhdrh, arlinh ;
		where ;
		&cAcctFilter ;
		arhdrh.transactionnumber=arlinh.documentnumber ;
		and arhdrh.sysdocid=arlinh.sysdocid ;
		and between(arhdrh.invoicedate,tstart,tend) ;
		into cursor xtemp
	
	scan for invamt#0
		m.company = left(locationkey,1)
		m.amount = invamt
		m.billtoid = val(customerkey)
		m.shiptoid=shiptokey
		m.wo_num = customerponumber
		m.invdate = invoicedate

		m.revacct = revenueacctkey
		if substr(m.revacct,8,1)#substr(m.revacct,10,1) and substr(m.revacct,8,1)#'0'
			m.revacct=left(m.revacct,9)+substr(m.revacct,8,1)
		endif
		
		if !empty(shiptokey)
			m.accountid=eval("0x"+shiptokey)
		else
			if isdigit(applyto) or inlist(applyto,"Z","X")
				if seek(left(applyto,6),"xtemp","invnum")
					m.accountid=xtemp.accountid
				else
					loop
				endif
			else
				loop
			endif
		endif		
		insert into temp31 from memvar
	endscan

	Use in xtemp

    SELECT temp31
	INDEX ON invdate TAG INVDT
	Locate

	STORE 10 TO tnum
	STORE 0 TO GQTY,GCUBE,GH,GS,GSP,ginven
	osheet1=oworkbook.worksheets[1]
	osheet2=oworkbook.worksheets[12]

	DO WHILE !EOF()
		STORE ALLTRIM(STR(tnum)) TO anum
		STORE MONTH(INVdate) TO tmonth
		STORE YEAR(INVdate) TO tyear
		STORE ALLTRIM(STR(tmonth)) TO cmonth
		STORE STR(YEAR(INVdate)) TO cyear
		IF LEN(CMONTH)=1
			STORE "0"+CMONTH TO CMONTH
		ENDIF

		STORE cmonth+'/'+cyear TO cdate
		STORE 0 TO mHANDLING,mSTORAGE,mPROJECTS
		DO WHILE MONTH(INVdate)=tmonth AND YEAR(INVdate)=tyear AND !EOF()
			DO case
			Case toffice='N' AND COMPANY= 'I'
			Case toffice="C" AND (COMPANY= 'W' OR COMPANY= '1' OR COMPANY='2')
			Case toffice='M' AND COMPANY='M'
			Otherwise
				SKIP
				LOOP
			EndCase

			SELECT glchart
			LOCATE FOR code=temp31.revacct

			SELECT temp31
			DO case 
			Case glchart.glcode='13'
				mHANDLING=mHANDLING+amount
				GH=GH+amount
			Case glchart.glcode='14'
				mSTORAGE=mSTORAGE+amount
				GS=GS+amount
			Case glchart.glcode='16'
				mPROJECTS=mPROJECTS+amount
				GSP=GSP+amount
			EndCase
			SKIP
		ENDDO

		SELECT temp30
		SET FILTER TO MONTH(unloaded)=TMONTH AND YEAR(unloaded)=TYEAR
		Locate
		STORE 0 TO mqty,mskus,mcube
		DO WHILE MONTH(UNLOADED)=tmonth AND YEAR(UNLOADED)=tyear AND !EOF()
			mqty=mqty+QTY
			mskus=mskus+SKUS
			mcube=mcube+QTY*CTNCUBE
			GQTY=GQTY+qty
			GCUBE=GCUBE+qty*ctncube
			SKIP
		ENDDO
		IF MQTY>0
			STORE mcube/mqty TO mcube
		ELSE
			STORE 0 TO MCUBE
		endif

		SELECT temp31

		osheet1=oworkbook.worksheets[1]
		osheet2=oworkbook.worksheets[12]

		osheet1.range("A"+anum).value = cdate
		osheet2.range("A"+anum).value = cdate	
		osheet1.range("C"+anum).value = Mqty

		osheet1.range("E"+anum).value = Iif(mcube>0, mcube, osheet1.range("E"+anum).value)
		osheet1.range("G"+anum).value = Iif(Mhandling<>0, MHANDLING, osheet1.range("G"+anum).value)
		osheet1.range("I"+anum).value = Iif(mstorage<>0, MSTORAGE, osheet1.range("I"+anum).value)
		osheet2.range("G"+anum).value = Iif(mstorage<>0, MSTORAGE, osheet2.range("G"+anum).value)
		osheet1.range("K"+anum).value = Iif(mprojects<>0, MPROJECTS, osheet1.range("K"+anum).value)
		osheet1.range("M"+anum).value = Iif(mhandling+mstorage+mprojects<>0, ;
			Mhandling+mstorage+mprojects, osheet1.range("M"+anum).value)
			

		SELECT cargo
		IF tmonth<10
			STORE "0"+ALLTRIM(STR(tmonth)) TO zMONTH1
		ELSE
			STORE ALLTRIM(STR(tmonth)) TO zMONTH1
		endif
		
		STORE SUBSTR(ALLTRIM(cyear),3,2) TO zYEAR1
		STORE zmonth1+zyear1 TO ZZMONTH
		IF allaccounts
			SET FILTER TO zmonth=ZZMONTH
			GO top
		ELSE
			LOCATE FOR accountid=TACCT AND zmonth=zzmonth
		ENDIF

		STORE 0 TO TINVCTNS,TINVCUBE
		DO WHILE !EOF()
			TINVCTNS=TINVCTNS+ctnsinv
			TINVCUBE=TINVCUBE+(ctnsinv*avgcuftinv)
			IF !allaccounts
				EXIT
			endif
			skip
		ENDDO

		GINVEN=GINVEN+TINVCTNS
		IF TINVCUBE>0
			STORE tinvcube/tinvctns TO TAVG
		ELSE
			STORE 0 TO TAVG
		ENDIF
		
		IF mstorage>0
			STORE mstorage/ctnsinv TO AVGPERCTN
			STORE mstorage/TINVCUBE TO AVGPERCUBE
		ELSE
			STORE 0 TO avgperctn,avgpercube
		ENDIF
		
		osheet2.range("C"+anum).value = tinvctns
		osheet2.range("E"+anum).value = tavg
		osheet2.range("I"+anum).value = AVGPERCTN
		osheet2.range("K"+anum).value = AVGPERCUBE
		STORE "G"+anum TO GGFIELD
		STORE CTNSINV TO &GGFIELD
		
		SELECT temp31
		tnum=tnum+1
	ENDDO

	STORE ALLTRIM(STR(tnum)) TO anum

	osheet1.range("A"+anum).value = "Totals"
	osheet1.range("C"+anum).value = Gqty

	STORE Iif(gQTY>0, Gcube/Gqty, 0) TO Gcube

	osheet1.range("E"+anum).value = Iif(Gcube>0, Gcube, osheet1.range("E"+anum).value)
	osheet1.range("G"+anum).value = Iif(gh>0, gh, osheet1.range("G"+anum).value)
	osheet1.range("I"+anum).value = Iif(gs>0, gs, osheet1.range("I"+anum).value)
	osheet1.range("K"+anum).value = Iif(gsp>0, gsp, osheet1.range("K"+anum).value)
	osheet1.range("M"+anum).value = Iif(gh+gs+gsp>0, gsp+gh+gs, osheet1.range("M"+anum).value)
	
	osheet2.range("A"+anum).value = "Average"
	IF ginven>0
		STORE ginven/(VAL(anum)-10) TO avginven
	ELSE
		STORE 0 TO avginven
	endif
	osheet2.range("C"+anum).value = avginven

	STORE 10 TO GNUM
	DO WHILE GNUM<tnum
		STORE "G"+ALLTRIM(STR(gnum)) TO GGfield
		STORE &GGFIELD TO MINVEN
		STORE mINVEN/AVGINVEN TO PTA
		osheet2.range("M"+ALLTRIM(STR(gnum))).value = PTA
		GNUM=GNUM+1
	ENDDO

**************************************Now the revenue detail by account
	IF allaccounts
		*temp31 has all the invoices in the period
		*temp has all the inbound records for all the accounts in the period
		SELECT account
		SET ORDER TO accountid
		SELECT temp31
		locate
		SELECT temp
		LOCATE
		TNUM=TNUM+3

		SELECT temp31

		STORE 0 TO GQTY,GCUBE,GH,GS,GSP
		DO WHILE !EOF()
			STORE RECNO() TO TREC
			STORE MONTH(INVdate) TO tmonth
			STORE YEAR(INVdate) TO tyear
			STORE ALLTRIM(STR(tmonth)) TO cmonth
			STORE STR(YEAR(INVdate)) TO cyear
			IF LEN(CMONTH)=1
				STORE "0"+CMONTH TO CMONTH
			ENDIF

			STORE cmonth+'/'+cyear TO cdate
			STORE ALLTRIM(STR(tnum)) TO anum				
			osheet1.range("A"+anum).value = cdate
			tnum=tnum+1
			SELECT temp
			locate				

			DO WHILE !EOF()
				STORE 0 TO mHANDLING,mSTORAGE,mPROJECTS
				STORE ALLTRIM(STR(tnum)) TO anum
				STORE accountid TO TACCT
				SELECT account
				SEEK tacct
				STORE acctname TO TEMPACCTNAME
				
			    SELECT temp31
			    SET FILTER TO accountid=TACCT AND YEAR(invdate)=tyear AND MONTH(invdate)=tmonth
			    LOCATE
			    
				DO WHILE !EOF()
					DO case
					Case toffice='N' AND COMPANY= 'I'
					Case toffice="C" AND (COMPANY= 'W' OR COMPANY= '1' OR COMPANY='2')
					Case toffice='M' AND COMPANY='M'
					Otherwise
						SKIP
						LOOP
					EndCase

					SELECT glchart
					LOCATE FOR code=temp31.revacct
					SELECT temp31

					DO case 
					Case glchart.glcode='13'
						mHANDLING=mHANDLING+amount
						GH=GH+amount
					Case glchart.glcode='14'
						mSTORAGE=mSTORAGE+amount
						GS=GS+amount
					Case glchart.glcode='16'
						mPROJECTS=mPROJECTS+amount
						GSP=GSP+amount
					EndCase
					SKIP
				ENDDO

				SELECT temp
				SET FILTER TO accountid=TACCT AND MONTH(unloaded)=TMONTH AND YEAR(unloaded)=TYEAR
				Locate
				STORE 0 TO mqty,mskus,mcube
				DO WHILE !EOF()
					mqty=mqty+QTY
					mskus=mskus+SKUS
					mcube=mcube+QTY*CTNCUBE
					GQTY=GQTY+qty
					GCUBE=GCUBE+qty*ctncube
					SKIP
				ENDDO
				IF MQTY>0
					STORE mcube/mqty TO mcube
				ELSE
					STORE 0 TO MCUBE
				endif

				osheet1=oworkbook.worksheets[1]

				STORE ALLTRIM(STR(tnum)) TO anum		
				osheet1.range("A"+anum).value = tempacctname
				osheet1.range("C"+anum).value = Mqty
				osheet1.range("E"+anum).value = Iif(mcube>0, mcube, osheet1.range("E"+anum).value)
				osheet1.range("G"+anum).value = Iif(Mhandling>0, MHANDLING, osheet1.range("G"+anum).value)
				osheet1.range("I"+anum).value = Iif(mstorage>0, MSTORAGE, osheet1.range("I"+anum).value)
				osheet1.range("K"+anum).value = Iif(mprojects>0, MPROJECTS, osheet1.range("K"+anum).value)
				osheet1.range("M"+anum).value = Iif(mhandling+mstorage+mprojects>0, ;
					Mhandling+mstorage+mprojects, osheet1.range("M"+anum).value)

				SELECT temp
				SET FILTER TO 
				SEEK TACCT
				DO WHILE accountid=TACCT AND !EOF()
					SKIP
				enddo
				tnum=tnum+1
			ENDDO

			SELECT temp
			SET FILTER TO 
			SELECT temp31
			SET FILTER TO 
			GOTO TREC
			DO WHILE MONTH(invdate)=tmonth AND YEAR(invdate)=tyear AND !EOF()
				SKIP
			ENDDO
			TNUM=TNUM+1
		ENDDO
	ENDIF 

	If Used("temp31")
	  USE in temp31
	ENDIF
	IF USED("glchart")
	  USE IN glchart
    ENDIF
	IF USED("arhdrh")
	  USE IN arhdrh
    ENDIF
	IF USED("arlinh")
	  USE IN arlinh
    ENDIF
    
**************************************End revenue detail by account

	**these aliases are used in Outbound for different tables
	If Used("inwolog")
	  USE in inwolog
	EndIf
	If Used("indet")
	  USE in indet
	EndIf
	If Used("inloc")
	  USE in inloc
	EndIf
	If Used("temp")
	  USE IN TEMP
	EndIf
	If Used("temp2")
	  USE in temp2
	EndIf
EndIf &&End Inbound Section - InList(reporttype,1,3)
****************************************************


**** Return if only inbound
IF reporttype=1
	=closeData()

	WAIT clear
	oworkBook.save()
	oexcel.visible=.t.

	Return
EndIf
****


**** Begin Outbound
WAIT "Compiling outbound data. This process could take a few minutes... "+;
	"The output Excel spreadsheet will appear on completion." WINDOW NOWAIT NOCLEAR

USE wh!outship in 0
USE wh!outDET ORDER outshipID ALIAS DET IN 0
USE wh!outloc ORDER outdetID ALIAS LOC IN 0


IF ALLACCOUNTS
	STORE 'ALL ACCOUNTS' TO tacctname
ENDIF

STORE DTOC(tstart)+"-"+DTOC(tend) TO datestring
DO CASE
CASE toffice="C"
	STORE "San Pedro" TO tlocation
CASE toffice='N'
	STORE "Carteret" TO tlocation
CASE toffice='M'
	STORE "Miami" TO tlocation
	CASE toffice='Y'
	STORE "Carson2" TO tlocation
ENDCASE


if inlist(reporttype,2,3)
	CREATE CURSOR TEMP (ACCOUNTID N(10),acctname c(30),PROCESSED D, QTY N(10,0), PTS N(10,0), ;
		SKUS N(10,0),SWCS N(10,0),ctncube N(10,2),UPSCTNS N(10,0),numlocs n(10,0))

	CREATE CURSOR TEMPLOC (whseloc c(10))

	SELECT TEMP
	INDEX ON accountid TAG account
	INDEX ON DTOC(PROCESSED) TAG uniqueunl unique
	INDEX ON PROCESSED TAG PROCESSED

	Store Iif(allAccounts, "", "and accountId = tAcct") to cAcctFilter
	Store Iif(allAccounts, "", "and outshiph.accountId = tAcct") to cAcctFilter1
	SELECT accountid,outshipid,qty,picked,labeled,consignee,swcnum,swcover,cnee_ref,wo_num,ship_via,del_date FROM outship WHERE outship.WO_DATE>=TSTART-30 AND outship.wo_date<tend+30 &cAcctFilter ;
		UNION ;
		SELECT accountid,outshipid,qty,picked,labeled,consignee,swcnum,swcover,cnee_ref,wo_num,ship_via,del_date FROM outshiph WHERE outshiph.WO_DATE>=TSTART-30 AND outshiph.wo_date<tend+30 &cAcctFilter1 ;
	  INTO cursor log readWrite

	INDEX ON CONSIGNEE+CNEE_REF+STR(WO_NUM,8,0)+STR(ACCOUNTID,4,0) TAG MIAMI
	INDEX ON STR(ACCOUNTID,4)+swcnum+swcover TAG ACCTPO

	IF TOFFICE='M'
		SET ORDER TO MIAMI
	ENDIF

	LOCATE
	IF EOF()
		strMsg = "No outbound data for this account."
		=MessageBox(strMsg, 16, "Metrics Report")

		=CloseData()
		RETURN
	ENDIF

	IF ALLACCOUNTS
		DO WHILE !EOF()
			STORE outshipid TO tid
			STORE accountid TO TACCT

			SELECT DET
			SEEK LOG.OUTSHIPID
			
			IF EOF()
				SELECT odeth
				SEEK log.outshipid
			ENDIF

			SELECT LOG
			IF DET.UNITS OR odeth.units
				replace qty WITH 0
				SKIP
				LOOP
			ENDIF

			STORE picked TO TDATE
			IF EMPTYnul(picked) AND !EMPTYnul(labeled)
				STORE labeled TO TDATE
			ENDIF
			IF EMPTYnul(labeled) AND EMPTYnul(picked) AND !EMPTYnul(del_date)
				STORE del_date TO TDATE
			EndIf

			IF tdate<tstart OR tdate>tend 
				replace qty WITH 0
				SKIP
				LOOP
			ENDIF

			*HERE YOU HAVE A PICK TICKET THAT FITS THE DATE PARAMETERS
			STORE CONSIGNEE TO tcnee
			STORE swcnum TO tswcnum
			STORE swcover TO tswcover
			STORE CNEE_REF TO tpo
			STORE WO_NUM TO two
			STORE CNEE_REF TO tref
			SELECT TEMP

			SEEK tdate
			DO WHILE processed=TDATE AND !EOF()
				IF accountid=TACCT
					exit
				ENDIF
				skip
			ENDDO

			IF !(accountid=TACCT AND PROCESSED=TDATE)
				APPEND BLANK
				REPLACE PROCESSED WITH tdate
				replace accountid WITH TACCT

				SELECT account
				LOCATE FOR accountid=tacct
				STORE acctname TO TACCTNAME

				SELECT temp
				replace acctname WITH tacctname
			ENDIF
						
			SELECT LOG
			STORE 0 TO tswcs
			IF TOFFICE='M'
				DO WHILE CONSIGNEE=tcnee AND CNEE_REF=tref AND WO_NUM = two AND ACCOUNTID=tacct AND !EOF()
					STORE 0 TO TITEMS,tcube,tqty

					SELECT DET
					SEEK tid
					IF !EOF()
						DO WHILE outshipid=tid AND !EOF()
							IF !units
								titems=titems+1
								tcube=tcube+(TOTQTY*CTNCUBE)
								tqty=tqty+TOTQTY
								STORE OUTDETID TO toutdetid
								SELECT LOC
								SEEK toutdetid
								DO WHILE OUTDETID=toutdetid AND !EOF()
									STORE whseloc TO TLOC
									SELECT temploc
									APPEND BLANK
									replace whseloc WITH TLOC
									SELECT loc
									SKIP
								ENDDO

								SELECT DET
					 		endif

							SKIP
						ENDDO
					ELSE
						 SELECT odeth
						 SEEK tid
						 DO WHILE outshipid=tid AND !EOF()
							IF !units
								titems=titems+1
								tcube=tcube+(TOTQTY*CTNCUBE)
								tqty=tqty+TOTQTY
								STORE OUTDETID TO toutdetid
								SELECT oLOCh
								SEEK toutdetid
								DO WHILE OUTDETID=toutdetid AND !EOF()
									STORE whseloc TO TLOC
									SELECT temploc
									APPEND BLANK
									replace whseloc WITH TLOC
									SELECT oloch
									SKIP
								ENDDO

								SELECT oDETh
					 		endif
							SKIP
						ENDDO
					ENDIF

					SELECT log
					replace qty WITH TQTY
					IF tqty>0
						SELECT TEMP
						
						IF LOG.SHIP_VIA='UPS' OR LOG.SHIP_VIA='FEDEX' OR LOG.SHIP_VIA='RPS' OR AT('AIR',LOG.SHIP_VIA)>0 OR AT('POST',LOG.SHIP_VIA)>0
							REPLACE UPSCTNS WITH UPSCTNS+tqty
						ENDIF
						REPLACE CTNCUBE WITH ((CTNCUBE*QTY)+(tcube))/(QTY+tqty)
						REPLACE QTY WITH QTY+tqty
						REPLACE SKUS WITH SKUS+titems
						REPLACE PTS WITH PTS+1
					ENDIF

					SELECT LOG
					SKIP
				ENDDO
			ELSE
				DO WHILE ACCOUNTID=tacct AND SWCNUM=tswcnum AND SWCOVER=tswcover AND !EOF()
					STORE 0 TO TITEMS,tcube,tqty

					SELECT DET
					SEEK tid
					IF !EOF()
						DO WHILE outshipid=tid AND !EOF()
							IF !units
								titems=titems+1
								tcube=tcube+(TOTQTY*CTNCUBE)
								tqty=tqty+TOTQTY
								STORE OUTDETID TO toutdetid
								SELECT LOC
								SEEK toutdetid
								DO WHILE OUTDETID=toutdetid AND !EOF()
									STORE whseloc TO TLOC
									SELECT temploc
									APPEND BLANK
									replace whseloc WITH TLOC
									SELECT loc
									SKIP
								ENDDO

								SELECT DET
					 		endif

							SKIP
						ENDDO
					ELSE
						SELECT odeth
						SEEK tid
						DO WHILE outshipid=tid AND !EOF()
							IF !units
								titems=titems+1
								tcube=tcube+(TOTQTY*CTNCUBE)
								tqty=tqty+TOTQTY
								STORE OUTDETID TO toutdetid
								SELECT oLOCh
								SEEK toutdetid
								DO WHILE OUTDETID=toutdetid AND !EOF()
									STORE whseloc TO TLOC
									SELECT temploc
									APPEND BLANK
									replace whseloc WITH TLOC
									SELECT oloch
									SKIP
								ENDDO

								SELECT oDETh
					 		endif

							SKIP
						ENDDO
					ENDIF

					SELECT log
					replace qty WITH TQTY
					IF tqty>0
						SELECT TEMP
						
						IF LOG.SHIP_VIA='UPS' OR LOG.SHIP_VIA='FEDEX' OR LOG.SHIP_VIA='RPS' OR AT('AIR',LOG.SHIP_VIA)>0 OR AT('POST',LOG.SHIP_VIA)>0
							REPLACE UPSCTNS WITH UPSCTNS+tqty
						ENDIF
						REPLACE CTNCUBE WITH ((CTNCUBE*QTY)+(tcube))/(QTY+tqty)
						REPLACE QTY WITH QTY+tqty
						REPLACE SKUS WITH SKUS+titems
						REPLACE PTS WITH PTS+1
					ENDIF

					SELECT LOG
					SKIP
				ENDDO
			ENDIF

			SELECT temploc
			INDEX ON whseloc TAG whseloc unique
			COUNT TO TPALLETS
			zap

			SELECT TEMP
			replace numlocs WITH numlocs+TPALLETS
			REPLACE SWCS WITH SWCS+1

			SELECT log
		ENDDO

		SELECT temploc
		INDEX ON whseloc TAG whseloc unique
		COUNT TO TPALLETS
		zap

		SELECT TEMP
		replace numlocs WITH TPALLETS

		SELECT log
	Else &&allaccounts
		DO WHILE ACCOUNTID=tacct AND !EOF()
			STORE outshipid TO tid

			SELECT DET
			SEEK LOG.OUTSHIPID
			SELECT oDETh
			INDEX ON OUTSHIPID TAG OUTSHIPID
			SEEK LOG.OUTSHIPID

			SELECT LOG

*******commented out 10/14/16 TMARG
*!*				IF DET.UNITS OR odeth.units
*!*					replace qty WITH 0
*!*					SKIP
*!*					LOOP
*!*				ENDIF

			STORE picked TO TDATE
			IF EMPTYnul(picked) AND !EMPTYnul(labeled)
				STORE labeled TO TDATE
			ENDIF
			IF EMPTYnul(labeled) AND EMPTYnul(picked) AND !EMPTYnul(del_date)
				STORE del_date TO TDATE
			endif

			IF tdate<tstart OR tdate>tend OR (ACCOUNTID=449 AND CONSIGNEE='LOLLY')
				replace qty WITH 0
				SKIP
				LOOP
			ENDIF

			*HERE YOU HAVE A PICK TICKET THAT FITS THE DATE PARAMETERS
			STORE CONSIGNEE TO tcnee
			STORE SWCNUM TO tswcnum
			STORE SWCOVER TO tswcover
			STORE CNEE_REF TO tpo
			STORE WO_NUM TO two
			STORE CNEE_REF TO tref

			SELECT TEMP
			SEEK tdate
			DO WHILE processed=TDATE AND !EOF()
				IF accountid=TACCT
					exit
				ENDIF
				skip
			ENDDO

			IF !(accountid=TACCT AND PROCESSED=TDATE)
				APPEND BLANK
				REPLACE PROCESSED WITH tdate
				replace accountid WITH TACCT

				SELECT account
				LOCATE FOR accountid=tacct
				STORE acctname TO TACCTNAME

				SELECT temp
				replace acctname WITH tacctname
			ENDIF
						
			STORE 0 TO tswcs
			SELECT LOG
			IF TOFFICE='M'
				DO WHILE CONSIGNEE=tcnee AND CNEE_REF=tref AND WO_NUM = two AND ACCOUNTID=tacct AND !EOF()
					STORE 0 TO TITEMS,tcube,tqty

					SELECT DET
					SEEK tid
					IF !EOF()
						DO WHILE outshipid=tid AND !EOF()
							IF !units
								titems=titems+1
								tcube=tcube+(TOTQTY*CTNCUBE)
								tqty=tqty+TOTQTY
								STORE OUTDETID TO toutdetid
								SELECT LOC
								SEEK toutdetid
								DO WHILE OUTDETID=toutdetid AND !EOF()
									STORE whseloc TO TLOC
									SELECT temploc
									APPEND BLANK
									replace whseloc WITH TLOC
									SELECT loc
									SKIP
								ENDDO

								SELECT DET
					 		endif

							SKIP
						ENDDO
					ELSE
						SELECT odeth
						SEEK tid
						DO WHILE outshipid=tid AND !EOF()
							IF !units
								titems=titems+1
								tcube=tcube+(TOTQTY*CTNCUBE)
								tqty=tqty+TOTQTY
								STORE OUTDETID TO toutdetid
								SELECT oLOCh
								SEEK toutdetid
								DO WHILE OUTDETID=toutdetid AND !EOF()
									STORE whseloc TO TLOC
									SELECT temploc
									APPEND BLANK
									replace whseloc WITH TLOC
									SELECT oloch
									SKIP
								ENDDO

								SELECT oDETh
					 		endif

							SKIP
						ENDDO
					ENDIF
						
					SELECT log
					replace qty WITH TQTY
					
					IF tqty>0
						SELECT TEMP
						IF LOG.SHIP_VIA='UPS' OR LOG.SHIP_VIA='FEDEX' OR LOG.SHIP_VIA='RPS' OR AT('AIR',LOG.SHIP_VIA)>0 OR AT('POST',LOG.SHIP_VIA)>0
							REPLACE UPSCTNS WITH UPSCTNS+tqty
						ENDIF

						REPLACE CTNCUBE WITH ((CTNCUBE*QTY)+(tcube))/(QTY+tqty)
						REPLACE QTY WITH QTY+tqty
						REPLACE SKUS WITH SKUS+titems
						REPLACE PTS WITH PTS+1
					ENDIF
					
					SELECT LOG
					SKIP
				ENDDO
			Else &&toffice = 'M'
				DO WHILE ACCOUNTID=tacct AND SWCNUM=tswcnum AND SWCOVER=tswcover AND !EOF()
					STORE 0 TO TITEMS,tcube,tqty

					SELECT DET
					SEEK tid
					IF !EOF()
						DO WHILE outshipid=tid AND !EOF()
							IF !units
								titems=titems+1
								tcube=tcube+(TOTQTY*CTNCUBE)
								tqty=tqty+TOTQTY
								STORE OUTDETID TO toutdetid

								SELECT LOC
								SEEK toutdetid
								DO WHILE OUTDETID=toutdetid AND !EOF()
									STORE whseloc TO TLOC

									SELECT temploc
									APPEND BLANK
									replace whseloc WITH TLOC

									SELECT loc
									SKIP
								ENDDO
								
								SELECT DET
					 		ENDIF
							SKIP
						ENDDO
					ELSE
						SELECT odeth
						SEEK tid
					
						DO WHILE outshipid=tid AND !EOF()
							IF !units
								titems=titems+1
								tcube=tcube+(TOTQTY*CTNCUBE)
								tqty=tqty+TOTQTY
								STORE OUTDETID TO toutdetid

								SELECT oLOCh
								SEEK toutdetid
								DO WHILE OUTDETID=toutdetid AND !EOF()
									STORE whseloc TO TLOC

									SELECT temploc
									APPEND BLANK
									replace whseloc WITH TLOC

									SELECT oloch
									SKIP
								ENDDO
								
								SELECT oDETh
					 		ENDIF
							SKIP
						ENDDO
					ENDIF

					SELECT log
					replace qty WITH TQTY
 					
					IF tqty>0
						SELECT TEMP
						IF LOG.SHIP_VIA='UPS' OR LOG.SHIP_VIA='FEDEX' OR LOG.SHIP_VIA='RPS' OR AT('AIR',LOG.SHIP_VIA)>0 OR AT('POST',LOG.SHIP_VIA)>0
							REPLACE UPSCTNS WITH UPSCTNS+tqty
						ENDIF

						REPLACE CTNCUBE WITH ((CTNCUBE*QTY)+(tcube))/(QTY+tqty)
						REPLACE QTY WITH QTY+tqty
						REPLACE SKUS WITH SKUS+titems
						REPLACE PTS WITH PTS+1
					ENDIF
					
					SELECT LOG
					SKIP
				ENDDO
			ENDIF

			SELECT temploc
			INDEX ON whseloc TAG whselog unique
			COUNT TO TPALLETS
			zap

			SELECT TEMP
			REPLACE SWCS WITH SWCS+1
			replace numlocs WITH numlocs + TPALLETS
			SELECT LOG
		ENDDO
	ENDIF &&ALLACCOUNTS


	*CLOSE DATABASES ALL
SET STEP ON 
	IF reporttype=2
		COPY FILE s:\metrics\whserpt.xls TO &cFileName
		oexcel=createobject("excel.application")
		oworkbook=oexcel.workbooks.open(cFileName)
	endif

	*Headings for all three worksheets
	IF allaccounts
		STORE "ALL ACCOUNTS" TO tacctname
	ENDIF


	osheet1=oworkbook.worksheets[7]
	osheet1.range("B4").value=tacctname
	osheet1.range("B5").value=DATESTRING
	osheet1.range("B6").value=TLOCATION

	osheet1=oworkbook.worksheets[8]
	osheet1.range("B4").value=tacctname
	osheet1.range("B5").value=DATESTRING
	osheet1.range("B6").value=TLOCATION

	osheet1=oworkbook.worksheets[9]
	osheet1.range("B4").value=tacctname
	osheet1.range("B5").value=DATESTRING
	osheet1.range("B6").value=TLOCATION

	osheet1=oworkbook.worksheets[10]
	osheet1.range("B4").value=tacctname
	osheet1.range("B5").value=DATESTRING
	osheet1.range("B6").value=TLOCATION

	osheet1=oworkbook.worksheets[11]
	osheet1.range("B4").value=tacctname
	osheet1.range("B5").value=DATESTRING
	osheet1.range("B6").value=TLOCATION

	osheet1=oworkbook.worksheets[12]
	osheet1.range("B4").value=tacctname
	osheet1.range("B5").value=DATESTRING
	osheet1.range("B6").value=TLOCATION


	SELECT TEMP
	Set Order to processed

	STORE 11 TO TNUM
	Select * from temp group by ACCOUNTID into cursor temp2 readWrite
	DO WHILE !EOF()
		STORE ACCOUNTID TO tacct

		SELECT TEMP
		SET FILTER TO ACCOUNTID=tacct
		GO TOP
		STORE acctname TO TACCTNAME
		osheet1=oworkbook.worksheets[7]

		DO WHILE !EOF()
			SUM QTY,PTS,SKUS,UPSCTNS,swcs,qty*ctncube  TO  tqty,tpts,tskus,tups,TSWCS,tcube
			COUNT TO TCOUNT
			GO top
			STORE qty TO TPEAK
			DO WHILE !EOF()
				IF qty>TPEAK
					STORE qty TO TPEAK
				ENDIF
				SKIP
			ENDDO
						
			*now the sustained peak								
			GO top
			STORE 0 TO oSUSPEAK
			STORE DATE() TO oPEAKSTART,oPEAKEND
			DO WHILE processed<oPEAKSTART+6 AND !EOF()
				IF DOW(processed)=2 
					EXIT
				ENDIF
				SKIP
			ENDDO

			STORE RECNO() TO TREC
			GO top
			SUM qty TO oSUSPEAK WHILE RECNO()<=TREC

			DO WHILE !EOF()
				STORE processed TO TDATE,oPEAKSTART
				STORE processed+6 TO oPEAKEND
				STORE 0 TO TEMPPEAK
				DO WHILE processed<(TDATE+7) and !EOF() 
					TEMPPEAK=TEMPPEAK+qty
					SKIP
					IF DOW(processed)=2
						EXIT
					endif
				enddo
				IF temppeak>osuspeak
					STORE temppeak TO oSUSPEAK
				ENDIF
			enddo

			STORE ALLTRIM(STR(tnum)) TO anum

			osheet1.range("A"+anum).value = TACCTNAME
			osheet1.range("C"+anum).value = TQTY
			osheet1.range("E"+anum).value = TPTS
			osheet1.range("G"+anum).value = TSKUS
			osheet1.range("I"+anum).value = TSWCS
			osheet1.range("K"+anum).value = TQTY/TPTS
			osheet1.range("M"+anum).value = TQTY/TSWCS
			osheet1.range("O"+anum).value = TUPS
			osheet1.range("Q"+anum).value = TQTY/Totnumdays

			STORE tqty/totnumdays TO TAVG
			osheet1.range("S"+anum).value = Tpeak
			osheet1.range("U"+anum).value = tpeak/tavg
			osheet1.range("W"+anum).value = oSUSPEAK/5
			osheet1.range("Y"+anum).value = DTOC(opeakstart)+'-'+DTOC(opeakend)
			osheet1.range("AA"+anum).value = (osuspeak/5)/tavg

			tnum=tnum+1
		ENDDO

		SELECT TEMP2
		SKIP
	ENDDO

	IF allaccounts
		SELECT temp
		SET FILTER TO 
		SUM QTY,PTS,SKUS,UPSCTNS,swcs,qty*ctncube  TO  tqty,tpts,tskus,tups,TSWCS,tcube

		GO top
		STORE 0 TO TPEAK
		DO WHILE !EOF()
			STORE processed TO TDATE
			STORE 0 TO THISDATE
			DO WHILE processed=TDATE AND !EOF()
				THISDATE=THISDATE+qty
				SKIP
			ENDDO
			IF thisdate>TPEAK
				STORE thisdate TO TPEAK
			endif
		ENDDO

		*LOOK FOR THE PEAK TOTAL CARTONS UNLOADED IN ANY CALENDAR WEEK (DAYS 1-7)
		GO top
		STORE 0 TO oSUSPEAK
		STORE DATE() TO oPEAKSTART,oPEAKEND
		DO WHILE processed<oPEAKSTART+6 AND !EOF()
			IF DOW(processed)=2 
				EXIT
			ENDIF
			SKIP
		ENDDO

		STORE RECNO() TO TREC
		GO top
		SUM qty TO oSUSPEAK WHILE RECNO()<=TREC
		DO WHILE !EOF()
			STORE processed TO TDATE,oPEAKSTART
			STORE processed+6 TO oPEAKEND
			STORE 0 TO TEMPPEAK
			DO WHILE processed<(TDATE+7) and !EOF() 
				TEMPPEAK=TEMPPEAK+qty
				SKIP
				IF DOW(processed)=2
					EXIT
				endif
			enddo
			IF temppeak>osuspeak
				STORE temppeak TO oSUSPEAK
			ENDIF
		enddo

		STORE ALLTRIM(STR(tnum)) TO anum

		osheet1.range("A"+anum).value = "TOTALS"
		osheet1.range("C"+anum).value = TQTY
		osheet1.range("E"+anum).value = TPTS
		osheet1.range("G"+anum).value = TSKUS
		osheet1.range("I"+anum).value = TSWCS
		osheet1.range("K"+anum).value = TQTY/TPTS
		osheet1.range("M"+anum).value = TQTY/TSWCS
		osheet1.range("O"+anum).value = TUPS
		osheet1.range("Q"+anum).value = TQTY/Totnumdays

		STORE tqty/totnumdays TO TAVG
		osheet1.range("S"+anum).value = Tpeak
		osheet1.range("U"+anum).value = tpeak/tavg
		osheet1.range("W"+anum).value = oSUSPEAK/5
		osheet1.range("Y"+anum).value = DTOC(opeakstart)+'-'+DTOC(opeakend)
		osheet1.range("AA"+anum).value = (osuspeak/5)/tavg
	EndIf


	**now sheet 11 - the Pallet Pull report
	osheet1=oworkbook.worksheets[11]

	STORE 11 TO TNUM

	SELECT temp
	SET ORDER TO processed
	Locate
	DO WHILE !EOF()
		STORE processed TO TDATE
		STORE RECNO() TO TREC
		SUM QTY,PTS,SKUS,UPSCTNS,swcs,qty*ctncube,numlocs TO tqty,tpts,tskus,tups,TSWCS,tcube,TNUMLOCS WHILE processed=TDATE

		STORE ALLTRIM(STR(tnum)) TO anum

		osheet1.range("A"+anum).value = TDATE
		osheet1.range("C"+anum).value = TQTY
		osheet1.range("E"+anum).value = TNUMLOCS
		osheet1.range("G"+anum).value = TSWCS
		osheet1.range("I"+anum).value = TNUMLOCS/TSWCS
		osheet1.range("K"+anum).value = Tqty/TSWCS
		osheet1.range("M"+anum).value = Tqty/TNUMLOCS

		STORE tcube/tqty TO TAVGCUBE
		osheet1.range("O"+anum).value = TAVGCUBE
		tnum=tnum+1
	EndDo

	SUM QTY,PTS,SKUS,UPSCTNS,swcs,qty*ctncube,numlocs  TO  tqty,tpts,tskus,tups,TSWCS,tcube,TNUMLOCS

	STORE ALLTRIM(STR(tnum)) TO anum

	osheet1.range("A"+anum).value = "TOTALS"
	osheet1.range("C"+anum).value = TQTY
	osheet1.range("E"+anum).value = Tnumlocs
	osheet1.range("G"+anum).value = TSWCS
	osheet1.range("I"+anum).value = Tnumlocs/tswcs
	osheet1.range("K"+anum).value = Tqty/TSWCS
	osheet1.range("M"+anum).value = Tqty/TNUMLOCS

	STORE tcube/tqty TO TAVGCUBE
	osheet1.range("O"+anum).value = TAVGCUBE

SET STEP ON 
	**now sheet 8 - the daily report
	osheet1=oworkbook.worksheets[8]
	STORE 11 TO TNUM

	SELECT temp
	SET ORDER TO processed
	Locate
	DO WHILE !EOF()
		STORE processed TO TDATE
		STORE RECNO() TO TREC
		SUM QTY,PTS,SKUS,UPSCTNS,swcs,qty*ctncube  TO  tqty,tpts,tskus,tups,TSWCS,tcube WHILE processed=TDATE

		STORE ALLTRIM(STR(tnum)) TO anum

		osheet1.range("A"+anum).value = TDATE
		osheet1.range("C"+anum).value = TQTY
		osheet1.range("E"+anum).value = TPTS
		osheet1.range("G"+anum).value = TSKUS
		osheet1.range("I"+anum).value = TSWCS
		osheet1.range("K"+anum).value = TUPS
		osheet1.range("M"+anum).value = TQTY/TPTS
		osheet1.range("O"+anum).value = tqty/TSWCS

		STORE tcube/tqty TO TAVGCUBE
		osheet1.range("Q"+anum).value = tavgcube
		tnum=tnum+1
	ENDDO

	SUM QTY,PTS,SKUS,UPSCTNS,swcs,qty*ctncube  TO  tqty,tpts,tskus,tups,TSWCS,tcube

	STORE ALLTRIM(STR(tnum)) TO anum

	osheet1.range("A"+anum).value = "TOTALS"
	osheet1.range("C"+anum).value = TQTY
	osheet1.range("E"+anum).value = TPTS
	osheet1.range("G"+anum).value = TSKUS
	osheet1.range("I"+anum).value = TSWCS
	osheet1.range("K"+anum).value = TUPS
	osheet1.range("M"+anum).value = TQTY/TPTS
	osheet1.range("O"+anum).value = tqty/TSWCS

	STORE tcube/tqty TO TAVGCUBE
	osheet1.range("Q"+anum).value = tavgcube


	**now sheet 3 - the Monthly report
	osheet1=oworkbook.worksheets[9]
	STORE 11 TO TNUM

	SELECT temp
	SET ORDER TO processed
	Locate
	DO WHILE !EOF()
		STORE ALLTRIM(STR(tnum)) TO anum
		STORE MONTH(processed) TO tmonth
		STORE YEAR(processed) TO tyear
		STORE ALLTRIM(STR(tmonth)) TO cmonth
		STORE STR(YEAR(processed)) TO cyear

		IF LEN(CMONTH)=1
			STORE "0"+CMONTH TO CMONTH
		ENDIF
		STORE cmonth+'/'+cyear TO cdate

		STORE 0 TO mqty,mpts,mskus,mups,mswcs,mcube
		DO WHILE MONTH(processed)=tmonth AND YEAR(processed)=tyear AND !EOF()
			MQTY=MQTY+qty
			MPTS=MPTS+pts
			MSKUS=MSKUS+skus
			MUPS=MUPS+upsctns
			MSWCS=MSWCS+swcs
			MCUBE=MCUBE+(qty*ctncube)
			SKIP
		ENDDO

		STORE ALLTRIM(STR(tnum)) TO anum

		osheet1.range("A"+anum).value = cdate
		osheet1.range("C"+anum).value = mqty
		osheet1.range("E"+anum).value = mpts
		osheet1.range("G"+anum).value = mskus
		osheet1.range("I"+anum).value = mswcs
		osheet1.range("K"+anum).value = mups
		osheet1.range("M"+anum).value = mqty/mpts
		osheet1.range("O"+anum).value = mqty/mswcs

		STORE mcube/mqty TO TAVGCUBE
		osheet1.range("Q"+anum).value = tavgcube

		tnum=tnum+1
	ENDDO

	SUM QTY,PTS,SKUS,UPSCTNS,swcs,qty*ctncube  TO  tqty,tpts,tskus,tups,TSWCS,tcube

	STORE ALLTRIM(STR(tnum)) TO anum

	osheet1.range("A"+anum).value = "TOTALS"
	osheet1.range("C"+anum).value = TQTY
	osheet1.range("E"+anum).value = TPTS
	osheet1.range("G"+anum).value = TSKUS
	osheet1.range("I"+anum).value = TSWCS
	osheet1.range("K"+anum).value = TUPS
	osheet1.range("M"+anum).value = TQTY/TPTS
	osheet1.range("O"+anum).value = tqty/TSWCS

	STORE tcube/tqty TO TAVGCUBE
	osheet1.range("Q"+anum).value = tavgcube


	**now sheet 4 - Customer Concentration
	create cursor xrpt (zmonth c(4), accountid i, mass n(8), acctname c(30), dept n(8), bout n(8), total n(8),swcs n(10), ;
	massswcs n(10), boutswcs n(10), deptswcs n(10), month n(2), year n(4))
	index on zmonth+str(accountid) tag zorder


	osheet1=oworkbook.worksheets[10]
	SELECT log
	INDEX ON accountid TAG account
	Locate
	DO WHILE !EOF()
		store accountid TO tacct

		Select account
		Locate for accountid = tAcct
		store ACCTNAME TO tacctname

		select log
		Count while accountId = tAcct &&get to next acct

	    select dt2month(outship.ptdate) as zmonth, outship.accountid, outship.ship_via, outship.swcnum, outship.swcover ;
			from outship ;
			where between(outship.ptdate,tstart,tend) and outship.accountid=tacct ;
		  union ;
			select dt2month(outshiph.ptdate) as zmonth, outshiph.accountid, outshiph.ship_via, outshiph.swcnum, outshiph.swcover ;
			from outshiph ;
			where between(outshiph.ptdate,tstart,tend) and outshiph.accountid=tacct ;
			group by swcnum, swcover ;
		  into cursor TEMP2 readwrite

		Select t.swcnum, t.swcover, style, color, id, pack, os.outshipid from temp2 t ;
				left join outship os on os.swcnum+os.swcover=t.swcnum+t.swcover ;
				left join outdet od on os.outshipid = od.outshipid ;
			  where !empty(t.swcnum) ;
			union select t2.swcnum, t2.swcover, style, color, id, pack, osh.outshipid from temp2 t2 ;
				left join outshiph osh on osh.swcnum+osh.swcover=t2.swcnum+t2.swcover ;
				left join odeth odh on osh.outshipid = odh.outshipid ;
			  where !empty(t2.swcnum) ;
			group by 1,2,3,4,5,6 ;
		  into cursor csrSkuCnt readwrite

		delete for empty(outshipid)

		select TEMP2
		Scan
			SCATTER MEMVAR
			if !seek(m.zmonth+str(m.accountid,10),"xrpt","zorder")
				insert into xrpt (zmonth, accountid) values (m.zmonth, m.accountid)
			endif

			replace xrpt.acctname WITH tacctname

			If !Empty(m.swcnum+m.swcover) &&toffice # "M" &&no swc nums for miami, this section will not work
				select outship
				SET ORDER TO swcnumover
				SEEK m.swcnum+m.swcover

				IF EOF()
					SELECT outshiph
					SET ORDER TO SWCNUMOVER
					SEEK m.swcnum+m.swcover
				ENDIF

				sum ctnqty to xctnqty while swcnum+swcover=m.swcnum+m.swcover

				if inlist(m.ship_via,&xsmallpackagecarriers) and !inlist(m.ship_via,&xnotsmallpackagecarriers)
					replace xrpt.bout with xrpt.bout+xctnqty in xrpt
					replace xrpt.swcs WITH xrpt.swcs+1 IN xrpt
				ELSE
	*!*					select style, color, id, pack ;
	*!*						from outship, outdet ;
	*!*						where outship.outshipid=outdet.outshipid and outship.accountid=m.accountid and swcnum+swcover=m.swcnum+m.swcover ;
	*!*						UNION ;
	*!*						select style, color, id, pack ;
	*!*						from outshiph, odeth ; 
	*!*						where outshiph.outshipid=odeth.outshipid and outshiph.accountid=m.accountid and swcnum+swcover=m.swcnum+m.swcover ;
	*!*						group by 1,2,3,4 ;
	*!*					  into cursor xsku

	*!*					xnumsku=reccount("xsku")

					Select csrSkuCnt
					Count to xnumsku for swcnum = m.swcnum and swcover = m.swcover
					
					xavgctnsku=xctnqty/xnumsku

					do case
					case xctnqty>=150 and xavgctnsku>=30
						replace mass with xrpt.mass+xctnqty in xrpt
					case xctnqty>=150 or (xctnqty>=50 and xavgctnsku>=20)
						replace dept with xrpt.dept+xctnqty in xrpt
					otherwise
						replace bout with xrpt.bout+xctnqty in xrpt
					endcase
				endif
			EndIf
		EndScan

		select xrpt
		replace all total with mass+dept+bout
		Select log
	EndDo

	STORE 17 TO tnum
	SELECT XRPT
	INDEX ON acctname +SUBSTR(zmonth,3,2)+SUBSTR(zmonth,1,2) TAG tacctname

	replace all month WITH VAL(SUBSTR(zmonth,1,2)), ;
				year WITH 2000+VAL(SUBSTR(zmonth,3,2))
	GO top
	DO WHILE !EOF()
		STORE ACCTNAME TO tacctname
		DO WHILE ACCTNAME=tacctname AND !EOF()
			STORE ALLTRIM(STR(tnum)) TO anum

			osheet1.range("A"+anum).value = acctname
			osheet1.range("C"+anum).value = MONTH
			osheet1.range("D"+anum).value = YEAR
			IF TOTAL>0
				osheet1.range("E"+anum).value = TOTAL
				osheet1.range("F"+anum).value = TOTAL/SWCS
			endif
			IF MASS>0
				osheet1.range("G"+anum).value = MASS
				osheet1.range("H"+anum).value = MASS/TOTAL
			ENDIF
			IF DEPT>0
				osheet1.range("I"+anum).value = DEPT
				osheet1.range("J"+anum).value = DEPT/TOTAL
			ENDIF
			IF BOUT>0
				osheet1.range("K"+anum).value = BOUT
				osheet1.range("L"+anum).value = BOUT/TOTAL
			ENDIF

			tnum=tnum+1
			SKIP
		ENDDO

		*HERE PUT IN A SUBTOTAL FOR EACH ACCOUNT FOR THE MONTHS IN THE REPORT
		SET FILTER TO ACCTNAME=tacctname
		locate
		SUM TOTAL, SWCS, mass, MASSSWCS, DEPT, DEPTSWCS, BOUT, BOUTSWCS ;
			TO ttotCTNS, TTOTSWCS, TMASSCTNS, TMASSSWCS, TDEPTCTNS, TDEPTSWCS, TBOUTCTNS, TBOUTSWCS

		STORE ALLTRIM(STR(tnum)) TO anum
		osheet1.range("A"+anum).value = acctname
		osheet1.range("C"+anum).value = "Total:"

		IF tTOTCTNS>0
			osheet1.range("E"+anum).value = tTOTctns
			osheet1.range("F"+anum).value = tTOTCTNS/SWCS
		endif
		IF tMASSCTNS>0
			osheet1.range("G"+anum).value = tMASSCTNS
			osheet1.range("H"+anum).value = tMASSCTNS/tTOTCTNS

		ENDIF
		IF tDEPTCTNS>0
			osheet1.range("I"+anum).value = tDEPTCTNS
			osheet1.range("J"+anum).value = tDEPTCTNS/TTOTCTNS

		ENDIF
		IF tBOUTCTNS>0
			osheet1.range("K"+anum).value = tBOUTCTNS
			osheet1.range("L"+anum).value = tBOUTCTNS/ttotctns

		ENDIF
		tnum=tnum+1
		GO BOTT

		SET FILTER TO 
		SKIP
	enddo

	IF allaccounts
		SET FILTER TO 
		GO TOP
		STORE YEAR TO tyear
		SUM TOTAL, SWCS, mass, MASSSWCS, DEPT, DEPTSWCS, BOUT, BOUTSWCS ;
			TO ttotCTNS, TTOTSWCS, TMASSCTNS, TMASSSWCS, TDEPTCTNS, TDEPTSWCS, TBOUTCTNS, TBOUTSWCS

		STORE ALLTRIM(STR(tnum)) TO anum
		osheet1.range("A"+anum).value = "All Accounts"
		osheet1.range("C"+anum).value = "Total:"
		osheet1.range("D"+anum).value = tyear
		
		IF tTOTctns>0
			osheet1.range("E"+anum).value = tTOTctns
			osheet1.range("F"+anum).value = tTOTCTNS/ttotSWCS
		endif
		IF tMASSCTNS>0
			osheet1.range("G"+anum).value = tMASSCTNS
			osheet1.range("H"+anum).value = tMASSCTNS/tTOTCTNS
		ENDIF
		IF tDEPTCTNS>0
			osheet1.range("I"+anum).value = tDEPTCTNS
			osheet1.range("J"+anum).value = tDEPTCTNS/TTOTCTNS
		ENDIF
		IF tBOUTCTNS>0
			osheet1.range("K"+anum).value = tBOUTCTNS
			osheet1.range("L"+anum).value = tBOUTCTNS/tTOTCTNS
		ENDIF
	EndIf
	**** END Shipping
endif


IF reporttype=4
	COPY FILE s:\metrics\whserpt.xls TO &cFileName
	oexcel=createobject("excel.application")
	oworkbook=oexcel.workbooks.open(cFileName)
endif

**** Begin Special Projects
**get the total number of workdays within this period
Select account
LOCATE FOR accountid=tacct
STORE Iif(ALLACCOUNTS, 'ALL ACCOUNTS', ACCTNAME) TO tacctname

STORE DTOC(tstart)+"-"+DTOC(tend) TO datestring
DO CASE
CASE toffice="C"
	STORE "San Pedro" TO tlocation
CASE toffice='N'
	STORE "Carteret" TO tlocation
CASE toffice='M'
	STORE "Miami" TO tlocation
CASE toffice='L'
	STORE "Mira Loma" TO tlocation
CASE toffice='K'
	STORE "Louisville" TO tlocation
CASE toffice='R'
	STORE "Rialto" TO tlocation
CASE toffice='X'
	STORE "Carson" TO tlocation
CASE toffice='O'
	STORE "Ontario" TO tlocation
CASE toffice='Y'
	STORE "Carson 2" TO tlocation
CASE toffice='R'
	STORE "Mira Loma 2" TO tlocation
ENDCASE


cAcctFilter = Iif(allAccounts, "", "project.ACCOUNTID="+transform(tacct))
*SELECT * FROM project, PROJDET ;
	WHERE PROJECT.PROJECTID=PROJDET.PROJECTID and &cAcctFilter INVDT>=tstart AND INVDT<=tend  ;
	INTO cursor temp readWrite
set step on 
xsqlexec("select * from project, projdet where project.projectid=projdet.projectid and "+cacctfilter+" " + ;
	"and project.mod='"+xmod+"' and between(invdt,{"+dtoc(tstart)+"},{"+dtoc(tend)+"})","temp",,"wh")

INDEX ON accountid TAG acct
INDEX ON invdt TAG invdt
INDEX ON STR(accountid,4)+description+CONSIGNEE TAG CONSIGNEE
INDEX ON STR(accountid,4)+DESCRIPTION TAG DESCRIP

*Headings for all worksheets
osheet1=oworkbook.worksheets[13]
osheet1.range("B4").value=tacctname
osheet1.range("B5").value=DATESTRING
osheet1.range("B6").value=TLOCATION

osheet1=oworkbook.worksheets[14]
osheet1.range("B4").value=tacctname
osheet1.range("B5").value=DATESTRING
osheet1.range("B6").value=TLOCATION

osheet1=oworkbook.worksheets[15]
osheet1.range("B4").value=tacctname
osheet1.range("B5").value=DATESTRING
osheet1.range("B6").value=TLOCATION

osheet1=oworkbook.worksheets[16]
osheet1.range("B4").value=tacctname
osheet1.range("B5").value=DATESTRING
osheet1.range("B6").value=TLOCATION

osheet1=oworkbook.worksheets[17]
osheet1.range("B4").value=tacctname
osheet1.range("B5").value=DATESTRING
osheet1.range("B6").value=TLOCATION

**Summary Page
osheet1=oworkbook.worksheets[13]
STORE 11 TO TNUM

SELECT temp
SET ORDER TO acct
Locate
DO WHILE !EOF()
	STORE accountid TO TACCT

	SELECT account
	Locate for accountid = tAcct
	STORE acctname TO TNAME

	SELECT temp
	STORE RECNO() TO TREC
	SUM totcharge TO GTCHG WHILE accountid=TACCT
	GOTO trec
	COUNT TO TNUMSPS WHILE accountid=TACCT
									
	STORE ALLTRIM(STR(tnum)) TO anum

	osheet1.range("A"+anum).value = tname
	osheet1.range("C"+anum).value = gtchg
	osheet1.range("E"+anum).value = TNUMSPS
	osheet1.range("G"+anum).value = gtchg/tnumsps

	tnum=tnum+1
ENDDO

SUM totcharge TO  gtchg
COUNT TO gtnumsps

STORE ALLTRIM(STR(tnum)) TO anum

osheet1.range("A"+anum).value = "Total"
osheet1.range("C"+anum).value = gtchg
osheet1.range("E"+anum).value = GTNUMSPS
osheet1.range("G"+anum).value = gtchg/gtnumsps


**Daily Page
osheet1=oworkbook.worksheets[14]

STORE 11 TO TNUM
SELECT temp
SET ORDER TO invdt
Locate
DO WHILE !EOF()
	STORE invdt TO MDATE
	STORE 0 TO GTCHG,TNUMSPS
	DO WHILE invdt=MDATE AND !EOF()
		GTCHG=GTCHG+totcharge
		TNUMSPS=TNUMSPS+1
		SKIP
	enddo
									
	STORE ALLTRIM(STR(tnum)) TO anum

	osheet1.range("A"+anum).value = MDATE
	osheet1.range("C"+anum).value = gtchg
	osheet1.range("E"+anum).value = TNUMSPS
	osheet1.range("G"+anum).value = gtchg/tnumsps

	tnum=tnum+1
ENDDO

SUM totcharge TO  gtchg
COUNT TO gtnumsps

STORE ALLTRIM(STR(tnum)) TO anum

osheet1.range("A"+anum).value = "Total"
osheet1.range("C"+anum).value = gtchg
osheet1.range("E"+anum).value = GTNUMSPS
osheet1.range("G"+anum).value = gtchg/gtnumsps


**Monthly Page
osheet1=oworkbook.worksheets[15]

STORE 11 TO TNUM
SELECT temp
SET ORDER TO invdt
Locate
DO WHILE !EOF()
	STORE MONTH(invdt) TO tmonth
	STORE YEAR(invdt) TO tyear
	STORE ALLTRIM(STR(tmonth)) TO cmonth
	STORE STR(YEAR(invdt)) TO cyear
	IF LEN(CMONTH)=1
		STORE "0"+CMONTH TO CMONTH
	ENDIF

	STORE cmonth+'/'+cyear TO cdate

	STORE 0 TO mrev,mnum
	DO WHILE MONTH(invdt)=tmonth AND YEAR(invdt)=tyear AND !EOF()
		MREV=MREV+totcharge
		MNUM=MNUM+1
		SKIP
	ENDDO
				
	STORE ALLTRIM(STR(tnum)) TO anum

	osheet1.range("A"+anum).value = cdate
	osheet1.range("C"+anum).value = mrev
	osheet1.range("E"+anum).value = mnum
	osheet1.range("G"+anum).value = mrev/mnum

	tnum=tnum+1
ENDDO

SUM totcharge TO  gtchg
COUNT TO gtnumsps

STORE ALLTRIM(STR(tnum)) TO anum

osheet1.range("A"+anum).value = "Total"
osheet1.range("C"+anum).value = gtchg
osheet1.range("E"+anum).value = GTNUMSPS
osheet1.range("G"+anum).value = gtchg/gtnumsps


**BY TYPE Page
**section rewritten to use cursor xrpt, made some other subtle changes - mvw 02/13/08
osheet1=oworkbook.worksheets[16]

store 11 to tnum
select accountid, description, sum(totcharge) as totcharge, rate, sum(qty) as totqty from temp group by accountid, description, rate into cursor xrpt
scan
	=seek(xrpt.accountid,"account","accountid")

	anum=transform(tnum)
	osheet1.range("A"+anum).value = account.acctname
	osheet1.range("C"+anum).value = xrpt.totcharge
	osheet1.range("E"+anum).value = xrpt.description
	osheet1.range("F"+anum).value = xrpt.rate
	osheet1.range("G"+anum).value = xrpt.totqty
	tnum=tnum+1
endscan

store transform(tnum) to anum
sum totcharge, totqty to xtotchg, xtotqty
osheet1.range("A"+anum).value = "Total"
osheet1.range("C"+anum).value = xtotchg
osheet1.range("G"+anum).value = xtotqty

use in xrpt

**BY Consignee Page
osheet1=oworkbook.worksheets[17]

store 11 to tnum
select accountid, description, consignee, sum(totcharge) as totcharge, rate, sum(qty) as totqty from temp group by accountid, description, consignee, rate into cursor xrpt
scan
	=seek(xrpt.accountid,"account","accountid")

	anum=transform(tnum)
	osheet1.range("A"+anum).value = account.acctname
	osheet1.range("C"+anum).value = xrpt.totcharge
	osheet1.range("E"+anum).value = xrpt.description
	osheet1.range("F"+anum).value = xrpt.consignee
	osheet1.range("G"+anum).value = xrpt.rate
	osheet1.range("H"+anum).value = xrpt.totqty
	tnum=tnum+1
endscan

store transform(tnum) to anum
sum totcharge, totqty to xtotchg, xtotqty
osheet1.range("A"+anum).value = "Total"
osheet1.range("C"+anum).value = xtotchg
osheet1.range("G"+anum).value = xtotqty

use in xrpt
**** END SPecial Projects


=CloseData()

WAIT CLEAR
oworkBook.save()
oexcel.visible=.t.





**********************
***** Procedures *****
*********************
PROCEDURE DT2MONTH
lparameter xdate

	return left(dtoc(xdate),2)+right(dtoc(xdate),2)

Return


Procedure closeData

	Set Database To wh
	Close Databases

	Set Database To ar
	Close Databases


	USE in account
	USE in wolog

	If Used("temp")
	  USE in temp
	EndIf
	If Used("temp2")
	  USE in temp2
	EndIf
	If Used("temp3")
	  USE in temp3
	EndIf
	If Used("temp4")
	  USE in temp4
	EndIf
	If Used("temp30")
	  USE in temp30
	EndIf
	If Used("temploc")
	  USE in temploc
	EndIf
	If Used("log")
	  USE in log
	ENDIF
	If Used("CARGO")
	  USE in CARGO
	ENDIF
	IF USED("SWCACCT")
	  USE IN SWCACCT
	ENDIF
	IF USED("oloch")
	USE IN oloch
	ENDIF
	IF USED("odeth")
	USE IN odeth
	ENDIF
	IF USED("deth")
	USE IN deth
	ENDIF
	IF USED("loch")
	USE IN loch
	ENDIF
	
Return
