*!* m:\dev\prg\bugaboo940_process.prg
WAIT WINDOW "Now in PROCESS Phase..." NOWAIT

CD &lcPath

LogCommentStr = "LOADED"

delimchar = cDelimiter
lcTranOpt= cTranslateOption

lnNum = ADIR(tarray,"*.FTP")

IF lnNum = 0
	WAIT WINDOW AT 10,10 "      No "+cCustname+" 940s to import     " TIMEOUT 3
	NormalExit = .T.
	THROW
ENDIF

ASSERT .F. MESSAGE "At initial input file loop"
FOR thisfile = 1  TO lnNum
	Xfile = lcPath+tarray[thisfile,1] && +"."
	cfilename = ALLTRIM(LOWER(tarray[thisfile,1]))
	nFileSize = Val(Transform(tarray(thisfile,2)))
	WAIT WINDOW "" TIMEOUT 2
	IF UPPER(LEFT(cfilename,3))="IN7"
		cArchiveFileName = cfilename
	ELSE
		cArchiveFileName = "IN7"+TTOC(DATETIME(),1)+".ftp"
		RENAME [&xfile] TO (lcPath+cArchiveFileName)
		xfile = (lcPath+cArchiveFileName)
		cfilename = JUSTFNAME(cArchiveFileName)
	ENDIF
	archivefile  = (lcArchivePath+cArchiveFileName)

	WAIT WINDOW "Importing file: "+cfilename NOWAIT
	SELECT x856
	APPEND FROM [&xfile] TYPE DELIMITED WITH CHARACTER "*"

	IF lTesting
		LOCATE
*	BROWSE
	ENDIF


	SET STEP ON 
	DO create_pt_cursors WITH cUseFolder,lLoadSQL && Added to prep for SQL Server transition

	DO ("m:\dev\prg\"+cCustname+"_940edi_bkdn")

	DO m:\dev\prg\all940_import
	
NEXT thisfile

&& now clean up the 940in archive files, delete for the upto the last 10 days
deletefile(lcarchivepath,IIF(DATE()<{^2016-05-01},20,10))

WAIT CLEAR

WAIT "Entire "+cCustname+" 940 Process Now Complete" WINDOW TIMEOUT 5
NormalExit = .T.
