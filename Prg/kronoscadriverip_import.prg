* Populate the Kronos PAYCATEDITIMPORT table with CA Driver Incentive Pay.
* Once this is done, an Import can be run from within Kronos to apply the paycode edits to the timecards.

* EXEs go in F:\UTIL\KRONOS\

utilsetup("KRONOSCADRIVERIP_IMPORT")

loKronosCADriverIP_Import = CREATEOBJECT('KronosCADriverIP_Import')
loKronosCADriverIP_Import.MAIN()

schedupdate()

CLOSE DATABASES ALL
RETURN

#DEFINE LOGIT 1
#DEFINE WAITIT 2
#DEFINE NOWAITIT 4
#DEFINE SENDIT 8
#DEFINE CRLF CHR(13) + CHR(10)
#DEFINE LF CHR(10)
#DEFINE RETURN_DATA_MANDATORY .F.
#DEFINE RETURN_DATA_NOT_MANDATORY .T.

DEFINE CLASS KronosCADriverIP_Import AS CUSTOM

	cProcessName = 'KronosCADriverIP_Import'

	lTestMode = .F.  && .F. FOR NORMAL OPERATION, .T. TO SUPPRESS ALL BUT DEVELOPER EMAILS AND CHANGE INPUT TABLE, ETC.
	
	lAutoYield = .T.
	cStartTime = TTOC(DATETIME())

	* connection properties
	nSQLHandle = 0

	* file properties
	nFileHandle = 0

	* table properties
	cInputTable = 'F:\UTIL\Kronos\DATAIMPORT\CADRIVERINCPAY'
	cInputTableArchived = 'F:\UTIL\Kronos\DATAIMPORT\ARCHIVED\CADRIVERINCPAY'
	cControlTable = 'F:\UTIL\Kronos\DATAIMPORT\CONTROL\CADRIVERIPCTRL'
	
	* date properties
	dtNow = DATETIME()

	dToday = DATE()
	* to force different pay weeks
	*dToday = {^2009-05-26}
	
	* procesing props
	*** Only set the prop below to .T. if you are trying to reverse a load!
	lReverseSign = .F.  && should normally be .F. !!!!

	* wait window properties
	nWaitWindowTimeout = 2
	lWaitWindowIsOn = .T.

	* logfile properties
	lLoggingIsOn = .T.
	cLogFile = 'F:\UTIL\KRONOS\Logfiles\KronosCADriverIP_Import_log.txt'

	* INTERNAL email properties
	lSendInternalEmailIsOn = .T.
	cFrom = 'FMI Corporate <fmicorporate@fmiint.com>'
	*cSendTo = 'Mark Bennett <mbennett@fmiint.com>'
	cSendTo = 'mbennett@fmiint.com'
	*cSendTo = 'mbennett@fmiint.com, Steven.Pyle@Tollgroup.com, Jennifer.Bonsanto@Tollgroup.com'
	cCC = ''
	cSubject = 'Kronos CA Driver Incentive Pay Import for: ' + DTOC(DATE())
	cAttach = ''
	cBodyText = ''

	cCOMPUTERNAME = ''
	cUSERNAME = ''

	FUNCTION INIT
		IF NOT DODEFAULT()
			RETURN .F.
		ENDIF
		WITH THIS
			CLEAR
			*SET RESOURCE OFF
			CLOSE DATA
			SET CENTURY ON
			SET DATE AMERICAN
			SET HOURS TO 24
			SET ANSI ON
			SET TALK OFF
			SET DELETED ON
			SET CONSOLE OFF
			SET EXCLUSIVE OFF
			SET SAFETY OFF
			SET EXACT OFF
			SET STATUS BAR ON
			SET SYSMENU OFF
			SET ENGINEBEHAVIOR 70
			_VFP.AUTOYIELD = .lAutoYield
			.cCOMPUTERNAME = GETENV("COMPUTERNAME")
			.cUSERNAME = GETENV("USERNAME")
			IF .lTestMode THEN
				.cSendTo = 'Mark Bennett <mbennett@fmiint.com>'
				.cLogFile = 'F:\UTIL\KRONOS\Logfiles\KronosCADriverIP_Import_log_TESTMODE.txt'
				.cInputTable = 'F:\UTIL\Kronos\DATAIMPORT\TEST\CADRIVERINCPAY'
			ENDIF
			.lLoggingIsOn = .lLoggingIsOn AND NOT EMPTY(.cLogFile)
			IF .lLoggingIsOn THEN
				SET ALTERNATE TO (.cLogFile) ADDITIVE
				SET ALTERNATE ON
			ENDIF
		ENDWITH
	ENDFUNC


	FUNCTION DESTROY
		WITH THIS
			IF .lLoggingIsOn  THEN
				SET ALTERNATE OFF
				SET ALTERNATE TO
			ENDIF
		ENDWITH
		DODEFAULT()
	ENDFUNC


	FUNCTION MAIN
		WITH THIS
			LOCAL lnNumberOfErrors, lcSQL, lcSQL2, lcSQL3, lcSQL4, loError, llValid
			LOCAL lcXLFileName, lcFileDate, lnRetCode, lcEditDate
			LOCAL lcSendTo, lcCC, lcBodyText, ldEditDate

			TRY
				lnNumberOfErrors = 0

				*.TrackProgress('==================================================================================================================', LOGIT+SENDIT)
				.TrackProgress('KRONOS CA Driver Incentive Pay Import process started....', LOGIT+SENDIT+NOWAITIT)
				.TrackProgress('COMPUTERNAME = ' + .cCOMPUTERNAME, LOGIT+SENDIT)
				.TrackProgress('USERNAME = ' + .cUSERNAME, LOGIT+SENDIT)
				.TrackProgress('==================================================================================================================', LOGIT+SENDIT)
				IF .lTestMode THEN
					.TrackProgress('                        TEST MODE                        ', LOGIT+SENDIT)
					.TrackProgress('==================================================================================================================', LOGIT+SENDIT)
				ENDIF
				.TrackProgress('', LOGIT)

				.cSubject = 'Kronos CA Driver Incentive Pay Import for: ' + DTOC(.dToday)	
				
				USE (.cControlTable) IN 0 ALIAS CA_INC_PAY_CTRL_TABLE EXCLUSIVE				
									
				USE (.cInputTable) AGAIN IN 0 SHARED ALIAS CA_INC_PAY_TABLE
				ldEditDate = CA_INC_PAY_TABLE.SATURDAY
				
				* if ldEditDate is already in control table, do not process because it may cause duplicates
				SELECT CA_INC_PAY_CTRL_TABLE
				LOCATE FOR EDITDATE = ldEditDate
				IF FOUND() THEN				
					.TrackProgress('ERROR: Date ' + TRANSFORM(ldEditDate) + ' has already been processed!', LOGIT+SENDIT)
					THROW					
				ELSE
					* okay - continue...
				
					.TrackProgress('Connecting to WTK System....', LOGIT+SENDIT)

					* access kronos
					OPEN DATABASE F:\UTIL\KRONOS\KRONOSEDI

					.nSQLHandle = SQLCONNECT("WTK_ON_PAYROLL2","sa","kr0n1tes")

					IF .nSQLHandle > 0 THEN

						* first fix problem with duplicate Jorge A. Ramirez's by changing emp_num 2550 (ZXU) to 3584 (SXI)
						SELECT CA_INC_PAY_TABLE
						SCAN FOR EMP_NUM = 2550
							REPLACE CA_INC_PAY_TABLE.EMP_NUM WITH 3584
							.TrackProgress('Changed EMP_NUM 2550 to 3584 (Jorge A. Ramirez)', LOGIT+SENDIT)						
						ENDSCAN
						
					
						* Now...open import table and scan it, at each rec build SQL INSERT and send it.
						
						* each SQL statement should look like:
						
						* Insert into PAYCATEDITIMPORT (REQ_CODE, EMPID, EDITDATE, PAYCAT, AMOUNT)
						* Values ('6', '999', '7/4/2009', 'CA Driver Incentive Pay', '345.76')
						
						IF .lReverseSign THEN
							SELECT CA_INC_PAY_TABLE
							SCAN
								REPLACE CA_INC_PAY_TABLE.PAY WITH (CA_INC_PAY_TABLE.PAY * -1)
							ENDSCAN
						ENDIF
						
						SELECT CA_INC_PAY_TABLE
						SCAN
							lcSQL = "Insert into PAYCATEDITIMPORT (REQ_CODE, EMPID, EDITDATE, PAYCAT, AMOUNT) " + ;
									"Values ('6', '" + ALLTRIM(TRANSFORM(CA_INC_PAY_TABLE.EMP_NUM)) + ;
									"', '" + ALLTRIM(TRANSFORM(MONTH(CA_INC_PAY_TABLE.SATURDAY))) + ;
									"/" + ALLTRIM(TRANSFORM(DAY(CA_INC_PAY_TABLE.SATURDAY))) + ;
									"/" + ALLTRIM(TRANSFORM(YEAR(CA_INC_PAY_TABLE.SATURDAY))) + ;
									"', 'CA Driver Incentive Pay', '" + ;
									ALLTRIM(TRANSFORM(CA_INC_PAY_TABLE.PAY)) + ;
									"')"					
						
*!*								lcSQL = "Insert into PAYCATEDITIMPORT (REQ_CODE, EMPID, EDITDATE, PAYCAT, AMOUNT) " + ;
*!*										"Values ('6', '" + ALLTRIM(TRANSFORM(CA_INC_PAY_TABLE.EMP_NUM)) + ;
*!*										"', '" + ALLTRIM(TRANSFORM(MONTH(CA_INC_PAY_TABLE.SATURDAY))) + ;
*!*										"/" + ALLTRIM(TRANSFORM(DAY(CA_INC_PAY_TABLE.SATURDAY))) + ;
*!*										"/" + ALLTRIM(TRANSFORM(YEAR(CA_INC_PAY_TABLE.SATURDAY))) + ;
*!*										"', 'Driver Incentive Pay', '" + ;
*!*										ALLTRIM(TRANSFORM(CA_INC_PAY_TABLE.PAY)) + ;
*!*										"')"					
						
							lnRetCode = .ExecSQL(lcSQL, 'CUR_NOT_USED', RETURN_DATA_NOT_MANDATORY)
							.TrackProgress('RetCode [' + ALLTRIM(TRANSFORM(lnRetCode)) + '] for: ' + CRLF + lcSQL, LOGIT+SENDIT)
						
						ENDSCAN
						
						* update control table
						INSERT INTO CA_INC_PAY_CTRL_TABLE (editdate, processed) ;
							VALUES (ldEditDate, DATETIME())

						CLOSE DATABASES ALL
						
						IF NOT .lTestMode THEN
							* archive the input table
							lcEditDate = STRTRAN(TRANSFORM(ldEditDate),"/","")
							COPY FILE (.cInputTable + ".DBF") TO (.cInputTableArchived + "_" + lcEditDate + ".DBF")
							DELETE FILE (.cInputTable + ".DBF")
						ENDIF
						
					ELSE
						* connection error
						.TrackProgress('Unable to connect to WTK System.', LOGIT+SENDIT)
					ENDIF   &&  .nSQLHandle > 0
					
				ENDIF  && FOUND()
				
			CATCH TO loError

				.TrackProgress('There was an error.',LOGIT+SENDIT)
				.TrackProgress('The Error # is: ' + TRANSFORM(loError.ERRORNO), LOGIT+SENDIT)
				.TrackProgress('The Error Message is: ' + TRANSFORM(loError.MESSAGE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred in Program: ' + TRANSFORM(loError.PROCEDURE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred at line #: ' + TRANSFORM(loError.LINENO), LOGIT+SENDIT)
				lnNumberOfErrors = lnNumberOfErrors + 1
				IF TYPE('oExcel') = 'O' AND NOT ISNULL(oExcel) THEN
					oExcel.QUIT()
				ENDIF
				CLOSE DATA
				CLOSE ALL

			ENDTRY

			CLOSE DATA
			WAIT CLEAR
			***************** INTERNAL email results ******************************
			.TrackProgress('About to send status email.',LOGIT)
			.TrackProgress('==================================================================================================================', SENDIT)
			.TrackProgress('The logfile is: ' + .cLogFile, SENDIT)
			.TrackProgress('KRONOS CA Driver Incentive Pay Import process started: ' + .cStartTime, LOGIT+SENDIT)
			.TrackProgress('KRONOS CA Driver Incentive Pay Import process finished: ' + TTOC(DATETIME()), LOGIT+SENDIT)

			IF .lSendInternalEmailIsOn THEN
				* try to trap error from not having dartmail dll's registered on user's pc...
				TRY
					DO FORM dartmail2 WITH .cSendTo,.cFrom,.cSubject,.cCC,.cAttach,.cBodyText,"A"
					.TrackProgress('Sent status email.',LOGIT)
				CATCH TO loError
					*=MESSAGEBOX('There was an error sending the status email. Please contact Mark Bennett.',0+64,"KRONOS Temp Time")
					.TrackProgress('There was an error sending the status email.',LOGIT)
					.TrackProgress('The Error # is: ' + TRANSFORM(loError.ERRORNO), LOGIT)
					.TrackProgress('The Error Message is: ' + TRANSFORM(loError.MESSAGE), LOGIT)
					.TrackProgress('The Error occurred in Program: ' + TRANSFORM(loError.PROCEDURE), LOGIT)
					.TrackProgress('The Error occurred at line #: ' + TRANSFORM(loError.LINENO), LOGIT)
					lnNumberOfErrors = lnNumberOfErrors + 1
					CLOSE DATA
				ENDTRY

			ELSE
				.TrackProgress('Did not send status email: THIS.lSendInternalEmailIsOn = FALSE.',LOGIT)
			ENDIF

		ENDWITH
		RETURN
	ENDFUNC && main


	FUNCTION ExecSQL
		LPARAMETERS tcSQL, tcCursorName, tlNoDataReturnedIsOkay
		LOCAL llRetval, lnResult
		WITH THIS
			* close target cursor if it's open
			IF USED(tcCursorName)
				USE IN (tcCursorName)
			ENDIF
			WAIT WINDOW "Fetching remote data; please wait..." NOWAIT
			lnResult = SQLEXEC(.nSQLHandle, tcSQL, tcCursorName)
			llRetval = ( lnResult > 0 )
			IF llRetval THEN
				* see if any data came back
				IF NOT tlNoDataReturnedIsOkay THEN
					IF (NOT USED(tcCursorName)) OR EOF(tcCursorName) THEN
						llRetval = .F.
						.TrackProgress('No data was returned by this query: ' +CRLF + tcSQL, LOGIT+SENDIT)
					ENDIF
				ENDIF
			ELSE
				.TrackProgress("Result Code [" + TRANSFORM(lnResult) + "] returned by this query: " + tcSQL, LOGIT+SENDIT)
			ENDIF
			WAIT CLEAR
			RETURN llRetval
		ENDWITH
	ENDFUNC


	PROCEDURE TrackProgress
		* do any combination of Wait Window, writing to logfile, and adding to body of email,
		* based on nFlags parameter.
		LPARAMETERS tcExpression, tnFlags
		WITH THIS
			IF BITAND(tnFlags,LOGIT) = LOGIT THEN
				IF .lLoggingIsOn THEN
					?
					? .cProcessName + "  " + TTOC(DATETIME()) + ": " + tcExpression
				ENDIF
			ENDIF
			IF .lWaitWindowIsOn THEN
				IF BITAND(tnFlags,NOWAITIT) = NOWAITIT THEN
					WAIT WINDOW tcExpression NOWAIT
				ENDIF
				IF BITAND(tnFlags,WAITIT) = WAITIT THEN
					WAIT WINDOW tcExpression TIMEOUT .nWaitWindowTimeout
				ENDIF
			ENDIF
			IF BITAND(tnFlags,SENDIT) = SENDIT THEN
				IF .lSendInternalEmailIsOn THEN
					.cBodyText = .cBodyText + tcExpression + CRLF + CRLF
				ENDIF
			ENDIF
		ENDWITH
	ENDPROC  &&  TrackProgress
	

ENDDEFINE
