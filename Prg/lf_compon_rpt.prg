* LF_COMPON_RPT
* 
* A daily component report for Lifefactory, same report you get in FoxPro: Inventory Control: Material Builds. form = COMPON
* 
* This is Carteret Mod I:  F:\WHI\WHDATA
*
* specc'ed by Chris Malcolm and PG
*
*
* EXE = F:\UTIL\LF\LF_COMPON_RPT.EXE


runack("LF_COMPON_RPT")

LOCAL lnError, lcProcessName, loLF_COMPON_RPT

DECLARE INTEGER CreateMutex IN Win32API ;
	STRING @lpMutexAttributes, ;
	STRING bInitialOwner, ;
	STRING @lpName

DECLARE INTEGER GetLastError IN Win32API

* if running in EXE check to see if already running
IF _VFP.STARTMODE > 0 THEN
	lcProcessName = "LF_COMPON_RPT"
	CreateMutex(0,CHR(1),@lcProcessName)
	lnError = GetLastError()
	IF lnError = 183
		WAIT WINDOW TIMEOUT 10 "Lifefactory Component Report process is already running..."
		RETURN .F.
	ENDIF
ENDIF

utilsetup("LF_COMPON_RPT")

goffice="I"

loLF_COMPON_RPT = CREATEOBJECT('LF_COMPON_RPT')
loLF_COMPON_RPT.MAIN()

schedupdate()

CLOSE DATABASES ALL
RETURN

#DEFINE LOGIT 1
#DEFINE WAITIT 2
#DEFINE NOWAITIT 4
#DEFINE SENDIT 8
#DEFINE CRLF CHR(13) + CHR(10)
#DEFINE LF CHR(10)
#DEFINE RETURN_DATA_MANDATORY .F.
#DEFINE RETURN_DATA_NOT_MANDATORY .T.
#DEFINE DAILY_OT_MULTIPLIER 1.5
#DEFINE DAILY_OT20_MULTIPLIER 2.0
#DEFINE xlEdgeBottom 9
#DEFINE xlContinuous 1
#DEFINE xlMedium -4138

DEFINE CLASS LF_COMPON_RPT AS CUSTOM

	cProcessName = 'LF_COMPON_RPT'

	lTestMode = .F.  && .F. FOR NORMAL OPERATION, .T. TO SUPPRESS ALL BUT DEVELOPER EMAILS AND CHANGE INPUT TABLE, ETC.

	lAutoYield = .T.

	cStartTime = TTOC(DATETIME())
	
	* processing properties
	cProcessDesc = "Lifefactory Component Report process"
	nAccountID = 6034  && WHAT ABOUT 6176?
	
	* file properties
	nFileHandle = 0

	* date properties
	dtNow = DATETIME()

	dToday = DATE()
	*dToday = {^2017-03-07}

	cToday = DTOC(DATE())

	* wait window properties
	nWaitWindowTimeout = 2
	lWaitWindowIsOn = .T.

	* logfile properties
	lLoggingIsOn = .T.
	cLogFile = 'F:\UTIL\LF\LOGFILES\LF_COMPON_RPT_LOG.TXT'

	* INTERNAL email properties
	lSendInternalEmailIsOn = .T.
	cFrom = 'Mark.Bennett@Tollgroup.com'
	
	*cSendTo = 'ethan@lifefactory.com'
	cSendTo = 'chris.malcolm@tollgroup.com'
	*cCC = 'chris.malcolm@tollgroup.com'
	cCC = 'mbennett@fmiint.com, ethan@lifefactory.com, vlad@lifefactory.com, greg@lifefactory.com, joe@lifefactory.com, hannah@lifefactory.com'
	
	*cSendTo = 'mbennett@fmiint.com'
	*cCC = ''
	
	cSubject = ''
	cAttach = ''
	cBodyText = ''
	cTopBodyText = ''

	cCOMPUTERNAME = ''
	cUSERNAME = ''

	FUNCTION INIT
		IF NOT DODEFAULT()
			RETURN .F.
		ENDIF
		WITH THIS
			CLEAR
			*SET RESOURCE OFF
			CLOSE DATA
			SET CENTURY ON
			SET DATE AMERICAN
			SET HOURS TO 24
			SET ANSI ON
			SET TALK OFF
			SET DELETED ON
			SET CONSOLE OFF
			SET EXCLUSIVE OFF
			SET SAFETY OFF
			SET EXACT OFF
			SET STATUS BAR ON
			SET SYSMENU OFF
			SET ENGINEBEHAVIOR 70
			_VFP.AUTOYIELD = .lAutoYield
			.cCOMPUTERNAME = GETENV("COMPUTERNAME")
			.cUSERNAME = GETENV("USERNAME")
			IF .lTestMode THEN
				.cSendTo = 'mbennett@fmiint.com'
				.cCC = ''
				.cLogFile = 'F:\UTIL\LF\LOGFILES\LF_COMPON_RPT_LOG_TESTMODE.TXT'
			ENDIF
			.lLoggingIsOn = .lLoggingIsOn AND NOT EMPTY(.cLogFile)
		ENDWITH
	ENDFUNC


	FUNCTION DESTROY
		WITH THIS
			IF .lLoggingIsOn  THEN
				SET ALTERNATE OFF
				SET ALTERNATE TO
			ENDIF
		ENDWITH
		DODEFAULT()
	ENDFUNC


	FUNCTION MAIN

		WITH THIS
			LOCAL lnNumberOfErrors, lnAccountID, lcOutputCSV, ldToday, lcFileDateTime

			TRY
				lnNumberOfErrors = 0

				IF .lLoggingIsOn THEN
					SET ALTERNATE TO (.cLogFile) ADDITIVE
					SET ALTERNATE ON
				ENDIF

				.cSubject = .cProcessDesc + ' for ' + DTOC(.dToday)

				.TrackProgress(.cProcessDesc + ' started....', LOGIT+SENDIT+NOWAITIT)
				.TrackProgress('COMPUTERNAME = ' + .cCOMPUTERNAME, LOGIT+SENDIT)
				.TrackProgress('USERNAME = ' + .cUSERNAME, LOGIT+SENDIT)
				.TrackProgress('PROJECT = LF_COMPON_RPT', LOGIT+SENDIT)
				IF .lTestMode THEN
					.TrackProgress('                        TEST MODE                        ', LOGIT+SENDIT)
				ENDIF
				
				lnAccountID = .nAccountID
				
				ldToday = .dToday
				
				lcFileDateTime = PADL(YEAR(ldToday),4,"0") + PADL(MONTH(ldToday),2,"0") + PADL(DAY(ldToday),2,"0") + STRTRAN(SUBSTR(TIME(),1,5),":","")
				
				lcOutputCSV = "F:\UTIL\LF\REPORTS\COMPON_" + lcFileDateTime + ".CSV"
				
				xsqlexec("select * from comphead where mod='I'","comphead",,"wh")
				xsqlexec("select * from compon where mod='I'","compon",,"wh")
								
				* code below is from compon form, zzcreatereport(); only change was to hardcode the accountid to lnAccountID, i.e.6034				
				select ;
					comphead.accountid, ;
					comphead.style as masterstyle, ;
					00000000000 as canmake, ;
					comphead.compgrp, ;
					compon.style, ;
					compon.compqty, ;
					compon.noinven, ;
					.f. as sub, ;
					000000 as availqty, ;
					000000 as buildableqty, ;
					000000 as totavailqty ;
					from comphead, compon ;
					where comphead.compheadid=compon.compheadid ;
					and comphead.accountid=lnAccountID ;
					order by compgrp, masterstyle ;
					into cursor xrpt readwrite

				select xrpt
				scan 
					if xsqlexec("select * from inven where units=1 and accountid="+transform(xrpt.accountid)+" " + ;
						"and style='"+xrpt.style+"' and color#'DAMAGE' and availqty>0",,,"wh") # 0

						replace availqty with inven.availqty in xrpt
					endif
				endscan

				select masterstyle from xrpt group by 1 into cursor xtemp
				scan 
					xcanmake=99999
					select xrpt
					scan for masterstyle=xtemp.masterstyle and !noinven and compqty#0
						xcanmake = min(xcanmake,int(availqty/compqty))
					endscan
					replace canmake with xcanmake in xrpt for masterstyle=xtemp.masterstyle
				endscan

				select * from xrpt into cursor xrpt2

				select xrpt
				scan 
					select xrpt2
					locate for masterstyle=xrpt.style
					if found()
						replace buildableqty with xrpt2.canmake, sub with .t. in xrpt
					endif
				endscan

				replace all totavailqty with availqty+buildableqty in xrpt

				select masterstyle from xrpt where buildableqty>0 group by 1 into cursor xtemp
				scan 
					xcanmake=99999
					select xrpt
					scan for masterstyle=xtemp.masterstyle and !noinven and compqty#0
						xcanmake = min(xcanmake,int(totavailqty/compqty))
					endscan
					replace canmake with xcanmake in xrpt for masterstyle=xtemp.masterstyle
				endscan

				select xrpt
 
				COPY TO (lcOutputCSV) CSV
				
				IF FILE(lcOutputCSV) THEN
					.TrackProgress('Successfully created: ' + lcOutputCSV, LOGIT+SENDIT)
					.cAttach = lcOutputCSV
				ELSE
					.TrackProgress('!!! ERROR: there was a problem creating: ' + lcOutputCSV, LOGIT+SENDIT)
				ENDIF
				

				.TrackProgress(.cProcessDesc + ' ended normally....', LOGIT+SENDIT)

			CATCH TO loError

				.TrackProgress('There was an error.',LOGIT+SENDIT)
				.TrackProgress('The Error # is: ' + TRANSFORM(loError.ERRORNO), LOGIT+SENDIT)
				.TrackProgress('The Error Message is: ' + TRANSFORM(loError.MESSAGE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred in Program: ' + TRANSFORM(loError.PROCEDURE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred at line #: ' + TRANSFORM(loError.LINENO), LOGIT+SENDIT)
				lnNumberOfErrors = lnNumberOfErrors + 1

			ENDTRY

			WAIT CLEAR
			***************** INTERNAL email results ******************************
			.TrackProgress('About to send status email.',LOGIT)
			.TrackProgress('==================================================================================================================', SENDIT)
			.TrackProgress('The logfile is: ' + .cLogFile, LOGIT+SENDIT)
			.TrackProgress(.cProcessDesc + ' started: ' + .cStartTime, LOGIT+SENDIT)
			.TrackProgress(.cProcessDesc + ' finished: ' + TTOC(DATETIME()), LOGIT+SENDIT)

			IF .lSendInternalEmailIsOn THEN
				* try to trap error from not having dartmail dll's registered on user's pc...
				TRY
					.cBodyText = .cTopBodyText + CRLF + .cBodyText

					*DO FORM dartmail2 WITH .cSendTo,.cFrom,.cSubject,.cCC,.cAttach,.cBodyText,"A"
					DO FORM m:\dev\frm\dartmail2 WITH .cSendTo,.cFrom,.cSubject,.cCC,.cAttach,.cBodyText,"A"
					.TrackProgress('Sent status email.',LOGIT)
				CATCH TO loError
					.TrackProgress('There was an error sending the status email.',LOGIT)
					.TrackProgress('The Error # is: ' + TRANSFORM(loError.ERRORNO), LOGIT)
					.TrackProgress('The Error Message is: ' + TRANSFORM(loError.MESSAGE), LOGIT)
					.TrackProgress('The Error occurred in Program: ' + TRANSFORM(loError.PROCEDURE), LOGIT)
					.TrackProgress('The Error occurred at line #: ' + TRANSFORM(loError.LINENO), LOGIT)
					lnNumberOfErrors = lnNumberOfErrors + 1
				ENDTRY

			ELSE
				.TrackProgress('Did not send status email: THIS.lSendInternalEmailIsOn = FALSE.',LOGIT)
			ENDIF

		ENDWITH
		RETURN
	ENDFUNC && main


	PROCEDURE TrackProgress
		* do any combination of Wait Window, writing to logfile, and adding to body of email,
		* based on nFlags parameter.
		LPARAMETERS tcExpression, tnFlags
		WITH THIS
			IF BITAND(tnFlags,LOGIT) = LOGIT THEN
				IF .lLoggingIsOn THEN
					?
					? .cProcessName + "  " + TTOC(DATETIME()) + ": " + tcExpression
				ENDIF
			ENDIF
			IF .lWaitWindowIsOn THEN
				IF BITAND(tnFlags,NOWAITIT) = NOWAITIT THEN
					WAIT WINDOW tcExpression NOWAIT
				ENDIF
				IF BITAND(tnFlags,WAITIT) = WAITIT THEN
					WAIT WINDOW tcExpression TIMEOUT .nWaitWindowTimeout
				ENDIF
			ENDIF
			IF BITAND(tnFlags,SENDIT) = SENDIT THEN
				IF .lSendInternalEmailIsOn THEN
					.cBodyText = .cBodyText + tcExpression + CRLF + CRLF
				ENDIF
			ENDIF
		ENDWITH
	ENDPROC  &&  TrackProgress


ENDDEFINE
