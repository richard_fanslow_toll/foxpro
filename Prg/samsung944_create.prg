*!* Samsung944_CREATE.PRG
*!* This program is triggered automatically via the
*!* EDI_TRIGGER table, populated via the OUTWO.SCX "Create Invoice" button
*!* EDI_TRIGGER is active and sits at the current WO_Num record...

Parameters nWO_Num,nAcctNum,cOffice
nTrk_WO = 0

Public lXfer944,lTesting,cSuffix,lcType
lTesting = .F. && If true, disables certain functions (Default = .f.)
lTestInput = .F.  && If true, uses alternate source tables (Default = .f.)
lHoldFile = .F. && If true, holds output in 944HOLD (Default = .f.)
lEMail = .T.  && If true, sends regular email notices (Default = .t.)
lTestMail = .F. && If true, sends mail ONLY to Joe & Jim (Default = .f.)
lOverrideXcheck = .F. && If true, doesn't check ASN carton count against barcode scans.

nAcctNum = 4677
On Error Debug

Do m:\dev\prg\_setvars With lTesting

cOffice = Iif(cOffice = '1',"C",cOffice)

If lTesting
	cOffice = "C"
*  nAcctNum = 4677
*  nWO_Num = 254595
	Close Databases All
Endif

Dimension thisarray(1)

If lTesting
	cUseFolder = "F:\WHP\WHDATA\"
Else
	xReturn = "XXX"
	Do m:\dev\prg\wf_alt With cOffice,nAcctNum
	cUseFolder = Upper(xReturn)
Endif

If Type("nWO_Num") <> "N"  Or Empty(nWO_Num) && If WO is not numeric or equals Zero
	Wait Window "No WO# Provided...closing" Timeout 3
	Do ediupdate With "NO WO#"
	closeout()
	Return
Endif

cWO_Num = Alltrim(Str(nWO_Num))
cIntUsage = Iif(lTesting,"T","P")

Select 0
Use F:\3pl\Data\mailmaster Alias mm Shared
Locate For mm.accountid = 6649 And mm.edi_type = "944"

If !Found()
	Assert .F. Message "At missing Office/Acct"
	Wait Window "Office/Loc/Acct not found!" Timeout 2
	Do ediupdate With .T.,"ACCT/LOC !FOUND"
	NormalExit = .F.
	Throw
Endif

Store Trim(mm.acctname) To cCustName
Store Trim(mm.basepath) To lcOutPath
Store Trim(mm.holdpath) To lcHoldPath
Store Trim(mm.archpath) To lcArchivePath
_Screen.Caption = Alltrim(mm.scaption)+" "+Transform(nWO_Num)

lUseAlt = mm.USE_ALT
tsendto = Iif(lUseAlt,mm.sendtoalt,mm.sendto)
tcc = Iif(lUseAlt,mm.ccalt,mm.cc)
Locate

Locate For mm.accountid = 9999 And mm.office = "X"
lUseAlt = mm.USE_ALT
tsendtotest = Iif(lUseAlt,mm.sendtoalt,mm.sendto)
tcctest = Iif(lUseAlt,mm.ccalt,mm.cc)
Locate
Locate For edi_type = "MISC" And taskname = "COURTERR"
lUseAlt = mm.USE_ALT
tsendtoerr = Iif(lUseAlt,mm.sendtoalt,mm.sendto)
tccerr = Iif(lUseAlt,mm.ccalt,mm.cc)
Use In mm

Do Case
Case Inlist(cOffice,"C","1","2")
	cSuffix = "-CA"
	cWhseID = "LA1"
	cWhse = "SAN PEDRO"
Case cOffice = "N"
	cSuffix = "-NJ"
	cWhseID = "EC1"
	cWhse = "CARTERET"
Case cOffice = "M"
	cSuffix = "-FL"
	cWhseID = "FL1"
	cWhse = "MIAMI"
Endcase

cASN = ""
nSTSets = 0
cSpecChar = .F.
XFERUPDATE = .T.
nFilenum = 0
lBackup = .F.
lOverage = .F.
lsendmail = .T.
lCopyCA = .T.
cErrMsg = ""
lIsError = .F.
lXfer944 = .F.
cTrk_WO = ""
nCTNTotal = 0

*!* SET CUSTOMER CONSTANTS
cDirUsed = lcOutPath
cCustName = "SAMSUNG"  && Customer Identifier (for folders, etc.)
cCustPrefix = "SG" && Customer Prefix (for output file names)
cX12 = "004010"  && X12 Standards Set used
cCourtMailName = "SAMSUNG"  && Recipient eMail name
crecid = "SAMSUNGPROD"  && Recipient EDI address

dt1 = Ttoc(Datetime(),1)
dt2 = Datetime()
dtmail = Ttoc(Datetime())

cFilename = (lcHoldPath+cCustPrefix+dt1+".944")
cFilename2 = (lcOutPath+cCustPrefix+dt1+".944")
cArchiveFile = (lcArchivePath+cCustPrefix+dt1+".944")

Store "" To c_CntrlNum,c_GrpCntrlNum
nFilenum = Fcreate(cFilename)

*!* SET OTHER CONSTANTS
nCtnNumber = 1
csendqual = "ZZ"
csendid = "TGFUSAPROD"
crecqual = "ZZ"
cfd = "*"  && Field/element delimiter
csegd = "~" && Line/segment delimiter
nSegCtr = 0
cterminator = ":" && Used at end of ISA segment
cdate = Dtos(Date())
ctruncdate = Right(cdate,6)
ctime = Left(Time(Date()),2)+Substr(Time(Date()),4,2)
cfiledate = cdate+ctime
cOrig = "J"
cQtyRec = "1"
cCTypeUsed = ""
cMissingCtn = "Missing Cartons: "+Chr(13)
Store 0 To nQtyExpected,nCTNDamaged,nCTNShortage,nCTNExpected,nCTNActual
Store 0 To nPOExpecte,nPOActual,nCTNShortage
Store "" To cString,cPONum,cStyle

** PAD ID Codes
csendidlong = Padr(csendid,15," ")
crecidlong = Padr(crecid,15," ")

*!* Serial number incrementing table
If !Used("serfile")
	Use ("F:\3pl\data\serial\SAMSUNG_serial") In 0 Alias serfile
Endif

*!* Trigger Data
If !Used('edi_trigger')
	cEDIFolder = "F:\3PL\DATA\"
	Use (cEDIFolder+"edi_trigger") In 0 Alias edi_trigger Order Tag wo_num
Endif

*!* Search table (parent)
*****changed 1/13/17 TMARG SQL
goffice=cOffice
useca("inwolog","wh",,,"mod=' "+goffice+"' and  wo_num ="+Transform(nWO_Num) )

**** changed 1/13/17 TMARG SQL

useca("indet","wh",,,"mod=' "+goffice+"' and  wo_num ="+Transform(nWO_Num) )

*!* Search table (xref for PO's)
nRCnt = 0
nISACount = 0
nSTCount = 1
nSECount = 1

Select inwolog
Wait Window "" Timeout 2
If !Seek(nWO_Num,'inwolog','wo_num')
	Wait Window "WO# "+cWO_Num+" not found in INWOLOG...Terminating" Timeout 3
	If !lTesting
		Do ediupdate With "MISS INWOLOG WO",.T.
		closeout()
		Return
	Endif
Endif


Select indet
If !Seek(nWO_Num,"indet","wo_num")
	waitstr = "WO# "+cWO_Num+" Not Found in Inbd Detail Table..."+Chr(13)+;
		"WO has not been confirmed...Terminating"
	Wait Window waitstr Timeout 3
	Do ediupdate With "WO NOT CONF. IN INDET"
	closeout()
	Return
Endif

Select inwolog
cAcctname = Trim(inwolog.acctname)
*cRefer_num = TRIM(inwolog.reference)
cRefer_num = ""
cAcct_ref = Alltrim(inwolog.acct_ref)
Do num_incr

Store "ISA"+cfd+"00"+cfd+"          "+cfd+"00"+cfd+"          "+cfd+csendqual+cfd+csendidlong+cfd+;
	crecqual+cfd+crecidlong+cfd+ctruncdate+cfd+ctime+cfd+"U"+cfd+"00401"+cfd+;
	PADL(c_CntrlNum,9,"0")+cfd+"0"+cfd+cIntUsage+cfd+cterminator+csegd To cString
Do cstringbreak

Store "GS"+cfd+"RE"+cfd+csendid+cfd+crecid+cfd+cdate+cfd+ctime+cfd+c_CntrlNum+;
	cfd+"X"+cfd+cX12+csegd To cString
Do cstringbreak

nSegCtr = 0
nISACount = 1

Store "ST"+cfd+"944"+cfd+Padl(c_GrpCntrlNum,9,"0")+csegd To cString
Do cstringbreak
nSegCtr = nSegCtr + 1

Store "W17"+cfd+cOrig+cfd+cdate+cfd+cAcct_ref+cfd+Trim(cWO_Num)+csegd To cString
Do cstringbreak
nSegCtr = nSegCtr + 1

Store "N1"+cfd+"ZL"+cfd+cAcctname+csegd To cString
Do cstringbreak
nSegCtr = nSegCtr + 1

Store "N9"+cfd+"CN"+cfd+Alltrim(inwolog.Container)+csegd To cString  && Used for either AWB or container type
Do cstringbreak
nSegCtr = nSegCtr + 1

Store "N9"+cfd+"TL"+cfd+cAcct_ref+csegd To cString
Do cstringbreak
nSegCtr = nSegCtr + 1

Store "G62"+cfd+"09"+cfd+Dtos(Date())+csegd To cString
Do cstringbreak
nSegCtr = nSegCtr + 1

nSECount = 0

Select indet
Sum totqty  To nSumTotQty  For wo_num = nWO_Num And indet.inwologid = inwolog.inwologid And units
nSumOrigQty = nSumTotQty
nDiffQty = (nSumTotQty)

lcType = ""
Select indet
Scan For wo_num = nWO_Num And indet.units
*useca("upcmast","wh",,,"select * from upcmast where accountid in (6649,6650,6651,6652)",,"upcmast")
	Set Step On
	If upcmastsql(indet.accountid, indet.Style,   indet.Color,  indet.Id )  && changed TMARG 6/23/16 to match on indet.aacountid
		lcUPC=upcmast.upc
		Select upcmast
		Scan For lcUPC=upcmast.upc
			If 'SUBLINE' $(Info)
				lcType='CA'
			Else
				lcType='EA'
			Endif
		Endscan
	Endif


*!*		lcType= getmemodata("echo","PACKAGE_TYPE")
*!*	  lcUPC= getmemodata("echo","UPC")
	Store "W07"+cfd+Alltrim(Str(indet.totqty))+cfd+lcType+cfd+cfd+"UP"+cfd+;
		TRIM(lcUPC)+csegd To cString
	nCTNExpected = nCTNExpected + 1
	Do cstringbreak
	nSegCtr = nSegCtr + 1

Endscan

Wait Window "END OF SCAN/CHECK PHASE ROUND..." Nowait

nSTCount = 0
Do close944
lXfer944 = .T.
=Fclose(nFilenum)


If Used('serfile')
	Use In serfile
Endif

If !lTesting
	If !Used("ftpedilog")
		Select 0
		Use F:\edirouting\ftpedilog Alias ftpedilog
		Insert Into ftpedilog (ftpdate,filename,acct_name,Type) Values (dt2,cFilename,"COURT","944")
		Use In ftpedilog
	Endif
Endif

Select edi_trigger

Wait "944 Creation process complete:"+Chr(13)+cFilename Window At 45,60 Timeout 3
*DO F:\polorl\prg\total944.prg WITH lXfer944,cFilename  && Checks expected vs. actual totals
cFin = "944 FILE "+cFilename2+" CREATED WITHOUT ERRORS"
Copy File &cFilename To &cArchiveFile
If !lHoldFile
	Copy File &cFilename To &cFilename2
	Erase &cFilename
Endif

If !lTesting
	Select edi_trigger
	Replace edi_trigger.processed With .T. For edi_trigger.wo_num = nWO_Num
	Replace edi_trigger.created With .T. For edi_trigger.wo_num = nWO_Num
	Replace edi_trigger.when_proc With Datetime() For edi_trigger.wo_num = nWO_Num
	Replace edi_trigger.fin_status With "944 CREATED" For edi_trigger.wo_num = nWO_Num
	Replace edi_trigger.errorflag With .F. For edi_trigger.wo_num = nWO_Num
	Replace file944crt With cFilename For edi_trigger.wo_num = nWO_Num
	sendmail()
Endif

Release All Like T*
closedata()

************************************************************************
************************************************************************
*!* END OF MAIN PROGRAM
************************************************************************
************************************************************************


****************************
Procedure close944
****************************
** Footer Creation

If nSTCount = 0
*!* For 944 process only!
	Store "W14"+cfd+Alltrim(Str(nSumTotQty))+;
		cfd+Alltrim(Str(nSumOrigQty))+;
		cfd+Alltrim(Str(nSumOrigQty-nSumTotQty))+csegd To cString
	Do cstringbreak
	nSegCtr = nSegCtr + 1
*!* End 944 addition

	Store  "SE"+cfd+Alltrim(Str(nSegCtr+1))+cfd+Padl(c_GrpCntrlNum,9,"0")+csegd To cString
	Fputs(nFilenum,cString)
	nSegCtr = 0
	nSTSets = nSTSets + 1
Endif

Store  "GE"+cfd+Alltrim(Str(nSTSets))+cfd+c_CntrlNum+csegd To cString
Fputs(nFilenum,cString)

Store  "IEA"+cfd+"1"+cfd+Padl(c_CntrlNum,9,"0")+csegd To cString
Fputs(nFilenum,cString)

Endproc

****************************
Procedure cstringbreak
****************************
Fputs(nFilenum,cString)
Endproc

****************************
Procedure num_incr
****************************
Select serfile
Replace serfile.seqnum With serfile.seqnum + 1 In serfile
Replace serfile.grpseqnum With serfile.grpseqnum + 1 In serfile
c_CntrlNum = Alltrim(Str(serfile.seqnum))
c_GrpCntrlNum = Alltrim(Str(serfile.grpseqnum))
Endproc

****************************
Procedure ediupdate
****************************
Parameters cFin
If !lTesting
	Select edi_trigger
	Replace processed With .T.
	Replace created With .F.
	Replace fin_status With cFin
	If lIsError
		Replace errorflag With .T.
	Else
		Replace when_proc With Datetime()
		Replace errorflag With .F.
	Endif
	Blank Fields edi_trigger.file944crt Next 1
	If lsendmail
		sendmail()
	Endif
Endif
closeout()
Endproc

****************************
Procedure closeout
****************************
If !Empty(nFilenum)
	=Fclose(nFilenum)
	Erase &cFilename
Endif
If Used('barcodes')
	Select BARCODES
	Set Filter To
	Use In BARCODES
Endif
closedata()
Endproc

****************************
Procedure closedata
****************************
If Used('barcodes')
	Select BARCODES
	Set Filter To
	Use In BARCODES
Endif
If Used('edi_xfer')
	Use In edi_xfer
Endif
If Used('csrBCdata')
	Use In csrBCdata
Endif
If Used('detail')
	Use In Detail
Endif
If Used('indet')
	Use In indet
Endif
If Used('inwolog')
	Use In inwolog
Endif

On Error
Wait Clear
_Screen.Caption = "INBOUND POLLER - EDI"
Return

****************************
Procedure sendmail
****************************
tfrom = "TGF EDI Operations <toll-edi-ops@tollgroup.com>"

If lIsError
	tsendto = tsendtoerr
	tcc = tccerr
	tsubject = cCourtMailName+" EDI ERROR, Inv. WO "+cWO_Num
Else
	tsubject = cCourtMailName+" EDI FILE (944) Created, Inv. WO "+cWO_Num
Endif

tattach = " "
If (cTrk_WO = "0" Or Empty(cTrk_WO))
	cThisTrk_WO = "MISSING"
	tmessage = "Inv. WO #: "+cWO_Num++Chr(10)
Else
	cThisTrk_WO = cTrk_WO
	tmessage = "Inv. WO #: "+cWO_Num+", Trucking WO #: "+cThisTrk_WO+Chr(10)
Endif

If Empty(cErrMsg)
	tmessage = tmessage + cFin
Else
	tmessage = tmessage + cErrMsg
Endif

If lEMail
	Do Form m:\dev\frm\dartmail2 With tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
Endif

Endproc


****************************
Procedure errormail
****************************
If nDiffQty > 0
	tnote = "n OVERAGE"
Else
	tnote = " SHORTAGE"
Endif
tsendto = tsendtotest
tcc = tccerr
tfrom = "TGF EDI Operations <toll-edi-ops@tollgroup.com>"
tsubject = "ERROR: Courtaulds 944 WO# "+cWO_Num
tmessage = "The 944 for this WO was received as a"+ tnote
tmessage = "Original Quantity per ASN: "+Alltrim(Str(nSumOrigQty))
tmessage = "Quantity As Received: "+Alltrim(Str(nSumTotQty))

Do Form m:\dev\frm\dartmail2 With tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
Endproc

****************************
Procedure segmentget
****************************
Parameter thisarray,lcKey,nLength

For i = 1 To nLength
	If i > nLength
		Exit
	Endif
	lnEnd= At("*",thisarray[i])
	If lnEnd > 0
		lcThisKey =Trim(Substr(thisarray[i],1,lnEnd-1))
		If Occurs(lcKey,lcThisKey)>0
			Return Substr(thisarray[i],lnEnd+1)
			i = 1
		Endif
	Endif
Endfor

Return ""
