**sears redeployment flat file send

wait "Sears Reurn File Processing..." window nowait noclear

xpath="f:\ftpusers\meldisco\miraloma\prod\sis\as2download\mymailbox\"
*xpath="c:\xls\"

xarchivepath="F:\FTPUSERS\Meldisco\MiraLoma\Prod\SIS\AS2DownLoad\RedeployArchive\"
*xarchivepath="c:\xls\archive\"

xsethours=set("Hours")
xsetdate=set("Date")
xsetcentury=set("Century")

set century on
set hours to 24

**only send if last file sent is at least 2 hours old
xfilecnt=adir(afiles,xarchivepath+"SEARS_REDEPLOY*.*")
if xfilecnt>0
	create cursor xfilelist (file c(100), x c(10), moddt C(10), modtm c(10), moddttm t(1))
	append from array afiles
	replace all moddttm with ctot(moddt+" "+modtm)

	select * from xfilelist order by moddttm desc into cursor xtemp
	use in xfilelist

	**if most recent file is within 2 hours, exit
	if moddttm >= datetime()-(2*60*60)
		use in xtemp
		wait clear
		return
	endif

	use in xtemp
endif

set hours to 12

**this sqlsetprop disallows the connection dialog box from appearing if the sqlstrinconnect() fails - mvw 12/28/11
sqlsetprop(0,'DispLogin',3)
sqlsetprop(0,"dispwarnings",.F.)

if type("gsqlpassword")="U"
	use f:\wh\sqlpassword in 0
	gsqlpassword=alltrim(sqlpassword.password)
	use in sqlpassword
endif

cServer="tgfnjsql01"	&& dy 2/18/18
cDB="pickpack"
lcDSNLess="driver=SQL Server;server=&cServer;uid=SA;pwd=&gsqlpassword;DATABASE=&cDB"

xhandle=Sqlstringconnect(m.lcDSNLess,.T.)
If xhandle<=0 && bailout
	xmsg="No connection can be made to "+cServer+"!!"
	email("mwinter@fmiint.com","RETURNS RPOCESSING - searsret.prg (Edi Export): "+xmsg,,,,,.T.,,,,,.T.,,.T.)
	wait clear
	return .f.
EndIf

**grab everything processed 2+ hours ago in case changes need to be made to a given UCC after processing (unit qty, etc)
**added filter for outshipid (which holds bl.blid), do not send anything not already on a BL - mvw 08/08/13
xquery="Select * from cartons where ship_ref='SEARS RETURNS' and accountid = 5837 and insdttm < '"+transform(datetime()-(2*60*60))+"' and status != 'S' and outshipid !=0 order by ucc"
if sqlexec(xhandle,xquery,"xcartons")<=0
	xmsg="Error running SQL query, carton could not be created:"+chr(13)+chr(13)+xquery
	email("mwinter@fmiint.com","RETURNS RPOCESSING - POSTPROC.TXTCOMMAN.VALID(): "+xmsg,,,,,.T.,,,,,.T.,,.T.)
	wait clear
	return .f.
endif


if eof()
	use in xcartons
	wait clear
	return
endif

xfilestr=""

scan
	**dont send anything with a BL that was added less than 2 hours ago
	xsqlexec("select * from bl where blid="+transform(xcartons.outshipid),,,"wh")
	if bl.adddt>datetime()-(2*60*60)
		loop
	endif

	**update status 1st, if unable loop and we will send that ucc in the next file
	xquery="update cartons set status = 'S' where carton_id = "+transform(xcartons.carton_id)
	if sqlexec(xhandle,xquery,"xupdate")<=0
		xmsg="Error running SQL query, carton could not be updated:"+chr(13)+chr(13)+xquery
		email("mwinter@fmiint.com","RETURNS RPOCESSING - POSTPROC.TXTCOMMAN.VALID(): "+xmsg,,,,,.T.,,,,,.T.,,.T.)
		loop
	endif

	**Trailer (12a): Trailer number 
	**Carton ID (20n): Carton ID from shipping label
	**UPC (14n): Product identifier.  Left justified, space filled on right
	**Casepack (7n): Number of units to a full case for this product
	**EPI (7n):	Future Use.  Populate with 0000001
	**Quantity (7n): Number of units in carton
	xfilestr=xfilestr+padr(alltrim(ship_via),12," ")+padr(alltrim(ucc),20)+padr(alltrim(upc),14)+padl(alltrim(pack),7,"0")+"0000001"+padl(transform(totqty),7,"0")+chr(13)+chr(10)
endscan

set database to WH
close databases 

set date YMD
set hours to 24
set century on

if !empty(xfilestr)
	**filename: SEARS_REDEPLOY.txt.yyyymmddhhmm
	xfilename="SEARS_REDEPLOY.txt."+strtran(strtran(strtran(transform(datetime())," ",""),":",""),"/","")
	xfile=xpath+xfilename
	xarchivefile=xarchivepath+xfilename

	=strtofile(xfilestr,xfile)
	=strtofile(xfilestr,xarchivefile)
endif

use in xcartons
=sqldisconnect(0)

set hours to &xsethours
set date &xsetdate
set century &xsetcentury

wait clear
