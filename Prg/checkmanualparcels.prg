CLOSE DATA ALL
CLEAR
USE F:\3pl\DATA\parcel_carriers IN 0 ALIAS carriers
CREATE CURSOR tempnobol (goffice c(1),accountid N(5),ship_ref c(20),del_date d,scac c(5),keyrec c(22),inshipmt l)


FOR i = 1 TO 12
	goffice = ICASE(i=1,"1",i=2,"2",i=3,"5",i=4,"6",i=5,"7",i=6,"L",i=7,"M",i=8,"I",i=9,"J",i=10,"K",i=11,"S","Y")
	WAIT WINDOW "Now processing Mod "+goffice NOWAIT

	IF USED('outship')
		USE IN outship
	ENDIF

	xsqlexec("select del_date,ship_ref,scac,keyrec from outship where mod = '"+goffice+"' and bol_no = '' and del_date > {"+TRANSFORM(DATE()-15)+"}","xoutship",,"wh")

	SELECT xoutship
	LOCATE
	IF RECCOUNT()=0
		WAIT WINDOW "No empty BOLs, Mod "+goffice TIMEOUT 1
		LOOP
	ENDIF
	SCAN
		m.inshipmt = .F.
		IF !SEEK(xoutship.scac,"carriers","scac")
			DELETE NEXT 1 IN xoutship
			LOOP
		ENDIF
		cShip_ref = ALLTRIM(xoutship.ship_ref)
		nAcctNum = xoutship.accountid
		xsqlexec("select * from shipment where pickticket = '"+cShip_ref+"' and accountid = "+TRANSFORM(nAcctNum),,,"wh")
		SELECT shipment
		IF RECCOUNT()>0
			m.inshipmt = .T.
		ENDIF
		USE IN shipment
		SELECT xoutship
		SCATTER MEMVAR
		INSERT INTO tempnobol FROM MEMVAR
	ENDSCAN
ENDFOR
SELECT tempnobol
IF RECCOUNT()>0
	cfile = "h:\fox\nobol_parcel.xls"
	COPY TO &cfile TYPE XL5
	tsubject= "TGF Manual Parcel Issue Report: " +TTOC(DATETIME())
	tattach = ""
	tmessage = "The attached file contains a list of parcel shipments of the past 10 days"
	tmessage = tmessage+CHR(13)+"Check the PTs listed to see if KEYREC data must be moved to the BOL_NO field."
	tFrom ="TGF EDI Operations <toll-edi-ops@tollgroup.com>"
	tcc = ""
	tsendto = "joe.bianchi@tollgroup.com"
	tattach = cfile
	DO FORM m:\dev\frm\dartmail2 WITH tsendto,tFrom,tsubject,tcc,tattach,tmessage,"A"
ELSE
	WAIT WINDOW "No missing-BOL parcel records found" TIMEOUT 2
ENDIF
CLOSE DATA ALL
RETURN
