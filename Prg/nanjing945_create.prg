*!* NANJING USA 945 (Whse. Shipping Advice) Creation Program
*!* Creation Date: 06.01.2005 by Joe
*!* Last update: 08.01.2005 by Joe

PARAMETERS nWO_Num,cBOL,cShip_ref,cOffice

PUBLIC ARRAY thisarray(1)
PUBLIC cSCCNumber,nSCCSuffix,cWO_NumStr,cCustname,nUnitSum,nPTCount,cPPName,cErrMsg,cUseFolder,lTestInput,lMismatch,cISA_Num,lParcelType,lUseTrigger
PUBLIC lRuralKing,lJCPenney,cFreight,lTesting,cMod,cMBOL,cFileNameShort,cFilenameOut,cFilenameHold,cFilenameArch,cWO_NumList,lcPath,cEDIType,cSubBOLDetail,cProgName
PUBLIC tsendto,tcc,lOverflow,lLoadSQLBL,lcArchivePath,lcHoldPath,lNanfilesout

cProgName = "nanjing945_create.prg"

SET ESCAPE ON
ON ESCAPE CANCEL
SET ASSERTS ON
SET DELETED ON
lIsError = .F.
lDoCatch = .T.
cShip_ref = ""
lCloseOutput = .T.
nFilenum = 0
cErrMsg = ""
tfrom = "Toll EDI Operations <toll-edi-ops@tollgroup.com>"
cFreight = ""
cSubBOLDetail = ""
lOverflow = .f.

IF cBOL = '04907454694685432'
lOverflow = .t.
ENDIF

TRY
	nSCCSuffix = 0
	nAcctnum = 4694

	lTesting = .f.
	lTestInput = .f.
	lEmail = .T.
	lNanfilesout = .t.
	lTestMail = lTesting && Sends mail to Joe only
	lStandalone = lTesting
	cWinWait = IIF(lTesting,"TIMEOUT 2","NOWAIT")

	lJCPenney = .F.
	lUseTrigger = .T.
	lDoSQL = .T.
	lDoManUC = .F.

	IF TYPE("cOffice") = "L"
		IF !lTesting
			lCloseOutput = .F.
			WAIT WINDOW "Office not provided...terminating" TIMEOUT 3
			cErrMsg = "No OFFICE"
			DO ediupdate WITH cErrMsg,.T.
			THROW
		ELSE
			lUseTrigger = .F.
			CLOSE DATABASES ALL
			cBOL = "04907454694931843"
			cOffice = "Y"
		ENDIF
	ENDIF
	cMod = IIF(cOffice = "C","1",cOffice)
	gMasterOffice = IIF(cOffice = "I","N",cOffice)
	gOffice = cMod

	cBOL = ALLTRIM(cBOL)

	cUseFolder = "F:\WHY\WHDATA\"
	IF lTestInput
		cUseFolder = "F:\WHP\WHDATA\"
	ELSE
		cUseFolder = ("F:\WH"+cMod+"\WHOFFICE\")
		cLocAddr = ICASE(cOffice = "Z","TO", cOffice = "I","NJ",cOffice = "M","FL",cOffice = "C","CA","C2")
		WAIT WINDOW "Folder used: "+cLocAddr &cWinWait
	ENDIF

	lLoadSQLBL = .T.

	cEDIType = "945"

	IF USED('outship')
		USE IN outship
	ENDIF
	IF USED('outdet')
		USE IN outdet
	ENDIF

	xsqlexec("select * from outship where accountid = "+TRANSFORM(nAcctnum)+" and bol_no = '"+cBOL+"'",,,"wh")
	xsqlexec("select * from zoutship where accountid = "+TRANSFORM(nAcctnum)+" and bol_no = '"+cBOL+"'",,,"wh")
	SELECT outship
	APPEND FROM DBF('zoutship')
	USE IN zoutship
	INDEX on bol_no TAG bol_no
	INDEX on outshipid TAG outshipid
	INDEX on wo_num TAG wo_num

	selectoutdet()
	SELECT outdet
	INDEX ON outdetid TAG outdetid

	SELECT outship

	=SEEK(cBOL,'outship','bol_no')
	nWO_Num = outship.wo_num
	cShip_ref = ALLTRIM(outship.ship_ref)

	IF TYPE("nWO_Num")<> "N"
		lCloseOutput = .F.
		cErrMsg = "BAD WO#"
		DO ediupdate WITH cErrMsg,.T.
		THROW
	ENDIF
	cWO_Num = ALLTRIM(STR(nWO_Num))

	WAIT WINDOW "Now checking for SWC cut cartons..." &cWinWait
	lMBOL = .F.
	cMBOL = ""
	IF USED('bl')
		USE IN bl
	ENDIF

	IF lLoadSQLBL
		csq1 = [select * from bl where mblnum = ']+cBOL+[' and accountid in (4610,4694)]
		xsqlexec(csq1,,,"wh")
		LOCATE
		IF !EOF()
			lMBOL = .T.
		ENDIF
	ELSE
		SELECT 0
		USE (cUseFolder+'bl') ALIAS bl
		IF SEEK(cBOL,'bl','mblnum')
			IF INLIST(bl.accountid,4610,4694)
				lMBOL = .T.
			ENDIF
		ENDIF
	ENDIF

	IF lMBOL
		STORE cBOL TO cMBOL
		cSubBOLDetail = ""
		SELECT bl
		SCAN FOR mblnum = cMBOL
			cxBOL = ALLTRIM(bl.bol_no)
			cSubBOLDetail = IIF(EMPTY(cSubBOLDetail),cxBOL,cSubBOLDetail+CHR(13)+cxBOL)
			DO m:\dev\prg\swc_cutctns WITH cxBOL
		ENDSCAN
	ELSE
		DO m:\dev\prg\swc_cutctns WITH cBOL
	ENDIF
	IF USED('bl')
		USE IN bl
	ENDIF
	RELEASE lMBOL,cxBOL

*lTestMail = .T.

	lPrepack = .T.
	lPick = .F.

	PUBLIC c_CntrlNum,c_GrpCntrlNum
	WAIT WINDOW "Now preparing tables and variables" NOWAIT NOCLEAR

*!* SET CUSTOMER CONSTANTS
	cCustname = "NANJING"  && Customer Identifier
	cX12 = "004050"  && X12 Standards Set used
	cMailName = "Nanjing USA"
	cPPName = "Nanjing"
	SELECT 0
	USE F:\3pl\DATA\mailmaster ALIAS mm
	LOCATE FOR edi_type = '945' AND mm.office = cOffice AND mm.accountid = nAcctnum
	tsendto = IIF(mm.use_alt,sendtoalt,mm.sendto)
	tcc = IIF(mm.use_alt,mm.ccalt,mm.cc)
	lcPath = ALLTRIM(mm.basepath)
	lcArchivePath = ALLTRIM(mm.archpath)
	lcHoldPath = ALLTRIM(mm.holdpath)
	LOCATE
	LOCATE FOR mm.edi_type = 'MISC' AND mm.taskname = 'GENERAL'
	tsendtoerr = IIF(mm.use_alt,sendtoalt,mm.sendto)
	tccerr = IIF(mm.use_alt,mm.ccalt,mm.cc)
	tsendtotest = IIF(mm.use_alt,sendtoalt,mm.sendto)
	tcctest = IIF(mm.use_alt,mm.ccalt,mm.cc)
	LOCATE
	LOCATE FOR mm.edi_type = 'MISC' AND mm.taskname = 'NANQTYERR'
	tsendtoqty = IIF(mm.use_alt,sendtoalt,mm.sendto)
	tccqty = IIF(mm.use_alt,mm.ccalt,mm.cc)


*!* SET OTHER CONSTANTS
	DO CASE
	CASE cOffice = "Z"
		cCustLoc =  "TO"
		cFMIWarehouse = ""
		cCustPrefix = "945"+"t"
		cDivision = "Toronto"
		cSF_Addr1  = "6465 NORTHAM DR"
		cSF_CSZ    = "MISSISSAUGA*ON*L4V1J2"

	CASE cOffice = "M"
		cCustLoc =  "FL"
		cFMIWarehouse = ""
		cCustPrefix = "945"+"f"
		cDivision = "Florida"
		cSF_Addr1  = "11400 NW 32ND AVE"
		cSF_CSZ    = "MIAMI*FL*33167"

	CASE INLIST(cOffice,"I","N")
		cCustLoc =  "NJ"
		cFMIWarehouse = ""
		cCustPrefix = "945"+"j"
		cDivision = "New Jersey"
		cSF_Addr1  = "800 FEDERAL BLVD"
		cSF_CSZ    =  "CARTERET*NJ*07008"

	OTHERWISE
		cCustLoc =  "C2"
		cFMIWarehouse = ""
		cCustPrefix = "945"+"y"
		cDivision = "Carson2"
*		cFolder = "WHC"
		cSF_Addr1  = "1000 E 223RD ST"
		cSF_CSZ    = "CARSON*CA*90745"
	ENDCASE
	cCustFolder = UPPER(cCustname)  && +"-"+cCustLoc
	lNonEDI = .F.  && Assumes not a Non-EDI 945
	lDoPAL = .T.
	STORE "" TO lcKey
	STORE 0 TO alength,nLength

*!* SET OTHER CONSTANTS
	dt1 = TTOC(DATETIME(),1)
	dtmail = TTOC(DATETIME())
	dt2 = DATETIME()
	cString = ""

	csendqual = "ZZ"
	csendid = "FMIF"
	crecqual = "01"
	crecid = "654351030"

	cfd = CHR(0x07)
	csegd = "~"
	nSegCtr = 0
	cterminator = ">" && Used at end of ISA segment
	cdate = DTOS(DATE())
	ctruncdate = RIGHT(cdate,6)
	ctime = LEFT(TIME(DATE()),2)+SUBSTR(TIME(DATE()),4,2)
	cfiledate = cdate+ctime
	cOrig = "J"
	nSTCount = 0
	cStyle = ""
	cPTString = ""
	nTotCtnWt = 0
	nTotCtnCount = 0
	nUnitSum = 0
	cTargetStyle = ""

** PAD ID Codes
	csendidlong = PADR(csendid,15," ")
	crecidlong = PADR(crecid,15," ")

	lcPath = IIF(lTesting,("F:\FTPUSERS\Nanjing\945out\test\"),lcPath)
	cFilenameHold = (lcHoldPath+cCustPrefix+dt1+".txt")
	cFileNameShort = JUSTFNAME(cFilenameHold)
	cFilenameArch = UPPER(lcArchivePath+cCustPrefix+dt1+".txt")
	cFilenameOut = (lcPath+cFileNameShort)
	IF lTesting
	cFilenameOut = (cFilenameOut+"test\")
	endif
	nFilenum = FCREATE(cFilenameHold)

	CREATE CURSOR qtycompare (wo_num i,ship_ref c(10), osqty i, ppqty i,pctdiff i)

	IF !USED('OUTDET')
		USE (cUseFolder+"OUTDET") IN 0 ALIAS outdet ORDER TAG outdetid
		SELECT outdet
		LOCATE
	ENDIF

	IF !USED("scacs")
		xsqlexec("select * from scac","scacs",,"wh")
		INDEX ON scac TAG scac
	ENDIF

	USE F:\3pl\DATA\parcel_carriers IN 0 ALIAS carriers

	CREATE CURSOR tempnan945 (accountid i,isa_num c(10),st_num c(10),wo_num i,bol_no c(20),ship_ref c(20),filename c(50),filedate T)

&& Added code blocks to the following to overcome BOL vs. WO selection issues, 12.23.2005
	SELECT outship
	IF USED("sqlwonanjing")
		USE IN sqlwonanjing
	ENDIF
	DELETE FILE "F:\3pl\DATA\sqlwonanjing.dbf"

	SELECT bol_no,wo_num ;
		FROM outship WHERE bol_no = cBOL ;
		GROUP BY 1,2 ;
		ORDER BY 1,2 ;
		INTO DBF F:\3pl\DATA\sqlwonanjing
	USE IN sqlwonanjing
	USE F:\3pl\DATA\sqlwonanjing IN 0 ALIAS sqlwonanjing

	SELECT sqlwonanjing
*	BROWSE

	ASSERT .F. MESSAGE "At SQL connect...debug"
	IF lDoSQL
		cRetMsg = "X"
		ASSERT .F. MESSAGE "At sqlconnectnanjing load"
		DO m:\dev\prg\sqlconnectnanjing_bol  WITH nAcctnum,cCustname,cPPName,.T.,cOffice && Amended for UCC number sequencing	ENDIF

		SELECT vnanjingpp
		LOCATE

		IF cRetMsg<>"OK"
			cErrMsg = cRetMsg
			DO ediupdate WITH cErrMsg,.T.
			THROW
		ENDIF
	ENDIF

	SELECT ucc,SUM(totqty) AS uccqty FROM vnanjingpp GROUP BY 1 INTO CURSOR tempchk HAVING uccqty=0
	SELECT tempchk
	LOCATE
	IF !EOF()
		SCAN
			STORE ALLT(ucc) TO cUCC
			SELECT vnanjingpp
			LOCATE
			DELETE FOR ucc = cUCC
		ENDSCAN
	ENDIF
	USE IN tempchk

	CREATE CURSOR tempqtyerr (ship_ref c(20),ctnqty i,totqty i,cqty i,uqty i)
	IF lOverflow
		SELECT ship_ref,origqty AS ctnqty FROM outship WHERE bol_no = cBOL INTO CURSOR temptotals
	ELSE
		SELECT ship_ref,ctnqty FROM outship WHERE bol_no = cBOL INTO CURSOR temptotals
	ENDIF
	SELECT temptotals
	LOCATE

	SCAN
		cSRUse = ALLTRIM(temptotals.ship_ref)
		SELECT vnanjingpp
		SELECT 0
		SELECT DISTINCT(ucc) FROM vnanjingpp WHERE vnanjingpp.ship_ref = cSRUse INTO CURSOR tempxyz
		m.uqty = RECCOUNT()
		m.cqty = RECCOUNT()
		USE IN tempxyz
		IF (uqty # temptotals.ctnqty) OR (cqty # temptotals.ctnqty)
			INSERT INTO tempqtyerr (ship_ref,ctnqty,totqty,cqty,uqty) VALUES (cSRUse,temptotals.ctnqty,temptotals.ctnqty,m.cqty,m.uqty)
		ENDIF
	ENDSCAN

	SELECT tempqtyerr
	LOCATE
	IF lOverflow
		IF !EOF()
			IF lTesting
*			BROWSE
			ENDIF
			totqtyerr()
			cErrMsg = "OD/CZN QTY DISCREP."
			DO ediupdate WITH cErrMsg,.T.
			THROW
		ENDIF
	ENDIF

	SELECT vnanjingpp
	SET RELATION TO outdetid INTO outdet

*!* Added this block to trap duplicated scans in SQL, 10.21.2015

	SELECT outship
	LOCATE
	cErrMsg = ""
	
	
	#if 0
	lMismatch = .F.
	IF !lOverflow
		SCAN FOR bol_no = cBOL
			cShip_ref = ALLTRIM(outship.ship_ref)
*			cShip_ref = IIF(cbol='04907314694215902',STRTRAN(cShip_ref," OV",""),cship_ref)
			IF (" OV"$cShip_ref)
				LOOP
			ENDIF

			nPTCtns = outship.ctnqty
			SELECT vnanjingpp
			COUNT TO nSQLCtns FOR vnanjingpp.ship_ref = PADR(cShip_ref,20)
			IF nPTCtns # nSQLCtns
				SET STEP ON
				lMismatch = .T.
				cErrMsg = IIF(EMPTY(cErrMsg),"Mismatch Qty for PT(s) "+cShip_ref+CHR(10),cErrMsg+cShip_ref+CHR(10))
			ENDIF
		ENDSCAN
	ELSE
		LOCATE 
		cShip_ref = ALLTRIM(STRTRAN(cShip_ref," OV",""))
*			cShip_ref = IIF(cbol='04907314694215902',STRTRAN(cShip_ref," OV",""),cship_ref)
		SUM outship.ctnqty TO nPTCtns FOR outship.ship_ref = cShip_ref
		SELECT vnanjingpp
		COUNT TO nSQLCtns FOR vnanjingpp.ship_ref = cShip_ref
		IF nPTCtns # nSQLCtns
			SET STEP ON
			lMismatch = .T.
			cErrMsg = IIF(EMPTY(cErrMsg),"Mismatch Qty for PT(s) "+CHR(13)+cShip_ref+CHR(13),cErrMsg+cShip_ref+CHR(13))
		ENDIF
	ENDIF
	RELEASE nPTCtns,nSQLCtns
	IF lMismatch
		DO ediupdate WITH cErrMsg,.T.
		THROW
	ELSE
		RELEASE lMismatch
	ENDIF
	#endif
*!* End added code

	LOCATE
	WAIT CLEAR
	WAIT WINDOW "Now creating Header information" NOWAIT NOCLEAR

*!* HEADER LEVEL EDI DATA
	DO num_incr_isa

	cISA_Num = PADL(c_CntrlNum,9,"0")
	nISA_Num = INT(VAL(cISA_Num))
	lISAType = IIF(lTesting,"T","P")

	STORE "ISA"+cfd+"00"+cfd+"          "+cfd+"00"+cfd+"          "+cfd+csendqual+cfd+csendidlong+cfd+;
		crecqual+cfd+crecidlong+cfd+ctruncdate+cfd+ctime+cfd+"U"+cfd+"00401"+cfd+;
		cISA_Num+cfd+"0"+cfd+lISAType+cfd+cterminator+csegd TO cString
	DO cstringbreak

	STORE "GS"+cfd+"SW"+cfd+csendid+cfd+crecid+cfd+cdate+cfd+ctime+cfd+c_CntrlNum+;
		cfd+"X"+cfd+cX12+csegd TO cString
	DO cstringbreak

	SELECT outship
	LOCATE

	IF !SEEK(nWO_Num,"outship","wo_num")
		WAIT CLEAR
		WAIT WINDOW "Invalid Work Order Number - Not Found in OUTSHIP!" TIMEOUT 2
		cErrMsg = "WO# NOT FOUND"
		DO ediupdate WITH cErrMsg,.T.
		THROW
	ENDIF

	LOCATE
	IF EMPTY(cBOL)  && May be empty if triggered from PT screen, or FedEx shipment
		WAIT CLEAR
		WAIT WINDOW "MISSING Bill of Lading Number!" TIMEOUT 2
		cErrMsg = "BOL# EMPTY"
		DO ediupdate WITH cErrMsg,.T.
		THROW
	ELSE
		IF !SEEK(PADR(TRIM(cBOL),20),"outship","bol_no")
			WAIT CLEAR
			WAIT WINDOW "Invalid Bill of Lading Number - Not Found!" TIMEOUT 2
			cErrMsg = "BOL# NOT FOUND"
			DO ediupdate WITH cErrMsg,.T.
			THROW
		ENDIF
	ENDIF

*************************************************************************
*1	PTIDSOURCE/OUTSHIP RECORD MAIN LOOP
*	Loops through individual OUTSHIP lines for BOL#
*************************************************************************

*	ASSERT .F. MESSAGE "At start of OUTSHIP loop"

*!* Updated this section to make scan EDI_TRIGGER- vs. OUTSHIP-based unless in Testing, 08/01/05
*!* This allows only PT's which have included BOLs to process, and skips unshipped or unmarked BOLs in OUTSHIP.

	STORE "" TO cWO_NumStr,cWO_NumList
	STORE "XXX" TO cWO_NumOld
	SELECT 0
	SELECT DISTINCT wo_num FROM outship WHERE bol_no = cBOL INTO CURSOR tempwofile
	LOCATE
	SCAN
		cWO_Num = ALLTRIM(STR(tempwofile.wo_num))
		IF TRIM(cWO_Num) <> TRIM(cWO_NumOld)
			STORE cWO_Num TO cWO_NumOld
			IF EMPTY(cWO_NumStr)
				STORE cWO_Num TO cWO_NumStr
			ELSE
				cWO_NumStr = (cWO_NumStr+", "+cWO_Num)
			ENDIF
			IF EMPTY(cWO_NumList)
				STORE cWO_Num TO cWO_NumList
			ELSE
				cWO_NumList = (cWO_NumList+CHR(13)+cWO_Num)
			ENDIF
		ENDIF
	ENDSCAN
	USE IN tempwofile

	scanstr = "bol_no = cBOL"
	SELECT outship
	LOCATE
	SET ORDER TO

	WAIT CLEAR
	WAIT WINDOW "Now creating BOL#-based information..." NOCLEAR &cWinWait

	nPTCount = 0
	IF lOverflow
		SET DELETED OFF
	ENDIF


	SCAN FOR &scanstr
		WAIT WINDOW "" TIMEOUT 1
		cShip_ref = ALLTRIM(outship.ship_ref)
*			cShip_ref = IIF(cbol='04907314694215902',STRTRAN(cShip_ref," OV",""),cship_ref)
		IF (" OV"$cShip_ref) OR (RIGHT(ALLTRIM(cShip_ref),1)=".")
			LOOP
		ENDIF

		nWO_Num = outship.wo_num
		cWO_Num = ALLTRIM(STR(outship.wo_num))
		cPadWO_Num = PADR(cWO_Num,10)
		IF TRIM(cWO_Num) <> TRIM(cWO_NumOld)
			STORE cWO_Num TO cWO_NumOld
			IF EMPTY(cWO_NumStr)
				STORE cWO_Num TO cWO_NumStr
			ELSE
				cWO_NumStr = (cWO_NumStr+", "+cWO_Num)
			ENDIF
		ENDIF


		lNJPick = IIF("NJPICK"$outship.shipins,.T.,.F.)
		lNJPick = .F.  && Remove this for NJ Prepack-as-pickpack files
		lJCPenney = IIF("PENNEY"$outship.consignee,.T.,.F.)
		lRuralKing = IIF(outship.consignee = "RURAL KING",.T.,.F.)

		nOS_Qty = outship.qty

		IF EMPTYnul(outship.del_date) AND !lTesting
			LOOP
		ENDIF

		SCATTER MEMVAR MEMO
		IF "PREPACK"$outship.shipins
			lPrepack = .T.
			lPick = .F.
			WAIT WINDOW "This is a PREPACK order..." NOWAIT
		ENDIF
		IF "PICKPACK"$outship.shipins
			lPrepack = .F.
			lPick = .T.
		ENDIF
		IF lTesting
*			cBOL = TRIM(outship.bol_no)
		ENDIF
		alength = ALINES(apt,outship.shipins,.T.,CHR(13))
		nOutshipID = outship.outshipid
		nTotCtnCount = m.qty
		cPO_Num = ALLTRIM(outship.cnee_ref)
		nPTCount = nPTCount + 1

		IF ("OV"$cShip_ref)
			ASSERT .F. MESSAGE "At OV segment"
			DO CASE
			CASE "OV 2"$cShip_ref
				cShip_refedi = LEFT(cShip_ref,8)+"2"
			CASE "OV"$cShip_ref
				cShip_refedi = LEFT(cShip_ref,8)+"1"
			ENDCASE
		ELSE

			IF "!!"$cShip_ref
				cBaseShip_ref = LEFT(cShip_ref,AT("!",cShip_ref,1)-1)
				cShip_ref2 = LEFT(cShip_ref,AT("!",cShip_ref,1)-4)
				nSuffix = INT(VAL(RIGHT(cBaseShip_ref,3)))
				DO CASE
				CASE ("!!D"$cShip_ref)
					cSuffix = PADL(ALLTRIM(STR(nSuffix+4)),3,'0')
				CASE ("!!C"$cShip_ref)
					cSuffix = PADL(ALLTRIM(STR(nSuffix+3)),3,'0')
				CASE ("!!B"$cShip_ref)
					cSuffix = PADL(ALLTRIM(STR(nSuffix+2)),3,'0')
				OTHERWISE
					cSuffix = PADL(ALLTRIM(STR(nSuffix+1)),3,'0')
				ENDCASE
				cShip_refedi = cShip_ref2+cSuffix
			ELSE
				cShip_refedi = cShip_ref
			ENDIF
		ENDIF


		cWO_Num = ALLTRIM(STR(m.wo_num))
		cPTString = IIF(EMPTY(cPTString),m.consignee+" "+cShip_ref,cPTString+CHR(13)+m.consignee+" "+cShip_ref)
		m.CSZ = TRIM(m.CSZ)

		IF EMPTY(M.CSZ)
			WAIT CLEAR
			WAIT WINDOW "No SHIP-TO City/State/ZIP info...exiting" TIMEOUT 2
			cErrMsg = "BAD ADDRESS INFO"
			DO ediupdate WITH cErrMsg,.T.
			THROW
		ENDIF
		cCity = segmentget(@apt,"CITY",alength)
		cState = segmentget(@apt,"STATE",alength)
		cZip = segmentget(@apt,"ZIPCODE",alength)

		IF EMPTY(cCity) OR EMPTY(cState) OR EMPTY(cZip)
			m.CSZ = ALLTRIM(STRTRAN(m.CSZ,"  "," "))
			nSpaces = OCCURS(" ",ALLTRIM(m.CSZ))
			nCommaPos = AT(",",m.CSZ)
			nLastSpace = AT(" ",m.CSZ,nSpaces)

			IF nSpaces > 0
				IF ISALPHA(SUBSTR(TRIM(m.CSZ),AT(" ",m.CSZ,nSpaces-1)+1,2))
					cCity = LEFT(TRIM(m.CSZ),AT(",",m.CSZ)-1)
					cState = SUBSTR(m.CSZ,nCommaPos+2,2)
					cZip = TRIM(SUBSTR(m.CSZ,nLastSpace+1))
				ELSE
					WAIT CLEAR
					WAIT WINDOW "NOT ALPHA: "+SUBSTR(TRIM(m.CSZ),AT(" ",m.CSZ,nSpaces-1)+1,2) TIMEOUT 3
					cCity = LEFT(TRIM(m.CSZ),AT(",",m.CSZ)-1)
					cState = SUBSTR(TRIM(m.CSZ),AT(" ",m.CSZ,nSpaces-2)+1,2)
					cZip = TRIM(SUBSTR(m.CSZ,nLastSpace+1))
				ENDIF
			ENDIF
		ENDIF

		nCtnNumber = 1  && Begin sequence count

		DO num_incr_st
		WAIT CLEAR
		WAIT WINDOW "Now creating Line Item information" NOWAIT NOCLEAR
		IF !lTesting
			INSERT INTO tempnan945 (accountid,isa_num,st_num,wo_num,bol_no,ship_ref,filename,filedate) ;
				VALUES (m.accountid,c_CntrlNum,c_GrpCntrlNum,m.wo_num,m.bol_no,m.ship_ref,cFileNameShort,dt2)
*!*				INSERT INTO ackdata (groupnum, isanum,  transnum,  edicode,accountid,loaddt,loadtime,filename,ship_ref) VALUES ;
*!*			                                     (c_CntrlNum,cISA_Num,PADL(c_GrpCntrlNum,9,"0") ,"SW",m.accountid,DATE(),DATETIME(),cfilenameshort,m.ship_ref)
		ENDIF

		STORE "ST"+cfd+"945"+cfd+PADL(c_GrpCntrlNum,9,"0")+csegd TO cString
		DO cstringbreak
		nSTCount = nSTCount + 1
		nSegCtr = 1

		cShipmentID = RIGHT(TRIM(cBOL),10)
		STORE "W06"+cfd+"N"+cfd+cShip_refedi+cfd+cdate+cfd+TRIM(cShipmentID)+cfd+TRIM(cWO_Num)+cfd+cPO_Num+csegd TO cString
		DO cstringbreak
		nSegCtr = nSegCtr + 1

		cStoreNum = segmentget(@apt,"STORENUM",alength)
		IF EMPTY(cStoreNum)
			cStoreNum = ALLTRIM(m.dcnum)
		ENDIF
		STORE "N1"+cfd+"ST"+cfd+ALLTRIM(m.consignee)+cfd+"92"+cfd+cStoreNum+csegd TO cString
		DO cstringbreak
		nSegCtr = nSegCtr + 1

		STORE "N9"+cfd+"BM"+cfd+TRIM(cBOL)+csegd TO cString
		DO cstringbreak
		nSegCtr = nSegCtr + 1

		cInvoice = IIF(EMPTY(ALLTRIM(m.keyrec)),ALLTRIM(m.ship_ref),ALLTRIM(m.keyrec))
		STORE "N9"+cfd+"CN"+cfd+TRIM(cInvoice)+csegd TO cString
		DO cstringbreak
		nSegCtr = nSegCtr + 1

		cMR = segmentget(@apt,"MR",alength)
		STORE "N9"+cfd+"MR"+cfd+TRIM(cMR)+csegd TO cString
		DO cstringbreak
		nSegCtr = nSegCtr + 1

		STORE "N9"+cfd+"DP"+cfd+TRIM(outship.dept)+csegd TO cString
		DO cstringbreak
		nSegCtr = nSegCtr + 1

		STORE "N9"+cfd+"IA"+cfd+TRIM(outship.vendor_num)+csegd TO cString
		DO cstringbreak
		nSegCtr = nSegCtr + 1

		ddel_date = outship.del_date
		IF EMPTY(ddel_date)
			IF !lTesting
				SELECT edi_trigger
				LOCATE
				LOCATE FOR INLIST(accountid,4610,4694) AND ship_ref = cShip_ref AND bol = cBOL AND edi_type = "945"
				IF FOUND()
					REPLACE fin_status WITH "MISSING DELDATE",processed WITH .T.,errorflag WITH .T.
					tsubject = "945 Error in NANJING/EA BOL "+TRIM(cBOL)+"(At PT "+cShip_ref+")"
					tattach = " "
					tsendto = tsendtoerr
					tcc = tccerr
					tmessage = "945 Processing for WO# "+cWO_Num+" produced this error: (Office: "+cOffice+"): "+cStatus
					tmessage = tmessage + CHR(13) + "This will not stop 945 processing..."
					DO FORM m:\dev\frm\dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
				ENDIF
				LOOP
			ELSE
				ddel_date = DATE()
			ENDIF
		ENDIF
		dapptnum = IIF(EMPTY(outship.appt_num),"UNK",outship.appt_num)
		dapptdate = outship.appt
		IF EMPTY(dapptdate)
			dapptdate = outship.del_date
		ENDIF

		STORE "N9"+cfd+"AO"+cfd+TRIM(dapptnum)+csegd TO cString
		DO cstringbreak
		nSegCtr = nSegCtr + 1

		cUseTime = "153000"

		STORE "G62"+cfd+"11"+cfd+DTOS(ddel_date)+cfd+"9"+cfd+cUseTime+csegd TO cString  && Ship date
		DO cstringbreak
		nSegCtr = nSegCtr + 1

		SELECT scacs
		IF !EMPTY(TRIM(outship.scac))
			STORE outship.scac TO m.scac
		ELSE
			IF UPPER(LEFT(outship.SHIP_VIA,3)) = "UPS"
				REPLACE outship.scac WITH "UPSN" IN outship
			ELSE
				cErrMsg = "MISSING SCAC"
				DO ediupdate WITH cErrMsg,.T.
				THROW
			ENDIF
		ENDIF

		lParcelType = IIF(SEEK(TRIM(outship.scac),'carriers','scac'),.T.,.F.)

		SELECT outship

		cShipType="J"
		STORE "W27"+cfd+cShipType+cfd+TRIM(m.scac)+cfd+TRIM(m.SHIP_VIA)+csegd TO cString
		DO cstringbreak
		nSegCtr = nSegCtr + 1

		IF lParcelType AND lRuralKing
			IF USED('SHIPMENT')
				USE IN shipment
			ENDIF

			csq1 = [select * from shipment where accountid = ]+TRANSFORM(nAcctnum)
			xsqlexec(csq1,,,"wh")
			INDEX ON shipmentid TAG shipmentid
			INDEX ON pickticket TAG pickticket

			IF USED('PACKAGE')
				USE IN package
			ENDIF

			csq1 = [select * from package where accountid = ]+TRANSFORM(nAcctnum)
			xsqlexec(csq1,,,"wh")
			INDEX ON shipmentid TAG shipmentid
			SET ORDER TO TAG shipmentid

			SELECT shipment
			IF SEEK(cShip_ref,"shipment","pickticket")
				nShipmentid = shipment.shipmentid
				SELECT package
				SUM package.pkgcharge TO nFreight FOR package.shipmentid=nShipmentid
				IF nFreight = 0
					WAIT WINDOW "Missing Rural King UPS Charges" TIMEOUT 2
					cErrMsg = "MISS UPS CHG"
					DO ediupdate WITH cErrMsg,.T.
					THROW
				ELSE
					cFreight = STRTRAN(ALLTRIM(STR(nFreight,10,2)),".","")
					cFreight = ALLTRIM(STR(nFreight,10,2))
				ENDIF
			ENDIF
			STORE "G72"+cfd+"503"+cfd+"CA"+cfd+cFreight+csegd TO cString
			DO cstringbreak
			nSegCtr = nSegCtr + 1
			cFreight = ""
			USE IN shipment
			USE IN package
		ENDIF

*************************************************************************
*2	OUTDET LOOP - MATCHING OUTSHIPID FIELD (FROM OUTSHIP)
*************************************************************************
		WAIT CLEAR
		WAIT WINDOW "Now creating Detail information "+cShip_ref NOWAIT NOCLEAR
		SELECT vnanjingpp
		SUM totqty TO nPPQty FOR ship_ref = TRIM(cShip_ref) AND outshipid = nOutshipID
		INSERT INTO qtycompare (wo_num,ship_ref,osqty,ppqty) VALUES (nWO_Num,cShip_ref,nOS_Qty,nPPQty)
		SELECT qtycompare
		GO BOTT
		nPctdiff = INT(ABS((osqty-ppqty)/osqty)*100)
		REPLACE pctdiff WITH nPctdiff
		STORE 0 TO nOS_Qty,nPPQty,nPctdiff
		SELECT vnanjingpp
		LOCATE
		LOCATE FOR ship_ref = TRIM(cShip_ref) AND outshipid = nOutshipID
		IF !FOUND()
			IF !lTesting
				SET STEP ON 
				WAIT WINDOW "PT "+cShip_ref+" NOT FOUND in VNANJINGPP...ABORTING" TIMEOUT 2
				IF !lTesting
					cErrMsg = "MISS PT: "+cShip_ref
					DO ediupdate WITH cErrMsg,.T.
				ENDIF
				=FCLOSE(nFilenum)
				ERASE &cFilenameHold
				THROW
			ELSE
				LOOP
			ENDIF
		ENDIF
*!* Added this code to trap unmatched OUTDETIDs in OUTDET, Joe 11.22.2005 (xfer here 11.01.2010)
		SELECT vnanjingpp
		LOCATE
		ASSERT .F. MESSAGE "Scanning VPP for missing ODIDs...>>DEBUG<<"
		SCAN FOR vnanjingpp.ship_ref = TRIM(cShip_ref) AND vnanjingpp.outshipid = nOutshipID AND vnanjingpp.totqty > 0
			IF EMPTY(outdet.outdetid)
				ASSERT .F. MESSAGE "IN MISSING ODID AREA"
				SET STEP ON
				WAIT WINDOW "OUTDETID "+TRANSFORM(vnanjingpp.outdetid)+" NOT FOUND IN OUTDET!...CLOSING" TIMEOUT 2
				cErrMsg = "MISS OD-ID: "+TRANSFORM(vnanjingpp.outdetid)
				DO ediupdate WITH cErrMsg,.T.
				=FCLOSE(nFilenum)
				ERASE &cFilenameHold
				THROW
			ENDIF
		ENDSCAN

		scanstr = "vnanjingpp.ship_ref = cShip_ref and vnanjingpp.outshipid = nOutshipid and vnanjingpp.totqty > 0"
		SELECT vnanjingpp
		LOCATE FOR &scanstr
		cCartonNum= "XXX"
		cUCC="XXX"

*!*			SCAN FOR &scanstr
		DO WHILE &scanstr
			lSkipBack = .T.
			IF vnanjingpp.totqty = 0
				SKIP 1 IN vnanjingpp
				LOOP
			ENDIF

			IF vnanjingpp.ucc = 'CUTS'
				SKIP 1 IN vnanjingpp
				LOOP
			ENDIF

			IF TRIM(vnanjingpp.ucc) <> cCartonNum
				STORE TRIM(vnanjingpp.ucc) TO cCartonNum
			ENDIF

*			FOR z = 1 TO outdet.totqty
			cColor = ""
			IF "ORIG_COLOR*"$outdet.printstuff
				valreturn("orig_color")
				STORE cOrig_color TO cColor
			ENDIF

			cSize = ""
			IF "ORIG_ID*"$outdet.printstuff
				valreturn("orig_id")
				STORE cOrig_id TO cSize
			ENDIF

			cStyle = ""
			IF "ORIG_STYLE*"$outdet.printstuff
				valreturn("orig_style")
				STORE cOrig_style TO cStyle
			ELSE
				IF EMPTY(cStyle)
					cStyle = TRIM(outdet.STYLE)
				ENDIF
			ENDIF

			cUPC = TRIM(outdet.upc)
			nLine = ATCLINE("PRINTSTYLE*",outdet.printstuff)
			cPrintStyle = MLINE(outdet.printstuff,nLine)
			cPrintStyle = TRIM(SUBSTR(TRIM(cPrintStyle),AT("*",cPrintStyle)+1))

			IF lNJPick
				nDetQty = 12
			ELSE
				nDetQty = 1
			ENDIF

			STORE "LB" TO cWeightUnit
			IF lTesting && AND EMPTY(outdet.ctnwt)
				cCtnWt = "5"
				nTotCtnWt = nTotCtnWt + 5
			ELSE
				STORE ALLTRIM(STR(outdet.ctnwt)) TO cCtnWt
				nTotCtnWt = nTotCtnWt + outdet.ctnwt
			ENDIF

			IF EMPTY(cCtnWt) OR outdet.ctnwt=0
				nCtnWt = outship.weight/outship.ctnqty
				cCtnWt = ALLTRIM(STR(nCtnWt))
				nTotCtnWt = nTotCtnWt + nCtnWt
				IF EMPTY(cCtnWt)
					DO ediupdate WITH "MISS CTNWT "+TRANSFORM(vnanjingpp.outdetid),.T.
					=FCLOSE(nFilenum)
					ERASE &cFilenameHold
					THROW
				ENDIF
			ENDIF

			STORE "LX"+cfd+ALLTRIM(STR(nCtnNumber))+csegd TO cString   && Carton Seq. # (up to Carton total)
			DO cstringbreak
			nSegCtr = nSegCtr + 1
			lDoManSegment = .T.

			cDoString = "vnanjingpp.ucc = cCartonNum"

			DO WHILE &cDoString
				IF lDoManSegment
					cUCC = TRIM(vnanjingpp.ucc)
					IF lDoManUC
						c214 = vnanjingpp.scc214
						c214 = TRIM(STRTRAN(TRIM(c214)," ",""))
						lDoManSegment = .F.
						STORE "MAN"+cfd+"UC"+cfd+TRIM(c214)+csegd TO cString  && UCC Number (Wal-mart DSDC only)
						DO cstringbreak
						nSegCtr = nSegCtr + 1
					ELSE
						IF !EMPTY(cUCC) AND cUCC # "XXX"
							lDoManSegment = .F.
							STORE "MAN"+cfd+"GM"+cfd+TRIM(cUCC)+csegd TO cString  && UCC Number (Wal-mart DSDC only)
							DO cstringbreak
							nSegCtr = nSegCtr + 1
						ELSE
							WAIT CLEAR
							WAIT WINDOW "Empty UCC Number in vNanjingPP "+cShip_ref TIMEOUT 2
							IF lTesting
								ASSERT .F. MESSAGE "EMPTY UCC...DEBUG"
								CLOSE DATABASES ALL
								=FCLOSE(nFilenum)
								ERASE &cFilenameHold
								THROW
							ELSE
								cErrMsg = "EMPTY UCC# in "+cShip_ref
								DO ediupdate WITH cErrMsg,.T.
								=FCLOSE(nFilenum)
								ERASE &cFilenameHold
								THROW
							ENDIF
						ENDIF
					ENDIF
				ENDIF

				nGetline = ATCLINE("UNITSTYPE",outdet.printstuff)
				dataline = MLINE(outdet.printstuff,nGetline)
				cUnitsType = SUBSTR(dataline,AT("*",dataline)+1)
				nGetline = ATCLINE("W0104",outdet.printstuff)
				dataline = MLINE(outdet.printstuff,nGetline)
				cW0104 = SUBSTR(dataline,AT("*",dataline)+1)
				nGetline = ATCLINE("W0106",outdet.printstuff)
				dataline = MLINE(outdet.printstuff,nGetline)
				cW0106 = SUBSTR(dataline,AT("*",dataline)+1)
				nGetline = ATCLINE("W0115",outdet.printstuff)
				dataline = MLINE(outdet.printstuff,nGetline)
				cW0115 = SUBSTR(dataline,AT("*",dataline)+1)
				cItemNum = outdet.custsku

				IF EMPTY(cUnitsType)
					cUnitsType = "EA"
				ENDIF

				IF EMPTY(cW0104)
					cW0104 = "IN"
				ENDIF

				IF EMPTY(cW0106)
					cW0106 = "UP"
				ENDIF

				IF EMPTY(cW0115)
					cW0115 = "VN"
				ENDIF

				nDetQty = INT(VAL(outdet.PACK))
				IF !outdet.units
					IF lNJPick
						nDetQty = 12
					ELSE
						nDetQty = 1
					ENDIF
					IF lTesting
						nTotCtnWt = outship.ctnqty*5
					ELSE
						nTotCtnWt = outship.weight
					ENDIF
				ENDIF

				IF lPrepack
					nDetQty = 1
				ENDIF

				IF EMPTY(cW0104)
					cW0104 = "IN"
				ENDIF

				IF EMPTY(cW0106)
					cW0106 = "UP"
				ENDIF

				IF EMPTY(cW0115)
					cW0115 = "VN"
				ENDIF

				IF nDetQty>0
					nUnitSum = nUnitSum + nDetQty
					STORE "W12"+cfd+"CL"+cfd+ALLTRIM(STR(nDetQty))+cfd+ALLTRIM(STR(nDetQty))+cfd+cfd+cUnitsType+cfd+cUPC+;
						cfd+cW0104+cfd+TRIM(cItemNum)+cfd+cfd+cCtnWt+cfd+cWeightUnit+REPLICATE(cfd,6)+cW0106+cfd+cUPC+;
						REPLICATE(cfd,3)+cW0115+cfd+cStyle+csegd TO cString  && Eaches/Style
					DO cstringbreak
					nSegCtr = nSegCtr + 1

					IF !EMPTY(cColor)
						IF INLIST(nWO_Num,258008,258009) AND cColor = "DARK"
							cColor = "DARKD"
						ENDIF
						STORE "N9"+cfd+"VCL"+cfd+cColor+csegd TO cString
						DO cstringbreak
						nSegCtr = nSegCtr + 1
					ENDIF

					IF !EMPTY(cSize)
						STORE "N9"+cfd+"VSZ"+cfd+cSize+csegd TO cString
						DO cstringbreak
						nSegCtr = nSegCtr + 1
					ENDIF

				ENDIF
				nCtnNumber = nCtnNumber + 1
*!*					ENDFOR
*!*			ENDSCAN
				SKIP 1 IN vnanjingpp
			ENDDO
			lSkipBack = .T.
		ENDDO

*************************************************************************
*2^	END OUTDET MATCHING OUTSHIPID LOOP
*************************************************************************
		WAIT CLEAR
		WAIT WINDOW "Now creating Section Closure information" NOWAIT NOCLEAR
		STORE "W03"+cfd+ALLTRIM(STR(nUnitSum))+cfd+ALLTRIM(STR(nTotCtnWt))+cfd+;
			cWeightUnit+csegd TO cString   && Units sum, Weight sum, carton count

		nTotCtnWt = 0
		nTotCtnCount = 0
		nUnitSum = 0
		FPUTS(nFilenum,cString)

		STORE  "SE"+cfd+ALLTRIM(STR(nSegCtr+2))+cfd+PADL(c_GrpCntrlNum,9,"0")+csegd TO cString
		FPUTS(nFilenum,cString)

		SELECT outship
		WAIT CLEAR
	ENDSCAN

*************************************************************************
*1^	END OUTSHIP MAIN LOOP
*************************************************************************

	DO close945
	=FCLOSE(nFilenum)
	IF !lTesting AND lUseTrigger
		SELECT edi_trigger
		nRec = RECNO()
		DO ediupdate WITH "945 CREATED",.F.
		SELECT edi_trigger
		LOCATE

		IF !USED("ftpedilog")
			SELECT 0
			USE F:\edirouting\ftpedilog ALIAS ftpedilog
			INSERT INTO ftpedilog (TRANSFER,ftpdate,filename,acct_name,TYPE) VALUES ("945-"+cCustname+"-"+cCustLoc,dt2,cFilenameHold,UPPER(cCustname),"945")
			USE IN ftpedilog
		ENDIF
	ENDIF

	WAIT CLEAR
	WAIT WINDOW cCustFolder+" 945 Process complete..." &cWinWait

*!* Create eMail confirmation message

	cOutFolder = "FMI"+cCustLoc

	cPtCount = ALLTRIM(STR(nPTCount))
	nPTCount = 0
	tsubject = cMailName+" 945 EDI File from TGF as of "+dtmail+" ("+cCustLoc+")"
	tattach = " "
	tmessage = "945 EDI Info from TGF for "+cDivision+", Std. BOL# "+TRIM(cBOL)+CHR(13)
	tmessage = tmessage + "(File: "+cFileNameShort+")"+CHR(13)
	tmessage = tmessage + "(WO# "+cWO_NumStr+"), containing these "+cPtCount+" picktickets:"+CHR(13)
	tmessage = tmessage + cPTString + CHR(13)
	tmessage = tmessage + +"has been created and will be transmitted ASAP."+CHR(13)+CHR(13)
	tmessage = tmessage + "If you have any questions, please eMail or call 732-750-9000 x217."
	tmessage = tmessage+CHR(13)+"Run from prog: "+cProgName


	IF lTestMail
		tsendto = tsendtotest
		tcc = tcctest
	ENDIF

	IF lEmail
		DO FORM m:\dev\frm\dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
	ENDIF


	RELEASE ALL LIKE c_CntrlNum,c_GrpCntrlNum
	WAIT CLEAR
	WAIT WINDOW cMailName+" 945 EDI File output complete"+CHR(13)+"for WO# "+TRIM(cWO_NumStr) TIMEOUT 1

*!* Transfers files to correct output folders

	IF lNanFilesOut
	COPY FILE [&cFilenameHold] TO [&cFilenameOut]
	DELETE FILE [&cFilenameHold]
	ENDIF
	IF !lTesting
	SELECT tempnan945
	COPY TO "f:\3pl\data\tempnan945a.dbf"
	USE IN tempnan945
	SELECT 0
	USE "f:\3pl\data\tempnan945a.dbf" ALIAS tempnan945a
	SELECT 0
	USE "f:\3pl\data\pts_sent945.dbf" ALIAS pts_sent945
	APPEND FROM "f:\3pl\data\tempnan945a.dbf"
	USE IN pts_sent945
	USE IN tempnan945a
	DELETE FILE "f:\3pl\data\tempnan945a.dbf"
	ENDIF

	IF !lTesting
		asn_out_data()
	ENDIF

CATCH TO oErr
	IF lDoCatch
		ASSERT .F. MESSAGE "IN CATCH SUBROUTINE...>>DEBUG<<"
		SET STEP ON
		IF EMPTY(cErrMsg)
			lEmail = .F.
			DO ediupdate WITH ALLTRIM(oErr.MESSAGE),.T.
			tsubject = cCustname+" Error ("+TRANSFORM(oErr.ERRORNO)+") at "+TTOC(DATETIME())
			tattach  = ""
			tsendto  = IIF(lTesting,tsendtotest,tsendtoerr)
			tcc = IIF(lTesting,tcctest,tccerr)

			tmessage = cCustname+" Error processing "+CHR(13)

			tmessage =tmessage+ "Try/CATCH Exeception Message:"+CHR(13)+;
				[  Error: ] + STR(oErr.ERRORNO) +CHR(13)+;
				[  LineNo: ] + STR(oErr.LINENO) +CHR(13)+;
				[  Message: ] + oErr.MESSAGE +CHR(13)+;
				[  Procedure: ] + oErr.PROCEDURE +CHR(13)+;
				[  Details: ] + oErr.DETAILS +CHR(13)+;
				[  StackLevel: ] + STR(oErr.STACKLEVEL) +CHR(13)+;
				[  LineContents: ] + oErr.LINECONTENTS+CHR(13)+;
				[  UserValue: ] + oErr.USERVALUE

			tsubject = "945 EDI Poller Error at "+TTOC(DATETIME())
			tattach  = ""
			tfrom = "Toll EDI Operations <toll-edi-ops@tollgroup.com>"
			DO FORM m:\dev\frm\dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
		ENDIF
	ENDIF
FINALLY
	SET DELETED ON
*	RELEASE ALL LIKE T*
ENDTRY

*** END OF CODE BODY

****************************
PROCEDURE close945
****************************
** Footer Creation

STORE  "GE"+cfd+ALLTRIM(STR(nSTCount))+cfd+c_CntrlNum+csegd TO cString
FPUTS(nFilenum,cString)

STORE  "IEA"+cfd+"1"+cfd+PADL(c_CntrlNum,9,"0")+csegd TO cString
FPUTS(nFilenum,cString)

RETURN

****************************
PROCEDURE num_incr_isa
****************************

IF !USED("serfile")
	USE ("f:\3pl\data\serial\"+cCustname+"945_serial") IN 0 ALIAS serfile
ENDIF
SELECT serfile
nISA_Num = serfile.seqnum
c_CntrlNum = ALLTRIM(STR(serfile.seqnum))
REPLACE serfile.seqnum WITH serfile.seqnum + 1 IN serfile
SELECT outship
RETURN

****************************
PROCEDURE num_incr_st
****************************
IF !USED("serfile")
	USE ("f:\3pl\data\serial\"+cCustname+"945_serial") IN 0 ALIAS serfile
ENDIF
SELECT serfile
c_GrpCntrlNum = ALLTRIM(STR(serfile.grpseqnum))
REPLACE serfile.grpseqnum WITH serfile.grpseqnum + 1 IN serfile
SELECT outship
RETURN


****************************
PROCEDURE segmentget
****************************
PARAMETER thisarray,lcKey,nLength

FOR i = 1 TO nLength
	IF i > nLength
		EXIT
	ENDIF
	lnEnd= AT("*",thisarray[i])
	IF lnEnd > 0
		lcThisKey =TRIM(SUBSTR(thisarray[i],1,lnEnd-1))
		IF OCCURS(lcKey,lcThisKey)>0
			RETURN SUBSTR(thisarray[i],lnEnd+1)
			i = 1
		ENDIF
	ENDIF
ENDFOR

RETURN ""

****************************
PROCEDURE ediupdate
****************************
PARAMETER cStatus,lIsError
lDoCatch = .F.

IF !lTesting AND lUseTrigger
	SELECT edi_trigger
	LOCATE
	nRec = RECNO()
	IF !lIsError
		REPLACE processed WITH .T.,proc945 WITH .T.,file945 WITH cFilenameHold,;
			fin_status WITH "945 CREATED",errorflag WITH .F.,when_proc WITH DATETIME()  ;
			FOR edi_trigger.bol = cBOL AND INLIST(accountid,4610,4694)
	ELSE
		REPLACE processed WITH .T.,proc945 WITH .F.,file945 WITH "",;
			fin_status WITH cStatus,errorflag WITH .T.  ;
			FOR edi_trigger.bol = cBOL AND INLIST(accountid,4610,4694)
		IF lCloseOutput
			=FCLOSE(nFilenum)
			ERASE &cFilenameHold
		ENDIF
	ENDIF
ENDIF

IF lIsError AND lEmail
	tsubject = "945 Error in NANJING/EA BOL "+TRIM(cBOL)+IIF(!lMismatch,"At PT "+cShip_ref+")","")
	tattach = " "
	tsendto = tsendtoerr
	tcc = tccerr
	tmessage = "Generated from program: nanjing945_create, within EDI Outbound Poller"
	tmessage = tmessage + CHR(13) + "945 Processing for BOL# "+cBOL+" produced this error: (Office: "+cOffice+"): "+cStatus+CHR(13)+"Check EDI_TRIGGER and re-run"

	IF TIME(DATE())>"20:00:00" OR INLIST(DOW(DATE()),1,7)
*			tcc = tcc+",jsb1956@yahoo.com"
	ENDIF

	DO FORM m:\dev\frm\dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
ENDIF

IF USED('outship')
	USE IN outship
ENDIF
IF USED('outdet')
	USE IN outdet
ENDIF
IF USED('serfile')
	USE IN serfile
ENDIF
IF USED('scacs')
	USE IN scacs
ENDIF
IF USED('mm')
	USE IN mm
ENDIF
IF !lTesting AND lUseTrigger
	SELECT edi_trigger
	LOCATE
ENDIF
ENDPROC

****************************
PROCEDURE cstringbreak
****************************
cLen = LEN(ALLTRIM(cString))

FPUTS(nFilenum,cString)
RETURN

**************************************************************************************
PROCEDURE scc14generate
**************************************************************************************
STORE "" TO cartonid,cSCCNumber,cCHKDigit
nSCCSuffix = nSCCSuffix + 1

cPkgIndicator = "1"  && Prepack -- PickPack  is 0
cSCCPrefix = cPkgIndicator+" "+PADL(LEFT(outdet.upc,1),2,"0") + SUBSTR(outdet.upc,2,5)

cSCCSuffix = PADL(ALLTRIM(STR(nSCCSuffix)),5,"0")
cSCCNumber2 = cSCCPrefix+" "+cSCCSuffix

nValue1 = 0  && "ODD" Digits
nValue2 = 0  && "EVEN" Digits
nValue3 = 0  && SUMMARY Value
nChkDigit = 0

cSCCNumber = STRTRAN(cSCCNumber2," ","")

FOR i = LEN(TRIM(cSCCNumber)) TO 1 STEP -2
	nValue1 = nValue1 + INT(VAL(SUBSTR(cSCCNumber,i,1)))
ENDFOR
nValue1 = nValue1 * 3

FOR i = LEN(TRIM(cSCCNumber))-1 TO 1 STEP -2
	nValue2 = nValue2 + INT(VAL(SUBSTR(cSCCNumber,i,1)))
ENDFOR
nValue3 = nValue1 + nValue2
nChkDigit = 10-(nValue3%10)  && <--- The output check digit here
IF nChkDigit = 10
	nChkDigit = 0
ENDIF

cCHKDigit = ALLTRIM(STR(nChkDigit))
cSCCNumber = cSCCNumber + cCHKDigit
cSCCNumber = TRIM(cSCCNumber)

ENDPROC

****************************
PROCEDURE valreturn
****************************
PARAMETERS cIdentifier
cReturned = "c"+cIdentifier
RELEASE ALL LIKE &cReturned
PUBLIC &cReturned
nGetline = ATCLINE(cIdentifier,outdet.printstuff)
dataline = MLINE(outdet.printstuff,nGetline)
STORE SUBSTR(dataline,AT("*",dataline)+1) TO &cReturned
RETURN &cReturned
ENDPROC

****************************
PROCEDURE totqtyerr
****************************
SET STEP ON
cWO_NumStr = IIF(EMPTY(cWO_NumStr),ALLTRIM(STR(outship.wo_num)),cWO_NumStr)
tsubject = "Cartons/Units Discrepancy, Outbound vs. Cartons Data"
tmessage = "BOL# "+cBOL+", WO#(s) "+cWO_NumStr+CHR(13)
tmessage = tmessage+"The following PT(s) had unmatched counts between the outbound ('O') and cartonization ('C') data:"+CHR(13)+CHR(13)
tmessage = tmessage+PADR("PT#",20)+PADR("O.Ctns",10)+PADR("C.Ctns",10)+PADR("O.Units",10)+PADR("C.Units",10)+CHR(13)
SELECT tempqtyerr
SCAN
	cSRUse = ALLTRIM(tempqtyerr.ship_ref)
	cnctnqty = STR(tempqtyerr.ctnqty)
	cncqty = STR(tempqtyerr.cqty)
	cntotqty = STR(tempqtyerr.totqty)
	cnuqty = STR(tempqtyerr.uqty)
	DO CASE
	CASE (ctnqty # cqty) AND (totqty # uqty)
		cErrorLine = "Both Carton and Unit totals don't match"
	CASE (ctnqty # cqty)
		cErrorLine = "Only Carton totals don't match"
	CASE (totqty # uqty)
		cErrorLine = "Only Unit totals don't match"
	ENDCASE
	tmessage = tmessage+PADR(cSRUse,20)+PADR(cnctnqty,10)+PADR(cncqty,10)+PADR(cntotqty,10)+PADR(cnuqty,10)+" "+cErrorLine+CHR(13)
ENDSCAN

tmessage = tmessage+CHR(13)+"Please contact Toll I.T./EDI IMMEDIATELY to confirm which numbers are correct."
tsendto = IIF(lTesting,tsendtotest,tsendtoqty)
tcc = IIF(lTesting,tcctest,tccqty)
tattach = ""
DO FORM m:\dev\frm\dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
USE IN tempqtyerr
ENDPROC

