utilsetup("DRIVERIDLE")

if holiday(date())
	return
endif

use f:\wo\wodata\daily in 0

xidlefile="f:\auto\xidle"+dtos(date())+".dbf"
if !file(xidlefile)
	create table &xidlefile (truck c(8), poscs c(23), zdatetime t)
else
	use (xidlefile) in 0
endif

create cursor xrpt (truck c(8), driver c(30), poscs c(23), since c(5), minutes n(6), latitude n(11,6), longitude n(11,6), message c(80))

xsqlexec("select truck_num from pos where zdate={"+dtoc(date())+"} and !empty(truck_num) and truck_num#'TURN'","xtemp",,"fx")
select truck_num from xtemp group by 1 into cursor xtemp1

select xtemp1
scan 
	xsqlexec("select *from pos where zdate={"+dtoc(date())+"} and truck_num='"+xtemp1.truck_num+"'","xtemp",,"fx")

	select ctot(dtoc(zdate)+" "+chr2time(ztime)) as zdatetime, ztime, ;
		round(longitude,3) as longitude3, round(latitude,3) as latitude3, ;
		placename, poscs, longitude, latitude, message ;
		from xtemp ;
		order by 1 descending ;
		into cursor xtemp2 readwrite
		
	if reccount("xtemp2")=0
		loop
	endif

	if !empty(xtemp2.placename)
		xsqlexec("select * from qcloc where placename='"+placename+"'","qcloc",,"fx")
		if reccount()=0
			email("Dyoung@fmiint.com","INFO: missing qcloc 4",placename,,,,.t.,,,,,.t.,,.t.)
		else
			if qcloc.loctype#"REST AREA"
				loop
			endif
		endif
	endif

	if datetime()-xtemp2.zdatetime>3600		&& QC stopped sending data
		loop
	endif
	
	xposcs=xtemp2.poscs
	xplacename=xtemp2.placename
	xtime=xtemp2.ztime
	xlong=xtemp2.longitude
	xlat=xtemp2.latitude
	xmessage=""
	
	select xtemp2
	go top 
	do while poscs=xposcs and placename=xplacename and abs(xtemp2.latitude3-xlat)+abs(xtemp2.longitude3-xlong)<.02
		xcurrtime=xtemp2.ztime
		if !empty(xtemp2.message)
			xmessage=xtemp2.message
		endif
		skip in xtemp2
	enddo

	if xcurrtime#xtime
		select daily
		locate for dis_truck=xtemp1.truck_num and dispatched=date() and !empty(dis_time) and ;
			(datetime() - ctot(dtoc(date())+" "+chr2time(dis_time)))/60 > 60
			
		if found()
			select (xidlefile)
			locate for truck=xtemp1.truck_num and poscs=xposcs
			if !found()
				m.minutes=(ctot(dtoc(date())+" "+chr2time(xtime)) - ctot(dtoc(date())+" "+chr2time(xcurrtime)))/60
				if m.minutes>=30
					m.truck=xtemp1.truck_num
					m.poscs=xposcs
					m.since=xcurrtime
					m.latitude=xlat
					m.longitude=xlong
					m.driver=daily.dis_driver
					m.message=xmessage
					insert into xrpt from memvar
					
					m.zdatetime=datetime()
					insert into (xidlefile) from memvar
				endif
			endif
		endif
	endif
endscan

select xrpt
delete for ", CA"$poscs
index on minutes tag minutes descending
count to aa
if aa>0
	copy to f:\auto\driveridle.xls xls

	tsendto="michael.divirgilio@tollgroup.com, dyoung@fmiint.com"
*	tsendto="dyoung@fmiint.com"
	tattach="f:\auto\driveridle.xls"
	tfrom="TGF <support@fmiint.com>"
	tmessage="These drivers have been idle for at least 30 minutes"
	tsubject="Drivers currently idle"
*	do form dartmail2 with tsendto,tfrom,tsubject," ",tattach,tmessage,"A"
endif

schedupdate()

_screen.Caption=gscreencaption
on error
