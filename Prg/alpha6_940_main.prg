*!* m:\dev\prg\alpha6_940_main.prg
*!* derived from m:\dev\prg\merkury940_main.prg, 01.03.2017, JOEB

PARAMETERS cOfficeIn

PUBLIC nAcctNum,cDelimiter,nRun_num,cTranslateOption,cWhichFile,cOffice,cCustname,cPropername,cStorenum,lOK,tsendtopt,tccpt
PUBLIC cFilter,nRunID,lComplete,lTesting,m.UCCNUMBER,lEmail,lTestImport,emailcommentstr,cUseFolder,lBrowfiles,cMod,cISA_Num
PUBLIC ptid,ptctr,ptqty,xfile,cfilename,nUploadCount,pickticket_num_start,pickticket_num_end,nFileCount,units,cUCCNumber
PUBLIC lcPath,lcArchivepath,tsendto,tcc,NormalExit,lTestmail,LogCommentStr,lPick,lPrepack,tsendtoerr,tccerr,lImported
PUBLIC m.addby,m.adddt,m.addproc,cTransfer,lHighline,tfrom,tattach,cISA_Num,thisfile,tsendtotest,tcctest,lLoadSQL,nFileSize
PUBLIC cErrMsg

PUBLIC ARRAY a856(1)
CLOSE DATABASES ALL
NormalExit = .F.

cErrMsg="None yet"
SET STEP ON 

TRY
	lTesting = .f.
	lTestImport = lTesting
	lTestmail = lTesting
	lOverridebusy = lTesting
	lBrowfiles = lTesting
	lOverridebusy = .f.
*	lBrowfiles = .t.
	_screen.WindowState = IIF(lTesting,2,1)
	lookups()

	DO m:\dev\prg\_setvars WITH lTesting

	IF VARTYPE(cOfficeIn) # "C"
		cOfficeIn = "N"
	ENDIF
	cCustname = "ALPHA6"
	nAcctNum  = 5726
	cMod = "I"
	gMasterOffice = cOfficeIn
	gOffice = cMod
	lLoadSQL = !lTesting
	STORE cOfficeIn TO cOffice

	IF !lTesting
		SELECT 0
		USE F:\edirouting\ftpsetup SHARED
		cTransfer = "940-ALPHA6"
		LOCATE FOR ftpsetup.transfer = cTransfer AND ftpsetup.TYPE = "GET"
		IF ftpsetup.chkbusy AND !lOverridebusy
			WAIT WINDOW "Process is flagged busy...returning" TIMEOUT 1
			NormalExit = .T.
			THROW
		ELSE
			REPLACE chkbusy WITH .T.,trig_time WITH DATETIME() ;
				FOR ftpsetup.TYPE = "GET" ;
				AND ftpsetup.transfer = cTransfer ;
				IN ftpsetup
			USE IN ftpsetup
		ENDIF
	ENDIF

	ASSERT .F. MESSAGE "At start of Alpha6 940 processing"

	lEmail = .T.
	LogCommentStr = ""

	tfrom = "Toll EDI Operations <toll-edi-ops@tollgroup.com>"
	STORE " " TO tattach,tsendto,tcc

	SELECT 0
	USE F:\3pl\DATA\mailmaster ALIAS mm SHARED
	SET STEP ON 
	LOCATE FOR mm.edi_type = "940" AND mm.accountid = nAcctNum AND mm.office = cOfficeIn
	IF FOUND()
		STORE TRIM(mm.acctname) TO cCustname
		STORE TRIM(mm.basepath) TO lcPath
		STORE TRIM(mm.archpath) TO lcArchivepath
		_SCREEN.CAPTION = ALLT(mm.scaption)
*!*			STORE mm.testflag 	   TO lTest
		STORE TRIM(IIF(mm.use_alt,mm.sendtoalt,mm.sendto)) TO tsendto
		STORE TRIM(IIF(mm.use_alt,mm.ccalt,mm.cc)) TO tcc
		LOCATE
		LOCATE FOR mm.edi_type = "MISC" AND mm.taskname = "JOETEST"
		STORE TRIM(IIF(mm.use_alt,mm.sendtoalt,mm.sendto)) TO tsendtotest
		STORE TRIM(IIF(mm.use_alt,mm.ccalt,mm.cc)) TO tcctest
		LOCATE
		LOCATE FOR mm.edi_type = "MISC" AND mm.taskname = "GENERAL"
		STORE TRIM(IIF(mm.use_alt,mm.sendtoalt,mm.sendto)) TO tsendtoerr
		STORE TRIM(IIF(mm.use_alt,mm.ccalt,mm.cc)) TO tccerr
		LOCATE 
		LOCATE FOR mm.edi_type = "MISC" AND mm.GROUP = "MERKURY" AND mm.taskname = "DUPPTNOTICE"
		STORE TRIM(IIF(mm.use_alt,mm.sendtoalt,mm.sendto)) TO tsendtopt
		STORE TRIM(IIF(mm.use_alt,mm.ccalt,mm.cc)) TO tccpt
		USE IN mm
	ELSE
		USE IN mm
		WAIT WINDOW AT 10,10  "No parameters set for this acct# "+ALLTRIM(STR(nAcctnum))+"  ---> Office "+cOffice TIMEOUT 2
		RELEASE ALL
		NormalExit = .F.
		THROW
	ENDIF

	lcPath = IIF(lTesting,STRTRAN(lcPath,"940IN","940INTEST"),lcPath)
	lcArchivepath = IIF(lTesting,STRTRAN(lcArchivepath,"940IN","940INTEST"),lcArchivepath)

	cUseName = "ALPHA6"
	cPropername = PROPER(cUseName)

	_SCREEN.CAPTION = cPropername+" 940 Process"
	CLEAR
	WAIT WINDOW "Now setting up "+cPropername+" 940 process..." TIMEOUT 2

	DO m:\dev\prg\createx856a
	SELECT x856

*!*		cDelimiter = "*"
*!*		cTranslateOption = "TILDE"
*!*		cfile = ""

	dXdate1 = ""
	dXxdate2 = DATE()
	nFileCount = 0

	nRun_num = 999
	lnRunID = nRun_num

	IF lTesting
		cUseFolder = "F:\WHP\WHDATA\"
	ENDIF

	DO ("m:\dev\PRG\ALPHA6_940_PROCESS")

	SELECT 0
	USE F:\edirouting\ftpsetup SHARED
	REPLACE chkbusy WITH .F. ;
		FOR ftpsetup.TYPE = "GET" ;
		AND ftpsetup.transfer = cTransfer ;
		IN ftpsetup
	NormalExit = .T.

CATCH TO oErr
	IF NormalExit = .F.
	SET STEP ON 
		ASSERT .F. MESSAGE "In Catch section..."
		tsubject = "Alpha6 940 Upload Error ("+TRANSFORM(oErr.ERRORNO)+") at "+TTOC(DATETIME())
		tsendto  = tsendtoerr
		tcc = tccerr
		tmessage = "Alpha6 940 Upload Error..... Please fix me........!"
		lcSourceMachine = SYS(0)
		lcSourceProgram = "from alpha6_940 group"

		tmessage =tmessage+ "Try/CATCH Exeception Message:"+CHR(13)+;
			[  Error: ] + STR(oErr.ERRORNO) +CHR(13)+;
			[  LineNo: ] + STR(oErr.LINENO) +CHR(13)+;
			[  Message: ] + oErr.MESSAGE +CHR(13)+;
			[  Procedure: ] + oErr.PROCEDURE +CHR(13)

		IF !EMPTY(cErrMsg)
			tmessage =tmessage+CHR(13)+"Error found: "+cErrMsg
		ELSE
			tmessage =tmessage+[  Details: ] + oErr.DETAILS +CHR(13)+;
				[  StackLevel: ] + STR(oErr.STACKLEVEL) +CHR(13)+;
				[  LineContents: ] + oErr.LINECONTENTS+CHR(13)+;
				[  UserValue: ] + oErr.USERVALUE+CHR(13)+;
				[  Computer:  ] +lcSourceMachine+CHR(13)+;
				[  940 file:  ] +xfile+CHR(13)+;
				[  Program:   ] +lcSourceProgram
		ENDIF

		DO FORM m:\dev\frm\dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
	ELSE
		WAIT WINDOW AT 10,10 "Normal Exit " TIMEOUT 2
	ENDIF
FINALLY
	ON ERROR
	SET STATUS BAR ON
	CLOSE DATABASES ALL
ENDTRY
