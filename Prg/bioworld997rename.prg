close data all

	SELECT 0
	USE F:\edirouting\FTPSETUP ALIAS FTPSETUP SHARED
	LOCATE FOR FTPSETUP.TRANSFER = "997-BIOWORLD-RENAME"
	IF FTPSETUP.chkbusy
		WAIT WINDOW 'Rename Process Is Busy...try again later' TIMEOUT 2
		CLOSE DATABASES ALL
		CANCEL
		RETURN
	ELSE
	REPLACE FTPSETUP.chkbusy WITH .T. FOR FTPSETUP.TRANSFER = "997-BIOWORLD-RENAME" IN ftpsetup
	endif


cInPath = "F:\FTPUSERS\Bioworld\997Out_hold\"
CD &cInPath
cOutPath = "F:\FTPUSERS\Bioworld\997Out\"

len1 = ADIR(ary1,"*.edi")

IF len1 < 1
	WAIT WINDOW "No files to process...exiting" TIMEOUT 2
	CLOSE DATA ALL
	RETURN
ENDIF

FOR i = 1 TO len1
	datestr = TTOC(DATETIME(),1)
	cFilenameIn = (cInPath+ALLTRIM(ary1[i,1]))
	cFilestem = "BW"+datestr+".edi"
	cFilenameX = (cInPath+cFileStem)
	cFilenameOut = (cOutPath+cFileStem)
	RENAME [&cFilenameIn] TO [&cFilenameX]
	COPY file [&cFilenameX] TO [&cFilenameOut]
	DELETE FILE [&cFilenameX]
	WAIT WINDOW "" TIMEOUT 2
ENDFOR

SELECT ftpsetup
REPLACE FTPSETUP.chkbusy WITH .f. FOR FTPSETUP.TRANSFER = "997-BIOWORLD-RENAME" IN ftpsetup

WAIT WINDOW "All 997 renaming complete" TIMEOUT 2
