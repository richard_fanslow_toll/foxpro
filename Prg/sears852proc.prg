
************************************************************************
* AUTHOR--:PG
* DATE----:05/01/2010
* Modified much, now this takes in flat files and sends out 852, product activity data for Sears
* Converted into Class format 8/30/10 MB
************************************************************************
*
* Build exe as F:\UTIL\SEARSEDI\SEARS852PROC.EXE
*
************************************************************************
IF _VFP.STARTMODE = 4 THEN
	SET RESOURCE OFF	
ENDIF


LOCAL loSears852Process, lnError, lcProcessName

utilsetup("SEARS852PROC")

DECLARE INTEGER CreateMutex IN Win32API ;
	STRING @lpMutexAttributes, ;
	STRING bInitialOwner, ;
	STRING @lpName

DECLARE INTEGER GetLastError IN Win32API

* if running in EXE check to see if already running
IF _VFP.STARTMODE > 0 THEN
	lcProcessName = "Sears852Process"
	CreateMutex(0,CHR(1),@lcProcessName)
	lnError = GetLastError()
	IF lnError = 183
		WAIT WINDOW TIMEOUT 5 "This program is already running..."
		schedupdate()
		RETURN .F.
	ENDIF
ENDIF

loSears852Process = CREATEOBJECT('Sears852Process')
loSears852Process.MAIN()

schedupdate()

CLOSE DATABASES ALL

WAIT WINDOW TIMEOUT 60

RETURN

#DEFINE LOGIT 1
#DEFINE WAITIT 2
#DEFINE NOWAITIT 4
#DEFINE SENDIT 8
#DEFINE CR CHR(13)
#DEFINE LF CHR(10)
#DEFINE CRLF CHR(13) + CHR(10)
#DEFINE RETURN_DATA_MANDATORY .F.
#DEFINE RETURN_DATA_NOT_MANDATORY .T.
#DEFINE CSVFORMAT 6
#DEFINE WRITE_ONLY_UNBUFFERED 11
#DEFINE COMMA ","

DEFINE CLASS Sears852Process AS CUSTOM

	cProcessName = 'Sears852Process'

	lTestMode = .F.  && .F. FOR NORMAL OPERATION, .T. TO SUPPRESS ALL BUT DEVELOPER EMAILS AND CHANGE INPUT TABLE, ETC.

	lAutoYield = .T.

	* date time props
	dToday = DATE()
	*dToday = {^2010-07-24}

	cStartTime = TTOC(DATETIME())

	* file/folder properties
	cInputFolder = 'F:\FTPUSERS\SEARSEDI\RGTIDATAIN\'
	cArchiveFolder = 'F:\FTPUSERS\SEARSEDI\RGTIDATAIN\ARCHIVE\'
	cOutputFolder = 'F:\FTPUSERS\SEARSEDI\EDIDATAOUT\ARCHIVE\'
	cAS2UploadFolder = 'F:\FTPUSERS\SEARSEDI\AS2UPLOAD\'
	cOutputFile = ''
	cAS2UploadFile = ''
	cOutputFileRoot = ''
	cCurrentRootFileName = ''

	* processing properties
	lProductionMode = .T.
	lArchive = .T.
	lWeCalcSeqNums = .F.
	cErrors = ''
	cBreakExpression = ''
	cPrevExpression = ''
	nFatalErrorCount = 0
	cISAList = ''

	* edi properties
	*cSegterminator = "~"+CHR(13)
	cSegterminator = CHR(13)  && the tilde should not appear throughout the file, only once at top, per SHC 11/23/10.
	n852FileHandle = 0
	n852Ctr = 0
	nSTNum = 0
	nInterctrlNum = 0
	cInterctrlNum = ''
	nCTTcount = 0
	nHLSegcnt = 0
	nSegcnt = 0
	nLincnt = 0
	nTotlincnt = 0
	nHLctr = 0  &&  top level counter for unique HL segment counts
	cWorkordersprocessed = ''
	cSummaryinfo = ''
	cC_cnum = ''

	* header props
	cTime = ''
	cTday = ''
	cTmon = ''
	cTcent = ''
	cTyr = ''
	cTwholeyr = ''
	cD4 = ''
	cD1 = ''
	cD2 = ''
	cD3 = ''
	cTsendid = ''
	cTsendcode = ''
	cTqualcode = ''
	cTqualcodePrefix = ''

	* use these to store overall counters
	nShipmentlevelcount = 0
	nOrderlevelcount = 0
	nPacklevelcount = 0
	nItemlevelcount = 0

	* use these to store overall counters
	nThisShipmentlevelcount = 0
	nThisorderlevelcount = 0
	nThispacklevelcount = 0
	nThisitemlevelcount = 0

	* used to store the value of the current count for remembering the Parent HL Count
	nCurrentShipmentlevelcount = 0
	nCurrentorderlevelcount = 0
	nCurrentPacklevelcount = 0
	nCurrentitemlevelcount = 0


	* wait window properties
	nWaitWindowTimeout = 2
	lWaitWindowIsOn = .T.

	* logfile properties
	lLoggingIsOn = .T.
	cLogFile = ''

	* INTERNAL email properties
	lSendInternalEmailIsOn = .T.
	cFrom = 'TGF Corporate Communication Department <transload-ops@fmiint.com>'
	cSendTo = 'dougwachs@fmiint.com'
	cCC = 'pgaidis@fmiint.com, mbennett@fmiint.com, dbasiaga@fmiint.com'
	cSubject = 'Sears EDI 852 Process Results for ' + TTOC(DATETIME())
	cAttach = ''
	cBodyText = ''

	cCOMPUTERNAME = ''
	cUSERNAME = ''


	FUNCTION INIT
		IF NOT DODEFAULT()
			RETURN .F.
		ENDIF
		WITH THIS
			IF _VFP.STARTMODE = 4 THEN
				SET RESOURCE OFF	
			ENDIF
			CLOSE DATA
			DO setenvi
			SET CENTURY ON
			SET DATE TO ymd
			SET DECIMAL TO 0
			SET TALK OFF
			SET DELETED ON
			SET CONSOLE OFF
			SET EXCLUSIVE OFF
			SET SAFETY OFF
			SET EXACT OFF
			SET STATUS BAR ON
			SET SYSMENU OFF
			SET ENGINEBEHAVIOR 70
			_VFP.AUTOYIELD = .lAutoYield
			.cCOMPUTERNAME = GETENV("COMPUTERNAME")
			.cUSERNAME = GETENV("USERNAME")
			.cLogFile = 'F:\FTPUSERS\SEARSEDI\LOGFILES\Sears852Process_LOG_' + .cCOMPUTERNAME + '.txt'
			IF .lTestMode THEN
				.cInputFolder = "C:\SEARSEDI\RGTIDATAIN\"
				.cArchiveFolder = "C:\SEARSEDI\RGTIDATAIN\ARCHIVE\"
				.cOutputFolder = "C:\SEARSEDI\EDIDATAOUT\ARCHIVE\"
				.cAS2UploadFolder = 'C:\SEARSEDI\AS2UPLOAD\'
				.cLogFile = 'F:\FTPUSERS\SEARSEDI\LOGFILES\Sears852Process_LOG_' + .cCOMPUTERNAME + '_TESTMODE.txt'
				.cSendTo = 'mbennett@fmiint.com'
				.cCC = ''
			ENDIF
			.lLoggingIsOn = .lLoggingIsOn AND NOT EMPTY(.cLogFile)
		ENDWITH
	ENDFUNC


	FUNCTION DESTROY
		WITH THIS
			IF .lLoggingIsOn  THEN
				SET ALTERNATE OFF
				SET ALTERNATE TO
			ENDIF
		ENDWITH
		DODEFAULT()
	ENDFUNC


	FUNCTION MAIN
		WITH THIS

			LOCAL lnNumberOfErrors, lnStrLen, lnLineNumber, lnCurrentFile, lcError
			LOCAL jtimec, j1, j2, j3, hdate, lcFile, lcArchiveFile, lcRenamedFile, lcStr, lnGoodTranstypes
			LOCAL laFiles[1,5], laFilesSorted[1,6], llProcessLine, lcMarkFor
			LOCAL lnNumFiles, lnNumXXXFiles, lcXXXFileName

			TRY

				lnNumberOfErrors = 0

				IF .lLoggingIsOn THEN
					SET ALTERNATE TO (.cLogFile) ADDITIVE
					SET ALTERNATE ON
				ENDIF
				
				IF NOT .lTestMode THEN
*!*						IF DATE() < {^2015-04-08} THEN   && change this to correct date when advised by SHC
*!*							* keep using current output channel
*!*					 		.cAS2UploadFolder = 'F:\FTPUSERS\SEARSEDI\AS2UPLOAD\'
*!*					 	ELSE

			 		* PER SHC, FROM 4/8/15 ON, USE THE NEW COMM CHANNEL
			 		.cAS2UploadFolder = 'F:\FTPUSERS\SEARSEDI-SI\AS2UPLOAD\'
					.TrackProgress('OUTPUT MODE = SI', LOGIT+SENDIT)
					
*!*					 	ENDIF
				ENDIF

				.TrackProgress('Sears 852 Process process started....', LOGIT+SENDIT+NOWAITIT)
				.TrackProgress('COMPUTERNAME = ' + .cCOMPUTERNAME, LOGIT+SENDIT)
				.TrackProgress('USERNAME = ' + .cUSERNAME, LOGIT+SENDIT)
				*.TrackProgress('_VFP.STARTMODE = ' + TRANSFORM(_VFP.STARTMODE), LOGIT+SENDIT)
				IF .lProductionMode THEN
					.TrackProgress('!! PRODUCTION MODE !!', LOGIT+SENDIT)
				ENDIF
				IF .lTestMode THEN
					.TrackProgress('TEST MODE', LOGIT+SENDIT)
				ENDIF
				.TrackProgress('PROJECT = SEARS852PROC', LOGIT+SENDIT)
				.TrackProgress('.cAS2UploadFolder = ' + .cAS2UploadFolder, LOGIT+SENDIT)

				.nLincnt = 1

				************************************
				*** Set Header Record properties ***
				************************************
				jtimec = TIME()
				STORE SUBSTR(jtimec,1,2) TO j1
				STORE SUBSTR(jtimec,4,2) TO j2
				STORE SUBSTR(jtimec,7,2) TO j3
				.cTime = j1+j2
				*!*					.cTday = DAY(DATE())
				*!*					STORE STR(.cTday,2,0)     TO .cTday1
				STORE DTOC(DATE())      TO hdate
				STORE SUBSTR(hdate,6,2) TO .cTmon
				STORE SUBSTR(hdate,9,2) TO .cTday
				STORE SUBSTR(hdate,1,2) TO .cTcent
				STORE SUBSTR(hdate,3,2) TO .cTyr
				STORE SUBSTR(hdate,1,4) TO .cTwholeyr
				STORE SUBSTR(hdate,1,2) TO .cD4
				STORE SUBSTR(hdate,3,2) TO .cD1
				STORE SUBSTR(hdate,6,2) TO .cD2
				STORE SUBSTR(hdate,9,2) TO .cD3

				.cTsendid   = 'FMI'
				*.cTsendid    = 'LOGNET'
				.cTsendid    = PADR(.cTsendid,12," ")

				IF .lProductionMode THEN
				
					*.cTsendcode  = '6111250084'  && ONLY FOR PRODUCTION !!!!!
					.cTsendcode  = '6111250084FMI'  && Per Craig Agams - send to this different ID 11/12/12 MB   && ONLY FOR PRODUCTION !!!!!
					
					.cTqualcodePrefix = '*08*'   && ONLY FOR PRODUCTION !!!!!
				ELSE
					.cTsendcode  = '6111250080'  && FOR TEST
					.cTqualcodePrefix = '*12*'  && FOR TEST
				ENDIF


				*.cTqualcode = '6111250084'
				.cTqualcode  = PADR(.cTsendcode,15," ")

				WAIT WINDOW "Open tables and setting up........." NOWAIT
				*******************************************************************************************
				USE F:\searsedi\DATA\controlnum IN 0 ALIAS controlnum

				********************************************************************************************************
				**********************************************
				*** Build Hierarchical Level *** SHIPMENT LEVEL
				***********************************************
				WAIT WINDOW "Setting up the cursors......." NOWAIT

				CD (.cInputFolder)
				
				* per Doug 7/24/11 -- if we find an xxx852*.txt file (which is the result of a former error)
				* do not process any files, even if newer unprocessed files may also be present.
				* The plan is for Doug to delete the xxx file when he drops a good replacement for it.
				
				* look for files that were renamed due to bad/missing data
				lnNumXXXFiles = ADIR(laFiles,"xxx852*.txt")
				
				IF lnNumXXXFiles > 0 THEN
				
					* a bad file that previously errored still exists in the input folder -- log the error				
					lcXXXFileName = laFiles[1,1]
					.TrackProgress('Not processing; found error file: ' + lcXXXFileName,LOGIT+SENDIT)
					
				ELSE
					* okay to continue processing -- look for input files....
					lnNumFiles = ADIR(laFiles,"852*.txt")

					IF lnNumFiles > 0 THEN

						*!*	* sort file list by date/time
						*!*	.SortArrayByDateTime(@laFiles, @laFilesSorted)

						* NOW sort file list by FILENAME -- which has the date/time encoded like this: 852_201103262302.txt
						* necessary if a file is redropped by Doug due to errors, so that we still process the earliest file, if multiple are present.
						=ASORT(laFiles, 1)

						* 11/22/10 per Doug: each input file must produce 1 output file.
						FOR lnCurrentFile = 1 TO lnNumFiles

							IF USED('CURSHIPDATAPRE') THEN
								USE IN CURSHIPDATAPRE
							ENDIF

							CREATE CURSOR CURSHIPDATAPRE (;
								recnum INT, ;
								bol CHAR(16),;
								TYPE CHAR(3),;
								actcode CHAR(2),;
								duns CHAR(16),;
								rptloc CHAR(10),;
								seqnum CHAR(8),;
								CONTAINER CHAR(12),;
								ponum CHAR(12),;
								division CHAR(3),;
								markfor CHAR(10),;
								vendorid CHAR(9),;
								scac CHAR(4),;
								qty N(7),;
								ITEM  CHAR(20),;
								sku CHAR(20))

							*!*							lcFile = .cInputFolder + laFilesSorted[lnCurrentFile,1]
							*!*							lcArchiveFile = .cArchiveFolder + laFilesSorted[lnCurrentFile,1]
							.cCurrentRootFileName = laFiles[lnCurrentFile,1]
							lcFile = .cInputFolder + .cCurrentRootFileName
							lcRenamedFile = .cInputFolder + "xxx" + .cCurrentRootFileName
							lcArchiveFile = .cArchiveFolder + laFiles[lnCurrentFile,1]

							.TrackProgress("Importing file: " + lcFile,LOGIT+SENDIT+NOWAITIT)

							lnHandle = FOPEN(lcFile)
							lnLineNumber = 0
							DO WHILE !FEOF(lnHandle)
								lcStr = FGETS(lnHandle)
								lnLineNumber = lnLineNumber + 1
								lnStrLen = LEN(lcStr)

								llProcessLine = .F.

								DO CASE
									CASE lnStrLen = 0
										.TrackProgress('Encountered Blank Line #: ' + TRANSFORM(lnLineNumber),LOGIT+SENDIT)
									CASE lnStrLen = 98  && this should be the normal case = LINE LENGTH OF CURRENT REC FORMAT
										*WAIT WINDOW NOWAIT 'Processing Line #: ' + TRANSFORM(lnLineNumber)
										WAIT WINDOW NOWAIT 'Processing File ' + TRANSFORM(lnCurrentFile) + ' of ' + TRANSFORM(lnNumFiles) + ', Line #: ' + TRANSFORM(lnLineNumber)
										llProcessLine = .T.
									OTHERWISE
										.nFatalErrorCount = .nFatalErrorCount + 1
										lcError = 'ERROR - Invalid Record Length: ' + TRANSFORM(lnStrLen) + ' for input file: ' + lcFile + ' in Line #: ' + TRANSFORM(lnLineNumber)
										.LOGERROR(lcError)
								ENDCASE

								IF llProcessLine THEN

									SELECT CURSHIPDATAPRE
									APPEND BLANK

									* strip leading zero from markfor, per Doug 3/10/11.
									lcMarkFor = ALLTRIM(SUBSTR(lcStr,61,10))
									IF LEFT(lcMarkFor,1) = "0" THEN
										* extract from one extra digit to the right to strip the leading zero
										lcMarkFor = ALLTRIM(SUBSTR(lcStr,62,9))
									ENDIF

									* increased Vendorid from 6 to 9 chars
									REPLACE CURSHIPDATAPRE.recnum WITH RECNO(), ;
										CURSHIPDATAPRE.duns  WITH "000561597", ;
										CURSHIPDATAPRE.rptloc    WITH "8110", ;
										CURSHIPDATAPRE.bol       WITH ALLTRIM(SUBSTR(lcStr,1,16)), ;
										CURSHIPDATAPRE.TYPE      WITH ALLTRIM(SUBSTR(lcStr,17,3)), ;
										CURSHIPDATAPRE.actcode   WITH ALLTRIM(SUBSTR(lcStr,20,2)), ;
										CURSHIPDATAPRE.seqnum    WITH ALLTRIM(SUBSTR(lcStr,22,8)), ;
										CURSHIPDATAPRE.CONTAINER WITH ALLTRIM(SUBSTR(lcStr,30,12)), ;
										CURSHIPDATAPRE.ponum     WITH PADL(ALLTRIM(SUBSTR(lcStr,42,16)),12,'0'), ;
										CURSHIPDATAPRE.division  WITH ALLTRIM(SUBSTR(lcStr,58,3)), ;
										CURSHIPDATAPRE.markfor   WITH lcMarkFor, ;
										CURSHIPDATAPRE.vendorid  WITH ALLTRIM(SUBSTR(lcStr,71,9)), ;
										CURSHIPDATAPRE.scac      WITH ALLTRIM(SUBSTR(lcStr,80,4)), ;
										CURSHIPDATAPRE.qty       WITH VAL(SUBSTR(lcStr,84,7)), ;
										CURSHIPDATAPRE.ITEM      WITH ALLTRIM(SUBSTR(lcStr,91,5)), ;
										CURSHIPDATAPRE.sku       WITH ALLTRIM(SUBSTR(lcStr,96,3))

									* validate the data just read

									* EDI Type
									IF NOT INLIST(CURSHIPDATAPRE.TYPE,'REC','DIS','INV','ADJ') THEN
										.nFatalErrorCount = .nFatalErrorCount + 1
										lcError = 'ERROR - Unexpected TYPE: ' + CURSHIPDATAPRE.TYPE + ' in Line #: ' + TRANSFORM(lnLineNumber)
										.LOGERROR(lcError)
									ENDIF

									* BOL
									IF EMPTY(CURSHIPDATAPRE.bol) AND (CURSHIPDATAPRE.TYPE = 'REC') THEN
										.nFatalErrorCount = .nFatalErrorCount + 1
										lcError = 'ERROR - Empty BOL for Type=REC in Line #: ' + TRANSFORM(lnLineNumber)
										.LOGERROR(lcError)
									ENDIF

									* Action Code
									IF NOT INLIST(CURSHIPDATAPRE.actcode,'QR','QW','QA','QC','QP','QH') THEN
										.nFatalErrorCount = .nFatalErrorCount + 1
										lcError = 'ERROR - Unexpected ACTION CODE: ' + CURSHIPDATAPRE.actcode + ' in Line #: ' + TRANSFORM(lnLineNumber)
										.LOGERROR(lcError)
									ENDIF

									* check for division in Sequence #
									IF VAL(SUBSTR(CURSHIPDATAPRE.seqnum,3,2)) = 0 THEN
										.nFatalErrorCount = .nFatalErrorCount + 1
										lcError = 'ERROR - Division not found in Sequence # in Line #: ' + TRANSFORM(lnLineNumber)
										.LOGERROR(lcError)
									ENDIF

									* Container
									IF EMPTY(CURSHIPDATAPRE.CONTAINER) AND (CURSHIPDATAPRE.TYPE = 'REC') THEN
										.nFatalErrorCount = .nFatalErrorCount + 1
										lcError = 'ERROR - Empty CONTAINER for Type=REC in Line #: ' + TRANSFORM(lnLineNumber)
										.LOGERROR(lcError)
									ENDIF

									* PO Number
									IF EMPTY(CURSHIPDATAPRE.ponum) AND INLIST(CURSHIPDATAPRE.TYPE,'REC','DIS') THEN
										.nFatalErrorCount = .nFatalErrorCount + 1
										lcError = 'ERROR - Empty PONUM for Type=REC/DIS in Line #: ' + TRANSFORM(lnLineNumber)
										.LOGERROR(lcError)
									ENDIF

									* division
									IF VAL(CURSHIPDATAPRE.division) = 0 THEN
										.nFatalErrorCount = .nFatalErrorCount + 1
										lcError = 'ERROR - Empty OR Zero Division in Line #: ' + TRANSFORM(lnLineNumber)
										.LOGERROR(lcError)
									ENDIF

									* Mark For
									IF (VAL(CURSHIPDATAPRE.markfor) = 0) AND (CURSHIPDATAPRE.TYPE = 'DIS') THEN
										.nFatalErrorCount = .nFatalErrorCount + 1
										lcError = 'ERROR - Empty MARKFOR for Type=DIS in Line #: ' + TRANSFORM(lnLineNumber)
										.LOGERROR(lcError)
									ENDIF

									* Vendor ID
									IF EMPTY(CURSHIPDATAPRE.vendorid) AND (CURSHIPDATAPRE.TYPE = 'REC') THEN
										.nFatalErrorCount = .nFatalErrorCount + 1
										lcError = 'ERROR - Empty VENDORID for Type=REC in Line #: ' + TRANSFORM(lnLineNumber)
										.LOGERROR(lcError)
									ENDIF

									* SCAC
									IF EMPTY(CURSHIPDATAPRE.scac) AND (CURSHIPDATAPRE.TYPE = 'REC') THEN
										.nFatalErrorCount = .nFatalErrorCount + 1
										lcError = 'ERROR - Empty SCAC for Type=REC in Line #: ' + TRANSFORM(lnLineNumber)
										.LOGERROR(lcError)
									ENDIF

									* Quantity
									IF (CURSHIPDATAPRE.qty = 0) AND (CURSHIPDATAPRE.TYPE <> 'INV') THEN
										.nFatalErrorCount = .nFatalErrorCount + 1
										lcError = 'ERROR - Zero QTY in Line #: ' + TRANSFORM(lnLineNumber)
										.LOGERROR(lcError)
									ENDIF

									* Item
									IF EMPTY(CURSHIPDATAPRE.ITEM) THEN
										.nFatalErrorCount = .nFatalErrorCount + 1
										lcError = 'ERROR - Empty ITEM in Line #: ' + TRANSFORM(lnLineNumber)
										.LOGERROR(lcError)
									ENDIF

									* SKU
									IF EMPTY(CURSHIPDATAPRE.sku) THEN
										*.nFatalErrorCount = .nFatalErrorCount + 1
										REPLACE CURSHIPDATAPRE.sku WITH '000'  && per Doug
										lcError = 'WARNING - Empty SKU (replaced it with 000) in Line #: ' + TRANSFORM(lnLineNumber)
										.LOGERROR(lcError)
									ENDIF

								ENDIF  &&  llProcessLine THEN

							ENDDO  &&  WHILE !FEOF(lnHandle)

							FCLOSE(lnHandle)

							IF .nFatalErrorCount > 0 THEN

								* rename the file that errored out so it is not reprocessed on next run
								* delete it if the rename exists already...

								IF FILE(lcRenamedFile) THEN
									DELETE FILE (lcRenamedFile)
								ENDIF

								RENAME (lcFile) TO (lcRenamedFile)
								.TrackProgress("Renamed " + lcFile + " to " + lcRenamedFile + " due to fatal errors!",LOGIT+SENDIT)
							ELSE
								&& here we should archive and delete the files
								IF .lArchive THEN
									*IF NOT FILE(lcArchiveFile) THEN
									COPY FILE (lcFile) TO (lcArchiveFile)
									*ENDIF
									IF FILE(lcArchiveFile) THEN
										DELETE FILE (lcFile)
									ELSE
										.LOGERROR('ERROR archiving ' + lcArchiveFile)
									ENDIF
								ENDIF
							ENDIF

							*!*						ENDFOR  && lnCurrentFile = 1  TO lnNumFiles

							*!*	SELECT CURSHIPDATAPRE
							*!*	BROWSE
							*!*	throw

							IF .nFatalErrorCount > 0 THEN
								* no further processing, email only to internal FMI
								lcError = 'THERE WERE ' + TRANSFORM(.nFatalErrorCount) + ' FATAL ERRORS!'
								.LOGERROR(lcError)
								lcError = 'Errors occurred on File ' + TRANSFORM(lnCurrentFile) + ' of ' + TRANSFORM(lnNumFiles)
								.LOGERROR(lcError)
								EXIT FOR
							ELSE
								* OKAY TO PROCEED

								SELECT CURSHIPDATAPRE
								COUNT FOR INLIST(TYPE,'REC','INV','ADJ','DIS') TO lnGoodTranstypes
								IF lnGoodTranstypes = 0 THEN
									.TrackProgress("No 'REC','INV','ADJ','DIS' transtypes were found!",LOGIT+SENDIT)
								ELSE

									.Start852()
									.n852Ctr =0

									************************************************************************
									* process for shipdata.TYPE = "REC"
									************************************************************************
									IF USED('shipdata') THEN
										USE IN shipdata
									ENDIF

									*.cBreakExpression = 'shipdata.division + shipdata.ponum'
									.cBreakExpression = 'shipdata.seqnum'

									*!*						* make sure to order by the break expression!
									*!*						SELECT * ;
									*!*							FROM CURSHIPDATAPRE ;
									*!*							INTO CURSOR shipdata ;
									*!*							WHERE TYPE = "REC" ;
									*!*							ORDER BY division, PONUM

									* sort by recnum to maintain original file order
									SELECT * ;
										FROM CURSHIPDATAPRE ;
										INTO CURSOR shipdata ;
										WHERE TYPE = "REC" ;
										ORDER BY recnum

									IF USED('shipdata') AND NOT EOF('shipdata') THEN
										.TrackProgress("Processing 'REC' transactions...",LOGIT+SENDIT)
										.Write852()
										.CloseEnvelope()
									ENDIF

									************************************************************************
									* process for shipdata.TYPE = "DIS"
									************************************************************************
									IF USED('shipdata') THEN
										USE IN shipdata
									ENDIF

									*.cBreakExpression = 'shipdata.division + shipdata.markfor'
									.cBreakExpression = 'shipdata.seqnum'

									*!*						* make sure to order by the break expression!
									*!*						SELECT * ;
									*!*							FROM CURSHIPDATAPRE ;
									*!*							INTO CURSOR shipdata ;
									*!*							WHERE TYPE = "DIS" ;
									*!*							ORDER BY division, MARKFOR

									* sort by recnum to maintain original file order
									SELECT * ;
										FROM CURSHIPDATAPRE ;
										INTO CURSOR shipdata ;
										WHERE TYPE = "DIS" ;
										ORDER BY recnum

									IF USED('shipdata') AND NOT EOF('shipdata') THEN
										.TrackProgress("Processing 'DIS' transactions...",LOGIT+SENDIT)
										.Write852()
										.CloseEnvelope()
									ENDIF

									************************************************************************
									* process for shipdata.TYPE = "INV"
									************************************************************************
									IF USED('shipdata') THEN
										USE IN shipdata
									ENDIF

									*.cBreakExpression = 'shipdata.division'
									.cBreakExpression = 'shipdata.seqnum'

									*!*						* make sure to order by the break expression!
									*!*						SELECT * ;
									*!*							FROM CURSHIPDATAPRE ;
									*!*							INTO CURSOR shipdata ;
									*!*							WHERE TYPE = "INV" ;
									*!*							ORDER BY division

									* sort by recnum to maintain original file order
									SELECT * ;
										FROM CURSHIPDATAPRE ;
										INTO CURSOR shipdata ;
										WHERE TYPE = "INV" ;
										ORDER BY recnum

									IF USED('shipdata') AND NOT EOF('shipdata') THEN
										.TrackProgress("Processing 'INV' transactions...",LOGIT+SENDIT)
										.Write852()
										.CloseEnvelope()
									ENDIF

									************************************************************************
									* process for shipdata.TYPE = "ADJ"
									************************************************************************
									IF USED('shipdata') THEN
										USE IN shipdata
									ENDIF

									*.cBreakExpression = 'shipdata.division'
									.cBreakExpression = 'shipdata.seqnum'

									*!*						* make sure to order by the break expression!
									*!*						SELECT * ;
									*!*							FROM CURSHIPDATAPRE ;
									*!*							INTO CURSOR shipdata ;
									*!*							WHERE TYPE = "ADJ" ;
									*!*							ORDER BY division

									* sort by recnum to maintain original file order
									SELECT * ;
										FROM CURSHIPDATAPRE ;
										INTO CURSOR shipdata ;
										WHERE TYPE = "ADJ" ;
										ORDER BY recnum

									IF USED('shipdata') AND NOT EOF('shipdata') THEN
										.TrackProgress("Processing 'ADJ' transactions...",LOGIT+SENDIT)
										.Write852()
										.CloseEnvelope()
									ENDIF

									******************************

									.CloseEDIFile()

									* the file has now been FCREATED in EDIDATAOUT\ARCHIVE\ -- copy to AS2UPLOAD folder
									COPY FILE (.cOutputFile) TO (.cAS2UploadFile)

									IF FILE(.cAS2UploadFile) THEN
										.TrackProgress("Copied " + .cOutputFile + " to " + CRLF + "AS2 Upload Folder: " + .cAS2UploadFile,LOGIT+SENDIT)
									ELSE
										.TrackProgress("Error Copying " + .cOutputFile + " to AS2 Upload Folder!",LOGIT+SENDIT)
									ENDIF

									*!*								IF .lTestMode THEN
									*!*									.ADDLINEFEEDS()
									*!*								ENDIF

								ENDIF && lnGoodTranstypes = 0

							ENDIF && .nFatalErrorCount > 0 THEN

						ENDFOR  && lnCurrentFile = 1  TO lnNumFiles

						.cSubject = 'Sears EDI 852 ISA List: ' + .cISAList + ' for ' + TTOC(DATETIME())
						.cBodyText = 'The following ISAs were sent in the 852(s): ' + .cISAList + CRLF + CRLF + .cBodyText

					ELSE

						.TrackProgress('No Sears 852 Flat Files to import!',LOGIT+SENDIT)
						.cSubject = "NO FILES FOUND - " + .cSubject
						
						* don't send internal email
						.lSendInternalEmailIsOn = .F.
						
					ENDIF && lnNumFiles > 0
						
				ENDIF && lnNumXXXFiles > 0
				
				
				* only send to Doug/Dennis if there was an error
				IF (.nFatalErrorCount > 0) AND (NOT .lTestMode) THEN
					.cSendTo = 'dougwachs@fmiint.com'
					.cCC = 'pgaidis@fmiint.com, mbennett@fmiint.com, dbasiaga@fmiint.com'
				ELSE
					.cSendTo = 'mbennett@fmiint.com'
					IF EMPTY(.cISAList) THEN
						.cCC = ''
					ELSE
						* sent ISA info to SHC
						.cCC = 'Craig.Adams@searshc.com'
						*.cCC = 'mark.bennett@tollgroup.com'
					ENDIF
				ENDIF && .nFatalErrorCount > 0 THEN


				.TrackProgress('Sears 852 Process process ended normally!',LOGIT+SENDIT)
				*.cBodyText = .cErrors + CRLF + .cSummaryinfo + CRLF + .cBodyText
				.cBodyText = .cErrors + CRLF + .cBodyText

			CATCH TO loError

				.TrackProgress('There was an error.',LOGIT+SENDIT)
				.TrackProgress('The Error # is: ' + TRANSFORM(loError.ERRORNO), LOGIT+SENDIT)
				.TrackProgress('The Error Message is: ' + TRANSFORM(loError.MESSAGE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred in Program: ' + TRANSFORM(loError.PROCEDURE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred at line #: ' + TRANSFORM(loError.LINENO), LOGIT+SENDIT)
				lnNumberOfErrors = lnNumberOfErrors + 1
				.lSendInternalEmailIsOn = .T.
				CLOSE DATA

			ENDTRY

			CLOSE DATA
			*CLOSE ALL
			WAIT CLEAR
			***************** INTERNAL email results ******************************
			.TrackProgress('About to send status email.',LOGIT)
			.TrackProgress('The logfile is: ' + .cLogFile, SENDIT)
			.TrackProgress('Sears 852 Process process started: ' + .cStartTime, LOGIT+SENDIT)
			.TrackProgress('Sears 852 Process process finished: ' + TTOC(DATETIME()), LOGIT+SENDIT)

			IF .lTestMode THEN
				.cSendTo = 'mbennett@fmiint.com'
				.cCC = ''
			ENDIF

			IF .lSendInternalEmailIsOn THEN
				DO FORM dartmail2 WITH .cSendTo,.cFrom,.cSubject,.cCC,.cAttach,.cBodyText,"A"
				.TrackProgress('Sent status email.',LOGIT)
			ELSE
				.TrackProgress('Did not send status email: THIS.lSendInternalEmailIsOn = FALSE.',LOGIT)
			ENDIF

		ENDWITH
		RETURN
	ENDFUNC && main


	PROCEDURE LOGERROR
		LPARAMETERS tcError
		WITH THIS
			.TrackProgress(tcError,LOGIT+SENDIT)
			.cErrors = .cErrors + 'File ' + .cCurrentRootFileName + ' - ' + ALLTRIM(tcError) + CRLF
		ENDWITH
	ENDPROC


	********************************************************************************************************************
	* 852 Top Level
	********************************************************************************************************************
	PROCEDURE Start852

		LOCAL lcOutputString, lcSegNum

		SELECT controlnum
		lcSegNum = ALLTRIM(STR(controlnum.fileseqnum))
		REPLACE controlnum.fileseqnum WITH controlnum.fileseqnum + 1

		lcSegNum =PADL(lcSegNum,8,"0")

		.cOutputFileRoot = "SEARSFMI_852_"+ALLTRIM(STR(YEAR(DATE())))+PADL(ALLTRIM(STR(MONTH(DATE()))),2,"0")+;
			PADL(ALLTRIM(STR(DAY(DATE()))),2,"0")+PADL(ALLTRIM(STR(HOUR(DATETIME()))),2,"0")+;
			PADL(ALLTRIM(STR(MINUTE(DATETIME()))),2,"0")+PADL(ALLTRIM(STR(SEC(DATETIME()))),2,"0")+"_"+lcSegNum+".EDI"

		* set up name of AS2UPLOAD file here, for later use...
		.cAS2UploadFile = .cAS2UploadFolder + .cOutputFileRoot

		.cOutputFile = .cOutputFolder + .cOutputFileRoot

		.n852FileHandle = FCREATE(.cOutputFile)
		IF .n852FileHandle < 0
			.TrackProgress('ERROR FCREATING Output File, .n852FileHandle = ' + TRANSFORM(.n852FileHandle),LOGIT+SENDIT)
			THROW
		ENDIF
		WAIT WINDOW "852 Processing for Sears EDI............" NOWAIT

		.WriteHeader()

	ENDPROC

	****************************************
	PROCEDURE Write852
		LOCAL lcOutputString, lcBreakExpression, lcCurrentExpression, llAlreadyWroteShipmentHeader
		LOCAL lcPrevousLINString

		*.Shipmentheader()   &&& CHANGE so that called when there is a proper break expression!!!!

		* REDUCE SO AS NOT TO COUNT THE 2 ISA AND GS SEGS AT TOP OF FILE
		*STORE 3 TO .nSegcnt
		STORE 1 TO .nSegcnt

		*!*			SELECT shipdata
		*!*			GOTO TOP
		*!*			.shipment_level()

		.cPrevExpression = '~XYZ~'  && THIS WILL FORCE AN IMMEDIATE .Shipmentheader() AT TOP OF SCAN
		lcPrevousLINString = '~XYZ~'

		llAlreadyWroteShipmentHeader = .F.

		SELECT shipdata
		SCAN

			lcBreakExpression = .cBreakExpression
			lcCurrentExpression = &lcBreakExpression

			IF NOT (.cPrevExpression == lcCurrentExpression) THEN

				IF llAlreadyWroteShipmentHeader THEN
					* write the trailer for the current transaction section before beginning a new transaction section
					.CloseEnvelope()
				ENDIF

				.Shipmentheader()  && write a new transaction header section

				llAlreadyWroteShipmentHeader = .T.  && set this flag, used above

				.shipment_level()  &&& moved to here so that it always occurs inside a transaction

			ENDIF


			*!*				.nCTTcount = .nCTTcount +1


			DO CASE
				CASE shipdata.TYPE = "REC"

					STORE "LIN**"+"IN*"+ALLTRIM(shipdata.ITEM)+"*IZ*"+ALLTRIM(shipdata.sku)+.cSegterminator TO lcOutputString
					.WriteString(.n852FileHandle,lcOutputString)
					.nSegcnt=.nSegcnt+1
					.nCTTcount = .nCTTcount +1

				CASE shipdata.TYPE = "DIS"

					STORE "LIN**"+"IN*"+ALLTRIM(shipdata.ITEM)+"*IZ*"+ALLTRIM(shipdata.sku)+.cSegterminator TO lcOutputString
					.WriteString(.n852FileHandle,lcOutputString)
					.nSegcnt=.nSegcnt+1
					.nCTTcount = .nCTTcount +1

				CASE shipdata.TYPE = "INV"

					STORE "LIN**"+"IN*"+ALLTRIM(shipdata.ITEM)+"*IZ*"+ALLTRIM(shipdata.sku)+.cSegterminator TO lcOutputString

					IF lcOutputString == lcPrevousLINString THEN
						* nothing - we don't repeat this segment, so that we can follow it with multiple QC/QA/QP ZA segments for the same seq#/sku.
					ELSE
						* it's a new LIN segment - write it out
						.WriteString(.n852FileHandle,lcOutputString)
						.nSegcnt=.nSegcnt+1
						.nCTTcount = .nCTTcount + 1
					ENDIF
					lcPrevousLINString = lcOutputString

				CASE shipdata.TYPE = "ADJ"

					STORE "LIN**"+"IN*"+ALLTRIM(shipdata.ITEM)+"*IZ*"+ALLTRIM(shipdata.sku)+.cSegterminator TO lcOutputString
					.WriteString(.n852FileHandle,lcOutputString)
					.nSegcnt=.nSegcnt+1
					.nCTTcount = .nCTTcount +1

				OTHERWISE
					* nothing

			ENDCASE

			STORE "ZA*" + shipdata.actcode + "*" + ALLTRIM(STR(shipdata.qty)) + "*EA" + .cSegterminator TO lcOutputString
			.WriteString(.n852FileHandle,lcOutputString)
			.nSegcnt=.nSegcnt+1

			.cPrevExpression = lcCurrentExpression

		ENDSCAN

		*!*			ENDIF &&.shipment_level()

	ENDPROC

	*******************************************************************************************************
	PROCEDURE CloseEnvelope
		LOCAL lcOutputString, lc852Ctr, lcSegCtr

		STORE "CTT*"+ALLTRIM(STR(.nCTTcount,6,0))+.cSegterminator TO lcOutputString
		.WriteString(.n852FileHandle,lcOutputString)
		*lcCtr = ALLTRIM(STR(.nTotlincnt))
		.nLincnt = 1
		STORE 0 TO .nCTTcount
		lc852Ctr = ALLTRIM(STR(.n852Ctr))
		lcSegCtr = ALLTRIM(STR(.nSegcnt+1))

		lcOutputString ="SE*"+lcSegCtr+"*"+.nSTNum+PADL(lc852Ctr,4,"0")+.cSegterminator
		.WriteString(.n852FileHandle,lcOutputString)

		STORE 1 TO .nSegcnt  && ADDED TO RESET THIS CTR WITHIN EACH TRANSACTION 10/14/10 MB

		********************************************************************************************************
	PROCEDURE CloseEDIFile
		LOCAL lcOutputString, lc852Ctr

		**process the next work order in the table
		*!*	  Select asnwo
		*!*	  Replace asnwo.processed With .T. In asnwo
		*!*	  Replace asnwo.Process   With .F. In asnwo
		*!*	  Replace asnwo.date_proc  With Datetime() In asnwo
*!*			.cSummaryinfo = ;
*!*				"-----------------------------------------------------------------------------"+CHR(13)+;
*!*				'852 file name....'+"---> "+.cC_cnum+CHR(13)+;
*!*				'   created.......'+DTOC(DATE())+CHR(13)+;
*!*				'   file.....: '+.cOutputFile+CHR(13)+;
*!*				'-----------------------------------------------------------------------------'+CHR(13)+;
*!*				'shipment level count.....'+STR(.nThisShipmentlevelcount)+CHR(13)+;
*!*				'   order level count.....'+STR(.nThisorderlevelcount)+CHR(13)+;
*!*				'    pack level count.....'+STR(.nThispacklevelcount)+CHR(13)+;
*!*				'    item level count.....'+STR(.nThisitemlevelcount)+CHR(13)+;
*!*				'-----------------------------------------------------------------------------'+CHR(13)

		.cSummaryinfo = ;
			"-----------------------------------------------------------------------------"+CHR(13)+;
			'852 file name....'+ .cOutputFile + CHR(13) + ;
			'   created.......' + DTOC(DATE()) + CHR(13) + ;
			'-----------------------------------------------------------------------------'+CHR(13)+;
			'shipment level count.....'+STR(.nThisShipmentlevelcount)+CHR(13)+;
			'   order level count.....'+STR(.nThisorderlevelcount)+CHR(13)+;
			'    pack level count.....'+STR(.nThispacklevelcount)+CHR(13)+;
			'    item level count.....'+STR(.nThisitemlevelcount)+CHR(13)+;
			'-----------------------------------------------------------------------------'+CHR(13)

		.cBodyText = .cBodyText + CRLF + .cSummaryinfo


		*  Replace asnwo.Info With .cSummaryinfo+.cWorkordersprocessed



		lc852Ctr = ALLTRIM(STR(.n852Ctr))
		STORE "GE*"+lc852Ctr+"*"+.nSTNum+.cSegterminator TO lcOutputString
		.WriteString(.n852FileHandle,lcOutputString)

		STORE "IEA*1*"+.cInterctrlNum+.cSegterminator TO lcOutputString
		.WriteString(.n852FileHandle,lcOutputString)

		=FCLOSE(.n852FileHandle)

		STORE "" TO .cWorkordersprocessed
		STORE 0 TO .nThisShipmentlevelcount,.nThisorderlevelcount,.nThispacklevelcount,.nThisitemlevelcount

		SET CENTURY OFF
		SET DATE TO american
		RETURN

	ENDPROC


	********************************************************************************************************************
	* S H I P M E N T     L E V E L
	********************************************************************************************************************
	**** REVISED 11/15/10 MB to have all N9s come before the N1s
	********************************************************************************************************************
	PROC shipment_level

		LOCAL lcOutputString, lc_hlcount, llOceanContainer

		* reset all at each shipment level
		STORE 0 TO .nShipmentlevelcount
		STORE 0 TO .nOrderlevelcount
		STORE 0 TO .nPacklevelcount
		STORE 0 TO .nItemlevelcount
		STORE 0 TO .nHLctr

		.nShipmentlevelcount = .nShipmentlevelcount +1
		.nThisShipmentlevelcount = .nThisShipmentlevelcount + 1
		.nHLctr = .nHLctr +1
		.nCurrentShipmentlevelcount = .nHLctr
		**.nCurrentShipmentlevelcount = .nShipmentlevelcount
		lc_hlcount = ALLTRIM(.getnum(.nHLctr))
		STORE "HL*"+lc_hlcount+"**S"+.cSegterminator TO lcOutputString
		*.WriteString(.n852FileHandle,lcOutputString)
		********************************************************************************************************************
		.nSegcnt=.nSegcnt+1
		.nHLSegcnt=.nHLSegcnt+1


		********************************************************************************************************************
		**** Meg says we need an extra segment if it's an Ocean Container...
		llOceanContainer = ( VAL(LEFT(ALLTRIM(shipdata.CONTAINER),4)) = 0 ) && val() will be 0 for container prefix like "MAEU"
		IF shipdata.TYPE = "REC" THEN
			IF llOceanContainer THEN
				STORE "N9*OC*"+ALLTRIM(shipdata.CONTAINER)+.cSegterminator TO lcOutputString
				.WriteString(.n852FileHandle,lcOutputString)
				.nSegcnt=.nSegcnt+1
			ENDIF  &&  llOceanContainer
		ENDIF  &&  shipdata.TYPE = "REC"
		********************************************************************************************************************


		STORE "N9*IA*"+ALLTRIM(shipdata.duns)+.cSegterminator TO lcOutputString
		.WriteString(.n852FileHandle,lcOutputString)
		.nSegcnt=.nSegcnt+1

		STORE "N9*DP*"+ALLTRIM(shipdata.division)+.cSegterminator TO lcOutputString
		.WriteString(.n852FileHandle,lcOutputString)
		.nSegcnt=.nSegcnt+1

		DO CASE

			CASE shipdata.TYPE = "REC"

				STORE "N9*PO*"+ALLTRIM(shipdata.ponum)+.cSegterminator TO lcOutputString
				.WriteString(.n852FileHandle,lcOutputString)
				.nSegcnt=.nSegcnt+1

				STORE "N9*BM*"+ALLTRIM(shipdata.bol)+.cSegterminator TO lcOutputString
				.WriteString(.n852FileHandle,lcOutputString)
				.nSegcnt=.nSegcnt+1

			CASE shipdata.TYPE = "DIS"

				STORE "N9*PO*"+ALLTRIM(shipdata.ponum)+.cSegterminator TO lcOutputString
				.WriteString(.n852FileHandle,lcOutputString)
				.nSegcnt=.nSegcnt+1

			OTHERWISE
				* nothing

		ENDCASE

		*STORE "N1*RL*"+ALLTRIM(shipdata.rptloc)+.cSegterminator TO lcOutputString
		STORE "N1*RL**92*"+ALLTRIM(shipdata.rptloc)+.cSegterminator TO lcOutputString
		.WriteString(.n852FileHandle,lcOutputString)
		.nSegcnt=.nSegcnt+1

		DO CASE

			CASE shipdata.TYPE = "REC"

				*STORE "N1*VN*"+ALLTRIM(shipdata.vendorid)+.cSegterminator TO lcOutputString
				*STORE "N1*VN**92*"+ALLTRIM(shipdata.vendorid)+.cSegterminator TO lcOutputString
				* must left pad VendorID with zeroes to 9 digits per Meg 12/2/10.
				STORE "N1*VN**92*" + PADL(ALLTRIM(shipdata.vendorid),9,'0') + .cSegterminator TO lcOutputString
				.WriteString(.n852FileHandle,lcOutputString)
				.nSegcnt=.nSegcnt+1

				STORE "TD5**2*"+ALLTRIM(shipdata.scac)+.cSegterminator TO lcOutputString
				.WriteString(.n852FileHandle,lcOutputString)
				.nSegcnt=.nSegcnt+1

			CASE shipdata.TYPE = "DIS"

				*STORE "N1*BY*"+ALLTRIM(shipdata.markfor)+.cSegterminator TO lcOutputString
				STORE "N1*BY**92*"+ALLTRIM(shipdata.markfor)+.cSegterminator TO lcOutputString
				.WriteString(.n852FileHandle,lcOutputString)
				.nSegcnt=.nSegcnt+1

			OTHERWISE
				* nothing

		ENDCASE

		RETURN
	ENDPROC &&.shipment_level


	********************************************************************************************************
	PROCEDURE Shipmentheader

		LOCAL lcOutputString, lcSegNum, lcSeqCtrlField

		.n852Ctr = .n852Ctr +1
		STORE "ST*852*"+.nSTNum+PADL(ALLTRIM(STR(.n852Ctr)),4,"0")+.cSegterminator TO lcOutputString
		.WriteString(.n852FileHandle,lcOutputString)

		SELECT controlnum
		
		* commented out because the value from controlnum.bsnseg is not used anywhere in the output file
		* 5/17/2011 MB
		*.cC_cnum = PADL(ALLTRIM(STR(controlnum.bsnseg)),5,"0")
		*REPLACE controlnum.bsnseg WITH controlnum.bsnseg + 1

		STORE "XQ*H*"+.cD4+.cD1+.cD2+.cD3+.cSegterminator TO lcOutputString
		.WriteString(.n852FileHandle,lcOutputString)
		.nSegcnt=.nSegcnt+1

		IF .lWeCalcSeqNums THEN
			* we calculate/track sequence numbers at our end
			*** this needs to change to be based on expressions specific to rectype,
			*** and to look at the division numeric counter fields in contolnum.

			lcSeqCtrlField = 'CTR' + PADL(shipdata.division,4,'0')
			SELECT controlnum
			lcSegNum = ALLTRIM(STR(controlnum.&lcSeqCtrlField))
			REPLACE controlnum.&lcSeqCtrlField WITH controlnum.&lcSeqCtrlField + 1
			.TrackProgress('IN Shipmentheader, lcSeqCtrlField =' + lcSeqCtrlField,LOGIT+SENDIT)
		ELSE
			* we are using the sequence numbers that Doug sends us
			lcSegNum = shipdata.seqnum
		ENDIF

		lcSegNum =PADL(lcSegNum,8,"0")
		*.TrackProgress('IN Shipmentheader, lcSegNum =' + lcSegNum,LOGIT+SENDIT)

		STORE "N9*DD*"+ALLTRIM(shipdata.TYPE)+lcSegNum+.cSegterminator TO lcOutputString
		.WriteString(.n852FileHandle,lcOutputString)
		.nSegcnt=.nSegcnt+1

		.nTotlincnt = 0
	ENDPROC &&.Shipmentheader


	********************************************************************************************************
	PROCEDURE WriteHeader

		LOCAL lcOutputString, lcProdMode

		* get the next unique control number
		SELECT controlnum
		.nSTNum = ALLTRIM(STR(controlnum.cntrl852))
		REPLACE controlnum.cntrl852 WITH controlnum.cntrl852 + 1 IN controlnum
		* MOVED THIS DOWN 2 LINES SO THAT IT GETS INCREMENTED BEFORE GETTING WRITTEN TO THE HEADER, TO BE CONSISTENT WITH THIS FIELDS USE IN OTHR PROGRAMS. MB 05/18/2015.
		*.nInterctrlNum = controlnum.interctrl
		REPLACE controlnum.interctrl WITH (controlnum.interctrl + 1) IN controlnum
		.nInterctrlNum = controlnum.interctrl
		.cInterctrlNum = PADL(ALLTRIM(STR(.nInterctrlNum)),9,"0")
		
		* added 09/19/12 to support emailing SHC the ISA#s
		.cISAList = .cISAList + IIF(EMPTY(.cISAList),.cInterctrlNum,", " + .cInterctrlNum)

		*lcProdMode = 'T'  && use this while testing
		lcProdMode = 'P'  && use this when in Production, which we are as per Meg 11/22/10.

		*STORE "ISA*00*          *00*          *ZZ*FMI            *12*"+.cTqualcode+"*"+.cTyr+.cTmon+.cTday+"*"+.cTime+"*U*00401*"+.cInterctrlNum +"*0*T* "+.cSegterminator TO lcOutputString
		*STORE "ISA*00*          *00*          *ZZ*FMI            *12*"+.cTqualcode+"*"+.cTyr+.cTmon+.cTday+"*"+.cTime+"*U*00401*"+.cInterctrlNum +"*0*" + lcProdMode + "*" + .cSegterminator TO lcOutputString
		*STORE "ISA*00*          *00*          *ZZ*FMI            *12*"+.cTqualcode+"*"+.cTyr+.cTmon+.cTday+"*"+.cTime+"*U*00401*"+.cInterctrlNum +"*0*" + lcProdMode + "*~" TO lcOutputString
		STORE "ISA*00*          *00*          *ZZ*FMI            " + .cTqualcodePrefix + .cTqualcode+"*"+.cTyr+.cTmon+.cTday+"*"+.cTime+"*U*00401*"+.cInterctrlNum +"*0*" + lcProdMode + "*~" + .cSegterminator TO lcOutputString
		.WriteString(.n852FileHandle,lcOutputString)

		*STORE "GS*PD*"+ALLTRIM(.cTsendid)+"*"+ALLTRIM(.cTsendcode)+"*"+.cTwholeyr+.cTmon+.cTday+"*"+.cTime+"*"+.nSTNum+"*X*004010"+.cSegterminator TO lcOutputString
		STORE "GS*PD*"+ALLTRIM(.cTsendid)+"*"+ALLTRIM(.cTsendcode)+"*"+.cTwholeyr+.cTmon+.cTday+"*"+.cTime+"*"+.nSTNum+"*X*004010"+.cSegterminator TO lcOutputString
		.WriteString(.n852FileHandle,lcOutputString)
	ENDPROC


	********************************************************************************************************
	FUNCTION getnum
		PARAM plcount
		fidnum = IIF(BETWEEN(plcount,1,9),SPACE(11)+LTRIM(STR(plcount)),;
			IIF(BETWEEN(plcount,10,99),SPACE(10)+LTRIM(STR(plcount)),;
			IIF(BETWEEN(plcount,100,999),SPACE(9)+LTRIM(STR(plcount)),;
			IIF(BETWEEN(plcount,1000,9999),SPACE(8)+LTRIM(STR(plcount)),;
			IIF(BETWEEN(plcount,10000,99999),SPACE(7)+LTRIM(STR(plcount))," ")))))
		RETURN fidnum
	ENDFUNC


	********************************************************************************************************
	PROC WriteString
		PARAM filehandle,filedata
		FWRITE(filehandle,filedata)
	ENDPROC


	********************************************************************************************************
	PROC CheckEmpty
		PARAMETER lcDataValue
		RETURN IIF(EMPTY(lcDataValue),"UNKNOWN",lcDataValue)
	ENDPROC


	********************************************************************************************************
	PROC RevertTables
		TABLEREVERT(.T.,"controlnum")
		TABLEREVERT(.T.,"asnwo")
		TABLEREVERT(.T.,"a852info")
		TABLEREVERT(.T.,"bkdnWO")
	ENDPROC
	********************************************************************************************************


	PROCEDURE TrackProgress
		* do any combination of Wait Window, writing to logfile, and adding to body of email,
		* based on nFlags parameter.
		LPARAMETERS tcExpression, tnFlags
		WITH THIS
			IF BITAND(tnFlags,LOGIT) = LOGIT THEN
				IF .lLoggingIsOn THEN
					?
					? .cProcessName + "  " + TTOC(DATETIME()) + ": " + tcExpression
				ENDIF
			ENDIF
			IF .lWaitWindowIsOn THEN
				IF BITAND(tnFlags,NOWAITIT) = NOWAITIT THEN
					WAIT WINDOW tcExpression NOWAIT
				ENDIF
				IF BITAND(tnFlags,WAITIT) = WAITIT THEN
					WAIT WINDOW tcExpression TIMEOUT .nWaitWindowTimeout
				ENDIF
			ENDIF
			IF BITAND(tnFlags,SENDIT) = SENDIT THEN
				*IF .lSendInternalEmailIsOn THEN
					.cBodyText = .cBodyText + tcExpression + CRLF + CRLF
				*ENDIF
			ENDIF
		ENDWITH
	ENDPROC  &&  TrackProgress


	PROCEDURE SortArrayByDateTime
		* expects taInArray[] to be the result of an ADIR()
		LPARAMETERS taInArray, taOutArray
		EXTERNAL ARRAY taInArray, taOutArray
		LOCAL lnRows, lnCols, i, j
		* extending taInArray[] to process the files in date/time order
		lnRows = ALEN(taInArray,1)
		lnCols = ALEN(taInArray,2)
		* create taOutArray with one more column
		DIMENSION taOutArray[lnRows,lnCols + 1]
		* and fill it with taInArray columns AND (filedate + filetime) in new column
		* ( ACOPY() doesn't preserve elements properly because of different # of columns )
		FOR i = 1 TO lnRows
			FOR j = 1 TO lnCols
				taOutArray[i,j] = taInArray[i,j]
			ENDFOR
			taOutArray[i,lnCols + 1] = DTOS(taInArray[i,3]) + "_" + taInArray[i,4]
		ENDFOR
		* sort taOutArray on date/time column
		=ASORT(taOutArray,6)
	ENDPROC


	PROCEDURE SortArrayByFileName
		* expects taInArray[] to be the result of an ADIR()
		LPARAMETERS taInArray, taOutArray
		EXTERNAL ARRAY taInArray, taOutArray
		LOCAL lnRows, lnCols, i, j
		* extending taInArray[] to process the files in date/time order
		lnRows = ALEN(taInArray,1)
		lnCols = ALEN(taInArray,2)
		* create taOutArray with one more column
		DIMENSION taOutArray[lnRows,lnCols + 1]
		* and fill it with taInArray columns AND (filedate + filetime) in new column
		* ( ACOPY() doesn't preserve elements properly because of different # of columns )
		FOR i = 1 TO lnRows
			FOR j = 1 TO lnCols
				taOutArray[i,j] = taInArray[i,j]
			ENDFOR
			taOutArray[i,lnCols + 1] = DTOS(taInArray[i,3]) + "_" + taInArray[i,4]
		ENDFOR
		* sort taOutArray on date/time column
		=ASORT(taOutArray,6)
	ENDPROC

ENDDEFINE


*!*		********************************************************************************************************************
*!*		* I T E M     L E V E L
*!*		********************************************************************************************************************
*!*		PROC item_level
*!*			LOCAL lcOutputString, lc_hlcount, lc_parentcount

*!*			.nLincnt = .nLincnt + 1
*!*			.nTotlincnt = .nTotlincnt + 1
*!*			.nHLctr = .nHLctr +1
*!*			.nItemlevelcount = .nItemlevelcount+1
*!*			.nThisitemlevelcount = .nThisitemlevelcount+1
*!*			.nCurrentitemlevelcount = .nHLctr
*!*			lc_hlcount = ALLTRIM(.getnum(.nHLctr)) 						&& overall HL counter
*!*			lc_parentcount = ALLTRIM(.getnum(.nCurrentorderlevelcount))	&& the parent

*!*			STORE "HL*"+lc_hlcount+"*"+lc_parentcount+"*I"+.cSegterminator TO lcOutputString
*!*			.WriteString(.n852FileHandle,lcOutputString)
*!*			.nSegcnt=.nSegcnt+1
*!*			.nHLSegcnt=.nHLSegcnt+1


*!*			IF EMPTY(shipdata.custsku) AND  EMPTY(shipdata.STYLE)
*!*				STORE "LIN**"+"UP*"+ALLTRIM(shipdata.upc)+.cSegterminator TO lcOutputString
*!*			ENDIF

*!*			IF EMPTY(shipdata.custsku) AND  !EMPTY(shipdata.STYLE)
*!*				STORE "LIN**"+"IN*"+ALLTRIM(shipdata.STYLE)+"*"+"UP*"+ALLTRIM(shipdata.upc)+.cSegterminator TO lcOutputString
*!*			ENDIF

*!*			.WriteString(.n852FileHandle,lcOutputString)
*!*			.nSegcnt=.nSegcnt+1

*!*			STORE "SN1**"+ALLTRIM(STR(shipdata.qty))+"*ST"+.cSegterminator TO lcOutputString
*!*			.WriteString(.n852FileHandle,lcOutputString)
*!*			.nSegcnt=.nSegcnt+1

*!*		ENDPROC &&.item_level

*!*		********************************************************************************************************************
*!*		* O R D E R    L E V E L
*!*		********************************************************************************************************************
*!*		PROC order_level

*!*			LOCAL lcOutputString, lc_hlcount, lc_parentcount

*!*			.nHLctr = .nHLctr +1
*!*			.nOrderlevelcount = .nOrderlevelcount+1
*!*			.nThisorderlevelcount = .nThisorderlevelcount+1
*!*			lc_hlcount = .getnum(.nHLctr)								&& overall HL counter
*!*			lc_parentcount = .getnum(.nCurrentShipmentlevelcount)				&& the parent
*!*			.nCurrentorderlevelcount = .nHLctr

*!*			STORE "HL*"+ALLTRIM(lc_hlcount)+"*"+ALLTRIM(lc_parentcount)+"*O"+.cSegterminator TO lcOutputString
*!*			.WriteString(.n852FileHandle,lcOutputString)
*!*			********************************************************************************************************************************
*!*			.nSegcnt=.nSegcnt+1
*!*			.nHLSegcnt=.nHLSegcnt+1

*!*			** PRF
*!*			STORE "PRF*"+ALLTRIM(shipdata.ponum)+.cSegterminator TO lcOutputString
*!*			.WriteString(.n852FileHandle,lcOutputString)
*!*			.nSegcnt=.nSegcnt+1

*!*			STORE "PID*S**VI*FL"+.cSegterminator TO lcOutputString
*!*			.WriteString(.n852FileHandle,lcOutputString)
*!*			.nSegcnt=.nSegcnt+1

*!*			STORE "TD1**"+ALLTRIM(STR(shipdata.ctnqty))+.cSegterminator TO lcOutputString
*!*			.WriteString(.n852FileHandle,lcOutputString)
*!*			.nSegcnt=.nSegcnt+1

*!*			STORE "REF*DP*"+ALLTRIM(shipdata.division)+.cSegterminator TO lcOutputString
*!*			.WriteString(.n852FileHandle,lcOutputString)
*!*			.nSegcnt=.nSegcnt+1

*!*			STORE "N1*Z7**92*"+ALLTRIM(shipdata.markfor)+.cSegterminator TO lcOutputString
*!*			.WriteString(.n852FileHandle,lcOutputString)
*!*			.nSegcnt=.nSegcnt+1

*!*		ENDPROC &&.order_level
