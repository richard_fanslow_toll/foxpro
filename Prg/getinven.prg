lparameters xprocess, xaccountid, xstyle, xcolor, xid, xpack

if !used("invref2")
	use f:\invref2 in 0
endif

if empty(xaccountid)
	xaccountid=0
endif
taccountid=xaccountid

if empty(xstyle)
	xstyle=""
	tstyle="%"
else
	tstyle=xstyle
endif
if empty(xcolor)
	xcolor=""
	tcolor="%"
else
	tcolor=xcolor
endif
if empty(xid)
	xid=""
	tid="%"
else
	tid=xid
endif
if empty(xpack)
	xpack=""
	tpack="%"
else
	tpack=xpack
endif

tspecfilter="%"

xinvrefstart=datetime()

store 0 to xinvenrecs, xinvenlocrecs

if used("inven")
	=tablerevert(.t.,"inven")
	requery("inven")
	xinvenrecs=reccount("inven")
endif
if used("invenloc")
	=tablerevert(.t.,"invenloc")
	requery("invenloc")
	xinvenlocrecs=reccount("invenloc")
endif

xinvrefend=datetime()

if xinvenrecs#0 or xinvenlocrecs#0
	insert into invref2 (process, mod, accountid, style, color, id, pack, inven, invenloc, userid, zstart, zend, zsec) ;
		values (xprocess, goffice, xaccountid, xstyle, xcolor, xid, xpack, xinvenrecs, xinvenlocrecs, guserid, xinvrefstart, xinvrefend, xinvrefend-xinvrefstart)
endif
