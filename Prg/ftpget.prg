* ftp_gtfiles converted into a simplified class, with Mark B. as error email recipient.
* Build EXE in F:\UTIL\FTPGET\
* Must be scheduled to run on FTP2, which has CuteFTP Pro installed.

LPARAMETERS tcWhichget

LOCAL loFTPGET, llTestMode

llTestMode = .F.

IF NOT llTestMode THEN
	utilsetup("FTPGET")
ENDIF


loFTPGET = CREATEOBJECT('FTPGET')
loFTPGET.MAIN( llTestMode, tcWhichget )


IF NOT llTestMode THEN
	schedupdate()
ENDIF

CLOSE DATABASES ALL

RETURN

#DEFINE LOGIT 1
#DEFINE WAITIT 2
#DEFINE NOWAITIT 4
#DEFINE SENDIT 8
#DEFINE CR CHR(13)
#DEFINE LF CHR(10)
#DEFINE CRLF CHR(13) + CHR(10)
#DEFINE RETURN_DATA_MANDATORY .F.
#DEFINE RETURN_DATA_NOT_MANDATORY .T.
#DEFINE CSVFORMAT 6
#DEFINE WRITE_ONLY_UNBUFFERED 11
#DEFINE COMMA ","

DEFINE CLASS FTPGET AS CUSTOM

	cProcessName = 'FTPGET'
	cProject = 'FTPGET'

	lTestMode = .T.  && .F. FOR NORMAL OPERATION, .T. TO SUPPRESS ALL BUT DEVELOPER EMAILS AND CHANGE INPUT TABLE, ETC.

	lAutoYield = .F.

	* date time props
	dToday = DATE()

	cStartTime = TTOC(DATETIME())

	* file/folder properties

	* data properties

	* Excel
	oExcel = NULL

	* wait window properties
	nWaitWindowTimeout = 2
	lWaitWindowIsOn = .T.

	* logfile properties
	lLoggingIsOn = .T.
	cLogFile = ''

	nSQLHandle = 0

	* INTERNAL email properties
	lSendInternalEmailIsOn = .T.
	cFrom = 'Mark.Bennett@Tollgroup.com'
	cSendTo = 'mbennett@fmiint.com'
	cCC = ''
	cSubject = 'FTP GET Process Results for ' + TTOC(DATETIME())
	cAttach = ''
	cBodyText = ''
	cTopBodyText = ''

	cCOMPUTERNAME = ''
	cUSERNAME = ''


	FUNCTION INIT
		IF NOT DODEFAULT()
			RETURN .F.
		ENDIF
		WITH THIS
			*SET RESOURCE OFF
			SET CENTURY ON
			SET DATE YMD
			SET HOURS TO 24
			CLOSE DATA
			SET DECIMAL TO 0
			SET TALK OFF
			SET DELETED ON
			SET CONSOLE OFF
			SET EXCLUSIVE OFF
			SET SAFETY OFF
			SET EXACT OFF
			SET STATUS BAR ON
			SET SYSMENU OFF
			SET ENGINEBEHAVIOR 70
			_VFP.AUTOYIELD = .lAutoYield
			.cCOMPUTERNAME = GETENV("COMPUTERNAME")
			.cUSERNAME = GETENV("USERNAME")
			IF .lTestMode THEN
				.cLogFile = 'F:\UTIL\FTPGET\LOGFILES\' + .cProject + '_LOG_TESTMODE.TXT'
			ELSE
				.cLogFile = 'F:\UTIL\FTPGET\LOGFILES\' + .cProject + '_LOG.TXT'
			ENDIF
			.lLoggingIsOn = .lLoggingIsOn AND NOT EMPTY(.cLogFile)
		ENDWITH
	ENDFUNC


	FUNCTION DESTROY
		WITH THIS
			IF .lLoggingIsOn  THEN
				SET ALTERNATE OFF
				SET ALTERNATE TO
			ENDIF
		ENDWITH
		DODEFAULT()
	ENDFUNC


	FUNCTION MAIN
		LPARAMETERS tlTestMode, tcWhichget
		WITH THIS

			.lTestMode = tlTestMode

			TRY
				LOCAL lnNumberOfErrors, lcDLFile, lcRootFilename, lcDLMessage, loCuteFTP, lcWhichget
				LOCAL lnResult, lnFHandle, lcString, lcFileMask, lcLeftFName, lcRightFName, lnMatchCount

				lcWhichget = tcWhichget

				lnNumberOfErrors = 0

				IF .lLoggingIsOn THEN
					SET ALTERNATE TO (.cLogFile) ADDITIVE
					SET ALTERNATE ON
				ENDIF

				.TrackProgress('FTP GET Process started....', LOGIT+SENDIT)
				.TrackProgress('WHICH GET = ' + lcWhichget, LOGIT+SENDIT)
				.TrackProgress('COMPUTERNAME = ' + .cCOMPUTERNAME, LOGIT+SENDIT)
				.TrackProgress('USERNAME = ' + .cUSERNAME, LOGIT+SENDIT)
				.TrackProgress('PROJECT = ' + .cProject, LOGIT+SENDIT)
				IF .lTestMode THEN
					.TrackProgress('TEST MODE', LOGIT+SENDIT)
				ENDIF
				
				.cSubject = 'FTP GET Process Results for ' + lcWhichget + ' at ' + TTOC(DATETIME())
				_SCREEN.CAPTION = 'FTP GET Process for ' + lcWhichget

				.TrackProgress("Prepping for Transfer...", LOGIT+SENDIT+NOWAITIT)

				CREATE CURSOR CURFILES(FNAME c(50))

				IF NOT USED("ftpsetup")
					USE F:\edirouting\ftpsetup IN 0
				ENDIF

				lcWhichfind = PADR(ALLTRIM(lcWhichget),20)
				SELECT ftpsetup
				LOCATE FOR ftpsetup.transfer = lcWhichfind
				IF NOT FOUND() THEN
					.TrackProgress("ERROR: Transfer " + lcWhichSite + " is not defined in f:\edirouting\ftpsetup.dbf.", LOGIT+SENDIT+NOWAITIT)
					THROW
				ENDIF

				lcLocalPath = ALLTRIM(ftpsetup.localpath)
				IF NOT DIRECTORY(lcLocalPath) THEN
					.TrackProgress("ERROR: local directory " + lcLocalPath + " does not exist.", LOGIT+SENDIT+NOWAITIT)
					THROW
				ENDIF

				SET DEFAULT TO (lcLocalPath)
				lcArchivePath      = ALLTRIM(ftpsetup.archive)
				
				* llCheckArchive & llAutoRename not currently used....
				**llCheckArchive: if need to check the archive to be sure same file was not downloaded and processed on a previous run - mvw 04/27/11
				*llCheckArchive       = IIF(ftpsetup.chkarchive AND !EMPTY(lcArchivePath),.T.,.F.)
				**llAutoRename: transfers as .tmp file first, then renames to correct file ext (to avoid incomplete file transfers) - mvw 04/27/11
				*llAutoRename	   = ftpsetup.autorename
				
				lcRemoteFolder     = ALLTRIM(ftpsetup.rfolder)
				lcAccountName      = ALLTRIM(ftpsetup.account)
				lcEditType          = ALLTRIM(ftpsetup.editype)
				lcHostSite         = ALLTRIM(ftpsetup.site)
				lcUsername        = ALLTRIM(ftpsetup.login)
				lcPassword     = ALLTRIM(ftpsetup.PASSWORD)
				lcFileMask = ALLTRIM(ftpsetup.filemask)

				*Creating a connection object and assign it to the variable
				loCuteFTP = CREATEOBJECT("CuteFTPPro.TEConnection")
				loCuteFTP.protocol = "FTP"
				loCuteFTP.HOST     = lcHostSite
				loCuteFTP.login    = lcUsername
				loCuteFTP.PASSWORD = lcPassword
				loCuteFTP.useproxy = "BOTH"

				loCuteFTP.CONNECT

				IF loCuteFTP.isconnected >= 0
					.TrackProgress("ERROR: Could not connect to: " + loCuteFTP.HOST, LOGIT+SENDIT+NOWAITIT)
					THROW
				ELSE
					.TrackProgress("Sucessfully connected to " + loCuteFTP.HOST, LOGIT+SENDIT+NOWAITIT)
				ENDIF

				IF EMPTY(lcRemoteFolder) THEN
					loCuteFTP.remotefolder = ""
					.TrackProgress("Logged into home folder", LOGIT+SENDIT+NOWAITIT)
				ELSE
					.TrackProgress("Changing to remote folder: [" + lcRemoteFolder + "]....", LOGIT+SENDIT+NOWAITIT)
					lnResult = loCuteFTP.RemoteExists(lcRemoteFolder)
					IF lnResult >= 0 THEN
						.TrackProgress("ERROR: could not find remote folder: [" + lcRemoteFolder + "]", LOGIT+SENDIT+NOWAITIT)
						THROW
					ELSE
						loCuteFTP.remotefolder = lcRemoteFolder
						.TrackProgress("Remote folder now = [" + lcRemoteFolder + "]", LOGIT+SENDIT+NOWAITIT)
					ENDIF
				ENDIF

				loCuteFTP.localfolder = lcLocalPath
				loCuteFTP.getlist("","c:\tempfox\tempdir.dat","%NAME")

				lnFHandle = FOPEN('c:\tempfox\tempdir.dat')
				IF lnFHandle = -1
					.TrackProgress("ERROR: Can't open file [c:\tempfox\tempdir.dat]", LOGIT+SENDIT+NOWAITIT)
					THROW
				ENDIF

				DO WHILE !FEOF(lnFHandle)
					SELECT CURFILES
					APPEND BLANK
					lcString = FGETS(lnFHandle)
					REPLACE CURFILES.fname WITH TRIM(lcString)
				ENDDO
				FCLOSE(lnFHandle)


*!*	SELECT CURFILES
*!*	BROWSE

				* filter list by file mask in ftpsetup...
				
				IF EMPTY(lcFileMask) THEN
					.TrackProgress("ERROR: Empty file mask in FTPSETUP", LOGIT+SENDIT+NOWAITIT)
					THROW
				ELSE
					.TrackProgress("File mask = " + lcFileMask, LOGIT+SENDIT+NOWAITIT)
				ENDIF
				
				IF (lcFileMask == "*.*") THEN
					* We want all files -- no excluding necessary
					lnMatchCount = RECCOUNT('CURFILES')
				ELSE
					* count file mask matches and exclude the non-matches
					lnMatchCount = 0
					SELECT CURFILES
					SCAN
						lcFileName = ALLTRIM(UPPER(CURFILES.fname))
						**added wildcard logic - mvw 03/23/10
						DO CASE
							CASE OCCURS("*",lcFileMask) = 1
								lcLeftFName = UPPER(LEFT(lcFileMask,AT("*",lcFileMask,1)-1))
								lcRightFName = UPPER(RIGHT(lcFileMask,LEN(lcFileMask)-AT("*",lcFileMask,1)))
								IF (lcFileName = lcLeftFName) AND (RIGHT(lcFileName,LEN(lcRightFName)) = lcRightFName)
									* Match!
									lnMatchCount = lnMatchCount + 1
								ELSE
									* no match
									REPLACE CURFILES.fname WITH '' IN CURFILES
									.TrackProgress("Matching files like: " + lcFileMask + " ...excluding file: " + lcFileName, LOGIT+SENDIT+NOWAITIT)
								ENDIF
							CASE (AT(UPPER(lcFileMask), lcFileName) > 0)
								* Match!
								lnMatchCount = lnMatchCount + 1
							OTHERWISE
								* no match
								REPLACE CURFILES.fname WITH '' IN CURFILES
								.TrackProgress("Matching files like: " + lcFileMask + " ...excluding file: " + lcFileName, LOGIT+SENDIT+NOWAITIT)
						ENDCASE
					ENDSCAN
				ENDIF && (lcFileMask == "*.*")


*!*	SELECT CURFILES
*!*	BROWSE

				loCuteFTP.Retries = 3
				loCuteFTP.DELAY = 30 && Delay time between retries in seconds

				.TrackProgress("lnMatchCount = " + TRANSFORM(lnMatchCount), LOGIT+SENDIT+NOWAITIT)

				IF (lnMatchCount < 1) THEN
					.TrackProgress('No files to download!',LOGIT+SENDIT+NOWAITIT)				
				ELSE				
					SELECT CURFILES
					SCAN FOR NOT EMPTY(fname)
						lcRootFilename = ALLTRIM(CURFILES.fname)
						lcDLFile = lcLocalPath + lcRootFilename
						IF FILE(lcDLFile) THEN
							DELETE FILE (lcDLFile)
						ENDIF

						.TrackProgress("Downloading " + lcRootFilename + " to " + lcDLFile + "...", LOGIT+SENDIT+NOWAITIT)

						*loCuteFTP.Download(lcRootFilename,lcDLFile)
						loCuteFTP.Download(lcRootFilename)

						*lcDLMessage = loCuteFTP.WAIT(-1,20)

						*.TrackProgress("FTP Transfer message = " + lcDLMessage, LOGIT+SENDIT+NOWAITIT)
						*WAIT WINDOW TIMEOUT 5 "Transfer message: " + lcDLMessage

						IF NOT FILE(lcDLFile) THEN
							*WAIT WINDOW TIMEOUT 5 "Download failed"
							.TrackProgress("ERROR: Download failed", LOGIT+SENDIT+NOWAITIT)
						ELSE
							* successful download
							* delete remote file
							IF .lTestMode THEN
								.TrackProgress("In Test Mode: not deleting remote file " + lcRootFilename, LOGIT+SENDIT+NOWAITIT)
							ELSE
								.TrackProgress("Deleting remote file " + lcRootFilename, LOGIT+SENDIT+NOWAITIT)
								loCuteFTP.RemoteRemove(lcRootFilename)
							ENDIF
							
							* log d/l
							IF NOT USED("ftpedilog")
								USE F:\edirouting\ftpedilog IN 0 ALIAS ftpedilog
							ENDIF
							SELECT ftpedilog
							APPEND BLANK
							REPLACE ftpedilog.ftpdate WITH DATETIME(), ;
								ftpedilog.filename  WITH lcRootFilename, ;
								ftpedilog.acct_name WITH lcAccountName, ;
								ftpedilog.TYPE      WITH lcEditType, ;
								ftpedilog.xfertype  WITH "GET", ;
								ftpedilog.transfer  WITH lcWhichget
						ENDIF

					ENDSCAN
				ENDIF &&  (lnMatchCount < 1)
				
				WAIT CLEAR

				loCuteFTP.Disconnect

				loCuteFTP.CLOSE

				.TrackProgress('FTP GET Process ended normally!',LOGIT+SENDIT+NOWAITIT)

			CATCH TO loError

				.TrackProgress('There was an error.',LOGIT+SENDIT+NOWAITIT)
				.TrackProgress('The Error # is: ' + TRANSFORM(loError.ERRORNO), LOGIT+SENDIT+NOWAITIT)
				.TrackProgress('The Error Message is: ' + TRANSFORM(loError.MESSAGE), LOGIT+SENDIT+NOWAITIT)
				.TrackProgress('The Error occurred in Program: ' + TRANSFORM(loError.PROCEDURE), LOGIT+SENDIT+NOWAITIT)
				.TrackProgress('The Error occurred at line #: ' + TRANSFORM(loError.LINENO), LOGIT+SENDIT+NOWAITIT)
				lnNumberOfErrors = lnNumberOfErrors + 1
				CLOSE DATA

				IF (NOT ISNULL(.oExcel)) AND (TYPE('.oExcel') = 'O') THEN
					.oExcel.QUIT()
				ENDIF

			ENDTRY

			CLOSE DATABASES ALL

			WAIT CLEAR
			***************** INTERNAL email results ******************************
			.TrackProgress('About to send status email.',LOGIT)
			.TrackProgress('The logfile is: ' + .cLogFile, SENDIT)
			.TrackProgress('FTP GET Process started: ' + .cStartTime, LOGIT+SENDIT+NOWAITIT)
			.TrackProgress('FTP GET Process finished: ' + TTOC(DATETIME()), LOGIT+SENDIT+NOWAITIT)

			IF .lSendInternalEmailIsOn THEN
				DO FORM dartmail2 WITH .cSendTo,.cFrom,.cSubject,.cCC,.cAttach,.cBodyText,"A"
				.TrackProgress('Sent status email.',LOGIT)
			ELSE
				.TrackProgress('Did not send status email: THIS.lSendInternalEmailIsOn = FALSE.',LOGIT)
			ENDIF

		ENDWITH
		RETURN
	ENDFUNC && main



	PROCEDURE TrackProgress
		* do any combination of Wait Window, writing to logfile, and adding to body of email,
		* based on nFlags parameter.
		LPARAMETERS tcExpression, tnFlags
		WITH THIS
			IF BITAND(tnFlags,LOGIT) = LOGIT THEN
				IF .lLoggingIsOn THEN
					?
					? .cProcessName + "  " + TTOC(DATETIME()) + ": " + tcExpression
				ENDIF
			ENDIF
			IF .lWaitWindowIsOn THEN
				IF BITAND(tnFlags,NOWAITIT) = NOWAITIT THEN
					WAIT WINDOW tcExpression NOWAIT
				ENDIF
				IF BITAND(tnFlags,WAITIT) = WAITIT THEN
					WAIT WINDOW tcExpression TIMEOUT .nWaitWindowTimeout
				ENDIF
			ENDIF
			IF BITAND(tnFlags,SENDIT) = SENDIT THEN
				IF .lSendInternalEmailIsOn THEN
					.cBodyText = .cBodyText + tcExpression + CRLF + CRLF
				ENDIF
			ENDIF
		ENDWITH
	ENDPROC  &&  TrackProgress



ENDDEFINE





********************************************************************************************************


*!*	LPARAMETERS tcWhichget

*!*	#DEFINE crlf CHR(13) + CHR(10)

*!*	LOCAL lcattach, lcsendto, lcsubject, lcmessage, lcfrom, loCuteFTP, lcloCuteFTPlog, lcWhichget, lcWhichSite
*!*	LOCAL llTestMode, loerror

*!*	llTestMode = .T.

*!*	STORE '' TO lcattach, lcsendto, lcsubject, lcmessage, lcfrom, loCuteFTP, lcloCuteFTPlog, lcWhichget, lcWhichSite

*!*	lcWhichget = tcWhichget
*!*	lcWhichSite = lcWhichget



*!*	TRY
*!*
*!*		IF NOT llTestMode THEN
*!*			utilsetup("FTPGET")
*!*		ENDIF

*!*		SET EXCL OFF
*!*		SET TALK OFF
*!*		SET SAFETY OFF
*!*		SET DELE ON

*!*		PUBLIC lnftptries
*!*		lnftptries = 0
*!*		DO m:\dev\prg\_setvars WITH .T.

*!*		_SCREEN.CAPTION = "Executing FTP_Getfiles.exe ("+ALLTRIM(lcWhichget)+")"

*!*		WAIT WINDOW AT 10,10 "Ftp_GetFiles executing..." NOWAIT NOCLEAR

*!*		lcloCuteFTPlog = ""

*!*		*First specify a variable called loCuteFTP
*!*		LOCAL loCuteFTP
*!*		CREATE CURSOR filelist(fname c(50))

*!*		IF !USED("ftpsetup")
*!*			USE F:\edirouting\ftpsetup IN 0
*!*		ENDIF

*!*		* wait window at 10,10 "params = "+lcWhichSite+"  "+tuserid
*!*		lcWhichfind = PADR(ALLTRIM(lcWhichget),20)
*!*		SELECT ftpsetup
*!*		LOCATE FOR ftpsetup.transfer = lcWhichfind
*!*		IF !FOUND()
*!*			MESSAGEBOX("This transfer "+lcWhichSite+" is not defined in f:\edirouting\ftpsetup.dbf.... Aborting!")
*!*			USE IN ftpsetup
*!*			THROW
*!*		ELSE
*!*			dpath = ALLTRIM(ftpsetup.localpath)
*!*			IF !DIRECTORY(dpath)
*!*				MESSAGEBOX("This directory "+dpath+" does not exist... Aborting!")
*!*				*schedupdate()
*!*				THROW
*!*			ENDIF

*!*			SET DEFAULT TO &dpath
*!*			lcLocalPath             = ALLTRIM(ftpsetup.localpath)
*!*			lcArchivePath      = ALLTRIM(ftpsetup.archive)
*!*			**llCheckArchive: if need to check the archive to be sure same file was not downloaded and processed on a previous run - mvw 04/27/11
*!*			llCheckArchive       = IIF(ftpsetup.chkarchive AND !EMPTY(lcArchivePath),.T.,.F.)
*!*			**llAutoRename: transfers as .tmp file first, then renames to correct file ext (to avoid incomplete file transfers) - mvw 04/27/11
*!*			llAutoRename	   = ftpsetup.autorename
*!*			lcRemoteFolder     = ALLTRIM(ftpsetup.rfolder)
*!*			lcAccountName      = ALLTRIM(ftpsetup.account)
*!*			lcEditType          = ALLTRIM(ftpsetup.editype)
*!*			lcHostSite         = ALLTRIM(ftpsetup.site)
*!*			lcUsername        = ALLTRIM(ftpsetup.login)
*!*			lcPassword     = ALLTRIM(ftpsetup.PASSWORD)
*!*			_SCREEN.CAPTION    = ftpsetup.MESSAGE
*!*		ENDIF

*!*		lcWhichSite = ALLTRIM(lcHostSite)

*!*		WAIT WINDOW AT 10,10 CHR(13)+"    "+ _SCREEN.CAPTION+CHR(13)+CHR(13)+"****** Now Connecting to ----> "+lcHostSite+CHR(13) TIMEOUT 2

*!*		*Creating a connection object and assign it to the variable
*!*		loCuteFTP = CREATEOBJECT("CuteFTPPro.TEConnection")
*!*		loCuteFTP.protocol = "FTP"
*!*		loCuteFTP.HOST     = lcHostSite
*!*		loCuteFTP.login    = lcUsername
*!*		loCuteFTP.PASSWORD = lcPassword
*!*		loCuteFTP.useproxy = "BOTH"
*!*		loCuteFTP.CONNECT
*!*

*!*		IF loCuteFTP.isconnected >=0
*!*			WAIT ("Could not connect to: " + loCuteFTP.HOST + " Aborting!") WINDOW NOWAIT NOCLEAR
*!*			THROW
*!*		ELSE
*!*			WAIT ("You are now connected to " + loCuteFTP.HOST) WINDOW TIMEOUT 1
*!*		ENDIF

*!*	*!*		IF !USED("ftpedilog")
*!*	*!*			USE F:\edirouting\ftpedilog IN 0 ALIAS ftpedilog
*!*	*!*		ENDIF

*!*	*!*		SELECT ftpedilog

*!*		WAIT WINDOW AT 10,10 CHR(13)+"   Changing to folder.........."+lcRemoteFolder+CHR(13) NOWAIT

*!*		IF EMPTY(lcRemoteFolder)
*!*			WAIT WINDOW AT 10,10 CHR(13)+" Logged into home folder"+CHR(13) NOWAIT
*!*			loCuteFTP.remotefolder = ""
*!*			* when we login we are at our home folder
*!*		ELSE
*!*			WAIT WINDOW AT 10,10 CHR(13)+"   Changing to FTP site folder.........  "+lcRemoteFolder NOWAIT
*!*			b = loCuteFTP.remoteexists(lcRemoteFolder)
*!*			IF b>=0 THEN
*!*				MESSAGEBOX("Site Home directory not found...."+lcRemoteFolder)
*!*				schedupdate()
*!*				RETURN
*!*			ELSE
*!*				WAIT WINDOW AT 10,10 CHR(13)+"   Just changed to folder......->  "+lcRemoteFolder+CHR(13) TIMEOUT 2
*!*				WAIT "" TIMEOUT 2
*!*				loCuteFTP.remotefolder = TRIM(lcRemoteFolder)
*!*			ENDIF
*!*		ENDIF

*!*		loCuteFTP.localfolder = lcLocalPath
*!*		loCuteFTP.getlist("","c:\tempfox\tempdir.dat","%NAME")

*!*		gnhandle = FOPEN('c:\tempfox\tempdir.dat')
*!*		IF gnhandle = -1
*!*			MESSAGEBOX("Can't open file [c:\tempfox\tempdir.dat]")
*!*			CLOSE DATA ALL
*!*			schedupdate()
*!*			RETURN
*!*		ENDIF

*!*		DO WHILE !FEOF(gnhandle)
*!*			SELECT filelist
*!*			APPEND BLANK
*!*			gcstring = FGETS(gnhandle)
*!*			REPLACE filelist.fname WITH TRIM(gcstring)
*!*		ENDDO
*!*		FCLOSE(gnhandle)

*!*		lcFileMask = ALLTRIM(ftpsetup.filemask)
*!*		SELECT filelist
*!*		SCAN
*!*			xfilename=ALLTRIM(UPPER(filelist.fname))
*!*			**added wildcard logic - mvw 03/23/10
*!*			DO CASE
*!*				CASE OCCURS("*",lcFileMask)=1
*!*					lcLeftFName=UPPER(LEFT(lcFileMask,AT("*",lcFileMask,1)-1))
*!*					lcRightFName=UPPER(RIGHT(lcFileMask,LEN(lcFileMask)-AT("*",lcFileMask,1)))
*!*					IF xfilename#lcLeftFName OR RIGHT(xfilename,LEN(lcRightFName))#lcRightFName
*!*						WAIT WINDOW AT 10,10 "looking for files like... "+lcFileMask+"  ....deleting file..........  "+filelist.fname NOWAIT &&timeout 1
*!*						DELETE
*!*					ENDIF

*!*				CASE !AT(UPPER(lcFileMask), xfilename) > 0
*!*					WAIT WINDOW AT 10,10 "looking for files like... "+lcFileMask+"  ....deleting file..........  "+filelist.fname NOWAIT &&timeout 1
*!*					DELETE
*!*			ENDCASE
*!*		ENDSCAN
*!*
*!*
*!*
*!*		*********************************************************************************
*!*		*********************************************************************************
*!*		LOCAL lcDLFile, lcRootFilename, lcDLMessage
*!*
*!*		*lcLocalPath = "C:\"
*!*
*!*		loCuteFTP.Retries = 3
*!*	 	loCuteFTP.Delay = 30 && Delay time between retries in seconds
*!*		SELECT filelist
*!*		SCAN FOR NOT EMPTY(fname)
*!*			lcRootFilename = ALLTRIM(filelist.fname)
*!*			lcDLFile = lcLocalPath + lcRootFilename
*!*			IF FILE(lcDLFile) THEN
*!*				DELETE FILE (lcDLFile)
*!*			ENDIF
*!*
*!*			WAIT WINDOW TIMEOUT 10 "Downloading " + lcRootFilename + " to " + lcDLFile
*!*
*!*			*loCuteFTP.Download(lcRootFilename,lcDLFile)
*!*			loCuteFTP.Download(lcRootFilename)

*!*			lcDLMessage = loCuteFTP.WAIT(-1,20)

*!*			WAIT WINDOW TIMEOUT 5 "Transfer message: " + lcDLMessage

*!*			IF NOT FILE(lcDLFile) THEN
*!*				WAIT WINDOW TIMEOUT 5 "Download failed"
*!*			ELSE
*!*				* successful download
*!*				* delete remote file
*!*				IF NOT llTestMode THEN
*!*					loCuteFTP.remoteremove(lcRootFilename)
*!*				ENDIF
*!*				* log d/l
*!*				IF NOT USED("ftpedilog")
*!*					USE F:\edirouting\ftpedilog IN 0 ALIAS ftpedilog
*!*				ENDIF
*!*				SELECT ftpedilog
*!*				APPEND BLANK
*!*				REPLACE ftpedilog.ftpdate WITH DATETIME(), ;
*!*					ftpedilog.filename  WITH lcRootFilename, ;
*!*					ftpedilog.acct_name WITH lcAccountName, ;
*!*					ftpedilog.TYPE      WITH lcEditType, ;
*!*					ftpedilog.xfertype  WITH "GET", ;
*!*					ftpedilog.transfer  WITH lcWhichget
*!*			ENDIF
*!*
*!*		ENDSCAN
*!*		WAIT CLEAR
*!*		*********************************************************************************
*!*		*********************************************************************************
*!*
*!*		loCuteFTP.Disconnect
*!*
*!*		loCuteFTP.CLOSE


*!*	CATCH TO loerror
*!*		lcsendto = 'mbennett@fmiint.com'

*!*		lcattach = ''
*!*		lcfrom = "mbennett@fmiint.com"
*!*		lcsubject = "FTP Error during Connection To: " + lcWhichget
*!*		lcmessage = "The FTP process encountered an error."

*!*		lcmessage = lcmessage + crlf + 'The Error # is: ' + TRANSFORM(loerror.ERRORNO)
*!*		lcmessage = lcmessage + crlf + 'The Error Message is: ' + TRANSFORM(loerror.MESSAGE)
*!*		lcmessage = lcmessage + crlf + 'The Error occurred in Program: ' + TRANSFORM(loerror.PROCEDURE)
*!*		lcmessage = lcmessage + crlf + 'The Error occurred at line #: ' + TRANSFORM(loerror.LINENO)
*!*		lcmessage = lcmessage + crlf + 'The loCuteFTP log = ' + lcloCuteFTPlog

*!*		DO FORM dartmail2 WITH lcsendto, lcfrom, lcsubject, " ", lcattach, lcmessage,"A"

*!*	ENDTRY

*!*	IF NOT llTestMode THEN
*!*		schedupdate()
*!*	ENDIF

*!*	RETURN