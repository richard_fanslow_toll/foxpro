CLOSE DATA ALL
CLEAR ALL
PUBLIC cOffice,cMod,gOffice,gMasterOffice,lTesting,lEmail,cTransfer,NormalExit,lTestImport,nFileSize,lcErrMessage

lcErrorMessage ="Early Error"
xfile = "unknown"
TRY

	lTesting = .F.
	lTestImport = lTesting
	_setvars(lTesting)

	lOverrideBusy = lTesting
  lOverrideBusy = .t.
	lEmail = .T.
	NormalExit = .F.

	nAcctNum = 6751
	cOffice = "Y"
	STORE cOffice TO cMod,gOffice,gMasterOffice,tsendto,tcc,tsendtoerr,tccerr

	IF !lTesting
		cTransfer = "X"
		STORE "940-TJM" TO cTransfer
		STORE "" TO tsendto,tcc,tsendtoerr,tccerr
		SELECT 0
		USE F:\edirouting\FTPSETUP SHARED
		LOCATE FOR FTPSETUP.TRANSFER = cTransfer
		IF FTPSETUP.chkbusy = .T. AND !lOverrideBusy
			WAIT WINDOW "Busy...exiting program now" TIMEOUT 2
			NormalExit = .T.
			THROW
		ENDIF
		REPLACE chkbusy WITH .T.,trig_time WITH DATETIME() FOR FTPSETUP.TRANSFER = cTransfer
		USE IN FTPSETUP
	ENDIF

	WAIT WINDOW "At start of TJM CR2 940 process" TIMEOUT 2
	_SCREEN.CAPTION = ""
	_SCREEN.CAPTION = "TJM  CR2 940 Process"

	SELECT 0
	USE F:\3pl\DATA\mailmaster ALIAS mm SHARED
	LOCATE FOR (mm.accountid = nAcctNum AND mm.office = cOffice AND edi_type = "940")

	IF FOUND()
		STORE TRIM(mm.acctname) TO cAccountName
		STORE TRIM(mm.basepath) TO lcPath
		STORE TRIM(mm.archpath) TO lcArchivePath
		STORE TRIM(mm.holdpath) TO lcHoldPath
		tsendto = IIF(mm.use_alt,TRIM(mm.sendtoalt),TRIM(mm.sendto))
		tcc = IIF(mm.use_alt,TRIM(mm.ccalt),TRIM(mm.cc))

		IF lTesting
			LOCATE
			LOCATE FOR taskname = "JOETEST"
			tsendto = IIF(mm.use_alt,TRIM(mm.sendtoalt),TRIM(mm.sendto))
			tcc = IIF(mm.use_alt,TRIM(mm.ccalt),TRIM(mm.cc))
		ENDIF
		LOCATE FOR taskname = "GENERAL"
		tsendtoerr = IIF(mm.use_alt,TRIM(mm.sendtoalt),TRIM(mm.sendto))
		tccerr = IIF(mm.use_alt,TRIM(mm.ccalt),TRIM(mm.cc))
		USE IN mm
	ELSE
		SET STEP ON
		USE IN mm
		WAIT WINDOW AT 10,10  "No parameters set for this acct# "+ALLTRIM(STR(nAcctNum))+"  ---> Office "+cOffice TIMEOUT 2
		NormalExit = .F.
		THROW
	ENDIF

	STORE cAccountName TO cCustname
	cUseName = cCustname
	cPropername = cUseName

	CLEAR
	WAIT WINDOW "Now setting up "+cPropername+" 940 process..." TIMEOUT 2

	dXdate1 = ""
	dXxdate2 = DATE()
	nPTQty = 0
	nFileCount = 0

	IF lTesting
*		SET STEP ON
		lcPath = lcPath+"test\"
		WAIT WINDOW "Path used: "+lcPath TIMEOUT 2
	ENDIF

	CD F:\ftpusers\tjm\940IN

	IF lTesting
		cUseFolder = "F:\WHP\WHDATA\"
	ELSE
		xReturn="XXX"
		DO m:\dev\prg\wf_alt WITH cOffice,nAcctNum
		ASSERT .F.
		cUseFolder = xReturn
	ENDIF
set step On 
	DO ("m:\dev\PRG\"+cCustname+"940_PROCESS")

	IF USED('ftpsetup')
		USE IN FTPSETUP
	ENDIF
	IF !lTesting
		SELECT 0
		USE F:\edirouting\FTPSETUP SHARED
		REPLACE chkbusy WITH .F. FOR FTPSETUP.TRANSFER = cTransfer
		USE IN FTPSETUP
	ENDIF
	NormalExit = .T.

CATCH TO oErr
	IF !NormalExit
		ASSERT .F. MESSAGE "In CATCH"
		SET STEP ON
		tsubject = "TJM 940 Upload Error ("+TRANSFORM(oErr.ERRORNO)+") at "+TTOC(DATETIME())
		tattach  = ""
		tsendto  = tsendtoerr
		tcc = tccerr
		tmessage = "TJM 940 Upload Error..... Please fix me........!"
		lcSourceMachine = SYS(0)
		lcSourceProgram = SYS(16)

		tmessage =tmessage+ "Try/CATCH Exeception Message:"+CHR(13)+;
			[  Procedure: ] + oErr.PROCEDURE +CHR(13)+;
			[  LineNo: ] + STR(oErr.LINENO) +CHR(13)+;
			[  Message: ] + oErr.MESSAGE +CHR(13)+;
			[  Error: ] + STR(oErr.ERRORNO) +CHR(13)+;
			[  Details: ] + oErr.DETAILS +CHR(13)+;
			[  StackLevel: ] + STR(oErr.STACKLEVEL) +CHR(13)+;
			[  LineContents: ] + oErr.LINECONTENTS+CHR(13)+;
			[  Message: ] + lcErrMessage+CHR(13)+;
			[  UserValue: ] + oErr.USERVALUE+CHR(13)+;
			[  Computer:  ] +lcSourceMachine+CHR(13)+;
			[  940 file:  ] +xfile+CHR(13)+;
			[  Program:   ] +lcSourceProgram
		tmessage = tmessage+CHR(13)+CHR(13)+lcErrMessage

		tfrom    ="TGF EDI Processing Center <toll-edi-ops@tollgroup.com>"
		DO FORM m:\dev\frm\dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
	ELSE
		WAIT WINDOW AT 10,10 "Normal Exit " TIMEOUT 2
	ENDIF
FINALLY
	RELEASE ALL LIKE T*
	CLOSE DATABASES ALL
ENDTRY
