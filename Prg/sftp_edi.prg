*!* SFTP_EDI.PRG
*!* Sets up FTP Connections and Uploads files, currently based on triggers in SFTPJOBS.
*!* Also sends confirmation or "bad FTP" eMails - (old code, not tested).

*************** 9/25/14 MB ***************************************************************
* Based on FTP_EDI
* Called from form SFTPHANDLER in project SFTP_POLLER.
* Added support for secure protocols other than FTP, such as SFTP or FTPS.
* BUT must be run using CuteFTP 9 for these to work. That is currently only on FTP1 server.
* Also added support for optional PORT and DATACHANNEL assignments.
*
* Added support for FILEMASK field, so only matching files in a folder are uploaded, 9/29/14 MB
*
* Commented out Customer-specific special logic 9/29/14 MB
*
* Build EXE in F:\EDIRouting\proj\
*
* NOTE: uses clones of the regular ftp tables; FTPSETUP --> SFTSETUP, FTPJOBS--> SFTPJOBS, etc.
*
* 4/25/2018 MB: changed fmiint.com to Toll email.
*
******************************************************************************************

LPARAMETERS lcwhichsite, toCallingFormObjRef

#DEFINE CRLF CHR(13) + CHR(10)


*!*	*****************************************************************************************************************************
*!*	* added this try...catch block 10/31/2016 to try and prevent this program from running more than once at a time.
*!*	TRY
*!*		USE F:\UTIL\FTPSTUFF\DATA\SFTP_EDI EXCLUSIVE
*!*	CATCH
*!*		* if we got to here, another instance of this program is already opened and has the table locked! so we want to quit...
*!*		WAIT WINDOW TIMEOUT 5 '===> Another instance of this program has F:\UTIL\FTPSTUFF\DATA\sftp_edi locked. Quitting!'
*!*		QUIT
*!*	ENDTRY
*!*	*****************************************************************************************************************************


TRY

	LOCAL lcattach, lcfrom, lcsendto, lcsubject, lcmessage, mysite, loerror, llAppendExtension, lcExtension, lcRenamedFile
	LOCAL lnResult, lcUserMessage, lnNumFiles, lnCurrentFile, luRetVal, lcMemFile, lcOwner
	
	lcUserMessage = "====> Crashed opening F:\UTIL\FTPSTUFF\DATA\SFTP_EDI.DBF EXCLUSIVE - this process must be running twice!"
	USE F:\UTIL\FTPSTUFF\DATA\SFTP_EDI EXCLUSIVE
		
	lcUserMessage = ""
	
	lcMemFile = "F:\UTIL\FTPSTUFF\SFTP_EDI_" + RIGHT(DTOS(DATE()),6) + STRTRAN(TIME(),":","") + ".MEM"

	PRIVATE plGotObjRef, poCallingFormObjRef, pcWhichSite
	poCallingFormObjRef = toCallingFormObjRef
	plGotObjRef = (TYPE('poCallingFormObjRef') = 'O')

	UpdateMessage( "Now in SFTP_EDI" )
	
	pcWhichSite = lcwhichsite

	SET EXCL OFF
	SET SAFETY OFF
	SET TALK OFF

	pnnotsent = 0
	tsubject = ""
	tmessage = ""
	tattach = ""
	lncount = 0

	lcaccountname = "?"
	ARCHIVEFILE = ""
	lceditype = "?"
	lcftpfile = "?"
	lcftptype = "?"


	PUBLIC ftperror
	ftperror = .F.

	IF !USED("sftpedilog")
		USE F:\edirouting\sftpedilog IN 0 ALIAS sftpedilog
	ENDIF
	IF !USED("sftpsetup")
		USE F:\edirouting\sftpsetup IN 0 ALIAS sftpsetup
	ENDIF

	*!*	SELECT sftpjobs
	*!*	REPLACE sftpjobs.tries WITH sftpjobs.tries+1
	*!*	lnftptries = sftpjobs.tries

	SELECT sftpsetup


	LOCATE FOR UPPER(ALLTRIM(transfer)) == UPPER(ALLTRIM(lcwhichsite))

	IF !FOUND() && if transfer name does not exist in sftpsetup

		UpdateMessage( "This transfer "+lcwhichsite+" is not defined in f:\edirouting\sftpsetup.dbf.... Aborting!" )

		lcftpfile = "Aborted: Not defined in sftpSETUP..."
		lcftptype = "UNK"
		DO badftp


		THROW
	ELSE
		dpath = ALLTRIM(sftpsetup.localpath)
		xaccount=ALLTRIM(sftpsetup.account)
		lcOwner = ALLTRIM(sftpsetup.owner)

		IF !DIRECTORY(dpath) && if local folder is missing
			UpdateMessage( "This LOCAL directory "+dpath+" for transfer "+ALLTRIM(sftpsetup.transfer)+" does not exist... Aborting!" )

			lcftpfile = "Aborted: Directory Not Found..."
			lcftptype = "UNK"
			DO badftp


			THROW
		ENDIF

		SET DEFAULT TO &dpath
		lcpath=ALLTRIM(sftpsetup.localpath)
		lcarchivepath=ALLTRIM(sftpsetup.archive)

		IF !DIRECTORY(lcarchivepath) && if archive folder is missing
			UpdateMessage( "This ARCHIVE directory "+lcarchivepath+" does not exist... Aborting!" )

			lcftpfile = "Aborted: Archive DIR Not Found..."
			lcftptype = "UNK"
			DO badftp

			THROW
		ENDIF

		lcFileMask     = ALLTRIM(sftpsetup.filemask)
		IF EMPTY(lcFileMask) THEN
			lcFileMask = "*.*"
		ENDIF

		lcremotefolder     = ALLTRIM(sftpsetup.rfolder)
		lcaccountname      = ALLTRIM(sftpsetup.account)
		lceditype          = ALLTRIM(sftpsetup.editype)
		lcthissite         = ALLTRIM(sftpsetup.site)
		lcthislogin        = ALLTRIM(sftpsetup.login)
		lcthispassword     = ALLTRIM(sftpsetup.PASSWORD)
		lcDataChan         = UPPER(ALLTRIM(sftpsetup.datachan))
		lcProtocol         = UPPER(ALLTRIM(sftpsetup.protocol))
		lnPort             = sftpsetup.port
		llAppendExtension	= sftpsetup.lappendext
		lcExtension			= ALLTRIM(sftpsetup.cextension)
	ENDIF
	
	
	lcoldcaption = _SCREEN.CAPTION
	lcwhichsite = ALLTRIM(lcwhichsite)
	_SCREEN.CAPTION    = "SFTP_EDI: " + lcwhichsite
		
	* changed to do also this check BEFORE trying to connect, to save FTP resources. We will not try to connect if there are no files to upload
	* 8/4/2016 MB
	lnNumFiles = ADIR(ttarray,lcFileMask)

	IF lnNumFiles = 0 THEN
		WAIT WINDOW ("SFTP_EDI: found no files to upload in " + lcpath) TIMEOUT 5
	ELSE
		* continue with ftp connection, etc.

		SELECT sftpedilog
		UpdateMessage( "Connecting to "+lcthissite )

		*Creating a connection object and assign it to the variable
		mysite = CREATEOBJECT("CuteFTPPro.TEConnection")


		* assign connection properties
		IF NOT EMPTY(lcProtocol) THEN
			mysite.protocol = lcProtocol
		ELSE
			mysite.protocol = "FTP"
		ENDIF

		mysite.HOST     = lcthissite
		mysite.login    = lcthislogin
		mysite.PASSWORD = lcthispassword
		mysite.retries = 2 && perCuteFTP Help: This is CONNECTION retries, not TRANSFER retries.
		mysite.DELAY = 15 && perCuteFTP Help: This is the delay between CONNECTION retries, in seconds.
		mysite.useproxy = "BOTH"
		mysite.FileOverWriteMethod = "OVERWRITE"  && if target files exists, overwrite it.

		*mysite.datachannel="PORT"
		IF NOT EMPTY(lcDataChan) THEN
			mysite.datachannel = lcDataChan
		ENDIF

		IF lnPort > 0 THEN
			mysite.port = lnPort
		ENDIF

		* try to connect...
		mysite.CONNECT

		lcmessage = mysite.WAIT(-1,120000)

		IF !ISBLANK(lcmessage)
			UpdateMessage( "SFTP_EDI Transfer message: "+lcmessage )
		ENDIF
		
	*!*		IF !ISBLANK(lcmessage)
	*!*			WAIT WINDOW "SFTP_DOWNLOAD: Transfer message "+lcmessage TIMEOUT 2
	*!*		ENDIF

		IF mysite.isconnected >=0 THEN
			lcUserMessage = "Could not connect to: " + mysite.HOST + "...Aborting!"
				lcUserMessage = "Could not connect to: " + mysite.HOST + " Aborting!"
				UpdateMessage( lcUserMessage )
				lcftpfile = lcUserMessage
				lcftptype = "UNK"
				Do badftp
				THROW
		ELSE
			WAIT WINDOW ("SFTP_EDI: now connected to " + mysite.HOST) TIMEOUT 2
		ENDIF


	*!*		DO CASE
	*!*		
	*!*			CASE ATC("ERROR",lcmessage)>0 && if CuteFTP Pro can't make a socket connection
	*!*				lcUserMessage = "FTP Connection failure: " + lcwhichsite + ", User: " + tuserid + ", msg: " + lcmessage
	*!*				*lcftpfile = "Aborted: Transfer Error..."
	*!*				lcftpfile = lcUserMessage
	*!*				lcftptype = "UNK"
	*!*				Do badftp
	*!*				THROW
	*!*				
	*!*				*!*	tsubject = "FTP Socket Connect Error inside Cute FTP Pro TE"
	*!*				*!*	tmessage = "FTP Connection failure: "+lcwhichsite+", User: "+tuserid
	*!*				*!*	DO badftp
	*!*				
	*!*			CASE mysite.isconnected >=0 && If CuteFTP Pro has another connection error
	*!*				lcUserMessage = "Could not connect to: " + mysite.HOST + " Aborting!"
	*!*				UpdateMessage( lcUserMessage )
	*!*				lcftpfile = lcUserMessage
	*!*				lcftptype = "UNK"
	*!*				Do badftp
	*!*				THROW

	*!*				*!*	lcftpfile = "Aborted: Transfer Error..."
	*!*				*!*	lcftptype = "UNK"
	*!*				*!*	DO badftp

	*!*				*!*	RETURN

	*!*			OTHERWISE
	*!*				UpdateMessage( "Now connected to "+mysite.HOST )
	*!*		ENDCASE

		UpdateMessage( "Setting up folders.........." )

		IF EMPTY(lcremotefolder)
			mysite.remotefolder = ""
			* when we login we are at our home folder
		ELSE
			* WE NEED TO CHANGE FOLDERS
			WAIT WINDOW "SFTP_EDI: Changing to folder.........."+lcremotefolder TIMEOUT 2
			
			b=mysite.remoteexists(lcremotefolder)

			IF b>=0 THEN
				UpdateMessage( "remote directory not found...."+lcremotefolder+" for "+lcwhichsite )

				lcftpfile = "Aborted: remote directory not found"
				lcftptype = "UNK"
				DO badftp

				THROW
			ELSE
				mysite.remotefolder = ALLTRIM(lcremotefolder)
			ENDIF
		ENDIF

		mysite.localfolder = lcpath
		lnNumFiles = ADIR(tarray,lcFileMask)

		***************************************************************************************************
		* now start uploading the file(s)
		***************************************************************************************************
		archivename = TTOC(DATETIME(),1)  && replaces code above  ??????

		STORE "" TO XFILE,ARCHIVEFILE

		IF ftperror THEN
			THROW
		ENDIF

		IF lnNumFiles >= 1 THEN
		
			FOR lnCurrentFile = 1  TO lnNumFiles
			
				IF ftperror
					THROW
				ENDIF
				
				* Skip files with sizes less than 10 bytes...
				IF tarray[lnCurrentFile,2] < 10
					LOOP
				ENDIF

				cFilename = ALLTRIM(tarray[lnCurrentFile,1])

				XFILE = ADDBS(lcpath)+cFilename

				ARCHIVEFILE = ADDBS(lcarchivepath)+cFilename

				IF FILE(XFILE) THEN
				
					UpdateMessage( "Now uploading "+XFILE )
					llTransferOK = .T.

					luRetVal = mysite.upload(XFILE)
					
					*WAIT WINDOW TIMEOUT 2 "luRetVal = " + TRANSFORM(luRetVal)

					*strResult = mysite.WAIT(-1,60000)
					*strResult = mysite.WAIT(-1,120000)
					* 8/10/2016: According to Help, .upload is synchronous, meaning it must have finished if we go to here.
					* so no need to have a huge value for the .Wait.
					strResult = mysite.WAIT(-1,10)
					
					DO CASE
						CASE "CANCELLED"$strResult
							UpdateMessage( "FTP Operation cancelled......."+strResult )
							llTransferOK = .F.
						CASE "FINISHED"$strResult
							* modified this section to support adding .Ready extension for files we ftp to Ceridian. MB 1/8/2015.
							WAIT WINDOW TIMEOUT 2 "FTP Result = FINISHED."
							
							lcUserMessage = "Last file uploaded was: " + XFILE
							
							IF llAppendExtension THEN
								* rename the transferred file, adding specified extension
								WAIT WINDOW TIMEOUT 2 "We need to add extension [" + lcExtension + "] to the transferred file."
								
								* create new filename
								lcRenamedFile = cFilename + lcExtension
								
								* if the new filename already exists, delete it
								lnResult = mysite.RemoteExists(lcRenamedFile)
								IF lnResult < 0 THEN
									WAIT WINDOW TIMEOUT 3 "Renamed file [" + lcRenamedFile + "] already exists. DELETING IT"
									mysite.RemoteRemove(lcRenamedFile)
								ENDIF
								
								WAIT WINDOW TIMEOUT 2 "Now adding extension [" + lcExtension + "]"
								mysite.RemoteRename(cFilename, (lcRenamedFile))							
							ENDIF
						
							UpdateMessage( "FTP Operation complete......."+strResult )
						CASE "ERROR"$strResult
							UpdateMessage( "FTP Operation Error......"+strResult )
							llTransferOK = .F.
						CASE "SKIPPED"$strResult
							UpdateMessage( "FTP Operation skipped......."+strResult )
						OTHERWISE
							UpdateMessage( "Unknown FTP Operation return messaged......."+strResult )
							llTransferOK = .F.
					ENDCASE

					UpdateMessage( "Transfer message for "+lcwhichsite+": "+strResult )

					SELECT sftpedilog
					APPEND BLANK
					REPLACE sftpedilog.ftpdate   WITH DATETIME(), ;
					sftpedilog.transfer  WITH lcwhichsite, ;
					sftpedilog.xfertype  WITH "PUT", ;
					sftpedilog.filename  WITH XFILE, ;
					sftpedilog.acct_name WITH lcaccountname, ;
					sftpedilog.arch_name WITH ARCHIVEFILE, ;
					sftpedilog.TYPE      WITH lceditype, ;
					sftpedilog.transmsg  WITH strResult ;
					IN sftpedilog

					UpdateMessage( "SFTP Log Updated for file "+XFILE )

					IF ftperror OR !llTransferOK
						THROW
					ENDIF

					IF pnnotsent = 0
						COPY FILE [&XFILE] TO [&ARCHIVEFILE]
						IF FILE([&ARCHIVEFILE])
							DELETE FILE [&XFILE]
						ENDIF
					ENDIF
				ELSE
					UpdateMessage( XFILE+"  not found  !!!!!!!!!!!! " )
				ENDIF
				STORE "" TO XFILE,ARCHIVEFILE
			NEXT lnCurrentFile

			*!*		SELECT sftpjobs
			*!*		REPLACE terminated WITH .F.
			*!*		REPLACE COMPLETE WITH .T.
			*!*		REPLACE busy WITH .F.
			*!*		REPLACE exectime WITH DATETIME()
			*!*		DELETE
		ELSE
			UpdateMessage( "No files to upload!" )
		ENDIF  && lnNumFiles >= 1

		*mysite.tecommand("deletefinished")
		*mysite.CLOSE
		mysite.CLOSE("EXITNOPENDING")

		******************************************************************************************

		*!*	IF pnnotsent = 0
		*!*		IF sftpsetup.do_email = .T.
		*!*			tattach=""
		*!*			lnnum = ADIR(tarray,lcFileMask)
		*!*			IF lnnum >= 1
		*!*				FOR thisfile = 1  TO lnnum
		*!*					XFILE = lcpath+tarray[thisfile,1]
		*!*					IF FILE(XFILE)
		*!*						tattach = tattach+XFILE+";"
		*!*					ENDIF
		*!*				NEXT thisfile
		*!*			ENDIF
		*!*		ENDIF

		*!*		UpdateMessage( "Data Transfer Complete............" )
		*!*	ELSE
		*!*		UpdateMessage( "Data Transfer Incomplete !!!!" )

		*!*		lcftpfile = "Aborted: Data Xfer Incomplete..."
		*!*		lcftptype = "UNK"
		*!*		DO badftp

		*!*	ENDIF


		*!*	USE IN sftpsetup

		*!*	USE IN sftpedilog

		*!*	SELECT sftpjobs
		*!*	REPLACE COMPLETE WITH .T., terminated WITH .F., busy WITH .F. IN sftpjobs
		*!*	DELETE IN sftpjobs


		_SCREEN.CAPTION    = lcoldcaption
	
	ENDIF && lnnum = 0

CATCH TO loerror
	*SAVE TO &lcMemFile
	lcattach = ''
	lcfrom = "mark.bennett@tollgroup.com"
	lcsendto = "mark.bennett@tollgroup.com"
	lcsubject = "SFTP_EDI Error during Connection To: " + lcwhichsite
	lcmessage = "The FTP process encountered an error."

	lcmessage = lcmessage + crlf + 'The Error # is: ' + TRANSFORM(loerror.ERRORNO)
	lcmessage = lcmessage + crlf + 'The Error Message is: ' + TRANSFORM(loerror.MESSAGE)
	lcmessage = lcmessage + crlf + 'The Error occurred in Program: ' + TRANSFORM(loerror.PROCEDURE)
	lcmessage = lcmessage + crlf + 'The Error occurred at line #: ' + TRANSFORM(loerror.LINENO)
	lcmessage = lcmessage + crlf + crlf + '===> The User Defined Message = ' + lcUserMessage
	DO FORM dartmail2 WITH lcsendto, lcfrom, lcsubject, " ", lcattach, lcmessage,"A"

FINALLY

	CLOSE DATABASES ALL

ENDTRY

RETURN


PROCEDURE UpdateMessage
	LPARAMETERS tcMessage
	IF plGotObjRef THEN
		poCallingFormObjRef.AddTextLine( tcMessage )
	ELSE
		WAIT WINDOW NOWAIT tcMessage
	ENDIF
	RETURN
ENDPROC




********************************************************************************************************************
PROCEDURE badftp

	SELECT sftpedilog
	APPEND BLANK
	REPLACE sftpedilog.ftpdate     WITH DATETIME(), ;
	sftpedilog.filename    WITH lcftpfile, ;
	sftpedilog.acct_name   WITH lcaccountname, ;
	sftpedilog.TYPE        WITH lcftptype, ;
	sftpedilog.xfertype    WITH "PUT", ;
	sftpedilog.transfer    WITH pcWhichSite, ;
	sftpedilog.arch_name   WITH "PUT-FAILED" ;
	IN sftpedilog
	
	 && , ERROR ="+ALLTRIM(TRANSFORM(lcerror1))

	*!*		SELECT sftpjobs
	*!*		REPLACE sftpjobs.errcode        WITH lcerror1
	*!*		REPLACE sftpjobs.busy WITH .F.
ENDPROC
