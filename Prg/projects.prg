parameters xdetail

create cursor xrpt ( ;
	projno c(6), ;
	custrefno c(15), ;
	wo_num n(7), ;
	wotype c(1), ; &&added to eliminate error, not sure where wotype should be pulled from - mvw 08/26/08
	invnum c(8), ;
	startdt d, ;
	completeby d, ;
	completed d, ;
	pickticket c(15), ;
	accountid n(4), ;
	acctname c(30), ;
	consignee c(20), ;
	totrevenue n(9,2), ;
	hours n(4), ;
	cartons n(5), ;
	comment m, ;
	jobdesc c(50), ;
	qty n(6), ;
	unittype c(8), ;
	rate n(8,3), ;
	totcharge n(8,2))

select * from project where accountid = xaccountid and (completeddt={} and empty(invnum)) into cursor csrproject

if !xdetail
	scan
		scatter memvar

		select projdet
		if !seek(csrproject.projectid, "projDet", "projectId")
			scatter memvar blank
			m.jobdesc=""
			m.unittype=""
			insert into xrpt from memvar
		else
			scan for projectid = csrproject.projectid
				scatter memvar
				m.jobdesc=description
				insert into xrpt from memvar
			endscan
		endif
	endscan

else
	scan
		scatter memvar memo
		m.completeby=m.completebydt
		m.completed=m.completeddt
		if m.compneed
			m.comment=space(60)+"COMPONENTS NEEDED!"+crlf+crlf+m.comment
		endif

		select projdet
		if !seek(csrproject.projectid, "projDet", "projectId")
			scatter memvar blank
			m.jobdesc=""
			m.unittype=""
			insert into xrpt from memvar
		else
			scan for projectid = csrproject.projectid
				scatter memvar
				m.jobdesc=description
				insert into xrpt from memvar
				m.startdt={}
				m.completeby={}
			endscan
		endif
	endscan

endif

select xrpt

loadwebpdf(crptname)

use in xrpt

set database to wh
close databases
