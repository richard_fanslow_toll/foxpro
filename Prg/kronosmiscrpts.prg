* A variety of Kronos reports that will not be run terribly often, but still need to be automated.
* EXE goes in F:\UTIL\KRONOS\

LPARAMETERS tcReportName

LOCAL loKronosMiscReport, lcReportName, lTestMode
lTestMode = .F.

IF NOT lTestMode THEN
	utilsetup("KRONOSMISCRPTS")
ENDIF

DECLARE INTEGER CreateMutex IN Win32API ;
	STRING @lpMutexAttributes, ;
	STRING bInitialOwner, ;
	STRING @lpName

DECLARE INTEGER GetLastError IN Win32API

* if running in EXE check to see if already running
IF _VFP.STARTMODE > 0 THEN
	lcProcessName = "KRONOSMISCRPTS"
	CreateMutex(0,CHR(1),@lcProcessName)
	lnError = GetLastError()
	IF lnError = 183
		WAIT WINDOW TIMEOUT 5 "This program is already running..."
		IF NOT lTestMode THEN
			schedupdate()
		ENDIF
		RETURN .F.
	ENDIF
ENDIF

IF EMPTY(tcReportName) THEN
	lcReportName = "?"
ELSE
	lcReportName = UPPER(ALLTRIM(tcReportName))
ENDIF

loKronosMiscReport = CREATEOBJECT('KRONOSMISCRPTS')
loKronosMiscReport.MAIN( lcReportName, lTestMode )

IF NOT lTestMode THEN
	schedupdate()
ENDIF

CLOSE DATABASES ALL
RETURN

#DEFINE LOGIT 1
#DEFINE WAITIT 2
#DEFINE NOWAITIT 4
#DEFINE SENDIT 8
#DEFINE CRLF CHR(13) + CHR(10)
#DEFINE RETURN_DATA_MANDATORY .F.
#DEFINE RETURN_DATA_NOT_MANDATORY .T.

DEFINE CLASS KRONOSMISCRPTS AS CUSTOM

	cProcessName = 'KRONOSMISCRPTS'

	lTestMode = .F.  && .F. FOR NORMAL OPERATION, .T. TO SUPPRESS ALL BUT DEVELOPER EMAILS AND CHANGE INPUT TABLE, ETC.
	cReportName = "?"

	lAutoYield = .F.
	cStartTime = TTOC(DATETIME())

	* connection properties
	nSQLHandle = 0

	* file properties
	nFileHandle = 0

	* date properties
	dtNow = DATETIME()

	dToday = DATE()
	*dToday = {^2011-05-01}
	cToday = ''

	* wait window properties
	nWaitWindowTimeout = 2
	lWaitWindowIsOn = .T.

	* logfile properties
	lLoggingIsOn = .T.
	cLogFile = 'F:\UTIL\KRONOS\Logfiles\KronosMiscReport_log.txt'

	* INTERNAL email properties
	lSendInternalEmailIsOn = .T.
	cFrom = 'FMI Corporate <fmicorporate@fmiint.com>'
	*cSendTo = 'mbennett@fmiint.com'
	cSendTo = 'mbennett@fmiint.com'
	cCC = ''
	cSubject = 'Kronos Misc Report for: ' + TTOC(DATETIME())
	cAttach = ''
	cBodyText = ''
	cTopText = ''

	* reporting logic properties
	cCOMPUTERNAME = ''
	cUSERNAME = ''

	FUNCTION INIT
		IF NOT DODEFAULT()
			RETURN .F.
		ENDIF
		WITH THIS
			CLEAR
			*SET RESOURCE OFF
			CLOSE DATA
			SET CENTURY ON
			SET DATE YMD
			SET HOURS TO 24
			SET ANSI ON
			SET TALK OFF
			SET DELETED ON
			SET CONSOLE OFF
			SET EXCLUSIVE OFF
			SET SAFETY OFF
			SET EXACT OFF
			SET STATUS BAR ON
			SET SYSMENU OFF
			SET ENGINEBEHAVIOR 70
			_VFP.AUTOYIELD = .lAutoYield
			.cCOMPUTERNAME = GETENV("COMPUTERNAME")
			.cUSERNAME = GETENV("USERNAME")
			.lLoggingIsOn = .lLoggingIsOn AND NOT EMPTY(.cLogFile)
		ENDWITH
	ENDFUNC


	FUNCTION DESTROY
		WITH THIS
			IF .lLoggingIsOn  THEN
				SET ALTERNATE OFF
				SET ALTERNATE TO
			ENDIF
		ENDWITH
		DODEFAULT()
	ENDFUNC


	FUNCTION MAIN
		LPARAMETERS tcReportName, tlTestMode
		WITH THIS
			LOCAL lnNumberOfErrors, lcSQL, lcSQL2, loError, ldToday, ln3MonthsAgo, lcFilename
			LOCAL lcFromDate, lcToDate, lnFirstDayInMonth, lnLastDayInMonth, lnYear, lnMonth, lnDay

			lnNumberOfErrors = 0

			TRY

				.lTestMode = tlTestMode

				IF .lTestMode THEN
					.cSendTo = 'mbennett@fmiint.com'
					.cLogFile = 'F:\UTIL\KRONOS\Logfiles\KronosMiscReport_log_TESTMODE.txt'
				ENDIF

				IF .lLoggingIsOn THEN
					SET ALTERNATE TO (.cLogFile) ADDITIVE
					SET ALTERNATE ON
				ENDIF

				.TrackProgress('Kronos Misc Report process started....', LOGIT+SENDIT+NOWAITIT)
				.TrackProgress('COMPUTERNAME = ' + .cCOMPUTERNAME, LOGIT+SENDIT)
				.TrackProgress('USERNAME = ' + .cUSERNAME, LOGIT+SENDIT)
				.TrackProgress('REPORT NAME PARAMETER = ' + tcReportName, LOGIT+SENDIT)
				IF .lTestMode THEN
					.TrackProgress('                        TEST MODE                        ', LOGIT+SENDIT)
				ENDIF

				IF INLIST(tcReportName,"NEWHIREACCRUALS") THEN
					.cReportName = tcReportName
				ELSE
					.TrackProgress('Invalid Report Name Parameter = ' + tcReportName, LOGIT+SENDIT)
					THROW
				ENDIF

				*!*					* calculate pay period start/end dates.
				*!*					* We go back from today until we hit a Saturday (DOW = 7) , which is the end day of the previous pay period.
				*!*					* then we subtract 6 days from that to get the prior Sunday (DOW = 1), which is the start day of the previous pay period.

				*!*					.cToday = DTOC(.dToday)

				*!*					ldDate = .dToday
				*!*					DO WHILE DOW(ldDate,1) <> 7
				*!*						ldDate = ldDate - 1
				*!*					ENDDO
				*!*					ldEndDate = ldDate
				*!*					ldStartDate = ldDate - 6

				* define date-related variables
				.cToday = DTOC(.dToday)
				ldToday = .dToday
				lnYear = YEAR(ldToday)
				lnMonth = MONTH(ldToday)
				lnDay = DAY(ldToday)
				lnFirstDayInMonth = (ldToday - lnDay + 1)
				lnLastDayInMonth = GOMONTH(lnFirstDayInMonth,1) - 1

				DO CASE
					CASE .cReportName = "NEWHIREACCRUALS"
						* lists people with hire dates or seniority dates 3 months ago
						.cSendTo = 'mbennett@fmiint.com'
						.cCC = ''
						.cTopText = "Grant the quarter-based accruals in Kronos!"
						.cSubject = 'Kronos Misc Report [' + .cReportName + ']'

						ln3MonthsAgo = GOMONTH(ldToday,-3)
						lnYear = YEAR(ln3MonthsAgo)
						lnMonth = MONTH(ln3MonthsAgo)
						lnDay = DAY(ln3MonthsAgo)
						lnFirstDayInMonth = (ln3MonthsAgo - lnDay + 1)
						lnLastDayInMonth = GOMONTH(lnFirstDayInMonth,1) - 1

						lcFromDate = "'" + TRANSFORM(lnYear) + "-" + TRANSFORM(lnMonth) + "-" + TRANSFORM(DAY(lnFirstDayInMonth)) + "'"
						lcToDate = "'" + TRANSFORM(lnYear) + "-" + TRANSFORM(lnMonth) + "-" + TRANSFORM(DAY(lnLastDayInMonth)) + "'"

						SET TEXTMERGE ON
						TEXT TO	lcSQL NOSHOW

SELECT
PERSONNUM AS FILE_NUM,
PERSONFULLNAME AS NAME,
EMPLOYMENTSTATUS AS STATUS,
SENIORITYRANKDATE AS SENIORDATE,
COMPANYHIREDTM AS HIREDATE,
'                              ' AS COMMENT
FROM VP_EMPLOYEEV42
WHERE EMPLOYMENTSTATUS = 'ACTIVE'
AND HOMELABORLEVELNM1 <> 'TMP'
AND ( ( SENIORITYRANKDATE >= <<lcFromDate>> AND SENIORITYRANKDATE <= <<lcToDate>> )
OR ( COMPANYHIREDTM >= <<lcFromDate>> AND COMPANYHIREDTM <= <<lcToDate>> ) )
ORDER BY SENIORITYRANKDATE

						ENDTEXT
						SET TEXTMERGE OFF

						SET TEXTMERGE ON
						TEXT TO	lcSQL2 NOSHOW
						
SELECT
P.PERSONNUM AS FILE_NUM, P.FULLNM AS EMPLOYEE, R.ACTUALCUSTOMDTM AS SENIORDATE
FROM PERSON P
INNER JOIN JAIDS J
ON J.PERSONID = P.PERSONID
INNER JOIN PRSNCSTMDATEMM R
ON R.PERSONID = J.PERSONID
AND R.CUSTOMDATETYPEID = 1
WHERE J.PERSONID > 0
AND J.DELETEDSW = 0
AND P.PERSONNUM IN (SELECT PERSONNUM FROM VP_EMPLOYEEV42 WHERE EMPLOYMENTSTATUS <> 'Terminated')
ORDER BY P.PERSONNUM

						ENDTEXT
						SET TEXTMERGE OFF

						.nSQLHandle = SQLCONNECT("WTK2","sa","kr0n1tes")

						IF .nSQLHandle > 0 THEN
						
							IF USED('CURMISC') THEN
								USE IN CURMISC
							ENDIF

							IF USED('CURMISC2') THEN
								USE IN CURMISC2
							ENDIF

							IF .ExecSQL(lcSQL, 'CURMISC', RETURN_DATA_MANDATORY) AND ;
								.ExecSQL(lcSQL2, 'CURMISC2', RETURN_DATA_MANDATORY)THEN
								
								* REPLACE THE SENIORITY DATE IN CURMISC WITH THE ONE IN CURMISC2, IF FOUND
								SELECT CURMISC
								SCAN
									SELECT CURMISC2
									LOCATE FOR ALLTRIM(FILE_NUM) == ALLTRIM(CURMISC.FILE_NUM)
									IF FOUND() THEN
										REPLACE CURMISC.SENIORDATE WITH CURMISC2.SENIORDATE, CURMISC.COMMENT WITH "Note Seniority Date!"
									ENDIF								
								ENDSCAN

								*SELECT CURMISC
								*BROW

								lcFilename = "F:\UTIL\KRONOS\REPORTS\" + .cReportName + ".XLS"
								SELECT CURMISC
								COPY TO ( lcFilename ) XL5

								.cAttach = lcFilename

							ENDIF  &&  .ExecSQL(lcSQL, 'CURWTKEMPLOYEE', RETURN_DATA_MANDATORY)

						ELSE
							* connection error
							.TrackProgress('Unable to connect to WTK System.', LOGIT+SENDIT)
						ENDIF   &&  .nSQLHandle > 0
						
					OTHERWISE
						* nothing
				ENDCASE

				.cBodyText = .cTopText + CRLF + CRLF + '<<report log follows>>' + CRLF + CRLF + .cBodyText

			CATCH TO loError

				.TrackProgress('There was an error.',LOGIT+SENDIT)
				.TrackProgress('The Error # is: ' + TRANSFORM(loError.ERRORNO), LOGIT+SENDIT)
				.TrackProgress('The Error Message is: ' + TRANSFORM(loError.MESSAGE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred in Program: ' + TRANSFORM(loError.PROCEDURE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred at line #: ' + TRANSFORM(loError.LINENO), LOGIT+SENDIT)
				lnNumberOfErrors = lnNumberOfErrors + 1

			ENDTRY

			WAIT CLEAR
			***************** INTERNAL email results ******************************
			.TrackProgress('==================================================================================================================', SENDIT)
			.TrackProgress('The logfile is: ' + .cLogFile, SENDIT)
			.TrackProgress('Kronos Misc Report process started: ' + .cStartTime, LOGIT+SENDIT)
			.TrackProgress('Kronos Misc Report process finished: ' + TTOC(DATETIME()), LOGIT+SENDIT)
			.TrackProgress('About to send status email.',LOGIT)

			IF .lSendInternalEmailIsOn THEN
				* try to trap error from not having dartmail dll's registered on user's pc...
				DO FORM dartmail2 WITH .cSendTo,.cFrom,.cSubject,.cCC,.cAttach,.cBodyText,"A"
				.TrackProgress('Sent status email.',LOGIT)
			ELSE
				.TrackProgress('Did not send status email: THIS.lSendInternalEmailIsOn = FALSE.',LOGIT)
			ENDIF

		ENDWITH
		RETURN
	ENDFUNC && main


	FUNCTION ExecSQL
		LPARAMETERS tcSQL, tcCursorName, tlNoDataReturnedIsOkay
		LOCAL llRetval, lnResult
		WITH THIS
			* close target cursor if it's open
			IF USED(tcCursorName)
				USE IN (tcCursorName)
			ENDIF
			WAIT WINDOW "Fetching remote data; please wait..." NOWAIT
			lnResult = SQLEXEC(.nSQLHandle, tcSQL, tcCursorName)
			llRetval = ( lnResult > 0 )
			IF llRetval THEN
				* see if any data came back
				IF NOT tlNoDataReturnedIsOkay THEN
					IF (NOT USED(tcCursorName)) OR EOF(tcCursorName) THEN
						llRetval = .F.
						.TrackProgress('No data was returned by this query: ' +CRLF + tcSQL, LOGIT+SENDIT)
					ENDIF
				ENDIF
			ELSE
				.TrackProgress("Result Code [" + TRANSFORM(lnResult) + "] returned by this query: " + tcSQL, LOGIT+SENDIT)
			ENDIF
			WAIT CLEAR
			RETURN llRetval
		ENDWITH
	ENDFUNC


	PROCEDURE TrackProgress
		* do any combination of Wait Window, writing to logfile, and adding to body of email,
		* based on nFlags parameter.
		LPARAMETERS tcExpression, tnFlags
		WITH THIS
			IF BITAND(tnFlags,LOGIT) = LOGIT THEN
				IF .lLoggingIsOn THEN
					?
					? .cProcessName + "  " + TTOC(DATETIME()) + ": " + tcExpression
				ENDIF
			ENDIF
			IF .lWaitWindowIsOn THEN
				IF BITAND(tnFlags,NOWAITIT) = NOWAITIT THEN
					WAIT WINDOW tcExpression NOWAIT
				ENDIF
				IF BITAND(tnFlags,WAITIT) = WAITIT THEN
					WAIT WINDOW tcExpression TIMEOUT .nWaitWindowTimeout
				ENDIF
			ENDIF
			IF BITAND(tnFlags,SENDIT) = SENDIT THEN
				IF .lSendInternalEmailIsOn THEN
					.cBodyText = .cBodyText + tcExpression + CRLF + CRLF
				ENDIF
			ENDIF
		ENDWITH
	ENDPROC  &&  TrackProgress

ENDDEFINE
