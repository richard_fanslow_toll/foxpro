**adjReceipt.prg
**
**Adjustment Receipt of all adjustment made with date range
**

xacctname=acctname(xaccountid)

cwhpath = wf(xoffice, xaccountid)
goffice = wf(xoffice, xaccountid,,,,,,.t.)

xsqlexec("select * from adj where mod='"+goffice+"' and accountid="+transform(xaccountid)+" and between(adjdt,{"+dtoc(xstartdt)+"},{"+dtoc(xenddt)+"}) and totqty#0 and offset=0",,,"wh")
select * from adj order by adjdt, style, color, id, pack into cursor csradj

cReportName = "Adjustment Receipt"

loadwebpdf("adjReceipt")

use in adj
use in csradj
