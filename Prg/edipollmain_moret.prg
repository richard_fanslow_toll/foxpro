CLOSE DATABASES ALL
CLEAR ALL
ON ERROR throw

WITH _SCREEN
	.AUTOCENTER = .T.
	.WINDOWSTATE = 0
	.BORDERSTYLE = 1
	.WIDTH = 260
	.HEIGHT = 210
	.LEFT = 100
	.TOP = 570
	.CLOSABLE = .F.
	.MAXBUTTON = .F.
	.MINBUTTON = .F.
	.SCROLLBARS = 0
	.CAPTION = "MORET POLLER - 945 ONLY"
ENDWITH
TRY
	DO m:\dev\prg\_setvars

	IF USED('e1')
		USE IN e1
	ENDIF

	SELECT 0
	USE F:\3pl\DATA\edipollertrip SHARED ALIAS emor
	REPLACE emor.moret WITH .F.
	USE IN emor

	IF USED('morlast')
		USE IN morlast
	ENDIF

	SELECT 0
	USE F:\3pl\DATA\last945time ALIAS morlast
	REPLACE checkflag WITH .T. FOR poller = "MORET"
	REPLACE tries WITH 0 FOR poller = "MORET"
	USE IN morlast
	
	SET SYSMENU OFF
	ON ESCAPE CANCEL
	DO FORM m:\dev\frm\edi_jobhandler_moret

CATCH TO oErr
	ASSERT .F. MESSAGE "AT CATCH SECTION"
	IF !lDoCatch
		tfrom    ="TGF EDI Processing Center <tgf-transload-ops@fmiint.com>"
		tsubject = "Moret 945 Poller Error ("+TRANSFORM(oErr.ERRORNO)+") at "+TTOC(DATETIME())
		tattach  = ""
		SELECT 0
		USE F:\3pl\DATA\mailmaster ALIAS mm
		LOCATE FOR edi_type = 'MISC' AND taskname = 'GENERAL'
		tsendto  = IIF(mm.use_alt,sendtoalt,sendto)
		tcc = IIF(mm.use_alt,ccalt,cc)
		tmessage = "Moret 945 Poller Top-level Error..... Please fix me........!"
		lcSourceMachine = SYS(0)

		tmessage =tmessage+ "Main Prog. Try/CATCH Exeception Message:"+CHR(13)+;
			[  Error: ] + STR(oErr.ERRORNO) +CHR(13)+;
			[  LineNo: ] + STR(oErr.LINENO) +CHR(13)+;
			[  Message: ] + oErr.MESSAGE +CHR(13)+;
			[  Procedure: ] + oErr.PROCEDURE +CHR(13)+;
			[  Details: ] + oErr.DETAILS +CHR(13)+;
			[  StackLevel: ] + STR(oErr.STACKLEVEL) +CHR(13)+;
			[  LineContents: ] + oErr.LINECONTENTS+CHR(13)+;
			[  UserValue: ] + oErr.USERVALUE+CHR(13)+;
			[  Computer:  ] +lcSourceMachine+CHR(13)+;
			"Program: edipollmain.prg"

		DO FORM m:\dev\FRM\dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
	ENDIF
	SELECT 0
	USE F:\3pl\DATA\edipollertrip SHARED ALIAS emor
	REPLACE emor.moret WITH .T.
FINALLY
	SET LIBRARY TO 
	CLOSE DATABASES ALL
	ON ERROR
	SET STATUS BAR ON
ENDTRY