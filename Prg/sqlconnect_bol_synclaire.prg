PARAMETERS nAcctNum,cCustName,cPPName,lUCC,cOffice,lSQL3,lMoret,lDoCartons,lDoScanpack
IF VARTYPE(cOffice) # "C"
	cOffice = "C"
ENDIF
csql = "tgfnjsql01"
cSQLPass = ""
*IF lTesting
*ENDIF
SELECT 0
USE F:\wh\sqlpassword
cSQLPass = ALLTRIM(sqlpassword.PASSWORD)
USE IN sqlpassword

SET ESCAPE ON
ON ESCAPE CANCEL
cFileOutName = "V"+TRIM(cPPName)+"pp"
nAcct = ALLTRIM(STR(nAcctNum))
IF USED("tempsyn")
	USE IN tempsyn
ENDIF

lAppend = .F.

*SET STEP ON 
SELECT sqlptbcny
nPTRecs = RECCOUNT()
LOCATE
nct = 0
*!* Scans through all PTs within the OUTSHIP BOL# and creates correct SQL table extract for each

WAIT WINDOW "Now processing "+cCustName+" SQL view...please wait" NOWAIT
SCAN
	lScanpack = sqlptbcny.scanpack
	lcDSNLess="driver=SQL Server;server=tgfnjsql01;uid=SA;pwd=&cSQLPass;DATABASE=PICKPACK"

	nHandle=SQLSTRINGCONNECT(m.lcDSNLess,.T.)
	SQLSETPROP(0,'DispLogin',3)
	SQLSETPROP(0,"dispwarnings",.F.)

	IF nHandle<1 && bailout
		SET STEP ON
		WAIT WINDOW "Could not make SQL connection" TIMEOUT 3
		cRetMsg = "NO SQL CONNECT"
		RETURN cRetMsg
		THROW
	ENDIF

	cSR = ALLT(sqlptbcny.ship_ref)
	nct = nct+1

*	ASSERT .f. MESSAGE "In Labels selection"
	lDoCartons = IIF(cBOL='04907316521346957',.f.,lDoCartons)
	IF lTestinput OR !lDoCartons
		WAIT WINDOW "SQL Records will be selected from LABELS" nowait
		WAIT clear
		WAIT WINDOW "At PT# "+TRANSFORM(nct)+" of "+TRANSFORM(nptrecs) NOWAIT noclear
		lcQ1=[SELECT * FROM dbo.labels Labels]
		lcQ2 = [ WHERE Labels.ship_ref = ]
		lcQ3 = " '&cSR' "
		lcQ4 = [AND Labels.accountid = 6521]
		IF lUCC
			lcQ6 = [order by Labels.ship_ref,Labels.outshipid,Labels.ucc]
		ELSE
			lcQ6 = [order by Labels.ship_ref,Labels.outshipid,Labels.CartonNum]
		ENDIF
		lcsql = lcQ1+lcQ2+lcQ3+lcQ4+lcQ6

	else
		WAIT WINDOW "At PT# "+TRANSFORM(nct)+" of "+TRANSFORM(nptrecs) NOWAIT noclear

		if usesql()
			if lucc
				xorderby="order by ship_ref, outshipid, ucc"
			else
				xorderby="order by ship_ref, outshipid, cartonnum"
			endif
			
			if xsqlexec("select * from cartons where ship_ref='"+csr+"' and ucc#'CUTS' "+xorderby,cFileOutName,,"pickpack") = 0
				ASSERT .F. MESSAGE "At NO RECORDS statement in SQLConnect"
				WAIT WINDOW "No "+cCustName+" records found" TIMEOUT 2
				cRetMsg = "NO SQL CONNECT"
				RETURN cRetMsg
			endif

		else
			lcQ1 = [SELECT * FROM dbo.cartons Cartons]
			lcQ2 = [ WHERE Cartons.ship_ref = ]
			lcQ3 = " '&cSR' "
			lcQ4 = [ AND  Cartons.ucc <> ]
			lcQ5 = " 'CUTS' "
			IF lUCC
				lcQ6 = [order by Cartons.ship_ref,Cartons.outshipid,Cartons.ucc]
			ELSE
				lcQ6 = [order by Cartons.ship_ref,Cartons.outshipid,Cartons.CartonNum]
			ENDIF

			lcsql = lcQ1+lcQ2+lcQ3+lcQ4+lcQ5+lcQ6
		endif
	ENDIF

	if usesql()
	else
		llSuccess=SQLEXEC(nHandle,lcsql,cFileOutName)

		IF llSuccess<1  && no records -1 indicates no records and 1 indicates records found
			SQLCANCEL(nHandle)
			SQLDISCONNECT(nHandle)
			ASSERT .F. MESSAGE "At NO RECORDS statement in SQLConnect"
			WAIT WINDOW "No "+cCustName+" records found" TIMEOUT 2
			cRetMsg = "NO SQL CONNECT"
			RETURN cRetMsg
		ENDIF
		SQLCANCEL(nHandle)
	endif
	
	SELECT &cFileOutName
	IF lAppend = .F.
		lAppend = .T.
		IF lTesting
			LOCATE
*BROWSE
		ENDIF
		COPY TO ("F:\3pl\DATA\tempsyn")
		USE ("F:\3pl\DATA\tempsyn") IN 0 ALIAS tempsyn
	ELSE
		LOCATE
		SCAN
			SCATTER MEMVAR
			INSERT INTO tempsyn FROM MEMVAR
		ENDSCAN
	ENDIF
ENDSCAN
IF USED(cFileOutName)
	USE IN &cFileOutName
ENDIF

SELECT tempsyn
LOCATE
IF nAcctNum = 6521
	IF lTestSplits
		REPLACE tempsyn.ship_ref WITH STRTRAN(tempsyn.ship_ref,"~","%") ALL IN tempsyn
	ENDIF
ENDIF

IF lUCC
	SELECT * FROM tempsyn ;
		ORDER BY ship_ref,outshipid,ucc ;
		INTO CURSOR &cFileOutName READWRITE
ELSE
	SELECT * FROM tempsyn ;
		ORDER BY ship_ref,outshipid,cartonnum ;
		INTO CURSOR &cFileOutName READWRITE
ENDIF
IF lTesting
	SELECT &cFileOutName
*BROWSE
ENDIF

USE IN tempsyn
USE IN sqlptbcny
GO BOTT
WAIT CLEAR
*WAIT "" TIMEOUT 3
SQLDISCONNECT(nHandle)

cRetMsg = "OK"
RETURN cRetMsg
