*!* m:\dev\prg\grandvoyage940_process.prg

CD &lcPath

LogCommentStr = ""

delimchar = cDelimiter
lcTranOpt= cTranslateOption

lnNum = ADIR(tarray,"*.txt")

IF lnNum = 0
	WAIT WINDOW AT 10,10 "      No "+cCustname+" 940s to import     " TIMEOUT 3
	NormalExit = .T.
	THROW
ENDIF

FOR thisfile = 1  TO lnNum
	Xfile = lcPath+tarray[thisfile,1] && +"."
	cfilename = ALLTRIM(LOWER(tarray[thisfile,1]))
	nFileSize = VAL(TRANSFORM(tarray(thisfile,2)))
	archivefile  = (lcArchivepath+cfilename)
	IF !lTesting
*		COPY FILE [&xfile] TO ("F:\ftpusers\SteelSeries\Translate\"+cfilename) && to create 997
	ENDIF
	WAIT WINDOW "Importing file: "+cfilename NOWAIT
*	DO m:\dev\prg\loadedifile WITH Xfile,delimchar,lcTranOpt,cCustname
	DO m:\dev\prg\createx856a
	APPEND FROM [&xfile] TYPE DELIMITED WITH CHARACTER "*"

	IF lTesting
*brow
	ENDIF

	SELECT x856
	LOCATE
	LOCATE FOR x856.segment = 'GS'
	IF x856.f1 = "FA"
		WAIT WINDOW "This is a 997 FA file...moving to correct folder" TIMEOUT 2
		cfile997in = ("F:\ftpusers\"+cCustname+"\997in\"+cfilename)
		COPY FILE [&xfile] TO [&cfile997in]
		DELETE FILE [&xfile]
		LOOP
	ELSE
		LOCATE
	ENDIF

	DO create_pt_cursors WITH cUseFolder,lLoadSQL && Added to prep for SQL Server transition

	lOK = .T.
	DO ("m:\dev\prg\grandvoyage940_bkdn")
	IF lOK && If no divide-by-zero error
		DO "m:\dev\prg\all940_import"
	ENDIF

	IF !FILE(archivefile)
		COPY FILE [&Xfile] TO [&archivefile]
	ENDIF
	IF FILE(Xfile)
		DELETE FILE [&xfile]
	ENDIF
	
	*release_ptvars()

NEXT thisfile

&& now clean up the 940in archive files, delete for the upto the last 10 days
deletefile(lcarchivepath,IIF(DATE()<{^2016-05-01},20,10))

WAIT CLEAR

WAIT "Entire "+cMailname+" 940 Process Now Complete" WINDOW TIMEOUT 5
NormalExit = .T.
