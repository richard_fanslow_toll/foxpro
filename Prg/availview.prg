* creates vavail view - MUST BE RUN IN VFP 8.0 !!

set enginebehavior 70
set century off
set exclusive off
set safety off
close databases all

xsqlexec("select * from account where inactive=0","account",,"qq")
index on accountid tag accountid
set order to

*open database m:\dev\wodata\wo
open database f:\wo\wodata\wo

create sql view "vavail" as select *, padr(acctname(accountid),30) as acctname from wo!availlog where &xfilter

DBSetProp('vavail', 'View', 'UpdateType', 1)
DBSetProp('vavail', 'View', 'WhereType', 1)
DBSetProp('vavail', 'View', 'FetchMemo', .T.)
DBSetProp('vavail', 'View', 'SendUpdates', .f.)
DBSetProp('vavail', 'View', 'UseMemoSize', 255)
DBSetProp('vavail', 'View', 'FetchSize', 100)
DBSetProp('vavail', 'View', 'MaxRecords', -1)
DBSetProp('vavail', 'View', 'Tables', 'wo!availlog')
DBSetProp('vavail', 'View', 'Prepared', .F.)
DBSetProp('vavail', 'View', 'CompareMemo', .T.)
DBSetProp('vavail', 'View', 'FetchAsNeeded', .F.)
DBSetProp('vavail', 'View', 'FetchSize', 100)
DBSetProp('vavail', 'View', 'Comment', "")
DBSetProp('vavail', 'View', 'BatchUpdateCount', 1)
DBSetProp('vavail', 'View', 'ShareConnection', .F.)

DBSetProp('vavail.availlogid', 'Field', 'KeyField', .T.)
DBSetProp('vavail.availlogid', 'Field', 'Updatable', .T.)

gunshot()