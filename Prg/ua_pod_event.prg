Parameters eventcode,IBD,ponum,asnnum,offset

If Inlist(Type("offset"),"U","L")
  offset = 0000000  && 86400 = one day
Else
  offset= offset*86400
Endif

If Inlist(eventcode,"2800") && these events trigger by IBD or ASN
  lcIBD= IBD &&"6000026365"
  lcASN= asnnum &&"6000026365"
  lcCode = eventcode
Endif

Set Date YMD
lcTStamp = Strtran(Ttoc(Datetime()),"/","-")
lcTStamp = Strtran(lcTStamp," ","T")
lcTStamp = Strtran(lcTStamp,"TPM","")
lcTStamp = Alltrim(Strtran(lcTStamp,"TAM",""))

* 100000

lcEventTime = Strtran(Ttoc(Datetime()+offset),"/","-")
lcEventTime = Strtran(lcEventTime," ","T")
lcEventTime = Strtran(lcEventTime,"TPM","")
lcEventTime = Alltrim(Strtran(lcEventTime,"TAM",""))

lcHeader = Filetostr("f:\underarmour\xmltemplates\new_pod_event.txt")

lcHeader = Strtran(lcHeader,"<DOCDATETIME>",lcTStamp)
lcHeader = Strtran(lcHeader,"<IBDNUMBER>",lcIBD)
lcHeader = Strtran(lcHeader,"<EVENTDATETIME>",lcEventTime)
lcHeader = Strtran(lcHeader,"<EVENTLOCATION>","TOLLMIA")
lcHeader = Strtran(lcHeader,"<EVENTCODE>",lcCode) &&lcEventCodeUsed)
*!* For 214 file types:

lcFilestamp = Ttoc(Datetime())
lcFilestamp = Strtran(lcFilestamp,"/","")
lcFilestamp = Strtran(lcFilestamp,":","")
lcFilestamp = Strtran(lcFilestamp," ","")

Strtofile(lcHeader,"f:\ftpusers\underarmour\outbound\out2\LSPPOD_"+lcIBD+"_"+lcCode+"_"+lcFilestamp+".xml")
