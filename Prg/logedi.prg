parameters lnwo_num,lnacctnum,lcacctname,lcfilename,lcprogram,lcoffice,lctuserid

if !used("edilog")
*	use f:\fmiedi\edilog in 0

	if !file("h:\fox\edilog.dbf")
		use f:\fmiedi\edilog in 0
		xselect=select()
		select edilog
		copy stru to h:\fox\edilog.dbf
		use in edilog
		select (xselect)
	endif
	use h:\fox\edilog in 0
endif

append blank in edilog

replace wo_num    with lnwo_num, ;
		acct_num   with lnacctnum, ;
		acct_name  with lcacctname, ;
		filename  with lcfilename, ;
		program   with lcprogram, ;
		office    with lcoffice, ;
		tuserid   with lctuserid, ;
		created   with datetime() in edilog

use in edilog

