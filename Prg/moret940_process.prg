ASSERT .F. MESSAGE "At start of PROCESS phase"

lNewPT = .T.

cFA997Path   = ("F:\FTPUSERS\Moret\940translate\")
c997inPath   = ("F:\FTPUSERS\Moret"+cOutState+"\997IN\")
c888inPath   = ("F:\moret\skumaster\")
CD &lcPath

waitstr = "Processing records for "+cMailName
WAIT WINDOW waitstr TIMEOUT 2

lnNum = ADIR(tarray,cFilemask)

IF lnNum = 0
	WAIT WINDOW AT 10,20 "No "+cMailName+" 940's to import for Moret accounts" TIMEOUT 3
	RETURN
ENDIF

ASSERT .F. MESSAGE "At process looping"
*SET STEP ON
FOR thisfile = 1  TO lnNum
	lLoop = .F.
	cFilename = ALLTRIM(tarray[thisfile,1])
	nFileSize = VAL(TRANSFORM(tarray(thisfile,2)))
	cCoNum = LEFT(cFilename,2)
	xfile = lcPath+TRIM(tarray[thisfile,1])
	archivefile = lcArchivePath+TRIM(tarray[thisfile,1])
	fa997file = cFA997Path+ALLTRIM(tarray[thisfile,1])
	in997file = c997inPath+ALLTRIM(tarray[thisfile,1])
	in888file = c888inPath+ALLTRIM(tarray[thisfile,1])

	IF "_997_"$cFilename
		COPY FILE [&xfile] TO [&in997file]
		DELETE FILE [&xfile]
		LOOP
	ENDIF

	IF JUSTEXT(cFilename) = "888"
		COPY FILE [&xfile] TO [&in888file]
		DELETE FILE [&xfile]
		LOOP
	ENDIF

	WAIT WINDOW "Importing file: "+xfile TIMEOUT 2
	IF FILE(xfile)
		RELEASE ALL LIKE APT
		RELEASE ALL LIKE APTDET

		CREATE CURSOR tempsplits (ship_ref c(20),mix l)
		INDEX ON ship_ref TAG ship_ref
		DO m:\dev\prg\createx856a
		cDelimfile = FILETOSTR(xfile)
		cDelimiter = SUBSTR(cDelimfile,4,1)
		RELEASE cDelimfile
		DO m:\dev\prg\loadedifile WITH xfile,cDelimiter,"MORET-EDI","MORET" && Comment this and uncomment next two lines to load pre-converted 940s
*		append from &xfile type delim with char "*"
		SELECT x856
		LOCATE
		IF lTesting AND lBrowfiles
* BROWSE
		ENDIF
		SKIP
		IF ALLTRIM(x856.segment)="GS" AND ALLTRIM(x856.f1) # "OW"
			COPY FILE [&xfile] TO [&in997file]
			DELETE FILE [&xfile]
			LOOP
		ENDIF
		LOCATE

		DO create_pt_cursors WITH cUseFolder,lLoadSQL && Added to prep for SQL Server transition

*!* End of added code

		DO (cUseDir+"BKDN") WITH cFilename
		IF lLoop
			LOOP
		ENDIF

		IF !lTesting
			COPY FILE [&xfile] TO [&fa997file]
		ENDIF

		DO m:\dev\prg\ALL940_IMPORT
		SELECT xpt
		INDEX on ptid TAG ptid

	ENDIF

*	release_ptvars()

NEXT thisfile

&& now clean up the 940in archive files, delete for the upto the last 10 days
deletefile(lcArchivePath,IIF(DATE()<{^2016-05-01},20,10))

WAIT CLEAR
WAIT WINDOW "ALL "+cMailName+" 940 files finished processing for: "+cOfficename+"..." TIMEOUT 3

*************************************************************************************************
