* a revision of ftp_gtfiles which adds support for Secure FTP protocols, non-default datachannels * port #s, etc.
*
* Author: MB 2/14/2018
*
* This is intended to be run on FTP2, which does support some secure ftp protocols, but seemingly not as many as CuteFTP 9, which we have on FTP1.
* So if you have trouble using this with a secure protocol on FTP2, instead try using sftp_download on FTP1.

* downloads files from remote ftp clients based on transer info in ftsetup.dbf

* EXE = F:\EDIROUTING\PROJ\FTP_DOWNLOAD.EXE

LPARAMETERS tcTransfer

#DEFINE crlf CHR(13) + CHR(10)

SET EXCL OFF
SET TALK OFF
SET SAFETY OFF
SET DELE ON

TRY
	utilsetup("FTP_DOWNLOAD")

	PUBLIC lnftptries

	LOCAL loerror, lcattach, lcsendto, lcsubject, lcmessage, lcfrom, mysite, llDeleteDownloadedFile
	LOCAL lcTransfer, lcUserMessage, lcProtocol, lcDataChan, lnPort, lcMask, lnRetVal, lnColonPosition, lcListingFileName

	LOCAL gnhandle, gcstring, xfilename, xleft, xright, llautorename, fname

	lcTransfer = tcTransfer

	lcUserMessage = ""
	lcsendto = 'mbennett@fmiint.com'

	IF EMPTY(tcTransfer) THEN
		lcUserMessage = "EMPTY tcTransfer parameter!"
		THROW
	ENDIF



	*!*	lcUserMessage = "throw test!"
	*!*	THROW

	lnftptries = 0
	DO m:\dev\prg\_setvars WITH .T.

	*!*		_SCREEN.CAPTION = "Executing FTP_Download.EXE (" + ALLTRIM(lcTransfer) + ")"

	WAIT WINDOW "FTP_DOWNLOAD:  job = " + ALLTRIM(lcTransfer) NOWAIT


	*First specify a variable called Mysite
	LOCAL mysite
	CREATE CURSOR filelist(fname c(50))

	IF !USED("ftpsetup")
		USE F:\edirouting\ftpsetup IN 0
	ENDIF

	* wait window at 10,10 "params = "+lcTransfer+"  "+tuserid

	SELECT ftpsetup
	LOCATE FOR UPPER(ALLTRIM(ftpsetup.transfer)) == UPPER(ALLTRIM(lcTransfer))

	IF !FOUND() THEN
		lcUserMessage = "This transfer "+lcTransfer+" is not defined in f:\edirouting\ftpsetup.dbf.... Aborting!"
		THROW
	ENDIF

	lcsendto = ALLTRIM(ftpsetup.EMAIL_ADDR)

	dpath = ALLTRIM(ftpsetup.localpath)
	IF !DIRECTORY(dpath)
		lcUserMessage = "This directory "+dpath+" does not exist... Aborting!"
		THROW
	ENDIF


	SET DEFAULT TO &dpath
	lcpath             = ALLTRIM(ftpsetup.localpath)
	lcarchivepath      = ALLTRIM(ftpsetup.archive)
	**llchkarchive: if need to check the archive to be sure same file was not downloaded and processed on a previous run - mvw 04/27/11
	llchkarchive       = IIF(ftpsetup.chkarchive AND !EMPTY(lcarchivepath),.T.,.F.)
	**llautorename: transfers as .tmp file first, then renames to correct file ext (to avoid incomplete file transfers) - mvw 04/27/11
	llautorename	   = ftpsetup.autorename
	lcremotefolder     = ALLTRIM(ftpsetup.rfolder)
	lcaccountname      = ALLTRIM(ftpsetup.account)
	lceditype          = ALLTRIM(ftpsetup.editype)
	lcthissite         = ALLTRIM(ftpsetup.site)
	lcthislogin        = ALLTRIM(ftpsetup.login)
	lcthispassword     = ALLTRIM(ftpsetup.PASSWORD)

	lcDataChan         = UPPER(ALLTRIM(ftpsetup.datachan))
	lcProtocol         = UPPER(ALLTRIM(ftpsetup.protocol))
	lnPort             = ftpsetup.port
	lcMask 			   = ALLTRIM(ftpsetup.filemask)
	_SCREEN.CAPTION    = "FTP_DOWNLOAD: " + lcTransfer

	llDeleteDownloadedFile = .F.   && ftpsetup.delgotfile  


	*!*			_SCREEN.CAPTION    = ftpsetup.MESSAGE



	*lcTransfer = ALLTRIM(lcthissite)

	WAIT WINDOW "FTP_DOWNLOAD: Now Connecting to ----> "+lcthissite NOWAIT

	*Creating a connection object and assign it to the variable
	mysite = CREATEOBJECT("CuteFTPPro.TEConnection")

	IF NOT EMPTY(lcProtocol) THEN
		mysite.protocol = lcProtocol
	ELSE
		mysite.protocol = "FTP"
	ENDIF

	mysite.HOST     = lcthissite
	mysite.login    = lcthislogin
	mysite.PASSWORD = lcthispassword
	mysite.useproxy = "BOTH"

	IF NOT EMPTY(lcDataChan) THEN
		mysite.datachannel = lcDataChan
	ENDIF

	IF lnPort > 0 THEN
		mysite.port = lnPort
	ENDIF

	IF EMPTY(lcMask) THEN
		lcUserMessage = "FTP_DOWNLOAD: ERROR: empty filemask in FTPSETUP - ABORTING!"
		WAIT WINDOW lcUserMessage TIMEOUT 2
		THROW
	ENDIF


	* now connect
	mysite.CONNECT

	lcmessage = mysite.WAIT(-1,120000)

	IF !ISBLANK(lcmessage)
		WAIT WINDOW "FTP_DOWNLOAD: Transfer message "+lcmessage TIMEOUT 2
	ENDIF

	IF mysite.isconnected >=0 THEN
		lcUserMessage = "Could not connect to: " + mysite.HOST + "...Aborting!"
		THROW
	ELSE
		WAIT WINDOW ("FTP_DOWNLOAD: now connected to " + mysite.HOST) NOWAIT
	ENDIF

	IF !USED("ftpedilog")
		USE F:\edirouting\ftpedilog IN 0 ALIAS ftpedilog
	ENDIF

	SELECT ftpedilog

	WAIT WINDOW "FTP_DOWNLOAD: Changing to folder.........."+lcremotefolder NOWAIT

	IF EMPTY(lcremotefolder) THEN
		WAIT WINDOW "FTP_DOWNLOAD: Logged into home folder" NOWAIT
	ELSE
		WAIT WINDOW "FTP_DOWNLOAD: Changing to FTP site folder.........  "+lcremotefolder NOWAIT
		b = mysite.remoteexists(lcremotefolder)
		IF b>=0 THEN
			lcUserMessage = "!!!!!!!!! ERROR: Remote Folder not found: "+lcremotefolder
			THROW
		ELSE
			WAIT WINDOW "FTP_DOWNLOAD: Just changed to folder......->  "+lcremotefolder TIMEOUT 2
			WAIT "" TIMEOUT 2
			mysite.remotefolder = TRIM(lcremotefolder)
		ENDIF
	ENDIF

	mysite.localfolder = lcpath
	*mysite.fileoverwritemethod = "OVERWRITE"  && this property does nto exist in cuteFTP 6


	IF "*" $ lcMask THEN

		* wildcard specified, download multiple files



		*!*			SELECT ftpedilog
		*!*			APPEND BLANK
		*!*			REPLACE ftpedilog.ftpdate WITH DATETIME(), ftpedilog.filename WITH lcMask, ftpedilog.acct_name WITH lcaccountname, ;
		*!*				ftpedilog.TYPE WITH lceditype, ftpedilog.xfertype WITH "GET", ftpedilog.transfer WITH lcTransfer ;
		*!*				IN ftpedilog

		*!*		ENDIF  &&  lnRetVal >= 0

		*mysite.getlist("","c:\tempfox\tempdir.txt","%NAME")

		mysite.getlist("","c:\tempfox\tempdir.txt")  && complete listing will give us the info to identify and exclude folders, altho parsing to get filenames will be harder

		gnhandle = FOPEN('c:\tempfox\tempdir.txt')
		IF gnhandle = -1
			*MESSAGEBOX("Can't open file [c:\tempfox\tempdir.txt]")
			lcUserMessage = "Can't open file [c:\tempfox\tempdir.txt]"
			WAIT WINDOW "FTP_DOWNLOAD: " + lcUserMessage TIMEOUT 2
			THROW
		ENDIF

		DO WHILE !FEOF(gnhandle)
			gcstring = FGETS(gnhandle)
			gcstring = ALLTRIM(STRTRAN(gcstring,CHR(0),""))  && we seem to be getting unicode chars; remove the null characters
			IF UPPER(LEFT(gcstring,1)) = "D" THEN
				* this line is a Folder; skip it
			ELSE
				* not a folder; parse to get filename
				lnColonPosition = RAT(":",gcstring)  && get the position of the colon in the timestamp part of the directory listing
				* filename part of dir listing should start at lnColonPosition + 4
				lcListingFileName = SUBSTR(gcstring,lnColonPosition + 4)
				SELECT filelist
				APPEND BLANK
				REPLACE filelist.fname WITH ALLTRIM(lcListingFileName)
			ENDIF
		ENDDO
		FCLOSE(gnhandle)

*!*		SELECT filelist
*!*		COPY TO F:\UTIL\filelist
*!*	*THROW

		SELECT filelist
		LOCATE
		IF EOF() THEN
			WAIT WINDOW "FTP_DOWNLOAD: No files found for download in " + lcremotefolder TIMEOUT 4
		ENDIF

		*lcmask = alltrim(ftpsetup.filemask)
		IF NOT lcMask == "*.*" THEN
			SELECT filelist
			SCAN
				xfilename=ALLTRIM(UPPER(filelist.fname))
				**added wildcard logic - mvw 03/23/10
				DO CASE
					CASE OCCURS("*",lcMask)=1
						xleft=UPPER(LEFT(lcMask,AT("*",lcMask,1)-1))
						xright=UPPER(RIGHT(lcMask,LEN(lcMask)-AT("*",lcMask,1)))
						IF xfilename#xleft OR RIGHT(xfilename,LEN(xright))#xright
							WAIT WINDOW AT 10,10 "looking for files like... "+lcMask+"  ....deleting file..........  "+filelist.fname timeout 1
							DELETE
						ENDIF

					CASE !AT(UPPER(lcMask), xfilename) > 0
						WAIT WINDOW AT 10,10 "looking for files like... "+lcMask+"  ....deleting file..........  "+filelist.fname timeout 1
						DELETE
				ENDCASE
			ENDSCAN
		ENDIF  &&  NOT lcMask == "*.*"

		SELECT filelist
		SCAN
			xfile = lcpath+ALLTRIM(fname)
			DO CASE
				CASE FILE(xfile)
					WAIT WINDOW "File "+xfile+" already exists... Deleting in order to download new file." TIMEOUT 2
					DELETE FILE "&xfile"
					**if one of my processes, check to make sure this file has not already been downloaded and processed - mvw 04/27/11
				CASE llchkarchive AND FILE(lcarchivepath+ALLTRIM(fname))
					*			if messagebox("File "+xfile+"  was already downloaded and processed on a previous run! Delete it from server?",4))=6
					mysite.remoteremove(ALLTRIM(fname))
					LOOP
					WAIT WINDOW "File "+xfile+"  was already downloaded and processed on a previous run! File removed from remote server and skipped..." TIMEOUT 2
					*			endif
				OTHERWISE
					* nothing
			ENDCASE

			**download first to a temp file then rename in case connection drops - mvw 04/27/11
			IF llautorename
				xtmpfile = SUBSTR(xfile,1,RAT(".",xfile))+"tmp"
				IF FILE(xfile)
					WAIT WINDOW "Temp file "+xtmpfile+" already exists... Deleting in order to download new file." TIMEOUT 2
					DELETE FILE "&xtmpfile"
				ENDIF
			ENDIF

			WAIT WINDOW AT 10,10 CHR(13)+ "*Now downloading "+xfile+"   "+CHR(13) NOWAIT

			**download to xtmpfile (temp file) - mvw 04/24/11
			IF llautorename
				mysite.download(ALLTRIM(fname),xtmpfile)
			ELSE
				mysite.download(ALLTRIM(fname),xfile)
			ENDIF

			lcmessage = mysite.WAIT(-1,60000)

			WAIT WINDOW AT 10,10 CHR(13)+"    Transfer message "+lcmessage+"   "+CHR(13) TIMEOUT 2

			DO CASE
				CASE llautorename AND !FILE(xtmpfile)
					WAIT WINDOW AT 10,10 CHR(13)+xtmpfile+"  did not transfer properly !!!!!!!!!!!! "+CHR(13) TIMEOUT 2
				CASE !llautorename AND !FILE(xfile)
					WAIT WINDOW AT 10,10 CHR(13)+xfile+"  did not transfer properly !!!!!!!!!!!! "+CHR(13) TIMEOUT 2
				OTHERWISE
					WAIT "" TIMEOUT 2
					**rename xtmpfile (temp file) to xfile - mvw 04/24/11
					IF llautorename
						RENAME "&xtmpfile" TO "&xfile"
					ENDIF

					fname = ALLTRIM(fname)
					IF llDeleteDownloadedFile THEN
						mysite.remoteremove(ALLTRIM(fname))
					ENDIF

			ENDCASE

			SELECT ftpedilog
			APPEND BLANK
			REPLACE ftpedilog.ftpdate WITH DATETIME(), ftpedilog.filename WITH xfile, ftpedilog.acct_name WITH lcaccountname, ;
				ftpedilog.TYPE WITH lceditype, ftpedilog.xfertype WITH "GET", ftpedilog.transfer WITH lcTransfer ;
				IN ftpedilog

		ENDSCAN

	ELSE
		* single file download, use simple logic

		*mysite.getlist("","c:\tempfox\tempdir.txt")  && FOR TESTING

		* see if file to be downloaded exists
		lnRetVal = mysite.remoteexists(lcMask)
		IF lnRetVal >= 0 THEN
			lcUserMessage = "!!!!!!!!!!!!!!! ERROR: Download File [" + lcMask + "] does not exist!"
			WAIT WINDOW NOWAIT lcUserMessage
			THROW

		ENDIF

		* OK - DOWNLOAD IT

		WAIT WINDOW NOWAIT "In FTP_DOWNLOAD: Downloading for file mask = " + lcMask + "...."

		mysite.download(lcMask)

		lcmessage = mysite.WAIT(-1,60000)

		WAIT WINDOW NOWAIT "FTP_DOWNLOAD: Transfer message = "+lcmessage

		WAIT WINDOW "FTP_DOWNLOAD: FTP Complete........." TIMEOUT 2

		* DELETE THE REMOTE FILE IF WE GOT IT LOCALLY
		IF FILE(lcpath + lcMask) THEN
			IF llDeleteDownloadedFile THEN
				WAIT WINDOW "FTP_DOWNLOAD: Deleting remote file...." + lcMask + "...." TIMEOUT 2
				mysite.remoteremove(ALLTRIM(lcMask))
			ENDIF
		ELSE
			WAIT WINDOW "FTP_DOWNLOAD: Local downloaded file not found!" TIMEOUT 2
		ENDIF

		SELECT ftpedilog
		APPEND BLANK
		REPLACE ftpedilog.ftpdate WITH DATETIME(), ftpedilog.filename WITH lcMask, ftpedilog.acct_name WITH lcaccountname, ;
			ftpedilog.TYPE WITH lceditype, ftpedilog.xfertype WITH "GET", ftpedilog.transfer WITH lcTransfer ;
			IN ftpedilog

	ENDIF && "*" $ lcMask


	mysite.CLOSE
	*mysite.CLOSE("EXITNOPENDING")

	WAIT CLEAR

CATCH TO loerror
	lcattach = ''
	lcfrom = "mark.bennett@tollgroup.com"
	IF EMPTY(lcsendto) THEN
		lcsendto = "mbennett@fmiint.com"
	ENDIF
	lcsubject = "FTP Download Error during Connection To: " + lcTransfer
	lcmessage = "The FTP process encountered an error."
	lcmessage = lcmessage + crlf + 'The Error # is: ' + TRANSFORM(loerror.ERRORNO)
	lcmessage = lcmessage + crlf + 'The Error Message is: ' + TRANSFORM(loerror.MESSAGE)
	lcmessage = lcmessage + crlf + 'The Error occurred in Program: ' + TRANSFORM(loerror.PROCEDURE)
	lcmessage = lcmessage + crlf + 'The Error occurred at line #: ' + TRANSFORM(loerror.LINENO)
	lcmessage = lcmessage + crlf + crlf + 'The User Error Message = ' + lcUserMessage
	DO FORM dartmail2 WITH lcsendto, lcfrom, lcsubject, " ", lcattach, lcmessage,"A"

FINALLY

	*CLOSE DATABASES ALL
	schedupdate()

ENDTRY

RETURN