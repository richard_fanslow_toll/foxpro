*!* m:\dev\prg\rowone_bkdn.prg
Clear
lCheckStyle = .T.
lPrepack = .T.
m.color=""
cUCCNumber = ""
cUsePack = ""
lcUCCSeed = 1

lBrowfiles = lTesting
*lBrowfiles = .T.

Select x856
Count For Trim(segment) = "ST" To nSegCnt
cSegCnt = Alltrim(Str(nSegCnt))
Select x856
Locate

lTestrun = .F.
ptctr = 0

Wait Window "Now in 940 Breakdown" Timeout 1

Wait "There are "+cSegCnt+" P/Ts in this file" Window Timeout 1
Select x856
Locate

*!* Constants
m.acct_name = "ROW ONE BRANDS, INC."
Store nAcctNum To m.acct_num
m.careof = " "
m.sf_addr1 = "661 HIGHLAND AVE - SUITE 104"
m.sf_csz = "NEEDHAM, MA 02494"
m.printed = .F.
m.print_by = ""
m.printdate = Date()
*!* End constants

*!*	IF !USED("isa_num")
*!*		IF lTesting
*!*			USE F:\3pl\test\isa_num IN 0 ALIAS isa_num
*!*		ELSE
*!*			USE F:\3pl\DATA\isa_num IN 0 ALIAS isa_num
*!*		ENDIF
*!*	ENDIF

*!*	IF !USED("uploaddet")
*!*		IF lTestrun
*!*			USE F:\ediwhse\test\uploaddet IN 0 ALIAS uploaddet
*!*		ELSE
*!*			USE F:\ediwhse\DATA\uploaddet IN 0 SHARED ALIAS uploaddet
*!*		ENDIF
*!*	ENDIF

Select xpt
Delete All
Select xptdet
Delete All

Store 0 To pt_total_eaches
Store 0 To pt_total_cartons
Store 0 To nCtnCount

Select x856
Set Filter To
Locate

Store "" To cisa_num

Wait Window "NOW IN INITAL HEADER BREAKDOWN" Timeout 1
Wait Window "NOW IN INITAL HEADER BREAKDOWN" Nowait
*ASSERT .F. MESSAGE "At header bkdn...debug"
Do While !Eof("x856")
	cSegment = Trim(x856.segment)
	cCode = Trim(x856.f1)

	If Trim(x856.segment) = "ISA"
		cisa_num = Alltrim(x856.f13)
		Wait Window "This is a "+cUseName+" upload." Timeout 2
		ptctr = 0
		Select x856
		Skip
		Loop
	Endif

	If Trim(x856.segment) = "GS"
		cgs_num = Alltrim(x856.f6)
		Select x856
		Skip
		Loop
	Endif

	If Trim(x856.segment) = "ST" And Trim(x856.f1) = "940"
		Select x856
		Skip
		Loop
	Endif

	If Trim(x856.segment) = "W05"  && new shipment
		lWomens = .F.
**** Added 5/12/17 TMARG
		lmens = .F.
		nCtnCount = 0
		m.style = ""
		Select xpt
		nDetCnt = 0
		Append Blank

		Replace xpt.shipins With xpt.shipins+"FILENAME*"+cFilename In xpt
		Replace xpt.qty With 0 In xpt
		Replace xpt.origqty With 0 In xpt
		Store 0 To pt_total_eaches
		Store 0 To pt_total_cartons
		ptctr = ptctr +1
		Wait Window At 10,10 "Now processing PT# "+ Alltrim(x856.f2)+" Number "+Chr(13)+Transform(ptctr)+" of "+cSegCnt Nowait

		Replace xpt.ptid       With ptctr In xpt
*		REPLACE xpt.accountid  WITH nAcctNum IN xpt
*		REPLACE xpt.office     WITH cOffice && "N"
		Replace xpt.ptdate     With Date() In xpt
		m.ship_ref = Alltrim(x856.f2)
		cship_ref = Alltrim(x856.f2)
		If  cship_ref = '0011424'
			Assert .F. Message "At selected PT...DEBUG"
		Endif
		Replace xpt.ship_ref   With m.ship_ref In xpt  && Pick Ticket
		m.pt = Alltrim(x856.f2)
		If lTestrun
			Replace xpt.cacctnum   With "999" In xpt
		Endif
		Select x856
		Skip
		Loop
	Endif

	If Trim(x856.segment) = "N1" And Trim(x856.f1) = "DE" && DUNS data
		If Trim(x856.f3) = "9"
			m.duns = Trim(x856.f4)
		Endif
	Endif

	If Trim(x856.segment) = "N1" And Trim(x856.f1) = "ST" && Ship-to data

		cConsignee = Upper(Alltrim(x856.f2))
		Select xpt
		m.st_name = cConsignee
		Replace xpt.consignee With  cConsignee In xpt

		cStoreNum = Alltrim(x856.f4)
		If Empty(xpt.shipins)
			Replace xpt.shipins With "STORENUM*"+cStoreNum In xpt
		Else
			Replace xpt.shipins With xpt.shipins+Chr(13)+"STORENUM*"+cStoreNum In xpt
		Endif
		Replace xpt.dcnum With cStoreNum In xpt

		If Len(cStoreNum)>5
			Replace xpt.storenum  With Val(Right(cStoreNum,5)) In xpt
		Else
			Replace xpt.storenum  With Val(cStoreNum) In xpt
		Endif

		Skip 2 In x856
		If Trim(x856.segment) = "N3"  && address info
			m.st_addr1 = Upper(Trim(x856.f1))
			m.st_addr2 = Upper(Trim(x856.f2))
			Replace xpt.address  With m.st_addr1
			Replace xpt.address2 With m.st_addr2
		Endif
		Skip 1 In x856
		If Trim(x856.segment) = "N4"  && address info
			m.zipbar = Trim(x856.f3)
			m.st_csz = Upper(Trim(x856.f1))+", "+Upper(Trim(x856.f2))+" "+Trim(x856.f3)
			Replace xpt.csz With m.st_csz In xpt
			If !Empty(Alltrim(x856.f4))
				Replace xpt.shipins With xpt.shipins+Chr(13)+"COUNTRY*"+Alltrim(x856.f4) In xpt
			Endif
		Endif
		Select x856
		Skip
		Loop
	Endif

	If Trim(x856.segment) = "N1" And Trim(x856.f1) = "SO" && Sold-to data
		Replace xpt.shipins With xpt.shipins+Chr(13)+"ENDCUST*"+Alltrim(x856.f2) In xpt
		Replace xpt.cacctnum With Alltrim(x856.f4) In xpt
		Replace xpt.shipins With xpt.shipins+Chr(13)+"ORIGPT*"+Alltrim(xpt.ship_ref) In xpt
		If Alltrim(x856.f4) = '0000062' && Belk order
			Replace xpt.ship_ref With "B"+Alltrim(xpt.ship_ref) In xpt
		Endif
		If Alltrim(x856.f4) = '0000044' && Kohl's order
			Replace xpt.ship_ref With "K"+Alltrim(xpt.ship_ref) In xpt
		Endif
		Select x856
		Skip
		Loop
	Endif

	If Trim(x856.segment) = "N1" And Trim(x856.f1) = "Z7" && Ship-for data
		Select xpt
		Replace xpt.shipfor With Upper(Alltrim(x856.f2)) In xpt
		Replace xpt.shipins With xpt.shipins+Chr(13)+"STORENAME*"+Alltrim(x856.f2) In xpt

		Replace xpt.sforstore With Alltrim(x856.f4) In xpt
		Skip 2 In x856
		If Trim(x856.segment) = "N3"  && address info
			m.sforaddr1 = Upper(Trim(x856.f1))
			m.sforaddr2 = Upper(Trim(x856.f2))
			Replace xpt.sforaddr1  With m.sforaddr1 In xpt
			Replace xpt.sforaddr2 With m.sforaddr2 In xpt
		Else
			Select x856
			Skip -1
			Loop
		Endif
		Skip 1 In x856
		If Trim(x856.segment) = "N4"  && address info
			m.zipbar = Trim(x856.f3)
			m.sforcsz = Upper(Trim(x856.f1))+", "+Upper(Trim(x856.f2))+" "+Trim(x856.f3)
			Replace xpt.sforcsz With m.sforcsz In xpt
		Endif
		Select x856
		Skip
		Loop
	Endif

	If Trim(x856.segment) = "N9" And Trim(x856.f1) = "DP"  && customer dept
		m.dept = Alltrim(x856.f2)
		Replace xpt.dept With m.dept In xpt
		cFOB = Alltrim(x856.f3)
		Replace xpt.div With cFOB In xpt
		Replace xpt.shipins With xpt.shipins+Chr(13)+"FOB*"+cFOB In xpt
		Select x856
		Skip
		Loop
	Endif

* PG fixed the FOB map 7/19/2012, tweaked on 8/21/2012
	If Trim(x856.segment) = "N9" And Trim(x856.f1) = "MR"
		If "NEIMAN"$xpt.consignee Or "BERGDORF"$xpt.consignee && dept name, FOB
			cFOB = Alltrim(x856.f2)+" "+Alltrim(x856.f3)
			Replace xpt.div With cFOB In xpt
			Replace xpt.shipins With xpt.shipins+Chr(13)+"DIVNAME*"+cFOB In xpt
			Select x856
			Skip
			Loop
		Endif
	Endif

	If Trim(x856.segment) = "N9" And Trim(x856.f1) = "VNN"  && PO #
		m.cnee_ref = Alltrim(x856.f2)
		Replace xpt.cnee_ref With m.cnee_ref In xpt
		Select x856
		Skip
		Loop
	Endif

	If Trim(x856.segment) = "N9" And Trim(x856.f1) = "AD"  && DUNS # -  Added 09.14.2009, Joe
		cDUNS = Alltrim(x856.f2)
		Replace xpt.shipins With xpt.shipins+Chr(13)+"DUNS*"+cDUNS In xpt
		Select x856
		Skip
		Loop
	Endif

	If Trim(x856.segment) = "N9" And Trim(x856.f1) = "FL"  && Womens Shoes indicator -  Added 02.22.2017, Joe
		If Alltrim(x856.f2)="W"
			lWomens = .T.
			If !"W"$xpt.ship_ref
				Replace xpt.ship_ref With Alltrim(xpt.ship_ref)+"W" In xpt
			Endif
		Endif

		If Alltrim(x856.f2)="M"
			lmens = .T.
			If !"M"$xpt.ship_ref
				Replace xpt.ship_ref With Alltrim(xpt.ship_ref)+"M" In xpt
			Endif
		Endif

		Select x856
		Skip
		Loop
	Endif

	If Trim(x856.segment) = "N9" And Trim(x856.f1) = "PGC"  && PO #
		If Trim(x856.f2)="C"
			lPrepack = .F.
			m.units = .T.
			Replace xpt.goh With .T. In xpt
			Replace xpt.accountid With nAcctNum In xpt
		Else
			lPrepack = .T.
			m.units = .F.
			Replace xpt.accountid With nAcctNum In xpt
		Endif
		Select x856
		Skip
		Loop
	Endif

	If Trim(x856.segment) = "N9" And Trim(x856.f1) = "TV"  && Buying Unit; only for DSW
		Replace xpt.batch_num With Alltrim(x856.f2) In xpt
		Select x856
		Skip
		Loop
	Endif

	If Trim(x856.segment) = "N9" And Trim(x856.f1) = "WH"  && Shipper's Warehouse #
		Replace xpt.shipins With xpt.shipins + Chr(13) + "WAREHOUSE*"+Alltrim(x856.f2) In xpt
		Select x856
		Skip
		Loop
	Endif

	If Trim(x856.segment) = "G62" And Trim(x856.f1) == "10"  && start date
		lcDate = Alltrim(x856.f2)
		lxDate = Substr(lcDate,5,2)+"/"+Substr(lcDate,7,2)+"/"+Left(lcDate,4)
		ldDate = Ctod(lxDate)
		Store ldDate To dStart
		Replace xpt.Start With ldDate In xpt
		cMMYY = "MMYY*"+Padl(Alltrim(Str(Month(ldDate))),2,'0')+Right(Alltrim(Str(Year(ldDate))),2)
		Replace xpt.shipins With xpt.shipins+Chr(13)+cMMYY In xpt
		Select x856
		Skip
		Loop
	Endif

	If Trim(x856.segment) = "G62" And Trim(x856.f1) == "01"  && cancel date
		lcDate = Alltrim(x856.f2)
		lxDate = Substr(lcDate,5,2)+"/"+Substr(lcDate,7,2)+"/"+Left(lcDate,4)
		ldDate = Ctod(lxDate)
		Store ldDate To dCancel
		Replace xpt.Cancel With ldDate In xpt
		If dCancel<dStart  && Added to check for Start/Cancel errors...10/05/07
			Select xpt
			Brow
			errordates()
			Use In x856
			lOK = .F.
			Return
		Endif
		Select x856
		Skip
		Loop
	Endif

	If Trim(x856.segment) = "W66" && ship info
		Replace xpt.ship_via With Trim(x856.f5) In xpt
		Replace xpt.scac With Trim(x856.f10) In xpt
	Endif


	If Trim(x856.segment) = "LX" && start of PT detail, stay here until this PT is complete
		Assert .F. Message cship_ref+" Detail, Type = "+Iif(lPrepack,"Prepack","Pickpack")+"...debug"
		m.units = Iif(lPrepack,.F.,.T.)
		Wait "NOW IN DETAIL LOOP PROCESSING" Window Nowait

		Select xptdet
		Calculate Max(ptdetid) To m.ptdetid
		m.ptdetid = m.ptdetid + 1

		Select xptdet
		Skip 1 In x856

		Do While Alltrim(x856.segment) != "SE"
			cSegment = Trim(x856.segment)
			cCode = Trim(x856.f1)

			If Trim(x856.segment) = "W01" && Destination Quantity
				Select xptdet
				Append Blank
				Replace xptdet.ptid With xpt.ptid In xptdet
				Replace xptdet.ptdetid With m.ptdetid In xptdet
				Replace xptdet.units With m.units In xptdet
				m.ptdetid = m.ptdetid + 1
				Replace xptdet.accountid With xpt.accountid In xptdet
				If lPrepack
					cUsePack = Allt(Str(Int(Val(x856.f12))))
					If Empty(cUsePack)
						Wait Window "Empty cUsePack at PT"+cship_ref Timeout 3
						NormalExit = .F.
						errormail1()
						Use In x856
						lOK = .F.
						Return
					Endif
					If cUsePack == "1"
						cUsePack = Allt(x856.f1)
					Endif
				Else
					cUsePack = "1"
				Endif

				Replace xptdet.Pack With cUsePack In xptdet
				m.casepack = Int(Val(cUsePack))
				Replace xptdet.casepack With m.casepack In xptdet
				m.divvalue = Int(Val(Alltrim(x856.f1)))

				If lPrepack And m.divvalue = 1
					m.divvalue = Int(Val(cUsePack))
				Endif

				If m.divvalue = 0 And !lPrepack
					errormail1()
					Use In x856
					lOK = .F.
					Return
				Endif

				If !lPrepack  && Pickpack
					Replace xptdet.totqty With Int(Val(x856.f1)) In xptdet
				Else
					Replace xptdet.totqty With (m.divvalue/m.casepack) In xptdet
				Endif

				Replace xpt.qty With xpt.qty + xptdet.totqty In xpt
				Replace xpt.origqty With xpt.qty In xpt
				Replace xptdet.origqty With xptdet.totqty In xptdet

				If Empty(Trim(xptdet.printstuff))
					Replace xptdet.printstuff With "UNITCODE*"+Alltrim(x856.f2)
				Else
					Replace xptdet.printstuff With xptdet.printstuff+Chr(13)+"UNITCODE*"+Alltrim(x856.f2)
					If lWomens
						If !("WOMENSIZE"$xptdet.printstuff)
							Replace xptdet.printstuff With xptdet.printstuff+Chr(13)+"WOMENSIZE" In xptdet
						Endif
					ENDIF
SET STEP ON 
*******Added 5/12/17 TMARG
					If lmens
						If !("MENSIZE"$xptdet.printstuff)
							Replace xptdet.printstuff With xptdet.printstuff+Chr(13)+"MENSIZE" In xptdet
						Endif
					Endif
				Endif

				If Alltrim(x856.f4) = "VN"
					czStyle = Alltrim(x856.f5)
					Replace xptdet.Style With czStyle In xptdet
				Endif

				If Alltrim(x856.f6) = "LT"
					Replace xptdet.printstuff With xptdet.printstuff+Chr(13)+"LABELSTYLE*"+Alltrim(x856.f7)
				Endif

				If Alltrim(x856.f8) = "SK"
					Replace xptdet.printstuff With xptdet.printstuff+Chr(13)+"LABELSIZESKU*"+Alltrim(x856.f9)
				Endif

				If !("STORENUM*"$xptdet.printstuff)
					Replace xptdet.printstuff With xptdet.printstuff+Chr(13)+"STORENUM*"+cStoreNum In xptdet
				Endif
			Endif

			If Trim(x856.segment) = "G69" && shipstyle
				cPrtstr = "SHIPSTYLE*"+Upper(Trim(x856.f1))
				If !("SHIPSTYLE"$xptdet.printstuff)
					Replace xptdet.printstuff With xptdet.printstuff+Chr(13)+cPrtstr In xptdet
				Endif
			Endif

			If Trim(x856.segment) = "N9" And Alltrim(x856.f1) = "LI"
				Replace xptdet.linenum With Alltrim(x856.f2) In xptdet
			Endif

			If Trim(x856.segment) = "N9" And Inlist(Alltrim(x856.f1),"CS","UP")
				cStyleIn = Alltrim(x856.f2)
				If lPrepack And Left(cStyleIn,3) = "000"
					cStyleIn = Alltrim(Str(Int(Val(cStyleIn))))
				Endif

				If Alltrim(x856.f1) = "UP"
					Replace xptdet.upc With cStyleIn In xptdet
				Endif
			Endif

			If Trim(x856.segment) = "N9" And Alltrim(x856.f1) = "SZ"
				cID = Allt(x856.f2)
				Replace xptdet.Id With cID In xptdet
				Replace xptdet.printstuff With xptdet.printstuff+Chr(13)+"SIZE*"+Alltrim(x856.f2) In xptdet
			Endif

			If Trim(x856.segment) = "N9" And Alltrim(x856.f1) = "CL"
				Replace xptdet.Color With Allt(x856.f2) In xptdet
				Replace xptdet.printstuff With xptdet.printstuff+Chr(13)+"COLOR*"+Alltrim(x856.f2) In xptdet
			Endif

			If Trim(x856.segment) = "N9" And Alltrim(x856.f1) = "JP"  && Prepack Code - DSW Only
				Replace xptdet.shipstyle With Alltrim(x856.f2) In xptdet
			Endif

			If Trim(x856.segment) = "N9" And Alltrim(x856.f1) = "SK"  && Customer SKU - DSW Only
				Replace xptdet.custsku With Alltrim(x856.f2) In xptdet
			Endif

			If Trim(x856.segment) = "N9" And Trim(x856.f1) = "CG"  && "Gender SKU"  Added 01.13.2016, Joe
				cGenderSKU = Alltrim(x856.f2)
				Replace xptdet.printstuff With xptdet.printstuff+Chr(13)+"LABELGENDERSKU*"+cGenderSKU In xptdet
			Endif

			If Trim(x856.segment) = "AMT" And Alltrim(x856.f1) = "LP"
				Replace xptdet.printstuff With xptdet.printstuff+Chr(13)+"LISTPRICE*"+Alltrim(x856.f2) In xptdet
			Endif

			Skip 1 In x856
		Enddo

		Select xpt
		Select xptdet
	Endif

	Select x856
	If !Eof()
		Skip 1 In x856
	Endif
Enddo

Select xptdet
rollup()
Wait Window "At end of ROLLUP procedure" Timeout 2

Do m:\dev\prg\setuppercase940

If lBrowfiles
	Wait Window "If tables are incorrect, CANCEL when XPTDET window opens"+Chr(13)+"Otherwise, will load PT table" Timeout 2
	Select xpt
	Copy To h:\xptrowone Type Xl5
	Locate
*	BROWSE TIMEOUT 10
	Select xptdet
	Copy To h:\xptdetrowone Type Xl5
	Locate
	Browse
*	cancel
Endif

Wait Window cCustname+" Breakdown Round complete..." Timeout 2
Select x856
If Used("PT")
	Use In pt
Endif
If Used("PTDET")
	Use In ptdet
Endif
Wait Clear
Return

*************************************************************************************************
*!* Error Mail procedure - W01*12 Segment
*************************************************************************************************
Procedure errormail1

If lEmail
	Select 0
	Use F:\3pl\Data\mailmaster Alias mm Shared
	Locate For mm.edi_type = "MISC" And mm.taskname = "ROWONEERR"
	Store Trim(Iif(mm.use_alt,mm.sendtoalt,mm.sendto)) To tsendto
	Store Trim(Iif(mm.use_alt,mm.ccalt,mm.cc)) To tcc
	Use In mm
	cMailLoc = Iif(cOffice = "C","West",Iif(cOffice = "N","New Jersey","Florida"))
	tsubject= "TGF "+cMailLoc+" "+cCustname+" Pickticket Error: " +Ttoc(Datetime())
	tmessage = "File: "+cFilename+Chr(13)+" is missing W01*12 segment(s), causing a divide by zero error."
	tmessage = tmessage + "Please re-create and resend this file."
	Do Form m:\dev\frm\dartmail2 With tsendto,tFrom,tsubject,tcc,tattach,tmessage,"A"
Endif
Endproc

*************************************************************************************************
*!* Error Mail procedure - Cancel Before Start Date
*************************************************************************************************
Procedure errordates

If lEmail
	tsendto = tsendtotest
	tcc =  tcctest
	cMailLoc = Iif(cOffice = "C","West",Iif(cOffice = "N","New Jersey","Florida"))
	tsubject= "TGF "+cMailLoc+" "+cCustname+" Pickticket Error: " +Ttoc(Datetime())
	tmessage = "File: "+cFilename+Chr(13)+" has Cancel date(s) which precede Start date(s)"
	tmessage = tmessage + "File needs to be checked and possibly resubmitted."
	Do Form m:\dev\frm\dartmail2 With tsendto,tFrom,tsubject,tcc,tattach,tmessage,"A"
Endif
Endproc

******************************
Procedure rollup
******************************
Assert .F. Message "In rollup procedure"
nChangedRecs = 0
Select xptdet
Set Deleted On
Locate
If lTesting
	Browse
Endif
cMsgStr = "PT/OSID/ODIDs changed: "
Scan For !Deleted()
	Scatter Memvar
	nRecno = Recno()
	Locate For xptdet.ptid = m.ptid ;
		AND xptdet.Style = m.style ;
		AND xptdet.Color = m.color ;
		AND xptdet.Id = m.id ;
		AND xptdet.Pack = m.pack ;
		AND xptdet.upc = m.upc ;
		AND Recno('xptdet') > nRecno
	If Found()
		Select xpt
		Locate For xpt.ptid = m.ptid
		If !Found()
			Wait Window "PROBLEM!"
		Endif
		cship_ref = Allt(xpt.ship_ref)
		If nChangedRecs > 10
			cMsgStr = "More than 10 records rolled up"
		Else
			cMsgStr = cMsgStr+Chr(13)+cship_ref+" "+Transform(m.ptid)+" "+Transform(m.ptdetid)
		Endif
		nChangedRecs = nChangedRecs+1
		Select xptdet
		nTotqty1 = xptdet.totqty
		Delete Next 1 In xptdet
	Endif
	If !Eof()
		Go nRecno
		Replace xptdet.totqty With xptdet.totqty+nTotqty1 In xptdet
		Replace xptdet.origqty With xptdet.totqty In xptdet
	Else
		Go nRecno
	Endif
Endscan

If nChangedRecs > 0
	Wait Window cMsgStr Timeout 2
	Wait Window "There were "+Trans(nChangedRecs)+" records rolled up" Timeout 1
Else
	Wait Window "There were NO records rolled up" Timeout 1
Endif

Endproc
