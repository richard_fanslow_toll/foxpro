* cleanup utility

utilsetup("PURGEFILES")

store "" to guserid, gprocess
gdevelopment=.f.
xjobno=0
xsystem="PURGE"

* dedemail

xsqlexec("delete from dedemail where adddt<{"+dtoc(date()-30)+"}",,,"stuff")

* ptsav & ptsavdet

xsqlexec("delete from ptsave where adddt<{"+dtoc(date()-180)+"}",,,"wh")
xsqlexec("delete from ptdetsav where adddt<{"+dtoc(date()-180)+"}",,,"wh")

* delete files with certain extensions

delext("f","bak","tbk")

* delete old system EXEs

delexe("bugtrack","bt")
delexe("ex","ex")
delexe("express","fx")
delexe("hr","hr")
delexe("kpi","kpi")
delexe("mm","mm")
delexe("monthend","me")
delexe("ownerop","oo")
delexe("sysdata","qq")
delexe("rcv","rc")
delexe("shop","sh")
delexe("sl","sl")
delexe("users","us")
delexe("trailyrd","ty")
delexe("watusi","ar")
delexe("wo","wo")
delexe("wh1","wh")
delexe("wh2","wh")
delexe("wh5","wh")
delexe("wh6","wh")
delexe("wh7","wh")
delexe("wh8","wh")
delexe("whf","wh")
delexe("whi","wh")
delexe("whj","wh")
delexe("whm","wh")
delexe("whr","wh")
delexe("whk","wh")
delexe("whl","wh")
delexe("whs","wh")
delexe("whx","wh")
delexe("who","wh")
delexe("why","wh")
delexe("whz","wh")

*!*	* purge old SQL data	removed dy 3/29/18

*!*	xdate = Dtoc(date()-365)
*!*	yr = Substr(xdate,7,4)
*!*	mon =Substr(xdate,4,2)
*!*	dy = Substr(xdate,1,2)
*!*	xdate = Alltrim(Str(&yr))+[-]+Padl(Alltrim(Str(&dy)),2,"0")+[-]+Padl(Alltrim(Str(&mon)),2,"0")

*!*	for i=1 to 2
*!*		do case
*!*		case i=1
*!*			xserver="SQL4"
*!*		case i=2
*!*			xserver="SQL5"
*!*		case i=3
*!*			xserver="SP3"
*!*		endcase

*!*		xlcdsnless="driver=SQL Server;server="+xserver+";uid=SA;pwd=B%g23!7#$;DATABASE=PICKPACK"
*!*		xhandle=sqlstringconnect(xlcdsnless,.t.)

*!*		xsql = "delete from labels where insdttm<{ts '"+xdate+" 00:00:00.0'}"
*!*		nOK = SQLEXEC(xhandle,xsql,"xrpt")
*!*		if nok#1
*!*			email("Dyoung@fmiint.com","VAL purge old labels SQL data in "+xserver+" did not work ",,,,,.t.,,,,,.t.)
*!*		endif

*!*		xsql = "delete from cartons where insdttm<{ts '"+xdate+" 00:00:00.0'}"
*!*		nOK = SQLEXEC(xhandle,xsql,"xrpt")
*!*		if nok#1 and i#3	&& ignore SP3 error for now dy 6/15/12
*!*			email("Dyoung@fmiint.com","VAL purge old cartons SQL data in "+xserver+" did not work ",,,,,.t.,,,,,.t.)
*!*		endif

*!*		xsql = "delete from frtrans where insdttm<{ts '"+xdate+" 00:00:00.0'}"
*!*		nOK = SQLEXEC(xhandle,xsql,"xrpt")
*!*		if nok#1
*!*			email("Dyoung@fmiint.com","VAL purge old frtrans SQL data in "+xserver+" did not work ",,,,,.t.,,,,,.t.)
*!*		endif
*!*		
*!*		=sqldisconnect(xhandle)
*!*	next

* delete files in various folders

? deletefile("f:\ftpusers\ariat\out\archive",10)
? deletefile("f:\ftpusers\ariat\945-staging",10)
? deletefile("f:\ded",3)
? deletefile("f:\shop\tmpfiles",30)
? deletefile("f:\wh1\wavefiles",20)
? deletefile("f:\wh2\wavefiles",20)
? deletefile("f:\wh5\wavefiles",20)
? deletefile("f:\wh7\wavefiles",20)
? deletefile("f:\wo\dailyclose",20)
? deletefile("f:\watusi\pdfbak",30)
? deletefile("g:\malvern",360)

? deletefile("f:\moret\skumaster\archive",60)
? deletefile("f:\0-picktickets\archive",60)
? deletefile("f:\0-packinglist\archive",60)

? deletefile("f:\labeldata\agegroup",120)
? deletefile("f:\labeldata\bioworld",120)
? deletefile("f:\labeldata\hbest",120)
? deletefile("f:\labeldata\highlineunited",120)
? deletefile("f:\labeldata\hpdlabels",120)
? deletefile("f:\labeldata\in7",120)
? deletefile("f:\labeldata\intradeco",120)
? deletefile("f:\labeldata\jockey",120)
? deletefile("f:\labeldata\mjretail",120)
? deletefile("f:\labeldata\modernshoe",120)
? deletefile("f:\labeldata\moret",120)
? deletefile("f:\labeldata\nanjing",120)
? deletefile("f:\labeldata\saralee",120)
? deletefile("f:\labeldata\sbh_labels",120)
? deletefile("f:\labeldata\tko",120)
? deletefile("f:\labeldata\yellsteel",120)

? deletefile("f:\ftpusers\aeropostale\315out",60)
? deletefile("f:\ftpusers\ariat",60)
? deletefile("f:\ftpusers\synclaire",60)
*? deletefile("f:\ftpusers\bcny",60) removed dy 4/17/17 causing invalid seek offset error
? deletefile("f:\ftpusers\agegroup",60)
? deletefile("f:\ftpusers\agegroup-nj",60)
? deletefile("f:\ftpusers\americaneagle",60)
? deletefile("f:\ftpusers\biglots",30)
? deletefile("f:\ftpusers\billabong",90)
? deletefile("f:\ftpusers\bugaboo",60)
? deletefile("f:\ftpusers\courtaulds",60)
? deletefile("f:\ftpusers\intradeco",60)
? deletefile("f:\ftpusers\intradeco-ca",60)
? deletefile("f:\ftpusers\intradeco-fl",60)
? deletefile("f:\ftpusers\jag",60)
? deletefile("f:\ftpusers\jones",60)
? deletefile("f:\ftpusers\lifefactory",60)
? deletefile("f:\ftpusers\macys",30)
? deletefile("f:\ftpusers\mj_retail",15)
? deletefile("f:\ftpusers\mj_wholesale",30)
? deletefile("f:\ftpusers\MJ_Wholesale_CA",30)
? deletefile("f:\ftpusers\moret-sp",60)
? deletefile("f:\ftpusers\moretgroup",60)
? deletefile("f:\ftpusers\nanjing",60)
? deletefile("f:\ftpusers\sears\billdatain",60)
? deletefile("f:\ftpusers\searsedi",60)
? deletefile("f:\ftpusers\underarmour",30)
? deletefile("f:\ftpusers\mj_retail\wms_outbound\transfer_slips\staging",3)

? deletefile("f:\ftpusers\merkury\945out\archive",20)
*	? deletefile("f:\ftpusers\mj_retail\wms_outbound\ttpusers\merkury\945out\archive",20)
*	? deletefile("f:\ftpusers\mj_retail\wms_outbound\ttpusers\merkury\945out\archive",20)
? deletefile("f:\ftpusers\merkury\940translate",5)
? deletefile("f:\ftpusers\merkury\997in",10)
? deletefile("f:\ftpusers\merkury\in\archive",20)
? deletefile("f:\ftpusers\merkury\out\archive",10)
? deletefile("f:\ftpusers\merkury\inbounds\archive",30)

? deletefile("F:\FTPUSERS\TJMAX\861-STAGING",30)
? deletefile("F:\FTPUSERS\TJMAX\856Out\archive",30)
? deletefile("F:\FTPUSERS\TJMAX\250In\Archive",30)
? deletefile("F:\FTPUSERS\TJMAX\212out",30)
? deletefile("F:\FTPUSERS\TJMAX\IN\Archive",30)


? deletefile("f:\util\adpconnection\reports\adpgl\more",30)
? deletefile("f:\util\hr\dedxreports\reports",30)
? deletefile("f:\util\hr\hartford\reports",60)
? deletefile("f:\util\hr\hrflashreportforneil",60)
? deletefile("f:\util\hr\metlifeterminated\reports",30)
? deletefile("f:\util\hr\salariedlist\reports",30)
? deletefile("f:\util\hr\tgfstaffrpt\reports",60)
? deletefile("f:\util\johnhancock\reports",60)
? deletefile("f:\util\kronos\caallhourly12hoursreports",30)
? deletefile("f:\util\kronos\cadrivertimereports",30)
? deletefile("f:\util\kronos\dataimport\archived",60)
? deletefile("f:\util\kronos\logfiles",60)
? deletefile("f:\util\kronos\temptimereports",30)
? deletefile("f:\util\lf\reports",30)
? deletefile("f:\util\mlautobilling\reports",30)

*strtofile("X","f:\auto\purgefiles_"+dtos(date())+".txt")	&& when does this end?

schedupdate()

_screen.Caption=gscreencaption
on error
