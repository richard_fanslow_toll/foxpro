* Report on the entering of guaranteed hours, retro pay, etc., as well as any time someone enters a punch or 
* ESPECIALLY changes a punch for Hourly employees. For Marie.
* EXEs go in F:\UTIL\KRONOS\
LOCAL loKronosHourlyAuditReport

loKronosHourlyAuditReport = CREATEOBJECT('KronosHourlyAuditReport')
loKronosHourlyAuditReport.MAIN()
CLOSE DATABASES ALL
RETURN

#DEFINE LOGIT 1
#DEFINE WAITIT 2
#DEFINE NOWAITIT 4
#DEFINE SENDIT 8
#DEFINE CRLF CHR(13) + CHR(10)
#DEFINE LF CHR(10)
#DEFINE RETURN_DATA_MANDATORY .F.
#DEFINE RETURN_DATA_NOT_MANDATORY .T.
#DEFINE DAILY_OT_MULTIPLIER 1.5

DEFINE CLASS KronosHourlyAuditReport AS CUSTOM

	cProcessName = 'KronosHourlyAuditReport'

	lTestMode = .T.  && .F. FOR NORMAL OPERATION, .T. TO SUPPRESS ALL BUT DEVELOPER EMAILS AND CHANGE INPUT TABLE, ETC.

	lAutoYield = .T.

	cStartTime = TTOC(DATETIME())
	
	* processing properties
	cEmployeeListSQL =" (SELECT PERSONNUM FROM VP_EMPLOYEEV42 WHERE HOMELABORLEVELNM1 = 'SXI') "  && standard case
	*cEmployeeListSQL =" (SELECT PERSONNUM FROM VP_EMPLOYEEV42 WHERE PERSONNUM = '999') "  && for testing 
	
	* connection properties
	nSQLHandle = 0

	* file properties
	nFileHandle = 0

	* date properties
	dtNow = DATETIME()

	dToday = DATE()
	* to force different pay weeks
	*dToday = {^2009-05-26}

	cToday = DTOC(DATE())
	
	* wait window properties
	nWaitWindowTimeout = 2
	lWaitWindowIsOn = .T.

	* logfile properties
	lLoggingIsOn = .T.
	cLogFile = 'F:\UTIL\KRONOS\Logfiles\KronosHourlyAuditReport_log.txt'

	* INTERNAL email properties
	lSendInternalEmailIsOn = .T.
	cFrom = 'FMI Corporate <fmicorporate@fmiint.com>'
	cSendTo = 'mbennett@fmiint.com'
	cCC = ''
	cSubject = 'Hourly Audit Report'
	cAttach = ''
	cBodyText = ''

	cCOMPUTERNAME = ''
	cUSERNAME = ''

	FUNCTION INIT
		IF NOT DODEFAULT()
			RETURN .F.
		ENDIF
		WITH THIS
			CLEAR
			*SET RESOURCE OFF
			CLOSE DATA
			SET CENTURY ON
			SET DATE AMERICAN
			SET DECIMALS TO 1
			SET HOURS TO 24
			SET ANSI ON
			SET TALK OFF
			SET DELETED ON
			SET CONSOLE OFF
			SET EXCLUSIVE OFF
			SET SAFETY OFF
			SET EXACT OFF
			SET STATUS BAR ON
			SET SYSMENU OFF
			SET ENGINEBEHAVIOR 70
			_VFP.AUTOYIELD = .lAutoYield
			.cCOMPUTERNAME = GETENV("COMPUTERNAME")
			.cUSERNAME = GETENV("USERNAME")
			IF .lTestMode THEN
				.cSendTo = 'mbennett@fmiint.com'
				.cLogFile = 'F:\UTIL\KRONOS\Logfiles\KronosHourlyAuditReport_log_TESTMODE.txt'
			ENDIF
			.lLoggingIsOn = .lLoggingIsOn AND NOT EMPTY(.cLogFile)
			IF .lLoggingIsOn THEN
				SET ALTERNATE TO (.cLogFile) ADDITIVE
				SET ALTERNATE ON
			ENDIF
		ENDWITH
	ENDFUNC


	FUNCTION DESTROY
		WITH THIS
			IF .lLoggingIsOn  THEN
				SET ALTERNATE OFF
				SET ALTERNATE TO
			ENDIF
		ENDWITH
		DODEFAULT()
	ENDFUNC


	FUNCTION MAIN
		WITH THIS
			LOCAL lnNumberOfErrors, lcSQL, ldJan1st, lcAfterDateSQL, ldDate, ldEndDate, ldStartDate
			LOCAL llRetval1, llRetval2, lcStartDateSQL, lcEndDateSQL, lcEditType
			LOCAL oExcel, oWorkbook, oWorksheet, lnRow, lcRow, lnStartRow, lcStartRow, lnEndRow, i
			LOCAL lcTargetDirectory, lcFiletoSaveAs, llSaveAgain, lcXLFileName, lcTitle, lcPunchTime

			TRY
				lnNumberOfErrors = 0
				ldToday = .dToday

				*.TrackProgress('==================================================================================================================', LOGIT+SENDIT)
				.TrackProgress('KRONOS Hourly Time Card Audit process started....', LOGIT+SENDIT)
				.TrackProgress('COMPUTERNAME = ' + .cCOMPUTERNAME, LOGIT+SENDIT)
				.TrackProgress('USERNAME = ' + .cUSERNAME, LOGIT+SENDIT)
				.TrackProgress('(project = KRONOSHOURLYAUDITREPORT)', LOGIT+SENDIT)
				IF .lTestMode THEN
					.TrackProgress('                        TEST MODE                        ', LOGIT+SENDIT)
				ENDIF
				
				ldDate = .dToday

				DO WHILE DOW(ldDate,1) <> 7
					ldDate = ldDate - 1
				ENDDO
				
				ldEndDate = ldDate
				ldStartDate = ldEndDate - 6
				
*!*	***************************************
*!*	* activate to force custom date range
*!*	ldStartDate = {^2011-01-02}
*!*	ldEndDate = {^2011-01-08}
*!*	***************************************
				.cSubject = "Hourly Audit Report for " + TRANSFORM(ldStartDate) + " - " + TRANSFORM(ldEndDate)

				lcStartDateSQL = "{ts '" + TRANSFORM(YEAR(ldStartDate)) + "-" + PADL(MONTH(ldStartDate),2,"0") + "-" + PADL(DAY(ldStartDate),2,"0") + " 00:00:00.0'}"
				lcEndDateSQL = "{ts '" + TRANSFORM(YEAR(ldEndDate)) + "-" + PADL(MONTH(ldEndDate),2,"0") + "-" + PADL(DAY(ldEndDate),2,"0") + " 23:59:59.0'}"

				
*!*					ldJan1st = {^2011-01-01}
*!*					lcAfterDateSQL = "{ts '" + TRANSFORM(YEAR(ldJan1st)) + "-" + PADL(MONTH(ldJan1st),2,"0") + "-" + PADL(DAY(ldJan1st),2,"0") + " 00:00:00.0'}"

				IF USED('CURAUDIT') THEN
					USE IN CURAUDIT
				ENDIF
				IF USED('CURAUDIT2') THEN
					USE IN CURAUDIT2
				ENDIF
				
				* main SQL
				lcSQL = ;
					" SELECT DISTINCT ENTEREDONDTM AS EDITDTM,  EVENTDATE AS TIMCARDDTM,  PERSONFULLNAME AS EMPLOYEE,  PERSONNUM AS FILE_NUM, " + ;
					" {FN IFNULL(TRCACTIONTYPEID,0)} AS TYPEID,  {FN IFNULL(PAYCODENAME,'')} AS PAYCODE, {FN IFNULL(OVERRIDETYPE,'')} as PUNCHTYPE, " + ;
					" CLIENTUSERNAME AS CUSER,  {FN IFNULL(TIMEINSECONDS,0)} AS NSECS, " + ;  
					" {FN IFNULL(TIMESHEETITEMTYPE,'')} AS ITEMTYPE FROM VP_TIMECARDAUDIT " + ;
					" WHERE ((EVENTDATE >= " + lcStartDateSQL + " AND EVENTDATE <= " + lcEndDateSQL + ")" + ;
					" OR (ENTEREDONDTM >= " + lcStartDateSQL + " AND ENTEREDONDTM <= " + lcEndDateSQL + "))" + ;
					" AND PERSONNUM IN " + ;  
					.cEmployeeListSQL + ;
					" AND  FUNCTIONCODE = 'E'  AND (TIMESHEETITEMTYPE = 'PayCodeEdit' OR OVERRIDETYPE = 'In Punch' OR OVERRIDETYPE = 'Out Punch')  " + ;
					" ORDER BY ENTEREDONDTM DESC "
					
				IF .lTestMode THEN
					.TrackProgress('', LOGIT+SENDIT)
					.TrackProgress('lcSQL =' + lcSQL, LOGIT+SENDIT)
				ENDIF

				.nSQLHandle = SQLCONNECT("WTK2","sa","kr0n1tes")

				IF .nSQLHandle > 0 THEN

						IF .ExecSQL(lcSQL, 'CURAUDIT', RETURN_DATA_MANDATORY) THEN						
							IF USED('CURAUDIT') AND NOT EOF('CURAUDIT') THEN
								
								SELECT SPACE(200) AS EDITTYPE, * FROM CURAUDIT INTO CURSOR CURAUDIT2 ORDER BY EMPLOYEE, TIMCARDDTM READWRITE
								
								* populate Edit Type field
								SELECT CURAUDIT2
								SCAN
									lcPunchTime = PADL(HOUR(CURAUDIT2.TIMCARDDTM),2,'0') + ":" + PADL(MINUTE(CURAUDIT2.TIMCARDDTM),2,'0') + ":" + PADL(SEC(CURAUDIT2.TIMCARDDTM),2,'0') 
									DO CASE
										CASE (EMPTY(CURAUDIT2.TYPEID)) AND (NOT EMPTY(CURAUDIT2.PAYCODE)) AND UPPER(ALLTRIM(CURAUDIT2.ITEMTYPE))== 'PAYCODEEDIT'
											lcEditType = "added Pay Code '" + UPPER(ALLTRIM(CURAUDIT2.PAYCODE)) + "' for " + ;
												TRANSFORM(CURAUDIT2.NSECS/3600) + " hours on " + TRANSFORM(TTOD(CURAUDIT2.TIMCARDDTM))
										CASE (EMPTY(CURAUDIT2.TYPEID)) AND (EMPTY(CURAUDIT2.PAYCODE)) AND ("PUNCH" $ UPPER(ALLTRIM(CURAUDIT2.PUNCHTYPE)))
											lcEditType = "added an " + ALLTRIM(CURAUDIT2.PUNCHTYPE) + " at " + lcPunchTime + " on " + TRANSFORM(TTOD(CURAUDIT2.TIMCARDDTM))
										CASE (CURAUDIT2.TYPEID = 5) AND (NOT EMPTY(CURAUDIT2.PAYCODE)) AND UPPER(ALLTRIM(CURAUDIT2.ITEMTYPE))== 'PAYCODEEDIT'
											lcEditType = "deleted Pay Code '" + UPPER(ALLTRIM(CURAUDIT2.PAYCODE)) + "' on " + TRANSFORM(TTOD(CURAUDIT2.TIMCARDDTM))
										CASE (CURAUDIT2.TYPEID = 5) AND (EMPTY(CURAUDIT2.PAYCODE)) AND ("PUNCH" $ UPPER(ALLTRIM(CURAUDIT2.PUNCHTYPE)))
											*lcEditType = "deleted the " + ALLTRIM(CURAUDIT2.PUNCHTYPE) + " at " + TRANSFORM(TIMCARDDTM)
											lcEditType = "deleted the " + ALLTRIM(CURAUDIT2.PUNCHTYPE) + " at " + lcPunchTime + " on " + TRANSFORM(TTOD(CURAUDIT2.TIMCARDDTM))
										CASE (CURAUDIT2.TYPEID = 6) AND (NOT EMPTY(CURAUDIT2.PAYCODE)) AND UPPER(ALLTRIM(CURAUDIT2.ITEMTYPE))== 'PAYCODEEDIT'
											lcEditType = "changed Pay Code '" + PROPER(ALLTRIM(CURAUDIT2.PAYCODE)) + "' amount to " + ;
												TRANSFORM(CURAUDIT2.NSECS/3600) + " hours on " + TRANSFORM(TTOD(CURAUDIT2.TIMCARDDTM)) + "-for orig. amount see timecard audit"
										CASE (CURAUDIT2.TYPEID = 6) AND (EMPTY(CURAUDIT2.PAYCODE)) AND ("PUNCH" $ UPPER(ALLTRIM(CURAUDIT2.PUNCHTYPE)))
											lcEditType = "changed an " + ALLTRIM(CURAUDIT2.PUNCHTYPE) + " time to " + lcPunchTime + " on " + TRANSFORM(TTOD(CURAUDIT2.TIMCARDDTM)) + "-for orig. time see timecard audit"
										OTHERWISE
										lcEditType = "?"
									ENDCASE	
									REPLACE CURAUDIT2.EDITTYPE WITH lcEditType							
								ENDSCAN


								WAIT WINDOW NOWAIT "Opening Excel..."
								oExcel = CREATEOBJECT("excel.application")
								
								oWorkbook = oExcel.workbooks.OPEN("F:\UTIL\KRONOS\TEMPLATES\HOURLYAUDITTEMPLATE.XLS")

								lcFileDate = STRTRAN(TRANSFORM(ldStartDate),"/","") + "--" + STRTRAN(TRANSFORM(ldEndDate),"/","")

								lcTargetDirectory = "F:\UTIL\KRONOS\AUDITREPORTS\"

*!*									* create directory if it doesn't exist
*!*									IF NOT DIRECTORY(lcTargetDirectory) THEN
*!*										MKDIR (lcTargetDirectory)
*!*										WAIT WINDOW TIMEOUT 1 "Made Target Directory " + lcTargetDirectory
*!*									ENDIF


								llSaveAgain = .F.
*!*									IF DIRECTORY(lcTargetDirectory) THEN
								lcFiletoSaveAs = lcTargetDirectory  + "Hourly Audit Report for " + lcFileDate
								lcXLFileName = lcFiletoSaveAs + ".XLS"
								IF FILE(lcXLFileName) THEN
									DELETE FILE (lcXLFileName)
								ENDIF
								oWorkbook.SAVEAS(lcFiletoSaveAs)
								* set flag to save again after sheet is populated
								llSaveAgain = .T.
*!*									ENDIF
								***********************************************************************************************************************
								***********************************************************************************************************************

								lnRow = 3
								lnStartRow = lnRow + 1
								lcStartRow = ALLTRIM(STR(lnStartRow))

								oWorksheet = oWorkbook.Worksheets[1]
								oWorksheet.RANGE("A" + lcStartRow,"K2000").clearcontents()

								oWorksheet.RANGE("A" + lcStartRow,"K2000").FONT.SIZE = 10
								oWorksheet.RANGE("A" + lcStartRow,"K2000").FONT.NAME = "Arial"
								oWorksheet.RANGE("A" + lcStartRow,"K2000").FONT.bold = .T.								

								SET DATE AMERICAN

								*lcTitle = "Hourly Audit Report for " + TRANSFORM(ldStartDate) + " - " + TRANSFORM(ldEndDate)								
								*.cSubject = lcTitle
								lcTitle = .cSubject

								oWorksheet.RANGE("A1").VALUE = lcTitle

								* main scan/processing
								SELECT CURAUDIT2
								SCAN
									lnRow = lnRow + 1
									lcRow = LTRIM(STR(lnRow))
									oWorksheet.RANGE("A" + lcRow).VALUE = CURAUDIT2.EMPLOYEE
									oWorksheet.RANGE("B" + lcRow).VALUE = "'" + CURAUDIT2.FILE_NUM
									oWorksheet.RANGE("C" + lcRow).VALUE = CURAUDIT2.CUSER
									oWorksheet.RANGE("D" + lcRow).VALUE = "'" + CURAUDIT2.EDITTYPE
									oWorksheet.RANGE("E" + lcRow).VALUE = CURAUDIT2.EDITDTM
									oWorksheet.RANGE("F" + lcRow).VALUE = CURAUDIT2.TIMCARDDTM
									oWorksheet.RANGE("G" + lcRow).VALUE = CURAUDIT2.TYPEID
									oWorksheet.RANGE("H" + lcRow).VALUE = CURAUDIT2.PAYCODE
									oWorksheet.RANGE("I" + lcRow).VALUE = CURAUDIT2.PUNCHTYPE
									oWorksheet.RANGE("J" + lcRow).VALUE = CURAUDIT2.NSECS
									oWorksheet.RANGE("K" + lcRow).VALUE = CURAUDIT2.ITEMTYPE
								
								ENDSCAN

								lnRow = lnRow + 1
								lcRow = LTRIM(STR(lnRow))
								*oWorksheet.RANGE("A" + lcStartRow,"T" + lcRow).SELECT
								oWorksheet.PageSetup.PrintArea = "$A$1:$D$" + lcRow

								* save again
								IF llSaveAgain THEN
									oWorkbook.SAVE()
								ENDIF
								oWorkbook.CLOSE()
								
								*BROWSE
								*COPY TO C:\TEMPFOX\PCHOURLYAUDIT.XLS XL5
								.cAttach = lcXLFileName
							ENDIF													
						ENDIF			
				ELSE
					* connection error
					.TrackProgress('Unable to connect to WTK System.', LOGIT+SENDIT)
				ENDIF   &&  .nSQLHandle > 0
				CLOSE DATABASES ALL
				
			CATCH TO loError

				.TrackProgress('There was an error.',LOGIT+SENDIT)
				.TrackProgress('The Error # is: ' + TRANSFORM(loError.ERRORNO), LOGIT+SENDIT)
				.TrackProgress('The Error Message is: ' + TRANSFORM(loError.MESSAGE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred in Program: ' + TRANSFORM(loError.PROCEDURE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred at line #: ' + TRANSFORM(loError.LINENO), LOGIT+SENDIT)
				lnNumberOfErrors = lnNumberOfErrors + 1
				IF TYPE('oExcel') = 'O' AND NOT ISNULL(oExcel) THEN
					oExcel.QUIT()
				ENDIF
				CLOSE DATA
				CLOSE ALL

			ENDTRY

			CLOSE DATA
			WAIT CLEAR
			***************** INTERNAL email results ******************************
			.TrackProgress('About to send status email.',LOGIT)
			.TrackProgress('==================================================================================================================', SENDIT)
			.TrackProgress('The logfile is: ' + .cLogFile, SENDIT)
			.TrackProgress('KRONOS PayCode Audit process started: ' + .cStartTime, LOGIT+SENDIT)
			.TrackProgress('KRONOS PayCode Audit process finished: ' + TTOC(DATETIME()), LOGIT+SENDIT)

			IF .lSendInternalEmailIsOn THEN
				* try to trap error from not having dartmail dll's registered on user's pc...
				TRY
					DO FORM dartmail2 WITH .cSendTo,.cFrom,.cSubject,.cCC,.cAttach,.cBodyText,"A"
					.TrackProgress('Sent status email.',LOGIT)
				CATCH TO loError
					*=MESSAGEBOX('There was an error sending the status email. Please contact Mark Bennett.',0+64,"KRONOS Temp Time")
					.TrackProgress('There was an error sending the status email.',LOGIT)
					.TrackProgress('The Error # is: ' + TRANSFORM(loError.ERRORNO), LOGIT)
					.TrackProgress('The Error Message is: ' + TRANSFORM(loError.MESSAGE), LOGIT)
					.TrackProgress('The Error occurred in Program: ' + TRANSFORM(loError.PROCEDURE), LOGIT)
					.TrackProgress('The Error occurred at line #: ' + TRANSFORM(loError.LINENO), LOGIT)
					lnNumberOfErrors = lnNumberOfErrors + 1
					CLOSE DATA
				ENDTRY

			ELSE
				.TrackProgress('Did not send status email: THIS.lSendInternalEmailIsOn = FALSE.',LOGIT)
			ENDIF

		ENDWITH
		RETURN
	ENDFUNC && main
	

	FUNCTION GetSQLStartDateTimeString
		* returns a timestamp string for use in SQL queries
		LPARAMETERS tdDate
		LOCAL lcSetDate, lcSQLDateTimeString
		lcSetDate = SET('DATE')
		SET DATE YMD
		lcSQLDateTimeString   = "{ts '" + TRANSFORM(YEAR(tdDate)) + "-" + PADL(MONTH(tdDate),2,"0") + "-" + PADL(DAY(tdDate),2,"0") + " 00:00:00.0'}"
		SET DATE &lcSetDate.
		RETURN lcSQLDateTimeString
	ENDFUNC
	

	FUNCTION GetSQLEndDateTimeString
		* returns a timestamp string for use in SQL queries
		LPARAMETERS tdDate
		LOCAL lcSetDate, lcSQLDateTimeString
		lcSetDate = SET('DATE')
		SET DATE YMD
		lcSQLDateTimeString = "{ts '" + TRANSFORM(YEAR(tdDate)) + "-" + PADL(MONTH(tdDate),2,"0") + "-" + PADL(DAY(tdDate),2,"0") + " 23:59:59.9'}"
		SET DATE &lcSetDate.
		RETURN lcSQLDateTimeString
	ENDFUNC
	

	FUNCTION ExecSQL
		LPARAMETERS tcSQL, tcCursorName, tlNoDataReturnedIsOkay
		LOCAL llRetval, lnResult
		WITH THIS
			* close target cursor if it's open
			IF USED(tcCursorName)
				USE IN (tcCursorName)
			ENDIF
			WAIT WINDOW "Fetching remote data; please wait..." NOWAIT
			lnResult = SQLEXEC(.nSQLHandle, tcSQL, tcCursorName)
			llRetval = ( lnResult > 0 )
			IF llRetval THEN
				* see if any data came back
				IF NOT tlNoDataReturnedIsOkay THEN
					IF (NOT USED(tcCursorName)) OR EOF(tcCursorName) THEN
						llRetval = .F.
						.TrackProgress('No data was returned by this query: ' +CRLF + tcSQL, LOGIT+SENDIT)
					ENDIF
				ENDIF
			ELSE
				.TrackProgress("Result Code [" + TRANSFORM(lnResult) + "] returned by this query: " + tcSQL, LOGIT+SENDIT)
			ENDIF
			WAIT CLEAR
			RETURN llRetval
		ENDWITH
	ENDFUNC


	PROCEDURE TrackProgress
		* do any combination of Wait Window, writing to logfile, and adding to body of email,
		* based on nFlags parameter.
		LPARAMETERS tcExpression, tnFlags
		WITH THIS
			IF BITAND(tnFlags,LOGIT) = LOGIT THEN
				IF .lLoggingIsOn THEN
					?
					? .cProcessName + "  " + TTOC(DATETIME()) + ": " + tcExpression
				ENDIF
			ENDIF
			IF .lWaitWindowIsOn THEN
				IF BITAND(tnFlags,NOWAITIT) = NOWAITIT THEN
					WAIT WINDOW tcExpression NOWAIT
				ENDIF
				IF BITAND(tnFlags,WAITIT) = WAITIT THEN
					WAIT WINDOW tcExpression TIMEOUT .nWaitWindowTimeout
				ENDIF
			ENDIF
			IF BITAND(tnFlags,SENDIT) = SENDIT THEN
				IF .lSendInternalEmailIsOn THEN
					.cBodyText = .cBodyText + tcExpression + CRLF + CRLF
				ENDIF
			ENDIF
		ENDWITH
	ENDPROC  &&  TrackProgress
	

ENDDEFINE
