**run this which will create a new wavelabels rec with the new UCC
**next, adjust the existing and new wavelabels record to show the correct skus and quantities
**next, change the record(s) into labels and cartons for correct skus and quantities of the existing ucc, 
**   so 1/2 of the recs will still show existing UCC, 1/2 will be for new UCC
**
parameters xucc, xfullcase
**xfullcase - if set to true, no need to heck or update wavelabels

*!*	if gdevelopment
*!*		close databases all
*!*		wh('l')
*!*		gmasteroffice='L'
*!*		goffice='L'
*!*		use f:\wh\wavelabels in 0
*!*		useca("info","wh")
*!*	endif


if !empty(xucc)
	if messagebox("Are you sure you want to splt UCC "+xucc+"?"+chr(13)+chr(13)+"Please note that the UCC entered will be unpacked. Both the new UCC and the original UCC will need to be packed manually! Continue anyeway?",4+16,"UCC Split")=7
		return
	endif
else
	if messagebox("Please note that the UCC entered will be unpacked. Both the new UCC and the original UCC will need to be packed manually! Continue anyeway?",4+16,"UCC Split")=7
		return
	endif

	xucc=alltrim(inputbox("UCC:","UCC Splt"))
	if len(xucc)#20
		messagebox("Invalid UCC! Process cancelled",16,"UCC Split")
		return
	endif
endif

useca("labels","pickpack_sql5",,,"ucc='"+xucc+"'",,"xtmplabels")
if reccount("xtmplabels")=0 or !inlist(xtmplabels.accountid,&gbillabongaccts,&gbbcaccounts)
	use in xtmplabels
	messagebox("UCC could not be found! Process cancelled",16,"UCC Split")
	return
endif

xaccountid=xtmplabels.accountid
xwonum=xtmplabels.wo_num
xpt=xtmplabels.ship_ref

xctnqty=val(inputbox("Total # of cartons:","UCC Splt"))
if xctnqty>179370693039
	use in xtmplabels
	messagebox("You entered a UCC number",16,"UCC Split")	&& dy 10/9/17
	return
endif
if xctnqty=0
	use in xtmplabels
	messagebox("Process cancelled",16,"UCC Split")
	return
endif

*existing ucc without chkdigit
xolducc=left(xucc,19)

if !xfullcase
	select wavelabels
	locate for uccnumber=xolducc
	if !found() or empty(blindlpn) or emptynul(updatedt)
		use in xtmplabels
		if empty(blindlpn) or emptynul(updatedt)
			messagebox("UCC hasnt been closed yet on the put wall! Process cancelled",16,"UCC Split")
		else
			messagebox("UCC could not be found in wavelabels! Process cancelled",16,"UCC Split")
		endif
		return
	else
		xwonum=wavelabels.wo_num
		locate for wo_num=xwonum and cartonnum=xolducc and alltrim(uccnumber)#alltrim(cartonnum)
		if found()
			use in xtmplabels
			messagebox("This UCC has already been split!! Process cancelled",16,"UCC Split")
			return
		endif
	endif
endif

if xsqlexec("select * from ppconfig where accountid="+transform(xaccountid)+" and cfgkey='GETUCC    '","xppconfig",,"wh")=0
	use in xppconfig
	use in xtmplabels
	messagebox("Configuration data could not be found to create a UCC number for this account!! Process cancelled",16,"UCC Split")
	return
endif

xexecscript=xppconfig.moredata
use in xppconfig

dimension aucc(xctnqty)
aucc(1)=xucc
xuccstr=xucc+chr(13)
for xcounter = 2 to xctnqty
	**change to do ucc creation lookup in ppdfig as opposed to hardcoded just for BBG - mvw 01/31/18
*!*		lcuccid  = 6718
*!*		lcUccNum = sqlgenpk("6718UCCNUM","wh")
*!*		lcucctmp = "0000"+Alltrim(Str(lcuccid))+Padl(Alltrim(Str(lcUccNum)),11,"0")
	lcuccid=0
	lcUccNum=""
	lcucctmp=""
	execscript(xexecscript)
	**end changes - mvw 01/31/18

	xnewucc=lcucctmp

	dig1 = Val(Substr(xnewucc,1,1))
	dig2 = Val(Substr(xnewucc,2,1))
	dig3 = Val(Substr(xnewucc,3,1))
	dig4 = Val(Substr(xnewucc,4,1))
	dig5 = Val(Substr(xnewucc,5,1))
	dig6 = Val(Substr(xnewucc,6,1))
	dig7 = Val(Substr(xnewucc,7,1))
	dig8 = Val(Substr(xnewucc,8,1))
	dig9 = Val(Substr(xnewucc,9,1))
	dig10 = Val(Substr(xnewucc,10,1))
	dig11 = Val(Substr(xnewucc,11,1))
	dig12 = Val(Substr(xnewucc,12,1))
	dig13 = Val(Substr(xnewucc,13,1))
	dig14 = Val(Substr(xnewucc,14,1))
	dig15 = Val(Substr(xnewucc,15,1))
	dig16 = Val(Substr(xnewucc,16,1))
	dig17 = Val(Substr(xnewucc,17,1))
	dig18 = Val(Substr(xnewucc,18,1))
	dig19=  Val(Substr(xnewucc,19,1))

	lnsumodd = dig1+dig3+dig5+dig7+dig9+dig11+dig13+dig15+dig17+dig19
	lnsum = lnsumodd*3
	lnsumeven = dig2+dig4+dig6+dig8+dig10+dig12+dig14+dig16+dig18
	lntot=lnsum+lnsumeven
	lnckdigit = Mod(lntot,10)

	If lnckdigit#0
	  lnckdigit= 10-lnckdigit
	Endif

	xnewucc=xnewucc+transform(lnckdigit)
	aucc(xcounter)=xnewucc
	xuccstr=xuccstr+xnewucc+chr(13)

	if !xfullcase
		select wavelabels
		append blank
		replace uccnumber with left(xnewucc,19)
		replace cartonnum with left(xnewucc,19)
		replace chkdigit with transform(lnckdigit)

		locate for uccnumber=xolducc
		**all uccs will need to be manually scanned so no need for audit
		replace auditreq with .f.

		scatter fields except uccnumber, chkdigit memv
		m.adddt=datetime()
		m.recorder=9

		locate for uccnumber=left(xnewucc,19)
		gather memvar
		**add a suffix to additional cartons, ie "WO Carton 1 of 5" --> "WO Carton 1a of 5"
		replace n60 with strtran(n60," of ",chr(96+xcounter)+" of ")
	endif
endfor

xsqlexec("update cartons set deleteby='"+guserid+"', delproc='splitucc' where ucc='"+xucc+"'",,,"pickpack_sql5")
xsqlexec("delete from cartons where ucc='"+xucc+"'",,,"pickpack_sql5")

select xtmplabels
sum totqty to xtotqty
xtotqty=ceiling(xtotqty/xctnqty)
xqty=0
xcounter=1
xdatetime=datetime()

go top in xtmplabels &&need this since scan has a "while" clause
scan while insdttm#xdatetime
	do case
	case xqty+xtmplabels.totqty<=xtotqty
		replace ucc with aucc(xcounter), cartonnum with aucc(xcounter)
		xqty=xqty+xtmplabels.totqty

		if xqty=xtotqty
			xqty=0
			xcounter=xcounter+1
		endif
 	case xqty+xtmplabels.totqty>xtotqty &&split to 2 records
		scatter memvar
		replace ucc with aucc(xcounter), cartonnum with aucc(xcounter)
		replace qty with xtotqty-xqty, totqty with xtotqty-xqty

		xrecno=recno()
		xremqty=m.qty-xtmplabels.qty

		**may need to split this carton multiple times right here
		do while .t.
			xcounter=xcounter+1
			m.qty=min(xtotqty,xremqty)
			m.totqty=m.qty
			xqty=m.qty
			xremqty=xremqty-xqty
			m.ucc=aucc(xcounter)
			m.cartonnum=aucc(xcounter)
			m.insdttm=xdatetime
			insertinto("xtmplabels","pickpack_sql5")

			if xremqty<=0
				exit
			endif
		enddo

		go xrecno
	endcase
endscan

select sum(totqty) as dd, * from xtmplabels with (buffering = .t.) group by ucc into cursor xtemp
scan
	replace pack with transform(xtemp.dd) for ucc=xtemp.ucc in xtmplabels
	if !xfullcase
		replace pack with transform(xtemp.dd) for uccnumber=left(xtemp.ucc,19) in wavelabels
	endif
endscan
use in xtemp

tu("xtmplabels")

if !xfullcase and cursorgetprop("Buffering","wavelabels")#1
	=tableupdate(1,.t.,"wavelabels")
endif

info("SPLITUCC",xwonum,,xaccountid,xpt,,,,,"UCC Split","UCC List: "+xuccstr,xucc)
tu("info")

select wavelabels
locate for uccnumber=xolducc

**update outship.ctnqty and outwolog.ctnqty
if !xfullcase
	select wavelabels
	xwonum=wavelabels.wo_num
	xpt=wavelabels.pt
	sum 1, iif(pt=xpt,1,0) to xwoctnqty, xptctnqty for wo_num=xwonum

	xsqlexec("update outwolog set ctnqty="+transform(xwoctnqty)+" where wo_num="+transform(xwonum),,,"wh")
	xsqlexec("update outship set ctnqty="+transform(xptctnqty)+" where wo_num="+transform(xwonum)+" and ship_ref='"+alltrim(xpt)+"'",,,"wh")
endif

select * from xtmplabels order by ucc into cursor xtemp

xerror=.f.
try
	oexcel = createobject("excel.application")
	release oexcel
catch
	xerror=.t.
endtry

if xerror
	xfilename="h:\pdf\uccsplit"+xucc+".txt"
	copy fields ucc, upc, style, color, id, totqty, ship_ref to (xfilename) type sdf
else
	xfilename="h:\pdf\uccsplit"+xucc+".xls"
	copy fields ucc, upc, style, color, id, totqty, ship_ref to (xfilename) type xl5
endif


wshshell=createobject("wscript.shell")
try
	wshshell.run(xfilename)
catch
	x3winmsg("The file name "+trim(xfilename)+" is invalid")
endtry

use in xtmplabels
use in xtemp
