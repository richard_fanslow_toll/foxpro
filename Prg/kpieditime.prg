**Edi Timeliness KPI

Parameters cType, cFileType
**cType: "Inbound", "Outbound"
**cFileType: "944", "945"

**Time from actual delivery to the 945 being sent (edi accts) OR the updating delivery date
**  - Based on edi_trigger.trig_time (edi accts) or outShip.del_date Vs. ;
**		edi_trigger.when_proc (edi accts) or outShip.delenterdt
**

**define gofffice - added mvw 03/20/17
**xoffice=mod
goffice=xoffice
crptname=xrptname

Wait window "Gathering data for "+cType+" EDI Timeliness Spreadsheet..." nowait noclear

if !used('edi_trigger')
	cedifolder = iif(date()<{^2010-01-24},"F:\JAG3PL\DATA\","F:\3PL\DATA\")
	use (cedifolder+"edi_trigger") in 0 alias edi_trigger
endif

USE f:\wo\wodata\wolog In 0

Select edi_trigger
Locate for accountId = &cAcctNum and edi_type = cFileType
Store Iif(Found(), .t., .f.) to lEdiAcct

If cType = "Inbound" and !lEdiAcct
	strMsg = "This account is not setup for inbound EDI transmission."
	=MessageBox(strMsg, 48, cType+" EDI Timeliness")
Else
	DO case
	  Case cType = "Inbound"
		xsqlexec("select * from inWolog where accountid="+transform(&cAcctNum)+" and between(confirmDt,{"+dtoc(dStartDt)+"},{"+dtoc(dEndDt)+"})","csrDetail",,"wh")
		
		**take into account the 3 hr time difference for cali (-10800 secs)
		Select c.wo_num, c.wo_date, Max(Iif(Empty(w.pickedUp), w.wo_date, w.pickedUp)) as pickedUp, ;
			   Ttod(c.confirmDt) as confirmDt, Ttod(e.trig_time-Iif(inlist(cOffice,"C","L","R"),10800,0)) as trig_time, ;
			   Ttod(e.when_proc-Iif(inlist(cOffice,"C","L","R"),10800,0)) as processTm, edi_type, 00 as dateDiff, ;
			   00 as dateDiff2, 00 as dateDiff3 ;
			from csrDetail c ;
				left join edi_trigger e on c.wo_num = e.wo_num ;
				left join wolog w on c.wo_num = w.inwo ;
			where !Empty(when_proc) and !IsNull(when_proc) and edi_type = cFileType ;
			group by c.wo_num order by c.wo_date, c.wo_num ;
		  into cursor csrDetail readWrite

		replace pickedUp with confirmDt for IsNull(pickedUp)

		**calculate date differential into dateDiff - make necessary adjustments for weekends & FMI holidays
		DO kpicalcdiff with "confirmDt", "trig_time", "dateDiff"
		DO kpicalcdiff with "trig_time", "processTm", "dateDiff2"
		DO kpicalcdiff with "pickedUp", "processTm", "dateDiff3"
		cDesc = "Edi Trigger Date Vs. Confirmed Empty Date"
		cDateFld = "confirmDt"

	  Case cType = "Outbound" and lEdiAcct
		xquery="select * from outship where accountid = "+cAcctNum+" and del_date >= {"+transform(dStartDt)+"} and del_date <= {"+transform(dEndDt)+"} and notOnWip=0 and mod = '"+goffice+"'"
		xsqlexec(xquery,"csrdetail",,"wh",,,,,,,.t.)

		**take into account the 3 hr time difference for cali (-10800 secs)
		Select c.outshipId, c.wo_num, c.wo_date, c.ctnQty, c.del_date, Ttod(e.trig_time-Iif(inlist(cOffice,"C","L","R"),10800,0)) as trig_time, ;
			   Ttod(e.when_proc-Iif(inlist(cOffice,"C","L","R"),10800,0)) as processTm, edi_type, c.ship_ref, ;
			   e.ship_ref as ediShipRef, 00 as dateDiff, 00 as dateDiff2 ;
			from csrDetail c ;
				left join edi_trigger e on c.outshipId = e.outshipId ;
			where !Empty(when_proc) and !IsNull(when_proc) and edi_type = cFileType ;
			group by c.outshipId order by c.wo_date, c.wo_num, c.ship_ref ;
		  into cursor csrDetail readWrite

		**calculate date differential into dateDiff - make necessary adjustments for weekends & FMI holidays
		DO kpicalcdiff with "del_date", "trig_time", "dateDiff"
		DO kpicalcdiff with "trig_time", "processTm", "dateDiff2"
		cDesc = "Edi Trigger Date Vs. Delivery Date"
		cDateFld = "del_date"

	  Case cType = "Outbound"
		xquery="select * from outship where accountid = "+cAcctNum+" and between(del_date,{"+transform(dStartDt)+"},{"+transform(dEndDt)+"}) and mod = '"+goffice+"'"
		xsqlexec(xquery,"csrdetail",,"wh",,,,,,,.t.)

		select wo_num, wo_date, ship_ref, ctnQty, del_date, Ttod(delEnterDt-Iif(inlist(cOffice,"C","L","R"),10800,0)) as delEnterDt, ;
				00 as dateDiff, 00 as dateDiff2 ;
			from csrdetail ;
			where !Empty(delEnterDt) ;
		  into cursor csrDetail readWrite

		**calculate date differential into dateDiff - make necessary adjustments for weekends & FMI holidays
		DO kpicalcdiff with "del_date", "delEnterDt", "dateDiff"
		cDesc = "Delivery Date Update Vs. Actual Delivery Date"
		cDateFld = "del_date"
	EndCase

	Locate
	If Eof()
		strMsg = "No data found."
		=MessageBox(strMsg, 48, cType+" EDI Timeliness")
	Else
		Wait window "Creating "+cType+" EDI Timeliness Spreadsheet..." nowait noclear
		oWorkbook=""
		cComment = " - Totals indicate total "+Iif(cType = "Inbound", "work order", "pick ticket")+" count."
		DO xlsTimeGeneric with "", cDateFld, "1", "", cType+" EDI Timeliness", cDesc, cComment, ;
			" - Dates reflect local times.", "''", Iif(lEdiAcct, .f., .t.), .f.

		**need to add another summary section for EDI Accts
		If lEdiAcct
			nRow = Reccount("csrTotals")+4+6 &&insert just b4 notes at reccount()+4+8
			oWorkbook.workSheets[1].Range("A3:M4").copy()
			oWorkbook.workSheets[1].Range("A"+Alltrim(Str(nRow))).pasteSpecial()

			oworkbook.worksheets[1].range("C"+Alltrim(Str(nRow))).value = "EDI Process Date Vs. Trigger Date"

			**cusror csrTotals is created within xlsTimeGenSummary()
			DO xlsTimeGenSummary with "", cDateFld, "1", "", cComment, ;
				" - Dates reflect local times.", "''", .f., "dateDiff2", "reccount()+12", .f.

			**3rd section
			nRow = ((Reccount("csrTotals")+4+5)*2)-1 &&insert just b4 notes at reccount()+4+8
			oWorkbook.workSheets[1].Range("A3:M4").copy()
			oWorkbook.workSheets[1].Range("A"+Alltrim(Str(nRow))).pasteSpecial()

			If cType = "Inbound"
				oworkbook.worksheets[1].range("C"+Alltrim(Str(nRow))).value = "EDI Process Date Vs. Picked Up Date"
				**cusror csrTotals is created within xlsTimeGenSummary()
				DO xlsTimeGenSummary with "", cDateFld, "1", "", cComment, ;
					" - Dates reflect local times.", "''", .t., "dateDiff3", "((reccount()+10)*2)-1", .f.
			Else
				oworkbook.worksheets[1].range("C"+Alltrim(Str(nRow))).value = "EDI Process Date Vs. Delivery Date"
				**cusror csrTotals is created within xlsTimeGenSummary()
				DO xlsTimeGenSummary with "", cDateFld, "1", "", cComment, ;
					" - Dates reflect local times.", "''", .t., "dateDiff+dateDiff2", "((reccount()+10)*2)-1", .f.
			EndIf
		EndIf

		If lDetail
			Select csrTotals
			nWorksheet = 2
			Scan
				STORE tempfox+SUBSTR(SYS(2015), 3, 10)+".xls" TO cTmpFile
				do case
				case lEdiAcct and cType = "Outbound"
					**tt2, pt2 needed b/c can not copy dup fields
					Select *, "" as emptyCol, "" as emptyCol2, trig_time as tt2, processTm as pt2 from csrDetail ;
						where Year(&cDateFld) = csrTotals.year and Month(&cDateFld) = csrTotals.month into cursor csrDet

					Copy to &cTmpFile fields wo_num, wo_date, ship_ref, ctnQty, del_date, trig_time, dateDiff, emptyCol, ;
						tt2, processTm, dateDiff2 type xl5
					xcolhdrs="'W/O No','W/O Dt','PT No','Ctn Qty','Ship Dt','Trigger Dt','Diff','','Trigger Dt','EDI Sent','Diff'"

				case lEdiAcct and cType = "Inbound"
					**tt2, pt2 needed b/c can not copy dup fields
					Select *, "" as emptyCol, "" as emptyCol2, trig_time as tt2, processTm as pt2 from csrDetail ;
						where Year(&cDateFld) = csrTotals.year and Month(&cDateFld) = csrTotals.month into cursor csrDet

					Copy to &cTmpFile fields wo_num, wo_date, confirmDt, trig_time, dateDiff, emptyCol, ;
						tt2, processTm, dateDiff2, emptyCol2, pickedUp, pt2, dateDiff3 type xl5
					xcolhdrs="'W/O No','W/O Dt','Confirm Dt','Trigger Dt','Diff','','Trigger Dt','EDI Sent','Diff','','Pick Up Dt','EDI Sent','Diff'"
				otherwise &&not ediacct
					Select * from csrDetail where Year(&cDateFld) = csrTotals.year and Month(&cDateFld) = csrTotals.month into cursor csrDet
					Copy to &cTmpFile fields wo_num, wo_date, ship_ref, ctnQty, del_date, delEnterDt, dateDiff type xl5
					xcolhdrs="'W/O No','W/O Dt','PT No','Ctn Qty','Ship Dt','Deliv Enter Dt','Diff'"
				endcase

				DO xlsTimeGenDetail with cTmpFile, nWorkSheet, Iif(lEdiAcct, "M", "G"), &xcolhdrs
				nWorksheet = nWorksheet+1
			EndScan
			USE in csrDet
		EndIf

		oWorkbook.Worksheets[1].range("A1").activate()
		oWorkbook.Save()
	EndIf
EndIf

use in edi_trigger
if used('outship')
	use in outship
endif
use in wolog

if used("csrdetail")
  use in csrdetail
endif
if used("csrtotals")
  use in csrtotals
endif
