PARAMETERS cOffice,nAcctNum
* call this like this: do m:\DEV\prg\modernshoeinbound with "C",5864

*runack("WMSINBOUND")

IF VARTYPE(cOffice) # "C"
	cOffice = "C"
ENDIF

IF VARTYPE(nAcctNum) = "C"
	nAcctNum = INT(VAL(nAcctNum))
ENDIF

PUBLIC goffice,tsendto,tcc,lcAccountname,lCartonAcct,lDoGeneric,lCreateErr,PNAME,lHoldFile,lDoMix,nLineStart,cMod
PUBLIC m.style,m.color,m.id,m.size,m.pack,cTransfer,lTest,lAllPicks,lcWHPath,lnormalexit,lcErrorMessage,newAcctNum,lDoPL,cWhseMod
PUBLIC cAcctname,lAcctName,xacctnum,lPrepack,nPrepacks,nPickpacks,tFrom,lProcError,thisfile,lcpath,m.mod,m.office
PUBLIC cSLError,lPik,lPre,cMessage,cFilename,lTestmail,lModErr,xfile,cMyFile,lXLSX,archivefile,lDelDBF,plqty1,plqty2,lcErrorMessage

STORE "" TO m.style,m.color,m.id,m.size,m.pack,lcBrokerref,cMessage,cFilename,xfile
nAcctNum = 4752

CLOSE DATABASES ALL
SET PATH TO
SET PATH TO "M:\DEV\PRG\"
DO m:\dev\prg\_setvars WITH .T.

SET STATUS BAR ON
ON ESCAPE CANCEL

DO m:\dev\prg\lookups
llContinue =.T.
lcErrorMessage=""

lTest = .F.  && This directs output to F:\WHP\WHDATA test files (default = .f.)
lTestmail = .F. &&lTest && Causes mail to go to default test recipient(s)
lHoldFile = .F. && Causes error CATCH message; on completion with no errors, changed to .t. (default = .f.)
lBrowse = lHoldFile  && If this is set with lHoldFile, browsing will occur at various points (default = .f.)
lOverrideBusy = IIF(lHoldFile OR lTest,.T.,.F.) && If this is set, CHKBUSY flag in FTPSETUP will be ignored (default = .f.)

*lOverrideBusy = .T.

lAllPicks = .F.  && If this is set, all inbounding will be done as units...should not be used (default = .f.)
lDoSLError = .F.  && This flag will be set if there is missing info in the Courtaulds inbound sheet
lXLSX = .F.
lDelDBF = .T.

TRY
	useca("inrcv","wh")
	xfile = " "
	IF !lTest AND !lHoldFile
		ON ERROR THROW
	ENDIF

	_SCREEN.CAPTION = "Highline Inbound - Work Order Builder"
	_SCREEN.WINDOWSTATE=IIF(lHoldFile OR lTest,2,1)

	IF TYPE("nAcctNum") = "C"
		nAcctNum = VAL(nAcctNum)
	ENDIF

	tFrom = "TGF Modern Shoe Inbound EDI System Operations <inbound-ops@fmiint.com>"
	lDoGeneric = .F.
	lCreateErr = .F.
	lMainMail = .T.
	tsendto = ""
	tcc = ""
	cSLError = ""
	lCartonAcct = .F.
	m.office = cOffice
	cTransfer = "PL-MODERNSHOE-CA"
	cMod = "2"
	m.mod = cMod

	goffice = cMod
	gMasteroffice = cOffice

	lDoPL = .T.

	cWhseMod = LOWER("wh"+cMod)

	IF !lTest
	ASSERT .F. MESSAGE "At ftpsetup stage"
	LOCATE FOR UPPER(ftpsetup.transfer) = UPPER(cTransfer)
	IF FOUND()
		IF !lHoldFile AND !lBrowse AND !lOverrideBusy
			IF ftpsetup.CHKBUSY=.T.
				SET STEP ON
				WAIT WINDOW AT 10,10  "This transfer is busy...will try again later."+CHR(13)+cTransfer TIMEOUT 1
				lnormalexit = .T.
				THROW
			ELSE
				REPLACE chkbusy WITH .T.,trig_time WITH DATETIME() IN ftpsetup NEXT 1
			ENDIF
		ENDIF
	ENDIF
	endif

	IF lTest
		WAIT WINDOW "This is a test Modern Shoe inbound upload into WHP tables" TIMEOUT 2
	ENDIF

	IF USED('mm')
		USE IN mm
	ENDIF
	SELECT 0
	USE F:\3pl\DATA\mailmaster ALIAS mm
	LOCATE FOR (mm.accountid = nAcctNum) AND (mm.office = cOffice) AND edi_type = "PL"

	IF FOUND()
		STORE TRIM(mm.acctname) TO lcAccountname
		STORE TRIM(mm.basepath) TO lcpath
		STORE TRIM(mm.archpath) TO lcArchivePath
		tsendto = ALLTRIM(IIF(mm.use_alt,mm.sendtoalt,mm.sendto))
		tcc = ALLTRIM(IIF(mm.use_alt,mm.ccalt,mm.cc))
		STORE TRIM(mm.fmask)    TO filemask

		STORE TRIM(mm.progname) TO PNAME
		STORE TRIM(mm.scaption) TO _SCREEN.CAPTION
		STORE mm.CtnAcct  	TO lCartonAcct
		STORE mm.dogeneric TO lDoGeneric
		LOCATE FOR edi_type = "MISC" AND taskname = "GENERAL"
		tsendtoerr = ALLTRIM(IIF(mm.use_alt,mm.sendtoalt,mm.sendto))
		tccerr = ALLTRIM(IIF(mm.use_alt,mm.ccalt,mm.cc))
		tsendtotest = ALLTRIM(IIF(mm.use_alt,mm.sendtoalt,mm.sendto))
		tcctest = ALLTRIM(IIF(mm.use_alt,mm.ccalt,mm.cc))
		USE IN mm
	ELSE
		lnormalexit=.T.
		WAIT WINDOW AT 10,10  "No parameters set for this acct# "+ALLTRIM(STR(nAcctNum))+"  ---> Office "+cOffice TIMEOUT 2
		THROW
	ENDIF
	CD &lcpath

	IF lTest OR lHoldFile
		tsendto = tsendtotest
		tcc = tcctest
	ENDIF

	WAIT WINDOW AT 10,10  "Now setting up a WMS Inbound for "+lcAccountname+"  ---> Office "+cOffice TIMEOUT 1

	SELECT 0

	xsqlexec("select * from account where inactive=0","account",,"qq")
	INDEX ON accountid TAG accountid

	SELECT account

	IF SEEK(nAcctNum,"account","accountid")
		cAcctname= account.acctname
	ELSE
		lnormalexit=.T.
		WAIT WINDOW AT 10,10 "No account name on file.........." TIMEOUT 2
		CLOSE DATABASES ALL
		THROW
	ENDIF

	IF lTest
		lcWHPath = "F:\WHP\WHDATA\"
	ELSE
		xreturn = "X"
		DO m:\dev\prg\wf_ALT WITH cOffice,nAcctNum
		lcWHPath = UPPER(xreturn)
		IF xreturn = "X"
			WAIT WINDOW "No such account for this office" TIMEOUT 2
			THROW
		ENDIF
	ENDIF

	SET PATH TO '&lcWHPath' ADDITIVE
	lnNum = ADIR(tarray,filemask)

	IF lnNum = 0
		lnormalexit=.T.
		WAIT WINDOW AT 10,10 "    No Inbound files to process for "+lcAccountname+"   office--> "+cOffice  TIMEOUT 2
		THROW
	ENDIF

	FOR thisfile = 1  TO lnNum
		IF USED('wolog')
			USE IN wolog
		ENDIF
		IF USED('inwolog')
			USE IN inwolog
		ENDIF
		IF USED('pl')
			USE IN pl
		ENDIF

		USE F:\wo\wodata\wolog IN 0 ALIAS wolog
		useca("inwolog","wh")

		useca("pl","wh")

		IF USED("generatepk")	&& dy 11/18/09
			USE IN generatepk
		ENDIF

*		IF USED("whgenpk")	&& jb 04.22.2010
*			USE IN whgenpk
*		ENDIF
*		IF USED("genpk")	&& jb 04.22.2010
*			USE IN genpk
*		ENDIF
*		USE &lcWHPath.whgenpk IN 0 ALIAS whgenpk

		IF USED('upcmast')
			USE IN upcmast
		ENDIF
*		USE F:\wh\upcmast IN 0 ALIAS upcmast SHARED

		CREATE CURSOR wos ( ;
			filename   c(100),;
			wo_num     N(15),;
			CONTAINER  c(18), ;
			sku_units  N(6),;
			cartons    N(6),;
			quantity   N(10), ;
			sorts      N(6),;
			season     c(25),;
			computer   c(1))

		CREATE CURSOR oldwos ( ;
			filename   c(100),;
			wo_num     N(15),;
			wo_date    d,;
			CONTAINER  c(18), ;
			sku_units  N(6),;
			cartons    N(6),;
			quantity   N(10), ;
			sorts      N(6),;
			computer   c(1))

		IF llContinue = .F.
			NormalExit = .T.
			CLOSE DATABASES ALL
			THROW
		ELSE
			USE IN 201
		ENDIF

		IF nAcctNum = 4752
* 			place a copy of the file here so Softshare ECS/Delta will generate a 997 acknowledgement
			xfile = lcpath+TRIM(tarray[thisfile,1])
			ackfile = "F:\FTPUSERS\ModernShoe\ASN-997\"+TRIM(tarray[thisfile,1])
			COPY FILE [&xfile] TO [&ackfile]
		ENDIF


*		ASSERT .F. MESSAGE "At main process ATTRIB change point...DEBUG"
		xfile = lcpath+TRIM(tarray[thisfile,1])
		attrfile = '"'+lcpath+TRIM(tarray[thisfile,1])+'"'
		archivefile = lcArchivePath+TRIM(tarray[thisfile,1])
		IF !INLIST(nAcctNum,6035,4610,4694)
			!ATTRIB -R &attrfile  && Removes read-only flag from file to allow deletion
		ENDIF

		WAIT WINDOW "Importing file: "+xfile TIMEOUT 2

		IF FILE(xfile)
			IF UPPER(JUSTEXT(xfile)) = "XLSM"
				WAIT WINDOW "Invalid Excel Format TYPE" TIMEOUT 2
				DELETE FILE [&xfile]
				THROW
			ENDIF

			IF INLIST(nAcctNum,1285,5432,6097,6209,6242)
				IF UPPER(JUSTEXT(xfile)) = "XLSX"
					lXLSX = .T.
					DO m:\dev\prg\xlsx_conversion WITH xfile
				ELSE
					WAIT WINDOW "No file conversion for account "+TRANSFORM(nAcctNum) TIMEOUT 2
				ENDIF
			ENDIF

*			useca("upcmast","wh",,,,,"upcmastsql")

			DO &PNAME WITH xfile,nAcctNum,cAcctname,cOffice

			SELECT pl
			COUNT TO xnumskus FOR !units AND wo_num=inwolog.wo_num

			xsqlexec("select * from inrcv where container='"+inwolog.CONTAINER+"' and status#'CONFIRMED'","xinrcv",,"wh")
			SELECT xinrcv

			IF RECCOUNT() > 0
				xsqlexec("update inrcv set inwonum="+TRANSFORM(inwolog.wo_num)+" where container='"+inwolog.CONTAINER+"' and status#'CONFIRMED'",,,"wh")
			ELSE
				SELECT inrcv
				SCATTER MEMVAR MEMO BLANK
				m.inrcvid=dygenpk("inrcv","whall")  && new get the ID value
				m.inwonum=inwolog.wo_num
				m.accountid=inwolog.accountid
				m.acctname=inwolog.acctname
				m.container=inwolog.CONTAINER
				m.status="NOT AVAILABLE"
				m.skus=xnumskus
				m.cartons=inwolog.plinqty
				m.newdt=DATE()
				m.addfrom="IN"
				m.confirmdt=empty2nul(m.confirmdt)
				m.newdt=empty2nul(m.newdt)
				m.dydt=empty2nul(m.dydt)
				m.firstavaildt=empty2nul(m.firstavaildt)
				m.apptdt=empty2nul(m.apptdt)
				m.pickedup=empty2nul(m.pickedup)
				m.office="C"	&& dy 1/9/18
				m.mod="2"
				INSERT INTO inrcv FROM MEMVAR
				tu("inrcv")
			ENDIF

			SELECT wos
			nWOSRecs = RECCOUNT()
			cfilenamein = wos.filename

			LOCATE

			IF lMainMail AND (wos.cartons>0 OR wos.quantity>0)

				IF INLIST(nAcctNum,&gtkohighlineaccounts)
					cAcctname = "MODERN SHOE/HIGHLINE"
				ENDIF


				EmailcommentStr = REPLICATE("-",80)+CHR(13)
				EmailcommentStr = EmailcommentStr+"Packinglist upload run from machine: "+SYS(0)+CHR(13)
				EmailcommentStr = EmailcommentStr+REPLICATE("-",80)+CHR(13)
				EmailcommentStr = EmailcommentStr+"Inbound Workorders created for "+cAcctname+":"+CHR(13)
				EmailcommentStr = EmailcommentStr+"Data from File : "+cfilenamein+CHR(13)
				EmailcommentStr = EmailcommentStr+REPLICATE("-",80)+CHR(13)

				SELECT wos
				SCAN
					EmailcommentStr = EmailcommentStr+"("+ALLT(wos.acctcode)+") WO#: "+ALLTRIM(TRANSFORM(wos.wo_num))+;
						", Cust BOL: "+ALLTRIM(wos.REFNUM)+",  Ctns: "+PADL(ALLTRIM(STR(wos.cartons)),5)+;
						", Pairs: "+ALLTRIM(STR(wos.sku_units))+"  Ctr/AWB: "+ALLTRIM(wos.CONTAINER)+CHR(13)+IIF(wos.accrue," (*ACCRUED*)","")+CHR(13)
				ENDSCAN
				WAIT CLEAR
				tsubject= "Inbound WO(s) created for "+ALLTRIM(cAcctname)+" at "+TTOC(DATETIME())
				tattach = ""
				IF USED('mm')
					USE IN mm
				ENDIF
				SELECT 0
				USE F:\3pl\DATA\mailmaster ALIAS mm
				IF INLIST(nAcctNum,5864,5910)
					LOCATE FOR mm.edi_type = "MISC" AND mm.taskname = "ADDHIGHLINE"
					IF EMPTY(tcc)
						tcc = IIF(mm.usealt,ALLTRIM(mm.ccalt),ALLTRIM(mm.cc))
					ELSE
						tcc = tcc+","+IIF(mm.usealt,ALLTRIM(mm.ccalt),ALLTRIM(mm.cc))
					ENDIF
				ENDIF
				IF lTestmail OR lTest
					LOCATE FOR edi_type = "MISC" AND taskname = "GENERAL"
					tsendto = ALLTRIM(IIF(mm.use_alt,mm.sendtoalt,mm.sendto))
					tcc = ALLTRIM(IIF(mm.use_alt,mm.ccalt,mm.cc))
				ENDIF
				USE IN mm

				tmessage = EmailcommentStr

*				ASSERT .F. MESSAGE "At email process...debug"
				IF !lTest
					SELECT wos
					nWOSCtns = wos.cartons
				ENDIF
				DO FORM m:\dev\frm\dartmail2 WITH tsendto,tFrom,tsubject,tcc,tattach,tmessage,"A"
			ENDIF
		ENDIF

*		SET STEP ON
		IF !lHoldFile && and lTest
			IF FILE(xfile)
				IF !lXLSX  && If file hadn't already been converted
					COPY FILE [&xfile] TO [&archivefile]
				ENDIF
				DELETE FILE [&xfile]
			ENDIF
		ENDIF
	ENDFOR

	lnormalexit = .T.
	IF lnormalexit = .T.
		WAIT WINDOW AT 10,10 "Normal Exit " TIMEOUT 2
		SELECT ftpsetup
		REPLACE ftpsetup.CHKBUSY WITH .F. FOR UPPER(ftpsetup.transfer) = UPPER(cTransfer) IN ftpsetup
	ENDIF

CATCH TO oErr
	IF lnormalexit = .F.
		ASSERT .F. MESSAGE "AT MAIN CATCH SECTION"
		SET STEP ON
		tmessage = "WMS Inbound Error..... Please fix me........!"
		lcSourceMachine = SYS(0)
		lcSourceProgram = SYS(16)

		tmessage =tmessage+ "Try/CATCH Exeception Message:"+CHR(13)+;
			[  Error: ] + STR(oErr.ERRORNO) +CHR(13)+;
			[  LineNo: ] + STR(oErr.LINENO) +CHR(13)+;
			[  Message: ] + oErr.MESSAGE +CHR(13)+;
			[  Procedure: ] + oErr.PROCEDURE +CHR(13)+;
			[  Details: ] + oErr.DETAILS +CHR(13)+;
			[  StackLevel: ] + STR(oErr.STACKLEVEL) +CHR(13)+;
			[  LineContents: ] + oErr.LINECONTENTS+CHR(13)+;
			[  UserValue: ] + oErr.USERVALUE+CHR(13)+;
			[  Computer:  ] +lcSourceMachine+CHR(13)+;
			[  Program:   ] +lcSourceProgram

		tsubject = "WMS Inbound Error ("+TRANSFORM(oErr.ERRORNO)+") at "+TTOC(DATETIME())
		tattach  = " "
		IF USED('mm')
			USE IN mm
		ENDIF
		SELECT 0
		USE F:\3pl\DATA\mailmaster ALIAS mm
		LOCATE FOR edi_type = "MISC" AND taskname = "GENERAL"
		lUseAlt = mm.use_alt
		tsendto = ALLTRIM(IIF(lUseAlt,mm.sendtoalt,mm.sendto))
		tcc = ALLTRIM(IIF(lUseAlt,mm.ccalt,mm.cc))
		USE IN mm
		WAIT WINDOW "SEND TO: "+tsendto+CHR(13)+"CC: "+tcc TIMEOUT 2
		DO FORM m:\dev\FRM\dartmail2 WITH tsendto,tFrom,tsubject,tcc,tattach,tmessage,"A"
	ELSE
		WAIT WINDOW AT 10,10 "Normal Exit " TIMEOUT 2
	ENDIF
FINALLY
	SELECT 0
	CLOSE DATA ALL
	SET STATUS BAR ON
	ON ERROR
	closedata()
ENDTRY

********************************
PROCEDURE ModernShoeInbound
********************************

PARAMETERS xfile,xacctnum,xacctname,cOffice

IF USED('wolog')
	USE IN wolog
ENDIF

USE F:\wo\wodata\wolog IN 0 ALIAS wolog

IF USED('wos')
	USE IN wos
ENDIF

CREATE CURSOR wos (;
	filename   c(100),;
	wo_num     N(15),;
	CONTAINER  c(18),;
	REFNUM     c(25),;
	sku_units  N(6),;
	cartons    N(6),;
	quantity   N(10),;
	sorts      N(6),;
	season     c(25),;
	computer   c(1),;
	acctcode   c(2),;
	accrue     l)

SELECT wos  && use this for the email alert
ZAP

SELECT * FROM inwolog WHERE .F. INTO CURSOR xinwolog READW
SELECT * FROM pl WHERE .F. INTO CURSOR xpl READW
CREATE CURSOR New_in (GROUP N(3),;
	accountid N(5),;
	coname c(15),;
	units  l,;
	STYLE c(20),;
	COLOR c(10),;
	ID    c(10),;
	SIZE  c(10),;
	PACK  c(10),;
	LENGTH N(4),;
	WIDTH  N(4),;
	ECHO m,;
	upc    c(14),;
	acct_ref c(20),;
	CONTAINER c(14),;
	REFERENCE c(20),;
	depth  N(4),;
	ctnwt N(4),;
	skuqty N(6), ;
	ctnqty N(4),;
	totqty N(4),;
	comments m)

DO m:\dev\prg\createx856a

m.date_rcvd=DATE()
m.plinqty=0

SELECT x856

DO m:\dev\prg\loadedifile WITH xfile,"*","MODERNSHOE","UNKNOWN"

SELECT x856
DELETE FOR EMPTY(segment)

SELECT New_in
SCATTER MEMVAR MEMO BLANK

SELECT x856
LOCATE
lnGroupNum = 0
lcCurrentContainer = "zzzzzzzzzz"
cDivString = ""

ASSERT .F. MESSAGE "At Start of MODSHOE ASN process"
m.packunits = ""
DO WHILE !EOF('x856')
	IF x856.segment = "ST"
		SKIP 1 IN x856
		DO WHILE x856.segment # "ST"
			IF x856.segment = "TD3"
				lAir = IIF(ALLTRIM(x856.f16)="AIR" OR ALLTRIM(x856.f9)="AIR",.T.,.F.)
				m.container = ALLTRIM(x856.f2)+ALLTRIM(x856.f3)
				IF lcCurrentContainer != m.container
					lcCurrentContainer = m.container
					lnGroupNum = lnGroupNum +1
					m.group = lnGroupNum
				ENDIF
			ENDIF
			IF x856.segment = "PRF"
*					ASSERT .F. MESSAGE "In PO field"
				lcCurrentPO = ALLTRIM(x856.f1)
				m.acct_ref = ALLTRIM(x856.f1)
			ENDIF
			IF x856.segment = "REF" AND ALLTRIM(x856.f1) = "BM"
				m.reference = ALLTRIM(x856.f2)
				lcBrokerref = ALLTRIM(x856.f2)
			ENDIF
			IF x856.segment = "LIN"
				m.color = UPPER(ALLTRIM(x856.f3))
			ENDIF

			IF x856.segment = "MEA" AND ALLTRIM(x856.f2) = "SU"
				m.packunits = UPPER(ALLTRIM(x856.f3))
			ENDIF
			IF !EMPTY(m.packunits)
				IF x856.segment = "MEA" AND ALLTRIM(x856.f2) = "NM"
					cCtnQty = ALLTRIM(x856.f3)
					m.ctnqty = VAL(cCtnQty)
					m.totqty = VAL(cCtnQty)
					IF EMPTY(m.packunits)
						lcErrorMsg = "MEA*SU MISSING"
						lnormalexit = .F.
						THROW
					ENDIF
					m.pack = TRANSFORM(VAL(ALLTRIM(m.packunits))/m.ctnqty)
					m.skuqty = VAL(m.pack)*m.ctnqty
					m.packunits = ""
				ENDIF
			ENDIF

			IF x856.segment = "REF" AND ALLTRIM(x856.f1) = "OB"  && Added 10.21.2008, req by Joe Smith.
				IF !EMPTY(ALLTRIM(x856.f2))
					cDivString = "DIVCODE*"+UPPER(ALLTRIM(x856.f2))
					m.comments = cDivString
				ENDIF
				cDivString = ""
			ENDIF
			IF x856.segment = "REF" AND ALLTRIM(x856.f1) = "P1"
				m.style = UPPER(ALLTRIM(x856.f2))
				m.upc  = UPPER(ALLTRIM(x856.f2))
			ENDIF
			IF x856.segment = "REF" AND ALLTRIM(x856.f1) = "LI"
				cLI = ALLTRIM(x856.f2)
				m.echo = "PO*"+lcCurrentPO
				m.echo = m.echo+CHR(13)+"LI*"+cLI
				m.echo = m.echo+CHR(13)+"ORIGPOINFO*"+lcCurrentPO+"|"+cLI+"|"+cCtnQty  && Contains PO, Line# and totqty
			ENDIF
			IF x856.segment = "N1" AND ALLTRIM(x856.f1) = "CO"
				cCOString = "CONAME*"+UPPER(ALLTRIM(x856.f2))
				m.coname = UPPER(ALLTRIM(x856.f2))
				m.accountid = ICASE(m.coname = "HIGHLINE",5864,m.coname = "NEW ALAGUNA",6414,4752)
				IF !cCOString$m.comments
					m.comments = IIF(EMPTY(m.comments),cCOString,m.comments+CHR(13)+cCOString)
				ENDIF
				cCOString = ""
			ENDIF
			IF x856.segment = "SAC"
				SELECT New_in
				APPEND BLANK
				GATHER MEMVAR MEMO
				m.echo=""
				SELECT x856
				SKIP 1 IN x856
			ELSE
				SELECT x856
				IF !EOF("X856")
					SKIP 1 IN x856
				ELSE
					EXIT
				ENDIF
			ENDIF
		ENDDO
	ENDIF
	SELECT x856
	IF !x856.segment = "ST"
		IF !EOF("X856")
			SKIP 1 IN x856
		ELSE
			EXIT
		ENDIF
	ENDIF
ENDDO
SELECT New_in
IF lTest OR DATETIME() < DATETIME(2012,09,06,17,00,00)
*!*			BROWSE TIMEOUT 60
*!*			SET STEP ON
ENDIF

* each of these will be a new work order
SELECT * FROM New_in INTO CURSOR twos GROUP BY accountid,GROUP,CONTAINER

STORE 0 TO m.inwologid, m.plid, m.wo_num
SELECT twos
IF lBrowse OR lHoldFile
*!*			BROWSE
*!*			ASSERT .F.
ENDIF
SCAN
	xacctname = ICASE(coname="HIGHLINE","HIGHLINE UNITED LLC",coname="NEW ALAGUNA","NEW ALAGUNA","MODERN SHOE COMPANY LLC")
	xacctnum = ICASE(coname="HIGHLINE",5864,coname="NEW ALAGUNA",6414,4752)
	SELECT xinwolog
	m.wo_num = m.wo_num + 1
	m.inwologid = m.inwologid + 1
	m.addby='MODSHOE'
	m.addproc="MODSHOEPL"
	m.adddt=DATETIME()
	m.plinqty=0
	m.picknpack = .F.
	m.toteaches = 0
	m.reference = twos.REFERENCE
	m.brokerref = twos.REFERENCE
	m.comments = twos.comments
	m.quantity=0
	m.container = twos.CONTAINER
	m.totctnqty =0
	m.adddt = {}
	m.adddt=DATETIME()
	m.wo_date=DATE()
	m.accountid=xacctnum
	m.acctname=xacctname
	m.wo_date=DATE()
	INSERT INTO xinwolog FROM MEMVAR
	m.quantity = 0

* now build the detail lines for each work order
	SELECT New_in
	SCAN FOR New_in.GROUP = twos.GROUP AND New_in.accountid = twos.accountid
		SELECT New_in
		SCATTER FIELDS EXCEPT accountid,wo_num,inwologid MEMVAR MEMO
		m.date_rcvd = DATE()
		m.plid = m.plid + 1
		INSERT INTO xpl FROM MEMVAR
	ENDSCAN

	SELECT New_in
	LOCATE
	SUM ctnqty,skuqty TO num_cartons,num_pairs FOR New_in.GROUP= twos.GROUP AND New_in.accountid = twos.accountid

	SELECT xinwolog
	REPLACE xinwolog.plinqty WITH num_cartons IN xinwolog

ENDSCAN

SELECT xpl
LOCATE
IF lTest OR lHoldFile
*		BROWSE
	LOCATE
ENDIF

nTotQty = 0
nCnt1 = 0
cMsg = "At XPL rollup scanning"
WAIT WINDOW cMsg NOWAIT
*	ASSERT .F. MESSAGE "At XPL rollup scanning"
IF lHoldFile
	SET STEP ON
ENDIF

SCAN FOR !DELETED()
	nRec = RECNO()
	SCATTER FIELDS inwologid,accountid,STYLE,COLOR,PACK MEMVAR
	IF lHoldFile
		SET STEP ON
	ENDIF

	COUNT TO nCnt1 FOR xpl.inwologid = m.inwologid AND ;
		xpl.accountid = m.accountid AND ;
		xpl.STYLE = m.style AND ;
		xpl.COLOR = m.color AND ;
		xpl.PACK = m.pack AND ;
		RECNO() > nRec

	IF nCnt1 > 0
		LOCATE
		SCAN FOR xpl.inwologid = m.inwologid AND ;
				xpl.accountid = m.accountid AND ;
				xpl.STYLE = m.style AND ;
				xpl.COLOR = m.color AND ;
				xpl.PACK = m.pack AND ;
				RECNO() > nRec

			nTotQty = nTotQty+xpl.totqty
			line1 = ATLINE("ORIGPOINFO",xpl.ECHO)
			cOrigline = ALLTRIM(MLINE(xpl.ECHO,line1))
			DELETE NEXT 1 IN xpl
		ENDSCAN
		GO nRec
		REPLACE xpl.totqty WITH xpl.totqty + nTotQty IN xpl
		REPLACE xpl.ECHO WITH xpl.ECHO+CHR(13)+cOrigline IN xpl
		nTotQty = 0
	ENDIF
	GO nRec
	nCnt1 = 0
ENDSCAN

IF lTest OR lHoldFile
	SELECT xpl
	LOCATE
	BROWSE
ENDIF

*!* Clear deleted records from XPL
SELECT * FROM xpl WHERE !DELETED() INTO CURSOR xpl2 READWRITE
USE IN xpl
SELECT * FROM xpl2 WHERE !DELETED() INTO CURSOR xpl READWRITE
USE IN xpl2

IF lHoldFile OR lTest
	WAIT WINDOW "Browsing cursors...CANCEL after xpl browse or data will load!" TIMEOUT 3
	SELECT xinwolog
	SCATTER MEMVAR BLANK
	LOCATE
	BROWSE
	SELECT xpl
	SCATTER MEMVAR BLANK
	LOCATE
	BROWSE
ENDIF

SELECT xinwolog
*	ASSERT .F. MESSAGE "At XINWOLOG scan...DEBUG here"
SCAN
	cMsg = "At INWOLOG/PL population"
	WAIT WINDOW cMsg NOWAIT
	SELECT inwolog
	LOCATE FOR inwolog.accountid = xinwolog.accountid AND inwolog.CONTAINER = xinwolog.CONTAINER AND inwolog.REFERENCE = xinwolog.REFERENCE
	IF FOUND()
		WAIT WINDOW "This is an ACCRUAL to existing data by acct. ID, Ctr and CustBOL (Reference#)" TIMEOUT 2
		SCATTER FIELDS wo_num,inwologid MEMVAR
		REPLACE inwolog.plinqty WITH inwolog.plinqty + xinwolog.plinqty

		SELECT xpl
		SUM(VAL(xpl.PACK)*xpl.totqty) TO num_pairs FOR xpl.inwologid = xinwolog.inwologid

		SELECT wos
		APPEND BLANK
		REPLACE filename WITH xfile IN wos
		REPLACE wo_num WITH m.wo_num IN wos
		REPLACE acctcode WITH IIF(xinwolog.accountid=4752,"MS","HL")
		REPLACE sku_units WITH num_pairs
		REPLACE cartons WITH xinwolog.plinqty
		REPLACE REFNUM WITH IIF(lAir,"AIR",xinwolog.REFERENCE)
		REPLACE CONTAINER WITH xinwolog.CONTAINER
		REPLACE accrue WITH .T.

		SELECT xpl
		SCAN FOR xpl.inwologid = xinwolog.inwologid
			SELECT pl
			LOCATE FOR pl.inwologid = inwolog.inwologid AND pl.STYLE = xpl.STYLE AND pl.COLOR = xpl.COLOR AND pl.PACK = xpl.PACK
			IF FOUND()
				REPLACE pl.totqty WITH pl.totqty+xpl.totqty
			ELSE
				WAIT WINDOW "New PL record to add..." TIMEOUT 2
				SELECT xpl
				SCATTER FIELDS EXCEPT wo_num,inwologid MEMVAR MEMO
				m.inwologid = inwolog.inwologid
				m.wo_num = inwolog.wo_num
				m.office = cOffice
				m.mod = "2"
				insertinto("pl","wh",.T.)
			ENDIF
		ENDSCAN
	ELSE
		ASSERT .F. MESSAGE "At XINWOLOG new rec insertion...DEBUG here"
		SELECT xinwolog
		SCATTER MEMVAR MEMO
		m.wo_num = dygenpk("wonum",cWhseMod)
		m.refer_num=whrefnum(m.accountid)

		SELECT xpl
		SUM(VAL(xpl.PACK)*xpl.totqty) TO num_pairs FOR xpl.inwologid = xinwolog.inwologid

		SELECT wos
		APPEND BLANK
		REPLACE filename WITH xfile IN wos
		REPLACE wo_num WITH m.wo_num IN wos
		REPLACE acctcode WITH IIF(xinwolog.accountid=4752,"MS","HL")
		REPLACE sku_units WITH num_pairs
		REPLACE cartons WITH xinwolog.plinqty
		REPLACE REFNUM WITH xinwolog.REFERENCE
		REPLACE CONTAINER WITH xinwolog.CONTAINER
		REPLACE accrue WITH .F.

		m.office = cOffice
		m.mod = "2"

		insertinto("inwolog","wh",.T.)

		SELECT xpl
		SCAN FOR xpl.inwologid = xinwolog.inwologid
			SCATTER MEMVAR MEMO
			m.wo_num = inwolog.wo_num
			m.inwologid = inwolog.inwologid

			m.office = cOffice
			m.mod = "2"
			insertinto("pl","wh",.T.)
		ENDSCAN
	ENDIF
ENDSCAN

cMsg = "At end of Modern Shoe inbound processing"

WAIT WINDOW cMsg NOWAIT
ASSERT .F. MESSAGE cMsg
STORE 4752 TO nAcctNum,xacctnum

SELECT inwolog
STORE "" TO xtruckwonum, xtruckseal
DO "m:\dev\prg\whtruckwonum.prg" WITH inwolog.wo_num, inwolog.REFERENCE, inwolog.CONTAINER, inwolog.accountid
REPLACE inwolog.truckwonum WITH xtruckwonum, inwolog.Seal WITH xtruckseal IN inwolog

tu("inwolog")
tu("pl")

******************************
PROCEDURE closedata
******************************

IF USED('inwolog')
	USE IN inwolog
ENDIF

IF USED('pl')
	USE IN pl
ENDIF

IF USED('upcmast')
	USE IN upcmast
ENDIF

IF USED('upcmastsql')
	USE IN upcmastsql
ENDIF

IF USED('account')
	USE IN account
ENDIF

*IF USED('whgenpk')
*	USE IN whgenpk
*ENDIF
ENDPROC

