LOCAL lcViewName
lcViewName = "voildetl"

OPEN DATABASE F:\shop\shdata\sh

*CREATE SQL VIEW voildetl AS ;

************************************************************************************
* PASTE VIEW GEN CODE BELOW

CREATE SQL VIEW voildetl AS ;
	SELECT Oildetl.oildetlid, Oildetl.ordhdrid, Oildetl.ordno, Oildetl.vehno,;
	Oildetl.oildate, Oildetl.partno, Oildetl.partdesc, Oildetl.oilqty,;
	Oildetl.unitdim, Oildetl.unitprice, Oildetl.pmordno, Oildetl.extprice;
	FROM ;
	sh!Oildetl;
	WHERE  Oildetl.vehno = ( ?tvehno );
	ORDER BY Oildetl.oildate DESC

DBSETPROP(lcViewName,"View","SendUpdates",.T.)
DBSETPROP(lcViewName,"View","BatchUpdateCount",1)
DBSETPROP(lcViewName,"View","CompareMemo",.T.)
DBSETPROP(lcViewName,"View","FetchAsNeeded",.F.)
DBSETPROP(lcViewName,"View","FetchMemo",.T.)
DBSETPROP(lcViewName,"View","FetchSize",100)
DBSETPROP(lcViewName,"View","MaxRecords",-1)
DBSETPROP(lcViewName,"View","Prepared",.F.)
DBSETPROP(lcViewName,"View","UpdateType",1)
DBSETPROP(lcViewName,"View","UseMemoSize",255)
DBSETPROP(lcViewName,"View","Tables","sh!oildetl")
DBSETPROP(lcViewName,"View","WhereType",1)

DBSETPROP(lcViewName+".oildetlid","Field","DataType","I")
DBSETPROP(lcViewName+".oildetlid","Field","UpdateName","sh!oildetl.oildetlid")
DBSETPROP(lcViewName+".oildetlid","Field","KeyField",.T.)
DBSETPROP(lcViewName+".oildetlid","Field","Updatable",.T.)

DBSETPROP(lcViewName+".ordhdrid","Field","DataType","I")
DBSETPROP(lcViewName+".ordhdrid","Field","UpdateName","sh!oildetl.ordhdrid")
DBSETPROP(lcViewName+".ordhdrid","Field","KeyField",.F.)
DBSETPROP(lcViewName+".ordhdrid","Field","Updatable",.T.)

DBSETPROP(lcViewName+".ordno","Field","DataType","N(6)")
DBSETPROP(lcViewName+".ordno","Field","UpdateName","sh!oildetl.ordno")
DBSETPROP(lcViewName+".ordno","Field","KeyField",.F.)
DBSETPROP(lcViewName+".ordno","Field","Updatable",.T.)

DBSETPROP(lcViewName+".vehno","Field","DataType","C(4)")
DBSETPROP(lcViewName+".vehno","Field","UpdateName","sh!oildetl.vehno")
DBSETPROP(lcViewName+".vehno","Field","KeyField",.F.)
DBSETPROP(lcViewName+".vehno","Field","Updatable",.T.)

DBSETPROP(lcViewName+".oildate","Field","DataType","D")
DBSETPROP(lcViewName+".oildate","Field","UpdateName","sh!oildetl.oildate")
DBSETPROP(lcViewName+".oildate","Field","KeyField",.F.)
DBSETPROP(lcViewName+".oildate","Field","Updatable",.T.)

DBSETPROP(lcViewName+".partno","Field","DataType","C(20)")
DBSETPROP(lcViewName+".partno","Field","UpdateName","sh!oildetl.partno")
DBSETPROP(lcViewName+".partno","Field","KeyField",.F.)
DBSETPROP(lcViewName+".partno","Field","Updatable",.T.)

DBSETPROP(lcViewName+".partdesc","Field","DataType","C(30)")
DBSETPROP(lcViewName+".partdesc","Field","UpdateName","sh!oildetl.partdesc")
DBSETPROP(lcViewName+".partdesc","Field","KeyField",.F.)
DBSETPROP(lcViewName+".partdesc","Field","Updatable",.T.)

DBSETPROP(lcViewName+".oilqty","Field","DataType","N(4)")
DBSETPROP(lcViewName+".oilqty","Field","UpdateName","sh!oildetl.oilqty")
DBSETPROP(lcViewName+".oilqty","Field","KeyField",.F.)
DBSETPROP(lcViewName+".oilqty","Field","Updatable",.T.)

DBSETPROP(lcViewName+".unitdim","Field","DataType","C(10)")
DBSETPROP(lcViewName+".unitdim","Field","UpdateName","sh!oildetl.unitdim")
DBSETPROP(lcViewName+".unitdim","Field","KeyField",.F.)
DBSETPROP(lcViewName+".unitdim","Field","Updatable",.T.)

DBSETPROP(lcViewName+".unitprice","Field","DataType","N(8,2)")
DBSETPROP(lcViewName+".unitprice","Field","UpdateName","sh!oildetl.unitprice")
DBSETPROP(lcViewName+".unitprice","Field","KeyField",.F.)
DBSETPROP(lcViewName+".unitprice","Field","Updatable",.T.)

DBSETPROP(lcViewName+".pmordno","Field","DataType","N(6)")
DBSETPROP(lcViewName+".pmordno","Field","UpdateName","sh!oildetl.pmordno")
DBSETPROP(lcViewName+".pmordno","Field","KeyField",.F.)
DBSETPROP(lcViewName+".pmordno","Field","Updatable",.T.)

DBSETPROP(lcViewName+".extprice","Field","DataType","N(10,2)")
DBSETPROP(lcViewName+".extprice","Field","UpdateName","sh!oildetl.extprice")
DBSETPROP(lcViewName+".extprice","Field","KeyField",.F.)
DBSETPROP(lcViewName+".extprice","Field","Updatable",.T.)



*****************************************************************************


CLOSE DATABASES

RETURN
