* Grab data through prior Saturday.
* Then Assemble data from various spreadsheets/sources and produce a weekly Dashboard Spreadsheet.
* EXEs go in F:\UTIL\DASHBOARD\
LPARAMETERS tcMode
LOCAL lnError, lcProcessName, loDashboardReport, lcMode

DECLARE INTEGER CreateMutex IN Win32API ;
	STRING @lpMutexAttributes, ;
	STRING bInitialOwner, ;
	STRING @lpName

DECLARE INTEGER GetLastError IN Win32API

* if running in EXE check to see if already running
IF _VFP.STARTMODE > 0 THEN
	lcProcessName = "DASHBOARDREPORT"
	CreateMutex(0,CHR(1),@lcProcessName)
	lnError = GetLastError()
	IF lnError = 183
		WAIT WINDOW TIMEOUT 10 "Dashboard Report Process is already running..."
		RETURN .F.
	ENDIF
ENDIF

*utilsetup("KRONOSDASHBOARDREPORT")

IF EMPTY(tcMode) THEN
	lcMode = "WEEKLY"
ELSE
	lcMode = UPPER(ALLTRIM(tcMode))
ENDIF

loDashboardReport = CREATEOBJECT('DashboardReport')
loDashboardReport.MAIN( lcMode )

*schedupdate()

CLOSE DATABASES ALL
RETURN

#DEFINE LOGIT 1
#DEFINE WAITIT 2
#DEFINE NOWAITIT 4
#DEFINE SENDIT 8
#DEFINE CRLF CHR(13) + CHR(10)
#DEFINE LF CHR(10)
#DEFINE RETURN_DATA_MANDATORY .F.
#DEFINE RETURN_DATA_NOT_MANDATORY .T.
#DEFINE DAILY_OT_MULTIPLIER 1.5
#DEFINE DAILY_OT20_MULTIPLIER 2.0
#DEFINE xlEdgeBottom 9
#DEFINE xlContinuous 1
#DEFINE xlMedium -4138
#DEFINE xlFONT_RED -16776961

DEFINE CLASS DashboardReport AS CUSTOM

	cProcessName = 'DashboardReport'

	lTestMode = .F.  && .F. FOR NORMAL OPERATION, .T. TO SUPPRESS ALL BUT DEVELOPER EMAILS AND CHANGE INPUT TABLE, ETC.

	lAutoYield = .T.

	cStartTime = TTOC(DATETIME())

	cMode = 'WEEKLY'  && or "DAILY"

	* connection properties
	nSQLHandle = 0

	* file properties
	nFileHandle = 0
	
	* processing properties
	lTempsOnly = .F.
	
	* table properties
	cBasisTable = 'F:\UTIL\DASHBOARD\DATA\BASIS'
	
	* filter properties
	cCompanyWhere = " AND (D.LABORLEV1NM IN ('SXI','TMP')) "
	*cCompanyWhere = " AND (D.LABORLEV1NM IN ('TMP')) "
	*cCompanyWhere = " "

	cExtraWhere = ""
	*cExtraWhere = " AND ( (D.LABORLEV5NM = 'B') OR (D.LABORLEV4NM = 'CA4') ) "

	* date properties
	dtNow = DATETIME()

	dToday = DATE()
	* to force different pay weeks
	*dToday = {^2011-09-21}

	cToday = DTOC(DATE())
	
	* filename properties
	cFileNameCompany = "_HOURLY_AND_TEMPS_"

	* wait window properties
	nWaitWindowTimeout = 2
	lWaitWindowIsOn = .T.

	* logfile properties
	lLoggingIsOn = .T.
	cLogFile = 'F:\UTIL\KRONOS\Logfiles\DashboardReport_log.txt'

	* INTERNAL email properties
	lSendInternalEmailIsOn = .T.
	cFrom = 'mark.bennett@tollgroup.com'
	cSendTo = 'mark.bennett@tollgroup.com'
	cCC = ''
	cSubject = 'Dashboard Report '
	cAttach = ''
	cBodyText = ''
	cTopBodyText = ''

	cCOMPUTERNAME = ''
	cUSERNAME = ''

	FUNCTION INIT
		IF NOT DODEFAULT()
			RETURN .F.
		ENDIF
		WITH THIS
			CLEAR
			*SET RESOURCE OFF
			CLOSE DATA
			SET CENTURY ON
			SET DATE AMERICAN
			SET HOURS TO 24
			SET DECIMALS TO 2
			SET ANSI ON
			SET TALK OFF
			SET DELETED ON
			SET CONSOLE OFF
			SET EXCLUSIVE OFF
			SET SAFETY OFF
			SET EXACT OFF
			SET STATUS BAR ON
			SET SYSMENU OFF
			SET ENGINEBEHAVIOR 70
			_VFP.AUTOYIELD = .lAutoYield
			.cCOMPUTERNAME = GETENV("COMPUTERNAME")
			.cUSERNAME = GETENV("USERNAME")
			IF .lTestMode THEN
				.cSendTo = 'mark.bennett@tollgroup.com'
				.cLogFile = 'F:\UTIL\KRONOS\Logfiles\DashboardReport_log_TESTMODE.txt'
			ENDIF
			.lLoggingIsOn = .lLoggingIsOn AND NOT EMPTY(.cLogFile)
		ENDWITH
	ENDFUNC


	FUNCTION DESTROY
		WITH THIS
			IF .lLoggingIsOn  THEN
				SET ALTERNATE OFF
				SET ALTERNATE TO
			ENDIF
		ENDWITH
		DODEFAULT()
	ENDFUNC


	FUNCTION MAIN
		LPARAMETERS tcMode
		WITH THIS
			LOCAL lnNumberOfErrors, lcSQL, lcSQLADP, lcSQLADP2, ldToday, ldFromDate, ldToDate, lcSQLFromDate, lcSQLToDate
			LOCAL lcOutputFile, lcMonthYear, ldADPFromDate, ldADPToDate, lnDay
			LOCAL lcSQL2, lcSQL3, lcSQL4, lcSQL5, loError, llValid
			LOCAL lcXLFileName, ldStartDate, ldEndDate, lcStartDate, lcEndDate, ldDate
			LOCAL lcSQLStartDate, lcSQLEndDate, lcXLFileName, ldYesterday, lnTotalRow
			LOCAL oExcel, oWorkbook, oWorksheet, lnRow, lcRow, lnStartRow, lcStartRow, lnEndRow, lnPP, lcPP, lnNN, lcNN, lcJJ, lcEndRow
			LOCAL lcColsToProcess, lnColumn, lcBoldColumn, i, j, lcBoldRow, luCellValue, lcTargetDirectory, ldPayDate, lcFiletoSaveAs
			LOCAL lcFileDate, lcDivision, lcTitle, llSaveAgain, lcPayCode, lcSpecialWhere
			LOCAL lcSQLToday, lcSQLTomorrow, ldToday, ldTomorrow, lcExcluded9999s, lcFoxProTimeReviewerTable, lcMissingApprovals
			LOCAL lcAuditSubject, oWorkbookAUDIT, oWorksheetAUDIT, lcSendTo, lcCC, lcBodyText, lcXLFileNameAUDIT, lcTotalRow
			LOCAL lcTRWhere, lnStraightHours, lnStraightDollars, lnOTDollars, lcFilePrefix
			LOCAL ldPass2Date, lcSpreadsheetTemplate, lnMarkup
			LOCAL lcOrderBy, lnTempMarkup, ldCurrentDate, lcSQLStartDate, lcSQLEndDate
			LOCAL lnShiftDiff, lcXLSummaryFileName, lnWage, lnOTPay, lnRegPay, lcRootPDFFilename
			LOCAL ltInpunch, lnTotHours, lcADPFromDate, lcADPToDate, lcADPDivWhere
			LOCAL lnSQLHandle, lcSQLADP, lnMultiplier, lcFinalFilename, ldPriorSaturday, lnDaysInMonth, lnMaxRow
			LOCAL lnCellDivision, lcDetailFile, lcByEmployeeFile, lcByEmployeeSummaryFile
			LOCAL lnRevenue1, lnRevenue2, ldYesterday, lcSummaryByDept, oWorksheet2, lnYear, lnMonth

			TRY
				lnNumberOfErrors = 0

				IF .lLoggingIsOn THEN
					SET ALTERNATE TO (.cLogFile) ADDITIVE
					SET ALTERNATE ON
				ENDIF
				
				IF .lTempsOnly THEN
					.cCompanyWhere = " AND (D.LABORLEV1NM IN ('TMP')) "
					.cFileNameCompany = "_Temps_Only_"
				ELSE
					.cCompanyWhere = " AND (D.LABORLEV1NM IN ('SXI','TMP')) "
					.cFileNameCompany = "_Hourly_AND_Temps_"
				ENDIF

				.TrackProgress('Dashboard Report process started....', LOGIT+SENDIT+NOWAITIT)
				.TrackProgress('cExtraWhere = ' + .cExtraWhere, LOGIT+SENDIT)				
				.TrackProgress('cCompanyWhere = ' + .cCompanyWhere, LOGIT+SENDIT)
				.TrackProgress('cFileNameCompany = ' + .cFileNameCompany, LOGIT+SENDIT)
				.TrackProgress('COMPUTERNAME = ' + .cCOMPUTERNAME, LOGIT+SENDIT)
				.TrackProgress('USERNAME = ' + .cUSERNAME, LOGIT+SENDIT)
				.TrackProgress('tcMode = ' + tcMode, LOGIT+SENDIT)				
				.TrackProgress('PROJECT = KRONOSDASHBOARDREPORT', LOGIT+SENDIT)
				IF .lTestMode THEN
					.TrackProgress('                        TEST MODE                        ', LOGIT+SENDIT)
				ENDIF
				
				IF INLIST(tcMode,'WEEKLY','DAILY') THEN
					* valid mode parameter
					.cMode = tcMode
				ELSE
					.TrackProgress('!!!! ERROR: invalid tcMode parameter', LOGIT+SENDIT)
					THROW
				ENDIF

				* DON'T RUN IN DAILY MODE IF TOO EARLY IN THE MONTH
				IF (.cMode = 'DAILY') AND (DAY(DATE()) < 2) THEN
					.TrackProgress('----------------------------------', LOGIT+SENDIT)
					.TrackProgress('Too Early in month for DAILY mode.', LOGIT+SENDIT)
					.TrackProgress('----------------------------------', LOGIT+SENDIT)
					.cSendTo = 'mark.bennett@tollgroup.com,mbennett@fmiint.com'
					.cSubject = 'DASHBOARD (RUN TOO EARLY)'
					.cCC = ''
					THROW
				ENDIF

				IF USED('CURADP') THEN
					USE IN CURADP
				ENDIF

				* get ADP hourly rates (for hourly employees)
				.nSQLHandle = SQLCONNECT("PCPAYWIN","MBENNETT","VP737A")

				IF .nSQLHandle > 0 THEN

					SET TEXTMERGE ON
					TEXT TO	lcSQLADP NOSHOW
SELECT
FILE# AS FILE_NUM,
{fn ifnull(MAX(rate1amt),0.00)} AS RATE
FROM REPORTS.V_EMPLOYEE
WHERE COMPANYCODE = 'E87'
GROUP BY FILE#
					ENDTEXT
					SET TEXTMERGE OFF

					.TrackProgress('lcSQLADP =' + lcSQLADP, LOGIT+SENDIT)

					IF .ExecSQL(lcSQLADP, 'CURADP', RETURN_DATA_MANDATORY) THEN

						*!*						SELECT CURADP
						*!*						BROWSE

						=SQLDISCONNECT(.nSQLHandle)

					ENDIF  &&  .ExecSQL(lcSQLADP, 'CURADP', RETURN_DATA_MANDATORY)

				ELSE
					* connection error
					.TrackProgress('Unable to connect to ADP Payroll System.', LOGIT+SENDIT)
					THROW
				ENDIF   &&  .nSQLHandle > 0				

				IF NOT .lTestMode THEN
					* assign live email recipients
					DO CASE
						CASE .cMode = 'WEEKLY'
							.cSendTo = ''
							.cCC = ''
						CASE .cMode = 'DAILY'
							.cSendTo = ''
							.cCC = 'Mark.Bennett@Tollgroup.com'
						OTHERWISE
							* nothing
					ENDCASE
				ENDIF

				ldToday = .dToday
								
				DO CASE
					CASE .cMode = 'WEEKLY'
						* we want to report on MTD of the month the prior saturday was in.
						ldPriorSaturday = ldToday
						DO WHILE DOW(ldPriorSaturday,1) <> 7
							ldPriorSaturday = ldPriorSaturday - 1
						ENDDO
						lnDay = DAY(ldPriorSaturday)
						ldFromDate = ldPriorSaturday - lnDay  && this is last day of prior month
						ldFromDate = ldFromDate + 1  && this is the first day of the month
						lnDaysInMonth = DAY( GOMONTH(ldFromDate,+1) -1 ) && This is the # of days in the month
						ldToDate = ldPriorSaturday
				
					CASE .cMode = 'DAILY'
						* we want to report on MTD of current month from day 1 through yesterday.
						ldYesterday = ldToday - 1

						lnDay = DAY(ldYesterday)
						ldFromDate = ldYesterday - lnDay  && this is last day of prior month
						ldFromDate = ldFromDate + 1  && this is the first day of current month
						lnDaysInMonth = DAY( GOMONTH(ldFromDate,+1) -1 ) && This is the # of days in the current month
						ldToDate = ldYesterday
						
						*!*	IF NOT .lTestMode THEN
						*!*		.cSendto = ???
						*!*	ENDIF

					OTHERWISE
						* nothing
				ENDCASE
				
*!*		* for special date-range, activate this block
*!*		IF .lTestMode THEN
*!*			ldFromDate = {^2011-09-01}
*!*			ldToDate = {^2011-09-30}
*!*		ENDIF

				*!*					ldADPToDate = ldToDate
				*!*					ldADPFromDate = ldFromDate

				lcMonthYear = TRANSFORM(ldFromDate) + "-" + TRANSFORM(ldToDate)

				.cSubject = 'Dashboard Report for MTD Through: ' + TRANSFORM(ldToDate)
				lcOutputFileSummary = "F:\UTIL\DASHBOARD\REPORTS\Dash_Report" + .cFileNameCompany + "_" + .cMode + DTOS(ldToDate) + ".XLS"
				lcByEmployeeFile = "F:\UTIL\DASHBOARD\REPORTS\Dash_Report_By_Employee" + .cFileNameCompany + "_" + .cMode + DTOS(ldToDate) + ".XLS"
				lcByEmployeeSummaryFile	= "F:\UTIL\DASHBOARD\REPORTS\Dash_Report_By_Employee_Summary" + .cFileNameCompany + "_" + .cMode + DTOS(ldToDate) + ".XLS"						
				lcFinalFilename = "F:\UTIL\DASHBOARD\REPORTS\DATA_BYDIV_MTD_" + "_" + .cMode + DTOS(ldToday) + ".XLS"
				lcDetailFile = "F:\UTIL\DASHBOARD\DETAILXLS\Dash_Report_Detail" + .cFileNameCompany + "_" + .cMode + DTOS(ldToDate) + ".XLS"
				lcSummaryByDept = "F:\UTIL\DASHBOARD\REPORTS\Dash_Report_By_Dept_Summary" + .cFileNameCompany + "_" + .cMode + DTOS(ldToDate) + ".XLS"

				lcSQLFromDate = .GetSQLStartDateTimeString( ldFromDate )
				lcSQLToDate = .GetSQLEndDateTimeString( ldToDate )

				*!*				lcADPFromDate = "DATE'" + TRANSFORM(YEAR(ldADPFromDate)) + "-" + PADL(MONTH(ldADPFromDate),2,'0') + "-" + PADL(DAY(ldADPFromDate),2,'0') + "'"
				*!*				lcADPToDate = "DATE'" + TRANSFORM(YEAR(ldADPToDate)) + "-" + PADL(MONTH(ldADPToDate),2,'0') + "-" + PADL(DAY(ldADPToDate),2,'0') + "'"

				IF USED('CURWTKHOURS') THEN
					USE IN CURWTKHOURS
				ENDIF
				IF USED('CURWTKHOURSPRE') THEN
					USE IN CURWTKHOURSPRE
				ENDIF
				IF USED('CURFINAL') THEN
					USE IN CURFINAL
				ENDIF
				IF USED('CURFINALBYDIV') THEN
					USE IN CURFINALBYDIV
				ENDIF
				IF USED('CURFINALBYEMP') THEN
					USE IN CURFINALBYEMP
				ENDIF
				IF USED('CURSUMMARYBYEMP') THEN
					USE IN CURSUMMARYBYEMP
				ENDIF
				

				*!*					" D.LABORLEV3NM AS DEPT, " + ;
				*!*					" D.LABORLEV3DSC AS DEPTNAME, " + ;
				*!*					" C.SHORTNM AS SHORTNAME, " + ;

				* main SQL
				lcSQL = ;
					" SELECT " + ;
					" A.APPLYDTM, " + ;
					" D.LABORLEV1NM AS ADP_COMP, " + ;
					" D.LABORLEV2NM AS DIVISION, " + ;
					" D.LABORLEV2DSC AS DIVNAME, " + ;
					" D.LABORLEV3NM AS DEPT, " + ;
					" D.LABORLEV3DSC AS DEPTNAME, " + ;
					" D.LABORLEV4NM AS WORKSITE, " + ;
					" D.LABORLEV4DSC AS WKSITENAME, " + ;
					" D.LABORLEV5NM AS AGENCYNUM, " + ;
					" D.LABORLEV5DSC AS AGENCY, " + ;
					" C.FULLNM AS EMPLOYEE, " + ;
					" C.PERSONNUM AS FILE_NUM, " + ;
					" C.PERSONID, " + ;
					" A.EMPLOYEEID, " + ;
					" E.ABBREVIATIONCHAR AS PAYCODE, " + ;
					" E.NAME AS PAYCODEDESC, " + ;
					" (A.DURATIONSECSQTY / 3600.00) AS HOURS, " + ;
					" {fn ifnull(A.MONEYAMT,0000.00)} AS MONEY, " + ;
					" 0000.00 AS BASEWAGE, " + ;
					" 0000.00 AS WAGE, " + ;
					" 000000.00 AS REGPAY, " + ;
					" 000000.00 AS OTPAY, " + ;
					" 000000.00 AS DTPAY, " + ;
					" 000000.00 AS TOTPAY " + ;
					" FROM WFCTOTAL A " + ;
					" JOIN WTKEMPLOYEE B " + ;
					" ON B.EMPLOYEEID = A.EMPLOYEEID " + ;
					" JOIN PERSON C " + ;
					" ON C.PERSONID = B.PERSONID " + ;
					" JOIN LABORACCT D " + ;
					" ON D.LABORACCTID = A.LABORACCTID " + ;
					" JOIN PAYCODE E " + ;
					" ON E.PAYCODEID = A.PAYCODEID " + ;
					" WHERE (A.APPLYDTM >= " + lcSQLFromDate + ")" + ;
					" AND (A.APPLYDTM <= " + lcSQLToDate + ")" + ;					
					.cCompanyWhere + ;
					.cExtraWhere


				IF .lTestMode THEN
					.TrackProgress('', LOGIT+SENDIT)
					.TrackProgress('lcSQL =' + lcSQL, LOGIT+SENDIT)
					.TrackProgress('', LOGIT+SENDIT)
					.TrackProgress('ldToDate =' + TRANSFORM(ldToDate), LOGIT+SENDIT)
					.TrackProgress('ldFromDate =' + TRANSFORM(ldFromDate), LOGIT+SENDIT)
				ENDIF

				.nSQLHandle = SQLCONNECT("WTK2","sa","kr0n1tes")

				IF .nSQLHandle > 0 THEN

					IF .ExecSQL(lcSQL, 'CURWTKHOURSPRE', RETURN_DATA_MANDATORY) THEN

						* add a foxpro date field to the cursor
						SELECT TTOD(APPLYDTM) AS WORKDATE, * ;
							FROM CURWTKHOURSPRE ;
							INTO CURSOR CURWTKHOURS ;
							ORDER BY APPLYDTM, ADP_COMP, WORKSITE, DIVISION, DEPT, AGENCYNUM, EMPLOYEE, PAYCODE ;
							READWRITE

						* populate Temp base wages (which are in Kronos and are date-sensitive)
						SET DATE YMD

						ldCurrentDate = ldFromDate

						DO WHILE ldCurrentDate <= ldToDate

							*	 create "YYYYMMDD" safe date format for use in SQL query
							lcSQLStartDate = STRTRAN(DTOC(ldCurrentDate),"/","")

							lcSQLWAGES = ;
								" SELECT " + ;
								" EMPLOYEEID, " + ;
								" BASEWAGEHOURLYAMT AS WAGE, " + ;
								" EFFECTIVEDTM, " + ;
								" EXPIRATIONDTM " + ;
								" FROM BASEWAGERTHIST " + ;
								" WHERE '" + lcSQLStartDate + "' BETWEEN EFFECTIVEDTM AND EXPIRATIONDTM " + ;
								" ORDER BY EMPLOYEEID DESC, EXPIRATIONDTM DESC "

							IF USED('CURWAGES') THEN
								USE IN CURWAGES
							ENDIF

							IF .ExecSQL(lcSQLWAGES, 'CURWAGES', RETURN_DATA_MANDATORY) THEN

								SELECT CURWTKHOURS
								*SCAN FOR (WORKDATE = ldCurrentDate) AND INLIST(ALLTRIM(AGENCYNUM),"A","B")
								SCAN FOR (WORKDATE = ldCurrentDate) AND (UPPER(ALLTRIM(ADP_COMP)) == "TMP")

									* use Temp agency markup to create adjusted wage
									DO CASE
										CASE ALLTRIM(CURWTKHOURS.AGENCYNUM) = 'A'
											lnMarkup = 1.28
										CASE ALLTRIM(CURWTKHOURS.AGENCYNUM) = 'B'
											lnMarkup = 1.275
										OTHERWISE
											lnMarkup = 1.00
									ENDCASE

									* populate WAGE fields in CURWTKHOURSTEMP
									SELECT CURWAGES
									LOCATE FOR EMPLOYEEID = CURWTKHOURS.EMPLOYEEID
									IF FOUND() THEN
										REPLACE CURWTKHOURS.BASEWAGE WITH CURWAGES.WAGE, CURWTKHOURS.WAGE WITH ( CURWAGES.WAGE * lnMarkup )
									ELSE
										REPLACE CURWTKHOURS.BASEWAGE WITH 8.00
									ENDIF
								ENDSCAN
							ELSE
								REPLACE CURWTKHOURS.BASEWAGE WITH 8.00
							ENDIF  && .ExecSQL(lcSQLWAGES

							ldCurrentDate = ldCurrentDate + 1

						ENDDO

						SET DATE AMERICAN


						* Populate non-temp hourly wage
						SELECT CURWTKHOURS
						*SCAN FOR (NOT INLIST(ALLTRIM(AGENCYNUM),"A","B"))
						SCAN FOR (UPPER(ALLTRIM(ADP_COMP)) == "SXI")
							SELECT CURADP
							LOCATE FOR FILE_NUM = VAL(CURWTKHOURS.FILE_NUM)
							IF FOUND() THEN
								REPLACE CURWTKHOURS.BASEWAGE WITH CURADP.RATE, CURWTKHOURS.WAGE WITH CURADP.RATE
							ENDIF
						ENDSCAN

						* populate final Pay based on wage and paycode

						SELECT CURWTKHOURS
						SCAN
							lcPayCode = UPPER(ALLTRIM(CURWTKHOURS.PAYCODE))
							DO CASE
								CASE lcPayCode == "OTM"
									* OVERTIME
									lnMultiplier = 1.50
									REPLACE CURWTKHOURS.OTPAY WITH ( CURWTKHOURS.HOURS * CURWTKHOURS.WAGE * lnMultiplier )
								CASE lcPayCode == "DTM"
									* DOUBLE-TIME
									lnMultiplier = 2.00
									REPLACE CURWTKHOURS.DTPAY WITH ( CURWTKHOURS.HOURS * CURWTKHOURS.WAGE * lnMultiplier )
								CASE INLIST(lcPayCode,"FML","ULV","UPH")
									* THESE ARE THE MAIN UNPAID PAY CODES: FMLA, UNPAID LEAVE, UNPAID HOURS
									lnMultiplier = 0.00
									* no replace needed
								OTHERWISE
									* ASSUME STRAIGHT TIME SUCH AS REG HRS, SICK, VAC
									lnMultiplier = 1.00
									REPLACE CURWTKHOURS.REGPAY WITH ( CURWTKHOURS.HOURS * CURWTKHOURS.WAGE * lnMultiplier )
							ENDCASE

							*REPLACE CURWTKHOURS.PAY WITH ( CURWTKHOURS.HOURS * CURWTKHOURS.WAGE * lnMultiplier ) + CURWTKHOURS.MONEY

						ENDSCAN

*!*	IF .lTestMode THEN
						* POPULATE TOTPAY FIELD
						SELECT CURWTKHOURS
						SCAN
							REPLACE CURWTKHOURS.TOTPAY WITH (CURWTKHOURS.REGPAY + CURWTKHOURS.OTPAY + CURWTKHOURS.DTPAY + CURWTKHOURS.MONEY)
						ENDSCAN

							
						SELECT CURWTKHOURS
						COPY TO (lcDetailFile) XL5 
						
						
						*FOR INLIST(WORKDATE,{^2011-05-14},{^2011-05-21},{^2011-06-18},{^2011-06-25})
*!*	ENDIF

*!*	IF .lTestMode THEN
*!*		SELECT CURWTKHOURS
*!*		COPY TO C:\A\KMTD_OT20.XLS XL5 FOR UPPER(ALLTRIM(CURWTKHOURS.PAYCODE))== "DTM" AND adp_comp = "TMP"
*!*	ENDIF
						
*!*							SELECT ;
*!*								WKSITENAME, ;
*!*								WORKDATE, ;
*!*								ADP_COMP, ;
*!*								DEPTNAME, ;
*!*								DIVISION, ;
*!*								DIVNAME, ;
*!*								SUM(MONEY) AS MONEY, ;
*!*								SUM(REGPAY) AS REGPAY, ;
*!*								SUM(OTPAY) AS OTPAY, ;
*!*								SUM(DTPAY) AS DTPAY, ;
*!*								SUM(REGPAY + OTPAY + DTPAY + MONEY) AS TOTPAY ;
*!*								FROM CURWTKHOURS ;
*!*								INTO CURSOR CURFINAL ;
*!*								GROUP BY 1, 2, 3, 4, 5, 6 ;
*!*								ORDER BY 1, 2, 3, 4, 5
*!*								
*!*							lcFinalFilename = "C:\A\DASHDATA_MTD_" + DTOS(ldToday) + ".XLS"
*!*							
*!*							SELECT CURFINAL
*!*							COPY TO (lcFinalFilename) XL5


* this is for Steven Sykes
*!*	IF .lTestMode THEN
*!*							SELECT ;
*!*								ADP_COMP, ;
*!*								DIVISION, ;
*!*								DIVNAME, ;
*!*								DEPT, ;
*!*								DEPTNAME, ;
*!*								SUM(HOURS) AS HOURS, ;
*!*								SUM(REGPAY) AS REGPAY, ;
*!*								SUM(OTPAY) AS OTPAY, ;
*!*								SUM(DTPAY) AS DTPAY, ;
*!*								SUM(REGPAY + OTPAY + DTPAY + MONEY) AS TOTPAY ;
*!*								FROM CURWTKHOURS ;
*!*								INTO CURSOR CURBYDEPT ;
*!*								WHERE DEPT = '0645' ;
*!*								GROUP BY 1, 2, 3, 4, 5 ;
*!*								ORDER BY 1, 2, 4
*!*								
*!*							SELECT CURBYDEPT
*!*							COPY TO (lcSummaryByDept) XL5
*!*	*!*							THROW
*!*								SELECT CURWTKHOURS
*!*								COPY TO C:\A\DIV0645WORKERS.XLS XL5 FOR DEPT = '0645' AND (NOT INLIST(DIVISION,'04','06','07'))
*!*	ENDIF
						
							
						SELECT ;
							DIVISION, ;
							DIVNAME, ;
							SUM(MONEY) AS MONEY, ;
							SUM(REGPAY) AS REGPAY, ;
							SUM(OTPAY) AS OTPAY, ;
							SUM(DTPAY) AS DTPAY, ;
							SUM(TOTPAY) AS TOTPAY ;
							FROM CURWTKHOURS ;
							INTO CURSOR CURFINALBYDIV ;
							GROUP BY 1, 2 ;
							ORDER BY 1
						
						SELECT CURFINALBYDIV
						COPY TO (lcFinalFilename) XL5
						

						SELECT ;
							ADP_COMP, ;
							DIVISION, ;
							DIVNAME, ;
							EMPLOYEE, ;
							PAYCODEDESC, ;
							SUM(HOURS) AS HOURS, ;
							MAX(BASEWAGE)AS BASEWAGE, ;
							MAX(WAGE) AS WAGE, ;
							SUM(MONEY) AS MONEY, ;
							SUM(REGPAY) AS REGPAY, ;
							SUM(OTPAY) AS OTPAY, ;
							SUM(DTPAY) AS DTPAY, ;
							SUM(TOTPAY) AS TOTPAY ;
							FROM CURWTKHOURS ;
							INTO CURSOR CURFINALBYEMP ;
							GROUP BY 1, 2, 3, 4, 5 ;
							ORDER BY 1, 2, 4, 5
							
						SELECT CURFINALBYEMP
						COPY TO (lcByEmployeeFile) XL5
						
						SELECT ;
							DISTINCT ;
							ADP_COMP, ;
							DIVISION, ;
							DIVNAME, ;
							DEPT, ;
							DEPTNAME, ;
							EMPLOYEE ;
							FROM CURWTKHOURS ;
							INTO CURSOR CURSUMMARYBYEMP ;
							ORDER BY 1, 2, 3, 6
							
						SELECT CURSUMMARYBYEMP
						COPY TO (lcByEmployeeSummaryFile) XL5
						
						
						***********************************************************************************
						** output to Excel spreadsheet
						* delete output file if it already exists...
						IF FILE(lcOutputFileSummary) THEN
							DELETE FILE (lcOutputFileSummary)
						ENDIF

						.TrackProgress('Creating spreadsheet: ' + lcOutputFileSummary, LOGIT+SENDIT+NOWAITIT)
						lcSpreadsheetTemplate = 'F:\UTIL\DASHBOARD\TEMPLATES\DASH_RPT_TEMPLATE.XLS'
						*lcSpreadsheetTemplate = 'F:\UTIL\DASHBOARD\TEMPLATES\DASH_ALL_TEMPLATE.XLS'

						oExcel = CREATEOBJECT("excel.application")
						oExcel.displayalerts = .F.
						oExcel.VISIBLE = .F.
						oWorkbook = oExcel.workbooks.OPEN(lcSpreadsheetTemplate)
						oWorkbook.SAVEAS(lcOutputFileSummary)

						oWorksheet = oWorkbook.Worksheets[1]
						oWorksheet2 = oWorkbook.Worksheets[2]
						
						oWorksheet.RANGE("A1").VALUE = "Month-to-Date Dashboard Report as of " + TRANSFORM(ldToDate)
						oWorksheet.RANGE("B3").VALUE = ldFromDate
						oWorksheet.RANGE("B4").VALUE = lnDay
						oWorksheet.RANGE("B5").VALUE = lnDaysInMonth
						oWorksheet.RANGE("H1").VALUE = .cCompanyWhere
						oWorksheet.RANGE("H2").VALUE = .cExtraWhere
						

						* REACTIVATE WHEN ALL DATA IS REAL
						*oWorksheet.RANGE("B" + ALLTRIM(STR(lnStartRow)),"I" + ALLTRIM(STR(lnMaxRow))).ClearContents()
						
						
						* populate the source basis data on Sheet 2 from Master basis table...
						IF USED('BASISTABLE') THEN
							USE IN BASISTABLE
						ENDIF
						
						USE (.cBasisTable) IN 0 ALIAS BASISTABLE
						
						lnYear = YEAR(ldFromDate)
						lnMonth = MONTH(ldFromDate)
						
						lcMonth = "'" + CMONTH(ldFromDate) + " " + TRANSFORM(lnYear)
						oWorksheet2.RANGE("B1").VALUE = lcMonth
						
						lnStartRow = 5
						lnMaxRow = 20
						FOR lnRow = lnStartRow TO lnMaxRow
							lcRow = ALLTRIM(STR(lnRow))
							luCellValue = oWorksheet2.RANGE("A"+lcRow).VALUE
							IF NOT ISNULL(luCellValue) THEN
								* ASSUME DIV # IN CHAR FORMAT, PRECEDED BY '
								lcCellDivision = RIGHT(ALLTRIM(luCellValue),2)
								SELECT BASISTABLE
								LOCATE FOR (DIVISION = lcCellDivision) ;
									AND YEAR = lnYear ;
									AND MONTH = lnMonth									
								IF FOUND() THEN
									oWorksheet2.RANGE("B"+lcRow).VALUE = BASISTABLE.PREVENUE
									oWorksheet2.RANGE("C"+lcRow).VALUE = BASISTABLE.PLABOR
								ELSE
									.TrackProgress('-------------------------------------------------' + lcCellDivision, LOGIT+SENDIT)
									.TrackProgress('!!! ERROR: could not find basis data for Div: ' + lcCellDivision, LOGIT+SENDIT)
									.TrackProgress('-------------------------------------------------' + lcCellDivision, LOGIT+SENDIT)
								ENDIF							
							ENDIF && NOT ISNULL(luCellValue)						
						ENDFOR
						IF USED('BASISTABLE') THEN
							USE IN BASISTABLE
						ENDIF

						* populate time data previously retrieved onto Sheet 1...
						lnStartRow = 9
						lnMaxRow = 24
						FOR lnRow = lnStartRow TO lnMaxRow
							lcRow = ALLTRIM(STR(lnRow))
							* checking for 0.00 revenues, which will cause div-by-zero errors in the spreadsheet cells
							lnRevenue1 = oWorksheet.RANGE("C"+lcRow).VALUE
							lnRevenue2 = oWorksheet.RANGE("G"+lcRow).VALUE
							
							luCellValue = oWorksheet.RANGE("A"+lcRow).VALUE
							IF NOT ISNULL(luCellValue) THEN
								* ASSUME DIV # IN CHAR FORMAT, PRECEDED BY '
								lcCellDivision = RIGHT(ALLTRIM(luCellValue),2)
								SELECT CURFINALBYDIV
								LOCATE FOR DIVISION = lcCellDivision
								IF FOUND() THEN
									oWorksheet.RANGE("B"+lcRow).VALUE = CURFINALBYDIV.DIVNAME
									oWorksheet.RANGE("H"+lcRow).VALUE = CURFINALBYDIV.TOTPAY
									IF (NOT ISNULL(lnRevenue1)) AND (NOT ISNULL(lnRevenue2)) AND (lnRevenue1 > 0.00) AND (lnRevenue2 > 0.00) THEN
										* cell in col J should be okay to access
										IF oWorksheet.RANGE("J"+lcRow).VALUE > 0.00 THEN
											* actual % of labor was greater than forecast -- make bold and red font
											oWorksheet.RANGE("J"+lcRow).Font.Color = xlFONT_RED
											oWorksheet.RANGE("J"+lcRow).Font.Bold = .T.
										ENDIF
									ENDIF  &&  (NOT ISNULL(lnRevenue1)) AND (NOT ISNULL(lnRevenue2)) AND (lnRevenue1 > 0.00) AND (lnRevenue2 > 0.00)						
								ENDIF							
							ENDIF && NOT ISNULL(luCellValue)						
						ENDFOR
						

*!*							* populate header counts, sums, etc.
*!*							lnTotalRow = lnEndRow + 2
*!*							lcEndRow = ALLTRIM(STR(lnEndRow))
*!*							lcTotalRow = ALLTRIM(STR(lnTotalRow))

*!*							* sum $ columns
*!*							oWorksheet.RANGE("A" + lcTotalRow).VALUE = "Totals:"
*!*							oWorksheet.RANGE("E" + lcTotalRow).VALUE = "=SUM(E2:E" + lcEndRow + ")"
*!*							oWorksheet.RANGE("F" + lcTotalRow).VALUE = "=SUM(F2:F" + lcEndRow + ")"
*!*							oWorksheet.RANGE("G" + lcTotalRow).VALUE = "=SUM(G2:G" + lcEndRow + ")"
*!*							oWorksheet.RANGE("H" + lcTotalRow).VALUE = "=SUM(H2:H" + lcEndRow + ")"
*!*							oWorksheet.RANGE("I" + lcTotalRow).VALUE = "=SUM(I2:I" + lcEndRow + ")"
*!*							oWorksheet.RANGE("J" + lcTotalRow).VALUE = "=SUM(J2:J" + lcEndRow + ")"
*!*							oWorksheet.RANGE("K" + lcTotalRow).VALUE = "=SUM(K2:K" + lcEndRow + ")"

*!*							oWorksheet.RANGE("L" + lcTotalRow).VALUE = "=(J" + lcTotalRow + "/K" + lcTotalRow + ")"

*!*							* UNDERLINE cell columns to be totaled...
*!*							oWorksheet.RANGE("F" + lcEndRow + ":K" + lcEndRow).BORDERS(xlEdgeBottom).LineStyle = xlContinuous
*!*							oWorksheet.RANGE("F" + lcEndRow + ":K" + lcEndRow).BORDERS(xlEdgeBottom).Weight = xlMedium

						* SAVE AND QUIT EXCEL
						oWorkbook.SAVE()
						oExcel.QUIT()
						**************************************************************

						IF FILE(lcOutputFileSummary) THEN
							.TrackProgress('Output to Summary spreadsheet: ' + lcOutputFileSummary, LOGIT+SENDIT)
							* attach output file to email
							.cAttach = lcOutputFileSummary
							.cBodyText = "See attached report. NOTE: #s are subject to change, pending final time card reviews." + ;
								CRLF + CRLF + "(do not reply - this is an automated report)" + ;
								CRLF + CRLF + "<report log follows>" + ;
								CRLF + .cBodyText
						ELSE
							.TrackProgress('ERROR creating Summary spreadsheet: ' + lcOutputFileSummary, LOGIT+SENDIT)
						ENDIF

					ENDIF  &&  .ExecSQL(lcSQL, 'CURWTKHOURSPRE', RETURN_DATA_MANDATORY)

					.TrackProgress('Dashboard Report  process finished normally', LOGIT+SENDIT)



				ELSE
					* connection error
					.TrackProgress('Unable to connect to WTK System.', LOGIT+SENDIT)
				ENDIF   &&  .nSQLHandle > 0

			CATCH TO loError

				.TrackProgress('There was an error.',LOGIT+SENDIT)
				.TrackProgress('The Error # is: ' + TRANSFORM(loError.ERRORNO), LOGIT+SENDIT)
				.TrackProgress('The Error Message is: ' + TRANSFORM(loError.MESSAGE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred in Program: ' + TRANSFORM(loError.PROCEDURE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred at line #: ' + TRANSFORM(loError.LINENO), LOGIT+SENDIT)
				lnNumberOfErrors = lnNumberOfErrors + 1
				IF TYPE('oExcel') = 'O' AND NOT ISNULL(oExcel) THEN
					oExcel.QUIT()
				ENDIF
				CLOSE DATA

			ENDTRY

			CLOSE DATA
			WAIT CLEAR
			***************** INTERNAL email results ******************************
			.TrackProgress('About to send status email.',LOGIT)
			.TrackProgress('==================================================================================================================', SENDIT)
			.TrackProgress('The logfile is: ' + .cLogFile, SENDIT)
			.TrackProgress('Dashboard Report  process started: ' + .cStartTime, LOGIT+SENDIT)
			.TrackProgress('Dashboard Report  process finished: ' + TTOC(DATETIME()), LOGIT+SENDIT)

			IF .lSendInternalEmailIsOn THEN
				* try to trap error from not having dartmail dll's registered on user's pc...
				TRY
					.cBodyText = .cTopBodyText + CRLF + .cBodyText

					DO FORM dartmail2 WITH .cSendTo,.cFrom,.cSubject,.cCC,.cAttach,.cBodyText,"A"
					.TrackProgress('Sent status email.',LOGIT)
				CATCH TO loError
					*=MESSAGEBOX('There was an error sending the status email. Please contact Mark Bennett.',0+64,"KRONOS Temp Time")
					.TrackProgress('There was an error sending the status email.',LOGIT)
					.TrackProgress('The Error # is: ' + TRANSFORM(loError.ERRORNO), LOGIT)
					.TrackProgress('The Error Message is: ' + TRANSFORM(loError.MESSAGE), LOGIT)
					.TrackProgress('The Error occurred in Program: ' + TRANSFORM(loError.PROCEDURE), LOGIT)
					.TrackProgress('The Error occurred at line #: ' + TRANSFORM(loError.LINENO), LOGIT)
					lnNumberOfErrors = lnNumberOfErrors + 1
					CLOSE DATA
				ENDTRY

			ELSE
				.TrackProgress('Did not send status email: THIS.lSendInternalEmailIsOn = FALSE.',LOGIT)
			ENDIF

		ENDWITH
		RETURN
	ENDFUNC && main


	FUNCTION GetSQLStartDateTimeString
		* returns a timestamp string for use in SQL queries
		LPARAMETERS tdDate
		LOCAL lcSetDate, lcSQLDateTimeString
		lcSetDate = SET('DATE')
		SET DATE YMD
		lcSQLDateTimeString   = "{ts '" + TRANSFORM(YEAR(tdDate)) + "-" + PADL(MONTH(tdDate),2,"0") + "-" + PADL(DAY(tdDate),2,"0") + " 00:00:00.0'}"
		SET DATE &lcSetDate.
		RETURN lcSQLDateTimeString
	ENDFUNC


	FUNCTION GetSQLEndDateTimeString
		* returns a timestamp string for use in SQL queries
		LPARAMETERS tdDate
		LOCAL lcSetDate, lcSQLDateTimeString
		lcSetDate = SET('DATE')
		SET DATE YMD
		lcSQLDateTimeString = "{ts '" + TRANSFORM(YEAR(tdDate)) + "-" + PADL(MONTH(tdDate),2,"0") + "-" + PADL(DAY(tdDate),2,"0") + " 23:59:59.9'}"
		SET DATE &lcSetDate.
		RETURN lcSQLDateTimeString
	ENDFUNC


	FUNCTION ExecSQL
		LPARAMETERS tcSQL, tcCursorName, tlNoDataReturnedIsOkay
		LOCAL llRetval, lnResult
		WITH THIS
			* close target cursor if it's open
			IF USED(tcCursorName)
				USE IN (tcCursorName)
			ENDIF
			WAIT WINDOW "Fetching remote data; please wait..." NOWAIT
			lnResult = SQLEXEC(.nSQLHandle, tcSQL, tcCursorName)
			llRetval = ( lnResult > 0 )
			IF llRetval THEN
				* see if any data came back
				IF NOT tlNoDataReturnedIsOkay THEN
					IF (NOT USED(tcCursorName)) OR EOF(tcCursorName) THEN
						llRetval = .F.
						.TrackProgress('No data was returned by this query: ' +CRLF + tcSQL, LOGIT+SENDIT)
					ENDIF
				ENDIF
			ELSE
				.TrackProgress("Result Code [" + TRANSFORM(lnResult) + "] returned by this query: " + tcSQL, LOGIT+SENDIT)
			ENDIF
			WAIT CLEAR
			RETURN llRetval
		ENDWITH
	ENDFUNC


	PROCEDURE TrackProgress
		* do any combination of Wait Window, writing to logfile, and adding to body of email,
		* based on nFlags parameter.
		LPARAMETERS tcExpression, tnFlags
		WITH THIS
			IF BITAND(tnFlags,LOGIT) = LOGIT THEN
				IF .lLoggingIsOn THEN
					?
					? .cProcessName + "  " + TTOC(DATETIME()) + ": " + tcExpression
				ENDIF
			ENDIF
			IF .lWaitWindowIsOn THEN
				IF BITAND(tnFlags,NOWAITIT) = NOWAITIT THEN
					WAIT WINDOW tcExpression NOWAIT
				ENDIF
				IF BITAND(tnFlags,WAITIT) = WAITIT THEN
					WAIT WINDOW tcExpression TIMEOUT .nWaitWindowTimeout
				ENDIF
			ENDIF
			IF BITAND(tnFlags,SENDIT) = SENDIT THEN
				IF .lSendInternalEmailIsOn THEN
					.cBodyText = .cBodyText + tcExpression + CRLF + CRLF
				ENDIF
			ENDIF
		ENDWITH
	ENDPROC  &&  TrackProgress



ENDDEFINE
