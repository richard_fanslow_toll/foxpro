
***************************************************************************
* Converted PG's SearsEDIImport.prg into Class format 11/30/10 MB
* Build EXE in F:\UTIL\SEARSEDI\
* modified 2/1/2011 to handle multipel EDI types within a input source file
***************************************************************************
LPARAMETERS tcMode

LOCAL loSearsEdiImportProcess, lnError, lcProcessName, lcMode

utilsetup("SEARSEDIIMPORTPROC")

DECLARE INTEGER CreateMutex IN Win32API ;
	STRING @lpMutexAttributes, ;
	STRING bInitialOwner, ;
	STRING @lpName

DECLARE INTEGER GetLastError IN Win32API

* if running in EXE check to see if already running
IF _VFP.STARTMODE > 0 THEN
	lcProcessName = "SearsEdiImportProcess"
	CreateMutex(0,CHR(1),@lcProcessName)
	lnError = GetLastError()
	IF lnError = 183
		WAIT WINDOW TIMEOUT 30 "Sears EDI Import process is already running..."
		schedupdate()
		RETURN .F.
	ENDIF
ENDIF

IF (TYPE('tcMode') = 'C') AND (tcMode == 'SI') THEN
	lcMode = tcMode
ELSE
	lcMode = 'ORIG'
ENDIF

loSearsEdiImportProcess = CREATEOBJECT('SearsEdiImportProcess')
loSearsEdiImportProcess.MAIN( lcMode )

schedupdate()

CLOSE DATABASES ALL


RETURN

#DEFINE LOGIT 1
#DEFINE WAITIT 2
#DEFINE NOWAITIT 4
#DEFINE SENDIT 8
#DEFINE CR CHR(13)
#DEFINE LF CHR(10)
#DEFINE CRLF CHR(13) + CHR(10)
#DEFINE RETURN_DATA_MANDATORY .F.
#DEFINE RETURN_DATA_NOT_MANDATORY .T.
#DEFINE CSVFORMAT 6
#DEFINE WRITE_ONLY_UNBUFFERED 11
#DEFINE COMMA ","

DEFINE CLASS SearsEdiImportProcess AS CUSTOM

	cProcessName = 'SearsEdiImportProcess'

	lTestMode = .F.  && .F. FOR NORMAL OPERATION, .T. TO SUPPRESS ALL BUT DEVELOPER EMAILS AND CHANGE INPUT TABLE, ETC.

	lAutoYield = .T.

	* date time props
	dToday = DATE()

	cStartTime = TTOC(DATETIME())

	* file/folder properties
	cAS2DownLoadFolder = 'F:\FTPUSERS\SEARSEDI\AS2DOWNLOAD\'
	cAS2DownLoadArchiveFolder = 'F:\FTPUSERS\SEARSEDI\AS2DOWNLOAD\ARCHIVE\'
	cEdiInputFolder = 'F:\FTPUSERS\SEARSEDI\EDIDATAIN\'
	cEdiArchiveFolder = 'F:\FTPUSERS\SEARSEDI\EDIDATAIN\ARCHIVE\'
	cEdiOutputArchiveFolder = 'F:\FTPUSERS\SEARSEDI\EDIDATAOUT\ARCHIVE\'	
	cTrigger997Folder = 'F:\FTPUSERS\SEARSEDI\SEARS997\'
	
	* changed output folder per Doug 3/25/11 MB.
	*cOutputFolder = 'F:\FTPUSERS\SEARSEDI\RGTIDATAOUT\'
	cOutputFolder = 'F:\FTPUSERS\MELDISCO\MIRALOMA\PROD\SIS\AS2DOWNLOAD\MYMAILBOX\'
	
	* table properties
	cPOTable = 'F:\SEARSEDI\DATA\OUTPO'
	cStoresTable = 'F:\SEARSEDI\DATA\STORES'
	cISAsLoadedTable = 'F:\SEARSEDI\DATA\ISAS'

	* processing properties
	lArchive = .T.
	cEDIType = "999"
	cThrowDescription = ''
	nOutRecs = 0
	cISAList = "ISA's processed:"
	nFilesProcessed = 0
	cMode = "ORIG"  && IF = 'SI' THEN LOOK FOR SOURCE FILES IN NEW 3/10/2015 LOCATIONS, AND 

	* wait window properties
	nWaitWindowTimeout = 2
	lWaitWindowIsOn = .T.

	* logfile properties
	lLoggingIsOn = .T.
	cLogFile = 'F:\FTPUSERS\SEARSEDI\LOGFILES\SearsEdiImportProcess_log.txt'

	* INTERNAL email properties
	lSendInternalEmailIsOn = .T.
	cFrom = 'TGF Corporate Communication Department <transload-ops@fmiint.com>'
	cSendTo = 'mbennett@fmiint.com'
	cCC = ''
	*cSendTo = 'DougWachs@fmiint.com'
	*cCC = 'mbennett@fmiint.com,pgaidis@fmiint.com'
	cSubject = 'Sears EDI Import Process Results for ' + TTOC(DATETIME())
	cAttach = ''
	cBodyText = ''
	cTopBodyText = ''

	cCOMPUTERNAME = ''
	cUSERNAME = ''


	FUNCTION INIT
		IF NOT DODEFAULT()
			RETURN .F.
		ENDIF
		WITH THIS
			*SET RESOURCE OFF
			CLOSE DATA
			DO setenvi
			SET CENTURY ON
			SET DATE YMD
			SET DECIMAL TO 0
			SET TALK OFF
			SET DELETED ON
			SET CONSOLE OFF
			SET EXCLUSIVE OFF
			SET SAFETY OFF
			SET EXACT OFF
			SET STATUS BAR ON
			SET SYSMENU OFF
			SET ENGINEBEHAVIOR 70
			_VFP.AUTOYIELD = .lAutoYield
			.cCOMPUTERNAME = GETENV("COMPUTERNAME")
			.cUSERNAME = GETENV("USERNAME")
			IF .lTestMode THEN
				.cAS2DownLoadFolder = 'C:\SEARSEDI\AS2DOWNLOAD\'
				.cAS2DownLoadArchiveFolder = 'C:\SEARSEDI\AS2DOWNLOAD\ARCHIVE\'
				.cEdiInputFolder = 'C:\SEARSEDI\EDIDATAIN\'
				.cEdiArchiveFolder = 'C:\SEARSEDI\EDIDATAIN\ARCHIVE\'
				.cOutputFolder = 'C:\SEARSEDI\MELDISCO\MYMAILBOX\'
				.cEdiOutputArchiveFolder = 'C:\SEARSEDI\EDIDATAOUT\ARCHIVE\'
				.cTrigger997Folder = 'C:\SEARSEDI\SEARS997\'
				.cLogFile = 'F:\FTPUSERS\SEARSEDI\LOGFILES\SearsEdiImportProcess_LOG_TESTMODE.txt'
				.cSendTo = 'mbennett@fmiint.com'
				.cCC = ''
				.cPOTable = 'F:\UTIL\SEARSEDI\TESTDATA\OUTPO'
				.cStoresTable = 'F:\UTIL\SEARSEDI\TESTDATA\STORES'
			ENDIF
			.lLoggingIsOn = .lLoggingIsOn AND NOT EMPTY(.cLogFile)
		ENDWITH
	ENDFUNC


	FUNCTION DESTROY
		WITH THIS
			IF .lLoggingIsOn  THEN
				SET ALTERNATE OFF
				SET ALTERNATE TO
			ENDIF
		ENDWITH
		DODEFAULT()
	ENDFUNC


	FUNCTION MAIN
		LPARAMETERS tcMode
		WITH THIS

			TRY

				*THROW

				LOCAL lnNumberOfErrors, laFiles[1,5], laFiles2[1,5], laFilesSorted[1,6]
				LOCAL lnNumFiles, lnCurrentFile, lcSourceFile, lcArchiveFile, lcTargetFile
				LOCAL lcEDISourceFile, lcEDIArchiveFile, lcOutstrx, lcDelimChar, llArchived, lcTrigger997File, lcSHC_EDI_ID

				PRIVATE m.dateloaded, m.sent, m.ponum, m.chngcode, m.chngdesc, m.purpose, m.pcode, m.dept, m.vendorid
				PRIVATE m.shipdc, m.shipstore, m.billto, m.sac, m.chngqty, m.style, m.sku, m.upc, m.qty, m.storename, m.storenum
				PRIVATE m.storealias, m.addr1, m.addr2, m.state, m.zip, m.isa, m.qualdate, m.maintcode, m.direction
				PRIVATE m.potype, m.addproc, m.promonum
				
				.cMode = tcMode

				* set up file/folders based on cMode
				* NOTE: Doug's folders do not change based on cMode.
				DO CASE
					CASE .cMode == 'SI'
						* use the new folders
						.cAS2DownLoadFolder = 'F:\FTPUSERS\SEARSEDI-SI\AS2DOWNLOAD\'
						.cAS2DownLoadArchiveFolder = 'F:\FTPUSERS\SEARSEDI-SI\AS2DOWNLOAD\ARCHIVE\'
						.cEdiInputFolder = 'F:\FTPUSERS\SEARSEDI-SI\EDIDATAIN\'
						.cEdiArchiveFolder = 'F:\FTPUSERS\SEARSEDI-SI\EDIDATAIN\ARCHIVE\'
						.cEdiOutputArchiveFolder = 'F:\FTPUSERS\SEARSEDI-SI\EDIDATAOUT\ARCHIVE\'	
						.cTrigger997Folder = 'F:\FTPUSERS\SEARSEDI-SI\SEARS997\'
					OTHERWISE
						* use the old folders
						.cAS2DownLoadFolder = 'F:\FTPUSERS\SEARSEDI\AS2DOWNLOAD\'
						.cAS2DownLoadArchiveFolder = 'F:\FTPUSERS\SEARSEDI\AS2DOWNLOAD\ARCHIVE\'
						.cEdiInputFolder = 'F:\FTPUSERS\SEARSEDI\EDIDATAIN\'
						.cEdiArchiveFolder = 'F:\FTPUSERS\SEARSEDI\EDIDATAIN\ARCHIVE\'
						.cEdiOutputArchiveFolder = 'F:\FTPUSERS\SEARSEDI\EDIDATAOUT\ARCHIVE\'	
						.cTrigger997Folder = 'F:\FTPUSERS\SEARSEDI\SEARS997\'
				ENDCASE

				m.dateloaded = DATETIME()
				m.sent = .F.
				m.ponum = ""
				m.chngcode = ""
				m.chngdesc = ""
				m.purpose = ""
				m.pcode = ""
				m.dept = ""
				m.vendorid = ""
				m.shipdc = ""
				m.shipstore = ""
				m.billto = ""
				m.sac = ""
				m.promonum = SPACE(2)
				m.chngqty = 0
				m.style = ""
				m.sku = ""
				m.upc = ""
				m.qty = 0
				m.storename = ""
				m.storenum = ""
				m.storealias = ""
				m.addr1 = ""
				m.addr2 = ""
				m.state = ""
				m.zip = ""
				m.isa = ""
				m.qualdate = CTOD('')
				m.maintcode = ""
				m.qdatetype = ""
				m.direction = ""
				m.addproc = "SEARSEDIIMPORT"

				lnNumberOfErrors = 0

				IF .lLoggingIsOn THEN
					SET ALTERNATE TO (.cLogFile) ADDITIVE
					SET ALTERNATE ON
				ENDIF

				.TrackProgress('Sears EDI Import Process process started....', LOGIT+SENDIT+NOWAITIT)
				.TrackProgress('COMPUTERNAME = ' + .cCOMPUTERNAME, LOGIT+SENDIT)
				.TrackProgress('USERNAME = ' + .cUSERNAME, LOGIT+SENDIT)
				.TrackProgress('PROJECT = SEARSEDIIMPORTPROC', LOGIT+SENDIT)
				.TrackProgress('MODE = ' + .cMode, LOGIT+SENDIT)
				IF .lTestMode THEN
					.TrackProgress('TEST MODE', LOGIT+SENDIT)
				ENDIF
				.TrackProgress('.cAS2DownLoadFolder = ' + .cAS2DownLoadFolder, LOGIT+SENDIT)
				.TrackProgress('.cAS2DownLoadArchiveFolder = ' + .cAS2DownLoadArchiveFolder, LOGIT+SENDIT)
				.TrackProgress('.cEdiInputFolder = ' + .cEdiInputFolder, LOGIT+SENDIT)
				.TrackProgress('.cEdiArchiveFolder = ' + .cEdiArchiveFolder, LOGIT+SENDIT)
				.TrackProgress('.cOutputFolder = ' + .cOutputFolder, LOGIT+SENDIT)
				.TrackProgress('.cEdiOutputArchiveFolder = ' + .cEdiOutputArchiveFolder, LOGIT+SENDIT)
				.TrackProgress('.cTrigger997Folder = ' + .cTrigger997Folder, LOGIT+SENDIT)
				.TrackProgress('.cSendTo = ' + .cSendTo, LOGIT+SENDIT)
				.TrackProgress('.cCC = ' + .cCC, LOGIT+SENDIT)

				CLOSE DATA ALL
				
				USE (.cISAsLoadedTable) IN 0 ALIAS ISATABLE

				***************************************************************************************************************
				* first, move files from the AS2DOWNLOAD folder into the EDIDATAIN folder.
				***************************************************************************************************************

				lnNumFiles = ADIR(laFiles,(.cAS2DownLoadFolder + "*.txt"))

				IF lnNumFiles > 0 THEN

					FOR lnCurrentFile = 1 TO lnNumFiles
						lcSourceFile = .cAS2DownLoadFolder + laFiles[lnCurrentFile,1]
						lcArchiveFile = .cAS2DownLoadArchiveFolder + laFiles[lnCurrentFile,1]
						lcTargetFile = .cEdiInputFolder + laFiles[lnCurrentFile,1]
						lcTrigger997File = .cTrigger997Folder + laFiles[lnCurrentFile,1]

						*.TrackProgress("Moving file: " + lcSourceFile,LOGIT+SENDIT+NOWAITIT)

						* if the file already exists in the archive folder, make sure it is not read-only.
						* this is to prevent errors we get copying into archive on a resend of an already-archived file.
						* Necessary because the files in the archive folders were sometimes marked as read-only (not sure why)
						llArchived = FILE(lcArchiveFile)
						IF llArchived THEN
							RUN ATTRIB -R &lcArchiveFile.
							.TrackProgress('   Used ATTRIB -R for re-archiving : ' + lcSourceFile,LOGIT+SENDIT)
						ENDIF

						* copy source file to target folder
						COPY FILE (lcSourceFile) TO (lcTargetFile)

						IF NOT .lTestMode THEN
							* copy source file to 997 Trigger folder
							COPY FILE (lcSourceFile) TO (lcTrigger997File)
						ENDIF

						* archive source file
						COPY FILE (lcSourceFile) TO (lcArchiveFile)

						IF NOT .lTestMode THEN
							* delete original source file if the copies were ok...
							IF FILE(lcTargetFile) AND FILE(lcArchiveFile) THEN
								DELETE FILE(lcSourceFile)
								.TrackProgress("Archived file: " + lcSourceFile + ' to ' + .cAS2DownLoadArchiveFolder,LOGIT+SENDIT)
								.TrackProgress("Copied file: " + lcSourceFile + ' to ' + .cEdiInputFolder,LOGIT+SENDIT+NOWAITIT)
								.TrackProgress("For 997 Trigger, copied file: " + lcSourceFile + ' to ' + .cTrigger997Folder,LOGIT+SENDIT)
							ELSE
								.TrackProgress('!! There were errors copying file: ' + lcSourceFile + ' to ' + .cEdiInputFolder,LOGIT+SENDIT)
							ENDIF
						ENDIF

					ENDFOR

				ELSE
					.TrackProgress('Found no files in the AS2DOWNLOAD folder', LOGIT)
				ENDIF

				*.TrackProgress('=========================================================', LOGIT+SENDIT)

				***************************************************************************************************************
				* Now process any files in the EDIDATAIN folder.
				***************************************************************************************************************
				PUBLIC lTestImport

				lTestImport = .lTestMode
				*!*					.cEDIType = "999"

				lnNumFiles = ADIR(laFiles2,(.cEdiInputFolder+ "*.txt"))

				IF lnNumFiles > 0 THEN

					PRIVATE cDelimiter, cTranslateOption

					cDelimiter = "*"
					cTranslateOption = "NONE"
					DO createx856a

					*!*						USE F:\searsedi\DATA\outpo IN 0
					*!*						USE F:\searsedi\DATA\stores IN 0
					USE ( .cPOTable ) IN 0 ALIAS outpo
					USE ( .cStoresTable ) IN 0 ALIAS stores

					* sort file list by date/time
					.SortArrayByDateTime(@laFiles2, @laFilesSorted)

					FOR lnCurrentFile = 1 TO lnNumFiles
					
						.nFilesProcessed = .nFilesProcessed + 1
						
						lcEDISourceFile = .cEdiInputFolder + laFilesSorted[lnCurrentFile,1]
						lcEDIArchiveFile = .cEdiArchiveFolder + laFilesSorted[lnCurrentFile,1]

						*lcTargetFile = .cEdiInputFolder + laFilesSorted[lnCurrentFile,1]

						.nOutRecs = 0  && tracking # of recs appended to either OutPo or Stores table, to try and identify empty files

						.TrackProgress("Processing EDI file: " + lcEDISourceFile,LOGIT+SENDIT+NOWAITIT)

						lcOutstrx = FILETOSTR(lcEDISourceFile)
						IF SUBSTR(lcOutstrx,1,3) = "ISA" THEN
							* continue processing

							lcDelimChar = SUBSTR(lcOutstrx,4,1)
							*lcEOLChar = SUBSTR(lcOutstrx,106,1)

							DO loadedifile WITH lcEDISourceFile,lcDelimChar,"SEARSEDI"
							SELECT x856

							*SET STEP ON

							DO WHILE !EOF("x856")
								m.filename = laFilesSorted[lnCurrentFile,1]

								*cSegment = ALLTRIM(x856.segment)
								*cCode = ALLTRIM(x856.f1)

								IF ALLTRIM(x856.segment) = "ISA"
									m.isa = ALLTRIM(x856.f13)
									.TrackProgress("ISA # = " + m.isa,LOGIT+SENDIT)
									*.cISAList = .cISAList + " " + m.isa
									*.cISAList = .cISAList + " " + RIGHT(m.isa,4)
									.cISAList = .cISAList + " " + RIGHT(m.isa,5)
									
									* added 7/16/13 MB to better track the ISA #s we've handled.
									INSERT INTO ISATABLE(isanum, sourcefile, whendt) VALUES (m.isa, lcEDISourceFile, DATETIME())
									
									SELECT x856
									SKIP
									LOOP
								ENDIF

								IF ALLTRIM(x856.segment) = "GS"
								
									* check the edi id that was sent. If cMode = 'SI', it means we are using the new comm folders and should be getting one of the new ids.
									lcSHC_EDI_ID = ALLTRIM(x856.f2)
									.TrackProgress("SHC EDI ID = " + lcSHC_EDI_ID,LOGIT+SENDIT)
									
									*!*	DO CASE
									*!*		CASE (.cMode == 'SI') AND INLIST(lcSHC_EDI_ID,'6111250011','6111250055','6111250084') 
									*!*			* OKAY
									*!*		CASE (.cMode == 'SI') AND NOT INLIST(lcSHC_EDI_ID,'6111250011','6111250055','6111250084')
									*!*			* error 
									*!*			.TrackProgress("ERROR: DID NOT RECEIVE EXPECTED ID 6111250011 OR 6111250055 OR 6111250084",LOGIT+SENDIT)
									*!*		CASE (.cMode <> 'SI') AND INLIST(lcSHC_EDI_ID,'6111250055','6111250084') 
									*!*			* unexpected id
									*!*			.TrackProgress("ERROR: RECEIVED UNEXPECTED ID 6111250055 OR 6111250084",LOGIT+SENDIT)
									*!*		OTHERWISE
									*!*		* NOTHING									
									*!*	ENDCASE
									
									*  11/29/2016 MB; currently Softshare on EDI1 will send 997s for these IDs to the SI Channel, :
									* ===>	'6111250011','6111250050','6111250055','6111250084'
									*  So warn if we get any different IDs because they would not generate a 997.
									* (NOTE: there are currently no IDs in the non-SI channel in SoftShare)
									IF NOT INLIST(lcSHC_EDI_ID,'6111250011','6111250050','6111250055','6111250084') THEN
										.TrackProgress("====> WARNING: RECEIVED UNEXPECTED SHC EDI ID",LOGIT+SENDIT)
									ENDIF
									
									.TrackProgress("ISA # = " + m.isa,LOGIT+SENDIT)
									SELECT x856
									SKIP
									LOOP
								ENDIF

								IF ALLTRIM(x856.segment) = "ST" AND (NOT INLIST(ALLTRIM(x856.f1),"850","860","816")) THEN
									.cEDIType = ALLTRIM(x856.f1)
									.TrackProgress(.cEDIType + " - " + ALLTRIM(x856.f2) + " segment encountered.",LOGIT+SENDIT)
									SELECT x856
									SKIP
									LOOP
								ENDIF

								DO CASE

									CASE ALLTRIM(x856.segment) = "ST" AND ALLTRIM(x856.f1) = "850"
										.cEDIType = "850"
										.TrackProgress(.cEDIType + " - " + ALLTRIM(x856.f2) + " segment encountered.",LOGIT+SENDIT)
										*.TrackProgress("New " + .cEDIType + " segment encountered.",LOGIT+SENDIT)
										.parse850out()

									CASE ALLTRIM(x856.segment) = "ST" AND ALLTRIM(x856.f1) = "860"
										.cEDIType = "860"
										.TrackProgress(.cEDIType + " - " + ALLTRIM(x856.f2) + " segment encountered.",LOGIT+SENDIT)
										.parse860out()

									CASE ALLTRIM(x856.segment) = "ST" AND ALLTRIM(x856.f1) = "816"
										.cEDIType = "816"
										.TrackProgress(.cEDIType + " - " + ALLTRIM(x856.f2) + " segment encountered.",LOGIT+SENDIT)
										.parse816out()

									OTHERWISE
										* NOTHING
								ENDCASE

								SELECT x856
								IF !EOF("x856") AND (NOT ALLTRIM(x856.segment) = "ST") THEN
									SKIP
								ENDIF

							ENDDO  && WHILE !EOF("x856")

							.WriteOutFile("816")
							.WriteOutFile("850/860")

							.TrackProgress("# of OutPo / Stores inserts: " + TRANSFORM(.nOutRecs),LOGIT+SENDIT)

							*IF NOT .lTestMode THEN
							* archive and then delete the file.
							llArchived = FILE(lcEDIArchiveFile)
							IF llArchived THEN
								RUN ATTRIB -R &lcEDIArchiveFile.
								.TrackProgress('   Used ATTRIB -R for re-archiving : ' + lcEDISourceFile,LOGIT+SENDIT)
							ENDIF

							* archive source file
							COPY FILE (lcEDISourceFile) TO (lcEDIArchiveFile)

							* delete original source file if the copy was made...
							IF FILE(lcEDIArchiveFile) THEN
								DELETE FILE(lcEDISourceFile)
								.TrackProgress("Archived file: " + lcEDISourceFile + ' to ' + .cEdiArchiveFolder,LOGIT+SENDIT)
							ELSE
								.TrackProgress("!! There were errors archiving file: " + lcEDISourceFile,LOGIT+SENDIT)
							ENDIF
							*ENDIF

						ELSE
							.TrackProgress("ERROR: " + lcEDISourceFile + " is not an EDI File!",LOGIT+SENDIT)
						ENDIF

						*.TrackProgress('=========================================================', LOGIT+SENDIT)

					ENDFOR && lnCurrentFile = 1 TO lnNumFiles


				ELSE
					.TrackProgress('Found no files in the EDIDATAIN folder', LOGIT+SENDIT)
					* don't send an email if we processed no files -- to support running this proc every half hour per Doug.
				ENDIF

				.TrackProgress('Sears EDI Import Process process ended normally!',LOGIT+SENDIT)

				.cBodyText = .cISAList + CRLF + CRLF + .cTopBodyText + CRLF + ;
					"<Report log follows>" + CRLF + CRLF + .cBodyText

			CATCH TO loError

				.TrackProgress('There was an error.',LOGIT+SENDIT)
				.TrackProgress('The Error # is: ' + TRANSFORM(loError.ERRORNO), LOGIT+SENDIT)
				.TrackProgress('The Error Message is: ' + TRANSFORM(loError.MESSAGE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred in Program: ' + TRANSFORM(loError.PROCEDURE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred at line #: ' + TRANSFORM(loError.LINENO), LOGIT+SENDIT)
				.TrackProgress(.cThrowDescription, LOGIT+SENDIT)
				lnNumberOfErrors = lnNumberOfErrors + 1
				CLOSE DATA
				IF .lTestMode THEN
					.cSendTo = 'mbennett@fmiint.com'
				ELSE
					.cSendTo = 'DougWachs@fmiint.com,mbennett@fmiint.com,pgaidis@fmiint.com'
				ENDIF

			ENDTRY

			WAIT CLEAR
			CLOSE DATABASES ALL
			
			TRY
				* ZAP THE PO TABLE, OTHERWISE IT WILL EXCEED FOXPRO FILE SIZE LIMITS WITHIN A FEW MONTHS
				USE ( .cPOTable ) ALIAS outpo EXCLUSIVE 
				SELECT outpo
				DELETE ALL
				PACK
				.TrackProgress('PACKed PO TABLE!',LOGIT+SENDIT)
			CATCH
				.TrackProgress('COULD NOT PACK PO TABLE!',LOGIT+SENDIT)
			ENDTRY
			*CLOSE ALL
			***************** INTERNAL email results ******************************
			.TrackProgress('About to send status email.',LOGIT)
			.TrackProgress('The logfile is: ' + .cLogFile, SENDIT)
			.TrackProgress('Sears EDI Import Process process started: ' + .cStartTime, LOGIT+SENDIT)
			.TrackProgress('Sears EDI Import Process process finished: ' + TTOC(DATETIME()), LOGIT+SENDIT)

			IF .lSendInternalEmailIsOn THEN
				IF (.nFilesProcessed > 0) OR (lnNumberOfErrors > 0) THEN
					DO FORM dartmail2 WITH .cSendTo,.cFrom,.cSubject,.cCC,.cAttach,.cBodyText,"A"
					.TrackProgress('Sent status email.',LOGIT)
				ENDIF
			ELSE
				.TrackProgress('Did not send status email: THIS.lSendInternalEmailIsOn = FALSE.',LOGIT)
			ENDIF

		ENDWITH
		RETURN
	ENDFUNC && main


	PROCEDURE WriteOutFile
		LPARAMETERS	tcEDIType
		WITH THIS

			*.TrackProgress("In WriteOutFile(), tcEDIType = " + tcEDIType,LOGIT+SENDIT)

			LOCAL lcSeqNum, lcOutputFile, lnFileHandle, lnCTR, lcOutString, lcQualDate, lcEdiOutArchiveFile

			DO CASE
				CASE INLIST(tcEDIType,"850/860")

					IF USED('temp') THEN
						USE IN temp
					ENDIF

					SELECT * ;
						FROM outpo ;
						WHERE sent = .F. ;
						AND (editype = "850" OR editype = "860") ;
						INTO CURSOR temp

					IF USED('temp') AND NOT EOF('temp') THEN
					
						WAIT WINDOW "Delaying to guarantee unique time-based filenames..." TIMEOUT 5

						lcSeqNum = "1001"
						lcOutputFile = "SEARSPODATA_"+ALLTRIM(STR(YEAR(DATE())))+PADL(ALLTRIM(STR(MONTH(DATE()))),2,"0")+;
							PADL(ALLTRIM(STR(DAY(DATE()))),2,"0")+PADL(ALLTRIM(STR(HOUR(DATETIME()))),2,"0")+;
							PADL(ALLTRIM(STR(MINUTE(DATETIME()))),2,"0")+PADL(ALLTRIM(STR(SEC(DATETIME()))),2,"0")+"_"+lcSeqNum+".txt"
						
						lcEdiOutArchiveFile = .cEdiOutputArchiveFolder + lcOutputFile
												
						lcOutputFile = .cOutputFolder + lcOutputFile

						lnFileHandle = FCREATE(lcOutputFile)
						IF !lnFileHandle>0
							.TrackProgress("!! Unable to create PO output file: " + lcOutputFile,LOGIT+SENDIT)
							RETURN
						ENDIF

						lnCTR = 1
						SELECT temp
						SCAN
							lcOutString= ""
							lcOutString= ALLTRIM(STR(YEAR(DATETIME())))+PADL(MONTH(DATETIME()),2,"0")+PADL(DAY(DATETIME()),2,"0")
							lcOutString= lcOutString+PADL(HOUR(DATETIME()),2,"0")+PADL(MINUTE(DATETIME()),2,"0")+PADL(SEC(DATETIME()),2,"0")
							lcOutString= lcOutString+PADL(ALLTRIM(STR(lnCTR)),7,"0")
							lcOutString= lcOutString+PADL(ALLTRIM(isa),10,"0")
							lcOutString= lcOutString+PADL(ALLTRIM(editype),6," ")
							lcOutString= lcOutString+potype
							lcOutString= lcOutString+pcode
							lcOutString= lcOutString+purpose
							lcOutString= lcOutString+ponum
							lcOutString= lcOutString+STRTRAN(DTOC(shipdate),"/","")
							lcOutString= lcOutString+STRTRAN(DTOC(reqdate),"/","")
							lcOutString= lcOutString+STRTRAN(DTOC(newshipdt),"/","")
							lcOutString= lcOutString+dept
							lcOutString= lcOutString+vendorid
							lcOutString= lcOutString+STRTRAN(DTOC(podate),"/","")
							lcOutString= lcOutString+PADR(ALLTRIM(shipdc),10," ")
							lcOutString= lcOutString+PADR(ALLTRIM(shipstore),10," ")
							lcOutString= lcOutString+PADR(ALLTRIM(billto),10," ")
							lcOutString= lcOutString+PADR(ALLTRIM(STYLE),20," ")
							lcOutString= lcOutString+PADR(ALLTRIM(upc),14," ")
							lcOutString= lcOutString+PADR(ALLTRIM(dept)+ALLTRIM(STYLE)+ALLTRIM(sku),17," ")  &&Padl(Alltrim(sku),17," ")
							lcOutString= lcOutString+PADL(ALLTRIM(STR(qty)),7,"0")
							lcOutString= lcOutString+PADL(ALLTRIM(STR(chngqty)),7,"0")
							lcOutString= lcOutString+chngcode
							lcOutString= lcOutString+chngdesc
							lcOutString= lcOutString+sac
							lcOutString= lcOutString+promonum
							lcOutString= lcOutString+STRTRAN(DTOC(TTOD(dateloaded)),"/","")
							lcOutString= lcOutString+filename
							lcOutString= lcOutString+STRTRAN(DTOC(datesent),"/","")
							FPUTS(lnFileHandle,lcOutString)
							lnCTR = lnCTR+1
						ENDSCAN

						FCLOSE(lnFileHandle)

						* copy output file to edi out archive folder
						COPY FILE (lcOutputFile) TO (lcEdiOutArchiveFile)						

						.TrackProgress("Created PO output file: " + lcOutputFile,LOGIT+SENDIT)
						.TrackProgress("Created PO archive file: " + lcEdiOutArchiveFile,LOGIT+SENDIT)
						.cTopBodyText = .cTopBodyText + "Created PO output file: " + lcOutputFile + CRLF + CRLF

						SELECT outpo
						REPLACE datesent WITH DATETIME(), sent WITH .T. ;
							FOR sent = .F. AND (editype = "850" OR editype = "860") ;
							IN outpo
					ELSE
						.TrackProgress("No new PO data found" ,LOGIT+SENDIT)
					ENDIF  && USED('temp')

				CASE INLIST(tcEDIType ,"816")

					IF USED('temp') THEN
						USE IN temp
					ENDIF

					SELECT * ;
						FROM stores ;
						WHERE sent =.F. ;
						AND editype = tcEDIType ;
						INTO CURSOR temp

					IF USED('temp') AND NOT EOF('temp') THEN
					
						WAIT WINDOW "Delaying to guarantee unique time-based filenames..." TIMEOUT 5

						lcSeqNum = "1001"
						lcOutputFile = "SEARSSTOREDATA_"+ALLTRIM(STR(YEAR(DATE())))+PADL(ALLTRIM(STR(MONTH(DATE()))),2,"0")+;
							PADL(ALLTRIM(STR(DAY(DATE()))),2,"0")+PADL(ALLTRIM(STR(HOUR(DATETIME()))),2,"0")+;
							PADL(ALLTRIM(STR(MINUTE(DATETIME()))),2,"0")+PADL(ALLTRIM(STR(SEC(DATETIME()))),2,"0")+"_"+lcSeqNum+".txt"
						
						lcEdiOutArchiveFile = .cEdiOutputArchiveFolder + lcOutputFile

						lcOutputFile = .cOutputFolder + lcOutputFile

						lnFileHandle=FCREATE(lcOutputFile)
						IF !lnFileHandle>0
							.TrackProgress("!! Unable to create STORES output file: " + lcOutputFile,LOGIT+SENDIT+NOWAITIT)
							RETURN
						ENDIF

						lnCTR = 0
						SELECT temp
						SCAN
							lcOutString = ""
							lcOutString = "CSTM"+"A"+"12"
							lcOutString= lcOutString+PADL(ALLTRIM(storenum),5,"0")
							lcOutString= lcOutString+PADR(ALLTRIM(storename),30," ")
							lcOutString= lcOutString+PADR(ALLTRIM(storealias),30," ")
							lcOutString= lcOutString+PADR(ALLTRIM(addr1),30," ")
							lcOutString= lcOutString+PADR(ALLTRIM(addr2),30," ")
							lcOutString= lcOutString+PADR(ALLTRIM(city),30," ")
							lcOutString= lcOutString+state
							lcOutString= lcOutString+PADR(ALLTRIM(zip),10," ")
							lcOutString= lcOutString+SPACE(15)
							lcOutString= lcOutString+SPACE(8)

							IF temp.qdatetype = "C" THEN
								* qualdate is a closing date, write it out, per Doug
								lcQualDate = STRTRAN(TRANSFORM(qualdate),"/","")
								lcOutString= lcOutString + PADR(lcQualDate,10," ")
							ELSE
								* qualdate is another date type, do not write it out
								lcOutString= lcOutString+SPACE(10)
							ENDIF
							*lcOutString= lcOutString+SPACE(3)
							lcOutString= lcOutString + PADL(maintcode,3,"0")

							lcOutString= lcOutString+SPACE(5)
							FPUTS(lnFileHandle,lcOutString)
						ENDSCAN

						FCLOSE(lnFileHandle)
						
						* copy output file to edi out archive folder
						COPY FILE (lcOutputFile) TO (lcEdiOutArchiveFile)						
						
						.TrackProgress("Created STORES output file: " + lcOutputFile,LOGIT+SENDIT)
						.TrackProgress("Created STORES archive file: " + lcEdiOutArchiveFile,LOGIT+SENDIT)
						.cTopBodyText = .cTopBodyText + "Created STORES output file: " + lcOutputFile + CRLF + CRLF

						SELECT stores
						REPLACE datesent WITH DATETIME(), dateloaded WITH DATETIME(), sent WITH .T. ;
							FOR sent = .F. AND editype = tcEDIType ;
							IN stores
					ELSE
						.TrackProgress("No new STORES data found",LOGIT+SENDIT)
					ENDIF  && USED('temp')

				OTHERWISE
					* nothing
			ENDCASE
		ENDWITH

	ENDPROC && WriteOutFile


	FUNCTION ConvertYYYYMMDDStringToDate
		LPARAMETERS tcYYYYMMDDString
		* this assumes we are already in SET DATE YMD mode...
		LOCAL ldDate
		ldDate = CTOD( LEFT(tcYYYYMMDDString,4) + "/" + SUBSTR(tcYYYYMMDDString,5,2) + "/" + SUBSTR(tcYYYYMMDDString,7,2) )
		RETURN ldDate
	ENDFUNC


	PROCEDURE parse850out
		WITH THIS

			LOCAL lcDate, lxDate

			.clear_variables( "ALL" )

			SELECT x856
			SKIP

			DO WHILE !INLIST(ALLTRIM(x856.segment),"SE","GE","IEA")
				m.editype="850"
				IF ALLTRIM(x856.segment) = "BEG"  && new PO
					m.ponum = ALLTRIM(f3)
					m.potype=ALLTRIM(f2)
					lcDate = ALLTRIM(x856.f5)
					*lxDate = CTOD(SUBSTR(lcDate,5,2)+"/"+SUBSTR(lcDate,7,2)+"/"+LEFT(lcDate,4))
					*m.podate = lxDate
					m.podate = .ConvertYYYYMMDDStringToDate( lcDate )
				ENDIF
				IF ALLTRIM(x856.segment) = "REF" AND ALLTRIM(f1)="DP"
					m.dept = ALLTRIM(f2)
				ENDIF
				IF ALLTRIM(x856.segment) = "REF" AND ALLTRIM(f1)="IA"
					m.vendorid = ALLTRIM(f2)
				ENDIF
				IF ALLTRIM(x856.segment) = "REF" AND ALLTRIM(f1)="PD"
					m.promonum = ALLTRIM(f2)
*!*						IF EMPTY(m.promonum) THEN
*!*							m.promonum = SPACE(2)
*!*						ELSE
*!*							m.promonum = LEFT(m.promonum,2)  && Doug says this Promo Code is only 2 digits
*!*						ENDIF
				ENDIF
				IF ALLTRIM(x856.segment) = "SAC"
					m.sac = ALLTRIM(f15)
				ENDIF
				IF ALLTRIM(x856.segment) = "DTM"
					lcDate = ALLTRIM(x856.f2)
					*lxDate = CTOD(SUBSTR(lcDate,5,2)+"/"+SUBSTR(lcDate,7,2)+"/"+LEFT(lcDate,4))
					*m.shipdate = lxDate
					m.shipdate = .ConvertYYYYMMDDStringToDate( lcDate )
				ENDIF
				IF ALLTRIM(x856.segment) = "N1" AND ALLTRIM(f1)="BY"
					m.billto = ALLTRIM(f4)
					* PO TYPE IN/OUT BASED ON THIS LOGIC PER DOUG 1/27/2011
					IF m.billto = "8110" THEN
						m.direction = "IN"
					ELSE
						m.direction = "OUT"
					ENDIF
				ENDIF
				IF ALLTRIM(x856.segment) = "N1" AND ALLTRIM(f1)="ST"
					m.shipdc = ALLTRIM(f4)
				ENDIF
				IF ALLTRIM(x856.segment) = "N1" AND ALLTRIM(f1)="Z7"
					m.shipstore = ALLTRIM(f4)
				ENDIF

				IF ALLTRIM(x856.segment) = "PO1"
					DO WHILE ALLTRIM(x856.segment) = "PO1"
						.clear_variables( "DETAIL" )
						m.dateloaded = DATETIME()
						m.sent = .F.
						DO CASE
							CASE ALLTRIM(f6)="IN"
								m.style= PADL(ALLTRIM(x856.f7),5,'0')
							CASE ALLTRIM(f6)="IZ"
								m.sku= ALLTRIM(x856.f7)
							CASE ALLTRIM(f6)="UP"
								m.upc  = ALLTRIM(x856.f7)
						ENDCASE
						DO CASE
							CASE ALLTRIM(f8)="IN"
								m.style= PADL(ALLTRIM(x856.f9),5,'0')
							CASE ALLTRIM(f8)="IZ"
								m.sku= ALLTRIM(x856.f9)
							CASE ALLTRIM(f8)="UP"
								m.upc  = ALLTRIM(x856.f9)
						ENDCASE
						DO CASE
							CASE ALLTRIM(f10)="IN"
								m.style= PADL(ALLTRIM(x856.f11),5,'0')
							CASE ALLTRIM(f10)="IZ"
								m.sku= ALLTRIM(x856.f11)
							CASE ALLTRIM(f10)="UP"
								m.upc  = ALLTRIM(x856.f11)
						ENDCASE
						m.qty  = VAL(ALLTRIM(x856.f2))
						SELECT outpo
						APPEND BLANK
						GATHER MEMVAR
						.nOutRecs = .nOutRecs + 1
						SELECT x856
						SKIP 1
					ENDDO
				ENDIF
				SELECT x856
				SKIP
			ENDDO
		ENDWITH
	ENDPROC && parse850out


	PROCEDURE parse816out
		WITH THIS
			LOCAL lcDate
			m.editype="816"
			.clear_variables( "816" )
			SELECT x856
			DO WHILE ALLTRIM(x856.segment) != "SE"
				IF INLIST(ALLTRIM(x856.segment),"N1","N2","N3","N4","DTM","ASI") THEN
					IF ALLTRIM(x856.segment) = "N1" AND ALLTRIM(x856.f1) = "BU" THEN
						IF NOT EMPTY(m.storename) THEN
							* WE JUST PARSED A SEQUENCE OF "N1","N2","N3"...SEGMENTS, AND ARE BEGINNING A NEW ONE
							* SO WRITE TO STORE TABLE FOR PREVIOUS SEG DATA AND RE-CLEAR VARIABLES FOR THE NEW ONE
							SELECT stores
							APPEND BLANK
							GATHER MEMVAR
							.nOutRecs = .nOutRecs + 1
							.clear_variables( "816" )
						ENDIF
					ENDIF
					DO CASE
						CASE ALLTRIM(x856.segment) = "N1" AND ALLTRIM(x856.f1) = "BU"
							m.storename = ALLTRIM(x856.f2)
							m.storenum = ALLTRIM(x856.f4)
						CASE ALLTRIM(x856.segment) = "N2"
							m.storealias = ALLTRIM(x856.f1)
						CASE ALLTRIM(x856.segment) = "N3"
							m.addr1 = ALLTRIM(x856.f1)
							IF LEN(ALLTRIM(x856.f2)) >=1 THEN
								m.addr2 = ALLTRIM(x856.f2)
							ENDIF
						CASE ALLTRIM(x856.segment) = "N4"
							m.city = ALLTRIM(x856.f1)
							m.state = ALLTRIM(x856.f2)
							m.zip = ALLTRIM(x856.f3)
						CASE ALLTRIM(x856.segment) = "DTM"
							DO CASE
								CASE ALLTRIM(x856.f1) = "007"
									m.qdatetype = "E"  && Effective date
								CASE ALLTRIM(x856.f1) = "145"
									m.qdatetype = "O"  && Opening date
								CASE ALLTRIM(x856.f1) = "146"
									m.qdatetype = "C"  && Closing date
								OTHERWISE
									* NOTHING
							ENDCASE
							lcDate = ALLTRIM(x856.f2)
							m.qualdate = .ConvertYYYYMMDDStringToDate( lcDate )
						CASE ALLTRIM(x856.segment) = "ASI" AND ALLTRIM(x856.f1) = "2"
							m.maintcode = PADL(ALLTRIM(x856.f2),3,'0')
						OTHERWISE
							* nothing
					ENDCASE
				ENDIF && INLIST(ALLTRIM(x856.segment),"N1","N2","N3","N4","DTM","ASI")
				SELECT x856
				SKIP
			ENDDO
			* WRITE TO STORE TABLE FOR LAST SEG DATA
			SELECT stores
			APPEND BLANK
			GATHER MEMVAR
			.nOutRecs = .nOutRecs + 1
		ENDWITH
	ENDPROC  &&  parse816out


	PROCEDURE parse860out
		WITH THIS
			LOCAL lcMessage

			.clear_variables( "ALL" )

			SELECT x856
			SKIP

			DO WHILE ALLTRIM(x856.segment) != "SE"
				m.editype="860"

				IF ALLTRIM(x856.segment) = "BCH"  && new PO
					m.potype=ALLTRIM(f2)
					m.pcode=ALLTRIM(f1)
					lcDate = ALLTRIM(x856.f6)
					*lxDate = CTOD(SUBSTR(lcDate,5,2)+"/"+SUBSTR(lcDate,7,2)+"/"+LEFT(lcDate,4))
					*m.podate = lxDate
					m.podate = .ConvertYYYYMMDDStringToDate( lcDate )


					lcDate = ALLTRIM(x856.f11)
					*lxDate = CTOD(SUBSTR(lcDate,5,2)+"/"+SUBSTR(lcDate,7,2)+"/"+LEFT(lcDate,4))
					*m.reqdate = lxDate
					m.reqdate = .ConvertYYYYMMDDStringToDate( lcDate )
					DO CASE
						CASE m.pcode="01"
							m.purpose="CANCEL"
						CASE m.pcode="04"
							m.purpose="CHANGE"
					ENDCASE
					m.ponum = ALLTRIM(f3)
				ENDIF
				IF ALLTRIM(x856.segment) = "REF" AND ALLTRIM(f1)="DP"
					m.dept = ALLTRIM(f2)
				ENDIF
				IF ALLTRIM(x856.segment) = "REF" AND ALLTRIM(f1)="IA"
					m.vendorid = ALLTRIM(f2)
				ENDIF
				IF ALLTRIM(x856.segment) = "REF" AND ALLTRIM(f1)="PD"
					m.promonum = ALLTRIM(f2)
*!*						IF EMPTY(m.promonum) THEN
*!*							m.promonum = SPACE(2)
*!*						ELSE
*!*							m.promonum = LEFT(m.promonum,2)  && Doug says this Promo Code is only 2 digits
*!*						ENDIF
				ENDIF
				IF ALLTRIM(x856.segment) = "DTM"
					lcDate = ALLTRIM(x856.f2)
					*lxDate = CTOD(SUBSTR(lcDate,5,2)+"/"+SUBSTR(lcDate,7,2)+"/"+LEFT(lcDate,4))
					*m.newshipdt = lxDate
					m.newshipdt = .ConvertYYYYMMDDStringToDate( lcDate )
				ENDIF


				*********************************************************************
				* corrected this section, per Doug & Renay 1/25/11 MB
				*!*	IF ALLTRIM(x856.segment) = "N1" AND ALLTRIM(f1)="BY"
				*!*		m.shipstore = ALLTRIM(f4)
				*!*	ENDIF
				*!*	IF ALLTRIM(x856.segment) = "N1" AND ALLTRIM(f1)="ST"
				*!*		m.shipdc = ALLTRIM(f4)
				*!*	ENDIF
				*!*	IF ALLTRIM(x856.segment) = "N1" AND ALLTRIM(f1)="Z7"
				*!*		m.billto = ALLTRIM(f4)
				*!*	ENDIF

				IF ALLTRIM(x856.segment) = "N1" AND ALLTRIM(f1)="BY"
					m.billto = ALLTRIM(f4)
					* PO TYPE IN/OUT BASED ON THIS LOGIC PER DOUG 1/27/2011
					IF m.billto = "8110" THEN
						m.direction = "IN"
					ELSE
						m.direction = "OUT"
					ENDIF
				ENDIF
				IF ALLTRIM(x856.segment) = "N1" AND ALLTRIM(f1)="ST"
					m.shipdc = ALLTRIM(f4)
				ENDIF
				IF ALLTRIM(x856.segment) = "N1" AND ALLTRIM(f1)="Z7"
					m.shipstore = ALLTRIM(f4)
				ENDIF
				*********************************************************************


				IF ALLTRIM(x856.segment) = "SAC"
					m.sac = ALLTRIM(f15)
				ENDIF

				IF ALLTRIM(x856.segment) = "POC"
					DO WHILE ALLTRIM(x856.segment) = "POC"
						.clear_variables( "DETAIL" )
						m.dateloaded = DATETIME()
						m.sent = .F.
						m.chngcode = ALLTRIM(x856.f2)
						DO CASE
							CASE m.chngcode = "AI"
								m.chngdesc="ADDITEM"
							CASE m.chngcode = "CA"
								m.chngdesc="CHANGEITEM"
							CASE m.chngcode = "CT"
								m.chngdesc="CHANGEDATE"
							CASE m.chngcode = "DI"
								m.chngdesc="DELETEITEM"
							CASE m.chngcode = "PC"
								m.chngdesc="PRICECHANG"
							CASE m.chngcode = "QD"
								m.chngdesc="DECQTY"
							CASE m.chngcode = "QI"
								m.chngdesc="INCQTY"
						ENDCASE
						m.qty     = VAL(ALLTRIM(x856.f3))
						m.chngqty = VAL(ALLTRIM(x856.f4))

						DO CASE
							CASE ALLTRIM(f8)="IN"
								m.style= PADL(ALLTRIM(x856.f9),5,'0')
							CASE ALLTRIM(f8)="IZ"
								m.sku= ALLTRIM(x856.f9)
							CASE ALLTRIM(f8)="UP"
								m.upc  = ALLTRIM(x856.f9)
						ENDCASE

						DO CASE
							CASE ALLTRIM(f10)="IN"
								m.style= PADL(ALLTRIM(x856.f11),5,'0')
							CASE ALLTRIM(f10)="IZ"
								m.sku= ALLTRIM(x856.f11)
							CASE ALLTRIM(f10)="UP"
								m.upc  = ALLTRIM(x856.f11)
						ENDCASE

						DO CASE
							CASE ALLTRIM(f12)="IN"
								m.style= PADL(ALLTRIM(x856.f13),5,'0')
							CASE ALLTRIM(f12)="IZ"
								m.sku= ALLTRIM(x856.f13)
							CASE ALLTRIM(f12)="UP"
								m.upc  = ALLTRIM(x856.f13)
						ENDCASE

						IF m.chngcode = "CT"
							SELECT x856
							SKIP 1
							IF ALLTRIM(x856.segment)!= "DTM"
								*MESSAGEBOX("860 upload sequence error, no DTM after a line item date change",16,"Sears EDI Import/Formatter",1500)
								*RETURN
								lcMessage = "860 upload sequence error, no DTM after a line item date change"
								.cThrowDescription = lcMessage
								.TrackProgress(lcMessage,LOGIT+SENDIT+NOWAITIT)
								THROW
							ENDIF
							lcDate = ALLTRIM(x856.f2)
							*lxDate = CTOD(SUBSTR(lcDate,5,2)+"/"+SUBSTR(lcDate,7,2)+"/"+LEFT(lcDate,4))
							*m.newshipdt = lxDate
							m.newshipdt = .ConvertYYYYMMDDStringToDate( lcDate )
						ENDIF
						SELECT outpo
						APPEND BLANK
						GATHER MEMVAR
						.nOutRecs = .nOutRecs + 1
						SELECT x856
						SKIP 1
					ENDDO
				ENDIF

				IF ALLTRIM(x856.segment) = "CTT" AND ALLTRIM(f1)="0"
					m.dateloaded = DATETIME()
					SELECT outpo
					APPEND BLANK
					GATHER MEMVAR
					.nOutRecs = .nOutRecs + 1
				ENDIF
				SELECT x856
				SKIP
			ENDDO
		ENDWITH
	ENDPROC  && parse860out


	PROCEDURE clear_variables
		LPARAMETERS whichtype
		WITH THIS

			IF INLIST(whichtype,"ALL","HEADER")
				m.dateloaded = DATETIME()
				m.sent = .F.
				m.ponum = ""
				m.chngcode=""
				m.chngdesc=""
				m.purpose =""
				m.pcode=""
				m.dept= ""
				m.vendorid =""
				m.shipdc=""
				m.shipstore=""
				m.billto=""
				m.sac=""
				m.promonum = SPACE(2)
				m.potype = ""
				m.direction = ""
			ENDIF

			IF INLIST(whichtype,"ALL","DETAIL")
				m.chngqty=0
				m.style=""
				m.sku=""
				m.upc=""
				m.qty=0
			ENDIF

			IF INLIST(whichtype,"816")
				m.storename=""
				m.storenum =""
				m.storealias=""
				m.addr1=""
				m.addr2=""
				m.state=""
				m.zip=""
				m.qualdate = CTOD('')
				m.maintcode = ""
			ENDIF
		ENDWITH
	ENDPROC  && clear_variables


	PROCEDURE TrackProgress
		* do any combination of Wait Window, writing to logfile, and adding to body of email,
		* based on nFlags parameter.
		LPARAMETERS tcExpression, tnFlags
		WITH THIS
			IF BITAND(tnFlags,LOGIT) = LOGIT THEN
				IF .lLoggingIsOn THEN
					?
					? .cProcessName + "  " + TTOC(DATETIME()) + ": " + tcExpression
				ENDIF
			ENDIF
			IF .lWaitWindowIsOn THEN
				IF BITAND(tnFlags,NOWAITIT) = NOWAITIT THEN
					WAIT WINDOW tcExpression NOWAIT
				ENDIF
				IF BITAND(tnFlags,WAITIT) = WAITIT THEN
					WAIT WINDOW tcExpression TIMEOUT .nWaitWindowTimeout
				ENDIF
			ENDIF
			IF BITAND(tnFlags,SENDIT) = SENDIT THEN
				IF .lSendInternalEmailIsOn THEN
					.cBodyText = .cBodyText + tcExpression + CRLF + CRLF
				ENDIF
			ENDIF
		ENDWITH
	ENDPROC  &&  TrackProgress


	PROCEDURE SortArrayByDateTime
		* expects taInArray[] to be the result of an ADIR()
		LPARAMETERS taInArray, taOutArray
		EXTERNAL ARRAY taInArray, taOutArray
		LOCAL lnRows, lnCols, i, j
		* extending taInArray[] to process the files in date/time order
		lnRows = ALEN(taInArray,1)
		lnCols = ALEN(taInArray,2)
		* create taOutArray with one more column
		DIMENSION taOutArray[lnRows,lnCols + 1]
		* and fill it with taInArray columns AND (filedate + filetime) in new column
		* ( ACOPY() doesn't preserve elements properly because of different # of columns )
		FOR i = 1 TO lnRows
			FOR j = 1 TO lnCols
				taOutArray[i,j] = taInArray[i,j]
			ENDFOR
			taOutArray[i,lnCols + 1] = DTOS(taInArray[i,3]) + "_" + taInArray[i,4]
		ENDFOR
		* sort taOutArray on date/time column
		=ASORT(taOutArray,6)
	ENDPROC


ENDDEFINE
