PARAMETERS cFilename

lDoColor = .T.
lLX = .F.
lPick = .T.
lPrepack = .F.
lSolidPack = .F.
lDoPKG = .F.

SELECT x856
SET FILTER TO

LOCATE
nSegCnt = 0
COUNT FOR TRIM(segment) = "W05" TO nSegCnt
LOCATE

cSegCnt = ALLTRIM(STR(nSegCnt))
WAIT "There are "+cSegCnt+" P/Ts in this file" WINDOW TIMEOUT 2
nPTqty = nSegCnt
nTotPT = 1
STORE 0 TO pt_total_cartons,nINSegnum,nCtnCount,ptctr,nTotWt,nTotCube,m.PTDETID,lnEaches,nRepQty,lPackQty
STORE "" TO cStoreNum,cBillTo,m.isa_num,m.printstuff


*!*	IF !USED("uploaddet")
*!*		IF lTesting OR lTestUploaddet
*!*			USE F:\3pl\test\uploaddet IN 0 ALIAS uploaddet
*!*		ELSE
*!*			USE F:\ediwhse\DATA\uploaddet IN 0 SHARED ALIAS uploaddet
*!*		ENDIF
*!*	ENDIF

SELECT xpt
DELETE ALL
SELECT xptdet
DELETE ALL

SELECT x856
LOCATE
IF lTesting
*	BROWSE
ENDIF

DO WHILE !EOF("x856")
	cSegment = TRIM(x856.segment)
	cF1 = TRIM(x856.f1)
*	ASSERT .f. MESSAGE "At Segment Loop in x856...DEBUG"

	DO CASE
		CASE TRIM(x856.segment) = "ISA"
			cISA_Num = ALLTRIM(x856.f13)
			SELECT x856
			IF !EOF()
				SKIP 1 IN x856
				LOOP
			ENDIF

		CASE INLIST(TRIM(x856.segment),"GS","ST")
			SELECT x856
			IF !EOF()
				SKIP 1 IN x856
				LOOP
			ENDIF

		CASE TRIM(x856.segment) = "W05"  && new shipment
			STORE "" TO cShip_ref,cCNEE_ref
			lAsst = .F.
			nSubline = 0
			SELECT xpt
			APPEND BLANK
			STORE 0 TO pt_total_cartons
			nXPTQty = 0
			ptctr = ptctr +1
			m.office = IIF(cOffice="I","N",cOffice)
			m.mod = cMod
			REPLACE xpt.office WITH m.office IN xpt
			REPLACE xpt.MOD WITH m.mod IN xpt
			REPLACE xpt.shipins WITH "FILENAME*"+cFilename IN xpt
			REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"ISANUM*"+cISA_Num+CHR(13)+"FILE940*"+JUSTFNAME(xfile) IN xpt
			REPLACE xpt.addby     WITH "FMI PROC" IN xpt
			REPLACE xpt.adddt     WITH DATETIME() IN xpt
			REPLACE xpt.addproc     WITH "LIFE940EDI" IN xpt
			REPLACE xpt.ptid      WITH ptctr IN xpt
			REPLACE xpt.ptdate    WITH DATE() IN xpt
			REPLACE xpt.accountid WITH nAcctNum IN xpt
			cShip_ref = ALLTRIM(x856.f2)
			m.ship_ref = ALLTRIM(cShip_ref)
			REPLACE xpt.ship_ref WITH cShip_ref IN xpt && Pickticket
			cCNEE_ref = ""
			cCNEE_ref = UPPER(ALLTRIM(x856.f3))
			REPLACE xpt.cnee_ref WITH cCNEE_ref IN xpt  && Customer PO
			WAIT CLEAR
			waitstr = "Now processing PT # "+cShip_ref+CHR(13)+"FILE: "+cFilename
			WAIT WINDOW AT 10,10  waitstr NOWAIT

		CASE (TRIM(x856.segment) = "N1" AND TRIM(x856.f1) = "BT")  &&Bill-to info
			REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"BILLTONAME*"+ALLTRIM(x856.f2) IN xpt
			SKIP 1 IN x856
			IF TRIM(x856.segment) = "N3"
				REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"BILLTOADDR*"+ALLTRIM(x856.f1) IN xpt
			ELSE
				LOOP
			ENDIF
			SKIP 1 IN x856
			IF TRIM(x856.segment) = "N4"
				REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"BILLTOCSZ*"+;
					ALLTRIM(x856.f1)+", "+ALLTRIM(x856.f2)+" "+ALLTRIM(x856.f3)+" "+ALLTRIM(x856.f4) IN xpt
			ENDIF


		CASE TRIM(x856.segment) = "N1" AND INLIST(TRIM(x856.f1),"ST","CN")
			cStoreNum = UPPER(ALLTRIM(x856.f4))
			cConsignee = UPPER(ALLT(x856.f2))
			cDCNum = ""
			REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"STORENUM*"+cStoreNum IN xpt  && DC or Store Number
			REPLACE xpt.dcnum WITH cStoreNum IN xpt
			REPLACE xpt.storenum WITH INT(VAL(RIGHT(ALLT(xpt.dcnum),5))) IN xpt

			cSFStoreNum = UPPER(ALLTRIM(x856.f4))
			REPLACE xpt.sforstore WITH cStoreNum IN xpt

			lBBB = IIF(("BED"$UPPER(cConsignee) AND "BATH"$UPPER(cConsignee)) OR UPPER(cConsignee)="BBB",.T.,.F.)
			REPLACE xpt.consignee WITH cConsignee IN xpt NEXT 1
			REPLACE xpt.NAME WITH xpt.consignee IN xpt NEXT 1

			SKIP 1 IN x856
			DO CASE
				CASE TRIM(x856.segment) = "N2"  && address info
					IF !lTesting
						cStoreNum = UPPER(ALLTRIM(x856.f1))
						cStoreName = UPPER(ALLTRIM(x856.f2))
						IF !"FAC*"$xpt.shipins AND !EMPTY(cStoreNum)
							REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"FAC*"+cStoreNum IN xpt
						ENDIF
*					REPLACE xpt.dcnum WITH cStoreNum IN xpt
					ENDIF

					SKIP 1 IN x856
					IF TRIM(x856.segment) = "N3"  && address info
						REPLACE xpt.address WITH UPPER(TRIM(x856.f1)) IN xpt
						REPLACE xpt.address2  WITH UPPER(TRIM(x856.f2)) IN xpt
					ELSE
						cMessage = "Missing Ship-To Address Info"
						WAIT WINDOW cMessage TIMEOUT 2
						NormalExit = .F.
						THROW
					ENDIF

				CASE TRIM(x856.segment) = "N3"  && address info
					REPLACE xpt.address WITH UPPER(TRIM(x856.f1)) IN xpt
					REPLACE xpt.address2  WITH UPPER(TRIM(x856.f2)) IN xpt
				OTHERWISE
					cMessage = "Missing Ship-To Address Info"
					WAIT WINDOW cMessage TIMEOUT 2
					NormalExit = .F.
					THROW
			ENDCASE

			SKIP 1 IN x856
			IF TRIM(x856.segment) = "N4"  && address info
				m.country = TRIM(x856.f4)
*!*				IF EMPTY(TRIM(x856.f2))
*!*				cCSZ = TRIM(x856.f1)+", "+TRIM(x856.f4)+" "+TRIM(x856.f3)
*!*				else
				cCSZ = TRIM(x856.f1)+", "+PADR(TRIM(x856.f2),2)+" "+TRIM(x856.f3)
*!*				endif
				REPLACE xpt.csz WITH UPPER(cCSZ) IN xpt
				IF !EMPTY(ALLTRIM(x856.f4))
					REPLACE xpt.country WITH ALLTRIM(x856.f4) IN xpt
				ENDIF
			ELSE
				cMessage = "Missing Ship-To CSZ Info"
				WAIT WINDOW cMessage TIMEOUT 2
				NormalExit = .F.
				THROW
			ENDIF

		CASE (TRIM(x856.segment) = "N1" AND TRIM(x856.f1) = "Z7")
			STORE UPPER(TRIM(x856.f2)) TO cMarkfor && will be overwritten if there is an N2 loop
			REPLACE xpt.shipfor WITH cMarkfor IN xpt
			cSFStoreNum = UPPER(ALLTRIM(x856.f4))
			REPLACE xpt.sforstore WITH cSFStoreNum IN xpt
			REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"SFSTORENUM*"+cSFStoreNum IN xpt  && DC or Store Number
			SKIP 1 IN x856
			DO CASE
				CASE TRIM(x856.segment) = "N2"  && address info
					STORE UPPER(TRIM(x856.f2)) TO cMarkfor
					REPLACE xpt.shipfor WITH cMarkfor IN xpt
					REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"SFSTORELONG*"+UPPER(ALLTRIM(x856.f1)) IN xpt
					REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"STORENAME*"+ALLTRIM(x856.f2) IN xpt
					REPLACE xpt.sforstore WITH ALLTRIM(x856.f1) IN xpt
					SKIP 1 IN x856
					IF TRIM(x856.segment) = "N3"  && address info
						REPLACE xpt.sforaddr1 WITH UPPER(TRIM(x856.f1)) IN xpt
						REPLACE xpt.sforaddr2  WITH UPPER(TRIM(x856.f2)) IN xpt
					ELSE
						cMessage = "Missing Ship-For Address Info (N2>N3), PT "+cShip_ref
						WAIT WINDOW cMessage TIMEOUT 2
						SET STEP ON
						NormalExit = .F.
						THROW
					ENDIF
				CASE TRIM(x856.segment) = "N3"  && address info
					REPLACE xpt.sforaddr1 WITH UPPER(TRIM(x856.f1)) IN xpt
					REPLACE xpt.sforaddr2  WITH UPPER(TRIM(x856.f2)) IN xpt
				OTHERWISE
					cMessage = "Missing Ship-For Address Info (N1>N2), PT "+cShip_ref
					WAIT WINDOW cMessage TIMEOUT 2
					SET STEP ON
					NormalExit = .F.
					THROW
			ENDCASE
			SKIP 1 IN x856
			IF TRIM(x856.segment) = "N4"  && address info
*!*				IF EMPTY(TRIM(x856.f2))
*!*				csforCSZ = TRIM(x856.f1)+", "+TRIM(x856.f4)+" "+TRIM(x856.f3)
*!*				else
				csforCSZ = TRIM(x856.f1)+", "+PADR(TRIM(x856.f2),2)+" "+TRIM(x856.f3)
*!*				endif
				REPLACE xpt.sforcsz WITH UPPER(csforCSZ) IN xpt
			ELSE
				cMessage = "Missing Ship-For CSZ Info"
				WAIT WINDOW cMessage TIMEOUT 2
				NormalExit = .F.
				THROW
			ENDIF

		CASE TRIM(x856.segment) = "N9" AND TRIM(x856.f1) = "DP" && Dept Num.
			cDept = TRIM(x856.f2)
			REPLACE xpt.dept WITH cDept IN xpt

		CASE TRIM(x856.segment) = "N9" AND TRIM(x856.f1) = "DC" && Shipto DC#
			IF !lBBB
				cDCNum = TRIM(x856.f2)
				REPLACE xpt.dcnum WITH cDCNum IN xpt
			ENDIF

		CASE TRIM(x856.segment) = "N9" AND TRIM(x856.f1) = "IA" && Vendor Num.
			REPLACE xpt.vendor_num WITH ALLTRIM(x856.f2) IN xpt
			cDUNS =  ALLTRIM(x856.f3)
			IF !EMPTY(cDUNS)
				REPLACE xpt.DUNS WITH cDUNS IN xpt
			ENDIF

		CASE TRIM(x856.segment) = "N9" AND TRIM(x856.f1) = "01" && Not sure what this is
			cRouting = UPPER(ALLTRIM(x856.f2))
			REPLACE xpt.ship_via WITH cRouting IN xpt
			REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"ROUTING*"+cRouting IN xpt  && SPS Routing Key

		CASE TRIM(x856.segment) = "N9" AND TRIM(x856.f1) = "02" && Not sure what this is
			cCustCode = UPPER(ALLTRIM(x856.f2))
			REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"CUSTCODE*"+cCustCode IN xpt  && SPS Routing Key

		CASE TRIM(x856.segment) = "N9" AND TRIM(x856.f1) = "03" && Not sure what this is
			cRepCode = UPPER(ALLTRIM(x856.f2))
			REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"REPCODE*"+cRepCode IN xpt  && SPS Routing Key

		CASE TRIM(x856.segment) = "N9" AND TRIM(x856.f1) = "04"
			cTerms = UPPER(ALLTRIM(x856.f2))
			REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"TERMS*"+cTerms IN xpt  && Terms
			REPLACE xpt.terms WITH cTerms IN xpt  && Terms

		CASE TRIM(x856.segment) = "N9" AND TRIM(x856.f1) = "05"
			cFOB = UPPER(ALLTRIM(x856.f2))
			REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"FOB*"+cFOB IN xpt  && Family of business

		CASE TRIM(x856.segment) = "N9" AND TRIM(x856.f1) = "06" && Email contact, needs to be returned on the 945
			cEmail = ALLTRIM(x856.f2)
			REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"EMAIL*"+cEmail IN xpt  && SPS Routing Key

		CASE TRIM(x856.segment) = "N9" AND TRIM(x856.f1) = "20	" && Return on 945
			cCacctnum = UPPER(ALLTRIM(x856.f2)) && Shipping Acct #
			REPLACE xpt.cacctnum WITH cCacctnum IN xpt
			REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"SHIPACCT*"+cCacctnum IN xpt

		CASE TRIM(x856.segment) = "N9" AND TRIM(x856.f1) = "23" && Return on 945
			cN923 = ALLTRIM(x856.f2)
			REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"N923*"+cN923 IN xpt  && SPS Routing Key

		CASE TRIM(x856.segment) = "N9" AND TRIM(x856.f1) = "DC" &&   Target Facility Code
			cTargetCode = ALLTRIM(x856.f2)
			REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"TARGETCODE*"+ALLTRIM(cTargetCode)  IN xpt  && SPS Routing Key

		CASE TRIM(x856.segment) = "G62" AND TRIM(x856.f1) == "37" && Start Date
			cXdate1 = TRIM(x856.f2)
			datestrconversion()
			REPLACE xpt.START WITH dxdate2 IN xpt

		CASE TRIM(x856.segment) = "G62" AND TRIM(x856.f1) == "38" && Cancel Date
			cXdate1 = TRIM(x856.f2)
			datestrconversion()
			REPLACE xpt.CANCEL WITH dxdate2 IN xpt

		CASE TRIM(x856.segment) = "W66"  && Shipping info
			REPLACE xpt.ship_via WITH ALLTRIM(x856.f5) IN xpt
			REPLACE xpt.scac WITH ALLTRIM(x856.f10) IN xpt
			REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"CARRMODE*"+ALLTRIM(x856.f2) IN xpt

*********************************************
*!* XPTDET Update Section
*********************************************
			nTotQty = 0

		CASE TRIM(x856.segment) = "LX" && start of PT detail, stay here until this PT is complete
			nTotQty = 0
			lLX = .T.

			DO WHILE TRIM(x856.segment) != "SE"
				m.printstuff = ""
				cxSegment = TRIM(x856.segment)
				cxField01 = TRIM(x856.f1)

				IF TRIM(x856.segment) = "LX"
					SKIP 1 IN x856
					LOOP
				ENDIF

				IF TRIM(x856.segment) = "W01"
					m.printstuff = ""
					cOrigStyle = ""

					lcItemType = ALLTRIM(x856.f2)  && either "CA" or "EA"
					m.printstuff = "ITEMTYPE*"+TRIM(lcItemType)

					IF x856.f4 = "IN"
						m.style = ALLTRIM(x856.f5)
						cOrigStyle = ALLT(x856.f5)
						m.upc = LEFT(m.style,12) && init the UPC here incase of no UPC sent
					ENDIF

					IF x856.f15 = "VN"
						m.custsku = ALLTRIM(x856.f16)
					ENDIF

					IF x856.f6 = "UP"
						m.upc = LEFT(ALLTRIM(x856.f7),12)
						m.printstuff = m.printstuff+CHR(13)+"UPC*"+TRIM(m.upc)
					ENDIF

					IF lcItemType = "EA" AND !EMPTY(m.upc)  && added this ItemType stuff 11/06/2015  PG
&& do nothing keep the UPC as is
					ELSE
						m.upc = m.style
					ENDIF

					m.office = IIF(cOffice="I","N",cOffice)
					m.mod = cMod
					m.pack ="1"
					m.units =.T.
					m.printstuff = m.printstuff+CHR(13)+"ORIGSTYLE*"+cOrigStyle
					m.totqty = INT(VAL(x856.f1))
					m.origqty = m.totqty
					nTotQty = nTotQty + m.totqty
					cUnitCode = IIF(EMPTY(x856.f2),"EA",TRIM(x856.f2))
					m.printstuff = m.printstuff+CHR(13)+"UNITCODE*"+cUnitCode

					SELECT xptdet
					APPEND BLANK
					m.PTDETID=m.PTDETID+1
					GATHER MEMVAR MEMO
					REPLACE xptdet.addby WITH "FMI PROC" IN xptdet
					REPLACE xptdet.adddt WITH DATETIME() IN xptdet
					REPLACE xptdet.addproc WITH "LIFE940EDI" IN xptdet
					REPLACE xptdet.ptid      WITH xpt.ptid IN xptdet
					REPLACE xptdet.accountid WITH xpt.accountid IN xptdet
					nPack = 0
					nXPTQty = nXPTQty + m.totqty
					lnEaches = 0
				ENDIF
				SELECT x856

				IF TRIM(x856.segment) = "N9" AND TRIM(x856.f1) = "DES"
					REPLACE xptdet.COLOR WITH ALLTRIM(UPPER(x856.f2)) IN xptdet
				ENDIF

				IF TRIM(x856.segment) = "N9" AND TRIM(x856.f1) = "VCL"
					REPLACE xptdet.ID WITH ALLTRIM(UPPER(x856.f2)) IN xptdet
				ENDIF

				IF TRIM(x856.segment) = "G69"
					IF EMPTY(xptdet.printstuff)
						REPLACE xptdet.printstuff WITH "DESC*"+TRIM(x856.f1) IN xptdet
					ELSE
						REPLACE xptdet.printstuff WITH xptdet.printstuff+CHR(13)+"DESC*"+TRIM(x856.f1) IN xptdet
					ENDIF
				ENDIF

				IF TRIM(x856.segment) = "QTY" AND TRIM(x856.f1) = "63"
					SET STEP ON 
					REPLACE xptdet.printstuff WITH xptdet.printstuff+CHR(13)+"OUTERPACK*"+TRIM(x856.f2) IN xptdet
				ENDIF

				SELECT x856
				IF !EOF()
					SKIP 1 IN x856
				ENDIF
			ENDDO  && SEGMENT NOW = 'SE'
	ENDCASE
	IF !EOF()
		SKIP 1 IN x856
	ENDIF
ENDDO

SELECT xpt
REPLACE xpt.qty WITH nTotQty IN xpt
REPLACE xpt.origqty WITH xpt.qty IN xpt
nTotQty = 0

DO m:\dev\prg\setuppercase940

WAIT CLEAR
IF lBrowfiles
	WAIT WINDOW "Now displaying XPT/XPTDET cursors..."+CHR(13)+"CANCEL at XPTDET if desired." TIMEOUT 2
	SELECT xpt
	SET ORDER TO
	LOCATE
	BROWSE
	SELECT xptdet
	SET ORDER TO
	LOCATE
	BROWSE
	IF MESSAGEBOX("Do you wish to continue?",4+32+256,"Browse Files Box")<>6
		CANCEL
	ENDIF
ENDIF

WAIT CLEAR
WAIT cMailName+" BKDN process complete for file "+xfile WINDOW TIMEOUT 2
lLX = .F.
lnEaches = 0
lPick = .F.
nRepQty = 0
m.isa_num = ""
SELECT x856
cTotPT = ALLTRIM(STR(nTotPT))
lNewPT = .T.
RETURN

******************************
PROCEDURE datestrconversion
******************************
	cXdate1 = TRIM(cXdate1)
	dxdate2 = CTOD(SUBSTR(cXdate1,5,2)+"/"+RIGHT(cXdate1,2)+"/"+LEFT(cXdate1,4))
ENDPROC

