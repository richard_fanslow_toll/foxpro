**This script identifies when a 945 has not been created for a PT.  

utilsetup("BILLABONG_MISSING_945")
Set Exclusive Off
Set Deleted On
Set Talk Off
Set Safety Off 

WITH _SCREEN	
	.AUTOCENTER = .T.
	.WINDOWSTATE = 0
	.BORDERSTYLE = 1
	.WIDTH = 320
	.HEIGHT = 210
	.TOP = 200
	.LEFT = 100
	.CLOSABLE = .T.
	.MAXBUTTON = .T.
	.MINBUTTON = .T.
	.SCROLLBARS = 0
	.CAPTION = "BILLABONG_MISSING_945"
ENDWITH	

goffice="L"

If !Used("edi_trigger")
  use f:\3pl\data\edi_trigger shared In 0
ENDIF

xsqlexec("select accountid, wo_num, ship_ref from outship where mod='"+goffice+"' " + ;
	"and accountid in (6718,6719,6722,6744,6745,6746,6747,6748,6749) and delenterdt>{"+dtoc(date()-20)+"} and delenterdt<{"+ttoc(datetime()-1290)+"}","temp945",,"wh")

*xsqlexec("select * from edi_trigger where accountid=6665 and edi_type='945'","t1",,"ar")

SELECT * FROM edi_trigger where INLIST(accountid,6718,6719,6722,6744,6745,6746,6747,6748,6749) AND edi_type='945' INTO CURSOR t1 READWRITE

SELECT * FROM temp945 a LEFT JOIN t1 b ON a.accountid=b.accountid AND a.wo_num=b.wo_num AND a.ship_ref=b.ship_ref INTO CURSOR c1 READWRITE
SELECT * FROM c1 where ISNULL(fin_status) INTO CURSOR c2 READWRITE 
SELECT c2
If Reccount() > 0
  Export To "S:\Billabong\temp\billabong_945_no_trigger_record"  Type Xls
  tsendto = "tmarg@fmiint.com,todd.margolin@tollgroup.com,paul.gaidis@tollgroup.com,joe.bianchi@tollgroup.com"
  tattach = "S:\Billabong\temp\billabong_945_no_trigger_record.xls"
  tcc =""
  tFrom ="Toll/FMI EDI System Operations <fmi-transload-ops@fmiint.com>"
  tmessage = "Billabong 945 missing trigger record"+Ttoc(Datetime())
  tSubject = "Billabong 945 missing trigger record, CHECK POLLER"
  Do Form m:\dev\frm\dartmail2 With tsendto,tFrom,tSubject, tcc,tattach,tmessage  ,"A"
ELSE 
  tsendto = "tmarg@fmiint.com"
  tattach = ""
  tcc =""
  tFrom ="Toll/FMI EDI System Operations <fmi-transload-ops@fmiint.com>"
  tmessage = "NO_Billabong 945 missing trigger record exist"+Ttoc(Datetime())
  tSubject = "NO_Billabong 945 missing trigger record exist"
  Do Form m:\dev\frm\dartmail2 With tsendto,tFrom,tSubject, tcc,tattach,tmessage  ,"A"
ENDIF


SELECT DISTINCT ACCOUNTID,WO_NUM,SHIP_REF FROM t1 WHERE EDI_TYPE='945 '  AND INLIST(accountid,6718,6719,6722,6744,6745,6746,6747,6748,6749);
 AND !errorflag AND (empty(fin_status) OR EMPTY(when_proc)) AND fin_status !='NO 945' INTO CURSOR TEMP21
SELECT  TEMP21
If Reccount() > 0
  Export To "S:\billabong\temp\bbg_945_trigger_created_not_processed"  Type Xls

  tsendto = "todd.margolin@tollgroup.com,paul.gaidis@tollgroup.com,joe.bianchi@tollgroup.com"
  tattach = "S:\billabong\temp\bbg_945_trigger_created_not_processed.xls"
  tcc =""
  tFrom ="Toll/FMI EDI System Operations <fmi-transload-ops@fmiint.com>"
  tmessage = "billabong_945_trigger_created_not_processed"+Ttoc(Datetime())
  tSubject = "billabong_945_trigger_created_not_processed"
  Do Form m:\dev\frm\dartmail2 With tsendto,tFrom,tSubject, tcc,tattach,tmessage  ,"A"
ELSE 
  tsendto = "tmarg@fmiint.com"
  tattach = ""
  tcc =""
  tFrom ="Toll/FMI EDI System Operations <fmi-transload-ops@fmiint.com>"
  tmessage = "NO_unprocessed_945s_for_billabong"+Ttoc(Datetime())
  tSubject = "NO_unprocessed_945s_for_billabong"
  Do Form m:\dev\frm\dartmail2 With tsendto,tFrom,tSubject, tcc,tattach,tmessage  ,"A"
ENDIF

******* to identify a processed 945 but missing 945 file from file945
SELECT DISTINCT ACCOUNTID,WO_NUM,SHIP_REF FROM t1 WHERE EDI_TYPE='945 '  AND INLIST(accountid,6718,6719,6722,6744,6745,6746,6747,6748,6749);
 AND !errorflag AND EMPTY(file945) AND fin_status !='NO 945' INTO CURSOR TEMP21
SELECT  TEMP21
If Reccount() > 0
  Export To "S:\billabong\temp\bbg_945_trigger_no_file_created"  Type Xls

  tsendto = "todd.margolin@tollgroup.com,paul.gaidis@tollgroup.com,joe.bianchi@tollgroup.com"
  tattach = "S:\billabong\temp\bbg_945_trigger_no_file_created.xls"
  tcc =""
  tFrom ="Toll/FMI EDI System Operations <fmi-transload-ops@fmiint.com>"
  tmessage = "billabong_945_trigger_created_no file created"+Ttoc(Datetime())
  tSubject = "billabong_945_trigger_created_no file created"
  Do Form m:\dev\frm\dartmail2 With tsendto,tFrom,tSubject, tcc,tattach,tmessage  ,"A"
ELSE 
  tsendto = "tmarg@fmiint.com"
  tattach = ""
  tcc =""
  tFrom ="Toll/FMI EDI System Operations <fmi-transload-ops@fmiint.com>"
  tmessage = "NO_billabong_945_trigger_created_no file created"+Ttoc(Datetime())
  tSubject = "NO_billabong_945_trigger_created_no file created"
  Do Form m:\dev\frm\dartmail2 With tsendto,tFrom,tSubject, tcc,tattach,tmessage  ,"A"
ENDIF



SELECT * FROM t1 WHERE EDI_TYPE='945 '  AND INLIST(accountid,6718,6719,6722,6744,6745,6746,6747,6748,6749) AND errorflag INTO CURSOR TEMP21

SELECT  TEMP21
If Reccount() > 0
  Export To "S:\billabong\temp\billabong_trigger_error"  Type Xls

  tsendto = "tmarg@fmiint.com,todd.margolin@tollgroup.com,paul.gaidis@tollgroup.com,joe.bianchi@tollgroup.com"
  tattach = "S:\billabong\temp\billabong_trigger_error.xls"
  tcc =""
  tFrom ="Toll/FMI EDI System Operations <fmi-transload-ops@fmiint.com>"
  tmessage = "billabong_945_trigger_error"+Ttoc(Datetime())
  tSubject = "billabong_945_trigger_error"
  Do Form m:\dev\frm\dartmail2 With tsendto,tFrom,tSubject, tcc,tattach,tmessage  ,"A"
ELSE 
  tsendto = "tmarg@fmiint.com"
  tattach = ""
  tcc =""
  tFrom ="Toll/FMI EDI System Operations <fmi-transload-ops@fmiint.com>"
  tmessage = "NO_billabong_trigger_errors"+Ttoc(Datetime())
  tSubject = "NO_billabong_trigger_errors"
  Do Form m:\dev\frm\dartmail2 With tsendto,tFrom,tSubject, tcc,tattach,tmessage  ,"A"
 endif 
********************************missing bol  

xsqlexec("select wo_num, ship_ref, del_date from outship where mod='"+goffice+"' " + ;
	"and accountid in (6718,6719,6722,6744,6745,6746,6747,6748,6749)  and del_date>{"+dtoc(date()-30)+"} and empty(bol_no)","t1",,"wh")

Select t1
If Reccount() > 0
  Export To "S:\billabong\temp\billabong_del_date_missing_bol"  Type Xls

  tsendto = "tmarg@fmiint.com,todd.margolin@tollgroup.com,paul.gaidis@tollgroup.com,joe.bianchi@tollgroup.com"
  tattach = "S:\billabong\temp\billabong_del_date_missing_bol.xls"
  tcc =""
  tFrom ="Toll/FMI EDI System Operations <fmi-transload-ops@fmiint.com>"
  tmessage = "Billabong_PT missing BOL error"+Ttoc(Datetime())
  tSubject = "Billabong_PT missing BOL error"
  Do Form m:\dev\frm\dartmail2 With tsendto,tFrom,tSubject, tcc,tattach,tmessage  ,"A"
ELSE 
  tsendto = "tmarg@fmiint.com"
  tattach = ""
  tcc =""
  tFrom ="Toll/FMI EDI System Operations <fmi-transload-ops@fmiint.com>"
  tmessage = "NO_Billabong_PT missing BOL error"+Ttoc(Datetime())
  tSubject = "NO_Billabong_PT missing BOL error"
  Do Form m:\dev\frm\dartmail2 With tsendto,tFrom,tSubject, tcc,tattach,tmessage  ,"A"
Endif

Close Data All
schedupdate()
_Screen.Caption=gscreencaption