* copy monthly inventory data to eom tables

utilsetup("EOMINVENB")

guserid="AUTO"
gdevelopment=.f.

do case
case date()={11/29/17}
	xeomfilter1="half=.f."
	xeomfilter2=".t."
	xhalf=.f.
	xzmonth="1217"
case inlist(day(date()),15,16,17)
	xeomfilter1="half=.t."
	xeomfilter2="inlist(accountid,6213,6416,6417,6418)"
	xhalf=.t.
	xzmonth=dt2month(date())
otherwise
	xeomfilter1="half=.f."
	xeomfilter2=".t."
	xhalf=.f.
	xzmonth=dt2month(date()-1)
endcase

if date()={5/2/17}
	xzmonth="0417"
endif

xsqlexec("select * from whoffice",,,"wh")

select whoffice
scan for !test and inlist(office,"1","2","5","6","8")
	eominvendo()
endscan

wait clear

email("Dyoung@fmiint.com","INFO: EOMINVENB is done",,,,,.t.,,,,,.t.,,.t.)

*

schedupdate()

_screen.Caption=gscreencaption
on error

gunshot()