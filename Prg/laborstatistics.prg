* run Dept of Labor statistics as of each monthly payroll which contains the 12th

* need count for that payroll of non-supervisory vs all.

* non-supervisory = E87 with tax code 21,  ALL = just tax code 21

* run on or after the 24th of each month

runack("LS")

LOCAL loLaborStatisticsReport
loLaborStatisticsReport = CREATEOBJECT('LaborStatisticsReport')
loLaborStatisticsReport.MAIN()
CLOSE DATABASES ALL
RETURN

#DEFINE LOGIT 1
#DEFINE WAITIT 2
#DEFINE NOWAITIT 4
#DEFINE SENDIT 8
#DEFINE CRLF CHR(13) + CHR(10)
#DEFINE RETURN_DATA_MANDATORY .F.
#DEFINE RETURN_DATA_NOT_MANDATORY .T.
#DEFINE CSVFORMAT 6
#DEFINE WRITE_ONLY_UNBUFFERED 11
#DEFINE COMMA ","

DEFINE CLASS LaborStatisticsReport AS CUSTOM

	cProcessName = 'LaborStatisticsReport'

	lTestMode = .F.  && .F. FOR NORMAL OPERATION, .T. TO SUPPRESS ALL BUT DEVELOPER EMAILS AND CHANGE INPUT TABLE, ETC.

	dToday = DATE()
	*dToday = {^2010-01-24}

	lAutoYield = .T.

	cStartTime = TTOC(DATETIME())

	* connection properties
	nSQLHandle = 0

	* wait window properties
	nWaitWindowTimeout = 2
	lWaitWindowIsOn = .T.

	* logfile properties
	lLoggingIsOn = .T.
	cLogFile = 'F:\UTIL\HR\LABORSTATISTICS\LOGFILES\LABOR_STATISTICS_REPORT_log.txt'

	* INTERNAL email properties
	lSendInternalEmailIsOn = .T.
	cFrom = 'TGF Corporate <fmicorporate@fmiint.com>'
	cSendTo = 'Lucille.Waldrip@tollgroup.com, mariedesaye@fmiint.com, Lauren.Klaver@Tollgroup.com'
	cCC = 'Mark Bennett <mbennett@fmiint.com>'
	cSubject = 'Department of Labor Statistics Monthly Report'
	cAttach = ''
	cBodyText = ''

	cCOMPUTERNAME = ''
	cUSERNAME = ''


	FUNCTION INIT
		IF NOT DODEFAULT()
			RETURN .F.
		ENDIF
		WITH THIS
			*SET RESOURCE OFF
			CLOSE DATA
			SET CENTURY ON
			SET DATE AMERICAN  && MM/DD/YYYY
			SET HOURS TO 24
			SET ANSI ON
			SET TALK OFF
			SET DELETED ON
			SET CONSOLE OFF
			SET EXCLUSIVE OFF
			SET SAFETY OFF
			SET EXACT OFF
			SET STATUS BAR ON
			SET SYSMENU OFF
			SET ENGINEBEHAVIOR 70
			_VFP.AUTOYIELD = .lAutoYield
			.cCOMPUTERNAME = GETENV("COMPUTERNAME")
			.cUSERNAME = GETENV("USERNAME")
			IF .lTestMode THEN
				.cLogFile = 'F:\UTIL\HR\LABORSTATISTICS\LOGFILES\LABOR_STATISTICS_REPORT_log_TESTMODE.txt'
				.cSendTo = 'Mark Bennett <mbennett@fmiint.com>'
			ENDIF
			.lLoggingIsOn = .lLoggingIsOn AND NOT EMPTY(.cLogFile)
			IF .lLoggingIsOn THEN
				SET ALTERNATE TO (.cLogFile) ADDITIVE
				SET ALTERNATE ON
			ENDIF
		ENDWITH
	ENDFUNC


	FUNCTION DESTROY
		WITH THIS
			IF .lLoggingIsOn  THEN
				SET ALTERNATE OFF
				SET ALTERNATE TO
			ENDIF
		ENDWITH
		DODEFAULT()
	ENDFUNC


	FUNCTION MAIN
		WITH THIS
			LOCAL lnNumberOfErrors, lcSQLALLCOUNT , lcSQLE87COUNT , loError, lnThisMonth, lnThisYear, lcSQLNONE87COUNT
			LOCAL ldTwelfthOfThisMonth, lnDOW, lnPaydayNumber, ldPaydate, lnAllCount, lnE87Count, lnNONE87CountPrevWeek 
			LOCAL lnLastMonth, lnLastYear, lnGrossPay, lcReportInfo, lnGrossPayHourly ,lnGrossPaySalaried, lnNONE87Count

			TRY
				lnNumberOfErrors = 0

				.TrackProgress('=========================================================', LOGIT+SENDIT)
				.TrackProgress('LABOR STATISTICS REPORT process started....', LOGIT+SENDIT+NOWAITIT)
				.TrackProgress('COMPUTERNAME = ' + .cCOMPUTERNAME, LOGIT+SENDIT)
				.TrackProgress('USERNAME = ' + .cUSERNAME, LOGIT+SENDIT)
				.TrackProgress('PROJECT = LS', LOGIT+SENDIT)
				IF .lTestMode THEN
					.TrackProgress('                        TEST MODE                        ', LOGIT+SENDIT)
				ENDIF

				* connect to ADP
				OPEN DATABASE F:\UTIL\ADPCONNECTION\CONNECT1

				.nSQLHandle = SQLCONNECT("PCPAYWIN","MBENNETT","VP737A")

				IF .nSQLHandle > 0 THEN


					* calculate the paydate which contains the 12th of current month
					* IF THE 12TH IS A SATURDAY, THE PAYDATE = THE 18TH
					* IF THE 12TH IS A FRIDAY, THE PAYDATE = THE 19TH
					* THURSDAY = THE 20TH
					* WEDNESDAY = THE 21ST
					* TUESDAY = THE 22ND
					* MONDAY = THE 23RD
					* SUNDAY = THE 24TH

					lnThisMonth = MONTH(.dToday)
					lnThisYear = YEAR(.dToday)
					IF lnThisMonth = 1 THEN
						lnLastMonth = 12
						lnLastYear = lnThisYear - 1
					ELSE
						lnLastMonth = lnThisMonth - 1
						lnLastYear = lnThisYear
					ENDIF
					ldTwelfthOfThisMonth = CTOD( PADL(lnThisMonth,2,'0') + "/12/" + PADL(lnThisYear,4,'0')  )  && mm/dd/yyyy
					lnDOW = DOW(ldTwelfthOfThisMonth,1)  && parameter indicates that 1st dow = Sunday
					
					lnPaydayNumber = 25 - lnDOW
					
					*lnPaydayNumber = 23  && forcing a non-friday pay date
					
					ldPaydate = CTOD( PADL(lnThisMonth,2,'0') + "/" + PADL(lnPaydayNumber,2,'0') + " /" + STR(lnThisYear)  )  && mm/dd/yyyy
					lcPaydate = PADL(lnThisYear,4,'0') + "-"  + PADL(lnThisMonth,2,'0') + "-" + PADL(lnPaydayNumber,2,'0')

					lnPaydayNumberPrevWeek = 25 - lnDOW - 7
					ldPaydatePrevWeek = CTOD( PADL(lnThisMonth,2,'0') + "/" + PADL(lnPaydayNumberPrevWeek,2,'0') + " /" + STR(lnThisYear)  )  && mm/dd/yyyy
					lcPaydatePrevWeek = PADL(lnThisYear,4,'0') + "-"  + PADL(lnThisMonth,2,'0') + "-" + PADL(lnPaydayNumberPrevWeek,2,'0')

					* SQL FOR COUNT OF ALL NJ WORKERS -- NEED DISTINCT BECAUSE AN EE MAY HAVE GOTTEN MORE THAN ONE CHECK
					lcSQLALLCOUNT = ;
						"SELECT " + ;
						" DISTINCT " + ;
						" COMPANYCODE, " + ;
						" FILE# AS FILE_NUM " + ;
						" FROM REPORTS.V_CHK_VW_INFO " + ;
						" WHERE CHECKVIEWPAYDATE = DATE '" + lcPaydate + "' " + ;
						" AND CHECKVIEWSUISDICD = '21' "

					* SQL FOR COUNT OF SUPERVISORY NJ WORKERS -- NEED DISTINCT BECAUSE AN EE MAY HAVE GOTTEN MORE THAN ONE CHECK
					lcSQLNONE87COUNT = ;
						"SELECT " + ;
						" DISTINCT " + ;
						" COMPANYCODE, " + ;
						" FILE# AS FILE_NUM " + ;
						" FROM REPORTS.V_CHK_VW_INFO " + ;
						" WHERE CHECKVIEWPAYDATE = DATE '" + lcPaydate + "' " + ;
						" AND CHECKVIEWSUISDICD = '21' " + ;
						" AND COMPANYCODE IN ('E88','E89') "
					
					* THIS SQL IS FOR PREVIOUS WEEK, WHICH MAY OR NOT BE NEEDED.
					* IF WE GET 0 FOR SUPERVISORY COUNT FOR ldPaydate, THEN WE USE THIS SQL BASED ON ldPaydatePrevWeek TO GET SUPERVISORY COUNT.
					* SQL FOR COUNT OF SUPERVISORY NJ WORKERS -- NEED DISTINCT BECAUSE AN EE MAY HAVE GOTTEN MORE THAN ONE CHECK
					lcSQLNONE87COUNTPREVWEEK = ;
						"SELECT " + ;
						" DISTINCT " + ;
						" COMPANYCODE, " + ;
						" FILE# AS FILE_NUM " + ;
						" FROM REPORTS.V_CHK_VW_INFO " + ;
						" WHERE CHECKVIEWPAYDATE = DATE '" + lcPaydatePrevWeek + "' " + ;
						" AND CHECKVIEWSUISDICD = '21' " + ;
						" AND COMPANYCODE IN ('E88','E89') "

					* SQL FOR COUNT OF NON-SUPERVISORY NJ WORKERS -- NEED DISTINCT BECAUSE AN EE MAY HAVE GOTTEN MORE THAN ONE CHECK
					lcSQLE87COUNT = ;
						"SELECT " + ;
						" DISTINCT " + ;
						" COMPANYCODE, " + ;
						" FILE# AS FILE_NUM " + ;
						" FROM REPORTS.V_CHK_VW_INFO " + ;
						" WHERE CHECKVIEWPAYDATE = DATE '" + lcPaydate + "' " + ;
						" AND CHECKVIEWSUISDICD = '21' " + ;
						" AND COMPANYCODE = 'E87' "

					* SQL FOR TOTAL GROSS PAY FOR PRIOR MONTH
					lcSQLGrossPay = ;
						"SELECT " + ;
						" SUM({fn IFNULL(CHECKVIEWGROSSPAYA,00000000.00)}) AS GROSSPAY " + ;
						" FROM REPORTS.V_CHK_VW_INFO " + ;
						" WHERE {fn MONTH(CHECKVIEWPAYDATE)} = " + TRANSFORM(lnLastMonth) + ;
						" AND {fn YEAR(CHECKVIEWPAYDATE)} = " + TRANSFORM(lnLastYear)

					* SQL FOR TOTAL GROSS PAY FOR PRIOR MONTH - HOURLY
					lcSQLGrossPayHourly = ;
						"SELECT " + ;
						" SUM({fn IFNULL(CHECKVIEWGROSSPAYA,00000000.00)}) AS GROSSPAY " + ;
						" FROM REPORTS.V_CHK_VW_INFO " + ;
						" WHERE {fn MONTH(CHECKVIEWPAYDATE)} = " + TRANSFORM(lnLastMonth) + ;
						" AND {fn YEAR(CHECKVIEWPAYDATE)} = " + TRANSFORM(lnLastYear) + ;
						" AND COMPANYCODE = 'E87' "

					* SQL FOR TOTAL GROSS PAY FOR PRIOR MONTH - SALARIED
					lcSQLGrossPaySalaried = ;
						"SELECT " + ;
						" SUM({fn IFNULL(CHECKVIEWGROSSPAYA,00000000.00)}) AS GROSSPAY " + ;
						" FROM REPORTS.V_CHK_VW_INFO " + ;
						" WHERE {fn MONTH(CHECKVIEWPAYDATE)} = " + TRANSFORM(lnLastMonth) + ;
						" AND {fn YEAR(CHECKVIEWPAYDATE)} = " + TRANSFORM(lnLastYear) + ;
						" AND COMPANYCODE IN ('E88','E89') "

					IF .lTestMode THEN
						.TrackProgress('                        TEST MODE                        ', LOGIT+SENDIT)
						.TrackProgress('lcSQLALLCOUNT =' + lcSQLALLCOUNT , LOGIT+SENDIT)
						.TrackProgress('lcSQLE87COUNT =' + lcSQLE87COUNT, LOGIT+SENDIT)
						.TrackProgress('lcSQLGrossPay =' + lcSQLGrossPay, LOGIT+SENDIT)
					ENDIF

					IF USED('CURALLCOUNT') THEN
						USE IN CURALLCOUNT
					ENDIF
					IF USED('CURE87COUNT') THEN
						USE IN CURE87COUNT
					ENDIF
					IF USED('CURNONE87COUNT') THEN
						USE IN CURNONE87COUNT
					ENDIF
					IF USED('CURNONE87COUNTPREVWEEK') THEN
						USE IN CURNONE87COUNTPREVWEEK
					ENDIF
					IF USED('CURGROSSPAY') THEN
						USE IN CURGROSSPAY
					ENDIF
					IF USED('CURGROSSPAYHOURLY') THEN
						USE IN CURGROSSPAYHOURLY
					ENDIF
					IF USED('CURGROSSPAYSALARIED') THEN
						USE IN CURGROSSPAYSALARIED
					ENDIF

					IF .ExecSQL(lcSQLALLCOUNT , 'CURALLCOUNT', RETURN_DATA_MANDATORY) AND ;
							.ExecSQL(lcSQLE87COUNT, 'CURE87COUNT', RETURN_DATA_MANDATORY) AND ;
							.ExecSQL(lcSQLGrossPay, 'CURGROSSPAY', RETURN_DATA_MANDATORY) AND ;
							.ExecSQL(lcSQLGrossPayHourly, 'CURGROSSPAYHOURLY', RETURN_DATA_MANDATORY) AND ;
							.ExecSQL(lcSQLNONE87COUNT, 'CURNONE87COUNT', RETURN_DATA_NOT_MANDATORY) AND ;
							.ExecSQL(lcSQLNONE87COUNTPREVWEEK, 'CURNONE87COUNTPREVWEEK', RETURN_DATA_NOT_MANDATORY) AND ;
							.ExecSQL(lcSQLGrossPaySalaried, 'CURGROSSPAYSALARIED', RETURN_DATA_NOT_MANDATORY) THEN

						lnAllCount = RECCOUNT('CURALLCOUNT')

						lnE87Count = RECCOUNT('CURE87COUNT')

						SELECT CURGROSSPAY
						GOTO TOP
						lnGrossPay = CURGROSSPAY.GROSSPAY

						SELECT CURGROSSPAYHOURLY
						GOTO TOP
						lnGrossPayHourly = CURGROSSPAYHOURLY.GROSSPAY

						IF USED('CURNONE87COUNT') THEN
							lnNONE87Count = RECCOUNT('CURNONE87COUNT')
						ELSE
							lnNONE87Count = 0
						ENDIF
						
						IF USED('CURNONE87COUNTPREVWEEK') THEN
							lnNONE87CountPrevWeek = RECCOUNT('CURNONE87COUNTPREVWEEK')
						ELSE
							lnNONE87CountPrevWeek = 0
						ENDIF

						IF USED('CURGROSSPAYSALARIED') AND NOT EOF('CURGROSSPAYSALARIED') THEN
							SELECT CURGROSSPAYSALARIED
							GOTO TOP
							lnGrossPaySalaried = CURGROSSPAYSALARIED.GROSSPAY
						ELSE
							lnGrossPaySalaried = 0
						ENDIF

						*SET STEP ON
						IF lnNONE87Count > 0 THEN
							* there were both Salaried and Hourly counts for the paydate
							lnAllCount = lnE87Count + lnNONE87Count
							lcReportInfo = "For Payroll Date = " + DTOC(ldPaydate) + ":" + CRLF+ CRLF + ;
								"    Count of Supervisory NJ Workers = " + TRANSFORM(lnNONE87Count,'9999') + CRLF + CRLF + ;
								"Count of Non-supervisory NJ Workers = " + TRANSFORM(lnE87Count,'9999') + CRLF + CRLF
						ELSE
							* there were only Hourly counts for the paydate; get Salaried from previous paydate
							lnAllCount = lnE87Count + lnNONE87CountPrevWeek
							lcReportInfo = "For Payroll Date = " + DTOC(ldPaydate) + ":" + CRLF+ CRLF + ;
								"Count of Non-supervisory NJ Workers = " + TRANSFORM(lnE87Count,'9999') + CRLF + CRLF + ;
								"For Payroll Date = " + DTOC(ldPaydatePrevWeek) + ":" + CRLF+ CRLF + ;
								"    Count of Supervisory NJ Workers = " + TRANSFORM(lnNONE87CountPrevWeek,'9999') + CRLF + CRLF 
						ENDIF

						lcReportInfo = lcReportInfo + ;
								"            Count of All NJ Workers = " + TRANSFORM(lnAllCount,'9999') + CRLF + CRLF + CRLF

						lcReportInfo = lcReportInfo + ;
							"For Pay Month = " + CMONTH(GOMONTH(ldPaydate,-1)) + " " + TRANSFORM(lnLastYear) + ":" + CRLF + CRLF + ;
							"    Total Gross Pay for Supervisory NJ employees = " + TRANSFORM(lnGrossPaySalaried,'$9,999,999,999.00') + CRLF + CRLF + ;
							"Total Gross Pay for Non-supervisory NJ employees = " + TRANSFORM(lnGrossPayHourly,'$9,999,999,999.00') + CRLF + CRLF + ;
							"            Total Gross Pay for all NJ employees = " + TRANSFORM(lnGrossPay,'$9,999,999,999.00') + CRLF + CRLF

						.cBodyText = lcReportInfo + CRLF + CRLF + .cBodyText

						.cSubject = 'Department of Labor Statistics Monthly Report, for Pay Date: ' + DTOC(ldPaydate)

						CLOSE DATABASES ALL
					ENDIF  &&  .ExecSQL(lcSQLALLCOUNT , 'SQLCURSOR1', RETURN_DATA_MANDATORY)

					.TrackProgress('LABOR STATISTICS REPORT process ended normally.', LOGIT+SENDIT+NOWAITIT)
				ELSE
					* connection error
					.TrackProgress('Unable to connect to ADP Payroll System.', LOGIT+SENDIT)
				ENDIF   &&  .nSQLHandle > 0

			CATCH TO loError

				.TrackProgress('There was an error.',LOGIT+SENDIT)
				.TrackProgress('The Error # is: ' + TRANSFORM(loError.ErrorNo), LOGIT+SENDIT)
				.TrackProgress('The Error Message is: ' + TRANSFORM(loError.MESSAGE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred in Program: ' + TRANSFORM(loError.PROCEDURE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred at line #: ' + TRANSFORM(loError.LINENO), LOGIT+SENDIT)
				lnNumberOfErrors = lnNumberOfErrors + 1
				IF TYPE('oWorkbook') = "O" THEN
					oWorkbook.SAVE()
				ENDIF
				IF TYPE('oExcel') = "O" THEN
					oExcel.QUIT()
				ENDIF
				CLOSE DATABASES ALL

			ENDTRY

			CLOSE DATA
			WAIT CLEAR
			***************** INTERNAL email results ******************************
			.TrackProgress('About to send status email.',LOGIT)
			.TrackProgress('The logfile is: ' + .cLogFile, SENDIT)
			.TrackProgress('LABOR STATISTICS REPORT process started: ' + .cStartTime, LOGIT+SENDIT)
			.TrackProgress('LABOR STATISTICS REPORT process finished: ' + TTOC(DATETIME()), LOGIT+SENDIT)

			IF .lSendInternalEmailIsOn THEN
				DO FORM dartmail2 WITH .cSendTo,.cFrom,.cSubject,.cCC,.cAttach,.cBodyText,"A"
				.TrackProgress('Sent status email.',LOGIT)
			ELSE
				.TrackProgress('Did not send status email: THIS.lSendInternalEmailIsOn = FALSE.',LOGIT)
			ENDIF

		ENDWITH
		RETURN
	ENDFUNC && main


	FUNCTION ExecSQL
		LPARAMETERS tcSQL, tcCursorName, tlNoDataReturnedIsOkay
		LOCAL llRetval, lnResult
		WITH THIS
			* close target cursor if it's open
			IF USED(tcCursorName)
				USE IN (tcCursorName)
			ENDIF
			WAIT WINDOW "Fetching remote data; please wait..." NOWAIT
			lnResult = sqlexec(.nSQLHandle, tcSQL, tcCursorName)
			llRetval = ( lnResult > 0 )
			IF llRetval THEN
				* see if any data came back
				IF NOT tlNoDataReturnedIsOkay THEN
					IF (NOT USED(tcCursorName)) OR EOF(tcCursorName) THEN
						llRetval = .F.
						.TrackProgress('No data was returned by this query: ' +CRLF + tcSQL, LOGIT+SENDIT)
						.cSendTo = 'Mark Bennett <mbennett@fmiint.com>'
					ENDIF
				ENDIF
			ELSE
				.TrackProgress("Result Code [" + TRANSFORM(lnResult) + "] returned by this query: " + tcSQL, LOGIT+SENDIT)
				.cSendTo = 'Mark Bennett <mbennett@fmiint.com>'
			ENDIF
			WAIT CLEAR
			RETURN llRetval
		ENDWITH
	ENDFUNC


	PROCEDURE TrackProgress
		* do any combination of Wait Window, writing to logfile, and adding to body of email,
		* based on nFlags parameter.
		LPARAMETERS tcExpression, tnFlags
		WITH THIS
			IF BITAND(tnFlags,LOGIT) = LOGIT THEN
				IF .lLoggingIsOn THEN
					?
					? .cProcessName + "  " + TTOC(DATETIME()) + ": " + tcExpression
				ENDIF
			ENDIF
			IF .lWaitWindowIsOn THEN
				IF BITAND(tnFlags,NOWAITIT) = NOWAITIT THEN
					WAIT WINDOW tcExpression NOWAIT
				ENDIF
				IF BITAND(tnFlags,WAITIT) = WAITIT THEN
					WAIT WINDOW tcExpression TIMEOUT .nWaitWindowTimeout
				ENDIF
			ENDIF
			IF BITAND(tnFlags,SENDIT) = SENDIT THEN
				IF .lSendInternalEmailIsOn THEN
					.cBodyText = .cBodyText + tcExpression + CRLF + CRLF
				ENDIF
			ENDIF
		ENDWITH
	ENDPROC  &&  TrackProgress

ENDDEFINE

