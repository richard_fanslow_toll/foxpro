* Monthly reports for Steve Sykes
* a report, by division, how many temp labor hours were worked by shipping and receiving personnel in SAN PEDRO
*
* To be run early each month against prior month.
*
* EXEs = F:\UTIL\KRONOS\KRONOSSSYKESREPORT.EXE

LOCAL loKRONOSSSYKESREPORT

runack("KRONOSSSYKESREPORT")

utilsetup("KRONOSSSYKESREPORT")


loKRONOSSSYKESREPORT = CREATEOBJECT('KRONOSSSYKESREPORT')
loKRONOSSSYKESREPORT.MAIN()


schedupdate()

CLOSE DATABASES ALL
RETURN

#DEFINE LOGIT 1
#DEFINE WAITIT 2
#DEFINE NOWAITIT 4
#DEFINE SENDIT 8
#DEFINE CRLF CHR(13) + CHR(10)
#DEFINE LF CHR(10)
#DEFINE RETURN_DATA_MANDATORY .F.
#DEFINE RETURN_DATA_NOT_MANDATORY .T.
#DEFINE DAILY_OT_MULTIPLIER 1.5
#DEFINE DAILY_OT20_MULTIPLIER 2.0
#DEFINE xlEdgeBottom 9
#DEFINE xlContinuous 1
#DEFINE xlMedium -4138

DEFINE CLASS KRONOSSSYKESREPORT AS CUSTOM

	cProcessName = 'KRONOSSSYKESREPORT'

	lTestMode = .F.  && .F. FOR NORMAL OPERATION, .T. TO SUPPRESS ALL BUT DEVELOPER EMAILS AND CHANGE INPUT TABLE, ETC.

	lAutoYield = .T.

	cStartTime = TTOC(DATETIME())

	* connection properties
	nSQLHandle = 0

	* file properties
	nFileHandle = 0

	* date properties
	dtNow = DATETIME()

	dToday = DATE()
	* to force different pay weeks
	*dToday = {^2010-10-01}

	cToday = DTOC(DATE())

	* wait window properties
	nWaitWindowTimeout = 2
	lWaitWindowIsOn = .T.

	* logfile properties
	lLoggingIsOn = .T.
	cLogFile = 'F:\UTIL\KRONOS\Logfiles\KRONOSSSYKESREPORT_log.txt'

	* INTERNAL email properties
	lSendInternalEmailIsOn = .T.
	cFrom = 'mark.bennett@tollgroup.com'
	cSendTo = 'Steven.Sykes@Tollgroup.com'
	cCC = 'mbennett@fmiint.com'
	cSubject = ''
	cAttach = ''
	cBodyText = ''

	cCOMPUTERNAME = ''
	cUSERNAME = ''

	FUNCTION INIT
		IF NOT DODEFAULT()
			RETURN .F.
		ENDIF
		WITH THIS
			CLEAR
			*SET RESOURCE OFF
			CLOSE DATA
			SET CENTURY ON
			SET DATE AMERICAN
			SET HOURS TO 24
			SET ANSI ON
			SET TALK OFF
			SET DELETED ON
			SET CONSOLE OFF
			SET EXCLUSIVE OFF
			SET SAFETY OFF
			SET EXACT OFF
			SET STATUS BAR ON
			SET SYSMENU OFF
			SET ENGINEBEHAVIOR 70
			_VFP.AUTOYIELD = .lAutoYield
			.cCOMPUTERNAME = GETENV("COMPUTERNAME")
			.cUSERNAME = GETENV("USERNAME")
			IF .lTestMode THEN
				.cSendTo = 'mbennett@fmiint.com'
				.cLogFile = 'F:\UTIL\KRONOS\Logfiles\KRONOSSSYKESREPORT_log_TESTMODE.txt'
			ENDIF
			.lLoggingIsOn = .lLoggingIsOn AND NOT EMPTY(.cLogFile)
			IF .lLoggingIsOn THEN
				SET ALTERNATE TO (.cLogFile) ADDITIVE
				SET ALTERNATE ON
			ENDIF
		ENDWITH
	ENDFUNC


	FUNCTION DESTROY
		WITH THIS
			IF .lLoggingIsOn  THEN
				SET ALTERNATE OFF
				SET ALTERNATE TO
			ENDIF
		ENDWITH
		DODEFAULT()
	ENDFUNC


	FUNCTION MAIN
		WITH THIS
			LOCAL lnNumberOfErrors, lcSQL, ldToday, ldFromDate, ldToDate, lcSQLFromDate, lcSQLToDate
			LOCAL lcMonthYear, lcOutputFile, lcOutputFileShipping, lcOutputFileReceiving, lcMonthYear3, lcMonthYear2

			TRY
				lnNumberOfErrors = 0

				.TrackProgress('Kronos Temp Hours by Worksite Report process started....', LOGIT+SENDIT+NOWAITIT)
				.TrackProgress('COMPUTERNAME = ' + .cCOMPUTERNAME, LOGIT+SENDIT)
				.TrackProgress('USERNAME = ' + .cUSERNAME, LOGIT+SENDIT)
				.TrackProgress('PROJECT = KRONOSSSYKESREPORT', LOGIT+SENDIT)
				IF .lTestMode THEN
					.TrackProgress('                        TEST MODE                        ', LOGIT+SENDIT)
				ENDIF

				ldToday = .dToday
				ldToDate = ldToday - DAY(ldToday)  && this is last day of prior month
				ldFromDate = GOMONTH(ldToDate + 1,-1)

				*!*	**** to force other date range	**************************************************
				*ldFromDate = {^2013-08-01}
				*ldToDate   = {^2013-08-31}
				*!*	**********************************************************************************


				lcMonthYear = PROPER(CMONTH(ldFromDate)) + " " + TRANSFORM(YEAR(ldFromDate))
				lcMonthYear2 = TRANSFORM(ldFromDate) + " - " + TRANSFORM(ldToDate)
				lcMonthYear3 = DTOS(ldFromDate) + "-" + DTOS(ldToDate)

				.cSubject = 'San Pedro Temp Reports for: ' + lcMonthYear2
				lcOutputFileReceiving = "F:\UTIL\KRONOS\TEMPTIMEREPORTS\SP_TEMP_RECEIVING_" + lcMonthYear3 + ".XLS"
				lcOutputFileShipping = "F:\UTIL\KRONOS\TEMPTIMEREPORTS\SP_TEMP_SHIPPING_" + lcMonthYear3 + ".XLS"
				lcOutputFile = "F:\UTIL\KRONOS\TEMPTIMEREPORTS\SP_TEMP_SHIPPING_RECEIVING_" + lcMonthYear3 + ".XLS"

				lcSQLFromDate = .GetSQLStartDateTimeString( ldFromDate )
				lcSQLToDate = .GetSQLEndDateTimeString( ldToDate )

				IF USED('CURWTKHOURSPRE') THEN
					USE IN CURWTKHOURSPRE
				ENDIF
				IF USED('CURTOTALHOURS') THEN
					USE IN CURTOTALHOURS
				ENDIF

				SET TEXTMERGE ON

				TEXT TO	lcSQL NOSHOW

SELECT
D.LABORLEV1NM AS ADP_COMP,
D.LABORLEV2NM AS DIVISION,
D.LABORLEV3NM AS DEPT,
D.LABORLEV3DSC AS DEPTNAME,
D.LABORLEV4NM AS WORKSITE,
C.FULLNM AS EMPLOYEE,
A.APPLYDTM,
E.NAME AS PAYCODE,
(A.DURATIONSECSQTY / 3600.00) AS TOTHOURS
FROM WFCTOTAL A
JOIN WTKEMPLOYEE B
ON B.EMPLOYEEID = A.EMPLOYEEID
JOIN PERSON C
ON C.PERSONID = B.PERSONID
JOIN LABORACCT D
ON D.LABORACCTID = A.LABORACCTID
JOIN PAYCODE E
ON E.PAYCODEID = A.PAYCODEID
WHERE (A.APPLYDTM >= <<lcSQLFromDate>>)
AND (A.APPLYDTM <= <<lcSQLToDate>>)
AND (D.LABORLEV1NM IN ('TMP'))
AND (D.LABORLEV3NM IN ('0620','0625'))
AND (D.LABORLEV4NM IN ('SP2'))
AND E.NAME IN ('Regular Hours','Overtime Hours','Doubltime Hours')
ORDER BY 1,2,3,6,7

				ENDTEXT

				SET TEXTMERGE OFF


				IF .lTestMode THEN
					.TrackProgress('', LOGIT+SENDIT)
					.TrackProgress('lcSQL =' + lcSQL, LOGIT+SENDIT)
				ENDIF

				.nSQLHandle = SQLCONNECT("WTK2","sa","kr0n1tes")

				IF .nSQLHandle > 0 THEN

					IF .ExecSQL(lcSQL, 'CURWTKHOURSPRE', RETURN_DATA_MANDATORY) THEN

*!*				SELECT CURWTKHOURSPRE
*!*				BROWSE

						SELECT PADR(lcMonthYear2,30) AS DATERANGE, ;
							DIVISION, ;
							DEPTNAME, ;
							SUM(IIF(PAYCODE = 'Regular Hours',TOTHOURS,0.00)) AS REGHRS, ;
							SUM(IIF(PAYCODE = 'Overtime Hours',TOTHOURS,0.00)) AS OTHRS, ;
							SUM(IIF(PAYCODE = 'Doubltime Hours',TOTHOURS,0.00)) AS DTHRS, ;
							SUM(TOTHOURS) AS TOTHOURS ;
							FROM CURWTKHOURSPRE ;
							INTO CURSOR CURTOTALHOURS ;
							GROUP BY 1, 2, 3 ;
							ORDER BY 1, 2, 3 ;
							READWRITE

*!*				SELECT CURTOTALHOURS
*!*				BROWSE

						SELECT CURTOTALHOURS
						COPY TO (lcOutputFile) XL5
						.TrackProgress('Output to spreadsheet: ' + lcOutputFile, LOGIT+SENDIT+NOWAITIT)

						IF FILE(lcOutputFile) THEN
							* attach output file to email
							.cAttach = lcOutputFile
							.cBodyText = "See attached reports." + ;
								CRLF + CRLF + "<report log follows>" + ;
								CRLF + .cBodyText
						ELSE
							.TrackProgress('ERROR: unable to email spreadsheet: ' + lcOutputFileShipping, LOGIT+SENDIT+NOWAITIT)
						ENDIF

*!*							SELECT CURTOTALHOURS
*!*							COPY TO (lcOutputFileTotHours) XL5
*!*							.TrackProgress('Output to spreadsheet: ' + lcOutputFileTotHours, LOGIT+SENDIT+NOWAITIT)

*!*							IF FILE(lcOutputFileTotHours) THEN
*!*								* attach output file to email
*!*								.cAttach = .cAttach + "," + lcOutputFileTotHours
*!*							ELSE
*!*								.TrackProgress('ERROR: unable to email spreadsheet: ' + lcOutputFileTotHours, LOGIT+SENDIT+NOWAITIT)
*!*							ENDIF

					ENDIF  &&  .ExecSQL(lcSQL, 'CURWTKEMPLOYEE', RETURN_DATA_MANDATORY)

					CLOSE DATABASES ALL


				ELSE
					* connection error
					.TrackProgress('Unable to connect to WTK System.', LOGIT+SENDIT)
				ENDIF   &&  .nSQLHandle > 0

			CATCH TO loError

				.TrackProgress('There was an error.',LOGIT+SENDIT)
				.TrackProgress('The Error # is: ' + TRANSFORM(loError.ERRORNO), LOGIT+SENDIT)
				.TrackProgress('The Error Message is: ' + TRANSFORM(loError.MESSAGE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred in Program: ' + TRANSFORM(loError.PROCEDURE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred at line #: ' + TRANSFORM(loError.LINENO), LOGIT+SENDIT)
				lnNumberOfErrors = lnNumberOfErrors + 1
				CLOSE DATABASES ALL

			ENDTRY

			CLOSE DATA
			WAIT CLEAR
			***************** INTERNAL email results ******************************
			.TrackProgress('About to send status email.',LOGIT)
			.TrackProgress('==================================================================================================================', SENDIT)
			.TrackProgress('The logfile is: ' + .cLogFile, SENDIT)
			.TrackProgress('San Pedro Temp Reports process started: ' + .cStartTime, LOGIT+SENDIT)
			.TrackProgress('San Pedro Temp Reports process finished: ' + TTOC(DATETIME()), LOGIT+SENDIT)

			IF .lSendInternalEmailIsOn THEN
				* try to trap error from not having dartmail dll's registered on user's pc...
				TRY
					DO FORM dartmail2 WITH .cSendTo,.cFrom,.cSubject,.cCC,.cAttach,.cBodyText,"A"
					.TrackProgress('Sent status email.',LOGIT)
				CATCH TO loError
					.TrackProgress('There was an error sending the status email.',LOGIT)
					.TrackProgress('The Error # is: ' + TRANSFORM(loError.ERRORNO), LOGIT)
					.TrackProgress('The Error Message is: ' + TRANSFORM(loError.MESSAGE), LOGIT)
					.TrackProgress('The Error occurred in Program: ' + TRANSFORM(loError.PROCEDURE), LOGIT)
					.TrackProgress('The Error occurred at line #: ' + TRANSFORM(loError.LINENO), LOGIT)
					lnNumberOfErrors = lnNumberOfErrors + 1
					CLOSE DATA
				ENDTRY

			ELSE
				.TrackProgress('Did not send status email: THIS.lSendInternalEmailIsOn = FALSE.',LOGIT)
			ENDIF

		ENDWITH
		RETURN
	ENDFUNC && main


	FUNCTION GetSQLStartDateTimeString
		* returns a timestamp string for use in SQL queries
		LPARAMETERS tdDate
		LOCAL lcSetDate, lcSQLDateTimeString
		lcSetDate = SET('DATE')
		SET DATE YMD
		lcSQLDateTimeString   = "{ts '" + TRANSFORM(YEAR(tdDate)) + "-" + PADL(MONTH(tdDate),2,"0") + "-" + PADL(DAY(tdDate),2,"0") + " 00:00:00.0'}"
		SET DATE &lcSetDate.
		RETURN lcSQLDateTimeString
	ENDFUNC


	FUNCTION GetSQLEndDateTimeString
		* returns a timestamp string for use in SQL queries
		LPARAMETERS tdDate
		LOCAL lcSetDate, lcSQLDateTimeString
		lcSetDate = SET('DATE')
		SET DATE YMD
		lcSQLDateTimeString = "{ts '" + TRANSFORM(YEAR(tdDate)) + "-" + PADL(MONTH(tdDate),2,"0") + "-" + PADL(DAY(tdDate),2,"0") + " 23:59:59.9'}"
		SET DATE &lcSetDate.
		RETURN lcSQLDateTimeString
	ENDFUNC


	FUNCTION ExecSQL
		LPARAMETERS tcSQL, tcCursorName, tlNoDataReturnedIsOkay
		LOCAL llRetval, lnResult
		WITH THIS
			* close target cursor if it's open
			IF USED(tcCursorName)
				USE IN (tcCursorName)
			ENDIF
			WAIT WINDOW "Fetching remote data; please wait..." NOWAIT
			lnResult = SQLEXEC(.nSQLHandle, tcSQL, tcCursorName)
			llRetval = ( lnResult > 0 )
			IF llRetval THEN
				* see if any data came back
				IF NOT tlNoDataReturnedIsOkay THEN
					IF (NOT USED(tcCursorName)) OR EOF(tcCursorName) THEN
						llRetval = .F.
						.TrackProgress('No data was returned by this query: ' +CRLF + tcSQL, LOGIT+SENDIT)
					ENDIF
				ENDIF
			ELSE
				.TrackProgress("Result Code [" + TRANSFORM(lnResult) + "] returned by this query: " + tcSQL, LOGIT+SENDIT)
			ENDIF
			WAIT CLEAR
			RETURN llRetval
		ENDWITH
	ENDFUNC


	PROCEDURE TrackProgress
		* do any combination of Wait Window, writing to logfile, and adding to body of email,
		* based on nFlags parameter.
		LPARAMETERS tcExpression, tnFlags
		WITH THIS
			IF BITAND(tnFlags,LOGIT) = LOGIT THEN
				IF .lLoggingIsOn THEN
					?
					? .cProcessName + "  " + TTOC(DATETIME()) + ": " + tcExpression
				ENDIF
			ENDIF
			IF .lWaitWindowIsOn THEN
				IF BITAND(tnFlags,NOWAITIT) = NOWAITIT THEN
					WAIT WINDOW tcExpression NOWAIT
				ENDIF
				IF BITAND(tnFlags,WAITIT) = WAITIT THEN
					WAIT WINDOW tcExpression TIMEOUT .nWaitWindowTimeout
				ENDIF
			ENDIF
			IF BITAND(tnFlags,SENDIT) = SENDIT THEN
				IF .lSendInternalEmailIsOn THEN
					.cBodyText = .cBodyText + tcExpression + CRLF + CRLF
				ENDIF
			ENDIF
		ENDWITH
	ENDPROC  &&  TrackProgress


ENDDEFINE
