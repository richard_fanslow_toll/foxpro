*!* Checks existing BOLs marked delivered to determine if any have not produced records in the edi_trigger table

CLOSE DATA ALL
CLEAR

DO m:\dev\prg\_setvars WITH .T.

DO m:\dev\prg\lookups

ON ERROR DO fmerror WITH ERROR(), PROGRAM(), LINENO(1), SYS(16),, .T.

lcScreenCaption = "Delivery Cross Checker... Processing...."
_SCREEN.CAPTION = lcScreenCaption

CREATE CURSOR misstrigs (accountid N(10),acctname c(30),bol_no c(20),wo_num N(10),scac c(5),whse c(1),delenterdt T,parcel l)

USE F:\3pl\DATA\trigger945accts IN 0 ALIAS trigaccts SHARED NOUPDATE
xsqlexec("select * from account where inactive=0","account",,"qq")
index on accountid tag accountid
set order to
USE F:\3pl\DATA\edi_trigger IN 0 ALIAS edi_trigger SHARED NOUPDATE
USE F:\3pl\DATA\parcel_carriers IN 0 ALIAS pcarrs SHARED NOUPDATE

lPac = .F.
FOR nwhse = 1 TO 12
	DO CASE
		CASE nwhse = 1
			m.whse = "1"
			lPac = .T.
			_SCREEN.CAPTION= "BOL Cross Checking, MOD A"
		CASE nwhse = 2
			m.whse = "2"
			lPac = .T.
			_SCREEN.CAPTION= "BOL Cross Checking, MOD D"
		CASE nwhse = 3
			m.whse = "5"
			lPac = .T.
			_SCREEN.CAPTION= "BOL Cross Checking, MOD C"
		CASE nwhse = 4
			m.whse = "7"
			lPac = .T.
			_SCREEN.CAPTION= "BOL Cross Checking, MOD E"
		CASE nwhse = 5
			m.whse = "L"
			lPac = .T.
			_SCREEN.CAPTION= "BOL Cross Checking, MOD L"
		CASE nwhse = 6
			m.whse = "O"
			lPac = .T.
			_SCREEN.CAPTION= "BOL Cross Checking, MOD O"
		CASE nwhse = 7
			m.whse = "I"
			lPac = .T.
			_SCREEN.CAPTION= "BOL Cross Checking, MOD I"
		CASE nwhse = 8
			m.whse = "J"
			lPac = .F.
			_SCREEN.CAPTION= "BOL Cross Checking, MOD J"
		CASE nwhse = 9
			m.whse = "M"
			lPac = .F.
			_SCREEN.CAPTION= "BOL Cross Checking, MOD M"
		CASE nwhse = 10
			m.whse = "K"
			lPac = .F.
			_SCREEN.CAPTION= "BOL Cross Checking, MOD K"
		CASE nwhse = 11
			m.whse = "S"
			lPac = .F.
			_SCREEN.CAPTION= "BOL Cross Checking, MOD S"
		CASE nwhse = 12
			m.whse = "X"
			lPac = .F.
			_SCREEN.CAPTION= "BOL Cross Checking, MOD X"
	ENDCASE

	xsqlexec("select * from outship where mod='"+m.whse+"' and wo_date>{"+DTOC(DATE()-10)+"}",,,"wh")

	freightload()
	parcelload()
	USE IN outship

ENDFOR

SELECT misstrigs
DELETE FOR EMPTY(misstrigs.accountid)
LOCATE
SCAN
	IF !SEEK(misstrigs.accountid,'trigaccts','accountid')
		DELETE NEXT 1 IN misstrigs
	ENDIF
ENDSCAN

LOCATE
IF EOF()
	WAIT WINDOW "There are no missing BOLs in the EDI_TRIGGER table" TIMEOUT 3
ELSE
	COUNT TO recs1 FOR !DELETED()
	WAIT WINDOW "There are "+TRANSFORM(recs1)+" delivered BOLs not in the EDI_TRIGGER table" TIMEOUT 3
*	BROW
	COPY TO F:\3pl\DATA\missingtrigs TYPE XL5
	tattach = "f:\3pl\data\missingtrigs.xls"
	sendmail()
	DELETE FILE "f:\3pl\data\missingtrigs.xls"
ENDIF
CLOSE ALL

********************
PROCEDURE sendmail
********************
	SELECT 0
	USE F:\3pl\DATA\mailmaster ALIAS mm
	LOCATE FOR mm.edi_type = "MISC" AND mm.taskname = "GENERAL"
	STORE TRIM(IIF(mm.use_alt,mm.sendtoalt,mm.sendto)) TO tsendto
	STORE TRIM(IIF(mm.use_alt,mm.ccalt,mm.cc)) TO tcc
	USE IN mm
	tsendto = 'joe.bianchi@tollgroup.com'
	tcc = ''

	tsubject = "Delivered BOLs not in EDI_TRIGGER table"
	tmessage = "The attached Excel file contains shipments which have not produced a trigger record in EDI_TRIGGER."
	tmessage = tmessage+CHR(13)+"These shipments could be parcels where a tracking number has been manually entered."
	tFrom ="Toll EDI Operations <toll-edi-ops@tollgroup.com>"
	DO FORM m:\dev\frm\dartmail2 WITH tsendto,tFrom,tsubject,tcc,tattach,tmessage,"A"
ENDPROC

***********************
PROCEDURE freightload
***********************
	SELECT accountid,bol_no,consignee,ship_ref,scac,IIF(lPac,delenterdt+(3600*3),delenterdt) AS delenterdt,.F. AS parcel ;
		FROM outship ;
		WHERE del_date = DATE() ;
		AND !EMPTY(bol_no) ;
		INTO CURSOR tempos READW
	DELETE FOR delenterdt > DATETIME()-2400
	DELETE FOR !("SAMPLE"$tempos.ship_ref) OR !("SAMPLE"$tempos.consignee)
	LOCATE
	SCAN FOR !DELETED() && AND !"SAMPLE"$tempos.ship_ref AND !"SAMPLE"$tempos.consignee
		SCATTER MEMVAR
		IF m.accountid = 6416 AND LEFT(m.ship_ref,3) = '207'
			LOOP
		ENDIF

		IF SEEK(m.accountid,'account','accountid')
			m.acctname = ALLTRIM(account.acctname)
		ENDIF
		cBOL = ALLTRIM(tempos.bol_no)
		IF !SEEK(cBOL,'edi_trigger','bol')
*			SET STEP ON
			INSERT INTO misstrigs FROM MEMVAR
		ENDIF
	ENDSCAN
	USE IN tempos
ENDPROC

***********************
PROCEDURE parcelload
***********************
	SELECT accountid,bol_no,consignee,ship_ref,scac,IIF(lPac,delenterdt+(3600*3),delenterdt) AS delenterdt,.T. AS parcel ;
		FROM outship ;
		WHERE del_date = DATE() ;
		AND EMPTY(bol_no) ;
		INTO CURSOR tempos READW
	DELETE FOR delenterdt > DATETIME()-2400
	DELETE FOR !("SAMPLE"$tempos.ship_ref) OR !("SAMPLE"$tempos.consignee)

	LOCATE
	SCAN FOR !DELETED() && AND !"SAMPLE"$tempos.ship_ref
		SCATTER MEMVAR
		IF m.accountid = 6416 AND LEFT(m.ship_ref,3) = '207'
			LOOP
		ENDIF
		cSCAC = ALLTRIM(tempos.scac)
		IF !SEEK(cSCAC,'pcarrs','scac')
			LOOP
		ENDIF

		IF SEEK(m.accountid,'account','accountid')
			m.acctname = ALLTRIM(account.acctname)
		ENDIF
		INSERT INTO misstrigs FROM MEMVAR
	ENDSCAN
	USE IN tempos
ENDPROC
