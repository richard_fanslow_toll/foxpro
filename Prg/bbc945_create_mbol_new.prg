*!* BBC 945 MBOL Creation Program
*!* Creation Date: 08.15.2017, Joe from std 945 prg

Parameters cBOL,nAcctNum

Public Array thisarray(1)
Public c_CntrlNum,c_GrpCntrlNum,cReturned,cWO_Num,cWO_NumStr,nWO_Num1,lROFilesOut,lPrepack
Public nUnitsum,nCtnCount,tsendto,tcc,tsendtoerr,tccerr,lTesting,lTestinput,nPTCount,cUseFolder,cMBOL
Public nOrigSeq,nOrigGrpSeq,cProgname,cISA_Num,cRefBOL,cMailName,cOutPath,cHoldPath,cArchivePath
Public cMod,cEDIType,lParcelType,cFilenameShort,cFilenameOut,cFilenameArch,lcPath,cWO_NumList,cSubBOLDetail
Wait Window "BBC 945 Process" Timeout 1

lOverflow = .F.

lTesting   = .F.  && Set to .t. for testing
lTestinput = .F. && lTesting  &&  Set to .t. for test input files only!
lEmail = .T. && !lTesting
lTestmail = lTesting

Do m:\dev\prg\_setvars With lTesting

If !lTesting
  Set Asserts Off
Endif

Set Deleted On
cProgname = "bbc945_create_mbol"
If lTesting
  Assert .F. Message "At beginning of BBC 945 creation"
Endif
Set Escape On
On Escape Cancel
*ON ERROR debug
lIsError = .F.
lDoCatch = .T.
cShip_ref = ""
lCloseOutput = .T.
Store 0 To nFilenum,nOrigSeq,nOrigGrpSeq
cCarrierType = "M" && Default as "Motor Freight"
ccustname = ""
cWO_NumStr = ""
cWO_NumList = ""
cStatus=""
cRefBOL = '9999'
cUseFolder = ""

Try
  lFederated = .F.
  cPPName = "BBC"
  tfrom = "TGF Warehouse Operations <toll-edi-ops@tollgroup.com>"
  tmessage=""
  tcc = ""
  lDoBOLGEN = .F.

  lISAFlag = .T.
  lSTFlag = .T.
  nLoadid = 1
  lSQLMail = .F.
  lDoCompare = Iif(lTesting Or lTestinput,.F.,.T.)

  lDoCompare = .F.
  cOffice = "L"
  cMod = cOffice
  gMasterOffice = cOffice
  goffice = cMod
  cEDIType = "945"
  lParcelType = .F.

  If Vartype(cBOL) = "L"
    If !lTesting And !lTestinput && AND DATE()<>{^2006-10-20}
      Wait Window "Office not provided...terminating" Timeout 3
      lCloseOutput = .F.
      Do ediupdate With "No OFFICE",.T.
      Throw
    Else
      Close Databases All
      If !Used('edi_trigger')
        cEDIFolder = "F:\3PL\DATA\"
        Use (cEDIFolder+"edi_trigger") In 0 Alias edi_trigger
      Endif
      Do m:\dev\prg\lookups
*!* TEST DATA AREA
      cBOL = "04917526757039494" && "04917526757035649"
      nAcctNum = 6757
      cTime = Datetime()
    Endif
  Endif

  cBOL=Trim(cBOL)
  cMBOL =Alltrim(cBOL)
  cSubBOLDetail =  ""

  cSCAC = ""

  swc_cutctns(cBOL)

  If Used('OUTSHIP')
    Use In outship
  Endif

  csq1 = [select * from outship where accountid = ]+Transform(nAcctNum)+[ and office = ']+cOffice+[' and bol_no = ']+cBOL+[']
  xsqlexec(csq1,,,"wh")
  If Reccount() = 0
    cErrMsg = "MISS BOL "+cBOL
    Do ediupdate With cErrMsg,.T.
    Throw
  Endif
  Index On bol_no Tag bol_no
  Index On wo_num Tag wo_num
  Index On ship_ref Tag ship_ref

  If !Used('BL')
    csq1 = [select * from bl where mblnum = ']+cMBOL+[']
    xsqlexec(csq1,,,"wh")
    Select bl
    Locate
    Count To nSubBillCnt For bl.mblnum = cMBOL
    Locate
  Endif

  Select outship
  If Seek(cBOL,'outship','bol_no')
    cSCAC = Trim(outship.scac)
  Else
    Set Step On
    Do ediupdate With "BAD SCAC",.T.
    Throw
  Endif
  Select outship
  Scan For outship.bol_no = cBOL
    If Empty(outship.dcnum)
      Replace outship.dcnum With '9999' In outship Next 1
      Replace outship.storenum With 9999 In outship Next 1
    Endif
  Endscan
  Locate

  Select Distinct ship_ref From outship Where bol_no = cBOL Into Cursor temp1ms
  Select temp1ms
  Select 0
  Create Cursor ovpts (outshipid N(10))
  Select ovpts
  Index On outshipid Tag outshipid

  Select temp1ms
  Scan
    cShip_ref = Alltrim(temp1ms.ship_ref)
    Select outship
    Scan For outship.ship_ref = cShip_ref
      If "OV"$outship.ship_ref
        Insert Into ovpts (outshipid) Values (outship.outshipid)
        Wait Window "Overflow PT Found at Ship_ref: "+Alltrim(outship.ship_ref) Nowait
        Exit
      Endif
    Endscan
  Endscan
  Use In temp1ms
  cRefBOL = Iif(lOverflow,cBOL,"9999")

  Select bol_no,wo_num ;
  FROM outship Where bol_no = cBOL ;
  AND accountid = nAcctNum ;
  GROUP By 1,2 ;
  ORDER By 1,2 ;
  INTO Cursor sqldc
  Locate
  Scan
    nWO_Num = sqldc.wo_num
    If lDoCompare
      cRetMsg = "X"
      Do m:\dev\prg\sqldata-Compare With "",cBOL,nWO_Num,cOffice,nAcctNum,cPPName
      If cRetMsg<>"OK"
        lCloseOutput = .T.
        cWO_Num = Alltrim(Str(nWO_Num1))
        Do ediupdate With "QTY COMPARE ERROR",.T.
        Throw
      Endif
    Endif
  Endscan

  cMailName = "BBC"
  Select 0
  Use F:\3pl\Data\mailmaster Alias mm Shared
  If lTesting Or lTestmail
    Locate For mm.edi_type = "MISC" And mm.taskname = "JOETEST"
    lUseAlt = mm.use_alt
    tsendto = Iif(lUseAlt,mm.sendtoalt,mm.sendto)
    tcc = Iif(lUseAlt,mm.ccalt,mm.cc)
    tsendtoerr = tsendto
    tccerr = tcc
    Locate
    Locate For mm.edi_type = "945" And mm.office = "L" And mm.accountid = nAcctNum
    cOutPath = Alltrim(mm.basepath)
    cArchivePath = Alltrim(mm.archpath)
    cHoldPath = Alltrim(mm.holdpath)+"mbolhold\"
  Else
    Locate For mm.edi_type = "945" And mm.office = "L" And mm.accountid = nAcctNum
    If !Found()
      Do ediupdate With "MAILMASTER ACCT PROBLEM",.T.
      Throw
    Endif
    lUseAlt = mm.use_alt
    tsendto = Iif(lUseAlt,mm.sendtoalt,mm.sendto)
    tcc = Iif(lUseAlt,mm.ccalt,mm.cc)
    cOutPath = Alltrim(mm.basepath)
    cArchivePath = Alltrim(mm.archpath)
    cHoldPath = Alltrim(mm.holdpath)+"mbolhold\"
  Endif

  Cd [&cHoldPath]
  Delete File *.*

  Locate
  Locate For mm.edi_type = "MISC" And taskname = "GENERAL"
  lUseAlt = mm.use_alt
  tsendtoerr = Iif(lUseAlt,mm.sendtoalt,mm.sendto)
  tccerr = Iif(lUseAlt,mm.ccalt,mm.cc)
  tsendtotest = Iif(lUseAlt,mm.sendtoalt,mm.sendto)
  tcctest = Iif(lUseAlt,mm.ccalt,mm.cc)
  Use In mm

*!* Mail sending for all MBOL 945s
  If !lTesting
    tsubject2 = "NOTICE: BBC 945 Process for MBOL"
    tattach2 = " "
    tsendto2 = tsendtotest
    tcc2 = tcctest
    tmessage2 = "Potential files for MBOL# "+cBOL+"...monitor output for correct number of files."
    tmessage2 = tmessage2 + Chr(13) + "Expected number of 945 files: "+Alltrim(Str(nSubBillCnt))
    tmessage2 = tmessage2 + Chr(13) + "Processed at "+Ttoc(Datetime())
    Do Form m:\dev\frm\dartmail2 With tsendto2,tfrom,tsubject2,tcc2,tattach2,tmessage2,"A"
  Endif
*!* End Mailing section

  lParcelType = .F.
  cCarrierType = Iif(lParcelType,"U","M")

  If Type("nWO_Num")<> "N"
    lCloseOutput = .T.
    Do ediupdate With "BAD WO#",.T.
    Throw
  Endif
  cWO_Num = Alltrim(Str(nWO_Num))

  lTestmail = lTesting && Sends mail to Joe only
  lBBCFilesOut = !lTesting && If true, copies output to FTP folder (default = .t.)
  lStandalone = lTesting

*!*  lTestMail = .T.
*!* lBBCFilesOut = .F.

  Store "LB" To cWeightUnit
  lPrepack = .F.

  Wait Window "Now preparing tables and variables" Nowait Noclear

*!* SET CUSTOMER CONSTANTS
  ccustname = Upper("BBC")  && Customer Identifier
  cX12 = "004050"  && X12 Standards Set used

*!* SET OTHER CONSTANTS
  Do Case
  Case cOffice = "M"
    cCustLoc =  "FL"
    cFMIWarehouse = ""
    cCustPrefix = "RW"
    cDivision = "Florida"
    cFolder = "WHM"
    cSF_Addr1  = "11400 NW 32ND AVE"
    cSF_CSZ    = "MIAMI+FL+33167"

  Case cOffice = "N"
    cCustLoc =  "NJ"
    cFMIWarehouse = ""
    cCustPrefix ="RW"
    cDivision = "New Jersey"
    cFolder = "WHI"
    cSF_Addr1  = "800 FEDERAL BLVD"
    cSF_CSZ    =  "CARTERET+NJ+07008"

  Case cOffice = "C"
    cCustLoc =  "CA"
    cFMIWarehouse = ""
    cCustPrefix ="RW"
    cDivision = "San Pedro"
    cFolder = "WH2"
    cSF_Addr1  = "450 WESTMONT DRIVE"
    cSF_CSZ    = "SAN PEDRO+CA+90731"
  Otherwise
    cCustLoc =  "ML"
    cFMIWarehouse = ""
    cCustPrefix ="RW"
    cDivision = "Mira Loma"
    cFolder = "WHL"
    cSF_Addr1  = "3355 DULLES DR"
    cSF_CSZ    = "MIRA LOMA+CA+91752"

  Endcase
  cCustFolder = Upper(ccustname)
  lNonEDI = .F.  && Assumes not a Non-EDI 945
  Store "" To lcKey
  Store 0 To alength,nLength
  cTime = Datetime()
  cDelTime = Substr(Ttoc(cTime,1),9,4)
  cApptNum = ""

*!* SET OTHER CONSTANTS
  cString = ""

  csendqual = "ZZ"
  csendid = Iif(lTesting,"TOLLTEST","TOLL")
  crecqual = "ZZ"
  crecid = Iif(lTesting,"BBCTOLL","BBCTOLLTEST")

  cfd = "|"
  csegd = "~"
  cterminator = ">" && Used at end of ISA segment

  nSegCtr = 0
  cdate = Dtos(Date())
  cTruncDate = Right(cdate,6)
  cTruncTime = Substr(Ttoc(Datetime(),1),9,4)
  cfiledate = cdate+cTruncTime
  cOrig = "J"
  nSTCount = 0
  cStyle = ""
  cPTString = ""
  nTotCtnWt = 0
  nTotCtnCount = 0
  nUnitsum = 0
  cTargetStyle = ""

** PAD ID Codes

  If Used('outship')
    Use In outship
  Endif

  xsqlexec("select * from outship where bol_no = '"+cBOL+"'",,,"wh")
  Index On bol_no Tag bol_no
  Index On ship_ref Tag ship_ref
  Index On wo_num Tag wo_num

  If !Seek(cBOL,'outship','bol_no')
    Wait Window "BOL not found in OUTSHIP" Timeout 2
    Do ediupdate With "BOL NOT FOUND",.T.
    Throw
  Endif

  Select dcnum From outship Where outship.bol_no = cBOL Group By 1 Into Cursor tempx
  Locate

  If Eof()
    Do ediupdate With "EMPTY DC/STORE#",.T.
    Throw
  Endif

  If Empty(tempx.dcnum)
    Use In tempx
    Select storenum From outship Where outship.bol_no = cBOL Group By 1 Into Cursor tempx
    If Eof()
      Do ediupdate With "EMPTY DC/STORE#",.T.
      Throw
    Endif

    If Empty(tempx.storenum)
      Do ediupdate With "EMPTY DC/STORE#",.T.
      Throw
    Else
      If Reccount('tempx')  > 1 And cBOL<>"04907314677854548"
        Do ediupdate With "MULTIPLE DC #'s",.T.
        Throw
      Endif
    Endif
  Endif

  If Used('tempx')
    Use In tempx
  Endif

  If Used('OUTDET')
    Use In outdet
  Endif

  selectoutdet()
  Select outdet
  Index On outdetid Tag outdetid

&& Added code blocks to the following to overcome BOL vs. WO selection issues, 12.23.2005
  Select outship
  Locate

*!*    IF USED("sqlwo")
*!*      USE IN sqlwo
*!*    ENDIF
*!*    DELETE FILE "F:\3pl\DATA\sqlwo.dbf"
*!*    SELECT bol_no,wo_num ;
*!*      FROM outship ;
*!*      WHERE bol_no = cBOL ;
*!*      AND INLIST(accountid,nAcctNum) ;
*!*      GROUP BY 1,2 ;
*!*      ORDER BY 1,2 ;
*!*      INTO DBF F:\3pl\DATA\sqlwo
*!*    USE IN sqlwo
*!*    SELECT 0
*!*    USE F:\3pl\DATA\sqlwo ALIAS sqlwo
*!*    IF lTesting
*!*      LOCATE
*!*      IF EOF()
*!*        BROWSE
*!*      ENDIF
*!*    ENDIF

  If Used("sqlwopt")
    Use In sqlwopt
  Endif

  If File("F:\3pl\DATA\sqlwopt.dbf")
    Delete File "F:\3pl\DATA\sqlwopt.dbf"
  Endif

  Select bol_no,ship_ref,Iif("PROCESSMODE*SCANPACK"$outship.shipins,.T.,.F.) As scanpack ;
  FROM outship ;
  WHERE bol_no = cBOL ;
  AND accountid = nAcctNum ;
  AND del_date > Date()-3;
  GROUP By 1,2 ;
  ORDER By 1,2 ;
  INTO Dbf F:\3pl\Data\sqlwopt
  Use In sqlwopt
  Select 0
  Use F:\3pl\Data\sqlwopt Alias sqlwopt
  Locate
  If Eof()
    cErrMsg = "NO SQL DATA"
    Do ediupdate With cErrMsg,.T.
    Throw
  Endif

  cRetMsg = ""

  Do m:\dev\prg\sqlconnect_pt  With nAcctNum,ccustname,cPPName,.T.,cOffice,.T.
*  DO m:\dev\prg\sqlconnect_bol  WITH nAcctNum,ccustname,cPPName,.T.,cOffice

  If cRetMsg<>"OK"
    Do ediupdate With "SQLCONN ERR: "+cRetMsg,.T.
    Throw
  Endif

  Select vbbcpp
  If lTesting
    cWinMsg = "Browsing SQL Data file"
*ASSERT .F. MESSAGE cWinMsg
*WAIT WINDOW cWinMsg TIMEOUT 2
*BROWSE
  Endif

  #If 0
    Select wo_num From vbbcpp Group By 1 Into Cursor temprunid1
    Select runid From vbbcpp Group By 1 Into Cursor temprunid2
    Select temprunid1
    Store Reccount() To nWORecs
    Select temprunid2
    Store Reccount() To nRIDRecs

    If nWORecs#nRIDRecs
      Do ediupdate With "MULTIPLE RUNIDS",.T.
      Throw
    Endif
    Use In temprunid1
    Use In temprunid2
  #Endif

  If !Used("scacs")
    xsqlexec("select * from scac","scacs",,"wh")
    Index On scac Tag scac
  Endif

  Select outship
  Locate
  Wait Clear
*  WAIT WINDOW "Now creating Header information" NOWAIT NOCLEAR

  Select outship
  Locate

  If !lTesting
    If Empty(cBOL)  && May be empty if triggered from PT screen, or FedEx shipment
      Wait Clear
      Wait Window "MISSING Bill of Lading Number!" Timeout 2
      Do ediupdate With "BOL# EMPTY",.T.
      Throw
    Else
      If !Seek(Padr(Trim(cBOL),20),"outship","bol_no")
        Wait Clear
        Wait Window "Invalid Bill of Lading Number - Not Found!" Timeout 2
        Do ediupdate With "BOL# NOT FOUND",.T.
        Throw
      Endif
    Endif
  Endif

*************************************************************************
*1  PTIDSOURCE/OUTSHIP RECORD MAIN LOOP
*  Loops through individual OUTSHIP lines for BOL#
*************************************************************************
  Wait Clear
  Wait Window "Now creating MBOL#-based information..." Nowait Noclear

  Select bl
  Locate
  cMissDel = ""

  oscanstr = "bl.mblnum = cMBOL"

  nBLID = 0
  nSubRec = 0
  Assert .F. Message "At MBOL scan"
  Scan For &oscanstr
    cBOL = Trim(bl.bol_no)
    If cMBOL = '9999' And cBOL # '04907304677451588'  && to select only a single sub-BOL to reprocess
      Loop
    Endif
    nAcctNum = 6757
    nBLID = bl.blid
    Wait Window "Now processing Sub-BOL #"+cBOL Timeout 2
    Create Cursor temp945 (accountid i,isa_num c(10),st_num c(10),wo_num i,bol_no c(20),ship_ref c(20),filename c(50),filedate T)
    Create Cursor tempbbc945 (accountid i,isa_num c(10),st_num c(10),wo_num i,bol_no c(20),ship_ref c(20),filename c(50),filedate T)
*!* HEADER LEVEL EDI DATA
    Do num_incr_isa

    cISA_Num = Padl(c_CntrlNum,9,"0")
    nISA_Num = Int(Val(cISA_Num))

    cISACode = Iif(lTesting,"T","P")
    dt2 = Datetime()
    dt1 = Ttoc(dt2,1)
    dtmail = Ttoc(dt2)

    csendidlong = Padr(csendid,15," ")
    crecidlong = Padr(crecid,15," ")

    lcPath = Iif(lTesting,("F:\FTPUSERS\BBCRW\945OUTTest\"),cOutPath)
    cFilenameHold = (cHoldPath+cCustPrefix+dt1+".945")
    cFilenameShort = Justfname(cFilenameHold)
    cFilenameOut = (lcPath+cFilenameShort)
    cFilenameArch = (cArchivePath+cFilenameShort)
    nFilenum = Fcreate(cFilenameHold)


    Store "ISA"+cfd+"00"+cfd+"          "+cfd+"00"+cfd+"          "+cfd+csendqual+cfd+csendidlong+cfd+;
    crecqual+cfd+crecidlong+cfd+cTruncDate+cfd+cTruncTime+cfd+"U"+cfd+"00401"+cfd+;
    cISA_Num+cfd+"0"+cfd+cISACode+cfd+cterminator+csegd To cString
    Do cstringbreak

    Store "GS"+cfd+"SW"+cfd+csendid+cfd+crecid+cfd+cdate+cfd+cTruncTime+cfd+c_CntrlNum+;
    cfd+"X"+cfd+cX12+csegd To cString
    Do cstringbreak

    xsqlexec("select * from bldet where mod='"+goffice+"' and blid="+Transform(nBLID),,,"wh")

    Select bldet
    If lTesting
      Browse
      Set Step On
    Endif
    Locate
    Scan For bldet.blid = nBLID And Inlist(bldet.accountid,&gbbcaccounts)
      cShip_ref = Alltrim(bldet.ship_ref)
      Select outship
      Locate
      If !Seek(nWO_Num,"outship","wo_num")
        Wait Clear
        Wait Window "Invalid Work Order Number - Not Found in OUTSHIP!" Timeout 2
        Do ediupdate With "WO# NOT FOUND",.T.
        Throw
      Endif
      Locate
      Locate For outship.ship_ref = cShip_ref And Inlist(outship.accountid,&gbbcaccounts)
      If !Found()
        Set Step On
        Do ediupdate With "MISS SHIP_REF",.T.
        Throw
      Endif
      nWO_Num = outship.wo_num
      Scatter Memvar Memo

      csq1 = [select * from outwolog where accountid = ]+Transform(nAcctNum)+[ and wo_num = ]+Transform(outship.wo_num)
      xsqlexec(csq1,,,"wh")
      If Reccount() = 0
        cErrMsg = "MISS OUTWOLOG REC FOR WO# "+cWO_Num
        Do ediupdate With cErrMsg,.T.
        Throw
      Endif
      Select outwolog
      Index On wo_num Tag wo_num

      If Seek(nWO_Num,"outwolog","wo_num")
        lPick = Iif(outwolog.picknpack,.T.,.F.)
        lPrepack = Iif(lPick,.F.,.T.)
      Endif

      Use In outwolog

*!* Added this code to trap miscounts in OUTDET Units
      If lPrepack
        Assert .F. Message "In prepack detail unit calculation"
        Select outdet
        Set Order To
        Sum totqty To nUnitTot1 For units And outdet.outshipid = outship.outshipid
        Sum (Val(Pack)*totqty) To nUnitTot2 For !units And outdet.outshipid = outship.outshipid
        If nUnitTot1<>nUnitTot2
          Set Step On
          Do ediupdate With "OUTDET TOTQTY ERR",.T.
          Throw
        Endif
        Select vbbcpp
        Sum totqty To nUnitTot2 For vbbcpp.outshipid = outship.outshipid
*        SUM totqty TO nUnitTot2 FOR units AND vbbcpp.outshipid = outship.outshipid
        If nUnitTot1<>nUnitTot2
          Set Step On
          Do ediupdate With "SQL TOTQTY ERR",.T.
          Throw
        Endif
        Locate
      Endif
      Select outdet
      Set Order To outdetid
      Locate
      Select outship
*!* End code addition
      cPO_Num = Alltrim(m.cnee_ref)


      Select outship
      If !lTesting
        Count To N For outship.bol_no = cMBOL And !emptynul(del_date)
      Else
        Count To N For outship.bol_no = cMBOL
      Endif

      If N=0
        Do ediupdate With "INCOMP BOL",.T.
        Throw
      Endif

      Locate
      cMissDel = ""

*  oscanstr = "outship.bol_no = PADR(cBOL,20) AND !EMPTYnul(outship.del_date) AND INLIST(outship.accountid,nAcctNum)"

      Select outship
      nPTCount = 0
*!*    SCAN FOR &oscanstr
*!*      IF "OV"$outship.ship_ref
*!*        LOOP
*!*      ENDIF
*!*      lPrepack = !outship.picknpack

*!*      SCATTER MEMVAR MEMO

      nWO_Num = outship.wo_num
      cWO_Num = Alltrim(Str(nWO_Num))
      nAcctNum = outship.accountid
      If !(cWO_Num$cWO_NumStr)
        cWO_NumStr = Iif(Empty(cWO_NumStr),cWO_Num,cWO_NumStr+","+cWO_Num)
      Endif
      If !(cWO_Num$cWO_NumList)
        cWO_NumList = Iif(Empty(cWO_NumList),cWO_Num,cWO_NumList+Chr(13)+cWO_Num)
      Endif

      cPO_Num = Alltrim(m.cnee_ref)
      cShip_ref = Alltrim(m.ship_ref)
      nPTCount = nPTCount+1
      csq1 = [select * from outwolog where accountid = ]+Transform(nAcctNum)+[ and wo_num = ]+Transform(outship.wo_num)
      xsqlexec(csq1,,,"wh")
      If Reccount() = 0
        cErrMsg = "MISS OUTWOLOG REC FOR WO# "+cWO_Num
        Do ediupdate With cErrMsg,.T.
        Throw
      Endif
      Select outwolog
      Index On wo_num Tag wo_num

      If Seek(nWO_Num,"outwolog","wo_num")
        lPick = Iif(outwolog.picknpack,.T.,.F.)
        lPrepack = Iif(lPick,.F.,.T.)
      Endif

      Select outship
*!* Added this code to trap miscounts in OUTDET/SQL\
      Wait Window "Now summing outdet/SQL by outshipid" Nowait

      If lPrepack
        If Seek(outship.outshipid,'ovpts','outshipid') Or cBOL = '04907315910258581'
          Set Deleted Off
          cRefBOL = cBOL
        Else
          Set Deleted On
        Endif
        Select outdet
        If Set("Deleted") = "OFF"
          Sum origqty To nUnitTot1 ;
          FOR !units ;
          AND outdet.outshipid = outship.outshipid ;
          AND outdet.wo_num = nWO_Num
        Else
          Sum totqty To nUnitTot1 ;
          FOR !units ;
          AND outdet.outshipid = outship.outshipid ;
          AND outdet.wo_num = nWO_Num
        Endif
        Select vbbcpp
        If Inlist(nWO_Num,5049591)
          Sum (totqty/Int(Val(Pack))) To nUnitTot2 For vbbcpp.outshipid = outship.outshipid
        Else
          Count To nUnitTot2 For vbbcpp.outshipid = outship.outshipid And vbbcpp.totqty > 0
        Endif

        If nUnitTot1<>nUnitTot2
          Set Step On
          Assert .F. Message "At SQL UNITQTY ERR"
          Do ediupdate With "SQL UNITQTY ERR-OSID "+Transform(outship.outshipid),.T.
          Throw
        Endif
      Else
*      ASSERT .f. MESSAGE "At OSID Qty scanning main loop"
        Select outdet
        Sum totqty To nUnitTot1 ;
        FOR units ;
        AND outdet.outshipid = outship.outshipid ;
        AND outdet.wo_num = nWO_Num
        Select vbbcpp
        Sum totqty To nUnitTot2 ;
        FOR vbbcpp.outshipid = outship.outshipid ;
        AND vbbcpp.totqty > 0
        If nUnitTot1<>nUnitTot2
          Assert .F. Message "At SQL UNITQTY ERR"
          Set Step On
          Do ediupdate With "SQL UNITQTY ERR-PIK",.T.
          Throw
        Endif
      Endif
      Select outdet
      Set Order To outdetid
      Locate

*!* End code addition

      If ("PENNEY"$Upper(outship.consignee) Or "KMART"$Upper(outship.consignee) Or "K-MART"$Upper(outship.consignee) Or "AMAZON"$Upper(outship.consignee))
        lApptFlag = .T.
      Else
        lApptFlag = .F.
      Endif
      If lTestinput
        ddel_date = Date()
        cApptNum = "99999"
        dapptdate = Date()
      Else
        ddel_date = outship.del_date
        If Empty(ddel_date)
          cMissDel = Iif(Empty(cMissDel),"The following PTs had no Delivery Dates:"+Chr(13)+Trim(cShip_ref),cMissDel+Chr(13)+Trim(cShip_ref))
        Endif
        cApptNum = Alltrim(outship.appt_num)

        If Empty(cApptNum) And !lParcelType && Penney/KMart Appt Number check
          If (!(lApptFlag) Or (Date()={^2006-05-18} And cBOL = "04907314677812395"))
            cApptNum = ""
          Else
            Do ediupdate With "EMPTY APPT #",.T.
            Throw
          Endif
        Endif
        dapptdate = outship.appt

        If Empty(dapptdate)
          dapptdate = outship.del_date
        Endif
      Endif

      If Alltrim(outship.SForCSZ) = ","
        Blank Fields outship.SForCSZ Next 1 In outship
      Endif

      alength = Alines(apt,outship.shipins,.T.,Chr(13))

      cTRNum = ""
      cPRONum = ""

      If (("WALMART"$outship.consignee) Or ("WAL-MART"$outship.consignee) ;
        OR ("WAL MART"$outship.consignee) Or ("AMAZON"$outship.consignee) Or m.scac = "ABFS")
        cKeyRec = Alltrim(outship.keyrec)
        cKeyRec = Trim(Strtran(cKeyRec,"#",""))
        cPRONum = Trim(Strtran(cKeyRec,"PR",""))

        If Empty(cPRONum)
          If lParcelType
            cPRONum = Alltrim(outship.bol_no)
          Endif
        Endif
      Endif

      nOutshipid = m.outshipid
      If lParcelType
        cPTString = Iif(Empty(cPTString),m.consignee+" "+cBOL+" "+cShip_ref,cPTString+Chr(13)+m.consignee+" "+cBOL+" "+cShip_ref)
      Else
        cPTString = Iif(Empty(cPTString),m.consignee+" "+cShip_ref,cPTString+Chr(13)+m.consignee+" "+cShip_ref)
      Endif
      m.CSZ = Trim(m.CSZ)
      Do Case
      Case m.CSZ = ","
        Do ediupdate With "MISSING CSZ "+cShip_ref,.T.
        Throw
      Case !(", "$m.CSZ)
        Do ediupdate With "UNSPACED COMMA IN CSZ",.T.
        Throw
      Endcase

      If Empty(M.CSZ)
        Wait Clear
        Wait Window "No SHIP-TO City/State/ZIP info...exiting" Timeout 2
        Do ediupdate With "NO CSZ ADDRESS INFO",.T.
        Throw
      Endif
      m.CSZ = Alltrim(Strtran(m.CSZ,"  "," "))
      nSpaces = Occurs(" ",Alltrim(m.CSZ))
      If nSpaces = 0
        Wait Window "SHIP-TO CSZ Segment Error" Timeout 3
        Do ediupdate With "SHIP-TO CSZ ERR: "+cShip_ref,.T.
        Throw
      Else
        nCommaPos = At(",",m.CSZ)
        nLastSpace = At(" ",m.CSZ,nSpaces)
        If Isalpha(Substr(Trim(m.CSZ),At(" ",m.CSZ,nSpaces-1)+1,2))
          cCity = Left(Trim(m.CSZ),At(",",m.CSZ)-1)
          cState = Substr(m.CSZ,nCommaPos+2,2)
          cZip = Trim(Substr(m.CSZ,nLastSpace+1))
        Else
          Wait Clear
          Wait Window "NOT ALPHA: "+Substr(Trim(m.CSZ),At(" ",m.CSZ,nSpaces-1)+1,2) Timeout 3
          cCity = Left(Trim(m.CSZ),At(",",m.CSZ)-1)
          cState = Substr(Trim(m.CSZ),At(" ",m.CSZ,nSpaces-2)+1,2)
          cZip = Trim(Substr(m.CSZ,nLastSpace+1))
        Endif
      Endif

      Store "" To cSForCity,cSForState,cSForZip
      cStoreName = segmentget(@apt,"STORENAME",alength)
      If !lFederated
        If !Empty(M.SForCSZ)
          m.SForCSZ = Alltrim(Strtran(m.SForCSZ,"  "," "))
          nSpaces = Occurs(" ",Alltrim(m.SForCSZ))
          If nSpaces = 0
            m.SForCSZ = Strtran(m.SForCSZ,",","")
            cSForCity = Alltrim(m.SForCSZ)
            cSForState = ""
            cSForZip = ""
          Else
            nCommaPos = At(",",m.SForCSZ)
            nLastSpace = At(" ",m.SForCSZ,nSpaces)
            nMinusSpaces = Iif(nSpaces=1,0,1)
            If Isalpha(Substr(Trim(m.SForCSZ),At(" ",Trim(m.SForCSZ),nSpaces-nMinusSpaces)+1,2))
              cSForCity = Left(Trim(m.SForCSZ),At(",",m.SForCSZ)-1)
              cSForState = Substr(m.SForCSZ,nCommaPos+2,2)
              cSForZip = Alltrim(Substr(m.SForCSZ,nCommaPos+4))
              If Isalpha(cSForZip)
                cSForZip = ""
              Endif
            Else
              Wait Clear
              Wait Window "NOT ALPHA: "+Substr(Trim(m.SForCSZ),At(" ",m.SForCSZ,nSpaces-1)+1,2) Timeout 3
              cSForCity = Left(Trim(m.SForCSZ),At(",",m.SForCSZ)-1)
              cSForState = Substr(Trim(m.SForCSZ),At(" ",m.SForCSZ,nSpaces-2)+1,2)
              cSForZip = Alltrim(m.SForCSZ)
              If Isalpha(cSForZip)
                cSForZip = ""
              Endif
            Endif
          Endif
        Endif
      Else
        Store "" To cCity,cState,cZip
      Endif

      Do num_incr_st
      Wait Clear
      Wait Window "Now creating Line Item information" Nowait Noclear

      Insert Into temp945 (accountid,isa_num,st_num,wo_num,bol_no,ship_ref,filename,filedate) ;
      VALUES (m.accountid,c_CntrlNum,c_GrpCntrlNum,m.wo_num,m.bol_no,m.ship_ref,cFilenameShort,dt2)

      Store "ST"+cfd+"945"+cfd+Padl(c_GrpCntrlNum,9,"0")+csegd To cString
      Do cstringbreak
      nSTCount = nSTCount + 1
      nSegCtr = 1

      cSalesOrder = segmentget(@apt,"SALESORDER",alength)
      Store "W06"+cfd+"J"+cfd+cShip_ref+cfd+cdate+cfd+Alltrim(cWO_Num)+cfd+cBOL+cfd+cPO_Num+csegd To cString
      Do cstringbreak
      nSegCtr = nSegCtr + 1
      nCtnNumber = 1  && Seed carton sequence count

      cStoreNum = segmentget(@apt,"STORENUM",alength)
      If Empty(cStoreNum)
        cStoreNum = Alltrim(m.dcnum)
      Endif
      Store "N1"+cfd+"ST"+cfd+Alltrim(m.consignee)+cfd+"93"+cfd+cStoreNum+csegd To cString
      Do cstringbreak
      nSegCtr = nSegCtr + 1
      If Empty(M.ADDRESS2)
        Store "N3"+cfd+Alltrim(M.ADDRESS)+csegd To cString
      Else
        Store "N3"+cfd+Alltrim(M.ADDRESS)+cfd+Alltrim(M.ADDRESS2)+csegd To cString
      Endif
      Do cstringbreak
      nSegCtr = nSegCtr + 1

      cCountry = segmentget(@apt,"COUNTRY",alength)
      cCountry = Allt(cCountry)
      If Empty(cCountry)
        cCountry = "US"
      Endif
      Store "N4"+cfd+cCity+cfd+cState+cfd+cZip+cfd+cCountry+csegd To cString
      Do cstringbreak
      nSegCtr = nSegCtr + 1

      If !Empty(Alltrim(m.shipfor))
        Store "N1"+cfd+"Z7"+cfd+Alltrim(m.shipfor)+cfd+"92"+cfd+Alltrim(m.sforstore)+csegd To cString
        Do cstringbreak
        nSegCtr = nSegCtr + 1
        If Empty(M.ADDRESS2)
          Store "N3"+cfd+Alltrim(M.SFORADDR1)+csegd To cString
        Else
          Store "N3"+cfd+Alltrim(M.SFORADDR1)+cfd+Alltrim(M.SFORADDR2)+csegd To cString
        Endif
        Do cstringbreak
        nSegCtr = nSegCtr + 1
        If !Empty(cSForState)
          Store "N4"+cfd+cSForCity+cfd+cSForState+cfd+cSForZip+cfd+"US"+csegd To cString
        Else
          Store "N4"+cfd+cSForCity+csegd To cString
        Endif
        Do cstringbreak
        nSegCtr = nSegCtr + 1
      Endif

      Store "N9"+cfd+"MB"+cfd+Alltrim(cMBOL)+csegd To cString
      Do cstringbreak
      nSegCtr = nSegCtr + 1

      If lParcelType
        Do Case
        Case outship.terms = "BS"
          cBilling = "PP"
        Case outship.terms = "BTP"
          cBilling = "TP"
        Case outship.terms = "BR"
          cBilling = "CC"
        Otherwise
          cBilling = "PP"
        Endcase
        Store "W27"+cfd+"M"+cfd+Alltrim(outship.scac)+cfd+Alltrim(carrcode)+cfd+Alltrim(cBilling)+csegd To cString
        Do cstringbreak
        nSegCtr = nSegCtr + 1
      Endif


*************************************************************************
*2  DETAIL LOOP - MATCHING SHIP_REF FIELD (FROM OUTSHIP)
*************************************************************************
*    ASSERT .F. MESSAGE "In Detail Loop"
      Store "LB" To cWeightUnit
      Wait Clear
      Wait Window "Now creating Detail information "+cShip_ref Nowait Noclear
      Select vbbcpp
      Locate
      Select outdet
      Set Order To Tag outdetid

      If cBOL = cRefBOL
        Set Deleted Off
      Else
        Set Deleted On
      Endif
      Select vbbcpp
      Set Relation To outdetid Into outdet

      Locate
      Locate For ship_ref = Trim(cShip_ref) And outshipid = nOutshipid
      If !Found()
        If !lTesting
          Wait Window "PT "+cShip_ref+" NOT FOUND in vbbcpp...ABORTING" Timeout 2
          If !lTesting
            lSQLMail = .T.
          Endif
          Do ediupdate With "MISS PT-SQL: "+cShip_ref,.T.
          Throw
        Else
          Loop
        Endif
      Endif
***********************  common code with base 945 prg ****************************8
&& OK here we grab the carton data from Cartons for this pick ticket

&& we also need to grab the UPC/Line# data so wee can maintain outbound lineto UPC
      Set Step On

      Do setup_upc_line_data With cShip_ref

      scanstr = "vbbcpp.ship_ref = cShip_ref and vbbcpp.outshipid = nOutshipid"
      Select vbbcpp
      Locate
      Locate For &scanstr
      cCartonNum= "XXX"

      Do While &scanstr
        nUnitsum  = outship.qty
        nTotCtnWt = outship.weight
        lDoManSegment = .T.
        lDoPALSegment = .F.
        llPrepackSpecial = .F.

        If vbbcpp.loccode ="PREPACK"
          PrepackUPC =vbbcpp.mupc
          Select outdet
          PrepackStyle = outdet.Style

          llPrepackSpecial = .T.

** OK parse both arrays, need to get the linenum from out ship
** need to get the prepacksize and prepackcolor from the correct prepack grouping in printstuff

          alengthos = Alines(ashipins,outship.shipins,.T.,Chr(13))
          alengthod = Alines(aprintstuff,outdet.printstuff,.T.,Chr(13))
          izzz=1
          done = .F.
          Do While !done
            If Alltrim(PrepackUPC)$ashipins[izzz]
              done = .T.
            Else
              izzz=izzz+1
            Endif
            If izzz >= alengthos
              done = .T.
            Endif
          Enddo
          izzz=izzz+3
          PrepackLine  = Substr(ashipins[izzz],Atc("*",ashipins[izzz])+1)

          izzz=1
          done = .F.
          Do While !done
            If Alltrim(PrepackUPC)$aprintstuff[izzz]
              done = .T.
            Else
              izzz=izzz+1
            Endif
            If izzz >= alengthod
              done = .T.
              PrepackSize="NA"
              PrepackColor="NA"
            Endif
          Enddo

          done =.F.
          Do While !done
            If "PREPACKSIZE"$aprintstuff[izzz]
              done = .T.
            Else
              izzz=izzz+1
            Endif
          Enddo
          PrepackSize  = Substr(aprintstuff[izzz],Atc("*",aprintstuff[izzz])+1)
          izzz=izzz+1
          PrepackColor = Substr(aprintstuff[izzz],Atc("*",aprintstuff[izzz])+1)
        Else
          llPrepackSpecial = .F.
        Endif

        lSkipBack = .T.

        If Trim(vbbcpp.ucc) <> cCartonNum
          Store Trim(vbbcpp.ucc) To cCartonNum
        Endif

        Store "LX"+cfd+Alltrim(Str(nCtnNumber))+csegd To cString   && Carton Seq. # (up to Carton total)
        Do cstringbreak
        nSegCtr = nSegCtr + 1

        Do While vbbcpp.ucc = cCartonNum
          If vbbcpp.totqty < 1
            Skip 1 In vbbcpp
            Loop
          Endif

          cUCCNumber = vbbcpp.ucc
          If Empty(cUCCNumber) Or Isnull(cUCCNumber)
            Wait Clear
            Wait Window "Empty UCC Number in vbbcpp "+cShip_ref Timeout 1
            lSQLMail = .T.
            Do ediupdate With "EMPTY UCC# in "+cShip_ref,.T.
            Throw
          Endif
          cUCCNumber = Trim(Strtran(Trim(cUCCNumber)," ",""))

          If lDoManSegment
            nCtnNumber = nCtnNumber + 1
            lDoManSegment = .F.

            If lTesting
              cTrkNumber = ""
            Else
              Select package1
              Locate For  package1.ucc = cUCCNumber
              If Found()
                cTrkNumber = Alltrim(package1.trknumber)
              Else
                cTrkNumber = ""
              Endif
            Endif

            Store "MAN"+cfd+"GM"+cfd+Trim(cUCCNumber)+cfd+cfd+"CP"+cfd+cTrkNumber+csegd To cString
            Do cstringbreak
            nSegCtr = nSegCtr + 1
            nTotCtnCount = nTotCtnCount + 1
          Endif

          If llPrepackSpecial = .T.
*           Do While vbbcpp.ucc = cUCCNumber  And !Eof("vbbcpp")
            lcThisUCC = cUCCNumber

            Store "W12"+cfd+"CL"+cfd+"1"+cfd+"1"+cfd+cfd+"CA"+cfd+Trim(PrepackUPC)+csegd To cString
            Do cstringbreak
            nSegCtr = nSegCtr + 1

            Store "N9"+cfd+"LI"+cfd+PrepackLine+csegd To cString
            Do cstringbreak
            nSegCtr = nSegCtr + 1

            Store "N9"+cfd+"IX"+cfd+PrepackStyle+csegd To cString
            Do cstringbreak
            nSegCtr = nSegCtr + 1

            Store "N9"+cfd+"CO"+cfd+Alltrim(PrepackColor)+csegd To cString
            Do cstringbreak
            nSegCtr = nSegCtr + 1

            Store "N9"+cfd+"IZ"+cfd+Alltrim(PrepackSize)+csegd To cString
            Do cstringbreak
            nSegCtr = nSegCtr + 1
            Select vbbcpp
            Do While vbbcpp.ucc = cUCCNumber  And !Eof("vbbcpp")
              Skip 1 In vbbcpp
            Enddo

            Loop
          Else
*************************************************************************************************
** OK here we are NOT aprepack but must get the correct line number from the UPCDATA array

            If Empty(outdet.ctnwt) Or outdet.ctnwt = 0
              cCtnWt = Alltrim(Str(Int(outship.weight/outship.ctnqty)))
              If (Empty(cCtnWt) Or Int(Val(cCtnWt)) = 0) And outdet.totqty >0
                If lTesting
                  cCtnWt = '5'
                Else
                  Do ediupdate With "WEIGHT ERR: PT "+cShip_ref,.T.
                  Throw
                Endif
              Endif
              nTotCtnWt = nTotCtnWt + Int(Val(cCtnWt))
            Else
              Store Alltrim(Str(outdet.ctnwt)) To cCtnWt
              nTotCtnWt = nTotCtnWt + outdet.ctnwt
            Endif
          Endif

          cStyle    = Trim(vbbcpp.Style)
          cColor    = Trim(vbbcpp.Color)
          cSize     = Trim(vbbcpp.Id)
          cUPC      = Trim(vbbcpp.upc)
          lnShipQty = vbbcpp.totqty
          cItemNum  = Trim(outdet.custsku)
          nODID = outdet.outdetid
          cUnitCode = "EA"
          nShipStat = "CL"

** OK write out an item loop, keep track of items per line number in UPCDATA
*aaa

          lnThisUPC_QTY = vbbcpp.totqty

          Do While lnThisUPC_QTY >0

            Select upcdata
            Go Top
            Locate For Alltrim(upc) = Alltrim(cUPC)  And upcdata.remainqty > 0
            If Found("upcdata")
              Do Case
              Case upcdata.remainqty = lnThisUPC_QTY
                cLineNum  = upcdata.linenum
                nOrigQty  = upcdata.origqty
                nShipQty  = lnThisUPC_QTY
                lnThisUPC_QTY =0
                Replace upcdata.remainqty With 0 In upcdata

              Case upcdata.remainqty > lnThisUPC_QTY
                cLineNum = upcdata.linenum
                nOrigQty = upcdata.origqty
                nShipQty = lnThisUPC_QTY
                Replace upcdata.remainqty With upcdata.remainqty - lnThisUPC_QTY In upcdata
                lnThisUPC_QTY =0

              Case upcdata.remainqty < lnThisUPC_QTY
*set step On
                cLineNum  = upcdata.linenum
                nOrigQty  = upcdata.origqty
                lnThisUPC_QTY =lnThisUPC_QTY - upcdata.remainqty
                nShipQty = upcdata.remainqty
                Replace upcdata.remainqty With 0 In upcdata
              Endcase
            Else


            Endif

            Store "W12"+cfd+nShipStat+cfd+Alltrim(Str(nOrigQty))+cfd+Alltrim(Str(nShipQty))+cfd+cfd+cUnitCode+cfd+Trim(cUPC)+csegd To cString
            Do cstringbreak
            nSegCtr = nSegCtr + 1

            Store "N9"+cfd+"LI"+cfd+cLineNum+csegd To cString
            Do cstringbreak
            nSegCtr = nSegCtr + 1

            Store "N9"+cfd+"IX"+cfd+cStyle+csegd To cString
            Do cstringbreak
            nSegCtr = nSegCtr + 1

            Store "N9"+cfd+"CO"+cfd+Alltrim(outdet.Color)+csegd To cString
            Do cstringbreak
            nSegCtr = nSegCtr + 1

            Store "N9"+cfd+"IZ"+cfd+Alltrim(outdet.Id)+csegd To cString
            Do cstringbreak
            nSegCtr = nSegCtr + 1

          Enddo
*set step on

          Skip 1 In vbbcpp
        Enddo

        lSkipBack = .T.

*      ASSERT .F. MESSAGE "At end of detail looping-debug"
      Enddo


*********** end of common 945 base code  ***************************************88

*************************************************************************
*2^  END DETAIL MATCHING SHIP_REF LOOP
*************************************************************************
      Wait Clear
      Wait Window "Now creating Section Closure information" Nowait Noclear
      Store "W03"+cfd+Alltrim(Str(nUnitsum))+cfd+Alltrim(Str(nTotCtnWt))+cfd+;
      cWeightUnit+cfd+Alltrim(Str(outship.cuft,10,2))+cfd+"CF"+cfd+Alltrim(Str(nTotCtnCount))+cfd+"CT"+csegd To cString   && Units sum, Weight sum, carton count

      nTotCtnWt = 0
      nTotCtnCount = 0
      nUnitsum = 0
      Do cstringbreak

      Store  "SE"+cfd+Alltrim(Str(nSegCtr+2))+cfd+Padl(c_GrpCntrlNum,9,"0")+csegd To cString
      Do cstringbreak

      Select bldet
      Wait Clear

      If lTesting
        Set Step On
      Endif
    Endscan
    Assert .F. Message "After sub-bill det scan"


*************************************************************************
*1^  END OUTSHIP MAIN LOOP
*************************************************************************

    Do close945
    =Fclose(nFilenum)

    nSubRec = nSubRec+1
*!* Create eMail confirmation message
    tsubject = cMailName+" 945 EDI File from TGF as of "+dtmail+" ("+cCustLoc+")"
    tattach = " "
    tmessage = "945 EDI Info from TGF, file "+cFilenameShort+Chr(13)
    tmessage = tmessage + "Division "+cDivision+", MBOL# "+cMBOL+", BOL# "+Trim(cBOL)+Chr(13)
    tmessage = tmessage + "File "+Alltrim(Str(nSubRec))+" of "+Alltrim(Str(nSubBillCnt))+Chr(13)
    tmessage = tmessage + "For Work Orders: "+cWO_NumStr+","+Chr(13)
    tmessage = tmessage + "containing these "+Transform(nPTCount)+" picktickets:"+Chr(13)
    tmessage = tmessage + cPTString + Chr(13)
    tmessage = tmessage +"has been created and will be transmitted ASAP."+Chr(13)+Chr(13)
    If !Empty(cMissDel)
      tmessage = tmessage+Chr(13)+Chr(13)+cMissDel+Chr(13)+Chr(13)
    Endif
    tmessage = tmessage + "If you have any questions, please eMail or call 732-750-9000 x217."

    If lEmail
      Do Form m:\dev\frm\dartmail2 With tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
    Endif
    nPTCount = 0
    cPTString = ""

    Wait Clear
    Wait Window cMailName+" 945 EDI File sub-BOL output complete" Timeout 1

*  ENDSCAN  && scan for all the sub bols

********  Joe look from here on,  I had to move code below out of the MBOL loop/scan as only one file would be created.........


*!* Transfers files to correct output folders
**    COPY FILE [&cFilenameHold] TO [&cFilenameArch]
    oFSO=Createobject("Scripting.FileSystemObject")
    oFSO.CopyFile("&cFilenameHold","&cFilenameArch",.T.)
    If lBBCFilesOut And !lTesting
      oFSO=Createobject("Scripting.FileSystemObject")
      oFSO.CopyFile("&cFilenameHold","&cFilenameOUT",.T.)
*      COPY FILE [&cFilenameHold] TO [&cFilenameOut]
      Delete File [&cFilenameHold]
      Select temp945
      Copy To "f:\3pl\data\temp945bbc.dbf"
      Use In temp945
      Select 0
      Use "f:\3pl\data\temp945bbc.dbf" Alias temp945bbc
      Select 0
      Use "f:\3pl\data\pts_sent945.dbf" Alias pts_sent945
      Append From "f:\3pl\data\temp945bbc.dbf"
      Use In pts_sent945
      Use In temp945bbc
      Delete File "f:\3pl\data\temp945bbc.dbf"
    Endif

    Assert .F. Message "At end of sub-bill main scan"

    If !lTesting
      asn_out_data()
    Endif
    cWO_NumList = ""
  Endscan
  Release All Like c_CntrlNum,c_GrpCntrlNum

*  ASSERT .F. MESSAGE "At end of main loop...debug here"
  If !lTesting
    Select edi_trigger
    Do ediupdate With "945 CREATED",.F.
    Select edi_trigger
    Locate

    If !Used("ftpedilog")
      Select 0
      Use F:\edirouting\ftpedilog Alias ftpedilog
      Insert Into ftpedilog (TRANSFER,ftpdate,filename,acct_name,Type) Values ("945-"+ccustname+"-"+cCustLoc,dt2,cFilenameHold,Upper(ccustname),"945")
      Use In ftpedilog
    Endif
  Endif

  Wait Clear
  lDoCatch = .F.

Catch To oErr
  If lDoCatch
    Assert .F. Message "In error CATCH..."
    Set Step On
    lEmail = .F.
    Do ediupdate With Alltrim(oErr.Message),.T.
    tsubject = ccustname+" Error ("+Transform(oErr.ErrorNo)+") at "+Ttoc(Datetime())
    tattach  = ""
    tsendto  = "pgaidis@fmiint.com"&&tsendtoerr
    tcc = tccerr

    tmessage = ccustname+" Error processing "+Chr(13)

    tmessage =tmessage+ "Try/CATCH Exeception Message:"+Chr(13)+;
    [  Error: ] + Str(oErr.ErrorNo) +Chr(13)+;
    [  LineNo: ] + Str(oErr.Lineno) +Chr(13)+;
    [  Message: ] + oErr.Message +Chr(13)+;
    [  Procedure: ] + oErr.Procedure +Chr(13)+;
    [  Details: ] + oErr.Details +Chr(13)+;
    [  StackLevel: ] + Str(oErr.StackLevel) +Chr(13)+;
    [  LineContents: ] + oErr.LineContents+Chr(13)+;
    [  UserValue: ] + oErr.UserValue
    tmessage = tmessage + Chr(13)+"PROGRAM: "+cProgname
    If !Empty(cStatus)
      tmessage = tmessage + Chr(13)+"Error: "+cStatus
    Endif
    tsubject = "945 EDI Poller Error at "+Ttoc(Datetime())
    tattach  = ""
    tfrom    ="TGF EDI 945 Poller Operations <transload-ops@tollgroup.com>"
    Do Form m:\dev\frm\dartmail2 With tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
  Endif
Endtry

*** END OF CODE BODY

****************************
Procedure close945
****************************
** Footer Creation

Store  "GE"+cfd+Alltrim(Str(nSTCount))+cfd+c_CntrlNum+csegd To cString
Do cstringbreak

Store  "IEA"+cfd+"1"+cfd+Padl(c_CntrlNum,9,"0")+csegd To cString
Do cstringbreak
Return

****************************
Procedure num_incr_isa
****************************

If !Used("serfile")
  Use ("f:\3pl\data\serial\"+ccustname+"945_serial") In 0 Alias serfile
Endif
Select serfile
If lTesting And lISAFlag
  Assert .F. Message "At group seqnum...debug"
  nOrigSeq = serfile.seqnum
  lISAFlag = .F.
Endif
nISA_Num = serfile.seqnum
c_CntrlNum = Alltrim(Str(serfile.seqnum))
Replace serfile.seqnum With serfile.seqnum + 1 In serfile
Select outship
Return

****************************
Procedure num_incr_st
****************************

If !Used("serfile")
  Use ("f:\3pl\data\serial\"+ccustname+""945_serial") IN 0 ALIAS serfile
Endif
Select serfile
If lTesting And lSTFlag
  nOrigGrpSeq = serfile.grpseqnum
  lSTFlag = .F.
Endif
c_GrpCntrlNum = Alltrim(Str(serfile.grpseqnum))
Replace serfile.grpseqnum With serfile.grpseqnum + 1 In serfile
*  SELECT outship
Return


****************************
Procedure segmentget
****************************

Parameter thisarray,lcKey,nLength

For i = 1 To nLength
  If i > nLength
    Exit
  Endif
  lnEnd= At("*",thisarray[i])
  If lnEnd > 0
    lcThisKey =Trim(Substr(thisarray[i],1,lnEnd-1))
    If Occurs(lcKey,lcThisKey)>0
      Return Substr(thisarray[i],lnEnd+1)
      i = 1
    Endif
  Endif
Endfor

Return ""

****************************
Procedure ediupdate
****************************
Parameter cStatus,lIsError
*  lDoCatch = IIF(lIsError,.T.,.F.)
lDoCatch = .F.
Set Step On

If !lTesting
  Select edi_trigger
  If !lIsError
    Replace processed With .T.,proc945 With .T.,file945 With cFilenameHold,isa_num With cISA_Num,;
    fin_status With "945 CREATED",errorflag With .F.,when_proc With Datetime() ;
    FOR edi_trigger.bol = cMBOL And Inlist(accountid,&gbbcaccounts)
*!*        xsqlexec("update edi_trigger set processed=1, proc945=1, file945='"+cFilenameHold+"',isa_num='"+cISA_Num+"' fin_status='945 CREATED', errorflag=0, when_proc={"+TTOC(DATETIME())+"} where bol='"+cMBOL+"' and accountid in ("+gbbcaccounts+")",,,"stuff")
  Else
    Replace processed With .T.,proc945 With .F.,file945 With "",;
    fin_status With cStatus,errorflag With .T. ;
    FOR edi_trigger.bol = cMBOL And Inlist(accountid,&gbbcaccounts)
*!*        xsqlexec("update edi_trigger set processed=1, proc945=0, file945=' ', fin_status='"+cStatus+"', errorflag=1 where bol='"+cMBOL+"' and accountid in ("+gbbcaccounts+")",,,"stuff")
    If lCloseOutput
      =Fclose(nFilenum)
      Cd [&cHoldpath]
      Delete File *.*
    Endif
  Endif
Endif


If lIsError And lEmail && AND cStatus # "SQL ERROR"
  tsubject = "945 Error in BBC BOL "+Trim(cBOL)+"(At PT "+cShip_ref+")"
  tattach = " "
  tsendto = Alltrim(tsendtoerr)
  tcc = Alltrim(tccerr)
  tmessage = "945 Processing for BOL# "+cBOL+", WO# "+cWO_Num+" produced this error: (Office: "+cOffice+"): "+cStatus+Chr(13)+"Check EDI_TRIGGER and re-run"
  If "TOTQTY ERR"$cStatus
    tmessage = tmessage + Chr(13) + "At OUTSHIPID: "+Alltrim(Str(m.outshipid))
  Endif
  Do Form m:\dev\frm\dartmail2 With tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
Endif

If Used('outship')
  Use In outship
Endif
If Used('outdet')
  Use In outdet
Endif
If Used('shipment')
  Use In shipment
Endif
If Used('package')
  Use In package
Endif

If Used('serfile')
  Select serfile
  If lTesting
    If nOrigSeq > 0
      Replace serfile.seqnum With nOrigSeq
    Endif
    If nOrigGrpSeq > 0
      Replace serfile.grpseqnum With nOrigGrpSeq
    Endif
  Endif
  Use In serfile
Endif
If Used('scacs')
  Use In scacs
Endif
If Used('mm')
  Use In mm
Endif
If Used('tempx')
  Use In tempx
Endif
If Used('vbbcpp')
  Use In vbbcpp
Endif

If !lTesting
  Select edi_trigger
  Locate
Endif
If lIsError
Endif
Return

****************************
Procedure cstringbreak
****************************
cLen = Len(Alltrim(cString))

Try
  Fputs(nFilenum,cString)
Catch To oErr1
  Set Step On
  tmessage = ccustname+" Error processing "+Chr(13)

  tmessage =tmessage+ "Try/CATCH Exeception Message:"+Chr(13)+;
  [  Error: ] + Str(oErr.ErrorNo) +Chr(13)+;
  [  LineNo: ] + Str(oErr.Lineno) +Chr(13)+;
  [  Message: ] + oErr.Message +Chr(13)+;
  [  Procedure: ] + oErr.Procedure +Chr(13)+;
  [  Details: ] + oErr.Details +Chr(13)+;
  [  StackLevel: ] + Str(oErr.StackLevel) +Chr(13)+;
  [  LineContents: ] + oErr.LineContents+Chr(13)+;
  [  UserValue: ] + oErr.UserValue
Endtry
*  FWRITE(nFilenum,cString)

Return
************************************************************************************
Procedure setup_upc_line_data
************************************************************************************
Parameters xship_ref

Create Cursor upcdata (;
ship_ref Char(20),;
upc Char(12),;
STYLE Char(20),;
COLOR Char(10),;
ID Char(10),;
origqty  N(6),;
linenum  Char(15),;
remainqty N(6))

xsqlexec("select * from outship where ship_ref='"+xship_ref+"'","outshipx",,"wh")
xsqlexec("select * from outdet where outshipid = "+Transform(outshipx.outshipid),"outdetx",,"wh")

Select outdetx
Go Top

Scan
  alength= Alines(a,printstuff)
  done = .F.
  izzz=1
  Do While !done
    If "OPENUPC"$a[izzz]
      lnUPCStart=Atc("*",a[izzz])+1
      lnUPCEnd  =Atc("*",a[izzz],2)
      lnUPCLength = lnUPCEnd-lnUPCStart
      m.upc = Substr(a[izzz],lnUPCStart,lnUPCLength)
      lnLineNumStart = Atc("*",a[izzz],2)+1
      lnLineNumEnd   = Atc("*",a[izzz],3)
      lnLineNumLength = lnLineNumEnd-lnLineNumStart
      m.linenum = Substr(a[izzz],lnLineNumStart,lnLineNumLength)
      lnQtyStart =Atc("*",a[izzz],3)+1
      m.qty = Substr(a[izzz],lnQtyStart)
      Insert Into upcdata(ship_ref,upc,Style,Color,Id,linenum,origqty,remainqty) Values (xship_ref,m.upc,outdetx.Style,outdetx.Color,outdetx.Id, m.linenum,Val(m.qty),Val(m.qty))
    Endif
    izzz=izzz+1
    If izzz > alength
      done = .T.
    Endif
  Enddo
Endscan

Endproc
************************************************************************************
