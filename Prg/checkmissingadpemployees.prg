* List employees who we would expect to be in export from kronos to ADP Payroll, but are not,
* presumably because they had no hours in the prior period.

* This is basically a double-check that employees whose time is entered manually, like TN1 folks,
* has actually been entered.

** Must be run after the Kronos ADP Payroll export, since it will open that spreadsheet.

* Build EXE as F:\UTIL\Kronos\checkmissingadpemployees.exe

runack("CHECKMISSINGADPEMPLOYEES")

LOCAL loCheckMissingKronosEmployees
loCheckMissingKronosEmployees = CREATEOBJECT('CheckMissingKronosEmployees')
loCheckMissingKronosEmployees.MAIN()
CLOSE DATABASES ALL
RETURN

#DEFINE LOGIT 1
#DEFINE WAITIT 2
#DEFINE NOWAITIT 4
#DEFINE SENDIT 8
#DEFINE CRLF CHR(13) + CHR(10)
#DEFINE RETURN_DATA_MANDATORY .F.
#DEFINE RETURN_DATA_NOT_MANDATORY .T.

DEFINE CLASS CheckMissingKronosEmployees AS CUSTOM

	cProcessName = 'CheckMissingKronosEmployees'

	lTestMode = .F.  && .F. FOR NORMAL OPERATION, .T. TO SUPPRESS ALL BUT DEVELOPER EMAILS AND CHANGE INPUT TABLE, ETC.

	lAutoYield = .T.

	cStartTime = TTOC(DATETIME())

	* connection properties
	nSQLHandle = 0

	* file properties
	nFileHandle = 0

	* date properties
	dAsOfDate = DATE()
	dtNow = DATETIME()
	dToday = DATE()
	cToday = DTOC(DATE())

	* wait window properties
	nWaitWindowTimeout = 2
	lWaitWindowIsOn = .T.

	* logfile properties
	lLoggingIsOn = .T.
	cLogFile = 'F:\UTIL\KRONOS\Logfiles\CheckMissingKronosEmployees_log.txt'

	* INTERNAL email properties
	lSendInternalEmailIsOn = .T.
	cFrom = 'TGF Corporate <fmicorporate@fmiint.com>'
	*cSendTo = 'Mark Bennett <mbennett@fmiint.com>'
	cSendTo = 'lwaldrip@fmiint.com'
	cCC = 'mariedesaye@fmiint.com, lauren.klaver@tollgroup.com, mbennett@fmiint.com'
	cSubject = 'CHECK MISSING KRONOS EMPLOYEES for: ' + TTOC(DATETIME())
	cAttach = ''
	cBodyText = ''

	cCOMPUTERNAME = ''
	cUSERNAME = ''

	FUNCTION INIT
		IF NOT DODEFAULT()
			RETURN .F.
		ENDIF
		WITH THIS
			CLEAR
			*SET RESOURCE OFF
			CLOSE DATA
			SET CENTURY ON
			SET DATE YMD
			SET HOURS TO 24
			SET ANSI ON
			SET TALK OFF
			SET DELETED ON
			SET CONSOLE OFF
			SET EXCLUSIVE OFF
			SET SAFETY OFF
			SET EXACT OFF
			SET STATUS BAR ON
			SET SYSMENU OFF
			SET ENGINEBEHAVIOR 70
			_VFP.AUTOYIELD = .lAutoYield
			.cCOMPUTERNAME = GETENV("COMPUTERNAME")
			.cUSERNAME = GETENV("USERNAME")
			IF .lTestMode THEN
				.cSendTo = 'Mark Bennett <mbennett@fmiint.com>'
				.cCC = ''
				.cLogFile = 'F:\UTIL\KRONOS\Logfiles\CheckMissingKronosEmployees_log_TESTMODE.txt'
			ENDIF
			.lLoggingIsOn = .lLoggingIsOn AND NOT EMPTY(.cLogFile)
			IF .lLoggingIsOn THEN
				SET ALTERNATE TO (.cLogFile) ADDITIVE
				SET ALTERNATE ON
			ENDIF
			* re-init date properties after all SETTINGS
			.dtNow = DATETIME()
			.dToday = DATE()
			.cToday = DTOC(DATE())
		ENDWITH
	ENDFUNC


	FUNCTION DESTROY
		WITH THIS
			IF .lLoggingIsOn  THEN
				SET ALTERNATE OFF
				SET ALTERNATE TO
			ENDIF
		ENDWITH
		DODEFAULT()
	ENDFUNC


	FUNCTION MAIN
		WITH THIS
			LOCAL lnNumberOfErrors, lcSQL, lcADPSQL, loError, lcTopBodyText
			LOCAL oExcel, oWorkbook, oWorksheet, lnRow, lcRow, luValue
			LOCAL lcTS_EffectiveFrom, lcTS_EffectiveTo

			TRY
				lnNumberOfErrors = 0
				lcTopBodyText = ''

				.TrackProgress('', LOGIT)
				.TrackProgress('==================================================================================================================', LOGIT+SENDIT)
				.TrackProgress('CHECK MISSING KRONOS EMPLOYEES process started....', LOGIT+SENDIT+NOWAITIT)
				.TrackProgress('COMPUTERNAME = ' + .cCOMPUTERNAME, LOGIT+SENDIT)
				.TrackProgress('USERNAME = ' + .cUSERNAME, LOGIT+SENDIT)
				.TrackProgress('PROJECT = CHECKMISSINGADPEMPLOYEES', LOGIT+SENDIT)
				IF .lTestMode THEN
					.TrackProgress('                        TEST MODE                        ', LOGIT+SENDIT)
				ENDIF

				lcTS_EffectiveFrom   = "{ts '" + TRANSFORM(YEAR(.dAsOfDate)) + "-" + PADL(MONTH(.dAsOfDate),2,"0") + "-" + PADL(DAY(.dAsOfDate),2,"0") + " 00:00:00.0'}"
				lcTS_EffectiveTo = "{ts '" + TRANSFORM(YEAR(.dAsOfDate)) + "-" + PADL(MONTH(.dAsOfDate),2,"0") + "-" + PADL(DAY(.dAsOfDate),2,"0") + " 23:59:59.9'}"

				* get ss#s from ADP
				OPEN DATABASE F:\UTIL\ADPCONNECTION\CONNECT1

				.nSQLHandle = SQLCONNECT("PCPAYWIN","MBENNETT","VP737A")

				IF .nSQLHandle > 0 THEN

*!*						lcADPSQL = ;
*!*							"SELECT " + ;
*!*							" {fn IFNULL(FILE#,0000)} AS FILE_NUM " + ;
*!*							" FROM REPORTS.V_EMPLOYEE " + ;
*!*							" WHERE STATUS <> 'T' "

					lcADPSQL = ;
						"SELECT " + ;
						" {fn IFNULL(FILE#,0000)} AS FILE_NUM " + ;
						" FROM REPORTS.V_EMPLOYEE " + ;
						" WHERE STATUS = 'A' " + ;
						" AND COMPANYCODE IN ('E87','E88','E89') "

					IF .lTestMode THEN
						.TrackProgress('lcADPSQL = ' + lcADPSQL, LOGIT+SENDIT)
					ENDIF

					IF USED('CURADPACTIVEEMPSPRE') THEN
						USE IN CURADPACTIVEEMPSPRE
					ENDIF
					IF USED('CURADPACTIVEEMPS') THEN
						USE IN CURADPACTIVEEMPS
					ENDIF

					IF .ExecSQL(lcADPSQL, 'CURADPACTIVEEMPSPRE', RETURN_DATA_MANDATORY) THEN

						SELECT ;
							INT(FILE_NUM) AS FILE_NUM ;
							FROM CURADPACTIVEEMPSPRE ;
							INTO CURSOR CURADPACTIVEEMPS ;
							ORDER BY 1

						*!*		SELECT CURADPACTIVEEMPS
						*!*		BROWSE

						* main SQL
						SET TEXTMERGE ON

*!*	TEXT TO lcSQL

*!*	SELECT     PE.PERSONID, PE.PERSONNUM, PE.FULLNM AS PERSONFULLNAME,
*!*	      CASE WHEN (ISNULL(LA1.LABORLEV1NM, '') + ISNULL(LA1.LABORLEV2NM, '') + ISNULL(LA1.LABORLEV3NM, '')
*!*	      + ISNULL(LA1.LABORLEV4NM, '') + ISNULL(LA1.LABORLEV5NM, '') + ISNULL(LA1.LABORLEV6NM, '') + ISNULL(LA1.LABORLEV7NM, '') = '') THEN NULL
*!*	      ELSE CAST(ISNULL(LA1.LABORLEV1NM, '') AS VARCHAR(32)) + '/' + CAST(ISNULL(LA1.LABORLEV2NM, '') AS VARCHAR(32))
*!*	      + '/' + CAST(ISNULL(LA1.LABORLEV3NM, '') AS VARCHAR(32)) + '/' + CAST(ISNULL(LA1.LABORLEV4NM, '') AS VARCHAR(32))
*!*	      + '/' + CAST(ISNULL(LA1.LABORLEV5NM, '') AS VARCHAR(32)) + '/' + CAST(ISNULL(LA1.LABORLEV6NM, '') AS VARCHAR(32))
*!*	      + '/' + CAST(ISNULL(LA1.LABORLEV7NM, '') AS VARCHAR(32)) END AS HOMELABORACCTNAME, LA1.LABORLEV1NM AS HOMELABORLEVELNM1,
*!*	      LA1.LABORLEV2NM AS HOMELABORLEVELNM2, LA1.LABORLEV3NM AS HOMELABORLEVELNM3, LA1.LABORLEV4NM AS HOMELABORLEVELNM4,
*!*	      LA1.LABORLEV5NM AS HOMELABORLEVELNM5, LA1.LABORLEV6NM AS HOMELABORLEVELNM6, LA1.LABORLEV7NM AS HOMELABORLEVELNM7,
*!*	      CASE WHEN (ISNULL(LA1.LABORLEV1DSC, '') + ISNULL(LA1.LABORLEV2DSC, '') + ISNULL(LA1.LABORLEV3DSC, '') + ISNULL(LA1.LABORLEV4DSC, '')
*!*	      + ISNULL(LA1.LABORLEV5DSC, '') + ISNULL(LA1.LABORLEV6DSC, '') + ISNULL(LA1.LABORLEV7DSC, '') = '') THEN NULL
*!*	      ELSE CAST(ISNULL(LA1.LABORLEV1DSC, '') AS VARCHAR(32)) + '/' + CAST(ISNULL(LA1.LABORLEV2DSC, '') AS VARCHAR(32))
*!*	      + '/' + CAST(ISNULL(LA1.LABORLEV3DSC, '') AS VARCHAR(32)) + '/' + CAST(ISNULL(LA1.LABORLEV4DSC, '') AS VARCHAR(32))
*!*	      + '/' + CAST(ISNULL(LA1.LABORLEV5DSC, '') AS VARCHAR(32)) + '/' + CAST(ISNULL(LA1.LABORLEV6DSC, '') AS VARCHAR(32))
*!*	      + '/' + CAST(ISNULL(LA1.LABORLEV7DSC, '') AS VARCHAR(32)) END AS HOMELABORACCTDSC, LA1.LABORLEV1DSC AS HOMELABORLEVELDSC1,
*!*	      LA1.LABORLEV2DSC AS HOMELABORLEVELDSC2, LA1.LABORLEV3DSC AS HOMELABORLEVELDSC3,
*!*	      LA1.LABORLEV4DSC AS HOMELABORLEVELDSC4, LA1.LABORLEV5DSC AS HOMELABORLEVELDSC5,
*!*	      LA1.LABORLEV6DSC AS HOMELABORLEVELDSC6, LA1.LABORLEV7DSC AS HOMELABORLEVELDSC7
*!*	FROM         PERSON PE LEFT OUTER JOIN
*!*	      POSTALADDRESS PA1 ON (PE.PERSONID = PA1.PERSONID AND PA1.CONTACTTYPEID = 4) LEFT OUTER JOIN
*!*	      POSTALADDRESS PA2 ON (PE.PERSONID = PA2.PERSONID AND PA2.CONTACTTYPEID = 5) LEFT OUTER JOIN
*!*	      PHONENUMBER PN1 ON (PE.PERSONID = PN1.PERSONID AND PN1.CONTACTTYPEID = 1) LEFT OUTER JOIN
*!*	      PHONENUMBER PN2 ON (PE.PERSONID = PN2.PERSONID AND PN2.CONTACTTYPEID = 2) LEFT OUTER JOIN
*!*	      PHONENUMBER PN3 ON (PE.PERSONID = PN3.PERSONID AND PN3.CONTACTTYPEID = 3) LEFT OUTER JOIN
*!*	      CONTACTTYPE CT1 ON (PN1.CONTACTTYPEID = CT1.CONTACTTYPEID) LEFT OUTER JOIN
*!*	      CONTACTTYPE CT2 ON (PN2.CONTACTTYPEID = CT2.CONTACTTYPEID) LEFT OUTER JOIN
*!*	      CONTACTTYPE CT3 ON (PN3.CONTACTTYPEID = CT3.CONTACTTYPEID) LEFT OUTER JOIN
*!*	      EMAILADDRESS EM1 ON (PE.PERSONID = EM1.PERSONID AND EM1.CONTACTTYPEID = 4) LEFT OUTER JOIN
*!*	      EMAILADDRESS EM2 ON (PE.PERSONID = EM2.PERSONID AND EM2.CONTACTTYPEID = 5) LEFT OUTER JOIN
*!*	      PERSONSTATUSMM PG ON (PE.PERSONID = PG.PERSONID AND GETDATE() BETWEEN PG.EFFECTIVEDTM AND PG.EXPIRATIONDTM)
*!*	      LEFT OUTER JOIN
*!*	      PERSONSTATUSMM PG2 ON (PE.PERSONID = PG2.PERSONID AND PG2.EXPIRATIONDTM =
*!*	          (SELECT     MAX(EXPIRATIONDTM)
*!*	            FROM          PERSONSTATUSMM PG3
*!*	            WHERE      PG3.PERSONID = PG2.PERSONID AND PG3.EXPIRATIONDTM < getdate() AND PG.EMPLOYMENTSTATID <> PG3.EMPLOYMENTSTATID))
*!*	      LEFT OUTER JOIN
*!*	      PERSONSTATUSMM PG4 ON (PE.PERSONID = PG4.PERSONID AND PG4.EFFECTIVEDTM =
*!*	          (SELECT     MIN(EFFECTIVEDTM)
*!*	            FROM          PERSONSTATUSMM PG5
*!*	            WHERE      PG5.PERSONID = PG4.PERSONID AND NOT EXISTS
*!*	                   (SELECT     'x'
*!*	                     FROM          PERSONSTATUSMM PG6
*!*	                     WHERE      PG5.PERSONID = PG6.PERSONID AND PG6.EXPIRATIONDTM < PG.EXPIRATIONDTM AND
*!*	                                            PG.EMPLOYMENTSTATID <> PG6.EMPLOYMENTSTATID))) LEFT OUTER JOIN
*!*	      EMPLOYMENTSTAT ES ON (PG.EMPLOYMENTSTATID = ES.EMPLOYMENTSTATID) LEFT OUTER JOIN
*!*	      EMPLOYMENTSTAT ES2 ON (PG.USERACCTSTATID = ES2.EMPLOYMENTSTATID) LEFT OUTER JOIN
*!*	      PERSONEXPHRSMM PF1 ON (PE.PERSONID = PF1.PERSONID AND PF1.TIMEPERIODTYPEID = 1) LEFT OUTER JOIN
*!*	      PERSONEXPHRSMM PF2 ON (PE.PERSONID = PF2.PERSONID AND PF2.TIMEPERIODTYPEID = 2) LEFT OUTER JOIN
*!*	      PERSONEXPHRSMM PF5 ON (PE.PERSONID = PF5.PERSONID AND PF5.TIMEPERIODTYPEID = 5) LEFT OUTER JOIN
*!*	      PRSNLICTYPEMM PT1 ON (PE.PERSONID = PT1.PERSONID AND PT1.LICENSETYPEID = 1) LEFT OUTER JOIN
*!*	      PRSNLICTYPEMM PT2 ON (PE.PERSONID = PT2.PERSONID AND PT2.LICENSETYPEID = 2) LEFT OUTER JOIN
*!*	      PRSNLICTYPEMM PT3 ON (PE.PERSONID = PT3.PERSONID AND PT3.LICENSETYPEID = 3) LEFT OUTER JOIN
*!*	      (EFFPRSNACSASGN EPZ LEFT OUTER JOIN
*!*	      ORGSET OS1 ON (EPZ.MGRACCESSOSID = OS1.ORGSETID) LEFT OUTER JOIN
*!*	      ORGSET OS2 ON (EPZ.MGRTRANSFEROSID = OS2.ORGSETID) LEFT OUTER JOIN
*!*	      ORGSET OS3 ON (EPZ.SSETRANSFEROSID = OS3.ORGSETID)) ON (PE.PERSONID = EPZ.PERSONID AND GETDATE() BETWEEN
*!*	      EPZ.EFFECTIVEDTM AND EPZ.EXPIRATIONDTM) LEFT OUTER JOIN
*!*	      (PRSNACCSASSIGN PZ LEFT OUTER JOIN
*!*	      ACCESSPROFILE AP ON (AP.ACCESSPROFID = PZ.ACCESSPROFID) LEFT OUTER JOIN
*!*	      WORKRULEDAP WD1 ON (WD1.WORKRULEDAPID = PZ.MGRWORKRULEDAPID) LEFT OUTER JOIN
*!*	      WORKRULEDAP WD2 ON (WD2.WORKRULEDAPID = PZ.SSEWORKRULEDAPID) LEFT OUTER JOIN
*!*	      PAYCODEDAP PB1 ON (PB1.PAYCODEDAPID = PZ.MGRPAYCODEDAPID) LEFT OUTER JOIN
*!*	      PAYCODEDAP PB2 ON (PB2.PAYCODEDAPID = PZ.SSEPAYCODEDAPID) LEFT OUTER JOIN
*!*	      LABORACCTSET LS1 ON (LS1.LABORACCTSETID = PZ.MGRACCESSLASID) LEFT OUTER JOIN
*!*	      LABORACCTSET LS2 ON (LS2.LABORACCTSETID = PZ.SSETRANSFERLASID) LEFT OUTER JOIN
*!*	      LABORACCTSET LS3 ON (LS3.LABORACCTSETID = PZ.MGRTRANSFERLASID) LEFT OUTER JOIN
*!*	      REPORTDAP RD ON (RD.REPORTDAPID = PZ.REPORTDAPID) LEFT OUTER JOIN
*!*	      TIMEENTRYTYPE TM ON (TM.TIMEENTRYTYPEID = PZ.TIMEENTRYTYPEID) LEFT OUTER JOIN
*!*	      PREFERENCEPROF PH ON (PH.PREFERENCEPROFID = PZ.PREFERENCEPROFID) LEFT OUTER JOIN
*!*	      GROUPSCHEDDAP GSD ON (GSD.GROUPSCHEDDAPID = PZ.GROUPSCHEDDAPID) LEFT OUTER JOIN
*!*	      SCHEDPATTRNDAP SPD ON (SPD.SCHEDPATTRNDAPID = PZ.SCHEDPATTRNDAPID) LEFT OUTER JOIN
*!*	      SHIFTCODEDAP SCD ON (SCD.SHIFTCODEDAPID = PZ.SHIFTCODEDAPID) LEFT OUTER JOIN
*!*	      AVLBLTYPTRNDAP APD ON (APD.AVLBLTYPTRNDAPID = PZ.AVLBLTYPTRNDAPID)) ON (PE.PERSONID = PZ.PERSONID) LEFT OUTER JOIN
*!*	      WTKEMPLOYEE WE ON (PE.PERSONID = WE.PERSONID AND WE.PRIMARYJOBSW = 1) LEFT OUTER JOIN
*!*	      COMBHOMEACCT HA ON (WE.EMPLOYEEID = HA.EMPLOYEEID AND GETDATE() BETWEEN HA.EFFECTIVEDTM AND HA.EXPIRATIONDTM)
*!*	      LEFT OUTER JOIN
*!*	      WFCJOBORG JO ON (HA.WFCJOBORGID = JO.WFCJOBORGID) LEFT JOIN
*!*	      LABORACCT LA1 ON (LA1.LABORACCTID = HA.LABORACCTID) LEFT OUTER JOIN
*!*	      USERACCOUNT UA ON (PE.PERSONID = UA.PERSONID)
*!*	WHERE     PE.PERSONID > 0

*!*	ENDTEXT

TEXT TO lcSQL


SELECT DISTINCT
	P.PERSONNUM, P.FULLNM, L.LABORLEV1NM, L.LABORLEV2NM, L.LABORLEV3NM, L.LABORLEV7NM, L.LABORLEV7DSC
FROM PERSON P
INNER JOIN JAIDS J
ON J.PERSONID = P.PERSONID
INNER JOIN COMBHOMEACCT C
ON C.EMPLOYEEID = J.EMPLOYEEID
INNER JOIN LABORACCT L
ON L.LABORACCTID = C.LABORACCTID
WHERE J.PERSONID > 0
AND J.DELETEDSW = 0
AND C.EFFECTIVEDTM <= <<lcTS_EffectiveTo>>
AND C.EXPIRATIONDTM > <<lcTS_EffectiveFrom>>
AND L.LABORLEV1NM = 'SXI'
ORDER BY P.FULLNM

ENDTEXT


						IF .lTestMode THEN
							.TrackProgress('', LOGIT+SENDIT)
							.TrackProgress('lcSQL =' + lcSQL, LOGIT+SENDIT)
						ENDIF

						.TrackProgress('Connecting to WTK System....', LOGIT+SENDIT)

						* get ss#s from ADP
						OPEN DATABASE F:\UTIL\KRONOS\KRONOSEDI

						*.nSQLHandle = SQLCONNECT("KRONOSEDI","tkcsowner","tkcsowner")
						.nSQLHandle = SQLCONNECT("WTK_ON_PAYROLL2","sa","kr0n1tes")

						IF .nSQLHandle > 0 THEN

							IF USED('CUREMPACCTS') THEN
								USE IN CUREMPACCTS
							ENDIF

							IF .ExecSQL(lcSQL, 'CUREMPACCTS', RETURN_DATA_MANDATORY) THEN

								*SELECT CUREMPACCTS
								*BROWSE

								IF USED('KRONOSEMPS') THEN
									USE IN KRONOSEMPS
								ENDIF

*!*									* delete DBFs
*!*									IF FILE('F:\UTIL\KRONOS\DATA\CHECKKRONOSEMPS.DBF') THEN
*!*										DELETE FILE F:\UTIL\KRONOS\DATA\CHECKKRONOSEMPS.DBF
*!*									ENDIF

								* delete XLSs
								IF FILE('F:\UTIL\KRONOS\DATA\MISSINGKRONOSEMPS.XLS') THEN
									DELETE FILE F:\UTIL\KRONOS\DATA\MISSINGKRONOSEMPS.XLS
								ENDIF

								IF FILE('F:\UTIL\KRONOS\DATA\MISSINGKRONOSEMPS.XLS') THEN
									.TrackProgress('Error deleting F:\UTIL\KRONOS\DATA\MISSINGKRONOSEMPS.XLS', LOGIT+SENDIT)
								ENDIF

								IF USED('CURPAYEXPORTKRONOSEMPS') THEN
									USE IN CURPAYEXPORTKRONOSEMPS
								ENDIF

								* only ee's who would get exported to ADP from Kronos based on F:\UTIL\KRONOS\DATA\TIMERVWR
*!*									SELECT ;
*!*										LEFT(HOMELABORLEVELNM7,4) AS TIMERVWR, ;
*!*										LEFT(HOMELABORLEVELDSC7,30) AS TRNAME, ;
*!*										LEFT(HOMELABORLEVELNM1,3) AS ADP_COMP, ;
*!*										LEFT(HOMELABORLEVELNM2,2) AS DIVISION, ;
*!*										LEFT(HOMELABORLEVELNM3,4) AS DEPT, ;
*!*										LEFT(PERSONFULLNAME,30) AS EMPLOYEE, ;
*!*										INT(VAL(PERSONNUM)) AS FILE_NUM ;
*!*										FROM CUREMPACCTS ;
*!*										INTO CURSOR CURPAYEXPORTKRONOSEMPS ;
*!*										WHERE LEFT(HOMELABORLEVELNM7,4) IN ;
*!*										(SELECT TIMERVWR FROM F:\UTIL\KRONOS\DATA\TIMERVWR WHERE LACTIVE) ;
*!*										AND INT(VAL(PERSONNUM)) IN ;
*!*										(SELECT FILE_NUM FROM CURADPACTIVEEMPS) ;
*!*										ORDER BY 1, 3, 4, 5, 6


								SELECT ;
									LEFT(LABORLEV7NM,4) AS TIMERVWR, ;
									LEFT(LABORLEV7DSC,30) AS TRNAME, ;
									LEFT(LABORLEV1NM,3) AS ADP_COMP, ;
									LEFT(LABORLEV2NM,2) AS DIVISION, ;
									LEFT(LABORLEV3NM,4) AS DEPT, ;
									LEFT(FULLNM,30) AS EMPLOYEE, ;
									INT(VAL(PERSONNUM)) AS FILE_NUM ;
									FROM CUREMPACCTS ;
									INTO CURSOR CURPAYEXPORTKRONOSEMPS ;
									WHERE LEFT(LABORLEV7NM,4) IN ;
									(SELECT TIMERVWR FROM F:\UTIL\KRONOS\DATA\TIMERVWR WHERE LACTIVE AND LHOURLY) ;
									AND INT(VAL(PERSONNUM)) IN ;
									(SELECT FILE_NUM FROM CURADPACTIVEEMPS) ;
									ORDER BY 1, 3, 4, 5, 6


*!*									* COPY DBF
*!*									SELECT CURPAYEXPORTKRONOSEMPS
*!*									*BROWSE
*!*									GOTO TOP
*!*									COPY TO F:\UTIL\KRONOS\DATA\CHECKKRONOSEMPS

*!*									* COPY XLS
*!*									SELECT CURPAYEXPORTKRONOSEMPS
*!*									GOTO TOP
*!*									COPY TO F:\UTIL\KRONOS\DATA\CHECKKRONOSEMPS.XLS XL5

								*******************************************************************************************
								*******************************************************************************************
								* now open the ADP Payroll export file and build a list of File#s that are in it

								WAIT WINDOW NOWAIT "Opening Excel..."
								oExcel = CREATEOBJECT("excel.application")
								oExcel.VISIBLE = .F.
								oWorkbook = oExcel.workbooks.OPEN("F:\UTIL\KRONOS\EDI\ADPFILES\EPIE8799.CSV")
								oWorksheet = oWorkbook.Worksheets[1]
								
								IF USED('CURACTUALPAYEXPORT') THEN
									USE IN CURACTUALPAYEXPORT
								ENDIF
								
								CREATE CURSOR CURACTUALPAYEXPORT ( FILE_NUM int )
							
								FOR lnRow = 2 TO 10000
									lcRow = ALLTRIM(STR(lnRow))
									* get File# from Column C for any row which has "EPIP50" in Col B
									luValue = oWorksheet.RANGE("B" + lcRow).VALUE
									*IF (NOT ISNULL(luValue)) AND (NOT EMPTY(luValue)) AND (TYPE('luValue') = 'C') AND (ALLTRIM(luValue) = "EPIP50") THEN
									IF (NOT ISNULL(luValue)) AND (NOT EMPTY(luValue)) AND (TYPE('luValue') = 'C') AND (LEFT(ALLTRIM(luValue),4) = "EPIP") THEN
										m.FILE_NUM = oWorksheet.RANGE("C" + lcRow).VALUE
										INSERT INTO CURACTUALPAYEXPORT FROM MEMVAR
									ELSE
										* we're done, stop looking
										EXIT FOR
									ENDIF									
								ENDFOR

								oExcel.QUIT()

								* create a cursor of CURPAYEXPORTKRONOSEMPS file#s that are *not* in ADP Payroll export file

								IF USED('CURMISSING') THEN
									USE IN CURMISSING
								ENDIF
								
								SELECT DIVISION, DEPT, EMPLOYEE, FILE_NUM, TIMERVWR, TRNAME ;
									FROM CURPAYEXPORTKRONOSEMPS ;
									INTO CURSOR CURMISSING ;
									WHERE FILE_NUM NOT IN (SELECT FILE_NUM FROM CURACTUALPAYEXPORT) ;
									ORDER BY 1, 2, 3
								
								IF (NOT USED('CURMISSING')) OR EOF('CURMISSING') THEN
									lcTopBodyText = "All eligible employees were in the Kronos-->ADP Payroll Export File." + CRLF + CRLF + ;
										"No action is necessary."
								ELSE
									lcTopBodyText = "WARNING: Not all eligible employees were in the Kronos-->ADP Payroll Export File!" + CRLF + CRLF + ;
										"The employees in the attached spreadsheet were not exported to ADP! Please review."

									* copy to XLS file
									SELECT CURMISSING
									GOTO TOP
									COPY TO F:\UTIL\KRONOS\DATA\MISSINGKRONOSEMPS.XLS XL5
									
									IF NOT FILE('F:\UTIL\KRONOS\DATA\MISSINGKRONOSEMPS.XLS') THEN
										.TrackProgress('Error creating F:\UTIL\KRONOS\DATA\MISSINGKRONOSEMPS.XLS', LOGIT+SENDIT)
									ELSE
										.TrackProgress('Created F:\UTIL\KRONOS\DATA\MISSINGKRONOSEMPS.XLS!', LOGIT+SENDIT)
										.cAttach = .cAttach + 'F:\UTIL\KRONOS\DATA\MISSINGKRONOSEMPS.XLS'
									ENDIF
								
								ENDIF
								*******************************************************************************************
								*******************************************************************************************

							ENDIF  &&  .ExecSQL(lcSQL, 'CURWTKEMPLOYEE', RETURN_DATA_MANDATORY)

							CLOSE DATABASES ALL

*!*								IF NOT FILE('F:\UTIL\KRONOS\DATA\CHECKKRONOSEMPS.DBF') THEN
*!*									.TrackProgress('Error creating F:\UTIL\KRONOS\DATA\CHECKKRONOSEMPS.DBF', LOGIT+SENDIT)
*!*								ELSE
*!*									.TrackProgress('Created F:\UTIL\KRONOS\DATA\CHECKKRONOSEMPS.DBF!', LOGIT+SENDIT)
*!*								ENDIF


						ELSE
							* connection error
							.TrackProgress('Unable to connect to WTK System.', LOGIT+SENDIT)
						ENDIF   &&  .nSQLHandle > 0

					ENDIF  &&  .ExecSQL(lcADPSQL, 'CURADPACTIVEEMPSPRE', RETURN_DATA_MANDATORY)

				ELSE
					* connection error
					.TrackProgress('Unable to connect to ADP Payroll System.', LOGIT+SENDIT)
				ENDIF   &&  .nSQLHandle > 0

				CLOSE DATABASES ALL

			CATCH TO loError

				.TrackProgress('There was an error.',LOGIT+SENDIT)
				.TrackProgress('The Error # is: ' + TRANSFORM(loError.ErrorNo), LOGIT+SENDIT)
				.TrackProgress('The Error Message is: ' + TRANSFORM(loError.MESSAGE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred in Program: ' + TRANSFORM(loError.PROCEDURE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred at line #: ' + TRANSFORM(loError.LINENO), LOGIT+SENDIT)
				lnNumberOfErrors = lnNumberOfErrors + 1
				IF TYPE('oExcel') = 'O' AND NOT ISNULL(oExcel) THEN
					oExcel.QUIT()
				ENDIF
				CLOSE DATA
				CLOSE ALL

			ENDTRY

			CLOSE DATA
			WAIT CLEAR
			***************** INTERNAL email results ******************************
			.TrackProgress('About to send status email.',LOGIT)
			.TrackProgress('==================================================================================================================', SENDIT)
			.TrackProgress('The logfile is: ' + .cLogFile, SENDIT)
			.TrackProgress('CHECK MISSING KRONOS EMPLOYEES process started: ' + .cStartTime, LOGIT+SENDIT)
			.TrackProgress('CHECK MISSING KRONOS EMPLOYEES process finished: ' + TTOC(DATETIME()), LOGIT+SENDIT)
			
			.cBodyText = lcTopBodyText + CRLF + CRLF + .cBodyText

			IF .lSendInternalEmailIsOn THEN
				* try to trap error from not having dartmail dll's registered on user's pc...
				TRY
					DO FORM dartmail2 WITH .cSendTo,.cFrom,.cSubject,.cCC,.cAttach,.cBodyText,"A"
					.TrackProgress('Sent status email.',LOGIT)
				CATCH TO loError
					=MESSAGEBOX('There was an error sending the status email. Please contact Mark Bennett.',0+64,"CHECK MISSING KRONOS EMPLOYEES")
					.TrackProgress('There was an error sending the status email.',LOGIT)
					.TrackProgress('The Error # is: ' + TRANSFORM(loError.ErrorNo), LOGIT)
					.TrackProgress('The Error Message is: ' + TRANSFORM(loError.MESSAGE), LOGIT)
					.TrackProgress('The Error occurred in Program: ' + TRANSFORM(loError.PROCEDURE), LOGIT)
					.TrackProgress('The Error occurred at line #: ' + TRANSFORM(loError.LINENO), LOGIT)
					lnNumberOfErrors = lnNumberOfErrors + 1
					CLOSE DATA
				ENDTRY
			ELSE
				.TrackProgress('Did not send status email: THIS.lSendInternalEmailIsOn = FALSE.',LOGIT)
			ENDIF

		ENDWITH
		RETURN
	ENDFUNC && main


	FUNCTION ExecSQL
		LPARAMETERS tcSQL, tcCursorName, tlNoDataReturnedIsOkay
		LOCAL llRetval, lnResult
		WITH THIS
			* close target cursor if it's open
			IF USED(tcCursorName)
				USE IN (tcCursorName)
			ENDIF
			WAIT WINDOW "Fetching remote data; please wait..." NOWAIT
			lnResult = sqlexec(.nSQLHandle, tcSQL, tcCursorName)
			llRetval = ( lnResult > 0 )
			IF llRetval THEN
				* see if any data came back
				IF NOT tlNoDataReturnedIsOkay THEN
					IF (NOT USED(tcCursorName)) OR EOF(tcCursorName) THEN
						llRetval = .F.
						.TrackProgress('No data was returned by this query: ' +CRLF + tcSQL, LOGIT+SENDIT)
					ENDIF
				ENDIF
			ELSE
				.TrackProgress("Result Code [" + TRANSFORM(lnResult) + "] returned by this query: " + tcSQL, LOGIT+SENDIT)
			ENDIF
			WAIT CLEAR
			RETURN llRetval
		ENDWITH
	ENDFUNC


	PROCEDURE TrackProgress
		* do any combination of Wait Window, writing to logfile, and adding to body of email,
		* based on nFlags parameter.
		LPARAMETERS tcExpression, tnFlags
		WITH THIS
			IF BITAND(tnFlags,LOGIT) = LOGIT THEN
				IF .lLoggingIsOn THEN
					?
					? .cProcessName + "  " + TTOC(DATETIME()) + ": " + tcExpression
				ENDIF
			ENDIF
			IF .lWaitWindowIsOn THEN
				IF BITAND(tnFlags,NOWAITIT) = NOWAITIT THEN
					WAIT WINDOW tcExpression NOWAIT
				ENDIF
				IF BITAND(tnFlags,WAITIT) = WAITIT THEN
					WAIT WINDOW tcExpression TIMEOUT .nWaitWindowTimeout
				ENDIF
			ENDIF
			IF BITAND(tnFlags,SENDIT) = SENDIT THEN
				IF .lSendInternalEmailIsOn THEN
					.cBodyText = .cBodyText + tcExpression + CRLF + CRLF
				ENDIF
			ENDIF
		ENDWITH
	ENDPROC  &&  TrackProgress

ENDDEFINE

