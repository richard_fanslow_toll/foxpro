utilsetup("ANF_POUPLOAD")
Close Data All

Public lTesting
Public Array a856(1)

lTesting = .F.
Do m:\dev\prg\_setvars With lTesting

Use f:\anf\data\podata.dbf

lcUpLoadDet = "POs loaded:"+Chr(13)+"PO       PO Code  POType"+Chr(13) 

useca("ackdata","wh")

If !lTesting
  Use F:\edirouting\ftpsetup Shared In 0
  cTransfer = "850-ANF"
  Locate For ftpsetup.transfer = cTransfer And ftpsetup.Type = "GET"
  If ftpsetup.chkbusy And !lOverridebusy
    Wait Window "Process is flagged busy...returning" Timeout 3
    NormalExit = .T.
    Return
  Else
    Replace chkbusy With .T. For ftpsetup.transfer = cTransfer In ftpsetup
  Endif
Endif

goffice="I"
gmasteroffice="N"

NumAdded = 0
NumUpdated = 0

*Select * From poroute Where .F. Into Cursor temppo Readwrite
lcCurrDir = ""

If !lTesting
  lcPath = 'F:\FTPUSERS\ANF\850IN\'
  lcArchivePath = 'F:\FTPUSERS\ANF\850IN\\ARCHIVE\'
  tsendto="PGAIDIS@FMIINT.COM,juan@fmiint.com"
  tcc=""
Else
  lcPath = 'F:\FTPUSERS\ANF\850IN\'
  lcArchivePath = 'F:\FTPUSERS\ANF\850IN\\ARCHIVE\'
  tsendto="PGAIDIS@FMIINT.COM,juan@fmiint.com"
  tcc=""
Endif

len1 = Adir(ary1,'F:\FTPUSERS\ANF\850IN\*.*')
If len1 = 0
  Wait Window "No files found...exiting" Timeout 2
  Close Data All
  schedupdate()
  _Screen.Caption=gscreencaption
  On Error
  Return
Endif

lcAddedStr  =""
ii=1

For Thisfile = 1 To len1
  cFilename = Alltrim(ary1[thisfile,1])
  xfile = lcPath+cFilename
  Do m:\dev\prg\createx856a
  Do m:\dev\prg\loadedifile With xfile,"*","TILDE"

  m.addby = "FILE"+Alltrim(Transform(Thisfile))
  m.adddt = Datetime()
  m.addproc = "FILE"+Alltrim(Transform(Thisfile))
  m.accountid = 6576  && Ambercrombie and Fitch
  m.info = ""

  Select podata
  Scatter Memvar Memo Blank


  Select x856
  Goto Top
  Scan
    Do Case
    Case Trim(x856.segment) = "ISA"
      cisa_num = Alltrim(x856.f13)
      m.isanum = Alltrim(x856.f13)
      lcDate = "20"+Alltrim(x856.f9)
      lxDate = Substr(lcDate,5,2)+"/"+Substr(lcDate,7,2)+"/"+Left(lcDate,4)
      ldDate = Ctod(lxDate)
      m.isadate = ldDate

    Case Trim(x856.segment) = "GS"
      cgs_num = Alltrim(x856.f6)
      lcCurrentGroupNum = Alltrim(x856.f6)
      m.groupnum = Alltrim(x856.f6)
      
		m.groupnum=lcCurrentGroupNum
		m.isanum=cisa_num
		m.transnum=alltrim(x856.f2)
		m.edicode="PV"
		m.accountid=6565
		m.loaddt=date()
		m.loadtime=datetime()
		m.filename=xfile
		insertinto("ackdata","wh",.t.)
		tu("ackdata")
	  
    Case Trim(x856.segment) = "BEG"
      m.pocode = Upper(Allt(x856.f2))
      m.po_num = padr(Upper(x856.f3),len(podata.po_num))

      lcDate = Alltrim(x856.f5)
      lxDate = Substr(lcDate,5,2)+"/"+Substr(lcDate,7,2)+"/"+Left(lcDate,4)
      ldDate = Ctod(lxDate)
      Store ldDate To m.edidate

    Case x856.segment = "REF" And Allt(x856.f1) = "PT"
      m.style = Upper(Allt(x856.f2))

	  do case
      case Substr(m.pocode,4,1) = "M" And Inlist(m.style,"INIT","FLOW")
        m.style = "MATRIX"

	  **added DTC logic per Juan - mvw 09/29/16
      case inlist(m.pocode,"US1TC","US1TT")
        m.style = "DTC"
      endcase

    Case x856.segment = "SE"
      lcUpLoadDet = lcUpLoadDet+m.po_num+" - "+m.pocode+" - "+m.style+Chr(13)
      m.accountid = 6576
      m.adddt = Datetime()
      m.filename = cFilename

      Select podata
	  **delete if already exists - mvw 04/01/16
	  delete for accountid=m.accountid and po_num=m.po_num
      append blank
      gather memvar
    Endcase
  Endscan

  cLoadFile = (lcPath+cFilename)
  cArchiveFile = (lcArchivePath+cFilename)
  Copy File [&cLoadFile] To [&cArchiveFile]
  Delete File [&cLoadFile]

Endfor

tattach = ""
tFrom ="Toll EDI System Operations <toll-edi-ops@tollgroup.com>"
tmessage = "ANF 850 Upload updated at: "+Ttoc(Datetime())+Chr(13)+Chr(13)+lcUpLoadDet

tSubject = "ANF 850 Updated"
Do Form m:\dev\frm\dartmail2 With tsendto,tFrom,tSubject,tcc,tattach,tmessage,"A"
Wait Window "All 850 Files finished processing...exiting" Timeout 2

Select ftpsetup
Replace chkbusy With .F. For ftpsetup.transfer = "850-ANF" In ftpsetup

Close Data All

schedupdate()
*_Screen.Caption=gscreencaption
On Error
