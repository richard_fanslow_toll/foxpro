Lparameters uccnum

dig1 = Val(Substr(uccnum,1,1))*1
dig2 = Val(Substr(uccnum,2,1))*3
dig3 = Val(Substr(uccnum,3,1))*1
dig4 = Val(Substr(uccnum,4,1))*3
dig5 = Val(Substr(uccnum,5,1))*1
dig6 = Val(Substr(uccnum,6,1))*3
dig7 = Val(Substr(uccnum,7,1))*1
dig8 = Val(Substr(uccnum,8,1))*3
dig9 = Val(Substr(uccnum,9,1))*1
dig10 = Val(Substr(uccnum,10,1))*3
dig11 = Val(Substr(uccnum,11,1))*1
dig12 = Val(Substr(uccnum,12,1))*3
dig13 = Val(Substr(uccnum,13,1))

lnsum = dig1+dig2+dig3+dig4+dig5+dig6+dig7+dig8+dig9+dig10+dig11+dig12
lntot=lnsum
lnckdigit = Mod(lntot,10)

If lnckdigit = 0
  Return Alltrim(Str(lnckdigit))
Else
  lnckdigit= 10-lnckdigit
  Return Alltrim(Str(lnckdigit))
Endif
