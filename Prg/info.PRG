lparameters xformname, xwo_num, xbol_no, xaccountid, xship_ref, xswcnum, xcnee_ref, xfuture2, xsystem, xmessage, xdetails, xstyle, xcolor, xid, xpack, xqty, xwavenum

if empty(xmessage)
	return
endif

if len(trim(xmessage))>80
	email("Dyoung@fmiint.com","INFO: message too long",xmessage,,,,.t.,,,,,.t.,,.t.)
endif

**removed because now that the insert is done via inertinto(), these vars are undefined within insertinto() - mvw 06/27/16
local m.formname, m.wo_num, m.bol_no, m.accountid, m.ship_ref, m.cnee_ref, m.swcnum, m.system, m.message, m.details, m.style, m.color, m.id, m.pack, m.qty, m.userid, m.zdatetime

m.formname=iif(empty(xformname),"",upper(iif(xformname="frm",substr(xformname,4),xformname)))
m.wo_num=iif(empty(xwo_num),0,xwo_num)
m.wavenum=iif(empty(xwavenum),0,xwavenum)
m.bol_no=iif(empty(xbol_no),"",xbol_no)
m.accountid=iif(empty(xaccountid),0,xaccountid)
m.ship_ref=iif(empty(xship_ref),"",xship_ref)
m.cnee_ref=iif(empty(xcnee_ref),"",xcnee_ref)
m.swcnum=iif(empty(xswcnum),"",xswcnum)
m.system=xsystem
m.message=xmessage
m.details=iif(emptynul(xdetails),"",xdetails)
m.style=iif(empty(xstyle),"",xstyle)
m.color=iif(empty(xcolor),"",xcolor)
m.id=iif(empty(xid),"",xid)
m.pack=iif(empty(xpack),"",xpack)
m.qty=iif(empty(xqty),0,xqty)
m.userid=guserid
m.zdate=qdate()
m.zdatetime=qdate(.t.)
m.updateby=""
m.updatedt={}
m.updproc=""
m.addby=""
m.adddt={}
m.addproc=""
m.office=gmasteroffice
m.mod=goffice
m.flag=.f.
*m.infoid=dygenpk("info","whall")
m.infoid=sqlgenpk("info","whall")

if m.wo_num=0 and inlist(m.formname,"INWO","OUTWO") and empty(m.bol_no)
	m.wo_num=1		&& we're adding a manual WMS in/out WO
endif

try	
	insert into info from memvar
catch
	email("Dyoung@fmiint.com","INFO: failed to save info",,,,,.t.,,,,,.t.,,.t.)
endtry
