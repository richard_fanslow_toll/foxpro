**errHandler.prg
**
Parameters cUserId, nErrorNo, cErrorMsg, cFileName, cLineOfCode, nLineNo, lImportPoller
**lImportPoller - indicates that this error occurred in the barcode import prg, not tlMain

tsubject = "Import Poller Error - "+Transform(nErrorNo)
tsendto = "mwinter@fmiint.com"
tattach = ""
tFrom ="Import Poller <fmi-transload-ops@fmiint.com>"
lcSourceMachine = Sys(0)

DO case
  Case !lImportPoller and InList(nErrorNo, 108, 109) &&"file is in use by another user", "record is in use by another user"
	strMsg = "Attempting to obtain access to necessary files... If this problem persists please contact MIS."
	Wait strMsg window noClear timeOut 3
	Wait clear
	Retry

  Case !lImportPoller and nErrorNo = 1705 &&"file access denied"
	strMsg = "Attempting to obtain access to necessary files... If this problem persists please contact MIS."+;
			 Chr(13)+Chr(13)+"File Running: "+Alltrim(cFileName)+Chr(13)+"Line: "+Alltrim(Str(nLineNo))
	Wait strMsg window noClear timeOut 3
	Wait clear
	Retry

*!*	  Case InList(nErrorNo, 102, 1102) and lImportPoller &&cannot create file...
*!*		tmessage = "Will Retry, but check to make sure the process was able to correct itself."
*!*		Do Form dartmail With tsendto,tFrom,tsubject," ",tattach,tmessage,"A"

*!*		strMsg = "Attempting to create necessary files... If this problem persists please contact MIS."+;
*!*				 Chr(13)+Chr(13)+"File Running: "+Alltrim(cFileName)+Chr(13)+"Line: "+Alltrim(Str(nLineNo))
*!*		Wait strMsg window noClear timeOut 3
*!*		Wait clear
*!*		Retry

  Otherwise
	If lImportPoller
		tmessage = "Error Occurred, please check."+chr(13)+chr(13)+"Error Msg: "+alltrim(cErrorMsg)+chr(13)+;
			"File: "+cFileName+chr(13)+"Line of Code: "+cLineOfCode+chr(13)+"Line No: "+transform(nLineNo)

		if type("cAcctFilter")="C"
			tmessage=tmessage+chr(13)+"cacctfilter: '"+cAcctFilter+"'"
		endif
		if type("cDivFilter")="C"
			tmessage=tmessage+chr(13)+"cdivfilter: '"+cDivFilter+"'"
		endif
		

		Do Form dartmail With tsendto,tFrom,tsubject," ",tattach,tmessage,"A"
		on error
		naction=1
	EndIf

	if !limportpoller
		USE tl!errors In 0
		Insert into errors (errorNo, errorMsg, fileName, lineOfCode, lineNo, userId, occurred) values ;
						   (nErrorNo, cErrorMsg, cFileName, cLineOfCode, nLineNo, cUserId, Datetime())
		USE in errors

		cErrorId = SUBSTR(SYS(2015), 3, 10)
		DO email with "mwinter@fmiint.com", "Error Retry - Error Id: "+cErrorId, "", , .F., , , .T.

		**nAction: 0 - ignore, 1 - retry, 2 - cancel
		DO form errHandler with cUserId, nErrorNo, cErrorMsg, cFileName, cLineOfCode, nLineNo, lImportPoller, cErrorId ;
			to nAction
	endif

	DO case
	  Case nAction = 1
		Retry
	  Case nAction = 2
		Cancel
	EndCase
EndCase
