*!* m:\dev\prg\agegroup940_main.prg

Parameters cOfficeIn,ThisAcctNum

runack("AGEGROUP940")

****************************8

*  Do Not compile, being eddited,  PG 01/30/2018


***********************************
Close Data All

Public nAcctNum,cDelimiter,nRun_num,cTranslateOption,cWhichFile,cOffice,cCustname,cPropername,cStorenum,lBrowfiles
Public cFilter,nRunID,lComplete,lTesting,m.UCCNUMBER,lEmail,lTestImport,emailcommentstr,cUseFolder,lRLFile,nPTQty
Public ptid,ptctr,ptqty,xfile,cfilename,UploadCount,pickticket_num_start,pickticket_num_end,nFileCount,units,m.printstuff
Public lcPath,lcArchivePath,tsendto,tcc,cTransfer,NormalExit,lcErrMessage,tsendtoerr,tccerr,lOverrideBusy,nCASEPACK,lTestmail
Public xfile,lcSourceProgram,lcErrMessage,lLoadSQL,cMod,nFileSize,cISA_Num,cPTString,cISA_Num,lDoImport,tsendtostyle,tccstyle
Public Array a856(1)

xfile="Not Set Yet"
lcSourceProgram =""
lcErrMessage=""
nFileSize = 0
cPTString = ""

set step On 

** AgeGroup          1285 is in Carson2 and NJ
** Essential Brands  6769 is in Carson2, NJ and SanPedro

nAcctNum= Val(Transform(ThisAcctNum))

Do m:\dev\prg\_setvars With .T.
Set Status Bar On
*SET STEP ON

Try
  lTesting = .f.
  lTestImport = lTesting

  lEmail = .T.
  lTestmail = lTesting
  lBrowfiles = lTesting
*  lBrowfiles = .t.
  NormalExit = .F.
  lOverrideBusy = lTesting
*  lOverrideBusy = .T.
  lTestmail = lTesting
  _Screen.WindowState = Iif(lTesting Or lOverrideBusy,2,1)

  If !lTesting
    cTransfer = "X"
    Store "940-AGEGROUP" To cTransfer
    Store "" To tsendto,tcc,tsendtoerr,tccerr
    Select 0
    Use F:\edirouting\FTPSETUP Shared
    Locate For FTPSETUP.TRANSFER = cTransfer
    If FTPSETUP.chkbusy = .T. And !lOverrideBusy
      Wait Window "Busy...exiting program now" Timeout 2
      NormalExit = .T.
      Throw
    Endif
    Replace chkbusy With .T.,trig_time With Datetime() For FTPSETUP.TRANSFER = cTransfer
    Use In FTPSETUP
  Endif

  If Vartype(cOfficeIn) = "L"
    cOfficeIn = "Y"
    nAcctNum = 1285
  Endif


  cOffice = cOfficeIn
  gOffice = cMod
  gMasterOffice = cOffice
  lLoadSQL = .T.


  Do Case
  Case cOffice = "Y" And nAcctNum= 6769
    cUseName = "ESSENTIAL BRANDS"
    Wait Window At 10,10 "At start of Essential Brands (6769) for Carson 2...940 process" Timeout 2
    _Screen.Caption = "At start of Essential Brands for Carson 2...940 process"
    cMod= "Y"

  Case cOffice = "N" And nAcctNum= 6769
    cUseName = "ESSENTIAL BRANDS"
    Wait Window At 10,10 "At start of Essential Brands (6769) for NJ...940 process" Timeout 2
    _Screen.Caption = "At start of Essential Brands for NJ...940 process"
    cMod= "I"

  Case cOffice = "C" And nAcctNum= 6769
    cUseName = "ESSENTIAL BRANDS"
    Wait Window At 10,10 "At start of Essential Brands (6769) for San Pedro...940 process" Timeout 2
    _Screen.Caption = "At start of Essential Brands for San Pedro...940 process"
    cMod= "1"

  Case cOffice = "Y" And nAcctNum= 1285
    cUseName = "AGE GROUP"
    Wait Window At 10,10 "At start of Age Group (1285) for Carson 2...940 process" Timeout 2
    _Screen.Caption = "At start of Age Group for Carson 2...940 process"
    cMod= "Y"

  Case cOffice = "N" And nAcctNum= 1285
    cUseName = "AGE GROUP"
    Wait Window At 10,10 "At start of Age Group (1285) for NJ...940 process" Timeout 2
    _Screen.Caption = "At start of Age Group for NJ...940 process"
    cMod= "I"
  Endcase

  cPropername = Proper(cUseName)

  Select 0
  Use F:\3pl\Data\mailmaster Alias mm Shared
  Locate For (mm.accountid = nAcctNum And mm.office = cOffice And edi_type = "940")

  If Found()
    Store Trim(mm.acctname) To cAccountName
    Store Trim(mm.basepath) To lcPath
    Store Trim(mm.archpath) To lcArchivePath
    Store Trim(mm.holdpath) To lcHoldPath

    If lTesting
      Locate
      Locate For taskname = "JOETEST"
    Endif

    tsendto = Iif(mm.use_alt,Trim(mm.sendtoalt),Trim(mm.sendto))
    tcc = Iif(mm.use_alt,Trim(mm.ccalt),Trim(mm.cc))
    Locate For (mm.accountid = 9999) And (mm.office = "X")
    tsendtoerr = Iif(mm.use_alt,Trim(mm.sendtoalt),Trim(mm.sendto))
    tccerr = Iif(mm.use_alt,Trim(mm.ccalt),Trim(mm.cc))
    Locate
    Locate For (mm.Group = "AGEGROUP") And (mm.taskname = "STYLEERROR")
    tsendtostyle = Iif(mm.use_alt,Trim(mm.sendtoalt),Trim(mm.sendto))
    tccstyle = Iif(mm.use_alt,Trim(mm.ccalt),Trim(mm.cc))

    Use In mm
  Else
    Set Step On
    Use In mm
    Wait Window At 10,10  "No parameters set for this acct# "+Alltrim(Str(nAcctNum))+"  ---> Office "+cOffice Timeout 2
    NormalExit = .F.
    Throw
  Endif

  Store cAccountName To cCustname
  Clear
  Wait Window "Now setting up "+cPropername+" 940 process..." Timeout 1

  cfile = ""
  lRLFile = .F.

  dXdate1 = ""
  dXxdate2 = Date()
  nPTQty = 0
  nFileCount = 0

  nRun_num = 999
  lnRunID = nRun_num

  If lTesting
    lcPath = Strtran(lcPath,"940in","940intest")
    Wait Window "Path used: "+lcPath Timeout 2
  Endif

  xReturn="XXX"
  xReturn = wf(cOffice,nAcctNum)
*  ASSERT .f.
  cUseFolder = xReturn

  Do ("m:\dev\PRG\AgeGroup940_PROCESS")

  If Used('ftpsetup')
    Use In FTPSETUP
  Endif
  If !lTesting
    Select 0
    Use F:\edirouting\FTPSETUP Shared
    Replace chkbusy With .F. For FTPSETUP.TRANSFER = cTransfer
    Use In FTPSETUP
  Endif

Catch To oErr
  If !NormalExit
    Assert .F. Message "In CATCH"
    Set Step On
    tsubject = cPropername+" 940 Upload Error ("+Transform(oErr.ErrorNo)+") at "+Ttoc(Datetime())
    tattach  = ""
    tsendto  = tsendtoerr
    tcc = tccerr
    tmessage = cPropername+" 940 Upload Error..... Please fix me........!"
    lcSourceMachine = Sys(0)
    lcSourceProgram = Sys(16)

    tmessage =tmessage+ "Try/CATCH Exeception Message:"+Chr(13)+;
      [  Procedure: ] + oErr.Procedure +Chr(13)+;
      [  LineNo: ] + Str(oErr.Lineno) +Chr(13)+;
      [  Message: ] + oErr.Message +Chr(13)+;
      [  Error: ] + Str(oErr.ErrorNo) +Chr(13)+;
      [  Details: ] + oErr.Details +Chr(13)+;
      [  StackLevel: ] + Str(oErr.StackLevel) +Chr(13)+;
      [  LineContents: ] + oErr.LineContents+Chr(13)+;
      [  Message: ] + lcErrMessage+Chr(13)+;
      [  UserValue: ] + oErr.UserValue+Chr(13)+;
      [  Computer:  ] +lcSourceMachine+Chr(13)+;
      [  940 file:  ] +xfile+Chr(13)+;
      [  Program:   ] +lcSourceProgram
    tmessage = tmessage+Chr(13)+Chr(13)+lcErrMessage

    tfrom    ="TGF EDI Processing Center <toll-edi-ops@tollgroup.com>"
    Do Form m:\dev\frm\dartmail2 With tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
  Else
    Wait Window At 10,10 "Normal Exit " Timeout 2
  Endif
Finally
  Release All Like T*
  Close Databases All
Endtry
