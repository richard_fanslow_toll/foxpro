set step on

public xfile

lcPath ="f:\ftpusers\kg-ca\832\"

lnNum = ADIR(tarray,lcPath+"*.*")
IF lnNum = 0
  WAIT WINDOW "No files found...exiting" TIMEOUT 2
  CLOSE DATA ALL
  schedupdate()
  _SCREEN.CAPTION="Kurt Geiger Packing List Upload.............."
  ON ERROR
  RETURN
Else
 For thisfile = 1  To lnNum
  cfilename = ALLTRIM(tarray[thisfile,1])
  xfile = lcpath+cfilename
  loadfile()
  next 
Endif


******************************************************************
Procedure loadfile 
Create Cursor x832 (;
status char(1),;
Code  char(3),;
codename char(30),;
brand char(30),;
style char(10),;
prodname char(30),;
color    char(10),;
material char(30),;
size     char(13),;
upc      char(13),;
price    char(12))


Select x832
Set Step On 

append from &xfile Type csv
endproc