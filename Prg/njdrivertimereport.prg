* Create spreadsheet of Hours summary for NJ Drivers for the previous pay period
* BY DAY, NOT ROLLED UP FOR THE WEEK!
*
* Highlight those shifts where hours are >= 5.5 but less than 8.0
* run Monday am for Mike Drew

* 4/30/2018 MB: changed fmiint.com email to Toll
*
* EXE = F:\UTIL\TIMESTAR\NJDRIVERTIMEREPORT.EXE
*

LOCAL loNJDRIVERTIMEREPORT

runack("NJDRIVERTIMEREPORT")

utilsetup("NJDRIVERTIMEREPORT")

loNJDRIVERTIMEREPORT = CREATEOBJECT('NJDRIVERTIMEREPORT')
loNJDRIVERTIMEREPORT.MAIN()

=SchedUpdate()

CLOSE DATABASES ALL
RETURN

#DEFINE LOGIT 1
#DEFINE WAITIT 2
#DEFINE NOWAITIT 4
#DEFINE SENDIT 8
#DEFINE NOCRLF 16
#DEFINE CRLF CHR(13) + CHR(10)
#DEFINE LF CHR(10)
#DEFINE RETURN_DATA_MANDATORY .F.
#DEFINE RETURN_DATA_NOT_MANDATORY .T.
#DEFINE DAILY_OT_MULTIPLIER 1.5
#DEFINE DAILY_OT20_MULTIPLIER 2.0

DEFINE CLASS NJDRIVERTIMEREPORT AS CUSTOM

	cProcessName = 'NJDRIVERTIMEREPORT'

	lTestMode = .F.  && .F. FOR NORMAL OPERATION, .T. TO SUPPRESS ALL BUT DEVELOPER EMAILS AND CHANGE INPUT TABLE, ETC.

	* which payroll server, and associated connections, to hit
	lUsePAYROLL2Server = .T.

	lAutoYield = .T.

	cStartTime = TTOC(DATETIME())

	* connection properties
	nSQLHandle = 0

	* file properties
	nFileHandle = 0

	* sql/report filter properties
	cTempCoWhere = ""
	cTempCoWhere2 = ""
	cTempCo = ""
	cTempCoName = ""
	cExtraWhere = ""
	cExtraWhere2 = ""
	* hours discrepancy testing props
	nMinHoursTest = 5.5
	nMaxHoursTest = 8

	* date properties
	dtNow = DATETIME()

	dEndDate = {}
	dStartDate = {}

	dToday = DATE()
	* to force different pay weeks
	*dToday = {^2014-08-18}

	cToday = DTOC(DATE())

	* wait window properties
	nWaitWindowTimeout = 2
	lWaitWindowIsOn = .T.

	* logfile properties
	lLoggingIsOn = .T.
	cLogFile = 'F:\UTIL\TIMESTAR\LOGFILES\NJDRIVERTIMEREPORT_log.txt'

	* INTERNAL email properties
	lSendInternalEmailIsOn = .T.
	cFrom = 'mark.bennett@tollgroup.com'
	cSendTo = 'mike.drew@tollgroup.com, nadine.donovan@tollgroup.com'
	cCC = 'mark.bennett@tollgroup.com'
	cSubject = ''
	cAttach = ''
	cBodyText = ''

	cCOMPUTERNAME = ''
	cUSERNAME = ''

	FUNCTION INIT
		IF NOT DODEFAULT()
			RETURN .F.
		ENDIF
		WITH THIS
			CLEAR
			*SET RESOURCE OFF
			CLOSE DATA
			SET CENTURY ON
			SET DATE YMD
			SET HOURS TO 24
			SET ANSI ON
			SET TALK OFF
			SET DELETED ON
			SET CONSOLE OFF
			SET EXCLUSIVE OFF
			SET SAFETY OFF
			SET EXACT OFF
			SET STATUS BAR ON
			SET SYSMENU OFF
			SET ENGINEBEHAVIOR 70
			SET DECIMALS TO 4
			_VFP.AUTOYIELD = .lAutoYield
			.cCOMPUTERNAME = GETENV("COMPUTERNAME")
			.cUSERNAME = GETENV("USERNAME")
			IF .lTestMode THEN
				.cSendTo = 'mark.bennett@tollgroup.com'
				.cCC = ''
				.cLogFile = 'F:\UTIL\TIMESTAR\LOGFILES\NJDRIVERTIMEREPORT_log_TESTMODE.txt'
			ENDIF
			.lLoggingIsOn = .lLoggingIsOn AND NOT EMPTY(.cLogFile)
			IF .lLoggingIsOn THEN
				SET ALTERNATE TO (.cLogFile) ADDITIVE
				SET ALTERNATE ON
			ENDIF
		ENDWITH
	ENDFUNC


	FUNCTION DESTROY
		WITH THIS
			IF .lLoggingIsOn  THEN
				SET ALTERNATE OFF
				SET ALTERNATE TO
			ENDIF
		ENDWITH
		DODEFAULT()
	ENDFUNC


	FUNCTION MAIN
		WITH THIS
			LOCAL lnNumberOfErrors, lcSQL, lcSQL2, lcSQL3, lcSQL4, lcSQL5, loError, lcTopBodyText, llValid
			LOCAL lcXLFileName, ldStartDate, ldEndDate, lcStartDate, lcEndDate, ldDate
			LOCAL lcSQLStartDate, lcSQLEndDate, lcXLFileName, ldYesterday
			LOCAL oExcel, oWorkbook, oWorksheet, lnRow, lcRow, lnStartRow, lcStartRow, lnEndRow, lnPP, lcPP, lnNN, lcNN, lcJJ
			LOCAL lcColsToProcess, lnColumn, lcBoldColumn, i, j, lcBoldRow, luCellValue, lcTargetDirectory, ldPayDate, lcFiletoSaveAs
			LOCAL lcFileDate, lcDivision, lcTitle, llSaveAgain, lcPayCode, lcSpecialWhere


			TRY
				lnNumberOfErrors = 0
				lcTopBodyText = ''

				.TrackProgress('NJ Driver Time Report process started....', LOGIT+SENDIT)
				.TrackProgress('COMPUTERNAME = ' + .cCOMPUTERNAME, LOGIT+SENDIT)
				.TrackProgress('USERNAME = ' + .cUSERNAME, LOGIT+SENDIT)
				.TrackProgress('Project =  NJDRIVERTIMEREPORT', LOGIT+SENDIT)
				IF .lTestMode THEN
					.TrackProgress('                        TEST MODE                        ', LOGIT+SENDIT)
				ENDIF


				ldToday = .dToday

				ldTomorrow = ldToday + 1
				ldYesterday = ldToday - 1

				* calculate pay period start/end dates.
				* We go back from today until we hit a Saturday (DOW = 7) , which is the end day of the previous pay period.
				* then we subtract 6 days from that to get the prior Sunday (DOW = 1), which is the start day of the previous pay period.
				ldDate = ldToday
				DO WHILE DOW(ldDate,1) <> 7
					ldDate = ldDate - 1
				ENDDO
				ldEndDate = ldDate
				ldStartDate = ldDate - 6

*!*	*!*	***********************************
*!*	*!*	** force custom date ranges here
*!*	ldStartDate = {^2016-01-01}
*!*	ldEndDate = {^2016-06-30}
*!*	*!*	***********************************


				.dEndDate = ldEndDate
				.dStartDate = ldStartDate

				lcSpecialWhere = ""
				************************************************************

				SET DATE AMERICAN

				lcStartDate = STRTRAN(DTOC(ldStartDate),"/","-")
				lcEndDate = STRTRAN(DTOC(ldEndDate),"/","-")

				SET DATE YMD

				*	 create "YYYYMMDD" safe date format for use in SQL query
				* NOTE: for SQL we are going to use < EndDate ( not <= ), so we are going to add 1 to that date here for querying purposes.
				lcSQLStartDate = STRTRAN(DTOC(ldStartDate),"/","")
				lcSQLEndDate = STRTRAN(DTOC(ldEndDate + 1),"/","")
				lcSQLToday = STRTRAN(DTOC(ldToday),"/","")
				lcSQLTomorrow = STRTRAN(DTOC(ldTomorrow),"/","")

				SET DATE AMERICAN

				.cSubject = 'NJ Driver Time for Week: ' + lcStartDate + " to " + lcEndDate

				USE F:\UTIL\TIMESTAR\DATA\TIMEDATA.DBF IN 0 ALIAS TIMEDATA

				IF USED('CURDRIVERS') THEN
					USE IN CURDRIVERS
				ENDIF
* STANDARD REPORT

				SELECT ;
					DIVISION, ;
					DEPT, ;
					NAME, ;
					FILE_NUM, ;
					WORKDATE, ;
					SUM(TOTHOURS) AS TOTHOURS ;
					FROM TIMEDATA ;
					INTO CURSOR CURDRIVERS ;
					WHERE (DIVISION = '03') ;
					AND (DEPT = '0650') ;
					AND (WORKDATE >= ldStartDate) ;
					AND (WORKDATE <= ldEndDate) ;
					GROUP BY DIVISION, DEPT, NAME, FILE_NUM, WORKDATE ;
					ORDER BY NAME, WORKDATE ;
					READWRITE

*!*	* chambersburg switchers
*!*					SELECT ;
*!*						DIVISION, ;
*!*						DEPT, ;
*!*						NAME, ;
*!*						FILE_NUM, ;
*!*						WORKDATE, ;
*!*						SUM(TOTHOURS) AS TOTHOURS ;
*!*						FROM TIMEDATA ;
*!*						INTO CURSOR CURDRIVERS ;
*!*						WHERE (DIVISION = '08') ;
*!*						AND (DEPT = '0660') ;
*!*						AND (WORKDATE >= ldStartDate) ;
*!*						AND (WORKDATE <= ldEndDate) ;
*!*						GROUP BY DIVISION, DEPT, NAME, FILE_NUM, WORKDATE ;
*!*						ORDER BY NAME, WORKDATE ;
*!*						READWRITE

*!*	* CUSTOM QUERY FOR MIKE DIV.
*!*					SELECT ;
*!*						DIVISION, ;
*!*						DEPT, ;
*!*						NAME, ;
*!*						FILE_NUM, ;
*!*						WORKDATE, ;
*!*						SUM(TOTHOURS) AS TOTHOURS ;
*!*						FROM TIMEDATA ;
*!*						INTO CURSOR CURDRIVERS ;
*!*						WHERE INLIST(FILE_NUM,'2309496','2309476','2309504','2309457','2309486','2309448','2309451','2490970','2309453') ;
*!*						AND (WORKDATE >= ldStartDate) ;
*!*						AND (WORKDATE <= ldEndDate) ;
*!*						GROUP BY DIVISION, DEPT, NAME, FILE_NUM, WORKDATE ;
*!*						ORDER BY NAME, WORKDATE ;
*!*						READWRITE


				WAIT WINDOW NOWAIT "Preparing data..."

				SELECT CURDRIVERS
				GOTO TOP
				IF EOF() THEN
					.TrackProgress("There was no data to export!", LOGIT+SENDIT)
				ELSE

					lcFilePrefix = "NJ Driver Weekly Time "
					ldPayDate = ldEndDate + 6
					lcFileDate = PADL(MONTH(ldPayDate),2,"0") + "-"  + PADL(DAY(ldPayDate),2,"0") + "-" + PADL(YEAR(ldPayDate),4,"0")

					lcTargetDirectory = "F:\UTIL\TIMESTAR\REPORTS\" && + lcFileDate + "\"

					lcFiletoSaveAs = lcTargetDirectory + lcFilePrefix + " " + lcFileDate
					lcXLFileName = lcFiletoSaveAs + ".XLS"

					* create primary, detail spreadsheet

					WAIT WINDOW NOWAIT "Opening Excel..."
					oExcel = CREATEOBJECT("excel.application")
					oExcel.VISIBLE = .F.
					oWorkbook = oExcel.workbooks.OPEN("F:\UTIL\TIMESTAR\TEMPLATES\NJDRIVERTIMEREPORT.XLS")

					* if target directory exists, save there
					llSaveAgain = .F.
					IF DIRECTORY(lcTargetDirectory) THEN
						IF FILE(lcXLFileName) THEN
							DELETE FILE (lcXLFileName)
						ENDIF
						oWorkbook.SAVEAS(lcFiletoSaveAs)
						* set flag to save again after sheet is populated
						llSaveAgain = .T.
					ENDIF
					***********************************************************************************************************************
					***********************************************************************************************************************
					WAIT WINDOW NOWAIT "Building NJ Driver Time spreadsheet..."

					lnRow = 3
					lnStartRow = lnRow + 1
					lcStartRow = ALLTRIM(STR(lnStartRow))

					oWorksheet = oWorkbook.Worksheets[1]
					oWorksheet.RANGE("A" + lcStartRow,"P10000").clearcontents()

					oWorksheet.RANGE("A" + lcStartRow,"P10000").FONT.SIZE = 10
					oWorksheet.RANGE("A" + lcStartRow,"P10000").FONT.NAME = "Arial"
					oWorksheet.RANGE("A" + lcStartRow,"P10000").FONT.bold = .F.

					lcTitle = "NJ Driver Time" + LF + ;
						" for Pay Date: " + DTOC(ldPayDate) + LF + ;
						" Week of: " + lcStartDate + " - " + lcEndDate

					oWorksheet.RANGE("A1").VALUE = lcTitle

					* main scan/processing
					SELECT CURDRIVERS
					SCAN
						* NO CHANGES; just advance a line
						lnRow = lnRow + 1

						lcRow = LTRIM(STR(lnRow))
						oWorksheet.RANGE("A" + lcRow).VALUE = "'" + ALLTRIM(CURDRIVERS.DIVISION)
						oWorksheet.RANGE("B" + lcRow).VALUE = "'" + ALLTRIM(CURDRIVERS.DEPT)
						oWorksheet.RANGE("C" + lcRow).VALUE = CURDRIVERS.NAME
						oWorksheet.RANGE("D" + lcRow).VALUE = CURDRIVERS.FILE_NUM
						oWorksheet.RANGE("E" + lcRow).VALUE = CURDRIVERS.WORKDATE
						oWorksheet.RANGE("F" + lcRow).VALUE = CURDRIVERS.TOTHOURS

						* hightlight hours if they worked at least 5.5 hours but less than 8, which per Mike Drew means they need Guaranteed Hours added.
						lnTotHours = CURDRIVERS.TOTHOURS
						IF (lnTotHours >= 5.50) AND (lnTotHours < 8.0) THEN
							oWorksheet.RANGE("A" + lcRow,"F" + lcRow).FONT.ColorIndex = 3
							oWorksheet.RANGE("A" + lcRow,"F" + lcRow).FONT.bold = .T.
						ENDIF

					ENDSCAN


					oWorksheet.PageSetup.PrintArea = "$A$"  + lcStartRow + ":$F$" + lcRow

					*******************************************************************************************
					*******************************************************************************************

					* save again
					IF llSaveAgain THEN
						oWorkbook.SAVE()
					ENDIF
					oWorkbook.CLOSE()

					oExcel.QUIT()

					lcTopBodyText = "Shifts with hours between 5.5 and 8.0 are bolded and highlighted in red."


					IF FILE(lcXLFileName) THEN
						.cAttach = lcXLFileName
						.TrackProgress('Attached file : ' + lcXLFileName, LOGIT+SENDIT)
						.TrackProgress('NJ Driver Time process ended normally.', LOGIT+SENDIT+NOWAITIT)
					ELSE
						.TrackProgress('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!', LOGIT+SENDIT)
						.TrackProgress("ERROR: There was a problem attaching file : " + lcXLFileName, LOGIT+SENDIT)
						.TrackProgress('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!', LOGIT+SENDIT)
					ENDIF

					IF .lTestMode THEN
						.cBodyText = lcTopBodyText + CRLF + CRLF + "<processing log follows>" + CRLF + CRLF + .cBodyText
					ELSE
						.cBodyText = lcTopBodyText + CRLF + CRLF + .cBodyText
					ENDIF

				ENDIF  && EOF() CURDRIVERS


			CATCH TO loError

				.TrackProgress('There was an error.',LOGIT+SENDIT)
				.TrackProgress('The Error # is: ' + TRANSFORM(loError.ERRORNO), LOGIT+SENDIT)
				.TrackProgress('The Error Message is: ' + TRANSFORM(loError.MESSAGE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred in Program: ' + TRANSFORM(loError.PROCEDURE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred at line #: ' + TRANSFORM(loError.LINENO), LOGIT+SENDIT)
				lnNumberOfErrors = lnNumberOfErrors + 1
				IF TYPE('oExcel') = 'O' AND NOT ISNULL(oExcel) THEN
					oExcel.QUIT()
				ENDIF
				*CLOSE DATA
				*CLOSE ALL

			ENDTRY

			CLOSE DATA
			WAIT CLEAR
			***************** INTERNAL email results ******************************
			.TrackProgress('About to send status email.',LOGIT)
			.TrackProgress('==================================================================================================================', SENDIT)
			.TrackProgress('The logfile is: ' + .cLogFile, SENDIT)
			.TrackProgress('Kronos NJ Driver Time process started: ' + .cStartTime, LOGIT+SENDIT)
			.TrackProgress('Kronos NJ Driver Time process finished: ' + TTOC(DATETIME()), LOGIT+SENDIT)

			IF .lSendInternalEmailIsOn THEN
				* try to trap error from not having dartmail dll's registered on user's pc...
				TRY
					DO FORM dartmail2 WITH .cSendTo,.cFrom,.cSubject,.cCC,.cAttach,.cBodyText,"A"
					.TrackProgress('Sent status email.',LOGIT)
				CATCH TO loError
					*=MESSAGEBOX('There was an error sending the status email. Please contact Mark Bennett.',0+64,"Kronos NJ Driver Time")
					.TrackProgress('There was an error sending the status email.',LOGIT)
					.TrackProgress('The Error # is: ' + TRANSFORM(loError.ERRORNO), LOGIT)
					.TrackProgress('The Error Message is: ' + TRANSFORM(loError.MESSAGE), LOGIT)
					.TrackProgress('The Error occurred in Program: ' + TRANSFORM(loError.PROCEDURE), LOGIT)
					.TrackProgress('The Error occurred at line #: ' + TRANSFORM(loError.LINENO), LOGIT)
					lnNumberOfErrors = lnNumberOfErrors + 1
					*CLOSE DATA
				ENDTRY

			ELSE
				.TrackProgress('Did not send status email: THIS.lSendInternalEmailIsOn = FALSE.',LOGIT)
			ENDIF

		ENDWITH
		RETURN
	ENDFUNC && main


	PROCEDURE TrackProgress
		* do any combination of Wait Window, writing to logfile, and adding to body of email,
		* based on nFlags parameter.
		LPARAMETERS tcExpression, tnFlags
		LOCAL lcCRLF
		WITH THIS
			IF BITAND(tnFlags,NOCRLF) = NOCRLF THEN
				lcCRLF = ""
			ELSE
				lcCRLF = CRLF
			ENDIF
			IF BITAND(tnFlags,LOGIT) = LOGIT THEN
				IF .lLoggingIsOn THEN
					?
					? .cProcessName + "  " + TTOC(DATETIME()) + ": " + tcExpression
				ENDIF
			ENDIF
			IF .lWaitWindowIsOn THEN
				IF BITAND(tnFlags,NOWAITIT) = NOWAITIT THEN
					WAIT WINDOW tcExpression NOWAIT
				ENDIF
				IF BITAND(tnFlags,WAITIT) = WAITIT THEN
					WAIT WINDOW tcExpression TIMEOUT .nWaitWindowTimeout
				ENDIF
			ENDIF
			IF BITAND(tnFlags,SENDIT) = SENDIT THEN
				IF .lSendInternalEmailIsOn THEN
					.cBodyText = .cBodyText + tcExpression + CRLF + lcCRLF
				ENDIF
			ENDIF
		ENDWITH
	ENDPROC  &&  TrackProgress


	FUNCTION CreatePDFReport
		* create PDF report
		LPARAMETERS tcRootPDFFilename, tcPDFReportFRX

		WITH THIS

			.TrackProgress('in CreatePDFReport tcRootPDFFilename=' + tcRootPDFFilename, LOGIT)
			.TrackProgress('in CreatePDFReport tcPDFReportFRX=' + tcPDFReportFRX, LOGIT)

			LOCAL llPDFReportWasCreated, lcRootPDFFilename, lcFullPDFFilename, lcPDFReportFRX
			lcCentury = SET("CENTURY")

			SET CENTURY OFF  && FOR XX/XX/XX FORMAT DATES ON REPORT
			lcRootPDFFilename = tcRootPDFFilename
			lcPDFReportFRX = tcPDFReportFRX

			lcFullPDFFilename = lcRootPDFFilename + ".PDF"

			llPDFReportWasCreated = .F.

			DO M:\DEV\PRG\RPT2PDF WITH lcPDFReportFRX, lcRootPDFFilename, llPDFReportWasCreated

			IF NOT llPDFReportWasCreated THEN
				.TrackProgress('ERROR - PDF file [' + lcFullPDFFilename + '] was not created',LOGIT+WAITIT+SENDIT)
			ENDIF

			*!*				* keep track of pdf report files for emailing later
			*!*				.AddAttachment(lcFullPDFFilename)

			SET PRINTER TO DEFAULT
			SET CENTURY &lcCentury
		ENDWITH
	ENDFUNC  &&  CreatePDFReport


ENDDEFINE
