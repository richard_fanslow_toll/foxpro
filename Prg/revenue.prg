* revenue.prg
*
* display total revenue and no. of shipments per
*   origin/destination for a given month
*

xacctname=acctname(xaccountid)

xsqlexec("select * from invoice",,,"ar")
index on wo_num tag wo_num
set order to

use f:\wo\wodata\wolog alias mainlog in 0

xsqlexec("select * from pudlocs",,,"wo")
index on accountid tag accountid
set order to

xstartdt=month2dt(nmonth)
xenddt=gomonth(xstartdt,1)-1

xquery="select wo_num, origin, destination from fxwolog " + ;
	"where accountid="+transform(xaccountid)+" and betwen(invdt,{"+dtoc(xstartdt)+"},{"+dtoc(xenddt)+"})"

xsqlexec(xquery,"fxlog",,"fx")

select i.*, m.office, iif(!isnull(f.origin), f.origin, iif(empty(m.fromloc), ;
	padr("OCEAN",30), fromloc.acct_name)) as "Origin", ;
	iif(!isnull(f.destination), f.destination, iif(empty(m.toloc), ;
	padr(alltrim(upper(o.city))+", "+o.state, 30), toloc.acct_name)) as "Destination" ;
	from invoice i ;
	left join fxlog f         on f.wo_num = i.wo_num ;
	left join mainlog m       on m.wo_num = i.wo_num ;
	left join pudlocs toloc   on str(toloc.accountid,4)+str(toloc.dash,4) = str(xaccountid,4)+str(m.toloc,4) ;
	left join pudlocs fromloc on str(fromloc.accountid,4)+str(fromloc.dash,4) = str(xaccountid,4)+str(m.fromloc,4) ;
	left join offices o       on o.locale = m.office ;
	group by invoiceid ;
	where i.accountid = xaccountid and month(i.invdt)= nmonth and ;
	year(i.invdt) = nyear and !i.void and !empty(i.wo_num) ;
	into cursor csrdata

select *, sum(invamt) as "Revenue", cnt(1) as "TotCnt" from csrdata ;
	group by office, origin, destination ;
	into cursor csrinvoice

insert into pdf_file (unique_id) values (reports.unique_id)

if eof() &&need something to distinguish from "poller down"
	select pdf_file
	replace memo with "NO DATA"
else
	public oexcel
	oexcel = createobject("Excel.Application")
	oexcel.displayalerts = .f.

	xfile = tempfox+alltrim(str(reports.unique_id))+".xls"
	copy file f:\webrpt\revenue.xls to (xfile)

	oworkbook = oexcel.workbooks.open(xfile)
	oexcel.activesheet.range("A2").value = "Monthly Revenue For: "+padl(ltrim(str(nmonth,2,0)),2,"0")+"/"+str(nyear,4,0)
	oexcel.activesheet.range("A3").value = "Account: "+alltrim(xacctname)

	lshaded = .t.
	nrownum = 7
	select csrinvoice
	scan
		if !isnull(origin)
			crownum = alltrim(str(nrownum, 5, 0))

			with oexcel.activesheet
				.range("A"+crownum).value = origin
				.range("B"+crownum).value = destination
				.range("C"+crownum).value = totcnt
				.range("D"+crownum).value = revenue
			endwith

			if lshaded
				oexcel.activesheet.range("A"+crownum+":"+"D"+crownum).interior.color = rgb(179,219,234)
			endif

			lshaded = !lshaded
			nrownum = nrownum+1
		endif
	endscan

	oworkbook.save()
	oworkbook.close()
	oexcel.quit()
	release oworkbook, oexcel

	select pdf_file
	**in order to avoid "VFPOLEDB" "provider ran out of memory" errors for large files (>2mb) no longer move file 
	**  thru linked server, just copy file directly to \\iis2\apps\CargoTrackingWeb\reports\ - mvw 03/08/13
	*append memo memo from &xfile overwrite
	copy file "&xfile" to \\iis2\apps\CargoTrackingWeb\reports\*.*
	replace memo with "FILE COPIED TO REPORT FOLDER" in pdf_file
endif

use in invoice
use in csrinvoice
use in fxlog
use in mainlog
use in pudlocs

set database to ar
close databases

set database to fx
close databases
