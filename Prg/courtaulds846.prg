*lparameter lcoffice, xaccountid, xtest
utilsetup("COURTAULDS846")
set exclusive off
set deleted on
set talk off
set safety off   &&Allows for overwrite of existing files

close databases all
lcoffice='M'
xaccountid=4677
xtest=.f.
*utilsetup("SENDINVENTORY")
xscreencaption=_screen.caption
*_screen.caption="SENDINVENTORY for Account: "+xaccountid+" and Office: "+lcOffice

guserid="SENDINVENTORY"
gprocess=guserid
gdevelopment=.f.

*xaccountid = val(xaccountid)
lcwhpath = "f:\whm\whdata\"
xmod = 'M'
goffice=xmod

if !xtest
*!*		if usesqloutwo()
*!*
*!*		else
*!*			use f:\whm\whdata\outship in 0 order acctpt
*!*			use f:\whm\whdata\outdet in 0 order outshipid
*!*		endif

	xsqlexec("select * from inven where mod='"+goffice+"'",,,"wh")
else
	use f:\tmptmp\paulg\inven in 0
	use f:\tmptmp\paulg\outship in 0
	use f:\tmptmp\paulg\outdet in 0
	use f:\tmptmp\paulg\project in 0
endif

* get inventory

select units, style, color, id, pack, availqty, allocqty, holdqty, 0000000 as pickedqty, 0000000 as spqty ;
from inven where accountid=4677 into cursor inventory readwrite
set step on
index on transform(units)+style+color+id+pack tag match

* inprocess is a cursor of all outship records where they are not yet delivered and also excluding "notonwip"

xsqlexec("select * from outship where mod='"+goffice+"' and accountid=4677 " + ;
"and del_date={} and notonwip=0 and qty#0 order by wo_num","inprocess",,"wh")

xsqlexec("select units, style, color, id, pack, totqty from outship, outdet " + ;
"where outship.outshipid=outdet.outshipid " + ;
"and outship.mod='"+goffice+"' and outship.accountid=4677 " + ;
"and del_date={} and notonwip=0 and qty#0","xdy",,"wh")

select units, style, color, id, pack, sum(totqty) as pickedqty ;
	from xdy group by units, style, color, id, pack into cursor xunshipped

select xunshipped
scan
	=seek(transform(units)+style+color+id+pack,"inventory","match")
	replace pickedqty with xunshipped.pickedqty in inventory
endscan


* uncompleted special projects

xsqlexec("select outdet.wo_num, units, style, color, id, pack, totqty " + ;
"from outdet, project where outdet.mod='"+goffice+"' " + ;
"and outdet.wo_num=project.wo_num and completeddt={} and outdet.accountid=4677","xdy",,"wh")

select wo_num, units, style, color, id, pack, sum(totqty) as spqty ;
from xdy group by 1,2,3,4,5,6 into cursor xsp

select xsp
scan
	=seek(transform(units)+style+color+id+pack,"inventory","match")
	replace spqty with xsp.spqty in inventory
endscan

select inventory
delete for availqty=0 and pickedqty=0 and holdqty=0 and spqty=0

* copy and email inventory file

lcfilename =  "COURTAULDS_"+lcoffice+"_"+ttoc(datetime(),1)+".846"
lcfilename2 = "COURTAULDS_"+lcoffice+"_inproc_"+ttoc(datetime(),1)+".846"
tsendto = 'pgaidis@fmiint.com,jackie.barrett@courtaulds.com'

xlsbasefilename = justfname(lcfilename)
xlsfilename= "f:\ftpusers\Courtaulds\846out\"+xlsbasefilename+".xls"
xlsfilenamearchive= "f:\ftpusers\Courtaulds\846out\archive\"+xlsbasefilename+".xls"

csvfilename = justfname(lcfilename)
delimitedfilename= justfname(lcfilename)
csvfilename= "f:\ftpusers\Courtaulds\846out\"+csvfilename+".csv"
delimitedfilename= "f:\ftpusers\Courtaulds\846out\"+delimitedfilename+".csv"

export to &xlsfilename fields style,color,id,pack,availqty,pickedqty,holdqty,spqty for units = .t. type xls
export to &xlsfilenamearchive fields style,color,id,pack,availqty,pickedqty,holdqty,spqty for units = .t. type xls
copy to &delimitedfilename fields style,color,id,pack,availqty,pickedqty,holdqty,spqty for units = .t. delimited
set step on
tsubject= "Courtaulds Inventory Sent: " +ttoc(datetime())
tmessage = "Updated inventory file in the Courtaulds FTP Site.............."+chr(13)+chr(13)+;
"This email is generated via an automated process"+chr(13)+"For changes in content or distribution please contact: Paul Gaidis @ 732-750-9000x143 "

*

tattach = xlsfilename

tfrom ="TGF Corporate <fmicorporate@fmiint.com>"

if !empty(tattach)
	do form dartmail2 with tsendto,tfrom,tsubject,"",tattach,tmessage,"A"
endif

schedupdate()

on error
_screen.caption=xscreencaption