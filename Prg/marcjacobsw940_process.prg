PARAMETERS cOffice
lEmail = .T.

CD &lcPath
lc997Path = ("f:\ftpusers\marcjacobs\940translate\")

*!*	waitstr = "Processing records for "+cProperName+" - "+cOfficename
*!*	WAIT WINDOW waitstr TIMEOUT WAITTIME

IF lTesting
	lnNum = ADIR(tarray,"*.int")
ELSE
	lnNum = ADIR(tarray,"*.int")
ENDIF

WAIT WINDOW "There are "+TRANSFORM(lnNum)+" files to process" TIMEOUT 2
 
IF lnNum = 0
	WAIT WINDOW AT 10,20 "No "+cProperName+" 940's to import for "+cOfficename TIMEOUT 3
*	SET STEP ON
	NormalExit = .T.
	THROW
ENDIF
*SET STEP ON
FOR runfiles = 1  TO lnNum
	cFilename = ALLTRIM(tarray[runfiles,1])
	nFileSize = VAL(TRANSFORM(tarray(runfiles,2)))
	tFileDT = DTOT(tarray[runfiles,3])
	cTime = ALLT(tarray[runfiles,4])
	xfile = lcPath+cFilename
	archivefile = lcArchivePath+cFilename
	in997file = ("F:\ftpusers\marcjacobs\997in\"+JUSTSTEM(xfile)+".997")
	in943file = ("F:\FTPUSERS\MJ_Wholesale\WMS_Inbound\943IN\"+cFilename)
	missingUPC= "F:\FTPUSERS\MJ_Wholesale\WMS_Outbound\MissingUPCs\"+cFilename


	SELECT x856

	archivefile = lcArchivePath+cFilename
	to997file = lc997Path+cFilename
	WAIT WINDOW "Importing file: "+xfile NOWAIT
	IF FILE(xfile)
		lcStrx = FILETOSTR(xfile)
		cDelimiter = SUBSTR(lcStrx,4,1)
		RELEASE ALL LIKE lcStrx

*!* To load from a wrapped file snippet, uncomment the following section,
*!* and comment the "loadedifile" line below it
*	ASSERT .f. MESSAGE "at createx856a"
*	DO m:\dev\prg\createx856a
*	APPEND FROM f:\0-picktickets\marcjacobsw\20080414104840b.940 TYPE DELIMITED WITH CHARACTER "*"

		DO m:\dev\prg\loadedifile WITH xfile,cDelimiter,cTranslateOption,cCustName

		SELECT x856
		LOCATE FOR x856.segment = "GS"
		IF FOUND()
			DO CASE
				CASE x856.f1 = "AR"
					COPY FILE [&xfile] TO [&in943file]
					DELETE FILE [&xfile]
					LOOP

				CASE x856.f1 = "FA"  && 997 file
					COPY FILE [&xfile] TO [&in997file]
					DELETE FILE [&xfile]
					LOOP
			ENDCASE
		ENDIF
		COPY FILE [&xfile] TO [&to997file]

		RELEASE ALL LIKE APT
		RELEASE ALL LIKE APTDET

*		INDEX ON upc TAG upc

		IF USED('xptdet')
			USE IN xptdet
		ENDIF
		IF USED('xpt')
			USE IN xpt
		ENDIF

		IF lLoadSQL
			xsqlexec("select * from pt where .f.","x1pt",,"wh")
			SELECT *,SPACE(30) AS ERRORMSG, .F. AS ERRORFLAG FROM X1PT INTO CURSOR XPT READWRITE
			xsqlexec("select * from ptdet where .f.","xptdet",,"wh")
		ELSE
			USE (cUseFolder+"PTDET") IN 0
			SELECT * FROM ptdet WHERE .F. INTO CURSOR xptdet READWRITE
			USE (cUseFolder+"PT") IN 0
			SELECT *,SPACE(30) AS errormsg,.F. AS errorflag FROM pt WHERE .F. INTO CURSOR xpt READWRITE  && added the error fields to selectively eliminate single bad PTs PG 9/06/2013
		ENDIF
		SELECT xpt
		INDEX ON ship_ref TAG ship_ref
		INDEX ON ptid TAG ptid
		SET ORDER TO ship_ref

		SELECT x856
		COUNT TO nST FOR x856.segment = "ST"

		lDoImport = .T.

		DO (cUseDir+"BKDNax2012")

		IF !EMPTY(cPTDupes)
			dupeptmail()
			cPTDupes = ""
		ENDIF

		IF lDoImport
			DO m:\dev\prg\all940_import
		ENDIF

	*	release_ptvars()

	ENDIF

*	COPY FILE [&xfile] TO [&archivefile]

	IF FILE(archivefile) AND !lTesting
		DELETE FILE [&xfile]
	ENDIF

	IF USED('xpt')
		USE IN xpt
	ENDIF
	IF USED('xptdet')
		USE IN xptdet
	ENDIF
ENDFOR

&& now clean up the 940in archive files, delete for the upto the last 10 days
**deletefile(lcArchivePath,IIF(DATE()<{^2016-05-01},20,10))

*!*	WAIT CLEAR
*!*	WAIT WINDOW "ALL "+UPPER(cProperName)+" 940 files finished processing for: "+cOfficename+"..." TIMEOUT WAITTIME
RETURN

***************************
PROCEDURE dupeptmail
***************************
	IF !lTesting
		SELECT 0
		USE F:\3pl\DATA\mailmaster ALIAS mm
		LOCATE FOR GROUP = "MARCJACOBS" AND taskname = 'MJPLERROR'
		STORE TRIM(IIF(mm.use_alt,mm.sendtoalt,mm.sendto)) TO tsendto
		STORE TRIM(IIF(mm.use_alt,mm.ccalt,mm.cc)) TO tcc
		USE IN mm
	ELSE
		tsendto = tsendtoerr
		tcc = tccerr
	ENDIF

	tsubject = "Error: Marc Jacobs Duplicate Pickticket"
	tmessage = "Marc Jacobs (W) 940 Upload Error!"
	tmessage = tmessage+CHR(13)+"The following PTs from file "+cFilename+" were duplicates, and skipped in processing:"
	tmessage = tmessage+CHR(13)+cPTDupes
	tattach = ""
	tfrom    ="Toll EDI Processing Center <toll-edi-ops@tollgroup.com>"
	DO FORM m:\dev\frm\dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
ENDPROC
