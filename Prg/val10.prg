* validate WMS data during the day - every 30 minutes

lparameters xskipreports

utilsetup("VAL10")

if holiday(date())
	return
endif

store "" to guserid, gprocess
gdevelopment=.f.
xjobno=0
xsystem="VAL10"

external array xdbf
 
use f:\auto\val10 exclusive
zap
use


*

valset("check for incorrect detleftqty and detstageleftqty in swcdet")

useca("swcdet","wh",,,"start>{"+dtoc(date()-30)+"} and notonwipqty=0 and " + ;
	"(detexpqty-detlabelqty-detcutqty#detleftqty or detexpqty-detstageqty-detcutqty-notonwipqty#detstageleftqty) and wo_num#6526605")

scan
	xleftqty=max(0,swcdet.detexpqty-swcdet.detlabelqty-swcdet.detcutqty-swcdet.notonwipqty)
	xstageleftqty=max(0,swcdet.detexpqty-swcdet.detstageqty-swcdet.detcutqty-swcdet.notonwipqty)
	replace detleftqty with xleftqty, detstageleftqty with xstageleftqty in swcdet
endscan

if reccount()>0
	tu("swcdet")
	copy to ("f:\auto\xswcleftqty")
	email("Dyoung@fmiint.com","VAL "+transform(reccount())+" incorrect detleftqty and detstageleftqty in swcdet ","xswcleftqty",,,,.t.,,,,,.t.)

	useca("swcdet","wh",,,"start>{"+dtoc(date()-30)+"} and notonwipqty=0 and " + ;
		"(detexpqty-detlabelqty-detcutqty#detleftqty or detexpqty-detstageqty-detcutqty-notonwipqty#detstageleftqty)")

	if reccount()>0
		copy to ("f:\auto\xswcleftqtybad")
		email("Dyoung@fmiint.com","VAL "+transform(reccount())+" incorrect detleftqty and detstageleftqty in swcdet ","xswcleftqtybad",,,,.t.,,,,,.t.)
	endif
endif

*

valset("check for staged Rag & Bone WOs not marked as staged")

xsqlexec("select * from outwolog where 1=0","xrpt",,"wh")

useca("outwolog","wh",,,"accountid=6699 and staged=1 and stageleftq=0")

scan 
	if xsqlexec("select wo_num from outship where wo_num="+transform(outwolog.wo_num)+" and notonwip=0 and staged={} and ctnqty#0",,,"wh") # 0
		select outwolog
		scatter memvar
		insert into xrpt from memvar
		xsqlexec("update outship set staged={"+dtoc(date())+"} where wo_num="+transform(outwolog.wo_num)+" and notonwip=0 and staged={}",,,"wh")
	endif
endscan

select xrpt	
count to aa
	
if aa>0
	copy to ("f:\auto\xragandbone")
	email("Dyoung@fmiint.com","VAL (fixed) "+transform(aa)+" staged Rag & Bone WOs not marked as staged","xragandbone",,,,.t.,,,,,.t.)
endif

*

valset("check for mismatched wavenum/waveid in tote")

useca("tote","wh",,,"adddt>{"+dtoc(date()-2)+"}")

select space(14) as reason, * from tote where .f. into cursor xrpt readwrite

select tote
scan 
	if xsqlexec("select waveid, wavenum from wave where waveid="+transform(tote.waveid),,,"wh") = 0 
		select tote
		scatter memvar
		m.reason="no wave record"
		insert into xrpt from memvar
	else
		if wave.wavenum#tote.wavenum
			select tote
			scatter memvar
			m.reason="bad wavenum"
			insert into xrpt from memvar
			replace wavenum with wave.wavenum in tote
		endif
	endif
endscan

select xrpt
count to aa

if aa>0
	tu("tote")
	copy to f:\auto\xmistotwave
	email("Dyoung@fmiint.com","VAL "+transform(aa)+" mismatched wavenum/waveid in tote ","xmistotwave",,,,.t.,,,,,.t.)
endif

*

valset("fix bad outbound & swc data")

xsqlexec("select * from account where inactive=0","account",,"qq")
index on accountid tag accountid
set order to

select whoffice
scan for !test and !novalid and picknpack
	xoffice=whoffice.office
	goffice=xoffice
	xfolder=whoffice.folder+"\whdata\"
	xpnpaccount=pnpacct(xoffice)
	gmasteroffice=whoffice.rateoffice

	xsqlexec("select * from outwolog where mod='"+goffice+"' and swctype#'S' and "+xpnpaccount+" and staged=1 and splitpt=0 and wo_date>{"+dtoc(date()-12)+"}",,,"wh")
	xsqlexec("select * from swcdet where 1=0",,,"wh")
	
	select swcdetid, wo_num, {} as wo_date, accountid, swcnum, swcover, .f. as staged, 000000 as swcqty, 000000 as outqty, ;
		000000 as woqty, 000000 as shipqty, 00000 as swcdetneg from swcdet where .f. into cursor xppswcout readwrite

	select outwolog
	scan for swctype#"S" and &xpnpaccount and staged
		if xsqlexec("select * from outship where wo_num="+transform(outwolog.wo_num)+" and masterpack=1",,,"wh") # 0
			loop
		endif

		locate for " OV"$ship_ref
		if found()
			loop
		endif
		
		xsqlexec("select * from swcdet where mod='"+goffice+"' and outwologid="+transform(outwolog.outwologid)+" and detexpqty#999",,,"wh")

		select swcdet
		scan
			xsqlexec("select ctnqty from outship where mod='"+goffice+"' and swcnum='"+swcdet.swcnum+"' and swcover='"+swcdet.swcover+"'",,,"wh")
			sum ctnqty to m.outqty
						
			if swcdet.detexpqty-swcdet.detcutqty#m.outqty
				select swcdet
				scatter memvar fields swcdetid, wo_num, swcnum, swcover, accountid
				m.wo_date=outwolog.wo_date
				m.staged=outwolog.staged
				m.swcqty=swcdet.detexpqty-swcdet.detcutqty
				insert into xppswcout from memvar
			endif
		endscan
		
		xsqlexec("select ctnqty from outship where wo_num="+transform(outwolog.wo_num),,,"wh")
		sum ctnqty to xqty
		if xqty#outwolog.ctnqty
			select xppswcout
			locate for wo_num=outwolog.wo_num
			if !found()
				insert into xppswcout (wo_num, woqty, shipqty) value (outwolog.wo_num, outwolog.ctnqty, xqty)
			endif
		endif
		
		if xsqlexec("select * from swcdet where mod='"+goffice+"' and outwologid="+transform(outwolog.outwologid)+" and detlabelqty>detexpqty and detexpqty#999",,,"wh") > 0
			insert into xppswcout (wo_num, swcdetneg) value (outwolog.wo_num, swcdet.detlabelqty-swcdet.detexpqty)
		endif
	endscan

	select xppswcout
	if reccount()>0
		use in outwolog
		use in outship
		use in swcdet
		
		scan
			useca("outwolog","wh",,,xppswcout.wo_num)
			useca("outship","wh",,,xppswcout.wo_num)
			useca("outdet","wh",,,xppswcout.wo_num)
			useca("swcdet","wh",,,xppswcout.wo_num)
			useca("swcstyle","wh",,,xppswcout.wo_num)
			
			select outship
			scan
				select outdet
				sum totqty to xx for outshipid=outship.outshipid and units
				if outship.qty # xx
					replace qty with xx in outship
				endif
			endscan

			select outship
			sum ctnqty, qty to xx, yy
			if outwolog.ctnqty # xx
				replace ctnqty with xx in outwolog
			endif
			if outwolog.quantity # yy
				replace quantity with yy in outwolog
			endif

			select outship
			scan
				select outdet
				if outwolog.picknpack
					sum totqty, totcube, totwt to ww, xx, yy for outshipid=outship.outshipid and units
				else
					sum totqty, totcube, totwt to ww, xx, yy for outshipid=outship.outshipid and !units
				endif
					
				if outship.ctnqty # ww and !outwolog.picknpack and !outship.masterpack 
					replace ctnqty with ww in outship
				endif
				if outship.qty # ww and !picknpack(outship.accountid)
					replace qty with ww in outship
				endif
				if outship.cuft # xx and !outwolog.picknpack
					replace cuft with xx in outship
				endif
				if outship.weight # yy and !outwolog.picknpack and !outship.masterpack
					replace weight with yy in outship
				endif	
			endscan

			if !outwolog.picknpack
				xwonum=outwolog.wo_num

				select swcnum, ship_ref, style, color, id, pack, totqty as outqty, 000000 as swcqty ;
					from outwolog, outship, outdet ;
					where outwolog.outwologid=outship.outwologid ;
					and outship.outshipid=outdet.outshipid ;
					and outwolog.wo_num=xwonum ;
					and !units ;
					into cursor xrpt readwrite
					
				index on ship_ref+style+color+id+pack tag zorder

				select swcstyle
				scan for outwologid=outwolog.outwologid
					if seek(ship_ref+style+color+id+pack,"xrpt","zorder")
						replace swcqty with swcstyle.shipqty in xrpt
					else
						scatter memvar
						m.outqty=0
						m.swcqty=shipqty
						insert into xrpt from memvar
					endif
				endscan

				select xrpt
				scan for outqty#swcqty
					xrecalc2=.t.

					if outqty<swcqty
						replace stylecutqty with swcstyle.stylecutqty+xrpt.swcqty-xrpt.outqty, ;
								shipqty with swcstyle.styleexpqty-swcstyle.stylecutqty in swcstyle ;
								for ship_ref=xrpt.ship_ref and style=xrpt.style and color=xrpt.color and id=xrpt.id and pack=xrpt.pack
								
					else
						select swcstyle
						locate for ship_ref=xrpt.ship_ref and style=xrpt.style and color=xrpt.color and id=xrpt.id and pack=xrpt.pack
						if !found()
							select swcstyle
							scatter memvar memo blank
							m.office=gmasteroffice
							m.mod=goffice
							m.scandt=.null.
							
							select xrpt
							scatter memvar fields ship_ref, style, color, id, pack
							m.styleexpqty=xrpt.outqty
							m.shipqty=xrpt.outqty
							m.outwologid=outwolog.outwologid
							m.wo_num=outwolog.wo_num
							m.accountid=outwolog.accountid
							select swcdet
							locate for swcnum=xrpt.swcnum
							m.swcdetid=swcdet.swcdetid
							m.swcstyleid=dygenpk("swcstyle","whall")
							insert into swcstyle from memvar
						else
							select swcstyle
							scan for ship_ref=xrpt.ship_ref and style=xrpt.style and color=xrpt.color and id=xrpt.id and pack=xrpt.pack
								if stylecutqty>=xrpt.outqty-xrpt.swcqty
									replace stylecutqty with swcstyle.stylecutqty-xrpt.outqty+xrpt.swcqty in swcstyle
								else		
									replace styleexpqty with swcstyle.styleexpqty+xrpt.outqty-xrpt.swcqty in swcstyle
								endif
								replace shipqty with swcstyle.styleexpqty-swcstyle.stylecutqty in swcstyle
							endscan
						endif
					endif
				endscan
			else
				xwonum=outwolog.wo_num

				select swcnum, ship_ref, outship.ctnqty as outqty, 000000 as swcqty ;
					from outwolog, outship ;
					where outwolog.outwologid=outship.outwologid ;
					and outwolog.wo_num=xwonum ;
					into cursor xrpt readwrite
					
				index on ship_ref tag zorder

				select swcstyle
				scan for outwologid=outwolog.outwologid
					if seek(ship_ref,"xrpt","zorder")
						replace swcqty with swcstyle.shipqty in xrpt
					else
						scatter memvar
						m.outqty=0
						m.swcqty=shipqty
						insert into xrpt from memvar
					endif
				endscan

				select xrpt
				scan for outqty#swcqty
					xrecalc2=.t.
					select swcstyle
					locate for ship_ref=xrpt.ship_ref

					if xrpt.outqty<xrpt.swcqty
						replace styleexpqty with xrpt.outqty, shipqty with xrpt.outqty in swcstyle
								
					else
						if !found("swcstyle")
							select swcstyle
							scatter memvar memo blank
							m.office=gmasteroffice
							m.mod=goffice
							m.scandt=.null.

							m.ship_ref=xrpt.ship_ref
							m.styleexpqty=xrpt.outqty
							m.shipqty=xrpt.outqty
							m.outwologid=outwolog.outwologid
							m.wo_num=outwolog.wo_num
							m.accountid=outwolog.accountid
							select swcdet
							
							select swcdet
							locate for swcnum=xrpt.swcnum
							m.swcdetid=swcdet.swcdetid
							m.swcstyleid=dygenpk("swcstyle","whall")
							insert into swcstyle from memvar
						else
							replace styleexpqty with xrpt.outqty, shipqty with xrpt.outqty in swcstyle
						endif
					endif
				endscan
			endif
			
			if !outwolog.picknpack and !outwolog.caseplus
				select swcdet
				scan
					select outship
					locate for swcnum=swcdet.swcnum and swcover=swcdet.swcover
					
					if !outship.masterpack
						select swcstyle
						sum styleexpqty, stylecutqty to xstyleexpqty, xstylecutqty for swcdetid=swcdet.swcdetid
						if xstyleexpqty#swcdet.detexpqty or xstylecutqty#swcdet.detcutqty 
							xrecalc=.t.

							replace detexpqty with xstyleexpqty, detcutqty with xstylecutqty in swcdet

							replace detleftqty with max(0,swcdet.detexpqty-swcdet.detlabelqty-swcdet.detcutqty-swcdet.notonwipqty), ;
									detstageleftqty with max(0,swcdet.detexpqty-swcdet.detlabelqty-swcdet.detcutqty-swcdet.notonwipqty) in swcdet
						endif
					else
						select outship
						sum ctnqty to xstyleexpqty for swcnum+swcover = swcdet.swcnum+swcdet.swcover
						if xstyleexpqty#swcdet.detexpqty
							xrecalc=.t.
							replace detexpqty with xstyleexpqty in swcdet

							replace detleftqty with max(0,swcdet.detexpqty-swcdet.detlabelqty-swcdet.detcutqty-swcdet.notonwipqty), ;
									detstageleftqty with max(0,swcdet.detexpqty-swcdet.detlabelqty-swcdet.detcutqty-swcdet.notonwipqty) in swcdet
						endif
					endif		
				endscan

			else
				select swcdet
				scan 
					xmj=.f.
					if inlist(swcdet.accountid,&gmjacctlist,6699) or gmasteroffice="L"	 && when scanpacking don't set detexpqty if not done packing the entire SWC
						select outship
						locate for swcnum+swcover=swcdet.swcnum+swcdet.swcover and ctnqty=0 and !masterpack and !notonwip
						if found()
							xmj=.t.
							if swcdet.detexpqty#999
								xrecalc=.t.
								replace detexpqty with 999 in swcdet

								replace detleftqty with max(0,swcdet.detexpqty-swcdet.detlabelqty-swcdet.detcutqty-swcdet.notonwipqty), ;
										detstageleftqty with max(0,swcdet.detexpqty-swcdet.detlabelqty-swcdet.detcutqty-swcdet.notonwipqty) in swcdet
							endif
						endif
					endif
					
					if !xmj
						select outship
						sum ctnqty to xstyleexpqty for swcnum+swcover=swcdet.swcnum+swcdet.swcover
						if xstyleexpqty#swcdet.detexpqty
							xrecalc=.t.
							replace detexpqty with xstyleexpqty, detcutqty with 0 in swcdet

							replace detleftqty with max(0,swcdet.detexpqty-swcdet.detlabelqty-swcdet.detcutqty-swcdet.notonwipqty), ;
									detstageleftqty with max(0,swcdet.detexpqty-swcdet.detlabelqty-swcdet.detcutqty-swcdet.notonwipqty) in swcdet
						endif
					endif
				endscan
			endif
			
			select outship
			locate for masterpack
			xmastpack=found()

			do case
			case xmastpack
				select outship
				sum ctnqty, cuft, weight to ww, xx, yy
			case outwolog.picknpack or picknpack(outwolog.accountid)
				select outdet
				sum totqty to ww for units
				select outship
				sum cuft, weight to xx, yy
			otherwise
				select outdet
				sum totqty, totcube, totwt to ww, xx, yy for !units
			endcase

			if outwolog.quantity # ww and !xmastpack and outwolog.accountid#6532
				replace quantity with ww in outwolog
			endif
			if outwolog.ctnqty # ww and !outwolog.picknpack and !picknpack(outwolog.accountid)
				replace ctnqty with ww in outwolog
			endif
			if outwolog.cuft # xx
				replace cuft with xx in outwolog
			endif
			if outwolog.weight # yy
				replace weight with yy in outwolog
			endif

			replace detstageleftqty with detexpqty-detstageqty for detstageleftqty#detexpqty-detstageqty in swcdet
			
			tu("outwolog")
			tu("outship")
			tu("outdet")
			tu("swcdet")
			tu("swcstyle")
		endscan
		
		copy to ("f:\auto\xoutswc"+xoffice)
		email("Dyoung@fmiint.com","VAL (fixed) "+transform(reccount())+" fixed bad outbound & swc data for "+xoffice,"xoutswc"+xoffice,,,,.t.,,,,,.t.)
	endif

	if used("outwolog")
		use in outwolog
	endif
	if used("outship")
		use in outship
	endif
	if used("outdet")
		use in outdet
	endif
	if used("swcdet")
		use in swcdet
	endif
	if used("swcstyle")
		use in swcstyle
	endif
endscan

*

valset("check for incorrect mod in inrcv")

xsqlexec("select * from modacct",,,"wh")
index on str(accountid,4)+office tag acctoff
set order to

xsqlexec("select * from inrcv where 1=0","xrpt",,"wh")

useca("inrcv","wh",,,"adddt>{"+dtoc(date()-1)+"} and inlist(office,'N','C')")
scan 
	=seek(str(accountid,4)+office,"modacct","acctoff")
	if inrcv.mod#modacct.mod
		scatter memvar 
		insert into xrpt from memvar
		replace mod with modacct.mod in inrcv
		tu("inrcv")
	endif
endscan

if reccount("xrpt")>0
	select xrpt
	copy to f:\auto\xmodinrcv
	email("Dyoung@fmiint.com","VAL "+transform(reccount("xrpt"))+" incorrect mod in inrcv","xmodinrcv",,,,.t.,,,,,.t.)
endif

*

valset("check for incorrect mod in adj")

xsqlexec("select * from modacct",,,"wh")
index on str(accountid,4)+office tag acctoff
set order to

xsqlexec("select * from adj where 1=0","xrpt",,"wh")

useca("adj","wh",,,"adjdt>{"+dtoc(date()-1)+"} and inlist(office,'N','C')")
scan 
	=seek(str(accountid,4)+office,"modacct","acctoff")
	if adj.mod#modacct.mod
		scatter memvar 
		insert into xrpt from memvar
		replace mod with modacct.mod in adj
		tu("adj")
	endif
endscan

if reccount("xrpt")>0
	select xrpt
	copy to f:\auto\xmodadj
	email("Dyoung@fmiint.com","VAL "+transform(reccount("xrpt"))+" incorrect mod in adj","xmodadj",,,,.t.,,,,,.t.)
endif

*

valset("check for incorrect mod in inven")

xsqlexec("select * from modacct",,,"wh")
index on str(accountid,4)+office tag acctoff
set order to

xsqlexec("select * from inven where 1=0","xrpt",,"wh")

useca("inven","wh",,,"adddt>{"+dtoc(date()-1)+"} and inlist(office,'N','C')")
scan 
	=seek(str(accountid,4)+office,"modacct","acctoff")
	if inven.mod#modacct.mod
		scatter memvar 
		insert into xrpt from memvar
		replace mod with modacct.mod in inven
		tu("inven")
	endif
endscan

if reccount("xrpt")>0
	select xrpt
	copy to f:\auto\xmodinven
	email("Dyoung@fmiint.com","VAL "+transform(reccount("xrpt"))+" incorrect mod in inven","xmodinven",,,,.t.,,,,,.t.)
endif

*

valset("check for incorrect mod in invenloc")

xsqlexec("select * from modacct",,,"wh")
index on str(accountid,4)+office tag acctoff
set order to

xsqlexec("select * from invenloc where 1=0","xrpt",,"wh")

useca("invenloc","wh",,,"adddt>{"+dtoc(date()-1)+"} and inlist(office,'N','C')")
scan 
	=seek(str(accountid,4)+office,"modacct","acctoff")
	if invenloc.mod#modacct.mod
		scatter memvar 
		insert into xrpt from memvar
		replace mod with modacct.mod in invenloc
		tu("invenloc")
	endif
endscan

if reccount("xrpt")>0
	select xrpt
	copy to f:\auto\xmodinvenloc
	email("Dyoung@fmiint.com","VAL "+transform(reccount("xrpt"))+" incorrect mod in invenloc","xmodinvenloc",,,,.t.,,,,,.t.)
endif

*

valset("check for incorrect mod in indet")

xsqlexec("select * from modacct",,,"wh")
index on str(accountid,4)+office tag acctoff
set order to

xsqlexec("select * from indet where 1=0","xrpt",,"wh")

useca("indet","wh",,,"adddt>{"+dtoc(date()-1)+"} and inlist(office,'N','C')")
scan 
	=seek(str(accountid,4)+office,"modacct","acctoff")
	if indet.mod#modacct.mod
		scatter memvar 
		insert into xrpt from memvar
		replace mod with modacct.mod in indet
		tu("indet")
	endif
endscan

if reccount("xrpt")>0
	select xrpt
	copy to f:\auto\xmodindet
	email("Dyoung@fmiint.com","VAL "+transform(reccount("xrpt"))+" incorrect mod in indet","xmodindet",,,,.t.,,,,,.t.)
endif

*

valset("check for incorrect mod in inloc")

xsqlexec("select * from modacct",,,"wh")
index on str(accountid,4)+office tag acctoff
set order to

xsqlexec("select * from inloc where 1=0","xrpt",,"wh")

useca("inloc","wh",,,"adddt>{"+dtoc(date()-1)+"} and inlist(office,'N','C')")
scan 
	=seek(str(accountid,4)+office,"modacct","acctoff")
	if inloc.mod#modacct.mod
		scatter memvar 
		insert into xrpt from memvar
		replace mod with modacct.mod in inloc
		tu("inloc")
	endif
endscan

if reccount("xrpt")>0
	select xrpt
	copy to f:\auto\xmodinloc
	email("Dyoung@fmiint.com","VAL "+transform(reccount("xrpt"))+" incorrect mod in inloc","xmodinloc",,,,.t.,,,,,.t.)
endif

*

valset("check for incorrect mod in outdet")

xsqlexec("select * from modacct",,,"wh")
index on str(accountid,4)+office tag acctoff
set order to

xsqlexec("select * from outdet where 1=0","xrpt",,"wh")

useca("outdet","wh",,,"adddt>{"+dtoc(date()-1)+"} and inlist(office,'N','C')")
scan 
	=seek(str(accountid,4)+office,"modacct","acctoff")
	if outdet.mod#modacct.mod
		scatter memvar 
		insert into xrpt from memvar
		replace mod with modacct.mod in outdet
		tu("outdet")
	endif
endscan

if reccount("xrpt")>0
	select xrpt
	copy to f:\auto\xmodoutdet
	email("Dyoung@fmiint.com","VAL "+transform(reccount("xrpt"))+" incorrect mod in outdet","xmodoutdet",,,,.t.,,,,,.t.)
endif

*

valset("check for incorrect mod in outloc")

xsqlexec("select * from modacct",,,"wh")
index on str(accountid,4)+office tag acctoff
set order to

xsqlexec("select * from outloc where 1=0","xrpt",,"wh")

useca("outloc","wh",,,"adddt>{"+dtoc(date()-1)+"} and inlist(office,'N','C')")
scan 
	=seek(str(accountid,4)+office,"modacct","acctoff")
	if outloc.mod#modacct.mod
		scatter memvar 
		insert into xrpt from memvar
		replace mod with modacct.mod in outloc
		tu("outloc")
	endif
endscan

if reccount("xrpt")>0
	select xrpt
	copy to f:\auto\xmodoutloc
	email("Dyoung@fmiint.com","VAL "+transform(reccount("xrpt"))+" incorrect mod in outloc","xmodoutloc",,,,.t.,,,,,.t.)
endif

*

valset("check for incorrect mod in cycle")

xsqlexec("select * from modacct",,,"wh")
index on str(accountid,4)+office tag acctoff
set order to

xsqlexec("select * from cycle where 1=0","xrpt",,"wh")

useca("cycle","wh",,,"adddt>{"+dtoc(date()-1)+"} and inlist(office,'N','C')")
scan 
	=seek(str(accountid,4)+office,"modacct","acctoff")
	if cycle.mod#modacct.mod
		scatter memvar 
		insert into xrpt from memvar
		replace mod with modacct.mod in cycle
		tu("cycle")
	endif
endscan

if reccount("xrpt")>0
	select xrpt
	copy to f:\auto\xmodcycle
	email("Dyoung@fmiint.com","VAL "+transform(reccount("xrpt"))+" incorrect mod in cycle","xmodcycle",,,,.t.,,,,,.t.)
endif

*

valset("check for empty del_dates that should be there")

xsqlexec("select cast('' as char(12)) as reason, office, mod, accountid, cast('' as char(30)) as acctname, " + ;
	"ship_ref, consignee, ship_via, bol_no, wo_date, delenterdt, cast('' as date) as shipdate " + ;
	"from outship where del_date={} and wo_date>{"+dtoc(date()-17)+"} and ctnqty#0","xrpt",,"wh")

xsqlexec("select shipmentid, accountid, shipdate, mod, carrier, pickticket, office from shipment where between(shipdate,{"+dtoc(date()-4)+"},{"+dtoc(date()-1)+"})",,,"wh")
index on mod+pickticket tag modpt
set order to

select xrpt
scan 
	if seek(mod+ship_ref,"shipment","modpt")
		xok=.f.
		
		if inlist(altupsacct(shipment.accountid),&gbillabongaccts)
			xsqlexec("select accountid, shipins, ship_ref from outship where office='L' and ship_ref='"+shipment.pickticket+"'",,,"wh")
			
			if "PICKPACK"$outship.shipins and xsqlexec("select * from updtgr where ship_ref='"+Alltrim(outship.ship_ref)+"' and accountid = "+Transform(outship.accountid),"xmwtemp",,"wh")=0
			else
				xok=.t.
			endif
		else
			xok=.t.
		endif

		if xok and xsqlexec("select * from ufclose where carrier='"+shipment.carrier+"' and mod='"+shipment.mod+"' " + ;
			"and closedt>={"+dtoc(date())+"} and notfinal=0",,,"wh") # 0
			replace acctname with acctname(accountid), shipdate with shipment.shipdate, reason with "UF" in xrpt
		endif

	else
		if !empty(bol_no)
			if xsqlexec("select infoid from info where bol_no='"+bol_no+"' and message='WIP updated' and zdate>{"+dtoc(date()-90)+"}",,,"wh",,,,.t.) # 0
				replace acctname with acctname(accountid), reason with "BL" in xrpt
			endif
		endif
	endif
endscan

select xrpt
count to xcnt for !empty(reason)
if xcnt>0
	copy to f:\auto\xnodeldate for !empty(reason)
	email("Dyoung@fmiint.com","VAL "+transform(xcnt)+" empty del_dates that should be there","xnodeldate",,,,.t.,,,,,.t.)
endif

*

valset("fix bad allocqty in inven")

select whoffice
scan for !test and !novalid
	xoffice=whoffice.office
	goffice=xoffice
	xmasteroffice=whoffice.rateoffice
	gmasteroffice=xmasteroffice
	xfolder=whoffice.folder+"\whdata\"
	gmodfilter="mod='"+goffice+"'"

	useca("inven","wh",,,"mod='"+goffice+"' and updatedt>={"+dtoc(date())+"} and allocqty#0")
	useca("invenloc","wh",,,"mod='"+goffice+"'")

	xsqlexec("select * from whseloc where office='"+xmasteroffice+"'","whseloc",,"wh")
	index on whseloc tag whseloc
	set order to

	select invenid, accountid, units, style, color, id, pack, totqty, holdqty, allocqty, 0000000 as allocqtysb, ;
		availqty, updateby, updatedt, updproc from inven into cursor xrpt readwrite

	scan
		xallocqty=0

		xsqlexec("select outdet.outdetid, outdet.accountid, style, color, id, pack, " + ;
			"units, whseloc, totqty, locqty from outdet left join outloc on outdet.outdetid=outloc.outdetid " + ;
			"where outdet.mod='"+goffice+"' and outdet.accountid="+transform(xrpt.accountid)+" " + ;
			"and units="+transform(xrpt.units)+" and style='"+xrpt.style+"' " + ;
			"and color='"+xrpt.color+"' and id='"+xrpt.id+"' and pack='"+xrpt.pack+"'","xdytemp",,"wh")

		select outdetid, accountid, style, color, id, pack, units, whseloc, ;
			sum(totqty) as totqty, sum(locqty) as locqty ;
			from xdytemp group by 1,2,3,4,5,6,7,8 into cursor xoutdet

		sum totqty to xoutdetqty for isnull(whseloc)
		xallocqty=xallocqty+xoutdetqty
		
		if !xrpt.units
			xsqlexec("select * from pprep where mod='"+goffice+"' and accountid="+transform(xrpt.accountid)+" and style='"+xrpt.style+"' " + ;
				"and color='"+xrpt.color+"' and id='"+xrpt.id+"' and pack='"+xrpt.pack+"' and movedt={}","xpprep",,"wh")
			
			sum totqty to xpprepqty 
			xallocqty=xallocqty+xpprepqty

		else
			xsqlexec("select * from putback where mod='"+goffice+"' and accountid="+transform(xrpt.accountid)+" and style='"+xrpt.style+"' " + ;
				"and color='"+xrpt.color+"' and id='"+xrpt.id+"' and pack='"+xrpt.pack+"' and movedt={}","xputback",,"wh")
			
			sum unitqty to xputbackqty
			xallocqty=xallocqty+xputbackqty
		endif
		
		replace allocqtysb with xallocqty in xrpt
	endscan

	select xrpt
	count to xcnt for allocqty#allocqtysb

	if xcnt>0
		scan for allocqty#allocqtysb
			select inven
			locate for accountid=xrpt.accountid and units=xrpt.units and style=xrpt.style and color=xrpt.color and id=xrpt.id and pack=xrpt.pack
			if found()
				replace allocqty with xrpt.allocqtysb, availqty with inven.totqty-inven.allocqty-inven.holdqty in inven 
			endif
		endscan
		
		tu("inven")
		
		copy to ("f:\auto\xallocbad"+xoffice) for allocqty#allocqtysb
		email("Dyoung@fmiint.com","VAL (fixed) "+transform(xcnt)+" bad allocqty in inven for "+xoffice,"xallocbad"+xoffice,,,,.t.,,,,,.t.)
	endif
endscan

*

valset("fix mismatched inven/invenloc qty")

select whoffice 
scan for !test and !novalid
	xoffice=whoffice.office
	goffice=xoffice
	xmasteroffice=whoffice.rateoffice
	gmasteroffice=xmasteroffice
	xfolder=whoffice.folder+"\whdata\"
	gmodfilter="mod='"+goffice+"'"

	xsqlexec("select inven.invenid, inven.accountid, inven.units, inven.style, inven.color, inven.id, inven.pack, totqty, sum(locqty) as locqty " + ;
		"from inven, invenloc where inven.invenid=invenloc.invenid and inven.mod='"+goffice+"' " + ;
		"group by inven.invenid, inven.accountid, inven.units, inven.style, inven.color, inven.id, inven.pack, totqty","xtemp",,"wh")

	select * from xtemp where totqty#locqty into cursor xrpt

	select xrpt	
	count to xcnt
	if xcnt>0
		useca("inven","wh",,,"mod='"+goffice+"'")
		useca("invenloc","wh",,,"mod='"+goffice+"'")

		xsqlexec("select * from whseloc where office='"+xmasteroffice+"'","whseloc",,"wh")
		index on whseloc tag whseloc
		set order to

		select xrpt
		scan 	
			select inven
			locate for accountid=xrpt.accountid and units=xrpt.units and style=xrpt.style and color=xrpt.color and id=xrpt.id and pack=xrpt.pack
			if found()
				fixinven(.t.,,.t.)
			endif
		endscan

		copy to ("f:\auto\xmisinvqty"+xoffice)
		email("Dyoung@fmiint.com","VAL (fixed) "+transform(xcnt)+" mismatched inven/invenloc for "+xoffice,"xmisinvqty"+xoffice,,,,.t.,,,,,.t.)
	endif
endscan

*

valset("check for missing swcdet records")

select whoffice 
scan for !test and !novalid
	xoffice=whoffice.office
	goffice=xoffice
	xmasteroffice=whoffice.rateoffice
	xfolder=whoffice.folder+"\whdata\"

	xsqlexec("select wo_num, swcnum from outship where 1=0","xrpt",,"wh")
	xsqlexec("select wo_num from outwolog where mod='"+goffice+"' and staged=1 and wo_date>{"+dtoc(date()-30)+"} and updatedt>{"+dtoc(date()-1)+"}",,,"wh")
	
	scan
		xsqlexec("select wo_num, swcnum from outship where wo_num="+transform(outwolog.wo_num),,,"wh")
		select wo_num, swcnum from outship group by 1,2 into cursor xoutship
		scan
			if xsqlexec("select swcnum from swcdet where mod='"+goffice+"' and swcnum='"+swcnum+"'",,,"wh") = 0
				select xoutship
				scatter memvar
				insert into xrpt from memvar
			endif
		endscan
	endscan

	select xrpt	
	count to xcnt
	if xcnt>0
		copy to ("f:\auto\xmissswc"+xoffice)
		email("Dyoung@fmiint.com","VAL "+transform(xcnt)+" missing swcdet for "+xoffice,"xmissswc"+xoffice,,,,.t.,,,,,.t.)
	endif
endscan

*

valset("check for combined PT's listed separately in BLDET")

select whoffice
scan for !test and !novalid
	xoffice=whoffice.office
	goffice=xoffice
	xfolder=whoffice.folder+"\whdata\"

	xsqlexec("select * from bl where mod='"+goffice+"' and manual=0 and bldate>={"+dtoc(date()-30)+"}",,,"wh")
	
	xsqlexec("select * from bldet where mod='"+goffice+"' and adddt>{"+dtoc(date()-30)+"}",,,"wh")
	index on blid tag blid
	set order to
	
	select bl.blid, bol_no, ship_ref, combinedpt ;
		from bl, bldet ;
		where bl.blid=bldet.blid ;
		and !manual ;
		and bldate>date()-30 ;
		group by 1,3 having count(1)>1 ;
		into cursor xtemp

	if reccount()>0
		copy to ("f:\auto\xbldetcomb"+xoffice)
		email("Dyoung@fmiint.com","VAL "+transform(reccount())+" combined PT's listed separately in BLDET, office "+xoffice,"xbldetcomb"+xoffice,,,,.t.,,,,,.t.)
	endif
	
	use in bl
	use in bldet
endscan

*

valset("offset XRACK")

xsqlexec("select * from account where inactive=0","account",,"qq")
index on accountid tag accountid
set order to

select whoffice
scan for !test and !novalid and picknpack
	xoffice=whoffice.office
	goffice=xoffice
	xmasteroffice=whoffice.rateoffice
	gmasteroffice=xmasteroffice
	xfolder=whoffice.folder+"\whdata\"
	gmodfilter="mod='"+goffice+"'"

	useca("invenloc","wh",,,"mod='"+goffice+"'")
	
	useca("adj","wh")

	xsqlexec("select * from whseloc where office='"+xmasteroffice+"'","whseloc",,"wh")
	index on whseloc tag whseloc
	set order to
		
	select accountid, units, style, color, id, pack, whseloc, locqty, space(7) as oldwhseloc from invenloc where .f. into cursor xrack readwrite

	select * from invenloc where inlist(whseloc,"XRACK","ZRACK") and locqty<0 into cursor xtemp readwrite
	scan
		m.oldwhseloc=xtemp.whseloc
		select invenloc
		scan for invenid=xtemp.invenid and !inlist(whseloc,"XRACK","RACK","BSTAT","PSTAT","PFLOW","CYCLEU","CYCLEZ","CUT") and !hold and locqty>0 and xtemp.locqty<0
			**for Ariat only look at the slot location not decking or baker shelves - mvw 08/18/15
			if accountid=6532 and seek(invenloc.whseloc,"whseloc","whseloc") and whseloc.loctype#"SCAN PICK"
				loop
			endif

			scatter memvar
			m.locqty=min(-xtemp.locqty,invenloc.locqty)
			insert into xrack from memvar
			replace locqty with xtemp.locqty+invenloc.locqty in xtemp
		endscan
	endscan

	offsetxrack(,"VAL10")
	
	tu("adj")
	tu("invenloc")	
	indetadjupdate()

	if used("outship")
		use in outship
		use in outdet
		use in outloc
	endif
		
	if used("inwolog")
		use in inwolog
		use in indet
		use in inloc
	endif
	
	if used("adj")
		use in adj
	endif

	use in whseloc
endscan

*

valset("update Amazon delivery dates for Synclaire")

useca("outship","wh",,,"(consignee='AZ-' or consignee='AP-') and del_date={}")

select office, mod, accountid, ship_ref, del_date, consignee from outship where .f. into cursor xrpt readwrite

select outship
scan 
	if xsqlexec("select * from shipment where mod='"+outship.mod+"' and pickticket='"+outship.ship_ref+"' and sent=1",,,"wh") # 0
		select outship
		scatter memvar
		m.del_date=shipment.shipdate
		insert into xrpt from memvar
		
		replace truckloaddt with date(), del_date with date(), delenterdt with date(), delenterby with "AMAZON" in outship
	endif
endscan

tu("outship")

*

valset("check for missing delenterdt")

useca("outship","wh",,,"del_date#{} and delenterdt={}")

if reccount()>0
	replace all delenterdt with date(), delenterby with "VAL10" in outship
	tu("outship")
	
	copy to ("f:\auto\xdelenterdt")
	email("Dyoung@fmiint.com","VAL "+transform(reccount())+" missing delenterdt (fixed)","xdelenterdt",,,,.t.,,,,,.t.)
endif

*

valset("check invoice totals")

xsqlexec("select invoice.invnum, invamt, invdetamt from invoice, invdet " + ;
	"where invoice.invoiceid=invdet.invoiceid and invdt>{"+dtoc(date()-1)+"} and main=0","xdy",,"ar")

select invnum, invamt, sum(invdetamt) as invdetamt from xdy group by 1 into cursor xtemp

count for invamt#invdetamt to xcnt

if xcnt>0
	copy to f:\auto\xinvtot for invamt#invdetamt
	email("Dyoung@fmiint.com","VAL "+transform(xcnt)+" bad invoice totals","xinvtot",,,,.t.,,,,,.t.)
endif

*

valset("check for duplicate WO # for MJ")

xsqlexec("select * from inwolog where inlist(accountid,6325,6543,6303,6304,6453) and wo_date>{"+dtoc(date()-90)+"}",,,"wh")

select * from inwolog group by wo_num having count(1)>1 into cursor xrpt

if reccount()>0
	copy to ("f:\auto\xdupmj")
	email("Dyoung@fmiint.com","VAL "+transform(reccount())+" duplicate WO # for MJ","xdupmj",,,,.t.,,,,,.t.)
endif

*

valset("check for bad acctcode")

useca("upsacct","wh",,,"acctcode#bjacctcode and blujay=1")

if reccount()>0
	copy to ("f:\auto\xbadacctcode")
	email("Dyoung@fmiint.com","VAL "+transform(reccount())+" bad acctcodes","xbadacctcode",,,,.t.,,,,,.t.)
endif

*

valset("fix mismatched tote/totdet's")

useca("tote","wh",,,"adddt>{"+dtoc(date())+"}")
useca("totedet","wh",,,"adddt>{"+dtoc(date())+"}")
select tote.toteid, tote.totenum, tote.operator, totqty, totedet.totenum as totenumdet, sum(qty) as detqty, tote.adddt, tote.addby, totedet.addby as addbydet ;
	from tote, totedet where tote.toteid=totedet.toteid group by tote.toteid, totedet.totenum into cursor xtemp
	
select * from xtemp where totqty#detqty into cursor xrpt

if reccount()>0
	set step on 
	select xrpt
	scan 
		select tote
		locate for toteid=xrpt.toteid
		replace totqty with xrpt.detqty
	endscan
	
	tu("tote")

	copy to ("f:\auto\xmistote")
	email("Dyoung@fmiint.com","VAL (fixed) "+transform(reccount())+" mismatched tote/totdet's","xmistote",,,,.t.,,,,,.t.)
endif

*

valset("check for missing totenum in totedet")

select whoffice
scan for !test and !novalid
	xoffice=whoffice.office
	goffice=xoffice
	xfolder=whoffice.folder+"\whdata\"

	xsqlexec("select * from totedet where mod='"+goffice+"' and empty(totenum) and adddt>{"+dtoc(date()-7)+"}",,,"wh")
	if reccount()>0
		copy to ("f:\auto\xtotedet"+xoffice)
		email("Dyoung@fmiint.com","VAL "+transform(reccount())+" missing totenum in totedet for "+xoffice,"xtotedet"+xoffice,,,,.t.,,,,,.t.)
	endif

	use in totedet
endscan

*

valset("check for bad wavenum in outwolog")

select whoffice
scan for !test and !novalid
	xoffice=whoffice.office
	goffice=xoffice
	xmasteroffice=whoffice.rateoffice
	gmasteroffice=xmasteroffice
	
	useca("outwolog","wh",,,"mod='"+goffice+"' and wavenum#0 and wo_date>{"+dtoc(date()-30)+"}")
	
	create cursor xrpt (wo_num n(7))

	select outwolog
	scan
		if xsqlexec("select * from wavewo where wo_num="+transform(outwolog.wo_num),,,"wh") = 0
			replace wavenum with 0 in outwolog
			insert into xrpt (wo_num) value (outwolog.wo_num)
		else
			if outwolog.wavenum#wavewo.wavenum
				replace wavenum with wavewo.wavenum in outwolog
				insert into xrpt (wo_num) value (outwolog.wo_num)
			endif		
		endif
	endscan

	select xrpt		
	xcnt=reccount()
	if xcnt>0
		tu("outwolog")
		copy to ("f:\auto\xbadwavenum"+xoffice)
		email("Dyoung@fmiint.com","VAL (fixed) "+transform(xcnt)+" bad wavenum in outwolog for "+xoffice,"xbadwavenum"+xoffice,,,,.t.,,,,,.t.)
	endif
endscan

*

valset("check for duplicate upsacct")

xsqlexec("select * from upsacct",,,"wh")
select accountid, carrier, office from upsacct where accountid#9999 group by 1,2,3 having count(1)>1 into cursor xrpt

if reccount()>0
	copy to f:\auto\xdupupsacct
	email("Dyoung@fmiint.com","VAL "+transform(reccount())+" duplicate upsacct","xdupupsacct",,,,.t.,,,,,.t.)
endif

*

valset("check for incorrect PDFLOADED flags in invoice")

create cursor xpdf (invoiceid i, invdt d, wo_num n(7), invnum c(6), pdfloaded l, cfs l)

xsqlexec("select * from invoice where invdt={"+dtoc(date()-1)+"} and void=0",,,"ar")

select invoice
scan for invdt=date() and !void
	xsqlexec("select invoiceid from invpdf where invoiceid="+transform(invoice.invoiceid),"invpdf",,"invpdf")

	do case
	case invoice.pdfloaded and reccount("invpdf")=0
		m.pdfloaded=.t.
		m.invoiceid=invoice.invoiceid
		m.invdt=invoice.invdt
		m.wo_num=invoice.wo_num
		m.invnum=invoice.invnum
		m.cfs=invoice.cfs
		insert into xpdf from memvar
*			replace pdfloaded with .f. in invoice

	case !invoice.pdfloaded and reccount("invpdf")#0
		m.pdfloaded=.f.
		m.invoiceid=invoice.invoiceid
		m.invdt=invoice.invdt
		m.wo_num=invoice.wo_num
		m.invnum=invoice.invnum
		m.cfs=invoice.cfs
		insert into xpdf from memvar
*			replace pdfloaded with .t. in invoice
	endcase
endscan

if reccount("xpdf")>0
	select xpdf
	copy to f:\auto\xpdf
	email("Dyoung@fmiint.com","VAL "+transform(reccount("xpdf"))+" incorrect PDFLOADED flags in invoice","xpdf",,,,.t.,,,,,.t.)
endif

*

valset("find invenid=0 in invenloc")

select whoffice
scan for !test and !novalid
	xoffice=whoffice.office
	goffice=xoffice
	xfolder=whoffice.folder+"\whdata\"

	useca("invenloc","wh",,,"mod='"+goffice+"' and invenid=0")

	if reccount()>0
		set step on 
		copy to ("f:\auto\xinvenidzero"+xoffice)
		email("Dyoung@fmiint.com","VAL "+transform(reccount())+" invenid=0 in invenloc for "+xoffice,"xinvenidzero"+xoffice,,,,.t.,,,,,.t.)
	endif

	use in invenloc
endscan

* this caused 400,000 inven records to have allocqty of -28 somehow   dy 11/13/17

*!*	valset("fix duplicate inven records")

*!*	select whoffice
*!*	scan for !test and !novalid
*!*		xoffice=whoffice.office
*!*		goffice=xoffice
*!*		xmasteroffice=whoffice.rateoffice
*!*		gmasteroffice=xmasteroffice
*!*		xfolder=whoffice.folder+"\whdata\"
*!*		
*!*		useca("inven","wh",,,"mod='"+goffice+"'")

*!*		select * from inven group by accountid, units, style, color, id, pack having count(1)>1 into cursor xrpt
*!*			
*!*		xcnt=_tally
*!*		if xcnt>0
*!*			copy to ("f:\auto\xdupinv"+xoffice)
*!*			email("Dyoung@fmiint.com","VAL (fixed) "+transform(xcnt)+" duplicate inventory for "+xoffice,"xdupinv"+xoffice,,,,.t.,,,,,.t.)

*!*			useca("invenloc","wh",,,"mod='"+goffice+"'")

*!*			if usesqloutwo()
*!*			else
*!*				use (xfolder+"outwolog") in 0
*!*				use (xfolder+"outdet") in 0
*!*				use (xfolder+"outloc") in 0
*!*			endif

*!*			select xrpt		
*!*			scan 
*!*				delete in invenloc for accountid=xrpt.accountid and units=xrpt.units and style=xrpt.style and color=xrpt.color and id=xrpt.id and pack=xrpt.pack
*!*				select inven
*!*				locate for accountid=xrpt.accountid and units=xrpt.units and style=xrpt.style and color=xrpt.color and id=xrpt.id and pack=xrpt.pack
*!*				delete 
*!*				locate for accountid=xrpt.accountid and units=xrpt.units and style=xrpt.style and color=xrpt.color and id=xrpt.id and pack=xrpt.pack
*!*				fixinven(.t.,,.t.)
*!*			endscan

*!*			if usesqloutwo()
*!*			else
*!*				use in outwolog
*!*				use in outdet
*!*				use in outloc
*!*			endif
*!*		endif

*!*		use in inven
*!*	endscan

*

valset("fix bad availqty in inven")

select whoffice
scan for !test and !novalid
	xoffice=whoffice.office
	goffice=xoffice
	xmasteroffice=whoffice.rateoffice
	xfolder=whoffice.folder+"\whdata\"
	gmodfilter="mod='"+goffice+"'"

	useca("inven","wh",,,"mod='"+goffice+"' and availqty#totqty-holdqty-allocqty")

	if reccount()>0
		replace all availqty with totqty-holdqty-allocqty
		tu("inven")
		
		copy to ("f:\auto\xavailbad"+xoffice)
		email("Dyoung@fmiint.com","VAL (fixed) "+transform(reccount())+" bad availqty in inven for "+xoffice,"xavailbad"+xoffice,,,,.t.,,,,,.t.)
	endif

	use in inven
endscan

*

*!*	valset("find incorrect quantities in pt")

*!*	select whoffice
*!*	scan for !test and !novalid
*!*		xoffice=whoffice.office
*!*		goffice=xoffice
*!*		xfolder=whoffice.folder+"\whdata\"
*!*		m.office=xoffice

*!*		xsqlexec("select * from pt where mod='"+goffice+"'",,,"wh")
*!*		xsqlexec("select * from ptdet where mod='"+goffice+"'",,,"wh")
*!*		
*!*		select ptid, accountid, ptdate, consignee, ship_ref, qty, origqty, 0000000 as correctqty from pt where .f. into cursor xrpt readwrite
*!*		
*!*		select pt
*!*		scan for !finis and !" TOLL "$consignee
*!*			select ptdet
*!*			sum totqty to xx for ptid=pt.ptid
*!*			if pt.qty#xx
*!*				select pt
*!*				scatter memvar
*!*				m.correctqty=xx
*!*				insert into xrpt from memvar
*!*			endif
*!*		endscan					

*!*		use in pt
*!*		use in ptdet

*!*		select xrpt
*!*	*	delete for correctqty=0
*!*		count to aa
*!*		
*!*		if aa>0
*!*			select xrpt
*!*			copy to ("f:\auto\xbadptqty"+xoffice)
*!*			email("Dyoung@fmiint.com","VAL "+transform(aa)+" incorrect quantities in PT, office "+xoffice,"xbadptqty"+xoffice,,,,.t.,,,,,.t.)
*!*		endif
*!*	endscan

*

valset("fix non-cycle locations on cycle hold")

select whoffice
scan for !test and !novalid
	xoffice=whoffice.office
	goffice=xoffice
	xfolder=whoffice.folder+"\whdata\"

	useca("inven","wh",,,"mod='"+goffice+"'")
	useca("invenloc","wh",,,"mod='"+goffice+"' and whseloc#'CYCLE' and holdtype='Cycle'")
	
	select invenid, accountid, units, style, color, id, pack, whseloc, locqty, updateby, updatedt, updproc ;
		from invenloc where .f. into cursor xinvenloc readwrite

	select invenloc
	scan for whseloc#'CYCLE' and holdtype='Cycle'
		scatter memvar
		insert into xinvenloc from memvar
		
		replace holdtype with "", hold with .f. in invenloc
		
		replace holdqty with inven.holdqty-invenloc.locqty, availqty with inven.totqty-inven.allocqty-inven.holdqty in inven for invenid=invenloc.invenid
	endscan

	select xinvenloc
	count to xcnt

	if xcnt>0
		tu("inven")
		tu("invenloc")

		copy to ("f:\auto\xcycleholdqty"+xoffice)
		email("Dyoung@fmiint.com","VAL (fixed) "+transform(xcnt)+" bad cycle hold quantities for "+xoffice,"xcycleholdqty"+xoffice,,,,.t.,,,,,.t.)
	endif

	use in inven
	use in invenloc
endscan

*

valset("fix incorrect removeqty in SQL adj")

select whoffice
scan for !test and !novalid 
	xoffice=whoffice.office
 	goffice=xoffice
	xfolder=whoffice.folder+"\whdata\"

	useca("adj","wh",,,"mod='"+goffice+"' and removeqty>totqty and totqty>0")

	if reccount()>0
		copy to ("f:\auto\xadjremovesql"+xoffice)
*		email("Dyoung@fmiint.com","VAL (fixed) "+transform(reccount())+" incorrect removeqty in SQL adj, office "+xoffice,"xadjremovesql"+xoffice,,,,.t.,,,,,.t.)

		replace all removeqty with totqty, remainqty with 0
		tu()
	endif

	use in adj
endscan

*

valset("check for duplicate dash in wo pudlocs")

xsqlexec("select * from pudlocs",,,"wo")

select accountid, dash, ttod(adddt) as adddt from pudlocs group by 1,2 having count(1)>1 into cursor xrpt

select xrpt
count to aa
if aa>0
	select xrpt
	copy to ("f:\auto\xpudlocdupdash")
	email("Dyoung@fmiint.com","VAL "+transform(aa)+" duplicate dash in pudlocs","xpudlocdupdash",,,,.t.,,,,,.t.)
endif

*

valset("Update GOH field in PT")

select whoffice
scan for !test and !novalid and picknpack
	xoffice=whoffice.office
	goffice=xoffice
	xfolder=whoffice.folder+"\whdata\"

	useca("pt","wh",,,"m-"+goffice)
	useca("ptdet","wh",,,"m-"+goffice)
	
	select * from pt where .f. into cursor xrpt readwrite
	
	select pt
	scan 
		select ptdet
		locate for ptid=pt.ptid and units
		xgoh=found()
		
		if pt.goh#xgoh
			replace goh with xgoh in pt
			select pt
			scatter memvar
			insert into xrpt from memvar
		endif
	endscan

	select xrpt
	count to xcnt
	if xcnt>0
		tu("pt")
		copy to ("f:\auto\xptgoh"+xoffice) 
*		email("Dyoung@fmiint.com","VAL (fixed) "+transform(xcnt)+" updated GOHs in PT, office "+xoffice,"xptgoh"+xoffice,,,,.t.,,,,,.t.)
	endif

	use in pt
	use in ptdet
endscan

*

*

valset("Send Mike Drew an email for drivers in one place > 2 hours")

use f:\wo\wodata\daily in 0

for i=1 to 2
	if i=1
		xoffice="N"
		xemail="mike.drew@tollgroup.com; don.kistner@tollgroup.com; thomas.keaveney@tollgroup.com; joe.damato@tollgroup.com"
	else
		xoffice="C"
		xemail="cesar.nevares@tollgroup.com; ruben.madrigal-delarosa@tollgroup.com; fernando.villanueva@tollgroup.com; jorge.ramirez@tollgroup.com; alvaro.sanchez@tollgroup.com; johanna.sanchez@tollgroup.com"
	endif
	
	create cursor xrpt (driver c(30), truck c(8), placename c(30), minutes n(3), ztime c(15))

	select dis_driver, dis_truck from daily where !empty(dis_driver) and office=xoffice and empty(time_in) and dvrcompany="T" group by 1 into cursor xdaily

	scan 
		xsqlexec("select * from pos where truck_num='"+xdaily.dis_truck+"' and zdate={"+dtoc(date())+"}","xtemp",,"fx")
		select posid, ctot(dtoc(zdate)+" "+chr2time(ztime)) as zdatetime, placename ;
			from xtemp order by 2 descending into cursor xtemp

		if reccount()=0 or empty(placename) 
			loop
		endif

		xstartdatetime={}
		
		xplacename=placename
		scan 
			if placename#xplacename
				skip -1
				xstartdatetime=zdatetime
				exit
			endif
		endscan

		if !empty(xstartdatetime)
			if datetime()-xstartdatetime>7200
				m.driver=xdaily.dis_driver
				m.truck=xdaily.dis_truck
				m.placename=xplacename
				m.minutes=round((datetime()-xstartdatetime)/60,0)
				xhours=int(m.minutes/60)
				xminutes=(m.minutes-xhours*60)%60
				if xminutes=0
					m.ztime=transform(xhours)+" hrs"
				else
					m.ztime=transform(xhours)+" hrs "+transform(xminutes)+" min"+iif(xminutes=1,"","s")
				endif
				insert into xrpt from memvar		
			endif
		endif
	endscan

	xbody=""

	if reccount("xrpt")>0
		select xrpt
		index on minutes tag minutes descending
		scan 
			xbody=xbody+driver+truck+placename+ztime+crlf
		endscan

		emailx(xemail,,"Drivers in one place for over two hours", xbody)
	endif
next

* removed dy 10/29/17 was getting a duplicate key in invenloc

*!*	valset("fix negative availqty for Lifefactory")

*!*	xmasteroffice="N"
*!*	gmasteroffice="N"
*!*	xoffice="I"
*!*	goffice="I"

*!*	useca("inven","wh",,,"mod='I' and availqty<0")
*!*	useca("invenloc","wh",,,"mod='I'")

*!*	if usesqloutwo()
*!*	else
*!*		use f:\whi\whdata\outwolog in 0
*!*		use f:\whi\whdata\outdet in 0
*!*		use f:\whi\whdata\outloc in 0
*!*	endif

*!*	select inven
*!*	scan for availqty<0
*!*		fixinven(.t.,,.t.)
*!*	endscan

*

valset("check for bad records in invenloc")

select whoffice
scan for !test and !novalid
	xoffice=whoffice.office
	goffice=xoffice
	xmasteroffice=whoffice.rateoffice
	xfolder=whoffice.folder+"\whdata\"
	xpnpaccount=pnpacct(xoffice)
	
	if goffice="J" and date()<{3/6/17}
		loop
	endif

	xsqlexec("select * from whseloc where office='"+xmasteroffice+"'","whseloc",,"wh")
	index on whseloc tag whseloc
	set order to

	useca("invenloc","wh",,,"mod='"+goffice+"'")

	select space(20) as problem, * from invenloc where .f. into cursor xrpt readwrite
	
	select invenloc
	scan for locqty<0 and !inlist(whseloc,"CYCLE","XRACK","INHLD")
		scatter memvar
		m.problem="negative locqty"
		insert into xrpt from memvar
	endscan
	
	select invenloc
	scan for !&xpnpaccount and whseloc="RACK"
		scatter memvar
		m.problem="rack for non-pnp acct"
		insert into xrpt from memvar
	endscan
	
	select xrpt	
	if xoffice="K" 
		delete for accountid=0
	endif
	delete for accountid=6453 	&& per Todd
	
*	delete for style='MI-VRH01-101W       '	&& dy 10/6/16

	count to aa
	if aa>0
		copy to ("f:\auto\xlocqty"+xoffice)
		
		if inlist(xoffice,"J","L")
			emailx("todd.margolin@tollgroup.com",,transform(aa)+" bad records in INVENLOC, office "+xoffice,"f:\auto\xlocqty"+xoffice)
		else
			email("Dyoung@fmiint.com","VAL "+transform(aa)+" bad records in INVENLOC, office "+xoffice,"xlocqty"+xoffice,,,,.t.,,,,,.t.)
		endif
	endif
	
	use in invenloc
	use in whseloc
endscan

*

*!*	valset("check for track & trace not running")

*!*	if substr(ttoc(datetime()),12)<"17" and between(dow(date()),2,6)
*!*		use f:\wo\wodata\tt in 0
*!*		select tt
*!*		go bottom
*!*		if datetime()-timestampvalue>5200
*!*			email("Dyoung@fmiint.com; mwinter@fmiint.com","Track & Trace is not running",,,,,.t.,,,,,.t.)
*!*		endif
*!*	endif

*

valset("check for missing package records")

select whoffice
scan for !test and !novalid
	xoffice=whoffice.office
	goffice=xoffice
	xmasteroffice=whoffice.rateoffice
	xfolder=whoffice.folder+"\whdata\"
	xpnpaccount=pnpacct(xoffice)

	xsqlexec("select * from shipment where mod='"+goffice+"' and shipdate>={"+dtoc(date())+"}",,,"wh")
	calculate min(shipmentid), max(shipmentid) to xmin, xmax
	xsqlexec("select * from package where mod='"+goffice+"' and between(shipmentid,"+transform(xmin)+","+transform(xmax)+")",,,"wh")
	index on shipmentid tag shipmentid
	
	select * from shipment where .f. into cursor xrpt readwrite

	select shipment
	scan for shipdate>=date()
		if !seek(shipmentid,"package","shipmentid")
			scatter memvar
			insert into xrpt from memvar
		endif
	endscan

	select xrpt
	if reccount()>0
		copy to ("f:\auto\xmissingpackage"+xoffice)
		email("Dyoung@fmiint.com","VAL "+transform(reccount())+" missing package records, office "+xoffice,"xmissingpackage"+xoffice,,,,.t.,,,,,.t.)
	endif

	use in shipment
	use in package
endscan

*

valset("check for status problems in inrcv")

useca("inrcv","wh",,,"status#'CONFIRMED'")
useca("rcv","wh",.t.)

use f:\wo\wodata\wolog in 0

create cursor xrpt (problem c(20), mod c(1), inrcvid i, inwonum n(7), ltwonum n(7), status c(15))

select inrcv
scan for accountid#6493
	if !empty(inrcv.inwonum)
		goffice=inrcv.mod
		
		xsqlexec("select * from inwolog where wo_num="+transform(inrcv.inwonum),,,"wh")
		if !emptynul(inwolog.confirmdt)
			insert into xrpt (problem, mod, inrcvid, inwonum, ltwonum, status) values ("confirmed", inrcv.mod, inrcv.inrcvid, inrcv.inwonum, inrcv.ltwonum, inrcv.status)
			replace status with "CONFIRMED" in inrcv
			use in inwolog
			loop
		endif
		use in inwolog
						
		select rcv
		locate for wo_num=inrcv.inwonum and rcvqty-awayqty=0 and rcvqty#0
		if found()
			if inrcv.status#"PUTAWAY DONE"
				insert into xrpt (problem, mod, inrcvid, inwonum, ltwonum, status) values ("putaway done", inrcv.mod, inrcv.inrcvid, inrcv.inwonum, inrcv.ltwonum, inrcv.status)
				replace status with "PUTAWAY DONE" in inrcv
			endif
			loop
		endif

		locate for wo_num=inrcv.inwonum
		if found()
			if inrcv.status#"ON RECEIVING"
				insert into xrpt (problem, mod, inrcvid, inwonum, ltwonum, status) values ("putaway start", inrcv.mod, inrcv.inrcvid, inrcv.inwonum, inrcv.ltwonum, inrcv.status)
				replace status with "ON RECEIVING" in inrcv
			endif
			loop
		endif
	else
*		if inrcv.status="PUTAWAY"	dy 2/3/17
*			insert into xrpt (problem, mod, inrcvid, inwonum, ltwonum, status) values ("putaway", inrcv.mod, inrcv.inrcvid, inrcv.inwonum, inrcv.ltwonum, inrcv.status)
*			replace status with "CONFIRM" in inrcv
*		endif
*		loop
	endif
	
	if inrcv.status#"IN YARD" and inrcv.accountid=6493
		loop
	endif
	
	if inrcv.status="IN YARD"	&& dy 9/12/16
		loop
	endif

	if !empty(inrcv.ltwonum)
		select wolog
		locate for wo_num=inrcv.ltwonum and (!emptynul(pickedup) or inlist(wostatus,"DONE","WAREHOUSE LOAD","ON HOLD IN YARD","EMPTY IN YARD","DROPPED CONTAINER","DELIVER","RETURN","DONE"))
		if found()
			if inrcv.status#"IN YARD" and inrcv.accountid#6493
				insert into xrpt (problem, mod, inrcvid, inwonum, ltwonum, status) values ("in yard", inrcv.mod, inrcv.inrcvid, inrcv.inwonum, inrcv.ltwonum, inrcv.status)
				replace status with "IN YARD" in inrcv
			endif
			loop
		endif

		select wolog
		locate for wo_num=inrcv.ltwonum and wostatus="PICKUP IN PROGRESS"
		if found() 
			if inrcv.status#"DISPATCHED" and wo_num#1753473
				insert into xrpt (problem, mod, inrcvid, inwonum, ltwonum, status) values ("available", inrcv.mod, inrcv.inrcvid, inrcv.inwonum, inrcv.ltwonum, inrcv.status)
				replace status with "DISPATCHED" in inrcv
			endif
			loop
		endif

		select wolog
		locate for wo_num=inrcv.ltwonum and wostatus="AVAILABLE"
		if found() 
			if inrcv.status#"AVAILABLE"
				insert into xrpt (problem, mod, inrcvid, inwonum, ltwonum, status) values ("available", inrcv.mod, inrcv.inrcvid, inrcv.inwonum, inrcv.ltwonum, inrcv.status)
				replace status with "AVAILABLE" in inrcv
			endif
			loop
		endif

		if !(inrcv.accountid=5687 and inrcv.mod="I")
			select wolog
			locate for wo_num=inrcv.ltwonum and wostatus="NOT AVAILABLE"
			if found() 
				if inrcv.status#"NOT AVAILABLE"
					insert into xrpt (problem, mod, inrcvid, inwonum, ltwonum, status) values ("not available", inrcv.mod, inrcv.inrcvid, inrcv.inwonum, inrcv.ltwonum, inrcv.status)
					replace status with "NOT AVAILABLE" in inrcv
				endif
				loop
			endif
		endif
	endif
	
	if inrcv.status#"NOT AVAILABLE" and inrcv.updproc#"INRCV" and !inlist(inrcv.addproc,"INRCV","INWO") and inrcv.mod#"M" and !(inrcv.accountid=5687 and inrcv.mod="I")
*		insert into xrpt (problem, mod, inrcvid, inwonum, ltwonum, status) values ("???", inrcv.mod, inrcv.inrcvid, inrcv.inwonum, inrcv.ltwonum, inrcv.status)
	endif
endscan

select xrpt
if reccount()>0
	tu("inrcv")
	locate for problem="???"
	if !found()
		xfixed="(fixed) "
	else
		xfixed=""
	endif
	copy to ("f:\auto\xinrcvstatus")
	email("Dyoung@fmiint.com","VAL "+xfixed+transform(reccount())+" status problems in inrcv","xinrcvstatus",,,,.t.,,,,,.t.)
endif

*

valset("check for problems in inrcv")

release guserid

use f:\wo\wodata\wolog in 0
use f:\wo\wodata\daily in 0
use f:\wo\wodata\tdaily in 0

xsqlexec("select * from account where inactive=0","account",,"qq")
index on accountid tag accountid
index on acctname tag acctname
set order to

select whoffice
scan for !test and !novalid
	xoffice=whoffice.office
	goffice=xoffice
	xfolder=whoffice.folder+"\whdata\"
		
	useca("inrcv","wh",,,"mod='"+xoffice+"' and status#'CONFIRMED' and ltwonum#1679246 and inrcvid#1002979 and addfrom#'I1'")
	
	select space(50) as problem, .f. as fixed, * from inrcv where .f. into cursor xrpt readwrite

	select inrcv
	scan 
		if empty(dychar)
			replace dychar with "5" 
		endif
	
		if inlist(xoffice,"7","L") and moretaccount("accountid") and !empty(ltwonum) and inrcvid#7751
			=seek(ltwonum,"wolog","wo_num")
			if ((xoffice="7" and wolog.delloc#"SAN PEDRO") or (xoffice="L" and wolog.delloc#"MIRA LOMA")) and !inrcv.flag
				select inrcv
				scatter memvar
				m.problem="Moret WO in incorrect office"
				m.fixed=.t.
				insert into xrpt from memvar
				delete in inrcv
			endif
		endif
			
*		if inwonum#0
*			select indet
*			locate for wo_num=inrcv.inwonum
*			if found()
*				select inrcv
*				scatter memvar
*				m.problem="confirmed inbound WO showing unconfirmed"
*				m.fixed=.t.
*				insert into xrpt from memvar
*				replace status with "CONFIRMED", confirmdt with indet.date_rcvd in inrcv
*			endif
*		endif
		
		select inrcv		
		if empty(ltwonum)
			select wolog
			locate for accountid=inrcv.accountid and container=left(inrcv.container,11) and wo_date>date()-30
			if found()
				select inrcv
				scatter memvar
				m.problem="Trucking WO # missing"
				m.fixed=.t.
				insert into xrpt from memvar
				replace ltwonum with wolog.wo_num in inrcv
			endif
			select inrcv
		endif

		select inrcv
		if inrcv.inwonum=0 and !empty(inrcv.container) and inrcv.ltwonum#0
			if xsqlexec("select * from inwolog where container='"+left(inrcv.container,11)+"' and accountid="+transform(inrcv.accountid)+" and wo_date>{"+dtoc(date()-90)+"}",,,"wh") # 0
				select inrcv
				scatter memvar
				m.problem="Inbound WO # missing (container)"
				m.fixed=.t.
				insert into xrpt from memvar
				replace inwonum with inwolog.wo_num in inrcv
			endif
		endif
				
		select inrcv
		if !inlist(accountid,5442,6235,1,62,4484,6151,6565,1747)
			m.problem=""

			do case
			case accountid=0
				m.problem="accountid=0"
			case !seek(accountid,"account","accountid")
				m.problem="Account not found"
			otherwise
				do case
				case xoffice="I" and account.njmod#"I"
					m.problem="Account is for a different office"
				case xoffice="J" and account.njmod#"J"
					m.problem="Account is for a different office"
				case xoffice="1" and account.building#"4"
					m.problem="Account is for a different office"
				case xoffice="2" and !inlist(account.building,"7","8")
					m.problem="Account is for a different office"
				case xoffice="5" and account.building#"6"
					m.problem="Account is for a different office"
				case xoffice="6" and account.building#"5"
					m.problem="Account is for a different office"
				case xoffice="7" and account.building#"0"
					m.problem="Account is for a different office"
				case xoffice="8" and account.building#"1"
					m.problem="Account is for a different office"
				endcase
			endcase
			
			if inlist(inrcv.accountid,0,6705)
				select inrcv
				scatter memvar
				m.fixed=.t.
				insert into xrpt from memvar
				delete in inrcv
			else
				if !empty(m.problem)
					select inrcv
					scatter memvar
					m.fixed=.f.
					insert into xrpt from memvar
				endif
			endif
		endif

		select inrcv		
		if status="NOT AVAILABLE" and !empty(ltwonum) and seek(ltwonum,"wolog","wo_num") and !emptynul(wolog.availdt)
			scatter memvar
			m.problem="Not available in inrcv but available in wolog"
			m.fixed=.t.
			insert into xrpt from memvar
			replace status with "AVAILABLE" in inrcv
		endif
		
		select inrcv
		if status="DISPATCHED" and !emptynul(pickedup)
			scatter memvar
			m.problem="Missing In Yard status"
			m.fixed=.t.
			insert into xrpt from memvar
			replace status with "IN YARD" in inrcv
		endif

*!*			select inrcv
*!*			if !empty(inwonum) and !empty(container) and !isdigit(container) and len(trim(container))=11 and !inlist(container,"HANG","LTL","AIR","SEA","CONTAINER","HAWB","SHAI607144B","TEMU7641383   ","GAL0","SP#") and !inlist(inwonum,3201073,3201069,5317622,5333636,7527884,7726709,7726174)
*!*				if xsqlexec("select * from inwolog where wo_num="+transform(inrcv.inwonum),,,"wh") # 0
*!*					if inwolog.container#left(inrcv.container,11)
*!*						select inrcv
*!*						scatter memvar
*!*						m.fixed=.f.
*!*						m.problem="Incorrect container"
*!*						insert into xrpt from memvar
*!*					endif
*!*				endif
*!*			endif
		
		select inrcv
		if !empty(ltwonum) and empty(container) and wotype#"D" and !inlist(ltwonum,1762120,1762322,1793354)
			if seek(ltwonum,"wolog","wo_num")
				do case
				case !empty(wolog.container)
					m.problem="Missing container"
				case !empty(wolog.awb)
					m.problem="Missing AWB"
				otherwise
					m.problem="No container or AWB in wolog"
				endcase
				
				select inrcv
				scatter memvar
				m.fixed=.f.
				insert into xrpt from memvar
			endif
		endif
		
		select inrcv
		if emptynul(pickedup) and !empty(ltwonum)
			if seek(ltwonum,"wolog","wo_num") and !emptynul(wolog.pickedup)
				scatter memvar
				m.problem="Missing pickedup date"
				m.fixed=.t.
				insert into xrpt from memvar
				replace pickedup with wolog.pickedup, status with "IN YARD" in inrcv
			endif
		endif
		
		select inrcv
		if emptynul(dydt) and !empty(ltwonum)
			if seek(ltwonum,"wolog","wo_num") and !emptynul(wolog.lastfreedt)
				scatter memvar
				m.problem="Missing last free date"
				m.fixed=.t.
				insert into xrpt from memvar
				replace dydt with wolog.lastfreedt in inrcv
			endif
		endif
		
		select inrcv
		if inlist(status,"AVAILABLE","NOT AVAILABLE") and !inlist(ltwonum,0,  1753473)
			if (seek(ltwonum,"daily","wo_num") and emptynul(daily.datein)) or (seek(ltwonum,"tdaily","wo_num") and !"ABORT"$tdaily.choice)
				scatter memvar
				m.problem="Not showing as dispatched"
				m.fixed=.t.
				insert into xrpt from memvar
				replace status with "DISPATCHED" in inrcv
			endif
		endif
	endscan

	select * from inrcv ;
		where !empty(container) and len(trim(container))>=10 and !inlist(accountid,5687,5154) and newdt>date()-45 and container#"TCKU9006757" and !inlist(ltwonum,1793354,1789542) ;
		and (inwonum#0 or ltwonum#0) and addfrom#"I1" ;
		group by inwonum, ltwonum, accountid, container having count(1)>1 into cursor xdy readwrite

	scan 
		scatter memvar
		m.problem="Duplicate account/containers"
		m.fixed=.f.
		insert into xrpt from memvar
	endscan

	select * from inrcv where inwonum#0 and newdt>date()-45 and addfrom#"I1" ;
		group by inwonum, ltwonum having count(1)>1 into cursor xdy readwrite

	scan for !inlist(inwonum,5249482,7701249,6225156,7529731)
		scatter memvar
		m.problem="Duplicate inbound WOs"
		m.fixed=.f.
		insert into xrpt from memvar
	endscan

	select xrpt
	if reccount()>0
		tu("inrcv")
		locate for !fixed
		if !found()
			xfixed="(fixed) "
		else
			xfixed=""
		endif
		copy to ("f:\auto\xinrcv"+xoffice)
		email("Dyoung@fmiint.com","VAL "+xfixed+transform(reccount())+" problems in inrcv, office "+xoffice,"xinrcv"+xoffice,,,,.t.,,,,,.t.)
	endif

	use in inrcv
	if used("inwolog")
		use in inwolog
	endif
endscan

* removed dy 4/4/17

*!*	valset("check for PIPs that show as available")

*!*	use f:\wo\wodata\wolog in 0
*!*	use f:\wo\wodata\daily in 0

*!*	select * from wolog where .f. into cursor xrpt readwrite

*!*	select wolog
*!*	scan for wostatus="AVAILABLE"
*!*		select daily
*!*		locate for wo_num=wolog.wo_num and inlist(action,"PU","LD") and emptynul(datein)
*!*		if found()
*!*			select wolog
*!*			scatter memvar
*!*			insert into xrpt from memvar
*!*		endif
*!*	endscan

*!*	select xrpt
*!*	count to aa
*!*	if aa>0
*!*		copy to f:\auto\xavailpip
*!*		email("Dyoung@fmiint.com","VAL "+transform(aa)+" PIPs that show as available","xavailpip",,,,.t.,,,,,.t.)
*!*	endif

*

valset("check for missing puloc in drop")

use f:\wo\wodata\drop
use f:\wo\wodata\wolog in 0

select * from drop where empty(puloc) and qty_type#"TRAILE" into cursor xrpt

count to xcnt
if xcnt>0
	scan 
		if seek(wo_num,"wolog","wo_num")
			=seek(dropid,"drop","dropid")
			replace puloc with wolog.puloc in drop
		endif
	endscan

	copy to ("f:\auto\xmispuloc")
	email("Dyoung@fmiint.com","VAL "+transform(xcnt)+" missing puloc in drop","xmispuloc",,,,.t.,,,,,.t.)
endif

*

valset("check for blank delivered date in wolog")

use f:\wo\wodata\wolog in 0
use f:\wo\wodata\tdaily in 0

select * from wolog where .f. into cursor xrpt readwrite

select wolog
scan for wo_date>date()-60 and emptynul(delivered)
	select tdaily
	locate for wo_num=wolog.wo_num and "DELIVERED"$choice and !"PARTIAL"$choice
	if found()
		select wolog
		scatter memvar
		insert into xrpt from memvar
		replace delivered with tdaily.dispatched in wolog
	endif
endscan

if reccount("xrpt")>0
	select xrpt
	copy to f:\auto\xwologdel
	email("Dyoung@fmiint.com","VAL (fixed) "+transform(reccount("xrpt"))+" blank delivered date in wolog","xwologdel",,,,.t.,,,,,.t.)
endif

*

valset("fix incorrect totqty in inven")

select whoffice
scan for !test and !novalid and office="L"
	xoffice=whoffice.office
	goffice=xoffice
	gmasteroffice=whoffice.rateoffice
	xmasteroffice=whoffice.rateoffice
	xfolder=whoffice.folder+"\whdata\"

	useca("inven","wh",,,"mod='"+goffice+"'")
	useca("invenloc","wh",,,"mod='"+goffice+"'")

	select inven.invenid, inven.accountid, inven.units, inven.style, inven.color, inven.id, inven.pack, totqty, sum(locqty) as locqty ;
		from inven left outer join invenloc on inven.invenid=invenloc.invenid group by 1 into cursor xrpt readwrite
		
	delete for totqty=locqty
	delete for totqty=0 and isnull(locqty)

	count to xcnt		

	if xcnt>0
		xsqlexec("select * from whseloc where office='"+xmasteroffice+"'","whseloc",,"wh")
		index on whseloc tag whseloc
		set order to

		select xrpt
		scan 	
			select inven
			locate for accountid=xrpt.accountid and units=xrpt.units and style=xrpt.style and color=xrpt.color and id=xrpt.id and pack=xrpt.pack
			if found()
				fixinven(.t.,,.t.)
			endif
		endscan
		
		select xrpt
		copy to ("f:\auto\xinvenqty"+xoffice)
		email("Dyoung@fmiint.com","VAL (fixed) "+transform(xcnt)+" incorrect totqty in inven for "+xoffice,"xinvenqty"+xoffice,,,,.t.,,,,,.t.)
	endif

	if used("indet")
		use in indet
		use in inloc
	endif
	use in inven
	use in invenloc
	if used("adj")
		use in adj
	endif
	
	if used("outwolog")
		use in outwolog
		use in outdet
		use in outloc
	endif
	
	if used("whseloc")
		use in whseloc
	endif
endscan

*

valset("find extra hidden units in indet for Louisville")

gmasteroffice="K"
xmasteroffice="K"
xoffice="K"
goffice="K"

xsqlexec("select * from indet where mod='K' and date_rcvd>={"+dtoc(date()-1)+"} and !inlist(color,'ASST','9900') and remainqty#0",,,"wh")
index on indetid tag indetid
set order to

select indetid, 0 as num, wo_num, po, {} wo_date, style, color, pack, totqty from indet where .f. into cursor xrpt readwrite

select indet
scan for !units
	xindetid=indetid
	xwonum=wo_num
	xpo=po
	
	count to aa for wo_num=xwonum and po=xpo and units
	if aa>1
		locate for wo_num=xwonum and po=xpo and !units
		skip
		insert into xrpt (indetid, num, wo_num, po, wo_date, style, color, pack, totqty) values (indet.indetid, aa, indet.wo_num, indet.po, indet.date_rcvd, indet.style, indet.color, indet.pack, indet.totqty)
	endif
	=seek(xindetid,"indet","indetid")
endscan

select xrpt
if reccount()>0
	copy to ("f:\auto\xhiddenindet")
	email("Dyoung@fmiint.com","VAL "+transform(reccount())+" extra hidden units in indet for Louisville  SQL VERSION","xhiddenindet",,,,.t.,,,,,.t.)
endif

*

valset("check for unpulled & delivered WO's")

select whoffice
scan for !test and !novalid
	xoffice=whoffice.office
	goffice=xoffice
	xfolder=whoffice.folder+"\whdata\"

	xsqlexec("select outwolog.wo_num, outwolog.wo_date, outwolog.acctname, outship.ship_ref " + ;
		"from outwolog, outship where outwolog.outwologid=outship.outwologid and outwolog.pulled=0 " + ;
		"and outwolog.mod='"+goffice+"' and del_date#{} and outwolog.wo_date>{"+dtoc(date()-28)+"}","xdytemp",,"wh")

	select wo_num, wo_date, acctname, ship_ref from xdytemp group by 1 into cursor xtemp
	
	if reccount()>0
		copy to ("f:\auto\xunpull"+xoffice)
		email("Dyoung@fmiint.com","VAL "+transform(reccount())+" unpulled & delivered WO's, office "+xoffice,"xunpull"+xoffice,,,,.t.,,,,,.t.)
	endif
	
	if used("outwolog")
		use in outwolog
	endif
	if used("outship")
		use in outship
	endif
endscan

*

valset("check for unpulled outbound wo's with outloc records")

select whoffice
scan for !test and !novalid
	xoffice=whoffice.office
	goffice=xoffice
	xfolder=whoffice.folder+"\whdata\"

	xsqlexec("select * from outwolog where mod='"+goffice+"' and pulled=0 and wo_date>{"+dtoc(date()-7)+"}",,,"wh")
	
	create cursor xunpulloutloc (office c(1), wo_num n(7))

	select outwolog
	scan for !pulled and wo_date>date()-7
		if xsqlexec("select * from outloc where outwologid="+transform(outwolog.outwologid),,,"wh") # 0
			insert into xunpulloutloc (office, wo_num) values (xoffice, outwolog.wo_num)
		endif
	endscan

	select xunpulloutloc
	if reccount("xunpulloutloc")>0
		copy to ("f:\auto\xunpulloutloc"+xoffice)
		email("Dyoung@fmiint.com","VAL "+transform(reccount("xunpulloutloc"))+" unpulled wo's with outloc's, office "+xoffice,"xunpulloutloc"+xoffice,,,,.t.,,,,,.t.)
	endif

	use in outwolog
	if used("outloc")
		use in outloc
	endif
endscan

*

use f:\auto\val10
go bottom
replace zendtime with datetime()
replace all seconds with zendtime-zstarttime
use 

schedupdate()

_screen.Caption=gscreencaption
on error
