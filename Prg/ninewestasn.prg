Public xfile,asn_origin,TranslateOption,lcScreenCaption 

runack("NINEWESTASN")

*set default to "f:\ninewest856\asnin\"
lcPath        = "f:\ninewest856\asnin\"
lcArchivePath = "f:\ninewest856\asnarchive\"

Set Safety Off
Set Exclusive Off

delimiter ="*"
TranslateOption = "NONE"
asn_origin = "LOGNET"

lcScreenCaption = "Ninewest ASN Uploader Process... Processing File: "
_screen.Caption = lcScreenCaption

lnNum = ADIR(TARRAY,lcPath+"nwd*.856")
if lnNum =0
  Wait window at 10,10 "No Ninewest ASN's to import................." timeout 2
  return
endif
lnNum2 = asort(tarray)

DO m:\dev\prg\createx856

set step on 


if lnNum >= 1
for thisfile = 1  to lnNum
  xfile = upper(lcpath+tarray[thisfile,1])
  archivefile = upper(lcArchivePath+tarray[thisfile,1])
  wait window "Importing file: "+xfile nowait
  
  if file(xfile)
    if at("NWD",upper(xfile)) > 0 && only import the files like "NWD.... .856" 
       _screen.Caption = lcScreenCaption+xfile
       DO loadedifile WITH xfile,delimiter,TranslateOption,"JONES856"
       Do ImportNinewestASN
    endif  
    copy file &xfile to &archivefile
    if file(archivefile)
      delete file &xfile
    endif
  endif  
next thisfile
else
  wait window at 10,10  "          No 856's to Import          " nowait
endif


***************************************************************************************************************
Procedure ImportNinewestASN

Set CENTURY ON
Set DATE TO YMD

select x856
goto top
store 0 to lnTotalAdded
lnCurrentTripNumber =0
lcHLLevel = ""

if !used("csize")
  use f:\jones856\csize in 0 alias csize
endif  

if !used("NWDetail")
  use f:\ninewest856\nwestdetail in 0 alias nwdetail
endif  

if !used("nwpl")
  use f:\ninewest856\nwpl in 0 alias nwpl
endif  

if !used("nwshipments")
  use f:\ninewest856\shipments in 0 alias nwshipments
endif  

if !used("newssco")
  use f:\ninewest856\data\newssco in 0 alias newssco
endif  

if !used("NWdellocs")
  use f:\NINEWEST856\dellocs in 0 alias NWDellocs
endif


POs_added = 0
ShipmentNumber = 1
ThisShipment = 0
origawb = ""
lnTotalKiolgrams = 0.0
lcDiv = ""
lcSeason = ""
lcSize = ""
lcCtrprfx = ""
lcContainer = ""
lcVessel = ""
lcSSCO = ""
lcFwdr = ""
lcPtOrigin = ""
lcOrigin = ""
lcDest = ""
lcItemID = ""
lcCtnRange = ""
lcCurrentmasterBOL = ""
lcSealNum = ""
lcCurrentShipID = ""
lcCurrentVoyagenum = ""
lcCurrentModiAWB = ""
         
select nwdetail
set order to filename
seek alltrim(upper(xfile))

**added to delete on reimport of file if not already loaded into the fmi system - mvw 03/07/11
delete while filename=alltrim(upper(xfile)) for empty(wo_num)

select x856
set filter to 
goto top

do while !eof("x856")
 wait window "At the outer loop " nowait

  if trim(x856.segment) = "BSN" 
    lcCurrentShipID = alltrim(x856.f2)
    ll856Update = .F.
    select nwshipments
    append blank
    replace nwshipments.shipmentid with lcCurrentShipID
    replace nwshipments.bsn02      with alltrim(x856.f1)
    replace nwshipments.dateloaded with datetime()
    replace nwshipments.filename   with upper(xfile)
    select x856    
  endif

  if trim(x856.segment) = "BSN" and alltrim(x856.f1) = "05"
    ll856Update = .T.
    lcShipID = padr(alltrim(x856.f2),len(NWDetail.shipid)," ")

    select NWDetail
    set order to shipid
    seek lcShipID
    if found()
      select NWDetail
      delete while NWDetail.shipid = lcShipID
    endif
    select NWPL
    set order to shipid
    seek lcShipID
    if found()
      select NWPL
      delete while NWPL.shipid = lcShipID
    endif
  endif

  select x856
  if !eof()
    skip 1 in x856
  endif

  if at("HL",x856.segment)>0
    lcHLLevel = alltrim(x856.f3) 
  endif
  
  if lcHLLevel = "S"
    select NWDetail
    append blank
    replace NWDetail.filename   with upper(xfile)
    replace NWDetail.dateloaded with datetime()
    replace NWDetail.loose      with .F.
    replace NWDetail.acct_name  with "NINE WEST DISTRIBUTION"
    replace NWDetail.acct_num   with 1747
    replace NWDetail.shipid     with lcCurrentShipID
    if ll856Update = .T.
      replace NWDetail.updated with DateTime()
    else
      
    endif  
    ShipmentNumber = ShipmentNumber +1
    ThisShipment = ShipmentNumber
  endif
  
  do while ShipmentNumber = ThisShipment
    wait window "Importing shipment # "+str(ShipmentNumber) nowait
    select x856
    if eof()
      *messagebox("POs added: "+alltrim(str(pos_added)),48,"Jones 856 Processor")
      return
    endif
    skip 1 in x856
    
    if trim(x856.segment) = "HL" and alltrim(x856.f3) = "S"
      Shipmentnumber =ShipmentNumber +1
      append blank
      ThisShipment = ShipmentNumber
      *loop
      exit
    endif
    
    if trim(x856.segment) = "HL"
      lcHLLevel = trim(x856.f3)
    endif
    
    if lcHLLevel = "S"

      if at("V1",x856.segment)>0
         replace NWDetail.vessel with upper(alltrim(x856.f2))
         lcCurrentVessel = NWDetail.vessel
         lcCurrentVoyagenum = upper(alltrim(x856.f4))
         replace NWDetail.voyagenum with lcCurrentVoyagenum
         replace NWDetail.ssco   with upper(alltrim(x856.f5))
         lcSSCO = upper(alltrim(x856.f5))
         
         if alltrim(x856.f9) = "A"
           replace NWDetail.type with "A"
           lcWOType = "A"
           replace NWDetail.awb with lcCurrentModiAWB           
         endif  
         if alltrim(x856.f9) = "S"
           replace NWDetail.type with "O"
           lcWOType = "O"
           replace NWDetail.awb with lcCurrentMasterBOL
           select newssco
           set order to sscode
           seek upper(alltrim(x856.f5))
           if found()
             replace NWDetail.ssco with newssco.carrier
           endif
           select x856
        endif  
      endif

      if trim(x856.segment) = "DTM" and upper(trim(x856.f1)) = "371"
         ldArrivedate = substr(alltrim(x856.f2),1,4)+"/"+substr(alltrim(x856.f2),5,2)+"/"+substr(alltrim(x856.f2),7,2)
         replace NWDetail.arriv_date with ctod(ldArrivedate)
      endif

      if trim(x856.segment) = "DTM" and upper(trim(x856.f1)) = "161"
         ldLoadeddate = substr(alltrim(x856.f2),1,4)+"/"+substr(alltrim(x856.f2),5,2)+"/"+substr(alltrim(x856.f2),7,2)
         replace NWDetail.loaded with ctod(ldLoadeddate)
      endif

      if trim(x856.segment) = "DTM" and upper(trim(x856.f1)) = "011"
         ldShippeddate = substr(alltrim(x856.f2),1,4)+"/"+substr(alltrim(x856.f2),5,2)+"/"+substr(alltrim(x856.f2),7,2)
         replace NWDetail.shipped with ctod(ldShippeddate)
      endif

      if trim(x856.segment) = "DTM" and upper(trim(x856.f1)) = "017"
         ldEstDeldate = substr(alltrim(x856.f2),1,4)+"/"+substr(alltrim(x856.f2),5,2)+"/"+substr(alltrim(x856.f2),7,2)
         replace NWDetail.Estdeliv with ctod(ldEstDeldate)
      endif

      if trim(x856.segment) = "REF" and upper(trim(x856.f1)) = "MB"
         replace NWDetail.hawb with alltrim(x856.f2)
         lcCurrentMasterBOL = alltrim(x856.f2)
      endif

      if trim(x856.segment) = "REF" and upper(trim(x856.f1)) = "AW"
        replace NWDetail.hawb with alltrim(x856.f2)
        lcCurrentHAWB = alltrim(x856.f2)
      endif

      if trim(x856.segment) = "R4" and upper(trim(x856.f1)) = "O"
        lcPtOrigin = alltrim(x856.f3)
      endif
    
      if trim(x856.segment) = "R4" and upper(trim(x856.f1)) = "M"
*        replace NWDetail.delloc with upper(alltrim(x856.f3))
*        lcCurrentDelloc = NWDetail.delloc
      endif
  
      if trim(x856.segment) = "R4" and upper(trim(x856.f1)) = "D"
        replace NWDetail.discharge with upper(alltrim(x856.f3))
      endif

      if trim(x856.f2) = "GDX OPT:005"
        replace NWDetail.delloc with upper(alltrim(x856.f3))
       lcCurrentDelloc = NWDetail.delloc
      endif

      if trim(x856.f2) = "GDX OPT:010"
        replace NWDetail.fwdr with upper(alltrim(x856.f3))
        lcCurrentFWDR = NWDetail.fwdr
      endif

      if trim(x856.f2) = "GDX OPT:015"
        lcCurrentMasterBOL =alltrim(x856.f3)
        tempawb = origawb
        tempawb = strtran(alltrim(x856.f3),"-","")
        tempawb = substr(tempawb,1,3)+"-"+substr(tempawb,4,4)+"-"+substr(tempawb,8,4)
        lcCurrentModiAWB = alltrim(tempawb)
        lcCurrentOrigAWB = alltrim(origawb)
      endif
    endif  
  
    if lcHLLevel = "E"
      do while lcHLLevel = "E"
        if trim(x856.segment) = "HL" and alltrim(x856.f3) != "E"
		  lcHLLevel = alltrim(x856.f3)
		  exit
        endif

      if trim(x856.segment) = "TD3"
         replace NWDetail.ctrprfx   with upper(alltrim(x856.f2))
         replace NWDetail.container with alltrim(x856.f3)
         lcCtrprfx   = upper(alltrim(x856.f2))
         lcContainer = alltrim(x856.f3)
		 lcSealNum = alltrim(x856.f9)
		 replace NWDetail.importseal with lcSealNum

         select csize
         locate for code = alltrim(x856.f10)
         if found()
           replace size with csize.size in nwdetail
           replace code with alltrim(x856.f10) in nwdetail
         else
           replace size with alltrim(x856.f10) in nwdetail
           replace code with alltrim(x856.f10) in nwdetail
         endif

         select x856
      endif 
      
      if trim(x856.segment) = "REF" and upper(trim(x856.f1)) = "AW"
        origawb =alltrim(x856.f2)
        tempawb = strtran(alltrim(x856.f2),"-","")
        if lcWOType = "A"
          tempawb = substr(tempawb,1,3)+"-"+substr(tempawb,4,4)+"-"+substr(tempawb,8,4)
        endif  
        replace NWDetail.awb with alltrim(tempawb)
        lcCurrentAWB = NWDetail.awb
      endif
      select x856
      skip 1
    enddo
  endif
   
    if lcHLLevel = "O"
      if trim(x856.segment) = "PRF"
         replace NWDetail.po_num   with alltrim(x856.f1)
         lcCurrentPO = alltrim(x856.f1)
         POs_added = POs_added +1
      endif
      if trim(x856.f2) = "GDX OPT:405"
         replace NWDetail.div  with upper(trim(x856.f3))
         lcDiv = upper(trim(x856.f3))
      endif
      if trim(x856.f2) = "GDX OPT:410"
         replace NWDetail.season with upper(trim(x856.f3))
         lcSeason = upper(trim(x856.f3))
      endif
    endif

    
    if lcHLLevel = "I"
      do while lcHLLevel = "I"

        * another shipment ?
        if trim(x856.segment) = "HL" and alltrim(x856.f3) = "S"
          ThisShipment = -1
          lcHLLevel = "S"
          select x856 
          skip -1
          loop
        endif

        if trim(x856.segment) = "HL" and (alltrim(x856.f3) = "O" OR alltrim(x856.f3) = "E")
          lcHLLevel = "O"
          select NWDetail
          scatter memvar
          append blank
          gather memvar
          replace NWDetail.awb with ""
          select x856 
          skip -1
          loop
        endif

        if trim(x856.segment) = "HL" and alltrim(x856.f3) != "S"
          lcHLLevel = alltrim(x856.f3)
          select x856 
          if empty(NWDetail.qty_type)
             lcCurrentType = "GOH"
             replace NWDetail.qty_type with "GOH"
             replace NWDetail.pl_qty   with NWDetail.units
          endif   
          exit
        endif

        if trim(x856.segment) = "N1" and upper(trim(x856.f1)) = "SF"
          select x856
          skip 1
          lcOrigin = alltrim(x856.f6)
          replace NWDetail.origin with lcOrigin
          skip -1
        endif


        if trim(x856.segment) = "N1" and upper(trim(x856.f1)) = "ST"
          select x856
          skip 1  && skip to N4
          * here we use the item level destination as the DELLOC, used to load in the shipment level data which was wrong
          lcDest = upper(alltrim(x856.f6))
		  replace delloc with lcdest in NWDetail
		  do case
		  **if "US LAX" or "US" translate to 1250 - mvw 08/30/11
		  case inlist(lcDest,"US LAX","3050") or (len(lcDest)=2 and lcdest="US")
			replace delloc with "WDP-1250" in NWDetail
		  case inlist(lcDest,"USCNO","3A","3090")
			replace delloc with "TP1" in NWDetail
		  case lcDest="1150"
			replace delloc with "WDP-1245" in NWDetail
		  endcase

          lcCurrentDelloc = NWDetail.delloc
          replace NWDetail.dest with lcDest
          skip -1

          lcItemID = upper(alltrim(NWDetail.style))
          * now that we have the style we can decode whether it goes to bldg1 or bldg2
          lnISWDP=at("WDP",NWDetail.delloc)
          lnISCRU=at("CRU",NWDetail.delloc)

          if lnISWDP=0 and lnISCRU=0 && dont calibrate the delloc unless its destined for WDP but update the label
            endPrefix = at("-",NWDetail.style)-1
            lcPrefix = substr(NWDetail.style,1,endPrefix)
            lcDivCode = "H"+alltrim(NWDetail.div)
            select NWDellocs
            locate for NWDellocs.div_code = lcDivCode and  NWDellocs.sty_prefix = lcPrefix
            if found()
              replace NWDetail.label  with NWDellocs.div_name in NWDetail
            else
              replace NWDetail.label  with "????"        in NWDetail
            endif
          else
            endPrefix = at("-",NWDetail.style)-1
            lcPrefix = substr(NWDetail.style,1,endPrefix)
            lcDivCode = "H"+alltrim(NWDetail.div)
            select NWDellocs
            locate for NWDellocs.div_code = lcDivCode and NWDellocs.sty_prefix = lcPrefix
            if found()
              replace NWDetail.delloc with "WDP"+"-"+NWDellocs.bldg in NWDetail
              replace NWDetail.label  with NWDellocs.div_name in NWDetail
            else
              replace NWDetail.delloc with "WDP"+"-1250" in NWDetail
              replace NWDetail.label  with "????"        in NWDetail
            endif
            if substr(lcDivCode,2,1) = "O"  && all outlet store freight goes to 1250
              replace NWDetail.delloc with "WDP-1250" in NWDetail
            endif              
          endif 
          select x856
        endif

		if Trim(x856.segment) = "REF" And Upper(Trim(x856.f1)) = "RV"
			replace docrcpt with x856.f2 in NWDetail
		endif

        if trim(x856.segment) = "LIN" and trim(x856.f2) = "IN"
          replace NWDetail.style  with upper(alltrim(x856.f3))
        endif
        
        if trim(x856.segment) = "SN1"
           replace NWDetail.units  with val(x856.f2)
        endif
        if trim(x856.segment) = "MEA" and trim(x856.f1) = "PD"
           replace NWDetail.cbm      with val(x856.f3)
           lnCBM = val(x856.f3)
        endif
        if trim(x856.segment) = "MEA" and trim(x856.f1) = "CT" and trim(x856.f2) = "SQ"
           replace NWDetail.pl_qty  with val(x856.f3)
           lnPlQty = val(x856.f3)
           replace NWDetail.qty_type with "CARTONS"
           lcCurrentType = "CARTONS"
        endif
*----------------------------------------------------------------------------------------------------------------
        if trim(x856.segment) = "MEA" and trim(x856.f1) = "WT"
           replace NWDetail.totalkgs  with val(x856.f3)
           replace NWDetail.weight  with NWDetail.totalkgs*2.2046224
           lnWeight         = val(x856.f3)*2.2046224
           lnTotalKilograms = val(x856.f3)
           ** now check the cbm's
*           set step on
*!*	           if (NWDetail.cbm/NWDetail.pl_qty) > .2
*!*	             replace NWDetail.cbm with (NWDetail.cbm/144.0)
*!*	             replace NWDetail.cbm with (NWDetail.cbm*0.028317)
*!*	           endif

           replace NWDetail.ctncube with (NWDetail.cbm/NWDetail.pl_qty)
        endif
*----------------------------------------------------------------------------------------------------------------
        if NWDetail.qty_type = "GOH"
           replace NWDetail.pl_qty   with NWDetail.units
        endif   
        
        select x856
        skip 1 in x856
        if eof()
          exit
        endif
      enddo
    endif
    
    
    if lcHLLevel = "P"
      lnPCTR = 0
      lnTotQty = 0
      do while lcHLLevel = "P"
        lnPCTR = lnPCtr +1
        wait window "At the P Level "+str(lnPCtr) nowait
      
        ** new line item
        if trim(x856.segment) = "HL" and alltrim(x856.f3) = "P"
          select nwpl
          append blank
          replace nwpl.shipid    with nwdetail.shipid
          replace nwpl.masterbl  with nwdetail.awb
          replace nwpl.container with nwdetail.ctrprfx+nwdetail.container
          replace nwpl.vessel    with nwdetail.vessel
          replace nwpl.voyagenum with nwdetail.voyagenum
          replace nwpl.itemid    with lcItemID
          replace nwpl.hawb      with nwdetail.hawb 
          replace nwpl.po_num    with nwdetail.po_num 
          replace nwpl.qty       with nwdetail.pl_qty
          replace nwpl.ptorigin  with lcPtOrigin
          replace nwpl.origin    with lcOrigin
          replace nwpl.dest      with nwdetail.delloc
          select x856
        endif
        if trim(x856.segment) = "LIN"
          replace nwpl.size_run with alltrim(x856.f5) in nwpl
        endif        
        if trim(x856.segment) = "PO4"
          replace nwpl.tot_pairs with alltrim(x856.f1) in nwpl
        endif        
        
       if alltrim(x856.f2) = "GDX OPT:835"
         replace nwpl.sku       with upper(alltrim(x856.f3)) in nwpl
        endif

        if trim(x856.segment) = "MAN"
          replace nwpl.bcode with alltrim(nwpl.po_num)+alltrim(substr(nwpl.sku,1,6))+alltrim(nwpl.size_run)+padl(alltrim(nwpl.tot_pairs),2,"0") in nwpl
          lnStartCtn = Val(x856.f2)
          lnEndCtn = Val(x856.f3)
          lnNumCtns = (lnEndCtn - lnStartCtn)+1
          replace nwpl.qty          with lnNumCtns in nwpl
          replace nwpl.ctnrange     with alltrim(str(lnStartCtn))+"-"+alltrim(str(lnEndCtn))
        endif

        if trim(x856.segment) = "HL" and alltrim(x856.f3) = "S"
          ThisShipment = -1
          lcCurrentAWB = ""
          lcCurrentHAWB = ""
          lcCurrentModiAWB = ""
          lcCurrentOrigAWB = ""
		  lcCurrentmasterBOL = ""
          lcHLLevel = "S"
          lcPtOrign = ""
          lcOrigin = ""
          lcDest = ""
          lcItemID = ""
          lcSealNum = ""
          lcCurrentVoyagenum = ""
          select x856 
          skip -2  && changed to -2 to pickup the BSN segment
          if trim(x856.segment) = "BSN"

          else
            skip 1 in x856
          endif  
          loop
        endif

        if trim(x856.segment) = "HL" and alltrim(x856.f3) = "O"
          lcHLLevel = "O"
          select NWDetail
          scatter memvar
          append blank
          gather memvar
          select x856
          loop
        endif

       if trim(x856.segment) = "HL" and alltrim(x856.f3) = "E"
          lcHLLevel = "E"
          select NWDetail
          scatter memvar
          append blank
          gather memvar
          select x856
          loop
        endif

        if trim(x856.segment) = "HL"
          lcHLLevel = trim(x856.f3)
        endif
        if trim(x856.segment) = "SN1"
          lnTotQty = lnTotQty + val(x856.f2)
        endif
        select x856
        skip 1 in x856
        if eof()
          exit
        endif
      enddo
*      replace JDetail.units with lnTotQty
      wait clear
    endif
  enddo   
enddo

**delete all previously imported data for ctr+shipid - mvw 03/07/11
wait "Deleting previously imported data" window nowait
select nwdetail
set order to

select * from nwdetail where filename = alltrim(upper(xfile)) group by ctrprfx,container,shipid into cursor xtemp
scan
	select nwdetail
	locate for ctrprfx+container = xtemp.ctrprfx+xtemp.container and shipid=xtemp.shipid and !empty(wo_num)

	if found()
		cfilename=filename
		xdateloaded=dateloaded
		delete for ctrprfx+container = xtemp.ctrprfx+xtemp.container and shipid=xtemp.shipid and ;
			!(filename=alltrim(upper(cfilename)) and dateloaded=xdateloaded)
	else
		delete for shipid=xtemp.shipid and ctrprfx+container = xtemp.ctrprfx+xtemp.container and filename#alltrim(upper(xfile))
	endif
endscan

select x856
wait clear
EndProc 
*************************************************************************************************************
