close data all
set exclusive off
set safety off
set deleted on
ctransfer='MJ_RADETAILLOAD'

*******************************************************************

lcpath="F:\FTPUSERS\MJ_Wholesale\IN\RAD\"
lcarchivepath="F:\FTPUSERS\MJ_Wholesale\IN\RAD\archive\"

cd &lcpath

lnnum = adir(tarray,"F:\FTPUSERS\MJ_Wholesale\IN\RAD\*.*")
set step on
if lnnum = 0
	wait window at 10,10 "    No MJ RA details to import.............." timeout 2
*!*	     Use F:\edirouting\ftpsetup Shared
*!*	     Locate For ftpsetup.transfer =  cTransfer
*!*	     If Found()
*!*	          Replace chkbusy With .F. For ftpsetup.transfer = cTransfer
*!*	     Endif
*!*	     NormalExit = .T.

*    Throw
endif
USE F:\whj\whdata\radetail IN 0
for thisfile = 1  to lnnum
* Archivename = Ttoc(Datetime(),1)
	xfile = lcpath+tarray[thisfile,1]&&+"."

*Xfile = lcPath+Allt(tarray[thisfile,1])
	!attrib -r &xfile  && Removes read-only flag from file to allow deletion

	archivefile = lcarchivepath+tarray[thisfile,1]
	Copy File "&xfile" To "&archivefile"
	lcfilename = tarray[thisfile,1]
	wait window "Importing file: "+xfile nowait
	if file(xfile)
		set step on
		create cursor bc1 (line_num c(4), sales_id c(20), slip_sid c(22), item_sid c(22),upc c(14), out_store_ c(5), in_store_n c(5),  style c(25), size c(10) ,    qty n(10,0), ;
			comments c(50), ship_reque c(20),  ship_deadl c(20), slip_no c(12) )
		appe from "&xfile"  deli with character |
DELETE FOR upc='UPC'
*******start remove duplicate slip_no from upload file
SELECT distinct slip_no FROM radetail INTO CURSOR xslipexist READWRITE
SELECT distinct slip_no FROM bc1 INTO CURSOR xslipadd READWRITE
SELECT * FROM xslipadd a LEFT JOIN xslipexist b ON a.slip_no=b.slip_no INTO cursor xslipremove readwrite
DELETE FOR slip_no_a='SLIP'
SELECT distinct slip_no_b as slip_no FROM xslipremove WHERE !ISNULL(slip_no_b) INTO CURSOR xslipremove2 readwrite
DELETE FROM bc1 WHERE slip_no in (select slip_no FROM xslipremove2)
********end remove duplicate slip_no from upload file
select bc1
scan 
scatter memvar
insert into radetail from memvar
endscan


		xsqlexec("select * from cmtrans",,,"wh")
		index on invloc tag invloc
		set order to
		
		select distinct slip_no from radetail into cursor xslip readwrite

		select invloc,upc, style,color,id,sum(qty) as qty from xslip a left join cmtrans b on a.slip_no=b.invloc group by invloc,upc, style,color,id into cursor t1 readwrite
		delete for isnull(invloc)

		select * from radetail a ;
			full join t1 b on a.slip_no=b.invloc and a.upc=b.upc into cursor t2 readwrite
		replace upc_a with upc_b for isnull(upc_a) and !isnull(upc_b) in t2
		replace style_a with alltrim(style_b)+'-'+color for isnull(style_a) and !isnull(style_b) in t2
		replace qty_a with 0 for isnull(qty_a) and !isnull(qty_b) in t2
		replace slip_no with invloc for isnull(slip_no) and !isnull(invloc) in t2
		replace size with id for isnull(size) and !isnull(id) in t2
		select ship_reque as date_requested,slip_no,out_store_ as out_store,upc_a as upc,style_a as style, size,qty_a as qty_expctd,qty_b as qty_scanned, invloc from t2 ;
			into cursor t3 readwrite
		select * from t3 where !isnull(slip_no) into cursor t4 readwrite
		delete for style='STYLE'
*		replace qty_scanned with 0 for isnull(qty_scanned)
		select date_requested,slip_no,out_store,upc, style, size,qty_expctd, qty_scanned, qty_scanned-qty_expctd as diff, invloc from t4 into cursor t5 readwrite
		copy TO "S:\MarcJacobsData\Reports\Returns_inventory\ra_detail\ra_details" Fields date_requested,slip_no,out_store,upc, style, size,qty_expctd, qty_scanned, diff type csv
*		COPY TO "S:\MarcJacobsData\Reports\mjdashboard\whseloc" type csv
*		EXPORT TO "S:\MarcJacobsData\Reports\Returns_inventory\ra_detail\ra_details_"+Ttoc(Datetime(),1) Fields date_requested,slip_no,out_store,upc, style, size,qty_expctd, qty_scanned, diff type xls
SET STEP ON 
	endif
DELETE file "&xfile"
next thisfile


