*!* m:\dev\prg\modernshoe_bkdn.prg
CLEAR
lCheckStyle = .T.
lPrepack = .T.
m.color=""
cUCCNumber = ""
cUsePack = ""
lcUCCSeed = 1

SELECT x856
COUNT FOR TRIM(segment) = "ST" TO nSegCnt
cSegCnt = ALLTRIM(STR(nSegCnt))
SELECT x856
LOCATE

lTestrun = .F.
ptctr = 0

WAIT WINDOW "Now in 940 Breakdown" TIMEOUT 1

WAIT "There are "+cSegCnt+" P/Ts in this file" WINDOW TIMEOUT 1
SELECT x856
LOCATE

*!* Constants
m.acct_name = "MODERN SHOE"
STORE nAcctNum TO m.acct_num
m.careof = " "
m.sf_addr1 = "101 SPRAGUE STREET"
m.sf_csz = "HYDE PARK, MA 02136"
m.printed = .F.
m.print_by = ""
m.printdate = DATE()
*!* End constants

*!*	IF !USED("isa_num")
*!*		IF lTesting
*!*			USE F:\3pl\test\isa_num IN 0 ALIAS isa_num
*!*		ELSE
*!*			USE F:\3pl\DATA\isa_num IN 0 ALIAS isa_num
*!*		ENDIF
*!*	ENDIF

*!*	IF !USED("uploaddet")
*!*		IF lTestrun
*!*			USE F:\ediwhse\test\uploaddet IN 0 ALIAS uploaddet
*!*		ELSE
*!*			USE F:\ediwhse\DATA\uploaddet IN 0 SHARED ALIAS uploaddet
*!*		ENDIF
*!*	ENDIF

SELECT xpt
DELETE ALL
SELECT xptdet
DELETE ALL

STORE 0 TO pt_total_eaches
STORE 0 TO pt_total_cartons
STORE 0 TO nCtnCount

STORE .F. TO lHighline,lModShoe,lMSL,lHUL
SELECT x856
SET FILTER TO

IF lTestrun
*	SET STEP ON
ENDIF

STORE "" TO cisa_num

LOCATE
WAIT WINDOW "NOW IN INITAL HEADER BREAKDOWN" TIMEOUT 1
WAIT WINDOW "NOW IN INITAL HEADER BREAKDOWN" NOWAIT
*ASSERT .F. MESSAGE "At header bkdn...debug"
DO WHILE !EOF("x856")
	cSegment = TRIM(x856.segment)
	cCode = TRIM(x856.f1)

	IF TRIM(x856.segment) = "ISA"
		cisa_num = ALLTRIM(x856.f13)
		ptctr = 0
		SELECT x856
		SKIP
		LOOP
	ENDIF

	IF TRIM(x856.segment) = "GS"
		cgs_num = ALLTRIM(x856.f6)
*!*			SELECT isa_num
*!*			LOCATE FOR isa_num.isa_num = cisa_num ;
*!*				AND isa_num.acct_num = nAcctNum
*!*			IF !FOUND()
*!*				APPEND BLANK
*!*				REPLACE isa_num.isa_num WITH cisa_num IN isa_num
*!*				REPLACE isa_num.acct_num WITH nAcctNum IN isa_num
*!*				REPLACE isa_num.office WITH cOffice IN isa_num
*!*				REPLACE isa_num.TYPE WITH "940" IN isa_num
*!*				REPLACE isa_num.ack WITH .F. IN isa_num
*!*				REPLACE isa_num.dateloaded WITH DATETIME() IN isa_num
*!*				REPLACE isa_num.filename WITH UPPER(TRIM(xfile)) IN isa_num
*!*			ENDIF
		SELECT x856
		SKIP
		LOOP
	ENDIF

	IF TRIM(x856.segment) = "ST" AND TRIM(x856.f1) = "940"
		SELECT x856
		SKIP
		LOOP
	ENDIF

	IF TRIM(x856.segment) = "W05"  && new shipment
		nCtnCount = 0
		m.style = ""
		SELECT xpt
		nDetCnt = 0
		APPEND BLANK

		REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"FILENAME*"+cFilename IN xpt
		IF !EMPTY(ALLTRIM(x856.f7))
			REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"REPLENISHMENT*Y" IN xpt
		ENDIF

		REPLACE xpt.qty WITH 0 IN xpt
		REPLACE xpt.origqty WITH 0 IN xpt
		STORE 0 TO pt_total_eaches
		STORE 0 TO pt_total_cartons
		ptctr = ptctr +1
		WAIT WINDOW AT 10,10 "Now processing PT# "+ ALLTRIM(x856.f2)+" Number "+CHR(13)+TRANSFORM(ptctr)+" of "+cSegCnt NOWAIT

		REPLACE xpt.ptid       WITH ptctr IN xpt
*		REPLACE xpt.accountid  WITH nAcctNum IN xpt
*		REPLACE xpt.office     WITH cOffice && "N"
		REPLACE xpt.ptdate     WITH DATE() IN xpt
		m.ship_ref = ALLTRIM(x856.f2)
		cship_ref = ALLTRIM(x856.f2)
		REPLACE xpt.ship_ref   WITH m.ship_ref IN xpt  && Pick Ticket
		m.pt = ALLTRIM(x856.f2)
		IF lTestrun
			REPLACE xpt.cacctnum   WITH "999" IN xpt
		ENDIF
		SELECT x856
		SKIP
		LOOP
	ENDIF

	IF TRIM(x856.segment) = "N1" AND TRIM(x856.f1) = "DE" && DUNS data
		IF TRIM(x856.f3) = "9"
			m.duns = TRIM(x856.f4)
		ENDIF
	ENDIF

	IF TRIM(x856.segment) = "N1" AND TRIM(x856.f1) = "ST" && Ship-to data
*!*			SELECT uploaddet
*!*			LOCATE
*!*			DO WHILE !FLOCK('uploaddet')
*!*				WAIT WINDOW "" NOWAIT
*!*			ENDDO
*!*			IF !EMPTY(cisa_num)
*!*				APPEND BLANK
*!*				REPLACE uploaddet.office    WITH cOffice
*!*				REPLACE uploaddet.cust_name WITH UPPER(TRIM(x856.f2))
*!*				REPLACE uploaddet.acct_num  WITH nAcctNum
*!*				REPLACE uploaddet.DATE      WITH DATETIME()
*!*				REPLACE uploaddet.isa_num   WITH cisa_num
*!*				REPLACE uploaddet.startpt   WITH xpt.ship_ref
*!*				REPLACE uploaddet.runid     WITH lnRunID
*!*			ENDIF
*!*			UNLOCK IN uploaddet

		cConsignee = UPPER(ALLTRIM(x856.f2))
		SELECT xpt
		m.st_name = cConsignee
		REPLACE xpt.consignee WITH  cConsignee IN xpt
		cStoreNum = ALLTRIM(x856.f4)
		IF EMPTY(xpt.shipins)
			REPLACE xpt.shipins WITH "STORENUM*"+cStoreNum IN xpt
		ELSE
			REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"STORENUM*"+cStoreNum IN xpt
		ENDIF
		REPLACE xpt.dcnum WITH cStoreNum IN xpt
		IF LEN(cStoreNum)>5
			REPLACE xpt.storenum  WITH VAL(RIGHT(cStoreNum,5)) IN xpt
		ELSE
			REPLACE xpt.storenum  WITH VAL(cStoreNum) IN xpt
		ENDIF

		SKIP 2 IN x856
		IF TRIM(x856.segment) = "N3"  && address info
			m.st_addr1 = UPPER(TRIM(x856.f1))
			m.st_addr2 = UPPER(TRIM(x856.f2))
			REPLACE xpt.address  WITH m.st_addr1
			REPLACE xpt.address2 WITH m.st_addr2
		ENDIF
		SKIP 1 IN x856
		IF TRIM(x856.segment) = "N4"  && address info
			m.zipbar = TRIM(x856.f3)
			m.st_csz = UPPER(TRIM(x856.f1))+", "+UPPER(TRIM(x856.f2))+" "+TRIM(x856.f3)
			IF !EMPTY(ALLTRIM(x856.f4))
				REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"COUNTRY*"+ALLTRIM(x856.f4) IN xpt
				REPLACE xpt.country WITH ALLTRIM(x856.f4) IN xpt
			ENDIF
			REPLACE xpt.csz WITH m.st_csz
		ENDIF
		SELECT x856
		SKIP
		LOOP
	ENDIF

	IF TRIM(x856.segment) = "N1" AND TRIM(x856.f1) = "SO" && Sold-to data
		REPLACE xpt.cacctnum WITH ALLTRIM(x856.f4) IN xpt
		REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"SOSITE*"+ALLTRIM(x856.f2) IN xpt
		SKIP 1 IN x856
		IF TRIM(x856.segment) = "N2"
			REPLACE xpt.upsacctnum WITH ALLTRIM(x856.f1) IN xpt
			REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"UPSACCT*"+ALLTRIM(x856.f1) IN xpt
		ELSE
			SKIP -1 IN x856
		ENDIF

		SELECT x856
		SKIP
		LOOP
	ENDIF

	IF TRIM(x856.segment) = "N1" AND TRIM(x856.f1) = "Z7" && Ship-for data
		SELECT xpt
		REPLACE xpt.shipfor WITH UPPER(ALLTRIM(x856.f2)) IN xpt
		REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"STORENAME*"+UPPER(ALLTRIM(x856.f2)) IN xpt
		REPLACE xpt.sforstore WITH ALLTRIM(x856.f4) IN xpt

		SKIP 1 IN xpt
		IF TRIM(x856.segment) = "N2"  && Dept for label use. added 01.23.2017 by JoeB
			REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"LABELNAME*"+UPPER(ALLTRIM(x856.f1)) IN xpt
		ENDIF
		SKIP -1 IN xpt

		SKIP 2 IN x856
		IF TRIM(x856.segment) = "N3"  && address info
			m.sforaddr1 = UPPER(TRIM(x856.f1))
			m.sforaddr2 = UPPER(TRIM(x856.f2))
			REPLACE xpt.sforaddr1  WITH m.sforaddr1 IN xpt
			REPLACE xpt.sforaddr2 WITH m.sforaddr2 IN xpt
		ELSE
			SELECT x856
			SKIP -1
			LOOP
		ENDIF
		SKIP 1 IN x856
		IF TRIM(x856.segment) = "N4"  && address info
			m.zipbar = TRIM(x856.f3)
			m.sforcsz = UPPER(TRIM(x856.f1))+", "+UPPER(TRIM(x856.f2))+" "+TRIM(x856.f3)
			REPLACE xpt.sforcsz WITH m.sforcsz IN xpt
		ENDIF
		SELECT x856
		SKIP
		LOOP
	ENDIF

	IF TRIM(x856.segment) = "N9" AND TRIM(x856.f1) = "DP"  && customer dept
		m.dept = ALLTRIM(x856.f2)
		REPLACE xpt.dept WITH m.dept IN xpt
		cFOB = ALLTRIM(x856.f3)
		REPLACE xpt.div WITH cFOB IN xpt
		REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"FOB*"+cFOB IN xpt
		SELECT x856
		SKIP
		LOOP
	ENDIF

* PG fixed the FOB map 7/19/2012, tweaked on 8/21/2012
	IF TRIM(x856.segment) = "N9" AND TRIM(x856.f1) = "MR"
* now check for div+divname for Neiman and Bergdorf
		IF "NEIMAN"$xpt.consignee OR "BERGDORF"$xpt.consignee && dept name, FOB
			cFOB = ALLTRIM(x856.f2)+" "+ALLTRIM(x856.f3)
			REPLACE xpt.div WITH cFOB IN xpt
			REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"DIVNAME*"+cFOB IN xpt
			SELECT x856
			SKIP
			LOOP
		ENDIF
	ENDIF


	IF TRIM(x856.segment) = "N9" AND TRIM(x856.f1) = "VNN"  && PO #
		m.cnee_ref = ALLTRIM(x856.f2)
		REPLACE xpt.cnee_ref WITH m.cnee_ref IN xpt
		IF !EMPTY(ALLTRIM(x856.f3))
			REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"VNN*"+ALLTRIM(x856.f3) IN xpt
		ENDIF
		SELECT x856
		SKIP
		LOOP
	ENDIF

	IF TRIM(x856.segment) = "N9" AND TRIM(x856.f1) = "AD"  && DUNS # -  Added 09.14.2009, Joe
		cDUNS = ALLTRIM(x856.f2)
		REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"DUNS*"+cDUNS IN xpt
		SELECT x856
		SKIP
		LOOP
	ENDIF

	IF TRIM(x856.segment) = "N9" AND x856.f1 = "CO"
		cCoCode = ALLTRIM(x856.f2)
		STORE .F. TO lModShoe,lHighline,lMSL,lHUL
		DO CASE
			CASE cCoCode = "901"  && MS Liquidation
				lMSL = .T.
			CASE cCoCode = "902"  && Highline Liquidation
				lHUL = .T.
			CASE cCoCode = "001"  && Modern Shoe
				lModShoe = .T.
			OTHERWISE
				lHighline = .T.
		ENDCASE
		REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"COMPANY*"+cCoCode IN xpt
		REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"CONAME*"+ICASE(lHUL,"HU LIQUIDATION",lMSL,"MSC LIQUIDATION",lModShoe,"MODERN SHOE","HIGHLINE") IN xpt
*	WAIT WINDOW "This is a "+ICASE(lHUL,"HIGHLINE LIQUIDATION",lMSL,"MOD.SHOE LIQUIDATION",lModShoe,"MODERN SHOE","HIGHLINE")+" upload." TIMEOUT 2  && ,lAlaguna,"NEW ALAGUNA"
	ENDIF

	IF TRIM(x856.segment) = "N9" AND TRIM(x856.f1) = "PGC"  && PO #
		IF TRIM(x856.f2)="C"
			lPrepack = .F.
			m.units = .T.
			nAcctNum = ICASE(lHUL,6669,lMSL,6671,lHighline,5910,5318)  && ,lAlaguna,6414
			REPLACE xpt.goh WITH .T. IN xpt
			REPLACE xpt.accountid WITH nAcctNum IN xpt
		ELSE
			lPrepack = .T.
			m.units = .F.
			nAcctNum = ICASE(lHUL,6668,lMSL,6670,lHighline,5864,lAlaguna,6414,4752)
			REPLACE xpt.accountid WITH nAcctNum IN xpt
		ENDIF
		IF INLIST(lHUL,lHighline)
			SELECT 0
			USE F:\3pl\DATA\mailmaster ALIAS mm SHARED
			LOCATE FOR mm.edi_type = "940" AND mm.acctname = "HIGHLINE UNITED" AND mm.office = cOffice
			STORE TRIM(IIF(mm.use_alt,mm.sendtoalt,mm.sendto)) TO tsendto
			STORE TRIM(IIF(mm.use_alt,mm.ccalt,mm.cc)) TO tcc
			USE IN mm
		ENDIF
*!*			IF lAlaguna
*!*				SELECT 0
*!*				USE F:\3pl\DATA\mailmaster ALIAS mm SHARED
*!*				LOCATE FOR mm.edi_type = "940" AND mm.acctname = "NEW ALAGUNA" AND mm.office = cOffice
*!*				STORE TRIM(IIF(mm.use_alt,mm.sendtoalt,mm.sendto)) TO tsendto
*!*				STORE TRIM(IIF(mm.use_alt,mm.ccalt,mm.cc)) TO tcc
*!*				USE IN mm
*!*			ENDIF
		SELECT x856
		IF !EOF()
			SKIP
		ENDIF
		LOOP
	ENDIF

  IF TRIM(x856.segment) = "N9" AND TRIM(x856.f1) = "MSG"  && gift message
    REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"MSG1*"+ALLTRIM(x856.f2) IN xpt
    SELECT x856
    SKIP
    LOOP
  ENDIF

	IF TRIM(x856.segment) = "N9" AND TRIM(x856.f1) = "NO"  && Customer order number (DS)
		REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"CUSTORDER*"+ALLTRIM(x856.f2) IN xpt
		SELECT x856
		SKIP
		LOOP
	ENDIF

	IF TRIM(x856.segment) = "N9" AND TRIM(x856.f1) = "SH"  && Ship-via for DS
		REPLACE xpt.ship_via WITH ALLTRIM(x856.f2) IN xpt
		SELECT x856
		SKIP
		LOOP
	ENDIF

	IF TRIM(x856.segment) = "N9" AND TRIM(x856.f1) = "MR"  && "last call direct"
		REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+ "NMLASTCALL*"+ALLTRIM(x856.f2)+" "+ALLTRIM(x856.f3)+CHR(13)IN xpt
		SELECT x856
		SKIP
		LOOP
	ENDIF

	IF TRIM(x856.segment) = "N9" AND TRIM(x856.f1) = "04"  && Terms
		REPLACE xpt.terms WITH ALLTRIM(x856.f2) IN xpt
		SELECT x856
		SKIP
		LOOP
	ENDIF

	IF TRIM(x856.segment) = "N9" AND TRIM(x856.f1) = "87"  && Flag for DS
		REPLACE xpt.shipins WITH "DROP SHIP ORDER"+CHR(13)+xpt.shipins IN xpt
		cChar = RIGHT(ALLTRIM(xpt.ship_ref),1)
		IF ISALPHA(cChar)
*
		ELSE
			REPLACE xpt.ship_ref WITH ALLTRIM(xpt.ship_ref)+"DS" IN xpt
		ENDIF
		IF "NORDSTROM"$xpt.consignee
			REPLACE xpt.terms WITH "TP" IN xpt
		ENDIF
		SELECT x856
		SKIP
		LOOP
	ENDIF

	IF TRIM(x856.segment) = "N9" AND TRIM(x856.f1) = "TV"  && Buying Unit; only for DSW
		REPLACE xpt.batch_num WITH ALLTRIM(x856.f2) IN xpt
		SELECT x856
		SKIP
		LOOP
	ENDIF

	IF TRIM(x856.segment) = "N9" AND TRIM(x856.f1) = "WH"  && Shipper's Warehouse #
		REPLACE xpt.shipins WITH xpt.shipins + CHR(13) + "WAREHOUSE*"+ALLTRIM(x856.f2) IN xpt
		SELECT x856
		SKIP
		LOOP
	ENDIF

	IF TRIM(x856.segment) = "G62" AND TRIM(x856.f1) == "10"  && start date
		lcDate = ALLTRIM(x856.f2)
		lxDate = SUBSTR(lcDate,5,2)+"/"+SUBSTR(lcDate,7,2)+"/"+LEFT(lcDate,4)
		ldDate = CTOD(lxDate)
		STORE ldDate TO dStart
		REPLACE xpt.START WITH ldDate IN xpt
		SELECT x856
		SKIP
		LOOP
	ENDIF

	IF TRIM(x856.segment) = "G62" AND TRIM(x856.f1) == "01"  && cancel date
		lcDate = ALLTRIM(x856.f2)
		lxDate = SUBSTR(lcDate,5,2)+"/"+SUBSTR(lcDate,7,2)+"/"+LEFT(lcDate,4)
		ldDate = CTOD(lxDate)
		STORE ldDate TO dCancel
		REPLACE xpt.CANCEL WITH ldDate IN xpt
		IF dCancel<dStart  && Added to check for Start/Cancel errors...10/05/07
			SELECT xpt
			BROW
			errordates()
			USE IN x856
			lOK = .F.
			RETURN
		ENDIF
		SELECT x856
		SKIP
		LOOP
	ENDIF

	IF TRIM(x856.segment) = "W66"  && Shipping info
		REPLACE xpt.ship_via WITH ALLTRIM(x856.f5) IN xpt
		REPLACE xpt.scac WITH ALLTRIM(x856.f10) IN xpt
		REPLACE xpt.terms WITH ALLTRIM(x856.f1) IN xpt
		SELECT x856
		SKIP
		LOOP
	ENDIF

	IF TRIM(x856.segment) = "LX" && start of PT detail, stay here until this PT is complete
*ASSERT .F. MESSAGE cship_ref+" Detail, Type = "+IIF(lPrepack,"Prepack","Pickpack")+"...debug"
		m.units = IIF(lPrepack,.F.,.T.)
		WAIT "NOW IN DETAIL LOOP PROCESSING" WINDOW NOWAIT

		SELECT xptdet
		CALCULATE MAX(ptdetid) TO m.ptdetid
		m.ptdetid = m.ptdetid + 1

		SELECT xptdet
		SKIP 1 IN x856

		DO WHILE ALLTRIM(x856.segment) != "SE"
			cSegment = TRIM(x856.segment)
			cCode = TRIM(x856.f1)

			IF TRIM(x856.segment) = "W01" && Destination Quantity
				SELECT xptdet
				APPEND BLANK
				REPLACE xptdet.ship_ref   WITH m.ship_ref IN xptdet  && Pick Ticket
				REPLACE xptdet.ptid WITH xpt.ptid IN xptdet
				REPLACE xptdet.ptdetid WITH m.ptdetid IN xptdet
				REPLACE xptdet.units WITH m.units IN xptdet
				m.ptdetid = m.ptdetid + 1
				REPLACE xptdet.accountid WITH xpt.accountid IN xptdet
				IF lPrepack
					cUsePack = ALLT(STR(INT(VAL(x856.f12))))
					IF EMPTY(cUsePack)
						WAIT WINDOW "Empty cUsePack at PT"+cship_ref TIMEOUT 3
						NormalExit = .F.
						errormail1()
						USE IN x856
						lOK = .F.
						RETURN
					ENDIF
					IF cUsePack == "1"
						cUsePack = ALLT(x856.f1)
					ENDIF
				ELSE
					cUsePack = "1"
				ENDIF
				REPLACE xptdet.PACK WITH cUsePack IN xptdet
				m.casepack = INT(VAL(cUsePack))
				REPLACE xptdet.casepack WITH m.casepack IN xptdet
				m.divvalue = INT(VAL(ALLTRIM(x856.f1)))
				IF lPrepack AND m.divvalue = 1
					m.divvalue = INT(VAL(cUsePack))
				ENDIF
				IF m.divvalue = 0 AND !lPrepack
					errormail1()
					USE IN x856
					lOK = .F.
					RETURN
				ENDIF

				IF !lPrepack  && Pickpack
					REPLACE xptdet.totqty WITH INT(VAL(x856.f1)) IN xptdet
				ELSE
					REPLACE xptdet.totqty WITH (m.divvalue/m.casepack) IN xptdet
				ENDIF
				REPLACE xpt.qty WITH xpt.qty + xptdet.totqty IN xpt
				REPLACE xpt.origqty WITH xpt.qty IN xpt
				REPLACE xptdet.origqty WITH xptdet.totqty IN xptdet

				IF EMPTY(TRIM(xptdet.printstuff))
					REPLACE xptdet.printstuff WITH "UNITCODE*"+ALLTRIM(x856.f2)
				ELSE
					REPLACE xptdet.printstuff WITH xptdet.printstuff+CHR(13)+"UNITCODE*"+ALLTRIM(x856.f2)
				ENDIF

				IF ALLTRIM(x856.f4) = "VN"
					REPLACE xptdet.COLOR WITH ALLTRIM(x856.f5) IN xptdet
				ENDIF

				IF !("STORENUM*"$xptdet.printstuff)
					REPLACE xptdet.printstuff WITH xptdet.printstuff+CHR(13)+"STORENUM*"+cStoreNum IN xptdet
				ENDIF
			ENDIF

			IF TRIM(x856.segment) = "G69" && shipstyle
				cPrtstr = "SHIPSTYLE*"+UPPER(TRIM(x856.f1))
				IF !("SHIPSTYLE"$xptdet.printstuff)
					REPLACE xptdet.printstuff WITH xptdet.printstuff+CHR(13)+cPrtstr IN xptdet
				ENDIF
			ENDIF

			IF TRIM(x856.segment) = "N9" AND ALLTRIM(x856.f1) = "LI"
				REPLACE xptdet.linenum WITH ALLTRIM(x856.f2) IN xptdet
			ENDIF

			IF TRIM(x856.segment) = "N9" AND INLIST(ALLTRIM(x856.f1),"CS","UP")
				cStyleIn = ALLTRIM(x856.f2)
				IF lPrepack AND LEFT(cStyleIn,3) = "000"
					cStyleIn = ALLTRIM(STR(INT(VAL(cStyleIn))))
				ENDIF
				REPLACE xptdet.STYLE WITH cStyleIn IN xptdet
				IF ALLTRIM(x856.f1) = "UP"
					REPLACE xptdet.upc WITH cStyleIn IN xptdet
				ENDIF
			ENDIF

			IF TRIM(x856.segment) = "N9" AND ALLTRIM(x856.f1) = "SZ"
				REPLACE xptdet.printstuff WITH xptdet.printstuff+CHR(13)+"SIZE*"+ALLTRIM(x856.f2) IN xptdet
			ENDIF

			IF TRIM(x856.segment) = "N9" AND ALLTRIM(x856.f1) = "CL"
				REPLACE xptdet.printstuff WITH xptdet.printstuff+CHR(13)+"COLOR*"+ALLTRIM(x856.f2) IN xptdet
			ENDIF

			IF TRIM(x856.segment) = "N9" AND ALLTRIM(x856.f1) = "JP"  && Prepack Code - DSW Only
				REPLACE xptdet.shipstyle WITH ALLTRIM(x856.f2) IN xptdet
			ENDIF

			IF TRIM(x856.segment) = "N9" AND ALLTRIM(x856.f1) = "SK"  && Customer SKU - DSW Only
				REPLACE xptdet.custsku WITH ALLTRIM(x856.f2) IN xptdet
			ENDIF

			IF TRIM(x856.segment) = "AMT" AND ALLTRIM(x856.f1) = "LP"
				REPLACE xptdet.printstuff WITH xptdet.printstuff+CHR(13)+"LISTPRICE*"+ALLTRIM(x856.f2) IN xptdet
			ENDIF

			SKIP 1 IN x856
		ENDDO

		SELECT xptdet
	ENDIF

	SELECT x856
	IF !EOF()
		SKIP 1 IN x856
	ENDIF
ENDDO

SELECT xptdet
rollup()
WAIT WINDOW "At end of ROLLUP procedure" TIMEOUT 2

DO m:\dev\prg\setuppercase940

IF lBrowfiles
	WAIT WINDOW "If tables are incorrect, CANCEL when XPTDET window opens"+CHR(13)+"Otherwise, will load PT table" TIMEOUT 2
	SELECT xpt
*	COPY TO h:\xpt TYPE XL5
	LOCATE
	BROWSE
	SELECT xptdet
	COPY TO h:\xptdet TYPE XL5
	LOCATE
	BROWSE
*	cancel
ENDIF

WAIT WINDOW cCustname+" Breakdown Round complete..." TIMEOUT 2
SELECT x856
IF USED("PT")
	USE IN pt
ENDIF
IF USED("PTDET")
	USE IN ptdet
ENDIF
WAIT CLEAR
RETURN

*************************************************************************************************
*!* Error Mail procedure - W01*12 Segment
*************************************************************************************************
PROCEDURE errormail1

	IF lEmail
		SELECT 0
		USE F:\3pl\DATA\mailmaster ALIAS mm SHARED
		LOCATE FOR mm.edi_type = "MISC" AND mm.taskname = "MODSHOEERR"
		STORE TRIM(IIF(mm.use_alt,mm.sendtoalt,mm.sendto)) TO tsendto
		STORE TRIM(IIF(mm.use_alt,mm.ccalt,mm.cc)) TO tcc
		USE IN mm
		cMailLoc = IIF(cOffice = "C","West",IIF(cOffice = "N","New Jersey","Florida"))
		tsubject= "TGF "+cMailLoc+" "+cCustname+" Pickticket Error: " +TTOC(DATETIME())
		tmessage = "File: "+cFilename+CHR(13)+" is missing W01*12 segment(s), causing a divide by zero error."
		tmessage = tmessage + "Please re-create and resend this file."
		DO FORM m:\dev\frm\dartmail2 WITH tsendto,tFrom,tsubject,tcc,tattach,tmessage,"A"
	ENDIF
ENDPROC

*************************************************************************************************
*!* Error Mail procedure - Cancel Before Start Date
*************************************************************************************************
PROCEDURE errordates

	IF lEmail
		tsendto = tsendtotest
		tcc =  tcctest
		cMailLoc = IIF(cOffice = "C","West",IIF(cOffice = "N","New Jersey","Florida"))
		tsubject= "TGF "+cMailLoc+" "+cCustname+" Pickticket Error: " +TTOC(DATETIME())
		tmessage = "File: "+cFilename+CHR(13)+" has Cancel date(s) which precede Start date(s)"
		tmessage = tmessage + "File needs to be checked and possibly resubmitted."
		DO FORM m:\dev\frm\dartmail2 WITH tsendto,tFrom,tsubject,tcc,tattach,tmessage,"A"
	ENDIF
ENDPROC

******************************
PROCEDURE rollup
******************************
	ASSERT .F. MESSAGE "In rollup procedure"
	nChangedRecs = 0
	SELECT xptdet
	SET DELETED ON
	LOCATE
	IF lTesting
*		BROWSE
	ENDIF
	cMsgStr = "PT/OSID/ODIDs changed: "
	SCAN FOR !DELETED()
		SCATTER MEMVAR
		nRecno = RECNO()
		LOCATE FOR xptdet.ptid = m.ptid ;
			AND xptdet.STYLE = m.style ;
			AND xptdet.COLOR = m.color ;
			AND xptdet.ID = m.id ;
			AND xptdet.PACK = m.pack ;
			AND xptdet.upc = m.upc ;
			AND RECNO('xptdet') > nRecno
		IF FOUND()
			SELECT xpt
			LOCATE FOR xpt.ptid = m.ptid
			IF !FOUND()
				WAIT WINDOW "PROBLEM!"
			ENDIF
			cship_ref = ALLT(xpt.ship_ref)
			IF nChangedRecs > 10
				cMsgStr = "More than 10 records rolled up"
			ELSE
				cMsgStr = cMsgStr+CHR(13)+cship_ref+" "+TRANSFORM(m.ptid)+" "+TRANSFORM(m.ptdetid)
			ENDIF
			nChangedRecs = nChangedRecs+1
			SELECT xptdet
			nTotqty1 = xptdet.totqty
			DELETE NEXT 1 IN xptdet
		ENDIF
		IF !EOF()
			GO nRecno
			REPLACE xptdet.totqty WITH xptdet.totqty+nTotqty1 IN xptdet
			REPLACE xptdet.origqty WITH xptdet.totqty IN xptdet
		ELSE
			GO nRecno
		ENDIF
	ENDSCAN

	IF nChangedRecs > 0
		WAIT WINDOW cMsgStr TIMEOUT 2
		WAIT WINDOW "There were "+TRANS(nChangedRecs)+" records rolled up" TIMEOUT 1
	ELSE
		WAIT WINDOW "There were NO records rolled up" TIMEOUT 1
	ENDIF

ENDPROC
