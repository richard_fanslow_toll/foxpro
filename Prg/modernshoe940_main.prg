*!* m:\dev\prg\modernshoe940_main.prg

PARAMETERS cOfficeIn

runack("MODERNSHOE940")

PUBLIC nAcctNum,cDelimiter,nRun_num,cTranslateOption,cWhichFile,cOffice,cCustname,cPropername,cStorenum,lOK,NormalExit 
PUBLIC cFilter,nRunID,lComplete,lTesting,m.UCCNUMBER,lEmail,lTestImport,lTestRun,emailcommentstr,cUseFolder
PUBLIC ptid,ptctr,ptqty,xfile,cfilename,nUploadCount,nFileCount,units,cUCCNumber,cMod,cISA_Num,nFileSize
PUBLIC lcPath,lcArchivepath,tsendto,tcc,NormalExit,lTestmail,LogCommentStr,lPick,lPrepack,tsendtoerr,tccerr,lImport,ArchiveFile
PUBLIC m.addby,m.adddt,m.addproc,cTransfer,lHighline,tfrom,tattach,cisa_num,thisfile,tsendtotest,tcctest,lAlaguna,lLoadSQL
PUBLIC ARRAY a856(1)
CLOSE DATABASES ALL

TRY
	NormalExit = .F.
	ON ERROR THROW

	lTesting = .f.
	
	lTestImport = lTesting
	lTestRun = lTesting
	lTestmail = lTesting
	lOverridebusy = lTesting
	lBrowfiles = lTesting
	lOverridebusy = .t.
*	lBrowfiles = .f.
	lEmail = .t.

	DO m:\dev\prg\_setvars WITH lTesting

	IF VARTYPE(cOfficeIn)="L"
		cOfficeIn = "C"
	ENDIF

*!*  	IF lTesting
*!*  	cDir = "F:\wh2\"
*!*  	dotest()
*!*  	endif	

	cCustname = "MODERN SHOE"
	ASSERT .F. MESSAGE "At start of ModernShoe 940 processing"
	SELECT 0
	USE F:\edirouting\ftpsetup SHARED
	cTransfer = "940-MODERNSHOE"
	IF !lTesting
	LOCATE FOR ftpsetup.transfer = cTransfer AND ftpsetup.TYPE = "GET"
	IF ftpsetup.chkbusy AND !lOverridebusy
		WAIT WINDOW "Process is flagged busy...returning" TIMEOUT 3
		NormalExit = .T.
		THROW
	ELSE
		REPLACE chkbusy WITH .T.,trig_time WITH DATETIME()  ;
			FOR ftpsetup.TYPE = "GET" ;
			AND ftpsetup.transfer = cTransfer ;
			IN ftpsetup
		USE IN ftpsetup
	ENDIF
	endif

	LogCommentStr = ""
	nAcctNum  = 4752
	lHighline = .F.
	lAlaguna = .F.

	STORE cOfficeIn TO cOffice
	cMod = IIF(cOffice = "C","2",cOffice)
	gOffice = cMod
	gMasterOffice = cOffice
	lLoadSQL = .t.

	tfrom = "Toll EDI Operations <toll-edi-ops@tollgroup.com>"
	STORE " " TO tattach,tsendto,tcc

	_SCREEN.WINDOWSTATE= IIF(lTesting OR lOverridebusy,2,1)

	SELECT 0
	USE F:\3pl\DATA\mailmaster ALIAS mm SHARED
	LOCATE FOR mm.edi_type = "940" AND mm.accountid = nAcctNum AND mm.office = cOffice
	IF FOUND()
		STORE TRIM(mm.acctname) TO cCustname
		STORE TRIM(mm.basepath) TO lcPath
		STORE TRIM(mm.archpath) TO lcArchivepath
		_SCREEN.CAPTION = ALLT(mm.scaption)
*!*			STORE mm.testflag 	   TO lTest
		STORE TRIM(IIF(mm.use_alt,mm.sendtoalt,mm.sendto)) TO tsendto
		STORE TRIM(IIF(mm.use_alt,mm.ccalt,mm.cc)) TO tcc
		LOCATE
		LOCATE FOR mm.edi_type = "MISC" AND mm.taskname = "GENERAL"
		STORE TRIM(IIF(mm.use_alt,mm.sendtoalt,mm.sendto)) TO tsendtotest
		STORE TRIM(IIF(mm.use_alt,mm.ccalt,mm.cc)) TO tcctest
		tsendtoerr = tsendtotest
		tccerr = tcctest
		USE IN mm
	ELSE
	SET STEP ON 
		USE IN mm
		WAIT WINDOW AT 10,10  "No parameters set for this acct# "+ALLTRIM(STR(Acctnum))+"  ---> Office "+lcOffice TIMEOUT 2
		RELEASE ALL
		NormalExit = .F.
		THROW
	ENDIF
	SET STEP ON 
	lcPath = IIF(lTesting,lcPath+"test\",lcPath)

	cUseName = "MODERN SHOE"
	cPropername = PROPER(cUseName)

	_SCREEN.CAPTION = cPropername+" 940 Process"
	_SCREEN.WINDOWSTATE = IIF(lTesting,2,1)
	CLEAR
	WAIT WINDOW "Now setting up "+cPropername+" 940 process..." TIMEOUT 2

	DO m:\dev\prg\createx856a
	SELECT x856

	cDelimiter = "*"
	cTranslateOption = "MODERNSHOE"
	cfile = ""

	dXdate1 = ""
	dXxdate2 = DATE()
	nFileCount = 0

	nRun_num = 999
	lnRunID = nRun_num

	xReturn="XXX"
	DO m:\dev\prg\wf_alt WITH cOffice,nAcctNum
	cUseFolder=xReturn

	DO ("m:\dev\PRG\"+cCustname+"940_PROCESS")

	SELECT 0
	USE F:\edirouting\ftpsetup SHARED
	REPLACE chkbusy WITH .F. ;
		FOR ftpsetup.TYPE = "GET" ;
		AND ftpsetup.transfer = "940-MODERNSHOE" ;
		IN ftpsetup
	NormalExit = .T.

CATCH TO oErr
	IF NormalExit = .F.
		SET STEP ON 
		ASSERT .F. MESSAGE "In Catch section..."
		tsubject = "ModernShoe 940 Upload Error ("+TRANSFORM(oErr.ERRORNO)+") at "+TTOC(DATETIME())
		tsendto  = tsendtoerr
		tcc = tccerr
		tmessage = "ModernShoe 940 Upload Error..... Please fix me........!"
		lcSourceMachine = SYS(0)
		lcSourceProgram = "from modernshoe940 group"

		tmessage =tmessage+ "Try/CATCH Exeception Message:"+CHR(13)+;
			[  Error: ] + STR(oErr.ERRORNO) +CHR(13)+;
			[  LineNo: ] + STR(oErr.LINENO) +CHR(13)+;
			[  Message: ] + oErr.MESSAGE +CHR(13)+;
			[  Procedure: ] + oErr.PROCEDURE +CHR(13)+;
			[  Details: ] + oErr.DETAILS +CHR(13)+;
			[  StackLevel: ] + STR(oErr.STACKLEVEL) +CHR(13)+;
			[  LineContents: ] + oErr.LINECONTENTS+CHR(13)+;
			[  UserValue: ] + oErr.USERVALUE+CHR(13)+;
			[  Computer:  ] +lcSourceMachine+CHR(13)+;
			[  940 file:  ] +xfile+CHR(13)+;
			[  Program:   ] +lcSourceProgram

		DO FORM m:\dev\frm\dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
	ELSE
		WAIT WINDOW AT 10,10 "Normal Exit " TIMEOUT 2
	ENDIF
FINALLY
	ON ERROR
	SET STATUS BAR ON
	CLOSE DATABASES ALL
ENDTRY
