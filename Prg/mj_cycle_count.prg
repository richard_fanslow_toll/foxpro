* This program extracts cycle count data from previous day
utilsetup("MJ_CYCLE_COUNT")
SET EXCLUSIVE OFF 
SET DELETED ON 
SET TALK OFF  
SET SAFETY OFF   &&Allows for overwrite of existing files
SET STEP ON 
close databases all

********************************************START CYCLE COUNT 365
xsqlexec("select * from cycle where mod='J'  and office='N' and cycledt>={"+dtoc(date()-365)+"}","cycle",,"wh")
*Wait window at 10,10 "Performing the first query...." nowait
SELECT cycleid, whseloc as Whse_Loc, cycledt as Cycle_date,	 style as Style, color as Color,  id as Size,	pack as Pack, expqty as Expected_Qty, actqty as Actual_Qty, chgqty*-1 as Changed_Qty, drivername,;
SPACE(8) as season  FROM cycle WHERE ACCOUNTID!=6453 ORDER BY cycle_date ;
INTO CURSOR cycle1
upcmastsql(6303)

SELECT a.*, upc, info FROM cycle1 a LEFT JOIN upcmast b ON a.style=b.style AND a.color=b.color AND a.size=b.id INTO CURSOR cycle2 READWRITE 
replace all SEASON with getmemodata("info","SEASON")

	If Reccount() > 0 
		COPY TO "S:\MarcJacobsData\Reports\Cycle_count\cycle_count"  TYPE CSV
		tsendto = "tmarg@fmiint.com"
		*tsendto = "chad.linville@tollgroup.com"
		tattach = "" 
		tcc =""                                                                                                                               
		tFrom ="Toll EDI System Operations <fmi-transload-ops@fmiint.com>"                                
		tmessage = "CYCLE COUNT COMPLETE_: "+Ttoc(Datetime())        
		tSubject = "CYCLE COUNT COMPLETE"                                                           
		Do Form m:\dev\frm\dartmail2 With tsendto,tFrom,tSubject, tcc,tattach,tmessage  ,"A"     
			
	ELSE 
		export TO "S:\MarcJacobsData\Reports\Cycle_count\no_cycle_counts_for_" + TTOC(DATE()-1, 0)  TYPE xls
	ENDIF
	
********************************************START CYCLE COUNT FOR THE CURRENT QUARTER	
********************************Quarterly
xday=day(date())
xdow=cdow(date())
xmonth=month(date())
xcmonth=cmonth(date())
xyear=year(date())
if xmonth =0
	xmonth=12
	xyear=year(date())-1
ENDIF
IF  INLIST(xmonth,1,2,3)
qtrmonth =1 
ENDIF
IF  INLIST(xmonth,4,5,6)
qtrmonth =4  
ENDIF
IF  INLIST(xmonth,7,8,9)
qtrmonth =7 
ENDIF
IF INLIST(xmonth,10,11,12)
qtrmonth =10 
ENDIF

*!*	xstartdt=ctod(transform(xmonth)+"/1/"+transform(xyear))
*!*	xenddt=gomonth(xstartdt,1)-1
	
qrtstart=ctod(transform(qtrmonth)+"/1/"+transform(xyear)) 
SET STEP ON 	
SELECT cycleid, whseloc as Whse_Loc, cycledt as Cycle_date,	 style as Style, color as Color,  id as Size,	pack as Pack, expqty as Expected_Qty, actqty as Actual_Qty, chgqty*-1 as Changed_Qty, drivername,;
SPACE(8) as season  FROM cycle WHERE cycledt>=qrtstart AND ACCOUNTID!=6453  ORDER BY cycle_date ;
INTO CURSOR cycle1
SET STEP ON 
******* identify empty locations
useca('whseloc','wh',.t.)
SELECT * FROM whseloc where mod='J' AND office='N' AND cyclecntdt >=qrtstart INTO CURSOR t1 READWRITE 
SELECT distinct whse_loc as whseloc FROM cycle1 INTO CURSOR vdone1 READWRITE
DELETE FROM t1 WHERE whseloc in (select whseloc FROM vdone1)


SELECT whselocid as cycleid, whseloc as whse_loc, cyclecntdt as cycle_date, SPACE(20) as style, SPACE(10) as color, SPACE(10) as size, SPACE(10) as pack, ;
'0' as expected_qty, '0' as actual_qty, '0' as changed_qty, 'EMPTY LOC' as drivername, SPACE(10) as season FROM t1  INTO CURSOR c2 READWRITE
SELECT  cycleid, whse_loc,  cycle_date,  style,  color,  size,  pack, VAL(expected_qty) as expected_qty, val(actual_qty) as actual_qty, val(changed_qty) as changed_qty,  drivername, season ;
FROM c2  INTO CURSOR c3 READWRITE
SELECT * FROM c3 UNION ALL SELECT * FROM cycle1 INTO CURSOR cycle2 READWRITE 
DELETE for ISNULL(cycleid)


SELECT a.*, upc, info FROM cycle2 a LEFT JOIN upcmast b ON a.style=b.style AND a.color=b.color AND a.size=b.id INTO CURSOR cycle3 READWRITE 
replace all SEASON with getmemodata("info","SEASON")

	If Reccount() > 0 
		COPY TO "S:\MarcJacobsData\Reports\Cycle_count\cycle_count_qtrly"  TYPE CSV
		tsendto = "tmarg@fmiint.com"	&& Maybe this should go to a group than be managed so hundreds of emails don't go to accounts every day that don't get checked.
		*tsendto = "chad.linville@tollgroup.com"
		tattach = "" 
		tcc =""                                                                                                                               
		tFrom ="Toll EDI System Operations <fmi-transload-ops@fmiint.com>"                                
		tmessage = "QRTLY_CYCLE COUNT COMPLETE_: "+Ttoc(Datetime())        
		tSubject = "QRTLY_CYCLE COUNT COMPLETE"                                                           
		Do Form m:\dev\frm\dartmail2 With tsendto,tFrom,tSubject, tcc,tattach,tmessage  ,"A"     
			
	ELSE 
		export TO "S:\MarcJacobsData\Reports\Cycle_count\no_qtrly_cycle_counts_for_" + TTOC(DATE()-1, 0)  TYPE xls
	ENDIF

********************************************START CYCLE COUNT WHSE LOC	
useca ("WHSELOC",'wh',,,'m-j')

goffice="J"
xsqlexec("select * from invenloc where mod='"+goffice+"'",,,"wh")

useca ("whloctype",'wh',.t.)
SELECT whseloc FROM whseloc WHERE whseloc !='PL' ORDER BY whseloc INTO CURSOR t1 READWRITE
SELECT accountid, WHSELOC ,SUM(LOCQTY) AS LOCQTY FROM INVENLOC WHERE LOCQTY!=0 GROUP BY WHSELOC INTO CURSOR V1 READWRITE
SELECT ACCOUNTID, A.WHSELOC,LOCQTY , SPACE(5) AS INVENTORY FROM T1 A LEFT JOIN V1 B ON A.WHSELOC=B.WHSELOC INTO CURSOR CI1 READWRITE
REPLACE inventory WITH 'YES' FOR !ISNULL(LOCQTY)
REPLACE inventory WITH 'NO' FOR ISNULL(LOCQTY)
DELETE FOR ACCOUNTID=6453
SELECT ACCOUNTID,  WHSELOC, INVENTORY FROM CI1 INTO CURSOR CI2 READWRITE
SELECT a.*,descrip FROM ci2 a LEFT JOIN whloctype b ON a.whseloc=b.whseloc INTO CURSOR ci3 READWRITE
COPY TO "S:\MarcJacobsData\Reports\Cycle_count\whseloc" type csv
SET STEP ON 
********************************************START CYCLE COUNT WHSE LOC	QRTLY
IF	INLIST(qtrmonth,1,4,7,10) AND day(date())=1
useca ("WHSELOC",'wh',,,'m-j')
useca ("whloctype",'wh',.t.)
SELECT whseloc FROM whseloc WHERE whseloc !='PL' ORDER BY whseloc INTO CURSOR t1 READWRITE
SELECT accountid, WHSELOC ,SUM(LOCQTY) AS LOCQTY FROM INVENLOC WHERE LOCQTY!=0 GROUP BY WHSELOC INTO CURSOR V1 READWRITE
SELECT ACCOUNTID, A.WHSELOC,LOCQTY , SPACE(5) AS INVENTORY FROM T1 A LEFT JOIN V1 B ON A.WHSELOC=B.WHSELOC INTO CURSOR CI1 READWRITE
REPLACE inventory WITH 'YES' FOR !ISNULL(LOCQTY)
REPLACE inventory WITH 'NO' FOR ISNULL(LOCQTY)
DELETE FOR ACCOUNTID=6453
SELECT ACCOUNTID,  WHSELOC, INVENTORY FROM CI1 INTO CURSOR CI2 READWRITE
SELECT a.*,descrip FROM ci2 a LEFT JOIN whloctype b ON a.whseloc=b.whseloc INTO CURSOR ci3 READWRITE
SET STEP ON 
COPY TO "S:\MarcJacobsData\Reports\Cycle_count\whselocQTRLY_"+TRANSFORM(qtrmonth)+"_"+TRANSFORM(xyear)  type csv
ELSE
ENDIF
********************************************START CYCLE COUNT DAILY
SELECT cycledt, accountid, drivername,style,color,id,expqty,actqty,chgqty, whseloc FROM cycle WHERE INLIST(accountid,6303,6325,6543) AND cycledt =DATE()-1 INTO CURSOR vdly READWRITE
	If Reccount() > 0 
		COPY TO "S:\MarcJacobsData\Reports\Cycle_count\cycle_count_daily"  TYPE XLS
		tsendto = "brian.mcglone@tollgroup.com"
		tattach = "S:\MarcJacobsData\Reports\Cycle_count\cycle_count_daily.XLS" 
		tcc =""                                                                                                                               
		tFrom ="Toll EDI System Operations <fmi-transload-ops@fmiint.com>"                                
		tmessage = "DAILY_CYCLE COUNT COMPLETE_: "+Ttoc(Date()-1)        
		tSubject = "DAILY_CYCLE COUNT COMPLETE"                                                           
		Do Form m:\dev\frm\dartmail2 With tsendto,tFrom,tSubject, tcc,tattach,tmessage  ,"A"     
			
	ELSE 
	ENDIF


********************************************START CYCLEZ
SELECT accountid as account, style,color,id,whseloc,locqty , DTOC(adddt) as add_date FROM invenloc WHERE INLIST(accountid,6303,6325,6543) AND WHSELOC='CYCLE' AND locqty!=0 INTO CURSOR t1 READWRITE
SELECT a.*,SPACE(10) AS SEASON, upc, info FROM T1 a LEFT JOIN upcmast b ON a.style=b.style AND a.color=b.color AND a.ID=b.id INTO CURSOR T2 READWRITE 
replace all SEASON with getmemodata("info","SEASON")
	If Reccount() > 0 
		COPY TO "S:\MarcJacobsData\Reports\Cycle_count\cyclez_inventory"  TYPE CSV
		tsendto = "tmarg@fmiint.com"
		*tsendto = "chad.linville@tollgroup.com"
		tattach = "" 
		tcc =""                                                                                                                               
		tFrom ="Toll EDI System Operations <fmi-transload-ops@fmiint.com>"                                
		tmessage = "CYCLEZ INVENTORY FOR_: "+Ttoc(Datetime())        
		tSubject = "CYCLEZ INVENTORY COMPLETE"                                                           
		Do Form m:\dev\frm\dartmail2 With tsendto,tFrom,tSubject, tcc,tattach,tmessage  ,"A"     
			
	ELSE 
	ENDIF

	

********************************************START PRE COUNT
*!*	USE F:\wh\pi_pre_count.Dbf IN 0
*!*	SELECT * FROM pi_pre_count WHERE INLIST(accountid,6303,6325,6543) AND adddt>{07/01/2016} into CURSOR c1 READWRITE
*!*		If Reccount() > 0 
*!*			COPY TO "S:\MarcJacobsData\Reports\Cycle_count\pi_pre_count"  TYPE CSV
*!*			tsendto = "tmarg@fmiint.com"
*!*			tattach = "" 
*!*			tcc =""                                                                                                                               
*!*			tFrom ="Toll EDI System Operations <fmi-transload-ops@fmiint.com>"                                
*!*			tmessage = "PI PRE COUNT COMPLETE_: "+Ttoc(Datetime())        
*!*			tSubject = "PI PRE COUNT COMPLETE"                                                           
*!*			Do Form m:\dev\frm\dartmail2 With tsendto,tFrom,tSubject, tcc,tattach,tmessage  ,"A"     
*!*				
*!*		ELSE 
*!*	*!*			tsendto = "tmarg@fmiint.com"
*!*	*!*			tattach = "" 
*!*	*!*			tcc =""                                                                                                                               
*!*	*!*			tFrom ="Toll EDI System Operations <fmi-transload-ops@fmiint.com>"                                
*!*	*!*			tmessage = "CYCLE COUNT COMPLETE_: "+Ttoc(Datetime())        
*!*	*!*			tSubject = "CYCLE COUNT COMPLETE"                                                           
*!*	*!*			Do Form m:\dev\frm\dartmail2 With tsendto,tFrom,tSubject, tcc,tattach,tmessage  ,"A"   

*!*		ENDIF
*!*	close data all 


********************************* START DRIVER CYCLE COUNT LOCATIONS PER DAY

SELECT  drivername,cycledt,whseloc, SUM(actqty) as qty FROM cycle WHERE cycledt>=DATE()-5 GROUP BY drivername,cycledt,whseloc  INTO CURSOR t1 READWRITE
*SELECT drivername, cycledt, COUNT(1) as count FROM t1 GROUP BY drivername, cycledt INTO CURSOR t2 READWRITE
*SELECT drivername, cycledt, COUNT(1) as loc_count FROM t1 GROUP BY drivername, cycledt INTO CURSOR t2 READWRITE
SELECT drivername, cycledt, COUNT(1) as loc_count, SUM(qty) as unit_qty FROM t1 GROUP BY drivername, cycledt INTO CURSOR t3 READWRITE
	If Reccount() > 0 
		export TO "S:\MarcJacobsData\TEMP\driver_cycle_loc_count"  TYPE xls
		*tsendto = "tmarg@fmiint.com,brian.mcglone@tollgroup.com"
		tsendto = "brian.mcglone@tollgroup.com"
		tattach = "S:\MarcJacobsData\TEMP\driver_cycle_loc_count.xls" 
		tcc =""                                                                                                                               
		tFrom ="Toll EDI System Operations <fmi-transload-ops@fmiint.com>"                                
		tmessage = "Driver Cycle count location by day_: "+Ttoc(Datetime())        
		tSubject = "Driver Cycle count location by day is COMPLETE"                                                           
		Do Form m:\dev\frm\dartmail2 With tsendto,tFrom,tSubject, tcc,tattach,tmessage  ,"A"     
			
	ELSE 
*!*			tsendto = "tmarg@fmiint.com"
*!*			tattach = "" 
*!*			tcc =""                                                                                                                               
*!*			tFrom ="Toll EDI System Operations <fmi-transload-ops@fmiint.com>"                                
*!*			tmessage = "CYCLE COUNT COMPLETE_: "+Ttoc(Datetime())        
*!*			tSubject = "CYCLE COUNT COMPLETE"                                                           
*!*			Do Form m:\dev\frm\dartmail2 With tsendto,tFrom,tSubject, tcc,tattach,tmessage  ,"A"   

	ENDIF


Wait window at 10,10 "all done...." timeout 2
CLOSE DATABASES all

schedupdate()
_screen.Caption=gscreencaption
on error

