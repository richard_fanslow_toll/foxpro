Close Data All

wo=getwonumber()

If Empty(wo)
  Return
Endif

goffice="J"

if usesqloutwo()
	xsqlexec("select * from outwolog where wo_num="+transform(wo),,,"wh")
else
	Use F:\whj\whdata\outwolog In 0
endif

Select outwolog
Locate For wo_num = Val(Alltrim(Transform(wo)))

If !Found()
  Messagebox("Cant find that work order",0,"MJ UPC Updater")
Else
  destacct =  outwolog.accountid
  lnAcct = outwolog.accountid
  If lnAcct = 6305
    lnAcct = 6303
  Endif
  If lnAcct = 6320
    lnAcct = 6304
  Endif
Endif

Set Exclusive Off
Set Safety Off

if usesqloutwo()
	xsqlexec("select * from outdet where wo_num="+transform(wo),,,"wh")
else
	Use (wf("J",lnAcct)+"outdet") In 0
endif

nWO = Val(Alltrim(Transform(wo)))
lcNoUPC = ""


If Inlist(destacct,6322,6323,6321)
  Do Case
  Case destacct = 6322  && France
    Use F:\wh\mjfrmaster In 0 Alias tempmast
  Case destacct = 6323  && Italy
    Use F:\wh\mjitmaster In 0 Alias tempmast
  Case destacct = 6321  && UK
    Use F:\wh\mjukmaster In 0 Alias tempmast
  Endcase
Endif
MsgStr =""
Select outdet
Goto Top
Scan For wo_num = nWO
  destacct= outdet.accountid
  SeekStr =Str(lnAcct,4)+outdet.Style+outdet.Color+outdet.Id

  if upcmastsql(lnAcct,outdet.Style,outdet.Color,outdet.Id)
    Select upcmast
    If Inlist(lnAcct,6303,6305)
      lcItem = upcmast.upc
    Else
      lcItem = GetMemoData("info","ITEM")
    Endif
    Replace outdet.upc With lcItem In outdet
  Else
    If Inlist(destacct,6322,6323,6321)
      Set Step On
      Do Case
      Case destacct = 6322  && France
        lcDestAcct = "MJ France"
        =Seek(Str(6322,4)+outdet.Style+outdet.Color+outdet.Id,"tempmast","stylecolid")
      Case destacct = 6323  && Italy
        lcDestAcct = "MJ Italy"
        =Seek(Str(6323,4)+outdet.Style+outdet.Color+outdet.Id,"tempmast","stylecolid")
      Case destacct = 6321  && UK
        lcDestAcct = "MJ UK"
        =Seek(Str(6321,4)+outdet.Style+outdet.Color+outdet.Id,"tempmast","stylecolid")
      Endcase
      If Found("tempmast")
        Select tempmast
        Scatter Fields Except upcmastid Memvar Memo
        Select upcmast
        Append Blank
        m.accountid = destacct
        m.addby="UPCUPDATER"
        m.adddt=Datetime()
        m.updateby="PAULG"
        m.addproc ="UPCUPDATER"
        Gather Memvar Memo Fields Except upcmastid
      Else
        MsgStr = MsgStr+"Style: "+outdet.Style+"  Color: "+outdet.Color+"  Size: "+outdet.Id
      Endif
    Endif
    lcNoUPC= lcNoUPC+outdet.Style+outdet.Color+outdet.Id
  Endif

Endscan



Select outdet
Select Style,Color,Id, upc  From outdet Where wo_num=nWO And Len(Alltrim(upc))=0 Into Cursor temp
lcStr ="All UPCs loaded........."

If Reccount("temp") >0
  lcStr="Need UPCs for the following items:"+Chr(13)
  Select temp
  Scan
    lcStr = lcStr +Style+Color+Id+upc+Chr(13)
  Endscan
  Messagebox(lcStr,32,"Marc Jacobs UPC Loader")
Else
  Messagebox(lcStr,32,"Marc Jacobs UPC Loader")
Endif

Return
************************************************************
*  FUNCTION getwonumber()
************************************************************
Function getwonumber()
Local lnWo,lnRetVal
lcRetval=""
Do While .T.
  lcBOL=Inputbox('WO #',"Enter a Marc Jacobs to attach UPCs........")   && WO num has to be an integer
  lcRetval=lcBOL
  If Empty(lcBOL)  && either nothing was put into the box or cancel was pressed
    lnAnswer=Messagebox("There is no WO  entered or Cancel was pressed"+ Chr(13)+;
      "   Do you want to exit program ?",36)
    If lnAnswer=6  && yes exit
      Return lcRetval  && return 0
    Else  && try again
      Loop
    Endif
  Endif
  Return lcRetval
Enddo
Endfunc
************************************************************
