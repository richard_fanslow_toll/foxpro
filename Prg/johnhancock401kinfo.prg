* 401k info for John hancock

* Build EXE = F:\UTIL\JOHNHANCOCK\HRJOHNHANCOCK401KINFO.EXE


LOCAL loJohnHancock401k
loJohnHancock401k = CREATEOBJECT('JohnHancock401k')
loJohnHancock401k.MAIN()
CLOSE DATABASES ALL
RETURN

#DEFINE LOGIT 1
#DEFINE WAITIT 2
#DEFINE NOWAITIT 4
#DEFINE SENDIT 8
#DEFINE CRLF CHR(13) + CHR(10)
#DEFINE RETURN_DATA_MANDATORY .F.
#DEFINE RETURN_DATA_NOT_MANDATORY .T.
#DEFINE CSVFORMAT 6
#DEFINE WRITE_ONLY_UNBUFFERED 11
#DEFINE COMMA ","

DEFINE CLASS JohnHancock401k AS CUSTOM

	cProcessName = 'JohnHancock401k'

	lTestMode = .T.  && .F. FOR NORMAL OPERATION, .T. TO SUPPRESS ALL BUT DEVELOPER EMAILS AND CHANGE INPUT TABLE, ETC.

	lAutoYield = .T.

	cStartTime = TTOC(DATETIME())
	
	* connection properties
	nSQLHandle = 0

	* wait window properties
	nWaitWindowTimeout = 2
	lWaitWindowIsOn = .T.

	* logfile properties
	lLoggingIsOn = .T.
	cLogFile = 'F:\UTIL\ADPREPORTS\LOGFILES\John_Hancock_401k_REPORT_log.txt'

	* INTERNAL email properties
	lSendInternalEmailIsOn = .T.
	cFrom = 'TGF Corporate <fmicorporate@fmiint.com>'
	cSendTo = 'mbennett@fmiint.com'
	cCC = ''
	cSubject = 'John Hancock 401k Report for ' + DTOC(DATE())
	cAttach = ''
	cBodyText = ''

	cCOMPUTERNAME = ''
	cUSERNAME = ''


	FUNCTION INIT
		IF NOT DODEFAULT()
			RETURN .F.
		ENDIF
		WITH THIS
			*SET RESOURCE OFF
			CLOSE DATA
			SET CENTURY ON
			SET DATE AMERICAN
			SET HOURS TO 24
			SET ANSI ON
			SET TALK OFF
			SET DELETED ON
			SET CONSOLE OFF
			SET EXCLUSIVE OFF
			SET SAFETY OFF
			SET EXACT OFF
			SET STATUS BAR ON
			SET SYSMENU OFF
			SET ENGINEBEHAVIOR 70
			SET STRICTDATE TO 0
			_VFP.AUTOYIELD = .lAutoYield
			.cCOMPUTERNAME = GETENV("COMPUTERNAME")
			.cUSERNAME = GETENV("USERNAME")
			IF .lTestMode THEN
				.cLogFile = 'F:\UTIL\ADPREPORTS\LOGFILES\John_Hancock_401k_REPORT_log_TESTMODE.txt'
				.cSendTo = 'Mark Bennett <mbennett@fmiint.com>'
				.cCC = ''
			ENDIF
			.lLoggingIsOn = .lLoggingIsOn AND NOT EMPTY(.cLogFile)
			IF .lLoggingIsOn THEN
				SET ALTERNATE TO (.cLogFile) ADDITIVE
				SET ALTERNATE ON
			ENDIF
		ENDWITH
	ENDFUNC


	FUNCTION DESTROY
		WITH THIS
			IF .lLoggingIsOn  THEN
				SET ALTERNATE OFF
				SET ALTERNATE TO
			ENDIF
		ENDWITH
		DODEFAULT()
	ENDFUNC


	FUNCTION MAIN
		WITH THIS
			LOCAL lnNumberOfErrors, lcSQL, lcSQL2, lcSQL3, lcSQL4, lcSQL5, lcSQL6, lcSQL7, lcSQL8, lcSQL9, loError, ldBlankDate
			LOCAL lcFiletoSaveAs, lcSpreadsheetTemplate, lcWhere
			LOCAL oExcel, oWorkbook, oWorksheet, lnRow, lcRow
			LOCAL lnTwoMonthsAgo, lnHireMonth, lnHireYear, ldEligible
			LOCAL lcName, lcCompany, ldEligdate, lcSS_NUM, lnEmployeeCount, lcDupeList
			
			TRY
				lnNumberOfErrors = 0

				.TrackProgress('', LOGIT)
				.TrackProgress('', LOGIT)
				.TrackProgress('=========================================================', LOGIT+SENDIT)
				.TrackProgress("John Hancock 401kprocess started....", LOGIT+SENDIT+NOWAITIT)
				.TrackProgress('COMPUTERNAME = ' + .cCOMPUTERNAME, LOGIT+SENDIT)
				.TrackProgress('USERNAME = ' + .cUSERNAME, LOGIT+SENDIT)
				.TrackProgress('=========================================================', LOGIT+SENDIT)
				IF .lTestMode THEN
					.TrackProgress('                        TEST MODE                        ', LOGIT+SENDIT)
					.TrackProgress('=========================================================', LOGIT+SENDIT)
				ENDIF
				.TrackProgress('', LOGIT)

				OPEN DATABASE F:\UTIL\ADPCONNECTION\CONNECT1

				.nSQLHandle = SQLCONNECT("PCPAYWIN","MBENNETT","VP737A")
				
				ldBlankDate = CTOD('')

				IF .nSQLHandle > 0 THEN


					* get basic info for anyone who had was on payroll in 2013.
					* company is needed because calcs are different for hourly vs salaried
					* DISTINCT added to prevent people who had more than one file# from showing up twice.
					lcSQL = ;
						" SELECT DISTINCT " + ;
						" A.COMPANYCODE AS ADP_COMP, " + ;
						" A.NAME, " + ;
						" A.STATUS, " + ;
						" A.SOCIALSECURITY# AS SS_NUM, " + ;
						" A.GENDER, " + ;
						" A.BIRTHDATE, " + ;
						" A.HIREDATE, " + ;
						" A.TERMINATIONDATE AS TERMDATE, " + ;
						" 0.0 AS DUPEFLAG " + ;
						" FROM REPORTS.V_EMPLOYEE A " + ;
						" WHERE EXISTS " + ;
						" (SELECT * FROM REPORTS.V_CHK_VW_INFO WHERE CHECKVIEWYEAR# = 2013 AND COMPANYCODE = A.COMPANYCODE AND NAME = A.NAME) " + ;
						" AND A.NAME IN ( SELECT NAME FROM REPORTS.V_EMPLOYEE WHERE " + ;
						" {fn LEFT(HOMEDEPARTMENT,2)} IN ('20','21','22','23','24','25','27','28','29','30','31','32','35','36','37','38','39','71','72','73','91') AND STATUS <> 'T' ) "  + ;
						" ORDER BY A.COMPANYCODE, A.NAME, A.SOCIALSECURITY#, A.STATUS DESC"
						
					* get # weeks worked for salaried
					lcSQL2 = ;
						"SELECT " + ;
						" COMPANYCODE AS ADP_COMP, " + ;
						" NAME, " + ;
						" SOCIALSECURITY# AS SS_NUM, " + ;
						" COUNT( DISTINCT CHECKVIEWWEEK# ) AS NUM_WEEKS " + ;
						" FROM REPORTS.V_CHK_VW_INFO " + ;
						" WHERE CHECKVIEWYEAR# = 2013 " + ;
						" AND COMPANYCODE NOT IN ('SXI','E87') " + ;
						" AND {FN IFNULL(CHECKVIEWGROSSPAYA,0.00)} > 0.00 " + ; 
						" GROUP BY COMPANYCODE, NAME, SOCIALSECURITY# " + ;
						" ORDER BY COMPANYCODE, NAME "

					* get hourly hours
					lcSQL3 = ;
						"SELECT " + ;
						" COMPANYCODE AS ADP_COMP, " + ;
						" NAME, " + ;
						" SOCIALSECURITY# AS SS_NUM, " + ;
						" SUM({fn IFNULL(CHECKVIEWREGHOURS,0000.00)}) AS REG_HOURS, " + ;
						" SUM({fn IFNULL(CHECKVIEWOTHOURS,0000.00)}) AS OT_HOURS, " + ;
						" SUM({fn IFNULL(CHECKVIEWHOURSAMT,0000.00)}) AS OTHER_HRS " + ;
						" FROM REPORTS.V_CHK_VW_HOURS " + ;
						" WHERE CHECKVIEWYEAR# = 2013 " + ;
						" AND COMPANYCODE IN ('SXI','E87') " + ;
						" GROUP BY COMPANYCODE, NAME, SOCIALSECURITY# "
					
					* get total grosspay, regearning - no dates
					lcSQL4 = ;
						"SELECT " + ;
						" COMPANYCODE AS ADP_COMP, " + ;
						" NAME, " + ;
						" SOCIALSECURITY# AS SS_NUM, " + ;
						" SUM({fn IFNULL(CHECKVIEWGROSSPAYA,0000.00)}) AS GROSSPAY, " + ;
						" SUM({fn IFNULL(CHECKVIEWREGERNING,0000.00)}) AS REGERNING " + ;
						" FROM REPORTS.V_CHK_VW_INFO " + ;
						" WHERE CHECKVIEWYEAR# = 2013 " + ;
						" GROUP BY COMPANYCODE, NAME, SOCIALSECURITY# "
						
					* get bonuses - no dates
					lcSQL5 = ;
						"SELECT " + ;
						" COMPANYCODE AS ADP_COMP, " + ;
						" NAME, " + ;
						" SOCIALSECURITY# AS SS_NUM, " + ;
						" SUM({fn IFNULL(CHECKVIEWEARNSAMT,0.00)}) AS BONUS " + ;
						" FROM REPORTS.V_CHK_VW_EARNINGS " + ;
						" WHERE CHECKVIEWYEAR# = 2013 AND CHECKVIEWEARNSCD = 'B' " + ;
						" GROUP BY COMPANYCODE, NAME, SOCIALSECURITY# "
						
					
					* get total grosspay, regearning - with dates to support later selects
					lcSQL6 = ;
						"SELECT " + ;
						" COMPANYCODE AS ADP_COMP, " + ;
						" NAME, " + ;
						" SOCIALSECURITY# AS SS_NUM, " + ;
						" CHECKVIEWPAYDATE AS PAYDATETM, " + ;
						" SUM({fn IFNULL(CHECKVIEWGROSSPAYA,0000.00)}) AS GROSSPAY, " + ;
						" SUM({fn IFNULL(CHECKVIEWREGERNING,0000.00)}) AS REGERNING " + ;
						" FROM REPORTS.V_CHK_VW_INFO " + ;
						" WHERE CHECKVIEWYEAR# = 2013 " + ;
						" GROUP BY COMPANYCODE, NAME, SOCIALSECURITY#, CHECKVIEWPAYDATE " + ;
						" ORDER BY COMPANYCODE, NAME, CHECKVIEWPAYDATE "
						
					* get bonuses - with dates to support later selects
					lcSQL7 = ;
						"SELECT " + ;
						" COMPANYCODE AS ADP_COMP, " + ;
						" NAME, " + ;
						" SOCIALSECURITY# AS SS_NUM, " + ;
						" CHECKVIEWPAYDATE AS PAYDATETM, " + ;
						" SUM({fn IFNULL(CHECKVIEWEARNSAMT,0.00)}) AS BONUS " + ;
						" FROM REPORTS.V_CHK_VW_EARNINGS " + ;
						" WHERE CHECKVIEWYEAR# = 2013 AND CHECKVIEWEARNSCD = 'B' " + ;
						" GROUP BY COMPANYCODE, NAME, SOCIALSECURITY#, CHECKVIEWPAYDATE " + ;
						" ORDER BY COMPANYCODE, NAME, CHECKVIEWPAYDATE "

					* get Roth ira & 401k loan deductions
					lcSQL8 = ;
						"SELECT " + ;
						" COMPANYCODE AS ADP_COMP, " + ;
						" NAME, " + ;
						" SOCIALSECURITY# AS SS_NUM, " + ;
						" CHECKVIEWDEDCODE AS DEDCODE, " + ;
						" SUM({fn IFNULL(CHECKVIEWDEDAMT,0.00)}) AS DEDAMT " + ;
						" FROM REPORTS.V_CHK_VW_DEDUCTION " + ;
						" WHERE CHECKVIEWDEDCODE IN ('N','O','P','R') " + ;
						" AND CHECKVIEWYEAR# = 2013 " + ;
						" GROUP BY COMPANYCODE, NAME, SOCIALSECURITY#, CHECKVIEWDEDCODE "

					* get 401k, WHICH HAS MULTIPLE CODES
					lcSQL85 = ;
						"SELECT " + ;
						" COMPANYCODE AS ADP_COMP, " + ;
						" NAME, " + ;
						" SOCIALSECURITY# AS SS_NUM, " + ;
						" SUM({fn IFNULL(CHECKVIEWDEDAMT,0.00)}) AS DEDAMT " + ;
						" FROM REPORTS.V_CHK_VW_DEDUCTION " + ;
						" WHERE CHECKVIEWDEDCODE IN ('4','5','21','81','83','84','K') " + ;
						" AND CHECKVIEWYEAR# = 2013 " + ;
						" GROUP BY COMPANYCODE, NAME, SOCIALSECURITY# "

					* get 401k match
					lcSQL9 = ;
						"SELECT " + ;
						" COMPANYCODE AS ADP_COMP, " + ;
						" NAME, " + ;
						" SOCIALSECURITY# AS SS_NUM, " + ;
						" SUM({FN IFNULL(CHECKVIEWMEMOAMT,0.00)}) AS MATCH_AMT " + ;
						" FROM REPORTS.V_CHK_VW_MEMO " + ;
						" WHERE (CHECKVIEWMEMOCD = 'K') " + ;
						" AND CHECKVIEWYEAR# = 2013 " + ;
						" GROUP BY COMPANYCODE, NAME, SOCIALSECURITY# "

					IF .lTestMode THEN
						.TrackProgress('                        TEST MODE                        ', LOGIT+SENDIT)
						.TrackProgress('lcSQL =' + lcSQL, LOGIT+SENDIT)
						.TrackProgress('                        TEST MODE                        ', LOGIT+SENDIT)
						.TrackProgress('lcSQL2 =' + lcSQL2, LOGIT+SENDIT)
						.TrackProgress('                        TEST MODE                        ', LOGIT+SENDIT)
						.TrackProgress('lcSQL3 =' + lcSQL3, LOGIT+SENDIT)
						.TrackProgress('                        TEST MODE                        ', LOGIT+SENDIT)
						.TrackProgress('lcSQL4 =' + lcSQL4, LOGIT+SENDIT)
						.TrackProgress('                        TEST MODE                        ', LOGIT+SENDIT)
						.TrackProgress('lcSQL5 =' + lcSQL5, LOGIT+SENDIT)
						.TrackProgress('                        TEST MODE                        ', LOGIT+SENDIT)
						.TrackProgress('lcSQL6 =' + lcSQL6, LOGIT+SENDIT)
						.TrackProgress('                        TEST MODE                        ', LOGIT+SENDIT)
						.TrackProgress('lcSQL7 =' + lcSQL7, LOGIT+SENDIT)
						.TrackProgress('                        TEST MODE                        ', LOGIT+SENDIT)
						.TrackProgress('lcSQL8 =' + lcSQL8, LOGIT+SENDIT)
						.TrackProgress('                        TEST MODE                        ', LOGIT+SENDIT)
						.TrackProgress('lcSQL85 =' + lcSQL85, LOGIT+SENDIT)
						.TrackProgress('                        TEST MODE                        ', LOGIT+SENDIT)
						.TrackProgress('lcSQL9 =' + lcSQL9, LOGIT+SENDIT)
					ENDIF

					IF USED('JH0') THEN
						USE IN JH0
					ENDIF
					IF USED('JH1') THEN
						USE IN JH1
					ENDIF
					IF USED('JH2') THEN
						USE IN JH2
					ENDIF
					IF USED('JHWEEKS') THEN
						USE IN JHWEEKS
					ENDIF
					IF USED('JHHOURS') THEN
						USE IN JHHOURS
					ENDIF
					IF USED('JHPAY') THEN
						USE IN JHPAY
					ENDIF
					IF USED('JHBONUS') THEN
						USE IN JHBONUS
					ENDIF
					IF USED('JHPAYBYDATEPRE') THEN
						USE IN JHPAYBYDATEPRE
					ENDIF
					IF USED('JHBONUSBYDATEPRE') THEN
						USE IN JHBONUSBYDATEPRE
					ENDIF
					IF USED('JHPAYBYDATE') THEN
						USE IN JHPAYBYDATE
					ENDIF
					IF USED('JHBONUSBYDATE') THEN
						USE IN JHBONUSBYDATE
					ENDIF
					IF USED('JH401K') THEN
						USE IN JH401K
					ENDIF
					IF USED('JH401KNEW') THEN
						USE IN JH401KNEW
					ENDIF
					IF USED('JHMATCH') THEN
						USE IN JHMATCH
					ENDIF

					IF .ExecSQL(lcSQL, 'JH1', RETURN_DATA_MANDATORY) AND ;
						.ExecSQL(lcSQL2, 'JHWEEKS', RETURN_DATA_MANDATORY) AND ;
						.ExecSQL(lcSQL3, 'JHHOURS', RETURN_DATA_MANDATORY) AND ;
						.ExecSQL(lcSQL4, 'JHPAY', RETURN_DATA_MANDATORY) AND ;
						.ExecSQL(lcSQL5, 'JHBONUS', RETURN_DATA_MANDATORY) AND ;
						.ExecSQL(lcSQL6, 'JHPAYBYDATEPRE', RETURN_DATA_MANDATORY) AND ;
						.ExecSQL(lcSQL7, 'JHBONUSBYDATEPRE', RETURN_DATA_MANDATORY) AND ;
						.ExecSQL(lcSQL8, 'JH401K', RETURN_DATA_MANDATORY) AND ;
						.ExecSQL(lcSQL85, 'JH401KNEW', RETURN_DATA_MANDATORY) AND ;
						.ExecSQL(lcSQL9, 'JHMATCH', RETURN_DATA_MANDATORY) THEN
						
*!*				SELECT JH401KNEW
*!*				BROWSE
*!*				
*!*				THROW
					
						****** DELETE JH1 DUPS HERE -- based on ADP_COMP, NAME, SS_NUM
						SELECT JH1
						LOCATE
						lcCompany = ""
						lcName = ""
						lcSS_NUM = ""
						
						SELECT JH1
						SCAN
							IF (JH1.ADP_COMP == lcCompany) AND (JH1.NAME == lcName) AND (JH1.SS_NUM == lcSS_NUM) THEN
								REPLACE JH1.DUPEFLAG WITH 1.0
							ENDIF
							lcCompany = JH1.ADP_COMP
							lcName = JH1.NAME
							lcSS_NUM = JH1.SS_NUM
						ENDSCAN						
						
*!*							SELECT JH1						
*!*							BROWSE
						
						
						* Convert dt fields to dates
						SELECT *, TTOD(PAYDATETM ) AS PAYDATE ;
							FROM JHPAYBYDATEPRE ;
							INTO CURSOR JHPAYBYDATE ;
							READWRITE
						
						SELECT *, TTOD(PAYDATETM ) AS PAYDATE ;
							FROM JHBONUSBYDATEPRE ;
							INTO CURSOR JHBONUSBYDATE ;
							READWRITE
						
						* add fields to be calculated
						SELECT ADP_COMP, STATUS, NAME, SS_NUM, GENDER, ;
							TTOD(BIRTHDATE) AS BIRTHDATE, ;
							TTOD(HIREDATE) AS HIREDATE, ;
							TTOD(TERMDATE) AS TERMDATE, ;							
							000000.00 AS HOURSWKD, ;
							ldBlankDate AS ELIG401KDT, ;
							.F. AS ELIG2013, ;
							000000.00 AS GROSSPAY, ;
							000000.00 AS REGERNING, ;
							000000.00 AS BONUS, ;
							000000.00 AS ELIGGROSS, ;
							000000.00 AS ELIGERNING, ;
							000000.00 AS ELIGBONUS, ;
							000000.00 AS CONT401K, ;
							000000.00 AS CONTROTH, ;
							000000.00 AS CONTMATCH, ;
							000000.00 AS CONTEMPL, ;
							000000.00 AS LOANPYMTS ;
							FROM JH1 ;
							INTO CURSOR JH2 ;
							WHERE DUPEFLAG = 0.0 ;
							ORDER BY NAME ;
							READWRITE
												
						lnEmployeeCount = RECCOUNT('JH2')
						.TrackProgress('lnEmployeeCount = ' + TRANSFORM(lnEmployeeCount), LOGIT+SENDIT)


*!*	SELECT JH2
*!*	BROWSE
*!*							X = 2 * .F.

						
						SELECT JH2 
						SCAN
							* compute 401k eligibility date
							ldEligDate = GOMONTH((JH2.HIREDATE - DAY(JH2.HIREDATE) + 1), 7)
							REPLACE JH2.ELIG401KDT WITH ldEligDate
							* determine if eligibility fell within 2013 later than 1/1/2013
							IF BETWEEN(ldEligDate,{^2013-02-01},{^2013-12-01}) THEN
								REPLACE JH2.ELIG2013 WITH .T.
							ENDIF
							* compute hours worked
							IF INLIST(JH2.ADP_COMP,"E88","E89","ZXU","AXA") THEN
								* COMPUTE SALARIED HOURS
								SELECT JHWEEKS
								LOCATE FOR ADP_COMP = JH2.ADP_COMP AND NAME = JH2.NAME AND SS_NUM = JH2.SS_NUM
								IF FOUND() THEN
									REPLACE JH2.HOURSWKD WITH MIN( JHWEEKS.NUM_WEEKS, 26 ) * 80
								ENDIF
								LOCATE
							ELSE
								* COMPUTE HOURLY HOURS
								SELECT JHHOURS
								LOCATE FOR ADP_COMP = JH2.ADP_COMP AND NAME = JH2.NAME AND SS_NUM = JH2.SS_NUM
								IF FOUND() THEN
									REPLACE JH2.HOURSWKD WITH JHHOURS.REG_HOURS + JHHOURS.OT_HOURS + JHHOURS.OTHER_HRS
								ENDIF
								LOCATE
							ENDIF							
							
							* populate GROSS PAY & REGERNING - no dates
							SELECT JHPAY
							LOCATE FOR ADP_COMP = JH2.ADP_COMP AND NAME = JH2.NAME AND SS_NUM = JH2.SS_NUM
							IF FOUND() THEN
								REPLACE JH2.GROSSPAY WITH JHPAY.GROSSPAY, JH2.REGERNING WITH JHPAY.REGERNING, ;
									JH2.ELIGGROSS WITH JHPAY.GROSSPAY, JH2.ELIGERNING WITH JHPAY.REGERNING 
							ENDIF
							LOCATE
							* populate BONUS - no dates
							SELECT JHBONUS
							LOCATE FOR ADP_COMP = JH2.ADP_COMP AND NAME = JH2.NAME AND SS_NUM = JH2.SS_NUM
							IF FOUND() THEN
								REPLACE JH2.BONUS WITH JHBONUS.BONUS, JH2.ELIGBONUS WITH JHBONUS.BONUS 
							ENDIF
							LOCATE
							
							***NOTE: for salaried col h = grosspay, col i = regerning
							**       for hourly col h = grosspay, col i = grosspay - bonus
							
							* Populate partial-year values for those who became eligible in 2013
							IF JH2.ELIG2013 THEN
								IF USED('CURTEMP') THEN
									USE IN CURTEMP	
								ENDIF
								IF USED('CURTEMP2') THEN
									USE IN CURTEMP2	
								ENDIF
								lcName = JH2.NAME
								lcCompany = JH2.ADP_COMP
								ldEligdate = JH2.ELIG401KDT
								lcSS_NUM = JH2.SS_NUM
								
								WAIT WINDOW NOWAIT lcName
								
								SELECT;
									SUM(GROSSPAY) AS GROSSPAY, ;
									SUM(REGERNING) AS REGERNING ;
									FROM JHPAYBYDATE ; 
									INTO CURSOR CURTEMP ;
									WHERE (NAME = lcName) AND (ADP_COMP = lcCompany) AND (PAYDATE >= ldEligdate) AND (SS_NUM = lcSS_NUM)
									
								REPLACE JH2.ELIGGROSS WITH CURTEMP.GROSSPAY, JH2.ELIGERNING WITH CURTEMP.REGERNING 
								
								SELECT;
									SUM(BONUS) AS BONUS ;
									FROM JHBONUSBYDATE ; 
									INTO CURSOR CURTEMP2 ;
									WHERE (NAME = lcName) AND (ADP_COMP = lcCompany) AND (PAYDATE >= ldEligdate) AND (SS_NUM = lcSS_NUM)
									
								REPLACE JH2.ELIGBONUS WITH CURTEMP2.BONUS
									
							ENDIF  &&  JH2.ELIG2013
							
							IF USED('CURTEMP') THEN
								USE IN CURTEMP	
							ENDIF
							IF USED('CURTEMP2') THEN
								USE IN CURTEMP2	
							ENDIF
							
							* POPULATE 401k cont, roth ira cont, 401k loan
							* 401k
							SELECT JH401KNEW
							LOCATE FOR ADP_COMP = JH2.ADP_COMP AND NAME = JH2.NAME AND SS_NUM = JH2.SS_NUM
							IF FOUND() THEN
								REPLACE JH2.CONT401K WITH JH401KNEW.DEDAMT
							ENDIF
							LOCATE
							
							* roth ira
							SELECT JH401K
							LOCATE FOR ADP_COMP = JH2.ADP_COMP AND NAME = JH2.NAME AND DEDCODE = 'R' AND SS_NUM = JH2.SS_NUM
							IF FOUND() THEN
								REPLACE JH2.CONTROTH WITH JH401K.DEDAMT
							ENDIF
							LOCATE
							* 401k loan
							SELECT JH401K
							LOCATE FOR ADP_COMP = JH2.ADP_COMP AND NAME = JH2.NAME AND DEDCODE = 'N' AND SS_NUM = JH2.SS_NUM
							IF FOUND() THEN
								REPLACE JH2.LOANPYMTS WITH (JH2.LOANPYMTS + JH401K.DEDAMT)
							ENDIF
							LOCATE
							LOCATE FOR ADP_COMP = JH2.ADP_COMP AND NAME = JH2.NAME AND DEDCODE = 'O' AND SS_NUM = JH2.SS_NUM
							IF FOUND() THEN
								REPLACE JH2.LOANPYMTS WITH (JH2.LOANPYMTS + JH401K.DEDAMT)
							ENDIF
							LOCATE
							LOCATE FOR ADP_COMP = JH2.ADP_COMP AND NAME = JH2.NAME AND DEDCODE = 'P' AND SS_NUM = JH2.SS_NUM
							IF FOUND() THEN
								REPLACE JH2.LOANPYMTS WITH (JH2.LOANPYMTS + JH401K.DEDAMT)
							ENDIF
							LOCATE
							
							* Populate match
							SELECT JHMATCH
							LOCATE FOR ADP_COMP = JH2.ADP_COMP AND NAME = JH2.NAME AND SS_NUM = JH2.SS_NUM
							IF FOUND() THEN
								REPLACE JH2.CONTMATCH WITH JHMATCH.MATCH_AMT
							ENDIF
							LOCATE
							
						ENDSCAN
						
						***********************************************
						***********************************************
						***********************************************
						
						**** !!!!  add another SCAN to zero out cols J, K if Elig Date > 12-01-2013
						***********************************************
						***********************************************
						***********************************************
						***********************************************
						
						WAIT CLEAR
						
*!*				SELECT JH2 
*!*				BROWSE

						* see if we had any names with more than 1 row, which means they changed adp company during 2013
						IF USED('CURDUPES') THEN
							USE IN CURDUPES
						ENDIF
						SELECT ;
							NAME, COUNT(*) AS CNT ;
							FROM JH2 ;
							INTO CURSOR CURDUPES ;
							GROUP BY NAME ;
							ORDER BY NAME
						
						lcDupeList = "Note: the followinG employees were in 2 different payroll " + CRLF + ;
						             "companies during the reporting period, and each has 2 rows " + CRLF + ;
						             "in the spreadsheet that need to be totaled:" + CRLF + ;
									 "--------------------------------"
						SELECT CURDUPES
						SCAN FOR CNT > 1
							lcDupeList = lcDupeList + CRLF + CURDUPES.NAME
						ENDSCAN
						
						* save a detail copy of the spreadsheet
						SELECT JH2
						COPY TO "F:\UTIL\JOHNHANCOCK\REPORTS\TGF 2013 Census DETAIL.XLS" XL5

						**************************************************************************************************************
						**************************************************************************************************************
						** setup file names

						lcSpreadsheetTemplate = "F:\UTIL\JOHNHANCOCK\Templates\Summit Management 2013 Census Template.XLS"

						lcFiletoSaveAs = "F:\UTIL\JOHNHANCOCK\REPORTS\TGF 2013 Census.XLS"

						**************************************************************************************************************
						**************************************************************************************************************

						IF FILE(lcFiletoSaveAs) THEN
							DELETE FILE (lcFiletoSaveAs)
						ENDIF


						***********************************************************************************
						***********************************************************************************
						** output to Excel spreadsheet
						.TrackProgress('Creating spreadsheet: ' + lcFiletoSaveAs, LOGIT+SENDIT+NOWAITIT)

						oExcel = CREATEOBJECT("excel.application")
						oExcel.VISIBLE = .F.
						oWorkbook = oExcel.workbooks.OPEN(lcSpreadsheetTemplate)
						oWorkbook.SAVEAS(lcFiletoSaveAs)

						oWorksheet = oWorkbook.Worksheets[1]
						oWorksheet.RANGE("A3","P" + TRANSFORM(lnEmployeeCount + 5)).ClearContents()

						lnRow = 2

						SELECT JH2
												
						SCAN
							lnRow = lnRow + 1
*!*								lnEndRow = lnRow
							lcRow = ALLTRIM(STR(lnRow))
							
							oWorksheet.RANGE("A"+lcRow).VALUE = JH2.NAME
							oWorksheet.RANGE("B"+lcRow).VALUE = "'" + JH2.SS_NUM
							oWorksheet.RANGE("C"+lcRow).VALUE = JH2.GENDER
							oWorksheet.RANGE("D"+lcRow).VALUE = JH2.BIRTHDATE
							oWorksheet.RANGE("E"+lcRow).VALUE = JH2.HIREDATE
							oWorksheet.RANGE("F"+lcRow).VALUE = JH2.TERMDATE
							oWorksheet.RANGE("G"+lcRow).VALUE = HOURSWKD
							IF INLIST(JH2.ADP_COMP,"SXI","E87") THEN
								* HOURLY
								oWorksheet.RANGE("H"+lcRow).VALUE = JH2.GROSSPAY  && what JH calls Gross W-2
								oWorksheet.RANGE("I"+lcRow).VALUE = JH2.GROSSPAY - JH2.BONUS  && what JH calls Base Pay
								oWorksheet.RANGE("J"+lcRow).VALUE = JH2.ELIGGROSS	&& what JH calls Gross Earnings		
								oWorksheet.RANGE("K"+lcRow).VALUE = JH2.ELIGGROSS - JH2.ELIGBONUS	&& what JH calls Base Earnings
							ELSE
								* SALARIED
								oWorksheet.RANGE("H"+lcRow).VALUE = JH2.GROSSPAY  && what JH calls Gross W-2
								oWorksheet.RANGE("I"+lcRow).VALUE = JH2.REGERNING  && what JH calls Base Pay
								oWorksheet.RANGE("J"+lcRow).VALUE = JH2.ELIGGROSS	&& what JH calls Gross Earnings		
								oWorksheet.RANGE("K"+lcRow).VALUE = JH2.ELIGERNING	&& what JH calls Base Earnings
							ENDIF
							oWorksheet.RANGE("L"+lcRow).VALUE = JH2.CONT401K
							oWorksheet.RANGE("M"+lcRow).VALUE = JH2.CONTROTH
							oWorksheet.RANGE("N"+lcRow).VALUE = JH2.CONTMATCH
							oWorksheet.RANGE("O"+lcRow).VALUE = JH2.CONTEMPL
							oWorksheet.RANGE("P"+lcRow).VALUE = JH2.LOANPYMTS
							
						ENDSCAN

						* SAVE AND QUIT EXCEL
						oWorkbook.SAVE()
						oExcel.QUIT()

						***********************************************************************************
						***********************************************************************************

						IF FILE(lcFiletoSaveAs) THEN
							* attach output file to email
							.cAttach = lcFiletoSaveAs
							.cBodyText = lcDupeList + ;
								CRLF + CRLF + "<report log follows>" + ;
								CRLF + .cBodyText
						ELSE
							.TrackProgress('ERROR: unable to email spreadsheet: ' + lcFiletoSaveAs, LOGIT+SENDIT+NOWAITIT)
						ENDIF

						CLOSE DATABASES ALL
					ENDIF  &&  .ExecSQL(lcSQL, 'JH1', RETURN_DATA_MANDATORY)

					.TrackProgress("John Hancock 401kprocess ended normally.", LOGIT+SENDIT+NOWAITIT)
				ELSE
					* connection error
					.TrackProgress('Unable to connect to ADP Payroll System.', LOGIT+SENDIT)
				ENDIF   &&  .nSQLHandle > 0

			CATCH TO loError

				.TrackProgress('There was an error.',LOGIT+SENDIT)
				.TrackProgress('The Error # is: ' + TRANSFORM(loError.ErrorNo), LOGIT+SENDIT)
				.TrackProgress('The Error Message is: ' + TRANSFORM(loError.MESSAGE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred in Program: ' + TRANSFORM(loError.PROCEDURE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred at line #: ' + TRANSFORM(loError.LINENO), LOGIT+SENDIT)
				lnNumberOfErrors = lnNumberOfErrors + 1
				IF TYPE('oWorkbook') = "O" THEN
					oWorkbook.SAVE()
				ENDIF
				IF TYPE('oExcel') = "O" THEN
					oExcel.QUIT()
				ENDIF
				CLOSE DATABASES ALL

			ENDTRY

			CLOSE DATA
			WAIT CLEAR
			***************** INTERNAL email results ******************************
			.TrackProgress('About to send status email.',LOGIT)
			.TrackProgress('The logfile is: ' + .cLogFile, SENDIT)
			.TrackProgress("John Hancock 401kprocess started: " + .cStartTime, LOGIT+SENDIT)
			.TrackProgress("John Hancock 401kprocess finished: " + TTOC(DATETIME()), LOGIT+SENDIT)

			IF .lSendInternalEmailIsOn THEN
				DO FORM dartmail2 WITH .cSendTo,.cFrom,.cSubject,.cCC,.cAttach,.cBodyText,"A"
				.TrackProgress('Sent status email.',LOGIT)
			ELSE
				.TrackProgress('Did not send status email: THIS.lSendInternalEmailIsOn = FALSE.',LOGIT)
			ENDIF

		ENDWITH
		RETURN
	ENDFUNC && main


	FUNCTION ExecSQL
		LPARAMETERS tcSQL, tcCursorName, tlNoDataReturnedIsOkay
		LOCAL llRetval, lnResult
		WITH THIS
			* close target cursor if it's open
			IF USED(tcCursorName)
				USE IN (tcCursorName)
			ENDIF
			WAIT WINDOW "Fetching remote data; please wait..." NOWAIT
			lnResult = sqlexec(.nSQLHandle, tcSQL, tcCursorName)
			llRetval = ( lnResult > 0 )
			IF llRetval THEN
				* see if any data came back
				IF NOT tlNoDataReturnedIsOkay THEN
					IF (NOT USED(tcCursorName)) OR EOF(tcCursorName) THEN
						llRetval = .F.
						.TrackProgress('No data was returned by this query: ' +CRLF + tcSQL, LOGIT+SENDIT)
						.cSendTo = 'Mark Bennett <mbennett@fmiint.com>'
					ENDIF
				ENDIF
			ELSE
				.TrackProgress("Result Code [" + TRANSFORM(lnResult) + "] returned by this query: " + tcSQL, LOGIT+SENDIT)
				.cSendTo = 'Mark Bennett <mbennett@fmiint.com>'
			ENDIF
			WAIT CLEAR
			RETURN llRetval
		ENDWITH
	ENDFUNC


	PROCEDURE TrackProgress
		* do any combination of Wait Window, writing to logfile, and adding to body of email,
		* based on nFlags parameter.
		LPARAMETERS tcExpression, tnFlags
		WITH THIS
			IF BITAND(tnFlags,LOGIT) = LOGIT THEN
				IF .lLoggingIsOn THEN
					?
					? .cProcessName + "  " + TTOC(DATETIME()) + ": " + tcExpression
				ENDIF
			ENDIF
			IF .lWaitWindowIsOn THEN
				IF BITAND(tnFlags,NOWAITIT) = NOWAITIT THEN
					WAIT WINDOW tcExpression NOWAIT
				ENDIF
				IF BITAND(tnFlags,WAITIT) = WAITIT THEN
					WAIT WINDOW tcExpression TIMEOUT .nWaitWindowTimeout
				ENDIF
			ENDIF
			IF BITAND(tnFlags,SENDIT) = SENDIT THEN
				IF .lSendInternalEmailIsOn THEN
					.cBodyText = .cBodyText + tcExpression + CRLF + CRLF
				ENDIF
			ENDIF
		ENDWITH
	ENDPROC  &&  TrackProgress

ENDDEFINE

