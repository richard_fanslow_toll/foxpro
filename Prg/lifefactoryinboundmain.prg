*!*	This project is ONLY used for Lifefactory inbounds

PARAMETERS cOffice,nAcctNum
PUBLIC m.accountid,NormalExit,cUseFolder,tsendto,tsendtoerr,tcc,tccerr,cTransfer,lDoMail,cCustName
PUBLIC lDoSQL,cWhseMod,lBrowFiles

CLOSE DATABASES ALL

TRY
	lTesting = .F.
	lTestinput = .F.
	NormalExit = .F.
	DO m:\dev\prg\_setvars WITH .T.
	lDoMail = .T.
	lBrowFiles = lTesting
*	lBrowFiles = .T.

	nAcctNum = 6034
	cOffice = "N"
	cMod = "I"
	gOffice = cMod
	gMasteroffice = cOffice
	m.accountid = nAcctNum

	IF !lTesting
		SELECT 0
		USE F:\edirouting\FTPSETUP SHARED
		cTransfer = "PL-LIFEFACTORY-NJ"
		REPLACE chkbusy WITH .T.,trig_time WITH DATETIME()  FOR FTPSETUP.TRANSFER = cTransfer
		USE IN FTPSETUP
	ENDIF

	IF lTestinput
		WAIT WINDOW "Running as TEST" TIMEOUT 2
		cUseFolder = "F:\WHP\WHDATA\"
	ELSE
		xReturn = "XXX"
		xReturn = wf(cMod,nAcctNum)
		cUseFolder = UPPER(xReturn)
		WAIT WINDOW "Using folder "+cUseFolder TIMEOUT 2
	ENDIF

	SET STEP ON 
	SELECT 0
	USE F:\3pl\DATA\mailmaster ALIAS mm
	LOCATE FOR mm.accountid = nAcctNum AND mm.office = "N" AND mm.edi_type = "PL"
	IF FOUND()
		STORE TRIM(mm.AcctName) TO lcAccountname
		STORE TRIM(mm.basepath) TO lcPath
		IF lTesting
			WAIT WINDOW "File path is "+lcPath TIMEOUT 2
		ENDIF
		STORE TRIM(mm.archpath) TO lcArchPath
		STORE TRIM(mm.scaption) TO _SCREEN.CAPTION
		IF lTesting
			LOCATE
			LOCATE FOR edi_type = "MISC" AND taskname = "JOETEST"
		ENDIF
			tsendto = IIF(mm.use_alt,sendtoalt,sendto)
			tcc = IIF(mm.use_alt,ccalt,cc)
		LOCATE
		LOCATE FOR edi_type = "MISC" AND taskname = "GENERAL"
		tsendtoerr = IIF(mm.use_alt,sendtoalt,sendto)
		tccterr = IIF(mm.use_alt,ccalt,cc)
		USE IN mm
	ELSE
		WAIT WINDOW AT 10,10  "No parameters set for this acct# "+ALLTRIM(STR(nAcctNum))+"  ---> Office "+lcOffice TIMEOUT 5
		THROW
	ENDIF
	cWhseMod = LOWER("wh"+cMod)

*USE (cUseFolder+"whgenpk") IN 0 ALIAS whgenpk
	useca("inwolog","wh")

	IF !lTestinput
		useca("pl","wh")
	ELSE
		USE (cUseFolder+"pl") IN 0 ALIAS pl
	ENDIF

	xsqlexec("select * from account where inactive=0","account",,"qq")
	INDEX ON accountid TAG accountid
	SET ORDER TO

	cCustName = "LIFEFACTORY"
	cPnp = ""
	lCtnAcct = ""

	SET STEP ON
	SELECT account
	=SEEK(nAcctNum,'account','accountid')
	cPnp = "pnpn"
	IF FOUND()
		cCustName = ALLT(account.AcctName)
		lCtnAcct = !(account.&cPnp)
	ELSE
		cCustName = "NA"
		lCtnAcct = .T.
	ENDIF

	IF cCustName = "NA"
		WAIT WINDOW "Houston, we have a problem..." TIMEOUT 2
		THROW
	ENDIF

	cInfolder = IIF(lTesting,lcPath+"\test\",lcPath)
	cArchfolder = lcArchPath
	CD [&cInfolder]
	len1 = ADIR(ary1,"*.xls")
	WAIT WINDOW "There are "+TRANSFORM(len1)+" PL files to process" TIMEOUT 2
	cfilenamexx = (cInfolder+"temp1.xls")

	IF lTesting
		SET STEP ON
	ENDIF

	FOR ee = 1 TO len1
		xsqlexec("select * from inwolog where .f.","xinwolog",,"wh")

		SELECT xinwolog
		INDEX ON inwologid TAG inwologid

		IF !lTestinput
			xsqlexec("select * from pl where .f.","xpl",,"wh")
		ELSE
			SELECT * FROM pl WHERE .F. INTO CURSOR xpl READWRITE
		ENDIF

		SELECT 0
		CREATE CURSOR intake (f1 c(50),f2 c(30),f3 c(30),f4 c(30),f5 c(30),f6 c(30),f7 c(30),f8 c(30))
		IF FILE(cfilenamexx)
			DELETE FILE [&cfilenamexx]
		ENDIF
		cfilename = ALLTRIM(ary1[ee,1])
		cfullname = (cInfolder+cfilename)
		cArchfilename = (cArchfolder+cfilename)
		COPY FILE [&cfullname] TO [&carchfilename]
		oExcel = CREATEOBJECT("Excel.Application")
		oWorkbook = oExcel.Workbooks.OPEN(cfullname)
		WAIT WINDOW "Now saving file "+cfilename NOWAIT NOCLEAR
		SET SAFETY OFF
		oWorkbook.SAVEAS(cfilenamexx,20) && re-saves Excel sheet as text file
*		oWorkbook.SAVEAS(cfilenamexx,39)  && re-saves Excel sheet as XL95/97 file
		WAIT CLEAR
		WAIT WINDOW "File save complete...continuing" TIMEOUT 1
		oWorkbook.CLOSE(.T.)
		oExcel.QUIT()
		RELEASE oExcel

		SELECT intake
		APPEND FROM [&cfilenamexx] TYPE DELIMITED WITH TAB
		DELETE FILE [&cfilenamexx]
		GO BOTT
		DO WHILE EMPTY(intake.f1)
			SKIP -1
		ENDDO
		nRec1 = RECNO('intake')
		DELETE FOR RECNO()>nRec1
		IF lTesting OR lTestinput
			LOCATE
			BROWSE
		ENDIF
		m.wo_date = DATE()
		m.date_rcvd = DATE()
		m.picknpack = .F.
		m.plid = 0
		m.AcctName = cCustName
		m.addby = "TGF-PROC"
		m.adddt = DATETIME()
		m.addproc = "LFINBD"

		LOCATE
		LOCATE FOR UPPER(intake.f1) = "CONTAINER"
		m.container = ALLTRIM(intake.f2)
		SKIP 1 IN intake
		m.acct_ref = ALLTRIM(intake.f2)
		SKIP 1 IN intake
		m.brokerref = ALLTRIM(intake.f2)
		SKIP 2 IN intake
		m.seal = ALLTRIM(intake.f2)
		SKIP 1 IN intake
		m.comments = ALLTRIM(intake.f2)
		SKIP 1 IN intake
		m.printcomments = ALLTRIM(intake.f2)
		m.office = cOffice
		m.mod = cMod
		INSERT INTO xinwolog FROM MEMVAR

		LOCATE FOR "CARTON"$UPPER(intake.f1)
		IF !FOUND()
			WAIT WINDOW "CARTON cell not found!" TIMEOUT 10
			CLOSE DATA ALL
			THROW
		ENDIF
		REPLACE intake.f5 WITH "gqty",intake.f6 WITH "Totqty",intake.f3 WITH "Color",intake.f8 WITH "Pack" IN intake NEXT 1
		nRec1 = RECNO()
		SCAN FOR RECNO()>nRec1
			IF "-"$intake.f1
				nDigit1 = INT(VAL(LEFT(ALLTRIM(intake.f1),AT("-",intake.f1)-1)))
				nDigit2 = INT(VAL(SUBSTR(ALLTRIM(intake.f1),AT("-",intake.f1)+1)))
				REPLACE intake.f6 WITH TRANSFORM(nDigit2-nDigit1+1)  && totqty
			ELSE
				REPLACE intake.f6 WITH "1"  && totqty
			ENDIF
			REPLACE intake.f8 WITH ALLT(f5) && ALLTRIM(TRANS(INT(VAL(intake.f5))/INT(VAL(f6))))  && Pack
		ENDSCAN

		SELECT f4 AS ID, f2 AS STYLE,f3 AS COLOR,INT(VAL(f6)) AS ctnqty,INT(VAL(f5)) AS unitqty,ALLT(f8) AS cpack ;
			FROM intake ;
			WHERE RECNO()>nRec1 ;
			INTO CURSOR temppl READWRITE

		SELECT temppl
		LOCATE
		IF lTesting
*			BROW
*SET STEP ON
		ENDIF
		m.plid = 0
		npo = 0
		SCAN
			SCATTER MEMVAR
			m.pack = cpack
			m.units = .F.
			m.totqty = m.ctnqty
			REPLACE xinwolog.plinqty WITH xinwolog.plinqty+m.ctnqty IN xinwolog
			REPLACE xinwolog.plunitsinqty WITH IIF(lCtnAcct,0,xinwolog.plinqty) IN xinwolog
			m.style = UPPER(ALLTRIM(m.style))
			m.color = UPPER(ALLTRIM(m.color))
			m.id = UPPER(ALLTRIM(m.id))
			m.echo = "ORIGSTYLE*"+m.style+CHR(13)+"ORIGCOLOR*"+m.color+CHR(13)+"ORIGSIZE*"+m.id
			npo = npo+1
			m.po = TRANSFORM(npo)
			m.plid = m.plid+1
			m.office = cOffice
			m.mod = cMod
			INSERT INTO xpl FROM MEMVAR
			IF !lCtnAcct
				m.totqty  = INT(VAL(cpack))*m.ctnqty
				m.pack = '1'
				m.units = .T.
				m.plid = m.plid+1
				INSERT INTO xpl FROM MEMVAR
			ENDIF
		ENDSCAN

		IF lTesting OR lBrowFiles
			SELECT xinwolog
			LOCATE
			BROWSE
			SELECT xpl
			LOCATE
			BROWSE
			SET STEP ON
		ENDIF

		DO m:\dev\prg\lifefactoryinbound_import
		IF !lTesting
			DELETE FILE [&cfullname]
		ENDIF

	ENDFOR
*	SET STEP ON &&&TMARG
	NormalExit = .T.

CATCH TO oErr
	IF !NormalExit
		SET STEP ON
		lnRec = 0
		ptError = .T.
		tsubject = "Inbound Upload Error ("+TRANSFORM(oErr.ERRORNO)+") for "+cCustName+" at "+TTOC(DATETIME())+"  on file "+cfilename
		tattach  = ""
		tmessage = "Error processing Inbound file, Filename: "+cfilename+CHR(13)
		tmessage = tmessage+CHR(13)+" Error at record "+TRANSFORM(lnRec)+CHR(13)+CHR(13)
		tmessage =tmessage+ "Try/CATCH Exeception Message:"+CHR(13)+;
			[  Error: ] + STR(oErr.ERRORNO) +CHR(13)+;
			[  LineNo: ] + STR(oErr.LINENO) +CHR(13)+;
			[  Message: ] + oErr.MESSAGE +CHR(13)+;
			[  Procedure: ] + oErr.PROCEDURE +CHR(13)+;
			[  Details: ] + oErr.DETAILS +CHR(13)+;
			[  StackLevel: ] + STR(oErr.STACKLEVEL) +CHR(13)+;
			[  LineContents: ] + oErr.LINECONTENTS+CHR(13)+;
			[  UserValue: ] + oErr.USERVALUE+CHR(13)+;
			[  Machine Name: ] + SYS(0)+CHR(13)+;
			[  Error Message :]+CHR(13)

		tsubject = "Inbound WO Error for "+cCustName+" at "+TTOC(DATETIME())+"  on file "+cfilename
		tattach  = ""
		tsendto = tsendtoerr
		tcc = tccerr

		tFrom    ="Toll WMS Inbound EDI System Operations <toll-edi-ops@tollgroup.com>"
		DO FORM m:\dev\frm\dartmail2 WITH tsendto,tFrom,tsubject,tcc,tattach,tmessage,"A"
		lCreateErr = .T.
	ENDIF
FINALLY
	WAIT WINDOW "All "+cCustName+" Inbound Complete...Exiting" TIMEOUT 2
	IF !lTesting
		SELECT 0
		USE F:\edirouting\FTPSETUP SHARED
		REPLACE chkbusy WITH .F. FOR FTPSETUP.TRANSFER = cTransfer
		USE IN FTPSETUP
	ENDIF
	CLOSE DATABASES ALL
ENDTRY
