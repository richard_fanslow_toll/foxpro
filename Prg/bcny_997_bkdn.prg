Parameters lcFilename

Set Deleted On
Set Century On
Select x856
Goto Top
SET EXCLUSIVE OFF 
SET SAFETY OFF

lcHLLevel = ""

Wait Window "Now importing file..........   "+lcFilename Nowait

select x856
scan
	do case
	case inlist(segment,"AK1")
		grpnum=alltrim(x856.f2)

	case inlist(segment,"AK2")
		tnum=alltrim(x856.f2)

	case inlist(segment, "AK5") && This value determines if it was accepted or rejected
		stat=alltrim(x856.f1)

		xsqlexec("update ackdata set processed=1, accepted="+iif(stat='A','1','0')+", " + ;
			"rcvdt={"+ttoc(datetime())+"}, fafilename='"+lcfilename+"' " + ;
			"where inlist(accountid,6221,6521) " + ;
			"and groupnum='"+padr(alltrim(grpnum),10," ")+"' " + ;
			"and transnum='"+padr(alltrim(tnum),10," ")+"'",,,"wh")
	endcase
endscan

lProcessOK = .T.
