CLOSE DATA ALL
SET EXCLUSIVE OFF
SET SAFETY OFF
SET DELETED ON

doemail=.T.

****** IF DOING THIS MANUALLY, uncomment the next block and comment out the line ******
****** marked "comment this" below
**	xReturn = "X"
**	DO m:\dev\prg\wf_alt WITH "C",4677
**	lcWHPath = UPPER(xReturn)

lcWHPath = wf("C",4677) && Comment this for manual run

xsqlexec("select * from account where inactive=0","account",,"qq")
INDEX ON accountid TAG accountid
SET ORDER TO

IF !USED('edi_trigger')
	cEDIFolder = IIF(DATE()<{^2010-01-24},"F:\JAG3PL\DATA\","F:\3PL\DATA\")
	USE (cEDIFolder+"edi_trigger") IN 0 ALIAS edi_trigger
ENDIF
SELECT edi_trigger
SET ORDER TO wo_num
*Set Step on

SET EngineBehavior 70
SET CENTURY OFF
lcRptTitle ="TGF Inc. - Daily Appt. Status Report (EDI Cust Only)"

xsqlexec("select * from outship where accountid in (4677,4694,4551,4752,4610,4650,1)","rptsource",,"wh")
SELECT wo_num,COUNT(ship_ref) AS ship_ref,accountid,acctname(accountid)AS acctname,consignee,ship_via,appt,SUM(qty) AS unitqty, SUM(ctnqty) AS ctnqty,CANCEL,;
	del_date,bol_no AS bol,scac, {  /  /  } AS editrigdate,"          " AS trigwho, "                             " AS edi_stat FROM ;
	rptsource WHERE BETWEEN(appt,DATE()-12,DATE()+1) AND INLIST(accountid,4677,4694,4551,4752,4610,4650,1) INTO CURSOR;
	rpt READWRITE  GROUP BY accountid,wo_num
USE IN rptsource

SELECT rpt
LOCATE
SCAN
	SELECT edi_trigger
	SEEK rpt.wo_num
	IF FOUND()
		REPLACE rpt.editrigdate WITH trig_time
		REPLACE rpt.edi_stat WITH edi_trigger.fin_status
		REPLACE rpt.trigwho WITH edi_trigger.trig_by
	ENDIF
ENDSCAN

SELECT rpt
GOTO TOP

SCAN
	DELETE FOR edi_stat="945 CREATED"
ENDSCAN
report_date= DATE()


IF doemail = .T.
	lcReportname = "f:\3pl\reports\delrpt"
	lcFilename = "h:\fox\ApptStatusReport"
	STORE  .F. TO REPORTOK

	DO REPORTTOPDF WITH lcReportname,lcFilename,REPORTOK
	IF  REPORTOK = .F.
		=MESSAGEBOX("PDF file not created",48,"PDF Creator")
	ENDIF

	SELECT 0
	USE F:\3pl\DATA\mailmaster ALIAS mm SHARED
	LOCATE FOR edi_type = "MISC" AND taskname = "GENERAL"
	tsendto = IIF(mm.use_alt,mm.sendtoalt,mm.sendto)
	tcc = IIF(mm.use_alt,mm.ccalt,mm.cc)
	USE IN mm

	tattach = lcFilename
	tFrom ="TGF EDI System Operations <fmi-transload-ops@fmiint.com>"
	tmessage = "See attached report file on recent appointment and EDI/945 activity"
	tFrom ="TGF 3PL/EDI Operations Center <fmi-transload-ops@fmiint.com>"
	tSubject = "Daily Appt/EDI StatusReport"
	DO FORM dartmail2 WITH tsendto,tFrom,tSubject," ",tattach,tmessage,"A"
ELSE
	REPORT FORM delrpt PREVIEW
ENDIF



****************************************************************************************************************************************88
PROCEDURE acctname

	LPARAMETER xaccountid

	IF SEEK(xaccountid,"account","accountid")
		RETURN TRIM(account.acctname)
	ELSE
		RETURN ""
	ENDIF
