**This script identifies inventory in CART locations
utilsetup("MJ_RETURNS_CART_INVENTORY")
Set Exclusive Off
Set Deleted On
Set Talk Off
Set Safety Off 

xsqlexec("select * from invenloc where mod='J'",,,"wh")

SELECT invenloc

SELECT ACCOUNTID, STYLE, COLOR, ID, LOCQTY, WHSELOC, DTOC(ADDDT) as date FROM INVENLOC WHERE whseloc='CART' AND LOCQTY!=0;
AND adddt <=(date()-2)  INTO CURSOR cart
SELECT caRT

If Reccount() > 0
  Export To "S:\MarcJacobsData\Inventory\CART_inventory"  Type Xls

  tsendto = "tmarg@fmiint.com,yenny.garcia@tollgroup.com"
  tattach = "S:\MarcJacobsData\inventory\CART_inventory.xls"
  tcc =""
  tFrom ="Toll EDI System Operations <fmi-transload-ops@fmiint.com>"
  tmessage = "INVENTORY EXISTS IN CART LOCATIONS  "+Ttoc(Datetime())
  tSubject = "INVENTORY exists in carts older than 2 days"
  Do Form m:\dev\frm\dartmail2 With tsendto,tFrom,tSubject, tcc,tattach,tmessage  ,"A"
ELSE 
  tsendto = "tmarg@fmiint.com,yenny.garcia@tollgroup.com"
  tattach = ""
  tcc =""
  tFrom ="Toll EDI System Operations <fmi-transload-ops@fmiint.com>"
  tmessage = "NO_inventory exists in carts older than 2 days "+Ttoc(Datetime())
  tSubject = "NO_inventory exists in carts older than 2 days"
  Do Form m:\dev\frm\dartmail2 With tsendto,tFrom,tSubject, tcc,tattach,tmessage  ,"A"
ENDIF


Close Data All
schedupdate()
_Screen.Caption=gscreencaption