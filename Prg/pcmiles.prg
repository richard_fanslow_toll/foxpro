pcminit()

dimension astates[1,1]
xnumstates = statemil(@astates, puloc, delloc)

if xnumstates>0
	m.traveled=0

	for i=1 to xnumstates
		if !inlist(astates[i,1],"US")
			m.traveled = m.traveled + astates[i,2]
		endif
		do case
		case emptynul(astates[i,1]) or inlist(astates[i,1],"US")
		case astates[i,1]="AK"
			m.akmiles = astates[i,2]
		case astates[i,1]="AL"
			m.almiles = astates[i,2]
		case astates[i,1]="AR"
			m.armiles = astates[i,2]
		case astates[i,1]="AZ"
			m.azmiles = astates[i,2]
		case astates[i,1]="BC"
			m.bcmiles = astates[i,2]
		case astates[i,1]="BJ"
			m.bjmiles = astates[i,2]
		case astates[i,1]="CA"
			m.camiles = astates[i,2]
		case astates[i,1]="CO"
			m.comiles = astates[i,2]
		case astates[i,1]="CT"
			m.ctmiles = astates[i,2]
		case astates[i,1]="DC"
			m.dcmiles = astates[i,2]
		case astates[i,1]="DE"
			m.demiles = astates[i,2]
		case astates[i,1]="FL"
			m.flmiles = astates[i,2]
		case astates[i,1]="GA"
			m.gamiles = astates[i,2]
		case astates[i,1]="IA"
			m.iamiles = astates[i,2]
		case astates[i,1]="ID"
			m.idmiles = astates[i,2]
		case astates[i,1]="IL"
			m.ilmiles = astates[i,2]
		case astates[i,1]="IN"
			m.inmiles = astates[i,2]
		case astates[i,1]="KS"
			m.ksmiles = astates[i,2]
		case astates[i,1]="KY"
			m.kymiles = astates[i,2]
		case astates[i,1]="LA"
			m.lamiles = astates[i,2]
		case astates[i,1]="MA"
			m.mamiles = astates[i,2]
		case astates[i,1]="MD"
			m.mdmiles = astates[i,2]
		case astates[i,1]="ME"
			m.memiles = astates[i,2]
		case astates[i,1]="MI"
			m.mimiles = astates[i,2]
		case astates[i,1]="MN"
			m.mnmiles = astates[i,2]
		case astates[i,1]="MO"
			m.momiles = astates[i,2]
		case astates[i,1]="MS"
			m.msmiles = astates[i,2]
		case astates[i,1]="MT"
			m.mtmiles = astates[i,2]
		case astates[i,1]="MX"
			m.mxmiles = astates[i,2]
		case astates[i,1]="NC"
			m.ncmiles = astates[i,2]
		case astates[i,1]="ND"
			m.ndmiles = astates[i,2]
		case astates[i,1]="NE"
			m.nemiles = astates[i,2]
		case astates[i,1]="NH"
			m.nhmiles = astates[i,2]
		case astates[i,1]="NJ"
			m.njmiles = astates[i,2]
		case astates[i,1]="NM"
			m.nmmiles = astates[i,2]
		case astates[i,1]="NV"
			m.nvmiles = astates[i,2]
		case astates[i,1]="NY"
			m.nymiles = astates[i,2]
		case astates[i,1]="OH"
			m.ohmiles = astates[i,2]
		case astates[i,1]="OK"
			m.okmiles = astates[i,2]
		case astates[i,1]="ON"
			m.onmiles = astates[i,2]
		case astates[i,1]="OR"
			m.ormiles = astates[i,2]
		case astates[i,1]="PA"
			m.pamiles = astates[i,2]
		case astates[i,1]="PQ"
			m.pqmiles = astates[i,2]
		case astates[i,1]="QC"
			m.qcmiles = astates[i,2]
		case astates[i,1]="RI"
			m.rimiles = astates[i,2]
		case astates[i,1]="SC"
			m.scmiles = astates[i,2]
		case astates[i,1]="SD"
			m.sdmiles = astates[i,2]
		case astates[i,1]="TN"
			m.tnmiles = astates[i,2]
		case astates[i,1]="TX"
			m.txmiles = astates[i,2]
		case astates[i,1]="UT"
			m.utmiles = astates[i,2]
		case astates[i,1]="VA"
			m.vamiles = astates[i,2]
		case astates[i,1]="VT"
			m.vtmiles = astates[i,2]
		case astates[i,1]="WA"
			m.wamiles = astates[i,2]
		case astates[i,1]="WI"
			m.wimiles = astates[i,2]
		case astates[i,1]="WV"
			m.wvmiles = astates[i,2]
		case astates[i,1]="WY"
			m.wymiles = astates[i,2]
		case astates[i,1]="YT"
			m.ytmiles = astates[i,2]
		endcase
	next
	
	m.miles2dest=m.traveled
endif

pcmclose()