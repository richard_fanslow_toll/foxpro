*!* bbc850ret_CREATE.PRG  Created 08.03.2017 to use 850s in place of 943s for returns
PARAMETERS nAcctNum

PUBLIC lXfer943,cFilename,tsendto,tcc,tsendtoerr,tccerr,tsendtotest,tcctest,lIsError,nOrigAcctNum,nDivLoops,cInFolder,cWhse,nRecLU,lTesting
PUBLIC lDoMail,tfrom,tattach,cxErrMsg,NormalExit,Acctname,lPick,lBrowFiles,cTransfer,cArchivePath,lcPath,cContainer,cAcct_ref,cWhseMod
PUBLIC lDoSQL,gMasterOffice,cOffice,cMod
PUBLIC ARRAY a856(1)

CLOSE DATABASES ALL

nAcctNum = IIF(VARTYPE(nAcctNum)="L",6757,nAcctNum)  && bbc rw

lTesting = .T. && If TRUE, disables certain functions
lOverrideBusy = lTesting
lOverrideBusy = .T.

DO m:\dev\prg\_setvars WITH lTesting
ON ERROR THROW

*SET STEP ON

IF lTesting
	WAIT WINDOW "This is a "+IIF(lTesting,"TEST","PRODUCTION")+" upload" TIMEOUT 2
ENDIF

TRY
	DO m:\dev\prg\lookups
	tfrom = "Toll/FMI EDI Operations <toll-edi-ops@tollgroup.com>"
	tattach = " "
	cOffice = IIF(lTesting,"P","L")
	cMod = cOffice
	gOffice = cMod
	gMasterOffice = cOffice
	cWhseMod = LOWER("wh"+cMod)

	lDoSQL = .T.

	lTestmail = lTesting
	lBrowFiles = lTesting
	lIsError = .F.
	lDoMail = .T.  && If .t., sends mail
	NormalExit = .F.
	lPick = .F.
	nDivLoops = 0
	nRecLU = 0
	cMasterComments = ""
	cContainer = ""
	cReference = ""

	nAcctNum = IIF(EMPTY(nAcctNum),6757,nAcctNum)  && bbc rw
	cxErrMsg = ""
	cCustName = "BBC RW"  && Customer Identifier (for folders, etc.)
	cMailName = "BBC RW"  && Proper name for eMail, etc.

	cTransfer ="850-BBC"

	IF lTesting
		SELECT 0
		USE F:\edirouting\ftpsetup
		LOCATE FOR ftpsetup.transfer = cTransfer
		IF ftpsetup.chkbusy AND !lTesting AND !lOverrideBusy
			WAIT WINDOW "Process is busy...exiting" TIMEOUT 2
			CLOSE DATA ALL
			NormalExit = .T.
			THROW
		ENDIF
		REPLACE chkbusy WITH .T.,trig_time WITH DATETIME()  FOR transfer = cTransfer
		USE IN ftpsetup
	ENDIF

	setuprcvmail(cOffice)

	IF !lTesting
		xpath = "f:\ftpusers\bbcrw\850in\"
		cArchivePath = "f:\ftpusers\bbcrw\850in\archive\"
	ELSE
		xpath = "f:\ftpusers\bbcrw\850intest\"
		cArchivePath = "f:\ftpusers\bbcrw\850intest\archive\"
	ENDIF

	len1 = ADIR(ary1,xpath+"*.txt")
	IF len1 = 0
		WAIT WINDOW "There are no 850 files to process...exiting" TIMEOUT 2
		CLOSE DATABASES ALL
		NormalExit = .T.
		THROW
	ENDIF
	WAIT WINDOW "There are "+TRANSFORM(len1)+" 850 files to process" NOWAIT &&TIMEOUT 2


	FOR kk = 1 TO len1
		cContainer = ""
		cAcct_ref = ""
		cFilename = ALLTRIM(ary1[kk,1])
		cArchiveFile  = (cArchivePath+cFilename)
		dFiledate = ary1[kk,3]
		xfile = xpath+cFilename

		DO m:\dev\prg\createx856a

		loadedifile(xfile,"*","PIPE3","BBC")

		SELECT x856
		LOCATE
		LOCATE FOR x856.segment = "GS" AND x856.f1 = "PO"
		IF !FOUND()
			SET STEP ON
			cxErrMsg = cFilename+" is NOT an 850 document"
			errormail()
		ENDIF

		SELECT x856
		LOCATE

		IF lTesting
			cUseFolder = "F:\WHP\WHDATA\"
		ELSE
			xReturn = "XXX"
			DO m:\dev\prg\wf_alt WITH cOffice,nAcctNum
			IF xReturn = "XXX"
				SET STEP ON
				cxErrMsg = "NO WHSE FOUND IN WMS"
				errormail()
				RETURN
			ENDIF
			cUseFolder = UPPER(xReturn)
		ENDIF

		useca("inwolog","wh")
		xsqlexec("select * from inwolog where .f.","xinwolog",,"wh")

		useca("pl","wh")
		xsqlexec("select * from pl where .f.","xpl",,"wh")

		useca("inrcv","wh")

		xsqlexec("select * from account where inactive=0","account",,"qq")
		INDEX ON accountid TAG accountid
		SET ORDER TO

		SELECT xinwolog
		SCATTER MEMVAR BLANK
		=SEEK(nAcctNum,'account','accountid')
		m.Acctname = account.Acctname
		m.accountid = account.accountid
		SELECT pl
		STORE "FILE850*"+cFilename TO m.comments
		STORE m.comments+CHR(13)+"FILETIME*"+TTOC(DATETIME()) TO m.comments
		STORE 0 TO m.inwologid,m.plid,npo
		STORE "" TO m.echo,m.po
		STORE "FMI-PROC" TO m.addby
		STORE DATETIME() TO m.adddt
		STORE "BBC850RET" TO m.addproc
		STORE DATE() TO m.wo_date,m.date_rcvd

		m.inwologid =0
		m.plid = 0
		m.po ="0"
		m.container =""

		SELECT x856
		DO WHILE !EOF("x856")

			DO CASE
				SELECT xinwolog
				STORE "BBC850" TO m.addby
				STORE DATETIME() TO m.adddt
				STORE "BBC850RET" TO m.addproc

			CASE x856.segment = "BEG"
				m.comments = "PONUM*"+ALLT(x856.f3)

			CASE x856.segment = "REF"
				DO CASE
				CASE ALLT(x856.f1) = "CO"  && Cust Order number
					m.acct_ref = ALLT(x856.f2)

				CASE ALLT(x856.f1) = "IT"  && Cust Order number
					m.comments = m.comments+CHR(13)+"CUSTNAME*"+ALLT(x856.f3)

				CASE ALLT(x856.f1) = "MR"  && Merch type
					ALLT(x856.f2)

				CASE ALLT(x856.f1) = "OR"  && Order number
					ALLT(x856.f2)
				ENDCASE

			CASE x856.segment = "N1" AND ALLT(x856.f1) = "SF" && returned from
				m.container = "RET"+m.acct_ref
				m.office = cOffice
				m.mod = cMod
				m.inwologid = m.inwologid+1
				m.wo_num = m.wo_num+1
				INSERT INTO xinwolog FROM MEMVAR

			CASE x856.segment = "PO1" && item detail
				m.totqty = INT(VAL(x856.f2))
				ntotqty = m.totqty
				REPLACE xinwolog.plinqty WITH xinwolog.plinqty+m.totqty IN xinwolog
				m.echo = "UOM*"+ALLTRIM(x856.f3)
				m.style = ALLTRIM(x856.f7)
				m.color = ALLTRIM(x856.f9)
				m.id = ALLTRIM(x856.f11)
				m.echo = m.echo+CHR(13)+"UPC*"+ALLTRIM(x856.f13)

			CASE x856.segment = "PID" && prod. info
				m.echo = m.echo+CHR(13)+"COLORDESC*"+ALLTRIM(x856.f5)

			CASE x856.segment = "PO4" && pack info
				nPack = INT(VAL(x856.f1))
				m.pack = ALLTRIM(STR(nPack))

				m.units = .F.
				npo = npo+1
				m.plid = m.plid + 1
				m.po = ALLTRIM(STR(npo))
				m.office = cOffice
				m.mod = cMod
				INSERT INTO xpl FROM MEMVAR
				IF xpl.inwologid = 0
					REPLACE xpl.inwologid WITH  1 IN xpl
				ENDIF

				m.units = .T.
				m.pack="1"
				m.totqty = (ntotqty*nPack)
				m.plid = m.plid + 1
				INSERT INTO xpl FROM MEMVAR
				IF xpl.inwologid = 0
					REPLACE xpl.inwologid WITH 1 IN xpl
				ENDIF

			ENDCASE
			SKIP 1 IN x856
		ENDDO


		IF lTesting
			SELECT xpl
		ENDIF

*		xplrollup()

		IF lTesting
			SELECT xinwolog
			DELETE FOR plinqty = 0
			LOCATE
			BROWSE
			SELECT xpl
			DELETE FOR totqty = 0
			LOCATE
			BROWSE
			CANCEL
		ENDIF

		SELECT xinwolog
		LOCATE
		SCAN
			xAcct_Num =  xinwolog.accountid
			xAcct_ref = xinwolog.acct_ref
			xContainer = xinwolog.CONTAINER
			WAIT WINDOW "AT XINWOLOG RECORD "+ALLTRIM(STR(RECNO())) NOWAIT NOCLEAR
			csq1 = [select * from inwolog where accountid = ]+ALLTRIM(STR(xAcct_Num))+[ and container = ']+xContainer+[' and acct_ref = ']+xAcct_ref+[']
			xsqlexec(csq1,"i1",,"wh")
			IF RECCOUNT("i1")>0
				cWO = TRANSFORM(i1.wo_num)
				dupemail(cWO)
				SET STEP ON
				ninwologid = xinwolog.inwologid
				DELETE FOR xinwolog.inwologid = ninwologid
				SELECT xpl
				DELETE FOR xpl.inwologid = ninwologid
				SELECT xinwolog
				WAIT WINDOW "This set of values already exists in the INWOLOG table...looping" TIMEOUT 2
				LOOP
			ENDIF
		ENDSCAN
		nUploadCount = 0


		SET STEP ON
		SELECT xinwolog
		LOCATE
		WAIT WINDOW "Currently in "+UPPER(IIF(lTesting,"test","production"))+" mode, base folder: "+cUseFolder TIMEOUT 2
		SET STEP ON
		SCAN  FOR !DELETED() && Scanning xinwolog here
			SCATTER MEMVAR MEMO
			cPLCtns = ALLTRIM(STR(m.plinqty))
			cPLUnits = ALLTRIM(STR(m.plunitsinqty))
			nWO_num  = dygenpk("wonum",cWhseMod)

			cWO_Num = ALLTRIM(STR(nWO_num))
			ninwologid = dygenpk("inwolog",cWhseMod)

			m.wo_num = nWO_num
			SET ORDER TO
			insertinto("inwolog","wh",.T.)
			nUploadCount = nUploadCount+1

			SELECT xpl
			SCAN FOR inwologid=xinwolog.inwologid
				SCATTER FIELDS EXCEPT inwologid MEMVAR MEMO
				m.wo_num = nWO_num
				m.inwologid = inwolog.inwologid
				insertinto("pl","wh",.T.)
			ENDSCAN
*    goodmail()
		ENDSCAN

		IF nUploadCount = 0
			SET STEP ON
			IF FILE(cFilename)
				COPY FILE [&cFilename] TO [&cArchivefile]
				IF FILE(cArchiveFile) AND !lTesting
					DELETE FILE [&cFilename]
				ENDIF
			ENDIF
			EXIT
		ENDIF

*    SELECT whgenpk
*!*    cUploadCount = Alltrim(Str(nUploadCount))
*!*    Wait Window "Intradeco 943 file "+cFilename+" processed."+Chr(13)+"Header records loaded: "+cUploadCount Timeout 1
		IF FILE(cFilename)
			COPY FILE [&cFilename] TO [&cArchivefile]
			IF FILE(cArchiveFile) AND !lTesting
				DELETE FILE [&cFilename]
			ENDIF
		ENDIF

		cWO_Num = TRANSFORM(nWO_num)
		SELECT xinwolog
		cPLCtns = TRANSFORM(xinwolog.plinqty)
		cPLUnits = TRANSFORM(xinwolog.plunitsinqty)

		SELECT inwolog

		IF !EMPTY(inwolog.CONTAINER)
			SELECT pl
			COUNT TO xnumskus FOR !units AND wo_num=inwolog.wo_num

			xsqlexec("select * from inrcv where container='"+inwolog.CONTAINER+"' and status#'CONFIRMED'","xinrcv",,"wh")
			SELECT xinrcv
			IF RECCOUNT() > 0
				xsqlexec("update inrcv set inwonum="+TRANSFORM(inwolog.wo_num)+" where container='"+inwolog.CONTAINER+"' and status#'CONFIRMED'",,,"wh")
			ELSE
				SELECT inrcv
				SCATTER MEMVAR MEMO BLANK
				m.mod = cMod
				m.office = cOffice
				m.inrcvid=dygenpk("inrcv","whall")  && new get the ID value
				m.inwonum=inwolog.wo_num
				m.accountid=inwolog.accountid
				m.Acctname=inwolog.Acctname
				m.container=inwolog.CONTAINER
				m.status="NOT AVAILABLE"
				m.skus=xnumskus
				m.cartons=inwolog.plinqty
				m.newdt=DATE()
				m.addfrom="IN"
				m.confirmdt=empty2nul(m.confirmdt)
				m.newdt=empty2nul(m.newdt)
				m.dydt=empty2nul(m.dydt)
				m.firstavaildt=empty2nul(m.firstavaildt)
				m.apptdt=empty2nul(m.apptdt)
				m.pickedup=empty2nul(m.pickedup)

				INSERT INTO inrcv FROM MEMVAR
*      tu("inrcv")
			ENDIF

		ENDIF

	ENDFOR

	SET STEP ON

	tu("inwolog")
	tu("pl")

	WAIT WINDOW "All Intradeco 943s processed...exiting" TIMEOUT 2

	closeftpsetup()

	NormalExit = .T.

CATCH TO oErr
	IF NormalExit = .F.
		ASSERT .F. MESSAGE "AT CATCH SECTION"
		SET STEP ON
		tsubject = cCustName+" 943 Upload Error ("+TRANSFORM(oErr.ERRORNO)+") at "+TTOC(DATETIME())
		tattach  = ""
		tsendto  = tsendtoerr
		tcc      = tccerr
		tmessage = cCustName+" 943 Upload Error"
		lcSourceMachine = SYS(0)
		lcSourceProgram = SYS(16)

		IF EMPTY(cxErrMsg)
			tmessage =tmessage+ "Try/CATCH Exeception Message:"+CHR(13)+;
				[  Error: ] + STR(oErr.ERRORNO) +CHR(13)+;
				[  Procedure: ] + oErr.PROCEDURE +CHR(13)+;
				[  LineNo: ] + STR(oErr.LINENO) +CHR(13)+;
				[  Message: ] + oErr.MESSAGE +CHR(13)+;
				[  Details: ] + oErr.DETAILS +CHR(13)+;
				[  StackLevel: ] + STR(oErr.STACKLEVEL) +CHR(13)+;
				[  LineContents: ] + oErr.LINECONTENTS+CHR(13)+;
				[  UserValue: ] + oErr.USERVALUE+CHR(13)+;
				[  Computer:  ] +lcSourceMachine+CHR(13)+;
				[  943 file:  ] +cFilename+CHR(13)+;
				[  Program:   ] +lcSourceProgram
		ELSE
			tmessage = tmessage + CHR(13) + CHR(13) + cxErrMsg
		ENDIF
		tattach  = ""
		tfrom    ="FMI EDI Processing Center <fmi-transload-ops@fmiint.com>"
		DO FORM m:\dev\FRM\dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
	ENDIF
FINALLY
	SET STATUS BAR ON
	DELETE FILE (xpath+'*.*')
	CLOSE DATABASES ALL
	ON ERROR
ENDTRY


****************************
PROCEDURE goodmail
****************************
tsubject = cMailName+" EDI FILE (943) Processed, Inv. WO"+cWO_Num
tmessage = REPLICATE("-",90)+CHR(13)
tmessage = tmessage+"Packinglist upload run from machine: "+SYS(0)+CHR(13)
tmessage = tmessage+REPLICATE("-",90)+CHR(13)
tmessage = tmessage+"Inbound Workorders created for "+ALLTRIM(m.Acctname)+", Loc: "+cWhse+CHR(13)
tmessage = tmessage+IIF(!EMPTY(cContainer),"Container: "+cContainer+CHR(13),"AWB :"+cReference)+CHR(13)
tmessage = tmessage+"Data from File : "+cFilename+CHR(13)
tmessage = tmessage+REPLICATE("-",90)+CHR(13)
tmessage = tmessage+"Acct. Ref: "+cAcct_ref+", WO# "+cWO_Num+SPACE(2)+"CTNS: "+cPLCtns+SPACE(2)+"UNITS: "+cPLUnits

IF lDoMail
	DO FORM m:\dev\frm\dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
ENDIF
ENDPROC

****************************
PROCEDURE dupemail
****************************
PARAMETERS cWO
tsubject = cMailName+" EDI FILE (943): Duplicate"
tmessage = REPLICATE("-",90)+CHR(13)
tmessage = tmessage+"Data from File : "+cFilename+CHR(13)
tmessage = tmessage+"For Account ID: "+TRANSFORM(nAcctNum)
tmessage = tmessage+ ", Container "+cContainer
tmessage = tmessage+ ", Acct. Reference "+cAcct_ref+CHR(13)
tmessage = tmessage+"already exists in the Inbound WO table, in Work Order "+cWO
tmessage = tmessage+CHR(13)+REPLICATE("-",90)+CHR(13)

IF lTesting
	SET STEP ON
ENDIF

tsendto="pgaidis@fmiint.com,mwinter@fmiint.com"

IF lDoMail
	DO FORM m:\dev\frm\dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
ENDIF
ENDPROC

****************************
PROCEDURE errormail
****************************
SET STEP ON
tsubject = cMailName+" EDI ERROR (943)"
tsendto = tsendtoerr
tcc = tccerr
tmessage = "File: "+cFilename
tmessage = tmessage+CHR(13)+cxErrMsg

DO FORM m:\dev\frm\dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
NormalExit = .T.
SET STEP ON
THROW
ENDPROC

****************************
PROCEDURE setuprcvmail
****************************
PARAMETERS cOffice
*  ASSERT .f. MESSAGE "In Mail/Folder selection...debug"
SELECT 0
USE F:\3pl\DATA\mailmaster ALIAS mm
LOCATE FOR mm.edi_type = "943" AND accountid = nAcctNum
IF !FOUND()
	SET STEP ON
	cxErrMsg = "Office/Loc/Acct not in Mailmaster!"
	errormail()
ENDIF
STORE TRIM(mm.basepath) TO lcPath
STORE TRIM(mm.archpath) TO cArchivePath
lUseAlt = mm.use_alt
STORE IIF(lUseAlt,TRIM(mm.sendtoalt),TRIM(mm.sendto)) TO tsendto
STORE IIF(lUseAlt,TRIM(mm.ccalt),TRIM(mm.cc)) TO tcc
LOCATE
LOCATE FOR mm.edi_type = "MISC" AND mm.taskname = "JOETEST"
lUseAlt = mm.use_alt
STORE IIF(lUseAlt,TRIM(mm.sendtoalt),TRIM(mm.sendto)) TO tsendtotest
STORE IIF(lUseAlt,TRIM(mm.ccalt),TRIM(mm.cc)) TO tcctest
LOCATE
LOCATE FOR mm.edi_type = "MISC" AND mm.taskname = "GENERAL"
lUseAlt = mm.use_alt
STORE IIF(lUseAlt,TRIM(mm.sendtoalt),TRIM(mm.sendto)) TO tsendtoerr
STORE IIF(lUseAlt,TRIM(mm.ccalt),TRIM(mm.cc)) TO tccerr
IF lTesting OR lTestmail
	tsendto = tsendtotest
	tcc = tcctest
ENDIF
USE IN mm
ENDPROC

***************************************
PROCEDURE closeftpsetup
***************************************
SELECT 0
USE F:\edirouting\ftpsetup
REPLACE chkbusy WITH .F. FOR transfer = cTransfer
USE IN ftpsetup
ENDPROC

***************************************
PROCEDURE assignofficelocs
***************************************
cWhseLoc = ICASE(cOffice = "M","FL",cOffice = "I","NJ",cOffice = "C","CA","ML")
cWhse = ICASE(cOffice = "M","MIAMI",cOffice = "I","CARTERET",cOffice = "C","SAN PEDRO","MIRA LOMA")
ENDPROC

***************************************
PROCEDURE xplrollup  && Added 02.20.2014, Joe, per Chris M.
***************************************
SELECT xpl
*Select inwologid,Style,Count(Style) As cnt1 From xpl Group By 1,2 Into Cursor tempxpl

SELECT inwologid,STYLE,COLOR,ID,PACK, COUNT(1) AS cnt1 FROM xpl WHERE !units GROUP BY inwologid,STYLE,COLOR,ID,PACK INTO CURSOR tempxpl
SELECT tempxpl
LOCATE
IF lTesting
	BROWSE FOR cnt1>1
ENDIF
LOCATE

SET STEP ON

SCAN FOR tempxpl.cnt1>1
	cStyle1 = ALLTRIM(tempxpl.STYLE)
	SELECT xpl
	LOCATE FOR xpl.inwologid = tempxpl.inwologid AND xpl.STYLE = tempxpl.STYLE AND xpl.COLOR = tempxpl.COLOR AND xpl.ID = tempxpl.ID AND xpl.PACK = tempxpl.PACK
	nrec1 = RECNO()
	REPLACE xpl.ECHO WITH xpl.ECHO+CHR(13)+"QTY*"+ALLT(STR(xpl.totqty)) IN xpl

	SCAN FOR !DELETED() AND  xpl.STYLE = tempxpl.STYLE AND xpl.COLOR = tempxpl.COLOR AND xpl.ID = tempxpl.ID AND xpl.PACK = tempxpl.PACK AND RECNO()>nrec1
		nrec2 = RECNO()
		SCATTER FIELDS ECHO MEMO MEMVAR
		ntotqty = xpl.totqty
		GO nrec1
		IF !"MULTILINES"$xpl.ECHO
			REPLACE xpl.ECHO WITH "MULTILINES"+CHR(13)+xpl.ECHO IN xpl
		ENDIF
		REPLACE xpl.ECHO WITH xpl.ECHO+CHR(13)+m.echo IN xpl
		REPLACE xpl.ECHO WITH xpl.ECHO+CHR(13)+"QTY*"+ALLT(STR(ntotqty)) IN xpl
		REPLACE xpl.totqty WITH xpl.totqty+ntotqty IN xpl
		lnPack = VAL(xpl.PACK )
		lnStyleTotal = xpl.totqty
		SKIP 1 IN xpl
		REPLACE xpl.totqty WITH lnStyleTotal*lnPack  IN xpl
		GO nrec2
		DELETE NEXT 2 IN xpl
	ENDSCAN

	STORE "" TO m.echo
ENDSCAN
ENDPROC
***************************************************************************************************************
