CLOSE DATA ALL
CLEAR
DO m:\dev\prg\lookups
PUBLIC dDate
gOffice  = "Y"

lTesting = .F.

DO m:\dev\prg\_setvars WITH lTesting
ON ESCAPE CANCEL

IF !lTesting
	runack('2XU945REPORT')
ENDIF

_SCREEN.CAPTION = "2XU 940s Processed Report"

SET DATE AMERICAN

dDate = DATE()-1  && Changed to reflect 1AM PST as the new run time.
*dDate = {^2016-12-08}

WAIT WINDOW "Now extracting PTHIST records" NOWAIT
csq1 = [select isanum as isa_num,filename,timeloaded,consignee,pt as pickticket,SPACE(10) as deleterec from pthist where accountid = 6665 and dateloaded = {]+DTOC(dDate)+[} order by isa_num, pickticket]
xsqlexec(csq1,,,"stuff")
WAIT WINDOW "PTHIST records extracted: "+TRANSFORM(RECCOUNT('pthist')) TIMEOUT 1
?RECCOUNT()

WAIT WINDOW "Now extracting INFO (deleted) records" NOWAIT
SELECT 0
csq1 = [select ship_ref as pickticket from info where accountid = 6665 and message = 'PT Deleted' and zdate = {]+DTOC(dDate)+[}]
xsqlexec(csq1,,,"wh")
WAIT WINDOW "There were "+TRANSFORM(RECCOUNT('info'))+" records marked deleted in PTHIST" TIMEOUT 2
SELECT INFO
IF lTesting
	BROWSE
ENDIF
lDelFile = .T.
IF RECCOUNT()=0
	lDelFile = .F.
ELSE
	cFileOutDel = ("h:\fox\deleted-2xu940_"+DTOS(dDate)+".xls")
	COPY TO &cFileOutDel TYPE XL5
ENDIF
USE IN INFO

WAIT WINDOW "Now extracting PT records" NOWAIT
SELECT 0
csq1 = [select ship_ref,cnee_ref as po_num,cancel,qty from pt where accountid = 6665 and ptdate = {]+DTOC(dDate)+[}]
xsqlexec(csq1,,,"wh")
WAIT WINDOW "PT records extracted: "+TRANSFORM(RECCOUNT('pt')) TIMEOUT 1
?RECCOUNT()

WAIT WINDOW "Now extracting OUTSHIP records" NOWAIT

csq1 = [select ship_ref,cnee_ref AS po_num,CANCEL,qty from outship where mod='Y' and accountid = 6665 and ptdate = {]+DTOC(dDate)+[}]
xsqlexec(csq1,"tempos",,"wh")

WAIT WINDOW "OUTSHIP records extracted: "+TRANSFORM(RECCOUNT('tempos')) TIMEOUT 1
?RECCOUNT('tempos')

WAIT WINDOW "Now compiling cursors" NOWAIT

SELECT a.*,b.po_num,b.CANCEL,b.qty FROM pthist a,pt b WHERE a.pickticket = b.ship_ref INTO CURSOR tempmergept READWRITE
nRecPT = RECCOUNT('tempmergept')

SELECT a.*,b.po_num,b.CANCEL,b.qty FROM pthist a,tempos b WHERE a.pickticket = b.ship_ref INTO CURSOR tempmergeos

IF lTesting OR (DATETIME()<DATETIME(2016,12,05,19,30,00)) OR (nRecPT > 0)
	SELECT tempmergept
	APPEND FROM DBF('tempmergeos')
	LOCATE
*	BROWSE
ENDIF
gsystemmodule = "WHY"
guserid = "JOEB"
cFileOut = ("h:\fox\2xu940_"+DTOS(dDate)+".xls")
COPY TO &cFileOut TYPE XL5
WAIT WINDOW "" TIMEOUT 2

SET DATE LONG

WAIT WINDOW "Now mailing report" NOWAIT

USE F:\3pl\DATA\mailmaster ALIAS mm IN 0
SELECT mm

tfrom = "Toll EDI Operations <toll-edi-ops@tollgroup.com>"
IF lTesting
	LOCATE FOR mm.edi_type = "MISC" AND taskname = "JOETEST"
ELSE
	LOCATE FOR mm.accountid = 6665 AND taskname = "940REPORT"
ENDIF

tsendto = IIF(mm.use_alt,sendtoalt,mm.sendto)
tcc = IIF(mm.use_alt,mm.ccalt,mm.cc)
USE IN mm

tsubject ="Daily 2XU 940 Upload Report"
tmessage = "Daily 940 uploads for "+DTOC(dDate)+" are attached"
IF lDelFile
	tattach = cFileOut+","+cFileOutDel
	tmessage = tmessage+CHR(13)+"We are including a separate Excel sheet with any picktickets deleted on the floor."
ELSE
	tattach = cFileOut	&& dy 7/5/17
	tmessage = tmessage+CHR(13)+"There were no orders deleted on the day of this report."
ENDIF

SET STEP ON
TRY
	DO FORM m:\dev\frm\dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
CATCH TO oErr
	SET STEP ON
ENDTRY

SET DATE AMERICAN
DELETE FILE &cFileOut
DELETE FILE &cFileOutDel

IF !lTesting
	schedupdate()
ENDIF

WAIT CLEAR
CLOSE DATA ALL
RETURN
