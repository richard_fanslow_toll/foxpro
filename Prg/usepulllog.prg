lparameters xprocess

for upi=1 to 1000
	try
		use (wf(goffice)+"pulllog") exclusive in 0
		upi=9999
	catch
	endtry

	xwvlogok=.f.
	try		&& sometimes pulllog is opened read-only  ???
		replace flag with .t. in pulllog
		xwvlogok=.t.
	catch
	endtry
	
	if !xwvlogok
		try
			use in pulllog
		catch
		endtry
		errmsg("PULLLOG was opened read-only - "+transform(upi),"usepulllog")		
		loop
	else
		exit
	endif
next

if !used("pulllog")
*	email("Dyoung@fmiint.com","PULLLOG can't be used by "+xprocess,,,,,.t.,,,,,.t.)
	return .f.
endif