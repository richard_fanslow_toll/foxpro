CD &lcPath
ASSERT .F. MESSAGE "In PROCESS phase...debug"
lEmail = .F.
lNewPT = .T.

lcFA997Path = "f:\ftpusers\intradeco-"+IIF(cOffice="C","ca","fl")+"\997in\"

waitstr = "Processing records for "+cMailName
WAIT WINDOW waitstr TIMEOUT 2

cFilemask = IIF(lTesting,"*.*",cFilemask)
lnNum = ADIR(tarray,cFilemask)

IF lnNum = 0
	WAIT WINDOW AT 10,20 "No "+cMailName+" 940's to import for "+cOfficename TIMEOUT 2
	NormalExit = .T.
	THROW
ENDIF

delimchar = "*"
lcTranOpt = "NOTILDE"

FOR thisfile = 1  TO lnNum
	lLoop = .F.
	xfile = lcPath+TRIM(tarray[thisfile,1])
	cFilename = TRIM(tarray[thisfile,1])
	nFileSize = VAL(TRANSFORM(tarray(thisfile,2)))
	fa997file = lcFA997Path+cFilename

	DO m:\dev\prg\createx856a
	IF !INLIST(cFilename,"20111220120317a_outbound.edi")
		DO m:\dev\prg\loadedifile WITH xfile,delimchar,lcTranOpt,cCustname
	ELSE
		SELECT x856
		APPEND FROM [&cFilename] TYPE DELIMITED WITH CHARACTER "*"
		IF lTesting
			BROWSE
		ENDIF
	ENDIF

	WAIT WINDOW "Importing file: "+xfile TIMEOUT 1
	IF FILE(xfile)
		RELEASE ALL LIKE APT
		RELEASE ALL LIKE APTDET
		SELECT x856
		archivefile = lcArchivepath+cFilename

		DO create_pt_cursors WITH cUseFolder,lLoadSQL && Added to prep for SQL Server transition

		SELECT xpt
		INDEX ON ship_ref TAG ship_ref
		INDEX ON ptid TAG ptid
		SET ORDER TO ship_ref
		IF USED('pt')
		USE IN pt
		endif

		SELECT x856
		LOCATE
		IF FILE(xfile)
			COPY FILE [&xfile] TO [&archivefile]
		ENDIF

		DO (cUseDir+"BKDN") WITH cFilename
		IF lLoop
			LOOP
		ENDIF
		
		DO m:\dev\prg\all940_import

*		release_ptvars()

	ENDIF
NEXT thisfile

&& now clean up the 940in archive files, delete for the upto the last 10 days
deletefile(lcarchivepath,10)

WAIT clear
WAIT WINDOW "ALL "+cMailName+" 940 files finished processing for: "+cOfficename+"..." TIMEOUT 3
RETURN
