lparameters xfromauto, xnosettalkon, xinventable, xwithbuffering

*set coverage to m:\darren\covlog

if xinventable
	xxinven="inven"
	xxinvenloc="invenloc"
else
	xxinven="vinven"
	xxinvenloc="vinvenloc"
endif

if xwithbuffering
	xbuffering="with (buffering=.t.)"
else
	xbuffering=""
endif

xinvenid=&xxinven..invenid
xaccountid=&xxinven..accountid
xallocqty=0

create cursor xmatch (whseloc c(7), qty n(9), hold l)
index on whseloc tag whseloc

* un-hold RACK location

replace hold with .f., holdtype with "" for hold and units=&xxinven..units and style=&xxinven..style and color=&xxinven..color and id=&xxinven..id and pack=&xxinven..pack and whseloc="RACK" in &xxinvenloc

* inbounds

xsqlexec("select whseloc, locqty from indet, inloc where indet.indetid=inloc.indetid and indet."+gmodfilter+" " + ;
	"and indet.accountid="+transform(&xxinven..accountid)+" and units="+transform(&xxinven..units)+" and style='"+&xxinven..style+"' " + ;
	"and color='"+&xxinven..color+"' and id='"+&xxinven..id+"' and pack='"+&xxinven..pack+"'","xfixtemp",,"wh")	

scan 
	if !seek(xfixtemp.whseloc,"xmatch","whseloc")
		m.whseloc=xfixtemp.whseloc
		m.qty=xfixtemp.locqty
		insert into xmatch from memvar
	else
		replace qty with xmatch.qty+xfixtemp.locqty in xmatch
	endif
endscan

* outbounds

if !xnosettalkon
*	set talk on
*	set notify on
endif

xsqlexec("select outdet.outdetid, outdet.accountid, style, color, id, pack, " + ;
	"units, whseloc, totqty, locqty from outdet left join outloc on outdet.outdetid=outloc.outdetid " + ;
	"where outdet."+gmodfilter+" and outdet.accountid="+transform(&xxinven..accountid)+" " + ;
	"and units="+transform(&xxinven..units)+" and style='"+&xxinven..style+"' " + ;
	"and color='"+&xxinven..color+"' and id='"+&xxinven..id+"' and pack='"+&xxinven..pack+"'","xdytemp",,"wh")

select outdetid, accountid, style, color, id, pack, units, whseloc, ;
	sum(totqty) as totqty, sum(locqty) as locqty ;
	from xdytemp group by 1,2,3,4,5,6,7,8 into cursor xoutdet

if !xnosettalkon	
*	set talk off
*	set notify off
	clear
endif

if date()={1/8/18}
	set step on
endif

select xoutdet
scan 
	if isnull(whseloc)
		xallocqty=xallocqty+xoutdet.totqty
	else
		m.qty=-xoutdet.locqty
		
		if !seek(xoutdet.whseloc,"xmatch","whseloc")
			m.whseloc=xoutdet.whseloc
			insert into xmatch from memvar
		else
			replace qty with xmatch.qty+m.qty in xmatch
		endif
	endif
endscan

* adjustments

xsqlexec("select * from adj where "+gmodfilter+" and accountid="+transform(&xxinven..accountid)+" " + ;
	"and "+gmodfilter+" and units="+transform(&xxinven..units)+" and style='"+&xxinven..style+"' " + ;
	"and color='"+&xxinven..color+"' and id='"+&xxinven..id+"' and pack='"+&xxinven..pack+"'","xadj",,"wh")

scan
	if !seek(xadj.whseloc,"xmatch","whseloc")
		m.whseloc=xadj.whseloc	
		m.qty=xadj.totqty
		insert into xmatch from memvar
	else
		replace qty with xmatch.qty+xadj.totqty in xmatch
	endif
endscan

* pick & pack replenishment & unit putback

if type("goffice")#"C"	&& dy 12/30/17 when called from a utility program
	public goffice
	goffice=xoffice
endif

if !&xxinven..units
	xsqlexec("select * from pprep where "+gmodfilter+" and accountid="+transform(xaccountid)+" and style='"+&xxinven..style+"' " + ;
		"and color='"+&xxinven..color+"' and id='"+&xxinven..id+"' and pack='"+&xxinven..pack+"' and movedt={}","xpprep",,"wh")
	
	sum totqty to xpprepqty 
	xallocqty=xallocqty+xpprepqty

else
	xsqlexec("select * from putback where "+gmodfilter+" and accountid="+transform(xaccountid)+" and style='"+&xxinven..style+"' " + ;
		"and color='"+&xxinven..color+"' and id='"+&xxinven..id+"' and pack='"+&xxinven..pack+"' and movedt={}","xputback",,"wh")
	
	sum unitqty to xputbackqty
	xallocqty=xallocqty+xputbackqty
endif

*

if !xfromauto
	select * from &xxinvenloc where hold and whseloc#"RACK" into cursor xhold
else
	select * from &xxinvenloc where hold and whseloc#"RACK" and units=&xxinven..units and style=&xxinven..style and color=&xxinven..color and id=&xxinven..id and pack=&xxinven..pack into cursor xhold
endif

*

if !xfromauto
	delete from &xxinvenloc where invenid=&xxinven..invenid and locqty=0
endif
delete from &xxinvenloc where invenid=&xxinven..invenid

select &xxinven
scatter memvar

select xmatch
sum qty to xtotqty
scan for qty#0
	m.locqty=qty
	m.whseloc=whseloc
	m.date_rcvd=qdate()
	m.hold=holdloc(m.whseloc)
	m.holdtype=gholdtype
	insertinto(xxinvenloc,"wh",.t.)
endscan

*

if !xnosettalkon
	select xhold
	scan
		if xinventable
			select &xxinvenloc
			locate for accountid=xhold.accountid and units=xhold.units and style=xhold.style and color=xhold.color and id=xhold.id and pack=xhold.pack and whseloc=xhold.whseloc
			if found()
				replace hold with .t., holdtype with xhold.holdtype in &xxinvenloc
			endif
		else
			if seek(str(accountid,4)+transform(units)+style+color+id+pack+whseloc,xxinvenloc,"matchwh")
				replace hold with .t., holdtype with xhold.holdtype in &xxinvenloc
			endif
		endif
	endscan
endif

select &xxinvenloc
sum locqty to xholdqty for invenid=&xxinven..invenid and hold

if type("gmasteroffice")="C"
	xfixoffice=gmasteroffice
else
	xfixoffice=iif(inlist(xoffice,"1","2","5","6","7"),"C",iif(inlist(xoffice,"I","J"),"N",xoffice))
endif

if !used("account")
	xsqlexec("select * from account where inactive=0","account",,"qq")
	index on accountid tag accountid
	set order to
endif

if &xxinven..units and seek(&xxinven..accountid,"account","accountid") and account.pnp&xfixoffice
	if used("invenloc")
		select invenloc
	else
		select &xxinvenloc
	endif
	scan for !units and style=&xxinven..style and color=&xxinven..color and id=&xxinven..id and hold and whseloc#"CYCLE"
		xholdqty=xholdqty+val(pack)*locqty
	endscan

* probably need to restore and fix		dy 5/30/17
	
*	xmusical = musical(&xxinven..accountid, &xxinven..style, &xxinven..color, &xxinven..id, &xxinven..pack)

*	if xmusical>1
*		select indet
*		locate for wo_num=xwo_num and po=xpo and !units
*		if found()
*			if seek(str(indet.accountid,4)+".F."+indet.style+indet.color+indet.id+indet.pack,xxinven,"match")
*				if &xxinven..holdqty>0	
*					xholdqty=xholdqty+xunitqty/indet.totqty*&xxinven..holdqty
*				endif
*			endif
*		endif
		
*		=seek(xinvenid,xxinven,"invenid")
*	endif
endif

replace totqty with xtotqty, holdqty with xholdqty, allocqty with xallocqty, availqty with &xxinven..totqty-&xxinven..allocqty-&xxinven..holdqty in &xxinven

tu(xxinven)
tu(xxinvenloc)
