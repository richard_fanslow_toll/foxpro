CD &lcPath
ASSERT .F. MESSAGE "In PROCESS phase...debug"
lEmail = .F.
lNewPT = .T.

lcFA997Path   = "f:\ftpusers\NAUTICA\940xfer\"

waitstr = "Processing records for "+cMailName
WAIT WINDOW waitstr TIMEOUT 2

cFilemask = IIF(lTesting,"*.*",cFilemask)
lnNum = ADIR(tarray,cFilemask)

IF lnNum = 0
	WAIT WINDOW AT 10,20 "No "+cMailName+" 940's to import for "+cOfficename TIMEOUT 2
	NormalExit = .T.
	THROW
ENDIF

FOR thisfile = 1  TO lnNum
	lLoop = .F.
	xfile = lcPath+TRIM(tarray[thisfile,1])
	cFilename = TRIM(tarray[thisfile,1])
	nFileSize = VAL(TRANSFORM(tarray(thisfile,2)))
	archivefile = lcArchivePath+cFilename
	fa997file = lcFA997Path+cFilename

	DO m:\dev\prg\createx856a
	DO m:\dev\prg\loadedifile WITH xfile,"*","07H",cCustname

	WAIT WINDOW AT 10,10 "Importing file: "+xfile TIMEOUT 1

	IF FILE(xfile)
		SELECT x856

		DO create_pt_cursors WITH cUseFolder,lLoadSQL && Added to prep for SQL Server transition

		SELECT xpt
		INDEX ON ship_ref TAG ship_ref
		INDEX ON ptid TAG ptid
		SET ORDER TO ship_ref
*
*		USE IN pt
*!* End of added code
		SELECT x856

		IF FILE(xfile)
			COPY FILE [&xfile] TO [&archivefile]
			IF !lTesting
			COPY FILE [&xfile] TO [&fa997file]
			endif
		ENDIF

		SET STEP ON 
		DO m:\dev\prg\nautica_940bkdn WITH xfile
		DO m:\dev\prg\all940_import

	ENDIF
NEXT thisfile

&& now clean up the 940in archive files, delete for the upto the last 10 days
deletefile(lcarchivepath,IIF(DATE()<{^2016-05-01},20,10))

WAIT CLEAR 
WAIT WINDOW "ALL "+cMailName+" 940 files finished processing for: "+cOfficename+"..." TIMEOUT 3
RETURN
