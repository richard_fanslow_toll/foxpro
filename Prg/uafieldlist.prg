PUBLIC fieldlist

SET EXCLUSIVE OFF
SET SAFETY OFF
CLOSE DATA ALL
SET DELETED ON
USE F:\wh6\whdata\outship IN 0
IF !USED('edi_trigger')
	cEDIFolder = IIF(DATE()<{^2010-01-24},"F:\JAG3PL\DATA\","F:\3PL\DATA\")
	USE (cEDIFolder+"edi_trigger") IN 0 ALIAS edi_trigger
ENDIF
USE F:\ftplogs\DATA\ftplog IN 0


SELECT wo_num,wo_date,ship_ref,bol_no,scac,START,CANCEL,del_date,appt,SPACE(50) AS fname, SPACE(15) AS UARcvd FROM outship;
	WHERE accountid = 5687 AND (del_date > DATE()-14 OR EMPTYnul(del_date)) INTO CURSOR bols READWRITE
SELECT bols
GOTO TOP
SCAN
	DELETE FOR "OV"$ship_ref
	DELETE FOR wo_date < {^2009-06-30}
ENDSCAN

GOTO TOP
SCAN
	SELECT edi_trigger
	SET ORDER TO bol
	SEEK bols.bol_no
	IF FOUND()
		REPLACE bols.fname WITH JUSTFNAME(file945)
	ENDIF
ENDSCAN

SET STEP ON
SELECT bols
GOTO TOP
SCAN
	SELECT ftplog
	LOCATE FOR UPPER(ALLTRIM(bols.fname))$UPPER(ftplog.filename)
	IF FOUND()
		REPLACE bols.UARcvd WITH "PickedUp" IN bols
	ELSE
		REPLACE bols.UARcvd WITH "NO" IN bols
	ENDIF
ENDSCAN

rptok = .F.
DO reporttopdf WITH "ua945report","c:\tempfox\UA945Report",rptok

SET STEP ON


SET FILTER TO accountid = 5687 AND edi_type ="944X"
fieldlist = "edi_type,office,accountid,when_proc,wo_num,date,fin_status,ponum,arrival,awb,ctrprfx,container,trig_time,file944crt"
*Browse fields &fieldlist
rptok = .F.

*Do reporttopdf with "uatpmreport","c:\tempfox\UA_TPM_Status_Report",rptok

SELECT &fieldlist FROM edi_trigger INTO CURSOR temp1 READWRITE WHERE accountid = 5687 AND edi_type ="944X"
SELECT temp1
GOTO TOP

REPLACE fin_status WITH "No TPM\ASN data" FOR fin_status = "NO FIND IN SHIPHDR"
GOTO TOP
REPLACE ALL file944crt WITH JUSTFNAME(file944crt)

*Set filter to fin_status != "944 CREATED"
GOTO TOP


DO reporttopdf WITH "uatpmreport","c:\tempfox\UA_TPM_Status_Report",rptok


PUBLIC fieldlist

SET EXCLUSIVE OFF
SET SAFETY OFF
CLOSE DATA ALL
SET DELETED ON
IF !USED('edi_trigger')
	cEDIFolder = IIF(DATE()<{^2010-01-24},"F:\JAG3PL\DATA\","F:\3PL\DATA\")
	USE (cEDIFolder+"edi_trigger") IN 0 ALIAS edi_trigger
ENDIF
SELECT edi_trigger

SET FILTER TO accountid = 5687 AND edi_type ="944X"
fieldlist = "edi_type,office,accountid,when_proc,wo_num,date,fin_status,ponum,arrival,awb,ctrprfx,container,trig_time,file944crt"
*Browse fields &fieldlist
rptok = .F.

*Do reporttopdf with "uatpmreport","c:\tempfox\UA_TPM_Status_Report",rptok

SELECT &fieldlist FROM edi_trigger INTO CURSOR temp1 READWRITE WHERE accountid = 5687 AND edi_type ="944X"
SELECT temp1
GOTO TOP

REPLACE fin_status WITH "No TPM\ASN data" FOR fin_status = "NO FIND IN SHIPHDR"
GOTO TOP
REPLACE ALL file944crt WITH JUSTFNAME(file944crt)

*Set filter to fin_status != "944 CREATED"
GOTO TOP


DO reporttopdf WITH "uatpmreport","c:\tempfox\UA_TPM_Status_Report",rptok


SET FILTER TO accountid = 5687 AND INLIST(edi_type,"945T","945X")
fieldlist = "edi_type,bol,scac,office,accountid,when_proc,wo_num,date,fin_status,ship_ref,ponum,arrival,awb,ctrprfx,container,trig_time,file945,file944crt"
*Browse fields &fieldlist
rptok = .F.

*Do reporttopdf with "uatpmreport","c:\tempfox\UA_TPM_Status_Report",rptok

SELECT &fieldlist FROM edi_trigger INTO CURSOR temp1 READWRITE WHERE accountid = 5687 AND INLIST(edi_type,"945T","945X")
SELECT temp1
GOTO TOP
REPLACE fin_status WITH "No TPM\ASN data" FOR fin_status = "NO FIND IN SHIPHDR"
GOTO TOP
REPLACE ALL file944crt WITH JUSTFNAME(file945)
GOTO TOP
REPLACE ALL file945 WITH JUSTFNAME(file945)

GOTO TOP
*Set filter to !InList(fin_status,"945 CREATED","NO 945 NEEDED")
GOTO TOP

DO reporttopdf WITH "uatpmshipmentreport","c:\tempfox\UA_TPM_Status_Report2",rptok








*Parameter lcReportName,lcOutputFileName,ReportOK
