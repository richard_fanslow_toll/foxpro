* add/update inrcv

lparameters xaddfrom, xstatus, xforcetofmi, xwonum

if !empty(xwonum)
	=seek(xwonum,"wolog","wo_num")
endif

if inrcvok(wolog.accountid,wolog.wo_num)
	xhouseaccount=.t.
	
	if used("inwolog")
		use in inwolog
	endif
	if used("pl")
		use in pl
	endif

	xtofmi=.t.
	if !xforcetofmi and !(wolog.office="N" and inlist(wolog.accountid,1,62,1747))
		if seek(wolog.wo_num,"detail","wo_num")
			select detail
			locate for wo_num=wolog.wo_num and delloc="FMI"
			if !found()
				xtofmi=.f.
			endif
		endif
	endif

	xwmsmod=wf(goffice,wolog.accountid,,,,,,.t.)
	xaccountid=wolog.accountid
	
	if xtofmi
		select inrcv
		scatter memvar memo blank

		m.inwonum=0
		m.ltwonum=wolog.wo_num
		m.acctref=iif(!empty(wolog.cust_ref),wolog.cust_ref,wolog.brokref)

		do case
		case !empty(wolog.container)
			m.container=wolog.container
		case !empty(wolog.awb)
			m.container=wolog.awb
		otherwise
			m.container=wolog.wotype
		endcase	

		if m.container#"EDIT"
			store "" to xinrcvfilter, xinrcvfilter2
			inrcvfilters()

			xsqlexec("select * from inwolog where "+xinrcvfilter+" and "+xinrcvfilter2,,,"wh")
			locate
			
			if !found()
				if reccount("inrcv")=0
					m.accountid=wolog.accountid
					m.acctname=wolog.acctname
					m.inwonum=0
					m.skus=0
					m.cartons=iif(wolog.quantity=1,0,wolog.quantity)
					inrcvinsert(xaddfrom, xstatus)
				else
					inrcvstatus(xaddfrom, xstatus)
				endif

			else
				select inwolog
				scan for &xinrcvfilter and &xinrcvfilter2
					select inrcv
					locate for accountid=inwolog.accountid and inwonum=inwolog.wo_num
					if found()
						replace ltwonum with wolog.wo_num in inrcv
						inrcvstatus(xaddfrom, xstatus)
						if inrcv.container#wolog.container and !empty(wolog.container)
							replace container with wolog.container in inrcv
						endif
					else
						if reccount("inrcv")#0
							select inrcv
							scan for ltwonum=wolog.wo_num
								if inrcv.inwonum#inwolog.wo_num
									replace inwonum with inwolog.wo_num in inrcv
								endif
								if inrcv.accountid#inwolog.accountid
									replace accountid with inwolog.accountid in inrcv
								endif
								inrcvstatus(xaddfrom, xstatus)
							endscan
						else
							m.inwonum=inwolog.wo_num
							m.accountid=inwolog.accountid
							m.acctname=inwolog.acctname
							if gsystemmodule#"WH"	
								xdyoffice=wf(wolog.office,wolog.accountid,,,,,,.t.)
							else
								xdyoffice=goffice
							endif
							xsqlexec("select * from pl where mod='"+xdyoffice+"' and inwologid="+transform(inwolog.inwologid),,,"wh")
							count to m.skus for !units
							m.cartons=inwolog.plinqty

							inrcvinsert(xaddfrom, xstatus)
						endif
					endif
				endscan
			endif
		endif
	endif
		
	if used("inwolog")
		use in inwolog
	endif	
	if used("pl")
		use in pl
	endif
	
	try
		set database to wh
		close databases
	catch
	endtry
	
	tu("inrcv")
endif


************
procedure inrcvinsert

lparameters xaddfrom, xstatus

m.newdt=date()
m.addfrom=xaddfrom
m.dychar="5"

if goffice="N" and (seek(wolog.wo_num,"detail","wo_num") or m.accountid=5687)
	m.transload=.t.
endif

do case
case wolog.wotype="OCEAN FULL"
	m.wotype="O"
case wolog.wotype="OCEAN EXPORT"
	m.wotype="E"
case wolog.wotype="OCEAN LOOSE"
	m.wotype="L"
case wolog.wotype="AIR"
	m.wotype="A"
case wolog.wotype="DOMESTIC"
	m.wotype="D"
otherwise
	m.wotype="?"
endcase

m.confirmdt=empty2nul(m.confirmdt)
m.dydt=empty2nul(m.dydt)
m.firstavaildt=empty2nul(m.firstavaildt)
m.apptdt=empty2nul(m.apptdt)
m.pickedup=empty2nul(m.pickedup)

inrcvok(wolog.accountid,wolog.wo_num)		&& need to run this to reset gforceoffice for the next call to wf()

m.mod=wf(goffice,wolog.accountid,,,,,,.t.)

if inlist(m.mod,"L","Y","R")
	m.office=m.mod
else
	m.office=goffice
endif

m.inrcvid=dygenpk("inrcv","whall")
insert into inrcv from memvar
set step on 
if guserid="DYOUNG"
	x3winmsg("inrcv added")
endif

inrcvstatus(xaddfrom, xstatus)


************
procedure inrcvstatus

lparameters xaddfrom, xstatus

do case
case xaddfrom="F"	
	if !inlist(inrcv.status,"PUTAWAY","CONFIRM")		
		replace status with "DISPATCHED" in inrcv
	endif
case xaddfrom="A"
	if empty(inrcv.status) or inlist(inrcv.status,"AVAILABLE","NOT AVAILABLE")
		if inrcv.status#"AVAILABLE" and xstatus="AVAILABLE"
			replace firstavaildt with qdate() in inrcv
		endif
		replace status with iif(xstatus="AVAILABLE","AVAILABLE","NOT AVAILABLE") in inrcv
	endif	
case inlist(xaddfrom,"C","T")
	if !inlist(inrcv.status,"PUTAWAY","CONFIRM")		
		replace status with "IN YARD" in inrcv
	endif
case inrcv.status#xstatus
	replace status with xstatus in inrcv
endcase
