*!* RAGBONE944_CREATE.PRG
*!* This program is triggered automatically via the
*!* EDI_TRIGGER table, populated via the OUTWO.SCX "Create Invoice" button
*!* EDI_TRIGGER is active and sits at the current WO_Num record...

PARAMETERS nwo_num
ntrk_wo = 0

PUBLIC lxfer944,ltesting,csuffix,lhold944,lcarchivepath,lcoutpath,cfilenameout,cfilenamehold,cfilenamearchive,cfin

ltesting = .f. && If true, disables certain functions (Default = .f.)
ltestinput = .f.  && If true, uses alternate source tables (Default = .f.)
lhold944 = ltesting && If true, holds output in 944HOLD (Default = .f.)
lemail = .T.  && If true, sends regular email notices (Default = .t.)
*lTestMail = .t. && If true, sends mail ONLY to Joe (Default = .f.)
loverridexcheck = .F. && If true, doesn't check ASN carton count against barcode scans.
lemail = .T.

nacctnum = 6699
coffice = "N"
cmod = "I"
IF USED('inwolog')
	USE IN inwolog
ENDIF

*!* Added as a pre-check for Return to Stock items, no 944 per Chris Malcolm, 01.16.2018, Joe
IF !ltesting
	xsqlexec("select * from inwolog where accountid = "+TRANSFORM(nacctnum)+" and wo_num = "+TRANSFORM(nwo_num),,,"wh")
	IF inwolog.returntostock = .T.
		REPLACE edi_trigger.processed WITH .T.,edi_trigger.created WITH .T.,edi_trigger.when_proc WITH DATETIME(),;
			edi_trigger.fin_status WITH "NO 944 NEEDED - RTS",edi_trigger.errorflag WITH .F.;
			FOR  edi_trigger.accountid = nacctnum AND edi_trigger.edi_type = '944' AND edi_trigger.wo_num = nwo_num IN edi_trigger
	USE IN inwolog
	RETURN
	ENDIF
ENDIF
*!* End of pre-check

IF USED('inwolog')
	USE IN inwolog
ENDIF

gmasteroffice = coffice
goffice = cmod
m.office = coffice
m.mod = cmod
ON ERROR DEBUG

DO m:\dev\prg\_setvars WITH ltesting

IF ltesting
	CLOSE DATABASES ALL
	nwo_num =   3229566
ENDIF

DIMENSION thisarray(1)
IF ltestinput
	cusefolder = "F:\WHP\WHDATA\"
ELSE
	xreturn = "XXX"
	wf(coffice,nacctnum)
	cusefolder = UPPER(xreturn)
ENDIF

IF TYPE("nWO_Num") <> "N"  OR EMPTY(nwo_num) && If WO is not numeric or equals Zero
	WAIT WINDOW "No WO# Provided...closing" TIMEOUT 3
	DO ediupdate WITH "NO WO#"
	closeout()
	RETURN
ENDIF

cwo_num = ALLTRIM(STR(nwo_num))
cintusage = IIF(ltesting,"T","P")

SELECT 0
USE F:\3pl\DATA\mailmaster ALIAS mm SHARED
SELECT mm
LOCATE FOR mm.accountid = nacctnum ;
	AND mm.office = coffice ;
	AND mm.edi_type = "944"
IF !FOUND()
	ASSERT .F. MESSAGE "At missing Office/Acct"
	WAIT WINDOW "Office/Loc/Acct not found!" TIMEOUT 2
	DO ediupdate WITH .T.,"ACCT/LOC !FOUND"
	normalexit = .F.
	THROW
ENDIF
STORE TRIM(mm.acctname) TO ccustname
STORE TRIM(mm.basepath) TO lcoutpath
STORE TRIM(mm.holdpath) TO lcholdpath
STORE TRIM(mm.archpath) TO lcarchivepath
_SCREEN.CAPTION = ALLTRIM(mm.scaption)+" "+TRANSFORM(nwo_num)
IF ltesting
	LOCATE
	LOCATE FOR edi_type = "MISC" AND taskname = "JOETEST"
ENDIF
lusealt = mm.use_alt
tsendto = IIF(lusealt,mm.sendtoalt,mm.sendto)
tcc = IIF(lusealt,mm.ccalt,mm.cc)
LOCATE
LOCATE FOR edi_type = "MISC" AND taskname = "GENERAL"
tsendtotest = IIF(lusealt,mm.sendtoalt,mm.sendto)
tcctest = IIF(lusealt,mm.ccalt,mm.cc)
tsendtoerr = IIF(lusealt,mm.sendtoalt,mm.sendto)
tccerr = IIF(lusealt,mm.ccalt,mm.cc)
USE IN mm

casn = ""
nstsets = 0
cspecchar = .F.
xferupdate = .T.
nfilenum = 0
lbackup = .F.
loverage = .F.
lsendmail = .T.
cerrmsg = ""
liserror = .F.
lxfer944 = .F.
ctrk_wo = ""
nctntotal = 0

IF !ltesting
	IF !USED("edi_xfer")
		USE F:\edirouting\DATA\edi_xfer IN 0 ALIAS edi_xfer ORDER TAG inven_wo
		SELECT edi_xfer
		SET FILTER TO edi_type = "944" AND accountid = nacctnum
		IF SEEK(nwo_num)
			WAIT WINDOW "Work Order "+cwo_num+" already created and logged in EDI_XFER" TIMEOUT 2
			IF MESSAGEBOX("944 for Work Order "+cwo_num+" already created...Overwrite Date?",4+32+256,"WO# FOUND",8000) = 7 &&No
				USE IN edi_xfer
				DO ediupdate WITH "DUPLICATE WO#"
				closeout()
				RETURN
			ELSE
				xferupdate = .F.
			ENDIF
		ENDIF
	ENDIF
ENDIF

*!* SET CUSTOMER CONSTANTS
cdirused = lcoutpath

cx12 = "004010"  && X12 Standards Set used

*!* Search table (parent)
IF USED('inwolog')
	USE IN inwolog
ENDIF

xsqlexec("select * from inwolog where accountid = "+TRANSFORM(nacctnum)+" and wo_num = "+TRANSFORM(nwo_num),,,"wh")
SELECT inwolog
IF RECCOUNT()  = 0
	WAIT WINDOW "WO# "+cwo_num+" not found in INWOLOG...Terminating" TIMEOUT 3
	IF !ltesting
		DO ediupdate WITH "MISS INWOLOG WO",.T.
		closeout()
		RETURN
	ENDIF
ENDIF

lra = IIF(LEFT(ALLTRIM(inwolog.brokerref),2)= "RA",.T.,.F.)  && Added per Tom, 08.30.2017, Joe
IF  lra  && Added TMARG 09/06/17 to pass correct W17 01
	corig = "C"
ELSE
	corig = "J"
ENDIF
ccustprefix = IIF(lra,"TG-RA","TG-ASN")
crecid = STRTRAN(ccustname," ","")
crbmailname = ccustname
dt1 = TTOC(DATETIME(),1)
dt2 = DATETIME()
dtmail = TTOC(DATETIME())

cfileshort = ccustprefix+dt1+".944"
cfilenamehold = (lcholdpath+cfileshort)
lcoutpath = IIF(ltesting, lcoutpath+"test\",lcoutpath)
cfilenameout = lcoutpath+cfileshort
cfilenamearchive = (lcarchivepath+cfileshort)

STORE "" TO c_cntrlnum,c_grpcntrlnum
nfilenum = FCREATE(cfilenamehold)

*!* SET OTHER CONSTANTS
nctnnumber = 1
csendqual = "ZZ"
csendid = "TOLLUSA"
crecqual = "01"
cfd = "|"  && Field/element delimiter
csegd = "~" && Line/segment delimiter
nsegctr = 0
cterminator = ":" && Used at end of ISA segment
cdate = DTOS(DATE())
ctruncdate = RIGHT(cdate,6)
ctime = LEFT(TIME(DATE()),2)+SUBSTR(TIME(DATE()),4,2)
cfiledate = cdate+ctime
*corig = "C"
cqtyrec = "1"
cctypeused = ""
cmissingctn = "Missing Cartons: "+CHR(13)
STORE 0 TO nqtyexpected,nctndamaged,nctnshortage,nctnexpected,nctnactual
STORE 0 TO npoexpecte,npoactual,nctnshortage
STORE "" TO cstring,cponum,cstyle

** PAD ID Codes
csendidlong = PADR(csendid,15," ")
crecidlong = PADR(crecid,15," ")

*!* Serial number incrementing table
IF !USED("serfile")
	USE ("F:\3pl\data\serial\ragbone944_serial") IN 0 ALIAS serfile
ENDIF

*!* Trigger Data
IF !USED('edi_trigger')
	cedifolder = "F:\3PL\DATA\"
	USE (cedifolder+"edi_trigger") IN 0 ALIAS edi_trigger ORDER TAG wo_num
ENDIF

*!* Search table (parent)
IF USED('inwolog')
	USE IN inwolog
ENDIF

xsqlexec("select * from inwolog where accountid = "+TRANSFORM(nacctnum)+" and wo_num = "+TRANSFORM(nwo_num),,,"wh")
SELECT inwolog
IF RECCOUNT()  = 0
	WAIT WINDOW "WO# "+cwo_num+" not found in INWOLOG...Terminating" TIMEOUT 3
	IF !ltesting
		DO ediupdate WITH "MISS INWOLOG WO",.T.
		closeout()
		RETURN
	ENDIF
ENDIF

*!* Search table (xref for PO's)
IF USED('INDET')
	USE IN indet
ENDIF

IF ltesting
	xsqlexec("select * from pl where accountid = "+TRANSFORM(nacctnum)+" and wo_num = "+TRANSFORM(nwo_num),"indet",,"wh")
ELSE
	xsqlexec("select * from indet where accountid = "+TRANSFORM(nacctnum)+" and wo_num = "+TRANSFORM(nwo_num),,,"wh")
ENDIF
SELECT indet
INDEX ON inwologid TAG inwologid
IF !ltesting
	LOCATE FOR wo_num = nwo_num
	IF !FOUND()
		waitstr = "WO# "+cwo_num+" Not Found in Inbd Detail Table..."+CHR(13)+;
			"WO has not been confirmed...Terminating"
		WAIT WINDOW waitstr TIMEOUT 3
		DO ediupdate WITH "WO NOT CONF. IN INDET"
		closeout()
		RETURN
	ENDIF
ENDIF

nrcnt = 0
nisacount = 0
nstcount = 1
nsecount = 1

SELECT inwolog
alength = ALINES(apt,inwolog.comments,.T.,CHR(13)) && Creates an array of Comments lines
ccontainer = ALLTRIM(inwolog.CONTAINER)

IF EMPTY(TRIM(ccontainer))
	WAIT WINDOW "There is no container attached to "+cwo_num TIMEOUT 3
	DO ediupdate WITH "NO CONTAINER"
	closeout()
	RETURN
ENDIF

SELECT inwolog
cacctname = TRIM(inwolog.acctname)
crefer_num = ""
cacct_ref = ALLTRIM(inwolog.acct_ref)
cbrokref = ALLTRIM(inwolog.brokerref)
cseal = ALLTRIM(inwolog.seal)
IF nisacount = 0
	DO num_incr
	STORE "ISA"+cfd+"00"+cfd+"          "+cfd+"00"+cfd+"          "+cfd+csendqual+cfd+csendidlong+cfd+;
		crecqual+cfd+crecidlong+cfd+ctruncdate+cfd+ctime+cfd+"U"+cfd+"00401"+cfd+;
		PADL(c_cntrlnum,9,"0")+cfd+"0"+cfd+cintusage+cfd+cterminator+csegd TO cstring
	DO cstringbreak

	STORE "GS"+cfd+"RE"+cfd+csendid+cfd+crecid+cfd+cdate+cfd+ctime+cfd+c_cntrlnum+;
		cfd+"X"+cfd+cx12+csegd TO cstring
	DO cstringbreak

	nsegctr = 0
	nisacount = 1
ENDIF

SELECT inwolog

IF nsecount = 1
	STORE "ST"+cfd+"944"+cfd+PADL(c_grpcntrlnum,9,"0")+csegd TO cstring
	DO cstringbreak
	nsegctr = nsegctr + 1
*** changed W1703 to SEAL  TMARG  09/13/17
*	store "W17"+cfd+corig+cfd+cdate+cfd+cseal+cfd+cbrokref+csegd to cstring  && Changed TMARG 09/06/17
	STORE "W17"+cfd+corig+cfd+cdate+cfd+cacct_ref+cfd+cbrokref+csegd TO cstring  && Changed TMARG 09/06/17
*	store "W17"+cfd+corig+cfd+cdate+cfd+cbrokref+csegd to cstring
	DO cstringbreak
	nsegctr = nsegctr + 1

	STORE "N1"+cfd+"ZL"+cfd+ccustname+csegd TO cstring
	DO cstringbreak
	nsegctr = nsegctr + 1

	STORE "N9"+cfd+"CN"+cfd+ccontainer+csegd TO cstring  && Used for either AWB or container type
	DO cstringbreak
	nsegctr = nsegctr + 1

**	if !empty(cbrokref)
	IF 	corig = "C"
		STORE "N9"+cfd+"RA"+cfd+cbrokref+csegd TO cstring
		DO cstringbreak
		nsegctr = nsegctr + 1
	ENDIF

	STORE "N9"+cfd+"TL"+cfd+cacct_ref+csegd TO cstring
	DO cstringbreak
	nsegctr = nsegctr + 1

	STORE "N9"+cfd+"DE"+cfd+ALLTRIM(TRANSFORM(nwo_num))+csegd TO cstring
	DO cstringbreak
	nsegctr = nsegctr + 1

	STORE "G62"+cfd+"09"+cfd+DTOS(DATE())+csegd TO cstring
	DO cstringbreak
	nsegctr = nsegctr + 1

	nsecount = 0
ENDIF

cstring = ""
SELECT indet
SUM totqty  TO nsumtotqty  FOR wo_num = nwo_num AND indet.inwologid = inwolog.inwologid AND units
nsumorigqty = nsumtotqty
ndiffqty = (nsumtotqty)

LOCATE

SCAN
	alendet = ALINES(aptdet,indet.ECHO,.T.,CHR(13)) && Creates an array of Echo lines

	cupc = TRIM(segmentget(@aptdet,"UPC",alendet))
	lnxqty = indet.totqty
	cstyle = ALLTRIM(indet.STYLE)
	STORE "W07"+cfd+ALLTRIM(STR(lnxqty))+cfd+"EA"+cfd+cfd+"UP"+cfd+cupc+cfd+"VN"+cfd+cstyle+csegd TO cstring
	DO cstringbreak
	nsegctr = nsegctr + 1

	STORE "N9"+cfd+"CL"+cfd+ALLTRIM(indet.COLOR)+csegd TO cstring
	DO cstringbreak
	nsegctr = nsegctr + 1

	STORE "N9"+cfd+"SZ"+cfd+ALLTRIM(indet.ID)+csegd TO cstring
	DO cstringbreak
	nsegctr = nsegctr + 1

	cline = TRIM(segmentget(@aptdet,"LINENUM",alendet))
	STORE "N9"+cfd+"LI"+cfd+ALLTRIM(cline)+csegd TO cstring
	DO cstringbreak
	nsegctr = nsegctr + 1

	cdesc = TRIM(segmentget(@aptdet,"DESC",alendet))
	STORE "G69"+cfd+cdesc+csegd TO cstring
	DO cstringbreak
	nsegctr = nsegctr + 1
ENDSCAN

WAIT WINDOW "END OF SCAN/CHECK PHASE ROUND..." NOWAIT

nstcount = 0
DO close944
lxfer944 = .T.
=FCLOSE(nfilenum)

IF USED('serfile')
	USE IN serfile
ENDIF

IF !ltesting
	IF !USED("ftpedilog")
		SELECT 0
		USE F:\edirouting\ftpedilog ALIAS ftpedilog
		INSERT INTO ftpedilog (ftpdate,filename,acct_name,TYPE) VALUES (dt2,cfilenamehold,"RAGBONE","944")
		USE IN ftpedilog
	ENDIF
ENDIF

cfin = "944 FILE "+cfilenameout+" CREATED WITHOUT ERRORS"
IF !ltesting
	SELECT edi_trigger
	REPLACE edi_trigger.processed WITH .T.,edi_trigger.created WITH .T.,edi_trigger.when_proc WITH DATETIME(),;
		edi_trigger.fin_status WITH "944 CREATED",edi_trigger.errorflag WITH .F.,file944crt WITH cfilenamehold ;
		FOR  edi_trigger.accountid = nacctnum AND edi_trigger.wo_num = nwo_num IN edi_trigger
	sendmail()
ENDIF

WAIT "RAG&BONE 944 Creation process complete:"+CHR(13)+cfilenamehold WINDOW AT 45,60 TIMEOUT 3
oFSO=CREATEOBJECT("Scripting.FileSystemObject")
oFSO.CopyFile("&cFilenamehold","&cfilenamearchive",.T.)
*copy file &cfilenamehold to &cfilenamearchive

RELEASE ALL LIKE T*
closedata()

************************************************************************
************************************************************************
*!* END OF MAIN PROGRAM
************************************************************************
************************************************************************


****************************
PROCEDURE close944
****************************
** Footer Creation

	IF nstcount = 0
*!* For 944 process only!
		STORE "W14"+cfd+ALLTRIM(STR(nsumtotqty))+;
			cfd+ALLTRIM(STR(nsumorigqty))+;
			cfd+ALLTRIM(STR(nsumorigqty-nsumtotqty))+csegd TO cstring
		DO cstringbreak
		nsegctr = nsegctr + 1
*!* End 944 addition

		STORE  "SE"+cfd+ALLTRIM(STR(nsegctr+1))+cfd+PADL(c_grpcntrlnum,9,"0")+csegd TO cstring
		FPUTS(nfilenum,cstring)
		nsegctr = 0
		nstsets = nstsets + 1
	ENDIF

	STORE  "GE"+cfd+ALLTRIM(STR(nstsets))+cfd+c_cntrlnum+csegd TO cstring
	FPUTS(nfilenum,cstring)

	STORE  "IEA"+cfd+"1"+cfd+PADL(c_cntrlnum,9,"0")+csegd TO cstring
	FPUTS(nfilenum,cstring)

ENDPROC

****************************
PROCEDURE cstringbreak
****************************
	FPUTS(nfilenum,cstring)
ENDPROC

****************************
PROCEDURE num_incr
****************************
	SELECT serfile
	REPLACE serfile.seqnum WITH serfile.seqnum + 1 IN serfile
	REPLACE serfile.grpseqnum WITH serfile.grpseqnum + 1 IN serfile
	c_cntrlnum = ALLTRIM(STR(serfile.seqnum))
	c_grpcntrlnum = ALLTRIM(STR(serfile.grpseqnum))
ENDPROC

****************************
PROCEDURE ediupdate
****************************
	PARAMETERS cfin
	IF !ltesting
		SELECT edi_trigger
		REPLACE processed WITH .T.,created WITH .F.,fin_status WITH cfin,errorflag WITH liserror,;
			when_proc WITH DATETIME(),errorflag WITH .F. ;
			FOR edi_trigger.accountid = nacctnum AND edi_trigger.wo_num = nwo_num IN edi_trigger
		IF lsendmail
			sendmail()
		ENDIF
	ENDIF
	closeout()
ENDPROC

****************************
PROCEDURE closeout
****************************
	IF !EMPTY(nfilenum)
		=FCLOSE(nfilenum)
		ERASE &cfilename
	ENDIF
	closedata()
ENDPROC

****************************
PROCEDURE closedata
****************************
	IF USED('edi_xfer')
		USE IN edi_xfer
	ENDIF
	IF USED('detail')
		USE IN DETAIL
	ENDIF
	IF USED('indet')
		USE IN indet
	ENDIF
	IF USED('inwolog')
		USE IN inwolog
	ENDIF

	ON ERROR
	WAIT CLEAR
	_SCREEN.CAPTION = "INBOUND POLLER - EDI"
	RETURN

****************************
PROCEDURE sendmail
****************************
	tfrom = "TGF EDI Operations <toll-edi-ops@tollgroup.com>"

	IF liserror
		tsendto = tsendtoerr
		tcc = tccerr
		tsubject = crbmailname+" EDI ERROR, Inv. WO "+cwo_num
	ELSE
		tsubject = crbmailname+" EDI FILE (944) Created, Inv. WO "+cwo_num
	ENDIF


	tattach = " "
	IF (ctrk_wo = "0" OR EMPTY(ctrk_wo))
		cthistrk_wo = "MISSING"
		tmessage = "Inv. WO #: "+cwo_num++CHR(10)
	ELSE
		cthistrk_wo = ctrk_wo
		tmessage = "Inv. WO #: "+cwo_num+", Trucking WO #: "+cthistrk_wo+CHR(10)
	ENDIF

	IF EMPTY(cerrmsg)
		tmessage = tmessage + cfin
	ELSE
		tmessage = tmessage + cerrmsg
	ENDIF

	IF lemail
		DO FORM m:\dev\frm\dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
	ENDIF

ENDPROC


****************************
PROCEDURE errormail
****************************
	IF ndiffqty > 0
		tnote = "n OVERAGE"
	ELSE
		tnote = " SHORTAGE"
	ENDIF
	tsendto = tsendtotest
	tcc = tccerr
	tfrom = "TGF EDI Operations <toll-edi-ops@tollgroup.com>"
	tsubject = "ERROR: BBC 944 WO# "+cwo_num
	tmessage = "The 944 for this WO was received as a"+ tnote
	tmessage = "Original Quantity per ASN: "+ALLTRIM(STR(nsumorigqty))
	tmessage = "Quantity As Received: "+ALLTRIM(STR(nsumtotqty))
	SET STEP ON 
	DO FORM m:\dev\frm\dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
ENDPROC

****************************
PROCEDURE segmentget
****************************
	PARAMETER thisarray,lckey,nlength

	FOR i = 1 TO nlength
		IF i > nlength
			EXIT
		ENDIF
		lnend= AT("*",thisarray[i])
		IF lnend > 0
			lcthiskey =TRIM(SUBSTR(thisarray[i],1,lnend-1))
			IF OCCURS(lckey,lcthiskey)>0
				RETURN SUBSTR(thisarray[i],lnend+1)
				i = 1
			ENDIF
		ENDIF
	ENDFOR

	RETURN ""
