utilsetup("MJ_RETURNS_WKLY")
Set Exclusive Off
Set Deleted On
Set Talk Off
Set Safety Off   &&Allows for overwrite of existing files

Close Databases All
**********************************************MONTHLY
If Substr(Dmy(Date()),1,2) = '01'

	xmonth=Month(Date())-1
	xcmonth=Cmonth(Date())
	xyear=Year(Date())
	If xmonth=0
		xmonth=12
		xyear=Year(Date())-1
	Else
	Endif

goffice='J'
useca("inwolog","wh",,,"mod='"+goffice+"' and  accountid in (6303,6325,6543) and confirmdt>{"+dtoc(date()-60)+"}")
useca("indet","wh",,,"mod='"+goffice+"' and  accountid in (6303,6325,6543) and date_rcvd>{"+dtoc(date()-60)+"}")

useca('mjraret','wh',.t.)
**	Use F:\wh\MJ_RA_RETURN.Dbf In 0

useca("cartret","wh",.t.,,,,"mj_carton_returns")

Select 'NJ' As warehouse, a.accountid, Reference As RA,unbilledcomment As cartonid,Style,Color,Id,totqty,Dtoc(confirmdt) As wms_date From inwolog a, indet b Where a.inwologid=b.inwologid And  zreturns And Reference='10-'  And Month(confirmdt)=xmonth And Year(confirmdt)=xyear Into Cursor NJ1 Readwrite
Select a.*, date_rcvd From NJ1 a Left Join mj_carton_returns b On a.cartonid=b.carton_id Into Cursor nj2 Readwrite
Use In inwolog
Use In indet

goffice='L'
useca("inwolog","wh",,,"mod='"+goffice+"' and  accountid in (6303,6325,6543) and confirmdt>{"+dtoc(date()-60)+"}")
useca("indet","wh",,,"mod='"+goffice+"' and  accountid in (6303,6325,6543) and date_rcvd>{"+dtoc(date()-60)+"}")

	Select 'CA' As warehouse, a.accountid, Reference As RA,unbilledcomment As cartonid,Style,Color,Id,totqty,Dtoc(confirmdt) As wms_date From inwolog a, indet b Where a.inwologid=b.inwologid And  zreturns And Reference='10-'  AND INLIST(a.accountid,6303,6325) And Month(confirmdt)=xmonth And Year(confirmdt)=xyear Into Cursor CA1 Readwrite
	Select a.*, date_rcvd From CA1 a Left Join mj_carton_returns b On a.cartonid=b.carton_id Into Cursor CA2 Readwrite
	Select * From nj2 Union All Select *From CA2 Into Cursor TOT1 Readwrite
	SELECT distinct(ra) as ra, consignee  FROM MJRARET INTO CURSOR vMJ_RA_RETURN readwrite
	Select warehouse,accountid As ACCOUNT,a.RA As RA, CONSIGNEE,cartonid, Style,Color,Id,totqty As QTY, wms_date,DTOC(date_rcvd) as date_rcvd From TOT1 a Left Join vMJ_RA_RETURN b On a.RA=b.RA Into Cursor FINAL1 Readwrite
	Replace ACCOUNT With 1000 For ACCOUNT =6303 And warehouse='NJ'
	Replace ACCOUNT With 1200 For ACCOUNT =6304 And warehouse='NJ'
	Replace ACCOUNT With 1320 For ACCOUNT =6305 And warehouse='NJ'
	Replace ACCOUNT With 1310 For ACCOUNT =6320 And warehouse='NJ'
	Replace ACCOUNT With 1221 For ACCOUNT =6321 And warehouse='NJ'
	Replace ACCOUNT With 1222 For ACCOUNT =6322 And warehouse='NJ'
	Replace ACCOUNT With 1223 For ACCOUNT =6323 And warehouse='NJ'
	Replace ACCOUNT With 1250 For ACCOUNT =6324 And warehouse='NJ'
	Replace ACCOUNT With 1151 For ACCOUNT =6325 And warehouse='NJ'
	Replace ACCOUNT With 1400 For ACCOUNT =6364 And warehouse='NJ'
	Replace ACCOUNT With 1600 For ACCOUNT =6543 And warehouse='NJ'
	Replace ACCOUNT With 1001 For ACCOUNT =6303 And warehouse='CA'
	Replace ACCOUNT With 1152 For ACCOUNT =6325 And warehouse='CA'
	replace ra WITH SUBSTR(ra,4,15) FOR INLIST(ra,'10-6','10-7')
	Export To S:\MarcJacobsData\TEMP\mj_returns_mthly Xls
	If Reccount() > 0
**	tsendto = "tmarg@fmiint.com,e.phillips@marcjacobs.com,J.HALPIN@marcjacobs.com,E.BARNES@marcjacobs.com,o.batista@marcjacobs.com"
		tsendto =  "tmarg@fmiint.com,v.gil@marcjacobs.com"
		tattach = "S:\MarcJacobsData\TEMP\mj_returns_mthly.xls"
		tcc =""
		tFrom ="Toll EDI System Operations <fmi-transload-ops@fmiint.com>"
		tmessage = "MJ RETURNS MONTHLY "
		tSubject = "MJ RETURNS MONTHLY"
		Do Form m:\dev\frm\dartmail2 With tsendto,tFrom,tSubject, tcc,tattach,tmessage  ,"A"

	Else


	Endif

Endif
**********************************************WEEKLY
SET STEP ON 
If Dow(Date())=6
goffice='J'
useca("inwolog","wh",,,"mod='"+goffice+"' and  accountid in (6303,6325,6543) and confirmdt>{"+dtoc(date()-60)+"}")
useca("indet","wh",,,"mod='"+goffice+"' and  accountid in (6303,6325,6543) and date_rcvd>{"+dtoc(date()-60)+"}")

	useca('mjraret','wh',.t.)   &&&& >=Date()-4

	useca("cartret","wh",.t.,,,,"mj_carton_returns")

	Select 'NJ' As warehouse, a.accountid, Reference As RA,unbilledcomment As cartonid,Style,Color,Id,totqty,Dtoc(confirmdt) As wms_date From inwolog a, indet b Where a.inwologid=b.inwologid And  zreturns And Reference='10-'  And (confirmdt)>=Date()-4 Into Cursor NJ1 Readwrite
	Select a.*, date_rcvd From NJ1 a Left JOIN mj_carton_returns b On a.cartonid=b.carton_id Into Cursor nj2 Readwrite

	Use In inwolog
	Use In indet
goffice='L'
useca("inwolog","wh",,,"mod='"+goffice+"' and  accountid in (6303,6325,6543) and confirmdt>{"+dtoc(date()-60)+"}")
useca("indet","wh",,,"mod='"+goffice+"' and  accountid in (6303,6325,6543) and date_rcvd>{"+dtoc(date()-60)+"}")

	Select 'CA' As warehouse, a.accountid, Reference As RA,unbilledcomment As cartonid,Style,Color,Id,totqty,Dtoc(confirmdt) As wms_date From inwolog a, indet b Where a.inwologid=b.inwologid And  zreturns And Reference='10-'  And (confirmdt)>=Date()-4 Into Cursor CA1 Readwrite
	Select a.*, date_rcvd From CA1 a Left Join mj_carton_returns b On a.cartonid=b.carton_id Into Cursor CA2 Readwrite
	Select * From nj2 Union All Select *From CA2 Into Cursor TOT1 Readwrite
	SELECT distinct(ra) as ra, consignee  FROM mjraret INTO CURSOR vMJ_RA_RETURN readwrite
	Select warehouse,accountid As ACCOUNT,a.RA As RA, CONSIGNEE,cartonid, Style,Color,Id,totqty As QTY, wms_date,DTOC(date_rcvd) as date_rcvd From TOT1 a Left Join vMJ_RA_RETURN b On a.RA=b.RA Into Cursor FINAL1 Readwrite
	Replace ACCOUNT With 1000 For ACCOUNT =6303 And warehouse='NJ'
	Replace ACCOUNT With 1200 For ACCOUNT =6304 And warehouse='NJ'
	Replace ACCOUNT With 1320 For ACCOUNT =6305 And warehouse='NJ'
	Replace ACCOUNT With 1310 For ACCOUNT =6320 And warehouse='NJ'
	Replace ACCOUNT With 1221 For ACCOUNT =6321 And warehouse='NJ'
	Replace ACCOUNT With 1222 For ACCOUNT =6322 And warehouse='NJ'
	Replace ACCOUNT With 1223 For ACCOUNT =6323 And warehouse='NJ'
	Replace ACCOUNT With 1250 For ACCOUNT =6324 And warehouse='NJ'
	Replace ACCOUNT With 1151 For ACCOUNT =6325 And warehouse='NJ'
	Replace ACCOUNT With 1400 For ACCOUNT =6364 And warehouse='NJ'
	Replace ACCOUNT With 1600 For ACCOUNT =6543 And warehouse='NJ'
	Replace ACCOUNT With 1001 For ACCOUNT =6303 And warehouse='CA'
	Replace ACCOUNT With 1152 For ACCOUNT =6325 And warehouse='CA'
	replace ra WITH SUBSTR(ra,4,15) FOR INLIST(ra,'10-6','10-7')
	Export To S:\MarcJacobsData\TEMP\mj_returns_weekly Xls
	If Reccount() > 0
**	tsendto = "tmarg@fmiint.com,e.phillips@marcjacobs.com,J.HALPIN@marcjacobs.com,E.BARNES@marcjacobs.com,o.batista@marcjacobs.com"
		tsendto =  "tmarg@fmiint.com,v.gil@marcjacobs.com"
		tattach = "S:\MarcJacobsData\TEMP\mj_returns_weekly.xls"
		tcc =""
		tFrom ="Toll EDI System Operations <fmi-transload-ops@fmiint.com>"
		tmessage = "MJ RETURNS WEEKLY "
		tSubject = "MJ RETURNS WEEKLY"
		Do Form m:\dev\frm\dartmail2 With tsendto,tFrom,tSubject, tcc,tattach,tmessage  ,"A"

	Else


	Endif
ENDIF





**********************************************YTD
SET STEP ON 
If Dow(Date())=6
goffice='J'
useca("inwolog","wh",,,"mod='"+goffice+"' and  accountid in (6303,6325,6543) and zreturns=.t. and confirmdt>{01/01/2017}")
useca("indet","wh",,,"mod='"+goffice+"' and  accountid in (6303,6325,6543) and date_rcvd>{01/01/2017}")

	useca('mjraret','wh',.t.)   &&&& >=Date()-4

	useca("cartret","wh",.t.,,,,"mj_carton_returns")

	Select 'NJ' As warehouse, a.accountid, Reference As RA,unbilledcomment As cartonid,Style,Color,Id,totqty, ttod(confirmdt) As wms_date From inwolog a, indet b Where a.inwologid=b.inwologid And  zreturns And Reference='10-'  And (confirmdt)>={01/01/2017} Into Cursor NJ1 Readwrite
	Select a.*, date_rcvd From NJ1 a Left JOIN mj_carton_returns b On a.cartonid=b.carton_id Into Cursor nj2 Readwrite

	Use In inwolog
	Use In indet
goffice='L'
useca("inwolog","wh",,,"mod='"+goffice+"' and  accountid in (6303,6325,6543) and zreturns=.t. and confirmdt>{01/01/2017}")
useca("indet","wh",,,"mod='"+goffice+"' and  accountid in (6303,6325,6543) and date_rcvd>{01/01/2017}")

	Select 'CA' As warehouse, a.accountid, Reference As RA,unbilledcomment As cartonid,Style,Color,Id,totqty, ttod(confirmdt) As wms_date From inwolog a, indet b Where a.inwologid=b.inwologid And  zreturns And Reference='10-'  And (confirmdt)>={01/01/2017} Into Cursor CA1 Readwrite
	Select a.*, date_rcvd From CA1 a Left Join mj_carton_returns b On a.cartonid=b.carton_id Into Cursor CA2 Readwrite
	Select * From nj2 Union All Select *From CA2 Into Cursor TOT1 Readwrite
	SELECT distinct(ra) as ra, consignee  FROM mjraret INTO CURSOR vMJ_RA_RETURN readwrite
	Select warehouse,accountid As ACCOUNT,a.RA As RA, CONSIGNEE,cartonid, Style,Color,Id,totqty As QTY, wms_date, date_rcvd From TOT1 a Left Join vMJ_RA_RETURN b On a.RA=b.RA Into Cursor FINAL1 Readwrite
	Replace ACCOUNT With 1000 For ACCOUNT =6303 And warehouse='NJ'
	Replace ACCOUNT With 1200 For ACCOUNT =6304 And warehouse='NJ'
	Replace ACCOUNT With 1320 For ACCOUNT =6305 And warehouse='NJ'
	Replace ACCOUNT With 1310 For ACCOUNT =6320 And warehouse='NJ'
	Replace ACCOUNT With 1221 For ACCOUNT =6321 And warehouse='NJ'
	Replace ACCOUNT With 1222 For ACCOUNT =6322 And warehouse='NJ'
	Replace ACCOUNT With 1223 For ACCOUNT =6323 And warehouse='NJ'
	Replace ACCOUNT With 1250 For ACCOUNT =6324 And warehouse='NJ'
	Replace ACCOUNT With 1151 For ACCOUNT =6325 And warehouse='NJ'
	Replace ACCOUNT With 1400 For ACCOUNT =6364 And warehouse='NJ'
	Replace ACCOUNT With 1600 For ACCOUNT =6543 And warehouse='NJ'
	Replace ACCOUNT With 1001 For ACCOUNT =6303 And warehouse='CA'
	Replace ACCOUNT With 1152 For ACCOUNT =6325 And warehouse='CA'
	replace ra WITH SUBSTR(ra,4,15) FOR INLIST(ra,'10-6','10-7')
	Export To S:\MarcJacobsData\Reports\Returns_inventory\mj_returns_ytd Xls
	If Reccount() > 0
**	tsendto = "tmarg@fmiint.com,e.phillips@marcjacobs.com,J.HALPIN@marcjacobs.com,E.BARNES@marcjacobs.com,o.batista@marcjacobs.com"
		tsendto =  "tmarg@fmiint.com,v.gil@marcjacobs.com"
*		tattach = "S:\MarcJacobsData\TEMP\mj_returns_weekly.xls"
		tcc =""
		tFrom ="Toll EDI System Operations <fmi-transload-ops@fmiint.com>"
		tmessage = "MJ RETURNS YTD is complete and available in S:\MarcJacobsData\Reports\Returns_inventory "
		tSubject = "MJ RETURNS YTD is complete"
		Do Form m:\dev\frm\dartmail2 With tsendto,tFrom,tSubject, tcc,tattach,tmessage  ,"A"

	Else


	Endif
Endif


schedupdate()
_Screen.Caption=gscreencaption
On Error


