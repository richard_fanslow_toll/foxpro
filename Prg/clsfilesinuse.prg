* FilesInUse Class
*-- For Windows NT Platform (NT 4, NT 2000)
*
* Modified version of Paul Vlad Tatavu file:
* Who opened what files on the network?
*
* This one doesn't use the Pointers.OCX or Pointers.VCX
* It's only Visual Foxpro Code.
*
*--
* The following program displays the open files, the users that
* opened these files and other related information.
*
* This code detects only the files opened using a net shared
* path. It does not return the files opened by a user on the
* local computer using a local path (i.e. the computer where
* the user is logged on). This is normal, because, otherwise,
* the number of returned files would be huge.
*
* The user running this program must be a member of the
* Administrators or Account Operators local group.
*
* In order to keep the code simple, the error handling only
* displays the error code. You should integrate it in your
* error handling mechanism.
*
*-- This function returns information about open files.
* It returns the open files only if they were
* opened using a share on that computer.
*
*-- It uses:
* - The NetFileEnum Win32 API function
* to retrieve the wanted information from the OS.
*
*-- Parameters:
* 1. The server name. It can also be a domain name.
* Use an empty string for the current computer.
* Ex: MyServer
* 2. The base path. Only the files in this directory
* (and all subdirectories) will be returned.
* This path must be specified as the local path
* on the computer where the files were opened, and
* not as the UNC path.
* 3. The user name.
* Use an empty string for all users.
*--

#DEFINE VER_PLATFORM_WIN32S 0
#DEFINE VER_PLATFORM_WIN32_WINDOWS 1
#DEFINE VER_PLATFORM_WIN32_NT 2


DEFINE CLASS clsFilesInUse AS CUSTOM


	FUNCTION INIT

		DECLARE INTEGER NetFileEnum IN NETAPI32 ;
			STRING @SERVERNAME, STRING @BasePath, ;
			STRING @UserName, INTEGER nLevel, ;
			INTEGER @BufferPointer, INTEGER PreferedMaxLength, ;
			INTEGER @EntriesRead, INTEGER @TotalEntries, ;
			INTEGER @ResumeHandle

		DECLARE INTEGER NetApiBufferFree IN NETAPI32 ;
			INTEGER Pointer

		*-- This is the structure used by NetFileEnum
		* to retrieve the information.
		*typedef struct _FILE_INFO_3 {
		* DWORD fi3_id;
		* DWORD fi3_permissions;
		* DWORD fi3_num_locks;
		* LPWSTR fi3_pathname;
		* LPWSTR fi3_username;} FILE_INFO_3

		DECLARE RtlMoveMemory IN Kernel32 STRING@ DEST, INTEGER src, INTEGER nsize

		DECLARE RtlMoveMemory IN Kernel32 STRING@ DEST, INTEGER src, INTEGER nsize

		DECLARE INTEGER lstrlenW IN Kernel32 INTEGER src

		DECLARE GetVersionEx IN win32api STRING @OSVERSIONINFO

		RETURN DODEFAULT()

	ENDFUNC



	PROCEDURE FindFilesInUse

		LPARAMETERS tcServerName, tcBasePath, tcUserName

		WAIT WINDOW "Processing, please wait..." NOWAIT

		LOCAL lnMajor, lnMinor, lnPlatform, lnBuild
		LOCAL lcPermissions, lcErrorMessage

		WITH THIS

			.GetOSVersion(@lnMajor, @lnMinor, @lnPlatform, @lnBuild)
			IF lnPlatform # VER_PLATFORM_WIN32_NT
				=MESSAGEBOX('Requires Windows NT',0 + 16)
				RETURN
			ENDIF

			IF TYPE('tcServerName')#'C' .OR. TYPE('tcBasePath')#'C' .OR. TYPE('tcUserName')#'C'
				=MESSAGEBOX('Invalid Parameters',0 + 16)
				RETURN
			ENDIF

			LOCAL lcServerName, lcBasePath, lcUserName, lnBufferPointer
			LOCAL lnPreferedMaxLength, lnEntriesRead, lnTotalEntries
			LOCAL lnResumeHandle, lnError
			LOCAL loPointersObject
			LOCAL lnI, lcDll, lnPermissions, lnID
			LOCAL llContinue, lnpFileInfo, lcFileName
			LOCAL lnLocks, lcUserName

			*!*	DECLARE INTEGER NetFileEnum IN NETAPI32 ;
			*!*		STRING @SERVERNAME, STRING @BasePath, ;
			*!*		STRING @UserName, INTEGER nLevel, ;
			*!*		INTEGER @BufferPointer, INTEGER PreferedMaxLength, ;
			*!*		INTEGER @EntriesRead, INTEGER @TotalEntries, ;
			*!*		INTEGER @ResumeHandle

			*!*	DECLARE INTEGER NetApiBufferFree IN NETAPI32 ;
			*!*		INTEGER Pointer

			*!*	*-- This is the structure used by NetFileEnum
			*!*	* to retrieve the information.
			*!*	*typedef struct _FILE_INFO_3 {
			*!*	* DWORD fi3_id;
			*!*	* DWORD fi3_permissions;
			*!*	* DWORD fi3_num_locks;
			*!*	* LPWSTR fi3_pathname;
			*!*	* LPWSTR fi3_username;} FILE_INFO_3

			*-- The server name, the base path and the user name
			* must be in Unicode format.
			lcServerName = STRCONV(STRCONV(tcServerName + CHR(0), 1), 5)
			lcBasePath = STRCONV(STRCONV(tcBasePath + CHR(0), 1), 5)
			lcUserName = STRCONV(STRCONV(tcUserName + CHR(0), 1), 5)

			*-- Allow for a very large buffer.
			* If this length is not enough, the info
			* will be retrieved in more than one step.
			lnPreferedMaxLength = 100000000

			lnResumeHandle = 0
			lnEntriesRead = 0
			lnTotalEntries = 0
			lnBufferPointer = 0

			llContinue = .T.
			DO WHILE llContinue
				lnError = NetFileEnum( lcServerName, lcBasePath, lcUserName, 3, ;
					@lnBufferPointer, lnPreferedMaxLength, @lnEntriesRead, ;
					@lnTotalEntries, @lnResumeHandle)
				IF lnEntriesRead = 0
					*-- There are no (more) open files.
					llContinue = .F.
				ENDIF
				IF lnError = 0
					FOR lnI = 1 TO lnEntriesRead
						lnpFileInfo = lnBufferPointer + (lnI - 1) * 20

						*-- Extract the file name
						lcFileName = .GetMemoryLPString(lnpFileInfo + 12)

						*-- Extract the number of locks on this file
						lnLocks = .GetMemoryDWORD(lnpFileInfo + 8)

						*-- Extract the user name that opened the file
						lcUserName = .GetMemoryLPString(lnpFileInfo + 16)

						*-- Extract the permissions on this file
						lnPermissions = .GetMemoryDWORD( lnpFileInfo + 4)

						lcPermissions = ''

						IF BITTEST(lnPermissions, 0)
							lcPermissions = lcPermissions + "Read "
						ENDIF
						IF BITTEST(lnPermissions, 1)
							lcPermissions = lcPermissions + "Write "
						ENDIF
						IF BITTEST(lnPermissions, 2)
							lcPermissions = lcPermissions + "Create "
						ENDIF
						IF BITTEST(lnPermissions, 3)
							lcPermissions = lcPermissions + "Execute "
						ENDIF
						IF BITTEST(lnPermissions, 4)
							lcPermissions = lcPermissions + "Delete "
						ENDIF
						IF BITTEST(lnPermissions, 5)
							lcPermissions = lcPermissions + "ChangeAttributes "
						ENDIF
						IF BITTEST(lnPermissions, 6)
							lcPermissions = lcPermissions + "ChangeACL "
						ENDIF

						*-- Extract the ID for this file.
						* This ID is generated when the file is opened and
						* can be used as parameter for the NetFileGetInfo
						* Win32 API function.
						lnID = .GetMemoryDWORD(lnpFileInfo)

						SELECT curFilesInUse
						APPEND BLANK
						REPLACE cFileName WITH lcFileName, ;
							nNumLocks WITH lnLocks, ;
							cUser WITH lcUserName, ;
							cPermissions WITH lcPermissions, ;
							nNetInfoID WITH lnID

					ENDFOR

					*-- Free the memory allocated by NetFileEnum
					IF lnBufferPointer <> 0
						NetApiBufferFree(lnBufferPointer)
					ENDIF

				ELSE
					lcErrorMessage = .getErrorDescription(lnError)
					*=MESSAGEBOX("Error #: " + TRANSFORM(lnError),0 + 16)
					=MESSAGEBOX(lcErrorMessage,0 + 16,"Files In Use Utility")
					llContinue = .F.
				ENDIF
			ENDDO

		ENDWITH  && THIS

		SELECT curFilesInUse
		GOTO TOP

		WAIT CLEAR

		RETURN

	ENDPROC


	FUNCTION GetMemoryDWORD
		LPARAMETER m.nPointer
		LOCAL m.cstrDWORD

		*DECLARE RtlMoveMemory IN Kernel32 STRING@ DEST, INTEGER src, INTEGER nsize
		m.cstrDWORD = REPLICATE(CHR(0), 4)
		RtlMoveMemory(@m.cstrDWORD, m.nPointer, 4)
		RETURN THIS.str2long(m.cstrDWORD)
	ENDFUNC


	FUNCTION GetMemoryLPString
		LPARAMETER m.nPointer, m.nPlatform
		LOCAL m.cResult, m.nLPStr, m.nslen

		m.nLPStr = THIS.GetMemoryDWORD(m.nPointer)

		*DECLARE RtlMoveMemory IN Kernel32 STRING@ DEST, INTEGER src, INTEGER nsize
		*DECLARE INTEGER lstrlenW IN Kernel32 INTEGER src

		m.nslen = lstrlenW(m.nLPStr) * 2
		m.cResult = REPLICATE(CHR(0), m.nslen)
		RtlMoveMemory(@m.cResult, m.nLPStr, m.nslen)
		m.cResult = STRCONV(STRCONV(m.cResult, 6), 2)

		RETURN m.cResult
	ENDFUNC


	FUNCTION GetOSVersion
		LPARAMETER m.major, m.minor, m.platform, m.build
		LOCAL m.osversion

		*DECLARE GetVersionEx IN win32api STRING @OSVERSIONINFO
		WITH THIS
			* The OSVERSIONINFO structure is 148 bytes long.
			m.osversion = .long2str(148) + REPLICATE(CHR(0), 144)
			=GetVersionEx(@m.osversion)
			m.major = .str2long(SUBSTR(m.osversion, 5, 4))
			m.minor = .str2long(SUBSTR(m.osversion, 9, 4))
			m.build = .str2long(SUBSTR(m.osversion, 13, 4))
			m.platform = .str2long(SUBSTR(m.osversion, 17, 4))
			m.build = BITAND(m.build, 0xFFFF)
		ENDWITH
	ENDFUNC


	FUNCTION long2str
		LPARAMETER m.nLong

		m.nLong = INT(m.nLong)
		RETURN (CHR(BITAND(m.nLong,255)) + ;
			CHR(BITAND(BITRSHIFT(m.nLong, 8), 255)) + ;
			CHR(BITAND(BITRSHIFT(m.nLong, 16), 255)) + ;
			CHR(BITAND(BITRSHIFT(m.nLong, 24), 255)))
	ENDFUNC


	FUNCTION str2long
		LPARAMETER m.cpLong
		RETURN (BITLSHIFT(ASC(SUBSTR(m.cpLong, 4, 1)), 24) + ;
			BITLSHIFT(ASC(SUBSTR(m.cpLong, 3, 1)), 16) + ;
			BITLSHIFT(ASC(SUBSTR(m.cpLong, 2, 1)), 8) + ;
			ASC(SUBSTR(m.cpLong, 1, 1)))
	ENDFUNC

	FUNCTION getErrorDescription
		LPARAMETERS tnErrorCode
		LOCAL lcErrorDescription
		DO CASE
			CASE tnErrorCode = 5
				lcErrorDescription = "Access Denied - you do not have access to the requested information."
			CASE tnErrorCode = 8
				lcErrorDescription = "Not Enough Memory - Insufficient memory is available."
			CASE tnErrorCode = 122
				lcErrorDescription = "Buffer Too Small - The supplied buffer is too small."
			CASE tnErrorCode = 124
				lcErrorDescription = "Invalid Level - The value specified for the level parameter is invalid."
			CASE tnErrorCode = 234
				lcErrorDescription = "More Data - More entries are available. Specify a large enough buffer to receive all entries."
			OTHERWISE
				lcErrorDescription = "Unidentified Error"
		ENDCASE lcErrorDescription
		lcErrorDescription = "Error Code = " + ALLTRIM(STR(tnErrorCode)) + ": " + lcErrorDescription
		RETURN 	
	ENDFUNC

ENDDEFINE

