lparameter lcoffice, xaccountid, xtest

lcoffice ="Y"
xaccountid = "6650,6651,6652"
close data all

do m:\dev\prg\_setvars with .f.

gsystemmodule="wh"

*utilsetup("SENDINVENTORY")
xscreencaption=_screen.caption
_screen.caption="SENDINVENTORY for Account: "+xaccountid+" and Office: "+lcoffice

guserid="SENDINVENTORY"
gprocess=guserid
gdevelopment=.f.

xmod="Y"
goffice="Y"

useca("upcmast","wh",,,[select * from upcmast where accountid In (6650,6651,6652)])
select upcmast

copy to h:\fox\tempupcmast
use in upcmast

use h:\fox\tempupcmast in 0 alias upcmast exclusive
select upcmast
index on style+color+id tag style


xaccountid = val(xaccountid)
lcwhpath="F:\WHY\whdata\"

if !xtest
	use (lcwhpath+"outship") in 0 order acctpt
	use (lcwhpath+"outdet") in 0 order outshipid

	xsqlexec("select * from inven where mod='Y'",,,"wh")

else
	use f:\tmptmp\paulg\inven in 0
	use f:\tmptmp\paulg\outship in 0
	use f:\tmptmp\paulg\outdet in 0
	use f:\tmptmp\paulg\project in 0
endif

* get inventory

select space(25) as acctname,accountid,units, style, color, id, space(15) as upc, availqty, allocqty, holdqty, 0000000 as pickedqty, 0000000 as spqty ;
from inven where inlist(accountid,6650,6651,6652) and units into cursor inventory readwrite

index on transform(units)+style+color+id tag match


* inprocess is a cursor of all outship records where they are not yet delivered and also excluding "notonwip"

if usesqloutwo()
	xsqlexec("select cast(' ' as char(25)) as acctname, * from outship " + ;
		"where mod='"+goffice+"' and inlist(accountid,6650,6651,6652) " + ;
		"and del_date={} and notonwip=0 and qty#0 order by wo_num","inprocess",,"wh")

	xsqlexec("select cast(' ' as char(25)) as acctname, units, style, color, " + ;
		"id, pack, totqty from outship, outdet " + ;
		"where outship.outshipid=outdet.outshipid " + ;
		"and outship.mod='"+goffice+"' and inlist(outship.accountid,6650,6651,6652) " + ;
		"and del_date={} and notonwip=0 and qty#0","xdy",,"wh") 
		
	select units, style, color, id, pack, sum(totqty) as pickedqty ;
		from xdy group by units, style, color, id, pack into cursor xunshipped
		
else
	select space(25) as acctname,* from outship ;
		where inlist(accountid,6650,6651,6652) and emptynul(del_date) and !notonwip and qty#0 ;
		order by wo_num into cursor inprocess readwrite

	select space(25) as acctname, outdet.accountid,units, style, color, id, pack,upc, sum(totqty) as pickedqty ;
		from outdet, inprocess ;
		where outdet.outshipid= inprocess.outshipid ;
		group by outdet.accountid,units, style, color, id, pack, upc ;
		into cursor xunshipped readwrite
endif

select xunshipped
scan
	=seek(transform(units)+style+color+id,"inventory","match")
	replace pickedqty with xunshipped.pickedqty in inventory
endscan

select inventory
delete for availqty=0 and pickedqty=0 and holdqty=0 and spqty=0

select inventory
scan
	select upcmast
	=seek(inventory.style+inventory.color+inventory.id,"upcmast","style")
	if found("upcmast")
		replace inventory.upc with upcmast.upc
	else
		replace inventory.upc with "UNK"
	endif
endscan

select inprocess
scan
	do case
	case accountid = 6650
		replace acctname with "Q4 DESIGN"
	case accountid = 6651
		replace acctname with "Q4 KIDS"
	case accountid = 6652
		replace acctname with "TAYLOR FASHION"
	endcase
endscan

select xunshipped
scan
	do case
	case accountid = 6650
		replace acctname with "Q4 DESIGN"
	case accountid = 6651
		replace acctname with "Q4 KIDS"
	case accountid = 6652
		replace acctname with "TAYLOR FASHION"
	endcase
endscan

select inventory
scan
	do case
	case accountid = 6650
		replace acctname with "Q4 DESIGN"
	case accountid = 6651
		replace acctname with "Q4 KIDS"
	case accountid = 6652
		replace acctname with "TAYLOR FASHION"
	endcase
endscan


set step on

lcfilename  = "f:\ftpusers\samsung\846out\"+"samsung_"+lcoffice+"_inventory"+ttoc(datetime(),1)+".xls"
lcfilename2 = "f:\ftpusers\samsung\846out\"+"samsung_"+lcoffice+"_unshipped_"+ttoc(datetime(),1)+".xls"
lcfilename3 = "f:\ftpusers\samsung\846out\"+"samsung_"+lcoffice+"_inproccess_"+ttoc(datetime(),1)+".xls"

select inventory
export to &lcfilename fields acctname,accountid,upc,style,color,id, availqty,pickedqty type xls

select xunshipped
export to &lcfilename2 fields acctname,accountid,upc,style,color,id,pickedqty type xls

select inprocess
export to &lcfilename3 fields acctname,accountid,wo_num,ship_ref,ptdate,wo_date,called,appt,pulleddt,picked,labeled,staged,appt_num,appt_time,apptremarks type xls


tmessage = "Updated inventory file in the 2XU FTP Site.............."+chr(13)+chr(13)+;
"This email is generated via an automated process"+chr(13)+"For changes in content or distribution please contact: Paul Gaidis @ 732-750-9000x143 "
tsubject= "Samsung Inventory Sent: " +ttoc(datetime())

tattach = lcfilename+";"+lcfilename2+";"+lcfilename3
tsendto = 'pgaidis@fmiint.com'
tfrom ="TGF Corporate <TGFSupplyChain@fmiint.com>"

if !empty(tattach)
	do form dartmail2 with tsendto,tfrom,tsubject,"",tattach,tmessage,"A"
endif

schedupdate()

on error
_screen.caption=xscreencaption
