SET EXCLUSIVE OFF 
SET SAFETY OFF 
ldCurrentTime = DATE()+3600*24
ldGracePeriod = datetime()-3600*24

lcTimeError = "No 997 for: "+ CHR(13)
lcRejectError = "Rej Error: "+ CHR(13)

xsqlexec("select * from ackdata where inlist(accountid,6221,6521) and edicode='SW' and processed=1 and rcvdt#{} and accepted=0","rejects",,"wh")

SELECT Rejects

IF reccount() > 0

  EXPORT TO h:\fox\bcny_Rejects_errors.xls type xls

  tsendto ="maria.estrella@tollgroup.com,alma.navarro@tollgroup.com,todd.margolin@tollgroup.com"
  tfrom = "Toll EDI Operations <toll-edi-ops@tollgroup.com>"
  tSubject = "BCNY 997 Rejection ERRORS"
  tcc =""
  tattach = "h:\fox\bcny_Rejects_errors.xls"
  tmessage= lcRejectError
CD  m:\dev\frm\
  DO FORM dartmail2 WITH tsendto,tfrom,tSubject,tcc,tattach,tmessage,"A"
ENDIF 

IF RECCOUNT() = 0
  
  tsendto ="maria.estrella@tollgroup.com,alma.navarro@tollgroup.com,todd.margolin@tollgroup.com"
  tfrom = "Toll EDI Operations <toll-edi-ops@tollgroup.com>"
  tSubject = "NO BCNY 997 Rejection ERRORS"
  tcc =""
  tattach = ""
  tmessage= "All good"
CD  m:\dev\frm\
  DO FORM dartmail2 WITH tsendto,tfrom,tSubject,tcc,tattach,tmessage,"A"

ENDIF 

xsqlexec("select * from ackdata where inlist(accountid,6221,6521) and edicode='SW' and rcvdt={} " + ;
	"and processed=0 and (loadtime>{08/21/2014} and loadtime<{"+dtoc(ldgraceperiod)+"})","timestatus",,"wh")

SELECT TimeStatus

IF reccount() > 0
  
  EXPORT TO h:\fox\bcny_time_errors.xls TYPE XLS

  tsendto ="maria.estrella@tollgroup.com,alma.navarro@tollgroup.com,todd.margolin@tollgroup.com"
  tfrom = "Toll EDI Operations <toll-edi-ops@tollgroup.com>"
  tSubject = "BCNY 997 Reconciliation ERRORS"
  tcc =""
  tattach = "h:\fox\bcny_time_Errors.xls"
  tmessage= lcTimeError
CD  m:\dev\frm\
  DO FORM dartmail2 WITH tsendto,tfrom,tSubject,tcc,tattach,tmessage,"A"
ENDIF 
IF reccount() = 0

  tsendto ="maria.estrella@tollgroup.com,alma.navarro@tollgroup.com,todd.margolin@tollgroup.com"
  tfrom = "Toll EDI Operations <toll-edi-ops@tollgroup.com>"
  tSubject = "NO BCNY 997 Reconciliation ERRORS"
  tcc =""
  tattach = ""
  tmessage= "All good"
CD  m:\dev\frm\
  DO FORM dartmail2 WITH tsendto,tfrom,tSubject,tcc,tattach,tmessage,"A"
ENDIF
RETURN
