PARAMETERS lCurrday

utilsetup("NANJING_LASTDEL")

CLOSE DATABASES ALL
CLEAR
DO m:\dev\prg\_setvars WITH .T.
SET RESOURCE OFF

ON ESCAPE CANCEL
PUBLIC lTesting,cFilenameOut,lNoRecsMail,nDaysback,j

lTesting = .t.

lNoRecsMail = .F.

nDaysback = 5

IF lTesting
*	nDaysback =1
ENDIF

cDateBack = DTOC(DATE()-nDaysback)
WAIT WINDOW "DATE OF REPORT: "+cDateBack TIMEOUT 3

DELETE FILE ("c:\tempfox\nanjing_shipped\*.xls")
DELETE FILE ("c:\tempfox\adny_shipped\*.xls")

cFilenameOut = ("c:\tempfox\nanjing_shipped"+TTOC(DATETIME(),1)+".xls")
cFilenameOut_a = ("c:\tempfox\adny_shipped"+TTOC(DATETIME(),1)+".xls")

*ASSERT .F.

lDoMail = .T.

WAIT WINDOW "Number of days back to search: "+ALLTRIM(STR(nDaysback)) TIMEOUT 2
FOR j = 1 TO 3
	CLOSETABLES()
	DO CASE
		CASE j = 1
			goffice = "Y"
			xsqlexec("select * from outship where mod = 'Y' and del_date = {"+dtoc(DATE()-nDaysback)+"} and accountid in (4610,4694)",,,"wh")
			cSfrom_loc="2"
			cWhse = "C2"
		CASE j = 2
			goffice = "I"
			xsqlexec("select * from outship where mod = 'I' and del_date = {"+dtoc(DATE()-nDaysback)+"} and accountid in (4610,4694)",,,"wh")
			cSfrom_loc="3"
			cWhse = "NJ"
		OTHERWISE
			goffice = "Z"
			xsqlexec("select * from outship where mod = 'Z' and del_date = {"+dtoc(DATE()-nDaysback)+"} and accountid in (4610,4694)",,,"wh")
			cSfrom_loc="12"
			cWhse = "TO"
		
	ENDCASE

	SELECT outship
	COUNT TO N FOR del_date = DATE()-nDaysback AND ((outship.accountid=4610) OR ;
		(outship.accountid=4694 AND "MEIJER"$UPPER(outship.consignee))) ;
		OR "WAL-MART"$UPPER(outship.consignee) ;
		OR "WALMART"$UPPER(outship.consignee) ;
		OR "ECOMMERCE"$UPPER(outship.consignee) ;
		OR "SUPERCENTER"$UPPER(outship.consignee) ;
		OR "SEARS"$UPPER(outship.consignee) ;
		OR "RURAL KING"$UPPER(outship.consignee) ;
		OR "CITI"$UPPER(outship.consignee) ;
		OR "PENNEY"$UPPER(outship.consignee) ;
		OR UPPER("Regional Distribution Center")$UPPER(outship.consignee) ;
		OR "GREENCASTLE"$UPPER(consignee) ;
		OR "PATASKALA"$UPPER(consignee) ;
		OR "MAURICE"$UPPER(outship.consignee) ;
		OR "NEW YORK & COMPANY"$UPPER(outship.consignee) ;
		OR ("LORD"$UPPER(outship.consignee)  AND "TAYLOR"$UPPER(outship.consignee)) ;
		OR ("CAL"$UPPER(outship.consignee)  AND "RANCH"$UPPER(outship.consignee)) ;
		OR UPPER(outship.consignee) = "HBC" ;
		OR "ROSS"$UPPER(outship.consignee) ;
		OR "ATWOOD"$UPPER(outship.consignee)

	WAIT WINDOW "Number of "+ICASE(j=1,"Carson 2",j=2,"New Jersey","Toronto")+" SELECTED shipments, count: "+ALLTRIM(STR(N)) TIMEOUT 3

	DO CASE
		CASE j=1
			xsqlexec("select * from outdet where mod = 'Y' and wo_date >= {"+dtoc(DATE()-60)+"} and accountid in (4610,4694)",,,"wh")
		CASE j=2
			xsqlexec("select * from outdet where mod = 'I' and wo_date >= {"+dtoc(DATE()-60)+"} and accountid in (4610,4694)",,,"wh")
		OTHERWISE 
			xsqlexec("select * from outdet where mod = 'Z' and wo_date >= {"+dtoc(DATE()-60)+"} and accountid in (4610,4694)",,,"wh")
	ENDCASE


	IF j=2  && This section is for ADNY extracts
*		ASSERT .F. MESSAGE "At NJ extract...DEBUG"
		WAIT WINDOW "Now extracting ADNY shipments" TIMEOUT 2
		SELECT PADR(cSfrom_loc,2) AS sfrom_loc,accountid,cnee_ref AS po_num, outshipid, del_date AS shipdate, bol_no, SUM(ctnqty) AS cartons, ;
			SUM(qty) AS units,SUM(qty) AS oldunits,IIF(ATLINE("PREPACK-REP",shipins)>0,.T.,.F.) AS RPL ;
			FROM outship ;
			WHERE del_date = (DATE()-nDaysback) ;
			AND UPPER(cnee_ref) # "SMPL" ;
			AND LEFT(vendor_num,3)="10-" AND INLIST(accountid,4610,4694) ;
			GROUP BY 2,3 ;
			INTO CURSOR tempadny
		LOCATE
		WAIT WINDOW "There are "+ALLTRIM(STR(RECCOUNT()))+" ADNY records in this file" TIMEOUT 1
		IF lTesting AND RECCOUNT() > 0
			BROWSE TIMEOUT 3
		ENDIF
		IF RECCOUNT() > 0
			LOCATE
			SCAN
				IF RPL = .T.
					SELECT outdet
					SUM outdet.totqty*INT(VAL(outdet.PACK)) TO nUnits FOR outdet.outshipid = tempadny.outshipid
					REPLACE tempadny.units WITH nUnits IN tempadny NEXT 1
				ENDIF
			ENDSCAN

			SELECT sfrom_loc,shipdate,po_num,bol_no, SUM(cartons) AS cartons, ;
				SUM(units) AS units,IIF(accountid=4694 AND RPL=.F.,"PRE","PRR") AS packtype ;
				FROM tempadny ;
				GROUP BY 4 ;
				ORDER BY shipdate,packtype,bol_no ;
				INTO CURSOR tempadny2 READWRITE

			REPLACE tempadny2.units WITH tempadny2.cartons FOR packtype = "PRE" ALL
			LOCATE
			IF lTesting
				WAIT WINDOW "Now Browsing 2nd stage prepack listing"
				BROWSE
			ENDIF
		ENDIF
		COPY TO &cFilenameOut_a	TYPE XL5
	ENDIF


	WAIT WINDOW "Now extracting "+ICASE(j=1,"Carson 2",j=2,"New Jersey","Toronto")+" NANJING normal shipments" TIMEOUT 3
	SELECT PADR(cSfrom_loc,2) AS sfrom_loc,accountid,cnee_ref AS po_num, bol_no, outshipid, del_date AS shipdate, SUM(ctnqty) AS cartons, ;
		SUM(qty) AS units,SUM(qty) AS oldunits,IIF("PREPACK-REP"$shipins,.T.,.F.) AS RPL ;
		FROM outship ;
		WHERE del_date = (DATE()-nDaysback) ;
		AND UPPER(cnee_ref) # "SMPL" ;
		AND UPPER(bol_no)# "NOTHING SHIPPED" ;
		AND ((accountid=4610) OR ;
		(accountid=4694 AND ("MEIJER"$UPPER(consignee) ;
		OR "WAL-MART"$UPPER(consignee) ;
		OR "WALMART"$UPPER(consignee) ;
		OR "SUPERCENTER"$UPPER(consignee) ;
		OR "ECOMMERCE"$UPPER(outship.consignee) ;
		OR "SEARS"$UPPER(consignee) ;
		OR "CITI"$UPPER(consignee) ;
		OR "PENNEY"$UPPER(consignee) ;
		OR "GREENCASTLE"$UPPER(consignee) ;
		OR "PATASKALA"$UPPER(consignee) ;
		OR "MAURICE"$UPPER(consignee) ;
		OR "TARGET"$UPPER(consignee) ;
		OR "ATWOOD"$UPPER(consignee) ;
		OR "ROSS"$UPPER(outship.consignee) ;
		OR "RURAL KING"$UPPER(consignee) ;
		OR "NEW YORK & COMPANY"$UPPER(outship.consignee) ;
		OR ("CAL"$UPPER(outship.consignee)  AND "RANCH"$UPPER(outship.consignee)) ;
		OR UPPER(outship.consignee) = "HBC" ;
		OR ("LORD"$UPPER(outship.consignee)  AND "TAYLOR"$UPPER(outship.consignee)) ;
		OR UPPER("Regional Distribution Center")$UPPER(consignee)))) ;
		GROUP BY 2,3,4 ;
		INTO CURSOR tempout READWRITE

	SELECT tempout
	IF lTesting AND RECCOUNT()>0
		WAIT WINDOW "Browsing "+ICASE(j=1,"Carson 2",j=2,"New Jersey","Toronto")+" results: TEMPOUT" TIMEOUT 2
		LOCATE
		BROWSE
		SET STEP ON 
*		SUSPEND
*		DEBUG
	ENDIF
	SELECT distinct outshipid FROM tempout INTO CURSOR temposid
	
	IF lTesting
	SET STEP ON 
	ENDIF
	
	SELECT temposid
	locate
	SCAN
		SELECT outdet
		nUnits = 0
		SCAN FOR outdet.outshipid = temposid.outshipid
		IF "ITEMTYPE*PRE"$outdet.printstuff OR "ITEMTYPE*PIK"$outdet.printstuff
			SUM outdet.totqty*INT(VAL(outdet.PACK)) TO nUnits
			REPLACE tempout.units WITH nUnits FOR tempout.outshipid = temposid.outshipid
		ENDIF
	ENDSCAN
	SELECT tempout
	LOCATE

	SELECT sfrom_loc,shipdate,po_num,bol_no, SUM(cartons) AS cartons, ;
		SUM(units) AS units,ICASE(accountid=4610,"PIK",accountid=4694 AND RPL=.F.,"PRE","PRR") AS packtype ;
		FROM tempout ;
		GROUP BY 3,4 ;
		ORDER BY shipdate,packtype,bol_no ;
		INTO CURSOR tempout2 READWRITE

	IF lTesting
		BROWSE TIMEOUT 10
	ENDIF

	SELECT tempout2
	REPLACE tempout2.units WITH cartons FOR INLIST(packtype,"PRE") ALL
	IF j=1
		SELECT 0
		SELECT * FROM tempout2 INTO CURSOR tempoutfinal READWRITE
		SELECT tempout2
	ELSE
		SELECT tempoutfinal
		APPEND FROM DBF('tempout2')
	ENDIF
	LOCATE
ENDFOR
*ASSERT  .F.
SELECT tempoutfinal
IF RECCOUNT() = 0
	WAIT WINDOW "No records found" TIMEOUT 2
	lNoRecsMail = .T.
ENDIF

IF (lTesting AND RECCOUNT()>0)
	WAIT WINDOW "Browsing Final results" TIMEOUT 1
	BROWSE
ENDIF

COPY TO &cFilenameOut TYPE XL5

IF lDoMail
	DO mailproc
	IF FILE(cFilenameOut)
		DELETE FILE &cFilenameOut
	ENDIF
ENDIF
CLOSE DATABASES ALL

schedupdate()


******************
PROCEDURE mailproc
******************
	ASSERT .F. MESSAGE "In Mail process"
	tFrom ="TGF EDI Operations <toll-edi-ops@tollgroup.com>"
	SELECT 0
	USE F:\3pl\DATA\mailmaster ALIAS mm
	IF lTesting
		LOCATE FOR mm.edi_type = "MISC" AND mm.taskname = "GENERAL"
	ELSE
		LOCATE FOR mm.taskname = "NANJINGLASTDEL"
	ENDIF

	tsendto = IIF(mm.use_alt,sendtoalt,sendto)
	tcc = IIF(mm.use_alt,ccalt,cc)
	USE IN mm

	IF lNoRecsMail
		tsubject = "Nanjing Prior Day Shipping Report: EMPTY"
		tattach = " "
		tmessage = "There were no Nanjing deliveries yesterday."
	ELSE
		tsubject = "Daily Final Nanjing Shipping Report"
		tattach = cFilenameOut
		IF FILE(cFilenameOut_a)
			tattach = tattach+";"+cFilenameOut_a
		ENDIF
		tmessage = "Attached is the Nanjing shipping summary for "+cDateBack 
	ENDIF

	IF lTesting
		tmessage = tmessage + CHR(10) + "This is a TEST RUN..."
	ENDIF

	DO FORM m:\dev\frm\dartmail2 WITH tsendto,tFrom,tsubject,tcc,tattach,tmessage,"A"
	DELETE FILE [&cFilenameOut_a]
ENDPROC

*********************
PROCEDURE CLOSETABLES
*********************

	IF USED('outship')
		USE IN outship
	ENDIF
	IF USED('outdet')
		USE IN outdet
	ENDIF
	IF USED('tempout')
		USE IN tempout
	ENDIF
	IF USED('tempout2')
		USE IN tempout2
	ENDIF
ENDPROC
