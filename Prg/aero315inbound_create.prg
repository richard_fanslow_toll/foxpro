*!* This is for 315A FILES ONLY!
parameters nwo_num,coffice,nacctnum,dttrigtime
public c_cntrlnum,c_grpcntrlnum,cwo_num,cgroupname,cdivision,ccustname,cfilename,div214,cst_csz
public tsendto,tcc,ctrigdate,ctrigtime,cproctype,carrival,clocation,lnorecs
on escape cancel

ltesting = .f.
do m:\dev\prg\_setvars with ltesting
lfilesout = !ltesting
*lFilesOut = .f.
on error throw

cfilename = ""
liserror = .f.
lnorecs = .f.
ldocatch = .t.
nfilenum = 0
cproctype = "A"  && 315A = Arrival, 315D = Delivery
ceventcode = iif(cproctype="D","035","860")
assert .f. message "At start of 315"+cproctype+" Process"
try
	cwo_num = ""
	lemail = .t.
	ldoerror = .f.
	if vartype(nwo_num)= "L"
		if ltesting
			close databases all
			lfilesout = .f.
			nwo_num =    1330755
			nacctnum = 980
			coffice = "C"
			dttrigtime = datetime()-2400
		else
			wait window "No params specified" timeout 2
			ldocatch = .f.
			throw
		endif
	endif
	clocname = icase(coffice="C","CA",coffice="L","ML",coffice="M","FL","NJ")
	ctrigdate = dtos(ttod(dttrigtime))
	ctrigtime = left(right(ttoc(dttrigtime,1),6),4)

	cdivision = iif(coffice="C","California",iif(coffice="M","Miami","New Jersey"))
	coffcity = iif(coffice="C","SAN PEDRO",iif(coffice="M","MIAMI","CARTERET"))
	coffstate = iif(coffice="C","CA",iif(coffice="M","FL","NJ"))

	xreturn = "XXX"
	do m:\dev\prg\wf_alt with coffice,nacctnum
	if empty(alltrim(xreturn))
		do errormail with "NO WHSE"
		throw
	endif
	cusefolder = upper(xreturn)

	cwo_num = alltrim(str(nwo_num))

	dimension thisarray(1)

	tfrom = "TGF Warehouse Operations <TGF-warehouse-ops@tollgroup.com>"
	select 0
	use f:\3pl\data\mailmaster alias mm
	locate for mm.edi_type = "MISC" and mm.taskname = "GENERAL"
	tsendto = iif(mm.use_alt,sendtoalt,sendto)
	tcc = iif(mm.use_alt,ccalt,cc)
	locate
	locate for mm.accountid = 9999 and mm.office = "X"
	tsendtoerr = iif(mm.use_alt,sendtoalt,sendto)
	tccerr = iif(mm.use_alt,ccalt,cc)
	use in mm
	tattach = ""

	ccustname = "AEROPOSTALE"
	ccustfolder = "AEROPOSTALE"
	ccustprefix = "AP315"+lower(cproctype)+"_"+lower(coffice)
	cmailname = proper(ccustname)

*!* Open Tables
	cdetailerrors = ""
	if used('detail')
		use in detail
	endif

	xsqlexec("select * from dellocs",,,"wo")
	index on str(accountid,4)+location tag acct_loc
	set order to

	select 0
	use f:\wo\wodata\detail alias detail shared noupdate
	locate
	count to n for detail.wo_num = nwo_num
	if n = 0
		misswomail()
		do ediupdate with "Missing WO",.t.
		ldocatch = .f.
		throw
	endif

	count to ndet1 for detail.wo_num = nwo_num
	nscandet1 = 0
	scan for detail.wo_num = nwo_num
		if empty(alltrim(detail.arrival))
			nscandet1 = nscandet1+1
			cpoerr = alltrim(detail.po_num)
			cdetailerrors = iif(empty(cdetailerrors),cpoerr,cdetailerrors+chr(13)+cpoerr)
		endif
	endscan
	if !empty(cdetailerrors)
		if ndet1 = nscandet1
			cdetailerrors = cdetailerrors+chr(13)+"No 315 will be sent for WO# "+transform(nwo_num)
			do missdetmail with cdetailerrors
			ldocatch = .f.
			do ediupdate with "NO 315 POSSIBLE",.t.
			throw
		endif
	endif
	select arrival,delloc ;
	from detail ;
	where wo_num = nwo_num ;
	group by 1 ;
	into cursor temparrivalsx readwrite

	assert .f. message "At delloc selection file"
*	BROWSE TIMEOUT 30

	scan
		clocation = alltrim(temparrivalsx.delloc)
		cacctarr = str(nacctnum,4)+clocation
		if seek(cacctarr,'dellocs','acct_loc')
			if !inlist(dellocs.city,"ONTARIO","WALTON","SOUTH RIVER")
				delete next 1 in temparrivalsx
			endif
		else
			assert .f. message "LOC not found"
			do ediupdate with "MISS LOC",.t.
			do errormail with "MISSLOC"
			ldocatch = .f.
			throw
		endif
	endscan

	locate
	if eof()  && No records left in the temp file
		do ediupdate with "NO RECS",.f.
		ldocatch = .f.
		throw
	endif

	select arrival from temparrivalsx into cursor temparrivals
	use in temparrivalsx

	if usesqlwo()
		if xsqlexec("select * from wolog where wo_num="+transform(nwo_num),,,"wo") = 0
			assert .f. message "WO# not found"
			do errormail with "MISSWO"
			throw
		endif
		ccontainer = iif(empty(wolog.container),alltrim(wolog.awb),alltrim(wolog.container))
	else
		select 0
		if !used('wolog')
			use f:\wo\wodata\wolog alias wolog order tag wo_num shared noupdate
		endif
		if !seek(nwo_num,'wolog','wo_num')
			assert .f. message "WO# not found"
			do errormail with "MISSWO"
			throw
		endif
		ccontainer = iif(empty(wolog.container),alltrim(wolog.awb),alltrim(wolog.container))
		if empty(ccontainer)
	*!*				ASSERT .F. MESSAGE "In Empty wolog Container"
	*!*				IF wolog.TYPE # "D"
	*!*					DO errormail WITH "MISSEQUIP"
	*!*				ENDIF
	*!*				THROW
		endif
	endif
	
	cst_name = "TGF INC"
	cfd = "*"  && Field delimiter
	csegd = "�"  && Segment delimiter
	cterminator = ">"  && Used at end of ISA segment

	csendqual = "ZZ"  && Sender qualifier
	csendid = "FMIF"  && Sender ID code
	crecqual = "ZZ"  && Recip qualifier, was "08"
	crecid = "OHLGFML"   && Recip ID Code, was "6113310214"
	cdate = dtos(date())
	ctruncdate = right(cdate,6)
	ctrunctime = substr(ttoc(datetime(),1),9,4)
	cfiledate = cdate+ctrunctime
	csendidlong = padr(csendid,15," ")
	crecidlong = padr(crecid,15," ")

	if ltesting
		cisacode = "T"
	else
		cisacode = "P"
	endif

	select temparrivals
	locate
	scan
		carrival = alltrim(temparrivals.arrival)
		cstring = ""

		nstcount = 0
		nsegctr = 0
		nlxnum = 1
		store "" to cequipnum,chawb
		dt1 = ttoc(datetime(),1)
		dtmail = ttoc(datetime())
		dt2 = datetime()

		cfilename = ("f:\ftpusers\"+ccustfolder+"\315-Staging\"+ccustprefix+dt1+".txt")
		cfilenameshort = justfname(cfilename)
		cfilename2 = ("f:\ftpusers\"+ccustfolder+"\315OUT\archive\"+cfilenameshort)
		cfilename3 = ("f:\ftpusers\"+ccustfolder+"\315OUT\"+cfilenameshort)
		nfilenum = fcreate(cfilename)

		do num_incr_isa
		cisa_num = padl(c_cntrlnum,9,"0")
		store "ISA"+cfd+"00"+cfd+"          "+cfd+"00"+cfd+"          "+cfd+csendqual+cfd+csendidlong+cfd+;
		crecqual+cfd+crecidlong+cfd+ctruncdate+cfd+ctrunctime+cfd+"U"+cfd+"00401"+cfd+;
		cisa_num+cfd+"0"+cfd+cisacode+cfd+cterminator+csegd to cstring
		do cstringbreak

		store "GS"+cfd+"QO"+cfd+csendid+cfd+crecid+cfd+cdate+cfd+ctrunctime+cfd+c_cntrlnum+;
		cfd+"X"+cfd+"004010"+csegd to cstring
		do cstringbreak

		do num_incr_st
		store "ST"+cfd+"315"+cfd+padl(c_grpcntrlnum,9,"0")+csegd to cstring
		do cstringbreak
		nstcount = nstcount + 1
		nsegctr = nsegctr + 1

		store "B4"+cfd+cfd+cfd+"AD"+cfd+ctrigdate+cfd+ctrigtime+csegd to cstring
		do cstringbreak
		nsegctr = nsegctr + 1

		store "N9"+cfd+"EQ"+cfd+carrival+csegd to cstring
*		STORE "N9"+cfd+"EQ"+cfd+cContainer+csegd TO cString
		do cstringbreak
		nsegctr = nsegctr + 1

		store "N9"+cfd+"IT"+cfd+"2052"+csegd to cstring  && Changed per Aero mail, 06.17.2010
		do cstringbreak
		nsegctr = nsegctr + 1

		store "N9"+cfd+"WO"+cfd+cwo_num+csegd to cstring
		do cstringbreak
		nsegctr = nsegctr + 1

		store "R4"+cfd+"D"+cfd+"D"+cfd+clocname+csegd to cstring
		do cstringbreak
		nsegctr = nsegctr + 1

		store "DTM"+cfd+ceventcode+cfd+ctrigdate+cfd+ctrigtime+cfd+"LT"+csegd to cstring
		do cstringbreak
		nsegctr = nsegctr + 1

*!* Finish processing
		do close315
		if nfilenum>0
			=fclose(nfilenum)
		endif

		do ediupdate with "315"+cproctype+" CREATED",.f.

		if lfilesout
			copy file &cfilename to &cfilename2
			copy file &cfilename to &cfilename3
			delete file &cfilename
		endif

		if !ltesting
			if !used("ftpedilog")
				select 0
				use f:\edirouting\ftpedilog alias ftpedilog
				insert into ftpedilog (transfer,ftpdate,filename,acct_name,type) values ("214-"+ccustname,dt2,cfilename,upper(ccustname),"214")
				use in ftpedilog
			endif
		endif

		if ltesting
			tsubject = "Aeropostale 315"+cproctype+" *TEST* EDI from TGF as of "+dtmail
		else
			tsubject = "Aeropostale 315"+cproctype+" EDI from TGF as of "+dtmail
		endif

		tmessage = "315"+cproctype+" EDI Info from TGF-"+coffstate+", Truck WO# "+;
		cwo_num+", Arrival "+carrival+chr(13)
		tmessage = tmessage +" has been created."+chr(13)+"(Filename: "+cfilenameshort+")"
		if ltesting
			tmessage = tmessage + chr(13)+chr(13)+"This is a TEST RUN..."
		endif

		if lemail
			do form m:\dev\frm\dartmail2 with tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
		endif

		wait window cmailname+" 315"+cproctype+" EDI File complete for WO# "+cwo_num+", Arrv. "+carrival at 20,60 timeout 2
	endscan

	release all like c_cntrlnum,c_grpcntrlnum
	release all like norigseq,noriggrpseq

	wait clear
	wait window "All "+cmailname+" 315"+cproctype+" EDI File output complete" at 20,60 timeout 2

	closefiles()

catch to oerr
	if ldocatch
		lemail = .t.
		do ediupdate with "ERRHAND ERROR",.t.
		tmessage = ccustname+" Error processing "+chr(13)

		tmessage =tmessage+ "Try/CATCH Exeception Message:"+chr(13)+;
		[  Error: ] + str(oerr.errorno) +chr(13)+;
		[  LineNo: ] + str(oerr.lineno) +chr(13)+;
		[  Message: ] + oerr.message +chr(13)+;
		[  Procedure: ] + oerr.procedure +chr(13)+;
		[  Details: ] + oerr.details +chr(13)+;
		[  StackLevel: ] + str(oerr.stacklevel) +chr(13)+;
		[  LineContents: ] + oerr.linecontents+chr(13)+;
		[  UserValue: ] + oerr.uservalue

		tsubject = "315"+cproctype+" EDI Poller Error at "+ttoc(datetime())
		tattach  = ""
		tcc=""
		tfrom    ="TGF EDI 315"+cproctype+" Poller Operations <fmi-transload-ops@tollgroup.com>"
		do form m:\dev\frm\dartmail2 with tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
		lemail = .f.
	endif
finally
	on error
	closefiles()
	release all like t*
	ltesting = .f.
endtry

&& END OF MAIN CODE SECTION



****************************
procedure num_incr_isa
****************************

if !used("serfile")
	use ("F:\3pl\data\serial\"+ccustname+"315_serial") in 0 alias serfile
endif
select serfile
if ltesting
	norigseq = serfile.seqnum
	noriggrpseq = serfile.grpseqnum
	lisaflag = .f.
endif
nisa_num = serfile.seqnum
c_cntrlnum = alltrim(str(serfile.seqnum))
replace serfile.seqnum with serfile.seqnum + 1 in serfile
select wolog
endproc

****************************
procedure num_incr_st
****************************

if !used("serfile")
	use ("F:\3pl\data\serial\"+ccustname+"315_serial") in 0 alias serfile
endif
select serfile
if ltesting
	norigseq = serfile.seqnum
	noriggrpseq = serfile.grpseqnum
	lstflag = .f.
endif
c_grpcntrlnum = alltrim(str(serfile.grpseqnum))
replace serfile.grpseqnum with serfile.grpseqnum + 1 in serfile
select wolog
endproc

****************************
procedure close315
****************************
store  "SE"+cfd+alltrim(str(nsegctr+1))+cfd+padl(c_grpcntrlnum,9,"0")+csegd to cstring
do cstringbreak

store  "GE"+cfd+alltrim(str(nstcount))+cfd+c_cntrlnum+csegd to cstring
do cstringbreak

store  "IEA"+cfd+"1"+cfd+padl(c_cntrlnum,9,"0")+csegd to cstring
do cstringbreak

endproc

****************************
procedure cstringbreak
****************************
clen = len(alltrim(cstring))
fputs(nfilenum,cstring)
endproc

****************************
procedure errormail
****************************
parameters msgtype
assert .f. message "In ERRORMAIL"
ldocatch = .f.

assert .f. message "At Errormail...Debug"
tsubject = "Aeropostale 315 EDI File Error"
tmessage = "Aeropostale 315 EDI Info, WO# "+cwo_num+chr(10)
tsendto = tsendtoerr
tcc = tccerr

do case
case msgtype = "MISSGROUP"
	tmessage = tmessage + "There was no Group Name in WO# "+cwo_num
	do ediupdate with msgtype,.t.
case msgtype = "MISSWO"
	tmessage = tmessage + "WO# "+cwo_num+" does not exist in F:\WO\WODATA\WOLOG."
	do ediupdate with msgtype,.t.
case msgtype = "MISSEQUIP"
	tmessage = tmessage + "Equipment not specified for WO# "+cwo_num+chr(10)
	tmessage = tmessage + "Data in WOLOG must be corrected."
	do ediupdate with msgtype,.t.
case msgtype = "BAD TRIPID"
	tmessage = tmessage + "Trip ID does not exist in F:\WO\WODATA\WOLOG."
	do ediupdate with msgtype,.t.
case msgtype = "MISSLOC"
	tmessage = tmessage + "Location "+clocation+" does NOT EXIST IN DELLOCS for Acct. 980"
	do ediupdate with msgtype,.t.
endcase

if ltesting
	tmessage = tmessage + chr(10)+chr(10)+"This is TEST DATA only..."
endif
if lemail
	do form m:\dev\frm\dartmail2 with tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
endif
endproc

**************************
procedure ediupdate
**************************
parameters cfin_status, liserror
assert .f. message "At EDIUPDATE process"
if nfilenum>0
	=fclose(nfilenum)
endif
if cfin_status = "NO RECS"
	lnorecs = .t.
endif

if !ltesting
	select edi_trigger
	locate
	if liserror
		lerrorflag = iif(cfin_status = "NO 315 POSSIBLE",.f.,.t.)
		replace edi_trigger.processed with .t.,edi_trigger.created with .f.,;
		edi_trigger.fin_status with cfin_status,edi_trigger.errorflag with lerrorflag ;
		edi_trigger.when_proc with datetime() ;
		for edi_trigger.wo_num = nwo_num and edi_type="315"+cproctype

*			xsqlexec("update edi_trigger set processed=1, created=0, fin_status='"+cfin_status+"', " + ;
"errorflag="+transform(lerrorflag)+", when_proc={"+ttoc(datetime())+"} " + ;
"where wo_num="+transform(nwo_num) and edi_type='315'+cproctype,,,"stuff")

		if file(cfilename)
			delete file &cfilename
		endif
	else
		if lnorecs
			replace edi_trigger.processed with .t.,edi_trigger.created with .f.,proc214 with .t.;
			edi_trigger.when_proc with datetime(),;
			edi_trigger.fin_status with "NO 315 NEEDED";
			edi_trigger.errorflag with .f.,file214 with "" ;
			for edi_trigger.wo_num = nwo_num and edi_type="315"+cproctype

*				xsqlexec("update edi_trigger set processed=1, created=0, proc214=1, when_proc={"+ttoc(datetime())+"}, " + ;
"fin_status='no 315 needed', errorflag=0, file214='' " + ;
"where wo_num="+transform(nwo_num)+" and edi_type='315'"+cproctype,,,"stuff")

			if file(cfilename)
				delete file &cfilename
			endif
		else
			replace edi_trigger.processed with .t.,edi_trigger.created with .t.,proc214 with .t.;
			edi_trigger.when_proc with datetime(),;
			edi_trigger.fin_status with "315 CREATED";
			edi_trigger.errorflag with .f.,file214 with cfilename ;
			for edi_trigger.wo_num = nwo_num and edi_type="315"+cproctype

*				xsqlexec("update edi_trigger set processed=1, created=1, proc214=1, when_proc={"+ttoc(datetime())+"}, " + ;
"fin_status='315 created', errorflag=0, file214='"+cfilename+"' " + ;
"where wo_num="+transform(nwo_num)+" and edi_type='315'"+cproctype,,,"stuff")
		endif
	endif
endif

*!* Added 09/18/2014, Joe
if !liserror
	set step on
	select 0
	use f:\edirouting\ftpjobs
	insert into ftpjobs (jobname,userid,jobtime) values (iif(ltesting,"315-TEST-OHL","315-OHL"),"tollglobal",datetime())
	use in ftpjobs
endif
endproc

*********************
procedure closefiles
*********************
if used('wolog')
	use in wolog
endif
if used('dellocs')
	use in dellocs
endif
endproc

*********************
procedure missdetmail
*********************
parameters cdetailerrors
select 0
use f:\3pl\data\mailmaster alias mm shared
locate for edi_type = "MISC" and taskname = "MISS315INFO"
lusealt = mm.use_alt
tsendto = iif(lusealt,sendtoalt,sendto)
tcc = iif(lusealt,ccalt,cc)
use in mm
tsubject = ccustname+" Detail Error at "+ttoc(datetime())
tattach  = ""
tmessage = "315"+cproctype+" processing shows missing Detail arrivals in the following PO(s):"
tmessage = tmessage+chr(13)+cdetailerrors
tattach = " "
if lemail
	do form m:\dev\frm\dartmail2 with tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
endif
endproc

*********************
procedure misswomail
*********************
select 0
use f:\3pl\data\mailmaster alias mm shared
locate for edi_type = "MISC" and taskname = "MISS315INFO"
lusealt = mm.use_alt
tsendto = iif(lusealt,sendtoalt,sendto)
tcc = iif(lusealt,ccalt,cc)
use in mm
tsubject = ccustname+" Detail Error at "+ttoc(datetime())
tattach  = ""
tmessage = "315"+cproctype+" processing shows no Detail records for WO# "+cwo_num+", please determine cause."
tattach = " "
if lemail
	do form m:\dev\frm\dartmail2 with tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
endif
endproc
