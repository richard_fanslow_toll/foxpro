**********************************************************************************
* Office Depot 856 Uplod and bkdn
***********************************************************************************
* The 856 is structured as
* Shipment
*     Pack
*
*  Started: 10/02/2013,  PG
***********************************************************************************
PUBLIC lProcessOK,lTesting,gContainer,cOffice,nAccountid,cTransfer,NormalExit
PUBLIC lcFilename, xfile, lcASN_Number,tsendto,tcc

CLOSE DATABASES ALL

WAIT WINDOW AT 10,10  "Now setting up Offie Depot 856 upload............" TIMEOUT 1
SET tablevalidate TO 0
DO m:\dev\prg\_setvars WITH .T.
ON ESCAPE CANCEL

SET EXCLUSIVE OFF

lcASN_Number = ""
xfile = ""
lcFilename =""

TRY
	lTesting = .F.
	lOverridebusy =  .F. &&l&Testing
	cOffice = "M"
	cMod = "M"
	gOffice = cMod
	gMasterOffice = cOffice
	nAccountid = 6494
	cTransfer = "856-OFFICEDEPOT"
	cMailName = "Office Depot"
	NormalExit = .F.
*	lOverridebusy =  .t.

	_SCREEN.WINDOWSTATE = IIF(lTesting OR lOverridebusy,2,1)
	_SCREEN.CAPTION = "Office Depot 856 Inbound Process"

	IF !lTesting
	USE F:\edirouting\ftpsetup IN 0
	SELECT ftpsetup
	LOCATE FOR ftpsetup.transfer =  cTransfer
	IF FOUND()
		IF !lTesting AND !lOverridebusy
			IF !ftpsetup.chkbusy
				REPLACE chkbusy WITH .T. FOR ftpsetup.transfer = cTransfer
			ELSE
				WAIT WINDOW "Transfer locked...can't start new process" TIMEOUT 2
				NormalExit = .T.
				THROW
			ENDIF
		ELSE
			REPLACE chkbusy WITH .T.,trig_time WITH DATETIME() FOR ftpsetup.transfer = cTransfer
		ENDIF
		USE IN ftpsetup
	ELSE
		WAIT WINDOW "No Record in FTPSetup for Office Deport 856" TIMEOUT 2
		NormalExit = .T.
		THROW
	ENDIF
	endif

	xReturn = "XX"
	IF !lTesting
		DO m:\dev\prg\wf_alt WITH "C",nAccountid
		cUseFolder = UPPER(xReturn)
	ELSE
		cUseFolder = "f:\whp\whdata\"
	ENDIF

	SELECT 0
	USE F:\3pl\DATA\mailmaster ALIAS mm SHARED
	LOCATE FOR mm.edi_type = "856" AND mm.group = "OD"
	IF FOUND()
		STORE TRIM(IIF(mm.use_alt,mm.sendtoalt,mm.sendto)) TO tsendto
		STORE TRIM(IIF(mm.use_alt,mm.ccalt,mm.cc)) TO tcc
		LOCATE
		LOCATE FOR mm.edi_type = "MISC" AND mm.taskname = "GENERAL"
		STORE TRIM(IIF(mm.use_alt,mm.sendtoalt,mm.sendto)) TO tsendtotest
		STORE TRIM(IIF(mm.use_alt,mm.ccalt,mm.cc)) TO tcctest
		IF lTesting
		tsendto = tsendtotest
		tcc = tcctest
		endif
		USE IN mm
	ELSE
		USE IN mm
		WAIT WINDOW AT 10,10  "No parameters set for this acct# "+ALLTRIM(STR(nAcctNum))+"  ---> Office "+cOffice TIMEOUT 2
		RELEASE ALL
		NormalExit = .F.
		THROW
	ENDIF

*	USE (cUseFolder+"whgenpk") IN 0 ALIAS whgenpk SHARED
	xsqlexec("select * from inwolog where .f.","xinwolog",,"wh")
	useca("inwolog","wh")

	USE (cUseFolder+"ctnucc") IN 0  SHARED
	gMasterOffice = cOffice
	gOffice = cOffice

	useca("pl","wh")
	SELECT * FROM pl      WHERE .F. INTO CURSOR xpl READWRITE

	xsqlexec("select * from odlanes",,,"wh")

	DO m:\dev\prg\createx856

	lcDELIMITER = "*"
	lcTranslateOption ="OFFICEDEPOT-EDI"
	delimchar = lcDELIMITER
	lcTranOpt= lcTranslateOption

	lcpath="f:\ftpusers\officedepot\in\"
	lcarchivepath="f:\ftpusers\officedepot\856in\archive\"

	CD &lcpath

	lnNum = ADIR(tarray,"f:\ftpusers\officedepot\in\*.edi")

	IF lnNum = 0
		WAIT WINDOW AT 10,10 "    No Office Depot 856's to import.............." TIMEOUT 2
		USE F:\edirouting\ftpsetup SHARED
		LOCATE FOR ftpsetup.transfer =  cTransfer
		IF FOUND()
			REPLACE chkbusy WITH .F. FOR ftpsetup.transfer = cTransfer
		ENDIF
		NormalExit = .T.
		THROW
	ENDIF

	FOR thisfile = 1  TO lnNum
* Archivename = Ttoc(Datetime(),1)
		xfile = lcpath+tarray[thisfile,1]+"."

*Xfile = lcPath+Allt(tarray[thisfile,1])
		!ATTRIB -R &xfile  && Removes read-only flag from file to allow deletion

		archivefile = lcarchivepath+tarray[thisfile,1]
		lcFilename = tarray[thisfile,1]
		WAIT WINDOW "Importing file: "+xfile NOWAIT
		IF FILE(xfile)
* load the file into the 856 array
			SELECT x856
			ZAP
			DO m:\dev\prg\loadedifile WITH xfile,delimchar,lcTranOpt,"OFFICEDEPOT-EDI" &&"NAUTICA"
			lProcessOK = .F.

			DO m:\dev\prg\od_856bkdn WITH xfile

*!*				SELECT whgenpk		dy 12/26/17
*!*				LOCATE FOR gpk_pk = "WONUM"
*!*				REPLACE gpk_currentnumber WITH gpk_currentnumber + 1
*!*				m.wo_num = gpk_currentnumber

			m.wo_num = sqlgenpk("wonum-m","wh")

			SELECT xdetail
			SCAN
				SELECT odlanes
				LOCATE FOR ALLTRIM(STORE) = ALLTRIM(xdetail.STORE)
				IF FOUND()
					REPLACE xdetail.ID WITH ALLTRIM(odlanes.lane)
				ENDIF
			ENDSCAN

			asnloadtime = DATETIME()

			SELECT xdetail
			SCAN
				SELECT xdetail
				REPLACE xdetail.inwonum WITH  m.wo_num
				REPLACE xdetail.STYLE WITH ALLTRIM(xdetail.bol)+"-"+xdetail.STORE
				SCATTER MEMVAR FIELDS EXCEPT ctnuccid

				m.ctnuccid = sqlgenpk("ctnucc","wh")
				SELECT ctnucc
				INSERT INTO ctnucc FROM MEMVAR

				REPLACE asnstatus WITH "L" IN ctnucc
				REPLACE asnloaddttm WITH asnloadtime IN ctnucc
			ENDSCAN

			SELECT xdetail
			lnNumCartons = RECCOUNT("xdetail")

*** m:\dev\prg\officedepot_createinbound

			useca("od_asns","wh")
			
			m.office="C"
			m.mod="8"
			m.asn=lcASN_Number
			m.filename=lcFilename
			m.qty=lnNumCartons
			m.uploaddt=date()
			m.uploadtime=asnloadtime
			insertinto("od_asns","wh",.t.)
			
			tu("od_asns")

			IF !lTesting
				COPY FILE &xfile TO &archivefile
				IF FILE(archivefile)
					DELETE FILE &xfile
				ENDIF
				tsendto = "Omar.Garcia@tollgroup.com"
				tcc = "pgaidis@fmiint.com"
				tattach=""
				tsubject = "Office Deport ASN Upload (Production Upload) at "+TTOC(DATETIME())+"TOLL Sequence#: "+TRANSFORM(m.wo_num)+" ASN# "+lcASN_Number+"  Cartons: "+ALLTRIM(TRANSFORM(lnNumCartons))
				tmessage = "Office Deport ASN Upload"+CHR(13)+"TOLL Sequence#: "+TRANSFORM(m.wo_num)+CHR(13)+" ASN# "+lcASN_Number+"  Cartons: "+ALLTRIM(TRANSFORM(lnNumCartons))+;
					CHR(13)+" File: "+lcFilename
				tfrom    ="TGF EDI Processing Center <edi-ops@fmiint.com>"
				DO FORM m:\dev\FRM\dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
			ENDIF
		ENDIF
	NEXT thisfile

	WAIT WINDOW AT 10,10 "Office Depot ASN upload complete for: "+lcFilename TIMEOUT 10

	IF !lTesting
		SELECT 0
		USE F:\edirouting\ftpsetup SHARED
		REPLACE chkbusy WITH .F. FOR ftpsetup.transfer = cTransfer
		USE IN ftpsetup
	ENDIF
	NormalExit = .T.


CATCH TO oErr
	IF NormalExit = .F.
		ASSERT .F. MESSAGE "In Error Catch"
		SET STEP ON
		tsendto = "pgaidis@fmiint.com"
		tcc = "juan.rocio@tollgroup.com"
		tsubject = cMailName+" 856 Upload Error ("+TRANSFORM(oErr.ERRORNO)+") at "+TTOC(DATETIME())
		tmessage = cMailName+" 856 Upload Error..... Please fix"
		lcSourceMachine = SYS(0)
		lcSourceProgram = SYS(16)

		tmessage =tmessage+ "Try/CATCH Exeception Message:"+CHR(13)+;
			[  Error: ] + STR(oErr.ERRORNO) +CHR(13)+;
			[  LineNo: ] + STR(oErr.LINENO) +CHR(13)+;
			[  Message: ] + oErr.MESSAGE +CHR(13)+;
			[  Procedure: ] + oErr.PROCEDURE +CHR(13)+;
			[  Details: ] + oErr.DETAILS +CHR(13)+;
			[  StackLevel: ] + STR(oErr.STACKLEVEL) +CHR(13)+;
			[  LineContents: ] + oErr.LINECONTENTS+CHR(13)+;
			[  UserValue: ] + oErr.USERVALUE+CHR(13)+;
			[  Computer:  ] +lcSourceMachine+CHR(13)+;
			[  940 file:  ] +xfile+CHR(13)+;
			[  Program:   ] +lcSourceProgram

		tattach  =  " "
		tfrom    ="TGF EDI Processing Center <edi-ops@fmiint.com>"
		DO FORM m:\dev\FRM\dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
	ELSE
		WAIT WINDOW AT 10,10 "Normal Exit " TIMEOUT 2
	ENDIF
FINALLY
	ON ERROR
	SET STATUS BAR ON
	CLOSE DATABASES ALL
ENDTRY

&& ricardo.tapia@tollgroup.com,juan.rocio@tollgroup.com

