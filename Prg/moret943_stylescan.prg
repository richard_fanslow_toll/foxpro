*!* MORET 943_STYLESCAN.PRG
CLOSE DATABASES ALL
CLEAR ALL
RELEASE ALL

lTesting = .F.
lTestmail = .F.
lKeepScreen = .F.

DO m:\dev\prg\_setvars WITH lTesting
IF lTesting
	WAIT WINDOW "This is a "+IIF(lTesting,"TEST","PRODUCTION")+" verification" TIMEOUT 2
ENDIF

TRY
	PUBLIC lXfer943,cWhse,cFilename,cFilename2,tsendto,tcc,lIsError,nOrigAcctNum,nDivLoops
	PUBLIC lDoMail,tfrom,tattach,cxErrMsg,NormalExit,nAcctNum,Acctname,lMoretSend,cOffice,lPick,lStyleDup
	PUBLIC tsendtostyle,tccstyle,tccerr,tsendtoerr
	_SCREEN.CAPTION = "Moret 945 Style Scan Process"
	_SCREEN.WINDOWSTATE = IIF(lTesting OR lKeepScreen,2,1)
	tfrom = "TGF EDI Operations <toll-edi-ops@tollgroup.com>"
	tattach = " "
	lDoMail = .T.  && If .t., sends mail
	NormalExit = .F.
	cFilesmoved = ""
	cInfolder = ("F:\ftpusers\moret\943IN\")
	cOutfolder = ("F:\0-Packinglist\Moret-All\943_Process\")
	cHoldfolder = ("F:\FTPUSERS\MORET\943in\Hold for Moret\")
	c997folder = ("F:\FTPUSERS\MORET\940translate\")
	lOverrideBusy = .F.

	nAcctNum = 5742
	cOffice = "C"
	cxErrMsg = ""
	cMailName = "Moret"  && Proper name for eMail, etc.

	IF !lTesting
		SELECT 0
		USE F:\edirouting\ftpsetup SHARED
		LOCATE FOR ftpsetup.transfer = "AUX-STYLESCAN-MORET"
		IF ftpsetup.chkbusy AND !lOverrideBusy
			WAIT WINDOW "Process is flagged busy...returning" TIMEOUT 3
			NormalExit = .T.
			THROW
		ELSE
			REPLACE chkbusy WITH .T.,trig_time WITH DATETIME() FOR ftpsetup.transfer = "AUX-STYLESCAN-MORET" IN ftpsetup
			USE IN ftpsetup
		ENDIF
	ENDIF

	xsqlexec("select * from account where inactive=0","account",,"qq")
	INDEX ON accountid TAG accountid
	SET ORDER TO

	USE F:\3pl\DATA\moretedidivs.DBF IN 0 ALIAS mdivs
	SELECT 0

	STORE "MORET943" TO m.addby
	STORE DATETIME() TO m.adddt
	STORE "PROC943" TO m.addproc
	STORE DATE() TO m.wo_date,m.date_rcvd

	CD [&cInFolder]
	DELETE FILE (cInfolder+'*.txt')
	DELETE FILE (cInfolder+'foxuser.*')

	len1 = ADIR(ary1,"*.edi")
	IF len1 = 0
		WAIT WINDOW "There are no pending 943 files to process...exiting" TIMEOUT 2
		CLOSE DATABASES ALL
		NormalExit = .T.
		THROW
	ENDIF

	FOR kk = 1 TO len1
		cFilename = ALLTRIM(ary1[kk,1])
		cOutFile = (cOutfolder+cFilename)
		c997File = (c997folder+cFilename)
		cHoldFile = (cHoldfolder+cFilename)
		cCoNum = LEFT(cFilename,2)
		dFiledate = ary1[kk,3]

		cFilename2 = JUSTSTEM(cFilename)+"2.TXT"
		cString = FILETOSTR(cFilename)
		cString = STRTRAN(cString,"|","*")
		cString = STRTRAN(cString,"~",CHR(13))
		STRTOFILE(cString,cFilename2)
		SELECT 0
		CREATE CURSOR tempstyles (conum c(2),coname c(10),division c(3),accountid N(5),STYLE c(20),thisdiv l,pnp l,packcheck l,FOUND l)
		DO m:\dev\prg\createx856a
		SELECT x856
		APPEND FROM &cFilename2 TYPE DELIMITED WITH CHARACTER "*"

		LOCATE
		LOCATE FOR x856.segment = "W06"
		cAcctRef = ALLTRIM(x856.f2)
		LOCATE FOR x856.segment = "N1" AND x856.f1 = "WH"
		cWhseCode = RIGHT(ALLTRIM(x856.f4),2)
		cWhseName = ICASE(cWhseCode = 'NJ','CARTERET',cWhseCode = 'FL','MIAMI','SAN PEDRO')
		cOffice = ICASE(cWhseCode = 'NJ','I',cWhseCode = 'FL','M','C')
		setuprcvmail()

		SELECT x856
		LOCATE FOR x856.segment = "GS" AND x856.f1 = "PO"
		IF !FOUND()
			cxErrMsg = cFilename+" is NOT a 943 document"
			errormail()
		ENDIF

		cConame = ALLTRIM(x856.f2)
		DO CASE
			CASE cConame = "SBHINTMTS"
				nAcctNum = 5451
				cCoNum = '02'
			CASE cConame = "HIGHPOINT"
				cCoNum = '04'
				nAcctNum = 5452
			CASE cConame = "HBESTLMTD"
				cCoNum = '03'
				nAcctNum = 5453
			CASE cConame = "BOZKEELLC"
				cCoNum = '08'
				nAcctNum = 6262
			CASE cConame = "REGENTSUT"
				cCoNum = '05'
				nAcctNum = 6093
			CASE cConame = "BOYMEETSG"
				cCoNum = '07'
				nAcctNum = 6572
			CASE cConame = "MORET00SK"
				cCoNum = '06'
				nAcctNum = 6403
			CASE cConame = "JMORETINC"
				nAcctNum = 5742  && Seed with JMI Ladies
				cCoNum = '01'
			OTHERWISE
		ENDCASE

		SELECT x856
		LOCATE
		LOCATE FOR x856.segment = "W27"
		cContainer = ALLTRIM(x856.f6)+ALLTRIM(x856.f7)
		LOCATE
		SCAN
			IF x856.segment = "W04"
				cStyle = ALLTRIM(x856.f5)
				lPnP = IIF(x856.f2='PG',.T.,.F.)
			ENDIF
			IF x856.segment = "N9" AND x856.f1 = "DV"
				cDivision = ALLTRIM(x856.f2)
				INSERT INTO tempstyles (conum,coname,division,accountid,STYLE,thisdiv,pnp,packcheck,FOUND) ;
					VALUES (cCoNum,cConame,cDivision,9999,cStyle,.F.,lPnP,.F.,.F.)
			ENDIF
		ENDSCAN

		SELECT tempstyles
		SCAN
			SELECT mdivs
			LOCATE FOR mdivs.conum = tempstyles.conum AND mdivs.divcode = tempstyles.division
			IF !FOUND()
				cxErrMsg = "Unknown Co/Div: "+ALLTRIM(tempstyles.conum)+"/"+ALLTRIM(tempstyles.division)
				errormail()
			ELSE
				REPLACE tempstyles.accountid WITH mdivs.accountid IN tempstyles
			ENDIF
		ENDSCAN

		LOCATE
*SET STEP ON
		SCAN
			cStyle = UPPER(ALLTRIM(tempstyles.STYLE))
			lPnP = tempstyles.pnp
			cAcctStyle = STR(tempstyles.accountid,4)+PADR(cStyle,20)

			lFound = upcmastsql(,cStyle)
			lThisDiv = upcmastsql(tempstyles.accountid,cStyle)

			lPackCheck = .F.
			IF lThisDiv
				lPackCheck = IIF(upcmast.pnp=lPnP,.T.,.F.)
			ENDIF
			REPLACE tempstyles.packcheck WITH lPackCheck IN tempstyles
			REPLACE tempstyles.thisdiv WITH lThisDiv IN tempstyles
			REPLACE tempstyles.FOUND WITH lFound IN tempstyles
		ENDSCAN

		IF lTesting OR lTestmail
			BROWSE
		ENDIF

		ASSERT .F. MESSAGE "DEBUG"
		LOCATE FOR (tempstyles.thisdiv = .F. OR tempstyles.FOUND = .F. OR tempstyles.packcheck = .F.)
		IF FOUND()
			badstylemail()
			COPY FILE [&cFilename] TO [&cHoldfile]
			DELE FILE [&cFilename]
		ELSE
			WAIT WINDOW "All Styles in file"+cFilename+" were found for correct divisions in UPCMAST" TIMEOUT 1
			cFilesmoved = IIF(EMPTY(cFilesmoved),cFilename,cFilesmoved+CHR(13)+cFilename)
			IF !lTestmail
				COPY FILE [&cFilename] TO [&c997file]
				COPY FILE [&cFilename] TO [&cOutfile]
				DELE FILE [&cFilename]
			ENDIF
		ENDIF
	ENDFOR

	IF !EMPTY(cFilesmoved)
		goodmail()
	ENDIF

	IF !lTesting
		SELECT 0
		USE F:\edirouting\ftpsetup SHARED
		REPLACE chkbusy WITH .F. ;
			FOR ftpsetup.transfer = "AUX-STYLESCAN-MORET" ;
			IN ftpsetup
	ENDIF

	WAIT WINDOW "All incoming Moret 943s processed...exiting" TIMEOUT 2
	NormalExit = .T.

CATCH TO oErr
	IF NormalExit = .F.
		SET STEP ON
		tsubject = cMailName+" 943 Upload Error ("+TRANSFORM(oErr.ERRORNO)+") at "+TTOC(DATETIME())
		tattach  = ""
		tmessage = cMailName+" 943 Upload Error"+CHR(13)
		lcSourceMachine = SYS(0)
		lcSourceProgram = SYS(16)

		tmessage =tmessage+ "Try/CATCH Exeception Message:"+CHR(13)+;
			[  Error: ] + STR(oErr.ERRORNO) +CHR(13)+;
			[  Procedure: ] + oErr.PROCEDURE +CHR(13)+;
			[  LineNo: ] + STR(oErr.LINENO) +CHR(13)+;
			[  Message: ] + oErr.MESSAGE +CHR(13)+;
			[  Details: ] + oErr.DETAILS +CHR(13)+;
			[  StackLevel: ] + STR(oErr.STACKLEVEL) +CHR(13)+;
			[  LineContents: ] + oErr.LINECONTENTS+CHR(13)+;
			[  UserValue: ] + oErr.USERVALUE+CHR(13)+;
			[  Computer:  ] +lcSourceMachine+CHR(13)+;
			[  940 file:  ] +cFilename+CHR(13)+;
			[  Program:   ] +lcSourceProgram

		tmessage = tmessage + CHR(13) + CHR(13) + cxErrMsg
		tattach  = ""
		DO FORM m:\dev\FRM\dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
	ENDIF
FINALLY
	DELETE FILE (cInfolder+'*.txt')
	DELETE FILE (cInfolder+'foxuser.*')
	CLOSE DATABASES ALL
	ON ERROR
ENDTRY


****************************
PROCEDURE goodmail
****************************
	tsubject = cMailName+" EDI FILE (943) moved into process folder"
	tmessage = REPLICATE("-",90)+CHR(13)
	tmessage = tmessage+"Data in File(s) : "+CHR(13)+cFilesmoved+CHR(13)
	tmessage = tmessage+"passed Style and other checks and is being moved to the 943 process folder"

	IF lTesting
		ASSERT .F. MESSAGE "In Good-Mail process"
	ENDIF
	IF lDoMail
		DO FORM m:\dev\frm\dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
	ENDIF
ENDPROC

****************************
PROCEDURE badstylemail
****************************
	SET STEP ON
	IF !lTesting AND !lTestmail
		tsendto  = tsendtostyle
		tcc      = tccstyle
	ENDIF
	tsubject = cMailName+" Style Errors in 943 EDI file"+CHR(13)
	tmessage = "There were Style Master errors in File : "+cFilename+CHR(13)
	tmessage = tmessage+"Acct.Ref: "+cAcctRef+", Container: "+cContainer+CHR(13)+CHR(13)
	tmessage = tmessage+PADR("CO#",5)+PADR("Division",10)+PADR("Style",20)+"Note"+CHR(13)
	tmessage = tmessage+REPLICATE("-",90)+CHR(13)
	SELECT tempstyles
	SCAN
		IF (!tempstyles.FOUND OR !tempstyles.thisdiv OR !tempstyles.packcheck)
*			SET STEP ON
			tmessage = tmessage+PADR(ALLT(tempstyles.conum),5)+PADR(ALLT(tempstyles.division),10)+PADR(ALLT(tempstyles.STYLE),20)
			tmessage = tmessage+ICASE(!tempstyles.FOUND," Style Not Found At All",!tempstyles.thisdiv," Style Not Found For Div. "+ALLT(tempstyles.division)," Pack type Mismatch (Moret 943 shows: "+IIF(lPnP,"PnP","Prepack")+")")+CHR(13)
		ENDIF
	ENDSCAN

	tmessage = tmessage+CHR(13)+;
		IIF(tempstyles.FOUND AND tempstyles.thisdiv AND !tempstyles.packcheck,"Please correct pack type and RESEND THE 943",;
		"Please send the 888 Style Master update file as soon as possible.")
	tmessage = tmessage+CHR(13)+"(File has been moved to Hold folder)"

	IF lTesting
		ASSERT .F. MESSAGE "In Bad/Missing Style Mail procedure"
	ENDIF
	IF lDoMail
		DO FORM m:\dev\frm\dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
	ENDIF
ENDPROC

****************************
PROCEDURE errormail
****************************
	tsubject = cMailName+" EDI ERROR (943)"
	tmessage = "File: "+cFilename
	tmessage = tmessage+CHR(13)+cxErrMsg
	SET STEP ON
	DO FORM m:\dev\frm\dartmail2 WITH tsendtoerr,tfrom,tsubject,tccerr,tattach,tmessage,"A"
	NormalExit = .T.
	THROW
ENDPROC

****************************
PROCEDURE setuprcvmail
****************************
	SELECT 0
	USE F:\3pl\DATA\mailmaster ALIAS mm
	LOCATE
	LOCATE FOR mm.edi_type = "MISC" AND mm.taskname = "JOETEST"
	lUseAlt = mm.use_alt
	STORE IIF(lUseAlt,TRIM(mm.sendtoalt),TRIM(mm.sendto)) TO tsendto
	STORE IIF(lUseAlt,TRIM(mm.ccalt),TRIM(mm.cc)) TO tcc
	LOCATE
	LOCATE FOR mm.edi_type = "MISC" AND mm.taskname = "GENERAL"
	lUseAlt = mm.use_alt
	STORE IIF(lUseAlt,TRIM(mm.sendtoalt),TRIM(mm.sendto)) TO tsendtoerr
	STORE IIF(lUseAlt,TRIM(mm.ccalt),TRIM(mm.cc)) TO tccerr
	LOCATE
	LOCATE FOR mm.edi_type = "MISC" AND mm.taskname = "MORETSTYLE"
	lUseAlt = mm.use_alt
	STORE IIF(lUseAlt,TRIM(mm.sendtoalt),TRIM(mm.sendto)) TO tsendtostyle
	STORE IIF(lUseAlt,TRIM(mm.ccalt),TRIM(mm.cc)) TO tccstyle
	USE IN mm
ENDPROC
