lparameters xreportname

insert into pdf_file (unique_id) values (reports.unique_id)

locate
if eof() &&need something to distinguish from "poller down"
	select pdf_file
	replace memo with "NO DATA"

else
	xfile = tempfox+alltrim(str(reports.unique_id))+".pdf"

	rpt2pdf(xreportname,xfile)

	select pdf_file
	**in order to avoid "VFPOLEDB" "provider ran out of memory" errors for large files (>2mb) no longer move file 
	**  thru linked server, just copy file directly to \\iis2\apps\CargoTrackingWeb\reports\ - mvw 03/08/13
	*append memo memo from &xfile overwrite
	copy file "&xfile" to \\iis2\apps\CargoTrackingWeb\reports\*.*
	replace memo with "FILE COPIED TO REPORT FOLDER" in pdf_file
endif
