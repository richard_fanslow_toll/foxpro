* Create monthly summary of Temp hours by worksite for Jack Drohan. 
* To be run early each month against prior month.
*
* 3/7/2017 - added columns for guards hours/headcnt, and a row for wilmington yard; filled guard info with default values from Diana Landeros.
*
* 3/10/2017 - added statistics for Owner Operators at request of Diana Landeros.
*
* EXEs = F:\UTIL\KRONOS\KRONOSTEMPHOURSBYWORKSITE.EXE

LOCAL loKronosTempHoursByWorksite

runack("KRONOSTEMPHOURSBYWORKSITE")

utilsetup("KRONOSTEMPHOURSBYWORKSITE")


loKronosTempHoursByWorksite = CREATEOBJECT('KronosTempHoursByWorksite') 
loKronosTempHoursByWorksite.MAIN()


schedupdate()

CLOSE DATABASES ALL
RETURN

#DEFINE LOGIT 1
#DEFINE WAITIT 2
#DEFINE NOWAITIT 4
#DEFINE SENDIT 8
#DEFINE CRLF CHR(13) + CHR(10)
#DEFINE LF CHR(10)
#DEFINE RETURN_DATA_MANDATORY .F.
#DEFINE RETURN_DATA_NOT_MANDATORY .T.
#DEFINE DAILY_OT_MULTIPLIER 1.5
#DEFINE DAILY_OT20_MULTIPLIER 2.0
#DEFINE xlEdgeBottom 9
#DEFINE xlContinuous 1
#DEFINE xlMedium -4138

DEFINE CLASS KronosTempHoursByWorksite AS CUSTOM

	cProcessName = 'KronosTempHoursByWorksite'

	lTestMode = .F.  && .F. FOR NORMAL OPERATION, .T. TO SUPPRESS ALL BUT DEVELOPER EMAILS AND CHANGE INPUT TABLE, ETC.s

	lAutoYield = .T.

	cStartTime = TTOC(DATETIME())
	
	* connection properties
	nSQLHandle = 0

	* file properties
	nFileHandle = 0

	* date properties
	dtNow = DATETIME()

	dToday = DATE()
	* to force different pay weeks
	*dToday = {^2018-05-01}

	cToday = DTOC(DATE())
	
	* wait window properties
	nWaitWindowTimeout = 2
	lWaitWindowIsOn = .T.

	* logfile properties
	lLoggingIsOn = .T.
	cLogFile = 'F:\UTIL\KRONOS\Logfiles\KronosTempHoursByWorksite_log.txt'

	* INTERNAL email properties
	lSendInternalEmailIsOn = .T.
	cFrom = 'mark.bennett@tollgroup.com'
	cSendTo = 'Mariel.Calello@tollgroup.com, Diana.Landeros@Tollgroup.com, Abigail.Melaika@tollgroup.com'
	cCC = 'mbennett@fmiint.com'
	cSubject = ''
	cAttach = ''
	cBodyText = ''

	cCOMPUTERNAME = ''
	cUSERNAME = ''

	FUNCTION INIT
		IF NOT DODEFAULT()
			RETURN .F.
		ENDIF
		WITH THIS
			CLEAR
			*SET RESOURCE OFF
			CLOSE DATA
			SET CENTURY ON
			SET DECIMALS TO 0
			SET DATE AMERICAN
			SET HOURS TO 24
			SET ANSI ON
			SET TALK OFF
			SET DELETED ON
			SET CONSOLE OFF
			SET EXCLUSIVE OFF
			SET SAFETY OFF
			SET EXACT OFF
			SET STATUS BAR ON
			SET SYSMENU OFF
			SET ENGINEBEHAVIOR 70
			_VFP.AUTOYIELD = .lAutoYield
			.cCOMPUTERNAME = GETENV("COMPUTERNAME")
			.cUSERNAME = GETENV("USERNAME")
			IF .lTestMode THEN
				.cSendTo = 'mbennett@fmiint.com'
				.cLogFile = 'F:\UTIL\KRONOS\Logfiles\KronosTempHoursByWorksite_log_TESTMODE.txt'
			ENDIF
			.lLoggingIsOn = .lLoggingIsOn AND NOT EMPTY(.cLogFile)
			IF .lLoggingIsOn THEN
				SET ALTERNATE TO (.cLogFile) ADDITIVE
				SET ALTERNATE ON
			ENDIF
		ENDWITH
	ENDFUNC


	FUNCTION DESTROY
		WITH THIS
			IF .lLoggingIsOn  THEN
				SET ALTERNATE OFF
				SET ALTERNATE TO
			ENDIF
		ENDWITH
		DODEFAULT()
	ENDFUNC


	FUNCTION MAIN
		WITH THIS
			LOCAL lnNumberOfErrors, lcSQL, lcSQL2, lcSQL3, ldToday, ldFromDate, ldToDate, lcSQLFromDate, lcSQLToDate
			LOCAL lcMonthYear, lcOutputFileSummary, lcDiv59Where, lcOutputFileMichaels, lcOutputFileBBB
			LOCAL oExcel, oWorkbook, oWorksheet, lnRow, lcRow, lcOutputFileOODETAIL, lcOutputFileOOSUMMARY

			TRY
				lnNumberOfErrors = 0

				.TrackProgress('Kronos Temp Hours by Worksite Report process started....', LOGIT+SENDIT+NOWAITIT)
				.TrackProgress('COMPUTERNAME = ' + .cCOMPUTERNAME, LOGIT+SENDIT)
				.TrackProgress('USERNAME = ' + .cUSERNAME, LOGIT+SENDIT)
				.TrackProgress('PROJECT = KRONOSTEMPHOURSBYWORKSITE', LOGIT+SENDIT)
				IF .lTestMode THEN
					.TrackProgress('                        TEST MODE                        ', LOGIT+SENDIT)
				ENDIF

				ldToday = .dToday
				ldToDate = ldToday - DAY(ldToday)  && this is last day of prior month
				ldFromDate = GOMONTH(ldToDate + 1,-1)

*!*	**** to force other date range	**************************************************			
*!*	ldFromDate = {^2017-01-01}
*!*	ldToDate   = {^2017-12-22}
*!*	**********************************************************************************
				

				lcDiv59Where = "" 
				
				lcMonthYear = PROPER(CMONTH(ldFromDate)) + " " + TRANSFORM(YEAR(ldFromDate))
				lcMonthYear2 = TRANSFORM(ldFromDate) + " - " + TRANSFORM(ldToDate)

				.cSubject = 'Kronos Temp Hours by Worksite Report for: ' + lcMonthYear2
				lcOutputFileSummary = "F:\UTIL\KRONOS\TEMPTIMEREPORTS\SUMMARY TEMP HOURS BY WORKSITE FOR " + lcMonthYear + ".XLSX"
				lcOutputFileOODETAIL = "F:\UTIL\KRONOS\TEMPTIMEREPORTS\OWNEROP DETAIL FOR " + lcMonthYear + ".XLS"
				lcOutputFileOOSUMMARY = "F:\UTIL\KRONOS\TEMPTIMEREPORTS\OWNEROP SUMMARY FOR " + lcMonthYear + ".XLS"
				
				lcSQLFromDate = .GetSQLStartDateTimeString( ldFromDate )
				lcSQLToDate = .GetSQLEndDateTimeString( ldToDate )	

				.GetOwnerOpData( ldFromDate, ldToDate )	
				
				SELECT lcMonthYear AS DATERANGE, DRIVER, MILEAGE, WORKSITE ; 
					FROM ODRIVERS ;
					INTO CURSOR CUROODETAIL ;
					WHERE FLAGGED ;
					ORDER BY DRIVER
				
				SELECT CUROODETAIL
				COPY TO (lcOutputFileOODETAIL) XL5
				
				SELECT WORKSITE, COUNT(*) AS HEADCOUNT, SUM(MILEAGE) AS MILEAGE ;
					FROM ODRIVERS ;
					INTO CURSOR CUROOSUMMARY ;
					GROUP BY WORKSITE ;
					WHERE FLAGGED ;
					ORDER BY WORKSITE
				
				SELECT CUROOSUMMARY
				COPY TO (lcOutputFileOOSUMMARY) XL5
				
				USE IN ODRIVERS
				
							
				*THROW
				
				IF USED('CURWTKHOURS') THEN
					USE IN CURWTKHOURS
				ENDIF
				IF USED('CURWTKHOURSPRE') THEN 
					USE IN CURWTKHOURSPRE
				ENDIF

			SET TEXTMERGE ON

			TEXT TO	lcSQL NOSHOW

SELECT
D.LABORLEV4NM AS WSITECODE,
D.LABORLEV4DSC AS WSITENAME,
COUNT(DISTINCT C.PERSONNUM) AS HEADCNT,
(SUM(A.DURATIONSECSQTY) / 3600.00) AS TOTHOURS
FROM WFCTOTAL A
JOIN WTKEMPLOYEE B
ON B.EMPLOYEEID = A.EMPLOYEEID
JOIN PERSON C
ON C.PERSONID = B.PERSONID
JOIN LABORACCT D
ON D.LABORACCTID = A.LABORACCTID
WHERE (A.APPLYDTM >= <<lcSQLFromDate>>)
AND (A.APPLYDTM <= <<lcSQLToDate>>)<<lcDiv59Where>>
AND (D.LABORLEV1NM = 'TMP')
GROUP BY D.LABORLEV4NM, D.LABORLEV4DSC
ORDER BY 1, 2

			ENDTEXT
			
			SET TEXTMERGE OFF

*!*	* sql below is to get #s for Michael's at Edison; DIV 68				

*!*				SET TEXTMERGE ON

*!*				TEXT TO	lcSQL2 NOSHOW

*!*	SELECT
*!*	D.LABORLEV4NM AS WSITECODE,
*!*	D.LABORLEV4DSC AS WSITENAME,
*!*	COUNT(DISTINCT C.PERSONNUM) AS HEADCNT,
*!*	(SUM(A.DURATIONSECSQTY) / 3600.00) AS TOTHOURS
*!*	FROM WFCTOTAL A
*!*	JOIN WTKEMPLOYEE B
*!*	ON B.EMPLOYEEID = A.EMPLOYEEID
*!*	JOIN PERSON C
*!*	ON C.PERSONID = B.PERSONID
*!*	JOIN LABORACCT D
*!*	ON D.LABORACCTID = A.LABORACCTID
*!*	WHERE (A.APPLYDTM >= <<lcSQLFromDate>>)
*!*	AND (A.APPLYDTM <= <<lcSQLToDate>>)
*!*	AND (D.LABORLEV2NM = '68') AND (D.LABORLEV5NM = 'D') 
*!*	AND (D.LABORLEV1NM = 'TMP')
*!*	GROUP BY D.LABORLEV4NM, D.LABORLEV4DSC
*!*	ORDER BY 1, 2

*!*				ENDTEXT
*!*				
*!*				SET TEXTMERGE OFF

*!*	* sql below is to get #s for Bed Bath & Beyond at Fontana location; DIV 57				

*!*				SET TEXTMERGE ON

*!*				TEXT TO	lcSQL3 NOSHOW

*!*	SELECT
*!*	D.LABORLEV4NM AS WSITECODE,
*!*	D.LABORLEV4DSC AS WSITENAME,
*!*	COUNT(DISTINCT C.PERSONNUM) AS HEADCNT,
*!*	(SUM(A.DURATIONSECSQTY) / 3600.00) AS TOTHOURS
*!*	FROM WFCTOTAL A
*!*	JOIN WTKEMPLOYEE B
*!*	ON B.EMPLOYEEID = A.EMPLOYEEID
*!*	JOIN PERSON C
*!*	ON C.PERSONID = B.PERSONID
*!*	JOIN LABORACCT D
*!*	ON D.LABORACCTID = A.LABORACCTID
*!*	WHERE (A.APPLYDTM >= <<lcSQLFromDate>>)
*!*	AND (A.APPLYDTM <= <<lcSQLToDate>>)
*!*	AND (D.LABORLEV2NM = '57') AND (D.LABORLEV5NM = 'T') 
*!*	AND (D.LABORLEV1NM = 'TMP')
*!*	GROUP BY D.LABORLEV4NM, D.LABORLEV4DSC
*!*	ORDER BY 1, 2

*!*				ENDTEXT
*!*				
*!*				SET TEXTMERGE OFF

					
				IF .lTestMode THEN
					.TrackProgress('', LOGIT+SENDIT)
					.TrackProgress('lcSQL =' + lcSQL, LOGIT+SENDIT)
				ENDIF

				.nSQLHandle = SQLCONNECT("WTK2","sa","kr0n1tes")

				IF .nSQLHandle > 0 THEN

					*!*	IF .ExecSQL(lcSQL2, 'CURMICHAELS', RETURN_DATA_MANDATORY) THEN
					*!*		*SELECT CURMICHAELS
					*!*		*browse
					*!*		SELECT CURMICHAELS
					*!*		SCAN
					*!*			REPLACE CURMICHAELS.WSITECODE WITH 'EDS', CURMICHAELS.WSITENAME WITH 'NJ - EDISON' IN CURMICHAELS
					*!*		ENDSCAN

					*!*		SELECT lcMonthYear2 AS DATERANGE, ;
					*!*			ALLTRIM(WSITECODE) AS WORKSITE, ;
					*!*			LEFT(WSITENAME,30) AS SITENAME, ;
					*!*			'Warehouse' AS JOBDESC, ;
					*!*			HEADCNT, ;
					*!*			TOTHOURS ;
					*!*			FROM CURMICHAELS ;
					*!*			INTO CURSOR CURMICHAELS2 ;
					*!*			ORDER BY 1 ;
					*!*			READWRITE

					*!*		lcOutputFileMichaels = "F:\UTIL\KRONOS\TEMPTIMEREPORTS\TEMP-INFO-MICHAELS-EDISON FOR " + lcMonthYear + ".XLS"

					*!*		SELECT CURMICHAELS2
					*!*		COPY TO (lcOutputFileMichaels) XL5
					*!*		.TrackProgress('lcOutputFileMichaels =' + lcOutputFileMichaels, LOGIT+SENDIT)
					*!*		
					*!*	ENDIF					

					*!*	IF .ExecSQL(lcSQL3, 'CURBBB', RETURN_DATA_MANDATORY) THEN
					*!*		*SELECT CURBBB
					*!*		*browse
					*!*		SELECT CURBBB
					*!*		SCAN
					*!*			REPLACE CURBBB.WSITECODE WITH 'FON', CURMICHAELS.WSITENAME WITH 'CA - FONTANA' IN CURBBB
					*!*		ENDSCAN

					*!*		SELECT lcMonthYear2 AS DATERANGE, ;
					*!*			ALLTRIM(WSITECODE) AS WORKSITE, ;
					*!*			LEFT(WSITENAME,30) AS SITENAME, ;
					*!*			'Warehouse' AS JOBDESC, ;
					*!*			HEADCNT, ;
					*!*			TOTHOURS ;
					*!*			FROM CURBBB ;
					*!*			INTO CURSOR CURBBB2 ;
					*!*			ORDER BY 1 ;
					*!*			READWRITE

					*!*		lcOutputFileBBB = "F:\UTIL\KRONOS\TEMPTIMEREPORTS\TEMP-INFO-BBB-FONTATA FOR " + lcMonthYear + ".XLS"

					*!*		SELECT CURBBB2
					*!*		COPY TO (lcOutputFileBBB) XL5
					*!*		.TrackProgress('lcOutputFileBBB =' + lcOutputFileBBB, LOGIT+SENDIT)
					*!*		
					*!*	ENDIF
					
					IF .ExecSQL(lcSQL, 'CURWTKHOURSPRE', RETURN_DATA_MANDATORY) THEN
					
					
						*!*	SELECT lcMonthYear2 AS DATERANGE, ;
						*!*		ALLTRIM(WSITECODE) AS WORKSITE, ;
						*!*		LEFT(WSITENAME,30) AS SITENAME, ;
						*!*		'Warehouse' AS JOBDESC, ;
						*!*		HEADCNT, ;
						*!*		TOTHOURS ;
						*!*		FROM CURWTKHOURSPRE ;
						*!*		INTO CURSOR CURWTKHOURS ;
						*!*		ORDER BY 1 ;
						*!*		READWRITE
						
*!*							SET STEP ON 
						
						* ADD REC FOR SP3 - WILMINGTON YARD IF NOT THERE
						SELECT CURWTKHOURSPRE
						LOCATE FOR ALLTRIM(WSITECODE) == 'SP3'
						IF NOT FOUND() THEN
							m.WSITECODE = 'SP3'
							m.DATERANGE = lcMonthYear2
							INSERT INTO CURWTKHOURSPRE FROM MEMVAR
						ENDIF						
						
						SELECT CURWTKHOURSPRE
						LOCATE
*!*	browse

						SELECT lcMonthYear2 AS DATERANGE, ;
							ALLTRIM(A.WSITECODE) AS WORKSITE, ;
							NVL(B.DESC,'') AS SITENAME, ;
							NVL(B.OSHANUM,'') AS SITENUM, ;
							'Warehouse' AS JOBDESC, ;
							HEADCNT, ;
							TOTHOURS, ;
							000 AS GHEADCNT, ;
							000000.00 AS GHOURS ;
							FROM CURWTKHOURSPRE A ;
							LEFT OUTER JOIN ;
							F:\UTIL\HR\DATA\WORKSITES B ;
							ON B.WORKSITE = A.WSITECODE ;
							INTO CURSOR CURWTKHOURS ;
							ORDER BY 1, 2 ;
							READWRITE
							
						* FILL IN DEFAULTS FOR GUARDS, WHICH DIANA LANDEROS SAYS WON'T CHANGE OFTEN, SO PREFILL THEM 3/7/2017
						SELECT CURWTKHOURS
						
						LOCATE FOR ALLTRIM(WORKSITE) == 'CA1'
						IF FOUND() THEN
							REPLACE CURWTKHOURS.GHEADCNT WITH 3, CURWTKHOURS.GHOURS WITH 672 IN CURWTKHOURS	
						ENDIF
						
						LOCATE FOR ALLTRIM(WORKSITE) == 'CA2'
						IF FOUND() THEN
							REPLACE CURWTKHOURS.GHEADCNT WITH 3, CURWTKHOURS.GHOURS WITH 672 IN CURWTKHOURS	
						ENDIF
						
						LOCATE FOR ALLTRIM(WORKSITE) == 'CA4'
						IF FOUND() THEN
							REPLACE CURWTKHOURS.GHEADCNT WITH 7, CURWTKHOURS.GHOURS WITH 1872 IN CURWTKHOURS	
						ENDIF
						
						LOCATE FOR ALLTRIM(WORKSITE) == 'KY1'
						IF FOUND() THEN
							REPLACE CURWTKHOURS.GHEADCNT WITH 5, CURWTKHOURS.GHOURS WITH 1200 IN CURWTKHOURS	
						ENDIF
						
						LOCATE FOR ALLTRIM(WORKSITE) == 'NJ1'
						IF FOUND() THEN
							REPLACE CURWTKHOURS.GHEADCNT WITH 4, CURWTKHOURS.GHOURS WITH 1512 IN CURWTKHOURS	
						ENDIF
						
						LOCATE FOR ALLTRIM(WORKSITE) == 'SP2'
						IF FOUND() THEN
							REPLACE CURWTKHOURS.GHEADCNT WITH 7, CURWTKHOURS.GHOURS WITH 1568 IN CURWTKHOURS	
						ENDIF
						
						LOCATE FOR ALLTRIM(WORKSITE) == 'SP3'
						IF FOUND() THEN
							REPLACE CURWTKHOURS.GHEADCNT WITH 3, CURWTKHOURS.GHOURS WITH 672 IN CURWTKHOURS	
						ENDIF
						
						
						* add rec for Carteret Transport Express (i.e. owner ops) per Diana Landeros 10/23/2017 MB - THIS IS NOT BASED ON TEMP DATA IN ANY WAY
						SELECT CURWTKHOURS
						APPEND BLANK
						REPLACE CURWTKHOURS.SITENAME WITH 'Carteret Transport Express', CURWTKHOURS.SITENUM WITH 'US0035', ;
							CURWTKHOURS.HEADCNT WITH 0, CURWTKHOURS.TOTHOURS WITH 0, CURWTKHOURS.GHEADCNT WITH 0, CURWTKHOURS.GHOURS WITH 0 IN CURWTKHOURS


						WAIT WINDOW NOWAIT "Opening Excel..."
						oExcel = CREATEOBJECT("excel.application")
						oExcel.VISIBLE = .F.
						oExcel.DisplayAlerts = .F.
						oWorkbook = oExcel.workbooks.OPEN("F:\UTIL\KRONOS\TEMPLATES\SUMMARY TEMP HOURS BY WORKSITE 01.XLSX")
						oWorkbook.SAVEAS(lcOutputFileSummary)
						oWorksheet = oWorkbook.Worksheets[1]
						
						lnRow = 2
						SELECT CURWTKHOURS
						SCAN
							lnRow = lnRow + 1
							lcRow = LTRIM(STR(lnRow))
							oWorksheet.RANGE("A" + lcRow).VALUE = CURWTKHOURS.DATERANGE
							oWorksheet.RANGE("B" + lcRow).VALUE = CURWTKHOURS.WORKSITE
							oWorksheet.RANGE("C" + lcRow).VALUE = CURWTKHOURS.SITENAME
							oWorksheet.RANGE("D" + lcRow).VALUE = CURWTKHOURS.SITENUM
							oWorksheet.RANGE("E" + lcRow).VALUE = CURWTKHOURS.JOBDESC
							oWorksheet.RANGE("F" + lcRow).VALUE = CURWTKHOURS.HEADCNT
							oWorksheet.RANGE("G" + lcRow).VALUE = CURWTKHOURS.TOTHOURS
							oWorksheet.RANGE("H" + lcRow).VALUE = CURWTKHOURS.GHEADCNT
							oWorksheet.RANGE("I" + lcRow).VALUE = CURWTKHOURS.GHOURS  
						ENDSCAN
						
						oWorkbook.SAVE()
						oExcel.QUIT()
						RELEASE ALL LIKE oWorkbook
						RELEASE ALL LIKE oExcel


				
 
*!*							SELECT CURWTKHOURS
*!*	 						COPY TO (lcOutputFileSummary) XL5
*!*							.TrackProgress('Output to spreadsheet: ' + lcOutputFileSummary, LOGIT+SENDIT+NOWAITIT)

						IF FILE(lcOutputFileSummary) THEN
							* attach output file to email
							.cAttach = lcOutputFileSummary
							.cBodyText = "See attached reports." + ;
								CRLF + CRLF + "<report log follows>" + ;
								CRLF + .cBodyText
						ELSE
							.TrackProgress('ERROR: unable to email spreadsheet: ' + lcOutputFileSummary, LOGIT+SENDIT+NOWAITIT)
						ENDIF

						IF FILE(lcOutputFileOODETAIL) THEN
							* attach output file to email
							.cAttach = .cAttach + "," + lcOutputFileOODETAIL
						ELSE
							.TrackProgress('ERROR: unable to email spreadsheet: ' + lcOutputFileOODETAIL, LOGIT+SENDIT+NOWAITIT)
						ENDIF

						IF FILE(lcOutputFileOOSUMMARY) THEN
							* attach output file to email
							.cAttach = .cAttach + "," + lcOutputFileOOSUMMARY
						ELSE
							.TrackProgress('ERROR: unable to email spreadsheet: ' + lcOutputFileOOSUMMARY, LOGIT+SENDIT+NOWAITIT)
						ENDIF

					ENDIF  &&  .ExecSQL(lcSQL, 'CURWTKEMPLOYEE', RETURN_DATA_MANDATORY)

					CLOSE DATABASES ALL

				ELSE
					* connection error
					.TrackProgress('Unable to connect to WTK System.', LOGIT+SENDIT)
				ENDIF   &&  .nSQLHandle > 0
				
			CATCH TO loError
			
				IF TYPE('oExcel') = "O" AND NOT oExcel = NULL THEN
					oExcel.QUIT()
				ENDIF

				.TrackProgress('There was an error.',LOGIT+SENDIT)
				.TrackProgress('The Error # is: ' + TRANSFORM(loError.ERRORNO), LOGIT+SENDIT)
				.TrackProgress('The Error Message is: ' + TRANSFORM(loError.MESSAGE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred in Program: ' + TRANSFORM(loError.PROCEDURE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred at line #: ' + TRANSFORM(loError.LINENO), LOGIT+SENDIT)
				lnNumberOfErrors = lnNumberOfErrors + 1
				CLOSE DATABASES ALL

			ENDTRY

			CLOSE DATA
			WAIT CLEAR
			***************** INTERNAL email results ******************************
			.TrackProgress('About to send status email.',LOGIT)
			.TrackProgress('==================================================================================================================', SENDIT)
			.TrackProgress('The logfile is: ' + .cLogFile, SENDIT)
			.TrackProgress('Kronos Temp Hours by Worksite Report process started: ' + .cStartTime, LOGIT+SENDIT)
			.TrackProgress('Kronos Temp Hours by Worksite Report process finished: ' + TTOC(DATETIME()), LOGIT+SENDIT)

			IF .lSendInternalEmailIsOn THEN
				* try to trap error from not having dartmail dll's registered on user's pc...
				TRY
					DO FORM dartmail2 WITH .cSendTo,.cFrom,.cSubject,.cCC,.cAttach,.cBodyText,"A"
					.TrackProgress('Sent status email.',LOGIT)
				CATCH TO loError
					*=MESSAGEBOX('There was an error sending the status email. Please contact Mark Bennett.',0+64,"KRONOS Temp Time")
					.TrackProgress('There was an error sending the status email.',LOGIT)
					.TrackProgress('The Error # is: ' + TRANSFORM(loError.ERRORNO), LOGIT)
					.TrackProgress('The Error Message is: ' + TRANSFORM(loError.MESSAGE), LOGIT)
					.TrackProgress('The Error occurred in Program: ' + TRANSFORM(loError.PROCEDURE), LOGIT)
					.TrackProgress('The Error occurred at line #: ' + TRANSFORM(loError.LINENO), LOGIT)
					lnNumberOfErrors = lnNumberOfErrors + 1
					CLOSE DATA
				ENDTRY

			ELSE
				.TrackProgress('Did not send status email: THIS.lSendInternalEmailIsOn = FALSE.',LOGIT)
			ENDIF

		ENDWITH
		RETURN
	ENDFUNC && main
	
	
	PROCEDURE GetOwnerOpData
		LPARAMETERS tdStartDate, tdEndDate

		* Get count of West, East, Express OwnerOps - code stolen from M:\DARREN\OO.PRG :)

		LOCAL ldStartDate, ldEndDate, lnMileage

		ldStartDate = tdStartDate
		ldEndDate = tdEndDate

		lnMileage = 0.0
		
		xsqlexec("select distinct driver, ss_num, company, active from odrivers","odriverspre",,"oo")
		
		
		SELECT *, 000000.00 as mileage, SPACE(3) AS WORKSITE, SPACE(1) AS OFFICE, .F. AS FLAGGED ;
			from odriverspre ;
			into CURSOR odrivers ;
			ORDER BY active desc, driver ;
			READWRITE
			
		* using 'delivered' as the date criterion below, only because that is used in the OO pay report as the date filter
		
		*xsqlexec("select * from fxtrips where dvrcompany='O'","fxtrips",,"fx")
		xsqlexec("select * from fxtrips where dvrcompany='O' and between(delivered,{" + DTOC(ldStartDate) + "},{" + DTOC(ldEndDate) + "})","fxtrips",,"fx")
		
		
*!*			xsqlexec("select * from fxpay where ownersid#0 and between(delivered,{" + DTOC(ldStartDate) + "},{" + DTOC(ldEndDate) + "})","fxpay",,"fx")
*!*			
*!*			SELECT fxpay
*!*			COPY TO c:\a\fxpay

			
SELECT odrivers
INDEX ON ALLTRIM(ss_num) TAG ss_num
INDEX ON ACTIVE DESCENDING TAG ACTIVE
SET ORDER TO ACTIVE

*!*	SELECT fxtrips
*!*	COPY TO c:\a\fxtrips

SELECT fxtrips
	SCAN FOR NOT EMPTY(DVR1SS)
	SELECT odrivers
	LOCATE FOR ALLTRIM(ss_num) == ALLTRIM(fxtrips.dvr1ss)
	IF FOUND() THEN
		* set flagged = .t. and add to mileage
		REPLACE odrivers.flagged WITH .T., odrivers.mileage WITH odrivers.mileage + fxtrips.traveled, odrivers.office WITH fxtrips.office
	ELSE
		.TrackProgress('====> ERROR: could not find this ss_num in Odrivers: ' + ALLTRIM(fxtrips.dvr1ss),LOGIT+SENDIT)	
	ENDIF
ENDSCAN


* repeat for 2nd ss_num
SELECT fxtrips
	SCAN FOR NOT EMPTY(DVR2SS)
	SELECT odrivers
	LOCATE FOR ALLTRIM(ss_num) == ALLTRIM(fxtrips.dvr2ss)
	IF FOUND() THEN
		* set flagged = .t. and add to mileage
		REPLACE odrivers.flagged WITH .T., odrivers.mileage WITH odrivers.mileage + fxtrips.traveled, odrivers.office WITH fxtrips.office
	ELSE
		.TrackProgress('====> ERROR: could not find this ss_num in Odrivers: ' + ALLTRIM(fxtrips.dvr2ss),LOGIT+SENDIT)	
	ENDIF
ENDSCAN

SELECT odrivers
SCAN
	IF ALLTRIM(odrivers.office) == 'N' then
		REPLACE odrivers.worksite WITH 'NJ1' IN odrivers
	ENDIF
	IF ALLTRIM(odrivers.office) == 'C' then
		REPLACE odrivers.worksite WITH 'SP2' IN odrivers
	ENDIF
	IF ALLTRIM(odrivers.office) == 'M' then
		REPLACE odrivers.worksite WITH 'FL1' IN odrivers
	ENDIF
ENDSCAN

*!*	SELECT odrivers
*!*	COPY TO c:\a\odrivers_mileage_dispatched.xls XL5 FOR flagged


*!*			*USE F:\ownerop\oodata\odrivers IN 0
*!*			USE F:\wo\wodata\tdaily IN 0
*!*			*USE F:\EXPRESS\fxdata\fxtrips IN 0

*!*			CREATE CURSOR xrpt (DRIVER c(30), ss_num c(11), ltn N(3), ltc N(3), ex N(3))
*!*			INDEX ON ss_num TAG ss_num
*!*			INDEX ON DRIVER TAG DRIVER

*!*			SELECT tdaily
*!*			SCAN FOR BETWEEN(dispatched,ldStartDate,ldEndDate) AND !EMPTY(dis_driver)
*!*				SELECT odrivers
*!*				LOCATE FOR ACTIVE AND PADR(LEFT(TRIM(FIRST)+" "+LAST,30),30)=tdaily.dis_driver
*!*				IF FOUND()
*!*					IF !SEEK(odrivers.ss_num,"xrpt","ss_num")
*!*						INSERT INTO xrpt (DRIVER, ss_num) VALUES (odrivers.DRIVER, odrivers.ss_num)
*!*					ENDIF
*!*					xoffice=tdaily.office
*!*					REPLACE lt&xoffice WITH xrpt.lt&xoffice+1 IN xrpt
*!*				ENDIF
*!*			ENDSCAN

*!*			SELECT fxtrips
*!*			SCAN FOR BETWEEN(delivered,ldStartDate,ldEndDate) AND dvrcompany="O" AND !deadhead
*!*				FOR i=1 TO 2
*!*					ii=TRANSFORM(i)
*!*					IF !EMPTY(dvr&ii.ss)
*!*						IF SEEK(dvr&ii.ss,"odrivers","ss_num")

*!*							lnMileage = lnMileage + fxtrips.traveled

*!*							IF !SEEK(odrivers.ss_num,"xrpt","ss_num")
*!*								INSERT INTO xrpt (DRIVER, ss_num) VALUES (odrivers.DRIVER, odrivers.ss_num)
*!*							ENDIF
*!*							REPLACE ex WITH xrpt.ex+1 IN xrpt
*!*						ENDIF
*!*					ENDIF
*!*				NEXT
*!*			ENDSCAN

*!*	SELECT xrpt
*!*	GO TOP
*!*	BROWSE

*!*			*!*	SELECT COUNT(DISTINCT ss_num) AS HEADCOUNT ;
*!*			*!*		FROM xrpt ;
*!*			*!*		INTO CURSOR CUREASTOO ;
*!*			*!*		WHERE ltn > 0

*!*			*!*	SELECT CUREASTOO
*!*			*!*	GOTO TOP
*!*			*!*	m.ECSTOOPS = CUREASTOO.HEADCOUNT

*!*			*!*	SELECT COUNT(DISTINCT ss_num) AS HEADCOUNT ;
*!*			*!*		FROM xrpt ;
*!*			*!*		INTO CURSOR CURWESTOO ;
*!*			*!*		WHERE ltc > 0

*!*			*!*	SELECT CURWESTOO
*!*			*!*	GOTO TOP
*!*			*!*	m.WCSTOOPS = CURWESTOO.HEADCOUNT

*!*			*!*	SELECT COUNT(DISTINCT ss_num) AS HEADCOUNT ;
*!*			*!*		FROM xrpt ;
*!*			*!*		INTO CURSOR CURXPROO ;
*!*			*!*		WHERE ex > 0

*!*			*!*	SELECT CURXPROO
*!*			*!*	GOTO TOP
*!*			*!*	m.XPRSOOPS = CURXPROO.HEADCOUNT


*!*	*!*			.nOOMileage = lnMileage
*!*	*!*			.nOOHeadCnt = RECCOUNT( 'xrpt' )

*!*	*!*	SET STEP ON

		USE IN odriverspre
		*USE IN odrivers
		*USE IN tdaily
		USE IN fxtrips

		RETURN
	ENDPROC


	FUNCTION GetSQLStartDateTimeString
		* returns a timestamp string for use in SQL queries
		LPARAMETERS tdDate
		LOCAL lcSetDate, lcSQLDateTimeString
		lcSetDate = SET('DATE')
		SET DATE YMD
		lcSQLDateTimeString   = "{ts '" + TRANSFORM(YEAR(tdDate)) + "-" + PADL(MONTH(tdDate),2,"0") + "-" + PADL(DAY(tdDate),2,"0") + " 00:00:00.0'}"
		SET DATE &lcSetDate.
		RETURN lcSQLDateTimeString
	ENDFUNC
	

	FUNCTION GetSQLEndDateTimeString
		* returns a timestamp string for use in SQL queries
		LPARAMETERS tdDate
		LOCAL lcSetDate, lcSQLDateTimeString
		lcSetDate = SET('DATE')
		SET DATE YMD
		lcSQLDateTimeString = "{ts '" + TRANSFORM(YEAR(tdDate)) + "-" + PADL(MONTH(tdDate),2,"0") + "-" + PADL(DAY(tdDate),2,"0") + " 23:59:59.9'}"
		SET DATE &lcSetDate.
		RETURN lcSQLDateTimeString
	ENDFUNC
	

	FUNCTION ExecSQL
		LPARAMETERS tcSQL, tcCursorName, tlNoDataReturnedIsOkay
		LOCAL llRetval, lnResult
		WITH THIS
			* close target cursor if it's open
			IF USED(tcCursorName)
				USE IN (tcCursorName)
			ENDIF
			WAIT WINDOW "Fetching remote data; please wait..." NOWAIT
			lnResult = SQLEXEC(.nSQLHandle, tcSQL, tcCursorName)
			llRetval = ( lnResult > 0 )
			IF llRetval THEN
				* see if any data came back
				IF NOT tlNoDataReturnedIsOkay THEN
					IF (NOT USED(tcCursorName)) OR EOF(tcCursorName) THEN
						llRetval = .F.
						.TrackProgress('No data was returned by this query: ' +CRLF + tcSQL, LOGIT+SENDIT)
					ENDIF
				ENDIF
			ELSE
				.TrackProgress("Result Code [" + TRANSFORM(lnResult) + "] returned by this query: " + tcSQL, LOGIT+SENDIT)
			ENDIF
			WAIT CLEAR
			RETURN llRetval
		ENDWITH
	ENDFUNC


	PROCEDURE TrackProgress
		* do any combination of Wait Window, writing to logfile, and adding to body of email,
		* based on nFlags parameter.
		LPARAMETERS tcExpression, tnFlags
		WITH THIS
			IF BITAND(tnFlags,LOGIT) = LOGIT THEN
				IF .lLoggingIsOn THEN
					?
					? .cProcessName + "  " + TTOC(DATETIME()) + ": " + tcExpression
				ENDIF
			ENDIF
			IF .lWaitWindowIsOn THEN
				IF BITAND(tnFlags,NOWAITIT) = NOWAITIT THEN
					WAIT WINDOW tcExpression NOWAIT
				ENDIF
				IF BITAND(tnFlags,WAITIT) = WAITIT THEN
					WAIT WINDOW tcExpression TIMEOUT .nWaitWindowTimeout
				ENDIF
			ENDIF
			IF BITAND(tnFlags,SENDIT) = SENDIT THEN
				IF .lSendInternalEmailIsOn THEN
					.cBodyText = .cBodyText + tcExpression + CRLF + CRLF
				ENDIF
			ENDIF
		ENDWITH
	ENDPROC  &&  TrackProgress
	

ENDDEFINE
