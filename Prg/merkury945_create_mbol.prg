*!* Merkury 945 (Whse. Shipping Advice) Creation Program
*!* Creation Date: 12.04.2014 by Joe (Derived from JDL945_create program)

PARAMETERS cOffice,cBOL

PUBLIC ARRAY thisarray(1)
PUBLIC c_CntrlNum,c_GrpCntrlNum,cReturned,cWO_Num,cWO_NumStr,nWO_Num1,lTesting,lDoUPC,nAcctNum,cRunningProg,cMod,cMBOL,cSubBOLDetail,lOverflow
PUBLIC lEmailMerk,lMerkFilesout,dDateTimeCal,nFilenum,nWO_Xcheck,nOutshipid,cCharge,lDoSQL,dapptnum,tfrom,cEDIType,lParcelType,cFilenameShort
PUBLIC cCity,cState,cZip,nOrigSeq,nOrigGrpSeq,tsendto,tcc,tsendtoerr,tccerr,cISA_Num,cErrMsg,cSCAC,lDoCatch,cFilenameOut,cFilenameArch

lTesting   = .F.  && Set to .t. for testing
lTestinput = .F. &&  Set to .t. for test input files only!

IF lTesting
	CLOSE DATABASES ALL
	DO m:\dev\prg\lookups
ENDIF

TRY
*	Set Step On
	nAcctNum = 6561
	DO m:\dev\prg\_setvars WITH lTesting

	IF (EMPTY(cOffice) OR VARTYPE(cOffice) # "C")
		IF lTesting
			cOffice = "C"
		ELSE
			WAIT WINDOW "Missing Office" TIMEOUT 2
			THROW
		ENDIF
	ENDIF

	IF lTestinput
		cUseFolder = "F:\WHP\WHDATA\"
	ELSE
		xReturn = "XXX"
		xReturn = wf(cOffice,nAcctNum)
		cUseFolder = UPPER(xReturn)
	ENDIF
	cMBOL = ""
	cMod = IIF(cOffice = "N","I","2")
	goffice = cMod
	gMasterOffice = cOffice

	cEDIType = "945"
	lParcelType = .F.
	cSubBOLDetail = ""

	cOVBOL = "08447020001304351"

	lManualRun = .F.
	lDoSQL = .T.
	lPick = .T.
	lPrepack = .F.
	lJCP = .F.
	lIsError = .F.
	lDoCatch = .T.
	cShip_ref = ""
	lCloseOutput = .T.
	nFilenum = 0
	cCharge = "0.00"
	cWO_NumStr = ""
	cWO_NumList = ""
	cString = ""
	cErrMsg = ""
	nPTCtnTot = 0
	cRunningProg = "merkury945_create_mbol"

	lFederated = .F.
	nOrigSeq = 0
	nOrigGrpSeq = 0
	cPPName = "MERKURY"
	WAIT WINDOW "At the start of "+cPPName+" 945 process..." NOWAIT
	tfrom = "TGF EDI Operations <toll-edi-ops@tollgroup.com>"
	tmessage=""
	tcc = ""
	lDoBOLGEN = .F.
	lSamples = .F.
	cBOL1 = ""
	lISAFlag = .T.
	lSTFlag = .T.
	nLoadid = 1
	lSQLMail = .F.
	lOverflow = .F.

	IF VARTYPE(cBOL) = "L"
		IF !lTesting AND !lTestinput
			WAIT WINDOW "BOL not provided...terminating" TIMEOUT 2
			lCloseOutput = .F.
			cErrMsg = "NO BOL"
			THROW
		ELSE
			CLOSE DATABASES ALL
			IF !USED('edi_trigger')
				cEDIFolder = "F:\3PL\DATA\"
				USE (cEDIFolder+"edi_trigger") IN 0 ALIAS edi_trigger
			ENDIF
*!* TEST DATA AREA
			cBOL = "08447020001245272"
			cTime = DATETIME()
			lFedEx = .F.
		ENDIF
	ENDIF

	CREATE CURSOR temp945 (accountid i,isa_num c(10),st_num c(10),wo_num i,bol_no c(20),ship_ref c(20),filename c(50),filedate T)
	cBOL=TRIM(cBOL)
*	ASSERT .F. MESSAGE "At Warehouse folder selection"
	cRetMsg = "X"

	IF lTestinput
		cUseFolder = "F:\WHP\WHDATA\"
	ENDIF

	lEmailMerk = .T.
	lTestMail = lTesting && Sends mail to Joe only
	lMerkFilesout = !lTesting && If true, copies output to FTP folder (default = .t.)
	lStandalone = lTesting

*lTestMail = .T.
*lMerkFilesout = .F.

	STORE "LB" TO cWeightUnit
	cfd = "*"
	csegd = "~"
	nSegCtr = 0
	cterminator = ">" && Used at end of ISA segment

	STORE cBOL TO cMBOL
	DO m:\dev\prg\swc_cutctns_gen WITH cBOL


	WAIT WINDOW "Now preparing tables and variables" NOWAIT NOCLEAR

*!* SET CUSTOMER CONSTANTS
	ccustname = "MERKURY"  && Customer Identifier
	cX12 = "004010"  && X12 Standards Set used
	cMailName = "Merkury"
	SELECT 0
	USE F:\3pl\DATA\mailmaster ALIAS mm
	LOCATE FOR mm.edi_type = "945" AND mm.office = cOffice AND mm.accountid = nAcctNum
	tsendto = IIF(mm.use_alt,sendtoalt,sendto)
	tcc = IIF(mm.use_alt,ccalt,cc)
	lcPath = ALLTRIM(mm.basepath)
	lcArchivePath = ALLTRIM(mm.archpath)
	LOCATE
	LOCATE FOR edi_type = "MISC" AND taskname = "GENERAL"
	tsendtoerr = IIF(mm.use_alt,sendtoalt,sendto)
	tccerr = IIF(mm.use_alt,ccalt,cc)
	tsendtotest = IIF(mm.use_alt,sendtoalt,sendto)
	tcctest = IIF(mm.use_alt,ccalt,cc)
	USE IN mm

	upcmastsql(6561)
	INDEX ON STYLE TAG STYLE

*!* SET OTHER CONSTANTS
	DO CASE
		CASE cOffice = "N"
			cCustLoc =  "NJ"
			cFMIWarehouse = ""
			cCustPrefix = "945j"
			cDivision = "New Jersey"
			cSF_Addr1  = "800 FEDERAL BLVD"
			cSF_CSZ    =  "CARTERET"+cfd+"NJ"+cfd+"07008"

		CASE cOffice = "M"
			cCustLoc =  "FL"
			cFMIWarehouse = ""
			cCustPrefix = "945f"
			cDivision = "Florida"
			cSF_Addr1  = "11400 NW 32ND AVE"
			cSF_CSZ    = "MIAMI"+cfd+"FL"+cfd+"33167"

		OTHERWISE
			cCustLoc =  "CA"
			cFMIWarehouse = ""
			cCustPrefix = "945c"
			cDivision = "California"
			cSF_Addr1  = "450 WESTMONT DRIVE"
			cSF_CSZ    = "SAN PEDRO"+cfd+"CA"+cfd+"90731"
	ENDCASE
	IF lTesting
		cCustPrefix = "945t"
	ENDIF

	cCustFolder = UPPER(ccustname)

	IF lTesting
*SET STEP ON
	ENDIF

	cMBOLFolder = 'F:\FTPUSERS\MERKURY\mbolhold\'
	CD &cMBOLFolder
	DELETE FILE *.*

	lNonEDI = .F.  && Assumes not a Non-EDI 945
	STORE "" TO lcKey
	STORE 0 TO alength,nLength
	cTime = DATETIME()
	cDelTime = SUBSTR(TTOC(cTime,1),9,4)
	cCarrierType = "M" && Default as "Motor Freight"
	dapptnum = ""

*!* SET OTHER CONSTANTS
	cString = ""

	csendqual = "ZZ"
	csendid = "FMIX"+cCustLoc

	crecqual = "12"
	crecid = "2129315055"

	cdate = DTOS(DATE())
	cOrig = "J"
	nSTCount = 0
	cStyle = ""
	cPTString = ""
	nTotCtnWt = 0
	nTotCtnCount = 0
	nUnitSum = 0
	cTargetStyle = ""

** PAD ID Codes
	csendidlong = PADR(csendid,15," ")
	crecidlong = PADR(crecid,15," ")

	useca("ackdata","wh")

	IF USED('OUTSHIP')
		USE IN outship
	ENDIF
	IF USED('outdet')
		USE IN outdet
	ENDIF

	xsqlexec("select * from outship where accountid = "+TRANSFORM(nAcctNum)+" and bol_no = '"+cBOL+"'",,,"wh")
	INDEX ON bol_no TAG bol_no
	INDEX ON ship_ref TAG ship_ref

	IF cMBOL = cOVBOL
		lOverflow =.T.
		WAIT WINDOW "This MBOL contains OV PT(s)...setting deleted off" TIMEOUT 2
		SET DELETED OFF
	ENDIF

	selectoutdet()
	SELECT outdet
	INDEX ON outdetid TAG outdetid
	IF DATETIME()<DATETIME(2017,11,15,20,00,00)
		LOCATE
		SET STEP ON
	ENDIF

	SELECT outship
	LOCATE
	=SEEK(cBOL,'outship','bol_no')
	IF !FOUND()
		THROW
	ENDIF

	cSCAC = ALLTRIM(outship.scac)

	IF lTesting
*SET STEP ON
	ENDIF

	IF USED('BL')
		USE IN bl
	ENDIF

	csq1 = [select * from bl where mblnum = ']+cMBOL+[' and accountid = ]+TRANSFORM(nAcctNum)
	xsqlexec(csq1,,,"wh")
	SELECT bol_no FROM bl INTO CURSOR tempbl1 GROUP BY 1

	IF lTesting OR lTestinput
*BROWSE
	ENDIF
	STORE RECCOUNT() TO nSubBillCnt
	USE IN tempbl1
	SELECT bl
	LOCATE

	SELECT 0
	CREATE CURSOR tempinfo (filenameshort c(50),subbol c(20),ptcount c(10),ptstring m)

*!* Mail sending for all MBOL 945s
*	ASSERT .F. MESSAGE "At creation of MBOL notice...>>DEBUG<<"
	tsubject2 = "NOTICE: MERKURY 945 MBOL Process"
	SELECT 0
	USE F:\3pl\DATA\mailmaster ALIAS mm SHARED
	LOCATE FOR mm.edi_type = "MISC" AND mm.taskname = "GENERAL"
	tsendto2 = IIF(mm.use_alt,mm.sendtoalt,mm.sendto)
	tcc2 = IIF(mm.use_alt,mm.ccalt,mm.cc)
	USE IN mm

	tattach2 = " "
	tmessage2 = "Potential files for MERKURY MBOL# "+cMBOL+"...monitor output for correct number of files."
	tmessage2 = tmessage2 + CHR(13) + "Expected number of 945 files: "+ALLTRIM(STR(nSubBillCnt))
	tmessage2 = tmessage2 + CHR(13) + "Processed at "+TTOC(DATETIME())
	DO FORM m:\dev\frm\dartmail2 WITH tsendto2,tfrom,tsubject2,tcc2,tattach2,tmessage2,"A"
*!* End Mailing section

	SELECT outshipid ;
		FROM outship ;
		WHERE bol_no = cMBOL ;
		AND accountid = nAcctNum ;
		INTO CURSOR tempsr
	SELECT tempsr
	WAIT WINDOW "There are "+ALLTRIM(STR(RECCOUNT()))+" outshipid's to check" NOWAIT

	SELECT outship
	IF !SEEK(cMBOL,'outship','bol_no')
		ASSERT .F. MESSAGE "at missing BOL point"
		WAIT WINDOW "BOL not found in OUTSHIP" TIMEOUT 2
		cErrMsg = "BOL NOT FOUND"
		THROW
	ENDIF

&& Added code blocks to the following to overcome BOL vs. WO selection issues, 12.23.2005

	IF lDoSQL
		cSQL="SQL3"
		SELECT outship
		LOCATE
		IF USED("sqlwomerk")
			USE IN sqlwomerk
		ENDIF
		IF FILE("F:\3pl\DATA\sqlwomerk.dbf")
			DELETE FILE "F:\3pl\DATA\sqlwomerk.dbf"
		ENDIF
		SELECT bol_no,wo_num ;
			FROM outship ;
			WHERE bol_no == cMBOL ;
			AND accountid = nAcctNum ;
			GROUP BY 1,2 ;
			ORDER BY 1,2 ;
			INTO DBF F:\3pl\DATA\sqlwomerk
		USE IN sqlwomerk
		SELECT 0
		USE F:\3pl\DATA\sqlwomerk ALIAS sqlwomerk
		LOCATE
		IF EOF()
			cErrMsg = "NO SQL DATA"
			THROW
		ENDIF
		cRetMsg = ""
		ASSERT .F. MESSAGE "At SQL connect...debug"
		DO m:\dev\prg\sqlconnectmerk_bol  WITH nAcctNum,ccustname,cPPName,.T.,cOffice,.T.
		IF cRetMsg<>"OK"
			STORE cRetMsg TO cErrMsg
			THROW
		ENDIF
		SELECT vmerkurypp
		LOCATE
		IF lTesting
*			BROWSE
		ENDIF
		IF EOF()
			WAIT WINDOW "SQL select data is EMPTY...error!" NOWAIT
			cErrMsg = "SQL DATA EMPTY"
			THROW
		ENDIF
	ENDIF

	SELECT outship
	LOCATE
	WAIT CLEAR
	WAIT WINDOW "Now creating Header information" NOWAIT NOCLEAR

	SELECT outship
	LOCATE

	IF !lTesting
		IF EMPTY(cMBOL)  && May be empty if triggered from PT screen
			WAIT CLEAR
			WAIT WINDOW "MISSING Bill of Lading Number!" TIMEOUT 2
			cErrMsg = "BOL# EMPTY"
			THROW
		ELSE
			IF !SEEK(PADR(TRIM(cMBOL),20),"outship","bol_no")
				WAIT CLEAR
				WAIT WINDOW "Invalid Bill of Lading Number - Not Found!" TIMEOUT 2
				cErrMsg = "BOL# NOT FOUND"
				THROW
			ENDIF
		ENDIF
	ENDIF

*************************************************************************
*1	PTIDSOURCE/OUTSHIP RECORD MAIN LOOP
*	Loops through individual OUTSHIP lines for BOL#
*************************************************************************
	WAIT CLEAR

	SELECT outship
	SET ORDER TO
	COUNT TO N FOR outship.bol_no = cMBOL AND !EMPTYnul(del_date)
	IF N=0
		cErrMsg = "INCOMP BOL"
		THROW
	ENDIF

	LOCATE
	LOCATE FOR outship.bol_no = cMBOL
	cMissDel = ""
	nPTCount = 0
	nBLID = 0
	nFileID = 0
	nTcnt = 0
	nSubCnt = 0

	WAIT WINDOW "Now creating Master BOL#-based information..." NOWAIT
	SELECT bl
	bolscanstr = "bl.mblnum = cMBOL" && and bl.accountid = nAcctNum"
	COUNT TO nTotSubs FOR &bolscanstr

	WAIT WINDOW "Will scan through "+TRANSFORM(nTotSubs)+" sub-BOLs..." TIMEOUT 1

	LOCATE
	LOCATE FOR &bolscanstr
	cTempBOL = "XXX"
	SCAN FOR &bolscanstr
		nBLRec = RECNO()
		STORE TRIM(bl.bol_no) TO cBOL
		IF EMPTY(cBOL)
			SET STEP ON
		ENDIF

		DO m:\dev\prg\swc_cutctns_gen WITH cBOL

		IF cMBOL = '08226910000039806' AND cBOL # '9999' && Allows MBOL/BOL combos to be specified for a 945
			LOOP
		ENDIF

		IF INLIST(cOffice,'C','1','2','5')
			dDateTimeCal = (DATETIME()-(3*3600))
		ELSE
			dDateTimeCal = DATETIME()
		ENDIF

		dt1 = TTOC(dDateTimeCal,1)
		dtmail = TTOC(dDateTimeCal)
		dt2 = dDateTimeCal
		cString = ""
		cTruncDate = RIGHT(cdate,6)
		cTruncTime = SUBSTR(TTOC(dDateTimeCal,1),9,4)
		cfiledate = cdate+cTruncTime

		cHoldfolder = cMBOLFolder
		cFilenameHold = (cHoldfolder+cCustPrefix+dt1+".txt")
		cFilenameShort = JUSTFNAME(cFilenameHold)
		cFilenameOut = (lcPath+cFilenameShort)
		cFilenameArch = (lcArchivePath+cFilenameShort)
		nFilenum = FCREATE(cFilenameHold)
		nSTCount = 0

		DO num_incr_isa
		headerfill()
		nBLID = bl.blid
		nSubCnt = nSubCnt + 1

		xsqlexec("select * from bldet where mod='"+goffice+"' and blid="+TRANSFORM(nBLID),,,"wh")

		SELECT bldet
		SCAN FOR bldet.blid = nBLID
			CREATE CURSOR temp945 (accountid i,isa_num c(10),st_num c(10),wo_num i,bol_no c(20),ship_ref c(20),filename c(50),filedate T)
			cBLShip_ref = ALLTRIM(bldet.ship_ref)

			IF "OV"$cBLShip_ref
				LOOP
			ENDIF

			lCombPT = IIF(bldet.combinedpt,.T.,.F.)
			IF lCombPT
				bldetscanstr = "outship.combpt = PADR(cBLShip_ref,20) AND outship.accountid = nAcctNum and !DELETED()"
			ELSE
				bldetscanstr = "outship.ship_ref = PADR(cBLShip_ref,20) AND outship.accountid =nAcctNum and !DELETED()"
			ENDIF

			SELECT outship
			LOCATE FOR accountid = nAcctNum AND &bldetscanstr
			IF !FOUND()
				ASSERT .F. MESSAGE "At no-find of Outbound PT#"
				SET STEP ON
				cErrMsg = "MISS OBD PT: "+cBLShip_ref
				DO ediupdate WITH cErrMsg,.T.
				THROW
			ENDIF
			LOCATE

			IF !lManualRun
				COUNT TO N FOR (!EMPTYnul(del_date) AND accountid = nAcctNum AND &bldetscanstr)
				IF N=0
					cErrMsg = "INCOMP SUB-BOL"
					ASSERT .F. MESSAGE cErrMsg+" >>DEBUG<<"
					SET STEP ON
					DO ediupdate WITH cErrMsg,.T.
					THROW
				ELSE
					IF lCombPT
						WAIT WINDOW "Number of Combined PTs: "+ALLTRIM(STR(N)) TIMEOUT 2
					ENDIF
				ENDIF
			ENDIF

			SELECT outship
			SCAN FOR accountid = nAcctNum AND &bldetscanstr
				nWO_Num = outship.wo_num
				IF TYPE("nWO_Num")<> "N"
					lCloseOutput = .T.
					DO ediupdate WITH "BAD WO#",.T.
					THROW
				ENDIF
				alength = ALINES(apt,outship.shipins,.T.,CHR(13))
				SCATTER MEMVAR MEMO
				cWO_Num = ALLTRIM(STR(nWO_Num))
				IF !(cWO_Num$cWO_NumStr)
					cWO_NumStr = IIF(EMPTY(cWO_NumStr),cWO_Num,cWO_NumStr+","+cWO_Num)
				ENDIF
				IF !(cWO_Num$cWO_NumList)
					cWO_NumList = IIF(EMPTY(cWO_NumList),cWO_Num,cWO_NumList+CHR(13)+cWO_Num)
				ENDIF
				nPTCount = nPTCount + 1
				cShip_ref = ALLTRIM(m.ship_ref)

				IF "PENNEY"$UPPER(m.consignee) OR UPPER(m.consignee)="JCP"
					lJCP = .T.
				ENDIF

				nOutshipid = m.outshipid
				IF "OV"$cShip_ref
					LOOP
				ENDIF
				cPackType = ALLTRIM(segmentget(@apt,"PROCESSMODE",alength))
				lPrepack = IIF(cPackType#"PICKPACK",.T.,.F.)
				m.scac = ALLTRIM(outship.scac)
				STORE m.scac TO cSCAC

				nTotCtnCount = m.qty
				cPO_Num = ALLTRIM(m.cnee_ref)
				cShip_ref = ALLTRIM(m.ship_ref)
				cCharge = "0.00"


*!* Added this code to trap miscounts in OUTDET Units
*		ASSERT .F. MESSAGE "In outdet/SQL tot check"
				IF lPrepack
					SELECT outdet
					SET ORDER TO
					IF lOverflow
						SUM origqty TO nCtnTot1 FOR !units AND outdet.outshipid = outship.outshipid
					ELSE
						SUM totqty TO nCtnTot1 FOR !units AND outdet.outshipid = outship.outshipid
					ENDIF
*!*	Check carton count
					SELECT COUNT(ucc) AS cnt1 ;
						FROM vmerkurypp ;
						WHERE vmerkurypp.outshipid = outship.outshipid ;
						AND vmerkurypp.ucc#"CUTS" ;
						AND vmerkurypp.totqty > 0 ;
						INTO CURSOR tempsqlx
					STORE tempsqlx.cnt1 TO nCtnTot2
					USE IN tempsqlx
					IF nCtnTot1<>nCtnTot2
						ASSERT .F. MESSAGE "At CTN QTY error"
						SET STEP ON
						cErrMsg = "SQL CTNQTY ERR, PT "+cShip_ref
						THROW
					ENDIF
				ELSE
					SELECT outdet
					IF lOverflow
						SUM origqty TO nUnitTot1 FOR outdet.units=.T. AND outdet.outshipid = outship.outshipid
					ELSE
						SUM totqty TO nUnitTot1 FOR outdet.units=.T. AND outdet.outshipid = outship.outshipid
					ENDIF
					SELECT vmerkurypp
					SUM vmerkurypp.totqty TO nUnitTot2 FOR vmerkurypp.outshipid = outship.outshipid AND vmerkurypp.accountid = nAcctNum
					IF nUnitTot1<>nUnitTot2
						ASSERT .F. MESSAGE "IN TOTQTY COMPARE"
						SET STEP ON
						cErrMsg = "SQL UNITQTY ERR, PT "+cShip_ref
						THROW
					ENDIF
				ENDIF

				SELECT outdet
				SET ORDER TO outdetid
				LOCATE

*!* End code addition

				IF (("PENNEY"$UPPER(outship.consignee)) OR ("KMART"$UPPER(outship.consignee)) OR ("K-MART"$UPPER(outship.consignee)))
					lApptFlag = .T.
				ELSE
					lApptFlag = .F.
				ENDIF

				IF lTestinput OR lManualRun
					ddel_date = DATE()
					dapptnum = "99999"
					dapptdate = DATE()
				ELSE
					ddel_date = outship.del_date
					IF EMPTY(ddel_date)
						cMissDel = IIF(EMPTY(cMissDel),"The following PTs had no Delivery Dates:"+CHR(13)+TRIM(cShip_ref),cMissDel+CHR(13)+TRIM(cShip_ref))
					ENDIF
					dapptnum = ALLTRIM(outship.appt_num)

					IF EMPTY(dapptnum) && Penney/KMart Appt Number check
						IF !(lApptFlag)
							dapptnum = ""
						ELSE
							cErrMsg = "EMPTY APPT #"
							THROW
						ENDIF
					ENDIF
					dapptdate = outship.appt

					IF EMPTY(dapptdate)
						dapptdate = outship.del_date
					ENDIF
				ENDIF

				IF ALLTRIM(outship.SForCSZ) = ","
					BLANK FIELDS outship.SForCSZ NEXT 1 IN outship
				ENDIF

				cTRNum = ""
				cPRONum = ""

				cPRONum = outship.keyrec
				IF EMPTY(outship.keyrec)
					cPRONum = outship.appt_num
				ENDIF

				m.CSZ = TRIM(m.CSZ)
				IF EMPTY(ALLT(STRTRAN(M.CSZ,",","")))
					SET STEP ON
					WAIT CLEAR
					WAIT WINDOW "No SHIP-TO City/State/ZIP info...exiting" TIMEOUT 2
					cErrMsg = "NO CSZ INFO"
					THROW
				ENDIF
				IF !(", "$m.CSZ)
					m.CSZ = ALLTRIM(STRTRAN(m.CSZ,",",", "))
				ENDIF
				m.CSZ = ALLTRIM(STRTRAN(m.CSZ,"  "," "))
				cCity = ALLT(LEFT(TRIM(m.CSZ),AT(",",m.CSZ)-1))
				cStateZip = ALLT(SUBSTR(TRIM(m.CSZ),AT(",",m.CSZ)+1))
				cState = ALLT(LEFT(cStateZip,2))
				cZip = ALLT(SUBSTR(cStateZip,3))

				STORE "" TO cSForCity,cSForState,cSForZip
				IF !lFederated
					m.SForCSZ = ALLTRIM(STRTRAN(m.SForCSZ,"  "," "))
					nSpaces = OCCURS(" ",ALLTRIM(m.SForCSZ))
					IF nSpaces = 0
						m.SForCSZ = STRTRAN(m.SForCSZ,",","")
						cSForCity = ALLTRIM(m.SForCSZ)
						cSForState = ""
						cSForZip = ""
					ELSE
						nCommaPos = AT(",",m.SForCSZ)
						nLastSpace = AT(" ",m.SForCSZ,nSpaces)
						nMinusSpaces = IIF(nSpaces=1,0,1)
						IF ISALPHA(SUBSTR(TRIM(m.SForCSZ),AT(" ",TRIM(m.SForCSZ),nSpaces-nMinusSpaces)+1,2))
							cSForCity = LEFT(TRIM(m.SForCSZ),AT(",",m.SForCSZ)-1)
							cSForState = SUBSTR(m.SForCSZ,nCommaPos+2,2)
							cSForZip = TRIM(RIGHT(TRIM(m.SForCSZ),5))
							IF ISALPHA(cSForZip)
								cSForZip = ""
							ENDIF
						ELSE
							WAIT CLEAR
							WAIT WINDOW "NOT ALPHA: "+SUBSTR(TRIM(m.SForCSZ),AT(" ",m.SForCSZ,nSpaces-1)+1,2) TIMEOUT 3
							cSForCity = LEFT(TRIM(m.SForCSZ),AT(",",m.SForCSZ)-1)
							cSForState = SUBSTR(TRIM(m.SForCSZ),AT(" ",m.SForCSZ,nSpaces-2)+1,2)
							cSForZip = TRIM(RIGHT(TRIM(m.SForCSZ),5))
							IF ISALPHA(cSForZip)
								cSForZip = ""
							ENDIF
						ENDIF
					ENDIF
				ELSE
					cStoreName = segmentget(@apt,"STORENAME",alength)
				ENDIF

				DO num_incr_st
				WAIT CLEAR
				WAIT WINDOW "Now creating Line Item information" NOWAIT NOCLEAR

				INSERT INTO temp945 (accountid,isa_num,st_num,wo_num,bol_no,ship_ref,filename,filedate) ;
					VALUES (m.accountid,c_CntrlNum,c_GrpCntrlNum,m.wo_num,m.bol_no,m.ship_ref,cFilenameShort,dt2)
&&&& added 1/24/17 TMARG to capture data for 997

				m.groupnum=c_CntrlNum
				m.isanum=cISA_Num
				m.transnum=PADL(c_GrpCntrlNum,9,"0")
				m.edicode="SW"
				m.accountid=m.accountid
				m.loaddt=DATE()
				m.loadtime=DATETIME()
				m.filename=cFilenameShort
				m.ship_ref=m.ship_ref
				insertinto("ackdata","wh",.T.)
				tu("ackdata")

				STORE "ST"+cfd+"945"+cfd+PADL(c_GrpCntrlNum,9,"0")+csegd TO cString
				DO cstringbreak
				nSTCount = nSTCount + 1
				nSegCtr = 1

				cShip_refW06 = ALLTRIM(segmentget(@apt,"OLDPT",alength))
				IF EMPTY(cPTString)
					cPTString = "Sub-BOL: "+cBOL+CHR(13)+" "+m.consignee+" "+cShip_ref+SPACE(5)+cShip_refW06
				ELSE
					IF cTempBOL # cBOL AND cTempBOL # "XXX"
						cPTString = cPTString+CHR(13)+CHR(13)+"Sub-BOL:"+cBOL+CHR(13)+" "+m.consignee+" "+cShip_ref+SPACE(5)+cShip_refW06
					ELSE
						cPTString = cPTString+CHR(13)+" "+m.consignee+" "+cShip_ref+SPACE(5)+cShip_refW06
					ENDIF
				ENDIF
				cTempBOL = cBOL
*				cShip_refW06 = IIF("OV"$cShip_ref,ALLTRIM(LEFT(cShip_ref,AT(" ",cShip_ref,1))),cShip_ref)
				STORE "W06"+cfd+"N"+cfd+cShip_refW06+cfd+cdate+cfd+TRIM(cBOL)+cfd+TRIM(cWO_Num)+cfd+cPO_Num+csegd TO cString
				DO cstringbreak
				nSegCtr = nSegCtr + 1

				nCtnNumber = 1  && Seed carton sequence count

				cStoreNum = m.dcnum
				IF EMPTY(cStoreNum)
					STORE "N1"+cfd+"ST"+cfd+ALLTRIM(m.consignee)+csegd TO cString
				ELSE
					STORE "N1"+cfd+"ST"+cfd+ALLTRIM(m.consignee)+cfd+"92"+cfd+cStoreNum+csegd TO cString
				ENDIF
				DO cstringbreak
				nSegCtr = nSegCtr + 1

				cCountry = segmentget(@apt,"COUNTRY",alength)
				cCountry = ALLT(cCountry)
				IF EMPTY(cCountry)
					cCountry = ALLTRIM(outship.country)
					IF EMPTY(cCountry)
						cCountry = "USA"
					ENDIF
				ENDIF
				STORE "N3"+cfd+TRIM(outship.address)+IIF(!EMPTY(outship.address2),ALLTRIM(outship.address2)+csegd,csegd) TO cString
				DO cstringbreak
				nSegCtr = nSegCtr + 1

				IF m.consignee = "TJX UK"
					STORE "N4*HERTS, ENGLAND*HT*WD17 1TX*GB"+csegd TO cString
				ELSE
					STORE "N4"+cfd+cCity+cfd+cState+cfd+cZip+cfd+cCountry+csegd TO cString
				ENDIF
				DO cstringbreak
				nSegCtr = nSegCtr + 1

				cSFStoreNum = segmentget(@apt,"SFSTORENUM",alength)
				IF EMPTY(cSFStoreNum)
					cSFStoreNum = ALLTRIM(m.sforstore)
				ENDIF
				IF !EMPTY(m.shipfor)
					IF EMPTY(ALLTRIM(m.sforstore))
						STORE "N1"+cfd+"Z7"+cfd+ALLTRIM(m.shipfor)+csegd TO cString
					ELSE
						STORE "N1"+cfd+"Z7"+cfd+ALLTRIM(m.shipfor)+cfd+"92"+cfd+cSFStoreNum+csegd TO cString
					ENDIF
					DO cstringbreak
					nSegCtr = nSegCtr + 1

					STORE "N3"+cfd+TRIM(m.sforaddr1)+csegd TO cString
					DO cstringbreak
					nSegCtr = nSegCtr + 1

					IF !EMPTY(cSForState)
						IF m.consignee = "TJX UK"
							STORE "N4*HERTS, ENGLAND*HT*WD17 1TX*GB"+csegd TO cString
						ELSE
							STORE "N4"+cfd+cCity+cfd+cState+cfd+cZip+cfd+cCountry+csegd TO cString
						ENDIF
					ELSE
						STORE "N4"+cfd+cSForCity+csegd TO cString
					ENDIF
					DO cstringbreak
					nSegCtr = nSegCtr + 1
				ENDIF

				FOR nom = 1 TO 3
					cNom = ICASE(nom=1,"SD",nom=2,"PF","MA")
					cNomLoc = cNom+"_N1"
					cNomInfo = ALLTRIM(segmentget(@apt,cNomLoc,alength))
*					cNomInfo = STRTRAN(cNomInfo,"|",cfd)
					IF EMPTY(cNomInfo)
						LOOP
					ENDIF

					STORE "N1"+cfd+cNom+cfd+cNomInfo+csegd TO cString
					DO cstringbreak
					nSegCtr = nSegCtr + 1

					cNomLoc = cNom+"_N2"
					cNomInfo = ALLTRIM(segmentget(@apt,cNomLoc,alength))
					cNomInfo = STRTRAN(cNomInfo,"|",cfd)
					IF !EMPTY(cNomInfo)
						STORE "N2"+cfd+cNomInfo+csegd TO cString
						DO cstringbreak
						nSegCtr = nSegCtr + 1
					ENDIF

					cNomLoc = cNom+"_N3"
					cNomInfo = ALLTRIM(segmentget(@apt,cNomLoc,alength))
					cNomInfo = STRTRAN(cNomInfo,"|",cfd)
					IF !EMPTY(cNomInfo)
						STORE "N3"+cfd+cNomInfo+csegd TO cString
						DO cstringbreak
						nSegCtr = nSegCtr + 1
					ENDIF

					cNomLoc = cNom+"_N4"
					cNomInfo = ALLTRIM(segmentget(@apt,cNomLoc,alength))
					cNomInfo = STRTRAN(cNomInfo,"|",cfd)
					IF !EMPTY(cNomInfo)
						IF m.consignee = "TJX UK"
							STORE "N4"+cfd+cNomInfo+cfd+"GB"+csegd TO cString
						ELSE
							STORE "N4"+cfd+cNomInfo+csegd TO cString
						ENDIF
						DO cstringbreak
						nSegCtr = nSegCtr + 1
					ENDIF
				ENDFOR
				RELEASE cNomLoc,cNomInfo

				STORE "N1"+cfd+"CA"+cfd+ALLTRIM(outship.ship_via)+cfd+"92"+cfd+ALLTRIM(outship.scac)+csegd TO cString
				DO cstringbreak
				nSegCtr = nSegCtr + 1

				STORE "N9"+cfd+"MB"+cfd+ALLTRIM(cMBOL)+csegd TO cString
				DO cstringbreak
				nSegCtr = nSegCtr + 1

				STORE "N9"+cfd+"BM"+cfd+ALLTRIM(cBOL)+csegd TO cString
				DO cstringbreak
				nSegCtr = nSegCtr + 1

				STORE "N9"+cfd+"CN"+cfd+ALLTRIM(cPRONum)+csegd TO cString
				DO cstringbreak
				nSegCtr = nSegCtr + 1

				IF !EMPTY(bl.trailer)
					STORE "N9"+cfd+"RU"+cfd+ALLTRIM(bl.trailer)+csegd TO cString
					DO cstringbreak
					nSegCtr = nSegCtr + 1
				ENDIF
				IF !EMPTY(bl.seal)
					STORE "N9"+cfd+"T5"+cfd+ALLTRIM(bl.seal)+csegd TO cString
					DO cstringbreak
					nSegCtr = nSegCtr + 1
				ENDIF

				cShipcode = ALLTRIM(segmentget(@apt,"SHIPCODE",alength))
				IF !EMPTY(cShipcode)
					STORE "N9"+cfd+"DP"+cfd+ALLTRIM(cShipcode)+csegd TO cString
					DO cstringbreak
					nSegCtr = nSegCtr + 1
				ENDIF

				cDiv = ALLTRIM(outship.div)
				IF !EMPTY(cDiv)
					STORE "N9"+cfd+"DV"+cfd+cDiv+csegd TO cString
					DO cstringbreak
					nSegCtr = nSegCtr + 1
				ENDIF
				RELEASE cDiv

				STORE "N9"+cfd+"CU"+cfd+ALLTRIM(outship.cacctnum)+csegd TO cString
				DO cstringbreak
				nSegCtr = nSegCtr + 1

				cCO = IIF("OV"$cShip_ref,ALLTRIM(LEFT(cShip_ref,AT(" ",cShip_ref,1))),cShip_ref)  && Reversed this from actual CO data to current PT#, JoeB, 03.22.2017
				cCO = IIF(ISALPHA(cShip_ref),ALLTRIM(SUBSTR(cShip_ref,2)),cShip_ref)
*				cCO = ALLTRIM(segmentget(@apt,"CO",alength))
				IF !EMPTY(cCO)
					STORE "N9"+cfd+"CO"+cfd+ALLTRIM(cCO)+csegd TO cString
					DO cstringbreak
					nSegCtr = nSegCtr + 1
				ENDIF

				STORE "G62"+cfd+"11"+cfd+TRIM(DTOS(ddel_date))+cfd+"D"+cfd+cDelTime+csegd TO cString  && Ship date/time
				DO cstringbreak
				nSegCtr = nSegCtr + 1

				SELECT outship

				STORE "W27"+cfd+cCarrierType+cfd+TRIM(m.scac)+cfd+TRIM(m.ship_via)+csegd TO cString
				DO cstringbreak
				nSegCtr = nSegCtr + 1

*************************************************************************
*2	DETAIL LOOP - MATCHING SHIP_REF FIELD (FROM OUTSHIP)
*************************************************************************
				WAIT CLEAR
				WAIT WINDOW "Now creating Detail information "+cShip_ref NOWAIT NOCLEAR
				SELECT vmerkurypp

				SELECT outdet
				SET ORDER TO TAG outdetid
				SELECT vmerkurypp
				SET RELATION TO outdetid INTO outdet

				SELECT vmerkurypp
				LOCATE
*!*			BROWSE FIELDS wo_num,ship_ref,outshipid
				LOCATE FOR vmerkurypp.ship_ref = TRIM(cShip_ref) AND vmerkurypp.outshipid = nOutshipid
				IF !FOUND()
					IF !lTesting
						WAIT WINDOW "PT "+cShip_ref+" NOT FOUND in vmerkurypp...ABORTING" TIMEOUT 2
						IF !lTesting
							lSQLMail = .T.
						ENDIF
						cErrMsg = "MISS PT-SQL: "+cShip_ref
						THROW
					ELSE
						LOOP
					ENDIF
				ENDIF

				vscanstr = "vmerkurypp.ship_ref = cShip_ref and vmerkurypp.outshipid = nOutshipid"

				SELECT vmerkurypp
				LOCATE
				LOCATE FOR &vscanstr
				cUCC= "XXX"
				DO WHILE &vscanstr
					lSkipBack = .T.

					IF vmerkurypp.totqty = 0  && Added to prevent zero quantity detail from adding, 08.14.2017, Joe
						SKIP 1 IN vmerkurypp
						LOOP
					ENDIF

					IF TRIM(vmerkurypp.ucc) <> cUCC
						STORE TRIM(vmerkurypp.ucc) TO cUCC
					ENDIF

					STORE "LX"+cfd+ALLTRIM(STR(nCtnNumber))+csegd TO cString   && Carton Seq. # (up to Carton total)
					lDoManSegment = .T.
					lDoPALSegment = .F.
					DO cstringbreak
					nSegCtr = nSegCtr + 1

					DO WHILE vmerkurypp.ucc = cUCC
						cDesc = ""
						SELECT outdet
						alength = ALINES(aptdet,outdet.printstuff,.T.,CHR(13))
*!* Prepack determination based on actual style info
						lPrepack945 = .F.
						cUCCNumber = vmerkurypp.ucc
						cUCCNumber = ALLTRIM(STRTRAN(TRIM(cUCCNumber)," ",""))

						IF lDoManSegment
							lDoManSegment = .F.
							nPTCtnTot =  nPTCtnTot + 1
							STORE "MAN"+cfd+"GM"+cfd+TRIM(cUCCNumber)+csegd TO cString  && SCC-14 Number (Wal-mart spec)
							DO cstringbreak
							nSegCtr = nSegCtr + 1
							nShipDetQty = INT(VAL(outdet.PACK))
							nUnitSum = nUnitSum + nShipDetQty

							IF (EMPTY(outdet.ctnwt) OR outdet.ctnwt = 0) AND outdet.totqty > 0
								cCtnWt = ALLTRIM(STR(INT(outship.weight/outship.ctnqty)))
								nTotCtnWt = nTotCtnWt + INT(VAL(cCtnWt))
							ELSE
								STORE ALLTRIM(STR(outdet.ctnwt)) TO cCtnWt
								nTotCtnWt = nTotCtnWt + outdet.ctnwt
							ENDIF
						ENDIF

						cStyle = TRIM(outdet.STYLE)
						IF EMPTY(cStyle)
							ASSERT .F. MESSAGE "EMPTY STYLE...DEBUG"
							IF nOutshipid = 2421365
								SET DELETED OFF
								SELECT vmerkurypp
								SET RELATION TO outdetid INTO outdet
								cStyle = TRIM(outdet.STYLE)
							ELSE
								WAIT WINDOW "Empty style in "+cShip_ref NOWAIT && TIMEOUT 1
							ENDIF
						ENDIF

						cColor = TRIM(outdet.COLOR)
						cSize = TRIM(outdet.ID)
						lDoUPC = .T.
*				ASSERT .F. MESSAGE "At UPC extract...debug"
						cUPC = ALLTRIM(outdet.upc)
						IF (ISNULL(cUPC) OR EMPTY(cUPC) OR VAL(cUPC) = 0)
							cUPC = TRIM(vmerkurypp.upc)
							IF (ISNULL(cUPC) OR EMPTY(cUPC) OR VAL(cUPC) = 0)
								lDoUPC = .F.
							ENDIF
						ENDIF
						cItemNum = TRIM(outdet.custsku)
						nShipDetQty = vmerkurypp.totqty
						nOrigDetQty = vmerkurypp.qty
						cUnitCode = "EA"
						nShipStat = "SH"

						IF !SEEK(outdet.STYLE,"upcmast","style")
							lcLength = "9.00"
							lcWidth  = "9.00"
							lcHeight = "9.00"
							lcWeight = "15.00"
							lcCube    = "15.00"
							lcUPC     = "UNK"
						ELSE
							SELECT upcmast
							lcLength = getmemodata("info","LENGTH")
							lcWidth  = getmemodata("info","WIDTH")
							lcHeight = getmemodata("info","HEIGHT")
							lcWeight = TRANSFORM(upcmast.weight)
							lcCube   = TRANSFORM(upcmast.cube)
							lcUPC    = upcmast.upc
						ENDIF

						STORE "W12"+cfd+nShipStat+cfd+ALLTRIM(STR(nOrigDetQty))+cfd+ALLTRIM(STR(nShipDetQty))+cfd+ALLTRIM(STR(nOrigDetQty-nShipDetQty))+cfd+;
							cUnitCode+cfd+IIF(EMPTY(outdet.upc),ALLTRIM(lcUPC),ALLTRIM(outdet.upc))+cfd+"CB"+cfd+ALLTRIM(outdet.STYLE)+cfd+cfd+ALLTRIM(lcWeight)+cfd+"G"+cfd+"L"+cfd+cfd+cfd+cfd+cfd+"VN"+cfd+ALLTRIM(outdet.STYLE)+csegd TO cString
						DO cstringbreak
						nSegCtr = nSegCtr + 1


						STORE "N9"+cfd+"LN"+cfd+ALLTRIM(lcLength)+csegd TO cString
						DO cstringbreak
						nSegCtr = nSegCtr + 1

						STORE "N9"+cfd+"WD"+cfd+ALLTRIM(lcWidth)+csegd TO cString
						DO cstringbreak
						nSegCtr = nSegCtr + 1

						STORE "N9"+cfd+"HE"+cfd+ALLTRIM(lcHeight)+csegd TO cString
						DO cstringbreak
						nSegCtr = nSegCtr + 1

						STORE "N9"+cfd+"CUB"+cfd+ALLTRIM(lcCube)+csegd TO cString
						DO cstringbreak
						nSegCtr = nSegCtr + 1


*				ASSERT .f. MESSAGE "AT G69 LOOP"
						cDesc = ALLTRIM(segmentget(@aptdet,"MASTSTYLEDESC",alength))
						IF !EMPTY(cDesc)
							STORE "G69"+cfd+cDesc+csegd TO cString
							DO cstringbreak
							nSegCtr = nSegCtr + 1
						ENDIF

						cPkg = TRIM(segmentget(@aptdet,"PKG",alength))
						cPkgQty = TRIM(segmentget(@aptdet,"QTY",alength))
						IF !EMPTY(cPkg)
							STORE "N9"+cfd+"PKG"+cfd+cPkg+cfd+cPkgQty+csegd TO cString
							DO cstringbreak
							nSegCtr = nSegCtr + 1
						ENDIF

						SKIP 1 IN vmerkurypp
						SET DELETED ON
					ENDDO
					lSkipBack = .T.
					nCtnNumber = nCtnNumber + 1
*		nUnitSum = nUnitSum + 1
				ENDDO

*************************************************************************
*2^	END DETAIL MATCHING SHIP_REF LOOP
*************************************************************************
				WAIT CLEAR
				IF outship.cuft>0
					cCube = STR(outship.cuft,6,2)
				ELSE
					cCube = "0"
				ENDIF
				WAIT WINDOW "Now creating Section Closure information" NOWAIT NOCLEAR
				STORE "W03"+cfd+ALLTRIM(STR(nPTCtnTot))+cfd+ALLTRIM(STR(nTotCtnWt))+cfd+;
					cWeightUnit+cfd+cCube+cfd+"CF"+csegd TO cString   && Units sum, Weight sum, carton count

				nTotCtnWt = 0
				nTotCtnCount = 0
				nUnitSum = 0
				IF lTesting
					FPUTS(nFilenum,cString)
				ELSE
					FWRITE(nFilenum,cString)
				ENDIF

				STORE  "SE"+cfd+ALLTRIM(STR(nSegCtr+2))+cfd+PADL(c_GrpCntrlNum,9,"0")+csegd TO cString
				IF lTesting
					FPUTS(nFilenum,cString)
				ELSE
					FWRITE(nFilenum,cString)
				ENDIF

				SELECT outship
				WAIT CLEAR
			ENDSCAN
			SELECT bldet
		ENDSCAN

*************************************************************************
*1^	END OUTSHIP MAIN LOOP
*************************************************************************

		close945()
		=FCLOSE(nFilenum)

*!* Transfers files to correct output folders
		COPY FILE [&cFilenameHold] TO [&cFilenameArch]

		cWO_NumList = ""

		WAIT WINDOW "" TIMEOUT 1
		IF lMerkFilesout AND !lTesting
			SELECT temp945
			COPY TO "f:\3pl\data\temp945mk.dbf"
			USE IN temp945
			SELECT 0
			USE "f:\3pl\data\temp945mk.dbf" ALIAS temp945mk
			SELECT 0
			USE "f:\3pl\data\pts_sent945.dbf" ALIAS pts_sent945
			APPEND FROM "f:\3pl\data\temp945mk.dbf"
			USE IN pts_sent945
			USE IN temp945mk
			DELETE FILE "f:\3pl\data\temp945mk.dbf"
		ENDIF

		cTotSubs = ALLTRIM(STR(nTotSubs))
		cSubCnt = ALLTRIM(STR(nSubCnt))
		cPTCount = ALLTRIM(STR(nPTCount))

		INSERT INTO tempinfo (filenameshort,subbol,ptcount,ptstring) VALUES (cFilenameShort,cBOL,cPTCount,cPTString)

		WAIT CLEAR
		WAIT WINDOW cPPName+" 945 EDI File output complete for"+CHR(13)+"Sub-BOL: "+cBOL NOWAIT

IF !lTesting
*asn_out_data()
endif

		SELECT bl

	ENDSCAN

	IF !lTesting
		SELECT edi_trigger
		DO ediupdate WITH "945 CREATED",.F.
		SELECT edi_trigger
		LOCATE

		IF !USED("ftpedilog")
			SELECT 0
			USE F:\edirouting\ftpedilog ALIAS ftpedilog
			DO WHILE ISFLOCKED("ftpedilog")
				LOOP
			ENDDO
			INSERT INTO ftpedilog (TRANSFER,ftpdate,filename,acct_name,TYPE) VALUES ("945-"+ccustname+"-"+cCustLoc,dt2,cFilenameHold,UPPER(ccustname),"945")
			USE IN ftpedilog
		ENDIF
	ENDIF

	RELEASE ALL LIKE c_CntrlNum,c_GrpCntrlNum
	WAIT CLEAR
	WAIT WINDOW "All Sub-BOLs for MBOL "+cMBOL+" have been completed...exiting" TIMEOUT 2

*!* Create eMail confirmation message
	IF lTestMail
		tsendto = tsendtotest
		tcc = tcctest
	ENDIF

	cOutFolder = "FMI"+cCustLoc

	tsubject = cMailName+" 945 EDI File from TGF as of "+dtmail+" ("+cCustLoc+")"
	tattach = " "
	tmessage = "945 EDI Info from TGF, for division "+cDivision+", MBOL# "+TRIM(cMBOL)+CHR(13)
	tmessage = tmessage + "File Name "+cFilenameShort
	tmessage = tmessage + "Under Toll Work Orders: "+cWO_NumStr+","+CHR(13)
	tmessage = tmessage + "containing these "+ALLTRIM(STR(nPTCount))+" picktickets as follows:"+CHR(13)+CHR(13)
	tmessage = tmessage + cPTString + CHR(13)+CHR(13)
	tmessage = tmessage + "has been created and will be transmitted ASAP."+CHR(13)+CHR(13)
	IF !EMPTY(cMissDel)
		tmessage = tmessage+CHR(13)+CHR(13)+cMissDel+CHR(13)+CHR(13)
	ENDIF
	IF lTesting OR lTestinput
		tmessage = tmessage + "This is a TEST 945"
	ELSE
		tmessage = tmessage + "If you have any questions, please eMail or call 732-750-9000 x217."
	ENDIF

	IF lEmailMerk
		ASSERT .F. MESSAGE "At do mail stage...DEBUG"
		DO FORM m:\dev\frm\dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
	ENDIF
	cPTString = ""
	nPTCount = 0
	cWO_NumStr = ""

	IF lMerkFilesout
		CD &cMBOLFolder
		COPY FILE *.* TO (lcPath+"*.*")
		DELETE FILE *.*
	ENDIF
*!* Transfers files to correct output folders
	lDoCatch = .F.

CATCH TO oErr
	IF lDoCatch
		ASSERT .F. MESSAGE "In Error CATCH"
		SET STEP ON
		lcQuery = [delete from edioutlog where accountid = ]+TRANS(nAcctNum)+[ and mbol = ']+cMBOL+[']
		xsqlexec(lcQuery,,,"stuff")
		IF EMPTY(cErrMsg)
			cErrMsg = ALLTRIM(oErr.MESSAGE)
			lIsError = .T.
		ENDIF
		DO ediupdate WITH cErrMsg,.T.
		tsubject = ccustname+" Error ("+TRANSFORM(oErr.ERRORNO)+") at "+TTOC(DATETIME())
		tattach  = ""
		IF lTesting
			tsendto  = tsendtotest
			tcc = tcctest
		ELSE
			tsendto  = tsendtoerr
			tcc = tccerr
		ENDIF

		IF !lIsError
			tmessage = ccustname+" Error processing "+CHR(13)
			tmessage = tmessage+TRIM(PROGRAM())+CHR(13)

			tmessage = tmessage+"Try/CATCH Exeception Message:"+CHR(13)+;
				[  Error: ] + STR(oErr.ERRORNO) +CHR(13)+;
				[  LineNo: ] + STR(oErr.LINENO) +CHR(13)+;
				[  Message: ] + oErr.MESSAGE +CHR(13)+;
				[  Procedure: ] + oErr.PROCEDURE +CHR(13)+;
				[  Details: ] + oErr.DETAILS +CHR(13)+;
				[  StackLevel: ] + STR(oErr.STACKLEVEL) +CHR(13)+;
				[  LineContents: ] + oErr.LINECONTENTS+CHR(13)+;
				[  UserValue: ] + oErr.USERVALUE+CHR(13)
			tmessage = tmessage+"Program: "+cRunningProg+CHR(13)

			tsubject = "945 EDI Poller Error at "+TTOC(DATETIME())
			tattach  = ""
			tcc=""
			ASSERT .F. MESSAGE "At do mail stage...DEBUG"
			DO FORM m:\dev\frm\dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
		ENDIF
		FCLOSE(nFilenum)
	ENDIF
FINALLY
	IF USED('OUTSHIP')
		USE IN outship
	ENDIF
	IF USED('OUTDET')
		USE IN outdet
	ENDIF
	IF USED('SERFILE')
		USE IN serfile
	ENDIF

	WAIT CLEAR

ENDTRY

*** END OF CODE BODY

****************************
PROCEDURE close945
****************************
** Footer Creation

	STORE  "GE"+cfd+ALLTRIM(STR(nSTCount))+cfd+c_CntrlNum+csegd TO cString
	IF lTesting
		FPUTS(nFilenum,cString)
	ELSE
		FWRITE(nFilenum,cString)
	ENDIF

	STORE  "IEA"+cfd+"1"+cfd+PADL(c_CntrlNum,9,"0")+csegd TO cString
	IF lTesting
		FPUTS(nFilenum,cString)
	ELSE
		FWRITE(nFilenum,cString)
	ENDIF

	RETURN

****************************
PROCEDURE num_incr_isa
****************************

	IF !USED("serfile")
		USE ("f:\3pl\data\serial\"+ccustname+"945_serial") IN 0 ALIAS serfile
	ENDIF
	SELECT serfile
	IF lTesting AND lISAFlag
		nOrigSeq = serfile.seqnum
		lISAFlag = .F.
	ENDIF
	nISA_Num = serfile.seqnum
	c_CntrlNum = ALLTRIM(STR(serfile.seqnum))
	REPLACE serfile.seqnum WITH serfile.seqnum + 1 IN serfile
	SELECT outship
	RETURN

****************************
PROCEDURE num_incr_st
****************************

	IF !USED("serfile")
		USE ("f:\3pl\data\serial\"+ccustname+""945_serial") IN 0 ALIAS serfile
	ENDIF
	SELECT serfile
	IF lTesting AND lSTFlag
		nOrigGrpSeq = serfile.grpseqnum
		lSTFlag = .F.
	ENDIF
	c_GrpCntrlNum = ALLTRIM(STR(serfile.grpseqnum))
	REPLACE serfile.grpseqnum WITH serfile.grpseqnum + 1 IN serfile
	SELECT outship
	RETURN


****************************
PROCEDURE segmentget
****************************
	PARAMETER thisarray,lcKey,nLength

	FOR i = 1 TO nLength
		IF i > nLength
			EXIT
		ENDIF
		lnEnd= AT("*",thisarray[i])
		IF lnEnd > 0
			lcThisKey =TRIM(SUBSTR(thisarray[i],1,lnEnd-1))
			IF OCCURS(lcKey,lcThisKey)>0
				RETURN SUBSTR(thisarray[i],lnEnd+1)
				i = 1
			ENDIF
		ENDIF
	ENDFOR

	RETURN ""

****************************
PROCEDURE ediupdate
****************************
	PARAMETER cStatus,lIsError

	IF !lTesting
		SELECT edi_trigger
		nRec = RECNO()
		IF !lIsError
			REPLACE processed WITH .T.,proc945 WITH .T.,file945 WITH cFilenameHold,isa_num WITH cISA_Num,;
				fin_status WITH "945 CREATED",errorflag WITH .F.,when_proc WITH DATETIME() ;
				FOR edi_trigger.bol = cMBOL AND accountid = nAcctNum
			lDoCatch = .F.
		ELSE
			lDoCatch = .F.
			REPLACE processed WITH .T.,proc945 WITH .F.,file945 WITH "",;
				fin_status WITH cStatus,errorflag WITH .T. ;
				FOR edi_trigger.bol = cMBOL AND accountid = nAcctNum
			num_decrement()

			IF lCloseOutput
				=FCLOSE(nFilenum)
				ERASE &cFilenameHold
			ENDIF
		ENDIF
	ENDIF

	IF lIsError AND lEmailMerk AND cStatus<>"SQL ERROR"
		tsubject = "945 Error in "+cMailName+" BOL "+TRIM(cBOL)+"(At PT "+cShip_ref+")"
		tattach = " "
		tsendto = tsendtoerr
		tcc = tccerr
		tmessage = "945 Processing for BOL# "+cBOL+", WO# "+cWO_Num+" produced this error: (Office: "+cOffice+"): "+cStatus+CHR(13)+"Check EDI_TRIGGER and re-run"
		IF "TOTQTY ERR"$cStatus
			tmessage = tmessage + CHR(13) + "At OUTSHIPID: "+ALLTRIM(STR(m.outshipid))
		ENDIF
		ASSERT .F. MESSAGE "At do mail stage...DEBUG"
		DO FORM m:\dev\frm\dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
	ENDIF

	IF USED('outship')
		USE IN outship
	ENDIF
	IF USED('outdet')
		USE IN outdet
	ENDIF

	IF USED('serfile')
		SELECT serfile
		IF lTesting
			REPLACE serfile.seqnum WITH nOrigSeq
			REPLACE serfile.grpseqnum WITH nOrigGrpSeq
		ENDIF
		USE IN serfile
	ENDIF
	IF USED('mm')
		USE IN mm
	ENDIF
	IF USED('tempx')
		USE IN tempx
	ENDIF
	IF !lTesting
		SELECT edi_trigger
		LOCATE
	ENDIF

ENDPROC

****************************
PROCEDURE cstringbreak
****************************
	cLen = LEN(ALLTRIM(cString))
	IF ISNULL(cString)
		ASSERT .F. MESSAGE "At cStringBreak procedure"
	ENDIF
	IF lTesting
		FPUTS(nFilenum,cString)
	ELSE
		FWRITE(nFilenum,cString)
	ENDIF
ENDPROC


****************************
PROCEDURE cszbreak
****************************
	cCSZ = ALLT(m.CSZ)

	FOR ii = 5 TO 2 STEP -1
		cCSZ = STRTRAN(cCSZ,SPACE(ii),SPACE(1))
	ENDFOR
	cCSZ = STRTRAN(cCSZ,",","")
	len1 = LEN(ALLT(cCSZ))
	nSpaces = OCCURS(" ",cCSZ)

	IF nSpaces<2
		cErrMsg = "BAD CSZ INFO"
		THROW
	ENDIF

*	ASSERT .F. MESSAGE "In CSZ Breakout"
	FOR ii = nSpaces TO 1 STEP -1
		ii1 = ALLT(STR(ii))
		DO CASE
			CASE ii = nSpaces
				nPOS = AT(" ",cCSZ,ii)
				cZip = ALLT(SUBSTR(cCSZ,nPOS))
*				WAIT WINDOW "ZIP: "+cZip TIMEOUT 1
				nEndState = nPOS
			CASE ii = (nSpaces - 1)
				nPOS = AT(" ",cCSZ,ii)
				cState = ALLT(SUBSTR(cCSZ,nPOS,nEndState-nPOS))
*				WAIT WINDOW "STATE: "+cState TIMEOUT 1
				IF nSpaces = 2
					cCity = ALLT(LEFT(cCSZ,nPOS))
*					WAIT WINDOW "CITY: "+cCity TIMEOUT 1
					EXIT
				ENDIF
			OTHERWISE
				nPOS = AT(" ",cCSZ,ii)
				cCity = ALLT(LEFT(cCSZ,nPOS))
*				WAIT WINDOW "CITY: "+cCity TIMEOUT 2
		ENDCASE
	ENDFOR
ENDPROC

****************************
PROCEDURE num_decrement
****************************
*!* This procedure decrements the ISA/GS numbers in the counter
*!* in the event of failure and deletion of the current 945
	IF lCloseOutput
		IF !USED("serfile")
			USE ("f:\3pl\data\serial\"+ccustname+"945_serial") IN 0 ALIAS serfile
		ENDIF
		SELECT serfile
		REPLACE serfile.seqnum WITH serfile.seqnum - 1 IN serfile  && ISA number
		REPLACE serfile.grpseqnum WITH serfile.grpseqnum - 1 IN serfile  && GS number
		RETURN
	ENDIF
ENDPROC

*********************
PROCEDURE headerfill
*********************
*!* HEADER LEVEL EDI DATA
	cISA_Num = PADL(c_CntrlNum,9,"0")
	nISA_Num = INT(VAL(cISA_Num))

	IF lTesting AND DATE()#{^2011-05-24}
		cISACode = "T"
	ELSE
		cISACode = "P"
	ENDIF

	STORE "ISA"+cfd+"00"+cfd+"          "+cfd+"00"+cfd+"          "+cfd+csendqual+cfd+csendidlong+cfd+;
		crecqual+cfd+crecidlong+cfd+cTruncDate+cfd+cTruncTime+cfd+"U"+cfd+"00401"+cfd+;
		cISA_Num+cfd+"0"+cfd+cISACode+cfd+cterminator+csegd TO cString
	DO cstringbreak

	STORE "GS"+cfd+"SW"+cfd+csendid+cfd+crecid+cfd+cdate+cfd+cTruncTime+cfd+c_CntrlNum+;
		cfd+"X"+cfd+cX12+csegd TO cString
	DO cstringbreak

ENDPROC
