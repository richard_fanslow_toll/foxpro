* this is actually move type not WO type

lparameters xaction, xtype, xloose, xbobtail, xdeadhead

do case
case xaction="RT"
	return "TRAILER MOVE"
case xaction="CM"
	return "CONTAINER MOVE"
case xaction="CH"
	return "CHASSIS MOVE"
case xaction="CP"
	return "CHASSIS PICKUP"
case xaction="CD"
	return "CHASSIS DROP"
case xdeadhead
	return "DEADHEAD"
case xbobtail
	return "BOBTAIL"
case xaction="OB"
	return "OB TO CONSOLIDATORS"
case xaction="DL" and xtype="A"
	return "AIR DELIVERY"
case xaction="DL" and xtype="D"
	return "DOMESTIC DELIVERY"
case xaction="DL" and xtype="O" and !xloose
	return "FULL OCEAN DELIVERY"
case xaction="DL" and xtype="O" and xloose
	return "LOOSE OCEAN DELIVERY"
case xaction="DL" and xtype=" "
	return "LINEHAUL DELIVERY"
case xaction="LD"
	return "EXPORT DELIVERY"
case xaction="EP"
	return "EMPTY EXPORT PICKUP"
case xaction="LP"
	return "LOOSE PICKUP"
case xaction="PU" and xtype="A"
	return "AIR PICKUP"
case xaction="PU" and xtype="D"
	return "DOMESTIC PICKUP"
case xaction="PU" and xtype="O" and !xloose
	return "FULL OCEAN PICKUP"
case xaction="PU" and xtype="O" and xloose
	return "LOOSE OCEAN PICKUP"
case xaction="PU" and xtype=" "
	return "LINEHAUL PICKUP"
case xaction="MT"
	return "EMPTY RETURN"
otherwise
	return "???"
endcase