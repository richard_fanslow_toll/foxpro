* Reformats, Adds TXT extension to the Platinum ACH files for CitiBank, and copies to the tollprod output folder.
* Runs a couple of times per night.
*
* 4/24/2018 MB: changed fmiint.com emails to Toll emails.
*
* EXE = F:\BOA\CITIRENAMEPLATACH.EXE

LPARAMETERS tcLast4AcctNum

LOCAL loCITIRENAMEPLATACH

runack("CITIRENAMEPLATACH")

utilsetup("CITIRENAMEPLATACH")

_SCREEN.CAPTION = "Rewrite ACH for CitiBank"

loCITIRENAMEPLATACH = CREATEOBJECT('CITIRENAMEPLATACH')
loCITIRENAMEPLATACH.MAIN( tcLast4AcctNum )

*loCITIRENAMEPLATACH.recoverarchived()

*loCITIRENAMEPLATACH.RecoverPosPay()


schedupdate()

CLOSE DATABASES ALL

RETURN

#DEFINE LOGIT 1
#DEFINE WAITIT 2
#DEFINE NOWAITIT 4
#DEFINE SENDIT 8
#DEFINE NOCRLF 16
#DEFINE CRLF CHR(13) + CHR(10)
#DEFINE LF CHR(10)
#DEFINE RETURN_DATA_MANDATORY .F.
#DEFINE RETURN_DATA_NOT_MANDATORY .T.
#DEFINE DAILY_OT_MULTIPLIER 1.5
#DEFINE DAILY_OT20_MULTIPLIER 2.0

DEFINE CLASS CITIRENAMEPLATACH AS CUSTOM

	cProcessName = 'CITIRENAMEPLATACH'

	lTestMode = .F.  && .F. FOR NORMAL OPERATION, .T. TO SUPPRESS ALL BUT DEVELOPER EMAILS AND CHANGE INPUT TABLE, ETC.

	* wait window properties
	nWaitWindowTimeout = 2
	lWaitWindowIsOn = .T.

	* logfile properties
	lLoggingIsOn = .T.
	cLogFile = 'F:\BOA\Logfiles\CITIRENAMEPLATACH_log.txt'

	* date/time properties
	cStartTime = TTOC(DATETIME())

	* file properties
	nNewFileHandle = -1
	nSourceFileHandle = -1

	* processing properties
	cLast4AcctNum = ''
	cFullAcctNum = ''
	
	* INTERNAL email properties
	lSendInternalEmailIsOn = .T.
	cFrom = 'TGF Corporate <fmicorporate@fmiint.com>'
	cSendTo = 'mark.bennett@tollgroup.com'
	cCC = ''
	cAttach = ''
	cBodyText = ''
	cSubject = 'CitiBank Rewrite Platinum ACH process for ' + TRANSFORM(DATETIME())

	cCOMPUTERNAME = ''
	cUSERNAME = ''

	FUNCTION INIT
		IF NOT DODEFAULT()
			RETURN .F.
		ENDIF
		WITH THIS
			CLEAR
			*SET RESOURCE OFF
			CLOSE DATA
			SET CENTURY ON
			SET DATE YMD
			SET HOURS TO 24
			SET ANSI ON
			SET TALK OFF
			SET DELETED ON
			SET CONSOLE OFF
			SET EXCLUSIVE OFF
			SET SAFETY OFF
			SET EXACT OFF
			SET STATUS BAR ON
			SET SYSMENU OFF
			SET ENGINEBEHAVIOR 70
			SET DECIMALS TO 4
			.cCOMPUTERNAME = GETENV("COMPUTERNAME")
			.cUSERNAME = GETENV("USERNAME")
			IF .lTestMode THEN
				.cSendTo = 'mark.bennett@tollgroup.com'
				.cCC = ''
				.cLogFile = 'F:\BOA\Logfiles\CITIRENAMEPLATACH_log_TESTMODE.txt'
			ENDIF
			.lLoggingIsOn = .lLoggingIsOn AND NOT EMPTY(.cLogFile)
			IF .lLoggingIsOn THEN
				SET ALTERNATE TO (.cLogFile) ADDITIVE
				SET ALTERNATE ON
			ENDIF
		ENDWITH
	ENDFUNC


	FUNCTION DESTROY
		WITH THIS
			IF .lLoggingIsOn  THEN
				SET ALTERNATE OFF
				SET ALTERNATE TO
			ENDIF
		ENDWITH
		DODEFAULT()
	ENDFUNC


	FUNCTION MAIN
		LPARAMETERS tcLast4AcctNum
		WITH THIS
			LOCAL lnNumberOfErrors, lcInputFolder, lcOutputFolder, laFiles[1,5], laFilesSorted[1,6], lcSourceFile, lcTargetFile
			LOCAL lnNumFiles, lnCurrentFile, lcString, lcNewFile, lcNewFileRoot, lcLineOut, lcLineIn, lcSourceFileName, lcArchivedFile

			TRY
				lnNumberOfErrors = 0
				lcTopBodyText = ''

				.TrackProgress('Rewrite ACH for CitiBank process started....', LOGIT+SENDIT)
				.TrackProgress('COMPUTERNAME = ' + .cCOMPUTERNAME, LOGIT+SENDIT)
				.TrackProgress('USERNAME = ' + .cUSERNAME, LOGIT+SENDIT)
				.TrackProgress('Project =  CITIRENAMEPLATACH', LOGIT+SENDIT)

				IF .lTestMode THEN
					.TrackProgress('                        TEST MODE                        ', LOGIT+SENDIT)
				ENDIF

				IF (NOT EMPTY('tcLast4AcctNum')) AND TYPE('tcLast4AcctNum') = 'C' THEN
					.cLast4AcctNum = ALLTRIM(tcLast4AcctNum)
				ELSE
					.TrackProgress('!!!!! ERROR: invalid or missing Acct # parameter!', LOGIT+SENDIT)
					THROW
				ENDIF

				.TrackProgress('.cLast4AcctNum = ' + .cLast4AcctNum, LOGIT+SENDIT)
				
				* calc full account # because it is not in the source file anywhere
				DO CASE
					CASE .cLast4AcctNum == '1067'
						.cFullAcctNum = '30831067'
					OTHERWISE
						.TrackProgress('!!!!! ERROR: unsupported account #: '+ .cLast4AcctNum, LOGIT+SENDIT)
						THROW
				ENDCASE
				

				IF .lTestMode THEN
					lcInputFolder = 'F:\UTIL\CITIRENAMEPLATACH\PLATACH\'
					lcArchiveFolder = 'F:\UTIL\CITIRENAMEPLATACH\PLATACH\ARCHIVED\'
					lcOutputFolder = 'F:\UTIL\CITIRENAMEPLATACH\TESTPRODOUT\'
				ELSE
					lcInputFolder = 'F:\FTPUSERS\CITIBANKOUT\PLATACH\'
					lcArchiveFolder = 'F:\FTPUSERS\CITIBANKOUT\PLATACH\ARCHIVED\'
					lcOutputFolder = 'F:\FTPUSERS\CITIBANKOUT\TOLLPROD\'
				ENDIF

				.TrackProgress('Input Folder = ' + lcInputFolder, LOGIT+SENDIT)
				.TrackProgress('Archived Folder = ' + lcArchiveFolder, LOGIT+SENDIT)
				.TrackProgress('Output Folder = ' + lcOutputFolder, LOGIT+SENDIT)

				lnNumFiles = ADIR(laFiles,(lcInputFolder + "*.*"))

				IF lnNumFiles > 0 THEN

					* sort file list by date/time
					.SortArrayByDateTime(@laFiles, @laFilesSorted)

					FOR lnCurrentFile = 1 TO lnNumFiles

						lcSourceFileName = laFilesSorted[lnCurrentFile,1]

						* make sure source filename is of format ACHXXXX where XXXX is the passed last 4 digits of the account #
						IF NOT UPPER(LEFT(lcSourceFileName,7)) == ("ACH" + .cLast4AcctNum) THEN
						
							.TrackProgress('WARNING: this source filename does not match the account # parameter: ' + lcSourceFileName + CRLF + 'This file will not be processed!', LOGIT+SENDIT)
							
						ELSE
							* Looks like we have the correct file, ok to process it.

							* create new file in archived folder, will copy source file to ARCHIVED folder for processing....
							lcSourceFile = lcInputFolder + lcSourceFileName
							lcArchivedFile = lcArchiveFolder + lcSourceFileName

							.TrackProgress('Processing file: ' + lcSourceFile, LOGIT+SENDIT)

							*lcNewFileRoot = "TOLLAP1067_" + DTOS(DATE()) + lcSourceFileName
							lcNewFileRoot = "TOLLAP1067_" + DTOS(DATE()) + STRTRAN(TIME(),":","")

							* note new file will be created in the input folder, will copy to ftp output folder later
							lcNewFile = lcArchiveFolder + lcNewFileRoot

							lcTargetFile = lcOutputFolder + lcNewFileRoot

							* append .TXT to target file if necessary
							IF NOT UPPER(RIGHT(lcTargetFile,4)) == ".TXT" THEN
								lcTargetFile = lcTargetFile + ".TXT"
							ENDIF

							* move source file to archived folder for further processing
							COPY FILE (lcSourceFile) TO (lcArchivedFile)
							IF NOT FILE(lcArchivedFile) THEN
								.TrackProgress('*** There was a problem copying [' + lcSourceFile + '] to [' + lcArchivedFile + ']', LOGIT+SENDIT)
								THROW
							ENDIF


							****************** add logic to create proper target filename !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

							**********************************************************************************************
							**********************************************************************************************

							.nNewFileHandle = FCREATE(lcNewFile)

							IF .nNewFileHandle < 1 THEN
								.TrackProgress('!!!! Error opening output file: ' + lcNewFile, LOGIT+SENDIT)
								THROW
							ENDIF
							**********************************************************************************************
							**********************************************************************************************


							* open source file FROM ARCHIVED FOLDER WHERE IT HAS BEEN MOVED
							.nSourceFileHandle = FOPEN(lcArchivedFile)

							IF .nSourceFileHandle < 1 THEN
								.TrackProgress('!!!! Error opening input file: ' + lcArchivedFile, LOGIT+SENDIT)
								THROW
							ENDIF
							
							* INIT KEY VARIABLES
							LOCAL lcValueDate, lcRecTypeCode, lcPayment, lcBeneficiaryAccountNum, lcBeneficiaryName, lcBankRoutingNum
							STORE '' TO lcValueDate, lcRecTypeCode, lcPayment, lcBeneficiaryAccountNum, lcBeneficiaryName, lcBankRoutingNum

							* read through source file, extracting and writing to new file
							DO WHILE NOT FEOF(.nSourceFileHandle)

								lcLineIn = ALLTRIM(FGETS(.nSourceFileHandle,100))
								
								lcRecTypeCode = LEFT(lcLineIn,1)
								
								DO CASE
									CASE lcRecTypeCode == '1'
										* we can skip this record
										
									CASE lcRecTypeCode == '5'
									
										* get the value date from this record
										lcValueDate = SUBSTR(lcLineIn,70,6)
										.TrackProgress('lcValueDate = ' + lcValueDate, LOGIT+SENDIT)
										
										* PREFIX year part of date with 20 for output file
										lcValueDate = "20" + lcValueDate
										
									CASE lcRecTypeCode == '6'
									
										* if we get to here with an empty Value Date, error out
										IF EMPTY(lcValueDate) THEN
											.TrackProgress('!!!!! ERROR: empty Value Date encountering Rec Type 6 - aborting process.', LOGIT+SENDIT)
											THROW										
										ENDIF
										
										* get Payment Amount
										lcPayment = SUBSTR(lcLineIn,30,8) + "." + SUBSTR(lcLineIn,38,2)
										
										* Debit Account - we already have it if we got to here
										
										* get Beneficiary Account #
										lcBeneficiaryAccountNum = ALLTRIM(SUBSTR(lcLineIn,13,17))
										
										* get Beneficiary Name
										lcBeneficiaryName = ALLTRIM(SUBSTR(lcLineIn,55,22))
										
										* get Bank Routing # - length is 9 because we are including the check digit
										lcBankRoutingNum = SUBSTR(lcLineIn,4,9)
										
										* assemble output line
										lcLineOut = ;
											"#US#ACH#" + ;
											lcValueDate + ;
											"######" + ;
											lcPayment + ;
											"##" + ;
											.cFullAcctNum + ;
											"###########" + ;
											lcValueDate + ;
											"####C###CCD#PAYMENT############01#" + ;
											lcBeneficiaryAccountNum + ;
											"#" + ;
											lcBeneficiaryName + ;
											"######" + ;
											lcBankRoutingNum + ;
											"##################################"											
										
										* write output line
										=FPUTS(.nNewFileHandle ,lcLineOut)
										
									CASE lcRecTypeCode == '8'
										* we can skip this record
										
									CASE lcRecTypeCode == '9'
										* we can skip this record
										
									OTHERWISE
										* unexpected rec type - just skip it
								
								ENDCASE				
								

*!*									* write output file
*!*									lcLineOut = lcLineIn
*!*									=FPUTS(.nNewFileHandle ,lcLineOut)

							ENDDO

							=FCLOSE(.nNewFileHandle)

							=FCLOSE(.nSourceFileHandle)

							**************************************************************
							*!* Send file to ftp output directory for poller to pickup
							**************************************************************
							* 'copy file' changes case of filename to lower, so use alternate method from Joe B.
							* cStr = Filetostr(filename), then strtofile(cStr(upper(filename)) to another folder does it for Joe B.
							*COPY FILE (lcSourceFile) TO (lcTargetFile)
							lcString = FILETOSTR(lcNewFile)
							=STRTOFILE(lcString,UPPER(lcTargetFile))
							**************************************************************

							IF FILE(lcTargetFile) THEN
								.TrackProgress('SUCCESS: Rewritten ACH file was copied to the FTP outbound folder!', LOGIT+SENDIT)
								.TrackProgress('Copied [' + lcNewFile + '] to [' + lcTargetFile + ']', LOGIT+SENDIT)
							ELSE
								.TrackProgress('ERROR: Rewritten ACH file was *NOT* copied to the FTP outbound folder!', LOGIT+SENDIT)
								.TrackProgress('*** There was a problem copying [' + lcNewFile + '] to [' + lcTargetFile + ']', LOGIT+SENDIT)
							ENDIF


							* delete the original source file in the source folder
							DELETE FILE (lcSourceFile)

						ENDIF && NOT UPPER(LEFT(,7)) == "ACH" + .cLast4AcctNum THEN
					
						WAIT WINDOW "Pausing to create unique filenames..." TIMEOUT 65

					ENDFOR && lnCurrentFile = 1 TO lnNumFiles

				ELSE

					* no files to process
					.TrackProgress('Found no files to process!', LOGIT+SENDIT)

				ENDIF  && lnNumFiles > 0


			CATCH TO loError

				.TrackProgress('There was an error.',LOGIT+SENDIT)
				.TrackProgress('The Error # is: ' + TRANSFORM(loError.ERRORNO), LOGIT+SENDIT)
				.TrackProgress('The Error Message is: ' + TRANSFORM(loError.MESSAGE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred in Program: ' + TRANSFORM(loError.PROCEDURE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred at line #: ' + TRANSFORM(loError.LINENO), LOGIT+SENDIT)
				lnNumberOfErrors = lnNumberOfErrors + 1
				
				*.cSendTo = 'mark.bennett@tollgroup.com, paul.gaidis@tollgroup.com'

				.cSendTo = 'mark.bennett@tollgroup.com'

			ENDTRY



			*!*				CLOSE DATABASES all

			WAIT CLEAR

			***************** INTERNAL email results ******************************
			.TrackProgress('About to send status email.',LOGIT)
			.TrackProgress('==================================================================================================================', SENDIT)
			.TrackProgress('The logfile is: ' + .cLogFile, SENDIT)
			.TrackProgress('Rewrite ACH for CitiBank process started: ' + .cStartTime, LOGIT+SENDIT)
			.TrackProgress('Rewrite ACH for CitiBank process finished: ' + TTOC(DATETIME()), LOGIT+SENDIT)

			IF .lSendInternalEmailIsOn THEN
				* try to trap error from not having dartmail dll's registered on user's pc...
				TRY
					DO FORM dartmail2 WITH .cSendTo,.cFrom,.cSubject,.cCC,.cAttach,.cBodyText,"A"
					.TrackProgress('Sent status email.',LOGIT)
				CATCH TO loError
					*=MESSAGEBOX('There was an error sending the status email. Please contact Mark Bennett.',0+64,"Citibank Rename Platinum ACH")
					.TrackProgress('There was an error sending the status email.',LOGIT)
					.TrackProgress('The Error # is: ' + TRANSFORM(loError.ERRORNO), LOGIT)
					.TrackProgress('The Error Message is: ' + TRANSFORM(loError.MESSAGE), LOGIT)
					.TrackProgress('The Error occurred in Program: ' + TRANSFORM(loError.PROCEDURE), LOGIT)
					.TrackProgress('The Error occurred at line #: ' + TRANSFORM(loError.LINENO), LOGIT)
					lnNumberOfErrors = lnNumberOfErrors + 1
					*!*						CLOSE DATA
				ENDTRY

			ELSE
				.TrackProgress('Did not send status email: THIS.lSendInternalEmailIsOn = FALSE.',LOGIT)
			ENDIF

		ENDWITH
		RETURN
	ENDFUNC && main


*!*		FUNCTION recoverarchived
*!*			WITH THIS
*!*				LOCAL lnNumberOfErrors, lcInputFolder, lcOutputFolder, laFiles[1,5], laFilesSorted[1,6], lcSourceFile, lcTargetFile
*!*				LOCAL lnNumFiles, lnCurrentFile, lcString, lcNewFile, lcNewFileRoot, lcLineOut, lcLineIn, lcSourceFileName, lcArchivedFile
*!*				LOCAL lcOutputFile

*!*				TRY
*!*					lnNumberOfErrors = 0
*!*					lcTopBodyText = ''

*!*					.TrackProgress('Recover ACH for CitiBank process started....', LOGIT+SENDIT)
*!*					.TrackProgress('COMPUTERNAME = ' + .cCOMPUTERNAME, LOGIT+SENDIT)
*!*					.TrackProgress('USERNAME = ' + .cUSERNAME, LOGIT+SENDIT)
*!*					.TrackProgress('Project =  CITIRENAMEPLATACH', LOGIT+SENDIT)

*!*					IF .lTestMode THEN
*!*						.TrackProgress('                        TEST MODE                        ', LOGIT+SENDIT)
*!*					ENDIF
*!*					
*!*					SET DATE YMD
*!*					SET CENTURY ON
*!*					SET DECIMALS TO 2

*!*					.cLast4AcctNum = '1067'
*!*					.cFullAcctNum = '30831067'
*!*					
*!*					lcInputFolder = 'F:\FTPUSERS\CITIBANKOUT\PLATACH\TESTRECOVER\'
*!*					lcOutputFolder = 'F:\FTPUSERS\CITIBANKOUT\PLATACH\recoveredxls\'
*!*					
*!*					lcOutputFile = lcOutputFolder + 'ACH_RECOVERED_' + DTOS(DATE()) + '.XLS'

*!*					.TrackProgress('Input Folder = ' + lcInputFolder, LOGIT+SENDIT)
*!*					.TrackProgress('Output Folder = ' + lcOutputFolder, LOGIT+SENDIT)

*!*					CREATE CURSOR CURACH( ACCOUNT C(8), VALUEDATE D, PAYMENT N(12,2), BENEACCT C(17), BENENAME C(22), ROUTINGNUM C(9), SOURCEFILE C(50) )

*!*					PRIVATE m.ACCOUNT, m.VALUEDATE, m.PAYMENT, m.BENEACCT, m.BENENAME, m.ROUTINGNUM, m.SOURCEFILE
*!*					
*!*					m.ACCOUNT = .cFullAcctNum

*!*					lnNumFiles = ADIR(laFiles,(lcInputFolder + "ACH1067*.*"))

*!*					IF lnNumFiles > 0 THEN

*!*						* sort file list by date/time
*!*						.SortArrayByDateTime(@laFiles, @laFilesSorted)

*!*						FOR lnCurrentFile = 1 TO lnNumFiles

*!*							lcSourceFileName = laFilesSorted[lnCurrentFile,1]
*!*							
*!*								m.SOURCEFILE = lcSourceFileName

*!*								lcSourceFile = lcInputFolder + lcSourceFileName

*!*								.TrackProgress('Processing file: ' + lcSourceFile, LOGIT+SENDIT)


*!*								* open source file FROM ARCHIVED FOLDER WHERE IT HAS BEEN MOVED
*!*								.nSourceFileHandle = FOPEN(lcSourceFile)

*!*								IF .nSourceFileHandle < 1 THEN
*!*									.TrackProgress('!!!! Error opening input file: ' + lcSourceFile, LOGIT+SENDIT)
*!*									THROW
*!*								ENDIF
*!*								
*!*								* INIT KEY VARIABLES
*!*								LOCAL lcValueDate, lcRecTypeCode, lcPayment, lcBeneficiaryAccountNum, lcBeneficiaryName, lcBankRoutingNum
*!*								STORE '' TO lcValueDate, lcRecTypeCode, lcPayment, lcBeneficiaryAccountNum, lcBeneficiaryName, lcBankRoutingNum
*!*								
*!*								
*!*								
*!*								STORE 0.00 TO m.PAYMENT
*!*								STORE {} TO m.VALUEDATE
*!*								STORE '' TO m.BENEACCT, m.BENENAME, m.ROUTINGNUM

*!*								* read through source file, extracting and writing to new file
*!*								DO WHILE NOT FEOF(.nSourceFileHandle)

*!*									lcLineIn = ALLTRIM(FGETS(.nSourceFileHandle,100))
*!*									
*!*									lcRecTypeCode = LEFT(lcLineIn,1)
*!*									
*!*									DO CASE
*!*										CASE lcRecTypeCode == '1'
*!*											* we can skip this record
*!*											
*!*										CASE lcRecTypeCode == '5'
*!*										
*!*											* get the value date from this record
*!*											lcValueDate = SUBSTR(lcLineIn,70,6)
*!*											.TrackProgress('lcValueDate = ' + lcValueDate, LOGIT+SENDIT)
*!*											
*!*											* PREFIX year part of date with 20 for output file
*!*											lcValueDate = "20" + lcValueDate
*!*											* E.G. lcValueDate = "20161222" 
*!*											
*!*											m.VALUEDATE = LEFT(lcValueDate,4) + "/" + SUBSTR(lcValueDate,5,2) + "/" + RIGHT(lcValueDate,2)
*!*											m.VALUEDATE = CTOD(m.VALUEDATE)
*!*											
*!*										CASE lcRecTypeCode == '6'
*!*										
*!*											* if we get to here with an empty Value Date, error out
*!*											IF EMPTY(m.VALUEDATE) THEN
*!*												.TrackProgress('!!!!! ERROR: empty Value Date encountering Rec Type 6 - aborting process.', LOGIT+SENDIT)
*!*												THROW										
*!*											ENDIF
*!*											
*!*											* get Payment Amount
*!*											lcPayment = SUBSTR(lcLineIn,30,8) + "." + SUBSTR(lcLineIn,38,2)
*!*											
*!*											m.PAYMENT = VAL(lcPayment)
*!*											
*!*											* Debit Account - we already have it if we got to here
*!*											
*!*											* get Beneficiary Account #
*!*											lcBeneficiaryAccountNum = ALLTRIM(SUBSTR(lcLineIn,13,17))
*!*											
*!*											m.BENEACCT = ALLTRIM(lcBeneficiaryAccountNum)
*!*											
*!*											* get Beneficiary Name
*!*											lcBeneficiaryName = ALLTRIM(SUBSTR(lcLineIn,55,22))
*!*											
*!*											m.BENENAME = ALLTRIM(lcBeneficiaryName)
*!*											
*!*											* get Bank Routing # - length is 9 because we are including the check digit
*!*											lcBankRoutingNum = SUBSTR(lcLineIn,4,9)
*!*											
*!*											m.ROUTINGNUM = lcBankRoutingNum
*!*											
*!*											INSERT INTO CURACH FROM MEMVAR
*!*											
*!*											
*!*										CASE lcRecTypeCode == '8'
*!*											* we can skip this record
*!*											
*!*										CASE lcRecTypeCode == '9'
*!*											* we can skip this record
*!*											
*!*										OTHERWISE
*!*											* unexpected rec type - just skip it
*!*									
*!*									ENDCASE				

*!*								ENDDO

*!*								=FCLOSE(.nSourceFileHandle)

*!*						ENDFOR && lnCurrentFile = 1 TO lnNumFiles
*!*						
*!*						SELECT CURACH
*!*						COPY TO (lcOutputFile) XL5

*!*					ELSE

*!*						* no files to process
*!*						.TrackProgress('Found no files to process!', LOGIT+SENDIT)

*!*					ENDIF  && lnNumFiles > 0


*!*				CATCH TO loError

*!*					.TrackProgress('There was an error.',LOGIT+SENDIT)
*!*					.TrackProgress('The Error # is: ' + TRANSFORM(loError.ERRORNO), LOGIT+SENDIT)
*!*					.TrackProgress('The Error Message is: ' + TRANSFORM(loError.MESSAGE), LOGIT+SENDIT)
*!*					.TrackProgress('The Error occurred in Program: ' + TRANSFORM(loError.PROCEDURE), LOGIT+SENDIT)
*!*					.TrackProgress('The Error occurred at line #: ' + TRANSFORM(loError.LINENO), LOGIT+SENDIT)
*!*					lnNumberOfErrors = lnNumberOfErrors + 1
*!*					.cSendTo = 'mark.bennett@tollgroup.com'


*!*				ENDTRY


*!*				WAIT CLEAR

*!*				***************** INTERNAL email results ******************************
*!*				.TrackProgress('About to send status email.',LOGIT)
*!*				.TrackProgress('==================================================================================================================', SENDIT)
*!*				.TrackProgress('The logfile is: ' + .cLogFile, SENDIT)
*!*				.TrackProgress('Recover ACH for CitiBank process started: ' + .cStartTime, LOGIT+SENDIT)
*!*				.TrackProgress('Recover ACH for CitiBank process finished: ' + TTOC(DATETIME()), LOGIT+SENDIT)

*!*				IF .lSendInternalEmailIsOn THEN
*!*					* try to trap error from not having dartmail dll's registered on user's pc...
*!*					TRY
*!*						DO FORM dartmail2 WITH .cSendTo,.cFrom,.cSubject,.cCC,.cAttach,.cBodyText,"A"
*!*						.TrackProgress('Sent status email.',LOGIT)
*!*					CATCH TO loError
*!*						*=MESSAGEBOX('There was an error sending the status email. Please contact Mark Bennett.',0+64,"Citibank Rename Platinum ACH")
*!*						.TrackProgress('There was an error sending the status email.',LOGIT)
*!*						.TrackProgress('The Error # is: ' + TRANSFORM(loError.ERRORNO), LOGIT)
*!*						.TrackProgress('The Error Message is: ' + TRANSFORM(loError.MESSAGE), LOGIT)
*!*						.TrackProgress('The Error occurred in Program: ' + TRANSFORM(loError.PROCEDURE), LOGIT)
*!*						.TrackProgress('The Error occurred at line #: ' + TRANSFORM(loError.LINENO), LOGIT)
*!*						lnNumberOfErrors = lnNumberOfErrors + 1
*!*						*!*						CLOSE DATA
*!*					ENDTRY

*!*				ELSE
*!*					.TrackProgress('Did not send status email: THIS.lSendInternalEmailIsOn = FALSE.',LOGIT)
*!*				ENDIF

*!*			ENDWITH
*!*			RETURN
*!*		ENDFUNC && recoverarchived



*!*		FUNCTION RecoverPosPay
*!*			WITH THIS
*!*				LOCAL lnNumberOfErrors, lcInputFolder, lcOutputFolder, laFiles[1,5], laFilesSorted[1,6], lcSourceFile, lcTargetFile
*!*				LOCAL lnNumFiles, lnCurrentFile, lcString, lcNewFile, lcNewFileRoot, lcLineOut, lcLineIn, lcSourceFileName, lcArchivedFile
*!*				LOCAL lcOutputFile

*!*				TRY
*!*					lnNumberOfErrors = 0
*!*					lcTopBodyText = ''

*!*					.TrackProgress('Recover PosPay for CitiBank process started....', LOGIT+SENDIT)
*!*					.TrackProgress('COMPUTERNAME = ' + .cCOMPUTERNAME, LOGIT+SENDIT)
*!*					.TrackProgress('USERNAME = ' + .cUSERNAME, LOGIT+SENDIT)
*!*					.TrackProgress('Project =  CITIRENAMEPLATACH', LOGIT+SENDIT)

*!*					IF .lTestMode THEN
*!*						.TrackProgress('                        TEST MODE                        ', LOGIT+SENDIT)
*!*					ENDIF
*!*					
*!*					SET DATE YMD
*!*					SET CENTURY ON
*!*					SET DECIMALS TO 2

*!*						
*!*					lcInputFolder = 'F:\FTPUSERS\CITIBANKOUT\RECOVERPOSPAY\'
*!*					lcOutputFolder = 'F:\FTPUSERS\CITIBANKOUT\RECOVERPOSPAY\RECOVEREDXLS\'
*!*					
*!*					lcOutputFile = lcOutputFolder + 'POSPAY_RECOVERED_' + DTOS(DATE()) + '.XLS'

*!*					.TrackProgress('Input Folder = ' + lcInputFolder, LOGIT+SENDIT)
*!*					.TrackProgress('Output Folder = ' + lcOutputFolder, LOGIT+SENDIT)

*!*					CREATE CURSOR CURPOSPAY( ACCOUNT C(8), CHKAMT N(12,2), CHKNUM C(10), ISSUEDATE D, VOID C(1), PAYEE C(60), SOURCEFILE C(50) )

*!*					PRIVATE m.ACCOUNT, m.ISSUEDATE, m.CHKAMT, m.CHKNUM, m.VOID, m.PAYEE,  m.SOURCEFILE

*!*					lnNumFiles = ADIR(laFiles,(lcInputFolder + "ISS4339*.TXT"))

*!*					IF lnNumFiles > 0 THEN

*!*						* sort file list by date/time
*!*						.SortArrayByDateTime(@laFiles, @laFilesSorted)

*!*						FOR lnCurrentFile = 1 TO lnNumFiles

*!*							lcSourceFileName = laFilesSorted[lnCurrentFile,1]
*!*							
*!*								m.SOURCEFILE = lcSourceFileName

*!*								lcSourceFile = lcInputFolder + lcSourceFileName

*!*								.TrackProgress('Processing file: ' + lcSourceFile, LOGIT+SENDIT)


*!*								* open source file FROM ARCHIVED FOLDER WHERE IT HAS BEEN MOVED
*!*								.nSourceFileHandle = FOPEN(lcSourceFile)

*!*								IF .nSourceFileHandle < 1 THEN
*!*									.TrackProgress('!!!! Error opening input file: ' + lcSourceFile, LOGIT+SENDIT)
*!*									THROW
*!*								ENDIF
*!*								
*!*								
*!*								STORE 0.00 TO m.CHKAMT
*!*								STORE {} TO m.ISSUEDATE
*!*								STORE '' TO m.ACCOUNT, m.CHKNUM, m.PAYEE, m.VOID

*!*								* read through source file, extracting and writing to new file
*!*								DO WHILE NOT FEOF(.nSourceFileHandle)

*!*									lcLineIn = ALLTRIM(FGETS(.nSourceFileHandle,200))
*!*									
*!*									lcRecTypeCode = LEFT(lcLineIn,1)
*!*									
*!*									DO CASE
*!*										CASE lcRecTypeCode == 'T'
*!*											* we can skip this TRAILER record
*!*											
*!*										OTHERWISE
*!*											
*!*											IF NOT EMPTY(lcLineIn) THEN
*!*											
*!*												m.ACCOUNT = SUBSTR(lcLineIn,1,8)
*!*												
*!*												m.CHKAMT = SUBSTR(lcLineIn,9,11)
*!*												m.CHKAMT = VAL(m.CHKAMT) / 100
*!*												
*!*												m.CHKNUM = SUBSTR(lcLineIn,20,10)
*!*											
*!*												m.ISSUEDATE = SUBSTR(lcLineIn,30,6)
*!*												
*!*												* PREFIX year part of date with 20 for output file
*!*												m.ISSUEDATE = "20" + m.ISSUEDATE
*!*												
*!*												m.ISSUEDATE = LEFT(m.ISSUEDATE,4) + "/" + SUBSTR(m.ISSUEDATE,5,2) + "/" + RIGHT(m.ISSUEDATE,2)
*!*												m.ISSUEDATE = CTOD(m.ISSUEDATE)
*!*												
*!*												m.VOID = SUBSTR(lcLineIn,66,1)
*!*												
*!*												m.PAYEE = ALLTRIM(SUBSTR(lcLineIn,67,60))
*!*												
*!*											
*!*												
*!*												INSERT INTO CURPOSPAY FROM MEMVAR
*!*											
*!*											ENDIF && NOT EMPTY(lcLineIn)
*!*									
*!*									ENDCASE				

*!*								ENDDO

*!*								=FCLOSE(.nSourceFileHandle)

*!*						ENDFOR && lnCurrentFile = 1 TO lnNumFiles
*!*						
*!*						SELECT CURPOSPAY
*!*						COPY TO (lcOutputFile) XL5

*!*					ELSE

*!*						* no files to process
*!*						.TrackProgress('Found no files to process!', LOGIT+SENDIT)

*!*					ENDIF  && lnNumFiles > 0


*!*				CATCH TO loError

*!*					.TrackProgress('There was an error.',LOGIT+SENDIT)
*!*					.TrackProgress('The Error # is: ' + TRANSFORM(loError.ERRORNO), LOGIT+SENDIT)
*!*					.TrackProgress('The Error Message is: ' + TRANSFORM(loError.MESSAGE), LOGIT+SENDIT)
*!*					.TrackProgress('The Error occurred in Program: ' + TRANSFORM(loError.PROCEDURE), LOGIT+SENDIT)
*!*					.TrackProgress('The Error occurred at line #: ' + TRANSFORM(loError.LINENO), LOGIT+SENDIT)
*!*					lnNumberOfErrors = lnNumberOfErrors + 1
*!*					.cSendTo = 'mark.bennett@tollgroup.com'


*!*				ENDTRY


*!*				WAIT CLEAR

*!*				***************** INTERNAL email results ******************************
*!*				.TrackProgress('About to send status email.',LOGIT)
*!*				.TrackProgress('==================================================================================================================', SENDIT)
*!*				.TrackProgress('The logfile is: ' + .cLogFile, SENDIT)
*!*				.TrackProgress('Recover PosPay for CitiBank process started: ' + .cStartTime, LOGIT+SENDIT)
*!*				.TrackProgress('Recover PosPay for CitiBank process finished: ' + TTOC(DATETIME()), LOGIT+SENDIT)

*!*				IF .lSendInternalEmailIsOn THEN
*!*					* try to trap error from not having dartmail dll's registered on user's pc...
*!*					TRY
*!*						DO FORM dartmail2 WITH .cSendTo,.cFrom,.cSubject,.cCC,.cAttach,.cBodyText,"A"
*!*						.TrackProgress('Sent status email.',LOGIT)
*!*					CATCH TO loError
*!*						*=MESSAGEBOX('There was an error sending the status email. Please contact Mark Bennett.',0+64,"Citibank Rename Platinum ACH")
*!*						.TrackProgress('There was an error sending the status email.',LOGIT)
*!*						.TrackProgress('The Error # is: ' + TRANSFORM(loError.ERRORNO), LOGIT)
*!*						.TrackProgress('The Error Message is: ' + TRANSFORM(loError.MESSAGE), LOGIT)
*!*						.TrackProgress('The Error occurred in Program: ' + TRANSFORM(loError.PROCEDURE), LOGIT)
*!*						.TrackProgress('The Error occurred at line #: ' + TRANSFORM(loError.LINENO), LOGIT)
*!*						lnNumberOfErrors = lnNumberOfErrors + 1
*!*						*!*						CLOSE DATA
*!*					ENDTRY

*!*				ELSE
*!*					.TrackProgress('Did not send status email: THIS.lSendInternalEmailIsOn = FALSE.',LOGIT)
*!*				ENDIF

*!*			ENDWITH
*!*			RETURN
*!*		ENDFUNC && recoverPOSPAY




	FUNCTION ExecSQL
		LPARAMETERS tcSQL, tcCursorName, tlNoDataReturnedIsOkay
		LOCAL llRetval, lnResult
		WITH THIS
			* close target cursor if it's open
			IF USED(tcCursorName)
				USE IN (tcCursorName)
			ENDIF
			WAIT WINDOW "Fetching remote data; please wait..." NOWAIT
			lnResult = SQLEXEC(.nSQLHandle, tcSQL, tcCursorName)
			llRetval = ( lnResult > 0 )
			IF llRetval THEN
				* see if any data came back
				IF NOT tlNoDataReturnedIsOkay THEN
					IF (NOT USED(tcCursorName)) OR EOF(tcCursorName) THEN
						llRetval = .F.
						.TrackProgress('No data was returned by this query: ' +CRLF + tcSQL, LOGIT+SENDIT)
					ENDIF
				ENDIF
			ELSE
				.TrackProgress("Result Code [" + TRANSFORM(lnResult) + "] returned by this query: " + tcSQL, LOGIT+SENDIT)
			ENDIF
			WAIT CLEAR
			RETURN llRetval
		ENDWITH
	ENDFUNC


	PROCEDURE TrackProgress
		* do any combination of Wait Window, writing to logfile, and adding to body of email,
		* based on nFlags parameter.
		LPARAMETERS tcExpression, tnFlags
		LOCAL lcCRLF
		WITH THIS
			IF BITAND(tnFlags,NOCRLF) = NOCRLF THEN
				lcCRLF = ""
			ELSE
				lcCRLF = CRLF
			ENDIF
			IF BITAND(tnFlags,LOGIT) = LOGIT THEN
				IF .lLoggingIsOn THEN
					?
					? .cProcessName + "  " + TTOC(DATETIME()) + ": " + tcExpression
				ENDIF
			ENDIF
			IF .lWaitWindowIsOn THEN
				IF BITAND(tnFlags,NOWAITIT) = NOWAITIT THEN
					WAIT WINDOW tcExpression NOWAIT
				ENDIF
				IF BITAND(tnFlags,WAITIT) = WAITIT THEN
					WAIT WINDOW tcExpression TIMEOUT .nWaitWindowTimeout
				ENDIF
			ENDIF
			IF BITAND(tnFlags,SENDIT) = SENDIT THEN
				IF .lSendInternalEmailIsOn THEN
					.cBodyText = .cBodyText + tcExpression + CRLF + lcCRLF
				ENDIF
			ENDIF
		ENDWITH
	ENDPROC  &&  TrackProgress


	PROCEDURE SortArrayByDateTime
		* expects taInArray[] to be the result of an ADIR()
		LPARAMETERS taInArray, taOutArray
		EXTERNAL ARRAY taInArray, taOutArray
		LOCAL lnRows, lnCols, i, j
		* extending taInArray[] to process the files in date/time order
		lnRows = ALEN(taInArray,1)
		lnCols = ALEN(taInArray,2)
		* create taOutArray with one more column
		DIMENSION taOutArray[lnRows,lnCols + 1]
		* and fill it with taInArray columns AND (filedate + filetime) in new column
		* ( ACOPY() doesn't preserve elements properly because of different # of columns )
		FOR i = 1 TO lnRows
			FOR j = 1 TO lnCols
				taOutArray[i,j] = taInArray[i,j]
			ENDFOR
			taOutArray[i,lnCols + 1] = DTOS(taInArray[i,3]) + "_" + taInArray[i,4]
		ENDFOR
		* sort taOutArray on date/time column
		=ASORT(taOutArray,6)
	ENDPROC

ENDDEFINE
