*!* TKO 945 (Whse. Shipping Advice) Creation Program
*!* Creation Date: 12.03.2008 by Joe (Derived from Cleatskins 945_create program)

PARAMETERS nAcctNum,cBOL,cShip_ref,cOffice

PUBLIC ARRAY thisarray(1)
PUBLIC c_CntrlNum,c_GrpCntrlNum,cReturned,cWO_Num,cWO_NumStr,nWO_Num1,lTesting,lTestmail,dapptnum,lOverflow
PUBLIC lEmail,lFilesout,dDateTimeCal,nFilenum,nWO_Xcheck,nOutshipid,cCharge,lDoSQL,nZcnt,nTcnt,cStatus
PUBLIC cCity,cState,cZip,nOrigSeq,nOrigGrpSeq,tsendto,tcc,tsendtoerr,tccerr,cErrMsg,cProgname,lSolidpack,cSubBOLDetail
PUBLIC cMod,cMBOL,cEDIType,lParcelType,cFilenameShort,cFilenameOut,cFilenameArch,lcPath,cWO_NumList,cISA_Num,lLoadSQL

lTesting   = .f.  && Set to .t. for testing
lTestinput = .F.  &&  Set to .t. for test input files only!
lDoTrapMiscounts = .T. && Set to .f. to ignore mismatched counts in SQL vs. Outdet (only do that when testing SQL)
closedata()

DO m:\dev\prg\_setvars WITH lTesting
cProgname = "tko945_create_mbol"

WAIT WINDOW  "At start of TKO MBOL process" nowait

IF lTesting
	CLOSE DATABASES ALL
ENDIF

lDoSQL = .T.
lPick = .F.
lPrepack = .F.
lJCP = .F.
lIsError = .F.
lDoCatch = .T.
cShip_ref = ""
lCloseOutput = .T.
nFilenum = 0
cCharge = "0.00"
ccustname = ""
cWO_NumStr = ""
cWO_NumList = ""
cString = ""
cErrMsg = ""
nPTCtnTot = 0
cStatus = ""
lSolidpack = .F.
cEDIType = "945"
lookups()
lOverflow = .f.

*!* Note: For Wal-mart testing, be sure that there is a BOL and DEL_DATE filled in on the test OUTSHIP table,
*!* and that it matches the test data below


TRY
	lFederated = .F.
	IF lTesting
		nAcctNum = 5836
	ENDIF
	nOrigSeq = 0
	nOrigGrpSeq = 0
	cPPName = "TKO"
	WAIT WINDOW "At the start of "+cPPName+" 945 PICKPACK (MBOL) process..." TIMEOUT 1
	tfrom = "TGF Warehouse Operations <warehouse-ops@tollgroup.com>"
	tmessage=""
	tcc = ""
	lUPSType = .F.
	lDoBOLGEN = .F.
	lSamples = .F.
	lISAFlag = .T.
	lSTFlag = .T.
	nLoadid = 1
	lSQLMail = .F.

	IF TYPE("cOffice") = "L"
		IF !lTesting AND !lTestinput
			WAIT WINDOW "Office not provided...terminating" TIMEOUT 2
			lCloseOutput = .F.
			cErrMsg = "NO OFFICE"
			DO ediupdate WITH cErrMsg,.T.
			THROW
		ELSE
			CLOSE DATABASES ALL
			IF !USED('edi_trigger')
				cEDIFolder = IIF(DATE()<{^2010-01-24},"F:\JAG3PL\DATA\","F:\3PL\DATA\")
				USE (cEDIFolder+"edi_trigger") IN 0 ALIAS edi_trigger
			ENDIF
*!* TEST DATA AREA
			nAcctNum = 5836
			cBOL = "04907315865822721"
			cShip_ref = "SPS-1214655"
			cOffice = "C"
			cTime = DATETIME()
		ENDIF
	ENDIF
	cOffice = UPPER(cOffice)
	CREATE CURSOR temp945 (accountid i,isa_num c(10),st_num c(10),wo_num i,bol_no c(20),ship_ref c(20),filename c(50),filedate T)
	cBOL=TRIM(cBOL)
	cMod = Icase(cOffice = "C","2",cOffice = "N","I","M")
	gMasterOffice = cOffice
	goffice = cMod
	cMBOL = cBOL
	cEDIType = "945"
	lParcelType = .F.
	cSubBOLDetail = ""

	lLoadSQLBL = .T.
	cRetMsg = "X"

	IF lTestinput
		cUseFolder = "C:\TEMPFOX\"
	ENDIF

	lEmail = .T.
	lTestmail = IIF(lTesting,.T.,.F.) && Sends mail to Joe only
	lFilesout = IIF(lTesting,.F.,.T.) && If true, copies output to FTP folder (default = .t.)
	lStandalone = IIF(lTesting,.T.,.F.)

*!*	lTestMail = .t.
*!*	lFilesout = .F.

	STORE "LB" TO cWeightUnit
	cfd = "*"
	csegd = "~"
	nSegCtr = 0
	cterminator = ">" && Used at end of ISA segment

	WAIT WINDOW "Now preparing tables and variables" NOWAIT NOCLEAR

*!* SET CUSTOMER CONSTANTS
	ccustname = "TKO"  && Customer Identifier
	cX12 = "004010"  && X12 Standards Set used
	cMailName = "TKO"

*	ASSERT .F. MESSAGE "At MAIL population...>>DEBUG<<"
	IF USED('mm')
		USE IN mm
	ENDIF
	SELECT 0
	USE F:\3pl\DATA\mailmaster ALIAS mm SHARED
	LOCATE
	IF lTestmail OR lTesting
		LOCATE FOR mm.edi_type = "MISC" AND mm.taskname = "GENERAL"
	ELSE
		LOCATE FOR mm.edi_type = "945" AND mm.office = cOffice AND mm.GROUP = "TKO"
	ENDIF
	tsendto = IIF(mm.use_alt,mm.sendtoalt,mm.sendto)
	tcc = IIF(mm.use_alt,mm.ccalt,mm.cc)
	LOCATE
	LOCATE FOR mm.edi_type = "MISC" AND mm.taskname = "GENERAL"
	tsendtoerr = IIF(mm.use_alt,mm.sendtoalt,mm.sendto)
	tccerr = IIF(mm.use_alt,mm.ccalt,mm.cc)
	USE IN mm

*!* SET OTHER CONSTANTS
	DO CASE
		CASE INLIST(cOffice,"N","I")
			cCustLoc =  "NJ"
			cFMIWarehouse = ""
			cCustPrefix = "945j"
			cDivision = "New Jersey"
			cSF_Addr1  = "800 FEDERAL BLVD"
			cSF_CSZ    =  "CARTERET"+cfd+"NJ"+cfd+"07008"

		CASE cOffice = "M"
			cCustLoc =  "FL"
			cFMIWarehouse = ""
			cCustPrefix = "945f"
			cDivision = "Florida"
			cSF_Addr1  = "11400 NW 32ND AVE"
			cSF_CSZ    = "MIAMI"+cfd+"FL"+cfd+"33167"

		OTHERWISE
			cCustLoc =  "CA"
			cFMIWarehouse = ""
			cCustPrefix = "945c"
			cDivision = "California"
			cSF_Addr1  = "450 WESTMONT DRIVE"
			cSF_CSZ    = "SAN PEDRO"+cfd+"CA"+cfd+"90731"
	ENDCASE
	IF lTesting
		cCustPrefix = "945t"
	ENDIF
	
	SET STEP ON 
	swc_cutctns(cBOL)

	cCustFolder = UPPER(ccustname)+"-"+cCustLoc

	lNonEDI = .F.  && Assumes not a Non-EDI 945
	STORE "" TO lcKey
	STORE 0 TO alength,nLength
	cTime = DATETIME()
	cDelTime = SUBSTR(TTOC(cTime,1),9,4)
	cCarrierType = "M" && Default as "Motor Freight"
	dapptnum = ""

*!* SET OTHER CONSTANTS
	IF INLIST(cOffice,'C','1','2')
		dDateTimeCal = (DATETIME()-(3*3600))
	ELSE
		dDateTimeCal = DATETIME()
	ENDIF
	dt1 = TTOC(dDateTimeCal,1)
	dtmail = TTOC(dDateTimeCal)
	dt2 = dDateTimeCal
	cString = ""

	csendqual = "ZZ"
	csendid = "FMIX"
	crecqual = "ZZ"
	crecid = "TKO"

	cdate = DTOS(DATE())
	cTruncDate = RIGHT(cdate,6)
	cTruncTime = SUBSTR(TTOC(dDateTimeCal),9,4)
	cfiledate = cdate+cTruncTime
	cOrig = "J"
	nSTCount = 0
	cStyle = ""
	cPTString = ""
	nTotCtnWt = 0
	nTotCtnCount = 0
	nUnitSum = 0
	cTargetStyle = ""

** PAD ID Codes
	csendidlong = PADR(csendid,15," ")
	crecidlong = PADR(crecid,15," ")

	lcPath = ("f:\ftpusers\"+cCustFolder+"\945OUT\")
	cFilenameHold = ("f:\ftpusers\"+cCustFolder+"\945-Staging\"+cCustPrefix+dt1+".txt")
	cFilenameShort = JUSTFNAME(cFilenameHold)
	cFilenameOut = (lcPath+cFilenameShort)
	cFilenameArch = ("f:\ftpusers\"+cCustFolder+"\945OUT\Archive\"+cFilenameShort)
	nFilenum = FCREATE(cFilenameHold)

		csq1 = [select * from outship where accountid in (]+gtkoaccounts+[) and office = ']+cOffice+[' and bol_no = ']+cBOL+[']
		xsqlexec(csq1,,,"wh")
		IF RECCOUNT() = 0
			cErrMsg = "MISS BOL "+cBOL
			DO ediupdate WITH cErrMsg,.T.
			THROW
		ENDIF
		INDEX ON bol_no TAG bol_no
		INDEX on wo_num TAG wo_num
		INDEX on ship_ref TAG ship_ref
	
	SELECT outshipid ;
		FROM outship ;
		WHERE bol_no = cBOL ;
		INTO CURSOR tempsr
	SELECT tempsr
	WAIT WINDOW "There are "+ALLTRIM(STR(RECCOUNT()))+" outshipid's to check" NOWAIT

	SELECT outship
	IF !SEEK(cBOL,'outship','bol_no')
		WAIT WINDOW "BOL not found in OUTSHIP" TIMEOUT 2
		cErrMsg = "BOL NOT FOUND"
		DO ediupdate WITH cErrMsg,.T.
		THROW
	ELSE
		nWO_num = outship.wo_num
	ENDIF
	cWO_Num = ALLTRIM(STR(nWO_num))


	IF USED('OUTDET')
		USE IN outdet
	ENDIF
		selectoutdet()
		SELECT outdet
		INDEX ON outdetid TAG outdetid

	IF !USED("scacs")
		xsqlexec("select * from scac","scacs",,"wh")
		INDEX ON scac TAG scac
	ENDIF

	STORE cBOL TO cMBOL
	IF !USED('BL')
			csq1 = [select * from bl where mblnum = ']+cMBOL+[']
			xsqlexec(csq1,,,"wh")
			csq1 = [select bol_no from bl where mblnum = ']+cMBOL+[' group by bol_no]
			xsqlexec(csq1,"tempbl1",,"wh")
		SELECT tempbl1
		IF lTestinput
*		BROWSE
		ENDIF
		STORE RECCOUNT() TO nSubBillCnt
		LOCATE
		SCAN
			cSubBOLDetail = IIF(EMPTY(cSubBOLDetail),ALLTRIM(tempbl1.bol_no),cSubBOLDetail+CHR(13)+ALLTRIM(tempbl1.bol_no))
		ENDSCAN
		USE IN tempbl1
		SELECT bl
		LOCATE
	ENDIF

*!* Mail sending for all MBOL 945s
*	ASSERT .F. MESSAGE "At creation of MBOL notice...>>DEBUG<<"
	tsubject2 = "NOTICE: TKO 945 MBOL Process"
	SELECT 0
	USE F:\3pl\DATA\mailmaster ALIAS mm SHARED
	LOCATE FOR mm.edi_type = "MISC" AND mm.taskname = "GENERAL"
	tsendto2 = IIF(mm.use_alt,mm.sendtoalt,mm.sendto)
	tcc2 = IIF(mm.use_alt,mm.ccalt,mm.cc)
	USE IN mm

	tattach2 = " "
	tmessage2 = "Potential files for TKO MBOL# "+cMBOL+"...monitor output for correct number of files."
	tmessage2 = tmessage2 + CHR(13) + "Expected number of 945 files: "+ALLTRIM(STR(nSubBillCnt))
	tmessage2 = tmessage2 + CHR(13) + "Processed at "+TTOC(DATETIME())
	DO FORM m:\dev\frm\dartmail2 WITH tsendto2,tfrom,tsubject2,tcc2,tattach2,tmessage2,"A"
*!* End Mailing section

&& Added code blocks to the following to overcome BOL vs. WO selection issues, 12.23.2005
*	ASSERT .F. MESSAGE "At creation of SQL data"
	IF lDoSQL
		cSQL="SQL3"
		SELECT outship
		LOCATE
		IF USED("sqlwo")
			USE IN sqlwo
		ENDIF
		IF FILE("F:\3pl\DATA\sqlwo.dbf")
			DELETE FILE "F:\3pl\DATA\sqlwo.dbf"
		ENDIF
*		ASSERT .F. MESSAGE "At BOL-WO table creation...debug"
		SELECT bol_no,wo_num ;
			FROM outship ;
			WHERE bol_no == cBOL ;
			AND INLIST(accountid,&gtkoaccounts) ;
			GROUP BY 1,2 ;
			ORDER BY 1,2 ;
			INTO DBF F:\3pl\DATA\sqlwo
		IF lTesting OR DATETIME()<DATETIME(2010,07,12,18,30,00)
			BROWSE TIMEOUT 120
		ENDIF
		USE IN sqlwo
		SELECT 0
		USE F:\3pl\DATA\sqlwo ALIAS sqlwo
		LOCATE
		IF EOF()
			cErrMsg = "NO SQL DATA"
			SET STEP ON 
			DO ediupdate WITH cErrMsg,.T.
			THROW
		ENDIF
		cRetMsg = ""

		DO m:\dev\prg\sqlconnect_bol  WITH nAcctNum,ccustname,cPPName,.T.,cOffice,.T.
		IF cRetMsg<>"OK"
			STORE cRetMsg TO cErrMsg
			DO ediupdate WITH cErrMsg,.T.
			THROW
		ENDIF
		SELECT vtkopp
		LOCATE
		IF EOF()
			WAIT WINDOW "SQL select data is EMPTY...error!" NOWAIT
			cErrMsg = "SQL DATA EMPTY"
			DO ediupdate WITH cErrMsg,.T.
			THROW
		ENDIF
		REPLACE vtkopp.ship_ref WITH LEFT(ship_ref,11) FOR OCCURS("-",ship_ref)>1
		IF lTesting
			LOCATE
*			BROWSE
		ENDIF
	ENDIF

	SELECT outship
	LOCATE
	WAIT CLEAR
	WAIT WINDOW "Now creating Header information" NOWAIT NOCLEAR

*!* HEADER LEVEL EDI DATA
	DO num_incr_isa

	cISA_Num = PADL(c_CntrlNum,9,"0")
	nISA_Num = INT(VAL(cISA_Num))

	IF lTesting
		cISACode = "T"
	ELSE
		cISACode = "P"
	ENDIF

	STORE "ISA"+cfd+"00"+cfd+"          "+cfd+"00"+cfd+"          "+cfd+csendqual+cfd+csendidlong+cfd+;
		crecqual+cfd+crecidlong+cfd+cTruncDate+cfd+cTruncTime+cfd+"U"+cfd+"00401"+cfd+;
		cISA_Num+cfd+"0"+cfd+cISACode+cfd+cterminator+csegd TO cString
	DO cstringbreak

	STORE "GS"+cfd+"SW"+cfd+csendid+cfd+crecid+cfd+cdate+cfd+cTruncTime+cfd+c_CntrlNum+;
		cfd+"X"+cfd+cX12+csegd TO cString
	DO cstringbreak

	SELECT outship
	LOCATE

	IF !lTesting
		IF EMPTY(cBOL)  && May be empty if triggered from PT screen, or FedEx shipment
			WAIT CLEAR
			WAIT WINDOW "MISSING Bill of Lading Number!" TIMEOUT 2
			cErrMsg = "BOL# EMPTY"
			DO ediupdate WITH cErrMsg,.T.
			THROW
		ELSE
			IF !SEEK(PADR(TRIM(cBOL),20),"outship","bol_no")
				WAIT CLEAR
				WAIT WINDOW "Invalid Bill of Lading Number - Not Found!" TIMEOUT 2
				cErrMsg = "BOL# NOT FOUND"
				DO ediupdate WITH cErrMsg,.T.
				THROW
			ENDIF
		ENDIF
	ENDIF

*************************************************************************
*1	PTIDSOURCE/OUTSHIP RECORD MAIN LOOP
*	Loops through individual OUTSHIP lines for BOL#
*************************************************************************
	SELECT outship
	SET STEP ON
	SET ORDER TO
	COUNT TO N FOR outship.bol_no = cBOL AND !EMPTYnul(del_date)
	IF N=0
		cErrMsg = "INCOMP BOL"
		DO ediupdate WITH cErrMsg,.T.
		THROW
	ENDIF
	LOCATE

	cMissDel = ""

	WAIT CLEAR
	SELECT outship
	SET ORDER TO
	LOCATE FOR outship.bol_no = cBOL

	WAIT WINDOW "Now creating Master BOL#-based information..." NOWAIT NOCLEAR
	SELECT bl
	LOCATE
	cMissDel = ""
	bolscanstr = "bl.mblnum = cMBOL"
	nBLID = 0
	nFileID = 0
	nPTCount = 0
	nTcnt = 0

*	ASSERT .F. MESSAGE "At MBOL scan"
	LOCATE FOR &bolscanstr
	SCAN FOR &bolscanstr
		STORE TRIM(bl.bol_no) TO cBOL
*!*			IF cMBOL = '04907315865822721' and blid # 512987
*!*			LOOP
*!*			endif


		nBLID = bl.blid
		xsqlexec("select * from bldet where mod='"+goffice+"' and blid="+TRANSFORM(nBLID),,,"wh")

		SELECT bldet
		SCAN FOR bldet.blid = nBLID AND INLIST(bldet.accountid,&gtkoaccounts)
			IF DATETIME()>DATETIME(2010,04,19,20,00,00)
*				ASSERT .F. MESSAGE "At BLDET scan"
			ENDIF
			cBLShip_ref = ALLTRIM(bldet.ship_ref)
			IF "OV"$cBLShip_ref
				LOOP
			ENDIF
*!*			IF blid = 512987 AND cBLShip_ref = 'SPS-1214655'
*!*			LOOP
*!*			endif
			lCombPT = .F.
			IF bldet.combinedpt = .F.
				lDoubleDash = .F.
				IF OCCURS("-",cBLShip_ref)>1 AND cBLShip_ref
					lDoubleDash = .T.
*					ASSERT .F. MESSAGE "In standard PT with multiple dashes"
					cBLShip_ref = LEFT(cBLShip_ref,11)
				ENDIF
				bldetscanstr = "outship.ship_ref = PADR(cBLShip_ref,20)"
			ELSE
				bldetscanstr = "outship.combpt = PADR(cBLShip_ref,20)"
				lCombPT = .T.
			ENDIF

			SELECT outship
			LOCATE FOR INLIST(accountid,&gtkoaccounts) AND &bldetscanstr
			IF !FOUND()
				ASSERT .F. MESSAGE "At no-find of Outbound PT#"
				cErrMsg = "MISS OBD PT "+cBLShip_ref
				DO ediupdate WITH cErrMsg,.T.
				THROW
			ENDIF

			LOCATE
			COUNT TO N FOR (!EMPTYnul(del_date) AND INLIST(accountid,&gtkoaccounts) AND &bldetscanstr)
			IF N=0
				cErrMsg = "INCOMP SUB-BOL"
				SET STEP ON
				ASSERT .F. MESSAGE cErrMsg+" "+cBOL
				SET STEP ON
				DO ediupdate WITH cErrMsg,.T.
				THROW
			ELSE
				IF lCombPT
					WAIT WINDOW "Number of Combined PTs: "+ALLTRIM(STR(N)) TIMEOUT 2
				ENDIF
			ENDIF

			LOCATE
*!* In outship
*			ASSERT .F. MESSAGE "At outship scan of BLDET PT/COMB PT...debug"
			SCAN FOR INLIST(accountid,&gtkoaccounts) AND &bldetscanstr
				cConsignee = UPPER(ALLTRIM(outship.consignee))
				lSears = IIF("SEARS"$cConsignee OR "KMART"$cConsignee OR "K-MART"$cConsignee,.T.,.F.)
				lAAFES = IIF("AAFES"$cConsignee OR "ARMED FORCES"$cConsignee,.T.,.F.)
				alength = ALINES(apt,outship.shipins,.T.,CHR(13))
				SCATTER MEMVAR MEMO
				lSolidGroup = IIF("PACKGROUP*SOLIDPACK"$m.shipins,.T.,.F.)
				IF "PENNEY"$UPPER(m.consignee) OR UPPER(m.consignee)="JCP"
					lJCP = .T.
				ENDIF
				nOutshipid = m.outshipid
				nWO_num = m.wo_num
				cWO_Num = ALLTRIM(STR(m.wo_num))
				cShip_ref = ALLTRIM(m.ship_ref)
				cPackType = ALLTRIM(segmentget(@apt,"PROCESSMODE",alength))
				lPrepack = IIF(cPackType#"PICKPACK",.T.,.F.)

				cTrackNum = ALLT(m.keyrec)  && ProNum if available
				cCharge = "0.00"
				IF !(cWO_Num$cWO_NumStr)
					cWO_NumStr = IIF(EMPTY(cWO_NumStr),cWO_Num,cWO_NumStr+","+cWO_Num)
				ENDIF

				IF !(cWO_Num$cWO_NumList)
					cWO_NumList = IIF(EMPTY(cWO_NumList),cWO_Num,cWO_NumList+CHR(13)+cWO_Num)
				ENDIF

				nTotCtnCount = m.qty
				cPO_Num = ALLTRIM(m.cnee_ref)
				nPTCount = nPTCount+1
				lSolidpack = IIF("PACK940*SOLIDPACK"$outship.shipins,.T.,.F.)

				IF lOverflow
					SET DELETED OFF  && Enable this if MBOL and contains a split PT
				ENDIF
*!* Added this code to trap miscounts in OUTDET Units
				IF lDoTrapMiscounts
					IF lPrepack
						SELECT outdet
						SET ORDER TO
						IF INLIST(cOffice,"I","N") AND nAcctNum = 6705
							SUM totqty TO nCtnTot1 FOR units AND outdet.outshipid = outship.outshipid
						ELSE
							SUM totqty TO nCtnTot1 FOR !units AND outdet.outshipid = outship.outshipid
						ENDIF

*!*	Check carton count
						IF INLIST(cOffice,"I","N") AND nAcctNum = 6705
							SELECT SUM(totqty) AS cnt1 ;
								FROM vtkopp ;
								WHERE vtkopp.outshipid = outship.outshipid ;
								AND (vtkopp.ucc#"CUTS" AND vtkopp.totqty>0);
								INTO CURSOR tempsqlx
						ELSE
							SELECT COUNT(ucc) AS cnt1 ;
								FROM vtkopp ;
								WHERE vtkopp.outshipid = outship.outshipid ;
								AND (vtkopp.ucc#"CUTS" AND vtkopp.totqty>0);
								INTO CURSOR tempsqlx
						ENDIF
						STORE tempsqlx.cnt1 TO nCtnTot2
						USE IN tempsqlx
						IF nCtnTot1<>nCtnTot2
							ASSERT .F. MESSAGE "At SQL CTNQTY ERROR...DEBUG!"
							SET STEP ON
							cErrMsg = "SQL CTNQTY OSID: "+ALLT(STR(outship.outshipid))
							DO ediupdate WITH cErrMsg,.T.
							THROW
						ENDIF
					ELSE
						SELECT outdet
						SUM totqty TO nUnitTot1 FOR outdet.units=.T. AND outdet.outshipid = outship.outshipid
						SELECT vtkopp
						SUM totqty TO nUnitTot2 FOR vtkopp.outshipid = outship.outshipid AND INLIST(vtkopp.accountid,&gtkoaccounts)
						IF nUnitTot1<>nUnitTot2
							ASSERT .F. MESSAGE "At SQL UNITQTY ERROR...DEBUG!"
							SET STEP ON
							cErrMsg = "SQL UNITQTY OSID: "+ALLT(STR(outship.outshipid))
							DO ediupdate WITH cErrMsg,.T.
							THROW
						ENDIF
					ENDIF
				ENDIF

				SELECT outdet

				SET ORDER TO outdetid
				LOCATE

*!* End code addition

				IF (("PENNEY"$UPPER(outship.consignee)) OR ("KMART"$UPPER(outship.consignee)) OR ("K-MART"$UPPER(outship.consignee)))
					lApptFlag = .T.
				ELSE
					lApptFlag = .F.
				ENDIF

				IF lTestinput
					ddel_date = DATE()
					dapptnum = "99999"
					dapptdate = DATE()
				ELSE
					ddel_date = outship.del_date
					IF EMPTY(ddel_date)
						cMissDel = IIF(EMPTY(cMissDel),"The following PTs had no Delivery Dates:"+CHR(10)+TRIM(cShip_ref),cMissDel+CHR(10)+TRIM(cShip_ref))
					ENDIF
					dapptnum = ALLTRIM(outship.appt_num)

					IF EMPTY(dapptnum) && Penney/KMart Appt Number check
						IF !(lApptFlag)
							dapptnum = ""
						ELSE
							cErrMsg = "EMPTY APPT #"
							DO ediupdate WITH cErrMsg,.T.
							THROW
						ENDIF
					ENDIF
					dapptdate = outship.appt

					IF EMPTY(dapptdate)
						dapptdate = outship.del_date
					ENDIF
				ENDIF

				IF !(cWO_Num$cWO_NumStr)
					cWO_NumStr = IIF(EMPTY(cWO_NumStr),cWO_Num,cWO_NumStr+","+cWO_Num)
				ENDIF

				IF ALLTRIM(outship.SForCSZ) = ","
					BLANK FIELDS outship.SForCSZ NEXT 1 IN outship
				ENDIF

				cTRNum = ""
				cPRONum = ""

				IF (("WALMART"$outship.consignee) OR ("WAL-MART"$outship.consignee) OR ("WAL MART"$outship.consignee))
					IF LEFT(outship.keyrec,2) = "PR"
						STORE TRIM(keyrec) TO cPRONum
					ENDIF
					IF LEFT(outship.keyrec,2) = "TR"
						STORE TRIM(keyrec) TO cTRNum
					ENDIF
				ENDIF

				cPTString = IIF(EMPTY(cPTString),m.consignee+" "+cShip_ref,cPTString+CHR(13)+CHR(10)+m.consignee+" "+cShip_ref)

				m.CSZ = TRIM(m.CSZ)
				IF EMPTY(ALLT(STRTRAN(M.CSZ,",","")))
					WAIT CLEAR
					WAIT WINDOW "No SHIP-TO City/State/ZIP info...exiting" TIMEOUT 2
					cErrMsg = "NO CSZ INFO"
					DO ediupdate WITH cErrMsg,.T.
					THROW
				ENDIF
				IF !(", "$m.CSZ)
					m.CSZ = ALLTRIM(STRTRAN(m.CSZ,",",", "))
				ENDIF
				m.CSZ = ALLTRIM(STRTRAN(m.CSZ,"  "," "))
				cCity = ALLT(LEFT(TRIM(m.CSZ),AT(",",m.CSZ)-1))
				cStateZip = ALLT(SUBSTR(TRIM(m.CSZ),AT(",",m.CSZ)+1))
				cState = ALLT(LEFT(cStateZip,2))
				cZip = ALLT(SUBSTR(cStateZip,3))

				STORE "" TO cSForCity,cSForState,cSForZip
				IF !lFederated
					m.SForCSZ = ALLTRIM(STRTRAN(m.SForCSZ,"  "," "))
					nSpaces = OCCURS(" ",ALLTRIM(m.SForCSZ))
					IF nSpaces = 0
						m.SForCSZ = STRTRAN(m.SForCSZ,",","")
						cSForCity = ALLTRIM(m.SForCSZ)
						cSForState = ""
						cSForZip = ""
					ELSE
						nCommaPos = AT(",",m.SForCSZ)
						nLastSpace = AT(" ",m.SForCSZ,nSpaces)
						nMinusSpaces = IIF(nSpaces=1,0,1)
						IF ISALPHA(SUBSTR(TRIM(m.SForCSZ),AT(" ",TRIM(m.SForCSZ),nSpaces-nMinusSpaces)+1,2))
							cSForCity = LEFT(TRIM(m.SForCSZ),AT(",",m.SForCSZ)-1)
							cSForState = SUBSTR(m.SForCSZ,nCommaPos+2,2)
							cSForZip = TRIM(RIGHT(TRIM(m.SForCSZ),5))
							IF ISALPHA(cSForZip)
								cSForZip = ""
							ENDIF
						ELSE
							WAIT CLEAR
							WAIT WINDOW "NOT ALPHA: "+SUBSTR(TRIM(m.SForCSZ),AT(" ",m.SForCSZ,nSpaces-1)+1,2) TIMEOUT 3
							cSForCity = LEFT(TRIM(m.SForCSZ),AT(",",m.SForCSZ)-1)
							cSForState = SUBSTR(TRIM(m.SForCSZ),AT(" ",m.SForCSZ,nSpaces-2)+1,2)
							cSForZip = TRIM(RIGHT(TRIM(m.SForCSZ),5))
							IF ISALPHA(cSForZip)
								cSForZip = ""
							ENDIF
						ENDIF
					ENDIF
				ELSE
					cStoreName = segmentget(@apt,"STORENAME",alength)
				ENDIF

				DO num_incr_st
				WAIT CLEAR
				WAIT WINDOW "Now creating Line Item information" NOWAIT NOCLEAR

				INSERT INTO temp945 (accountid,isa_num,st_num,wo_num,bol_no,ship_ref,filename,filedate) ;
					VALUES (m.accountid,c_CntrlNum,c_GrpCntrlNum,m.wo_num,m.bol_no,m.ship_ref,cFilenameShort,dt2)

				STORE "ST"+cfd+"945"+cfd+PADL(c_GrpCntrlNum,9,"0")+csegd TO cString
				DO cstringbreak
				nSTCount = nSTCount + 1
				nSegCtr = 1

				cShip_refW06 = STRTRAN(ALLTRIM(cShip_ref),"-DS","")
				STORE "W06"+cfd+"N"+cfd+cShip_refW06+cfd+cdate+cfd+TRIM(cBOL)+cfd+TRIM(cWO_Num)+cfd+cPO_Num+csegd TO cString
				DO cstringbreak
				nSegCtr = nSegCtr + 1

				nCtnNumber = 1  && Seed carton sequence count
				cStoreNum = segmentget(@apt,"GLN",alength)
				IF EMPTY(cStoreNum)
					cStoreNum = segmentget(@apt,"STORENUM",alength)
				ENDIF

				IF EMPTY(cStoreNum)
					cStoreNum = m.dcnum
					IF EMPTY(cStoreNum)
						STORE "N1"+cfd+"CN"+cfd+ALLTRIM(m.consignee)+csegd TO cString
					ENDIF
				ELSE
					STORE "N1"+cfd+"CN"+cfd+ALLTRIM(m.consignee)+cfd+"92"+cfd+cStoreNum+csegd TO cString
				ENDIF
				DO cstringbreak
				nSegCtr = nSegCtr + 1

				cCountry = segmentget(@apt,"COUNTRY",alength)
				cCountry = ALLT(cCountry)
				IF EMPTY(cCountry)
					cCountry = "USA"
				ENDIF

				STORE "N3"+cfd+TRIM(outship.address)+csegd TO cString
				DO cstringbreak
				nSegCtr = nSegCtr + 1

				STORE "N4"+cfd+cCity+cfd+cState+cfd+cZip+cfd+cCountry+csegd TO cString
				DO cstringbreak
				nSegCtr = nSegCtr + 1

				cSFStoreNum = segmentget(@apt,"SFSTORENUM",alength)
				IF EMPTY(cSFStoreNum)
					cSFStoreNum = ALLTRIM(m.sforstore)
				ENDIF
				IF !EMPTY(m.shipfor)
					IF EMPTY(ALLTRIM(m.sforstore))
						STORE "N1"+cfd+"Z7"+cfd+ALLTRIM(m.shipfor)+csegd TO cString
					ELSE
						STORE "N1"+cfd+"Z7"+cfd+ALLTRIM(m.shipfor)+cfd+"92"+cfd+cSFStoreNum+csegd TO cString
					ENDIF
					DO cstringbreak
					nSegCtr = nSegCtr + 1

					STORE "N3"+cfd+TRIM(m.sforaddr1)+csegd TO cString
					DO cstringbreak
					nSegCtr = nSegCtr + 1

					IF !EMPTY(cSForState)
						STORE "N4"+cfd+cSForCity+cfd+cSForState+cfd+cSForZip+cfd+"USA"+csegd TO cString
					ELSE
						STORE "N4"+cfd+cSForCity+csegd TO cString
					ENDIF
					DO cstringbreak
					nSegCtr = nSegCtr + 1
				ENDIF

				cContact = ALLTRIM(segmentget(@apt,"CONTACT",alength))
				IF !EMPTY(cContact)
					STORE "PER"+cfd+"ST"+cfd+cContact+csegd TO cString
					DO cstringbreak
					nSegCtr = nSegCtr + 1
				ENDIF

				cCompany1 = ALLTRIM(segmentget(@apt,"COMPANY",alength))
				IF !EMPTY(cCompany1)
					STORE "N9"+cfd+"CO"+cfd+cCompany1+csegd TO cString
					DO cstringbreak
					nSegCtr = nSegCtr + 1
				ENDIF

				STORE "N1"+cfd+"BT"+cfd+ALLTRIM(m.consignee)+cfd+"92"+cfd+cStoreNum+csegd TO cString
				DO cstringbreak
				nSegCtr = nSegCtr + 1

				cCountry = segmentget(@apt,"COUNTRY",alength)
				cCountry = ALLT(cCountry)
				IF EMPTY(cCountry)
					cCountry = "USA"
				ENDIF

				STORE "N3"+cfd+TRIM(outship.address)+csegd TO cString
				DO cstringbreak
				nSegCtr = nSegCtr + 1

				STORE "N4"+cfd+cCity+cfd+cState+cfd+cZip+cfd+cCountry+csegd TO cString
				DO cstringbreak
				nSegCtr = nSegCtr + 1

				STORE "N9"+cfd+"MB"+cfd+cMBOL+csegd TO cString
				DO cstringbreak
				nSegCtr = nSegCtr + 1

				cDP = ALLTRIM(outship.dept)
				IF !EMPTY(cDP)
					STORE "N9"+cfd+"DP"+cfd+cDP+csegd TO cString
					DO cstringbreak
					nSegCtr = nSegCtr + 1
				ENDIF

				cIA = ALLTRIM(outship.vendor_num)
				IF !EMPTY(cIA)
					STORE "N9"+cfd+"IA"+cfd+cIA+csegd TO cString
					DO cstringbreak
					nSegCtr = nSegCtr + 1
				ENDIF

				cMR = ALLTRIM(segmentget(@apt,"MR",alength))
				IF !EMPTY(cMR)
					STORE "N9"+cfd+"MR"+cfd+cMR+csegd TO cString
					DO cstringbreak
					nSegCtr = nSegCtr + 1
				ENDIF

				IF !EMPTY(dapptnum) && AND lJCP * Changed to all shipments, per Maria E, 05.17.2016
					STORE "N9"+cfd+"AO"+cfd+dapptnum+csegd TO cString
					DO cstringbreak
					nSegCtr = nSegCtr + 1
				ENDIF

				IF !EMPTY(ALLTRIM(cPRONum))
					STORE "N9"+cfd+"CN"+cfd+ALLTRIM(cPRONum)+csegd TO cString
					DO cstringbreak
					nSegCtr = nSegCtr + 1
				ELSE
					IF lAAFES
						STORE "N9"+cfd+"CN"+cfd+ALLTRIM(cBOL)+csegd TO cString
						DO cstringbreak
						nSegCtr = nSegCtr + 1
					ENDIF
				ENDIF

				STORE "N9"+cfd+"CT"+cfd+ALLTRIM(STR(m.ctnqty))+csegd TO cString
				DO cstringbreak
				nSegCtr = nSegCtr + 1

				nTotWt = IIF(m.weight>0,m.weight,m.ctnqty*3)
				STORE "N9"+cfd+"CTW"+cfd+ALLTRIM(STR(nTotWt))+csegd TO cString
				DO cstringbreak
				nSegCtr = nSegCtr + 1

				STORE "G62"+cfd+"11"+cfd+TRIM(DTOS(ddel_date))+cfd+"D"+cfd+cDelTime+csegd TO cString  && Ship date/time
				DO cstringbreak
				nSegCtr = nSegCtr + 1

				SELECT scacs
				IF lTesting AND (EMPTY(m.scac) OR EMPTY(m.ship_via))
					STORE "TEST" TO m.scac
					STORE "TEST SHIPPER" TO m.ship_via
				ELSE
					IF !EMPTY(TRIM(outship.scac))
						STORE outship.scac TO m.scac
						SELECT outship
					ELSE
						WAIT CLEAR
						cErrMsg = "MISSING SCAC"
						DO ediupdate WITH cErrMsg,.T.
						THROW
					ENDIF
				ENDIF

				SELECT outship

				IF !EMPTY(cTRNum)
					STORE "W27"+cfd+cCarrierType+cfd+TRIM(m.scac)+cfd+TRIM(m.ship_via)+REPLICATE(cfd,2)+"TL"+;
						REPLICATE(cfd,2)+cTRNum+csegd TO cString
				ELSE
					STORE "W27"+cfd+cCarrierType+cfd+TRIM(m.scac)+cfd+TRIM(m.ship_via)+csegd TO cString
				ENDIF
				DO cstringbreak
				nSegCtr = nSegCtr + 1

*************************************************************************
*2	DETAIL LOOP - MATCHING SHIP_REF FIELD (FROM OUTSHIP)
*************************************************************************
				WAIT CLEAR
				WAIT WINDOW "Now creating Detail information "+cShip_ref NOWAIT NOCLEAR
				IF nOutshipid = 2503880
					ASSERT .F. MESSAGE "In detail creation...DEBUG"
				ENDIF
				SELECT outdet
				SET ORDER TO TAG outdetid
				SELECT vtkopp
				SET RELATION TO outdetid INTO outdet
				LOCATE
				LOCATE FOR vtkopp.ship_ref = TRIM(cShip_ref) AND vtkopp.outshipid = nOutshipid
				IF !FOUND()
					IF !lTesting
						ASSERT .F. MESSAGE "DEBUG HERE!!!!!"
						SET STEP ON 
						WAIT WINDOW "PT "+cShip_ref+" NOT FOUND in vtkopp...ABORTING" TIMEOUT 2
						IF !lTesting
							lSQLMail = .T.
						ENDIF
						DO ediupdate WITH "MISS PT-SQL: "+cShip_ref,.T.
						cErrMsg = "MISS PT-SQL: "+cShip_ref
						DO ediupdate WITH cErrMsg,.T.
						THROW
					ELSE
						LOOP
					ENDIF
				ENDIF

				SELECT ucc,SUM(totqty) AS sumtq ;
					FROM vtkopp ;
					GROUP BY 1 ;
					INTO CURSOR tempzerouccs READWRITE ;
					HAVING sumtq = 0
				INDEX ON ucc TAG ucc

				detscanstr = "vtkopp.ship_ref = cShip_ref and vtkopp.outshipid = nOutshipid"
				SELECT vtkopp
				LOCATE
				LOCATE FOR &detscanstr
				cUCC= "XXX"
				nZcnt = 0
				DO WHILE &detscanstr
					lSkipBack = .T.

					IF TRIM(vtkopp.ucc) <> cUCC
						STORE TRIM(vtkopp.ucc) TO cUCC
					ENDIF

					IF SEEK(cUCC,'tempzerouccs','ucc')
						nZcnt = nZcnt + 1
						SKIP 1 IN vtkopp
						LOOP
					ENDIF

					STORE "LX"+cfd+ALLTRIM(STR(nCtnNumber))+csegd TO cString   && Carton Seq. # (up to Carton total)
					lDoManSegment = .T.
					lDoPALSegment = .F.
					DO cstringbreak
					nSegCtr = nSegCtr + 1

					SELECT vtkopp
					LOCATE FOR vtkopp.ucc = cUCC
					DO WHILE vtkopp.ucc = cUCC
						IF vtkopp.totqty = 0
							SKIP 1 IN vtkopp
							LOOP
						ENDIF

						cDesc = ""
						SELECT outdet
						alength = ALINES(aptdet,outdet.printstuff,.T.,CHR(13))
*!* Prepack determination based on actual style info
*						ASSERT .F. MESSAGE "At lPrepack945 choice"
						lPrepack945 = .F.
						DO CASE
							CASE (("PACKGROUP*SOLIDPACK"$UPPER(outship.shipins)) OR ("PACKGROUP*CASEPACK"$UPPER(outship.shipins)))
								lPrepack945 = .T.
							CASE ("SINGLES"$UPPER(outship.shipins) OR "PIECES"$UPPER(outship.shipins))
								lPrepack945 = .F.
							CASE "PENNEY"$outship.consignee
								lPrepack945 = .T.
							CASE "SEARS"$outship.consignee
								lPrepack945 = .T.
							CASE outship.consignee = "SAM"
								lPrepack945 = .F.
							CASE "SOLIDPACK"$outdet.printstuff
								lPrepack945 = .F.
							CASE "PREPACK"$outdet.printstuff
								lPrepack945 =.T.
							CASE ("-"$outdet.STYLE AND INLIST(SUBSTR(outdet.STYLE,AT("-",outdet.STYLE,1)-3,1),"S","P"))
								lPrepack945 = .T.
							CASE ("/"$outdet.STYLE AND INLIST(SUBSTR(outdet.STYLE,AT("/",outdet.STYLE,1)-3,1),"S","P"))  && Added due to style discreps
								lPrepack945 = .T.
							CASE (INLIST(SUBSTR(ALLTRIM(outdet.STYLE),LEN(ALLTRIM(outdet.STYLE))-2,1),"S","P"))
								lPrepack945 = .T.
							OTHERWISE
								lPrepack945 = .F.
						ENDCASE
						cUCCNumber = vtkopp.ucc
						cUCCNumber = ALLTRIM(STRTRAN(TRIM(cUCCNumber)," ",""))

						IF lDoManSegment
							lDoManSegment = .F.
							STORE "MAN"+cfd+"GM"+cfd+TRIM(cUCCNumber)+csegd TO cString  && SCC-14 Number (Wal-mart spec)
							DO cstringbreak
							nSegCtr = nSegCtr + 1
							nShipDetQty = INT(VAL(outdet.PACK))
							nUnitSum = nUnitSum + nShipDetQty

							IF EMPTY(outdet.ctnwt) OR outdet.ctnwt = 0
								cCtnWt = ALLTRIM(STR(INT(outship.weight/outship.ctnqty)))
								IF lTesting
									cCtnWt = "3"
								ELSE
									IF EMPTY(cCtnWt) OR INT(VAL(cCtnWt)) = 0
										cErrMsg = "WEIGHT ERR: PT "+cShip_ref
										DO ediupdate WITH cErrMsg,.T.
										THROW
									ENDIF
								ENDIF
								nTotCtnWt = nTotCtnWt + INT(VAL(cCtnWt))
							ELSE
								STORE ALLTRIM(STR(outdet.ctnwt)) TO cCtnWt
								nTotCtnWt = nTotCtnWt + outdet.ctnwt
							ENDIF
						ENDIF

						lDoUPC = .T.
						cUPC = TRIM(segmentget(@aptdet,"UPC",alength))
						IF EMPTY(cUPC)
							cUPC = TRIM(outdet.upc)
							IF (ISNULL(cUPC) OR EMPTY(cUPC) OR VAL(cUPC) = 0)
								cUPC = TRIM(vtkopp.upc)
								IF (ISNULL(cUPC) OR EMPTY(cUPC) OR VAL(cUPC) = 0)
									lDoUPC = .F.
								ENDIF
							ENDIF
						ENDIF
						cItemNum = TRIM(outdet.custsku)
						cStyle = TRIM(outdet.STYLE)
						IF EMPTY(cStyle)
							ASSERT .F. MESSAGE "At empty style"
							WAIT WINDOW "Empty style in "+cShip_ref TIMEOUT 2
						ENDIF

						nShipDetQty = vtkopp.totqty
						IF ISNULL(nShipDetQty) OR EMPTY(nShipDetQty)
							nShipDetQty = outdet.totqty
						ENDIF

						nOrigDetQty = vtkopp.qty
						IF ISNULL(nOrigDetQty)
							nOrigDetQty = nShipDetQty
						ENDIF

*!* Changed the following to utilize original 940 unit codes from Printstuff field
						cUnitCode = TRIM(segmentget(@aptdet,"UNITCODE",alength))
						cUnitType = TRIM(segmentget(@aptdet,"UNITTYPE",alength))
						IF EMPTY(cUnitCode)
							cUnitCode = cUnitType
							IF EMPTY(cUnitCode)
								IF lPrepack AND !lSolidGroup
									cUnitCode = "CA"
								ELSE
									cUnitCode = "EA"
								ENDIF
							ENDIF
						ENDIF

						IF lPrepack AND lPrepack945 AND !lSolidGroup && Changed 01.13.2009 per Paul (was "1" for both); reverted 02.06.2009
							nOrigDetQty = 1
							STORE nOrigDetQty TO nShipDetQty
*							cUnitCode = "CS"
						ENDIF

						IF lSolidpack AND !lSolidGroup
*					nOrigdetqty = vtkopp.totqty*INT(VAL(outdet.PACK))
							nOrigDetQty = INT(VAL(outdet.PACK))
							STORE nOrigDetQty TO nShipDetQty
						ENDIF

						IF lSolidGroup  && NewPack
							nOrigDetQty = vtkopp.totqty
							STORE nOrigDetQty TO nShipDetQty
						ENDIF

						IF nOrigDetQty = nShipDetQty
							nShipStat = "CC"
						ELSE
							nShipStat = "CP"
						ENDIF


						IF lDoUPC
							STORE "W12"+cfd+nShipStat+cfd+ALLTRIM(STR(nOrigDetQty))+cfd+;
								ALLTRIM(STR(nShipDetQty))+cfd+ALLTRIM(STR(nOrigDetQty-nShipDetQty))+cfd+;
								cUnitCode+cfd+ALLTRIM(outdet.custsku)+cfd+"VN"+cfd+cUPC+csegd TO cString
						ELSE
							STORE "W12"+cfd+nShipStat+cfd+ALLTRIM(STR(nOrigDetQty))+cfd+;
								ALLTRIM(STR(nShipDetQty))+cfd+ALLTRIM(STR(nOrigDetQty-nShipDetQty))+cfd+;
								cUnitCode+cfd+ALLTRIM(outdet.custsku)+csegd TO cString
						ENDIF
						DO cstringbreak
						nSegCtr = nSegCtr + 1
						cDesc = ALLTRIM(segmentget(@aptdet,"MASTSTYLEDESC",alength))
						IF !EMPTY(cDesc)
							STORE "G69"+cfd+cDesc+csegd TO cString
							DO cstringbreak
							nSegCtr = nSegCtr + 1
						ENDIF

						cStyle = TRIM(outdet.STYLE)
						IF !EMPTY(cStyle)
							STORE "N9"+cfd+"ST"+cfd+cStyle+csegd TO cString
							DO cstringbreak
							nSegCtr = nSegCtr + 1
						ENDIF

						cColor = ""
						cColor = ALLTRIM(segmentget(@aptdet,"ORIGCOLOR",alength))
						cColor = IIF(EMPTY(cColor),ALLTRIM(outdet.COLOR),cColor)
						IF !EMPTY(cColor)
							STORE "N9"+cfd+"CL"+cfd+cColor+csegd TO cString
							DO cstringbreak
							nSegCtr = nSegCtr + 1
						ENDIF

						cSize = TRIM(outdet.ID)
						IF !EMPTY(cSize)
							STORE "N9"+cfd+"SZ"+cfd+cSize+csegd TO cString
							DO cstringbreak
							nSegCtr = nSegCtr + 1
						ENDIF

						cPkg = TRIM(segmentget(@aptdet,"PKG",alength))
						cPkgQty = TRIM(segmentget(@aptdet,"QTY",alength))
						IF !EMPTY(cPkg)
							STORE "N9"+cfd+"PKG"+cfd+cPkg+cfd+cPkgQty+csegd TO cString
							DO cstringbreak
							nSegCtr = nSegCtr + 1
						ENDIF

						SKIP 1 IN vtkopp
					ENDDO
					lSkipBack = .T.
					nCtnNumber = nCtnNumber + 1
				ENDDO
				nTcnt = nTcnt + nZcnt

*************************************************************************
*2^	END DETAIL MATCHING SHIP_REF LOOP
*************************************************************************
				WAIT CLEAR
				WAIT WINDOW "Now creating Section Closure information" NOWAIT NOCLEAR
				nPTCtnTot = outship.ctnqty
				STORE "W03"+cfd+ALLTRIM(STR(nPTCtnTot))+cfd+ALLTRIM(STR(nTotCtnWt))+cfd+;
					cWeightUnit+csegd TO cString   && Units sum, Weight sum, carton count

				nPTCtnTot = 0
				nTotCtnWt = 0
				nTotCtnCount = 0
				nUnitSum = 0
				FPUTS(nFilenum,cString)

				STORE  "SE"+cfd+ALLTRIM(STR(nSegCtr+2))+cfd+PADL(c_GrpCntrlNum,9,"0")+csegd TO cString
				FPUTS(nFilenum,cString)

				SELECT outship
				WAIT CLEAR
			ENDSCAN
			SELECT bldet
		ENDSCAN
		SELECT bl

	ENDSCAN
	WAIT WINDOW "Grand Total, skipped UCCs: "+TRANSFORM(nTcnt) TIMEOUT 1


*************************************************************************
*1^	END OUTSHIP MAIN LOOP
*************************************************************************

	DO close945
	=FCLOSE(nFilenum)
	lDoCatch = .F.
	IF !lTesting
		SELECT edi_trigger
		DO ediupdate WITH "945 CREATED",.F.
		SELECT edi_trigger
		LOCATE

		IF !USED("ftpedilog")
			SELECT 0
			USE F:\edirouting\ftpedilog ALIAS ftpedilog
			INSERT INTO ftpedilog (TRANSFER,ftpdate,filename,acct_name,TYPE) VALUES ("945-"+ccustname+"-"+cCustLoc,dt2,cFilenameHold,UPPER(ccustname),"945")
			USE IN ftpedilog
		ENDIF
	ENDIF

	WAIT CLEAR
	WAIT WINDOW cCustFolder+" 945 Process complete..." NOWAIT

*!* Create eMail confirmation message
	cOutFolder = "FMI"+cCustLoc

	tsubject = cMailName+" 945 EDI File from TGF as of "+dtmail+" ("+cCustLoc+")"
	tattach = " "
	tmessage = "945 EDI Info from TGF, for division "+cDivision+", MBOL# "+TRIM(cMBOL)+CHR(10)
	tmessage = tmessage + "Filename: "+cFilenameShort+CHR(13)
	tmessage = tmessage + "For Work Orders: "+cWO_NumStr+","+CHR(10)
	tmessage = tmessage + "containing these "+ALLTRIM(STR(nPTCount))+" picktickets:"+CHR(10)+CHR(13)
	tmessage = tmessage + cPTString + CHR(10)+CHR(13)
	tmessage = tmessage +"has been created and will be transmitted ASAP."+CHR(10)+CHR(10)
	IF !EMPTY(cMissDel)
		tmessage = tmessage+CHR(10)+CHR(10)+cMissDel+CHR(10)+CHR(10)
	ENDIF

	IF nTcnt > 0
		tmessage = tmessage+CHR(13)+"*** There were "+TRANSFORM(nTcnt)+" zero qty cartons cut from this MBOL. ***"+CHR(13)
	ENDIF

	IF lTesting OR lTestinput
		tmessage = tmessage + "This is a TEST 945"
	ELSE
		tmessage = tmessage + "If you have any questions, please eMail or call 732-750-9000 x217."
	ENDIF

	IF lEmail
		DO FORM m:\dev\frm\dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
	ENDIF

	cFilenameArchive = UPPER("f:\ftpusers\"+cCustFolder+"\945OUT\ARCHIVE\"+cCustPrefix+dt1+".txt")

	RELEASE ALL LIKE c_CntrlNum,c_GrpCntrlNum
	WAIT CLEAR
	WAIT WINDOW cMailName+" 945 EDI File output complete" TIMEOUT 1

*!* Transfers files to correct output folders
	COPY FILE [&cFilenameHold] TO [&cFilenameArch]
	IF lFilesout=.T. AND lTesting=.F.
		COPY FILE [&cFilenameHold] TO [&cFilenameOut]
		DELETE FILE &cFilenameHold
		SELECT temp945
		COPY TO "f:\3pl\data\temp945a.dbf"
		USE IN temp945
		SELECT 0
		USE "f:\3pl\data\temp945a.dbf" ALIAS temp945a
		SELECT 0
		USE "f:\3pl\data\pts_sent945.dbf" ALIAS pts_sent945
		APPEND FROM "f:\3pl\data\temp945a.dbf"
		USE IN pts_sent945
		USE IN temp945a
		DELETE FILE "f:\3pl\data\temp945a.dbf"
	ENDIF

	*!* asn_out_data()()

CATCH TO oErr
	IF lDoCatch
		ASSERT .F. MESSAGE "In Error CATCH"
		SET STEP ON
		IF EMPTY(cErrMsg) AND EMPTY(cStatus)
			cErrMsg = "ERRHAND ERROR"
		ELSE
			cErrMsg = cStatus
		ENDIF
		DO ediupdate WITH cErrMsg,.T.
		tsubject = ccustname+" Error ("+TRANSFORM(oErr.ERRORNO)+") at "+TTOC(DATETIME())
		tattach  = ""
		IF !lTesting
			tsendto  = tsendtoerr
			tcc = tccerr
		ENDIF

		tmessage = ccustname+" Error processing "+CHR(13)

		tmessage =tmessage+ "Try/CATCH Exeception Message:"+CHR(13)+;
			[  Error: ] + STR(oErr.ERRORNO) +CHR(13)+;
			[  LineNo: ] + STR(oErr.LINENO) +CHR(13)+;
			[  Message: ] + oErr.MESSAGE +CHR(13)+;
			[  Procedure: ] + oErr.PROCEDURE +CHR(13)+;
			[  Details: ] + oErr.DETAILS +CHR(13)+;
			[  StackLevel: ] + STR(oErr.STACKLEVEL) +CHR(13)+;
			[  LineContents: ] + oErr.LINECONTENTS+CHR(13)+;
			[  UserValue: ] + oErr.USERVALUE
		tmessage =tmessage+CHR(13)+cProgname

		tsubject = "945 EDI Poller Error at "+TTOC(DATETIME())
		tattach  = ""
		tcc=""
		tfrom    ="TGF EDI 945 Poller Operations <transload-ops@tollgroup.com>"
		DO FORM m:\dev\frm\dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
		FCLOSE(nFilenum)
	ENDIF

FINALLY
	IF USED('OUTSHIP')
		USE IN outship
	ENDIF
	IF USED('OUTDET')
		USE IN outdet
	ENDIF
	IF USED('SERFILE')
		USE IN serfile
	ENDIF
	WAIT CLEAR

ENDTRY

*** END OF CODE BODY

****************************
PROCEDURE close945
****************************
** Footer Creation

	STORE  "GE"+cfd+ALLTRIM(STR(nSTCount))+cfd+c_CntrlNum+csegd TO cString
	FPUTS(nFilenum,cString)

	STORE  "IEA"+cfd+"1"+cfd+PADL(c_CntrlNum,9,"0")+csegd TO cString
	FPUTS(nFilenum,cString)

	RETURN

****************************
PROCEDURE num_incr_isa
****************************

	IF !USED("serfile")
		USE ("f:\3pl\data\serial\"+ccustname+"945_serial") IN 0 ALIAS serfile
	ENDIF
	SELECT serfile
	IF lTesting AND lISAFlag
		nOrigSeq = serfile.seqnum
		lISAFlag = .F.
	ENDIF
	nISA_Num = serfile.seqnum
	c_CntrlNum = ALLTRIM(STR(serfile.seqnum))
	REPLACE serfile.seqnum WITH serfile.seqnum + 1 IN serfile
	SELECT outship
	RETURN

****************************
PROCEDURE num_incr_st
****************************

	IF !USED("serfile")
		USE ("f:\3pl\data\serial\"+ccustname+""945_serial") IN 0 ALIAS serfile
	ENDIF
	SELECT serfile
	IF lTesting AND lSTFlag
		nOrigGrpSeq = serfile.grpseqnum
		lSTFlag = .F.
	ENDIF
	c_GrpCntrlNum = ALLTRIM(STR(serfile.grpseqnum))
	REPLACE serfile.grpseqnum WITH serfile.grpseqnum + 1 IN serfile
	SELECT outship
	RETURN


****************************
PROCEDURE segmentget
****************************

	PARAMETER thisarray,lcKey,nLength

	FOR i = 1 TO nLength
		IF i > nLength
			EXIT
		ENDIF
		lnEnd= AT("*",thisarray[i])
		IF lnEnd > 0
			lcThisKey =TRIM(SUBSTR(thisarray[i],1,lnEnd-1))
			IF OCCURS(lcKey,lcThisKey)>0
				RETURN SUBSTR(thisarray[i],lnEnd+1)
				i = 1
			ENDIF
		ENDIF
	ENDFOR

	RETURN ""

****************************
PROCEDURE ediupdate
****************************
	PARAMETER cStatus,lIsError
	lDoCatch = lIsError

	IF !lTesting
		SELECT edi_trigger
		nRec = RECNO()
		IF !lIsError
			REPLACE processed WITH .T.,proc945 WITH .T.,file945 WITH cFilenameHold,isa_num WITH cISA_Num,;
				fin_status WITH "945 CREATED",errorflag WITH .F.,when_proc WITH DATETIME(),masterbill WITH .T. ;
				FOR edi_trigger.bol = cMBOL AND INLIST(accountid,&gtkoaccounts)
		ELSE
			REPLACE processed WITH .T.,proc945 WITH .F.,file945 WITH "",;
				fin_status WITH cStatus,errorflag WITH .T. ;
				FOR edi_trigger.bol = cMBOL AND INLIST(accountid,&gtkoaccounts)

			IF lCloseOutput
				=FCLOSE(nFilenum)
				ERASE &cFilenameHold
			ENDIF
			num_decrement()
		ENDIF
	ENDIF

	IF lIsError AND lEmail AND cStatus<>"SQL ERROR"
		tsubject = "945 Error in "+cMailName+" BOL "+TRIM(cBOL)+"(At PT "+cShip_ref+")"
		tattach = " "
		tsendto = tsendtoerr
		tcc = tccerr
		tmessage = "945 Processing for BOL# "+cBOL+", WO# "+cWO_Num+" produced this error: (Office: "+cOffice+"): "+cStatus+CHR(10)+"Check EDI_TRIGGER and re-run"
		IF "TOTQTY ERR"$cStatus
			tmessage = tmessage + CHR(10) + "At OUTSHIPID: "+ALLTRIM(STR(m.outshipid))
		ENDIF
		DO FORM m:\dev\frm\dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
	ENDIF
	closedata()
ENDPROC

*********************
PROCEDURE closedata
*********************

	IF USED('outship')
		USE IN outship
	ENDIF
	IF USED('outdet')
		USE IN outdet
	ENDIF

	IF USED('serfile')
		SELECT serfile
		IF lTesting
			REPLACE serfile.seqnum WITH nOrigSeq
			REPLACE serfile.grpseqnum WITH nOrigGrpSeq
		ENDIF
		USE IN serfile
	ENDIF
	IF USED('scacs')
		USE IN scacs
	ENDIF
	IF USED('mm')
		USE IN mm
	ENDIF
	IF USED('tempx')
		USE IN tempx
	ENDIF
	IF !lTesting
		SELECT edi_trigger
		LOCATE
	ENDIF

ENDPROC

****************************
PROCEDURE cstringbreak
****************************
	cLen = LEN(ALLTRIM(cString))
	IF ISNULL(cString)
		ASSERT .F. MESSAGE "At cStringBreak procedure"
	ENDIF
	FPUTS(nFilenum,cString)
ENDPROC


****************************
PROCEDURE cszbreak
****************************
	cCSZ = ALLT(m.CSZ)

	FOR ii = 5 TO 2 STEP -1
		cCSZ = STRTRAN(cCSZ,SPACE(ii),SPACE(1))
	ENDFOR
	cCSZ = STRTRAN(cCSZ,",","")
	len1 = LEN(ALLT(cCSZ))
	nSpaces = OCCURS(" ",cCSZ)

	IF nSpaces<2
		DO ediupdate WITH "BAD CSZ INFO",.T.
		cErrMsg = "BAD CSZ INFO"
		DO ediupdate WITH cErrMsg,.T.
		THROW
	ENDIF

*	ASSERT .F. MESSAGE "In CSZ Breakout"
	FOR ii = nSpaces TO 1 STEP -1
		ii1 = ALLT(STR(ii))
		DO CASE
			CASE ii = nSpaces
				nPOS = AT(" ",cCSZ,ii)
				cZip = ALLT(SUBSTR(cCSZ,nPOS))
*				WAIT WINDOW "ZIP: "+cZip TIMEOUT 1
				nEndState = nPOS
			CASE ii = (nSpaces - 1)
				nPOS = AT(" ",cCSZ,ii)
				cState = ALLT(SUBSTR(cCSZ,nPOS,nEndState-nPOS))
*				WAIT WINDOW "STATE: "+cState TIMEOUT 1
				IF nSpaces = 2
					cCity = ALLT(LEFT(cCSZ,nPOS))
*					WAIT WINDOW "CITY: "+cCity TIMEOUT 1
					EXIT
				ENDIF
			OTHERWISE
				nPOS = AT(" ",cCSZ,ii)
				cCity = ALLT(LEFT(cCSZ,nPOS))
*				WAIT WINDOW "CITY: "+cCity TIMEOUT 2
		ENDCASE
	ENDFOR
ENDPROC

****************************
PROCEDURE num_decrement
****************************
*!* This procedure decrements the ISA/GS numbers in the counter
*!* in the event of failure and deletion of the current 945
	IF lCloseOutput
		IF !USED("serfile")
			USE ("f:\3pl\data\serial\"+ccustname+"945_serial") IN 0 ALIAS serfile
		ENDIF
		SELECT serfile
		REPLACE serfile.seqnum WITH serfile.seqnum - 1 IN serfile  && ISA number
		REPLACE serfile.grpseqnum WITH serfile.grpseqnum - 1 IN serfile  && GS number
		RETURN
	ENDIF
ENDPROC