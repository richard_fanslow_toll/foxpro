*!* m:\dev\prg\s3_940_process.prg

CD &lcPath

LogCommentStr = ""

ll940test = .t.

If ll940test
  xpath = "F:\FTPUSERS\bbcrw\940INTest\"
  lnNum = Adir(tarray,xpath+"*.*")
Else
  lnNum = Adir(tarray,lcPath+"*.*")
Endif




lnNum = ADIR(tarray,"*.*")

IF lnNum = 0
	WAIT WINDOW "      No Steel Series 940s in folder "+lcPath+ "  to import     " TIMEOUT 3
	NormalExit = .T.
	THROW
ENDIF

FOR thisfile = 1  TO lnNum
	STORE "" TO xfile,cFilename
	STORE 0 TO nFileSize
	xfile = lcPath+tarray[thisfile,1] && +"."
	nFileSize = VAL(TRANSFORM(tarray(thisfile,2)))
	cFilename = ALLTRIM(LOWER(tarray[thisfile,1]))
	dFileDate = tarray[thisfile,3]

	archivefile  = (lcArchivepath+cFilename)
	WAIT WINDOW "Importing file: "+cFilename NOWAIT

	DO m:\dev\prg\createx856a
	SELECT x856
	DO m:\dev\prg\loadedifile WITH xfile,"*","NOTILDE",cCustname

	IF lTesting
		SELECT x856
		LOCATE
*		BROW
	ENDIF

	SELECT x856
	LOCATE FOR x856.segment = 'GS'
	IF x856.f1 = "FA"
		WAIT WINDOW "This is a 997 FA file...moving to correct folder" TIMEOUT 1
		cfile997in = ("F:\ftpusers\S3\997in\"+cFilename)
		COPY FILE [&xfile] TO [&cfile997in]
		DELETE FILE [&xfile]
		LOOP
	ELSE
		LOCATE
	ENDIF

	DO create_pt_cursors WITH cUseFolder,lLoadSQL && Added to prep for SQL Server transition

	lOK = .T.
	DO ("m:\dev\prg\s3_940_bkdn")
	IF lOK
		SELECT xpt
		GO TOP
		DO m:\dev\prg\all940_import
	ENDIF

ENDFOR

&& now clean up the 940in archive files, delete for the upto the last 20 days
IF !lTesting
deletefile(lcArchivepath,20)
ENDIF

WAIT CLEAR
WAIT "Entire "+cMailname+" 940 Process Now Complete" WINDOW TIMEOUT 3

**************************
PROCEDURE remove_foreign
**************************
	PARAMETERS cForeignStr
	IF lTesting
*		SET STEP ON
	ENDIF
	cForeignStr = STRTRAN(cForeignStr,CHR(195)+CHR(161),"a") && � as ó
	cForeignStr = STRTRAN(cForeignStr,CHR(195)+CHR(163),"a") && � as ã
	cForeignStr = STRTRAN(cForeignStr,CHR(195)+CHR(168),"e") && � as è
	cForeignStr = STRTRAN(cForeignStr,CHR(195)+CHR(169),"e") && � as é
	cForeignStr = STRTRAN(cForeignStr,CHR(195)+CHR(171),"e") && � as ë
	cForeignStr = STRTRAN(cForeignStr,CHR(195)+CHR(173),"i") && i as í
	cForeignStr = STRTRAN(cForeignStr,CHR(195)+CHR(175),"i") && � as ë
	cForeignStr = STRTRAN(cForeignStr,CHR(195)+CHR(179),"o") && � as á
	cForeignStr = STRTRAN(cForeignStr,CHR(195)+CHR(180),"o") && � as ô
	cForeignStr = STRTRAN(cForeignStr,CHR(195)+CHR(185),"u") && � as ù
	cForeignStr = STRTRAN(cForeignStr,CHR(195)+CHR(167),"s") && � as ç
	cForeignStr = STRTRAN(cForeignStr,CHR(194)+CHR(186),"o") && o as º
	cForeignStr = STRTRAN(cForeignStr,CHR(195)+CHR(137),"e") && o as º
	cForeignStr = STRTRAN(cForeignStr,CHR(196)+CHR(159),"g") && g as º
	cForeignStr = STRTRAN(cForeignStr,CHR(195)+CHR(182),"oe") && � as º
	cForeignStr = STRTRAN(cForeignStr,CHR(195)+CHR(170),"ei") && � as ª
	cForeignStr = STRTRAN(cForeignStr,CHR(195)+CHR(186),"u") && � as ú
	cForeignStr = STRTRAN(cForeignStr,CHR(195)+CHR(177),"n") && � as ñ
	cForeignStr = STRTRAN(cForeignStr,CHR(195)+CHR(129),"a") && � as Á
	cForeignStr = STRTRAN(cForeignStr,CHR(195)+CHR(145),"n") && � as Ñ
	cForeignStr = STRTRAN(cForeignStr,CHR(195)+CHR(131),"a") && � as Ã
	cForeignStr = STRTRAN(cForeignStr,CHR(194)+CHR(176),"o") && � as º
	cForeignStr = STRTRAN(cForeignStr,CHR(195)+CHR(184),"ae") && � as ø
	cForeignStr = STRTRAN(cForeignStr,"h`","h") && Typo, came in with a reverse apostrophe. Preventative check.
	cForeignStr = STRTRAN(cForeignStr,CHR(150),"-")
*	cForeignStr = UPPER(cForeignStr)
ENDPROC
