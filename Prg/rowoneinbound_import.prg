*!* This is a generic form import program for CR8REC transfer sheets, etc.

*SET STEP ON
IF lTesting
	lTestMail = .T.
ENDIF

SELECT xinwolog
SET ORDER TO inwologid
LOCATE
STORE RECCOUNT() TO lnCount
WAIT WINDOW AT 10,10 "Container lines to load ...........  "+STR(lnCount) NOWAIT TIMEOUT 1

nUploadcount = 0
cRecs = ALLTRIM(STR(lnCount))

*ASSERT .F. MESSAGE "At XINWOLOG Import loop...debug"
cCtrString = PADR("WO NUM",10)+PADR("CONTAINER",15)+PADR("CTNS",6)+"PRS"+CHR(13)

SCAN  && Currently in XINWOLOG table
	cContainer = ALLT(xinwolog.CONTAINER)
	cCtnQty = ALLT(STR(xinwolog.plinqty))
	WAIT "AT XINWOLOG RECORD "+ALLTRIM(STR(RECNO()))+" OF "+cRecs WINDOW NOWAIT NOCLEAR
	SELECT inwolog
	IF SEEK(cContainer,'inwolog','container')
		WAIT WINDOW "Container "+cContainer+" already exists in INWOLOG...looping" TIMEOUT 6
		RETURN
	ENDIF

	SELECT xinwolog
	SCATTER FIELDS EXCEPT inwologid MEMVAR MEMO

	SELECT inwolog
	LOCATE

	m.accountid = nAcctNum
	nWO_num = dygenpk("wonum",cWhseMod)
	m.wo_num = nWO_num
	cWO_Num = ALLT(STR(m.wo_num))

	m.mod= cMod
	m.office = cOffice
	insertinto("inwolog","wh",.T.)

	nUploadcount = nUploadcount + 1

	SELECT xpl
	SUM totqty TO nGTUnits FOR xpl.units = .T.
	cGTUnits = ALLTRIM(STR(nGTUnits))
	cCtrString = cCtrString+CHR(13)+PADR(cWO_Num,10)+PADR(cContainer,15)+PADR(cCtnQty,6)+cGTUnits  && ,cUnitQty)

	SCAN FOR inwologid = xinwolog.inwologid
		SCATTER MEMVAR MEMO
		m.inwologid=inwolog.inwologid
		m.wo_num = nWO_num
		m.accountid = inwolog.accountid
		m.office = cOffice
		m.mod = cMod
		insertinto("pl","wh",.t.)
	ENDSCAN
ENDSCAN

SELECT inwolog
SCATTER MEMVAR MEMO BLANK
SELECT pl
SCATTER MEMVAR MEMO BLANK

WAIT CLEAR

WAIT WINDOW AT 10,10 "Container records loaded...........  "+STR(nUploadcount) TIMEOUT 2

IF nUploadcount > 0

	tu("inwolog")
	
	IF !lTesting
		tu("pl")
	ENDIF

*	SET STEP ON
	tsubject= "Inbound WO(s) created for "+cCustname

	IF lTesting
		tsubject= tsubject+" (TEST) at "+TTOC(DATETIME())
	ELSE
		DO CASE
			CASE cOffice = "C"
				tsubject = tsubject+"(CA) at "+TTOC(DATETIME())
			CASE cOffice = "M"
				tsubject = tsubject+"(FL) at "+TTOC(DATETIME())
			CASE cOffice = "I"
				tsubject = tsubject+"(NJ) at "+TTOC(DATETIME())
			OTHERWISE
				tsubject = tsubject+"(UNK) at "+TTOC(DATETIME())
		ENDCASE
	ENDIF
	tattach = ""
	tmessage = cCtrString
	tmessage = tmessage + CHR(13) + CHR(13) + "From Row One inbd file: "+cFilename
	IF lTesting
		tmessage = tmessage + CHR(13) + "Data is in F:\WHP\WHDATA"
	ENDIF
	tFrom ="Toll WMS Operations <toll-edi-ops@tollgroup.com>"
	IF lDoMail
		DO FORM m:\dev\frm\dartmail2 WITH tsendto,tFrom,tsubject,tcc,tattach,tmessage,"A"
	ENDIF
	cCtrString = ""
	WAIT cCustname+" Inbound Import Process Complete for file "+cFilename WINDOW TIMEOUT 2
ENDIF

RETURN
