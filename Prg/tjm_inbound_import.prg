*!* This is a generic form import program for CR8REC transfer sheets, etc.

*SET STEP ON
IF lTesting
	lTestMail = .T.
ENDIF

SELECT xinwolog
SET ORDER TO inwologid
LOCATE
STORE RECCOUNT() TO lnCount
WAIT WINDOW AT 10,10 "Container lines to load ...........  "+STR(lnCount) NOWAIT TIMEOUT 1

nUploadcount = 0
cRecs = ALLTRIM(STR(lnCount))

*ASSERT .F. MESSAGE "At XINWOLOG Import loop...debug"
cCtrString = PADR("WO NUM",10)+PADR("CONTAINER",15)+PADR("CTNS",6)+"PRS"+CHR(13)

SCAN  && Currently in XINWOLOG table
	cContainer = ALLT(xinwolog.CONTAINER)
	cCtnQty = ALLT(STR(xinwolog.plinqty))
	WAIT "AT XINWOLOG RECORD "+ALLTRIM(STR(RECNO()))+" OF "+cRecs WINDOW NOWAIT NOCLEAR
xsqlexec("select * from inwolog where accountid = 6751 and container = '"+cContainer+"'","inwo2",,"wh")
	SELECT inwo2
	IF RECCOUNT()>0
		WAIT WINDOW "Container "+cContainer+" already exists in INWOLOG...looping" TIMEOUT 6
		close data all 
		RETURN
	ENDIF

	DO CASE
		CASE INLIST(cOffice,"N","I","J")
			lcWarehouse = "NJ1"
		CASE cOffice = "M"
			lcWarehouse = "FL1"
		OTHERWISE
			lcWarehouse = "LA1"
	ENDCASE

	SELECT xinwolog
	SCATTER MEMVAR MEMO

	m.accountid = nAcctNum
	nwo_num = dygenpk("wonum",cWhseMod)
	cWO_Num = ALLT(STR(m.wo_num))
	m.wo_num = nwo_num
	m.mod = cMod
	m.office = cOffice
	m.wo_date = DATE()

	insertinto("inwolog","wh",.T.)

	nUploadcount = nUploadcount + 1

	SELECT xpl
	SUM totqty TO nGTUnits FOR xpl.units = .T.
	cGTUnits = ALLTRIM(STR(nGTUnits))
	cCtrString = cCtrString+CHR(13)+PADR(cWO_Num,10)+PADR(cContainer,15)+PADR(cCtnQty,6)+cGTUnits  && ,cUnitQty)

	SCAN FOR inwologid = xinwolog.inwologid
		SCATTER MEMVAR MEMO
		m.office =  cOffice
		m.mod = cMod
		m.wo_num = nwo_num
		m.accountid =  nAcctNum
		m.date_rcvd =  DATE()
		m.inwologid = inwolog.inwologid
		m.plid = dygenpk("pl",cWhseMod)
		INSERT INTO pl FROM MEMVAR
	ENDSCAN
ENDSCAN

WAIT WINDOW AT 10,10 "Container records loaded...........  "+STR(nUploadcount) TIMEOUT 2
WAIT CLEAR

IF nUploadcount > 0

	tu("inwolog")
	tu("pl")

*	SET STEP ON
	tsubject= "Inbound WO(s) created for "+cCustname

	IF lTesting
		tsubject= tsubject+" (TEST) at "+TTOC(DATETIME())
	ELSE
		DO CASE
			CASE cOffice = "C"
				tsubject = tsubject+"(CA) at "+TTOC(DATETIME())
			CASE cOffice = "M"
				tsubject = tsubject+"(FL) at "+TTOC(DATETIME())
			CASE cOffice = "I"
				tsubject = tsubject+"(NJ) at "+TTOC(DATETIME())
			OTHERWISE
				tsubject = tsubject+"(UNK) at "+TTOC(DATETIME())
		ENDCASE
	ENDIF
	tattach = ""
	tmessage = cCtrString
	tmessage = tmessage + CHR(13) + CHR(13) + "From generic file: "+cFilename
	IF lTesting
		tmessage = tmessage + CHR(13) + "Data is in F:\WHP\WHDATA"
	ENDIF
	tFrom ="Toll WMS Operations <toll-edi-ops@tollgroup.com>"
	IF lDoMail
		DO FORM m:\dev\frm\dartmail2 WITH tsendto,tFrom,tsubject,tcc,tattach,tmessage,"A"
	ENDIF
	cCtrString = ""
	WAIT cCustname+" Inbound Import Process Complete for file "+cFilename WINDOW TIMEOUT 2
ENDIF

RETURN
