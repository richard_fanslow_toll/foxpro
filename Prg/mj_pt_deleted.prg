
utilsetup("MJ_PT_DELETED")
SET EXCLUSIVE OFF 
SET DELETED ON 
SET TALK OFF  
SET SAFETY OFF   &&Allows for overwrite of existing files

close databases all



xmonth=month(date())-1
xcmonth=cmonth(Date())
xyear=year(date())
IF xmonth=0
  xmonth=12
  xyear=year(date())-1
endif

xstartdt=ctod(transform(xmonth)+"/1/"+transform(xyear))
xenddt=gomonth(xstartdt,1)-1

SET STEP ON 

*goffice="J"

	

useca("ptbak","wh",,,"select * from ptbak where accountid in (6303,6325,6543)",,"ptbak")		
SELECT * FROM ptbak WHERE  inlist(accountid,6303,6325,6543) AND   Month(adddt)=xmonth AND year(adddt)=xyear INTO CURSOR t1 READWRITE 
SELECT mod,accountid,ptdate,consignee, ship_ref as pt, cnee_ref as po, qty,start, cancel, batch_num as div, DTOC(adddt) as date_deleted FROM t1 INTO CURSOR t2 READWRITE


	If Reccount() > 0 
		export TO "S:\MarcJacobsData\TEMP\mj_pt_deleted"  TYPE xls
		
		tsendto = "tmarg@fmiint.com,P.MAXEY@marcjacobs.com,o.batista@marcjacobs.com,A.Verilhac@marcjacobs.com,L.LOPEZ@marcjacobs.com,M.BUTAKIS@marcjacobs.com,E.SKRZYNIARZ@marcjacobs.com,K.Mageras@marcjacobs.com,S.SILVA@marcjacobs.com"
		tattach = "S:\MarcJacobsData\TEMP\mj_pt_deleted.xls" 
		tcc =""                                                                                                                               
		tFrom ="Toll EDI System Operations <fmi-transload-ops@fmiint.com>"                                
		tmessage = "MJ DELETED PT'S PREVIOUS MONTH:  "+Ttoc(Datetime())       
		tSubject = "MJ DELETED PT'S PREVIOUS MONTH:"                                                           
		Do Form m:\dev\frm\dartmail2 With tsendto,tFrom,tSubject, tcc,tattach,tmessage  ,"A"     
			
	ELSE 

		tsendto = "tmarg@fmiint.com,P.MAXEY@marcjacobs.com,o.batista@marcjacobs.com,A.Verilhac@marcjacobs.com,L.LOPEZ@marcjacobs.com,M.BUTAKIS@marcjacobs.com,E.SKRZYNIARZ@marcjacobs.com,K.Mageras@marcjacobs.com,S.SILVA@marcjacobs.com"
		tattach = " "
		tcc =""                                                                                                                               
		tFrom ="Toll EDI System Operations <fmi-transload-ops@fmiint.com>"                                
		tmessage = "NO MJ DELETED PT'S PREVIOUS MONTH  "+Ttoc(Datetime())       
		tSubject = "NO MJ DELETED PT'S PREVIOUS MONTH"                                                           
		Do Form m:\dev\frm\dartmail2 With tsendto,tFrom,tSubject, tcc,tattach,tmessage  ,"A"   

	ENDIF
close data all 

Wait window at 10,10 "all done...." timeout 2


schedupdate()
_screen.Caption=gscreencaption
on error
