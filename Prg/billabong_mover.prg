PUBLIC cfilename,cfileoutname,cAriatTransfer,normalexit
CLOSE DATA ALL
DO M:\DEV\PRG\_SETVARS WITH .T.
normalexit = .F.
cCustName = "BILLABONG"

tsendtoerr ="pgaidis@fmiint.com"
tccerr =""
xfile=""

TRY
	lTesting = .F.
	lOverrideBusy = lTesting
	lOverrideBusy = .T.
	cBillabongTransfer = "BILLABONG-MOVE"
	IF !lTesting
		SELECT 0
		USE F:\edirouting\ftpsetup SHARED
		LOCATE FOR ftpsetup.transfer = cBillabongTransfer
		IF ftpsetup.chkbusy = .T. AND !lOverrideBusy
			WAIT WINDOW "File is processing...exiting" TIMEOUT 2
			normalexit = .T.
			THROW
		ENDIF
		REPLACE chkbusy WITH .T. FOR ftpsetup.transfer = cBillabongTransfer
		USE IN ftpsetup
	ENDIF

	ASSERT .F. MESSAGE "In  mail data subroutine"
	SELECT 0
	USE F:\3pl\DATA\mailmaster ALIAS mm SHARED
	LOCATE FOR mm.edi_type = "MISC" AND mm.taskname = "GENERAL"
	STORE TRIM(IIF(mm.use_alt,mm.sendtoalt,mm.sendto)) TO tsendtoerr
	STORE TRIM(IIF(mm.use_alt,mm.ccalt,mm.cc)) TO tccerr
	USE IN mm

	cDirIn = "f:\ftpusers\billabong\in\"
	CD &cDirIn

	_SCREEN.CAPTION="Billabong File Move Process"
*(  _Screen.WindowState=Iif(lTesting,2,1)

	nFound = ADIR(ary1,cDirIn+"*")
	IF nFound = 0
		CLOSE DATABASES ALL
		WAIT WINDOW "No Billabong iles found to move.exiting" TIMEOUT 2
		close data all 
		CANCEL
		return
	ENDIF
	
SET STEP ON 
	
	len1 = ALEN(ary1,1)
	FOR xxi = 1 TO len1
		ASSERT .F. MESSAGE "In move loop"
		lFA = .f.
		cARfilename = ALLT(ary1[xxi,1])
		xfile = (cDirIn+cARfilename)

		WAIT WINDOW "Processing file# "+ALLTRIM(TRANSFORM(xxi))+ "of "+ALLTRIM(TRANSFORM(xxi))+"  File(s)  "+ cARfilename NOWAIT
		DO M:\DEV\PRG\createx856a
		DO M:\DEV\PRG\loadedifile WITH xfile,"|","PIPE2","BILLABONG"
		SELECT x856
		LOCATE FOR x856.segment = "GS"
		DO CASE
		CASE x856.f1 = 'SC' AND x856.f2 = "BBGTOLLPRD"
			cDirOut = ("F:\ftpusers\billabong\832in\")

		CASE x856.f1 = 'SC' AND x856.f2 = "BBGTOLLTST"
			cDirOut = ("F:\ftpusers\billabong\832in\test\")

		CASE x856.f1 = 'PO' AND x856.f2 = "BBGTOLLPRD"
			cDirOut = ("F:\ftpusers\billabong\850in\")

		CASE x856.f1 = 'PO' AND x856.f2 = "BBGTOLLTST"
			cDirOut = ("F:\ftpusers\billabong\850in\850test\")

		CASE x856.f1 = 'SH' AND !x856.f2 = "BBGTOLLPRD"
			cDirOut = ("F:\ftpusers\billabong\856in\")

		CASE x856.f1 = 'SH' AND x856.f2 = "BBGTOLLTST"
			cDirOut = ("F:\ftpusers\billabong\856in\test\")

		CASE x856.f1 = 'FA' AND x856.f2 = "BBGTOLLPRD"
			lFA = .t.
			cDirOut = ("F:\ftpusers\billabong\997in\")

		CASE x856.f1 = 'FA' AND x856.f2 = "BBGTOLLTST"
			lFA = .t.
			cDirOut = ("F:\ftpusers\billabong\997in\test\")

		OTHERWISE
			cDirOut = ("F:\ftpusers\billabong\UNK\")
		ENDCASE

		cDirXfer = ("F:\ftpusers\billabong\997xfer\")
		cFileXferName = (cDirXfer+cARfilename)

		cfileoutname = (cDirOut+cARfilename)

		COPY FILE [&xfile] TO [&cFileOutName]
		IF !lFA
		COPY FILE [&xfile] TO [&cFileXferName]  && Added 05.15.2017 to send 997s for all edi files
		ENDIF
		
		IF FILE(cfileoutname)
			DELETE FILE [&xfile]
		ENDIF
	ENDFOR

	WAIT WINDOW "Billabong file move process complete" TIMEOUT 1
	SELECT 0
	USE F:\edirouting\ftpsetup SHARED
	REPLACE chkbusy WITH .F. FOR ftpsetup.transfer = cBillabongTransfer

	WAIT CLEAR
	normalexit = .T.

CATCH TO oErr
	IF !normalexit
		ASSERT .F. MESSAGE "At CATCH..."
		SET STEP ON

		tsubject = cCustName+" File Move (Billbong) Upload Error ("+TRANSFORM(oErr.ERRORNO)+") at "+TTOC(DATETIME())
		tattach  = ""
		tsendto  = tsendtoerr
		tcc = tccerr
		tmessage = cCustName+" File Move (Billabong) Upload Error..... Please fix me........!"+CHR(13)
		lcSourceMachine = SYS(0)
		lcSourceProgram = SYS(16)

		tmessage =tmessage+ "Try/CATCH Exeception Message:"+CHR(13)+;
			[  Message: ] + oErr.MESSAGE +CHR(13)+;
			[  Procedure: ] + oErr.PROCEDURE +CHR(13)+;
			[  LineNo: ] + STR(oErr.LINENO) +CHR(13)+;
			[  Error: ] + STR(oErr.ERRORNO) +CHR(13)+;
			[  Details: ] + oErr.DETAILS +CHR(13)+;
			[  StackLevel: ] + STR(oErr.STACKLEVEL) +CHR(13)+;
			[  LineContents: ] + oErr.LINECONTENTS+CHR(13)+;
			[  UserValue: ] + oErr.USERVALUE+CHR(13)+;
			[  Computer:  ] +lcSourceMachine+CHR(13)+;
			[  940 file:  ] +xfile+CHR(13)+;
			[  Program:   ] +lcSourceProgram

		tattach  = ""
		tFrom ="TGF EDI Operations <toll-edi-ops@tollgroup.com>"
		DO FORM M:\DEV\frm\dartmail2 WITH tsendto,tFrom,tsubject,tcc,tattach,tmessage,"A"
	ELSE
		WAIT WINDOW AT 10,10 "Normal Exit " TIMEOUT 2
	ENDIF
FINALLY
	CLOSE DATABASES ALL
ENDTRY
