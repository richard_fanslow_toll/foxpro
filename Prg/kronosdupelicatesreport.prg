* list associates who may be in Kronos more than once
* build as f:\util\kronos\KRONOSDUPLICATESREPORT.exe

LPARAMETERS tcTempAgency

LOCAL loKRONOSDUPLICATESREPORT, lcTempAgency

IF TYPE('tcTempAgency') = 'L' THEN
	lcTempAgency = 'SSG'
ELSE
	lcTempAgency = UPPER(ALLTRIM(tcTempAgency))
ENDIF

utilsetup("KRONOSDUPLICATESREPORT")


loKRONOSDUPLICATESREPORT = CREATEOBJECT('KRONOSDUPLICATESREPORT')

loKRONOSDUPLICATESREPORT.SetTempAgency(lcTempAgency)

loKRONOSDUPLICATESREPORT.MAIN()

schedupdate()

CLOSE DATABASES ALL
RETURN

#DEFINE LOGIT 1
#DEFINE WAITIT 2
#DEFINE NOWAITIT 4
#DEFINE SENDIT 8
#DEFINE CRLF CHR(13) + CHR(10)
#DEFINE LF CHR(10)
#DEFINE RETURN_DATA_MANDATORY .F.
#DEFINE RETURN_DATA_NOT_MANDATORY .T.

DEFINE CLASS KRONOSDUPLICATESREPORT AS CUSTOM

	cProcessName = 'KRONOSDUPLICATESREPORT'

	lTestMode = .F.  && .F. FOR NORMAL OPERATION, .T. TO SUPPRESS ALL BUT DEVELOPER EMAILS AND CHANGE INPUT TABLE, ETC.

	***********************   SET REPORT TYPE BEFORE BUILDING EXE   **********************
	**************************************************************************************
	**************************************************************************************

	lAutoYield = .T.

	*!*		* !!! When you set this flag to .T. make the exe = KRONOSDUPLICATESREPORT-TUG.EXE !!!!!
	*!*		lDoSpecialTUGReport = .F.

	cStartTime = TTOC(DATETIME())

	* connection properties
	nSQLHandle = 0

	* file properties
	nFileHandle = 0

	* report filter properties
	cTempAgency = ""
	cTempAgencyName = ""
	cTempAgencyWhere = ""

	* date properties
	dtNow = DATETIME()

	dToday = DATE()

	* wait window properties
	nWaitWindowTimeout = 2
	lWaitWindowIsOn = .T.

	* logfile properties
	lLoggingIsOn = .T.
	cLogFile = 'F:\UTIL\KRONOS\Logfiles\KRONOSDUPLICATESREPORT_log.txt'

	* INTERNAL email properties
	lSendInternalEmailIsOn = .T.
	cFrom = 'FMI Corporate <fmicorporate@fmiint.com>'
	*cSendTo = 'Mark Bennett <mbennett@fmiint.com>'
	cSendTo = 'Mark Bennett <mbennett@fmiint.com>'
	cCC = ''
	cSubject = 'Kronos Duplicate Associates Report for: ' + DTOC(DATE())
	cAttach = ''
	cBodyText = ''

	cCOMPUTERNAME = ''
	cUSERNAME = ''

	FUNCTION INIT
		IF NOT DODEFAULT()
			RETURN .F.
		ENDIF
		WITH THIS
			CLEAR
			*SET RESOURCE OFF
			CLOSE DATA
			SET CENTURY ON
			SET DATE AMERICAN
			SET HOURS TO 24
			SET ANSI ON
			SET TALK OFF
			SET DELETED ON
			SET CONSOLE OFF
			SET EXCLUSIVE OFF
			SET SAFETY OFF
			SET EXACT OFF
			SET STATUS BAR ON
			SET SYSMENU OFF
			SET ENGINEBEHAVIOR 70
			_VFP.AUTOYIELD = .lAutoYield
			.cCOMPUTERNAME = GETENV("COMPUTERNAME")
			.cUSERNAME = GETENV("USERNAME")
			IF .lTestMode THEN
				.cSendTo = 'Mark Bennett <mbennett@fmiint.com>'
				.cLogFile = 'F:\UTIL\KRONOS\Logfiles\KRONOSDUPLICATESREPORT_log_TESTMODE.txt'
			ENDIF
			.lLoggingIsOn = .lLoggingIsOn AND NOT EMPTY(.cLogFile)
			IF .lLoggingIsOn THEN
				SET ALTERNATE TO (.cLogFile) ADDITIVE
				SET ALTERNATE ON
			ENDIF
		ENDWITH
	ENDFUNC


	FUNCTION DESTROY
		WITH THIS
			IF .lLoggingIsOn  THEN
				SET ALTERNATE OFF
				SET ALTERNATE TO
			ENDIF
		ENDWITH
		DODEFAULT()
	ENDFUNC


	FUNCTION MAIN
		WITH THIS
			LOCAL lnNumberOfErrors, lcSQL, lloError, llValid
			LOCAL lcXLFileName, lcSQLNumber, lcSQLName
			LOCAL lcSendTo, lcCC, lcBodyText, lcTempAgencyWhere

			TRY
				lnNumberOfErrors = 0

				.TrackProgress('Kronos Duplicate Associates Report process started....', LOGIT+SENDIT+NOWAITIT)
				.TrackProgress('COMPUTERNAME = ' + .cCOMPUTERNAME, LOGIT+SENDIT)
				.TrackProgress('USERNAME = ' + .cUSERNAME, LOGIT+SENDIT)
				IF .lTestMode THEN
					.TrackProgress('                        TEST MODE                        ', LOGIT+SENDIT)
				ENDIF
				.TrackProgress('PROJECT = KRONOSDUPLICATESREPORT', LOGIT+SENDIT)

				IF EMPTY(.cTempAgency) THEN
					THROW
				ENDIF

				.cSubject = 'Kronos Duplicate Associates Report for ' + .cTempAgencyName + DTOC(.dToday) && + " TRISTATEAUTOFWD"

				* calculate pay period start/end dates.
				* We go back from today until we hit a Saturday (DOW = 7) , which is the end day of the previous pay period.
				* then we subtract 6 days from that to get the prior Sunday (DOW = 1), which is the start day of the previous pay period.

				IF USED('CURDUPES') THEN
					USE IN CURDUPES
				ENDIF

				IF USED('CURDUPES2') THEN
					USE IN CURDUPES2
				ENDIF

				IF USED('CURDUPES3') THEN
					USE IN CURDUPES3
				ENDIF

				IF NOT .lTestMode THEN
					* assign Send to based on TempCo (which indicates temp agency)
					DO CASE
						CASE .cTempAgency == 'SSG'
							.cSendTo = 'apetsuch@thessg.com, jcano@thessg.com, achavez@thessg.com, rsandoval@thessg.com'
							.cCC = 'mbennett@fmiint.com'
						OTHERWISE
							.cSendTo = 'mbennett@fmiint.com'
							.cCC = ""
					ENDCASE
				ENDIF

				* main SQL

				lcTempAgencyWhere = .cTempAgencyWhere

				SET TEXTMERGE ON

				TEXT TO	lcSQLNumber NOSHOW

SELECT
PERSONFULLNAME AS NAME,
PERSONNUM AS KRONOSNUM,
SHORTNM AS AGENCYNUM,
HOMELABORLEVELNM2 AS DIVISION,
HOMELABORLEVELNM3 AS DEPT,
HOMELABORLEVELNM4 AS WORKSITE,
HOMELABORLEVELNM5 AS SITECODE
FROM VP_EMPLOYEEV42
WHERE <<lcTempAgencyWhere>>
AND EMPLOYMENTSTATUS = 'Active'
ORDER BY NAME

				ENDTEXT

				SET TEXTMERGE OFF

				.TrackProgress('Connecting to WTK System....', LOGIT+SENDIT)

				.nSQLHandle = SQLCONNECT("WTK2","sa","kr0n1tes")

				IF .nSQLHandle > 0 THEN

					IF .ExecSQL(lcSQLNumber, 'CURDUPES', RETURN_DATA_NOT_MANDATORY) THEN

						SELECT AGENCYNUM, ;
							COUNT(*) AS CNT ;
							FROM CURDUPES ;
							INTO CURSOR CURDUPES2 ;
							WHERE VAL(AGENCYNUM) > 0 ;
							GROUP BY AGENCYNUM

						*!*		SELECT CURDUPES2
						*!*		BROWSE FOR CNT > 1

						SELECT * ;
							FROM CURDUPES ;
							WHERE AGENCYNUM IN (SELECT AGENCYNUM FROM CURDUPES2 WHERE CNT > 1) ;
							INTO CURSOR CURDUPES3 ;
							ORDER BY AGENCYNUM

						IF USED('CURDUPES3') AND NOT EOF('CURDUPES3') THEN

							lcXLFileName = 'F:\UTIL\KRONOS\TEMPTIMEREPORTS\' + .cTempAgency + '_DUPLICATES_' + DTOS(DATE()) + '.XLS'
							SELECT CURDUPES3
							COPY TO (lcXLFileName) XL5
							IF FILE(lcXLFileName) THEN
								.cAttach = lcXLFileName
								.TrackProgress('Created Duplicate Associates Report File : ' + lcXLFileName, LOGIT+SENDIT)
								.TrackProgress('Kronos Duplicate Associates Report process ended normally.', LOGIT+SENDIT+NOWAITIT)
							ELSE
								.TrackProgress('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!', LOGIT+SENDIT)
								.TrackProgress("ERROR: There was a problem creating Duplicate Associates File : " + lcXLFileName, LOGIT+SENDIT)
								.TrackProgress('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!', LOGIT+SENDIT)
							ENDIF

						ELSE
							.TrackProgress('=====> No duplicates were found today!', LOGIT+SENDIT)
						ENDIF


					ENDIF  &&  .ExecSQL(...)

					.cBodyText = "The attached spreadsheets shows possible Duplicate Associates." + CRLF + CRLF + ;
						"----------------------------------------------------------------------" + CRLF + ;
						"<process log follows>" + CRLF + ;
						"----------------------------------------------------------------------" + CRLF + ;
						.cBodyText

					CLOSE DATABASES ALL
				ELSE
					* connection error
					.TrackProgress('Unable to connect to WTK System.', LOGIT+SENDIT)
				ENDIF   &&  .nSQLHandle > 0
			CATCH TO loError

				.TrackProgress('There was an error.',LOGIT+SENDIT)
				.TrackProgress('The Error # is: ' + TRANSFORM(loError.ERRORNO), LOGIT+SENDIT)
				.TrackProgress('The Error Message is: ' + TRANSFORM(loError.MESSAGE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred in Program: ' + TRANSFORM(loError.PROCEDURE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred at line #: ' + TRANSFORM(loError.LINENO), LOGIT+SENDIT)
				lnNumberOfErrors = lnNumberOfErrors + 1
				IF TYPE('oExcel') = 'O' AND NOT ISNULL(oExcel) THEN
					oExcel.QUIT()
				ENDIF
				CLOSE DATA
				CLOSE ALL

			ENDTRY

			CLOSE DATA
			WAIT CLEAR
			***************** INTERNAL email results ******************************
			.TrackProgress('About to send status email.',LOGIT)
			.TrackProgress('==================================================================================================================', SENDIT)
			.TrackProgress('The logfile is: ' + .cLogFile, SENDIT)
			.TrackProgress('Kronos Duplicate Associates Report process started: ' + .cStartTime, LOGIT+SENDIT)
			.TrackProgress('Kronos Duplicate Associates Report process finished: ' + TTOC(DATETIME()), LOGIT+SENDIT)

			IF .lSendInternalEmailIsOn THEN
				* try to trap error from not having dartmail dll's registered on user's pc...
				TRY
					DO FORM dartmail2 WITH .cSendTo,.cFrom,.cSubject,.cCC,.cAttach,.cBodyText,"A"
					.TrackProgress('Sent status email.',LOGIT)
				CATCH TO loError
					*=MESSAGEBOX('There was an error sending the status email. Please contact Mark Bennett.',0+64,"KRONOS Temp Time")
					.TrackProgress('There was an error sending the status email.',LOGIT)
					.TrackProgress('The Error # is: ' + TRANSFORM(loError.ERRORNO), LOGIT)
					.TrackProgress('The Error Message is: ' + TRANSFORM(loError.MESSAGE), LOGIT)
					.TrackProgress('The Error occurred in Program: ' + TRANSFORM(loError.PROCEDURE), LOGIT)
					.TrackProgress('The Error occurred at line #: ' + TRANSFORM(loError.LINENO), LOGIT)
					lnNumberOfErrors = lnNumberOfErrors + 1
					CLOSE DATA
				ENDTRY

			ELSE
				.TrackProgress('Did not send status email: THIS.lSendInternalEmailIsOn = FALSE.',LOGIT)
			ENDIF

		ENDWITH
		RETURN
	ENDFUNC && main


	FUNCTION ExecSQL
		LPARAMETERS tcSQL, tcCursorName, tlNoDataReturnedIsOkay
		LOCAL llRetval, lnResult
		WITH THIS
			* close target cursor if it's open
			IF USED(tcCursorName)
				USE IN (tcCursorName)
			ENDIF
			WAIT WINDOW "Fetching remote data; please wait..." NOWAIT
			lnResult = SQLEXEC(.nSQLHandle, tcSQL, tcCursorName)
			llRetval = ( lnResult > 0 )
			IF llRetval THEN
				* see if any data came back
				IF NOT tlNoDataReturnedIsOkay THEN
					IF (NOT USED(tcCursorName)) OR EOF(tcCursorName) THEN
						llRetval = .F.
						.TrackProgress('No data was returned by this query: ' +CRLF + tcSQL, LOGIT+SENDIT)
					ENDIF
				ENDIF
			ELSE
				.TrackProgress("Result Code [" + TRANSFORM(lnResult) + "] returned by this query: " + tcSQL, LOGIT+SENDIT)
			ENDIF
			WAIT CLEAR
			RETURN llRetval
		ENDWITH
	ENDFUNC


	PROCEDURE TrackProgress
		* do any combination of Wait Window, writing to logfile, and adding to body of email,
		* based on nFlags parameter.
		LPARAMETERS tcExpression, tnFlags
		WITH THIS
			IF BITAND(tnFlags,LOGIT) = LOGIT THEN
				IF .lLoggingIsOn THEN
					?
					? .cProcessName + "  " + TTOC(DATETIME()) + ": " + tcExpression
				ENDIF
			ENDIF
			IF .lWaitWindowIsOn THEN
				IF BITAND(tnFlags,NOWAITIT) = NOWAITIT THEN
					WAIT WINDOW tcExpression NOWAIT
				ENDIF
				IF BITAND(tnFlags,WAITIT) = WAITIT THEN
					WAIT WINDOW tcExpression TIMEOUT .nWaitWindowTimeout
				ENDIF
			ENDIF
			IF BITAND(tnFlags,SENDIT) = SENDIT THEN
				IF .lSendInternalEmailIsOn THEN
					.cBodyText = .cBodyText + tcExpression + CRLF + CRLF
				ENDIF
			ENDIF
		ENDWITH
	ENDPROC  &&  TrackProgress



	PROCEDURE SetTempAgency
		LPARAMETERS tcTempAgency
		WITH THIS
			DO CASE
				CASE tcTempAgency == 'SSG'
					.cTempAgency = 'SSG'
					.cTempAgencyName = 'Staffing Solutions Group'
					.cTempAgencyWhere = [ HOMELABORLEVELNM5 IN ('R','T','U','V') ]
					.TrackProgress('Set cTempAgency = ' + .cTempAgency,LOGIT+SENDIT)
				OTHERWISE
					.TrackProgress('INVALID cTempAgency parameter = ' + tcTempAgency,LOGIT+SENDIT)
			ENDCASE
		ENDWITH
	ENDPROC


ENDDEFINE
