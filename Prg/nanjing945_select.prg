PARAMETERS nWO_Num,cBOL,cShip_ref,cOffice,nAcctNum,lPTFlag,cConsignee,lDSDC,tTrigTime,cSCAC
PUBLIC lParcelType

gOffice = IIF(coffice = "C","1",cOffice)
ltesting = .F.
lParcelType = .F.
cSelWait = IIF(ltesting,"TIMEOUT 2","NOWAIT")

*SET STEP ON
IF USED('outship')
	USE IN outship
ENDIF

WAIT WINDOW "At Nanjing Type determination in SELECT" nowait

lPickNPack = IIF(nAcctNum=4610,.T.,.F.)

lPickPackDSDC =.F.
IF ("DSDC"$cConsignee OR lDSDC)
	SELECT 0
	xsqlexec("select * from outship where accountid = "+TRANSFORM(nAcctNum)+" and bol_no = '"+cBOL+"'",,,"wh")
	INDEX ON ship_ref TAG ship_ref

	SELECT outship
	IF SEEK(cShip_ref)
		IF "PREPACK"$outship.shipins
			lPickPackDSDC = .T.
		ENDIF
	ENDIF
	USE IN outship
ENDIF
IF USED('bl')
	USE IN bl
ENDIF
gOffice = IIF(cOffice = "C","1",cOffice)
gMasterOffice= IIF(cOffice = "I","N",cOffice)

csq1 = [select * from bl where mblnum = ']+cBOL+[']
xsqlexec(csq1,,,"wh")
SELECT bl
INDEX ON bol_no TAG bol_no
INDEX ON mblnum TAG mblnum

IF USED('carriers')
	USE IN carriers
ENDIF
IF USED('parcel_carriers')
	USE IN parcel_carriers
ENDIF
SELECT 0
USE F:\3pl\DATA\parcel_carriers ALIAS parcel_carriers
IF SEEK(cSCAC,'parcel_carriers','scac')
	lParcelType = .T.
ENDIF
USE IN parcel_carriers
RELEASE cUseFolder

IF lPickNPack OR lPickPackDSDC
	DO CASE
	IF DATETIME()<DATETIME(2017,11,02,15,30,00)
	SET STEP ON 
	endif
	CASE (lParcelType AND ("DSDC"$cConsignee OR lDSDC)) && UPS/DSDC
		WAIT WINDOW "This is a DSDC Parcel BOL" &cSelWait
		DO m:\dev\prg\nanjing945pp-dsdc_create_ups WITH nWO_Num,cBOL,cShip_ref,cOffice,lPTFlag,lPickPackDSDC  && DSDC UPS
	CASE (!lParcelType AND ("DSDC"$cConsignee OR lDSDC)) && Non-UPS/DSDC
		lMBOL = .F.
		SELECT bl
		IF SEEK(cBOL,'bl','mblnum')
			IF INLIST(bl.accountid,4610,4694)
				lMBOL = .T.
				REPLACE edi_trigger.masterbill WITH .T. FOR edi_trigger.bol = cBOL IN edi_trigger
			ENDIF
		ENDIF
		IF lMBOL
			WAIT WINDOW "This is a DSDC Master BOL" &cSelWait
			DO m:\dev\prg\nanjing945pp-dsdc_create_mbol WITH nWO_Num,cBOL,cShip_ref,cOffice,lPTFlag,lPickPackDSDC  && DSDC UPS
		ELSE
			WAIT WINDOW "This is a DSDC Standard BOL" &cSelWait
			DO m:\dev\prg\nanjing945pp-dsdc_create WITH nWO_Num,cBOL,cShip_ref,cOffice,lPTFlag,lPickPackDSDC  && DSDC UPS
		ENDIF
	CASE (lParcelType AND !("DSDC"$cConsignee) AND !lDSDC) && AND DATETIME()>DATETIME(2016,09,27,13,45,00) && UPS/To Store
		WAIT WINDOW "This is a Standard UPS BOL" &cSelWait
		DO m:\dev\prg\nanjing945pp_create_ups WITH cBOL,nWO_Num,cShip_ref,cOffice,lPTFlag
	OTHERWISE && Non-UPS/To Store
		WAIT WINDOW "This is a Standard PnP BOL" &cSelWait
		DO m:\dev\prg\nanjing945pp_create WITH nWO_Num,cBOL,cShip_ref,cOffice,lPTFlag
	ENDCASE
ELSE && Account ID = 4694
	DO CASE
	CASE ("PENNEY"$cConsignee OR "SEARS"$cConsignee) AND !lParcelType
		WAIT WINDOW "This is a JCPenney BOL" &cSelWait
		DO m:\dev\prg\nanjing945jcp_create WITH nWO_Num,cBOL,cShip_ref,cOffice,lPTFlag,tTrigTime
	CASE lParcelType
		WAIT WINDOW "This is a Parcel Prepack BOL" &cSelWait
		DO m:\dev\prg\nanjing945_create_ups WITH cBOL,nWO_Num,cShip_ref,cOffice
	OTHERWISE
		WAIT WINDOW "This is a Standard Prepack BOL" &cSelWait
		DO m:\dev\prg\nanjing945_create WITH nWO_Num,cBOL,cShip_ref,cOffice
	ENDCASE
ENDIF
IF USED('bl')
	USE IN bl
ENDIF

PROCEDURE triggerclose
PARAMETERS cMessage
SELECT edi_trigger
REPLACE edi_trigger.processed WITH .T.,edi_trigger.errorflag WITH .T.,edi_trigger.fin_status WITH cMessage ;
	FOR edi_trigger.wo_num = nWO_Num

IF USED('outwolog')
	USE IN outwolog
ENDIF
ENDPROC
