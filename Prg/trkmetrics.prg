Parameters tAcct, tAcctName, tOffice, tStart, tEnd

*!*	STORE 59 TO TACCT
*!*	STORE 'AMERICAN EAGLE' TO TACCTNAME
*!*	STORE 'C' TO TOFFICe
*!*	STORE {7/1} to TSTART
*!*	STORE gomonth(TSTART,1)-1 TO tend
*!*	STORE 3 TO REPORTTYPE

STORE 12-LEN(ALLTRIM(STR(TACCT))) TO TLEN
STORE ALLTRIM(STR(tacct)) TO xcustomerkey
DO WHILE TLEN>0
	STORE '0'+XCUSTOMERKEY TO XCUSTOMERKEY
	TLEN=TLEN-1
ENDDO

Store Iif(Empty(tAcct), .t., .f.) to allaccounts
Store Iif(Empty(tAcct), "ALL ACCOUNTS", tAcctName) TO tAcctName

USE F:\wo\wodata\WOLOG order wo_date IN 0

*Temp has the data consolidated by date and temp4 is used as temporary file for sortation
create cursor TEMP (DATE D, DOS N(10,0), AVAIL N(10,0), PICKEDUP N(10,0), RETURNED N(10,0))
INDEX ON DATE TAG DATE

*Temp2 has the data consolidated by account and month and temp4 is used as temporary file for sortation
create cursor TEMP2 (monthyr c(5), accountid n(4), acctname c(30), DOS N(10,0), AVAIL N(10,0), PICKEDUP N(10,0), RETURNED N(10,0))
INDEX ON accountid TAG accountid

create cursor TEMP4 (monthyr c(5), accountid n(4), acctname c(30), DOS N(10,0), AVAIL N(10,0), PICKEDUP N(10,0), RETURNED N(10,0))
INDEX on pickedup TAG ACCTNAME descending

*Temp3 has the data consolidated by account and day
create cursor TEMP3 (date D, accountid n(4), acctname c(30), DOS N(10,0), AVAIL N(10,0), PICKEDUP N(10,0), RETURNED N(10,0))
INDEX ON accountid TAG accountid

*temp5 is used to sort the temp3 data by the number of containers instead of alphabetically by account
create cursor TEMP5 (Xacctname c(30) , tt n(10,0) , tavg n(10,2), tpeak n(10,0) , peakday d , pta n(10,2), suspeak n(10,0) , susdates c(30), suspta n(10,2) )
INDEX ON tt TAG tt descending

Select wolog
DO WHILE .T.
	SEEK tstart
	IF EOF()
		tstart=tstart+1
	ELSE
		exit
	ENDIF
ENDDO

STORE tstart-30 TO BEGIN
DO WHILE WO_DATE>BEGIN
	SKIP -1
ENDDO

DO WHILE WO_DATE<=tend AND !EOF()
	IF !ALLACCOUNTS AND accountid<>Tacct
		SKIP
		LOOP
	ENDIF

	IF OFFICE=TOFFICE AND type='O' AND !loose
		STORE WO_NUM TO two

		*FIRST THE DO DATE
		IF WO_DATE>=tstart
			SELECT TEMP
			SEEK WOLOG.WO_DATE
			IF EOF()
				APPEND BLANK
				REPLACE DATE WITH WOLOG.wo_date
			ENDIF
			REPLACE DOS WITH DOS+1

			SELECT temp2
			SEEK wolog.accountid
			IF EOF()
				APPEND BLANK
				replace accountid WITH wolog.accountid
				replace acctname WITH wolog.acctname
				STORE UPPER(SUBSTR(CMONTH(wolog.wo_date),1,3))+SUBSTR(STR(YEAR(wolog.wo_date),4,0),3,2) TO cmo
				replace monthyr WITH cmo
			ENDIF
			STORE UPPER(SUBSTR(CMONTH(wolog.wo_date),1,3))+SUBSTR(STR(YEAR(wolog.wo_date),4,0),3,2) TO cmo
			DO WHILE accountid=WOLOG.accountid AND !EOF()
				IF monthyr=cmo
					EXIT
				ENDIF
				SKIP
			ENDDO

			IF !(monthyr=cmo AND accountid=WOLOG.accountid)
				APPEND BLANK
				replace accountid WITH wolog.accountid
				replace acctname WITH wolog.acctname
				replace monthyr WITH cmo
			ENDIF
			replace dos WITH dos+1

			SELECT temp3
			SEEK wolog.accountid
			IF EOF()
				APPEND BLANK
				replace accountid WITH wolog.accountid
				replace acctname WITH wolog.acctname
				replace date WITH wolog.wo_date
			ENDIF
			DO WHILE accountid=WOLOG.accountid AND !EOF()
				IF date=wolog.wo_date
					EXIT
				ENDIF
				SKIP
			ENDDO

			IF !(date=wolog.wo_date AND accountid=WOLOG.accountid)
				APPEND BLANK
				replace accountid WITH wolog.accountid
				replace acctname WITH wolog.acctname
				replace date WITH wolog.wo_date
			ENDIF

			replace dos WITH dos+1
		ENDIF

		*Then the pick up date
		SELECT temp
		IF WOLOG.PICKEDUP>=tstart AND WOLOG.PICKEDUP<=tend
			SEEK WOLOG.PICKEDUP
			IF EOF()
				APPEND BLANK
				REPLACE DATE WITH WOLOG.PICKEDUP
			ENDIF
			REPLACE PICKEDUP WITH PICKEDUP+1

			SELECT temp2
			SEEK wolog.accountid
			IF EOF()
				APPEND BLANK
				replace accountid WITH wolog.accountid
				replace acctname WITH wolog.acctname
				STORE UPPER(SUBSTR(CMONTH(wolog.PICKEDUP),1,3))+SUBSTR(STR(YEAR(wolog.PICKEDUP),4,0),3,2) TO cmo

				replace monthyr WITH cmo
			ENDIF
			STORE UPPER(SUBSTR(CMONTH(wolog.PICKEDUP),1,3))+SUBSTR(STR(YEAR(wolog.PICKEDUP),4,0),3,2) TO cmo

			DO WHILE accountid=WOLOG.accountid AND !EOF()
				IF monthyr=cmo
					EXIT
				ENDIF
				SKIP
			ENDDO
			IF !(monthyr=cmo AND accountid=WOLOG.accountid)
				APPEND BLANK
				replace accountid WITH wolog.accountid
				replace acctname WITH wolog.acctname
				replace monthyr WITH cmo
			ENDIF
			replace pickedup WITH pickedup+1

			SELECT temp3
			SEEK wolog.accountid
			IF EOF()
				APPEND BLANK
				replace accountid WITH wolog.accountid
				replace acctname WITH wolog.acctname
				replace date WITH wolog.pickedup
			ENDIF
			DO WHILE accountid=WOLOG.accountid AND !EOF()
				IF date=wolog.pickedup
					EXIT
				ENDIF
				SKIP
			ENDDO
			IF !(date=wolog.pickedup AND accountid=WOLOG.accountid)
				APPEND BLANK
				replace accountid WITH wolog.accountid
				replace acctname WITH wolog.acctname
				replace date WITH wolog.pickedup
			ENDIF
			replace pickedup WITH pickedup+1
		ENDIF

		*Then the return date
		SELECT temp
		IF WOLOG.RETURNED>=TSTART AND WOLOG.RETURNED<=tend
			SEEK WOLOG.RETURNED
			IF EOF()
				APPEND BLANK
				REPLACE DATE WITH WOLOG.RETURNED
			ENDIF
			REPLACE RETURNED WITH RETURNED+1

			SELECT temp2
			SEEK wolog.accountid
			IF EOF()
				APPEND BLANK
				replace accountid WITH wolog.accountid
				replace acctname WITH wolog.acctname
				STORE UPPER(SUBSTR(CMONTH(wolog.RETURNED),1,3))+SUBSTR(STR(YEAR(wolog.RETURNED),4,0),3,2) TO cmo		
				replace monthyr WITH cmo
			ENDIF
			STORE UPPER(SUBSTR(CMONTH(wolog.RETURNED),1,3))+SUBSTR(STR(YEAR(wolog.RETURNED),4,0),3,2) TO cmo
			DO WHILE accountid=WOLOG.accountid AND !EOF()
				IF monthyr=cmo
					EXIT
				ENDIF
				SKIP
			ENDDO

			IF !(monthyr=cmo AND accountid=WOLOG.accountid)
				APPEND BLANK
				replace accountid WITH wolog.accountid
				replace acctname WITH wolog.acctname
				replace monthyr WITH cmo
			ENDIF
			replace returned WITH returned+1

			SELECT temp3
			SEEK wolog.accountid
			IF EOF()
				APPEND BLANK
				replace accountid WITH wolog.accountid
				replace acctname WITH wolog.acctname
				replace date WITH wolog.returned
			ENDIF

			DO WHILE accountid=WOLOG.accountid AND !EOF()
				IF date=wolog.returned
					EXIT
				ENDIF
				Skip
			ENDDO

			IF !(date=wolog.returned AND accountid=WOLOG.accountid)
				APPEND BLANK
				replace accountid WITH wolog.accountid
				replace acctname WITH wolog.acctname
				replace date WITH wolog.returned
			ENDIF
			replace returned WITH returned+1
		ENDIF

		*THen the avail date
		
		xsqlexec("select * from availlog where wo_num="+transform(two)+" and avail#{}",,,"wo")
		
		IF WO_NUM=two AND AVAIL>=tstart AND AVAIL<=tend
			SELECT TEMP
			SEEK AVAILLOG.AVAIL
			IF EOF()
				APPEND BLANK
				REPLACE DATE WITH AVAILLOG.AVAIL
			ENDIF
			REPLACE temp.AVAIL WITH temp.AVAIL +1

			SELECT temp2
			SEEK WOLOG.accountid
			IF EOF()
				APPEND BLANK
				replace accountid WITH wolog.accountid
				replace acctname WITH wolog.acctname
				STORE UPPER(SUBSTR(CMONTH(AVAILLOG.AVAIL),1,3))+SUBSTR(STR(YEAR(AVAILLOG.AVAIL),4,0),3,2) TO cmo
				replace monthyr WITH cmo
			ENDIF
			STORE UPPER(SUBSTR(CMONTH(AVAILLOG.AVAIL),1,3))+SUBSTR(STR(YEAR(AVAILLOG.AVAIL),4,0),3,2) TO cmo
			DO WHILE accountid=WOLOG.accountid AND !EOF()
				IF monthyr=cmo
					EXIT
				ENDIF
				SKIP
			ENDDO

			IF !(monthyr=cmo AND accountid=WOLOG.accountid)
				APPEND BLANK
				replace accountid WITH wolog.accountid
				replace acctname WITH wolog.acctname
				replace monthyr WITH cmo
			ENDIF
			replace avail WITH avail +1

			SELECT temp3
			SEEK wolog.accountid
			IF EOF()
				APPEND BLANK
				replace accountid WITH wolog.accountid
				replace acctname WITH wolog.acctname
				replace date WITH availlog.avail
			ENDIF
			DO WHILE accountid=WOLOG.accountid AND !EOF()
				IF date=availlog.avail
					EXIT
				ENDIF
				SKIP
			ENDDO

			IF date=availlog.avail AND accountid=WOLOG.accountid
			ELSE
				APPEND BLANK
				replace accountid WITH wolog.accountid
				replace acctname WITH wolog.acctname
				replace date WITH availlog.avail
			ENDIF
			replace avail WITH avail+1
		Else
			* if there was no avail date recorded but the container was pickedup, use the pickup date as avail.
			IF WOLOG.PICKEDUP>=tstart AND WOLOG.PICKEDUP<=tend
				SELECT TEMP
				SEEK WOLOG.PICKEDUP
				REPLACE temp.AVAIL WITH temp.AVAIL +1

				SELECT temp2
				SEEK wolog.accountid
				IF EOF()
					APPEND BLANK
					replace accountid WITH wolog.accountid
					replace acctname WITH wolog.acctname
					STORE UPPER(SUBSTR(CMONTH(wolog.PICKEDUP),1,3))+SUBSTR(STR(YEAR(wolog.PICKEDUP),4,0),3,2) TO cmo
					replace monthyr WITH cmo
				ENDIF
				STORE UPPER(SUBSTR(CMONTH(wolog.PICKEDUP),1,3))+SUBSTR(STR(YEAR(wolog.PICKEDUP),4,0),3,2) TO cmo
				DO WHILE accountid=WOLOG.accountid AND !EOF()
					IF monthyr=cmo
						EXIT
					ENDIF
					SKIP
				ENDDO

				IF !(monthyr=cmo AND accountid=WOLOG.accountid)
					APPEND BLANK
					replace accountid WITH wolog.accountid
					replace acctname WITH wolog.acctname
					replace monthyr WITH cmo
				ENDIF
				replace avail WITH avail +1

				SELECT temp3
				SEEK wolog.accountid
				IF EOF()
					APPEND BLANK
					replace accountid WITH wolog.accountid
					replace acctname WITH wolog.acctname
					replace date WITH wolog.pickedup
				ENDIF
				DO WHILE accountid=WOLOG.accountid AND !EOF()
					IF date=wolog.pickedup
						EXIT
					ENDIF
					SKIP
				ENDDO
				IF !(date=wolog.pickedup AND accountid=WOLOG.accountid)
					APPEND BLANK
					replace accountid WITH wolog.accountid
					replace acctname WITH wolog.acctname
					replace date WITH wolog.pickedup
				ENDIF
				replace avail WITH avail+1
			ENDIF
		ENDIF

		SELECT wolog
	EndIf

	skip
ENDDO

**create a random filename to prevent overwriting of files
cFileName = "h:\fox\"+SUBSTR(SYS(2015), 3, 10)+".xls"	&& dy 11/7/11

xsqlexec("select * from glchart",,,"ar")

xsqlexec("select * from account where inactive=0","account",,"qq")
index on accountid tag accountid
set order to

WAIT "Compiling data. This process could take a few minutes... "+;
	"The output Excel spreadsheet will appear on completion." WINDOW NOWAIT NOCLEAR

*get the total number of workdays within this period
STORE tstart TO TDATE
STORE 0 TO TOTNUMDAYS

DO WHILE TDATE<=TEND
	IF DOW(TDATE)>=2 AND DOW(TDATE)<=6
		TOTNUMDAYS=TOTNUMDAYS+1
	ENDIF
	TDATE=TDATE+1
ENDDO

*TAKE OUT HOLIDAYS 
IF totnumdays>360 AND totnumdays<370
	totnumdays=totnumdays-10
ENDIF

STORE DTOC(tstart)+"-"+DTOC(tend)+" ("+ALLTRIM(STR(TOTNUMDAYS))+" days)" TO datestring
DO CASE
  CASE toffice="C"
	STORE "San Pedro" TO tlocation
  CASE toffice='N'
	STORE "Carteret" TO tlocation
  CASE toffice='M'
	STORE "Miami" TO tlocation
ENDCASE

SELECT temp
IF EOF()
    strMsg = "No data was found for the selected account."
	=MessageBox(strMsg, 16, "Metrics Report")
	=closeData()
	Return
EndIf

COPY FILE f:\wo\xls\drayrpt.xls TO &cFileName	&& dy 11/7/11

oexcel=createobject("excel.application")

* open existing:
oworkbook=oexcel.workbooks.open(cFileName)

* point to worksheets:
osheet1=oworkbook.worksheets[1]
osheet1.range("B4").value=tacctname
osheet1.range("B5").value=DATESTRING
osheet1.range("B6").value=TLOCATION

osheet1=oworkbook.worksheets[2]
osheet1.range("B4").value=tacctname
osheet1.range("B5").value=DATESTRING
osheet1.range("B6").value=TLOCATION

osheet1=oworkbook.worksheets[3]
osheet1.range("B4").value=tacctname
osheet1.range("B5").value=DATESTRING
osheet1.range("B6").value=TLOCATION

osheet1=oworkbook.worksheets[4]
osheet1.range("B4").value=tacctname
osheet1.range("B5").value=DATESTRING
osheet1.range("B6").value=TLOCATION

SELECT temp3
LOCATE
DO WHILE !EOF()
	STORE accountid TO XACCT
	STORE acctname TO XACCTNAME
	STORE acctname TO m.XACCTNAME
	STORE RECNO() TO TREC
	SUM pickedup TO m.tt WHILE accountid=XACCT
	STORE tt/TOTNUMDAYS TO m.tavg
	GOTO TREC
	STORE 0 TO m.tpeak
	STORE DATE() TO m.peakday

	DO WHILE accountid=XACCT AND !EOF()
	    IF PICKEDUP>tpeak
			STORE PICKEDUP TO m.tpeak
			STORE DAte TO m.peakday
	    ENDIF
	    SKIP
    ENDDO

	STORE tpeak/tavg TO m.PTA
	
	*LOOK FOR THE PEAK containers pickedupIN ANY CALENDAR WEEK (DAYS 1-7)
	goto TREC
	*	GSUSPEAK = the number of containers pickedup in the peak week of the period.
	STORE 0 TO GSUSPEAK
	STORE DATE() TO GPEAKSTART,GPEAKEND

	DO WHILE accountid=xACCT AND date<GPEAKSTART+6 AND !EOF()
		IF DOW(date)=2 
			EXIT
		ENDIF
		SKIP
	ENDDO

	STORE RECNO() TO zREC
	GOTO TREC

	SUM pickedup TO GSUSPEAK WHILE RECNO()<=zREC
	DO WHILE accountid=XACCT AND !EOF()
		STORE date TO TDATE,GPEAKSTART
		STORE date+6 TO GPEAKEND
		STORE 0 TO TEMPPEAK
		DO WHILE accountid=xacct AND date<(TDATE+7) and !EOF() 
			TEMPPEAK=TEMPPEAK+pickedup
			SKIP
			IF DOW(date)=2
				EXIT
			endif
		enddo

		IF temppeak>gsuspeak
			STORE temppeak TO GSUSPEAK
		ENDIF
	ENDDO

	STORE tpeak/tavg TO m.PTA
	STORE DTOC(gpeakstart)+'-'+DTOC(gpeakend) TO m.SUSDATES
	STORE gsuspeak/5 TO m.suspeak
	STORE ((gsuspeak/5)/tavg) TO m.SUSPTA

	INSERT INTO temp5 FROM memvar
	SELECT temp3
EndDo
			

SELECT temp5
locate
STORE 12 TO tnum	
osheet1=oworkbook.worksheets[1]
DO WHILE !EOF()
	IF tt>0
		STORE ALLTRIM(STR(tnum)) TO anum

		osheet1.range("A"+anum).value = xacctname
		osheet1.range("B"+anum).value = TT
		osheet1.range("D"+anum).value = tavg
		osheet1.range("F"+anum).value = tpeak
		osheet1.range("H"+anum).value = peakday
		osheet1.range("J"+anum).value = pta
		osheet1.range("L"+anum).value = suspeak
		osheet1.range("N"+anum).value = susdates
		osheet1.range("P"+anum).value = suspta
	endif

	SKIP
	TNUM=TNUM+1
	STORE TNUM TO SHEET3ENDROW
ENDDO

SELECT temp
SUM pickedup TO TT
STORE TT/totnumdays TO TAVG
LOCATE
STORE 0 TO TPEAK
STORE DATE() TO peakday
DO WHILE !EOF()
	IF pickedup>TPEAK
		STORE pickedup TO TPEAK
		STORE date TO peakday
	ENDIF
	SKIP
enddo

locate
STORE 0 TO GSUSPEAK
STORE DATE() TO GPEAKSTART,GPEAKEND

DO WHILE date<GPEAKSTART+6 AND !EOF()
	IF DOW(date)=2 
		EXIT
	ENDIF
	SKIP
ENDDO

STORE RECNO() TO zREC
Locate

SUM pickedup TO GSUSPEAK WHILE RECNO()<=zREC
DO WHILE !EOF()
	STORE date TO TDATE,GPEAKSTART
	STORE date+6 TO GPEAKEND
	STORE 0 TO TEMPPEAK
	DO WHILE date<(TDATE+7) and !EOF() 
		TEMPPEAK=TEMPPEAK+pickedup
		SKIP
		IF DOW(date)=2
			EXIT
		endif
	enddo

	IF temppeak>gsuspeak
		STORE temppeak TO GSUSPEAK
	ENDIF
ENDDO

STORE ALLTRIM(STR(tnum)) TO anum	

osheet1.range("A"+anum).value = "Totals"
osheet1.range("B"+anum).value = TT
osheet1.range("D"+anum).value = tavg
osheet1.range("F"+anum).value = tpeak
osheet1.range("H"+anum).value = peakday
osheet1.range("J"+anum).value = TPEAK/Tavg
osheet1.range("L"+anum).value = GSUSpeak/5
osheet1.range("N"+anum).value = DTOC(gpeakstart)+'-'+DTOC(gpeakend)
osheet1.range("P"+anum).value = ((gsuspeak/5)/TAVG)

*End of Page One
	
*Now the monthly recap
STORE 11 TO tnum
SELECT temp2
locate
DO WHILE !EOF()
	STORE MONTHYR TO TMO
	DO WHILE MONTHYR=tmo AND !EOF()
		SCATTER MEMVAR
		INSERT INTO temp4 FROM memvar
		SELECT TEMP2
		SKIP
	ENDDO
	SELECT TEMP4
	LOCATE
		
	osheet1=oworkbook.worksheets[2]

	STORE 0 TO totctrs

	DO WHILE !EOF()
	    TOTCTRS=TOTCTRS+PICKEDUP
		STORE ALLTRIM(STR(tnum)) TO anum

		osheet1.range("A"+anum).value = MONTHYR
		osheet1.range("C"+anum).value = acctname
		osheet1.range("E"+anum).value = PICKEDUP
		IF SUBSTR(MONTHYR,1,3)='FEB'
			osheet1.range("G"+anum).value = PICKEDUP/20
		ELSE
			IF SUBSTR(MONTHYR,1,3)='SEP' OR SUBSTR(MONTHYR,1,3)='JUN' OR SUBSTR(MONTHYR,1,3)='APR' OR SUBSTR(MONTHYR,1,3)='NOV'
				osheet1.range("G"+anum).value = PICKEDUP/21
			ELSE
				osheet1.range("G"+anum).value = PICKEDUP/22
			ENDIF
		ENDIF
			
		tnum=tnum+1
		skip
	ENDDO
	STORE ALLTRIM(STR(tnum)) TO anum			

	*TOTALS
	osheet1.range("A"+anum).value = MONTHYR
	osheet1.range("C"+anum).value = "TOTALS"
	osheet1.range("E"+anum).value = TOTCTRS
	IF SUBSTR(MONTHYR,1,3)='FEB'
		osheet1.range("G"+anum).value = TOTCTRS/20
	ELSE
		IF SUBSTR(MONTHYR,1,3)='SEP' OR SUBSTR(MONTHYR,1,3)='JUN' OR SUBSTR(MONTHYR,1,3)='APR' OR SUBSTR(MONTHYR,1,3)='NOV'
			osheet1.range("G"+anum).value = TOTCTRS/21
		ELSE
			osheet1.range("G"+anum).value = TOTCTRS/22
		ENDIF
	ENDIF

	SELECT TEMP4
	LOCATE
	DELETE WHILE !EOF()
	SELECT TEMP2
ENDDO
	
*End of Monthly Page 2

SELECT temp
GO top
STORE 10 TO TNUM

osheet1=oworkbook.worksheets[3]
STORE 0 TO runtotal
STORE 0 TO GPU,GAV,GR
DO WHILE !EOF()
	GPU=GPU+PICKEDUP
	GAV=GAV+AVAIL
	GR=GR+RETURNED
	STORE ALLTRIM(STR(tnum)) TO anum		
	osheet1.range("A"+anum).value = date
	osheet1.range("C"+anum).value = pickedup
	osheet1.range("E"+anum).value = avail
	osheet1.range("G"+anum).value = returned
	runtotal=runtotal+avail-pickedup
	osheet1.range("I"+anum).value = runtotal
	tnum=tnum+1
	skip
ENDDO

STORE ALLTRIM(STR(tnum)) TO anum
osheet1.range("C"+anum).value = gpu
osheet1.range("E"+anum).value = gav
osheet1.range("G"+anum).value = gr

SELECT temp4
GO top
DELETE while !EOF()

**** Begin Revenue
create cursor temp31 ( ;
	accountid i, ;
	billtoid i, ;
	company c(1), ;
	revacct c(10), ;
	wo_num c(6), ;
	invdate d, ;
	amount n(10,2), ;
	group c(14))
	
Store Iif(allAccounts, "", "arhdrh.customerkey='"+xcustomerkey+"' and ") to cAcctFilter

xsqlexec("select arhdrh.applyto, arhdrh.shiptokey, arhdrh.customerkey, arhdrh.customerponumber, arlinh.locationkey, " + ;
	"arlinh.revenueacctkey, arhdrh.invoicedate, arlinh.unitprice, arlinh.qtyordered " + ;
	"from arhdrh, arlinh where "+cAcctFilter+" arhdrh.transactionnumber=arlinh.documentnumber " + ;
	"and arhdrh.sysdocid=arlinh.sysdocid and between(arhdrh.invoicedate,{"+dtoc(tstart)+"},{"+dtoc(tend)+"})","xdy",,"ar")
	
select applyto, shiptokey, customerkey, customerponumber, locationkey, revenueacctkey, invoicedate, ;
	unitprice*qtyordered as invamt from xdy into cursor xtemp
	
scan for invamt#0
	m.company = left(locationkey,1)
	m.amount = invamt
	m.billtoid = val(customerkey)
	m.wo_num = customerponumber
	m.invdate = invoicedate

	m.revacct = revenueacctkey
	if substr(m.revacct,8,1)#substr(m.revacct,10,1) and substr(m.revacct,8,1)#'0'
		m.revacct=left(m.revacct,9)+substr(m.revacct,8,1)
	endif

	if !empty(shiptokey)
		m.accountid=eval("0x"+shiptokey)
	else
		if isdigit(applyto) or inlist(applyto,"Z","X")
			xsqlexec("select * from invoice where invnum='"+left(applyto,6)+"'",,,"ar")
			m.accountid=invoice.accountid
		else
			loop
		endif
	endif		
	insert into temp31 from memvar
endscan

USE in xTemp

SELECT temp31
INDEX ON invdate TAG INVDT
Locate

STORE 10 TO tnum
STORE 0 TO GQTY,GCUBE,GH,GS,GSP,ginven
DO WHILE !EOF()
	STORE ALLTRIM(STR(tnum)) TO anum
	STORE MONTH(INVdate) TO tmonth
	STORE YEAR(INVdate) TO tyear
	STORE ALLTRIM(STR(tmonth)) TO cmonth
	STORE STR(YEAR(INVdate)) TO cyear
	IF LEN(CMONTH)=1
		STORE "0"+CMONTH TO CMONTH
	ENDIF

	STORE cmonth+'/'+cyear TO cdate
	STORE 0 TO mHANDLING,mSTORAGE,mPROJECTS
	DO WHILE MONTH(INVdate)=tmonth AND YEAR(INVdate)=tyear AND !EOF()
		DO case
		Case toffice='N' AND COMPANY= 'I'
		Case toffice="C" AND (COMPANY= 'W' OR COMPANY= '1' OR COMPANY='2')
		Case toffice='M' AND COMPANY='M'
		Otherwise
			SKIP
			LOOP
		EndCase

		SELECT glchart
		LOCATE FOR code=temp31.revacct
		SELECT temp31
		DO case 
		Case glchart.glcode='05'
			mHANDLING=mHANDLING+amount
			GH=GH+amount
		Case glchart.glcode='29'
			mSTORAGE=mSTORAGE+amount
			GS=GS+amount
		Case glchart.glcode='30'
			mPROJECTS=mPROJECTS+amount
			GSP=GSP+amount
		EndCase
		SKIP
	ENDDO

	SELECT temp
	SET FILTER TO MONTH(date)=TMONTH AND YEAR(date)=TYEAR
	Locate
	STORE 0 TO mqty,mskus,mcube
	DO WHILE MONTH(date)=tmonth AND YEAR(date)=tyear AND !EOF()
		mqty=mqty+pickedup
		SKIP
	ENDDO

	SELECT temp31

	osheet1=oworkbook.worksheets[4]
	osheet1.range("A"+anum).value = cdate
	osheet1.range("C"+anum).value = Mqty
	osheet1.range("E"+anum).value = Iif(Mhandling>0, MHANDLING, osheet1.range("E"+anum).value)
	osheet1.range("G"+anum).value = Iif(mstorage>0, MSTORAGE, osheet1.range("G"+anum).value)
	osheet1.range("I"+anum).value = Iif(mprojects>0, MPROJECTS, osheet1.range("I"+anum).value)
	osheet1.range("K"+anum).value = Iif(mprojects+mstorage+mhandling>0, mprojects+mstorage+mhandling, osheet1.range("K"+anum).value)		
	STORE mhandling/mqty TO MAVG
	osheet1.range("M"+anum).value = Iif(mavg>0, mavg, osheet1.range("M"+anum).value)
	
	SELECT temp31
	tnum=tnum+1
ENDDO

=CloseData()

WAIT CLEAR
oworkBook.save()
oexcel.visible=.t.



**********************
***** Procedures *****
**********************
Procedure closeData

	IF USED("arhdrh")
		USE IN ARHDRH
	ENDIF
	IF USED("arlinh")
		USE IN arlinh
	ENDIF
	IF USED("invoice")
		USE IN invoice
	ENDIF
	IF USED("GLCHART")
		USE IN GLCHART
	ENDIF

	IF USED("mainacct")
		USE in mainAcct
	ENDIF
	IF USED("wolog")
		USE in wolog
	endif
	IF USED("availlog")
		USE in availlog
	endif

	If Used("temp")
	  USE in temp
	EndIf
	If Used("temp2")
	  USE in temp2
	EndIf
	If Used("temp3")
	  USE in temp3
	EndIf
	If Used("temp4")
	  USE in temp4
	ENDIF
	If Used("temp5")
	  USE in temp5
	EndIf
	If Used("temp30")
	  USE in temp30
	EndIf
	If Used("temp31")
	  USE in temp31
	EndIf
	If Used("temploc")
	  USE in temploc
	EndIf
	If Used("xtemp")
	  USE in xtemp
	ENDIF
	If Used("log")
	  USE in log
	ENDIF
	If Used("CARGO")
	  USE in CARGO
	EndIf

Return
