* create a worksheet of 'Non-Accident' type work orders for Mike DiVirgilio
* show WO #, WO Date, Cost Center, $, comments

*LPARAMETER tdStartDate, tdEndDate

#DEFINE CRLF CHR(13) + CHR(10)
#DEFINE HDECS 2
#DEFINE HOURLY_RATE 60
#DEFINE xlEdgeBottom 9
#DEFINE xlContinuous 1
#DEFINE xlMedium -4138


SET SAFETY OFF
SET DELETED ON
SET STRICTDATE TO 0

LOCAL lnRow, lcRow, lcReportPeriod, lcFiletoSaveAs, lcSpreadsheetTemplate
LOCAL oExcel, oWorkbook, oWorksheet, lcErr, lcTitle, loError, lnRate
LOCAL 	lcStartRow, lcEndRow, lcTotalsRow

TRY
	* make these visible in called procs
	PRIVATE pdStartDate, pdEndDate

	*pdStartDate = tdStartDate
	*pdEndDate = tdEndDate
	pdStartDate = {^2013-07-01}
	pdEndDate   = {^2014-06-30}

	IF (MONTH(pdStartDate) = MONTH(pdEndDate)) AND (YEAR(pdStartDate) = YEAR(pdEndDate)) THEN
		lcReportPeriod = CMONTH(pdStartDate) + "_" + TRANSFORM(YEAR(pdStartDate))
	ELSE
		lcReportPeriod = STRTRAN(DTOC(pdStartDate),"/","-") + "_thru_" + STRTRAN(DTOC(pdEndDate),"/","-")
	ENDIF

	lcSpreadsheetTemplate = "F:\SHOP\SHDATA\NON-ACCIDENT_RPT_TEMPLATE1.XLS"

	lcFiletoSaveAs = "F:\SHOP\AllocationReports\Shop_Non-Accident_Report_for_" + lcReportPeriod + ".XLS"
	lcTitle = "Non-Accident Report Spreadsheet for " + lcReportPeriod

	*!*		IF NOT USED('ordhdr')
	*!*			USE SH!ordhdr IN 0
	*!*		ENDIF
	*!*
	*!*		IF NOT USED('vehmast')
	*!*			USE SH!vehmast IN 0
	*!*		ENDIF

	*!*		IF NOT USED('trucks')
	*!*			USE SH!trucks IN 0
	*!*		ENDIF

	*!*		IF NOT USED('trailer')
	*!*			USE SH!trailer IN 0
	*!*		ENDIF

	IF NOT USED('CHASLIST')
		USE F:\SHOP\SHDATA\CHASLIST IN 0
	ENDIF

	IF NOT USED('ordhdr')
		USE F:\SHOP\SHDATA\ordhdr IN 0
	ENDIF

	IF NOT USED('vehmast')
		USE F:\SHOP\SHDATA\vehmast IN 0
	ENDIF

	IF NOT USED('trucks')
		USE F:\SHOP\SHDATA\trucks IN 0
	ENDIF

	IF NOT USED('trailer')
		USE F:\SHOP\SHDATA\trailer IN 0
	ENDIF

	IF USED('CURORDHDR') THEN
		USE IN CURORDHDR
	ENDIF

	IF USED('CURORDHDRPRE') THEN
		USE IN CURORDHDRPRE
	ENDIF

	IF USED('CURINSIDE') THEN
		USE IN CURINSIDE
	ENDIF

	IF USED('CUROUTSIDE') THEN
		USE IN CUROUTSIDE
	ENDIF

	IF USED('CURINCIDENT') THEN
		USE IN CURINCIDENT
	ENDIF

	* create cursor of selected order header records, with addtional fields for RATE & LABRCOST
	SELECT *, 0000.00 AS RATE, 000000.00 AS LABRCOST, "  " AS DIVISION ;
		FROM ordhdr ;
		INTO CURSOR CURORDHDRPRE ;
		WHERE (crdate >= pdStartDate) AND (crdate <= pdEndDate) ;
		AND (TYPE <> 'BLDG') ;
		AND (REPCAUSE <> 'A') ;
		AND (NOT EMPTY(ORDNO)) ;
		ORDER BY crdate ;
		READWRITE

	IF (NOT USED('CURORDHDRPRE')) OR EOF('CURORDHDRPRE') THEN
		=MESSAGEBOX("No non-accident data was found")
	ELSE

		* POPULATE RATE BY CRDATE AND CALC LABOR COST FOR EACH WO REC
		SELECT CURORDHDRPRE
		SCAN
			lnRate = GetHourlyRateByCRDate( CURORDHDRPRE.crdate )
			REPLACE CURORDHDRPRE.RATE WITH lnRate, CURORDHDRPRE.LABRCOST WITH ( CURORDHDRPRE.tothours * lnRate )
		ENDSCAN

		SELECT CURORDHDRPRE
		SCAN

			WAIT WINDOW "checking Vehicle #" + TRANSFORM(CURORDHDRPRE.vehno) NOWAIT

			DO CASE
				CASE CURORDHDRPRE.TYPE == "TRUCK"
					* check trucks first

					SELECT trucks
					LOCATE FOR ALLTRIM(truck_num) == ALLTRIM(CURORDHDRPRE.vehno)
					IF FOUND() THEN
						REPLACE CURORDHDRPRE.DIVISION WITH trucks.DIVISION IN CURORDHDRPRE
						SELECT trucks
						LOCATE
						LOOP
					ENDIF

					SELECT trailer
					LOCATE FOR ALLTRIM(trail_num) == ALLTRIM(CURORDHDRPRE.vehno)
					IF FOUND() THEN
						REPLACE CURORDHDRPRE.DIVISION WITH trailer.DIVISION IN CURORDHDRPRE
						SELECT trailer
						LOCATE
						LOOP
					ENDIF

					SELECT vehmast
					LOCATE FOR ALLTRIM(vehno) == ALLTRIM(CURORDHDRPRE.vehno)
					IF FOUND() THEN
						REPLACE CURORDHDRPRE.DIVISION WITH vehmast.DIVISION IN CURORDHDRPRE
						SELECT vehmast
						LOCATE
						LOOP
					ENDIF

					SELECT CHASLIST
					LOCATE FOR ALLTRIM(CHASSIS) == ALLTRIM(CURORDHDRPRE.vehno)
					IF FOUND() THEN
						REPLACE CURORDHDRPRE.DIVISION WITH CHASLIST.DIVISION IN CURORDHDRPRE
						SELECT CHASLIST
						LOCATE
						LOOP
					ENDIF

				CASE CURORDHDRPRE.TYPE == "TRAILER"
					* check TRAILERS first

					SELECT trailer
					LOCATE FOR ALLTRIM(trail_num) == ALLTRIM(CURORDHDRPRE.vehno)
					IF FOUND() THEN
						REPLACE CURORDHDRPRE.DIVISION WITH trailer.DIVISION IN CURORDHDRPRE
						SELECT trailer
						LOCATE
						LOOP
					ENDIF

					SELECT trucks
					LOCATE FOR ALLTRIM(truck_num) == ALLTRIM(CURORDHDRPRE.vehno)
					IF FOUND() THEN
						REPLACE CURORDHDRPRE.DIVISION WITH trucks.DIVISION IN CURORDHDRPRE
						SELECT trucks
						LOCATE
						LOOP
					ENDIF

					SELECT vehmast
					LOCATE FOR ALLTRIM(vehno) == ALLTRIM(CURORDHDRPRE.vehno)
					IF FOUND() THEN
						REPLACE CURORDHDRPRE.DIVISION WITH vehmast.DIVISION IN CURORDHDRPRE
						SELECT vehmast
						LOCATE
						LOOP
					ENDIF

					SELECT CHASLIST
					LOCATE FOR ALLTRIM(CHASSIS) == ALLTRIM(CURORDHDRPRE.vehno)
					IF FOUND() THEN
						REPLACE CURORDHDRPRE.DIVISION WITH CHASLIST.DIVISION IN CURORDHDRPRE
						SELECT CHASLIST
						LOCATE
						LOOP
					ENDIF

				CASE CURORDHDRPRE.TYPE == "MISC"
					* check vehmast first

					SELECT vehmast
					LOCATE FOR ALLTRIM(vehno) == ALLTRIM(CURORDHDRPRE.vehno)
					IF FOUND() THEN
						REPLACE CURORDHDRPRE.DIVISION WITH vehmast.DIVISION IN CURORDHDRPRE
						SELECT vehmast
						LOCATE
						LOOP
					ENDIF

					SELECT trucks
					LOCATE FOR ALLTRIM(truck_num) == ALLTRIM(CURORDHDRPRE.vehno)
					IF FOUND() THEN
						REPLACE CURORDHDRPRE.DIVISION WITH trucks.DIVISION IN CURORDHDRPRE
						SELECT trucks
						LOCATE
						LOOP
					ENDIF

					SELECT trailer
					LOCATE FOR ALLTRIM(trail_num) == ALLTRIM(CURORDHDRPRE.vehno)
					IF FOUND() THEN
						REPLACE CURORDHDRPRE.DIVISION WITH trailer.DIVISION IN CURORDHDRPRE
						SELECT trailer
						LOCATE
						LOOP
					ENDIF

					SELECT CHASLIST
					LOCATE FOR ALLTRIM(CHASSIS) == ALLTRIM(CURORDHDRPRE.vehno)
					IF FOUND() THEN
						REPLACE CURORDHDRPRE.DIVISION WITH CHASLIST.DIVISION IN CURORDHDRPRE
						SELECT CHASLIST
						LOCATE
						LOOP
					ENDIF

				OTHERWISE
					* CHASSIS - check chaslist first

					SELECT CHASLIST
					LOCATE FOR ALLTRIM(CHASSIS) == ALLTRIM(CURORDHDRPRE.vehno)
					IF FOUND() THEN
						REPLACE CURORDHDRPRE.DIVISION WITH CHASLIST.DIVISION IN CURORDHDRPRE
						SELECT CHASLIST
						LOCATE
						LOOP
					ENDIF

					SELECT trucks
					LOCATE FOR ALLTRIM(truck_num) == ALLTRIM(CURORDHDRPRE.vehno)
					IF FOUND() THEN
						REPLACE CURORDHDRPRE.DIVISION WITH trucks.DIVISION IN CURORDHDRPRE
						SELECT trucks
						LOCATE
						LOOP
					ENDIF

					SELECT trailer
					LOCATE FOR ALLTRIM(trail_num) == ALLTRIM(CURORDHDRPRE.vehno)
					IF FOUND() THEN
						REPLACE CURORDHDRPRE.DIVISION WITH trailer.DIVISION IN CURORDHDRPRE
						SELECT trailer
						LOCATE
						LOOP
					ENDIF

					SELECT vehmast
					LOCATE FOR ALLTRIM(vehno) == ALLTRIM(CURORDHDRPRE.vehno)
					IF FOUND() THEN
						REPLACE CURORDHDRPRE.DIVISION WITH vehmast.DIVISION IN CURORDHDRPRE
						SELECT vehmast
						LOCATE
						LOOP
					ENDIF

			ENDCASE

			*!*				IF CURORDHDRPRE.TYPE == "TRUCK" THEN
			*!*
			*!*					* check trucks first

			*!*					SELECT trucks
			*!*					LOCATE FOR ALLTRIM(truck_num) == ALLTRIM(CURORDHDRPRE.vehno)
			*!*					IF FOUND() THEN
			*!*						REPLACE CURORDHDRPRE.DIVISION WITH trucks.DIVISION IN CURORDHDRPRE
			*!*						SELECT trucks
			*!*						LOCATE
			*!*						LOOP
			*!*					ENDIF

			*!*					SELECT trailer
			*!*					LOCATE FOR ALLTRIM(trail_num) == ALLTRIM(CURORDHDRPRE.vehno)
			*!*					IF FOUND() THEN
			*!*						REPLACE CURORDHDRPRE.DIVISION WITH trailer.DIVISION IN CURORDHDRPRE
			*!*						SELECT trailer
			*!*						LOCATE
			*!*						LOOP
			*!*					ENDIF

			*!*					SELECT vehmast
			*!*					LOCATE FOR ALLTRIM(vehno) == ALLTRIM(CURORDHDRPRE.vehno)
			*!*					IF FOUND() THEN
			*!*						REPLACE CURORDHDRPRE.DIVISION WITH vehmast.DIVISION IN CURORDHDRPRE
			*!*						SELECT vehmast
			*!*						LOCATE
			*!*						LOOP
			*!*					ENDIF
			*!*
			*!*				ELSE
			*!*
			*!*					* check trailers first

			*!*					SELECT trailer
			*!*					LOCATE FOR ALLTRIM(trail_num) == ALLTRIM(CURORDHDRPRE.vehno)
			*!*					IF FOUND() THEN
			*!*						REPLACE CURORDHDRPRE.DIVISION WITH trailer.DIVISION IN CURORDHDRPRE
			*!*						SELECT trailer
			*!*						LOCATE
			*!*						LOOP
			*!*					ENDIF

			*!*					SELECT trucks
			*!*					LOCATE FOR ALLTRIM(truck_num) == ALLTRIM(CURORDHDRPRE.vehno)
			*!*					IF FOUND() THEN
			*!*						REPLACE CURORDHDRPRE.DIVISION WITH trucks.DIVISION IN CURORDHDRPRE
			*!*						SELECT trucks
			*!*						LOCATE
			*!*						LOOP
			*!*					ENDIF

			*!*					SELECT vehmast
			*!*					LOCATE FOR ALLTRIM(vehno) == ALLTRIM(CURORDHDRPRE.vehno)
			*!*					IF FOUND() THEN
			*!*						REPLACE CURORDHDRPRE.DIVISION WITH vehmast.DIVISION IN CURORDHDRPRE
			*!*						SELECT vehmast
			*!*						LOCATE
			*!*						LOOP
			*!*					ENDIF
			*!*
			*!*				ENDIF && CURORDHDRPRE.TYPE == "TRUCK"

		ENDSCAN
		
		WAIT CLEAR

		SELECT * FROM CURORDHDRPRE INTO CURSOR CURORDHDR ORDER BY DIVISION, crdate READWRITE

		SELECT * FROM CURORDHDRPRE INTO CURSOR CURINCIDENT WHERE (LCHARGINC OR NOT EMPTY(CIDRIVER)) ORDER BY crdate READWRITE

		SELECT DIVISION, SUM(LABRCOST + TOTPCOST + OUTSCOST) AS TOTCOST FROM CURORDHDRPRE INTO CURSOR CURINSIDE WHERE NOT ("OUTSIDE VENDOR" $ UPPER(MECHNAME)) GROUP BY 1 ORDER BY 1 READWRITE

		SELECT DIVISION, SUM(LABRCOST + TOTPCOST + OUTSCOST) AS TOTCOST FROM CURORDHDRPRE INTO CURSOR CUROUTSIDE WHERE ("OUTSIDE VENDOR" $ UPPER(MECHNAME)) GROUP BY 1 ORDER BY 1 READWRITE

		oExcel = CREATEOBJECT("excel.application")
		oExcel.displayalerts = .F.
		oExcel.VISIBLE = .F.

		oWorkbook = oExcel.workbooks.OPEN(lcSpreadsheetTemplate)
		oWorkbook.SAVEAS(lcFiletoSaveAs)

		oWorksheet = oWorkbook.Worksheets[1]
		oWorksheet.RANGE("A3","L1000").clearcontents()
		lnRow = 2
		lcStartRow = '3'

		oWorksheet.RANGE("A1").VALUE = lcTitle + " (inside costs)"

		SELECT CURORDHDR
		*SCAN FOR (OUTSCOST = 0.00)
		SCAN FOR NOT ("OUTSIDE VENDOR" $ UPPER(MECHNAME))

			lnRow = lnRow + 1
			lcRow = ALLTRIM(STR(lnRow))

			oWorksheet.RANGE("A"+lcRow).VALUE = CURORDHDR.ORDNO
			oWorksheet.RANGE("B"+lcRow).VALUE = CURORDHDR.crdate
			oWorksheet.RANGE("C"+lcRow).VALUE = CURORDHDR.TYPE
			oWorksheet.RANGE("D"+lcRow).VALUE = CURORDHDR.vehno
			oWorksheet.RANGE("E"+lcRow).VALUE = CURORDHDR.MAINTTYPE
			oWorksheet.RANGE("F"+lcRow).VALUE = CURORDHDR.REPCAUSE
			oWorksheet.RANGE("G"+lcRow).VALUE = CURORDHDR.MECHNAME
			oWorksheet.RANGE("H"+lcRow).VALUE = CURORDHDR.tothours
			oWorksheet.RANGE("I"+lcRow).VALUE = CURORDHDR.RATE
			oWorksheet.RANGE("J"+lcRow).VALUE = CURORDHDR.LABRCOST
			oWorksheet.RANGE("K"+lcRow).VALUE = CURORDHDR.TOTPCOST
			oWorksheet.RANGE("L"+lcRow).VALUE = CURORDHDR.OUTSCOST
			oWorksheet.RANGE("M"+lcRow).VALUE = "'" + CURORDHDR.DIVISION
			oWorksheet.RANGE("N"+lcRow).VALUE = "=SUM(J"+lcRow + "..L"+lcRow+")"
			oWorksheet.RANGE("O"+lcRow).VALUE = CURORDHDR.REMARKS
			oWorksheet.RANGE("P"+lcRow).VALUE = CURORDHDR.WORKDESC

		ENDSCAN

		IF lnRow > 2 THEN
			* We did scan records above

			* TOTAL COLUMN N
			lcEndRow = lcRow

			* UNDERLINE cell to be totaled...
			oWorksheet.RANGE("N" + lcEndRow).BORDERS(xlEdgeBottom).LineStyle = xlContinuous
			oWorksheet.RANGE("N" + lcEndRow).BORDERS(xlEdgeBottom).Weight = xlMedium

			lnRow = lnRow + 1
			lcTotalsRow = ALLTRIM(STR(lnRow))

			oWorksheet.RANGE("N" + lcTotalsRow).VALUE = "=SUM(N" + lcStartRow + "..N" + lcEndRow + ")"

		ENDIF && lnRow > 2

		*************************************************************************************************
		* add div summary
		lnRow = 2
		lcStartRow = '3'
		SELECT CURINSIDE
		SCAN

			lnRow = lnRow + 1
			lcRow = ALLTRIM(STR(lnRow))

			oWorksheet.RANGE("R"+lcRow).VALUE = "'" + CURINSIDE.DIVISION
			oWorksheet.RANGE("S"+lcRow).VALUE = CURINSIDE.TOTCOST

		ENDSCAN

		IF lnRow > 2 THEN
			* We did scan records above

			* TOTAL COLUMN S
			lcEndRow = lcRow

			* UNDERLINE cell to be totaled...
			oWorksheet.RANGE("S" + lcEndRow).BORDERS(xlEdgeBottom).LineStyle = xlContinuous
			oWorksheet.RANGE("S" + lcEndRow).BORDERS(xlEdgeBottom).Weight = xlMedium

			lnRow = lnRow + 1
			lcTotalsRow = ALLTRIM(STR(lnRow))

			oWorksheet.RANGE("S" + lcTotalsRow).VALUE = "=SUM(S" + lcStartRow + "..S" + lcEndRow + ")"

		ENDIF && lnRow > 2
		*************************************************************************************************


		oWorksheet = oWorkbook.Worksheets[2]
		oWorksheet.RANGE("A3","L1000").clearcontents()
		lnRow = 2
		lcStartRow = '3'

		oWorksheet.RANGE("A1").VALUE = lcTitle + " (outside costs)"

		SELECT CURORDHDR
		*SCAN FOR (OUTSCOST > 0.00)
		SCAN FOR ("OUTSIDE VENDOR" $ UPPER(MECHNAME))

			lnRow = lnRow + 1
			lcRow = ALLTRIM(STR(lnRow))

			oWorksheet.RANGE("A"+lcRow).VALUE = CURORDHDR.ORDNO
			oWorksheet.RANGE("B"+lcRow).VALUE = CURORDHDR.crdate
			oWorksheet.RANGE("C"+lcRow).VALUE = CURORDHDR.TYPE
			oWorksheet.RANGE("D"+lcRow).VALUE = CURORDHDR.vehno
			oWorksheet.RANGE("E"+lcRow).VALUE = CURORDHDR.MAINTTYPE
			oWorksheet.RANGE("F"+lcRow).VALUE = CURORDHDR.REPCAUSE
			oWorksheet.RANGE("G"+lcRow).VALUE = CURORDHDR.MECHNAME
			oWorksheet.RANGE("H"+lcRow).VALUE = CURORDHDR.tothours
			oWorksheet.RANGE("I"+lcRow).VALUE = CURORDHDR.RATE
			oWorksheet.RANGE("J"+lcRow).VALUE = CURORDHDR.LABRCOST
			oWorksheet.RANGE("K"+lcRow).VALUE = CURORDHDR.TOTPCOST
			oWorksheet.RANGE("L"+lcRow).VALUE = CURORDHDR.OUTSCOST
			oWorksheet.RANGE("M"+lcRow).VALUE = "'" + CURORDHDR.DIVISION
			oWorksheet.RANGE("N"+lcRow).VALUE = "=SUM(J"+lcRow + "..L"+lcRow+")"
			oWorksheet.RANGE("O"+lcRow).VALUE = CURORDHDR.REMARKS
			oWorksheet.RANGE("P"+lcRow).VALUE = CURORDHDR.WORKDESC

		ENDSCAN

		IF lnRow > 2 THEN
			* We did scan records above

			* TOTAL COLUMN N
			lcEndRow = lcRow

			* UNDERLINE cell to be totaled...
			oWorksheet.RANGE("N" + lcEndRow).BORDERS(xlEdgeBottom).LineStyle = xlContinuous
			oWorksheet.RANGE("N" + lcEndRow).BORDERS(xlEdgeBottom).Weight = xlMedium

			lnRow = lnRow + 1
			lcTotalsRow = ALLTRIM(STR(lnRow))

			oWorksheet.RANGE("N" + lcTotalsRow).VALUE = "=SUM(N" + lcStartRow + "..N" + lcEndRow + ")"

		ENDIF && lnRow > 2


		*************************************************************************************************
		* add div summary
		lnRow = 2
		lcStartRow = '3'
		SELECT CUROUTSIDE
		SCAN

			lnRow = lnRow + 1
			lcRow = ALLTRIM(STR(lnRow))

			oWorksheet.RANGE("R"+lcRow).VALUE = "'" + CUROUTSIDE.DIVISION
			oWorksheet.RANGE("S"+lcRow).VALUE = CUROUTSIDE.TOTCOST

		ENDSCAN

		IF lnRow > 2 THEN
			* We did scan records above

			* TOTAL COLUMN S
			lcEndRow = lcRow

			* UNDERLINE cell to be totaled...
			oWorksheet.RANGE("S" + lcEndRow).BORDERS(xlEdgeBottom).LineStyle = xlContinuous
			oWorksheet.RANGE("S" + lcEndRow).BORDERS(xlEdgeBottom).Weight = xlMedium

			lnRow = lnRow + 1
			lcTotalsRow = ALLTRIM(STR(lnRow))

			oWorksheet.RANGE("S" + lcTotalsRow).VALUE = "=SUM(S" + lcStartRow + "..S" + lcEndRow + ")"

		ENDIF && lnRow > 2
		*************************************************************************************************


		* 3rd tab for Chargeable Incident Work Orders

		oWorksheet = oWorkbook.Worksheets[3]
		oWorksheet.RANGE("A3","L1000").clearcontents()
		lnRow = 2
		lcStartRow = '3'

		oWorksheet.RANGE("A1").VALUE = lcTitle + " Chargeable Incidents"

		SELECT CURINCIDENT
		SCAN

			lnRow = lnRow + 1
			lcRow = ALLTRIM(STR(lnRow))

			oWorksheet.RANGE("A"+lcRow).VALUE = CURINCIDENT.ORDNO
			oWorksheet.RANGE("B"+lcRow).VALUE = CURINCIDENT.crdate
			oWorksheet.RANGE("C"+lcRow).VALUE = CURINCIDENT.TYPE
			oWorksheet.RANGE("D"+lcRow).VALUE = CURINCIDENT.vehno
			oWorksheet.RANGE("E"+lcRow).VALUE = CURINCIDENT.MAINTTYPE
			oWorksheet.RANGE("F"+lcRow).VALUE = CURINCIDENT.REPCAUSE
			oWorksheet.RANGE("G"+lcRow).VALUE = CURINCIDENT.MECHNAME
			oWorksheet.RANGE("H"+lcRow).VALUE = CURINCIDENT.tothours
			oWorksheet.RANGE("I"+lcRow).VALUE = CURINCIDENT.RATE
			oWorksheet.RANGE("J"+lcRow).VALUE = CURINCIDENT.LABRCOST
			oWorksheet.RANGE("K"+lcRow).VALUE = CURINCIDENT.TOTPCOST
			oWorksheet.RANGE("L"+lcRow).VALUE = CURINCIDENT.OUTSCOST
			oWorksheet.RANGE("M"+lcRow).VALUE = "'" + CURINCIDENT.DIVISION
			oWorksheet.RANGE("N"+lcRow).VALUE = "=SUM(J"+lcRow + "..L"+lcRow+")"

			oWorksheet.RANGE("O"+lcRow).VALUE = CURINCIDENT.REMARKS
			oWorksheet.RANGE("P"+lcRow).VALUE = CURINCIDENT.WORKDESC
			oWorksheet.RANGE("Q"+lcRow).VALUE = CURINCIDENT.CIDRIVER

		ENDSCAN

		IF lnRow > 2 THEN
			* We did scan records above

			* TOTAL COLUMN N
			lcEndRow = lcRow

			* UNDERLINE cell to be totaled...
			oWorksheet.RANGE("N" + lcEndRow).BORDERS(xlEdgeBottom).LineStyle = xlContinuous
			oWorksheet.RANGE("N" + lcEndRow).BORDERS(xlEdgeBottom).Weight = xlMedium

			lnRow = lnRow + 1
			lcTotalsRow = ALLTRIM(STR(lnRow))

			oWorksheet.RANGE("N" + lcTotalsRow).VALUE = "=SUM(N" + lcStartRow + "..N" + lcEndRow + ")"

		ENDIF && lnRow > 2

		*************************************************************************************************





		MESSAGEBOX("The Accident Report Spreadsheet has been populated in Excel.",0 + 64,lcTitle)
		oWorkbook.SAVE()
		*oExcel.VISIBLE = .T.

		IF TYPE('oExcel') = 'O' AND NOT ISNULL(oExcel) THEN
			oExcel.QUIT()
		ENDIF

	ENDIF  &&  (NOT USED('CURORDHDRPRE')) OR EOF('CURORDHDRPRE')

CATCH TO loError

	lcErr = 'There was an error.' + CRLF
	lcErr = lcErr + 'The Error # is: ' + TRANSFORM(loError.ERRORNO) + CRLF
	lcErr = lcErr + 'The Error Message is: ' + TRANSFORM(loError.MESSAGE) + CRLF
	lcErr = lcErr + 'The Error occurred in Program: ' + TRANSFORM(loError.PROCEDURE) + CRLF
	lcErr = lcErr + 'The Error occurred at line #: ' + TRANSFORM(loError.LINENO)
	MESSAGEBOX(lcErr,0+16,lcTitle)

	*oExcel.VISIBLE = .T.

	IF TYPE('oExcel') = 'O' AND NOT ISNULL(oExcel) THEN
		oExcel.QUIT()
	ENDIF

FINALLY

	IF USED('CURORDHDR') THEN
		USE IN CURORDHDR
	ENDIF

	IF USED('CURORDHDRPRE') THEN
		USE IN CURORDHDRPRE
	ENDIF

	IF USED('CURINSIDE') THEN
		USE IN CURINSIDE
	ENDIF

	IF USED('CUROUTSIDE') THEN
		USE IN CUROUTSIDE
	ENDIF

ENDTRY

RETURN


FUNCTION GetHourlyRateByCRDate
	LPARAMETERS tdCRdate
	DO CASE
		CASE tdCRdate < {^2008-09-01}
			lnRate = 60.00
		OTHERWISE
			lnRate = 70.00
	ENDCASE
	RETURN lnRate
ENDFUNC
