* report OT by HR Dept for a date range for Ken.
* run monthly on the 29th - per Ken
* exe = F:\UTIL\ADPREPORTS\ADPOTBYHRDEPT.EXE

LOCAL loADPOTBYHRDEPT

runack("ADPOTBYHRDEPT")

utilsetup("ADPOTBYHRDEPT")

loADPOTBYHRDEPT = CREATEOBJECT('ADPOTBYHRDEPT')
loADPOTBYHRDEPT.MAIN()

schedupdate()

CLOSE DATABASES ALL
RETURN

#DEFINE LOGIT 1
#DEFINE WAITIT 2
#DEFINE NOWAITIT 4
#DEFINE SENDIT 8
#DEFINE CRLF CHR(13) + CHR(10)
#DEFINE RETURN_DATA_MANDATORY .F.
#DEFINE RETURN_DATA_NOT_MANDATORY .T.
#DEFINE CSVFORMAT 6
#DEFINE WRITE_ONLY_UNBUFFERED 11
#DEFINE COMMA ","

DEFINE CLASS ADPOTBYHRDEPT AS CUSTOM

	cProcessName = 'ADPOTBYHRDEPT'

	lTestMode = .F.  && .F. FOR NORMAL OPERATION, .T. TO SUPPRESS ALL BUT DEVELOPER EMAILS AND CHANGE INPUT TABLE, ETC.

	dToday = DATE()

	lAutoYield = .T.

	cStartTime = TTOC(DATETIME())

	* report properties
	lShowSalaries = .T.

	* connection properties
	nSQLHandle = 0

	* wait window properties
	nWaitWindowTimeout = 2
	lWaitWindowIsOn = .T.

	* logfile properties
	lLoggingIsOn = .T.
	cLogFile = 'F:\UTIL\ADPREPORTS\LOGFILES\ADP_OT_BYHRDEPT_log.txt'

	* INTERNAL email properties
	lSendInternalEmailIsOn = .T.
	cFrom = 'TGF Corporate <fmicorporate@fmiint.com>'
	cSendTo = 'Ken.Kausner@Tollgroup.com, Robert.Agresti@Tollgroup.com'
	cCC = 'mbennett@fmiint.com'
	cSubject = 'OT by HR Department Report for ' + DTOC(DATE())
	cAttach = ''
	cBodyText = ''

	cCOMPUTERNAME = ''
	cUSERNAME = ''


	FUNCTION INIT
		IF NOT DODEFAULT()
			RETURN .F.
		ENDIF
		WITH THIS
			*SET RESOURCE OFF
			CLOSE DATA
			SET CENTURY ON
			SET DATE AMERICAN
			SET HOURS TO 24
			SET ANSI ON
			SET TALK OFF
			SET DELETED ON
			SET CONSOLE OFF
			SET EXCLUSIVE OFF
			SET SAFETY OFF
			SET EXACT OFF
			SET STATUS BAR ON
			SET SYSMENU OFF
			SET ENGINEBEHAVIOR 70
			_VFP.AUTOYIELD = .lAutoYield
			.cCOMPUTERNAME = GETENV("COMPUTERNAME")
			.cUSERNAME = GETENV("USERNAME")
			IF .lTestMode THEN
				.cLogFile = 'F:\UTIL\ADPREPORTS\LOGFILES\ADP_OT_BYHRDEPT_log_TESTMODE.txt'
				.cSendTo = 'mbennett@fmiint.com'
				.cCC = ''
			ENDIF
			.lLoggingIsOn = .lLoggingIsOn AND NOT EMPTY(.cLogFile)
			IF .lLoggingIsOn THEN
				SET ALTERNATE TO (.cLogFile) ADDITIVE
				SET ALTERNATE ON
			ENDIF
		ENDWITH
	ENDFUNC


	FUNCTION DESTROY
		WITH THIS
			IF .lLoggingIsOn  THEN
				SET ALTERNATE OFF
				SET ALTERNATE TO
			ENDIF
		ENDWITH
		DODEFAULT()
	ENDFUNC


	FUNCTION MAIN
		WITH THIS
			LOCAL lnNumberOfErrors, lcSQL, loError, lcFiletoSaveAs, ldStartDate, lcStartDate, ldEndDate, lcEndDate, ldDate,lcFile2 
			
			TRY
				lnNumberOfErrors = 0

				.TrackProgress("Driver Info Report process started....", LOGIT+SENDIT+NOWAITIT)
				.TrackProgress('COMPUTERNAME = ' + .cCOMPUTERNAME, LOGIT+SENDIT)
				.TrackProgress('USERNAME = ' + .cUSERNAME, LOGIT+SENDIT)
				.TrackProgress('PROJECT = ADPOTBYHRDEPT', LOGIT+SENDIT)
				IF .lTestMode THEN
					.TrackProgress('                        TEST MODE                        ', LOGIT+SENDIT)
				ENDIF
				
				* construct dates for prior month
				* we're running this on the 29th, so add one week to push current date into next month, then later logic calcs prior month
				ldDate = DATE() + 7

				* TO FORCE RERUNS OF OLDER MONTHS
				*ldDate = {^2013-02-01}

				ldEndDate = ldDate - DAY(ldDate)  && this is last day of prior month
				ldStartDate = GOMONTH(ldEndDate + 1,-1)   && this is first day of prior month

*!*					*!*	* to force date ranges
*!*					ldStartDate = {^2013-09-01}
*!*					ldEndDate =   {^2013-09-30}
								
				* construct sql filters
				lcStartDate = "DATE'" + TRANSFORM(YEAR(ldStartDate)) + ;
					"-" + PADL(MONTH(ldStartDate),2,"0") + "-" + PADL(DAY(ldStartDate),2,"0") + "'" 

				lcEndDate = "DATE'" + TRANSFORM(YEAR(ldEndDate)) + ;
					"-" + PADL(MONTH(ldEndDate),2,"0") + "-" + PADL(DAY(ldEndDate),2,"0") + "'" 
	
				.cSubject = "OT by HR Department Report for " + TRANSFORM(ldStartDate) + " thru " + TRANSFORM(ldEndDate)				

				* get ss#s from ADP
				OPEN DATABASE F:\UTIL\ADPCONNECTION\CONNECT1

				.nSQLHandle = SQLCONNECT("PCPAYWIN","MBENNETT","VP737A")

				IF .nSQLHandle > 0 THEN

			SET TEXTMERGE ON

			TEXT TO	lcSQL NOSHOW

SELECT
<<"'" + TRANSFORM(ldStartDate) + " thru " + TRANSFORM(ldEndDate) + "'">> AS DATERANGE,
{fn LEFT(A.CHECKVIEWHOMEDEPT,2)} AS DIVISION,
{fn SUBSTRING(A.CHECKVIEWHOMEDEPT,3,4)} AS DEPT,
C.DESCRIPTION AS DEPTDESC,
SUM({FN IFNULL(A.CHECKVIEWREGERNING,0.00)}) AS REGPAY,
SUM({FN IFNULL(A.CHECKVIEWOTEARNING,0.00)}) AS OTPAY
FROM REPORTS.V_CHK_VW_INFO A, REPORTS.V_DEPARTMENT C
WHERE (C.COMPANYCODE(+) = A.COMPANYCODE) AND (C.DEPARTMENT(+) = A.CHECKVIEWHOMEDEPT)
AND A.CHECKVIEWPAYDATE >= <<lcStartDate>> AND A.CHECKVIEWPAYDATE <= <<lcEndDate>>
GROUP BY {fn LEFT(A.CHECKVIEWHOMEDEPT,2)}, {fn SUBSTRING(A.CHECKVIEWHOMEDEPT,3,4)}, C.DESCRIPTION
ORDER BY {fn LEFT(A.CHECKVIEWHOMEDEPT,2)}, {fn SUBSTRING(A.CHECKVIEWHOMEDEPT,3,4)}

			ENDTEXT
			
			SET TEXTMERGE OFF


					IF .lTestMode THEN
						.TrackProgress('lcSQL =' + lcSQL, LOGIT+SENDIT)
					ENDIF

					IF USED('SQLCURSOR1') THEN
						USE IN SQLCURSOR1
					ENDIF
					IF USED('SQLCURSOR2') THEN
						USE IN SQLCURSOR2
					ENDIF
					IF USED('SQLCURSOR3') THEN
						USE IN SQLCURSOR3
					ENDIF
					IF USED('CURCOLLAR') THEN
						USE IN CURCOLLAR
					ENDIF
					
					USE F:\UTIL\ADPREPORTS\DATA\DEPTTRANS.DBF AGAIN IN 0 ALIAS DEPTRANS

					IF .ExecSQL(lcSQL, 'SQLCURSOR1', RETURN_DATA_MANDATORY) THEN

*!*	SELECT SQLCURSOR1
*!*	BROWSE

						lcFiletoSaveAs = "F:\UTIL\ADPREPORTS\REPORTS\OT_BY_HRDEPT_" + STRTRAN(DTOC(ldStartDate),"/","-") + "-" + STRTRAN(DTOC(ldEndDate),"/","-") + ".XLS"

						SELECT A.*, (B.GLACCTNO + "-0000-" + A.DIVISION) AS GLCODE, B.COLLARTYPE ;
							FROM SQLCURSOR1 A ;
							LEFT OUTER JOIN DEPTRANS B ;
							ON B.DEPT = A.DEPT ;
							INTO CURSOR SQLCURSOR2 ;
							ORDER BY A.DIVISION, A.DEPT ;
							READWRITE
							
							
*!*							SELECT DATERANGE, DIVISION, DEPT, DEPTDESC, SUM(OTPAY) AS OTPAY, SUM(REGPAY) AS REGPAY, SUM(GROSSPAY) AS GROSSPAY ;
*!*								FROM SQLCURSOR2 ;
*!*								INTO CURSOR SQLCURSOR3 ;
*!*								GROUP BY 1, 2, 3, 4 ;
*!*								ORDER BY 1, 2, 3 ;
*!*								READWRITE							
							
						
*!*			SELECT SQLCURSOR2 
*!*			BROW
					
						
						
						
						SELECT SQLCURSOR2 
						COPY TO (lcFiletoSaveAs) XL5						


						IF FILE(lcFiletoSaveAs) THEN
							* attach output file to email
							.cAttach = lcFiletoSaveAs
						ELSE
							.TrackProgress('ERROR: unable to email spreadsheet: ' + lcFiletoSaveAs, LOGIT+SENDIT+NOWAITIT)
						ENDIF
						
						
						lcFile2 = "F:\UTIL\ADPREPORTS\REPORTS\OT_BY_COLLAR_" + STRTRAN(DTOC(ldStartDate),"/","-") + "-" + STRTRAN(DTOC(ldEndDate),"/","-") + ".XLS"

						SELECT DATERANGE, COLLARTYPE, SUM(REGPAY) AS REGPAY, SUM(OTPAY) AS OTPAY ;
							FROM SQLCURSOR2 ;
							INTO CURSOR CURCOLLAR ;
							GROUP BY 1, 2 ;
							ORDER BY 1, 2
							
						SELECT CURCOLLAR
						COPY TO (lcFile2) XL5						

						IF FILE(lcFile2) THEN
							* attach output file to email
							IF EMPTY(.cAttach) THEN
								.cAttach = lcFile2
							ELSE
								.cAttach = .cAttach + "," + lcFile2
							ENDIF
						ELSE
							.TrackProgress('ERROR: unable to email spreadsheet: ' + lcFile2, LOGIT+SENDIT+NOWAITIT)
						ENDIF
					
						CLOSE DATABASES ALL
					ENDIF  &&  .ExecSQL(lcSQL, 'SQLCURSOR1', RETURN_DATA_MANDATORY)

					.TrackProgress("OT by HR Department Report process ended normally.", LOGIT+SENDIT+NOWAITIT)
				ELSE
					* connection error
					.TrackProgress('Unable to connect to ADP Payroll System.', LOGIT+SENDIT)
				ENDIF   &&  .nSQLHandle > 0

			CATCH TO loError

				.TrackProgress('There was an error.',LOGIT+SENDIT)
				.TrackProgress('The Error # is: ' + TRANSFORM(loError.ERRORNO), LOGIT+SENDIT)
				.TrackProgress('The Error Message is: ' + TRANSFORM(loError.MESSAGE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred in Program: ' + TRANSFORM(loError.PROCEDURE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred at line #: ' + TRANSFORM(loError.LINENO), LOGIT+SENDIT)
				lnNumberOfErrors = lnNumberOfErrors + 1
				IF TYPE('oWorkbook') = "O" THEN
					oWorkbook.SAVE()
				ENDIF
				IF TYPE('oExcel') = "O" THEN
					oExcel.QUIT()
				ENDIF
				CLOSE DATABASES ALL

			ENDTRY

			CLOSE DATA
			WAIT CLEAR
			***************** INTERNAL email results ******************************
			.TrackProgress('About to send status email.',LOGIT)
			.TrackProgress('The logfile is: ' + .cLogFile, SENDIT)
			.TrackProgress("OT by HR Department Report process started: " + .cStartTime, LOGIT+SENDIT)
			.TrackProgress("OT by HR Department Report process finished: " + TTOC(DATETIME()), LOGIT+SENDIT)

			IF .lSendInternalEmailIsOn THEN
				DO FORM dartmail2 WITH .cSendTo,.cFrom,.cSubject,.cCC,.cAttach,.cBodyText,"A"
				.TrackProgress('Sent status email.',LOGIT)
			ELSE
				.TrackProgress('Did not send status email: THIS.lSendInternalEmailIsOn = FALSE.',LOGIT)
			ENDIF

		ENDWITH
		RETURN
	ENDFUNC && main


	FUNCTION ExecSQL
		LPARAMETERS tcSQL, tcCursorName, tlNoDataReturnedIsOkay
		LOCAL llRetval, lnResult
		WITH THIS
			* close target cursor if it's open
			IF USED(tcCursorName)
				USE IN (tcCursorName)
			ENDIF
			WAIT WINDOW "Fetching remote data; please wait..." NOWAIT
			lnResult = SQLEXEC(.nSQLHandle, tcSQL, tcCursorName)
			llRetval = ( lnResult > 0 )
			IF llRetval THEN
				* see if any data came back
				IF NOT tlNoDataReturnedIsOkay THEN
					IF (NOT USED(tcCursorName)) OR EOF(tcCursorName) THEN
						llRetval = .F.
						.TrackProgress('No data was returned by this query: ' +CRLF + tcSQL, LOGIT+SENDIT)
						.cSendTo = 'mbennett@fmiint.com'
					ENDIF
				ENDIF
			ELSE
				.TrackProgress("Result Code [" + TRANSFORM(lnResult) + "] returned by this query: " + tcSQL, LOGIT+SENDIT)
				.cSendTo = 'mbennett@fmiint.com'
			ENDIF
			WAIT CLEAR
			RETURN llRetval
		ENDWITH
	ENDFUNC


	PROCEDURE TrackProgress
		* do any combination of Wait Window, writing to logfile, and adding to body of email,
		* based on nFlags parameter.
		LPARAMETERS tcExpression, tnFlags
		WITH THIS
			IF BITAND(tnFlags,LOGIT) = LOGIT THEN
				IF .lLoggingIsOn THEN
					?
					? .cProcessName + "  " + TTOC(DATETIME()) + ": " + tcExpression
				ENDIF
			ENDIF
			IF .lWaitWindowIsOn THEN
				IF BITAND(tnFlags,NOWAITIT) = NOWAITIT THEN
					WAIT WINDOW tcExpression NOWAIT
				ENDIF
				IF BITAND(tnFlags,WAITIT) = WAITIT THEN
					WAIT WINDOW tcExpression TIMEOUT .nWaitWindowTimeout
				ENDIF
			ENDIF
			IF BITAND(tnFlags,SENDIT) = SENDIT THEN
				IF .lSendInternalEmailIsOn THEN
					.cBodyText = .cBodyText + tcExpression + CRLF + CRLF
				ENDIF
			ENDIF
		ENDWITH
	ENDPROC  &&  TrackProgress

ENDDEFINE

