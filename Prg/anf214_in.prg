*!* ANF 214 Inbound Processing

PUBLIC c_CntrlNum,c_GrpCntrlNum,cWO_Num,cGroupName,cDivision,cCustName,cFilename,cHAWB
PUBLIC tsendto,tcc,cTrigDate,cTrigTime,cProcType,cArrival,cLocation,lNoRecs,cErrMsg,tFrom,cTransfer,NormalExit
ON ESCAPE CANCEL
CLOSE DATABASES ALL

TRY
	lTesting = .F.
	lOverRideBusy =  lTesting
* lOverRideBusy = .t.

	DO m:\dev\prg\_setvars WITH lTesting

	STORE "" TO cFilename,xfile,cErrMsg
	_screen.Caption = "A&F 214 Inbound Process"
	_screen.WindowState = IIF(lTesting,0,1)
	lIsError = .F.
	NormalExit = .F.
	lEmail = .T.

	cAcctname ="ABERCROMBIE & FITCH"
	nAcctNum = 6576
	tFrom = "Toll Operations <toll-edi-ops@tollgroup.com>"
	cTransfer = "214IN-ANF"

	SELECT 0
	USE F:\edirouting\ftpsetup SHARED
	LOCATE FOR ftpsetup.transfer = cTransfer
	IF ftpsetup.chkbusy AND !lOverRideBusy
		WAIT WINDOW "Process is flagged busy...returning" TIMEOUT 2
		NormalExit = .T.
		THROW
	ELSE
		REPLACE chkbusy WITH .T.,trig_time WITH DATETIME()  FOR ftpsetup.transfer = cTransfer IN ftpsetup
		USE IN ftpsetup
	ENDIF

	SELECT 0
	USE F:\3pl\DATA\mailmaster ALIAS mm
	LOCATE FOR mm.edi_type = "214" AND mm.accountid = 6576
	lcPath = ALLTRIM(mm.basepath)
	lcArchPath = ALLTRIM(mm.archpath)
	lcHoldPath = ALLTRIM(mm.holdpath)
	IF lTesting
		LOCATE
		LOCATE FOR mm.edi_type = "MISC" AND mm.taskname = "GENERAL"
	ENDIF
	lUseAlt = mm.use_alt
	tsendto = IIF(lUseAlt,sendtoalt,sendto)
	tcc = IIF(lUseAlt,ccalt,cc)
	USE IN mm
	tsendtoerr = tsendto
	tccerr = tcc
	tattach = " "
	cWO_Num = ""
	cArrival = ""

	cCustName = "ANF"
	IF lTesting
*		USE F:\anf\DATA\anfdetail.DBF ALIAS manifest
		USE c:\tempfox\temp\manifest ALIAS manifest
	ELSE
		USE F:\wo\wodata\manifest ALIAS manifest
	ENDIF

	IF lTesting
		SET STEP ON
	ENDIF
	CD [&lcPath]
	len1 = ADIR(ary1,'*.edi')
	FOR this214 = 1 TO len1
		cFilename = ALLTRIM(ary1[this214,1])
		xfile = (lcPath+cFilename)
		cFilenameArchive = (lcArchPath+cFilename)
		cFilenameHold = (lcHoldPath+cFilename)
		DO m:\dev\prg\createx856a
		APPEND FROM [&xfile] TYPE DELIMITED WITH CHARACTER "*"
		SELECT x856
*!*			IF lTesting
*!*				LOCATE
*!*				BROWSE
*!*			ENDIF
		LOCATE
		SCAN
			DO CASE
				CASE INLIST(ALLTRIM(x856.segment),"ISA","GS","ST","SE","GE","IEA")
					LOOP

				CASE ALLTRIM(x856.segment) = "B10"
					cHAWB = ALLTRIM(x856.f2)

				CASE ALLTRIM(x856.segment) = "L11" AND ALLTRIM(x856.f2) = "MA"
					nTripNum = INT(VAL((x856.f1)))
					cWO_Num = ALLTRIM(x856.f1)

				CASE ALLTRIM(x856.segment) = "L11" AND ALLTRIM(x856.f2) = "IB"
					cArrival = ALLT(x856.f1)

					SELECT manifest
					IF lTesting
						LOCATE FOR manifest.hawb = cHAWB
					ELSE
						LOCATE FOR manifest.wo_num = nTripNum AND manifest.hawb = cHAWB
					ENDIF
					IF !FOUND()
						lIsError = .T.
						cErrMsg = "HAWB "+cHAWB+" not found in Manifest, File "+cFilename+CHR(13)+"File has been moved to HOLD folder"
						WAIT WINDOW cErrMsg TIMEOUT 2
						COPY FILE [&xfile] TO [&cFilenameHold]
						DELETE FILE [&xfile]
						LOOP
					ELSE
						REPLACE manifest.arrival WITH cArrival IN manifest
						useca("info","wh")
						SELECT INFO
						SCATTER MEMVAR MEMO
						m.infoid = dygenpk("info","WHALL")
						SELECT manifest
						m.wo_num = manifest.wo_num
						m.office = manifest.office
						m.accountid = manifest.accountid
						m.message = "IT Received"
						tu("info")
						IF USED("info")
							USE IN INFO
						ENDIF
					ENDIF
			ENDCASE
		ENDSCAN

		DO goodmail
		IF lTesting
			SET STEP ON
		ENDIF
		COPY FILE [&xfile] TO [&cFilenameArchive]
		DELETE FILE [&xfile]
	ENDFOR

	SELECT 0
	USE F:\edirouting\ftpsetup SHARED
	LOCATE FOR ftpsetup.transfer = cTransfer
	REPLACE chkbusy WITH .F. FOR ftpsetup.transfer = cTransfer IN ftpsetup

	WAIT CLEAR
	WAIT WINDOW "A&F EDI File "+cFilename+" upload complete" AT 20,60 TIMEOUT 2
	NormalExit = .T.

CATCH TO oErr
	IF !NormalExit
		SET STEP ON
		tmessage ="A&F Error processing "+CHR(13)
		IF !EMPTY(cErrMsg)
			tmessage =tmessage+cErrMsg
		ELSE
			tmessage =tmessage+ "Try/CATCH Exeception Message:"+CHR(13)+;
				[  Error: ] + STR(oErr.ERRORNO) +CHR(13)+;
				[  LineNo: ] + STR(oErr.LINENO) +CHR(13)+;
				[  Message: ] + oErr.MESSAGE +CHR(13)+;
				[  Procedure: ] + oErr.PROCEDURE +CHR(13)+;
				[  Details: ] + oErr.DETAILS +CHR(13)+;
				[  StackLevel: ] + STR(oErr.STACKLEVEL) +CHR(13)+;
				[  LineContents: ] + oErr.LINECONTENTS+CHR(13)+;
				[  UserValue: ] + oErr.USERVALUE
		ENDIF
		tsendto = tsendtoerr
		tcc = tccerr
		tsubject = "214 Inbound EDI Poller Error at "+TTOC(DATETIME())
		tattach  = ""
		tFrom    ="Toll EDI Operations <toll-edi-ops@tollgroup.com>"
		DO FORM m:\dev\frm\dartmail2 WITH tsendto,tFrom,tsubject,tcc,tattach,tmessage,"A"
		lEmail = .F.
	ENDIF
FINALLY
	CLOSE DATA ALL
	ON ERROR
ENDTRY

***********************
PROCEDURE goodmail
***********************
	IF lTesting
		tsubject = "A&F *TEST* EDI 214 Upload EDI as of "+TTOC(DATETIME())
		tmessage = "214 file from A&F into Toll arrival "+cArrival+CHR(13)
	ELSE
		tsubject = "A&F EDI 214 Upload EDI as of "+TTOC(DATETIME())
		tmessage = "214 file from A&F into Toll truck WO# "+cWO_Num+", arrival "+cArrival+CHR(13)
	ENDIF

	tmessage = tmessage +" has been uploaded."
	IF lTesting
		tmessage = tmessage + CHR(13)+CHR(13)+"This is a TEST RUN..."
	ENDIF

	IF lEmail
		DO FORM m:\dev\frm\dartmail2 WITH tsendto,tFrom,tsubject,tcc,tattach,tmessage,"A"
	ENDIF

ENDPROC
