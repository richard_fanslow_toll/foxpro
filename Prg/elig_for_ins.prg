* create "Eligible for Insurance" report for HR

* Employees Hired 1/1/08 thru 1/31/08 would be eligible for insurance on 5/1/08  -- email should go out 3rd week in March
* Employees Hired 2/1/08 thru 2/29/08 would be eligible for insurance on 6/1/08  -- email should go out 3rd week in April
* etc.

* The report needs need to be run/emailed the 3rd week of the 2ND month prior to when they are eligible

* create EXE in F:\UTIL\ADPREPORTS\

LOCAL loEligibleForInsurance
loEligibleForInsurance = CREATEOBJECT('EligibleForInsurance')
loEligibleForInsurance.MAIN()
CLOSE DATABASES ALL
RETURN

#DEFINE LOGIT 1
#DEFINE WAITIT 2
#DEFINE NOWAITIT 4
#DEFINE SENDIT 8
#DEFINE CRLF CHR(13) + CHR(10)
#DEFINE RETURN_DATA_MANDATORY .F.
#DEFINE RETURN_DATA_NOT_MANDATORY .T.
#DEFINE CSVFORMAT 6
#DEFINE WRITE_ONLY_UNBUFFERED 11
#DEFINE COMMA ","

DEFINE CLASS EligibleForInsurance AS CUSTOM

	cProcessName = 'EligibleForInsurance'

	lTestMode = .F.  && .F. FOR NORMAL OPERATION, .T. TO SUPPRESS ALL BUT DEVELOPER EMAILS AND CHANGE INPUT TABLE, ETC.

	dToday = DATE()
	*dToday = {^2013-01-15}

	lAutoYield = .T.

	cStartTime = TTOC(DATETIME())
	
	* report properties
	lShowSalaries = .T.

	* connection properties
	nSQLHandle = 0

	* wait window properties
	nWaitWindowTimeout = 2
	lWaitWindowIsOn = .T.

	* logfile properties
	lLoggingIsOn = .T.
	cLogFile = 'F:\UTIL\ADPREPORTS\LOGFILES\ELIG_FOR_INS_REPORT_log.txt'

	* INTERNAL email properties
	lSendInternalEmailIsOn = .T.
	cFrom = 'TGF Corporate <fmicorporate@fmiint.com>'
	cSendTo = 'mariedesaye@fmiint.com'
	cCC = 'lucille.waldrip@tollgroup.com, mbennett@fmiint.com, lauren.klaver@tollgroup.com'
	cSubject = 'Insurance Eligibility Report for ' + DTOC(DATE())
	cAttach = ''
	cBodyText = ''

	cCOMPUTERNAME = ''
	cUSERNAME = ''


	FUNCTION INIT
		IF NOT DODEFAULT()
			RETURN .F.
		ENDIF
		WITH THIS
			*SET RESOURCE OFF
			CLOSE DATA
			SET CENTURY ON
			SET DATE AMERICAN
			SET HOURS TO 24
			SET ANSI ON
			SET TALK OFF
			SET DELETED ON
			SET CONSOLE OFF
			SET EXCLUSIVE OFF
			SET SAFETY OFF
			SET EXACT OFF
			SET STATUS BAR ON
			SET SYSMENU OFF
			SET ENGINEBEHAVIOR 70
			_VFP.AUTOYIELD = .lAutoYield
			.cCOMPUTERNAME = GETENV("COMPUTERNAME")
			.cUSERNAME = GETENV("USERNAME")
			IF .lTestMode THEN
				.cLogFile = 'F:\UTIL\ADPREPORTS\LOGFILES\ELIG_FOR_INS_REPORT_log_TESTMODE.txt'
				.cSendTo = 'Mark Bennett <mbennett@fmiint.com>'
				.cCC = ''
			ENDIF
			.lLoggingIsOn = .lLoggingIsOn AND NOT EMPTY(.cLogFile)
			IF .lLoggingIsOn THEN
				SET ALTERNATE TO (.cLogFile) ADDITIVE
				SET ALTERNATE ON
			ENDIF
		ENDWITH
	ENDFUNC


	FUNCTION DESTROY
		WITH THIS
			IF .lLoggingIsOn  THEN
				SET ALTERNATE OFF
				SET ALTERNATE TO
			ENDIF
		ENDWITH
		DODEFAULT()
	ENDFUNC


	FUNCTION MAIN
		WITH THIS
			LOCAL lnNumberOfErrors, lcSQL, lcSQL2, lcSQL3, loError, ldToday
			LOCAL lcFiletoSaveAs, lcSpreadsheetTemplate, lcWhere
			LOCAL oExcel, oWorkbook, oWorksheet, oWorksheet2, lnRow, lcRow
			LOCAL lnTwoMonthsAgo, lnHireMonthINS, lnHireYearINS, ldEligible, lcSubject
			LOCAL lnFiveMonthsAgo, lnHireMonth401k, lnHireYear401k
			
			TRY
				lnNumberOfErrors = 0

				.TrackProgress('', LOGIT)
				.TrackProgress('', LOGIT)
				.TrackProgress('=========================================================', LOGIT+SENDIT)
				.TrackProgress("Insurance Eligibility REPORT process started....", LOGIT+SENDIT+NOWAITIT)
				.TrackProgress('COMPUTERNAME = ' + .cCOMPUTERNAME, LOGIT+SENDIT)
				.TrackProgress('USERNAME = ' + .cUSERNAME, LOGIT+SENDIT)
				.TrackProgress('=========================================================', LOGIT+SENDIT)
				IF .lTestMode THEN
					.TrackProgress('                        TEST MODE                        ', LOGIT+SENDIT)
					.TrackProgress('=========================================================', LOGIT+SENDIT)
				ENDIF
				.TrackProgress('', LOGIT)

				ldToday = .dToday
				
				ldEligible = GOMONTH(( ldToday - DAY(ldToday) + 1), 2)
				.cSubject = "Employees Eligible for Insurance/401k on " + TRANSFORM(ldEligible)
				
				* determine hire month/year we want to report on
				lnTwoMonthsAgo = GOMONTH(ldToday,-2)
				lnFiveMonthsAgo = GOMONTH(ldToday,-5)
				
				lnHireMonthINS = MONTH(lnTwoMonthsAgo)				
				lnHireYearINS = YEAR(lnTwoMonthsAgo)				

				lnHireMonth401k = MONTH(lnFiveMonthsAgo)				
				lnHireYear401k = YEAR(lnFiveMonthsAgo)				

				* get ss#s from ADP
				OPEN DATABASE F:\UTIL\ADPCONNECTION\CONNECT1

				.nSQLHandle = SQLCONNECT("PCPAYWIN","MBENNETT","VP737A")

				IF .nSQLHandle > 0 THEN
				
					lcWhere = ""					

					lcSQL = ;
						" SELECT " + ;
						" A.COMPANYCODE AS ADP_COMP, " + ;
						" {fn LEFT(A.HOMEDEPARTMENT,2)} AS DIVISION, " + ;
						" {fn SUBSTRING(A.HOMEDEPARTMENT,3,4)} AS DEPARTMENT, " + ;
						" A.NAME, " + ;
						" A.FILE# AS FILE_NUM, " + ;
						" A.HIREDATE, " + ;
						" A.GENDER, " + ;
						" A.STREETLINE1, " + ;
						" {FN IFNULL(A.STREETLINE2,' ')} AS STREETLINE2, " + ;
						" A.CITY, " + ;
						" A.ZIPCODE, " + ;
						" A.STATE, " + ;
						" A.BIRTHDATE, " + ;
						" {FN IFNULL(A.RATE1AMT,0.00)} AS RATE1AMT, " + ;
						" A.SOCIALSECURITY# AS SS_NUM, " + ;
						" A.EEOCJOBCLASS AS JOBCLASS, " + ;
						" B.DESCRIPTION_TX AS JOBDESC, " + ;
						" {FN IFNULL(a.ANNUALSALARY,0.00)} AS ANNSALARY " + ;
						" FROM REPORTS.V_EMPLOYEE A, PCPAYSYS.T_CO_JOB_CLASS B " + ;
						" WHERE (B.CO_C(+) = A.COMPANYCODE) AND (B.EEOC_JOB_CLASS_C(+) = A.EEOCJOBCLASS) " + ;
						" AND A.COMPANYCODE IN ('E87','E88','E89') " + ;
						" AND A.STATUS <> 'T' " + ;
						lcWhere

					IF .lTestMode THEN
						.TrackProgress('                        TEST MODE                        ', LOGIT+SENDIT)
						.TrackProgress('lcSQL =' + lcSQL, LOGIT+SENDIT)
					ENDIF

					IF USED('SQLCURSOR1') THEN
						USE IN SQLCURSOR1
					ENDIF
					IF USED('SQLCURSORINS') THEN
						USE IN SQLCURSORINS
					ENDIF
					IF USED('SQLCURSOR401K') THEN
						USE IN SQLCURSOR401K
					ENDIF

					IF .ExecSQL(lcSQL, 'SQLCURSOR1', RETURN_DATA_MANDATORY) THEN

						SELECT * ;
							FROM SQLCURSOR1 ;
							INTO CURSOR SQLCURSORINS ;
							WHERE MONTH(HIREDATE) = lnHireMonthINS ;
							AND YEAR(HIREDATE) = lnHireYearINS ;
							ORDER BY HIREDATE

						SELECT * ;
							FROM SQLCURSOR1 ;
							INTO CURSOR SQLCURSOR401K ;
							WHERE MONTH(HIREDATE) = lnHireMonth401K ;
							AND YEAR(HIREDATE) = lnHireYear401K ;
							ORDER BY HIREDATE

						**************************************************************************************************************
						**************************************************************************************************************
						** setup file names

						lcSpreadsheetTemplate = "F:\UTIL\ADPREPORTS\ELIG_FOR_INS_TEMPLATE.XLS"

						lcFiletoSaveAs = "F:\UTIL\ADPREPORTS\REPORTS\Insurance Eligibility Report for " + STRTRAN(DTOC(ldToday),"/","-") + ".XLS"

						**************************************************************************************************************
						**************************************************************************************************************

						IF FILE(lcFiletoSaveAs) THEN
							DELETE FILE (lcFiletoSaveAs)
						ENDIF


						***********************************************************************************
						***********************************************************************************
						** output to Excel spreadsheet
						.TrackProgress('Creating spreadsheet: ' + lcFiletoSaveAs, LOGIT+SENDIT+NOWAITIT)

						oExcel = CREATEOBJECT("excel.application")
						oExcel.VISIBLE = .F.
						oWorkbook = oExcel.workbooks.OPEN(lcSpreadsheetTemplate)
						oWorkbook.SAVEAS(lcFiletoSaveAs)
						
						** INSURANCE LIST IS WORKSHEET 1

						oWorksheet = oWorkbook.Worksheets[1]
						oWorksheet.RANGE("A5","P100").ClearContents()
						lcSubject = "Employees Eligible for Insurance on " + TRANSFORM(ldEligible)
						oWorksheet.RANGE("A1").VALUE = lcSubject

						lnRow = 4

						SELECT SQLCURSORINS												
						SCAN
							lnRow = lnRow + 1
							lcRow = ALLTRIM(STR(lnRow))
							
							oWorksheet.RANGE("A"+lcRow).VALUE = SQLCURSORINS.NAME
							oWorksheet.RANGE("B"+lcRow).VALUE = "'" + SQLCURSORINS.DIVISION
							oWorksheet.RANGE("C"+lcRow).VALUE = SQLCURSORINS.STREETLINE1
							oWorksheet.RANGE("D"+lcRow).VALUE = SQLCURSORINS.STREETLINE2
							oWorksheet.RANGE("E"+lcRow).VALUE = SQLCURSORINS.CITY
							oWorksheet.RANGE("F"+lcRow).VALUE = SQLCURSORINS.STATE
							oWorksheet.RANGE("G"+lcRow).VALUE = "'" + SQLCURSORINS.ZIPCODE
							oWorksheet.RANGE("H"+lcRow).VALUE = "'" + SQLCURSORINS.SS_NUM
							oWorksheet.RANGE("I"+lcRow).VALUE = SQLCURSORINS.BIRTHDATE
							oWorksheet.RANGE("J"+lcRow).VALUE = SQLCURSORINS.HIREDATE
							oWorksheet.RANGE("K"+lcRow).VALUE = SQLCURSORINS.JOBDESC
							oWorksheet.RANGE("L"+lcRow).VALUE = SQLCURSORINS.RATE1AMT
						ENDSCAN

						** 401k LIST IS WORKSHEET 2
						oWorksheet2 = oWorkbook.Worksheets[2]
						oWorksheet2.RANGE("A5","P100").ClearContents()
						lcSubject = "Employees Eligible for 401k on " + TRANSFORM(ldEligible)
						oWorksheet2.RANGE("A1").VALUE = lcSubject

						lnRow = 4

						SELECT SQLCURSOR401k												
						SCAN
							lnRow = lnRow + 1
							lcRow = ALLTRIM(STR(lnRow))
							
							oWorksheet2.RANGE("A"+lcRow).VALUE = SQLCURSOR401k.NAME
							oWorksheet2.RANGE("B"+lcRow).VALUE = "'" + SQLCURSOR401k.DIVISION
							oWorksheet2.RANGE("C"+lcRow).VALUE = SQLCURSOR401k.STREETLINE1
							oWorksheet2.RANGE("D"+lcRow).VALUE = SQLCURSOR401k.STREETLINE2
							oWorksheet2.RANGE("E"+lcRow).VALUE = SQLCURSOR401k.CITY
							oWorksheet2.RANGE("F"+lcRow).VALUE = SQLCURSOR401k.STATE
							oWorksheet2.RANGE("G"+lcRow).VALUE = "'" + SQLCURSOR401k.ZIPCODE
							oWorksheet2.RANGE("H"+lcRow).VALUE = "'" + SQLCURSOR401k.SS_NUM
							oWorksheet2.RANGE("I"+lcRow).VALUE = SQLCURSOR401k.BIRTHDATE
							oWorksheet2.RANGE("J"+lcRow).VALUE = SQLCURSOR401k.HIREDATE
							oWorksheet2.RANGE("K"+lcRow).VALUE = SQLCURSOR401k.JOBDESC
							oWorksheet2.RANGE("L"+lcRow).VALUE = SQLCURSOR401k.RATE1AMT
						ENDSCAN

						* SAVE AND QUIT EXCEL
						oWorkbook.SAVE()
						oExcel.QUIT()

						***********************************************************************************
						***********************************************************************************


						IF FILE(lcFiletoSaveAs) THEN
							* attach output file to email
							.cAttach = lcFiletoSaveAs
							.cBodyText = "See attached Insurance/401k Eligibility report." + ;
								CRLF + CRLF + "Insurance list = Tab 1; 401k list = Tab 2" + ;
								CRLF + CRLF + "(do not reply - this is an automated report)" + ;
								CRLF + CRLF + "<report log follows>" + ;
								CRLF + .cBodyText
						ELSE
							.TrackProgress('ERROR: unable to email spreadsheet: ' + lcFiletoSaveAs, LOGIT+SENDIT+NOWAITIT)
						ENDIF

						CLOSE DATABASES ALL
					ENDIF  &&  .ExecSQL(lcSQL, 'SQLCURSOR1', RETURN_DATA_MANDATORY)

					.TrackProgress("Insurance Eligibility REPORT process ended normally.", LOGIT+SENDIT+NOWAITIT)
				ELSE
					* connection error
					.TrackProgress('Unable to connect to ADP Payroll System.', LOGIT+SENDIT)
				ENDIF   &&  .nSQLHandle > 0

			CATCH TO loError

				.TrackProgress('There was an error.',LOGIT+SENDIT)
				.TrackProgress('The Error # is: ' + TRANSFORM(loError.ErrorNo), LOGIT+SENDIT)
				.TrackProgress('The Error Message is: ' + TRANSFORM(loError.MESSAGE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred in Program: ' + TRANSFORM(loError.PROCEDURE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred at line #: ' + TRANSFORM(loError.LINENO), LOGIT+SENDIT)
				lnNumberOfErrors = lnNumberOfErrors + 1
				IF TYPE('oWorkbook') = "O" THEN
					oWorkbook.SAVE()
				ENDIF
				IF TYPE('oExcel') = "O" THEN
					oExcel.QUIT()
				ENDIF
				CLOSE DATABASES ALL

			ENDTRY

			CLOSE DATA
			WAIT CLEAR
			***************** INTERNAL email results ******************************
			.TrackProgress('About to send status email.',LOGIT)
			.TrackProgress('The logfile is: ' + .cLogFile, SENDIT)
			.TrackProgress("Insurance Eligibility REPORT process started: " + .cStartTime, LOGIT+SENDIT)
			.TrackProgress("Insurance Eligibility REPORT process finished: " + TTOC(DATETIME()), LOGIT+SENDIT)

			IF .lSendInternalEmailIsOn THEN
				DO FORM dartmail2 WITH .cSendTo,.cFrom,.cSubject,.cCC,.cAttach,.cBodyText,"A"
				.TrackProgress('Sent status email.',LOGIT)
			ELSE
				.TrackProgress('Did not send status email: THIS.lSendInternalEmailIsOn = FALSE.',LOGIT)
			ENDIF

		ENDWITH
		RETURN
	ENDFUNC && main


	FUNCTION ExecSQL
		LPARAMETERS tcSQL, tcCursorName, tlNoDataReturnedIsOkay
		LOCAL llRetval, lnResult
		WITH THIS
			* close target cursor if it's open
			IF USED(tcCursorName)
				USE IN (tcCursorName)
			ENDIF
			WAIT WINDOW "Fetching remote data; please wait..." NOWAIT
			lnResult = sqlexec(.nSQLHandle, tcSQL, tcCursorName)
			llRetval = ( lnResult > 0 )
			IF llRetval THEN
				* see if any data came back
				IF NOT tlNoDataReturnedIsOkay THEN
					IF (NOT USED(tcCursorName)) OR EOF(tcCursorName) THEN
						llRetval = .F.
						.TrackProgress('No data was returned by this query: ' +CRLF + tcSQL, LOGIT+SENDIT)
						.cSendTo = 'Mark Bennett <mbennett@fmiint.com>'
					ENDIF
				ENDIF
			ELSE
				.TrackProgress("Result Code [" + TRANSFORM(lnResult) + "] returned by this query: " + tcSQL, LOGIT+SENDIT)
				.cSendTo = 'Mark Bennett <mbennett@fmiint.com>'
			ENDIF
			WAIT CLEAR
			RETURN llRetval
		ENDWITH
	ENDFUNC


	PROCEDURE TrackProgress
		* do any combination of Wait Window, writing to logfile, and adding to body of email,
		* based on nFlags parameter.
		LPARAMETERS tcExpression, tnFlags
		WITH THIS
			IF BITAND(tnFlags,LOGIT) = LOGIT THEN
				IF .lLoggingIsOn THEN
					?
					? .cProcessName + "  " + TTOC(DATETIME()) + ": " + tcExpression
				ENDIF
			ENDIF
			IF .lWaitWindowIsOn THEN
				IF BITAND(tnFlags,NOWAITIT) = NOWAITIT THEN
					WAIT WINDOW tcExpression NOWAIT
				ENDIF
				IF BITAND(tnFlags,WAITIT) = WAITIT THEN
					WAIT WINDOW tcExpression TIMEOUT .nWaitWindowTimeout
				ENDIF
			ENDIF
			IF BITAND(tnFlags,SENDIT) = SENDIT THEN
				IF .lSendInternalEmailIsOn THEN
					.cBodyText = .cBodyText + tcExpression + CRLF + CRLF
				ENDIF
			ENDIF
		ENDWITH
	ENDPROC  &&  TrackProgress

ENDDEFINE

