Parameters nWO_Num,cStatusCode,dTrig_time

* Do m:\dev\prg\amazon214_create.prg With 1758236,"AJ",ctot("01/30/2017 11:00:00") && out for delivery
* Do m:\dev\prg\amazon214_create.prg With 1758236,"17",ctot("01/30/2017 12:00:00") && estimated delivery time


* Do m:\dev\prg\amazon214_create.prg With 1758236,"X5",ctot("01/30/2017 11:45:00") && arrive at delivery location
* Do m:\dev\prg\amazon214_create.prg With 1758236,"1D1,ctot("01/30/2017 11:45:00") && delivered


Public c_CntrlNum,c_GrpCntrlNum,cWO_Num,cGroupName,cDivision,cCustName,cFilename,div214,cST_CSZ
Public tsendto,tcc,tsendtoerr,tccerr,cFin_Status,lDoCatch,lDoJfilesout,cPrgName

cPrgName =  "AMAZON 214"
cCustName = "Amazon"
*

cOffice ="C"
dTrig_time = Datetime()

lTesting = .T.
lDoJfilesout = !lTesting
lEmail = !lTesting
lUseTestData = .F.
cProcType = ""

Do m:\dev\prg\_setvars With lTesting

If lTesting
  Close Data All
Else
  closefiles()
Endif

cFilename = ""
lFClose = .T.
lIsError = .F.
lDoCatch = .T.
cShip_ref = ""
cST_CSZ = ""
cFin_Status = ""
nFilenum = 0

*ASSERT .f.
Try
  cSystemName = "UNASSIGNED"
  nAcctNum = 6703
  nAcctNum = 6703
  cWO_Num = ""
  lDoError = .F.

  If lTesting

  &&  cStatusCode = "AG"  && del appt req on date  These 3 appear to be made manually in the CARP system WEB site
  &&  cStatusCode = "X9"  && Del date requested
  &&  cStatusCode = "XB"  && Del date conf received

  &&  cStatusCode = "X2"  && ETA arrive at port
  
  && cStatusCode = "X1"  && ATA actual arrive at port date

*!*      cStatusCode = "AJ"  && out for delivery--> picked up in the trucking system  ** close of the manifest
*!*      cStatusCode = "17"  && est delivery date  ** at close of manifest, update appt date and send 214

*!*      cStatusCode = "X5"  && Arrive at delivery date amd time, del_date from local or express
*!*      cStatusCode = "D1"  && ACD actual tme of delivery, del_date from local or express

      cReasonCode = "NS"

*!*     
*!*      cStatusCode = "AJ"
*!*      cOffice = "C"
*!*      nWO_Num = 1758236
*!*      dTrig_time = Datetime()
  Endif

  cWO_Num = Transform(nWO_Num)

  If Used("manifest")
    Use In manifest
  Endif
  Use F:\wo\wodata\manifest In 0 Alias manifest

  If Used("detail")
    Use In Detail
  Endif
  Use F:\wo\wodata\Detail In 0 Alias Detail

  If Used("wolog")
    Use In wolog
  Endif
  Use F:\wo\wodata\wolog In 0 Alias  wolog

  If Seek(nWO_Num,"wolog","wo_num")
    c214Type = "LOCAL"
*    Select hawb,po_num,rcv_qty,weight,cbm,units,trailer,delloc From manifest Where wo_num = nWO_Num Into Cursor csrpos

&& needs to be RCV_QTY at some time

    Select arrival,hawb,po_num,pl_qty,weight,cbm,units,delloc,trailer From manifest Where wo_num = nWO_Num Into Cursor csrpos
    Select csrpos
    Go top
    xTrailer = csrpos.trailer

    If Reccount("csrpos") = 0
      cFin_Status = "NO MANIFEST RECORDS FOR WOLOG "+Alltrim(Transform(nWO_Num))
      Throw
    Endif
  Else  &&6703
    lcquery = [select * from fxwolog where accountid = 1747 and wo_num = ]+Alltrim(Transform(nWO_Num))
    xsqlexec(lcquery,"fxwolog",,"fx")
    If Reccount("fxwolog") =1  &&
      c214Type = "EXPRESS"
      Select hawb,po_num,rcv_qty,weight,cbm,units,trailer,delloc From manifest Where wo_num = nWO_Num Into Cursor csrpos
      If Reccount("csrpos") = 0
        cFin_Status = "NO MANIFEST RECORDS FOR TRIP: "+Alltrim(Transform(nWO_Num))
        Throw
      Endif
    Else
      cFin_Status = "NO VALID WORK ORDER NUMBER"
      Throw
    Endif
  Endif

  If c214Type = "LOCAL"
    xCity = "LONG BEACH"
    xState = "CA"

  Endif

  If c214Type = "EXPRESS"
    xCity = "LONG BEACH"
    xState = "CA"

  Endif

  Dimension thisarray(1)
  cCustName = "AMAZON"
  cMailName = "AMAZON"
  cCustFolder = "AMAZON"
  dtmail = Ttoc(Datetime())
  dt2 = Datetime()
  cCustPrefix = cCustName+"214_"
  dt1 = Ttoc(Datetime(),1)

  Select 0

  Use F:\3pl\Data\mailmaster Alias mm Shared

  Locate
  Locate For mm.edi_type = "MISC" And mm.taskname = "GENERAL"
  tsendtoerr = Iif(mm.use_alt,mm.sendtoalt,mm.sendto)
  tccerr = Iif(mm.use_alt,mm.ccalt,mm.cc)


  Locate For mm.edi_type = "214" And mm.accountid = nAcctNum
  _Screen.Caption = Alltrim(mm.scaption)
  tsendto = Iif(mm.use_alt,mm.sendtoalt,mm.sendto)
  tcc = Iif(mm.use_alt,mm.ccalt,mm.cc)


  tfrom = "TGF Warehouse Operations <fmi-warehouse-ops@fmiint.com>"
  tattach = ""

  cfd = "*"  && Field delimiter
  csegd = "~"  && Segment delimiter
  cterminator = ">"  && Used at end of ISA segment

  csendqual = "01"  && Sender qualifier
  csendid = "TGFH"  && Sender ID code
  crecqual = "ZZ"  && Recip qualifier
  crecid = "AMAZON_KINDLE"   && Recip ID Code

  cDate = Dtos(Date())
  cTruncDate = Right(cDate,6)
  cTruncTime = Substr(Ttoc(Datetime(),1),9,4)
  cfiledate = cDate+cTruncTime
  csendidlong = Padr(csendid,15," ")
  crecidlong = Padr(crecid,15," ")
  cString = ""

  cFilename = Alltrim(mm.holdpath)+("SCS_AMAZON_"+Alltrim(Transform(nWO_Num))+"_"+Alltrim(cStatusCode)+"_"+dt1+".214")
  cFilenameShort = Justfname(cFilename)
  cFilenameArch = (Alltrim(mm.archpath)+cFilenameShort)
  cFilenameOut = (Alltrim(mm.basepath)+cFilenameShort)
  nFilenum = Fcreate(cFilename)

  Wait Clear
  Wait Window "Now creating 214.." Nowait

  Select arrival,hawb From csrpos Group By arrival Into Cursor csrarrival

  nSTCount = 0
  nSegCtr = 0
  nLXNum = 1

  Store "" To cEquipNum,cB10
  cISACode = Iif(lTesting,"T","P")

  Do num_incr_isa
  cISA_Num = Padl(c_CntrlNum,9,"0")

  Store "ISA"+cfd+"00"+cfd+"          "+cfd+"00"+cfd+"          "+cfd+csendqual+cfd+csendidlong+cfd+;
    crecqual+cfd+crecidlong+cfd+cTruncDate+cfd+cTruncTime+cfd+"U"+cfd+"00401"+cfd+;
    cISA_Num+cfd+"0"+cfd+cISACode+cfd+":"+csegd To cString  && +cfd+cterminator
  Do cstringbreak

  Store "GS"+cfd+"QM"+cfd+csendid+cfd+crecid+cfd+cDate+cfd+cTruncTime+cfd+c_CntrlNum+;
    cfd+"X"+cfd+"004010"+csegd To cString
  Do cstringbreak
  Do num_incr_st

  Select csrarrival
  Scan
    nSegCtr = 0
    nLXNum = 1
    create_214(csrarrival.arrival,csrarrival.hawb)
  Endscan

*!* Finish processing
  close214()
  If nFilenum>0
    =Fclose(nFilenum)
  Endif

  Copy File [&cFilename] To [&cFilenameArch]
  If lDoJfilesout
    Copy File &cFilename To &cFilenameOut
    Delete File &cFilename
  Endif

********************************************************************
  Assert .F. Message "At file closure"

  If !lTesting
    Do ediupdate With "214 CREATED",.F.,.F.
  Endif

  If !lTesting
    If !Used("ftpedilog")
      Select 0
      Use F:\edirouting\ftpedilog Alias ftpedilog
      Insert Into ftpedilog (TRANSFER,ftpdate,filename,acct_name,Type) Values ("214-"+cCustName,dt2,cFilename,Upper(cCustName),"214")
      Use In ftpedilog
    Endif
  Endif

  tsubject = "Amazon 214 "+Iif(lTesting,"*TEST*","")+" EDI from TGF as of "+dtmail
  tmessage = "214 ("+cProcType+") EDI Info from TGF-CA, Truck WO# "+cWO_Num+Chr(10)
  tmessage = tmessage + "for "+cMailName+Chr(13)
  tmessage = tmessage +cStatusCode+" has been created."+Chr(10)+"(Filename: "+cFilenameShort+")"
*!*  +CHR(10)+CHR(10)
*!*  tmessage = tmessage + "If you have any questions, please eMail or call 732-750-9000 x217."
  If lTesting
    tmessage = tmessage + Chr(10)+Chr(10)+"This is a TEST RUN..."
  Endif

  If lEmail
    Do Form dartmail2 With tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
  Endif

  Select 0
  Use F:\edirouting\ftpjobs
*  Insert Into ftpjobs (jobname,Userid,jobtime) Values ("214-BIGLOTS","PAULG",Datetime())

  Release All Like c_CntrlNum,c_GrpCntrlNum
  Release All Like nOrigSeq,nOrigGrpSeq
  Wait Clear
  Wait Window cMailName+" 214 "+cProcType+" EDI File output complete" At 20,60 Nowait Timeout 3

  closefiles()

Catch To oErr
  Set Step On
  If lDoCatch
    lEmail = .T.
    If Empty(cFin_Status)
      cFin_Status = "ERRHAND ERROR"
    Endif
*   Do ediupdate With cFin_Status,.T.
*!*      tsubject = cCustName+" Error ("+Transform(oErr.ErrorNo)+") at "+Ttoc(Datetime())
*!*      tattach  = ""
*!*      tsendto  = tsendtoerr
*!*      tmessage = tccerr
    tmessage =""
    tmessage =tmessage+ "Try/CATCH Exeception Message:"+Chr(13)+;
      [  Error: ] + Str(oErr.ErrorNo) +Chr(13)+;
      [  LineNo: ] + Str(oErr.Lineno) +Chr(13)+;
      [  Message: ] + oErr.Message +Chr(13)+;
      [  Procedure: ] + oErr.Procedure +Chr(13)+;
      [  Details: ] + oErr.Details +Chr(13)+;
      [  StackLevel: ] + Str(oErr.StackLevel) +Chr(13)+;
      [  LineContents: ] + oErr.LineContents+Chr(13)+;
      [  UserValue: ] + oErr.UserValue

    tmessage = tmessage+Chr(13)+cFin_Status

    tsubject = "214 EDI Poller Error at "+Ttoc(Datetime())
    tattach  = ""
    tcc=""
    tfrom    ="TGF EDI 214 Poller Operations <fmi-transload-ops@fmiint.com>"
    Do Form dartmail2 With tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
    lEmail = .F.
  Endif
Finally
  Set Hours To 12
  Fclose(nFilenum)
  closefiles()
Endtry

&& END OF MAIN CODE SECTION
****************************************************************************************************************************
Procedure create_214
Parameters whicharrival,whichhbl

*Select * From csrpos Where csrpos.hawb= whichhbl Into Cursor csrhbl

Store "ST"+cfd+"214"+cfd+Padl(c_GrpCntrlNum,9,"0")+csegd To cString
Do cstringbreak
nSTCount = nSTCount + 1
nSegCtr = nSegCtr + 1
cRefID = Allt(wolog.brokref)

Store "B10"+cfd+Alltrim(whichhbl)+cfd+Alltrim(whicharrival)+cfd+"TGFH"+csegd To cString
Do cstringbreak
nSegCtr = nSegCtr + 1

*!*  Select csrpos
*!*  Scan For hawb= whichhbl
*!*    Store "L11"+cfd+Alltrim(csrpos.po_num)+cfd+"PO"+csegd To cString
*!*    Do cstringbreak
*!*    nSegCtr = nSegCtr + 1
*!*  Endscan

Store "L11"+cfd+Alltrim(Transform(nWO_Num))+cfd+"BM"+csegd To cString
Do cstringbreak
nSegCtr = nSegCtr + 1

Store "L11"+cfd+Alltrim(Transform(whichhbl))+cfd+"EQ"+csegd To cString
Do cstringbreak
nSegCtr = nSegCtr + 1

Store "N1"+cfd+"SH"+cfd+"TOLL GLOBAL FORWARDING"+csegd To cString
Do cstringbreak
nSegCtr = nSegCtr + 1

Select csrpos
Locate For arrival= whicharrival

Store "N1"+cfd+"ST"+cfd+"AMAZON DC"+cfd+"ZZ"+cfd+Alltrim(csrpos.delloc)+csegd To cString
Do cstringbreak
nSegCtr = nSegCtr + 1

*!*    Store "G62"+cfd+cStatusCode+cfd+cTruncDate+cfd+"L"+cfd+cTruncTime+csegd To cString
*!*    Do cstringbreak
*!*    nSegCtr = nSegCtr + 1

*!* Now in line loop
Store "LX"+cfd+Alltrim(Str(nLXNum))+csegd To cString  && Size
Do cstringbreak
nSegCtr = nSegCtr + 1
nLXNum = nLXNum + 1

cStatusDate = Left(Ttoc(dTrig_time,1),8)
cStatusTime = Right(Ttoc(dTrig_time,1),6)

Store "AT7"+cfd+cStatusCode+cfd+cReasonCode+cfd+cfd+cfd+cStatusDate+cfd+cStatusTime+cfd+"LT"+csegd To cString
Do cstringbreak
nSegCtr = nSegCtr + 1

Store "MS1"+cfd+"USLAX"+cfd+"CA"+cfd+"US"+csegd To cString
Do cstringbreak
nSegCtr = nSegCtr + 1

*xTrailer = "TRAILER#"&&csrarrival.trailer

Store "MS2"+cfd+"TGFH"+cfd+Alltrim(xTrailer)+csegd To cString
Do cstringbreak
nSegCtr = nSegCtr + 1

Store "L11"+cfd+Alltrim(Transform(nWO_Num))+cfd+"DJ"+cfd+"DLN"+csegd To cString
Do cstringbreak
nSegCtr = nSegCtr + 1


Select Sum(weight) As wt, Sum(cbm) As xcube, Sum(pl_qty) As xqty,Sum(units) as xunits From csrpos Where csrpos.arrival= whicharrival Into Cursor temp

Store "AT8"+cfd+"G"+cfd+"K"+cfd+Alltrim(Transform(temp.wt))+cfd+Alltrim(Transform(temp.xqty))+cfd+Alltrim(Transform(xunits))+cfd+"X"+cfd+Alltrim(Transform(temp.xcube))+csegd To cString
Do cstringbreak
nSegCtr = nSegCtr + 1

Store  "SE"+cfd+Alltrim(Str(nSegCtr+1))+cfd+Padl(c_GrpCntrlNum,9,"0")+csegd To cString
Do cstringbreak

Endproc

****************************
Procedure num_incr_isa
****************************

If !Used("serfile")
  Use ("f:\3pl\data\serial\"+cCustName+"214_serial") In 0 Alias serfile
Endif
Select serfile
If lTesting
  nOrigSeq = serfile.seqnum
  nOrigGrpSeq = serfile.grpseqnum
  lISAFlag = .F.
Endif
nISA_Num = serfile.seqnum
c_CntrlNum = Alltrim(Str(serfile.seqnum))
Replace serfile.seqnum With serfile.seqnum + 1 In serfile
Use In serfile
Select wolog
Endproc

****************************
Procedure num_incr_st
****************************

If !Used("serfile")
  Use ("f:\3pl\data\serial\"+cCustName+"214_serial") In 0 Alias serfile
Endif
Select serfile
If lTesting
  nOrigSeq = serfile.seqnum
  nOrigGrpSeq = serfile.grpseqnum
  lSTFlag = .F.
Endif
c_GrpCntrlNum = Alltrim(Str(serfile.grpseqnum))
Replace serfile.grpseqnum With serfile.grpseqnum + 1 In serfile
Use In serfile
Select wolog
Endproc

****************************
Procedure segmentget
****************************

Parameter thisarray,lcKey,nLength

For i = 1 To nLength
  If i > nLength
    Exit
  Endif
  lnEnd= At("*",thisarray[i])
  If lnEnd > 0
    lcThisKey =Trim(Substr(thisarray[i],1,lnEnd-1))
    If Occurs(lcKey,lcThisKey)>0
      Return Substr(thisarray[i],lnEnd+1)
      i = 1
    Endif
  Endif
Endfor

Return ""

****************************
Procedure close214
****************************

Store  "GE"+cfd+Alltrim(Str(nSTCount))+cfd+c_CntrlNum+csegd To cString
Do cstringbreak

Store  "IEA"+cfd+"1"+cfd+Padl(c_CntrlNum,9,"0")+csegd To cString
Do cstringbreak

Return

****************************
Procedure cstringbreak
****************************
cLen = Len(Alltrim(cString))
*Fputs(nFilenum,cString)
fwrite(nFilenum,cString)
Return



**************************
Procedure ediupdate
**************************
Parameters cFin_Status, lIsError,lFClose
If !lTesting
  Select edi_trigger
  Locate
  If lIsError
    Replace edi_trigger.processed With .T.,edi_trigger.created With .F.,;
      edi_trigger.fin_status With cFin_Status,edi_trigger.errorflag With .T.;
      edi_trigger.when_proc With Datetime() ;
      FOR edi_trigger.wo_num = nWO_Num And edi_trigger.edi_type = "214 "
    If nFilenum>0
      =Fclose(nFilenum)
    Endif
    If File(cFilename)
      Delete File &cFilename
    Endif
    closefiles()
  Else
    Replace edi_trigger.processed With .T.,edi_trigger.created With .T.,proc214 With .T.;
      edi_trigger.when_proc With Datetime(),;
      edi_trigger.fin_status With cStatusCode+" 214 CREATED";
      edi_trigger.errorflag With .F.,file214 With cFilename ;
      FOR edi_trigger.wo_num = nWO_Num And edi_trigger.edi_type = "214 "
  Endif
Endif
Endproc

*********************
Procedure closefiles
*********************
If Used('ftpedilog')
  Use In ftpedilog
Endif
If Used('serfile')
  Use In serfile
Endif
If Used('wolog')
  Use In wolog
Endif
If Used('xref')
  Use In xref
Endif
If Used('ftpjobs')
  Use In ftpjobs
Endif
Endproc
