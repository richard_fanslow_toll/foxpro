*!* Nautica 940

PARAMETERS cOfficeUsed

IF VARTYPE(cOfficeUsed) = "L"
	cOfficeUsed = "M"
ENDIF

CLOSE DATABASES ALL

*!* Set and initialize public variables
PUBLIC xFile,archivefile,cXdate1,dXdate2,cOffice,lTesting,cUseDir,cCustName,nTotPT,cMailName
PUBLIC cDelimiter,EmailCommentStr,LogCommentStr,nAcctNum,cAcctNum,lLoop,lTestImport,lCons
PUBLIC chgdate,ptid,ptctr,nPTQty,lTestUploaddet,cOfficename,cConsignee,cStoreNum,cTotPT,cMod
PUBLIC cPickticket_num_start,cPickticket_num_end,cLoadID,lcPath,cUseFolder,lEmail
PUBLIC tsendto,tcc,lcArchivepath,nXPTQty,NormalExit,cMessage,fa997file,addproc,cTransfer,cISA_Num
PUBLIC tsendtoerr,tccerr,tsendtotest,tcctest,lBrowFiles,lOverridebusy,cFilename,lLoadSQL,nFileName
PUBLIC ARRAY a856(1)

lTesting = .f.
lTestImport = lTesting
lOverridebusy = lTesting
*lOverridebusy = .T.
lBrowFiles = lTesting
lEmail = .t.
SET STEP on

DO m:\dev\prg\_setvars WITH lTesting

nAcctNum = 687
CaptionStr = "Nautica 940 Process"
STORE CaptionStr TO _SCREEN.CAPTION

IF lTesting
	CLOSE DATABASES ALL
	CLEAR
	lTestUploaddet = .T.
ELSE
	lTesting = .F.
	lTestUploaddet = .F.
ENDIF

STORE cOfficeUsed TO cOffice
cMod = cOffice
gOffice = cMod
lLoadSQL = .t.

cOfficename = ICASE(cOffice = "C","California",INLIST(cOffice,"N","I"),"New Jersey","Florida")
STORE "" TO EmailCommentStr,cLoadID,cConsignee
NormalExit = .F.
lCons = .F.
cMessage = " "

TRY
	IF !lTesting
		_SCREEN.WINDOWSTATE=1
	ENDIF
	cCustName = "NAUTICA"
	cMailName = "NAUTICA-KIPLING"
	cAlias = "NAUTICA"

	xFile = ""
	cXdate1 = ""
	dXdate2 = DATE()

	STORE nAcctNum TO Acctnum
	cAcctNum = ALLTRIM(STR(nAcctNum))
	LogCommentStr = ""

	SELECT 0
	USE F:\3pl\DATA\mailmaster ALIAS mm SHARED
	LOCATE FOR (mm.accountid = Acctnum) AND (mm.office= "M") AND mm.acctname = cAlias AND edi_type = "940"
	IF FOUND("mm")
		STORE TRIM(mm.acctname) TO cAccountName
		STORE TRIM(mm.basepath) TO lcPath
		STORE TRIM(mm.archpath) TO lcArchivepath
		STORE TRIM(IIF(mm.use_alt,mm.sendtoalt,mm.sendto)) TO tsendto
		STORE TRIM(IIF(mm.use_alt,mm.ccalt,mm.cc)) TO tcc
		STORE TRIM(mm.scaption) TO thiscaption
		STORE JUSTFNAME(TRIM(mm.fmask)) TO cFilemask
		IF lTesting
			cFilemask = "*.*"
		ENDIF
		STORE mm.CtnAcct TO lCartonAcct
		LOCATE
		LOCATE FOR mm.edi_type = "MISC" AND mm.taskname = "GENERAL"
		STORE TRIM(IIF(mm.use_alt,mm.sendtoalt,mm.sendto)) TO tsendtoerr
		STORE TRIM(IIF(mm.use_alt,mm.ccalt,mm.cc)) TO tccerr
		STORE TRIM(IIF(mm.use_alt,mm.sendtoalt,mm.sendto)) TO tsendtotest
		STORE TRIM(IIF(mm.use_alt,mm.ccalt,mm.cc)) TO tcctest
		USE IN mm
	ELSE
		WAIT WINDOW AT 10,10  "No parameters set for this acct# "+ALLTRIM(STR(Acctnum))+"  ---> Office "+cOffice TIMEOUT 2
		NormalExit = .F.
		THROW
	ENDIF

	IF lTesting
		lcPath = "F:\FTPUSERS\NAUTICA\TEST\940IN\"
		lcArchivepath = "F:\FTPUSERS\NAUTICA\TEST\940IN\ARCHIVE\"
		cUseFolder = "F:\WHP\WHDATA\"
		tsendto = tsendtotest
		tcc = tcctest
	ELSE
		SELECT 0
		USE F:\edirouting\ftpsetup SHARED
		cTransfer = "940-NAUTICA-FL"
		LOCATE FOR ftpsetup.transfer = cTransfer
		IF ftpsetup.chkbusy = .T. AND !lOverridebusy
			WAIT WINDOW "File is processing...exiting" TIMEOUT 2
			NormalExit = .T.
			THROW
		ENDIF
		REPLACE chkbusy WITH .T.,trig_time WITH DATETIME() FOR ftpsetup.transfer = cTransfer
		USE IN ftpsetup

		xReturn = "XXX"
		DO m:\dev\prg\wf_alt WITH cOffice,nAcctNum
		cUseFolder = UPPER(xReturn)
	ENDIF

	WAIT CLEAR
	DO m:\dev\prg\nautica_940Process
	SELECT 0
	USE F:\edirouting\ftpsetup SHARED
	cTransfer = "940-NAUTICA-FL"
	REPLACE chkbusy WITH .F. FOR ftpsetup.transfer = cTransfer
	CLOSE DATABASES ALL

CATCH TO oErr
	IF !NormalExit
		ASSERT .F. MESSAGE "In Error Catch"
		tsubject = cMailName+" 940 Upload Error at "+TTOC(DATETIME())
		tattach  = ""
		tsendto  = tsendtoerr
		tcc = tccerr
		tmessage = cMailName+" 940 Upload ERROR"

		DO CASE
			CASE "MISSING STYLE"$UPPER(cMessage)
				tmessage = tmessage+CHR(13)+cMessage
				SELECT 0
				USE F:\3pl\DATA\mailmaster ALIAS mm
				LOCATE FOR edi_type = "MISC" AND mm.taskname = "GENERAL"
				tcc = IIF(mm.use_alt,mm.ccalt,mm.cc)
				USE IN mm

			CASE !EMPTY(cMessage)
				tmessage = tmessage+CHR(13)+cMessage

			OTHERWISE
				lcSourceMachine = SYS(0)
				lcSourceProgram = SYS(16)
				tmessage =tmessage+ "Try/CATCH Exeception Message:"+CHR(13)+;
					[  Error: ] + STR(oErr.ERRORNO) +CHR(13)+;
					[  Procedure: ] + oErr.PROCEDURE +CHR(13)+;
					[  LineNo: ] + STR(oErr.LINENO) +CHR(13)+;
					[  Message: ] + oErr.MESSAGE +CHR(13)+;
					[  Details: ] + oErr.DETAILS +CHR(13)+;
					[  StackLevel: ] + STR(oErr.STACKLEVEL) +CHR(13)+;
					[  LineContents: ] + oErr.LINECONTENTS+CHR(13)+;
					[  UserValue: ] + oErr.USERVALUE+CHR(13)+;
					[  Computer:  ] +lcSourceMachine+CHR(13)+;
					[  940 file:  ] +xFile+CHR(13)+;
					[  Program:   ] +lcSourceProgram
		ENDCASE

		tattach  = ""
		tfrom    ="TGF EDI Processing Center <edi-ops@fmiint.com>"
		DO FORM m:\dev\frm\dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"

		IF FILE(archivefile)
			DELETE FILE &xFile
		ENDIF
	ELSE
		WAIT WINDOW AT 10,10 "Normal Exit " TIMEOUT 2
	ENDIF
FINALLY
	CLOSE DATABASES ALL
	ON ERROR
ENDTRY

