DO M:\DEV\PRG\_SETVARS WITH .T.
PUBLIC cfilename,cfileoutname,NormalExit,cMailName,tfrom

TRY
	lTesting = .F.
	lOverrideBusy = lTesting
*	lOverrideBusy = .t.
	NormalExit = .F.

	_SCREEN.CAPTION="Moret File Move Process"
	_SCREEN.WINDOWSTATE=IIF(lTesting or lOverrideBusy,2,1)
	
	tfrom = "Toll EDI Operations <toll-edi-ops@tollgroup.com>"


	IF !lTesting
		SELECT 0
		USE F:\edirouting\ftpsetup SHARED
		cTransfer = "MORET-MOVE"
		LOCATE FOR ftpsetup.transfer = cTransfer
		IF ftpsetup.chkbusy = .T. AND !lOverrideBusy
			WAIT WINDOW "File is processing...exiting" TIMEOUT 2
			NormalExit = .T.
			THROW
		ENDIF
		REPLACE chkbusy WITH .T. FOR ftpsetup.transfer = cTransfer
		USE IN ftpsetup
	ENDIF
	cMailName = "Moret"
	ASSERT .F. MESSAGE "In  mail data subroutine"
	SELECT 0
	USE F:\3pl\DATA\mailmaster ALIAS mm SHARED
	LOCATE FOR mm.edi_type = "MISC" AND mm.taskname = "GENERAL"
	IF FOUND()
		STORE TRIM(IIF(mm.use_alt,mm.sendtoalt,mm.sendto)) TO tsendtoerr
		STORE TRIM(IIF(mm.use_alt,mm.ccalt,mm.cc)) TO tccerr
		LOCATE
		LOCATE FOR mm.edi_type = "MISC" AND mm.taskname = "JOETEST"
		STORE TRIM(IIF(mm.use_alt,mm.sendtoalt,mm.sendto)) TO tsendto
		STORE TRIM(IIF(mm.use_alt,mm.ccalt,mm.cc)) TO tcc
		STORE TRIM(IIF(mm.use_alt,mm.sendtoalt,mm.sendto)) TO tsendtotest
		STORE TRIM(IIF(mm.use_alt,mm.ccalt,mm.cc)) TO tcctest
		USE IN mm
	ELSE
		WAIT WINDOW AT 10,10  "No parameters set for this routine: Moret940_move" TIMEOUT 2
		NormalExit = .T.
		THROW
	ENDIF

	cDirIn = "f:\ftpusers\moretgroup\in\"
	CD &cDirIn

	nFound = ADIR(ary1,'*.*')
	IF nFound = 0
		CLOSE DATABASES ALL
		WAIT WINDOW "No 940 files found to process...exiting" TIMEOUT 2
		NormalExit = .t.
		throw
	ENDIF

	len1 = ALEN(ary1,1)

	FOR i = 1 TO len1
		ASSERT .F. MESSAGE "In move loop"
		cfilename = ALLT(ary1[i,1])
		xfile = (cDirIn+cfilename)
		IF "["$cfilename
			cFilename2 = STRTRAN(cfilename,"[","(")
			cFilename2 = STRTRAN(cFilename2,"]",")")
			RENAME &cfilename TO &cFilename2
		ENDIF

		IF UPPER(JUSTEXT(cfilename)) = "TMP"
			LOOP
		ENDIF
		IF "TRANSIT"$UPPER(cfilename)
			LOOP
		ENDIF

		WAIT WINDOW "Processing "+cfilename TIMEOUT 1
		lEDIType = .T. && IIF(UPPER(JUSTEXT(cfilename)) = "EDI",.T.,.F.)
		cFile4 = UPPER(LEFT(cfilename,4))
		cFile3 = LEFT(cfilename,3)

		DO CASE
			CASE INLIST(cFile4,"W753","W754") OR "_753_"$cfilename OR "_754_"$cfilename
				cDirOut = ("F:\ftpusers\moret-sp\754IN\")
				cDirXfer = ("F:\ftpusers\moret\754Translate\")
				cfileoutname = (cDirOut+cfilename)
				cFileXferName = (cDirXfer+cfilename)
				COPY FILE [&xfile] TO [&cFileOutName]
				COPY FILE [&xfile] TO [&cFileXferName]  && Added 08.08.2011 to send 997s for these
				IF FILE(cfileoutname)
					IF FILE(cfilename)
						DELETE FILE [&xfile]
					ENDIF
				ENDIF
				LOOP

			CASE "_997_"$cfilename
				cWhse = ICASE("FMINJ"$UPPER(cfilename),"nj","FMIFL"$UPPER(cfilename),"fl","TOLLO"$UPPER(cfilename),"cr","FMIML"$UPPER(cfilename),"ml","sp")
				cDirOut = ("F:\ftpusers\moret-"+cWhse+"\997IN\")
				cfileoutname = (cDirOut+cfilename)
				COPY FILE [&xfile] TO [&cFileOutName]
				IF FILE(cfileoutname)
					IF FILE(xfile)
						DELETE FILE [&xfile]
					ENDIF
				ENDIF
				STORE "" TO cWhse
				LOOP

			CASE "_943_"$cfilename
				cDirOut = ("F:\ftpusers\moret\943IN\")
				cfileoutname = (cDirOut+cfilename)
				COPY FILE [&xfile] TO [&cFileOutName]
				IF FILE(cfileoutname)
					IF FILE(xfile)
						DELETE FILE [&xfile]
					ENDIF
				ENDIF
				LOOP
			CASE JUSTEXT(cfilename) = "888"
				cDirOut = ("f:\moret\skumaster\")
				cfileoutname = (cDirOut+cfilename)
				COPY FILE [&xfile] TO [&cFileOutName]
				IF FILE(cfileoutname)
					IF FILE(xfile)
						DELETE FILE [&xfile]
					ENDIF
				ENDIF
				LOOP
			OTHERWISE
				IF lEDIType
					DO CASE
						CASE "FMINJ"$UPPER(cfilename)
							cWhse = "nj"
							cLocation = "New Jersey"
						CASE "FMIFL"$UPPER(cfilename)
							cWhse = "fl"
							cLocation = "Miami"
						CASE "TOLLO"$UPPER(cfilename)
							cWhse = "cr"  && Changed per Liana, 12.11.2015
							cLocation = "Carson"
						CASE "FMIML"$UPPER(cfilename)
							cWhse = "ml"
							cLocation = "Mira Loma"
						OTHERWISE
							cWhse = "sp"
							cLocation = "San Pedro"
					ENDCASE
				ELSE
					DO CASE
						CASE SUBSTR(cfilename,5,1) = "3"
							cWhse = "sp"
							cLocation = "San Pedro"
						CASE SUBSTR(cfilename,5,1) = "4"
							cWhse = "fl"
							cLocation = "Miami"
						OTHERWISE
							cWhse = "sp"
							cLocation = "San Pedro"
					ENDCASE
				ENDIF
		ENDCASE

		cDirOut = ("F:\0-picktickets\moret-"+cWhse+"\")
		WAIT WINDOW "File will be moved to folder: "+cLocation NOWAIT
		cfileoutname = (cDirOut+cfilename)
		COPY FILE &xfile TO &cfileoutname
		IF FILE(cfileoutname)
			IF FILE(xfile)
				DELETE FILE [&xfile]
			ENDIF
		ENDIF
	ENDFOR

	WAIT WINDOW "Moret move process complete" TIMEOUT 1
	SELECT 0
	USE F:\edirouting\ftpsetup SHARED
	cTransfer = "MORET-MOVE"
	REPLACE chkbusy WITH .F. FOR ftpsetup.transfer = cTransfer
	WAIT CLEAR

	NormalExit = .T.

CATCH TO oErr
	IF NormalExit = .F.
		ASSERT .F. MESSAGE "In Error Catch"
		SET STEP ON
		tsendto = tsendtoerr
		tcc = tccerr
		tsubject = cMailName+" 940 Transfer Error ("+TRANSFORM(oErr.ERRORNO)+") at "+TTOC(DATETIME())
		tattach  = ""
		tmessage = cMailName+" 940 Transfer Error..... Please fix"
		lcSourceMachine = SYS(0)
		lcSourceProgram = SYS(16)

		tmessage =tmessage+ "Try/CATCH Exeception Message:"+CHR(13)+;
			[  Error: ] + STR(oErr.ERRORNO) +CHR(13)+;
			[  LineNo: ] + STR(oErr.LINENO) +CHR(13)+;
			[  Message: ] + oErr.MESSAGE +CHR(13)+;
			[  Procedure: ] + oErr.PROCEDURE +CHR(13)+;
			[  Details: ] + oErr.DETAILS +CHR(13)+;
			[  StackLevel: ] + STR(oErr.STACKLEVEL) +CHR(13)+;
			[  LineContents: ] + oErr.LINECONTENTS+CHR(13)+;
			[  UserValue: ] + oErr.USERVALUE+CHR(13)+;
			[  Computer:  ] +lcSourceMachine+CHR(13)+;
			[  940 file:  ] +xfile+CHR(13)+;
			[  Program:   ] +lcSourceProgram

		tattach  = ""

		DO FORM M:\DEV\FRM\dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
		IF !lTesting
			COPY FILE &xfile TO &archivefile

			IF FILE(archivefile)
				DELETE FILE [&xFile]
			ENDIF
		ENDIF
	ELSE
		WAIT WINDOW AT 10,10 "Normal Exit " TIMEOUT 2
	ENDIF
FINALLY
	ON ERROR
	CLOSE DATABASES ALL
ENDTRY

