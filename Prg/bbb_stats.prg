
close data all 

For xii = 1 To 2

If xii = 1
  whichwhse ="LOS"
Endif

If xii = 2
  whichwhse ="SDG"
Endif


Create Cursor bbStats (;
 opdate d,;
 inbound n(10),;
 totalout n(10),; 
 store n(10),;
 crossdock  n(10))

 xsqlexec([select arrivedt,Count(1) as qty from bbin where whse = ']+Alltrim(whichwhse)+[' group by arrivedt],"inbound",,"ar") 
 xsqlexec([select closedt,Count(1) as qty from bbtn where whse = ']+Alltrim(whichwhse)+[' and shiptype ='B' group by closedt],"manifest_B",,"ar") 
 xsqlexec([select closedt,Count(1) as qty from bbtn where whse = ']+Alltrim(whichwhse)+[' and shiptype ='S' group by closedt],"Manifest_S",,"ar") 

Select bbstats
For ii = 1 To 90
  Insert Into bbstats (opdate) Values (Date()-(90-ii))
Next 

Select bbstats
Scan
  Select inbound
  Locate For arrivedt = bbstats.opdate
  If Found()
    replace bbstats.inbound With inbound.qty
  Endif
Endscan 

Select bbstats
Scan
  Select manifest_s
  Locate For closedt = bbstats.opdate
  If Found()
    replace bbstats.store With manifest_s.qty
  Endif
Endscan 

Select bbstats
Scan
  Select manifest_b
  Locate For closedt = bbstats.opdate
  If Found()
    replace bbstats.crossdock With manifest_b.qty
  Endif
Endscan 

Select bbstats
Scan
  replace totalout With store+crossdock
endscan


*!*  Select bbstats
*!*  Goto top
*!*  Do While Empty(inbound)
*!*      Delete In bbstats
*!*    Skip 1 In bbstats
*!*  Enddo 


If whichwhse ="LOS"
  filename1 = "h:\fox\LOS_bbstats_"+Dtoc(Datetime(),2)+".xls"
  Export To &filename1 Type xls
Endif

If whichwhse ="SDG"
  filename2 = "h:\fox\SDG_bbstats_"+Dtoc(Datetime(),2)+".xls"
  Export To &filename2 Type xls
Endif

Next xii

tfrom    ="TGF EDI Operations <transload-ops@fmiint.com>"
tsubject = "BBB Mira Loma Daily Carton Stats for "+Dtoc(Date())
tattach =filename1+";"+filename2
tmessage = "See attached file for stats............."+Chr(13)+"Do not reply to this email, it is part of an automated process"+Chr(13)+;
+Chr(13)+"For changes in format and/or content contact Tom Delaney"

tsendto ="bbbedimiraloma@tollgroup.com,tom.delaney@tollgroup.com,steven.sykes@tollgroup.com,scott.schreck@tollgroup.com"
*tsendto ="pgaidis@fmiint.com"
tcc="pgaidis@fmiint.com"
Do Form dartmail2 With tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"

Return

