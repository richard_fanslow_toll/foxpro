*!* mjecomm_order_upload.prg taken from ragbone940_main.prg 
*!* Initial version 2018-02-07; D. Wachs

PUBLIC nAcctNum,cDelimiter,nRun_num,cTranslateOption,cWhichFile,cOffice,cCustname,cPropername,cStorenum,lOK,tsendtopt,tccpt
PUBLIC cFilter,nRunID,lComplete,lTesting,m.UCCNUMBER,lEmail,lTestImport,emailcommentstr,cUseFolder,lBrowfiles,cMailName,cMod
PUBLIC ptid,ptctr,ptqty,xfile,cfilename,nUploadCount,pickticket_num_start,pickticket_num_end,nFileCount,units,cUCCNumber,cISA_Num
PUBLIC lcPath,lcArchivepath,tsendto,tcc,NormalExit,lTestmail,LogCommentStr,lPick,lPrepack,tsendtoerr,tccerr,lImported,lLoadSQL
PUBLIC m.addby,m.adddt,m.addproc,cTransfer,tfrom,tattach,thisfile,tsendtotest,tcctest,lNextDay,lMonday,cForeignStr,lTestPTHIST
PUBLIC cErrMsg,dFileDate,nFileSize
Public coffice, ltesting, ltestimport,gMasterOffice,nAcctNum,gOffice,cMod


PUBLIC ARRAY a856(1)
CLOSE DATABASES ALL
NormalExit = .F.
*ON ERROR THROW

cMailName="Marc Jacobs eComm"
nAcctNum  = 6303
SET STEP ON 
_SCREEN.CAPTION = cMailName

cErrMsg = ""

cOffice ="N"
cMod = "J" 
   
TRY
	lTesting = .f.
	lTestImport = lTesting
	lTestmail = lTesting
	lOverridebusy = lTesting
	lBrowfiles = lTesting
*	lOverridebusy = .t.
*!*		lBrowfiles = .t.

	gMasteroffice= cOffice
	goffice = cMod
	lLoadSQL = .t.

	DO m:\dev\prg\_setvars WITH lTesting
	cCustname = "MJECOMM"

	WAIT WINDOW "At start of MJ eComm OTF processing" nowait
	SELECT 0
	USE F:\edirouting\ftpsetup SHARED
	
	cTransfer = "940-MJ_ECOMM"
	
	IF !lTesting
		LOCATE FOR ftpsetup.transfer = cTransfer AND ftpsetup.TYPE = "GET"
		IF ftpsetup.chkbusy AND !lOverridebusy
			WAIT WINDOW "Process is flagged busy...returning" TIMEOUT 1
			NormalExit = .T.
			THROW
		ELSE
			REPLACE chkbusy WITH .T.,trig_time WITH DATETIME() ;
				FOR ftpsetup.TYPE = "GET" ;
				AND ftpsetup.transfer = cTransfer ;
				IN ftpsetup
			USE IN ftpsetup
		ENDIF
	ENDIF

	lEmail = .T.
	LogCommentStr = ""


	tfrom = "Toll EDI Operations <toll-edi-ops@tollgroup.com>"
	STORE " " TO tattach,tsendto,tcc


***********************************************************************************
** CHECK FOR CORRECT MAILMASTER SETUP FOR MJ ECOMM                                 
***********************************************************************************
***********************************************************************************

	SELECT 0
	USE F:\3pl\DATA\mailmaster ALIAS mm SHARED
	locate
	LOCATE FOR mm.edi_type = "940E" AND mm.accountid = nAcctNum AND mm.office = cOffice
	IF FOUND()
		STORE TRIM(mm.acctname) TO cCustname
		STORE TRIM(mm.basepath) TO lcPath
		STORE TRIM(mm.archpath) TO lcArchivepath
		_SCREEN.CAPTION = ALLT(mm.scaption)
*!*			STORE mm.testflag 	   TO lTest
		STORE TRIM(IIF(mm.use_alt,mm.sendtoalt,mm.sendto)) TO tsendto
		STORE TRIM(IIF(mm.use_alt,mm.ccalt,mm.cc)) TO tcc
		LOCATE
		LOCATE FOR mm.edi_type = "MISC" AND mm.taskname = "JOETEST"
		STORE TRIM(IIF(mm.use_alt,mm.sendtoalt,mm.sendto)) TO tsendtotest
		STORE TRIM(IIF(mm.use_alt,mm.ccalt,mm.cc)) TO tcctest
		LOCATE
		LOCATE FOR mm.edi_type = "MISC" AND mm.taskname = "GENERAL"
		STORE TRIM(IIF(mm.use_alt,mm.sendtoalt,mm.sendto)) TO tsendtoerr
		STORE TRIM(IIF(mm.use_alt,mm.ccalt,mm.cc)) TO tccerr
		USE IN mm
	ELSE
		USE IN mm
		WAIT WINDOW AT 10,10  "No parameters set for this acct# "+ALLTRIM(STR(nAcctnum))+"  ---> Office "+cOffice TIMEOUT 2
		RELEASE ALL
		NormalExit = .F.
		THROW
	ENDIF

	lcPath = IIF(lTesting,STRTRAN(UPPER(lcPath),"940IN","940INTEST"),lcPath)
	lcArchivepath = IIF(lTesting,STRTRAN(UPPER(lcArchivePath),"940IN","940INTEST"),lcArchivePath)
	cUseName = "MJ_ECOMM"
	cPropername = cUseName

	_SCREEN.CAPTION = cPropername+" 940 Process"
	_SCREEN.WINDOWSTATE = IIF(lTesting OR lOverridebusy,2,1)
	CLEAR
	WAIT WINDOW "Now setting up "+cPropername+" 940 process..." TIMEOUT 2

*!*		cDelimiter = "*"
*!*		cTranslateOption = "TILDE"

	cfile = ""
	dXdate1 = ""
	dXxdate2 = DATE()
	nFileCount = 0

	nRun_num = 999
	lnRunID = nRun_num

*!*		IF lTesting
*!*			coffice = "P"
*!*			cMod = "P"
*!*			cUseFolder = "F:\WHP\WHDATA\"
*!*		ELSE
*!*			xReturn="XXX"
*!*			DO m:\dev\prg\wf_alt WITH cOffice,nAcctNum
*!*			cUseFolder=xReturn
*!*		ENDIF

	DO ("m:\dev\PRG\MARCJACOBSOTF")

	IF !lTesting
		SELECT 0
		USE F:\edirouting\ftpsetup SHARED
		REPLACE chkbusy WITH .f. ;
			FOR ftpsetup.TYPE = "GET" ;
			AND ftpsetup.transfer = "940-MJ_ECOMM" ;
			IN ftpsetup
	ENDIF
	NormalExit = .T.

CATCH TO oErr
	IF NormalExit = .F.
		SET STEP ON
		tsubject = cMailName+" 940 Upload Error ("+TRANSFORM(oErr.ERRORNO)+") at "+TTOC(DATETIME())
		tsendto  = tsendtoerr
		tcc = tccerr
		tmessage = cMailName+" 940 Upload Error..... Please fix me........!"
		lcSourceMachine = SYS(0)
		lcSourceProgram = "from Marc Jacobs eComm group"
		tmessage =tmessage+ "Try/CATCH Exeception Message:"+CHR(13)+;
			[  Error: ] + STR(oErr.ERRORNO) +CHR(13)+;
			[  LineNo: ] + STR(oErr.LINENO) +CHR(13)+;
			[  Message: ] + oErr.MESSAGE +CHR(13)+;
			[  Procedure: ] + oErr.PROCEDURE +CHR(13)

		IF !EMPTY(cErrMsg)
			tmessage =tmessage+CHR(13)+"Error found: "+cErrMsg
		ELSE
			tmessage =tmessage+[  Details: ] + oErr.DETAILS +CHR(13)+;
				[  StackLevel: ] + STR(oErr.STACKLEVEL) +CHR(13)+;
				[  LineContents: ] + oErr.LINECONTENTS+CHR(13)+;
				[  UserValue: ] + oErr.USERVALUE+CHR(13)+;
				[  Computer:  ] +lcSourceMachine+CHR(13)+;
				[  940 file:  ] +xfile+CHR(13)+;
				[  Program:   ] +lcSourceProgram
		ENDIF

		DO FORM m:\dev\frm\dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
	ELSE
		WAIT WINDOW AT 10,10 "Normal Exit " TIMEOUT 2
	ENDIF
FINALLY
	ON ERROR
	SET STATUS BAR ON
	CLOSE DATABASES ALL
ENDTRY
