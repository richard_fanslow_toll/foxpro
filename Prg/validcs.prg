lparameters xcs, xnomessage

*return .t.	&& pcmxxx

if xcs="WILMINGTON PIER"
	return .t.
endif

pcminit(.t.)

if gnopcmiler
	return .t.
else
	xtrip=pcmiler_newtrip(gserverid)
	xvalidcs=pcmiler_lookup(xtrip,xcs,0)
	pcmiler_deletetrip(xtrip)
*	pcmclose()
	
	if xvalidcs=0
		if !xnomessage
			x3winmsg("Location is not valid")
		endif
		return .f.
	else
		return .t.
	endif
endif
