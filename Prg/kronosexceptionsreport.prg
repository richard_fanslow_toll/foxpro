* Time Card Exceptions report, to be run in 2 modes: Prior Week and Current Week to Date.
* EXE in F:\UTIL\KRONOS\
LPARAMETERS tcMode, tcFor

runack("KRONOSEXCEPTIONSREPORT")

LOCAL loKronosExceptionsReport, llTestMode

llTestMode = .F.

IF NOT llTestMode THEN
	utilsetup("KRONOSEXCEPTIONSREPORT")
ENDIF

DECLARE INTEGER CreateMutex IN Win32API ;
	STRING @lpMutexAttributes, ;
	STRING bInitialOwner, ;
	STRING @lpName

DECLARE INTEGER GetLastError IN Win32API

* if running in EXE check to see if already running
IF _VFP.STARTMODE > 0 THEN
	lcProcessName = "KRONOSEXCEPTIONSREPORT"
	CreateMutex(0,CHR(1),@lcProcessName)
	lnError = GetLastError()
	IF lnError = 183
		WAIT WINDOW TIMEOUT 5 "This program is already running..."
		IF NOT llTestMode THEN
			schedupdate()
		ENDIF
		RETURN .F.
	ENDIF
ENDIF

loKronosExceptionsReport = CREATEOBJECT('KronosExceptionsReport')
loKronosExceptionsReport.MAIN( llTestMode, tcMode, tcFor )

IF NOT llTestMode THEN
	schedupdate()
ENDIF

CLOSE DATABASES ALL
RETURN

#DEFINE LOGIT 1
#DEFINE WAITIT 2
#DEFINE NOWAITIT 4
#DEFINE SENDIT 8
#DEFINE NOCRLF 16
#DEFINE CRLF CHR(13) + CHR(10)
#DEFINE LF CHR(10)
#DEFINE RETURN_DATA_MANDATORY .F.
#DEFINE RETURN_DATA_NOT_MANDATORY .T.
#DEFINE EXCPT_EARLY '2'
#DEFINE EXCPT_LATE '3'
#DEFINE EXCPT_LONG_BREAK '4'
#DEFINE EXCPT_MISSED_PUNCH '8'
#DEFINE EXCPT_MISSED_OUT_PUNCH '9'
#DEFINE EXCPT_SHORT_BREAK '10'
#DEFINE EXCPT_VERY_EARLY '12'
#DEFINE EXCPT_VERY_LATE '13'
#DEFINE EXCPT_NUM_EARLY 2
#DEFINE EXCPT_NUM_LATE 3
#DEFINE EXCPT_NUM_VERY_EARLY 12
#DEFINE EXCPT_NUM_VERY_LATE 13

DEFINE CLASS KronosExceptionsReport AS CUSTOM

	cProcessName = 'KronosExceptionsReport'

	lTestMode = .F.  && .F. FOR NORMAL OPERATION, .T. TO SUPPRESS ALL BUT DEVELOPER EMAILS AND CHANGE INPUT TABLE, ETC.

	* which payroll server, and associated connections, to hit
	lAutoYield = .T.
	cStartTime = TTOC(DATETIME())

	* mode properties
	cMode = "WTD"
	*cMode = "PRIORWEEK"
	
	* recipient properties - CHOOSE ONE
	*cFor = "SP2MANAGERS"
	*cFor  = "CA4HR"
	cFor  = "SP2HR"
	
	* date properties
	dtNow = DATETIME()
	dToday = DATE()
	* to force different pay weeks
	*dToday = {^2011-04-25}

	* connection properties
	nSQLHandle = 0

	* file properties
	nFileHandle = 0

	* wait window properties
	nWaitWindowTimeout = 2
	lWaitWindowIsOn = .T.

	* logfile properties
	lLoggingIsOn = .T.
	cLogFile = 'F:\UTIL\KRONOS\Logfiles\KronosExceptionsReport_log.txt'

	* INTERNAL email properties
	lSendInternalEmailIsOn = .T.
	cFrom = 'FMI Corporate <fmicorporate@fmiint.com>'
	cSendTo = 'mbennett@fmiint.com'
	cSendToLive = ''
	cCC = ''
	cSubject = 'Time Card Exceptions'
	cAttach = ''
	cBodyText = ''

	cCOMPUTERNAME = ''
	cUSERNAME = ''

	FUNCTION INIT
		IF NOT DODEFAULT()
			RETURN .F.
		ENDIF
		WITH THIS
			CLEAR
			*SET RESOURCE OFF
			CLOSE DATA
			SET CENTURY ON
			SET DATE YMD
			SET HOURS TO 24
			SET ANSI ON
			SET TALK OFF
			SET DELETED ON
			SET CONSOLE OFF
			SET EXCLUSIVE OFF
			SET SAFETY OFF
			SET EXACT OFF
			SET STATUS BAR ON
			SET SYSMENU OFF
			SET ENGINEBEHAVIOR 70
			SET DECIMALS TO 4
			_VFP.AUTOYIELD = .lAutoYield
			.cCOMPUTERNAME = GETENV("COMPUTERNAME")
			.cUSERNAME = GETENV("USERNAME")
			.lLoggingIsOn = .lLoggingIsOn AND NOT EMPTY(.cLogFile)
		ENDWITH
	ENDFUNC


	FUNCTION DESTROY
		WITH THIS
			IF .lLoggingIsOn  THEN
				SET ALTERNATE OFF
				SET ALTERNATE TO
			ENDIF
		ENDWITH
		DODEFAULT()
	ENDFUNC


	FUNCTION MAIN
		LPARAMETERS tlTestMode, tcMode, tcFor
		WITH THIS
			LOCAL lnNumberOfErrors, lcSQL, loError, lcTopBodyText, lcTS_StartDate, lcTS_EndDate
			LOCAL lcXLFileName, ldStartDate, ldEndDate, lcStartDate, lcEndDate, ldDate, lcDivisionWhere, lcWorksiteWhere
			LOCAL lcSQLStartDate, lcSQLEndDate, lcXLFileName, ldYesterday, lcExceptionIDList
			LOCAL oExcel, oWorkbook, oWorksheet, lnRow, lcRow, lnStartRow, lcStartRow, lnEndRow, lcINorOUT
			lnNumberOfErrors = 0
			lcTopBodyText = ''
			
			TRY
				.lTestMode = tlTestMode
				.cMode = tcMode
				.cFor = tcFor
				
				IF .lTestMode THEN
					.cSendTo = 'mbennett@fmiint.com'
					.cLogFile = 'F:\UTIL\KRONOS\Logfiles\KronosExceptionsReport_log_TESTMODE.txt'
				ENDIF
				IF .lLoggingIsOn THEN
					SET ALTERNATE TO (.cLogFile) ADDITIVE
					SET ALTERNATE ON
				ENDIF
				
				IF EMPTY(.cMode) OR (NOT INLIST(.cMode, "WTD", "PRIORWEEK")) THEN
					.TrackProgress('UNEXPECTED .cMode = ' + TRANSFORM(.cMode), LOGIT+SENDIT)
					THROW
				ENDIF

				IF EMPTY(.cFor) OR (NOT INLIST(.cFor, "SP2MANAGERS", "CA4HR", "SP2HR")) THEN
					.TrackProgress('UNEXPECTED .cFor = ' + TRANSFORM(.cFor), LOGIT+SENDIT)
					THROW
				ENDIF
				
				ldToday = .dToday
				
				* calculate pay period start/end dates.
				DO CASE
					CASE .cMode = "WTD"
						* go back from yesterday until we hit Sunday (DOW = 1), make that start date
						ldDate = ldToday - 1
						ldEndDate = ldDate
						DO WHILE DOW(ldDate,1) <> 1
							ldDate = ldDate - 1
						ENDDO
						ldStartDate = ldDate
						ldPayDate = ldStartDate + 12
					CASE .cMode = "PRIORWEEK"
						* We go back from today until we hit a Saturday (DOW = 7) , which is the end day of the previous pay period.
						* then we subtract 6 days from that to get the prior Sunday (DOW = 1), which is the start day of the previous pay period.
						ldDate = ldToday
						DO WHILE DOW(ldDate,1) <> 7
							ldDate = ldDate - 1
						ENDDO
						ldEndDate = ldDate
						ldStartDate = ldDate - 6
						ldPayDate = ldEndDate + 6
				ENDCASE
				
				.TrackProgress('Kronos Time Card Exceptions Report process started....', LOGIT+SENDIT)
				.TrackProgress('COMPUTERNAME = ' + .cCOMPUTERNAME, LOGIT+SENDIT)
				.TrackProgress('PROJECT = KRONOSEXCEPTIONSREPORT', LOGIT+SENDIT)
				.TrackProgress('Kronos Time Card Exceptions Report process started....', LOGIT+SENDIT)
				IF .lTestMode THEN
					.TrackProgress('                        TEST MODE                        ', LOGIT+SENDIT)
				ENDIF


				*!*
				*!*			***********************************
				*!*			** force custom date ranges here
				*!*			ldStartDate = {^2010-03-07}
				*!*			ldEndDate = {^2010-07-03}
				*!*			***********************************


				************************************************************

				SET DATE AMERICAN

				lcStartDate = STRTRAN(DTOC(ldStartDate),"/","-")
				lcEndDate = STRTRAN(DTOC(ldEndDate),"/","-")

				SET DATE YMD
				lcTS_StartDate = "{ts '" + TRANSFORM(YEAR(ldStartDate)) + "-" + PADL(MONTH(ldStartDate),2,"0") + "-" + PADL(DAY(ldStartDate),2,"0") + " 00:00:00.0'}"
				lcTS_EndDate = "{ts '" + TRANSFORM(YEAR(ldEndDate)) + "-" + PADL(MONTH(ldEndDate),2,"0") + "-" + PADL(DAY(ldEndDate),2,"0") + " 23:59:59.9'}"
				SET DATE AMERICAN

				.cSubject = 'Time Card Exceptions for Week: ' + lcStartDate + " to " + lcEndDate

				IF USED('CUREXCEPTIONSPRE') THEN
					USE IN CUREXCEPTIONSPRE
				ENDIF
				IF USED('CUREXCEPTIONS') THEN
					USE IN CUREXCEPTIONS
				ENDIF
				
				DO CASE
							
					CASE .cFor == "CA4HR"
					
						.cSendToLive = 'Maria.Diaz@tollgroup.com'
						
						lcExceptionIDList = "(" + ;
							EXCPT_EARLY + "," + ;
							EXCPT_LATE + "," + ;
							EXCPT_LONG_BREAK + "," + ;
							EXCPT_MISSED_PUNCH + "," + ;
							EXCPT_MISSED_OUT_PUNCH + "," + ;
							EXCPT_SHORT_BREAK + "," + ;
							EXCPT_VERY_EARLY + "," + ;
							EXCPT_VERY_LATE + ") "
							
						lcDivisionWhere = " AND (L.LABORLEV2NM IN('12','50','71')) "
						lcWorksiteWhere = " AND (L.LABORLEV4NM IN ('CA4','CA5','CA6','CA7','CA8','SP2')) "
						
					CASE .cFor == "SP2HR"
					
						.cSendToLive = 'CCruz@fmiint.com'
						
						lcExceptionIDList = "(" + ;
							EXCPT_EARLY + "," + ;
							EXCPT_LATE + "," + ;
							EXCPT_LONG_BREAK + "," + ;
							EXCPT_MISSED_PUNCH + "," + ;
							EXCPT_MISSED_OUT_PUNCH + "," + ;
							EXCPT_SHORT_BREAK + "," + ;
							EXCPT_VERY_EARLY + "," + ;
							EXCPT_VERY_LATE + ") "
							
						lcDivisionWhere = " AND (L.LABORLEV2NM IN('05','12','25','26','28','50','52','53','54','55','56','57','58','59','71')) "
						lcWorksiteWhere = " AND (L.LABORLEV4NM IN ('CA1','CA2','CA4','CA5','CA6','CA7','CA8','SP2')) "
							
					CASE .cFor == "SP2MANAGERS"
					
						.cSendToLive = 'hvalentin@fmiint.com'
						
						lcExceptionIDList = "(" + EXCPT_SHORT_BREAK + ") "
						
						lcDivisionWhere = ''
						lcWorksiteWhere = " AND (L.LABORLEV4NM IN ('CA1','CA2','CA4','CA5','CA6','CA7','CA8','SP2')) "
							
					OTHERWISE
						.cSendTo = 'mbennett@fmiint.com'
						.TrackProgress('Unexpected .cFor is undefined!', LOGIT+SENDIT)
						THROW				
				ENDCASE
				
				IF .lTestMode THEN
					.TrackProgress('TEST MODE, .cSendToLive = ' + .cSendToLive, LOGIT+SENDIT)
					.cSendTo = 'mbennett@fmiint.com'
					.cCC = ''
				ELSE
					.cSendTo = .cSendToLive
					.cCC = 'mbennett@fmiint.com'
				ENDIF

				* main SQL
				lcSQL = ;
					" SELECT " + ;
					" P.PERSONNUM AS FILE_NUM, P.FULLNM AS EMPLOYEE, E.EXCEPTIONTYPEID, E.INPUNCHSW, " + ;
					" L.LABORLEV2NM AS DIVISION, " + ;
					" L.LABORLEV4NM AS WORKSITE, " + ;
					" L.LABORLEV7DSC AS TIMERVWR, " + ;
					" E.DURATIONSECSQTY AS SCHEDDUR, " + ;
					" E.XCPNDIFFERENCEQTY AS DIFF, " + ;
					" T.EVENTDTM " + ; 
					" FROM WFCEXCEPTION E " + ;
					" INNER JOIN TIMESHEETITEM T " + ;
					" ON T.TIMESHEETITEMID = E.TIMESHEETITEMID " + ;
					" INNER JOIN WTKEMPLOYEE W " + ;
					" ON W.EMPLOYEEID = T.EMPLOYEEID " + ;
					" INNER JOIN PERSON P " + ;
					" ON P.PERSONID = W.PERSONID " + ;
					" INNER JOIN LABORACCT L " + ;
					" ON T.LABORACCTID = L.LABORACCTID " + ;
					" WHERE E.EXCEPTIONTYPEID IN " + lcExceptionIDList + ;
					" AND (L.LABORLEV1NM = 'SXI') " + ;
					lcDivisionWhere + ;
					lcWorksiteWhere + ;
					" AND (T.EVENTDTM >= " + lcTS_StartDate + ") " + ;
					" AND (T.EVENTDTM <= " + lcTS_EndDate + ") " + ;
					" ORDER BY L.LABORLEV7DSC, P.FULLNM, T.EVENTDTM " 

*!*						" WHERE E.EXCEPTIONTYPEID IN (" + EXCPT_SHORT_BREAK + ") " + ;

				IF .lTestMode THEN
					.TrackProgress('', LOGIT+SENDIT)
					.TrackProgress('lcSQL =' + lcSQL, LOGIT+SENDIT)
				ENDIF

				.nSQLHandle = SQLCONNECT("WTK2","sa","kr0n1tes")

				IF .nSQLHandle > 0 THEN

					IF .ExecSQL(lcSQL, 'CUREXCEPTIONSPRE', RETURN_DATA_MANDATORY) THEN


						WAIT WINDOW NOWAIT "Preparing data..."

						SELECT CUREXCEPTIONSPRE
						GOTO TOP
						IF EOF() THEN
							.TrackProgress("There was no Exceptions data to report!", LOGIT+SENDIT)
						ELSE
							SELECT ;
								A.DIVISION, ;
								A.TIMERVWR, ;
								A.EMPLOYEE, ;
								A.FILE_NUM, ;
								A.WORKSITE, ;
								A.INPUNCHSW, ;
								A.EXCEPTIONTYPEID AS EXCEPTCODE, ;
								TTOD(A.EVENTDTM) AS DATE, ;
								B.DESC AS EXCEPTION, ;
								(A.SCHEDDUR / 60) AS BRKLENGTH ;
								FROM CUREXCEPTIONSPRE A ;
								INNER JOIN F:\UTIL\KRONOS\DATA\EXCEPTIONTYPES B ;
								ON B.EXCODE = A.EXCEPTIONTYPEID ;
								INTO CURSOR CUREXCEPTIONS ;
								ORDER BY 1, 2, 3, 8	;
								READWRITE

*!*	SELECT CUREXCEPTIONS
*!*	BROWSE
								
						SELECT CUREXCEPTIONS
						SCAN FOR INLIST(CUREXCEPTIONS.EXCEPTCODE,EXCPT_NUM_EARLY,EXCPT_NUM_LATE,EXCPT_NUM_VERY_EARLY,EXCPT_NUM_VERY_LATE)
							lcINorOUT = IIF(CUREXCEPTIONS.INPUNCHSW = 1,'IN','OUT')
							REPLACE CUREXCEPTIONS.EXCEPTION WITH (ALLTRIM(CUREXCEPTIONS.EXCEPTION) + lcINorOUT) IN CUREXCEPTIONS						
						ENDSCAN
														
									
*!*	SELECT CUREXCEPTIONS
*!*	BROWSE
*!*	x = 2 * .f.

*!*	COPY TO C:\A\EXCEPTIONS.XLS XL5

							lcFilePrefix = "Exceptions" + .cFor + .cMode + "_"
							*ldPayDate = ldEndDate + 6
							lcFileDate = PADL(MONTH(ldPayDate),2,"0") + "-"  + PADL(DAY(ldPayDate),2,"0") + "-" + PADL(YEAR(ldPayDate),4,"0")

							lcTargetDirectory = "F:\UTIL\KRONOS\HOURLYEXCEPTIONREPORTS\" && + lcFileDate + "\"

							lcFiletoSaveAs = lcTargetDirectory + lcFilePrefix + " " + lcFileDate
							lcXLFileName = lcFiletoSaveAs + ".XLS"

							* create primary, detail spreadsheet

							WAIT WINDOW NOWAIT "Opening Excel..."
							oExcel = CREATEOBJECT("excel.application")
							oExcel.VISIBLE = .F.
  							oExcel.DisplayAlerts = .F.
							oWorkbook = oExcel.workbooks.OPEN("F:\UTIL\KRONOS\TEMPLATES\KronosExceptionsReport.XLS")

							* if target directory exists, save there
							llSaveAgain = .F.
							IF DIRECTORY(lcTargetDirectory) THEN
								IF FILE(lcXLFileName) THEN
									DELETE FILE (lcXLFileName)
								ENDIF
								oWorkbook.SAVEAS(lcFiletoSaveAs)
								* set flag to save again after sheet is populated
								llSaveAgain = .T.
							ENDIF

							WAIT WINDOW NOWAIT "Building Hourly Exceptions spreadsheet..."

							lnRow = 2
							lnStartRow = lnRow + 1
							lcStartRow = ALLTRIM(STR(lnStartRow))

							oWorksheet = oWorkbook.Worksheets[1]
							oWorksheet.RANGE("A" + lcStartRow,"H1000").clearcontents()

							oWorksheet.RANGE("A" + lcStartRow,"H1000").FONT.SIZE = 10
							oWorksheet.RANGE("A" + lcStartRow,"H1000").FONT.NAME = "Arial"
							oWorksheet.RANGE("A" + lcStartRow,"H1000").FONT.bold = .F.

							lcTitle = "Hourly Exceptions" + LF + ;
								" for Pay Date: " + DTOC(ldPayDate) + LF + ;
								" Week of: " + lcStartDate + " - " + lcEndDate

							oWorksheet.RANGE("A1").VALUE = lcTitle

							* main scan/processing
							SELECT CUREXCEPTIONS

							SCAN
								lnRow = lnRow + 1
								lcRow = LTRIM(STR(lnRow))
								oWorksheet.RANGE("A" + lcRow).VALUE = "'" + ALLTRIM(CUREXCEPTIONS.DIVISION)
								oWorksheet.RANGE("B" + lcRow).VALUE = ALLTRIM(CUREXCEPTIONS.TIMERVWR)
								oWorksheet.RANGE("C" + lcRow).VALUE = ALLTRIM(CUREXCEPTIONS.EMPLOYEE)
								oWorksheet.RANGE("D" + lcRow).VALUE = CUREXCEPTIONS.FILE_NUM
								oWorksheet.RANGE("E" + lcRow).VALUE = ALLTRIM(CUREXCEPTIONS.WORKSITE)
								oWorksheet.RANGE("F" + lcRow).VALUE = CUREXCEPTIONS.DATE
								oWorksheet.RANGE("G" + lcRow).VALUE = ALLTRIM(CUREXCEPTIONS.EXCEPTION)
								oWorksheet.RANGE("H" + lcRow).VALUE = CUREXCEPTIONS.BRKLENGTH
							ENDSCAN

							oWorksheet.PageSetup.PrintArea = "$A$"  + lcStartRow + ":$H$" + lcRow

							* save again
							IF llSaveAgain THEN
								oWorkbook.SAVE()
							ENDIF
							oWorkbook.CLOSE()

							oExcel.QUIT()

							IF FILE(lcXLFileName) THEN
								.cAttach = lcXLFileName
								.TrackProgress('Attached Exceptions File : ' + lcXLFileName, LOGIT+SENDIT)
								.TrackProgress('Kronos Hourly Exceptions process ended normally.', LOGIT+SENDIT+NOWAITIT)
							ELSE
								.TrackProgress('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!', LOGIT+SENDIT)
								.TrackProgress("ERROR: There was a problem attaching Exceptions File : " + lcXLFileName, LOGIT+SENDIT)
								.TrackProgress('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!', LOGIT+SENDIT)
							ENDIF

						ENDIF  && EOF() CURTEMPS

					ENDIF  &&  .ExecSQL(lcSQL, 'CURWTKEMPLOYEE', RETURN_DATA_MANDATORY)

					CLOSE DATABASES ALL

					IF .lTestMode THEN
						.cBodyText = lcTopBodyText + CRLF + CRLF + "<processing log follows>" + CRLF + CRLF + .cBodyText
					ELSE
						.cBodyText = lcTopBodyText + CRLF + CRLF + .cBodyText
					ENDIF

				ELSE
					* connection error
					.TrackProgress('Unable to connect to WTK System.', LOGIT+SENDIT)
				ENDIF   &&  .nSQLHandle > 0
			CATCH TO loError

				.TrackProgress('There was an error.',LOGIT+SENDIT)
				.TrackProgress('The Error # is: ' + TRANSFORM(loError.ERRORNO), LOGIT+SENDIT)
				.TrackProgress('The Error Message is: ' + TRANSFORM(loError.MESSAGE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred in Program: ' + TRANSFORM(loError.PROCEDURE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred at line #: ' + TRANSFORM(loError.LINENO), LOGIT+SENDIT)
				lnNumberOfErrors = lnNumberOfErrors + 1
				IF TYPE('oExcel') = 'O' AND NOT ISNULL(oExcel) THEN
					oExcel.QUIT()
				ENDIF
				CLOSE DATA
				CLOSE ALL

			ENDTRY

			CLOSE DATA
			WAIT CLEAR
			***************** INTERNAL email results ******************************
			.TrackProgress('About to send status email.',LOGIT)
			.TrackProgress('==================================================================================================================', SENDIT)
			.TrackProgress('The logfile is: ' + .cLogFile, SENDIT)
			.TrackProgress('Kronos Time Card Exceptions Report process started: ' + .cStartTime, LOGIT+SENDIT)
			.TrackProgress('Kronos Time Card Exceptions Report process finished: ' + TTOC(DATETIME()), LOGIT+SENDIT)

			IF .lSendInternalEmailIsOn THEN
				* try to trap error from not having dartmail dll's registered on user's pc...
				TRY
					DO FORM dartmail2 WITH .cSendTo,.cFrom,.cSubject,.cCC,.cAttach,.cBodyText,"A"
					.TrackProgress('Sent status email.',LOGIT)
				CATCH TO loError
					*=MESSAGEBOX('There was an error sending the status email. Please contact Mark Bennett.',0+64,"Kronos Time Card Exceptions Report Time")
					.TrackProgress('There was an error sending the status email.',LOGIT)
					.TrackProgress('The Error # is: ' + TRANSFORM(loError.ERRORNO), LOGIT)
					.TrackProgress('The Error Message is: ' + TRANSFORM(loError.MESSAGE), LOGIT)
					.TrackProgress('The Error occurred in Program: ' + TRANSFORM(loError.PROCEDURE), LOGIT)
					.TrackProgress('The Error occurred at line #: ' + TRANSFORM(loError.LINENO), LOGIT)
					lnNumberOfErrors = lnNumberOfErrors + 1
					CLOSE DATA
				ENDTRY

			ELSE
				.TrackProgress('Did not send status email: THIS.lSendInternalEmailIsOn = FALSE.',LOGIT)
			ENDIF

		ENDWITH
		RETURN
	ENDFUNC && main


	FUNCTION ExecSQL
		LPARAMETERS tcSQL, tcCursorName, tlNoDataReturnedIsOkay
		LOCAL llRetval, lnResult
		WITH THIS
			* close target cursor if it's open
			IF USED(tcCursorName)
				USE IN (tcCursorName)
			ENDIF
			WAIT WINDOW "Fetching remote data; please wait..." NOWAIT
			lnResult = SQLEXEC(.nSQLHandle, tcSQL, tcCursorName)
			llRetval = ( lnResult > 0 )
			IF llRetval THEN
				* see if any data came back
				IF NOT tlNoDataReturnedIsOkay THEN
					IF (NOT USED(tcCursorName)) OR EOF(tcCursorName) THEN
						llRetval = .F.
						.TrackProgress('No data was returned by this query: ' +CRLF + tcSQL, LOGIT+SENDIT)
					ENDIF
				ENDIF
			ELSE
				.TrackProgress("Result Code [" + TRANSFORM(lnResult) + "] returned by this query: " + tcSQL, LOGIT+SENDIT)
			ENDIF
			WAIT CLEAR
			RETURN llRetval
		ENDWITH
	ENDFUNC


	PROCEDURE TrackProgress
		* do any combination of Wait Window, writing to logfile, and adding to body of email,
		* based on nFlags parameter.
		LPARAMETERS tcExpression, tnFlags
		LOCAL lcCRLF
		WITH THIS
			IF BITAND(tnFlags,NOCRLF) = NOCRLF THEN
				lcCRLF = ""
			ELSE
				lcCRLF = CRLF
			ENDIF
			IF BITAND(tnFlags,LOGIT) = LOGIT THEN
				IF .lLoggingIsOn THEN
					?
					? .cProcessName + "  " + TTOC(DATETIME()) + ": " + tcExpression
				ENDIF
			ENDIF
			IF .lWaitWindowIsOn THEN
				IF BITAND(tnFlags,NOWAITIT) = NOWAITIT THEN
					WAIT WINDOW tcExpression NOWAIT
				ENDIF
				IF BITAND(tnFlags,WAITIT) = WAITIT THEN
					WAIT WINDOW tcExpression TIMEOUT .nWaitWindowTimeout
				ENDIF
			ENDIF
			IF BITAND(tnFlags,SENDIT) = SENDIT THEN
				IF .lSendInternalEmailIsOn THEN
					.cBodyText = .cBodyText + tcExpression + CRLF + lcCRLF
				ENDIF
			ENDIF
		ENDWITH
	ENDPROC  &&  TrackProgress


ENDDEFINE
