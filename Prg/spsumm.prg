* special project summary report

parameters xoffice,xfilter,xinvstartdt,xinvenddt,xcompstartdt,xcompenddt

xinvstartdt=iif(empty(xinvstartdt),{},xinvstartdt)
xinvenddt=iif(empty(xinvenddt),{},xinvenddt)
xcompstartdt=iif(empty(xcompstartdt),{},xcompstartdt)
xcompenddt=iif(empty(xcompenddt),{},xcompenddt)

do case
case !empty(xinvstartdt) and !empty(xinvenddt) and !empty(xcompstartdt) and !empty(xcompenddt)
	xheader="Invoiced between "+dtoc(xinvstartdt)+" and "+dtoc(xinvenddt)+" & Completed between "+dtoc(xcompstartdt)+" and "+dtoc(xcompenddt)
	xfilter2="between(invdt,{"+dtoc(xinvstartdt)+"},{"+dtoc(xinvenddt)+"}) and between(completeddt,{"+dtoc(xcompstartdt)+"},{"+dtoc(xcompenddt)+"})"
case !empty(xinvstartdt) and !empty(xinvenddt)
	xheader="Invoiced between "+dtoc(xinvstartdt)+" and "+dtoc(xinvenddt)
	xfilter2="between(invdt,{"+dtoc(xinvstartdt)+"},{"+dtoc(xinvenddt)+"})"
case !empty(xcompstartdt) and !empty(xcompenddt)
	xheader="Completed between "+dtoc(xcompstartdt)+" and "+dtoc(xcompenddt)
	xfilter2="between(completeddt,{"+dtoc(xcompstartdt)+"},{"+dtoc(xcompenddt)+"})"
otherwise
	x3winmsg("You need to enter some dates")
	return .f.
endcase

xsqlexec("select * from project where "+gmodfilter+" and "+xfilter+" and "+xfilter2,,,"wh")
	
select projno, wo_num, completeddt, invnum, invdt, accountid, acctname, custrefno, consignee, ;
	totrevenue, hours, samshours, unitqty, 000000.00 as actrev ;
	from project ;
	into cursor xrpt readwrite

scan 
	if xsqlexec("select * from invoice where invnum='"+invnum+"'",,,"ar") # 0
		xsqlexec("select * from invdet where invoiceid="+transform(invoice.invoiceid),,,"ar")
		sum invdetamt to xx for invoiceid=invoice.invoiceid and wo_num=xrpt.wo_num and glcode="16"
		if xx=0
			sum invdetamt to xx for invoiceid=invoice.invoiceid and glcode="16"
		endif
		
		replace actrev with xx in xrpt
	endif
endscan

locate
