* Create DAILY SNAPSHOTS OF KRONOS & ADP DATA
*which can be used to double-check data after making mass changes in adp and/or kronos hr.

* Build EXe as F:\UTIL\SNAPSHOTS\KRONOSADPSNAPSHOTS.EXE

utilsetup("KRONOSADPSNAPSHOTS")

LOCAL loKronosADPSnapshotProcess
loKronosADPSnapshotProcess = CREATEOBJECT('KronosADPSnapshotProcess')
loKronosADPSnapshotProcess.MAIN()

schedupdate()

CLOSE DATABASES ALL
RETURN

#DEFINE LOGIT 1
#DEFINE WAITIT 2
#DEFINE NOWAITIT 4
#DEFINE SENDIT 8
#DEFINE CRLF CHR(13) + CHR(10)
#DEFINE CARRIAGE_RETURN CHR(13)
#DEFINE LINE_FEED CHR(10)
#DEFINE RETURN_DATA_MANDATORY .F.
#DEFINE RETURN_DATA_NOT_MANDATORY .T.

DEFINE CLASS KronosADPSnapshotProcess AS CUSTOM

	cProcessName = 'KronosADPSnapshotProcess'

	lTestMode = .T.  && .F. FOR NORMAL OPERATION, .T. TO SUPPRESS ALL BUT DEVELOPER EMAILS AND CHANGE INPUT TABLE, ETC.

	lAutoYield = .T.

	lCalcSickBalance = .F.

	dToday = DATE()

	cStartTime = TTOC(DATETIME())

	* connection properties
	nSQLHandle = 0

	* wait window properties
	nWaitWindowTimeout = 2
	lWaitWindowIsOn = .T.

	* logfile properties
	lLoggingIsOn = .T.
	cLogFile = 'F:\UTIL\SNAPSHOTS\Logfiles\KronosADPSnapshotProcess_log.txt'

	* INTERNAL email properties
	lSendInternalEmailIsOn = .T.
	cFrom = 'FMI Corporate <fmicorporate@fmiint.com>'
	cSendTo = 'Mark Bennett <mbennett@fmiint.com>'
	cCC = ''
	cSubject = 'Kronos ADP Snapshot Report for: ' + TTOC(DATETIME())
	cAttach = ''
	cBodyText = ''

	cCOMPUTERNAME = ''
	cUSERNAME = ''

	FUNCTION INIT
		IF NOT DODEFAULT()
			RETURN .F.
		ENDIF
		WITH THIS
			CLEAR
			*SET RESOURCE OFF
			CLOSE DATA
			SET CENTURY ON
			SET DATE AMERICAN
			SET DECIMALS TO 3
			SET HOURS TO 24
			SET ANSI ON
			SET TALK OFF
			SET DELETED ON
			SET CONSOLE OFF
			SET EXCLUSIVE OFF
			SET SAFETY OFF
			SET EXACT OFF
			SET STATUS BAR ON
			SET SYSMENU OFF
			SET ENGINEBEHAVIOR 70
			_VFP.AUTOYIELD = .lAutoYield
			.cCOMPUTERNAME = GETENV("COMPUTERNAME")
			.cUSERNAME = GETENV("USERNAME")
			IF .lTestMode THEN
				.cSendTo = 'Mark Bennett <mbennett@fmiint.com>'
				.cLogFile = 'F:\UTIL\SNAPSHOTS\Logfiles\KronosADPSnapshotProcess_log_TESTMODE.txt'
			ENDIF
			.lLoggingIsOn = .lLoggingIsOn AND NOT EMPTY(.cLogFile)
			IF .lLoggingIsOn THEN
				SET ALTERNATE TO (.cLogFile) ADDITIVE
				SET ALTERNATE ON
			ENDIF
			* re-init date properties after all SETTINGS
			.dToday = DATE()
		ENDWITH
	ENDFUNC


	FUNCTION DESTROY
		WITH THIS
			IF .lLoggingIsOn  THEN
				SET ALTERNATE OFF
				SET ALTERNATE TO
			ENDIF
		ENDWITH
		DODEFAULT()
	ENDFUNC


	FUNCTION MAIN
		WITH THIS
			LOCAL lnNumberOfErrors, loError, lcSQL, lcSQL2, lcDateString, lcOutputTable, lcYearString, lcCumTable, ldToday
			LOCAL lnAnnsal

			TRY
				lnNumberOfErrors = 0

				*.TrackProgress('==================================================================================================================', LOGIT+SENDIT)
				.TrackProgress('Kronos ADP Snapshot Report process started....', LOGIT+SENDIT+NOWAITIT)
				.TrackProgress('COMPUTERNAME = ' + .cCOMPUTERNAME, LOGIT+SENDIT)
				.TrackProgress('USERNAME = ' + .cUSERNAME, LOGIT+SENDIT)
				.TrackProgress('==================================================================================================================', LOGIT+SENDIT)
				IF .lTestMode THEN
					.TrackProgress('', LOGIT)
					.TrackProgress('==================================================================================================================', LOGIT+SENDIT)
					.TrackProgress('                        TEST MODE                        ', LOGIT+SENDIT)
					.TrackProgress('==================================================================================================================', LOGIT+SENDIT)
				ENDIF

				*!*					* for testing & reinitialization
				*!*					USE F:\UTIL\SNAPSHOTS\KRONOS\SNAP2008 EXCLUSIVE
				*!*					ZAP
				*!*					USE F:\UTIL\SNAPSHOTS\ADP\SNAP2008 EXCLUSIVE
				*!*					ZAP
				*!*					USE F:\UTIL\SNAPSHOTS\KRONOS\SNAP2009 EXCLUSIVE
				*!*					ZAP
				*!*					USE F:\UTIL\SNAPSHOTS\ADP\SNAP2009 EXCLUSIVE
				*!*					ZAP
				*!*					USE

				ldToday = .dToday


*ldToday = {^2012-01-26}


				lcDateString = DTOS(ldToday)
				lcYearString = ALLTRIM(TRANSFORM(YEAR(ldToday)))

				*!*					lcSQL = ;
				*!*						" SELECT " + ;
				*!*						" PERSONNUM AS FILE_NUM, " + ;
				*!*						" PERSONFULLNAME AS NAME, " + ;
				*!*						" EMPLOYMENTSTATUS AS STATUS, " + ;
				*!*						" HOMELABORLEVELNM1 AS ADP_COMP, " + ;
				*!*						" HOMELABORLEVELNM2 AS DIVISION, " + ;
				*!*						" HOMELABORLEVELNM3 AS DEPT, " + ;
				*!*						" HOMELABORLEVELNM4 AS WORKSITE, " + ;
				*!*						" HOMELABORLEVELNM5 AS EETYPE, " + ;
				*!*						" HOMELABORLEVELNM6 AS SHIFT, " + ;
				*!*						" HOMELABORLEVELNM7 AS TIMERVWR, " + ;
				*!*						" PAYRULENAME AS PAYRULE, " + ;
				*!*						" ACCRUALPRFLNAME AS ACPROFILE, " + ;
				*!*						" DCMDEVGRPNAME AS DEVGROUP, " + ;
				*!*						" TIMEZONENAME AS TIMEZONE, " + ;
				*!*						" SENIORITYRANKDATE AS SENIORDATE, " + ;
				*!*						" COMPANYHIREDTM AS HIREDATE " + ;
				*!*						" FROM VP_EMPLOYEEV42 " + ;
				*!*						" WHERE EMPLOYMENTSTATUS = 'Active' " + ;
				*!*						" ORDER BY PERSONNUM "

				*!*					IF .lTestMode THEN
				*!*						.TrackProgress('', LOGIT+SENDIT)
				*!*						.TrackProgress('lcSQL =' + lcSQL, LOGIT+SENDIT)
				*!*					ENDIF

				*!*					.TrackProgress('Connecting to WTK System....', LOGIT+SENDIT)

				*!*					OPEN DATABASE F:\UTIL\KRONOS\KRONOSEDI

				*!*					.nSQLHandle = SQLCONNECT("WTK_ON_PAYROLL2","sa","kr0n1tes")

				*!*					IF .nSQLHandle > 0 THEN

				*!*						IF USED('CURKRONOS') THEN
				*!*							USE IN CURKRONOS
				*!*						ENDIF
				*!*						IF USED('CURKRONOS2') THEN
				*!*							USE IN CURKRONOS2
				*!*						ENDIF
				*!*
				*!*						IF .ExecSQL(lcSQL, 'CURKRONOS', RETURN_DATA_MANDATORY) THEN
				*!*
				*!*							SELECT CURKRONOS
				*!*							GOTO TOP

				*!*							IF EOF() THEN
				*!*								.TrackProgress("There was no data to export!", LOGIT+SENDIT)
				*!*							ELSE
				*!*
				*!*								SELECT ldToday AS DATE, * ;
				*!*									FROM CURKRONOS ;
				*!*									INTO CURSOR CURKRONOS2 ;
				*!*									ORDER BY FILE_NUM

				*!*								lcOutputTable = "F:\UTIL\SNAPSHOTS\KRONOS\SNAP" + lcDateString
				*!*								SELECT CURKRONOS2
				*!*								COPY TO (lcOutputTable)
				*!*
				*!*								* also import into Yearly cum table to support trending
				*!*								lcCumTable = "F:\UTIL\SNAPSHOTS\KRONOS\SNAP" + lcYearString
				*!*
				*!*								USE (lcCumTable)
				*!*								APPEND FROM (lcOutputTable)

				*!*								=SQLDISCONNECT(.nSQLHandle)
				*!*
				*!*								IF USED('CURKRONOS') THEN
				*!*									USE IN CURKRONOS
				*!*								ENDIF
				*!*								IF USED('CURKRONOS2') THEN
				*!*									USE IN CURKRONOS2
				*!*								ENDIF

				* get data from ADP
				OPEN DATABASE F:\UTIL\ADPCONNECTION\CONNECT1

				.nSQLHandle = SQLCONNECT("PCPAYWIN","MBENNETT","VP737A")

				IF .nSQLHandle > 0 THEN
					lcSQL2 = ;
						" SELECT " + ;
						" A.FILE# AS FILE_NUM, " + ;
						" A.STATUS, " + ;
						" A.COMPANYCODE AS ADP_COMP, " + ;
						" {fn LEFT(A.HOMEDEPARTMENT,2)} AS DIVISION, " + ;
						"  {fn SUBSTRING(A.HOMEDEPARTMENT,3,4)} AS DEPARTMENT, " + ;
						" A.NAME, " + ;
						" A.STATE, " + ;
						" {FN IFNULL(A.RATE1AMT,0.00)} AS RATE1AMT " + ;
						" FROM REPORTS.V_EMPLOYEE A " + ;
						" WHERE COMPANYCODE IN ('E87','E88','E89') " + ;
						" ORDER BY 1 "

					*!*									lcSQL2 = ;
					*!*										" SELECT " + ;
					*!*										" A.FILE# AS FILE_NUM, " + ;
					*!*										" A.STATUS, " + ;
					*!*										" A.COMPANYCODE AS ADP_COMP, " + ;
					*!*										" {fn LEFT(A.HOMEDEPARTMENT,2)} AS DIVISION, " + ;
					*!*										"  {fn SUBSTRING(A.HOMEDEPARTMENT,3,4)} AS DEPARTMENT, " + ;
					*!*										" A.NAME, " + ;
					*!*										" A.HIREDATE, " + ;
					*!*										" A.GENDER, " + ;
					*!*										" A.STREETLINE1, " + ;
					*!*										" {FN IFNULL(A.STREETLINE2,' ')} AS STREETLINE2, " + ;
					*!*										" A.CITY, " + ;
					*!*										" A.ZIPCODE, " + ;
					*!*										" A.STATE, " + ;
					*!*										" {FN IFNULL(A.CUSTAREA3,'   ')} AS WORKSITE, " + ;
					*!*										" A.BIRTHDATE, " + ;
					*!*										" {FN IFNULL(A.RATE1AMT,0.00)} AS RATE1AMT, " + ;
					*!*										" A.SOCIALSECURITY# AS SS_NUM, " + ;
					*!*										" A.EEOCJOBCLASS AS JOBCLASS, " + ;
					*!*										" B.DESCRIPTION_TX AS JOBDESC, " + ;
					*!*										" {FN IFNULL(a.ANNUALSALARY,0.00)} AS ANNSALARY " + ;
					*!*										" FROM REPORTS.V_EMPLOYEE A, PCPAYSYS.T_CO_JOB_CLASS B " + ;
					*!*										" WHERE (B.CO_C(+) = A.COMPANYCODE) AND (B.EEOC_JOB_CLASS_C(+) = A.EEOCJOBCLASS) " + ;
					*!*										" AND COMPANYCODE IN ('E87','E88','E89') " + ;
					*!*										" ORDER BY 1 "

					IF .lTestMode THEN
						.TrackProgress('lcSQL2 = ' + lcSQL2, LOGIT+SENDIT)
					ENDIF

					IF USED('CURADP') THEN
						USE IN CURADP
					ENDIF
					IF USED('CURADP2') THEN
						USE IN CURADP2
					ENDIF

					IF .ExecSQL(lcSQL2, 'CURADP', RETURN_DATA_MANDATORY) THEN

						SELECT ldToday AS DATE, *, 0000000.00 AS ANNSAL ;
							FROM CURADP ;
							INTO CURSOR CURADP2 ;
							ORDER BY FILE_NUM ;
							READWRITE
							
						SELECT CURADP2
						SCAN
							DO CASE
								CASE CURADP2.RATE1AMT = 0.00
									* OTR DRIVERS, calc at $20/hour
									lnAnnsal = 20 * 40 * 52
								CASE INLIST(CURADP2.ADP_COMP,'SXI','E87')
									* HOURLY - hourly rate 
									lnAnnsal = CURADP2.RATE1AMT * 40 * 52 
								OTHERWISE
									* SALARIED- biweekly rate							
									lnAnnsal = CURADP2.RATE1AMT * 26
							ENDCASE
							REPLACE CURADP2.ANNSAL WITH lnAnnsal
						ENDSCAN

						lcOutputTable = "F:\UTIL\SNAPSHOTS\ADP\SNAP" + lcDateString
						SELECT CURADP2
						COPY TO (lcOutputTable)

*!*							* also import into Yearly cum table to support trending
*!*							lcCumTable = "F:\UTIL\SNAPSHOTS\ADP\SNAP" + lcYearString

*!*							USE (lcCumTable)
*!*							APPEND FROM (lcOutputTable)

						* DELETE ANY RECORDS FOR TODAY IN THE CONSOLIDATED TABLE, TO PREVENT DUPLICATES IF THE UTIL RUNS TWICE IN ONE DAY
						USE F:\UTIL\SNAPSHOTS\ADP\SNAPALL 
						DELETE FOR (DATE = ldToday)
						* import into consolidated table, all years
						*USE F:\UTIL\SNAPSHOTS\ADP\SNAPALL 
						APPEND FROM (lcOutputTable)

						CLOSE DATABASES ALL

						* delete output table
						DELETE FILE (lcOutputTable + ".DBF")

					ENDIF

				ELSE
					* connection error
					.TrackProgress('Unable to connect to ADP Payroll System.', LOGIT+SENDIT)
				ENDIF

				*!*							ENDIF

				*!*						ENDIF

				*!*						CLOSE DATABASES ALL
				*!*					ELSE
				*!*						* connection error
				*!*						.TrackProgress('Unable to connect to WTK System.', LOGIT+SENDIT)
				*!*					ENDIF   &&  .nSQLHandle > 0
			CATCH TO loError

				.TrackProgress('There was an error.',LOGIT+SENDIT)
				.TrackProgress('The Error # is: ' + TRANSFORM(loError.ERRORNO), LOGIT+SENDIT)
				.TrackProgress('The Error Message is: ' + TRANSFORM(loError.MESSAGE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred in Program: ' + TRANSFORM(loError.PROCEDURE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred at line #: ' + TRANSFORM(loError.LINENO), LOGIT+SENDIT)
				lnNumberOfErrors = lnNumberOfErrors + 1
				IF TYPE('oExcel') = 'O' AND NOT ISNULL(oExcel) THEN
					oExcel.QUIT()
				ENDIF
				CLOSE DATA
				CLOSE ALL

			ENDTRY

			CLOSE DATA
			WAIT CLEAR
			***************** INTERNAL email results ******************************
			.TrackProgress('About to send status email.',LOGIT)
			.TrackProgress('==================================================================================================================', SENDIT)
			.TrackProgress('The logfile is: ' + .cLogFile, SENDIT)
			.TrackProgress('Kronos ADP Snapshot Report process started: ' + .cStartTime, LOGIT+SENDIT)
			.TrackProgress('Kronos ADP Snapshot Report process finished: ' + TTOC(DATETIME()), LOGIT+SENDIT)

			IF .lSendInternalEmailIsOn THEN
				* try to trap error from not having dartmail dll's registered on user's pc...
				TRY
					DO FORM dartmail2 WITH .cSendTo,.cFrom,.cSubject,.cCC,.cAttach,.cBodyText,"A"
					.TrackProgress('Sent status email.',LOGIT)
				CATCH TO loError
					=MESSAGEBOX('There was an error sending the status email. Please contact Mark Bennett.',0+64,"Kronos ADP Snapshot Report")
					.TrackProgress('There was an error sending the status email.',LOGIT)
					.TrackProgress('The Error # is: ' + TRANSFORM(loError.ERRORNO), LOGIT)
					.TrackProgress('The Error Message is: ' + TRANSFORM(loError.MESSAGE), LOGIT)
					.TrackProgress('The Error occurred in Program: ' + TRANSFORM(loError.PROCEDURE), LOGIT)
					.TrackProgress('The Error occurred at line #: ' + TRANSFORM(loError.LINENO), LOGIT)
					lnNumberOfErrors = lnNumberOfErrors + 1
					CLOSE DATA
				ENDTRY
			ELSE
				.TrackProgress('Did not send status email: THIS.lSendInternalEmailIsOn = FALSE.',LOGIT)
			ENDIF

		ENDWITH
		RETURN
	ENDFUNC && main


	FUNCTION ExecSQL
		LPARAMETERS tcSQL, tcCursorName, tlNoDataReturnedIsOkay
		LOCAL llRetval, lnResult
		WITH THIS
			* close target cursor if it's open
			IF USED(tcCursorName)
				USE IN (tcCursorName)
			ENDIF
			WAIT WINDOW "Fetching remote data; please wait..." NOWAIT
			lnResult = SQLEXEC(.nSQLHandle, tcSQL, tcCursorName)
			llRetval = ( lnResult > 0 )
			IF llRetval THEN
				* see if any data came back
				IF NOT tlNoDataReturnedIsOkay THEN
					IF (NOT USED(tcCursorName)) OR EOF(tcCursorName) THEN
						llRetval = .F.
						.TrackProgress('No data was returned by this query: ' +CRLF + tcSQL, LOGIT+SENDIT)
					ENDIF
				ENDIF
			ELSE
				.TrackProgress("Result Code [" + TRANSFORM(lnResult) + "] returned by this query: " + tcSQL, LOGIT+SENDIT)
			ENDIF
			WAIT CLEAR
			RETURN llRetval
		ENDWITH
	ENDFUNC


	PROCEDURE TrackProgress
		* do any combination of Wait Window, writing to logfile, and adding to body of email,
		* based on nFlags parameter.
		LPARAMETERS tcExpression, tnFlags
		WITH THIS
			IF BITAND(tnFlags,LOGIT) = LOGIT THEN
				IF .lLoggingIsOn THEN
					?
					? .cProcessName + "  " + TTOC(DATETIME()) + ": " + tcExpression
				ENDIF
			ENDIF
			IF .lWaitWindowIsOn THEN
				IF BITAND(tnFlags,NOWAITIT) = NOWAITIT THEN
					WAIT WINDOW tcExpression NOWAIT
				ENDIF
				IF BITAND(tnFlags,WAITIT) = WAITIT THEN
					WAIT WINDOW tcExpression TIMEOUT .nWaitWindowTimeout
				ENDIF
			ENDIF
			IF BITAND(tnFlags,SENDIT) = SENDIT THEN
				IF .lSendInternalEmailIsOn THEN
					.cBodyText = .cBodyText + tcExpression + CRLF + CRLF
				ENDIF
			ENDIF
		ENDWITH
	ENDPROC  &&  TrackProgress

ENDDEFINE

