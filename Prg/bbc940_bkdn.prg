*!* m:\dev\prg\bbc940_bkdn.prg

clear
lcheckstyle = .t.
lprepack = .f.  && Inventory to be kept as PnP
m.color=""
cuccnumber = ""
cusepack = ""
lcuccseed = 1

select x856
count for trim(segment) = "ST" to nsegcnt
csegcnt = alltrim(str(nsegcnt))
select x856
locate

ptctr = 0

wait window "Now in "+cmailname+" 940 Breakdown" nowait

wait "There are "+csegcnt+" P/Ts in this file" window nowait
select x856
locate

*!* Constants
m.acct_name = icase(nacctnum = 6757,"BBC INT. - ROBERT WAYNE",nacctnum = 6763,"BBC INT - CLUB FOOT",nacctnum = 6759,"BBC INT. - FEIYUE","BBC INTERNATIONAL")

store nacctnum to m.accountid
m.careof = " "
m.sf_addr1 = "1515 N FEDERAL HWY #206"
m.sf_addr2 = ""
m.sf_csz = "BOCA RATON, FL 33432"
m.printed = .f.
m.print_by = ""
m.printdate = date()
*!* End constants

select xpt
delete all
select xptdet
delete all

store 0 to pt_total_eaches
store 0 to pt_total_cartons
store 0 to nctncount
 
select x856
set filter to
locate
create cursor curppk (;
upc char(12),;
qty n(6),;
style char(20),;
color char(10),;
id    char(20),;
musical char(40))

store "" to cisa_num
llhasprepacks = .f.

wait window "NOW IN INITAL HEADER BREAKDOWN" timeout 1
wait window "NOW IN INITAL HEADER BREAKDOWN" nowait
*ASSERT .F. MESSAGE "At header bkdn...debug"
do while !eof("x856")
	csegment = trim(x856.segment)
	ccode = trim(x856.f1)

	if trim(x856.segment) = "ISA"
		cisa_num = alltrim(x856.f13)
		currentisanum = alltrim(x856.f13)
		wait window "This is a "+cmailname+" PT upload" nowait
*  ptctr = 0
		select x856
		skip
		loop
	endif

	if trim(x856.segment) = "GS"
		cgs_num = alltrim(x856.f6)
		lccurrentgroupnum = alltrim(x856.f6)
		select x856
		skip
		loop
	endif

	if trim(x856.segment) = "ST" and trim(x856.f1) = "940"
		if !ltesting
			select x856
			skip
			loop
		endif
	endif

	if trim(x856.segment) = "W05"  && new shipment
		nctncount = 0
		m.style = ""
		select xpt
		ndetcnt = 0
		append blank

		replace xpt.shipins with xpt.shipins+chr(13)+"FILENAME*"+cfilename in xpt
		replace xpt.shipins with xpt.shipins+chr(13)+"CONAME*BBC" in xpt
		replace xpt.qty with 0 in xpt
		replace xpt.origqty with 0 in xpt
		store 0 to pt_total_eaches
		store 0 to pt_total_cartons
		ptctr = ptctr +1
		wait window at 10,10 "Now processing PT# "+ alltrim(x856.f2)+" Number "+chr(13)+transform(ptctr)+" of "+csegcnt nowait

		replace xpt.ptid       with ptctr in xpt
		replace xpt.accountid  with nacctnum in xpt
		replace xpt.ptdate     with dfiledate in xpt
		m.ship_ref = alltrim(x856.f2)
		cship_ref = alltrim(x856.f2)
		replace xpt.ship_ref   with m.ship_ref in xpt  && Pick Ticket
		m.pt = alltrim(x856.f2)
		replace xpt.cnee_ref   with alltrim(x856.f3)  && cust PO
		llhasprepacks = .f.  && always reset this to .f. no prepacks
		select x856
		skip
		loop
	endif

	if trim(x856.segment) = "N1" and trim(x856.f1) = "ST" && Ship-to data
		cconsignee = upper(alltrim(x856.f2))
		if "~"$cconsignee
*   cConsignee = SUBSTR(cConsignee,AT("~",cConsignee,1)+1)
		endif
		select xpt
		m.st_name = cconsignee
		replace xpt.consignee with  cconsignee in xpt
		replace xpt.name with  cconsignee in xpt

		cstorenum = alltrim(x856.f4)
		if empty(xpt.shipins)
			replace xpt.shipins with "STORENUM*"+cstorenum in xpt
		else
			replace xpt.shipins with xpt.shipins+chr(13)+"STORENUM*"+cstorenum in xpt
		endif
		if empty(xpt.dcnum)
			replace xpt.dcnum with cstorenum in xpt
		endif
		if len(cstorenum)>5
			replace xpt.storenum  with val(right(cstorenum,5)) in xpt
		else
			replace xpt.storenum  with val(cstorenum) in xpt
		endif

		skip 1 in x856
		do case
		case trim(x856.segment) = "N2"  && name ext. info
			m.name = upper(alltrim(x856.f1))
			replace xpt.name with m.name in xpt
			skip 1 in x856
			if trim(x856.segment) = "N3"  && address info
				m.st_addr1 = upper(alltrim(x856.f1))
				replace xpt.address  with m.st_addr1 in xpt
				m.st_addr2 = upper(alltrim(x856.f2))
				replace xpt.address2 with m.st_addr2 in xpt
			endif
			skip 1 in x856
			if trim(x856.segment) = "N4"  && address info
				m.zipbar = upper(alltrim(x856.f3))
				m.st_csz = upper(alltrim(x856.f1))+", "+upper(alltrim(x856.f2))+" "+trim(x856.f3)
				replace xpt.csz with m.st_csz in xpt
				replace xpt.shipins with xpt.shipins+chr(13)+"COUNTRY*"+alltrim(x856.f4) in xpt
			endif
		case trim(x856.segment) = "N3"  && address info
			m.st_addr1 = upper(alltrim(x856.f1))
			replace xpt.address  with m.st_addr1 in xpt
			m.st_addr2 = upper(alltrim(x856.f2))
			replace xpt.address2 with m.st_addr2 in xpt
			skip 1 in x856
			if trim(x856.segment) = "N4"  && address info
				m.zipbar = upper(alltrim(x856.f3))
				m.st_csz = upper(trim(x856.f1))+", "+upper(trim(x856.f2))+" "+trim(x856.f3)
				replace xpt.csz with m.st_csz in xpt
				replace xpt.shipins with xpt.shipins+chr(13)+"COUNTRY*"+alltrim(x856.f4) in xpt
				replace xpt.country with alltrim(x856.f4) in xpt
			endif
		otherwise
			wait window "UNKNOWN ADDRESS SEGMENT" timeout 2
			throw
		endcase
		select x856
		skip
		loop
	endif

	if trim(x856.segment) = "N1" and inlist(trim(x856.f1),"Z7","SD","MA","PF") && Returned data
		cnom = alltrim(x856.f1)
		replace xpt.shipfor  with  alltrim(x856.f2) in xpt
		replace xpt.sforstore with alltrim(x856.f4) in xpt
		replace xpt.storenum  with val(cstorenum) in xpt
		replace xpt.shipins with xpt.shipins+chr(13)+cnom+"_N1*"+alltrim(x856.f2) in xpt
		skip 1 in x856

		do case
		case trim(x856.segment) = "N2"
			replace xpt.sforaddr1 with alltrim(x856.f1) in xpt
			replace xpt.shipins with xpt.shipins+chr(13)+cnom+"_N2*"+alltrim(x856.f1) in xpt
			skip 1 in x856
			if trim(x856.segment) = "N3"
				replace xpt.shipfor  with  alltrim(x856.f1) in xpt
				replace xpt.sforaddr1 with alltrim(x856.f1) in xpt
				replace xpt.sforaddr2 with alltrim(x856.f2) in xpt

				replace xpt.shipins with xpt.shipins+chr(13)+cnom+"_N3*"+alltrim(x856.f1)+"|"+alltrim(x856.f2) in xpt
				skip 1 in x856
			endif
			if trim(x856.segment) = "N4"
				replace xpt.shipins with xpt.shipins+chr(13)+cnom+"_N4*"+alltrim(x856.f1)+"|"+alltrim(x856.f2)+"|"+alltrim(x856.f3) in xpt
				replace xpt.sforcsz with alltrim(x856.f1)+", "+alltrim(x856.f2)+" "+alltrim(x856.f3) in xpt
			endif

		case trim(x856.segment) = "N3"
			replace xpt.shipfor  with  alltrim(x856.f1) in xpt
			replace xpt.sforaddr1 with alltrim(x856.f1) in xpt
			replace xpt.sforaddr2 with alltrim(x856.f2) in xpt
			replace xpt.shipins with xpt.shipins+chr(13)+cnom+"_N3*"+alltrim(x856.f1)+"|"+alltrim(x856.f2) in xpt
			skip 1 in x856
			if trim(x856.segment) = "N4"
				replace xpt.shipins with xpt.shipins+chr(13)+cnom+"_N4*"+alltrim(x856.f1)+"|"+alltrim(x856.f2)+"|"+alltrim(x856.f3) in xpt
				replace xpt.sforcsz with alltrim(x856.f1)+", "+alltrim(x856.f2)+" "+alltrim(x856.f3) in xpt
			endif

		case trim(x856.segment) = "N4"
			replace xpt.shipins with xpt.shipins+chr(13)+cnom+"_N4*"+alltrim(x856.f1)+"|"+alltrim(x856.f2)+"|"+alltrim(x856.f3) in xpt
			replace xpt.sforcsz with alltrim(x856.f1)+", "+alltrim(x856.f2)+" "+alltrim(x856.f3) in xpt
		endcase

		select x856
		skip
		loop
	endif

	if trim(x856.segment) = "N9"  && Additional info segments
		do case

		case alltrim(x856.f1) = "CR"  && ofr joutneys pack list
			replace xpt.shipins with xpt.shipins+chr(13)+"JOURNEYREF*"+alltrim(x856.f2) in xpt

		case alltrim(x856.f1) = "DP"  && Department
			replace xpt.dept with alltrim(x856.f2) in xpt

		case alltrim(x856.f1) = "CO"  && Customer Order number
			replace xpt.shipins with xpt.shipins+chr(13)+"CUSTORDER*"+alltrim(x856.f2) in xpt

		case alltrim(x856.f1) = "98" && Container/Packaging Specification Number
*REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"PRIORITY*"+ALLTRIM(x856.f2) IN xpt

		case alltrim(x856.f1) = "12"  && Billing account
			replace xpt.shipins with xpt.shipins+chr(13)+"CUSTACCT*"+alltrim(x856.f2) in xpt  && use to be the cacctnum
			replace xpt.upsacctnum with alltrim(x856.f2) in xpt

		case alltrim(x856.f1) = "DU"  && Billing account
			replace xpt.duns with alltrim(x856.f2) in xpt

		case alltrim(x856.f1) = "15"  && used for label template selection
			replace xpt.cacctnum with alltrim(x856.f2) in xpt
			replace xpt.shipins with xpt.shipins+chr(13)+"LABEL*"+alltrim(x856.f2) in xpt

		case alltrim(x856.f1) = "14"  && Master Account Number
			replace xpt.shipins with xpt.shipins+chr(13)+"MASTERACCT*"+alltrim(x856.f2) in xpt

		case alltrim(x856.f1) = "OP"  && Order priority/type
			replace xpt.batch_num with alltrim(x856.f2) in xpt
			replace xpt.shipins with xpt.shipins+chr(13)+"BATCHNUM*"+alltrim(x856.f2) in xpt
*      Replace xpt.consignee With  Alltrim(x856.f2)+"~"+Alltrim(xpt.consignee) In xpt  && removed dy 9/22/17

		case alltrim(x856.f1) = "ST"  && Original Purchase Order
			replace xpt.shipfor with alltrim(x856.f2) in xpt
			replace xpt.shipins with xpt.shipins+chr(13)+"STORENAME*"+alltrim(x856.f2) in xpt

		endcase
	endif

	if trim(x856.segment) = "G62" and trim(x856.f1) == "37"  && start date
		lcdate = alltrim(x856.f2)
		lxdate = substr(lcdate,5,2)+"/"+substr(lcdate,7,2)+"/"+left(lcdate,4)
		lddate = ctod(lxdate)
		store lddate to dstart
		replace xpt.start with lddate in xpt
		select x856
		skip
		loop
	endif

	if trim(x856.segment) = "G62" and trim(x856.f1) == "38"  && cancel date
		lcdate = alltrim(x856.f2)
		lxdate = substr(lcdate,5,2)+"/"+substr(lcdate,7,2)+"/"+left(lcdate,4)
		lddate = ctod(lxdate)
		store lddate to dcancel
		replace xpt.cancel with lddate in xpt
		select x856
		skip
		loop
	endif

	llprepackflag = .f.

	if trim(x856.segment) = "W66"
		if alltrim(x856.f1) = "PP"
			llprepackflag = .t.
		endif
		replace carrcode with alltrim(x856.f5) in xpt
		lcshipcode =alltrim(x856.f10)

		if xsqlexec("select * from altscac where accountid=6757 and shipmode='"+alltrim(lcshipcode)+"'",,,"wh") # 0
			replace carrcode with alltrim(altscac.shipmode) in xpt
			replace ship_via with alltrim(altscac.shipname) in xpt
			replace scac     with alltrim(altscac.scaccode) in xpt
		else
			replace carrcode with "UNK" in xpt
			replace ship_via with "UNKNOWN" in xpt
			replace scac     with "" in xpt
		endif

		select x856
		skip
		loop
	endif

	if trim(x856.segment) = "LX" && start of PT detail, stay here until this PT is complete
		lnLXCtr = val(trim(x856.f1))
		assert .f. message cship_ref+" Detail, Type = "+iif(lprepack,"Prepack","Pickpack")+"...debug"
		m.units = iif(lprepack,.f.,.t.)
		llprepackflag = .f.

		wait "NOW IN DETAIL LOOP PROCESSING" window nowait

		select xptdet
		calculate max(ptdetid) to m.ptdetid
		m.ptdetid = m.ptdetid + 1

		select xptdet
		skip 1 in x856
		lnlinenum=1
		do while alltrim(x856.segment) != "SE"
			csegment = trim(x856.segment)
			ccode = trim(x856.f1)

			if trim(x856.segment) = "W01" && Destination Quantity
				lcitemtype = alltrim(x856.f2)  && either CA or EA
				if lcitemtype = "CA"
					m.units = .f.
					llhasprepacks = .t.
					lnprepackqty = val(trim(x856.f1))
					lcmasterupc  = alltrim(x856.f7)
					lcvendorsku  = alltrim(x856.f16)
					select curppk
					zap
					select x856
				else
					m.units = .t.
				endif
				lncureentrec = recno("x856")
				skip 2 in x856
				if trim(x856.segment) = "N9" and trim(x856.f1) = "MR"
*          REPLACE xpt.qty WITH 0 IN xpt
					llprepackflag = .t.
					skip -1 in x856
					cprtstr = "DESC*"+upper(trim(x856.f1))
					replace xptdet.printstuff with xptdet.printstuff+chr(13)+cprtstr in xptdet
					skip 1 in x856
					do while trim(x856.segment) = "N9" and trim(x856.f1) = "MR"
						lcquery = [select * from upcmast where upc = ']+trim(x856.f2)+[']
						xsqlexec(lcquery,"xupcmast",,"wh")
						select xupcmast
						if reccount("xupcmast") > 0
							lcstyle = xupcmast.style
							lcsize  = xupcmast.id
							lccolor = xupcmast.color
						else

						endif
						insert into curppk (upc,style,color,id,qty) values (trim(x856.f2),lcstyle,lccolor,lcsize,val(trim(x856.f3)))
						skip 1 in x856
					enddo

&& let roll out the detail lines
					lcprepackstr  = "PREPACKUPC*"+alltrim(lcmasterupc)+chr(13)
          lcprepackstr  = lcprepackstr+"PREPACKLINE*"+transform(lnLXCtr)+chr(13)
					lcprepackstr  = lcprepackstr+"PREPACKSKU*"+alltrim(lcvendorsku)+chr(13)
					lcprepackstr  = lcprepackstr+"PREPACKQTY*"+transform(lnprepackqty)+chr(13)

					select curppk
					go top
					scan
						lcprepackstr = lcprepackstr + alltrim(transform(curppk.qty))+" "+alltrim(curppk.upc)+"-"+"Style:"+ alltrim(curppk.style)+"  Color: "+alltrim(curppk.color)+" Size: "+ alltrim(curppk.id)+"  Qty: "+transform(curppk.qty)+chr(13)
					endscan

					select curppk
					go top
					scan
						replace curppk.qty with curppk.qty * lnprepackqty
					endscan

					select curppk
					locate
					if ltesting
						browse
					endif
					scan
						select xptdet
						append blank
						replace xptdet.ptid     with xpt.ptid  in xptdet
						replace xptdet.ptdetid  with m.ptdetid in xptdet
						replace xptdet.units    with .t.       in xptdet
						replace xptdet.linenum  with alltrim(transform(lnLXCtr)) in xptdet
						replace xptdet.ship_ref with xpt.ship_ref  in xptdet
						m.ptdetid = m.ptdetid + 1
						replace xptdet.accountid with xpt.accountid in xptdet
						replace xptdet.pack  with "1" in xptdet
						replace xptdet.style with curppk.style in xptdet
						replace xptdet.color with curppk.color in xptdet
						replace xptdet.id    with curppk.id    in xptdet
						replace xptdet.upc   with curppk.upc   in xptdet
						replace xptdet.size_scale with "1"     in xptdet  && this used later to ensure that prepack detail lines are first during rollup

						replace xptdet.totqty  with curppk.qty in xptdet
						replace xpt.qty        with xpt.qty + xptdet.totqty in xpt
						replace xpt.origqty    with xpt.qty in xpt
						replace xptdet.origqty with xptdet.totqty in xptdet

						replace xptdet.custsku    with lcvendorsku  in xptdet
						replace xptdet.shipstyle  with lcvendorsku  in xptdet
						replace xptdet.printstuff with lcprepackstr in xptdet
*            Replace xptdet.printstuff With xptdet.printstuff+Chr(13)+"MASTERUPC*"+lcMasterUPC In xptdet
*            Replace xptdet.printstuff With xptdet.printstuff+Chr(13)+"VENDORSKU*"+lcVendorSKU In xptdet
*            Replace xptdet.printstuff With xptdet.printstuff+Chr(13)+"PREPACK*"+lcVendorSKU   In xptdet

					endscan
					replace shipins with shipins+chr(13)+ lcprepackstr+"ENDPREPACK"+chr(13) in xpt
					select x856
					skip -1 in x856
					lcmiscstr =""
					do while trim(x856.segment) != "W20"
						if trim(x856.segment) = "N9" and trim(x856.f1) = "VSZ"
							lcmiscstr = lcmiscstr+chr(13)+"PREPACKSIZE*"+trim(x856.f2)
						endif
						if trim(x856.segment) = "N9" and trim(x856.f1) = "VCL"
							lcmiscstr=lcmiscstr+chr(13)+"PREPACKCOLOR*"+trim(x856.f2)
						endif
						if trim(x856.segment) = "N9" and trim(x856.f1) = "CD"
							lcmiscstr=lcmiscstr+chr(13)+"PREPACKCD*"+trim(x856.f2)
						endif
						skip 1 in x856
					enddo
					select xptdet
					scan for alltrim(xptdet.linenum) = alltrim(str(lnLXCtr))
						replace xptdet.printstuff with xptdet.printstuff+lcmiscstr in xptdet
					endscan
				else
					skip -2 in x856
				endif

				if llprepackflag = .f.
					select xptdet
					append blank

					replace xptdet.ptid    with xpt.ptid in xptdet
					replace xptdet.ptdetid with m.ptdetid in xptdet
					replace xptdet.units   with m.units in xptdet
					replace xptdet.linenum with alltrim(str(lnlinenum)) in xptdet
					lnlinenum = lnlinenum + 1
					m.ptdetid = m.ptdetid + 1
					replace xptdet.accountid with xpt.accountid in xptdet
					cusepack = "1"
					replace xptdet.size_scale with "2"     in xptdet  && this used later to ensure that prepack detail lines are first during rollup
					replace xptdet.pack with cusepack in xptdet
					replace xptdet.ship_ref with xpt.ship_ref  in xptdet
					m.casepack = int(val(cusepack))
					replace xptdet.casepack with m.casepack in xptdet

*!*          IF !lPrepack  && Pickpack
					replace xptdet.totqty with int(val(x856.f1)) in xptdet
*!*          ENDIF
					replace xpt.qty with xpt.qty + xptdet.totqty in xpt
					replace xpt.origqty with xpt.qty in xpt
					replace xptdet.origqty with xptdet.totqty in xptdet

					if empty(trim(xptdet.printstuff))
						replace xptdet.printstuff with "UOM*"+alltrim(x856.f2) in xptdet
					else
						replace xptdet.printstuff with xptdet.printstuff+chr(13)+"UOM*"+alltrim(x856.f2) in xptdet
					endif

					if alltrim(x856.f4) = "IN"
						m.style = alltrim(x856.f5)
						replace xptdet.style with alltrim(x856.f5) in xptdet
					endif

					if alltrim(x856.f6) = "UP"
						m.upc = alltrim(x856.f7)
						thisupc = alltrim(x856.f7)
						replace xptdet.upc with alltrim(x856.f7) in xptdet
					endif

					if alltrim(x856.f15) = "VN"
						m.shipstyle = alltrim(x856.f16)
						m.custsku   = alltrim(x856.f16)
						replace xptdet.shipstyle with alltrim(x856.f16) in xptdet
						replace xptdet.custsku with alltrim(x856.f16) in xptdet
					endif
				endif

			endif

			if trim(x856.segment) = "G69" && desc
				cprtstr = "DESC*"+upper(trim(x856.f1))
				replace xptdet.printstuff with xptdet.printstuff+chr(13)+cprtstr in xptdet
			endif


			if trim(x856.segment) = "N9"  and alltrim(x856.f1) = "VSZ"
				m.id = alltrim(x856.f2)
				replace xptdet.id with alltrim(x856.f2) in xptdet
			endif

			if trim(x856.segment) = "N9"  and alltrim(x856.f1) = "VCL"
				m.color = alltrim(x856.f2)
				replace xptdet.color with alltrim(x856.f2) in xptdet
			endif

			if trim(x856.segment) = "W20"
*set step On
				if lcitemtype = "CA"
					replace xptdet.pack with alltrim(x856.f1) in xptdet
					replace xptdet.casepack with val(alltrim(x856.f1)) in xptdet
				endif
				if llprepackflag = .f.
					replace xpt.shipins with xpt.shipins+chr(13)+"Next Line is UPC Pack Qty"  in xpt
					replace xpt.shipins with xpt.shipins+chr(13)+alltrim(thisupc)+"*"+alltrim(x856.f1) in xpt
				endif
			endif

			select x856
			skip 1 in x856
			llprepackflag = .f.
		enddo

		select xptdet
	endif

	if llhasprepacks = .t.
		replace xpt.batch_num with "PREPACK" in xpt
	endif

	select x856
	if !eof()
		skip 1 in x856
	endif
enddo

*!*  Select xptdet  && PG removed this 8/29/2017
*!*  Sum(totqty) To lnPTTotal
*!*  Replace xpt.qty With lnPTTotal In xpt

select xptdet
index on size_scale to pkorder
set order to pkorder

*Set Step On
do rollup_ptdet_units
*rollup()
wait window "At end of ROLLUP procedure" nowait
*lbrowfiles = .t.
*Set Step On

if lbrowfiles or ltesting
	wait window "If tables are incorrect, CANCEL when XPTDET window opens"+chr(13)+"Otherwise, will load PT table" timeout 2
	select xpt
	locate
	browse
	select xptdet
	locate
	browse
*  cancel
endif

wait window ccustname+" Breakdown Round complete..." nowait
select x856
if used("PT")
	use in pt
endif
if used("PTDET")
	use in ptdet
endif
wait clear
select xpt
replace all office with "L" in xpt
replace all mod with "L" in xpt

select xptdet

replace all office with "L" in xptdet
replace all mod with "L" in xptdet

*!*  Select ptid From xpt Into Cursor justptids Group By ptid

*!*  Select justptids
*!*  Go top
*!*  Scan
*!*    Select xptdet
*!*    Sum totqty To lnptqty For xptdet.ptid = justptids.ptid
*!*    Select xpt
*!*    replace All qty With lnptqty For xpt.ptid = justptids.ptid In xpt
*!*  Endscan



*Set Step On
return

*************************************************************************************************
*!* Error Mail procedure - Cancel Before Start Date
*************************************************************************************************
procedure errordates

if lemail
	tsendto = "pgaidis@fmiint.com"   &&tsendtotest
	tcc =  "pgaidis@fmiint.com" &&tcctest
	cmailloc = iif(coffice = "C","West",iif(coffice = "N","New Jersey","Florida"))
	tsubject= "TGF "+cmailloc+" "+ccustname+" Pickticket Error: " +ttoc(datetime())
	tmessage = "File: "+cfilename+chr(13)+" has Cancel date(s) which precede Start date(s)"
	tmessage = tmessage + "File needs to be checked and possibly resubmitted."

	if llemailtest
		do form m:\dev\frm\sendmail_au with tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A",.t.
	else
		do form m:\dev\frm\dartmail2 with tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
	endif

endif
endproc

******************************
procedure rollup
******************************
assert .f. message "In rollup procedure"
nchangedrecs = 0
select xptdet
set deleted on
locate
if ltesting
*BROWSE
endif
cmsgstr = "PT/OSID/ODIDs changed: "
scan for !deleted()
	scatter memvar
	nrecno = recno()
	locate for xptdet.ptid = m.ptid ;
	and xptdet.style = m.style ;
	and xptdet.color = m.color ;
	and xptdet.id = m.id ;
	and xptdet.pack = m.pack ;
	and xptdet.upc = m.upc ;
	and recno('xptdet') > nrecno
	if found()
		select xpt
		locate for xpt.ptid = m.ptid
		if !found()
			wait window "PROBLEM!"
		endif
		cship_ref = allt(xpt.ship_ref)
		if nchangedrecs > 10
			cmsgstr = "More than 10 records rolled up"
		else
			cmsgstr = cmsgstr+chr(13)+cship_ref+" "+transform(m.ptid)+" "+transform(m.ptdetid)
		endif
		nchangedrecs = nchangedrecs+1
		select xptdet
		ntotqty1 = xptdet.totqty
		delete next 1 in xptdet
	endif
	if !eof()
		go nrecno
		replace xptdet.totqty with xptdet.totqty+ntotqty1 in xptdet
		replace xptdet.origqty with xptdet.totqty in xptdet
	else
		go nrecno
	endif
endscan

if nchangedrecs > 0
	wait window cmsgstr timeout 1
	wait window "There were "+trans(nchangedrecs)+" records rolled up" timeout 1
else
	wait window "There were NO records rolled up" timeout 1
endif

endproc
