*!* FTP_EDI.PRG
*!* Sets up OUTBOUND EDI Connection and
*!* sends confirmation or "bad FTP" eMails.
*
* Added support for protocols other than  FTP, and setting Port, both defined in ftpsetup 2/9/2018 MB

*First specify a variable called Mysite
Parameter lcwhichsite,tuserid
Local mysite
LOCAL lcProtocol, lnPort  && 2/9/2018 MB

set excl off
set safety off
set talk off

pnnotsent = 0
tsubject = ""
tmessage = ""
tattach = ""
lncount = 0
llisregatta = .F.
lcregattaloc = ""

*!* This traps any non-connects outside CuteFTP's handling
on error do errorhandle with error()

public ftperror
ftperror = .f.

if !used("ftpedilog")
  use f:\edirouting\ftpedilog in 0 alias ftpedilog
endif
if !used("ftpsetup")
  use f:\edirouting\ftpsetup in 0 alias ftpsetup
endif

select ftpjobs
replace ftpjobs.tries with ftpjobs.tries+1
lnftptries = ftpjobs.tries

select ftpsetup
**added padr() code to get exact match - ie, "XXX" isnt the same as "XXX-999" - mvw 05/02/12
locate for transfer = padr(lcwhichsite,len(ftpsetup.transfer)," ")
if !found() && if transfer name does not exist in ftpsetup
	Wait "This transfer "+lcwhichsite+" is not defined in f:\edirouting\ftpsetup.dbf.... Aborting!" Window Timeout 3

	lcftpfile = "Aborted: Not defined in FTPSETUP..."
	lcftptype = "UNK"
	Do badftp

	tsubject= "Job Not Defined in FTPSETUP - FTP Aborted"
	tmessage = "Transfer '"+lcwhichsite+"' is not defined in f:\edirouting\ftpsetup.dbf. FTP Aborted! Check FTPSETUP and repair"
	Do errorhandle

	Return
else
	dpath = alltrim(ftpsetup.localpath)
	xaccount=alltrim(ftpsetup.account)

	If !Directory(dpath) && if local folder is missing
		Wait "This LOCAL directory "+dpath+" for transfer "+alltrim(ftpsetup.transfer)+" does not exist... Aborting!" Window Timeout 3

		lcftpfile = "Aborted: Directory Not Found..."
		lcftptype = "UNK"
		Do badftp

		tsubject= "Directory Doesn't Exist - FTP Aborted"
		tmessage = "The directory "+dpath+" for "+lcwhichsite+;
		  " does not exist... FTP Aborted! Please check and repair directory info for this transfer"
		Do errorhandle

		Return
	Endif

	Set Default To &dpath
	lcpath=Alltrim(ftpsetup.localpath)
	lcarchivepath=Alltrim(ftpsetup.archive)

	If !Directory(lcarchivepath) && if archive folder is missing
		Wait "This ARCHIVE directory "+lcarchivepath+" does not exist... Aborting!" Window Timeout 3

		lcftpfile = "Aborted: Archive DIR Not Found..."
		lcftptype = "UNK"
		Do badftp

		tsubject= "Archive Path Doesn't Exist - FTP Aborted"
		tmessage = "The archive directory "+lcarchivepath+" for "+lcwhichsite+;
		  " does not exist... FTP Aborted! Please check and repair directory info for this transfer"
		Do errorhandle
		Return
	Endif

	lcremotefolder     = Alltrim(ftpsetup.rfolder)
	lcaccountname      = Alltrim(ftpsetup.account)
	lceditype          = Alltrim(ftpsetup.editype)
	lcthissite         = Alltrim(ftpsetup.site)
	lcthislogin        = Alltrim(ftpsetup.login)
	lcthispassword     = Alltrim(ftpsetup.Password)
	lcDataChan         = UPPER(Alltrim(ftpsetup.datachan))
	
	lcProtocol         = UPPER(ALLTRIM(ftpsetup.protocol)) && 2/9/2018 MB
	lnPort             = ftpsetup.port && 2/9/2018 MB
		
Endif

lcoldcaption = _Screen.Caption
lcwhichsite = Alltrim(lcwhichsite)
_Screen.Caption    = Alltrim(ftpsetup.Message)

Select ftpedilog
Wait Window At 10,10 Chr(13)+_Screen.Caption+Chr(13)+"Connecting to "+lcthissite+Chr(13)+"User ID: "+tuserid+Chr(13) Nowait

*Creating a connection object and assign it to the variable
mysite = Createobject("CuteFTPPro.TEConnection")

* 2/9/2018 MB
*mysite.protocol = "FTP"
IF NOT EMPTY(lcProtocol) THEN
	mysite.protocol = lcProtocol
ELSE
	mysite.protocol = "FTP"
ENDIF
IF lnPort > 0 THEN
	mysite.port = lnPort
ENDIF

mysite.Host     = lcthissite
mysite.login    = lcthislogin
mysite.Password = lcthispassword
mysite.retries = 3 && Can be adjusted if it causes timeouts
mysite.Delay = 30 && Can also be adjusted if it causes timeouts
mysite.useproxy = "BOTH"

*mysite.datachannel="PORT"
IF NOT EMPTY(lcDataChan) THEN
	mysite.datachannel = lcDataChan
ENDIF

mysite.Connect

lcmessage = mysite.Wait(-1,10000)

If !Isblank(lcmessage)
	Wait Window "Transfer message "+lcmessage Timeout 2
Endif

do case
case Atc("ERROR",lcmessage)>0 && if CuteFTP Pro can't make a socket connection
	lcftpfile = "Aborted: Transfer Error..."
	lcftptype = "UNK"
	Do badftp

	tsubject = "FTP Socket Connect Error inside Cute FTP Pro TE"
	tmessage = "FTP Connection failure: "+lcwhichsite+", User: "+tuserid
	Do errorhandle
	Return

case mysite.isconnected >=0 && If CuteFTP Pro has another connection error
	Wait "Could not connect to: " + mysite.Host + " Aborting!" Window Timeout 3

	lcftpfile = "Aborted: Transfer Error..."
	lcftptype = "UNK"
	Do badftp

	tsubject = "FTP Non-Connect Error inside Cute FTP Pro TE"
	tmessage = "Connection failure: "+lcwhichsite+", User: "+tuserid
	Do errorhandle
	Return

otherwise
	Wait "You are now connected to "+mysite.Host Window Timeout 3
endcase

Wait Window At 10,10 Chr(13)+"Setting up folders.........."+Chr(13) Timeout 3

If Empty(lcremotefolder)
	mysite.remotefolder = ""
* when we login we are at our home folder
Else
	b=mysite.remoteexists(lcremotefolder)

	If b>=0 Then
		Wait "Home directory not found...."+lcremotefolder+" for "+lcwhichsite Window Timeout 3
		lcftpfile = "Aborted: No Home DIR..."
		lcftptype = "UNK"
		Do badftp

		tsubject = "Missing Remote Folder - FTP Aborted"
		tmessage = "Home Directory "+lcremotefolder+" Not Found For "+lcwhichsite
		Do errorhandle
		Return
	Else
		mysite.remotefolder = Alltrim(lcremotefolder)
	Endif
Endif

mysite.localfolder = lcpath
lnnum = Adir(tarray)

**set order to filename desc so wave files "OW" get sent before Tote file "IC" - mvw 08/28/17
if upper(lcwhichsite)="SDI-PUTWALL-PUT"
	asort(tarray,1,lnnum,1)
endif

***************************************************************************************************
* now start uploading the file
***************************************************************************************************
archivename = Ttoc(Datetime(),1)  && replaces code above

Store "" To XFILE,ARCHIVEFILE

If FTPERROR
	Return
Endif

If lnnum >= 1
	For thisfile = 1  To lnnum
		If FTPERROR
			Return
		Endif
		If tarray[thisfile,2] < 10
			Loop
		Endif
		cFilename = alltrim(tarray[thisfile,1])

		do case
		case upper(xaccount)="AMERICAN EAGLE"
			xfile = lower(addbs(lcpath)+cfilename)
		**for PO out to Toll for BIOWORLD, need csv extension in lowercase, per Bill Lillicrap - mvw 09/04/13
		case upper(xaccount)="BIOWORLD" and upper(right(alltrim(cfilename),3))="CSV"
			xfile = addbs(lcpath)+left(cfilename,len(cfilename)-3)+lower(right(cfilename,3))
		otherwise
			xfile = addbs(lcpath)+cfilename
		endcase

		archivefile = addbs(lcarchivepath)+cfilename
		If ftpjobs.jobname = "EFREIGHT-AMSXFER    "
			Select ftpedilog
			Append Blank
			Replace ftpedilog.ftpdate   With Datetime()
			Replace ftpedilog.transfer  With "START XML XFER"
			Replace ftpedilog.xfertype  With ""
			Replace ftpedilog.filename  With XFILE
			Replace ftpedilog.acct_name With lcaccountname
			Replace ftpedilog.arch_name With ARCHIVEFILE
			Replace ftpedilog.Type      With lceditype
		Endif

		if ftperror
			return
		endif
		Wait Window At 10,10 Chr(13)+"       FTPing file: "+XFILE+Chr(13) Nowait
		If File(XFILE)
			Wait Window At 10,10 Chr(13)+"     Now uploading "+XFILE+Chr(13) Nowait
			llTransferOK = .T.

			mysite.upload(xfile)

			**must transfer the file and then rename so they dont pick up an unfinished file - mvw 08/29/17
			if upper(lcwhichsite)="SDI-PUTWALL-PUT"
				xxfile=alltrim(justfname(xfile))
				xnewfile=left(xxfile,len(xxfile)-3)+"dat"

**wildcards do not work with remoterename(), added auto rename of .tmp files to .dat in cuteftppro properties
**  TOOLS --> GLOBAL OPTIONS --> TRANSFERS --> RENAME RULES

*				mysite.remoterename(xxfile,xnewfile)
*				mysite.remoterename("*.tmp","*.tmp2")
				if mysite.remoteexists(xnewfile)>=0
*					messagebox("rename failed")
				endif
			endif

			**for AE all files need to be uploaded to the INBOUND and the INBOUND\BACKUP folders - mvw 04/26/12
			if upper(xaccount)="AMERICAN EAGLE"
				Wait Window At 10,10 "Now uploading "+xfile+" to BACKUP directory" Nowait
				xBUPath="BACKUP"
				if mysite.remoteexists(xBUPath)<0
					mysite.remotefolder = xBUPath
					mysite.upload(xfile)

					**make sure backup files actaully get transferred - mvw 06/07/12
					**get just the filename w/o path
					xbufile=right(alltrim(xfile),len(alltrim(xfile))-rat("\",alltrim(xfile)))
					if mysite.remoteexists(xbufile)>=0
						tsubject = "AMERICAN EAGLE - File Never Made It To Backup Folder! Processing "+lcwhichsite
						tmessage = tsubject
						Do errormail
					endif

					**set back to original remote folder - need "/" to set to absolute path vs "BACKUP" which sets to folder in the existing remote folder
					mysite.remotefolder = "/"+Alltrim(lcremotefolder)
				else
					**make sure backup files actaully get transferred - mvw 06/07/12
					tsubject = "AMERICAN EAGLE - Backup Folder Not Found! Processing "+lcwhichsite
					tmessage = tsubject
					Do errormail
				endif
			endif

			strResult = mysite.Wait(-1,5000)
			Do Case
			Case "CANCELLED"$strResult
				Wait Window At 10,10 Chr(13)+" FTP Operation cancelled......."+strResult+Chr(13) Timeout 2
				llTransferOK = .F.
			Case "FINISHED"$strResult
				Wait Window At 10,10 Chr(13)+" FTP Operation complete......."+strResult+Chr(13) Timeout 1
			Case "ERROR"$strResult
				Wait Window At 10,10 Chr(13)+" FTP Operation Error......"+strResult+Chr(13) Timeout 2
				llTransferOK = .F.
			Case "SKIPPED"$strResult
				Wait Window At 10,10 Chr(13)+" FTP Operation skipped......."+strResult+Chr(13) Timeout 2
			Otherwise
				Wait Window At 10,10 Chr(13)+" Unknown FTP Operation return messaged......."+strResult
				llTransferOK = .F.
			Endcase

			If FTPERROR Or !llTransferOK
				Return
			Endif

			Wait Window At 10,10 Chr(13)+"     Transfer message for "+lcwhichsite+": "+lcmessage+"    "+Chr(13) Timeout 2

			Select ftpedilog
			Append Blank
			Replace ftpedilog.ftpdate   With Datetime()
			Replace ftpedilog.transfer  With ftpjobs.jobname
			Replace ftpedilog.xfertype  With "PUT"
			Replace ftpedilog.filename  With XFILE
			Replace ftpedilog.acct_name With lcaccountname
			Replace ftpedilog.arch_name With ARCHIVEFILE
			Replace ftpedilog.Type      With lceditype
			Wait Window At 10,10 Chr(13)+"     FTP Log Updated for file "+XFILE+Chr(13) Timeout 2
			If pnnotsent = 0
				Copy File [&XFILE] To [&ARCHIVEFILE]
				If File([&ARCHIVEFILE])
					Delete File [&XFILE]
				Endif
			Endif
		Else
			Wait Window XFILE+"  not found  !!!!!!!!!!!! " Timeout 3
		Endif
		Store "" To XFILE,ARCHIVEFILE
	Next thisfile

	Select ftpjobs
	Replace terminated With .F.
	Replace Complete With .T.
	Replace busy With .F.
	Replace exectime With Datetime()
	Delete
Endif

*mysite.tecommand("deletefinished")
mysite.Close

******************************************************************************************

If pnnotsent = 0
	If ftpsetup.do_email = .T.
		tattach=""
		lnnum = Adir(tarray)
		If lnnum >= 1
			For thisfile = 1  To lnnum
				XFILE = lcpath+tarray[thisfile,1]
				If File(XFILE)
					tattach = tattach+XFILE+";"
				Endif
			Next thisfile
		Endif
	Endif

	Wait Window At 10,10 Chr(13)+"   "+_Screen.Caption+"   FTP Complete.........      "+Chr(13) Timeout 2
	lcmessage = _Screen.Caption+"   Data Transfer Complete............"
Else
	Wait Window At 10,10 Chr(13)+"   "+_Screen.Caption+"   FTP Aborted.........      "+Chr(13) Timeout 2
	lcmessage = _Screen.Caption+"   Data Transfer Incomplete for"+lcwhichsite

	lcftpfile = "Aborted: Data Xfer Incomplete..."
	lcftptype = "UNK"
	Do badftp

	tsubject = "Data Transfer Incomplete "+lcwhichsite+" - FTP Aborted"
	tmessage = "Data Transfer was not completed for "+lcwhichsite
	Do errorhandle
Endif

Select ftpsetup
If ftpsetup.sendmsg = .T. And Len(Alltrim(tuserid)) >2
	!net Send &tuserid &lcmessage
Endif

Select ftpsetup
Use In ftpsetup

Select ftpedilog
Use In ftpedilog

Select ftpjobs
Replace Complete With .T.
Replace terminated With .F.
Replace busy With .F.
If Complete
	Delete
Endif

If lncount = 1
	On Error
Endif

Wait Clear
_Screen.Caption    = lcoldcaption

Return



********************************************************************************************************************
*!* COMMENT FROM HERE DOWN IF NOT WORKING
********************************************************************************************************************
Procedure errorhandle
Parameters lcerror1


FTPERROR = .T.

lcerrorstr = Alltrim(Str(lcerror1))
Aerror(errtemp)

If !Used("ftperrorlog")
	Use F:\edirouting\ftperrorlog In 0 Alias ftperrorlog
Endif
lcCWA = Alias()

Select ftperrorlog
Append From Array errtemp
Replace ftperrorlog.errdate With Datetime()
Replace ftperrorlog.jobname With ftpsetup.transfer
Use In ftperrorlog
Select &lcCWA
pnnotsent = 1

If lcerror1>1400
	pnnotsent = 1
	Wait "Running Procedure ERRORHANDLE"+Chr(13)+"Connection failed - Error: "+lcerrorstr+;
		CHR(13)+"Notification Sent..." Window At 10,10 Timeout 7

	If lncount = 0
		Do Case
		Case lncount  = 2 && remove this after retry-limit approval finalized

		Case lnFTPTries => 10 And llisregatta = .F.
			lcftpfile = "Aborted: FoxPro ErrCode...FINAL"
			lcftptype = "UNK"
			Do badftp
			tsubject = "URGENT!!!  FTP Aborted at "+Ttoc(Datetime(),1)
			tmessage = "VFP FTP Error: "+lcerrorstr+"; "+lcwhichsite+;
			  "...FTP Aborted. Contact sender to re-submit...possible remote FTP Site error."
			Select ftpjobs
			Delete
			Replace ftpjobs.Complete With .T.
			Replace ftpjobs.terminated With .T.
			Replace ftpjobs.busy With .F.
		Otherwise
			Replace ftpjobs.nextrun With Datetime() + 600 && Adds 10 minutes to next-run time, other than Regatta
			Replace ftpjobs.busy With .F.
			lcftpfile = "Aborted: FoxPro ErrCode Trapped..."
			lcftptype = "UNK"
			Do badftp
			tsubject = "Internal FoxPro Error Codes - FTP Aborted for "+lcwhichsite
			tmessage = "Trapped VFP FTP Error: "+lcerrorstr+"; "+lcwhichsite+"...FTP Aborted. Will retry."
			Do errormail
		Endcase

		lncount = 1
	Endif
	Wait Clear
	Return To Master
Endif

Endproc



********************************************************************************************************************
Procedure errormail

tattach = ""
tfrom ="joe.bianchi@tollgroup.com"

*tsendto = "sburns@fmiint.com,joe.bianchi@tollgroup.com"
tsendto = "joe.bianchi@tollgroup.com"
If occurs("EMODAL",lcwhichsite)>0 or occurs("AEO-850",lcwhichsite)>0
	tsendto = "mwinter@fmiint.com"
	tcc = ""
Else
	tcc = "pgaidis@fmiint.com"
Endif

Do Form  dartmail2 With tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"

Endproc



********************************************************************************************************************
Procedure badftp

Select ftpedilog
Append Blank
Replace ftpedilog.ftpdate     With Datetime()
Replace ftpedilog.filename    With lcftpfile
Replace ftpedilog.acct_name   With lcwhichsite
Replace ftpedilog.Type        With lcftptype
Replace ftpedilog.xfertype    With "PUT"
Replace ftpedilog.transfer    With ftpjobs.jobname
Replace ftpedilog.arch_name   With "PUT-FAILED, ERROR ="+Alltrim(Transform(lcerror1))

Select ftpjobs
Replace ftpjobs.errcode        With lcerror1
Replace ftpjobs.busy With .F.
endproc
