CLOSE DATABASES ALL

goffice="L"

xsqlexec("select * from pt where mod='L'",,,"wh")
xsqlexec("select * from ptdet where mod='L'",,,"wh")

USE F:\3pl\DATA\trigger945accts IN 0 ALIAS trigs
cPtString = ""
SELECT trigs
SCAN FOR GROUP = "MORET" AND office="L"
	nAcctNum = trigs.accountid
	WAIT WINDOW "Now scanning "+ALLT(STR(nAcctNum)) NOWAIT
	SELECT pt
	FOR i = 1 TO 2
		IF i = 1
			cBatch = "C"
		ELSE
			cBatch = "U"
		ENDIF

		SCAN FOR pt.accountid = nAcctNum AND batch_num = cBatch
			SELECT ptdet
			SCAN FOR ptdet.ptid = pt.ptid
				IF cBatch = "C" AND ptdet.units
					WAIT WINDOW "Mismatched units type in PTDET at PTDETID "+ALLT(STR(ptdetid))+;
						CHR(13)+"Pickticket: "+pt.ship_ref NOWAIT
					cPtString = cPtString+CHR(13)+ALLT(pt.ship_ref)
				ENDIF
				IF cBatch = "U" AND !ptdet.units
					WAIT WINDOW "Mismatched units type in PTDET at PTDETID "+ALLT(STR(ptdetid))+;
						CHR(13)+"Pickticket: "+pt.ship_ref NOWAIT
					cPtString = cPtString+CHR(13)+ALLT(pt.ship_ref)
				ENDIF
			ENDSCAN
		ENDSCAN
	ENDFOR
ENDSCAN

IF !EMPTY(ALLT(cPtString))
	STRTOFILE(cPtString,"c:\tempfox\moreterr.txt")
	MODIFY FILE "c:\tempfox\moreterr.txt"
ELSE
	WAIT WINDOW "All Moret PT records are clean" TIMEOUT 2
ENDIF

CLOSE DATABASES ALL
RETURN
