* HRMILESTONESRPT.PRG
* produce monthy milestones report - run once late in month.

* Build EXE as F:\UTIL\HR\F:\UTIL\HR\MILESTONES\HRMILESTONESRPT.exe

LOCAL loHRMILESTONESRPT

runack("HRMILESTONESRPT")

utilsetup("HRMILESTONESRPT")

loHRMILESTONESRPT = CREATEOBJECT('HRMILESTONESRPT')

loHRMILESTONESRPT.MAIN()

*!*	schedupdate()

CLOSE DATABASES ALL
RETURN


#DEFINE LOGIT 1
#DEFINE WAITIT 2
#DEFINE NOWAITIT 4
#DEFINE SENDIT 8
#DEFINE CRLF CHR(13) + CHR(10)
#DEFINE RETURN_DATA_MANDATORY .F.
#DEFINE RETURN_DATA_NOT_MANDATORY .T.
#DEFINE CSVFORMAT 6
#DEFINE WRITE_ONLY_UNBUFFERED 11
#DEFINE COMMA ","
#DEFINE xlEdgeBottom 9
#DEFINE xlContinuous 1
#DEFINE xlMedium -4138
#DEFINE xlFONT_RED -16776961

DEFINE CLASS HRMILESTONESRPT AS CUSTOM

	cProcessName = 'HRMILESTONESRPT'

	lTestMode = .T.  && .F. FOR NORMAL OPERATION, .T. TO SUPPRESS ALL BUT DEVELOPER EMAILS AND CHANGE INPUT TABLE, ETC.

	dToday = DATE()
	*dToday = {^2013-07-05}

	lAutoYield = .T.

	* processing properties
	nMonthSpan = 3
	nMilestoneYear = 10

	cStartTime = TTOC(DATETIME())

	* connection properties
	nSQLHandle = 0

	* wait window properties
	nWaitWindowTimeout = 2
	lWaitWindowIsOn = .T.

	* logfile properties
	lLoggingIsOn = .T.
	cLogFile = 'F:\UTIL\HR\MILESTONES\LOGFILES\MILESTONES_Report_log.txt'

	* INTERNAL email properties
	lSendInternalEmailIsOn = .T.
	cFrom = 'Mark.Bennett@Tollgroup.com'
	*cSendTo = 'marie.freiberger@tollgroup.com, Lauren.Klaver@Tollgroup.com'
	cSendTo = 'mbennett@fmiint.com'
	cCC = 'mbennett@fmiint.com'
	cSubject = 'HR MILESTONES RPT for ' + TRANSFORM(DATE())
	cAttach = ''
	cBodyText = ''

	cCOMPUTERNAME = ''
	cUSERNAME = ''


	FUNCTION INIT
		IF NOT DODEFAULT()
			RETURN .F.
		ENDIF
		WITH THIS
			*SET RESOURCE OFF
			CLOSE DATA
			SET DECIMALS TO 4
			SET CENTURY ON
			SET DATE AMERICAN
			SET HOURS TO 24
			SET ANSI ON
			SET TALK OFF
			SET DELETED ON
			SET CONSOLE OFF
			SET EXCLUSIVE OFF
			SET SAFETY OFF
			SET EXACT OFF
			SET STATUS BAR ON
			SET SYSMENU OFF
			SET ENGINEBEHAVIOR 70
			_VFP.AUTOYIELD = .lAutoYield
			.cCOMPUTERNAME = GETENV("COMPUTERNAME")
			.cUSERNAME = GETENV("USERNAME")
			IF .lTestMode THEN
				.cLogFile = 'F:\UTIL\HR\MILESTONES\LOGFILES\MILESTONES_Report_log_TESTMODE.txt'
				.cSendTo = 'mbennett@fmiint.com'
			ENDIF
			.lLoggingIsOn = .lLoggingIsOn AND NOT EMPTY(.cLogFile)
			IF .lLoggingIsOn THEN
				SET ALTERNATE TO (.cLogFile) ADDITIVE
				SET ALTERNATE ON
			ENDIF
			SET PROCEDURE TO VALIDATIONS ADDITIVE
		ENDWITH
	ENDFUNC


	FUNCTION DESTROY
		WITH THIS
			IF .lLoggingIsOn  THEN
				SET ALTERNATE OFF
				SET ALTERNATE TO
			ENDIF
		ENDWITH
		DODEFAULT()
	ENDFUNC


	FUNCTION MAIN
		WITH THIS
			LOCAL lnNumberOfErrors, lcSQL, lcSQL2, loError, ldToday
			LOCAL lcFiletoSaveAs, lcSpreadsheetTemplate
			LOCAL ldFromDate, ldToDate, lcADPFromDate, lcADPToDate
			LOCAL lcDateSuffix, lcComment
			LOCAL i, oExcel, oWorkbook, oWorksheet, lnStartRow, lcStartRow, lnRow, lcRow
			LOCAL lcFirstName, lcNewFirstName, lcLastName, lnSpacePos, lcMI, lnMaxAmount

			TRY
				lnNumberOfErrors = 0

				.TrackProgress("HR MILESTONES RPT process started....", LOGIT+SENDIT+NOWAITIT)
				.TrackProgress('COMPUTERNAME = ' + .cCOMPUTERNAME, LOGIT+SENDIT)
				.TrackProgress('USERNAME = ' + .cUSERNAME, LOGIT+SENDIT)
				.TrackProgress('PROJECT = HRMILESTONESRPT', LOGIT+SENDIT)
				IF .lTestMode THEN
					.TrackProgress('                        TEST MODE                        ', LOGIT+SENDIT)
				ENDIF

				* we want to calc a 3 month date range beginning at the 1st of next month

				* determine prior Saturday
				ldFromDate = .dToday - DAY(.dToday) + 1  && 1st of current month
				ldToDate = GOMONTH(ldFromDate,.nMonthSpan) - 1

				* now move each date back the # of years in the desired milestone
				ldFromDate = GOMONTH(ldFromDate,-12 * .nMilestoneYear)
				ldToDate = GOMONTH(ldToDate,-12 * .nMilestoneYear)
				
				lcComment = TRANSFORM(.nMilestoneYear) + ' year anniversary!'


				*!*	******************************************
				*!*	** activate to force specific date range
				*!*	ldFromDate = {^2012-07-01}
				*!*	ldToDate = {^2012-07-13}
				*!*	******************************************

				lcDateSuffix = DTOC(ldFromDate) + " - " + DTOC(ldToDate)
				.cSubject = 'HR MILESTONES RPT for ' + lcDateSuffix

				lcADPFromDate = "DATE'" + TRANSFORM(YEAR(ldFromDate)) + "-" + PADL(MONTH(ldFromDate),2,'0') + "-" + PADL(DAY(ldFromDate),2,'0') + "'"
				lcADPToDate = "DATE'" + TRANSFORM(YEAR(ldToDate)) + "-" + PADL(MONTH(ldToDate),2,'0') + "-" + PADL(DAY(ldToDate),2,'0') + "'"

				lcSpreadsheetTemplate = 'F:\UTIL\HR\MILESTONES\TEMPLATES\MILESTONESTEMPLATE.XLS'
				lcFiletoSaveAs = 'F:\UTIL\HR\MILESTONES\REPORTS\MILESTONES ' + STRTRAN(lcDateSuffix,"/","") + '.XLS'
				.TrackProgress('Creating spreadsheet: ' + lcFiletoSaveAs, LOGIT+SENDIT+NOWAITIT)


				* now connect to ADP...

				.nSQLHandle = SQLCONNECT("PCPAYWIN","MBENNETT","VP737A")

				IF .nSQLHandle > 0 THEN


					SET TEXTMERGE ON
					TEXT TO	lcSQL NOSHOW
SELECT
NAME,
HIREDATE
FROM REPORTS.V_EMPLOYEE
WHERE STATUS <> 'T'
AND HIREDATE >= <<lcADPFromDate>>
AND HIREDATE <= <<lcADPToDate>>
					ENDTEXT
					SET TEXTMERGE OFF

					IF .lTestMode THEN
						.TrackProgress('lcSQL = ' + lcSQL, LOGIT+SENDIT)
					ENDIF

					IF USED('CURMILESTONESPRE') THEN
						USE IN CURMILESTONESPRE
					ENDIF
					IF USED('CURMILESTONES') THEN
						USE IN CURMILESTONES
					ENDIF

					IF .ExecSQL(lcSQL, 'CURMILESTONESPRE', RETURN_DATA_MANDATORY) THEN

						IF USED('CURMILESTONESPRE') AND NOT EOF('CURMILESTONESPRE') THEN


							SELECT NAME, TTOD(HIREDATE) AS HIREDATE, lcComment as COMMENT ;
								FROM CURMILESTONESPRE ;
								INTO CURSOR CURMILESTONES ;
								ORDER BY HIREDATE ;
								READWRITE

							* delete output file if it already exists...
							IF FILE(lcFiletoSaveAs) THEN
								DELETE FILE (lcFiletoSaveAs)
							ENDIF

							SELECT CURMILESTONES
							COPY TO (lcFiletoSaveAs) XL5



							*!*							SUM EECONTRIB TO lnTotalMILESTONES

							*!*							oExcel = CREATEOBJECT("excel.application")
							*!*							oExcel.displayalerts = .F.
							*!*							oExcel.VISIBLE = .F.
							*!*							oWorkbook = oExcel.workbooks.OPEN(lcSpreadsheetTemplate)
							*!*							oWorkbook.SAVEAS(lcFiletoSaveAs)
							*!*							oWorksheet = oWorkbook.Worksheets[1]

							*!*							oWorksheet.RANGE("B2").VALUE = ldToDate
							*!*							oWorksheet.RANGE("D2").VALUE = lnTotalMILESTONES

							*!*							lnRow = 3

							*!*							SELECT CURMILESTONES
							*!*							SCAN
							*!*								lnRow = lnRow + 1
							*!*								lcRow = ALLTRIM(STR(lnRow))
							*!*								oWorksheet.RANGE("A"+lcRow).VALUE = CURMILESTONES.CONTROL
							*!*								oWorksheet.RANGE("B"+lcRow).VALUE = CURMILESTONES.SUFFIX
							*!*								oWorksheet.RANGE("C"+lcRow).VALUE = CURMILESTONES.ACCOUNT
							*!*								oWorksheet.RANGE("D"+lcRow).VALUE = CURMILESTONES.SSN
							*!*								oWorksheet.RANGE("E"+lcRow).VALUE = CURMILESTONES.LASTNAME
							*!*								oWorksheet.RANGE("F"+lcRow).VALUE = CURMILESTONES.FIRSTNAME
							*!*								oWorksheet.RANGE("G"+lcRow).VALUE = CURMILESTONES.MI
							*!*								oWorksheet.RANGE("H"+lcRow).VALUE = CURMILESTONES.STREET1
							*!*								oWorksheet.RANGE("I"+lcRow).VALUE = CURMILESTONES.STREET2
							*!*								oWorksheet.RANGE("J"+lcRow).VALUE = CURMILESTONES.CITY
							*!*								oWorksheet.RANGE("K"+lcRow).VALUE = CURMILESTONES.STATE
							*!*								oWorksheet.RANGE("L"+lcRow).VALUE = CURMILESTONES.ZIPCODE
							*!*								oWorksheet.RANGE("M"+lcRow).VALUE = CURMILESTONES.ZIPEXT
							*!*								oWorksheet.RANGE("N"+lcRow).VALUE = CURMILESTONES.GENDER
							*!*								oWorksheet.RANGE("O"+lcRow).VALUE = TTOD(CURMILESTONES.BIRTHDATE)
							*!*								oWorksheet.RANGE("P"+lcRow).VALUE = TTOD(CURMILESTONES.OPDATE)
							*!*								oWorksheet.RANGE("Q"+lcRow).VALUE = CURMILESTONES.STATUS
							*!*								oWorksheet.RANGE("R"+lcRow).VALUE = TTOD(CURMILESTONES.STATDATE)
							*!*								oWorksheet.RANGE("S"+lcRow).VALUE = CURMILESTONES.MAXAMOUNT
							*!*								oWorksheet.RANGE("T"+lcRow).VALUE = TTOD(CURMILESTONES.MAXDATE)
							*!*								oWorksheet.RANGE("U"+lcRow).VALUE = CURMILESTONES.PRODCODE
							*!*								oWorksheet.RANGE("V"+lcRow).VALUE = CURMILESTONES.ERCONTRIB
							*!*								oWorksheet.RANGE("W"+lcRow).VALUE = CURMILESTONES.EECONTRIB
							*!*								oWorksheet.RANGE("AD"+lcRow).VALUE = IIF(EMPTY(CURMILESTONES.HEALTHCODE)," ","POS")
							*!*								oWorksheet.RANGE("AG"+lcRow).VALUE = IIF(EMPTY(CURMILESTONES.VISIONCODE)," ","VISION")
							*!*							ENDSCAN

							*!*							IF TYPE('oWorkbook') = "O" THEN
							*!*								oWorkbook.SAVE()
							*!*								oWorkbook = NULL
							*!*							ENDIF
							*!*							IF TYPE('oExcel') = "O" THEN
							*!*								oExcel.QUIT()
							*!*								oExcel = NULL
							*!*							ENDIF


							IF FILE(lcFiletoSaveAs) THEN
								.cAttach = lcFiletoSaveAs
								.cBodyText = 'See attached list of employees with ' + transform(.nMilestoneYear) + ' year milestones in the next ' + TRANSFORM(.nMonthSpan) + ' MONTHS.' + CRLF + CRLF + '<<REPORT LOG FOLLOWS>>' + CRLF + .cBodyText
							ELSE
								.TrackProgress("ERROR attaching " + lcFiletoSaveAs, LOGIT+SENDIT+NOWAITIT)
							ENDIF

						ELSE
							.TrackProgress('No milestones were found for the date range!', LOGIT+SENDIT)
						ENDIF  &&  USED('CURMILESTONESPRE') AND NOT EOF('CURMILESTONESPRE')


						=SQLDISCONNECT(.nSQLHandle)

					ENDIF  &&  .ExecSQL(lcSQL, 'SQLCURSOR1', RETURN_DATA_MANDATORY)
				ELSE
					* connection error
					.TrackProgress('Unable to connect to ADP Payroll System.', LOGIT+SENDIT)
				ENDIF   &&  .nSQLHandle > 0

				.TrackProgress("HR MILESTONES RPT process ended normally.", LOGIT+SENDIT+NOWAITIT)


			CATCH TO loError

				.TrackProgress('There was an error.',LOGIT+SENDIT)
				.TrackProgress('The Error # is: ' + TRANSFORM(loError.ERRORNO), LOGIT+SENDIT)
				.TrackProgress('The Error Message is: ' + TRANSFORM(loError.MESSAGE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred in Program: ' + TRANSFORM(loError.PROCEDURE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred at line #: ' + TRANSFORM(loError.LINENO), LOGIT+SENDIT)
				lnNumberOfErrors = lnNumberOfErrors + 1

*!*					IF TYPE('oWorkbook') = "O" AND NOT ISNULL(oWorkbook) THEN
*!*						oWorkbook.SAVE()
*!*					ENDIF
*!*					IF TYPE('oExcel') = "O" AND NOT ISNULL(oExcel) THEN
*!*						oExcel.QUIT()
*!*					ENDIF

*!*					CLOSE DATABASES ALL

			ENDTRY

			WAIT CLEAR
			***************** INTERNAL email results ******************************

			.TrackProgress('About to send status email.',LOGIT)
			.TrackProgress('The logfile is: ' + .cLogFile, SENDIT)
			.TrackProgress("HR MILESTONES RPT process started: " + .cStartTime, LOGIT+SENDIT)
			.TrackProgress("HR MILESTONES RPT process finished: " + TTOC(DATETIME()), LOGIT+SENDIT)

			IF .lSendInternalEmailIsOn THEN
				DO FORM dartmail2 WITH .cSendTo,.cFrom,.cSubject,.cCC,.cAttach,.cBodyText,"A"
				.TrackProgress('Sent status email.',LOGIT)
			ELSE
				.TrackProgress('Did not send status email: THIS.lSendInternalEmailIsOn = FALSE.',LOGIT)
			ENDIF

			IF (lnNumberOfErrors = 0) THEN
				schedupdate()
			ENDIF

		ENDWITH
		RETURN
	ENDFUNC && main


	FUNCTION ExecSQL
		LPARAMETERS tcSQL, tcCursorName, tlNoDataReturnedIsOkay
		LOCAL llRetval, lnResult
		WITH THIS
			* close target cursor if it's open
			IF USED(tcCursorName)
				USE IN (tcCursorName)
			ENDIF
			WAIT WINDOW "Fetching remote data; please wait..." NOWAIT
			lnResult = SQLEXEC(.nSQLHandle, tcSQL, tcCursorName)
			llRetval = ( lnResult > 0 )
			IF llRetval THEN
				* see if any data came back
				IF NOT tlNoDataReturnedIsOkay THEN
					IF (NOT USED(tcCursorName)) OR EOF(tcCursorName) THEN
						llRetval = .F.
						.TrackProgress('No data was returned by this query: ' +CRLF + tcSQL, LOGIT+SENDIT)
						.cSendTo = 'Mark Bennett <mbennett@fmiint.com>'
					ENDIF
				ENDIF
			ELSE
				.TrackProgress("Result Code [" + TRANSFORM(lnResult) + "] returned by this query: " + tcSQL, LOGIT+SENDIT)
				.cSendTo = 'Mark Bennett <mbennett@fmiint.com>'
			ENDIF
			WAIT CLEAR
			RETURN llRetval
		ENDWITH
	ENDFUNC


	PROCEDURE TrackProgress
		* do any combination of Wait Window, writing to logfile, and adding to body of email,
		* based on nFlags parameter.
		LPARAMETERS tcExpression, tnFlags
		WITH THIS
			IF BITAND(tnFlags,LOGIT) = LOGIT THEN
				IF .lLoggingIsOn THEN
					?
					? .cProcessName + "  " + TTOC(DATETIME()) + ": " + tcExpression
				ENDIF
			ENDIF
			IF .lWaitWindowIsOn THEN
				IF BITAND(tnFlags,NOWAITIT) = NOWAITIT THEN
					WAIT WINDOW tcExpression NOWAIT
				ENDIF
				IF BITAND(tnFlags,WAITIT) = WAITIT THEN
					WAIT WINDOW tcExpression TIMEOUT .nWaitWindowTimeout
				ENDIF
			ENDIF
			IF BITAND(tnFlags,SENDIT) = SENDIT THEN
				IF .lSendInternalEmailIsOn THEN
					.cBodyText = .cBodyText + tcExpression + CRLF + CRLF
				ENDIF
			ENDIF
		ENDWITH
	ENDPROC  &&  TrackProgress


	FUNCTION GetSQLStartDateTimeString
		* returns a timestamp string for use in SQL queries
		LPARAMETERS tdDate
		LOCAL lcSetDate, lcSQLDateTimeString
		lcSetDate = SET('DATE')
		SET DATE YMD
		lcSQLDateTimeString   = "{ts '" + TRANSFORM(YEAR(tdDate)) + "-" + PADL(MONTH(tdDate),2,"0") + "-" + PADL(DAY(tdDate),2,"0") + " 00:00:00.0'}"
		SET DATE &lcSetDate.
		RETURN lcSQLDateTimeString
	ENDFUNC


	FUNCTION GetSQLEndDateTimeString
		* returns a timestamp string for use in SQL queries
		LPARAMETERS tdDate
		LOCAL lcSetDate, lcSQLDateTimeString
		lcSetDate = SET('DATE')
		SET DATE YMD
		lcSQLDateTimeString = "{ts '" + TRANSFORM(YEAR(tdDate)) + "-" + PADL(MONTH(tdDate),2,"0") + "-" + PADL(DAY(tdDate),2,"0") + " 23:59:59.9'}"
		SET DATE &lcSetDate.
		RETURN lcSQLDateTimeString
	ENDFUNC


ENDDEFINE
