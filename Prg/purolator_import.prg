*Set Step On
Set Date YMD

Public nAcctNum,cOffice,lLoadSQL,lTesting,lTestImport,cOffice,gMasterOffice,cMod,gldor,cUseFolder

nAcctNum = 6674
cOffice = "N"
gMasterOffice = "N"
cMod="I"
lLoadSQL= .T.
lTestingt = .F.
lTestImport =.F.
gldor=.T.


cUseFolder ="f:\ftpusers\purolator\in\"

lcArchivepath= "f:\ftpusers\steelseries\purolator\in\archive\"
lcPath= "f:\ftpusers\steelseries\purolator\in\"

*Use f:\wh\purolator_shipments In 0
*Use f:\wh\purolator_errors In 0

lnNum = Adir(tarray,"f:\ftpusers\steelseries\purolator\in\*.xml")

*Messagebox(Str(lnnum))

If lnNum = 0
  Wait Window At 10,10 "      No Purolator files to import     " Timeout 3
  NormalExit = .T.
  Throw
Endif

lnShipments = 0
lnErrors = 0

For thisfile = 1  To lnNum
  Xfile = lcPath+tarray[thisfile,1] && +"."
  cfilename = Alltrim(Lower(tarray[thisfile,1]))
  nFileSize = Val(Transform(tarray(thisfile,2)))
  ArchiveFile = (lcArchivepath+cfilename)

  ictr = 1
  lcStr = Filetostr(Xfile)

  If "Severity"$lcStr 
    Wait Window At 10,10 "Importing error file: "+xfile nowait
    lnErrors = lnErrors +1
    parse_error_data()
  Else
    lnShipments = lnShipments +1
    Wait Window At 10,10 "Importing shipment file: "+xfile nowait
    parse_label_data()
  Endif

  Copy File &xfile To &ArchiveFile
  Delete File &xfile 

Next
  Wait clear
  =Messagebox("Import Complete"+Chr(13)+" Shipments: "+Alltrim(Transform(lnShipments))+" imported"+Chr(13)+" Errors: "+Alltrim(Transform(lnErrors)),0,"Purolattor Shipping")

Return


*  "<?xml version="1.0" encoding="utf-8"?>"

* <?xml version="1.0" encoding="utf-16"?>

**************************************************************************
Procedure parse_label_data

Select purolator_shipments
Scatter Memvar memo blank

Tag1 = "<ZPLCode>"
m.zplcode = GetData(lcStr,Tag1)
m.xmldata = lcStr

Tag1 = "<PackageID>"
m.packageid = GetData(lcStr,Tag1)

Tag1 = "<TrackingNumber>"
m.trknum = GetData(lcStr,Tag1)

Tag1 = "<ShipmentID>"
m.shipid = GetData(lcStr,Tag1)

Tag1 = "<AccountID>"
m.accountnum = GetData(lcStr,Tag1)

Tag1 = "<ShipperAccountID>"
m.shipperacct = GetData(lcStr,Tag1)

Tag1 = "<Name>"
m.name = GetData(lcStr,Tag1)

Tag1 = "<Add1>"
m.add1 = GetData(lcStr,Tag1)

Tag1 = "<Add2>"
m.add2 = GetData(lcStr,Tag1)

Tag1 = "<City>"
m.city = GetData(lcStr,Tag1)

Tag1 = "<StateProv>"
m.stateprov = GetData(lcStr,Tag1)

Tag1 = "<PostalCode>"
m.zipcode = GetData(lcStr,Tag1)

Tag1 = "<ShippingCharge>"
m.charge = GetData(lcStr,Tag1)

Tag1 = "<OrderNumber>"
m.ship_ref = GetData(lcStr,Tag1)
m.datercvd = Datetime()


Append blank
Gather Memvar Memo

*!*  !net Use lpt1 /Delete
*!*  !net Use lpt1 "\\dc3\pgzebra"

*!*  Strtofile(m.zplcode,"h:\fox\temp.txt")
*!*  lcfile = "h:\fox\temp.txt"
*!*  !Type &lcfile > lpt1


Endproc

**************************************************************************
Procedure parse_error_data

msg_found = .T.
msg_num = 1
msg_start= At("<Message>",lcStr,msg_num)

Do While msg_found

   msg_start= At("<Message>",lcStr,msg_num)
   
  If msg_start > 0

    Select purolator_errors
    Append blank
    Scatter Memvar memo

    Tag1 = "<Message>"
    m.message = getdata_whichone(lcStr,Tag1,msg_num)
    m.xmldata = lcStr

    Tag1 = "<Number>"
    m.errornum = getdata_whichone(lcStr,Tag1,msg_num)

    Tag1 = "<Severity>"
    m.severity = getdata_whichone(lcStr,Tag1,msg_num)
    m.datercvd = Datetime()

    Gather Memvar Memo

  Else
    msg_found= .F.
  Endif
  msg_num = msg_num +1
Enddo



Endproc
**************************************************************************
Procedure getdata_whichone
Parameters sdata,starttag,whichmessage

endtag = Strtran(starttag,"<","</")
lnDataStart = At(starttag,lcStr,whichmessage)
lnDataEnd = At(endtag,lcStr,whichmessage)

lcXdata = Substr(lcStr,lnDataStart,lnDataEnd-lnDataStart)

lcXdata = Strtran(lcXdata,starttag,"")
lcXdata = Strtran(lcXdata,endtag,"")

Return lcXdata

Endproc
**************************************************************************
Procedure GetData
Parameters sdata,starttag

endtag = Strtran(starttag,"<","</")
lnDataStart = At(starttag,lcStr)
lnDataEnd = At(endtag,lcStr)

lcXdata = Substr(lcStr,lnDataStart,lnDataEnd-lnDataStart)

lcXdata = Strtran(lcXdata,starttag,"")
lcXdata = Strtran(lcXdata,endtag,"")

Return lcXdata

Endproc
**************************************************************************
Procedure getdatanotes
Parameters sdata,match
tdata =""
xxdata =""
lnstart = At(match,sdata)
tdata = sdata
tdata=Strtran(tdata,match)
lnend = At("</",tdata)

Do While lnend =0
  xxdata=xxdata+Chr(13)+Alltrim(tdata)
  Select temp1
  Skip 1
  tdata = temp1.f1
  lnend = At("</",tdata)
Enddo
xxdata = xxdata+Chr(13)+Substr(tdata,1,lnend)
Return xxdata

Endproc
***************************************************************************
Procedure getfield
Parameters sdata,match

lnstart = At(match,sdata)
If "="$match
  lnstart = lnstart-1
Endif
firstquote = lnstart+Len(match)+1
cc = Substr(sdata,firstquote,1)
lnsecond = firstquote+1
zzCtr = 0
Do While Substr(sdata,lnsecond,1)!=["]
  zzCtr = zzCtr+1
  If zzCtr > 200
    Return "ERROR"
  Endif
  lnsecond = lnsecond+1
Enddo

Return Substr(sdata,firstquote+1,lnsecond-(firstquote+1))

Endproc
**************************************************************************
Procedure GotoTag
Parameters tagname,stoptag,endofdocument
xtagfilter=Iif(Empty(stoptag),"","and F1!= stoptag")
Do While f1!= tagname &xtagfilter
  If Eof("temp1")
    If !endofdocument
      Wait Window At 10,10 " Cant find looking for TAG "+tagname
    Endif
    Exit
  Else
    Skip 1 In temp1
  Endif
Enddo
Endproc
**************************************************************************



