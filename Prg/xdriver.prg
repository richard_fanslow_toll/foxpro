create cursor xdriver (driver c(30))
index on driver tag driver

if goffice="N"
	xdriverfilter='division=3'
	xoodriverfilter='company="T"'
else
	xdriverfilter='inlist(division,5,50,53,54,55,58)'
	xoodriverfilter='company="W"'
endif

select employee
scan for active and inlist(classif,1,2) and &xdriverfilter
	m.driver=trim(proper(first))+" "+proper(last)
	insert into xdriver from memvar
endscan

xsqlexec("select * from odrivers where active=1 and !empty(ss_num) and "+xoodriverfilter,"odrivers",,"oo")

select odrivers
scan for active and !empty(ss_num) and &xoodriverfilter 
	m.driver=trim(proper(first))+" "+proper(last)
	insert into xdriver from memvar
endscan
