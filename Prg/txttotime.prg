Parameters txttime

  tday=Substr(txttime,5,2)
  tmon = Substr(txttime,7,3)
  timepart = Substr(txttime,10)

  Do case
    Case Upper(tmon) = "JAN"
      tmon="01"
    Case Upper(tmon) = "FEB"
      tmon="02"
    Case Upper(tmon) = "MAR"
      tmon="03"
    Case Upper(tmon) = "APR"
      tmon="04"
    Case Upper(tmon) = "MAY"
      tmon="05"
    Case Upper(tmon) = "JUN"
      tmon="06"
    Case Upper(tmon) = "JUL"
      tmon="07"
    Case Upper(tmon) = "AUG"
      tmon="08"
    Case Upper(tmon) = "SEP"
      tmon="09"
    Case Upper(tmon) = "OCT"
      tmon="10"
    Case Upper(tmon) = "NOV"
      tmon="11"
    Case Upper(tmon) = "DEC"
      tmon="12"
  EndCase
     
 ttime =tmon+"/"+tday+"/20"+timepart
 realtime = Ctot(ttime)
 return realtime
 
*{  /  /       :  :  }