coffice='C'
acctnum=6682

*Parameters coffice,acctnum
* call this like this: DO m:\dev\prg\2xu943.prg WITH "C", 6665

*runack("WMSINBOUND")
ltesting = .F.
Do m:\dev\prg\_setvars With ltesting
If Vartype(acctnum) = "C"
  acctnum = Int(Val(acctnum))
Endif

Public goffice,tsendto,tcc,lcaccountname,lcartonacct,ldogeneric,lcreateerr,pname,nacctnum,lholdfile,ldomix,nlinestart,cmod
Public m.style,m.color,m.id,m.size,m.pack,ctransfer,ltest,lallpicks,lcwhpath,lnormalexit,lcerrormessage,newacctnum
Public cacctname,lacctname,xacctnum,lprepack,nprepacks,npickpacks,tfrom,lprocerror,thisfile,lcpath,ldoslerror
Public cslerror,lpik,lpre,cmessage,cfilename,ltestmail,lmoderr,xfile,cmyfile,lxlsx,archivefile,ldeldbf,plqty1,plqty2,lcerrormessage
Public lcRefValue,cWhseMod,nacctnum,cmod,gmasteroffice

Store "" To m.style,m.color,m.id,m.size,m.pack,lcbrokerref,cmessage,cfilename,xfile


nacctnum = 6682

Close Databases All
Set Path To
Set Path To "M:\DEV\PRG\"
Do m:\dev\prg\_setvars With .T.

gsystemmodule = "wh"

Set Status Bar On
On Escape Cancel
Set tablevalidate To 0
Set Talk Off
Set enginebehavior 70
Set Escape On
Set Safety Off
Set Multilocks On
Set Deleted On
Set Exclusive Off

Do m:\dev\prg\lookups

lcerrormessage=""
ltest = .F.  && This directs output to F:\WHP\WHDATA test files (default = .f.)
ltestmail = .F. &&lTest && Causes mail to go to default test recipient(s)
lholdfile = .F. && Causes error CATCH message; on completion with no errors, changed to .t. (default = .f.)
lbrowse = lholdfile  && If this is set with lHoldFile, browsing will occur at various points (default = .f.)
loverridebusy = Iif(lholdfile Or ltest,.T.,.F.) && If this is set, CHKBUSY flag in FTPSETUP will be ignored (default = .f.)

loverridebusy = .T.

lallpicks = .F.  && If this is set, all inbounding will be done as units...should not be used (default = .f.)
ldoslerror = .F.  && This flag will be set if there is missing info in the Courtaulds inbound sheet
lxlsx = .F.
ldeldbf = .T.

If Type("acctnum") = "C"
  acctnum = Val(acctnum)
Endif

Store acctnum To nacctnum

tfrom = "TGF WMS Inbound EDI System Operations <inbound-ops@fmiint.com>"
ldogeneric = .F.
lcreateerr = .F.
lmainmail = .T.
tsendto = ""
tcc = ""
cslerror = ""
lcartonacct = .F.
Select 0
Use F:\edirouting\ftpsetup
*  ASSERT .f. MESSAGE "In FTPSETUP list"

m.office = "C"



************************added TM
If !ltesting
  lcpath = "F:\FTPUSERS\michaels\943IN\"
  lcarchivepath = 'F:\FTPUSERS\michaels\943IN\archive\'
  tsendto="PGAIDIS@FMIINT.COM"
  tcc="PGAIDIS@FMIINT.COM"
Else
  lcpath = "F:\FTPUSERS\michaels\943IN\"
  lcarchivepath = 'F:\FTPUSERS\michaels\943IN\archive\'
  tsendto="PGAIDIS@FMIINT.COM"
  tcc="PGAIDIS@FMIINT.COM"
Endif
************************************************************
Cd &lcpath
len1 = Adir(tarray,"*.*")
If len1 = 0
  Wait Window "No files found...exiting" Timeout 2
  Close Data All
  schedupdate()
  _Screen.Caption=gscreencaption
  On Error
  Return
Endif

*!*  Use &lcwhpath.inwolog In 0 Alias inwolog
*!*  Use &lcwhpath.pl In 0 Alias pl
*!*  Use &lcwhpath.whgenpk In 0 Alias whgenpk

For thisfile = 1 To len1
***  cfilename = alltrim(ary1[thisfile,1])
  cfilename = Alltrim(tarray[thisfile,1])
  xfile = lcpath+cfilename
  llcontinue = .T.
  Try
    xfile = lcpath+Alltrim(tarray[thisfile,1])
    If Inlist(Upper(Justext(xfile)),"TMP","DBF")
      Loop
    Endif

    attrfile = '"'+lcpath+Alltrim(tarray[thisfile,1])+'"'
    cfilename = Justfname(xfile)
    !Attrib -r &attrfile  && Removes read-only flag from file to allow deletion
    Wait Window "Filename = "+xfile Timeout 1
    Create Cursor tempmain (ref c(50), ;
    b c(30), ;
    c c(30), ;
    office c(50), ;
    style c(20), ;
    f c(20), ;
    dest c(50), ;
    dc c(50), ;
    i c(50), ;
    j c(50), ;
    k c(50), ;
    l c(50), ;
    m c(50), ;
    n c(50), ;
    vessel c(50), ;
    o c(50), ;
    container c(11), ;
    r c(50), ;
    s c(50), ;
    t c(50), ;
    u c(15), ;
    v c(50), ;
    w c(50),;
    x c(50),;
    y c(50),;
    z c(50),;
    aa c(10),;
    ab c(10),;
    ac c(50),;
    pack c(50),;
    ae c(10),;
    quantity c(50),;
    units c(50),;
    ah c(50),;
    ai c(50),;
    aj c(50),;
    ak c(50))


    xfilecsv = Juststem(Justfname(xfile))+".csv"
    Do m:\dev\prg\excel_to_csv With cfilename
    Select tempmain
    Append From [&xfilecsv] Type Csv
*BROW

    Select tempmain
    Set Step On
    Go Top
    Skip 1
    lcRefValue = Substr(tempmain.ref,12)
    Go Top
    Delete Next 4
    Delete For Empty(Container)
    Goto Top

    Select tempmain
*    Delete For !Inlist(Dest,"USCET","USEZA","USCSF","USNYC","USLAX","USRDO","CET","EZA","CSF","NYC","LAX","RDO")
*    Delete For !Inlist(Dest,"","00091","00098")
*    Delete For !Inlist(Dest,"","00094","0001","0004")
    Select tempmain
    Scan
*      Replace office With "C" For Inlist(Dest,"USLAX","USRDO","LAX","RDO")
      Replace office With "N" For Inlist(Dest,"00098","0008","8","98","00008","00002")
      Replace office With "C" For Inlist(Dest,"1","4","00094","94","0001","0004","00001","00004")
    Endscan

    Set Step On
*    Set Deleted OFF
    Select Container,office,Dest From tempmain Where office ="C" Group By Container,office Into Cursor xctrs_c Order By Container
    Select Container,office,Dest From tempmain Where office ="N" Group By Container,office Into Cursor xctrs_n Order By Container

*******************************************************************************************************8
    lDoSQL = .T.
    lDoInwo = .F.
    coffice ="N"
    goffice ="I"
    gmasteroffice="N"
    cWhseMod= "whi"
    cmod ="I"
    lcwhpath = "F:\whi\whdata\"
    Use &lcwhpath.inwolog In 0 Alias inwolog
    xsqlexec("select * from pl where .f.","xpl",,"wh")
    useca("pl","wh")
    Select xctrs_n
    Scan For office = "N"
      Select tempmain
      Set Filter To tempmain.Container = xctrs_n.Container
      Do DoImport
    Endscan
*******************************************************************************************************8

    Set Step On
    Use In inwolog

    coffice ="C"
    gmasteroffice="C"
    cWhseMod = "wh8"
    goffice ="8"
    cmod ="8"
    lDoSQL = .T.
    lDoInwo = .F.

    lcwhpath = "F:\wh8\whdata\"

    useca("inwolog","wh")

    xsqlexec("select * from pl where .f.","xpl",,"wh")
    useca("pl","wh")
    Select xctrs_c
    Scan For office = "C"
      Select tempmain
      Set Filter To tempmain.Container = xctrs_c.Container
      Do DoImport

    Endscan

    Use In inwolog
    Use In pl
*    Use In whgenpk
***************************************************************************************************
***************************************************************************************************

    archivefile=lcarchivepath+cfilename
    Copy File [&xfile] To [&archivefile]

    Set Step On

    Delete File  [&xfile]
    xfile = Strtran(xfile,"xls","csv")
    xfile = Strtran(xfile,"XLS","CSV")
    Delete File  [&xfile]


  Catch To oerr
    Set Step On
    Assert .F. Message "In internal Catch Section (Not main loop)...debug"
    If lnormalexit = .F.
      tmessage = "Account: "+Alltrim(Str(acctnum))+" (Internal loop)"
      tmessage = tmessage+Chr(13)+"TRY/CATCH Exeception Message:"+Chr(13)+;
      [  Error: ] + Str(oerr.ErrorNo) +Chr(13)+;
      [  LineNo: ] + Str(oerr.Lineno) +Chr(13)+;
      [  Message: ] + oerr.Message +Chr(13)+;
      [  Procedure: ] + oerr.Procedure +Chr(13)+;
      [  Details: ] + oerr.Details +Chr(13)+;
      [  StackLevel: ] + Str(oerr.StackLevel) +Chr(13)+;
      [  LineContents: ] + Left(oerr.LineContents,50)+Chr(13)+;
      [  UserValue: ] + oerr.UserValue+Chr(13)

      tsubject = "Error Loading Inbound File "+xfile
      tsendto = tsendtoerr
      Do Case
      Case Inlist(coffice,"C","Y") And Inlist(nacctnum,4610,4694,6665)  && Nanjing
*            CASE cOffice = "Y" AND INLIST(nAcctNum,4610,4694)  && Nanjing
        tcc = ""
      Case coffice = "C" And nacctnum = 1285  && AgeGroup
        tcc = "yvalencia@fmiint.com"
      Otherwise
        tcc = tccerr
      Endcase
      tattach = ""
      Do Form m:\dev\frm\dartmail2 With tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"

      Messagebox(tmessage,0,"EDI Error",5000)

      Wait Clear
      llcontinue = .F.
    Else
      Wait Window "Normal Exit..." Timeout 2
    Endif
    Set Status Bar On
  Endtry
Next
**************************************************************try
Procedure DoImport


Store "" To m.style,m.color,m.id,m.size,m.pack

Select tempmain
Go Top
*****************************************************************start loop for multiple files here
Select * From inwolog Where .F. Into Cursor xinwolog Readwrite
recctr = 0

recctr = recctr +1
Wait Window At 10,10 "Checking Record # "+Transform(recctr) Nowait

m.adddt=Datetime()
m.updatedt=Datetime()
m.addby='MICHAELS943'
m.updateby='PGMANUEL'
m.accountid=6665
m.addproc='MICHAELS943'
m.updproc='MICHAELS943'

Select tempmain
Scatter Memvar
m.wo_date = Date()
m.acct_ref= lcRefValue
m.quantity = Val(m.quantity)
m.comments = "VESSEL: "+m.vessel
m.reference = m.vessel
Insert Into xinwolog From Memvar

********************************

recctr = 0
poctr=0
plid=0
m.units = .F.

m.adddt=Datetime()
m.updatedt=Datetime()
m.addby='MICHAELS943'
m.updateby='2XU943'
m.accountid=6682
m.acctname='MICHAELS'
m.addproc='MICHAELS943'
m.updproc='MICHAELS943'

Zap In xpl

Select tempmain
lnPoCtr = 1
Scan
  Select tempmain
  Scatter Memvar
  poctr=poctr+1
  plid=plid+1
  recctr = recctr +1
  m.units = .F.
  m.toqty = Val(tempmain.quantity)
  m.po = Transform(lnPoCtr)
  m.date_rcvd = Date()
  Insert Into xpl From Memvar
  Select xpl
  Replace Pack With tempmain.Pack  In xpl
  Replace xpl.totqty With Val(tempmain.quantity) In xpl

  m.units = .T.
  Insert Into xpl From Memvar
  Select xpl
  Replace Pack With "1" In xpl
  Replace xpl.totqty With Val(tempmain.quantity) * Val(Alltrim(tempmain.Pack)) In xpl
  lnPoCtr = lnPoCtr +1

Endscan

Select xpl
Sum(totqty) To xunitsqty  For units
Sum(totqty) To xcartonsqty For !units

nuploadcount = 0


Select xinwolog
Locate
Scan
  Scatter Memvar Memo
  Wait Window "Processing Container/AcctRef "+Alltrim(m.container)+"/"+Alltrim(m.acct_ref) Nowait
*m.wo_num=genpk("wonum","wh",,,.T.,,.T.)
  m.office = coffice
  m.mod = coffice
  nWO_Num = dygenpk("wonum",cWhseMod)
  m.plunitsinqty=xunitsqty
  m.plinqty=xcartonsqty
  m.remainqty =xcartonsqty
  m.quantity=xcartonsqty
  m.wo_date = Date()
  m.wo_num = nWO_Num
  m.accountid = nacctnum

  insertinto("inwolog","wh",.T.)
  tu("inwolog")

  Select xpl

  Scan For xpl.inwologid = xinwolog.inwologid
    Scatter Fields Except inwologid Memvar Memo
    m.addproc='MICHAELS943'
    m.updproc='MICHAELS943'
    If lDoSQL
      m.accountid = nacctnum
      m.wo_num = nWO_Num
      m.office = coffice
      m.mod = cmod
      insertinto("pl","whi",.T.)
    Else
      m.inwologid = inwolog.inwologid
      m.plid = dygenpk("pl",cWhseMod)
      Insert Into pl From Memvar
    Endif
  Endscan
  If lDoSQL
    tu("pl")
  Endif
Endscan


Endproc
******************************
Procedure closedata
******************************

If Used('inwolog')
  Use In inwolog
Endif

If Used('pl')
  Use In pl
Endif

If Used('upcmast')
  Use In upcmast
Endif

If Used('upcmastsql')
  Use In upcmastsql
Endif

If Used('account')
  Use In account
Endif

*If Used('whgenpk')
*  Use In whgenpk
*Endif
Endproc

*****************************
Procedure slerrormail
******************************
If Used('mm')
  Use In mm
Endif
Select 0
Use F:\3pl\Data\mailmaster Alias mm
Locate For accountid = 4677 And taskname = "EXCELERROR"
lusealt = mm.use_alt
tsendto = Alltrim(Iif(lusealt,mm.sendtoalt,mm.sendto))
tcc = Alltrim(Iif(lusealt,mm.ccalt,mm.cc))
Use In mm
tsubject = "Missing information in Inbound Excel sheet"
tattach = ""
tmessage = "The following required FIELD(s) is/are missing data:"
tmessage = tmessage+Chr(13)+cslerror
Do Form m:\dev\frm\dartmail2 With tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
Endproc

******************************
Procedure smerrmail
******************************
&& Used for all GENERIC INBOUND accounts
If Used('account')
  Use In account
Endif
Select 0
xsqlexec("select * from account where inactive=0","account",,"qq")
Index On accountid Tag accountid
Set Order To
If Seek(nacctnum,'account','accountid')
  cacctname = Alltrim(account.acctname)
Endif

If Used('mm')
  Use In mm
Endif
Select 0
Use F:\3pl\Data\mailmaster Alias mm
If !ltest
  If nacctnum # 6137
    Locate For mm.accountid = nacctnum And mm.office = goffice And mm.edi_type = 'PL'
  Else
    Locate For mm.accountid = nacctnum And mm.taskname = 'SMMAIL'
  Endif
  lusealt = mm.use_alt
  tsendto = Alltrim(Iif(lusealt,mm.sendtoalt,mm.sendto))
  tcc = Alltrim(Iif(lusealt,mm.ccalt,mm.cc))
Else
  Locate For mm.edi_type = "MISC" And mm.taskname = "GENERAL"
  lusealt = mm.use_alt
  tsendto = Alltrim(Iif(lusealt,mm.sendtoalt,mm.sendto))
  tcc = Alltrim(Iif(lusealt,mm.ccalt,mm.cc))
Endif
Use In mm
tsubject = "Style Errors in Inbound Excel sheet: "+cacctname
tattach = ""
tmessage = "File Name: "+Iif(!Empty(cfilename),cfilename,Justfname(xfile))
tmessage = tmessage+Chr(13)+Chr(13)+"The following Style/Color/Size combination(s) is/are not in our Style Master."
tmessage = tmessage+Chr(13)+"Please send these Style Master Updates, then RE-TRANSMIT the PL file. Thanks."+Chr(13)
tmessage = tmessage+Chr(13)+Padr("STYLE",22)+Padr("COLOR",12)+"SIZE"
tmessage = tmessage+Chr(13)+Replicate("=",38)
Select sm_err
Scan
  cstyle = Padr(Alltrim(sm_err.Style),22)
  ccolor = Padr(Alltrim(sm_err.Color),12)
  csize = Alltrim(sm_err.Id)
  tmessage = tmessage+Chr(13)+cstyle+ccolor+csize
Endscan
Use In sm_err
Do Form m:\dev\frm\dartmail2 With tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
Endproc
