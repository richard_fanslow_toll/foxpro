*!* INTRADECO944_CREATE.PRG
*!* This program is triggered automatically via the
*!* EDI_TRIGGER table, populated via the OUTWO.SCX "Create Invoice" button

PARAMETERS nWO_Num,cOffice
PUBLIC lXfer944,NormalExit,tfrom,cWO_Num,lTestInput,nFileNum,tsendto,tcc,cIntUsage
PUBLIC lcOutPath,lcHoldPath,lcArchivePath,nAcctNum,lHoldIDFile,cContainer,cAcct_ref,cMod,cMailName
_SCREEN.CAPTION = "INTRADECO 944 PROCESS"
tfrom = "TGF EDI Processing Center <toll-edi-ops@tollgroup.com>"

DO m:\dev\prg\_setvars
ON ESCAPE CANCEL
ON ERROR DEBUG

TRY
	closefiles()
	lTesting = .f. && If TRUE, disables certain functions
	lTestInput = .F. && lTesting
	NormalExit = .F.
	lHoldIDFile = lTesting && If true, keeps output file in 944HOLD folder, otherwise into the OUT folder
	lDoMail = .T.  && If .t., sends mail

	IF TYPE("nWO_Num") = "L" AND lTesting
		nWO_Num = 7152331
		cOffice = "C"
		CLOSE DATABASES ALL
	ENDIF
	SET STEP ON
	cOffice = IIF(INLIST(cOffice,"2","5"),"C",cOffice)
	cOffice = IIF(INLIST(cOffice,"N","I"),"I",cOffice)
	nAcctNum = IIF(INLIST(cOffice,"I"),6237,5154)
	cMod = IIF(cOffice = "C","5",cOffice)
	gOffice = cMod
	gMasterOffice = IIF(cOffice = "I","N",cOffice)
	cWhseMod = LOWER("wh"+cMod)

	IF TYPE("nWO_Num") <> "N"  OR EMPTY(nWO_Num) && If WO is not numeric or equals Zero
		cErrMsg = "No WO# Provided"
		WAIT WINDOW cErrMsg+"...closing" TIMEOUT 3
		DO ediupdate WITH "NO WO#"
		closeout()
		THROW
	ENDIF
	cWO_Num = ALLTRIM(STR(nWO_Num))

	ASSERT .F. MESSAGE "At Use Folder"
	IF lTestInput
		cUseFolder = "F:\WHP\WHDATA\"
	ELSE
		xReturn = "XXX"
		DO m:\dev\prg\wf_alt WITH cOffice,nAcctNum
		cUseFolder = UPPER(xReturn)
	ENDIF

	cSuffix = ""
	DO CASE
		CASE INLIST(cOffice,"C","1","5")
			cSuffix = "-SP"
			cWhse = "SAN PEDRO"
		CASE cOffice = "X"
			cSuffix = "-CR"
			cWhse = "CARSON"
		CASE cOffice = "M"
			cSuffix = "-FL"
			cWhse = "MIAMI"
		OTHERWISE
			cSuffix = "-NJ"
			cWhse = "CARTERET"
	ENDCASE

	DIMENSION thisarray(1)
	cASN = ""
	nSTSets = 0
	cSpecChar = .F.
	XFERUPDATE = .T.
	nFileNum = 0
	lBackup = .F.
	lOverage = .F.
	lgoodmail = .T.
	lCopyCA = .T.
	cErrMsg = ""
	lIsError = .T.
	lXfer944 = .F.
	cTrk_WO = ""
	nCTNTotal = 0

	IF !lTesting
		IF !USED("edi_xfer")
			USE F:\edirouting\DATA\edi_xfer IN 0 ALIAS edi_xfer ORDER TAG inven_wo
			SELECT edi_xfer
			SET FILTER TO edi_type = "944" AND accountid = nAcctNum
			IF SEEK(nWO_Num)
				WAIT WINDOW "Work Order "+cWO_Num+" already created and logged in EDI_XFER" TIMEOUT 2
				IF MESSAGEBOX("944 for Work Order "+cWO_Num+" already created...Overwrite Date?",4+32+256,"WO# FOUND",8000) = 7 &&No
					USE IN edi_xfer
					DO ediupdate WITH "DUPLICATE WO#"
					closeout()
					RETURN
				ELSE
					XFERUPDATE = .F.
				ENDIF
			ENDIF
		ENDIF
	ENDIF

	SELECT 0
	USE F:\3pl\DATA\mailmaster ALIAS mm SHARED
	LOCATE FOR mm.edi_type = "944" AND mm.accountid = nAcctNum AND mm.office = cOffice
	IF FOUND()
		STORE TRIM(mm.basepath) TO lcOutPath
		STORE TRIM(mm.holdpath) TO lcHoldPath
		STORE TRIM(mm.archpath) TO lcArchivePath

		IF !lTesting
			STORE TRIM(IIF(mm.use_alt,mm.sendtoalt,mm.sendto)) TO tsendto
			STORE TRIM(IIF(mm.use_alt,mm.ccalt,mm.cc)) TO tcc
			LOCATE
		ELSE
			LOCATE FOR mm.edi_type = "MISC" AND mm.taskname = "GENERAL"
			STORE TRIM(IIF(mm.use_alt,mm.sendtoalt,mm.sendto)) TO tsendto
			STORE TRIM(IIF(mm.use_alt,mm.ccalt,mm.cc)) TO tcc
		ENDIF
		LOCATE FOR mm.edi_type = "MISC" AND mm.taskname = "GENERAL"
		STORE TRIM(IIF(mm.use_alt,mm.sendtoalt,mm.sendto)) TO tsendtoerr
		STORE TRIM(IIF(mm.use_alt,mm.ccalt,mm.cc)) TO tccerr
		STORE TRIM(IIF(mm.use_alt,mm.sendtoalt,mm.sendto)) TO tsendtotest
		STORE TRIM(IIF(mm.use_alt,mm.ccalt,mm.cc)) TO tcctest
		USE IN mm
	ELSE
		WAIT WINDOW AT 10,10  "No parameters set for this acct# "+ALLTRIM(STR(nAcctNum))+"  ---> Office "+cOffice TIMEOUT 2
		USE IN mm
		RELEASE ALL
		NormalExit = .F.
		THROW
	ENDIF
	tsendto = IIF(lTesting,tsendtotest,tsendto)
	tcc = IIF(lTesting,tcctest,tcc)

	dt1 = TTOC(DATETIME(),1)
	dt2 = DATETIME()
	dtmail = TTOC(DATETIME())

	cCustPrefix = IIF(cOffice = "I","IV","ID") && Customer Prefix (for output file names)
	cFilename = "944"+cCustPrefix+dt1+".edi"
	cOutFile = (lcOutPath+cFilename)
	cHoldFile = (lcHoldPath+cFilename)
	cArchiveFile = (lcArchivePath+cFilename)

	CREATE CURSOR temp1 (wo_num N(6), ;
		po_num c(5), ;
		STYLE c(20), ;
		expected i, ;
		actual i)

*!* SET CUSTOMER CONSTANTS
	cCustName = IIF(cOffice = "I","IVORY","INTRADECO")  && Customer Identifier (for folders, etc.)
	cMailName = PROPER(cCustName)
	cX12 = "004010"  && X12 Standards Set used
	crecid = "3052648888"  && Recipient EDI address

	STORE "" TO c_CntrlNum,c_GrpCntrlNum
	nFileNum = FCREATE(cHoldFile)
	ASSERT .F. MESSAGE "At file creation...debug"

*!* SET OTHER CONSTANTS
	nCtnNumber = 1
	cString = ""
	csendqual = "ZZ"
	csendid = "TOLLLOGISTICS"
	crecqual = "12"
	cfd = "*"  && Field/element delimiter
	csegd = "~" && Line/segment delimiter
	nSegCtr = 0
	cterminator = ">" && Used at end of ISA segment
	cdate = DTOS(DATE())
	cTime = TTOC(DATETIME(),1)
	ctruncdate = RIGHT(cdate,6)
	cTime = SUBSTR(cTime,9,4)
	cfiledate = cdate+cTime
	cOrig = "J"
	cQtyRec = "1"
	cCTypeUsed = ""
	nQtyExpected = 0
	nCTNShortage = 0
	nCTNExpected = 0  && From INWOLOG count
	nCTNActual = 0
	cMissingCtn = "Missing Cartons: "+CHR(13)
	cPONum = ""
	cStyle = ""
	nPOExpected = 0
	nPOActual = 0
	nCTNShortage = 0

** PAD ID Codes
	csendidlong = PADR(csendid,15," ")
	crecidlong = PADR(crecid,15," ")

*!* Serial number incrementing table
	IF !USED("serfile")
		USE ("F:\3pl\data\serial\intradeco944_serial") IN 0 ALIAS serfile
	ENDIF

*!* Trigger Data
	IF !USED('edi_trigger')
		cEDIFolder = "F:\3PL\DATA\"
		USE (cEDIFolder+"edi_trigger") IN 0 ALIAS edi_trigger ORDER TAG wo_num
	ENDIF

*!* Search table (parent)
	IF !USED('inwolog')
		csqi = [select * from inwolog where accountid = ]+TRANSFORM(nAcctNum)+[ and wo_num = ]+TRANSFORM(nWO_Num)
		xsqlexec(csqi,,,"wh")
		RELEASE csqi
	ENDIF

*!* Search table (xref for PO's)
	IF !USED('indet')
		csqi = [select * from indet where accountid = ]+TRANSFORM(nAcctNum)+[ and wo_num = ]+TRANSFORM(nWO_Num)
		xsqlexec(csqi,,,"wh")
		RELEASE csqi
	ENDIF

	IF !USED('pl')
		csqp = [select * from pl where accountid = ]+TRANSFORM(nAcctNum)+[ and wo_num = ]+TRANSFORM(nWO_Num)
		xsqlexec(csqp,,,"wh")
		RELEASE csqp
	ENDIF

	nISACount = 0
	nSTCount = 1
	nSECount = 1

	SELECT inwolog

	ninwologid = inwolog.inwologid

	cContainer = IIF(!EMPTY(inwolog.CONTAINER),TRIM(inwolog.CONTAINER),TRIM(inwolog.REFERENCE)) && Extracts Container ID from INWOLOG

	IF EMPTY(TRIM(cContainer))
		WAIT WINDOW "There is no container attached to "+cWO_Num TIMEOUT 3
		lCopyChris = .T.
		DO ediupdate WITH "NO CONTAINER"
		closeout()
		RETURN
	ENDIF

	msgstr = "Do you wish to check expected against scanned?"
*nCheckScan = MESSAGEBOX(msgstr,4+32,"Check Scanned?")
	nCheckScan = 7  && If 6, will check cartons against TLDATA/Barcodes table

	SELECT inwolog
	ALEN = ALINES(apt,inwolog.comments,.T.,CHR(13))
	cAcctname = TRIM(inwolog.acctname)
	cContainer = TRIM(inwolog.CONTAINER)
	cCtrType = IIF(SUBSTR(cContainer,4,1)="U","SQ","AW")
	cAcct_ref = TRIM(inwolog.acct_ref)
	nCTNExpected = inwolog.plinqty
	nCTNActual = inwolog.plinqty-inwolog.removeqty
	cIntUsage = IIF(lTesting,"T","P")  && ISA Flag for testing/production

	IF nISACount = 0
		DO num_incr
		STORE "ISA"+cfd+"00"+cfd+"          "+cfd+"00"+cfd+"          "+cfd+csendqual+cfd+csendidlong+cfd+;
			crecqual+cfd+crecidlong+cfd+ctruncdate+cfd+cTime+cfd+"U"+cfd+"00401"+cfd+;
			PADL(c_CntrlNum,9,"0")+cfd+"0"+cfd+cIntUsage+cfd+cterminator+csegd TO cString
		DO cstringbreak
		nSegCtr = nSegCtr + 1

		STORE "GS"+cfd+"RE"+cfd+csendid+cfd+crecid+cfd+cdate+cfd+cTime+cfd+c_CntrlNum+;
			cfd+"X"+cfd+cX12+csegd TO cString
		DO cstringbreak
		nSegCtr = nSegCtr + 1

		nSegCtr = 0
		nISACount = 1
	ENDIF

	IF nSECount = 1
		STORE "ST"+cfd+"944"+cfd+PADL(c_GrpCntrlNum,9,"0")+csegd TO cString
		DO cstringbreak
		nSegCtr = nSegCtr + 1

		STORE "W17"+cfd+cOrig+cfd+cdate+cfd+cAcct_ref+csegd TO cString
		DO cstringbreak
		nSegCtr = nSegCtr + 1

		STORE "N1"+cfd+"ZL"+cfd+"INTRADECO APPAREL, INC."+csegd TO cString
		DO cstringbreak
		nSegCtr = nSegCtr + 1

		STORE "N3"+cfd+"9500 NW 108TH AVE."+csegd TO cString
		DO cstringbreak
		nSegCtr = nSegCtr + 1

		STORE "N4"+cfd+"MIAMI"+cfd+"FL"+cfd+"33178"+cfd+"USA"+csegd TO cString
		DO cstringbreak
		nSegCtr = nSegCtr + 1

*!*		STORE "PER"+cfd+"CN"+cfd+"Leonor Segovia"+cfd+"TE"+cfd+"888-555-1212"+;
*!*			cfd+"FX"+cfd+"888-555-1212"+cfd+"EM"+cfd+"Leonor_Segovia@Intradeco.com"+csegd TO cString
*!*		DO cstringbreak

		IF !EMPTY(cContainer)
			STORE "N9"+cfd+cCtrType+cfd+cContainer+csegd TO cString  && Used for either AWB or container type
			DO cstringbreak
			nSegCtr = nSegCtr + 1
		ENDIF

		STORE "N9"+cfd+"TG"+cfd+cAcct_ref+csegd TO cString
		DO cstringbreak
		nSegCtr = nSegCtr + 1

		cPO = ALLTRIM(segmentget(@apt,"PO",ALEN))
		IF !EMPTY(cPO)
			STORE "N9"+cfd+"PO"+cfd+cPO+csegd TO cString
			DO cstringbreak
			nSegCtr = nSegCtr + 1

			STORE "N9"+cfd+"TN"+cfd+cWO_Num+csegd TO cString
			DO cstringbreak
			nSegCtr = nSegCtr + 1
		ENDIF
	ENDIF

*SET STEP ON
*!*		IF lDoSQL
		csqi = [select * FROM pl where inwologid = ]+TRANS(ninwologid)
		xsqlexec(csqi,"temppl",,"wh")
		RELEASE csqi
*!*		ELSE
*!*			SELECT * FROM pl WHERE pl.inwologid = inwolog.inwologid INTO CURSOR temppl READWRITE
*!*		ENDIF

	SELECT temppl
	INDEX ON STYLE TAG STYLE
	LOCATE
	SCAN
		SELECT indet
		LOCATE FOR indet.inwologid = temppl.inwologid AND indet.STYLE = temppl.STYLE
		IF FOUND()
			DELETE NEXT 1 IN temppl
		ENDIF
	ENDSCAN
	SELECT temppl
	IF lTesting
		BROWSE
	ENDIF

	SELECT indet
	SCAN FOR indet.inwologid = inwolog.inwologid AND !units
		lDonDetTotqty = .T.
		IF !"MULTILINES"$indet.ECHO  && Standard detail line
			alendet = ALINES(aptdet,indet.ECHO,.T.,CHR(13))
			cUPC = ALLTRIM(segmentget(@aptdet,"UPC",alendet))
			cLN = ALLTRIM(segmentget(@aptdet,"LINENUM",alendet))
			IF EMPTY(cLN)
				SET STEP ON
				cErrMsg = "No LineNum @ IDID "+TRANSFORM(indet.indetid)
				WAIT WINDOW cErrMsg+"...closing" TIMEOUT 3
				DO ediupdate WITH cErrMsg
				closeout()
				THROW
			ENDIF
			STORE "W07"+cfd+TRANSFORM(indet.totqty)+cfd+"CT"+cfd+cUPC+cfd+"ST"+cfd+TRIM(indet.STYLE)+cfd+"LN"+cfd+cLN+csegd TO cString && Changed from totqty per Myrnaly, 04.13.2014
			DO cstringbreak
			nSegCtr = nSegCtr + 1
		ELSE
			CLEAR
			cDetStyle = ALLT(indet.STYLE)
			nQtyLines = OCCURS('QTY',indet.ECHO)
			nQtyLines2 = 0
			IF lDonDetTotqty
				lDonDetTotqty = .F.
				nDetTotqty = indet.totqty
			ENDIF

			lenary = ALINES(arydet,indet.ECHO,.T.,CHR(13))
			IF lTesting
				DISPLAY arydet
			ENDIF
			FOR icz = 1 TO lenary
				DO CASE
					CASE arydet[icz] = "MULTILINES"
						LOOP
					CASE arydet[icz] = "UPC"
						cUPC = ALLT(SUBSTR(arydet[icz],AT("*",arydet[icz])+1))
					CASE arydet[icz] = "LINENUM"
						cLN = ALLT(SUBSTR(arydet[icz],AT("*",arydet[icz])+1))
					CASE arydet[icz] = "QTY"
						nQtyLines2 = nQtyLines2+1
						cTQ = ALLT(SUBSTR(arydet[icz],AT("*",arydet[icz])+1))
						nTQ = INT(VAL(cTQ))
						IF nQtyLines2 < nQtyLines
							lNegVal = .F.
							IF nTQ >= nDetTotqty
								lNegVal = .T.
								cTQ = ALLT(STR(nDetTotqty))
							ELSE
								nDetTotqty = nDetTotqty-nTQ
							ENDIF
						ELSE
							IF nDetTotqty # nTQ
								IF lNegVal
									cTQ = '0'
								ELSE
									cTQ = ALLT(STR(nDetTotqty))
								ENDIF
							ENDIF
						ENDIF
						STORE "W07"+cfd+cTQ+cfd+"CT"+cfd+cUPC+cfd+"ST"+cfd+cDetStyle+cfd+"LN"+cfd+cLN+csegd TO cString
						DO cstringbreak
						nSegCtr = nSegCtr + 1
				ENDCASE
			ENDFOR
		ENDIF
	ENDSCAN
	SELECT temppl
	COUNT TO pl1cnt FOR !DELETED()
	IF lTesting
		SET STEP ON
	ENDIF
	IF pl1cnt>0
		domissingstyle()
	ENDIF

	WAIT WINDOW "END OF SCAN/CHECK PHASE ROUND..." TIMEOUT 2

	nSTCount = 0
	DO close944
	lXfer944 = .T.
	=FCLOSE(nFileNum)

	IF USED('serfile')
		IF lTesting
			SELECT serfile
			REPLACE seqnum WITH 1,grpseqnum WITH 101
		ENDIF
		USE IN serfile
	ENDIF

	WAIT "944 Creation process complete:"+CHR(13)+cFilename WINDOW AT 45,60 TIMEOUT 3
	cFin = "944 FILE "+cFilename+" CREATED WITHOUT ERRORS"
	ASSERT .F. MESSAGE "At file folder transfer...debug"
	SET STEP ON

	COPY FILE [&cHoldFile] TO [&cArchiveFile]
	IF !lHoldIDFile
		COPY FILE [&cHoldFile] TO [&cOutFile]
		DELETE FILE [&cHoldFile]
	ENDIF

	IF !lTesting
		IF !USED("ftpedilog")
			SELECT 0
			USE F:\edirouting\ftpedilog ALIAS ftpedilog
			INSERT INTO ftpedilog (ftpdate,filename,acct_name,TYPE) VALUES (dt2,cFilename,cCustName+cSuffix,"944")
			USE IN ftpedilog
		ENDIF
		SELECT edi_trigger
	ENDIF

	NormalExit = .T.
	WAIT WINDOW "Intradeco 944 process complete" TIMEOUT 1

CATCH TO oErr
	IF NormalExit = .F.
		closeout()
		ASSERT .F. MESSAGE "In Catch section..."
		tsubject = cMailName+" Inbound Error ("+TRANSFORM(oErr.ERRORNO)+") at "+TTOC(DATETIME())
		lcSourceMachine = SYS(0)
		lcSourceProgram = "intradeco944_create.prg"
		tcc = ""
		tattach = ""

		tmessage="Try/CATCH Exeception Message:"+CHR(13)+;
			[  Error: ] + STR(oErr.ERRORNO) +CHR(13)+;
			[  LineNo: ] + STR(oErr.LINENO) +CHR(13)+;
			[  Message: ] + oErr.MESSAGE +CHR(13)+;
			[  Procedure: ] + oErr.PROCEDURE +CHR(13)+;
			[  Details: ] + oErr.DETAILS +CHR(13)+;
			[  StackLevel: ] + STR(oErr.STACKLEVEL) +CHR(13)+;
			[  LineContents: ] + oErr.LINECONTENTS+CHR(13)+;
			[  UserValue: ] + oErr.USERVALUE+CHR(13)+;
			[  Computer:  ] +lcSourceMachine+CHR(13)+;
			[  Inbound file:  ] +cFilename+CHR(13)+;
			[  Program:   ] +lcSourceProgram
		tmessage =tmessage+CHR(13)+"PROGRAM: intradeco944_create.prg"
		tmessage =tmessage+CHR(13)+CHR(13)+"SPECIFIC ERROR: "+cErrMsg

		IF lDoMail
			DO FORM m:\dev\frm\dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
		ENDIF
	ELSE
		WAIT WINDOW AT 10,10 "Normal Exit " TIMEOUT 2
	ENDIF
FINALLY
	SET CENTURY ON
	ON ERROR
	IF lTestInput
		CLOSE DATABASES ALL
	ENDIF
ENDTRY

************************************************************************
************************************************************************
*!* END OF MAIN PROGRAM
************************************************************************
************************************************************************


****************************
PROCEDURE stclose
****************************
	nCTNShortage = 0
	nCTNTotal = nCTNTotal + nCTNExpected
	IF nCTNShortage < 0
		WAIT CLEAR
		lOverage = .T.
		tsubject = cMailName+" EDI ERROR: Overage, Inv. WO "+cWO_Num
		tattach = " "
		tmessage = "PO "+cPONumOld+" OVERAGE (More ctns than expected for a PO)! Check EDI File immediately!"
		tmessage = tmessage + "Expected: "+TRANSFORM(nCTNExpected)+", Actual: "+TRANSFORM(nCTNActual)
		DO FORM m:\dev\frm\dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
	ENDIF

*!* For 944 process only!

	IF nSECount = 0
		STORE "W14"+cfd+TRANS(nCTNExpected)+;
			cfd+TRANSFORM(nCTNExpected-nCTNShortage)+;
			cfd+TRANSFORM(nCTNShortage)+csegd TO cString
		DO cstringbreak
		nSegCtr = nSegCtr + 1
*!* End 944 addition

		STORE  "SE"+cfd+ALLTRIM(STR(nSegCtr+1))+cfd+PADL(c_GrpCntrlNum,9,"0")+csegd TO cString
		FPUTS(nFileNum,cString)
		nSTCount = 1
		nSECount = 1
	ENDIF
	nSegCtr = 0
	nSTSets = nSTSets + 1

	SELECT serfile
	REPLACE serfile.grpseqnum WITH serfile.grpseqnum + 1
	c_GrpCntrlNum = ALLTRIM(STR(serfile.grpseqnum))
*? c_GrpCntrlNum
	SELECT indet1
	LXCount = 1
	STORE 0 TO nCTNExpected,nCTNActual,nCTNShortage
ENDPROC

****************************
PROCEDURE close944
****************************
** Footer Creation

	IF nSTCount = 0
*!* For 944 process only!

		nCTNShortage = (nCTNExpected - nCTNActual)
		STORE "W14"+cfd+ALLTRIM(STR(nCTNExpected))+;
			cfd+ALLTRIM(STR(nCTNExpected-nCTNShortage))+;
			cfd+ALLTRIM(STR(nCTNShortage))+csegd TO cString
		DO cstringbreak
		nSegCtr = nSegCtr + 1
*!* End 944 addition

		STORE  "SE"+cfd+ALLTRIM(STR(nSegCtr+1))+cfd+PADL(c_GrpCntrlNum,9,"0")+csegd TO cString
		FPUTS(nFileNum,cString)
		nSegCtr = 0
		nSTSets = nSTSets + 1
	ENDIF

	STORE  "GE"+cfd+ALLTRIM(STR(nSTSets))+cfd+c_CntrlNum+csegd TO cString
	FPUTS(nFileNum,cString)

	STORE  "IEA"+cfd+"1"+cfd+PADL(c_CntrlNum,9,"0")+csegd TO cString
	FPUTS(nFileNum,cString)

	lIsError = .F.
	DO ediupdate WITH "944 CREATED"
*!*		SELECT 0
*!*		USE F:\edirouting\ftpjobs
*!*		INSERT INTO F:\edirouting\ftpjobs (jobname,USERID,JOBTIME) VALUES ("944-TO-INTRADECO","paulg",DATETIME())
*!*		USE IN ftpjobs

****************************
PROCEDURE cstringbreak
****************************
	FPUTS(nFileNum,cString)
ENDPROC

****************************
PROCEDURE num_incr
****************************
	SELECT serfile
	REPLACE serfile.seqnum WITH serfile.seqnum + 1 IN serfile
	REPLACE serfile.grpseqnum WITH serfile.grpseqnum + 1 IN serfile
	c_CntrlNum = ALLTRIM(STR(serfile.seqnum))
	c_GrpCntrlNum = ALLTRIM(STR(serfile.grpseqnum))
ENDPROC

****************************
PROCEDURE ediupdate
****************************
	PARAMETERS cFin
	IF !lTesting
		SELECT edi_trigger
		REPLACE processed WITH .T.
		REPLACE fin_status WITH cFin
		IF lIsError
			REPLACE errorflag WITH .T.
			REPLACE created WITH .F.
			BLANK FIELDS edi_trigger.file944crt NEXT 1
		ELSE
			REPLACE when_proc WITH DATETIME()
			REPLACE errorflag WITH .F.
			REPLACE created WITH .T.
			REPLACE file944crt WITH cArchiveFile
		ENDIF
	ENDIF
	IF lgoodmail
		goodmail()
	ENDIF
	closeout()
ENDPROC

****************************
PROCEDURE closeout
****************************
	IF !EMPTY(nFileNum)
		=FCLOSE(nFileNum)
*	ERASE &cFilename
	ENDIF
	closefiles()
ENDPROC

****************************
PROCEDURE closefiles
****************************
	IF USED('barcodes')
		SELECT BARCODES
		SET FILTER TO
		USE IN BARCODES
	ENDIF
	IF USED('edi_xfer')
		USE IN edi_xfer
	ENDIF
	IF USED('csrBCdata')
		USE IN csrBCdata
	ENDIF
	IF USED('detail')
		USE IN DETAIL
	ENDIF
	IF USED('indet')
		USE IN indet
	ENDIF
	IF USED('inwolog')
		USE IN inwolog
	ENDIF
	IF USED('pl')
		USE IN pl
	ENDIF
	WAIT CLEAR
	_SCREEN.CAPTION = "INBOUND POLLER - EDI"
ENDPROC

****************************
PROCEDURE goodmail
****************************
	IF lIsError
		tsubject = cMailName+" EDI ERROR, Inv. WO"+cWO_Num
	ELSE
		tsubject = cMailName+" EDI FILE (944) Created, Inv. WO"+cWO_Num
	ENDIF

	tattach = " "
	tmessage = "Inv. WO #: "+cWO_Num+CHR(13)
	tmessage = tmessage+"Container: "+cContainer+", Acct Ref: "+cAcct_ref
	IF EMPTY(cErrMsg)
		tmessage = tmessage + cFin
	ELSE
		tmessage = tmessage + cErrMsg
	ENDIF
	IF lDoMail
		DO FORM m:\dev\frm\dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
	ENDIF
ENDPROC

****************************
PROCEDURE segmentget
****************************
	PARAMETER thisarray,lcKey,nLength

	FOR i = 1 TO nLength
		IF i > nLength
			EXIT
		ENDIF
		lnEnd= AT("*",thisarray[i])
		IF lnEnd > 0
			lcThisKey =TRIM(SUBSTR(thisarray[i],1,lnEnd-1))
			IF OCCURS(lcKey,lcThisKey)>0
				RETURN SUBSTR(thisarray[i],lnEnd+1)
				i = 1
			ENDIF
		ENDIF
	ENDFOR

	RETURN ""

*********************************
PROCEDURE domissingstyle
*********************************
	IF lTesting
		SET STEP ON
	ENDIF
	SELECT temppl
	SCAN FOR !DELETED()
		lDonDetTotqty = .T.
		IF !"MULTILINES"$temppl.ECHO  && Standard detail line
			alendet2 = ALINES(aptdet2,temppl.ECHO,.T.,CHR(13))
			cUPC = ALLTRIM(segmentget(@aptdet2,"UPC",alendet))
			cLN = ALLTRIM(segmentget(@aptdet2,"LINENUM",alendet))
			IF EMPTY(cLN)
				SET STEP ON
				cErrMsg = "No LineNum @ IDID "+TRANSFORM(temppl.indetid)
				WAIT WINDOW cErrMsg+"...closing" TIMEOUT 3
				DO ediupdate WITH cErrMsg
				closeout()
				THROW
			ENDIF
			STORE "W07"+cfd+"0"+cfd+"CT"+cfd+cUPC+cfd+"ST"+cfd+TRIM(temppl.STYLE)+cfd+"LN"+cfd+cLN+csegd TO cString && Changed from totqty per Myrnaly, 04.13.2014
			DO cstringbreak
			nSegCtr = nSegCtr + 1
		ELSE
			CLEAR
			cDetStyle = ALLT(temppl.STYLE)
			nQtyLinest = OCCURS('QTY',temppl.ECHO)
			nQtyLines3 = 0
			IF lDonDetTotqty
				lDonDetTotqty = .F.
				nDetTotqty = 0
			ENDIF

			lenary2 = ALINES(arydet2,temppl.ECHO,.T.,CHR(13))
			IF lTesting
				DISPLAY arydet2
			ENDIF
			FOR icz = 1 TO lenary
				DO CASE
					CASE arydet2[icz] = "MULTILINES"
						LOOP
					CASE arydet2[icz] = "UPC"
						cUPC = ALLT(SUBSTR(arydet[icz],AT("*",arydet[icz])+1))
					CASE arydet2[icz] = "LINENUM"
						cLN = ALLT(SUBSTR(arydet[icz],AT("*",arydet[icz])+1))
					CASE arydet2[icz] = "QTY"
						nQtyLines3 = nQtyLines3+1
						cTQ = ALLT(SUBSTR(arydet[icz],AT("*",arydet[icz])+1))
						nTQ = INT(VAL(cTQ))
						IF nQtyLines3 < nQtyLines
							lNegVal = .F.
							IF nTQ >= nDetTotqty
								lNegVal = .T.
								cTQ = ALLT(STR(nDetTotqty))
							ELSE
								nDetTotqty = nDetTotqty-nTQ
							ENDIF
						ELSE
							IF nDetTotqty # nTQ
								IF lNegVal
									cTQ = '0'
								ELSE
									cTQ = ALLT(STR(nDetTotqty))
								ENDIF
							ENDIF
						ENDIF
						STORE "W07"+cfd+cTQ+cfd+"CT"+cfd+cUPC+cfd+"ST"+cfd+cDetStyle+cfd+"LN"+cfd+cLN+csegd TO cString
						DO cstringbreak
						nSegCtr = nSegCtr + 1
				ENDCASE
			ENDFOR
		ENDIF
	ENDSCAN
	SELECT indet
ENDPROC
