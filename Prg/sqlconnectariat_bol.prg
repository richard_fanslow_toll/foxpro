PARAMETERS nAcctNum,cCustName,cPPName,lUCC,cOffice,lSQL3,lMoret,lDoCartons,lDoScanpack
IF VARTYPE(cOffice) # "C"
	cOffice = "C"
ENDIF
csql = "tgfnjsql01"
cSQLPass = ""
IF lTesting
*SET STEP ON
ENDIF
SELECT 0
USE F:\wh\sqlpassword
cSQLPass = ALLTRIM(sqlpassword.PASSWORD)
USE IN sqlpassword

SET ESCAPE ON
ON ESCAPE CANCEL
cFileOutName = "V"+TRIM(cPPName)+"pp"
nAcct = ALLTRIM(STR(nAcctNum))

*!*	IF USED("tempar")
*!*		USE IN tempar
*!*	ENDIF

lcDSNLess="driver=SQL Server;server=tgfnjsql01;uid=SA;pwd=&cSQLPass;DATABASE=PICKPACK"
nHandle=SQLSTRINGCONNECT(m.lcDSNLess,.T.)
SQLSETPROP(0,'DispLogin',3)
SQLSETPROP(0,"dispwarnings",.F.)
WAIT WINDOW "Now processing "+cCustName+" SQL view...please wait" NOWAIT
IF nHandle<1 && bailout
	SET STEP ON
	WAIT WINDOW "Could not make SQL connection" TIMEOUT 3
	cRetMsg = "NO SQL CONNECT"
	RETURN cRetMsg
	THROW
ENDIF

lAppend = .F.

SELECT sqlpt

*!* Scans through all PTs within the OUTSHIP BOL#
IF lTesting AND lTestinput
*!*		brow
*!*		SET STEP ON
ENDIF

SET STEP ON 
SCAN
*!*		nWO_Num = sqlwo.wo_num
*!*		nWo   = Alltrim(Str(nWO_Num))
	cPT = ALLTRIM(sqlpt.ship_ref)
	lcQ3 = " '&cPT' "

*!* Changed select to drop accountid requirement, 12.30.2005
	IF lTestinput
		WAIT WINDOW "SQL Records will be selected from LABELS" TIMEOUT 2
		lcQ1=[SELECT * FROM dbo.labels Labels]
		lcQ2 = [ WHERE Labels.ship_ref = ]
		lcQ6 = [order by Labels.ship_ref,Labels.outshipid,Labels.ucc,Labels.outdetid]
		lcsql = lcQ1+lcQ2+lcQ3+lcQ6
	else
		if usesql()
			if !lscanpack
				xorderby="order by outshipid, ucc, outdetid"
			else
				xorderby="order by outshipid, outdetid, upc, ucc"
			endif

			xcuts = iif(nAcctNum=6532,'XYZ','CUTS')
			
			if xsqlexec("select * from cartons where ship_ref='"+cpt+"' and ucc#'"+xcuts+"' "+xorderby,cFileOutName,,"pickpack") = 0
				ASSERT .F. MESSAGE "At NO RECORDS statement in SQLConnect"
				WAIT WINDOW "No "+cCustName+" records found" TIMEOUT 2
				cRetMsg = "NO SQL CONNECT"
				RETURN cRetMsg
			endif
		else
			lcQ1 = [SELECT * FROM dbo.cartons Cartons]
			lcQ2 = [ WHERE Cartons.ship_ref = ]
			lcQ4 = [ AND  Cartons.ucc <> ]
			lcQ5 = IIF(nAcctNum=6532," 'XYZ' "," 'CUTS' ")
			IF !lScanpack
				lcQ6 = [order by Cartons.outshipid,Cartons.ucc,Cartons.outdetid]
			ELSE
				lcQ6 = [order by Cartons.outshipid,Cartons.outdetid,Cartons.upc,Cartons.ucc]
			ENDIF

			lcsql = lcQ1+lcQ2+lcQ3+lcQ4+lcQ5+lcQ6
		endif
	ENDIF

	if usesql()
	else
		llSuccess=SQLEXEC(nHandle,lcsql,cFileOutName)

		IF llSuccess=0  && no records 0 indicates no records and 1 indicates records found
			ASSERT .F. MESSAGE "At NO RECORDS statement in SQLConnect"
			WAIT WINDOW "No "+cCustName+" records found" TIMEOUT 2
			cRetMsg = "NO SQL CONNECT"
			RETURN cRetMsg
		ENDIF
		SQLCANCEL(nHandle)
	endif
	
	IF lAppend = .F.
		lAppend = .T.
		SELECT &cFileOutName
		IF lTesting
*!*			ASSERT .f. MESSAGE "In SQLConnect, browsing 'V' file"
*!*			BROWSE
		ENDIF
		COPY TO ("F:\3pl\DATA\tempar")
		USE ("F:\3pl\DATA\tempar") IN 0 ALIAS tempar
	ELSE
		SELECT &cFileOutName
		SCAN
			SCATTER MEMVAR
			INSERT INTO tempar FROM MEMVAR
		ENDSCAN
	ENDIF
ENDSCAN
IF USED(cFileOutName)
	USE IN &cFileOutName
ENDIF

SELECT tempar
LOCATE
IF !lScanpack  && For future non-Scanpack use.
	SELECT * FROM tempar ;
		WHERE ucc # "CUTS" ;
		ORDER BY outshipid,outdetid ;
		INTO CURSOR &cFileOutName READWRITE
ELSE
	SELECT * FROM tempar ;
		WHERE ucc # "CUTS" ;
		ORDER BY outshipid,outdetid,upc,ucc ;
		INTO CURSOR &cFileOutName READWRITE
ENDIF


*!*	IF lUCC
*!*		SELECT * FROM tempar ;
*!*			ORDER BY ship_ref,outshipid,ucc ;
*!*			INTO CURSOR &cFileOutName READWRITE
*!*	ELSE
*!*		SELECT * FROM tempar ;
*!*			ORDER BY ship_ref,outshipid,cartonnum ;
*!*			INTO CURSOR &cFileOutName READWRITE
*!*	ENDIF
IF lTesting
	SELECT &cFileOutName
*BROWSE
ENDIF

USE IN tempar
USE IN sqlpt
GO BOTT
WAIT CLEAR
*WAIT "" TIMEOUT 3
SQLDISCONNECT(nHandle)

cRetMsg = "OK"
RETURN cRetMsg
