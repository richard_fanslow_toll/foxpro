lparameters xdrop

if !empty(daily.trailer)
	if seek(left(daily.trailer,8),"trailer","trail_num")
		replace wo_num with daily.wo_num, ;
				accountid with daily.accountid, ;
				acctname with daily.acctname, ;
				location with xendloc, ;
				dropped with daily.dispatched, ;
				droptime with daily.time_in, ;
				dispatched with {}, ;
				emptyasof with {}, ;
				poscs with xendloc, ;
				postm with daily.time_in, ;
				posdt with xdate, ;
				dateinloc with xdate in trailer
	endif
	
	if xdrop
		if seek(daily.trailer,"drop","trailer")
			delete in drop
		endif

		select daily
		scatter memvar memo
		m.container=""

		store "" to m.action, m.dis_truck, m.dis_driver, m.dis_time, m.time_in, m.addby, m.seal
		store {} to m.emptyondt, m.dispatched, m.adddt
		m.undel=.f.
		m.pdnf=.f.
		m.pdstartdt={}
		m.returned={}
		m.delivered=xdate
		m.priority=5

		if empty(m.pickedup)
			=seek(daily.wo_num,"wolog","wo_num")
			m.pickedup=wolog.pickedup
		endif

		do case
		case xendloc="CARTERET YARD"
			m.delloc="CARTERET, NJ"
		case xendloc="SAN PEDRO YARD"
			m.delloc="SAN PEDRO, CA"
		case xendloc="WILMINGTON YARD"
			m.delloc="WILMINGTON, CA"
		case xendloc="MIRA LOMA YARD"
			m.delloc="MIRA LOMA, CA"
		otherwise
			m.delloc=xendloc
		endcase
		
		m.dropto=""
		m.remarks=""
		m.addfrom="86"
		m.zorder=0
		m.dropid=x3genpk("drop","wo")
		insert into drop from memvar

*		replace dropdt with qdate(), droptime with datetime2time(qdate(.t.)), yardloc with m.dropto, dropupdfrom with "!1" in wolog for wo_num=xwonum

	endif
endif
