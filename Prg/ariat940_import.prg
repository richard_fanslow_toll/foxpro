WAIT WINDOW "Now in IMPORT Phase..." NOWAIT
EmailcommentStr = ""
nptqty = 0
m.UploadCount = 0
m.acct_num = nAcctNum
cPTString = ""

IF lTesting
	lTestImport = .T.
	cUseFolder = "h:\fox\"
ENDIF

guserid = "TGFPROC"

IF lTesting
	USE (cUseFolder+"PT") IN 0 ALIAS pt
	USE (cUseFolder+"PTDET") IN 0 ALIAS ptdet
	USE (cUseFolder+"WHGENPK") IN 0 ALIAS whgenpk
ENDIF

IF USED('outship')
	USE IN outship
ENDIF
USE (cUseFolder+"OUTSHIP") IN 0 ALIAS outship

ASSERT .f.
SELECT xpt
SET ORDER TO
lnCount = 0
STORE RECCOUNT() TO lnCount
LOCATE
IF !lTesting
WAIT WINDOW "Picktickets to load ...........  "+STR(lnCount) nowait
endif
cPTString = PADR("CONSIGNEE",42)+"PICKTICKET"+CHR(13)

*lcWarehouse = "C"
gMasterOffice = "K"
gOffice = "K"

SELECT xpt
SCAN
	WAIT "AT XPT RECORD "+ALLTRIM(STR(RECNO())) WINDOW NOWAIT NOCLEAR
*!* Check for existence of PT in outship
	IF SEEK(STR(xpt.accountid,4)+xpt.ship_ref,'outship','acctpt')
		WAIT WINDOW "Found in outship...looping" NOWAIT
		SELECT xpt
		LOOP
	ENDIF

	SELECT pt
		xShip_ref = xpt.ship_ref
		lcQuery = [select * from pt where accountid = ]+TRANSFORM(nAcctNum)+[ and ship_ref = ']+xShip_ref+[']
		xsqlexec(lcQuery,"p1",,"wh")
		IF RECCOUNT()>0
			SELECT xpt
			LOOP
		ENDIF
		USE IN p1

	SELECT xpt
	cPTString = cPTString+CHR(13)+PADR(ALLT(xpt.consignee),42)+TRIM(xpt.ship_ref)
	nPTID = xpt.ptid
	SCATTER MEMVAR MEMO
		m.office = cOffice
		m.mod = cMod
	m.adddt = DATETIME()
	m.addby = "TGF-PROC"
	m.addproc = "ARIAT940"
	m.accountid = m.acct_num
	SELECT pt
	insertinto("pt","wh",.T.)

	nptqty = nptqty+1

	m.UploadCount = m.UploadCount  +1
	SELECT xptdet
	SCAN FOR ptid=xpt.ptid
		SCATTER MEMVAR MEMO
			gMasterOffice = IIF(INLIST(cOffice,"J","I"),"N",cOffice)
			m.office = cOffice
			m.mod = cMod
			gOffice = cMod

		m.accountid = nAcctNum
		m.ptid=pt.ptid
		insertinto("ptdet","wh",.T.)
	ENDSCAN
ENDSCAN

*!*	IF lLoadSQL
*!*		tu("pt")
*!*		tu("ptdet")
*!*	ENDIF

IF !lTestImport
	pthist("P")
	pthist("E")
ENDIF

SELECT xpt
*!*	LOCATE
*!*	pickticket_num_start = xpt.ship_ref
*!*	GOTO BOTT
*!*	pickticket_num_end = xpt.ship_ref
*!*	LOCATE

*COPY FILE [&xfile] TO [&ArchiveFile]

DELETE file [&xfile]

IF !lTesting
	WAIT WINDOW "Picktickets uploaded...........  "+STR(m.UploadCount) nowait
*!*	ELSE
*!*		WAIT WINDOW "No New Picktickets uploaded from file "+xfile TIMEOUT 2
ENDIF

WAIT "940 Import Round Complete" WINDOW nowait
WAIT CLEAR

USE IN pt
USE IN ptdet
USE IN outship
IF USED('whgenpk')
	USE IN whgenpk
ENDIF

currfile = xfile
DO CASE
	CASE cOffice = "C"
		cOfficeGroup = "CA"
	CASE cOffice = "N"
		cOfficeGroup = "NJ"
	CASE cOffice = "M"
		cOfficeGroup = "FL"
	OTHERWISE
		SELECT 0
		USE F:\3pl\DATA\mailmaster ALIAS mm SHARED
		LOCATE FOR edi_type = "MISC" AND taskname = "GENERAL"
		tsendto = ALLTRIM(IIF(mm.use_alt,mm.sendtoalt,mm.sendto))
		tcc = ALLTRIM(IIF(mm.use_alt,mm.ccalt,mm.cc))
		USE IN mm
		cOfficeGroup = "XX"
ENDCASE
IF lTesting
	tcc = ""
ENDIF

EmailcommentStr = EmailcommentStr+CHR(13)+cPTString

*************************************************************************************************
*!* E-Mail process
*************************************************************************************************
lEmail = IIF(lTesting,.f.,lEmail)
IF nptqty > 0
	cPTQty = ALLT(STR(nptqty))
	IF lEmail
		IF lTesting OR lTestImport
			SELECT 0
			USE F:\3pl\DATA\mailmaster ALIAS mm SHARED
			LOCATE FOR edi_type = "MISC" AND taskname = "GENERAL"
			tsendto = ALLTRIM(IIF(mm.use_alt,mm.sendtoalt,mm.sendto))
			tcc = ALLTRIM(IIF(mm.use_alt,mm.ccalt,mm.cc))
			USE IN mm
		ELSE
			IF cOfficeGroup = "XX"
				tcc = ""
			ENDIF
		ENDIF
		cMailLoc = IIF(cOffice = "C","West",IIF(cOffice = "N","New Jersey","Florida"))
		tsubject= "TGF "+cMailLoc+" "+cCustname+" Pickticket Upload: " +TTOC(DATETIME())
		tattach = ""
		tmessage = "Uploaded "+cPTQty+" Picktickets for "+cCustname+CHR(13)+"From File: "+cfilename+CHR(13)
		tmessage = tmessage + "(Saved as our file: "+cArchiveFileName+")"+CHR(13)
		tmessage = tmessage + EmailcommentStr
		tFrom ="TGF EDI Operations <toll-edi-ops@tollgroup.com>"
		IF lTesting OR lTestImport
			tmessage = tmessage+CHR(13)+"*TEST DATA* - LOADED INTO F:\WHP TABLES"
		ENDIF
		DO FORM m:\dev\frm\dartmail2 WITH tsendto,tFrom,tsubject,tcc,tattach,tmessage,"A"
	ENDIF
	nptqty = 0
	cPTQty = ""
ENDIF
*************************************************************************************************

RETURN
