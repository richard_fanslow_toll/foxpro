Public xfile,asn_origin,TranslateOption,lcScreenCaption

runack("JAGASN")
xfile = ""
set safety off
set exclusive off
set enginebehavior 70

lcScreenCaption = "Jones ASN Uploader Process... Processing File: "
_Screen.Caption = lcScreenCaption

Wait Window At 10,10 " Now uploading Jones 856's from LOG-NET........." Timeout 2
TranslateOption = "CR_TILDE" &&"NONE"
asn_origin = "LOGNET"

Do createx856

xsqlexec("select * from dellocs",,,"wo")
index on str(accountid,4)+location tag acct_loc
set order to

use f:\jones856\lateupdt in 0

*Set Default To "f:\jones856\asnin\"
lcPath        = "f:\jones856\asnin\"
lcArchivePath = "f:\jones856\asnarchive\"
Wait Window At 10,10  "Processing Jones 856s from Lognet.................." Timeout 1

*Delimiter = "*"
Delimiter = Chr(07)
Do ProcessJonesASN With "FMI"

Delimiter = Chr(07)
Do ProcessJonesASN With "JAG"


* then process asns from JAG
Wait Window At 10,10 " Now uploading Jones 856's from JAG........." Timeout 2
Delimiter = "*"
TranslateOption="85H"
asn_origin = "JAG"

*Set Default To  "f:\jag3pl\asnin\"
lcPath        = "f:\jag3pl\asnin\"
lcArchivePath = "f:\jag3pl\asnarchive\"
Wait Window At 10,10  "Processing Jones 856s from JAG.................." Timeout 1
Do ProcessJonesASN With "FMI"

use in dellocs
use in lateupdt

***********************************************************************************************************

***********************************************************************************************************
procedure processjonesasn
parameters filemask

	lnNum = Adir(TARRAY,lcPath+"*.856")

	if lnnum=0
	  return
	endif

	lnnum2 = asort(tarray)

	If lnNum >= 1
	  For thisfile = 1  To lnNum
	    xfile = lcPath+TARRAY[thisfile,1]  &&+"."
	    archivefile = lcArchivePath+TARRAY[thisfile,1]  &&+"."
	    Wait Window "Importing file: "+xfile Nowait

	    If File(xfile)
	      If At(Upper(filemask),Upper(xfile)) > 0 && only import the files like "FMI856"
			_Screen.Caption = lcScreenCaption+xfile
			Do loadedifile With xfile,Delimiter,TranslateOption,"JONES856"

			do importjagasn

			copy file &xfile to &archivefile
			if file(archivefile)
				delete file &xfile
			endif
	      Endif
	    Endif
	  Next thisfile
	Else
	  Wait Window At 10,10  "          No 856's to Import          " Timeout 1
	Endif

Endproc
******************************************************************************************************************

******************************************************************************************************************
Procedure ImportJAGASN

Try

  Set Century On
  Set Date To YMD
  lcUPC = "UNK"

  If Type("asn_origin") $ "L"
    asn_origin = "LOGNET"
  Endif
  Select x856
  Goto Top

  Store 0 To lnTotalAdded
  lnCurrentTripNumber =0
  lcHLLevel = ""
  lcVendorCode = ""
  Store 1000 To lnHdrid
  lcCurrentHAWB = ""

  tfile = "856"+"FMI"+Alltrim(Str(Year(Date())))+Padl(Alltrim(Str(Month(Date()))),2,"0")+;
    PADL(Alltrim(Str(Day(Date()))),2,"0")+Padl(Alltrim(Str(Hour(Datetime()))),2,"0")+;
    padl(Alltrim(Str(Minute(Datetime()))),2,"0")+Padl(Alltrim(Str(Sec(Datetime()))),2,"0")+".EDI"

  tLog = Fcreate('f:\jones856\logs\&tfile')
  =Fputs(tLog,"Processing of file "+xfile+" started at "+Ttoc(Datetime()))

  If !Used("csize")
    Use F:\jones856\Data\csize In 0 Alias csize
  Endif

  testing=.F.


  If testing = .T.
    If Used("JDetail")
      Use In jdetail
    Endif

    If !Used("JDetail")
      Use F:\jones856\test\jonesdetail In 0 Alias jdetail
    Endif
    If !Used("jshipments")
      Use F:\jones856\test\shipments In 0 Alias jshipments
    Endif
    If !Used("jonesplhdr")
      Use F:\jones856\jonesplhdr In 0 Alias jonesplhdr
    Endif
    If !Used("jonespldet")
      Use F:\jones856\jonespldet In 0 Alias jonespldet
    Endif
    If !Used("arrivals")
      Use F:\jones856\arrivals In 0 Alias arrivals
    Endif
  Else
    If !Used("JDetail")
      Use F:\jones856\jonesdetail In 0 Alias jdetail
    Endif
    If !Used("jshipments")
      Use F:\jones856\shipments In 0 Alias jshipments
    Endif
    If !Used("jonesplhdr")
      Use F:\jones856\jonesplhdr In 0 Alias jonesplhdr
    Endif
    If !Used("jonespldet")
      Use F:\jones856\jonespldet In 0 Alias jonespldet
    Endif
    If !Used("arrivals")
      Use F:\jones856\arrivals In 0 Alias arrivals
    Endif
  Endif


  Select * From "f:\jones856\jonesplhdr.dbf" Where .F. Into Cursor jhdr Readwrite
  Select * From "f:\jones856\jonespldet.dbf" Where .F. Into Cursor jdet Readwrite

  POs_added = 0
  ShipmentNumber = 1
  ThisShipment = 0
  origawb = ""
  lcCurrentShipID = ""
  lcCurrentArrival = ""
  LAFreight = .F.

  Select jdetail
  Set Filter To
  Set Order To filename
  seek alltrim(upper(xfile))

  **added to delete on reimport of file if not already loaded into the fmi system - mvw 02/13/07
  delete while filename=alltrim(upper(xfile)) for empty(wo_num)

  Select x856
  Set Filter To
  Goto Top

  Do While !Eof("x856")
    Wait Window "At the outer loop " Nowait

    If Trim(x856.segment) = "BSN"
      lcCurrentShipID = Alltrim(x856.f2)
      ll856Update = .F.
      Select jshipments
      Append Blank
      Replace jshipments.shipmentid With lcCurrentShipID
      Replace jshipments.bsn02      With Alltrim(x856.f1)
      Replace jshipments.dateloaded With Datetime()
      Replace jshipments.filename   With Upper(xfile)
      Replace jshipments.loadedfrom With asn_origin
      lcCurrentHAWB = Alltrim(x856.f2)
      Select x856
    Endif

    If Trim(x856.segment) = "BSN"&& and alltrim(x856.f1) = "05"  && must delete then add new info
      ll856Update = .T.
      lcShipID = Alltrim(x856.f2)

      Select jdetail
      Set Order To shipid
      Seek lcShipID
      If Found()
        Select jdetail
		**BSN code of "03" indicates a delete of this hawb/shipid, must delete in jonesdesdetail - mvw 
*!*	        Delete For shipid = lcShipID And Inlist(delloc,"LA1","NJ1")
		locate for shipid = lcShipID and !empty(wo_num)
		if !found()
			xfilter=iif(alltrim(x856.f1)="03","","And Inlist(delloc,'LA1','NJ1')")
	        Delete For shipid = lcShipID &xfilter
		else
			insert into lateupdt (zdatetime, hawb, info) values (datetime(),lcShipID,"Hawb Deletion for hawb already loaded to FMI")
		endif
	  Endif

      Select jonesplhdr
      Set Order To shipmentid
      Seek lcShipID
      If Found()
		**BSN code of "03" indicates a delete of this hawb/shipid, must delete in jonesdesdetail - mvw 
*!*	        Delete For shipmentid = lcShipID And Inlist(delloc,"LA1","NJ1")
		xfilter=iif(alltrim(x856.f1)="03","","And Inlist(delloc,'LA1','NJ1')")
        Delete For shipmentid = lcShipID &xfilter
      Endif
    Endif

    Select x856
    If !Eof()
      Skip 1 In x856
    Endif

    If At("HL",x856.segment)>0
      lcHLLevel = Alltrim(x856.f3)
    Endif

    If lcHLLevel = "S"
      Select jdetail
      Append Blank
      Replace jdetail.filename   With Upper(xfile)
      Replace jdetail.dateloaded With datetime()
      Replace jdetail.shipid     With lcCurrentShipID
      Replace jdetail.loadedfrom With asn_origin
      ShipmentNumber = ShipmentNumber +1
      ThisShipment = ShipmentNumber
    Endif

    Do While ShipmentNumber = ThisShipment
      Wait Window "Importing shipment # "+Str(ShipmentNumber) Nowait
      Select x856
      Skip 1 In x856

      If Trim(x856.segment) = "HL" And Alltrim(x856.f3) = "S"
		Skip -1 In x856
        Exit
      Endif

      If Trim(x856.segment) = "HL"
        lcHLLevel = Trim(x856.f3)
      Endif

      If lcHLLevel = "S"
set step on 




        If At("V1",x856.segment)>0
          Replace jdetail.vessel With Upper(Alltrim(x856.f2))
          lcCurrentVessel = jdetail.vessel
          Replace jdetail.voyagenum With Upper(Alltrim(x856.f4))
          Replace jdetail.ssco      With Upper(Alltrim(x856.f5))
          If Alltrim(x856.f9) = "A"
            Replace jdetail.Type With "A"
            lcCurrentType = jdetail.Type
          Endif
          If Alltrim(x856.f9) = "S"
            Replace jdetail.Type With "O"
            lcCurrentType = jdetail.Type
            Replace jdetail.awb With origawb
            lcCurrentAWB = jdetail.awb
          Endif
        Endif

		&& an arrival means we have some LA1 freight
        If Trim(x856.segment) = "REF" And Upper(Trim(x856.f1)) = "##"
          Replace jdetail.arrival With Alltrim(x856.f2)
          lcAlias = Alias()
          Select arrivals  && not sure what the container number is just yet
          Locate For arrival= Alltrim(x856.f2)
          If !Found()
            Append Blank
            Replace arrivals.arrival With Alltrim(x856.f2)
          Else

          Endif
          lcCurrentArrival = arrivals.arrival
          Store .T. To LAFreight
          Select &lcAlias
        Endif

        If Trim(x856.segment) = "DTM" And Upper(Trim(x856.f1)) = "161"
          ldLoadeddate = Substr(Alltrim(x856.f2),1,4)+"/"+Substr(Alltrim(x856.f2),5,2)+"/"+Substr(Alltrim(x856.f2),7,2)
          Replace jdetail.loaded With Ctod(ldLoadeddate)
        Endif

        If Trim(x856.segment) = "DTM" And Upper(Trim(x856.f1)) = "011"
          ldShippeddate = Substr(Alltrim(x856.f2),1,4)+"/"+Substr(Alltrim(x856.f2),5,2)+"/"+Substr(Alltrim(x856.f2),7,2)
          Replace jdetail.shipped With Ctod(ldShippeddate)
        Endif

        If Trim(x856.segment) = "DTM" And Upper(Trim(x856.f1)) = "371"
          ldEstArrivaldate = Substr(Alltrim(x856.f2),1,4)+"/"+Substr(Alltrim(x856.f2),5,2)+"/"+Substr(Alltrim(x856.f2),7,2)
          Replace jdetail.estarrival With Ctod(ldEstArrivaldate)
        Endif

        If Trim(x856.segment) = "DTM" And Upper(Trim(x856.f1)) = "017"
          ldEstDeldate = Substr(Alltrim(x856.f2),1,4)+"/"+Substr(Alltrim(x856.f2),5,2)+"/"+Substr(Alltrim(x856.f2),7,2)
          Replace jdetail.estdeliv With Ctod(ldEstDeldate)
        Endif

		**added the possibility of receiving a REF*TB (Trucking BOL) as oppoed to the correct REF*MB (Master BOL) 
		**  in case of data entry error son Jones' side - mvw 03/21/17
        If Trim(x856.segment) = "REF" And inlist(Upper(Trim(x856.f1)),"MB","TB")
          Replace jdetail.hawb With Alltrim(x856.f2)
          lcCurrentHAWB = jdetail.hawb
        Endif
        If Trim(x856.segment) = "REF" And Upper(Trim(x856.f1)) = "AW"
          Replace jdetail.hawb With Alltrim(x856.f2)
          lcCurrentHAWB = jdetail.hawb
          If LAFreight
            Replace arrivals.hawb With lcCurrentHAWB
          Endif
        Endif

**origin now populated from N1 segment  - 09/11/07 mvw
*!*	        If Trim(x856.segment) = "R4" And Upper(Trim(x856.f1)) = "O"
*!*	          Replace jdetail.origin With Upper(Alltrim(x856.f3))
*!*	        Endif

**discharged now populated from N1 segment - 09/11/07 mvw
**grab discharge from R4 again, dest from n1 - mvw 03/22/11
        If Trim(x856.segment) = "R4" And Upper(Trim(x856.f1)) = "D"
          Replace jdetail.discharge With Upper(Alltrim(x856.f3))
        Endif

**taken out as we appear to be grabbing delloc from 2 places - mvw 05/12/08
*!*	        if lower(trim(x856.f2))="gdx opt:005" &&delloc
*!*	       		replace jdetail.delloc with upper(alltrim(x856.f3))
*!*	        	lcCurrentDelloc = jdetail.delloc
*!*	        endif

        If inlist(lower(trim(x856.f2)),"gdx opt:010","gdx opt:act frght fwdr")
          Replace jdetail.fwdr With Upper(Alltrim(x856.f3))
          lcCurrentFWDR = jdetail.fwdr
        Endif

        If Trim(x856.f2) = "GDX OPT:015"
          origawb =Alltrim(x856.f3)
          tempawb = Strtran(Alltrim(x856.f3),"-","")
          tempawb = Substr(tempawb,1,3)+"-"+Substr(tempawb,4,4)+"-"+Substr(tempawb,8,4)
          Replace jdetail.awb With Alltrim(tempawb)
          lcCurrentTempAWB = jdetail.awb
        Endif

      Endif

      If lcHLLevel = "E"
        Select x856
        Skip -1
        Do While lcHLLevel = "E"
          If Trim(x856.segment) = "HL" And Alltrim(x856.f3) != "E"
            lcHLLevel = Alltrim(x856.f3)
            Exit
          Endif

          If Trim(x856.segment) = "TD3"
            Replace jdetail.ctrprfx   With Upper(Alltrim(x856.f2))
            Replace jdetail.Container With Alltrim(x856.f3)
            Replace jdetail.awb With ""

            If Empty(jdetail.multiconts)
              Replace jdetail.multiconts With Upper(Alltrim(x856.f2))+Alltrim(x856.f3)
            Else
              Replace jdetail.multiconts With jdetail.multiconts + Chr(13)+Upper(Alltrim(x856.f2))+Alltrim(x856.f3)
            Endif

            If LAFreight
              Select arrivals
              Locate For Alltrim(arrivals.ctrprfx) = Alltrim(jdetail.ctrprfx) And Alltrim(arrivals.Container) = Alltrim(jdetail.Container)
              If !Found()
                Append Blank
                Replace arrivals.arrival With lcCurrentArrival
                Replace arrivals.ctrprfx   With jdetail.ctrprfx
                Replace arrivals.Container With jdetail.Container
              Endif
            Endif

            Select csize
            Locate For Code = Alltrim(x856.f10)
            If Found()
              Replace jdetail.Size With csize.Size
              replace jdetail.code with Alltrim(x856.f10)
            Else
              Replace Size With Alltrim(x856.f10) In jdetail
              replace code with Alltrim(x856.f10) in jdetail
            Endif
            Select x856
          Endif

          If Trim(x856.segment) = "REF" And Upper(Trim(x856.f1)) = "AW"
            origawb =Alltrim(x856.f2)
            tempawb = Strtran(Alltrim(x856.f2),"-","")
            tempawb = Substr(tempawb,1,3)+"-"+Substr(tempawb,4,4)+"-"+Substr(tempawb,8,4)
            Replace jdetail.awb With Alltrim(tempawb)
            lcCurrentTempAWB = jdetail.awb
            If LAFreight
              Replace arrivals.awb With jdetail.awb
            Endif
          Endif

          If Trim(x856.segment) = "REF" And Upper(Trim(x856.f2)) = "GDX OPT:CARRIER OBL"
			Replace info with info+"CARRIEROBL*"+alltrim(x856.f3)+chr(13) in jdetail
          Endif

          Select x856
          Skip 1
        Enddo
      Endif

      If lcHLLevel = "O"
        If Trim(x856.segment) = "PRF"
          Replace jdetail.po_num   With Alltrim(x856.f1)
          POs_added = POs_added +1
        Endif

		do case
		case lower(trim(x856.f2))="gdx opt:405" &&division
			Replace jdetail.div  With Upper(Trim(x856.f3))
        case Trim(x856.f2) = "GDX OPT:410"
          Replace jdetail.season With Upper(Trim(x856.f3))
        case Trim(x856.f2) = "GDX OPT:520"
          Replace jdetail.caseid With Upper(Trim(x856.f3))
        Endcase
      Endif

      If lcHLLevel = "I" && stay here until we loop out of the Item Loop
        Do While lcHLLevel = "I"
		  * another shipment ?
			do case
			case Trim(x856.segment) = "HL"
				lcHLLevel = Alltrim(x856.f3)
				do case
				case Alltrim(x856.f3) = "S"
					ThisShipment = -1
					Select x856
					LAFreight = .F.
					lcCurrentArrival = ""
					Skip -1
					Loop
				case inlist(Alltrim(x856.f3),"O","E")
					lcHLLevel = "O"
					Select jdetail
					**carry over carrier obl from info to new recs - mvw 10/26/12
					xcarrierobl=getmemodata("info","CARRIEROBL",,.T.)
					Scatter Memvar
					Append Blank
					Gather fields except jdetailid,pl_qty,qty_type,cbm,totalkgs,weight Memvar
					**do not carry over Carrier OBL on equipment change within a Hawb, only po to po, itemm to itwm within a ctr/awb - mvw 04/18/13
					if !empty(xcarrierobl) and lcHLLevel#"E"
						replace info with info+"CARRIEROBL*"+xcarrierobl+chr(13)
					endif
					
					Select x856
					Skip -1
					Loop
				Otherwise
					Select x856
					do case
					case Empty(jdetail.qty_type) and "SHIPTYPE*F"$jdetail.info
						Replace jdetail.qty_type With "CARTONS"
					case Empty(jdetail.qty_type)
					
						Replace jdetail.qty_type With "GOH"
						Replace jdetail.pl_qty   With jdetail.units
					endcase
					Exit
				endcase

			case Trim(x856.segment)="SN1"
				Replace jdetail.units  With Val(x856.f2)

			case Trim(x856.segment) = "MEA" And Trim(x856.f1) = "WT"
				Replace jdetail.totalkgs  With Val(x856.f3)
				Replace jdetail.weight  With jdetail.totalkgs*2.2046

			case Trim(x856.segment) = "MEA" And Trim(x856.f1) = "PD"
				Replace jdetail.cbm      With Val(x856.f3)

			case Trim(x856.segment) = "MEA" And Trim(x856.f1) = "CT" And Trim(x856.f2) = "SQ"
				Replace jdetail.pl_qty  With Val(x856.f3)
				Replace jdetail.qty_type With "CARTONS"

			case Trim(x856.segment) = "N1" And Trim(x856.f1) = "VN"
				lcVendorCode = Upper(Alltrim(x856.f4))

			case Trim(x856.segment) = "N1" And Trim(x856.f1) = "SF"
				Skip 1 In x856
				if Trim(x856.segment) = "N4"
					replace origin with x856.f6 in jdetail
				endif

			case Trim(x856.segment) = "N1" And Trim(x856.f1) = "ST"
				Skip 1 In x856
				if Trim(x856.segment) = "N4"
					**changed to dest as opposed to disacharge, taking in discharge port above - mvw 03/22/11
*!*						replace discharge with x856.f6 in jdetail
					replace dest with x856.f6 in jdetail
				endif

			case Trim(x856.segment) = "REF" And Upper(Trim(x856.f1)) = "RV"
				replace docrcpt with x856.f2 in jdetail

			**added to grab optional "net weight" field value to be used if the weight looks to be incorrect (ctn weight < 1 lbs) - mvw 05/12/10
			case trim(x856.segment)="REF" and lower(trim(x856.f2))="gdx opt:net weight"
				if jdetail.qty_type="CARTONS" and jdetail.weight/jdetail.pl_qty<1
					Replace jdetail.totalkgs  With Val(x856.f3)
					Replace jdetail.weight  With jdetail.totalkgs*2.2046
				endif

			case trim(x856.segment)="REF" and lower(trim(x856.f2))="gdx opt:dest warehouse" &&delloc
				cdelloc=upper(alltrim(left(x856.f3,4)))
				do case
				case inlist(cdelloc,"0111","0611","1511","6011")
					cdelloc="TN1"
				**added 2312, 2612 - mvw 03/17/15
				case inlist(cdelloc,"0112","0612","1512","6012","2312","2612")
					cdelloc="TN2"
				case inlist(cdelloc,"0115","0615","6015")
					cdelloc="VA1"
				case inlist(cdelloc,"0130","0630","6030")
					cdelloc="AL1"
*!*					case inlist(cdelloc,"0190","0690","6090","3090")
*!*						cdelloc="TP1" &&was 3PLW - changed mvw 07/02/08
				case inlist(cdelloc,"0190","0690","6090","3090","4190","4390")
*					cdelloc="TP1" &&was 3PLW - changed mvw 07/02/08
					cdelloc="PLG-MAYO" &&TP1 no longer exists - "90" loca now point to PLG-MAYO per Paul B - 01/18/16 mvw
				case inlist(cdelloc,"0191","0691","6091")
					cdelloc="TP2" &&was 3PLE - changed mvw 07/02/08
				**added 2389 - mvw 03/03/15
				**added 3089, 2689 - mvw 03/17/15
				case inlist(cdelloc,"0189","0689","2389","3089","2689")
					cdelloc="PLG-CHERYL"
				case inlist(cdelloc,"0192","0692")
					**changed to EDISONKGP bc address has changed, need to keep EDISON for older wos - mvw 03/01/13
					cdelloc="EDISONKGP"
				case cdelloc="0632"
					cdelloc="NJ2"
				case cdelloc="1150" &&added 07/06/11
					cdelloc="WDP-1245"
				case inlist(cdelloc,"3050","4150","4350") &&added 10/14/11
					cdelloc="WDP-1250"
				case cdelloc="0699" &&added 02/05/14
					cdelloc="HUDD EAST"
				case inlist(cdelloc,"3030","4130") &&added 01/11/13
					cdelloc="PPACK"
				endcase

        		replace delloc with cdelloc, info with info+"WHCODE*"+alltrim(x856.f3)+chr(13) in jdetail
	        	lcCurrentDelloc = jdetail.delloc

			case lower(trim(x856.f2))="gdx opt:division name" &&division name
				select dellocs
				locate for accountid=1 and label=padr(Upper(Trim(x856.f3)),len(dellocs.label)," ")
				Replace div With iif(found("dellocs"),dellocs.div,"999"), label with Upper(Trim(x856.f3)) in jdetail

			**material number now goes to style, old style goes to sapstyle - mvw 05/07/08
			case Trim(x856.segment) = "REF" And Upper(Trim(x856.f1)) = "IX"
				replace style with upper(alltrim(x856.f2)) in jdetail
			case Trim(x856.segment)="LIN" And Trim(x856.f2)="IN"
				replace sapstyle with upper(alltrim(x856.f3)) in jdetail

			case lower(trim(x856.f2))="gdx opt:material type"
				Replace info with info+"**MATERIALTYPE: "+alltrim(x856.f3)+chr(13) in jdetail
			**grab "original line #" for grouping - mvw 04/13/10
			case lower(trim(x856.f2))="gdx opt:original line #"
				Replace info with info+"ORIGLINE*"+alltrim(x856.f3)+chr(13) in jdetail

			case Trim(x856.segment) = "REF" And lower(trim(x856.f2))="gdx opt:coll dlvy start"
				xyear=iif(substr(x856.f3,1,4)="9999", substr(x856.f3,1,4), substr(x856.f3,3,2))
				replace startdt with ctod(xyear+"/"+substr(x856.f3,6,2)+"/"+substr(x856.f3,9,2)) in jdetail
			case Trim(x856.segment) = "REF" And lower(trim(x856.f2))="gdx opt:coll dlvy end"
				xyear=iif(substr(x856.f3,1,4)="9999", substr(x856.f3,1,4), substr(x856.f3,3,2))
				replace enddt with ctod(xyear+"/"+substr(x856.f3,6,2)+"/"+substr(x856.f3,9,2)) in jdetail

			case Trim(x856.segment) = "REF" And lower(trim(x856.f2))="gdx opt:rcv foh code #1"
				replace info with info+"SHIPTYPE*"+alltrim(x856.f3)+chr(13) in jdetail

			case Trim(x856.segment) = "REF" And inlist(lower(trim(x856.f2)),"gdx opt:010","gdx opt:act frght fwdr")
				replace fwdr with upper(alltrim(x856.f3)) in jdetail
				lccurrentfwdr = jdetail.fwdr 
			endcase

			If jdetail.qty_type = "GOH"
				Replace jdetail.pl_qty With jdetail.units
			Endif

			Select x856
			Skip 1 In x856
			If Eof()
				Exit
			Endif
		Enddo
      Endif

      If lcHLLevel = "P"
        NewPack = .T.

        lnPCTR = 0
        lnTotQty = 0
        Do While lcHLLevel = "P"
			lnPCTR = lnPCTR +1
			Wait Window "At the P Level "+Str(lnPCTR) Nowait

			**added to account for possible missing "PO4" segments - mvw 05/21/8
			lnCurrentPack=0
			do case
			case Trim(x856.segment) = "LIN"
				lcCurrentPO      = jdetail.po_num
				lcCurrentStyle   = jdetail.po_num
				lcCurrentDelloc  = jdetail.delloc
				lcCurrentColor   = Upper(Alltrim(x856.f3))
				lcCurrentSize    = Upper(Alltrim(x856.f5))
				lcCurrentCtrprfx = jdetail.ctrprfx
				lcCurrentCont    = jdetail.Container
				lcCurrentAWB     = jdetail.awb
				lcCurrentFile    = Upper(xfile)
			case Trim(x856.segment) = "SN1"
				lcCurrentQty = Val(Alltrim(x856.f2))
			case Trim(x856.segment) = "PO4"
				lnCurrentPack=Val(Alltrim(x856.f1))
			case Trim(x856.segment) = "REF" And inlist(lower(trim(x856.f2)),"gdx opt:800","gdx opt:upc code")
				lcUPC =Alltrim(x856.f3)
			case Trim(x856.segment) = "REF" And lower(trim(x856.f2))="gdx opt:prepack code" and !("**prepack: true"$lower(jdetail.info))
				xlen=iif(occurs("-",x856.f3)=3, at("-",x856.f3,3), len(x856.f3))
				replace info with info+"**PREPACK: True"+chr(13)+"MASTERSTYLE*"+;
					substr(x856.f3,at("-",x856.f3,1)+1,xlen-at("-",x856.f3,1)-1)+chr(13) in jdetail
			case Trim(x856.segment) = "MAN"
				RepeatCase = .F.
				Select jhdr
				Locate For caseid = lcCurrentPO And ctnlo = Val(x856.f2) And ctnhi = Val(x856.f3)
				RepeatCase = !Eof()

				Do Case
				Case NewPack
					NewPack = .F.
					lastctnlo = Alltrim(x856.f2)
					lastctnhi = Alltrim(x856.f3)

					Select jhdr
					Append Blank
					Replace jhdr.hdrid With lnHdrid
					lnHdrid = lnHdrid+1
					Replace jhdr.dateloaded With datetime()
					Replace jhdr.arrival    With lcCurrentArrival
					Replace jhdr.hawb       With lcCurrentHAWB
					Replace jhdr.Style      With jdetail.Style
					Replace jhdr.caseid     With jdetail.caseid
					Replace jhdr.po_num     With jdetail.po_num
					Replace jhdr.cutid      With jdetail.po_num
					Replace jhdr.delloc     With jdetail.delloc
					Replace jhdr.ctrprfx    With jdetail.ctrprfx
					Replace jhdr.Container  With jdetail.Container
					Replace jhdr.awb        With jdetail.awb
					Replace jhdr.shipmentid With lcCurrentShipID
					Replace jhdr.filename   With Upper(xfile)

					**put in check to make sure it was between 0-99999 to avoid numeric overflow error we've been seeing - mvw 09/10/13
					Replace jhdr.ctnlo      With iif(between(Val(Strtran(x856.f2,",","")),0,999999),Val(Strtran(x856.f2,",","")),0)
					Replace jhdr.ctnhi      With iif(between(Val(Strtran(x856.f3,",","")),0,999999),Val(Strtran(x856.f3,",","")),0)

					Replace jhdr.rcvd       With .F.
					Replace jhdr.confirmed  With .F.
					Replace jhdr.vendor     With lcVendorCode

					If Val(Alltrim(x856.f2)) =0 And Val(Alltrim(x856.f3)) =0
						Replace jhdr.qty_type With "GOH"
						Replace jhdr.ctnqty With 0
						Replace jdet.units With jdet.Pack
					Else
						Replace jhdr.qty_type With "CARTONS"
						Replace jhdr.ctnqty With (Val(Alltrim(Strtran(x856.f3,",","")))) - Val(Alltrim(Strtran(x856.f2,",",""))) + 1
						Replace jdet.units With Iif(jhdr.ctnqty * jdet.Pack > 999999, 999999, jhdr.ctnqty * jdet.Pack)
					Endif

					Select jdet
					Append Blank
					Replace hdrid With jhdr.hdrid
					Replace hdrid  With jhdr.hdrid
					Replace po_num With lcCurrentPO
					Replace upc    With lcUPC
					Replace Pack   With lnCurrentPack &&jonespl.pack
					Replace Color  With lcCurrentColor &&jonespl.color
					Replace Size   With lcCurrentSize &&jonespl.size
					Replace Style  With jhdr.Style
					If jhdr.qty_type = "GOH"
						Replace jdet.units With jdet.Pack
					Else
						Replace jdet.units With jhdr.ctnqty * jdet.Pack
					Endif

				Case RepeatCase = .T.
					Select jdet
					Append Blank
					Replace jdet.hdrid With jhdr.hdrid
					Replace jdet.hdrid  With jonesplhdr.hdrid
					Replace jdet.po_num With lcCurrentPO &&jonespl.po_num
					Replace jdet.upc    With lcUPC &&jonespl.upc
					Replace jdet.Pack   With lnCurrentPack &&jonespl.pack
					Replace jdet.Color  With lcCurrentColor &&jonespl.color
					Replace jdet.Size   With lcCurrentSize &&jonespl.size
					Replace jdet.Style  With jhdr.Style
					Replace jdet.units With jhdr.ctnqty * jdet.Pack

					If jhdr.qty_type = "GOH"
						Replace jdet.units With jdet.Pack
					Else
						Replace jdet.units With jhdr.ctnqty * jdet.Pack
					Endif

				Case lastctnlo = Alltrim(x856.f2) And lastctnhi = Alltrim(x856.f3)
					Select jdet
					Append Blank
					Replace jdet.hdrid  With jhdr.hdrid
					Replace jdet.po_num With lcCurrentPO &&jonespl.po_num
					Replace jdet.upc    With lcUPC &&jonespl.upc
					Replace jdet.Pack   With lnCurrentPack &&jonespl.pack
					Replace jdet.Color  With lcCurrentColor &&jonespl.color
					Replace jdet.Size   With lcCurrentSize &&jonespl.size
					Replace jdet.Style  With jhdr.Style
					Replace jdet.units  With jhdr.ctnqty * jonespldet.Pack

					If jhdr.qty_type = "GOH"
						Replace jdet.units With jdet.Pack
					Else
						Replace jdet.units With jhdr.ctnqty * jdet.Pack
					Endif

				Otherwise
					lastctnlo = Alltrim(x856.f2)
					lastctnhi = Alltrim(x856.f3)
					Select jhdr
					Append Blank
					Replace jhdr.hdrid With lnHdrid
					lnHdrid = lnHdrid+1
					Replace jhdr.dateloaded With datetime()
					Replace jhdr.arrival    With lcCurrentArrival
					Replace jhdr.hawb       With lcCurrentHAWB
					Replace jhdr.Style      With jdetail.Style
					Replace jhdr.delloc     With jdetail.delloc
					Replace jhdr.caseid     With jdetail.caseid
					Replace jhdr.po_num With jdetail.po_num
					Replace jhdr.cutid With jdetail.po_num
					Replace jhdr.ctrprfx    With jdetail.ctrprfx
					Replace jhdr.Container  With jdetail.Container
					Replace jhdr.awb        With jdetail.awb
					Replace jhdr.shipmentid With lcCurrentShipID
					Replace jhdr.filename   With Upper(xfile)

					**put in check to make sure it was between 0-99999 to avoid numeric overflow error we've been seeing - mvw 09/10/13
					Replace jhdr.ctnlo      With iif(between(Val(Strtran(x856.f2,",","")),0,999999),Val(Strtran(x856.f2,",","")),0)
					Replace jhdr.ctnhi      With iif(between(Val(Strtran(x856.f3,",","")),0,999999),Val(Strtran(x856.f3,",","")),0)

					Replace jhdr.rcvd       With .F.
					Replace jhdr.confirmed  With .F.
					Replace jhdr.vendor     With lcVendorCode

					Select jdet
					Append Blank
					Replace jdet.hdrid  With jhdr.hdrid
					Replace jdet.po_num With lcCurrentPO &&jonespl.po_num
					Replace jdet.upc    With lcUPC &&jonespl.upc
					Replace jdet.Pack   With lnCurrentPack &&jonespl.pack
					Replace jdet.Color  With lcCurrentColor &&jonespl.color
					Replace jdet.Style  With jhdr.Style
					Replace jdet.Size   With lcCurrentSize &&jonespl.size

					If Val(Alltrim(x856.f2)) =0 And Val(Alltrim(x856.f3)) =0
						Replace jhdr.qty_type With "GOH"
						Replace jhdr.ctnqty With 0
						Replace jdet.units With jdet.Pack
					Else
						Replace jhdr.qty_type With "CARTONS"
						Replace jhdr.ctnqty With (Val(Alltrim(Strtran(x856.f3,",",""))) - Val(Alltrim(Strtran(x856.f2,",","")))) + 1
						Replace jdet.units With jhdr.ctnqty * jdet.Pack
					Endif
				Endcase

				**put in check to make sure it was between 0-99999 to avoid numeric overflow error we've been seeing - mvw 09/10/13
				lnCtnLO = iif(between(Val(Strtran(x856.f2,",","")),0,999999),Val(Strtran(x856.f2,",","")),0)
				lnCtnHI = iif(between(Val(Strtran(x856.f3,",","")),0,999999),Val(Strtran(x856.f3,",","")),0)

				If lnCtnLO=0 And lnCtnHI=0
					Replace jhdr.qty_type With "GOH"
					Replace jhdr.ctnqty With 0
				Else
					Replace jhdr.qty_type With "CARTONS"
					**make sure we do not have a numeric overflow (ctnqty=n(6))
					xctnqty=(lnCtnHI-lnCtnLO)+1
					Replace jhdr.ctnqty With iif(xctnqty<=999999,xctnqty,0)
				Endif

			case Inlist(Trim(x856.segment),"SE")
				Select x856
				Skip 1 In x856
				If Alltrim(x856.segment)= "ST"
				  Skip 1 In x856
				Endif
				ThisShipment = -1
				lcHLLevel = "S"
				Select x856
				LAFreight = .F.
				lcCurrentArrival = ""
				Exit

			case Trim(x856.segment) = "HL"
				lcHLLevel = alltrim(x856.f3)
				do case
				case lcHLLevel = "S"
					ThisShipment = -1
					Select x856
					Skip -1
					LAFreight = .F.
					lcCurrentArrival = ""
					Exit
				case inlist(lcHLLevel,"O","E","I")
					Select jdetail
					**carry over carrier obl from info to new recs - mvw 10/26/12
					xcarrierobl=getmemodata("info","CARRIEROBL",,.t.)
					Scatter Memvar
					Append Blank
					m.info=""
					Gather fields except jdetailid,pl_qty,qty_type,cbm,totalkgs,weight Memvar
					**do not carry over Carrier OBL on equipment change within a Hawb, only po to po, itemm to itwm within a ctr/awb - mvw 04/18/13
					if !empty(xcarrierobl) and lcHLLevel#"E"
						replace info with info+"CARRIEROBL*"+xcarrierobl+chr(13)
					endif

					Select x856
					Loop
				endcase

			case Trim(x856.segment) = "SN1"
				lnTotQty = lnTotQty + Val(x856.f2)
			Endcase

			Select x856
			Skip 1 In x856
			If Eof()
				Exit
			Endif
		enddo
		Wait Clear
	  Endif

	  If Eof()
		exit
	  EndIf
    EndDo &&ShipmentNumber = ThisShipment
  EndDo &&!eof("x856")

  **delete all previously imported data for ctr+shipid - mvw 02/13/07
  wait "Deleting previously imported data" window nowait
  select jdetail
  set order to

  set deleted on

  select * from jdetail where filename = alltrim(upper(xfile)) group by ctrprfx,container,shipid into cursor xtemp
  scan
	select jdetail
	locate for ctrprfx+container = xtemp.ctrprfx+xtemp.container and shipid=xtemp.shipid and !empty(wo_num)

	if found()
		cfilename=filename
		xdateloaded=dateloaded
		delete for ctrprfx+container = xtemp.ctrprfx+xtemp.container and shipid=xtemp.shipid and ;
			!(filename=alltrim(upper(cfilename)) and dateloaded=xdateloaded)
	else
		delete for shipid=xtemp.shipid and ctrprfx+container = xtemp.ctrprfx+xtemp.container and filename#alltrim(upper(xfile))
	endif
  endscan

  **delete duplicate recs
  select cnt(1) as cnt, * from jdetail where filename=alltrim(upper(xfile)) ;
	group by ctrprfx,container,awb,hawb,po_num,style,sapstyle,delloc,pl_qty,qty_type into cursor xtemp
  scan for cnt>1
	select jdetail
	locate for filename=alltrim(upper(xfile)) and ctrprfx+container=xtemp.ctrprfx+xtemp.container and awb=xtemp.awb and ;
		hawb=xtemp.hawb and po_num=xtemp.po_num and style=xtemp.style and sapstyle=xtemp.sapstyle and delloc=xtemp.delloc and ;
		pl_qty=xtemp.pl_qty and qty_type=xtemp.qty_type and !empty(wo_num)
	if found()
		delete in jdetail ;
			for filename=alltrim(upper(xfile)) and ctrprfx+container=xtemp.ctrprfx+xtemp.container and awb=xtemp.awb and ;
			hawb=xtemp.hawb and po_num=xtemp.po_num and style=xtemp.style and style=xtemp.style and delloc=xtemp.delloc and ;
			pl_qty=xtemp.pl_qty and qty_type=xtemp.qty_type and empty(wo_num)
	else
		delete in jdetail ;
			for filename=alltrim(upper(xfile)) and ctrprfx+container=xtemp.ctrprfx+xtemp.container and awb=xtemp.awb and ;
			hawb=xtemp.hawb and po_num=xtemp.po_num and style=xtemp.style and style=xtemp.style and delloc=xtemp.delloc and ;
			pl_qty=xtemp.pl_qty and qty_type=xtemp.qty_type and jdetailid#xtemp.jdetailid
	endif
  endscan


*!*	  wait "Consolidating suit records" window nowait
*!*	  select cnt(1) as cnt, sum(weight) as totwt, * from jdetail where filename=alltrim(upper(xfile)) and qty_type='GOH' and ;
*!*			"**materialtype: zsut"$lower(info) and "**prepack: true"$lower(info) ;
*!*		group by ctrprfx,container,awb,hawb,po_num,pl_qty into cursor xtemp
*!*	  scan for cnt > 1
*!*		select jdetail
*!*		ncnt=1
*!*		scan for filename=alltrim(upper(xfile)) and ctrprfx+container=xtemp.ctrprfx+xtemp.container and awb=xtemp.awb and ;
*!*		hawb=xtemp.hawb and po_num=xtemp.po_num and pl_qty=xtemp.pl_qty and qty_type='GOH' and "**materialtype: zsut"$lower(info) and "**prepack: true"$lower(info)
*!*			if ncnt>1
*!*				delete
*!*			else
*!*				replace weight with xtemp.totwt
*!*			endif
*!*			ncnt=ncnt+1
*!*		endscan
*!*	  endscan


  **if suit separates and only give weight on the first piece, this weight is total for both line items... must divide accordingly
  wait "Correcting weights on suit separates" window nowait

  select cnt(1) as cnt, max(weight) as maxwt, * from jdetail where filename=alltrim(upper(xfile)) and qty_type='GOH' ;
	group by ctrprfx,container,awb,pl_qty into cursor xtemp
  scan for cnt>1
	select jdetail
	locate for filename=alltrim(upper(xfile)) and ctrprfx+container=xtemp.ctrprfx+xtemp.container and awb=xtemp.awb and pl_qty=xtemp.pl_qty and qty_type='GOH' and !empty(weight)
	if !eof() &&some come in with no weights at all
		nweight=weight
		locate for filename=alltrim(upper(xfile)) and ctrprfx+container=xtemp.ctrprfx+xtemp.container and awb=xtemp.awb and pl_qty=xtemp.pl_qty and qty_type='GOH'

		cstyle=style
		nrecno=recno()

		ntotrecs=1
		continue
		do while !eof()
			nlen=iif(len(alltrim(cstyle))=len(alltrim(style)), len(alltrim(style))-1, min(len(alltrim(cstyle)),len(alltrim(style))))
			cfilter="left(cstyle,"+transform(nlen)+")=left(style,"+transform(nlen)+")"
			if filename=alltrim(upper(xfile)) and ctrprfx+container=xtemp.ctrprfx+xtemp.container and awb=xtemp.awb and ;
			pl_qty=xtemp.pl_qty and qty_type='GOH' and &cfilter &&and empty(weight) - took out this part in case weightless rec came first
				ntotrecs=ntotrecs+1
			endif

			continue
		enddo

		if ntotrecs>1
			nweight=int(nweight/ntotrecs)

			locate for filename=alltrim(upper(xfile)) and ctrprfx+container=xtemp.ctrprfx+xtemp.container and awb=xtemp.awb and pl_qty=xtemp.pl_qty and qty_type='GOH'
			replace weight with (nweight)+mod(xtemp.maxwt,ntotrecs)

			continue
			do while !eof()
				nlen=iif(len(alltrim(cstyle))=len(alltrim(style)), len(alltrim(style))-1, min(len(alltrim(cstyle)),len(alltrim(style))))
				cfilter="left(cstyle,"+transform(nlen)+")=left(style,"+transform(nlen)+")"
				if filename=alltrim(upper(xfile)) and ctrprfx+container=xtemp.ctrprfx+xtemp.container and awb=xtemp.awb and ;
				pl_qty=xtemp.pl_qty and qty_type='GOH' and &cfilter &&and empty(weight) - took out this part in case weightless rec came first
					replace weight with nweight
				endif

				continue
			enddo
		endif
	endif
  endscan
  use in xtemp
  *****

  set deleted off

  *identify the freight for 3PL processing
  *  do f:\jag3pl\prg\identify
  =Fputs(tLog,"Processing of file "+xfile+" ended at "+Ttoc(Datetime()))
  =Fclose(tLog)
  *messagebox("POs added: "+alltrim(str(pos_added)),48,"Jones 856 Processor")


Catch To oErr
  tsubject = "URGENT: JAG ASN Uplaod Error ("+Transform(oErr.ErrorNo)+") at "+Ttoc(Datetime())
  tmessage = "URGENT: This must be fixed and the file must be reimported or it could cause duplicate data"+chr(13)+chr(13)+"JAG ASN Upload Error processing"

  tmessage =tmessage+ "Try/CATCH Exeception Message:"+Chr(13)+;
    [  Error: ] + Str(oErr.ErrorNo) +Chr(13)+;
    [  LineNo: ] + Str(oErr.Lineno) +Chr(13)+;
    [  Message: ] + oErr.Message +Chr(13)+;
    [  Procedure: ] + oErr.Procedure +Chr(13)+;
    [  Details: ] + oErr.Details +Chr(13)+;
    [  StackLevel: ] + Str(oErr.StackLevel) +Chr(13)+;
    [  LineContents: ] + oErr.LineContents+Chr(13)+;
    [  UserValue: ] + oErr.UserValue+Chr(13)+;
    [  ASN Filename: ]+xfile

  tattach  = ""
  tcc=""
  tsendto  = "mwinter@fmiint.com"
  tFrom    ="TGF Corporate Communication Department <fmi-transload-ops@fmiint.com>"
  Do Form dartmail2 With tsendto,tFrom,tsubject,tcc,tattach,tmessage,"A"
Endtry

Endproc

**************************************************************************************
