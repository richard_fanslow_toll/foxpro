Close Data All
Public gsentmsg

Do set_envi
If already_running("mj_edi_status_rpt")
	Wait Window '1' Nowait
	tsendto = "pgaidis@fmiint.com,tmarg@fmiint.com"
	tcc = ""
	tattach = ""
	tFrom ="Toll EDI System Operations <fmi-transload-ops@fmiint.com>"
	tmessage = "Marc Jacobs EDI Report Stuck in Memory: "+Ttoc(Datetime())
	tSubject = "Marc Jacobs Report Alert"
	Do Form m:\dev\frm\dartmail2 With tsendto,tFrom,tSubject,tcc,tattach,tmessage,"A"
	Return
Endif

runack("MJ_EDI_STATUS_RPT")

utilsetup("MJ_EDI_STATUS_RPT")

Do m:\dev\prg\_setvars
_Screen.WindowState=1

Set Deleted On
**!*	Wait window at 10,10 "Running Retail EDI Reports" && timeout 1
**!*	Do form edi_reports with .t.,"..MARC JACOBS RETAIL"

**!*	Wait window at 10,10 "Running Wholesale EDI Reports" && timeout 1
**!*	Do form edi_reports with .t.,".MARC JACOBS WHOLESALE"

*!*	Wait Window At 10,10 "Running Retail Status Report"  Timeout 1
*!*	Do mjr_edi_status_rpt
SET STEP ON 
Wait Window At 10,10 "Running Wholesale Status Report"  Timeout 1
Do mjw_edi_status_rpt

*!*	Wait Window At 10,10 "Running WtoR Status Report"  Timeout 1
*!*	Do mj_wtor_edi_status_rpt

*!*	Wait Window At 10,10 "Running Retail Ondock Report"  Timeout 1
*!*	Do mj_ondockrpt

*!*	Wait Window At 10,10 "Running Wholesale Ondock Report"  Timeout 1
*!*	Do mj_w_ondockrpt

Wait Window At 10,10 "Running Appt Status Report"  Timeout 1
Do mj_appt_status

*!*	Wait Window At 10,10 "Running Appt Status Report2"  Timeout 1
*!*	Do mj_appt_status2

*!*	Wait Window At 10,10 "Running Retail RCV Status Report"  Timeout 1
*!*	Do mj_retail_rcv_status

Wait Window At 10,10 "Running Retail RCV Status Report"  Timeout 1
Do mj_wholesale_rcv_status

Wait Window At 10,10 "Running Unallocated Status Reports"  Timeout 1
Do mj_unallocated

Wait Window At 10,10 "Running Unallocated Status Reports"  Timeout 1
Do mj_bol_detail

*!*	Wait Window At 10,10 "Running INTL Wip Status Reports"  Timeout 1
*!*	Do mjwip_intl

Wait Window At 10,10 "Running New Wholesale\Retail Delivery Status Reports"  Timeout 1
Do new_wholesale_deliveries

*!*	Wait Window At 10,10 "Running Ouitslip/Voucher Log Report"  Timeout 1
*!*	Do mj_log_export

*!*	Wait Window At 10,10 "Running Partner WIP"  Timeout 1
*!*	Do mj_PARTNER_WIP

*!*	Wait Window At 10,10 "Running WIP  Status Report#2"  Timeout 1
*!*	Do mj_spec_wip

*!*	Wait Window At 10,10 "Running WIP  Status Report"  Timeout 1
*!*	Do m:\dev\prg\mj_wip

*!*	Wait Window At 10,10 "Running ECOM Deliv Detail Report"  Timeout 1
*!*	Do m:\dev\prg\mj_ecom_deliv_dtls


schedupdate()


Wait Window At 10,10 "All done........" Timeout 2

Return
***************************************************************************************************
Procedure check_retail

	Use h:\fox\mj_retail_deliveries Alias rdata
	lcIssueStr = ""

	Select rdata
	Delete For edistatus ="NO DATA IN SCAN"  && delete mthese most likely personnels and samples
	current_time = Datetime()
	Delete For ua_status = "NO" And current_time >=proctime+7200  && give 2 hours to pick up files
	Set Deleted On

	Goto Top
	Scan
		If ua_status = "NO" And !Empty(trigtime) And !Empty(proctime) And Empty(xfertime)
			lcIssueStr = lcIssueStr+"Check Out: "+Consignee+": "+rdata.bol+" "+edistatus+" Process Time: "+Ttoc(proctime)+Chr(13)
		Endif
	Endscan

	If Len(lcIssueStr) !=0
		tsendto = "pgaidis@fmiint.com,joe.bianchi@tollgroup.com,tmarg@fmiint.com"
* tsendto = "pgaidis@fmiint.com"
		tcc = ""
		tattach = ""
		tFrom ="Toll EDI System Operations <fmi-transload-ops@fmiint.com>"
		tmessage = "Marc Jacobs Slip/Voucher: "+Ttoc(Datetime())+Chr(13)+Chr(13)+lcIssueStr
		tSubject = "Marc Jacobs Slip/Voucher Alert"
		Do Form m:\dev\frm\dartmail2 With tsendto,tFrom,tSubject,tcc,tattach,tmessage,"A"
	Else
		Wait Window At 10,10 "No issues to report.............." && timeout 1
	Endif

****************************************************************************************************
Procedure check_wholesale

	Use h:\fox\mj_wholesale_deliveries Alias rdata

	lcIssueStr = ""
	Select rdata
	current_time = Datetime()
	Delete For ua_status = "NO" And current_time <=proctime+7200  && give 2 hours to pick up files
	Delete For edistatus = "SLIP-CREATED"
	Delete For trigtime <= {06/08/2012}
	Set Deleted On

	Goto Top
	Scan
		If ua_status = "NO" And !Empty(trigtime) And !Empty(proctime) And Empty(xfertime)
			lcIssueStr = lcIssueStr+"Check Out: "+Consignee+": "+rdata.bol+" "+edistatus+" Process Time: "+Ttoc(proctime)+Chr(13)
		Endif
	Endscan

	If Len(lcIssueStr) != 0
		tsendto = "pgaidis@fmiint.com,joe.bianchi@tollgroup.com,tmarg@fmiint.com"
*  tsendto = "pgaidis@fmiint.com"
		tcc = ""
		tattach = ""
		tFrom ="Toll EDI System Operations <fmi-transload-ops@fmiint.com>"
		tmessage = "Marc Jacobs 945 possible issues: "+Ttoc(Datetime())+Chr(13)+Chr(13)+lcIssueStr
		tSubject = "Marc Jacobs Wholsale 945 Alert"
		Do Form m:\dev\frm\dartmail2 With tsendto,tFrom,tSubject,tcc,tattach,tmessage,"A"
	Else
		Wait Window At 10,10 "No issues to report.............." && timeout 1
	Endif


****************************************************************************************************
