*!* m:\dev\prg\grandvoyage940_bkdn.prg
CLEAR
lCheckStyle = .T.
lPrepack = .F.  && Inventory to be kept as PnP
m.color=""
cUCCNumber = ""
cUsePack = ""
lcUCCSeed = 1

SELECT x856
COUNT FOR TRIM(segment) = "W05" TO nSegCnt
cSegCnt = ALLTRIM(STR(nSegCnt))
SELECT x856
LOCATE

goffice="L"
gmasteroffice="L"

ptctr = 0

WAIT WINDOW "Now in "+cMailName+" 940 Breakdown" TIMEOUT 1

WAIT "There are "+cSegCnt+" P/Ts in this file" WINDOW TIMEOUT 1
SELECT x856
LOCATE

*!* Constants
m.acct_name = "GRAND VOYAGE LLC"
STORE nAcctNum TO m.accountid
m.careof = " "
m.sf_addr1 = "8961 SUNSET BLVD. 2A"
m.sf_addr2 = ""
m.sf_csz = "WEST HOLLYWOOD, CA 90069"
m.printed = .F.
m.print_by = ""
m.printdate = DATE()
*!* End constants

useca("ackdata","wh")

*!*	IF !USED("isa_num")
*!*		IF lTesting
*!*			USE F:\3pl\test\isa_num IN 0 ALIAS isa_num
*!*		ELSE
*!*			USE F:\3pl\DATA\isa_num IN 0 ALIAS isa_num
*!*		ENDIF
*!*	ENDIF

*!*	IF !USED("uploaddet")
*!*		IF lTesting
*!*			USE F:\ediwhse\test\uploaddet IN 0 ALIAS uploaddet
*!*		ELSE
*!*			USE F:\ediwhse\DATA\uploaddet IN 0 SHARED ALIAS uploaddet
*!*		ENDIF
*!*	ENDIF

SELECT xpt
DELETE ALL
SELECT xptdet
DELETE ALL

STORE 0 TO pt_total_eaches
STORE 0 TO pt_total_cartons
STORE 0 TO nCtnCount

SELECT x856
SET FILTER TO
LOCATE

STORE "" TO cisa_num

WAIT WINDOW "NOW IN INITAL HEADER BREAKDOWN" TIMEOUT 1
WAIT WINDOW "NOW IN INITAL HEADER BREAKDOWN" NOWAIT
*ASSERT .F. MESSAGE "At header bkdn...debug"
DO WHILE !EOF("x856")
	cSegment = TRIM(x856.segment)
	cCode = TRIM(x856.f1)

	IF EMPTY(cSegment)
		IF lTesting
			SELECT x856
			SKIP
			LOOP
		ELSE
			cErrMsg = "Incorrect file format: Blank segment"
			THROW
		ENDIF
	ENDIF

	IF TRIM(x856.segment) = "ISA"
		cisa_num = ALLTRIM(x856.f13)
		WAIT WINDOW "This is a "+cMailName+" PT upload" TIMEOUT 2
		ptctr = 0
		SELECT x856
		SKIP
		LOOP
	ENDIF

	IF TRIM(x856.segment) = "GS"
		cgs_num = ALLTRIM(x856.f6)
		lcCurrentGroupNum = ALLTRIM(x856.f6)
*!*			SELECT isa_num
*!*			LOCATE FOR isa_num.isa_num = cisa_num ;
*!*				AND isa_num.acct_num = nAcctNum
*!*			IF !FOUND()
*!*				APPEND BLANK
*!*				REPLACE isa_num.isa_num WITH cisa_num IN isa_num
*!*				REPLACE isa_num.acct_num WITH nAcctNum IN isa_num
*!*				REPLACE isa_num.office WITH cOffice IN isa_num
*!*				REPLACE isa_num.TYPE WITH "940" IN isa_num
*!*				REPLACE isa_num.ack WITH .F. IN isa_num
*!*				REPLACE isa_num.dateloaded WITH DATETIME() IN isa_num
*!*				REPLACE isa_num.filename WITH UPPER(TRIM(xfile)) IN isa_num
*!*			ENDIF
		SELECT x856
		SKIP
		LOOP
	ENDIF

	IF TRIM(x856.segment) = "ST" AND TRIM(x856.f1) = "940"
		IF !lTesting
			m.groupnum=lcCurrentGroupNum
			m.isanum=cisa_num
			m.transnum=x856.f2
			m.edicode="OW"
			m.accountid=6617
			m.loaddt=date()
			m.loadtime=datetime()
			m.filename=xfile
			insertinto("ackdata","wh",.t.)
			tu("ackdata")

			SELECT x856
			SKIP
			LOOP
		ENDIF
	ENDIF

	IF TRIM(x856.segment) = "W05"  && new shipment
		nCtnCount = 0
		m.style = ""
		SELECT xpt
		nDetCnt = 0
		APPEND BLANK

		REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"FILENAME*"+cFilename IN xpt
		REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"CONAME*GRAND VOYAGE"
		REPLACE xpt.qty WITH 0 IN xpt
		REPLACE xpt.origqty WITH 0 IN xpt
		STORE 0 TO pt_total_eaches
		STORE 0 TO pt_total_cartons
		ptctr = ptctr +1
		WAIT WINDOW AT 10,10 "Now processing PT# "+ ALLTRIM(x856.f2)+" Number "+CHR(13)+TRANSFORM(ptctr)+" of "+cSegCnt NOWAIT

		REPLACE xpt.ptid       WITH ptctr IN xpt
		REPLACE xpt.accountid  WITH nAcctNum IN xpt
*		REPLACE xpt.office     WITH cOffice && "N"
		REPLACE xpt.ptdate     WITH DATE() IN xpt
		m.ship_ref = ALLTRIM(x856.f2)
		cship_ref = ALLTRIM(x856.f2)
		REPLACE xpt.ship_ref   WITH m.ship_ref IN xpt  && Pick Ticket
		m.pt = ALLTRIM(x856.f2)
		REPLACE xpt.cnee_ref   WITH ALLTRIM(x856.f3)  && cust PO
		SELECT x856
		SKIP
		LOOP
	ENDIF

	IF TRIM(x856.segment) = "N1" AND TRIM(x856.f1) = "ST" && Ship-to data
*!*			SELECT uploaddet
*!*			LOCATE
*!*			DO WHILE !FLOCK('uploaddet')
*!*				WAIT WINDOW "" NOWAIT
*!*			ENDDO
*!*			IF !EMPTY(cisa_num)
*!*				APPEND BLANK
*!*				REPLACE uploaddet.office    WITH cOffice
*!*				REPLACE uploaddet.cust_name WITH UPPER(TRIM(x856.f2))
*!*				REPLACE uploaddet.acct_num  WITH nAcctNum
*!*				REPLACE uploaddet.DATE      WITH DATETIME()
*!*				REPLACE uploaddet.isa_num   WITH cisa_num
*!*				REPLACE uploaddet.startpt   WITH xpt.ship_ref
*!*				REPLACE uploaddet.runid     WITH lnRunID
*!*			ENDIF
*!*			UNLOCK IN uploaddet

		cConsignee = UPPER(ALLTRIM(x856.f2))
		SELECT xpt
		m.st_name = cConsignee
		REPLACE xpt.consignee WITH  cConsignee IN xpt

		cStoreNum = ALLTRIM(x856.f4)
		IF EMPTY(xpt.shipins)
			REPLACE xpt.shipins WITH "STORENUM*"+cStoreNum IN xpt
		ELSE
			REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"STORENUM*"+cStoreNum IN xpt
		ENDIF
		IF EMPTY(xpt.dcnum)
			REPLACE xpt.dcnum WITH cStoreNum IN xpt
		ENDIF
		IF LEN(cStoreNum)>5
			REPLACE xpt.storenum  WITH VAL(RIGHT(cStoreNum,5)) IN xpt
		ELSE
			REPLACE xpt.storenum  WITH VAL(cStoreNum) IN xpt
		ENDIF

		SKIP 1 IN x856
		DO CASE
			CASE TRIM(x856.segment) = "N2"  && name ext. info
				m.name = UPPER(TRIM(x856.f1))
				REPLACE xpt.NAME WITH m.name
				SKIP 1 IN x856
				IF TRIM(x856.segment) = "N3"  && address info
					m.st_addr1 = UPPER(TRIM(x856.f1))
					m.st_addr2 = UPPER(TRIM(x856.f2))
					REPLACE xpt.address  WITH m.st_addr1
					REPLACE xpt.address2 WITH m.st_addr2
				ENDIF
				SKIP 1 IN x856
				IF TRIM(x856.segment) = "N4"  && address info
					m.zipbar = TRIM(x856.f3)
					m.st_csz = UPPER(TRIM(x856.f1))+", "+UPPER(TRIM(x856.f2))+" "+TRIM(x856.f3)
					REPLACE xpt.csz WITH m.st_csz IN xpt
					REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"COUNTRY*"+ALLTRIM(x856.f4) IN xpt
				ENDIF
			CASE TRIM(x856.segment) = "N3"  && address info
				m.st_addr1 = UPPER(TRIM(x856.f1))
				m.st_addr2 = UPPER(TRIM(x856.f2))
				REPLACE xpt.address  WITH m.st_addr1
				REPLACE xpt.address2 WITH m.st_addr2
				SKIP 1 IN x856
				IF TRIM(x856.segment) = "N4"  && address info
					m.zipbar = TRIM(x856.f3)
					m.st_csz = UPPER(TRIM(x856.f1))+", "+UPPER(TRIM(x856.f2))+" "+TRIM(x856.f3)
					REPLACE xpt.csz WITH m.st_csz IN xpt
					REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"COUNTRY*"+ALLTRIM(x856.f4) IN xpt
				ENDIF
			OTHERWISE
				WAIT WINDOW "UNKNOWN ADDRESS SEGMENT" TIMEOUT 2
				THROW
		ENDCASE
		SELECT x856
		SKIP
		LOOP
	ENDIF

	IF TRIM(x856.segment) = "N1" AND INLIST(TRIM(x856.f1),"SD","MA","PF") && Returned data
		cNom = ALLTRIM(x856.f1)
		IF lTesting AND cNom = "SD"
*SET STEP ON
		ENDIF
		REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+cNom+"_N1*"+ALLTRIM(x856.f2) IN xpt
		SKIP 1 IN x856

		DO CASE
			CASE TRIM(x856.segment) = "N2"
				REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+cNom+"_N2*"+ALLTRIM(x856.f1) IN xpt
				SKIP 1 IN x856
				IF TRIM(x856.segment) = "N3"
					REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+cNom+"_N3*"+ALLTRIM(x856.f1)+"|"+ALLTRIM(x856.f2) IN xpt
					SKIP 1 IN x856
				ENDIF
				IF TRIM(x856.segment) = "N4"
					REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+cNom+"_N4*"+ALLTRIM(x856.f1)+"|"+ALLTRIM(x856.f2)+"|"+ALLTRIM(x856.f3) IN xpt
				ENDIF

			CASE TRIM(x856.segment) = "N3"
				REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+cNom+"_N3*"+ALLTRIM(x856.f1)+"|"+ALLTRIM(x856.f2) IN xpt
				SKIP 1 IN x856
				IF TRIM(x856.segment) = "N4"
					REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+cNom+"_N4*"+ALLTRIM(x856.f1)+"|"+ALLTRIM(x856.f2)+"|"+ALLTRIM(x856.f3) IN xpt
				ENDIF

			CASE TRIM(x856.segment) = "N4"
				REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+cNom+"_N4*"+ALLTRIM(x856.f1)+"|"+ALLTRIM(x856.f2)+"|"+ALLTRIM(x856.f3) IN xpt
		ENDCASE

		SELECT x856
		SKIP
		LOOP
	ENDIF

	IF TRIM(x856.segment) = "PER"  && Personal contact
		IF INLIST(x856.f1,"AJ","RE")
			REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"CONTACT*"+UPPER(ALLTRIM(x856.f2)) IN xpt
		ENDIF
		IF x856.f3 = "TE"
			REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"PHONE*"+UPPER(ALLTRIM(x856.f4)) IN xpt
		ENDIF
		IF INLIST(x856.f5,"EA","EM")
			IF x856.f5 = "EA"
				REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"EDI_CONTACT*"+UPPER(ALLTRIM(x856.f6)) IN xpt
			ELSE
				REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"EMAIL*"+UPPER(ALLTRIM(x856.f6)) IN xpt
			ENDIF
		ENDIF
		SELECT x856
		SKIP
		LOOP
	ENDIF

	IF TRIM(x856.segment) = "N9"  && Additional info segments
		DO CASE
			CASE x856.f1 = "CO"
				REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"COMPANY*"+ALLTRIM(x856.f2) IN xpt
			CASE x856.f1 = "98"
				lPrepack = IIF(x856.f2 = "PIK",.F.,.T.)
				REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"ORDERTYPE*"+ALLTRIM(x856.f2) IN xpt
*				REPLACE xpt.batch_num WITH ALLTRIM(x856.f2) IN xpt
			CASE INLIST(x856.f1,"EA","EM")
				IF x856.f1 = "EA"
					REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"EDI_CONTACT*"+UPPER(ALLTRIM(x856.f2)) IN xpt
				ELSE
					REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"EMAIL*"+UPPER(ALLTRIM(x856.f2)) IN xpt
				ENDIF
		ENDCASE
	ENDIF

	IF TRIM(x856.segment) = "G62" AND TRIM(x856.f1) == "37"  && start date
		lcDate = ALLTRIM(x856.f2)
		lxDate = SUBSTR(lcDate,5,2)+"/"+SUBSTR(lcDate,7,2)+"/"+LEFT(lcDate,4)
		ldDate = CTOD(lxDate)
		STORE ldDate TO dStart
		REPLACE xpt.START WITH ldDate IN xpt
		SELECT x856
		SKIP
		LOOP
	ENDIF

	IF TRIM(x856.segment) = "G62" AND TRIM(x856.f1) == "38"  && cancel date
		lcDate = ALLTRIM(x856.f2)
		lxDate = SUBSTR(lcDate,5,2)+"/"+SUBSTR(lcDate,7,2)+"/"+LEFT(lcDate,4)
		ldDate = CTOD(lxDate)
		STORE ldDate TO dCancel
		REPLACE xpt.CANCEL WITH ldDate IN xpt
		SELECT x856
		SKIP
		LOOP
	ENDIF

	IF TRIM(x856.segment) = "LX" && start of PT detail, stay here until this PT is complete
		ASSERT .F. MESSAGE cship_ref+" Detail, Type = "+IIF(lPrepack,"Prepack","Pickpack")+"...debug"
		m.units = IIF(lPrepack,.F.,.T.)
		WAIT "NOW IN DETAIL LOOP PROCESSING" WINDOW NOWAIT

		SELECT xptdet
		CALCULATE MAX(ptdetid) TO m.ptdetid
		m.ptdetid = m.ptdetid + 1

		SELECT xptdet
		SKIP 1 IN x856
		lnLinenum=1
		DO WHILE ALLTRIM(x856.segment) != "SE"
			cSegment = TRIM(x856.segment)
			cCode = TRIM(x856.f1)
			IF TRIM(x856.segment) = "W01" && Destination Quantity
				SELECT xptdet
				APPEND BLANK
				REPLACE xptdet.ptid WITH xpt.ptid IN xptdet
				REPLACE xptdet.ptdetid WITH m.ptdetid IN xptdet
				REPLACE xptdet.units WITH m.units IN xptdet
				REPLACE xptdet.linenum WITH ALLTRIM(STR(lnLinenum)) IN xptdet
				lnLinenum = lnLinenum + 1
				m.ptdetid = m.ptdetid + 1
				REPLACE xptdet.accountid WITH xpt.accountid IN xptdet
				cUsePack = "1"
				REPLACE xptdet.PACK WITH cUsePack IN xptdet
				m.casepack = INT(VAL(cUsePack))
				REPLACE xptdet.casepack WITH m.casepack IN xptdet

				IF !lPrepack  && Pickpack
					REPLACE xptdet.totqty WITH INT(VAL(x856.f1)) IN xptdet
				ENDIF
				REPLACE xpt.qty WITH xpt.qty + xptdet.totqty IN xpt
				REPLACE xpt.origqty WITH xpt.qty IN xpt
				REPLACE xptdet.origqty WITH xptdet.totqty IN xptdet

				IF EMPTY(TRIM(xptdet.printstuff))
					REPLACE xptdet.printstuff WITH "UOM*"+ALLTRIM(x856.f2) IN xptdet
				ELSE
					REPLACE xptdet.printstuff WITH xptdet.printstuff+CHR(13)+"UOM*"+ALLTRIM(x856.f2) IN xptdet
				ENDIF

				IF ALLTRIM(x856.f4) = "IN"
					cStyleBlock = UPPER(ALLTRIM(x856.f5))  && Added this per Paul, 11.11.2015, Joe
					IF OCCURS("-",cStyleBlock)>0
						nDash1 = AT("-",cStyleBlock,1)
						nDash2 = AT("-",cStyleBlock,2)
						m.style = LEFT(cStyleBlock,(nDash1-1))
						m.color = SUBSTR(cStyleBlock,(nDash1+1),(nDash2-1)-nDash1)
						m.id = ALLTRIM(SUBSTR(cStyleBlock,(nDash2+1)))
						REPLACE xptdet.STYLE WITH m.style,xptdet.COLOR WITH m.color,xptdet.ID WITH m.id IN xptdet
					ELSE
						REPLACE xptdet.STYLE WITH cStyleBlock IN xptdet
					ENDIF
				ENDIF

				IF ALLTRIM(x856.f6) = "UP" AND ALLTRIM(x856.f6) # "*"
					REPLACE xptdet.upc WITH UPPER(ALLTRIM(x856.f7)) IN xptdet
				ENDIF

				IF ALLTRIM(x856.f8) = "VN"
					REPLACE xptdet.shipstyle WITH ALLTRIM(x856.f9) IN xptdet
					REPLACE xptdet.custsku WITH ALLTRIM(x856.f9) IN xptdet
				ENDIF
			ENDIF

			IF TRIM(x856.segment) = "N9" AND TRIM(x856.f1) = "VCL"  && customer order #
				cColor = UPPER(ALLTRIM(x856.f2))
				REPLACE xptdet.COLOR WITH cColor IN xpt
				REPLACE xptdet.printstuff WITH xptdet.printstuff+CHR(13)+"ORIGCOLOR*"+cColor IN xptdet
				cColor = ""
			ENDIF

			IF TRIM(x856.segment) = "G69" && desc
				cPrtstr = "DESC*"+UPPER(TRIM(x856.f1))
				REPLACE xptdet.printstuff WITH xptdet.printstuff+CHR(13)+cPrtstr IN xptdet
			ENDIF

			SELECT x856
			IF !EOF()
				SKIP 1 IN x856
			ELSE
				EXIT
			ENDIF
		ENDDO

		SELECT xptdet
	ENDIF

	SELECT x856
	IF !EOF()
		SKIP 1 IN x856
	ENDIF
ENDDO

SELECT xptdet

rollup()
WAIT WINDOW "At end of ROLLUP procedure" NOWAIT
*lbrowfiles = .t.

IF lBrowfiles
	WAIT WINDOW "If tables are incorrect, CANCEL when XPTDET window opens"+CHR(13)+"Otherwise, will load PT table" TIMEOUT 2
	SELECT xpt
	COPY TO h:\xpt TYPE XL5
	LOCATE
	BROWSE
	SELECT xptdet
	COPY TO h:\xptdet TYPE XL5
	LOCATE
	BROWSE
*	cancel
ENDIF

WAIT WINDOW cCustname+" Breakdown Round complete..." TIMEOUT 2

SELECT x856
IF USED("PT")
	USE IN pt
ENDIF
IF USED("PTDET")
	USE IN ptdet
ENDIF
WAIT CLEAR
RETURN

*************************************************************************************************
*!* Error Mail procedure - Cancel Before Start Date
*************************************************************************************************
PROCEDURE errordates

	IF lEmail
		tsendto = tsendtotest
		tcc =  tcctest
		cMailLoc = IIF(cOffice = "C","West",IIF(cOffice = "N","New Jersey","Florida"))
		tsubject= "TGF "+cMailLoc+" "+cCustname+" Pickticket Error: " +TTOC(DATETIME())
		tmessage = "File: "+cFilename+CHR(13)+" has Cancel date(s) which precede Start date(s)"
		tmessage = tmessage + "File needs to be checked and possibly resubmitted."
		DO FORM m:\dev\frm\dartmail2 WITH tsendto,tFrom,tsubject,tcc,tattach,tmessage,"A"
	ENDIF
ENDPROC

******************************
PROCEDURE rollup
******************************
	ASSERT .F. MESSAGE "In rollup procedure"
	nChangedRecs = 0
	SELECT xptdet
	SET DELETED ON
	LOCATE
	IF lTesting
*BROWSE
	ENDIF
	cMsgStr = "PT/OSID/ODIDs changed: "
	SCAN FOR !DELETED()
		SCATTER MEMVAR
		nRecno = RECNO()
		LOCATE FOR xptdet.ptid = m.ptid ;
			AND xptdet.STYLE = m.style ;
			AND xptdet.COLOR = m.color ;
			AND xptdet.ID = m.id ;
			AND xptdet.PACK = m.pack ;
			AND xptdet.upc = m.upc ;
			AND RECNO('xptdet') > nRecno
		IF FOUND()
			SELECT xpt
			LOCATE FOR xpt.ptid = m.ptid
			IF !FOUND()
				WAIT WINDOW "PROBLEM!"
			ENDIF
			cship_ref = ALLT(xpt.ship_ref)
			IF nChangedRecs > 10
				cMsgStr = "More than 10 records rolled up"
			ELSE
				cMsgStr = cMsgStr+CHR(13)+cship_ref+" "+TRANSFORM(m.ptid)+" "+TRANSFORM(m.ptdetid)
			ENDIF
			nChangedRecs = nChangedRecs+1
			SELECT xptdet
			nTotqty1 = xptdet.totqty
			DELETE NEXT 1 IN xptdet
		ENDIF
		IF !EOF()
			GO nRecno
			REPLACE xptdet.totqty WITH xptdet.totqty+nTotqty1 IN xptdet
			REPLACE xptdet.origqty WITH xptdet.totqty IN xptdet
		ELSE
			GO nRecno
		ENDIF
	ENDSCAN

	IF nChangedRecs > 0
		WAIT WINDOW cMsgStr TIMEOUT 2
		WAIT WINDOW "There were "+TRANS(nChangedRecs)+" records rolled up" TIMEOUT 1
	ELSE
		WAIT WINDOW "There were NO records rolled up" TIMEOUT 1
	ENDIF

ENDPROC
