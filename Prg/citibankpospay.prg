LPARAMETER modetype,company,tcNoExtras

* build exe in f:\boa\

#INCLUDE wconnect.h

LOCAL lcProcessName, lnError

DECLARE INTEGER CreateMutex IN Win32API ;
	STRING @lpMutexAttributes, ;
	STRING bInitialOwner, ;
	STRING @lpName

DECLARE INTEGER GetLastError IN Win32API


IF EMPTY( tcNoExtras ) THEN

	* if running in EXE check to see if already running
	IF _VFP.STARTMODE > 0 THEN
		lcProcessName = "CITIBANKPOSPAY"
		CreateMutex(0,CHR(1),@lcProcessName)
		lnError = GetLastError()
		IF lnError = 183
			WAIT WINDOW TIMEOUT 5 "This program is already running..."
			utilsetup("CITIBANKPOSPAY")
			schedupdate()
			RETURN .F.
		ENDIF
	ENDIF

	utilsetup("CITIBANKPOSPAY")

ENDIF  &&  EMPTY( tcNoExtras )


IF INLIST(TYPE("modetype"),"U","L")
	runmode = "MANUAL"
ENDIF

IF INLIST(TYPE("modetype"),"C")
	IF modetype = "AUTO"
		runmode = "AUTO"
	ELSE
		runmode = "MANUAL"
	ENDIF
ENDIF

* temporary disable 2/1/12 MB
SET CENTURY ON
DO FORM m:\dev\frm\CitiBankPosPay WITH runmode,company

*WAIT WINDOW TIMEOUT 60

IF EMPTY( tcNoExtras ) THEN
	schedupdate()
ENDIF

RETURN

********************************************************************

*!*	PROCEDURE BOAQUERYFMI
*!*	SET EXACT ON
*!*	** CreateConnection()
*!*	LOCAL lcCompany, lcDsnName, ldStartdate, ldEnddate
*!*	LOCAL lcSql, A, B, C, D, lcStartdate, lcEnddate, lnResult, lnConhandle

*!*	lcCompany = "FMI"

*!*	ldStartdate = {^2009-03-23}
*!*	ldEnddate = {^2009-03-25}

*!*	DO CASE
*!*		CASE lcCompany = "FMI"
*!*			lcDsnName = "PLATINUM-408FMI"
*!*		CASE lcCompany = "SMGH"
*!*			lcDsnName = "PLATINUM-408SMG"
*!*		CASE lcCompany = "SGL"
*!*			lcDsnName = "PLATINUMSGL"
*!*	ENDCASE

*!*	lnConhandle = SQLSTRINGCONNECT("DSN="+ALLTRIM(lcDsnName),.T.)
*!*	IF lnConhandle > -1 THEN

*!*		** GetCheckReg()
*!*		*!* we need to convert the date into YYYY-MM-DD so Pervasive can use it
*!*		lcStartdate=['] + GetPlatinumDate(ldStartdate) + [']
*!*		lcEnddate=['] + GetPlatinumDate(ldEnddate) + [']

*!*		*!**********************************************************************************************
*!*		*!* Aptran.DocumentNumber<>'0000' means when they have an item and they do not want it sent
*!*		*!* to BOA they use four zero's
*!*		*!**********************************************************************************************
*!*		A="SELECT Aptran.DocumentNumber,SUM(Aptran.DocumentAmt) AS amt,Aptran.DocumentDate, Aptran.CashAcctKey,Aptran.TransactionType,"

*!*		B="apvend.vendorname,Aptran.vendorkey from Aptran,apvend"

*!*		C= " WHERE Aptran.vendorkey=apvend.vendorkey and (Aptran.TransactionType='P'or Aptran.TransactionType='V') and Aptran.DocumentNumber<>'0000' and Aptran.RecDate between &lcStartdate and &lcEnddate " && need payments and voids only

*!*		DO CASE
*!*			CASE lcCompany = "FMI"
*!*				D = " and Aptran.cashacctkey='1032000000' GROUP BY Aptran.DocumentNumber, Aptran.CashAcctKey,Aptran.DocumentDate,Aptran.transactiontype,apvend.vendorname,Aptran.vendorkey"
*!*			CASE lcCompany = "SMGH"
*!*				D = " and Aptran.cashacctkey='1032000090' GROUP BY Aptran.DocumentNumber, Aptran.CashAcctKey,Aptran.DocumentDate,Aptran.transactiontype,apvend.vendorname,Aptran.vendorkey"
*!*			CASE lcCompany = "SGL"
*!*				D = " and Aptran.cashacctkey='1030000090' GROUP BY Aptran.DocumentNumber, Aptran.CashAcctKey,Aptran.DocumentDate,Aptran.transactiontype,apvend.vendorname,Aptran.vendorkey"
*!*		ENDCASE


*!*		lcSql = A + B + C + D

*!*		lnResult=SQLEXEC(lnConhandle,lcSql,'checkreg')
*!*		BROWSE
*!*	ELSE
*!*		WAIT WINDOW "Connection failed"
*!*	ENDIF
*!*	CLOSE DATABASES ALL

*!*	RETURN

*!*	*!*	  Browse
*!*	FUNCTION GetPlatinumDate
*!*		LPARAMETERS pDate
*!*		LOCAL lcYear, lcDay, lcRetval, lcMonth
*!*		** get year
*!*		lcYear = ALLTRIM(STR(YEAR(pDate)))

*!*		** get month
*!*		lcMonth = IIF(LEN(ALLTRIM(STR(MONTH(pDate))))=1,"0"+ALLTRIM(STR(MONTH(pDate))),ALLTRIM(STR(MONTH(pDate))))

*!*		** get day
*!*		lcDay = IIF(LEN(ALLTRIM(STR(DAY(pDate))))=1,"0"+ALLTRIM(STR(DAY(pDate))),ALLTRIM(STR(DAY(pDate))))

*!*		lcRetval = lcYear + "-" + lcMonth + "-" + lcDay

*!*		RETURN lcRetval
*!*	ENDFUNC


*!*	ENDPROC
