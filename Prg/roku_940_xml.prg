*Set Step On 
close data all 
Set Date YMD

Public nAcctNum,cOffice,lLoadSQL,lTesting,lTestImport,cOffice,gMasterOffice,cMod,gldor,cUseFolder

nAcctnum = 6674
cOffice = "N"
gmasterOffice = "N"
cMod="I"
lLoadSQL= .t.
lTestingt = .f.
lTestImport =.f.
gldor=.t.

cUseFolder ="f:\whi\whdata\"

Use f:\whi\whdata\outship In 0

Create Cursor temp1 (rec i, f1 char(250))
lcArchivepath= "f:\ftpusers\roku-nj\940-orders\archive\"
lcPath= "f:\ftpusers\roku-nj\940-orders\"

lnNum = ADIR(tarray,"f:\ftpusers\roku-nj\940-orders\*.*")

*Messagebox(Str(lnnum))

IF lnNum = 0
  WAIT WINDOW AT 10,10 "      No ROKU 940s to import     " TIMEOUT 3
  NormalExit = .T.
  THROW
ENDIF

useca("pt","wh")
useca("ptdet","wh")

Select * From pt Into Cursor xpt readwrite
Select * From ptdet Into Cursor xptdet readwrite

Use In pt
Use In ptdet

xxptid = 1
xxptdetid = 1
m.ptdate = Date()
m.office = "N"
m.mod="I"
m.shipins=""
m.notes=""

FOR thisfile = 1  TO lnNum
  Xfile = lcPath+tarray[thisfile,1] && +"."
  cfilename = ALLTRIM(LOWER(tarray[thisfile,1]))
  nFileSize = VAL(TRANSFORM(tarray(thisfile,2)))
  ArchiveFile = (lcArchivepath+cfilename)
  m.shipins=""
  m.notes=""

*fhand = Fopen("f:\ftplogs\logs\lexicom.xml")
*xfile = getfile()
*fhand = Fopen("f:\ftpusers\roku\orders\bestbuyorder2.xml")

fhand = Fopen(xfile)

ictr = 1
Select temp1
Zap

Do while !feof(fhand)
  lcStr = Alltrim(Fgets(fhand,500))
  Select temp1
  Append Blank
  replace f1 with lcStr,rec With ictr 
  ictr = ictr+1
  If ictr = 300000
    exit
  endif  
  Wait window at 10,10 "record# "+Transform(ictr) nowait 
EndDo 

Fclose(fhand)


Select temp1
Go top

Do While !Eof("temp1")
 Set Date YMD

Tag = "<ReferenceNum>"
GotoTag(Tag)
m.Ship_ref = getdata(f1,Tag)
m.accountid = 6674

Tag = "<PONum>"
GotoTag(Tag)
m.cnee_ref = Upper(getdata(f1,Tag))

Tag = "<ShipCancelDate"
GotoTag(Tag)
m.cancel = Ctod(Strtran(Upper(Upper(getdata(f1,Tag))),">",""))

Tag = "<EarliestShipDate"
GotoTag(Tag)
m.start = Ctod(Strtran(Upper(Upper(getdata(f1,Tag))),">",""))

Tag = "<CompanyName>"
GotoTag(Tag)
m.consignee = Upper(getdata(f1,Tag))
m.name = m.consignee

Tag = "<Address1>"
GotoTag(Tag)
m.address = Upper(getdata(f1,Tag))

Tag = "<City>"
GotoTag(Tag)
m.city = Upper(getdata(f1,Tag))

Tag = "<State>"
GotoTag(Tag)
m.state = Upper(getdata(f1,Tag))

Tag = "<Zip>"
GotoTag(Tag)
m.zip = getdata(f1,Tag)
m.csz = Alltrim(m.city)+", "+Alltrim(m.state)+" "+Alltrim(m.zip)

Tag = "<Country>"
GotoTag(Tag)
m.country = Upper(getdata(f1,Tag))

Tag = "<Carrier"
GotoTag(Tag)
m.ship_via = Strtran(Upper(Upper(getdata(f1,Tag))),">","")

Tag = "<Mode"
GotoTag(Tag)
*m.mode = Upper(getdata(f1,Tag))
m.mode =Strtran(Upper(Upper(getdata(f1,Tag))),">","")
m.mode = "MODE*"+m.mode
m.shipins=m.shipins+m.mode+Chr(13)

Tag = "<BillingCode>"
GotoTag(Tag)
m.code = Upper(getdata(f1,Tag))
m.terms = m.code
m.code = "BILLINGCODE*"+m.code
m.shipins=m.shipins+m.code+Chr(13)

Tag = "<ShippingNotes>"
GotoTag(Tag)
m.notes = "SHIPPINGNOTES*"+getdatanotes(f1,Tag)
m.shipins=m.shipins+m.notes+Chr(13)

*!*  *Select outship
*!*  =Seek("6674"+Padr(Alltrim(m.Ship_ref),20," "),"outship","acctpt")
*!*  If Found("outship")
*!*    replace outship.shipins With outship.shipins+Chr(13)+m.shipins In outship
*!*  Endif 

Tag = "<Code>LoadID"
GotoTag(Tag)
Skip 1 In temp1
Tag = "<Description>"
m.loadid = "LOADID*"+Upper(getdata(f1,Tag))
m.shipins=m.shipins+m.loadid+Chr(13)

Tag = "<Code>StoreNumber"
GotoTag(Tag)
Skip 1 In temp1
Tag = "<Description>"
m.store = Upper(getdata(f1,Tag))
m.storenum = Val(m.store)
m.dcnum = m.store
m.shipins=m.shipins+"STORE*"+m.store+Chr(13)

Tag = "<Code>DepartmentDescription"
GotoTag(Tag)
Skip 1 In temp1
Tag = "<Description>"
m.dept = Upper(getdata(f1,Tag))

Set Date MDY

Insert Into xpt From memvar
replace xpt.ptid With xxptid
xxptid = xxptid +1

Do While !eof("temp1")

Tag = "<SKU>"
GotoTag(Tag)
m.style = getdata(f1,Tag)

Tag = "<OrderQty>"
GotoTag(Tag)
m.totqty = Val(getdata(f1,Tag))
m.origqty = m.totqty

Tag = "<UPC>"
GotoTag(Tag)
m.upc = getdata(f1,Tag)

Tag = "<Code>CustItemRefNum"
GotoTag(Tag)
Skip 1 In temp1
Tag = "<Description>"
m.shipstyle = getdata(f1,Tag)
m.custsku = getdata(f1,Tag)

Tag = "<Code>NetsuiteOrderLineNumber"
GotoTag(Tag)
Skip 1 In temp1
Tag = "<Description>"
m.linenum = getdata(f1,Tag)

Tag = "</OrderItem>"
GotoTag(Tag)

Insert Into xptdet From memvar
replace xptdet.ptdetid With xxptdetid
replace xptdet.ptid With xpt.ptid
xxptdetid = xxptdetid +1

 Skip 1 In temp1
 If f1 = "<OrderItem>"

 Else
  exit
 endif    

Enddo
   Tag = "<WarehouseTransaction>"
   GotoTag(Tag,,.t.)
Enddo 

next 
Select xpt
Scan
  Select xptdet
  Sum totqty for xptdet.ptid = xpt.ptid To lnpttot
  replace xpt.qty With lnpttot In xpt
  replace xpt.origqty With lnpttot In xpt
  replace All units With .t.
  replace All pack With "1"
Endscan


Set Step On 

Do all940_import

Return

**************************************************************
Do while !Eof("temp1")
  
 Select temp1
 m.storeno     = getfield(f1,[store_no])
 m.storecode   = getfield(FIELD1,[store_code])
 m.gstorecode  = getfield(FIELD1,[glob_store_code])
 m.storename   = getfield(FIELD1,[store_name])
 m.address     = getfield(FIELD1,[address1])
 m.csz         = getfield(FIELD1,[address2])
 
 Select mjstores
 Append Blank
 Gather memvar

 Select temp1
 Skip 1 
 
 If temp1.field1= "</STORES>"
   exit
 EndIf 
   
EndDo

**************************************************************************
PROCEDURE getdata
  PARAMETERS sdata,match

  lnstart = AT(match,sdata)
  tdata = sdata
  tdata=Strtran(tdata,match) 
  lnend = At("</",tdata)
  RETURN SUBSTR(tdata,1,lnend-1)

ENDPROC
**************************************************************************
PROCEDURE getdatanotes
  PARAMETERS sdata,match
  tdata =""
  xxdata =""
  lnstart = AT(match,sdata)
  tdata = sdata
  tdata=Strtran(tdata,match) 
  lnend = At("</",tdata)

  Do While lnEnd =0
     xxdata=xxdata+Chr(13)+Alltrim(tdata)
     Select temp1
     Skip 1 
     tdata = temp1.f1 
    lnend = At("</",tdata)
   Enddo 
   xxdata = xxdata+Chr(13)+Substr(tdata,1,lnEnd)
RETURN xxdata

ENDPROC
***************************************************************************
PROCEDURE getfield
  PARAMETERS sdata,match

  lnstart = AT(match,sdata)
  If "="$match
    lnStart = lnStart-1
  EndIf 
  firstquote = lnstart+LEN(match)+1
  cc = SUBSTR(sdata,firstquote,1)
  lnsecond = firstquote+1
  zzCtr = 0
  DO WHILE SUBSTR(sdata,lnsecond,1)!=["]
     zzCtr = zzCtr+1
     If zzCtr > 200
       return "ERROR"
     EndIf 
    lnsecond = lnsecond+1
  ENDDO

  RETURN SUBSTR(sdata,firstquote+1,lnsecond-(firstquote+1))

ENDPROC
**************************************************************************
PROCEDURE GotoTag
  PARAMETERS tagname,stoptag,endofdocument
  xtagfilter=IIF(EMPTY(stoptag),"","and F1!= stoptag")
  DO WHILE F1!= tagname &xtagfilter
    If Eof("temp1")
     If !endofdocument 
       Wait Window At 10,10 " Cant find looking for TAG "+tagname
     endif
      Exit
    else  
      Skip 1 in temp1
    EndIf  
  ENDDO
ENDPROC
**************************************************************************



