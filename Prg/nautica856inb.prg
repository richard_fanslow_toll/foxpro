**********************************************************************************
* Nautica 856 bkdn, for the Dostro in Miami
***********************************************************************************
* The 856 is structured as
* Shipment
*   Order
*     Item
*        Pack
*
*  Started: 04/23/2012,  PG
*  Checked for incorrect uploads within a single 856, 12.29.2016 JOEB
***********************************************************************************
PUBLIC lProcessOK,lTesting,gContainer,cOffice,nAccountid,cTransfer,NormalExit,xfile,nInwologid,nPLID

*runack("NAUTICA856INB")

CLOSE DATABASES ALL

WAIT WINDOW AT 10,10  "Now setting up Nautica 856 upload............" TIMEOUT 1
SET tablevalidate TO 0
DO m:\dev\prg\_setvars WITH .T.
ON ESCAPE CANCEL

TRY
	lTesting = .F.
	lOverridebusy =  lTesting
	cOffice = "M"
	goffice = "M"
	gMasterOffice = cOffice
	nAccountid = 6356
	cTransfer = "856-NAUTICA-FL"
	cMailName = "Nautica-Kipling"
	NormalExit = .F.
	nPLID =0
	nInwologid =0
	lOverridebusy =  .t.

	IF !lTesting
		SELECT 0
		USE F:\edirouting\ftpsetup SHARED
		LOCATE FOR ftpsetup.transfer =  cTransfer
		IF FOUND()
			IF !lTesting AND !lOverridebusy
				IF !ftpsetup.chkbusy
					REPLACE chkbusy WITH .T. FOR ftpsetup.transfer = cTransfer
				ELSE
					WAIT WINDOW "Transfer locked...can't start new process" TIMEOUT 2
					NormalExit = .T.
					THROW
				ENDIF
			ELSE
				REPLACE chkbusy WITH .T.,trig_time WITH DATETIME() FOR ftpsetup.transfer = cTransfer
			ENDIF
			USE IN ftpsetup
		ENDIF
	ENDIF

	xReturn = "XX"
	IF !lTesting
		DO m:\dev\prg\wf_alt WITH "M",nAccountid
		cUseFolder = UPPER(xReturn)
	ELSE
		cUseFolder = "f:\whp\whdata\"
	ENDIF

*	USE (cUseFolder+"whgenpk") IN 0 ALIAS whgenpk

	useca("ctnucc","wh")
	xsqlexec("select * from ctnucc where .f.","xdetail",,"wh")
	xsqlexec("select * from ctnucc where .f.","detail",,"wh")
	*SELECT * FROM ctnucc WHERE .F. INTO CURSOR XDETAIL

	useca("inwolog","wh")
	xsqlexec("select * from inwolog where .f.","xinwolog",,"wh")

	IF lTesting
		USE (cUseFolder+"PL") IN 9 ALIAS pl
		SELECT * FROM pl WHERE .F. INTO CURSOR xpl READWRITE
	ELSE
		useca("pl","wh")
		xsqlexec("select * from pl where .f.","xpl",,"wh")
	ENDIF

* useca("ctnucc","wh")
* xsqlexec("select * from ctnucc where .f.","xdetail",,"wh")

	DO m:\dev\prg\createx856

	lcDELIMITER = "*"
	lcTranslateOption ="NAUTICA"

	delimchar = lcDELIMITER
	lcTranOpt= lcTranslateOption

	lcpath="f:\ftpusers\nautica\856in\"
	lcpath = IIF(lTesting,lcpath+"test\",lcpath)
	IF lTesting
		WAIT WINDOW "This is a TEST upload" TIMEOUT 2
	ENDIF
	lcarchivepath="f:\ftpusers\nautica\856in\archive\"

	CD &lcpath

	lnNum = ADIR(tarray,"*.*")

	IF lnNum = 0
		WAIT WINDOW AT 10,10 "    No Nautica 856's to import.............." TIMEOUT 2
		NormalExit = .T.
		THROW
	ENDIF
	FOR thisfile = 1  TO lnNum
		Archivename = TTOC(DATETIME(),1)
		xfile = lcpath+tarray[thisfile,1]+"."
		archivefile = lcarchivepath+Archivename+tarray[thisfile,1]
		WAIT WINDOW "Importing file: "+xfile NOWAIT
		IF FILE(xfile)
* load the file into the 856 array
			DO m:\dev\prg\createx856
			DO m:\dev\prg\loadedifile WITH xfile,delimchar,lcTranOpt,"NAUTICA"
			lProcessOK = .F.
			DO m:\dev\prg\nautica856bkdnB WITH xfile
			
			SELECT xdetail
			LOCATE 

			SELECT shipid,CONTAINER,accountid FROM XDETAIL INTO CURSOR woctrs GROUP BY shipid,CONTAINER,accountid
	
			SELECT woctrs
			LOCATE
			SCAN
				DO m:\dev\prg\nauticacreatewo WITH woctrs.CONTAINER,woctrs.accountid,woctrs.shipid

*!*					IF !usesqlctn()		removed dy 1/12/18
*!*						SELECT XDETAIL
*!*						SCAN FOR CONTAINER = woctrs.CONTAINER AND shipid = woctrs.shipid AND accountid = woctrs.accountid
*!*							SELECT XDETAIL
*!*							SCATTER MEMVAR
*!*							SELECT DETAIL
*!*							APPEND BLANK
*!*							m.ctnuccid = dygenpk("CTNUCC","whm")
*!*							m.inwonum  = inwolog.wo_num
*!*							GATHER MEMVAR
*!*						ENDSCAN
*!*					ENDIF
*      replace detail.inwonum with inwolog.wo_num  for container= whichcontainer and accountid = whichaccount
			ENDSCAN

			IF lProcessOK AND !lTesting
				COPY FILE &xfile TO &archivefile
				IF FILE(archivefile)
					DELETE FILE &xfile
				ENDIF
			ENDIF
		ENDIF
	NEXT thisfile

	IF !lTesting
		SELECT 0
		USE F:\edirouting\ftpsetup SHARED
		REPLACE chkbusy WITH .F. FOR ftpsetup.transfer = cTransfer
		USE IN ftpsetup
	ENDIF
	NormalExit = .T.
	WAIT WINDOW "Entire Nautica 856 Process complete...exiting" TIMEOUT 2

CATCH TO oErr
	IF NormalExit = .F.
		ASSERT .F. MESSAGE "In Error Catch"
		SET STEP ON
		tsendto = "pgaidis@fmiint.com,joe.bianchi@tollgroup.com"
		tcc = ""
		tsubject = cMailName+" 856 Upload Error ("+TRANSFORM(oErr.ERRORNO)+") at "+TTOC(DATETIME())
		tmessage = cMailName+" 856 Upload Error..... Please fix"
		lcSourceMachine = SYS(0)
		lcSourceProgram = SYS(16)

		tmessage =tmessage+ "Try/CATCH Exeception Message:"+CHR(13)+;
			[  Error: ] + STR(oErr.ERRORNO) +CHR(13)+;
			[  LineNo: ] + STR(oErr.LINENO) +CHR(13)+;
			[  Message: ] + oErr.MESSAGE +CHR(13)+;
			[  Procedure: ] + oErr.PROCEDURE +CHR(13)+;
			[  Details: ] + oErr.DETAILS +CHR(13)+;
			[  StackLevel: ] + STR(oErr.STACKLEVEL) +CHR(13)+;
			[  LineContents: ] + oErr.LINECONTENTS+CHR(13)+;
			[  UserValue: ] + oErr.USERVALUE+CHR(13)+;
			[  Computer:  ] +lcSourceMachine+CHR(13)+;
			[  940 file:  ] +xfile+CHR(13)+;
			[  Program:   ] +lcSourceProgram

		tattach  =  " "
		tfrom    ="TGF EDI Processing Center <edi-ops@fmiint.com>"
		DO FORM m:\dev\FRM\dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
	ELSE
		WAIT WINDOW AT 10,10 "Normal Exit " TIMEOUT 2
	ENDIF
FINALLY
	ON ERROR
	SET STATUS BAR ON
	CLOSE DATABASES ALL
ENDTRY

