if x3winmsg("Did you just create the Account Balances Spreadsheet?","Just Checkin'","YN") = "NO"
	return .f.
endif

select ;
	accountid, ;
	acctname, ;
	billname, ;
	invnum, ;
	invdt, ;
	sum(origamt) as origamt, ;
	sum(-credit) as credit, ;
	sum(-payment) as payment, ;
	sum(balance) as balance ;
	from f:\watusi\eoy ;
	where payment#0 ;
	group by invnum ;
	order by accountid ;
	into cursor xrpt
	
report form partpay to print preview
