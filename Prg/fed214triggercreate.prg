*!* Federated 214 Trigger Creation

PARAMETERS cConsignee,nWO_Num,nTripID,dUseDate,tUseTime,nLeg,EventCode,lPU
IF VARTYPE(nWO_Num)#"N"
	WAIT WINDOW "Bad Input WO...closing"
	RETURN
ENDIF
WAIT WINDOW "In FED214 Trigger Creation process..." TIMEOUT 2
lTesting = .f.

lAB = IIF(EventCode = "AB",.t.,.f.)
*lTesting = IIF(lAB,.t.,lTesting)

IF USED('edi_trigger')
	USE IN edi_trigger
ENDIF

IF lTesting OR lAB
	IF !FILE('h:\fox\edi_trigger.DBF') or !FILE('h:\fox\edi_trigger.fpt')
		USE F:\3pl\DATA\edi_trigger IN 0 SHARED
		SELECT * FROM edi_trigger WHERE .F. INTO DBF h:\fox\edi_trigger READWRITE
		tAlias = ALIAS()
		USE IN edi_trigger
		USE IN &tAlias
	ENDIF
	USE h:\fox\edi_trigger IN 0 ALIAS edi_trigger

*!*		nWO_Num = 734922
*!*		nTripID = 1026996
*!*		tUseTime = "0759A"
*!*		dUseDate = {^2010-03-22}
*!*		EventCode = "X3"
ELSE
	USE F:\3pl\DATA\edi_trigger IN 0 ALIAS edi_trigger SHARED
ENDIF

*ASSERT .F. MESSAGE "In triggercreate field calculations"

tUseTime = TRIM(tUseTime)
nTimeVal = INT(VAL(LEFT(tUseTime,4)))
nHrVal = INT(VAL(LEFT(tUseTime,2)))
tAMPM = RIGHT(tUseTime,1)
IF tAMPM = "P" AND nHrVal < 12
	nTimeVal = nTimeVal + 1200
ENDIF

STORE .F. TO m.processed,m.errorflag
m.consignee = cConsignee
m.style = EventCode
m.leg = nLeg
m.time = PADL(ALLTRIM(STR(nTimeVal)),4,"0")
m.trig_from = "FED214TRIG"
m.trig_time = DATETIME()
m.trig_by = "TGF-PROC"
m.edi_type = "214F"
m.ptflag = lPU
m.wo_num = nWO_Num
m.tripid = nTripID
m.date = dUseDate
m.office = "N"
m.accountid = 647

INSERT INTO edi_trigger FROM MEMVAR
SELECT edi_trigger

IF lTesting and m.style="CD"
	BROWSE FIELDS EDI_TYPE,PROCESSED,ERRORFLAG,OFFICE,CONSIGNEE,WO_NUM,DATE,TIME,TRIPID,LEG,STYLE,TRIG_TIME
ENDIF
RETURN
