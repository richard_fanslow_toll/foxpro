
* process vcard return files sent from CitiBank to us.
*
* Set it up to poll 1 or 2 times a day for return files.
*
* Build EXE as F:\UTIL\CITIVCAPROC\CITIBANKVCARETURNS.exe
*
** 4/24/2018 MB: changed mbennett@fmiint.com to my Toll email.

LOCAL loCITIBANKVCARETURNS, lnError, lcProcessName

runack("CITIBANKVCARETURNS")

utilsetup("CITIBANKVCARETURNS")

DECLARE INTEGER CreateMutex IN Win32API ;
	STRING @lpMutexAttributes, ;
	STRING bInitialOwner, ;
	STRING @lpName

DECLARE INTEGER GetLastError IN Win32API

* if running in EXE check to see if already running
IF _VFP.STARTMODE > 0 THEN
	lcProcessName = "CITIBANKVCARETURNS"
	CreateMutex(0,CHR(1),@lcProcessName)
	lnError = GetLastError()
	IF lnError = 183
		WAIT WINDOW TIMEOUT 5 "This program is already running..."
		schedupdate()
		RETURN .F.
	ENDIF
ENDIF

loCITIBANKVCARETURNS = CREATEOBJECT('CITIBANKVCARETURNS')
loCITIBANKVCARETURNS.MAIN()
*
schedupdate()

CLOSE DATABASES ALL

RETURN

#DEFINE LOGIT 1
#DEFINE WAITIT 2
#DEFINE NOWAITIT 4
#DEFINE SENDIT 8
#DEFINE LF CHR(10)
#DEFINE CRLF CHR(13) + CHR(10)
#DEFINE RETURN_DATA_MANDATORY .F.
#DEFINE RETURN_DATA_NOT_MANDATORY .T.
#DEFINE CSVFORMAT 6
#DEFINE WRITE_ONLY_UNBUFFERED 11
#DEFINE COMMA ","

DEFINE CLASS CITIBANKVCARETURNS AS CUSTOM

	cProcessName = 'CITIBANKVCARETURNS'

	lTestMode = .F.  && .F. FOR NORMAL OPERATION, .T. TO SUPPRESS ALL BUT DEVELOPER EMAILS AND CHANGE INPUT TABLE, ETC.

	lAutoYield = .T.

	* date time props
	dToday = DATE()

	cStartTime = TTOC(DATETIME())

	* folder properties
	cInputFolder = ''
	cArchivedFolder = ''
	cReportsFolder = 'F:\UTIL\CITIVCAPROC\REPORTS\'

	cProdInputFolder = 'F:\FTPUSERS\CITIBANKIN\VCARD\PROD\'
	cProdArchivedFolder = 'F:\FTPUSERS\CITIBANKIN\VCARDARCHIVE\'

	cTestInputFolder = 'F:\FTPUSERS\CITIBANKIN\VCARD\TEST\'
	cTestArchivedFolder = 'F:\UTIL\CITIVCAPROC\VCARDTESTARCHIVE\'

					
	* table properties
	cReconFileTable = ''

	cProdReconFileTable = 'F:\UTIL\CITIVCAPROC\DATA\VCARDRTNS'
	cTestReconFileTable = 'F:\UTIL\CITIVCAPROC\TESTDATA\TVCARDRTNS'
	
	cSentTable = ''
	cProdSentTable = 'F:\UTIL\CITIVCAPROC\DATA\VCARDSENT'
	cTestSentTable = 'F:\UTIL\CITIVCAPROC\TESTDATA\TVCARDSENT'

	* processing properties
	lArchiveFiles = .T.
	lConvertFiles = .T.
	cErrors = ''
	lFoundReconciliationFile = .F.
	lFoundResponseFile = .F.
	
	* file properties
	nNewFileHandle = -1
	nSourceFileHandle = -1
	
	*cashacctkey = '3250-0000-90'
	cashacctkey = '3250000090'
	
	nResponseErrors = 0
	
	* object properties
	oExcel = NULL
	oWorkbook = NULL
	oWorksheet = NULL

	* connection properties
	nSQLHandle = 0
	cDsn = "DSN=PLATINUM-408SMG"

	* wait window properties
	nWaitWindowTimeout = 2
	lWaitWindowIsOn = .T.

	* logfile properties
	lLoggingIsOn = .T.
	cLogFile = 'F:\UTIL\CITIVCAPROC\LOGFILES\CITIBANKVCARETURNS_log.txt'

	* INTERNAL email properties
	lSendInternalEmailIsOn = .T.
	cFrom = 'Mark.Bennett@Tollgroup.com'
	cSendTo = 'mark.bennett@tollgroup.com'
	cCC = ''
	cSubject = ''
	cAttach = ''
	cBodyText = ''

	cCOMPUTERNAME = ''
	cUSERNAME = ''


	FUNCTION INIT
		IF NOT DODEFAULT()
			RETURN .F.
		ENDIF
		WITH THIS
			*SET RESOURCE OFF
			CLOSE DATA
			SET CENTURY ON
			SET DATE AMERICAN
			SET HOURS TO 24
			SET ANSI ON
			SET TALK OFF
			SET DELETED ON
			SET CONSOLE OFF
			SET EXCLUSIVE OFF
			SET SAFETY OFF
			SET EXACT OFF
			SET STATUS BAR ON
			SET SYSMENU OFF
			SET ENGINEBEHAVIOR 70
			_VFP.AUTOYIELD = .lAutoYield
			.cCOMPUTERNAME = GETENV("COMPUTERNAME")
			.cUSERNAME = GETENV("USERNAME")
			IF .lTestMode THEN
				.cLogFile = 'F:\UTIL\CITIVCAPROC\LOGFILES\CITIBANKVCARETURNS_LOG_TESTMODE.txt'
				.cSendTo = 'mark.bennett@tollgroup.com'
				.cCC = ''
			ENDIF
			.lLoggingIsOn = .lLoggingIsOn AND NOT EMPTY(.cLogFile)
			IF .lLoggingIsOn THEN
				SET ALTERNATE TO (.cLogFile) ADDITIVE
				SET ALTERNATE ON
			ENDIF
		ENDWITH
	ENDFUNC


	FUNCTION DESTROY
		WITH THIS
			IF .lLoggingIsOn  THEN
				SET ALTERNATE OFF
				SET ALTERNATE TO
			ENDIF
		ENDWITH
		DODEFAULT()
	ENDFUNC


	FUNCTION MAIN
		WITH THIS
			LOCAL lnNumberOfErrors, loError, ldToday, ldYesterday, lcCompany, lcYesterday
			LOCAL lcTransText, lcInputFolder, lcArchivedFolder, lcReportsFolder, lnNumFiles, lcRootFileName
			LOCAL laFiles[1,5], laFilesSorted[1,6], i, lcFile, lcArchivedFile, llArchived
			LOCAL lcConvertedFolder, lcConvertedFile, lcStringOld, lcStringNew, lcFileToProcess
			LOCAL lcDateTimeStamp, ltDateTimeStamp, lcAttach, lcSendTo, lcCC, lcFrom, lcSubject, lcBodyText


			TRY

				lnNumberOfErrors = 0

				IF .lTestMode THEN
					.cInputFolder = .cTestInputFolder
					.cArchivedFolder = .cTestArchivedFolder
					.cReconFileTable = .cTestReconFileTable
					.cSentTable = .cTestSentTable
				ELSE
					.cInputFolder = .cProdInputFolder
					.cArchivedFolder = .cProdArchivedFolder
					.cReconFileTable = .cProdReconFileTable
					.cSentTable = .cProdSentTable
				ENDIF

				.TrackProgress('Citi Bank Vcard Returns process started....', LOGIT+SENDIT+NOWAITIT)
				.TrackProgress('COMPUTERNAME = ' + .cCOMPUTERNAME, LOGIT+SENDIT)
				.TrackProgress('USERNAME = ' + .cUSERNAME, LOGIT+SENDIT)
				.TrackProgress('PROJECT = CITIBANKVCARETURNS', LOGIT+SENDIT)
				IF .lTestMode THEN
					.TrackProgress('                        TEST MODE                        ', LOGIT+SENDIT)
				ENDIF
				.TrackProgress('.cInputFolder = ' + .cInputFolder, LOGIT+SENDIT)
				.TrackProgress('.cArchivedFolder = ' + .cArchivedFolder, LOGIT+SENDIT)
				.TrackProgress('.cReconFileTable = ' + .cReconFileTable, LOGIT+SENDIT)
				.TrackProgress('.cReportsFolder = ' + .cReportsFolder, LOGIT+SENDIT)
				.TrackProgress('.cSentTable = ' + .cSentTable, LOGIT+SENDIT)

				ldToday = .dToday
				ldYesterday = ldToday - 1
				lcYesterday = DTOC(ldYesterday)

				.cSubject = 'VCard CitiBank Returns for ' + TRANSFORM(DATETIME())
				
				*.TestSQL2()
				*THROW

				lcInputFolder = .cInputFolder
				lcArchivedFolder = .cArchivedFolder
				lcReportsFolder = .cReportsFolder


				* get list of files in input folder
				SET DEFAULT TO (lcInputFolder)
				lnNumFiles = ADIR(laFiles,"TOLLGP_FPCSQ*.csv")

				.TrackProgress(TRANSFORM(lnNumFiles) + ' files found in ' + lcInputFolder,LOGIT+SENDIT)

				IF lnNumFiles > 0 THEN

					* open Excel
					.oExcel = CREATEOBJECT("Excel.Application")
					.oExcel.VISIBLE = .F.
					.oExcel.displayalerts = .F.

					* sort file list by date/time
					.SortArrayByDateTime(@laFiles, @laFilesSorted)

					FOR i = 1 TO lnNumFiles
						lcRootFileName = laFilesSorted[i,1]
						lcDateTimeStamp = ( DTOC(laFilesSorted[i,3]) + " " + laFilesSorted[i,4] )
						ltDateTimeStamp = CTOT( lcDateTimeStamp )
						lcFile = lcInputFolder + lcRootFileName
						lcArchivedFile = lcArchivedFolder + lcRootFileName

						* process each file
						.TrackProgress('Processing ' + lcFile + '....',LOGIT+SENDIT)

						* first archive the file
						llArchived = FILE(lcArchivedFile)
						IF llArchived THEN
							RUN ATTRIB -R &lcArchivedFile.
							.TrackProgress('   Used ATTRIB -R for re-moving : ' + lcFile,LOGIT+SENDIT)
						ENDIF

						* move file...
						COPY FILE (lcFile) TO (lcArchivedFile)
						llArchived = FILE(lcArchivedFile)
						IF llArchived THEN
							DELETE FILE (lcFile)
							.TrackProgress('   Moved ' + lcFile,LOGIT+SENDIT)

							* process file...
							DO CASE

								CASE UPPER(LEFT(lcRootFileName,14)) = 'TOLLGP_FPCSQKO'

									.TrackProgress('=====> Found VCA Reconciliation file: ' + lcRootFileName,LOGIT+SENDIT)
									
									.lFoundReconciliationFile = .T.

									* store file info in recon file history table
									.ProcessVCAReconciliationFile(lcArchivedFile, lcRootFileName, ltDateTimeStamp)
									
									**************************************************************************************
									* email the file
									lcAttach = lcArchivedFile

									IF .lTestMode THEN
										lcSendTo = 'mark.bennett@tollgroup.com'
										lcCC = ''
									ELSE
										lcSendTo = 'george.gereis@Tollgroup.com, joe.cangelosi@Tollgroup.com, neil.devine@Tollgroup.com, joanna.scott@Tollgroup.com'
										lcCC = 'mark.bennett@tollgroup.com'
									ENDIF
									
									lcFrom = 'mark.bennett@tollgroup.com'
									lcSubject = 'VCard Reconciliation File from CitiBank for ' + TRANSFORM(DATE())
									lcBodyText = 'See attached VCard reconciliation file.' + CRLF + CRLF + '<PROJECT = CITIBANKVCARETURNS>'
									
									DO FORM dartmail2 WITH lcSendTo,lcFrom,lcSubject,lcCC,lcAttach,lcBodyText,"A"
									**************************************************************************************


								CASE UPPER(LEFT(lcRootFileName,14)) = 'TOLLGP_FPCSQKN'

									.TrackProgress('======> Found VCA Response file: ' + lcRootFileName,LOGIT+SENDIT)
									
									.lFoundResponseFile = .T.
									
									* this routine corrects a problem with the PAN (i.e.VCARD) field -- must be done before it is opened in Excel.
									.CorrectPanFields( lcArchivedFile, lcRootFileName )

									.ProcessVCAResponseFile(lcArchivedFile, lcRootFileName, ltDateTimeStamp)

								OTHERWISE
									.LOGERROR('   ERROR - unexpected file naming convention for ' + lcFile)
							ENDCASE

						ELSE
							.LOGERROR('   ERROR moving ' + lcFile)
						ENDIF

					ENDFOR  && i = 1 TO lnNumFiles
					
					
					*IF .lFoundReconciliationFile THEN			
					IF .lFoundResponseFile THEN			
						.UpdateAPTransTable2()
					ENDIF


				ENDIF  &&  lnNumFiles > 0
				
				IF TYPE('.oExcel') = 'O' AND NOT ISNULL(.oExcel) THEN
					.oExcel.QUIT()
					.oExcel = NULL
				ENDIF
				IF TYPE('.nSQLHandle') = 'N' AND .nSQLHandle > 0 THEN
					=SQLDISCONNECT(.nSQLHandle)
					.nSQLHandle = 0
				ENDIF

				.TrackProgress('Citi Bank VCard Returns process ended normally!',LOGIT+SENDIT)

			CATCH TO loError

				.TrackProgress('There was an error.',LOGIT+SENDIT)
				.TrackProgress('The Error # is: ' + TRANSFORM(loError.ERRORNO), LOGIT+SENDIT)
				.TrackProgress('The Error Message is: ' + TRANSFORM(loError.MESSAGE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred in Program: ' + TRANSFORM(loError.PROCEDURE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred at line #: ' + TRANSFORM(loError.LINENO), LOGIT+SENDIT)
				lnNumberOfErrors = lnNumberOfErrors + 1
				CLOSE DATA
				IF TYPE('.oExcel') = 'O' AND NOT ISNULL(.oExcel) THEN
					.oExcel.QUIT()
				ENDIF
				IF TYPE('.nSQLHandle') = 'N' AND .nSQLHandle > 0 THEN
					=SQLDISCONNECT(.nSQLHandle)
					.nSQLHandle = 0
				ENDIF

			ENDTRY

			CLOSE DATA
			WAIT CLEAR
			***************** INTERNAL email results ******************************
			.TrackProgress('About to send status email.',LOGIT)
			.TrackProgress('The logfile is: ' + .cLogFile, SENDIT)
			.TrackProgress('Citi Bank Vcard Returns process started: ' + .cStartTime, LOGIT+SENDIT)
			.TrackProgress('Citi Bank Vcard Returns process finished: ' + TTOC(DATETIME()), LOGIT+SENDIT)
				
			IF .nResponseErrors > 0 THEN
				.cSubject = .cSubject + " (RESPONSE ERRORS) "
			ENDIF
			
			IF NOT EMPTY(.cErrors) THEN
				.cSubject = .cSubject + " (PROCESSING ERRORS) "
			ENDIF
			
			.cBodyText = .cErrors + CRLF + CRLF + .cBodyText

			IF .lSendInternalEmailIsOn THEN
				DO FORM dartmail2 WITH .cSendTo,.cFrom,.cSubject,.cCC,.cAttach,.cBodyText,"A"
				.TrackProgress('Sent status email.',LOGIT)
			ELSE
				.TrackProgress('Did not send status email: THIS.lSendInternalEmailIsOn = FALSE.',LOGIT)
			ENDIF

		ENDWITH
		RETURN
	ENDFUNC && main


	PROCEDURE LOGERROR
		LPARAMETERS tcError
		WITH THIS
			.TrackProgress(tcError,LOGIT+SENDIT)
			.cErrors = .cErrors + ALLTRIM(tcError) + CRLF
		ENDWITH
	ENDPROC
	

	PROCEDURE CorrectPanFields
		LPARAMETERS tcFileToProcess, tcRootFileName
		* we want to insert an apostrophe before the Vcard #s in the Pan columns. Per Rick DePola, these are 16 digits and ours always begin with '555027'.
		* This is to correct a problem where Excel thinks the field is numeric and, because it can only handle 15 digits, it converts to scientific notation 
		* and changes the final to a zero!
		*
		* we will rename the archived original file, prefixing with "ORIG_" then rewrite it, correcting the PAN field, back to the original name.
		*
		WITH THIS
			LOCAL lc6DigitsToMatch, lcNew6Digits, lnPriorCommaPosition, lcOrigFile, lnNewFileHandle, lnSourceFileHandle, lcLineIn
			LOCAL lcLineOut, lnRow, lcLeftPartOfLine, lcRightPartOfLine
			
			lc6DigitsToMatch = ["555027]
			lcNew6Digits     = ["'555027]
			lcOrigFile = .cArchivedFolder + "ORIG_" + tcRootFileName
			
			COPY FILE (tcFileToProcess) TO (lcOrigFile)
			IF NOT FILE(lcOrigFile) THEN
				.TrackProgress('*** There was a problem copying [' + tcFileToProcess + '] to [' + lcOrigFile + ']', LOGIT+SENDIT)
				THROW
			ENDIF

			* open output file
			.nNewFileHandle = FCREATE(tcFileToProcess)  && FCREATE() overwrites the existing file

			IF .nNewFileHandle < 1 THEN
				.TrackProgress('!!!! Error opening output file: ' + tcFileToProcess, LOGIT+SENDIT)
				THROW
			ENDIF

			* open source file 
			.nSourceFileHandle = FOPEN(lcOrigFile)

			IF .nSourceFileHandle < 1 THEN
				.TrackProgress('!!!! Error opening input file: ' + lcOrigFile, LOGIT+SENDIT)
				THROW
			ENDIF
			
			lnRow = 0
			
			DO WHILE NOT FEOF(.nSourceFileHandle)
				
				lnRow = lnRow + 1

				* get input line
				lcLineIn = ALLTRIM(FGETS(.nSourceFileHandle,1024))
				
				IF lnRow = 1 THEN
					* We are on the header line - do not change
					lcLineOut = lcLineIn
				ELSE
					* we are on a detail line - change the PAN field.
					* first get the position of the 3rd comma from the right, because the PAN field will be to the right of that, and we only need to change that part.
					lnPriorCommaPosition = RAT(",",lcLineIn,3)
					.TrackProgress('lnPriorCommaPosition = ' + TRANSFORM(lnPriorCommaPosition), LOGIT+SENDIT)
					lcLeftPartOfLine = LEFT(lcLineIn,lnPriorCommaPosition)
					lcRightPartOfLine = SUBSTR(lcLineIn,lnPriorCommaPosition + 1)
					.TrackProgress('lcLeftPartOfLine = ' + lcLeftPartOfLine, LOGIT+SENDIT)
					.TrackProgress('lcRightPartOfLine = ' + lcRightPartOfLine, LOGIT+SENDIT)
					
					* change the right part of the line if necessary
					IF NOT (lcNew6Digits $ lcRightPartOfLine) THEN
						lcRightPartOfLine = STRTRAN(lcRightPartOfLine,lc6DigitsToMatch,lcNew6Digits)
					ENDIF
					
					* recombine parts to make the whole line
					lcLineOut = lcLeftPartOfLine + lcRightPartOfLine
					.TrackProgress('lcLineOut = ' + lcLineOut, LOGIT+SENDIT)
				ENDIF				
				
				* write output line
				=FPUTS(.nNewFileHandle, lcLineOut)

			ENDDO

			=FCLOSE(.nNewFileHandle)

			=FCLOSE(.nSourceFileHandle)

		ENDWITH
	ENDPROC  &&  CorrectPanFields
	

	PROCEDURE ProcessVCAResponseFile
		LPARAMETERS tcFileToProcess, tcRootFileName, ttDateTimeStamp
		*
		* Process the Vcard response files from Citi, update the matching SENT TABLE records and notify everyone if there were errors. Email it to Acctng?
		
		LOCAL lnRow, lcRow, luColumnA
		
		*
		LOCAL lcRequestIDColumn, lcRespCodeColumn, lcRespErrorColumn, lcResPurchIDColumn, lcResPPanColumn, lcRespCVC2Column, lcRespExpiryColumn
		STORE '' TO lcRequestIDColumn, lcRespCodeColumn, lcRespErrorColumn, lcResPurchIDColumn, lcResPPanColumn, lcRespCVC2Column, lcRespExpiryColumn
		
		PRIVATE m.requestid, m.respfile, m.respdatetm, m.respcode, m.resperror, m.respurchid, m.resppan, m.respcvc2, m.respexpiry, m.resprocdtm
		
		m.respfile = tcRootFileName
		m.respdatetm = ttDateTimeStamp

		USE (.cSentTable) IN 0 ALIAS SENTTABLE
		
		* Open spreadsheet and extract data
		.oWorkbook = .oExcel.workbooks.OPEN(tcFileToProcess)
		.oWorksheet = .oWorkbook.worksheets[1]
		
		lcRequestIDColumn = .GetColumnFromHeader( 'RequestId' )
		.TrackProgress("======> lcRequestIDColumn = " + lcRequestIDColumn,LOGIT+SENDIT)
		IF EMPTY(lcRequestIDColumn) THEN
			.LOGERROR('   ERROR - could not find [RequestId] column in Response file.')
		ENDIF
		
		lcRespCodeColumn = .GetColumnFromHeader( 'ResponseCode' )
		.TrackProgress("======> lcRespCodeColumn = " + lcRespCodeColumn,LOGIT+SENDIT)
		IF EMPTY(lcRespCodeColumn) THEN
			.LOGERROR('   ERROR - could not find [ResponseCode] column in Response file.')
		ENDIF
		
		lcRespErrorColumn = .GetColumnFromHeader( 'ErrorDescription' )
		.TrackProgress("======> lcRespErrorColumn = " + lcRespErrorColumn,LOGIT+SENDIT)
		IF EMPTY(lcRespErrorColumn) THEN
			.LOGERROR('   ERROR - could not find [ErrorDescription] column in Response file.')
		ENDIF
		
		lcResPurchIDColumn = .GetColumnFromHeader( 'PurchaseID' )
		.TrackProgress("======> lcResPurchIDColumn = " + lcResPurchIDColumn,LOGIT+SENDIT)
		IF EMPTY(lcResPurchIDColumn) THEN
			.LOGERROR('   ERROR - could not find [PurchaseID] column in Response file.')
		ENDIF
		
		lcResPPanColumn = .GetColumnFromHeader( 'PAN' )
		.TrackProgress("======> lcResPPanColumn = " + lcResPPanColumn,LOGIT+SENDIT)
		IF EMPTY(lcResPPanColumn) THEN
			.LOGERROR('   ERROR - could not find [PAN] column in Response file.')
		ENDIF
		
		lcRespCVC2Column = .GetColumnFromHeader( 'CVC2' )
		.TrackProgress("======> lcRespCVC2Column = " + lcRespCVC2Column,LOGIT+SENDIT)
		IF EMPTY(lcRespCVC2Column) THEN
			.LOGERROR('   ERROR - could not find [CVC2] column in Response file.')
		ENDIF
		
		lcRespExpiryColumn = .GetColumnFromHeader( 'Expiry' )
		.TrackProgress("======> lcRespExpiryColumn = " + lcRespExpiryColumn,LOGIT+SENDIT)
		IF EMPTY(lcRespExpiryColumn) THEN
			.LOGERROR('   ERROR - could not find [Expiry] column in Response file.')
		ENDIF
		
*SET STEP ON 

		FOR lnRow = 2 TO 10000
			lcRow = ALLTRIM(STR(lnRow))
			luColumnA = .oWorksheet.RANGE("A" + lcRow).VALUE
			IF ISNULL(luColumnA) THEN
				EXIT FOR
			ENDIF
			
			m.requestid = NVL(.oWorksheet.RANGE(lcRequestIDColumn + lcRow).VALUE,'')
			m.requestid = ALLTRIM(STR(m.requestid))
			m.requestid = PADL(m.requestid,6,'0')
			
			m.respcode = NVL(.oWorksheet.RANGE(lcRespCodeColumn + lcRow).VALUE,'')
			m.resperror = NVL(.oWorksheet.RANGE(lcRespErrorColumn + lcRow).VALUE,'')
			
			IF NOT EMPTY(m.resperror) THEN	
				.nResponseErrors = .nResponseErrors + 1		
				.TrackProgress("============================================================",LOGIT+SENDIT)
				.TrackProgress("==> On line " + lcRow + " of response file, Error = " + m.resperror,LOGIT+SENDIT)
				.TrackProgress("============================================================",LOGIT+SENDIT)
			ENDIF
			
			m.respurchid = NVL(.oWorksheet.RANGE(lcResPurchIDColumn + lcRow).VALUE,0)
			m.respurchid = INT(m.respurchid)
			
			*m.resppan = NVL(.oWorksheet.RANGE(lcResPPanColumn + lcRow).VALUE,0)
			m.resppan = NVL(.oWorksheet.RANGE(lcResPPanColumn + lcRow).VALUE,'')
			*m.resppan  = INT(m.resppan )
			*m.resppan = ALLTRIM(STR(m.resppan,20,0))
			m.resppan = ALLTRIM(m.resppan)
			* VCARD #S SHOULD NOW BE PREFIXED BY APOSTROPHES ( DONE IN .CorrectPanFields() ) TO FORCE CHARACTER REPRESENTATION; IF SO, REMOVE APOSTROPHES
			IF LEFT(m.resppan,1) = "'" THEN
				m.resppan = ALLTRIM(SUBSTR(m.resppan,2))
			ENDIF
			
			m.respcvc2 = NVL(.oWorksheet.RANGE(lcRespCVC2Column + lcRow).VALUE,0)
			m.respcvc2 = INT(m.respcvc2)
			
			m.respexpiry = NVL(.oWorksheet.RANGE(lcRespExpiryColumn + lcRow).VALUE,0)
			m.respexpiry = INT(m.respexpiry)
			
			m.resprocdtm = DATETIME()
				
			* update SENT TABLE
			* try to match the Request ID in the SENT table.
			
			SELECT SENTTABLE
			LOCATE FOR REQUESTID == m.requestid
			IF FOUND() THEN
			
				* update found record
				REPLACE SENTTABLE.respfile WITH m.respfile, SENTTABLE.respdatetm WITH m.respdatetm, ;
					SENTTABLE.respcode WITH m.respcode, SENTTABLE.resperror WITH m.resperror, SENTTABLE.respurchid WITH m.respurchid, ;
					SENTTABLE.resppan WITH m.resppan, SENTTABLE.respcvc2 WITH m.respcvc2, SENTTABLE.respexpiry WITH m.respexpiry, SENTTABLE.resprocdtm WITH m.resprocdtm ;
					IN SENTTABLE
					
				.TrackProgress("===> SUCCESS: Updated record that matched RequestID [" + m.requestid + "] in SENT TABLE",LOGIT+SENDIT)
			ELSE
			
				* just insert a new incomplete record
				INSERT INTO SENTTABLE FROM MEMVAR
				.TrackProgress("===> ERROR: could not find RequestID [" + m.requestid + "] in SENT TABLE",LOGIT+SENDIT)
				
			ENDIF
		
		ENDFOR

		IF USED('SENTTABLE') THEN
			USE IN SENTTABLE
		ENDIF

		.oWorkbook.CLOSE()

		RETURN
	ENDPROC  && ProcessVCAResponseFile
	
	
	
	FUNCTION GetColumnFromHeader
		LPARAMETERS tcHeader
		WITH THIS
			LOCAL lcFoundColumn, lnColumn, lcHeader, lcFoundHeader, lcColumn
			lcFoundColumn = ''
			lcHeader = UPPER(ALLTRIM(tcHeader))
			
			*SET STEP ON 

			FOR lnColumn = 1 TO 60
				lcColumn = GetXLAlphaColumn( lnColumn )
				lcFoundHeader = .oWorksheet.RANGE(lcColumn + "1").VALUE
				lcFoundHeader = UPPER(ALLTRIM(lcFoundHeader))
				IF lcHeader == lcFoundHeader THEN
					lcFoundColumn = lcColumn
					EXIT FOR
				ENDIF
			ENDFOR

			RETURN lcFoundColumn
		ENDWITH
	ENDFUNC



	PROCEDURE ProcessVCAReconciliationFile
		LPARAMETERS tcFileToProcess, tcRootFileName, ttDateTimeStamp
		*
		* Process the Vcard Reconciliation files from Citi ??and send them to Accounts Payable in a Platinum-Import-friendly format??.
		*
		LOCAL lnRow, lcRow, llHeaderOk, lcA1, lcB1, lcD1, lcE1, lcF1, lcG1, lcH1, lcI1, lcJ1, llFooterOk, lnNumRows, luColumnA, luColumnB, lnLastDetailRow
		PRIVATE m.filename, m.postdate, m.merchname, m.processdt, m.vendorid, m.docnum, m.invnum, m.amount, m.pymtdate

		m.filename = tcFileToProcess
		
		* Open spreadsheet and extract data

		.oWorkbook = .oExcel.workbooks.OPEN(tcFileToProcess)
		.oWorksheet = .oWorkbook.worksheets[1]
		
		* attempt to validate the spreadsheet format
		llHeaderOk = .T.
		
		lcA1 = .oWorksheet.RANGE("A1").VALUE
		IF NOT UPPER(ALLTRIM(lcA1)) == "POSTING DATE" THEN
			.LogError("ERROR: Header Cell A1 not = 'POSTING DATE'")
			llHeaderOk = .F.
		ENDIF
		
		lcB1 = .oWorksheet.RANGE("B1").VALUE
		IF NOT UPPER(ALLTRIM(lcB1)) == "BILLING AMOUNT" THEN
			.LogError("ERROR: Header Cell B1 not = 'BILLING AMOUNT'")
			llHeaderOk = .F.
		ENDIF
		
		lcC1 = .oWorksheet.RANGE("C1").VALUE
		IF NOT UPPER(ALLTRIM(lcC1)) == "MERCHANT NAME" THEN
			.LogError("ERROR: Header Cell C1 not = 'MERCHANT NAME'")
			llHeaderOk = .F.
		ENDIF
		
		lcD1 = .oWorksheet.RANGE("D1").VALUE
		IF NOT UPPER(ALLTRIM(lcD1)) == "DOCUMENT NUMBER" THEN		
			.LogError("ERROR: Header Cell D1 not = 'DOCUMENT #'")			
			llHeaderOk = .F.
		ENDIF
		
		lcE1 = .oWorksheet.RANGE("E1").VALUE
		IF NOT UPPER(ALLTRIM(lcE1)) == "VENDOR ID" THEN
			.LogError("ERROR: Header Cell E1 not = 'VENDOR ID'")
			llHeaderOk = .F.
		ENDIF
		
		lcG1 = .oWorksheet.RANGE("G1").VALUE
		IF NOT UPPER(ALLTRIM(lcG1)) == "PAYMENT DATE" THEN
			.LogError("ERROR: Header Cell G1 not = 'PAYMENT DATE'")
			llHeaderOk = .F.
		ENDIF
		
		lcH1 = .oWorksheet.RANGE("H1").VALUE
		IF NOT UPPER(ALLTRIM(lcH1)) == "INVOICE NUMBER" THEN
			.LogError("ERROR: Header Cell H1 not = 'INVOICE NUMBER'")
			llHeaderOk = .F.
		ENDIF
		
		
		
		IF llHeaderOk THEN
			.TrackProgress("======> Reconciliation Spreadsheet Header is valid.",LOGIT+SENDIT)
		ELSE
			.LogError('ERROR: Reconciliation Spreadsheet Header Row did not match the expected format. Unable to process!')
			THROW
		ENDIF
		
		* locate and validate the footer record
		llFooterOk = .F.
		FOR lnRow = 2 TO 10000
			lcRow = ALLTRIM(STR(lnRow))
			luColumnA = .oWorksheet.RANGE("A" + lcRow).VALUE
			IF ISNULL(luColumnA) THEN
				EXIT FOR
			ENDIF
			
			IF TYPE('luColumnA') = 'C' AND UPPER(ALLTRIM(luColumnA)) == "COUNT" THEN
			
				.TrackProgress("'Count' found in spreadsheet row: " + ALLTRIM(STR(lnRow)),LOGIT+SENDIT)
				
				lnLastDetailRow = lnRow - 1  && save this value for 2nd pass thru spreadsheet
				
				
				* GET THE COUNT VALUE FROM NEXT CELL OVER
				luColumnB = .oWorksheet.RANGE("B" + lcRow).VALUE
				
				IF ISNULL(luColumnB) THEN
					EXIT FOR
				ENDIF
				
				* HANDLE WHEN they give us a number with an apostrophe in front, making to a text var to vfp. In ths case make it a number.
				IF TYPE('luColumnB') = 'C' THEN
					luColumnB = VAL(luColumnB)
				ENDIF
				
				IF TYPE('luColumnB') = 'N' THEN
					IF NOT (luColumnB > 0) THEN
						EXIT FOR
					ENDIF
				ENDIF
				
				.TrackProgress("Count Value = " + ALLTRIM(STR(luColumnB)),LOGIT+SENDIT)
				* the count value should be the row # minus 2 (total rows minus the header and footer rows)
				IF luColumnB = (lnRow - 2) THEN
					llFooterOk = .T.
					.TrackProgress("======> Reconciliation Spreadsheet Footer is valid.",LOGIT+SENDIT)
				ELSE
					.TrackProgress("ERROR: Reconciliation Spreadsheet Footer 'Count' Value is incorrect.",LOGIT+SENDIT)
				ENDIF
				
				EXIT FOR
				
			ENDIF		
		
		ENDFOR
		
		IF NOT (lnLastDetailRow > 2) THEN
			.LogError('ERROR: No detail rows exist in the Reconciliation spreadsheet. Unable to process!')
			THROW
		ENDIF
		
		IF NOT llFooterOk THEN
			.LogError('ERROR: Reconciliation Spreadsheet Footer Row is missing or invalid. Unable to process!')
			THROW
		ENDIF

		* Log the file processed in HISTORY TABLE
		USE (.cReconFileTable) IN 0 ALIAS RECONTABLE


		*SET STEP ON 
				
		* now loop thru spreadsheet detail rows again to gather info
		.TrackProgress("Processing Detail Rows 2 - " + TRANSFORM(lnLastDetailRow),LOGIT+SENDIT)

		FOR lnRow = 2 TO lnLastDetailRow

			lcRow = ALLTRIM(STR(lnRow))
			
			m.postdate = .oWorksheet.RANGE("A" + lcRow).VALUE
			
			m.amount = .oWorksheet.RANGE("B" + lcRow).VALUE
			
			m.merchname = .oWorksheet.RANGE("C" + lcRow).VALUE
			IF ISNULL(m.merchname) then
				m.merchname = ""
			ELSE
				m.merchname = ALLTRIM(m.merchname)
			ENDIF
			
			m.docnum = .oWorksheet.RANGE("D" + lcRow).VALUE
			IF ISNULL(m.docnum) then
				m.docnum = ""
			ELSE
				m.docnum = ALLTRIM(m.docnum)
			ENDIF
			
			m.vendorid = .oWorksheet.RANGE("E" + lcRow).VALUE
			IF ISNULL(m.vendorid) then
				m.vendorid = ""
			ELSE
				m.vendorid = ALLTRIM(m.vendorid)
			ENDIF
			
			m.pymtdate = .oWorksheet.RANGE("G" + lcRow).VALUE
			IF ISNULL(m.pymtdate) then
				m.pymtdate = {}
			ENDIF
			
			m.invnum = .oWorksheet.RANGE("H" + lcRow).VALUE
			IF ISNULL(m.invnum) then
				m.invnum = ""
			ELSE
				m.invnum = ALLTRIM(TRANSFORM(m.invnum))
			ENDIF
			
			m.transid = .oWorksheet.RANGE("I" + lcRow).VALUE
			IF ISNULL(m.transid) THEN
				m.transid = ""
			ELSE
				m.transid = ALLTRIM(TRANSFORM(m.transid))
			ENDIF
			
			m.vcardno = .oWorksheet.RANGE("J" + lcRow).VALUE
			IF ISNULL(m.vcardno) THEN
				m.vcardno = ""
			ELSE
				m.vcardno = ALLTRIM(TRANSFORM(m.vcardno))
			ENDIF
			
			m.realcardno = .oWorksheet.RANGE("K" + lcRow).VALUE
			IF ISNULL(m.realcardno) THEN
				m.realcardno = ""
			ELSE
				m.realcardno = ALLTRIM(TRANSFORM(m.realcardno))
			ENDIF

			m.processdt = DATETIME()
			
			INSERT INTO RECONTABLE FROM MEMVAR

		ENDFOR


		IF USED('RECONTABLE') THEN
			USE IN RECONTABLE
		ENDIF

		.oWorkbook.CLOSE()

		RETURN
	ENDPROC  && ProcessVCAReconciliationFile

	
	
	PROCEDURE UpdateAPTransTable2
		* write the returned VCARD #s into the Aptrans reference field in the platinum tables.
		* to do this, loop thru SENT table and process recs where aptransok = .F.
		*
		* NOTE: This routine can only be run from a server with the proper platinum dsn on it, such as MIS16

		WITH THIS
		
			LOCAL llAPTRANSOK

			.nSQLHandle = SQLSTRINGCONNECT(.cDsn,.T.)

			IF .nSQLHandle > 0 THEN

				USE (.cSentTable) IN 0 ALIAS SENTTABLE
			
				SELECT SENTTABLE
				SCAN FOR ( RESPDATETM > (DATE()-6) ) AND ( NOT APTRANSOK )

					lcSql = "UPDATE APTRAN " + ;
							" SET REFERENCE = 'VCA=" + ALLTRIM(SENTTABLE.RESPPAN) + "'" + ;
					        " WHERE cashacctkey = '" + .cashacctkey + "'" + ;
					        " AND DOCUMENTNUMBER = '" + ALLTRIM(SENTTABLE.DOCNUM) + "'" + ;
					        " AND INVOICENUMBER = '" + ALLTRIM(SENTTABLE.INVNUM) + "'" + ;
					        " AND DOCUMENTAMT = " + ALLTRIM(STR((-1*SENTTABLE.AMOUNT),10,2))

					* NOTE: the Reference field is 20 characters long.

					lnResult = SQLEXEC(.nSQLHandle, lcSql)
					.TrackProgress("Result Code [" + TRANSFORM(lnResult) + "] returned by this SQL: " + CRLF + lcSql, LOGIT+SENDIT)
					
					* now do a Select to see if the Update command took
					
					IF USED('CURCHECKAP') THEN
						USE IN CURCHECKAP
					ENDIF	
									
					lcSql = "SELECT REFERENCE FROM APTRAN " + ;
					        " WHERE cashacctkey = '" + .cashacctkey + "'" + ;
					        " AND DOCUMENTNUMBER = '" + ALLTRIM(SENTTABLE.DOCNUM) + "'" + ;
					        " AND INVOICENUMBER = '" + ALLTRIM(SENTTABLE.INVNUM) + "'" + ;
					        " AND DOCUMENTAMT = " + ALLTRIM(STR((-1*SENTTABLE.AMOUNT),10,2))

					lnResult = SQLEXEC(.nSQLHandle, lcSql, 'CURCHECKAP')
					.TrackProgress("Result Code [" + TRANSFORM(lnResult) + "] returned by this SQL: " + CRLF + lcSql, LOGIT+SENDIT)
					
					llAPTRANSOK = .F.
					
					IF USED('CURCHECKAP') AND (NOT EOF('CURCHECKAP')) THEN
						IF ALLTRIM(CURCHECKAP.REFERENCE) == "VCA=" + ALLTRIM(SENTTABLE.RESPPAN) THEN
							llAPTRANSOK = .T.
							* UPDATE SENT TABLE
							REPLACE SENTTABLE.APTRANSOK WITH llAPTRANSOK IN SENTTABLE
						ELSE
							* nothing, leave flag set to False
						ENDIF
					ELSE
						* nothing, leave flag set to False
					ENDIF
					
					IF NOT llAPTRANSOK THEN
						.TrackProgress("==========> WARNING: the previous VCARD Update command seems to have failed!", LOGIT+SENDIT)
					ENDIF
					
				
				ENDSCAN

				IF USED('SENTTABLE') THEN
					USE IN SENTTABLE
				ENDIF

				=SQLDISCONNECT(.nSQLHandle)
				.nSQLHandle = 0

			ELSE
				* connection error
				.TrackProgress('ERROR: in UpdateAPTransTable(), unable to connect to PLATINUM.', LOGIT+SENDIT)
			ENDIF  &&  .nSQLHandle > 0

		ENDWITH
	ENDPROC && UpdateAPTransTable2

	
	


	PROCEDURE TestSQL2
		WITH THIS

			LOCAL lcDSN, lnResult, lnRetVal

			.nSQLHandle = SQLSTRINGCONNECT(.cDsn,.T.)

			IF .nSQLHandle > 0 THEN


				*lcSql = "SELECT * FROM APTRAN WHERE cashacctkey = '1010000000' AND RecDate < '2015-01-01' ORDER BY RECDATE"
				*lcSql = "SELECT * FROM APTRAN WHERE DocumentDate > '2015-06-01' order by DocumentDate "
				*lcSql = "SELECT * FROM APTRAN WHERE DocumentDate > '2015-09-01'"
				lcSql = "SELECT * FROM APTRAN WHERE cashacctkey = '" + .cashacctkey + "' order by DocumentDate "

				IF .ExecSQL(lcSql, 'CURAPTRANS', RETURN_DATA_NOT_MANDATORY) THEN

					IF USED('CURAPTRANS') AND (NOT EOF('CURAPTRANS')) THEN

						SELECT CURAPTRANS
						BROWSE
						*COPY TO F:\UTIL\CITIVCAPROC\REPORTS\TEXT.XLS XL5
						
					ELSE
						.TrackProgress("==========> Found no recs with new cash account: " + .cashacctkey, LOGIT+SENDIT)

					ENDIF

				ENDIF  &&  .ExecSQL(lcSQL

				IF USED('CURAPTRANS') THEN
					USE IN CURAPTRANS
				ENDIF

				=SQLDISCONNECT(.nSQLHandle)
				.nSQLHandle = 0

			ELSE
				* connection error
				.TrackProgress('Unable to connect to PLATINUM.', LOGIT+SENDIT)
			ENDIF  &&  .nSQLHandle > 0


		ENDWITH

	ENDPROC  &&  TestSQL2


	FUNCTION ExecSQL
		LPARAMETERS tcSQL, tcCursorName, tlNoDataReturnedIsOkay
		LOCAL llRetval, lnResult
		WITH THIS
			* close target cursor if it's open
			IF USED(tcCursorName)
				USE IN (tcCursorName)
			ENDIF
			WAIT WINDOW "Fetching remote data; please wait..." NOWAIT
			lnResult = SQLEXEC(.nSQLHandle, tcSQL, tcCursorName)
			llRetval = ( lnResult > 0 )
			IF llRetval THEN
				* see if any data came back
				IF NOT tlNoDataReturnedIsOkay THEN
					IF (NOT USED(tcCursorName)) OR EOF(tcCursorName) THEN
						llRetval = .F.
						.TrackProgress('No data was returned by this query: ' +CRLF + tcSQL, LOGIT+SENDIT)
						.cSendTo = 'Mark Bennett <mark.bennett@tollgroup.com>'
					ENDIF
				ENDIF
			ELSE
				.TrackProgress("Result Code [" + TRANSFORM(lnResult) + "] returned by this query: " + tcSQL, LOGIT+SENDIT)
				.cSendTo = 'Mark Bennett <mark.bennett@tollgroup.com>'
			ENDIF
			WAIT CLEAR
			RETURN llRetval
		ENDWITH
	ENDFUNC


	PROCEDURE TrackProgress
		* do any combination of Wait Window, writing to logfile, and adding to body of email,
		* based on nFlags parameter.
		LPARAMETERS tcExpression, tnFlags
		WITH THIS
			IF BITAND(tnFlags,LOGIT) = LOGIT THEN
				IF .lLoggingIsOn THEN
					?
					? .cProcessName + "  " + TTOC(DATETIME()) + ": " + tcExpression
				ENDIF
			ENDIF
			IF .lWaitWindowIsOn THEN
				IF BITAND(tnFlags,NOWAITIT) = NOWAITIT THEN
					WAIT WINDOW tcExpression NOWAIT
				ENDIF
				IF BITAND(tnFlags,WAITIT) = WAITIT THEN
					WAIT WINDOW tcExpression TIMEOUT .nWaitWindowTimeout
				ENDIF
			ENDIF
			IF BITAND(tnFlags,SENDIT) = SENDIT THEN
				IF .lSendInternalEmailIsOn THEN
					.cBodyText = .cBodyText + tcExpression + CRLF + CRLF
				ENDIF
			ENDIF
		ENDWITH
	ENDPROC  &&  TrackProgress


	PROCEDURE SortArrayByDateTime
		* expects taInArray[] to be the result of an ADIR()
		LPARAMETERS taInArray, taOutArray
		EXTERNAL ARRAY taInArray, taOutArray
		LOCAL lnRows, lnCols, i, j
		* extending taInArray[] to process the files in date/time order
		lnRows = ALEN(taInArray,1)
		lnCols = ALEN(taInArray,2)
		* create taOutArray with one more column
		DIMENSION taOutArray[lnRows,lnCols + 1]
		* and fill it with taInArray columns AND (filedate + filetime) in new column
		* ( ACOPY() doesn't preserve elements properly because of different # of columns )
		FOR i = 1 TO lnRows
			FOR j = 1 TO lnCols
				taOutArray[i,j] = taInArray[i,j]
			ENDFOR
			taOutArray[i,lnCols + 1] = DTOS(taInArray[i,3]) + "_" + taInArray[i,4]
		ENDFOR
		* sort taOutArray on date/time column
		=ASORT(taOutArray,6)
	ENDPROC


ENDDEFINE


	
*!*		PROCEDURE UpdateAPTransTable
*!*			* write the returned VCARD #s into the Aptrans reference field.
*!*			* to do this, loop thru history table and process recs where aptransok = .F.
*!*			*
*!*			* NOTE: This routine can only be run from a server with the proper platinum dsn on it, such as MIS16

*!*			WITH THIS
*!*			
*!*				LOCAL llAPTRANSOK

*!*				.nSQLHandle = SQLSTRINGCONNECT(.cDsn,.T.)

*!*				IF .nSQLHandle > 0 THEN

*!*					USE (.cHistoryTable) IN 0 ALIAS HISTORYTABLE
*!*				
*!*					SELECT HISTORYTABLE
*!*					SCAN FOR (FILEDT > DATE()-2) AND (NOT APTRANSOK)

*!*						* UPDATE THE REFERENCE FIELD WITH THE VCARD #
*!*						*!*	lcSql = "UPDATE APTRAN " + ;
*!*						*!*			" SET REFERENCE = '" + ALLTRIM(HISTORYTABLE.VCARDNUM) + "'" + ;
*!*						*!*	        " WHERE cashacctkey = '" + .cashacctkey + "'" + ;
*!*						*!*	        " AND DOCUMENTNUMBER = '" + ALLTRIM(HISTORYTABLE.DOCNUM) + "'" + ;
*!*						*!*	        " AND INVOICENUMBER = '" + ALLTRIM(HISTORYTABLE.INVNUM) + "'" + ;
*!*						*!*	        " AND DOCUMENTAMT = " + ALLTRIM(STR(HISTORYTABLE.AMOUNT,10,2))

*!*						lcSql = "UPDATE APTRAN " + ;
*!*								" SET REFERENCE = 'VCARD=" + ALLTRIM(HISTORYTABLE.VCARDNUM) + "'" + ;
*!*						        " WHERE cashacctkey = '" + .cashacctkey + "'" + ;
*!*						        " AND DOCUMENTNUMBER = '" + ALLTRIM(HISTORYTABLE.DOCNUM) + "'" + ;
*!*						        " AND INVOICENUMBER = '" + ALLTRIM(HISTORYTABLE.INVNUM) + "'" + ;
*!*						        " AND DOCUMENTAMT = " + ALLTRIM(STR(HISTORYTABLE.AMOUNT,10,2))

*!*						* NOTE: the Reference field is 20 characters long.

*!*						lnResult = SQLEXEC(.nSQLHandle, lcSql)
*!*						.TrackProgress("Result Code [" + TRANSFORM(lnResult) + "] returned by this SQL: " + CRLF + lcSql, LOGIT+SENDIT)
*!*						
*!*						* now do a Select to see if the Update command took
*!*						
*!*						IF USED('CURCHECKAP') THEN
*!*							USE IN CURCHECKAP
*!*						ENDIF	
*!*										
*!*						lcSql = "SELECT REFERENCE FROM APTRAN " + ;
*!*						        " WHERE cashacctkey = '" + .cashacctkey + "'" + ;
*!*						        " AND DOCUMENTNUMBER = '" + ALLTRIM(HISTORYTABLE.DOCNUM) + "'" + ;
*!*						        " AND INVOICENUMBER = '" + ALLTRIM(HISTORYTABLE.INVNUM) + "'" + ;
*!*						        " AND DOCUMENTAMT = " + ALLTRIM(STR(HISTORYTABLE.AMOUNT,10,2))

*!*						lnResult = SQLEXEC(.nSQLHandle, lcSql, 'CURCHECKAP')
*!*						.TrackProgress("Result Code [" + TRANSFORM(lnResult) + "] returned by this SQL: " + CRLF + lcSql, LOGIT+SENDIT)
*!*						
*!*						llAPTRANSOK = .F.
*!*						
*!*						IF USED('CURCHECKAP') AND (NOT EOF('CURCHECKAP')) THEN
*!*							IF ALLTRIM(CURCHECKAP.REFERENCE) == ALLTRIM(HISTORYTABLE.VCARDNUM) THEN
*!*								llAPTRANSOK = .T.
*!*								* UPDATE HISTORY TABLE
*!*								REPLACE HISTORYTABLE.APTRANSOK WITH llAPTRANSOK IN HISTORYTABLE
*!*							ELSE
*!*								* nothing, leave flag set to False
*!*							ENDIF
*!*						ELSE
*!*							* nothing, leave flag set to False
*!*						ENDIF
*!*						
*!*						IF NOT llAPTRANSOK THEN
*!*							.TrackProgress("==========> WARNING: the previous VCARD Update command seems to have failed!", LOGIT+SENDIT)
*!*						ENDIF
*!*						
*!*					
*!*					ENDSCAN

*!*					IF USED('HISTORYTABLE') THEN
*!*						USE IN HISTORYTABLE
*!*					ENDIF

*!*					=SQLDISCONNECT(.nSQLHandle)
*!*					.nSQLHandle = 0

*!*				ELSE
*!*					* connection error
*!*					.TrackProgress('ERROR: in UpdateAPTransTable(), unable to connect to PLATINUM.', LOGIT+SENDIT)
*!*				ENDIF  &&  .nSQLHandle > 0

*!*			ENDWITH
*!*		ENDPROC && UpdateAPTransTable


*!*		PROCEDURE TestSQL
*!*			WITH THIS

*!*				LOCAL lnResult, lnRetVal

*!*				.nSQLHandle = SQLSTRINGCONNECT(.cDsn,.T.)

*!*				IF .nSQLHandle > 0 THEN

*!*					* TEST UPDATING THE REFERENCE FIELD
*!*					lcSql = "UPDATE APTRAN SET REFERENCE = '                    ' WHERE cashacctkey = '1010000000' AND DOCUMENTNUMBER = '000357818' AND INVOICENUMBER = '040514' AND DOCUMENTAMT = 1459.15"

*!*					* NOTE: the Reference field is 20 characters long.

*!*					*!*						lnResult = SQLEXEC(.nSQLHandle, lcSql)
*!*					*!*						.TrackProgress("Result Code [" + TRANSFORM(lnResult) + "] returned by this SQL: " + lcSql, LOGIT+SENDIT)


*!*					IF USED('CURAPTRANS') THEN
*!*						USE IN CURAPTRANS
*!*					ENDIF

*!*					*lcSql = "SELECT * FROM APTRAN WHERE cashacctkey = '1010000000' AND RecDate < '2015-01-01' ORDER BY RECDATE"
*!*					lcSql = "SELECT * FROM APTRAN WHERE cashacctkey = '1010000000' AND DOCUMENTNUMBER = '000357818' AND INVOICENUMBER = '040514' AND DOCUMENTAMT = 1459.15"

*!*					IF .ExecSQL(lcSql, 'CURAPTRANS', RETURN_DATA_NOT_MANDATORY) THEN

*!*						IF USED('CURAPTRANS') AND NOT EOF('CURAPTRANS') THEN

*!*							SELECT CURAPTRANS
*!*							BROWSE
*!*							*COPY TO F:\UTIL\CITIVCAPROC\REPORTS\TEXT.XLS XL5

*!*						ENDIF

*!*					ENDIF  &&  .ExecSQL(lcSQL

*!*					IF USED('CURAPTRANS') THEN
*!*						USE IN CURAPTRANS
*!*					ENDIF

*!*					=SQLDISCONNECT(.nSQLHandle)
*!*					.nSQLHandle = 0

*!*				ELSE
*!*					* connection error
*!*					.TrackProgress('Unable to connect to PLATINUM.', LOGIT+SENDIT)
*!*				ENDIF  &&  .nSQLHandle > 0


*!*			ENDWITH

*!*		ENDPROC  && TestSQL

