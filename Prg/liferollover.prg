* Rollover report (WIP style detail) for Lifefactory

utilsetup("LIFEROLLOVER")

goffice="I"

xsqlexec("select outship.*, units, totqty, style, color, id, pack " + ;
	"from outship, outdet where outship.outshipid=outdet.outshipid " + ;
	"and outship.accountid=6034 and del_date={} and notonwip=0 " + ;
	"and (ctnqty#0 or masterpack=1) and sp=0","xdytemp",,"wh")

select *, sum(iif(units, totqty, 000000)) as qtyfld, ;
	left(dtoc(start),5) as startview, left(dtoc(min(cancel)),5) as cancelview, ;
	left(dtoc(picked),5) as pickedview, left(dtoc(max(called)),5) as calledview, ;
	left(dtoc(max(appt)),5) as apptview, left(dtoc(del_date),5) as delview, ;
	iif(sp, "Y", "N") as spview, space(20) as status ;
	from xdytemp group by ship_ref, style, color, id, pack ;
	into cursor xrpt readwrite

scan
	do case
	case notonwip and left(keyrec="CANCEL",6)
		replace status with "Ret To Stock"
	case !pulled
		replace status with "Not Pulled"
	case !emptynul(del_date) and (xups or xfedex or xusps)
		replace status with "Shipped"
	case !emptynul(del_date)
		replace status with "Delivered"
	case !emptynul(truckloaddt)
		replace status with "Loaded"
	case !emptynul(staged)
		replace status with "Staged"
	case !emptynul(labeled)
		replace status with "Labeled"
	case !emptynul(picked)
		replace status with "Picked"
	otherwise
		replace status with "Pulled"
	endcase
endscan
set step on 
xfilename="h:\fox\life"+dt2month(date())+".xls"
copy file f:\wh\liferollover.xls to &xfilename

oexcel = createobject("Excel.Application")
oworkbook = oexcel.workbooks.open(xfilename)
osheet1=oworkbook.worksheets[1]
xsheet="osheet1"
i=5

select xrpt
scan 
	i=i+1
	ii=transform(i)
	exceltext("A"+ii,ship_ref)
	exceltext("B"+ii,wo_num)
	exceltext("C"+ii,consignee)
	exceltext("D"+ii,cnee_ref)
	exceltext("E"+ii,style)
	exceltext("F"+ii,color)
	exceltext("G"+ii,id)
	exceltext("H"+ii,pack)
	exceltext("I"+ii,ship_via)
	exceltext("J"+ii,qtyfld)
	exceltext("K"+ii,startview)
	exceltext("L"+ii,cancelview)
	exceltext("M"+ii,status)
	exceltext("N"+ii,calledview)
	exceltext("O"+ii,apptview)
	exceltext("P"+ii,appt_time)
	exceltext("Q"+ii,appt_num)
endscan

oworkbook.save()
oworkbook.close()
oexcel.quit()
release oexcel

*

tattach=xfilename
tsendto="Gina@Lifefactory.com, ethan@lifefactory.com"
*tsendto="dyoung@fmiint.com"
tcc=""
tFrom ="TGF Operations <fmi-transload-ops@fmiint.com>"
tmessage = "See attached file"
tSubject = "Month End WIP report"

Do Form dartmail2 With tsendto,tFrom,tSubject," ",tattach,tmessage,"A"

schedupdate()

_screen.Caption=gscreencaption
on error
