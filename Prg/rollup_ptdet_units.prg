******************************
*Procedure newrollup
******************************
nChangedRecs = 0
Select Count(1) as dup,ptid,style,color,id,pack,upc From xptdet into Cursor xptids Group By ptid,style,color,id,pack,upc

Select xptdet
Go top
Sum(totqty) to lnBeforeCount

Select xptids
Count For dup >1 To nChangedRecs

Select xptdet
Set Deleted On
Locate
Select xptids
Scan For dup >1
  Select xptdet 
  Locate For ptid=xptids.ptid And style=xptids.style and color=xptids.color and id=xptids.id and pack=xptids.pack and upc=xptids.upc
  Scatter Memvar memo
  Sum(totqty) to nTotqty1  for ptid=xptids.ptid and style=xptids.style and color=xptids.color and id=xptids.id and pack=xptids.pack and upc=xptids.upc
  Select xptdet
  Delete for ptid=xptids.ptid and style=xptids.style and color=xptids.color and id=xptids.id and pack=xptids.pack and upc=xptids.upc 
  Insert Into xptdet From Memvar 
  replace totqty  With nTotqty1 In xptdet
  replace origqty With nTotqty1 In xptdet
Endscan

Select xptdet
Go top
Sum(totqty) to lnAfterCount

If lnBeforeCount != lnAfterCount
   tsendto = "pgaidis@fmiint.com"
   tcc = ""
   tattach = ""
   cErrMsg = "ROLL UP ISSUE for file "+xfile
   throw
Endif 

If nChangedRecs > 0
  Wait Window "There were "+Trans(nChangedRecs)+" records rolled up" Timeout 1
Endif

*Endproc
**********************************************************************************************************8
