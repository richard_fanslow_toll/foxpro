**This script will capture all files that have been successfully FTP'd

utilsetup("MJ_ADJ_FROM_RETURNS")
set exclusive off
set deleted on
set talk off
set safety off
with _screen
	.autocenter = .t.
	.windowstate = 0
	.borderstyle = 1
	.width = 320
	.height = 210
	.top = 290
	.left = 110
	.closable = .t.
	.maxbutton = .t.
	.minbutton = .t.
	.scrollbars = 0
	.caption = "MJ_ADJ_FROM_RETURNS"
endwith

*!*	coffice='J'
*!*	if usesqladj()
*!*		xsqlexec("select * from adj where mod='"+COFFICE+"'",,,"wh")
*!*		index on adjid tag adjid
*!*		set order to
*!*	else
*!*		if !used("adj")
*!*			use f:\whj\whdata\adj in 0
*!*		endif
*!*	endif

goffice='J'
xsqlexec("select * from adj where mod='"+goffice+"' and addby='YEN' and adjdt=["+DTOC(DATE()-10)+"] and " + ;
	"comment#'WAREHOUSE LOCATION CHANGED BY DRIVER' and comment#'RETURNS C' and palletid#'SENT'",,,"wh")

select adjid,adjdt,style,color,id,totqty,whseloc,comment,adjtype from adj where addby='YEN' and adjdt >{10/01/2015} and ;
	comment!='WAREHOUSE LOCATION CHANGED BY DRIVER' and comment!='RETURNS C' and palletid!='SENT' into cursor t1 readwrite

select adj
scan for adjdt>date()-5
	select t1
	locate for adjid=adj.adjid
	if found()
		replace adj.palletid with 'SENT' in adj
	endif
endscan

select t1

if reccount() > 0
	export to "S:\MarcJacobsData\TEMP\mj_adj_from_returns"  type xls
	tsendto = "tmarg@fmiint.com,yenny.garcia@tollgroup.com,brian.mcglone@tollgroup.com,o.batista@marcjacobs.com,v.gil@marcjacobs.com"
	tattach = "S:\MarcJacobsData\temp\mj_adj_from_returns.xls"
	tcc =""
	tfrom ="Toll EDI System Operations <fmi-transload-ops@fmiint.com>"
	tmessage = "MJ ADJUSTMENTS FROM RETURNS:  " +dtoc (date())
	tsubject = "MJ ADJUSTMENTS FROM RETURNS"
	do form m:\dev\frm\dartmail2 with tsendto,tfrom,tsubject, tcc,tattach,tmessage  ,"A"

else
endif






close data all
schedupdate()

