* creates vdetent view - MUST BE RUN IN VFP 8.0 !!

set enginebehavior 70
set exclusive off
set safety off

if type("gmilitarytime")="U"
	public gmilitarytime
endif

close databases all
open database f:\wo\wodata\wo
*open database m:\dev\wodata\wo

CREATE SQL VIEW "VCHASTRK" AS SELECT * FROM wo!chastrk where &gchastrkfilter

DBSetProp('VCHASTRK', 'View', 'UpdateType', 1)
DBSetProp('VCHASTRK', 'View', 'WhereType', 1)
DBSetProp('VCHASTRK', 'View', 'FetchMemo', .T.)
DBSetProp('VCHASTRK', 'View', 'SendUpdates', .T.)
DBSetProp('VCHASTRK', 'View', 'UseMemoSize', 255)
DBSetProp('VCHASTRK', 'View', 'FetchSize', 100)
DBSetProp('VCHASTRK', 'View', 'MaxRecords', -1)
DBSetProp('VCHASTRK', 'View', 'Tables', 'wo!chastrk')
DBSetProp('VCHASTRK', 'View', 'Prepared', .F.)
DBSetProp('VCHASTRK', 'View', 'CompareMemo', .T.)
DBSetProp('VCHASTRK', 'View', 'FetchAsNeeded', .F.)
DBSetProp('VCHASTRK', 'View', 'FetchSize', 100)
DBSetProp('VCHASTRK', 'View', 'Comment', "")
DBSetProp('VCHASTRK', 'View', 'BatchUpdateCount', 1)
DBSetProp('VCHASTRK', 'View', 'ShareConnection', .F.)

DBSetProp('VCHASTRK.chastrkid', 'Field', 'KeyField', .T.)
DBSetProp('VCHASTRK.chastrkid', 'Field', 'Updatable', .T.)

close databases all 

gunshot()