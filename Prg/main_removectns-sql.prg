PARAMETERS cOffice
WAIT WINDOW "Now removing cartons..." TIMEOUT 1
*SET STEP ON

*xgetsql

cSrvr = "tgfnjsql01"

SELECT 0
USE F:\wh\sqlpassword
cSQLPass = ALLTRIM(sqlpassword.PASSWORD)
USE IN sqlpassword

lcDSNLess="driver=SQL Server;server=&csrvr;uid=SA;pwd=&cSQLPass;DATABASE=PICKPACK"
nHandle=SQLSTRINGCONNECT(lcDSNLess,.T.)
SQLSETPROP(nHandle,"DispLogin",3)

IF nHandle=0 && bailout
	WAIT WINDOW "Could not make SQL connection" TIMEOUT 3
	cxMessage = "NO SQL CONNECT"
	THROW
ENDIF

SELECT cutuccs
cWaitstr = "Removing "+TRANSFORM(RECCOUNT())+" records"
WAIT WINDOW cWaitstr TIMEOUT 1

scan
	if usesql()
		cucc = alltrim(cutuccs.ucc)
		wait window "now updating carton: "+cucc nowait
		xsqlexec("update cartons set totqty=0 where ucc='"+cucc+"'",,,"pickpack")
	else
		cUCC = ALLTRIM(cutuccs.ucc)
		WAIT WINDOW "Now updating carton: "+cUCC nowait
		lcQ1 = [update dbo.cartons]
		lcQ2 = [ SET TOTQTY = 0 ]
		lcQ3 = [ WHERE UCC = ]
		lcQ4 = " '&cUCC' "
		lcsql = lcQ1+lcQ2+lcQ3+lcQ4
		llSuccess=SQLEXEC(nHandle,lcsql)
		IF llSuccess=0  && no records 0 indicates no records and 1 indicates records found
			ASSERT .F. MESSAGE "At NO RECORDS statement in SQLConnect"
			WAIT WINDOW "No "+cCustName+" "+cWhseLoc+" records found" TIMEOUT 2
		endif
	endif
ENDSCAN
SQLCANCEL(nHandle)
*SQLDISCONNECT(0)
WAIT WINDOW "SQL UCC update complete...exiting" nowait

RETURN

