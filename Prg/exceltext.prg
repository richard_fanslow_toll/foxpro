* start with:
* oexcel=createobject("excel.application")
* oexcel.sheetsinnewworkbook=1
* oworkbook=oexcel.workbooks.add()
* oexcel.visible=.t.
*
* for i=1 to 1
*	ii=transform(i)
* 	osheet&ii=oworkbook.worksheets[i]
*	osheet&ii..name=ii
* next
*
* xsheet="osheet1"

* examples:
* exceltext("e1","Account #:   ","bold right middle")
* exceltext("g"+transform(xline),rate,"shrink highlight",8,'_($* #,##0.00_);_($* (#,##0.00);_($* ""-""??_);_(@_)')

lparameters xrange, xtext, xformat, xsize, xnumberformat, xblankifzero, xbackcolor
**xbacksolor needs to be a string indicating RGB(), ie "RGB(255,255,128)"

if xblankifzero and xtext=0
	return
endif

if vartype(xtext)="C"
	xtext=trim(xtext)
endif
if vartype(xtext)="D" and empty(xtext)
	xtext=""
endif
if vartype(xtext)="L"
	xtext=iif(xtext,"YES","")
endif

with &xsheet

try
	.range(xrange).value = xtext
catch
	if type("xtext")="D"
		if gsystemmodule#"WO"
			messagebox("You have a bad date: "+dtoc(xtext))
		endif
	endif
endtry

if !empty(xformat)
	if "bold"$xformat
		.range(xrange).font.bold = .t.
	endif
	if "underline"$xformat
		.range(xrange).font.underline = 2
	endif
	if "italic"$xformat
		.range(xrange).font.italic = .t.
	endif
	if "left"$xformat
		.range(xrange).horizontalalignment = -4131
	endif
	if "center"$xformat
		.range(xrange).horizontalalignment = -4108
	endif
	if "right"$xformat
		.range(xrange).horizontalalignment = -4152
	endif
	if "middle"$xformat
		.range(xrange).verticalalignment = -4108
	endif
	if "highlight"$xformat
		.range(xrange).Interior.ColorIndex = 6
	endif
	if "shrink"$xformat
		.range(xrange).shrinktofit = .t.
	endif
	if "red"$xformat
		.range(xrange).font.color = rgb(255,0,0)
	endif
endif

if !empty(xsize)
	.range(xrange).font.size = xsize
endif

if !empty(xbackcolor)
	.range(xrange).interior.color = &xbackcolor
endif

* number format examples
* '_($* #,##0.00_);_($* (#,##0.00);_($* ""-""??_);_(@_)'
* "###,###"

if !empty(xnumberformat)
	.range(xrange).numberformat = xnumberformat
endif
	
endwith