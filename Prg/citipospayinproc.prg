
***********************************************************************************************************
* Import Genesis Check Issuance (positive pay) files from UK and output issuance files to CitiBank
* output files will be in same format as those we currently do in CitiBankPosPay project.
*
* Build EXE in F:\UTIL\CITIPOSPAYINPROC\
*
***********************************************************************************************************

LOCAL loCitiPosPayImportProcess, lnError, lcProcessName

DECLARE INTEGER CreateMutex IN Win32API ;
	STRING @lpMutexAttributes, ;
	STRING bInitialOwner, ;
	STRING @lpName

DECLARE INTEGER GetLastError IN Win32API

* if running in EXE check to see if already running
IF _VFP.STARTMODE > 0 THEN
	lcProcessName = "CitiPosPayInProc"
	CreateMutex(0,CHR(1),@lcProcessName)
	lnError = GetLastError()
	IF lnError = 183
		WAIT WINDOW TIMEOUT 30 "Citi Pos Pay Import process is already running..."
		RETURN .F.
	ENDIF
ENDIF

loCitiPosPayImportProcess = CREATEOBJECT('CitiPosPayImportProcess')
loCitiPosPayImportProcess.MAIN()

CLOSE DATABASES ALL


RETURN

#DEFINE LOGIT 1
#DEFINE WAITIT 2
#DEFINE NOWAITIT 4
#DEFINE SENDIT 8
#DEFINE CR CHR(13)
#DEFINE LF CHR(10)
#DEFINE CRLF CHR(13) + CHR(10)
#DEFINE RETURN_DATA_MANDATORY .F.
#DEFINE RETURN_DATA_NOT_MANDATORY .T.
#DEFINE CSVFORMAT 6
#DEFINE WRITE_ONLY_UNBUFFERED 11
#DEFINE COMMA ","

DEFINE CLASS CitiPosPayImportProcess AS CUSTOM

	cProcessName = 'CitiPosPayImportProcess'

	lTestMode = .T.  && .F. FOR NORMAL OPERATION, .T. TO SUPPRESS ALL BUT DEVELOPER EMAILS AND CHANGE INPUT TABLE, ETC.

	lAutoYield = .F.

	* date time props
	dToday = DATE()

	cStartTime = TTOC(DATETIME())

	* file/folder properties
	cInputFolder = 'F:\UTIL\CITIPOSPAYINPROC\TESTIN\'
	cOutputFolder = 'F:\UTIL\CITIPOSPAYINPROC\TESTOUT\'
	cArchiveFolder = 'F:\UTIL\CITIPOSPAYINPROC\ARCHIVED\'

	* processing properties
	company = ""
	cTotalAmt = ""
	ctotalitems = ""
	cPaymentamt = ""
	cpaymentitems = ""
	cVoidAmt = ""
	cVoidItems = ""
	nhandle = 0
	cAcctnumber = ""
	prefix = ""
	cTransFileRoot = ""
	cThrowDescription = ''
	StartDate = {}
	EndDate = {}
	cTRANSFILENAME = ""

	* wait window properties
	nWaitWindowTimeout = 2
	lWaitWindowIsOn = .T.

	* logfile properties
	lLoggingIsOn = .T.
	cLogFile = 'F:\UTIL\CITIPOSPAYINPROC\LOGFILES\CitiPosPayImportProcess_LOG.txt'

	* INTERNAL email properties
	lSendInternalEmailIsOn = .T.
	cFrom = 'FMI System OPS <fmi-transload-ops@fmiint.com>'
	cSendTo = 'mbennett@fmiint.com'
	cCC = ''
	cSubject = 'Citi Pos Pay Import Process Results for ' + TTOC(DATETIME())
	cAttach = ''
	cBodyText = ''
	cTopBodyText = ''

	cCOMPUTERNAME = ''
	cUSERNAME = ''


	FUNCTION INIT
		IF NOT DODEFAULT()
			RETURN .F.
		ENDIF
		WITH THIS
			*SET RESOURCE OFF
			CLOSE DATA
			SET HOURS TO 24
			SET CENTURY OFF
			SET DATE YMD
			SET DECIMAL TO 0
			SET TALK OFF
			SET DELETED ON
			SET CONSOLE OFF
			SET EXCLUSIVE OFF
			SET SAFETY OFF
			SET EXACT OFF
			SET STATUS BAR ON
			SET SYSMENU OFF
			SET ENGINEBEHAVIOR 70
			_VFP.AUTOYIELD = .lAutoYield
			.cCOMPUTERNAME = GETENV("COMPUTERNAME")
			.cUSERNAME = GETENV("USERNAME")
			IF .lTestMode THEN
				.cLogFile = 'F:\UTIL\CITIPOSPAYINPROC\LOGFILES\CitiPosPayImportProcess_LOG_TESTMODE.txt'
			ENDIF
			.lLoggingIsOn = .lLoggingIsOn AND NOT EMPTY(.cLogFile)
		ENDWITH
	ENDFUNC


	FUNCTION DESTROY
		WITH THIS
			IF .lLoggingIsOn  THEN
				SET ALTERNATE OFF
				SET ALTERNATE TO
			ENDIF
		ENDWITH
		DODEFAULT()
	ENDFUNC


	FUNCTION MAIN
		WITH THIS

			TRY
				LOCAL lnNumberOfErrors, laFiles[1,5], laFilesSorted[1,6]
				LOCAL lnNumFiles, lnCurrentFile, lcSourceFile, lcArchiveFile, lcTargetFile
				LOCAL lcEDISourceFile, lcEDIArchiveFile, lcOutstrx, lcDelimChar, llArchived
				LOCAL lcRootFileName, lnHandle, lcLine

				lnNumberOfErrors = 0

				IF .lLoggingIsOn THEN
					SET ALTERNATE TO (.cLogFile) ADDITIVE
					SET ALTERNATE ON
				ENDIF

				.TrackProgress('Citi Pos Pay Import Process process started....', LOGIT+SENDIT+NOWAITIT)
				.TrackProgress('COMPUTERNAME = ' + .cCOMPUTERNAME, LOGIT+SENDIT)
				.TrackProgress('USERNAME = ' + .cUSERNAME, LOGIT+SENDIT)
				IF .lTestMode THEN
					.TrackProgress('TEST MODE', LOGIT+SENDIT)
				ENDIF

				.TrackProgress('.cInputFolder = ' + .cInputFolder, LOGIT+SENDIT)
				.TrackProgress('.cOutputFolder = ' + .cOutputFolder, LOGIT+SENDIT)
				.TrackProgress('.cArchiveFolder = ' + .cArchiveFolder, LOGIT+SENDIT)

				***************************************************************************************************************
				* process any files in the input folder.
				***************************************************************************************************************

				* OPEN DATA TABLES
				IF .lTestMode THEN
					* test data
					USE F:\UTIL\CITIPOSPAYINPROC\TESTDATA\TRANSDET IN 0 ALIAS TRANSDET
					USE F:\UTIL\CITIPOSPAYINPROC\TESTDATA\TRANSLOG IN 0 ALIAS TRANSLOG
					*USE F:\UTIL\CITIPOSPAYINPROC\TESTDATA\SYSDATA IN 0 ALIAS SYSDATA

				ELSE
					* live data
					USE F:\BOA\DATA\TRANSDET IN 0 ALIAS TRANSDET
					USE F:\BOA\DATA\TRANSDET IN 0 ALIAS TRANSLOG
					*USE F:\BOA\DATA\TRANSDET IN 0 ALIAS SYSDATA
				ENDIF

				lnNumFiles = ADIR(laFiles,(.cInputFolder + "CDA*.TXT"))

				IF lnNumFiles > 0 THEN

					* sort file list by date/time
					.SortArrayByDateTime(@laFiles, @laFilesSorted)

					FOR lnCurrentFile = 1 TO lnNumFiles

						lcRootFileName = laFilesSorted[lnCurrentFile,1]

						lcEDISourceFile = .cInputFolder + lcRootFileName
						lcTargetFile = .cOutputFolder + lcRootFileName
						lcEDIArchiveFile = .cArchiveFolder + lcRootFileName

						.TrackProgress("Processing EDI file: " + lcEDISourceFile,LOGIT+SENDIT+NOWAITIT)

						CREATE CURSOR CURINPUT( ACCTNUM C(8), CHECK_AMT N(11), CHECK_NO C(10), CHECK_DATE D, STATUS_ID C(1), LINENUM I )

						lnHandle = FOPEN(lcEDISourceFile)
						m.LINENUM = 0

						DO WHILE !FEOF(lnHandle)

							lcLine = FGETS(lnHandle)

							m.LINENUM = m.LINENUM + 1

							* this parsing will change!!!!
							m.ACCTNUM = SUBSTR(lcLine,1,8)
							m.CHECK_AMT  = VAL(SUBSTR(lcLine,9,11))
							m.CHECK_NO = SUBSTR(lcLine,20,10)
							* parse YYMMDD date
							m.CHECK_DATE = CTOD( SUBSTR(lcLine,30,2) + "/" + SUBSTR(lcLine,32,2) + "/" + SUBSTR(lcLine,34,2) )
							m.NOTUSED = SUBSTR(lcLine,36,30)
							m.STATUS_ID = SUBSTR(lcLine,66,1)

							INSERT INTO CURINPUT FROM MEMVAR

						ENDDO

						=FCLOSE(lnHandle)

*!*							SELECT CURINPUT
*!*							BROWSE

						* identify bad data

						SELECT CURINPUT
						SCAN
							* verify account #
							IF NOT INLIST(CURINPUT.ACCTNUM,"38859443","38859478","38859486","38859451") THEN
								lcErrorMessage = ;
									"--------------------------------------------------------------" + CRLF + ;
									"!!! Invalid Account #: " + CURINPUT.ACCTNUM + " at line " + TRANSFORM(CURINPUT.LINENUM) + CRLF + ;
									"--------------------------------------------------------------"
								.TrackProgress( lcErrorMessage,LOGIT+SENDIT )
							ENDIF
						ENDSCAN

						** process each account # to produce one output file each
						*.MakeOutputFile( "38824399" )

						*!*
						*!*
						.MakeOutputFile( "38859443" )
						.MakeOutputFile( "38859478" )
						.MakeOutputFile( "38859486" )
						.MakeOutputFile( "38859451" )


						IF NOT .lTestMode THEN
							* archive and then delete the file.
							llArchived = FILE(lcEDIArchiveFile)
							IF llArchived THEN
								RUN ATTRIB -R &lcEDIArchiveFile.
								.TrackProgress('   Used ATTRIB -R for re-archiving : ' + lcEDISourceFile,LOGIT+SENDIT)
							ENDIF

							* archive source file
							COPY FILE (lcEDISourceFile) TO (lcEDIArchiveFile)

							* delete original source file if the copy was made...
							IF FILE(lcEDIArchiveFile) THEN
								DELETE FILE(lcEDISourceFile)
								.TrackProgress("Archived file: " + lcEDISourceFile + ' to ' + .cArchiveFolder,LOGIT+SENDIT)
							ELSE
								.TrackProgress("!! There were errors archiving file: " + lcEDISourceFile,LOGIT+SENDIT)
							ENDIF
						ENDIF

						*.TrackProgress('=========================================================', LOGIT+SENDIT)

					ENDFOR && lnCurrentFile = 1 TO lnNumFiles


				ELSE
					.TrackProgress('Found no files in the input folder', LOGIT+SENDIT)
					* don't send an email if we processed no files -- to support running this proc every half hour per Doug.
				ENDIF

				.TrackProgress('Citi Pos Pay Import process ended normally!',LOGIT+SENDIT)

				*!*					.cBodyText = .cISAList + CRLF + CRLF + .cTopBodyText + CRLF + ;
				*!*						"<Report log follows>" + CRLF + CRLF + .cBodyText

			CATCH TO loError

				.TrackProgress('There was an error.',LOGIT+SENDIT)
				.TrackProgress('The Error # is: ' + TRANSFORM(loError.ERRORNO), LOGIT+SENDIT)
				.TrackProgress('The Error Message is: ' + TRANSFORM(loError.MESSAGE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred in Program: ' + TRANSFORM(loError.PROCEDURE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred at line #: ' + TRANSFORM(loError.LINENO), LOGIT+SENDIT)
				.TrackProgress(.cThrowDescription, LOGIT+SENDIT)
				lnNumberOfErrors = lnNumberOfErrors + 1
				CLOSE DATA
				IF .lTestMode THEN
					.cSendTo = 'mbennett@fmiint.com'
				ELSE
					.cSendTo = 'mbennett@fmiint.com,pgaidis@fmiint.com'
				ENDIF

			ENDTRY

			CLOSE DATABASES ALL

			WAIT CLEAR
			***************** INTERNAL email results ******************************
			.TrackProgress('About to send status email.',LOGIT)
			.TrackProgress('The logfile is: ' + .cLogFile, SENDIT)
			.TrackProgress('Citi Pos Pay Import Process process started: ' + .cStartTime, LOGIT+SENDIT)
			.TrackProgress('Citi Pos Pay Import Process process finished: ' + TTOC(DATETIME()), LOGIT+SENDIT)

			IF .lSendInternalEmailIsOn THEN
				DO FORM dartmail2 WITH .cSendTo,.cFrom,.cSubject,.cCC,.cAttach,.cBodyText,"A"
				.TrackProgress('Sent status email.',LOGIT)
			ELSE
				.TrackProgress('Did not send status email: THIS.lSendInternalEmailIsOn = FALSE.',LOGIT)
			ENDIF

		ENDWITH
		RETURN
	ENDFUNC && main


	PROCEDURE MakeOutputFile
		LPARAMETERS tcAccountNumber
		WITH THIS
			LOCAL lcAccountNumber, lcCheckAmount, lcCheckNumber, lcCheckIssueDate, lcAdditionalData, lcVoidIndicator, lcPayeeName1, lcPayeeName2, lcFiller
			LOCAL lcOutputLine, lnResult, lcTrailerIndicator, lnTrailerAmount, lnTrailerItemCount, lnPaymentTotal, lnPaymentCount
			LOCAL lnVoidCount, lnVoidTotal, cline, lnPaymentPlusVoidsCount, lcDTSuffix

			.cAcctnumber = tcAccountNumber
			.TrackProgress("Processing Account #: " + .cAcctnumber,LOGIT+SENDIT)

			* see if there is any data for this account #
			IF USED('CURISDATA') THEN
				USE IN CURISDATA
			ENDIF

			SELECT * ;
				FROM CURINPUT ;
				INTO CURSOR CURISDATA ;
				WHERE ACCTNUM = .cAcctnumber

			IF USED('CURISDATA') AND (NOT EOF('CURISDATA')) THEN
				* we have data - proceed

				IF USED('CURDATES') THEN
					USE IN CURDATES
				ENDIF

				SELECT MIN(CHECK_DATE) AS StartDate, MAX(CHECK_DATE) AS EndDate ;
					FROM CURINPUT ;
					INTO CURSOR CURDATES ;
					WHERE ACCTNUM = .cAcctnumber

				SELECT CURDATES
				.StartDate = CURDATES.StartDate
				.EndDate = CURDATES.EndDate

				* create translog record
				SELECT TRANSLOG
				APPEND BLANK
				REPLACE TRANSLOG.StartDate WITH .StartDate, ;
					TRANSLOG.EndDate WITH .EndDate, ;
					TRANSLOG.sentto WITH "CITIBANK", ;
					TRANSLOG.company WITH .company IN TRANSLOG

				lcDTSuffix = STRTRAN(TTOC(DATETIME()),"/","_")
				lcDTSuffix = STRTRAN(lcDTSuffix," ","_")
				lcDTSuffix = STRTRAN(lcDTSuffix,":","")

				DO CASE
					CASE .cAcctnumber = "38859443"
						.company = "GENESIS-NY"
					CASE .cAcctnumber = "38859478"
						.company = "GENESIS-IL"
					CASE .cAcctnumber = "38859486"
						.company = "GENESIS-TX"
					CASE .cAcctnumber = "38859451"
						.company = "GENESIS-CA"
					OTHERWISE
						.TrackProgress('ERROR: unexpected Acct #: ' + .cAcctnumber,LOGIT+SENDIT)
						RETURN
				ENDCASE
				
				.prefix = RIGHT(.cAcctnumber,4)
				.cTransFileRoot = 'ISS' + .prefix + 'TRANSFILE_' + lcDTSuffix + '.TXT'

				.cTRANSFILENAME = 'C:\TEMPFOX\' + .cTransFileRoot

				.TrackProgress("Creating CDA File: " + .cTRANSFILENAME,LOGIT+SENDIT)

				IF FILE(.cTRANSFILENAME)      && if file exists delete it
					DELETE FILE (.cTRANSFILENAME)
				ENDIF

				.nhandle = FCREATE(.cTRANSFILENAME)  &&  create it

				IF .nhandle < 0    && Check for error opening file
					.cThrowDescription = 'ERROR FOPENing output file!'
					THROW
				ENDIF

				*!**********************************************************************************************
				*!*	CitiBank Controlled Disbursement Issuance File Specifications
				*!*	When transmitting 1 file per Account #, which is what we will be doing, write out 1 Account Detail record per check,
				*!*	then a single summary Trailer Record.

				*!* Account Detail Record:
				*!*	 The layout is
				*!*				 Account #					1-8
				*!*				 Check Amount				9-19
				*!*				 Check #					20-29
				*!*				 Check Issue Date	YYMMDD 	30-35
				*!*				 Additional Data (optional)	36-65
				*!*				 Void Indicator (optional)	66-66
				*** REMAINING FIELDS LEFT BLANK PER CITIBANK 10/1/11 M.B.
				*!*				 Payee Name 1 (optional)	67-126
				*!*				 Payee Name 2 (optional)	127-186
				*!*				 Filler						187-200
				*!*
				*!*	 Trailer Record:
				*!*				Trailer Indicator		1-1
				*!*				Trailer Amount			2-12
				*!*				Trailer Item Count		13-22
				*!*				Filler					23-200
				*!**********************************************************************************************

				SELECT CURINPUT
				SCAN FOR ACCTNUM = .cAcctnumber
					lcAccountNumber = PADR(ALLTRIM(.cAcctnumber),8,'0')
					lcCheckAmount = PADL(.convertnumbers(IIF(CURINPUT.CHECK_AMT < 0,CURINPUT.CHECK_AMT * -1,CURINPUT.CHECK_AMT),"CKAMT"),11,'0')
					lcCheckNumber = PADL(CURINPUT.CHECK_NO,10,'0')
					lcCheckIssueDate = RIGHT(TRANSFORM(YEAR(CURINPUT.CHECK_DATE)),2) + PADL(MONTH(CURINPUT.CHECK_DATE),2,'0') + PADL(DAY(CURINPUT.CHECK_DATE),2,'0')
					lcCheckIssueDate = PADL(lcCheckIssueDate,6,' ')
					lcAdditionalData = SPACE(30)
					lcVoidIndicator = IIF(CURINPUT.STATUS_ID='V','V',' ')
					* Payee names removed per CitiBank, bringing total reclength down from 200 to 80 8/4/10 MB
					*!*			lcPayeeName1 = PADR(checkreg.payee_name,60,' ')
					*!*			lcPayeeName2 = SPACE(60)
					lcPayeeName1 = ''
					lcPayeeName2 = ''
					lcFiller = SPACE(14)
					lcOutputLine = lcAccountNumber + lcCheckAmount + lcCheckNumber + lcCheckIssueDate + ;
						lcAdditionalData + lcVoidIndicator + lcPayeeName1 + lcPayeeName2 + lcFiller + CRLF
					lnResult = FWRITE(.nhandle,lcOutputLine)
					IF lnResult < 0
						=FCLOSE(.nhandle)
						.cThrowDescription = "FWRITE() error in MakeOutputFile() detail record SCAN"
						THROW
					ENDIF
				ENDSCAN

				* calc summary totals and write Trailer Record

				* overal stats for all transactions, for Trailer record and form properties
				SELECT CURINPUT
				SUM CHECK_AMT FOR ACCTNUM = .cAcctnumber TO lnTrailerAmount 
				COUNT FOR ACCTNUM = .cAcctnumber TO lnTrailerItemCount
				STORE STR(lnTrailerAmount * -1,12,2) TO .cTotalAmt
				STORE STR(lnTrailerItemCount) TO .ctotalitems

				* payment stats for form properties
				SELECT CURINPUT
				SUM CHECK_AMT FOR ACCTNUM = .cAcctnumber AND STATUS_ID = 'P' TO lnPaymentTotal
				COUNT FOR ACCTNUM = .cAcctnumber AND STATUS_ID = 'P' TO lnPaymentCount
				STORE STR(lnPaymentTotal * -1, 12, 2) TO .cPaymentamt
				STORE STR(lnPaymentCount) TO .cpaymentitems

				* void stats for form properties
				SELECT CURINPUT
				SUM CHECK_AMT FOR ACCTNUM = .cAcctnumber AND STATUS_ID = 'V' TO lnVoidTotal
				COUNT FOR ACCTNUM = .cAcctnumber AND STATUS_ID = 'V' TO lnVoidCount
				STORE STR(lnVoidTotal, 12, 2) TO .cVoidAmt
				STORE STR(lnVoidCount) TO .cVoidItems

				lcTrailerIndicator = 'T'
				lcTrailerAmount = PADL(.convertnumbers(lnTrailerAmount,"CKAMT"),11,'0')
				lcTrailerItemCount = PADL(.convertnumbers(lnTrailerItemCount,"ITEMCTN"),10,'0')
				* reduce reclength to 80 down from 120 per CitiBank 8/4/10 MB
				*lcFiller = SPACE(178)
				lcFiller = SPACE(58)
				lcOutputLine = lcTrailerIndicator + lcTrailerAmount + lcTrailerItemCount + lcFiller + CRLF
				lnResult = FWRITE(.nhandle,lcOutputLine)
				IF lnResult < 0
					=FCLOSE(.nhandle)
					.cThrowDescription = "FWRITE() error in MakeOutputFile() Trailer Record"
					THROW
				ENDIF

				=FCLOSE(.nhandle)
				
				.update_translog('populateoutputfile','YES')

				.UpdateTransdetTable()
				
				.SendData()

			ELSE
				.TrackProgress('NOTE: No checks found for Acct #: ' + .cAcctnumber,LOGIT+SENDIT)
			ENDIF &&  USED('CURISDATA') AND (NOT EOF('CURISDATA')) THEN


			RETURN
		ENDWITH
	ENDPROC  &&  MakeOutputFile


	PROCEDURE SendData
		WITH THIS
			LOCAL lcTRANSFILENAME, lcString, lcOutputFile
			LOCAL lcFileRoot, lcReportname, lcFilename, lcSendTo, lcFrom, lcSubject, lcCC, lcAttach, lcMessage

			IF USED('temp') THEN
				USE IN temp
			ENDIF
			IF USED('checks') THEN
				USE IN checks
			ENDIF

			CREATE CURSOR temp (s C(100))
			CREATE CURSOR checks (ctype C(1), Checknum C(10),checkamt N(12,2), payee C(20), checkdate C(8))

			lcTRANSFILENAME = .cTRANSFILENAME

			* create a cursor for the PDF report
			SELECT temp
			APPEND FROM &lcTRANSFILENAME TYPE SDF
			GOTO TOP
			* exclude Trailer record, which begins with 'T' from scan
			* removed payee per CitiBank 8/4/10 MB
			SCAN FOR LEFT(temp.s,1) <> 'T'
				SELECT checks
				APPEND BLANK
				REPLACE ctype WITH IIF(SUBSTR(temp.s,66,1) = 'V','Z','I'), ;
					Checknum  WITH SUBSTR(temp.s,20,10), ;
					checkamt  WITH VAL(SUBSTR(temp.s,09,11))/100, ;
					checkdate WITH SUBSTR(temp.s,30,2)+"/"+SUBSTR(temp.s,32,2)+"/"+SUBSTR(temp.s,34,2)
			ENDSCAN
			*!*			payee     WITH SUBSTR(temp.s,67,20), ;


			SELECT checks
			INDEX ON Checknum TAG cknum

			lcOutputFile = .cOutputFolder + .cTRANSFILEROOT
			
			.TrackProgress("Copying Output File to : " + lcOutputFile,LOGIT+SENDIT)


			**************************************************************
			*!* Send file to ftp output directory for poller to pickup
			**************************************************************
			* 'copy file' changes case of filename to lower, so use alternate method from Joe B.
			* cStr = Filetostr(filename), then strtofile(cStr(upper(filename)) to another folder does it for Joe B.
			*COPY FILE (lcTRANSFILENAME) TO (lcOutputFile)
			lcString = FILETOSTR(lcTRANSFILENAME)
			=STRTOFILE(lcString,UPPER(lcOutputFile))
			
			IF NOT FILE(lcOutputFile) THEN
				.TrackProgress("!!---------------------------------------------!!" + lcOutputFile,LOGIT+SENDIT)
				.TrackProgress("ERROR creating Output File: " + lcOutputFile,LOGIT+SENDIT)
				.TrackProgress("!!---------------------------------------------!!" + lcOutputFile,LOGIT+SENDIT)
			ENDIF


			* update translog
			.update_translog('COMPLETED',"YES")
			.update_translog('cprefix',.prefix)
			.update_translog('ctransfile',.cTRANSFILEROOT)
			.update_translog('nrecssent',VAL(.ctotalitems))
			.update_translog('namtsent',ABS(VAL(.cTotalAmt)))
			.update_translog('insdttm',DATETIME())

			* SEND EMAIL
			
			SELECT checks
			GOTO TOP

			lcFileRoot = 'c:\tempfox\CitiBankPosPayTransferReport' + .cTRANSFILEROOT

			* CREATE THE REGULAR pdf REPORT
			lcReportname = "CitiBankPosPay_rpt"
			lcFilename = lcFileRoot + '.PDF'
			lcAttach = lcFilename
			IF !rpt2pdf(lcReportname,lcFilename)
				lcAttach = ""
				.cThrowDescription = "rpt2pdf() returned .F. in SendData()"
				THROW
			ENDIF

			lcMessage = "Customer Input Sheet" + CHR(13) + ;
				"Issue File Transmittal Form" + CHR(13) + ;
				"Check Disbursement Reconcilement"+ CHR(13)+;
				"Transmittal Date" + SPACE(2)+ DTOC(DATE())+ CHR(13) + ;
				"Account Name: " + .company + CHR(13) + ;
				"Account Number: " + .cAcctnumber + CHR(13)+ CHR(13) + ;
				"Items Sent:" + CHR(13) + ;
				.cpaymentitems + "    " + "Issue Total:   $ " + .cPaymentamt + CHR(13) + ;
				.cVoidItems + "    " + "Void Total:    $ " + .cVoidAmt + CHR(13) + ;
				.ctotalitems + "    " + "Account Total: $ " + .cTotalAmt + CHR(13) + CHR(13) + ;
				"Customer's Phone #: 732-750-9000 x143" + CHR(13) + ;
				"Contact Name:       Paul Gaidis" + CHR(13) + ;
				"Today's Date:     " + DTOC(DATE())

			lcFrom = 'FMI System OPS <fmi-transload-ops@fmiint.com>'
*!*				lcCC = 'pgaidis@fmiint.com,mbennett@fmiint.com'
			lcSubject  = "CitiBankPosPay Transfer Report for " + .company
*!*					IF INLIST(.company,"OTI-LA","OTI-MI","OTI-NY","SCM-NY","SCM-LA","SML-LA") THEN
*!*						lcSendTo = "kwaldrip@fmiint.com,joannascott@summitgl.com"
*!*					ELSE
*!*						lcSendTo = 'kwaldrip@fmiint.com'
*!*					ENDIF
*!*					IF THISFORM.istesting THEN
				lcSendTo   = 'mbennett@fmiint.com'
				lcCC       = ''
*!*					ENDIF

				DO FORM dartmail2 WITH lcSendTo, lcFrom, lcSubject, lcCC, lcAttach, lcMessage, "A"

			ENDWITH
		ENDPROC && SendData


*!*		PROCEDURE LOGERROR
*!*			LPARAMETERS tcErrorMessage
*!*			WITH THIS
*!*				.TrackProgress(tcErrorMessage,LOGIT+SENDIT)

*!*				RETURN
*!*			ENDWITH
*!*		ENDPROC  &&  LOGERROR


	PROCEDURE TrackProgress
		* do any combination of Wait Window, writing to logfile, and adding to body of email,
		* based on nFlags parameter.
		LPARAMETERS tcExpression, tnFlags
		WITH THIS
			IF BITAND(tnFlags,LOGIT) = LOGIT THEN
				IF .lLoggingIsOn THEN
					?
					? .cProcessName + "  " + TTOC(DATETIME()) + ": " + tcExpression
				ENDIF
			ENDIF
			IF .lWaitWindowIsOn THEN
				IF BITAND(tnFlags,NOWAITIT) = NOWAITIT THEN
					WAIT WINDOW tcExpression NOWAIT
				ENDIF
				IF BITAND(tnFlags,WAITIT) = WAITIT THEN
					WAIT WINDOW tcExpression TIMEOUT .nWaitWindowTimeout
				ENDIF
			ENDIF
			IF BITAND(tnFlags,SENDIT) = SENDIT THEN
				IF .lSendInternalEmailIsOn THEN
					.cBodyText = .cBodyText + tcExpression + CRLF + CRLF
				ENDIF
			ENDIF
		ENDWITH
	ENDPROC  &&  TrackProgress


	PROCEDURE SortArrayByDateTime
		* expects taInArray[] to be the result of an ADIR()
		LPARAMETERS taInArray, taOutArray
		EXTERNAL ARRAY taInArray, taOutArray
		LOCAL lnRows, lnCols, I, j
		* extending taInArray[] to process the files in date/time order
		lnRows = ALEN(taInArray,1)
		lnCols = ALEN(taInArray,2)
		* create taOutArray with one more column
		DIMENSION taOutArray[lnRows,lnCols + 1]
		* and fill it with taInArray columns AND (filedate + filetime) in new column
		* ( ACOPY() doesn't preserve elements properly because of different # of columns )
		FOR I = 1 TO lnRows
			FOR j = 1 TO lnCols
				taOutArray[i,j] = taInArray[i,j]
			ENDFOR
			taOutArray[i,lnCols + 1] = DTOS(taInArray[i,3]) + "_" + taInArray[i,4]
		ENDFOR
		* sort taOutArray on date/time column
		=ASORT(taOutArray,6)
	ENDPROC


	FUNCTION convertnumbers
		**converts check amount into the format that keybank wants
		LPARAMETERS tnNumber,tcType
		LOCAL lcRetVal

		DO CASE
			CASE UPPER(tcType)="CKAMT"
				lcRetVal=STR(tnNumber,11,2) && convert into a string)
				lcRetVal=STRTRAN(lcRetVal,".") &&get rid the the decimal points
				lcRetVal=STRTRAN(lcRetVal,"-")  && get rid of the negative sign
				lcRetVal=PADL(ALLTRIM(lcRetVal),11,"0")   && make length 11 padded with 0 on the left
			CASE UPPER(tcType)="ITEMCTN"
				lcRetVal=STR(tnNumber,9,0) && convert into a string)
				lcRetVal=STRTRAN(lcRetVal,".") &&get rid the the decimal points
				lcRetVal=STRTRAN(lcRetVal,"-")  && get rid of the negative sign
				lcRetVal=PADL(ALLTRIM(lcRetVal),10,"0")   && make length 10 for item count
		ENDCASE

		RETURN lcRetVal
	ENDFUNC


	FUNCTION FormatDate
		LPARAMETERS tdDate
		LOCAL lcYear, lcMonth, lcDay, lcRetVal
		** get year
		lcYear = RIGHT(ALLTRIM(STR(YEAR(tdDate))),2)

		** get month
		lcMonth = IIF(LEN(ALLTRIM(STR(MONTH(tdDate))))=1,"0"+ALLTRIM(STR(MONTH(tdDate))),ALLTRIM(STR(MONTH(tdDate))))

		** get day
		lcDay = IIF(LEN(ALLTRIM(STR(DAY(tdDate))))=1,"0"+ALLTRIM(STR(DAY(tdDate))),ALLTRIM(STR(DAY(tdDate))))

		lcRetVal = lcMonth + "-" + lcDay + lcYear

		RETURN lcRetVal
	ENDFUNC


	FUNCTION RemoveZero
		LPARAMETERS tcExpression
		LOCAL lnTotal, lnPos, lcRetVal, lnLen
		lcRetVal = ''

		lnLen = LEN(tcExpression)

		FOR lnPos = 1 TO lnLen
			IF SUBSTR(tcExpression,lnPos,1)='0'
				lnTotal=lnPos
			ELSE
				lcRetVal = STRTRAN(tcExpression,'0','',1,lnPos-1)
				EXIT
			ENDIF
		ENDFOR

		RETURN lcRetVal
	ENDFUNC


	PROCEDURE UpdateTransdetTable
		WITH THIS

			PRIVATE m.company, m.prefix, m.ACCTNUM, m.Checknum, m.checkamt, m.VOIDIND, m.processdtm, m.ctransfile

			*!*	USE F:\BOA\DATA\TRANSDET IN 0 ALIAS TRANSDET

			m.ACCTNUM = PADL(ALLTRIM(.cAcctnumber),10,'0')
			m.company = .company
			m.prefix = .prefix
			m.processdtm = DATETIME()
			m.ctransfile = ALLTRIM(.cTransFileRoot)

			SELECT CURINPUT
			SCAN FOR ACCTNUM = .cAcctnumber
				m.checkamt = CURINPUT.CHECK_AMT
				m.Checknum = PADL(CURINPUT.CHECK_NO,10,'0')
				m.issuedt = CURINPUT.CHECK_DATE
				m.VOIDIND = IIF(CURINPUT.STATUS_ID='V','V',' ')

				SELECT TRANSDET
				LOCATE FOR (ACCTNUM == M.ACCTNUM) AND (Checknum == M.Checknum)
				IF FOUND() THEN
					* must be a resend - update non-key fields
					REPLACE TRANSDET.processdtm WITH m.processdtm, ;
						TRANSDET.company WITH m.company, ;
						TRANSDET.prefix WITH m.prefix, ;
						TRANSDET.ctransfile WITH m.ctransfile, ;
						TRANSDET.checkamt WITH ABS(m.checkamt), ;
						TRANSDET.issuedt WITH m.issuedt, ;
						TRANSDET.VOIDIND WITH m.VOIDIND
				ELSE
					* a new check trans, add a record
					INSERT INTO TRANSDET FROM MEMVAR
				ENDIF
			ENDSCAN

			RETURN
		ENDWITH
	ENDPROC  &&  UpdateTransDetTable


	PROCEDURE update_translog
		LPARAMETERS pField,pData
*!*			LOCAL cSelect
*!*			cSelect=ALIAS()

*!*			SELECT TRANSLOG
*!*			REPLACE &pField WITH pData
		REPLACE &pField WITH pData IN TRANSLOG
*!*			SELECT &cSelect
		RETURN
	ENDPROC




ENDDEFINE
