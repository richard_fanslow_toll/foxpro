parameters xfilter

create cursor xrpt ( ;
	projno c(7), ;
	custrefno c(15), ;
	wo_num n(7), ;
	wonum1 n(7), ;
	wonum2 n(7), ;
	wonum3 n(7), ;
	wonum4 n(7), ;
	wonum5 n(7), ;
	wonum6 n(7), ;
	wonum7 n(7), ;
	wonum8 n(7), ;
	wonum9 n(7), ;
	wonum10 n(7), ;
	wonum11 n(7), ;
	wonum12 n(7), ;
	wonum13 n(7), ;
	wonum14 n(7), ;
	wonum15 n(7), ;
	wonum16 n(7), ;
	wonum17 n(7), ;
	wonum18 n(7), ;
	wonum19 n(7), ;
	putwonum n(7), ;
	invnum c(8), ;
	startdt d, ;
	completeby d, ;
	completed d, ;
	pickticket c(15), ;
	wotype c(15),;
	accountid n(4), ;
	acctname c(30), ;
	consignee c(20), ;
	totrevenue n(9,2), ;
	hours n(4), ;
	samshours n(6,1), ;
	cartons n(5), ;
	unitqty n(5), ;
	comment m, ;
	spwipcomm m, ;
	compon c(28), ;
	jobdesc c(60), ;
	qty n(8,1), ;
	qtytype c(10), ;
	rate n(8,3), ;
	totcharge n(8,2), ;
	compneed l, ;
	cncomment c(254))

select xlist
scan for &xfilter
	xsqlexec("select * from project where projectid="+transform(xlist.projectid),"xproject",,"wh")
	scatter memvar memo

	m.completeby=nul2empty(m.completebydt)
	m.completed=nul2empty(m.completeddt)
	m.compdt=nul2empty(m.compdt)
	m.startdt=nul2empty(m.startdt)

	if !m.compneed
		m.compon="NO"
	else
		m.compon="YES - "+m.component+" "+iif(!empty(m.compdt),dtoc(m.compdt),"")
	endif

	xsqlexec("select * from projdet where projectid="+transform(xlist.projectid),"xtemp",,"wh")
	
	if reccount()=0
		scatter memvar blank
		m.jobdesc=""
		m.qtytype=""
		insert into xrpt from memvar
	else
		scan
			scatter memvar
			m.jobdesc=description
			m.startdt=nul2empty(m.startdt)
			m.completeby=nul2empty(m.completeby)
			m.completed=nul2empty(m.completed)
			insert into xrpt from memvar
			m.startdt={}
			m.completeby={}
		endscan
	endif
endscan

select xrpt
scan
	if xsqlexec("select * from outwolog where wo_num="+transform(xrpt.wo_num),,,"wh") # 0
		if outwolog.picknpack
			replace xrpt.wotype with "Pick/Pack"
		else
			replace xrpt.wotype with "Casepack"
		endif
	endif
endscan
