* Create spreadsheet of Hours summary for Temp Workers for the previous pay period.
* BY DAY, NOT ROLLED UP FOR THE WEEK!
** Meant to be run after hours are reviewed and cleaned up; Monday pm or Tuesday am.
* Modified 12/2011 to also report on Hourly San Pedro employees for Jaime Barba.
* EXEs go in F:\UTIL\KRONOS\


ON ERROR DO errHandlerKTTR WITH ERROR( ), MESSAGE( ), MESSAGE(1), PROGRAM( ), LINENO( ), tcTempCo, tcMode

*!*	runack("KRONOSCADRIVERTIMECONVERSION")

LOCAL lnError, lcProcessName

DECLARE INTEGER CreateMutex IN Win32API ;
	STRING @lpMutexAttributes, ;
	STRING bInitialOwner, ;
	STRING @lpName

DECLARE INTEGER GetLastError IN Win32API

* if running in EXE check to see if already running
IF _VFP.STARTMODE > 0 THEN
	lcProcessName = "KRONOSCADRIVERTIMECONVERSION"
	CreateMutex(0,CHR(1),@lcProcessName)
	lnError = GetLastError()
	IF lnError = 183
		WAIT WINDOW TIMEOUT 10 "KRONOS CA DRIVER TIME CONVERSION process is already running..."
		RETURN .F.
	ENDIF
ENDIF

*!*	utilsetup("KRONOSCADRIVERTIMECONVERSION")

LOCAL loKRONOSCADRIVERTIMECONVERSION

loKRONOSCADRIVERTIMECONVERSION = CREATEOBJECT('KRONOSCADRIVERTIMECONVERSION')

loKRONOSCADRIVERTIMECONVERSION.MAIN()

ON ERROR DO errHandlerKTTR WITH ERROR( ), MESSAGE( ), MESSAGE(1), PROGRAM( ), LINENO( ), tcTempCo, tcMode

*!*	schedupdate()

CLOSE DATABASES ALL


WAIT WINDOW TIMEOUT 60  && TO FORCE THIS PROCESS TO RUN FOR > 1 MINUTE, I THINK IF IT RUNS WITHIN 1 MINUTE STM RUNS IT TWICE

RETURN

#DEFINE LOGIT 1
#DEFINE WAITIT 2
#DEFINE NOWAITIT 4
#DEFINE SENDIT 8
#DEFINE NOCRLF 16
#DEFINE CRLF CHR(13) + CHR(10)
#DEFINE LF CHR(10)
#DEFINE RETURN_DATA_MANDATORY .F.
#DEFINE RETURN_DATA_NOT_MANDATORY .T.
#DEFINE DAILY_OT_MULTIPLIER 1.5
#DEFINE DAILY_OT20_MULTIPLIER 2.0
#DEFINE ADJUSTMENT_CODE "Z"

DEFINE CLASS KRONOSCADRIVERTIMECONVERSION AS CUSTOM

	cProcessName = 'KRONOSCADRIVERTIMECONVERSION'

	lTestMode = .T.  && .F. FOR NORMAL OPERATION, .T. TO SUPPRESS ALL BUT DEVELOPER EMAILS AND CHANGE INPUT TABLE, ETC.

	lAutoYield = .T.

	cStartTime = TTOC(DATETIME())

	* mode properties
	cMode = "WEEKLY"

	* connection properties
	nSQLHandle = 0

	* file properties
	nFileHandle = 0


	* date properties
	nPeriodEndDayNum = 7

	* hours discrepancy testing props
	nMinHoursTest = 2
	nMaxHoursTest = 10

	* date properties
	dtNow = DATETIME()

	dToday = DATE()
	* to force different pay weeks
	*dToday = {^2013-02-05}

	dEndDate = {}
	dStartDate = {}

	cToday = DTOC(DATE())

	* wait window properties
	nWaitWindowTimeout = 2
	lWaitWindowIsOn = .T.

	* logfile properties
	lLoggingIsOn = .T.
	cLogFile = 'F:\UTIL\KRONOS\Logfiles\KRONOSCADRIVERTIMECONVERSION_log.txt'

	* INTERNAL email properties
	lSendInternalEmailIsOn = .T.
	cFrom = 'FMI Corporate <fmicorporate@fmiint.com>'
	cSendTo = 'mbennett@fmiint.com'
	cCC = ''
	cSubject = 'CA Driver Time Conversion'
	cAttach = ''
	cBodyText = ''

	cCOMPUTERNAME = ''
	cUSERNAME = ''

	FUNCTION INIT
		IF NOT DODEFAULT()
			RETURN .F.
		ENDIF
		WITH THIS
			CLEAR
			*SET RESOURCE OFF
			CLOSE DATA
			SET CENTURY ON
			SET DATE YMD
			SET HOURS TO 24
			SET ANSI ON
			SET TALK OFF
			SET DELETED ON
			SET CONSOLE OFF
			SET EXCLUSIVE OFF
			SET SAFETY OFF
			SET EXACT OFF
			SET STATUS BAR ON
			SET SYSMENU OFF
			SET ENGINEBEHAVIOR 70
			SET DECIMALS TO 2
			
			SET FIXED ON
			
			_VFP.AUTOYIELD = .lAutoYield
			.cCOMPUTERNAME = GETENV("COMPUTERNAME")
			.cUSERNAME = GETENV("USERNAME")
		ENDWITH
	ENDFUNC


	FUNCTION DESTROY
		WITH THIS
			IF .lLoggingIsOn  THEN
				SET ALTERNATE OFF
				SET ALTERNATE TO
			ENDIF
		ENDWITH
		DODEFAULT()
	ENDFUNC


	FUNCTION MAIN
		LPARAMETERS tcTempCo, tcMode
		WITH THIS
			LOCAL lnNumberOfErrors, lcSQL, lcSQL2, lcSQL3, lcSQL4, lcSQL5, loError, lcTopBodyText, llValid
			LOCAL lcXLFileName, ldStartDate, ldEndDate, lcStartDate, lcEndDate, ldDate
			LOCAL lcSQLStartDate, lcSQLEndDate, lcXLFileName, ldYesterday
			LOCAL oExcel, oWorkbook, oWorksheet, lnRow, lcRow, lnStartRow, lcStartRow, lnEndRow, lnPP, lcPP, lnNN, lcNN, lcJJ
			LOCAL lcColsToProcess, lnColumn, lcBoldColumn, i, j, lcBoldRow, luCellValue, lcTargetDirectory, ldPayDate, lcFiletoSaveAs
			LOCAL lcFileDate, lcDivision, lcTitle, llSaveAgain, lcPayCode, lcSpecialWhere
			LOCAL lcSQLToday, lcSQLTomorrow, ldToday, ldTomorrow, lcExcluded9999s, lcFoxProTimeReviewerTable, lcMissingApprovals
			LOCAL lcAuditSubject, oWorkbookAUDIT, oWorksheetAUDIT, lcSendTo, lcCC, lcBodyText, lcXLFileNameAUDIT, lcTotalRow
			LOCAL lcTRWhere, lnStraightHours, lnStraightDollars, lnOTDollars, lcFilePrefix
			LOCAL ldPass2Date, lcPrevFileNum, lnPrevFileNumStartRow, lnPrevFileNumEndRow, lcPrevFileNumStartRow, lcPrevFileNumEndRow
			LOCAL lcOrderBy, lcPrevDept, lnPrevDeptStartRow, lnPrevDeptEndRow, lcPrevDeptStartRow, lcPrevDeptEndRow, lnTempMarkupREG
			LOCAL lcPrevDivision, lnPrevDivisionStartRow, lcPrevDivisionStartRow, lnPrevDivisionEndRow, lcPrevDivisionEndRow
			LOCAL lnShiftDiff, lcTopBodyText, lcXLSummaryFileName, lnWage, lnPay, lcRootPDFFilename, lcChar
			LOCAL ltInpunch, lnTotHours, lcPRIMACCTCHANGED, lcNoBioFileNum, lcUnscannable, lcInvalidRegHours, lnTempMarkupOT
			LOCAL llContOT

			ON ERROR

			TRY
				lnNumberOfErrors = 0
				lcTopBodyText = ''

				*.TrackProgress('==================================================================================================================', LOGIT+SENDIT)
				.TrackProgress('KRONOS CA DRIVER TIME CONVERSION process started....', LOGIT+SENDIT)
				.TrackProgress('COMPUTERNAME = ' + .cCOMPUTERNAME, LOGIT+SENDIT)
				.TrackProgress('USERNAME = ' + .cUSERNAME, LOGIT+SENDIT)
				.TrackProgress('PROJECT = KRONOSCADRIVERTIMECONVERSION', LOGIT+SENDIT)

				IF .lTestMode THEN
					.TrackProgress('                        TEST MODE                        ', LOGIT+SENDIT)
				ENDIF

				.TrackProgress('.nPeriodEndDayNum = ' + TRANSFORM(.nPeriodEndDayNum), LOGIT+SENDIT)

				ldToday = .dToday
				ldTomorrow = ldToday + 1
				ldYesterday = ldToday - 1

				.cToday = DTOC(.dToday)

				* we can go back until we hit a Sunday (DOW = 1) for a Monday-Sunday Pay Period for Temps,
				* based on whether .nPeriodEndDayNum = 7 or 1

				ldDate = .dToday
				DO WHILE DOW(ldDate,1) <> .nPeriodEndDayNum
					ldDate = ldDate - 1
				ENDDO
				ldEndDate = ldDate
				ldStartDate = ldDate - 6

			*****************************************
			* manually define start/end dates
			ldStartDate = {^2013-01-01}
			ldEndDate = {^2013-02-28}
			*****************************************

				.dEndDate = ldEndDate
				.dStartDate = ldStartDate
				
				lcSpecialWhere = ""
				************************************************************

				SET DATE AMERICAN

				lcStartDate = STRTRAN(DTOC(ldStartDate),"/","-")
				lcEndDate = STRTRAN(DTOC(ldEndDate),"/","-")

				SET DATE YMD

				*	 create "YYYYMMDD" safe date format for use in SQL query
				* NOTE: for SQL we are going to use < EndDate ( not <= ), so we are going to add 1 to that date here for querying purposes.
				lcSQLStartDate = "'" + STRTRAN(DTOC(ldStartDate),"/","") + "'"
				lcSQLEndDate = "'" + STRTRAN(DTOC(ldEndDate + 1),"/","") + "'"
				
*!*					lcSQLToday = STRTRAN(DTOC(ldToday),"/","")
*!*					lcSQLTomorrow = STRTRAN(DTOC(ldTomorrow),"/","")

				SET DATE AMERICAN

				.cSubject = 'CA Driver Time Conversion for Week: ' + lcStartDate + " to " + lcEndDate

				IF USED('CURWTKHOURS') THEN
					USE IN CURWTKHOURS
				ENDIF
				IF USED('CURWTKFINAL') THEN
					USE IN CURWTKFINAL
				ENDIF
				IF USED('CURWTKHOURSPRE') THEN
					USE IN CURWTKHOURSPRE
				ENDIF
				IF USED('CURWTKHOURSPRE2') THEN
					USE IN CURWTKHOURSPRE2
				ENDIF

				IF USED('CURTIMESHEET') THEN
					USE IN CURTIMESHEET
				ENDIF
				IF USED('CURTIMESHEET2') THEN
					USE IN CURTIMESHEET2
				ENDIF


				* main SQL
			SET TEXTMERGE ON
			TEXT TO	lcSQL NOSHOW
SELECT
C.FULLNM AS EMPLOYEE,
C.PERSONNUM AS FILE_NUM,
E.NAME AS PAYCODE,
A.APPLYDTM AS DATE,
A.DURATIONSECSQTY / 3600.00 AS TOTHOURS
FROM WFCTOTAL A
JOIN WTKEMPLOYEE B
ON B.EMPLOYEEID = A.EMPLOYEEID
JOIN PERSON C
ON C.PERSONID = B.PERSONID
JOIN LABORACCT D
ON D.LABORACCTID = A.LABORACCTID
JOIN PAYCODE E
ON E.PAYCODEID = A.PAYCODEID
WHERE (A.APPLYDTM >= <<lcSQLStartDate>>)
AND (A.APPLYDTM < <<lcSQLEndDate>>)
AND (A.DURATIONSECSQTY <> 0.00)
AND (D.LABORLEV1NM = 'SXI')
AND (D.LABORLEV2NM = '55')
AND (D.LABORLEV3NM = '0650')
ORDER BY C.FULLNM, A.APPLYDTM
			ENDTEXT
			SET TEXTMERGE OFF

*!*	AND E.NAME IN ('Sick Hours','Sick Hours - CA Drivers','Vacation Hours','Vacation Rollover Hours','Bereavement Hours')

*!*						" WHERE (A.APPLYDTM >= '" + lcSQLStartDate + "')" + ;
*!*						" AND (A.APPLYDTM < '" + lcSQLEndDate + "')" + ;



			SET TEXTMERGE ON
			TEXT TO	lcSQL5 NOSHOW
SELECT 
PERSONNUM AS FILE_NUM, 
EVENTDATE, 
INPUNCHDTM
FROM VP_TIMESHTPUNCHV42 
WHERE (EVENTDATE >= <<lcSQLStartDate>>)
AND (EVENTDATE < <<lcSQLEndDate>>)
AND (LABORLEVELNAME1 = 'SXI')
AND (LABORLEVELNAME2 = '55')
AND (LABORLEVELNAME3 = '0650')
ORDER BY PERSONNUM, EVENTDATE, INPUNCHDTM
			ENDTEXT
			SET TEXTMERGE OFF
			
				IF .lTestMode THEN
					.TrackProgress('', LOGIT+SENDIT)
					.TrackProgress('lcSQL =' + lcSQL, LOGIT+SENDIT)
					.TrackProgress('', LOGIT+SENDIT)
					.TrackProgress('lcSQL5 =' + lcSQL5, LOGIT+SENDIT)
				ENDIF

				.nSQLHandle = SQLCONNECT("WTK2","sa","kr0n1tes")

				IF .nSQLHandle > 0 THEN

					IF .ExecSQL(lcSQL, 'CURWTKHOURSPRE', RETURN_DATA_MANDATORY) AND ;
							.ExecSQL(lcSQL5, 'CURTIMESHEET2', RETURN_DATA_MANDATORY) THEN
							
							
							
						SELECT CURWTKHOURSPRE
						BROWSE
						
						* roll up by paycode/person/date AND FILTER OUT UNPAID PAY CODES
						SELECT EMPLOYEE, FILE_NUM, TTOD(DATE) AS DATE, UPPER(PAYCODE) AS PAYCODE, SUM(TOTHOURS) AS TOTHOURS ;
							FROM CURWTKHOURSPRE ;
							INTO CURSOR CURWTKHOURSPRE2 ;
							WHERE NOT UPPER(ALLTRIM(PAYCODE)) IN ('UNPAID HOURS','FMLA') ;
							GROUP BY 1, 2, 3, 4 ;
							ORDER BY 1, 2, 3, 4 ;
							READWRITE
							
						* add extra fields  - TYPE 'O' = ORIGINAL DATA; 'A' = ADJUSTMENT
						SELECT *, "O" AS TYPE, .F. AS lContOT, SPACE(100) AS COMMENT ;
							FROM CURWTKHOURSPRE2 ;
							INTO CURSOR CURWTKHOURS ;
							ORDER BY EMPLOYEE, FILE_NUM, DATE, PAYCODE ;
							READWRITE
							
						* identify which paycodes contribute to weekly Overtime.		
						SELECT CURWTKHOURS
						SCAN	
							lcPayCode = UPPER(ALLTRIM(CURWTKHOURS.PAYCODE))
							llContOT = .F.
							DO CASE
								CASE "REGULAR" $ lcPayCode 
									llContOT = .T.
								CASE "OVERTIME" $ lcPayCode 
									llContOT = .T.
								CASE "SHIFT DIFF" $ lcPayCode
									llContOT = .T.
								CASE "SICK" $ lcPayCode
									llContOT = .T.
								CASE "PERSONAL" $ lcPayCode
									llContOT = .T.
								CASE "VACATION" $ lcPayCode
									llContOT = .T.
									**  HOL hours do not count towards weekly OT for the CA drivers, per HR 3/12/13
*!*									CASE "HOLIDAY" $ lcPayCode
*!*										llContOT = .T.
								CASE "JURY" $ lcPayCode
									llContOT = .T.
								CASE "BEREAVEMENT" $ lcPayCode
									llContOT = .T.
								OTHERWISE
									* the pay code does not contribute ot weekly OT
							ENDCASE
							REPLACE CURWTKHOURS.lContOT WITH llContOT IN CURWTKHOURS
						ENDSCAN
	
*!*							SELECT CURWTKHOURS
*!*							BROWSE
*!*							THROW

						SELECT ;
							FILE_NUM, ;
							TTOD(EVENTDATE) AS EVENTDATE,  ;
							INPUNCHDTM ;
							FROM CURTIMESHEET2 ;
							INTO CURSOR CURTIMESHEET ;
							ORDER BY FILE_NUM, EVENTDATE, INPUNCHDTM ;
							WHERE NOT ISNULL( INPUNCHDTM )

						IF USED('CURTIMESHEET2') THEN
							USE IN CURTIMESHEET2
						ENDIF

						*!*	IF .lTestMode THEN
						*!*		SELECT CURTIMESHEET
						*!*		BROWSE
						*!*	ENDIF

						*!*	SELECT CURDEPARTMENTS   CURTIMESHEET
						*!*	BROWSE

						****** horizontalize cursor so paycodes are in separate columns

						IF USED('CURDAILY') THEN
							USE IN CURDAILY
						ENDIF

						* create cursor with one row per employee per date which will drive adjustment processing

						SELECT DISTINCT ;
							A.EMPLOYEE, ;
							A.FILE_NUM, ;
							A.DATE, ;
							0000.00 AS REG_HRS, ;
							0000.00 AS OT_HRS, ;
							0000.00 AS GTD_HRS, ;
							0000.00 AS SDIFF_HRS, ;
							0000.00 AS SICK_HRS, ;
							0000.00 AS PERS_HRS, ;
							0000.00 AS VAC_HRS, ;
							0000.00 AS HOL_HRS, ;
							0000.00 AS JURY_HRS, ;
							0000.00 AS BREAVE_HRS, ;
							0000.00 AS OTHER_HRS, ;
							DTOT(CTOD("  /  /  ")) AS INPUNCHDTM ;
							FROM CURWTKHOURS A ;
							INTO CURSOR CURDAILY ;
							ORDER BY 1, 2, 3 ;
							READWRITE

						* populate hours/code fields in main cursor
						SELECT CURDAILY
						SCAN
							SCATTER MEMVAR
							STORE 0000.00 TO ;
								M.REG_HRS, ;
								M.OT_HRS, ;
								M.GTD_HRS, ;
								M.SDIFF_HRS, ;
								M.SICK_HRS, ;
								M.PERS_HRS, ;
								M.VAC_HRS, ;
								M.HOL_HRS, ;
								M.JURY_HRS, ;
								M.BREAVE_HRS,  ;
								M.OTHER_HRS

							SELECT CURWTKHOURS
							SCAN FOR ;
									DATE = CURDAILY.DATE AND ;
									EMPLOYEE = CURDAILY.EMPLOYEE AND ;
									FILE_NUM = CURDAILY.FILE_NUM AND ;
									NOT DELETED()

								lcPayCode = UPPER(ALLTRIM(CURWTKHOURS.PAYCODE))

								DO CASE
									CASE "REGULAR" $ lcPayCode 
										m.REG_HRS = m.REG_HRS + CURWTKHOURS.TOTHOURS
									CASE "OVERTIME" $ lcPayCode
										m.OT_HRS = m.OT_HRS + CURWTKHOURS.TOTHOURS
									CASE "GUARANTEED" $ lcPayCode
										m.GTD_HRS = m.GTD_HRS + CURWTKHOURS.TOTHOURS
									CASE "SHIFT DIFF" $ lcPayCode
										m.SDIFF_HRS = m.SDIFF_HRS + CURWTKHOURS.TOTHOURS
									CASE "SICK" $ lcPayCode
										m.SICK_HRS = m.SICK_HRS + CURWTKHOURS.TOTHOURS
									CASE "PERSONAL" $ lcPayCode
										m.PERS_HRS = m.PERS_HRS + CURWTKHOURS.TOTHOURS
									CASE "VACATION" $ lcPayCode
										m.VAC_HRS = m.VAC_HRS + CURWTKHOURS.TOTHOURS
									CASE "HOLIDAY" $ lcPayCode
										m.HOL_HRS = m.HOL_HRS + CURWTKHOURS.TOTHOURS
									CASE "JURY" $ lcPayCode
										m.JURY_HRS = m.JURY_HRS + CURWTKHOURS.TOTHOURS
									CASE "BEREAVEMENT" $ lcPayCode
										m.BREAVE_HRS = m.BREAVE_HRS + CURWTKHOURS.TOTHOURS
									OTHERWISE
										* UNEXPECTED CODE
										m.OTHER_HRS = m.OTHER_HRS + CURWTKHOURS.TOTHOURS
										.TrackProgress('unexpected pay code = ' + lcPayCode, LOGIT+SENDIT) 
								ENDCASE

							ENDSCAN
							* accumulate totals
							SELECT CURDAILY
							GATHER MEMVAR
						ENDSCAN
						
						

						*!*	IF .lTestMode THEN
						*!*		SELECT CURDAILY
						*!*		COPY TO C:\A\KTTR_OT20.XLS XL5 FOR OT20_HRS > 0
						*!*	ENDIF


						* populate inpunchdtm and calcdshift in main cursor
						SELECT CURDAILY
						SCAN
							SELECT CURTIMESHEET
							GOTO TOP
							LOCATE FOR (FILE_NUM = CURDAILY.FILE_NUM) AND (EVENTDATE = CURDAILY.DATE)
							IF FOUND() THEN
								ltInpunch = CURTIMESHEET.INPUNCHDTM
								REPLACE CURDAILY.INPUNCHDTM WITH ltInpunch   &&  , CURDAILY.CALCDSHIFT WITH .GetCalculatedShift( ltInpunch )
							ENDIF
						ENDSCAN

				SELECT CURDAILY
				BROWSE

*!*							WAIT WINDOW NOWAIT "Preparing data..."
						
						LOCAL lnHOL_Hours, lnREG_Hours, lnOT_Hours, lcAdjName
						
						SELECT CURDAILY
						SCAN
							* insert adjustments into CURWTKHOURS
							lnHOL_Hours = CURDAILY.HOL_HRS
							lnREG_Hours = CURDAILY.REG_HRS 
							lnOT_Hours = CURDAILY.OT_HRS 
							
							* adjustment 1 =
							*	 if a Holiday is worked, then *delete* the HOL hours granted by Kronos, and enter actual hours worked as OT (1.5 time). Also make time worked count towards weekly OT.
							*	 but if a Holiday is not worked, retain the HOL hours granted by Kronos, and they do not count towards weekly OT.
						
							IF (lnHOL_Hours > 0.00) AND (( lnREG_Hours + lnOT_Hours ) > 0.00) THEN
								* HOL HOURS INDICATES THAT IT WAS A HOLIDAY
								* AND THE EXISTENCES OF REG OR OT HOURS INDICATES THAT THE PERSON ACTUALLY WORKED THE HOLIDAY
								
								lcAdjName = "Holiday was worked: "
								
								* add an adjustment to delete the HOL hours
								INSERT INTO CURWTKHOURS ;
									(EMPLOYEE, ;
									FILE_NUM, ;
									DATE, ;
									PAYCODE, ;
									TOTHOURS, ;
									TYPE, ;
									lContOT, ;
									COMMENT) ;
								VALUES ;
									( CURDAILY.EMPLOYEE, ;
									CURDAILY.FILE_NUM, ;
									CURDAILY.DATE, ;
									"HOLIDAY HOURS", ;
									(-1 * lnHOL_Hours), ;
									ADJUSTMENT_CODE, ;
									.F., ;
									lcAdjName + "deleting HOL Hours on " + TRANSFORM(CURDAILY.DATE))
								
								* if there were reg hours, convert them to OT hours by 2 adjustments
								IF (lnREG_Hours > 0.00) THEN
								
									* add an adjustment to delete the REG hours
									INSERT INTO CURWTKHOURS ;
										(EMPLOYEE, ;
										FILE_NUM, ;
										DATE, ;
										PAYCODE, ;
										TOTHOURS, ;
										TYPE, ;
										lContOT, ;
										COMMENT) ;
									VALUES ;
										( CURDAILY.EMPLOYEE, ;
										CURDAILY.FILE_NUM, ;
										CURDAILY.DATE, ;
										"REGULAR HOURS", ;
										(-1 * lnREG_Hours), ;
										ADJUSTMENT_CODE, ;
										.F., ;
										lcAdjName + "deleting " + TRANSFORM(lnREG_Hours) + " REG Hours on " + TRANSFORM(CURDAILY.DATE))
										
									* add an adjustment to ADD the REG hours amount as OT Hours
									INSERT INTO CURWTKHOURS ;
										(EMPLOYEE, ;
										FILE_NUM, ;
										DATE, ;
										PAYCODE, ;
										TOTHOURS, ;
										TYPE, ;
										lContOT, ;
										COMMENT) ;
									VALUES ;
										( CURDAILY.EMPLOYEE, ;
										CURDAILY.FILE_NUM, ;
										CURDAILY.DATE, ;
										"OVERTIME HOURS", ;
										(lnREG_Hours), ;
										ADJUSTMENT_CODE, ;
										.T., ;
										lcAdjName + "adding " + TRANSFORM(lnREG_Hours) + " OT Hours on " + TRANSFORM(CURDAILY.DATE))
										
								ENDIF
								
								* NOTE: any existing OT hours can be left as is, they should already be set to contribute to OT
								
							ELSE
								* they did not work a holiday - nothing to do for this particular adjustment...
							ENDIF
						 

						ENDSCAN  && CURDAILY
						
						* SORT THE MAIN CURSOR SO ADJUSTMENTS APEAR WITH ORIGINAL DATA
						SELECT * FROM CURWTKHOURS ;
							INTO CURSOR CURWTKFINAL ;
							ORDER BY EMPLOYEE, DATE, TYPE ;
							READWRITE
							
			SELECT CURWTKFINAL
			BROWSE
						
					ENDIF  &&  .ExecSQL(lcSQL, 'CURWTKEMPLOYEE', RETURN_DATA_MANDATORY)

					CLOSE DATABASES ALL

				ELSE
					* connection error
					.TrackProgress('Unable to connect to WTK System.', LOGIT+SENDIT)
				ENDIF   &&  .nSQLHandle > 0
			CATCH TO loError

				.TrackProgress('There was an error.',LOGIT+SENDIT)
				.TrackProgress('The Error # is: ' + TRANSFORM(loError.ERRORNO), LOGIT+SENDIT)
				.TrackProgress('The Error Message is: ' + TRANSFORM(loError.MESSAGE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred in Program: ' + TRANSFORM(loError.PROCEDURE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred at line #: ' + TRANSFORM(loError.LINENO), LOGIT+SENDIT)
				lnNumberOfErrors = lnNumberOfErrors + 1
				IF TYPE('oExcel') = 'O' AND NOT ISNULL(oExcel) THEN
					oExcel.QUIT()
				ENDIF
				CLOSE DATA
				*CLOSE ALL

			ENDTRY

			CLOSE DATA
			WAIT CLEAR
			***************** INTERNAL email results ******************************
			.TrackProgress('About to send status email.',LOGIT)
			.TrackProgress('==================================================================================================================', SENDIT)
			.TrackProgress('The logfile is: ' + .cLogFile, SENDIT)
			.TrackProgress('KRONOS CA DRIVER TIME CONVERSION process started: ' + .cStartTime, LOGIT+SENDIT)
			.TrackProgress('KRONOS CA DRIVER TIME CONVERSION process finished: ' + TTOC(DATETIME()), LOGIT+SENDIT)

			IF .lSendInternalEmailIsOn THEN
				* try to trap error from not having dartmail dll's registered on user's pc...
				TRY
					DO FORM dartmail2 WITH .cSendTo,.cFrom,.cSubject,.cCC,.cAttach,.cBodyText,"A"
					.TrackProgress('Sent status email.',LOGIT)
				CATCH TO loError
					*=MESSAGEBOX('There was an error sending the status email. Please contact Mark Bennett.',0+64,"KRONOS Temp Time")
					.TrackProgress('There was an error sending the status email.',LOGIT)
					.TrackProgress('The Error # is: ' + TRANSFORM(loError.ERRORNO), LOGIT)
					.TrackProgress('The Error Message is: ' + TRANSFORM(loError.MESSAGE), LOGIT)
					.TrackProgress('The Error occurred in Program: ' + TRANSFORM(loError.PROCEDURE), LOGIT)
					.TrackProgress('The Error occurred at line #: ' + TRANSFORM(loError.LINENO), LOGIT)
					lnNumberOfErrors = lnNumberOfErrors + 1
					CLOSE DATA
				ENDTRY

			ELSE
				.TrackProgress('Did not send status email: THIS.lSendInternalEmailIsOn = FALSE.',LOGIT)
			ENDIF

		ENDWITH
		RETURN
	ENDFUNC && main




*!*		FUNCTION GetCalculatedShift
*!*			LPARAMETERS tInpunchDTM
*!*			LOCAL lcShift, lcTimePortion
*!*			lcShift = "?"
*!*			lcTimePortion = RIGHT(TTOC(tInpunchDTM),8)
*!*			WITH THIS
*!*				DO CASE
*!*					CASE .cTempCo = 'A'
*!*						DO CASE
*!*							CASE lcTimePortion < "13:00:00"
*!*								lcShift = "1"
*!*							CASE lcTimePortion < "20:00:00"
*!*								lcShift = "2"
*!*							OTHERWISE
*!*								lcShift = "3"
*!*						ENDCASE
*!*					CASE INLIST(.cTempCo,'B','BJ','BS')
*!*						DO CASE
*!*							CASE lcTimePortion < "11:53:00"
*!*								lcShift = "1"
*!*							OTHERWISE
*!*								lcShift = "2"
*!*						ENDCASE
*!*					CASE INLIST(.cTempCo,'D')
*!*						DO CASE
*!*							CASE lcTimePortion < "16:23:00"
*!*								lcShift = "1"
*!*							OTHERWISE
*!*								lcShift = "2"
*!*						ENDCASE
*!*					OTHERWISE
*!*						* nothing
*!*				ENDCASE
*!*			ENDWITH
*!*			RETURN lcShift
*!*		ENDFUNC


*!*		FUNCTION IsDivisionValid
*!*			LPARAMETERS tcDivision
*!*			WITH THIS
*!*				LOCAL llRetVal
*!*				llRetVal = .F.
*!*				DO CASE
*!*					CASE (LEFT(.cTempCo,1) == 'A') AND (INLIST(tcDivision,'05','52','53','54','55','56','57','58','59','60'))
*!*						llRetVal = .T.
*!*					CASE (.cTempCo == 'B') AND (INLIST(tcDivision,'50','51'))
*!*						llRetVal = .T.
*!*					CASE (.cTempCo == 'BJ') AND (INLIST(tcDivision,'51'))
*!*						llRetVal = .T.
*!*					CASE (.cTempCo == 'BS') AND (INLIST(tcDivision,'50'))
*!*						llRetVal = .T.
*!*					CASE (.cTempCo == 'D') AND (INLIST(tcDivision,'04','14'))
*!*						llRetVal = .T.
*!*					OTHERWISE
*!*						* nothing
*!*				ENDCASE
*!*			ENDWITH
*!*			RETURN llRetVal
*!*		ENDFUNC


*!*		FUNCTION getTempAgencyMarkupREG
*!*			LPARAMETERS tcTempCo, tcSHORTNAME, tdPAYDATE
*!*			* note: this is for Reg Hours only; separate function for OT markup
*!*			LOCAL lnMarkup
*!*			DO CASE
*!*				CASE tcTempCo = 'A'
*!*	*!*					IF .lTestMode THEN
*!*	*!*						lnMarkup = 1.275
*!*	*!*					ELSE
*!*						DO CASE
*!*							CASE BETWEEN(tdPAYDATE,{^2001-01-01},{^2012-01-30})
*!*								lnMarkup = 1.28
*!*							CASE BETWEEN(tdPAYDATE,{^2012-01-31},{^2012-09-23})
*!*								lnMarkup = 1.275
*!*							CASE BETWEEN(tdPAYDATE,{^2012-09-24},{^2099-12-31})
*!*								lnMarkup = 1.2725
*!*						ENDCASE
*!*	*!*					ENDIF  &&  .lTestMode
*!*					
*!*				CASE INLIST(tcTempCo,'B','BJ','BS')
*!*					DO CASE
*!*						CASE BETWEEN(tdPAYDATE,{^2001-01-01},{^2012-12-31})
*!*							* Select indicated a flat 27.5% with no variations.
*!*							lnMarkup = 1.275
*!*						CASE BETWEEN(tdPAYDATE,{^2013-01-01},{^2099-12-31})
*!*							* Select indicated a flat 27.5% with no variations.
*!*							lnMarkup = 1.275
*!*					ENDCASE
*!*				CASE INLIST(tcTempCo,'D')  && QUALITY TEMPS - NJ
*!*					DO CASE
*!*						CASE BETWEEN(tdPAYDATE,{^2013-01-01},{^2099-12-31})
*!*							* flat 32% with no variations per Jim Lake 2/6/2013.
*!*							lnMarkup = 1.320
*!*					ENDCASE
*!*				OTHERWISE
*!*					* another temp co
*!*					lnMarkup = 1.00
*!*			ENDCASE
*!*			RETURN lnMarkup
*!*		ENDFUNC	&&  getTempAgencyMarkupREG


*!*		FUNCTION getTempAgencyMarkupOT
*!*			LPARAMETERS tcTempCo, tcSHORTNAME, tdPAYDATE
*!*			* note: this is for Reg Hours only; separate function below for OT markup
*!*			LOCAL lnMarkup
*!*			DO CASE
*!*				CASE tcTempCo = 'A'
*!*	*!*					IF .lTestMode THEN
*!*	*!*						lnMarkup = 1.275
*!*	*!*					ELSE
*!*						* Tri-State gets 28% markup unless temp is a rollover, which is 27.5% per Neftali 11/16/09
*!*						* Rollover will be indicated indicated by "ROLL" in shortname field.
*!*						DO CASE
*!*							CASE BETWEEN(tdPAYDATE,{^2001-01-01},{^2012-01-30})
*!*								lnMarkup = 1.28
*!*							CASE BETWEEN(tdPAYDATE,{^2012-01-31},{^2099-12-31})
*!*								lnMarkup = 1.24
*!*						ENDCASE
*!*	*!*					ENDIF  &&  .lTestMode
*!*					
*!*				CASE INLIST(tcTempCo,'B','BJ','BS')
*!*					DO CASE
*!*						CASE BETWEEN(tdPAYDATE,{^2001-01-01},{^2012-12-31})
*!*							* Select indicated a flat 27.5% with no variations.
*!*							lnMarkup = 1.275
*!*						CASE BETWEEN(tdPAYDATE,{^2013-01-01},{^2099-12-31})
*!*							* Select indicated a flat 27.5% with no variations.
*!*							lnMarkup = 1.275
*!*					ENDCASE
*!*				CASE INLIST(tcTempCo,'D')  && QUALITY TEMPS - NJ
*!*					DO CASE
*!*						CASE BETWEEN(tdPAYDATE,{^2013-01-01},{^2099-12-31})
*!*							* flat 32% with no variations per Jim Lake 2/6/2013.
*!*							lnMarkup = 1.320
*!*					ENDCASE
*!*				OTHERWISE
*!*					* another temp co
*!*					lnMarkup = 1.00
*!*			ENDCASE
*!*			RETURN lnMarkup
*!*		ENDFUNC	&& getTempAgencyMarkupOT


*!*		FUNCTION getShiftDiff
*!*			LPARAMETERS tcTempCo, tcCalcdShift, tcDept, tnBaseWage
*!*			LOCAL lnShiftDiff, lcDept, lcCalcdShift
*!*			lnShiftDiff = 0.00
*!*			lcDept = ALLTRIM( tcDept )
*!*			lcCalcdShift = ALLTRIM( tcCalcdShift )
*!*			DO CASE
*!*				CASE tcTempCo = 'A'
*!*					*!*	* Tri-State rules, updated per Neftali for base wage check 10/27/09 MB.
*!*					*!*	* basic rules for shift differential:
*!*					*!*	* if lcDept = '0622' (forklift drivers) or CalcdShift = '1' or base wage > 8.00 then no differential
*!*					*!*	* otherwise, give differential.
*!*					*!*	DO CASE
*!*					*!*		CASE (lcDept = '0622') OR (lcCalcdShift = '1') OR (tnBaseWage > 8.00)
*!*					*!*			lnShiftDiff = 0.00
*!*					*!*		OTHERWISE
*!*					*!*			lnShiftDiff = 0.20
*!*					*!*	ENDCASE
*!*					
*!*					lnShiftDiff = 0.00
*!*					
*!*				CASE INLIST(tcTempCo,'B','BJ','BS')
*!*					* Select rules, per Alex Figueroa and Juan Santisteban 2/10/10 MB.
*!*					* basic rules for shift differential:
*!*					* simply, if temp works 2nd shift, they get $0.20 extra
*!*					
*!*					* PER JUAN S 2/12/13 - THEY ARE PUTTING SHIFT DIFF IN BASE WAGE - SHOULD BE ZERO HERE
*!*					lnShiftDiff = 0.00
*!*					
*!*					*!*	DO CASE
*!*					*!*		CASE (lcCalcdShift = '1')
*!*					*!*			lnShiftDiff = 0.00
*!*					*!*		OTHERWISE
*!*					*!*			lnShiftDiff = 0.20
*!*					*!*	ENDCASE
*!*					
*!*				CASE INLIST(tcTempCo,'D')
*!*					* Quality rules, per Jim Lake 2/6/13.
*!*					* basic rules for shift differential:
*!*					* simply, if temp works 2nd shift, they get $0.40 extra
*!*					DO CASE
*!*						CASE (lcCalcdShift = '1')
*!*							lnShiftDiff = 0.00
*!*						OTHERWISE
*!*							lnShiftDiff = 0.40
*!*					ENDCASE
*!*				OTHERWISE
*!*					lnShiftDiff = 0.00
*!*			ENDCASE
*!*			RETURN lnShiftDiff
*!*		ENDFUNC


	PROCEDURE BuildDailyCursor
		LPARAMETERS tdPass2Date
		WITH THIS
			LOCAL lcSQLStartDate, lcSQLEndDate, lcSQLPass2, lcSQLPass2A

			SET DATE YMD

			*	 create "YYYYMMDD" safe date format for use in SQL query
			* NOTE: for SQL we are going to use < EndDate ( not <= ), so we are going to add 1 to that date here for querying purposes.
			lcSQLStartDate = STRTRAN(DTOC(tdPass2Date),"/","")
			lcSQLEndDate = STRTRAN(DTOC(tdPass2Date + 1),"/","")

			SET DATE AMERICAN

			IF USED('CURWTKHOURSTEMPPRE') THEN
				USE IN CURWTKHOURSTEMPPRE
			ENDIF
			IF USED('CURWTKHOURSTEMP') THEN
				USE IN CURWTKHOURSTEMP
			ENDIF
			IF USED('CURWAGES') THEN
				USE IN CURWAGES
			ENDIF

			*!*				* filter by Temp Co # if there was a passed tempco parameter
			*!*				IF NOT EMPTY(.cTempCo) THEN
			*!*					.cTempCoWhere = " AND (D.LABORLEV5NM = '" + .cTempCo + "') "
			*!*				ELSE
			*!*					.cTempCoWhere = ""
			*!*				ENDIF

			* pass 2 SQL
			* the join to basewagehistory table was causing duplicate records; had to get wage from separate sql
			* 9/23/09 MB

			* pass 2 SQL
			lcSQLPass2 = ;
				" SELECT " + ;
				" '2' AS CPASS, " + ;
				" D.LABORLEV5NM AS AGENCYNUM, " + ;
				" D.LABORLEV5DSC AS AGENCY, " + ;
				" D.LABORLEV2NM AS DIVISION, " + ;
				" D.LABORLEV3NM AS DEPT, " + ;
				" D.LABORLEV6NM AS SHIFT, " + ;
				" D.LABORLEV7NM AS TIMERVWR, " + ;
				" C.FULLNM AS EMPLOYEE, " + ;
				" C.SHORTNM AS SHORTNAME, " + ;
				" C.PERSONNUM AS FILE_NUM, " + ;
				" C.PERSONID, " + ;
				" A.EMPLOYEEID, " + ;
				" E.ABBREVIATIONCHAR AS PAYCODE, " + ;
				" E.NAME AS PAYCODEDESC, " + ;
				" (SUM(A.DURATIONSECSQTY) / 3600.00) AS TOTHOURS, " + ;
				" SUM(A.MONEYAMT) AS TOTPAY, " + ;
				" 0000.0000 AS WAGE " + ;
				" FROM WFCTOTAL A " + ;
				" JOIN WTKEMPLOYEE B " + ;
				" ON B.EMPLOYEEID = A.EMPLOYEEID " + ;
				" JOIN PERSON C " + ;
				" ON C.PERSONID = B.PERSONID " + ;
				" JOIN LABORACCT D " + ;
				" ON D.LABORACCTID = A.LABORACCTID " + ;
				" JOIN PAYCODE E " + ;
				" ON E.PAYCODEID = A.PAYCODEID " + ;
				" WHERE (A.APPLYDTM >= '" + lcSQLStartDate + "')" + ;
				" AND (A.APPLYDTM < '" + lcSQLEndDate + "')" + ;
				.cTempCoWhere + .cExtraWhere + ;
				" GROUP BY D.LABORLEV5NM, D.LABORLEV5DSC, D.LABORLEV2NM, D.LABORLEV3NM, D.LABORLEV6NM, D.LABORLEV7NM, C.FULLNM, C.SHORTNM, C.PERSONNUM, C.PERSONID, A.EMPLOYEEID, E.ABBREVIATIONCHAR, E.NAME " + ;
				" ORDER BY 1, 2, 3, 4, 5, 6, 7, 8, 9 "

			*!*	" AND (D.LABORLEV1NM = 'TMP') " + ;


			lcSQLPass2A = ;
				" SELECT " + ;
				" EMPLOYEEID, " + ;
				" BASEWAGEHOURLYAMT AS WAGE, " + ;
				" EFFECTIVEDTM, " + ;
				" EXPIRATIONDTM " + ;
				" FROM BASEWAGERTHIST " + ;
				" WHERE '" + lcSQLStartDate + "' BETWEEN EFFECTIVEDTM AND EXPIRATIONDTM " + ;
				" ORDER BY EMPLOYEEID DESC, EXPIRATIONDTM DESC "

			IF .lTestMode THEN
				.TrackProgress('', LOGIT+SENDIT)
				.TrackProgress('lcSQLPass2 =' + lcSQLPass2, LOGIT+SENDIT)
				.TrackProgress('', LOGIT+SENDIT)
				.TrackProgress('lcSQLPass2A =' + lcSQLPass2A, LOGIT+SENDIT)
			ENDIF

			IF .ExecSQL(lcSQLPass2, 'CURWTKHOURSTEMPPRE', RETURN_DATA_NOT_MANDATORY) AND ;
					.ExecSQL(lcSQLPass2A, 'CURWAGES', RETURN_DATA_MANDATORY) THEN

				IF USED('CURWTKHOURSTEMPPRE') AND NOT EOF('CURWTKHOURSTEMPPRE') THEN

					* data was returned

					* add a foxpro date field to the cursor
					SELECT tdPass2Date AS PAYDATE, * ;
						FROM CURWTKHOURSTEMPPRE ;
						INTO CURSOR CURWTKHOURSTEMP ;
						ORDER BY EMPLOYEE ;
						READWRITE

					*!*	SELECT CURWTKHOURSTEMP
					*!*	BROWSE
					*!*	SELECT CURWAGES
					*!*	BROWSE

					* populate WAGE field in CURWTKHOURSTEMP
					SELECT CURWTKHOURSTEMP
					SCAN
						SELECT CURWAGES
						LOCATE FOR EMPLOYEEID = CURWTKHOURSTEMP.EMPLOYEEID
						IF FOUND() THEN
							REPLACE CURWTKHOURSTEMP.WAGE WITH CURWAGES.WAGE
						ENDIF
					ENDSCAN

					*!*	SELECT CURWTKHOURSTEMP
					*!*	BROWSE


					* now append it to CURWTKHOURS
					SELECT CURWTKHOURS
					APPEND FROM DBF('CURWTKHOURSTEMP')


				ENDIF  &&  USED('CURWTKHOURSTEMPPRE') AND NOT EOF('CURWTKHOURSTEMPPRE')

			ENDIF  &&  .ExecSQL(lcSQLPass2, 'CURWTKHOURSTEMPPRE', RETURN_DATA_NOT_MANDATORY)

		ENDWITH
		RETURN
	ENDPROC && BuildDailyCursor



	FUNCTION ExecSQL
		LPARAMETERS tcSQL, tcCursorName, tlNoDataReturnedIsOkay
		LOCAL llRetVal, lnResult
		WITH THIS
			* close target cursor if it's open
			IF USED(tcCursorName)
				USE IN (tcCursorName)
			ENDIF
			WAIT WINDOW "Fetching remote data; please wait..." NOWAIT
			lnResult = SQLEXEC(.nSQLHandle, tcSQL, tcCursorName)
			llRetVal = ( lnResult > 0 )
			IF llRetVal THEN
				* see if any data came back
				IF NOT tlNoDataReturnedIsOkay THEN
					IF (NOT USED(tcCursorName)) OR EOF(tcCursorName) THEN
						llRetVal = .F.
						.TrackProgress('No data was returned by this query: ' +CRLF + tcSQL, LOGIT+SENDIT)
					ENDIF
				ENDIF
			ELSE
				.TrackProgress("Result Code [" + TRANSFORM(lnResult) + "] returned by this query: " + tcSQL, LOGIT+SENDIT)
			ENDIF
			WAIT CLEAR
			RETURN llRetVal
		ENDWITH
	ENDFUNC


	PROCEDURE TrackProgress
		* do any combination of Wait Window, writing to logfile, and adding to body of email,
		* based on nFlags parameter.
		LPARAMETERS tcExpression, tnFlags
		LOCAL lcCRLF
		WITH THIS
			IF BITAND(tnFlags,NOCRLF) = NOCRLF THEN
				lcCRLF = ""
			ELSE
				lcCRLF = CRLF
			ENDIF
			IF BITAND(tnFlags,LOGIT) = LOGIT THEN
				IF .lLoggingIsOn THEN
					?
					? .cProcessName + "  " + TTOC(DATETIME()) + ": " + tcExpression
				ENDIF
			ENDIF
			IF .lWaitWindowIsOn THEN
				IF BITAND(tnFlags,NOWAITIT) = NOWAITIT THEN
					WAIT WINDOW tcExpression NOWAIT
				ENDIF
				IF BITAND(tnFlags,WAITIT) = WAITIT THEN
					WAIT WINDOW tcExpression TIMEOUT .nWaitWindowTimeout
				ENDIF
			ENDIF
			IF BITAND(tnFlags,SENDIT) = SENDIT THEN
				IF .lSendInternalEmailIsOn THEN
					.cBodyText = .cBodyText + tcExpression + CRLF + lcCRLF
				ENDIF
			ENDIF
		ENDWITH
	ENDPROC  &&  TrackProgress


*!*		PROCEDURE SetTempCo
*!*			LPARAMETERS tcTempCo
*!*			WITH THIS
*!*				.nRoundingType = 1  && standard rounding
*!*				DO CASE
*!*					CASE tcTempCo == 'A'
*!*						.cTempCo = tcTempCo
*!*						.cTempCoName = 'Tri-State CA'
*!*						.lCheckBio = .T.
*!*						.nMinHoursTest = 2
*!*						.nMaxHoursTest = 10
*!*						IF .lTestMode THEN
*!*							.TrackProgress('Set TempCo = ' + tcTempCo,LOGIT+SENDIT)
*!*							.TrackProgress('Set TempCoName = ' + .cTempCoName,LOGIT+SENDIT)
*!*						ELSE
*!*							.TrackProgress('Set TempCo = ' + tcTempCo,LOGIT)
*!*							.TrackProgress('Set TempCoName = ' + .cTempCoName,LOGIT)
*!*						ENDIF
*!*					CASE tcTempCo == 'AJ'
*!*						.cTempCo = tcTempCo
*!*						.cTempCoName = 'Tri-State CA Jacques Moret'
*!*						.lCheckBio = .T.
*!*						.nMinHoursTest = 2
*!*						.nMaxHoursTest = 10
*!*						IF .lTestMode THEN
*!*							.TrackProgress('Set TempCo = ' + tcTempCo,LOGIT+SENDIT)
*!*							.TrackProgress('Set TempCoName = ' + .cTempCoName,LOGIT+SENDIT)
*!*						ELSE
*!*							.TrackProgress('Set TempCo = ' + tcTempCo,LOGIT)
*!*							.TrackProgress('Set TempCoName = ' + .cTempCoName,LOGIT)
*!*						ENDIF
*!*					CASE tcTempCo == 'A!J'
*!*						.cTempCo = tcTempCo
*!*						.cTempCoName = 'Tri-State CA Non-Jacques Moret'
*!*						.lCheckBio = .T.
*!*						.nMinHoursTest = 2
*!*						.nMaxHoursTest = 10
*!*						IF .lTestMode THEN
*!*							.TrackProgress('Set TempCo = ' + tcTempCo,LOGIT+SENDIT)
*!*							.TrackProgress('Set TempCoName = ' + .cTempCoName,LOGIT+SENDIT)
*!*						ELSE
*!*							.TrackProgress('Set TempCo = ' + tcTempCo,LOGIT)
*!*							.TrackProgress('Set TempCoName = ' + .cTempCoName,LOGIT)
*!*						ENDIF
*!*					CASE tcTempCo == 'A2'
*!*						.cTempCo = tcTempCo
*!*						.cTempCoName = 'Tri-State CA 2WIRE'
*!*						.lCheckBio = .T.
*!*						.nMinHoursTest = 2
*!*						.nMaxHoursTest = 10
*!*						IF .lTestMode THEN
*!*							.TrackProgress('Set TempCo = ' + tcTempCo,LOGIT+SENDIT)
*!*							.TrackProgress('Set TempCoName = ' + .cTempCoName,LOGIT+SENDIT)
*!*						ELSE
*!*							.TrackProgress('Set TempCo = ' + tcTempCo,LOGIT)
*!*							.TrackProgress('Set TempCoName = ' + .cTempCoName,LOGIT)
*!*						ENDIF
*!*					CASE tcTempCo == 'A!2'
*!*						.cTempCo = tcTempCo
*!*						.cTempCoName = 'Tri-State CA Non-2WIRE'
*!*						.lCheckBio = .T.
*!*						.nMinHoursTest = 2
*!*						.nMaxHoursTest = 10
*!*						IF .lTestMode THEN
*!*							.TrackProgress('Set TempCo = ' + tcTempCo,LOGIT+SENDIT)
*!*							.TrackProgress('Set TempCoName = ' + .cTempCoName,LOGIT+SENDIT)
*!*						ELSE
*!*							.TrackProgress('Set TempCo = ' + tcTempCo,LOGIT)
*!*							.TrackProgress('Set TempCoName = ' + .cTempCoName,LOGIT)
*!*						ENDIF
*!*					CASE tcTempCo == 'B'
*!*						.cTempCo = tcTempCo
*!*						.cTempCoName = 'Select ML '
*!*						.lCheckBio = .F.
*!*						
*!*						.nRoundingType = 1  && standard rounding
*!*						*.nRoundingType = 2  && no rounding
*!*						
*!*						.nMinHoursTest = 8
*!*						.nMaxHoursTest = 10
*!*						IF .lTestMode THEN
*!*							.TrackProgress('Set TempCo = ' + tcTempCo,LOGIT+SENDIT)
*!*							.TrackProgress('Set TempCoName = ' + .cTempCoName,LOGIT+SENDIT)
*!*						ELSE
*!*							.TrackProgress('Set TempCo = ' + tcTempCo,LOGIT)
*!*							.TrackProgress('Set TempCoName = ' + .cTempCoName,LOGIT)
*!*						ENDIF
*!*					CASE tcTempCo == 'BJ'
*!*						.cTempCo = tcTempCo
*!*						.cTempCoName = 'Select ML Jacques Moret '
*!*						.lCheckBio = .F.
*!*						.nMinHoursTest = 8
*!*						.nMaxHoursTest = 10
*!*						IF .lTestMode THEN
*!*							.TrackProgress('Set TempCo = ' + tcTempCo,LOGIT+SENDIT)
*!*							.TrackProgress('Set TempCoName = ' + .cTempCoName,LOGIT+SENDIT)
*!*						ELSE
*!*							.TrackProgress('Set TempCo = ' + tcTempCo,LOGIT)
*!*							.TrackProgress('Set TempCoName = ' + .cTempCoName,LOGIT)
*!*						ENDIF
*!*					CASE tcTempCo == 'BM'
*!*						.cTempCo = tcTempCo
*!*						.cTempCoName = 'Select ML Maintenance '
*!*						.lCheckBio = .F.
*!*						.nMinHoursTest = 8
*!*						.nMaxHoursTest = 10
*!*						IF .lTestMode THEN
*!*							.TrackProgress('Set TempCo = ' + tcTempCo,LOGIT+SENDIT)
*!*							.TrackProgress('Set TempCoName = ' + .cTempCoName,LOGIT+SENDIT)
*!*						ELSE
*!*							.TrackProgress('Set TempCo = ' + tcTempCo,LOGIT)
*!*							.TrackProgress('Set TempCoName = ' + .cTempCoName,LOGIT)
*!*						ENDIF
*!*					CASE tcTempCo == 'BS'
*!*						.cTempCo = tcTempCo
*!*						.cTempCoName = 'Select ML Sears-SHC '
*!*						.lCheckBio = .F.
*!*						.nMinHoursTest = 8
*!*						.nMaxHoursTest = 10
*!*						IF .lTestMode THEN
*!*							.TrackProgress('Set TempCo = ' + tcTempCo,LOGIT+SENDIT)
*!*							.TrackProgress('Set TempCoName = ' + .cTempCoName,LOGIT+SENDIT)
*!*						ELSE
*!*							.TrackProgress('Set TempCo = ' + tcTempCo,LOGIT)
*!*							.TrackProgress('Set TempCoName = ' + .cTempCoName,LOGIT)
*!*						ENDIF
*!*					CASE tcTempCo == 'HJB'
*!*						* report on SP2 hourly for Jaime Barba
*!*						.cTempCo = tcTempCo
*!*						.cTempCoName = 'Hourly - San Pedro '
*!*						.lCheckBio = .T.
*!*						.nMinHoursTest = 8
*!*						.nMaxHoursTest = 10
*!*						IF .lTestMode THEN
*!*							.TrackProgress('Set TempCo = ' + tcTempCo,LOGIT+SENDIT)
*!*							.TrackProgress('Set TempCoName = ' + .cTempCoName,LOGIT+SENDIT)
*!*						ELSE
*!*							.TrackProgress('Set TempCo = ' + tcTempCo,LOGIT)
*!*							.TrackProgress('Set TempCoName = ' + .cTempCoName,LOGIT)
*!*						ENDIF
*!*					CASE tcTempCo == 'HMG'
*!*						* report on RIALTO hourly for Mark Goldberg
*!*						.cTempCo = tcTempCo
*!*						.cTempCoName = 'Hourly - Div 59 '
*!*						.lCheckBio = .T.
*!*						.nMinHoursTest = 8
*!*						.nMaxHoursTest = 10
*!*						IF .lTestMode THEN
*!*							.TrackProgress('Set TempCo = ' + tcTempCo,LOGIT+SENDIT)
*!*							.TrackProgress('Set TempCoName = ' + .cTempCoName,LOGIT+SENDIT)
*!*						ELSE
*!*							.TrackProgress('Set TempCo = ' + tcTempCo,LOGIT)
*!*							.TrackProgress('Set TempCoName = ' + .cTempCoName,LOGIT)
*!*						ENDIF
*!*					CASE tcTempCo == 'D'
*!*						.cTempCo = tcTempCo
*!*						.cTempCoName = 'Quality NJ'
*!*						.lCheckBio = .T.
*!*						.nMinHoursTest = 2
*!*						.nMaxHoursTest = 10
*!*						IF .lTestMode THEN
*!*							.TrackProgress('Set TempCo = ' + tcTempCo,LOGIT+SENDIT)
*!*							.TrackProgress('Set TempCoName = ' + .cTempCoName,LOGIT+SENDIT)
*!*						ELSE
*!*							.TrackProgress('Set TempCo = ' + tcTempCo,LOGIT)
*!*							.TrackProgress('Set TempCoName = ' + .cTempCoName,LOGIT)
*!*						ENDIF
*!*					OTHERWISE
*!*						.TrackProgress('INVALID TempCo = ' + tcTempCo,LOGIT+SENDIT)
*!*						.cTempCoName = ''
*!*				ENDCASE

*!*				* determine if we are processing Hourly, not Temps
*!*				IF INLIST(.cTempCo,'HJB','HMG') THEN
*!*					.lHourlyNotTemps = .T.
*!*					.GetHourlyRatesFromADP()
*!*				ENDIF

*!*				* make logfiles company-specific so that report run times can overlap
*!*				IF .lTestMode THEN
*!*					.cLogFile = 'F:\UTIL\KRONOS\Logfiles\KRONOSCADRIVERTIMECONVERSION_' + .cTempCoName + "_" + .cMode + "_" + .cComputername + '_log_TESTMODE.txt'
*!*				ELSE
*!*					.cLogFile = 'F:\UTIL\KRONOS\Logfiles\KRONOSCADRIVERTIMECONVERSION_' + + .cTempCoName + "_" + .cMode + "_" + .cComputername + '_log.txt'
*!*				ENDIF
*!*				.lLoggingIsOn = .lLoggingIsOn AND NOT EMPTY(.cLogFile)
*!*				IF .lLoggingIsOn THEN
*!*					SET ALTERNATE TO (.cLogFile) ADDITIVE
*!*					SET ALTERNATE ON
*!*				ENDIF
*!*			ENDWITH
*!*		ENDPROC


	PROCEDURE GetHourlyRatesFromADP

		* get ADP hourly rates (for hourly employees)
		IF USED('CURADP') THEN
			USE IN CURADP
		ENDIF

		.nSQLHandle = SQLCONNECT("PCPAYWIN","MBENNETT","VP737A")

		IF .nSQLHandle > 0 THEN

			SET TEXTMERGE ON
			TEXT TO	lcSQLADP NOSHOW
SELECT
FILE# AS FILE_NUM,
{fn ifnull(MAX(rate1amt),0.00)} AS RATE
FROM REPORTS.V_EMPLOYEE
WHERE COMPANYCODE = 'E87'
GROUP BY FILE#
			ENDTEXT
			SET TEXTMERGE OFF

			.TrackProgress('lcSQLADP =' + lcSQLADP, LOGIT+SENDIT)

			IF .ExecSQL(lcSQLADP, 'CURADP', RETURN_DATA_MANDATORY) THEN

				*SELECT CURADP
				*BROWSE

				=SQLDISCONNECT(.nSQLHandle)

			ENDIF  &&  .ExecSQL(lcSQLADP, 'CURADP', RETURN_DATA_MANDATORY)

		ELSE
			* connection error
			.TrackProgress('Unable to connect to ADP Payroll System.', LOGIT+SENDIT)
			THROW
		ENDIF   &&  .nSQLHandle > 0
	ENDPROC


	FUNCTION GetDivisionName
		LPARAMETERS tcDivision
		LOCAL lcDivisionName, lnSelect
		lnSelect = SELECT(0)
		lcDivisionName = "?"
		SELECT CURDIVISIONS
		LOCATE FOR ALLTRIM(tcDivision) == ALLTRIM(CURDIVISIONS.NAME)
		IF FOUND() THEN
			lcDivisionName = ALLTRIM(CURDIVISIONS.DESCRIPTION)
		ENDIF
		SELECT (lnSelect)
		RETURN lcDivisionName
	ENDFUNC


	FUNCTION GetDepartmentName
		LPARAMETERS tcDepartment
		LOCAL lcDepartmentName, lnSelect
		lnSelect = SELECT(0)
		lcDepartmentName = "?"
		SELECT CURDEPARTMENTS
		LOCATE FOR ALLTRIM(tcDepartment) == ALLTRIM(CURDEPARTMENTS.NAME)
		IF FOUND() THEN
			lcDepartmentName = ALLTRIM(CURDEPARTMENTS.DESCRIPTION)
		ENDIF
		SELECT (lnSelect)
		RETURN lcDepartmentName
	ENDFUNC


	FUNCTION CreatePDFReport
		* create PDF report
		LPARAMETERS tcRootPDFFilename, tcPDFReportFRX

		WITH THIS

			.TrackProgress('in CreatePDFReport tcRootPDFFilename=' + tcRootPDFFilename, LOGIT)
			.TrackProgress('in CreatePDFReport tcPDFReportFRX=' + tcPDFReportFRX, LOGIT)

			LOCAL llPDFReportWasCreated, lcRootPDFFilename, lcFullPDFFilename, lcPDFReportFRX
			lcCentury = SET("CENTURY")

			SET CENTURY OFF  && FOR XX/XX/XX FORMAT DATES ON REPORT
			lcRootPDFFilename = tcRootPDFFilename
			lcPDFReportFRX = tcPDFReportFRX

			lcFullPDFFilename = lcRootPDFFilename + ".PDF"

			llPDFReportWasCreated = .F.

			DO M:\DEV\PRG\RPT2PDF WITH lcPDFReportFRX, lcRootPDFFilename, llPDFReportWasCreated

			IF NOT llPDFReportWasCreated THEN
				.TrackProgress('ERROR - PDF file [' + lcFullPDFFilename + '] was not created',LOGIT+WAITIT+SENDIT)
			ENDIF

			*!*				* keep track of pdf report files for emailing later
			*!*				.AddAttachment(lcFullPDFFilename)

			SET PRINTER TO DEFAULT
			SET CENTURY &lcCentury
		ENDWITH
	ENDFUNC  &&  CreatePDFReport


ENDDEFINE


PROCEDURE errHandlerKTTR
   PARAMETER merror, mess, mess1, mprog, mlineno, tcTempCo, tcMode
   LOCAL lcSendTo,lcFrom,lcSubject,lcCC,lcAttach,lcBodyText
   CLEAR
   lcSendTo = 'mbennett@fmiint.com'
   lcFrom = 'fmicorporate@fmiint.com'
   lcSubject = 'Early error in KRONOSCADRIVERTIMECONVERSION'
   lcCC = ''
   lcAttach = ''
   lcBodyText = 'Error number: ' + LTRIM(STR(merror)) + CRLF + ;
		'Error message: ' + mess + CRLF + ;
		'Line of code with error: ' + mess1 + CRLF + ;
		'Line number of error: ' + LTRIM(STR(mlineno)) + CRLF + ;
		'Program with error: ' + mprog + CRLF + ;
		'tcTempCo = ' + tcTempCo + CRLF + ;
		'tcMode = ' + tcMode
      
	DO FORM dartmail2 WITH lcSendTo,lcFrom,lcSubject,lcCC,lcAttach,lcBodyText,"A"
	
	QUIT
	
ENDPROC

