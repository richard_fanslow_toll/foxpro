lparameters xcarrier, xshipdt, xfix

use (gmalvernserver+"data\tcurrent") in 0
use (gmalvernserver+"data\hcurrent") in 0
use (gmalvernserver+"data\scurrent") in 0

* make sure shipdate is correct in tcurrent

if !inlist(goffice,"L","K")
	select tcurrent
	scan for carrier=xcarrier
		if xsqlexec("select * from package where packageid="+transform(val(pkgid)),"xpackage",,"wh") # 0
			if xsqlexec("select * from shipment where shipmentid="+trans(xpackage.shipmentid),"xshipment",,"wh") # 0
				if tcurrent.shipdate # xshipment.shipdate
					replace shipdate with xshipment.shipdate in tcurrent
					replace shipdate with xshipment.shipdate in scurrent for pkgid=tcurrent.pkgid
					replace shipdate with xshipment.shipdate, inv_date with shipment.shipdate in hcurrent for shpmntkey=tcurrent.shpmntkey
				endif
			endif
		endif
	endscan
endif

* move future shipments out of t/h/s current

if !inlist(goffice,"K","L")
	m.office=goffice
	select tcurrent
	scan for carrier=xcarrier and shipdate>xshipdt and status#"V"
		select upsacct
		locate for acctcode=tcurrent.acctcode and carrier=xcarrier
		if found()
			select tcurrent
			scatter memvar memo
			insert into tfuture from memvar
			blank in tcurrent
			delete in tcurrent
			
			select scurrent
			scan for pkgid=m.pkgid
				scatter memvar memo
				insert into sfuture from memvar
				blank in scurrent
				delete in scurrent
			endscan

			select hcurrent
			scan for shpmntkey=m.shpmntkey
				scatter memvar memo
				insert into hfuture from memvar
				blank in hcurrent
				delete in hcurrent
			endscan
		endif
	endscan
endif

*

create cursor malvernvoid (acctcode c(20), track_no c(20), reason c(1))

xsqlexec("select * from shipment where carrier='"+xcarrier+"' and shipdate={"+dtoc(xshipdt)+"} and pickuprec=' '","xshipment",,"wh")
scan
	xsqlexec("select trknumber from package where shipmentid="+transform(xshipment.shipmentid)+" and trknumber#' '","xpackage",,"wh")
	scan
		if !seek(padr(trknumber,24),"tcurrent","track_no")
			insert into malvernvoid (acctcode, track_no, reason) values (shipment.acctcode, xpackage.trknumber, "0")
*			set step on 
*			x3winmsg("Tracking number "+trim(trknumber)+" must be re-submitted")
*			use in shipment
*			use in package
*			use in tcurrent
*			return .f.
		else
			if tcurrent.status="V"
				insert into malvernvoid (acctcode, track_no, reason) values (tcurrent.acctcode, tcurrent.track_no, "1")
				if xfix
					replace status with "" in tcurrent
				endif
			endif
		endif
	endscan
endscan

* removed dy 11/10/16 due to SQL upgrade

*if goffice#"L"
*	select acctcode from upsacct where carrier=xcarrier group by 1 into cursor xupsacct
*	scan
*		select tcurrent
*		scan for carrier=xcarrier and status#"V" and acctcode=xupsacct.acctcode
*			if !seek(left(track_no,20),"package","trknumber")
*				insert into malvernvoid (acctcode, track_no, reason) values (tcurrent.acctcode, tcurrent.track_no, "2")
*				if xfix
*					replace status with "V" in tcurrent
*				endif
*			endif
*		endscan
*	endscan
*endif

if reccount("malvernvoid")#0
	select malvernvoid
	copy to h:\fox\malvern xls
*	email("Dyoung@fmiint.com","Malvern/WMS out of synch during closing, office "+goffice,,"h:\fox\malvern.xls",,,.t.,,,,,.t.)
endif

use in tcurrent
use in scurrent
use in hcurrent
