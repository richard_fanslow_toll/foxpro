Parameters lcFilename

Set Century On
Select x856
Goto Top

Store 0 To lnTotalAdded
lcCurrentHAWB = ""

testing=.F.

nMANProcessed = 0
POs_added = 0
ShipmentNumber = 1
ThisShipment = 0
origawb = ""
lcCurrentShipID = ""
lcCurrentArrival = ""
LAFreight = .F.

Select ctnucc
Set Filter To

lcHLLevel = ""

Select* From ctnucc Where .F. Into Cursor xdetail Readwrite
Select xdetail
Scatter Memvar Blank

m.acct_name = "KIPLING"
m.acct_num  = 6356
m.office = "M"

Select x856
Set Filter To
Locate

Wait Window "Now importing file..........   "+lcFilename Nowait
Do While !Eof("x856")
  m.loaddt  = Date()
  M.ACCOUNTID = 6494
  m.adddt   = Date()
  m.addproc ="OD-856LOAD"
  m.addby   ="PAULG"
  cSegment = Alltrim(x856.segment)
  cField1 = Alltrim(x856.f1)
  If Inlist(cSegment,"ISA")
    m.shipID    = strtran(Alltrim(x856.f13),"~","")
    Select x856
    Skip 1 In x856
    Loop
  Endif
  If Inlist(cSegment,"GS")
    Select x856
    Skip 1 In x856
    Loop
  Endif
  If Inlist(cSegment,"ST")
    Select x856
    Skip 1 In x856
    Loop
  Endif


  If Trim(x856.segment) = "BSN"
    m.asnfile  = Justfname(lcFilename)
    m.dateloaded= Date()
    m.acountid = 6494
    m.reference = strtran(Alltrim(x856.f2),"~","")
    lcASN_Number = m.reference
    Select x856
    Skip 1 In x856
    Loop
  Endif
  If At("HL",x856.segment)>0
    lcHLLevel = Alltrim(x856.f3)
    Select x856
    Skip 1 In x856
    Loop
  Endif

  If lcHLLevel = "S"
    Do While lcHLLevel = "S"
      If At("HL",x856.segment)>0
        lcHLLevel = Alltrim(x856.f3)
        Select x856
        Skip 1 In x856
        Loop
      Endif
      cSegment = Trim(x856.segment)
      cF1 = Trim(x856.f1)
      Do Case
      Case Trim(x856.segment) = "TD3"
        m.container = strtran(Alltrim(x856.f3),"~","")
      Case Trim(x856.segment) = "N1" And Alltrim(x856.f1) = "SN"
        m.store = strtran(Alltrim(x856.f4),"~","")
      Case Trim(x856.segment) = "REF" And Alltrim(x856.f1) = "BM"
        m.bol = strtran(Alltrim(x856.f2),"~","")
      Otherwise
        m.acct_name = "UNK"
        m.acct_num  = 9999
      Endcase
      Select x856
      Skip 1 In x856
    Enddo
  Endif

  If lcHLLevel = "O"
    Select xDetail
    m.timeloaded = Datetime()
    Select x856
    Do While Inlist(lcHLLevel,"O")
      If At("HL",x856.segment)>0
        lcHLLevel = Alltrim(x856.f3)
        Select x856
        Skip 1 In x856
        Loop
      Endif
      cSegment = Trim(x856.segment)
      cF1 = Trim(x856.f1)
      Do Case
      Case Trim(x856.segment) = "PRF"
        m.ponum  = Alltrim(x856.f1)
      Case Trim(x856.segment) = "REF" And Alltrim(x856.f1) = "PO"
        m.reference = Alltrim(x856.f2)
      Case Trim(x856.segment) = "DTM" And Alltrim(x856.f1)= "005"
        m.cancel = Ctod(Substr(Alltrim(x856.f2),5,2)+"/"+Substr(Alltrim(x856.f2),7,2)+"/"+Substr(Alltrim(x856.f2),1,4))
      Case Trim(x856.segment) = "DTM" And Alltrim(x856.f1)= "017"
        m.cancel = Ctod(Substr(Alltrim(x856.f2),5,2)+"/"+Substr(Alltrim(x856.f2),7,2)+"/"+Substr(Alltrim(x856.f2),1,4))
      Endcase
      Select x856
      Skip 1 In x856
    Enddo
    Select x856
  Endif

  If lcHLLevel = "I"
    Do While lcHLLevel = "I"
      If At("HL",x856.segment)>0
        lcHLLevel = Alltrim(x856.f3)
        Select x856
        Skip 1 In x856
        Loop
      Endif
      cSegment = Trim(x856.segment)
      cF1 = Trim(x856.f1)
      Do Case
      Case Trim(x856.segment) = "LIN"
        m.style = Alltrim(x856.f5)
        m.id  = Alltrim(x856.f7)
        m.color = Alltrim(x856.f9)
        m.upc   = Alltrim(x856.f3)
      Case Trim(x856.segment) = "SN1"
        m.totqty = Val(Alltrim(x856.f2))
      Endcase
      Select x856
      Skip 1 In x856
    Enddo
    Select x856
  Endif

  If lcHLLevel = "P"
    Do While lcHLLevel = "P"
      If At("HL",x856.segment)>0
        lcHLLevel = Alltrim(x856.f3)
        Select x856
        Skip 1 In x856
        Loop
      Endif
      cSegment = Trim(x856.segment)
      cF1 = Trim(x856.f1)
      If Alltrim(x856.segment) ="SE"
        Skip 1 In x856
        Exit
      Endif
      Do Case
      Case Trim(x856.segment) = "SN1"
        m.pack = Alltrim(x856.f2)
      Case Trim(x856.segment) = "MAN"
        m.ucc = Alltrim(x856.f2)
        Select xdetail
        Append Blank

*!*	        Select whgenpk
*!*	        Locate For gpk_pk = "CTNUCC"
*!*	        Replace gpk_currentnumber With gpk_currentnumber + 1 IN whgenpk
*!*	        m.ctnuccid = gpk_currentnumber

        m.ctnuccid = sqlgenpk("ctnucc","wh")

        Select xdetail
        Gather Memvar
      Endcase
      Select x856
      If !Eof()
        Skip 1 In x856
      Else
        Exit
      Endif
    Enddo
  Endif
Enddo

lProcessOK = .T.
Return
