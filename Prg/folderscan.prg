Parameters xtaskserver

runack("FOLDERSCAN")

DO m:\dev\prg\_setvars WITH .T.

ON ERROR THROW
PUBLIC cProgtorun,lDoCatch, taskserver

taskserver = xtaskserver

lDoCatch = .t.
cProgtorun = ""
 TRY
  	WITH _SCREEN
  		.TOP = 70
  		.LEFT = 450
  		.WINDOWSTATE = 0
  		.BORDERSTYLE = 1
 		.WIDTH = 460
 		.HEIGHT = 200
 		.CLOSABLE = .F.
  		.MAXBUTTON = .F.
  		.SCROLLBARS = 0
  		.CAPTION = "Toll Folder Checker"
  	ENDWITH

	DO FORM m:\dev\FRM\FOLDERSCAN

CATCH TO oErr
	IF lDoCatch
		ASSERT .F. MESSAGE "In Error CATCH"
		tsubject = "FOLDERSCAN Error ("+TRANSFORM(oErr.ERRORNO)+") at "+TTOC(DATETIME())
		tattach  = ""
		SELECT 0
		USE F:\3pl\DATA\mailmaster ALIAS mm
		LOCATE FOR mm.edi_type = "MISC" AND mm.taskname = "GENERAL"
		tsendto = IIF(mm.use_alt,mm.sendtoalt,mm.sendto)
		tcc = IIF(mm.use_alt,mm.ccalt,mm.cc)
		USE IN mm

		tmessage = "Folder Scan Error"+CHR(13)

		tmessage =tmessage+ "Try/CATCH Exeception Message:"+CHR(13)+;
			[  Error: ] + STR(oErr.ERRORNO) +CHR(13)+;
			[  LineNo: ] + STR(oErr.LINENO) +CHR(13)+;
			[  Message: ] + oErr.MESSAGE +CHR(13)+;
			[  Procedure: ] + oErr.PROCEDURE +CHR(13)+;
			[  Details: ] + oErr.DETAILS +CHR(13)+;
			[  StackLevel: ] + STR(oErr.STACKLEVEL) +CHR(13)+;
			[  LineContents: ] + oErr.LINECONTENTS+CHR(13)+;
			[  UserValue: ] + oErr.USERVALUE
		tmessage = tmessage+CHR(13)+CHR(13)+"PROGRAM IN QUEUE: "+cProgtorun

		tsubject = "Folderscan Poller Error at "+TTOC(DATETIME())
		tattach  = ""
		tfrom    ="Toll EDI 945 Poller Operations <toll-edi-ops@tollgroup.com>"
		DO FORM m:\dev\frm\dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
	ENDIF
FINALLY
	ON ERROR
	SET STATUS BAR ON
ENDTRY
