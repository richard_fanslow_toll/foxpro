**vasload_steelseries.prg
**
**load vas codes into xvas cursor
**called from packout and scanpackbase class
**

select outdet
for xvascnt = 1 to 10 &&up
  m.vascode=getmemodata("printstuff","VAS"+transform(xvascnt),.f.,.t.)
  if empty(m.vascode)
    loop
  endif
  scatter fields upc, style, color, id, custsku memvar
  m.upc=outdet.upc
  do case
  case m.vascode="VA01"
    m.vasdesc=""
  case m.vascode="VA02"
    m.vasdesc=""
  case m.vascode="V03"
    m.vasdesc=""
  otherwise
    m.vasdesc="Unknown VAS Code (Codigo VAS desconocido)"
  endcase
  insert into xvas from memvar
endfor
