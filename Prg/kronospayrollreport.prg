* Create spreadsheet of Hours summary for Employees we are exporting from kronos to ADP Payroll
* for the previous pay period.
** Meant to be run after hours are reviewed and cleaned up; Monday pm or Tuesday am.
* EXEs go in F:\UTIL\KRONOS\

LOCAL loKronosPayrollReport
*!*	IF NOT INLIST(UPPER(ALLTRIM(GETENV("COMPUTERNAME"))),"MBENNETT","HR1") THEN
*!*		=MESSAGEBOX("The Employee Export must be run from Lucille's PC!",0+16,"Export FoxPro Employees")
*!*		RETURN
*!*	ENDIF
loKronosPayrollReport = CREATEOBJECT('KronosPayrollReport')
loKronosPayrollReport.MAIN()
CLOSE DATABASES ALL
RETURN


*!*	*** SPECIAL FOR DARREN BACK PROCESSING
*!*	LOCAL ldDate, loKronosPayrollReport
*!*	loKronosPayrollReport = CREATEOBJECT('KronosPayrollReport')
*!*	loKronosPayrollReport.lSendInternalEmailIsOn = .F.

*!*	ldDate = {^2010-01-12}
*!*	DO WHILE ldDate < {^2010-01-20}
*!*		loKronosPayrollReport.SetDate( ldDate )
*!*		loKronosPayrollReport.MAIN()
*!*		loKronosPayrollReport.cBodyText = ''
*!*		ldDate = ldDate + 1
*!*	ENDDO
*!*	CLOSE DATABASES ALL
*!*	RETURN
*!*	********************************

#DEFINE LOGIT 1
#DEFINE WAITIT 2
#DEFINE NOWAITIT 4
#DEFINE SENDIT 8
#DEFINE CRLF CHR(13) + CHR(10)
#DEFINE RETURN_DATA_MANDATORY .F.
#DEFINE RETURN_DATA_NOT_MANDATORY .T.

DEFINE CLASS KronosPayrollReport AS CUSTOM

	cProcessName = 'KronosPayrollReport'

	lTestMode = .T.  && .F. FOR NORMAL OPERATION, .T. TO SUPPRESS ALL BUT DEVELOPER EMAILS AND CHANGE INPUT TABLE, ETC.

	***********************   SET REPORT TYPE BEFORE BUILDING EXE   **********************
	**************************************************************************************
	**************************************************************************************

	*cReportType = "STANDARD"	&& make the exe = KronosPayrollReport.EXE !!!!!
	*cReportType = "TGF"	&& make the exe = KronosPayrollReport-TGF.EXE !!!!!
	cReportType = "TUG"		&& make the exe = KronosPayrollReport-TUG.EXE !!!!!
	*cReportType = "BRIAN"		&& make the exe = KronosPayrollReport-BRIAN.EXE !!!!! RUN DAILY
	*cReportType = "JONIELL"	&& make the exe = KronosPayrollReport-JONEILL.EXE !!!!!
	*cReportType = "ALLOC_JONEILL"	&& make the exe = KronosPayrollReport-ALLOC_JONEILL.EXE !!!!!
	*cReportType = "DARREN"		&& make the exe = KronosPayrollReport-DARREN.EXE !!!!! RUN DAILY
	*cReportType = "MONTHLY"
	&&&!!!! USE SEPARATE PRG *cReportType = "RALPH_REYES_WTD"		&& make the exe = KronosPayrollReport-RREYES.EXE !!!!! RUN DAILY

	**************************************************************************************
	**************************************************************************************
	**************************************************************************************
	
	cADPCompany = "SXI"

	* which payroll server, and associated connections, to hit
	lUsePAYROLL2Server = .T.

	lAutoYield = .T.

	lProcessEPIE8799CSVFile = .T.

	lMakeSummaryByHomeDept = .T.

	*!*		* !!! When you set this flag to .T. make the exe = KronosPayrollReport-TUG.EXE !!!!!
	*!*		lDoSpecialTUGReport = .F.

	cStartTime = TTOC(DATETIME())

	* connection properties
	nSQLHandle = 0

	* file properties
	nFileHandle = 0

	* table properties
	cFoxProTimeReviewerTable = "F:\UTIL\KRONOS\DATA\TIMERVWR"

	* date properties
	dtNow = DATETIME()


	dToday = DATE()
	* to force different pay weeks
	*dToday = {^2010-11-01}

	cToday = DTOC(DATE())

	* wait window properties
	nWaitWindowTimeout = 2
	lWaitWindowIsOn = .T.

	* logfile properties
	lLoggingIsOn = .T.
	cLogFile = 'F:\UTIL\KRONOS\Logfiles\KronosPayrollReport_log.txt'

	* INTERNAL email properties
	lSendInternalEmailIsOn = .T.
	cFrom = 'FMI Corporate <fmicorporate@fmiint.com>'
	*cSendTo = 'mbennett@fmiint.com'
	cSendTo = ''
	cSendToAUDIT = ''
	cCC = ''
	cSubject = 'Kronos Payroll Report for: ' + TTOC(DATETIME())
	cAttach = ''
	cBodyText = ''

	* reporting logic properties
	lExcludeEmployeesWith9999OnReportDate = .T.

	cCOMPUTERNAME = ''
	cUSERNAME = ''

	FUNCTION INIT
		IF NOT DODEFAULT()
			RETURN .F.
		ENDIF
		WITH THIS
			CLEAR
			*SET RESOURCE OFF
			CLOSE DATA
			SET CENTURY ON
			SET DATE YMD
			SET HOURS TO 24
			SET ANSI ON
			SET TALK OFF
			SET DELETED ON
			SET CONSOLE OFF
			SET EXCLUSIVE OFF
			SET SAFETY OFF
			SET EXACT OFF
			SET STATUS BAR ON
			SET SYSMENU OFF
			SET ENGINEBEHAVIOR 70
			_VFP.AUTOYIELD = .lAutoYield
			.cCOMPUTERNAME = GETENV("COMPUTERNAME")
			.cUSERNAME = GETENV("USERNAME")
			IF .lTestMode THEN
				.cSendTo = 'mbennett@fmiint.com'
				.cSendToAUDIT = 'mbennett@fmiint.com'
				.cLogFile = 'F:\UTIL\KRONOS\Logfiles\KronosPayrollReport_log_TESTMODE.txt'
			ENDIF
			.lLoggingIsOn = .lLoggingIsOn AND NOT EMPTY(.cLogFile)
			IF .lLoggingIsOn THEN
				SET ALTERNATE TO (.cLogFile) ADDITIVE
				SET ALTERNATE ON
			ENDIF
		ENDWITH
	ENDFUNC


	FUNCTION DESTROY
		WITH THIS
			IF .lLoggingIsOn  THEN
				SET ALTERNATE OFF
				SET ALTERNATE TO
			ENDIF
		ENDWITH
		DODEFAULT()
	ENDFUNC


	FUNCTION MAIN
		WITH THIS
			LOCAL lnNumberOfErrors, lcSQL, lcSQL2, lcSQL3, lcSQL4, loError, lcTopBodyText, llValid
			LOCAL lcXLFileName, ldStartDate, ldEndDate, lcStartDate, lcEndDate, ldDate
			LOCAL lcSQLStartDate, lcSQLEndDate, lcXLFileName, ldYesterday
			LOCAL oExcel, oWorkbook, oWorksheet, lnRow, lcRow, lnStartRow, lcStartRow, lnEndRow, lnPP, lcPP, lnNN, lcNN, lcJJ
			LOCAL lcColsToProcess, lnColumn, lcBoldColumn, i, j, lcBoldRow, luCellValue, lcTargetDirectory, ldPayDate, lcFiletoSaveAs
			LOCAL lcFileDate, lcDivision, lcRateType, lcTitle, llSaveAgain, lcPayCode, lcSpecialWhere
			LOCAL lcSQLToday, lcSQLTomorrow, ldToday, ldTomorrow, lcExcluded9999s, lcFoxProTimeReviewerTable, lcMissingApprovals
			LOCAL lcAuditSubject, oWorkbookAUDIT, oWorksheetAUDIT, lcSendTo, lcCC, lcBodyText, lcXLFileNameAUDIT, lcTotalRow
			LOCAL lcTRWhere, lcOrderBy, lnStraightHours, lnStraightDollars, lnOTDollars


			TRY
				lnNumberOfErrors = 0
				lcTopBodyText = ''
				ldToday = .dToday
				ldTomorrow = ldToday + 1
				ldYesterday = ldToday - 1

				*.TrackProgress('==================================================================================================================', LOGIT+SENDIT)
				.TrackProgress('KRONOS PAYROLL REPORT process started....', LOGIT+SENDIT+NOWAITIT)
				.TrackProgress('COMPUTERNAME = ' + .cCOMPUTERNAME, LOGIT+SENDIT)
				.TrackProgress('USERNAME = ' + .cUSERNAME, LOGIT+SENDIT)
				.TrackProgress('==================================================================================================================', LOGIT+SENDIT)
				IF .lUsePAYROLL2Server THEN
					.TrackProgress('                   Accessing PAYROLL2 !!!                ', LOGIT+SENDIT)
					.TrackProgress('==================================================================================================================', LOGIT+SENDIT)
				ENDIF
				IF .lTestMode THEN
					.TrackProgress('                        TEST MODE                        ', LOGIT+SENDIT)
					.TrackProgress('==================================================================================================================', LOGIT+SENDIT)
				ENDIF
				.TrackProgress('', LOGIT)

				* calculate pay period start/end dates.
				* We go back from today until we hit a Saturday (DOW = 7) , which is the end day of the previous pay period.
				* then we subtract 6 days from that to get the prior Sunday (DOW = 1), which is the start day of the previous pay period.

				.cToday = DTOC(.dToday)

				ldDate = .dToday
				DO WHILE DOW(ldDate,1) <> 7
					ldDate = ldDate - 1
				ENDDO
				ldEndDate = ldDate
				ldStartDate = ldDate - 6


				************************************************************
				************************************************************
				************************************************************
				* to process non-standard date range, hardcode dates below
				* ONLY WORKS IN TEST MODE!
				*!*					IF .lTestMode THEN
				*!*						ldStartDate = {^2008-09-01}
				*!*						ldEndDate = {^2008-09-30}
				*!*						.lExcludeEmployeesWith9999OnReportDate = .F.
				*!*						.lProcessEPIE8799CSVFile = .F.
				*!*						.lMakeSummaryByHomeDept = .F.
				*!*					ENDIF
				************************************************************
				************************************************************
				************************************************************


				************************************************************
				************************************************************
				************************************************************
				DO CASE
					CASE .cReportType = "TGF"
						* produce a TGF summary report for Lucille
						.lProcessEPIE8799CSVFile = .F.
						.lMakeSummaryByHomeDept = .F.
						lcSpecialWhere = ""
						.cSendTo = ''
						.cCC = 'mbennett@fmiint.com'
						.cADPCompany = "TGF"
					CASE .cReportType = "MONTHLY"
						* produce a summary report for a date range, for whomever
						.lProcessEPIE8799CSVFile = .F.
						.lMakeSummaryByHomeDept = .F.
						lcSpecialWhere = " AND (D.LABORLEV2NM = '03')  AND (D.LABORLEV3NM = '0650') "
						.cSendTo = 'mbennett@fmiint.com'
						.cCC = 'mbennett@fmiint.com'
					CASE .cReportType = "TUG"
						* produce a summary report for TUG div 71, for Ericka
						.lProcessEPIE8799CSVFile = .F.
						.lMakeSummaryByHomeDept = .F.
						lcSpecialWhere = " AND (D.LABORLEV2NM = '71') "
						.cSendTo = ''
						.cCC = 'mbennett@fmiint.com'
					CASE .cReportType = "BRIAN"
						* produce a daily summary report for div 03, for Brian Southwell
						* this report will run every day, reporting on prior day
						ldStartDate = ldYesterday
						ldEndDate = ldYesterday
						.lProcessEPIE8799CSVFile = .F.
						.lMakeSummaryByHomeDept = .F.
						lcSpecialWhere = " AND (D.LABORLEV2NM = '03') "
						.cSendTo = ''
						.cCC = 'mbennett@fmiint.com'
					CASE .cReportType = "JONIELL"
						* produce a summary report for all div 60, for Jimmy O'Neill
						.lProcessEPIE8799CSVFile = .F.
						.lMakeSummaryByHomeDept = .F.
						lcSpecialWhere = " AND (D.LABORLEV2NM = '60') "
						.cSendTo = ''
						.cCC = 'mbennett@fmiint.com'
					CASE .cReportType = "DARREN"
						* update f:\wo\wodata\day.dbf, for Darren
						* this process will run every day, reporting on prior day
						ldStartDate = ldYesterday
						ldEndDate = ldYesterday
						.lProcessEPIE8799CSVFile = .F.
						.lMakeSummaryByHomeDept = .F.
						*lcSpecialWhere = " AND (D.LABORLEV2NM = '03') AND (D.LABORLEV3NM = '0650') "
						lcSpecialWhere = " AND (D.LABORLEV2NM IN ('03','55')) AND (D.LABORLEV3NM = '0650') "
						.cSendTo = 'mbennett@fmiint.com'
						.cCC = ''
					CASE .cReportType = "ALLOC_JONEILL"
						* produce a summary report (FOR PRIOR MONTH) for all div 60 for Jimmy O'Neill & Ken
						.lProcessEPIE8799CSVFile = .F.
						.lMakeSummaryByHomeDept = .F.
						lcSpecialWhere = " AND (D.LABORLEV2NM = '60') "
						.cSendTo = ''
						*.cSendTo = 'mbennett@fmiint.com'
						.cCC = 'mbennett@fmiint.com'
						* calc start/end dates of prior month
						ldStartDate = GOMONTH(.dToday - DAY(.dToday) + 1,-1)
						ldEndDate = .dToday - DAY(.dToday)
					CASE .cReportType = "RALPH_REYES_WTD"
						* produce a WEEK-TO-DATE report for all 03-0650 local drivers for Ralph Reyes per Kenny
						.lProcessEPIE8799CSVFile = .F.
						.lMakeSummaryByHomeDept = .F.
						lcSpecialWhere = " AND (D.LABORLEV2NM = '03')  AND (D.LABORLEV3NM = '0650 ')"
						.cSendTo = ''
						*.cSendTo = 'mbennett@fmiint.com'
						.cCC = 'mbennett@fmiint.com'
						* start date = Sunday of current pay period
						* end date = yesterday (the report will run early each weekday morning)
						* To get start date, we go back from today until we hit a Sunday (DOW = 1) , which is the start of the current pay period.
						ldDate = .dToday
						ldEndDate = ldDate - 1
						DO WHILE DOW(ldDate,1) <> 1
							ldDate = ldDate - 1
						ENDDO
						ldStartDate = ldDate
					OTHERWISE
						* CASE .cReportType = "STANDARD"
						lcSpecialWhere = ""
				ENDCASE

				IF .lTestMode THEN
					.cSendTo = 'mbennett@fmiint.com'
					.cSendToAUDIT = 'mbennett@fmiint.com'
					.cLogFile = 'F:\UTIL\KRONOS\Logfiles\KronosPayrollReport_log_TESTMODE.txt'
				ENDIF
				************************************************************
				************************************************************
				************************************************************


				*!*					* extra where filtering
				*!*					* ONLY WORKS IN TEST MODE!
				*!*					IF .lTestMode THEN
				*!*						lcSpecialWhere = " AND (D.LABORLEV2NM IN ('61','71','72','73','74')) "
				*!*					ENDIF


				SET DATE AMERICAN

				lcStartDate = STRTRAN(DTOC(ldStartDate),"/","-")
				lcEndDate = STRTRAN(DTOC(ldEndDate),"/","-")

				SET DATE YMD

				*	 create "YYYYMMDD" safe date format for use in SQL query
				* NOTE: for SQL we are going to use < EndDate ( not <= ), so we are going to add 1 to that date here for querying purposes.
				lcSQLStartDate = STRTRAN(DTOC(ldStartDate),"/","")
				lcSQLEndDate = STRTRAN(DTOC(ldEndDate + 1),"/","")
				lcSQLToday = STRTRAN(DTOC(ldToday),"/","")
				lcSQLTomorrow = STRTRAN(DTOC(ldTomorrow),"/","")

				SET DATE AMERICAN

				DO CASE
					CASE .cReportType = "TGF"
						.cSubject = 'TGF Time Report for: ' + lcStartDate + " to " + lcEndDate
					CASE .cReportType = "TUG"
						.cSubject = 'TUG Payroll Report for: ' + lcStartDate + " to " + lcEndDate
					CASE .cReportType = "MONTHLY"
						.cSubject = 'Monthly Payroll Report for: ' + lcStartDate + " to " + lcEndDate
					CASE .cReportType = "BRIAN"
						.cSubject = 'Div 03 Payroll Report for: ' + lcStartDate
					CASE .cReportType = "JONIELL"
						.cSubject = 'Div 60 Payroll Report for: ' + lcStartDate + " to " + lcEndDate
					CASE .cReportType = "DARREN"
						.cSubject = 'Updated WODATA\DAY Table for: ' + lcStartDate
					CASE .cReportType = "ALLOC_JONEILL"
						.cSubject = 'Div 60 Allocation Report for: ' + lcStartDate + " to " + lcEndDate
					CASE .cReportType = "RALPH_REYES_WTD"
						.cSubject = 'Local Drivers - Week To Date Timecard Summary for period beginning: ' + lcStartDate
					OTHERWISE
						* CASE .cReportType = "STANDARD"
						.cSubject = 'Kronos Payroll Report for: ' + lcStartDate + " to " + lcEndDate
				ENDCASE

				IF USED('CURWTKHOURS') THEN
					USE IN CURWTKHOURS
				ENDIF

				IF USED('CUR9999EMPS') THEN
					USE IN CUR9999EMPS
				ENDIF

				IF USED('CURAPPROVALS') THEN
					USE IN CURAPPROVALS
				ENDIF

				IF USED('CURMISSINGAPPROVALS') THEN
					USE IN CURMISSINGAPPROVALS
				ENDIF

				IF USED('CURSSNUMS') THEN
					USE IN CURSSNUMS
				ENDIF

				* open FoxPro Time Reviewer table
				IF USED('TIMERVWRTABLE') THEN
					USE IN TIMERVWRTABLE
				ENDIF
				lcFoxProTimeReviewerTable = .cFoxProTimeReviewerTable

				USE (lcFoxProTimeReviewerTable) AGAIN IN 0 ALIAS TIMERVWRTABLE


				* main SQL
				lcSQL = ;
					" SELECT " + ;
					" D.LABORLEV2NM AS DIVISION, " + ;
					" D.LABORLEV3NM AS DEPT, " + ;
					" D.LABORLEV7NM AS TIMERVWR, " + ;
					" C.FULLNM AS EMPLOYEE, " + ;
					" C.PERSONNUM AS FILE_NUM, " + ;
					" C.PERSONID, " + ;
					" E.ABBREVIATIONCHAR AS PAYCODE, " + ;
					" E.NAME AS PAYCODEDESC, " + ;
					" (SUM(A.DURATIONSECSQTY) / 3600.00) AS TOTHOURS, " + ;
					" SUM(A.MONEYAMT) AS TOTPAY " + ;
					" FROM WFCTOTAL A " + ;
					" JOIN WTKEMPLOYEE B " + ;
					" ON B.EMPLOYEEID = A.EMPLOYEEID " + ;
					" JOIN PERSON C " + ;
					" ON C.PERSONID = B.PERSONID " + ;
					" JOIN LABORACCT D " + ;
					" ON D.LABORACCTID = A.LABORACCTID " + ;
					" JOIN PAYCODE E " + ;
					" ON E.PAYCODEID = A.PAYCODEID " + ;
					" WHERE (A.APPLYDTM >= '" + lcSQLStartDate + "')" + ;
					" AND (A.APPLYDTM < '" + lcSQLEndDate + "')" + ;
					" AND (D.LABORLEV1NM = '" + .cADPCompany + "') " + ;
					lcSpecialWhere + ;
					" AND (D.LABORLEV7NM <> '9999') " + ;
					" GROUP BY D.LABORLEV2NM, D.LABORLEV3NM, D.LABORLEV7NM, C.FULLNM, C.PERSONNUM, C.PERSONID, E.ABBREVIATIONCHAR, E.NAME " + ;
					" ORDER BY 1, 2, 3, 4, 5, 7 "

*!*						" AND (D.LABORLEV7NM <> '1018') " + ;

				* SQL for getting the current LABORLEV7NM (looking for 9999s) as of the actual Report Date, because we want to exlude those.
				lcSQL2 = ;
					" SELECT DISTINCT C.PERSONNUM AS FILE_NUM, C.FULLNM AS EMPLOYEE " + ;
					" FROM PERSON C " + ;
					" JOIN WTKEMPLOYEE W " + ;
					" ON W.PERSONID = C.PERSONID " + ;
					" JOIN JAIDS J " + ;
					" ON J.EMPLOYEEID = W.EMPLOYEEID "  + ;
					" JOIN COMBHOMEACCT M " + ;
					" ON M.EMPLOYEEID = J.EMPLOYEEID "  + ;
					" JOIN LABORACCT L " + ;
					" ON L.LABORACCTID = M.LABORACCTID "  + ;
					" WHERE ( L.LABORLEV1NM = '" + .cADPCompany + "' ) " + ;
					" AND ( L.LABORLEV7NM = '9999' ) " + ;
					" AND ( J.PERSONID > 0 ) " + ;
					" AND ( J.DELETEDSW = 0 ) " + ;
					" AND ( M.EXPIRATIONDTM >= '" + lcSQLToday + "')" + ;
					" AND ( M.EFFECTIVEDTM < '" + lcSQLTomorrow + "')" + ;
					" ORDER BY C.FULLNM "

				* SQL for getting Dates approved by Managers for the pay period
				lcSQL3 = ;
					" SELECT " + ;
					" PERSONID, APPROVALDTM, MANAGERID " + ;
					" FROM MGRAPPROVAL " + ;
					" WHERE ( APPROVALDTM >= '" + lcSQLStartDate + "' )" + ;
					"   AND ( APPROVALDTM <= '" + lcSQLEndDate + "' )" + ;
					" ORDER BY PERSONID, APPROVALDTM "

				* SQL for getting ss_num for Darren's processing
				lcSQL4 = ;
					" SELECT CAST(empno AS CHAR) AS FILE_NUM, " + ;
					" CAST(persontaxidno AS CHAR) AS SS_NUM " + ;
					" FROM FMI_ADP_INTERFACE " + ;
					" ORDER BY 1 "


				* 5/16/06 MB: Now using APPLYDTM instead of ADJSTARTDTM to solve problem where hours crossing Midnight Saturday were not being reported.
				*					" WHERE (A.ADJSTARTDTM >= '" + lcSQLStartDate + "')" + ;
				*					" AND (A.ADJSTARTDTM < '" + lcSQLEndDate + "')" + ;

				IF .lTestMode THEN
					.TrackProgress('', LOGIT+SENDIT)
					.TrackProgress('lcSQL =' + lcSQL, LOGIT+SENDIT)
					.TrackProgress('', LOGIT+SENDIT)
					.TrackProgress('lcSQL2 =' + lcSQL2, LOGIT+SENDIT)
					.TrackProgress('', LOGIT+SENDIT)
					.TrackProgress('lcSQL3 =' + lcSQL3, LOGIT+SENDIT)
					.TrackProgress('', LOGIT+SENDIT)
					.TrackProgress('lcSQL4 =' + lcSQL4, LOGIT+SENDIT)
				ENDIF

				.TrackProgress('Connecting to WTK System....', LOGIT+SENDIT)

*!*					* access kronos
*!*					OPEN DATABASE F:\UTIL\KRONOS\KRONOSEDI

*!*					IF .lUsePAYROLL2Server THEN
*!*						.nSQLHandle = SQLCONNECT("WTK_ON_PAYROLL2","sa","kr0n1tes")
*!*					ELSE
*!*						.nSQLHandle = SQLCONNECT("KRONOSEDI","tkcsowner","tkcsowner")
*!*					ENDIF

				.nSQLHandle = SQLCONNECT("WTK2","sa","kr0n1tes")

				IF .nSQLHandle > 0 THEN

					IF .ExecSQL(lcSQL, 'CURWTKHOURS', RETURN_DATA_MANDATORY) AND ;
							.ExecSQL(lcSQL2, 'CUR9999EMPS', RETURN_DATA_NOT_MANDATORY) AND ;
							.ExecSQL(lcSQL3, 'CURAPPROVALS', RETURN_DATA_NOT_MANDATORY) AND ;
							.ExecSQL(lcSQL4, 'CURSSNUMS', RETURN_DATA_MANDATORY) THEN

*!*				SELECT CURWTKHOURS
*!*				BROWSE
*!*				
*!*				x = 2 * .f.

						* create a cursor of the employees who would have had hours/pay in this report,
						* except that we are excluding them because their time reviewer = 9999 on the report date.

						*!*							SELECT DISTINCT ;
						*!*								EMPLOYEE, FILE_NUM ;
						*!*								FROM CURWTKHOURS ;
						*!*								INTO CURSOR CUREXCLUDED9999S ;
						*!*								WHERE FILE_NUM IN (SELECT FILE_NUM FROM CUR9999EMPS) ;
						*!*								AND TIMERVWR IN (SELECT TIMERVWR FROM F:\UTIL\KRONOS\DATA\TIMERVWR WHERE LACTIVE)

						SELECT DISTINCT ;
							EMPLOYEE, FILE_NUM ;
							FROM CURWTKHOURS ;
							INTO CURSOR CUREXCLUDED9999S ;
							WHERE FILE_NUM IN (SELECT FILE_NUM FROM CUR9999EMPS) ;
							AND TIMERVWR IN (SELECT TIMERVWR FROM TIMERVWRTABLE WHERE LACTIVE AND LHOURLY)


						* BUILD A LIST FROM THE CURSOR
						lcExcluded9999s = ""
						IF USED('CUREXCLUDED9999S') AND NOT EOF('CUREXCLUDED9999S') THEN
							SELECT CUREXCLUDED9999S
							SCAN
								lcExcluded9999s = lcExcluded9999s + CRLF + LEFT(CUREXCLUDED9999S.EMPLOYEE,40) + " " + CUREXCLUDED9999S.FILE_NUM
							ENDSCAN
						ENDIF

						IF NOT EMPTY(lcExcluded9999s) THEN
							lcExcluded9999s = "NOTE: the following employees would have had hours in this spreadsheet, " + ;
								"but they were excluded because their Time Reviewer as of today = 9999." + CRLF+ CRLF + ;
								"NAME                                     FILE#" + CRLF + ;
								"---------------------------------------- -----" + ;
								lcExcluded9999s
						ENDIF

						*SELECT TEMP
						*BROWSE

						SELECT CURWTKHOURS
						INDEX ON DIVISION TAG TDIV
						INDEX ON DEPT TAG TDEPT
						INDEX ON EMPLOYEE TAG TEMPLOYEE
						INDEX ON FILE_NUM TAG TFILE_NUM

						****** horizontalize cursor so paycodes are in separate columns

						IF USED('CUREMPLOYEES') THEN
							USE IN CUREMPLOYEES
						ENDIF

						* create cursor with one row per employee which will drive spreadsheet,
						* ONLY FOR THOSE DIV/DEPTS MARKED AS NOT BEING EXPORTED FROM FOXPRO
						* (which means they should be exported from Kronos)

						* decide if we want to exclude ee's with 9999 as Time Reviewer as of the Report Date.
						
						IF .lExcludeEmployeesWith9999OnReportDate THEN
							lc9999Where = " AND FILE_NUM NOT IN (SELECT FILE_NUM FROM CUR9999EMPS) "
						ELSE
							lc9999Where = ""
						ENDIF
						
						DO CASE
							CASE .cReportType = "ALLOC_JONEILL"
								lcTRWhere = " WHERE .T. "
								lcOrderBy = "ORDER BY DEPT"
							CASE .cReportType = "TGF"
								lcTRWhere = " WHERE .T. "
								lcOrderBy = "ORDER BY FILE_NUM"
							OTHERWISE
								lcTRWhere = " WHERE TIMERVWR IN (SELECT TIMERVWR FROM TIMERVWRTABLE WHERE LACTIVE AND LHOURLY) "
								lcOrderBy = "ORDER BY FILE_NUM"
						ENDCASE

*!*							SELECT ;
*!*								DIVISION, ;
*!*								DEPT, ;
*!*								EMPLOYEE, ;
*!*								FILE_NUM, ;
*!*								PERSONID, ;
*!*								TIMERVWR, ;
*!*								0000.00 AS REG_HRS, ;
*!*								0000.00 AS GTD_HRS, ;
*!*								0000.00 AS SD1_HRS, ;
*!*								0000.00 AS SPP_HRS, ;
*!*								0000.00 AS OT_HRS, ;
*!*								0000.00 AS OT20_HRS, ;
*!*								0000.00 AS SICK_HRS, ;
*!*								0000.00 AS VAC_HRS, ;
*!*								0000.00 AS VAC_ADV, ;
*!*								0000.00 AS HDAY_HRS, ;
*!*								0000.00 AS HDAY_ADV, ;
*!*								0000.00 AS PP_VAC_HRS, ;
*!*								0000.00 AS PP_HDAY_HRS, ;
*!*								0000.00 AS PP_PERS_HRS, ;
*!*								0000.00 AS PP_COMP_HRS, ;
*!*								0000.00 AS PERS_HRS, ;
*!*								0000.00 AS PERS_ADV, ;
*!*								0000.00 AS COMP_HRS, ;
*!*								0000.00 AS COMP_ADV, ;
*!*								0000.00 AS JURY_HRS, ;
*!*								0000.00 AS BREAVE_HRS, ;
*!*								0000.00 AS OTHER_HRS, ;
*!*								0000.00 AS UNUSD_SICK, ;
*!*								0000.00 AS RETRO_OTH, ;
*!*								0000.00 AS OTHER_PAY, ;
*!*								0000.00 AS CADRVR_PAY ;
*!*								FROM CURWTKHOURS ;
*!*								INTO CURSOR CUREMPLOYEES ;
*!*								WHERE TIMERVWR IN ;
*!*								(SELECT TIMERVWR FROM TIMERVWRTABLE WHERE LACTIVE) ;
*!*								&lc9999Where. ;
*!*								GROUP BY DIVISION, DEPT, EMPLOYEE, FILE_NUM ;
*!*								ORDER BY FILE_NUM ;
*!*								READWRITE

						SELECT ;
							DIVISION, ;
							DEPT, ;
							EMPLOYEE, ;
							FILE_NUM, ;
							PERSONID, ;
							TIMERVWR, ;
							0000.00 AS REG_HRS, ;
							0000.00 AS GTD_HRS, ;
							0000.00 AS SD1_HRS, ;
							0000.00 AS SPP_HRS, ;
							0000.00 AS OT_HRS, ;
							0000.00 AS OT20_HRS, ;
							0000.00 AS SICK_HRS, ;
							0000.00 AS VAC_HRS, ;
							0000.00 AS VAC_RLVR_HRS, ;
							0000.00 AS VAC_ADV, ;
							0000.00 AS HDAY_HRS, ;
							0000.00 AS HDAY_ADV, ;
							0000.00 AS PP_VAC_HRS, ;
							0000.00 AS PP_HDAY_HRS, ;
							0000.00 AS PP_PERS_HRS, ;
							0000.00 AS PP_COMP_HRS, ;
							0000.00 AS PERS_HRS, ;
							0000.00 AS PERS_ADV, ;
							0000.00 AS COMP_HRS, ;
							0000.00 AS COMP_ADV, ;
							0000.00 AS JURY_HRS, ;
							0000.00 AS BREAVE_HRS, ;
							0000.00 AS OTHER_HRS, ;
							0000.00 AS UNUSD_SICK, ;
							0000.00 AS RETRO_OTH, ;
							0000.00 AS OTHER_PAY, ;
							0000.00 AS CADRVR_PAY, ;
							0000.00 AS PERS_PAY, ;
							0000.00 AS HOURLYRATE ;
							FROM CURWTKHOURS ;
							INTO CURSOR CUREMPLOYEES ;
							&lcTRWhere. ;
							&lc9999Where. ;
							GROUP BY DIVISION, DEPT, EMPLOYEE, FILE_NUM ;
							&lcOrderBy. ;
							READWRITE


						*!*								(SELECT TIMERVWR FROM F:\UTIL\KRONOS\DATA\TIMERVWR WHERE LACTIVE) ;

						lcMissingApprovals = ""
						IF USED('CURAPPROVALS') AND NOT EOF('CURAPPROVALS') THEN
							* we got list of approved dates from Kronos...
							* identify CUREMPLOYEES employees who did not have approvals
							SELECT EMPLOYEE,  LEFT(FILE_NUM,4) AS FILE_NUM, LEFT(TIMERVWR,4) AS TIMERVWR, PERSONID, SPACE(30) AS TRNAME ;
								FROM CUREMPLOYEES ;
								INTO CURSOR CURMISSINGAPPROVALS ;
								WHERE PERSONID NOT IN ( SELECT PERSONID FROM CURAPPROVALS ) ;
								ORDER BY EMPLOYEE ;
								READWRITE


							IF USED('CURMISSINGAPPROVALS') AND NOT EOF('CURMISSINGAPPROVALS') THEN

								* populate Time Reviewer name column
								SELECT CURMISSINGAPPROVALS
								SCAN
									SELECT TIMERVWRTABLE
									LOCATE FOR TIMERVWR == CURMISSINGAPPROVALS.TIMERVWR
									IF FOUND() THEN
										REPLACE CURMISSINGAPPROVALS.TRNAME WITH ALLTRIM(TIMERVWRTABLE.NAME)
									ENDIF
								ENDSCAN

								SELECT TIMERVWRTABLE
								LOCATE

								* build a list for email body
								SELECT CURMISSINGAPPROVALS
								SCAN
									lcMissingApprovals = lcMissingApprovals + CRLF + ;
										LEFT(CURMISSINGAPPROVALS.EMPLOYEE,40) + " " + ;
										CURMISSINGAPPROVALS.FILE_NUM + "  " + ;
										CURMISSINGAPPROVALS.TRNAME + "  # " + ;
										CURMISSINGAPPROVALS.TIMERVWR
								ENDSCAN

								IF NOT EMPTY(lcMissingApprovals) THEN
									lcMissingApprovals = CRLF+ CRLF + ;
										"WARNING: the following employees' timecards have not been approved!" + CRLF+ CRLF + ;
										"NAME                                     FILE# TIME REVIEWER" + CRLF + ;
										"---------------------------------------- ----- --------------------------------------" + ;
										lcMissingApprovals
								ENDIF

							ENDIF
						ENDIF  &&  USED('CURAPPROVALS') AND NOT EOF('CURAPPROVALS')

*!*							* populate hours/code fields in main cursor
*!*						SELECT CURWTKHOURS
*!*						COPY TO c:\a\test.xls XL5
*!*						
*!*						x = 2*.f.
						
						SELECT CUREMPLOYEES
						SCAN
							SCATTER MEMVAR
							STORE 0000.00 TO ;
								m.REG_HRS, ;
								m.GTD_HRS, ;
								m.SD1_HRS, ;
								m.SPP_HRS, ;
								m.OT_HRS, ;
								m.OT20_HRS, ;
								m.SICK_HRS, ;
								m.VAC_HRS, ;
								m.VAC_RLVR_HRS, ;
								m.VAC_ADV, ;
								m.HDAY_HRS, ;
								m.HDAY_ADV, ;
								m.PP_VAC_HRS, ;
								m.PP_HDAY_HRS, ;
								m.PP_PERS_HRS, ;
								m.PP_COMP_HRS, ;
								m.PERS_HRS, ;
								m.PERS_ADV, ;
								m.COMP_HRS, ;
								m.COMP_ADV, ;
								m.JURY_HRS, ;
								m.BREAVE_HRS, ;
								m.OTHER_HRS, ;
								m.UNUSD_SICK, ;
								m.RETRO_OTH, ;
								m.OTHER_PAY, ;
								m.CADRVR_PAY, ;
								m.PERS_PAY

							SELECT CURWTKHOURS
							SCAN FOR ;
									DIVISION = CUREMPLOYEES.DIVISION AND ;
									DEPT = CUREMPLOYEES.DEPT AND ;
									EMPLOYEE = CUREMPLOYEES.EMPLOYEE AND ;
									FILE_NUM = CUREMPLOYEES.FILE_NUM
								lcPayCode = UPPER(ALLTRIM(CURWTKHOURS.PAYCODE))
								DO CASE
									CASE lcPayCode == "REG"
										m.REG_HRS = m.REG_HRS + CURWTKHOURS.TOTHOURS
									CASE lcPayCode == "OTM"
										m.OT_HRS = m.OT_HRS + CURWTKHOURS.TOTHOURS
									CASE lcPayCode == "DTM"
										m.OT20_HRS = m.OT20_HRS + CURWTKHOURS.TOTHOURS
									CASE INLIST(lcPayCode,"VAC","VH7","VAD","VA7")
										m.VAC_HRS = m.VAC_HRS + CURWTKHOURS.TOTHOURS
										*IF lcPayCode == "VAD"
										IF INLIST(lcPayCode,"VAD","VA7")
											m.VAC_ADV = m.VAC_ADV + CURWTKHOURS.TOTHOURS
										ENDIF
									CASE lcPayCode == "RVP"
										* Vacation Rollovers
										m.VAC_RLVR_HRS = m.VAC_RLVR_HRS + CURWTKHOURS.TOTHOURS
									CASE INLIST(lcPayCode,"HOL","HAD")
										m.HDAY_HRS = m.HDAY_HRS + CURWTKHOURS.TOTHOURS
										IF lcPayCode == "HAD" THEN
											m.HDAY_ADV = m.HDAY_ADV + CURWTKHOURS.TOTHOURS
										ENDIF
										*CASE lcPayCode == "SIC"
									CASE INLIST(lcPayCode,"SIC","SC7")
										m.SICK_HRS = m.SICK_HRS + CURWTKHOURS.TOTHOURS
									CASE INLIST(lcPayCode,"BDY","BAD","PRS","PAD")
										m.PERS_HRS = m.PERS_HRS + CURWTKHOURS.TOTHOURS
										IF INLIST(lcPayCode,"BAD","PAD") THEN
											m.PERS_ADV = m.PERS_ADV + CURWTKHOURS.TOTHOURS
										ENDIF
									CASE INLIST(lcPayCode,"CMP","CAD")
										m.COMP_HRS = m.COMP_HRS + CURWTKHOURS.TOTHOURS
										IF lcPayCode == "CAD" THEN
											m.COMP_ADV = m.COMP_ADV + CURWTKHOURS.TOTHOURS
										ENDIF
									CASE lcPayCode == "JUR"
										m.JURY_HRS = m.JURY_HRS + CURWTKHOURS.TOTHOURS
									CASE lcPayCode == "BRV"
										m.BREAVE_HRS = m.BREAVE_HRS + CURWTKHOURS.TOTHOURS
									CASE lcPayCode == "GTD"
										m.GTD_HRS = m.GTD_HRS + CURWTKHOURS.TOTHOURS
									CASE lcPayCode == "OTH"
										m.OTHER_HRS = m.OTHER_HRS + CURWTKHOURS.TOTHOURS
									CASE lcPayCode == "SD1"
										m.SD1_HRS = m.SD1_HRS + CURWTKHOURS.TOTHOURS
									CASE lcPayCode == "PPV"
										m.PP_VAC_HRS = m.PP_VAC_HRS + CURWTKHOURS.TOTHOURS
									CASE lcPayCode == "PPH"
										m.PP_HDAY_HRS = m.PP_HDAY_HRS + CURWTKHOURS.TOTHOURS
									CASE INLIST(lcPayCode,"PPB","PPP")
										m.PP_PERS_HRS = m.PP_PERS_HRS + CURWTKHOURS.TOTHOURS
									CASE lcPayCode == "PPC"
										m.PP_COMP_HRS = m.PP_COMP_HRS + CURWTKHOURS.TOTHOURS
									CASE lcPayCode == "USI"
										m.UNUSD_SICK = m.UNUSD_SICK + CURWTKHOURS.TOTHOURS
									CASE lcPayCode == "SPP"
										m.SPP_HRS = m.SPP_HRS + CURWTKHOURS.TOTHOURS
									CASE lcPayCode == "OTP"
										m.OTHER_PAY = m.OTHER_PAY + CURWTKHOURS.TOTPAY
									CASE lcPayCode == "RTR"
										m.RETRO_OTH = m.RETRO_OTH + CURWTKHOURS.TOTPAY
									CASE lcPayCode == "CAI"
										m.CADRVR_PAY = m.CADRVR_PAY + CURWTKHOURS.TOTPAY
									CASE lcPayCode == "PRS"
										m.PERS_PAY = m.PERS_PAY + CURWTKHOURS.TOTPAY
									OTHERWISE
										* NOTHING
								ENDCASE

							ENDSCAN
							* accumulate totals
							SELECT CUREMPLOYEES
							GATHER MEMVAR
						ENDSCAN
						
						* populate Hourly rates from foxpro, if necessary
						DO CASE
							CASE .cReportType = "ALLOC_JONEILL"
								USE F:\HR\HRDATA\EMPLOYEE IN 0 AGAIN ALIAS FOXEMPLOYEE
								SELECT CUREMPLOYEES
								SCAN
									SELECT FOXEMPLOYEE
									LOCATE FOR ACTIVE AND EMP_NUM = VAL(CUREMPLOYEES.FILE_NUM)
									IF FOUND() THEN
										REPLACE CUREMPLOYEES.HOURLYRATE WITH FOXEMPLOYEE.SALARY
									ELSE
										* ACTIVE status not found, try inactive
										SELECT FOXEMPLOYEE
										LOCATE FOR EMP_NUM = VAL(CUREMPLOYEES.FILE_NUM)
										IF FOUND() THEN
											REPLACE CUREMPLOYEES.HOURLYRATE WITH FOXEMPLOYEE.SALARY
										ENDIF
									ENDIF
								ENDSCAN
								USE IN FOXEMPLOYEE
							OTHERWISE
								* NOTHING
						ENDCASE


						DO CASE
							CASE .cReportType = "DARREN"

								SELECT CUREMPLOYEES
								GOTO TOP
								IF EOF() THEN
									.TrackProgress("There was no data for" + TRANSFORM(ldStartDate), LOGIT+SENDIT)
								ELSE
									IF USED('CURDARREN') THEN
										USE IN CURDARREN
									ENDIF

									WAIT WINDOW NOWAIT "Prepping data..."
									SELECT ;
										DIVISION, ;
										DEPT, ;
										EMPLOYEE, ;
										FILE_NUM, ;
										SPACE(11) AS SS_NUM, ;
										PERSONID, ;
										TIMERVWR, ;
										(REG_HRS + GTD_HRS + OT_HRS) AS HOURS ;
										FROM CUREMPLOYEES ;
										INTO CURSOR CURDARREN ;
										ORDER BY EMPLOYEE ;
										READWRITE

									*!*
									*!*								SELECT CURSSNUMS
									*!*								BROWSE

									* POPULATE SS_NUM IN CURDARREN
									SELECT CURDARREN
									SCAN
										SELECT CURSSNUMS
										LOCATE FOR LEFT(FILE_NUM,4) == LEFT(CURDARREN.FILE_NUM,4)
										IF FOUND() THEN
											REPLACE CURDARREN.SS_NUM WITH CURSSNUMS.SS_NUM
										ENDIF
									ENDSCAN

*!*	SELECT CURDARREN
*!*	BROWSE
*!*	COPY TO c:\a\testdarren.xls xl5
*!*	*SET STEP ON

									* UPDATE DARREN'S DAY TABLE
									WAIT WINDOW NOWAIT "Updating Darren's Day table..."
									PUBLIC gProcess, gUserId
									gProcess = "UPDTDAYS"
									gUserId = "MBENNETT"

									USE F:\WO\WODATA\DAY IN 0 ALIAS DAYTABLE SHARED
									SELECT DAYTABLE
									SCAN FOR ZDATE = ldStartDate
										SELECT CURDARREN
										LOCATE FOR SS_NUM = DAYTABLE.SS_NUM
										IF FOUND() THEN
											REPLACE DAYTABLE.HOURS WITH CURDARREN.HOURS
											.TrackProgress("Updated hours for: " + ALLTRIM(CURDARREN.EMPLOYEE), LOGIT+SENDIT)
										ENDIF
									ENDSCAN

								ENDIF  && eof employee

								CLOSE DATABASES

								WAIT CLEAR

							OTHERWISE

								WAIT WINDOW NOWAIT "Preparing data..."

								SELECT CUREMPLOYEES
								GOTO TOP
								IF EOF() THEN
									.TrackProgress("There was no data to export!", LOGIT+SENDIT)
								ELSE

									lcRateType = "HOURLY"

									WAIT WINDOW NOWAIT "Opening Excel..."
									oExcel = CREATEOBJECT("excel.application")
									oExcel.VISIBLE = .F.
									
									*oWorkbook = oExcel.workbooks.OPEN("F:\UTIL\KRONOS\TEMPLATES\KRONOSPAYROLLREPORT.XLS")
									
									DO CASE
										CASE .cReportType = "ALLOC_JONEILL"
											oWorkbook = oExcel.workbooks.OPEN("F:\UTIL\KRONOS\TEMPLATES\KRONOS_ALLOC_JONEILL.XLS")
*!*											CASE .cReportType = "RALPH_REYES_WTD"
*!*												*oWorkbook = oExcel.workbooks.OPEN("F:\UTIL\KRONOS\TEMPLATES\KRONOS_RALPH_REYES_WTD.XLS")
*!*												oWorkbook = oExcel.workbooks.OPEN("F:\UTIL\KRONOS\TEMPLATES\KRONOS_RALPH_REYES_WTD_2.XLS")
										OTHERWISE
											oWorkbook = oExcel.workbooks.OPEN("F:\UTIL\KRONOS\TEMPLATES\KRONOSPAYROLLREPORT_NEW.XLS")
									ENDCASE
									***********************************************************************************************************************
									***********************************************************************************************************************
									WAIT WINDOW NOWAIT "Looking for target directory..."
									* see if target directory exists, and, if not, create it
									* e.g. F:\timeclk\adpfiles\01-07-2005
									ldPayDate = ldEndDate + 6
									lcFileDate = PADL(MONTH(ldPayDate),2,"0") + "-"  + PADL(DAY(ldPayDate),2,"0") + "-" + PADL(YEAR(ldPayDate),4,"0")

									DO CASE
										CASE .cReportType = "ALLOC_JONEILL"
											lcTargetDirectory = "F:\UTIL\KRONOS\PAYROLLREPORTS\ALLOCATIONS\"
										CASE .cReportType = "RALPH_REYES_WTD"
											lcTargetDirectory = "F:\UTIL\KRONOS\PAYROLLREPORTS\WTD-LOCALDRIVERS\"
										OTHERWISE
											lcTargetDirectory = "F:\UTIL\KRONOS\PAYROLLREPORTS\" + lcFileDate + "\"
									ENDCASE
									* for testing
									*lcTargetDirectory = "C:\TIMECLK\ADPFILES\" + lcFileDate + "\"

									*!*								IF .lDoSpecialTUGReport THEN
									*!*									lcFiletoSaveAs = lcTargetDirectory + lcFileDate + " TUG Div 71 " + lcRateType
									*!*								ELSE
									*!*									lcFiletoSaveAs = lcTargetDirectory + lcFileDate + " Kronos " + lcRateType
									*!*								ENDIF

									DO CASE
										CASE .cReportType = "MONTHLY"
											lcFiletoSaveAs = lcTargetDirectory + lcFileDate + " Monthly " + lcRateType
										CASE .cReportType = "TGF"
											lcFiletoSaveAs = lcTargetDirectory + lcFileDate + " TGF " + lcRateType
										CASE .cReportType = "TUG"
											lcFiletoSaveAs = lcTargetDirectory + lcFileDate + " TUG Div 71 " + lcRateType
										CASE .cReportType = "BRIAN"
											lcFiletoSaveAs = lcTargetDirectory + lcFileDate + " Div 03 " + lcRateType
										CASE .cReportType = "JONIELL"
											lcFiletoSaveAs = lcTargetDirectory + lcFileDate + " Div 60 " + lcRateType
										CASE .cReportType = "ALLOC_JONEILL"
											lcFiletoSaveAs = lcTargetDirectory + lcFileDate + " Div 60 Allocation" 
										CASE .cReportType = "RALPH_REYES_WTD"
											lcFiletoSaveAs = lcTargetDirectory + lcFileDate + " WTD for Local Drivers" 
										OTHERWISE
											* CASE .cReportType = "STANDARD"
											lcFiletoSaveAs = lcTargetDirectory + lcFileDate + " Kronos " + lcRateType
									ENDCASE
									lcXLFileName = lcFiletoSaveAs + ".XLS"

									* create directory if it doesn't exist
									IF NOT DIRECTORY(lcTargetDirectory) THEN
										MKDIR (lcTargetDirectory)
										WAIT WINDOW TIMEOUT 1 "Made Target Directory " + lcTargetDirectory
									ENDIF

									* if target directory exists, save there
									llSaveAgain = .F.
									IF DIRECTORY(lcTargetDirectory) THEN
										IF FILE(lcXLFileName) THEN
											DELETE FILE (lcXLFileName)
										ENDIF
										oWorkbook.SAVEAS(lcFiletoSaveAs)
										* set flag to save again after sheet is populated
										llSaveAgain = .T.
									ENDIF
									***********************************************************************************************************************
									***********************************************************************************************************************
									WAIT WINDOW NOWAIT "Building Payroll Report spreadsheet..."

									lnRow = 3
									lnStartRow = lnRow + 1
									lcStartRow = ALLTRIM(STR(lnStartRow))

									oWorksheet = oWorkbook.Worksheets[1]
									oWorksheet.RANGE("A" + lcStartRow,"Y1000").clearcontents()

									oWorksheet.RANGE("A" + lcStartRow,"Y1000").FONT.SIZE = 10
									oWorksheet.RANGE("A" + lcStartRow,"Y1000").FONT.NAME = "Arial Narrow"
									oWorksheet.RANGE("A" + lcStartRow,"Y1000").FONT.bold = .T.

									*!*								IF .lDoSpecialTUGReport THEN
									*!*									lcTitle = "TUG Payroll Summary for Pay Date: " + DTOC(ldPayDate) + ", Week of: " + lcStartDate + " - " + lcEndDate
									*!*								ELSE
									*!*									lcTitle = "Kronos Payroll Summary for Pay Date: " + DTOC(ldPayDate) + ", Week of: " + lcStartDate + " - " + lcEndDate
									*!*								ENDIF
									DO CASE
										CASE .cReportType = "MONTHLY"
											lcTitle = "Monthly Payroll Summary for " + lcStartDate + " - " + lcEndDate
										CASE .cReportType = "TGF"
											lcTitle = "TGF Time Summary for Pay Date: " + DTOC(ldPayDate) + ", Week of: " + lcStartDate + " - " + lcEndDate
										CASE .cReportType = "TUG"
											lcTitle = "TUG Payroll Summary for Pay Date: " + DTOC(ldPayDate) + ", Week of: " + lcStartDate + " - " + lcEndDate
										CASE .cReportType = "BRIAN"
											lcTitle = "Div 03 Payroll Summary for Date: " + lcStartDate
										CASE .cReportType = "JONIELL"
											lcTitle = "Div 60 Payroll Summary for Pay Date: " + DTOC(ldPayDate) + ", Week of: " + lcStartDate + " - " + lcEndDate
										CASE .cReportType = "ALLOC_JONEILL"
											lcTitle = "Div 60 Allocation Summary for Month of: " + lcStartDate + " - " + lcEndDate
										CASE .cReportType = "RALPH_REYES_WTD"
											lcTitle = 'Local Drivers - Week To Date Timecard Summary' + CRLF + 'for period beginning: ' + lcStartDate

										OTHERWISE
											* CASE .cReportType = "STANDARD"
											lcTitle = "Kronos Payroll Summary for Pay Date: " + DTOC(ldPayDate) + ", Week of: " + lcStartDate + " - " + lcEndDate
									ENDCASE
									oWorksheet.RANGE("A1").VALUE = lcTitle

									IF .cReportType = "RALPH_REYES_WTD" THEN
									
										oWorksheet.RANGE("D" + lcStartRow,"U1000").FONT.bold = .F.
										
										* main scan/processing
										SELECT CUREMPLOYEES
										SCAN
											lnRow = lnRow + 1
											lcRow = LTRIM(STR(lnRow))
											oWorksheet.RANGE("A" + lcRow).VALUE = CUREMPLOYEES.EMPLOYEE
											oWorksheet.RANGE("B" + lcRow).VALUE = CUREMPLOYEES.FILE_NUM
											oWorksheet.RANGE("C" + lcRow).FORMULA = "=SUM(D" + lcRow + "..U" + lcRow + ")"											
											oWorksheet.RANGE("D" + lcRow).VALUE = CUREMPLOYEES.REG_HRS
											oWorksheet.RANGE("E" + lcRow).VALUE = CUREMPLOYEES.GTD_HRS
											oWorksheet.RANGE("F" + lcRow).VALUE = CUREMPLOYEES.SD1_HRS
											oWorksheet.RANGE("G" + lcRow).VALUE = CUREMPLOYEES.OT_HRS
											oWorksheet.RANGE("H" + lcRow).VALUE = CUREMPLOYEES.OT20_HRS
											oWorksheet.RANGE("I" + lcRow).VALUE = CUREMPLOYEES.SICK_HRS
											*oWorksheet.RANGE("J" + lcRow).VALUE = CUREMPLOYEES.VAC_HRS
											oWorksheet.RANGE("J" + lcRow).VALUE =  ( CUREMPLOYEES.VAC_HRS + CUREMPLOYEES.VAC_RLVR_HRS )
											oWorksheet.RANGE("K" + lcRow).VALUE = CUREMPLOYEES.HDAY_HRS
											oWorksheet.RANGE("L" + lcRow).VALUE = CUREMPLOYEES.PERS_HRS
											oWorksheet.RANGE("M" + lcRow).VALUE = CUREMPLOYEES.COMP_HRS
											oWorksheet.RANGE("N" + lcRow).VALUE = CUREMPLOYEES.JURY_HRS
											oWorksheet.RANGE("O" + lcRow).VALUE = CUREMPLOYEES.BREAVE_HRS
											oWorksheet.RANGE("P" + lcRow).VALUE = CUREMPLOYEES.OTHER_HRS									
											oWorksheet.RANGE("Q" + lcRow).VALUE = CUREMPLOYEES.PP_VAC_HRS
											oWorksheet.RANGE("R" + lcRow).VALUE = CUREMPLOYEES.PP_HDAY_HRS
											oWorksheet.RANGE("S" + lcRow).VALUE = CUREMPLOYEES.PP_PERS_HRS
											oWorksheet.RANGE("T" + lcRow).VALUE = CUREMPLOYEES.PP_COMP_HRS
											oWorksheet.RANGE("U" + lcRow).VALUE = CUREMPLOYEES.UNUSD_SICK
											oWorksheet.RANGE("A" + lcRow,"U" + lcRow).Interior.ColorIndex = IIF(((lnRow % 2) = 1),15,0)											
										ENDSCAN
										lnEndRow = lnRow
										lcEndRow = ALLTRIM(STR(lnEndRow))
										lcColsToProcess = "DEFGHIJKLMNOPQRSTU"
										FOR i = 1 TO LEN(lcColsToProcess)
											lcBoldColumn = SUBSTR(lcColsToProcess,i,1)
											FOR j = lnStartRow TO lnEndRow
												lcBoldRow = ALLTRIM(STR(j))
												luCellValue = oWorksheet.RANGE(lcBoldColumn + lcBoldRow).VALUE
												IF TYPE([luCellValue])= "N" THEN
													IF luCellValue > 0.0 THEN
														*oWorksheet.RANGE(lcBoldColumn + lcBoldRow).FONT.bold = .T.
														* nothing
													ELSE
														oWorksheet.RANGE(lcBoldColumn + lcBoldRow).VALUE = ""
													ENDIF
												ENDIF
											ENDFOR  && j
										ENDFOR  && i										
										lnRow = lnRow + 2
										lcRow = LTRIM(STR(lnRow))
										oWorksheet.RANGE("A" + lcRow,"U" + lcRow).FONT.SIZE = 9
										oWorksheet.RANGE("A" + lcRow).VALUE = "Totals:"
										oWorksheet.RANGE("C" + lcRow).formula = "=sum(D" + lcStartRow + ":D" + lcEndRow + ")"
									ELSE

										* main scan/processing
										SELECT CUREMPLOYEES
										STORE 0 TO num_salary
										SCAN

											lnRow = lnRow + 1
											lcRow = LTRIM(STR(lnRow))
											oWorksheet.RANGE("A" + lcRow).VALUE = "'" + CUREMPLOYEES.DIVISION
											oWorksheet.RANGE("B" + lcRow).VALUE = CUREMPLOYEES.DEPT
											oWorksheet.RANGE("C" + lcRow).VALUE = CUREMPLOYEES.EMPLOYEE
											oWorksheet.RANGE("D" + lcRow).VALUE = CUREMPLOYEES.FILE_NUM
											oWorksheet.RANGE("E" + lcRow).VALUE = CUREMPLOYEES.REG_HRS
											oWorksheet.RANGE("F" + lcRow).VALUE = CUREMPLOYEES.GTD_HRS
											oWorksheet.RANGE("G" + lcRow).VALUE = CUREMPLOYEES.SD1_HRS
											oWorksheet.RANGE("H" + lcRow).VALUE = CUREMPLOYEES.OT_HRS
											oWorksheet.RANGE("I" + lcRow).VALUE = CUREMPLOYEES.OT20_HRS
											oWorksheet.RANGE("J" + lcRow).VALUE = CUREMPLOYEES.SICK_HRS
											*oWorksheet.RANGE("K" + lcRow).VALUE = CUREMPLOYEES.VAC_HRS
											oWorksheet.RANGE("K" + lcRow).VALUE = ( CUREMPLOYEES.VAC_HRS + CUREMPLOYEES.VAC_RLVR_HRS )
											oWorksheet.RANGE("L" + lcRow).VALUE = CUREMPLOYEES.HDAY_HRS
											oWorksheet.RANGE("M" + lcRow).VALUE = CUREMPLOYEES.PERS_HRS
											oWorksheet.RANGE("N" + lcRow).VALUE = CUREMPLOYEES.COMP_HRS

											oWorksheet.RANGE("O" + lcRow).VALUE = CUREMPLOYEES.JURY_HRS
											oWorksheet.RANGE("P" + lcRow).VALUE = CUREMPLOYEES.BREAVE_HRS
											oWorksheet.RANGE("Q" + lcRow).VALUE = CUREMPLOYEES.OTHER_HRS
											
											DO CASE
												CASE .cReportType = "ALLOC_JONEILL"
												
													oWorksheet.RANGE("R" + lcRow).VALUE = CUREMPLOYEES.HOURLYRATE
													
													lnStraightHours = CUREMPLOYEES.REG_HRS + ;
														CUREMPLOYEES.GTD_HRS + ;
														CUREMPLOYEES.SD1_HRS + ;
														CUREMPLOYEES.SICK_HRS + ;
														CUREMPLOYEES.VAC_HRS + ;
														CUREMPLOYEES.VAC_RLVR_HRS + ;
														CUREMPLOYEES.HDAY_HRS + ;
														CUREMPLOYEES.PERS_HRS + ;
														CUREMPLOYEES.COMP_HRS + ;
														CUREMPLOYEES.JURY_HRS + ;
														CUREMPLOYEES.BREAVE_HRS + ;
														CUREMPLOYEES.OTHER_HRS
													
													lnStraightDollars = CUREMPLOYEES.HOURLYRATE * lnStraightHours
													lnOTDollars = ( 1.5 * CUREMPLOYEES.HOURLYRATE * CUREMPLOYEES.OT_HRS ) + ;
														( 2.0 * CUREMPLOYEES.HOURLYRATE * CUREMPLOYEES.OT20_HRS )
													oWorksheet.RANGE("S" + lcRow).VALUE = lnStraightDollars
													oWorksheet.RANGE("T" + lcRow).VALUE = lnOTDollars
													oWorksheet.RANGE("U" + lcRow).VALUE = lnStraightDollars + lnOTDollars
													
													oWorksheet.RANGE("R"  + lcRow,"U" + lcRow).NumberFormat = "$#,##0.00"
												
													oWorksheet.RANGE("A" + lcRow,"U" + lcRow).Interior.ColorIndex = IIF(((lnRow % 2) = 1),15,0)
												OTHERWISE
													oWorksheet.RANGE("R" + lcRow).VALUE = CUREMPLOYEES.PP_VAC_HRS
													oWorksheet.RANGE("S" + lcRow).VALUE = CUREMPLOYEES.PP_HDAY_HRS
													oWorksheet.RANGE("T" + lcRow).VALUE = CUREMPLOYEES.PP_PERS_HRS
													oWorksheet.RANGE("U" + lcRow).VALUE = CUREMPLOYEES.PP_COMP_HRS

													oWorksheet.RANGE("V" + lcRow).VALUE = CUREMPLOYEES.UNUSD_SICK
													oWorksheet.RANGE("W" + lcRow).VALUE = CUREMPLOYEES.RETRO_OTH
													oWorksheet.RANGE("X" + lcRow).VALUE = CUREMPLOYEES.OTHER_PAY
													oWorksheet.RANGE("Y" + lcRow).VALUE = CUREMPLOYEES.CADRVR_PAY

													oWorksheet.RANGE("A" + lcRow,"Y" + lcRow).Interior.ColorIndex = IIF(((lnRow % 2) = 1),15,0)
											ENDCASE

											* insert NOPAYs where all values (except prepaid hrs) were zero.
											IF CUREMPLOYEES.REG_HRS = 0.0 ;
												AND CUREMPLOYEES.GTD_HRS = 0.0 ;
												AND CUREMPLOYEES.SD1_HRS = 0.0 ;
												AND CUREMPLOYEES.OT_HRS = 0.0 ;
												AND CUREMPLOYEES.OT20_HRS = 0.0 ;
												AND CUREMPLOYEES.SICK_HRS = 0.0 ;
												AND CUREMPLOYEES.VAC_HRS = 0.0 ;
												AND CUREMPLOYEES.VAC_RLVR_HRS = 0.0 ;
												AND CUREMPLOYEES.HDAY_HRS = 0.0 ;
												AND CUREMPLOYEES.PERS_HRS = 0.0 ;
												AND CUREMPLOYEES.COMP_HRS = 0.0 ;
												AND CUREMPLOYEES.JURY_HRS = 0.0 ;
												AND CUREMPLOYEES.BREAVE_HRS = 0.0 ;
												AND CUREMPLOYEES.OTHER_HRS = 0.0 ;
												AND CUREMPLOYEES.UNUSD_SICK = 0.0 ;
												AND CUREMPLOYEES.RETRO_OTH = 0.0 ;
												AND CUREMPLOYEES.OTHER_PAY = 0.0 ;
												AND CUREMPLOYEES.CADRVR_PAY = 0.0 ;
												AND CUREMPLOYEES.PERS_PAY = 0.0 THEN
													oWorksheet.RANGE("E"+lcRow).VALUE="NOPAY"
											ENDIF
											******************************************
											
										ENDSCAN

										lnEndRow = lnRow
										lcEndRow = ALLTRIM(STR(lnEndRow))

										***********************************
										* 01/20/04 MB - do all the bolding/clearing of numeric columns here in one nested loop,
										* to eliminate a lot of extraneous code above.
										lcColsToProcess = "EFGHIJKLMNOPQRSTUVWXY"
										FOR i = 1 TO LEN(lcColsToProcess)
											lcBoldColumn = SUBSTR(lcColsToProcess,i,1)
											FOR j = lnStartRow TO lnEndRow
												lcBoldRow = ALLTRIM(STR(j))
												luCellValue = oWorksheet.RANGE(lcBoldColumn + lcBoldRow).VALUE
												IF TYPE([luCellValue])= "N" THEN
													IF luCellValue > 0.0 THEN
														oWorksheet.RANGE(lcBoldColumn + lcBoldRow).FONT.bold = .T.
													ELSE
														oWorksheet.RANGE(lcBoldColumn + lcBoldRow).VALUE = ""
													ENDIF
												ENDIF
											ENDFOR  && j
										ENDFOR  && i

										* FORMAT LAST FEW COLUMNS FOR CURRENCY
										oWorksheet.RANGE("W" + lcStartRow,"Y1000").NumberFormat = "$#,##0.00"

										***********************************

										*!*								lcJJ = lcRow

										lnRow = lnRow + 2
										lcRow = LTRIM(STR(lnRow))
										oWorksheet.RANGE("C" + lcRow).VALUE = "Control Numbers"
										lnTotalsRow = lnRow + 1
										lcRow = LTRIM(STR(lnTotalsRow))

										oWorksheet.RANGE("A" + lcRow,"Y" + lcRow).FONT.SIZE = 9

										oWorksheet.RANGE("D" + lcRow).formula = "=sum(D" + lcStartRow + ":D" + lcEndRow + ")"
										oWorksheet.RANGE("E" + lcRow).formula = "=sum(E" + lcStartRow + ":E" + lcEndRow + ")"
										oWorksheet.RANGE("F" + lcRow).formula = "=sum(F" + lcStartRow + ":F" + lcEndRow + ")"
										oWorksheet.RANGE("G" + lcRow).formula = "=sum(G" + lcStartRow + ":G" + lcEndRow + ")"
										oWorksheet.RANGE("H" + lcRow).formula = "=sum(H" + lcStartRow + ":H" + lcEndRow + ")"
										oWorksheet.RANGE("I" + lcRow).formula = "=sum(I" + lcStartRow + ":I" + lcEndRow + ")"
										oWorksheet.RANGE("J" + lcRow).formula = "=sum(J" + lcStartRow + ":J" + lcEndRow + ")"
										oWorksheet.RANGE("K" + lcRow).formula = "=sum(K" + lcStartRow + ":K" + lcEndRow + ")"
										oWorksheet.RANGE("L" + lcRow).formula = "=sum(L" + lcStartRow + ":L" + lcEndRow + ")"
										oWorksheet.RANGE("M" + lcRow).formula = "=sum(M" + lcStartRow + ":M" + lcEndRow + ")"
										oWorksheet.RANGE("N" + lcRow).formula = "=sum(N" + lcStartRow + ":N" + lcEndRow + ")"
										oWorksheet.RANGE("O" + lcRow).formula = "=sum(O" + lcStartRow + ":O" + lcEndRow + ")"
										oWorksheet.RANGE("P" + lcRow).formula = "=sum(P" + lcStartRow + ":P" + lcEndRow + ")"
										oWorksheet.RANGE("Q" + lcRow).formula = "=sum(Q" + lcStartRow + ":Q" + lcEndRow + ")"
										oWorksheet.RANGE("R" + lcRow).formula = "=sum(R" + lcStartRow + ":R" + lcEndRow + ")"
										oWorksheet.RANGE("S" + lcRow).formula = "=sum(S" + lcStartRow + ":S" + lcEndRow + ")"
										oWorksheet.RANGE("T" + lcRow).formula = "=sum(T" + lcStartRow + ":T" + lcEndRow + ")"
										oWorksheet.RANGE("U" + lcRow).formula = "=sum(U" + lcStartRow + ":U" + lcEndRow + ")"									

									ENDIF && .cReportType = "RALPH_REYES_WTD" 

										
									DO CASE
										CASE .cReportType = "ALLOC_JONEILL"
											oWorksheet.PageSetup.PrintArea = "$A$"  + lcStartRow + ":$U$" + lcRow
										CASE .cReportType = "RALPH_REYES_WTD"
											oWorksheet.PageSetup.PrintArea = "$A$"  + lcStartRow + ":$U$" + lcRow
										OTHERWISE
											oWorksheet.RANGE("V" + lcRow).formula = "=sum(V" + lcStartRow + ":V" + lcEndRow + ")"
											oWorksheet.RANGE("W" + lcRow).formula = "=sum(W" + lcStartRow + ":W" + lcEndRow + ")"
											oWorksheet.RANGE("X" + lcRow).formula = "=sum(X" + lcStartRow + ":X" + lcEndRow + ")"
											oWorksheet.RANGE("Y" + lcRow).formula = "=sum(Y" + lcStartRow + ":Y" + lcEndRow + ")"

											oWorksheet.RANGE("C" + lcRow).VALUE = "Salaries= " + ALLTRIM(STR(num_salary))

											* make sure the reference numbers come out on white background
											oWorksheet.RANGE("A" + lcRow,"Y" + lcRow).Interior.ColorIndex = 0

											lnRow = lnRow + 1
											lcRow = LTRIM(STR(lnRow))

											* subtract val of file# from column total for all NOPAYs
											FOR lnRow = lnStartRow TO lnEndRow
												lcRow = ALLTRIM(STR(lnRow))
												IF TYPE([oworksheet.range("E" + lcRow).value]) = "C"
													IF oWorksheet.RANGE("E" + lcRow).VALUE = "NOPAY"
														oWorksheet.RANGE("D" + ALLTRIM(STR(lnTotalsRow))).formula = ;
															oWorksheet.RANGE("D" + ALLTRIM(STR(lnTotalsRow))).formula + "-" + ;
															ALLTRIM(STR(oWorksheet.RANGE("D" + lcRow).VALUE))
													ENDIF
												ENDIF
											ENDFOR
											*******************************************************************************************
											*******************************************************************************************
											** 4/19/06 MB -- list those employees who got Advances, because those are not broken out
											** as separate columns.

											* HEADINGS
											lnRow = lnTotalsRow + 2
											lcRow = LTRIM(STR(lnRow))
											oWorksheet.RANGE("A" + lcRow).VALUE = "The following employees had advances:"

											lnRow = lnRow + 1
											lcRow = LTRIM(STR(lnRow))
											oWorksheet.RANGE("A" + lcRow).VALUE = "Div"
											oWorksheet.RANGE("B" + lcRow).VALUE = "Dept"
											oWorksheet.RANGE("C" + lcRow).VALUE = "Employee Name"
											oWorksheet.RANGE("D" + lcRow).VALUE = "Emp#"
											oWorksheet.RANGE("E" + lcRow).VALUE = "Vac Adv"
											oWorksheet.RANGE("F" + lcRow).VALUE = "Hol Adv"
											oWorksheet.RANGE("G" + lcRow).VALUE = "Pers./BDay Adv"
											oWorksheet.RANGE("H" + lcRow).VALUE = "Comp Adv"
											oWorksheet.RANGE("A" + lcRow + ":" + "H" + lcRow).FONT.bold = .T.

											* list
											SELECT CUREMPLOYEES
											SCAN FOR (VAC_ADV <> 0) OR (HDAY_ADV <> 0) OR (PERS_ADV <> 0) OR (COMP_ADV <> 0)

												lnRow = lnRow + 1
												lcRow = LTRIM(STR(lnRow))
												oWorksheet.RANGE("A" + lcRow).VALUE = "'" + CUREMPLOYEES.DIVISION
												oWorksheet.RANGE("B" + lcRow).VALUE = CUREMPLOYEES.DEPT
												oWorksheet.RANGE("C" + lcRow).VALUE = CUREMPLOYEES.EMPLOYEE
												oWorksheet.RANGE("D" + lcRow).VALUE = CUREMPLOYEES.FILE_NUM
												oWorksheet.RANGE("E" + lcRow).VALUE = CUREMPLOYEES.VAC_ADV
												oWorksheet.RANGE("F" + lcRow).VALUE = CUREMPLOYEES.HDAY_ADV
												oWorksheet.RANGE("G" + lcRow).VALUE = CUREMPLOYEES.PERS_ADV
												oWorksheet.RANGE("H" + lcRow).VALUE = CUREMPLOYEES.COMP_ADV

											ENDSCAN
											
										oWorksheet.PageSetup.PrintArea = "$A$"  + lcStartRow + ":$X$" + lcRow

									ENDCASE
									*******************************************************************************************
									*******************************************************************************************

									* save again
									IF llSaveAgain THEN
										oWorkbook.SAVE()
									ENDIF
									oWorkbook.CLOSE()

									*******************************************************************************************
									*******************************************************************************************
									IF .lProcessEPIE8799CSVFile THEN

										* now open the ADP Payroll export file and build a list of File#s that are in it
										oWorkbook = oExcel.workbooks.OPEN("F:\UTIL\KRONOS\EDI\ADPFILES\EPIE8799.CSV")
										oWorksheet = oWorkbook.Worksheets[1]

										IF USED('CURACTUALPAYEXPORT') THEN
											USE IN CURACTUALPAYEXPORT
										ENDIF

										CREATE CURSOR CURACTUALPAYEXPORT ( FILE_NUM INT )

										FOR lnRow = 2 TO 10000
											lcRow = ALLTRIM(STR(lnRow))
											* get File# from Column C for any row which has "EPIP50" in Col B
											luValue = oWorksheet.RANGE("B" + lcRow).VALUE
											IF (NOT ISNULL(luValue)) AND (NOT EMPTY(luValue)) AND (TYPE('luValue') = 'C') AND (ALLTRIM(luValue) = "EPIP50") THEN
												m.FILE_NUM = oWorksheet.RANGE("C" + lcRow).VALUE
												INSERT INTO CURACTUALPAYEXPORT FROM MEMVAR
											ELSE
												* we're done, stop looking
												EXIT FOR
											ENDIF
										ENDFOR

										*oExcel.VISIBLE = .T.

									ENDIF && .lProcessEPIE8799CSVFile

									**************************************************************************************************************
									**************************************************************************************************************
									**************************************************************************************************************
									**************************************************************************************************************

									IF .lMakeSummaryByHomeDept THEN

										WAIT WINDOW NOWAIT "Preparing Payroll Summary by HomeDept..."

										lcAuditSubject = 'Kronos Payroll Summary by HomeDept for: ' + lcStartDate + " to " + lcEndDate

										* CREATE A NEW AUDIT REPORT WITH SUBTOTALS BY HOMEDEPT
										IF USED('CURAUDIT') THEN
											USE IN CURAUDIT
										ENDIF

										SELECT ;
											(ALLTRIM(DIVISION) + ALLTRIM(DEPT)) AS HOMEDEPT, ;
											SUM(REG_HRS) AS REG_HRS, ;
											SUM(GTD_HRS) AS GTD_HRS, ;
											SUM(SD1_HRS) AS SD1_HRS, ;
											SUM(SPP_HRS) AS SPP_HRS, ;
											SUM(OT_HRS) AS OT_HRS, ;
											SUM(OT20_HRS) AS OT20_HRS, ;
											SUM(SICK_HRS) AS SICK_HRS, ;
											SUM(VAC_HRS + VAC_RLVR_HRS) AS VAC_HRS, ;
											SUM(VAC_ADV) AS VAC_ADV, ;
											SUM(HDAY_HRS) AS HDAY_HRS, ;
											SUM(HDAY_ADV) AS HDAY_ADV, ;
											SUM(PP_VAC_HRS) AS PP_VAC_HRS, ;
											SUM(PP_HDAY_HRS) AS PP_HDAY_HRS, ;
											SUM(PP_PERS_HRS) AS PP_PERS_HRS, ;
											SUM(PP_COMP_HRS) AS PP_COMP_HRS, ;
											SUM(PERS_HRS) AS PERS_HRS, ;
											SUM(PERS_ADV) AS PERS_ADV, ;
											SUM(COMP_HRS) AS COMP_HRS, ;
											SUM(JURY_HRS) AS JURY_HRS, ;
											SUM(BREAVE_HRS) AS BREAVE_HRS, ;
											SUM(OTHER_HRS) AS OTHER_HRS, ;
											SUM(UNUSD_SICK) AS UNUSD_SICK, ;
											SUM(RETRO_OTH) AS RETRO_OTH, ;
											SUM(OTHER_PAY) AS OTHER_PAY, ;
											SUM(CADRVR_PAY) AS CADRVR_PAY, ;
											SUM(PERS_PAY) AS PERS_PAY ;
											FROM CUREMPLOYEES ;
											INTO CURSOR CURAUDIT ;
											GROUP BY 1 ;
											ORDER BY 1

										*SELECT CURAUDIT
										*BROWSE

										*oWorkbookAUDIT = oExcel.workbooks.OPEN("F:\UTIL\KRONOS\TEMPLATES\KRONOSPAYROLLREPORT_AUDIT.XLS")
										oWorkbookAUDIT = oExcel.workbooks.OPEN("F:\UTIL\KRONOS\TEMPLATES\KRONOSPAYROLLREPORT_AUDIT_NEW.XLS")

										***********************************************************************************************************************
										***********************************************************************************************************************
										WAIT WINDOW NOWAIT "Looking for target directory..."
										* see if target directory exists, and, if not, create it
										* e.g. F:\timeclk\adpfiles\01-07-2005
										ldPayDate = ldEndDate + 6
										lcFileDate = PADL(MONTH(ldPayDate),2,"0") + "-"  + PADL(DAY(ldPayDate),2,"0") + "-" + PADL(YEAR(ldPayDate),4,"0")

										lcTargetDirectory = "F:\UTIL\KRONOS\PAYROLLREPORTS\" + lcFileDate + "\"
										* for testing
										*lcTargetDirectory = "C:\TIMECLK\ADPFILES\" + lcFileDate + "\"

										lcFiletoSaveAs = lcTargetDirectory + lcFileDate + " Kronos Audit HomeDept Summary " + lcRateType
										lcXLFileNameAUDIT = lcFiletoSaveAs + ".XLS"

										* create directory if it doesn't exist
										IF NOT DIRECTORY(lcTargetDirectory) THEN
											MKDIR (lcTargetDirectory)
											WAIT WINDOW TIMEOUT 1 "Made Target Directory " + lcTargetDirectory
										ENDIF

										* if target directory exists, save there
										llSaveAgain = .F.
										IF DIRECTORY(lcTargetDirectory) THEN
											IF FILE(lcXLFileNameAUDIT) THEN
												DELETE FILE (lcXLFileNameAUDIT)
											ENDIF
											oWorkbookAUDIT.SAVEAS(lcFiletoSaveAs)
											* set flag to save again after sheet is populated
											llSaveAgain = .T.
										ENDIF
										***********************************************************************************************************************
										***********************************************************************************************************************
										WAIT WINDOW NOWAIT "Building Payroll Summary by HomeDept spreadsheet..."

										lnRow = 3
										lnStartRow = lnRow + 1
										lcStartRow = ALLTRIM(STR(lnStartRow))

										oExcel.VISIBLE = .F.
										oWorksheetAUDIT = oWorkbookAUDIT.Worksheets[1]
										oWorksheetAUDIT.RANGE("A" + lcStartRow,"V1000").clearcontents()

										oWorksheetAUDIT.RANGE("A" + lcStartRow,"V1000").FONT.SIZE = 10
										oWorksheetAUDIT.RANGE("A" + lcStartRow,"V1000").FONT.NAME = "Arial Narrow"
										oWorksheetAUDIT.RANGE("A" + lcStartRow,"V1000").FONT.bold = .T.

										lcTitle = "Kronos Payroll Summary by HomeDept for Pay Date: " + DTOC(ldPayDate) + ", Week of: " + lcStartDate + " - " + lcEndDate
										oWorksheetAUDIT.RANGE("A1").VALUE = lcTitle


										* main scan/processing
										SELECT CURAUDIT
										SCAN

											lnRow = lnRow + 1
											lcRow = LTRIM(STR(lnRow))
											oWorksheetAUDIT.RANGE("A" + lcRow).VALUE = "'" + CURAUDIT.HOMEDEPT
											oWorksheetAUDIT.RANGE("B" + lcRow).VALUE = CURAUDIT.REG_HRS
											oWorksheetAUDIT.RANGE("C" + lcRow).VALUE = CURAUDIT.GTD_HRS
											oWorksheetAUDIT.RANGE("D" + lcRow).VALUE = CURAUDIT.SD1_HRS
											oWorksheetAUDIT.RANGE("E" + lcRow).VALUE = CURAUDIT.OT_HRS
											oWorksheetAUDIT.RANGE("F" + lcRow).VALUE = CURAUDIT.OT20_HRS
											oWorksheetAUDIT.RANGE("G" + lcRow).VALUE = CURAUDIT.SICK_HRS
											oWorksheetAUDIT.RANGE("H" + lcRow).VALUE = CURAUDIT.VAC_HRS
											oWorksheetAUDIT.RANGE("I" + lcRow).VALUE = CURAUDIT.HDAY_HRS
											oWorksheetAUDIT.RANGE("J" + lcRow).VALUE = CURAUDIT.PERS_HRS
											oWorksheetAUDIT.RANGE("K" + lcRow).VALUE = CURAUDIT.COMP_HRS

											oWorksheetAUDIT.RANGE("L" + lcRow).VALUE = CURAUDIT.JURY_HRS
											oWorksheetAUDIT.RANGE("M" + lcRow).VALUE = CURAUDIT.BREAVE_HRS
											oWorksheetAUDIT.RANGE("N" + lcRow).VALUE = CURAUDIT.OTHER_HRS
											oWorksheetAUDIT.RANGE("O" + lcRow).VALUE = CURAUDIT.PP_VAC_HRS
											oWorksheetAUDIT.RANGE("P" + lcRow).VALUE = CURAUDIT.PP_HDAY_HRS
											oWorksheetAUDIT.RANGE("Q" + lcRow).VALUE = CURAUDIT.PP_PERS_HRS
											oWorksheetAUDIT.RANGE("R" + lcRow).VALUE = CURAUDIT.PP_COMP_HRS

											oWorksheetAUDIT.RANGE("S" + lcRow).VALUE = CURAUDIT.UNUSD_SICK
											oWorksheetAUDIT.RANGE("T" + lcRow).VALUE = CURAUDIT.RETRO_OTH
											oWorksheetAUDIT.RANGE("U" + lcRow).VALUE = CURAUDIT.OTHER_PAY
											oWorksheetAUDIT.RANGE("V" + lcRow).VALUE = CURAUDIT.CADRVR_PAY

											oWorksheetAUDIT.RANGE("A" + lcRow,"V" + lcRow).Interior.ColorIndex = IIF(((lnRow % 2) = 1),15,0)

										ENDSCAN

										lnEndRow = lnRow
										lcEndRow = ALLTRIM(STR(lnEndRow))

										lcTotalRow = ALLTRIM(STR(lnEndRow + 2))

										oWorksheetAUDIT.RANGE("B" + lcTotalRow).formula = "=sum(B" + lcStartRow + ":B" + lcEndRow + ")"
										oWorksheetAUDIT.RANGE("C" + lcTotalRow).formula = "=sum(C" + lcStartRow + ":C" + lcEndRow + ")"
										oWorksheetAUDIT.RANGE("D" + lcTotalRow).formula = "=sum(D" + lcStartRow + ":D" + lcEndRow + ")"
										oWorksheetAUDIT.RANGE("E" + lcTotalRow).formula = "=sum(E" + lcStartRow + ":E" + lcEndRow + ")"
										oWorksheetAUDIT.RANGE("F" + lcTotalRow).formula = "=sum(F" + lcStartRow + ":F" + lcEndRow + ")"
										oWorksheetAUDIT.RANGE("G" + lcTotalRow).formula = "=sum(G" + lcStartRow + ":G" + lcEndRow + ")"
										oWorksheetAUDIT.RANGE("H" + lcTotalRow).formula = "=sum(H" + lcStartRow + ":H" + lcEndRow + ")"
										oWorksheetAUDIT.RANGE("I" + lcTotalRow).formula = "=sum(I" + lcStartRow + ":I" + lcEndRow + ")"
										oWorksheetAUDIT.RANGE("J" + lcTotalRow).formula = "=sum(J" + lcStartRow + ":J" + lcEndRow + ")"
										oWorksheetAUDIT.RANGE("K" + lcTotalRow).formula = "=sum(K" + lcStartRow + ":K" + lcEndRow + ")"
										oWorksheetAUDIT.RANGE("L" + lcTotalRow).formula = "=sum(L" + lcStartRow + ":L" + lcEndRow + ")"
										oWorksheetAUDIT.RANGE("M" + lcTotalRow).formula = "=sum(M" + lcStartRow + ":M" + lcEndRow + ")"
										oWorksheetAUDIT.RANGE("N" + lcTotalRow).formula = "=sum(N" + lcStartRow + ":N" + lcEndRow + ")"
										oWorksheetAUDIT.RANGE("O" + lcTotalRow).formula = "=sum(O" + lcStartRow + ":O" + lcEndRow + ")"
										oWorksheetAUDIT.RANGE("P" + lcTotalRow).formula = "=sum(P" + lcStartRow + ":P" + lcEndRow + ")"
										oWorksheetAUDIT.RANGE("Q" + lcTotalRow).formula = "=sum(Q" + lcStartRow + ":Q" + lcEndRow + ")"
										oWorksheetAUDIT.RANGE("R" + lcTotalRow).formula = "=sum(R" + lcStartRow + ":R" + lcEndRow + ")"
										oWorksheetAUDIT.RANGE("S" + lcTotalRow).formula = "=sum(S" + lcStartRow + ":S" + lcEndRow + ")"
										oWorksheetAUDIT.RANGE("T" + lcTotalRow).formula = "=sum(T" + lcStartRow + ":T" + lcEndRow + ")"
										oWorksheetAUDIT.RANGE("U" + lcTotalRow).formula = "=sum(U" + lcStartRow + ":U" + lcEndRow + ")"
										oWorksheetAUDIT.RANGE("V" + lcTotalRow).formula = "=sum(V" + lcStartRow + ":V" + lcEndRow + ")"

										***********************************
										* 01/20/04 MB - do all the bolding/clearing of numeric columns here in one nested loop,
										* to eliminate a lot of extraneous code above.
										lcColsToProcess = "BCDEFGHIJKLMNOPQRSTUV"
										FOR i = 1 TO LEN(lcColsToProcess)
											lcBoldColumn = SUBSTR(lcColsToProcess,i,1)
											FOR j = lnStartRow TO lnEndRow
												lcBoldRow = ALLTRIM(STR(j))
												luCellValue = oWorksheetAUDIT.RANGE(lcBoldColumn + lcBoldRow).VALUE
												IF TYPE([luCellValue])= "N" THEN
													IF luCellValue > 0.0 THEN
														oWorksheetAUDIT.RANGE(lcBoldColumn + lcBoldRow).FONT.bold = .T.
													ELSE
														oWorksheetAUDIT.RANGE(lcBoldColumn + lcBoldRow).VALUE = ""
													ENDIF
												ENDIF
											ENDFOR  && j
										ENDFOR  && i
										***********************************

										* FORMAT LAST FEW COLUMNS FOR CURRENCY
										oWorksheetAUDIT.RANGE("T" + lcStartRow,"V1000").NumberFormat = "$#,##0.00"

										*oWorksheetAUDIT.RANGE("A" + lcStartRow,"T" + lcRow).SELECT
										oWorksheetAUDIT.PageSetup.PrintArea = "$A$"  + lcStartRow + ":$V$" + lcTotalRow

										* save again
										IF llSaveAgain THEN
											oWorkbookAUDIT.SAVE()
										ENDIF
										oWorkbookAUDIT.CLOSE()

										oExcel.QUIT()

									ENDIF && .lMakeSummaryByHomeDept
									*******************************************************************************************
									*******************************************************************************************

									IF .lProcessEPIE8799CSVFile	THEN

										* create a cursor of CUREMPLOYEES file#s that are *not* in ADP Payroll export file

										IF USED('CURMISSING') THEN
											USE IN CURMISSING
										ENDIF

										SELECT DIVISION, DEPT, EMPLOYEE, FILE_NUM  ;
											FROM CUREMPLOYEES ;
											INTO CURSOR CURMISSING ;
											WHERE INT(VAL(FILE_NUM)) NOT IN (SELECT FILE_NUM FROM CURACTUALPAYEXPORT) ;
											ORDER BY 1, 2, 3

										IF (NOT USED('CURMISSING')) OR EOF('CURMISSING') THEN
											lcTopBodyText = "DATA CHECK: All Employees in this spreadsheet were in the Kronos-->ADP Payroll Export File." + CRLF
										ELSE
											lcTopBodyText = "DATA WARNING: All Employees in this spreadsheet were NOT in the Kronos-->ADP Payroll Export File!" + CRLF + ;
												"The list of Employees follows: " + CRLF + CRLF + ;
												"NAME                                     FILE#" + CRLF + ;
												"---------------------------------------- -----"
											SELECT CURMISSING
											SCAN
												lcTopBodyText = lcTopBodyText + CRLF + LEFT(CURMISSING.EMPLOYEE,40) + " " + CURMISSING.FILE_NUM
											ENDSCAN
										ENDIF
										*******************************************************************************************
										*******************************************************************************************
										lcTopBodyText = lcTopBodyText + CRLF+ CRLF + ;
											"==================================================================================================================" + ;
											CRLF + CRLF

										* create a cursor of ADP Payroll export file file#s that are *not* in CUREMPLOYEES

										IF USED('CURMISSING') THEN
											USE IN CURMISSING
										ENDIF

										SELECT FILE_NUM  ;
											FROM CURACTUALPAYEXPORT ;
											INTO CURSOR CURMISSING ;
											WHERE FILE_NUM NOT IN (SELECT INT(VAL(FILE_NUM)) AS FILE_NUM FROM CUREMPLOYEES ) ;
											ORDER BY 1

										IF (NOT USED('CURMISSING')) OR EOF('CURMISSING') THEN
											lcTopBodyText = lcTopBodyText + "DATA CHECK: All File#s in the Kronos-->ADP Payroll Export File were in this spreadsheet." + CRLF
										ELSE
											lcTopBodyText = lcTopBodyText + "DATA WARNING: All File#s in the Kronos-->ADP Payroll Export File were NOT in this spreadsheet!" + CRLF + ;
												"The list of File#s follows: " + CRLF + CRLF + ;
												"FILE#" + CRLF + ;
												"-----"
											SELECT CURMISSING
											SCAN
												lcTopBodyText = lcTopBodyText + CRLF + TRANSFORM(CURMISSING.FILE_NUM)
											ENDSCAN
										ENDIF
										*******************************************************************************************
										*******************************************************************************************

										.cBodyText = lcTopBodyText + CRLF + ;
											"==================================================================================================================" + ;
											CRLF +lcMissingApprovals + CRLF + ;
											"==================================================================================================================" + ;
											CRLF + CRLF + lcExcluded9999s + CRLF + CRLF + ;
											"==================================================================================================================" + ;
											CRLF + "<report log follows>" + ;
											CRLF + CRLF + .cBodyText

										*!*								oExcel.VISIBLE = .T.

										*!*								* save again
										*!*								IF llSaveAgain THEN
										*!*									oWorkbook.SAVE()
										*!*								ENDIF
										*!*
										*!*								oExcel.Quit()
									ENDIF && .lProcessEPIE8799CSVFile

									IF FILE(lcXLFileName) THEN
										.cAttach = lcXLFileName
										.TrackProgress('Created Payroll File : ' + lcXLFileName, LOGIT+SENDIT)
										.TrackProgress('KRONOS PAYROLL REPORT process ended normally.', LOGIT+SENDIT+NOWAITIT)
									ELSE
										.TrackProgress('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!', LOGIT+SENDIT)
										.TrackProgress("ERROR: There was a problem Creating Payroll File : " + lcXLFileName, LOGIT+SENDIT)
										.TrackProgress('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!', LOGIT+SENDIT)
									ENDIF

								ENDIF  && EOF() CUREMPLOYEES

						ENDCASE

					ENDIF  &&  .ExecSQL(lcSQL, 'CURWTKEMPLOYEE', RETURN_DATA_MANDATORY)

					CLOSE DATABASES ALL
				ELSE
					* connection error
					.TrackProgress('Unable to connect to WTK System.', LOGIT+SENDIT)
				ENDIF   &&  .nSQLHandle > 0
			CATCH TO loError

				.TrackProgress('There was an error.',LOGIT+SENDIT)
				.TrackProgress('The Error # is: ' + TRANSFORM(loError.ERRORNO), LOGIT+SENDIT)
				.TrackProgress('The Error Message is: ' + TRANSFORM(loError.MESSAGE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred in Program: ' + TRANSFORM(loError.PROCEDURE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred at line #: ' + TRANSFORM(loError.LINENO), LOGIT+SENDIT)
				lnNumberOfErrors = lnNumberOfErrors + 1
				IF TYPE('oExcel') = 'O' AND NOT ISNULL(oExcel) THEN
					oExcel.QUIT()
				ENDIF
				CLOSE DATA
				CLOSE ALL

			ENDTRY

			CLOSE DATA
			WAIT CLEAR
			***************** INTERNAL email results ******************************
			.TrackProgress('About to send status email.',LOGIT)
			.TrackProgress('==================================================================================================================', SENDIT)
			.TrackProgress('The logfile is: ' + .cLogFile, SENDIT)
			.TrackProgress('KRONOS PAYROLL REPORT process started: ' + .cStartTime, LOGIT+SENDIT)
			.TrackProgress('KRONOS PAYROLL REPORT process finished: ' + TTOC(DATETIME()), LOGIT+SENDIT)
			
			IF .lSendInternalEmailIsOn THEN
				* try to trap error from not having dartmail dll's registered on user's pc...
				TRY
					DO FORM dartmail2 WITH .cSendTo,.cFrom,.cSubject,.cCC,.cAttach,.cBodyText,"A"
					.TrackProgress('Sent status email.',LOGIT)
				CATCH TO loError
					*=MESSAGEBOX('There was an error sending the status email. Please contact Mark Bennett.',0+64,"KRONOS PAYROLL REPORT")
					.TrackProgress('There was an error sending the status email.',LOGIT)
					.TrackProgress('The Error # is: ' + TRANSFORM(loError.ERRORNO), LOGIT)
					.TrackProgress('The Error Message is: ' + TRANSFORM(loError.MESSAGE), LOGIT)
					.TrackProgress('The Error occurred in Program: ' + TRANSFORM(loError.PROCEDURE), LOGIT)
					.TrackProgress('The Error occurred at line #: ' + TRANSFORM(loError.LINENO), LOGIT)
					lnNumberOfErrors = lnNumberOfErrors + 1
					CLOSE DATA
				ENDTRY

				IF .lMakeSummaryByHomeDept THEN
					* send email only to Lucille with Audit report
					lcCC = 'mbennett@fmiint.com'
					lcBodyText = 'See attached Kronos Payroll totals by HomeDepartment.'
					TRY
						DO FORM dartmail2 WITH .cSendToAUDIT,.cFrom,lcAuditSubject,lcCC,lcXLFileNameAUDIT,lcBodyText,"A"
						.TrackProgress('Sent status email.',LOGIT)
					CATCH TO loError
						*=MESSAGEBOX('There was an error sending the status email. Please contact Mark Bennett.',0+64,"KRONOS PAYROLL REPORT")
						.TrackProgress('There was an error sending the status email.',LOGIT)
						.TrackProgress('The Error # is: ' + TRANSFORM(loError.ERRORNO), LOGIT)
						.TrackProgress('The Error Message is: ' + TRANSFORM(loError.MESSAGE), LOGIT)
						.TrackProgress('The Error occurred in Program: ' + TRANSFORM(loError.PROCEDURE), LOGIT)
						.TrackProgress('The Error occurred at line #: ' + TRANSFORM(loError.LINENO), LOGIT)
						lnNumberOfErrors = lnNumberOfErrors + 1
						CLOSE DATA
					ENDTRY
				ENDIF && .lMakeSummaryByHomeDept
				
				
				IF .cReportType = "STANDARD" THEN
					* send me a reminder to make sure I exported payroll on PAYROLL2
					lcSendto = 'mbennett@fmiint.com'
					lcCC = ''
					lcBodyText = 'Go ahead!'
					lcSubject = 'DO PAYROLL EXPORT ON \\PAYROLL2 !!!'
					TRY
						DO FORM dartmail2 WITH lcSendto,.cFrom,lcSubject,'','',lcBodyText,"A"
						.TrackProgress('Sent Payroll2 Export reminder email.',LOGIT)
					CATCH TO loError
						.TrackProgress('There was an error sending the Payroll2 Export reminder email.',LOGIT)
						.TrackProgress('The Error # is: ' + TRANSFORM(loError.ERRORNO), LOGIT)
						.TrackProgress('The Error Message is: ' + TRANSFORM(loError.MESSAGE), LOGIT)
						.TrackProgress('The Error occurred in Program: ' + TRANSFORM(loError.PROCEDURE), LOGIT)
						.TrackProgress('The Error occurred at line #: ' + TRANSFORM(loError.LINENO), LOGIT)
					ENDTRY				
				ENDIF

			ELSE
				.TrackProgress('Did not send status email: THIS.lSendInternalEmailIsOn = FALSE.',LOGIT)
			ENDIF



		ENDWITH
		RETURN
	ENDFUNC && main


	FUNCTION ExecSQL
		LPARAMETERS tcSQL, tcCursorName, tlNoDataReturnedIsOkay
		LOCAL llRetval, lnResult
		WITH THIS
			* close target cursor if it's open
			IF USED(tcCursorName)
				USE IN (tcCursorName)
			ENDIF
			WAIT WINDOW "Fetching remote data; please wait..." NOWAIT
			lnResult = SQLEXEC(.nSQLHandle, tcSQL, tcCursorName)
			llRetval = ( lnResult > 0 )
			IF llRetval THEN
				* see if any data came back
				IF NOT tlNoDataReturnedIsOkay THEN
					IF (NOT USED(tcCursorName)) OR EOF(tcCursorName) THEN
						llRetval = .F.
						.TrackProgress('No data was returned by this query: ' +CRLF + tcSQL, LOGIT+SENDIT)
					ENDIF
				ENDIF
			ELSE
				.TrackProgress("Result Code [" + TRANSFORM(lnResult) + "] returned by this query: " + tcSQL, LOGIT+SENDIT)
			ENDIF
			WAIT CLEAR
			RETURN llRetval
		ENDWITH
	ENDFUNC


	PROCEDURE TrackProgress
		* do any combination of Wait Window, writing to logfile, and adding to body of email,
		* based on nFlags parameter.
		LPARAMETERS tcExpression, tnFlags
		WITH THIS
			IF BITAND(tnFlags,LOGIT) = LOGIT THEN
				IF .lLoggingIsOn THEN
					?
					? .cProcessName + "  " + TTOC(DATETIME()) + ": " + tcExpression
				ENDIF
			ENDIF
			IF .lWaitWindowIsOn THEN
				IF BITAND(tnFlags,NOWAITIT) = NOWAITIT THEN
					WAIT WINDOW tcExpression NOWAIT
				ENDIF
				IF BITAND(tnFlags,WAITIT) = WAITIT THEN
					WAIT WINDOW tcExpression TIMEOUT .nWaitWindowTimeout
				ENDIF
			ENDIF
			IF BITAND(tnFlags,SENDIT) = SENDIT THEN
				IF .lSendInternalEmailIsOn THEN
					.cBodyText = .cBodyText + tcExpression + CRLF + CRLF
				ENDIF
			ENDIF
		ENDWITH
	ENDPROC  &&  TrackProgress
	
	
	PROCEDURE SetDate
		LPARAMETERS tdDate
		THIS.dToday = tdDate
	ENDPROC

ENDDEFINE
