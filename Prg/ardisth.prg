valset("check for ardist in ardisth")

open database f:\watusi\platinum
use platinum!ardist alias ardist in 0
go bottom in ardist

open database f:\watusi\platinum
use platinum!ardisth alias ardisth in 0
go bottom in ardisth

select documentnumber from ardisth where distributiontype="R" into cursor xardisth
index on documentnumber tag zorder
set order to

select * from ardist where .f. into cursor xrpt readwrite

select ardist
scan for distributiontype="R"
	if seek(documentnumber,"xardisth","zorder")
		scatter memvar
		insert into xrpt from memvar
	endif
endscan

if reccount("xrpt")>0
	select xrpt
	copy to f:\auto\xardist
	email("Dyoung@fmiint.com","VAL "+transform(reccount())+" ardist in ardisth","xardist",,,,.t.,,,,,.t.)
endif

gunshot()
