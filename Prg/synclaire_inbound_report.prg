* PROCESS NAME:  SYNCLAIRE_INBOUND_RPT
*
* DESCRIPTION: reports miscellaneous data from inbound work orders for prior month
*
* Supports Synclaire and BCNY, based on passed parameter 'S' or 'B'
*
* build EXE as: F:\AUTO\SYNCLAIRE_INBOUND_RPT.EXE
*
* s.b. scheduled to run on day 1 of each month 
*
* Adapted to run as auto exe MB 12/07/2017 - old code written by PG is at bottom, commented out.

Lparameters tcClientCode

Local loSYNCLAIRE_INBOUND_RPT, llTestMode

llTestMode = .F.

If Not llTestMode Then
	utilsetup("SYNCLAIRE_INBOUND_RPT")
Endif

loSYNCLAIRE_INBOUND_RPT = Createobject('SYNCLAIRE_INBOUND_RPT')

loSYNCLAIRE_INBOUND_RPT.Main( llTestMode, tcClientCode )

If Not llTestMode Then
	schedupdate()
Endif

Close Databases All
Return

#Define LOGIT 1
#Define WAITIT 2
#Define NOWAITIT 4
#Define SENDIT 8
#Define CRLF Chr(13) + Chr(10)
#Define LF Chr(10)
#Define RETURN_DATA_MANDATORY .F.
#Define RETURN_DATA_NOT_MANDATORY .T.

Define Class SYNCLAIRE_INBOUND_RPT As Custom

	cProcessName = 'SYNCLAIRE_INBOUND_RPT'

	lTestMode = .F.  && .F. FOR NORMAL OPERATION, .T. TO SUPPRESS ALL BUT DEVELOPER EMAILS AND CHANGE INPUT TABLE, ETC.

	lAutoYield = .T.

	cStartTime = Ttoc(Datetime())

	* date properties
	dtNow = Datetime()

	dToday = Date()

	* wait window properties
	nWaitWindowTimeout = 2
	lWaitWindowIsOn = .T.

	* logfile properties
	lLoggingIsOn = .T.
	cLogFile = 'F:\UTIL\3PL\Logfiles\SYNCLAIRE_INBOUND_RPT_log.txt'

	* folder properties

	* process flow properties
	cClientCode = ''
	cClientName = ''
	nAccountID = 0

	* INTERNAL email properties
	lSendInternalEmailIsOn = .T.
	cFrom = "TGF EDI Ops <fmicorporate@fmiint.com>"
	cSendTo = 'mbennett@fmiint.com'
	cCC = ''
	cSubject = ''
	cAttach = ''
	cBodyText = ''

	cCOMPUTERNAME = ''
	cUSERNAME = ''

	Function Init
		If Not DoDefault()
			Return .F.
		Endif
		With This
			CLEAR
			IF _VFP.STARTMODE > 0 THEN
				SET RESOURCE OFF
			ENDIF
			Close Data
			Set Century On
			Set Date AMERICAN
			Set Hours To 24
			Set Ansi On
			Set Talk Off
			Set Deleted On
			Set Console Off
			Set Exclusive Off
			Set Safety Off
			Set Exact Off
			Set Status Bar On
			Set Sysmenu Off
			Set ENGINEBEHAVIOR 70
			_vfp.AutoYield = .lAutoYield
			.cCOMPUTERNAME = Upper(Alltrim(Getenv("COMPUTERNAME")))
			.cUSERNAME = Getenv("USERNAME")
			If .lTestMode Then
				.cSendTo = 'mbennett@fmiint.com'
				.cLogFile = 'F:\UTIL\3PL\Logfiles\SYNCLAIRE_INBOUND_RPT_log_TESTMODE.txt'
			Endif
			.lLoggingIsOn = .lLoggingIsOn And Not Empty(.cLogFile)
			If .lLoggingIsOn Then
				Set Alternate To (.cLogFile) Additive
				Set Alternate On
			Endif
			.cSubject = .cProcessName + ' process for: ' + Dtoc(Date())
		Endwith
	Endfunc


	Function Destroy
		With This
			If .lLoggingIsOn  Then
				Set Alternate Off
				Set Alternate To
			Endif
		Endwith
		DoDefault()
	Endfunc


	Function Main
		Lparameters tlTestMode, tcClientCode
		With This
			Local i, lnNumberOfErrors, lcOutputFile, ldToday, ldStartDate, lcStartDate, lcOutputFolder

			Try
				lnNumberOfErrors = 0

				.TrackProgress(.cProcessName + " process started....", LOGIT+SENDIT+NOWAITIT)
				.TrackProgress('COMPUTERNAME = ' + .cCOMPUTERNAME, LOGIT+SENDIT)
				.TrackProgress('USERNAME = ' + .cUSERNAME, LOGIT+SENDIT)
				.TrackProgress('PROJECT = SYNCLAIRE_INBOUND_RPT', LOGIT+SENDIT)

				.lTestMode = tlTestMode

				If .lTestMode Then
					.TrackProgress('====> TEST MODE', LOGIT+SENDIT)
				Endif

				.cClientCode = Upper(Alltrim(tcClientCode))

				Do Case
					Case .cClientCode == 'B'
						.nAccountID = 6221
						.cClientName = 'BCNY'
						If .lTestMode Then
							lcOutputFolder = 'F:\UTIL\SYNCLAIRE\TESTREPORTS\'
						Else
							lcOutputFolder = 'F:\FTPUSERS\BCNY\INBOUNDREPORT\'
						Endif
					Case .cClientCode == 'S'
						.nAccountID = 6521
						.cClientName = 'SYNCLAIRE'
						If .lTestMode Then
							lcOutputFolder = 'F:\UTIL\SYNCLAIRE\TESTREPORTS\'
						Else
							lcOutputFolder = 'F:\FTPUSERS\SYNCLAIRE\INBOUNDREPORT\'
						Endif
				Endcase

				.TrackProgress('.cClientName = ' + .cClientName, LOGIT+SENDIT)

				ldToday = Date()

				ldStartDate = ldToday - Day(ldToday) + 1

				ldStartDate = Gomonth(ldStartDate, -1)

				.TrackProgress('ldStartDate = ' + Dtoc(ldStartDate), LOGIT+SENDIT)

				.cSubject = .cProcessName + ' process for ' + .cClientName + ' for : ' + Dtoc(ldToday)

				lcOutputFile = lcOutputFolder + .cClientName + "_INBOUND_REPORT_" + Dtos(ldStartDate) + ".XLS"

				.TrackProgress('lcOutputFile = ' + lcOutputFile, LOGIT+SENDIT)

				**************************************************************************************************
				*Do setenvi
				*Do _setvars

				* we're not updating - changed to xsqlexec 12/5/2017 MB
				*!*	lcStr = "accountid = "+lcAcct+" and wo_date >= {11/01/2017}"
				*!*	useca("inwolog","wh",,,lcStr)
				xsqlexec("select * from inwolog where accountid = " + Transform(.nAccountID) + " and wo_date >= {" + Dtoc(ldStartDate) + "}","inwolog",,"wh")

				*!*	lcStr = "accountid = "+lcAcct
				* we're not updating - changed to xsqlezec 12/5/2017 MB
				*!*	useca("indet","wh",,,lcStr)
				* this brings back a ridiculous # of records - added a date filter MB 12/7/2017
				*xsqlexec("select * from indet where accountid = " + Transform(.nAccountID),"indet",,"wh")
				xsqlexec("select * from indet where date_rcvd >= {" + Dtoc(ldStartDate) + "} and accountid = " + Transform(.nAccountID),"indet",,"wh")
				
				*xsqlexec("select * from zzindet where accountid = "+lcAcct+" and date_rcvd >= {11/01/2017}","xindet",,"wh")
				xsqlexec("select * from zzindet where accountid = " + Transform(.nAccountID) + " and date_rcvd >= {" + Dtoc(ldStartDate) + "}","xindet",,"wh")

				Create Cursor indat (;
					wo_num   Int,;
					CONTAINER Char(10),;
					wo_date Date,;
					confirmdt Date,;
					PO      Char(15),;
					POnum   Char(15),;
					UPC     Char(12),;
					NAME    Char(40),;
					ctnqty  N(10),;
					PACK    Char(10),;
					totqty  N(10),;
					totwt   N(10.2),;
					totcube N(10.2) )

				Select wo_num, wo_date, Container, confirmdt From inwolog Where Len(Alltrim(Container))= 11 And wo_date >= ldStartDate And !Isnull(confirmdt) Into Cursor wolist Readwrite
				Select wolist
				Delete For Empty(Container)

				Select wolist
				Scan

					Select inwolog.wo_num ,wo_date,Container,confirmdt,indet.PO, indet.Style As UPC, indet.Pack, indet.totqty, Space(20) As Name From inwolog ;
						LEFT Join indet On indet.inwologid = inwolog.inwologid;
						WHERE inwolog.wo_num = wolist.wo_num And units And confirmdt >= ldStartDate Order By wo_date Into Cursor temp1
					Select temp1

					If Reccount("temp1") = 0
						Select inwolog.wo_num ,wo_date,Container,confirmdt,xindet.PO, xindet.Style As UPC, xindet.Pack, xindet.totqty, Space(20) As Name From inwolog ;
							LEFT Join xindet On xindet.inwologid = inwolog.inwologid;
							WHERE inwolog.wo_num = wolist.wo_num And units And confirmdt >= ldStartDate Order By wo_date Into Cursor temp1
					Endif

					If Reccount("temp1") = 0
						Set Step On
					Endif
					Scan
						Select temp1
						Scatter Memvar
						Select indat
						Append Blank
						Gather Memvar
						*   Insert Into indat From memvar
					Endscan

					Select inwolog.wo_num ,wo_date,Container,confirmdt,indet.PO, indet.cayset As POnum,indet.totcube,indet.totwt,indet.Style As UPC, indet.Pack,;
						indet.totqty, Space(20) As Name From inwolog Left Outer Join indet On indet.inwologid = inwolog.inwologid;
						WHERE inwolog.wo_num = wolist.wo_num And !units And wo_date >= ldStartDate Order By wo_date Into Cursor temp2

					Select temp2

					If Reccount("temp2") = 0
						Select inwolog.wo_num ,wo_date,Container,confirmdt,xindet.PO, xindet.cayset As POnum,xindet.totcube,xindet.totwt,xindet.Style As UPC, xindet.Pack,;
							xindet.totqty, Space(20) As Name From inwolog Left Outer Join xindet On xindet.inwologid = inwolog.inwologid;
							WHERE inwolog.wo_num = wolist.wo_num And !units And wo_date >= ldStartDate Order By wo_date Into Cursor temp2
					Endif

					Select temp2
					Scan
						Select indat
						Locate For (wo_num = temp2.wo_num) And (PO = temp2.PO)
						If Found() Then
							Replace indat.ctnqty  With temp2.totqty, ;
								indat.Pack    With temp2.Pack, ;
								indat.totcube With temp2.totcube, ;
								indat.totwt   With temp2.totwt, ;
								indat.POnum   With temp2.POnum ;
								IN indat
						Endif
					Endscan
				Endscan


				Select indat
				Scan
					*!*	IF whichaccount = 6521
					*!*		upcmastsql(6521,indat.UPC)
					*!*	ELSE
					*!*		upcmastsql(6221,indat.UPC)
					*!*	ENDIF
					upcmastsql(.nAccountID,indat.UPC)
					Replace indat.Name With upcmast.Descrip In indat
				Endscan

				Select wo_num,wo_date,Container,confirmdt,POnum,UPC,Name,ctnqty,Pack,totqty,totwt,totcube From indat Order By confirmdt Desc Into Cursor temp

				Select temp
				Copy To (lcOutputFile) Xl5

				*!*	IF whichaccount = 6521
				*!*		EXPORT TO F:\ftpusers\synclaire\inboundReport\synclaire_inbound_rpt.XLS TYPE XLS
				*!*	ELSE
				*!*		EXPORT TO F:\ftpusers\BCNY\inboundReport\bcny_inbound_rpt.XLS TYPE XLS
				*!*	ENDIF

				If File(lcOutputFile) Then
					.TrackProgress('=====>  Output file created: ' + lcOutputFile, LOGIT+SENDIT+NOWAITIT)
				Else
					.TrackProgress('=====>  ERROR: output file not found: ' + lcOutputFile, LOGIT+SENDIT+NOWAITIT)
					Throw
				Endif

				**************************************************************************************************

				.TrackProgress(.cProcessName + ' process ended normally', LOGIT+SENDIT+NOWAITIT)

			Catch To loError

				.TrackProgress('There was an error.',LOGIT+SENDIT)
				.TrackProgress('The Error # is: ' + Transform(loError.ErrorNo), LOGIT+SENDIT)
				.TrackProgress('The Error Message is: ' + Transform(loError.Message), LOGIT+SENDIT)
				.TrackProgress('The Error occurred in Program: ' + Transform(loError.Procedure), LOGIT+SENDIT)
				.TrackProgress('The Error occurred at line #: ' + Transform(loError.Lineno), LOGIT+SENDIT)
				lnNumberOfErrors = lnNumberOfErrors + 1
				If Type('oExcel') = 'O' And Not Isnull(oExcel) Then
					oExcel.Quit()
				Endif
				Close Data

			Endtry

			Close Data
			Wait Clear
			***************** INTERNAL email results ******************************
			.TrackProgress('About to send status email.',LOGIT)
			.TrackProgress('--', SENDIT)
			.TrackProgress('The logfile is: ' + .cLogFile, SENDIT)
			.TrackProgress(.cProcessName + ' process started: ' + .cStartTime, LOGIT+SENDIT)
			.TrackProgress(.cProcessName + ' process finished: ' + Ttoc(Datetime()), LOGIT+SENDIT)

			If .lSendInternalEmailIsOn Then
				* try to trap error from not having dartmail dll's registered on user's pc...
				Try
					Do Form dartmail2 With .cSendTo,.cFrom,.cSubject,.cCC,.cAttach,.cBodyText,"A"
					.TrackProgress('Sent status email.',LOGIT)
				Catch To loError
					*=MESSAGEBOX('There was an error sending the status email. Please contact Mark Bennett.',0+64,"KRONOS Temp Time")
					.TrackProgress('There was an error sending the status email.',LOGIT)
					.TrackProgress('The Error # is: ' + Transform(loError.ErrorNo), LOGIT)
					.TrackProgress('The Error Message is: ' + Transform(loError.Message), LOGIT)
					.TrackProgress('The Error occurred in Program: ' + Transform(loError.Procedure), LOGIT)
					.TrackProgress('The Error occurred at line #: ' + Transform(loError.Lineno), LOGIT)
					lnNumberOfErrors = lnNumberOfErrors + 1
				Endtry

			Else
				.TrackProgress('Did not send status email: THIS.lSendInternalEmailIsOn = FALSE.',LOGIT)
			Endif

		Endwith
		Return
	Endfunc && main


	Procedure TrackProgress
		* do any combination of Wait Window, writing to logfile, and adding to body of email,
		* based on nFlags parameter.
		Lparameters tcExpression, tnFlags
		With This
			If Bitand(tnFlags,LOGIT) = LOGIT Then
				If .lLoggingIsOn Then
					?
					? .cProcessName + "  " + Ttoc(Datetime()) + ": " + tcExpression
				Endif
			Endif
			If .lWaitWindowIsOn Then
				If Bitand(tnFlags,NOWAITIT) = NOWAITIT Then
					Wait Window tcExpression Nowait
				Endif
				If Bitand(tnFlags,WAITIT) = WAITIT Then
					Wait Window tcExpression Timeout .nWaitWindowTimeout
				Endif
			Endif
			If Bitand(tnFlags,SENDIT) = SENDIT Then
				If .lSendInternalEmailIsOn Then
					.cBodyText = .cBodyText + tcExpression + CRLF + CRLF
				Endif
			Endif
		Endwith
	Endproc  &&  TrackProgress

Enddefine


*!*	* OLD CODE BELOW ************************************************
*!*	PARAMETERS whichaccount

*!*	*whichaccount = 6521
*!*	lcAcct = TRANSFORM(whichaccount)
*!*	SET SAFETY OFF
*!*	SET CENTURY ON
*!*	DO setenvi
*!*	DO _setvars
*!*	SET DATE AMERICAN

*!*	CLOSE DATA ALL
*!*	*useca("inwolog","wh",,,"accountid ="+Transform(whichaccount))
*!*	*useca("indet","wh",,,"accountid = "+Transform(whichaccount))

*!*	lcStr = "accountid = "+lcAcct+" and wo_date >= {11/01/2017}"
*!*	useca("inwolog","wh",,,lcStr)

*!*	lcStr = "accountid = "+lcAcct
*!*	useca("indet","wh",,,lcStr)

*!*	xsqlexec("select * from zzindet where accountid = "+lcAcct+" and date_rcvd >= {11/01/2017}","xindet",,"wh")



*!*	CREATE CURSOR indat (;
*!*		wo_num   INT,;
*!*		CONTAINER CHAR(10),;
*!*		wo_date DATE,;
*!*		confirmdt DATE,;
*!*		PO      CHAR(15),;
*!*		POnum   CHAR(15),;
*!*		UPC     CHAR(12),;
*!*		NAME    CHAR(40),;
*!*		ctnqty  N(10),;
*!*		PACK    CHAR(10),;
*!*		totqty  N(10),;
*!*		totwt   N(10.2),;
*!*		totcube N(10.2) )

*!*	SELECT wo_num,wo_date,CONTAINER,confirmdt FROM inwolog WHERE LEN(ALLTRIM(CONTAINER))= 11 AND wo_date >= {^2017-11-01} AND !ISNULL(confirmdt) INTO CURSOR wolist READWRITE
*!*	SELECT wolist
*!*	DELETE FOR EMPTY(CONTAINER)

*!*	SELECT indat
*!*	ZAP

*!*	SELECT wolist
*!*	SCAN

*!*		SELECT inwolog.wo_num ,wo_date,CONTAINER,confirmdt,indet.PO, indet.STYLE AS UPC, indet.PACK, indet.totqty, SPACE(20) AS NAME FROM inwolog ;
*!*			LEFT JOIN indet ON indet.inwologid = inwolog.inwologid;
*!*			WHERE inwolog.accountid = &lcAcct AND inwolog.wo_num = wolist.wo_num AND units AND confirmdt >= {^2017-11-01} ORDER BY wo_date INTO CURSOR temp1
*!*		SELECT temp1

*!*		IF RECCOUNT("temp1") =0
*!*			SELECT inwolog.wo_num ,wo_date,CONTAINER,confirmdt,xindet.PO, xindet.STYLE AS UPC, xindet.PACK, xindet.totqty, SPACE(20) AS NAME FROM inwolog ;
*!*				LEFT JOIN xindet ON xindet.inwologid = inwolog.inwologid;
*!*				WHERE inwolog.accountid = &lcAcct AND inwolog.wo_num = wolist.wo_num AND units AND confirmdt >= {^2017-11-01} ORDER BY wo_date INTO CURSOR temp1
*!*		ENDIF

*!*		IF RECCOUNT("temp1") =0
*!*			SET STEP ON
*!*		ENDIF
*!*		SCAN
*!*			SELECT temp1
*!*			SCATTER MEMVAR
*!*			SELECT indat
*!*			APPEND BLANK
*!*			GATHER MEMVAR
*!*			*   Insert Into indat From memvar
*!*		ENDSCAN

*!*		SELECT inwolog.wo_num ,wo_date,CONTAINER,confirmdt,indet.PO, indet.cayset AS POnum,indet.totcube,indet.totwt,indet.STYLE AS UPC, indet.PACK,;
*!*			indet.totqty, SPACE(20) AS NAME FROM inwolog LEFT OUTER JOIN indet ON indet.inwologid = inwolog.inwologid;
*!*			WHERE inwolog.accountid = &lcAcct AND inwolog.wo_num = wolist.wo_num AND !units AND wo_date >= {^2017-11-01} ORDER BY wo_date INTO CURSOR temp2
*!*		SELECT temp2

*!*		IF RECCOUNT("temp2") =0
*!*			SELECT inwolog.wo_num ,wo_date,CONTAINER,confirmdt,xindet.PO, xindet.cayset AS POnum,xindet.totcube,xindet.totwt,xindet.STYLE AS UPC, xindet.PACK,;
*!*				xindet.totqty, SPACE(20) AS NAME FROM inwolog LEFT OUTER JOIN xindet ON xindet.inwologid = inwolog.inwologid;
*!*				WHERE inwolog.accountid = &lcAcct AND inwolog.wo_num = wolist.wo_num AND !units AND wo_date >= {^2017-11-01} ORDER BY wo_date INTO CURSOR temp2
*!*		ENDIF


*!*		SCAN
*!*			SELECT indat
*!*			LOCATE FOR wo_num = temp2.wo_num AND PO = temp2.PO
*!*			IF FOUND()
*!*				REPLACE ctnqty  WITH temp2.totqty
*!*				REPLACE PACK    WITH temp2.PACK
*!*				REPLACE totcube WITH temp2.totcube
*!*				REPLACE totwt   WITH temp2.totwt
*!*				REPLACE POnum   WITH temp2.POnum
*!*			ENDIF
*!*		ENDSCAN
*!*	ENDSCAN


*!*	SELECT indat
*!*	SCAN
*!*		IF whichaccount = 6521
*!*			upcmastsql(6521,indat.UPC)
*!*		ELSE
*!*			upcmastsql(6221,indat.UPC)
*!*		ENDIF
*!*		REPLACE indat.NAME WITH upcmast.DESCRIP IN indat
*!*	ENDSCAN

*!*	SELECT wo_num,wo_date,CONTAINER,confirmdt,POnum,UPC,NAME,ctnqty,PACK,totqty,totwt,totcube FROM indat ORDER BY confirmdt DESC INTO CURSOR temp

*!*	SELECT temp

*!*	IF whichaccount = 6521
*!*		EXPORT TO F:\ftpusers\synclaire\inboundReport\synclaire_inbound_rpt.XLS TYPE XLS
*!*	ELSE
*!*		EXPORT TO F:\ftpusers\BCNY\inboundReport\bcny_inbound_rpt.XLS TYPE XLS
*!*	ENDIF
*!*	RETURN
************************************************************************************************************************************
