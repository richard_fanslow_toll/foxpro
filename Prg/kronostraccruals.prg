* Create spreadsheet of ACCRUALS BALANCES for each time reviewer, daily.

* build as F:\UTIL\KRONOS\KRONOSTRACCRUALS.EXE

*** BY TIMEREVIEWER, and email to each just his people ****

LPARAMETERS tcMode

*!*	runack("KRONOSTRACCRUALS")

LOCAL loKRONOSTRACCRUALS, llShift2, lcTRNum, llSpecial, lcMode

IF EMPTY(tcMode) THEN
	WAIT WINDOW '***** INVALID PARAMETER *****' TIMEOUT 30
	RETURN .F.
ELSE
	lcMode = UPPER(ALLTRIM(tcMode))	
ENDIF

IF NOT INLIST(lcMode,'STANDARD','DAILY','WEEKLY','SPECIAL') THEN
	WAIT WINDOW '***** INVALID PARAMETER *****' TIMEOUT 30
	RETURN .F.
ENDIF

llSpecial = (lcMode = 'SPECIAL') 

IF NOT llSpecial THEN
	utilsetup("KRONOSTRACCRUALS")
ENDIF

loKRONOSTRACCRUALS = CREATEOBJECT('KRONOSTRACCRUALS')
USE F:\UTIL\KRONOS\DATA\TIMERVWR.DBF AGAIN IN 0 ALIAS RVWRLIST

DO CASE
	CASE lcMode = 'SPECIAL'
		loKRONOSTRACCRUALS.SetSpecial()
		SELECT RVWRLIST
		SCAN FOR SPECIAL AND LACTIVE AND LHOURLY
			loKRONOSTRACCRUALS.MAIN( RVWRLIST.TIMERVWR, RVWRLIST.NAME, RVWRLIST.EMAIL, RVWRLIST.CC, lcMode )
		ENDSCAN
	CASE lcMode = 'DAILY'
		SELECT RVWRLIST
		SCAN FOR LACTIVE AND LHOURLY AND LDAILYACC
			loKRONOSTRACCRUALS.MAIN( RVWRLIST.TIMERVWR, RVWRLIST.NAME, RVWRLIST.EMAIL, RVWRLIST.CC, lcMode )
		ENDSCAN
	CASE lcMode = 'WEEKLY'
		SELECT RVWRLIST
		SCAN FOR LACTIVE AND LHOURLY AND LWEEKLYACC
			loKRONOSTRACCRUALS.MAIN( RVWRLIST.TIMERVWR, RVWRLIST.NAME, RVWRLIST.EMAIL, RVWRLIST.CC, lcMode )
		ENDSCAN
	OTHERWISE && CASE lcMode = 'STANDARD'
		SELECT RVWRLIST
		SCAN FOR LACTIVE AND LHOURLY
			loKRONOSTRACCRUALS.MAIN( RVWRLIST.TIMERVWR, RVWRLIST.NAME, RVWRLIST.EMAIL, RVWRLIST.CC, lcMode )
		ENDSCAN
ENDCASE

*!*	IF llSpecial THEN

*!*		loKRONOSTRACCRUALS = CREATEOBJECT('KRONOSTRACCRUALS')
*!*		loKRONOSTRACCRUALS.SetSpecial()
*!*		USE F:\UTIL\KRONOS\DATA\TIMERVWR.DBF AGAIN IN 0 ALIAS RVWRLIST
*!*		SELECT RVWRLIST
*!*		SCAN FOR SPECIAL AND LACTIVE AND LHOURLY
*!*			loKRONOSTRACCRUALS.MAIN( RVWRLIST.TIMERVWR, RVWRLIST.NAME, RVWRLIST.EMAIL, RVWRLIST.CC )
*!*		ENDSCAN

*!*	ELSE

*!*		loKRONOSTRACCRUALS = CREATEOBJECT('KRONOSTRACCRUALS')
*!*		* normal case
*!*		USE F:\UTIL\KRONOS\DATA\TIMERVWR.DBF AGAIN IN 0 ALIAS RVWRLIST
*!*		SELECT RVWRLIST
*!*		SCAN FOR LACTIVE AND LHOURLY
*!*			loKRONOSTRACCRUALS.MAIN( RVWRLIST.TIMERVWR, RVWRLIST.NAME, RVWRLIST.EMAIL, RVWRLIST.CC )
*!*		ENDSCAN

*!*	ENDIF && llSpecial

IF NOT llSpecial THEN
	schedupdate()
ENDIF

CLOSE DATABASES ALL
RETURN

#DEFINE LOGIT 1
#DEFINE WAITIT 2
#DEFINE NOWAITIT 4
#DEFINE SENDIT 8
#DEFINE CRLF CHR(13) + CHR(10)
#DEFINE RETURN_DATA_MANDATORY .F.
#DEFINE RETURN_DATA_NOT_MANDATORY .T.

DEFINE CLASS KRONOSTRACCRUALS AS CUSTOM

	cProcessName = 'KRONOSTRACCRUALS'

	lSpecial = .T.

	lTestMode = .F.  && .F. FOR NORMAL OPERATION, .T. TO SUPPRESS ALL BUT DEVELOPER EMAILS AND CHANGE INPUT TABLE, ETC.

	lAutoYield = .T.

	cStartTime = TTOC(DATETIME())
	
	* processing properties
	cMode = ''

	* Excel properties
	oExcel = NULL

	* connection properties
	nSQLHandle = 0

	* file properties
	nFileHandle = 0

	* table properties
	cFoxProTimeReviewerTable = "F:\UTIL\KRONOS\DATA\TIMERVWR"
	cDailyAccrualsTable = "F:\UTIL\KRONOS\ACCRUALSDATA\ACCRUALS"

	* date properties
	dtNow = DATETIME()
	dToday = DATE()
	cToday = DTOC(DATE())

	* wait window properties
	nWaitWindowTimeout = 2
	lWaitWindowIsOn = .T.

	* logfile properties
	lLoggingIsOn = .F.
	cLogFile = ''

	* INTERNAL email properties
	lSendInternalEmailIsOn = .T.
	cFrom = 'FMI Corporate <fmicorporate@fmiint.com>'
	cSendTo = 'mbennett@fmiint.com'
	cCC = 'mbennett@fmiint.com'
	cSubject = 'KRONOS ACCRUALS REPORT for: ' + DTOC(DATE())
	cAttach = ''
	cBodyText = ''

	cCOMPUTERNAME = ''
	cUSERNAME = ''

	FUNCTION INIT
		IF NOT DODEFAULT()
			RETURN .F.
		ENDIF
		WITH THIS
			LOCAL loError
			CLEAR
			*SET RESOURCE OFF
			CLOSE DATA
			SET CENTURY ON
			SET DATE YMD
			SET HOURS TO 24
			SET ANSI ON
			SET TALK OFF
			SET DELETED ON
			SET CONSOLE OFF
			SET EXCLUSIVE OFF
			SET SAFETY OFF
			SET EXACT OFF
			SET STATUS BAR ON
			SET SYSMENU OFF
			SET ENGINEBEHAVIOR 70
			_VFP.AUTOYIELD = .lAutoYield
			.cCOMPUTERNAME = GETENV("COMPUTERNAME")
			.cUSERNAME = GETENV("USERNAME")
			IF .lTestMode THEN
				.cSendTo = 'Mark Bennett <mbennett@fmiint.com>'
				.cCC = ''
				.cLogFile = 'F:\UTIL\KRONOS\Logfiles\KRONOSTRACCRUALS_log_' + .cCOMPUTERNAME + '_TESTMODE.txt'
			ELSE
				.cLogFile = 'F:\UTIL\KRONOS\Logfiles\KRONOSTRACCRUALS_log_' + .cCOMPUTERNAME + '.txt'
			ENDIF
			.lLoggingIsOn = .lLoggingIsOn AND NOT EMPTY(.cLogFile)
			IF .lLoggingIsOn THEN
				SET ALTERNATE TO (.cLogFile) ADDITIVE
				SET ALTERNATE ON
			ENDIF
			* re-init date properties after all SETTINGS
			.dtNow = DATETIME()
			.dToday = DATE()
			.cToday = DTOC(DATE())
			.cAttach = ''

			WAIT WINDOW NOWAIT "Opening Excel..."
			TRY
				.oExcel = CREATEOBJECT("excel.application")
				.oExcel.VISIBLE = .F.
			CATCH
				.oExcel = NULL
			ENDTRY
			
		ENDWITH
	ENDFUNC


	FUNCTION DESTROY
		WITH THIS
			IF .lLoggingIsOn  THEN
				SET ALTERNATE OFF
				SET ALTERNATE TO
			ENDIF
			IF TYPE('.oExcel') = 'O' AND NOT ISNULL(.oExcel) THEN
				.oExcel.QUIT()
			ENDIF
		ENDWITH
		DODEFAULT()
	ENDFUNC


	FUNCTION MAIN

		LPARAMETERS tcTRNumber, tcTRName, tcTReMail, tcCC, tcMode

		LOCAL lnNumberOfErrors, lcSQL, lcSQL2, loError, lcTopBodyText, llValid
		LOCAL lcXLFileName, ldStartDate, ldEndDate, lcStartDate, lcEndDate, ldDate
		LOCAL lcSQLStartDate, lcSQLEndDate, lcXLFileName, lcSpecialtext
		LOCAL oWorkbook, oWorksheet, lnRow, lcRow, lnStartRow, lcStartRow, lnEndRow, lnPP, lcPP, lnNN, lcNN, lcJJ
		LOCAL lcColsToProcess, lnColumn, lcBoldColumn, i, j, lcBoldRow, luCellValue, lcTargetDirectory, ldPayDate, lcFiletoSaveAs
		LOCAL lcFileDate, lcDivision, lcRateType, lcTitle, llSaveAgain, lcPayCode, lcOrderBy
		LOCAL lcTRName, lcTReMail, lcTRNumber, lcCC, lcTestCC, lcFoxProTimeReviewerTable, lcMissingApprovals

		WITH THIS

			TRY
				lnNumberOfErrors = 0
				
				.cMode = tcMode
				
				lcTopBodyText = ''
				lcTRNumber = tcTRNumber
				lcTRName = ALLTRIM(tcTRName)
				lcTReMail = ALLTRIM(tcTReMail)
				lcCC = ALLTRIM(tcCC)
				*lcCC = ''

				* repeat some setup
				SET CONSOLE OFF
				SET TALK OFF
				*.cCC = 'Mark Bennett <mbennett@fmiint.com>, Lucille Waldrip <lwaldrip@fmiint.com>'
				.cCC = 'mbennett@fmiint.com'
				.cStartTime = TTOC(DATETIME())
				.dtNow = DATETIME()
				.cAttach = ''

				IF .lTestMode THEN
					.cSendTo = 'mbennett@fmiint.com'
					IF NOT EMPTY(lcCC) THEN
						lcTestCC = lcCC + ", " + .cCC
					ELSE
						lcTestCC = .cCC
					ENDIF
					.cCC = ""
				ELSE
					.cSendTo = lcTReMail
					IF NOT EMPTY(lcCC) THEN
						.cCC = lcCC + ", " + .cCC
					ENDIF
				ENDIF

				.cBodyText = ""

				.TrackProgress('KRONOS ACCRUALS REPORT process started....', LOGIT+SENDIT+NOWAITIT)
				.TrackProgress('COMPUTERNAME = ' + .cCOMPUTERNAME, LOGIT+SENDIT)
				.TrackProgress('PROJECT = KRONOSTRACCRUALS', LOGIT+SENDIT)
				.TrackProgress('MODE = ' + .cMode, LOGIT+SENDIT)
				IF .lTestMode THEN
					.TrackProgress('                        TEST MODE                        ', LOGIT+SENDIT)
					.TrackProgress('Time Reviewer # = ' + lcTRNumber, LOGIT+SENDIT)
					.TrackProgress('Time Reviewer Name = ' + lcTRName, LOGIT+SENDIT)
					.TrackProgress('cSendTo would be: ' + lcTReMail, LOGIT+SENDIT)
					.TrackProgress('cCC would be: ' + lcTestCC, LOGIT+SENDIT)
				ENDIF

				* calculate pay period start/end dates.
				* We go back from today until we hit a Saturday (DOW = 7) , which is the end day of the previous pay period.
				* then we subtract 6 days from that to get the prior Sunday (DOW = 1), which is the start day of the previous pay period.
				ldDate = .dToday

				IF USED('CURACCRUALS') THEN
					USE IN CURACCRUALS
				ENDIF
				IF USED('ACCRUALSTABLE') THEN
					USE IN ACCRUALSTABLE
				ENDIF
				
				USE (.cDailyAccrualsTable) IN 0 ALIAS ACCRUALSTABLE

				lcFiletoSaveAs = 'F:\UTIL\KRONOS\ACCRUALSDATA\ACCRUALSBALANCES-' + STRTRAN(DTOC(.dToday),"/","") + '-' + lcTRName
				lcXLFileName = lcFiletoSaveAs + ".XLS"

				SELECT ASOFDATE, EMPLOYEE, FILE_NUM, VACATION, VACRLVR, PERSONAL, SICK, CADRVRSICK ;
					FROM ACCRUALSTABLE ;
					INTO CURSOR CURACCRUALS ;
					WHERE TRNUM = tcTRNumber ;
					ORDER BY EMPLOYEE ;
					READWRITE

				IF USED('CURACCRUALS') AND NOT EOF('CURACCRUALS') THEN

*!*						SELECT CURACCRUALS
*!*						COPY TO (lcXLFileName) XL5


					oWorkbook = .oExcel.workbooks.OPEN("F:\UTIL\KRONOS\TEMPLATES\TRACCRUALS_TEMPLATE.XLS")

					IF FILE(lcXLFileName) THEN
						DELETE FILE (lcXLFileName)
					ENDIF
					oWorkbook.SAVEAS(lcFiletoSaveAs)
					
					
					lnRow = 3
					lnStartRow = lnRow + 1
					lcStartRow = ALLTRIM(STR(lnStartRow))

					oWorksheet = oWorkbook.Worksheets[1]
					oWorksheet.RANGE("A" + lcStartRow,"H100").clearcontents()

					oWorksheet.RANGE("A" + lcStartRow,"H100").FONT.SIZE = 10
					oWorksheet.RANGE("A" + lcStartRow,"H100").FONT.NAME = "Arial Narrow"
					oWorksheet.RANGE("A" + lcStartRow,"H100").FONT.bold = .T.
					

					SET DATE AMERICAN

*!*									lcTitle = "Kronos Payroll Summary for Pay Date: " + DTOC(ldPayDate) + ", Week of: " + lcStartDate + " - " + lcEndDate
*!*									oWorksheet.RANGE("A1").VALUE = lcTitle
*!*									oWorksheet.RANGE("A2").VALUE = "EEs for: " + lcTRName

					* main scan/processing
					SELECT CURACCRUALS
					SCAN

						lnRow = lnRow + 1
						lcRow = LTRIM(STR(lnRow))
						oWorksheet.RANGE("A" + lcRow).VALUE = CURACCRUALS.ASOFDATE
						oWorksheet.RANGE("B" + lcRow).VALUE = CURACCRUALS.EMPLOYEE
						oWorksheet.RANGE("C" + lcRow).VALUE = "'" + CURACCRUALS.FILE_NUM
						oWorksheet.RANGE("D" + lcRow).VALUE = CURACCRUALS.VACATION
						oWorksheet.RANGE("E" + lcRow).VALUE = CURACCRUALS.VACRLVR
						oWorksheet.RANGE("F" + lcRow).VALUE = CURACCRUALS.PERSONAL
						oWorksheet.RANGE("G" + lcRow).VALUE = CURACCRUALS.SICK
						oWorksheet.RANGE("H" + lcRow).VALUE = CURACCRUALS.CADRVRSICK
			
					ENDSCAN
		
					oWorkbook.SAVE()
					oWorkbook.CLOSE()
					oWorkbook = NULL
					RELEASE oWorkbook
					
					IF USED('ACCRUALSTABLE') THEN
						USE IN ACCRUALSTABLE
					ENDIF

					IF FILE(lcXLFileName) THEN
						.cAttach = lcXLFileName
						.TrackProgress('Created Accruals File : ' + lcXLFileName, LOGIT+SENDIT)
						.TrackProgress('KRONOS ACCRUALS REPORT process ended normally.', LOGIT+SENDIT+NOWAITIT)
					ELSE
						.TrackProgress('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!', LOGIT+SENDIT)
						.TrackProgress("ERROR: There was a problem creating accruals File : " + lcXLFileName, LOGIT+SENDIT)
						.TrackProgress('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!', LOGIT+SENDIT)
					ENDIF
					
					.cBodyText = "See attached spreadsheet with your employees' current accruals balances. ALL BALANCES ARE IN HOURS!" + CRLF + ;
						'Regards, Mark' + CRLF + CRLF + '<<REPORT LOG FOLLOWS>>' + .cBodyText

				ELSE

					.cAttach = ''
					.TrackProgress("We didn't find any accruals data!", LOGIT+SENDIT)

				ENDIF && USED('CURACCRUALS') AND NOT EOF('CURACCRUALS')

				IF USED('ACCRUALSTABLE') THEN
					USE IN ACCRUALSTABLE
				ENDIF

				IF USED('CURACCRUALS') THEN
					USE IN CURACCRUALS
				ENDIF

			CATCH TO loError

				.TrackProgress('There was an error.',LOGIT+SENDIT)
				.TrackProgress('The Error # is: ' + TRANSFORM(loError.ERRORNO), LOGIT+SENDIT)
				.TrackProgress('The Error Message is: ' + TRANSFORM(loError.MESSAGE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred in Program: ' + TRANSFORM(loError.PROCEDURE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred at line #: ' + TRANSFORM(loError.LINENO), LOGIT+SENDIT)
				lnNumberOfErrors = lnNumberOfErrors + 1
				*!*					IF TYPE('.oExcel') = 'O' AND NOT ISNULL(.oExcel) THEN
				*!*						.oExcel.VISIBLE = .T.
				*!*					ENDIF
				*!*					IF .nSQLHandle > 0 THEN
				*!*						SQLDISCONNECT(.nSQLHandle)
				*!*					ENDIF
				*CLOSE DATA
				*CLOSE ALL

			ENDTRY

			*CLOSE DATA
			WAIT CLEAR
			***************** INTERNAL email results ******************************
			.TrackProgress('About to send status email.',LOGIT)
			.TrackProgress('==================================================================================================================', SENDIT)
			.TrackProgress('The logfile is: ' + .cLogFile, SENDIT)
			.TrackProgress('KRONOS ACCRUALS REPORT process started: ' + .cStartTime, LOGIT+SENDIT)
			.TrackProgress('KRONOS ACCRUALS REPORT process finished: ' + TTOC(DATETIME()), LOGIT+SENDIT)
			
*!*				IF (lnNumberOfErrors = 0) AND (NOT .lTestMode) AND (NOT (.cMode = 'SPECIAL')) THEN
*!*					schedupdate()
*!*				ENDIF

			IF .lSendInternalEmailIsOn THEN
				* try to trap error from not having dartmail dll's registered on user's pc...
				TRY
					DO FORM dartmail2 WITH .cSendTo,.cFrom,.cSubject,.cCC,.cAttach,.cBodyText,"A"
					.TrackProgress('Sent status email.',LOGIT)
				CATCH TO loError
					=MESSAGEBOX('There was an error sending the status email. Please contact Mark Bennett.',0+64,"KRONOS ACCRUALS REPORT")
					.TrackProgress('There was an error sending the status email.',LOGIT)
					.TrackProgress('The Error # is: ' + TRANSFORM(loError.ERRORNO), LOGIT)
					.TrackProgress('The Error Message is: ' + TRANSFORM(loError.MESSAGE), LOGIT)
					.TrackProgress('The Error occurred in Program: ' + TRANSFORM(loError.PROCEDURE), LOGIT)
					.TrackProgress('The Error occurred at line #: ' + TRANSFORM(loError.LINENO), LOGIT)
					lnNumberOfErrors = lnNumberOfErrors + 1
					CLOSE DATA
				ENDTRY
			ELSE
				.TrackProgress('Did not send status email: THIS.lSendInternalEmailIsOn = FALSE.',LOGIT)
			ENDIF

		ENDWITH
		RETURN
	ENDFUNC && main


	FUNCTION ExecSQL
		LPARAMETERS tcSQL, tcCursorName, tlNoDataReturnedIsOkay
		LOCAL llRetval, lnResult
		WITH THIS
			* close target cursor if it's open
			IF USED(tcCursorName)
				USE IN (tcCursorName)
			ENDIF
			WAIT WINDOW "Fetching remote data; please wait..." NOWAIT
			lnResult = SQLEXEC(.nSQLHandle, tcSQL, tcCursorName)
			llRetval = ( lnResult > 0 )
			IF llRetval THEN
				* see if any data came back
				IF NOT tlNoDataReturnedIsOkay THEN
					IF (NOT USED(tcCursorName)) OR EOF(tcCursorName) THEN
						llRetval = .F.
						.TrackProgress('No data was returned by this query: ' +CRLF + tcSQL, LOGIT+SENDIT)
					ENDIF
				ENDIF
			ELSE
				.TrackProgress("Result Code [" + TRANSFORM(lnResult) + "] returned by this query: " + tcSQL, LOGIT+SENDIT)
			ENDIF
			WAIT CLEAR
			RETURN llRetval
		ENDWITH
	ENDFUNC


	FUNCTION SetSpecial
		THIS.lSpecial = .T.
	ENDFUNC


	PROCEDURE TrackProgress
		* do any combination of Wait Window, writing to logfile, and adding to body of email,
		* based on nFlags parameter.
		LPARAMETERS tcExpression, tnFlags
		WITH THIS
			IF BITAND(tnFlags,LOGIT) = LOGIT THEN
				IF .lLoggingIsOn THEN
					?
					? .cProcessName + "  " + TTOC(DATETIME()) + ": " + tcExpression
				ENDIF
			ENDIF
			IF .lWaitWindowIsOn THEN
				IF BITAND(tnFlags,NOWAITIT) = NOWAITIT THEN
					WAIT WINDOW tcExpression NOWAIT
				ENDIF
				IF BITAND(tnFlags,WAITIT) = WAITIT THEN
					WAIT WINDOW tcExpression TIMEOUT .nWaitWindowTimeout
				ENDIF
			ENDIF
			IF BITAND(tnFlags,SENDIT) = SENDIT THEN
				IF .lSendInternalEmailIsOn THEN
					.cBodyText = .cBodyText + tcExpression + CRLF + CRLF
				ENDIF
			ENDIF
		ENDWITH
	ENDPROC  &&  TrackProgress

ENDDEFINE
