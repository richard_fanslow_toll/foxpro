PARAMETERS lmanualSelect,lFullScreen
PUBLIC NormalExit,xfile,lemail,dDaysBack,lMissSCACs

NormalExit = .F.
xfile = ""
lemail = .T.

TRY
	utilsetup("TRUCKING945TRIGGER")
	SET REPROCESS TO 500
	lTesting = .F.

*DHW	dDaysBack = DATE()-IIF(lTesting,15,10)
	dDaysBack = DATE()-15
*	dDaysBack = DATE()-30


	_SCREEN.CAPTION = "Trucking 945 Trigger Process"
	cErrMsg = ""

	DO m:\dev\prg\_setvars WITH .T.
	CLOSE DATABASES ALL
	PUBLIC i
	lMissSCACs = .F.

	DO m:\dev\prg\lookups
	IF FILE('f:\3pl\data\trigtemp.dbf')
		DELETE FILE F:\3pl\DATA\trigtemp.DBF
	ENDIF

	lFullScreen = IIF(lTesting OR lmanualSelect,.T.,.F.)
	lOverridebusy = lTesting

*	lFullScreen = .t.
*	lOverridebusy = .T.

	_SCREEN.WINDOWSTATE=IIF(lFullScreen OR lOverridebusy,2,1)

	IF lmanualSelect
*!* Moret Only processing
		nStart = 4 && DO NOT CHANGE, S/B 4
		nTotLoops = 6 && DO NOT CHANGE, S/B  6
		lOverridebusy = .T.
		dDaysBack = DATE()-5
	ENDIF

	IF !lTesting
		SELECT 0
		USE F:\edirouting\FTPSETUP SHARED
		LOCATE FOR FTPSETUP.TRANSFER = "TRIG-TRUCK945"
		IF FTPSETUP.folderchk = .F. AND !lOverridebusy
			CLOSE DATABASES ALL
			CANCEL
			RETURN
		ENDIF

		IF FTPSETUP.chkbusy AND !lOverridebusy
			WAIT WINDOW 'TRIG Busy...try again later' TIMEOUT 1
			CLOSE DATABASES ALL
			CANCEL
			RETURN
		ENDIF
		REPLACE chkbusy WITH .T.,trig_time WITH DATETIME() FOR FTPSETUP.TRANSFER = "TRIG-TRUCK945"
		USE IN FTPSETUP
	ENDIF

	CREATE CURSOR missscacs (office c(1),accountid N(5),bol_no c(20),ship_ref c(20))

	IF USED('edi_trigger')
		USE IN edi_trigger
	ENDIF
	USE F:\3pl\DATA\edi_trigger IN 0 ALIAS edi_trigger SHARED
	USE F:\3pl\DATA\trigger945accts IN 0 ALIAS trigaccts SHARED
	USE F:\3pl\DATA\parcel_carriers IN 0 ALIAS pcarriers SHARED

	_SCREEN.CAPTION= "EDI All Trucking 945 Trigger Process"

	WAIT WINDOW  "At start of Trucking 945 trigger process" NOWAIT

	IF !lmanualSelect  && < < <Normal run-through
		nStart = 1			&& Default = 1
		nTotLoops = 15 	&& Default = 15
	ENDIF


	FOR i = nStart TO nTotLoops
		DO CASE
			CASE i = 1
				cWhse1 = "1"
				cMod = "SP MOD A"
				cWOff = "C"
				_SCREEN.CAPTION= "EDI All Trucking 945 Trigger Process, SP MOD A"
			CASE i = 2
				cWhse1 = "2"
				cMod = "SP MOD D"
				cWOff = "C"
				_SCREEN.CAPTION= "EDI All Trucking 945 Trigger Process, SP MOD D"
			CASE i = 3
				cWhse1 = "5"
				cMod = "SP MOD C"
				cWOff = "C"
				_SCREEN.CAPTION= "EDI All Trucking 945 Trigger Process, SP MOD C"
			CASE i = 4 && Moret SP Mod E
				cWhse1 = "7"
				cMod = "SP MOD E"
				cWOff = "C"
				_SCREEN.CAPTION= "EDI All Trucking 945 Trigger Process, SP MOD E"
			CASE i = 5 && Mira Loma
				cWhse1 = "L"
				cMod = "MIRA LOMA"
				cWOff = cWhse1
				_SCREEN.CAPTION= "EDI All Trucking 945 Trigger Process, ML"
			CASE i = 6 && Carson
				cWhse1 = "X"
				cMod = "CARSON"
				cWOff = cWhse1
				_SCREEN.CAPTION= "EDI All Trucking 945 Trigger Process, CR"
			CASE i = 7 && SP Mod B
				cWhse1 = "6"
				cMod = "SP MOD B"
				cWOff = "C"
				_SCREEN.CAPTION= "EDI All Trucking 945 Trigger Process, SP MOD B"
			CASE i = 8
				cWhse1 = "M"
				cMod = "FLORIDA"
				cWOff = cWhse1
				_SCREEN.CAPTION= "EDI All Trucking 945 Trigger Process, FL"
			CASE i = 9
				cWhse1 = "I"
				cMod = "NJ-I"
				cWOff = "N"
				_SCREEN.CAPTION= "EDI All Trucking 945 Trigger Process, NJ MOD I"
			CASE i = 10 && NJ Marc Jacobs
				cWhse1 = "J"
				cMod = "NJ-J"
				cWOff = "N"
				_SCREEN.CAPTION= "EDI All Trucking 945 Trigger Process, NJ MOD J"
			CASE i = 11 && Ariat - Louisville
				cWhse1 = "K"
				cMod = "KY-K"
				cWOff = cWhse1
				_SCREEN.CAPTION= "EDI All Trucking 945 Trigger Process, KY MOD K"
			CASE i = 12 && Steel Series		&& steelxyz
				cWhse1 = "S"
				cMod = "KY-S"
				cWOff = "K"
				_SCREEN.CAPTION= "EDI All Trucking 945 Trigger Process, KY MOD S"
			CASE i = 13 && Steel Series
				cWhse1 = "X"
				cMod = "CARSON-OTHER"
				cWOff = cWhse1
				_SCREEN.CAPTION= "EDI All Trucking 945 Trigger Process, CARSON-OTHER"
			CASE i = 14 && Carson II
				cWhse1 = "Y"
				cMod = "CARSON II"
				cWOff = cWhse1
				_SCREEN.CAPTION= "EDI All Trucking 945 Trigger Process, CARSON-II"
			CASE i = 15 && Toronto, Canada
				cWhse1 = "Z"
				cMod = "TORONTO"
				cWOff = cWhse1
				_SCREEN.CAPTION= "EDI All Trucking 945 Trigger Process, TORONTO"
		ENDCASE

		gOffice = cWhse1
		gMasterOffice = cWOff

		WAIT CLEAR
		WAIT WINDOW "Now processing Whse Group: "+cMod+CHR(13)+"In loop "+TRANSFORM(i)+" of "+TRANSFORM(nTotLoops) NOWAIT NOCLEAR

		csq1 = [select * from outship where office = ']+cWOff+[' and mod = ']+cWhse1+[' and del_date >= {]+DTOC(dDaysBack)+[}]

		xsqlexec(csq1,,,"wh")
		INDEX ON outshipid TAG outshipid

******************************************

		DO CASE
&&Changed "945F","945"  to "945","945" no longer sending .rdf TM 07/03/14
			CASE INLIST(i,9) && NJ accounts
				SELECT cWOff AS office,accountid,wo_num,WO_DATE AS DATE,;
					ship_ref,cnee_ref,bol_no AS bol,SCAC,ship_via,.F. AS PROCESSED,CONSIGNEE,outshipid,;
					IIF(outship.accountid = 6305,"945 ","945 ") AS EDI_TYPE,"TRUCK-TRIGGER" AS TRIG_FROM, ;
					"FMI-PROC" AS TRIG_BY,DATETIME() AS trig_time, ;
					.F. AS moret, ;
					IIF(LEFT(vendor_num,3)="10-" AND accountid = 4694,.T.,.F.) AS adny, ;
					.F. AS parcel ;
					FROM outship ;
					WHERE BETWEEN(outship.del_date,dDaysBack,DATE()) ;
					AND !EMPTYnul(del_date);
					AND !EMPTY(bol_no) ;
					AND !INLIST(accountid,6252,5687,6682) ;
					GROUP BY 1,2,5 ;
					ORDER BY bol_no,ship_ref ;
					INTO CURSOR outtrk1 READWRITE

			CASE INLIST(i,4,5,6)
				SELECT cWhse1 AS office,accountid,wo_num,WO_DATE AS DATE,;
					ship_ref,cnee_ref,bol_no AS bol,SCAC,ship_via,.F. AS PROCESSED,CONSIGNEE,outshipid,;
					"945" AS EDI_TYPE,"TRUCK-TRIGGERS" AS TRIG_FROM,;
					"FMI-PROC" AS TRIG_BY,DATETIME() AS trig_time, ;
					IIF((INLIST(accountid,&gMoretAcctList) OR INLIST(accountid,&gMoretAcctList2)),.T.,.F.) AS moret, ;
					.F. AS parcel ;
					FROM outship ;
					WHERE BETWEEN(outship.del_date,dDaysBack,DATE()) ;
					AND !EMPTYnul(del_date);
					AND !EMPTY(bol_no) ;
					AND ctnqty>0 ;
					AND !"OV"$ship_ref ;
					GROUP BY 1,2,5 ;
					ORDER BY bol_no,ship_ref ;
					INTO CURSOR outtrk1 READWRITE
				LOCATE

			CASE i = 11  && Ariat-KY only  tmarg changed from 10 to 11 6/9/16
				SELECT outship
				SELECT cWhse1 AS office,accountid,wo_num,WO_DATE AS DATE,;
					ship_ref,cnee_ref,bol_no AS bol,SCAC,ship_via,.F. AS PROCESSED,CONSIGNEE,outshipid,;
					"945 " AS EDI_TYPE,"TRUCK-TRIGGERS" AS TRIG_FROM,;
					"FMI-PROC" AS TRIG_BY,DATETIME() AS trig_time,.F. AS DSDC, ;
					.F. AS parcel ;
					FROM outship ;
					WHERE BETWEEN(outship.del_date,dDaysBack,DATE()) ;
					AND !EMPTYnul(outship.del_date) ;
					AND !EMPTY(outship.bol_no) ;
					AND !EMPTY(outship.SCAC) ;
					AND outship.accountid = 6532 ;
					GROUP BY 1,2,5 ;
					ORDER BY bol_no,ship_ref ;
					INTO CURSOR outtrk1 READWRITE

			OTHERWISE
				SELECT IIF(INLIST(cWhse1,"I","J"),"N",cWhse1) AS office,accountid,wo_num,WO_DATE AS DATE,;
					ship_ref,cnee_ref,bol_no AS bol,SCAC,ship_via,.F. AS PROCESSED,CONSIGNEE,outshipid,;
					IIF((outship.accountid = 6303 AND "EXTWHNO"$outship.shipins),"945E","945 ") AS EDI_TYPE, ;
					"TRUCK-TRIGGERS" AS TRIG_FROM,;
					"FMI-PROC" AS TRIG_BY,DATETIME() AS trig_time,.F. AS DSDC, ;
					.F. AS moret, .F. AS parcel ;
					FROM outship ;
					WHERE BETWEEN(outship.del_date,dDaysBack,DATE()) ;
					AND !EMPTYnul(outship.del_date);
					AND !EMPTY(outship.bol_no) ;
					AND !INLIST(outship.accountid,5687) ;
					AND !INLIST(outship.accountid, &gMoretAcctList) ;
					AND !INLIST(outship.accountid, &gMoretAcctList2) ;
					GROUP BY 1,2,5 ;
					ORDER BY bol_no,ship_ref ;
					INTO CURSOR outtrk1 READWRITE
		ENDCASE

		SELECT outtrk1
		LOCATE
		IF lOverridebusy
*			BROWSE
*			SET STEP on
		ENDIF

		SCAN FOR !DELETED()
			IF INLIST(outtrk1.office,"1","2","5","6","7")
				REPLACE outtrk1.office WITH "C" IN outtrk1 NEXT 1
			ENDIF
			cBOLa = ALLTRIM(outtrk1.bol)
			cCR = ALLTRIM(outtrk1.cnee_ref)
			nAccta = outtrk1.accountid
			cSRUsed = outtrk1.ship_ref
			cEDIType = PADR(ALLTRIM(outtrk1.EDI_TYPE),4)
			cxSCAC = ALLTRIM(outtrk1.SCAC)
			IF cSRUsed = '5125852'
*				SET STEP ON
			ENDIF

			IF EMPTY(cxSCAC)  && Added to correct missing SCACs with known dropout issues, 05.26.2017, JoeB
				cShipvia = outtrk1.ship_via
				DO CASE
					CASE INLIST(cShipvia,PADR('USPS PREPAID 1ST CLASS MAIL',31),PADR('USPS FIRST CLASS PACKAGE',31))
						REPLACE outtrk1.SCAC WITH "SFIR" IN outtrk1 NEXT 1
					CASE INLIST(cShipvia,PADR('USPS PREPAID PRIORITY MAIL',31),PADR('PRIORITY MAIL',31))
						REPLACE outtrk1.SCAC WITH "PRRM" IN outtrk1 NEXT 1
					CASE INLIST(cShipvia,PADR('SMARTPOST PREPAID PARCEL SELECT',31),PADR('FEDEX SMARTPOST',31))
						REPLACE outtrk1.SCAC WITH "FSP" IN outtrk1 NEXT 1
					CASE cShipvia = PADR('USPS PARCEL SELECT LIGHTWEIGHT',31)
						REPLACE outtrk1.SCAC WITH "SPAR" IN outtrk1 NEXT 1
					CASE cShipvia = PADR('USPS PRIORITY MAIL',31)
						REPLACE outtrk1.SCAC WITH "SPRI" IN outtrk1 NEXT 1
					CASE cShipvia = PADR('USPS FLAT-RATE PADDED ENVELOPE',31)
						REPLACE outtrk1.SCAC WITH "SPAD" IN outtrk1 NEXT 1
					CASE INLIST(cShipvia,PADR('FEDEX GROUND',31),PADR('FEDEX GROUND PREPAID',31),PADR('FEDEX PREPAID GROUND',31))
						REPLACE outtrk1.SCAC WITH "FDEG" IN outtrk1 NEXT 1
					CASE cShipvia = PADR('FEDEX PREPAID 2-DAY ECONOMY',31)
						REPLACE outtrk1.SCAC WITH "FDXT" IN outtrk1 NEXT 1
					CASE cBOLa = '04331826681915700'
* Do nothing
					OTHERWISE
						lMissSCACs = .T.
						INSERT INTO missscacs (office,accountid,bol_no,ship_ref) VALUES (ALLTRIM(outtrk1.office),nAccta,cBOLa,cSRUsed)
						DELETE NEXT 1 IN outtrk1
						LOOP
				ENDCASE
			ENDIF

			IF (INLIST(outtrk1.accountid, &gMoretAcctList) OR INLIST(outtrk1.accountid, &gMoretAcctList2))
				REPLACE outtrk1.moret WITH .T. IN outtrk1 NEXT 1
			ENDIF

			lParcel = IIF(SEEK(outtrk1.SCAC,'pcarriers','scac'),.T.,.F.)
			REPLACE outtrk1.parcel WITH lParcel IN outtrk1 NEXT 1
			SELECT trigaccts
			IF i # 9
				LOCATE FOR trigaccts.accountid = outtrk1.accountid AND trigaccts.office = outtrk1.office
				IF !FOUND()
					DELETE NEXT 1 IN outtrk1
					LOOP
				ELSE
					IF trigaccts.ACTIVE#.T.
						WAIT WINDOW ALLTRIM(STR(outtrk1.accountid))+" IS NOT AN ACTIVE ACCOUNT..." NOWAIT
						DELETE NEXT 1 IN outtrk1
						LOOP
					ENDIF
				ENDIF
			ENDIF

			SELECT edi_trigger
			LOCATE
			IF  lParcel=.T. && Parcel shipments
				LOCATE FOR edi_trigger.accountid = outtrk1.accountid ;
					AND edi_trigger.bol = PADR(cBOLa,20) ;
					AND edi_trigger.ship_ref = PADR(TRIM(outtrk1.ship_ref),20)
			ELSE
				IF BETWEEN(i,24,26)
					LOCATE FOR edi_trigger.accountid = outtrk1.accountid ;
						AND edi_trigger.EDI_TYPE = outtrk1.EDI_TYPE ;
						AND edi_trigger.wo_num = outtrk1.wo_num ;
						AND edi_trigger.bol = PADR(cBOLa,20) ;
						AND edi_trigger.ship_ref = PADR(ALLTRIM(outtrk1.ship_ref),20)

				ELSE
					LOCATE FOR edi_trigger.accountid = outtrk1.accountid ;
						AND edi_trigger.bol = PADR(cBOLa,20) ;
						AND edi_trigger.EDI_TYPE == cEDIType ;
						AND TTOD(edi_trigger.trig_time)> IIF(edi_trigger.accountid = 6612,DATE()-25,{^2015-01-01})
				ENDIF
			ENDIF

			IF FOUND()
				SELECT outtrk1
				DO CASE
					CASE (i = 20) OR lParcel=.T.
						DELETE NEXT 1 IN outtrk1
					OTHERWISE
						nrec1 = RECNO()
						LOCATE
						DELETE FOR outtrk1.bol = cBOLa AND outtrk1.accountid = nAccta
						GO nrec1
				ENDCASE
			ENDIF
		ENDSCAN

		SELECT outtrk1
		LOCATE
		IF lTesting
*BROWSE
		ENDIF

		SCAN
			IF outtrk1.accountid = 4694
				SELECT outship
				IF SEEK(outtrk1.outshipid,'outship','outshipid')
					IF "MR*0073"$outship.shipins
						REPLACE outtrk1.DSDC WITH .T. IN outtrk1
					ENDIF
				ENDIF
			ENDIF
			IF moretaccount("outtrk1.accountid") && Moret
				REPLACE outtrk1.moret WITH .T.
			ENDIF
		ENDSCAN
		LOCATE

		WAIT CLEAR
		IF EOF()
			WAIT WINDOW "No outstanding 945s for Whse Group/Mod: "+cMod TIMEOUT 3
		ELSE
			IF !lTesting
			
			SET STEP ON 
			
				WAIT WINDOW "Inserting 945 recs for Whse Group/Mod: "+cMod TIMEOUT 1
				SELECT edi_trigger
				APPEND FROM DBF('outtrk1')
			ENDIF
			USE IN outtrk1
		ENDIF
		IF USED('outship')
			USE IN outship
		ENDIF
		IF USED('manifest')
			USE IN manifest
		ENDIF
		IF USED('shiphdr')
			USE IN shiphdr
		ENDIF
	ENDFOR

	IF !lTesting
		IF lMissSCACs
			SELECT missscacs
			IF RECCOUNT()>0 AND missscacs.bol_no # '04907456652825492'
				missscacmail()
			ENDIF
		ENDIF
	ENDIF

	schedupdate()

*!*	Added 09.08.2016 to record runs of this process
	SELECT 0
	USE F:\3pl\DATA\trigger_runs
	INSERT INTO trigger_runs (PROCESS,run_time) VALUES ("TRUCKING",DATETIME())
	USE IN trigger_runs

	NormalExit = .T.

CATCH TO oErr
	IF !NormalExit
		SET STEP ON
		SELECT 0
		USE F:\3pl\DATA\mailmaster ALIAS mm
		SELECT mm
		LOCATE FOR EDI_TYPE = "MISC" AND taskname = "GENERAL"
		tsubject = "Trucking Trigger Error ("+TRANSFORM(oErr.ERRORNO)+") at "+TTOC(DATETIME())
		tattach  = ""
		tSendto = IIF(mm.Use_Alt,mm.sendtoalt,mm.sendto)
		tCC = IIF(mm.Use_Alt,mm.ccalt,mm.cc)
*		tCC = tccerr  removed this 10/21/2015 PG
		tmessage =  "Trucking Trigger Error..... Please fix me........!"

		lcSourceMachine = SYS(0)
		lcSourceProgram = SYS(16)

		IF EMPTY(cErrMsg)
			tmessage =tmessage+ "Try/CATCH Exeception Message:"+CHR(13)+;
				[  Error: ] + STR(oErr.ERRORNO) +CHR(13)+;
				[  LineNo: ] + STR(oErr.LINENO) +CHR(13)+;
				[  Message: ] + oErr.MESSAGE +CHR(13)+;
				[  Procedure: ] + oErr.PROCEDURE +CHR(13)+;
				[  Details: ] + oErr.DETAILS +CHR(13)+;
				[  StackLevel: ] + STR(oErr.STACKLEVEL) +CHR(13)+;
				[  LineContents: ] + oErr.LINECONTENTS+CHR(13)+;
				[  UserValue: ] + oErr.USERVALUE+CHR(13)+;
				[  Computer:  ] +lcSourceMachine+CHR(13)+;
				[  940 file:  ] +xfile+CHR(13)+;
				[  Program:   ] +lcSourceProgram
		ELSE
			tmessage = tmessage+CHR(13)+CHR(13)+"Error: "+cErrMsg
		ENDIF

		tattach  = ""
		tfrom    ="Toll EDI Processing Center <toll-edi-ops@tollgroup.com>"
		IF lemail
			DO FORM m:\dev\frm\dartmail2 WITH tSendto,tfrom,tsubject,tCC,tattach,tmessage,"A"
		ENDIF
	ENDIF

FINALLY
	CLOSE DATABASES ALL
	WAIT WINDOW "Finished triggering all trucking accounts...exiting" TIMEOUT 2
	SELECT 0
	USE F:\edirouting\FTPSETUP SHARED
	REPLACE FTPSETUP.chkbusy WITH .F. FOR FTPSETUP.TRANSFER = "TRIG-TRUCK945"
	CLOSE DATABASES ALL
	WAIT CLEAR
ENDTRY

**********************
PROCEDURE missscacmail
**********************
	SELECT missscacs
	cOutFile = 'h:\fox\missscacs.xls'
	COPY TO &cOutFile TYPE XL5
	USE IN missscacs

	SELECT 0
	USE F:\3pl\DATA\mailmaster ALIAS mm
	SELECT mm
	LOCATE FOR EDI_TYPE = "MISC" AND taskname = "GENERAL"
	tfrom    ="Toll EDI Processing Center <toll-edi-ops@tollgroup.com>"
	tsubject = "Missing SCACs in Trucking Trigger process "+TTOC(DATETIME())
	tattach  = cOutFile
	tSendto = IIF(mm.Use_Alt,mm.sendtoalt,mm.sendto)
	tCC = IIF(mm.Use_Alt,mm.ccalt,mm.cc)
	tmessage =  "Shipments with Missing SCAC codes are attached"
	DO FORM m:\dev\frm\dartmail2 WITH tSendto,tfrom,tsubject,tCC,tattach,tmessage,"A"
ENDPROC
