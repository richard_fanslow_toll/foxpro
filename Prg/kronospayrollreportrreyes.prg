* EXE goes in F:\UTIL\KRONOS\KronosPayrollReportRREYES.EXE

utilsetup("KRONOSPAYROLLREPORTRREYES")

LOCAL loKronosPayrollReport
loKronosPayrollReport = CREATEOBJECT('KRONOSPAYROLLREPORTRREYES')
loKronosPayrollReport.MAIN()

schedupdate()

CLOSE DATABASES ALL
RETURN

#DEFINE LOGIT 1
#DEFINE WAITIT 2
#DEFINE NOWAITIT 4
#DEFINE SENDIT 8
#DEFINE CRLF CHR(13) + CHR(10)
#DEFINE RETURN_DATA_MANDATORY .F.
#DEFINE RETURN_DATA_NOT_MANDATORY .T.

DEFINE CLASS KRONOSPAYROLLREPORTRREYES AS CUSTOM
	cProcessName = 'KronosPayrollReport'
	lTestMode = .F.  && .F. FOR NORMAL OPERATION, .T. TO SUPPRESS ALL BUT DEVELOPER EMAILS AND CHANGE INPUT TABLE, ETC.
	lAutoYield = .T.
	cStartTime = TTOC(DATETIME())
	* connection properties
	nSQLHandle = 0
	* file properties
	nFileHandle = 0

	* date properties
	dtNow = DATETIME()

	dToday = DATE()
	* to force different pay weeks
	*dToday = {^2010-01-09}

	cToday = DTOC(DATE())

	* wait window properties
	nWaitWindowTimeout = 2
	lWaitWindowIsOn = .T.

	* logfile properties
	lLoggingIsOn = .T.
	cLogFile = 'F:\UTIL\KRONOS\Logfiles\KronosPayrollReport-RReyes_log.txt'

	* INTERNAL email properties
	lSendInternalEmailIsOn = .T.
	cFrom = 'FMI Corporate <fmicorporate@fmiint.com>'
	*cSendTo = 'Mark Bennett <mbennett@fmiint.com>'
	cSendTo = ''
	cSendToAUDIT = ''
	cCC = ''
	cSubject = 'Kronos Payroll Report for: ' + TTOC(DATETIME())
	cAttach = ''
	cBodyText = ''

	* reporting logic properties
	lExcludeEmployeesWith9999OnReportDate = .T.

	cCOMPUTERNAME = ''
	cUSERNAME = ''

	FUNCTION INIT
		IF NOT DODEFAULT()
			RETURN .F.
		ENDIF
		WITH THIS
			CLEAR
			*SET RESOURCE OFF
			CLOSE DATA
			SET CENTURY ON
			SET DATE YMD
			SET HOURS TO 24
			SET ANSI ON
			SET TALK OFF
			SET DELETED ON
			SET CONSOLE OFF
			SET EXCLUSIVE OFF
			SET SAFETY OFF
			SET EXACT OFF
			SET STATUS BAR ON
			SET SYSMENU OFF
			SET ENGINEBEHAVIOR 70
			_VFP.AUTOYIELD = .lAutoYield
			.cCOMPUTERNAME = GETENV("COMPUTERNAME")
			.cUSERNAME = GETENV("USERNAME")
			IF .lTestMode THEN
				.cSendTo = 'Mark Bennett <mbennett@fmiint.com>'
				.cSendToAUDIT = 'Mark Bennett <mbennett@fmiint.com>'
				.cLogFile = 'F:\UTIL\KRONOS\Logfiles\KronosPayrollReport-RReyes_log_TESTMODE.txt'
			ENDIF
			.lLoggingIsOn = .lLoggingIsOn AND NOT EMPTY(.cLogFile)
			IF .lLoggingIsOn THEN
				SET ALTERNATE TO (.cLogFile) ADDITIVE
				SET ALTERNATE ON
			ENDIF
		ENDWITH
	ENDFUNC


	FUNCTION DESTROY
		WITH THIS
			IF .lLoggingIsOn  THEN
				SET ALTERNATE OFF
				SET ALTERNATE TO
			ENDIF
		ENDWITH
		DODEFAULT()
	ENDFUNC


	FUNCTION MAIN
		WITH THIS
			LOCAL lnNumberOfErrors, lcSQL, lcSQL2, lcSQL3, lcSQL4, loError, lcTopBodyText, llValid
			LOCAL lcXLFileName, ldStartDate, ldEndDate, lcStartDate, lcEndDate, ldDate
			LOCAL lcSQLStartDate, lcSQLEndDate, lcXLFileName, ldYesterday
			LOCAL oExcel, oWorkbook, oWorksheet, lnRow, lcRow, lnStartRow, lcStartRow, lnEndRow, lnPP, lcPP, lnNN, lcNN, lcJJ
			LOCAL lcColsToProcess, lnColumn, lcBoldColumn, i, j, lcBoldRow, luCellValue, lcTargetDirectory, ldPayDate, lcFiletoSaveAs
			LOCAL lcFileDate, lcDivision, lcRateType, lcTitle, llSaveAgain, lcPayCode, lcSpecialWhere
			LOCAL lcSQLToday, lcSQLTomorrow, ldToday, ldTomorrow, lcExcluded9999s
			LOCAL lcAuditSubject, oWorkbookAUDIT, oWorksheetAUDIT, lcSendTo, lcCC, lcBodyText, lcXLFileNameAUDIT, lcTotalRow
			LOCAL lcTRWhere, lcOrderBy, lnStraightHours, lnStraightDollars, lnOTDollars, lcCurEmployee, lcCurEmployeeStartRow

			TRY
				lnNumberOfErrors = 0
				lcTopBodyText = ''
				ldToday = .dToday
				ldTomorrow = ldToday + 1
				ldYesterday = ldToday - 1

				*.TrackProgress('==================================================================================================================', LOGIT+SENDIT)
				.TrackProgress('KRONOS PAYROLL REPORT process started....', LOGIT+SENDIT+NOWAITIT)
				.TrackProgress('COMPUTERNAME = ' + .cCOMPUTERNAME, LOGIT+SENDIT)
				.TrackProgress('USERNAME = ' + .cUSERNAME, LOGIT+SENDIT)
				.TrackProgress('==================================================================================================================', LOGIT+SENDIT)
				IF .lTestMode THEN
					.TrackProgress('                        TEST MODE                        ', LOGIT+SENDIT)
					.TrackProgress('==================================================================================================================', LOGIT+SENDIT)
				ENDIF
				.TrackProgress('', LOGIT)

				* calculate pay period start/end dates.
				* We go back from today until we hit a Saturday (DOW = 7) , which is the end day of the previous pay period.
				* then we subtract 6 days from that to get the prior Sunday (DOW = 1), which is the start day of the previous pay period.

				.cToday = DTOC(.dToday)

				ldDate = .dToday
				DO WHILE DOW(ldDate,1) <> 7
					ldDate = ldDate - 1
				ENDDO
				ldEndDate = ldDate
				ldStartDate = ldDate - 6


				************************************************************
				************************************************************
				************************************************************
				* to process non-standard date range, hardcode dates below
				* ONLY WORKS IN TEST MODE!
				*!*					IF .lTestMode THEN
				*!*						ldStartDate = {^2008-09-01}
				*!*						ldEndDate = {^2008-09-30}
				*!*						.lExcludeEmployeesWith9999OnReportDate = .F.
				*!*						.lProcessEPIE8799CSVFile = .F.
				*!*						.lMakeSummaryByHomeDept = .F.
				*!*					ENDIF

				* produce a WEEK-TO-DATE report for all 03-0650 local drivers for Ralph Reyes per Kenny
				lcSpecialWhere = " AND (D.LABORLEV2NM = '03')  AND (D.LABORLEV3NM = '0650 ')"
				.cSendTo = "mdivirgilio@fmiint.com, MDrew@fmiint.com, JDamato@fmiint.com, TBranco@fmiint.com"
				.cCC = 'mbennett@fmiint.com, mike.drew@tollgroup.com, mdivirgilio@fmiint.com, Michael.DiVirgilio@Tollgroup.com'

				* start date = Sunday of current pay period
				* end date = yesterday (the report will run early each weekday morning)
				* To get start date, we go back from today until we hit a Sunday (DOW = 1) , which is the start of the current pay period.
				ldDate = .dToday
				ldEndDate = ldDate - 1
				DO WHILE DOW(ldDate,1) <> 1
					ldDate = ldDate - 1
				ENDDO
				ldStartDate = ldDate

				IF .lTestMode THEN
					.cSendTo = 'Mark Bennett <mbennett@fmiint.com>'
					.cSendToAUDIT = 'Mark Bennett <mbennett@fmiint.com>'
					.cLogFile = 'F:\UTIL\KRONOS\Logfiles\KronosPayrollReport-RREYES_log_TESTMODE.txt'
				ENDIF
				************************************************************
				************************************************************
				************************************************************


				*!*					* extra where filtering
				*!*					* ONLY WORKS IN TEST MODE!
				*!*					IF .lTestMode THEN
				*!*						lcSpecialWhere = " AND (D.LABORLEV2NM IN ('61','71','72','73','74')) "
				*!*					ENDIF


				SET DATE AMERICAN

				lcStartDate = STRTRAN(DTOC(ldStartDate),"/","-")
				lcEndDate = STRTRAN(DTOC(ldEndDate),"/","-")

				SET DATE YMD

				*	 create "YYYYMMDD" safe date format for use in SQL query
				* NOTE: for SQL we are going to use < EndDate ( not <= ), so we are going to add 1 to that date here for querying purposes.
				lcSQLStartDate = STRTRAN(DTOC(ldStartDate),"/","")
				lcSQLEndDate = STRTRAN(DTOC(ldEndDate + 1),"/","")
				lcSQLToday = STRTRAN(DTOC(ldToday),"/","")
				lcSQLTomorrow = STRTRAN(DTOC(ldTomorrow),"/","")

				SET DATE AMERICAN
				.cSubject = 'Local Drivers - Week To Date Timecard Summary for period beginning: ' + lcStartDate

				IF USED('CURWTKHOURS') THEN
					USE IN CURWTKHOURS
				ENDIF

				IF USED('CURWTKHOURS2') THEN
					USE IN CURWTKHOURS2
				ENDIF

				IF USED('CUR9999EMPS') THEN
					USE IN CUR9999EMPS
				ENDIF

				IF USED('CURAPPROVALS') THEN
					USE IN CURAPPROVALS
				ENDIF

				IF USED('CURMISSINGAPPROVALS') THEN
					USE IN CURMISSINGAPPROVALS
				ENDIF

				IF USED('CURSSNUMS') THEN
					USE IN CURSSNUMS
				ENDIF

				lcSQL = ;
					" SELECT " + ;
					" A.APPLYDTM AS PAYDATE, " + ;
					" D.LABORLEV2NM AS DIVISION, " + ;
					" D.LABORLEV3NM AS DEPT, " + ;
					" D.LABORLEV7NM AS TIMERVWR, " + ;
					" C.FULLNM AS EMPLOYEE, " + ;
					" C.PERSONNUM AS FILE_NUM, " + ;
					" C.PERSONID, " + ;
					" E.ABBREVIATIONCHAR AS PAYCODE, " + ;
					" E.NAME AS PAYCODEDESC, " + ;
					" A.DURATIONSECSQTY AS TOTSECS, " + ;
					" A.MONEYAMT " + ;
					" FROM WFCTOTAL A " + ;
					" JOIN WTKEMPLOYEE B " + ;
					" ON B.EMPLOYEEID = A.EMPLOYEEID " + ;
					" JOIN PERSON C " + ;
					" ON C.PERSONID = B.PERSONID " + ;
					" JOIN LABORACCT D " + ;
					" ON D.LABORACCTID = A.LABORACCTID " + ;
					" JOIN PAYCODE E " + ;
					" ON E.PAYCODEID = A.PAYCODEID " + ;
					" WHERE (A.APPLYDTM >= '" + lcSQLStartDate + "')" + ;
					" AND (A.APPLYDTM <= '" + lcSQLEndDate + "')" + ;
					" AND (D.LABORLEV1NM = 'SXI') " + ;
					lcSpecialWhere + ;
					" AND (D.LABORLEV7NM <> '9999') "


				* 5/16/06 MB: Now using APPLYDTM instead of ADJSTARTDTM to solve problem where hours crossing Midnight Saturday were not being reported.
				*					" WHERE (A.ADJSTARTDTM >= '" + lcSQLStartDate + "')" + ;
				*					" AND (A.ADJSTARTDTM < '" + lcSQLEndDate + "')" + ;

				IF .lTestMode THEN
					.TrackProgress('', LOGIT+SENDIT)
					.TrackProgress('lcSQL =' + lcSQL, LOGIT+SENDIT)
					*!*						.TrackProgress('', LOGIT+SENDIT)
					*!*						.TrackProgress('lcSQL2 =' + lcSQL2, LOGIT+SENDIT)
					*!*						.TrackProgress('', LOGIT+SENDIT)
					*!*						.TrackProgress('lcSQL3 =' + lcSQL3, LOGIT+SENDIT)
					*!*						.TrackProgress('', LOGIT+SENDIT)
					*!*						.TrackProgress('lcSQL4 =' + lcSQL4, LOGIT+SENDIT)
				ENDIF

				.TrackProgress('Connecting to WTK System....', LOGIT+SENDIT)

				* access kronos
				OPEN DATABASE F:\UTIL\KRONOS\KRONOSEDI

				.nSQLHandle = SQLCONNECT("WTK_ON_PAYROLL2","sa","kr0n1tes")

				IF .nSQLHandle > 0 THEN

					*!*						IF .ExecSQL(lcSQL, 'CURWTKHOURS2', RETURN_DATA_MANDATORY) AND ;
					*!*								.ExecSQL(lcSQL2, 'CUR9999EMPS', RETURN_DATA_MANDATORY) AND ;
					*!*								.ExecSQL(lcSQL3, 'CURAPPROVALS', RETURN_DATA_NOT_MANDATORY) AND ;
					*!*								.ExecSQL(lcSQL4, 'CURSSNUMS', RETURN_DATA_MANDATORY) THEN

					IF .ExecSQL(lcSQL, 'CURWTKHOURS2', RETURN_DATA_MANDATORY) THEN

						SELECT EMPLOYEE, DIVISION, DEPT, FILE_NUM, TIMERVWR, TTOD(PAYDATE) AS PAYDATE, PERSONID, PAYCODE, PAYCODEDESC, ;
							(SUM(TOTSECS / 3600.00)) AS TOTHOURS, ;
							SUM(MONEYAMT) AS TOTPAY ;
							FROM CURWTKHOURS2 ;
							INTO CURSOR CURWTKHOURS ;
							GROUP BY 1,2,3,4,5,6,7,8,9 ;
							ORDER BY 1,2,3,4,5,6,7,8,9 ;
							READWRITE

						*!*							SELECT CURWTKHOURS
						*!*							BROWSE

						SELECT CURWTKHOURS
						INDEX ON PAYDATE TAG PAYDATE
						INDEX ON DIVISION TAG TDIV
						INDEX ON DEPT TAG TDEPT
						INDEX ON EMPLOYEE TAG TEMPLOYEE
						INDEX ON FILE_NUM TAG TFILE_NUM

						****** horizontalize cursor so paycodes are in separate columns

						IF USED('CUREMPLOYEES') THEN
							USE IN CUREMPLOYEES
						ENDIF

						SELECT ;
							DIVISION, ;
							DEPT, ;
							EMPLOYEE, ;
							FILE_NUM, ;
							PAYDATE, ;
							0000.00 AS REG_HRS, ;
							0000.00 AS GTD_HRS, ;
							0000.00 AS SD1_HRS, ;
							0000.00 AS SPP_HRS, ;
							0000.00 AS OT_HRS, ;
							0000.00 AS OT20_HRS, ;
							0000.00 AS SICK_HRS, ;
							0000.00 AS VAC_HRS, ;
							0000.00 AS VAC_RLVR_HRS, ;
							0000.00 AS VAC_ADV, ;
							0000.00 AS HDAY_HRS, ;
							0000.00 AS HDAY_ADV, ;
							0000.00 AS PP_VAC_HRS, ;
							0000.00 AS PP_HDAY_HRS, ;
							0000.00 AS PP_PERS_HRS, ;
							0000.00 AS PP_COMP_HRS, ;
							0000.00 AS PERS_HRS, ;
							0000.00 AS PERS_ADV, ;
							0000.00 AS COMP_HRS, ;
							0000.00 AS COMP_ADV, ;
							0000.00 AS JURY_HRS, ;
							0000.00 AS BREAVE_HRS, ;
							0000.00 AS OTHER_HRS, ;
							0000.00 AS UNUSD_SICK, ;
							0000.00 AS RETRO_OTH, ;
							0000.00 AS OTHER_PAY, ;
							0000.00 AS CADRVR_PAY, ;
							0000.00 AS PERS_PAY, ;
							0000.00 AS HOURLYRATE ;
							FROM CURWTKHOURS ;
							INTO CURSOR CUREMPLOYEES ;
							GROUP BY EMPLOYEE, DIVISION, DEPT, FILE_NUM, PAYDATE ;
							ORDER BY EMPLOYEE, DIVISION, DEPT, FILE_NUM, PAYDATE ;
							READWRITE

						SELECT CUREMPLOYEES
						SCAN
							SCATTER MEMVAR
							STORE 0000.00 TO ;
								m.REG_HRS, ;
								m.GTD_HRS, ;
								m.SD1_HRS, ;
								m.SPP_HRS, ;
								m.OT_HRS, ;
								m.OT20_HRS, ;
								m.SICK_HRS, ;
								m.VAC_HRS, ;
								m.VAC_RLVR_HRS, ;
								m.VAC_ADV, ;
								m.HDAY_HRS, ;
								m.HDAY_ADV, ;
								m.PP_VAC_HRS, ;
								m.PP_HDAY_HRS, ;
								m.PP_PERS_HRS, ;
								m.PP_COMP_HRS, ;
								m.PERS_HRS, ;
								m.PERS_ADV, ;
								m.COMP_HRS, ;
								m.COMP_ADV, ;
								m.JURY_HRS, ;
								m.BREAVE_HRS, ;
								m.OTHER_HRS, ;
								m.UNUSD_SICK, ;
								m.RETRO_OTH, ;
								m.OTHER_PAY, ;
								m.CADRVR_PAY, ;
								m.PERS_PAY

							SELECT CURWTKHOURS
							SCAN FOR ;
									DIVISION = CUREMPLOYEES.DIVISION AND ;
									DEPT = CUREMPLOYEES.DEPT AND ;
									EMPLOYEE = CUREMPLOYEES.EMPLOYEE AND ;
									FILE_NUM = CUREMPLOYEES.FILE_NUM AND ;
									PAYDATE = CUREMPLOYEES.PAYDATE

								lcPayCode = UPPER(ALLTRIM(CURWTKHOURS.PAYCODE))
								DO CASE
									CASE lcPayCode == "REG"
										m.REG_HRS = m.REG_HRS + CURWTKHOURS.TOTHOURS
									CASE lcPayCode == "OTM"
										m.OT_HRS = m.OT_HRS + CURWTKHOURS.TOTHOURS
									CASE lcPayCode == "DTM"
										m.OT20_HRS = m.OT20_HRS + CURWTKHOURS.TOTHOURS
									CASE INLIST(lcPayCode,"VAC","VH7","VAD","VA7")
										m.VAC_HRS = m.VAC_HRS + CURWTKHOURS.TOTHOURS
										*IF lcPayCode == "VAD"
										IF INLIST(lcPayCode,"VAD","VA7")
											m.VAC_ADV = m.VAC_ADV + CURWTKHOURS.TOTHOURS
										ENDIF
									CASE lcPayCode == "RVP"
										* Vacation Rollovers
										m.VAC_RLVR_HRS = m.VAC_RLVR_HRS + CURWTKHOURS.TOTHOURS
									CASE INLIST(lcPayCode,"HOL","HAD")
										m.HDAY_HRS = m.HDAY_HRS + CURWTKHOURS.TOTHOURS
										IF lcPayCode == "HAD" THEN
											m.HDAY_ADV = m.HDAY_ADV + CURWTKHOURS.TOTHOURS
										ENDIF
										*CASE lcPayCode == "SIC"
									CASE INLIST(lcPayCode,"SIC","SC7")
										m.SICK_HRS = m.SICK_HRS + CURWTKHOURS.TOTHOURS
									CASE INLIST(lcPayCode,"BDY","BAD","PRS","PAD")
										m.PERS_HRS = m.PERS_HRS + CURWTKHOURS.TOTHOURS
										IF INLIST(lcPayCode,"BAD","PAD") THEN
											m.PERS_ADV = m.PERS_ADV + CURWTKHOURS.TOTHOURS
										ENDIF
									CASE INLIST(lcPayCode,"CMP","CAD")
										m.COMP_HRS = m.COMP_HRS + CURWTKHOURS.TOTHOURS
										IF lcPayCode == "CAD" THEN
											m.COMP_ADV = m.COMP_ADV + CURWTKHOURS.TOTHOURS
										ENDIF
									CASE lcPayCode == "JUR"
										m.JURY_HRS = m.JURY_HRS + CURWTKHOURS.TOTHOURS
									CASE lcPayCode == "BRV"
										m.BREAVE_HRS = m.BREAVE_HRS + CURWTKHOURS.TOTHOURS
									CASE lcPayCode == "GTD"
										m.GTD_HRS = m.GTD_HRS + CURWTKHOURS.TOTHOURS
									CASE lcPayCode == "OTH"
										m.OTHER_HRS = m.OTHER_HRS + CURWTKHOURS.TOTHOURS
									CASE lcPayCode == "SD1"
										m.SD1_HRS = m.SD1_HRS + CURWTKHOURS.TOTHOURS
									CASE lcPayCode == "PPV"
										m.PP_VAC_HRS = m.PP_VAC_HRS + CURWTKHOURS.TOTHOURS
									CASE lcPayCode == "PPH"
										m.PP_HDAY_HRS = m.PP_HDAY_HRS + CURWTKHOURS.TOTHOURS
									CASE INLIST(lcPayCode,"PPB","PPP")
										m.PP_PERS_HRS = m.PP_PERS_HRS + CURWTKHOURS.TOTHOURS
									CASE lcPayCode == "PPC"
										m.PP_COMP_HRS = m.PP_COMP_HRS + CURWTKHOURS.TOTHOURS
									CASE lcPayCode == "USI"
										m.UNUSD_SICK = m.UNUSD_SICK + CURWTKHOURS.TOTHOURS
									CASE lcPayCode == "SPP"
										m.SPP_HRS = m.SPP_HRS + CURWTKHOURS.TOTHOURS
									CASE lcPayCode == "OTP"
										m.OTHER_PAY = m.OTHER_PAY + CURWTKHOURS.TOTPAY
									CASE lcPayCode == "RTR"
										m.RETRO_OTH = m.RETRO_OTH + CURWTKHOURS.TOTPAY
									CASE lcPayCode == "CAI"
										m.CADRVR_PAY = m.CADRVR_PAY + CURWTKHOURS.TOTPAY
									CASE lcPayCode == "PRS"
										m.PERS_PAY = m.PERS_PAY + CURWTKHOURS.TOTPAY
									OTHERWISE
										* NOTHING
								ENDCASE

							ENDSCAN
							* accumulate totals
							SELECT CUREMPLOYEES
							GATHER MEMVAR
						ENDSCAN

						WAIT WINDOW NOWAIT "Preparing data..."

						SELECT CUREMPLOYEES
						GOTO TOP
						IF EOF() THEN
							.TrackProgress("There was no data to export!", LOGIT+SENDIT)
						ELSE

							lcRateType = "HOURLY"

							WAIT WINDOW NOWAIT "Opening Excel..."
							oExcel = CREATEOBJECT("excel.application")
							oExcel.VISIBLE = .F.
							oWorkbook = oExcel.workbooks.OPEN("F:\UTIL\KRONOS\TEMPLATES\KRONOS_RALPH_REYES_WTD_2.XLS")

							WAIT WINDOW NOWAIT "Looking for target directory..."
							* see if target directory exists, and, if not, create it; e.g. F:\timeclk\adpfiles\01-07-2005
							ldPayDate = ldEndDate + 6
							lcFileDate = PADL(MONTH(ldPayDate),2,"0") + "-"  + PADL(DAY(ldPayDate),2,"0") + "-" + PADL(YEAR(ldPayDate),4,"0")
							lcTargetDirectory = "F:\UTIL\KRONOS\PAYROLLREPORTS\WTD-LOCALDRIVERS\"
							lcFiletoSaveAs = lcTargetDirectory + lcFileDate + " WTD for Local Drivers"
							lcXLFileName = lcFiletoSaveAs + ".XLS"

							* create directory if it doesn't exist
							IF NOT DIRECTORY(lcTargetDirectory) THEN
								MKDIR (lcTargetDirectory)
								WAIT WINDOW TIMEOUT 1 "Made Target Directory " + lcTargetDirectory
							ENDIF

							* if target directory exists, save there
							llSaveAgain = .F.
							IF DIRECTORY(lcTargetDirectory) THEN
								IF FILE(lcXLFileName) THEN
									DELETE FILE (lcXLFileName)
								ENDIF
								oWorkbook.SAVEAS(lcFiletoSaveAs)
								* set flag to save again after sheet is populated
								llSaveAgain = .T.
							ENDIF
							***********************************************************************************************************************
							***********************************************************************************************************************
							WAIT WINDOW NOWAIT "Building Payroll Report spreadsheet..."

							lnRow = 3
							lnStartRow = lnRow + 1
							lcStartRow = ALLTRIM(STR(lnStartRow))

							oWorksheet = oWorkbook.Worksheets[1]
							oWorksheet.RANGE("A" + lcStartRow,"Y1000").clearcontents()

							oWorksheet.RANGE("A" + lcStartRow,"Y1000").FONT.SIZE = 10
							oWorksheet.RANGE("A" + lcStartRow,"Y1000").FONT.NAME = "Arial Narrow"
							oWorksheet.RANGE("A" + lcStartRow,"Y1000").FONT.bold = .T.

							lcTitle = 'Local Drivers - Week To Date Timecard Summary for period beginning: ' + lcStartDate
							oWorksheet.RANGE("A1").VALUE = lcTitle

							oWorksheet.RANGE("D" + lcStartRow,"U1000").FONT.bold = .F.

							* main scan/processing
							SELECT CUREMPLOYEES
							lcCurEmployee = CUREMPLOYEES.EMPLOYEE
							lcCurEmployeeStartRow = LTRIM(STR(lnRow + 1))
							SCAN
								lnRow = lnRow + 1
								IF NOT (lcCurEmployee == CUREMPLOYEES.EMPLOYEE) THEN
									lnRow = lnRow + 1
									lcCurEmployeeStartRow = LTRIM(STR(lnRow))
								ENDIF
								lcRow = LTRIM(STR(lnRow))
								oWorksheet.RANGE("A" + lcRow).VALUE = CUREMPLOYEES.EMPLOYEE
								oWorksheet.RANGE("B" + lcRow).VALUE = CUREMPLOYEES.FILE_NUM
								oWorksheet.RANGE("C" + lcRow).VALUE = DTOC(CUREMPLOYEES.PAYDATE)+ "  " + CDOW(CUREMPLOYEES.PAYDATE)

								*oWorksheet.RANGE("D" + lcRow).FORMULA = "=SUM(E" + lcRow + "..V" + lcRow + ")"
								oWorksheet.RANGE("D" + lcRow).FORMULA = "=SUM(E" + lcCurEmployeeStartRow + "..V" + lcRow + ")"

								oWorksheet.RANGE("E" + lcRow).VALUE = CUREMPLOYEES.REG_HRS
								oWorksheet.RANGE("F" + lcRow).VALUE = CUREMPLOYEES.GTD_HRS
								oWorksheet.RANGE("G" + lcRow).VALUE = CUREMPLOYEES.SD1_HRS
								oWorksheet.RANGE("H" + lcRow).VALUE = CUREMPLOYEES.OT_HRS
								oWorksheet.RANGE("I" + lcRow).VALUE = CUREMPLOYEES.OT20_HRS
								oWorksheet.RANGE("J" + lcRow).VALUE = CUREMPLOYEES.SICK_HRS
								oWorksheet.RANGE("K" + lcRow).VALUE =  ( CUREMPLOYEES.VAC_HRS + CUREMPLOYEES.VAC_RLVR_HRS )
								oWorksheet.RANGE("L" + lcRow).VALUE = CUREMPLOYEES.HDAY_HRS
								oWorksheet.RANGE("M" + lcRow).VALUE = CUREMPLOYEES.PERS_HRS
								oWorksheet.RANGE("N" + lcRow).VALUE = CUREMPLOYEES.COMP_HRS
								oWorksheet.RANGE("O" + lcRow).VALUE = CUREMPLOYEES.JURY_HRS
								oWorksheet.RANGE("P" + lcRow).VALUE = CUREMPLOYEES.BREAVE_HRS
								oWorksheet.RANGE("Q" + lcRow).VALUE = CUREMPLOYEES.OTHER_HRS
								oWorksheet.RANGE("R" + lcRow).VALUE = CUREMPLOYEES.PP_VAC_HRS
								oWorksheet.RANGE("S" + lcRow).VALUE = CUREMPLOYEES.PP_HDAY_HRS
								oWorksheet.RANGE("T" + lcRow).VALUE = CUREMPLOYEES.PP_PERS_HRS
								oWorksheet.RANGE("U" + lcRow).VALUE = CUREMPLOYEES.PP_COMP_HRS
								oWorksheet.RANGE("V" + lcRow).VALUE = CUREMPLOYEES.UNUSD_SICK
								oWorksheet.RANGE("A" + lcRow,"V" + lcRow).Interior.ColorIndex = IIF(((lnRow % 2) = 1),15,0)
								lcCurEmployee = CUREMPLOYEES.EMPLOYEE
							ENDSCAN
							lnEndRow = lnRow
							lcEndRow = ALLTRIM(STR(lnEndRow))
							lcColsToProcess = "DEFGHIJKLMNOPQRSTUV"
							FOR i = 1 TO LEN(lcColsToProcess)
								lcBoldColumn = SUBSTR(lcColsToProcess,i,1)
								FOR j = lnStartRow TO lnEndRow
									lcBoldRow = ALLTRIM(STR(j))
									luCellValue = oWorksheet.RANGE(lcBoldColumn + lcBoldRow).VALUE
									IF TYPE([luCellValue])= "N" THEN
										IF luCellValue > 0.0 THEN
											oWorksheet.RANGE(lcBoldColumn + lcBoldRow).FONT.bold = .T.
											* nothing
										ELSE
											oWorksheet.RANGE(lcBoldColumn + lcBoldRow).VALUE = ""
										ENDIF
									ENDIF
								ENDFOR  && j
							ENDFOR  && i
*!*								lnRow = lnRow + 2
*!*								lcRow = LTRIM(STR(lnRow))
*!*								oWorksheet.RANGE("A" + lcRow,"U" + lcRow).FONT.SIZE = 9
*!*								oWorksheet.RANGE("A" + lcRow).VALUE = "Totals:"
*!*								oWorksheet.RANGE("D" + lcRow).FORMULA = "=sum(D" + lcStartRow + ":D" + lcEndRow + ")"

							oWorksheet.PageSetup.PrintArea = "$A$"  + lcStartRow + ":$V$" + lcRow

							* save again
							IF llSaveAgain THEN
								oWorkbook.SAVE()
							ENDIF
							oWorkbook.CLOSE()

							IF FILE(lcXLFileName) THEN
								.cAttach = lcXLFileName
								.TrackProgress('Created Payroll File : ' + lcXLFileName, LOGIT+SENDIT)
								.TrackProgress('KRONOS PAYROLL REPORT process ended normally.', LOGIT+SENDIT+NOWAITIT)
							ELSE
								.TrackProgress('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!', LOGIT+SENDIT)
								.TrackProgress("ERROR: There was a problem Creating Payroll File : " + lcXLFileName, LOGIT+SENDIT)
								.TrackProgress('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!', LOGIT+SENDIT)
							ENDIF

						ENDIF  && EOF() CUREMPLOYEES

					ENDIF  &&  .ExecSQL(lcSQL, 'CURWTKEMPLOYEE', RETURN_DATA_MANDATORY)

					CLOSE DATABASES ALL
				ELSE
					* connection error
					.TrackProgress('Unable to connect to WTK System.', LOGIT+SENDIT)
				ENDIF   &&  .nSQLHandle > 0
			CATCH TO loError

				.TrackProgress('There was an error.',LOGIT+SENDIT)
				.TrackProgress('The Error # is: ' + TRANSFORM(loError.ERRORNO), LOGIT+SENDIT)
				.TrackProgress('The Error Message is: ' + TRANSFORM(loError.MESSAGE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred in Program: ' + TRANSFORM(loError.PROCEDURE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred at line #: ' + TRANSFORM(loError.LINENO), LOGIT+SENDIT)
				lnNumberOfErrors = lnNumberOfErrors + 1
				IF TYPE('oExcel') = 'O' AND NOT ISNULL(oExcel) THEN
					oExcel.QUIT()
				ENDIF
				CLOSE DATA
				CLOSE ALL

			ENDTRY

			CLOSE DATA
			WAIT CLEAR
			***************** INTERNAL email results ******************************
			.TrackProgress('About to send status email.',LOGIT)
			.TrackProgress('==================================================================================================================', SENDIT)
			.TrackProgress('The logfile is: ' + .cLogFile, SENDIT)
			.TrackProgress('KRONOS PAYROLL REPORT process started: ' + .cStartTime, LOGIT+SENDIT)
			.TrackProgress('KRONOS PAYROLL REPORT process finished: ' + TTOC(DATETIME()), LOGIT+SENDIT)

			IF .lSendInternalEmailIsOn THEN
				* try to trap error from not having dartmail dll's registered on user's pc...
				TRY
					DO FORM dartmail2 WITH .cSendTo,.cFrom,.cSubject,.cCC,.cAttach,.cBodyText,"A"
					.TrackProgress('Sent status email.',LOGIT)
				CATCH TO loError
					*=MESSAGEBOX('There was an error sending the status email. Please contact Mark Bennett.',0+64,"KRONOS PAYROLL REPORT")
					.TrackProgress('There was an error sending the status email.',LOGIT)
					.TrackProgress('The Error # is: ' + TRANSFORM(loError.ERRORNO), LOGIT)
					.TrackProgress('The Error Message is: ' + TRANSFORM(loError.MESSAGE), LOGIT)
					.TrackProgress('The Error occurred in Program: ' + TRANSFORM(loError.PROCEDURE), LOGIT)
					.TrackProgress('The Error occurred at line #: ' + TRANSFORM(loError.LINENO), LOGIT)
					lnNumberOfErrors = lnNumberOfErrors + 1
					CLOSE DATA
				ENDTRY

			ELSE
				.TrackProgress('Did not send status email: THIS.lSendInternalEmailIsOn = FALSE.',LOGIT)
			ENDIF



		ENDWITH
		RETURN
	ENDFUNC && main


	FUNCTION ExecSQL
		LPARAMETERS tcSQL, tcCursorName, tlNoDataReturnedIsOkay
		LOCAL llRetval, lnResult
		WITH THIS
			* close target cursor if it's open
			IF USED(tcCursorName)
				USE IN (tcCursorName)
			ENDIF
			WAIT WINDOW "Fetching remote data; please wait..." NOWAIT
			lnResult = SQLEXEC(.nSQLHandle, tcSQL, tcCursorName)
			llRetval = ( lnResult > 0 )
			IF llRetval THEN
				* see if any data came back
				IF NOT tlNoDataReturnedIsOkay THEN
					IF (NOT USED(tcCursorName)) OR EOF(tcCursorName) THEN
						llRetval = .F.
						.TrackProgress('No data was returned by this query: ' +CRLF + tcSQL, LOGIT+SENDIT)
					ENDIF
				ENDIF
			ELSE
				.TrackProgress("Result Code [" + TRANSFORM(lnResult) + "] returned by this query: " + tcSQL, LOGIT+SENDIT)
			ENDIF
			WAIT CLEAR
			RETURN llRetval
		ENDWITH
	ENDFUNC


	PROCEDURE TrackProgress
		* do any combination of Wait Window, writing to logfile, and adding to body of email,
		* based on nFlags parameter.
		LPARAMETERS tcExpression, tnFlags
		WITH THIS
			IF BITAND(tnFlags,LOGIT) = LOGIT THEN
				IF .lLoggingIsOn THEN
					?
					? .cProcessName + "  " + TTOC(DATETIME()) + ": " + tcExpression
				ENDIF
			ENDIF
			IF .lWaitWindowIsOn THEN
				IF BITAND(tnFlags,NOWAITIT) = NOWAITIT THEN
					WAIT WINDOW tcExpression NOWAIT
				ENDIF
				IF BITAND(tnFlags,WAITIT) = WAITIT THEN
					WAIT WINDOW tcExpression TIMEOUT .nWaitWindowTimeout
				ENDIF
			ENDIF
			IF BITAND(tnFlags,SENDIT) = SENDIT THEN
				IF .lSendInternalEmailIsOn THEN
					.cBodyText = .cBodyText + tcExpression + CRLF + CRLF
				ENDIF
			ENDIF
		ENDWITH
	ENDPROC  &&  TrackProgress


	PROCEDURE SetDate
		LPARAMETERS tdDate
		THIS.dToday = tdDate
	ENDPROC

ENDDEFINE
