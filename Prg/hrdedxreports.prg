* run ADP Deduction report based on passed parameter
* Build EXE as F:\UTIL\HR\DEDXREPORTS\HRDEDXREPORTS.EXE -- PASSING ANY PARAMETER MEANS TO RUN THE ONCE-A-MONTH REPORTS
LPARAMETERS tcMonthly
LOCAL loHRDEDXREPORTS, llTestMode, lcMonthly

*********************************************************************************************************************
llTestMode = .F.   && .F. FOR NORMAL OPERATION, .T. TO SUPPRESS ALL BUT DEVELOPER EMAILS AND CHANGE INPUT TABLE, ETC.
*********************************************************************************************************************

IF NOT llTestMode THEN
	utilsetup("HRDEDXREPORTS")
ENDIF

loHRDEDXREPORTS = CREATEOBJECT('HRDEDXREPORTS', llTestMode)

IF EMPTY(tcMonthly) THEN
	* run the weekly reports (schedule them on Thursday)
	loHRDEDXREPORTS.SetDedCode("K")
	loHRDEDXREPORTS.MAIN()
	loHRDEDXREPORTS.SetDedCode("N")
	loHRDEDXREPORTS.MAIN()
	*!*	loHRDEDXREPORTS.SetDedCode("L")
	*!*	loHRDEDXREPORTS.MAIN()
	*!*	loHRDEDXREPORTS.SetDedCode("O")
	*!*	loHRDEDXREPORTS.MAIN()
ELSE
	* run the monthly reports (schedule them on Day 1)
	loHRDEDXREPORTS.SetDedCode("D")
	loHRDEDXREPORTS.MAIN()
	loHRDEDXREPORTS.SetDedCode("I")
	loHRDEDXREPORTS.MAIN()
	loHRDEDXREPORTS.SetDedCode("S")
	loHRDEDXREPORTS.MAIN()
	loHRDEDXREPORTS.SetDedCode("T")
	loHRDEDXREPORTS.MAIN()
ENDIF

IF NOT llTestMode THEN
	schedupdate()
ENDIF

CLOSE DATABASES ALL
RETURN

#DEFINE LOGIT 1
#DEFINE WAITIT 2
#DEFINE NOWAITIT 4
#DEFINE SENDIT 8
#DEFINE CRLF CHR(13) + CHR(10)
#DEFINE RETURN_DATA_MANDATORY .F.
#DEFINE RETURN_DATA_NOT_MANDATORY .T.
#DEFINE CSVFORMAT 6
#DEFINE WRITE_ONLY_UNBUFFERED 11
#DEFINE COMMA ","

DEFINE CLASS HRDEDXREPORTS AS CUSTOM

	cProcessName = 'HRDEDXREPORTS'

	lTestMode = .F.  && .F. FOR NORMAL OPERATION, .T. TO SUPPRESS ALL BUT DEVELOPER EMAILS AND CHANGE INPUT TABLE, ETC.

	dToday = DATE()
	*dToday = {^2011-01-01}  && to force report for January
	*dToday = {^2012-07-05}  

	lAutoYield = .T.

	cStartTime = TTOC(DATETIME())

	* deduction properties
	cDedCode = ''

	* connection properties
	nSQLHandle = 0

	* wait window properties
	nWaitWindowTimeout = 2
	lWaitWindowIsOn = .T.

	* deduction code properties
	cDedDescript = ''
	cDedCodeTable = "F:\UTIL\ADPCONNECTION\DEDCODES"

	* logfile properties
	lLoggingIsOn = .T.
	cLogFile = 'F:\UTIL\HR\DEDXREPORTS\LOGFILES\DEDXREPORT_log.txt'

	* Deds K/O/N properties
	lGetOnlyLatestPaydate = .T.

	* INTERNAL email properties
	lSendInternalEmailIsOn = .T.
	*cFrom = 'FMI Corporate <fmicorporate@fmiint.com>'
	cFrom = 'mark.bennett@tollgroup.com'
	cSendTo = ''
	cCC = 'mark.bennett@tollgroup.com'
	cSubject = ''
	cAttach = ''
	cBodyText = ''

	cCOMPUTERNAME = ''
	cUSERNAME = ''


	FUNCTION INIT
		LPARAMETERS tlTestMode
		IF NOT DODEFAULT()
			RETURN .F.
		ENDIF
		WITH THIS
			*SET RESOURCE OFF
			.lTestMode = tlTestMode
			CLOSE DATA
			SET CENTURY OFF
			SET DATE AMERICAN
			SET HOURS TO 24
			SET ANSI ON
			SET TALK OFF
			SET DELETED ON
			SET CONSOLE OFF
			SET EXCLUSIVE OFF
			SET SAFETY OFF
			SET EXACT OFF
			SET STATUS BAR OFF
			SET SYSMENU OFF
			SET ENGINEBEHAVIOR 70
			_VFP.AUTOYIELD = .lAutoYield
			.cCOMPUTERNAME = GETENV("COMPUTERNAME")
			.cUSERNAME = GETENV("USERNAME")
			IF .lTestMode THEN
				.cLogFile = 'F:\UTIL\HR\DEDXREPORTS\LOGFILES\DEDXREPORT_log_TESTMODE.txt'
				.cCC = 'mbennett@fmiint.com'
			ENDIF
			.lLoggingIsOn = .lLoggingIsOn AND NOT EMPTY(.cLogFile)
		ENDWITH
	ENDFUNC


	FUNCTION DESTROY
		WITH THIS
			IF .lLoggingIsOn  THEN
				SET ALTERNATE OFF
				SET ALTERNATE TO
			ENDIF
		ENDWITH
		DODEFAULT()
	ENDFUNC


	FUNCTION MAIN
		WITH THIS
			LOCAL lnNumberOfErrors, lcSQL, loError, lnThisMonth, lnThisYear, lnLastMonth, lnLastYear
			LOCAL ldLatestPaydate, lcSpecialWhere, lcOrderBy
			TRY
				lnNumberOfErrors = 0
				lcSpecialWhere = ""

				IF .lLoggingIsOn THEN
					SET ALTERNATE TO (.cLogFile) ADDITIVE
					SET ALTERNATE ON
				ENDIF

				.TrackProgress('ADP DEDUCTION REPORT process started....', LOGIT+SENDIT)
				.TrackProgress('COMPUTERNAME = ' + .cCOMPUTERNAME, LOGIT+SENDIT)
				.TrackProgress('USERNAME = ' + .cUSERNAME, LOGIT+SENDIT)
				.TrackProgress('PROJECT = HRDEDXREPORTS', LOGIT+SENDIT)
				IF .lTestMode THEN
					.TrackProgress('                        TEST MODE                        ', LOGIT+SENDIT)
				ENDIF
				

				IF EMPTY(.cDedCode) THEN
					.TrackProgress('ADP Deduction Report is missing the Deduction Code parameter!', LOGIT+SENDIT)
				ELSE
					* okay to proceed.
					.TrackProgress('Passed Deduction Code parameter = [' + .cDedCode + ']', LOGIT+SENDIT)

					* construct month/year values for SELECT
					lnThisMonth = MONTH(.dToday)
					lnThisYear = YEAR(.dToday)

					IF lnThisMonth > 1 THEN
						lnLastMonth = lnThisMonth - 1
						lnLastYear = lnThisYear
					ELSE
						lnLastMonth = 12
						lnLastYear = lnThisYear - 1
					ENDIF

					*OPEN DATABASE F:\UTIL\ADPCONNECTION\CONNECT1

					.nSQLHandle = SQLCONNECT("PCPAYWIN","MBENNETT","VP737A")

					IF .nSQLHandle > 0 THEN					
					
*!*							* special where works only in test mode
*!*							 IF .lTestMode THEN
*!*							 	lcSpecialWhere = " AND CHECKVIEWPAYDATE = DATE '2008-12-12' "
*!*							 ELSE
*!*							 	lcSpecialWhere = ""
*!*							 ENDIF

						DO CASE
							CASE INLIST(.cDedCode,"K","O","N")
								.cSubject = 'ADP Payroll ' + .cDedDescript + ', for Month/Year: ' + TRANSFORM(lnThisMonth) + "/" + TRANSFORM(lnThisYear)
								lcSQL = ;
									"SELECT " + ;
									" COMPANYCODE AS ADPCOMP, " + ;
									" NAME, " + ;
									" SOCIALSECURITY# AS SS_NUM, " + ;
									" CHECKVIEWPAYDATE AS PAYDATE, " + ;
									" CHECKVIEWDEDCODE AS DEDCODE, " + ;
									" CHECKVIEWDEDAMT AS DEDAMT, " + ;
									" FILE# AS FILENUM " + ;
									" FROM REPORTS.V_CHK_VW_DEDUCTION " + ;
									" WHERE CHECKVIEWDEDCODE = '" + .cDedCode + "' " + ;
									" AND {fn MONTH(CHECKVIEWPAYDATE)} = " + TRANSFORM(lnThisMonth) + ;
									" AND {fn YEAR(CHECKVIEWPAYDATE)} = " + TRANSFORM(lnThisYear) + ;
									lcSpecialWhere
							OTHERWISE
								lcSQL = ;
									"SELECT " + ;
									" COMPANYCODE AS ADPCOMP, " + ;
									" NAME, " + ;
									" SOCIALSECURITY# AS SS_NUM, " + ;
									" CHECKVIEWPAYDATE AS PAYDATE, " + ;
									" CHECKVIEWDEDCODE AS DEDCODE, " + ;
									" CHECKVIEWDEDAMT AS DEDAMT, " + ;
									" FILE# AS FILENUM " + ;
									" FROM REPORTS.V_CHK_VW_DEDUCTION " + ;
									" WHERE CHECKVIEWDEDCODE = '" + .cDedCode + "' " + ;
									" AND {fn MONTH(CHECKVIEWPAYDATE)} = " + TRANSFORM(lnLastMonth) + ;
									" AND {fn YEAR(CHECKVIEWPAYDATE)} = " + TRANSFORM(lnLastYear) + ;
									lcSpecialWhere
						ENDCASE

						IF .lTestMode THEN
							.TrackProgress('                        TEST MODE                        ', LOGIT+SENDIT)
							.TrackProgress('lcSQL =' + lcSQL, LOGIT+SENDIT)
						ENDIF

						IF USED('SQLCURSOR1') THEN
							USE IN SQLCURSOR1
						ENDIF

						IF .ExecSQL(lcSQL, 'SQLCURSOR1', RETURN_DATA_MANDATORY) THEN

							*!*								SELECT SQLCURSOR1
							*!*								BROWSE

							IF USED('SQLCURSOR2') THEN
								USE IN SQLCURSOR2
							ENDIF

							* if ded k/o/n and we want only latest paydate
							IF INLIST(.cDedCode,"K","O","N") AND .lGetOnlyLatestPaydate THEN

								* determine latest paydate and filter by that
								IF USED('CURLATESTPAYDATE') THEN
									USE IN CURLATESTPAYDATE
								ENDIF

								SELECT TTOD(MAX(PAYDATE)) AS PAYDATE ;
									FROM SQLCURSOR1 ;
									INTO CURSOR CURLATESTPAYDATE

								SELECT CURLATESTPAYDATE
								GOTO TOP
								ldLatestPaydate = CURLATESTPAYDATE.PAYDATE

								.cSubject = 'ADP Payroll ' + .cDedDescript + ', for Pay Date: ' + DTOC(ldLatestPaydate)
								lcOutputFile = "F:\UTIL\HR\DEDXREPORTS\REPORTS\ADP_" + .cDedCode + "_DEDS_" + STRTRAN(DTOC(ldLatestPaydate),"/","-") + ".XLS"

								IF USED('CURLATESTPAYDATE') THEN
									USE IN CURLATESTPAYDATE
								ENDIF

								SELECT ;
									ADPCOMP, ;
									NAME, ;
									(LEFT(SS_NUM,3) + "-" + SUBSTR(SS_NUM,4,2) + "-" + RIGHT(SS_NUM,4)) AS SS_NUM, ;
									TTOD(PAYDATE) AS PAYDATE, ;
									FILENUM, ;
									DEDCODE, ;
									DEDAMT ;
									FROM SQLCURSOR1 ;
									INTO CURSOR SQLCURSOR2 ;
									WHERE PAYDATE = ldLatestPaydate ;
									ORDER BY 1, 2, 3

							ELSE

								* default: don't filter by paydates
								lcOutputFile = "F:\UTIL\HR\DEDXREPORTS\REPORTS\ADP_" + .cDedCode + "_DEDS_" + PADL(lnLastMonth,2,"0") + "_" + ;
									TRANSFORM(lnLastYear) + ".XLS"
								
								* new orderings for Marie 2/15/2011
								lcOrderBy = "ORDER BY NAME"
								DO CASE
									CASE INLIST(.cDedCode,'D','S','T')  &&  Disability / Supp. Life / Dental
										lcOrderBy = "ORDER BY NAME"
									CASE .cDedCode = "I"  && Life Ins.
										lcOrderBy = "ORDER BY ADPCOMP, NAME"
									OTHERWISE
										* nothing
								ENDCASE

								SELECT ;
									ADPCOMP, ;
									NAME, ;
									(LEFT(SS_NUM,3) + "-" + SUBSTR(SS_NUM,4,2) + "-" + RIGHT(SS_NUM,4)) AS SS_NUM, ;
									TTOD(PAYDATE) AS PAYDATE, ;
									FILENUM, ;
									DEDCODE, ;
									DEDAMT ;
									FROM SQLCURSOR1 ;
									INTO CURSOR SQLCURSOR2 ;
									&lcOrderBy.

							ENDIF  &&  INLIST(.cDedCode,"K","O","N") AND .lGetOnlyLatestPaydate


							SELECT SQLCURSOR2
							GOTO TOP
							*BROWSE

							COPY TO (lcOutputFile) XL5
							.TrackProgress('Output to spreadsheet: ' + lcOutputFile, LOGIT+SENDIT)

							IF FILE(lcOutputFile) THEN
								* attach output file to email
								.cAttach = lcOutputFile
								.cBodyText = "See attached ADP Deductions report." + ;
									CRLF + CRLF + "(do not reply - this is an automated report)" + ;
									CRLF + CRLF + "<report log follows>" + ;
									CRLF + .cBodyText
							ELSE
								.TrackProgress('ERROR: unable to email spreadsheet: ' + lcOutputFile, LOGIT+SENDIT)
							ENDIF

							CLOSE DATABASES ALL
						ENDIF  &&  .ExecSQL(lcSQL, 'SQLCURSOR1', RETURN_DATA_MANDATORY)
						
						=SQLDISCONNECT( .nSQLHandle )

						.TrackProgress('ADP DEDUCTION REPORT process ended normally.', LOGIT+SENDIT+NOWAITIT)
					ELSE
						* connection error
						.TrackProgress('Unable to connect to ADP Payroll System.', LOGIT+SENDIT)
					ENDIF   &&  .nSQLHandle > 0
				ENDIF  && EMPTY(.cDedCode)

			CATCH TO loError

				.TrackProgress('There was an error.',LOGIT+SENDIT)
				.TrackProgress('The Error # is: ' + TRANSFORM(loError.ErrorNo), LOGIT+SENDIT)
				.TrackProgress('The Error Message is: ' + TRANSFORM(loError.MESSAGE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred in Program: ' + TRANSFORM(loError.PROCEDURE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred at line #: ' + TRANSFORM(loError.LINENO), LOGIT+SENDIT)
				lnNumberOfErrors = lnNumberOfErrors + 1
				CLOSE DATA

			ENDTRY

			CLOSE DATA
			WAIT CLEAR
			***************** INTERNAL email results ******************************
			.TrackProgress('About to send status email.',LOGIT)
			.TrackProgress('The logfile is: ' + .cLogFile, SENDIT)
			.TrackProgress('ADP DEDUCTION REPORT process started: ' + .cStartTime, LOGIT+SENDIT)
			.TrackProgress('ADP DEDUCTION REPORT process finished: ' + TTOC(DATETIME()), LOGIT+SENDIT)

			IF .lSendInternalEmailIsOn THEN
				DO FORM dartmail2 WITH .cSendTo,.cFrom,.cSubject,.cCC,.cAttach,.cBodyText,"A"
				.TrackProgress('Sent status email.',LOGIT)
			ELSE
				.TrackProgress('Did not send status email: THIS.lSendInternalEmailIsOn = FALSE.',LOGIT)
			ENDIF
			
			* blank this out to support repeated runs...
			.cBodyText = ""

		ENDWITH
		RETURN
	ENDFUNC && main


	FUNCTION ExecSQL
		LPARAMETERS tcSQL, tcCursorName, tlNoDataReturnedIsOkay
		LOCAL llRetval, lnResult
		WITH THIS
			* close target cursor if it's open
			IF USED(tcCursorName)
				USE IN (tcCursorName)
			ENDIF
			WAIT WINDOW "Fetching remote data; please wait..." NOWAIT
			lnResult = sqlexec(.nSQLHandle, tcSQL, tcCursorName)
			llRetval = ( lnResult > 0 )
			IF llRetval THEN
				* see if any data came back
				IF NOT tlNoDataReturnedIsOkay THEN
					IF (NOT USED(tcCursorName)) OR EOF(tcCursorName) THEN
						llRetval = .F.
						.TrackProgress('No data was returned by this query: ' +CRLF + tcSQL, LOGIT+SENDIT)
						.cSendTo = 'mbennett@fmiint.com'
					ENDIF
				ENDIF
			ELSE
				.TrackProgress("Result Code [" + TRANSFORM(lnResult) + "] returned by this query: " + tcSQL, LOGIT+SENDIT)
				.cSendTo = 'mbennett@fmiint.com'
			ENDIF
			WAIT CLEAR
			RETURN llRetval
		ENDWITH
	ENDFUNC


	PROCEDURE TrackProgress
		* do any combination of Wait Window, writing to logfile, and adding to body of email,
		* based on nFlags parameter.
		LPARAMETERS tcExpression, tnFlags
		WITH THIS
			IF BITAND(tnFlags,LOGIT) = LOGIT THEN
				IF .lLoggingIsOn THEN
					?
					? .cProcessName + "  " + TTOC(DATETIME()) + ": " + tcExpression
				ENDIF
			ENDIF
			IF .lWaitWindowIsOn THEN
				IF BITAND(tnFlags,NOWAITIT) = NOWAITIT THEN
					WAIT WINDOW tcExpression NOWAIT
				ENDIF
				IF BITAND(tnFlags,WAITIT) = WAITIT THEN
					WAIT WINDOW tcExpression TIMEOUT .nWaitWindowTimeout
				ENDIF
			ENDIF
			IF BITAND(tnFlags,SENDIT) = SENDIT THEN
				IF .lSendInternalEmailIsOn THEN
					.cBodyText = .cBodyText + tcExpression + CRLF + CRLF
				ENDIF
			ENDIF
		ENDWITH
	ENDPROC  &&  TrackProgress


	PROCEDURE SetDedCode
		LPARAMETERS tcDedCode
		WITH THIS
			IF TYPE('tcDedCode') = 'C' THEN
				.cDedCode = UPPER(ALLTRIM(tcDedCode))
				* change 'send to' based on deduction
				DO CASE
					CASE .lTestMode
						.cSendTo = 'mbennett@fmiint.com'
					CASE INLIST(.cDedCode,'D','I','S','T')  &&  Disability / Life Insurance / Supp. Life / Dental
						* Insurance
						.cSendTo = 'lucille.waldrip@tollgroup.com, Marie.Freiberger@Tollgroup.com, Lauren.Klaver@tollgroup.com'
					CASE INLIST(.cDedCode,'L')
						* ?
						.cSendTo = 'kkausner@fmiint.com'
					CASE INLIST(.cDedCode,'K','O','N')
						* 401K DEDUCTION & LOANS
						.cSendTo = 'george.gereis@tollgroup.com'
					OTHERWISE
						.cSendTo = 'lucille.waldrip@tollgroup.com, Marie.Freiberger@Tollgroup.com, Lauren.Klaver@tollgroup.com'
				ENDCASE
				IF NOT USED('DED_DESCRIPTS') THEN
					* open deduction codes table now
					USE (.cDedCodeTable) AGAIN IN 0 ALIAS DED_DESCRIPTS
				ENDIF
				* look up Deduction Description based on code
				SELECT DED_DESCRIPTS
				LOCATE FOR DEDCODE = .cDedCode
				IF FOUND() THEN
					.cDedDescript = "Deduction " + .cDedCode + " (" + ALLTRIM(DED_DESCRIPTS.DESCRIPT) + ")"
				ELSE
					.cDedDescript = "Deduction " + .cDedCode
				ENDIF
				.cSubject = 'ADP Payroll ' + .cDedDescript + ', for month prior to: ' + DTOC(.dToday)
			ELSE
				.TrackProgress('ERROR: Invalid or empty Deduction Code parameter in SetDedCode()!', LOGIT+SENDIT)
				.cSubject = 'ADP Payroll Deduction ?, Results for: ' + TTOC(DATETIME())
			ENDIF
		ENDWITH
	ENDPROC

ENDDEFINE

