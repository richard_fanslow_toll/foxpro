* This program extractsO/B shipping OS&D's for pre-invoiced MJ customers from previous day
utilsetup("MJ_OB_OSND")
SET EXCLUSIVE OFF 
SET DELETED ON 
SET TALK OFF  
SET SAFETY OFF   &&Allows for overwrite of existing files

close databases all

xsqlexec("select * from outship where mod='J' and inlist(accountid,6303,6325) and del_date>{"+dtoc(date())+"}",,,"wh")
xsqlexec("select * from outdet where mod='J' and inlist(accountid,6303,6325) and updateby>{"+dtoc(date())+"}",,,"wh")

If !Used("preinvacct")
  Use F:\whj\whdata\preinvacct.DBF In 0 
ENDIF

*Wait window at 10,10 "Performing the first query...." nowait

SELECT outd.accountid as account, consignee as customer,outs.cacctnum as account_number, ship_ref as pickticket, delenterdt as delivery_date,style, color, id as size, ;
  outd.origqty-outd.totqty as cut_qty;
  FROM outship as outs, outdet as outd , preinvacct WHERE outs.outshipid=outd.outshipid AND;
  outs.cacctnum=preinvacct.cacctnum AND INLIST(outs.accountid,6303,6325);
 AND Delenterdt>=(date()-1) and delenterdt<date() and outd.totqty != outd.origqty INTO CURSOR obosnd1 readwrite

xsqlexec("select * from info where inlist(accountid,6303,6325) and mod='J' and message='Style deleted on pick ticket screen'",,,"wh")

SELECT outship.accountid as account, outship.consignee as customer, outship.cacctnum as account_number, ;
  outship.ship_ref as pickticket, outship.delenterdt as delivery_date, info.style, info.color, info.id as size, ;
  info.qty as cut_qty;
  FROM outship , info, preinvacct WHERE  info.ship_ref=outship.ship_ref AND outship.cacctnum=preinvacct.cacctnum;
   AND INLIST(outship.accountid,6303,6325) ;
 AND Delenterdt>=(date()-1) and delenterdt<date()  INTO CURSOR obosnd2 readwrite
 SET STEP ON 
scan
	SCATTER MEMVAR 
        INSERT INTO obosnd1 FROM memvar
endscan
set step on 

SELECT obosnd1
 
	If Reccount() > 0 
		export TO "S:\MarcJacobsData\Reports\InventorySyncFiles\OB_OSnD\pre_invoice_osnd_" +TTOC (DATE()-1, 0)  TYPE xls
		tsendto = "tmarg@fmiint.com"
		tattach = "" 
		tcc =""                                                                                                                               
		tFrom ="Toll EDI System Operations <fmi-transload-ops@fmiint.com>"                                
		tmessage = "OB OS&D COMPLETE FOR:  "+Ttoc(Datetime())        
		tSubject = "OB OS&D COMPLETE"                                                           
		Do Form m:\dev\frm\dartmail2 With tsendto,tFrom,tSubject, tcc,tattach,tmessage  ,"A"     
			
	ELSE 
		export TO "S:\MarcJacobsData\Reports\InventorySyncFiles\OB_OSnD\NO_pre_invoice_osnd_exist_" + TTOC(DATE()-1, 0)  TYPE xls
		tsendto = "tmarg@fmiint.com"
		tattach = "" 
		tcc =""                                                                                                                               
		tFrom ="Toll EDI System Operations <fmi-transload-ops@fmiint.com>"                                
		tmessage = "OB OS&D COMPLETE FOR:  "+Ttoc(Datetime())        
		tSubject = "OB OS&D COMPLETE"                                                           
		Do Form m:\dev\frm\dartmail2 With tsendto,tFrom,tSubject, tcc,tattach,tmessage  ,"A"   

	ENDIF
close data all 

Wait window at 10,10 "all done...." timeout 2


schedupdate()
_screen.Caption=gscreencaption
on error

