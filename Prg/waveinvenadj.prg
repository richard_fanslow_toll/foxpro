lparameters xpack, xwhseloc, xinadjchange, xpartial

m.partial=xpartial

if seek(str(m.accountid,4)+trans(m.units)+m.style+m.color+m.id+xpack,"inven","match")
	replace totqty with inven.totqty+xinadjchange, availqty with inven.totqty-inven.allocqty-inven.holdqty in inven
else
	m.pack=xpack
	m.totqty=xinadjchange
	m.availqty=m.totqty
	m.date_rcvd=qdate()
	insertinto("inven","wh",.t.)
	m.invenid=inven.invenid
endif

if seek(str(m.accountid,4)+trans(m.units)+m.style+m.color+m.id+xpack+xwhseloc,"invenloc","matchwh")
	xholdreturn = (invenloc.hold and invenloc.whseloc#"PSTAT")
	replace locqty with invenloc.locqty+xinadjchange in invenloc
	if invenloc.hold
		replace holdqty with inven.holdqty+xinadjchange, availqty with inven.totqty-inven.allocqty-inven.holdqty in inven
		pnphold(xinadjchange)
	endif
	if invenloc.locqty=0
		delete in invenloc
	endif
else
	select inven
	scatter memvar
	m.pack=xpack
	m.locqty=xinadjchange
	m.whseloc=xwhseloc
	m.invenid=inven.invenid
	insertinto("invenloc","wh",.t.)
	
	if holdloc(xwhseloc)
		replace hold with .t., holdtype with gholdtype in invenloc
		replace holdqty with inven.holdqty+m.locqty, availqty with inven.totqty-inven.allocqty-inven.holdqty in inven
		pnphold(m.locqty)
	endif
endif		
