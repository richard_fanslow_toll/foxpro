*******************************************************************************************************
* Process ML Auto pm data to produce a prior month summary of handling charges.
* Meant to be run for Sears after the Sears final monthly invoice has been produced.
*
* EXE = F:\UTIL\MLAUTOBILLING\PRIORMONTHSUMMARY.EXE
*******************************************************************************************************
LPARAMETERS tcCLIENT
LOCAL loSearsProc, ldToday, ldFrom, ldTo, llGenerateInvoice, llUsePrepackForManualUnitCounting, lcClient

TRY
	lcClient = UPPER(ALLTRIM(tcCLIENT))

	ldToday = DATE()

	ldFrom = GOMONTH((ldToday - DAY(ldToday) + 1), -1) && 1st of prior month
	ldTo = ldToday - DAY(ldToday) && last day of prior month
	* parameters to set in class
	llGenerateInvoice = .F.
	llUsePrepackForManualUnitCounting = .T.

	* load main processing class
	SET PROCEDURE TO M:\DEV\PRG\SEARSCLASS
	loSearsProc = CREATEOBJECT('SEARS')
	loSearsProc.SetSummaryOnly() && this forces a summary Excel sheet to be generated, not the pdf billing
	loSearsProc.SetUsePrepackForManualUnitCountingMode( llUsePrepackForManualUnitCounting )
	IF loSearsProc.SetClient(lcClient) THEN
		loSearsProc.MakeBillForWeek(ldFrom, ldTo, llGenerateInvoice)
	ENDIF
CATCH
ENDTRY
CLOSE DATABASES ALL

RETURN
