* KRONOS Punch Detail Report.  So Temp Agencies can see the punches, sorted by division/dept without requiring multiple huge Kronos Time Card Detail Reports.
* MB 08/07/2017
*
* author: Mark Bennett
*
* exe = F:\UTIL\KRONOS\KRONOSPUNCHDETAILRPT.EXE
*
* Modified 11/1/2017 to calc lunch duration in minutes at Chartwell's request
*
* Modified 1/3/2018 MB to add Chartwell NJ and changes agencies U and V from SSG to Simplifed Labor SS.
*
* modified 2/2/2018 MB to add Chartwell KY as agency F.
*
*2/28/2018 MB:  added Date Range mode to run reports for Stacey in Legal.
*
* 3/20/2018 MB: Added support for agency codes F and Z
*
* 4/24/201 MB: changed fmiint.com references to my toll email.

LPARAMETERS tcTempCo, tcMode

LOCAL lcTempCo, lcMode

IF TYPE('tcTempCo') = "L" THEN
	tcTempCo = "?"
ENDIF 

IF TYPE('tcMode') = "L" THEN
	tcMode = "?"
ENDIF

ON ERROR DO errHandlerKTTR WITH ERROR( ), MESSAGE( ), MESSAGE(1), PROGRAM( ), LINENO( ), tcTempCo, tcMode

runack("KRONOSPUNCHDETAILRPT")

LOCAL lnError, lcProcessName

DECLARE INTEGER CreateMutex IN Win32API ;
	STRING @lpMutexAttributes, ;
	STRING bInitialOwner, ;
	STRING @lpName

DECLARE INTEGER GetLastError IN Win32API

* if running in EXE check to see if already running
IF _VFP.STARTMODE > 0 THEN
	lcProcessName = "KRONOSPUNCHDETAILRPT"
	CreateMutex(0,CHR(1),@lcProcessName)
	lnError = GetLastError()
	IF lnError = 183
		WAIT WINDOW TIMEOUT 10 "KRONOS REPORT process is already running..."
		RETURN .F.
	ENDIF
ENDIF

SET PROCEDURE TO M:\DEV\PRG\KRONOSCLASSLIBRARY ADDITIVE

_SCREEN.CAPTION = "KRONOSPUNCHDETAILRPT"

utilsetup("KRONOSPUNCHDETAILRPT")

LOCAL loKRONOSPUNCHDETAILRPT, lcTempCo

loKRONOSPUNCHDETAILRPT = CREATEOBJECT('KRONOSPUNCHDETAILRPT')

lcTempCo = UPPER(ALLTRIM(tcTempCo))

lcMode = UPPER(ALLTRIM(tcMode))

loKRONOSPUNCHDETAILRPT.MAIN( lcTempCo, lcMode )


schedupdate()

CLOSE DATABASES ALL

WAIT WINDOW TIMEOUT 60  && TO FORCE THIS PROCESS TO RUN FOR > 1 MINUTE, I THINK IF IT RUNS WITHIN 1 MINUTE STM RUNS IT TWICE

RETURN

#DEFINE LOGIT 1
#DEFINE WAITIT 2
#DEFINE NOWAITIT 4
#DEFINE SENDIT 8
#DEFINE NOCRLF 16
#DEFINE CRLF CHR(13) + CHR(10)
#DEFINE LF CHR(10)
#DEFINE RETURN_DATA_MANDATORY .F.
#DEFINE RETURN_DATA_NOT_MANDATORY .T.
#DEFINE DAILY_OT_MULTIPLIER 1.5
#DEFINE DAILY_OT20_MULTIPLIER 2.0
#DEFINE xlEdgeBottom 9
#DEFINE xlContinuous 1
#DEFINE xlMedium -4138
#DEFINE TAB CHR(9)

DEFINE CLASS KRONOSPUNCHDETAILRPT AS CUSTOM

	cProcessName = 'KRONOSPUNCHDETAILRPT'

	lTestMode = .F.  && .F. FOR NORMAL OPERATION, .T. TO SUPPRESS ALL BUT DEVELOPER EMAILS AND CHANGE INPUT TABLE, ETC.

	lAutoYield = .T.

	* date properties
	dtNow = DATETIME()

	dToday = DATE()
	*dToday = {^2017-12-03}

	cStartTime = TTOC(DATETIME())

	* connection properties
	nSQLHandle = 0
	oExcel = NULL

	* sql/report filter properties
	cTempCoWhere = ""
	cTempCoWhere2 = ""
	cTempCo = ""
	cTempCoName = ""
	cMode = ""
	lCalcLunch = .F.

	nPeriodEndDayNum = 1   && if 7, weekly pay period ends on a Saturday (Sunday - Saturday), if 1, it ends on a Sunday (Monday - Sunday)
	oTempAgency = NULL


	cToday = DTOC(DATE())

	* wait window properties
	nWaitWindowTimeout = 2
	lWaitWindowIsOn = .T.

	* logfile properties
	lLoggingIsOn = .T.
	cLogFile = 'F:\UTIL\KRONOS\Logfiles\KRONOSPUNCHDETAILRPT_log.txt'

	* INTERNAL email properties
	lSendInternalEmailIsOn = .T.
	cFrom = 'mark.bennett@tollgroup.com'
	cSendTo = 'mark.bennett@tollgroup.com'
	cCC = ''
	cSubject = 'Kronos Punch Detail Report'
	cAttach = ''
	cBodyText = ''
	cTopBodyText = ''

	cCOMPUTERNAME = ''
	cUSERNAME = ''

	FUNCTION INIT
		IF NOT DODEFAULT()
			RETURN .F.
		ENDIF
		WITH THIS
			CLEAR
			*SET RESOURCE OFF
			CLOSE DATA
			SET CENTURY ON
			SET DATE YMD
			SET HOURS TO 24
			SET ANSI ON
			SET TALK OFF
			SET DELETED ON
			SET CONSOLE OFF
			SET EXCLUSIVE OFF
			SET SAFETY OFF
			SET EXACT OFF
			SET STATUS BAR ON
			SET SYSMENU OFF
			SET ENGINEBEHAVIOR 70
			SET DECIMALS TO 0

			SET FIXED ON

			_VFP.AUTOYIELD = .lAutoYield
			.cCOMPUTERNAME = GETENV("COMPUTERNAME")
			.cUSERNAME = GETENV("USERNAME")
		ENDWITH
	ENDFUNC


	FUNCTION DESTROY
		WITH THIS
			IF .lLoggingIsOn  THEN
				SET ALTERNATE OFF
				SET ALTERNATE TO
			ENDIF
			IF TYPE('.oExcel') = 'O' AND NOT ISNULL(.oExcel) THEN
				.oExcel.QUIT()
				.oExcel = NULL
			ENDIF
		ENDWITH
		DODEFAULT()
	ENDFUNC


	FUNCTION MAIN
		LPARAMETERS tcTempCo, tcMode
		WITH THIS
			LOCAL lnNumberOfErrors, lcSQL, lcSQL2, lcSQL3, lcSQL4, lcSQL5, lcSQL6, lcSQL7, loError, loWorkbook, loWorksheet, lcType
			LOCAL lcOutputFile, ldStartDate, ldEndDate, lcSQLStartDate, lcSQLEndDate, ldDate, lcTempCoWhere, lcTempCo, lcMode, lcDateRange, lnRow, lcRow
			LOCAL lnPrevRow, lcPrevRow, lcName, lcPrevName, lnLunchMinutes, ltLunchInDateTime, ltLunchOutDateTime, lcLunchInDateTime, lcLunchOutDateTime

			ON ERROR

			TRY
				lnNumberOfErrors = 0

				*.TrackProgress('==================================================================================================================', LOGIT+SENDIT)
				.TrackProgress('Kronos Punch Detail process started....', LOGIT+SENDIT)
				.TrackProgress('COMPUTERNAME = ' + .cCOMPUTERNAME, LOGIT+SENDIT)
				.TrackProgress('USERNAME = ' + .cUSERNAME, LOGIT+SENDIT)
				.TrackProgress('PROJECT = KRONOSPUNCHDETAILRPT', LOGIT+SENDIT)
				.TrackProgress('Temp Co = ' + tcTempCo, LOGIT+SENDIT)
				.TrackProgress('Mode = ' + tcMode, LOGIT+SENDIT)
				IF .lTestMode THEN
					.TrackProgress('====>TEST MODE', LOGIT+SENDIT)
				ENDIF
				
				IF NOT INLIST(tcMode,'DATERANGE','YESTERDAY','PRIORWEEK','WEEK-TO-DATE') THEN
					.TrackProgress('Missing or Invalid MODE Parameter!', LOGIT+SENDIT)
					THROW
				ENDIF
				
*!*					* set temp agency from parameter
*!*					.SetTempCo( tcTempCo )
				
				.oTempAgency = CREATEOBJECT('TempAgency',tcTempCo)				
				lcType = TYPE('.oTempAgency')
				
				IF lcType == 'O' THEN
					.cTempCo = tcTempCo
					.cTempCoName = .oTempAgency.GetTempCoName()
					.nPeriodEndDayNum = .oTempAgency.nPeriodEndDayNum
				ELSE
					.TrackProgress('====> ERROR instantiating Temp Agency class!', LOGIT+SENDIT)
					THROW
				ENDIF

				IF EMPTY(.cTempCoName) THEN
					.TrackProgress('Missing or Invalid Temp CO Parameter!', LOGIT+SENDIT)
					THROW
				ENDIF

				* make logfiles company-specific so that report run times can overlap
				IF .lTestMode THEN
					.cLogFile = 'F:\UTIL\KRONOS\Logfiles\KRONOSPUNCHDETAILRPT_' + .cTempCoName + "_"  + .cCOMPUTERNAME + '_log_TESTMODE.txt'
				ELSE
					.cLogFile = 'F:\UTIL\KRONOS\Logfiles\KRONOSPUNCHDETAILRPT_' + + .cTempCoName + "_" + .cCOMPUTERNAME + '_log.txt'
				ENDIF
				.lLoggingIsOn = .lLoggingIsOn AND NOT EMPTY(.cLogFile)
				IF .lLoggingIsOn THEN
					SET ALTERNATE TO (.cLogFile) ADDITIVE
					SET ALTERNATE ON
				ENDIF
				

				ldToday = .dToday
				.cToday = DTOC(.dToday)
				
				
				DO CASE
					CASE tcMode == 'DATERANGE'
					
						ldStartDate = {^2017-09-01}
						*ldEndDate = GOMONTH(ldStartDate,1) - 1
						ldEndDate = {^2017-09-15}
						.cSubject = 'Punch Report ' + .cTempCoName + ' for ' + DTOC(ldStartDate) + " - " + DTOC(ldEndDate)
						lcDateRange = DTOC(ldStartDate) + " - " + DTOC(ldEndDate)
						
					CASE tcMode == 'YESTERDAY'
					
						ldStartDate = ldToday -1
						ldEndDate = ldToday -1
						.cSubject = 'Punch Report ' + PROPER(tcMode) + ' ' + .cTempCoName + ' for ' + DTOC(ldStartDate)
						lcDateRange = DTOC(ldStartDate)
						
					CASE tcMode == 'PRIORWEEK'
					
						ldDate = .dToday

						DO WHILE DOW(ldDate,1) <> .nPeriodEndDayNum
							ldDate = ldDate - 1
						ENDDO
						ldEndDate = ldDate
						ldStartDate = ldDate - 6
						
						.cSubject = 'Punch Report ' + PROPER(tcMode) + ' ' + .cTempCoName + ' for ' + DTOC(ldStartDate) + " - " + DTOC(ldEndDate)
						lcDateRange = DTOC(ldStartDate) + " - " + DTOC(ldEndDate)
					
					CASE tcMode == 'WEEK-TO-DATE'
					
						ldDate = .dToday

						DO WHILE DOW(ldDate,1) <> .nPeriodEndDayNum
							ldDate = ldDate - 1
						ENDDO
						ldStartDate = ldDate + 1
						ldEndDate = ldToday -1
						
						.cSubject = 'Punch Report ' + PROPER(tcMode) + ' ' + .cTempCoName + ' for ' + DTOC(ldStartDate) + " - " + DTOC(ldEndDate)
						lcDateRange = DTOC(ldStartDate) + " - " + DTOC(ldEndDate)
					
					OTHERWISE
						* nothing				
				ENDCASE
				
				lcSQLStartDate = "'" + STRTRAN(TRANSFORM(ldStartDate),"/","") + "'"
				lcSQLEndDate = "'" + STRTRAN(TRANSFORM(ldEndDate),"/","") + "'"

				IF USED('CURPUNCHES') THEN
					USE IN CURPUNCHES
				ENDIF

				IF USED('CURPUNCHESPRE') THEN
					USE IN CURPUNCHESPRE
				ENDIF

				* define filters / emails by passed tempco and mode parameters
				DO CASE
					CASE .cTempCo == 'D'
						.cSendTo = 'sueoqualitytemps@outlook.com, toll@chartwellstaff.com'
						.cCC = 'mark.bennett@tollgroup.com'
					CASE .cTempCo == 'F'
						.cSendTo = 'toll@chartwellstaff.com, cheri.foster@tollgroup.com, dwayne.sutton@tollgroup.com, robin.ragg@tollgroup.com'
						.cCC = 'mark.bennett@tollgroup.com'
					CASE .cTempCo == 'G'
						.cSendTo = 'toll@chartwellstaff.com'
						.cCC = 'mark.bennett@tollgroup.com'
					CASE .cTempCo == 'Q'
						.cSendTo = 'cheri.foster@tollgroup.com, dwayne.sutton@tollgroup.com, robin.ragg@tollgroup.com, mtorres@cor-tech.net, awhittle@cor-tech.net, ealvarez@cor-tech.net, rlayton@cor-tech.net'
						.cCC = 'mark.bennett@tollgroup.com'
					CASE .cTempCo == 'U' 
						*.cSendTo = 'malvarez@thessg.com, jcano@thessg.com, jpreciado@thessg.com, operea@thessg.com'
						.cSendTo = 	'marisol.monreal@yourstaffingfirm.com, martha.diaz@yourstaffingfirm.com, Sandra.Senbol@yourstaffingfirm.com, maria.sanchez@yourstaffingfirm.com, Cynthia.Stopani@yourstaffingfirm.com, crystal.hernandez@yourstaffingfirm.com '
						.cCC = 'mark.bennett@tollgroup.com'
					CASE .cTempCo == 'V'   
						*.cSendTo = 'malvarez@thessg.com, jcano@thessg.com, jpreciado@thessg.com, operea@thessg.com'
						.cSendTo = 	'marisol.monreal@yourstaffingfirm.com, martha.diaz@yourstaffingfirm.com, Sandra.Senbol@yourstaffingfirm.com, maria.sanchez@yourstaffingfirm.com, Cynthia.Stopani@yourstaffingfirm.com, crystal.hernandez@yourstaffingfirm.com '
						.cCC = 'mark.bennett@tollgroup.com'
					CASE .cTempCo == 'X'   && Chartwell
						.cSendTo = 'toll@chartwellstaff.com'
						.cCC = 'mark.bennett@tollgroup.com'
						.lCalcLunch = .T.
					CASE .cTempCo == 'Y'   && Simplified
						.cSendTo = 'Sandra.Senbol@yourstaffingfirm.com, Seandy.Reyes@yourstaffingfirm.com, maria.sanchez@yourstaffingfirm.com, juan.santisteban@yourstaffingfirm.com, Cynthia.Stopani@yourstaffingfirm.com'
						.cCC = 'mark.bennett@tollgroup.com'
					CASE .cTempCo == 'Z'
						.cSendTo = 'cheri.foster@tollgroup.com, dwayne.sutton@tollgroup.com, robin.ragg@tollgroup.com'
						.cCC = 'mark.bennett@tollgroup.com'
					OTHERWISE
						* NOTHING  
				ENDCASE
				
				IF .lTestMode THEN
					.cSendTo = 'mark.bennett@tollgroup.com'
					.cCC = ""
					.lCalcLunch = .T.
				ENDIF
				
				.cTempCoWhere =  " AND (LABORLEVELNAME1 = 'TMP') AND (LABORLEVELNAME5 = '" + .cTempCo + "') "

				lcTempCoWhere = .cTempCoWhere

				SET TEXTMERGE ON

				TEXT TO	lcSQL NOSHOW
				
SELECT
PERSONFULLNAME AS NAME,
LABORLEVELNAME2 AS DIVISION,
LABORLEVELNAME3 AS DEPT,
INPUNCHDTM,
OUTPUNCHDTM
FROM VP_TIMESHTPUNCHV42
WHERE (EVENTDATE >= <<lcSQLStartDate>>)
AND (EVENTDATE <= <<lcSQLEndDate>>)
<<lcTempCoWhere>>
ORDER BY EVENTDATE, PERSONFULLNAME, INPUNCHDTM

				ENDTEXT

				SET TEXTMERGE OFF

				*!*	AND LABORLEVELNAME1 = 'TMP'
				*!*

				IF .lTestMode THEN
					.TrackProgress('', LOGIT+SENDIT)
					.TrackProgress('lcSQL =' + lcSQL, LOGIT+SENDIT)
				ENDIF

				.nSQLHandle = SQLCONNECT("WTK2","sa","kr0n1tes")

				IF .nSQLHandle > 0 THEN

					IF .ExecSQL(lcSQL, 'CURPUNCHESPRE', RETURN_DATA_NOT_MANDATORY) THEN

						IF USED('CURPUNCHESPRE') AND NOT EOF('CURPUNCHESPRE') THEN
						
							* blank out 1/1/1900 dates which really means null date
							SELECT CURPUNCHESPRE
							SCAN
								IF ISNULL(CURPUNCHESPRE.INPUNCHDTM) OR (YEAR(CURPUNCHESPRE.INPUNCHDTM) = 1900) THEN
									REPLACE CURPUNCHESPRE.INPUNCHDTM WITH {} IN CURPUNCHESPRE
								ENDIF
								IF ISNULL(CURPUNCHESPRE.OUTPUNCHDTM) OR (YEAR(CURPUNCHESPRE.OUTPUNCHDTM) = 1900) THEN
									REPLACE CURPUNCHESPRE.OUTPUNCHDTM WITH {} IN CURPUNCHESPRE
								ENDIF
							ENDSCAN
						
							* OPEN Excel
							.oExcel = CREATEOBJECT("excel.application")
							.oExcel.displayalerts = .F.
							.oExcel.VISIBLE = .F.

							lcOutputFile = "F:\UTIL\Kronos\TEMPTIMEREPORTS\PunchDetailRpt_" + .cTempCo + "_" + DTOS(ldStartDate) + "-" + DTOS(ldEndDate) + ".XLSX"
							
							lcTemplateFile = 'F:\UTIL\KRONOS\TEMPLATES\KronosPunchDetailRpt-template02.XLSX'

							loWorkbook = .oExcel.workbooks.OPEN(lcTemplateFile)
							loWorkbook.SAVEAS(lcOutputFile,51)  && 51 = xlsx file format
							loWorksheet = loWorkbook.worksheets[1]
							*loWorksheet.SELECT()
							*SET STEP ON 
							
							loWorksheet.RANGE("A1").VALUE = 'Kronos Punch Detail Report - ' + .cTempCoName + ' for ' + lcDateRange
														
							lnRow = 3							
							SELECT CURPUNCHESPRE
							SCAN
								lnPrevRow = lnRow
								lcPrevRow = LTRIM(STR(lnPrevRow))
								lnRow = lnRow + 1
								lcRow = LTRIM(STR(lnRow))
								loWorksheet.RANGE("A" + lcRow).VALUE = CURPUNCHESPRE.NAME
								loWorksheet.RANGE("B" + lcRow).VALUE = CURPUNCHESPRE.DIVISION
								loWorksheet.RANGE("C" + lcRow).VALUE = CURPUNCHESPRE.DEPT
								
								loWorksheet.RANGE("D" + lcRow).VALUE = TTOC(CURPUNCHESPRE.INPUNCHDTM)
								
								IF EMPTY(CURPUNCHESPRE.INPUNCHDTM) THEN
									loWorksheet.RANGE("D" + lcRow).Interior.ColorIndex = 3
								ENDIF
								
								loWorksheet.RANGE("E" + lcRow).VALUE = TTOC(CURPUNCHESPRE.OUTPUNCHDTM)
								
								IF EMPTY(CURPUNCHESPRE.OUTPUNCHDTM) THEN
									loWorksheet.RANGE("E" + lcRow).Interior.ColorIndex = 3
								ENDIF
								
								IF (NOT .lCalcLunch) THEN
									* blank out lunch minutes column headers
									loWorksheet.RANGE("F2").VALUE = ''
									loWorksheet.RANGE("F3").VALUE = ''
								ELSE								
									* if we are on the second OF 2 rows for an associate (i.e., current name = previous name) then calculate the lunch duration - lnLunchMinutes
									lcName = UPPER(ALLTRIM(loWorksheet.RANGE("A" + lcRow).VALUE))
									lcPrevName = UPPER(ALLTRIM(loWorksheet.RANGE("A" + lcPrevRow).VALUE))
									IF (lcName == lcPrevName) THEN
									 
										* we _are_ on the second OF 2 rows for an associate
										* lunch out punch is the Out punch from the previous row
										ltLunchOutDateTime = NVL(loWorksheet.RANGE("E" + lcPrevRow).VALUE,'')
										* lunch in punch is the In punch from the current row
										ltLunchInDateTime = NVL(loWorksheet.RANGE("D" + lcRow).VALUE,'')
										
										IF (TYPE('ltLunchOutDateTime') <> 'T') OR (TYPE('ltLunchInDateTime') <> 'T') THEN
											.TrackProgress('=====> ERROR: Lunch calc not possible for ' + loWorksheet.RANGE("A" + lcRow).VALUE, LOGIT+SENDIT)
										ELSE
											* the lunch times should be valid for the minutes calculation
											
											* only proceed with the calculation if both times are for the same date.
											* this is needed for the weekly report, where there are many more than 2 rows per associate -BUT NOT NEEDED IF WE REORDER THE WEEKLY REPORT
											
											*IF (TTOD(ltLunchOutDateTime) = TTOD(ltLunchInDateTime)) THEN
												* both times are for the same date...continue
																						
												IF (NOT EMPTY(ltLunchOutDateTime)) AND (NOT EMPTY(ltLunchInDateTime)) THEN
													* Both date strings had values - calc difference									
													lnLunchMinutes = (ltLunchInDateTime - ltLunchOutDateTime) / 60
													loWorksheet.RANGE("F" + lcRow).VALUE = lnLunchMinutes
													IF NOT .ltestMode THEN
														IF (lnLunchMinutes < 38.0) THEN
															* HIGHLIGHT because their lunch break was not long enough
															loWorksheet.RANGE("F" + lcRow).Interior.ColorIndex = 3
														ENDIF
													ENDIF
												ELSE
													* HIGHLIGHT where minutes would have been because we could not calculate them due to a null datetime
													loWorksheet.RANGE("F" + lcRow).Interior.ColorIndex = 3
												ENDIF && (NOT EMPTY(lcLunchOutDateTime)) AND (NOT EMPTY(lcLunchInDateTime))
												
											*ENDIF && (TTOD(ltLunchOutDateTime) = TTOD(ltLunchInDateTime))
											
										ENDIF && (NOT EMPTY(lcLunchOutDateTime)) AND (NOT EMPTY(lcLunchInDateTime))
									
									ENDIF  && (lcName == lcPrevName)

								ENDIF && (NOT .lCalcLunch) THEN
																
							ENDSCAN

							loWorkbook.SAVE()	
							
							.oExcel.Quit()							

							.cAttach = lcOutputFile
							
							.cTopBodyText = "See attached punch detail report. Last row = " + lcRow

						ELSE
							.cTopBodyText = "There is no punch detail to report."
						ENDIF


					ENDIF  &&  .ExecSQL(lcSQL

				ELSE
					* connection error
					.TrackProgress('Unable to connect to WTK System.', LOGIT+SENDIT)
				ENDIF   &&  .nSQLHandle > 0

				.TrackProgress('Kronos Punch Detail process ended normally.', LOGIT+SENDIT+NOWAITIT)
				
				.cBodyText = .cTopBodyText + CRLF+ CRLF + "<<report log follows>>" + CRLF + CRLF + .cBodyText


			CATCH TO loError

				.TrackProgress('There was an error.',LOGIT+SENDIT)
				.TrackProgress('The Error # is: ' + TRANSFORM(loError.ERRORNO), LOGIT+SENDIT)
				.TrackProgress('The Error Message is: ' + TRANSFORM(loError.MESSAGE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred in Program: ' + TRANSFORM(loError.PROCEDURE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred at line #: ' + TRANSFORM(loError.LINENO), LOGIT+SENDIT)
				lnNumberOfErrors = lnNumberOfErrors + 1
				IF TYPE('.oExcel') = 'O' AND NOT ISNULL(.oExcel) THEN
					.oExcel.QUIT()
					.oExcel = NULL
				ENDIF
				*CLOSE ALL

			ENDTRY

			CLOSE DATA
			WAIT CLEAR
			***************** INTERNAL email results ******************************
			.TrackProgress('About to send status email.',LOGIT)
			.TrackProgress('==================================================================================================================', SENDIT)
			.TrackProgress('The logfile is: ' + .cLogFile, SENDIT)
			.TrackProgress('Kronos Punch Detail process started: ' + .cStartTime, LOGIT+SENDIT)
			.TrackProgress('Kronos Punch Detail process finished: ' + TTOC(DATETIME()), LOGIT+SENDIT)

			IF .lSendInternalEmailIsOn THEN
				* try to trap error from not having dartmail dll's registered on user's pc...
				TRY
					DO FORM dartmail2 WITH .cSendTo,.cFrom,.cSubject,.cCC,.cAttach,.cBodyText,"A"
					.TrackProgress('Sent status email.',LOGIT)
				CATCH TO loError
					*=MESSAGEBOX('There was an error sending the status email. Please contact Mark Bennett.',0+64,"KRONOS Temp Time")
					.TrackProgress('There was an error sending the status email.',LOGIT)
					.TrackProgress('The Error # is: ' + TRANSFORM(loError.ERRORNO), LOGIT)
					.TrackProgress('The Error Message is: ' + TRANSFORM(loError.MESSAGE), LOGIT)
					.TrackProgress('The Error occurred in Program: ' + TRANSFORM(loError.PROCEDURE), LOGIT)
					.TrackProgress('The Error occurred at line #: ' + TRANSFORM(loError.LINENO), LOGIT)
					lnNumberOfErrors = lnNumberOfErrors + 1
				ENDTRY

			ELSE
				.TrackProgress('Did not send status email: THIS.lSendInternalEmailIsOn = FALSE.',LOGIT)
			ENDIF

		ENDWITH
		RETURN
	ENDFUNC && main


	FUNCTION ExecSQL
		LPARAMETERS tcSQL, tcCursorName, tlNoDataReturnedIsOkay
		LOCAL llRetVal, lnResult
		WITH THIS
			* close target cursor if it's open
			IF USED(tcCursorName)
				USE IN (tcCursorName)
			ENDIF
			WAIT WINDOW "Fetching remote data; please wait..." NOWAIT
			lnResult = SQLEXEC(.nSQLHandle, tcSQL, tcCursorName)
			llRetVal = ( lnResult > 0 )
			IF llRetVal THEN
				* see if any data came back
				IF NOT tlNoDataReturnedIsOkay THEN
					IF (NOT USED(tcCursorName)) OR EOF(tcCursorName) THEN
						llRetVal = .F.
						.TrackProgress('No data was returned by this query: ' +CRLF + tcSQL, LOGIT+SENDIT)
					ENDIF
				ENDIF
			ELSE
				.TrackProgress("Result Code [" + TRANSFORM(lnResult) + "] returned by this query: " + tcSQL, LOGIT+SENDIT)
			ENDIF
			WAIT CLEAR
			RETURN llRetVal
		ENDWITH
	ENDFUNC


	PROCEDURE TrackProgress
		* do any combination of Wait Window, writing to logfile, and adding to body of email,
		* based on nFlags parameter.
		LPARAMETERS tcExpression, tnFlags
		LOCAL lcCRLF
		WITH THIS
			IF BITAND(tnFlags,NOCRLF) = NOCRLF THEN
				lcCRLF = ""
			ELSE
				lcCRLF = CRLF
			ENDIF
			IF BITAND(tnFlags,LOGIT) = LOGIT THEN
				IF .lLoggingIsOn THEN
					?
					? .cProcessName + "  " + TTOC(DATETIME()) + ": " + tcExpression
				ENDIF
			ENDIF
			IF .lWaitWindowIsOn THEN
				IF BITAND(tnFlags,NOWAITIT) = NOWAITIT THEN
					WAIT WINDOW tcExpression NOWAIT
				ENDIF
				IF BITAND(tnFlags,WAITIT) = WAITIT THEN
					WAIT WINDOW tcExpression TIMEOUT .nWaitWindowTimeout
				ENDIF
			ENDIF
			IF BITAND(tnFlags,SENDIT) = SENDIT THEN
				IF .lSendInternalEmailIsOn THEN
					.cBodyText = .cBodyText + tcExpression + CRLF + lcCRLF
				ENDIF
			ENDIF
		ENDWITH
	ENDPROC  &&  TrackProgress


*!*	PROCEDURE SetTempCo
*!*		LPARAMETERS tcTempCo
*!*		WITH THIS
*!*			DO CASE
*!*				CASE tcTempCo == 'D'
*!*					.cTempCo = tcTempCo
*!*					.cTempCoName = 'Quality Staffing - Carteret'
*!*					.TrackProgress('Set TempCo = ' + tcTempCo,LOGIT+SENDIT)
*!*					.TrackProgress('Set TempCoName = ' + .cTempCoName,LOGIT+SENDIT)
*!*				CASE tcTempCo == 'X'
*!*					.cTempCo = tcTempCo
*!*					.cTempCoName = 'Chartwell Staffing Solutions - San Pedro'
*!*					.TrackProgress('Set TempCo = ' + tcTempCo,LOGIT+SENDIT)
*!*					.TrackProgress('Set TempCoName = ' + .cTempCoName,LOGIT+SENDIT)
*!*				CASE tcTempCo == 'Y'
*!*					.cTempCo = tcTempCo
*!*					.cTempCoName = 'Simplified Labor Staffing Solutions - Mira Loma'
*!*					.TrackProgress('Set TempCo = ' + tcTempCo,LOGIT+SENDIT)
*!*					.TrackProgress('Set TempCoName = ' + .cTempCoName,LOGIT+SENDIT)
*!*				OTHERWISE
*!*					.TrackProgress('INVALID TempCo = ' + tcTempCo,LOGIT+SENDIT)
*!*					.cTempCoName = ''
*!*			ENDCASE

*!*			* make logfiles company-specific so that report run times can overlap
*!*			IF .lTestMode THEN
*!*				.cLogFile = 'F:\UTIL\KRONOS\Logfiles\KRONOSPUNCHDETAILRPT_' + .cTempCoName + "_"  + .cCOMPUTERNAME + '_log_TESTMODE.txt'
*!*			ELSE
*!*				.cLogFile = 'F:\UTIL\KRONOS\Logfiles\KRONOSPUNCHDETAILRPT_' + + .cTempCoName + "_" + .cCOMPUTERNAME + '_log.txt'
*!*			ENDIF
*!*			.lLoggingIsOn = .lLoggingIsOn AND NOT EMPTY(.cLogFile)
*!*			IF .lLoggingIsOn THEN
*!*				SET ALTERNATE TO (.cLogFile) ADDITIVE
*!*				SET ALTERNATE ON
*!*			ENDIF
*!*		ENDWITH
*!*	ENDPROC  &&  SetTempCo




	PROCEDURE errHandlerKTTR
		PARAMETER merror, MESS, mess1, mprog, mlineno, tcTempCo, tcMode
		LOCAL lcSendTo,lcFrom,lcSubject,lcCC,lcAttach,lcBodyText
		CLEAR

		lcSendTo = 'mark.bennett@tollgroup.com'
		lcFrom = 'fmicorporate@fmiint.com'
		lcSubject = 'Early error in KRONOSPUNCHDETAILRPT'
		lcCC = ''
		lcAttach = ''
		lcBodyText = 'Error number: ' + LTRIM(STR(merror)) + CRLF + ;
			'Error message: ' + MESS + CRLF + ;
			'Line of code with error: ' + mess1 + CRLF + ;
			'Line number of error: ' + LTRIM(STR(mlineno)) + CRLF + ;
			'Program with error: ' + mprog + CRLF + ;
			'tcTempCo = ' + tcTempCo + CRLF + ;
			'tcMode = ' + tcMode

		DO FORM dartmail2 WITH lcSendTo,lcFrom,lcSubject,lcCC,lcAttach,lcBodyText,"A"

		QUIT

	ENDPROC
	
ENDDEFINE
