* Make day-specific backups of F:\UTIL\ADPEDI\DATA\ADPDEPTS.DBF

SET SAFETY OFF


utilsetup("BACKUPADPDEPTS")

LOCAL lcSourceFile, lcTargetFile, lcToday, lcMachine
lcMachine = UPPER(ALLTRIM(GETENV("COMPUTERNAME")))
lcSourceFile = "F:\UTIL\ADPEDI\DATA\ADPDEPTS.DBF"
lcToday = DTOS(DATE())

* backup to F:
lcTargetFile = "F:\UTIL\ADPEDI\DATA\DAILYBACKUPS\ADPDEPTS_" + lcToday + ".DBF"
COPY FILE (lcSourceFile) TO (lcTargetFile)

* also backup to C:
lcTargetFile = "C:\UTIL\ADPEDI\DATA\DAILYBACKUPS\ADPDEPTS_" + lcToday + ".DBF"
COPY FILE (lcSourceFile) TO (lcTargetFile)

schedupdate()

RETURN


