*!*	* create a report that gives a running tally of div 02 and 03 costs - for Fran - MTD expected to be run each Friday

*!*	Every Friday by month.

*!*	Sample of what I would like to see .  
*!*	 
*!*	Company 02 
*!*	Trailer Cost $45,000
*!*	Truck Cost $25,000

*!*	Over the road repairs % versus repairs made at our shop.

*!*	Same info requested for Company 3.

#DEFINE CRLF CHR(13) + CHR(10)
#DEFINE HDECS 2
#DEFINE HOURLY_RATE 60
#DEFINE xlEdgeBottom 9
#DEFINE xlContinuous 1
#DEFINE xlMedium -4138


SET SAFETY OFF
SET DELETED ON
SET STRICTDATE TO 0
SET TALK OFF

LOCAL lnRow, lcRow, lcReportPeriod, lcFiletoSaveAs, lcSpreadsheetTemplate
LOCAL oExcel, oWorkbook, lcErr, lcTitle, loError, lnRate, ldToday
LOCAL lcDivision, lnLastRow

TRY
	* make these visible in called procs
	PRIVATE pdStartDate, pdEndDate, oWorksheet
	
	* START by selecting entire MTD; later we will just report on the fridays.

	ldToday = DATE()
	
	*ldToday = {^2014-06-30}  && FOR TESTING
	
	pdStartDate = ldToday - DAY(ldToday) + 1
	pdEndDate = ldToday

	lcReportPeriod = CMONTH(pdStartDate) + "_" + TRANSFORM(YEAR(pdStartDate))

	lcSpreadsheetTemplate = "F:\SHOP\SHDATA\DIV0203_RPT_TEMPLATE.XLS"

	lcFiletoSaveAs = "F:\SHOP\AllocationReports\DIV_02_03_Report_for_" + lcReportPeriod + ".XLS"
	lcTitle = "Company 02 & 03 MTD Shop Costs for " + lcReportPeriod 
	
	CLOSE DATABASES ALL

	USE F:\SHOP\SHDATA\ORDHDR IN 0
	USE F:\SHOP\SHDATA\TRUCKS IN 0
	USE F:\SHOP\SHDATA\TRAILER IN 0
	
	* create cursor of selected order header records, with addtional fields for DIVISION
*!*		SELECT *, 0000.00 AS RATE, 000000.00 AS LABRCOST, "  " AS DIVISION ;
*!*			FROM ordhdr ;
*!*			INTO CURSOR CURORDHDRPRE ;
*!*			WHERE BETWEEN(crdate,pdStartDate,pdEndDate) ;
*!*			AND (NOT EMPTY(ORDNO)) ;
*!*			AND TYPE IN ("TRUCK  ","TRAILER") ;
*!*			ORDER BY crdate ;
*!*			READWRITE
	SELECT *, "  " AS DIVISION ;
		FROM ordhdr ;
		INTO CURSOR CURORDHDRPRE ;
		WHERE BETWEEN(crdate, pdStartDate, pdEndDate) ;
		AND (NOT EMPTY(ORDNO)) ;
		AND TYPE IN ("TRUCK  ","TRAILER") ;
		ORDER BY crdate ;
		READWRITE
		
		
*!*		SELECT CURORDHDRPRE
*!*		BROW

*!*		* POPULATE RATE BY CRDATE AND CALC LABOR COST FOR EACH WO REC
*!*		SELECT CURORDHDRPRE
*!*		SCAN
*!*			lnRate = GetHourlyRateByCRDate( CURORDHDRPRE.crdate )
*!*			REPLACE CURORDHDRPRE.RATE WITH lnRate, CURORDHDRPRE.LABRCOST WITH ( CURORDHDRPRE.tothours * lnRate )
*!*		ENDSCAN

	SELECT CURORDHDRPRE
	SCAN

		WAIT WINDOW "checking Vehicle #" + TRANSFORM(CURORDHDRPRE.vehno) NOWAIT
		
		IF CURORDHDRPRE.TYPE == "TRUCK" THEN
			
			* check trucks first			

			SELECT trucks
			LOCATE FOR ALLTRIM(truck_num) == ALLTRIM(CURORDHDRPRE.vehno)
			IF FOUND() THEN
				REPLACE CURORDHDRPRE.DIVISION WITH trucks.DIVISION IN CURORDHDRPRE
				SELECT trucks
				LOCATE
				LOOP
			ENDIF

			SELECT trailer
			LOCATE FOR ALLTRIM(trail_num) == ALLTRIM(CURORDHDRPRE.vehno)
			IF FOUND() THEN
				REPLACE CURORDHDRPRE.DIVISION WITH trailer.DIVISION IN CURORDHDRPRE
				SELECT trailer
				LOCATE
				LOOP
			ENDIF
		
		ELSE
			
			* check trailers first			

			SELECT trailer
			LOCATE FOR ALLTRIM(trail_num) == ALLTRIM(CURORDHDRPRE.vehno)
			IF FOUND() THEN
				REPLACE CURORDHDRPRE.DIVISION WITH trailer.DIVISION IN CURORDHDRPRE
				SELECT trailer
				LOCATE
				LOOP
			ENDIF

			SELECT trucks
			LOCATE FOR ALLTRIM(truck_num) == ALLTRIM(CURORDHDRPRE.vehno)
			IF FOUND() THEN
				REPLACE CURORDHDRPRE.DIVISION WITH trucks.DIVISION IN CURORDHDRPRE
				SELECT trucks
				LOCATE
				LOOP
			ENDIF
			
		ENDIF && CURORDHDRPRE.TYPE == "TRUCK" 

	ENDSCAN
	WAIT CLEAR

	oExcel = CREATEOBJECT("excel.application")
	oExcel.displayalerts = .F.
	oExcel.VISIBLE = .T.

	oWorkbook = oExcel.workbooks.OPEN(lcSpreadsheetTemplate)
	oWorkbook.SAVEAS(lcFiletoSaveAs)

	oWorksheet = oWorkbook.Worksheets[1]
	oWorksheet.RANGE("A5","W20").clearcontents()
	oWorksheet.RANGE("D1").VALUE = lcReportPeriod

	lnRow = 4
	lcDivision = '02'
	
	lnLastRow = CreateRows(lnRow, lcDivision)
	
	lcDivision = '03'
	lnLastRow = CreateRows(lnLastRow + 1, lcDivision)
	

	oWorkbook.SAVE()
	oExcel.VISIBLE = .T.
	MESSAGEBOX("The Spreadsheet has been populated in Excel.",0 + 64,lcTitle)
	IF TYPE('oExcel') = 'O' AND NOT ISNULL(oExcel) THEN
		oExcel.QUIT()
	ENDIF	
	

CATCH TO loError

	lcErr = 'There was an error.' + CRLF
	lcErr = lcErr + 'The Error # is: ' + TRANSFORM(loError.ERRORNO) + CRLF
	lcErr = lcErr + 'The Error Message is: ' + TRANSFORM(loError.MESSAGE) + CRLF
	lcErr = lcErr + 'The Error occurred in Program: ' + TRANSFORM(loError.PROCEDURE) + CRLF
	lcErr = lcErr + 'The Error occurred at line #: ' + TRANSFORM(loError.LINENO)
	MESSAGEBOX(lcErr,0+16,lcTitle)

	*oExcel.VISIBLE = .T.
	IF TYPE('oExcel') = 'O' AND NOT ISNULL(oExcel) THEN
		oExcel.QUIT()
	ENDIF

FINALLY

	CLOSE DATABASES ALL
	
	
ENDTRY

RETURN


*!*	FUNCTION GetHourlyRateByCRDate
*!*		LPARAMETERS tdCRdate
*!*		DO CASE
*!*			CASE tdCRdate < {^2008-09-01}
*!*				lnRate = 60.00
*!*			OTHERWISE
*!*				lnRate = 70.00
*!*		ENDCASE
*!*		RETURN lnRate
*!*	ENDFUNC


FUNCTION CreateRows
	LPARAMETERS tnRow, tcDivision
	* loop through the Fridays in the month so far, running a query and populating a spreadsheet row for each
	LOCAL lnRow, lcRow, lnInside, lnOutside, lnTotal, lnPercent, lcDivision, ldDate

	ldDate = pdStartDate
	lnRow = tnRow
	
	DO WHILE ldDate <= pdEndDate
	
		IF DOW(ldDate,1) = 6 THEN
			*it's a Friday, query for that date  
			lnRow = lnRow + 1
			lcRow = ALLTRIM(STR(lnRow))
			
			* TRAILERS
			SELECT CURORDHDRPRE
			SUM (TOTLCOST + TOTPCOST) TO lnInside FOR BETWEEN(CRDATE, pdStartDate, ldDate) AND DIVISION = tcDivision AND TYPE = 'TRAILER'
			SUM (OUTSCOST) TO lnOutside FOR BETWEEN(CRDATE, pdStartDate, ldDate) AND DIVISION = tcDivision AND TYPE = 'TRAILER'
			lnTotal = lnInside + lnOutside
			IF lnTotal > 0 THEN
				lnPercent = lnOutside / lnTotal
			ELSE
				lnPercent = 0
			ENDIF
			oWorksheet.RANGE("A"+lcRow).VALUE = CDOW(ldDate)
			oWorksheet.RANGE("B"+lcRow).VALUE = ldDate
			oWorksheet.RANGE("C"+lcRow).VALUE = "'" + tcDivision
			oWorksheet.RANGE("D"+lcRow).VALUE = lnInside
			oWorksheet.RANGE("E"+lcRow).VALUE = lnOutside
			oWorksheet.RANGE("F"+lcRow).VALUE = lnTotal
			oWorksheet.RANGE("G"+lcRow).VALUE = lnPercent
			
			* TRUCKS
			SELECT CURORDHDRPRE
			SUM (TOTLCOST + TOTPCOST) TO lnInside FOR BETWEEN(CRDATE, pdStartDate, ldDate) AND DIVISION = tcDivision AND TYPE = 'TRUCK  '
			SUM (OUTSCOST) TO lnOutside FOR BETWEEN(CRDATE, pdStartDate, ldDate) AND DIVISION = tcDivision AND TYPE = 'TRUCK  '
			lnTotal = lnInside + lnOutside
			IF lnTotal > 0 THEN
				lnPercent = lnOutside / lnTotal
			ELSE
				lnPercent = 0
			ENDIF
			oWorksheet.RANGE("A"+lcRow).VALUE = CDOW(ldDate)
			oWorksheet.RANGE("B"+lcRow).VALUE = ldDate
			oWorksheet.RANGE("C"+lcRow).VALUE = "'" + tcDivision
			oWorksheet.RANGE("I"+lcRow).VALUE = lnInside
			oWorksheet.RANGE("J"+lcRow).VALUE = lnOutside
			oWorksheet.RANGE("K"+lcRow).VALUE = lnTotal
			oWorksheet.RANGE("L"+lcRow).VALUE = lnPercent
			
		ENDIF && DOW(ldDate,1) = 6
			
		ldDate = ldDate + 1
	
	ENDDO
	
	RETURN lnRow

ENDFUNC  && CreateRows

