ASSERT .F. MESSAGE "At start of PROCESS phase"

lNewPT = .T.

CD &lcPath

waitstr = "Processing records for "+cMailName
WAIT WINDOW waitstr TIMEOUT 2

lnNum = ADIR(tarray,cFilemask)

IF lnNum = 0
	WAIT WINDOW AT 10,20 "No "+cMailName+" 940's to import for Lifefactory EDI" TIMEOUT 3
	RETURN
ENDIF

ASSERT .F. MESSAGE "At process looping"

FOR thisfile = 1  TO lnNum
	lLoop = .F.
	cFilename = ALLTRIM(tarray[thisfile,1])
	nFileSize = VAL(TRANSFORM(tarray(thisfile,2)))
	IF lTesting
		? cFilename
	ENDIF
	cCoNum = LEFT(cFilename,2)
	xfile = lcPath+TRIM(tarray[thisfile,1])
	archivefile = lcArchivePath+TRIM(tarray[thisfile,1])
	IF JUSTFNAME(cFilename) = "FOXUSER"
		DELETE FILE [&xfile]
		LOOP
	ENDIF

	c997inPath = "F:\FTPUSERS\LIFEFACTORY\997in\"
	c997file = c997inPath+ALLTRIM(tarray[thisfile,1])
	c810inPath = "F:\FTPUSERS\LIFEFACTORY\810in\"
	c810file = c810inPath+ALLTRIM(tarray[thisfile,1])
	cErrorPath = "F:\FTPUSERS\LIFEFACTORY\940inEDI\errors\"
	cErrorFile = cErrorPath+ALLTRIM(tarray[thisfile,1])
*!*		fa997file = cFA997Path+ALLTRIM(tarray[thisfile,1])

	WAIT WINDOW "Importing file: "+xfile TIMEOUT 2
	IF FILE(xfile)
		RELEASE ALL LIKE APT
		RELEASE ALL LIKE APTDET

		DO m:\dev\prg\createx856a
		cFStr = FILETOSTR(xfile)
		DO CASE
			CASE "P*+'"$cFStr
				RELEASE cFStr
				DO m:\dev\prg\loadedifile WITH xfile,"*","LFAPOS","LIFEFACTORY" && Comment this and uncomment next line to load pre-converted 940s
			CASE "P~+'"$cFStr
				DO m:\dev\prg\loadedifile WITH xfile,"~","LFAPOS","LIFEFACTORY" && Comment this and uncomment next line to load pre-converted 940s
			OTHERWISE
				APPEND FROM [&xfile] TYPE DELIM WITH CHAR "*"
		ENDCASE

		SELECT x856
		LOCATE

		LOCATE FOR ALLTRIM(x856.segment)="GS"
		DO CASE
			CASE ALLTRIM(x856.f1) = "FA"  && 997
				WAIT WINDOW "This is a 997 file...moving and looping" TIMEOUT 1
				COPY FILE [&xfile] TO [&c997file]
				DELETE FILE [&xfile]
				LOOP
			CASE ALLTRIM(x856.f1) = "IN"  && 810
				WAIT WINDOW "This is an 810 file...moving and looping" TIMEOUT 1
				COPY FILE [&xfile] TO [&c810file]
				DELETE FILE [&xfile]
				LOOP
			CASE ALLTRIM(x856.f1) # "OW"  && other non-940
				WAIT WINDOW "This is a Non-940 file...moving and looping" TIMEOUT 1
				COPY FILE [&xfile] TO [&cErrorFile]
				DELETE FILE [&xfile]
				LOOP
		ENDCASE

		LOCATE
		LOCATE FOR x856.segment = "W01"
		IF !FOUND()
			WAIT WINDOW "This is an bad file structure...moving and looping" TIMEOUT 1
			COPY FILE [&xfile] TO [&cErrorFile]
			DELETE FILE [&xfile]
			LOOP
		ENDIF

		COPY FILE [&xfile] TO [&ArchiveFile]

		SELECT x856
		LOCATE
		IF lTesting AND lBrowfiles
* BROWSE
		ENDIF

		DO create_pt_cursors WITH cUseFolder,lLoadSQL && Added to prep for SQL Server transition

*!* End of added code

		DO (cUseDir+"BKDN") WITH cFilename
		IF lLoop
			LOOP
		ENDIF

		DO m:\dev\prg\all940_import

		IF FILE(archivefile)
			IF FILE(xfile) && AND !lTesting
				DELETE FILE [&xfile]
			ENDIF
		ENDIF

*release_ptvars()

	ENDIF
NEXT thisfile

&& now clean up the 940in archive files, delete for the upto the last 10 days
deletefile(lcArchivePath,IIF(DATE()<{^2016-05-01},20,10))

WAIT CLEAR
WAIT WINDOW "ALL "+cMailName+" 940 files finished processing for: "+cOfficename+"..." TIMEOUT 3

*************************************************************************************************
