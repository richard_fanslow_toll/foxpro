* Today's appointments for Highline

utilsetup("HIGHLINEAPPT")

if holiday(date())
	return
endif

guserid="AUTO"

goffice="2"

xsqlexec("select ship_ref, wo_num, wo_date, consignee, cnee_ref, ship_via, ctnqty, " + ;
	"start, cancel, called, appt, appt_time, appt_num, apptremarks, pulleddt, picked, labeled, staged " + ;
	"from outship where mod='"+goffice+"' and inlist(accountid,5864,5910) " + ;
	"and appt={"+dtoc(date())+"} and del_date={} and notonwip=0","xrpt",,"wh")

xfilename="h:\pdf\highlineappt.pdf"

rpt2pdf("highlineappt",xfilename)

tattach=xfilename
tsendto="nita@highlineunited.us"
*tsendto="dyoung@fmiint.com"
tcc=""
tFrom ="TGF EDI <transload-ops@fmiint.com>"
tmessage = "See attached file"
tSubject = "Today's Appointments"

Do Form dartmail2 With tsendto,tFrom,tSubject," ",tattach,tmessage,"A"

schedupdate()

_screen.Caption=gscreencaption
on error
