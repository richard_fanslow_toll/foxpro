PARAMETERS thisOffice

CLOSE DATABASES ALL
CLEAR

*!* Set and initialize public variables
PUBLIC cWhichfile,nRunID,xFile,cXdate1,dXdate2,cOffice,lTesting,cUseDir,cCustName,cProperName
PUBLIC cDelimiter,cTranslateOption,EmailCommentStr,LogCommentStr,nAcctNum,cAcct_num,l997
PUBLIC chgdate,ptid,ptctr,nPTQty,lTestUploaddet,cOfficename,cConsignee,cStoreNum,cTotPT
PUBLIC cPickticket_num_start,cPickticket_num_end,cLoadID,lcPath,lcStr,cUseFolder,lADNY,cTransfer
PUBLIC lcArchivePath,lBrowFiles,NormalExit,lTestImport,Archivefile,tsendto,tcc,lcArchivefile
PUBLIC lTestTableLoad,tsendtoerr,tccerr,lOverrideBusy,cFilename,cWO_Num,lDoImport,tsendtoerr,tccerr
PUBLIC cSCCNumber,m.sccnumber,cCHKDigit,m.chkdigit,cartonid,m.cartonid,cSTStoreNum,lDoImport,cMod,nFileSize
PUBLIC m.custsku,cSizenum,ptid,accountid,m.id,m.ptid,m.acct_num,lOldStyle,lPrepackRep,lnnum,cISA_Num
PUBLIC WAITTIME,errmsg,cPTDupes,nDetCnt,tsendtopt,tccpt,tsendtolbls,tcclbls,tfrom,lLoadSQL
PUBLIC llMoveFiletoNO_UPC
PUBLIC llEmailTest

SET STEP ON
lcOffice = thisOffice
llMoveFiletoNO_UPC = .F.
llEmailTest = .T.

TRY
	lTesting = .F.  && Default = .f.
	lTestImport = lTesting  && Default = .f.
	lOverrideBusy = lTesting  && If set, will overcome FTPSETUP chkbusy flag; default = .f.
*	lOverrideBusy = .T.

	IF VARTYPE(thisOffice)#"C"
		thisOffice = "J"
	ELSE
		IF EMPTY(thisOffice)
			WAIT WINDOW AT 10,10 "Must pass an office "  TIMEOUT 2
			RETURN
		ENDIF
	ENDIF
	cOffice = thisOffice
	cMod = IIF(cOffice = "N","J",cOffice)
	gOffice = cMod
	gMasterOffice = cOffice
	lLoadSQL = .T.

	WAITTIME = 1

	cCustName="Marc Jacobs"
	tsendto  = "pgaidis@fmiint.com"
	STORE "" TO tcc,tccerr,tsendtolbls,tcclbls,xFile,lcSourceProgram,errmsg

	DO m:\dev\prg\_setvars WITH .T.
	ASSERT .F. MESSAGE "At process start...debug"
	NormalExit = .F.
	PUBLIC ARRAY a856(1)

	STORE "" TO cPickticket_num_start,cPickticket_num_end,EmailCommentStr,cLoadID,LogCommentStr,xFile,cXdate1
	STORE "" TO tsendto,tcc,tsendtoerr,tccerr
*  cOffice = "J"  this now set as a calling parameter
	DO CASE
		CASE INLIST(cOffice,"N","J")
			cOfficename = "New Jersey"
		CASE cOffice='L'
			cOfficename = "California"
	ENDCASE

	nAcctNum = 6303

*	_SCREEN.WINDOWSTATE=IIF(lTestImport OR lOverrideBusy,2,1)

	SELECT 0
	USE F:\edirouting\ftpsetup SHARED
	cTransfer = "940-MARCJACOBS"
	IF ftpsetup.chkbusy AND !lOverrideBusy
		WAIT WINDOW cCustName+" 940 already in process...exiting" TIMEOUT 1
		CANCEL
		RETURN
	ENDIF
	REPLACE chkbusy WITH .T.,trig_time WITH DATETIME() FOR ftpsetup.TRANSFER = cTransfer
	USE IN ftpsetup

	dXdate2 = DATE()
	nRunID = 999
	cAcct_num = ALLTRIM(STR(nAcctNum))
	nDetCnt = 0


	CREATE CURSOR pterrors(;
		ship_ref CHAR(20),;
		errormsg  CHAR(30))

	SELECT 0

	USE F:\3pl\DATA\mailmaster ALIAS mm
	LOCATE FOR mm.edi_type = "940" AND mm.accountid = nAcctNum AND mm.office = cMod
	IF FOUND()
		STORE TRIM(mm.acctname) TO cCustName
		STORE TRIM(mm.basepath) TO lcPath
		STORE TRIM(mm.archpath) TO lcArchivePath
		STORE TRIM(IIF(mm.use_alt,mm.sendtoalt,mm.sendto)) TO tsendto
		STORE TRIM(IIF(mm.use_alt,mm.ccalt,mm.cc)) TO tcc
		IF !lTesting
			_SCREEN.CAPTION = ALLTRIM(mm.scaption)
		ELSE
			_SCREEN.CAPTION = ALLTRIM(mm.scaption)+" - TESTING"
		ENDIF
		LOCATE FOR mm.edi_type = "MISC" AND mm.taskname = "GENERAL"
		IF lTesting
			STORE TRIM(IIF(mm.use_alt,mm.sendtoalt,mm.sendto)) TO tsendto
			STORE TRIM(IIF(mm.use_alt,mm.ccalt,mm.cc)) TO tcc
		ENDIF
		STORE TRIM(IIF(mm.use_alt,mm.sendtoalt,mm.sendto)) TO tsendtoerr
		STORE TRIM(IIF(mm.use_alt,mm.ccalt,mm.cc)) TO tccerr
		LOCATE
		LOCATE FOR mm.edi_type = "MISC" AND mm.acctname = "MARCJACOBS" AND mm.taskname = "DUPPTNOTICE"
		STORE TRIM(IIF(mm.use_alt,mm.sendtoalt,mm.sendto)) TO tsendtopt
		STORE TRIM(IIF(mm.use_alt,mm.ccalt,mm.cc)) TO tccpt
		IF cOffice = "L"
			LOCATE
			LOCATE FOR mm.edi_type = "MISC" AND mm.acctname = "MARCJACOBS" AND mm.taskname = "LABELS2X2"
			STORE TRIM(IIF(mm.use_alt,mm.sendtoalt,mm.sendto)) TO tsendtolbls
			STORE TRIM(IIF(mm.use_alt,mm.ccalt,mm.cc)) TO tcclbls
		ENDIF
		USE IN mm

	ELSE
		WAIT WINDOW AT 10,10  "No parameters set for this acct# "+ALLTRIM(STR(nAcctNum))+"  ---> Office "+lcOffice TIMEOUT 2
		USE IN mm
		CLOSE DATA ALL
		CANCEL
		RETURN
	ENDIF

	DO CASE
		CASE lTesting
			lcPath = STRTRAN(ALLT(lcPath),"mj_wholesale","mj_wholesale_test")
			lcArchivePath = STRTRAN(ALLT(lcArchivePath),"mj_wholesale","mj_wholesale_test")

		CASE INLIST(cOffice,'N','J')
			lcPath = STRTRAN(ALLT(lcPath),"mj_wholesale","mj_wholesale")
			lcArchivePath = STRTRAN(ALLT(lcArchivePath),"mj_wholesale","mj_wholesale")

		CASE cOffice = 'L'
			lcPath = STRTRAN(ALLT(lcPath),"mj_wholesale_CA","mj_wholesale_CA")
			lcArchivePath = STRTRAN(ALLT(lcArchivePath),"mj_wholesale_CA","mj_wholesale_CA")
	ENDCASE


	IF lTestImport
		cUseFolder = "F:\WHP\WHDATA\"
	ELSE
		xReturn = "XXX"
		DO m:\dev\prg\wf_alt WITH cOffice,nAcctNum
		cUseFolder = UPPER(xReturn)
	ENDIF

	cProperName = "Marc Jacobs (W)"
	cDelimiter = "*"
	cTranslateOption ="TILDE"
	cUseDir = ("m:\dev\prg\"+cCustName+"940_")
	l997 = .F.

	IF lTesting
		lTestUploaddet = .T.
	ELSE
		lTestUploaddet = .F.
	ENDIF

	DO m:\dev\prg\createx856a
	WAIT CLEAR

	DO (cUseDir+"PROCESS") WITH cOffice

	SELECT 0
	USE F:\edirouting\ftpsetup SHARED
	cTransfer = "940-MARCJACOBS"
	REPLACE chkbusy WITH .F. FOR ftpsetup.TRANSFER = cTransfer
	USE IN ftpsetup

	NormalExit = .T.
	THROW

CATCH TO oErr
	IF !NormalExit
		ASSERT .F. MESSAGE "At Error Catch..."
		SET STEP ON
		tsubject = cCustName+" 940 Upload Error ("+TRANSFORM(oErr.ERRORNO)+") at "+TTOC(DATETIME())
		tattach  = ""
		tsendto  = "pgaidis@fmiint.com"
		tcc = "pgaidis@fmiint.com"
		tmessage = "Marc Jacobs (W) 940 Upload Error!"
		lcSourceMachine = SYS(0)
		lcSourceProgram = SYS(16)

		tmessage =tmessage+ "Try/CATCH Exeception Message:"+CHR(13)+;
			[  Error: ] + STR(oErr.ERRORNO) +CHR(13)+;
			[  LineNo: ] + STR(oErr.LINENO) +CHR(13)+;
			[  Message: ] + oErr.MESSAGE +CHR(13)+;
			[  Procedure: ] + oErr.PROCEDURE +CHR(13)+;
			[  Details: ] + oErr.DETAILS +CHR(13)+;
			[  StackLevel: ] + STR(oErr.STACKLEVEL) +CHR(13)+;
			[  LineContents: ] + oErr.LINECONTENTS+CHR(13)+;
			[  UserValue: ] + oErr.USERVALUE+CHR(13)+;
			[  Computer:  ] +lcSourceMachine+CHR(13)+;
			[  940 file:  ] +xFile+CHR(13)+;
			[  Error   :  ] +errmsg+CHR(13)+;
			[  Program:   ] +lcSourceProgram

		tattach  = ""
		tfrom    ="Toll EDI Processing Center <toll-edi-ops@tollgroup.com>"
    Do sendmail_au With tsendto,"noreply@tollgroup.com",tsubject,tcc,tattach,tmessage,""
           **Parameter lcTO, lcFrom,                lcSubject,lcCC,lcAttach,lcBody,lcBCC
	ELSE
		WAIT WINDOW AT 10,10 "Normal Exit " TIMEOUT 2
	ENDIF
ENDTRY
