* Lifefactory inventory report

utilsetup("LIFEINVRPT")

if holiday(date())
	return
endif

guserid="AUTO"

goffice="I"

delete file h:\fox\lifeinvrpt.xls
xfilename="h:\fox\lifeinvrpt.xls"
	
xsqlexec("select style, color, id, availqty, allocqty from inven where accountid=6034 and units=1","xdytemp",,"wh")

select style, color, id, availqty, allocqty as allocnotpulled, 0000000 as pullednotshipped ;
	from xdytemp order by style, color, id into cursor xrpt readwrite

index on style+color+id tag stylecolid

xsqlexec("select * from outship where accountid=6034 and pulled=1 and notonwip=0 and del_date={}",,,"wh")
scan
	xsqlexec("select * from outdet where outshipid="+transform(outship.outshipid)+" and totqty>0 and units=1",,,"wh")
	scan
		if seek(style+color+id,"xrpt","stylecolid")
			replace pullednotshipped with xrpt.pullednotshipped + outdet.totqty in xrpt
		else
			set step on 
		endif
	endscan
endscan

select xrpt
delete for availqty=0 and allocnotpulled=0 and pullednotshipped=0
copy to (xfilename) xls

tattach=xfilename
tsendto="Greg@Lifefactory.com, chris.malcolm@tollgroup.com, ethan@lifefactory.com, vlad@lifefactory.com"
*tsendto="chris.malcolm@tollgroup.com"
tcc=""
tFrom ="TGF Operations <fmi-transload-ops@fmiint.com>"
tmessage = "See attached file"
tSubject = "Inventory Report"

Do Form dartmail2 With tsendto,tFrom,tSubject," ",tattach,tmessage,"A"

*

if used("inven")
	use in inven
endif

use in outship
use in outdet

schedupdate()

_screen.Caption=gscreencaption
on error
