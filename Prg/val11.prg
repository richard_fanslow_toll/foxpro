* runs every two hours

lparameters xskipreports

utilsetup("VAL11")

if holiday(date())
	return
endif

store "" to guserid, gprocess
gdevelopment=.f.
xjobno=0
xsystem="VAL11"

external array xdbf
 
use f:\auto\val11 exclusive
zap
use


*

valset("fix incorrect remainqty in adj")

useca("adj","wh",,,"totqty-removeqty#remainqty")

if reccount()>0
	copy to f:\auto\xadjremain
	email("Dyoung@fmiint.com","VAL (fixed) "+transform(reccount())+" incorrect remainqty in adj","xadjremain",,,,.t.,,,,,.t.)

	replace all remainqty with totqty-removeqty
	tu()
endif

*

valset("fix incorrect remainqty in indet")

useca("indet","wh",,,"totqty-removeqty#remainqty")

if reccount()>0
	copy to f:\auto\xindetremain
	email("Dyoung@fmiint.com","VAL (fixed) "+transform(reccount())+" incorrect remainqty in indet","xindetremain",,,,.t.,,,,,.t.)

	replace all remainqty with totqty-removeqty
	tu()
endif

*

valset("fix incorrect remainqty in inloc")

useca("inloc","wh",,,"locqty-removeqty#remainqty")

if reccount()>0
	copy to f:\auto\xinlocremain
	email("Dyoung@fmiint.com","VAL (fixed) "+transform(reccount())+" incorrect remainqty in inloc","xinlocremain",,,,.t.,,,,,.t.)

	replace all remainqty with locqty-removeqty
	tu()
endif

*

valset("check for orphaned PACKAGEs")

select whoffice
scan for !test and !novalid
	xoffice=whoffice.office
	goffice=xoffice
	xfolder=whoffice.folder+"\whdata\"

	useca("package","wh",,,"mod='"+goffice+"' and adddt>={"+dtoc(date())+"}")
	
	select * from package where .f. into cursor xrpt readwrite
	
	select package
	scan
		if xsqlexec("select * from shipment where shipmentid="+transform(package.shipmentid),,,"wh") = 0
			select package
			scatter memvar
			insert into xrpt from memvar
			delete in package
		endif
	endscan

	select xrpt
	if reccount()>0
		tu("package")
		copy to ("f:\auto\xorphanpackage"+xoffice)
		email("Dyoung@fmiint.com","VAL (fixed) "+transform(reccount())+" orphaned PACKAGEs, office "+xoffice,"xorphanpackage"+xoffice,,,,.t.,,,,,.t.)
	endif
endscan

*

valset("check for mismatched master/sub BLs")

select whoffice
scan for !test and !novalid 
	xoffice=whoffice.office
	goffice=xoffice
	xfolder=whoffice.folder+"\whdata\"

	xsqlexec("select * from bl where mod='"+goffice+"'",,,"wh")
	xsqlexec("select * from bldet where mod='"+goffice+"'",,,"wh")
	index on blid tag blid
	set order to

	select bl.blid, bol_no, bldate, ship_ref from bl, bldet ;
		where bl.blid=bldet.blid and masterbill ;
		order by 2,4 into cursor xmaster readwrite
	
	select bl.blid, mblnum, bldate, ship_ref from bl, bldet ;
		where bl.blid=bldet.blid and !empty(mblnum) ;
		order by 2,4 into cursor xsub readwrite
		
	select *, .f. as notinmaster, .f. as notinsub from xmaster where .f. into cursor xrpt readwrite

	select xmaster
	scan 
		if xmaster.ship_ref # xsub.ship_ref
			if xmaster.bol_no+xmaster.ship_ref<xsub.mblnum+xsub.ship_ref
				scatter memvar
				insert into xrpt from memvar
				replace notinsub with .t. in xrpt
			else
				select xsub
				scatter memvar
				m.bol_no=mblnum
				insert into xrpt from memvar
				replace notinmaster with .t. in xrpt
				skip -1 in xmaster
				skip in xsub
			endif
		else
			skip in xsub
		endif
	endscan

	select xrpt	
	delete for bldate<date()-60 or inlist(bol_no,"04907314694120008","00866945000614100","08447020001182317","04907316182072356","04070086210195351")
	count to aa
	if aa>0
		copy to ("f:\auto\xmismastersubbl"+xoffice)	
		email("Dyoung@fmiint.com","VAL "+transform(aa)+" mismatched master/sub BLs, office "+xoffice,"xmismastersubbl"+xoffice,,,,.t.,,,,,.t.)
	endif

	use in bl
	use in bldet
endscan

*

valset("fix duplicate whseloc in inventory")

select whoffice
scan for !test and !novalid
	xoffice=whoffice.office
	goffice=xoffice
	xmasteroffice=whoffice.rateoffice
	gmasteroffice=xmasteroffice
	xfolder=whoffice.folder+"\whdata\"
	gmodfilter="mod='"+goffice+"'"

	useca("inven","wh",,,"mod='"+goffice+"'")
	useca("invenloc","wh",,,"mod='"+goffice+"'")
	
	xsqlexec("select * from whseloc where office='"+xmasteroffice+"'","whseloc",,"wh")
	index on whseloc tag whseloc
	set order to

	select inven.invenid, inven.accountid, inven.units, inven.style, inven.color, inven.id, inven.pack, whseloc ;
		from inven, invenloc where inven.invenid=invenloc.invenid group by 1,8 having count(whseloc)>1 into cursor xrpt

	if _tally>0
		xtally=_tally
		scan 
			select inven
			locate for accountid=xrpt.accountid and units=xrpt.units and style=xrpt.style and color=xrpt.color and id=xrpt.id and pack=xrpt.pack
			if found()
				fixinven(.t.,,.t.)
			endif
		endscan

		copy to ("f:\auto\xdupinvenwhseloc"+xoffice)
		email("Dyoung@fmiint.com","VAL (fixed) "+transform(xtally)+" duplicate whseloc in inventory for "+xoffice,"xdupinvenwhseloc"+xoffice,,,,.t.,,,,,.t.)
	endif

	if used("indet")
		use in indet
		use in inloc
	endif
	
	use in inven
	use in invenloc
	if used("adj")
		use in adj
	endif
	if used("outwolog")
		use in outwolog
		use in outdet
		use in outloc
	endif
	
	use in whseloc
endscan

*

valset("check for duplicate PT's in outship")

select whoffice
scan for !test and !novalid
	xoffice=whoffice.office
	goffice=xoffice
	xfolder=whoffice.folder+"\whdata\"

	xsqlexec("select * from outship where mod='"+goffice+"' and wo_date>{"+dtoc(date()-10)+"}",,,"wh")

	select accountid, ship_ref, wo_date from outship where wo_date>date()-10 group by 1,2 having count(1)>1 into cursor xrpt

	select xrpt	
	if reccount()>0
		copy to ("f:\auto\xdupptpt"+xoffice)	
		email("Dyoung@fmiint.com","VAL "+transform(reccount())+" duplicate PTs in outship, office "+xoffice,"xdupptpt"+xoffice,,,,.t.,,,,,.t.)
	endif

	use in outship
	gunshot()
endscan

*

valset("fix mismatched hold/holdtype in invenloc")

select whoffice
scan for !test and !novalid
	xoffice=whoffice.office
	goffice=xoffice
	gmasteroffice=whoffice.rateoffice
	xfolder=whoffice.folder+"\whdata\"
	gmodfilter="mod='"+goffice+"'"
	xpnpaccount=pnpacct(xoffice)
	
	useca("invenloc","wh",,,"mod='"+goffice+"'")
	
	select * from invenloc where locqty#0 and ((hold and empty(holdtype) and whseloc#"RACK") or (!hold and !empty(holdtype))) into cursor xrpt
	
	if reccount()#0
		copy to ("f:\auto\xinvenlochold"+xoffice)
		
		xfixed=0
		scan 
			do case
			case xrpt.whseloc="RACK" and !xrpt.hold
				xfixed=xfixed+1
				replace holdtype with "" in invenloc for invenlocid=xrpt.invenlocid
				tu("invenloc")

			case xrpt.whseloc="CYCLE" and empty(xrpt.holdtype)
				xfixed=xfixed+1
				replace holdtype with "Cycle" in invenloc for invenlocid=xrpt.invenlocid
				tu("invenloc")

			case xrpt.hold
				if !used("whseloc")
					useca("inven","wh",,,"mod='"+goffice+"'")

					xsqlexec("select * from whseloc where office='"+gmasteroffice+"'","whseloc",,"wh")
					index on whseloc tag whseloc
					set order to
				endif
				
				gholdtype=""
				if holdloc(xrpt.whseloc)
					assert .f.
					xfixed=xfixed+1
					replace holdtype with gholdtype in invenloc for invenlocid=xrpt.invenlocid
					tu("invenloc")

				else
					xfixed=xfixed+1
					replace hold with .f. in invenloc for invenlocid=xrpt.invenlocid
					tu("invenloc")
					
					select inven
					locate for accountid=xrpt.accountid and units=xrpt.units and style=xrpt.style and color=xrpt.color and id=xrpt.id and pack=xrpt.pack
					fixinven(.t.,,.t.)

					if &xpnpaccount and !xrpt.units		&& not musical
						select inven
						locate for accountid=xrpt.accountid and units=.t. and style=xrpt.style and color=xrpt.color and id=xrpt.id
						if found()
							fixinven(.t.,,.t.)
						endif
					endif				
				endif
			endcase
		endscan
		
		if xfixed=reccount()
			xfix="(fixed) "
			xcount=reccount()
		else
			xfix=""
			xcount=reccount()-xfixed
		endif

		email("Dyoung@fmiint.com","VAL "+xfix+transform(xcount)+" mismatched hold/holdtype in invenloc, office "+xoffice,"xinvenlochold"+xoffice,,,,.t.,,,,,.t.)
	endif

	tu("invenloc")
		
	use in invenloc

	if used("whseloc")
		if used("indet")
			use in indet
			use in inloc
		endif
		use in inven
		
		if used("outwolog")
			use in outwolog
			use in outdet
			use in outloc
		endif
		use in whseloc
	endif
endscan

*

use f:\auto\val11
go bottom
replace zendtime with datetime()
replace all seconds with zendtime-zstarttime
use 

schedupdate()

_screen.Caption=gscreencaption
on error