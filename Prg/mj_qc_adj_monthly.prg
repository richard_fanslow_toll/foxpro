* This program extracts Retail carton receivings from previous day
*
* MB: modified 2/1/2018 to remove Todd Margolin and replace Jim Killen with Chris Malcolm in the emails.
* Build EXE as F:\3PL\MJ_QC_ADJ_MONTHLY.EXE 
*
* Modified 4/2/201 MB to save the output spreadsheet as xl5, because Chris Malcolm cannot open the xls in his version of Excel.

utilsetup("MJ_QC_ADJ_MONTHLY")
set exclusive off
set deleted on
set talk off
set safety off   &&Allows for overwrite of existing files

close databases all
xday=day(date())
xdow=cdow(date())
xmonth=month(date())-1
xcmonth=cmonth(date())
xyear=year(date())
if xmonth=0
	xmonth=12
	xyear=year(date())-1
endif

xstartdt=ctod(transform(xmonth)+"/1/"+transform(xyear))
xenddt=gomonth(xstartdt,1)-1

set step on

goffice="J"
*!*	if usesqladj()
*!*		xsqlexec("select * from adj where mod='J' and between(adjdt,{"+dtoc(xstartdt)+"},{"+dtoc(xenddt)+"}",,,"wh")
*!*		SELECT ACCOUNTID,ADdDT,STYLE,COLOR,ID,totqty, whseloc,comment FROM ADJ WHERE 'QC' $(whseloc) INTO CURSOR TEMP556
*!*	else
*!*		If !Used("adj")
*!*		  use f:\whj\whdata\adj in 0
*!*		ENDIF
*!*		SELECT ACCOUNTID,ADdDT,STYLE,COLOR,ID,totqty, whseloc,comment FROM ADJ WHERE 'QC' $(whseloc)  AND Month(adjdt)=xmonth AND year(adjdt)=xyear INTO CURSOR TEMP556
*!*	ENDIF
useca("adj","wh",,,"mod='"+goffice+"' and  accountid in (6303,6325,6543) and ADJdt>{"+dtoc(date()-60)+"}",,"adj")
*************************************monthly
*!*	goffice='J'
*!*	useca("indet","wh",,,"mod='"+goffice+"' and  accountid in (6303,6325,6543) and date_rcvd>{"+dtoc(date()-60)+"}",,"indet")
*!*	goffice='J'
*!*	useca("inloc","wh",,,"mod='"+goffice+"' and  accountid in (6303,6325,6543) and adddt>{"+dtoc(date()-60)+"}",,"inloc")
*!*	SELECT DATE_RCVD, style,color,id,whseloc,locqty FROM indet a LEFT JOIN inloc b ON a.indetid=b.indetid INTO CURSOR vin READWRITE
*!*	SELECT * FROM vin WHERE 'QC' $(WHSELOC) INTO CURSOR VIN1 READWRITE


if xday =1
*********Capture receivings directly into QC
*!*		goffice='J'
*!*		useca("inwolog","wh",,,"mod='"+goffice+"' and  accountid in (6303,6325,6543) and confirmdt>{"+dtoc(date()-60)+"}",,"inwolog")
*!*		select accountid,wo_num,confirmdt from inwolog into cursor vinwo readwrite
*!*		useca("rcvdet","wh",,,"mod='"+goffice+"' and  accountid in (6303,6325,6543) and adddt>{"+dtoc(date()-60)+"}",,"rcvdet")
*!*		select a.accountid,rcvdetid,confirmdt, a.wo_num,style,color,id,totqty from vinwo a left join rcvdet b on a.wo_num=b.wo_num into cursor vrcv1 readwrite
*!*		useca("rcvloc","wh",,,"mod='"+goffice+"' and  accountid in (6303,6325,6543) and adddt>{"+dtoc(date()-60)+"}",,"rcvloc")




*!*		select distinct a.accountid,confirmdt,a.wo_num,style,color,id,totqty, whseloc  from vrcv1 a left join rcvloc b on a.rcvdetid=b.rcvdetid into cursor vrcv2 readwrite
*!*		select accountid, confirmdt,   style,color,id,totqty,whseloc,  wo_num from vrcv2 where'QC' $(whseloc) into cursor vrcv3a readwrite &&from rcv only

*!*		select accountid,confirmdt, wo_num from vrcv2 where isnull(whseloc) into cursor vrcv3b readwrite

*!*		useca("indet","wh",,,"mod='"+goffice+"' and  accountid in (6303,6325,6543) and date_rcvd>{"+dtoc(date()-60)+"}",,"indet")
*!*		select a.*,indetid, style,color,id,totqty from vrcv3b a left join indet b on a.wo_num=b.wo_num into cursor vrcv4b readwrite
*!*		useca("inloc","wh",,,"mod='"+goffice+"' and  accountid in (6303,6325,6543) and adddt>{"+dtoc(date()-60)+"}",,"inloc")
*!*		select a.accountid, confirmdt, style,color,id,totqty,whseloc,a.wo_num from vrcv4b a left join inloc b on a.indetid=b.indetid into cursor vrcv5b readwrite
*!*		select * from vrcv5b where 'QC' $(whseloc) into cursor vrcv6b readwrite
*!*		select *,space(50) as comment from vrcv3a union all select*,space(50) as comment from vrcv6b into cursor rcvfinal readwrite
*!*		replace comment with 'Received on WO '+trans(wo_num) for !empty(wo_num) in rcvfinal
*!*		select accountid,ttod(confirmdt) as trans_date, style,color,id,totqty,whseloc,comment from rcvfinal into cursor rcvfinal2 readwrite
	
	
	
	select accountid,adddt,style,color,id,totqty, whseloc,comment from adj where ('QC' $(whseloc) OR whseloc='MJHOLD' OR whseloc='MJSPEC') and month(adjdt)=xmonth and year(adjdt)=xyear and office='N' and mod='J'  AND !adjinv into cursor temp556
SET STEP ON 
	select accountid,adddt as trans_date, style,color,id,totqty, whseloc,comment,space(5) as qty from temp556 into cursor temp557 readwrite
	scan
		pos1=atc('UNI',comment)
		replace qty with substr(comment,1,pos1-1)
	endscan
	replace totqty with val(qty) for totqty=0
	select accountid,ttod(trans_date) as trans_date, style,color,id,totqty,whseloc,comment from temp557 into cursor temp558 readwrite
	REPLACE totqty WITH totqty*-1 FOR totqty<0 IN temp558
*!*		select * from rcvfinal2 union all select * from temp558 into cursor final readwrite
	select * from temp558 where month(trans_date)=xmonth and year(trans_date)=xyear into cursor final2 readwrite

*!*		select * from temp556 where totqty!=0 into cursor temp566
*!*		select * from temp566 a left join adj b on a.accountid=b.accountid and a.style=b.style and a.color=b.color and a.id=b.id and(a.adddt=b.adddt+1 or  a.adddt=b.adddt-1 or a.adddt=b.adddt) and (a.totqty)=((b.totqty)*-1) into cursor temp67
*!*		select accountid_b as accountid,adddt_b as adddt,style_b as style,color_b as color,id_b as id,totqty_b as totqty,whseloc_b as whseloc, comment_b as comment from temp67 into cursor temp68
*!*		select * from temp566 union all select * from temp68 order by adddt,style,color,id into cursor temp69 readwrite
*!*		select * from temp69 where !isnull(accountid) into cursor temp70

SET STEP ON 
	if reccount() > 0
		*export to "S:\MarcJacobsData\TEMP\mj_qc_adj"  type xls
		export to "S:\MarcJacobsData\TEMP\mj_qc_adj"  TYPE XL5  && 4/2/2018 MB

*		tsendto = "chris.malcolm@tollgroup.com"
		tsendto = "MJTollQCAdjustments@marcjacobs.com, Chris.Malcolm@Tollgroup.com, Carolyn.Balcezak@Tollgroup.com"   && 3/1/2018 MB: added Chris and Carolyn per Chris.
		tattach = "S:\MarcJacobsData\TEMP\mj_qc_adj.xls"
		tcc ="mbennett@fmiint.com"
		tfrom ="Toll EDI System Operations <fmi-transload-ops@fmiint.com>"
		tmessage = "MJ QC ADJUSTMENTS MONTHLY:  "+ttoc(datetime())
		tsubject = "MJ QC ADJUSTMENTS MONTHLY"
		do form m:\dev\frm\dartmail2 with tsendto,tfrom,tsubject, tcc,tattach,tmessage  ,"A"

	else
	endif
else
endif
*************************************weekly
if xdow='Sunday'
	select accountid,adddt,style,color,id,totqty, whseloc,comment from adj WHERE ('QC' $(whseloc) OR whseloc='MJHOLD' OR whseloc='MJSPEC')  and (adjdt)>=date()-7  and office='N' and mod='J'  into cursor temp556
*!*		select * from temp556 where totqty!=0 into cursor temp566
*!*		select * from temp566 a left join adj b on a.accountid=b.accountid and a.style=b.style and a.color=b.color and a.id=b.id and(a.adddt=b.adddt+1 or  a.adddt=b.adddt-1 or a.adddt=b.adddt) and (a.totqty)=((b.totqty)*-1) into cursor temp67
*!*		select accountid_b as accountid,adddt_b as adddt,style_b as style,color_b as color,id_b as id,totqty_b as totqty,whseloc_b as whseloc, comment_b as comment from temp67 into cursor temp68
*!*		select * from temp566 union all select * from temp68 order by adddt,style,color,id into cursor temp69 readwrite
*!*		select * from temp69 where !isnull(accountid) into cursor temp70
	if reccount() > 0
		*export to "S:\MarcJacobsData\TEMP\mj_qc_adj_wkly"  type xls
		export to "S:\MarcJacobsData\TEMP\mj_qc_adj_wkly"  type XL5  && 4/2/2018 MB

*		tsendto = "E.SKRZYNIARZ@marcjacobs.com"
		tsendto = "MJTollQCAdjustments@marcjacobs.com"
		tattach = "S:\MarcJacobsData\TEMP\mj_qc_adj_wkly.xls"
		tcc =""
		tfrom ="Toll EDI System Operations <fmi-transload-ops@fmiint.com>"
		tmessage = "MJ QC ADJUSTMENTS WEEKLY:  "+ttoc(datetime())
		tsubject = "MJ QC ADJUSTMENTS WEEKLY"
		do form m:\dev\frm\dartmail2 with tsendto,tfrom,tsubject, tcc,tattach,tmessage  ,"A"

	else
*		tsendto = "E.SKRZYNIARZ@marcjacobs.com"
		tsendto = "MJTollQCAdjustments@marcjacobs.com"
		tattach = ""
		tcc =""
		tfrom ="Toll EDI System Operations <fmi-transload-ops@fmiint.com>"
		tmessage = "NO MJ QC ADJUSTMENTS WEEKLY:  "+ttoc(datetime())
		tsubject = "NO MJ QC ADJUSTMENTS WEEKLY"
		do form m:\dev\frm\dartmail2 with tsendto,tfrom,tsubject, tcc,tattach,tmessage  ,"A"
	endif
else
endif

*************************************DAILY
select accountid,adddt,style,color,id,totqty, whseloc,comment from adj where  ('QC' $(whseloc) OR whseloc='MJHOLD' OR whseloc='MJSPEC')  and (adjdt)>=date()  and office='N' and mod='J'  into cursor temp556
*!*		select * from temp556 where totqty!=0 into cursor temp566
*!*		select * from temp566 a left join adj b on a.accountid=b.accountid and a.style=b.style and a.color=b.color and a.id=b.id and(a.adddt=b.adddt+1 or  a.adddt=b.adddt-1 or a.adddt=b.adddt) and (a.totqty)=((b.totqty)*-1) into cursor temp67
*!*		select accountid_b as accountid,adddt_b as adddt,style_b as style,color_b as color,id_b as id,totqty_b as totqty,whseloc_b as whseloc, comment_b as comment from temp67 into cursor temp68
*!*		select * from temp566 union all select * from temp68 order by adddt,style,color,id into cursor temp69 readwrite
*!*		select * from temp69 where !isnull(accountid) into cursor temp70
if reccount() > 0
	*export to "S:\MarcJacobsData\TEMP\mj_qc_adj_dly"  type xls
	export to "S:\MarcJacobsData\TEMP\mj_qc_adj_dly"  type XL5  && 4/2/2018 MB

*	tsendto = "E.SKRZYNIARZ@marcjacobs.com,M.deLaCruz@marcjacobs.com"
	tsendto = "MJTollQCAdjustments@marcjacobs.com"
	tattach = "S:\MarcJacobsData\TEMP\mj_qc_adj_dly.xls"
	tcc =""
	tfrom ="Toll EDI System Operations <fmi-transload-ops@fmiint.com>"
	tmessage = "MJ QC ADJUSTMENTS DAILY:  "+ttoc(datetime())
	tsubject = "MJ QC ADJUSTMENTS DAILY"
	do form m:\dev\frm\dartmail2 with tsendto,tfrom,tsubject, tcc,tattach,tmessage  ,"A"
else
*	tsendto = "E.SKRZYNIARZ@marcjacobs.com"
	tsendto = "MJTollQCAdjustments@marcjacobs.com"
	tattach = ""
	tcc =""
	tfrom ="Toll EDI System Operations <fmi-transload-ops@fmiint.com>"
	tmessage = "NO MJ QC ADJUSTMENTS DAILY:  "+ttoc(datetime())
	tsubject = "NO MJ QC ADJUSTMENTS DAILY"
	do form m:\dev\frm\dartmail2 with tsendto,tfrom,tsubject, tcc,tattach,tmessage  ,"A"
endif


close data all

wait window at 10,10 "all done...." timeout 2


schedupdate()
_screen.caption=gscreencaption
on error

