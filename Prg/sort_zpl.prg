*Set Step On
*Close Data All

Create Cursor test (Style Char(20),dcname Char(20),ctncount n(4),Label Memo)

Cd "f:\searslabels\"

Select test
tfile = Getfile()
fhand = Fopen(tfile)
ictr = 1

lcHdrStr = ""

For i = 1 To 13
  lcStr = Fgets(fhand,500)
  lcHdrStr = lcHdrStr+Chr(13)
Next i
llstock = .F.

lcStyle=""
dcname=""

Do While !Feof(fhand)
  lcstr = ""
  lcS =""
  lnNum = 35

*  For i = 1 To lnNum

  i=1
  lnCtncount =1

  Do While lcS != "^XZ"

    lcS = Fgets(fhand,500)

    If Feof(fhand)
      Exit
    Endif

    If "^XA^EG^XZ"$lcS
      For ii = 1 To 12
        lcS = Fgets(fhand,500)
      Next II
    Endif

    If "FT0747"$lcS
      lcdcname = Substr(lcS,30,3)
    Endif

*!*      If "FDKSN "$lcS
*!*        lcStyle = Substr(lcS,34,8)
*!*      Endif

    If "FDKSNPID"$lcS
      lcStyle = Substr(lcS,37,8)
    Endif

    If "Cartons"$lcS
      llStock = .T.
*      lcStyle= "STOCK"
      lcDCName= "STOCK"
      lcDCName = "Stock "+Transform(Val(Substr(lcS,41,8)))+ " Carton(s)"
      lnCtncount =Val(Substr(lcS,41,8))

    Endif

    lcStr = lcstr+lcS+Chr(13)
*Next i
  Enddo

  If !Empty(lcStyle)
    Select test
    Append Blank

    If llStock
      Replace Style With lcStyle &&"STOCK"
      Replace dcname With lcDcName &&"STOCK"
      replace ctncount With lnctncount
    Else
      Replace Style  With lcStyle
      Replace dcname With lcDcname
      replace ctncount With 1
    Endif

    Replace Label With lcStr

  Endif
  Wait Window At 10,10 "record# "+Transform(ictr) Nowait

  llStock = .F.

  lcStyle =""
  lcdcname = ""
  lcStr = ""
  lnCtncount =1

Enddo

Fclose(fhand)

Select test
Index On Style+dcname Tag Style

*Browse


xfile = "f:\searslabels\sorted\"+Justfname(tfile)

fhand2 = Fcreate(xfile)

Set Step On 
lcStart = Filetostr("f:\wh\zpltemplates\containerstart.txt")
lcStart=Strtran(lcStart,"<CONTAINER>",Justfname(tfile))
Fputs(fhand2,lcStart)

Select test
Scan
  lcStr = test.Label
  Fputs(fhand2,lcStr)
Endscan

lcStart = Filetostr("f:\wh\zpltemplates\containerend.txt")
lcStart=Strtran(lcStart,"<CONTAINER>",Justfname(tfile))
Fputs(fhand2,lcStart)

Fclose(fhand2)

*return

Set Step On 


Copy File &tfile to "f:\searslabels\archive\"+Justfname(tfile)
Delete File &tfile

Select test

Select Count(1) as CTNQTY,ctncount,style,dcname From test Group By style,dcname Into Cursor temp readwrite

Select temp
Scan For "Stock"$dcname
  replace ctnqty With ctncount
Endscan

Export Fields ctnqty,style,dcname To "f:\searslabels\sorted\xlsfiles\"+Justfname(tfile)+".xls" Type xls

Return
