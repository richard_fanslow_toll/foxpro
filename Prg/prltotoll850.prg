**Polo Ralph Lauren 850 file translation Icon
**850 files being pulled from ftp site in task poller via FTP_GETFILES
**

Wait "Polo 850 Processing" Window Nowait Noclear

**go to production - mvw 09/11/14
xtest=.F.

xindir="f:\ftpusers\poloralphlauren\850in\"
xoutdir=Iif(xtest,"f:\ftpusers\toll\poloralphlauren\850test\","f:\ftpusers\toll\poloralphlauren\850out\")

**check if files exist in the inbound folder
**looks like sap files will end in ".sap.002" and legacy files ".legacy.000" - but awaiting confirmation
xfilecnt=Adir(afiles,xindir+"*.00*")
If xfilecnt=0
  Wait Clear
  Return
Endif

xseparator=Chr(13)+Chr(10)
xcolcnt=25
xtollcustcode="POLO" &&temporarily entered as "PVH" for testing

If Empty(xtollcustcode) Or xtollcustcode="POLO"
  xFrom="TGFSYSTEM"
  If xtollcustcode="POLO"
    xSubject="POLO 850 ICON Transalation May Have A Test Toll Customer Code!! Must Change Before Going Live!"
  Else
    xSubject="POLO 850 ICON Transalation Cannot Be Run - Empty Toll Customer Code!!"
  Endif
  xbody=""
  Do Form dartmail2 With "mwinter@fmiint.com",xfrom,xsubject," ",,xbody,"a"

  If Empty(xtollcustcode)
    Wait Clear
    Return
  Endif
Endif

Set Century On
Set Hours To 24

Create Cursor xfilelist (File c(100), x c(10), moddt C(10), modtm c(10), moddttm T(1))
Append From Array afiles
Replace All moddttm With Ctot(moddt+" "+modtm)

**997 code, unneded for Polo at this point
*!*	scan
*!*		xfile=alltrim(xfilelist.file)
*!*		xinfile=xindir+alltrim(xfilelist.file)
*!*		xfs=filetostr(xinfile)

*!*		if "ST*997*"$xfs
*!*			**save in 997in in case we need them
*!*			copy file &xinfile to &x997in.&xfile
*!*		endif

*!*		**need to account for possibility of having 997s and 850s in same file. if not 850s, delete. else, process normally - mvw 11/08/12
*!*		if "ST*850*"$xfs
*!*			**copy ONLY 850 files to the 997 directory so 997s are sent as needed - mvw 06/08/12
*!*			copy file &xinfile to &x997dir.&xfile
*!*		else
*!*			**if just 997, delete
*!*			delete file &xinfile
*!*			delete in xfilelist
*!*		endif
*!*	endscan

Use F:\sysdata\qqdata\unloccode In 0
use f:\ediexport\data\podata in 0

select * from podata where .f. into cursor xpodata readwrite

Select 0
Create Cursor x850 (field1 c(254),Recno N(14,3))
Index On Recno Tag Recno

xfilecreated=.F.
Select * From xfilelist Order By moddttm Into Cursor xfilelist
Scan
  zap in xpodata
  zap in x850

**start with column headings
  xfilestr="order_no|order_split|order_status|client_cust_code|supplier_code|supplier_name|supplier_address_1|"+;
  "supplier_address_2|supplier_city|supplier_region_state|supplier_country|supplier_postcode|confirm_number|"+;
  "confirm_date|exw_required_by|delivery_required_by|description|order_date_time|order_value|order_currency_code|"+;
  "exchange_rate|incoterms|additional_terms|transport_mode|container_mode|milestone_tracking_date_exw|"+;
  "milestone_tracking_date_etd|milestone_tracking_date_eta|custom_milestone_est1|custom_milestone_act1|"+;
  "custom_milestone_est2|custom_milestone_act2|custom_milestone_est3|custom_milestone_act3|custom_milestone_est4|"+;
  "custom_milestone_act4|sending_agent|sending_agent_name|origin_port|destination_port|custom_text1|custom_text2|"+;
  "custom_text3|custom_text4|custom_text5|custom_date1|custom_date2|custom_decimal1|custom_decimal2|custom_decimal3|"+;
  "custom_decimal4|custom_decimal5|custom_flag1|custom_flag2|custom_flag3|custom_flag4|custom_flag5|contact1|"+;
  "contact2|country_code|notes|line_no|subline_no|linesplit_no|product|line_description|qty_ordered|innerpacks|"+;
  "outerpacks|unit_of_measure|itemprice|lineprice|linestatus|required_date|part_attrib1|part_attrib2|part_attrib3|"+;
  "line_custom_attrib1|line_custom_attrib2|line_custom_attrib3|line_custom_attrib4|line_custom_attrib5|"+;
  "line_custom_date1|line_custom_date2|line_custom_date3|line_custom_date4|line_custom_date5|line_custom_decimal1|"+;
  "line_custom_decimal2|line_custom_decimal3|line_custom_decimal4|line_custom_decimal5|line_custom_flag1|"+;
  "line_custom_flag2|line_custom_flag3|line_custom_flag4|line_custom_flag5|line_custom_text1|container_number|"+;
  "container_packing_order|country_of_origin|dg_substance|dg_flashpoint|weight|volume|weight_type|volume_type|"+;
  "special_instructions|additional_information|delivery_port|address_code|delivery_address_name|"+;
  "delivery_address_address1|delivery_address_address2|delivery_address_city|delivery_address_state|"+;
  "delivery_address_country|delivery_address_postcode"+Chr(13)+Chr(10)
  xhdrlen=Len(xfilestr)
  xfilename = xindir+Alltrim(xfilelist.File)
  xarchiveinfile = xindir+"archive\"+Alltrim(xfilelist.File)

**no need for temp file, etc if terminator segment is already chr(13)+chr(10), looks at pvhtotoll850.prg for that code
  xfile=Filetostr(xfilename)

**for test file with chr(13)+chr(10) segment delimiter
*xchar=chr(13)+chr(10)
  xchar=Substr(xfile,106,1)

  xfile=Strtran(xfile,xchar,Chr(13)+Chr(10))

  xtmpfile=xindir+"tmp"+Strtran(Strtran(Strtran(Ttoc(Datetime()),"/",""),":","")," ","")+".tmp"
  Strtofile(xfile,xtmpfile)

  xchar=Chr(13)+Chr(10)

  Select x850

**added try catch in attempt to avoid "file access denied" error, believe its coming
**  from the append if the file has yet to be completely ftp'd - mvw 06/29/12
  xerror=.F.
  Try
    Append From "&xtmpfile" Type Delimited With Character &xchar
  Catch
    xerror=.T.
  Endtry

  Replace All field1 With Alltrim(Upper(field1)), Recno With Recno()
  Locate

  Delete File &xtmpfile

**default delivery port UNLOC code required for Icon - default to NYC
  xdelivport="USNYC"

**keep track of previous po line as we have seen instances where PVH has sent the same line number
**	multiple times, each containing different skus - mvw 02/14/12
  xprevlineno=""
**need sln to be defined here now in case i need to combine 2 identical line numbers for same po - mvw 2/14/12
  xslncnt=1

**string to hold the data for each po as we need to only add to xfilestr once a po is complete - mvw 01/16/13
  xpostr=""

  xerror=.F.
  Do While !Eof()
    Do Case
    Case Left(field1,3)="ISA"
**decide if SAP via ISA06 - value is ACUSAP or ACUCUBE
      xsap=Substr(field1,At("*",field1,6)+1,At("*",field1,7)-(At("*",field1,6)+1))
      xsap=Iif(xsap="ACUSAP",.T.,.F.)

    Case Left(field1,2)="GS"
    Case Left(field1,2)="ST"
**if encounter a 997, skip to next 850 "ST" seg - mvw 11/08/12
      If Substr(field1,4,3)="997"
        Locate For Left(field1,2)="ST" And Substr(field1,4,3)="850" While !Eof()
        Loop
      Endif

    Case Left(field1,3)="BEG"
      xlevel="O"
      xaction=Alltrim(Substr(field1,At("*",field1,1)+1,At("*",field1,2)-(At("*",field1,1)+1)))
**we expect only a 00 (new), 03 (delete) or 05 (replace)
      xaction=Iif(Inlist(xaction,"00","05"),"PLC",Iif(xaction="03","CAN",""))
      If Empty(xaction)
        xfrom ="TGFSYSTEM"
        xmessage = "Error in file '"+xfilename+"' for PO '"+xponum+"': Order status has unexpected value - expect only '00','03' or '05'. Entire file cannot be processed."
        Do Form dartmail2 With "mwinter@fmiint.com",xFrom,"Polo 850 Translation Error"," ","",xmessage,"A"

        xerror=.T.
        Exit
      Endif

      xponum=Substr(field1,At("*",field1,3)+1,At("*",field1,4)-(At("*",field1,3)+1))

      xorderdt=Right(Alltrim(field1),Len(Alltrim(field1))-At("*",field1,5))
**check that its a valid date (within 10 years)
      If !Between(Ctod(Substr(xorderdt,5,2)+"/"+Substr(xorderdt,7,2)+"/"+Left(xorderdt,4)),Gomonth(Date(),-(10*12)),Gomonth(Date(),10*12))
        xorderdt=Transform(Year(Date()))+"-"+Padl(Transform(Month(Date())),2,"0")+"-"+Padl(Transform(Day(Date())),2,"0")
      Else
        xorderdt=Left(xorderdt,4)+"-"+Substr(xorderdt,5,2)+"-"+Substr(xorderdt,7,2)
      Endif

**need to make sure we haven't received the same po twice in the same file - in some cases they will send it twice (different ISA envelopes)!
**  use the latter instance - mvw 10/09/13
      xrecno=Recno()
      Skip
      Locate For field1="BEG" And "*"+xponum+"*" $ field1 While !Eof()
**if found below, skip to next ST*850 rec
      If Found()
        Go xrecno
        Locate For Left(field1,2)="ST" And Substr(field1,4,3)="850" While !Eof()
        Loop
      Endif

      Go xrecno
**end changes for po multiple times within file

**taken from TD5 in the PO1 loop for Polo
*			xshipvia="SEA" &&hardcode SEA bc not provided

**default to USD, taken from CUR segment at either order or item level (legacy has only item, sap appears to have both)
      xcurrency="USD"

      xnotes=""

      Store "" To xcustomtext1, xcustomtext2, xcustomtext3, xcustomtext4, xcustomtext5

**default HEADER custom text and decimal fields
      Store "" To xhcustomtext1, xhcustomtext2, xhcustomtext3, xhcustomtext4, xhcustomtext5
      Store "" To xhcustomdecimal1, xhcustomdecimal2, xhcustomdecimal3, xhcustomdecimal4, xhcustomdecimal5

      xcustomtext3=xponum
      xhcustomtext4=Iif(xsap,"ACUSAP","ACUCUBE")
      xppk=.F.

      xcontractreqdt=""
      xordervalue=""
      xsendagentcode=""
      xsendagentname=""

**reset for every po change - mvw 02/14/12
      xprevlineno=""
      xpostr=""

      xshiptocode=""

**must get totqty from CTT segment
      xrecno=Recno()
      Locate For Left(field1,3)="CTT" While !Eof()
      xtotqty=Right(Alltrim(field1),Len(Alltrim(field1))-At("*",field1,2))
      Goto xrecno

    Case Left(field1,3)="REF" And Substr(field1,5,2)="K0"
      xcustomtext1=Right(Alltrim(field1),Len(Alltrim(field1))-At("*",field1,2))
**po num needs to include REF02 where REF01 = "K0"
      xponum=xponum+"_"+xcustomtext1

    Case Left(field1,3)="REF" And Substr(field1,5,2)="6O"
      xcustomtext2=Right(Alltrim(field1),Len(Alltrim(field1))-At("*",field1,2))

    Case Left(field1,3)="CUR"
      xcurrency=Right(Alltrim(field1),Len(Alltrim(field1))-At("*",field1,2))

    Case Left(field1,3)="REF" And Substr(field1,5,2)="MR"
      xpodesc=Right(Alltrim(field1),Len(Alltrim(field1))-At("*",field1,3))

    Case Left(field1,3)="REF" And Substr(field1,5,2)="PG" &&purchasing group to xhcustomtext5 - mvw 03/21/14
      xhcustomtext5=Right(Alltrim(field1),Len(Alltrim(field1))-At("*",field1,3))

    Case Left(field1,3)="MTX" And Empty(xnotes) &&only want to grab first MTX segment, there are many!
      xnotes=Right(Alltrim(field1),Len(Alltrim(field1))-At("*",field1,2))

    Case Left(field1,3)="CTP" And Substr(field1,5,2)="RS" And Substr(field1,8,3)="MSR"
      xordervalue=Right(Alltrim(field1),Len(Alltrim(field1))-At("*",field1,3))

    Case Left(field1,3)="DTM" And Substr(field1,5,3)="065"
      xcontractreqdt=Right(Alltrim(field1),Len(Alltrim(field1))-At("*",field1,2))

**check that its a valid date (within 10 years), if not enter as today's date
      If !Between(Ctod(Substr(xcontractreqdt,5,2)+"/"+Substr(xcontractreqdt,7,2)+"/"+Left(xcontractreqdt,4)),Gomonth(Date(),-(10*12)),Gomonth(Date(),10*12))
        xcontractreqdt=Transform(Year(Date()))+"-"+Padl(Transform(Month(Date())),2,"0")+"-"+Padl(Transform(Day(Date())),2,"0")
      Else
        xcontractreqdt=Left(xcontractreqdt,4)+"-"+Substr(xcontractreqdt,5,2)+"-"+Substr(xcontractreqdt,7,2)
      Endif

**for legacy system, need to know if prepack to determine where to get style number from
    Case Left(field1,3)="PID" And xlevel="O"
      xvalue=Alltrim(Substr(field1,At("*",field1,7)+1,At("*",field1,8)-(At("*",field1,7)+1)))
      If Occurs("*",field1)=7
        xvalue=Right(Alltrim(field1),Len(Alltrim(field1))-At("*",field1,7))
      Endif

      If xvalue="PPK"
        xppk=Alltrim(Substr(field1,At("*",field1,4)+1,At("*",field1,5)-(At("*",field1,4)+1)))
        xppk=Iif(xppk="N",.F.,.T.)
      Endif

    Case Left(field1,2)="N1" And (Substr(field1,4,2)="AG" Or Substr(field1,4,3)="LYO")
**sending agent name and code
      Do Case
      Case Occurs("*",field1)=4
        xsendagentcode=Right(Alltrim(field1),Len(Alltrim(field1))-At("*",field1,4))
        xsendagentname=Alltrim(Substr(field1,At("*",field1,2)+1,At("*",field1,3)-(At("*",field1,2)+1)))
      Case Occurs("*",field1)=2
        xsendagentname=Right(Alltrim(field1),Len(Alltrim(field1))-At("*",field1,2))
      Endcase

    Case Left(field1,2)="N1" And Substr(field1,4,2)="BY"
**buyer name and code need to be sent back in the asn, save in contact1 (BF) and contact2 (BG)
      xbuyercode=Right(Alltrim(field1),Len(Alltrim(field1))-At("*",field1,4))
      xbuyername=Alltrim(Substr(field1,At("*",field1,2)+1,At("*",field1,3)-(At("*",field1,2)+1)))

      xhcustomtext3=xbuyername

    Case Left(field1,2)="N1" And Substr(field1,4,2)="DV"
      xcustomtext4=Alltrim(Substr(field1,At("*",field1,2)+1,At("*",field1,3)-(At("*",field1,2)+1)))

    Case Left(field1,2)="N1" And Substr(field1,4,2)="SF"
      xshipfromcode=Right(Alltrim(field1),Len(Alltrim(field1))-At("*",field1,4))
      xshipfromname=Alltrim(Substr(field1,At("*",field1,2)+1,At("*",field1,3)-(At("*",field1,2)+1)))

      Skip

      If Left(field1,2)="N2" &&if include an N2, skip past
        Skip
      Endif

      Store "" To xshipfromaddr1, xshipfromaddr2
      If Left(field1,2)="N3" &&should have N3 but if not, leave blank
        If Occurs("*",field1)=1
          xshipfromaddr1=Right(Alltrim(field1),Len(Alltrim(field1))-At("*",field1,1))
        Else
          xshipfromaddr1=Alltrim(Substr(field1,At("*",field1,1)+1,At("*",field1,2)-(At("*",field1,1)+1)))
          xshipfromaddr2=Right(Alltrim(field1),Len(Alltrim(field1))-At("*",field1,2))
        Endif
        Skip
      Endif

      If Left(field1,2)="N3" &&if 2nd N3, skip past
        Skip
      Endif

      Store "" To xshipfromcity, xshipfromstate, xshipfromzip, xshipfromcountry, xfobloccode
      If Left(field1,2)#"N4"
        Skip -1 &&if for some reason not provided, skip back bc there is a skip at end of loop
      Else
        xshipfromcity=Alltrim(Substr(field1,At("*",field1,1)+1,At("*",field1,2)-(At("*",field1,1)+1)))
**if N4 comes only with city, need to use the right() function below - applies to other fields below as well
        If Occurs("*",field1)=1
          xshipfromcity=Right(Alltrim(field1),Len(Alltrim(field1))-At("*",field1,1))
        Endif

        xshipfromstate=Alltrim(Substr(field1,At("*",field1,2)+1,At("*",field1,3)-(At("*",field1,2)+1)))
        If Occurs("*",field1)=2
          xshipfromstate=Right(Alltrim(field1),Len(Alltrim(field1))-At("*",field1,2))
        Endif
        xshipfromstate=Iif(Inlist(xshipfromstate,"N/","N\","NA"),"",xshipfromstate)

        xshipfromzip=Left(Alltrim(Substr(field1,At("*",field1,3)+1,At("*",field1,4)-(At("*",field1,3)+1))),5)
        If Occurs("*",field1)=3
          xshipfromzip=Left(Right(Alltrim(field1),Len(Alltrim(field1))-At("*",field1,3)),5)
        Endif
        xshipfromzip=Iif(Inlist(Upper(xshipfromzip),"N/A","NA","N\A"),"",xshipfromzip)

        xshipfromcountry=Alltrim(Substr(field1,At("*",field1,4)+1,At("*",field1,5)-(At("*",field1,4)+1)))
        If Occurs("*",field1)=4
          xshipfromcountry=Right(Alltrim(field1),Len(Alltrim(field1))-At("*",field1,4))
        Endif

**change to 3-digit iso country (unloccode.isocountry) - mvw 12/07/11
        xshipfromcountry2=xshipfromcountry &&save 2-digit country code, may be used
        =Seek(xshipfromcountry,"unloccode","country")
        xshipfromcountry=unloccode.isocountry
      Endif

      If !xsap
**per Bill L, if N401 is empty, default city to N301 (xsupplieraddr1)
        If Empty(xshipfromcity)
          xshipfromcity=xshipfromaddr1
        Endif
**per Bill L, if N402 is empty, default state to N302 (xsupplieraddr2)
        If Empty(xshipfromstate)
          xshipfromstate=xshipfromaddr2
        Endif
      Endif


    Case Left(field1,2)="N1" And Substr(field1,4,2)="SU"
      xsuppliercode=Right(Alltrim(field1),Len(Alltrim(field1))-At("*",field1,4))
      xsuppliername=Alltrim(Substr(field1,At("*",field1,2)+1,At("*",field1,3)-(At("*",field1,2)+1)))

      Skip

      If Left(field1,2)="N2" &&if include an N2, skip past
        Skip
      Endif

      Store "" To xsupplieraddr1, xsupplieraddr2
      If Left(field1,2)="N3" &&should have N3 but if not, leave blank
        If Occurs("*",field1)=1
          xsupplieraddr1=Right(Alltrim(field1),Len(Alltrim(field1))-At("*",field1,1))
        Else
          xsupplieraddr1=Alltrim(Substr(field1,At("*",field1,1)+1,At("*",field1,2)-(At("*",field1,1)+1)))
          xsupplieraddr2=Right(Alltrim(field1),Len(Alltrim(field1))-At("*",field1,2))
        Endif
        Skip
      Endif

      If Left(field1,2)="N3" &&if 2nd N3, skip past
        Skip
      Endif

      Store "" To xsuppliercity, xsupplierstate, xsupplierzip, xsuppliercountry, xfobloccode
      If Left(field1,2)#"N4"
        Skip -1 &&if for some reason not provided, skip back bc there is a skip at end of loop
      Else
        xsuppliercity=Alltrim(Substr(field1,At("*",field1,1)+1,At("*",field1,2)-(At("*",field1,1)+1)))
**if N4 comes only with city, need to use the right() function below - applies to other fields below as well
        If Occurs("*",field1)=1
          xsuppliercity=Right(Alltrim(field1),Len(Alltrim(field1))-At("*",field1,1))
        Endif

        xsupplierstate=Alltrim(Substr(field1,At("*",field1,2)+1,At("*",field1,3)-(At("*",field1,2)+1)))
        If Occurs("*",field1)=2
          xsupplierstate=Right(Alltrim(field1),Len(Alltrim(field1))-At("*",field1,2))
        Endif
        xsupplierstate=Iif(Inlist(xsupplierstate,"N/","N\","NA"),"",xsupplierstate)

        xsupplierzip=Left(Alltrim(Substr(field1,At("*",field1,3)+1,At("*",field1,4)-(At("*",field1,3)+1))),5)
        If Occurs("*",field1)=3
          xsupplierzip=Left(Right(Alltrim(field1),Len(Alltrim(field1))-At("*",field1,3)),5)
        Endif
        xsupplierzip=Iif(Inlist(Upper(xsupplierzip),"N/A","NA","N\A"),"",xsupplierzip)

        xsuppliercountry=Alltrim(Substr(field1,At("*",field1,4)+1,At("*",field1,5)-(At("*",field1,4)+1)))
        If Occurs("*",field1)=4
          xsuppliercountry=Right(Alltrim(field1),Len(Alltrim(field1))-At("*",field1,4))
        Endif

        Select unloccode
        Locate For Name=xsuppliercity And country=xsuppliercountry
        If Found()
          xfobloccode=Alltrim(country)+Alltrim(unloccode)
        Endif

        Select x850
**change to 3-digit iso country (unloccode.isocountry) - mvw 12/07/11
        xsuppliercountry2=xsuppliercountry &&save 2-digit country code, may be used
        =Seek(xsuppliercountry,"unloccode","country")
        xsuppliercountry=unloccode.isocountry
      Endif

      If !xsap
**per Bill L, if N401 is empty, default city to N301 (xsupplieraddr1)
        If Empty(xsuppliercity)
          xsuppliercity=xsupplieraddr1
        Endif
**per Bill L, if N402 is empty, default state to N302 (xsupplieraddr2)
        If Empty(xsupplierstate)
          xsupplierstate=xsupplieraddr2
        Endif
      Endif

    Case Left(field1,2)="N1" And Substr(field1,4,2)="BG"
**per Gianfranco, xshiptocode will now be a concatention of N104 where N1*BG + "-" + N104 where N1*WD - mvw 03/03/15
      xshiptocode=Right(Alltrim(field1),Len(Alltrim(field1))-At("*",field1,4))
    Case Left(field1,2)="N1" And Substr(field1,4,2)="WD"
**per Gianfranco, xshiptocode will now be a concatention of N104 where N1*BG + "-" + N104 where N1*WD - mvw 03/03/15
      xshiptocode=Alltrim(xshiptocode)+"-"+Right(Alltrim(field1),Len(Alltrim(field1))-At("*",field1,4))

    Case Left(field1,2)="N1" And Substr(field1,4,2)="ST"
**per Gianfranco, xshiptocode will now be a concatention of N104 where N1*BG + "-" + N104 where N1*WD - mvw 03/03/15
**if the BG & WD segments are missing, use the N1*ST segment (for POs out of Polo's legacy system) - mvw 06/25/15
      If Type("xshiptocode")="U" Or Empty(xshiptocode)
        xshiptocode=Right(Alltrim(field1),Len(Alltrim(field1))-At("*",field1,4))
      Endif

      xshiptoname=Alltrim(Substr(field1,At("*",field1,2)+1,At("*",field1,3)-(At("*",field1,2)+1)))

      Skip

      If Left(field1,2)="N2" &&if include an N2, skip past
        Skip
      Endif

      Store "" To xshiptoaddr1, xshiptoaddr2
      If Left(field1,2)="N3" &&should have N3 but if not, leave blank
        If Occurs("*",field1)=1
          xshiptoaddr1=Right(Alltrim(field1),Len(Alltrim(field1))-At("*",field1,1))
        Else
          xshiptoaddr1=Alltrim(Substr(field1,At("*",field1,1)+1,At("*",field1,2)-(At("*",field1,1)+1)))
          xshiptoaddr2=Right(Alltrim(field1),Len(Alltrim(field1))-At("*",field1,2))
        Endif
        Skip
      Endif

      If Left(field1,2)="N3" &&if 2nd N3, skip past
        Skip
      Endif

      Store "" To xshiptocity, xshiptostate, xshiptozip, xshiptocountry
      If Left(field1,2)#"N4"
        Skip -1 &&if for some reason not provided, skip back bc there is a skip at end of loop
      Else
        xshiptocity=Alltrim(Substr(field1,At("*",field1,1)+1,At("*",field1,2)-(At("*",field1,1)+1)))
**if N4 comes only with city, need to use the right() function below - applies to other fields below as well
        If Occurs("*",field1)=1
          xshiptocity=Right(Alltrim(field1),Len(Alltrim(field1))-At("*",field1,1))
        Endif

        xshiptostate=Alltrim(Substr(field1,At("*",field1,2)+1,At("*",field1,3)-(At("*",field1,2)+1)))
        If Occurs("*",field1)=2
          xshiptostate=Right(Alltrim(field1),Len(Alltrim(field1))-At("*",field1,2))
        Endif
        xshiptostate=Iif(Inlist(xshiptostate,"N/","N\","NA"),"",xshiptostate)

        xshiptozip=Left(Alltrim(Substr(field1,At("*",field1,3)+1,At("*",field1,4)-(At("*",field1,3)+1))),5)
        If Occurs("*",field1)=3
          xshiptozip=Left(Right(Alltrim(field1),Len(Alltrim(field1))-At("*",field1,3)),5)
        Endif
        xshiptozip=Iif(Inlist(Upper(xshiptozip),"N/A","NA","N\A"),"",xshiptozip)

**if doesnt contain 4 *'s, then country is not provided. if so default to US - mvw 11/29/11
        xshiptocountry=Iif(Occurs("*",field1)<4,"US",Right(Alltrim(field1),Len(Alltrim(field1))-At("*",field1,4)))
**change to 3-digit iso country (unloccode.isocountry) - mvw 12/07/11
        =Seek(xshiptocountry,"unloccode","country")
        xshiptocountry=unloccode.isocountry
      Endif

**for ACUCUBE, get ship to city state from N302 (xshiptoaddr2), separated by comma or space
      If !xsap
        Do Case
        Case Occurs(",",xshiptoaddr2)>0
          xshiptocity=Alltrim(Left(xshiptoaddr2,At(",",xshiptoaddr2)-1))
          xshiptostate=Alltrim(Right(xshiptoaddr2,Len(xshiptoaddr2)-At(",",xshiptoaddr2)))
        Case Occurs(" ",xshiptoaddr2)>0 &&if no comma, use last space as divider
          xspacecnt=Occurs(" ",xshiptoaddr2)
          xshiptocity=Alltrim(Left(xshiptoaddr2,At(" ",xshiptoaddr2,xspacecnt)-1))
          xshiptostate=Alltrim(Right(xshiptoaddr2,Len(xshiptoaddr2)-At(" ",xshiptoaddr2,xspacecnt)))
        Otherwise
          xshiptocity=xshiptoaddr2
        Endcase
        xshiptoaddr2=""
      Endif

**start item level
    Case Left(field1,3)="PO1"
      xlevel="I"
      xlineno=Alltrim(Substr(field1,At("*",field1,1)+1,At("*",field1,2)-(At("*",field1,1)+1)))
**remove the " - ", needs to be numeric going into ICON
**NO LONGER REMOVE THE " - " per Andi - 04/18/12
*			xlineno=strtran(strtran(xlineno,"-","")," ","")

**if duplicate line number for a given po, need to correct - mvw 02/14/12
      If xprevlineno==xlineno
**error out if duplicate lines - send email, move to next po, continue with file - mvw 01/16/13
        xfrom ="TGFSYSTEM"
        xmessage = "Error in file '"+xfilename+"' for PO '"+xponum+"': At least 1 duplicate PO line - "+xlineno+;
        chr(13)+Chr(13)+"Please note, all other POs in this file have been processed."
        Do Form dartmail2 With "mwinter@fmiint.com, rebecca.h.ma@tollgroup.com",xFrom,"Polo 850 Translation Error"," ","",xmessage,"A"

        xpostr=""
**cleaner to go to ST element and loop here rather than run thru the rest of this loop - mvw 06/28/13
*!*					locate for left(field1,2)="SE" while !eof()
*!*					xslncnt=xslncnt+1
        Locate For Left(field1,2)="ST" While !Eof()
        Loop
      Else
        xslncnt=1
      Endif

      xprevlineno=xlineno

      If xsap
        xstyle=Alltrim(Substr(field1,At("*",field1,13)+1,At("*",field1,14)-(At("*",field1,13)+1)))
        If Occurs("*",field1)=13
          xstyle=Right(Alltrim(field1),Len(Alltrim(field1))-At("*",field1,13))
        Endif

**grab from here but really should be in PID seg below, according to map from Bill L
        xpartattrib1=Alltrim(Substr(field1,At("*",field1,15)+1,At("*",field1,16)-(At("*",field1,15)+1)))
        If Occurs("*",field1)=15
          xpartattrib1=Right(Alltrim(field1),Len(Alltrim(field1))-At("*",field1,15))
        Endif
      Else
        xpos=Iif(xppk,11,15) &&if ppk, its in 11th pos, else 15th
        xstyle=Alltrim(Substr(field1,At("*",field1,xpos)+1,At("*",field1,xpos+1)-(At("*",field1,xpos)+1)))

**grab from here but really should be in PID seg below, according to map from Bill L
        xpartattrib1=Alltrim(Substr(field1,At("*",field1,19)+1,At("*",field1,20)-(At("*",field1,19)+1)))
        If Occurs("*",field1)=19
          xpartattrib1=Right(Alltrim(field1),Len(Alltrim(field1))-At("*",field1,19))
        Endif
      Endif

      xpartattrib2="" &&grabbed from PID segment below
      xpartattrib3="" &&grabbed from PID segment below
      xdescription="" &&grabbed in later segments depending on system and item type

**for column CW, use 2-digit ship from country. SAP will use the N1*OT seg within the PO1 loop below
      xorigincountry=xshipfromcountry2

**reset customtext5, etc every line item because they are supposed to be grabbed below this point... customtext1-4 grabbed at header
      Store "" To xcustomtext5
      Store "" To xcustomdecimal1, xcustomdecimal2, xcustomdecimal3, xcustomdecimal4, xcustomdecimal5
      Store "" To xcustomdate1, xcustomdate2, xcustomdate3, xcustomdate4, xcustomdate5

      xhts="" &&reset here and after write to xpostr in SLN case (value could be grabbed from TC2 or SLN segs depending if prepack)
      xqty=Alltrim(Substr(field1,At("*",field1,2)+1,At("*",field1,3)-(At("*",field1,2)+1)))
      xuom=Alltrim(Substr(field1,At("*",field1,3)+1,At("*",field1,4)-(At("*",field1,3)+1)))
      xunitprice="" &&grabbed from SLN seg

**for legacy system and SAP (when xuom#"AS"), grab style description from PID05 where PID07 = "MDL" at item level
    Case Left(field1,3)="PID" And xlevel="I" And Right(Alltrim(field1),Len(Alltrim(field1))-At("*",field1,7))="MDL"
      xdescription=Alltrim(Substr(field1,At("*",field1,5)+1,At("*",field1,6)-(At("*",field1,5)+1)))

**xpartattrib1 = PID04 where PID07=SHD
    Case Left(field1,3)="PID" And xlevel="I" And Right(Alltrim(field1),Len(Alltrim(field1))-At("*",field1,7))="SHD"
      xpartattrib1=Alltrim(Substr(field1,At("*",field1,4)+1,At("*",field1,5)-(At("*",field1,4)+1)))
**xpartattrib2 = PID04 where PID07=QLT
    Case Left(field1,3)="PID" And xlevel="I" And Right(Alltrim(field1),Len(Alltrim(field1))-At("*",field1,7))="QLT"
      xpartattrib2=Alltrim(Substr(field1,At("*",field1,4)+1,At("*",field1,5)-(At("*",field1,4)+1)))
**xpartattrib3 = PID04 where PID07=GOH
    Case Left(field1,3)="PID" And xlevel="I" And Right(Alltrim(field1),Len(Alltrim(field1))-At("*",field1,7))="GOH"
      xpartattrib3=Alltrim(Substr(field1,At("*",field1,4)+1,At("*",field1,5)-(At("*",field1,4)+1)))

    Case Left(field1,3)="FOB" And Occurs("*",field1)>=5 &&other FOB segs exist that do not contain an FOB05 element, disregard those
**valid ICON inco terms
**EXW	EX WORKS
**FCA	FREE CARRIER
**FAS	FREE ALONGSIDE SHIP
**FOB	FREE ON BOARD
**CFR	COST AND FREIGHT
**CIF	COST INSURANCE AND FREIGHT
**CPT	CARRIAGE PAID TO
**CIP	CARRIAGE AND INSURANCE PAID TO
**DAF	DELIVERED AT FRONTIER
**DES	DELIVERED EX SHIP
**DEQ	DELIVERED EX QUAY
**DDU	DELIVERED DUTY UNPAID
**DDP	DELIVERED DUTY PAID
**PPD	PREPAID
**DAP	DELIVERY AT PLACE
**COL	COLLECT

      xincoterms=Alltrim(Substr(field1,At("*",field1,5)+1,At("*",field1,6)-(At("*",field1,5)+1)))
      If Occurs("*",field1)=5
        xincoterms=Right(Alltrim(field1),Len(Alltrim(field1))-At("*",field1,5))
      Endif

**if invalid inco term, set to default - mvw 10/11/12
      If !Inlist(xincoterms,"EXW","FCA","FAS","FOB","CFR","CIF","CPT","CIP","DAF","DES","DEQ","DDU","DDP","PPD","DAP","COL")
        xincoterms="FOB"
      Endif

**if FOB06="PD", grab ship to city
      If Occurs("*",field1)>=7 And Alltrim(Substr(field1,At("*",field1,6)+1,At("*",field1,7)-(At("*",field1,6)+1)))="PD"
        xhcustomtext1=Alltrim(Substr(field1,At("*",field1,7)+1,At("*",field1,8)-(At("*",field1,7)+1)))
        If Occurs("*",field1)=7
          xhcustomtext1=Right(Alltrim(field1),Len(Alltrim(field1))-At("*",field1,7))
        Endif
      Endif

    Case Left(field1,3)="DTM" And ((Substr(field1,5,3)="118" And xsap) Or (Substr(field1,5,3)="010" And !xsap))
      xexwreqdt=Right(Alltrim(field1),Len(Alltrim(field1))-At("*",field1,2))

**check that its a valid date (within 10 years), if not enter as today's date
      If !Between(Ctod(Substr(xexwreqdt,5,2)+"/"+Substr(xexwreqdt,7,2)+"/"+Left(xexwreqdt,4)),Gomonth(Date(),-(10*12)),Gomonth(Date(),10*12))
        xexwreqdt=Transform(Year(Date()))+"-"+Padl(Transform(Month(Date())),2,"0")+"-"+Padl(Transform(Day(Date())),2,"0")
      Else
        xexwreqdt=Left(xexwreqdt,4)+"-"+Substr(xexwreqdt,5,2)+"-"+Substr(xexwreqdt,7,2)
      Endif
      xreqdt=xexwreqdt

    Case Left(field1,3)="DTM" And (Substr(field1,5,3)="118" And !xsap) &&in legacy, if DTM*010 missing use DTM*118
      xexwreqdt2=Right(Alltrim(field1),Len(Alltrim(field1))-At("*",field1,2))

**check that its a valid date (within 10 years), if not enter as today's date
      If !Between(Ctod(Substr(xexwreqdt2,5,2)+"/"+Substr(xexwreqdt2,7,2)+"/"+Left(xexwreqdt2,4)),Gomonth(Date(),-(10*12)),Gomonth(Date(),10*12))
        xexwreqdt2=Transform(Year(Date()))+"-"+Padl(Transform(Month(Date())),2,"0")+"-"+Padl(Transform(Day(Date())),2,"0")
      Else
        xexwreqdt2=Left(xexwreqdt2,4)+"-"+Substr(xexwreqdt2,5,2)+"-"+Substr(xexwreqdt2,7,2)
      Endif
      xreqdt2=xexwreqdt2

    Case Left(field1,3)="DTM" And Substr(field1,5,3)="077"
      xdate=Right(Alltrim(field1),Len(Alltrim(field1))-At("*",field1,2))

**check that its a valid date (within 10 years), leave blank
      If !Between(Ctod(Substr(xdate,5,2)+"/"+Substr(xdate,7,2)+"/"+Left(xdate,4)),Gomonth(Date(),-(10*12)),Gomonth(Date(),10*12))
        xdate=""
      Else
        xdate=Left(xdate,4)+"-"+Substr(xdate,5,2)+"-"+Substr(xdate,7,2)
      Endif
      xcustomdate1=xdate

    Case Left(field1,3)="DTM" And Substr(field1,5,3)="063"
      xdate=Right(Alltrim(field1),Len(Alltrim(field1))-At("*",field1,2))

**check that its a valid date (within 10 years), leave blank
      If !Between(Ctod(Substr(xdate,5,2)+"/"+Substr(xdate,7,2)+"/"+Left(xdate,4)),Gomonth(Date(),-(10*12)),Gomonth(Date(),10*12))
        xdate=""
      Else
        xdate=Left(xdate,4)+"-"+Substr(xdate,5,2)+"-"+Substr(xdate,7,2)
      Endif
      xcustomdate2=xdate

    Case Left(field1,3)="DTM" And Substr(field1,5,3)="196"
      xdate=Right(Alltrim(field1),Len(Alltrim(field1))-At("*",field1,2))

**check that its a valid date (within 10 years), leave blank
      If !Between(Ctod(Substr(xdate,5,2)+"/"+Substr(xdate,7,2)+"/"+Left(xdate,4)),Gomonth(Date(),-(10*12)),Gomonth(Date(),10*12))
        xdate=""
      Else
        xdate=Left(xdate,4)+"-"+Substr(xdate,5,2)+"-"+Substr(xdate,7,2)
      Endif
      xcustomdate3=xdate

    Case Left(field1,3)="DTM" And Substr(field1,5,3)="256"
      xdate=Right(Alltrim(field1),Len(Alltrim(field1))-At("*",field1,2))

**check that its a valid date (within 10 years), leave blank
      If !Between(Ctod(Substr(xdate,5,2)+"/"+Substr(xdate,7,2)+"/"+Left(xdate,4)),Gomonth(Date(),-(10*12)),Gomonth(Date(),10*12))
        xdate=""
      Else
        xdate=Left(xdate,4)+"-"+Substr(xdate,5,2)+"-"+Substr(xdate,7,2)
      Endif
      xcustomdate4=xdate


    Case Left(field1,3)="TC2"
      xhts=Alltrim(Substr(field1,At("*",field1,2)+1,At("*",field1,3)-(At("*",field1,2)+1)))
      If Occurs("*",field1)=2
        xhts=Right(Alltrim(field1),Len(Alltrim(field1))-At("*",field1,2))
      Endif

    Case Left(field1,3)="TD5"
**sent as A & S, only account for A or S bc thats all icon accepts
      xshipvia=Right(Alltrim(field1),Len(Alltrim(field1))-At("*",field1,4))
      xshipvia=Iif(xshipvia="A","AIR","SEA")

**if TD507 = "OR", TD508 contains origin port. if "DE", destination port
      xloctype=Alltrim(Substr(field1,At("*",field1,7)+1,At("*",field1,8)-(At("*",field1,7)+1)))
      xvarname=Iif(xloctype="OR","xfobloccode","xdelivport")
      If Occurs("*",field1)>8
        &xvarname=Alltrim(Substr(field1,At("*",field1,8)+1,At("*",field1,9)-(At("*",field1,8)+1)))
      Else
        &xvarname=Right(Alltrim(field1),Len(Alltrim(field1))-At("*",field1,8))
      Endif

      If xloctype="OR"
**service type
        xcustomtext5=Alltrim(Substr(field1,At("*",field1,12)+1,At("*",field1,13)-(At("*",field1,12)+1)))
        If Occurs("*",field1)=12
          xcustomtext5=Right(Alltrim(field1),Len(Alltrim(field1))-At("*",field1,12))
**also place the service type in column AP ber Bill L - mvw 10/01/14
          xhcustomtext2=xcustomtext5
        Endif
      Endif

    Case Left(field1,2)="N1" And Substr(field1,4,2)="OT" And xlevel="I" And xsap
      xorigincountry=Alltrim(Substr(field1,At("*",field1,4)+1,At("*",field1,5)-(At("*",field1,4)+1)))
      If Occurs("*",field1)=4
        xorigincountry=Right(Alltrim(field1),Len(Alltrim(field1))-At("*",field1,4))
      Endif
      xorigincountry=Left(xorigincountry,2) &&only left 2 of UNLOCCODE

    Case Left(field1,3)="SLN"
      xsubline=Alltrim(Substr(field1,At("*",field1,1)+1,At("*",field1,2)-(At("*",field1,1)+1)))
      xunitprice=Alltrim(Substr(field1,At("*",field1,6)+1,At("*",field1,7)-(At("*",field1,6)+1)))

      If Empty(xpartattrib2) &&should be popultaed from PID seg above, but if not grab from SLN16
        xpartattrib2=Alltrim(Substr(field1,At("*",field1,16)+1,At("*",field1,17)-(At("*",field1,16)+1)))
      Endif

      xhts=Iif(!Empty(xhts),xhts,"") &&could potentially be grabbing hts from SLN if not located in TC2

**removed. customtext fields changed with Bill L's map
*!*				xcustomtext1=xhts
*!*				xcustomtext2=alltrim(substr(field1,at("*",field1,5)+1,at("*",field1,6)-(at("*",field1,5)+1)))
*!*				xcustomtext3=alltrim(substr(field1,at("*",field1,14)+1,at("*",field1,15)-(at("*",field1,14)+1)))
*!*				xcustomtext4=alltrim(substr(field1,at("*",field1,10)+1,at("*",field1,11)-(at("*",field1,10)+1)))

**no longer need flag to be updated for CM per Rebecca - 12/15/14
*!*				xhcustomflag1=iif(xbuyercode="CM","TRUE","")
      xhcustomflag1=""
      xcustomflag1=Iif((xsap And xuom="AS") Or (!xsap And xppk), "TRUE", "")

**for SAP and uom = "AS" must grab the style description from the PID segment within the SLN loop
      If xsap And xuom="AS"
        xrecno=Recno()
        Skip
**should only be these types of segs within SLN loop (CTP, MTX & PID), if encounter any other exit out and go back to previous SLN seg (xrecno)
        Do While Inlist(Left(field1,3),"CTP","PID","MTX")
          If Left(field1,3)="PID" And Right(Alltrim(field1),Len(Alltrim(field1))-At("*",field1,7))="MDL"
            xdescription=Alltrim(Substr(field1,At("*",field1,5)+1,At("*",field1,6)-(At("*",field1,5)+1)))
          Endif
          Skip
        Enddo
        Goto xrecno
      Endif

**in legacy, if DTM*010 missing use DTM*118 (xexwreqdt2)
      If !xsap
        xexwreqdt=Iif(Type("xexwreqdt")="U",xexwreqdt2,xexwreqdt)
        xreqdt=Iif(Type("xreqdt")="U",xreqdt2,xreqdt)
      Endif

**if xexwreqdt exists use it for contractreqdt, else use the contractreqdt
      xcontractreqdt=Iif(!Empty(xexwreqdt),xexwreqdt,xcontractreqdt)

**skip all remaining SLN loops, only send 1 line per PO1 loop. So skip to next SE or PO1 seg
**need a count of SLN segs for col CJ
      Sum Iif(Left(field1,3)="SLN",1,0) To xslncnt While !Inlist(Left(field1,3),"SE","PO1") And !Eof()
      xcustomdecimal1=Iif((xsap And xuom="AS") Or (!xsap And xppk), Transform(xslncnt), "")

**moved xhdrstr definition here from the top of the PO1 case because xshipvia isn't defined until after PO1 in the TD5 seg
      xhdrstr=Alltrim(xponum)+"|"+; &&A
      "|"+; &&column(s) B empty
      xaction+"|"+; &&C
      xtollcustcode+"|"+;&&D
      xshipfromcode+"|"+; &&E
      xshipfromname+"|"+; &&F
      xshipfromaddr1+"|"+; &&G
      xshipfromaddr2+"|"+; &&H
      xshipfromcity+"|"+; &&I
      xshipfromstate+"|"+; &&J
      xshipfromcountry+"|"+; &&K
      xshipfromzip+"|"+; &&L
      "||"+; &&column(s) M-N empty
      xexwreqdt+"|"+; &&O
      xcontractreqdt+"|"+; &&P
      xpodesc+"|"+; &&Q
      xorderdt+"|"+; &&R
      xordervalue+"|"+; &&column(s) S empty
      xcurrency+"|"+; &&T
      "|"+; &&column(s) U empty
      xincoterms+"|"+; &&V
      "|"+; &&column(s) W empty
      xshipvia+"|"+; &&X
      "||"+; &&column(s) Y-Z is empty
      "|"+; &&AA
      "|"+; &&column(s) AB is empty
      xcontractreqdt+"|"+; &&AC
      "|||||||"+; &&column(s) AD-AJ empty
      xsendagentcode+"|"+; &&AK
      xsendagentname+"|"+; &&AL
      xfobloccode+"|"+; &&AM
      xdelivport+"|"+; &&AN
      xhcustomtext1+"|"+; &&AO
      xhcustomtext2+"|"+; &&AP
      xhcustomtext3+"|"+; &&AQ
      xhcustomtext4+"|"+; &&AR
      xhcustomtext5+"|"+; &&AS
      "||"+; &&column(s) AT-AU empty
      xtotqty+"|"+; &&AV
      "||||"+; &&column(s) AW-AZ empty
      xhcustomflag1+"|"+; &&BA
      "||||"+; &&column(s) BB-BE empty
      "|"+; &&BF
      "|"+; &&BG
      xsuppliercountry2+"|"+; &&BH
      xnotes+"|" &&BI

**send error if ship to code not sent - mvw 06/25/15
      If Empty(xshiptocode)
        xfrom ="TGFSYSTEM"
        xmessage = "Error in file '"+xfilename+"' for PO '"+xponum+"': Segments for ship-to code not found. (Segs N1*BG & N1*WD)"+;
        chr(13)+Chr(13)+"Please note, all other POs in this file have been processed."
        Do Form dartmail2 With "mwinter@fmiint.com, rebecca.h.ma@tollgroup.com, pgaidis@fmiint.com",xFrom,"Polo 850 Translation Error"," ","",xmessage,"A"
      Endif

      If xuom = "AS"
        xuom = "UNT"
      Endif

**only add po line if the qty>0 - mvw 05/09/12
      If Val(xqty)>0 And !Empty(xshiptocode)
**removed the "-" from the lineno as ICON could not accept it. Now line number "001" will go as "0011"
**  for the 1st SLN, "0012" for the 2nd and so on. The right-most character will need to be stripped
**  for the 856 going back to PVH - mvw 01/12/12
*xlineno+"-"+transform(xslncnt)+"|" &&BJ

**remove "-" from inbetween xpoline and xslncnt, ICON cannot handle "-"
*xlineno+padl(transform(xslncnt),2,"0")+"|"+; &&BJ
**update xpostr now, not xfilestr... only add to xfilestr once a po is complete in case of errors (dline dups, etc) within a po - mvw 01/16/13
        xpostr=xpostr+xhdrstr+;
        xlineno+"|"+; &&BJ
        "||"+; &&column(s) BK-BL empty
        xstyle+"|"+; &&BM
        xdescription+"|"+; &&BN
        Transform(xqty)+"|"+; &&BO
        "||"+; &&BP-BQ
        xuom+"|"+; &&BR
        Transform(xunitprice)+"|"+; &&BS
        "|"+; &&column(s) BT empty
        xaction+"|"+; &&BU
        xreqdt+"|"+; &&BV
        xpartattrib1+"|"+; &&BW
        xpartattrib2+"|"+; &&BX
        xpartattrib3+"|"+; &&BY
        xcustomtext1+"|"+; &&BZ
        xcustomtext2+"|"+; &&CA
        xcustomtext3+"|"+; &&CB
        xcustomtext4+"|"+; &&CC
        xcustomtext5+"|"+; &&CD
        xcustomdate1+"|"+; &&CE
        xcustomdate2+"|"+; &&CF
        xcustomdate3+"|"+; &&CG
        xcustomdate4+"|"+; &&CH
        xcustomdate5+"|"+; &&CI
        xcustomdecimal1+"|"+; &&CJ
        xcustomdecimal2+"|"+; &&CK
        xcustomdecimal3+"|"+; &&CL
        xcustomdecimal4+"|"+; &&CM
        xcustomdecimal5+"|"+; &&CN
        xcustomflag1+"|"+; &&CO
        "|"+; &&CP
        "||||||"+; &&column(s) CQ-CV empty
        xorigincountry+"|"+; &&CW
        "||||||||"+;&&column(s) CX-DE empty
        xdelivport+"|"+; &&DF
        xshiptocode+"|"+; &&DG
        xshiptoname+"|"+; &&DH
        xshiptoaddr1+"|"+; &&DI
        xshiptoaddr2+"|"+; &&DJ
        xshiptocity+"|"+; &&DK
        xshiptostate+"|"+; &&DL
        xshiptocountry+"|"+; &&DM
        xshiptozip+; &&DN
        Chr(13)+Chr(10)
      Endif

      xhts="" &&reset here and in PO1 case (value could be grabbed from TC2 or SLN segs depending if prepack)
*xslncnt=xslncnt+1

      If xerror
        Exit
      Endif

	  select xpodata
	  locate for po_num=xponum
	  if !found()
		m.po_num=xponum
		m.podate=ctod(right(xorderdt,2)+"/"+substr(xorderdt,6,2)+"/"+left(xorderdt,4))
		m.acctname="POLO"
		m.importdt=datetime()
		m.infile=upper(xfilename)
		insert into xpodata from memvar
	  endif

	  select x850

**skipped to next SE or PO1 seg above with the sum() statement
      Loop &&loop here to avoid skip before enddo

**removed, think remnent from other 850 code where loop thru SLN segs above - mvw 01/31/14
*skip -1

    Case Left(field1,3)="CTT"
    Case Left(field1,3)="SE"
**add xpostr to xfilestr once a po is complete in case of errors (line dups, etc) within a po - mvw 01/16/13
      xfilestr=xfilestr+xpostr
    Endcase

    Select x850
    Skip
  Enddo

  Select x850
  Locate

**if xfilestr only contains header info, something went wrong! - mvw 07/05/12
**it may now be that all the pos in this file were Non-Bangladesh pos, which we do not send... dont treat as error, just delete file and move on - mvw 10/03/12
*!*		if len(xfilestr)=xhdrlen
*!*			xerror=.t.
*!*		endif

  If !xerror
**if pos from countries we do business with exist (ie, Bangladesh (list of countries above)), send file... else just delete the file - mvw 10/03/12
    If Len(xfilestr)>xhdrlen
      Do While .T.
        Set Date ymd
        xoutfile="PO_TGFDI_"+Strtran(Strtran(Strtran(Ttoc(Datetime()),":","")," ",""),"/")+xtollcustcode+".txt"
        Set Date american

        xarchiveoutfile=xoutdir+"archive\"+xoutfile
        If !File(xarchiveoutfile)
          Exit
        Endif
      Enddo

      xoutfile=xoutdir+xoutfile
      Strtofile(xfilestr,xoutfile)
      Strtofile(xfilestr,xarchiveoutfile)

      xfilecreated=.T.
    Endif

    Copy File &xfilename To &xarchiveinfile
    Delete File &xfilename

	select xpodata
	scan
		scatter memvar
		m.outfile=upper(xoutfile)
		insert into podata from memvar
	endscan
  Endif
Endscan

If xfilecreated
**create an ftpjob
  Use F:\edirouting\ftpjobs In 0
  xjob=Iif(xtest,"POLO-TEST-850-ICON","POLO-850-TO-ICON")
  Insert Into ftpjobs (jobname, Userid, jobtime) Values (xjob,"POLO850",Datetime())
  Use In ftpjobs
Endif

Use In x850
Use In xfilelist
Use In unloccode
use in podata
use in xpodata

Wait Clear
