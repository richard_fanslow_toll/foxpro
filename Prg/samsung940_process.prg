*!* m:\dev\prg\samsung940_process.prg

CD &lcpath

LogCommentStr = ""
delimchar = cDelimiter
lcTranOpt= cTranslateOption

lnNum = IIF(lTesting,ADIR(tarray,"*.*"),ADIR(tarray,"*.940"))

IF lnNum = 0
	WAIT WINDOW AT 10,10 "      No "+cCustname+" 940s to import     " TIMEOUT 3
	NormalExit = .T.
	THROW
ENDIF

FOR thisfile = 1  TO lnNum
	Xfile = lcpath+tarray[thisfile,1] && +"."
	cfilename = ALLTRIM(LOWER(tarray[thisfile,1]))
	nFileSize = VAL(TRANSFORM(tarray(thisfile,2)))
	ArchiveFile = (lcArchivePath+cfilename)
*	c997File = ("f:\ftpusers\samsung\997out\"+cfilename)
*	COPY FILE [&xfile] TO [&c997file]
	WAIT "" TIMEOUT 2
	WAIT WINDOW "Importing file: "+cfilename NOWAIT
	DO m:\dev\prg\createx856a
*	APPEND from &xfile TYPE DELIMITED WITH CHARACTER "*" 
	DO m:\dev\prg\loadedifile WITH Xfile,delimchar,lcTranOpt,cCustname
	SELECT x856
	LOCATE
	IF lTesting
*		LOCATE
*		BROWSE
	endif

	DO create_pt_cursors WITH cUseFolder,lLoadSQL  && Added to prep for SQL Server transition, replaces old cursor creation

	DO ("m:\dev\prg\"+cCustname+"940_bkdn")
	DO "m:\dev\prg\all940_import"

*	release_ptvars()
NEXT thisfile

&& now clean up the 940in archive files, delete for the upto the last 10 days
*deletefile(lcarchivepath,IIF(DATE()<{^2016-05-01},20,10))

WAIT CLEAR

WAIT "Entire "+cCustname+" 940 Process Now Complete" WINDOW TIMEOUT 3
