
* STATISTICS FOR HECTOR V.
* Orig author unknown; probably was Greg DeSaye.
* Revised by Mark Bennett 12/2011
* it summarizes prior month and adds to cumulative spreadsheet

SET ENGINEBEHAVIOR 70
SET EXCLUSIVE OFF
SET DELETED ON
SET TALK OFF

CLOSE DATABASES ALL

LOCAL lcWHOfficeTable, i, lcWHDataFolder, ldDate, lcInWoLogTable, lcOutWoLogTable, lnCStatLength
LOCAL lcOutdetTable

lnCStatLength = 50


PRIVATE m.cOffice, m.cDescript, m.dStart, m.dEnd, m.cStatistic
PRIVATE m.quantity, m.weight, m.units, m.cartons

ldDate = DATE()
m.dEnd = ldDate - DAY(ldDate)
m.dStart = GOMONTH((m.dEnd + 1),-1)

*!*	m.dStart = {^2011-10-01}
*!*	m.dEnd = {^2011-10-31}

lcWHOfficeTable = 'F:\WH\WHOFFICE'

IF USED('CURWHOFFICE') THEN
	USE IN CURWHOFFICE
ENDIF

SELECT * ;
	FROM (lcWHOfficeTable) ;
	INTO CURSOR CURWHOFFICE ;
	WHERE (NOT TEST) ;
	ORDER BY OFFICE

IF USED('WHOFFICE') THEN
	USE IN WHOFFICE
ENDIF

*!*	SELECT CURWHOFFICE
*!*	BROW

USE F:\UTIL\sumrpt\DATA\SUMMARY IN 0 SHARED ALIAS SUMMARYTABLE

i = 0
SELECT CURWHOFFICE
SCAN
	i = i + 1

	lcWHDataFolder = UPPER(ALLTRIM(CURWHOFFICE.FOLDER)) + '\WHDATA\'
	m.cOffice = UPPER(ALLTRIM(CURWHOFFICE.OFFICE))
	m.cDescript = PADR(UPPER(CURWHOFFICE.DESCRIP),lnCStatLength,' ')


	***********************************************************
	*******************************************TRANSLOAD STATS
	***********************************************************

	* GOH
	m.cStatistic = PADR('TRANSLOAD GOH',lnCStatLength,' ')
	
	WAIT WINDOW NOWAIT 'Extracting data for ' + ALLTRIM(m.cStatistic) + " for " + ALLTRIM(m.cDescript) + "..."
	STORE 0 TO m.quantity, m.weight, m.unitsin, m.cartonsin, m.unitsout, m.cartonsout

	IF USED('temptable') THEN
		USE IN temptable
	ENDIF

	SELECT ;
		SUM(quantity) AS quantity, ;
		SUM(weight) AS weight ;
		FROM F:\WO\WODATA\MANIFEST ;
		INTO CURSOR temptable ;
		WHERE qty_type = 'GOH' ;
		AND OFFICE = m.cOffice ;
		AND wo_date >= m.dStart ;
		AND wo_date <= m.dEnd

	WAIT WINDOW NOWAIT 'Saving results for ' + ALLTRIM(m.cStatistic) + " for " + ALLTRIM(m.cDescript) + "..."

	IF USED('temptable') AND (NOT EOF('temptable')) THEN
		SELECT temptable
		LOCATE
		SCATTER MEMVAR
	ELSE
		STORE 0 TO m.quantity, m.weight
	ENDIF
	
	SELECT SUMMARYTABLE
	LOCATE FOR (dStart = m.dStart) AND (dEnd = m.dEnd) AND (cDescript == m.cDescript) AND (cOffice == m.cOffice) AND (cStatistic == m.cStatistic)
	IF FOUND() THEN
		DELETE
	ENDIF
	INSERT INTO SUMMARYTABLE FROM MEMVAR
	
	WAIT CLEAR

	* non GOH
	m.cStatistic = PADR('TRANSLOAD NON-GOH CARTONS',lnCStatLength,' ')
	WAIT WINDOW NOWAIT 'Extracting data for ' + ALLTRIM(m.cStatistic) + " for " + ALLTRIM(m.cDescript) + "..."
	STORE 0 TO m.quantity, m.weight, m.unitsin, m.cartonsin, m.unitsout, m.cartonsout

	IF USED('temptable') THEN
		USE IN temptable
	ENDIF

	SELECT ;
		SUM(quantity) AS quantity, ;
		SUM(weight) AS weight ;
		FROM F:\WO\WODATA\MANIFEST ;
		INTO CURSOR temptable ;
		WHERE qty_type <> 'GOH' ;
		AND OFFICE = m.cOffice ;
		AND wo_date >= m.dStart ;
		AND wo_date <= m.dEnd

	WAIT WINDOW NOWAIT 'Saving results for ' + ALLTRIM(m.cStatistic) + " for " + ALLTRIM(m.cDescript) + "..."

	IF USED('temptable') AND (NOT EOF('temptable')) THEN
		SELECT temptable
		LOCATE
		SCATTER MEMVAR
	ELSE
		STORE 0 TO m.quantity, m.weight
	ENDIF

	SELECT SUMMARYTABLE
	LOCATE FOR (dStart = m.dStart) AND (dEnd = m.dEnd) AND (cDescript == m.cDescript) AND (cOffice == m.cOffice) AND (cStatistic == m.cStatistic)
	IF FOUND() THEN
		DELETE
	ENDIF
	INSERT INTO SUMMARYTABLE FROM MEMVAR
	
	WAIT CLEAR

	*******************************************WAREHOUSE STATS, INBOUND
	lcInWoLogTable = lcWHDataFolder + "INWOLOG"

	USE (lcInWoLogTable) IN 0 ALIAS INWOLOG

	m.cStatistic = PADR('INWOLOG UNIT, CARTON COUNTS',lnCStatLength,' ')

	WAIT WINDOW NOWAIT 'Extracting data for ' + ALLTRIM(m.cStatistic) + " for " + ALLTRIM(m.cDescript) + "..."
	STORE 0 TO m.quantity, m.weight, m.unitsin, m.cartonsin, m.unitsout, m.cartonsout

*!*		IF USED('CURTEMP') THEN
*!*			USE IN CURTEMP
*!*		ENDIF

*!*		SELECT unitsinqty, quantity ;
*!*			FROM INWOLOG ;
*!*			INTO CURSOR CURTEMP ;
*!*			WHERE (NOT sp) ;
*!*			AND TTOD(confirmdt) >= m.dStart ;
*!*			AND TTOD(confirmdt) <= m.dEnd

*!*		IF USED('CURTEMP') AND (NOT EOF('CURTEMP')) THEN
*!*			SELECT CURTEMP
*!*			SUM unitsinqty TO m.units
*!*			SUM quantity FOR (unitsinqty = 0) TO m.cartons
*!*		ELSE
*!*			STORE 0 TO m.units, m.cartons
*!*		ENDIF

	SELECT INWOLOG
	SUM unitsinqty FOR (NOT sp) AND (TTOD(confirmdt) >= m.dStart) AND (TTOD(confirmdt) <= m.dEnd) TO m.unitsin
	SUM quantity   FOR (NOT sp) AND (TTOD(confirmdt) >= m.dStart) AND (TTOD(confirmdt) <= m.dEnd) AND (unitsinqty = 0) TO m.cartonsin

	SELECT SUMMARYTABLE
	LOCATE FOR (dStart = m.dStart) AND (dEnd = m.dEnd) AND (cDescript == m.cDescript) AND (cOffice == m.cOffice) AND (cStatistic == m.cStatistic)
	IF FOUND() THEN
		DELETE
	ENDIF
	INSERT INTO SUMMARYTABLE FROM MEMVAR
	
*!*		IF USED('CURTEMP') THEN
*!*			USE IN CURTEMP
*!*		ENDIF
	IF USED('INWOLOG') THEN
		USE IN INWOLOG
	ENDIF
	
	WAIT CLEAR

	*******************************************WAREHOUSE STATS, OUTBOUND
	lcOutWoLogTable = lcWHDataFolder + "OUTWOLOG"

	USE (lcOutWoLogTable) IN 0 ALIAS OUTWOLOG

	m.cStatistic = PADR('OUTWOLOG UNIT, CARTON COUNTS - by SUMMING OUTWOLOG',lnCStatLength,' ')

	WAIT WINDOW NOWAIT 'Extracting data for ' + ALLTRIM(m.cStatistic) + " for " + ALLTRIM(m.cDescript) + "..."
	STORE 0 TO m.quantity, m.weight, m.unitsin, m.cartonsin, m.unitsout, m.cartonsout

	SELECT OUTWOLOG
	SUM quantity FOR      picknpack  AND (NOT sp) AND (pulleddt >= m.dStart) AND (pulleddt <= m.dEnd) TO m.unitsout
	SUM quantity FOR (NOT picknpack) AND (NOT sp) AND (pulleddt >= m.dStart) AND (pulleddt <= m.dEnd) TO m.cartonsout

	SELECT SUMMARYTABLE
	LOCATE FOR (dStart = m.dStart) AND (dEnd = m.dEnd) AND (cDescript == m.cDescript) AND (cOffice == m.cOffice) AND (cStatistic == m.cStatistic)
	IF FOUND() THEN
		DELETE
	ENDIF
	INSERT INTO SUMMARYTABLE FROM MEMVAR
	
	IF USED('OUTWOLOG') THEN
		USE IN OUTWOLOG
	ENDIF
	
	WAIT CLEAR

	********
	lcOutdetTable = lcWHDataFolder + "OUTDET"

	USE (lcOutdetTable) IN 0 ALIAS OUTDET

	USE (lcOutWoLogTable) IN 0 ALIAS OUTWOLOG

	m.cStatistic = PADR('OUTWOLOG UNIT, CARTON COUNTS - BY JOINING OUTWOLOG w OUTDET',lnCStatLength,' ')

	WAIT WINDOW NOWAIT 'Extracting data for ' + ALLTRIM(m.cStatistic) + " for " + ALLTRIM(m.cDescript) + "..."
	STORE 0 TO m.quantity, m.weight, m.unitsin, m.cartonsin, m.unitsout, m.cartonsout

	IF USED('temptable') THEN
		USE IN temptable
	ENDIF
	
	SELECT SUM(B.TOTQTY) AS unitsout ;
		FROM OUTWOLOG A ;
		INNER JOIN OUTDET B ;
		ON B.OUTWOLOGID = A.OUTWOLOGID ;
		INTO CURSOR temptable ;
		WHERE (A.pulleddt >= m.dStart) AND (A.pulleddt <= m.dEnd) ;
		AND B.UNITS

	IF USED('temptable') AND (NOT EOF('temptable')) THEN
		SELECT temptable
		LOCATE
		SCATTER MEMVAR
	ELSE
		STORE 0 TO m.unitsout
	ENDIF

	SELECT SUMMARYTABLE
	LOCATE FOR (dStart = m.dStart) AND (dEnd = m.dEnd) AND (cDescript == m.cDescript) AND (cOffice == m.cOffice) AND (cStatistic == m.cStatistic)
	IF FOUND() THEN
		DELETE
	ENDIF
	INSERT INTO SUMMARYTABLE FROM MEMVAR
		
	IF USED('OUTDET') THEN
		USE IN OUTDET
	ENDIF
		
	IF USED('OUTWOLOG') THEN
		USE IN OUTWOLOG
	ENDIF
	
	WAIT CLEAR


ENDSCAN


*!*				SUM totqty FOR units WHILE outwologid=TID TO TT
*!*				Gtot=Gtot+TT
*!*				SEEK TID
*!*				SUM totqty FOR !units WHILE outwologid=TID TO TT
*!*				CTOT=CTOT+TT
*!*				SELECT temptable
*!*				SKIP
*!*			ENDDO
*!*			whcunitsout=whcunitsout+Gtot
*!*			whcctnsout=whcctnsout+CTOT


SELECT SUMMARYTABLE
BROWSE


IF USED('CURWHOFFICE') THEN
	USE IN CURWHOFFICE
ENDIF

IF USED('SUMMARYTABLE') THEN
	USE IN SUMMARYTABLE
ENDIF

IF USED('MANIFEST') THEN
	USE IN MANIFEST
ENDIF

IF USED('temptable') THEN
	USE IN temptable
ENDIF


RETURN
*!*	******************* old code follows ******************************************

*!*	STORE DATE()-28 TO last_start
*!*	STORE MONTH(DATE()-28) TO tmonth
*!*	STORE YEAR(DATE()-28) TO tyear

*!*	DO WHILE MONTH(last_start)=tmonth AND YEAR(last_start)=tyear AND DAY(last_start)>1
*!*		last_start=last_start-1
*!*	ENDDO
*!*	STORE last_start TO last_end
*!*	DO WHILE MONTH(last_end)=tmonth AND YEAR(last_end)=tyear
*!*		last_end=last_end+1
*!*	ENDDO
*!*	last_end=last_end-1


*!*	STORE .F. TO xaddrecords

*!*	USE F:\ex\exdata\sumrpt2
*!*	GO BOTT

*!*	IF !START=last_start
*!*		STORE .T. TO xaddrecords
*!*		STORE MONTH(START) TO tstartmonth
*!*		STORE YEAR(START) TO tstartyear

*!*		IF tstartmonth=12
*!*			STORE 1 TO tstartmonth
*!*			STORE tstartyear+1 TO tstartyear
*!*		ELSE
*!*			STORE tstartmonth+1 TO tstartmonth
*!*		ENDIF

*!*		IF tstartmonth<10
*!*			STORE CTOD('0'+STR(tstartmonth,1,0)+'/01/'+SUBSTR(STR(tstartyear,4,0),3,2)) TO ld_start
*!*			IF tstartmonth=4 OR tstartmonth=6
*!*				STORE CTOD('0'+STR(tstartmonth,1,0)+'/30/'+SUBSTR(STR(tstartyear,4,0),3,2)) TO ld_end
*!*			ELSE
*!*				IF tstartmonth=2
*!*					STORE CTOD('0'+STR(tstartmonth,1,0)+'/28/'+SUBSTR(STR(tstartyear,4,0),3,2)) TO ld_end
*!*				ELSE
*!*					IF tstartmonth=9
*!*						STORE CTOD('0'+STR(tstartmonth,1,0)+'/30/'+SUBSTR(STR(tstartyear,4,0),3,2)) TO ld_end
*!*					ELSE
*!*						STORE CTOD('0'+STR(tstartmonth,1,0)+'/31/'+SUBSTR(STR(tstartyear,4,0),3,2)) TO ld_end
*!*					ENDIF
*!*				ENDIF
*!*			ENDIF

*!*		ELSE
*!*			STORE CTOD(STR(tstartmonth,2,0)+'/01/'+SUBSTR(STR(tstartyear,4,0),3,2)) TO ld_start
*!*			IF tstartmonth=11
*!*				STORE CTOD(STR(tstartmonth,2,0)+'/30/'+SUBSTR(STR(tstartyear,4,0),3,2)) TO ld_end
*!*			ELSE
*!*				STORE CTOD(STR(tstartmonth,2,0)+'/31/'+SUBSTR(STR(tstartyear,4,0),3,2)) TO ld_end
*!*			ENDIF
*!*		ENDIF


*!*	ENDIF


*!*	USE


*!*	IF xaddrecords

*!*		DO WHILE ld_start<=last_start AND ld_end<=last_end


*!*			***********************************************************
*!*			*******************************************TRANSLOAD STATS
*!*			***********************************************************

*!*			SELECT * FROM F:\wo\wodata\manifest WHERE wo_date>=ld_start AND wo_date<=ld_end INTO CURSOR temptable
*!*			SELECT temptable

*!*			SUM quantity,weight FOR qty_type='GOH' AND OFFICE='C' TO tlcgoh,tlcgohwt
*!*			SUM quantity,weight FOR qty_type='GOH' AND OFFICE='N' TO tlngoh,tlngohwt
*!*			SUM quantity,weight FOR qty_type='GOH' AND OFFICE='M' TO tlmgoh,tlmgohwt
*!*			SUM quantity,weight FOR qty_type='GOH' AND OFFICE='S' TO tlsgoh,tlsgohwt

*!*			SUM quantity,weight FOR OFFICE='C' AND !qty_type='GOH' TO tlcctn,tlcctnwt
*!*			SUM quantity,weight FOR OFFICE='N' AND !qty_type='GOH' TO tlnctn,tlnctnwt
*!*			SUM quantity,weight FOR OFFICE='M' AND !qty_type='GOH' TO tlmctn,tlmctnwt
*!*			SUM quantity,weight FOR OFFICE='S' AND !qty_type='GOH' TO tlsctn,tlsctnwt
*!*			SELECT temptable
*!*			USE

*!*			*******************************************WAREHOUSE STATS

*!*			SELECT * FROM F:\wh1\whdata\inwolog WHERE TTOD(confirmdt)>=ld_start AND TTOD(confirmdt)<=ld_end  AND !sp INTO CURSOR temptable
*!*			SELECT temptable
*!*			SUM unitsinqty TO whcunitsin
*!*			SUM quantity FOR unitsinqty=0 TO whcctnsin
*!*			USE IN temptable
*!*			USE IN inwolog

*!*			SELECT * FROM F:\wh2\whdata\inwolog WHERE TTOD(confirmdt)>=ld_start AND TTOD(confirmdt)<=ld_end  AND !sp INTO CURSOR temptable
*!*			SELECT temptable
*!*			SUM unitsinqty TO tunits
*!*			SUM quantity FOR unitsinqty=0 TO tctns
*!*			whcunitsin=whcunitsin+tunits
*!*			whcctnsin=whcctnsin+tctns
*!*			USE IN temptable
*!*			USE IN inwolog

*!*			SELECT * FROM F:\wh6\whdata\inwolog WHERE TTOD(confirmdt)>=ld_start AND TTOD(confirmdt)<=ld_end  AND !sp INTO CURSOR temptable
*!*			SELECT temptable
*!*			SUM unitsinqty TO tunits
*!*			SUM quantity FOR unitsinqty=0 TO tctns
*!*			whcunitsin=whcunitsin+tunits
*!*			whcctnsin=whcctnsin+tctns
*!*			USE IN temptable
*!*			USE IN inwolog

*!*			SELECT * FROM F:\whl\whdata\inwolog WHERE TTOD(confirmdt)>=ld_start AND TTOD(confirmdt)<=ld_end  AND !sp INTO CURSOR temptable
*!*			SELECT temptable
*!*			STORE 0 TO whmlunitsin,whmlctnsin
*!*			SUM unitsinqty TO tunits
*!*			SUM quantity FOR unitsinqty=0 TO tctns
*!*			whmlunitsin=whmlunitsin+tunits
*!*			whmlctnsin=whmlctnsin+tctns
*!*			USE IN temptable
*!*			USE IN inwolog

*!*			SELECT * FROM F:\wh5\whdata\inwolog WHERE TTOD(confirmdt)>=ld_start AND TTOD(confirmdt)<=ld_end  AND !sp INTO CURSOR temptable
*!*			SELECT temptable
*!*			SUM unitsinqty TO tunits
*!*			SUM quantity FOR unitsinqty=0 TO tctns
*!*			whcunitsin=whcunitsin+tunits
*!*			whcctnsin=whcctnsin+tctns
*!*			USE IN temptable
*!*			USE IN inwolog

*!*			SELECT * FROM F:\whn\whdata\inwolog WHERE TTOD(confirmdt)>=ld_start AND TTOD(confirmdt)<=ld_end  AND !sp INTO CURSOR temptable
*!*			SELECT temptable
*!*			SUM unitsinqty TO whnunitsin
*!*			SUM quantity FOR unitsinqty=0 TO whnctnsin
*!*			USE IN temptable
*!*			USE IN inwolog

*!*			SELECT * FROM F:\whm\whdata\inwolog WHERE TTOD(confirmdt)>=ld_start AND TTOD(confirmdt)<=ld_end  AND !sp INTO CURSOR temptable
*!*			SELECT temptable
*!*			SUM unitsinqty TO whmunitsin
*!*			SUM quantity FOR unitsinqty=0 TO whmctnsin
*!*			USE IN temptable
*!*			USE IN inwolog


		SELECT * FROM F:\wh1\whdata\outwolog  WHERE pulleddt>=ld_start AND pulleddt<=ld_end INTO CURSOR temptable
		SELECT temptable
		SUM quantity FOR picknpack AND !sp TO whcunitsout
		SUM quantity FOR !picknpack AND !sp TO whcctnsout
		USE IN temptable
		USE IN outwolog

		SELECT * FROM F:\wh2\whdata\outwolog  WHERE pulleddt>=ld_start AND pulleddt<=ld_end INTO CURSOR temptable
		SELECT temptable

		USE F:\wh2\whdata\outdet ORDER outwologid IN 0
		STORE 0 TO Gtot, CTOT
		SELECT temptable
		GO TOP
		SELECT temptable
		DO WHILE !EOF()
			STORE outwologid TO TID
			SELECT outdet
			SEEK TID

			SUM totqty FOR units WHILE outwologid=TID TO TT
			Gtot=Gtot+TT
			SEEK TID
			SUM totqty FOR !units WHILE outwologid=TID TO TT
			CTOT=CTOT+TT
			SELECT temptable
			SKIP
		ENDDO
		whcunitsout=whcunitsout+Gtot
		whcctnsout=whcctnsout+CTOT
		USE IN temptable
		USE IN outdet

		USE IN outwolog

		SELECT * FROM F:\wh6\whdata\outwolog  WHERE pulleddt>=ld_start AND pulleddt<=ld_end INTO CURSOR temptable
		SELECT temptable
		USE F:\wh6\whdata\outdet ORDER outwologid IN 0
		STORE 0 TO Gtot, CTOT
		SELECT temptable
		GO TOP
		SELECT temptable
		DO WHILE !EOF()
			STORE outwologid TO TID
			SELECT outdet
			SEEK TID
			SUM totqty FOR units WHILE outwologid=TID TO TT
			Gtot=Gtot+TT
			SEEK TID
			SUM totqty FOR !units WHILE outwologid=TID TO TT
			CTOT=CTOT+TT
			SELECT temptable
			SKIP
		ENDDO
		whcunitsout=whcunitsout+Gtot
		whcctnsout=whcctnsout+CTOT
		USE IN temptable
		USE IN outdet

		USE IN outwolog

		SELECT * FROM F:\whl\whdata\outwolog  WHERE pulleddt>=ld_start AND pulleddt<=ld_end INTO CURSOR temptable
		SELECT temptable
		STORE 0 TO whmlunitsout,whmlctnsout
		USE F:\whl\whdata\outdet ORDER outwologid IN 0
		STORE 0 TO Gtot, CTOT
		SELECT temptable
		GO TOP
		SELECT temptable
		DO WHILE !EOF()
			STORE outwologid TO TID
			SELECT outdet
			SEEK TID
			SUM totqty FOR units WHILE outwologid=TID TO TT
			Gtot=Gtot+TT
			SEEK TID
			SUM totqty FOR !units WHILE outwologid=TID TO TT
			CTOT=CTOT+TT
			SELECT temptable
			SKIP
		ENDDO
		whmlunitsout=Gtot
		whmlctnsout=CTOT
		USE IN temptable
		USE IN outdet

		USE IN outwolog

		SELECT * FROM F:\wh5\whdata\outwolog  WHERE pulleddt>=ld_start AND pulleddt<=ld_end INTO CURSOR temptable
		SELECT temptable
		USE F:\wh5\whdata\outdet ORDER outwologid IN 0
		STORE 0 TO Gtot, CTOT
		SELECT temptable
		GO TOP
		SELECT temptable
		DO WHILE !EOF()
			STORE outwologid TO TID
			SELECT outdet
			SEEK TID

			SUM totqty FOR units WHILE outwologid=TID TO TT
			Gtot=Gtot+TT
			SEEK TID
			SUM totqty FOR !units WHILE outwologid=TID TO TT
			CTOT=CTOT+TT
			SELECT temptable
			SKIP
		ENDDO
		whcunitsout=whcunitsout+Gtot
		whcctnsout=whcctnsout+CTOT
		USE IN temptable
		USE IN outdet

		USE IN outwolog

		SELECT * FROM F:\whn\whdata\outwolog  WHERE pulleddt>=ld_start AND pulleddt<=ld_end INTO CURSOR temptable
		SELECT temptable
		USE F:\whn\whdata\outdet ORDER outwologid IN 0
		STORE 0 TO Gtot, CTOT
		SELECT temptable
		GO TOP
		SELECT temptable
		DO WHILE !EOF()
			STORE outwologid TO TID
			SELECT outdet
			SEEK TID

			SUM totqty FOR units WHILE outwologid=TID TO TT
			Gtot=Gtot+TT
			SEEK TID
			SUM totqty FOR !units WHILE outwologid=TID TO TT
			CTOT=CTOT+TT
			SELECT temptable
			SKIP
		ENDDO
		whnunitsout=Gtot
		whnctnsout=CTOT
		USE IN temptable
		USE IN outdet

		USE IN outwolog

		SELECT * FROM F:\whm\whdata\outwolog  WHERE pulleddt>=ld_start AND pulleddt<=ld_end INTO CURSOR temptable
		SELECT temptable

		USE F:\whm\whdata\outdet ORDER outwologid IN 0
		STORE 0 TO Gtot, CTOT
		SELECT temptable
		GO TOP
		SELECT temptable
		DO WHILE !EOF()
			STORE outwologid TO TID
			SELECT outdet
			SEEK TID

			SUM totqty FOR units WHILE outwologid=TID TO TT
			Gtot=Gtot+TT
			SEEK TID
			SUM totqty FOR !units WHILE outwologid=TID TO TT
			CTOT=CTOT+TT
			SELECT temptable
			SKIP
		ENDDO
		whmunitsout=Gtot
		whmctnsout=CTOT
		USE IN temptable
		USE IN outdet

		USE IN outwolog


		***********************************************************
		*******************************************TRUCKING STATS
		***********************************************************

		SELECT * FROM F:\wo\wodata\wolog WHERE pickedup>=ld_start AND pickedup<=ld_end INTO CURSOR temptable
		SELECT temptable
		COUNT FOR TYPE='O' AND OFFICE='N' AND !loose TO ncontainers
		COUNT FOR TYPE='O' AND OFFICE='C' AND !loose  TO ccontainers
		COUNT FOR TYPE='O' AND OFFICE='M' AND !loose  TO mcontainers


		SELECT temptable
		USE
		SELECT wolog
		USE

		***********************************************************
		*******************************************K-Mart STATS for Mira Loma
		***********************************************************
		SELECT * FROM F:\sears\pmdata\fthistory WHERE transdate>=ld_start AND transdate<=ld_end AND tnode="KM" INTO CURSOR temptable
		SELECT temptable
		SUM rxdockctns TO txdockctns
		SUM rnxdctns TO tsolids
		SUM tiltunits TO ttilttray


		SELECT temptable
		USE


		USE F:\ex\exdata\sumrpt2
		LOCATE FOR START=ld_start
		IF EOF()
			APPEND BLANK
		ENDIF
		REPLACE  START WITH ld_start
		REPLACE END WITH ld_end
		REPLACE tlc_goh WITH tlcgoh
		REPLACE tlc_gohwt WITH tlcgohwt
		REPLACE tlc_ctn WITH tlcctn
		REPLACE tlc_ctnwt WITH tlcctnwt
		REPLACE tln_goh WITH tlngoh
		REPLACE tln_gohwt WITH tlngohwt
		REPLACE tln_ctn WITH tlnctn
		REPLACE tln_ctnwt WITH tlnctnwt
		REPLACE tlm_goh WITH tlmgoh
		REPLACE tlm_gohwt WITH tlmgohwt
		REPLACE tlm_ctn WITH tlmctn
		REPLACE tlm_ctnwt WITH tlmctnwt
		REPLACE whc_ctnin WITH whcctnsin
		REPLACE whc_ctnout WITH whcctnsout
		REPLACE whc_gohin WITH whcunitsin
		REPLACE whc_gohout WITH whcunitsout
		REPLACE whn_ctnin WITH whnctnsin
		REPLACE whn_ctnout WITH whnctnsout
		REPLACE whn_gohin WITH whnunitsin
		REPLACE whn_gohout WITH whnunitsout
		REPLACE whm_ctnin WITH whmctnsin
		REPLACE whm_ctnout WITH whmctnsout
		REPLACE whm_gohin WITH whmunitsin
		REPLACE whm_gohout WITH whmunitsout
		REPLACE wh4_ctnin WITH whmlctnsin
		REPLACE wh4_ctnout WITH whmlctnsout
		REPLACE wh4_gohin WITH whmlunitsin
		REPLACE wh4_gohout WITH whmlunitsout
		REPLACE ncontainer WITH ncontainers
		REPLACE ccontainer WITH ccontainers
		REPLACE mcontainer WITH mcontainers
		REPLACE kmxdock WITH txdockctns
		REPLACE kmsolids WITH tsolids
		REPLACE kmtilttray WITH ttilttray


		SELECT sumrpt2
		USE
		STORE ld_end+1 TO ld_start,ld_end
		STORE MONTH(ld_end) TO tmonth
		STORE YEAR(ld_end) TO tyear
		DO WHILE MONTH(ld_end)=tmonth AND YEAR(ld_end)=tyear
			ld_end=ld_end+1
		ENDDO
		ld_end=ld_end-1

	ENDDO
ENDIF &&addrecords


***********************************************************
*now create the excel spreadsheet
**********************************************************

oexcel=CREATEOBJECT("excel.application")
oworkbook=oexcel.workbooks.OPEN("f:\ex\exdata\sumrpt.xls")

USE F:\ex\exdata\sumrpt2
GO BOTT
STORE END TO displayto
SET FILTER TO START>=displayto-395
GO TOP
STORE START TO displayfrom

STORE 10 TO i
oexcel.VISIBLE=.T.
DO WHILE !EOF()


	osheet1=oworkbook.worksheets[1]
	osheet2=oworkbook.worksheets[2]
	osheet3=oworkbook.worksheets[3]
	osheet4=oworkbook.worksheets[4]

	STORE 'osheet1' TO whichsheet

	WITH &whichsheet

		ii=TRANS(i)
		.RANGE("A2").VALUE="Report from "+DTOC(displayfrom)+" to "+DTOC(displayto)
		.RANGE("A"+ii).VALUE=TRANSFORM(START)
		.RANGE("B"+ii).VALUE=TRANSFORM(END)
		.RANGE("C"+ii).VALUE=TRANSFORM(tlc_goh)
		.RANGE("D"+ii).VALUE=TRANSFORM(tlc_gohwt)
		.RANGE("E"+ii).VALUE=TRANSFORM(tlc_ctn)
		.RANGE("F"+ii).VALUE=TRANSFORM(tlc_ctnwt)
		.RANGE("G"+ii).VALUE=TRANSFORM(tln_goh)
		.RANGE("H"+ii).VALUE=TRANSFORM(tln_gohwt)
		.RANGE("I"+ii).VALUE=TRANSFORM(tln_ctn)
		.RANGE("J"+ii).VALUE=TRANSFORM(tln_ctnwt)
		.RANGE("K"+ii).VALUE=TRANSFORM(tlm_goh)
		.RANGE("L"+ii).VALUE=TRANSFORM(tlm_gohwt)
		.RANGE("M"+ii).VALUE=TRANSFORM(tlm_ctn)
		.RANGE("N"+ii).VALUE=TRANSFORM(tlm_ctnwt)

	ENDWITH

	STORE 'osheet2' TO whichsheet

	WITH &whichsheet

		ii=TRANS(i)
		.RANGE("A2").VALUE="Report from "+DTOC(displayfrom)+" to "+DTOC(displayto)
		.RANGE("A"+ii).VALUE=TRANSFORM(START)
		.RANGE("B"+ii).VALUE=TRANSFORM(END)
		.RANGE("C"+ii).VALUE=TRANSFORM(whc_ctnin)
		.RANGE("D"+ii).VALUE=TRANSFORM(whc_ctnout)
		.RANGE("E"+ii).VALUE=TRANSFORM(whc_gohin)
		.RANGE("F"+ii).VALUE=TRANSFORM(whc_gohout)
		.RANGE("G"+ii).VALUE=TRANSFORM(whn_ctnin)
		.RANGE("H"+ii).VALUE=TRANSFORM(whn_ctnout)
		.RANGE("I"+ii).VALUE=TRANSFORM(whn_gohin)
		.RANGE("J"+ii).VALUE=TRANSFORM(whn_gohout)
		.RANGE("K"+ii).VALUE=TRANSFORM(whm_ctnin)
		.RANGE("L"+ii).VALUE=TRANSFORM(whm_ctnout)
		.RANGE("M"+ii).VALUE=TRANSFORM(whm_gohin)
		.RANGE("N"+ii).VALUE=TRANSFORM(whm_gohout)
		.RANGE("O"+ii).VALUE=TRANSFORM(wh4_ctnin)
		.RANGE("P"+ii).VALUE=TRANSFORM(wh4_ctnout)
		.RANGE("Q"+ii).VALUE=TRANSFORM(wh4_gohin)
		.RANGE("R"+ii).VALUE=TRANSFORM(wh4_gohout)

	ENDWITH

	STORE 'osheet3' TO whichsheet

	WITH &whichsheet

		ii=TRANS(i)
		.RANGE("A2").VALUE="Report from "+DTOC(displayfrom)+" to "+DTOC(displayto)
		.RANGE("A"+ii).VALUE=TRANSFORM(START)
		.RANGE("B"+ii).VALUE=TRANSFORM(END)
		.RANGE("C"+ii).VALUE=TRANSFORM(ccontainer)
		.RANGE("D"+ii).VALUE=TRANSFORM(ncontainer)
		.RANGE("E"+ii).VALUE=TRANSFORM(mcontainer)


	ENDWITH

	STORE 'osheet4' TO whichsheet

	WITH &whichsheet

		ii=TRANS(i)
		.RANGE("A2").VALUE="Report from "+DTOC(displayfrom)+" to "+DTOC(displayto)
		.RANGE("A"+ii).VALUE=TRANSFORM(START)
		.RANGE("B"+ii).VALUE=TRANSFORM(END)
		.RANGE("C"+ii).VALUE=TRANSFORM(kmxdock)
		.RANGE("D"+ii).VALUE=TRANSFORM(kmsolids)
		.RANGE("E"+ii).VALUE=TRANSFORM(kmtilttray)


	ENDWITH





	i=i+1
	SKIP
ENDDO
SELECT sumrpt2
USE

* SAVE

TRY
	DELETE FILE c:\tempfox\sumrpt.XLS
	xfile="C:\tempfox\sumrpt.xls"
CATCH
	DELETE FILE h:\fox\sumrpt.XLS
	xfile="h:\fox\sumrpt.xls"
ENDTRY

oworkbook.SAVEAS(xfile)
CLEAR
