* Print the sheets showing 2011 1/3 balances.

#DEFINE xlEdgeBottom 9
#DEFINE xlContinuous 1
#DEFINE xlMedium -4138
#DEFINE xlNone -4142

SET SAFETY OFF

LOCAL oExcel, oWorkbook, oWorksheet
LOCAL lnRow, lcRow, lcXLFile, lcPrintMark, lnJan1VacBalanceInDays, lnJan1VacRolloverDays, lnPersonalDays

*lcXLFile = GETFILE()
lcXLFile = 'C:\ACCRUALS_2011_FILES\vacation_01_03_2011.xls'

oExcel = CREATEOBJECT("excel.application")
oExcel.VISIBLE = .T.

oWorkbook = oExcel.workbooks.OPEN(lcXLFile)

*oWorkbook.SAVEAS(lcFiletoSaveAs)

oWorksheet = oWorkbook.Worksheets[1]

FOR lnRow = 3 TO 70
	lcRow = ALLTRIM(STR(lnRow))

	* SHOULD WE PRINT THIS ROW?
	lcPrintMark = "N"
	
	IF NOT ISNULL( oWorksheet.RANGE("Z" + lcRow).VALUE ) THEN
		lcPrintMark = UPPER(ALLTRIM(oWorksheet.RANGE("Z" + lcRow).VALUE))
	ENDIF
	
	lnJan1VacBalanceInDays = oWorksheet.RANGE("I" + lcRow).VALUE / 8
	lnJan1VacRolloverDays = oWorksheet.RANGE("J" + lcRow).VALUE / 8
	lnPersonalDays = oWorksheet.RANGE("U" + lcRow).VALUE / 8
	
*!*		IF (lcPrintMark = "Y") AND (lnJan1VacBalanceInDays > 0) THEN
	IF (lcPrintMark = "Y") THEN
	
		* PRINT VAC EXPLANATION SHEET
		
		* first populate the print area of the spreadsheet
		
		* Employee name
		oWorksheet.RANGE("AH3").VALUE = "For: " + ALLTRIM(oWorksheet.RANGE("C" + lcRow).VALUE)
		
		* Employee Seniority Date
		oWorksheet.RANGE("AH7").VALUE = "Your effective Hire Date was: " + TRANSFORM(TTOD(oWorksheet.RANGE("G" + lcRow).VALUE))
		
		* 1/1 vac Balance
		oWorksheet.RANGE("AI9").VALUE = lnJan1VacBalanceInDays
		
		IF lnJan1VacRolloverDays > 0 then
			oWorksheet.RANGE("AH10").VALUE = "Your unused Vacation days that rolled over from 2010 are: "
			oWorksheet.RANGE("AI10").VALUE = lnJan1VacRolloverDays
			oWorksheet.RANGE("AJ10").VALUE = "days"

			* UNDERLINE cell columns to be totaled...
			oWorksheet.RANGE("AI10").Borders(xlEdgeBottom).LineStyle = xlContinuous

			oWorksheet.RANGE("AH11").VALUE = "Total Vacation days: "
			oWorksheet.RANGE("AI11").VALUE = (lnJan1VacBalanceInDays + lnJan1VacRolloverDays)
			oWorksheet.RANGE("AJ11").VALUE = "days"
		ELSE
			* CLEAR THE AREA
			oWorksheet.RANGE("AH10","AJ11").ClearContents
			* un-UNDERLINE 
			oWorksheet.RANGE("AI10").Borders(xlEdgeBottom).LineStyle = xlNone
		ENDIF

		oWorksheet.RANGE("AI13").VALUE = lnPersonalDays
		
		
		* division
		*oWorksheet.RANGE("AH20").VALUE = "Div: " + PADL(INT(oWorksheet.RANGE("A" + lcRow).VALUE),2,"0")
		oWorksheet.RANGE("AH20").VALUE = "Div: " + oWorksheet.RANGE("A" + lcRow).VALUE
		
		* YAY PRINT!!!
		oWorksheet.PageSetup.PrintArea = "$AH$2:$AJ$21"
		oWorksheet.PrintOut()
		
		* MARK AS PRINTED
		oWorksheet.RANGE("AB" + lcRow).VALUE = "Y"
	ENDIF
	
	
*!*		IF ISNULL( oWorksheet.RANGE("Z" + lcRow).VALUE ) THEN
*!*			EXIT for
*!*		ENDIF

ENDFOR


* SAVE CHANGES

oWorkbook.Save()

oWorkbook.Close()

oExcel.Quit()