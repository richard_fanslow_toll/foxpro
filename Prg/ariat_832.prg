utilsetup("ARIAT_832")
Close Data All

Public lTesting
Public Array a856(1)

goffice="K"
gmasteroffice="K"

lTesting = .F.
Do m:\dev\prg\_setvars With lTesting

*Use F:\wh\upcmast Alias upcmast In 0 Shared

useca("upcmast","wh",,,"select * from upcmast where accountid = 6532",,"upcmast")

NumAdded = 0
NumUpdated = 0

Select * ;
  FROM upcmast ;
  WHERE .F. ;
  INTO Cursor tempmast Readwrite

Alter Table tempmast Drop Column upcmastid

set step on 
lcCurrDir = ""

useca("ackdata","wh")

If !lTesting
  lcPath = 'F:\FTPUSERS\ARIAT\Stylemaster\'
  lcArchivePath = 'F:\FTPUSERS\ARIAT\Stylemaster\archive\'
  tsendto="cheri.foster@tollgroup.com,kimberly.Sallee@tollgroup.com,jace.sipes@tollgroup.com"
  tcc="PGAIDIS@FMIINT.COM"
Else
  lcPath = 'F:\FTPUSERS\ARIAT\Stylemaster\'
  lcArchivePath = 'F:\FTPUSERS\ARIAT\Stylemaster\archive\'
  tsendto="PGAIDIS@FMIINT.COM"
  tcc=""
Endif
Cd &lcPath
len1 = Adir(ary1,"*.*")
If len1 = 0
  Wait Window "No files found...exiting" Timeout 2
  Close Data All
  schedupdate()
  _Screen.Caption=gscreencaption
  On Error
  Return
Endif

lcAddedStr  ="Styles Added:"+Chr(13)
lcUpdateStr ="Styles Updated:"+Chr(13)
ii=1

For Thisfile = 1 To len1
  cFilename = Alltrim(ary1[thisfile,1])
  xfile = lcPath+cFilename
  Do m:\dev\prg\createx856a
  Do m:\dev\prg\loadedifile With xfile,"*","TILDE"

  m.addby = "FILE"+Alltrim(Transform(Thisfile))
  m.adddt = Datetime()
  m.addproc = "FILE"+Alltrim(Transform(Thisfile))  &&"ARIAT832"
  m.accountid = 6532
  m.pnp = .T.
  m.uom = "EA"
  m.info = ""
  lcItemType =""

  Select x856
  Goto Top
  Scan
    Do Case
    Case Trim(x856.segment) = "ISA"
      cisa_num = Alltrim(x856.f13)
    Case Trim(x856.segment) = "GS"
      cgs_num = Alltrim(x856.f6)
      lcCurrentGroupNum = Alltrim(x856.f6)
    Case Trim(x856.segment) = "ST" And Trim(x856.f1) = "832"
		 m.groupnum=lcCurrentGroupNum
		 m.isanum=cisa_num
		 m.transnum=x856.f2
		 m.edicode="OW"
		 m.accountid=6532
		 m.loaddt=date()
		 m.loadtime=datetime()
		 m.filename=xfile
		 insertinto("ackdata","wh",.t.)
    Case x856.segment = "BCT"
      lcItemType = Upper(Allt(x856.f9))
      m.itemtype= lcItemType
*      m.info = m.info+Chr(13)+"TYPE*"+lcItemType
    Case x856.segment = "LIN"
      m.itemtype= lcItemType
      m.info = m.info+Chr(13)+"TYPE*"+lcItemType
**reset in case segments do not exist for upc - mvw 03/19/14
      Store "" To m.sid,m.descrip
      Store 0 To m.price,m.rprice

      If x856.f6 = "UP"
        m.upc = Allt(x856.f7)
      Endif
      If x856.f2 = "VN"
        m.style = Allt(x856.f3)
      Endif
      If x856.f8 = "CM"
        m.info = m.info+Chr(13)+"NRFCOLOR*"+Allt(x856.f9)
* m.color = Allt(x856.f9)
      Endif
      If x856.f10 = "SM"
        m.info = m.info+Chr(13)+"NRFSIZE*"+Allt(x856.f11)
      Endif

      If x856.f22 = "SE"
        m.info = "SEASON*"+Allt(x856.f23)
      Endif

    Case x856.segment = "PID" And x856.f1 = "F" And x856.f2 = "08"
      m.descrip = Allt(x856.f5)

    Case x856.segment = "PID" And x856.f1 = "F" And x856.f2 = "73"
      m.info = m.info+Chr(13)+"COLORDESC*"+Allt(x856.f5)

    Case x856.segment = "PID" And x856.f1 = "F" And x856.f2 = "74"
      m.color= x856.f5
      m.color = Strtran(Allt(x856.f5),"  "," ")
      m.info = m.info+Chr(13)+"ORIGSIZE*"+Allt(x856.f5)

    Case x856.segment = "PID" And x856.f1 = "F" And x856.f2 = "LO"
      m.htscode =Allt(x856.f5)
      m.info = m.info+Chr(13)+"HTSCODE*"+Allt(x856.f5)

    Case x856.segment = "PID" And x856.f1 = "F" And x856.f2 = "SC"
      m.coo=Allt(x856.f5)

    Case x856.segment = "G55"
      m.weight = Val(Alltrim(Transform(f23)))
      Insert Into tempmast From Memvar
      Scatter Memvar Memo Blank
      m.info = ""

    Endcase

  Endscan
  Select tempmast
  Locate

  RecCtr = 0
  Select tempmast
  Replace Color With Strtran(Color,"   "," ") For "   "$Alltrim(Color)
  Replace Color With Strtran(Color,"  "," ")  For "  "$Alltrim(Color)
  Replace Color With Substr(Color,1,4)+" "+Substr(Color,5)
  Replace Color With Substr(Color,1,4)+" "+Substr(Color,5) For Substr(Color,3,2)=".5"


  Select tempmast && has all new records.......
  Scan
    RecCtr = RecCtr +1
    Select upcmast
    Locate For accountid =tempmast.accountid And upc=tempmast.upc

    Wait Window At 10,10 "Checking Record # "+Transform(RecCtr) Nowait

    If !Found()
      NumAdded = NumAdded +1
      Select tempmast
      Scatter Fields Except upcmastid Memvar Memo
      m.adddt = Datetime()
      m.updatedt =  Datetime()
	  m.upcmastid=sqlgenpk("upcmast","wh")

      ii=ii+1
      Select upcmast  && add the recored to SQL
      Insert Into upcmast From Memvar

      lcAddedStr = lcAddedStr +m.upc+"-"+m.style+"-"+m.color+"-"+m.id+Chr(13)
    Else
      Replace upcmast.price    With tempmast.price   In upcmast
      Replace upcmast.price    With tempmast.price   In upcmast
      Replace upcmast.rprice   With tempmast.rprice  In upcmast
      Replace upcmast.Style    With tempmast.Style   In upcmast
      Replace upcmast.Color    With tempmast.Color   In upcmast
      Replace upcmast.Id       With tempmast.Id      In upcmast
      Replace upcmast.updatedt With tempmast.adddt   In upcmast
      Replace upcmast.Info     With tempmast.Info    In upcmast
      Replace upcmast.itemtype With tempmast.itemtype In upcmast
      Replace upcmast.htscode  With tempmast.htscode  In upcmast
      Replace upcmast.Descrip  With tempmast.Descrip  In upcmast
      NumUpdated = NumUpdated +1
      lcUpdateStr = lcUpdateStr +tempmast.upc+"-"+tempmast.Style+"-"+tempmast.Color+"-"+tempmast.Id+Chr(13)
&& add in all the updates
    Endif
  Endscan

*Set Step On

  Select tempmast
  Zap

  cLoadFile = (lcPath+cFilename)
  cArchiveFile = (lcArchivePath+cFilename)
  Copy File [&cLoadFile] To [&cArchiveFile]
  Delete File [&cLoadFile]

Endfor

tu("ackdata")

tu("upcmast")  && push new records and record updates back into sql

tattach = ""
tFrom ="Toll EDI System Operations <toll-edi-ops@tollgroup.com>"
tsendto = "tmarg@fmiint.com,Cori.DalPorto@Ariat.Com,Matt.Hardenberg@Ariat.Com,mmsupport@Ariat.Com,Robert.Liu@Ariat.Com"
tmessage = "Ariat Stylemaster updated at: "+Ttoc(Datetime())+Chr(13)+;
  Chr(13)+lcAddedStr+Chr(13)+;
  Chr(13)+lcUpdateStr

tSubject = "Ariat Stylemaster Update"
Do Form m:\dev\frm\dartmail2 With tsendto,tFrom,tSubject,tcc,tattach,tmessage,"A"
Wait Window "All 832 Files finished processing...exiting" Timeout 2

Close Data All

schedupdate()
*_Screen.Caption=gscreencaption
On Error
