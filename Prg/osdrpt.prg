parameters lwebsite

if lwebsite
	use f:\wo\wodata\manifest in 0
	xacctname=acctname(xaccountid)
endif

select * ;
	from manifest ;
	where accountid=xaccountid ;
	and between(wo_date, xstartdt, xenddt) ;
	and (rcv_qty # pl_qty or !empty(remarks)) ;
	and office = xoffice ;
	into cursor temp

if lwebsite
	loadwebpdf("OSDRPT")
	use in manifest
endif