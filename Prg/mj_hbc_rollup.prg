utilsetup("MJ_HBC_ROLLUP")

SET EXCLUSIVE OFF
SET DELETED ON
SET TALK OFF
SET SAFETY OFF

schedupdate()
*!*  with _screen
*!*  	.autocenter = .t.
*!*  	.windowstate = 0
*!*  	.borderstyle = 1
*!*  	.width = 320
*!*  	.height = 210
*!*  	.top = 290
*!*  	.left = 110
*!*  	.closable = .t.
*!*  	.maxbutton = .t.
*!*  	.minbutton = .t.
*!*  	.scrollbars = 0
*!*  	.caption = "MJ_HBC_ROLLUP"
*!*  endwith

lTestrun = .t.  && Allows file browsing, step through, etc.
SET STEP ON  

IF !USED("edi_trigger")
	USE F:\3pl\DATA\edi_trigger SHARED IN 0
ENDIF

**DHW ** useca("outship","wh",,,"inlist(accountid,6303,6543) and consignee='HUDSON' and mod='J' and del_date>={"+DTOC(DATE()-14)+"}",,"outshipnj")
useca("outship","wh",,,"inlist(accountid,6303,6543) and (consignee='HUDSON' or consignee = 'NORDSTROM CANADA' or consignee = 'NORDSTROM RACK CANADA') and mod='J' and del_date>={"+DTOC(DATE()-14)+"}",,"outshipnj")

SELECT DISTINCT accountid, wo_num, ship_ref, appt_num, del_date, scac, bol_no, consignee FROM outshipnj INTO CURSOR b1 READWRITE

*!*	useca("outship","wh",,,"inlist(accountid,6303,6543) and consignee='HUDSON' and mod='J' and del_date>={"+dtoc(date()-14)+"}",,"outshipml")
*!*	select distinct accountid, wo_num, ship_ref, appt_num, del_date, scac, bol_no from outshipml into cursor mlb1 readwrite

****************************START NJ
** DHW ** SELECT DISTINCT accountid, wo_num, ship_ref FROM edi_trigger WHERE INLIST(accountid,6303,6543) AND consignee='HUDSON' AND fin_status='945 ROLLUP NEEDED' AND office ='N' INTO CURSOR t1 READWRITE
SELECT DISTINCT accountid, wo_num, ship_ref FROM edi_trigger WHERE INLIST(accountid,6303,6543) AND INLIST(consignee,'HUDSON','NORDSTROM CANADA','NORDSTROM RACK CANADA') AND fin_status='945 ROLLUP NEEDED' AND office ='N' INTO CURSOR t1 READWRITE


SELECT a.*, appt_num,del_date FROM t1 a LEFT JOIN b1 b ON a.accountid=b.accountid AND a.wo_num=b.wo_num AND a.ship_ref=b.ship_ref INTO CURSOR c1 READWRITE

SELECT DISTINCT accountid,wo_num,appt_num,del_date FROM c1 INTO CURSOR c2 READWRITE

SELECT a.accountid,a.wo_num,a.appt_num, a.del_date ,MAX(b.bol_no) FROM c2 a LEFT JOIN b1 b ON a.accountid=b.accountid AND a.wo_num=b.wo_num AND a.del_date=b.del_date ;
	GROUP BY a.accountid,a.wo_num,a.appt_num, a.del_date  INTO CURSOR c3 READWRITE

*!*	SELECT b1
*!*	SCAN FOR INLIST(accountid,6303,6543) AND consignee='HUDSON'
*!*		SELECT c3
*!*		LOCATE FOR c3.accountid=b1.accountid AND c3.wo_num=b1.wo_num AND c3.appt_num=b1.appt_num AND c3.del_date=b1.del_date
*!*		IF FOUND("c3")
*!*			REPLACE bol_no  WITH c3.max_bol_no IN b1
*!*		ENDIF   
*!*	ENDSCAN

SELECT outshipnj
**DHW** SCAN FOR INLIST(accountid,6303,6543) AND consignee='HUDSON'
SCAN FOR INLIST(accountid,6303,6543) AND INLIST(consignee,'HUDSON','NORDSTROM CANADA','NORDSTROM RACK CANADA')
	SELECT c3
	LOCATE FOR c3.accountid=outshipnj.accountid AND c3.wo_num=outshipnj.wo_num AND c3.appt_num=outshipnj.appt_num AND c3.del_date=outshipnj.del_date
	IF FOUND("c3")
		REPLACE bol_no  WITH c3.max_bol_no IN outshipnj
	ENDIF   
ENDSCAN

SET STEP ON

SELECT outshipnj
tu()

SET STEP ON
SELECT edi_trigger
**DHW** SCAN FOR INLIST(accountid,6303,6543) AND consignee='HUDSON' AND fin_status='945 ROLLUP NEEDED'
SCAN FOR INLIST(accountid,6303,6543) AND INLIST(consignee,'HUDSON','NORDSTROM CANADA','NORDSTROM RACK CANADA') AND fin_status='945 ROLLUP NEEDED'
	SELECT c3
	LOCATE FOR c3.accountid=edi_trigger.accountid AND c3.wo_num=edi_trigger.wo_num
	IF FOUND("c3")
		REPLACE bol  WITH c3.max_bol_no  IN edi_trigger
		REPLACE processed WITH .F. IN edi_trigger
	ENDIF   
ENDSCAN
USE IN outshipnj

****************************START ML

**DHW** useca("outship","wh",,,"inlist(accountid,6303,6543) and consignee='HUDSON' and mod='J' and del_date>={"+DTOC(DATE()-14)+"}",,"outshipml")
useca("outship","wh",,,"inlist(accountid,6303,6543) and (consignee='HUDSON' or consignee = 'NORDSTROM CANADA' or consignee = 'NORDSTROM RACK CANADA') and mod='J' and del_date>={"+DTOC(DATE()-14)+"}",,"outshipml")
SELECT DISTINCT accountid, wo_num, ship_ref, appt_num, del_date, scac, bol_no FROM outshipml INTO CURSOR mlb1 READWRITE

**DHW** SELECT DISTINCT accountid, wo_num, ship_ref FROM edi_trigger WHERE INLIST(accountid,6303,6543) AND consignee='HUDSON' AND fin_status='945 ROLLUP NEEDED' AND office ='L' INTO CURSOR mlt1 READWRITE
SELECT DISTINCT accountid, wo_num, ship_ref FROM edi_trigger WHERE INLIST(accountid,6303,6543) AND INLIST(consignee,'HUDSON','NORDSTROM CANADA','NORDSTROM RACK CANADA') AND fin_status='945 ROLLUP NEEDED' AND office ='L' INTO CURSOR mlt1 READWRITE

SELECT a.*, appt_num,del_date FROM mlt1 a LEFT JOIN mlb1 b ON a.accountid=b.accountid AND a.wo_num=b.wo_num AND a.ship_ref=b.ship_ref INTO CURSOR mlc1 READWRITE

SELECT DISTINCT accountid,wo_num,appt_num,del_date FROM mlc1 INTO CURSOR mlc2 READWRITE

SELECT a.accountid,a.wo_num,a.appt_num, a.del_date ,MAX(bol_no) FROM mlc2 a LEFT JOIN mlb1 b ON a.accountid=b.accountid AND a.wo_num=b.wo_num AND a.del_date=b.del_date ;
	GROUP BY a.accountid,a.wo_num,a.appt_num, a.del_date  INTO CURSOR mlc3 READWRITE

SELECT outshipml
**DHW** SCAN FOR INLIST(accountid,6303,6543) AND consignee='HUDSON'
SCAN FOR INLIST(accountid,6303,6543) AND INLIST(consignee,'HUDSON','NORDSTROM CANADA','NORDSTROM RACK CANADA')
	SELECT mlc3
	LOCATE FOR mlc3.accountid=outshipml.accountid AND mlc3.wo_num=outshipml.wo_num AND mlc3.appt_num=outshipml.appt_num AND mlc3.del_date=outshipml.del_date
	IF FOUND("MLc3")
		REPLACE bol_no  WITH mlc3.max_bol_no IN outshipml
	ENDIF   
ENDSCAN

SELECT outshipml
tu()


SELECT edi_trigger
**DHW** SCAN FOR INLIST(accountid,6303,6543) AND consignee='HUDSON' AND fin_status='945 ROLLUP NEEDED'
SCAN FOR INLIST(accountid,6303,6543) AND INLIST(consignee,'HUDSON','NORDSTROM CANADA','NORDSTROM RACK CANADA') AND fin_status='945 ROLLUP NEEDED'
	SELECT mlc3
	LOCATE FOR mlc3.accountid=edi_trigger.accountid AND mlc3.wo_num=edi_trigger.wo_num
	IF FOUND("MLc3")
		REPLACE bol  WITH mlc3.max_bol_no  IN edi_trigger
*		REPLACE processed WITH .F. IN edi_trigger
	ENDIF   
ENDSCAN


CLOSE DATABASES ALL

schedupdate()
_SCREEN.CAPTION=gscreencaption
ON ERROR
