runack("EDIPOLLER_BILLABONG")

CLOSE DATABASES ALL
CLEAR ALL
RELEASE ALL
cPath = ["M:\DEV\PRG; M:\DEV\PROJ"]
SET PATH TO &cPath ADDITIVE

Public tsendto,tcc,tsendtoerr,tccerr

tsendto="pgaidis@fmiint.com"
tcc    ="pgaidis@fmiint.com"
tsendtoerr="pgaidis@fmiint.com"
tccerr="pgaidis@fmiint.com"


WITH _SCREEN
	.AUTOCENTER = .T.
	.WINDOWSTATE = 0
	.BORDERSTYLE = 1
	.WIDTH = 260
	.HEIGHT = 130
	.TOP = 850
	.LEFT = 380
	.CLOSABLE = .F.
	.MAXBUTTON = .F.
	.MINBUTTON = .F.
	.SCROLLBARS = 0
	.CAPTION = "BILLABONG POLLER - EDI"
ENDWITH
TRY
	SET STEP ON 
	DO m:\dev\prg\_setvars
	PUBLIC lSkipError
	lSkipError =  .F.
	SET STATUS BAR OFF
	SET SYSMENU OFF
	ON ESCAPE CANCEL
	SELECT 0
	USE F:\3pl\DATA\edipollertrip SHARED ALIAS eob
	REPLACE eob.billabong WITH .F.
	USE IN eob

	SELECT 0
	USE F:\3pl\DATA\last945time SHARED ALIAS oblast
	REPLACE checkflag WITH .T. FOR poller = "BBG"
	REPLACE tries WITH 0 FOR poller = "BBG" IN oblast
	USE IN oblast

	ASSERT .F. MESSAGE "Going into poller run"
	DO FORM m:\dev\frm\edi_jobhandler_billabong

CATCH TO oErr
	ASSERT .F. MESSAGE "AT CATCH SECTION"
	SET STEP ON 
	IF !lSkipError
		tfrom    ="TGF EDI Processing Center <tgf-transload-ops@tollgroup.com>"
		tsubject = "Billabong Outbound EDI Poller Error ("+TRANSFORM(oErr.ERRORNO)+") at "+TTOC(DATETIME())
		tattach  = ""
		SELECT 0
		USE F:\3pl\DATA\mailmaster ALIAS mm
		LOCATE
		LOCATE FOR mm.edi_type = 'MISC' AND mm.taskname = "GENERAL"
		tsendto = IIF(mm.use_alt,sendtoalt,sendto)
		tcc = IIF(mm.use_alt,ccalt,cc)
		USE IN mm

		tmessage = "Billabong Outbound 945 Poller Major Error..... Please fix me........!"
		lcSourceMachine = SYS(0)

		tmessage =tmessage+ "Try/CATCH Exeception Message:"+CHR(13)+;
			[  Error: ] + STR(oErr.ERRORNO) +CHR(13)+;
			[  LineNo: ] + STR(oErr.LINENO) +CHR(13)+;
			[  Message: ] + oErr.MESSAGE +CHR(13)+;
			[  Procedure: ] + oErr.PROCEDURE +CHR(13)+;
			[  Details: ] + oErr.DETAILS +CHR(13)+;
			[  StackLevel: ] + STR(oErr.STACKLEVEL) +CHR(13)+;
			[  LineContents: ] + oErr.LINECONTENTS+CHR(13)+;
			[  UserValue: ] + oErr.USERVALUE+CHR(13)+;
			[  Computer:  ] +lcSourceMachine+CHR(13)
		DO FORM m:\dev\FRM\dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
	ENDIF
	SELECT 0
	USE F:\3pl\DATA\edipollertrip SHARED ALIAS e1
	REPLACE e1.billabong WITH .T.
	CLOSE DATABASES ALL
FINALLY
	
*!*		clear CLASSLIB m:\dev\lib\stuffsql.vcx
	SET CLASSLIB TO
	SET STATUS BAR ON
	ON ERROR
ENDTRY