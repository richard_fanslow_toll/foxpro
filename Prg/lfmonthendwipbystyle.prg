* LFMonthEndWIPByStyle report
*
* For LifeFactory/Chris Malcolm.
*
* run immediately after midnight at end of each month to get the snapshot, save to a spreadsheet
* 
* Because our schedulers do not offer 'last day of month,' we will schedule for 12:01am on day 1 of each month, but name the spreadsheet as prior month.
*
* In wms allow user to load prior snapshots by selecting 'LF Month-End WIP by Style' under the Reports-2 Menu.
*
* build exe as F:\UTIL\LF\LFMONTHENDWIPBYSTYLE.EXE

LOCAL lTestMode, loLFMONTHENDWIPBYSTYLE, lcProcessName, lnError
lTestMode = .F.

DECLARE INTEGER CreateMutex IN Win32API ;
	STRING @lpMutexAttributes, ;
	STRING bInitialOwner, ;
	STRING @lpName

DECLARE INTEGER GetLastError IN Win32API

* if running in EXE check to see if already running
IF _VFP.STARTMODE > 0 THEN
	lcProcessName = "LFMONTHENDWIPBYSTYLE"
	CreateMutex(0,CHR(1),@lcProcessName)
	lnError = GetLastError()
	IF lnError = 183
		WAIT WINDOW TIMEOUT 5 "This program is already running..."
		IF NOT lTestMode THEN
			schedupdate()
		ENDIF
		RETURN .F.
	ENDIF
ENDIF

loLFMONTHENDWIPBYSTYLE = CREATEOBJECT('LFMONTHENDWIPBYSTYLE')

utilsetup("LFMONTHENDWIPBYSTYLE")

loLFMONTHENDWIPBYSTYLE.MAIN( lTestMode )

IF NOT lTestMode THEN
	schedupdate()
ENDIF

CLOSE DATABASES ALL
RETURN

#DEFINE LOGIT 1
#DEFINE WAITIT 2
#DEFINE NOWAITIT 4
#DEFINE SENDIT 8
#DEFINE CRLF CHR(13) + CHR(10)
#DEFINE RETURN_DATA_MANDATORY .F.
#DEFINE RETURN_DATA_NOT_MANDATORY .T.
#DEFINE CSVFORMAT 6
#DEFINE WRITE_ONLY_UNBUFFERED 11
#DEFINE COMMA ","

DEFINE CLASS LFMONTHENDWIPBYSTYLE AS CUSTOM

	cProcessName = 'LFMONTHENDWIPBYSTYLE'

	lTestMode = .F.  && .F. FOR NORMAL OPERATION, .T. TO SUPPRESS ALL BUT DEVELOPER EMAILS AND CHANGE INPUT TABLE, ETC.

	dToday = DATE()

	lAutoYield = .T.

	cStartTime = TTOC(DATETIME())

	* wait window properties
	nWaitWindowTimeout = 2
	lWaitWindowIsOn = .T.

	* logfile properties
	lLoggingIsOn = .T.
	cLogFile = 'F:\UTIL\LFMONTHENDWIPBYSTYLE\LOGFILES\LFMONTHENDWIPBYSTYLE_log.txt'

	* INTERNAL email properties
	lSendInternalEmailIsOn = .T.
	cFrom = 'mark.bennett@tollgroup.com'
	cSendTo = 'mbennett@fmiint.com'
	cCC = ''
	cSubject = "LF Month End WIP By Style Report process for " + TRANSFORM(DATETIME())
	cAttach = ''
	cBodyText = ''

	cCOMPUTERNAME = ''
	cUSERNAME = ''


	FUNCTION INIT
		IF NOT DODEFAULT()
			RETURN .F.
		ENDIF
		WITH THIS
			*SET RESOURCE OFF
			CLOSE DATA
			SET CENTURY ON
			*SET DATE AMERICAN
			*SET HOURS TO 24
			SET DECIMALS TO 2
			SET ANSI ON
			SET TALK OFF
			SET DELETED ON
			SET CONSOLE OFF
			SET EXCLUSIVE OFF
			SET SAFETY OFF
			SET EXACT OFF
			SET STATUS BAR ON
			SET SYSMENU OFF
			SET ENGINEBEHAVIOR 70
			SET MULTILOCKS ON
			_VFP.AUTOYIELD = .lAutoYield
			.cCOMPUTERNAME = GETENV("COMPUTERNAME")
			.cUSERNAME = GETENV("USERNAME")
			.lLoggingIsOn = .lLoggingIsOn AND NOT EMPTY(.cLogFile)
		ENDWITH
	ENDFUNC


	FUNCTION DESTROY
		WITH THIS
			IF .lLoggingIsOn  THEN
				SET ALTERNATE OFF
				SET ALTERNATE TO
			ENDIF
		ENDWITH
		DODEFAULT()
	ENDFUNC


	FUNCTION MAIN
		LPARAMETERS tlTestMode
		WITH THIS
			LOCAL lnRow, lcRow, lcFiletoSaveAs, lnNumberOfErrors
			LOCAL oExcel, oWorkbook, loError, ldToday
			LOCAL lcOutship, lcOutdet, lcTemplateFile

			TRY
				lnNumberOfErrors = 0

				.lTestMode = tlTestMode

				ldToday = .dToday
				* IF RUN ON DAY 1 JUST AFTER MIDNIGHT (THE NORMAL MODE), WE WANT TO PUSH THE DATE BACK ONE DAY SO IT REFLECTS THE PRIOR MONTH.
				IF DAY(ldToday) = 1 THEN
					ldToday = ldToday - 1
				ENDIF

				IF .lTestMode THEN
					.cLogFile = 'F:\UTIL\LFMONTHENDWIPBYSTYLE\LOGFILES\LFMONTHENDWIPBYSTYLE_log_TESTMODE.txt'
					.cSendTo = 'mbennett@fmiint.com'
				ENDIF

				IF .lLoggingIsOn THEN
					SET ALTERNATE TO (.cLogFile) ADDITIVE
					SET ALTERNATE ON
				ENDIF

				.cStartTime = TTOC(DATETIME())

				.TrackProgress('LF Month End WIP By Style process started....', LOGIT+SENDIT)
				.TrackProgress('COMPUTERNAME = ' + .cCOMPUTERNAME, LOGIT+SENDIT)
				.TrackProgress('USERNAME = ' + .cUSERNAME, LOGIT+SENDIT)
				.TrackProgress('PROJECT = LFMONTHENDWIPBYSTYLE', LOGIT+SENDIT)
				IF .lTestMode THEN
					.TrackProgress('====> TEST MODE', LOGIT+SENDIT)
				ENDIF
				
				WAIT WINDOW NOWAIT "Creating LF Month End WIP By Style Report..."
				
				* first check to see if the file already has been created, in which case we will end the process. 
				* This will allow us to schedule it on a couple of different servers at 12:01am, 12:10am, 12:20am, to be sure it runs.
				
				lcFiletoSaveAs = "F:\UTIL\LFMONTHENDWIPBYSTYLE\REPORTS\LFMonthEndWIPByStyle_" + dt2month(ldToday)+".xls"
				.TrackProgress("lcFiletoSaveAs = " + lcFiletoSaveAs, LOGIT+SENDIT)
				
				IF FILE(lcFiletoSaveAs) THEN
					.TrackProgress("====> File " + lcFiletoSaveAs + " already exists. Ending Process", LOGIT+SENDIT)
					THROW
				ENDIF				

				xsqlexec("select * from outship where mod='I' and accountid=6034","outship",,"wh")
				xsqlexec("select * from outdet where mod='I' and accountid=6034","outdet",,"wh")

*!*					lcOutship = wf("N",6034) + "outship"
*!*					lcOutdet = wf("N",6034) + "outdet"

*!*					USE (lcOutship) IN 0 ALIAS OUTSHIP
*!*					USE (lcOutdet) IN 0 ALIAS OUTDET

				*SET STEP ON

				*!*	SELECT OS.*, SUM(IIF(od.units, od.totqty, 000000)) AS qtyfld, ;
				*!*		LEFT(DTOC(START),5) AS startview, LEFT(DTOC(MIN(CANCEL)),5) AS cancelview, ;
				*!*		LEFT(DTOC(picked),5) AS pickedview, LEFT(DTOC(MAX(called)),5) AS calledview, ;
				*!*		LEFT(DTOC(MAX(appt)),5) AS apptview, LEFT(DTOC(del_date),5) AS delview, ;
				*!*		IIF(sp, "Y", "N") AS spview, od.STYLE, od.COLOR, od.ID, od.PACK, SPACE(20) AS STATUS ;
				*!*		FROM OUTSHIP OS LEFT JOIN OUTDET od ON OS.outshipid = od.outshipid ;
				*!*		WHERE OS.accountid = 6034 AND del_date={} AND !notOnWip AND (ctnQty#0 OR masterpack) AND !sp ;
				*!*		GROUP BY ship_ref, STYLE, COLOR, ID, PACK ;
				*!*		INTO CURSOR xrpt READWRITE

				SELECT OS.*, SUM(IIF(od.units, od.totqty, 000000)) AS qtyfld, ;
					LEFT(DTOC(START),5) AS startview, LEFT(DTOC(MIN(CANCEL)),5) AS cancelview, ;
					LEFT(DTOC(picked),5) AS pickedview, LEFT(DTOC(MAX(called)),5) AS calledview, ;
					LEFT(DTOC(MAX(appt)),5) AS apptview, LEFT(DTOC(del_date),5) AS delview, ;
					IIF(sp, "Y", "N") AS spview, od.STYLE, od.COLOR, od.ID, od.PACK, SPACE(20) AS STATUS ;
					FROM OUTSHIP OS LEFT JOIN OUTDET od ON OS.outshipid = od.outshipid ;
					WHERE OS.accountid = 6034 AND EMPTYNUL(del_date) AND !notOnWip AND (ctnQty#0 OR masterpack) AND !sp ;
					GROUP BY ship_ref, STYLE, COLOR, ID, PACK ;
					INTO CURSOR xrpt READWRITE

				
				SELECT xrpt
				SCAN
					DO CASE
						CASE notOnWip AND LEFT(keyrec="CANCEL",6)
							REPLACE STATUS WITH "Ret To Stock"
						CASE !pulled
							REPLACE STATUS WITH "Not Pulled"
						CASE !EMPTYnul(del_date) AND (xups OR xfedex OR xusps)
							REPLACE STATUS WITH "Shipped"
						CASE !EMPTYnul(del_date)
							REPLACE STATUS WITH "Delivered"
						CASE !EMPTYnul(truckloaddt)
							REPLACE STATUS WITH "Loaded"
						CASE !EMPTYnul(staged)
							REPLACE STATUS WITH "Staged"
						CASE !EMPTYnul(labeled)
							REPLACE STATUS WITH "Labeled"
						CASE !EMPTYnul(picked)
							REPLACE STATUS WITH "Picked"
						OTHERWISE
							REPLACE STATUS WITH "Pulled"
					ENDCASE
				ENDSCAN

				lcTemplateFile = "F:\UTIL\LFMONTHENDWIPBYSTYLE\TEMPLATES\LFMonthEndWIPByStyle.xls"
				
				COPY FILE(lcTemplateFile) TO (lcFiletoSaveAs)
				
				
				* populate copy of template file
				oExcel = CREATEOBJECT("Excel.Application")
				oExcel.VISIBLE = .F.
				oExcel.displayalerts = .F.

				oWorkbook = oExcel.workbooks.OPEN(lcFiletoSaveAs)
				osheet1 = oWorkbook.worksheets[1]
				xsheet = "osheet1"
				i=5

				SELECT xrpt
				SCAN
					i=i+1
					ii=TRANSFORM(i)
					exceltext("A"+ii,ship_ref)
					exceltext("B"+ii,wo_num)
					exceltext("C"+ii,consignee)
					exceltext("D"+ii,cnee_ref)
					exceltext("E"+ii,STYLE)
					exceltext("F"+ii,COLOR)
					exceltext("G"+ii,ID)
					exceltext("H"+ii,PACK)
					exceltext("I"+ii,ship_via)
					exceltext("J"+ii,qtyfld)
					exceltext("K"+ii,startview)
					exceltext("L"+ii,cancelview)
					exceltext("M"+ii,STATUS)
					exceltext("N"+ii,calledview)
					exceltext("O"+ii,apptview)
					exceltext("P"+ii,appt_time)
					exceltext("Q"+ii,appt_num)
				ENDSCAN

				oWorkbook.SAVE()

				WAIT CLEAR

				oWorkbook.CLOSE()
				oExcel.QUIT()
				RELEASE oExcel
				
				
					IF FILE(lcFiletoSaveAs) THEN
					
						* attach file
						.cAttach = lcFiletoSaveAs

						IF NOT .lTestMode THEN
								
								* add to list of reports
								USE F:\UTIL\LFMONTHENDWIPBYSTYLE\DATA\REPLIST.DBF AGAIN IN 0 ALIAS REPLIST
								
								m.filename = lcFiletoSaveAs
								m.whenmade = DATETIME()
								m.cmonthyear = TRANSFORM(YEAR(ldToday)) + " -- " + UPPER(CMONTH(ldToday))
								m.reptday = ldToday
								m.cserver = .cCOMPUTERNAME
								
								SELECT REPLIST
								LOCATE FOR UPPER(ALLTRIM(filename)) == UPPER(ALLTRIM(m.filename))
								IF FOUND() THEN
									REPLACE whenmade WITH m.whenmade, cmonthyear WITH m.cmonthyear, reptday WITH m.reptday, cserver WITH m.cserver IN REPLIST
								ELSE
									INSERT INTO REPLIST FROM MEMVAR
								ENDIF
						ENDIF && NOT .lTestMode
						
					ELSE				
						.TrackProgress('!!!! There was an error creating file: ' + lcFiletoSaveAs,LOGIT+SENDIT)
					ENDIF && FILE(lcFiletoSaveAs) 

				.TrackProgress('LF Month End WIP By Style process ended normally.', LOGIT+SENDIT)

			CATCH TO loError

				.TrackProgress('There was an error.',LOGIT+SENDIT)
				.TrackProgress('The Error # is: ' + TRANSFORM(loError.ERRORNO), LOGIT+SENDIT)
				.TrackProgress('The Error Message is: ' + TRANSFORM(loError.MESSAGE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred in Program: ' + TRANSFORM(loError.PROCEDURE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred at line #: ' + TRANSFORM(loError.LINENO), LOGIT+SENDIT)
				lnNumberOfErrors = lnNumberOfErrors + 1
				.cSendTo = 'mbennett@fmiint.com'

				IF TYPE('oExcel') = 'O' AND NOT ISNULL(oExcel) THEN
					oExcel.QUIT()
				ENDIF

			ENDTRY

			WAIT CLEAR
			***************** INTERNAL email results ******************************
			.TrackProgress('About to send status email.',LOGIT+SENDIT)
			.TrackProgress('The logfile is: ' + .cLogFile, LOGIT+SENDIT)
			.TrackProgress('LF Month End WIP By Style process started: ' + .cStartTime, LOGIT+SENDIT)
			.TrackProgress('LF Month End WIP By Style process finished: ' + TTOC(DATETIME()), LOGIT+SENDIT)

			IF .lSendInternalEmailIsOn THEN
				DO FORM dartmail2 WITH .cSendTo,.cFrom,.cSubject,.cCC,.cAttach,.cBodyText,"A"
				.TrackProgress('Sent status email.',LOGIT)
			ELSE
				.TrackProgress('Did not send status email: THIS.lSendInternalEmailIsOn = FALSE.',LOGIT+SENDIT)
			ENDIF

		ENDWITH
		RETURN
	ENDFUNC && main


	PROCEDURE TrackProgress
		* do any combination of Wait Window, writing to logfile, and adding to body of email,
		* based on nFlags parameter.
		LPARAMETERS tcExpression, tnFlags
		WITH THIS
			IF BITAND(tnFlags,LOGIT) = LOGIT THEN
				IF .lLoggingIsOn THEN
					?
					? .cProcessName + "  " + TTOC(DATETIME()) + ": " + tcExpression
				ENDIF
			ENDIF
			IF .lWaitWindowIsOn THEN
				IF BITAND(tnFlags,NOWAITIT) = NOWAITIT THEN
					WAIT WINDOW tcExpression NOWAIT
				ENDIF
				IF BITAND(tnFlags,WAITIT) = WAITIT THEN
					WAIT WINDOW tcExpression TIMEOUT .nWaitWindowTimeout
				ENDIF
			ENDIF
			IF BITAND(tnFlags,SENDIT) = SENDIT THEN
				IF .lSendInternalEmailIsOn THEN
					.cBodyText = .cBodyText + tcExpression + CRLF + CRLF
				ENDIF
			ENDIF
		ENDWITH
	ENDPROC  &&  TrackProgress


ENDDEFINE
