* FTP upload csv driver punch data every morning for prior day

* EXE = F:\UTIL\TIMESTAR\TIMESTAR_FTP_PUNCHES.EXE

********************************************************************************
**** Must be run from a server with CuteFTP9 installed, such as FTP1 !!!!!!!****
********************************************************************************

*** NOTE: qcpunch_import.prg populates the table this program takes punches from.
*
* 4/25/2018 MB: changed fmiint.com email to my Toll email.
*

*!*	runack("TIMESTAR_FTP_PUNCHES")

LOCAL lnError, lcProcessName, loTIMESTAR_FTP_PUNCHES

DECLARE INTEGER CreateMutex IN Win32API ;
	STRING @lpMutexAttributes, ;
	STRING bInitialOwner, ;
	STRING @lpName

DECLARE INTEGER GetLastError IN Win32API

* if running in EXE check to see if already running
IF _VFP.STARTMODE > 0 THEN
	lcProcessName = "TIMESTAR_FTP_PUNCHES"
	CreateMutex(0,CHR(1),@lcProcessName)
	lnError = GetLastError()
	IF lnError = 183
		WAIT WINDOW TIMEOUT 10 "FTP Driver Punches to TimeStar process is already running..."
		RETURN .F.
	ENDIF
ENDIF

*!*	utilsetup("TIMESTAR_FTP_PUNCHES")


loTIMESTAR_FTP_PUNCHES = CREATEOBJECT('TIMESTAR_FTP_PUNCHES')
loTIMESTAR_FTP_PUNCHES.MAIN()

*!*	schedupdate()

CLOSE DATABASES ALL
RETURN

#DEFINE LOGIT 1
#DEFINE WAITIT 2
#DEFINE NOWAITIT 4
#DEFINE SENDIT 8
#DEFINE CRLF CHR(13) + CHR(10)
#DEFINE LF CHR(10)
#DEFINE RETURN_DATA_MANDATORY .F.
#DEFINE RETURN_DATA_NOT_MANDATORY .T.
#DEFINE DAILY_OT_MULTIPLIER 1.5
#DEFINE DAILY_OT20_MULTIPLIER 2.0
#DEFINE xlEdgeBottom 9
#DEFINE xlContinuous 1
#DEFINE xlMedium -4138

DEFINE CLASS TIMESTAR_FTP_PUNCHES AS CUSTOM

	cProcessName = 'TIMESTAR_FTP_PUNCHES'

	lTestMode = .F.  && .F. FOR NORMAL OPERATION, .T. TO SUPPRESS ALL BUT DEVELOPER EMAILS AND CHANGE INPUT TABLE, ETC.

	lAutoYield = .T.

	cStartTime = TTOC(DATETIME())

	* file properties
	nFileHandle = -1
	
	* table properties
	cDriverPunchTable = ''
	
	* processing properties
	lFTPSendFile = .T.   && don't attempt to FTP if FALSE
	
	* date properties
	dtNow = DATETIME()

	dToday = DATE()

	cToday = DTOC(DATE())

	* wait window properties
	nWaitWindowTimeout = 2
	lWaitWindowIsOn = .T.

	* logfile properties
	lLoggingIsOn = .T.
	cLogFile = 'F:\UTIL\TIMESTAR\LOGFILES\TIMESTAR_FTP_PUNCHES_LOG.TXT'

	* INTERNAL email properties
	lSendInternalEmailIsOn = .T.
	cFrom = 'Mark.Bennett@Tollgroup.com'
	cSendTo = 'Mark.Bennett@Tollgroup.com'
	cCC = ''
	cSubject = 'FTP Driver Punches to TimeStar process'
	cAttach = ''
	cBodyText = ''
	cTopBodyText = ''

	cCOMPUTERNAME = ''
	cUSERNAME = ''

	FUNCTION INIT
		IF NOT DODEFAULT()
			RETURN .F.
		ENDIF
		WITH THIS
			CLEAR
			*SET RESOURCE OFF
			CLOSE DATA
			SET CENTURY ON
			SET DATE AMERICAN
			SET HOURS TO 24
			SET ANSI ON
			SET TALK OFF
			SET DELETED ON
			SET CONSOLE OFF
			SET EXCLUSIVE OFF
			SET SAFETY OFF
			SET EXACT OFF
			SET STATUS BAR ON
			SET SYSMENU OFF
			SET ENGINEBEHAVIOR 70
			_VFP.AUTOYIELD = .lAutoYield
			.cCOMPUTERNAME = GETENV("COMPUTERNAME")
			.cUSERNAME = GETENV("USERNAME")
			IF .lTestMode THEN
				.cSendTo = 'Mark.Bennett@Tollgroup.com'
				.cCC = ''
				.cLogFile = 'F:\UTIL\TIMESTAR\LOGFILES\TIMESTAR_FTP_PUNCHES_LOG_TESTMODE.TXT'
			ENDIF
			.lLoggingIsOn = .lLoggingIsOn AND NOT EMPTY(.cLogFile)
		ENDWITH
	ENDFUNC


	FUNCTION DESTROY
		WITH THIS
			IF .lLoggingIsOn  THEN
				SET ALTERNATE OFF
				SET ALTERNATE TO
			ENDIF
		ENDWITH
		DODEFAULT()
	ENDFUNC


	FUNCTION MAIN
		WITH THIS
			LOCAL lnNumberOfErrors, oMySite, lnRetVal, lcLocalFolder, lcMessage, lcRootFileName, lcOutputFileCSV, ldStartDate, ldEndDate
			LOCAL lcRemotefolder, lcThisSite, lcThislogin, lcThispassword, lcArchivedFolder, lcArchivedFile, ldToday, ldYesterday, lcDateRange
			LOCAL lcBadge, lcPUNCHDTM, lcYear, lcMonth, lcDay, lcTIME, lcPUNCHCODE, lcPunchTime, ld4DaysAgo

			TRY
				lnNumberOfErrors = 0

				IF .lLoggingIsOn THEN
					SET ALTERNATE TO (.cLogFile) ADDITIVE
					SET ALTERNATE ON
				ENDIF

				.TrackProgress('FTP Driver Punches to TimeStar process started....', LOGIT+SENDIT+NOWAITIT)
				.TrackProgress('COMPUTERNAME = ' + .cCOMPUTERNAME, LOGIT+SENDIT)
				.TrackProgress('USERNAME = ' + .cUSERNAME, LOGIT+SENDIT)
				.TrackProgress('PROJECT = TIMESTAR_FTP_PUNCHES', LOGIT+SENDIT)
				IF NOT .lFTPSendFile THEN
					.TrackProgress('FTP SEND = OFF', LOGIT+SENDIT)
				ENDIF
				IF .lTestMode THEN
					.TrackProgress('                        TEST MODE                        ', LOGIT+SENDIT)
				ENDIF
				
				
				ldToday = .dToday
				ldYesterday = ldToday - 1
				ld4DaysAgo = ldToday - 4
				
				ldStartDate = ldYesterday
				ldEndDate = ldYesterday
				
*!*					* to force more days to export
*!*					ldStartDate = {^2015-01-27}
*!*					ldEndDate = {^2015-01-28}

				IF ldStartDate = ldEndDate THEN
					lcDateRange = DTOC(ldStartDate)
				ELSE
					lcDateRange = DTOC(ldStartDate) + ' thru ' + DTOC(ldEndDate)
				ENDIF

				.cSubject = 'FTP Driver Punches to TimeStar, from ' + lcDateRange

				IF .lTestMode THEN
					lcLocalFolder = 'F:\UTIL\INSPERITY\TESTPUNCHESOUT\'
					.cDriverPunchTable = 'F:\UTIL\QUALCOMM\TESTDATA\TDRPUNCHES'
				ELSE
					* PRODUCTION
					lcLocalFolder = 'F:\FTPUSERS\INSPERITY\OUT\PUNCHES\'
					.cDriverPunchTable = 'F:\UTIL\QUALCOMM\DATA\DRPUNCHES'
				ENDIF
												
				lcRootFileName = 'TOLLPUNCHES.CSV'
				lcOutputFileCSV = lcLocalFolder + lcRootFileName
				lcArchivedFolder = lcLocalFolder + 'ARCHIVED\'
				lcRemotefolder     = 'data\Punches'
				*lcThisSite         = 'ftp.insperitytimestar.com'
				lcThisSite         = 'timestarftp.insperity.com'
				*lcThislogin        = 'ftp_tgf'
				lcThislogin        = 'tgf'
				*lcThispassword     = 'CB3(8.Cd'
				lcThispassword     = 'iw<&Nz*8'

				.TrackProgress('Driver Punch Table = ' + .cDriverPunchTable, LOGIT+SENDIT)
				.TrackProgress('Remote folder = ' + lcRemotefolder, LOGIT+SENDIT)
				.TrackProgress('Local Folder = ' + lcLocalFolder, LOGIT+SENDIT)
				.TrackProgress('Local Archived Folder = ' + lcArchivedFolder, LOGIT+SENDIT)
				.TrackProgress('Upload file = ' + lcOutputFileCSV, LOGIT+SENDIT)
				
				USE (.cDriverPunchTable) IN 0 ALIAS DRIVERPUNCHTABLE
				
				************************************************************************************************
				************************************************************************************************
				*** first we need to create the output file (lcOutputFileCSV) from yesterday's driver punches
				
				* SELECT AND FORMAT THE DATA
				IF USED('CURPUNCHES') THEN
					USE IN CURPUNCHES
				ENDIF
				
				*!*	SELECT * FROM (.cDriverPunchTable) ;
				*!*		INTO CURSOR CURPUNCHES ;
				*!*		WHERE PUNCHDATE >= ldStartDate ;
				*!*		AND PUNCHDATE <= ldEndDate ;
				*!*		ORDER BY EMP_NUM, PUNCHDATE, PUNCHTIME 
					
				* 2/10/2015: changed to export any punches not marked as sent via ftp, which are not more than 4 days old
				SELECT * FROM DRIVERPUNCHTABLE ;
					INTO CURSOR CURPUNCHES ;
					WHERE PUNCHDATE >= ld4DaysAgo ;
					AND EMPTY(FTPEDDT) ;
					ORDER BY EMP_NUM, PUNCHDATE, PUNCHTIME 
				
				IF NOT USED('CURPUNCHES') OR EOF('CURPUNCHES') THEN
					.TrackProgress('=======> WARNING: No unsent punch data was found!', LOGIT+SENDIT)
					THROW
				ENDIF
 
				
				
*!*					SELECT CURPUNCHES
*!*					BROWSE

				
				* from Josh @ Insperity:

*!*			-	The data out of Qualcomm needs to be in a .CSV format. File name and worksheet name within the .CSV must be persistent.
*!*			-	A "clean" .CSV file with the data needed, and only that data, should be sent each time the upload process runs. 
*!*			This will ensure we are doing everything to avoid duplicate records being imported.
*!*			-	The columns in the .CSV file will be: badge_number, punch_datetime, punch_type
*!*			-	Badge_number: Number that references employee's profile within TimeStar. Must be left-padded with zeroes to 9 digits. For example, Bob's badge is 4500, the file should reflect 000004500.
*!*			 Be very careful about opening and saving your data file in Microsoft Excel, as it tends to cut off leading zeroes on fields. Use a notepad editor if needed.
*!*			-	Punch_Datetime: Date & time of punch. TimeStar's field for this data is in the "smalldatetime" format. For example: 7/17/2014 14:35  MM/DD/YYYY HH:MM
*!*			-	Punch_type: Type of punch (IND, OUT, INL). These are actually numeric codes within the database of TimeStar, and should be sent in the file as follows:
*!*			o	IND (in for day) = 1
*!*			o	INL (in for lunch) = 4
*!*			o	OUT (out for any case) = 90


				* Per Insperity, the csv file should look like this:

*!*				000004500,7/15/2014 5:55,1
*!*				000004500,7/15/2014 17:56,90
*!*				000003450,7/15/2014 5:55,1
*!*				000003450,7/15/2014 11:55,90
*!*				000003450,7/15/2014 13:45,4
*!*				000003450,7/15/2014 17:56,90
*!*				000000123,7/15/2014 7:55,1
*!*				000000123,7/15/2014 15:56,90

				* delete the output ftp file from previous send if it is still there.
				IF FILE(lcOutputFileCSV) THEN
					DELETE FILE (lcOutputFileCSV)
				ENDIF

				* write cursor to output csv file in FTP out folder
				* open file for writing
				.nFileHandle = FCREATE(lcOutputFileCSV)

				IF .nFileHandle = -1 THEN
					.TrackProgress("There was an error FCREATEing " + lcOutputFileCSV,LOGIT+SENDIT)
					THROW
				ENDIF

				* file has been opened for low-level access, OK TO CONTINUE

				* do --not-- write header line per Josh 4/23/14
				*!*	* write header line
				*!*	lcString = "legalln,inspid,incpay"
				*!*	FPUTS(.nFileHandle,lcString)

				* write data lines
				SELECT CURPUNCHES
				SCAN
					lcBadge = PADL(CURPUNCHES.EMP_NUM,9,'0')
							
					*lcMonth = ALLTRIM(TRANSFORM(MONTH(CURPUNCHES.PUNCHDATE)))
					*lcDay = ALLTRIM(TRANSFORM(DAY(CURPUNCHES.PUNCHDATE)))
					*lcYear = ALLTRIM(TRANSFORM(YEAR(CURPUNCHES.PUNCHDATE)))
					*lcPunchTime = ALLTRIM(CURPUNCHES.PUNCHTIME)
					*lcTIME = LEFT(CHR2TIME(lcPunchTime),5)
					*lcPUNCHDTM = lcMonth + "/" + lcDay + "/" + lcYear + " " + lcTime
					
					lcPUNCHDTM = ALLTRIM(TRANSFORM(CURPUNCHES.PUNCHDATE)) + " " + ALLTRIM(CURPUNCHES.PUNCHTIME)
					
					DO CASE
						CASE UPPER(ALLTRIM(CURPUNCHES.PUNCHTYPE)) == "IND"
							lcPUNCHCODE = "1"
						CASE UPPER(ALLTRIM(CURPUNCHES.PUNCHTYPE)) == "INL"
							lcPUNCHCODE = "4"
						CASE UPPER(ALLTRIM(CURPUNCHES.PUNCHTYPE)) == "OUT"
							lcPUNCHCODE = "90"
						OTHERWISE
							* INVALID PUNCH CODE
							lcPUNCHCODE = ""
					ENDCASE
				
					lcString = lcBadge + "," + lcPUNCHDTM + "," + lcPUNCHCODE
					FPUTS(.nFileHandle,lcString)
				ENDSCAN

				* force Windows to flush buffers immediately
				=FFLUSH(.nFileHandle, .T.)

				* close the CSV file
				IF FCLOSE(.nFileHandle) THEN
					* nothing
				ELSE
					.TrackProgress("There was an error FCLOSEing " + lcOutputFileCSV,LOGIT+SENDIT)
				ENDIF

				
				IF FILE(lcOutputFileCSV) THEN
					.TrackProgress('Successfully created .csv file: ' + lcOutputFileCSV, LOGIT+SENDIT)
				ELSE
					.TrackProgress('****************************************************************', LOGIT+SENDIT)
					.TrackProgress('==========> ERROR creating .csv file: ' + lcOutputFileCSV, LOGIT+SENDIT)
					THROW
				ENDIF
				
				IF .lFTPSendFile THEN

					************************************************************************************************
					************************************************************************************************
					**
					**    change all this below here to insert a trigger into SFTPJobs?
					
					
					
					*** now we need to ftp the output file to Insperity
					.TrackProgress('Now Connecting to ===> ' + lcThisSite, LOGIT+SENDIT)

					*Create a connection object and assign it to the variable
					oMySite = CREATEOBJECT("CuteFTPPro.TEConnection")
					oMySite.protocol = "FTPS_IMPLICIT"
					oMySite.HOST     = lcThisSite
					oMySite.login    = lcThislogin
					oMySite.PASSWORD = lcThispassword
					oMySite.useproxy = "BOTH"
					oMySite.CONNECT

					IF oMySite.isconnected >= 0
						.TrackProgress('ERROR: Could not connect to: ' + oMySite.HOST, LOGIT+SENDIT)
						THROW
					ELSE
						.TrackProgress('Now connected to: ' + oMySite.HOST, LOGIT+SENDIT)
					ENDIF
					
					lnRetVal = oMySite.remoteexists(lcRemotefolder)
					IF lnRetVal >= 0 THEN
						.TrackProgress('ERROR: Remote folder [' + lcRemotefolder + '] does not exist!' , LOGIT+SENDIT)
						THROW
					ENDIF

					oMySite.remotefolder = lcRemotefolder
					oMySite.localfolder = lcLocalFolder
					
					.TrackProgress('Target folder = ' + lcRemotefolder, LOGIT+SENDIT)
					.TrackProgress('Beginning upload of: ' + lcRootFileName, LOGIT+SENDIT)
					
					
	*!*					* delete the remote file if it exists from previous upload --- this has to be a cuteftp command!!!!! or set cuteftp to overwrite without prompting?
	*!*					IF FILE(lcUploadedFile) THEN
	*!*						DELETE FILE (lcUploadedFile)
	*!*					ENDIF
					lnRetVal = oMySite.remoteexists(lcRootFileName)
					IF lnRetVal < 0 THEN
						.TrackProgress(lcRootFileName + ' already exists in remote folder - deleting it before upload!' , LOGIT+SENDIT)
						* delete it
						oMySite.remoteremove(lcRootFileName)
					ELSE
						.TrackProgress(lcRootFileName + ' does NOT exist in remote folder!' , LOGIT+SENDIT)
					ENDIF
					
					
						
					oMySite.upload(lcRootFileName)

					lcMessage = oMySite.WAIT(-1,20)

					.TrackProgress('File Transfer message = ' + lcMessage, LOGIT+SENDIT)

					oMySite.CLOSE
					
					IF NOT ("FINISHED" $ UPPER(lcMessage)) THEN
						.TrackProgress('=====> WARNING: unexpected file transfer message - check the upload!', LOGIT+SENDIT)
						THROW
					ENDIF

					*********************************************************************************************************************
					* if we got to here, assume ftp process completed successfully; so mark the sent punches as FTPed so they are not sent again.
					SELECT DRIVERPUNCHTABLE
					SCAN FOR (PUNCHDATE >= ld4DaysAgo) AND EMPTY(FTPEDDT)
						REPLACE FTPEDDT WITH DATETIME() IN DRIVERPUNCHTABLE
					ENDSCAN
					*********************************************************************************************************************
					
					* archive the file we sent, if there was an error we'll have to resend it manually
					*lcArchivedFile = lcArchivedFolder + lcRootFileName + '.' + DTOS(DATE())	
					lcArchivedFile = lcArchivedFolder + DTOS(DATE()) + "_" + lcRootFileName
						
					COPY FILE (lcOutputFileCSV) TO (lcArchivedFile)
					
					IF FILE(lcArchivedFile) THEN
						DELETE FILE (lcOutputFileCSV)
						.cAttach = lcArchivedFile
					ELSE
						.TrackProgress('**** ERROR creating local archiving file: ' + lcArchivedFile, LOGIT+SENDIT)
						lnNumberOfErrors = lnNumberOfErrors + 1
						.cAttach = ''
					ENDIF
					
					IF FILE(lcOutputFileCSV) THEN
						.TrackProgress('**** ERROR in the archiving; did not delete local file: ' + lcOutputFileCSV, LOGIT+SENDIT)
						lnNumberOfErrors = lnNumberOfErrors + 1
					ENDIF

				ENDIF && .lFTPSendFile 
				

				.TrackProgress('FTP Driver Punches to TimeStar process ended normally....', LOGIT+SENDIT+NOWAITIT)

			CATCH TO loError

				.TrackProgress('There was an error.',LOGIT+SENDIT)
				.TrackProgress('The Error # is: ' + TRANSFORM(loError.ERRORNO), LOGIT+SENDIT)
				.TrackProgress('The Error Message is: ' + TRANSFORM(loError.MESSAGE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred in Program: ' + TRANSFORM(loError.PROCEDURE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred at line #: ' + TRANSFORM(loError.LINENO), LOGIT+SENDIT)
				lnNumberOfErrors = lnNumberOfErrors + 1
				IF TYPE('oMySite') = 'O' AND NOT ISNULL(oMySite) THEN
					oMySite.CLOSE()
				ENDIF
				IF .nFileHandle <> -1 THEN
					=FCLOSE(.nFileHandle)
				ENDIF
				CLOSE DATA

			ENDTRY

			CLOSE DATA
			WAIT CLEAR
			
			IF lnNumberOfErrors > 0 THEN
				.cSubject = '(ERRORS) ' + .cSubject
			ENDIF
			
			***************** INTERNAL email results ******************************
			.TrackProgress('About to send status email.',LOGIT)
			.TrackProgress('==================================================================================================================', SENDIT)
			.TrackProgress('The logfile is: ' + .cLogFile, SENDIT)
			.TrackProgress('FTP Driver Punches to TimeStar process started: ' + .cStartTime, LOGIT+SENDIT)
			.TrackProgress('FTP Driver Punches to TimeStar process finished: ' + TTOC(DATETIME()), LOGIT+SENDIT)

			IF .lSendInternalEmailIsOn THEN
				* try to trap error from not having dartmail dll's registered on user's pc...
				TRY
					.cBodyText = .cTopBodyText + CRLF + .cBodyText

					DO FORM dartmail2 WITH .cSendTo,.cFrom,.cSubject,.cCC,.cAttach,.cBodyText,"A"
					.TrackProgress('Sent status email.',LOGIT)
				CATCH TO loError
					*=MESSAGEBOX('There was an error sending the status email. Please contact Mark Bennett.',0+64,"KRONOS Temp Time")
					.TrackProgress('There was an error sending the status email.',LOGIT)
					.TrackProgress('The Error # is: ' + TRANSFORM(loError.ERRORNO), LOGIT)
					.TrackProgress('The Error Message is: ' + TRANSFORM(loError.MESSAGE), LOGIT)
					.TrackProgress('The Error occurred in Program: ' + TRANSFORM(loError.PROCEDURE), LOGIT)
					.TrackProgress('The Error occurred at line #: ' + TRANSFORM(loError.LINENO), LOGIT)
					lnNumberOfErrors = lnNumberOfErrors + 1
					CLOSE DATA
				ENDTRY

			ELSE
				.TrackProgress('Did not send status email: THIS.lSendInternalEmailIsOn = FALSE.',LOGIT)
			ENDIF

		ENDWITH
		RETURN
	ENDFUNC && main


	PROCEDURE TrackProgress
		* do any combination of Wait Window, writing to logfile, and adding to body of email,
		* based on nFlags parameter.
		LPARAMETERS tcExpression, tnFlags
		WITH THIS
			IF BITAND(tnFlags,LOGIT) = LOGIT THEN
				IF .lLoggingIsOn THEN
					?
					? .cProcessName + "  " + TTOC(DATETIME()) + ": " + tcExpression
				ENDIF
			ENDIF
			IF .lWaitWindowIsOn THEN
				IF BITAND(tnFlags,NOWAITIT) = NOWAITIT THEN
					WAIT WINDOW tcExpression NOWAIT
				ENDIF
				IF BITAND(tnFlags,WAITIT) = WAITIT THEN
					WAIT WINDOW tcExpression TIMEOUT .nWaitWindowTimeout
				ENDIF
			ENDIF
			IF BITAND(tnFlags,SENDIT) = SENDIT THEN
				IF .lSendInternalEmailIsOn THEN
					.cBodyText = .cBodyText + tcExpression + CRLF + CRLF
				ENDIF
			ENDIF
		ENDWITH
	ENDPROC  &&  TrackProgress


ENDDEFINE
