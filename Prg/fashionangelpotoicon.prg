
* Send Fashion Angel PO file to ICON
* Build EXE in F:\UTIL\FASHIONANGEL\

LOCAL loFAPOTOICON, llTestMode

llTestMode = .T.

IF NOT llTestMode THEN
	utilsetup("FAPOTOICON")
ENDIF


loFAPOTOICON = CREATEOBJECT('FAPOTOICON')

loFAPOTOICON.MAIN( llTestMode )


IF NOT llTestMode THEN
	schedupdate()
ENDIF

RETURN

#DEFINE LOGIT 1
#DEFINE WAITIT 2
#DEFINE NOWAITIT 4
#DEFINE SENDIT 8
#DEFINE CR CHR(13)
#DEFINE LF CHR(10)
#DEFINE CRLF CHR(13) + CHR(10)
#DEFINE RETURN_DATA_MANDATORY .F.
#DEFINE RETURN_DATA_NOT_MANDATORY .T.
#DEFINE CSVFORMAT 6
#DEFINE WRITE_ONLY_UNBUFFERED 11
#DEFINE COMMA ","

DEFINE CLASS FAPOTOICON AS CUSTOM

	cProcessName = 'FAPOTOICON'
	cProject = 'FAPOTOICON'

	lTestMode = .F.  && .F. FOR NORMAL OPERATION, .T. TO SUPPRESS ALL BUT DEVELOPER EMAILS AND CHANGE INPUT TABLE, ETC.

	lAutoYield = .F.

	* date time props
	dToday = DATE()
	*dToday = {^2011-11-11}

	cStartTime = TTOC(DATETIME())

	* file/folder properties
	cInputFolder = ''
	cOutputFolder = ''
	cArchivedFolder = ''
	cRootFileName = ''
	cInputFile = ''
	cArchivedFile = ''
	lArchiveInputFile = .T.

	* data properties
	cClient = 'FASHIONANGELS'
	cOrdersTable = ''
	
	* processing properties
	nOutputFilesCreated = 0  
	nLastUsedRow = 0  && last row with data in spreadsheet
	lPrematureExit = .F.  && will be set to true if we did not process all rows with data

	* Excel
	oExcel = NULL

	* wait window properties
	nWaitWindowTimeout = 2
	lWaitWindowIsOn = .T.

	* logfile properties
	lLoggingIsOn = .T.
	cLogFile = ''

	nSQLHandle = 0

	* INTERNAL email properties
	lSendInternalEmailIsOn = .T.
	cFrom = ''
	cSendTo = ''
	cCC = ''
	cSubject = 'Fashion Angel PO to Icon Process Results for ' + TTOC(DATETIME())
	cAttach = ''
	cBodyText = ''
	cTopBodyText = ''

	cCOMPUTERNAME = ''
	cUSERNAME = ''


	FUNCTION INIT
		IF NOT DODEFAULT()
			RETURN .F.
		ENDIF
		WITH THIS
			*SET RESOURCE OFF
			SET CENTURY ON
			SET DATE YMD
			SET HOURS TO 24
			CLOSE DATA
			SET DECIMAL TO 0
			SET TALK OFF
			SET DELETED ON
			SET CONSOLE OFF
			SET EXCLUSIVE OFF
			SET SAFETY OFF
			SET EXACT OFF
			SET STATUS BAR ON
			SET SYSMENU OFF
			SET ENGINEBEHAVIOR 70
			_VFP.AUTOYIELD = .lAutoYield
			.cCOMPUTERNAME = GETENV("COMPUTERNAME")
			.cUSERNAME = GETENV("USERNAME")
		ENDWITH
	ENDFUNC


	FUNCTION DESTROY
		WITH THIS
			IF .lLoggingIsOn  THEN
				SET ALTERNATE OFF
				SET ALTERNATE TO
			ENDIF
		ENDWITH
		DODEFAULT()
	ENDFUNC


	FUNCTION MAIN
		LPARAMETERS tlTestMode
		WITH THIS

			.lTestMode = tlTestMode

			TRY
				LOCAL lnNumberOfErrors, ldToday, len1, i, ary1[1,1], cFieldname
				LOCAL lnCurrentFile, lnNumFiles, laFiles[1,5], laFilesSorted[1,6]

				lnNumberOfErrors = 0

				IF .lTestMode THEN
					.cLogFile = 'F:\UTIL\FASHIONANGEL\LOGFILES\' + .cProject + '_LOG_TESTMODE.TXT'
					.cFrom = 'Mark.Bennett@Tollgroup.com'
					.cSendTo = 'mbennett@fmiint.com'
					.cCC = ''
				ELSE
					.cLogFile = 'F:\UTIL\FASHIONANGEL\LOGFILES\' + .cProject + '_LOG.TXT'
					.cFrom = 'Mark.Bennett@Tollgroup.com'
					.cSendTo = 'mbennett@fmiint.com'
					.cCC = ''
				ENDIF
				.lLoggingIsOn = .lLoggingIsOn AND NOT EMPTY(.cLogFile)

				IF .lLoggingIsOn THEN
					SET ALTERNATE TO (.cLogFile) ADDITIVE
					SET ALTERNATE ON
				ENDIF

				IF .lTestMode THEN
					.cInputFolder = 'F:\UTIL\FASHIONANGEL\TEST850IN\'
					.cArchivedFolder = 'F:\UTIL\FASHIONANGEL\TEST850IN\ARCHIVED\'
					.cOutputFolder = 'F:\UTIL\FASHIONANGEL\TESTICONOUT\'
					.cOrdersTable = 'F:\UTIL\FASHIONANGEL\TESTDATA\TORDERS'

				ELSE
					.cInputFolder = 'F:\FTPUSERS\FASHIONANGELS\POIN\'
					.cArchivedFolder = 'F:\FTPUSERS\FASHIONANGELS\POIN\ARCHIVE\'
					.cOutputFolder = 'F:\FTPUSERS\FASHIONANGELS\POOUTTOICON\'
					.cOrdersTable = 'F:\UTIL\FASHIONANGEL\DATA\ORDERS'
				ENDIF

				.TrackProgress('Fashion Angel PO to Icon Process started....', LOGIT+SENDIT)
				.TrackProgress('COMPUTERNAME = ' + .cCOMPUTERNAME, LOGIT+SENDIT)
				.TrackProgress('USERNAME = ' + .cUSERNAME, LOGIT+SENDIT)
				.TrackProgress('PROJECT = ' + .cProject, LOGIT+SENDIT)
				.TrackProgress('Input Folder = ' + .cInputFolder, LOGIT+SENDIT)
				.TrackProgress('Archived Folder = ' + .cArchivedFolder, LOGIT+SENDIT)
				.TrackProgress('Output Folder = ' + .cOutputFolder, LOGIT+SENDIT)
				.TrackProgress('Order History Table = ' + .cOrdersTable, LOGIT+SENDIT)
				IF .lTestMode THEN
					.TrackProgress('TEST MODE', LOGIT+SENDIT)
				ENDIF

				* check for input files...
				lnNumFiles = ADIR(laFiles,(.cInputFolder + "*.XLS"))

				IF lnNumFiles > 0 THEN

					* open Excel...
					.oExcel = CREATEOBJECT("excel.application")
					.oExcel.displayalerts = .F.
					.oExcel.VISIBLE = .F.

					* sort file list by date/time
					.SortArrayByDateTime(@laFiles, @laFilesSorted)


					* prep working data...
					* get output table structure from dbc
					OPEN DATABASE F:\UTIL\FASHIONANGEL\DATA\bioworld.DBC
					USE bioworld!bioworld_comp_struc IN 0 ALIAS bcs SHARED
					SELECT * FROM bioworld_comp_struc WHERE .F. INTO CURSOR tempbcs READW

					USE (.cOrdersTable) AGAIN IN 0 ALIAS ORDERS SHARED
					SELECT ordernum, status, orderdate FROM ORDERS INTO CURSOR CURORDERS WHERE .F. READWRITE
					
					* make first row of table contain the table field names! (copied from J. Bianchi's BIOWORLD CODE)
					USE IN bcs
					SELECT tempbcs
					LOCATE
					APPEND BLANK
					AFIELDS(ary1,'tempbcs')
					len1 = ALEN(ary1,1)
					FOR i = 1 TO len1
						IF ary1[i,2] = "C"
							cFieldname = ALLTRIM(ary1[i,1])
						ENDIF
						REPLACE tempbcs.&cFieldname WITH LOWER(cFieldname) IN tempbcs
					ENDFOR
					LOCATE

					*!*						SELECT tempbcs
					*!*						BROWSE

					* process 1st input files...
					*FOR lnCurrentFile = 1 TO lnNumFiles
					FOR lnCurrentFile = 1 TO 1

						.cRootFileName = laFilesSorted[lnCurrentFile,1]
						.cInputFile = .cInputFolder + .cRootFileName
						.cArchivedFile = .cArchivedFolder + .cRootFileName

						.ProcessInputFile()


					ENDFOR && lnCurrentFile = 1 TO lnNumFiles

					* close Excel
					.oExcel.QUIT()

					.TrackProgress('.nOutputFilesCreated = ' + TRANSFORM(.nOutputFilesCreated), LOGIT+SENDIT)
					
					IF NOT .lTestMode THEN
						IF .nOutputFilesCreated > 0 then
							.TriggerFTPUpload()
						ENDIF
					ENDIF

				ELSE
					.TrackProgress('Found no files in the input folder', LOGIT+SENDIT)
				ENDIF

				CLOSE DATABASES ALL

				.TrackProgress('Fashion Angel PO to Icon Process ended normally!',LOGIT+SENDIT)

			CATCH TO loError

				.TrackProgress('There was an error.',LOGIT+SENDIT)
				.TrackProgress('The Error # is: ' + TRANSFORM(loError.ERRORNO), LOGIT+SENDIT)
				.TrackProgress('The Error Message is: ' + TRANSFORM(loError.MESSAGE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred in Program: ' + TRANSFORM(loError.PROCEDURE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred at line #: ' + TRANSFORM(loError.LINENO), LOGIT+SENDIT)
				lnNumberOfErrors = lnNumberOfErrors + 1
				CLOSE DATA

				IF (NOT ISNULL(.oExcel)) AND (TYPE('.oExcel') = 'O') THEN
					.oExcel.QUIT()
				ENDIF

			ENDTRY

			CLOSE DATABASES ALL

			WAIT CLEAR
			***************** INTERNAL email results ******************************
			.TrackProgress('About to send status email.',LOGIT)
			.TrackProgress('The logfile is: ' + .cLogFile, SENDIT)
			.TrackProgress('Fashion Angel PO to Icon Process started: ' + .cStartTime, LOGIT+SENDIT)
			.TrackProgress('Fashion Angel PO to Icon Process finished: ' + TTOC(DATETIME()), LOGIT+SENDIT)
			
			IF NOT EMPTY(.cTopBodyText) THEN
				.cBodyText = .cTopBodyText + CRLF + CRLF + "<< processing log follows >>" + CRLF + CRLF + .cBodyText
			ENDIF

			IF .lSendInternalEmailIsOn THEN
				DO FORM dartmail2 WITH .cSendTo,.cFrom,.cSubject,.cCC,.cAttach,.cBodyText,"A"
				.TrackProgress('Sent status email.',LOGIT)
			ELSE
				.TrackProgress('Did not send status email: THIS.lSendInternalEmailIsOn = FALSE.',LOGIT)
			ENDIF

		ENDWITH
		RETURN
	ENDFUNC && main


	PROCEDURE ProcessInputFile
		WITH THIS
			LOCAL oWorkbook, oWorksheet, lnRow, lcRow, lnMaxRow, lcProcessed, lcMessage
			LOCAL lcOrder_no, lcProduct, lcIconFile, lcIconFileRoot, lcClosedOrders, lcOldOrders
			LOCAL lcStr1, lcIconFileTemp, lcIconFileCSV, lcTestOrder, lcTestDate, ltDateTime

			PRIVATE m.order_no, m.supplier_code, m.order_date_time, m.line_custom_attrib3  &&, m.milwaukee_warehouse
			PRIVATE m.product, m.order_status, m.exw_required_by, m.custom_milestone_est1, m.unit_of_measure
			PRIVATE m.qty_ordered, m.line_description, m.address_code, m.line_no, m.origin_port
			PRIVATE m.transport_mode, m.itemprice, m.required_date

			.TrackProgress("Processing EDI file: " + .cInputFile,LOGIT+SENDIT)
			.cTopBodyText = .cTopBodyText + "Processing EDI file: " + .cInputFile + CRLF + CRLF

			IF USED('CUROPENORDERS') THEN
				USE IN CUROPENORDERS
			ENDIF
			CREATE CURSOR CUROPENORDERS (ordernum c(10))

			oWorkbook = .oExcel.workbooks.OPEN( .cInputFile )
			oWorksheet = oWorkbook.Worksheets[1]
			
			
			*!*	With ActiveSheet.UsedRange
			*!*	   LastRow = .Row + .Rows.Count - 1
			*!*	End With			
			*x = oWorksheet.UsedRange.Row
			.nLastUsedRow = oWorksheet.UsedRange.Rows.Count
			
			lcClosedOrders = ''
			lcOldOrders = ''
			lcProcessed = ''

			lnMaxRow = 30000

			* First make a prepass thru the spreadsheet to determine which Orders have at least one Open status associated with them.
			* Needed to support 1/18/12 logic from Andi Hadisaar & Rebecca Ma which requires that if any line item of an Order is Open,
			* then all line items must be sent, even the Completed ones.
			FOR lnRow = 2 TO lnMaxRow
				lcRow = ALLTRIM(STR(lnRow))
				lcOrder_no = oWorksheet.RANGE("D"+lcRow).VALUE
				lcProduct  = oWorksheet.RANGE("K"+lcRow).VALUE
*SET STEP ON 
				DO CASE
					CASE ( ISNULL(lcOrder_no) OR EMPTY(lcOrder_no) ) AND ( ISNULL(lcProduct) OR EMPTY(lcProduct) )
						* both key fields are empty -- we are past the last data row! exit the loop...
						EXIT FOR
					CASE ( ISNULL(lcOrder_no) OR EMPTY(lcOrder_no) )
						* Header key field is empty -- get remaining detail fields
						m.order_status = oWorksheet.RANGE("N"+lcRow).VALUE
						IF UPPER(ALLTRIM( m.order_status )) = "OPEN" THEN
							INSERT INTO CUROPENORDERS (ordernum) VALUES (lcTestOrder)
						ENDIF
					CASE ( ISNULL(lcProduct) OR EMPTY(lcProduct) )
						* Detail key field is empty -- get remaining header fields
*!*						CASE NOT ( ISNULL(lcOrder_no) OR EMPTY(lcOrder_no) )
*!*							* Header key field is populated -- get remaining header fields
						m.order_no = ALLTRIM(TRANSFORM(oWorksheet.RANGE("D"+lcRow).VALUE))
						lcTestOrder = PADL(m.order_no,10,' ')
					OTHERWISE
						* nothing
				ENDCASE			
			ENDFOR
			
*!*	SELECT CUROPENORDERS
*!*	BROWSE
*!*	throw

			* NOW THE REAL PROCESSING PASS
			FOR lnRow = 2 TO lnMaxRow
				lcRow = ALLTRIM(STR(lnRow))

				IF .lTestMode THEN
					.TrackProgress('lcRow = ' + lcRow, LOGIT+SENDIT)
				ENDIF
				* We need special logic for determining when we have processed all the rows in spreadsheet!
				*
				* Every row in the input spreadsheet should be either a PO Header Line or Detail Line, i.e.,
				* for PO Header row, Column D (PO Number) should always be populated,
				* for Detail row, Column K (Item Number) should always be populated.
				* So...
				* If both those fields are empty in the current row, we assume we are past the last row with data in it.
				lcOrder_no = oWorksheet.RANGE("D"+lcRow).VALUE
				lcProduct  = oWorksheet.RANGE("K"+lcRow).VALUE

				DO CASE

					CASE ( ISNULL(lcOrder_no) OR EMPTY(lcOrder_no) ) AND ( ISNULL(lcProduct) OR EMPTY(lcProduct) )
						* both key fields are empty -- we are past the last data row! exit the loop...
						EXIT FOR

					CASE ( ISNULL(lcOrder_no) OR EMPTY(lcOrder_no) )
						* Header key field is empty -- get remaining detail fields
						m.product  = TRANSFORM(oWorksheet.RANGE("K"+lcRow).VALUE)
						m.order_status = oWorksheet.RANGE("N"+lcRow).VALUE
						* Per Andi Hadisaar 1/22/12 - also change 'Completed' statuses to 'PLC'
						*IF UPPER(ALLTRIM( m.order_status )) = "OPEN" THEN
							m.order_status = "PLC"
						*ENDIF
						*m.exw_required_by = TRANSFORM(TTOD(oWorksheet.RANGE("O"+lcRow).VALUE))
						m.exw_required_by = .FormatDate(oWorksheet.RANGE("O"+lcRow).VALUE)
						m.unit_of_measure = UPPER(oWorksheet.RANGE("P"+lcRow).VALUE)
						m.qty_ordered = TRANSFORM(oWorksheet.RANGE("Q"+lcRow).VALUE)
						m.line_description = oWorksheet.RANGE("W"+lcRow).VALUE

						* the following are hardcoded per Rebecca Ma / Andi Hadisaar
						m.address_code = "MILWAUKEE"
						m.origin_port = "CNSHA"  && UNLOC CODE FOR SHANGHAI
						m.transport_mode = "SEA"
						m.itemprice = "1.40"
						*m.required_date = "12-Dec-11"
						m.required_date = .FormatDate( {^2011-12-11} )

						lcTestOrder = PADL(m.order_no,10,' ')
						lcTestDate = m.exw_required_by

*!*							* DO INSERT .... but only if the order status = OPEN (WHICH WOULD NOW BE PLC AFTER PROCESSING ABOVE)
*!*							IF NOT (m.order_status == "PLC") THEN
*!*								* the order was not Open
*!*								lcClosedOrders = lcClosedOrders + ;
*!*									lcTestOrder + ', ' +  m.order_status + ', ' + m.product + ', ' + m.qty_ordered + ', ' + m.line_description + CRLF
*!*								*.TrackProgress('Not processing non-Open order: ' + ;
*!*								*lcTestOrder + ', ' +  m.order_status + ', ' + m.product + ', ' + m.qty_ordered + ', ' + m.line_description,LOGIT+SENDIT)
*!*							ELSE
							* continue and DO INSERT .... if the order has not already been processed
*!*								SELECT ORDERS
*!*								LOCATE FOR (ordernum == lcTestOrder) AND (orderdate == lcTestDate)
*!*								IF FOUND() THEN
*!*									* the order has already been processed
*!*									lcOldOrders = lcOldOrders + lcTestOrder + ', ' +  lcTestDate + CRLF
*!*									*.TrackProgress('Not processing old order: ' + lcTestOrder + ', ' +  lcTestDate,LOGIT+SENDIT)
*!*								ELSE
*!*									* it's a new order - continue

								* do insert ... but only if at least one line item for this Order # had an OPEN status
								SELECT CUROPENORDERS
								LOCATE FOR (ordernum == lcTestOrder)
								IF FOUND() THEN
								
									* only insert order lines where QTY > 0, per Andi 1/25/12.									
									IF VAL(m.qty_ordered) > 0 THEN									
										SELECT tempbcs
										APPEND BLANK

										REPLACE ;
											tempbcs.order_no WITH m.order_no, ;
											tempbcs.PRODUCT WITH m.product, ;
											address_code WITH m.address_code, ;
											client_cust_code WITH .cClient, ;
											delivery_address_name WITH m.address_code, ;
											delivery_address_country WITH 'USA', ;
											origin_port WITH m.origin_port, ;
											destination_port WITH 'USMKE', ;
											delivery_port WITH 'USMKE', ;
											transport_mode WITH m.transport_mode, ;
											itemprice WITH m.itemprice, ;
											required_date WITH m.required_date, ;
											supplier_code WITH m.supplier_code, ;
											supplier_name WITH m.supplier_code, ;
											order_date_time WITH m.order_date_time, ;
											order_currency_code WITH 'USD', ;
											line_custom_attrib3 WITH m.order_date_time, ;
											order_status WITH m.order_status, ;
											linestatus WITH 'PLC', ;
											exw_required_by WITH m.exw_required_by, ;
											custom_milestone_est1 WITH m.exw_required_by, ;
											unit_of_measure WITH m.unit_of_measure, ;
											qty_ordered WITH m.qty_ordered, ;
											line_description WITH m.line_description, ;
											line_no WITH TRANSFORM(m.line_no) ;
											IN tempbcs

										m.line_no = m.line_no + 1

										* add Order + Date key to Orders cursor
										INSERT INTO CURORDERS( ordernum, status, orderdate ) VALUES (lcTestOrder, m.order_status, lcTestDate )
										
										lcProcessed = lcProcessed + ;
											lcTestOrder + ', ' +  m.order_status + ', ' + m.product + ', ' + m.qty_ordered + ', ' + m.line_description + CRLF
									ELSE
										* no insert, because QTY = ZERO for this order line
										lcClosedOrders = lcClosedOrders + ;
											lcTestOrder + ', ' +  m.order_status + ', ' + m.product + ', ' + m.qty_ordered + ', ' + m.line_description + CRLF
											
										* per Andi, incrementing line order # even for QTY = 0  1/30/12 MB
										* this is done so that lines will have the same # if the QTY changes to/from 0
										m.line_no = m.line_no + 1
										
									ENDIF && m.qty_ordered > 0
								ELSE
									* no insert, because none of the line items for this Order # had an Open status
									lcClosedOrders = lcClosedOrders + ;
										lcTestOrder + ', ' +  m.order_status + ', ' + m.product + ', ' + m.qty_ordered + ', ' + m.line_description + CRLF
								ENDIF && FOUND()

*!*								ENDIF && FOUND()
							
*!*							ENDIF && NOT (m.order_status == "PLC")
						
					CASE ( ISNULL(lcProduct) OR EMPTY(lcProduct) )
						* Detail key field is empty -- get remaining header fields
*!*						CASE NOT ( ISNULL(lcOrder_no) OR EMPTY(lcOrder_no) )
*!*							* Header key field is populated -- get remaining header fields
						m.line_no = 1
						*m.order_no = TRANSFORM(oWorksheet.RANGE("D"+lcRow).VALUE)
						m.order_no = ALLTRIM(TRANSFORM(oWorksheet.RANGE("D"+lcRow).VALUE))
						m.supplier_code = oWorksheet.RANGE("F"+lcRow).VALUE
						m.supplier_code = UPPER( .RemoveWhiteSpace( m.supplier_code ) )
						*m.order_date_time = TRANSFORM(TTOD(oWorksheet.RANGE("H"+lcRow).VALUE))
						m.order_date_time = .FormatDate(oWorksheet.RANGE("H"+lcRow).VALUE)
					OTHERWISE
						* nothing

				ENDCASE

			ENDFOR
			
			IF lnRow < .nLastUsedRow THEN
				* we had a premature exit from row processing loop; probably because a required data element was missing.
				.lPrematureExit = .T.
			ENDIF

			oWorkbook.CLOSE()

			IF .lArchiveInputFile THEN

				IF FILE(.cArchivedFile) THEN
					* ARCHIVED FILE WITH SAME NAME ALREADY EXISTS - DELETE IT
					DELETE FILE (.cArchivedFile)
				ENDIF
				COPY FILE (.cInputFile) TO (.cArchivedFile)
				DELETE FILE (.cInputFile)
			ENDIF

*!*				lcIconFileTemp = .cOutputFolder + "PO_TGFDI_TEMP.TXT"
*!*				lcIconFileRoot = "PO_TGFDI_"+TTOC(DATETIME(),1) + .cClient +".TXT"
*!*				lcIconFile = .cOutputFolder + lcIconFileRoot

			IF .lPrematureExit THEN
				lcMessage = 'ERROR: processing stopped at Row ' + TRANSFORM(lnRow) + ' of ' + ;
					TRANSFORM(.nLastUsedRow) + ' due to missing data!' + CRLF + CRLF + ;
					"No output file was generated." + CRLF
				.TrackProgress(lcMessage,LOGIT+SENDIT)
				.cTopBodyText = .cTopBodyText + lcMessage + CRLF
				IF NOT .lTestMode THEN
					.cCC = "rebecca.h.ma@tollgroup.com, Andi.Hadisaar@tollgroup.com"
				ENDIF
				THROW
			ENDIF

			IF (RECCOUNT('tempbcs') < 2) THEN
				.TrackProgress('NOTE: There were no new orders to process!',LOGIT+SENDIT)
			ELSE
				* per Andi Hadisaar 1/18/12 - we should send .CSV files, not .TXT
*!*					SELECT tempbcs
*!*					COPY TO (lcIconFileTemp) TYPE DELIMITED WITH CHARACTER "|"
				

*!*					************************************************************
*!*					* THIS VERSION OF THE OUTPUT FILE IS ONLY FOR MAKING IT EASIER TO REVIEW THE OUTPUT
*!*					*IF .lTestMode THEN
*!*					lcIconFileCSV = "F:\UTIL\FASHIONANGEL\TESTICONOUT\PO_TGFDI_"+TTOC(DATETIME(),1) + .cClient + ".CSV"
*!*					SELECT tempbcs
*!*					COPY TO (lcIconFileCSV) TYPE CSV FOR (RECNO() > 1)
*!*					*ENDIF
*!*					************************************************************
				lcIconFileTemp = .cOutputFolder + "PO_TGFDI_TEMP.TXT"
				lcIconFileRoot = "PO_TGFDI_"+TTOC(DATETIME(),1) + .cClient + ".CSV"
				lcIconFileCSV = .cOutputFolder + lcIconFileRoot

				SELECT tempbcs
				*COPY TO (lcIconFileTemp) TYPE CSV FOR (RECNO() > 1)
				* Andi Hadisaar wants file extension to be csv, but delimietd with pipes, not commas ?!?
				* 1/22/12 MB
				COPY TO (lcIconFileTemp) TYPE DELIMITED WITH CHARACTER "|" && FOR (RECNO() > 1)

				* REMOVE ""s from output file
				lcStr1 = FILETOSTR(lcIconFileTemp)
				lcStr1 = STRTRAN(lcStr1,'"','')
				STRTOFILE(lcStr1,lcIconFileCSV)
				RELEASE lcStr1
				DELETE FILE (lcIconFileTemp)

*!*					* REMOVE ""s from output file
*!*					lcStr1 = FILETOSTR(lcIconFileTemp)
*!*					lcStr1 = STRTRAN(lcStr1,'"','')
*!*					STRTOFILE(lcStr1,lcIconFile)
*!*					RELEASE lcStr1
*!*					DELETE FILE (lcIconFileTemp)

*!*					IF FILE(lcIconFile) THEN
*!*						.TrackProgress("Created Icon file: " + lcIconFile,LOGIT+SENDIT)
*!*						.nOutputFilesCreated = .nOutputFilesCreated + 1
*!*					ELSE
*!*						.TrackProgress("!!!! ERROR Creating Icon file: " + lcIconFile,LOGIT+SENDIT)
*!*					ENDIF
				IF FILE(lcIconFileCSV) THEN
					.TrackProgress("Created Icon file: " + lcIconFileCSV,LOGIT+SENDIT)
					.nOutputFilesCreated = .nOutputFilesCreated + 1
				ELSE
					.TrackProgress("!!!! ERROR Creating Icon file: " + lcIconFileCSV,LOGIT+SENDIT)
				ENDIF
			ENDIF  &&  (RECCOUNT('tempbcs') < 2)

			* update Orders table
			ltDateTime = DATETIME()
			SELECT CURORDERS
			SCAN
				INSERT INTO ;
					ORDERS( ordernum, status, orderdate, infile, outfile, procdt ) ;
					VALUES (CURORDERS.ordernum, CURORDERS.status, CURORDERS.orderdate, .cRootFileName, lcIconFileRoot, ltDateTime)
			ENDSCAN

			* Clear cursors in case we want to process multiple files in one run...
			SELECT tempbcs
			DELETE ALL FOR (RECNO() > 1) IN tempbcs
			
			SELECT CURORDERS
			DELETE ALL IN CURORDERS
			
			IF NOT EMPTY(lcProcessed) THEN
				lcProcessed = 'Items processed:' + CRLF + ;
				'Order #/Status/Product #/Qty_ordered/Description' + CRLF + lcProcessed 
				.TrackProgress(lcProcessed,LOGIT+SENDIT)
			ENDIF
			
*!*				IF NOT EMPTY(lcOldOrders) THEN
*!*					lcOldOrders = 'Not processing Orders/Ex Dates already processed:' + CRLF + 'Order #/Ex Date' + CRLF + lcOldOrders
*!*					.TrackProgress(lcOldOrders,LOGIT+SENDIT)
*!*				ENDIF
			
			*IF .lTestMode THEN
			IF NOT EMPTY(lcClosedOrders) THEN
				lcClosedOrders = 'Not processed because the Order # had no Open line items, or QTY = ZERO:' + CRLF + ;
				'Order #/Status/Product #/Qty_ordered/Description' + CRLF + lcClosedOrders
				.TrackProgress(lcClosedOrders,LOGIT+SENDIT)
			ENDIF
			*ENDIF  &&  .lTestMode

		ENDWITH
		RETURN
	ENDPROC


	PROCEDURE TriggerFTPUpload
		* trigger the FTP upload process
		* THIS PROC IS HERE JUST SO I CAN EASILY EXECUTE IT MANUALLY, IF NEEDED
		INSERT INTO F:\edirouting\ftpjobs (jobname) VALUES ("FA-PO-TO-ICON")
		.TrackProgress('Triggered FTP job FA-PO-TO-ICON', LOGIT+SENDIT)
		RETURN
	ENDPROC


	FUNCTION FormatDate
		LPARAMETERS tdDate
		LOCAL lcReturnDate, lnMonth, lnDay, lnYear
		lnMonth = MONTH( tdDate )
		lnDay = DAY( tdDate )
		lnYear = YEAR( tdDate )
		* format like 12-Dec-11
		*lcReturnDate = PADL(lnDay,2,'0') + "-" + LEFT(lcMonth,3) + "-" + RIGHT( TRANSFORM( lnYear ), 2 )
		* format like YYYY-MM-DD
		lcReturnDate = PADL(lnYear,4,'0') + "-" + PADL(lnMonth,2,'0') + "-" + PADL(lnDay,2,'0')
		RETURN lcReturnDate
	ENDFUNC


	FUNCTION RemoveWhiteSpace
		LPARAMETERS tcString
		* also should remove special characters, presumably anything other than A-Z, a-z, 0-9
		LOCAL lcReturnString, lnStrLen, i, lnAsciiCode, lcTestChar
		lcReturnString = ''
		lnStrLen = LEN( tcString )
		FOR i = 1 TO lnStrLen
			lcTestChar = SUBSTR(tcString, i, 1)
			lnAsciiCode = ASC( lcTestChar )
			IF ( lnAsciiCode >= 65 AND lnAsciiCode <= 90 ) ;
					OR ( lnAsciiCode >= 97 AND lnAsciiCode <= 122 ) ;
					OR ( lnAsciiCode >= 48 AND lnAsciiCode <= 57 ) THEN
				* char is a - z or A - Z or 0 - 9, concatentate it to output string
				lcReturnString = lcReturnString + lcTestChar
			ENDIF
		ENDFOR
		RETURN lcReturnString
	ENDFUNC


	FUNCTION ExecSQL
		LPARAMETERS tcSQL, tcCursorName, tlNoDataReturnedIsOkay
		LOCAL llRetval, lnResult
		WITH THIS
			* close target cursor if it's open
			IF USED(tcCursorName)
				USE IN (tcCursorName)
			ENDIF
			WAIT WINDOW "Fetching remote data; please wait..." NOWAIT
			lnResult = SQLEXEC(.nSQLHandle, tcSQL, tcCursorName)
			llRetval = ( lnResult > 0 )
			IF llRetval THEN
				* see if any data came back
				IF NOT tlNoDataReturnedIsOkay THEN
					IF (NOT USED(tcCursorName)) OR EOF(tcCursorName) THEN
						llRetval = .F.
						.TrackProgress('No data was returned by this query: ' +CRLF + tcSQL, LOGIT+SENDIT)
					ENDIF
				ENDIF
			ELSE
				.TrackProgress("Result Code [" + TRANSFORM(lnResult) + "] returned by this query: " + tcSQL, LOGIT+SENDIT)
			ENDIF
			WAIT CLEAR
			RETURN llRetval
		ENDWITH
	ENDFUNC


	PROCEDURE TrackProgress
		* do any combination of Wait Window, writing to logfile, and adding to body of email,
		* based on nFlags parameter.
		LPARAMETERS tcExpression, tnFlags
		WITH THIS
			IF BITAND(tnFlags,LOGIT) = LOGIT THEN
				IF .lLoggingIsOn THEN
					?
					? .cProcessName + "  " + TTOC(DATETIME()) + ": " + tcExpression
				ENDIF
			ENDIF
			IF .lWaitWindowIsOn THEN
				IF BITAND(tnFlags,NOWAITIT) = NOWAITIT THEN
					WAIT WINDOW tcExpression NOWAIT
				ENDIF
				IF BITAND(tnFlags,WAITIT) = WAITIT THEN
					WAIT WINDOW tcExpression TIMEOUT .nWaitWindowTimeout
				ENDIF
			ENDIF
			IF BITAND(tnFlags,SENDIT) = SENDIT THEN
				IF .lSendInternalEmailIsOn THEN
					.cBodyText = .cBodyText + tcExpression + CRLF + CRLF
				ENDIF
			ENDIF
		ENDWITH
	ENDPROC  &&  TrackProgress


	PROCEDURE SortArrayByDateTime
		* expects taInArray[] to be the result of an ADIR()
		LPARAMETERS taInArray, taOutArray
		EXTERNAL ARRAY taInArray, taOutArray
		LOCAL lnRows, lnCols, i, j
		* extending taInArray[] to process the files in date/time order
		lnRows = ALEN(taInArray,1)
		lnCols = ALEN(taInArray,2)
		* create taOutArray with one more column
		DIMENSION taOutArray[lnRows,lnCols + 1]
		* and fill it with taInArray columns AND (filedate + filetime) in new column
		* ( ACOPY() doesn't preserve elements properly because of different # of columns )
		FOR i = 1 TO lnRows
			FOR j = 1 TO lnCols
				taOutArray[i,j] = taInArray[i,j]
			ENDFOR
			taOutArray[i,lnCols + 1] = DTOS(taInArray[i,3]) + "_" + taInArray[i,4]
		ENDFOR
		* sort taOutArray on date/time column
		=ASORT(taOutArray,6)
	ENDPROC



ENDDEFINE
