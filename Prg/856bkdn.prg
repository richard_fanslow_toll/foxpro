PARAMETERS lcFilename 

SET CENTURY ON
SELECT x856
GOTO TOP

STORE 0 TO lnTotalAdded
lcCurrentHAWB = ""

testing=.F.

nMANProcessed = 0
POs_added = 0
ShipmentNumber = 1
ThisShipment = 0
origawb = ""
lcCurrentShipID = ""
lcCurrentArrival = ""
LAFreight = .F.

SELECT ndetail
SET FILTER TO

lcHLLevel = ""
SELECT ndetail
SCATTER MEMVAR BLANK
m.acct_name = "NAUTICA"
m.acct_num  = 4500
m.office = "C"

SELECT nbcodes
SCATTER MEMVAR FIELDS EXCEPT HDRID BLANK

SELECT x856
SET FILTER TO
LOCATE

WAIT WINDOW "Now importing file..........   "+lcFilename NOWAIT
DO WHILE !EOF("x856")
	IF !EMPTY(M.CONTAINER)
*!*		DEBUG
*!*		SET STEP ON
	ENDIF
	cSegment = ALLTRIM(x856.segment)
	cField1 = ALLTRIM(x856.f1)
	IF INLIST(cSegment,"ISA","GS","ST")
		SELECT x856
		SKIP 1 IN x856
		LOOP
	ENDIF
	IF TRIM(x856.segment) = "BSN"
		m.filename  = lcFilename
		m.dateloaded= DATE()
		m.shipID    = m.shipmentid

		m.shipmentid = ALLTRIM(x856.f2)
		SELECT x856
		SKIP 1 IN x856
		LOOP
	ENDIF
	IF AT("HL",x856.segment)>0
		lcHLLevel = ALLTRIM(x856.f3)
		SELECT x856
		SKIP 1 IN x856
		LOOP
	ENDIF

	IF lcHLLevel = "S"
		DO WHILE lcHLLevel = "S"
			cSegment = TRIM(x856.segment)
			cF1 = TRIM(x856.f1)
			DO CASE
				CASE TRIM(x856.segment) = "HL"
					lcHLLevel = ALLTRIM(x856.f3)
				CASE TRIM(x856.segment) = "TD1"
					m.totalkgs= VAL(x856.f7)
					m.weight  = VAL(x856.f7)*2.2
					m.cbm     = VAL(x856.f9)
					m.pl_qty  = VAL(x856.f2)
				CASE TRIM(x856.segment) = "TD5"
					m.type =  ALLTRIM(x856.f4)
				CASE TRIM(x856.segment) = "REF" AND ALLTRIM(x856.f1) = "MB"
					m.bol = ALLTRIM(x856.f2)
				CASE TRIM(x856.segment) = "REF" AND ALLTRIM(x856.f1) = "BN"
					m.brokref = ALLTRIM(x856.f2)
					m.hawb    = ALLTRIM(x856.f2)
				CASE TRIM(x856.segment) = "REF" AND ALLTRIM(x856.f1) = "OC"
					SET STEP ON 
					m.container = ALLTRIM(x856.f2)
				CASE TRIM(x856.segment) = "DTM" AND ALLTRIM(x856.f1)= "017"
					m.arriv_date  =  CTOD(SUBSTR(ALLTRIM(x856.f2),5,2)+"/"+SUBSTR(ALLTRIM(x856.f2),7,2)+"/"+SUBSTR(ALLTRIM(x856.f2),1,4))
					m.estarrival =  CTOD(SUBSTR(ALLTRIM(x856.f2),5,2)+"/"+SUBSTR(ALLTRIM(x856.f2),7,2)+"/"+SUBSTR(ALLTRIM(x856.f2),1,4))
				CASE TRIM(x856.segment) = "DTM" AND ALLTRIM(x856.f1)= "005"
					m.shipped =  CTOD(SUBSTR(ALLTRIM(x856.f2),5,2)+"/"+SUBSTR(ALLTRIM(x856.f2),7,2)+"/"+SUBSTR(ALLTRIM(x856.f2),1,4))
			ENDCASE

			SELECT x856
			SKIP 1 IN x856
		ENDDO
	ENDIF

	IF lcHLLevel = "O"
		SELECT ndetail
		m.timeloaded = DATETIME()
		APPEND BLANK
		GATHER MEMVAR
		SELECT x856
		DO WHILE INLIST(lcHLLevel,"O")
			cSegment = TRIM(x856.segment)
			cF1 = TRIM(x856.f1)
			DO CASE
				CASE TRIM(x856.segment) = "HL"
					lcHLLevel = ALLTRIM(x856.f3)
				CASE TRIM(x856.segment) = "PRF"
					REPLACE po_num WITH ALLTRIM(x856.f1) IN ndetail
					m.cust_po = ALLTRIM(x856.f1)
				CASE TRIM(x856.segment) = "TD3"
					REPLACE ndetail.ctrprfx   WITH UPPER(ALLTRIM(x856.f2)) IN ndetail
					REPLACE ndetail.CONTAINER WITH UPPER(ALLTRIM(x856.f3)) IN ndetail
					REPLACE ndetail.SIZE  WITH "" IN ndetail
					REPLACE ndetail.COLOR WITH "" IN ndetail
					m.container = UPPER(ALLTRIM(x856.f2))+UPPER(ALLTRIM(x856.f3))
				CASE TRIM(x856.segment) = "TD5"
					REPLACE ndetail.origin WITH UPPER(ALLTRIM(x856.f8)) IN ndetail
				CASE TRIM(x856.segment) = "REF" AND ALLTRIM(x856.f1) = "PO"
					m.import_po = ALLTRIM(x856.f2)
				CASE TRIM(x856.segment) = "DTM" AND ALLTRIM(x856.f1)= "001"
					m.cancel = CTOD(SUBSTR(ALLTRIM(x856.f2),5,2)+"/"+SUBSTR(ALLTRIM(x856.f2),7,2)+"/"+SUBSTR(ALLTRIM(x856.f2),1,4))
					REPLACE ndetail.cancel  with  m.cancel IN ndetail
				CASE TRIM(x856.segment) = "DTM" AND ALLTRIM(x856.f1)= "010"
					m.start =  CTOD(SUBSTR(ALLTRIM(x856.f2),5,2)+"/"+SUBSTR(ALLTRIM(x856.f2),7,2)+"/"+SUBSTR(ALLTRIM(x856.f2),1,4))
				CASE TRIM(x856.segment) = "N1" AND ALLTRIM(x856.f1) = "ST"
					m.name = ALLTRIM(x856.f2)
					m.dcnum = ALLTRIM(x856.f4)
					m.storenum = INT(VAL(ALLTRIM(x856.f4)))
				CASE TRIM(x856.segment) = "N3"
					m.address1 = ALLTRIM(x856.f2)
					m.address2 = ALLTRIM(x856.f3)
				CASE TRIM(x856.segment) = "N4"
					m.city = ALLTRIM(x856.f1)
					m.state = ALLTRIM(x856.f2)
					m.zip = ALLTRIM(x856.f3)
			ENDCASE

			SELECT x856
			SKIP 1 IN x856
		ENDDO
		
		SELECT dc_locs
		LOCATE FOR dc_locs.dc = m.dc
		IF !FOUND()
		INSERT INTO dc_locs FROM memvar
*		brow
		ENDIF
		
		SELECT x856
	ENDIF

	IF lcHLLevel = "I"
		DO WHILE lcHLLevel = "I"
			cSegment = TRIM(x856.segment)
			cF1 = TRIM(x856.f1)
			DO CASE
				CASE TRIM(x856.segment) = "HL"
					lcHLLevel = ALLTRIM(x856.f3)
				CASE TRIM(x856.segment) = "LIN"
					m.style = ALLTRIM(x856.f5)
					m.size  = ALLTRIM(x856.f7)
					m.color = ALLTRIM(x856.f9)
					m.upc   = ALLTRIM(x856.f3)
					m.storenum = ALLTRIM(x856.f13)
					REPLACE ndetail.STYLE WITH m.style IN ndetail
					REPLACE ndetail.size WITH m.size IN ndetail
					REPLACE ndetail.color WITH m.color IN ndetail
				CASE TRIM(x856.segment) = "SN1"
					m.totqty = ALLTRIM(x856.f2)
			ENDCASE
			SELECT x856
			SKIP 1 IN x856
		ENDDO
		SELECT x856
	ENDIF

	IF lcHLLevel = "P"
		DO WHILE lcHLLevel = "P"
			cSegment = TRIM(x856.segment)
			cF1 = TRIM(x856.f1)
			DO CASE
				CASE TRIM(x856.segment) = "HL"
					lcHLLevel = ALLTRIM(x856.f3)
				CASE TRIM(x856.segment) = "SN1"
					m.pack = ALLTRIM(x856.f2)
				CASE TRIM(x856.segment) = "MAN"
					nMANProcessed = nMANProcessed + 1
					WAIT WINDOW "Barcodes processed: "+ALLTRIM(STR(nMANProcessed)) NOWAIT
					m.barcode = ALLTRIM(x856.f2)
					IF m.barcode = "00007315178019868619"
*					SET STEP ON 
					endif
					INSERT INTO nbcodes FROM memvar
*!*						APPEND BLANK
*!*						GATHER MEMVAR
			ENDCASE
			SELECT x856
			IF !EOF()
				SKIP 1 IN x856
			ELSE
				EXIT
			ENDIF
		ENDDO
		WAIT CLEAR
	ENDIF

ENDDO

lProcessOK = .T.
