PARAMETER merror, MESS, mess1, mprog, mlineno
CLEAR
tmessage = 'Error number: ' + LTRIM(STR(merror)) + CHR(13) + ;
	'Error message: ' + MESS + CHR(13) + ;
	'Line of code with error: ' + mess1 + CHR(13) + ;
	'Line number of error: ' + LTRIM(STR(mlineno)) + CHR(13) + ;
	'Program with error: ' + mprog


tsubject = "Load Tender Screen Error " + TTOC(DATETIME())
tattach  = ""
*tsendto = "pgaidis@fmiint.com,mbennett@fmiint.com"
tsendto = "mbennett@fmiint.com"
tcc=""
tfrom    = "fmi-transload-ops@fmiint.com"

DO FORM dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"

=MESSAGEBOX(tmessage)

QUIT
