* import STEELSERIES styles
* programmed by MB
* build exe as F:\UTIL\STEELSERIES\STEELSERIESSTYLEIMPORT.EXE
*
* Added support for SQL 3/31/2016 MB
*
* NOTE: add whsql.vcx ang gethandleca.prg to project for the EXE.

LOCAL lTestMode
lTestMode = .F.

guserid = "SSSTYLEIMP"

IF NOT lTestMode THEN
	utilsetup("STEELSERIESSTYLEIMPORT")
ENDIF

DECLARE INTEGER CreateMutex IN Win32API ;
	STRING @lpMutexAttributes, ;
	STRING bInitialOwner, ;
	STRING @lpName

DECLARE INTEGER GetLastError IN Win32API

* if running in EXE check to see if already running
IF _VFP.STARTMODE > 0 THEN
	lcProcessName = "STEELSERIESSTYLEIMPORT"
	CreateMutex(0,CHR(1),@lcProcessName)
	lnError = GetLastError()
	IF lnError = 183
		WAIT WINDOW TIMEOUT 5 "This program is already running..."
		IF NOT lTestMode THEN
			schedupdate()
		ENDIF
		RETURN .F.
	ENDIF
ENDIF

loSTEELSERIESSTYLEIMPORT = CREATEOBJECT('STEELSERIESSTYLEIMPORT')
loSTEELSERIESSTYLEIMPORT.MAIN( lTestMode )

IF NOT lTestMode THEN
	schedupdate()
ENDIF

CLOSE DATABASES ALL
RETURN

#DEFINE LOGIT 1
#DEFINE WAITIT 2
#DEFINE NOWAITIT 4
#DEFINE SENDIT 8
#DEFINE CRLF CHR(13) + CHR(10)
#DEFINE RETURN_DATA_MANDATORY .F.
#DEFINE RETURN_DATA_NOT_MANDATORY .T.
#DEFINE CSVFORMAT 6
#DEFINE WRITE_ONLY_UNBUFFERED 11
#DEFINE COMMA ","

DEFINE CLASS STEELSERIESSTYLEIMPORT AS CUSTOM

	cProcessName = 'STEELSERIESSTYLEIMPORT'

	lTestMode = .T.  && .F. FOR NORMAL OPERATION, .T. TO SUPPRESS ALL BUT DEVELOPER EMAILS AND CHANGE INPUT TABLE, ETC.

	dToday = DATE()

	lAutoYield = .T.

	cStartTime = TTOC(DATETIME())

	* connection properties
	nSQLHandle = 0

	* folder properties

	cInputFolder1 = ''
	cArchiveFolder = ''
	cOrigFolder = ''	

*!*		* data properties
*!*		cUPCMastTable = ''
	
	nAccountID = 6612   && FROM F:\SYSDATA\QQDATA\account.dbf

	cAddProc = "STEELSTYLE"
	tADDDT = DATETIME()
	lPNP = .F.
	cDESCRIP = ''
	cSTYLE = ''
	nUPCMASTID = 0
	cUPC = ''
	nWEIGHT = 0
	nCUBE = 0
	nUIC = 0
	cINFO = ''
	cHTSCODE = ''
	cITEMTYPE = ''
	
	* processing properties
	lDeleteFoundRecs = .F.  && if .T. then when we find a key match we will delete existing rec and insert the new one. If .F. we will simply not insert when we find a new match.
	nRow = 0
	cStyle = ''
	nSkippedRecs = 0

	* Excel properties
	oExcel = NULL

	* wait window properties
	nWaitWindowTimeout = 2
	lWaitWindowIsOn = .T.

	* logfile properties
	lLoggingIsOn = .T.
	cLogFile = 'F:\UTIL\STEELSERIES\STYLES\LOGFILES\STEELSERIESSTYLEIMPORT_log.txt'

	* INTERNAL email properties
	lSendInternalEmailIsOn = .T.
	cFrom = 'TGF EDI Operations <toll-edi-ops@tollgroup.com>'
	cSendTo = 'mbennett@fmiint.com'
	cCC = ''
	cSubject = 'STEELSERIES Style Import for ' + TRANSFORM(DATETIME())
	cAttach = ''
	cBodyText = ''
	cMGRSendTo = ''
	cMGRFrom = ''
	cMGRSubject = ''
	cMGRCC = ''
	cMGRAttach = ''
	cMGRBodyText = ''

	cCOMPUTERNAME = ''
	cUSERNAME = ''


	FUNCTION INIT
		IF NOT DODEFAULT()
			RETURN .F.
		ENDIF
		WITH THIS
			*SET RESOURCE OFF
			CLOSE DATA
			SET CENTURY ON
			*SET DATE AMERICAN
			*SET HOURS TO 24
			SET DECIMALS TO 2
			SET ANSI ON
			SET TALK OFF
			SET DELETED ON
			SET CONSOLE OFF
			SET EXCLUSIVE OFF
			SET SAFETY OFF
			SET EXACT OFF
			SET STATUS BAR ON
			SET SYSMENU OFF
			SET ENGINEBEHAVIOR 70
			SET MULTILOCKS ON
			_VFP.AUTOYIELD = .lAutoYield
			.cCOMPUTERNAME = GETENV("COMPUTERNAME")
			.cUSERNAME = GETENV("USERNAME")
			.lLoggingIsOn = .lLoggingIsOn AND NOT EMPTY(.cLogFile)
		ENDWITH
	ENDFUNC


	FUNCTION DESTROY
		WITH THIS
			IF .lLoggingIsOn  THEN
				SET ALTERNATE OFF
				SET ALTERNATE TO
			ENDIF
		ENDWITH
		IF TYPE('.oExcel') = 'O' AND NOT ISNULL(.oExcel) THEN
			.oExcel.QUIT()
		ENDIF
		DODEFAULT()
	ENDFUNC


	FUNCTION MAIN
		LPARAMETERS tlTestMode
		WITH THIS
			LOCAL lnNumberOfErrors, lnAccountID, lnNumFiles1, lnNumFiles2, laFiles[1,5]

			TRY

				lnNumberOfErrors = 0
				
				.cStartTime = TTOC(DATETIME())

				.lTestMode = tlTestMode

				IF .lTestMode THEN
					.cLogFile = 'F:\UTIL\STEELSERIES\STYLES\LOGFILES\STEELSERIESSTYLEIMPORT_log_TESTMODE.txt'
					.cSendTo = 'mbennett@fmiint.com'
				ENDIF
				
				IF .lLoggingIsOn THEN
					SET ALTERNATE TO (.cLogFile) ADDITIVE
					SET ALTERNATE ON
				ENDIF
				
				IF .lTestMode THEN
					.cInputFolder1 = 'F:\UTIL\STEELSERIES\STYLES\TESTINPUT\'
*!*						.cUPCMastTable = 'F:\UTIL\STEELSERIES\STYLES\TESTDATA\UPCMAST'
					.cArchiveFolder = 'F:\UTIL\STEELSERIES\STYLES\TESTINPUT\ARCHIVED\'
					.cOrigFolder = 'F:\UTIL\STEELSERIES\STYLES\TESTINPUT\ORIG\'
				ELSE
					.cInputFolder1 = 'F:\FTPUSERS\STEELSERIES\STYLEMASTER\'
*!*						.cUPCMastTable = 'F:\WH\UPCMAST'
					.cArchiveFolder = 'F:\FTPUSERS\STEELSERIES\STYLEMASTER\ARCHIVED\'
					.cOrigFolder = 'F:\FTPUSERS\STEELSERIES\STYLEMASTER\ORIG\'	
				ENDIF
				

				.TrackProgress('STEELSERIES Style Import process started....', LOGIT+SENDIT)
				.TrackProgress('COMPUTERNAME = ' + .cCOMPUTERNAME, LOGIT+SENDIT)
				.TrackProgress('USERNAME = ' + .cUSERNAME, LOGIT+SENDIT)
				.TrackProgress('PROJECT = STEELSERIESSTYLEIMPORT', LOGIT+SENDIT)
				IF .lTestMode THEN
					.TrackProgress('                        TEST MODE                        ', LOGIT+SENDIT)
				ENDIF
				.TrackProgress('.cInputFolder1 = ' + .cInputFolder1, LOGIT+SENDIT)
*!*					.TrackProgress('.cUPCMastTable = ' + .cUPCMastTable, LOGIT+SENDIT)
				.TrackProgress('.cArchiveFolder = ' + .cArchiveFolder, LOGIT+SENDIT)
				.TrackProgress('.cOrigFolder = ' + .cOrigFolder, LOGIT+SENDIT)
				
				* OPEN Excel
				.oExcel = CREATEOBJECT("excel.application")
				.oExcel.displayalerts = .F.
				.oExcel.VISIBLE = .F.

*!*					USE (.cUPCMastTable) IN 0 ALIAS UPCMAST	
							
				* for sql
				useca("upcmast","wh",,,"select * from upcmast where accountid = " + ALLTRIM(STR(.nAccountID,4,0)),,"upcmastsql")
				SELECT "upcmastsql"
				GO bottom
			
				* process first source folder
				.ProcessFolder( .cInputFolder1, .cArchiveFolder, .cOrigFolder )
				
*!*					SELECT UPCMAST
*!*					BROWSE

				*.UpdateMeasures( .cInputFolder1, .cArchiveFolder, .cOrigFolder )

				CLOSE DATABASES ALL
				
				.oExcel.Quit()

				.TrackProgress('STEELSERIES Style Import process ended normally.', LOGIT+SENDIT)

			CATCH TO loError

				.TrackProgress('There was an error.',LOGIT+SENDIT)
				.TrackProgress('The Error # is: ' + TRANSFORM(loError.ERRORNO), LOGIT+SENDIT)
				.TrackProgress('The Error Message is: ' + TRANSFORM(loError.MESSAGE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred in Program: ' + TRANSFORM(loError.PROCEDURE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred at line #: ' + TRANSFORM(loError.LINENO), LOGIT+SENDIT)
				.TrackProgress('SPREADSHEET ROW = ' + TRANSFORM(.nRow), LOGIT+SENDIT)
				.TrackProgress('SPREADSHEET STYLE = ' + .cStyle, LOGIT+SENDIT)
				lnNumberOfErrors = lnNumberOfErrors + 1
				.lSendInternalEmailIsOn = .T.
				IF TYPE('.oExcel') = 'O' AND NOT ISNULL(.oExcel) THEN
					.oExcel.QUIT()
				ENDIF

			ENDTRY   

			WAIT CLEAR
			***************** INTERNAL email results ******************************
			.TrackProgress('About to send status email.',LOGIT+SENDIT)
			.TrackProgress('The logfile is: ' + .cLogFile, LOGIT+SENDIT)
			.TrackProgress('STEELSERIES Style Import process started: ' + .cStartTime, LOGIT+SENDIT)
			.TrackProgress('STEELSERIES Style Import process finished: ' + TTOC(DATETIME()), LOGIT+SENDIT)

			IF .lSendInternalEmailIsOn THEN
				DO FORM dartmail2 WITH .cSendTo,.cFrom,.cSubject,.cCC,.cAttach,.cBodyText,"A"
				.TrackProgress('Sent status email.',LOGIT)
			ELSE
				.TrackProgress('Did not send status email: THIS.lSendInternalEmailIsOn = FALSE.',LOGIT+SENDIT)
			ENDIF

		ENDWITH
		RETURN
	ENDFUNC && main


	PROCEDURE ProcessFolder
		LPARAMETERS tcInputFolder, tcArchivedFolder, tcOrigFolder
		WITH THIS
			LOCAL laFiles[1,5], laFilesSorted[1,6], lnNumFiles, lnCurrentFile, lcSourceFile, lcArchivedFile, lcOrigFile
			LOCAL lnHandle, lcStr, lnCtr, oWorkbook, oWorksheet, lnMaxRow, lnRow, lcRow, lnDupeDelCtr, lnDupeCtr
			LOCAL llArchived, lnColumnF, lcColumnD, lnLength, lnWidth, lnHeight
			LOCAL lcSize, lcEAN, lcCompare, llUpdateIfFound

*!*				PRIVATE	m.ACCOUNTID, m.ADDPROC, m.PNP, m.DESCRIP, m.UPC, m.STYLE, m.WEIGHT, m.CUBE, m.UIC, m.INFO, m.HTSCODE, m.ITEMTYPE
*!*				PRIVATE m.ADDBY, m.UPDATEBY, m.ADDDT, m.UPDATEDT

*!*				m.ACCOUNTID = .nAccountID
*!*				m.ADDPROC = .cAddProc
*!*				m.ADDBY = .cAddProc

			PRIVATE	m.UPCMASTID, m.ACCOUNTID, m.STYLE, m.PNP, m.DESCRIP, m.UPC, m.WEIGHT, m.CUBE, m.UIC, m.INFO, m.HTSCODE, m.ITEMTYPE
			STORE 0 TO m.UPCMASTID, m.WEIGHT, m.CUBE, m.UIC
			STORE '' TO m.STYLE, m.DESCRIP, m.UPC, m.INFO, m.HTSCODE, m.ITEMTYPE
			STORE .T. TO m.PNP

			m.ACCOUNTID = .nAccountID
			

			************************************************************************
			*** for sql
			PRIVATE m.ADDPROC, m.UPDPROC, m.ADDBY, m.ADDDT, m.UPDATEBY, m.UPDATEDT
			STORE .cAddProc TO m.ADDPROC, m.UPDPROC, m.ADDBY, m.UPDATEBY
			STORE .tADDDT TO m.ADDDT, m.UPDATEDT
			************************************************************************

			lnNumFiles = ADIR(laFiles,(tcInputFolder + "*.xlsx")) 

			IF lnNumFiles > 0 THEN

				* sort file list by date/time
				.SortArrayByDateTime(@laFiles, @laFilesSorted)

				* send an email only if files were found
				.lSendInternalEmailIsOn = .T.

				FOR lnCurrentFile = 1 TO lnNumFiles

					lcSourceFile = tcInputFolder + laFilesSorted[lnCurrentFile,1]
					lcArchivedFile = tcArchivedFolder + laFilesSorted[lnCurrentFile,1]
					lcOrigFile = tcOrigFolder + laFilesSorted[lnCurrentFile,1]

					.TrackProgress('lcSourceFile = ' + lcSourceFile,LOGIT+SENDIT)
					.TrackProgress('lcArchivedFile = ' + lcArchivedFile,LOGIT+SENDIT)
					.TrackProgress('lcOrigFile = ' + lcOrigFile,LOGIT+SENDIT)
					
					
					* move source file into ORIG folder so that, if an error causes the program to abort, the folderscan process does not keep reprocessing the bad source file in an infinite loop
					
					* if the file already exists in the orig folder, make sure it is not read-only.
					llArchived = FILE(lcOrigFile)
					IF llArchived THEN
						RUN ATTRIB -R &lcOrigFile.
						.TrackProgress('   Used ATTRIB -R for re-archiving : ' + lcOrigFile,LOGIT+SENDIT)
					ENDIF

					* archive the source file
					COPY FILE (lcSourceFile) TO (lcOrigFile)

					* always delete the source file
					*IF FILE(lcOrigFile) THEN
						DELETE FILE (lcSourceFile)
					*ENDIF
					
					
					* then process from the file once it is in the ORIG folder
					oWorkbook = .oExcel.workbooks.OPEN(lcOrigFile)
					oWorksheet = oWorkbook.Worksheets[1]

					lnMaxRow = 989
					
					lnCtr = 0
					lnDupeCtr = 0
					lnDupeDelCtr = 0
					
					FOR lnRow = 987 TO lnMaxRow   && start at row x to skip header line(s)
					
						.nRow = lnRow
					
						WAIT WINDOW NOWAIT 'Processing Spreadsheet Row: ' + TRANSFORM(lnRow)
						
						lcRow = ALLTRIM(STR(lnRow))
						* If the upc field is empty in the current row, we assume we are past the last row with data in it.
						
						
						m.STYLE = NVL(oWorksheet.RANGE("B"+lcRow).VALUE,'')
						
						m.DESCRIP = NVL(oWorksheet.RANGE("C"+lcRow).VALUE,'')											
						
						m.UPC = NVL(oWorksheet.RANGE("M"+lcRow).VALUE,'')
						
*!*							IF ( ISNULL(m.UPC) OR EMPTY(m.UPC) ) THEN
*!*								* we are past the last data row! exit the loop... 
*!*								EXIT FOR
*!*							ENDIF
						
						IF ( ISNULL(m.STYLE) OR EMPTY(m.STYLE) ) THEN
							* we are past the last data row! exit the loop...
							.TrackProgress('***** STOPPING: Empty Style encountered at row ' + TRANSFORM(lnRow),LOGIT+SENDIT)
							EXIT FOR
						ENDIF
*SET STEP ON 						
						* itemtype is required, per Juan. So skip to next row if empty.
						m.ITEMTYPE = NVL(oWorksheet.RANGE("D"+lcRow).VALUE,'')
						m.ITEMTYPE = UPPER(ALLTRIM(m.ITEMTYPE))	

						IF EMPTY(m.ITEMTYPE) THEN
							.nSkippedRecs = .nSkippedRecs + 1
							LOOP
						ENDIF

						
						IF TYPE('m.STYLE') = 'N' THEN
							m.STYLE = ALLTRIM(STR(m.STYLE))
						ENDIF
						m.STYLE = PADR(ALLTRIM(m.STYLE),20,' ')


						.cStyle = m.STYLE


						*lcColumnD = NVL(oWorksheet.RANGE("D"+lcRow).VALUE,0)
						*lcColumnD = UPPER(ALLTRIM(lcColumnD))						
						*m.PNP = (lcColumnD = 0)						
						
						m.PNP = .T.
						*m.PNP = lcColumnD  &&( lcColumnD = "TRUE" )  && Per Maria	
							
	
						* put this info in memo field per PG 1/20/15
						*
						* if the are not all zero, memo field INFO should look like
						* LENGTH*12.50
						* WIDTH*32.00
						* HEIGHT*03.27
						
						*lnInner  = NVL(oWorksheet.RANGE("D"+lcRow).VALUE,0) && per PG 1/14/2015
						*lnLength = NVL(oWorksheet.RANGE("E"+lcRow).VALUE,0) && per PG 1/14/2015
						*lnWidth  = NVL(oWorksheet.RANGE("F"+lcRow).VALUE,0) && per PG 1/14/2015
						*lnHeight = NVL(oWorksheet.RANGE("G"+lcRow).VALUE,0) && per PG 1/14/2015
						
						
						lcSize = NVL(oWorksheet.RANGE("J"+lcRow).VALUE,'') && per PG 1/14/2015
						
						lcEAN = NVL(oWorksheet.RANGE("L"+lcRow).VALUE,'') && per PG 1/14/2015
						IF TYPE('lcEAN') = 'N' THEN
							*lcEAN = ALLTRIM(STR(lcEAN))
							* add length 13 to prevent showing up in scientific notation in the string result
							lcEAN = ALLTRIM(STR(lcEAN,13,0))
						ENDIF
						
						m.INFO = ;
							"DIM*" + ALLTRIM(lcSize) + CRLF + ;
							"EAN*"  + ALLTRIM(lcEAN) + CRLF 
					
						
						m.UIC = NVL(oWorksheet.RANGE("E"+lcRow).VALUE,0) 
						IF TYPE('m.UIC') <> 'N' THEN
							m.UIC = VAL(m.UIC)
						ENDIF
						IF m.UIC > 999 THEN
							m.UIC = 999
						ENDIF
						*m.UIC = VAL(m.UIC)
						
						m.CUBE = NVL(oWorksheet.RANGE("I"+lcRow).VALUE,0)
						IF TYPE('m.CUBE') <> 'N' THEN
							m.CUBE = VAL(m.CUBE)
						ENDIF
						*m.CUBE = VAL(m.CUBE)
						
						
						m.WEIGHT = NVL(oWorksheet.RANGE("F"+lcRow).VALUE,'')
						IF TYPE('m.WEIGHT') <> 'N' THEN
							m.WEIGHT = VAL(m.WEIGHT)
						ENDIF
						
						
						m.HTSCODE = NVL(oWorksheet.RANGE("K"+lcRow).VALUE,'')
						IF TYPE('m.HTSCODE') = 'N' THEN
							m.HTSCODE = ALLTRIM(STR(m.HTSCODE))
						ENDIF
						m.HTSCODE = ALLTRIM(m.HTSCODE)
						
						IF 'N/A' $ UPPER(ALLTRIM(m.HTSCODE)) THEN
							m.HTSCODE = ' '
						ENDIF
						
						IF TYPE('m.UPC') = 'N' THEN
							m.UPC = ALLTRIM(STR(m.UPC,14,0))
						ENDIF
						m.UPC = PADR(ALLTRIM(m.UPC),14,' ')
						
						IF 'N/A' $ UPPER(ALLTRIM(m.UPC)) THEN
							m.UPC = SPACE(14)
						ENDIF
*set step on						
*!*							m.DESCRIP = PADR(ALLTRIM(m.DESCRIP),50,' ')

						*****************************************************************
						* for sql, save memvars gotten from Excel spreadsheet to properties for later recovery
						.lPNP = m.PNP
						.cDESCRIP = m.DESCRIP
						.cSTYLE = m.STYLE
						.cUPC = m.UPC
						.nWEIGHT = m.WEIGHT
						.nCUBE = m.CUBE
						.nUIC = m.UIC
						.cINFO = m.INFO
						.cHTSCODE = m.HTSCODE
						.cITEMTYPE = m.ITEMTYPE
						*****************************************************************


*!*							* m.ADDDT = DATETIME()

*!*							SELECT UPCMAST
*!*							LOCATE FOR (ACCOUNTID = m.ACCOUNTID) ;
*!*								AND (STYLE == m.STYLE)
*!*								
*!*							IF FOUND() THEN
*!*							
*!*	*!*								llUpdateIfFound = .T.
*!*	*!*								
*!*	*!*								IF llUpdateIfFound THEN
*!*							
*!*									* update 
*!*									REPLACE UPCMAST.PNP WITH m.PNP, ;
*!*										UPCMAST.DESCRIP WITH m.DESCRIP, ;
*!*										UPCMAST.UPC WITH m.UPC, ;
*!*										UPCMAST.UIC WITH m.UIC, ;
*!*										UPCMAST.CUBE WITH m.CUBE, ;
*!*										UPCMAST.WEIGHT WITH m.WEIGHT, ;
*!*										UPCMAST.HTSCODE WITH m.HTSCODE, ;
*!*										UPCMAST.ITEMTYPE WITH m.ITEMTYPE, ;
*!*										UPCMAST.INFO WITH m.INFO,  ;
*!*										UPCMAST.UPDATEDT WITH m.ADDDT, ;
*!*										UPCMAST.UPDATEBY WITH m.ADDBY ;
*!*										IN UPCMAST

*!*									* per DY, use the existing FoxPro UPCMASTID if we do an insert in SQL
*!*									m.UPCMASTID = UPCMAST.UPCMASTID
*!*									
*!*	*!*								ELSE
*!*	*!*									
*!*	*!*									* just compare to see what is different in new data
*!*	*!*									lcCompare = "Style: " + ALLTRIM(m.STYLE)
*!*	*!*									
*!*	*!*									IF UPCMAST.DESCRIP <> LEFT(ALLTRIM(m.DESCRIP),50) THEN
*!*	*!*										lcCompare = lcCompare + CRLF + CRLF + "DESCRIP [" + UPCMAST.DESCRIP + "] vs [" +  LEFT(ALLTRIM(m.DESCRIP),50) + "]"
*!*	*!*									ENDIF
*!*	*!*									
*!*	*!*									IF ALLTRIM(UPCMAST.UPC) <> ALLTRIM(m.UPC) THEN
*!*	*!*										lcCompare = lcCompare + CRLF + CRLF + "UPC [" + ALLTRIM(UPCMAST.UPC) + "] vs [" +  ALLTRIM(m.UPC) + "]"
*!*	*!*									ENDIF
*!*	*!*									
*!*	*!*									IF UPCMAST.UIC <> m.UIC THEN
*!*	*!*										lcCompare = lcCompare + CRLF + CRLF + "UIC [" + transform(UPCMAST.UIC) + "] vs [" +  transform(m.UIC) + "]"
*!*	*!*									ENDIF
*!*	*!*									
*!*	*!*									*IF UPCMAST.CUBE <> m.CUBE THEN
*!*	*!*									IF UPCMAST.CUBE <> ROUND(m.CUBE,2) THEN
*!*	*!*										lcCompare = lcCompare + CRLF + CRLF + "CUBE [" + transform(UPCMAST.CUBE) + "] vs [" +  transform(m.CUBE) + "]"
*!*	*!*									ENDIF
*!*	*!*									
*!*	*!*									IF UPCMAST.WEIGHT <> m.WEIGHT THEN
*!*	*!*										lcCompare = lcCompare + CRLF + CRLF + "WEIGHT [" + transform(UPCMAST.WEIGHT) + "] vs [" +  transform(m.WEIGHT) + "]"
*!*	*!*									ENDIF
*!*	*!*									
*!*	*!*									IF UPCMAST.HTSCODE <> LEFT(ALLTRIM(m.HTSCODE),10) THEN
*!*	*!*										lcCompare = lcCompare + CRLF + CRLF + "HTSCODE [" + UPCMAST.HTSCODE + "] vs [" + LEFT(ALLTRIM(m.HTSCODE),10) + "]"
*!*	*!*									ENDIF
*!*	*!*									
*!*	*!*									IF UPCMAST.ITEMTYPE <> LEFT(ALLTRIM(m.ITEMTYPE),10) THEN
*!*	*!*										lcCompare = lcCompare + CRLF + CRLF + "ITEMTYPE [" + UPCMAST.ITEMTYPE + "] vs [" + LEFT(ALLTRIM(m.ITEMTYPE),10) + "]"
*!*	*!*									ENDIF
*!*	*!*									
*!*	*!*									IF ALLTRIM(UPCMAST.INFO) <> ALLTRIM(m.INFO) THEN
*!*	*!*										lcCompare = lcCompare + CRLF + CRLF + "INFO [" + ALLTRIM(UPCMAST.INFO) + "] vs [" +  ALLTRIM(m.INFO) + "]"
*!*	*!*									ENDIF

*!*	*!*									.TrackProgress(lcCompare,LOGIT+SENDIT)
*!*	*!*									
*!*	*!*								ENDIF  && llUpdateIfFound
*!*									
*!*								*lnDupeCtr = lnDupeCtr + 1
*!*							ELSE
*!*							
*!*								m.UPCMASTID = dygenpk("upcmast","whall")
*!*								INSERT INTO UPCMAST FROM MEMVAR
*!*								
*!*								*lnCtr = lnCtr + 1
*!*								
*!*							ENDIF	&& found()

						*****************************************************************
						* sql updating below
						
						* save this to property for possible use below after scatter memvar
						*.nUPCMASTID = m.UPCMASTID
						
						SELECT upcmastsql
						LOCATE FOR (ACCOUNTID = m.ACCOUNTID) ;
							AND (STYLE == m.STYLE)
							
						IF FOUND() THEN
							REPLACE upcmastsql.PNP WITH m.PNP, upcmastsql.DESCRIP WITH m.DESCRIP, upcmastsql.UPC WITH m.UPC, ;
								upcmastsql.UIC WITH m.UIC, upcmastsql.CUBE WITH m.CUBE, upcmastsql.WEIGHT WITH m.WEIGHT, upcmastsql.INFO WITH m.INFO, ;
								upcmastsql.HTSCODE WITH m.HTSCODE, upcmastsql.ITEMTYPE WITH m.ITEMTYPE, ;
								upcmastsql.UPDPROC WITH m.UPDPROC, upcmastsql.UPDATEBY WITH m.UPDATEBY, upcmastsql.UPDATEDT WITH m.UPDATEDT IN upcmastsql
								
							lnDupeCtr = lnDupeCtr + 1
						ELSE
							SELECT upcmastsql
							SCATTER MEMVAR MEMO BLANK

							* restore memvars from properties
							STORE .cAddProc TO m.ADDPROC, m.UPDPROC, m.ADDBY, m.UPDATEBY 
							STORE .tADDDT TO m.ADDDT, m.UPDATEDT
							
							m.PNP = .lPNP
							m.DESCRIP = .cDESCRIP
							m.STYLE = .cSTYLE
							m.UPC = .cUPC
							m.ACCOUNTID = .nAccountID
							m.WEIGHT = .nWEIGHT
							m.CUBE = .nCUBE 
							m.UIC = .nUIC
							m.INFO = .cINFO 
							m.HTSCODE = .cHTSCODE
							m.ITEMTYPE = .cITEMTYPE 
							
							IF .lTestMode THEN
								m.upcmastid = 0
							ELSE
								m.upcmastid=sqlgenpk("upcmast","wh")
								*m.UPCMASTID = .nUPCMASTID
							ENDIF
							
							INSERT INTO upcmastsql FROM MEMVAR
							lnCtr = lnCtr + 1
						ENDIF
						*****************************************************************	
						
					ENDFOR
					
					oWorkbook.close()
					
					IF .lTestMode THEN
						.TrackProgress('TEST MODE: did not TU(upcmastsql)',LOGIT+SENDIT)
						SELECT upcmastsql
						BROWSE
					ELSE					
						* tableupate SQL
						IF tu("upcmastsql") THEN
							.TrackProgress('SUCCESS: TU(upcmastsql) returned .T.!',LOGIT+SENDIT)
						ELSE
							* ERROR on table update
							.TrackProgress('ERROR: TU(upcmastsql) returned .F.!',LOGIT+SENDIT)
							=AERROR(UPCERR)
							IF TYPE("UPCERR")#"U" THEN 
								.TrackProgress('Error Description = ' + UPCERR[1,2],LOGIT+SENDIT)
							ENDIF
						ENDIF
					ENDIF

					*.TrackProgress(TRANSFORM(lnCtr ) + ' INSERTS were made from ' + lcSourceFile,LOGIT+SENDIT)
					.TrackProgress(TRANSFORM(.nSkippedRecs) + ' rows were skipped because of missing data',LOGIT+SENDIT)
					.TrackProgress(TRANSFORM(lnCtr) + ' INSERTS were made from ' + lcOrigFile,LOGIT+SENDIT)
					*.TrackProgress(TRANSFORM(lnDupeDelCtr) + ' found records were deleted ',LOGIT+SENDIT)
					.TrackProgress(TRANSFORM(lnDupeCtr) + ' found records were updated/compared ',LOGIT+SENDIT)

					* if the file already exists in the archive folder, make sure it is not read-only.
					* this is to prevent errors we get copying into archive on a resend of an already-archived file.
					* Necessary because the files in the archive folders were sometimes marked as read-only (not sure why)
					llArchived = FILE(lcArchivedFile)
					IF llArchived THEN
						RUN ATTRIB -R &lcArchivedFile.
						*.TrackProgress('   Used ATTRIB -R for re-archiving : ' + lcSourceFile,LOGIT+SENDIT)
						.TrackProgress('   Used ATTRIB -R for re-archiving : ' + lcOrigFile,LOGIT+SENDIT)
					ENDIF

					* archive the source file
					*COPY FILE (lcSourceFile) TO (lcArchivedFile)
					COPY FILE (lcOrigFile) TO (lcArchivedFile)

					* delete the source file
					IF FILE(lcArchivedFile) THEN
						*DELETE FILE (lcSourceFile)
						DELETE FILE (lcOrigFile)
					ENDIF
			
					WAIT CLEAR

				ENDFOR && lnCurrentFile = 1 TO lnNumFiles
			ELSE
				.TrackProgress('Found no files in the STEELSERIES source folder', LOGIT+SENDIT)
			ENDIF

		ENDWITH
		RETURN
	ENDPROC  &&  ProcessFolder
	
	
*!*		PROCEDURE UpdateMeasures
*!*			LPARAMETERS tcInputFolder, tcArchivedFolder, tcOrigFolder
*!*			* load a separate spreadsheet with measures, different in format from the regular spreadsheet.
*!*			* If style is found, just update UIC, weight & cube -- no inserts.
*!*			WITH THIS
*!*				LOCAL laFiles[1,5], laFilesSorted[1,6], lnNumFiles, lnCurrentFile, lcSourceFile, lcArchivedFile, lcOrigFile
*!*				LOCAL lnHandle, lcStr, lnCtr, oWorkbook, oWorksheet, lnMaxRow, lnRow, lcRow, lnDupeDelCtr, lnDupeCtr
*!*				LOCAL llArchived, lnColumnF, lcColumnD, lnLength, lnWidth, lnHeight

*!*				PRIVATE	m.ACCOUNTID, m.ADDPROC, m.PNP, m.DESCRIP, m.UPC, m.STYLE, m.WEIGHT, m.CUBE, m.UIC, m.INFO
*!*				PRIVATE m.ADDBY, m.UPDATEBY, m.ADDDT, m.UPDATEDT

*!*				m.ACCOUNTID = .nAccountID
*!*				m.ADDPROC = .cAddProc
*!*				m.ADDBY = .cAddProc

*!*				lnNumFiles = ADIR(laFiles,(tcInputFolder + "*.xls")) 

*!*				IF lnNumFiles > 0 THEN

*!*					* sort file list by date/time
*!*					.SortArrayByDateTime(@laFiles, @laFilesSorted)

*!*					* send an email only if files were found
*!*					.lSendInternalEmailIsOn = .T.

*!*					FOR lnCurrentFile = 1 TO lnNumFiles

*!*						lcSourceFile = tcInputFolder + laFilesSorted[lnCurrentFile,1]
*!*						lcArchivedFile = tcArchivedFolder + laFilesSorted[lnCurrentFile,1]
*!*						lcOrigFile = tcOrigFolder + laFilesSorted[lnCurrentFile,1]

*!*						.TrackProgress('lcSourceFile = ' + lcSourceFile,LOGIT+SENDIT)
*!*						.TrackProgress('lcArchivedFile = ' + lcArchivedFile,LOGIT+SENDIT)
*!*						.TrackProgress('lcOrigFile = ' + lcOrigFile,LOGIT+SENDIT)
*!*						
*!*						
*!*						* move source file into ORIG folder so that, if an error causes the program to abort, the folderscan process does not keep reprocessing the bad source file in an infinite loop
*!*						
*!*						* if the file already exists in the orig folder, make sure it is not read-only.
*!*						llArchived = FILE(lcOrigFile)
*!*						IF llArchived THEN
*!*							RUN ATTRIB -R &lcOrigFile.
*!*							.TrackProgress('   Used ATTRIB -R for re-archiving : ' + lcOrigFile,LOGIT+SENDIT)
*!*						ENDIF

*!*						* archive the source file
*!*						COPY FILE (lcSourceFile) TO (lcOrigFile)

*!*						* always delete the source file
*!*						*IF FILE(lcOrigFile) THEN
*!*							DELETE FILE (lcSourceFile)
*!*						*ENDIF
*!*						
*!*						
*!*						* then process from the file once it is in the ORIG folder
*!*						oWorkbook = .oExcel.workbooks.OPEN(lcOrigFile)
*!*						oWorksheet = oWorkbook.Worksheets[1]

*!*						lnMaxRow = 500
*!*						
*!*						lnCtr = 0
*!*						lnDupeCtr = 0
*!*						lnDupeDelCtr = 0
*!*						
*!*						FOR lnRow = 2 TO lnMaxRow   && start at row x to skip header line(s)
*!*						
*!*							.nRow = lnRow
*!*						
*!*							WAIT WINDOW NOWAIT 'Processing Spreadsheet Row: ' + TRANSFORM(lnRow)
*!*							
*!*							lcRow = ALLTRIM(STR(lnRow))					
*!*							
*!*							m.STYLE = NVL(oWorksheet.RANGE("A"+lcRow).VALUE,'')
*!*													
*!*							IF ( ISNULL(m.STYLE) OR EMPTY(m.STYLE) ) THEN
*!*								* we are past the last data row! exit the loop...
*!*								EXIT FOR
*!*							ENDIF
*!*							
*!*							* per Maria 4/27/15, UIC is the value in the Pack column. 
*!*						
*!*							
*!*							m.UIC = NVL(oWorksheet.RANGE("B"+lcRow).VALUE,0) 
*!*							
*!*							m.CUBE = NVL(oWorksheet.RANGE("G"+lcRow).VALUE,0)
*!*							
*!*							m.WEIGHT = NVL(oWorksheet.RANGE("C"+lcRow).VALUE,0)
*!*							
*!*							
*!*											
*!*							
*!*							m.STYLE = PADR(ALLTRIM(m.STYLE),20,' ')

*!*							 m.ADDDT = DATETIME()

*!*							SELECT UPCMAST
*!*							LOCATE FOR (ACCOUNTID = m.ACCOUNTID) ;
*!*								AND (STYLE == m.STYLE)
*!*								
*!*							IF FOUND() THEN
*!*							
*!*	*!*								* update 
*!*	*!*								IF EMPTY(UPCMAST.UIC) THEN 
*!*	*!*									REPLACE UPCMAST.UIC WITH m.UIC, UPCMAST.UPDATEBY WITH m.ADDBY ,UPCMAST.UPDATEDT WITH m.ADDDT IN UPCMAST
*!*	*!*									lnDupeCtr = lnDupeCtr + 1
*!*	*!*								ENDIF
*!*								
*!*								IF EMPTY(UPCMAST.CUBE) THEN 
*!*									REPLACE UPCMAST.CUBE WITH m.CUBE, UPCMAST.UPDATEBY WITH m.ADDBY ,UPCMAST.UPDATEDT WITH m.ADDDT IN UPCMAST
*!*									lnDupeCtr = lnDupeCtr + 1
*!*								ENDIF
*!*								
*!*								IF EMPTY(UPCMAST.WEIGHT) THEN 
*!*									REPLACE UPCMAST.WEIGHT WITH m.WEIGHT, UPCMAST.UPDATEBY WITH m.ADDBY ,UPCMAST.UPDATEDT WITH m.ADDDT IN UPCMAST
*!*									lnDupeCtr = lnDupeCtr + 1
*!*								ENDIF
*!*								
*!*								
*!*	*!*								REPLACE ;
*!*	*!*									UPCMAST.UIC WITH m.UIC, ;
*!*	*!*									UPCMAST.CUBE WITH m.CUBE, ;
*!*	*!*									UPCMAST.WEIGHT WITH m.WEIGHT, ;
*!*	*!*									UPCMAST.UPDATEDT WITH m.ADDDT, ;
*!*	*!*									UPCMAST.UPDATEBY WITH m.ADDBY ;
*!*	*!*									IN UPCMAST
*!*									
*!*								*!*	ELSE
*!*								*!*		INSERT INTO UPCMAST FROM MEMVAR
*!*								*!*		
*!*								*!*		lnCtr = lnCtr + 1
*!*							ENDIF	
*!*							
*!*						ENDFOR
*!*						
*!*						oWorkbook.close()

*!*						*.TrackProgress(TRANSFORM(lnCtr ) + ' INSERTS were made from ' + lcSourceFile,LOGIT+SENDIT)
*!*						.TrackProgress(TRANSFORM(lnCtr ) + ' INSERTS were made from ' + lcOrigFile,LOGIT+SENDIT)
*!*						.TrackProgress(TRANSFORM(lnDupeDelCtr) + ' found records were deleted ',LOGIT+SENDIT)
*!*						.TrackProgress(TRANSFORM(lnDupeCtr) + ' found records were updated ',LOGIT+SENDIT)

*!*						* if the file already exists in the archive folder, make sure it is not read-only.
*!*						* this is to prevent errors we get copying into archive on a resend of an already-archived file.
*!*						* Necessary because the files in the archive folders were sometimes marked as read-only (not sure why)
*!*						llArchived = FILE(lcArchivedFile)
*!*						IF llArchived THEN
*!*							RUN ATTRIB -R &lcArchivedFile.
*!*							*.TrackProgress('   Used ATTRIB -R for re-archiving : ' + lcSourceFile,LOGIT+SENDIT)
*!*							.TrackProgress('   Used ATTRIB -R for re-archiving : ' + lcOrigFile,LOGIT+SENDIT)
*!*						ENDIF

*!*						* archive the source file
*!*						*COPY FILE (lcSourceFile) TO (lcArchivedFile)
*!*						COPY FILE (lcOrigFile) TO (lcArchivedFile)

*!*						* delete the source file
*!*						IF FILE(lcArchivedFile) THEN
*!*							*DELETE FILE (lcSourceFile)
*!*							DELETE FILE (lcOrigFile)
*!*						ENDIF
*!*				
*!*						WAIT CLEAR

*!*					ENDFOR && lnCurrentFile = 1 TO lnNumFiles
*!*				ELSE
*!*					.TrackProgress('Found no files in the MERKURY source folder', LOGIT+SENDIT)
*!*				ENDIF

*!*			ENDWITH
*!*			RETURN
*!*		ENDPROC  &&  UpdateMeasures
	
	
*!*		FUNCTION ZapZeroes
*!*			* removes the unwanted trailing zeroes in the ID field
*!*			LPARAMETERS tcField
*!*			LOCAL lcRetVal, lnLength, lnDecPos
*!*			lcRetVal = tcField
*!*			
*!*			DO CASE
*!*				CASE "." $ lcRetVal
*!*					* there is a decimal, take only through the first char after the decimal
*!*					lnLength = LEN(lcRetVal)
*!*					lnDecPos = AT(".",lcRetVal)
*!*					lcRetVal = LEFT(lcRetVal,(lnDecPos + 1))
*!*				OTHERWISE
*!*					* no decimal means we can return passed value, no matter if it was numeric or not
*!*			ENDCASE
*!*		
*!*			RETURN lcRetVal
*!*		ENDFUNC


*!*		FUNCTION ExecSQL
*!*			LPARAMETERS tcSQL, tcCursorName, tlNoDataReturnedIsOkay
*!*			LOCAL llRetval, lnResult
*!*			WITH THIS
*!*				* close target cursor if it's open
*!*				IF USED(tcCursorName)
*!*					USE IN (tcCursorName)
*!*				ENDIF
*!*				WAIT WINDOW "Fetching remote data; please wait..." NOWAIT
*!*				lnResult = SQLEXEC(.nSQLHandle, tcSQL, tcCursorName)
*!*				llRetval = ( lnResult > 0 )
*!*				IF llRetval THEN
*!*					* see if any data came back
*!*					IF NOT tlNoDataReturnedIsOkay THEN
*!*						IF (NOT USED(tcCursorName)) OR EOF(tcCursorName) THEN
*!*							llRetval = .F.
*!*							.TrackProgress('No data was returned by this query: ' +CRLF + tcSQL, LOGIT+SENDIT)
*!*							.cSendTo = 'mbennett@fmiint.com'
*!*						ENDIF
*!*					ENDIF
*!*				ELSE
*!*					.TrackProgress("Result Code [" + TRANSFORM(lnResult) + "] returned by this query: " + tcSQL, LOGIT+SENDIT)
*!*					.cSendTo = 'mbennett@fmiint.com'
*!*				ENDIF
*!*				WAIT CLEAR
*!*				RETURN llRetval
*!*			ENDWITH
*!*		ENDFUNC


	PROCEDURE TrackProgress
		* do any combination of Wait Window, writing to logfile, and adding to body of email,
		* based on nFlags parameter.
		LPARAMETERS tcExpression, tnFlags
		WITH THIS
			IF BITAND(tnFlags,LOGIT) = LOGIT THEN
				IF .lLoggingIsOn THEN
					?
					? .cProcessName + "  " + TTOC(DATETIME()) + ": " + tcExpression
				ENDIF
			ENDIF
			IF .lWaitWindowIsOn THEN
				IF BITAND(tnFlags,NOWAITIT) = NOWAITIT THEN
					WAIT WINDOW tcExpression NOWAIT
				ENDIF
				IF BITAND(tnFlags,WAITIT) = WAITIT THEN
					WAIT WINDOW tcExpression TIMEOUT .nWaitWindowTimeout
				ENDIF
			ENDIF
			IF BITAND(tnFlags,SENDIT) = SENDIT THEN
				IF .lSendInternalEmailIsOn THEN
					.cBodyText = .cBodyText + tcExpression + CRLF + CRLF
				ENDIF
			ENDIF
		ENDWITH
	ENDPROC  &&  TrackProgress


	PROCEDURE SortArrayByDateTime
		* expects taInArray[] to be the result of an ADIR()
		LPARAMETERS taInArray, taOutArray
		EXTERNAL ARRAY taInArray, taOutArray
		LOCAL lnRows, lnCols, i, j
		* extending taInArray[] to process the files in date/time order
		lnRows = ALEN(taInArray,1)
		lnCols = ALEN(taInArray,2)
		* create taOutArray with one more column
		DIMENSION taOutArray[lnRows,lnCols + 1]
		* and fill it with taInArray columns AND (filedate + filetime) in new column
		* ( ACOPY() doesn't preserve elements properly because of different # of columns )
		FOR i = 1 TO lnRows
			FOR j = 1 TO lnCols
				taOutArray[i,j] = taInArray[i,j]
			ENDFOR
			taOutArray[i,lnCols + 1] = DTOS(taInArray[i,3]) + "_" + taInArray[i,4]
		ENDFOR
		* sort taOutArray on date/time column
		=ASORT(taOutArray,6)
	ENDPROC

ENDDEFINE
