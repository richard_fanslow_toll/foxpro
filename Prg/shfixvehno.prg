USE F:\shop\shdata\repdetl IN 0 ALIAS repdetl
USE F:\shop\shdata\oildetl IN 0 ALIAS oildetl 
USE F:\shop\shdata\tiredetl IN 0 ALIAS tiredetl 
USE F:\shop\shdata\ordhdr IN 0 ALIAS ordhdr

*!*	USE m:\dev\shdata\repdetl IN 0 ALIAS repdetl
*!*	USE m:\dev\shdata\oildetl IN 0 ALIAS oildetl 
*!*	USE m:\dev\shdata\tiredetl IN 0 ALIAS tiredetl 
*!*	USE m:\dev\shdata\ordhdr IN 0 ALIAS ordhdr

SELECT repdetl
SCAN FOR EMPTY(vehno)
	SELECT ordhdr
	LOCATE FOR ORDNO = repdetl.ORDNO
	IF FOUND() THEN
		REPLACE repdetl.vehno WITH ordhdr.vehno
	ENDIF
ENDSCAN

SELECT oildetl 
SCAN FOR EMPTY(vehno)
	SELECT ordhdr
	LOCATE FOR ORDNO = oildetl.ORDNO
	IF FOUND() THEN
		REPLACE oildetl.vehno WITH ordhdr.vehno
	ENDIF
ENDSCAN

SELECT tiredetl
SCAN FOR EMPTY(vehno)
	SELECT ordhdr
	LOCATE FOR ORDNO = tiredetl.ORDNO
	IF FOUND() THEN
		REPLACE tiredetl.vehno WITH ordhdr.vehno
	ENDIF
ENDSCAN


USE IN repdetl
USE IN oildetl 
USE IN tiredetl 
USE IN ordhdr
