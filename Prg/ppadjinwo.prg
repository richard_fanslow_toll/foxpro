* find or create inbound WO for P&P adjustments
* called from ppinwosql when usesqlinwo() = .t.

lparameters xaccountid, xunitqty, xctnqty

select inwolog
locate for accountid=xaccountid and office=gmasteroffice and wo_date=qdate() and ppadj
if found()
	xwonumadd=inwolog.wo_num
	xidadd=inwolog.inwologid
	xpoadd=maxpo(inwolog.wo_num)

	m.inwologid=inwolog.inwologid
	m.wo_num=inwolog.wo_num

	replace quantity with quantity+xctnqty, remainqty with remainqty+xctnqty, ;
		unitsinqty with unitsinqty+xunitqty, unitsopenqty with unitsopenqty+xunitqty, ;
		cuft with cuft+xctnqty*.42, weight with weight+xctnqty*9 in inwolog

else
	if xsqlexec("select * from inwolog where accountid="+transform(xaccountid)+" and "+gmodfilter+" and wo_date={"+dtoc(qdate())+"} and ppadj=1","xppinwo",,"wh") # 0
		xwonumadd=xppinwo.wo_num
		xidadd=xppinwo.inwologid
		xpoadd=maxpo(xppinwo.wo_num)

		m.inwologid=xppinwo.inwologid
		m.wo_num=xppinwo.wo_num

		addsql("update inwolog set quantity=quantity+"+transform(xctnqty)+", remainqty=remainqty+"+transform(xctnqty)+", " + ;
			"unitsinqty=unitsinqty+"+transform(xunitqty)+", unitsopenqty=unitsopenqty+"+transform(xunitqty)+", " + ;
			"cuft=cuft+"+transform(xctnqty*.42)+", weight=weight+"+transform(xctnqty*9)+" where inwologid="+transform(xppinwo.inwologid),"wh")

	else		
		select inwolog
		scatter memvar memo blank
		m.ppadj=.t.
		m.wo_date=qdate()
		m.accountid=xaccountid
		m.acctname=acctname(xaccountid)
		m.unbilled=.t.

		m.wo_num=dygenpk("wonum","wh"+tumod(xaccountid))
		m.quantity=xctnqty
		m.remainqty=xctnqty
		m.unitsinqty=xunitqty
		m.unitsopenqty=xunitqty
		m.cuft=xctnqty*.42
		m.weight=xctnqty*9
		m.refer_num=whrefnum(xaccountid)
		m.container="PPADJ"
		m.comments="P&P ADJUSTMENT"
		m.unbilledcomment="P&P ADJUSTMENT"
		m.confirmdt=qdate(.t.)
		insertinto("inwolog","wh",.t.)

		m.inwologid=inwolog.inwologid
		xwonumadd=m.wo_num
		xidadd=m.inwologid
		xpoadd=maxpo(inwolog.wo_num)
	endif
endif

return xpoadd