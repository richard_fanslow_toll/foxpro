**vasload.prg
**
**load vas codes into xvas cursor
**called from packout and scanpackbase class
**

select outdet
for xvascnt = 1 to 10 &&up
	m.vascode=getmemodata("printstuff","VAS"+transform(xvascnt),.f.,.t.)
	if empty(m.vascode)
		exit
	endif

	scatter fields upc, style, color, id, custsku memvar
	m.upc=outdet.upc
	do case
	case m.vascode="V01"
		m.vasdesc="Carton Labels - Contents (Etiquetas del Carton - Contenido)"
	case m.vascode="V02"
		m.vasdesc="Carton Labels - Ariat Provided (Etiquetas del Carton  - Proveidas por Ariat)"
	case m.vascode="V03"
		m.vasdesc="Carton Labels - DC Provided (Etiquetas del Carton - Creadas en Almacen)"
	case m.vascode="V04"
		m.vasdesc="Product Labels - Ariat Provided (Etiquetas del Producto - Proveidas por Ariat)"
	case m.vascode="V05"
		m.vasdesc="Product Labels - DC Provided (Etiquetas del Producto - Creadas en Almacen)"
	case m.vascode="V06"
		m.vasdesc="Packing List - Special Paper, Color (Lista de Embalaje - Papel Especial de Colores)"
	case m.vascode="V07"
		m.vasdesc="Packing List - DC Provided from Routing (Lista de Embalaje - Proveida por el Almacen)"
	case m.vascode="V08"
		m.vasdesc="Copy/Apply Order Packing List onto Each Carton (Colocar Lista de Embalaje en cada Carton)"
	case m.vascode="V09"
		m.vasdesc="Inserts - Drop Ship (Insertos - Drop Ship)"
	case m.vascode="V10"
		m.vasdesc="Inserts - Customer Provided ("+alltrim(xxConsignee)+") (Insertos - Proveidos por el Cliente)"
	case m.vascode="V11"
		m.vasdesc="Inserts - Events and Launches ("+alltrim(xxConsignee)+") (Insertos - Eventos y Lanzamientos)"
	case m.vascode="V12"
		m.vasdesc="Packing Musical (With Text) (Empacado Especial (Con Texto))"
	case m.vascode="V13"
		m.vasdesc="Remove Boots from Boxes, Shrink Wrap; Ship in Bulk (Quite las Botas de las Cajas, envuelvalas en plastico y envielas al mayor)"
	case m.vascode="V14"
		m.vasdesc="Email Packing List and Commercial Invoice to Broker (E-Mail la Factura y Lista de Embalaje al Broker)"
	case m.vascode="V15"
		m.vasdesc="Tape Shoe Boxes (Coloque Tape on las Cajas de Zapatos)"
	case m.vascode="V16"
		m.vasdesc="Rubber Band Shoe Boxes (Coloque Banda Elastica en las Cajas)"
	case m.vascode="V17"
		m.vasdesc="Remove Hanger (Quite la Percha)"
	case m.vascode="V18"
		m.vasdesc="Place in Clear Poly Bag (Coloque en Bolsa Plastica Transparente)"
	case m.vascode="V19"
		m.vasdesc="Remove MSRP (Quitele el Precio)"
	case m.vascode="V20"
		m.vasdesc="Print Commercial Invoice (Imprima Factura)"
	case m.vascode="V21"
		m.vasdesc="Apply Carton Sticker (Coloque Etiqueta al Carton)"
	case m.vascode="V22"
		m.vasdesc="Orscheln Case Pack UPC Labels Required (Necesita Etiquetas Orscheln en el Carton )"
	case m.vascode="V23"
		m.vasdesc="Contact Ariat for Special Instructions (Cav's) (Contactar a Ariat para Instrucciones Especiales)"
	case m.vascode="V24"
		m.vasdesc="Contact Ariat for Special Instructions (BB) (Contactar a Ariat para Instrucciones Especiales)"
	case m.vascode="V25"
		m.vasdesc="Shep - Mark for Store (Shep - Marcar Para Tienda)"
	case m.vascode="V26"
		m.vasdesc="Exclude Packing List (Excluir Lista de Embalaje)"
	case m.vascode="V27"
		m.vasdesc="100% Inspection (Inspeccion al 100%)"
	case m.vascode="V28"
		m.vasdesc="TMS Site Packing Slip (Lista de Embalaje de TMS)"
	case m.vascode="V29"
		m.vasdesc="No Mailer Bags,Items to Ship in Corrugated Boxes Only! (No empaque en bolsa plastica, solo en cajas de carton!)"
	case m.vascode="V30"
		m.vasdesc="Add Hanger (A�adir Perchero)"
	case m.vascode="V31"
		m.vasdesc="Email Packing List (Enviar Packing List por E-Mail)"
	case m.vascode="V32"
		m.vasdesc="UCC 128 Placement Bottom left (Colocar UCC 128 en izquierda inferior)"
	case m.vascode="V33"
		m.vasdesc="Remove Polybag and Hangers (Retirar Bolsa Plastica y Percha/Perchero)"
	case m.vascode="V39"
		m.vasdesc="Cabela's code for label placement (El c�digo de Cabela para la colocaci�n de etiquetas)"
	case m.vascode="V41"
		m.vasdesc="Place packing list on outside of carton (Coloque la lista de empaque en el exterior de la caja de cart�n)"

	otherwise
		m.vasdesc="Unknown VAS Code (Codigo VAS desconocido)"
	endcase
	insert into xvas from memvar
endfor
