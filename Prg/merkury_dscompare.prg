PARAMETERS lSkipReport
CLOSE DATA ALL
DO  m:\dev\prg\_setvars WITH .T.
PUBLIC dDate1
gOffice = "2"

nGoBack = 0 && Number of days to backdate report. Default value = 0 (today)
ddate1 = DATE()-nGoBack

USE F:\3pl\DATA\edi_trigger IN 0 ALIAS edi_trigger SHARED NOUPDATE

xsqlexec("select * from outship where accountid = 6561 and del_date = {"+DTOC(ddate1)+"}",,,"wh")

WAIT WINDOW "Now selecting OUTSHIP recs..." NOWAIT NOCLEAR
SELECT distinct ship_ref,scac ;
	FROM outship ;
	WHERE accountid = 6561 ;
	AND (del_date = dDate1) ; 
	AND (INLIST(scac,"MIMK","FSP") OR INLIST(cacctnum,"GROUPDS","LIVSO10","QUIB","SUPPORT","STAPL")) ;
	order by ship_ref ;
	INTO CURSOR temp1
	IF lSkipReport
	locate
*	BROWSE
	endif
noutrecs  = RECCOUNT('temp1')

*!* Current day at select start time, based on days back in time
nYear = YEAR(DATE()-nGoBack)
nMth = MONTH(DATE()-nGoBack)
nDay = DAY(DATE()-nGoBack)

*!* Next day at select end time, based on days back in time
nYear2 = YEAR((DATE()-nGoBack)+1)
nMth2 = MONTH((DATE()-nGoBack)+1)
nDay2 = DAY((DATE()-nGoBack)+1)

WAIT CLEAR
WAIT WINDOW "Now selecting TRIGGER recs..." NOWAIT NOCLEAR
SELECT ship_ref ;
	FROM edi_trigger ;
	WHERE accountid = 6561 ;
	AND BETWEEN(trig_time,DATETIME(nYear,nMth,nDay,18,00,00),DATETIME(nYear2,nMth2,nDay2,02,00,00) );
	AND (INLIST(scac,"MIMK","FSP") OR PTFlag = .t.) ;
	order by ship_ref ;
	INTO CURSOR temp2
	IF lSkipReport
	locate
*	BROWSE
	endif
nedirecs  = RECCOUNT('temp2')

WAIT WINDOW "Outship recs: "+ALLTRIM(STR(noutrecs))+CHR(13)+"Trig recs: "+ALLTRIM(STR(nedirecs)) TIMEOUT 5

DO CASE
SET STEP ON 
	CASE (noutrecs = 0 AND nedirecs = 0)
		WAIT WINDOW "There were no Merkury orders processed today." TIMEOUT 2
	CASE (noutrecs # nedirecs)
		SET STEP ON 
		WAIT WINDOW "There was a discrepancy between Outship and EDI_Trigger" TIMEOUT 2
		IF !lSkipReport
		failreport()
		endif
			USE IN edi_trigger
			USE IN temp1
			USE IN temp2
			DO m:\dev\prg\merkury_dsreport
	OTHERWISE
		IF !lSkipReport
			WAIT WINDOW "Record counts match...running Report phase" TIMEOUT 1
			USE IN edi_trigger
			USE IN temp1
			USE IN temp2
			DO m:\dev\prg\merkury_dsreport
*!*			ELSE
*!*				WAIT WINDOW "Report skipped...exiting" TIMEOUT 1
		ENDIF
ENDCASE

*SET STEP ON
CLOSE DATA ALL
RETURN

PROCEDURE failreport
	cPTIssue = ""
	SELECT a.ship_ref,b.ship_ref AS ptedi ;
		FROM temp1 a LEFT JOIN temp2 b ;
		ON a.ship_ref = b.ship_ref ;
		HAVING ISNULL(ptedi) ;
		INTO CURSOR tempmatch
	LOCATE
	SCAN
		cPTIssue = cPTIssue+CHR(13)+ALLTRIM(tempmatch.ship_ref)
	ENDSCAN
	tfrom = "TGF EDI Operations <toll-edi-ops@tollgroup.com>"
	tsendto = "joe.bianchi@tollgroup.com"
	tattach = ""
	tcc = "maria.estrella@tollgroup.com,paul.gaidis@tollgroup.com"
	tsubject = "Merkury Drop Ship Report Error, "+DTOC(dDate1)
	tmessage=""
	tmessage = "The Merkury DS report showed the following PT discrepancies between Outship and EDI_Trigger: "
	tmessage = tmessage+CHR(13)+cPTIssue

	DO FORM m:\dev\frm\dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
ENDPROC

