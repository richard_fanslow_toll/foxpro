** CheckServer.PRG
**
** lWordExists = CheckServer("Word.Application")
**
** also, need registry.prg in the project (or default directory)
**
LPARAMETER cServerName

if vartype(cServerName)<>"C"
   cServerName = "Word.Application" && Default check for Word
endif

LOCAL oRegistry, cClassID, cEXEName, lEXEExists, ;
      aClassIDValues, aClassIDValues, aServerNameValues

*!*	IF VERSION() >= "Visual FoxPro 06"
*!*	   oRegistry = NewObject("Registry", Home()+"ffc\Registry")
*!*	ELSE
  SET PROCEDURE TO registry.prg ADDITIVE
  oRegistry = CreateObject("Registry")
*!*	ENDIF

lEXEExists = .F.

DECLARE aClassIDValues[1], aServerNameValues[1]

WITH oRegistry
  * Find the CLSID of the server. First, look for
  * the Class's Key.
  IF .OpenKey(cServerName + "\CLSID") = 0
    * The Class's Key is open, now enumerate its values
    .EnumKeyValues(@aClassIDValues)

    * The data portion of the first (only) value returned
   * is the CLSID. Find the LocalServer32 key for the CLSID
    IF .OpenKey("CLSID\" + aClassIDValues[1,2] + "\LocalServer32") = 0
      * Enumerate the LocalServer32 values
      .EnumKeyValues(@aServerNameValues)

      * The EXE file is stored in the first (only) data value returned.
      cEXEName = aServerNameValues[2]

      * The value that's returned may have " -Automation" or " /Automation" or
      * " /AUTOMATION" & other trailing stuff at the end. Strip it off.
      IF "AUTO" $ UPPER(cEXEName)
        cEXEName = LEFT(cEXEName, ATC("AUTO", UPPER(cEXEName)) - 2)
      ENDIF

      * Verify that the file exists
      lEXEExists = FILE(cEXEName)
    ENDIF
  ENDIF
ENDWITH

RETURN lEXEExists
