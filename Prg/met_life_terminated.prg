* run GREAT WEST WEEKLY TERMINATIONS REPORT FOR LATEST ADP PAYDATE
LPARAMETERS tcDedCode

utilsetup("MLT")

LOCAL loMetLifeWeeklyTerminatedReport
loMetLifeWeeklyTerminatedReport = CREATEOBJECT('MetLifeWeeklyTerminatedReport')
loMetLifeWeeklyTerminatedReport.MAIN()

schedupdate()

CLOSE DATABASES ALL
RETURN

#DEFINE LOGIT 1
#DEFINE WAITIT 2
#DEFINE NOWAITIT 4
#DEFINE SENDIT 8
#DEFINE CRLF CHR(13) + CHR(10)
#DEFINE RETURN_DATA_MANDATORY .F.
#DEFINE RETURN_DATA_NOT_MANDATORY .T.
#DEFINE CSVFORMAT 6
#DEFINE WRITE_ONLY_UNBUFFERED 11
#DEFINE COMMA ","

DEFINE CLASS MetLifeWeeklyTerminatedReport AS CUSTOM

	cProcessName = 'MetLifeWeeklyTerminatedReport'

	lTestMode = .F.  && .F. FOR NORMAL OPERATION, .T. TO SUPPRESS ALL BUT DEVELOPER EMAILS AND CHANGE INPUT TABLE, ETC.

	dToday = DATE()
	*dToday = {^2012-07-05}

	lAutoYield = .T.

	cStartTime = TTOC(DATETIME())

	* deduction properties
	cDedCode = ''

	* connection properties
	nSQLHandle = 0

	* wait window properties
	nWaitWindowTimeout = 2
	lWaitWindowIsOn = .T.

	* logfile properties
	lLoggingIsOn = .T.
	cLogFile = 'F:\UTIL\HR\METLIFETERMINATED\LOGFILES\METLIFE_WEEKLY_TERMINATIONS_REPORT_log.txt'

	* Deds K/O/N properties
	lGetOnlyLatestPaydate = .T.

	* INTERNAL email properties
	lSendInternalEmailIsOn = .T.
	*cFrom = 'FMI Corporate <fmicorporate@fmiint.com>'
	cFrom = 'mark.bennett@tollgroup.com'
	cSendTo = 'george.gereis@tollgroup.com'
	*cSendTo = 'mbennett@fmiint.com'
	cCC = 'mark.bennett@tollgroup.com'
	cSubject = 'Weekly Terminations Report for Great West'
	cAttach = ''
	cBodyText = ''

	cCOMPUTERNAME = ''
	cUSERNAME = ''


	FUNCTION INIT
		IF NOT DODEFAULT()
			RETURN .F.
		ENDIF
		WITH THIS
			*SET RESOURCE OFF
			CLOSE DATA
			SET CENTURY ON
			SET DATE AMERICAN
			SET HOURS TO 24
			SET ANSI ON
			SET TALK OFF
			SET DELETED ON
			SET CONSOLE OFF
			SET EXCLUSIVE OFF
			SET SAFETY OFF
			SET EXACT OFF
			SET STATUS BAR ON
			SET SYSMENU OFF
			SET ENGINEBEHAVIOR 70
			_VFP.AUTOYIELD = .lAutoYield
			.cCOMPUTERNAME = GETENV("COMPUTERNAME")
			.cUSERNAME = GETENV("USERNAME")
			IF .lTestMode THEN
				.cLogFile = 'F:\UTIL\HR\METLIFETERMINATED\LOGFILES\METLIFE_WEEKLY_TERMINATIONS_REPORT_log_TESTMODE.txt'
				.cSendTo = 'Mark Bennett <mbennett@fmiint.com>'
			ENDIF
			.lLoggingIsOn = .lLoggingIsOn AND NOT EMPTY(.cLogFile)
			IF .lLoggingIsOn THEN
				SET ALTERNATE TO (.cLogFile) ADDITIVE
				SET ALTERNATE ON
			ENDIF
		ENDWITH
	ENDFUNC


	FUNCTION DESTROY
		WITH THIS
			IF .lLoggingIsOn  THEN
				SET ALTERNATE OFF
				SET ALTERNATE TO
			ENDIF
		ENDWITH
		DODEFAULT()
	ENDFUNC


	FUNCTION MAIN
		WITH THIS
			LOCAL lnNumberOfErrors, lcSQL, lcSQL2, lcSQL3, lcSQL4, lcSQL5, lcSQL6, loError
			LOCAL ldEmptyDate, lc401kDed, lcPossibleDuplicates
			LOCAL lcFiletoSaveAs, lcSpreadsheetTemplate
			LOCAL oExcel, oWorkbook, oWorksheet
			TRY
				lnNumberOfErrors = 0

				.TrackProgress('', LOGIT)
				.TrackProgress('', LOGIT)
				.TrackProgress('=========================================================', LOGIT+SENDIT)
				.TrackProgress('GREAT WEST WEEKLY TERMINATIONS REPORT process started....', LOGIT+SENDIT+NOWAITIT)
				.TrackProgress('COMPUTERNAME = ' + .cCOMPUTERNAME, LOGIT+SENDIT)
				.TrackProgress('USERNAME = ' + .cUSERNAME, LOGIT+SENDIT)
				.TrackProgress('=========================================================', LOGIT+SENDIT)
				IF .lTestMode THEN
					.TrackProgress('                        TEST MODE                        ', LOGIT+SENDIT)
					.TrackProgress('=========================================================', LOGIT+SENDIT)
				ENDIF
				.TrackProgress('PROJECT = MLT', LOGIT+SENDIT)

				* okay to proceed.
				
				LOCAL ldToday, ldEndingDate, ldStartingDate, lcSQLEndingDate, lcSQLStartingDate
				
				ldToday = .dToday
				ldEndingDate = ldToday - 1
				ldStartingDate = ldToday - 7
				
*!*					* override values for custom date ranges
*!*					ldStartingDate = {^2006-01-01}
*!*					ldEndingDate = {^2006-10-31}				

				lcPossibleDuplicates = ""

				*!*		" WHERE TERMINATIONDATE > DATE '2004-09-12' " 
				lcSQLEndingDate = " DATE '" + ALLTRIM(STR(YEAR(ldEndingDate))) + "-" + PADL(MONTH(ldEndingDate),2,"0") + "-" + PADL(DAY(ldEndingDate),2,"0") + "'"
				lcSQLStartingDate = " DATE '" + ALLTRIM(STR(YEAR(ldStartingDate))) + "-" + PADL(MONTH(ldStartingDate),2,"0") + "-" + PADL(DAY(ldStartingDate),2,"0") + "'"

				.cSubject = 'TGF Terminations for Date Range: ' + DTOC(ldStartingDate) + " - " + DTOC(ldEndingDate)

				* get ss#s from ADP
				OPEN DATABASE F:\UTIL\ADPCONNECTION\CONNECT1

				.nSQLHandle = SQLCONNECT("PCPAYWIN","MBENNETT","VP737A")

				IF .nSQLHandle > 0 THEN

					lcSQL2 = ;
						"SELECT " + ;
						" NAME, " + ;
						" FILE# AS FILE_NUM, " + ;
						" SOCIALSECURITY# AS SS_NUM, " + ;
						" HOMEDEPARTMENT AS HOMEDEPT, " + ;
						" COMPANYCODE AS ADP_COMP, " + ;
						" STREETLINE1 AS STREET1, " + ;
						" {fn IFNULL(STREETLINE2,' ')} AS STREET2, " + ;
						" CITY, " + ;
						" STATE, " + ;
						" ZIPCODE, " + ;
						" {fn IFNULL(GENDER,'?')} AS GENDER, " + ;
						" HIREDATE, " + ;
						" TERMINATIONDATE AS TERMDATE, " + ;
						" BIRTHDATE, " + ;
						" STATUS " + ;
						" FROM REPORTS.V_EMPLOYEE " + ;
						" WHERE STATUS = 'T' " + ;
						" AND TERMINATIONDATE >= " + lcSQLStartingDate + ;
						" AND TERMINATIONDATE <= " + lcSQLEndingDate + ;
						" AND COMPANYCODE IN ('E87','E88','E89') "

*!*						lcSQL3 = ;
*!*							"SELECT " + ;
*!*							" NAME, " + ;
*!*							" COMPANYCODE AS ADP_COMP, " + ;
*!*							" FILE# AS FILE_NUM, " + ;
*!*							" SOCIALSECURITY# AS SS_NUM, " + ;
*!*							" {FN IFNULL(DEDFACTOR,0.00)} AS DEDFACTOR " + ;
*!*							" FROM REPORTS.V_DEDUCTIONS " + ;
*!*							" WHERE DEDUCTIONCODE = '81' " + ;
*!*							" AND COMPANYCODE IN ('E87','E88','E89') "
						
					lcSQL3 = ;
						"SELECT " + ;
						" NAME, " + ;
						" COMPANYCODE AS ADP_COMP, " + ;
						" FILE# AS FILE_NUM, " + ;
						" SOCIALSECURITY# AS SS_NUM, " + ;
						" {FN IFNULL(DEDUCTIONAMOUNT,0.00)} AS DEDAMOUNT, " + ;
						" {FN IFNULL(DEDFACTOR,0.00)} AS DEDFACTOR " + ;
						" FROM REPORTS.V_DEDUCTIONS " + ;
						" WHERE DEDUCTIONCODE IN ('4','5','21','81','83','84','K') " + ;
						" AND COMPANYCODE IN ('E87','E88','E89') " + ;
						" AND STATUS <> 'T' "



*!*						lcSQL4 = ;
*!*							"SELECT " + ;
*!*							" SOCIALSECURITY# AS SS_NUM " + ;
*!*							" FROM REPORTS.V_CHK_VW_INFO " + ;
*!*							" WHERE CHECKVIEWPAYDATE >= " + lcSQLStartingDate + ;
*!*							" AND CHECKVIEWPAYDATE <= " + lcSQLEndingDate + ;
*!*							" GROUP BY SOCIALSECURITY# "

					lcSQL5 = ;
						"SELECT " + ;
						" NAME, " + ;
						" FILE# AS FILE_NUM, " + ;
						" SOCIALSECURITY# AS SS_NUM, " + ;
						" HOMEDEPARTMENT AS HOMEDEPT, " + ;
						" COMPANYCODE AS ADP_COMP, " + ;
						" STREETLINE1 AS STREET1, " + ;
						" {fn IFNULL(STREETLINE2,' ')} AS STREET2, " + ;
						" CITY, " + ;
						" STATE, " + ;
						" ZIPCODE, " + ;
						" {fn IFNULL(GENDER,'?')} AS GENDER, " + ;
						" HIREDATE, " + ;
						" TERMINATIONDATE AS TERMDATE, " + ;
						" BIRTHDATE, " + ;
						" STATUS " + ;
						" FROM REPORTS.V_EMPLOYEE " + ;
						" WHERE HIREDATE >= " + lcSQLStartingDate + ;
						" AND COMPANYCODE IN ('E87','E88','E89') "

					IF .lTestMode THEN
						.TrackProgress('                        TEST MODE                        ', LOGIT+SENDIT)
						.TrackProgress('lcSQL2 =' + lcSQL2, LOGIT+SENDIT)
					ENDIF

					IF USED('CUREMPLOYEESPRE') THEN
						USE IN CUREMPLOYEESPRE
					ENDIF
					IF USED('CURSSNUMS') THEN
						USE IN CURSSNUMS
					ENDIF
					IF USED('CURKMATCH') THEN
						USE IN CURKMATCH
					ENDIF
					IF USED('CURKMATCH2') THEN
						USE IN CURKMATCH2
					ENDIF

					IF .ExecSQL(lcSQL2, 'CUREMPLOYEESPRE', RETURN_DATA_MANDATORY) ;
							AND .ExecSQL(lcSQL3, 'CURKMATCH', RETURN_DATA_MANDATORY) ;
							AND .ExecSQL(lcSQL5, 'CURALLEMPLOYEES', RETURN_DATA_NOT_MANDATORY) THEN
*!*								AND .ExecSQL(lcSQL4, 'CURSSNUMS', RETURN_DATA_MANDATORY) THEN

*!*							SELECT * ;
*!*								FROM CURKMATCH ;
*!*								INTO CURSOR CURKMATCH2 ;
*!*								WHERE (DEDFACTOR > 0) ;
*!*								AND SS_NUM IN (SELECT SS_NUM FROM CUREMPLOYEESPRE) ;
*!*								ORDER BY NAME
							
						SELECT * ;
							FROM CURKMATCH ;
							INTO CURSOR CURKMATCH2 ;
							WHERE ( (DEDAMOUNT > 0) OR (DEDFACTOR > 0) ) ;
							AND SS_NUM IN (SELECT SS_NUM FROM CUREMPLOYEESPRE) ;
							ORDER BY NAME
							
						IF USED('CURKMATCH2') AND NOT EOF('CURKMATCH2') THEN
							* at least one of the terminated had scheduled 401k deductions -- build a list
							lc401kDed = "The following terminated employees had scheduled 401k Deductions:"
							SELECT CURKMATCH2
							SCAN
								lc401kDed = lc401kDed + CRLF + ;
								"Company: " + LEFT(CURKMATCH2.ADP_COMP,3) + "  Name: " + LEFT(CURKMATCH2.NAME,30) + "  File#: " + TRANSFORM(CURKMATCH2.FILE_NUM) + "  DedAmount: " + TRANSFORM(CURKMATCH2.DEDAMOUNT) + "  DedFactor: " + TRANSFORM(CURKMATCH2.DedFactor)
							ENDSCAN
						ELSE
							* none of the terminated had scheduled 401k deductions
							lc401kDed = "None of the terminated employees had scheduled 401k Deductions!"
						ENDIF
							
						* am seeing one employee with null homedepat & hiredate (richard schuessler) but its a duplicate
						* of a good entry for him. Remove these bad entries.
						SELECT CUREMPLOYEESPRE
						DELETE FOR ISNULL(HOMEDEPT)
						LOCATE
						
						* more cleanup
						ldEmptyDate = {}
						SELECT CUREMPLOYEESPRE
						SCAN FOR ISNULL(BIRTHDATE)
							REPLACE BIRTHDATE WITH ldEmptyDate
						ENDSCAN
						SELECT CUREMPLOYEESPRE
						SCAN FOR ISNULL(HIREDATE)
							REPLACE HIREDATE WITH ldEmptyDate
						ENDSCAN

						
						* sort employees cursor so that when searching later we would always get latest record.
						* Necessary because any ss# could have more than one employee record if employee has changed companies, for example.
						* In that case the record for the old company would be terminated, so put active records on top of cursor.
						* Note that you also can't depend on hiredate changing in that case -- it usually would not.

					IF USED('CUREMPLOYEES') THEN
						USE IN CUREMPLOYEES
					ENDIF
					
					SELECT * ;
						FROM CUREMPLOYEESPRE ;
						INTO CURSOR CUREMPLOYEES ;
						ORDER BY SS_NUM, HIREDATE

						**************************************************************************************************************
						**************************************************************************************************************
						** setup file names

						lcSpreadsheetTemplate = "F:\UTIL\HR\METLIFETERMINATED\GREAT_WEST_TEMPLATE.XLS"

						lcFiletoSaveAs = "F:\UTIL\HR\METLIFETERMINATED\REPORTS\FMI_TERMINATIONS_" + ;
							STRTRAN(DTOC(ldStartingDate),"/","-") + "_THRU_" + STRTRAN(DTOC(ldEndingDate),"/","-") +   ".XLS"

						**************************************************************************************************************
						**************************************************************************************************************

						IF FILE(lcFiletoSaveAs) THEN
							DELETE FILE (lcFiletoSaveAs)
						ENDIF

						* now create main report cursor of distinct ss#s from CURSSNUM
						
						IF USED('CURMETLIFE') THEN
							USE IN CURMETLIFE
						ENDIF

						SELECT DISTINCT ;
							SS_NUM, ;
							SPACE(30) AS NAME, ;
							00000 AS FILE_NUM, ;
							SPACE(3) AS ADP_COMP, ;
							0000.00 AS K_AMOUNT, ;
							0000.00 AS K_MATCH, ;
							0000.00 AS N_AMOUNT, ;
							0000 AS NUM_WEEKS, ;
							0000 AS NUM_HOURS, ;
							SPACE(2) AS DIVISION, ;
							SPACE(30) AS STREET1, ;
							SPACE(30) AS STREET2, ;
							SPACE(20) AS CITY, ;
							SPACE(2) AS STATE, ;
							SPACE(10) AS ZIPCODE, ;
							SPACE(1) AS GENDER, ;
							{} AS HIREDATE, ;
							{} AS TERMDATE, ;
							{} AS BIRTHDATE ;
							FROM CUREMPLOYEES ;
							INTO CURSOR CURMETLIFE ;
							ORDER BY SS_NUM ;
							READWRITE

						SELECT CURMETLIFE
						SCAN							
							* get additional info from employee table:
							SELECT CUREMPLOYEES
							LOCATE FOR (SS_NUM = CURMETLIFE.SS_NUM)
							IF FOUND() THEN
								REPLACE ;
									CURMETLIFE.NAME WITH CUREMPLOYEES.NAME , ;
									CURMETLIFE.FILE_NUM WITH CUREMPLOYEES.FILE_NUM, ;
									CURMETLIFE.ADP_COMP WITH ALLTRIM(CUREMPLOYEES.ADP_COMP), ;
									CURMETLIFE.DIVISION WITH LEFT(CUREMPLOYEES.HOMEDEPT,2) , ;
									CURMETLIFE.STREET1 WITH CUREMPLOYEES.STREET1 , ;
									CURMETLIFE.STREET2 WITH CUREMPLOYEES.STREET2 , ;
									CURMETLIFE.CITY WITH CUREMPLOYEES.CITY , ;
									CURMETLIFE.STATE WITH CUREMPLOYEES.STATE , ;
									CURMETLIFE.ZIPCODE WITH CUREMPLOYEES.ZIPCODE , ;
									CURMETLIFE.GENDER WITH CUREMPLOYEES.GENDER , ;
									CURMETLIFE.HIREDATE WITH TTOD(CUREMPLOYEES.HIREDATE) , ;
									CURMETLIFE.TERMDATE WITH TTOD(CUREMPLOYEES.TERMDATE) , ;
									CURMETLIFE.BIRTHDATE WITH TTOD(CUREMPLOYEES.BIRTHDATE)
							ENDIF
						ENDSCAN						

						
						* SCAN FOR POSSIBLE DUPLICATES (I.E. EMPLOYEE WHO IS IN ADP WITH A DIFFERENT FILE#)
						IF USED('CURALLEMPLOYEES') AND NOT EOF('CURALLEMPLOYEES') THEN
						
							lcPossibleDuplicates = ""
							SELECT CURMETLIFE
							SCAN
								SELECT CURALLEMPLOYEES
								SCAN FOR (SS_NUM = CURMETLIFE.SS_NUM) AND (FILE_NUM <> CURMETLIFE.FILE_NUM)
									lcPossibleDuplicates = lcPossibleDuplicates + CRLF + ;
										"Name: " + LEFT(CURALLEMPLOYEES.NAME,30) + ;
										"  File #s: " + TRANSFORM(CURMETLIFE.FILE_NUM) + ;
										", " + TRANSFORM(CURALLEMPLOYEES.FILE_NUM) + ; 
										"  HireDate: " + TRANSFORM(TTOD(CURALLEMPLOYEES.HIREDATE))
								ENDSCAN
							ENDSCAN
							
							IF NOT EMPTY(lcPossibleDuplicates) THEN
								lcPossibleDuplicates = "WARNING: The following employees had other File #s in ADP with Hire Dates on or after their Termination Dates:" + ;
								CRLF + CRLF + lcPossibleDuplicates + CRLF + CRLF
							ENDIF
							
						ENDIF										

						*** SORT HERE !!!!
						IF USED('CURMETLIFE2') THEN
							USE IN CURMETLIFE2
						ENDIF

						SELECT * FROM CURMETLIFE INTO CURSOR CURMETLIFE2 ORDER BY NAME
						

						***********************************************************************************
						***********************************************************************************
						** output to Excel spreadsheet
						.TrackProgress('Creating spreadsheet: ' + lcFiletoSaveAs, LOGIT+SENDIT+NOWAITIT)

						oExcel = CREATEOBJECT("excel.application")
						oExcel.VISIBLE = .F.
						oWorkbook = oExcel.workbooks.OPEN(lcSpreadsheetTemplate)
						oWorkbook.SAVEAS(lcFiletoSaveAs)

						oWorksheet = oWorkbook.Worksheets[1]
						oWorksheet.RANGE("A4","AT2000").ClearContents()

						LOCAL lnStartRow, lnRow, lnEndRow, lnRowsProcessed, lcStartRow, lcEndRow

						lnStartRow = 4
						lcStartRow = ALLTRIM(STR(lnStartRow))

						lnEndRow = lnStartRow - 1
						lnRow = lnStartRow - 1

						SELECT CURMETLIFE2
						SCAN
							lnRow = lnRow + 1
							lnEndRow = lnRow

							lcRow = ALLTRIM(STR(lnRow))

							oWorksheet.RANGE("A"+lcRow).VALUE = "D"
							*oWorksheet.RANGE("B"+lcRow).VALUE = "108343"
							oWorksheet.RANGE("B"+lcRow).VALUE = "455221-01"
							oWorksheet.RANGE("C"+lcRow).VALUE = "'" + CURMETLIFE2.SS_NUM
							oWorksheet.RANGE("D"+lcRow).VALUE = CURMETLIFE2.NAME
							oWorksheet.RANGE("E"+lcRow).VALUE = "**A"
*!*								oWorksheet.RANGE("F"+lcRow).VALUE = CURMETLIFE2.K_AMOUNT
							oWorksheet.RANGE("G"+lcRow).VALUE = "**D"
*!*								oWorksheet.RANGE("H"+lcRow).VALUE = CURMETLIFE2.K_MATCH
							oWorksheet.RANGE("M"+lcRow).VALUE = "001"
*!*								oWorksheet.RANGE("N"+lcRow).VALUE = CURMETLIFE2.N_AMOUNT
							oWorksheet.RANGE("U"+lcRow).VALUE = CURMETLIFE2.STREET1
							oWorksheet.RANGE("V"+lcRow).VALUE = CURMETLIFE2.STREET2
							oWorksheet.RANGE("X"+lcRow).VALUE = CURMETLIFE2.CITY
							oWorksheet.RANGE("Y"+lcRow).VALUE = CURMETLIFE2.STATE
							oWorksheet.RANGE("Z"+lcRow).VALUE = "'" + CURMETLIFE2.ZIPCODE
							oWorksheet.RANGE("AA"+lcRow).VALUE = "USA"
							oWorksheet.RANGE("AB"+lcRow).VALUE = CURMETLIFE2.GENDER
							oWorksheet.RANGE("AC"+lcRow).VALUE = "'" + CURMETLIFE2.DIVISION
							oWorksheet.RANGE("AD"+lcRow).VALUE = IIF(EMPTY(CURMETLIFE2.BIRTHDATE),'',CURMETLIFE2.BIRTHDATE)
							oWorksheet.RANGE("AE"+lcRow).VALUE = IIF(EMPTY(CURMETLIFE2.HIREDATE),'',CURMETLIFE2.HIREDATE)
							oWorksheet.RANGE("AF"+lcRow).VALUE = IIF(EMPTY(CURMETLIFE2.TERMDATE),'',CURMETLIFE2.TERMDATE)
							oWorksheet.RANGE("AH"+lcRow).VALUE = "'7"

						ENDSCAN

						* populate header counts, sums, etc.
						lnRowsProcessed = lnEndRow - lnStartRow + 1
						lcEndRow = ALLTRIM(STR(lnEndRow))

						* rec count
						oWorksheet.RANGE("D2").VALUE = lnRowsProcessed


						* paycheck date
						oWorksheet.RANGE("V2").VALUE = DTOC(ldStartingDate) + " - " + DTOC(ldEndingDate)

						* SAVE AND QUIT EXCEL
						oWorkbook.SAVE()
						oExcel.QUIT()

						***********************************************************************************
						***********************************************************************************


						IF FILE(lcFiletoSaveAs) THEN
							* attach output file to email
							.cAttach = lcFiletoSaveAs
							.cBodyText = "See attached Great West Weekly Terminations report." + ;
								CRLF + CRLF + "(do not reply - this is an automated report)" + ;
								CRLF + CRLF + "<report log follows>" + ;
								CRLF + .cBodyText
						ELSE
							.TrackProgress('ERROR: unable to email spreadsheet: ' + lcFiletoSaveAs, LOGIT+SENDIT+NOWAITIT)
						ENDIF						
						
						.cBodyText = lc401kDed + CRLF + CRLF + lcPossibleDuplicates + .cBodyText

						CLOSE DATABASES ALL
					ENDIF  &&  .ExecSQL(lcSQL, 'SQLCURSOR1', RETURN_DATA_MANDATORY)

					.TrackProgress('GREAT WEST WEEKLY TERMINATIONS REPORT process ended normally.', LOGIT+SENDIT+NOWAITIT)
				ELSE
					* connection error
					.TrackProgress('Unable to connect to ADP Payroll System.', LOGIT+SENDIT)
				ENDIF   &&  .nSQLHandle > 0

			CATCH TO loError

				.TrackProgress('There was an error.',LOGIT+SENDIT)
				.TrackProgress('The Error # is: ' + TRANSFORM(loError.ErrorNo), LOGIT+SENDIT)
				.TrackProgress('The Error Message is: ' + TRANSFORM(loError.MESSAGE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred in Program: ' + TRANSFORM(loError.PROCEDURE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred at line #: ' + TRANSFORM(loError.LINENO), LOGIT+SENDIT)
				lnNumberOfErrors = lnNumberOfErrors + 1
				IF TYPE('oWorkbook') = "O" THEN
					oWorkbook.SAVE()
				ENDIF
				IF TYPE('oExcel') = "O" THEN
					oExcel.QUIT()
				ENDIF
				CLOSE DATABASES ALL

			ENDTRY

			CLOSE DATA
			WAIT CLEAR
			***************** INTERNAL email results ******************************
			.TrackProgress('About to send status email.',LOGIT)
			.TrackProgress('The logfile is: ' + .cLogFile, SENDIT)
			.TrackProgress('GREAT WEST WEEKLY TERMINATIONS REPORT process started: ' + .cStartTime, LOGIT+SENDIT)
			.TrackProgress('GREAT WEST WEEKLY TERMINATIONS REPORT process finished: ' + TTOC(DATETIME()), LOGIT+SENDIT)

			IF .lSendInternalEmailIsOn THEN
				DO FORM dartmail2 WITH .cSendTo,.cFrom,.cSubject,.cCC,.cAttach,.cBodyText,"A"
				.TrackProgress('Sent status email.',LOGIT)
			ELSE
				.TrackProgress('Did not send status email: THIS.lSendInternalEmailIsOn = FALSE.',LOGIT)
			ENDIF

		ENDWITH
		RETURN
	ENDFUNC && main


	FUNCTION ExecSQL
		LPARAMETERS tcSQL, tcCursorName, tlNoDataReturnedIsOkay
		LOCAL llRetval, lnResult
		WITH THIS
			* close target cursor if it's open
			IF USED(tcCursorName)
				USE IN (tcCursorName)
			ENDIF
			WAIT WINDOW "Fetching remote data; please wait..." NOWAIT
			lnResult = sqlexec(.nSQLHandle, tcSQL, tcCursorName)
			llRetval = ( lnResult > 0 )
			IF llRetval THEN
				* see if any data came back
				IF NOT tlNoDataReturnedIsOkay THEN
					IF (NOT USED(tcCursorName)) OR EOF(tcCursorName) THEN
						llRetval = .F.
						.TrackProgress('No data was returned by this query: ' +CRLF + tcSQL, LOGIT+SENDIT)
						.cSendTo = 'Mark Bennett <mbennett@fmiint.com>'
					ENDIF
				ENDIF
			ELSE
				.TrackProgress("Result Code [" + TRANSFORM(lnResult) + "] returned by this query: " + tcSQL, LOGIT+SENDIT)
				.cSendTo = 'Mark Bennett <mbennett@fmiint.com>'
			ENDIF
			WAIT CLEAR
			RETURN llRetval
		ENDWITH
	ENDFUNC


	PROCEDURE TrackProgress
		* do any combination of Wait Window, writing to logfile, and adding to body of email,
		* based on nFlags parameter.
		LPARAMETERS tcExpression, tnFlags
		WITH THIS
			IF BITAND(tnFlags,LOGIT) = LOGIT THEN
				IF .lLoggingIsOn THEN
					?
					? .cProcessName + "  " + TTOC(DATETIME()) + ": " + tcExpression
				ENDIF
			ENDIF
			IF .lWaitWindowIsOn THEN
				IF BITAND(tnFlags,NOWAITIT) = NOWAITIT THEN
					WAIT WINDOW tcExpression NOWAIT
				ENDIF
				IF BITAND(tnFlags,WAITIT) = WAITIT THEN
					WAIT WINDOW tcExpression TIMEOUT .nWaitWindowTimeout
				ENDIF
			ENDIF
			IF BITAND(tnFlags,SENDIT) = SENDIT THEN
				IF .lSendInternalEmailIsOn THEN
					.cBodyText = .cBodyText + tcExpression + CRLF + CRLF
				ENDIF
			ENDIF
		ENDWITH
	ENDPROC  &&  TrackProgress

ENDDEFINE

