PARAMETERS ManifestNum,xnomsg,xxspecial

*lcContainer ="APLU3984085"

PUBLIC lcOrder,lcOrdDetailsAndCartons,lcCartonOut,lcInwo,nAcctNum,cStatus

*IBDnum = "6000046560"
lcBol = ManifestNum
lcInwo = ManifestNum
nAcctNum = 5687
cStatus = "UNPROCESSED"

*!*  If !Used("ua_counter")
*!*    USE F:\3PL\DATA\UA_COUNTER In 0
*!*  Endif

*!*  lnCounter = ua_counter.seqnum
*!*  replace ua_counter.seqnum With ua_counter.seqnum+1 In ua_counter

lcDocnumber = lcBol

IF !USED("manifest")
	USE F:\wo\wodata\manifest IN 0
ENDIF
IF !USED("uaoutbounds")
	USE F:\3pl\DATA\uaoutbounds IN 0
ENDIF

If !xxspecial
	SELECT manifest
	LOCATE FOR wo_num = VAL(ALLTRIM(TRANSFORM(ManifestNum)))

	*!*	If Found()
	*!*	 lcTrailer = manifest.trailer
	*!*	 lcScac = manifest.carrier
	*!*	Else
	*!*	 lcTrailer = "T695"
	*!*	 lcScac    = "HUBG"
	*!*	Endif
	xerror=.F.

	IF !FOUND()
		xmsg="Trip not found... Cannot produce file."
		IF xnomsg
			WAIT xmsg WINDOW TIMEOUT 3
		ELSE
			MESSAGEBOX(xmsg,16,"Error")
		ENDIF
		xerror=.T.
	ENDIF

	lcTrailer = manifest.trailer
	lcScac = manifest.carrier
Else
	xerror =.f.
	lcTrailer = transform(manifestnum)
	lcScac = "FMIX"
Endif 


SELECT uaoutbounds
LOCATE FOR manifest = VAL(ALLTRIM(TRANSFORM(ManifestNum)))
IF FOUND() AND !xnomsg &&if xnomsg, assume it was intended to be retriggered
	xmsg="Trip has already been sent... Send anyway?"
	IF MESSAGEBOX(xmsg,4+16,"Duplicate")=7
		xerror=.T.
	ENDIF
ENDIF

IF xerror
	ediupdate("ERROR",.t.)

	USE IN manifest
	USE IN uaoutbounds
	RETURN
ENDIF

set step On 

&&first get the # of cartons
xsqlexec("select reference as IBD from ctnucc where accountid = 5687 and outwonum = "+TRANSFORM(ManifestNum)+" group by reference","xibds",,"wh")

**in case need to trigger again for a specific palletid - 2 other places need to be un commented
*xsqlexec("select reference as IBD from ctnucc where accountid = 5687 and outwonum = "+Transform(Manifestnum)+" and palletid='PLT010693' group by reference","xibds",,"wh")

*****************  OK write out the header here

  lcQuery = "select * from ctnucc where accountid = 5687 and outwonum = "+TRANSFORM(ManifestNum)
  xsqlexec(lcQuery,"xctn",,"wh")
  Select xctn
*  num1 = ctnn.ctnqty
  num1= RECCOUNT("xctn")
  SELECT xctn
  GO TOP

*  gcDocNumber = xctn.REFERENCE
  gcDocNumber = TRANSFORM(ManifestNum)

*!*  IBDnum = "6000027207"
*!*  xsqlexec("select * from ctnucc where accountid = 5687 and reference ='"+Alltrim(IBDnum)+"'","xctn",,"wh")
*!*  num2= Reccount("xctn")

  lnNumCtns = num1
  lnNumCtns = PADL(ALLTRIM(TRANSFORM(lnNumCtns)),5,"0")
&& now for the header of the combined file

* Replace all the predefined data elements
  SET DATE YMD
  SET CENTURY ON

  lcdate =STRTRAN(DTOC(DATE()),"/","")

  SELECT xctn
  GO TOP
  lcShipToID = xctn.shipid
  lcShipFrom = ALLTRIM(whseloc)
***lnNumCtns = Padl(Alltrim(Transform(Reccount("xctn"))),5,"0")


  lcBol = TRANSFORM(lcDocnumber)
  lcHeader = FILETOSTR("f:\underarmour\xmltemplates\transload_outbound_header.txt")

  lcHeader = STRTRAN(lcHeader,"<EXTDOCNUMBER>",ALLTRIM(TRANSFORM(lcDocnumber)))
  lcHeader = STRTRAN(lcHeader,"<DOCNUMBER>",ALLTRIM(TRANSFORM(lcDocnumber))+"_"+ALLTRIM(xibds.ibd) )
  lcHeader = STRTRAN(lcHeader,"<SUBBOL>",ALLTRIM(lcBol))
  lcHeader = STRTRAN(lcHeader,"<CARRIER>",ALLTRIM(lcScac))  && Needs to be a 4 digit SCAC code
  lcHeader = STRTRAN(lcHeader,"<TRAILER>",ALLTRIM(lcTrailer))
  lcHeader = STRTRAN(lcHeader,"<DOCTYPE>","IBDUPD")  && this for Bypass only
  lcHeader = STRTRAN(lcHeader,"<NUMOFCTNS>",lnNumCtns)
  lcHeader = STRTRAN(lcHeader,"<SHIPFROMID>",ALLTRIM(lcShipFrom))
  lcHeader = STRTRAN(lcHeader,"<SHIPTOID>",ALLTRIM(lcShipToID))
  lcHeader = STRTRAN(lcHeader,"<SHIPDATE>",lcdate)
  lcOutStr = lcHeader

  lcEndOrder = FILETOSTR("f:\underarmour\xmltemplates\transload_outbound_endorder.txt")

  lcFooter = FILETOSTR("f:\underarmour\xmltemplates\transload_outbound_footer.txt")

set step on

SELECT xibds

lnRun= 1

SCAN
	lcQuery = "select * from ctnucc where accountid = 5687 and reference ='"+ALLTRIM(xibds.ibd)+"' and outwonum = "+TRANSFORM(ManifestNum)

**in case need to trigger again for a specific palletid - 2 other places need to be un commented
*lcQuery = "select * from ctnucc where accountid = 5687 and reference ='"+Alltrim(xibds.ibd)+"' and outwonum = "+Transform(Manifestnum)+" and palletid = 'PLT010693'"

*!*  	xsqlexec(lcQuery,"xctn",,"wh")
*!*  	num1= RECCOUNT("xctn")
*!*  	SELECT xctn
*!*  	GO TOP

*!*  	gcDocNumber = xctn.REFERENCE

*!*  *!*  IBDnum = "6000027207"
*!*  *!*  xsqlexec("select * from ctnucc where accountid = 5687 and reference ='"+Alltrim(IBDnum)+"'","xctn",,"wh")
*!*  *!*  num2= Reccount("xctn")

*!*  	lnNumCtns = num1
*!*  	lnNumCtns = PADL(ALLTRIM(TRANSFORM(lnNumCtns)),5,"0")
*!*  && now for the header of the combined file

*!*  * Replace all the predefined data elements
*!*  	SET DATE YMD
*!*  	SET CENTURY ON

*!*  	lcdate =STRTRAN(DTOC(DATE()),"/","")

*!*  	SELECT xctn
*!*  	GO TOP
*!*  	lcShipToID = xctn.shipid
*!*  	lcShipFrom = ALLTRIM(whseloc)
*!*  ***lnNumCtns = Padl(Alltrim(Transform(Reccount("xctn"))),5,"0")


*!*  	lcBol = TRANSFORM(lcDocnumber)
*!*  	lcHeader = FILETOSTR("f:\underarmour\xmltemplates\transload_outbound_header.txt")

*!*  	lcHeader = STRTRAN(lcHeader,"<EXTDOCNUMBER>",ALLTRIM(TRANSFORM(lcDocnumber)))
*!*  	lcHeader = STRTRAN(lcHeader,"<DOCNUMBER>",ALLTRIM(TRANSFORM(lcDocnumber))+"_"+ALLTRIM(xibds.ibd) )
*!*  	lcHeader = STRTRAN(lcHeader,"<SUBBOL>",ALLTRIM(lcBol))
*!*  	lcHeader = STRTRAN(lcHeader,"<CARRIER>",ALLTRIM(lcScac))  && Needs to be a 4 digit SCAC code
*!*  	lcHeader = STRTRAN(lcHeader,"<TRAILER>",ALLTRIM(lcTrailer))
*!*  	lcHeader = STRTRAN(lcHeader,"<DOCTYPE>","IBDUPD")  && this for Bypass only
*!*  	lcHeader = STRTRAN(lcHeader,"<NUMOFCTNS>",lnNumCtns)
*!*  	lcHeader = STRTRAN(lcHeader,"<SHIPFROMID>",ALLTRIM(lcShipFrom))
*!*  	lcHeader = STRTRAN(lcHeader,"<SHIPTOID>",ALLTRIM(lcShipToID))
*!*  	lcHeader = STRTRAN(lcHeader,"<SHIPDATE>",lcdate)

&& header written out, next the Order data

	USE IN xctn

*	lcOutStr = lcHeader

*	lcEndOrder = FILETOSTR("f:\underarmour\xmltemplates\transload_outbound_endorder.txt")

	lcOrder=""
	lcOrdDetailsAndCartons=""
	lcCartonOut =""

	m.podata =""

	DO transload_OrderData WITH ALLTRIM(xibds.ibd)
	lcOutStr = lcOutStr+lcOrder+lcOrdDetailsAndCartons

	lcOutStr = lcOutStr &&+lcEndOrder  && close the high level Order loop

	cDate = DTOS(DATE())
	cTruncDate = RIGHT(cDate,6)
	cTruncTime = SUBSTR(TTOC(DATETIME(),1),9,6)
	cfiledate = cDate+cTruncTime

*	lcFooter = FILETOSTR("f:\underarmour\xmltemplates\transload_outbound_footer.txt")

*	lcOutStr = lcOutStr+lcFooter

*	xxfilename = "F:\FTPUSERS\UNDERARMOUR\OUTBOUND\WMSSHP_"+ALLTRIM(gcDocNumber)+"_"+cfiledate+".XML"
*  xxfilename = "F:\FTPUSERS\UNDERARMOUR\OUTBOUND\SPECIAL\WMSSHP_"+ALLTRIM(gcDocNumber)+"_"+cfiledate+".XML"
*	STRTOFILE(lcOutStr,upper(xxfilename))

*	WAIT WINDOW AT 10,10 "Creating file: "+xxfilename TIMEOUT 1

*!*  	SELECT uaoutbounds
*!*  	m.manifest = lcDocnumber
*!*  	m.ibd =ALLTRIM(gcDocNumber)
*!*  	m.shipdt = DATE()
*!*  	m.shiptm = DATETIME()
*!*  	m.shipto = lcShipToID
*!*  	m.shipfrom= lcShipFrom
*!*  	m.qty =  num1
*!*  	m.trailer =lcTrailer
*!*  	m.carrier = lcScac
*!*  	m.filename = xxfilename
*!*  	INSERT INTO uaoutbounds FROM MEMVAR

ENDSCAN
  lcOutStr = lcOutStr+lcFooter

*  xxfilename = "F:\FTPUSERS\UNDERARMOUR\OUTBOUND\WMSSHP_"+ALLTRIM(gcDocNumber)+"_"+cfiledate+".XML"
  xxfilename = "F:\FTPUSERS\UNDERARMOUR\OUTBOUND\hold\WMSSHP_"+ALLTRIM(gcDocNumber)+"_"+cfiledate+".XML"
  STRTOFILE(lcOutStr,upper(xxfilename))

  WAIT WINDOW AT 10,10 "Creating file: "+xxfilename TIMEOUT 1



INSERT INTO F:\edirouting\sftpjobs.DBF (jobname, USERID) VALUES ('PUT_XML_TO_UNDERARMOUR','PGAIDIS')

USE IN xibds
USE IN manifest
USE IN uaoutbounds
USE IN sftpjobs

lIsError = .F.
ediupdate("XML CREATED",lIsError)

*********************************************************************************************************8
PROCEDURE transload_OrderData

	PARAMETERS IBDnum

	m.podata = ""

	xsqlexec("select * from ctnucc where accountid = 5687 and reference ='"+ALLTRIM(xibds.ibd)+"' and outwonum = "+TRANSFORM(ManifestNum),"xctn",,"wh")

**in case need to trigger again for a specific palletid - 2 other places need to be un commented
*xsqlexec("select * from ctnucc where accountid = 5687 and reference ='"+Alltrim(xibds.ibd)+"' and outwonum = "+Transform(Manifestnum)+" and palletid = 'PLT010693'","xctn",,"wh")

	SELECT xctn

	lcOrder = FILETOSTR("f:\underarmour\xmltemplates\transload_outbound_order.txt")
	lcOrder = STRTRAN(lcOrder,"<IBDORDER>",xctn.REFERENCE)
	lcOrder = STRTRAN(lcOrder,"<ASNTYPE>",xctn.upc)
	IF xctn.STYLE="NA"
		lcOrder = STRTRAN(lcOrder,"<CUSTOMERPO>","")
	ELSE
		lcOrder = STRTRAN(lcOrder,"<CUSTOMERPO>",xctn.STYLE)
	ENDIF

	lcOrdDetailsAndCartons=""

	SELECT xctn
	SELECT ponum,serialno AS linenum FROM xctn INTO CURSOR thesepos GROUP BY ponum,linenum


	SELECT thesepos
	SCAN
		m.podata = m.podata+"PO*"+ALLTRIM(thesepos.ponum)+CHR(13)+"POLINE*"+thesepos.linenum+CHR(13)
		SELECT xctn
		LOCATE FOR ponum = thesepos.ponum AND serialno = thesepos.linenum
		lcOrderDetails = xctn.orddetails
		lccarton = FILETOSTR("f:\underarmour\xmltemplates\transload_outbound_carton.txt")
		lcCartonOut = ""

****

*set step On
		SELECT xctn
		lnCtn = 0
		SCAN FOR ponum = thesepos.ponum AND serialno = thesepos.linenum
			lcCartonOut = lcCartonOut+xctn.ctndetails
			lnCtn=lnCtn+1
		ENDSCAN
		m.podata = m.podata+"POQTY*"+ALLTRIM(TRANSFORM(lnCtn))+CHR(13)

		lcOrdDetailsAndCartons=lcOrdDetailsAndCartons+lcOrderDetails+CHR(13)+lcCartonOut
		lcOrderDetails=""
		lcCartonOut=""
	ENDSCAN

ENDPROC
***************************************************************************************************
PROCEDURE ediupdate
****************************
	PARAMETER cStatus,lIsError
*	lDoCatch = .F.

	if used("edi_trigger")
		SELECT edi_trigger
		nRec = RECNO()
		IF !lIsError
			REPLACE processed WITH .T.,proc945 WITH .T., ;
				fin_status WITH "856 CREATED",errorflag WITH .F.,when_proc WITH DATETIME() ;
				FOR edi_trigger.wo_num = ManifestNum AND accountid = nAcctNum
			lDoCatch = .F.
		ELSE
			REPLACE processed WITH .T.,fin_status WITH cStatus,errorflag WITH .T. ;
				FOR edi_trigger.wo_num = ManifestNum AND accountid = nAcctNum
			num_decrement()
		ENDIF
	endif

ENDPROC
	