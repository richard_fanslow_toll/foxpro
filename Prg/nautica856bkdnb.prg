PARAMETERS lcFilename
 
WAIT WINDOW "Now in Nautica Breakdown module..." nowait
SET CENTURY ON
SELECT x856
LOCATE

STORE 0 TO lnTotalAdded
lcCurrentHAWB = ""

testing=.F.

nMANProcessed = 0
POs_added = 0
ShipmentNumber = 1
ThisShipment = 0
origawb = ""
lcCurrentShipID = ""
lcCurrentArrival = ""
LAFreight = .F.

SELECT DETAIL
SET FILTER TO

lcHLLevel = ""

*!*	SELECT * FROM DETAIL WHERE .F. INTO CURSOR xdetail READWRITE
*!*	SELECT xdetail
*!*	SCATTER MEMVAR BLANK

m.acct_name = "KIPLING"
m.acct_num  = 6356
m.office = "M"

SELECT x856
SET FILTER TO
LOCATE

WAIT WINDOW "Now importing file..........   "+lcFilename NOWAIT
DO WHILE !EOF("x856")
	cSegment = ALLTRIM(x856.segment)
	cField1 = ALLTRIM(x856.f1)
	IF INLIST(cSegment,"ISA","GS")
		SELECT x856
		SKIP 1 IN x856
		LOOP
	ENDIF
	IF INLIST(cSegment,"ST")
		SELECT x856
		SKIP 1 IN x856
		LOOP
	ENDIF


	IF TRIM(x856.segment) = "BSN"
		m.asnfile  = JUSTFNAME(lcFilename)
		m.dateloaded= DATE()
		m.shipID    = ALLTRIM(x856.f2)
		SELECT x856
		SKIP 1 IN x856
		LOOP
	ENDIF
	IF AT("HL",x856.segment)>0
		lcHLLevel = ALLTRIM(x856.f3)
		SELECT x856
		SKIP 1 IN x856
		LOOP
	ENDIF

	IF lcHLLevel = "S"
		DO WHILE lcHLLevel = "S"
			IF AT("HL",x856.segment)>0
				lcHLLevel = ALLTRIM(x856.f3)
				SELECT x856
				SKIP 1 IN x856
				LOOP
			ENDIF
			cSegment = TRIM(x856.segment)
			cF1 = TRIM(x856.f1)
			DO CASE
				CASE TRIM(x856.segment) = "REF" AND ALLTRIM(x856.f1) = "MB"
					m.bol = ALLTRIM(x856.f2)
				CASE TRIM(x856.segment) = "REF" AND ALLTRIM(x856.f1) = "ER"
					m.container = ALLTRIM(x856.f2)
				CASE TRIM(x856.segment) = "N1" AND ALLTRIM(x856.f1) = "FR"
					IF "KIPLING"$x856.f2
						m.accountid  = 6356
					ENDIF
					IF "NAUTICA"$x856.f2
						m.accountid = 687
					ENDIF
				OTHERWISE
					m.acct_name = "UNK"
					m.acct_num  = 9999
			ENDCASE
			SELECT x856
			SKIP 1 IN x856
		ENDDO
	ENDIF

	IF lcHLLevel = "O"
		m.timeloaded = DATETIME()
		SELECT x856
		DO WHILE INLIST(lcHLLevel,"O")
			IF AT("HL",x856.segment)>0
				lcHLLevel = ALLTRIM(x856.f3)
				SELECT x856
				SKIP 1 IN x856
				LOOP
			ENDIF
			cSegment = TRIM(x856.segment)
			cF1 = TRIM(x856.f1)
			DO CASE
				CASE TRIM(x856.segment) = "PRF"
					m.ponum  = ALLTRIM(x856.f1)
				CASE TRIM(x856.segment) = "REF" AND ALLTRIM(x856.f1) = "PO"
					m.reference = ALLTRIM(x856.f2)
				CASE TRIM(x856.segment) = "DTM" AND ALLTRIM(x856.f1)= "005"
					m.cancel = CTOD(SUBSTR(ALLTRIM(x856.f2),5,2)+"/"+SUBSTR(ALLTRIM(x856.f2),7,2)+"/"+SUBSTR(ALLTRIM(x856.f2),1,4))
				CASE TRIM(x856.segment) = "DTM" AND ALLTRIM(x856.f1)= "017"
					m.cancel = CTOD(SUBSTR(ALLTRIM(x856.f2),5,2)+"/"+SUBSTR(ALLTRIM(x856.f2),7,2)+"/"+SUBSTR(ALLTRIM(x856.f2),1,4))
			ENDCASE
			SELECT x856
			SKIP 1 IN x856
		ENDDO
		SELECT x856
	ENDIF

	IF lcHLLevel = "I"
		DO WHILE lcHLLevel = "I"
			IF AT("HL",x856.segment)>0
				lcHLLevel = ALLTRIM(x856.f3)
				SELECT x856
				SKIP 1 IN x856
				LOOP
			ENDIF
			cSegment = TRIM(x856.segment)
			cF1 = TRIM(x856.f1)
			DO CASE
				CASE TRIM(x856.segment) = "LIN"
					m.style = ALLTRIM(x856.f5)
					m.id  = ALLTRIM(x856.f7)
					m.color = ALLTRIM(x856.f9)
					m.upc   = ALLTRIM(x856.f3)
				CASE TRIM(x856.segment) = "SN1"
					m.totqty = VAL(ALLTRIM(x856.f2))
			ENDCASE
			SELECT x856
			SKIP 1 IN x856
		ENDDO
		SELECT x856
	ENDIF

	IF lcHLLevel = "P"
		DO WHILE lcHLLevel = "P"
			IF AT("HL",x856.segment)>0
				lcHLLevel = ALLTRIM(x856.f3)
				SELECT x856
				SKIP 1 IN x856
				LOOP
			ENDIF
			cSegment = TRIM(x856.segment)
			cF1 = TRIM(x856.f1)
			IF ALLTRIM(x856.segment) ="SE"
				SKIP 1 IN x856
				EXIT
			ENDIF
			DO CASE
			CASE TRIM(x856.segment) = "SN1"
				m.pack = ALLTRIM(x856.f2)
			CASE TRIM(x856.segment) = "MAN"
				m.ucc = ALLTRIM(x856.f2)
				llUseSql = .F.
				insertinto("ctnucc","wh",.T.)
			ENDCASE

			SELECT x856
			IF !EOF()
				SKIP 1 IN x856
			ELSE
				EXIT
			ENDIF
		ENDDO
	ENDIF
ENDDO
SET STEP ON 
***added TMARG 7/7/17
SELECT ctnucc
tu()
SET STEP ON 
IF lTesting
SELECT xdetail
LOCATE
*BROWSE
ENDIF

lProcessOK = .T.
RETURN
