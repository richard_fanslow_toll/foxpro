close data all
set exclusive off
set safety off

goffice="K"

xsqlexec("select ship_ref, batch_num As ordertype, consignee, ship_via, del_date, wavenum, " + ;
	"start, cancel, ctnqty, qty, pulleddt, picked, labeled, truckloaddt	from outship " + ;
	"where mod='K' and del_date={} and inlist(batch_num,'DTOC','RUSH')","temp",,"wh")

select temp
llok = .t.

try
	export to h:\fox\orders_report.xls type xls
catch
	llok = .f.
	wait window at 10,10 "Someone has the file open " timeout 2
endtry

if llok
	tattach = "h:\fox\orders_report.Xls"

	tsubject = "Ariat Rush/DtoC Report at "+ttoc(datetime())
	tsendto  = "Cheri.Foster@tollgroup.com"
	tcc = ""
	tmessage = "See attached spredsheet"
	tfrom = "Toll EDI Operations <toll-edi-ops@tollgroup.com>"

	do form m:\dev\frm\dartmail2 with tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
endif