*This checks for edi_trigger errors and mj_issues

SET EXCLUSIVE OFF 
SET DELETED ON 
SET TALK OFF  
SET SAFETY OFF   &&Allows for overwrite of existing files
utilsetup("MJ_ERRORS_EDITRIGGERANDISSUES")
If !Used("edi_trigger")
  use f:\3pl\data\edi_trigger.dbf IN 0
EndIf 
*SET STEP ON 
SELECT edi_trigger
SELECT *; 
from f:\3pl\data\edi_trigger.dbf ;
where  inlist(accountid,6303,6304,6305,6306,6320,6321,6322,6323,6324,6325,6364) AND errorflag;
into cursor temp1
SELECT temp1
If Reccount() > 0 
	EXPORT 	to  c:\edi_trigger_errors.xls TYPE XLS 
	Select temp1                                                                                         
		tsendto = "tmarg@fmiint.com"
		tattach = "c:\edi_trigger_errors.xls" 
		tcc =""                                                                                                                               
		tFrom ="Toll EDI System Operations <fmi-transload-ops@fmiint.com>"                                
		tmessage = "EDI_TRIGGER errors: "+Ttoc(Datetime())        
		tSubject = "EDI_TRIGGER errors"                                                           
		Do Form m:\dev\frm\dartmail2 With tsendto,tFrom,tSubject, tcc,tattach,tmessage  ,"A"                 
Else
	Wait window at 10,10 "No  data to report.........." timeout 2
		tsendto = "tmarg@fmiint.com"
		tattach = "" 
		tcc =""                                                                                                                               
		tFrom ="Toll EDI System Operations <fmi-transload-ops@fmiint.com>"                                
		tmessage = "No EDI_TRIGGER errors: "+Ttoc(Datetime())        
		tSubject = "No EDI_TRIGGER errors"                                                           
		Do Form m:\dev\frm\dartmail2 With tsendto,tFrom,tSubject, tcc,tattach,tmessage  ,"A"   
endif 


*Checks for records in MJ_ISSUES
If !Used("mj_issues")
  use f:\wh\mj_issues.dbf IN 0
ENDIF

Wait window at 10,10 "Performing the second query...." nowait


SELECT mj_issues 

SELECT * FROM  f:\wh\mj_issues.dbf;
where status="OPEN";
into cursor temp2
SELECT temp2
If Reccount() > 0 
	EXPORT 	to  c:\mj_issues.xls TYPE XLS 
	Select temp2                                                                                        
		tsendto = "tmarg@fmiint.com"
		tattach = "c:\mj_issues.xls" 
		tcc =""                                                                                                                               
		tFrom ="Toll EDI System Operations <fmi-transload-ops@fmiint.com>"                                
		tmessage = "MJ ISSUES: "+Ttoc(Datetime())        
		tSubject = "MJ ISSUES"                                                           
		Do Form m:\dev\frm\dartmail2 With tsendto,tFrom,tSubject, tcc,tattach,tmessage  ,"A"                 
Else
	Wait window at 10,10 "No  data to report.........." timeout 2
		tsendto = "tmarg@fmiint.com"
		tattach = "" 
		tcc =""                                                                                                                               
		tFrom ="Toll EDI System Operations <fmi-transload-ops@fmiint.com>"                                
		tmessage = "No MJ ISSUES: "+Ttoc(Datetime())        
		tSubject = "No MJ ISSUES"                                                           
		Do Form m:\dev\frm\dartmail2 With tsendto,tFrom,tSubject, tcc,tattach,tmessage  ,"A"   
endif 
close data all 

Wait window at 10,10 "all done...." timeout 2

schedupdate()
_screen.Caption=gscreencaption
on error
