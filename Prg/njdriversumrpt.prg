
* Hours summary for NJ drivers by paydate report
* For Mike DiVirgilio
* run against latest paydate each Thursday or Friday
* Build EXE AS F:\UTIL\NJDRIVERSUMRPT\NJDRIVERSUMRPT.EXE

runack("NJDRIVERSUMRPT")

utilsetup("NJDRIVERSUMRPT")

LOCAL loNJDRIVERSUMRPTForMike

loNJDRIVERSUMRPTForMike = CREATEOBJECT('NJDRIVERSUMRPTForMike')
loNJDRIVERSUMRPTForMike.MAIN()
*loNJDRIVERSUMRPTForMike.CreateHistory()

schedupdate()

CLOSE DATABASES ALL

RETURN

#DEFINE LOGIT 1
#DEFINE WAITIT 2
#DEFINE NOWAITIT 4
#DEFINE SENDIT 8
#DEFINE CR CHR(13)
#DEFINE LF CHR(10)
#DEFINE CRLF CHR(13) + CHR(10)
#DEFINE RETURN_DATA_MANDATORY .F.
#DEFINE RETURN_DATA_NOT_MANDATORY .T.
#DEFINE CSVFORMAT 6
#DEFINE WRITE_ONLY_UNBUFFERED 11
#DEFINE COMMA ","
#DEFINE xlEdgeBottom 9
#DEFINE xlContinuous 1
#DEFINE xlMedium -4138

DEFINE CLASS NJDRIVERSUMRPTForMike AS CUSTOM

	cProcessName = 'NJDRIVERSUMRPTForMike'

	lTestMode = .F.  && .F. FOR NORMAL OPERATION, .T. TO SUPPRESS ALL BUT DEVELOPER EMAILS AND CHANGE INPUT TABLE, ETC.

	lAutoYield = .F.

	* date time props
	dToday = DATE()
	*dToday = {^2013-10-18}

	cStartTime = TTOC(DATETIME())

	* file/folder properties

	* wait window properties
	nWaitWindowTimeout = 2
	lWaitWindowIsOn = .T.

	* logfile properties
	lLoggingIsOn = .T.
	cLogFile = 'F:\UTIL\NJDRIVERSUMRPT\LOGFILES\NJDRIVERSUMRPT_LOG.TXT'
	
	nSQLHandle = 0

	* INTERNAL email properties
	lSendInternalEmailIsOn = .T.
	cFrom = 'Mark.Bennett@Tollgroup.com'
	cSendTo = 'Michael.DiVirgilio@tollgroup.com'
	cCC = 'mbennett@fmiint.com'
	cSubject = 'NJ Driver Weekly Summary Rpt Results for ' + TTOC(DATETIME())
	cAttach = ''
	cBodyText = ''
	cTopBodyText = ''

	cCOMPUTERNAME = ''
	cUSERNAME = ''


	FUNCTION INIT
		IF NOT DODEFAULT()
			RETURN .F.
		ENDIF
		WITH THIS
			*SET RESOURCE OFF
			CLOSE DATA
			SET HOURS TO 24
			SET CENTURY ON
			SET DATE MDY
			SET DECIMAL TO 0
			SET TALK OFF
			SET DELETED ON
			SET CONSOLE OFF
			SET EXCLUSIVE OFF
			SET SAFETY OFF
			SET EXACT OFF
			SET STATUS BAR ON
			SET SYSMENU OFF
			SET ENGINEBEHAVIOR 70
			_VFP.AUTOYIELD = .lAutoYield
			.cCOMPUTERNAME = GETENV("COMPUTERNAME")
			.cUSERNAME = GETENV("USERNAME")
			IF .lTestMode THEN
				.cLogFile = 'F:\UTIL\NJDRIVERSUMRPT\LOGFILES\NJDRIVERSUMRPT_LOG_TESTMODE.TXT'
				.cSendTo = 'mbennett@fmiint.com'
				.cCC = ''
			ENDIF
			.lLoggingIsOn = .lLoggingIsOn AND NOT EMPTY(.cLogFile)
			IF .lLoggingIsOn THEN
				SET ALTERNATE TO (.cLogFile) ADDITIVE
				SET ALTERNATE ON
			ENDIF
		ENDWITH
	ENDFUNC


	FUNCTION DESTROY
		WITH THIS
			IF .lLoggingIsOn  THEN
				SET ALTERNATE OFF
				SET ALTERNATE TO
			ENDIF
		ENDWITH
		DODEFAULT()
	ENDFUNC
	

	FUNCTION MAIN
		WITH THIS

			TRY
				LOCAL lnNumberOfErrors, ldProcessDate, lcSQLADP, ldDate, ldToday
				LOCAL ldADPToDate, ldADPFromDate, lcADPFromDate, lcADPToDate, ldLatestDate
				LOCAL oExcel, oWorkbook, oWorksheet, lnRow, lcRow, lcReportSheet, luCellValue
				LOCAL lnBlankRow, lnPrevRow, lcBlankRow, lcPrevRow, lnNextRow, lcNextRow
				LOCAL lnPrevTotalsRow, lcPrevTotalsRow, lnCheckNextRow, lcCheckNextRow

				lnNumberOfErrors = 0

				.TrackProgress('NJ Driver Summary Rpt For Mike started....', LOGIT+SENDIT+NOWAITIT)
				.TrackProgress('COMPUTERNAME = ' + .cCOMPUTERNAME, LOGIT+SENDIT)
				.TrackProgress('USERNAME = ' + .cUSERNAME, LOGIT+SENDIT)
				.TrackProgress('PROJECT = NJDRIVERSUMRPT', LOGIT+SENDIT)
				IF .lTestMode THEN
					.TrackProgress('TEST MODE', LOGIT+SENDIT)
				ENDIF

				ldToday = .dToday
				ldDate = ldToday
				DO WHILE DOW(ldDate,1) <> 7
					ldDate = ldDate - 1
				ENDDO
				ldADPToDate = ldDate + 6
				ldADPFromDate = ldDate  && make this the prior saturday to handle early pay dates

				lcADPFromDate = "DATE'" + TRANSFORM(YEAR(ldADPFromDate)) + "-" + PADL(MONTH(ldADPFromDate),2,'0') + "-" + PADL(DAY(ldADPFromDate),2,'0') + "'"
				lcADPToDate = "DATE'" + TRANSFORM(YEAR(ldADPToDate)) + "-" + PADL(MONTH(ldADPToDate),2,'0') + "-" + PADL(DAY(ldADPToDate),2,'0') + "'"

				.nSQLHandle = SQLCONNECT("PCPAYWIN","MBENNETT","VP737A")

				IF .nSQLHandle > 0 THEN

					IF USED('CURWTKHOURS') THEN
						USE IN CURWTKHOURS
					ENDIF

					SET TEXTMERGE ON
					TEXT TO	lcSQLADP NOSHOW
SELECT
A.FILE#,
A.CHECKVIEWPAYDATE AS PAYDATE,
SUM({FN IFNULL(A.CHECKVIEWREGERNING,0.00)}) AS REGPAY,
SUM({FN IFNULL(A.CHECKVIEWOTEARNING,0.00)}) AS OTPAY,
(SELECT {FN IFNULL(SUM(CHECKVIEWEARNSAMT),0.00)} FROM REPORTS.V_CHK_VW_EARNINGS
WHERE FILE#=A.FILE# AND CHECKVIEWPAYDATE = A.CHECKVIEWPAYDATE AND CHECKVIEWEARNSCD  = 'V') AS VACPAY,
(SELECT {FN IFNULL(SUM(CHECKVIEWEARNSAMT),0.00)} FROM REPORTS.V_CHK_VW_EARNINGS
WHERE FILE#=A.FILE# AND CHECKVIEWPAYDATE = A.CHECKVIEWPAYDATE AND CHECKVIEWEARNSCD  = 'S') AS SICKPAY,
(SELECT {FN IFNULL(SUM(CHECKVIEWEARNSAMT),0.00)} FROM REPORTS.V_CHK_VW_EARNINGS
WHERE FILE#=A.FILE# AND CHECKVIEWPAYDATE = A.CHECKVIEWPAYDATE AND CHECKVIEWEARNSCD  = 'O') AS OTHERPAY,
(SELECT {FN IFNULL(SUM(CHECKVIEWEARNSAMT),0.00)} FROM REPORTS.V_CHK_VW_EARNINGS
WHERE FILE#=A.FILE# AND CHECKVIEWPAYDATE = A.CHECKVIEWPAYDATE AND CHECKVIEWEARNSCD  = 'P') AS PERSPAY,
(SELECT {FN IFNULL(SUM(CHECKVIEWEARNSAMT),0.00)} FROM REPORTS.V_CHK_VW_EARNINGS
WHERE FILE#=A.FILE# AND CHECKVIEWPAYDATE = A.CHECKVIEWPAYDATE AND CHECKVIEWEARNSCD  = '1') AS OPTOUTPAY,
(SELECT {FN IFNULL(SUM(CHECKVIEWEARNSAMT),0.00)} FROM REPORTS.V_CHK_VW_EARNINGS
WHERE FILE#=A.FILE# AND CHECKVIEWPAYDATE = A.CHECKVIEWPAYDATE AND CHECKVIEWEARNSCD  = 'R') AS RETROPAY,
(SELECT {FN IFNULL(SUM(CHECKVIEWEARNSAMT),0.00)} FROM REPORTS.V_CHK_VW_EARNINGS
WHERE FILE#=A.FILE# AND CHECKVIEWPAYDATE = A.CHECKVIEWPAYDATE AND CHECKVIEWEARNSCD  = 'H') AS HOLPAY,
SUM({FN IFNULL(A.CHECKVIEWGROSSPAYA,0.00)}) AS GROSSPAY,
SUM({FN IFNULL(A.CHECKVIEWREGHOURS,0.00)}) AS REGHOURS,
SUM({FN IFNULL(A.CHECKVIEWOTHOURS,0.00)}) AS OTHOURS,
(SELECT {FN IFNULL(SUM(CHECKVIEWHOURSAMT),0.00)} FROM REPORTS.V_CHK_VW_HOURS
WHERE FILE#=A.FILE# AND CHECKVIEWPAYDATE = A.CHECKVIEWPAYDATE AND CHECKVIEWHOURSCODE = 'V') AS VACHOURS,
(SELECT {FN IFNULL(SUM(CHECKVIEWHOURSAMT),0.00)} FROM REPORTS.V_CHK_VW_HOURS
WHERE FILE#=A.FILE# AND CHECKVIEWPAYDATE = A.CHECKVIEWPAYDATE AND CHECKVIEWHOURSCODE = 'S') AS SICKHOURS,
(SELECT {FN IFNULL(SUM(CHECKVIEWHOURSAMT),0.00)} FROM REPORTS.V_CHK_VW_HOURS
WHERE FILE#=A.FILE# AND CHECKVIEWPAYDATE = A.CHECKVIEWPAYDATE AND CHECKVIEWHOURSCODE = 'P') AS PERSHOURS
FROM REPORTS.V_CHK_VW_INFO A
WHERE A.CHECKVIEWHOMEDEPT = '030650'
AND A.CHECKVIEWPAYDATE >= <<lcADPFromDate>>
AND A.CHECKVIEWPAYDATE <= <<lcADPToDate>>
GROUP BY A.FILE#, A.CHECKVIEWPAYDATE
ORDER BY A.FILE#, A.CHECKVIEWPAYDATE
					ENDTEXT
					SET TEXTMERGE OFF

					.TrackProgress('lcSQLADP =' + lcSQLADP, LOGIT+SENDIT)

					IF .ExecSQL(lcSQLADP, 'CURWTKHOURS', RETURN_DATA_NOT_MANDATORY) THEN
					
						IF USED('CURWTKHOURS') AND NOT EOF('CURWTKHOURS') THEN

							* roll up -- FILE #s not needed
							SELECT ldADPToDate AS PAYDATE, ;
								SUM(REGHOURS) AS REGHOURS, ;
								SUM(OTHOURS) AS OTHOURS, ;
								SUM(VACHOURS) AS VACHOURS, ;
								SUM(SICKHOURS) AS SICKHOURS, ;
								SUM(PERSHOURS) AS PERSHOURS, ;
								SUM(REGPAY) AS REGPAY, ;
								SUM(OTPAY) AS OTPAY, ;
								SUM(SICKPAY) AS SICKPAY, ;
								SUM(OTHERPAY) AS OTHERPAY, ;
								SUM(PERSPAY) AS PERSPAY, ;
								SUM(VACPAY) AS VACPAY, ;
								SUM(OPTOUTPAY) AS OPTOUTPAY, ;
								SUM(RETROPAY) AS RETROPAY, ;
								SUM(HOLPAY) AS HOLPAY, ;
								SUM(GROSSPAY) AS GROSSPAY ;
								FROM CURWTKHOURS ;
								INTO CURSOR CURSUMMARY ;
								GROUP BY 1 ;
								ORDER BY 1 ;
								READWRITE

*!*			SELECT CURSUMMARY 
*!*			BROWSE


								oExcel = CREATEOBJECT("excel.application")
								oExcel.displayalerts = .F.
								oExcel.VISIBLE = .F.
								lcReportSheet = 'F:\UTIL\NJDRIVERSUMRPT\REPORTS\NJDRIVERHOURS.XLS'
								oWorkbook = oExcel.workbooks.OPEN(lcReportSheet)
								
								*oWorkbook.SAVEAS(lcOutputFileSummary)

								oWorksheet = oWorkbook.Worksheets[1]
								
								* find blank row to place new data in
								lnPrevTotalsRow = 2  && Default value if there are no totals rows...
								lcPrevTotalsRow = ALLTRIM(STR(lnPrevTotalsRow))
								
								lnBlankRow = -1
								FOR lnRow = 2 TO 1000
									lcRow = ALLTRIM(STR(lnRow))
									luCellValue = oWorksheet.RANGE("A"+lcRow).VALUE
									IF ISNULL(luCellValue) OR EMPTY(luCellValue) THEN
										* FOUND 1 BLANK ROW;										
										* check next row; if it is also blank, then we saw 2 blank consecutive rows and can exit!
*!*						SET STEP ON 
										lnCheckNextRow = lnRow + 1
										lcCheckNextRow = ALLTRIM(STR(lnCheckNextRow))																		
										luCellValue = oWorksheet.RANGE("A"+lcCheckNextRow).VALUE
										IF ISNULL(luCellValue) OR EMPTY(luCellValue) THEN
											lnBlankRow = lnRow									
											EXIT FOR
										ENDIF
									ELSE
										* if it's a Totals Row, save the row for later use in SUM formula
										IF (TYPE('luCellValue') = 'C') AND (UPPER(ALLTRIM(luCellValue)) = 'TOTALS:') THEN
											lnPrevTotalsRow = lnRow + 2
											lcPrevTotalsRow = ALLTRIM(STR(lnPrevTotalsRow))
										ENDIF	
										* if it's a Date, remember it because if it's > or = to the current pay date, we can stop...
										IF (TYPE('luCellValue') = 'T') OR (TYPE('luCellValue') = 'D') THEN
											ldLatestDate = luCellValue
											IF ldLatestDate >= ldADPToDate THEN
												EXIT FOR
											ENDIF
										ENDIF																
									ENDIF								
								ENDFOR
								
								IF lnBlankRow > 0 THEN
									* We need to check the value in Col A in the row above, to determine what to do.
									* If that value = 'totals:' then we are beginning a new month, and want to skip a row before adding a new row.
									* If that value = a date
									*	if date is same as current, do nothing.
									*	if date is < current, add current in current row. 
									*
									* then check current date -- if it is last date in current month, add 'totals:' line
									
									* get prev row cell value
									lnPrevRow = lnBlankRow - 1
									lcPrevRow = ALLTRIM(STR(lnPrevRow))
									luCellValue = oWorksheet.RANGE("A"+lcPrevRow).VALUE
																		
									DO CASE
										CASE (TYPE('luCellValue') = 'C') AND (UPPER(ALLTRIM(luCellValue)) = 'TOTALS:')
											* Prev row contained month-end totals, so
											* we are beginning a new month, and want to skip a row before adding a new row
											lnNextRow = lnBlankRow + 1
											lcNextRow = ALLTRIM(STR(lnNextRow))
											oWorksheet.RANGE("A"+lcNextRow).VALUE = ldADPToDate
											oWorksheet.RANGE("B"+lcNextRow).VALUE = CURSUMMARY.REGHOURS
											oWorksheet.RANGE("C"+lcNextRow).VALUE = CURSUMMARY.OTHOURS
											oWorksheet.RANGE("D"+lcNextRow).VALUE = CURSUMMARY.VACHOURS
											oWorksheet.RANGE("E"+lcNextRow).VALUE = CURSUMMARY.SICKHOURS
											oWorksheet.RANGE("F"+lcNextRow).VALUE = CURSUMMARY.PERSHOURS
											
											oWorksheet.RANGE("G"+lcNextRow).FORMULA = "=SUM(B" + lcNextRow + "..C" + lcNextRow + ")"
											
											oWorksheet.RANGE("H"+lcNextRow).VALUE = CURSUMMARY.REGPAY
											oWorksheet.RANGE("I"+lcNextRow).VALUE = CURSUMMARY.OTPAY
											oWorksheet.RANGE("J"+lcNextRow).VALUE = CURSUMMARY.VACPAY
											oWorksheet.RANGE("K"+lcNextRow).VALUE = CURSUMMARY.SICKPAY
											oWorksheet.RANGE("L"+lcNextRow).VALUE = CURSUMMARY.PERSPAY
											
											oWorksheet.RANGE("M"+lcNextRow).VALUE = CURSUMMARY.OTHERPAY
											
											oWorksheet.RANGE("N"+lcNextRow).VALUE = CURSUMMARY.OPTOUTPAY
											oWorksheet.RANGE("O"+lcNextRow).VALUE = CURSUMMARY.HOLPAY
											oWorksheet.RANGE("P"+lcNextRow).VALUE = CURSUMMARY.RETROPAY
											
											oWorksheet.RANGE("Q"+lcNextRow).VALUE = CURSUMMARY.GROSSPAY
											
										CASE (TYPE('luCellValue') = 'T') OR (TYPE('luCellValue') = 'D')
											DO CASE
												CASE luCellValue < ldADPToDate
													* we are adding a date later than date in prev row,
													* add date in current row
													lnNextRow = lnBlankRow
													lcNextRow = ALLTRIM(STR(lnNextRow))
													oWorksheet.RANGE("A"+lcNextRow).VALUE = ldADPToDate
													oWorksheet.RANGE("B"+lcNextRow).VALUE = CURSUMMARY.REGHOURS
													oWorksheet.RANGE("C"+lcNextRow).VALUE = CURSUMMARY.OTHOURS
													oWorksheet.RANGE("D"+lcNextRow).VALUE = CURSUMMARY.VACHOURS
													oWorksheet.RANGE("E"+lcNextRow).VALUE = CURSUMMARY.SICKHOURS
													oWorksheet.RANGE("F"+lcNextRow).VALUE = CURSUMMARY.PERSHOURS
													
													oWorksheet.RANGE("G"+lcNextRow).FORMULA = "=SUM(B" + lcNextRow + "..C" + lcNextRow + ")"
													
													oWorksheet.RANGE("H"+lcNextRow).VALUE = CURSUMMARY.REGPAY
													oWorksheet.RANGE("I"+lcNextRow).VALUE = CURSUMMARY.OTPAY
													oWorksheet.RANGE("J"+lcNextRow).VALUE = CURSUMMARY.VACPAY
													oWorksheet.RANGE("K"+lcNextRow).VALUE = CURSUMMARY.SICKPAY
													oWorksheet.RANGE("L"+lcNextRow).VALUE = CURSUMMARY.PERSPAY
													
													oWorksheet.RANGE("M"+lcNextRow).VALUE = CURSUMMARY.OTHERPAY
													
													oWorksheet.RANGE("N"+lcNextRow).VALUE = CURSUMMARY.OPTOUTPAY
													oWorksheet.RANGE("O"+lcNextRow).VALUE = CURSUMMARY.HOLPAY
													oWorksheet.RANGE("P"+lcNextRow).VALUE = CURSUMMARY.RETROPAY
													
													oWorksheet.RANGE("Q"+lcNextRow).VALUE = CURSUMMARY.GROSSPAY
													
													* if the date we added was last in month, add a totals row
													LOCAL lnCurMonth, lnTestMonth, lnTotalsRow, lcTotalsRow
													lnCurMonth = MONTH(ldADPToDate)
													lnTestMonth = MONTH(ldADPToDate + 7)
													IF lnCurMonth <> lnTestMonth THEN
														* add a totals line
														lnTotalsRow = lnBlankRow + 1
														lcTotalsRow = ALLTRIM(STR(lnTotalsRow))
														oWorksheet.RANGE("A"+lcTotalsRow).VALUE = 'totals:'
														oWorksheet.RANGE("B"+lcTotalsRow).formula = "=sum(B"+lcPrevTotalsRow+":B"+lcNextRow+")"
														oWorksheet.RANGE("C"+lcTotalsRow).formula = "=sum(C"+lcPrevTotalsRow+":C"+lcNextRow+")"
														oWorksheet.RANGE("D"+lcTotalsRow).formula = "=sum(D"+lcPrevTotalsRow+":D"+lcNextRow+")"
														oWorksheet.RANGE("E"+lcTotalsRow).formula = "=sum(E"+lcPrevTotalsRow+":E"+lcNextRow+")"
														oWorksheet.RANGE("F"+lcTotalsRow).formula = "=sum(F"+lcPrevTotalsRow+":F"+lcNextRow+")"
														oWorksheet.RANGE("G"+lcTotalsRow).formula = "=sum(G"+lcPrevTotalsRow+":G"+lcNextRow+")"
														oWorksheet.RANGE("H"+lcTotalsRow).formula = "=sum(H"+lcPrevTotalsRow+":H"+lcNextRow+")"
														oWorksheet.RANGE("I"+lcTotalsRow).formula = "=sum(I"+lcPrevTotalsRow+":I"+lcNextRow+")"
														oWorksheet.RANGE("J"+lcTotalsRow).formula = "=sum(J"+lcPrevTotalsRow+":J"+lcNextRow+")"
														oWorksheet.RANGE("K"+lcTotalsRow).formula = "=sum(K"+lcPrevTotalsRow+":K"+lcNextRow+")"
														
														oWorksheet.RANGE("L"+lcTotalsRow).formula = "=sum(L"+lcPrevTotalsRow+":L"+lcNextRow+")"
														
														oWorksheet.RANGE("M"+lcTotalsRow).formula = "=sum(M"+lcPrevTotalsRow+":M"+lcNextRow+")"
														oWorksheet.RANGE("N"+lcTotalsRow).formula = "=sum(N"+lcPrevTotalsRow+":N"+lcNextRow+")"
														oWorksheet.RANGE("O"+lcTotalsRow).formula = "=sum(O"+lcPrevTotalsRow+":O"+lcNextRow+")"
														
														oWorksheet.RANGE("P"+lcTotalsRow).formula = "=sum(P"+lcPrevTotalsRow+":P"+lcNextRow+")"
														oWorksheet.RANGE("Q"+lcTotalsRow).formula = "=sum(Q"+lcPrevTotalsRow+":Q"+lcNextRow+")"
														
														* Bold totals line...
														oWorksheet.RANGE("A" + lcTotalsRow,"Q" + lcTotalsRow).FONT.bold = .T.

														* UNDERLINE cell columns to be totaled...
														oWorksheet.RANGE("B" + lcNextRow + ":Q" + lcNextRow).BORDERS(xlEdgeBottom).LineStyle = xlContinuous
														oWorksheet.RANGE("B" + lcNextRow + ":Q" + lcNextRow).BORDERS(xlEdgeBottom).Weight = xlMedium
														
													ENDIF
													
												OTHERWISE
													* nothing
											ENDCASE 
										OTHERWISE
											* nothing									
									ENDCASE
									
								
								ELSE								
									.TrackProgress('ERROR: lnBlankRow =' + TRANSFORM(lnBlankRow), LOGIT+SENDIT)								
								ENDIF
								

								* SAVE AND QUIT EXCEL
								oWorkbook.SAVE()
								oExcel.QUIT()
								**************************************************************

								IF FILE(lcReportSheet) THEN
									* attach output file to email
									.cAttach = lcReportSheet
									.cBodyText = "See attached report. NOTE: #s are subject to change, pending final time card reviews." + ;
										CRLF + CRLF + "(do not reply - this is an automated report)" + ;
										CRLF + CRLF + "<report log follows>" + ;
										CRLF + .cBodyText
								ELSE
									.TrackProgress('ERROR attaching Summary spreadsheet: ' + lcReportSheet, LOGIT+SENDIT)
								ENDIF

								
						ELSE

							.TrackProgress('!!No data found!!', LOGIT+SENDIT)
							
						ENDIF

					ENDIF  &&  .ExecSQL

					SQLDISCONNECT(.nSQLHandle)

					*CLOSE DATABASES ALL
				ELSE
					* connection error
					.TrackProgress('Unable to connect to WTK System.', LOGIT+SENDIT)
				ENDIF   &&  .nSQLHandle > 0


				.TrackProgress('NJ Driver Summary Rpt For Mike ended normally!',LOGIT+SENDIT)

			CATCH TO loError

				.TrackProgress('There was an error.',LOGIT+SENDIT)
				.TrackProgress('The Error # is: ' + TRANSFORM(loError.ERRORNO), LOGIT+SENDIT)
				.TrackProgress('The Error Message is: ' + TRANSFORM(loError.MESSAGE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred in Program: ' + TRANSFORM(loError.PROCEDURE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred at line #: ' + TRANSFORM(loError.LINENO), LOGIT+SENDIT)
				lnNumberOfErrors = lnNumberOfErrors + 1
				CLOSE DATA
				IF (NOT ISNULL(oExcel)) AND (TYPE('oExcel') = 'O') THEN
					oExcel.QUIT()
				ENDIF

			ENDTRY

			CLOSE DATABASES ALL

			WAIT CLEAR
			***************** INTERNAL email results ******************************
			.TrackProgress('About to send status email.',LOGIT)
			.TrackProgress('The logfile is: ' + .cLogFile, SENDIT)
			.TrackProgress('NJ Driver Summary Rpt For Mike started: ' + .cStartTime, LOGIT+SENDIT)
			.TrackProgress('NJ Driver Summary Rpt For Mike finished: ' + TTOC(DATETIME()), LOGIT+SENDIT)

			IF .lSendInternalEmailIsOn THEN
				DO FORM dartmail2 WITH .cSendTo,.cFrom,.cSubject,.cCC,.cAttach,.cBodyText,"A"
				.TrackProgress('Sent status email.',LOGIT)
			ELSE
				.TrackProgress('Did not send status email: THIS.lSendInternalEmailIsOn = FALSE.',LOGIT)
			ENDIF

		ENDWITH
		RETURN
	ENDFUNC && main
	
	
	PROCEDURE SetDate
		LPARAMETERS tdDate
		.dToday = tdDate
		RETURN
	ENDPROC
	
	
	PROCEDURE CreateHistory
		WITH THIS
			LOCAL ldDate
			ldDate = {^2012-12-27}
			.lSendInternalEmailIsOn = .F.
			DO WHILE ldDate <= {^2013-06-27}
				.SetDate( ldDate )
				.Main()
				.cBodyText = ''
				ldDate = ldDate + 7
			ENDDO	
		ENDWITH
	ENDPROC
	

	FUNCTION ExecSQL
		LPARAMETERS tcSQL, tcCursorName, tlNoDataReturnedIsOkay
		LOCAL llRetval, lnResult
		WITH THIS
			* close target cursor if it's open
			IF USED(tcCursorName)
				USE IN (tcCursorName)
			ENDIF
			WAIT WINDOW "Fetching remote data; please wait..." NOWAIT
			lnResult = SQLEXEC(.nSQLHandle, tcSQL, tcCursorName)
			llRetval = ( lnResult > 0 )
			IF llRetval THEN
				* see if any data came back
				IF NOT tlNoDataReturnedIsOkay THEN
					IF (NOT USED(tcCursorName)) OR EOF(tcCursorName) THEN
						llRetval = .F.
						.TrackProgress('No data was returned by this query: ' +CRLF + tcSQL, LOGIT+SENDIT)
					ENDIF
				ENDIF
			ELSE
				.TrackProgress("Result Code [" + TRANSFORM(lnResult) + "] returned by this query: " + tcSQL, LOGIT+SENDIT)
			ENDIF
			WAIT CLEAR
			RETURN llRetval
		ENDWITH
	ENDFUNC


	PROCEDURE TrackProgress
		* do any combination of Wait Window, writing to logfile, and adding to body of email,
		* based on nFlags parameter.
		LPARAMETERS tcExpression, tnFlags
		WITH THIS
			IF BITAND(tnFlags,LOGIT) = LOGIT THEN
				IF .lLoggingIsOn THEN
					?
					? .cProcessName + "  " + TTOC(DATETIME()) + ": " + tcExpression
				ENDIF
			ENDIF
			IF .lWaitWindowIsOn THEN
				IF BITAND(tnFlags,NOWAITIT) = NOWAITIT THEN
					WAIT WINDOW tcExpression NOWAIT
				ENDIF
				IF BITAND(tnFlags,WAITIT) = WAITIT THEN
					WAIT WINDOW tcExpression TIMEOUT .nWaitWindowTimeout
				ENDIF
			ENDIF
			IF BITAND(tnFlags,SENDIT) = SENDIT THEN
				IF .lSendInternalEmailIsOn THEN
					.cBodyText = .cBodyText + tcExpression + CRLF + CRLF
				ENDIF
			ENDIF
		ENDWITH
	ENDPROC  &&  TrackProgress



ENDDEFINE
