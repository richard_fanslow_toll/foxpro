lcoffice = "C"
xaccountid = "6561"
goffice="2"
xtest=.f.

* !!!! the exe goes in F:\3PL\

utilsetup("MERKURY_INVENTORY_SYNC")

xscreencaption=_screen.caption
_screen.caption="MERKURY Inventory Sync Files for Account: "+xaccountid+" and Office: "+lcoffice

guserid="MERKURY_WIP_RPT"
gprocess=guserid
gdevelopment=.f.

xaccountid = val(xaccountid)
lcwhpath = wf(lcoffice,xaccountid)

xsqlexec("select outship.wo_num, ship_ref, picked, style, color, id, totqty " + ;
	"from outship, outdet where outship.outshipid=outdet.outshipid " + ;
	"and outship.mod='"+goffice+"' and outship.accountid=6561 and units=1 " + ;
	"and del_date={} and notonwip=0 and totqty>0","bugaboostats",,"wh")

select bugaboostats
wipfilename= "f:\ftpusers\MERKURY\846out\wip_detail_"+strtran(dtoc(date()),"/")+".xls"
export to &wipfilename type xl5  &&f:\ftpusers\BUGABOO\846out\wip_detail.xls type xl5

* get inventory

xsqlexec("select * from inven where accountid=6561 and units=1",,,"wh")	&& fixed dy 3/11/18

select units, style, color, id, pack, totqty, availqty, allocqty, holdqty, 0000000 as pickedqty, 0000000 as spqty,000000 as total_units ;
	from inven into cursor inventory readwrite

select inventory
index on transform(units)+style+color+id+pack tag match
set order to

* inprocess is a cursor of all outship records where they are not yet delivered and also excluding "notonwip"

xsqlexec("select * from outship where mod='"+goffice+"' and accountid=6561 " + ;
	"and del_date={} and notonwip=0 and qty#0 and pulled=1 order by wo_num","inprocess",,"wh")

xsqlexec("select units, style, color, id, pack, totqty from outship, outdet " + ;
	"where outship.outshipid=outdet.outshipid " + ;
	"and outship.mod='"+goffice+"' and outship.accountid=6561 " + ;
	"and del_date={} and notonwip=0 and qty#0 and pulled=1","xdy",,"wh") 

select units, style, color, id, pack, sum(totqty) as pickedqty ;
	from xdy group by units, style, color, id, pack into cursor xunshipped

select inventory
scan
	replace total_units with totqty in inventory
endscan

select xunshipped
scan
	=seek(transform(units)+style+color+id+pack,"inventory","match")
	replace pickedqty with pickedqty+xunshipped.pickedqty in inventory
	replace total_units with totqty + xunshipped.pickedqty in inventory
endscan

* copy and email inventory file
set step on
select inventory
delete for availqty=0 and pickedqty=0 and holdqty=0 and spqty=0 and total_units = 0

xsqlexec("select style, color, id, pack, locqty from invenloc where mod='"+goffice+"' " + ;
	"and accountid=6561 and holdtype='DA' and locqty#0","xdy",,"wh")
	
select style, color, id, round(val(pack),0) as pack, locqty from xdy into cursor t1 readwrite

select *, pack*locqty as damqty from t1 into cursor t2 readwrite
select style,color,id,sum(damqty) as damqty from t2 group by style,color,id into cursor t2a readwrite
select a.*, damqty  from inventory a left join t2a b on a.style=b.style and a.color=b.color and a.id=b.id into cursor t3 readwrite
replace damqty with 0 for isnull(damqty)
replace totqty with totqty-damqty, holdqty with holdqty-damqty,total_units with total_units-damqty  for damqty !=0
select units, style,color,id,pack,totqty,availqty,allocqty,holdqty,pickedqty,spqty,total_units from t3 into cursor inventory readwrite
delete from inventory where color='DAM'
select inventory
xlsfilename= "f:\ftpusers\MERKURY\846out\MERKURY_inventory_"+strtran(dtoc(date()),"/")+".xls"

&&copy to &xlsfilename fields style,total_units ,totqty,availqty,allocqty,holdqty,pickedqty xls

copy to &xlsfilename fields style,total_units xls

cfilenamearchive = "f:\ftpusers\Merkury\846out\archive\"+justfname(xlsfilename)

copy file &xlsfilename to &cfilenamearchive
*SET STEP ON
tmessage = "Updated inventory file in the merkury FTP Site.............."+chr(13)+chr(13)+;
"This email is generated via an automated process"+chr(13)+"For changes in content or distribution please contact: Paul Gaidis @ 732-750-9000x143 "
tsubject= "Merkury Inventory Sent: " +ttoc(datetime())

tattach = xlsfilename+","+wipfilename
tfrom ="FMI Corporate <fmicorporate@fmiint.com>"

tsendto = 'viren@merkuryinnovations.com,pgaidis@fmiint.com,todd.margolin@tollgroup.com'
*tsendto = 'pgaidis@fmiint.com'

do form dartmail2 with tsendto,tfrom,tsubject,"",tattach,tmessage,"A"
set step on
schedupdate()

on error
_screen.caption=xscreencaption