************************************************************************************************************
* Revised Outlook Interface to eliminate Foxpro erros upon exit
* pg - 2/27/2001
************************************************************************************************************
LPARAMETER lc_address,psubject,pattachment,pmessage

PUBLIC oOutlook
#DEFINE olMailItem 0

IF FILE("C:\PMAIL\WINPM-32.EXE")
  DO PMAIL WITH lc_address,psubject,pattachment,pmessage
  RUN C:\PMAIL\WINPM-32 -J h:\fox\PMAIL.PMO
  RETURN
ENDIF

oOutlook = CREATEOBJECT("Outlook.Application")
oMailItem = oOutlook.CreateItem(olMailItem)

WITH oMailItem
  .Subject = psubject
  .Body = pmessage
ENDWITH

IF !EMPTY(lc_address)
*break up multiple Email address's and do an ADD for each
  IF AT(';',lc_address)>0
    DO WHILE !EMPTY(lc_address)
      IF AT(';',lc_address)>0
        STORE SUBSTR(lc_address,1,AT(';',lc_address)-1) TO nextaddr
        STORE LEN(lc_address) TO tlen
        STORE tlen-AT(';',lc_address)+1 TO tbal
        STORE SUBSTR(lc_address,AT(';',lc_address)+1,tbal) TO lc_address
      ELSE
        STORE lc_address TO nextaddr
        STORE "" TO lc_address
      ENDIF
      oMailItem.Recipients.Add(nextaddr)
    ENDDO
  ELSE
    oMailItem.Recipients.Add(lc_address)
  ENDIF
ENDIF

IF !EMPTY(pattachment)
*break up multiple attachments and do an ADD for each
  IF AT(',',pattachment)>0
    DO WHILE !EMPTY(pattachment)
      IF AT(',',pattachment)>0
        STORE alltrim(SUBSTR(pattachment,1,AT(',',pattachment)-1)) TO nextfile
        STORE LEN(pattachment) TO tlen
        STORE tlen-AT(',',pattachment)+1 TO tbal
        STORE SUBSTR(pattachment,AT(',',pattachment)+1,tbal) TO pattachment
      ELSE
        STORE alltrim(pattachment) TO nextfile
        STORE "" TO pattachment
      ENDIF
      IF !FILE(nextfile)
        =MESSAGEBOX("You are trying to attach the file..."+CHR(13)+nextfile+CHR(13)+" and it does not exist",48,"FoxPro Emailer")
      ELSE
        oMailItem.Attachments.Add(nextfile)
      ENDIF
    ENDDO
  ELSE
    IF !FILE(pattachment)
      =MESSAGEBOX("You are trying to attach the file..."+CHR(13)+pattachment+CHR(13)+" and it does not exist",48,"FoxPro Emailer")
    ELSE
      oMailItem.Attachments.Add(pattachment)
    ENDIF
  ENDIF
ENDIF

oMailItem.Save()
oMailItem.Send()
RETURN
************************************************************************************************************
PROCEDURE PMAIL
  PARAMETERS lc_address,psubject,pattachment,pmessage
  LOCAL lcString
 
 IF FILE('h:\fox\PMAIL.PMO')
   DELETE FILE h:\fox\PMAIL.PMO
 ENDIF
  
 tFhandle = FCREATE('h:\fox\PMAIL.PMO')
 IF tFhandle <0
   =MESSAGEBOX("CANT CREATE <H:\FOX\PMAIL.PMO >",48,"Emailer Applicaiton")
   RETURN
 ENDIF   
 
 lcString = "PEGASUS MAIL HEADER FILE INFO"
 FPUTS(tFhandle,lcString)

 lcString = "!M:70"
 FPUTS(tFhandle,lcString)
 
 lcString = "!T:4"
 FPUTS(tFhandle,lcString)
 
 lcString = "SY:0"
 FPUTS(tFhandle,lcString)

 lc_Address = CHRTRANC(lc_Address,";",",")
 lcString = "TO: "+lc_Address
 FPUTS(tFhandle,lcString)
 
 lcString = "SU: "+psubject
 FPUTS(tFhandle,lcString)

 lcString = "EN:0"
 FPUTS(tFhandle,lcString)

 lcString = "CS:1"
 FPUTS(tFhandle,lcString)

 lcString = "RC:0"
 FPUTS(tFhandle,lcString)

 lcString = "DC:0"
 FPUTS(tFhandle,lcString)

 lcString = "UR:0"
 FPUTS(tFhandle,lcString)

 lcString = "SS:0"
 FPUTS(tFhandle,lcString)

 lcString = "SG:0"
 FPUTS(tFhandle,lcString)

 lcString = "MI:1"
 FPUTS(tFhandle,lcString)

 lcString = "ID:<Default>"
 FPUTS(tFhandle,lcString)

 lcString = "EX:0"
 FPUTS(tFhandle,lcString)

IF !EMPTY(pattachment)
*break up multiple attachments and do an ADD for each
  IF AT(',',pattachment)>0
    DO WHILE !EMPTY(pattachment)
      IF AT(',',pattachment)>0
        STORE SUBSTR(pattachment,1,AT(',',pattachment)-1) TO nextfile
        STORE LEN(pattachment) TO tlen
        STORE tlen-AT(',',pattachment)+1 TO tbal
        STORE SUBSTR(pattachment,AT(',',pattachment)+1,tbal) TO pattachment
      ELSE
        STORE pattachment TO nextfile
        STORE "" TO pattachment
      ENDIF
      IF !FILE(nextfile)
        =MESSAGEBOX("You are trying to attach the file..."+CHR(13)+nextfile+CHR(13)+" and it does not exist",48,"FoxPro Emailer")
      ELSE
         lcString = "AT:"+nextfile+",Unknown,0"
		 FPUTS(tFhandle,lcString)
      ENDIF
    ENDDO
  ELSE && only a single attachement
    IF !FILE(pattachment)
      =MESSAGEBOX("You are trying to attach the file..."+CHR(13)+pattachment+CHR(13)+" and it does not exist",48,"FoxPro Emailer")
    ELSE
      lcString = "AT:"+pattachment+",Unknown,0"
 	  FPUTS(tFhandle,lcString)
    ENDIF
  ENDIF
ENDIF

 lcString = "TE: 1"
 FPUTS(tFhandle,lcString)

 lcString = "FL:0"
 FPUTS(tFhandle,lcString)

 lcString = ""
 FPUTS(tFhandle,lcString)

 lcString = pmessage
 FPUTS(tFhandle,lcString)

 FCLOSE(tFhandle)
 
ENDPROC


