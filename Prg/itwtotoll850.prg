xtest=.t.

xindir="f:\ftpusers\itw\850in\"

xoutdir=iif(xtest,"f:\ftpusers\toll\itw\850test\","f:\ftpusers\toll\itw\850out\")

x997dir="f:\ftpusers\itw\850-997\"

**check if files exist in the inbound folder
xfilecnt=Adir(afiles,xindir+"*.xls*")
if xfilecnt=0
	return
endif

*xseparator=chr(13)+chr(10)
xcolcnt=25
xtollcustcode="ITWARKNBB"

if empty(xtollcustcode) or xtollcustcode="ITW "
	xFrom="TGFSYSTEM"
	if xtollcustcode="ITW"
		xSubject="ITW 850 ICON Transalation May Have A Test Toll Customer Code!! Must Change Before Going Live!"
	else
		xSubject="ITW 850 ICON Transalation Cannot Be Run - Empty Toll Customer Code!!"
	endif
	xbody=""
	do form dartmail2 with "mwinter@fmiint.com",xfrom,xsubject," ",,xbody,"a"

	if empty(xtollcustcode)
		return
	endif
endif


set deleted on
set century on
set hours to 24

create cursor xfilelist (file c(100), x c(10), moddt C(10), modtm c(10), moddttm t(1))
append from array afiles
replace all moddttm with ctot(moddt+" "+modtm)

**handle 997 creation
*!*	scan
*!*		xfile=alltrim(xfilelist.file)
*!*		xinfile=xindir+alltrim(xfilelist.file)
*!*		xfs=filetostr(xinfile)
*!*		if "ST*997*"$xfs
*!*			**save in 997in in case we need them
*!*			copy file &xinfile to &x997in.&xfile
*!*			delete file &xinfile
*!*			delete in xfilelist
*!*		else
*!*			**copy ONLY 850 files to the 997 directory so 997s are sent as needed - mvw 06/08/12
*!*			copy file &xinfile to &x997dir.&xfile
*!*		endif
*!*	endscan

use f:\sysdata\qqdata\unloccode in 0
use f:\ftpusers\itw\data\podata in 0
use f:\sysdata\qqdata\uom in 0

select *, .f. as found from podata where .f. into cursor xpodata readwrite

xfilecreated=.f.
select * from xfilelist order by moddttm into cursor xfilelist

scan
	if used("x850")
		use in x850
	endif

	xerrormsg=""
	xfilestr=""

	xfile = xindir+alltrim(xfilelist.file)
	xarchiveinfile = xindir+"archive\"+alltrim(xfilelist.file)

	xxfile = xindir+"xtemp.xls"+iif(lower(right(alltrim(xfile),1))="x","x","")
	xxorigfile = xxfile &&in case xlsx, need to delete it as well... xxfile changes to xls afetr saveas
	copy file "&xfile" to "&xxfile"

	oexcel = createobject("excel.application")
	oexcel.displayalerts = .f.
	oworkbook = oexcel.workbooks.open(xxfile,,.f.)

	**must save as csv then as xls 95/97 because we've encountered issues with dates, something to do with formatting i guess - mvw 09/17/13
*	xxfile=iif(lower(right(alltrim(xxfile),1))="x", left(alltrim(xxfile),len(alltrim(xxfile))-1), xxfile)
*	oworkbook.saveas(xxfile,39) && �39� converts the save format to excel95/97
	xxcsvfile=xindir+"xtemp.csv"
	oworkbook.saveas(xxcsvfile,6) && �6� converts the save format to csv
	oworkbook.close()
	oworkbook = oexcel.workbooks.open(xxcsvfile,,.f.)
	xxfile=iif(lower(right(alltrim(xxfile),1))="x", left(alltrim(xxfile),len(alltrim(xxfile))-1), xxfile)
	oworkbook.saveas(xxfile,39) && �39� converts the save format to excel95/97

	oworkbook.close()
	oexcel.quit()
	release oexcel

	select 0
*	import from "&xxfile" type xls
	create cursor xtemp (a c(50),b c(50),c c(50),d c(50),e c(50),f c(50),g c(50),h c(50),i c(50),j c(50),k c(50),l c(50),m c(50),;
		n c(50),o c(50),p c(50),q c(50),r c(50),s c(50),t c(50),u c(50),v c(50),w c(50),x c(50),y c(50),z c(50),aa c(50),ab c(50),;
		ac c(50),ad c(50),ae c(50),af c(50),ag c(50),ah c(50),ai c(50),aj c(50),ak c(50),al c(50),am c(50),an c(50),ao c(50),;
		ap c(50),aq c(50),ar c(50),as c(50),at c(50),au c(50),av c(50),aw c(50),ax c(50),ay c(50),az c(50))
	append from "&xxfile" type xls
	locate

	delete file "&xxcsvfile"
	delete file "&xxfile"
	if file("&xxorigfile")
		delete file "&xxorigfile"
	endif
	
	select xtemp

	**changed from 3 to 1, newest test file only has a single row header
*	delete next 3 &&delete header lines
	delete next 1 &&delete header line(s)
	delete for empty(d) &&delete extra lines in file (Col D is po number)
	replace all d with alltrim(d), e with alltrim(e)
	select * from xtemp order by d,e into cursor x850 readwrite
	use in xtemp

	**delete all pos that are missing any info (cols A-AC)
	**removed V, W, X, Y, AA, AB from list of required cols, we dont use them
	**removed col Z from required fields, never populated
	select * from x850 ;
		where empty(c) or empty(d) or empty(e) or empty(f) or empty(g) or empty(h) or empty(i) or empty(j) or ;
			empty(k) or ctod(l)={} or empty(m) or empty(n) or empty(o) or empty(p) or empty(q) or empty(r) or empty(s) or empty(t) or ;
			ctod(u)={} ;
		into cursor xtemp

	if !eof()
		xerrormsg="The following list of po lines are missing data and therefore will not be transmitted (Missing/Invalid columns are indicated for each PO line): "+chr(13)+chr(13)
	endif

	scan
		delete for d=xtemp.d in x850

		xerrormsg=xerrormsg+alltrim(upper(d))+" ("+iif(empty(e),"MISSING LINE NO",alltrim(e))+"): "+;
			iif(empty(c),"C, ","")+iif(empty(d),"D, ","")+iif(empty(e),"E, ","")+iif(empty(f),"F, ","")+iif(empty(g),"G, ","")+;
			iif(empty(h),"H, ","")+iif(empty(i),"I, ","")+iif(empty(j),"J, ","")+iif(empty(k),"K, ","")+iif(ctod(l)={},"L, ","")+;
			iif(empty(m),"M, ","")+iif(empty(n),"N, ","")+iif(empty(o),"O, ","")+iif(empty(p),"P, ","")+iif(empty(q),"Q, ","")+;
			iif(empty(r),"R, ","")+iif(empty(s),"S, ","")+iif(empty(t),"T, ","")+iif(ctod(u)={},"U, ","")
		xerrormsg=left(xerrormsg,len(xerrormsg)-2)+chr(13)
	endscan

	use in xtemp

	if !empty(xerrormsg)
		xerrormsg=xerrormsg+chr(13)+chr(13)+"All other errors are listed below (Again, any pos listed below have not been transmitted):"+chr(13)+chr(13)
	endif

	select x850
	locate

	**default delivery port UNLOC code required for Icon - default to EL PASO
 	xdelivport="USELP"

	**no CUR segment sent for ITW
	xcurrency="USD"

	**no incoterms included, so default to "FOB" as is mandatory for ICON - 04/10/13
	xincoterms="FOB"

	**keep track of previous po line as we have seen instances where PVH has sent the same line number 
	**	multiple times, each containing different skus - mvw 02/14/112
	xprevlineno=""
	**need sln to be defined here now in case i need to combine 2 identical line numbers for same po - mvw 2/14/12
	xslncnt=1

	xerror=.f.

	do while !eof()
		**reset for every po change - mvw 02/14/12
		xprevlineno=""
		xpostr=""

		xpochange=.f.
		zap in xpodata

		xponum=alltrim(d)
		xaction="PLC"
		xitemaction=xaction
		xorderdt=alltrim(u)
		xdorderdt=ctod(xorderdt) &&actual date var
		xorderdt=right(xorderdt,4)+"-"+left(xorderdt,2)+"-"+substr(xorderdt,4,2)
		xreqdt=xorderdt
		xdreqdt=xdorderdt &&actual date var

		xmsg=alltrim(ab)
		xcontact=""

		**WET = "50", COOKING = "200"
		xshiptocode=iif(lower(alltrim(c))="wet","50","200")
		xshiptoname=alltrim(c)
		store "" to xshiptoaddr1,xshiptoaddr2,xshiptocity,xshiptostate,xshiptozip,xshiptocountry

		xsuppliercode="3256"
		xsuppliername="DONGGUAN ARK-LES ELECTRIC COMPONENTS CO., LTD"
		store "" to xsupplieraddr1,xsupplieraddr2,xsuppliercity,xsupplierstate,xsupplierzip,xsuppliercountry

		**default UNLOC code for origin port for ITW to SHENZHEN CN, per rebecca
		xfobloccode="CNSZX"

		**change to 3-digit iso country (unloccode.isocountry) - mvw 12/07/11
		if !empty(xsuppliercountry)
			=seek(xsuppliercountry,"unloccode","country")
			xsuppliercountry=unloccode.isocountry
		endif

		xshipvia=iif(lower(alltrim(r))="air","AIR","SEA")
		xplanner=alltrim(a)
		xsrp4=alltrim(b)

		do while d=padr(xponum,len(d))
			xlineno=alltrim(e)

			**if duplicate line number for a given po, need to correct - mvw 02/14/12
			if xprevlineno==xlineno
				**skip over duplicate lines, do not send - 10/18/13 mvw
				xerrormsg=xerrormsg+"PO "+xponum+": Line "+xlineno+" Duplicated. Only 1st instance of this line will be sent."+chr(13)
				skip
				loop

				*xslncnt=xslncnt+1
			else
				xslncnt=1
			endif

			xprevlineno=xlineno

			xstyle=alltrim(f)
			xqty=alltrim(m)
			xqty=transform(val(xqty)) &&remove unnecesaary "0" decimal places, ie 50.000000 - mvw 04/16/13
			xshipqty=alltrim(h)
			xpack=alltrim(i)
			xboxcnt=alltrim(j)

			xunitprice="0.01"
			xuom=""
			xdescription=""

			xitemreqdt=alltrim(l)
			xditemreqdt=ctod(xitemreqdt) &&actual date var
			xitemreqdt=right(xitemreqdt,4)+"-"+left(xitemreqdt,2)+"-"+substr(xitemreqdt,4,2)

			xmdstatus=alltrim(o)
			xshiptodate=alltrim(n)
			xmaninvno=alltrim(p)
			xcustpartno=alltrim(g)
			xctnqty=alltrim(ac)

			if xerror
				exit
			endif

			**this should be last segment inside the item loop (PO1 loop), write to file here
			=writetostr() &&procedure below (within this prg)

			insert into xpodata (po_num,lineno,itemno,ctnqty,pack,origqty,shipqty,shiptodate,reqdt,adddt) values ;
				(xponum,xlineno,xstyle,val(xboxcnt),val(xpack),val(xqty),val(xshipqty),val(xshiptodate),xditemreqdt,datetime())

			skip
		enddo

		if !seek(padr(xponum,len(podata.po_num)),"podata","po_num")
			xpochange=.t.
		else
			select podata
			scan for po_num=padr(xponum,len(podata.po_num))
				select xpodata
				locate for lineno=podata.lineno
				if !found()
					xpochange=.t.
					exit
				else
					replace found with .t. in xpodata
					if itemno#podata.itemno or ctnqty#podata.ctnqty or pack#podata.pack or origqty#podata.origqty or shipqty#podata.shipqty or ;
					shiptodate#podata.shiptodate or reqdt#podata.reqdt
						xpochange=.t.
						exit
					endif
				endif
			endscan

			if !xpochange
				select xpodata
				locate for !found
				if found()
					xpochange=.t.
				endif
			endif

			select x850
		endif

		if xpochange
			delete for po_num=padr(xponum,len(podata.po_num)) in podata
			insert into podata select po_num,lineno,itemno,ctnqty,pack,origqty,shipqty,shiptodate,reqdt,adddt from xpodata
		endif

		select x850
		if xerror
			**move to next po
			if !eof()
				count while d=padr(xponum,len(d)) to xx
			endif
			xerror=.f.
		else
			**if no changes to the po, do not include in file
			if xpochange
				xfilestr=xfilestr+upper(xpostr)
			endif
		endif
	enddo

	select x850
	locate

	if !empty(xfilestr)
		**start with column headings
		xfilestr="order_no|order_split|order_status|client_cust_code|supplier_code|supplier_name|supplier_address_1|"+;
			"supplier_address_2|supplier_city|supplier_region_state|supplier_country|supplier_postcode|confirm_number|"+;
			"confirm_date|exw_required_by|delivery_required_by|description|order_date_time|order_value|order_currency_code|"+;
			"exchange_rate|incoterms|additional_terms|transport_mode|container_mode|milestone_tracking_date_exw|"+;
			"milestone_tracking_date_etd|milestone_tracking_date_eta|custom_milestone_est1|custom_milestone_act1|"+;
			"custom_milestone_est2|custom_milestone_act2|custom_milestone_est3|custom_milestone_act3|custom_milestone_est4|"+;
			"custom_milestone_act4|sending_agent|sending_agent_name|origin_port|destination_port|custom_text1|custom_text2|"+;
			"custom_text3|custom_text4|custom_text5|custom_date1|custom_date2|custom_decimal1|custom_decimal2|custom_decimal3|"+;
			"custom_decimal4|custom_decimal5|custom_flag1|custom_flag2|custom_flag3|custom_flag4|custom_flag5|contact1|"+;
			"contact2|country_code|notes|line_no|subline_no|linesplit_no|product|line_description|qty_ordered|innerpacks|"+;
			"outerpacks|unit_of_measure|itemprice|lineprice|linestatus|required_date|part_attrib1|part_attrib2|part_attrib3|"+;
			"line_custom_attrib1|line_custom_attrib2|line_custom_attrib3|line_custom_attrib4|line_custom_attrib5|"+;
			"line_custom_date1|line_custom_date2|line_custom_date3|line_custom_date4|line_custom_date5|line_custom_decimal1|"+;
			"line_custom_decimal2|line_custom_decimal3|line_custom_decimal4|line_custom_decimal5|line_custom_flag1|"+;
			"line_custom_flag2|line_custom_flag3|line_custom_flag4|line_custom_flag5|line_custom_text1|container_number|"+;
			"container_packing_order|country_of_origin|dg_substance|dg_flashpoint|weight|volume|weight_type|volume_type|"+;
			"special_instructions|additional_information|delivery_port|address_code|delivery_address_name|"+;
			"delivery_address_address1|delivery_address_address2|delivery_address_city|delivery_address_state|"+;
			"delivery_address_country|delivery_address_postcode"+chr(13)+chr(10)+xfilestr

		do while .t.
			set date ymd
			xoutfile="PO_TGFDI_"+strtran(strtran(strtran(ttoc(datetime()),":","")," ",""),"/")+xtollcustcode+".txt"
			set date american

			xarchiveoutfile=xoutdir+"archive\"+xoutfile
			if !file(xarchiveoutfile)
				exit
			endif
		enddo

		xoutfile=xoutdir+xoutfile
		strtofile(xfilestr,xoutfile)
		copy file &xoutfile to &xarchiveoutfile

		xfilecreated=.t.
	endif

	if !empty(xerrormsg)
		xfrom ="TGFSYSTEM"
		do form dartmail2 With "mwinter@fmiint.com",xFrom,"ITW 850 Translation Error"," ","",xerrormsg,"A"
	endif

	copy file "&xfile" to "&xarchiveinfile"
	delete file "&xfile"
endscan

if xfilecreated
	**create an ftpjob
	**when change from test to production, need to change out directory as well (at top of this prg)
	use f:\edirouting\ftpjobs in 0
	xjob=iif(xtest,"ITW-TEST-850-ICON","ITW-850-ICON")
	insert into ftpjobs (jobname, userid, jobtime) values (xjob,"ITW850",datetime())
	use in ftpjobs
endif

use in x850
use in xfilelist
use in unloccode
use in podata
use in uom

procedure writetostr

	**moved xhdrstr definition here from the top of the PO1 case because xshipvia isn't defined until after PO1 in the TD5 seg
	xhdrstr=alltrim(xponum)+"|"+; &&A
		"|"+; &&column(s) B empty
		xaction+"|"+; &&C
		xtollcustcode+"|"+;&&D
		xsuppliercode+"|"+; &&E
		xsuppliername+"|"+; &&F
		xsupplieraddr1+"|"+; &&G
		xsupplieraddr2+"|"+; &&H
		xsuppliercity+"|"+; &&I
		xsupplierstate+"|"+; &&J
		xsuppliercountry+"|"+; &&K
		xsupplierzip+"|"+; &&L
		"||"+; &&column(s) M-N empty
		xreqdt+"|"+; &&O
		"||"+; &&column(s) P-Q empty
		xorderdt+"|"+; &&R
		"|"+; &&column(s) S empty
		xcurrency+"|"+; &&T
		"|"+; &&column(s) U empty
		xincoterms+"|"+; &&V
		"|"+; &&column(s) W empty
		xshipvia+"|"+; &&X
		"||"+; &&column(s) Y-Z is empty
		"|"+; &&AA
		"|"+; &&column(s) AB is empty
		xreqdt+"|"+; &&AC
		"|||||||||"+; &&column(s) AD-AL empty
		xfobloccode+"|"+; &&AM
		xdelivport+"|"+; &&AN
		xplanner+"|"+; &&AO
		xsrp4+"|"+; &&AP
		xshiptoname+"|"+; &&AQ
		"|||||||||"+; &&column(s) AR-AZ
		"|"+; &&BA (860 flag)
		"||||"+; &&column(s) BB-BE empty
		xcontact+"|"+; &&BF
		"|"+; &&BG
		"|"+; &&BH
		xmsg+"|" &&BI


	**only add po line if the qty>0, in case Calderon sends cancelled lines with a 0 qty like AE does - mvw 05/09/12
	if val(xqty)>0
		**must change UOM from 2-digit to 3-digit for ICON translation - mvw 04/11/13
		if len(xuom)=2
			**default to units (UNT) if not found is UOM master
			**if the uom3 is only 2 digits, need to trim per Bill L - mvw 07/02/14
			xuom=alltrim(iif(seek(xuom,"uom","uom2"),uom.uom3,"UNT"))
		endif

		**removed the "-" from the lineno as ICON could not accept it. Now line number "001" will go as "0011" 
		**  for the 1st SLN, "0012" for the 2nd and so on. The right-most character will need to be stripped 
		**  for the 856 going back to PVH - mvw 01/12/12
		*xlineno+"-"+transform(xslncnt)+"|" &&BJ
		**no need to add slncnt to po line as there should only be 1 sln per po1 seg
		*xlineno+padl(transform(xslncnt),2,"0")+"|" &&BJ
		xpostr=xpostr+xhdrstr+;
			xlineno+"|"+; &&BJ
			"||"+; &&column(s) BK-BL empty
			xstyle+"|"+; &&BM
			xdescription+"|"+; &&BN
			transform(xqty)+"|"+; &&BO
			""+"|"+; &&BP - was xpack but moved to column CN - mvw 10/11/13
			""+"|"+; &&BQ - was xboxcnt but moved to column CL - mvw 10/11/13
			xuom+"|"+; &&BR
			transform(xunitprice)+"|"+; &&BS
			"|"+; &&column(s) BT empty
			xitemaction+"|"+; &&BU
			xitemreqdt+"|"+; &&BV
			"|"+; &&BW
			"|"+; &&BX - empty, only need 2 attributes style and sku
			"|"+; &&BY - empty, had held the 1st HTS # - mvw 02/10/12
			xshiptodate+"|"+; &&BZ
			xmdstatus+"|"+; &&CA
			xmaninvno+"|"+; &&CB
			xcustpartno+"|"+; &&CC
			xctnqty+"|"+; &&CD
			"|||||||"+; &&column(s) CE-CK empty
			xboxcnt+"|"+; &&CL - moved from column BQ - mvw 10/11/13
			"|"+; &&CM
			xpack+"|"+; &&CN - moved from column BP - mvw 10/11/13
			"||||||||"+; &&column(s) CO-CV empty
			"|"+; &&CW
			"||||||||"+;&&column(s) CX-DE empty
			xdelivport+"|"+; &&DF
			xshiptocode+"|"+; &&DG
			xshiptoname+"|"+; &&DH
			xshiptoaddr1+"|"+; &&DI
			xshiptoaddr2+"|"+; &&DJ
			xshiptocity+"|"+; &&DK
			xshiptostate+"|"+; &&DL
			xshiptocountry+"|"+; &&DM
			xshiptozip+; &&DN
			chr(13)+chr(10)
	endif

return
