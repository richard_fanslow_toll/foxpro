Close Data All
Public xfile,asn_origin,TranslateOption,lcScreenCaption,etadate,tsendto,tcc,tsendtoerr,tccerr,cCustname,cMod,lcPath,cfilename,lDoSQL,cWhse
Public nFileSize,cISA_Num,xfile,LogCommentStr,m.acctname,nAcctNum,lAddStyleCode,cOldBrand,cTotQty,cETADate,cHouseBill
*runack("BILLASN")


lTesting = .f.
lBrowfiles = .f.
lOverridebusy = .T.

xfile = ""
etadate=""
lcTempID = ""
cOffice="L"
cMod="L"
nAcctNum = 6718
gOffice = cOffice
gMasterOffice = cOffice
guserid = "BILL856"
cCustname="BILLABONG"
m.acctname = "BILLABONG"
lcPath=""
cfilename=""
nFileSize = 0
cISA_Num=""
xfile =""
LogCommentStr =""
m.bol = ""
m.awb = ""
Store 0 To m.inwologid,m.wo_num,m.plid

Do m:\dev\prg\_setvars With lTesting
If !lTesting
  On Error Do fmerror With Error(), Program(), Lineno(1), Sys(16),, .T.
Else
  On Error
Endif
******Added TMARG
TranslateOption = "CR_TILDE" &&"NONE"
Do createx856
lcScreenCaption = "Billabong 856 ASN Uploader Process... Processing File: "
_Screen.Caption = lcScreenCaption

Wait Window At 10,10 " Now uploading Billabong 856's........." Timeout 1

Select 0
Use F:\edirouting\FTPSETUP Shared
Locate For FTPSETUP.TRANSFER = "856-BILLABONG"
If Found()
  If chkbusy And !lOverridebusy
    Wait Window "Billabong 856 Process is busy...exiting" Timeout 2
    NormalExit = .T.
    Return
  Endif
Endif
Replace chkbusy With .T.,trig_time With Datetime()  For FTPSETUP.TRANSFER = "856-BILLABONG"
Use In FTPSETUP

*Assert .F. Message "At MM Config load"
Select 0
Use F:\3pl\Data\mailmaster Alias mm Shared
Locate For mm.accountid = nAcctNum And edi_type = "856"
If Found()
  Store Trim(mm.basepath) To lcPath
  Store Trim(mm.archpath) To lcArchivePath
  tsendto = Iif(mm.use_alt,Trim(mm.sendtoalt),Trim(mm.sendto))
  tcc = Iif(mm.use_alt,Trim(mm.ccalt),Trim(mm.cc))
  Store Trim(mm.scaption) To thiscaption
  Store mm.testflag      To lTest
  _Screen.Caption = thiscaption
  Locate For (mm.edi_type = "MISC") And (mm.taskname = "GENERAL")
  If lTesting
    tsendto = Iif(mm.use_alt,Trim(mm.sendtoalt),Trim(mm.sendto))
    tcc = Iif(mm.use_alt,Trim(mm.ccalt),Trim(mm.cc))
  Endif
  tsendtoerr = Iif(mm.use_alt,Trim(mm.sendtoalt),Trim(mm.sendto))
  tccerr = Iif(mm.use_alt,Trim(mm.ccalt),Trim(mm.cc))
  Use In mm
Else
  Wait Window At 10,10  "No parameters set for this acct# "+Alltrim(Str(nAcctNum))+"  ---> Office "+cOffice Timeout 2
  Use In mm
  NormalExit = .T.
  Throw
Endif

Wait Window At 10,10  "Processing BILLABONG 856s............." Timeout 1

If lTesting
  cUseFolder = "F:\WHP\WHDATA\"
  cOffice="P"
  cMod = "P"
Else
  xReturn = "XX"
  Do m:\dev\prg\wf_alt With cOffice,nAcctNum
  cUseFolder = Upper(xReturn)
Endif
cWhse = Iif(lTesting,"WHP",Lower("wh"+cOffice))

*Use (cUseFolder+"whgenpk") In 0 Alias whgenpk

If !lTesting
  useca("pl","wh")
  xsqlexec("select * from pl where .f.","xpl",,"wh")
Else
  Use (cUseFolder+"pl") In 0 Alias pl
  Select * From pl Where .F. Into Cursor xpl Readwrite
Endif

useca("inwolog","wh")
xsqlexec("select * from inwolog where .f.","xinwolog",,"wh")

useca("ctnucc","wh")
xsqlexec("select Space(30) as shipmentid,* from ctnucc where 1=0","xctnucc",,"wh")

cDELIMITER = "|"
Do ProcessBILLABONGasn

Select 0
Use F:\edirouting\FTPSETUP Shared
Replace chkbusy With .F. For FTPSETUP.TRANSFER = "856-BILLABONG"
Use In FTPSETUP

***********************************************************************************************************
Procedure ProcessBILLABONGasn

lnNum = Adir(tarray,"F:\FTPUSERS\billabong\856IN\*.*")

If lTesting
  lcPath = "F:\FTPUSERS\Billabong\856IN\test\"
  lnNum = Adir(tarray,"F:\FTPUSERS\Billabong\856IN\test\*.*")
Endif

If lnNum > 0
  For thisfile = 1  To lnNum
    xfile = lcPath+tarray[thisfile,1]  &&+"."
    cfilename =tarray[thisfile,1]  &&+"."
    archivefile = lcArchivePath+tarray[thisfile,1]  &&+"."
    nFileSize = Val(Transform(tarray(thisfile,2)))
    Wait Window "Importing file: "+cfilename Timeout 1
    Do m:\dev\prg\createx856
    lcDELIMITER = "|"
    lcTranslateOption ="NAUTICA"
    Do loadedifile With xfile,"|",TranslateOption,"JONES856"
    Select x856
    Do  Importbillabongasn
    Copy File &xfile To &archivefile
    If File(archivefile)
      Delete File &xfile
    Endif
  Next thisfile
Else
  Wait Window  "No 856's to Import" Timeout 1
Endif

Endproc

******************************************************************************************************************
Procedure Importbillabongasn
******************************************************************************************************************

lnPrfCtr = 0
npo = 0
Set Century On
*SET DATE TO YMD

Select x856
Goto Top
lcHLLevel = ""
lcVendorCode = ""
lcCurrentHAWB = ""
lcCurrentPO = ""

testing=.F.

POs_added = 0
ShipmentNumber = 1
ThisShipment = 0
origawb = ""
lcCurrentShipID = ""
lcCurrentArrival = ""
LAFreight = .F.

** here we clear out asn data already loaded from this file
Select x856
Set Filter To
Locate
Store "" To m.suppdata,m.awb,m.echo
lNewPL = .T.

*!*    SET STEP ON
*!*    CREATE CURSOR tempco_in (cocode c(2),coacct N(4))
*!*    SELECT x856
*!*    locate
*!*    SCAN FOR x856.segment = "REF" AND x856.f1 = "3L"  && Company breakout
*!*      m.cocode = ALLTRIM(x856.f2)
*!*      m.coacct =  ICASE(ALLTRIM(m.cocode)="BB",6744,ALLTRIM(m.cocode)="CR",6747,ALLTRIM(m.cocode)="RV",6747,ALLTRIM(m.cocode)="CA",6744,6718)
*!*      INSERT INTO tempco_in FROM MEMVAR
*!*    ENDSCAN
*!*    SELECT * FROM tempco_in GROUP BY cocode,coacct INTO CURSOR tempco
*!*    USE IN tempco_in
*!*    IF lTesting
*!*      BROWSE
*!*    ENDIF
*!*    LOCATE

Select x856
Locate

llDone = .F.
lnCtnCount = 0

Do While !llDone &&!Eof("x856")
  Wait Window "At the outer loop " Nowait
  cHouseBill = ""
  If Trim(x856.segment) = "ISA"
    cISA_Num=Alltrim(x856.f13)
    m.isa   = Alltrim(x856.f13)
    m.dateloaded = Datetime()
    m.asnfile    = Upper(Justfname(xfile))
    m.office = cOffice
    m.mod = cMod
    Select x856
    Skip 1 In x856
    Loop
  Endif

  If Inlist(x856.segment,"GS","ST")
    Select x856
    Skip 1 In x856
    Loop
  Endif

  If Trim(x856.segment) = "BSN"&& and alltrim(x856.f1) = "05"  && must delete then add new info
    cOldBrand = "XX"
    m.inwologid = m.inwologid+1
    m.wo_num = m.wo_num+1
    m.wo_date = Date()
    m.adddt = Datetime()
    m.addproc = "BILL856"
    m.addby = "BILL856IN"
    m.accountid = nAcctNum
    m.reference = Alltrim(x856.f2)
    m.shipmentid = Alltrim(x856.f2)
    m.shipid = Alltrim(x856.f2)
*      m.bsn    = ALLTRIM(x856.f2)
    m.comments = "FILENAME*"+Alltrim(Upper(cfilename))
    Insert Into xinwolog From Memvar
    Select x856
*    Wait Window At 10,10 "Shipment: "+ Alltrim(x856.f2)
    Skip 1 In x856
    Loop
  Endif

  If At("HL",x856.segment)>0
    lcHLLevel = Alltrim(x856.f3)
  Endif

  If lcHLLevel = "S"
*!*        SELECT tctnucc
*!*        SCATTER MEMVAR MEMO BLANK FIELDS EXCEPT isa,bsn,asnfile,hawb,shipid
    Do While lcHLLevel = "S"
      Select x856
      Skip 1 In x856

      If Trim(x856.segment) = "TD1"
        cType1 = Alltrim(x856.f1)
        m.qty     = Int(Val(Alltrim(x856.f2)))
        Replace xinwolog.plinqty With m.qty In xinwolog
        Replace xinwolog.plunitsinqty With m.qty In xinwolog
        cTotQty = "CTNQTY*"+Trans(m.qty)
        Replace xinwolog.comments With xinwolog.comments+Chr(13)+cTotQty In xinwolog
      Endif

      If Trim(x856.segment) = "TD3"
        m.container  = Alltrim(x856.f2)+Alltrim(x856.f3)
        Replace xinwolog.Container With m.container In xinwolog
      Endif

      If Trim(x856.segment) = "DTM" And Alltrim(x856.f1)= "371"
        cETADate= "ETADATE*"+Substr(Alltrim(x856.f2),5,2)+"/"+Substr(Alltrim(x856.f2),7,2)+"/"+Substr(Alltrim(x856.f2),1,4)
        Replace xinwolog.comments With xinwolog.comments+Chr(13)+cETADate In xinwolog
      Endif

      Select x856
      If Trim(x856.segment) = "HL"
        lcHLLevel = Trim(x856.f3)
      Endif
    Enddo
  Endif   && end if HL S

  If lcHLLevel = "W"  && They may not send this HL level
    Do While lcHLLevel = "W"
      Select x856
      Skip 1 In x856
      If Trim(x856.segment) = "REF" And Trim(x856.f1) = "BM"
        cHouseBill = "HOUSEBOL*"+Alltrim(x856.f2)
        Replace xinwolog.acct_ref With Alltrim(x856.f2) In xinwolog
        Replace xinwolog.comments With xinwolog.comments+Chr(13)+cHouseBill In xinwolog
        Release cHouseBill
      Endif
      If Trim(x856.segment) = "HL"
        lcHLLevel = Trim(x856.f3)
      Endif
    Enddo
  Endif

  If lcHLLevel = "O"

*    Set Step On
    Select x856
    Skip 1 In x856
    If Trim(x856.segment) = "PRF"
*     Set Step On
      lcCurrentPO = Alltrim(x856.f1)
      m.ponum = Alltrim(x856.f1)
      Skip 3 In x856
      If Alltrim(x856.segment) = "REF" And Alltrim(x856.f1)="3L"
        cBrand = Alltrim(x856.f2)
        Do Case
        Case Alltrim(cBrand )="BB"
          nAcctNum= 6744
          cCustname='BILLABONG'
          m.acctname='BILLABONG'
          Replace xinwolog.accountid With nAcctNum In xinwolog
        Case Alltrim(cBrand )="EL"
          nAcctNum= 6718
          cCustname='ELEMENT'
          m.acctname='ELEMENT'
          Replace xinwolog.accountid With nAcctNum  In xinwolog
        Case Alltrim(cBrand )="RV"
          nAcctNum= 6747
          cCustname='RVCA'
          m.acctname='RVCA'
          Replace xinwolog.accountid With nAcctNum  In xinwolog
        Case Alltrim(cBrand )="CA"
          nAcctNum= 6744
          cCustname='BILLABONG'
          m.acctname='BILLABONG'
          Replace xinwolog.accountid With nAcctNum  In xinwolog
        Case Alltrim(cBrand )="EC"
          nAcctNum= 6718
          cCustname=' ELEMENT'
          m.acctname='ELEMENT'
          Replace xinwolog.accountid With nAcctNum  In xinwolog
        Case Alltrim(cBrand )="CR"
          nAcctNum= 6747
          cCustname='RVCA'
          m.acctname='RVCA'
          Replace xinwolog.accountid With nAcctNum  In xinwolog
        Endcase
      Endif
    Endif
    Skip 2 In x856  && was 3

    llCartonsDone = .F.
    llAnotherRecord = .F.

    Do While !llCartonsDone  x&&856.segment != "PRF"

      If Trim(x856.segment) = "HL"
        lcHLLevel = Trim(x856.f3)
        Skip 1 In x856
      Endif

      If Trim(x856.segment) = "PO4"
        m.totqty =0
        thispack =0
        Skip 1 In x856
      Endif

      If Trim(x856.segment) = "MAN"
*set step On
        m.ucc =  Alltrim(x856.f2)
      Endif

      Skip 2 In x856
      llCtnDone = .F.
      Do While !llCtnDone
        Wait Window "At the carton loop " Nowait
        If Trim(x856.segment) = "LIN"
          m.style = Alltrim(x856.f3)
          m.upc   = Alltrim(x856.f5)
          m.color = Alltrim(x856.f7)
          m.id    = Alltrim(x856.f9)
          m.pack = Alltrim(x856.f11)
          m.totqty = Val(Alltrim(x856.f11))
          insertinto("xctnucc")
          If llAnotherRecord = .T.
            replace xctnucc.ship_ref With Alltrim(m.ponum)+Right(Alltrim(ucc),4) In xctnucc
          Endif
           llAnotherRecord = .f.
        Endif

        Skip 2 In x856
        If  Trim(x856.segment) = "LIN"
          replace xctnucc.ship_ref With Alltrim(m.ponum)+Right(Alltrim(ucc),4) In xctnucc
          llAnotherRecord = .T.
        Else
        llAnotherRecord = .f.
        llCtnDone = .T.
       Endif

        If  Trim(x856.segment) != "LIN"
          Skip -2 In x856
          If Trim(x856.segment)="SE"
            Set Step On
            llCartonsDone = .T.
            lnCtnCount = lnCtnCount +1
            llCtnDone = .T.
          Endif
        Endif

       If Inlist(x856.segment,"GE","IEA")
         llCartonsDone = .T.
         llCtnDone = .T.
         llDone = .T.
       endif 
      Enddo

      If ! llAnotherRecord
        Skip 1 In x856
      Endif


      Do Case
      Case Trim(x856.segment) = "ST"
        llCartonsDone = .T.

      Case Trim(x856.segment) = "SE"
        llCartonsDone = .T.
        Skip 1 In x856

      Case Inlist(x856.segment,"GE","IEA")
        llCartonsDone = .T.
        llDone = .T.
        Set Step On
      Otherwise
&&    Skip -3 In x856
      Endcase

      If Trim(x856.segment) = "CTT"
        Set Step On
      Endif

      Select x856
*      Skip 1 In x856

      If Trim(x856.segment) = "HL"
        lcHLLevel = Trim(x856.f3)
        If lcHLLevel = "O"
          llCartonsDone = .T.
*      Wait Window At 10,10 "PO "+lcCurrentPO+"  Cartons: "+Transform(lnCtnCount)
        Endif
      Endif

      If Eof("x856")
        Exit
      Endif
    Enddo
  Endif

  If Eof("x856")
    Exit
  Endif
       If Inlist(x856.segment,"GE","IEA")
         llCartonsDone = .T.
         llCtnDone = .T.
         llDone = .T.
       endif 
Enddo


Set Step On

Select xctnucc
Scan
  Select xctnucc
  Scatter memvar 
  insertinto("ctnucc","wh",.T.)
Endscan

tu()
set step on

Select Count(1) as ctnqty ,shipmentid,Style,Color,Id,Pack,ponum From xctnucc Where Empty(ship_ref) Group By shipmentid,Style,Color,Id,Pack,ponum Into Cursor tempsolids

Select Count(1) as ctnqty,ship_ref as CtnType, shipmentid,Style,Color,Id,Pack,ponum From xctnucc Where !Empty(ship_ref) Group By shipmentid,Style,Color,Id,Pack,ponum Into Cursor tempmixed


xxid = 1
Select xinwolog
Scan
  Replace xinwolog.inwologid With xxid
  xxid = xxid +1
Endscan
m.adddt = Datetime()
m.addproc = "BBG856"
m.addby = "BBG856IN"

xxplid = 1

Select xinwolog
Scan
  Select tempsolids
  lnGroupnum = 1
  Scan For xinwolog.Reference = tempsolids.shipmentid
    Select xpl
    Scatter Memvar Memo
    m.po = Transform(lnGroupnum)
    m.units = .F.
    m.accountid = xinwolog.accountid
    m.inwologid = xinwolog.inwologid
    m.plid = xxplid
    m.style = tempsolids.Style
    m.color = tempsolids.Color
    m.id    = tempsolids.Id
    m.pack  = tempsolids.Pack
    m.totqty=  tempsolids.ctnqty
    m.cayset = tempsolids.ponum

    Insert Into xpl From Memvar
    m.totqty =Val(m.pack)* m.totqty
    m.units = .T.
    m.pack ="1"
    xxplid = xxplid+1
    m.plid = xxplid
    Insert Into xpl From Memvar
    lnGroupnum = lnGroupnum+1
    xxplid = xxplid+1
    m.plid = xxplid
  Endscan

  Select * From tempmixed Group By ctntype into Cursor justctns
  Select justctns
*  lnGroupnum = 1
  Scan For xinwolog.Reference = tempmixed.shipmentid
    Select xpl
    Scatter Memvar Memo
    m.po = Transform(lnGroupnum)
    m.units = .F.
    m.accountid = xinwolog.accountid
    m.inwologid = xinwolog.inwologid
    m.plid = xxplid
    lnCtnplid = xxplid
    m.style = justctns.Style
    m.color = justctns.Color
    m.id    = justctns.ctntype
    m.pack  = justctns.Pack
    m.totqty=  justctns.ctnqty
    m.cayset = justctns.ponum
    lcThismixedctn = justctns.ctntype
    Insert Into xpl From Memvar
    
    Select * From tempmixed Where ctntype = lcThismixedctn Into Cursor tmixed
    Select tmixed
    lnThisPack = 0
    scan  
      m.totqty =Val(tmixed.pack)
      lnThispack = lnThispack+m.totqty
      xxplid = xxplid+1
      m.id = tmixed.id
      m.plid = xxplid
      m.units = .T.
      m.pack ="1"
      Insert Into xpl From Memvar
      replace pack With Transform(lnThispack) For xpl.plid = lnCtnplid In xpl 
    endscan 
    xxplid = xxplid+1
    lnGroupnum = lnGroupnum+1
  Endscan

Endscan

set step On 


xxid = 1
Select xinwolog
Scan
  Select xpl
  Sum totqty To totunits  For units And xpl.inwologid = xxid
  Replace xinwolog.plunitsinqty With totunits In xinwolog
  xxid = xxid +1
Endscan


Set Step On

Select xpl
If lTesting
  Locate
*  BROWSE
Endif

*rollup()
Select xpl
Locate

If lTesting Or lBrowfiles

Endif

set step On 


If !lTesting
  Do StoreASNData
Endif

Do Import_Data

LogCommentStr = ""
m.awb=""

Endproc

*****************************
Procedure Import_Data
*****************************

Select xinwolog
Locate


Scan  && Currently in XINWOLOG table
  Select inwolog
  gOffice = "L"
  gMasterOffice = "L"
  m.mod = cMod
  m.office = cOffice
  Select xinwolog
  Scatter Memvar Memo
  nwo_num = dygenpk("wonum",cWhse)
  m.wo_num = nwo_num
  m.wo_date = Date()
  m.addproc ="BBG856IN"
*  m.accountid = nAcctNum
  cWO_Num = Allt(Str(m.wo_num))
  insertinto("inwolog","wh",.T.)

  Select xpl
  Scan For xpl.inwologid = xinwolog.inwologid
    Scatter Fields Except plid Memvar Memo
    m.date_rcvd = Date()
    m.mod = cMod
    m.office = cOffice
    m.inwologid = inwolog.inwologid
*    m.accountid = nAcctNum
    m.wo_num = nwo_num
    insertinto("pl","wh",.T.)
  Endscan

Endscan

tu("inwolog")
Set Step On

tu("pl")

Set Step On


tfrom    ="TGF/BILLABONG EDI Poller Operations <toll-edi-ops@tollgroup.com>"
tsubject = "BILLABONG ASN Uploader WO#: "+Alltrim(Transform(inwolog.wo_num))+"  Container: "+inwolog.Container+ "  ETA: "+etadate
tattach =""
tmessage = "Inbound Created for Toll-Mira Loma Warehouse"+Chr(13)
tmessage = tmessage +;
  "-------------------------------------------------"+Chr(13)+;
  "Work Order    :"+Alltrim(Transform(inwolog.wo_num))+Chr(13)+;
  "Container     :"+inwolog.Container+Chr(13)+;
  "Filename      : "+xfile+Chr(13)+;
  "ETA           : "+etadate+Chr(13)+;
  "BOL           : "+Transform(inwolog.Reference)+Chr(13)+;
  "Reference     : "+Transform(inwolog.brokerref)+Chr(13)+;
  "# of Cartons  : "+Alltrim(Transform(inwolog.plinqty))+Chr(13)+;
  "Containing    : "+Alltrim(Transform(inwolog.plunitsinqty))+" Units"

Do Form dartmail2 With tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"

Close Data All
etadate = ""

Endproc

***************************
Procedure StoreASNData
***************************
lcQuery = [select * from ediinlog where accountid = ]+Transform(nAcctNum)+[ and isanum = ']+cISA_Num+[']
xsqlexec(lcQuery,"p1",,"stuff")
If Reccount()=0
  Use In p1
  useca("ediinlog","stuff")  && Creates updatable ediinlog cursor

  Select ediinlog
  Scatter Memvar Memo
  m.ediinlogid = dygenpk("ediinlog","WHALL")
  m.acct_name  = cCustname
  m.accountid  = nAcctNum
  m.mod        = cMod
  m.office     = cOffice
  m.filepath   = lcPath
  m.filename   = cfilename
  m.size       = nFileSize
  m.FTIME      = Date()
  m.FDATETIME  = Datetime() &&tarray(thisfile,4)
  m.TYPE       = "856"
  m.qty        = 0
  m.isanum     =  cISA_Num
  m.uploadtime = Datetime()
  m.comments   = LogCommentStr
  m.edidata    = ""
  Insert Into ediinlog From Memvar
  Append Memo edidata From &xfile
  tu("ediinlog")
Endif

**********************
Procedure rollup
**********************
*SET STEP ON
Select xpl
Wait Window "In XPL Rollup procedure" Timeout 1
Wait Window "In XPL Rollup procedure" Nowait
*ASSERT .F. MESSAGE "In xpl rollup procedure"
nChangedRecs = 0
Select xpl
Set Deleted On
Locate
cMsgStr = "PL/INWOLOGID/PLIDs changed: "
nTotqty1 = 0
Scan For !Deleted()
  Scatter Memvar
  nRecno = Recno()
*!* Added some code to run through each line item rather than assuming only one duplicated line is involved.
  Skip 1 In xpl
  lChange = .F.
  Do While !Eof()
    If xpl.inwologid = m.inwologid And xpl.Style = m.style And xpl.Color = m.color And xpl.Id = m.id And xpl.Pack = m.pack
      lChange = .T.
      nTotqty1 = nTotqty1+xpl.totqty
      cEchoCopy = Alltrim(xpl.Echo)
      Delete Next 1 In xpl
      nChangedRecs = nChangedRecs+1
      If   lChange
        lChange = .F.
        Go nRecno
        Replace xpl.totqty With xpl.totqty+nTotqty1 In xpl
*        SET STEP on
        Replace xpl.Echo With xpl.Echo+Chr(13)+Chr(13)+cEchoCopy In xpl
        Release cEchoCopy
        nTotqty1 = 0
      Endif
      Skip 1 In xpl
    Else
      Skip 1 In xpl
    Endif
    nTotqty1 = 0
  Enddo
  Go nRecno
Endscan
Release lChange

If nChangedRecs > 0
  Wait Window "There were "+Trans(nChangedRecs)+" XPL records rolled up" Timeout 2
Else
  Wait Window "There were NO XPL records rolled up" Timeout 2
Endif

Endproc

************************
Procedure x856tally
************************
Create Cursor temp856 (upc c(12),Pack c(5),po_num c(15))
Select x856
Locate
Scan
  If x856.segment = "PRF"
    m.po_num = Alltrim(x856.f1)
  Endif
  If x856.segment = "LIN"
    m.upc = Alltrim(x856.f5)
    m.pack = Alltrim(x856.f11)
    Insert Into temp856 From Memvar
  Endif
Endscan
Select upc,Pack,po_num,Count(Pack) As cnt1 From temp856 Group By 1,2
Set Step On
