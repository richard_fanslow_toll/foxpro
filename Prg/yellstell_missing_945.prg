**This script identifies when a 945 has not been created for a PT.  

utilsetup("YELLSTEEL_MISSING_945")
Set Exclusive Off
Set Deleted On
Set Talk Off
Set Safety Off 

WITH _SCREEN	
	.AUTOCENTER = .T.
	.WINDOWSTATE = 0
	.BORDERSTYLE = 1
	.WIDTH = 320
	.HEIGHT = 210
	.TOP = 200
	.LEFT = 100
	.CLOSABLE = .T.
	.MAXBUTTON = .T.
	.MINBUTTON = .T.
	.SCROLLBARS = 0
	.CAPTION = "YELLSTEEL_MISSING_945"
ENDWITH	

schedupdate()

goffice="1"

if usesqloutwo()
else
	If !Used("outship")
	  Use F:\wh1\whdata\outship.Dbf In 0
	endif
endif

If !Used("edi_trigger")
  use f:\3pl\data\edi_trigger shared In 0
ENDIF
SET STEP ON

*!*	SELECT ACCOUNTID,WO_NUM,SHIP_REF FROM outship where INLIST(accountid,6532) AND  delenterdt >DATE()-20  and delenterdt<datetime()-1290;
*!*	 AND !INLIST(ship_ref,'0082389802','0082442242','0082587747','0082499503' ,'0082423603','0082367184','0082700041','0082632495','0082416752','0082967840','0083078040') into CURSOR temp945 
*!*	SELECT DISTINCT ACCOUNTID,WO_NUM,SHIP_REF FROM EDI_TRIGGER WHERE EDI_TYPE='945 ' AND TRIG_TIME>date()-90 AND INLIST(accountid,6532);
*!*	 AND !INLIST(ship_ref,'0082389802','0082442242','0082587747','0082499503' ,'0082423603','0082367184','0082700041','0082632495','0082416752','0082967840','0083078040') INTO CURSOR TEMP21
*!*	SELECT * FROM temp945 t LEFT JOIN TEMP21 e ON t.accountid=e.accountid AND t.wo_num=e.wo_num AND t.ship_ref=e.ship_ref  INTO CURSOR temp22
*!*	SELECT * FROM TEMP22 WHERE ISNULL(ACCOUNTID_B) INTO CURSOR TEMP23
*!*	SELECT  TEMP23

if usesqloutwo()
	xsqlexec("select accountid, wo_num, consignee, ship_ref from outship where mod='"+goffice+"' " + ;
		"and accountid=6209 and delenterdt>{"+dtoc(date()-20)+"} and delenterdt<{"+ttoc(datetime()-3600)+"} and !empty(bol_no)","temp945",,"wh")
else
	SELECT ACCOUNTID,WO_NUM,SHIP_REF,consignee,del_date FROM outship where INLIST(accountid,6209) AND  delenterdt >DATE()-20  and delenterdt<datetime()-3600;
		AND !EMPTY(bol_no) into CURSOR temp945
endif

SELECT * FROM edi_trigger WHERE accountid=6209 INTO CURSOR vedi_trigger READWRITE 
SELECT * FROM temp945 a LEFT JOIN vedi_trigger b ON a.accountid=b.accountid AND a.wo_num=b.wo_num AND a.ship_ref=b.ship_ref INTO CURSOR c1 READWRITE
SELECT * FROM c1 where ISNULL(fin_status) INTO CURSOR c2 READWRITE 

If Reccount() > 1
  Export To "t:\yellsteel\missing_945\yellsteel_945_trigger_not_created"  Type Xls
  tsendto = "todd.margolin@tollgroup.com,paul.gaidis@tollgroup.com,Maria.Estrella@Tollgroup.com,joe.bianchi@tollgroup.com,juan.rocio@tollgroup.com"
  tattach = "t:\yellsteel\missing_945\yellsteel_945_trigger_not_created.xls"
  tcc =""
  tFrom ="Toll/FMI EDI System Operations <fmi-transload-ops@fmiint.com>"
  tmessage = "yellsteel_945_trigger_not_created"+Ttoc(Datetime())
  tSubject = "yellsteel_945_trigger_not_created, CHECK POLLER WH1"
  Do Form m:\dev\frm\dartmail2 With tsendto,tFrom,tSubject, tcc,tattach,tmessage  ,"A"
ELSE 
  tsendto = "todd.margolin@tollgroup.com"
  tattach = ""
  tcc =""
  tFrom ="Toll/FMI EDI System Operations <fmi-transload-ops@fmiint.com>"
  tmessage = "NO_MISSING_yellsteel_945_triggers_exist"+Ttoc(Datetime())
  tSubject = "NO_MISSING_yellsteel_945_triggers_exist"
  Do Form m:\dev\frm\dartmail2 With tsendto,tFrom,tSubject, tcc,tattach,tmessage  ,"A"
ENDIF


SELECT DISTINCT ACCOUNTID,WO_NUM,SHIP_REF,fin_status FROM vEDI_TRIGGER WHERE EDI_TYPE='945'  AND INLIST(accountid,6209);
 AND !errorflag AND   (empty(fin_status) OR EMPTY(when_proc)) INTO CURSOR TEMP21
 SELECT * FROM temp21 WHERE fin_status !='NO 945 ' INTO CURSOR temp22
SELECT  TEMP22
If Reccount() > 0
  Export To "t:\yellsteel\missing_945\yellsteel_945_trigger_created_not_processed"  Type Xls

  tsendto = "todd.margolin@tollgroup.com,paul.gaidis@tollgroup.com,joe.bianchi@tollgroup.com"
  tattach = "t:\yellsteel\missing_945\yellsteel_945_trigger_created_not_processed.xls"
  tcc =""
  tFrom ="Toll/FMI EDI System Operations <fmi-transload-ops@fmiint.com>"
  tmessage = "yellsteel_945_trigger_created_not_processed"+Ttoc(Datetime())
  tSubject = "yellsteel_945_trigger_created_not_processed  WH1"
  Do Form m:\dev\frm\dartmail2 With tsendto,tFrom,tSubject, tcc,tattach,tmessage  ,"A"
ELSE 
  tsendto = "tmarg@fmiint.com"
  tattach = ""
  tcc =""
  tFrom ="Toll/FMI EDI System Operations <fmi-transload-ops@fmiint.com>"
  tmessage = "NO_unprocessed_945s_for_yellsteel"+Ttoc(Datetime())
  tSubject = "NO_unprocessed_945s_for_yellsteel"
  Do Form m:\dev\frm\dartmail2 With tsendto,tFrom,tSubject, tcc,tattach,tmessage  ,"A"
ENDIF



SELECT * FROM vEDI_TRIGGER WHERE EDI_TYPE='945 '  AND INLIST(accountid,6209);
 AND errorflag INTO CURSOR TEMP21
SELECT  TEMP21
If Reccount() > 0
  Export To "S:\steelseries\missing_945\steelseries_trigger_error"  Type Xls

  tsendto = "tmarg@fmiint.com,todd.margolin@tollgroup.com,paul.gaidis@tollgroup.com,joe.bianchi@tollgroup.com"
  tattach = "t:\yellsteel\missing_945\yellsteel_trigger_error.xls"
  tcc =""
  tFrom ="Toll/FMI EDI System Operations <fmi-transload-ops@fmiint.com>"
  tmessage = "yellsteel_945_trigger_error"+Ttoc(Datetime())
  tSubject = "yellsteel_945_trigger_error  WH1"
  Do Form m:\dev\frm\dartmail2 With tsendto,tFrom,tSubject, tcc,tattach,tmessage  ,"A"
ELSE 
  tsendto = "tmarg@fmiint.com"
  tattach = ""
  tcc =""
  tFrom ="Toll/FMI EDI System Operations <fmi-transload-ops@fmiint.com>"
  tmessage = "NO_yellsteel_trigger_errors"+Ttoc(Datetime())
  tSubject = "NO_yellsteel_trigger_errors"
  Do Form m:\dev\frm\dartmail2 With tsendto,tFrom,tSubject, tcc,tattach,tmessage  ,"A"
Endif

if usesqloutwo()
	xsqlexec("select wo_num, ship_ref, consignee, del_date from outship where mod='"+goffice+"' " + ;
		"and accountid=6209 and del_date>{"+dtoc(date()-30)+"} and empty(bol_no)","temp23",,"wh")
else
	SELECT ACCOUNTID,WO_NUM,SHIP_REF,consignee,del_date FROM outship where INLIST(accountid,6209) AND !EMPTY(del_date) AND EMPTY(bol_no) AND del_date>{01/01/2016};
	 into CURSOR temp23
endif

SELECT  TEMP23
If Reccount() > 0
  Export To "t:\yellsteel\missing_945\yellsteel_missing_bol"  Type Xls

  tsendto = "tmarg@fmiint.com,todd.margolin@tollgroup.com,paul.gaidis@tollgroup.com,joe.bianchi@tollgroup.com"
  tattach = "t:\yellsteel\missing_945\yellsteel_missing_bol.xls"
  tcc =""
  tFrom ="Toll/FMI EDI System Operations <fmi-transload-ops@fmiint.com>"
  tmessage = "yellsteel_missing_bol"+Ttoc(Datetime())
  tSubject = "yellsteel_missing_bol  WH1"
  Do Form m:\dev\frm\dartmail2 With tsendto,tFrom,tSubject, tcc,tattach,tmessage  ,"A"
ELSE 
  tsendto = "tmarg@fmiint.com"
  tattach = ""
  tcc =""
  tFrom ="Toll/FMI EDI System Operations <fmi-transload-ops@fmiint.com>"
  tmessage = "NO_yellsteel_missing_bol"+Ttoc(Datetime())
  tSubject = "NO_yellsteel_missing_bol"
  Do Form m:\dev\frm\dartmail2 With tsendto,tFrom,tSubject, tcc,tattach,tmessage  ,"A"
Endif
Close Data All

_Screen.Caption=gscreencaption