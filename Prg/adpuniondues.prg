* report prior month Union Dues Ded Code 25 for Lucille
* run around 1st of each month

* Build EXE as F:\UTIL\ADPREPORTS\ADPUNIONDUESRPT.EXE

runack("ADPUNIONDUESRPT")

utilsetup("ADPUNIONDUESRPT")

LOCAL loUnionDuesReport
loUnionDuesReport = CREATEOBJECT('UnionDuesReport')
loUnionDuesReport.MAIN()

schedupdate()

CLOSE DATABASES ALL
RETURN

#DEFINE LOGIT 1
#DEFINE WAITIT 2
#DEFINE NOWAITIT 4
#DEFINE SENDIT 8
#DEFINE CRLF CHR(13) + CHR(10)
#DEFINE RETURN_DATA_MANDATORY .F.
#DEFINE RETURN_DATA_NOT_MANDATORY .T.
#DEFINE CSVFORMAT 6
#DEFINE WRITE_ONLY_UNBUFFERED 11
#DEFINE COMMA ","

DEFINE CLASS UnionDuesReport AS CUSTOM

	cProcessName = 'UnionDuesReport'

	lTestMode = .F.  && .F. FOR NORMAL OPERATION, .T. TO SUPPRESS ALL BUT DEVELOPER EMAILS AND CHANGE INPUT TABLE, ETC.

	dToday = DATE()

	lAutoYield = .T.

	cStartTime = TTOC(DATETIME())

	* report properties
	lShowSalaries = .T.

	* connection properties
	nSQLHandle = 0

	* wait window properties
	nWaitWindowTimeout = 2
	lWaitWindowIsOn = .T.

	* logfile properties
	lLoggingIsOn = .T.
	cLogFile = 'F:\UTIL\ADPREPORTS\LOGFILES\Union Dues_REPORT_log.txt'

	* INTERNAL email properties
	lSendInternalEmailIsOn = .T.
	cFrom = 'TGF Corporate <fmicorporate@fmiint.com>'
	cSendTo = 'lucille.waldrip@tollgroup.com, marie.freiberger@tollgroup.com'
	cCC = 'mbennett@fmiint.com'
	cSubject = 'Union Dues Report for ' + DTOC(DATE())
	cAttach = ''
	cBodyText = ''

	cCOMPUTERNAME = ''
	cUSERNAME = ''


	FUNCTION INIT
		IF NOT DODEFAULT()
			RETURN .F.
		ENDIF
		WITH THIS
			*SET RESOURCE OFF
			CLOSE DATA
			SET CENTURY ON
			SET DATE AMERICAN
			SET HOURS TO 24
			SET ANSI ON
			SET TALK OFF
			SET DELETED ON
			SET CONSOLE OFF
			SET EXCLUSIVE OFF
			SET SAFETY OFF
			SET EXACT OFF
			SET STATUS BAR ON
			SET SYSMENU OFF
			SET ENGINEBEHAVIOR 70
			_VFP.AUTOYIELD = .lAutoYield
			.cCOMPUTERNAME = GETENV("COMPUTERNAME")
			.cUSERNAME = GETENV("USERNAME")
			IF .lTestMode THEN
				.cLogFile = 'F:\UTIL\ADPREPORTS\LOGFILES\Union Dues_REPORT_log_TESTMODE.txt'
				.cSendTo = 'mbennett@fmiint.com'
				.cCC = ''
			ENDIF
			.lLoggingIsOn = .lLoggingIsOn AND NOT EMPTY(.cLogFile)
			IF .lLoggingIsOn THEN
				SET ALTERNATE TO (.cLogFile) ADDITIVE
				SET ALTERNATE ON
			ENDIF
		ENDWITH
	ENDFUNC


	FUNCTION DESTROY
		WITH THIS
			IF .lLoggingIsOn  THEN
				SET ALTERNATE OFF
				SET ALTERNATE TO
			ENDIF
		ENDWITH
		DODEFAULT()
	ENDFUNC


	FUNCTION MAIN
		WITH THIS
			LOCAL lnNumberOfErrors, lcSQL, loError, ldToday, lcFiletoSaveAs, lcSpreadsheetTemplate
			LOCAL oExcel, oWorkbook, oWorksheet, ldStartDate, lcStartDate, ldEndDate, lcEndDate, lnRow, lcRow, lcSummaryFile 
			
			TRY
				lnNumberOfErrors = 0

				.TrackProgress("Union Dues Report process started....", LOGIT+SENDIT+NOWAITIT)
				.TrackProgress('COMPUTERNAME = ' + .cCOMPUTERNAME, LOGIT+SENDIT)
				.TrackProgress('USERNAME = ' + .cUSERNAME, LOGIT+SENDIT)
				.TrackProgress('PROJECT = ADPUNIONDUESRPT', LOGIT+SENDIT)
				IF .lTestMode THEN
					.TrackProgress('                        TEST MODE                        ', LOGIT+SENDIT)
				ENDIF
								
				ldToday = .dToday				
				*ldToday = {^2013-05-01}
				
				* this calc gives last day of prior month
				ldEndDate = ldToday - DAY(ldToday)
				
				* this calc gives first day of prior month
				ldStartDate = (ldEndDate + 1) - DAY(ldEndDate)

*!*	*!*				************************************************************
*!*	*!*				*** hardcode for special ranges
*!*				ldStartDate = {^2010-01-01}
*!*				ldEndDate = {^2010-06-30}
*!*	*!*				************************************************************
	
				.cSubject = "Union Dues Report for " + TRANSFORM(ldStartDate) + " - " + TRANSFORM(ldEndDate)
				
				* construct sql filters
				lcStartDate = "DATE'" + TRANSFORM(YEAR(ldStartDate)) + ;
					"-" + PADL(MONTH(ldStartDate),2,"0") + "-" + PADL(DAY(ldStartDate),2,"0") + "'" 

				lcEndDate = "DATE'" + TRANSFORM(YEAR(ldEndDate)) + ;
					"-" + PADL(MONTH(ldEndDate),2,"0") + "-" + PADL(DAY(ldEndDate),2,"0") + "'" 

				* get ss#s from ADP
				OPEN DATABASE F:\UTIL\ADPCONNECTION\CONNECT1

				.nSQLHandle = SQLCONNECT("PCPAYWIN","MBENNETT","VP737A")

				IF .nSQLHandle > 0 THEN

					lcSQL = ;
						"SELECT " + ;
						" NAME, " + ;
						" FILE# AS FILE_NUM, " + ;
						" CHECKVIEWPAYDATE AS PAYDATE, " + ;
						" CHECKVIEWDEDCODE AS DEDCODE, " + ;
						" {FN IFNULL(CHECKVIEWDEDAMT,0.00)} AS UNIONDUES " + ;
						" FROM REPORTS.V_CHK_VW_DEDUCTION " + ;
						" WHERE CHECKVIEWDEDCODE = '25' " + ;
						" AND CHECKVIEWPAYDATE >= " + lcStartDate + ;
						" AND CHECKVIEWPAYDATE <= " + lcEndDate + ;
						" ORDER BY NAME, CHECKVIEWPAYDATE "

					IF .lTestMode THEN
						.TrackProgress('                        TEST MODE                        ', LOGIT+SENDIT)
						.TrackProgress('lcSQL =' + lcSQL, LOGIT+SENDIT)
					ENDIF

					IF USED('SQLCURSOR1') THEN
						USE IN SQLCURSOR1
					ENDIF

					IF .ExecSQL(lcSQL, 'SQLCURSOR1', RETURN_DATA_MANDATORY) THEN

						*SELECT SQLCURSOR1 
						*BROW

						lcSpreadsheetTemplate = "F:\UTIL\ADPREPORTS\UnionDuesTemplate.XLS"
						lcFiletoSaveAs = "F:\UTIL\ADPREPORTS\REPORTS\UnionDuesDetail_" + STRTRAN(DTOC(ldStartDate),"/","-") + "-" + STRTRAN(DTOC(ldEndDate),"/","-") + ".XLS"
						lcSummaryFile = "F:\UTIL\ADPREPORTS\REPORTS\UnionDuesSummary_" + STRTRAN(DTOC(ldStartDate),"/","-") + "-" + STRTRAN(DTOC(ldEndDate),"/","-") + ".XLS"

						IF FILE(lcFiletoSaveAs) THEN
							DELETE FILE (lcFiletoSaveAs)
						ENDIF
						
						IF USED('CURSUMMARY') THEN
							USE IN CURSUMMARY
						ENDIF
						
						* ROLL UP DETAIL INTO SUMMARY FILE
						SELECT NAME, FILE_NUM, SUM(UNIONDUES) AS UNIONDUES ;
							FROM SQLCURSOR1 ;
							INTO CURSOR CURSUMMARY ;
							GROUP BY NAME, FILE_NUM ;
							ORDER BY NAME ;
							READWRITE
							
						SELECT CURSUMMARY
						COPY TO (lcSummaryFile) XL5						

						***********************************************************************************
						** output to Excel spreadsheet
						.TrackProgress('Creating spreadsheet: ' + lcFiletoSaveAs, LOGIT+SENDIT+NOWAITIT)

						oExcel = CREATEOBJECT("excel.application")
						oExcel.VISIBLE = .F.
						oExcel.DisplayAlerts = .F.
						oWorkbook = oExcel.workbooks.OPEN(lcSpreadsheetTemplate)
						oWorkbook.SAVEAS(lcFiletoSaveAs)

						oWorksheet = oWorkbook.Worksheets[1]
						oWorksheet.RANGE("A3","F200").ClearContents()
						oWorksheet.RANGE("A1").VALUE = "Union Dues Report for " + TRANSFORM(ldStartDate) + " - " + TRANSFORM(ldEndDate)

						lnRow = 2
						SELECT SQLCURSOR1
						SCAN

							lnRow = lnRow + 1
							lcRow = ALLTRIM(STR(lnRow))

							oWorksheet.RANGE("A"+lcRow).VALUE = SQLCURSOR1.NAME
							oWorksheet.RANGE("D"+lcRow).VALUE = "'" + TRANSFORM(SQLCURSOR1.FILE_NUM)
							oWorksheet.RANGE("C"+lcRow).VALUE = SQLCURSOR1.PAYDATE
							oWorksheet.RANGE("D"+lcRow).VALUE = "'" + SQLCURSOR1.DEDCODE
							oWorksheet.RANGE("E"+lcRow).VALUE = SQLCURSOR1.UNIONDUES

						ENDSCAN

						* SAVE AND QUIT EXCEL
						oWorkbook.SAVE()
						oExcel.QUIT()

						.cBodyText = "See attached Union Dues Detail & Summary Reports." + ;
							CRLF + CRLF + "(do not reply - this is an automated report)" + ;
							CRLF + CRLF + "<report log follows>" + ;
							CRLF + .cBodyText

						IF FILE(lcFiletoSaveAs) THEN
							* attach output file to email
							.cAttach = lcFiletoSaveAs
						ELSE
							.TrackProgress('ERROR: unable to email spreadsheet: ' + lcFiletoSaveAs, LOGIT+SENDIT+NOWAITIT)
						ENDIF
						
						IF FILE(lcSummaryFile) THEN
							* attach output file to email
							IF EMPTY(.cAttach) THEN
								.cAttach = lcSummaryFile
							ELSE
								.cAttach = .cAttach + "," + lcSummaryFile
							ENDIF
						ELSE
							.TrackProgress('ERROR: unable to email spreadsheet: ' + lcSummaryFile, LOGIT+SENDIT+NOWAITIT)
						ENDIF

						CLOSE DATABASES ALL
					ENDIF  &&  .ExecSQL(lcSQL, 'SQLCURSOR1', RETURN_DATA_MANDATORY)

					.TrackProgress("Union Dues Report process ended normally.", LOGIT+SENDIT+NOWAITIT)
				ELSE
					* connection error
					.TrackProgress('Unable to connect to ADP Payroll System.', LOGIT+SENDIT)
				ENDIF   &&  .nSQLHandle > 0

			CATCH TO loError

				.TrackProgress('There was an error.',LOGIT+SENDIT)
				.TrackProgress('The Error # is: ' + TRANSFORM(loError.ERRORNO), LOGIT+SENDIT)
				.TrackProgress('The Error Message is: ' + TRANSFORM(loError.MESSAGE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred in Program: ' + TRANSFORM(loError.PROCEDURE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred at line #: ' + TRANSFORM(loError.LINENO), LOGIT+SENDIT)
				lnNumberOfErrors = lnNumberOfErrors + 1
				IF TYPE('oWorkbook') = "O" THEN
					oWorkbook.SAVE()
				ENDIF
				IF TYPE('oExcel') = "O" THEN
					oExcel.QUIT()
				ENDIF
				CLOSE DATABASES ALL

			ENDTRY

			CLOSE DATA
			WAIT CLEAR
			***************** INTERNAL email results ******************************
			.TrackProgress('About to send status email.',LOGIT)
			.TrackProgress('The logfile is: ' + .cLogFile, SENDIT)
			.TrackProgress("Union Dues Report process started: " + .cStartTime, LOGIT+SENDIT)
			.TrackProgress("Union Dues Report process finished: " + TTOC(DATETIME()), LOGIT+SENDIT)

			IF .lSendInternalEmailIsOn THEN
				DO FORM dartmail2 WITH .cSendTo,.cFrom,.cSubject,.cCC,.cAttach,.cBodyText,"A"
				.TrackProgress('Sent status email.',LOGIT)
			ELSE
				.TrackProgress('Did not send status email: THIS.lSendInternalEmailIsOn = FALSE.',LOGIT)
			ENDIF

		ENDWITH
		RETURN
	ENDFUNC && main


	FUNCTION ExecSQL
		LPARAMETERS tcSQL, tcCursorName, tlNoDataReturnedIsOkay
		LOCAL llRetval, lnResult
		WITH THIS
			* close target cursor if it's open
			IF USED(tcCursorName)
				USE IN (tcCursorName)
			ENDIF
			WAIT WINDOW "Fetching remote data; please wait..." NOWAIT
			lnResult = SQLEXEC(.nSQLHandle, tcSQL, tcCursorName)
			llRetval = ( lnResult > 0 )
			IF llRetval THEN
				* see if any data came back
				IF NOT tlNoDataReturnedIsOkay THEN
					IF (NOT USED(tcCursorName)) OR EOF(tcCursorName) THEN
						llRetval = .F.
						.TrackProgress('No data was returned by this query: ' +CRLF + tcSQL, LOGIT+SENDIT)
						.cSendTo = 'Mark Bennett <mbennett@fmiint.com>'
					ENDIF
				ENDIF
			ELSE
				.TrackProgress("Result Code [" + TRANSFORM(lnResult) + "] returned by this query: " + tcSQL, LOGIT+SENDIT)
				.cSendTo = 'Mark Bennett <mbennett@fmiint.com>'
			ENDIF
			WAIT CLEAR
			RETURN llRetval
		ENDWITH
	ENDFUNC


	PROCEDURE TrackProgress
		* do any combination of Wait Window, writing to logfile, and adding to body of email,
		* based on nFlags parameter.
		LPARAMETERS tcExpression, tnFlags
		WITH THIS
			IF BITAND(tnFlags,LOGIT) = LOGIT THEN
				IF .lLoggingIsOn THEN
					?
					? .cProcessName + "  " + TTOC(DATETIME()) + ": " + tcExpression
				ENDIF
			ENDIF
			IF .lWaitWindowIsOn THEN
				IF BITAND(tnFlags,NOWAITIT) = NOWAITIT THEN
					WAIT WINDOW tcExpression NOWAIT
				ENDIF
				IF BITAND(tnFlags,WAITIT) = WAITIT THEN
					WAIT WINDOW tcExpression TIMEOUT .nWaitWindowTimeout
				ENDIF
			ENDIF
			IF BITAND(tnFlags,SENDIT) = SENDIT THEN
				IF .lSendInternalEmailIsOn THEN
					.cBodyText = .cBodyText + tcExpression + CRLF + CRLF
				ENDIF
			ENDIF
		ENDWITH
	ENDPROC  &&  TrackProgress

ENDDEFINE

