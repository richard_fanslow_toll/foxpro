CLOSE DATABASES ALL
WITH _SCREEN
	.AUTOCENTER = .T.
	.WINDOWSTATE = 0
	.BORDERSTYLE = 1
	.WIDTH = 260
	.HEIGHT = 210
	.TOP = 570
	.LEFT = 660
	.CLOSABLE = .F.
	.MINBUTTON = .F.
	.MAXBUTTON = .F.
	.SCROLLBARS = 0
	.CAPTION = "AUX FILES EDI POLLER"
ENDWITH

SELECT 0
USE F:\3pl\DATA\edipollertrip SHARED ALIAS eaux
REPLACE eaux.aux_edi WITH .F. IN eaux
USE IN eaux

TRY
	PUBLIC lSkipError
	lSkipError = .F.
	DO m:\dev\prg\_setvars

	SET SYSMENU OFF
	ON ESCAPE CANCEL
	SELECT 0
	USE F:\3pl\DATA\last945time ALIAS u_last
	REPLACE checkflag WITH .T. FOR poller = "AUX-EDI"
	REPLACE tries WITH 0 FOR poller = "AUX-EDI"
	USE IN u_last
	DO FORM m:\dev\frm\edi_jobhandler_aux-edi

CATCH TO oErr
	IF !lSkipError
		ASSERT .F. MESSAGE "AT CATCH SECTION"
		tfrom    ="TGF EDI Processing Center <tgf-warehouse-ops@tollgroup.com>"
		tsubject = "MISC AUX EDI Poller Error ("+TRANSFORM(oErr.ERRORNO)+") at "+TTOC(DATETIME())
		tattach  = ""
		SELECT 0
		USE F:\3pl\DATA\mailmaster ALIAS mm SHARED
		LOCATE FOR edi_type = "MISC" AND taskname = "GENERAL"
		lUseAlt = IIF(mm.use_alt,.T.,.F.)
		tsendto  = IIF(lUseAlt,mm.sendtoalt,mm.sendto)
		tcc  = IIF(lUseAlt,mm.ccalt,mm.cc)
		USE IN mm
		tmessage = "MISC AUX EDI Poller Major Error..... Please fix me........!"
		lcSourceMachine = SYS(0)

		tmessage =tmessage+ "Try/CATCH Exeception Message:"+CHR(13)+;
			[  Error: ] + STR(oErr.ERRORNO) +CHR(13)+;
			[  LineNo: ] + STR(oErr.LINENO) +CHR(13)+;
			[  Message: ] + oErr.MESSAGE +CHR(13)+;
			[  Procedure: ] + oErr.PROCEDURE +CHR(13)+;
			[  Details: ] + oErr.DETAILS +CHR(13)+;
			[  StackLevel: ] + STR(oErr.STACKLEVEL) +CHR(13)+;
			[  LineContents: ] + oErr.LINECONTENTS+CHR(13)+;
			[  UserValue: ] + oErr.USERVALUE+CHR(13)+;
			[  Computer:  ] +lcSourceMachine+CHR(13)
		tmessage = tmessage+CHR(13)+"Originating PRG: "+cPrgName

		DO FORM m:\dev\FRM\dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
	ENDIF
	SELECT 0
	USE F:\3pl\DATA\edipollertrip SHARED ALIAS eaux
	REPLACE eaux.aux_edi WITH .T.
FINALLY
	CLOSE DATABASES ALL
	SET COVERAGE TO
	ON ERROR
ENDTRY
