*!* Under Armour "214"/UAP Delivery Status notification
PARAMETERS cPO_Num,nWO_Num,cOffice,lcEventCode,tEventTime,lIsError,cStatus
PUBLIC cErrMsg,cPrgName

WAIT WINDOW "" TIMEOUT 2
cPrgName = "UA 214TL"

lTesting = .f.
lDoCatch = .T.
lIsError = .t.
cErrMsg = ""
cStatus = ""
*IF lTesting
*SET STEP ON
*ENDIF

DO m:\dev\prg\_setvars WITH lTesting
SET CENTURY ON

TRY

	IF VARTYPE(lcEventCode)#"C" OR lTesting
		CLEAR
		lcEventCode = "UAPS"
		cPO_Num = "4500459534"
		nWO_Num =    0
		cOffice = "C"
		tEventTime = DATETIME()-600
	ENDIF

	cMailName = "Under Armour"
	cPO_Num = ALLT(cPO_Num)
	IF "^"$cPO_Num
		cPO_Num = LEFT(TRIM(cPO_Num),AT("^",cPO_Num)-1)
	ENDIF
	l214Type = IIF(LEFT(lcEventCode,3) = '214',.T.,.F.)

*!* lcEventCode Potential values: UAPC, UAPS, UAPA,214A,214P
	lcEventName = ICASE(lcEventCode = "214A","UA AVAILABLE",lcEventCode = "214P","UA PICKUP",lcEventCode = "UAPC","UA PO CALLED",lcEventCode = "UAPS","UA PO STAGED","UA PO APPT")
	lcEventCodeUsed = ICASE(lcEventCode = "214A","2330",lcEventCode = "214P","2335",lcEventCode = "UAPC","1500",lcEventCode = "UAPS","1505","1510")  && 1510 = UAPA

	lTSuff = IIF(lTesting,"_T","")
	lFilesOut = !lTesting
	lTestcounter = lTesting
*lFilesOut = .F.
	lDoMail = .T.
	lTestmail = lTesting
	nAcctNum = 5687

	IF USED('mailmaster')
	USE IN mailmaster
	ENDIF
	
	SELECT 0
	USE F:\3pl\DATA\mailmaster SHARED ALIAS mm
	IF lTestmail OR lTesting
		LOCATE FOR edi_type = "MISC" AND taskname = "GENERAL"
	ELSE
		LOCATE FOR edi_type = "214" AND accountid = nAcctNum AND office = cOffice
	ENDIF
	tsendto = ALLTRIM(IIF(mm.use_alt,mm.sendtoalt,mm.sendto))
	tcc = ALLTRIM(IIF(mm.use_alt,mm.ccalt,mm.cc))
	LOCATE FOR mm.office= "X" AND mm.accountid = 9999
	tsendtoerr = ALLTRIM(IIF(mm.use_alt,mm.sendtoalt,mm.sendto))
	tccerr = ALLTRIM(IIF(mm.use_alt,mm.ccalt,mm.cc))
	USE IN mm
	cOffice = ICASE(INLIST(cOffice,'6','R'),'C',INLIST(cOffice,"I","N"),"I",cOffice)

	IF l214Type
		*SET STEP ON
		IF USED('wolog')
			USE IN wolog
		ENDIF
		USE F:\wo\wodata\wolog IN 0 ALIAS wolog
		IF SEEK(nWO_Num,'wolog','wo_num')
			cContainerID = ALLTRIM(wolog.CONTAINER)
      If wolog.type = "A"
         cContainerID = strtran(ALLTRIM(wolog.awb),"-","")
      Endif 
  	ELSE
			cErrMsg ="Container not found for WO "+TRANSFORM(nWO_Num)
			WAIT WINDOW cErrMsg TIMEOUT 2
			THROW
		ENDIF
		USE IN wolog
		cWO_Num= ALLTRIM(STR(nWO_Num))
	ENDIF


	cCustName = "UNDERARMOUR"
	cCustPrefix = "UA"
	cDocID = "9999"

	IF !USED('pcounter')
		SET STEP ON 
		SELECT 0
		IF lTestcounter && OR lTesting
			USE "f:\3pl\data\ua\pcounter"
			COPY TO "m:\joeb\data\ua\pcounter"
			USE
			USE "m:\joeb\data\ua\pcounter" ALIAS pcounter
		ELSE
			USE "f:\3pl\data\ua\pcounter" ALIAS pcounter
		ENDIF
	ENDIF
	cDocID = PADL(ALLTRIM(STR(pcounter.countnum)),9,"0")
	REPLACE pcounter.countnum WITH pcounter.countnum+1 IN pcounter

	SET DATE YMD
	lcTStamp = STRTRAN(TTOC(DATETIME()),"/","-")
	lcTStamp = STRTRAN(lcTStamp," ","T")
	lcTStamp = STRTRAN(lcTStamp,"TPM","")
	lcTStamp = ALLTRIM(STRTRAN(lcTStamp,"TAM",""))

	lcEventTime = STRTRAN(TTOC(tEventTime),"/","-")
	lcEventTime = STRTRAN(lcEventTime," ","T")
	lcEventTime = STRTRAN(lcEventTime,"TPM","")
	lcEventTime = ALLTRIM(STRTRAN(lcEventTime,"TAM",""))

* Read in the XML header template
	IF INLIST(lcEventCode,"214A","214P")
		lcHeader = FILETOSTR("f:\underarmour\ship214_header.txt")
	ELSE
		lcHeader = FILETOSTR("f:\underarmour\shipstat_header.txt")
	ENDIF
	IF lTesting
*	?lcHeader
	ASSERT .F. MESSAGE ">>DEBUG HERE<<"
	ENDIF


	cPO_ID = cPO_Num+PADL(TRANSFORM(INT(RAND()*10000)),4,"0")
* Replace all the predefined data elements in header
	lcHeader = STRTRAN(lcHeader,"PO_NUMBER",cPO_Num)
	lcHeader = STRTRAN(lcHeader,"TSTAMP",lcTStamp)
	lcHeader = STRTRAN(lcHeader,"PO_ID",cPO_ID)
	lcHeader = STRTRAN(lcHeader,"EVENTTIME",lcEventTime)
	lcHeader = STRTRAN(lcHeader,"EVENTCODE",lcEventCodeUsed)
	lcHeader = STRTRAN(lcHeader,"EVENTNAME","")
	lcHeader = STRTRAN(lcHeader,"HUBFMI",ICASE(cOffice="M","HUBFMI2",cOffice="N","HUBFMI3","HUBFMI"))
*!* For 214 file types:
	IF l214Type
		lcHeader = STRTRAN(lcHeader,"CONTAINERID",cContainerID)
	ENDIF

	IF lTesting
		SET STEP ON
	ENDIF

	nRounds = IIF(lcEventCode = '214P' AND INLIST(cOffice,"N","I","M"),2,1)
	FOR pRounds = 1 TO nRounds
		IF l214Type
			IF pRounds = 2  && NJ & FL only
				lcHeader = STRTRAN(lcHeader,"2335","2904")  && Runs a 2nd 214 with "Carrier in Yard" code 2904
				cDocID = PADL(ALLTRIM(STR(pcounter.countnum)),9,"0")
				REPLACE pcounter.countnum WITH pcounter.countnum+1 IN pcounter
			ELSE
			ENDIF
			cFilename = "CARRIER"+ALLTRIM(cWO_Num)+"_"+cDocID+"_"+TTOC(DATETIME(),1)+"stat"+lTSuff+".xml"
		ELSE
			cFilename = "P"+ALLTRIM(cPO_Num)+"_"+cDocID+"_"+TTOC(DATETIME(),1)+"stat"+lTSuff+".xml"
		ENDIF

		cFilenameHold = ("f:\ftpusers\"+cCustName+"\214-Staging\"+cFilename)
		WAIT WINDOW "" TIMEOUT 1

		STRTOFILE(lcHeader,cFilenameHold)

		DO CASE
			CASE lTesting
				cArchiveFile = ("f:\ftpusers\"+cCustName+"\testtpmshipments\archive\"+cFilename)
				cFilenameOut = ("f:\ftpusers\underarmour\214-Staging\"+cFilename)
			CASE l214Type
				cArchiveFile = ("f:\ftpusers\"+cCustName+"\tpmshipments\214Carrierarchive\"+cFilename)
				cFilenameOut = ("f:\ftpusers\underarmour\events\"+cFilename)
			OTHERWISE
				cArchiveFile = ("f:\ftpusers\"+cCustName+"\tpmshipments\UAParchive\"+cFilename)
				cFilenameOut = ("f:\ftpusers\underarmour\tpmshipments\"+cFilename)
		ENDCASE

		STRTOFILE(lcHeader,cArchiveFile)

		IF lFilesOut OR lTesting
			*SET STEP ON
			STRTOFILE(lcHeader,cFilenameOut)
			IF !lTesting
			DELETE FILE [&cFilenameHold]
			endif
		ENDIF

		IF !lTesting
			ediupdate(.F.,"214 CREATED (STATUS)")
		ELSE
			CLEAR
		ENDIF
	ENDFOR
	RELEASE lcHeader
	SET DATE AMERICAN
	WAIT WINDOW "214 Status file ("+lcEventCode+") complete for PO# "+cPO_Num TIMEOUT 1
	lDoCatch = .F.

CATCH TO oErr
	IF lDoCatch
		SET STEP ON
*		ASSERT .F. MESSAGE "In CATCH...debug"
		lEmail = .T.
		DO ediupdate WITH .T.,IIF(EMPTY(cErrMsg),"TRY/CATCH ERROR",cErrMsg)
		tsubject = cMailName+" 214 Error ("+TRANSFORM(oErr.ERRORNO)+") at "+TTOC(DATETIME())
		tattach  = ""
		tmessage = cCustName+" Error processing "+CHR(13)
		tmessage = tmessage+TRIM(PROGRAM())+CHR(13)

		tmessage =tmessage+ "Try/CATCH Exeception Message:"+CHR(13)+;
			[  Error: ] + STR(oErr.ERRORNO) +CHR(13)+;
			[  LineNo: ] + STR(oErr.LINENO) +CHR(13)+;
			[  Message: ] + oErr.MESSAGE +CHR(13)+;
			[  Procedure: ] + oErr.PROCEDURE +CHR(13)+;
			[  Details: ] + oErr.DETAILS +CHR(13)+;
			[  StackLevel: ] + STR(oErr.STACKLEVEL) +CHR(13)+;
			[  LineContents: ] + oErr.LINECONTENTS+CHR(13)+;
			[  UserValue: ] + oErr.USERVALUE

		tsubject = "214 Process Error at "+TTOC(DATETIME())
		tattach  = ""
		tcc=""
		tfrom    ="FMI EDI 214 Process Ops <fmi-warehouse-ops@fmiint.com>"
		DO FORM dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
		lEmail = .F.
	ENDIF
FINALLY
*	closefiles()
ENDTRY

****************************
PROCEDURE ediupdate
****************************
	PARAMETERS lIsError,cFin
	SET STEP ON 
	*ASSERT .F. MESSAGE "In EDIUPDATE...DEBUG"
	IF lIsError
		ASSERT .F. MESSAGE "Error found...debug"
	ENDIF

	SELECT edi_trigger
	LOCATE

	IF !lTesting
		IF lIsError
			WAIT WINDOW "Error in 214TL for PO# "+cPO_Num TIMEOUT 1
			REPLACE processed WITH .T.,fin_status WITH cFin,errorflag WITH .T.,created WITH .F. ;
				FOR ship_ref = cPO_Num AND edi_type = lcEventCode AND accountid = nAcctNum ;
				AND !processed IN edi_trigger

			BLANK FIELDS edi_trigger.file214 ;
				FOR ship_ref = cPO_Num AND edi_type = lcEventCode AND accountid = nAcctNum ;
				AND !processed IN edi_trigger

		ELSE
			IF l214Type
			REPLACE processed WITH .T.,fin_status WITH cFin,when_proc WITH DATETIME(),errorflag WITH .F.,;
				created WITH .T.,file214 WITH JUSTFNAME(cArchiveFile),trig_by WITH "PROCUA214" ;
				FOR wo_num = nWO_Num AND edi_type = lcEventCode AND accountid = nAcctNum ;
				AND !processed IN edi_trigger
			else
			REPLACE processed WITH .T.,fin_status WITH cFin,when_proc WITH DATETIME(),errorflag WITH .F.,;
				created WITH .T.,file214 WITH cArchiveFile,trig_by WITH "PROCUA214" ;
				FOR ship_ref = cPO_Num AND edi_type = lcEventCode AND accountid = nAcctNum ;
				AND !processed IN edi_trigger
			endif
			mailproc()
		ENDIF
		LOCATE
	ENDIF
	SET DATE AMERICAN
ENDPROC

*******************
PROCEDURE mailproc
*******************
	tfrom = "FMI EDI Operations <fmi-edi-ops@fmiint.com>"
	tattach = ""
	IF lTesting
		tsubject = "UnderArmour *TEST/MANUAL* 214 (PP) Created at "+TTOC(DATETIME())
	ELSE
		tsubject = "UnderArmour "+IIF(l214Type,"Carrier","214")+" (TRANSLOAD) Created at "+TTOC(DATETIME())+"Type: "+lcEventCode
	ENDIF
	tmessage = "214 (TL) Shipment file "+JUSTFNAME(cFilename)+CHR(13)
	tmessage = tmessage+CHR(13)+"for Office "+cOffice+", "+IIF(l214Type,"WO # "+cWO_Num,"PO # "+cPO_Num)
	tmessage = tmessage+CHR(13)+"has been created and will be transmitted shortly."

	IF lDoMail
		DO FORM m:\dev\frm\dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
	ENDIF
	cAddMsg = ""
ENDPROC
