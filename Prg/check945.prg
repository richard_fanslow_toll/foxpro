parameters lltomeonly
*!*	IF INLIST(DOW(DATE()),1,7)
*!*		WAIT WINDOW "Weekend...no reports run" TIMEOUT 2
*!*		RETURN
*!*	ENDIF

tfrom ="TGF EDI System Operations <toll-edi-ops@tollgroup.com>"

if vartype(lltomeonly) = "U"
	lltomeonly = .t.
else
	if vartype(lltomeonly) = "C"
		if lltomeonly = "T"
			lltomeonly = .t.
		else
			lltomeonly = .f.
		endif
	endif
endif

close data all
clear all
do m:\dev\prg\_setvars
set century on
on escape cancel
set hours to 24
do lookups
public tcc,coresendgroup,cacctname,csubname,tsendto2,tsubject,crpttitle,cacctlist,usesub
public lissuesonly,lexceptionsonly,ldoemail

ldoemail = .t.
lissuesonly = .t.
lexceptionsonly = .t.

tcc = " "
*gsystemmodule="PL"

thisuser=getuserid()
thisuser = "JBIANCHI"
lltomeonly = .t.

use f:\3pl\data\check945accounts in 0 alias ckaccts

ccoresendgroup = "pgaidis@fmiint.com,joe.bianchi@tollgroup.com"
store ccoresendgroup to tsendto

if lltomeonly
	do case
	case upper(thisuser) = "JBIANCHI"
		tsendto = "joe.bianchi@tollgroup.com"
	case upper(thisuser) = "PAULG"
		tsendto = "pgaidis@fmiint.com"
	otherwise
		tsendto = "pgaidis@fmiint.com"
	endcase
	wait window at 10,10 "Report going to "+tsendto timeout 2
else
	wait window at 10,10 "Report going to default addresses" timeout 2
endif

select ckaccts
assert .f. message "At start of accounts scan"
scan
	replace lastrun with datetime() in ckaccts next 1
	scatter memvar fields except lastrun
	cacctname = allt(m.acctname)
	csubname = allt(m.subname)
	msgstr = allt(cacctname)+iif(usesub," ("+csubname+")","")
	wait window "Now processing "+msgstr timeout 2
	store m.acctnum to accountnumber
	store m.acctnum to lnacctnum
	cacctlist = allt(str(m.acctnum))
	store m.acctnum2 to accountnumber2
	store m.acctnum3 to accountnumber3
	for zz = 2 to 3
		zz1 = allt(str(zz))
		if m.acctnum&zz1 <> m.acctnum
			cacctlist = cacctlist+","+allt(str(m.acctnum&zz1))
		endif
	endfor
	if lltomeonly
		tcc = ""
	else
		tcc = allt(m.tsendto2)
	endif
	tsubject= csubname+" 945-EDI Status Report"
	crpttitle = csubname+" - 940/945-EDI Status Report"

	lcwhpath = wf("C",lnacctnum)
	goffice = wf("C",lnacctnum,,,,,,.t.)

	if !used('edi_trigger')
		cedifolder = iif(date()<{^2010-01-24},"F:\JAG3PL\DATA\","F:\3PL\DATA\")
		use (cedifolder+"edi_trigger") in 0 alias edi_trigger
	endif
	if !used('pthist')
		cedifolder = iif(date()<{^2010-01-24},"F:\EDIWHSE\DATA\","F:\3PL\DATA\")
		use (cedifolder+"PTHIST") in 0 alias pthist
	endif
	else
		use &lcwhpath.outship in 0 order acctpt
	endif

	use &lcwhpath.pt in 0 order acctpt

	lntime = 60
	select *, {  /  /  } as d_date,{  /  /  } as p_date,{  /  /  } as l_date,{  /  /  } as c_date,;
	{  /  /  } as a_date,"                   " as bol, "    " as scaccode, 0000000 as wo_num,{  /  /  } as t_date,;
	"                                           " as file945,{  /  /  } as trigdate,{  /  /  } as b_date,;
	{  /  /  } as s_date,"                   " as edistat,{  /  /  } as proc_date, 00000 as unitqty, 0000 as ctnqty;
	from pthist where inlist(accountid,&cacctlist) and  !inlist(edistatus,"945 CREATED","CANCELED","NOTHING SHIPPED","MANUALLY REMOVED") ;
	and dateloaded >= date()-30 ;
	group by pt ;
	into cursor pts readwrite

	select edi_trigger
	set order to acct_pt  &&SHIP_REF   && SHIP_REF

	select pts
	scan
		wait window at 10,10 "Checking "+pt nowait
		select edi_trigger
		if pts.pt='0360071'
			set step on
		endif
		seek alltrim(str(accountnumber))+alltrim(pts.pt)
		if !found()
			seek str(accountnumber2,4,0)+pts.pt
		endif
		if !found()
			seek str(accountnumber3,4,0)+pts.pt
		endif

		if found()
			replace pts.confirmed with .t.
			lcfname = justfname(edi_trigger.file945)
			replace pts.file945  with lcfname
			replace pts.trigdate  with edi_trigger.trig_time

			replace pts.proc_date with edi_trigger.when_proc
			replace pts.edistat  with edi_trigger.fin_status

			select pthist
			set order to acctpt
			seek str(accountnumber)+pts.pt
			if !found()
				seek str(accountnumber2,4,0)+pts.pt
			endif
			if !found()
				seek str(accountnumber3,4,0)+pts.pt
			endif
			if found()
				do while inlist(pthist.accountid,&cacctlist) and pthist.pt=pts.pt
					replace pthist.edistatus with edi_trigger.fin_status in pthist
					skip 1 in pthist
				enddo
			endif

				xsqlexec("select * from outship where accountid = accountnumber and ship_ref = )"+pts.pt,,,"wh")
				if reccount("outship") = 0
					xsqlexec("select * from outship where accountid = accountnumber2 and ship_ref = )"+pts.pt,,,"wh")
				endif
				if reccount("outship") = 0
					xsqlexec("select * from outship where accountid = accountnumber3 and ship_ref = )"+pts.pt,,,"wh")
				endif
				if reccount("outship")>0
					replace pts.wo_num with wo_num
					replace pts.d_date with nul2empty(del_date)
					replace pts.p_date with nul2empty(picked)
					replace pts.l_date with nul2empty(labeled)
					replace pts.s_date with nul2empty(staged)
					replace pts.a_date with nul2empty(appt)
					replace pts.b_date with nul2empty(cancel)
					replace pts.bol    with bol_no
					replace pts.scaccode with scac
					replace pts.unitqty  with qty
					replace pts.ctnqty   with ctnqty
					if empty(pts.d_date)
						replace pts.edistat with "WO Not Complete"
					endif
				else
					select pt
					seek str(accountnumber,4,0)+pts.pt
					if !found()
						seek str(accountnumber2,4,0)+pts.pt
					endif
					if !found()
						seek str(accountnumber3,4,0)+pts.pt
					endif
					if found()
						replace pts.unitqty  with qty
					endif
				endif
			else
					xsqlexec("select * from outship where accountid = accountnumber and ship_ref = )"+pts.pt,,,"wh")
					if reccount("outship") = 0
						xsqlexec("select * from outship where accountid = accountnumber2 and ship_ref = )"+pts.pt,,,"wh")
					endif
					if reccount("outship") = 0
						xsqlexec("select * from outship where accountid = accountnumber3 and ship_ref = )"+pts.pt,,,"wh")
					endif
					if reccount("outship")>0
					else
						select outship
						seek str(accountnumber,4,0)+pts.pt
						if !found()
							seek str(accountnumber2,4,0)+pts.pt
						endif
						if !found()
							seek str(accountnumber3,4,0)+pts.pt
						endif

						if found()
							replace pts.wo_num with wo_num
							replace pts.d_date with nul2empty(del_date)
							replace pts.p_date with nul2empty(picked)
							replace pts.l_date with nul2empty(labeled)
							replace pts.s_date with nul2empty(staged)
							replace pts.a_date with nul2empty(appt)
							replace pts.b_date with nul2empty(cancel)
							replace pts.bol    with bol_no
							replace pts.scaccode with scac
							replace pts.unitqty  with qty
							replace pts.ctnqty   with ctnqty
							if empty(pts.d_date)
								replace pts.edistat with "WO Not Complete"
							endif
						else
							select pt
							seek str(accountnumber,4,0)+pts.pt
							if !found()
								seek str(accountnumber2,4,0)+pts.pt
							endif
							if !found()
								seek str(accountnumber3,4,0)+pts.pt
							endif
							if found()
								replace pts.unitqty  with qty
							endif
						endif
					else
						select outship
						seek str(accountnumber,4,0)+pts.pt
						if !found()
							seek str(accountnumber2,4,0)+pts.pt
						endif
						if !found()
							seek str(accountnumber3,4,0)+pts.pt
						endif
						if found()
							replace pts.wo_num with wo_num
							replace pts.d_date with nul2empty(del_date)
							replace pts.p_date with nul2empty(picked)
							replace pts.l_date with nul2empty(labeled)
							replace pts.s_date with nul2empty(staged)
							replace pts.a_date with nul2empty(appt)
							replace pts.b_date with nul2empty(cancel)
							replace pts.bol    with bol_no
							replace pts.scaccode with scac
							replace pts.unitqty  with qty
							replace pts.ctnqty   with ctnqty
							if pts.unitqty = pts.ctnqty
								replace pts.ctnqty   with ctnqty
								replace pts.edistat with "WO Not Processed"
							else
								if empty(pts.d_date)
									replace pts.edistat with "WO Not Complete"
								else

								endif
							endif
						else
*      Set Step on
							select pt
							seek str(accountnumber,4,0)+pts.pt
							if !found()
								seek str(accountnumber2,4,0)+pts.pt
							endif
							if !found()
								seek str(accountnumber3,4,0)+pts.pt
							endif
							if found()
								replace pts.unitqty with pt.qty in pts
								replace pts.edistat with "On PT Screen" in pts
								replace pts.b_date  with pt.cancel in pts
							else
								replace pts.edistat with "Not in System" in pts
							endif
						endif
					endif
					lcfname = justfname(pts.filename)
					replace pts.filename with lcfname

				endscan

				select pts
				index on pt tag pt
				set order to pt


				select edi_trigger
				set order to acct_pt

				select pthist
				set order to acctpt

				select pts
				locate

				scan
					wait window at 10,10 "Checking "+pt nowait

					select edi_trigger
					seek alltrim(str(accountnumber))+alltrim(pts.pt)
					if !found()
						seek str(accountnumber2,4,0)+pts.pt
					endif
					if !found()
						seek str(accountnumber3,4,0)+pts.pt
					endif

					if found()
						select pthist
						seek str(accountnumber)+pts.pt
						if !found()
							seek str(accountnumber2,4,0)+pts.pt
						endif
						if !found()
							seek str(accountnumber3,4,0)+pts.pt
						endif
						if found()
							replace pthist.edistatus with edi_trigger.fin_status
							wait window at 10,10 "Updating "+pt nowait
						endif
					endif

					if pts.unitqty =0 and pts.ctnqty =0
						select pthist
						set order to acctpt
						seek str(accountnumber)+pts.pt
						if !found()
							seek str(accountnumber2,4,0)+pts.pt
						endif
						if !found()
							seek str(accountnumber3,4,0)+pts.pt
						endif
						if found()
							do while pthist.accountid=accountnumber and pthist.pt=pts.pt
								replace pthist.edistatus with "NOTHING SHIPPED"
								skip 1 in pthist
							enddo
						endif

					endif

				endscan


*ASSERT .f.

				wait window at 10,10 "Deleting where 945 was created............" nowait
				delete for edistat= "MANUALLY REMOVED"

				if lexceptionsonly = .t. and lissuesonly=.t.
					delete for (unitqty =0 and ctnqty =0)
				endif


				if lexceptionsonly = .t.
					wait window at 10,10 "Deleting where 945 was created............" nowait
					delete for edistat= "945 CREATED"
				endif

				if lexceptionsonly = .t. and lissuesonly= .f.
					wait window at 10,10 "Deleting where Nothing was shipped..........." nowait
					delete for edistat= "NOTHING SHIPPED"
				endif

				if lexceptionsonly = .t. and  lissuesonly = .f.
					wait window at 10,10 "Deleting PTS not in system..........." nowait
					delete for edistat= "Not in System"
				endif


				if lexceptionsonly = .t. and lissuesonly = .t.
					wait window at 10,10 "Deleting PTs only on the PT Screen............" nowait
					delete for edistat= "On PT Screen"
				endif


				select pts
				scan
					wait window at 10,10 "Checking SCAC Issues.............." nowait
					if !empty(d_date) and empty(pts.trigdate)
						replace pts.edistat with "No Trig.Ck SCAC"
					endif
				endscan


				if lissuesonly = .t.
					wait window at 10,10 "Deleting non-issues.............." nowait
					delete for edistat= "WO Not Complete"
					delete for edistat= "WO Not Processed"
				endif


*If lExceptionsOnly = .t.
*!*	  Delete For edistat= "WO Not Complete"
*!*	EndIf

				select pts
				locate
				if eof()
					wait window "No open 945s for Account "+alltrim(str(lnacctnum))+"...exiting" timeout 2
					no945records()
					use in outship
					use in pt
					use in edi_trigger
					use in pthist
					loop
				endif

				copy to h:\fox\ptstatus.xls type xls
				report_date= date()


				if ldoemail = .t.
					lcreportname = "nanjingrpt"
					lcfilename = "h:\fox\940_945_Status_Report"
					store  .f. to reportok

					do rpt2pdf with lcreportname,lcfilename,reportok
					try
						tattach = lcfilename
						lcsourcemachine = sys(0)
						tmessage = "See attached report file"+chr(13)+" This report was generated from machine: "+lcsourcemachine
						do form dartmail2 with tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"

						if accountnumber = 4610 and !lltomeonly
							tattach = "h:\fox\ptstatus.xls"
							tmessage = "See attached report file in XLS format "+chr(13)+" This report was generated from machine: "+lcsourcemachine
							select 0
							use f:\3pl\data\mailmaster alias mm shared
							locate for mm.edi_type = "MISC" and mm.taskname = "NANSTATRPT"
							tsendto = alltrim(iif(mm.use_alt,mm.sendtoalt,mm.sendto))
							tcc = alltrim(iif(mm.use_alt,mm.ccalt,mm.cc))
							use in mm
							do form dartmail2 with tsendto,tfrom,tsubject," ",tattach,tmessage,"A"
						endif
					catch
						tattach = ""
						tmessage = "Problem with the 945 check routine on"+lcsourcemachine
						tcc = ""
						do form dartmail2 with tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
					endtry
				else
					report form nanjingrpt preview
				endif

				if used('edi_trigger')
					use in edi_trigger
				endif
				if used('pthist')
					use in pthist
				endif
				if used('outship')
					use in outship
				endif
				if used('pt')
					use in pt
				endif

			endscan

			close databases all


********************************************************************************************************
procedure getuserid
********************************************************************************************************
local lcusername,lres
declare integer GetUserName in advapi32 string@, integer@

lcusername = replicate(chr(0),255)
lres = getusername(@lcusername,255)

if lres # 0 then
	return left(lcusername,at(chr(0),lcusername)-1)
*    RETURN LEFT(lcusername,AT(CHR(0),lcusername)-1)
else
	return "UNK"
endif
endproc
** "End of  for" getuserid



********************************************************************************************************
procedure no945records
********************************************************************************************************

ccustomer = proper(cacctname)+iif(usesub," ("+proper(csubname)+")","")
tsubject = "No Open 945s for Customer: "+ccustomer
tattach = ""
tmessage = "There are no open 945s for this account, no PDF produced"
do form dartmail2 with tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
endproc
