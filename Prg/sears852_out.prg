*** this program is no longer used; use SEARS852PROC instead.
RETURN



************************************************************************
* AUTHOR--:PG
* DATE----:05/01/2010
* Modified much, now this takes in flat files and sends out 852, product activity data for Sears
************************************************************************

#DEFINE CR CHR(13)
#DEFINE CRLF CHR(13)+CHR(10)

PRIVATE plTesting
plTesting = .T.

DO setenvi
SET CENTURY ON
SET DATE TO ymd
SET DECIMAL TO 0

IF plTesting THEN
	xoutdir = "C:\SEARSEDI\EDIDATAOUT\"
ELSE
	xoutdir="f:\ftpusers\searsedi\EdiDataOut\"
ENDIF

CLOSE DATA ALL
STORE CHR(07) TO segterminator
STORE "~"+CHR(13) TO segterminator

*Store "" to segterminator

STORE "" TO fstring
STORE 0 TO shiplevel

PUBLIC t852, tfile, tshipid,cstring,slevel,plevel,lnInterControlnum,lcInterctrlNum,stNum,gn852Ctr
PUBLIC lcCode,lcOrigin,lcDischarge, lcDocrcpt

lcCode = "UNKN"
lcOrigin= "UNK"
lcDischarge= "UNK"
lcDocrcpt = "UNK"

STORE 0 TO gn852Ctr,stNum,lncttcount,lnsecount
STORE 0 TO hlcount2,hlpack,spcntp,hlsegcnt,segcnt,spcnt,spcnt2,hlitem,spcnti,lc_hlcount,a,lincnt,totlincnt,sesegcnt,segcnt

STORE "" TO workordersprocessed,summaryinfo,lc_852num,lc_cnum

* top level counter for unique HL segment counts
STORE 0 TO hlctr

* use these to store overall counters
STORE 0 TO shipmentlevelcount,orderlevelcount,packlevelcount,itemlevelcount

* use these to store overall counters
STORE 0 TO thisshipmentlevelcount,thisorderlevelcount,thispacklevelcount,thisitemlevelcount

* used to store the value of the curent count for remembering the Parent HL Count
STORE 0 TO currentshipmentlevelcount,currentorderlevelcount,currentpacklevelcount,currentitemlevelcount

STORE .F. TO llEditUpdate


CREATE CURSOR shipdata (;
	bol CHAR(16),;
	TYPE CHAR(3),;
	actcode CHAR(2),;
	CONTAINER CHAR(12),;
	duns CHAR(16),;
	rptloc CHAR(10),;
	ponum CHAR(16),;
	dept CHAR(6),;
	markfor CHAR(10),;
	vendorid CHAR(6),;
	scac CHAR(4),;
	qty N(7),;
	ITEM  CHAR(20),;
	sku CHAR(20))

lcstring  = ""
lincnt = 1
*******************************************************************************************
jtimec = TIME()
STORE SUBSTR(jtimec,1,2) TO j1
STORE SUBSTR(jtimec,4,2) TO j2
STORE SUBSTR(jtimec,7,2) TO j3
lcTime = j1+j2
tday = DAY(DATE())
STORE STR(tday,2,0)     TO tday1
STORE DTOC(DATE())      TO hdate
STORE SUBSTR(hdate,6,2) TO tmon
STORE SUBSTR(hdate,9,2) TO tday
STORE SUBSTR(hdate,1,2) TO tcent
STORE SUBSTR(hdate,3,2) TO tyr
STORE SUBSTR(hdate,1,4) TO twholeyr
STORE SUBSTR(hdate,1,2) TO d4
STORE SUBSTR(hdate,3,2) TO d1
STORE SUBSTR(hdate,6,2) TO d2
STORE SUBSTR(hdate,9,2) TO d3

***************************
*** Build Header Record ***
***************************
tsendid   = 'FMI'
*tsendid    = 'LOGNET'
tsendid    = PADR(tsendid,12," ")
tsendcode  = '6111250084'
******************************
tqualcode = '6111250084'
tqualcode  = PADR(tqualcode,15," ")
tqualtype = '08'

WAIT WINDOW "Open tables and setting up........." NOWAIT
*******************************************************************************************
STORE 0 TO EDI_testing
USE F:\searsedi\DATA\controlnum IN 0 ALIAS controlnum

********************************************************************************************************
**********************************************
*** Build Hierarchical Level *** SHIPMENT LEVEL
***********************************************
WAIT WINDOW "Setting up the cursors......." NOWAIT
IF plTesting THEN
	lcpath = "C:\SEARSEDI\RGTIDATAIN\"
	lcarchivepath = "C:\SEARSEDI\RGTIDATAIN\ARCHIVE\"
ELSE
	lcpath ="f:\ftpusers\searsedi\RGTIdataIn\"
	lcarchivepath ="f:\ftpusers\searsedi\RGTIdataIn\archive\"
ENDIF

CD &lcpath

Start852()
gn852Ctr =0

lnNum = ADIR(tarray,"852*.txt")

IF lnNum > 0 THEN

	FOR ii = 1 TO 4

		DO CASE
			CASE ii = 1
				lnNum = ADIR(tarray,"852_rec*.txt")
			CASE ii = 2
				lnNum = ADIR(tarray,"852_inv*.txt")
			CASE ii = 3
				lnNum = ADIR(tarray,"852_dis*.txt")
			CASE ii = 4
				lnNum = ADIR(tarray,"852_adj*.txt")
		ENDCASE

		IF lnNum > 0
			SELECT shipdata
			SET FILTER TO
			SET ORDER TO
			ZAP
			FOR thisfile = 1  TO lnNum
				xfile = lcpath+tarray[thisfile,1]
				archivefile = lcarchivepath+tarray[thisfile,1]
				WAIT WINDOW "Importing file: "+xfile NOWAIT
				lnHandle = FOPEN(xfile)
				DO WHILE !FEOF(lnHandle)
					lcStr= FGETS(lnHandle)
					SELECT shipdata
					APPEND BLANK
					REPLACE shipdata.bol   WITH ALLTRIM(SUBSTR(lcStr,1,16)), ;
						shipdata.TYPE      WITH ALLTRIM(SUBSTR(lcStr,17,3)), ;
						shipdata.actcode   WITH ALLTRIM(SUBSTR(lcStr,20,2)), ;
						shipdata.CONTAINER WITH ALLTRIM(SUBSTR(lcStr,22,12)), ;
						shipdata.duns      WITH "1125441", ;
						shipdata.rptloc    WITH "8110", ; &&Alltrim(Substr(lcStr,35,10))
					shipdata.ponum     WITH ALLTRIM(SUBSTR(lcStr,34,16)), ;
						shipdata.dept      WITH ALLTRIM(SUBSTR(lcStr,50,3)), ;
						shipdata.markfor   WITH ALLTRIM(SUBSTR(lcStr,53,10)), ;
						shipdata.vendorid  WITH ALLTRIM(SUBSTR(lcStr,80,6)), ;
						shipdata.qty       WITH VAL(SUBSTR(lcStr,73,7)), ;
						shipdata.scac      WITH ALLTRIM(SUBSTR(lcStr,69,4)), ;
						shipdata.ITEM      WITH ALLTRIM(SUBSTR(lcStr,79,5)), ;
						shipdata.sku       WITH ALLTRIM(SUBSTR(lcStr,85,3))
				ENDDO
				FCLOSE(lnHandle)

				*!*	SELECT shipdata
				*!*	BROW

				*!*	SET STEP ON

				SELECT shipdata
				GOTO TOP

				DO CASE
					CASE shipdata.TYPE = "REC"
						DELETE FOR EMPTY(bol)
						INDEX ON ponum TO po
						SET ORDER TO po
						SELECT DISTINCT ponum FROM shipdata INTO CURSOR ponumindex
						SELECT shipdata
						GOTO TOP
						SELECT ponumindex
						SCAN
							SELECT shipdata
							SET FILTER TO shipdata.ponum = ponumindex.ponum
							GOTO TOP
							Write852()
							CloseEnvelope()
						ENDSCAN

					CASE shipdata.TYPE = "DIS"
						SELECT ponum,markfor FROM shipdata INTO CURSOR pomarkfor GROUP BY ponum,markfor
						SELECT shipdata
						GOTO TOP
						SELECT pomarkfor
						GOTO TOP
						SCAN
							SELECT shipdata
							SET FILTER TO shipdata.ponum = pomarkfor.ponum AND shipdata.markfor = pomarkfor.markfor
							GOTO TOP
							Write852()
							CloseEnvelope()
						ENDSCAN

					CASE shipdata.TYPE = "INV"
						SELECT shipdata
						GOTO TOP
						SELECT shipdata
						GOTO TOP
						SCAN
							SELECT shipdata
							Write852()
							CloseEnvelope()
						ENDSCAN

					CASE shipdata.TYPE = "ADJ"
						SELECT shipdata
						GOTO TOP
						SELECT shipdata
						GOTO TOP
						SCAN
							SELECT shipdata
							Write852()
							CloseEnvelope()
						ENDSCAN

				ENDCASE


				&& here we should archive and delete the files
				IF !FILE(archivefile)
					COPY FILE [&xfile] TO [&archivefile]
				ENDIF
				*    If !lTestImport
				*	   erase [&xfile]
				*    Endif
			NEXT
		ENDIF

	NEXT ii

	*!*	SET STEP ON

ELSE
	MESSAGEBOX("No Sears 852 Flat Files to import",16,"Sears EDI Formatter, Flat to 852...",1500)
ENDIF && lnNum > 0

CloseEDIFile()

IF plTesting THEN
	ADDLINEFEEDS()
ENDIF


********************************************************************************************************************
* 852 Top Level
********************************************************************************************************************
PROCEDURE Start852

	SELECT controlnum
	seqNum = ALLTRIM(STR(controlnum.fileseqnum))
	REPLACE controlnum.fileseqnum WITH controlnum.fileseqnum + 1

	seqNum =PADL(seqNum,8,"0")

	tfile = "SEARSFMI_852_"+ALLTRIM(STR(YEAR(DATE())))+PADL(ALLTRIM(STR(MONTH(DATE()))),2,"0")+;
		PADL(ALLTRIM(STR(DAY(DATE()))),2,"0")+PADL(ALLTRIM(STR(HOUR(DATETIME()))),2,"0")+;
		PADL(ALLTRIM(STR(MINUTE(DATETIME()))),2,"0")+PADL(ALLTRIM(STR(SEC(DATETIME()))),2,"0")+"_"+seqNum+".EDI"

	tfile=xoutdir+tfile
	t852 = FCREATE(tfile)
	IF t852 < 0
		=FCLOSE(t852)
		CLOSE DATABASES ALL
		RETURN
	ENDIF
	WAIT WINDOW "852 Processing for Sears EDI............" NOWAIT

	WriteHeader()

ENDPROC

****************************************
PROCEDURE Write852

	Shipmentheader()

	STORE 3 TO segcnt

	xerrormsg="" &&may be updated in shipment_level() if error occurs

	IF !shipment_level()
		tsubject = "Sears EDI 852 Creation Error at "+TTOC(DATETIME())
		tmessage = xerrormsg
		tattach  = ""
		tcc=""
		tsendto  = "pgaidis@fmiint.com"
		tFrom    ="TGF Corporate Communication Department <transload-ops@fmiint.com>"
		DO FORM dartmail2 WITH tsendto,tFrom,tsubject,tcc,tattach,tmessage,"A"
		=FCLOSE(t852)
		DELETE FILE &tfile
	ELSE
		xerror=.F.
		xemptyfile=.T.
		DO WHILE !EOF("shipdata")
			lncttcount = lncttcount +1
			DO CASE
				CASE shipdata.TYPE = "REC"
					STORE "LIN**"+"IN*"+ALLTRIM(shipdata.ITEM)+"*IZ*"+ALLTRIM(shipdata.sku)+segterminator TO cstring
					fputstring(t852,cstring)
					segcnt=segcnt+1

					STORE "ZA*QR*"+ALLTRIM(STR(shipdata.qty))+"*EA"+segterminator TO cstring
					fputstring(t852,cstring)
					segcnt=segcnt+1
				CASE shipdata.TYPE = "DIS"
					STORE "LIN**"+"IN*"+ALLTRIM(shipdata.ITEM)+"*IZ*"+ALLTRIM(shipdata.sku)+segterminator TO cstring
					fputstring(t852,cstring)
					segcnt=segcnt+1

					STORE "ZA*QW*"+ALLTRIM(STR(shipdata.qty))+"*EA"+segterminator TO cstring
					fputstring(t852,cstring)
					segcnt=segcnt+1

				CASE shipdata.TYPE = "INV"
					STORE "LIN**"+"IN*"+ALLTRIM(shipdata.ITEM)+"*IZ*"+ALLTRIM(shipdata.sku)+segterminator TO cstring
					fputstring(t852,cstring)
					segcnt=segcnt+1

					STORE "ZA*QA*"+ALLTRIM(STR(shipdata.qty))+"*EA"+segterminator TO cstring
					fputstring(t852,cstring)
					segcnt=segcnt+1

				CASE shipdata.TYPE = "ADJ"
					STORE "LIN**"+"IN*"+ALLTRIM(shipdata.ITEM)+"*IZ*"+ALLTRIM(shipdata.sku)+segterminator TO cstring
					fputstring(t852,cstring)
					segcnt=segcnt+1

					STORE "ZA*"+ALLTRIM(shipdata.actcode)+"*"+ALLTRIM(STR(shipdata.qty))+"*EA"+segterminator TO cstring
					fputstring(t852,cstring)
					segcnt=segcnt+1

			ENDCASE
			SKIP 1 IN shipdata

		ENDDO
	ENDIF &&shipment_level()

ENDPROC

*******************************************************************************************************
PROCEDURE CloseEnvelope()

	STORE "CTT*"+ALLTRIM(STR(lncttcount,6,0))+segterminator TO cstring
	fputstring(t852,cstring)
	lcCtr = ALLTRIM(STR(totlincnt))
	lincnt = 1
	STORE 0 TO lncttcount
	lc852Ctr = ALLTRIM(STR(gn852Ctr))
	lcSegCtr = ALLTRIM(STR(segcnt+1))

	cstring ="SE*"+lcSegCtr+"*"+stNum+PADL(lc852Ctr,4,"0")+segterminator
	fputstring(t852,cstring)

	********************************************************************************************************
PROCEDURE CloseEDIFile

	**process the next work order in the table
	*!*	  Select asnwo
	*!*	  Replace asnwo.processed With .T. In asnwo
	*!*	  Replace asnwo.Process   With .F. In asnwo
	*!*	  Replace asnwo.date_proc  With Datetime() In asnwo
	summaryinfo = ;
		"-----------------------------------------------------------------------------"+CHR(13)+;
		'852 file name....'+"---> "+lc_cnum+CHR(13)+;
		'   created.......'+DTOC(DATE())+CHR(13)+;
		'   file.....: '+tfile+CHR(13)+;
		'-----------------------------------------------------------------------------'+CHR(13)+;
		'shipment level count.....'+STR(thisshipmentlevelcount)+CHR(13)+;
		'   order level count.....'+STR(thisorderlevelcount)+CHR(13)+;
		'    pack level count.....'+STR(thispacklevelcount)+CHR(13)+;
		'    item level count.....'+STR(thisitemlevelcount)+CHR(13)+;
		'-----------------------------------------------------------------------------'+CHR(13)
	*  Replace asnwo.Info With summaryinfo+workordersprocessed



	lc852Ctr = ALLTRIM(STR(gn852Ctr))
	STORE "GE*"+lc852Ctr+"*"+stNum+segterminator TO cstring
	fputstring(t852,cstring)

	STORE "IEA*1*"+lcInterctrlNum+segterminator TO cstring
	fputstring(t852,cstring)

	=FCLOSE(t852)

	STORE "" TO workordersprocessed
	STORE 0 TO thisshipmentlevelcount,thisorderlevelcount,thispacklevelcount,thisitemlevelcount

	SET CENTURY OFF
	SET DATE TO american
	RETURN

ENDPROC

********************************************************************************************************************
* I T E M     L E V E L
********************************************************************************************************************
PROC item_level
	lincnt = lincnt + 1
	hlctr = hlctr +1
	itemlevelcount = itemlevelcount+1
	thisitemlevelcount = thisitemlevelcount+1
	currentitemlevelcount = hlctr
	lc_hlcount = ALLTRIM(getnum(hlctr)) 						&& overall HL counter
	lc_parentcount = ALLTRIM(getnum(currentorderlevelcount))	&& the parent

	STORE "HL*"+lc_hlcount+"*"+lc_parentcount+"*I"+segterminator TO cstring
	fputstring(t852,cstring)
	segcnt=segcnt+1
	hlsegcnt=hlsegcnt+1


	IF EMPTY(shipdata.custsku) AND  EMPTY(shipdata.STYLE)
		STORE "LIN**"+"UP*"+ALLTRIM(shipdata.upc)+segterminator TO cstring
	ENDIF

	IF EMPTY(shipdata.custsku) AND  !EMPTY(shipdata.STYLE)
		STORE "LIN**"+"IN*"+ALLTRIM(shipdata.STYLE)+"*"+"UP*"+ALLTRIM(shipdata.upc)+segterminator TO cstring
	ENDIF

	fputstring(t852,cstring)
	segcnt=segcnt+1

	STORE "SN1**"+ALLTRIM(STR(shipdata.qty))+"*ST"+segterminator TO cstring
	fputstring(t852,cstring)
	segcnt=segcnt+1

ENDPROC &&item_level

********************************************************************************************************************
* O R D E R    L E V E L
********************************************************************************************************************
PROC order_level
	PARAMETER lcDelloc

	hlctr = hlctr +1
	orderlevelcount = orderlevelcount+1
	thisorderlevelcount = thisorderlevelcount+1
	lc_hlcount = getnum(hlctr)								&& overall HL counter
	lc_parentcount = getnum(currentshipmentlevelcount)				&& the parent
	currentorderlevelcount = hlctr

	STORE "HL*"+ALLTRIM(lc_hlcount)+"*"+ALLTRIM(lc_parentcount)+"*O"+segterminator TO cstring
	fputstring(t852,cstring)
	********************************************************************************************************************************
	segcnt=segcnt+1
	hlsegcnt=hlsegcnt+1

	** PRF
	STORE "PRF*"+ALLTRIM(shipdata.ponum)+segterminator TO cstring
	fputstring(t852,cstring)
	segcnt=segcnt+1

	STORE "PID*S**VI*FL"+segterminator TO cstring
	fputstring(t852,cstring)
	segcnt=segcnt+1

	STORE "TD1**"+ALLTRIM(STR(shipdata.ctnqty))+segterminator TO cstring
	fputstring(t852,cstring)
	segcnt=segcnt+1

	STORE "REF*DP*"+ALLTRIM(shipdata.dept)+segterminator TO cstring
	fputstring(t852,cstring)
	segcnt=segcnt+1

	STORE "N1*Z7**92*"+ALLTRIM(shipdata.markfor)+segterminator TO cstring
	fputstring(t852,cstring)
	segcnt=segcnt+1

ENDPROC &&order_level

********************************************************************************************************************
* S H I P M E N T     L E V E L
********************************************************************************************************************
PROC shipment_level
	* reset all at each shipment level
	STORE 0 TO shipmentlevelcount
	STORE 0 TO orderlevelcount
	STORE 0 TO packlevelcount
	STORE 0 TO itemlevelcount
	STORE 0 TO hlctr

	shipmentlevelcount = shipmentlevelcount +1
	thisshipmentlevelcount = thisshipmentlevelcount +1
	hlctr = hlctr +1
	currentshipmentlevelcount = hlctr
	**currentshipmentlevelcount = shipmentlevelcount
	lc_hlcount = ALLTRIM(getnum(hlctr))
	STORE "HL*"+lc_hlcount+"**S"+segterminator TO cstring
	*fputstring(t852,cstring)
	********************************************************************************************************************
	segcnt=segcnt+1
	hlsegcnt=hlsegcnt+1

	********** all versions have the following 3 segments **********
	STORE "N9*IA*"+ALLTRIM(shipdata.duns)+segterminator TO cstring
	fputstring(t852,cstring)
	segcnt=segcnt+1

	STORE "N9*DP*"+ALLTRIM(shipdata.dept)+segterminator TO cstring
	fputstring(t852,cstring)
	segcnt=segcnt+1

	STORE "N1*RL*"+ALLTRIM(shipdata.rptloc)+segterminator TO cstring
	fputstring(t852,cstring)
	segcnt=segcnt+1

	DO CASE
		CASE shipdata.TYPE = "REC"
			STORE "N9*PO*"+ALLTRIM(shipdata.ponum)+segterminator TO cstring
			fputstring(t852,cstring)
			segcnt=segcnt+1

			STORE "N9*BM*"+ALLTRIM(shipdata.bol)+segterminator TO cstring
			fputstring(t852,cstring)
			segcnt=segcnt+1


			STORE "N1*VN*"+ALLTRIM(shipdata.vendorid)+segterminator TO cstring
			fputstring(t852,cstring)
			segcnt=segcnt+1

			STORE "TD5**2*"+ALLTRIM(shipdata.scac)+segterminator TO cstring
			fputstring(t852,cstring)
			segcnt=segcnt+1

		CASE shipdata.TYPE = "DIS"
			STORE "N9*PO*"+ALLTRIM(shipdata.ponum)+segterminator TO cstring
			fputstring(t852,cstring)
			segcnt=segcnt+1

			STORE "N1*BY*"+ALLTRIM(shipdata.markfor)+segterminator TO cstring
			fputstring(t852,cstring)
			segcnt=segcnt+1

	ENDCASE

	RETURN
ENDPROC &&shipment_level

********************************************************************************************************
PROCEDURE Shipmentheader
	PARAMETERS tshipid

	gn852Ctr = gn852Ctr +1
	STORE "ST*852*"+stNum+PADL(ALLTRIM(STR(gn852Ctr)),4,"0")+segterminator TO cstring
	fputstring(t852,cstring)

	SELECT controlnum
	lc_cnum = PADL(ALLTRIM(STR(controlnum.bsnseg)),5,"0")
	REPLACE controlnum.bsnseg WITH controlnum.bsnseg + 1

	STORE "XQ*H*"+d4+d1+d2+d3+segterminator TO cstring
	fputstring(t852,cstring)
	segcnt=segcnt+1

	SELECT controlnum
	seqNum = ALLTRIM(STR(controlnum.rcvseqnum))
	REPLACE controlnum.rcvseqnum WITH controlnum.rcvseqnum + 1

	seqNum =PADL(seqNum,8,"0")
	STORE "N9*DD*"+ALLTRIM(shipdata.TYPE)+seqNum+segterminator TO cstring
	fputstring(t852,cstring)
	segcnt=segcnt+1

	totlincnt = 0
ENDPROC &&shipmentheader


********************************************************************************************************
PROCEDURE WriteHeader
	* get the next unique control number
	SELECT controlnum
	stNum = ALLTRIM(STR(controlnum.cntrl852))
	lc_852num =stNum
	REPLACE controlnum.cntrl852 WITH controlnum.cntrl852 + 1
	lnInterControlnum = controlnum.interctrl
	REPLACE controlnum.interctrl WITH controlnum.interctrl + 1
	lcInterctrlNum = PADL(ALLTRIM(STR(lnInterControlnum)),9,"0")

	*  write out the 214 header

	STORE "ISA*00*          *00*          *ZZ*FMI            *12*"+tqualcode+"*"+tyr+tmon+tday+"*"+lcTime+"*U*00401*"+lcInterctrlNum +"*0*T* "+segterminator TO cstring
	fputstring(t852,cstring)

	STORE "GS*PD*"+ALLTRIM(tsendid)+"*"+ALLTRIM(tsendcode)+"*"+twholeyr+tmon+tday+"*"+lcTime+"*"+stNum+"*X*004010"+segterminator TO cstring
	fputstring(t852,cstring)
ENDPROC


********************************************************************************************************
FUNCTION getnum
	PARAM plcount
	fidnum = IIF(BETWEEN(plcount,1,9),SPACE(11)+LTRIM(STR(plcount)),;
		IIF(BETWEEN(plcount,10,99),SPACE(10)+LTRIM(STR(plcount)),;
		IIF(BETWEEN(plcount,100,999),SPACE(9)+LTRIM(STR(plcount)),;
		IIF(BETWEEN(plcount,1000,9999),SPACE(8)+LTRIM(STR(plcount)),;
		IIF(BETWEEN(plcount,10000,99999),SPACE(7)+LTRIM(STR(plcount))," ")))))
	RETURN fidnum


	********************************************************************************************************
PROC fputstring
	PARAM filehandle,filedata
	FWRITE(filehandle,filedata)
ENDPROC


********************************************************************************************************
PROC CheckEmpty
	PARAMETER lcDataValue
	RETURN IIF(EMPTY(lcDataValue),"UNKNOWN",lcDataValue)
ENDPROC


********************************************************************************************************
PROC RevertTables
	TABLEREVERT(.T.,"controlnum")
	TABLEREVERT(.T.,"asnwo")
	TABLEREVERT(.T.,"a852info")
	TABLEREVERT(.T.,"bkdnWO")
ENDPROC
********************************************************************************************************

PROCEDURE ADDLINEFEEDS
	LOCAL lnNumFiles, lcRootFileName, lcString, lcInputFolder, lcConvertedFile, lcConvertedFolder

	lcInputFolder = 'C:\SEARSEDI\EDIDATAOUT\'
	lcConvertedFolder = 'C:\SEARSEDI\CONVERTED\'

	SET DEFAULT TO (lcInputFolder)
	lnNumFiles = ADIR(laFiles,"*.*")

	IF lnNumFiles > 0 THEN
		FOR i = 1 TO lnNumFiles
			lcRootFileName = laFiles[i,1]
			lcFile = lcInputFolder + lcRootFileName
			lcConvertedFile = lcConvertedFolder + lcRootFileName
			WAIT WINDOW NOWAIT 'Converting ' + lcFile
			lcString = FILETOSTR(lcFile)
			lcString = STRTRAN(lcString,CR,CRLF)
			=STRTOFILE(lcString,UPPER(lcConvertedFile))
		ENDFOR
		WAIT CLEAR
	ENDIF  &&  lnNumFiles
	RETURN
ENDPROC