* HR Check Certification Report.
* Send list of employee checks/vouchers to ? -- everything will be data-driven.
* To be run every Thursday morning (Friday's payroll usually gets downloaded on Wednesday)
* Build EXE as F:\UTIL\ADPREPORTS\HRCHECKCERTRPT.EXE

LOCAL lTestMode
lTestMode = .T.

IF NOT lTestMode THEN
	utilsetup("HRCHECKCERTRPT")
ENDIF

DECLARE INTEGER CreateMutex IN Win32API ;
	STRING @lpMutexAttributes, ;
	STRING bInitialOwner, ;
	STRING @lpName

DECLARE INTEGER GetLastError IN Win32API

* if running in EXE check to see if already running
IF _VFP.STARTMODE > 0 THEN
	lcProcessName = "HRCHECKCERTRPT"
	CreateMutex(0,CHR(1),@lcProcessName)
	lnError = GetLastError()
	IF lnError = 183
		WAIT WINDOW TIMEOUT 5 "This program is already running..."
		IF NOT lTestMode THEN
			schedupdate()
		ENDIF
		RETURN .F.
	ENDIF
ENDIF

loHRCHECKCERTRPT = CREATEOBJECT('HRCHECKCERTRPT')
loHRCHECKCERTRPT.MAIN( lTestMode )

IF NOT lTestMode THEN
	schedupdate()
ENDIF

CLOSE DATABASES ALL
RETURN

#DEFINE LOGIT 1
#DEFINE WAITIT 2
#DEFINE NOWAITIT 4
#DEFINE SENDIT 8
#DEFINE CRLF CHR(13) + CHR(10)
#DEFINE RETURN_DATA_MANDATORY .F.
#DEFINE RETURN_DATA_NOT_MANDATORY .T.
#DEFINE CSVFORMAT 6
#DEFINE WRITE_ONLY_UNBUFFERED 11
#DEFINE COMMA ","

DEFINE CLASS HRCHECKCERTRPT AS CUSTOM

	cProcessName = 'HRCHECKCERTRPT'

	lTestMode = .F.  && .F. FOR NORMAL OPERATION, .T. TO SUPPRESS ALL BUT DEVELOPER EMAILS AND CHANGE INPUT TABLE, ETC.

	dToday = DATE()
	*dToday = {^2005-02-01}  && to force report for January

	lAutoYield = .T.

	cStartTime = TTOC(DATETIME())

	* connection properties
	nSQLHandle = 0

	* data properties
	cCheckCertTable = 'F:\UTIL\ADPREPORTS\DATA\CHECKCERT'
	
	* processing properties
	cNotReportedList = ''
	oExcel = NULL

	* wait window properties
	nWaitWindowTimeout = 2
	lWaitWindowIsOn = .T.

	* logfile properties
	lLoggingIsOn = .T.
	cLogFile = 'F:\UTIL\ADPREPORTS\LOGFILES\HRCHECKCERTRPT_REPORT_log.txt'

	* INTERNAL email properties
	lSendInternalEmailIsOn = .T.
	cFrom = 'TGF Corporate <fmicorporate@fmiint.com>'
	cSendTo = 'mbennett@fmiint.com'
	cCC = ''
	cSubject = 'District Managers Weekly Payroll Audit Report Report'
	cAttach = ''
	cBodyText = ''
	cMGRSendTo = ''
	cMGRFrom = ''
	cMGRSubject = ''
	cMGRCC = ''
	cMGRAttach = ''
	cMGRBodyText = ''

	cCOMPUTERNAME = ''
	cUSERNAME = ''


	FUNCTION INIT
		IF NOT DODEFAULT()
			RETURN .F.
		ENDIF
		WITH THIS
			*SET RESOURCE OFF
			CLOSE DATA
			SET CENTURY ON
			*SET DATE AMERICAN
			*SET HOURS TO 24
			SET ANSI ON
			SET TALK OFF
			SET DELETED ON
			SET CONSOLE OFF
			SET EXCLUSIVE OFF
			SET SAFETY OFF
			SET EXACT OFF
			SET STATUS BAR ON
			SET SYSMENU OFF
			SET ENGINEBEHAVIOR 70
			_VFP.AUTOYIELD = .lAutoYield
			.cCOMPUTERNAME = GETENV("COMPUTERNAME")
			.cUSERNAME = GETENV("USERNAME")
			.lLoggingIsOn = .lLoggingIsOn AND NOT EMPTY(.cLogFile)
			WAIT WINDOW NOWAIT "Opening Excel..."
			TRY
				.oExcel = CREATEOBJECT("excel.application")
				.oExcel.VISIBLE = .F.
				.oExcel.DisplayAlerts = .F.
			CATCH
				.oExcel = NULL
			ENDTRY
		ENDWITH
	ENDFUNC


	FUNCTION DESTROY
		WITH THIS
			IF .lLoggingIsOn  THEN
				SET ALTERNATE OFF
				SET ALTERNATE TO
			ENDIF
		ENDWITH
		IF TYPE('.oExcel') = 'O' AND NOT ISNULL(.oExcel) THEN
			.oExcel.QUIT()
		ENDIF
		DODEFAULT()
	ENDFUNC


	FUNCTION MAIN
		LPARAMETERS tlTestMode
		WITH THIS
			LOCAL lnNumberOfErrors, lcSQLPayDate, lcSQLTR, lcSQLALL, loError, lnMissingCount, lcXLFileName
			LOCAL ldLatestPaydate, lcFiletoSaveAs, lcSQLWhere, lcFname, lcEmail, lcCC, lcManager
			LOCAL lnRow, lcRow, lcSQLPayDate
			
			TRY
				lnNumberOfErrors = 0

				.lTestMode = tlTestMode

				IF .lTestMode THEN
					.cLogFile = 'F:\UTIL\ADPREPORTS\LOGFILES\HRCHECKCERTRPT_REPORT_log_TESTMODE.txt'
					.cSendTo = 'mbennett@fmiint.com'
				ENDIF
				IF .lLoggingIsOn THEN
					SET ALTERNATE TO (.cLogFile) ADDITIVE
					SET ALTERNATE ON
				ENDIF

				.cStartTime = TTOC(DATETIME())

				.TrackProgress('=========================================================', LOGIT+SENDIT)
				.TrackProgress('HR Check Certification Rpt process started....', LOGIT+SENDIT+NOWAITIT)
				.TrackProgress('COMPUTERNAME = ' + .cCOMPUTERNAME, LOGIT+SENDIT)
				.TrackProgress('USERNAME = ' + .cUSERNAME, LOGIT+SENDIT)
				IF .lTestMode THEN
					.TrackProgress('                        TEST MODE                        ', LOGIT+SENDIT)
				ENDIF
				.TrackProgress('=========================================================', LOGIT+SENDIT)


				.nSQLHandle = SQLCONNECT("PCPAYWIN","MBENNETT","VP737A")

				IF .nSQLHandle > 0 THEN
				
					* first, get latest pay date in v_chk_vw_info
					
					SET TEXTMERGE ON
					TEXT TO	lcSQLPayDate NOSHOW
					
SELECT MAX(CHECKVIEWPAYDATE) AS PAYDATE FROM REPORTS.V_CHK_VW_INFO

					ENDTEXT
					SET TEXTMERGE OFF

					IF USED('CURPAYDATE') THEN
						USE IN CURPAYDATE
					ENDIF

					IF NOT .ExecSQL(lcSQLPayDate, 'CURPAYDATE', RETURN_DATA_MANDATORY) THEN
						.TrackProgress('COULD NOT EXECUTE SQL: lcSQLPayDate = ' + lcSQLPayDate, LOGIT+SENDIT)
						THROW
					ENDIF 

					* determine latest paydate for documentation
					SELECT CURPAYDATE
					GOTO TOP
					ldLatestPaydate = TTOD(CURPAYDATE.PAYDATE)
					
					********************************************
					** TO FORCE SPECIFIC PAY DATE, ACTIVATE BELOW
					ldLatestPaydate = {^2011-03-18}
					********************************************
					
					.cSubject = 'District Managers Weekly Payroll Audit Report, for Pay Date: ' + DTOC(ldLatestPaydate)
					
					lcSQLPayDate = "DATE'" + ALLTRIM(STR(YEAR(ldLatestPaydate))) + "-" + PADL(MONTH(ldLatestPaydate),2,'0') + ;
						"-" + PADL(DAY(ldLatestPaydate),2,'0') + "' "

					* next, get a list of all employees for the latest paydate, regardless of District Manager, 
					* which we will use at the end of the process to report on any who did not fall into a District Manager report.
					SET TEXTMERGE ON

					TEXT TO	lcSQLALL NOSHOW
SELECT
CHECKVIEWPAYDATE AS PAYDATE,
{fn LEFT(CHECKVIEWHOMEDEPT,2)} AS DIVISION,
NAME,
SUM({FN IFNULL(CHECKVIEWGROSSPAYA,0.00)}) AS GROSSPAY
FROM REPORTS.V_CHK_VW_INFO
WHERE CHECKVIEWPAYDATE = <<lcSQLPayDate>>
GROUP BY CHECKVIEWPAYDATE, {fn LEFT(CHECKVIEWHOMEDEPT,2)}, NAME
ORDER BY CHECKVIEWPAYDATE, {fn LEFT(CHECKVIEWHOMEDEPT,2)}, NAME

					ENDTEXT
					SET TEXTMERGE OFF
					
					IF USED('CURALL') THEN
						USE IN CURALL
					ENDIF
					IF USED('CURREPORTED') THEN
						USE IN CURREPORTED
					ENDIF

					IF .ExecSQL(lcSQLALL, 'CURALL', RETURN_DATA_MANDATORY) THEN
						*SELECT CURALL
						*BROWSE
						* create cursor which will hold all ee's who are in the DM reports
						SELECT * FROM CURALL ;
							INTO CURSOR CURREPORTED ;
							WHERE .F. READWRITE
					ELSE
						.TrackProgress('COULD NOT EXECUTE SQL: lcSQLALL = ' + lcSQLALL, LOGIT+SENDIT)
						THROW
					ENDIF 

					* determine latest paydate for documentation
					SELECT CURALL
					GOTO TOP
					ldLatestPaydate = CURALL.PAYDATE
					.cSubject = 'District Managers Weekly Payroll Audit Report, for Pay Date: ' + DTOC(ldLatestPaydate)

					* now we will scan through control table and generate one report/email per District Manager
					USE (.cCheckCertTable) IN 0 ALIAS DMTABLE
					
					SELECT DMTABLE
					SCAN FOR LACTIVE
						lcManager = ALLTRIM(DMTABLE.cmanager)
						lcFname = ALLTRIM(DMTABLE.cfname)
						lcEmail = ALLTRIM(DMTABLE.cemail)
						lcSQLWhere = ALLTRIM(DMTABLE.cwhere)
						lcCC = ALLTRIM(DMTABLE.cCC)

						.TrackProgress('lcManager = ' + lcManager, LOGIT+SENDIT)
						.TrackProgress('lcFname = ' + lcFname, LOGIT+SENDIT)
						.TrackProgress('lcEmail = ' + lcEmail, LOGIT+SENDIT)
						.TrackProgress('lcSQLWhere = ' + lcSQLWhere, LOGIT+SENDIT)
						.TrackProgress('lcCC = ' + lcCC, LOGIT+SENDIT)

						SET TEXTMERGE ON
						TEXT TO	lcSQLTR NOSHOW
SELECT
CHECKVIEWPAYDATE AS PAYDATE,
{fn LEFT(CHECKVIEWHOMEDEPT,2)} AS DIVISION,
NAME,
SUM({FN IFNULL(CHECKVIEWGROSSPAYA,0.00)}) AS GROSSPAY
FROM REPORTS.V_CHK_VW_INFO
<<lcSQLWhere>>
AND CHECKVIEWPAYDATE = <<lcSQLPayDate>>
GROUP BY CHECKVIEWPAYDATE, {fn LEFT(CHECKVIEWHOMEDEPT,2)}, NAME
ORDER BY CHECKVIEWPAYDATE, {fn LEFT(CHECKVIEWHOMEDEPT,2)}, NAME

						ENDTEXT
						SET TEXTMERGE OFF

						IF .lTestMode THEN
							.TrackProgress('lcSQLTR =' + lcSQLTR, LOGIT+SENDIT)
						ENDIF

						IF USED('SQLCURSOR1') THEN
							USE IN SQLCURSOR1
						ENDIF

						IF .ExecSQL(lcSQLTR, 'SQLCURSOR1', RETURN_DATA_MANDATORY) THEN
							
							* populate CURREPORTED
							SELECT SQLCURSOR1
							SCAN
								SCATTER MEMVAR
								INSERT INTO CURREPORTED FROM MEMVAR
							ENDSCAN
							
							* create excel spreadsheet report
							oWorkbook = .oExcel.workbooks.OPEN("F:\UTIL\ADPREPORTS\PAYROLLAUDIT_TEMPLATE.XLS")
							oWorksheet = oWorkbook.Worksheets[1]

							lcFiletoSaveAs = "F:\UTIL\ADPREPORTS\REPORTS\AUDIT" + UPPER(lcManager) + STRTRAN(DTOC(ldLatestPaydate),"/","-")
							lcXLFileName = lcFiletoSaveAs + ".XLS"
							IF FILE(lcXLFileName) THEN
								DELETE FILE (lcXLFileName)
							ENDIF
							oWorkbook.SAVEAS(lcFiletoSaveAs)
							
							lnRow = 1
							SELECT SQLCURSOR1
							SCAN
								lnRow = lnRow + 1
								lcRow = ALLTRIM(STR(lnRow))
								oWorksheet.RANGE("A" + lcRow).VALUE = TTOD(SQLCURSOR1.PAYDATE)
								oWorksheet.RANGE("B" + lcRow).VALUE = ALLTRIM(SQLCURSOR1.DIVISION)
								oWorksheet.RANGE("C" + lcRow).VALUE = ALLTRIM(SQLCURSOR1.NAME)
								oWorksheet.RANGE("D" + lcRow).VALUE = SQLCURSOR1.GROSSPAY								
							ENDSCAN
							
							oWorkbook.SAVE()
							oWorkbook.CLOSE()
							RELEASE oWorkbook
							RELEASE oWorksheet

							**************************************************************************************************************
							**************************************************************************************************************

*!*								IF FILE(lcFiletoSaveAs) THEN
*!*									DELETE FILE (lcFiletoSaveAs)
*!*								ENDIF

*!*								SELECT SQLCURSOR1
*!*								LOCATE
*!*								COPY TO (lcFiletoSaveAs) XL5

							IF FILE(lcXLFileName) THEN
								* attach output file to email
								.cMGRAttach = lcXLFileName
								.cMGRBodyText = "See attached Payroll Audit report. These are your employees who got paid last week. " + ;
									"If someone is there that shouldn't be, or someone is missing, contact Human Resources immediately! " + ;
									"Otherwise, please print, sign and mail the report to Human Resources." + ;
									CRLF + CRLF + "(do not reply - this is an automated report)"
							ELSE
								.TrackProgress('ERROR: unable to email spreadsheet: ' + lcFiletoSaveAs, LOGIT+SENDIT)
							ENDIF
				
							.cMGRSendTo = lcEmail
							.cMGRFrom = .cFrom
							.cMGRSubject = 'Weekly Payroll Audit Report, for ' + lcManager + ' for: ' + DTOC(ldLatestPaydate)
							.cMGRCC = lcCC

							IF .lTestMode THEN
								.cMGRSendTo = 'mbennett@fmiint.com'
								.cMGRCC = ''
							ENDIF

							DO FORM dartmail2 WITH .cMGRSendTo,.cMGRFrom,.cMGRSubject,.cMGRCC,.cMGRAttach,.cMGRBodyText,"A"

						ELSE
							.TrackProgress('!!No employee payroll data was found for ' + lcManager, LOGIT+SENDIT)
						ENDIF  &&  .ExecSQL(lcSQL, 'SQLCURSOR1', RETURN_DATA_MANDATORY)

					ENDSCAN
										
					* report on any ee's who were not reported in the individual DM reports
					IF USED('CURMISSING') THEN
						USE IN CURMISSING
					ENDIF
					
					SELECT * FROM CURALL ;
						INTO CURSOR CURMISSING ;
						WHERE NAME NOT IN (SELECT NAME FROM CURREPORTED) ;
						ORDER BY DIVISION, NAME
						
					IF USED('CURMISSING') AND NOT EOF('CURMISSING') THEN
						lnMissingCount = RECCOUNT('CURMISSING')
						.cNotReportedList = TRANSFORM(lnMissingCount) + ' employees were not in the District Manager reports:' + CRLF
						SELECT CURMISSING
						SCAN
							.cNotReportedList = .cNotReportedList + PADR(ALLTRIM(CURMISSING.NAME),40) + ALLTRIM(CURMISSING.DIVISION) + CRLF
						ENDSCAN
					ELSE
						.TrackProgress('!! All employees were reported on :)', LOGIT+SENDIT)
					ENDIF

				ELSE
					* connection error
					.TrackProgress('Unable to connect to ADP Payroll System.', LOGIT+SENDIT)
				ENDIF   &&  .nSQLHandle > 0

				
				IF NOT EMPTY(.cNotReportedList) THEN
					.cBodyText = .cNotReportedList + CRLF + CRLF + .cBodyText
				ENDIF
				

				.TrackProgress('HR Check Certification Rpt process ended normally.', LOGIT+SENDIT)

			CATCH TO loError

				.TrackProgress('There was an error.',LOGIT+SENDIT)
				.TrackProgress('The Error # is: ' + TRANSFORM(loError.ERRORNO), LOGIT+SENDIT)
				.TrackProgress('The Error Message is: ' + TRANSFORM(loError.MESSAGE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred in Program: ' + TRANSFORM(loError.PROCEDURE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred at line #: ' + TRANSFORM(loError.LINENO), LOGIT+SENDIT)
				lnNumberOfErrors = lnNumberOfErrors + 1
				IF TYPE('oWorkbook') = "O" THEN
					oWorkbook.SAVE()
				ENDIF
				IF TYPE('.oExcel') = 'O' AND NOT ISNULL(.oExcel) THEN
					.oExcel.VISIBLE = .T.
				ENDIF

			ENDTRY

			CLOSE DATA
			WAIT CLEAR
			***************** INTERNAL email results ******************************
			.TrackProgress('About to send status email.',LOGIT+SENDIT)
			.TrackProgress('The logfile is: ' + .cLogFile, LOGIT+SENDIT)
			.TrackProgress('HR Check Certification Rpt process started: ' + .cStartTime, LOGIT+SENDIT)
			.TrackProgress('HR Check Certification Rpt process finished: ' + TTOC(DATETIME()), LOGIT+SENDIT)

			IF .lSendInternalEmailIsOn THEN
				DO FORM dartmail2 WITH .cSendTo,.cFrom,.cSubject,.cCC,.cAttach,.cBodyText,"A"
				.TrackProgress('Sent status email.',LOGIT)
			ELSE
				.TrackProgress('Did not send status email: THIS.lSendInternalEmailIsOn = FALSE.',LOGIT)
			ENDIF

		ENDWITH
		RETURN
	ENDFUNC && main


	FUNCTION ExecSQL
		LPARAMETERS tcSQL, tcCursorName, tlNoDataReturnedIsOkay
		LOCAL llRetval, lnResult
		WITH THIS
			* close target cursor if it's open
			IF USED(tcCursorName)
				USE IN (tcCursorName)
			ENDIF
			WAIT WINDOW "Fetching remote data; please wait..." NOWAIT
			lnResult = SQLEXEC(.nSQLHandle, tcSQL, tcCursorName)
			llRetval = ( lnResult > 0 )
			IF llRetval THEN
				* see if any data came back
				IF NOT tlNoDataReturnedIsOkay THEN
					IF (NOT USED(tcCursorName)) OR EOF(tcCursorName) THEN
						llRetval = .F.
						.TrackProgress('No data was returned by this query: ' +CRLF + tcSQL, LOGIT+SENDIT)
						.cSendTo = 'Mark Bennett <mbennett@fmiint.com>'
					ENDIF
				ENDIF
			ELSE
				.TrackProgress("Result Code [" + TRANSFORM(lnResult) + "] returned by this query: " + tcSQL, LOGIT+SENDIT)
				.cSendTo = 'Mark Bennett <mbennett@fmiint.com>'
			ENDIF
			WAIT CLEAR
			RETURN llRetval
		ENDWITH
	ENDFUNC


	PROCEDURE TrackProgress
		* do any combination of Wait Window, writing to logfile, and adding to body of email,
		* based on nFlags parameter.
		LPARAMETERS tcExpression, tnFlags
		WITH THIS
			IF BITAND(tnFlags,LOGIT) = LOGIT THEN
				IF .lLoggingIsOn THEN
					?
					? .cProcessName + "  " + TTOC(DATETIME()) + ": " + tcExpression
				ENDIF
			ENDIF
			IF .lWaitWindowIsOn THEN
				IF BITAND(tnFlags,NOWAITIT) = NOWAITIT THEN
					WAIT WINDOW tcExpression NOWAIT
				ENDIF
				IF BITAND(tnFlags,WAITIT) = WAITIT THEN
					WAIT WINDOW tcExpression TIMEOUT .nWaitWindowTimeout
				ENDIF
			ENDIF
			IF BITAND(tnFlags,SENDIT) = SENDIT THEN
				IF .lSendInternalEmailIsOn THEN
					.cBodyText = .cBodyText + tcExpression + CRLF + CRLF
				ENDIF
			ENDIF
		ENDWITH
	ENDPROC  &&  TrackProgress
	
	
	PROCEDURE BACKUPTABLE
		SET SAFETY OFF
		USE F:\UTIL\ADPREPORTS\DATA\DISTMGR.DBF
		COPY TO F:\UTIL\ADPREPORTS\DATA\DISTMGRBAK
		USE
	ENDPROC

ENDDEFINE

