*!* Marc Jacobs ECOMM "Warehouse Returns Feed" Creation Program

********************************************************************************
** o Return will be in INWOLOG/INDET because it is product coming IN to TGF     
** o Product would have originally shipped FROM TGF, so old outbound order will 
**   exist in OUTSHIP/OUTDET                                                    
** o INWOLOG.acct_ref will contain the original outbound order number (link     
**   back to OUTSHIP)                                                           
**                                                                              
** QUESTION: INDET does not have UPC as a field.  What is connecting field that 
**           allows us to get the correct matching OUTDET record for the INDET  
**           record?  (whreturn file needs original order number and UPC from   
**           OUTDET)                                                            
**                                                                              
** NOTE 1: There is no real way to get a valid test until we get "Orders To     
**         Fulfil" data loaded (so that we can create returns against           
**         previous outbound orders)                                            
**                                                                              
** NOTE 2: Initially, returns will NOT be product originally shipped from TGF.  
**         An interface needs to be developed to allow Radial to send us        
**         original shipping info on returns and for us to store on a DBF on    
**         our system.                                                          
********************************************************************************

PARAMETERS inOrdNum

*SET STEP ON 

*fileDir = 'M:\doug\MarcJacobs\'
*fileDir = "F:\FTPUSERS\MJ_eCom\eComm_Out\"
**MJDir = 'h:\MJ_Rtn\'
**RadialDir='h:\Radial_Rtn\'
MJDir = 'f:\ftpusers\mj_ecom\MJ_Rtn\'
RadialDir='f:\ftpusers\mj_ecom\Radial_Rtn\'

CREATE CURSOR WHReturn;
	(RecordID C(2),;
	FulfillerStoreNum C(3),;
	TPFStoreNum C(5),;
	OrdInvoiceNum C(14),;
	OrdLineNum C(4),;
	PartNum C(15),;
	ReturnQty N(7,0),;
	CustReasonCd C(3),;
	FulfilReasonCd C(3),;
	ReturnDate C(14),;
	RTANum C(30),;
	FileCreateDate C(14))
	
cProcName = "marcjacobs_ecom_whreturns"
cInclude = "office = cOffice"

DIMENSION rsnList(8)
rsnList(1) = "C"
rsnList(2) = "CC1"
rsnList(3) = "DM"
rsnList(4) = "EA1"
rsnList(5) = "EA2"
rsnList(6) = "GA1"
rsnList(7) = "GA2"
rsnList(8) = "GA3"
*******************************************************

MJAcct    = 6303
MJOffice  = 'N'
MJMod     = 'J'
batchNo = PADL(TRANSFORM(dygenpk("mjecom","whj")),9,'0')

         
*!*  dhwInSQL = "select *" +    ***Not using "Select *" because we are joining tables
dhwInSQL = "select iwo.wo_num, wo_date, acct_ref, comments, totqty, style, color, id " +;
           "from inwolog iwo, indet ind " +;
           "where iwo.inwologid = ind.inwologid AND " +;
           "iwo.mod='"+MJMod+"' and iwo.acct_ref='"+inOrdNum+"'"   
         
dhwOsSQL = "select os.ship_ref, od.style, od.color, od.id, od.upc, od.linenum " +;
           "from outship os, outdet od " +;
           "where os.outshipid = od.outshipid AND " +;
           "os.ship_ref='"+inOrdNum+"'"   
lTesting = .F.

DO m:\dev\prg\_setvars WITH lTesting
*!* ON ERROR DEBUG
SET ESCAPE ON
ON ESCAPE CANCEL
NormalExit = .T.

cUseFolder = "F:\whl\whdata\"

*SET STEP ON

	lnSlipAccount = 0
	
	IF xsqlexec(dhwInSQL,"dhwIn",,"wh") = 0
		WAIT WINDOW AT 10,10 "Order not found in INWOLOG:--> "+inOrdNum TIMEOUT 2
	ENDIF
	
	IF xsqlexec(dhwOsSQL,"dhwOs",,"wh") = 0
		WAIT WINDOW AT 10,10 "Order not found in OUTSHIP:--> "+inOrdNum TIMEOUT 2
	ENDIF

	
	SELECT dhwIn
	thisCount = RECCOUNT()
	
	testLineNo = 0
	SCAN
*!*			testLineNo = testLineNo + 1
*!*			useUPC = '99999999999999'
*!*			
*!*			dhwOutSQL = "select * from outdet " +;
*!*	           "where wo_num = '" + dhwIn.acct_ref + "' AND UPC='"+useUPC+"'"
*!*			IF xsqlexec(dhwOutSQL,"dhwOut",,"wh") = 0
*!*				WAIT WINDOW AT 10,10 "Ship_ref not found in OUTDET--> "+dhwIn.acct_ref TIMEOUT 1
*!*				CREATE CURSOR dhwOut (UPC c(14), linenum c(6))
*!*				useUPC = "123456789"+PADL(TRANSFORM(testLineNo),5,'0')
*!*				SELECT dhwOut
*!*				APPEND BLANK 
*!*				REPLACE UPC WITH useUPC IN dhwOut
*!*				REPLACE linenum WITH PADL(TRANSFORM(testLineNo),6,'0') IN dhwOut
*!*				
*!*			ENDIF
		
		custReason = getmemodata("comments", "CUSTRSN")
		tgfReason  = getmemodata("comments", "TGFRSN")
		SELECT DhwOs
		LOCATE FOR style = dhwIn.style AND color = dhwIn.color AND id = dhwIn.id
		
		SELECT whReturn
		APPEND BLANK 
		REPLACE recordID WITH 'D3' IN whReturn
		
********** COMMENTED OUT DURING TESTING - UNCOMMENT WHEN USABLE DATA AVAILABLE
*!*			REPLACE ExtWarehouseNum WITH getmemodata("shipins", "EXTWHNO") IN whReturn
*!*			REPLACE FulfillmentLoc  WITH getmemodata("shipins", "FULFILLOC") IN whReturn
***********************************************************************************
		REPLACE FulfillerStoreNum WITH '419' IN whReturn
		REPLACE TPFStoreNum WITH '00419' IN whReturn
************************************************************************************
		REPLACE OrdInvoiceNum WITH dhwIn.acct_ref IN whReturn
		REPLACE OrdLineNum WITH dhwOs.linenum IN whReturn
		REPLACE PartNum WITH dhwOs.UPC IN whReturn
		REPLACE ReturnQty WITH dhwIn.totqty in whReturn
********** COMMENTED OUT DURING TESTING - UNCOMMENT WHEN USABLE DATA AVAILABLE
		REPLACE CustReasonCd WITH custReason IN whReturn
		REPLACE FulFilReasonCd WITH tgfReason IN whReturn		
		
		REPLACE ReturnDate WITH TTOC(dhwIn.wo_date,1) IN whReturn		
		REPLACE RTANum WITH TRANSFORM(dhwIn.wo_num) IN whReturn
		REPLACE FileCreateDate WITH TTOC(DATETIME(),1)in whReturn
	ENDSCAN 
	**SET STEP ON
	SELECT dhwIn
*!*		BROWSE last
	SELECT whReturn
*!* 	btno = PADL(TRANSFORM(dygenpk("mjecom","whj")),9,'0')
	trailerRec = "T3|" + TRANSFORM(thisCount+1) + '|' + batchNo
	RoutFileName = RadialDir + 'ESSWHR' + TTOC(DATETIME(),1) + '.dat'
	MoutFileName = MJDir + 'ESSWHR' + TTOC(DATETIME(),1) + '.dat'
	COPY TO (RoutFileName) DELIMITED WITH "" WITH CHARACTER '|'
	STRTOFILE(trailerRec, RoutFileName, 1)
	COPY TO (MoutFileName) DELIMITED WITH "" WITH CHARACTER '|'
	STRTOFILE(trailerRec, MoutFileName, 1)

*!*		updSQL = "UPDATE inwolog set confirmsent = .T. where " +;
*!*	           "mod='"+MJMod+"' and acct_ref='"+inOrdNum+"'"   
*!*		
*!*		xsqlexec(updSQL,,,"wh")
*	BROWSE LAST 


