Close Data All

Set Safety Off
Set Exclusive Off


Create Cursor edata (;
isa Char(9),;
ponum Char(15),;
status Char(3),;
pvhstatus char(3),;
statusdt Char(10),;
bsrvalue Char(27),;
bnvalue Char(15),;
filedate Date,;
ackd Char(1),;
filetime char(19),;
time997 char(19),;
filename Char(75))

typecheck="870"

Do Case
Case typecheck ="870"
  lcPath = "f:\ftpusers\pvh\870out\archive\"
  Cd &lcPath
  Wait Window At 10,10 "Creating the file list............." Nowait
  lnNum = Adir(tarray,"*.870")
  Archivebase = "f:\ftpusers\pvh\870out\lastarchive\"

Case typecheck ="997"
  lcPath = "f:\ftpusers\pvh\997in\"
  Cd &lcPath
  Wait Window At 10,10 "Creating the file list............." Nowait
  lnNum = Adir(tarray,"*.*")
  Archivebase = "f:\ftpusers\pvh\997in\archive\"
Endcase

 Archivebase997  = "f:\ftpusers\pvh870in\git-997\"

If lnNum = 0
  close data all 
  Use F:\ftpusers\pvh\Data\pvh870data In 0
  Select pvh870data
  Export To s:\pvh870data\pvh870data.xls Type xls
  Return
Endif

Set Step On

Wait Window "There are "+Alltrim(Str(lnNum))+" files to process" Timeout 1

*c997indir = "F:\0-Picktickets\archive\YellSteel\997in\"
delimchar = "*"
TransOption = "TILDE"

Do m:\dev\prg\createx856

For thisfile = 1  To lnNum

  Xfile = lcPath+Allt(tarray[thisfile,1])
  !Attrib -R &Xfile  && Removes read-only flag from file to allow deletion
  cFilenamearchive = Archivebase+Allt(tarray[thisfile,1])
  cFile997archive  = Archivebase997+Allt(tarray[thisfile,1])

  Select x856
  Zap

  Wait Window "Importing file # "+Transform(thisfile)+" out of "+Transform(lnNum)+" files doing file  "+  Xfile Nowait

  lnNumLines = Alines(a856,Strtran(Filetostr(Xfile),"~",Chr(13)))

  Do justloadedi
  Select x856
  Locate

 If typecheck ="870"
   Do 870bkdn
 else  
  Do 997bkdn
 Endif  

 Try
   Copy File &Xfile To &cFilenamearchive
   if typecheck ="997"
     Copy File &Xfile To &cFile997archive
   Endif 
 Catch
 
 endtry  
 
 
  try
    Erase &Xfile
  Catch
  
  endtry  

Endfor

If typecheck ="870"
  Select edata
  Copy To  F:\ftpusers\pvh\Data\pvh870datatemp
  Use   F:\ftpusers\pvh\Data\pvh870data
  Append From F:\ftpusers\pvh\Data\pvh870datatemp
  close data all 
  ! /N F:\3PL\parse_997s.exe
  close data all 
  Use F:\ftpusers\pvh\Data\pvh870data In 0
  Select pvh870data
  Export To s:\pvh870data\pvh870data.xls Type xls

  Select pvh870data
  Export To h:\fox\pvhdata_jan.xls For filedate > Date()-10 Type xls
  
  tsubject = "PVH Data upload at "+TTOC(DATETIME())
  tattach  = "h:\fox\pvhdata_jan.xls"
  tmessage = "See attached PVH data..........."
  tsendto  ="gaetana.pascazio@tollgroup.com,lorraine.davis@tollgroup.com,Delphine.Chabroux@tollgroup.com"
  tcc      ="pgaidis@fmiint.com"
  tfrom    ="TGF EDI PVH 870/997 Data Update <toll-edi-ops@tollgroup.com>"
  DO FORM dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
Endif

If typecheck ="997"
  Select edata
  Copy To  F:\ftpusers\pvh\Data\pvh997datatemp
  Use   F:\ftpusers\pvh\Data\pvh997data
  Append From F:\ftpusers\pvh\Data\pvh997datatemp

  Close Data All
  Use   F:\ftpusers\pvh\Data\pvh997datatemp Alias pvh997data In 0
  Use   F:\ftpusers\pvh\Data\pvh870data In 0

  i=1
  Select pvh997data
  jj=Alltrim(Str(Reccount()))

  Select pvh997data
  Goto Top
  Scan
    ii=Alltrim(Str(i))
    Wait Window "doing "+ii+" out of "+jj Nowait

    Select pvh870data
    Locate For isa = pvh997data.isa
    If Found()
      Replace pvh870data.rcv997 With "Y"
      Replace pvh997data.ackd With "Y"
      replace time997 With pvh997data.filetime
    Else
      Replace pvh870data.rcv997 With "N"
      Replace pvh997data.ackd With "N"
    Endif
    i=i+1
  Endscan
Endif


*h:\fox\pvh997dataout

**********************************************************************************************
Procedure 870bkdn

Do While !Eof("x856")
  cSegment = Trim(x856.segment)
  cF1 = Trim(x856.f1)

  If Inlist(Trim(x856.segment),"GS","ST")
    Select x856
    If !Eof()
      Skip 1 In x856
      Loop
    Endif
  Endif

  If Trim(x856.segment) = "ISA"
    Select edata
    Scatter Memvar Blank
    m.filename = Alltrim(Lower(tarray[thisfile,1]))
    m.filedate =  tarray[thisfile,3]
    m.isa = Alltrim(x856.f13)
    Skip 1 In x856
    Loop
  Endif

  If Trim(x856.segment) = "BSR"
    m.bsrvalue = Alltrim(x856.f3)
    Skip 1 In x856
    Loop
  Endif

  If Trim(x856.segment) = "REF" And Alltrim(x856.f1) ="BN"
    m.bnvalue = Alltrim(x856.f2)
    Skip 1 In x856
    Loop
  Endif

  If Trim(x856.segment) = "DTM"
    m.status = Alltrim(x856.f1)
    Do Case
     Case m.status = "537"
       m.pvhstatus = "DDA"
       
     Case m.status = "102"
       m.pvhstatus = "FCR"
       
     Case m.status = "ZZZ"
       m.pvhstatus = "CSD"
       
     Case m.status = "AA1"
       m.pvhstatus = "ETA"
       
     Case m.status = "810"
       m.pvhstatus = "FDC"
       
     Case m.status = "922"
       m.pvhstatus = "CRD"
       
     Case m.status = "AA2"
       m.pvhstatus = "ETD"
       
     Case m.status = "135"
       m.pvhstatus = "BNO"
     Otherwise
       m.pvhstatus = "---"
         
    Endcase 
    
    m.statusdt = Alltrim(x856.f2)
    Skip 1 In x856
    Loop
  Endif

  If Trim(x856.segment) = "PRF"
    m.ponum = Alltrim(x856.f1)
    Insert Into edata From Memvar
    Skip 200 In x856
    Loop
  Endif

  Skip 1 In x856

Enddo
*End **************************************************************************************
Procedure 997bkdn

Do While !Eof("x856")
  cSegment = Trim(x856.segment)
  cF1 = Trim(x856.f1)

  If Inlist(Trim(x856.segment),"ISA","AK9","SE","GS","ST")
    Select x856
    If !Eof()
      Skip 1 In x856
      Loop
    Endif
  Endif

  If Trim(x856.segment) = "GS"
    m.statusdt = Alltrim(x856.f4)
    Skip 1 In x856
    Loop
  Endif

  If Trim(x856.segment) = "AK1"
    Select edata
    Scatter Memvar Blank
    m.filename = Alltrim(Lower(tarray[thisfile,1]))
    m.filedate =  tarray[thisfile,3]
    m.isa = Alltrim(x856.f2)
    m.filetime = Substr(m.filename,8,19)
    Insert Into edata From Memvar
    Skip 1 In x856
    Loop
  Endif

  If Trim(x856.segment) = "GE"
    Skip 200 In x856
    Loop
  Endif

  Skip 1 In x856

Enddo
Return
****End **************************************************************************************

Close Data All
Use F:\ftpusers\pvh\Data\pvh997dataout In 0 Alias c997
Use F:\ftpusers\pvh\Data\pvh870dataout In 0


Select pvh870dataout

Scan
  Select c997
  Locate For isa = pvh870dataout.isa
  If Found()
    Replace pvh870dataout.confirmed With "Y"
    Replace pvh870dataout.time997 With c997.filetime
  Else
    Replace pvh870dataout.confirmed With "N" In pvh870dataout
  Endif

Endscan

*End **************************************************************************************


Set Step On
Close Data All
Use   F:\ftpusers\pvh\Data\pvh997datatemp Alias pvh997data In 0
Use   F:\ftpusers\pvh\Data\pvh870data In 0

i=1
Select pvh997data
jj=Alltrim(Str(Reccount()))

Select pvh997data
Goto Top
Scan
  ii=Alltrim(Str(i))
  Wait Window "doing "+ii+" out of "+jj Nowait

  Select pvh870data
  Locate For isa = pvh997data.isa
  If Found()
    Replace pvh870data.rcv997 With "Y"
    Replace pvh997data.ackd With "Y"
  Else
    Replace pvh870data.rcv997 With "N"
    Replace pvh997data.ackd With "N"
  Endif
  i=i+1
Endscan



