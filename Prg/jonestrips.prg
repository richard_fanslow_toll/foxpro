*jonestrips3
*FIRST GET WHICH LANES THE USER WANTS TO REVIEW

store date()-15 to ld_start
do while day(ld_start)>1
	ld_start=ld_start-1
enddo
store ld_start to ld_end
do while month(ld_end)=month(ld_start)
	ld_end=ld_end+1
enddo
ld_end=ld_end-1
lb_abort=.f.

store .f. to zzoriginny, zzoriginla, zzoriginmia
store .t. to zzoriginall
store .f. to zzdesttn, zzdestva, zzdesttx
store .t. to zzdestall

do form m:\dev\frm\get_jonestrips
if lb_abort
	return
endif

if zzoriginall
	store .t. to zzoriginny, zzoriginla, zzoriginmia
endif
if zzdestall
	store .t. to zzdesttn, zzdestva, zzdesttx
endif

wait "Please wait while data is assembled for Excel display..." window nowait


create cursor tempjones (lanectr n(10,0), wo_num n(10,0), wo_date d, orig_wo n(10,0), inctns n(10,0), inctnwt n(10,0), ingoh n(10,0), ingohwt n(10,0),outctns n(10,0), outctnwt n(10,0), outgoh n(10,0), outgohwt n(10,0), totlh n(10,2), totfsc n(10,2), plh n(10,2), pfsc n(10,2))

store 1 to lanecounter
do while lanecounter<7
	do case
	case lanecounter=1 and zzoriginla and zzdestva
		store "C" to toffice
		store "DELIVERY TO VIRGINIA" to tdescription
		store "'VA'" to tdelloc

	case lanecounter=2 and zzoriginla and zzdesttn
		store "C" to toffice
		store "DELIVERY TO TENNESSEE" to tdescription
		store "'TN'" to tdelloc

	case lanecounter=3 and zzoriginla and zzdesttx
		store "C" to toffice
		store "DELIVERY TO EL PASO" to tdescription
		store "'PM','AL1','ALAMEDA','G1','VB1','SOC','SOCORRO'" to tdelloc
	case lanecounter=4 and zzoriginny and zzdestva
		store "N" to toffice
		store "DELIVERY TO VIRGINIA" to tdescription
		store "'VA'" to tdelloc

	case lanecounter=5 and zzoriginny and zzdesttn
		store "N" to toffice
		store "DELIVERY TO TENNESSEE" to tdescription
		store "'TN'" to tdelloc

	case lanecounter=6 and zzoriginny and zzdesttx
		store "N" to toffice
		store "DELIVERY TO EL PASO" to tdescription
		store "'PM','AL1','ALAMEDA','G1','VB1','SOC','SOCORRO'" to tdelloc

	otherwise
		lanecounter=lanecounter+1
		loop
	endcase



	use f:\watusi\ardata\invdet in 0
	use f:\watusi\ardata\invoice in 0
	use f:\wo\wodata\detail order wo_num in 0
	use f:\wo\wodata\manifest order wo_num in 0


		select invoice
		set order to invdt   && INVDT
		go top
		seek ld_start
		do WHILE LD_START<LD_END AND eof()
			ld_start=ld_start+1
			seek ld_start
		ENDDO
		IF LD_START=LD_END
		USE IN INVDET
		USE IN INVOICE
		USE IN DETAIL
		USE IN MANIFEST
	    RETURN
	    ENDIF
		
		

*here you have the closest first record of the start date in invoice. Get the corresponding record in invdet
		store invoiceid to tid
		select invdet
		set order to invoiceid   && INVOICEID
		seek tid
		store recno() to trec



	select invdet
	set order to
	goto trec
	locate for year(adddt)>=year(ld_start) and month(adddt)>=month(ld_start) and day(adddt)>=day(ld_start) ;
	and year(adddt)<=year(ld_end) and month(adddt)<=month(ld_end) and day(adddt)<=day(ld_end) ;
	and description=tdescription while !eof()

	do while !eof()

		store 0 to finvdetamt, xinvdetamt
		store invoiceid to tid
		store 0 to findetamt, xindetamt
		skip
		store recno() to thisrec
		store 0 to desc
		do while invoiceid=tid and !description='DELIVERY TO' and !eof()
			if company='F'
				findetamt=findetamt+invdetamt
				if at("LBS",description)>0
					store alltrim(substr(description,at("LINEHAUL",description)+9,(at("LBS",description)-1)-(at("LINEHAUL",description)+9))) to tdesc
				else
					if at("CTNS LINEHAUL",description)>0
						store alltrim(substr(description,at("CTNS LINEHAUL",description)+15,at("LBS",description)-(at("CTNS LINEHAUL",description)+15))) to tdesc
					else
						if at("GOH LINEHAUL",description)>0
							store alltrim(substr(description,1,(at("GOH",description)-1))) to tdesc
						else
							store alltrim(substr(description,at("LINEHAUL",description)+9,(at("GOH",description)-1)-(at("LINEHAUL",description)+9))) to tdesc
						endif
					endif
				endif
			endif
			skip
		enddo
		goto thisrec
		store .f. to foundafuelsurchargematch
		do while invoiceid=tid and !eof()
			if company='X' and at(tdesc,description)>0
				xindetamt=xindetamt+invdetamt
				store .t. to foundafuelsurchargematch
			endif
			skip
		enddo

		if !foundafuelsurchargematch
			goto thisrec
			do while invoiceid=tid and !eof()
				if company='X'
					xindetamt=xindetamt+invdetamt
				endif
				skip
			enddo
		endif



		select invoice
		set order to invoiceid
		seek tid
		store wo_num to torigwo
		select detail
		seek torigwo

		if !office=toffice
			select invdet
			continue
			loop
		endif



		store 0 to dctns, dctnwt, dgoh, dgohwt
		do while wo_num=torigwo and !eof()
			if inlist(delloc,&tdelloc) and !qty_type='GOH'
				dctns=dctns+rcv_qty
				dctnwt=dctnwt+weight
			endif
			if inlist(delloc,&tdelloc) and qty_type='GOH'
				dgoh=dgoh+rcv_qty
				dgohwt=dgohwt+weight
			endif
			skip
		enddo

		select manifest
		set order to orig_wo
		seek torigwo
		do while orig_wo=torigwo and !eof()
			if !inlist(delloc,&tdelloc)
				skip
				loop
			endif

			store wo_num to ttrip
			store wo_date to tdate

			select tempjones
			locate for wo_num=ttrip and orig_wo=torigwo and lanectr=lanecounter
			if eof()
				append blank
				replace lanectr with lanecounter
			endif

			replace wo_num with ttrip
			replace orig_wo with torigwo
			replace wo_date with tdate
			if manifest.qty_type='GOH'
				replace outgoh with outgoh+manifest.quantity
				replace outgohwt with outgohwt+manifest.weight
			else
				replace outctns with outctns+manifest.quantity
				replace outctnwt with outctnwt+manifest.weight
			endif
			select manifest
			skip
		enddo


		select tempjones
		locate for orig_wo=torigwo and lanectr=lanecounter
		do while orig_wo=torigwo and !eof()
			replace inctns with dctns
			replace inctnwt with dctnwt
			replace ingoh with dgoh
			replace ingohwt with dgohwt
			replace totlh with findetamt
			replace totfsc with xindetamt
			if outctnwt>0
				replace plh with (outctnwt/dctnwt)*findetamt
				replace pfsc with (outctnwt/dctnwt)*xindetamt
			else
				replace plh with (outgoh/dgoh)*findetamt
				replace pfsc with (outgoh/dgoh)*xindetamt
			endif
			continue
		enddo



		select invdet
		continue
	enddo

	use in invdet
	use in invoice
	use in manifest
	use in detail


****
	lanecounter=lanecounter+1
enddo




select tempjones
go top
store 1 to lanecounter

*here you need to open the excel sheet and populate for each lane
oexcel=createobject("excel.application")
oworkbook=oexcel.workbooks.open("F:\MAIN\jonestrips.xls")
osheet1=oworkbook.worksheets[1]
osheet2=oworkbook.worksheets[2]
osheet3=oworkbook.worksheets[3]
osheet4=oworkbook.worksheets[4]
osheet5=oworkbook.worksheets[5]
osheet6=oworkbook.worksheets[6]
osheet7=oworkbook.worksheets[7]



do while lanecounter<7
	select tempjones
	set filter to lanectr=lanecounter
	go top
	store "osheet"+alltrim(str(lanecounter+1)) to whichsheet


	with &whichsheet
		.range("A3").value = "For the period "+trans(ld_start)+"  to "+trans(ld_end)


		i=6
		store 0 to gt1,gt2,gt3,gt4,gt5,gt6,gt7,gt8,gt9,gt10,gt11,gt12
		scan
			i=i+1
			ii=trans(i)
			.range("A"+ii).value=trans(wo_num)
			.range("B"+ii).value=trans(wo_date)
			.range("C"+ii).value=trans(orig_wo)
			.range("D"+ii).value=trans(inctns)
			.range("E"+ii).value=trans(inctnwt)
			.range("F"+ii).value=trans(ingoh)
			.range("G"+ii).value=trans(ingohwt)
			.range("H"+ii).value=trans(outctns)
			.range("I"+ii).value=trans(outctnwt)
			.range("J"+ii).value=trans(outgoh)
			.range("K"+ii).value=trans(outgohwt)
			.range("L"+ii).value=trans(totlh)
			.range("M"+ii).value=trans(totfsc)
			.range("N"+ii).value=trans(plh)
			.range("O"+ii).value=trans(pfsc)
			gt1=gt1+inctns
			gt2=gt2+inctnwt
			gt3=gt3+ingoh
			gt4=gt4+ingohwt
			gt5=gt5+outctns
			gt6=gt6+outctnwt
			gt7=gt7+outgoh
			gt8=gt8+outgohwt
			gt9=gt9+totlh
			gt10=gt10+totfsc
			gt11=gt11+plh
			gt12=gt12+pfsc

		endscan
	endwith

	i=i+1
	ii=trans(i)
	with &whichsheet
		.range("D"+ii).value=transform(gt1)
		.range("E"+ii).value=transform(gt2)
		.range("F"+ii).value=transform(gt3)
		.range("G"+ii).value=transform(gt4)
		.range("H"+ii).value=transform(gt5)
		.range("I"+ii).value=transform(gt6)
		.range("J"+ii).value=transform(gt7)
		.range("K"+ii).value=transform(gt8)
		.range("L"+ii).value=transform(gt9)
		.range("M"+ii).value=transform(gt10)
		.range("N"+ii).value=transform(gt11)
		.range("O"+ii).value=transform(gt12)
	endwith

	select tempjones
	index on wo_num tag wo_num unique
	count to tt
*where TT = the number of trips for this lane

	i=i+2
	ii=trans(i)
	with &whichsheet
		.range("E"+ii).value="Number of Trips this Period:"
		.range("H"+ii).value=transform(tt)
	endwith
	i=i+1
	ii=trans(i)
	store round((gt6+gt8)/tt,0) to tavg
	with &whichsheet
		.range("E"+ii).value="Average Trailer Weight:"
		.range("H"+ii).value=transform(tavg)
	endwith
	i=i+1
	ii=trans(i)
	store round((gt11)/tt,2) to tavg
	if lanecounter=2 or lanecounter=5
		with &whichsheet
			.range("E"+ii).value="*Average Linehaul per Trip:"
			.range("H"+ii).value=transform(tavg)
			.range("H"+ii).numberformat = "$#,##0.00"

		endwith
	else
		with &whichsheet
			.range("E"+ii).value="Average Linehaul per Trip:"
			.range("H"+ii).value=transform(tavg)
			.range("H"+ii).numberformat = "$#,##0.00"
		endwith
	endif
	i=i+1
	ii=trans(i)
	store round((gt12)/tt,2) to tavg
	with &whichsheet
		.range("E"+ii).value="Average FSC per Trip:"
		.range("H"+ii).value=transform(tavg)
		.range("H"+ii).numberformat = "$#,##0.00"
	endwith
	i=i+1
	ii=trans(i)
	store round((gt11+gt12)/tt,0) to tavg
	with &whichsheet
		.range("E"+ii).value="Average Total per Trip:"
		.range("H"+ii).value=transform(tavg)
		.range("H"+ii).numberformat = "$#,##0.00"
	endwith


	if lanecounter=2 or lanecounter=5
		i=i+2
		ii=trans(i)
		with &whichsheet
			.range("D"+ii).value="NOTE: Linehaul rates included switcher charges, trailer detention and cargo unloading at destination"
		endwith
		i=i+1
		ii=trans(i)
		with &whichsheet
			.range("D"+ii).value="prior to November 2009"
		endwith
	endif



*Summary on first page
	store "osheet1" to whichsheet

	with &whichsheet
		.range("A3").value = "For the period "+trans(ld_start)+"  to "+trans(ld_end)
	endwith
	do case
	case lanecounter=1
		i=7
	case lanecounter=2
		i=8
	case lanecounter=3
		i=9
	case lanecounter=4
		i=10
	case lanecounter=5
		i=11
	case lanecounter=6
		i=12
	endcase

    IF tt=0
    store 0 to avgtrailerwt
    else
	store round((gt6+gt8)/tt,0) to avgtrailerwt
	endif
	ii=trans(i)
	with &whichsheet
		.range("C"+ii).value=trans(tt)
		.range("D"+ii).value=trans(gt5)
		.range("E"+ii).value=trans(gt6)
		.range("F"+ii).value=trans(gt7)
		.range("G"+ii).value=trans(gt8)
		.range("H"+ii).value=trans(avgtrailerwt)
		.range("I"+ii).value=trans(gt11)
		.range("J"+ii).value=trans(gt12)
	endwith

	lanecounter=lanecounter+1
enddo


oexcel.visible=.t.
wait clear


* SAVE
delete file h:\fox\jonestrips.xls
xfile="h:\fox\jonestrips.xls"
oworkbook.saveas(xfile)
clear