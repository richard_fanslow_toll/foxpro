utilsetup("MJ_997_PROCESS")

ctransfer='MJ_997_PROCESS'

lcDELIMITER = "*"
lcTranslateOption ="MJ997"
delimchar = lcDELIMITER
lcTranOpt= lcTranslateOption

*!*	Do m:\dev\prg\997_resend_cleanup WITH 6303
*!*	Do m:\dev\prg\997_resend_cleanup WITH 6325
*!*	Do m:\dev\prg\997_resend_cleanup WITH 6543

DO m:\dev\prg\createx856

*******************************************************************

lcpath="F:\FTPUSERS\MJ_Wholesale\WMS_Outbound\997in\"
lcarchivepath="F:\FTPUSERS\MJ_Wholesale\WMS_Outbound\997in\archive\"

*CD &lcpath
CD m:\dev
lnNum = ADIR(tarray,"F:\FTPUSERS\MJ_Wholesale\WMS_Outbound\997in\*.*")

IF lnNum = 0
	WAIT WINDOW AT 10,10 "    No MJ 997's to import.............." TIMEOUT 2
	USE F:\edirouting\ftpsetup SHARED
	LOCATE FOR ftpsetup.transfer =  ctransfer
	IF FOUND()
		REPLACE chkbusy WITH .F. FOR ftpsetup.transfer = ctransfer
	ENDIF
	NormalExit = .T.

*    Throw
ENDIF

FOR thisfile = 1  TO lnNum
* Archivename = Ttoc(Datetime(),1)
	xfile = lcpath+tarray[thisfile,1]+"."

*Xfile = lcPath+Allt(tarray[thisfile,1])
	!ATTRIB -R &xfile  && Removes read-only flag from file to allow deletion

	archivefile = lcarchivepath+tarray[thisfile,1]
	lcFilename = tarray[thisfile,1]
	WAIT WINDOW "Importing file: "+xfile NOWAIT
	IF FILE(xfile)
* load the file into the 856 array
		SET STEP ON
		SELECT x856
		ZAP
		DO m:\dev\prg\loadedifile WITH xfile,delimchar,lcTranOpt,"MJ997" &&"NAUTICA"
		lProcessOK = .F.

		DO m:\dev\prg\mj_997_bkdn WITH xfile  && do the bkdn and reconile the 997 into ACKDATA

		IF lProcessOK
			COPY FILE &xfile TO &archivefile
			IF FILE(archivefile)
				DELETE FILE &xfile
			ENDIF
		ENDIF
	ENDIF

NEXT thisfile

&&&& now run through and look for errors

DO m:\dev\prg\mj_997_status_check  && do the bkdn and reconile the 997 into ACKDATA



**********Added 1/30/2015 for ML  TM

DO m:\dev\prg\createx856

*******************************************************************

lcpath="F:\FTPUSERS\MJ_Wholesale_CA\WMS_Out\997in\"
lcarchivepath="F:\FTPUSERS\MJ_Wholesale_CA\WMS_Out\997in\archive\"

CD &lcpath

lnNum = ADIR(tarray,"F:\FTPUSERS\MJ_Wholesale_CA\WMS_Out\997in\*.*")

IF lnNum = 0
	WAIT WINDOW AT 10,10 "    No MJ 997's to import.............." TIMEOUT 2
	IF USED('ftpsetup')
		USE IN ftpsetup
	ENDIF
	USE F:\edirouting\ftpsetup SHARED
	LOCATE FOR ftpsetup.transfer =  ctransfer
	IF FOUND()
		REPLACE chkbusy WITH .F. FOR ftpsetup.transfer = ctransfer
	ENDIF
	NormalExit = .T.

*    Throw
ENDIF

FOR thisfile = 1  TO lnNum
* Archivename = Ttoc(Datetime(),1)
	xfile = lcpath+tarray[thisfile,1]+"."

*Xfile = lcPath+Allt(tarray[thisfile,1])
	!ATTRIB -R &xfile  && Removes read-only flag from file to allow deletion

	archivefile = lcarchivepath+tarray[thisfile,1]
	lcFilename = tarray[thisfile,1]
	WAIT WINDOW "Importing file: "+xfile NOWAIT
	IF FILE(xfile)
* load the file into the 856 array
		SET STEP ON
		SELECT x856
		ZAP
		DO m:\dev\prg\loadedifile WITH xfile,delimchar,lcTranOpt,"MJ997" &&"NAUTICA"
		lProcessOK = .F.

		DO m:\dev\prg\mj_997_bkdn WITH xfile  && do the bkdn and reconile the 997 into ACKDATA

		IF lProcessOK
			COPY FILE &xfile TO &archivefile
			IF FILE(archivefile)
				DELETE FILE &xfile
			ENDIF
		ENDIF
	ENDIF

NEXT thisfile

&&&& now run through and look for errors
DO m:\dev\prg\997_resend_cleanup WITH 6303
DO m:\dev\prg\997_resend_cleanup WITH 6325
DO m:\dev\prg\997_resend_cleanup WITH 6543
DO m:\dev\prg\mj_997_status_check  && do the bkdn and reconile the 997 into ACKDATA

*

schedupdate()

_screen.Caption=gscreencaption
on error
