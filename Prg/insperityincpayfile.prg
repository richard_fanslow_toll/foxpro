* Create a weekly incentive pay csv file for Insperity.
* to be run every Tuesday around 10:15am

* EXE = F:\UTIL\INSPERITY\INSPERITYINCPAYFILE.EXE

runack("INSPERITYINCPAYFILE")

LOCAL lnError, lcProcessName, loINSPERITYINCPAYFILE

DECLARE INTEGER CreateMutex IN Win32API ;
	STRING @lpMutexAttributes, ;
	STRING bInitialOwner, ;
	STRING @lpName

DECLARE INTEGER GetLastError IN Win32API

* if running in EXE check to see if already running
IF _VFP.STARTMODE > 0 THEN
	lcProcessName = "INSPERITYINCPAYFILE"
	CreateMutex(0,CHR(1),@lcProcessName)
	lnError = GetLastError()
	IF lnError = 183
		WAIT WINDOW TIMEOUT 10 "Insperity Incentive Pay Export process is already running..."
		RETURN .F.
	ENDIF
ENDIF

utilsetup("INSPERITYINCPAYFILE")


loINSPERITYINCPAYFILE = CREATEOBJECT('INSPERITYINCPAYFILE')
loINSPERITYINCPAYFILE.MAIN()

schedupdate()

CLOSE DATABASES ALL
RETURN

#DEFINE LOGIT 1
#DEFINE WAITIT 2
#DEFINE NOWAITIT 4
#DEFINE SENDIT 8
#DEFINE CRLF CHR(13) + CHR(10)
#DEFINE LF CHR(10)
#DEFINE RETURN_DATA_MANDATORY .F.
#DEFINE RETURN_DATA_NOT_MANDATORY .T.
#DEFINE DAILY_OT_MULTIPLIER 1.5
#DEFINE DAILY_OT20_MULTIPLIER 2.0
#DEFINE xlEdgeBottom 9
#DEFINE xlContinuous 1
#DEFINE xlMedium -4138

DEFINE CLASS INSPERITYINCPAYFILE AS CUSTOM

	cProcessName = 'INSPERITYINCPAYFILE'

	lTestMode = .F.  && .F. FOR NORMAL OPERATION, .T. TO SUPPRESS ALL BUT DEVELOPER EMAILS AND CHANGE INPUT TABLE, ETC.

	lAutoYield = .T.

	cStartTime = TTOC(DATETIME())

	* file properties
	nFileHandle = 0

	* date properties
	dtNow = DATETIME()

	dToday = DATE()

	cToday = DTOC(DATE())

	* wait window properties
	nWaitWindowTimeout = 2
	lWaitWindowIsOn = .T.

	* logfile properties
	lLoggingIsOn = .T.
	cLogFile = 'F:\UTIL\INSPERITY\Logfiles\INSPERITYINCPAYFILE_log.txt'

	* INTERNAL email properties
	lSendInternalEmailIsOn = .T.
	cFrom = 'Mark.Bennett@Tollgroup.com'
	cSendTo = 'Mark.Bennett@Tollgroup.com'
	cCC = 'lauren.wojcik@tollgroup.com, marie.freiberger@tollgroup.com, abigail.melaika@tollgroup.com'
	cSubject = 'Insperity Incentive Pay Export process'
	cAttach = ''
	cBodyText = ''
	cTopBodyText = ''

	cCOMPUTERNAME = ''
	cUSERNAME = ''

	FUNCTION INIT
		IF NOT DODEFAULT()
			RETURN .F.
		ENDIF
		WITH THIS
			CLEAR
			*SET RESOURCE OFF
			CLOSE DATA
			SET CENTURY ON
			SET DATE AMERICAN
			SET HOURS TO 24
			SET ANSI ON
			SET TALK OFF
			SET DELETED ON
			SET CONSOLE OFF
			SET EXCLUSIVE OFF
			SET SAFETY OFF
			SET EXACT OFF
			SET STATUS BAR ON
			SET SYSMENU OFF
			SET ENGINEBEHAVIOR 70
			_VFP.AUTOYIELD = .lAutoYield
			.cCOMPUTERNAME = GETENV("COMPUTERNAME")
			.cUSERNAME = GETENV("USERNAME")
			IF .lTestMode THEN
				.cSendTo = 'mbennett@fmiint.com'
				.cCC = ''
				.cLogFile = 'F:\UTIL\INSPERITY\Logfiles\INSPERITYINCPAYFILE_log_TESTMODE.txt'
			ENDIF
			.lLoggingIsOn = .lLoggingIsOn AND NOT EMPTY(.cLogFile)
		ENDWITH
	ENDFUNC


	FUNCTION DESTROY
		WITH THIS
			IF .lLoggingIsOn  THEN
				SET ALTERNATE OFF
				SET ALTERNATE TO
			ENDIF
		ENDWITH
		DODEFAULT()
	ENDFUNC


	FUNCTION MAIN
		LPARAMETERS tcMode
		WITH THIS
			LOCAL lnNumberOfErrors, lcOutputFileCSV, lcXLFileNameCA, lcXLFileNameNJ, ldPayDate
			LOCAL lnRow, lcRow, lcString, llAFileWasMissing, lcXLFileNameCA_Archived, lcXLFileNameNJ_Archived
			PRIVATE oExcel, oWorkbook, oWorksheet

			TRY
				lnNumberOfErrors = 0

				IF .lLoggingIsOn THEN
					SET ALTERNATE TO (.cLogFile) ADDITIVE
					SET ALTERNATE ON
				ENDIF

				.TrackProgress('Insperity Incentive Pay Export process started....', LOGIT+SENDIT+NOWAITIT)
				.TrackProgress('COMPUTERNAME = ' + .cCOMPUTERNAME, LOGIT+SENDIT)
				.TrackProgress('USERNAME = ' + .cUSERNAME, LOGIT+SENDIT)
				.TrackProgress('PROJECT = INSPERITYINCPAYFILE', LOGIT+SENDIT)
				IF .lTestMode THEN
					.TrackProgress('                        TEST MODE                        ', LOGIT+SENDIT)
				ENDIF

				ldToday = .dToday

				*!*	*!*	*************************************************************************
				*!*	*!*	* to force date
				*ldToday = {^2014-05-27}
				*!*	*!*	*************************************************************************

				* calc paydate (should be the following friday)
				ldPayDate = ldToday
				DO WHILE UPPER(CDOW(ldPayDate)) <> "FRIDAY"
					ldPayDate = ldPayDate + 1
				ENDDO

				.cSubject = 'Insperity Incentive Pay Export process for paydate : ' + DTOC(ldPayDate)

				IF .lTestMode THEN
					*lcOutputFileCSV = "F:\UTIL\INSPERITY\INCPAY\TESTINCPAYOUT\TOLLINCPAY" + DTOS(ldPayDate) + ".CSV"
					lcOutputFileCSV = "F:\UTIL\INSPERITY\INCPAY\TESTINCPAYOUT\TOLLINCPAY.CSV"
				ELSE
					* PRODUCTION
					*lcOutputFileCSV = "F:\FTPUSERS\INSPERITY\OUT\INCPAY\TOLLINCPAY" + DTOS(ldPayDate) + ".CSV"
					lcOutputFileCSV = "F:\FTPUSERS\INSPERITY\OUT\INCPAY\TOLLINCPAY.CSV"
				ENDIF

				lcXLFileNameCA = "F:\UTIL\INSPERITY\INCPAY\CADRIVERINCPAY" + DTOS(ldPayDate) + ".XLS"

				lcXLFileNameNJ = "F:\UTIL\INSPERITY\INCPAY\NJDRIVERINCPAY" + DTOS(ldPayDate) + ".XLS"

				lcXLFileNameCA_Archived = "F:\UTIL\INSPERITY\INCPAY\ARCHIVED\CADRIVERINCPAY" + DTOS(ldPayDate) + ".XLS"

				lcXLFileNameNJ_Archived = "F:\UTIL\INSPERITY\INCPAY\ARCHIVED\NJDRIVERINCPAY" + DTOS(ldPayDate) + ".XLS"

				.TrackProgress('lcOutputFileCSV = ' + lcOutputFileCSV, LOGIT+SENDIT)

				.TrackProgress('lcXLFileNameCA = ' + lcXLFileNameCA, LOGIT+SENDIT)

				.TrackProgress('lcXLFileNameNJ = ' + lcXLFileNameNJ, LOGIT+SENDIT)

				.TrackProgress('lcXLFileNameCA_Archived = ' + lcXLFileNameCA_Archived, LOGIT+SENDIT)

				.TrackProgress('lcXLFileNameNJ_Archived = ' + lcXLFileNameNJ_Archived, LOGIT+SENDIT)

				* DELETE THE OUTPUT FTP FILE FROM PREVIOUS WEEK IF IT IS STILL THERE...so Marie can't accidentally resend it this week.
				IF FILE(lcOutputFileCSV) THEN
					DELETE FILE (lcOutputFileCSV)
				ENDIF

				llAFileWasMissing = .F.

				* CHECK FOR NEW SOURCE FILES...
				IF NOT FILE(lcXLFileNameCA) THEN
					.TrackProgress('**** ERROR: could not find the CA Incentive Pay spreadsheet: ' + lcXLFileNameCA, LOGIT+SENDIT)
					.TrackProgress('===> Ask Cesar Nevares to run the export!', LOGIT+SENDIT)
					llAFileWasMissing = .T.
				ENDIF

				IF NOT FILE(lcXLFileNameNJ) THEN
					.TrackProgress('**** ERROR: could not find the NJ Incentive Pay spreadsheet: ' + lcXLFileNameNJ, LOGIT+SENDIT)
					.TrackProgress('===> Ask Mike Drew to run the export!', LOGIT+SENDIT)
					llAFileWasMissing = .T.
				ENDIF

				IF llAFileWasMissing THEN
					THROW
				ENDIF

				* create cursor to hold data from both spreadsheets
				IF USED('CURINCPAY') THEN
					USE IN CURINCPAY
				ENDIF
				CREATE CURSOR CURINCPAY (LEGALLN C(30), INSPID I, INCPAY N(6,2))

				oExcel = CREATEOBJECT("excel.application")
				oExcel.displayalerts = .F.
				oExcel.VISIBLE = .F.

				*****************************************************

				* process CA file
				.TrackProgress("===> Processing file: " + lcXLFileNameCA,LOGIT+SENDIT)
				oWorkbook = oExcel.workbooks.OPEN(lcXLFileNameCA)
				oWorksheet = oWorkbook.Worksheets[1]

				.ProcessSpreadsheet()

				oWorkbook.CLOSE()

				*****************************************************

				* process NJ file
				.TrackProgress("===> Processing file: " + lcXLFileNameNJ,LOGIT+SENDIT)
				oWorkbook = oExcel.workbooks.OPEN(lcXLFileNameNJ)
				oWorksheet = oWorkbook.Worksheets[1]

				.ProcessSpreadsheet()

				oWorkbook.CLOSE()

				*****************************************************

				oExcel.QUIT()
				


				*!*					SELECT CURINCPAY
				*!*					BROWSE

				* write cursor to output csv file in FTP out folder
				* open file for writing
				.nFileHandle = FCREATE(lcOutputFileCSV)

				IF .nFileHandle = -1 THEN
					.TrackProgress("There was an error FCREATEing " + lcOutputFileCSV,LOGIT+SENDIT)
					THROW
				ENDIF

				* file has been opened for low-level access, OK TO CONTINUE

				* do --not-- write header line per Josh 4/23/14
				*!*	* write header line
				*!*	lcString = "legalln,inspid,incpay"
				*!*	FPUTS(.nFileHandle,lcString)

				* write data lines
				SELECT CURINCPAY
				SCAN
					lcString = ALLTRIM(CURINCPAY.LEGALLN) + "," + ALLTRIM(TRANSFORM(CURINCPAY.INSPID)) + "," + ALLTRIM(TRANSFORM(CURINCPAY.INCPAY))
					FPUTS(.nFileHandle,lcString)
				ENDSCAN

				* force Windows to flush buffers immediately
				=FFLUSH(.nFileHandle, .T.)

				* close the CSV file
				IF FCLOSE(.nFileHandle) THEN
					.nFileHandle = 0
				ELSE
					.TrackProgress("There was an error FCLOSEing " + lcOutputFileCSV,LOGIT+SENDIT)
				ENDIF

				* ARCHIVE THE SOURCE SPREADSHEETS
				COPY FILE (lcXLFileNameCA) TO (lcXLFileNameCA_Archived)
				COPY FILE (lcXLFileNameNJ) TO (lcXLFileNameNJ_Archived)
				
				IF FILE(lcXLFileNameCA) THEN
					DELETE FILE (lcXLFileNameCA)
				ELSE
					.TrackProgress("===> There was an error deleting file: " + lcXLFileNameCA,LOGIT+SENDIT)					
				ENDIF
				
				IF FILE(lcXLFileNameNJ) THEN
					DELETE FILE (lcXLFileNameNJ)
				ELSE
					.TrackProgress("===> There was an error deleting file: " + lcXLFileNameNJ,LOGIT+SENDIT)					
				ENDIF
				

				IF FILE(lcOutputFileCSV) THEN
				
					* trigger the FTP put on FTP1
					INSERT INTO F:\EDIROUTING\SFTPJOBS (JOBNAME) VALUES ("INSPERITY-INCPAY-PUT")

					* attach output file to email
					.cAttach = lcOutputFileCSV
					.cBodyText = "====> Triggered the FTP upload for the Incentive Pay file on FTP1" + ;
						CRLF + CRLF + "(do not reply - this is an automated report)" + ;
						CRLF + CRLF + "<report log follows>" + ;
						CRLF + .cBodyText
						
					.TrackProgress('The process completed normally.', LOGIT+SENDIT)
				ELSE
					.TrackProgress('****************************************************************', LOGIT+SENDIT)
					.TrackProgress('==========> ERROR creating .csv file: ' + lcOutputFileCSV, LOGIT+SENDIT)
				ENDIF

			CATCH TO loError

				.TrackProgress('There was an error.',LOGIT+SENDIT)
				.TrackProgress('The Error # is: ' + TRANSFORM(loError.ERRORNO), LOGIT+SENDIT)
				.TrackProgress('The Error Message is: ' + TRANSFORM(loError.MESSAGE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred in Program: ' + TRANSFORM(loError.PROCEDURE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred at line #: ' + TRANSFORM(loError.LINENO), LOGIT+SENDIT)
				lnNumberOfErrors = lnNumberOfErrors + 1
				IF TYPE('oExcel') = 'O' AND NOT ISNULL(oExcel) THEN
					oExcel.QUIT()
				ENDIF
				CLOSE DATA

			ENDTRY

			CLOSE DATA
			WAIT CLEAR
			***************** INTERNAL email results ******************************
			.TrackProgress('About to send status email.',LOGIT)
			.TrackProgress('==================================================================================================================', SENDIT)
			.TrackProgress('The logfile is: ' + .cLogFile, SENDIT)
			.TrackProgress('Insperity Incentive Pay Export process started: ' + .cStartTime, LOGIT+SENDIT)
			.TrackProgress('Insperity Incentive Pay Export process finished: ' + TTOC(DATETIME()), LOGIT+SENDIT)

			IF .lSendInternalEmailIsOn THEN
				* try to trap error from not having dartmail dll's registered on user's pc...
				TRY
					.cBodyText = .cTopBodyText + CRLF + .cBodyText

					DO FORM dartmail2 WITH .cSendTo,.cFrom,.cSubject,.cCC,.cAttach,.cBodyText,"A"
					.TrackProgress('Sent status email.',LOGIT)
				CATCH TO loError
					*=MESSAGEBOX('There was an error sending the status email. Please contact Mark Bennett.',0+64,"KRONOS Temp Time")
					.TrackProgress('There was an error sending the status email.',LOGIT)
					.TrackProgress('The Error # is: ' + TRANSFORM(loError.ERRORNO), LOGIT)
					.TrackProgress('The Error Message is: ' + TRANSFORM(loError.MESSAGE), LOGIT)
					.TrackProgress('The Error occurred in Program: ' + TRANSFORM(loError.PROCEDURE), LOGIT)
					.TrackProgress('The Error occurred at line #: ' + TRANSFORM(loError.LINENO), LOGIT)
					lnNumberOfErrors = lnNumberOfErrors + 1
					CLOSE DATA
				ENDTRY

			ELSE
				.TrackProgress('Did not send status email: THIS.lSendInternalEmailIsOn = FALSE.',LOGIT)
			ENDIF

		ENDWITH
		RETURN
	ENDFUNC && main


	PROCEDURE ProcessSpreadsheet
		WITH THIS
			LOCAL lnRow, lcRow
			PRIVATE m.LEGALLN, m.INSPID, m.INCPAY, m.XWEEKOF, m.DIS_DRIVER, m.EMP_NUM

			lnRow = 2
			lcRow = ALLTRIM(STR(lnRow))

			DO WHILE (lnRow < 500)

				m.LEGALLN = NVL(oWorksheet.RANGE("A"+lcRow).VALUE,"")
				m.INSPID = NVL(oWorksheet.RANGE("B"+lcRow).VALUE,0)
				m.INCPAY = NVL(oWorksheet.RANGE("C"+lcRow).VALUE,0.00)
				m.XWEEKOF = NVL(oWorksheet.RANGE("D"+lcRow).VALUE,CTOD(''))
				m.DIS_DRIVER = NVL(oWorksheet.RANGE("E"+lcRow).VALUE,'')
				m.EMP_NUM = NVL(oWorksheet.RANGE("F"+lcRow).VALUE,0)

				DO CASE
					CASE (EMPTY(m.LEGALLN)) AND (EMPTY(m.INSPID)) AND (EMPTY(m.INSPID)) AND (EMPTY(m.DIS_DRIVER)) AND (EMPTY(m.EMP_NUM))
						* empty row encountered - this should signal that we are done processing, but...
						IF lnRow < 30 THEN
							* WARN OF POSSIBLE EARLY EXIT
							.TrackProgress("********************************************",LOGIT+SENDIT)
							.TrackProgress("****  WARNING: exited processing at row " + TRANSFORM(lnRow),LOGIT+SENDIT)
							.TrackProgress("****  Check the spreadsheet!!!",LOGIT+SENDIT)
							.TrackProgress("********************************************",LOGIT+SENDIT)
						ENDIF
						EXIT
					CASE (NOT EMPTY(m.LEGALLN)) AND (NOT EMPTY(m.INSPID)) AND (NOT EMPTY(m.INSPID)) AND (NOT EMPTY(m.DIS_DRIVER)) AND (NOT EMPTY(m.EMP_NUM))
						* NOTHING - okay to proceed
					OTHERWISE
						* some data is missing in the row - throw an error message
						.TrackProgress("********************************",LOGIT+SENDIT)
						.TrackProgress("********************************",LOGIT+SENDIT)
						.TrackProgress("****  MISSING DATA ERROR in row " + TRANSFORM(lnRow),LOGIT+SENDIT)
						.TrackProgress("****  Check spreadsheet!!!",LOGIT+SENDIT)
						.TrackProgress("********************************",LOGIT+SENDIT)
						.TrackProgress("********************************",LOGIT+SENDIT)
						.cSubject = 'ERRORS in Insperity Incentive Pay Export!'
						THROW
				ENDCASE

				INSERT INTO CURINCPAY FROM MEMVAR
				lnRow = lnRow + 1
				lcRow = ALLTRIM(STR(lnRow))
			ENDDO

		ENDWITH
		RETURN
	ENDPROC


	PROCEDURE TrackProgress
		* do any combination of Wait Window, writing to logfile, and adding to body of email,
		* based on nFlags parameter.
		LPARAMETERS tcExpression, tnFlags
		WITH THIS
			IF BITAND(tnFlags,LOGIT) = LOGIT THEN
				IF .lLoggingIsOn THEN
					?
					? .cProcessName + "  " + TTOC(DATETIME()) + ": " + tcExpression
				ENDIF
			ENDIF
			IF .lWaitWindowIsOn THEN
				IF BITAND(tnFlags,NOWAITIT) = NOWAITIT THEN
					WAIT WINDOW tcExpression NOWAIT
				ENDIF
				IF BITAND(tnFlags,WAITIT) = WAITIT THEN
					WAIT WINDOW tcExpression TIMEOUT .nWaitWindowTimeout
				ENDIF
			ENDIF
			IF BITAND(tnFlags,SENDIT) = SENDIT THEN
				IF .lSendInternalEmailIsOn THEN
					.cBodyText = .cBodyText + tcExpression + CRLF + CRLF
				ENDIF
			ENDIF
		ENDWITH
	ENDPROC  &&  TrackProgress




ENDDEFINE
