*!* m:\dev\prg\samsung940_main.prg

runack("SAMSUNG940")
CLOSE DATA ALL

PUBLIC nAcctNum,cDelimiter,nRun_num,cTranslateOption,cWhichFile,cOffice,cCustname,cPropername,cStorenum
PUBLIC cFilter,nRunID,lComplete,lTesting,m.UCCNUMBER,lEmail,lTestImport,emailcommentstr,cUseFolder
PUBLIC ptid,ptctr,ptqty,xfile,cfilename,UploadCount,pickticket_num_start,pickticket_num_end,nFileCount,units,lBrowfiles,gOffice
PUBLIC cOldScreen,m.addby,m.adddt,m.addproc,tsendto,tcc,archivefile,lcarchivepath,thiscaption,cISA_Num,nFileSize,nPTLoadCount,cPTLoadCount
PUBLIC NormalExit,cTransfer,lD2S,thiscaption,lCartonAccount,lcPath,tfrom,tsendtoerr,tccerr,tsendtotest,tcctest,lLoadSQL,cMod,lnRunID
PUBLIC ARRAY a856(1)

lTesting = .F.

lEmail = .T.

DO M:\DEV\PRG\_SETVARS WITH lTesting
lSkipFTPcheck = lTesting
lOverrideBusy = lTesting
lOverRideBusy = .f.
lTestImport = lTesting
lBrowfiles = lTesting
*lBrowfiles = .t.
_SCREEN.WINDOWSTATE=IIF(lTesting or lBrowfiles,2,1)

STORE "" TO cOldScreen
cCustname = "SAMSUNG"
cUseName = "SAMSUNG"
cPropername = PROPER(cUseName)
nAcctNum  =  6649 && Will change as the N1*DV is incorporated during testing
tfrom ="TGF EDI Operations <toll-edi-ops@tollgroup.com>"

lD2S = .F.
NormalExit = .F.
SET ESCAPE ON
ON ESCAPE CANCEL

STORE cPropername+" 940 Process" TO _SCREEN.CAPTION
CLEAR
WAIT WINDOW "Now setting up "+cPropername+" 940 process..." TIMEOUT 2


cDelimiter = "*"
cTranslateOption = "NOTILDE"
cfile = ""

dXdate1 = ""
dXxdate2 = DATE()
nFileCount = 0

nRun_num = 999
lnRunID = nRun_num

cOffice = "Y"
cMod = cOffice
gOffice = cMod
lLoadSQL = .t.
WAIT WINDOW "LoadSql is: "+IIF(lLoadSQL,"","NOT")+" in effect" TIMEOUT 2

DO M:\DEV\PRG\createx856a

TRY
	ASSERT .F. MESSAGE "AT START OF SAMSUNG 940 PROCESS"
	IF lTesting
		cUseFolder = "F:\whp\whdata\"
	ELSE
		xReturn = "XXX"
		DO M:\DEV\PRG\wf_alt WITH cOffice,nAcctNum
		cUseFolder = UPPER(xReturn)
	ENDIF

	cTransfer = "940-SAMSUNG"
	IF !lTesting
		SELECT 0
		USE F:\edirouting\FTPSETUP SHARED
		LOCATE FOR FTPSETUP.TRANSFER = cTransfer
		IF FTPSETUP.CHKBUSY = .T. AND !lOverrideBusy
			WAIT WINDOW "IN PROCESS...RETURNING" TIMEOUT 1
			NormalExit = .T.
			THROW
		ENDIF
		REPLACE chkbusy WITH .T.,trig_time WITH DATETIME() FOR FTPSETUP.TRANSFER = cTransfer
		USE IN FTPSETUP
	ENDIF

	SELECT 0
	USE F:\3pl\DATA\mailmaster SHARED ALIAS mm
	LOCATE FOR (mm.accountid = nAcctNum) AND (mm.office = cOffice) ;
		AND mm.edi_type = "940"
	IF FOUND()
		IF !lTesting
			_SCREEN.CAPTION = ALLTRIM(mm.scaption)
		ENDIF
		STORE TRIM(mm.acctname) TO cAccountName
		STORE TRIM(mm.basepath) TO lcPath
		STORE TRIM(mm.archpath) TO lcarchivepath
		lUseAlt = mm.use_alt
		tsendto = IIF(lUseAlt,sendtoalt,sendto)
		tcc = IIF(lUseAlt,ccalt,cc)
		LOCATE
		LOCATE FOR (mm.accountid = 9999) AND (mm.office = "Y")
		lUseAlt = mm.use_alt
		tsendtoerr = IIF(lUseAlt,sendtoalt,sendto)
		tccerr = IIF(lUseAlt,ccalt,cc)
		LOCATE
		LOCATE FOR mm.edi_type = "MISC" AND mm.taskname = "GENERAL"
		lUseAlt = mm.use_alt
		tsendtotest = IIF(lUseAlt,sendtoalt,sendto)
		tcctest = IIF(lUseAlt,ccalt,cc)
		IF lTesting
			tsendtoerr = tsendtotest
			tccerr = tcctest
		ENDIF
		USE IN mm
	ELSE
		USE IN mm
		THROW
	ENDIF

	IF lTesting
		lcPath = lcPath+"test\"
	ENDIF

	DO ("m:\dev\PRG\"+cCustname+"940_PROCESS")
	NormalExit = .T.
	WAIT WINDOW AT 10,10 "Normal Exit " TIMEOUT 2
	SELECT 0
	USE F:\edirouting\FTPSETUP SHARED
	REPLACE CHKBUSY WITH .F. FOR FTPSETUP.TRANSFER = cTransfer
	THROW

CATCH TO oErr
	IF NormalExit = .F.
		SET STEP ON
		tsubject = cPropername+"  940 Upload Error ("+TRANSFORM(oErr.ERRORNO)+") at "+TTOC(DATETIME())
		tattach  = ""
		tsendto  = tsendtoerr
		tcc = tccerr
		tmessage = cPropername+"  940 Upload Error..... Please fix me........!"
		lcSourceMachine = SYS(0)
		lcSourceProgram = SYS(16)

		tmessage =tmessage+ "Try/CATCH Exeception Message:"+CHR(13)+;
			[  Error: ] + STR(oErr.ERRORNO) +CHR(13)+;
			[  LineNo: ] + STR(oErr.LINENO) +CHR(13)+;
			[  Message: ] + oErr.MESSAGE +CHR(13)+;
			[  Procedure: ] + oErr.PROCEDURE +CHR(13)+;
			[  Details: ] + oErr.DETAILS +CHR(13)+;
			[  StackLevel: ] + STR(oErr.STACKLEVEL) +CHR(13)+;
			[  LineContents: ] + oErr.LINECONTENTS+CHR(13)+;
			[  UserValue: ] + oErr.USERVALUE+CHR(13)+;
			[  Computer:  ] +lcSourceMachine+CHR(13)+;
			[  940 file:  ] +xfile+CHR(13)+;
			[  Program:   ] +lcSourceProgram

		tattach  = ""
		DO FORM M:\DEV\frm\dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
	ENDIF
FINALLY
	CLOSE DATABASES ALL
	ON ERROR
ENDTRY

