*!* ALPHA_943_CREATE.PRG
CLOSE DATABASES ALL
CLEAR ALL
RELEASE ALL
TRY
	DO m:\dev\prg\lookups

	lTesting = .f. && If TRUE, disables certain functions
	lOverrideBusy = lTesting
*	lOverrideBusy = .T.
	DO m:\dev\prg\_setvars WITH lTesting
	IF lTesting
		WAIT WINDOW "This is a "+IIF(lTesting,"TEST","PRODUCTION")+" upload" TIMEOUT 2
	ENDIF

	PUBLIC lXfer943,cWhse,cFilename,cFilename2,tsendto,tcc,tsendtoerr,tccerr,lIsError,nOrigAcctNum,nDivLoops,cInFolder,cMod,m.office,lDoSQL
	PUBLIC lDoMail,tfrom,tattach,cxErrMsg,NormalExit,nAcctNum,Acctname,cOffice,lPick,lBrowFiles,cTransfer,cMasterComments,ninwologid,lcPath,lcArchivePath
	_SCREEN.CAPTION = "Alpha6 943 Upload Process"
	_SCREEN.WINDOWSTATE=IIF(lTesting OR lOverrideBusy,2,0)

	tfrom = "TGF EDI Operations <toll-edi-ops@fmiint.com>"
	tattach = " "
	nAcctNum = 5726

	lTestmail = lTesting
	lBrowFiles = lTesting
*	lBrowFiles = .F.
	lIsError = .F.
	lDoMail = .T.  && If .t., sends mail
	NormalExit = .F.
	lPick = .F.
	nDivLoops = 0
	cMasterComments = ""
	cxErrMsg = ""
	cCustName = "ALPHA6"  && Customer Identifier (for folders, etc.)
	cMailName = "Alpha6"  && Proper name for eMail, etc.

	cTransfer = "943-ALPHA6"
	SELECT 0
	USE F:\edirouting\ftpsetup
	LOCATE FOR ftpsetup.transfer = cTransfer
	IF !lTesting
		IF ftpsetup.chkbusy AND !lOverrideBusy
			WAIT WINDOW "Process is busy...exiting" TIMEOUT 2
			CLOSE DATA ALL
			NormalExit = .T.
			THROW
		ENDIF
		REPLACE chkbusy WITH .T.,trig_time WITH DATETIME()  FOR transfer = cTransfer
		USE IN ftpsetup
	ENDIF

	xsqlexec("select * from account where inactive=0","account",,"qq")
	INDEX ON accountid TAG accountid
	SET ORDER TO

	USE "f:\wo\wodata\wolog" IN 0 ALIAS wolog
	useca("inrcv","wh")

	cOffice="N"
	cMod = "I"
	gOffice = cMod
	gMasteroffice = cOffice
	cWhseMod = LOWER("wh"+cMod)
	
	setuprcvmail(cOffice)
	SET STEP ON 
	cArchiveFolder = IIF(lTesting,STRTRAN(lcArchivePath,"943in","943intest"),lcArchivePath)
	cInFolder = lcPath
	cInFolder = IIF(lTesting,STRTRAN(lcPath,"943in","943intest"),lcPath)
	DELETE FILE (cInFolder+'foxuser.*')
	CD [&cInFolder]

	len1 = ADIR(ary1,"*.txt")
	IF len1 = 0
		WAIT WINDOW "There are no 943 files to process...exiting" TIMEOUT 2
		CLOSE DATABASES ALL
		NormalExit = .T.
*		SET STEP ON
		THROW
	ENDIF
	WAIT WINDOW "There are "+TRANSFORM(len1)+" 943 files to process" TIMEOUT 2

	FOR kk = 1 TO len1
*	FOR kk = 1 TO 1
		cFilename = ALLTRIM(ary1[kk,1])
		cArchiveFile = (cArchiveFolder+cFilename)
		dFiledate = ary1[kk,3]
		cFilename2 = JUSTSTEM(cFilename)+"2.TXT"
		cString = FILETOSTR(cFilename)
		cString = STRTRAN(cString,"|","*")
		cString = STRTRAN(cString,"~",CHR(13))
		STRTOFILE(cString,cFilename2)

		SELECT 0
		DO m:\dev\prg\createx856a
		SELECT x856
		APPEND FROM &cFilename2 TYPE DELIMITED WITH CHARACTER "*"
*!*			IF !lTesting
*!*				LOCATE
*!*				LOCATE FOR x856.segment = "GS" AND x856.f1 = "PO"
*!*				IF !FOUND()
*!*					cxErrMsg = cFilename+" is NOT a 943 document"
*!*					errormail()
*!*				ENDIF
*!*			ENDIF

		LOCATE
		IF lTesting
*			BROWSE
		ENDIF

		xsqlexec("select * from account where accountid = 5726",,,"qq")
		SCATTER FIELDS Acctname MEMVAR
		USE IN account

		SELECT x856
		FOR uu = 1 TO 1

*!*				cHoldFolder = "F:\FTPUSERS\Alpha6\943in\Hold\"
*!*				cHoldFile = (cHoldFolder+cFilename)

			IF lTesting
				cUseFolder = "F:\WHP\WHDATA\"
			ELSE
				xReturn = "XXX"
*SET STEP ON
				DO m:\dev\prg\wf_alt WITH cMod,nAcctNum
				IF xReturn = "XXX"
					cxErrMsg = "NO WHSE FOUND IN WMS"
					errormail()
					RETURN
				ENDIF
				cUseFolder = UPPER(xReturn)
			ENDIF
			IF USED('inwolog')
				USE IN inwolog
			ENDIF
			IF USED('pl')
				USE IN pl
			ENDIF

			useca("inwolog","wh")
			xsqlexec("select * from inwolog where .f.","xinwolog",,"wh")

			useca("pl","wh")
			xsqlexec("select * from pl where .f.","xpl",,"wh")
			SELECT xpl
			SCATTER MEMVAR MEMO BLANK

			
			SELECT xinwolog
			SCATTER FIELDS EXCEPT Acctname MEMVAR BLANK
			STORE "FILE943*"+cFilename TO m.comments
			STORE m.comments+CHR(13)+"FILETIME*"+TTOC(DATETIME()) TO m.comments
			STORE 0 TO m.inwologid,m.wo_num,m.plid
			STORE 1 TO nPO
			STORE "" TO m.echo,m.po
			STORE "ALPHA943" TO m.addby
			STORE DATETIME() TO m.adddt
			STORE "ALPHA6_943" TO m.addproc
			STORE DATE() TO m.wo_date,m.date_rcvd


			SELECT x856
*!* Scan for PnP pack of 1 errors

			IF lTesting
*				SET STEP ON
			ENDIF
			nInstIncr = 0

			SCAN
				DO CASE
				CASE INLIST(x856.segment,"ISA","GS","ST","N1")
					LOOP
				CASE x856.segment = "W06"
					m.inwologid = m.inwologid + 1
					m.wo_num = m.wo_num + 1
					m.acct_ref = ALLTRIM(x856.f2)
					cShipID = ALLTRIM(x856.f4)
					m.comments = m.comments+CHR(13)+"SHIPID*"+cShipID
					STORE m.acct_ref TO cAcct_ref
					LOOP
				CASE x856.segment = "N9" AND x856.f1 = "ABS"
					m.reference = ALLTRIM(x856.f2)
					IF EMPTY(m.reference)
						LOOP
					ENDIF

				CASE x856.segment = "N9" AND x856.f1 = "ZZ"
*						SET STEP ON
					nInstIncr = nInstIncr+1
					cCommentStr = "INST"+ALLTRIM(STR(nInstIncr))+"*"+ALLTRIM(x856.f2)
*!*							IF EMPTY(cMasterComments)
*!*								cMasterComments = cCommentStr
*!*							ELSE
*!*								cMasterComments = cMasterComments+CHR(13)+cCommentStr
*!*							ENDIF
					cCommentStr = ""

				CASE x856.segment = "W27"
*!*						IF lTesting
						m.Container = UPPER(ALLTRIM(x856.f5))
*!*						ELSE
*!*							m.Container = UPPER(ALLTRIM(x856.f6)+ALLTRIM(x856.f7))
*!*						ENDIF

					IF !EMPTY(cMasterComments)
						cMasterComments = cMasterComments+CHR(13)
					ENDIF
					STORE m.Container TO cContainer
					m.office = cOffice
					m.mod = cMod
					m.accountid = nAcctNum
					m.addby = "TFGPROC"
					m.adddt = DATETIME()
					m.addproc = "ALPHA6-943"
*						DISPLAY MEMORY TO FILE "c:\tempfox\mem.xls"
					m.acctname = "ALPHA 6 DISTRIBUTIONS, LLC"
					INSERT INTO xinwolog FROM MEMVAR
					LOOP
					
				CASE x856.segment = "W04"
					nTotqty = INT(VAL(x856.f1))
					m.totqty = nTotqty
					m.plinqty = m.plinqty+nTotqty
					REPLACE xinwolog.plinqty WITH m.plinqty IN xinwolog
					cUPC = ALLTRIM(x856.f3)
					cStyle = UPPER(ALLTRIM(x856.f5))
					m.echo ="UPC*"+cUPC

				CASE x856.segment = "N9"
					cPack = ""
					DO CASE
					CASE x856.f1 = "CL"  && Pack qty
						cColor = ALLTRIM(x856.f2)
					CASE x856.f1 = "SZ"  && Pack qty
						cID = ALLTRIM(x856.f2)
						lPrepack = IIF(cID = "PP",.T.,.F.)
						m.echo =  m.echo+CHR(13)+"PACKTYPE*"+IIF(lPrepack,"PRE","PIK")
						cPack = ""
						IF lPrepack
							cPack = "1"
						ENDIF
						m.style = cStyle+"-"+cColor+"-"+cID
					ENDCASE

				CASE x856.segment = "W20"
					m.inwologid = xinwolog.inwologid
					m.pack = IIF(!lPrepack,ALLTRIM(x856.f1),cPack)
					nPack = INT(VAL(m.pack))
					m.po = ALLTRIM(STR(npo))
					m.plid = m.plid + 1
					m.units = .F.
					INSERT INTO xpl FROM MEMVAR
					m.pack = '1'
					m.totqty = nTotqty * nPack
					m.plid = m.plid + 1
					m.units = .T.
					INSERT INTO xpl FROM MEMVAR
					npo = npo+1
				ENDCASE
			ENDSCAN

			IF lTesting
				SELECT xinwolog
				LOCATE
				BROWSE
				SELECT xpl
				LOCATE
				BROWSE
				CANCEL
			ENDIF


			IF !lTesting
				SELECT xinwolog
				LOCATE
				SCAN
					xAcct_Num =  xinwolog.accountid
					xContainer = ALLTRIM(xinwolog.CONTAINER)
					xAcct_ref = ALLTRIM(xinwolog.acct_ref)
					WAIT WINDOW "AT XINWOLOG RECORD "+ALLTRIM(STR(RECNO())) NOWAIT NOCLEAR
					csq1 = [select * from inwolog where 	accountid = ]+ALLTRIM(STR(xAcct_Num))+[ and container = ']+xContainer+[' and acct_ref = ']+xAcct_ref+[']
					xsqlexec(csq1,"p1",,"wh")
					LOCATE
					IF !EOF()
						SET STEP ON
						cWO = TRANSFORM(inwolog.wo_num)
						dupemail(cWO)
						SELECT xinwolog
						ninwologid = xinwolog.inwologid
						DELETE NEXT 1
						SELECT xpl
						DELETE FOR xpl.inwologid = ninwologid
						SELECT xinwolog
						WAIT WINDOW "This set of values already exists in the INWOLOG table...looping" TIMEOUT 2
						LOOP
					ENDIF
				ENDSCAN
			ENDIF

			SELECT STYLE,PACK,1 AS CNT FROM xpl WHERE !units INTO CURSOR xpl1
			SELECT STYLE,PACK,SUM(CNT) AS cnt1 FROM xpl1 GROUP BY 1,2 INTO CURSOR xpl2
			LOCATE

			SCAN FOR cnt1 > 1
				SCATTER MEMVAR
				m.style = UPPER(m.style)
				SELECT xpl
				LOCATE FOR xpl.STYLE = m.style AND xpl.PACK = m.pack AND !units
				IF EOF()
					EXIT
				ENDIF
				nRec = RECNO()
				nCtns = 0
				nUnits = 0
				SCAN FOR xpl.STYLE = m.style AND xpl.PACK = m.pack AND !units AND RECNO() > nRec AND !DELETED()
					nCtns = nCtns + xpl.totqty
					SKIP 1 IN xpl
					nUnits = nUnits + xpl.totqty
					SKIP -1 IN xpl
					DELETE NEXT 2 IN xpl
				ENDSCAN
				SELECT xpl
				GO nRec
				REPLACE xpl.totqty WITH xpl.totqty+nCtns IN xpl
				SKIP 1 IN xpl
				REPLACE xpl.totqty WITH xpl.totqty+nUnits IN xpl
			ENDSCAN

			IF lTesting
				WAIT WINDOW "Checking XPL deleted recs" TIMEOUT 3
				SELECT xpl
				LOCATE
				SET DELETED OFF
*				Browse
				SET DELETED ON

				SELECT xinwolog
				LOCATE
				BROWSE
				SELECT xpl
				LOCATE
				BROWSE
				CANCEL
			ENDIF


			nUploadCount = 0

			SELECT xinwolog
			LOCATE
			WAIT WINDOW "Currently in "+UPPER(IIF(lTesting,"test","production"))+" mode, base folder: "+cUseFolder TIMEOUT 2
*			SET STEP ON
			SCAN  FOR !DELETED() && Scanning xinwolog here
				SCATTER MEMVAR MEMO
				cPLCtns = ALLTRIM(STR(m.plinqty))
				cPLUnits = ALLTRIM(STR(m.plunitsinqty))
				nWO_num = dygenpk("wonum",cWhseMod)
				m.wo_num = nWO_num
				cWO_Num = ALLTRIM(STR(nWO_num))

				WAIT WINDOW "WO# is "+cWO_Num TIMEOUT 2

				SELECT inwolog

				m.mod = cMod
				m.office = cOffice
				insertinto("inwolog","wh",.T.)
				nUploadCount = nUploadCount+1

				SELECT xpl
				SCAN FOR inwologid=xinwolog.inwologid
					SCATTER FIELDS EXCEPT inwologid MEMVAR MEMO
					m.wo_num = nWO_num
					m.office = cOffice
					m.mod = cMod
					insertinto("pl","wh",.T.)
				ENDSCAN
				goodmail()
			ENDSCAN

			IF nUploadCount = 0
				SET STEP ON
				IF FILE(cFilename)
					COPY FILE [&cFilename] TO [&cArchivefile]
					IF FILE(cArchiveFile) AND !lTesting
						DELETE FILE [&cFilename]
					ENDIF
				ENDIF
				EXIT
			ENDIF

			STORE "" TO xtruckwonum, xtruckseal
			DO "m:\dev\prg\whtruckwonum.prg" WITH inwolog.wo_num, inwolog.REFERENCE, inwolog.CONTAINER, inwolog.accountid
			REPLACE truckwonum WITH xtruckwonum, seal WITH xtruckseal IN inwolog
			RELEASE xtruckwonum, xtruckseal

			cUploadCount = ALLTRIM(STR(nUploadCount))
			WAIT WINDOW "ALPHA6 943 file "+cFilename+" processed."+CHR(13)+"Header records loaded: "+cUploadCount TIMEOUT 1
			IF FILE(cFilename)
				COPY FILE [&cFilename] TO [&cArchivefile]
				IF FILE(cArchiveFile) AND !lTesting
					DELETE FILE [&cFilename]
				ENDIF
			ENDIF

			SELECT pl
			COUNT TO xnumskus FOR !units AND wo_num=inwolog.wo_num

			xsqlexec("select * from inrcv where container='"+inwolog.CONTAINER+"' and status#'CONFIRMED'","xinrcv",,"wh")
			SELECT xinrcv
			IF RECCOUNT() > 0
				xsqlexec("update inrcv set inwonum="+TRANSFORM(inwolog.wo_num)+" where container='"+inwolog.CONTAINER+"' and status#'CONFIRMED'",,,"wh")
			ELSE
				SELECT inrcv
				SCATTER MEMVAR MEMO BLANK
				m.mod = cMod
				m.office = cOffice
				m.inrcvid=dygenpk("inrcv","whall")  && new get the ID value
				m.inwonum=inwolog.wo_num
				m.accountid=inwolog.accountid
				m.Acctname=inwolog.Acctname
				m.Container=inwolog.CONTAINER
				m.status="NOT AVAILABLE"
				m.skus=xnumskus
				m.cartons=inwolog.plinqty
				m.newdt=DATE()
				m.addfrom="IN"
				m.confirmdt=empty2nul(m.confirmdt)
				m.newdt=empty2nul(m.newdt)
				m.dydt=empty2nul(m.dydt)
				m.firstavaildt=empty2nul(m.firstavaildt)
				m.apptdt=empty2nul(m.apptdt)
				m.pickedup=empty2nul(m.pickedup)

				INSERT INTO inrcv FROM MEMVAR
				tu("inrcv")
			ENDIF

			tu("inwolog")
			tu("pl")

			IF lTesting
				SELECT inwolog
				GO BOTTOM
				BROWSE TIMEOUT 5
				SELECT pl
				GO BOTTOM
				BROWSE TIMEOUT 5
			ENDIF
		ENDFOR

	ENDFOR
	WAIT WINDOW "All Alpha6 943s processed...exiting" TIMEOUT 2

	closeftpsetup()

	NormalExit = .T.

CATCH TO oErr
	IF NormalExit = .F.
		ASSERT .F. MESSAGE "AT CATCH SECTION"
		tsubject = cCustName+" 943 Upload Error ("+TRANSFORM(oErr.ERRORNO)+") at "+TTOC(DATETIME())
		tattach  = ""
		tsendto  = tsendtoerr
		tcc      = tccerr
		tmessage = cCustName+" 943 Upload Error"
		lcSourceMachine = SYS(0)
		lcSourceProgram = SYS(16)

		IF EMPTY(cxErrMsg)
			tmessage =tmessage+ "Try/CATCH Exeception Message:"+CHR(13)+;
				[  Error: ] + STR(oErr.ERRORNO) +CHR(13)+;
				[  Procedure: ] + oErr.PROCEDURE +CHR(13)+;
				[  LineNo: ] + STR(oErr.LINENO) +CHR(13)+;
				[  Message: ] + oErr.MESSAGE +CHR(13)+;
				[  Details: ] + oErr.DETAILS +CHR(13)+;
				[  StackLevel: ] + STR(oErr.STACKLEVEL) +CHR(13)+;
				[  LineContents: ] + oErr.LINECONTENTS+CHR(13)+;
				[  UserValue: ] + oErr.USERVALUE+CHR(13)+;
				[  Computer:  ] +lcSourceMachine+CHR(13)+;
				[  940 file:  ] +cFilename+CHR(13)+;
				[  Program:   ] +lcSourceProgram
		ELSE
			tmessage = tmessage + CHR(13) + CHR(13) + cxErrMsg
		ENDIF
		tattach  = ""
		tfrom    ="TGF EDI Processing Center <transload-ops@fmiint.com>"
		DO FORM m:\dev\FRM\dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
	ENDIF
FINALLY
	DELETE FILE (cInFolder+'*.txt')
	DELETE FILE (cInFolder+'foxuser.*')
	SET LIBRARY TO
	CLOSE DATABASES ALL
	ON ERROR
ENDTRY


****************************
PROCEDURE goodmail
****************************
tsubject = cMailName+" EDI FILE (943) Processed, Inv. WO "+cWO_Num
tmessage = REPLICATE("-",90)+CHR(13)
tmessage = tmessage+"Packinglist upload run from machine: "+SYS(0)+CHR(13)
tmessage = tmessage+REPLICATE("-",90)+CHR(13)
tmessage = tmessage+"Inbound Workorders created for NJ Warehouse N"+CHR(13)
tmessage = tmessage+"Data from File : "+cFilename+CHR(13)
tmessage = tmessage+REPLICATE("-",90)+CHR(13)
tmessage = tmessage+"Acct. Ref: "+cAcct_ref+SPACE(2)+"Container: "+cContainer+SPACE(2)+"WO# "+cWO_Num+SPACE(2)+"CTNS: "+cPLCtns+SPACE(2)+"UNITS: "+cPLUnits

IF lDoMail
	DO FORM m:\dev\frm\dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
ENDIF
ENDPROC

********************************
PROCEDURE divpackmail
********************************
tsubject = cMailName+" EDI FILE (943): PnP Calc. Ctn. Qty. of 0"
tmessage = REPLICATE("-",90)+CHR(13)
tmessage = tmessage+"Packinglist upload run from machine: "+SYS(0)+CHR(13)
tmessage = tmessage+REPLICATE("-",90)+CHR(13)
tmessage = tmessage+"Data from File : "+cFilename+CHR(13)
tmessage = tmessage+"Contains PnP items with a calculated carton quantity [Units/Pack] of ZERO."
tmessage = tmessage+CHR(13)+"Please correct and re-send."+CHR(13)
tmessage = tmessage+"Reference#: "+TRIM(m.acct_ref)+", Container: "+cContainer+CHR(13)
tmessage = tmessage+"Style: "+TRIM(m.style)
tcc = IIF(EMPTY(ALLTRIM(tcc)),'lupe@moret.com',ALLTRIM(tcc)+',lupe@moret.com')

IF lDoMail
	DO FORM m:\dev\frm\dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
ENDIF
ENDPROC

****************************
PROCEDURE packmail
****************************
tsubject = cMailName+" EDI FILE (943): PnP Pack of 1"
tmessage = REPLICATE("-",90)+CHR(13)
tmessage = tmessage+"Packinglist upload run from machine: "+SYS(0)+CHR(13)
tmessage = tmessage+REPLICATE("-",90)+CHR(13)
tmessage = tmessage+"Data from File : "+cFilename+CHR(13)
tmessage = tmessage+"Contains PnP items with a pack qty (N9*PY) of 1. This is only allowed for prepacks. Please correct and re-send."+CHR(13)
tmessage = tmessage+"Reference#: "+TRIM(cAcct_ref)+", Container: "+cContainer+CHR(13)+CHR(13)
IF !EMPTY(cStyleList)
	tmessage = tmessage+"Affected styles:"+CHR(13)+cStyleList
ENDIF
IF !lTesting
	tcc = IIF(EMPTY(ALLTRIM(tcc)),'lupe@moret.com',ALLTRIM(tcc)+',lupe@moret.com')
ENDIF

IF lDoMail
	DO FORM m:\dev\frm\dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
ENDIF
ENDPROC

****************************
PROCEDURE dupemail
****************************
PARAMETERS cWO
tsubject = cMailName+" EDI FILE (943): Duplicate"
tmessage = REPLICATE("-",90)+CHR(13)
tmessage = tmessage+"Data from File : "+cFilename+CHR(13)
tmessage = tmessage+"For Account ID: "+TRANSFORM(nAcctNum)
tmessage = tmessage+ ", Container "+cContainer
tmessage = tmessage+ ", Acct. Reference "+cAcct_ref+CHR(13)
tmessage = tmessage+"already exists in the Inbound WO table, in Work Order "+cWO
tmessage = tmessage+CHR(13)+REPLICATE("-",90)+CHR(13)

IF lTesting
	SET STEP ON
ENDIF

IF lDoMail
	DO FORM m:\dev\frm\dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
ENDIF
ENDPROC

****************************
PROCEDURE errormail
****************************
setuprcvmail(cOffice)
tsubject = cMailName+" EDI ERROR (943)"
tsendto = tsendtoerr
tcc = tccerr
IF lMoretSend AND !lTesting
	tcc      = tcc+",FMIConfirmation@moret.com"  && Amended 2/27/08, Joe
ENDIF
tmessage = "File: "+cFilename
tmessage = tmessage+CHR(13)+cxErrMsg

DO FORM m:\dev\frm\dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
NormalExit = .T.
SET STEP ON
THROW
ENDPROC

****************************
PROCEDURE setuprcvmail
****************************
PARAMETERS cOffice
*	ASSERT .f. MESSAGE "In Mail/Folder selection...debug"
SELECT 0
USE F:\3pl\DATA\mailmaster ALIAS mm
LOCATE FOR mm.edi_type = "943" AND mm.GROUP = "ALPHA6" AND mm.office = cOffice
IF !FOUND()
	cxErrMsg = "Office/Loc/Acct not in Mailmaster!"
	errormail()
ENDIF
lcPath = ALLTRIM(mm.basepath)
lcArchivePath = ALLTRIM(mm.archpath)

lUseAlt = mm.use_alt
STORE IIF(lUseAlt,TRIM(mm.sendtoalt),TRIM(mm.sendto)) TO tsendto
STORE IIF(lUseAlt,TRIM(mm.ccalt),TRIM(mm.cc)) TO tcc
LOCATE
LOCATE FOR mm.edi_type = "MISC" AND mm.taskname = "JOETEST"
lUseAlt = mm.use_alt
STORE IIF(lUseAlt,TRIM(mm.sendtoalt),TRIM(mm.sendto)) TO tsendtotest
STORE IIF(lUseAlt,TRIM(mm.ccalt),TRIM(mm.cc)) TO tcctest
LOCATE
LOCATE FOR mm.edi_type = "MISC" AND mm.taskname = "GENERAL"
lUseAlt = mm.use_alt
STORE IIF(lUseAlt,TRIM(mm.sendtoalt),TRIM(mm.sendto)) TO tsendtoerr
STORE IIF(lUseAlt,TRIM(mm.ccalt),TRIM(mm.cc)) TO tccerr
IF lTesting OR lTestmail
	tsendto = tsendtotest
	tcc = tcctest
ENDIF
USE IN mm
ENDPROC

*********************************
PROCEDURE closeftpsetup
*********************************
SELECT 0
USE F:\edirouting\ftpsetup
REPLACE chkbusy WITH .F. FOR transfer = cTransfer
USE IN ftpsetup
ENDPROC
