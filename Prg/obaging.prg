parameters xaccountid, xoffice

xfile="h:\fox\obaging.xls"

if file("&xfile")
	xerror=.f.
	try
		delete file &xfile
	catch
		wait "Error deleting file... Cannot continue." window timeout 3
		xerror=.t.
	endtry
	if xerror
		return .f.
	endif
endif

wait window "Collecting data..." nowait

select *, space(30) as carriername,{} as closedt ;
	from manifest where accountid=xaccountid and wo_date>=date()-60 and delivered={} and office=xoffice ;
	group by wo_num into cursor xbackup readwrite

xacctname=alltrim(acctname)

scan
	replace closedt with ttod(ctot(getmemodata("suppdata","COMPLETEDT",,.t.)))
	if !empty(carrier) and seek(xbackup.carrier,"carrier","scac")
		replace carriername with carrier.carrier
	endif
endscan

**remove anything that is not yet closed or was closed today
**changed to remove anything that is not closed
*delete for closedt={} or closedt>=date()
delete for closedt={}

**for TGF deliveries, remove if dispatched
scan for carrier='TGFJ'
	xsqlexec("select * from fxtrips where wo_num="+transform(xbackup.wo_num)+" and dispatched is not null","xfxtrips",,"fx")
	if reccount("xfxtrips")=0
		select daily
		locate for wo_num=xbackup.wo_num and dispatched#{}
		if !found()
			select tdaily
			locate for wo_num=xbackup.wo_num and dispatched#{}
		endif
	endif

	if !eof()
		delete in xbackup
	endif
endscan

select wo_num,wo_date,acctname,office,trailer,carriername,carrier,delloc,closedt from xbackup into cursor xbackup readwrite

xnodata=iif(reccount('xbackup')=0,.t.,.f.)

if !xnodata
	wait window "Creating spreadsheet..." nowait

	oexcel=createobject("excel.application")
	oexcel.sheetsinnewworkbook=2
	oworkbook=oexcel.workbooks.add()
	oworkbook.worksheets[1].name="Summary"
	oworkbook.worksheets[2].name="Detail"

	osheet1=oworkbook.worksheets[1]
	xsheet="osheet1"

	**get # of avail date columns required (how many different dates exist)
	select * from xbackup group by wo_date into cursor xdates readwrite
	xcolcnt=reccount()+2 &&add 2 for carrier & totals columns
	xlastcol=chr(64+xcolcnt) &&need alpha column, start at A (65) and add total # of columns

	osheet1.columns[1].columnwidth=30
	for i = 2 to xcolcnt
		osheet1.columns[i].columnwidth=7
	endfor

	**exceltext params: xrange, xtext, xformat, xsize, xnumberformat, xblankifzero, xbackcolor
	=exceltext("A1","Outbound Transload Aging Report","bold",12)
	=exceltext("A2","Account: "+xacctname,"bold")
	=exceltext("A3","Office: "+iif(xoffice='C','CA',iif(xoffice='M','FL','NJ')),"bold")
	=exceltext("A4","Run Date: "+transform(date()),"bold")

	=exceltext("C5","Manifest Date","bold center",,,,"RGB(255,255,128)")
	osheet1.range("C5:"+chr(64+(xcolcnt-1))+"5").merge

	xhdrrow=6

	=exceltext("A"+transform(xhdrrow),"Carrier","bold center",,,,"RGB(255,255,128)")

	select xdates
	xcol=66 &&start at column B
	xcolprefix=0
	scan
		=exceltext(iif(xcolprefix=0,"",chr(xcolprefix))+chr(xcol)+transform(xhdrrow),left(dtoc(wo_date),5),"bold center",,,,"RGB(255,255,128)")
		xcol=xcol+1
		if xcol>90 &&after Z, need to reset and add prefeix (or increment prefix)
			xcolprefix=iif(xcolprefix=0,65,xcolprefix+1)
			xcol=65
		endif
	endscan

	=exceltext(iif(xcolprefix=0,"",chr(xcolprefix))+chr(xcol)+transform(xhdrrow),"Total","bold center",,,,"RGB(255,255,128)")

	xrow=xhdrrow+1
	select * from xbackup group by carrier into cursor xcarriers

	select xcarriers
	scan
		osheet1.range("A"+transform(xrow)).value = alltrim(carriername)

		select xdates
		xcol=66 &&start at column B
		xcolprefix=0
		scan
			select xbackup
			count to xtrlcnt for wo_date=xdates.wo_date and carrier=xcarriers.carrier
			osheet1.range(iif(xcolprefix=0,"",chr(xcolprefix))+chr(xcol)+transform(xrow)).value = xtrlcnt
			xcol=xcol+1
			if xcol>90 &&after Z, need to reset and add prefeix (or increment prefix)
				xcolprefix=iif(xcolprefix=0,65,xcolprefix+1)
				xcol=65
			endif
		endscan

		select xbackup
		count to xtrlcnt for carrier=xcarriers.carrier
		osheet1.range(iif(xcolprefix=0,"",chr(xcolprefix))+chr(xcol)+transform(xrow)).value = xtrlcnt

		xrow=xrow+1
	endscan

	use in xcarriers

	=exceltext("A"+transform(xrow),"Date Totals","bold")

	select xdates
	xcol=66 &&start at column B
	xcolprefix=0
	scan
		select xbackup
		count to xtrlcnt for wo_date=xdates.wo_date
		osheet1.range(iif(xcolprefix=0,"",chr(xcolprefix))+chr(xcol)+transform(xrow)).value = xtrlcnt
		xcol=xcol+1
		if xcol>90 &&after Z, need to reset and add prefeix (or increment prefix)
			xcolprefix=iif(xcolprefix=0,65,xcolprefix+1)
			xcol=65
		endif
	endscan

	osheet1=oworkbook.worksheets[2]
	xsheet="osheet1"

	=exceltext("A1","Mfst No","bold center",,,,"RGB(255,255,128)")
	=exceltext("B1","Mfst Dt","bold center",,,,"RGB(255,255,128)")
	=exceltext("C1","Account","bold center",,,,"RGB(255,255,128)")
	=exceltext("D1","Office","bold center",,,,"RGB(255,255,128)")
	=exceltext("E1","Trailer","bold center",,,,"RGB(255,255,128)")
	=exceltext("F1","Carrier","bold",,,,"RGB(255,255,128)")
	=exceltext("G1","Scac","bold",,,,"RGB(255,255,128)")
	=exceltext("H1","Dest","bold",,,,"RGB(255,255,128)")
	=exceltext("I1","Closed","bold",,,,"RGB(255,255,128)")

	select xbackup
	xrow=2
	scan
		osheet1.range("A"+transform(xrow)).value = wo_num
		osheet1.range("B"+transform(xrow)).value = transform(wo_date)
		osheet1.range("C"+transform(xrow)).value = alltrim(acctname)
		osheet1.range("D"+transform(xrow)).value = alltrim(office)
		osheet1.range("E"+transform(xrow)).value = alltrim(trailer)
		osheet1.range("F"+transform(xrow)).value = alltrim(carriername)
		osheet1.range("G"+transform(xrow)).value = alltrim(carrier)
		osheet1.range("H"+transform(xrow)).value = alltrim(delloc)
		osheet1.range("I"+transform(xrow)).value = transform(closedt)
		xrow=xrow+1
	endscan

	oworkbook.saveas(xfile,39)

	oExcel.quit &&release the file before attempting to send
	release oexcel

*	run "&xfile"
endif

wait clear

use in xbackup

return !xnodata
