* this is normally called from trk_ucc_import.prg - I don't know if it was also meant to be run standalone. MB 1/12/2018
* this was written by Todd Margolin.
* build exe in F:\3PL\

set exclusive off
set deleted on
set talk off
set safety off

guserid = "TRKUCC"
gprocess = "TRKUCC"
goffice = 'L'
gmasteroffice = 'L'


*set step on

xdyfixed=.t.
xsqlexec("select ship_ref, serialno from ctnucc where updatedt>{"+dtoc(date()-10)+"}","xctnucc",,"wh")
* REMOVED updatedt - it was crashing and not needed here
*select ship_ref , max(serialno) as bol_no from ctnucc where updatedt >date()-10  group by ship_ref into cursor c1 readwrite
select ship_ref , max(serialno) as bol_no from xctnucc group by ship_ref into cursor c1 readwrite

useca('outship','wh',,,'select * from outship where inlist(accountid,6718,6719,6722,6744,6745,6746,6747,6748,6749) and del_date is null')

select outship
replace scac with 'UTDC' for inlist(accountid,6718,6719,6722,6744,6745,6746,6747,6748,6749) and empty(scac) and 'CODAMOUNT' $(shipins) and 'TRADE DIRECT' $(ship_via)
replace scac with 'USTD' for inlist(accountid,6718,6719,6722,6744,6745,6746,6747,6748,6749) and empty(scac) and 'TRADE DIRECT' $(ship_via)
select * from outship where inlist(accountid,6718,6719,6722,6744,6745,6746,6747,6748,6749) and (empty(del_date) or isnull(del_date)) into cursor voutship readwrite
select * from c1 a left join voutship b on a.ship_ref=b.ship_ref into cursor c22 readwrite
select ship_ref_a as ship_ref, bol_no_a as bol_no from c22 where !isnull(wo_num) into cursor c3a readwrite


select c3a
scan
	select outship
	locate for ship_ref=c3a.ship_ref
	if found("outship")   and isnull(del_date)
		replace bol_no with c3a.bol_no, del_date with date(),dylog with .t. , updatedt with date(), updateby with 'TDEX'
	else
	endif
endscan
select outship
tu()

select c3a
scan
	* added missing } after loaddt=  which was causing INFO table errors 1/12/2018 MB
	*xsqlexec("update ctnucc set loaddt={"+dtoc(date())+" where office='L' and ship_ref='"+c3a.ship_ref+"'",,,"wh")
	xsqlexec("update ctnucc set loaddt={"+dtoc(date())+"} where office='L' and ship_ref='"+c3a.ship_ref+"'",,,"wh")
endscan

xsqlexec("update ctnucc set asnfile=ship_ref where office='L' and ucc='LTL'",,,"wh")
xsqlexec("select asnfile as bol_no from ctnucc where office='L' and ucc='LTL'","t1a",,"wh")

select distinct ship_ref from outship where bol_no in (select bol_no from t1a) into cursor t1 readwrite
select t1
xfilter="inlist(ship_ref"
scan
	xfilter=xfilter+',"'+t1.ship_ref+'"'
endscan
xfilter=xfilter+")"

useca('cartons','pickpack_sql5',,,'select * from cartons where accountid in(6718,6744)')

*SET STEP On
xsqlexec("select * from ctnucc where office='L' and ucc='LTL'","vctnucc",,"wh")

select a.ship_ref as bol_no ,b.ship_ref as cship_ref,a.subpro from vctnucc a left join outship b on a.ship_ref=b.bol_no into cursor t1 readwrite
select a.*, ucc from t1 a left join cartons b on a.cship_ref=b.ship_ref into cursor t2a readwrite
select distinct bol_no,cship_ref,subpro, ucc from t2a into cursor t2 readwrite

if used("ctnucc")
	use in ctnucc
endif

useca("ctnucc","wh")

select t2
* getting NULL ship_ref and UCC errors in INSERTINTO() 2/7/2018
*scan
SCAN FOR (NOT ISNULL(cship_ref)) AND (NOT ISNULL(ucc))
*DHW*	if xsqlexec("select ctnuccid from ctnucc where office='L' and ship_ref='"+t2.bol_no+"'","xdy",,"wh") = 0
	if xsqlexec("select * from ctnucc where office='L' and ship_ref='"+t2.bol_no+"'","xdy",,"wh") != 0
		scatter memvar fields except ctnuccid
		gmasteroffice ="L"
		goffice ="L"
		loaddt=date()
		rcvdt=date()
		pickdt=date()
		m.ship_ref=t2.cship_ref
		m.ucc=t2.ucc
		insertinto("ctnucc","wh",.t.)
	endif
endscan

tu("ctnucc")

*

xsqlexec("delete from ctnucc where office='L' and ucc='LTL'",,,"wh")

*replace bol_no with subpro in outship and populate del date

select t1
scan
	select outship
	locate for bol_no=t1.bol_no
	if found("outship")
		replace bol_no with t1.subpro   in outship
		replace del_date with date()  in outship
	else
	endif
endscan

tu('outship')

select c3a
if reccount() > 0
*		export TO "S:\MarcJacobsData\Reports\Cut_report\cut_report"   TYPE xls
	tsendto = "doug.wachs@tollgroup.com,ariana.andrade@tollgroup.com,joni.golding@tollgroup.com"
	tattach = ""
	tcc =""
	tfrom ="Toll EDI System Operations <fmi-transload-ops@fmiint.com>"
	tmessage = "Trade Direct processing is COMPLETE_:"+ttoc(datetime())
	tsubject = "Trade Direct processing is COMPLETE"
	do form m:\dev\frm\dartmail2 with tsendto,tfrom,tsubject, tcc,tattach,tmessage  ,"A"

else


endif