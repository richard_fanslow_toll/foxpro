CLOSE DATABASES ALL
*!* Set and initialize public variables
PUBLIC xFile,archivefile,cXdate1,dXdate2,cOffice,lTesting,cUseDir,cCustName,nTotPT,cMailName
PUBLIC cDelimiter,cTranslateOption,EmailCommentStr,LogCommentStr,nAcctNum,cAcctNum,lLoop,lDoPkg
PUBLIC chgdate,ptid,ptctr,nPTQty,lTesting,lTestUploaddet,cOfficename,cConsignee,cStoreNum,cTotPT
PUBLIC cPickticket_num_start,cPickticket_num_end,EmailCommentStr,cLoadID,lcPath,cUseFolder,cShip_ref
PUBLIC tsendto,tcc,lcArchivepath,nXPTQty,NormalExit,cMessage,fa997file,m.printstuff,lTestImport,cTransfer

PUBLIC ARRAY a856(1)
SET EXCLUSIVE OFF
CaptionStr = "SEARS EDI RE-FORMATTER"
STORE CaptionStr TO _SCREEN.CAPTION
Set Step On 
lTesting = .T.
lTestInput = .T.
gsystemmodule="SEARSEDI"

if set("default")#"M"
	on error do fmerror with error(), program(), lineno(1), sys(16),, .t.
endif

_SCREEN.WINDOWSTATE=IIF(lTesting,2,1)
DO m:\dev\prg\_setvars
SET STATUS BAR ON

STORE "" TO EmailCommentStr
NormalExit = .F.
cMessage = " "

TRY
	m.adddt = DATETIME()
	m.addby = "FMI-PROC"
	cDelimiter = "*"
	cTranslateOption ="CR_TILDE"

	xFile = ""
	cXdate1 = ""
	dXdate2 = DATE()
	LogCommentStr = ""

      Do SearsEDIProcess
	CLOSE DATABASES ALL

CATCH TO oErr
	IF NormalExit = .F.
		ASSERT .F. MESSAGE "In Error Catch"
		tsubject = cMailName+" 940 Upload Error ("+TRANSFORM(oErr.ERRORNO)+") at "+TTOC(DATETIME())
		tattach  = ""
		tsendto  = "pgaidis@fmiint.com"
		tmessage = cMailName+" SEARS EDI Upload Error..... Please fix"
		tmessage = tmessage+CHR(13)+cMessage+CHR(13)+CHR(13)
		lcSourceMachine = SYS(0)
		lcSourceProgram = SYS(16)

		tmessage =tmessage+ "Try/CATCH Exeception Message:"+CHR(13)+;
			[  Error: ] + STR(oErr.ERRORNO) +CHR(13)+;
			[  LineNo: ] + STR(oErr.LINENO) +CHR(13)+;
			[  Message: ] + oErr.MESSAGE +CHR(13)+;
			[  Procedure: ] + oErr.PROCEDURE +CHR(13)+;
			[  Details: ] + oErr.DETAILS +CHR(13)+;
			[  StackLevel: ] + STR(oErr.STACKLEVEL) +CHR(13)+;
			[  LineContents: ] + oErr.LINECONTENTS+CHR(13)+;
			[  UserValue: ] + oErr.USERVALUE+CHR(13)+;
			[  Computer:  ] +lcSourceMachine+CHR(13)+;
			[  940 file:  ] +xFile+CHR(13)+;
			[  Program:   ] +lcSourceProgram

		tattach  = ""
		tcc=" "
		tfrom    ="TGF EDI Processing Center <transload-ops@fmiint.com>"
		DO FORM m:\dev\FRM\dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
	ELSE
		WAIT WINDOW AT 10,10 "Normal Exit " TIMEOUT 2
	ENDIF
FINALLY
	CLOSE DATABASES ALL
ENDTRY

