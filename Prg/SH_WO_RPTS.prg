* shop wo reports for Adam/Brian
LPARAMETERS tcVehicleType, tdFromDate, tdToDate

IF NOT USED('ordhdr')
	USE SH!ordhdr IN 0
ENDIF

IF NOT USED('repdetl')
	USE SH!repdetl IN 0 ORDER repdate
ENDIF

IF NOT USED('tiredetl')
	USE SH!tiredetl IN 0 ORDER tiredate
ENDIF

IF NOT USED('oildetl')
	USE SH!oildetl IN 0 ORDER oildate
ENDIF

IF NOT USED('vehmast')
	USE SH!vehmast IN 0
ENDIF

IF NOT USED('trucks')
	USE SH!trucks IN 0
ENDIF

IF NOT USED('trailer')
	USE SH!trailer IN 0
ENDIF

IF NOT USED('chaslist')
	USE SH!chaslist IN 0
ENDIF
