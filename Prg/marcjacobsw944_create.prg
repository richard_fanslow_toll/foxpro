*!* MARC JACOBS (W) 944_CREATE.PRG

parameters nwo_num,lcedi_type,nacctnum,coffice

public lxfer944,cwhse,cfilename,cfilename2,tsendto,tcc,tsendtotest,tcctest,tsendtoerr,tccerr,liserror,ljoemail
public lemail,tfrom,cacct_ref,normalexit,cerrmsg,tsendtoecho,tccecho,tattach,nfilenum

tfrom = "Toll EDI Operations <toll-edi-ops@tollgroup.com>"

useca("ackdata","wh")
SET STEP ON 
try
	ltesting = .f. && If TRUE, disables certain functions
	ltestinput = .f. &&lTesting
	do m:\dev\prg\_setvars with ltesting
	cintusage = iif(ltesting,"T","P")
	if ltesting
		close data all
		use f:\3pl\data\edi_trigger in 0
	endif

	ltestmail = ltesting
	normalexit = .f.
*	nAcctNum = 6303
	if INLIST(coffice,"I","J","N")
		coffice = "J"
	ENDIF
	
	if coffice='J'
		office=coffice
		goffice="J"
		gmasteroffice="N"
	endif
	if coffice='L'
		office=coffice
		goffice="L"
		gmasteroffice="L"
	endif

	liserror = .f.
	cacct_ref = ""
	cerrmsg = ""
	tattach = ""

	if type("nWO_Num") = "L"
		nwo_num = 3622096 && to 70
		nacctnum = 6303
		coffice = "J"
	endif

	if !ltesting
		if type("nWO_Num") <> "N"  or empty(nwo_num) && If WO is not numeric or equals Zero
			cerrmsg = "NO WO_NUM"
			wait window "No WO# Provided...closing" timeout 3
			do ediupdate with .t.,cerrmsg
			throw
		endif
	endif

	coffice=iif(inlist(coffice,"1","2","5"),"C",coffice)
	ccontainer = ""

	if ltestinput
		cusefolder = "F:\WHP\WHDATA\"
	else
		xreturn = "XXX"

		do m:\dev\prg\wf_alt with coffice,nacctnum
		if xreturn = "XXX"
			cerrmsg = "NO BLDG #"
			wait window "No Bldg. # Provided...closing" timeout 3
			do ediupdate with .t.,cerrmsg
			throw
		endif
		cusefolder = upper(xreturn)
	endif

	select 0
	use f:\3pl\data\mailmaster alias mm shared
**  Locate For mm.accountid = 6303
	locate for inlist(mm.accountid,6303,6543) ;
		and mm.mod = coffice ;
		and mm.edi_type = "944"
	if !found()
		cerrmsg = "ACCT/LOC !FOUND"
		wait window "Office/Loc/Acct not found!" timeout 2
		do ediupdate with .t.,cerrmsg
		throw
	endif
	store trim(mm.acctname) to ccustname
	store trim(mm.basepath) to lcoutpath
	store trim(mm.holdpath) to lcholdpath
	store trim(mm.archpath) to lcarchivepath
	lusealt = mm.use_alt
	tsendto = iif(lusealt,mm.sendtoalt,mm.sendto)
	tcc = iif(lusealt,mm.ccalt,mm.cc)
	tsendtoerr = iif(lusealt,mm.sendtoalt,mm.sendto)
	tccerr = iif(lusealt,mm.ccalt,mm.cc)
	locate
	locate for mm.edi_type = "MISC" and mm.taskname = "TODDMAIL"
	lusealt = mm.use_alt
	tsendtotest = iif(lusealt,mm.sendtoalt,mm.sendto)
	tcctest = iif(lusealt,mm.ccalt,mm.cc)
	locate for mm.edi_type = "MISC" and mm.taskname = "MJPLERROR"  && Adds Paul and Todd to certain emails
	tsendtoecho = iif(lusealt,mm.sendtoalt,mm.sendto)
	tccecho = iif(lusealt,mm.ccalt,mm.cc)
	if ltesting
		tsendto = tsendtotest
		tcc = tcctest
	endif
	use in mm

	dimension thisarray(1)
	csuffix = ""
	do case
	case coffice = "M"
		cwhse = "MIAMI"
	case coffice = "C"
		cwhse = "SAN PEDRO"
	case coffice = "L"
		cwhse = "MIRA LOMA"
	otherwise
		cwhse = "CARTERET"
	endcase

	lholdfile = ltesting && If true, keeps output file in 944HOLD folder, otherwise into 944OUT
	lemail = .t.  && If .t., sends mail to WAREHOUSES

	cupc = ""
	nstsets = 0
	xferupdate = .t.
	nfilenum = 0
	lbackup = .f.
	loverage = .f.
	ljoemail = .t.
	lcopyca = .t.
	cerrmsg = ""
	liserror = .t.
	lxfer944 = .f.
	nctntotal = 0
	noverallqty = 0

	cwo_num = alltrim(str(nwo_num))

	if !ltesting
		if used("mj944hist")
			use in mj944hist
		endif
		use f:\3pl\data\mj944hist in 0 alias mj944hist
	endif


	create cursor temp1 (wo_num n(6), ;
		po_num c(5), ;
		style c(20), ;
		expected i, ;
		actual i)

*!* SET CUSTOMER CONSTANTS
	ccustname = "MARCJACOBSW"  && Customer Identifier (for folders, etc.)
	cmailname = "Marc Jacobs (W)"  && Proper name for eMail, etc.
	czladdress = "72 SPRING STREET 3RD FLOOR"
	czlcity = "NEW YORK"
	czlstate = "NY"
	czlzip = "10012"
	ccustprefix = "MJW" && Customer Prefix (for output file names)
	cx12 = "004010"  && X12 Standards Set used
	crecid = "2129070080"  && Recipient EDI address

	dt1 = ttoc(datetime(),1)
	dt2 = datetime()
	dtmail = ttoc(datetime())

*!* Search table (parent)

	goffice=coffice

	csq = [select * from inwolog where mod = ']+goffice+[' and wo_num = ]+transform(nwo_num)
	xsqlexec(csq,,,"wh")
	select inwolog
	index on wo_num tag wo_num

*!*		if used('inwolog')
*!*			use in inwolog
*!*		endif
*!*		use (cusefolder+"inwolog") in 0 alias inwolog order tag wo_num noupdate
	select inwolog
	if !seek(nwo_num)
		wait window "WO# "+cwo_num+" not found in INWOLOG...Terminating" timeout 3
		if !ltesting
			lcopyca = .t.
			do ediupdate with .t.,"MISS INWOLOG WO"
			throw
		endif
	else
*	    nAcctNum = inwolog.accountid
		cacct_ref = alltrim(inwolog.acct_ref)
	endif

	if !ltesting
		if seek(nwo_num,'mj944hist','wo_num')
			if mj944hist.acct_ref = cacct_ref
				wait window "Work Order "+cwo_num+" already created and logged in MJ944HIST" timeout 2
				select edi_trigger
				replace edi_trigger.processed with .t.,edi_trigger.comments with "Attempted reprocess on date: "+dtoc(date()) ;
					for edi_trigger.accountid = nacctnum and edi_trigger.wo_num = nwo_num in edi_trigger
				closedata()
				normalexit = .t.
				throw
			endif
		endif
	endif

*	cFilename = (lcHoldPath+"MJ"+cAcct_ref+".944") && Holds file here until complete and ready to send
*	cFilename2 = (lcOutPath+"MJ"+cAcct_ref+".944")
*	cArchiveFile = (lcArchivePath+"MJ"+cAcct_ref+"_"+dt1+".944")

****************************** request to change name of 944 file by adding wo requested by Sandra E  TM 11/19/2012
	cfilename = (lcholdpath+"MJ"+cwo_num+"_"+cacct_ref+".944") && Holds file here until complete and ready to send
	cfilename2 = (lcoutpath+"MJ"+cwo_num+"_"+cacct_ref+".944")
	carchivefile = (lcarchivepath+"MJ"+cwo_num+"_"+cacct_ref+"_"+dt1+".944")
****************************** request to change name of 944 file by adding wo requested by Sandra E  TM 11/19/2012

	store "" to c_cntrlnum,c_grpcntrlnum
	nfilenum = fcreate(cfilename)

*!* SET OTHER CONSTANTS
	nctnnumber = 1
	cstring = ""
	csendqual = "ZZ"
	if coffice='L'
		csendid = "USCA1DC"
	else
		csendid = "USNJ1DC"
	endif
	crecqual = "12"
	cfd = "*"  && Field/element delimiter
	csegd = "~" && Line/segment delimiter
	nsegctr = 0
	cterminator = ">" && Used at end of ISA segment
	cdate = dtos(date())
	ctruncdate = right(cdate,6)
	cdatetime = ttoc(datetime(),1)

	ctime = substr(cdatetime,9,4)
	cfiledate = cdate+ctime
	if coffice='J'
		corig = "J"
	endif
	if coffice='L'
		corig = "L"
	endif
	cqtyrec = "1"
	cctypeused = ""
	nqtyexpected = 0
	nctndamaged = 0
	nctnshortage = 0
	nctnexpected = 0
	nctnactual = 0
	cmissingctn = "Missing Cartons: "+chr(13)
	cpo = ""
	cstyle = ""
	npoexpected = 0
	npoactual = 0

** PAD ID Codes
	csendidlong = padr(csendid,15," ")
	crecidlong = padr(crecid,15," ")

*!* Serial number incrementing table
	if used("serfile")
		use in serfile
	endif
	use ("F:\3pl\data\serial\marcjacobsw944_serial") in 0 alias serfile

*!* Trigger Data
	if !used('edi_trigger')
		cedifolder = "F:\3PL\DATA\"
		use (cedifolder+"edi_trigger") in 0 alias edi_trigger order tag wo_num
	endif

*!* Search table (xref for PO's)
**** changed 1/13/17 TMARG SQL

	set step on
	useca("indet","wh",,,"select * from indet where mod = '"+goffice+"' and wo_num = "+transform(nwo_num))
***** Added 4/10/2017 to copy echo data from PL to indet (missing some of the data)
	csq = [select * from pl where mod = ']+goffice+[' and wo_num = ]+transform(nwo_num)
	xsqlexec(csq,,,"wh")

***** Added 4/10/2017 to copy echo data from PL to indet (missing some of the data)
	select pl
	scan
		select indet
		locate for wo_num=pl.wo_num and style=pl.style and color=pl.color and id=pl.id
		if found("INDET")
			replace echo with pl.echo  in indet
		else
		endif
	endscan
	tu("indet")
	if used('INDET')
		use in indet
	endif

	csq = [select * from indet where mod = ']+goffice+[' and wo_num = ]+transform(nwo_num)
	xsqlexec(csq,,,"wh")
	index on inwologid tag inwologid
	index on wo_num tag wo_num

	nrcnt = 0
	nisacount = 0
	nstcount = 1
	nsecount = 1

	ccontainer = alltrim(inwolog.container)
	creference = alltrim(inwolog.reference)

	select indet
	if !ltesting
		if !seek(nwo_num,"indet","wo_num")
			waitstr = "WO# "+cwo_num+" Not Found in Inbd Detail Table..."+chr(13)+;
				"WO has not been confirmed...Terminating"
			wait window waitstr timeout 3
			do ediupdate with .t.,"WO NOT CONF. IN INDET"
			return
		endif
	endif
	select inwolog
	cacctname = trim(inwolog.acctname)
	ccontainer = trim(inwolog.container)
	cctrtype = iif(substr(ccontainer,4,1)="U","SQ","AW")
	cacct_ref = trim(inwolog.acct_ref)
	alenhead = alines(apthead,inwolog.comments,.t.,chr(13))

	if nisacount = 0
		do num_incr
		store "ISA"+cfd+"00"+cfd+"          "+cfd+"00"+cfd+"          "+cfd+csendqual+cfd+csendidlong+cfd+;
			crecqual+cfd+crecidlong+cfd+ctruncdate+cfd+ctime+cfd+"U"+cfd+"00401"+cfd+;
			padl(c_cntrlnum,9,"0")+cfd+"0"+cfd+cintusage+cfd+cterminator+csegd to cstring
		do cstringbreak

		store "GS"+cfd+"RE"+cfd+csendid+cfd+crecid+cfd+cdate+cfd+ctime+cfd+c_cntrlnum+;
			cfd+"X"+cfd+cx12+csegd to cstring
		do cstringbreak

		nsegctr = 0
		nisacount = 1
	endif

	if nsecount = 1
		store "ST"+cfd+"944"+cfd+padl(c_grpcntrlnum,9,"0")+csegd to cstring
		do cstringbreak
		nsegctr = nsegctr + 1

		if ltesting and empty(cacct_ref)
			cacct_ref = "9999999"
		endif
		if ltesting = .t.
			store "W17"+cfd+corig+cfd+cdate+cfd+cacct_ref+cfd+cwo_num+cfd+cacct_ref+csegd to cstring
		else
			store "W17"+cfd+corig+cfd+cdate+cfd+cacct_ref+cfd+cfd+cwo_num+csegd to cstring  && changed TM 050614
&&      Store "W17"+cfd+cOrig+cfd+cdate+cfd+cWO_Num+cfd+cfd+cAcct_ref+csegd To cString
		endif
		do cstringbreak
		nsegctr = nsegctr + 1

		cconame = trim(segmentget(@apthead,"CONAME",alenhead))
		cconame = iif(empty(cconame),upper(cmailname),cconame)
		store "N1"+cfd+"SF"+cfd+upper(cconame)+csegd to cstring
		do cstringbreak
		nsegctr = nsegctr + 1

		store "N3"+cfd+czladdress+csegd to cstring
		do cstringbreak
		nsegctr = nsegctr + 1

		store "N4"+cfd+czlcity+cfd+czlstate+cfd+czlzip+cfd+"US"+csegd to cstring
		do cstringbreak
		nsegctr = nsegctr + 1

		if coffice='L'
			store "N1"+cfd+"RC"+cfd+"TOLL MIRA LOMA"+csegd to cstring
			do cstringbreak
			nsegctr = nsegctr + 1

			store "N3"+cfd+"3355 DULLES DRIVE"+csegd to cstring
			do cstringbreak
			nsegctr = nsegctr + 1

			store "N4"+cfd+"MIRA LOMA"+cfd+"CA"+cfd+"91752"+cfd+"US"+csegd to cstring
			do cstringbreak
			nsegctr = nsegctr + 1

		else

			store "N1"+cfd+"RC"+cfd+"TOLL CARTERET"+csegd to cstring
			do cstringbreak
			nsegctr = nsegctr + 1

			store "N3"+cfd+"800 FEDERAL BLVD"+csegd to cstring
			do cstringbreak
			nsegctr = nsegctr + 1

			store "N4"+cfd+"CARTERET"+cfd+"NJ"+cfd+"07008"+cfd+"US"+csegd to cstring
			do cstringbreak
			nsegctr = nsegctr + 1

		endif
*!*		FUTURE USE
*!*		STORE "PER"+cfd+"CN"+cfd+"CHRIS MALCOLM"+cfd+"TE"+cfd+"732-750-9000"+cfd+"EM"+"cmalcolm@fmiint.com"+csegd TO cString
*!*		DO cstringbreak
*!*		nSegCtr = nSegCtr + 1

		if !empty(ccontainer)
			store "N9"+cfd+cctrtype+cfd+ccontainer+csegd to cstring  && Used for either AWB or container type
			do cstringbreak
			nsegctr = nsegctr + 1
		endif

		cdiv = trim(segmentget(@apthead,"DIVCODE",alenhead))
		if !empty(cdiv)
			store "N9"+cfd+"6P"+cfd+cdiv+csegd to cstring  && Added Division info per Joe S.
			do cstringbreak
			nsegctr = nsegctr + 1
		endif

		store "G62"+cfd+"AB"+cfd+dtos(date())+csegd to cstring
		do cstringbreak
		nsegctr = nsegctr + 1
		nsecount = 0
	endif

	select indet

	scan for indet.inwologid = inwolog.inwologid
		if indet.totqty = 0
			loop
		endif

		cstyle = alltrim(indet.style)
		cunits = indet.pack
		nctnwt = indet.ctnwt
		nctncube = indet.ctncube
		lechomsg = .f.

		if inlist(inwolog.brokerref,"BOL","GSI")
			nacct= indet.accountid
			if nacct=6305
				nacct = 6303
			endif

*!*	*Set Step On

*!*	      =Seek(Str(nAcct,4)+indet.Style+indet.Color+indet.Id,"upcmast","stylecolid")
*!*	      If Found("upcmast")
*!*	        cUPC   = upcmast.upc
*!*	      Endif
*!*	      If nAcct = 6304
*!*	*     Set Step On
			alendet = alines(aptdet,indet.echo,.t.,chr(13))
			cupc     = trim(segmentget(@aptdet,"UPC",alendet))
			clinenum = trim(segmentget(@aptdet,"LINEID",alendet))
			cpo      = trim(segmentget(@aptdet,"PO",alendet))
*!*	      Endif

			store "W07"+cfd+alltrim(transform(indet.totqty))+cfd+"EA"+cfd+cfd+"UP"+cfd+alltrim(cupc)+replicate(cfd,4)+cfd+"LT"+cfd+cpo+csegd to cstring
			do cstringbreak
			nsegctr = nsegctr + 1

			if empty(clinenum)
				waitstr = "Line number is missing from Inbd Detail Table..."
				wait window waitstr timeout 3
				do ediupdate with .t.,"MISSING LINE NUM"
				throw
			else
				store "N9"+cfd+"LI"+cfd+clinenum+csegd to cstring
				do cstringbreak
				nsegctr = nsegctr + 1
			endif

			noverallqty = noverallqty+indet.totqty  &&TM
			nctnexpected = noverallqty              &&TM
			cctnexpected = transform(noverallqty)   &&TM
		else
			do case
			case empty(alltrim(indet.echo))
*!*					nInrec = RECNO()
*!*					DO m:\dev\prg\mj_echocorrect WITH nWO_Num
*!*					SELECT indet
*!*					GO nInrec
*!*					IF EMPTY(ALLTRIM(indet.ECHO))
				cerrmsg = "EMPTY ECHO FIELD"
				lechomsg = .t.
				if inwolog.brokerref='BOL'
					lechomsg =  .f.
				endif
*!*					ENDIF
			case !("LINEID"$indet.echo)
				cerrmsg = "MISSING ECHO LINENUM"
				lechomsg = .t.
			case !("UPC"$indet.echo)
				cerrmsg = "MISSING ECHO UPC"
				lechomsg = .t.
			endcase
			if lechomsg
				lechomsg = .f.
				wait window cerrmsg timeout 2
				do ediupdate with .t.,cerrmsg
				throw
			endif

			if ltesting
*			SET STEP ON
			endif
			alendet = alines(aptdet,indet.echo,.t.,chr(13))
			noverallqty = noverallqty+indet.totqty
			nctnexpected = noverallqty
			cctnexpected = transform(noverallqty)

			if "ROLLED-UP LINES"$indet.echo && Added this section 08.24.2012
				ncheckqty = 0  && Summed to compare to received qty in INDET
				for i = 1 to alendet
					linedata = alltrim(aptdet[i])
					if "TOTQTY*"$linedata    &&&linedata = Chr(13)+"TOTQTY*"
						linedata = strtran(linedata,chr(10),"")
						linedata = strtran(linedata,"TOTQTY*","")
						ncheckqty = ncheckqty+int(val(linedata))
					endif
				endfor

				nqtydiff = 0 && Used for overage/shortage
				if ncheckqty # indet.totqty
					nqtydiff = (ncheckqty - indet.totqty) && Can be positive (shortage) or negative (overage)
				endif

				assert .f. message "At ECHO memo field scan in Rollup"
				if ltesting
*				SET STEP ON
				endif
				for i = 1 to alendet
					linedata = alltrim(aptdet[i])
					linedata = strtran(linedata,chr(10),"")
					do case
					case linedata = "+" or linedata = "ROLLED-UP"
						loop

					case linedata = "PO*"
						cpo = strtran(linedata,"PO*","")

					case linedata = "UPC*"
						cupc = strtran(linedata,"UPC*","")
*TM         upccheck(cUPC)

					case linedata = "TOTQTY*"
						ctotqty = strtran(linedata,"TOTQTY*","")
						ntotqty = int(val(ctotqty))
						do case
						case nqtydiff > 0 && Shortage
							if ntotqty >= nqtydiff
								ntotqty = ntotqty - nqtydiff
								nqtydiff = 0
							else
								nqtydiff = nqtydiff - ntotqty
								ntotqty = 0
							endif
							ctotqty = alltrim(str(ntotqty))
						case nqtydiff < 0
							ntotqty = ntotqty+abs(nqtydiff)  && Overage
							ctotqty = alltrim(str(ntotqty))
							nqtydiff = 0
						otherwise
							ctotqty = alltrim(str(ntotqty))
						endcase

					case linedata = "LINEID*"
						cli = strtran(linedata,"LINEID*","")

					case linedata = "="
						store "W07"+cfd+ctotqty+cfd+"EA"+cfd+cfd+"UP"+cfd+alltrim(cupc)+replicate(cfd,4)+cfd+"LT"+cfd+cpo+csegd to cstring
						do cstringbreak
						nsegctr = nsegctr + 1

						store "N9"+cfd+"LI"+cfd+cli+csegd to cstring
						do cstringbreak
						nsegctr = nsegctr + 1

					otherwise
						loop  && Redundant loop added
					endcase
				endfor
			else
				ctotqty = alltrim(str(indet.totqty))
				cpo = trim(segmentget(@aptdet,"PO",alendet))
				cupc = trim(segmentget(@aptdet,"UPC",alendet))
				thisdetrec= recno("indet")
* upccheck(cUPC)  && PG removed thus additional UPC check 4/24/2014
				select indet
				goto thisdetrec

				store "W07"+cfd+ctotqty+cfd+"EA"+cfd+cfd+"UP"+cfd+alltrim(cupc)+replicate(cfd,4)+cfd+"LT"+cfd+cpo+csegd to cstring
				do cstringbreak
				nsegctr = nsegctr + 1

				cli = trim(segmentget(@aptdet,"LINEID",alendet))
				store "N9"+cfd+"LI"+cfd+cli+csegd to cstring
				do cstringbreak
				nsegctr = nsegctr + 1
			endif
		endif
	endscan
	wait window "END OF SCAN/CHECK PHASE ROUND..." nowait

	nstcount = 0
	do close944
	lxfer944 = .t.
	=fclose(nfilenum)

	if used('serfile')
		use in serfile
	endif

	if !ltesting
		if !used("ftpedilog")
			select 0
			use f:\edirouting\ftpedilog alias ftpedilog
			insert into ftpedilog (ftpdate,filename,acct_name,type) values (dt2,cfilename,"cCustname",lcedi_type)
			use in ftpedilog
		endif
	endif

	select edi_trigger

	wait "944 Creation process complete:"+chr(13)+cfilename window at 45,60 timeout 3
*	DO F:\polorl\prg\total944.prg WITH lXfer944,cFilename  && Checks expected vs. actual totals
	cfin = "944 FILE "+cfilename2+" CREATED WITHOUT ERRORS"

	copy file &cfilename to &carchivefile

	if !lholdfile and !ltesting
		copy file &cfilename to &cfilename2
		erase &cfilename
	endif

	if !ltesting
		select edi_trigger
		locate
		replace edi_trigger.processed with .t.,edi_trigger.created with .t.,edi_trigger.when_proc with datetime(),;
			edi_trigger.fin_status with "944 CREATED",edi_trigger.errorflag with .f.,file944crt with cfilename ;
			for edi_trigger.wo_num = nwo_num and edi_trigger.accountid = nacctnum and edi_trigger.edi_type = lcedi_type  &&"944"
		locate
*!*			added 1/30/2017 TMARG to capture 997 data

		m.groupnum=c_cntrlnum
		m.isanum=padl(c_cntrlnum,9,"0")
		m.transnum=padl(c_grpcntrlnum,9,"0")
		m.edicode="RE"
		m.accountid=nacctnum
		m.loaddt=date()
		m.loadtime=datetime()
		m.filename=cfilename
		m.ship_ref=transform(nwo_num)
		m.senderid=csendidlong
		m.rcvid=crecidlong
		insertinto("ackdata","wh",.t.)
		tu("ackdata")

	endif
	liserror = .f.
	if lemail
*	SET STEP ON
		joemail()
	endif
	release all like t*
	closedata()
	normalexit = .t.

catch to oerr
	if normalexit = .f.

		closeout()
		tsendto = iif(ltesting,tsendtotest,tsendtoerr)
		tcc = iif(ltesting,tcctest,tccerr)


		tmessage = ""
		assert .f. message "In Catch section..."

		tsubject = cmailname+" Inbound Error ("+transform(oerr.errorno)+") at "+ttoc(datetime())
		lcsourcemachine = sys(0)
		lcsourceprogram = "marcjacobsw944_create.prg"

		tmessage =tmessage+ "Try/CATCH Exeception Message:"+chr(13)+;
			[  Error: ] + str(oerr.errorno) +chr(13)+;
			[  LineNo: ] + str(oerr.lineno) +chr(13)+;
			[  Message: ] + oerr.message +chr(13)+;
			[  Procedure: ] + oerr.procedure +chr(13)+;
			[  Details: ] + oerr.details +chr(13)+;
			[  StackLevel: ] + str(oerr.stacklevel) +chr(13)+;
			[  LineContents: ] + oerr.linecontents+chr(13)+;
			[  UserValue: ] + oerr.uservalue+chr(13)+;
			[  Computer:  ] +lcsourcemachine+chr(13)+;
			[  Inbound file:  ] +cfilename+chr(13)+;
			[  Program:   ] +lcsourceprogram
		tmessage =tmessage+chr(13)+chr(13)+"SPECIFIC ERROR: "+cerrmsg

		do form m:\dev\frm\dartmail2 with tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
	else
		wait window at 10,10 "Normal Exit " timeout 2
	endif
finally
	set century on
	on error
	if ltestinput or ltesting
		close databases all
		if !used('edi_trigger')
			use f:\3pl\data\edi_trigger in 0
		endif
	endif
endtry

************************************************************************
*!* END OF MAIN PROGRAM
************************************************************************


****************************
procedure stclose
****************************
*	SET STEP ON
nctnactual = nctnexpected
nctnshortage = (nctnexpected - nctnactual - nctndamaged)
nctntotal = nctntotal + nctnexpected
if nctnshortage < 0
	wait clear
	loverage = .t.
	tsendto = tsendtoerr
	tcc = tccerr
	tsubject = cmailname+" 944 EDI File OVERAGE, WO "+cwo_num
	tattach = " "
	tmessage = "PO "+cponumold+" OVERAGE (More ctns in inbound tables than expected for a PO)! Check EDI File immediately!"
	tmessage = tmessage + "Expected: "+cctnexpected+", Actual: "+transform(nctnactual)
	tfrom = "Toll EDI Operations <toll-edi-ops@tollgroup.com>"
	do form m:\dev\frm\dartmail2 with tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
endif
*SET STEP ON

*!* For 944 process only!
if nsecount = 0
	noverallqty = 0
	store "W14"+cfd+cctnexpected+;
		cfd+alltrim(str(nctnexpected-nctnshortage))+;
		cfd+alltrim(str(nctnshortage))+csegd to cstring
	do cstringbreak
	nsegctr = nsegctr + 1
*!* End 944 addition

	store  "SE"+cfd+alltrim(str(nsegctr+1))+cfd+padl(c_grpcntrlnum,9,"0")+csegd to cstring
	fwrite(nfilenum,cstring)
	nstcount = 1
	nsecount = 1
endif
nsegctr = 0
nstsets = nstsets + 1

select serfile
replace serfile.grpseqnum with serfile.grpseqnum + 1
c_grpcntrlnum = alltrim(str(serfile.grpseqnum))
*? c_GrpCntrlNum
select indet1
lxcount = 1
store 0 to nctnexpected,nctnactual,nctndamaged,nctnshortage
return

****************************
procedure close944
****************************
** Footer Creation
*SET STEP ON
if nstcount = 0
*!* For 944 process only!
	nctnactual = nctnexpected
	nctnshortage = (nctnexpected - nctnactual - nctndamaged)
	store "W14"+cfd+alltrim(str(nctnexpected))+;
		cfd+alltrim(str(nctnexpected-nctnshortage))+;
		cfd+alltrim(str(nctnshortage))+csegd to cstring
	do cstringbreak
	nsegctr = nsegctr + 1
*!* End 944 addition

	store  "SE"+cfd+alltrim(str(nsegctr+1))+cfd+padl(c_grpcntrlnum,9,"0")+csegd to cstring
	fwrite(nfilenum,cstring)
	nsegctr = 0
	nstsets = nstsets + 1
endif

store  "GE"+cfd+alltrim(str(nstsets))+cfd+c_cntrlnum+csegd to cstring
fwrite(nfilenum,cstring)

store  "IEA"+cfd+"1"+cfd+padl(c_cntrlnum,9,"0")+csegd to cstring
fwrite(nfilenum,cstring)

return

****************************
procedure cstringbreak
****************************
*FPUTS(nFilenum,cString)
fwrite(nfilenum,cstring)
return

****************************
procedure num_incr
****************************
select serfile
replace serfile.seqnum with serfile.seqnum + 1 in serfile
replace serfile.grpseqnum with serfile.grpseqnum + 1 in serfile
c_cntrlnum = alltrim(str(serfile.seqnum))
c_grpcntrlnum = alltrim(str(serfile.grpseqnum))
return

****************************
procedure ediupdate
****************************
parameters liserror,cfin
cerrmsg = cfin

*SET STEP ON
if !ltesting
	select edi_trigger
	locate
	replace processed with .t.,created with .f.,fin_status with cfin,errorflag with .f. ;
		for edi_trigger.wo_num = nwo_num and edi_trigger.accountid = nacctnum and edi_trigger.edi_type = lcedi_type  &&"944"
	locate
	if !liserror
		replace when_proc with datetime(),file944crt with archivefile ;
			for edi_trigger.wo_num = nwo_num ;
			and edi_trigger.accountid = nacctnum and edi_trigger.edi_type = lcedi_type &&"944"
	else
		replace processed with .t.,created with .f.,fin_status with cfin,errorflag with .t. ;
			edi_trigger.file944crt with "" ;
			for edi_trigger.wo_num = nwo_num and edi_trigger.accountid = nacctnum ;
			and edi_trigger.edi_type = lcedi_type && "944"
	endif
endif
if ltesting

	tsendto = tsendtotest
	tcc = tcctest
endif
if ljoemail and lemail
	joemail()
endif
closeout()
endproc

****************************
procedure closeout
****************************
if !empty(nfilenum)
	=fclose(nfilenum)
	erase &cfilename
endif
closedata()
endproc

****************************
procedure closedata
****************************
if used('edi_xfer')
	use in edi_xfer
endif
if used('csrBCdata')
	use in csrbcdata
endif
if used('detail')
	use in detail
endif
if used('indet')
	use in indet
endif
if used('mj944hist')
	use in mj944hist
endif
if used('inwolog')
	use in inwolog
endif
release all like ccontainer
on error
wait clear
_screen.caption = "INBOUND POLLER - EDI"
endproc

****************************
procedure joemail
****************************
*	SET STEP ON
if liserror
	tsubject = cmailname+" EDI ERROR (944), Inbd. WO "+cwo_num
	if !ltesting
		if "ECHO"$cerrmsg
			tsendto = tsendtoecho
			tcc = tccecho
		else
			tsendto = tsendtoerr
			tcc = tccerr
		endif
	endif
else
	tsubject = cmailname+" EDI FILE (944) Created, Inbd. WO "+cwo_num
endif

tattach = " "
tmessage = "Toll Inbound WO #: "+cwo_num+chr(10)
if empty(cerrmsg)
	tmessage = tmessage + cfin
else
	tmessage = tmessage + cerrmsg
endif

do form m:\dev\frm\dartmail2 with tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
endproc

****************************
procedure segmentget
****************************

parameter thisarray,lckey,nlength

for i = 1 to nlength
	if i > nlength
		exit
	endif
	lnend= at("*",thisarray[i])
	if lnend > 0
		lcthiskey =trim(substr(thisarray[i],1,lnend-1))
		if occurs(lckey,lcthiskey)>0
			return substr(thisarray[i],lnend+1)
			i = 1
		endif
	endif
endfor

return ""
endproc

****************************
procedure upccheck
****************************
parameters cupc
if ltesting
	wait window "INDETID "+transform(indet.indetid) nowait
*		SET STEP ON
endif

*!*		SELECT upcmast
*!*		LOCATE FOR upcmast.accountid = nAcctNum AND upcmast.upc = cUPC
*!*		IF !FOUND()
*!*			cErrMsg = cUPC+" not in UPCMAST"
*!*			WAIT WINDOW cErrMsg TIMEOUT 2
*!*			DO ediupdate WITH .T.,cErrMsg
*!*			THROW
*!*		ENDIF

locate
nacctnumstyle = 6303  && All styles are held under the main wholesale account
if !upcmastsql(nacctnumstyle,indet.style,indet.color,indet.id)
	cerrmsg = "UPCMAST <> INDET data, A/R"+cacct_ref+", IDID "+transform(indet.indetid)
	wait window cerrmsg timeout 2
	do ediupdate with .t.,cerrmsg
	throw
endif

endproc
