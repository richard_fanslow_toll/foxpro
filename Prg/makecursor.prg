lparameters xinvoice, xtkdaily, xavaillog, xwolog, xdetail

if xinvoice
	create cursor xinvoice ( ;
		invoiceid  i, ;
		invnum     c(6), ;
		wo_num     n(8), ;
		invdt      d, ;
		invamt     n(10,2), ;
		pdfloaded  l, ;
		updateby   c(8), ;
		updatedt   t, ;
		addby      c(8), ;
		adddt      t)

	index on invnum tag invnum
	index on wo_num tag wo_num
	index on invamt tag invamt
	index on invdt tag invdt descending
endif

if xtkdaily
	create cursor xtkdaily ( ;
		tdailyid   i, ;
		dailyid    i, ;
		wo_num     n(7), ;
		accountid  i, ;
		action     c(2), ;
		actioname  c(80), ;
		container  c(12), ;
		chassis    c(12), ;
		dispatched d, ;
		dis_time   c(8), ;
		timeout    c(5), ;
		time_in    c(8), ;
		dis_driver c(30), ;
		dis_truck  c(8), ;
		startloc   c(23), ;
		endloc     c(23), ;
		undel      l, ;
		pickedup   d, ;
		delivered  d, ;
		yard_hold  d, ;
		partial    l, ;
		office     c(1), ;
		returned   d, ;
		drop       l, ;
		remarks    c(80), ;
		mark       l, ;
		damaged    l, ;
		export     l, ;
		choice     c(80), ;
		confirmby  c(8), ;
		updateby   c(8), ;
		updatedt   t, ;
		addby      c(8), ;
		adddt      t)

	index on wo_num tag wo_num
	index on actioname tag actioname
	index on container tag container
	index on dis_time tag dis_time
	index on dis_driver tag dis_driver
	index on dis_truck tag dis_truck
	index on right(timeout,1)+iif(timeout="12","00",left(timeout,2))+substr(timeout,3,2) tag timeout
	index on dtos(dispatched)+str(999999-val(strtran(chr2time(time_in),":","")),6) tag dispatched descending
endif

*

if xavaillog
	create cursor xavaillog ( ;
		wo_num     n(7), ;
		called     d, ;
		remarks    c(30), ;
		note       c(50), ;
		ftexpires  d, ;
		addby      c(10), ;
		adddt      t)

	index on wo_num tag wo_num
	index on note tag note
	index on remarks tag remarks	
	index on ftexpires tag ftexpires
*	index on date()-called tag called
	index on dtos(called)+ttoc(adddt) tag called descending
endif

*

if xwolog
	create cursor xwolog ( ;
		wo_date    d, ;
		office     c(1), ;
		entryport  c(12), ;
		wo_num     n(7), ;
		accountid  i, ;
		acctname   c(30), ;
		type       c(1), ;
		typedesc   c(12), ;
		brokref    c(15), ;
		vessel     c(30), ;
		ssco       c(20), ;
		awb        c(13), ;
		loose      l, ;
		container  c(12), ;
		size       c(6), ;
		chassis    c(12), ;
		quantity   n(6), ;
		qty_type   c(7), ;
		weight     n(7), ;
		cbm        n(8,2), ;
		goh        l, ;
		puloc      c(30), ;
		delloc     c(30), ;
		retloc     c(30), ;
		pickedup   d, ;
		delivered  d, ;
		mtinyard   d, ;
		returned   d, ;
		remarks    c(50), ;
		fromloc    n(4), ;
		toloc      n(4), ;
		arfile     c(6), ;
		inv_num    c(8), ;
		inv_date   d, ;
		appt       d, ;
		apptime    c(8), ;
		cust_ref   c(15), ;
		exported   d, ;
		comments   c(25), ;
		origin     c(30), ;
		vendor     c(30), ;
		forwarder  c(30), ;
		qty        c(15), ;
		wt         c(10), ;
		cwonum     c(7), ;
		deliverto  c(30), ;
		eta        d)
endif

*

if xdetail
	create cursor xdetail ( ;
		accountid  i(4), ;
		wo_date    d, ;
		wo_num     n(7), ;
		tripno     n(7), ;
		brokref    c(15), ;
		hawb       c(15), ;
		container  c(12), ;
		style      c(20), ;
		pl_qty     n(6), ;
		rcv_qty    n(6), ;
		quantity   n(6), ;
		qty_type   c(7), ;
		delloc     c(20), ;
		stripped   d, ;
		delivered  d, ;
		remarks    c(50), ;
		po_num     c(10), ;
		arrival    c(14), ;
		startdt    d, ;
		updateby   c(8), ;
		updatedt   t, ;
		addby      c(8), ;
		adddt      t)
		
	index on wo_date tag wo_date
	index on wo_num tag wo_num     
	index on tripno tag tripno     
	index on brokref tag brokref    
	index on hawb tag hawb       
	index on container tag container  
	index on style tag style      
	index on pl_qty tag pl_qty     
	index on rcv_qty tag rcv_qty    
	index on quantity tag quantity   
	index on qty_type tag qty_type   
	index on delloc tag delloc     
	index on stripped tag stripped   
	index on delivered tag delivered  
	index on remarks tag remarks    
	index on po_num tag po_num     
	index on arrival tag arrival    
endif
