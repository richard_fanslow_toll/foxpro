* PROCESS NAME:  SYNCLAIRE_RETURNS_RPT
*
* DESCRIPTION: reports miscellaneous data from RETURNS-type inbound work orders for prior month
*
* Supports Synclaire and BCNY, based on passed parameter 'S' or 'B' 
*
* build EXE as: F:\AUTO\SYNCLAIRE_RETURNS_RPT.EXE
*
* s.b. scheduled to run on day 1 of each month
*
* Adapted to run as auto exe MB 12/08/2017 - old code written by PG is at bottom, commented out.

LPARAMETERS tcClientCode

LOCAL loSYNCLAIRE_RETURNS_RPT, llTestMode

llTestMode = .F.

IF NOT llTestMode THEN
	utilsetup("SYNCLAIRE_RETURNS_RPT")
ENDIF

loSYNCLAIRE_RETURNS_RPT = CREATEOBJECT('SYNCLAIRE_RETURNS_RPT')

loSYNCLAIRE_RETURNS_RPT.MAIN( llTestMode, tcClientCode )

IF NOT llTestMode THEN
	schedupdate()
ENDIF

CLOSE DATABASES ALL
RETURN

#DEFINE LOGIT 1
#DEFINE WAITIT 2
#DEFINE NOWAITIT 4
#DEFINE SENDIT 8
#DEFINE CRLF CHR(13) + CHR(10)
#DEFINE LF CHR(10)
#DEFINE RETURN_DATA_MANDATORY .F.
#DEFINE RETURN_DATA_NOT_MANDATORY .T.

DEFINE CLASS SYNCLAIRE_RETURNS_RPT AS CUSTOM

	cProcessName = 'SYNCLAIRE_RETURNS_RPT'

	lTestMode = .F.  && .F. FOR NORMAL OPERATION, .T. TO SUPPRESS ALL BUT DEVELOPER EMAILS AND CHANGE INPUT TABLE, ETC.

	lAutoYield = .T.

	cStartTime = TTOC(DATETIME())

	* date properties
	dtNow = DATETIME()

	dToday = DATE()

	* wait window properties
	nWaitWindowTimeout = 2
	lWaitWindowIsOn = .T.

	* logfile properties
	lLoggingIsOn = .T.
	cLogFile = 'F:\UTIL\3PL\Logfiles\SYNCLAIRE_RETURNS_RPT_log.txt'

	* folder properties

	* process flow properties
	cClientCode = ''
	cClientName = ''
	nAccountID = 0

	* INTERNAL email properties
	lSendInternalEmailIsOn = .T.
	cFrom = "TGF EDI Ops <fmicorporate@fmiint.com>"
	cSendTo = 'mbennett@fmiint.com'
	cCC = ''
	cSubject = ''
	cAttach = ''
	cBodyText = ''

	cCOMPUTERNAME = ''
	cUSERNAME = ''

	FUNCTION INIT
		IF NOT DODEFAULT()
			RETURN .F.
		ENDIF
		WITH THIS
			CLEAR
			IF _VFP.STARTMODE > 0 THEN
				SET RESOURCE OFF
			ENDIF
			CLOSE DATA
			SET CENTURY ON
			SET DATE AMERICAN
			SET HOURS TO 24
			SET ANSI ON
			SET TALK OFF
			SET DELETED ON
			SET CONSOLE OFF
			SET EXCLUSIVE OFF
			SET SAFETY OFF
			SET EXACT OFF
			SET STATUS BAR ON
			SET SYSMENU OFF
			SET ENGINEBEHAVIOR 70
			_VFP.AUTOYIELD = .lAutoYield
			.cCOMPUTERNAME = UPPER(ALLTRIM(GETENV("COMPUTERNAME")))
			.cUSERNAME = GETENV("USERNAME")
			IF .lTestMode THEN
				.cSendTo = 'mbennett@fmiint.com'
				.cLogFile = 'F:\UTIL\3PL\Logfiles\SYNCLAIRE_RETURNS_RPT_log_TESTMODE.txt'
			ENDIF
			.lLoggingIsOn = .lLoggingIsOn AND NOT EMPTY(.cLogFile)
			IF .lLoggingIsOn THEN
				SET ALTERNATE TO (.cLogFile) ADDITIVE
				SET ALTERNATE ON
			ENDIF
			.cSubject = .cProcessName + ' process for: ' + DTOC(DATE())
		ENDWITH
	ENDFUNC


	FUNCTION DESTROY
		WITH THIS
			IF .lLoggingIsOn  THEN
				SET ALTERNATE OFF
				SET ALTERNATE TO
			ENDIF
		ENDWITH
		DODEFAULT()
	ENDFUNC


	FUNCTION MAIN
		LPARAMETERS tlTestMode, tcClientCode
		WITH THIS
			LOCAL i, lnNumberOfErrors, lcOutputFile, ldToday, ldStartDate, lcStartDate, lcOutputFolder

			TRY
				lnNumberOfErrors = 0

				.TrackProgress(.cProcessName + " process started....", LOGIT+SENDIT+NOWAITIT)
				.TrackProgress('COMPUTERNAME = ' + .cCOMPUTERNAME, LOGIT+SENDIT)
				.TrackProgress('USERNAME = ' + .cUSERNAME, LOGIT+SENDIT)
				.TrackProgress('PROJECT = SYNCLAIRE_RETURNS_RPT', LOGIT+SENDIT)

				.lTestMode = tlTestMode

				IF .lTestMode THEN
					.TrackProgress('====> TEST MODE', LOGIT+SENDIT)
				ENDIF

				.cClientCode = UPPER(ALLTRIM(tcClientCode))

				DO CASE
					CASE .cClientCode == 'B'
						.nAccountID = 6221
						.cClientName = 'BCNY'
						IF .lTestMode THEN
							lcOutputFolder = 'F:\UTIL\SYNCLAIRE\TESTREPORTS\'
						ELSE
							lcOutputFolder = 'F:\FTPUSERS\BCNY\RETURNSREPORT\'
						ENDIF
					CASE .cClientCode == 'S'
						.nAccountID = 6521
						.cClientName = 'SYNCLAIRE'
						IF .lTestMode THEN
							lcOutputFolder = 'F:\UTIL\SYNCLAIRE\TESTREPORTS\'
						ELSE
							lcOutputFolder = 'F:\FTPUSERS\SYNCLAIRE\RETURNSREPORT\'
						ENDIF
				ENDCASE

				.TrackProgress('.cClientName = ' + .cClientName, LOGIT+SENDIT)

				ldToday = DATE()

				ldStartDate = ldToday - DAY(ldToday) + 1

				ldStartDate = GOMONTH(ldStartDate, -1)

				.TrackProgress('ldStartDate = ' + DTOC(ldStartDate), LOGIT+SENDIT)

				.cSubject = .cProcessName + ' process for ' + .cClientName + ' for : ' + DTOC(ldToday)

				lcOutputFile = lcOutputFolder + .cClientName + "_RETURNS_REPORT_" + DTOS(ldStartDate) + ".XLS"

				.TrackProgress('lcOutputFile = ' + lcOutputFile, LOGIT+SENDIT)

				**************************************************************************************************

				xsqlexec("select * from inwolog where acct_ref = 'RETURN' and accountid = " + TRANSFORM(.nAccountID) + " and wo_date >= {" + DTOC(ldStartDate) + "}","inwolog",,"wh")

				xsqlexec("select * from indet where date_rcvd >= {" + DTOC(ldStartDate) + "} and accountid = " + TRANSFORM(.nAccountID),"indet",,"wh")

				xsqlexec("select * from zzindet where accountid = " + TRANSFORM(.nAccountID) + " and date_rcvd >= {" + DTOC(ldStartDate) + "}","xindet",,"wh")

				**************************************************************************************************

				CREATE CURSOR indat (;
					wo_num   INT,;
					CONTAINER CHAR(10),;
					wo_date DATE,;
					brokerref CHAR(20),;
					REFERENCE CHAR(20),;
					confirmdt DATE,;
					PO      CHAR(15),;
					POnum   CHAR(15),;
					UPC     CHAR(12),;
					NAME    CHAR(40),;
					ctnqty  N(10),;
					PACK    CHAR(10),;
					totqty  N(10),;
					totwt   N(10.2),;
					totcube N(10.2) )

				SELECT wo_num,wo_date,CONTAINER,acct_ref,confirmdt ;
					FROM inwolog WHERE !ISNULL(confirmdt) ;
					AND !EMPTY(CONTAINER) ;
					INTO CURSOR wolist READWRITE

				SELECT wolist
				SCAN

					SELECT inwolog.wo_num ,wo_date,CONTAINER,brokerref,REFERENCE,confirmdt,indet.PO, indet.STYLE AS UPC, indet.PACK, indet.totqty, SPACE(20) AS NAME ;
						FROM inwolog ;
						LEFT OUTER JOIN indet ;
						ON indet.inwologid = inwolog.inwologid ;
						WHERE (inwolog.wo_num = wolist.wo_num) AND units AND (confirmdt >= ldStartDate) ORDER BY confirmdt INTO CURSOR temp

					SELECT temp
					IF RECCOUNT("temp")= 0 THEN
						SELECT inwolog.wo_num ,wo_date,CONTAINER,brokerref,REFERENCE,confirmdt,xindet.PO, xindet.STYLE AS UPC, xindet.PACK, xindet.totqty, SPACE(20) AS NAME ;
							FROM inwolog ;
							LEFT OUTER JOIN xindet ;
							ON xindet.inwologid = inwolog.inwologid ;
							WHERE (inwolog.wo_num = wolist.wo_num) AND units AND (confirmdt >= ldStartDate) ORDER BY confirmdt INTO CURSOR temp
					ENDIF

					SELECT temp
					SCAN
						SCATTER MEMVAR
						*SELECT indat
						*APPEND BLANK
						*GATHER MEMVAR
						INSERT INTO indat FROM MEMVAR
					ENDSCAN

					SELECT inwolog.wo_num ,wo_date,CONTAINER,brokerref,REFERENCE,confirmdt,indet.PO, indet.cayset AS POnum,indet.totcube,indet.totwt,indet.STYLE AS UPC, indet.PACK, indet.totqty, ;
						SPACE(20) AS NAME ;
						FROM inwolog ;
						LEFT OUTER JOIN indet ;
						ON indet.inwologid = inwolog.inwologid ;
						WHERE (inwolog.wo_num = wolist.wo_num) AND !units AND (confirmdt >= ldStartDate) ORDER BY confirmdt INTO CURSOR temp

					IF RECCOUNT("temp") =0
						SELECT inwolog.wo_num ,wo_date,CONTAINER,brokerref,REFERENCE,confirmdt,xindet.PO, xindet.cayset AS POnum,xindet.totcube,xindet.totwt,xindet.STYLE AS UPC, xindet.PACK,;
							xindet.totqty, SPACE(20) AS NAME ;
							FROM inwolog ;
							LEFT OUTER JOIN xindet ;
							ON xindet.inwologid = inwolog.inwologid;
							WHERE (inwolog.wo_num = wolist.wo_num) AND !units AND (confirmdt >= ldStartDate) ORDER BY confirmdt INTO CURSOR temp
					ENDIF

					SELECT temp
					SCAN
						SELECT indat
						LOCATE FOR (wo_num = temp.wo_num) AND (PO = temp.PO)
						IF FOUND() THEN
							REPLACE indat.ctnqty  WITH temp.totqty, ;
								indat.PACK    WITH temp.PACK, ;
								indat.totcube WITH temp.totcube, ;
								indat.totwt   WITH temp.totwt, ;
								indat.POnum   WITH temp.POnum ;
								IN indat
						ENDIF
					ENDSCAN
				ENDSCAN


				SELECT indat
				SCAN
					upcmastsql(.nAccountID,indat.UPC)
					REPLACE indat.NAME WITH upcmast.DESCRIP IN indat
				ENDSCAN

				* added this line to sort by confirmdt 12/08/2017 MB
				SELECT * FROM indat ORDER BY confirmdt INTO CURSOR temp

				**************************************************************************************************

				SELECT temp
				COPY TO (lcOutputFile) XL5 FIELDS wo_num, CONTAINER, brokerref, REFERENCE, confirmdt, POnum, UPC, NAME, ctnqty, PACK, totqty, totwt, totcube

				IF FILE(lcOutputFile) THEN
					.TrackProgress('=====>  Output file created: ' + lcOutputFile, LOGIT+SENDIT+NOWAITIT)
				ELSE
					.TrackProgress('=====>  ERROR: output file not found: ' + lcOutputFile, LOGIT+SENDIT+NOWAITIT)
					THROW
				ENDIF

				**************************************************************************************************

				.TrackProgress(.cProcessName + ' process ended normally', LOGIT+SENDIT+NOWAITIT)

			CATCH TO loError

				.TrackProgress('There was an error.',LOGIT+SENDIT)
				.TrackProgress('The Error # is: ' + TRANSFORM(loError.ERRORNO), LOGIT+SENDIT)
				.TrackProgress('The Error Message is: ' + TRANSFORM(loError.MESSAGE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred in Program: ' + TRANSFORM(loError.PROCEDURE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred at line #: ' + TRANSFORM(loError.LINENO), LOGIT+SENDIT)
				lnNumberOfErrors = lnNumberOfErrors + 1
				IF TYPE('oExcel') = 'O' AND NOT ISNULL(oExcel) THEN
					oExcel.QUIT()
				ENDIF
				CLOSE DATA

			ENDTRY

			CLOSE DATA
			WAIT CLEAR
			***************** INTERNAL email results ******************************
			.TrackProgress('About to send status email.',LOGIT)
			.TrackProgress('--', SENDIT)
			.TrackProgress('The logfile is: ' + .cLogFile, SENDIT)
			.TrackProgress(.cProcessName + ' process started: ' + .cStartTime, LOGIT+SENDIT)
			.TrackProgress(.cProcessName + ' process finished: ' + TTOC(DATETIME()), LOGIT+SENDIT)

			IF .lSendInternalEmailIsOn THEN
				* try to trap error from not having dartmail dll's registered on user's pc...
				TRY
					DO FORM dartmail2 WITH .cSendTo,.cFrom,.cSubject,.cCC,.cAttach,.cBodyText,"A"
					.TrackProgress('Sent status email.',LOGIT)
				CATCH TO loError
					*=MESSAGEBOX('There was an error sending the status email. Please contact Mark Bennett.',0+64,"KRONOS Temp Time")
					.TrackProgress('There was an error sending the status email.',LOGIT)
					.TrackProgress('The Error # is: ' + TRANSFORM(loError.ERRORNO), LOGIT)
					.TrackProgress('The Error Message is: ' + TRANSFORM(loError.MESSAGE), LOGIT)
					.TrackProgress('The Error occurred in Program: ' + TRANSFORM(loError.PROCEDURE), LOGIT)
					.TrackProgress('The Error occurred at line #: ' + TRANSFORM(loError.LINENO), LOGIT)
					lnNumberOfErrors = lnNumberOfErrors + 1
				ENDTRY

			ELSE
				.TrackProgress('Did not send status email: THIS.lSendInternalEmailIsOn = FALSE.',LOGIT)
			ENDIF

		ENDWITH
		RETURN
	ENDFUNC && main


	PROCEDURE TrackProgress
		* do any combination of Wait Window, writing to logfile, and adding to body of email,
		* based on nFlags parameter.
		LPARAMETERS tcExpression, tnFlags
		WITH THIS
			IF BITAND(tnFlags,LOGIT) = LOGIT THEN
				IF .lLoggingIsOn THEN
					?
					? .cProcessName + "  " + TTOC(DATETIME()) + ": " + tcExpression
				ENDIF
			ENDIF
			IF .lWaitWindowIsOn THEN
				IF BITAND(tnFlags,NOWAITIT) = NOWAITIT THEN
					WAIT WINDOW tcExpression NOWAIT
				ENDIF
				IF BITAND(tnFlags,WAITIT) = WAITIT THEN
					WAIT WINDOW tcExpression TIMEOUT .nWaitWindowTimeout
				ENDIF
			ENDIF
			IF BITAND(tnFlags,SENDIT) = SENDIT THEN
				IF .lSendInternalEmailIsOn THEN
					.cBodyText = .cBodyText + tcExpression + CRLF + CRLF
				ENDIF
			ENDIF
		ENDWITH
	ENDPROC  &&  TrackProgress

ENDDEFINE


*!*	* OLD CODE BELOW ************************************************
*!*	PARAMETERS whichaccount

*!*	*whichaccount = 6521
*!*	lcAcct = TRANSFORM(whichaccount)
*!*	*set step On


*!*	CLOSE DATA ALL
*!*	lcStr = "accountid = "+lcAcct+" and wo_date >= {11/01/2017}"
*!*	useca("inwolog","wh",,,lcStr)

*!*	lcStr = "accountid = "+lcAcct
*!*	useca("indet","wh",,,lcStr)

*!*	xsqlexec("select * from zzindet where accountid = "+lcAcct+" and date_rcvd >= {11/01/2017}","xindet",,"wh")


*!*	CREATE CURSOR indat (;
*!*		wo_num   INT,;
*!*		CONTAINER CHAR(10),;
*!*		wo_date DATE,;
*!*		brokerref CHAR(20),;
*!*		REFERENCE CHAR(20),;
*!*		confirmdt DATE,;
*!*		PO      CHAR(15),;
*!*		POnum   CHAR(15),;
*!*		UPC     CHAR(12),;
*!*		NAME    CHAR(40),;
*!*		ctnqty  N(10),;
*!*		PACK    CHAR(10),;
*!*		totqty  N(10),;
*!*		totwt   N(10.2),;
*!*		totcube N(10.2) )

*!*	SELECT wo_num,wo_date,CONTAINER,acct_ref,confirmdt FROM inwolog WHERE acct_ref ="RETURN" AND wo_date >= DATE()-365  AND !ISNULL(confirmdt) INTO CURSOR wolist READWRITE
*!*	SELECT wolist
*!*	DELETE FOR EMPTY(CONTAINER)

*!*	SELECT indat
*!*	ZAP

*!*	SELECT wolist
*!*	SCAN

*!*		SELECT inwolog.wo_num ,wo_date,CONTAINER,brokerref,REFERENCE,confirmdt,indet.PO, indet.STYLE AS UPC, indet.PACK, indet.totqty, SPACE(20) AS NAME FROM inwolog LEFT OUTER JOIN indet ON indet.inwologid = inwolog.inwologid;
*!*			WHERE inwolog.accountid = &lcAcct AND inwolog.wo_num = wolist.wo_num AND units AND acct_ref="RETURN" AND confirmdt >= DATE()-365 ORDER BY confirmdt INTO CURSOR temp
*!*		SELECT temp
*!*		IF RECCOUNT("temp")=0
*!*			SELECT inwolog.wo_num ,wo_date,CONTAINER,brokerref,REFERENCE,confirmdt,xindet.PO, xindet.STYLE AS UPC, xindet.PACK, xindet.totqty, SPACE(20) AS NAME FROM inwolog LEFT OUTER JOIN xindet ON xindet.inwologid = inwolog.inwologid;
*!*				WHERE inwolog.accountid = &lcAcct AND inwolog.wo_num = wolist.wo_num AND units AND acct_ref="RETURN" AND confirmdt >= DATE()-365 ORDER BY confirmdt INTO CURSOR temp

*!*		ENDIF

*!*		SCAN
*!*			SELECT temp
*!*			SCATTER MEMVAR
*!*			SELECT indat
*!*			APPEND BLANK
*!*			GATHER MEMVAR
*!*			*   Insert Into indat From memvar
*!*		ENDSCAN

*!*		SELECT inwolog.wo_num ,wo_date,CONTAINER,brokerref,REFERENCE,confirmdt,indet.PO, indet.cayset AS POnum,indet.totcube,indet.totwt,indet.STYLE AS UPC, indet.PACK, indet.totqty,;
*!*			SPACE(20) AS NAME FROM inwolog LEFT OUTER JOIN indet ON indet.inwologid = inwolog.inwologid;
*!*			WHERE inwolog.accountid = &lcAcct AND inwolog.wo_num = wolist.wo_num AND !units AND acct_ref="RETURN" AND confirmdt >= DATE()-365 ORDER BY confirmdt INTO CURSOR temp
*!*		SELECT temp

*!*		IF RECCOUNT("temp") =0
*!*			SELECT inwolog.wo_num ,wo_date,CONTAINER,brokerref,REFERENCE,confirmdt,xindet.PO, xindet.cayset AS POnum,xindet.totcube,xindet.totwt,xindet.STYLE AS UPC, xindet.PACK,;
*!*				xindet.totqty, SPACE(20) AS NAME FROM inwolog LEFT OUTER JOIN xindet ON xindet.inwologid = inwolog.inwologid;
*!*				WHERE inwolog.accountid = &lcAcct AND inwolog.wo_num = wolist.wo_num AND !units AND acct_ref="RETURN" AND confirmdt >= DATE()-365 ORDER BY confirmdt INTO CURSOR temp
*!*		ENDIF

*!*		SCAN
*!*			SELECT indat
*!*			LOCATE FOR wo_num = temp.wo_num AND PO = temp.PO
*!*			IF FOUND()
*!*				REPLACE ctnqty  WITH temp.totqty
*!*				REPLACE PACK    WITH temp.PACK
*!*				REPLACE totcube WITH temp.totcube
*!*				REPLACE totwt   WITH temp.totwt
*!*				REPLACE POnum   WITH temp.POnum
*!*			ENDIF
*!*		ENDSCAN
*!*	ENDSCAN


*!*	SELECT indat
*!*	SCAN
*!*		IF whichaccount = 6521
*!*			upcmastsql(6521,indat.UPC)
*!*		ELSE
*!*			upcmastsql(6221,indat.UPC)
*!*		ENDIF
*!*		REPLACE indat.NAME WITH upcmast.DESCRIP IN indat
*!*	ENDSCAN

*!*	IF whichaccount = 6521
*!*		EXPORT TO F:\ftpusers\synclaire\inboundReport\synclaire_return_rpt.XLS TYPE XLS FIELDS wo_num,CONTAINER,brokerref,REFERENCE,confirmdt,POnum,UPC,NAME,ctnqty,PACK,totqty,totwt,totcube
*!*	ELSE
*!*		EXPORT TO F:\ftpusers\BCNY\inboundReport\bcny_return_rpt.XLS TYPE XLS FIELDS wo_num,CONTAINER,brokerref,REFERENCE,confirmdt,POnum,UPC,NAME,ctnqty,PACK,totqty,totwt,totcube
*!*	ENDIF
*!*	RETURN
************************************************************************************************************************************
************************************************************************************************************************************
