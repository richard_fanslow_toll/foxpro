**lprpt
**12/08/10 mvw
**
**report all current inventory in locations DEFECT and QCMODD for Lindsay Philips
xaccountid=6137
xpath=wf("C",xaccountid)

use &xpath.invenloc in 0

xsqlexec("select * from account where inactive=0","account",,"qq")
index on accountid tag accountid
set order to

xacctname=acctname(xaccountid)

select whseloc,style,color,id,pack,locqty,locqty*val(pack) as unitqty from invenloc ;
	where accountid = xaccountid and inlist(whseloc,'DEFECT','QCMODD') and locqty>0 ;
	order by whseloc,style,color,id into cursor xrpt

xfilename = "h:\fox\"+sys(3)+".XLS"
copy to &xfilename type xls

cto="jennifer@lindsay-phillips.com,dalbert@lindsay-phillips.com,patti@lindsay-phillips.com"
ccc="mwinter@fmiint.com"

cAttach=xfilename
cFrom="TGF WMS Operations <fmicorporate@fmiint.com>"
cSubject=alltrim(xacctname)+" - DEFECT & QCMODD Locations as of "+transform(date())
cMessage='Worksheet Attached'
do form dartmail with cto,cfrom,csubject,ccc,cattach,cmessage,"A"

use in account
use in invenloc
use in xrpt

set database to WH
close data
