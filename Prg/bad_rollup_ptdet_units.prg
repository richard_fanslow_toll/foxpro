******************************
*Procedure oldnewrollup
******************************
nChangedRecs = 0
Select Count(1) as dup,ptid,style,color,id,pack,upc From xptdet into Cursor xptids Group By ptid,style,color,id,pack,upc

Select xptids
Count For dup >1 To nChangedRecs

Select xptdet
Set Deleted On
Locate
Select xptids
Scan For dup >1
  Select xptdet 
  Locate For style=xptids.style and color=xptids.color and id=xptids.id and pack=xptids.pack and upc=xptids.upc
  Scatter Memvar memo
  Sum(totqty) to nTotqty1  for ptid=xptids.ptid and style=xptids.style and color=xptids.color and id=xptids.id and pack=xptids.pack and upc=xptids.upc
  Select xptdet
  Delete for style=xptids.style and color=xptids.color and id=xptids.id and pack=xptids.pack and upc=xptids.upc 
  Insert Into xptdet From Memvar 
  replace totqty  With nTotqty1 In xptdet
  replace origqty With nTotqty1 In xptdet
Endscan

If nChangedRecs > 0
  Wait Window "There were "+Trans(nChangedRecs)+" records rolled up" Timeout 1
Endif

*Endproc
**********************************************************************************************************8
