*!* LIFEFACTORY 940 (EDI VERSION)
*!* (adapted from MORET940 proj), 06.01.2015

CLOSE DATABASES ALL
*!* Set and initialize public variables
PUBLIC xFile,archivefile,cXdate1,dXdate2,cOffice,lTesting,cUseDir,cCustName,nTotPT,cMailName,nSegCnt
PUBLIC cDelimiter,cTranslateOption,EmailCommentStr,LogCommentStr,nAcctNum,cAcctNum,lLoop,lDoPkg,nxptpts,nptpts
PUBLIC chgdate,ptid,ptctr,nPTQty,lTesting,lTestUploaddet,cOfficename,cConsignee,cStoreNum,cTotPT,cFilename
PUBLIC cPickticket_num_start,cPickticket_num_end,EmailCommentStr,cLoadID,lcPath,cUseFolder,cShip_ref,tfrom
PUBLIC tsendto,tcc,tsendtotest,tcctest,lcArchivepath,nXPTQty,NormalExit,cFilemask,cOutState,lBrowFiles,cMod,nFileSize,cISA_Num
PUBLIC cMessage,fa997file,m.printstuff,lTestImport,cTransfer,cCoCode,cCoNum,cDiv,lMix,tsendtoerr,tccerr,lEmail,lLoadSQL,gOffice
PUBLIC ARRAY a856(1)
SET EXCLUSIVE OFF

DO m:\dev\prg\_setvars WITH .T.

*SET STEP ON

TRY
	lTesting = .f.
	lTestFileIn = lTesting  && If .t., uses file(s) in subfolder "EDITEST"
	lTestImport = lTesting
	lTestUploaddet = lTesting
	lOverRideBusy = lTesting
	lBrowFiles = lTesting
	lEmail = .t.

*	lOverRideBusy = .T.
*	lBrowFiles = .F.

	CaptionStr = "LIFEFACTORY 940 (EDI VERSION)"
	STORE CaptionStr TO _SCREEN.CAPTION
	_SCREEN.WINDOWSTATE=IIF(lTesting OR lOverRideBusy,2,1)

	nAcctNum = 6034
	cOffice = "I"
	gOffice = cOffice
	cMod = cOffice
	lLoadSQL = .t.

	SELECT 0
	USE F:\edirouting\ftpsetup SHARED
	cTransfer = "940-LIFEFACTORYEDI-NJ"
	LOCATE FOR ftpsetup.transfer =  cTransfer
	IF FOUND()
		IF !lTesting AND !lOverRideBusy
			IF !ftpsetup.chkbusy
				REPLACE chkbusy WITH .T. FOR ftpsetup.transfer = cTransfer
			ELSE
				WAIT WINDOW "Transfer locked...can't start new process" TIMEOUT 2
				NormalExit = .T.
				THROW
			ENDIF
		ELSE
			REPLACE chkbusy WITH .T.,trig_time WITH DATETIME() For ftpsetup.transfer = cTransfer
		ENDIF
		USE IN ftpsetup
	ENDIF
	cFilename = ""
	STORE 0 TO nSegCnt,nxptpts,nptpts
	tfrom ="Toll EDI Operations <toll-edi-ops@tollgroup.com>"
	
	ASSERT .F. MESSAGE "In  mail data subroutine"
	SELECT 0
	USE F:\3pl\DATA\mailmaster ALIAS mm SHARED
	LOCATE FOR mm.edi_type = "940" AND mm.acctname = "LIFEFACTORY-EDI" AND mm.office = cOffice
	IF FOUND()
		STORE TRIM(mm.acctname) TO cAccountName
		STORE TRIM(mm.basepath) TO lcPath
		WAIT WINDOW "Path is: "+lcPath NOWAIT
		STORE TRIM(mm.archpath) TO lcArchivepath
		cFilemask = "*.*"
		STORE TRIM(IIF(mm.use_alt,mm.sendtoalt,mm.sendto)) TO tsendto
		STORE TRIM(IIF(mm.use_alt,mm.ccalt,mm.cc)) TO tcc
		LOCATE
		LOCATE FOR mm.office = "X" AND mm.accountid = 9999
		STORE TRIM(IIF(mm.use_alt,mm.sendtoalt,mm.sendto)) TO tsendtoerr
		STORE TRIM(IIF(mm.use_alt,mm.ccalt,mm.cc)) TO tccerr
		LOCATE
		LOCATE FOR mm.edi_type = "MISC" AND mm.taskname = "GENERAL"
		STORE TRIM(IIF(mm.use_alt,mm.sendtoalt,mm.sendto)) TO tsendtotest
		STORE TRIM(IIF(mm.use_alt,mm.ccalt,mm.cc)) TO tcctest
		USE IN mm
	ELSE
		WAIT WINDOW AT 10,10  "No parameters set for this acct# "+ALLTRIM(STR(nAcctNum))+"  ---> Office "+lcOffice TIMEOUT 2
		NormalExit = .T.
		THROW
	ENDIF

	IF lTesting
		CLOSE DATABASES ALL
		CLEAR
		WAIT WINDOW "This is a TEST upload into F:\WHP" TIMEOUT 2
	ENDIF

	IF lTesting OR lTestImport
		cUseFolder = "F:\WHP\WHDATA\"
	ELSE
		xReturn = "XXX"
		DO m:\dev\prg\wf_alt WITH cOffice,nAcctNum
		cUseFolder = UPPER(xReturn)
	ENDIF

	DO CASE
		CASE cOffice = "C"
			cOutState = "-SP"
			cOfficename = "San Pedro"
		CASE cOffice = "L"
			cOutState = "-ML"
			cOfficename = "Mira Loma"
		CASE cOffice = "M"
			cOutState = "-FL"
			cOfficename = "Florida"
		OTHERWISE
			cOutState = "-NJ"
			cOfficename = "New Jersey"
	ENDCASE

	STORE "" TO EmailCommentStr,cLoadID,cConsignee
	NormalExit = .F.
	cMessage = " "

	m.adddt = DATETIME()
	m.addby = "FMI-PROC"
	m.addproc = "LIFEEDI940"
	cCustName = "LIFEFACTORY"
	cMailName = PROPER(cCustName)
	cAlias = "LIFEFACTORY"
*!*		cDelimiter = "*"
*!*		cTranslateOption ="CR_TILDE"
	cUseDir = "m:\dev\prg\"+cCustName+"940edi_"

	xFile = ""
	cXdate1 = ""
	dXdate2 = DATE()

	cAcctNum = ALLTRIM(STR(nAcctNum))
	LogCommentStr = ""
	cShip_ref = ""

	IF lTesting
		tsendto = tsendtoerr
		tcc = tccerr
		lcPath = lcPath+"TEST\"
*	cFilemask = (lcPath+"*.*")
	ENDIF

	WAIT CLEAR

	DO (cUseDir+"PROCESS")
	CLOSE DATABASES ALL
	NormalExit = .T.

	IF !lTesting
		SELECT 0
		USE F:\edirouting\ftpsetup SHARED
		REPLACE chkbusy WITH .F. FOR ftpsetup.transfer = cTransfer
		USE IN ftpsetup
	ENDIF

CATCH TO oErr
	IF NormalExit = .F.
		ASSERT .F. MESSAGE "In Error Catch"
		SET STEP ON 
		tsendto = tsendtoerr
		tcc = tccerr
		tsubject = cMailName+" 940 Upload Error ("+TRANSFORM(oErr.ERRORNO)+") at "+TTOC(DATETIME())
		tattach  = ""
		tmessage = cMailName+" 940 Upload Error..... Please fix"
		tmessage = tmessage+CHR(13)+cMessage+CHR(13)+CHR(13)
		lcSourceMachine = SYS(0)
		lcSourceProgram = SYS(16)

		tmessage =tmessage+ "Try/CATCH Exeception Message:"+CHR(13)+;
			[  Error: ] + STR(oErr.ERRORNO) +CHR(13)+;
			[  LineNo: ] + STR(oErr.LINENO) +CHR(13)+;
			[  Message: ] + oErr.MESSAGE +CHR(13)+;
			[  Procedure: ] + oErr.PROCEDURE +CHR(13)+;
			[  Details: ] + oErr.DETAILS +CHR(13)+;
			[  StackLevel: ] + STR(oErr.STACKLEVEL) +CHR(13)+;
			[  LineContents: ] + oErr.LINECONTENTS+CHR(13)+;
			[  UserValue: ] + oErr.USERVALUE+CHR(13)+;
			[  Computer:  ] +lcSourceMachine+CHR(13)+;
			[  940 file:  ] +xFile+CHR(13)+;
			[  Program:   ] +lcSourceProgram

		IF "MISSING"$UPPER(cMessage)
		tcc = IIF(EMPTY(tcc),"chris.malcolm@tollgroup.com",tcc+";chris.malcolm@tollgroup.com")
		endif
		tattach  = ""

		DO FORM m:\dev\FRM\dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
		IF !lTesting
			COPY FILE &xFile TO &archivefile

			IF FILE(archivefile)
				DELETE FILE [&xFile]
			ENDIF
		ENDIF
	ELSE
		WAIT WINDOW AT 10,10 "Normal Exit " TIMEOUT 2
	ENDIF
FINALLY
	CLOSE DATABASES ALL
ENDTRY

