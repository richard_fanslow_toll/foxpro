* 6910 per diem expense report

utilsetup("6910RPT")

if holiday(date())
	return
endif

open data f:\watusi\platinum
use platinum!aplinh6910 alias aplinh nodata

tstartdt=gomonth(date(),-1)
tenddt=date()-1

*tstartdt={2/1}
*tenddt={2/28}

requery("aplinh")
go bottom in aplinh

select * from aplinh into cursor xrpt readwrite

delete for !inlist(substr(acct,9,2),"14","03","04")

index on vendorkey+vouchernumber tag vv

copy to h:\fox\perdiemexpense xls

tsendto="Joe.Damato@tollgroup.com,Mike.Drew@tollgroup.com,Tony.Branco@tollgroup.com,"
tsendto=tsendto+"thomas.keaveney@tollgroup.com,Steve.Zembryski@tollgroup.com,Nadine.Donovan@Tollgroup.com,Irena.Pagurek@tollgroup.com,Olga.Cruz@tollgroup.com,Michael.divirgilio@tollgroup.com"

*tsendto="Dyoung@fmiint.com"

tcc=""
tattach="h:\fox\perdiemexpense.xls"
tFrom="TGF EDI System Operations <fmi-transload-ops@fmiint.com>"
tmessage=""
tSubject="Per Diem Expenses for "+cmonth(tstartdt)+" "+transform(year(tstartdt))

Do Form dartmail2 With tsendto,tFrom,tSubject,tcc,tattach,tmessage,"A"

*

schedupdate()

_screen.Caption=gscreencaption
on error
