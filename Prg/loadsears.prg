* make exe in F:\UTIL\MLAUTOBILLING\
* 
*******************************************************************************************************
* Process SEARS pm data to produce a bill.
*
*******************************************************************************************************
LPARAMETERS tcMode

utilsetup("LOADSEARS")

LOCAL loSEARSProc

_screen.Caption = "Loading ML Auto Bill Data..."

SET PROCEDURE TO M:\DEV\PRG\SEARSCLASS

loSEARSProc = CREATEOBJECT('SEARS')

IF NOT EMPTY(tcMode) AND UPPER(ALLTRIM(tcMode)) = "AUTO" THEN
	loSEARSProc.SetAutoMode()
ENDIF

*loSEARSProc.SetClient("KMART FOOTWEAR")
loSEARSProc.SetClient("RACK ROOM FOOTWEAR")

* "RACK ROOM FOOTWEAR"

loSEARSProc.Load()

schedupdate()

RETURN
