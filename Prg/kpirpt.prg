**kpirpt.prg
**
**Automatic reporting via email
**
parameters creport, coffice, cbuilding, ctest, xaccountid
**cbuiling can contain a building # or MOD

if empty(creport)
	return
endif

**must match up parameter list in schedupdate() to update correct record - mvw 11/25/15
xparam='"'+transform(creport)+'"'+iif(empty(coffice),'',' "'+transform(coffice)+'"')+iif(empty(cbuilding),'',' "'+transform(cbuilding)+'"')+iif(empty(ctest),'',' "'+transform(ctest)+'"')+iif(empty(xaccountid),'',' "'+transform(xaccountid)+'"')

*do form dartmail2 with "mwinter@fmiint.com","TGFSYSTEM","KPIRPT Scheuled task started","","",xparam,"A"

wait "Kpirpt.exe started..." window nowait noclear

**removed account list vars from macros as they are created in utilsetup() with a call to lookups.prg - mvw 07/15/13
DO macros
DO set_envi

utilsetup("KPIRPT")

xsqlexec("select * from account","account",,"qq")
index on acctname tag acctname
index on accountid tag accountid
set order to

xdispoffice=""
if type("coffice")="C"
	xdispoffice=iif(inlist(coffice,"I","J","N"),"NJ",iif(coffice="L","ML",iif(coffice="M","FL",iif(inlist(coffice,"X","Y"),"CA","SP"))))
endif

cbuilding=iif(empty(cbuilding) or cbuilding=".f." or cbuilding="0", "", cbuilding)
ltest=iif(empty(ctest) or ctest=".f." or ctest="0", .f., .t.)
if type("xaccountid")#"N"
	xaccountid=iif(empty(xaccountid) or xaccountid=".f." or xaccountid="0", 0, val(xaccountid))
endif

**need to ignore xoffice in wf.prg for office "K" in order to do lookup by account to choose goffice "K" or "S" - mvw 09/03/15
xignorexoffice=.t.

xstep=1
wait "Kpirpt.exe running '"+creport+"'... Step "+transform(xstep) window nowait noclear

do case
case lower(creport)="coreobj"
	wait "Kpirpt.exe running... Step "+transform(xstep) window nowait noclear
	xinvenrpt=.t.
	if !empty(cbuilding)
		do autocoreobj with cbuilding, ltest
	endif

case lower(creport)="eodinven"
	do eodinven with coffice

case lower(creport)="whprod"
		**if before 4am ET, run for previous day
		xdate=iif(hour(datetime())<=4,date()-1,date())

		**run hourly for all warehouses - mvw 03/20/18
*		whprod(xdate,iif(hour(datetime())=4,"","inlist(office,'L','I','2','Y')"))
		whprod(xdate,"")

case lower(creport)="unpulledpt"
	xoffice=coffice
	lworksht=.t.
	xfilename="h:\fox\unpulledpt.xls"

	do ptrpt with xfilename
	use in account

	xto="oscarc@bioworldmerch.com,Maria.Estrella@Tollgroup.com"
	xcc="mwinter@fmiint.com"

	xAttach=xfilename
	xFrom="TGFSYSTEM"
	xSubject="Unallocated Picktickets"
	xMessage="Report Attached"
	do form dartmail2 with xto,xfrom,xsubject,xcc,xattach,xmessage,"A"

*case lower(creport)="avonrpt"		removed dy 11/14/15
*	**changed to a year-to-date report (starting 010/01 of the current year), but need dstart 
*	**  defined first for dend's definition - mvw 05/20/08
*	dstart = gomonth(ctod(left(dtoc(date()),2)+"/01/"+right(dtoc(date()),2)),-1)
*	dend = gomonth(dstart,1)-1
*	dstart = ctod("01/01/"+transform(year(dend)))
*	do avonrpt with dstart, dend
case lower(creport)="uaunshiprpt"
	xacctname="UNDER ARMOUR"
	do unshiprpt with 5687,"C",.f.

	cfile="h:\fox\uaunshiprpt.xls"

	select xtemp
	copy to &cfile type xls

	cto="dzuskin@underarmour.com"
	ccc="mwinter@fmiint.com"

	cAttach=cfile
	cFrom="TGFSYSTEM"
	cSubject="End of Month Stripped Unshipped Report: "+dtoc(date())
	cMessage=""
	DO FORM dartmail2 WITH cTo,cFrom,cSubject,cCc,cAttach,cMessage,"A"
case lower(creport)="uatransrpt"
	xacctname="UNDER ARMOUR"
	xfile="h:\fox\uatransrpt.xls"

	use f:\wo\wodata\manifest in 0
	**added undelivered to show in transit pos as well - mvw 08/18/11
*	select * from manifest where accountid=5687 and delivered>=date()-14 order by delivered into cursor xtemp
	select *, padr(getmemodata("suppdata","IBD",,.t.),15) as ibd from manifest where accountid=5687 and (delivered>=date()-14 or (wo_date>=date()-21 and delivered={} and complete)) order by delivered, wo_date into cursor xtemp
	copy fields wo_num, delivered, trailer, container, awb, hawb, po_num, style, quantity, qty_type, delloc, ibd to &xfile type xls

	use in manifest
	use in xtemp

	xto="tzorbach@underarmour.com, mwinter@fmiint.com"
	xcc=""

	xAttach=xfile
	xFrom="TGFSYSTEM"
	xSubject="Trasnload PO List Last 14 Days: "+dtoc(date())
	xMessage="Attached"
	DO FORM dartmail2 WITH xTo,xFrom,xSubject,xCc,xAttach,xMessage,"A"

case lower(creport)="uatransmonthly"
	xacctname="UNDER ARMOUR"
	xfile="h:\fox\uatransmonthly.xls"

	use f:\wo\wodata\manifest in 0

	xsqlexec("select * from dellocs",,,"wo")
	index on str(accountid,4)+location tag acct_loc
	set order to

	**need not only everything from manifest (originating from our facilities) but also express trips we picked up elsewhere
	xstartdt=ctod(transform(month(gomonth(date(),-1)))+"/01/"+transform(year(gomonth(date(),-1))))
	xenddt=gomonth(xstartdt,1)-1
	select sum(cbm) as totcbm, sum(quantity) as totctns, padr(iif(office="M","MIAMI, FL",iif(office="C","RIALTO, CA","CARTERET, NJ")),50) as origin, space(50) as destination, * from manifest ;
		where accountid=5687 and delivered>=xstartdt and delivered<=xenddt group by wo_num into cursor xtemp readwrite

	scan
		if seek(str(xtemp.accountid,4)+xtemp.delloc, "dellocs", "acct_loc")
			replace destination with alltrim(dellocs.city)+", "+alltrim(dellocs.state) in xtemp
		endif
	endscan

	xsqlexec("select * from fxwolog where accountid=5687 and between(wodeldt,{"+dtoc(xstartdt)+"},{"+dtoc(xenddt)+"})","xtemp2",,"fx")
	scan
		scatter fields acctname, wo_num, wo_date, trailer, origin, destination memvar

		select xtemp
		locate for wo_num=xtemp2.wo_num
		if !found()
			**in case delivered dt in manifest not filled in automatically at time of delivery from fxwo
			if seek(xtemp2.wo_num,"manifest","wo_num")
				select manifest
				sum quantity, cbm to m.totctns, m.totcbm for wo_num = xtemp2.wo_num
			endif

			select xtemp
			m.delivered=xtemp2.wodeldt
			append blank
			gather memvar
		endif
		replace origin with xtemp2.origin, destination with xtemp2.destination in xtemp
	endscan

	select * from xtemp order by delivered into cursor xtemp
	copy fields acctname, wo_num, wo_date, delivered, trailer, totctns, totcbm, origin, destination to &xfile type xls

	use in manifest
	use in dellocs
	use in xtemp
	use in xtemp2

	xto="tzorbach@underarmour.com, mwinter@fmiint.com"
	xcc=""

	xAttach=xfile
	xFrom="TGFSYSTEM"
	xSubject="Monthly Transload Report"+dtoc(date())
	xMessage="Attached"
	DO FORM dartmail2 WITH xTo,xFrom,xSubject,xCc,xAttach,xMessage,"A"

case lower(creport)="uahousecargo"
	xsqlexec("select * from offices",,,"wo")
	index on locale tag locale
	set order to

	**run for all 3 offices separately
	**changed to run for ALL offices at once - mvw 08/10/16
*!*		for xrpt = 1 to 3
		xaccountid=5687
		xacctname="UNDER ARMOUR"
*!*			xoffice=iif(xrpt=1,"C",iif(xrpt=2,"N","M"))
*!*			xfile="h:\fox\uahousecargo-"+xoffice+".xls"
		xoffice=""
		xfile="h:\fox\uahousecargo.xls"

		intlrpt(,,,.t.,,,,.t.,.t.)

		copy to &xfile type xls

		xto="AMcLendon@underarmour.com,PMcKeldin@underarmour.com,KGurski@underarmour.com,csichette@underarmour.com,logisticsops@underarmour.com,mwinter@fmiint.com,juan@fmiint.com,mdeters@underarmour.com"
		xcc=""
		xAttach=xfile
		xFrom="TGFSYSTEM"
*!*			xSubject="House Cargo Status - "+iif(xrpt=1,"San Pedro",iif(xrpt=2,"Carteret","Miami"))
		xSubject="House Cargo Status - All Offices"
		xMessage="Attached"
		DO FORM dartmail2 WITH xTo,xFrom,xSubject,xCc,xAttach,xMessage,"A"
*!*		endfor

	use in offices
	use in account

	xfile="h:\fox\uatransrpt.xls"
	use f:\wo\wodata\manifest in 0

	select *, padr(getmemodata("suppdata","IBD",,.t.),15) as ibd from manifest where accountid=5687 and wo_date>=date()-30 into cursor xtemp
	copy fields wo_num, office, wo_date, trailer, po_num, delloc, quantity, ibd to &xfile type xl5

*!*		select manifest
*!*		copy field wo_num, wo_date, office, trailer, po_num, delloc, quantity for accountid=5687 and wo_date>=date()-30 to &xfile type xl5
	use in manifest

	xSubject="Transload Report - All Offices"
	xAttach=xfile
	DO FORM dartmail2 WITH xTo,xFrom,xSubject,xCc,xAttach,xMessage,"A"


**creative recreation returns report
**added for synclaire - mvw 08/10/16
case inlist(lower(creport),"crreturns","synreturns")
	xfile="h:\fox\returnsrpt-"+strtran(dtoc(date()),"/","")+".xls"
	xacctlist=iif(lower(creport)="crreturns","6416,6417,6418","6521,6522")
	xfolder=wf("C",iif(lower(creport)="crreturns",6416,6521))
	goffice="2"	&& dy 1/8/17

	**changed ot grab consignee from returns as opposed to "padr(mline(w.printcomments,1),40," ")" - mvw 07/12/13
	**changed to pull retuns that were confirmed today, used to be confirmdt >= date()-14 - mvw 07/31/13
	**  must be run after midnight because will use date()-1
	**changed category to be pulled from brokerref vs acct_ref per Maria E - 08/06/13
	xdate=date()
*!*	useca('inwolog','wh',,,'select * from inwolog where accountid=6521 and zreturns=.t. and flag=.f.')
*!*	useca('indet','wh',,,'select * from indet where accountid=6521 and units=.t.')

	xsqlexec("select inwolog.wo_num, inwolog.accountid, brokerref, inwolog.wo_date, confirmdt, style, color, id, totqty, container " + ;
		"from inwolog, indet where inwolog.inwologid=indet.inwologid and inlist(inwolog.accountid,"+xacctlist+") " + ;
		"and zreturns=.t. and inwolog.flag=.f. and units=1 and inwolog.accountid in (6521,6522)","xdytemp",,"wh")

	useca('inwolog','wh',,,'select * from inwolog where accountid in (6521,6522) and confirmdt is not null and zreturns=.t. and flag=.f.')

	select distinct wo_num from xdytemp into cursor t1 readwrite 
	scan 
      select inwolog
      locate for inwolog.wo_num =t1.wo_num
      if found()
            replace flag with .t. in inwolog
            replace axdt with datetime() in inwolog
      endif  
	endscan
	tu('inwolog')

	select wo_num as wo, acctname(accountid) as acctname, space(15) as ranum, space(20) as ptnum, space(25) as po, space(31) as carrier, ;
		space(40) as comment1, space(40) as comment2, space(40) as comment3, space(35) as consignee,;
		brokerref as category, wo_date as rcvdt, ttod(confirmdt) as completedt, style, color, id as size, totqty ;
		from xdytemp  order by ptnum, confirmdt into cursor xrpt readwrite
	
	scan
		xsqlexec("select * from returns where wo_num="+transform(xrpt.wo),,,"wh")
		replace ranum with returns.ranum, ptnum with returns.ship_ref, po with returns.cnee_ref, carrier with returns.ship_via, ;
			consignee with returns.consignee, comment1 with mline(returns.comment,1), comment2 with mline(returns.comment,2), ;
			comment3 with mline(returns.comment,3);
		  in xrpt
		use in returns
	endscan

	copy to &xfile type xl5

	use in xrpt

	if lower(creport)="synreturns"
		copy file &xfile to f:\ftpusers\synclaire\returns\*.*
		copy file &xfile to f:\ftpusers\synclaire\returns\archive\*.*
	else
		select 0
		use f:\3pl\data\mailmaster
		locate for taskname=upper(creport)
		xto=alltrim(sendto)+iif(empty(sendtoalt),"",", "+alltrim(sendtoalt))
		xcc=alltrim(cc)+iif(empty(ccalt),"",", "+alltrim(ccalt))
		use in mailmaster

		xAttach=xfile
		xFrom="TGFSYSTEM"
		xSubject="Returns Report: "+dtoc(xdate)
		xMessage="Attached"
		DO FORM dartmail2 WITH xTo,xFrom,xSubject,xCc,xAttach,xMessage,"A"
	endif

case lower(creport)="ptcutfile"
	do ptcutfile
case lower(creport)="packdaily"
	do packdaily
case lower(creport)="guessrpt"
	do guessrpt
*!*	case lower(creport)="lprpt"		&& removed dy 1/17/17
*!*		do lprpt
*case lower(creport)="stylehist"	&& removed dy 11/17/16 was only used for Bugaboo
*	do stylehist with coffice, xaccountid

case lower(creport)="freetime"
	xfile="h:\fox\freetime-"+transform(xaccountid)+".xls"

	use f:\wo\wodata\wolog in 0
	select * from wolog ;
		where accountid=xaccountid and pickedup>=date()-60 and emptynul(returned) and office=coffice and type="O" and !loose ;
		order by pickedup into cursor xrpt
	xacctname=alltrim(acctname)
	copy fields wo_num, acctname, container, pickedup, returned to &xfile type xls

	use in wolog
	use in xrpt

	xto="mike.drew@tollgroup.com,mdivirgilio@fmiint.com,jdamato@fmiint.com,Anthony.sabatino@tollgroup.com,juan@fmiint.com"
	xcc="mwinter@fmiint.com"

	xAttach=xfile
	xFrom="TGFSYSTEM"
	xSubject="Free Time Allowance - Open Container List ("+xacctname+" - "+iif(coffice="C","CA",iif(coffice="M","FL","NJ"))+"): "+dtoc(date())
	xMessage="Report Attached"
	DO FORM dartmail2 WITH xTo,xFrom,xSubject,xCc,xAttach,xMessage,"A"

case lower(creport)="polo850"
	xfile="h:\fox\polo850.xls"

	use f:\ediexport\data\podata in 0
	select po_num, podate, acctname, importdt, padr(justfname(infile),50) as infile, padr(justfname(outfile),50) as outfile ;
		from podata where importdt>=date()-7 and acctname='POLO' into cursor xrpt
	copy to &xfile type xl5

	use in podata
	use in xrpt

	xto="keith.lee@tollgroup.com,bill.lillicrapp@tollgroup.com,janet.choi@tollgroup.com"
	xcc="mwinter@fmiint.com"

	xAttach=xfile
	xFrom="TGFSYSTEM"
	xSubject="Polo 850 Daily Report"
	xMessage="Report Attached"
	DO FORM dartmail2 WITH xTo,xFrom,xSubject,xCc,xAttach,xMessage,"A"

case lower(creport)="transtot"
	xfile="" &&set inside ctraging()

	**run for sun thru sat of previous week 
	xstartdt=date()-(7+(dow(date())-1))
	xenddt=xstartdt+6

	use f:\wo\wodata\wolog in 0
	use f:\wo\wodata\detail in 0
	use f:\wo\wodata\manifest in 0

	xfilecreated=transtot(xaccountid,coffice,xstartdt,xenddt)

	use in wolog
	use in detail
	use in manifest

	if xfilecreated
		xto="mike.drew@tollgroup.com,mdivirgilio@fmiint.com,jdamato@fmiint.com,rreyes@fmiint.com,Anthony.sabatino@tollgroup.com,juan@fmiint.com"
		xcc="mwinter@fmiint.com"

		xFrom="TGFSYSTEM"
		xSubject="Transload Equipment Totals ("+iif(coffice="C","CA",iif(coffice="M","FL","NJ"))+"): "+dtoc(xstartdt)+" - "+dtoc(xenddt)
		xMessage="Report Attached"
		DO FORM dartmail2 WITH xTo,xFrom,xSubject,xCc,xfile,xMessage,"A"
	endif

case lower(creport)="ctraging"
	xfile="" &&set inside ctraging()

	use f:\wo\wodata\wolog in 0
	
	xfilecreated=ctraging(xaccountid)

	use in wolog
	
	if xfilecreated
	*	xto="mike.drew@tollgroup.com,mdivirgilio@fmiint.com,jdamato@fmiint.com,rreyes@fmiint.com,Anthony.sabatino@tollgroup.com,juan@fmiint.com"
		xto="mwinter@fmiint.com"
		xcc="mwinter@fmiint.com"

		xFrom="TGFSYSTEM"
		xSubject="Container Aging Report"
		xMessage="Report Attached"
		DO FORM dartmail2 WITH xTo,xFrom,xSubject,xCc,xfile,xMessage,"A"
	endif

case inlist(lower(creport),"dailyappt","dailyshiprpt","dailywip")
	goffice=whbldg(coffice,xaccountid)

	xpdf=.f.
	do case
	case lower(creport)="dailyappt"
		**if run prior to 4PM, report today's appts. else report tomorrow's appts. now being run at 6am and 6pm - mvw 08/22/13
		xapptdt=iif(hour(datetime())<=16,date(),date()+1)

		**samsung (6652) needs different info and format (pdf)
		if xaccountid=6652
			xpdf=.t.
			xacctfilter="inlist(accountid,"+gSamsungaccounts+")"
			xSubject=iif(xapptdt=date(),"Today's Appointments","Tomorrow's Appointments")+"("+xdispoffice+") - Samsung Accounts"
			xacctname="Samsung Accounts"
			xheader=xsubject
			xtitle=""
			xpnpacct=.t.

			xquery="select * from outship where "+xacctfilter+" and appt={"+transform(xapptdt)+"} and del_date={} and mod = '"+goffice+"' order by ship_ref"
			xsqlexec(xquery,"xrpt",,"wh",,,,,,,.t.)
			
			select *, alltrim(acctname(xaccountid)) as acctname, space(20) as procstatus from xrpt order by ship_ref into cursor xrpt readwrite

			replace all procstatus with iif(!pulled,"NOT PULLED",iif(!emptynul(del_date),"DELIVERED",iif(!emptynul(truckloaddt),"LOADED",iif(!emptynul(staged),;
				"STAGED",iif(!emptynul(labeled),"LABELED",iif(!emptynul(picked),"PICKED","PULLED"))))))
			locate
		else
			**for moder shoe (4752) include both MS accounts - mvw 06/09/16
			xacctfilter=iif(xaccountid=4752,"inlist(accountid,4752,5318)","accountid="+transform(xaccountid))

			xSubject=iif(xapptdt=date(),"Today's Appointments","Tomorrow's Appointments")+"("+xdispoffice+") - "+alltrim(acctname(xaccountid))

			xquery="select bol_no, appt, appt_num, wo_num, ship_ref, consignee, ctnqty, qty, weight, dcnum, sforstore, cnee_ref, routedfor, ship_via "+;
				"from outship where "+xacctfilter+" and appt={"+transform(xapptdt)+"} and del_date={} and mod = '"+goffice+"' order by bol_no"
			xsqlexec(xquery,"xrpt",,"wh",,,,,,,.t.)
		endif

	case lower(creport)="dailywip"
		xSubject="End Of Day WIP ("+xdispoffice+") - "+alltrim(acctname(xaccountid))

		xquery="select * from outship where accountid="+transform(xaccountid)+" and del_date={} and notonwip=0 and mod = '"+goffice+"' order by ship_ref"
		xsqlexec(xquery,"xrpt",,"wh",,,,,,,.t.)

		select alltrim(acctname(xaccountid)) as acctname, ship_ref as pt, ptdate, wo_num as wo, wo_date as wodate, bol_no, consignee, ctnqty, qty, ;
			weight, cuft, called, appt, appt_num, apptremarks, apptstatus, dcnum, shipfor, sforstore, cnee_ref, routedfor, ship_via, dept, start, cancel, picked, labeled, staged, pulleddt, ;
			name, address, address2, csz ;
			from xrpt into cursor xrpt

	otherwise
		xSubject="Yesterday's Shipments ("+xdispoffice+") - "+alltrim(acctname(xaccountid))

		xquery="select bol_no, del_date, appt, appt_num, wo_num, ship_ref, consignee, ctnqty, qty, weight, dcnum, sforstore, cnee_ref, routedfor, ship_via "+;
			"from outship where accountid="+transform(xaccountid)+" and del_date={"+transform(date()-1)+"} and mod = '"+goffice+"' order by bol_no"
		xsqlexec(xquery,"xrpt",,"wh",,,,,,,.t.)
	endcase

	if !eof()
		if xpdf
			xfile="h:\pdf\"+alltrim(lower(creport))+"-"+transform(xaccountid)+".pdf"
			rpt2pdf("kpiappt",xfile)
		else
			xfile="h:\pdf\"+alltrim(lower(creport))+"-"+transform(xaccountid)+".xls"
			copy to &xfile xls
		endif

		select 0
		use f:\3pl\data\mailmaster
		locate for accountid=xaccountid and taskname=alltrim(upper(creport))
		xto=alltrim(sendto)+iif(empty(sendtoalt),"",", "+alltrim(sendtoalt))
		xcc=alltrim(cc)+iif(empty(ccalt),"",", "+alltrim(ccalt))
		use in mailmaster

		xattach=xfile
		xfrom="TGFSYSTEM"
		xmessage = "See attached file"

		xto=strtran(xto,",",";") &&change to semicolon for email.prg
		xcc=strtran(xcc,",",";") &&change to semicolon for email.prg
		emailx(xto,xcc,xsubject,xmessage,.f.,xattach)
	else
		do form dartmail2 with "mwinter@fmiint.com","TGFSYSTEM",xsubject,"","","No data found for "+transform(xaccountid),"A"
*		email("mwinter@fmiint.com",xsubject,"No data found for "+transform(xaccountid),,,,.t.)	
	endif

	use in xrpt

case lower(creport)="kpioctmj"
	**Marc Jacobs Custom Order Cycle Time
	_screen.Caption = "Mar Jacobs Custom Order Cycle Time"
	set sysmenu off

	use f:\hr\hrdata\holidays in 0
	use f:\kpi\data\emailaddr in 0

	select emailaddr
	locate for report = "KPIOCTMJ"
	cemaillist = emailaddr.toaddrs
	ccclist = emailaddr.ccaddrs

	**run for units
	xunits=.t.

	if adir(xx,"s:\marcjacobsdata\reports\kpi\*.*")>0
		copy files s:\marcjacobsdata\reports\kpi\*.* to s:\marcjacobsdata\reports\kpi\archive\*.*
		delete files s:\marcjacobsdata\reports\kpi\*.*
	endif
	release xx

	xinvenrpt=.t.
	xnoalert=.t. &&do not display messageboxes

	xrpttype="WIP"
	DO kpi.fxp with "kpioctmj", "", "All Accounts", "N", date(), date(), .t., cemaillist, ccclist, "J"

	**run for multiple date ranges
	**previous day (current day if run before midnite)
	xrpttype="PREV_DAY"
	DO kpi.fxp with "kpioctmj", "", "All Accounts", "N", date(), date(), .t., cemaillist, ccclist, "J"

	**previous week
	xrpttype="PREV_WEEK"
	xstartdt=(date()-(dow(date()-1)))-7 &&find date of previous sunday, then go back 7 days for the previous sunday
	DO kpi.fxp with "kpioctmj", "", "All Accounts", "N", xstartdt, xstartdt+7, .f., cemaillist, ccclist, "J"

	**previous month
	xrpttype="PREV_MONTH"
	xstartdt=gomonth(ctod(transform(month(date()))+"/1/"+transform(year(date()))),-1) &&1st day of last month
	DO kpi.fxp with "kpioctmj", "", "All Accounts", "N", xstartdt, gomonth(xstartdt,1)-1, .f., cemaillist, ccclist, "J"

	**W-T-D
	xrpttype="WTD"
	xstartdt=(date()-(dow(date()-1))) &&find date of previous sunday
	DO kpi.fxp with "kpioctmj", "", "All Accounts", "N", xstartdt, date(), .f., cemaillist, ccclist, "J"

	**M-T-D
	xrpttype="MTD"
	xstartdt=ctod(transform(month(date()))+"/1/"+transform(year(date()))) &&1st day of month
	DO kpi.fxp with "kpioctmj", "", "All Accounts", "N", xstartdt, date(), .f., cemaillist, ccclist, "J"

	**Y-T-D
	xrpttype="YTD"
	xstartdt=ctod("1/1/"+transform(year(date()))) &&1st day of year
	DO kpi.fxp with "kpioctmj", "", "All Accounts", "N", xstartdt, date(), .f., cemaillist, ccclist, "J"

	use in account
	use in holidays
	use in emailaddr

	wait clear

case lower(creport)="accountactivity"
	xsqlexec("select * from offices",,,"wo")
	index on locale tag locale
	set order to

	do case
	case lower(creport)="accountactivity-sears"
		xaccountid=5837
		xoffice="C"
		xsubject="Sears Account Activity Report"
	case lower(creport)="accountactivity-bcny"
		xaccountid=6221
		xoffice="C"
		xsubject="BCNY Account Activity Report"
	case lower(creport)="accountactivity-age"
		xaccountid=1285
		xoffice="C"
		xsubject="Age Group Account Activity Report"
	case lower(creport)="accountactivity-sun"
		xaccountid=4484
		xoffice="C"
		xsubject="Sun Express Account Activity Report"
	case lower(creport)="accountactivity-hlu"
		xaccountid=5864
		xoffice="C"
		xsubject="Highline United Account Activity Report"
	case lower(creport)="accountactivity-2xu"
		xaccountid=6665
		xoffice="C"
		xsubject="2XU Account Activity Report"
	case lower(creport)="accountactivity-ua"
		xaccountid=5687
		xoffice="ALL"
		xsubject="UA Account Activity Report"
	otherwise
		return
	endcase

	latest=.t.
	xenddt=date()
	xstartdt=date()-90
	lworksht=.t.
	cfrttype="ALL"

	xfile=""
	do oceanrpt with .f.,lworksht,,,,.t.
	use in offices

	select 0
	use f:\3pl\data\mailmaster
	locate for accountid=xaccountid and taskname=alltrim(upper(left(creport,15)))
	xto=alltrim(sendto)+iif(empty(sendtoalt),"",", "+alltrim(sendtoalt))
	xcc=alltrim(cc)+iif(empty(ccalt),"",", "+alltrim(ccalt))
	use in mailmaster

	xfrom="TGFSYSTEM"
	xto=strtran(xto,",",";") &&change to semicolon for email.prg
	xcc=strtran(xcc,",",";") &&change to semicolon for email.prg

	if !empty(xfile)
		xmessage="Report Attached"
		*DO FORM dartmail2 WITH xTo,xfrom,xsubject,xCc,xfile,xmessage,"A"
		emailx(xto,xcc,xsubject,xmessage,,xfile)
	else
		xmessage="No activity to report in the last 8 days."
		*DO FORM dartmail2 WITH xTo,xfrom,xsubject,xCc,,xmessage,"A"
		emailx(xto,xcc,xsubject,xmessage)
	endif

case lower(creport)="spsumm-bioworld"
	xinvstartdt=gomonth(ctod(transform(month(date()))+"/1/"+transform(year(date()))),-1)
	xinvenddt=ctod(transform(month(date()))+"/1/"+transform(year(date())))-1
	xaccountid=6182
	xoffice='5'
	gmodfilter="office='C'"		&& dy 12/20/17
	xfilter="project.accountid="+trans(xaccountid)

	**this report is also run internally through the system in which case there are a few fileds that need to be shown that would not be shown for the customer, ie. hours - mvw 07/08/16
	**xshowall var is used within the report form
	xshowall=.f.

	xheader="" &&defined within spsumm()
	spsumm(xoffice,xfilter,xinvstartdt,xinvenddt)

	**changed to excel format - mvw 07/12/16
*!*		xfile="h:\fox\spsumm-"+transform(xaccountid)+".pdf"
*!*		rpt2pdf("specproj",xfile)

	select projno, wo_num, completeddt, invnum, invdt, accountid, acctname, custrefno, consignee, actrev as invamt, unitqty ;
		from xrpt into cursor xrpt

	xfile="h:\fox\spsumm-"+transform(xaccountid)+".xls"
	copy to &xfile type xl5

	xto="Oscarc@bioworld.com,Maria.Estrella@Tollgroup.com,maria.rodriguez@tollgroup.com,Daniel.montoya@tollgroup.com,esthero@bioworldmerch.com,rayb@bioworldmerch.com"
	xcc="mwinter@fmiint.com"
	DO FORM dartmail2 WITH xTo,"TGFSYSTEM","Special Projects - "+transform(xinvstartdt)+ " to "+transform(xinvenddt),xCc,xfile,"Report Attached","A"

	use in xrpt
	if used("invoice")
		use in invoice
		use in invdet
	endif
endcase

xstep=xstep+1
wait "Kpirpt.exe running... Step "+transform(xstep) window nowait noclear

schedupdate(xparam)
