*!*  Steel Series EDI 945 (Whse. Shipping Advice) Creation Program
*!* Creation Date: 08.06.2015 by Joe (Derived from Lifefactory945edi_create program)

PARAMETERS cBOL,cOffice
WAIT WINDOW "Now at start of Steel Series EDI 945 process" NOWAIT
*SET STEP ON 

PUBLIC ARRAY thisarray(1)
PUBLIC c_CntrlNum,c_GrpCntrlNum,cReturned,cWO_Num,cWO_NumStr,nWO_Num1,lTesting,lDoUPC,nAcctNum,cUsefolder
PUBLIC lEmail,lSteelFilesOut,dDateTimeCal,nFilenum,nWO_Xcheck,nOutshipid,cCharge,lDoSQL,dapptnum,tfrom,cMailName,lOverflow
PUBLIC cCity,cState,cZip,nOrigSeq,nOrigGrpSeq,tsendto,tcc,tsendtoerr,tccerr,cISA_Num,cErrMsg,lAmazon,cCustname,dDaysback
PUBLIC cMod,cMBOL,cEDIType,lParcelType,cFilenameShort,cFilenameOut,cFilenameArch,lcPath,cWO_NumList,cISA_Num,lScanPack

lTesting   = .f.  && Set to .t. for testing
lTestinput = .F.  &&  Set to .t. for test input files only!

IF lTesting
	CLOSE DATABASES ALL
	DO m:\dev\prg\lookups
ENDIF

DO m:\dev\prg\_setvars WITH lTesting

TRY
	lDoSQL = .T.
	lPick = .F.
	lPrepack = .F.
	lJCP = .F.
	lIsError = .F.
	lDoCatch = .T.
	cShip_ref = ""
	lCloseOutput = .T.
	nFilenum = 0
	cCharge = "0.00"
	cWO_NumStr = ""
	cWO_NumList = ""
	cString = ""
	cErrMsg = ""
	nPTCtnTot = 0
	dDaysback = 3  && Default value...DON'T CHANGE THIS
	dDaysback = 10
	
	tempdt = DATETIME(2017,08,28,19,00,00)
	
	cMBOL = ""
	cEDIType = "945"
	lParcelType = .F.
	nAcctNum = 6612

	lFederated = .F.
	nOrigSeq = 0
	nOrigGrpSeq = 0
	cPPName = "STEELSERIES"
	cMailName = "Steel Series"
	WAIT WINDOW "At the start of "+cMailName+" 945 process..." NOWAIT
	tfrom = "TGF EDI Operations <toll-edi-ops@tollgroup.com>"
	tmessage=""
	tcc = ""
	lDoBOLGEN = .F.
	lSamples = .F.
	cBOL1 = ""
	lISAFlag = .T.
	lSTFlag = .T.
	nLoadid = 1
	lSQLMail = .F.
	lOverflow = .f.

	IF VARTYPE(cBOL) = "L"
		IF !lTesting AND !lTestinput
			WAIT WINDOW "BOL# not provided...terminating" TIMEOUT 2
			lCloseOutput = .F.
			cErrMsg = "NO BOL"
			DO ediupdate WITH cErrMsg,.T.
			THROW
		ELSE
			CLOSE DATABASES ALL
			IF !USED('edi_trigger')
				cEDIFolder = "F:\3PL\DATA\"
				USE (cEDIFolder+"edi_trigger") IN 0 ALIAS edi_trigger
			ENDIF
*!* TEST DATA AREA
			cBOL = "748037115246"
			cOffice = "Y"
			cTime = DATETIME()
		ENDIF
	ENDIF
	CREATE CURSOR temp945 (accountid i,isa_num c(10),st_num c(10),wo_num i,bol_no c(20),ship_ref c(20),filename c(50),filedate T)
	cOffice = IIF(cOffice = "S","K",cOffice)
	cMod = IIF(cOffice  = "K","S","Y")	&& Mod changeover
	goffice = cMod
	gMasterOffice = cOffice
	cBOL=TRIM(cBOL)

	IF lTestinput
		cUsefolder = "F:\WHP\WHDATA\"
	ELSE
		cUsefolder = "F:\WHY\WHDATA\"
	ENDIF

	IF USED('OUTSHIP')
		USE IN outship
	ENDIF
		csq1 = [select * from outship where accountid = ]+TRANSFORM(nAcctNum)+[ and office = ']+cOffice+[' and bol_no = ']+cBOL+['] && and del_date > {]+DTOC(DATE()-dDaysback)+[}]
		xsqlexec(csq1,,,"wh")
		IF RECCOUNT() = 0
			cErrMsg = "MISS BOL "+cBOL
			DO ediupdate WITH cErrMsg,.T.
			THROW
		ENDIF
		INDEX ON bol_no TAG bol_no

	= SEEK(cBOL,'outship','bol_no')
	nWO_Num = outship.wo_num
	cSCAC = ALLTRIM(outship.scac)

	lScanPack = IIF("PROCESSMODE*SCANPACK"$outship.shipins,.t.,.f.)

	IF USED('parcel_carriers')
		USE IN parcel_carriers
	ENDIF
	USE F:\3pl\DATA\parcel_carriers IN 0 ALIAS parcel_carriers

	lParcelType = IIF(SEEK(cSCAC,'parcel_carriers','scac'),.T.,.F.)
	lParcelType = IIF(cBOL = ALLTRIM(outship.keyrec),.F.,lParcelType)
	lParcelType = IIF(nWO_Num = 7744226,.F.,lParcelType)
	lParcelType = IIF(DATETIME() <DATETIME(2018,02,13,11,30,00),.F.,lParcelType)

	IF lTesting=.F. AND lParcelType=.T.
		IF !USED('steel_wohold')
			USE F:\3pl\DATA\steel_wohold IN 0
		ENDIF
		nWO_numOld = steel_wohold.wo_num
		IF nWO_Num = nWO_numOld
			lDoSQL = .F.
		ELSE
			REPLACE wo_num WITH nWO_Num IN steel_wohold
		ENDIF
		USE IN steel_wohold
	ENDIF

	lEmail = .T.
	lTestMail = lTesting && Sends mail to Joe only
	lSteelFilesOut = !lTesting && If true, copies output to FTP folder (default = .t.)
	lStandalone = lTesting

*lTestMail = .T.
*lSteelFilesOut = .F.

	STORE "LB" TO cWeightUnit
	cfd = "*"
	csegd = "~"
	nSegCtr = 0
	cterminator = ">" && Used at end of ISA segment

	WAIT WINDOW "Now preparing tables and variables" NOWAIT NOCLEAR

*!* SET CUSTOMER CONSTANTS
	cCustname = "STEELSERIES"  && Customer Identifier
	cX12 = "004010"  && X12 Standards Set used
	
	IF USED("mm")
	USE IN mm
	ENDIF
	IF USED("mailmaster")
	USE IN mailmaster
	endif
	SELECT 0
	USE F:\3pl\DATA\mailmaster ALIAS mm
	LOCATE FOR mm.edi_type = "945" AND mm.office = cOffice AND mm.accountid = nAcctNum
	tsendto = IIF(mm.use_alt,sendtoalt,sendto)
	tcc = IIF(mm.use_alt,ccalt,cc)
	cOutPath = IIF(lTesting,STRTRAN(ALLTRIM(mm.basepath),"Prod","UAT"),ALLTRIM(mm.basepath))
	cHoldPath = IIF(lTesting,STRTRAN(ALLTRIM(mm.holdpath),"Prod","UAT"),ALLTRIM(mm.holdpath))
	cArchivePath = IIF(lTesting,STRTRAN(ALLTRIM(mm.archpath),"Prod","UAT"),ALLTRIM(mm.archpath))
	LOCATE
	LOCATE FOR edi_type = "MISC" AND taskname = "GENERAL"
	tsendtoerr = IIF(mm.use_alt,sendtoalt,sendto)
	tccerr = IIF(mm.use_alt,ccalt,cc)
	tsendtotest = IIF(mm.use_alt,sendtoalt,sendto)
	tcctest = IIF(mm.use_alt,ccalt,cc)

	USE IN mm

*!* SET OTHER CONSTANTS
	DO CASE
		CASE cOffice = "L"
			cCustLoc =  "CA"
			cCustPrefix = "945l"
			cDivision = "Mira Loma"
			cSF_Addr1  = "3355 DULLES DR"
			cSF_CSZ    = "MIRA LOMA"+cfd+"CA"+cfd+"91752"

		CASE cOffice = "Y"
			cCustLoc =  "C2"
			cCustPrefix = "945r"
			cDivision = "Carson 2"
			cSF_Addr1  = "1000 E 223RD ST"
			cSF_CSZ    = "CARSON"+cfd+"CA"+cfd+"90745"

		CASE cOffice = "C"
			cCustLoc =  "CA"
			cCustPrefix = "945c"
			cDivision = "California"
			cSF_Addr1  = "400 WESTMONT DRIVE"
			cSF_CSZ    = "SAN PEDRO"+cfd+"CA"+cfd+"90731"

		CASE cOffice = "I"
			cCustLoc =  "NJ"
			cCustPrefix = "945j"
			cDivision = "New Jersey"
			cSF_Addr1  = "800 FEDERAL BLVD"
			cSF_CSZ    =  "CARTERET"+cfd+"NJ"+cfd+"07008"

		OTHERWISE
			cCustLoc =  "KY"
			cCustPrefix = "945s"
			cDivision = "Kentucky"
			cSF_Addr1  = "7501 WINSTEAD DR"
			cSF_CSZ    =  "LOUISVILLE"+cfd+"KY"+cfd+"40258"
	ENDCASE
	IF lTesting
		cCustPrefix = "945t"
	ENDIF

	cCustFolder = UPPER(cCustname)

	STORE "" TO lcKey
	STORE 0 TO alength,nLength
	cTime = DATETIME()
	cDelTime = SUBSTR(TTOC(cTime,1),9,4)
	cCarrierType = "M" && Default as "Motor Freight"
	dapptnum = ""

*!* SET OTHER CONSTANTS
	IF INLIST(cOffice,'Y','C','1','2')
		dDateTimeCal = (DATETIME()-(3*3600))
	ELSE
		dDateTimeCal = DATETIME()
	ENDIF
	dt1 = TTOC(dDateTimeCal,1)
	dtmail = TTOC(dDateTimeCal)
	dt2 = dDateTimeCal
	cString = ""

	csendqual = "ZZ"
	csendid = "TGFPROD940"
	crecqual = "ZZ"
	crecid = "STEELSR"

	cdate = DTOS(DATE())
	cTruncDate = RIGHT(cdate,6)
	cTruncTime = SUBSTR(TTOC(dDateTimeCal,1),9,4)
	cfiledate = cdate+cTruncTime
	cOrig = "J"
	nSTCount = 0
	cStyle = ""
	cPTString = ""
	nTotCtnWt = 0
	nTotCtnCount = 0
	nUnitSum = 0
	cTargetStyle = ""

** PAD ID Codes
	csendidlong = PADR(csendid,15," ")
	crecidlong = PADR(crecid,15," ")
	lcPath = cOutPath
	cFilenameHold = (cHoldPath+cCustPrefix+dt1+".txt")
	cFilenameShort = JUSTFNAME(cFilenameHold)
	cFilenameOut = (cOutPath+cFilenameShort)
	cFilenameArch = (cArchivePath+cFilenameShort)
	nFilenum = FCREATE(cFilenameHold)

	SELECT outship
	IF !SEEK(cBOL,'outship','bol_no')
		WAIT WINDOW "BOL not found in OUTSHIP" TIMEOUT 2
		cErrMsg = "BOL NOT FOUND-OUTSHIP"
		DO ediupdate WITH cErrMsg,.T.
		THROW
	ENDIF

	DO m:\dev\prg\swc_cutctns WITH cBOL

	SELECT outshipid ;
		FROM outship ;
		WHERE bol_no = cBOL ;
		AND accountid = nAcctNum ;
		AND del_date > DATE()-dDaysback ;
		INTO CURSOR tempsr
	SELECT tempsr
	WAIT WINDOW "There are "+ALLTRIM(STR(RECCOUNT()))+" outshipids to check" NOWAIT

	IF USED('OUTDET')
		USE IN outdet
	ENDIF
		selectoutdet()
		SELECT outdet
		INDEX ON outdetid TAG outdetid

		IF !USED('CTNUCC')
			USE (cUsefolder+"CTNUCC") IN 0 NOUPDATE ALIAS ctnucc
			SELECT ctnucc
			LOCATE
		ENDIF

	IF !USED("scacs")
		xsqlexec("select * from scac","scacs",,"wh")
		INDEX ON scac TAG scac
	ENDIF

	dDate = DATE()-dDaysback
	IF !lTestinput AND lParcelType
		IF USED('SHIPMENT')
			USE IN shipment
		ENDIF

		IF USED('PACKAGE')
			USE IN package
		ENDIF

*!*			csq1 = [select * from shipment where right(rtrim(str(accountid)),4) = ]+TRANSFORM(nAcctNum)+[ and shipdate >= {]+DTOC(dDate)+[}]
*!*			xsqlexec(csq1,,,"wh")

		xsqlexec("select * from shipment where 1=0",,,"wh")  && Sets up an empty cursor
		SELECT outship && temp cursor with all PTs on the BOL
		SCAN
			cSR = ALLTRIM(outship.ship_ref)
			xsqlexec("select * from shipment where right(rtrim(str(accountid)),4) ="+TRANSFORM(nAcctNum)+" and pickticket = '"+cSR+"'","sm1",,"wh")
			SELECT sm1
			LOCATE
			IF RECCOUNT()=0 AND lParcelType
				cErrMsg = "NO SHIPMENT DATA"
				THROW
			ENDIF

			SELECT shipment
			APPEND FROM DBF('sm1')
			USE IN sm1
		ENDSCAN
		RELEASE cSR
		
		SELECT shipment
		INDEX ON shipmentid TAG shipmentid
		INDEX ON pickticket TAG pickticket

		LOCATE
		IF RECCOUNT("shipment") > 0
			xjfilter="shipmentid in ("
			SCAN
				nShipmentId = shipment.shipmentid
				xsqlexec("select * from package where  right(rtrim(str(accountid)),4) = "+TRANSFORM(nAcctNum)+"and shipmentid="+TRANSFORM(nShipmentId),,,"wh")

				IF RECCOUNT("package")>0
					xjfilter=xjfilter+TRANSFORM(shipmentid)+","
				ELSE
					xjfilter="1=0"
				ENDIF
			ENDSCAN
			xjfilter=LEFT(xjfilter,LEN(xjfilter)-1)+")"

			xsqlexec("select * from package where "+xjfilter,,,"wh")
		ELSE
			xsqlexec("select * from package where .f.",,,"wh")
		ENDIF
		SELECT package

		INDEX ON shipmentid TAG shipmentid
		SET ORDER TO TAG shipmentid

		SELECT shipment
		SET RELATION TO shipmentid INTO package
	ENDIF

&& Added code blocks to the following to overcome BOL vs. WO selection issues, 12.23.2005

	lDoSQL = .T.
	IF lDoSQL
		cSQL="tgfnjsql01"		&& dy 2/18/18
		SELECT outship
		LOCATE
*!* Had to change this after Scanpack was entered in a different Server database (SQL4) than the Pnp (SP3)
		IF USED("sqlwopt")
			USE IN sqlwopt
		ENDIF
		IF FILE("F:\3pl\DATA\sqlwopt.dbf")
			DELETE FILE "F:\3pl\DATA\sqlwopt.dbf"
		ENDIF

		SELECT bol_no,ship_ref,IIF("PROCESSMODE*SCANPACK"$outship.shipins,.t.,.f.) as scanpack ;
			FROM outship ;
			WHERE bol_no = cBOL ;
			AND accountid = nAcctNum ;
			AND del_date > ddate ;
			GROUP BY 1,2 ;
			ORDER BY 1,2 ;
			INTO DBF F:\3pl\DATA\sqlwopt
		USE IN sqlwopt
		SELECT 0
		USE F:\3pl\DATA\sqlwopt ALIAS sqlwopt
		LOCATE

		IF EOF()
			cErrMsg = "NO SQL DATA"
			DO ediupdate WITH cErrMsg,.T.
			THROW
		ENDIF
		cRetMsg = ""
		
		**DHW
		
*		SET STEP ON 
		
		DO m:\dev\prg\sqlconnect_pt  WITH nAcctNum,cCustname,cPPName,.T.,cOffice,.T.
		IF cRetMsg<>"OK"
			STORE cRetMsg TO cErrMsg
			DO ediupdate WITH cErrMsg,.T.
			THROW
		ENDIF
		
		SELECT vsteelseriespp
		LOCATE
		IF EOF()
			WAIT WINDOW "SQL select data is EMPTY...error!" NOWAIT
			cErrMsg = "SQL DATA EMPTY"
			DO ediupdate WITH cErrMsg,.T.
			THROW
		ENDIF
	ENDIF

	SELECT outship
	LOCATE
	WAIT CLEAR
	WAIT WINDOW "Now creating Header information" NOWAIT NOCLEAR

*!* HEADER LEVEL EDI DATA
	DO num_incr_isa

	cISA_Num = PADL(c_CntrlNum,9,"0")
	nISA_Num = INT(VAL(cISA_Num))

	cISACode = IIF(lTesting,"T","P")

	STORE "ISA"+cfd+"00"+cfd+"          "+cfd+"00"+cfd+"          "+cfd+csendqual+cfd+csendidlong+cfd+;
		crecqual+cfd+crecidlong+cfd+cTruncDate+cfd+cTruncTime+cfd+"U"+cfd+"00401"+cfd+;
		cISA_Num+cfd+"0"+cfd+cISACode+cfd+cterminator+csegd TO cString
	DO cstringbreak

	STORE "GS"+cfd+"SW"+cfd+csendid+cfd+crecid+cfd+cdate+cfd+cTruncTime+cfd+c_CntrlNum+;
		cfd+"X"+cfd+cX12+csegd TO cString
	DO cstringbreak

	SELECT outship
	LOCATE

*************************************************************************
*1	PTIDSOURCE/OUTSHIP RECORD MAIN LOOP
*	Loops through individual OUTSHIP lines for BOL#
*************************************************************************
	WAIT CLEAR
	WAIT WINDOW "Now creating BOL#-based information..." NOWAIT NOCLEAR

	SELECT outship
	SET ORDER TO
	COUNT TO N FOR outship.bol_no = cBOL AND !EMPTYnul(del_date)
	IF N=0
		cErrMsg = "INCOMP BOL"
		DO ediupdate WITH cErrMsg,.T.
		THROW
	ENDIF

	LOCATE
	cMissDel = ""
	nPTCount = 0

	oscanstr = "outship.bol_no = cBOL AND !EMPTYnul(outship.del_date) and outship.del_date > DATE() - dDaysback"

	SELECT outship
	LOCATE FOR &oscanstr
	SCAN FOR &oscanstr
		nWO_Num = outship.wo_num
		cWO_Num = ALLTRIM(STR(nWO_Num))
		alength = ALINES(apt,outship.shipins,.T.,CHR(13))
		SCATTER MEMVAR MEMO
		cShip_ref = ALLTRIM(m.ship_ref)
		IF "OV"$cShip_ref
			LOOP
		ENDIF
		nPTCount = nPTCount + 1
		IF lParcelType
			SELECT shipment
			IF SEEK(cShip_ref,"shipment","pickticket")
				SELECT shipmentid,trknumber,ucc,ref4 FROM package WHERE package.shipmentid=shipment.shipmentid INTO CURSOR package1
			ENDIF
		ENDIF

		lJCP = IIF("PENNEY"$UPPER(m.consignee) OR UPPER(m.consignee)="JCP",.T.,.F.)
		lAmazon  = IIF("AMAZON"$UPPER(m.consignee) OR UPPER(m.consignee)="AMAZON",.T.,.F.)

		nOutshipid = m.outshipid
		nWO_Num = m.wo_num
		cWO_Num = ALLTRIM(STR(m.wo_num))
		cPackType = ALLTRIM(segmentget(@apt,"PROCESSMODE",alength))

		lPrepack = IIF(cPackType#"PICKPACK",.T.,.F.)

		lPrepack = .F.

		IF !(cWO_Num$cWO_NumList)
			cWO_NumList = IIF(EMPTY(cWO_NumList),cWO_Num,cWO_NumList+CHR(13)+cWO_Num)
		ENDIF
		IF !lParcelType
			cTrackNum = ALLT(m.keyrec)  && ProNum if available
			cCharge = "0.00"
			IF !(cWO_Num$cWO_NumStr)
				cWO_NumStr = IIF(EMPTY(cWO_NumStr),cWO_Num,cWO_NumStr+","+cWO_Num)
			ENDIF
		ELSE
*			cBOL = PADL(ALLT(STR(outship.wo_num)),17,'0')
			cTrackNum = ALLT(outship.bol_no)  && UPS Tracking Number
			cWO_NumStr = IIF(EMPTY(cWO_NumStr),cTrackNum,cWO_NumStr+","+cTrackNum)
			IF !lTestinput
				WAIT WINDOW "At UPS package charges..." NOWAIT
				SELECT shipment
*				LOCATE FOR shipment.accountid = nAcctNum AND PADR(ALLTRIM(cShip_ref),20) =  PADR(ALLT(shipment.pickticket),20)
				LOCATE FOR INT(VAL(right(rtrim(str(shipment.accountid)),4))) = nAcctNum AND PADR(ALLTRIM(cShip_ref),20) =  PADR(ALLT(shipment.pickticket),20)
				IF !FOUND()
					LOCATE FOR shipment.accountid = 9999 AND shipment.pickticket = PADR(cShip_ref,20)
					IF !FOUND()
						SET STEP ON
						cErrMsg = "MISS UPS REC "+cShip_ref
						DO ediupdate WITH cErrMsg,.T.
						THROW
					ELSE
						SELECT package
						LOCATE
						SUM pkgcharge TO nCharge FOR package.shipmentid = shipment.shipmentid
						cCharge = ALLT(STR(nCharge,8,2))
					ENDIF
				ELSE
					SELECT package
					LOCATE
					SUM pkgcharge TO nCharge FOR package.shipmentid = shipment.shipmentid
					cCharge = ALLT(STR(nCharge,8,2))
				ENDIF
			ELSE
				cCharge = "25.50"
			ENDIF
		ENDIF

		nTotCtnCount = m.qty
		cPO_Num = ALLTRIM(m.cnee_ref)
		cShip_ref = ALLTRIM(m.ship_ref)

*!* Added this code to trap miscounts in OUTDET Units
		IF lPrepack
			SELECT outdet
			SET ORDER TO
			SUM totqty TO nCtnTot1 FOR !units AND outdet.outshipid = outship.outshipid

*!*	Check carton count
			SELECT COUNT(ucc) AS cnt1 ;
				FROM vsteelseriespp ;
				WHERE vsteelseriespp.outshipid = outship.outshipid ;
				AND vsteelseriespp.ucc#"CUTS" ;
				AND vsteelseriespp.totqty > 0 ;
				INTO CURSOR tempsqlx
			STORE tempsqlx.cnt1 TO nCtnTot2
			USE IN tempsqlx
			IF nCtnTot1<>nCtnTot2
				SET STEP ON
				cErrMsg = "SQL CTNQTY ERR"
				DO ediupdate WITH cErrMsg,.T.
				THROW
			ENDIF
		ELSE
			SELECT outdet
			nODRec = RECNO()
			SUM totqty TO nUnitTot1 FOR outdet.units=.T. AND outdet.outshipid = outship.outshipid
			SELECT outdet
			IF !EOF()
				GO nODRec
			ELSE
				GO BOTT
			ENDIF
			SELECT vsteelseriespp
			nVRec = RECNO()
			IF DATETIME()<tempdt
				SUM vsteelseriespp.totqty TO nUnitTot2 FOR vsteelseriespp.ship_ref = outship.ship_ref AND vsteelseriespp.accountid = nAcctNum AND vsteelseriespp.ucc # "CUTS"
			ELSE
				SUM vsteelseriespp.totqty TO nUnitTot2 FOR vsteelseriespp.outshipid = outship.outshipid AND vsteelseriespp.accountid = nAcctNum AND vsteelseriespp.ucc # "CUTS"
			ENDIF
			IF !EOF()
				GO nVRec
			ELSE
				GO BOTT
			ENDIF
			IF nUnitTot1<>nUnitTot2
				SET STEP ON
				cErrMsg = "SQL UNITQTY ERR "+ALLTRIM(outship.ship_ref)
				DO ediupdate WITH cErrMsg,.T.
				THROW
			ENDIF
		ENDIF

		SELECT outdet
		SET ORDER TO outdetid
		LOCATE

*!* End code addition

		lApptFlag = IIF(lAmazon,.T.,.F.)

		IF lTestinput AND EMPTY(outship.appt_num)
			ddel_date = DATE()
			dapptnum = "99999"
			dapptdate = DATE()
		ELSE
			ddel_date = outship.del_date
			IF EMPTY(ddel_date)
				cMissDel = IIF(EMPTY(cMissDel),"The following PTs had no Delivery Dates:"+CHR(10)+TRIM(cShip_ref),cMissDel+CHR(10)+TRIM(cShip_ref))
			ENDIF
			dapptnum = ALLTRIM(outship.appt_num)

			IF EMPTY(dapptnum) && Amazon/Penney/KMart Appt Number check
				IF !(lApptFlag)
					dapptnum = ""
				ELSE
					cErrMsg = "EMPTY APPT #"
					DO ediupdate WITH cErrMsg,.T.
					THROW
				ENDIF
			ENDIF
			dapptdate = outship.appt

			IF EMPTY(dapptdate)
				dapptdate = outship.del_date
			ENDIF
		ENDIF

		IF !(cWO_Num$cWO_NumStr)
			cWO_NumStr = IIF(EMPTY(cWO_NumStr),cWO_Num,cWO_NumStr+","+cWO_Num)
		ENDIF

		IF ALLTRIM(outship.SForCSZ) = ","
			BLANK FIELDS outship.SForCSZ NEXT 1 IN outship
		ENDIF

		cTRNum = ""
		cPRONum = ""

		IF (("WALMART"$outship.consignee) OR ("WAL-MART"$outship.consignee) OR ("WAL MART"$outship.consignee))
			IF LEFT(outship.keyrec,2) = "PR"
				STORE TRIM(keyrec) TO cPRONum
			ENDIF
			IF LEFT(outship.keyrec,2) = "TR"
				STORE TRIM(keyrec) TO cTRNum
			ENDIF
		ENDIF

		cPTString = IIF(EMPTY(cPTString),m.consignee+" "+cShip_ref,cPTString+CHR(13)+CHR(10)+m.consignee+" "+cShip_ref)

		m.CSZ = TRIM(m.CSZ)
		IF EMPTY(ALLT(STRTRAN(M.CSZ,",","")))
			WAIT CLEAR
			WAIT WINDOW "No SHIP-TO City/State/ZIP info...exiting" TIMEOUT 2
			cErrMsg = "NO CSZ INFO"
			DO ediupdate WITH cErrMsg,.T.
			THROW
		ENDIF
		IF !(", "$m.CSZ)
			m.CSZ = ALLTRIM(STRTRAN(m.CSZ,",",", "))
		ENDIF
		m.CSZ = ALLTRIM(STRTRAN(m.CSZ,"  "," "))
		cCity = ALLT(LEFT(TRIM(m.CSZ),AT(",",m.CSZ)-1))
		cStateZip = ALLT(SUBSTR(TRIM(m.CSZ),AT(",",m.CSZ)+1))
		cState = ALLT(LEFT(cStateZip,2))
		cZip = ALLT(SUBSTR(cStateZip,3))

		STORE "" TO cSForCity,cSForState,cSForZip
		m.SForCSZ = ALLTRIM(STRTRAN(m.SForCSZ,"  "," "))
		nSpaces = OCCURS(" ",ALLTRIM(m.SForCSZ))
		IF nSpaces = 0
			m.SForCSZ = STRTRAN(m.SForCSZ,",","")
			cSForCity = ALLTRIM(m.SForCSZ)
			cSForState = ""
			cSForZip = ""
		ELSE
			nCommaPos = AT(",",m.SForCSZ)
			nLastSpace = AT(" ",m.SForCSZ,nSpaces)
			nMinusSpaces = IIF(nSpaces=1,0,1)
			IF ISALPHA(SUBSTR(TRIM(m.SForCSZ),AT(" ",TRIM(m.SForCSZ),nSpaces-nMinusSpaces)+1,2))
				cSForCity = LEFT(TRIM(m.SForCSZ),AT(",",m.SForCSZ)-1)
				cSForState = SUBSTR(m.SForCSZ,nCommaPos+2,2)
				cSForZip = TRIM(RIGHT(TRIM(m.SForCSZ),5))
				IF ISALPHA(cSForZip)
					cSForZip = ""
				ENDIF
			ELSE
				WAIT CLEAR
				WAIT WINDOW "NOT ALPHA: "+SUBSTR(TRIM(m.SForCSZ),AT(" ",m.SForCSZ,nSpaces-1)+1,2) TIMEOUT 3
				cSForCity = LEFT(TRIM(m.SForCSZ),AT(",",m.SForCSZ)-1)
				cSForState = SUBSTR(TRIM(m.SForCSZ),AT(" ",m.SForCSZ,nSpaces-2)+1,2)
				cSForZip = TRIM(RIGHT(TRIM(m.SForCSZ),5))
				IF ISALPHA(cSForZip)
					cSForZip = ""
				ENDIF
			ENDIF
		ENDIF

		DO num_incr_st
		WAIT CLEAR
		WAIT WINDOW "Now creating Line Item information" NOWAIT NOCLEAR

		INSERT INTO temp945 (accountid,isa_num,st_num,wo_num,bol_no,ship_ref,filename,filedate) ;
			VALUES (m.accountid,c_CntrlNum,c_GrpCntrlNum,m.wo_num,m.bol_no,m.ship_ref,cFilenameShort,dt2)

		STORE "ST"+cfd+"945"+cfd+PADL(c_GrpCntrlNum,9,"0")+csegd TO cString
		DO cstringbreak
		nSTCount = nSTCount + 1
		nSegCtr = 1

		STORE "W06"+cfd+"N"+cfd+cShip_ref+cfd+cdate+cfd+TRIM(cBOL)+cfd+TRIM(cWO_Num)+cfd+cPO_Num+csegd TO cString
		DO cstringbreak
		nSegCtr = nSegCtr + 1

		nCtnNumber = 1  && Seed carton sequence count

		cStoreNum = segmentget(@apt,"GLN",alength)
		IF EMPTY(cStoreNum)
			cStoreNum = segmentget(@apt,"STORENUM",alength)
		ENDIF

		IF EMPTY(cStoreNum)
			cStoreNum = m.dcnum
			IF EMPTY(cStoreNum)
				STORE "N1"+cfd+"ST"+cfd+ALLTRIM(m.consignee)+csegd TO cString
			ENDIF
		ELSE
			STORE "N1"+cfd+"ST"+cfd+ALLTRIM(m.consignee)+cfd+"92"+cfd+cStoreNum+csegd TO cString
		ENDIF
		DO cstringbreak
		nSegCtr = nSegCtr + 1

*!*			cCountry = segmentget(@apt,"COUNTRY",alength)
*!*			cCountry = ALLT(cCountry)
*!*			IF EMPTY(cCountry)
*!*				cCountry = "USA"
*!*			ENDIF

		STORE "N3"+cfd+TRIM(outship.address)+csegd TO cString
		DO cstringbreak
		nSegCtr = nSegCtr + 1

		STORE "N4"+cfd+cCity+cfd+cState+cfd+cZip+csegd TO cString &&+cfd+cCountry
		DO cstringbreak
		nSegCtr = nSegCtr + 1

		cSFStoreNum = segmentget(@apt,"SFSTORENUM",alength)
		IF EMPTY(cSFStoreNum)
			cSFStoreNum = ALLTRIM(m.sforstore)
		ENDIF
		IF !EMPTY(m.shipfor)
			IF EMPTY(ALLTRIM(m.sforstore))
				STORE "N1"+cfd+"Z7"+cfd+ALLTRIM(m.shipfor)+csegd TO cString
			ELSE
				STORE "N1"+cfd+"Z7"+cfd+ALLTRIM(m.shipfor)+cfd+"92"+cfd+cSFStoreNum+csegd TO cString
			ENDIF
			DO cstringbreak
			nSegCtr = nSegCtr + 1

			STORE "N3"+cfd+TRIM(m.sforaddr1)+csegd TO cString
			DO cstringbreak
			nSegCtr = nSegCtr + 1

			IF !EMPTY(cSForState)
				STORE "N4"+cfd+cSForCity+cfd+cSForState+cfd+cSForZip+cfd+"USA"+csegd TO cString
			ELSE
				STORE "N4"+cfd+cSForCity+csegd TO cString
			ENDIF
			DO cstringbreak
			nSegCtr = nSegCtr + 1
		ENDIF

		cContact = ALLTRIM(segmentget(@apt,"CONTACT",alength))
		cPhone = ALLTRIM(segmentget(@apt,"PHONE",alength))
		cEDIContact = ALLTRIM(segmentget(@apt,"EDI_CONTACT",alength))
		IF !EMPTY(cContact)
			STORE "PER"+cfd+"AJ"+cfd+cContact+cfd+"TE"+cfd+cPhone+cfd+"EA"+cfd+cEDIContact+csegd TO cString
			DO cstringbreak
			nSegCtr = nSegCtr + 1
		ENDIF

		cGrpCode = ALLTRIM(segmentget(@apt,"GROUPCODE",alength))
		IF !EMPTY(cGrpCode)
			STORE "N9"+cfd+"06"+cfd+cGrpCode+csegd TO cString
			DO cstringbreak
			nSegCtr = nSegCtr + 1
		ENDIF

		IF !EMPTY(outship.keyrec)
			STORE "N9"+cfd+"P8"+cfd+ALLTRIM(outship.keyrec)+csegd TO cString
			DO cstringbreak
			nSegCtr = nSegCtr + 1
		ENDIF

		IF !EMPTY(outship.appt_num)
			STORE "N9"+cfd+"LO"+cfd+ALLTRIM(outship.appt_num)+csegd TO cString
			DO cstringbreak
			nSegCtr = nSegCtr + 1
		ENDIF

		STORE "G62"+cfd+"11"+cfd+TRIM(DTOS(ddel_date))+cfd+"D"+cfd+cDelTime+csegd TO cString  && Ship date/time
		DO cstringbreak
		nSegCtr = nSegCtr + 1

		SELECT scacs
		IF lTesting AND (EMPTY(m.scac) OR EMPTY(m.ship_via))
			STORE "TEST" TO m.scac
			STORE "TEST SHIPPER" TO m.ship_via
		ELSE
			IF !EMPTY(TRIM(outship.scac))
				STORE ALLTRIM(outship.scac) TO m.scac
				lFedEx = .F.
				SELECT scacs
				IF SEEK(m.scac,"scacs","scac")
					IF ("FEDERAL EXPRESS"$NAME) OR ("FEDEX"$NAME)
						lFedEx = .T.
						IF m.scac = "FGC"
							m.scac = "FDEG"
						ENDIF
					ENDIF
					SELECT outship

					IF lParcelType OR lFedEx
						cCarrierType = "U" && Parcel as UPS or FedEx
					ENDIF
				ELSE
*				WAIT CLEAR
*				cErrMsg = "MISSING SCAC"
*				THROW
				ENDIF
			ENDIF
		ENDIF

		SELECT outship

		IF !EMPTY(cTRNum)
			STORE "W27"+cfd+cCarrierType+cfd+TRIM(m.scac)+cfd+TRIM(m.ship_via)+REPLICATE(cfd,2)+"TL"+;
				REPLICATE(cfd,2)+cTRNum+csegd TO cString
		ELSE
			STORE "W27"+cfd+cCarrierType+cfd+TRIM(m.scac)+cfd+TRIM(m.ship_via)+csegd TO cString
		ENDIF
		DO cstringbreak
		nSegCtr = nSegCtr + 1

		IF lParcelType
			STORE "G72"+cfd+"516"+cfd+"15"+REPLICATE(cfd,6)+ALLT(cCharge)+csegd TO cString
			DO cstringbreak
			nSegCtr = nSegCtr + 1
		ENDIF

*************************************************************************
*2	DETAIL LOOP - MATCHING SHIP_REF FIELD (FROM OUTSHIP)
*************************************************************************
		WAIT CLEAR
		WAIT WINDOW "Now creating Detail information "+cShip_ref NOWAIT NOCLEAR
		SELECT vsteelseriespp

		SELECT outdet
		SET ORDER TO TAG outdetid
		SELECT vsteelseriespp
		SET RELATION TO outdetid INTO outdet

		SELECT vsteelseriespp
		LOCATE
		IF DATETIME()<tempdt
			LOCATE FOR vsteelseriespp.ship_ref = TRIM(cShip_ref)
		ELSE
			LOCATE FOR vsteelseriespp.ship_ref = TRIM(cShip_ref) AND vsteelseriespp.outshipid = nOutshipid
		ENDIF
		IF !FOUND()
			IF !lTesting
				WAIT WINDOW "PT "+cShip_ref+" NOT FOUND in vsteelseriespp...ABORTING" TIMEOUT 2
				IF !lTesting
					lSQLMail = .T.
				ENDIF
				cErrMsg = "MISS PT-SQL: "+cShip_ref
				DO ediupdate WITH cErrMsg,.T.
				THROW
			ELSE
				LOOP
			ENDIF
		ENDIF

		IF DATETIME()<tempdt
			vscanstr = "vsteelseriespp.ship_ref = cShip_ref"
		ELSE
			vscanstr = "vsteelseriespp.ship_ref = cShip_ref and vsteelseriespp.outshipid = nOutshipid"
		ENDIF

		SELECT vsteelseriespp
		LOCATE
		LOCATE FOR &vscanstr
		cUCC= "XXX"

		DO WHILE &vscanstr
			lSkipBack = .T.

			IF TRIM(vsteelseriespp.ucc) <> cUCC
				STORE TRIM(vsteelseriespp.ucc) TO cUCC
			ENDIF

			STORE "LX"+cfd+ALLTRIM(STR(nCtnNumber))+csegd TO cString   && Carton Seq. # (up to Carton total)
			lDoManSegment = .T.
			lDoPALSegment = .F.
			DO cstringbreak
			nSegCtr = nSegCtr + 1

			DO WHILE vsteelseriespp.ucc = cUCC
				cDesc = ""
				SELECT outdet
				alength = ALINES(aptdet,outdet.printstuff,.T.,CHR(13))
				lPrepack945 = .F.
				cUCCNumber = vsteelseriespp.ucc
				cUCCNumber = ALLTRIM(STRTRAN(TRIM(cUCCNumber)," ",""))

				IF lDoManSegment
					lDoManSegment = .F.
					nPTCtnTot =  nPTCtnTot + 1
					IF lParcelType
						IF lTestinput
							cTrkNumber = ALLTRIM(outship.bol_no)
						ELSE
							SELECT package1
*!* Added this block to capture multiple trk. #s without accompanying UCC #s, Joe 07.17.2017
							WAIT WINDOW ALLTRIM(STR(RECCOUNT())) NOWAIT
							COUNT TO nUCCx FOR !EMPTY(package1.ucc)
							COUNT TO nRef4x FOR !EMPTY(package1.ref4)
							lNoCount = IIF((nUCCx = 0 AND nRef4x = 0),.T.,.F.)
							RELEASE nUCCx,nRef4x
							IF lTesting
								SET STEP ON
							ENDIF
							LOCATE
							IF lNoCount
								GO nCtnNumber
								cTrkNumber = ALLTRIM(package1.trknumber)
*!* End added code block
							ELSE
								LOCATE FOR  package1.ref4 = cUCCNumber
								IF !FOUND()
									LOCATE FOR  package1.ucc = cUCCNumber
									IF FOUND()
										cTrkNumber = ALLTRIM(package1.trknumber)
									ELSE
										=SEEK(cShip_ref,"shipment","pickticket")
										LOCATE FOR package1.shipmentid = shipment.shipmentid
										IF FOUND()
											cTrkNumber = ALLTRIM(package1.trknumber)
										ELSE
											DO ediupdate WITH "Missing TRK # at "+cShip_ref,.T.
											THROW
										ENDIF
									ENDIF
								ELSE
									cTrkNumber = ALLTRIM(package1.trknumber)
								ENDIF
							ENDIF
						ENDIF
					ENDIF
*							nCtnNumber = nCtnNumber + 1

					IF lParcelType
						STORE "MAN"+cfd+"GM"+cfd+TRIM(cUCCNumber)+cfd+cfd+"CP"+cfd+cTrkNumber+csegd TO cString
					ELSE
						STORE "MAN"+cfd+"GM"+cfd+TRIM(cUCCNumber)+csegd TO cString
					ENDIF
					DO cstringbreak
					nSegCtr = nSegCtr + 1
					nShipDetQty = INT(VAL(outdet.PACK))
					nUnitSum = nUnitSum + nShipDetQty
				ENDIF

				cStyle = TRIM(outdet.STYLE)
				IF EMPTY(cStyle)
					IF DATETIME()<tempdt
						cStyle = TRIM(vsteelseriespp.STYLE)
					ELSE
						WAIT WINDOW "Empty style in "+cShip_ref TIMEOUT 1
					ENDIF
				ENDIF

			lDoUPC = .T.
			cUPC = ""
			cUPC = ALLTRIM(outdet.upc)
			IF (ISNULL(cUPC) OR EMPTY(cUPC) OR VAL(cUPC) = 0)
				cUPC = TRIM(vsteelseriespp.upc)
			ENDIF
			cItemNum = TRIM(outdet.custsku)
			nShipDetQty = vsteelseriespp.totqty
			IF ISNULL(nShipDetQty) OR  nShipDetQty = 0 && OR EMPTY(nShipDetQty)
				SET STEP ON
				nShipDetQty = outdet.totqty
			ENDIF

			nOrigDetQty = vsteelseriespp.qty
			IF ISNULL(nOrigDetQty) OR nOrigDetQty = 0
				SET STEP ON
				nOrigDetQty = outdet.origqty
			ENDIF

*!* Changed the following to utilize original 940 unit codes from Printstuff field
			cUnitCode = TRIM(segmentget(@aptdet,"UNITCODE",alength))
			IF EMPTY(cUnitCode)
				cUnitCode = "EA"
			ENDIF

			IF nOrigDetQty = nShipDetQty
				nShipStat = "CC"
			ELSE
				nShipStat = "CP"
			ENDIF

			IF lDoUPC
				STORE "W12"+cfd+nShipStat+cfd+ALLTRIM(STR(nOrigDetQty))+cfd+;
					ALLTRIM(STR(nShipDetQty))+cfd+ALLTRIM(STR(nOrigDetQty-nShipDetQty))+cfd+;
					cUnitCode+cfd+cUPC+cfd+"IN"+cfd+ALLTRIM(outdet.STYLE)+IIF(!EMPTY(ALLTRIM(outdet.custsku)),REPLICATE(cfd,9)+"VN"+cfd+ALLTRIM(outdet.custsku)+csegd,csegd) TO cString
			ELSE
				STORE "W12"+cfd+nShipStat+cfd+ALLTRIM(STR(nOrigDetQty))+cfd+;
					ALLTRIM(STR(nShipDetQty))+cfd+ALLTRIM(STR(nOrigDetQty-nShipDetQty))+cfd+;
					cUnitCode+cfd+ALLTRIM(outdet.custsku)+csegd TO cString
			ENDIF
			DO cstringbreak
			nSegCtr = nSegCtr + 1

			cDesc = ALLTRIM(segmentget(@aptdet,"DESC",alength))
			IF !EMPTY(cDesc)
				STORE "G69"+cfd+cDesc+csegd TO cString
				DO cstringbreak
				nSegCtr = nSegCtr + 1
			ENDIF

&& OK now check to see if we have any serial numbers
			xsqlexec("select * from ctnucc where mod='Y' and ucc='"+cUCCNumber+"' and style='"+outdet.STYLE+"'","tempsns",,"wh")
			SCAN
				STORE "N9"+cfd+"SN"+cfd+ALLTRIM(tempsns.serialno)+csegd TO cString
				DO cstringbreak
				nSegCtr = nSegCtr + 1
			ENDSCAN

			SKIP 1 IN vsteelseriespp
			SET DELETED ON
		ENDDO
		lSkipBack = .T.
		nCtnNumber = nCtnNumber + 1
*		nUnitSum = nUnitSum + 1
	ENDDO

*************************************************************************
*2^	END DETAIL MATCHING SHIP_REF LOOP
*************************************************************************
	WAIT CLEAR
	IF outship.cuft>0
		cCube = ALLTRIM(STR(outship.cuft,6,2))
	ELSE
		cCube = "0"
	ENDIF
	WAIT WINDOW "Now creating Section Closure information" NOWAIT NOCLEAR
	STORE "W03"+cfd+ALLTRIM(STR(nPTCtnTot))+cfd+ALLTRIM(STR(outship.weight))+cfd+;
		cWeightUnit+cfd+cCube+cfd+"CF"+csegd TO cString   && Units sum, Weight sum, carton count

	nPTCtnTot = 0
	nTotCtnWt = 0
	nTotCtnCount = 0
	nUnitSum = 0
	FPUTS(nFilenum,cString)

	STORE  "SE"+cfd+ALLTRIM(STR(nSegCtr+2))+cfd+PADL(c_GrpCntrlNum,9,"0")+csegd TO cString
	FPUTS(nFilenum,cString)

	SELECT outship
	WAIT CLEAR
ENDSCAN

*************************************************************************
*1^	END OUTSHIP MAIN LOOP
*************************************************************************

DO close945
=FCLOSE(nFilenum)

IF !lTesting
	SELECT edi_trigger
	DO ediupdate WITH "945 CREATED",.F.
	SELECT edi_trigger
	LOCATE

	IF !USED("ftpedilog")
		SELECT 0
		USE F:\edirouting\ftpedilog ALIAS ftpedilog
		INSERT INTO ftpedilog (TRANSFER,ftpdate,filename,acct_name,TYPE) VALUES ("945-"+cCustname+"-"+cCustLoc,dt2,cFilenameHold,UPPER(cCustname),"945")
		USE IN ftpedilog
	ENDIF
ENDIF

WAIT CLEAR
WAIT WINDOW cCustFolder+" 945 Process complete..." NOWAIT

*!* Create eMail confirmation message
IF lTestMail
	tsendto = tsendtotest
	tcc = tcctest
ENDIF

cOutFolder = "FMI"+cCustLoc

tsubject = cMailName+" 945 EDI File from TGF as of "+dtmail+" ("+cCustLoc+")"
tattach = " "
tmessage = "945 EDI Info from TGF, for division "+cDivision+", BOL# "+TRIM(cBOL)+CHR(10)
tmessage = tmessage + "For Work Orders: "+cWO_NumStr+","+CHR(10)
tmessage = tmessage + "containing these "+ALLTRIM(STR(nPTCount))+" picktickets:"+CHR(10)+CHR(13)
tmessage = tmessage + cPTString + CHR(10)+CHR(13)
tmessage = tmessage +"has been created and will be transmitted ASAP."+CHR(10)+CHR(10)
IF !EMPTY(cMissDel)
	tmessage = tmessage+CHR(10)+CHR(10)+cMissDel+CHR(10)+CHR(10)
ENDIF
IF lTesting OR lTestinput
	tmessage = tmessage + "This is a TEST 945"
ELSE
	tmessage = tmessage + "If you have any questions, please eMail or call 732-750-9000 x217."
ENDIF

IF lEmail
	WAIT WINDOW "At do mail stage..." NOWAIT
	DO FORM m:\dev\frm\dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
ENDIF

cFilenameArchive = UPPER("f:\ftpusers\"+cCustFolder+"\945OUT\ARCHIVE\"+cCustPrefix+dt1+".txt")

RELEASE ALL LIKE c_CntrlNum,c_GrpCntrlNum
WAIT CLEAR
WAIT WINDOW cMailName+" 945 EDI File output complete" TIMEOUT 1
lDoCatch = .F.

*!* Transfers files to correct output folders
COPY FILE &cFilenameHold TO &cFilenameArch
IF lTesting
	SET STEP ON
ELSE
	WAIT WINDOW "" TIMEOUT 1
ENDIF
IF lSteelFilesOut=.T. AND lTesting=.F.
	COPY FILE &cFilenameHold TO &cFilenameOut
	DELETE FILE &cFilenameHold
	SELECT temp945
	COPY TO "f:\3pl\data\temp945ss.dbf"
	USE IN temp945
	SELECT 0
	USE "f:\3pl\data\temp945ss.dbf" ALIAS temp945ss
	SELECT 0
	USE "f:\3pl\data\pts_sent945.dbf" ALIAS pts_sent945
	APPEND FROM "f:\3pl\data\temp945ss.dbf"
	USE IN pts_sent945
	USE IN temp945ss
	DELETE FILE "f:\3pl\data\temp945ss.dbf"
ENDIF

	*!* asn_out_data()()

CATCH TO oErr
	IF lDoCatch
		WAIT WINDOW  "In Error CATCH" TIMEOUT 2
		SET STEP ON
		cErrMsg2 = cErrMsg
		IF EMPTY(ALLTRIM(cErrMsg))
			cErrMsg = "ERRHAND ERROR"
		ENDIF
		DO ediupdate WITH cErrMsg,.T.
		tsubject = cMailName+" Error ("+TRANSFORM(oErr.ERRORNO)+") at "+TTOC(DATETIME())
		tattach  = ""
		IF lTesting
			tsendto  = tsendtotest
			tcc = tcctest
		ELSE
			tsendto  = tsendtoerr
			tcc = tccerr
		ENDIF

		IF !lIsError AND cErrMsg2 # "MISS UPS REC"
			tmessage = cCustname+" Error processing "+CHR(13)
			tmessage = tmessage+TRIM(PROGRAM())+CHR(13)

			tmessage =tmessage+ "Try/CATCH Exeception Message:"+CHR(13)+;
				[  Error: ] + STR(oErr.ERRORNO) +CHR(13)+;
				[  LineNo: ] + STR(oErr.LINENO) +CHR(13)+;
				[  Message: ] + oErr.MESSAGE +CHR(13)+;
				[  Procedure: ] + oErr.PROCEDURE +CHR(13)+;
				[  Details: ] + oErr.DETAILS +CHR(13)+;
				[  StackLevel: ] + STR(oErr.STACKLEVEL) +CHR(13)+;
				[  LineContents: ] + oErr.LINECONTENTS+CHR(13)+;
				[  UserValue: ] + oErr.USERVALUE

			tsubject = "945 EDI Poller Error at "+TTOC(DATETIME())
			tattach  = ""
			tcc=""
*			ASSERT .F. MESSAGE "At do mail stage...DEBUG"
			DO FORM m:\dev\frm\dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
		ENDIF
		FCLOSE(nFilenum)
	ENDIF
FINALLY
	IF USED('OUTSHIP')
		USE IN outship
	ENDIF
	IF USED('OUTDET')
		USE IN outdet
	ENDIF
	IF USED('SHIPMENT')
		USE IN shipment
	ENDIF
	IF USED('PACKAGE')
		USE IN package
	ENDIF
	IF USED('SERFILE')
		USE IN serfile
	ENDIF

	WAIT CLEAR

ENDTRY

*** END OF CODE BODY

****************************
PROCEDURE close945
****************************
** Footer Creation

	STORE  "GE"+cfd+ALLTRIM(STR(nSTCount))+cfd+c_CntrlNum+csegd TO cString
	FPUTS(nFilenum,cString)

	STORE  "IEA"+cfd+"1"+cfd+PADL(c_CntrlNum,9,"0")+csegd TO cString
	FPUTS(nFilenum,cString)

	RETURN

****************************
PROCEDURE num_incr_isa
****************************

	IF !USED("serfile")
		USE ("f:\3pl\data\serial\"+cCustname+"945_serial") IN 0 ALIAS serfile
	ENDIF
	SELECT serfile
	IF lTesting AND lISAFlag
		nOrigSeq = serfile.seqnum
		lISAFlag = .F.
	ENDIF
	nISA_Num = serfile.seqnum
	c_CntrlNum = ALLTRIM(STR(serfile.seqnum))
	REPLACE serfile.seqnum WITH serfile.seqnum + 1 IN serfile
	SELECT outship
	RETURN

****************************
PROCEDURE num_incr_st
****************************

	IF !USED("serfile")
		USE ("f:\3pl\data\serial\"+cCustname+""945_serial") IN 0 ALIAS serfile
	ENDIF
	SELECT serfile
	IF lTesting AND lSTFlag
		nOrigGrpSeq = serfile.grpseqnum
		lSTFlag = .F.
	ENDIF
	c_GrpCntrlNum = ALLTRIM(STR(serfile.grpseqnum))
	REPLACE serfile.grpseqnum WITH serfile.grpseqnum + 1 IN serfile
	SELECT outship
	RETURN


****************************
PROCEDURE segmentget
****************************
	PARAMETER thisarray,lcKey,nLength

	FOR i = 1 TO nLength
		IF i > nLength
			EXIT
		ENDIF
		lnEnd= AT("*",thisarray[i])
		IF lnEnd > 0
			lcThisKey =TRIM(SUBSTR(thisarray[i],1,lnEnd-1))
			IF OCCURS(lcKey,lcThisKey)>0
				RETURN SUBSTR(thisarray[i],lnEnd+1)
				i = 1
			ENDIF
		ENDIF
	ENDFOR

	RETURN ""

****************************
PROCEDURE ediupdate
****************************
	PARAMETER cStatus,lIsError
*SET STEP ON
	IF !lTesting
		SELECT edi_trigger
		nRec = RECNO()
		IF !lIsError
			REPLACE processed WITH .T.,proc945 WITH .T.,file945 WITH cFilenameArch,isa_num WITH cISA_Num,;
				fin_status WITH "945 CREATED",errorflag WITH .F.,when_proc WITH DATETIME() ;
				FOR edi_trigger.bol = cBOL AND accountid = nAcctNum
			lDoCatch = .F.
		ELSE
			lDoCatch = .F.
			REPLACE processed WITH .T.,proc945 WITH .T.,file945 WITH "", fin_status WITH cStatus,errorflag WITH .T. ;
				FOR edi_trigger.bol = cBOL AND accountid = nAcctNum
			num_decrement()

			IF lCloseOutput
				=FCLOSE(nFilenum)
				ERASE &cFilenameHold
			ENDIF
		ENDIF
	ENDIF

	IF lIsError AND lEmail AND cStatus<>"SQL ERROR"
		tsubject = "945 Error in "+cMailName+" BOL "+TRIM(cBOL)+"(At PT "+cShip_ref+")"
		tattach = " "
		tsendto = tsendtoerr
		tcc = tccerr
		tmessage = "945 Processing for BOL# "+cBOL+", WO# "+cWO_Num+" produced this error: (Office: "+cOffice+"): "+cStatus+CHR(10)+"Check EDI_TRIGGER and re-run"
		IF "TOTQTY ERR"$cStatus
			tmessage = tmessage + CHR(10) + "At OUTSHIPID: "+ALLTRIM(STR(m.outshipid))
		ENDIF
*		ASSERT .F. MESSAGE "At do mail stage...DEBUG"
		DO FORM m:\dev\frm\dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
	ENDIF

	IF USED('outship')
		USE IN outship
	ENDIF
	IF USED('outdet')
		USE IN outdet
	ENDIF

	IF USED('serfile')
		SELECT serfile
		IF lTesting
			REPLACE serfile.seqnum WITH nOrigSeq
			REPLACE serfile.grpseqnum WITH nOrigGrpSeq
		ENDIF
		USE IN serfile
	ENDIF
	IF USED('scacs')
		USE IN scacs
	ENDIF
	IF USED('mm')
		USE IN mm
	ENDIF
	IF USED('tempx')
		USE IN tempx
	ENDIF
	IF !lTesting
		SELECT edi_trigger
		LOCATE
	ENDIF

ENDPROC

****************************
PROCEDURE cstringbreak
****************************
	cLen = LEN(ALLTRIM(cString))
	IF ISNULL(cString)
		ASSERT .F. MESSAGE "At cStringBreak procedure"
	ENDIF
	FPUTS(nFilenum,cString)
ENDPROC


****************************
PROCEDURE cszbreak
****************************
	cCSZ = ALLT(m.CSZ)

	FOR ii = 5 TO 2 STEP -1
		cCSZ = STRTRAN(cCSZ,SPACE(ii),SPACE(1))
	ENDFOR
	cCSZ = STRTRAN(cCSZ,",","")
	len1 = LEN(ALLT(cCSZ))
	nSpaces = OCCURS(" ",cCSZ)

	IF nSpaces<2
		cErrMsg = "BAD CSZ INFO"
		DO ediupdate WITH cErrMsg,.T.
		THROW
	ENDIF

*	ASSERT .F. MESSAGE "In CSZ Breakout"
	FOR ii = nSpaces TO 1 STEP -1
		ii1 = ALLT(STR(ii))
		DO CASE
			CASE ii = nSpaces
				nPOS = AT(" ",cCSZ,ii)
				cZip = ALLT(SUBSTR(cCSZ,nPOS))
*				WAIT WINDOW "ZIP: "+cZip TIMEOUT 1
				nEndState = nPOS
			CASE ii = (nSpaces - 1)
				nPOS = AT(" ",cCSZ,ii)
				cState = ALLT(SUBSTR(cCSZ,nPOS,nEndState-nPOS))
*				WAIT WINDOW "STATE: "+cState TIMEOUT 1
				IF nSpaces = 2
					cCity = ALLT(LEFT(cCSZ,nPOS))
*					WAIT WINDOW "CITY: "+cCity TIMEOUT 1
					EXIT
				ENDIF
			OTHERWISE
				nPOS = AT(" ",cCSZ,ii)
				cCity = ALLT(LEFT(cCSZ,nPOS))
*				WAIT WINDOW "CITY: "+cCity TIMEOUT 2
		ENDCASE
	ENDFOR
ENDPROC

****************************
PROCEDURE num_decrement
****************************
*!* This procedure decrements the ISA/GS numbers in the counter
*!* in the event of failure and deletion of the current 945
	IF lCloseOutput
		IF !USED("serfile")
			USE ("f:\3pl\data\serial\"+cCustname+"945_serial") IN 0 ALIAS serfile
		ENDIF
		SELECT serfile
		REPLACE serfile.seqnum WITH serfile.seqnum - 1 IN serfile  && ISA number
		REPLACE serfile.grpseqnum WITH serfile.grpseqnum - 1 IN serfile  && GS number
		RETURN
	ENDIF
ENDPROC