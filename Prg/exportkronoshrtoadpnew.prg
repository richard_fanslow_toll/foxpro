* Export new hires from Kronos HR in adp csv files, one each for each ADP company.
* NOTE: because info from existing position codes may no longer be current after we assign new ADP companies,
* get ADP_COMP and HOMEDEPT from WTK labor levels, instead of the ADP export view.
*
* NOTE: BUILD EXE AS F:\UTIL\ADPEDI\exportkronoshrtoadp.exe  !!!!
*
* Kronos HR status codes:
#DEFINE KACTIVE '-10131'
#DEFINE KTERMINATED '-10133'
#DEFINE KLEAVE '-10132'
#DEFINE EXPRESSHOMEDEPT '020655'

runack("EXPORTKRONOSHRTOADP")

LOCAL loKronosToADPExportProcess
loKronosToADPExportProcess = CREATEOBJECT('KronosToADPExportProcess')
loKronosToADPExportProcess.MAIN()
CLOSE DATABASES ALL
RETURN

#DEFINE LOGIT 1
#DEFINE WAITIT 2
#DEFINE NOWAITIT 4
#DEFINE SENDIT 8
#DEFINE CRLF CHR(13) + CHR(10)
#DEFINE CARRIAGE_RETURN CHR(13)
#DEFINE LINE_FEED CHR(10)
#DEFINE RETURN_DATA_MANDATORY .F.
#DEFINE RETURN_DATA_NOT_MANDATORY .T.

DEFINE CLASS KronosToADPExportProcess AS CUSTOM

	cProcessName = 'KronosToADPExportProcess'

	lTestMode = .F.  && .F. FOR NORMAL OPERATION, .T. TO SUPPRESS ALL BUT DEVELOPER EMAILS AND CHANGE INPUT TABLE, ETC.

	lAutoYield = .T.

	lCalcSickBalance = .F.

	dToday = DATE()

	cStartTime = TTOC(DATETIME())

	* output file properties
	cADPPath = 'C:\ADP\PCPW\ADPDATA\'
	nCurrentOutputCSVFileHandle = -1
	cOutputCSVHeaderLine1 = 'CO CODE,FILE #,SOCIAL SECURITY NUMBER,EMPLOYEE LAST NAME,EMPLOYEE FIRST NAME,ADDRESS LINE 1,ADDRESS LINE 2,CITY,STATE POSTAL CODE,'
	cOutputCSVHeaderLine2 = 'ZIP CODE,GENDER,HOME DEPARTMENT,RATE TYPE,RATE 1 AMOUNT,HIRE STATUS,HIRE DATE,BIRTH DATE,WORKED STATE TAX CODE,SUI/SDI TAX JURISDICTION CODE'
	*cOutputCSVHeaderLine2 = 'ZIP CODE,GENDER,HOME DEPARTMENT,RATE TYPE,RATE 1 AMOUNT,HIRE STATUS,HIRE DATE,BIRTH DATE,WORKED STATE TAX CODE'
	cArchiveTargetCSVPath = 'F:\UTIL\ADPEDI\FROMKRONOSHR\TARGETARCHIVED\'
	cFileDate = STRTRAN(DTOC(DATE()),"/","_")

	* processing properties
	nNumberOfOutputFiles = 0
	cTopEmailText = ""
	cErrorsText = ""
	lOutputAXAFile = .F.
	lOutputSXIFile = .F.
	lOutputZXUFile = .F.
	cNewHireList = ''

	* connection properties
	nSQLHandle = 0

	* wait window properties
	nWaitWindowTimeout = 2
	lWaitWindowIsOn = .T.

	* logfile properties
	lLoggingIsOn = .T.
	cLogFile = 'F:\UTIL\ADPEDI\Logfiles\KronosToADPExportProcess_log.txt'

	* INTERNAL email properties
	lSendInternalEmailIsOn = .T.
	cFrom = 'FMI Corporate <fmicorporate@fmiint.com>'
*!*		cSendTo = 'Mark Bennett <mbennett@fmiint.com>'
*!*		cCC = ''
	cSendTo = 'lucille.waldrip@Tollgroup.com'
	cCC = 'Marie.Freiberger@Tollgroup.com, Lauren.Klaver@Tollgroup.com, mbennett@fmiint.com'
	cSubject = 'Kronos HR To ADP Export for: ' + TTOC(DATETIME())
	cAttach = ''
	cBodyText = ''
	
*!*		* Express properties
*!*		cExpressSendTo = 'Mark Bennett <mbennett@fmiint.com>'
*!*		cExpressCC = ''
*!*		cExpressHireList = ''

	* other properties
	cCOMPUTERNAME = ''
	cUSERNAME = ''

	FUNCTION INIT
		IF NOT DODEFAULT()
			RETURN .F.
		ENDIF
		WITH THIS
			CLEAR
			*SET RESOURCE OFF
			CLOSE DATA
			SET CENTURY ON
			SET DATE AMERICAN
			SET DECIMALS TO 3
			SET HOURS TO 24
			SET ANSI ON
			SET TALK OFF
			SET DELETED ON
			SET CONSOLE OFF
			SET EXCLUSIVE OFF
			SET SAFETY OFF
			SET EXACT OFF
			SET STATUS BAR ON
			SET SYSMENU OFF
			SET ENGINEBEHAVIOR 70
			_VFP.AUTOYIELD = .lAutoYield
			.cCOMPUTERNAME = GETENV("COMPUTERNAME")
			.cUSERNAME = GETENV("USERNAME")
			IF .lTestMode THEN
				.cSendTo = 'Mark Bennett <mbennett@fmiint.com>'
				.cCC = ''
				.cLogFile = 'F:\UTIL\ADPEDI\LOGFILES\KronosToADPExportProcess_log_TESTMODE.txt'
			ENDIF
			.lLoggingIsOn = .lLoggingIsOn AND NOT EMPTY(.cLogFile)
			IF .lLoggingIsOn THEN
				SET ALTERNATE TO (.cLogFile) ADDITIVE
				SET ALTERNATE ON
			ENDIF
			* re-init date properties after all SETTINGS
			.dToday = DATE()
		ENDWITH
	ENDFUNC


	FUNCTION DESTROY
		WITH THIS
			IF .lLoggingIsOn  THEN
				SET ALTERNATE OFF
				SET ALTERNATE TO
			ENDIF
		ENDWITH
		DODEFAULT()
	ENDFUNC


	FUNCTION MAIN
		WITH THIS
			LOCAL lnNumberOfErrors, loError, lcSQLADP, lcSQLWTK, lcSQLKronosHR, ldToday, ldToday
			LOCAL lcSiteState, lcSUISDI, lcEmployeeIdentifier, lcBothStreets

			TRY
				lnNumberOfErrors = 0

				*.TrackProgress('==================================================================================================================', LOGIT+SENDIT)
				.TrackProgress('Kronos To ADP Export process started....', LOGIT+SENDIT+NOWAITIT)
				.TrackProgress('COMPUTERNAME = ' + .cCOMPUTERNAME, LOGIT+SENDIT)
				.TrackProgress('USERNAME = ' + .cUSERNAME, LOGIT+SENDIT)
				.TrackProgress('Project = EXPORTKRONOSHRTOADP', LOGIT+SENDIT)
				IF .lTestMode THEN
					.TrackProgress('                        TEST MODE                        ', LOGIT+SENDIT)
				ENDIF
				
				SET PROCEDURE TO VALIDATIONS ADDITIVE

				ldToday = .dToday
				ldOneMonthAgo = GOMONTH(ldToday,-1)
				*ldOneMonthAgo = GOMONTH(ldToday,-4)
				*ldOneMonthAgo = GOMONTH(ldToday,-60)

				lcSQLADP = ;
					" SELECT " + ;
					" FILE# AS FILE_NUM " + ;
					" FROM REPORTS.V_EMPLOYEE " + ;
					" WHERE STATUS <> 'T' " + ;
					" ORDER BY FILE# "

				* note: excluding empno 999 which is a Test employee
				* ALSO: excluding file #s 900-998 which are reserved for new Toll employees we don't want in ADP until some future date
				lcSQLWTK = ;
					" SELECT " + ;
					" PERSONNUM AS FILE_NUM, " + ;
					" PERSONFULLNAME AS NAME, " + ;
					" HOMELABORLEVELNM1 AS ADP_COMP, " + ;
					" HOMELABORLEVELNM2 AS DIVISION, " + ;
					" HOMELABORLEVELNM3 AS DEPT, " + ;
					" HOMELABORLEVELNM4 AS WORKSITE " + ;
					" FROM VP_EMPLOYEEV42 " + ;
					" WHERE HOMELABORLEVELNM1 <> 'TMP' " + ;
					" AND (NOT (PERSONNUM >= '900' AND PERSONNUM <= '998')) " + ;
					" AND PERSONNUM <> '999' " + ;
					" AND PERSONNUM <> '10000' " + ;
					" ORDER BY PERSONNUM "
					
*!*					* note: excluding empno 999 which is a Test employee
*!*					lcSQLKronosHR = ;
*!*						" SELECT " + ;
*!*						" CAST(empno AS CHAR) AS FILE_NUM, " + ;
*!*						" CAST(persontaxidno AS CHAR) AS SS_NUM, " + ;
*!*						" CAST(lastname AS CHAR) AS LASTNAME, " + ;
*!*						" CAST(firstname AS CHAR) AS FIRSTNAME, " + ;
*!*						" CAST(personaddress1 AS CHAR) AS STREET1, " + ;
*!*						" CAST(personaddress2 AS CHAR) AS STREET2, " + ;
*!*						" CAST(personaddresscity AS CHAR) AS CITY, " + ;
*!*						" CAST(localcodeabbreviation AS CHAR) AS STATE, " + ;
*!*						" CAST(personaddresspostalcode AS CHAR) AS ZIPCODE, " + ;
*!*						" CAST(Gender AS CHAR) AS GENDER, " + ;
*!*						" PAYRATE AS RATE1AMT, " + ;
*!*						" 'A' AS STATUS, " + ;
*!*						" ' ' AS RATETYPE, " + ;
*!*						" lasthiredate AS HIREDATE, " + ;
*!*						" birthdate AS DOB, " + ;
*!*						" CAST(WORKED_ST AS CHAR) AS WORKSTATE, " + ;
*!*						" '00' AS SUISDI " + ;
*!*						" FROM FMI_ADP_INTERFACE " + ;
*!*						" WHERE employeestatusidno = " + KACTIVE + ;
*!*						" AND COCODE <> 'TMP' " + ;
*!*						" AND empno <> '999' " + ;
*!*						" AND empno <> '10000' " + ;
*!*						" ORDER BY empno "

				* NEW SQL TO BYPASS USING THE FMI_ADP_INTERFACE VIEW, WHICH HAS ALL INNER JOINS
				* THIS USES LEFT OUTER JOINS SO WE CAN SEE WHAT DATA ELEMENTS ARE MISSING.
				lcSQLKronosHR = ;
					" SELECT DISTINCT " + ;
					" {FN IFNULL(B.CoCode,'')} AS ADP_COMP, " + ;
					" CAST(G.EmpNo AS CHAR) AS FILE_NUM, " + ; 
					" {FN IFNULL(CAST(A.PersonTaxIdNo AS CHAR),'')} AS SS_NUM, " + ;
					" {FN IFNULL(CAST(A.LastName AS CHAR),'')} AS LASTNAME, " + ;
					" {FN IFNULL(CAST(A.FirstName AS CHAR),'')} AS FIRSTNAME, " + ;
					" {FN IFNULL(CAST(F.PersonAddress1 AS CHAR),'')} AS STREET1, " + ; 
					" {FN IFNULL(CAST(F.PersonAddress2 AS CHAR),'')} AS STREET2, " + ;
					" {FN IFNULL(CAST(F.PersonAddressCity AS CHAR),'')} AS CITY, " + ;
					" {FN IFNULL(CAST(I.LocalCodeAbbreviation AS CHAR),'')} AS STATE, " + ;
					" {FN IFNULL(CAST(F.PersonAddressPostalCode AS CHAR),'')} AS ZIPCODE, " + ; 
					" CASE WHEN A.genderidno = '-10152' THEN 'M' ELSE 'F' END AS Gender, " + ;
					" {FN IFNULL(H.PayRate,0.00)} AS RATE1AMT, " + ;
					" 'A' AS STATUS, " + ;
					" ' ' AS RATETYPE, " + ; 
					" G.LastHireDate AS HIREDATE, " + ;
					" A.BirthDate AS DOB, " + ;
					" {FN IFNULL(CAST(B.WORKED_ST AS CHAR),'')} AS WORKSTATE, " + ;
					" '00' AS SUISDI " + ;
					" FROM tPERSONS A " + ;
					" LEFT OUTER JOIN tPERSON_POSITIONS C " + ;
					" ON A.PersonIdNo = C.PersonIdNo " + ;
					" LEFT OUTER JOIN tPOSITION_CODES D " + ;
					" ON C.PositionIdNo = D.PositionIdNo " + ;
					" LEFT OUTER JOIN tPOSITION_ORGS E " + ;
					" ON E.PositionIdNo = D.PositionIdNo " + ;
					" LEFT OUTER JOIN tPERSON_ADDRESSES F " + ;
					" ON F.PersonIdNo = A.PersonIdNo " + ;
					" LEFT OUTER JOIN tEMPLOYMENT_STATUS G " + ;
					" ON G.PersonIdNo = A.PersonIdNo " + ;
					" LEFT OUTER JOIN tPAY_STATUS H " + ;
					" ON H.PersonIdNo = A.PersonIdNo " + ;
					" LEFT OUTER JOIN tLOCAL_CODES I " + ;
					" ON I.LocalCodeIdNo = F.PersonAddressStateProvinceIdNo " + ;
					" LEFT OUTER JOIN FMI_ADP_REF B " + ;
					" ON B.POSITIONIDNO = C.PositionIdNo " + ;
					" WHERE (G.EmployeeStatusIdNo = " + KACTIVE + ") AND " + ;
					" {FN IFNULL(B.CoCode,'')} <> 'TMP' AND " + ;
					" (NOT (CAST(G.EmpNo AS CHAR) >= '900' AND CAST(G.EmpNo AS CHAR) <= '998')) AND " + ;
					" CAST(G.EmpNo AS CHAR) <> '999' AND " + ;
					" CAST(G.EmpNo AS CHAR) <> '10000' AND " + ;
					" (A.PersonToEffectDate = '1/1/3000') AND (G.EmploymentStatusToEffectDate = '1/1/3000') AND " + ; 
					" (F.PersonAddressToEffectDate = '1/1/3000') AND (D.PositionCodeToEffectDate = '1/1/3000') AND " + ; 
					" (H.PayStatusToEffectDate = '1/1/3000') AND (H.BasePayInd = '1') AND (C.PositionToEffectDate = '1/1/3000') " + ;
					" ORDER BY CAST(G.EmpNo AS CHAR) "


*!*						" AND NOT (empno >= '3800' AND empno <= '3900') " + ;
*!*						" AND (NOT (CAST(G.EmpNo AS CHAR) >= '900' AND CAST(G.EmpNo AS CHAR) <= '998')) " + ;


				IF .lTestMode THEN
					.TrackProgress('', LOGIT+SENDIT)
					.TrackProgress('lcSQLADP =' + lcSQLADP, LOGIT+SENDIT)
					.TrackProgress('', LOGIT+SENDIT)
					.TrackProgress('lcSQLWTK =' + lcSQLWTK, LOGIT+SENDIT)
					.TrackProgress('', LOGIT+SENDIT)
					.TrackProgress('lcSQLKronosHR =' + lcSQLKronosHR, LOGIT+SENDIT)
				ENDIF

				.TrackProgress('Connecting to ADP System....', LOGIT+SENDIT)

*!*					* get data from ADP
*!*					OPEN DATABASE F:\UTIL\ADPCONNECTION\CONNECT1

				.nSQLHandle = SQLCONNECT("PCPAYWIN","MBENNETT","VP737A")

				IF .nSQLHandle > 0 THEN
					IF USED('CURADP') THEN
						USE IN CURADP
					ENDIF
					IF .ExecSQL(lcSQLADP, 'CURADP', RETURN_DATA_MANDATORY) THEN
						* CURADP HOLDS LIST OF ADP FILE #S
						* nothing else to do
IF .lTestMode THEN					
	SELECT CURADP
	BROWSE
ENDIF
					ENDIF
					=SQLDISCONNECT(.nSQLHandle)
				ELSE
					* connection error
					.TrackProgress('Unable to connect to ADP Payroll System.', LOGIT+SENDIT)
				ENDIF

				.TrackProgress('Connecting to WTK System....', LOGIT+SENDIT)

*!*					OPEN DATABASE F:\UTIL\KRONOS\KRONOSEDI

*!*					.nSQLHandle = SQLCONNECT("WTK_ON_PAYROLL2","sa","kr0n1tes")
				.nSQLHandle = SQLCONNECT("WTK2","sa","kr0n1tes")

				IF .nSQLHandle > 0 THEN

					IF USED('CURKRONOSWTK') THEN
						USE IN CURKRONOSWTK
					ENDIF
					IF USED('CURKRONOSWTK2') THEN
						USE IN CURKRONOSWTK2
					ENDIF

					IF .ExecSQL(lcSQLWTK, 'CURKRONOSWTK', RETURN_DATA_MANDATORY) THEN

						SELECT ;
							INT(VAL(FILE_NUM)) AS FILE_NUM, ;
							LEFT(ADP_COMP,3) AS ADP_COMP, ;
							NAME, ;
							LEFT((ALLTRIM(DIVISION) + ALLTRIM(DEPT)),6) AS HOMEDEPT, ;
							LEFT(WORKSITE,3) AS WORKSITE ;
							FROM CURKRONOSWTK ;
							INTO CURSOR CURKRONOSWTK2 ;
							ORDER BY FILE_NUM ;
							READWRITE

					ENDIF

IF .lTestMode THEN					
	SELECT CURKRONOSWTK2
	*APPEND blank
	BROWSE
ENDIF

					IF USED('CURKRONOSHR') THEN
						USE IN CURKRONOSHR
					ENDIF
					IF USED('CURKRONOSHR2') THEN
						USE IN CURKRONOSHR2
					ENDIF

					IF .ExecSQL(lcSQLKronosHR, 'CURKRONOSHR', RETURN_DATA_MANDATORY) THEN

						SELECT ;
							'Y' AS ISGOOD, ;
							INT(VAL(FILE_NUM)) AS FILE_NUM, ;
							SS_NUM, ;
							LASTNAME, ;
							FIRSTNAME, ;
							STREET1, ;
							STREET2, ;
							CITY, ;
							STATE, ;
							ZIPCODE, ;
							GENDER, ;
							RATE1AMT, ;
							RATETYPE, ;
							STATUS, ;
							TTOD(HIREDATE) AS HIREDATE, ;
							TTOD(DOB) AS DOB, ;
							LEFT(WORKSTATE,2) AS WORKSTATE, ;
							LEFT(SUISDI,2) AS SUISDI ;
							FROM CURKRONOSHR ;
							INTO CURSOR CURKRONOSHR2 ;
							ORDER BY FILE_NUM ;
							READWRITE

*!*								LEFT(ALLTRIM(ADP_COMP),3) AS ADP_COMP, ;

IF .lTestMode THEN					
	SELECT CURKRONOSHR2
	BROWSE
ENDIF

					ENDIF

					* NOW JOIN CURSORS TO CREATE FINAL ONE
					IF USED('CURMASTER') THEN
						USE IN CURMASTER
					ENDIF
					IF USED('CURMASTER2') THEN
						USE IN CURMASTER2
					ENDIF

					* STD QUERY
					SELECT ;
						B.ADP_COMP, ;
						B.HOMEDEPT, ;
						B.WORKSITE, ;
						A.* ;
						FROM CURKRONOSHR2 A ;
						INNER JOIN CURKRONOSWTK2 B ;
						ON B.FILE_NUM = A.FILE_NUM ;
						INTO CURSOR CURMASTER ;
						WHERE A.FILE_NUM NOT IN (SELECT FILE_NUM FROM CURADP) ;
						AND (A.HIREDATE >= ldOneMonthAgo) ;
						ORDER BY A.FILE_NUM ;
						READWRITE

*!*						* ACTIVATE FOR SPECIAL PROCESSING
*!*						SELECT ;
*!*							B.HOMEDEPT, ;
*!*							B.WORKSITE, ;
*!*							A.* ;
*!*							FROM CURKRONOSHR2 A ;
*!*							INNER JOIN CURKRONOSWTK2 B ;
*!*							ON B.FILE_NUM = A.FILE_NUM ;
*!*							INTO CURSOR CURMASTER ;
*!*							WHERE A.FILE_NUM NOT IN (SELECT FILE_NUM FROM CURADP) ;
*!*							AND INLIST(A.FILE_NUM, 3982, 3983, 3984) ;
*!*							ORDER BY A.FILE_NUM ;
*!*							READWRITE

IF .lTestMode THEN					
	SELECT CURMASTER
	BROWSE
ENDIF

					SELECT CURMASTER
					SCAN
						*!*							* Clean up NULLS
						*!*							IF ISNULL(CURMASTER.STREET2) THEN
						*!*								REPLACE CURMASTER.STREET2 WITH " "
						*!*							ENDIF
						* change rate1amt TO BIWEEKLY for salaried; DETERMINE RATETYPE
						IF INLIST(CURMASTER.ADP_COMP,"ZXU","AXA") THEN
							REPLACE CURMASTER.RATE1AMT WITH (CURMASTER.RATE1AMT * 80), CURMASTER.RATETYPE WITH "S"
						ELSE
							REPLACE CURMASTER.RATETYPE WITH "H"
						ENDIF
						* adjustments for OTR drivers
						IF CURMASTER.HOMEDEPT = "020655" THEN
							REPLACE CURMASTER.RATE1AMT WITH 0.00, CURMASTER.RATETYPE WITH "N"
						ENDIF
						* populate Worked State and SUI/SDI codes
						lcSiteState = LEFT(CURMASTER.WORKSITE,2)
						* change SP1 or SP2 to CA
						IF lcSiteState = "SP" THEN
							lcSiteState = "CA"
						ENDIF
						* handle VR1 worksite
						IF lcSiteState = "VR" THEN
							lcSiteState = CURMASTER.WORKSTATE
						ENDIF
						* 
						lcSUISDI = GetSUISDIFromState(lcSiteState)
						*
						IF EMPTY(lcSUISDI) THEN
							* we had invalid/unexpected worksite; log error
							lcEmployeeIdentifier = CURMASTER.ADP_COMP + " " + TRANSFORM(CURMASTER.FILE_NUM) + " :  " + ;
								ALLTRIM(CURMASTER.LASTNAME) + ", " + ALLTRIM(CURMASTER.FIRSTNAME)
							.cErrorsText = .cErrorsText + 'WARNING: invalid worksite [' + CURMASTER.WORKSITE + '] for ' + lcEmployeeIdentifier + CRLF + ;
								'Please review Workstate and SUISDI Code in ADP after importing!' + CRLF + CRLF
								
							* since we had invalid workstate based on wtk worksite, use the workstate from HR to try and get suisdi
							lcSUISDI = GetSUISDIFromState(CURMASTER.WORKSTATE)
							IF NOT EMPTY(lcSUISDI) THEN
								REPLACE CURMASTER.SUISDI WITH lcSUISDI
							ENDIF
							
						ELSE
							* okay, replace the workstate and suisdi from HR
							REPLACE CURMASTER.WORKSTATE WITH lcSiteState, CURMASTER.SUISDI WITH lcSUISDI
						ENDIF
						
						* make sure neither of the street fields is > 20 chars, to prevent errors loading into ADP
						lcBothStreets = ALLTRIM(CURMASTER.STREET1) + " " + ALLTRIM(CURMASTER.STREET2)
						IF LEN(lcBothStreets) > 40 THEN
							lcBothStreets = LEFT(lcBothStreets,40)
						ENDIF
						lcBothStreets = PADR(lcBothStreets,40,' ')
						REPLACE CURMASTER.STREET1 WITH LEFT(lcBothStreets,20), CURMASTER.STREET2 WITH RIGHT(lcBothStreets,20)						
						
					ENDSCAN

if .ltestmode then					
	select curmaster
	browse
endif

*SET STEP ON 					
					* LOG ERRORS
					SELECT CURMASTER
					LOCATE
					
					SELECT CURMASTER
					SCAN
						IF NOT .CheckField( 'CURMASTER.ADP_COMP' ) THEN
							REPLACE CURMASTER.ISGOOD WITH 'N' IN CURMASTER
						ENDIF
						IF NOT .CheckField( 'CURMASTER.FILE_NUM' ) THEN
							REPLACE CURMASTER.ISGOOD WITH 'N' IN CURMASTER
						ENDIF
						IF NOT .CheckField( 'CURMASTER.HOMEDEPT' ) THEN
							REPLACE CURMASTER.ISGOOD WITH 'N' IN CURMASTER
						ENDIF
						IF NOT .CheckField( 'CURMASTER.WORKSITE' ) THEN
							REPLACE CURMASTER.ISGOOD WITH 'N' IN CURMASTER
						ENDIF
						IF NOT .CheckField( 'CURMASTER.SS_NUM' ) THEN
							REPLACE CURMASTER.ISGOOD WITH 'N' IN CURMASTER
						ENDIF
						IF NOT .CheckField( 'CURMASTER.LASTNAME' ) THEN
							REPLACE CURMASTER.ISGOOD WITH 'N' IN CURMASTER
						ENDIF
						IF NOT .CheckField( 'CURMASTER.FIRSTNAME' ) THEN
							REPLACE CURMASTER.ISGOOD WITH 'N' IN CURMASTER
						ENDIF
						IF NOT .CheckField( 'CURMASTER.STREET1' ) THEN
							REPLACE CURMASTER.ISGOOD WITH 'N' IN CURMASTER
						ENDIF
						IF NOT .CheckField( 'CURMASTER.CITY' ) THEN
							REPLACE CURMASTER.ISGOOD WITH 'N' IN CURMASTER
						ENDIF
						IF NOT .CheckField( 'CURMASTER.STATE' ) THEN
							REPLACE CURMASTER.ISGOOD WITH 'N' IN CURMASTER
						ENDIF
						IF NOT .CheckField( 'CURMASTER.ZIPCODE' ) THEN
							REPLACE CURMASTER.ISGOOD WITH 'N' IN CURMASTER
						ENDIF
						IF NOT .CheckField( 'CURMASTER.GENDER' ) THEN
							REPLACE CURMASTER.ISGOOD WITH 'N' IN CURMASTER
						ENDIF
						IF CURMASTER.HOMEDEPT <> "020655" THEN
							IF NOT .CheckField( 'CURMASTER.RATE1AMT' ) THEN
								REPLACE CURMASTER.ISGOOD WITH 'N' IN CURMASTER
							ENDIF
						ENDIF
						IF NOT .CheckField( 'CURMASTER.RATETYPE' ) THEN
							REPLACE CURMASTER.ISGOOD WITH 'N' IN CURMASTER
						ENDIF
						IF NOT .CheckField( 'CURMASTER.SUISDI' ) THEN
							REPLACE CURMASTER.ISGOOD WITH 'N' IN CURMASTER
						ENDIF
					ENDSCAN
					.cErrorsText = .cErrorsText + CRLF
					
IF .lTestMode THEN					
	SELECT CURMASTER
	BROWSE
ENDIF

					WAIT WINDOW NOWAIT "Processing AXA..."
					.ProcessKronosOutputFiles( "AXA" )

					WAIT WINDOW NOWAIT "Processing SXI..."
					.ProcessKronosOutputFiles( "SXI" )

					WAIT WINDOW NOWAIT "Processing ZXU..."
					.ProcessKronosOutputFiles( "ZXU" )

					* delete RESTART FILES IN ADP FOLDER, IF THEY EXIST
					IF FILE(.cADPPath + "RESTART.PRAXAEMP.CSV") THEN
						DELETE FILE (.cADPPath + "RESTART.PRAXAEMP.CSV")
					ENDIF
					IF FILE(.cADPPath + "RESTART.PRSXIEMP.CSV") THEN
						DELETE FILE (.cADPPath + "RESTART.PRSXIEMP.CSV")
					ENDIF
					IF FILE(.cADPPath + "RESTART.PRZXUEMP.CSV") THEN
						DELETE FILE (.cADPPath + "RESTART.PRZXUEMP.CSV")
					ENDIF


					* construct text for top of email
					IF .nNumberOfOutputFiles > 0 THEN
						IF NOT EMPTY(.cNewHireList) THEN
							.cTopEmailText = "New Employees have been exported from Kronos HR:" + CRLF + CRLF + ;
								.cNewHireList + CRLF + ;
								"Please import employee info into ADP." + CRLF + CRLF + "<process log follows>" + CRLF + CRLF
						ELSE
							.cTopEmailText = "New Employee Information has been exported from Kronos HR." + CRLF + CRLF + ;
								"Please import employee info into ADP." + CRLF + CRLF + "<process log follows>" + CRLF + CRLF
						ENDIF
					ELSE
						.cTopEmailText = "No new employees were found!" + CRLF + ;
							"No Kronos HR --> ADP Export files were created." + CRLF + CRLF + ;
							"<process log follows>" + CRLF + CRLF
					ENDIF

					.cBodyText = .cTopEmailText + .cErrorsText + .cBodyText

					=SQLDISCONNECT(.nSQLHandle)
					
				ELSE
					* connection error
					.TrackProgress('Unable to connect to WTK System.', LOGIT+SENDIT)
				ENDIF   &&  .nSQLHandle > 0


			CATCH TO loError

				.TrackProgress('There was an error.',LOGIT+SENDIT)
				.TrackProgress('The Error # is: ' + TRANSFORM(loError.ERRORNO), LOGIT+SENDIT)
				.TrackProgress('The Error Message is: ' + TRANSFORM(loError.MESSAGE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred in Program: ' + TRANSFORM(loError.PROCEDURE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred at line #: ' + TRANSFORM(loError.LINENO), LOGIT+SENDIT)
				lnNumberOfErrors = lnNumberOfErrors + 1
				IF TYPE('oExcel') = 'O' AND NOT ISNULL(oExcel) THEN
					oExcel.QUIT()
				ENDIF
				CLOSE DATA
				CLOSE ALL

			ENDTRY

			CLOSE DATA
			WAIT CLEAR
			***************** INTERNAL email results ******************************
			.TrackProgress('About to send status email.',LOGIT)
			.TrackProgress('==================================================================================================================', SENDIT)
			.TrackProgress('The logfile is: ' + .cLogFile, SENDIT)
			.TrackProgress('Kronos To ADP Export process started: ' + .cStartTime, LOGIT+SENDIT)
			.TrackProgress('Kronos To ADP Export process finished: ' + TTOC(DATETIME()), LOGIT+SENDIT)

			IF .lSendInternalEmailIsOn THEN
				* try to trap error from not having dartmail dll's registered on user's pc...
				TRY
					DO FORM dartmail2 WITH .cSendTo,.cFrom,.cSubject,.cCC,.cAttach,.cBodyText,"A"
					.TrackProgress('Sent status email.',LOGIT)
					
					*!*	IF NOT EMPTY(.cExpressHireList) then
					*!*		.cBodyText = .cExpressHireList
					*!*		DO FORM dartmail2 WITH .cExpressSendTo,.cFrom,.cSubject,.cExpressCC,.cAttach,.cBodyText,"A"
					*!*		.TrackProgress('Sent status email.',LOGIT)
					*!*	ENDIF
					
				CATCH TO loError
					=MESSAGEBOX('There was an error sending the status email. Please contact Mark Bennett.',0+64,"Kronos To ADP Export Report")
					.TrackProgress('There was an error sending the status email.',LOGIT)
					.TrackProgress('The Error # is: ' + TRANSFORM(loError.ERRORNO), LOGIT)
					.TrackProgress('The Error Message is: ' + TRANSFORM(loError.MESSAGE), LOGIT)
					.TrackProgress('The Error occurred in Program: ' + TRANSFORM(loError.PROCEDURE), LOGIT)
					.TrackProgress('The Error occurred at line #: ' + TRANSFORM(loError.LINENO), LOGIT)
					lnNumberOfErrors = lnNumberOfErrors + 1
					CLOSE DATA
				ENDTRY
			ELSE
				.TrackProgress('Did not send status email: THIS.lSendInternalEmailIsOn = FALSE.',LOGIT)
			ENDIF

		ENDWITH
		RETURN
	ENDFUNC && main


	FUNCTION CheckField
		LPARAMETERS tcField
		* ASSUMES CURMASTER IS OPEN AND SELECTED
		LOCAL lcEmployeeIdentifier, llRetVal
		llRetVal = .T.
		IF EMPTY(&tcField) THEN
			lcEmployeeIdentifier = ALLTRIM(CURMASTER.ADP_COMP) + " " + TRANSFORM(CURMASTER.FILE_NUM) + " :  " + ALLTRIM(CURMASTER.LASTNAME) + ", " + ALLTRIM(CURMASTER.FIRSTNAME)
			.cErrorsText = .cErrorsText + 'ERROR: empty [' + tcField + '] for ' + lcEmployeeIdentifier + CRLF
			llRetVal = .F.
		ENDIF
		RETURN llRetVal
	ENDFUNC


	PROCEDURE ProcessKronosOutputFiles
		LPARAMETERS tcADPCOMP
		WITH THIS
			LOCAL lcOutputFilename, lcOutputLine, lcTargetArchivedFilename, llWroteOutputHeader, lcEmployeeIdentifier

			llWroteOutputHeader = .F.
			.nCurrentOutputCSVFileHandle = -1

			SELECT CURMASTER
			SCAN FOR (ADP_COMP == tcADPCOMP) AND (ISGOOD = 'Y')

				IF NOT llWroteOutputHeader THEN
					.CreateOutputCSVFile(tcADPCOMP)
					llWroteOutputHeader = .WriteOutputHeader()
				ENDIF

				lcEmployeeIdentifier = tcADPCOMP + " " + TRANSFORM(CURMASTER.FILE_NUM) + " :  " + ;
					ALLTRIM(CURMASTER.LASTNAME) + ", " + ALLTRIM(CURMASTER.FIRSTNAME) + CRLF
					
				.cNewHireList = .cNewHireList + lcEmployeeIdentifier
				
				*!*	* build express hire list
				*!*	IF CURMASTER.HOMEDEPT == EXPRESSHOMEDEPT THEN
				*!*		.cExpressHireList = .cExpressHireList + lcEmployeeIdentifier
				*!*	ENDIF

				* Write Detail Line

				lcOutputLine = ;
					GetNewADPCompany(.CleanUpField(CURMASTER.ADP_COMP)) + "," + ;
					.CleanUpField(CURMASTER.FILE_NUM) + "," + ;
					.CleanUpField(CURMASTER.SS_NUM) + "," + ;
					.CleanUpField(CURMASTER.LASTNAME) + "," + ;
					.CleanUpField(CURMASTER.FIRSTNAME) + "," + ;
					.CleanUpField(CURMASTER.STREET1) + "," + ;
					.CleanUpField(CURMASTER.STREET2) + "," + ;
					.CleanUpField(CURMASTER.CITY) + "," + ;
					.CleanUpField(CURMASTER.STATE) + "," + ;
					.CleanUpField(CURMASTER.ZIPCODE) + "," + ;
					.CleanUpField(CURMASTER.GENDER) + "," + ;
					.CleanUpField(CURMASTER.HOMEDEPT) + "," + ;
					.CleanUpField(CURMASTER.RATETYPE) + "," + ;
					.CleanUpField(ALLTRIM(STR(CURMASTER.RATE1AMT,9,2))) + "," + ;
					.CleanUpField(CURMASTER.STATUS) + "," + ;
					.CleanUpField(TRANSFORM(CURMASTER.HIREDATE)) + "," + ;
					.CleanUpField(TRANSFORM(CURMASTER.DOB)) + "," + ;
					.CleanUpField(CURMASTER.WORKSTATE) + "," + ;
					.CleanUpField(CURMASTER.SUISDI)

				=FPUTS(.nCurrentOutputCSVFileHandle,lcOutputLine)

				*** removed tax jurisdiction per Lucille 5/8/08 MB because they were not always right
				*** .CleanUpField(lcSUISDITAXJURISCD)

			ENDSCAN

			* close output file
			.CloseOutputCSVFile()

			* copy output file (renamed so it is unique) to TARGETARCHIVED folder on F:
			lcOutputFilename = .cADPPath + "PR" + GetNewADPCompany(tcADPCOMP) + "EMP.CSV"
			lcTargetArchivedFilename = .cArchiveTargetCSVPath + "PR" + GetNewADPCompany(tcADPCOMP) + "EMP_" + .cFileDate + ".CSV"
			IF FILE(lcOutputFilename) THEN
				COPY FILE (lcOutputFilename) TO (lcTargetArchivedFilename)
			ENDIF

			.TrackProgress('=========================================================', LOGIT+SENDIT)
		ENDWITH
	ENDPROC


	FUNCTION CleanUpField
		* prepare field for inclusion in the string to be written with FPUTS().
		* especially critical to remove any commas, since that creates bogus column in CSV output file.
		LPARAMETERS tuValue
		LOCAL lcRetVal
		lcRetVal = STRTRAN(ALLTRIM(TRANSFORM(tuValue)),",","")
		RETURN lcRetVal
	ENDFUNC


	FUNCTION CreateOutputCSVFile
		LPARAMETERS tcADPCOMP
		WITH THIS
			LOCAL lcOutputFilename, llRetval
			lcOutputFilename = .cADPPath + "PR" + GetNewADPCompany(tcADPCOMP) + "EMP.CSV"
			llRetval = .T.
			.nCurrentOutputCSVFileHandle = FCREATE(lcOutputFilename)
			IF .nCurrentOutputCSVFileHandle < 0 THEN
				.TrackProgress('***** Error on FCREATE of: ' + lcOutputFilename, LOGIT+SENDIT)
				llRetval = .F.
			ELSE
				.TrackProgress('FCREATEed: ' + lcOutputFilename, LOGIT+SENDIT)
				.nNumberOfOutputFiles = .nNumberOfOutputFiles + 1
				DO CASE
					CASE tcADPCOMP = "AXA"
						.lOutputAXAFile = .T.
					CASE tcADPCOMP = "SXI"
						.lOutputSXIFile = .T.
					CASE tcADPCOMP = "ZXU"
						.lOutputZXUFile = .T.
					OTHERWISE
						* nothing
				ENDCASE
			ENDIF
			.TrackProgress('nCurrentOutputCSVFileHandle = ' + TRANSFORM(.nCurrentOutputCSVFileHandle), LOGIT+SENDIT)
		ENDWITH
		RETURN llRetval
	ENDFUNC


	FUNCTION WriteOutputHeader
		WITH THIS
			LOCAL llRetval, lnBytesWritten
			* note: no CRLF on first part of header line
			llRetval = .T.
			lnBytesWritten = FWRITE(.nCurrentOutputCSVFileHandle,.cOutputCSVHeaderLine1)
			.TrackProgress('FWROTE # of bytes: ' + TRANSFORM(lnBytesWritten), LOGIT+SENDIT)
			lnBytesWritten = FWRITE(.nCurrentOutputCSVFileHandle,.cOutputCSVHeaderLine2 + CRLF)
			.TrackProgress('FWROTE # of bytes: ' + TRANSFORM(lnBytesWritten), LOGIT+SENDIT)
		ENDWITH
		RETURN llRetval
	ENDFUNC


	FUNCTION CloseOutputCSVFile
		WITH THIS
			LOCAL llRetval
			llRetval = .T.
			IF .nCurrentOutputCSVFileHandle > -1 THEN
				llRetval = FCLOSE(.nCurrentOutputCSVFileHandle)
			ENDIF
		ENDWITH
		RETURN llRetval
	ENDFUNC


	FUNCTION ExecSQL
		LPARAMETERS tcSQL, tcCursorName, tlNoDataReturnedIsOkay
		LOCAL llRetval, lnResult
		WITH THIS
			* close target cursor if it's open
			IF USED(tcCursorName)
				USE IN (tcCursorName)
			ENDIF
			WAIT WINDOW "Fetching remote data; please wait..." NOWAIT
			lnResult = SQLEXEC(.nSQLHandle, tcSQL, tcCursorName)
			llRetval = ( lnResult > 0 )
			IF llRetval THEN
				* see if any data came back
				IF NOT tlNoDataReturnedIsOkay THEN
					IF (NOT USED(tcCursorName)) OR EOF(tcCursorName) THEN
						llRetval = .F.
						.TrackProgress('No data was returned by this query: ' +CRLF + tcSQL, LOGIT+SENDIT)
					ENDIF
				ENDIF
			ELSE
				.TrackProgress("Result Code [" + TRANSFORM(lnResult) + "] returned by this query: " + tcSQL, LOGIT+SENDIT)
			ENDIF
			WAIT CLEAR
			RETURN llRetval
		ENDWITH
	ENDFUNC


	PROCEDURE TrackProgress
		* do any combination of Wait Window, writing to logfile, and adding to body of email,
		* based on nFlags parameter.
		LPARAMETERS tcExpression, tnFlags
		WITH THIS
			IF BITAND(tnFlags,LOGIT) = LOGIT THEN
				IF .lLoggingIsOn THEN
					?
					? .cProcessName + "  " + TTOC(DATETIME()) + ": " + tcExpression
				ENDIF
			ENDIF
			IF .lWaitWindowIsOn THEN
				IF BITAND(tnFlags,NOWAITIT) = NOWAITIT THEN
					WAIT WINDOW tcExpression NOWAIT
				ENDIF
				IF BITAND(tnFlags,WAITIT) = WAITIT THEN
					WAIT WINDOW tcExpression TIMEOUT .nWaitWindowTimeout
				ENDIF
			ENDIF
			IF BITAND(tnFlags,SENDIT) = SENDIT THEN
				IF .lSendInternalEmailIsOn THEN
					.cBodyText = .cBodyText + tcExpression + CRLF + CRLF
				ENDIF
			ENDIF
		ENDWITH
	ENDPROC  &&  TrackProgress

ENDDEFINE

