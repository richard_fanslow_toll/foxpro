*!*	This project is ONLY used for TKO inbounds

PARAMETERS cOffice
PUBLIC m.accountid,NormalExit,cUseFolder,tsendto,tsendtoerr,tcc,tccerr,cTransfer,lDoMail,cCustName
PUBLIC lDoSQL,cWhseMod,lPrepack,cStylePO

CLOSE DATABASES ALL

TRY
	lTesting = .f.
	lTestinput = lTesting
	NormalExit = .F.
	DO m:\dev\prg\_setvars WITH .T.
	lDoMail = .T.
	cStylePO = ""
	lOverrideBusy = .t.

	_SCREEN.WINDOWSTATE=IIF(lTesting or lOverrideBusy,2,1)

	IF VARTYPE(cOffice) # "C"
		cOffice = "C"  && "N","M"
*		cOffice = ALLTRIM(STR(cOffice))
	ENDIF

	SELECT 0
	USE F:\edirouting\ftpsetup SHARED
	cTransfer = "PL-TKO-"
	LOCATE FOR ftpsetup.transfer = cTransfer
	IF ftpsetup.chkbusy AND !lOverrideBusy
		WAIT WINDOW "Transfer in process...exiting" TIMEOUT 1
		CLOSE DATA ALL
		CANCEL
		RETURN
	ELSE
		REPLACE chkbusy WITH .T.,trig_time WITH DATETIME()  FOR ftpsetup.transfer = cTransfer
	ENDIF
	USE IN ftpsetup

	nAcctNum = IIF(INLIST(cOffice,"N","I"),6705,5836)
	IF lTestinput
		WAIT WINDOW "Running as TEST" TIMEOUT 2
		cUseFolder = "F:\WHP\WHDATA\"
	ELSE
		xReturn = "XXX"
		wf(cOffice,nAcctNum)
		cUseFolder = UPPER(xReturn)
		WAIT WINDOW "Using folder "+cUseFolder TIMEOUT 2
	ENDIF

	cMod = ICASE(cOffice = "C","2",cOffice = "N","I",cOffice)
	cOffice = ICASE(ISDIGIT(cOffice),"C",cOffice = "I","N",cOffice)
	gOffice = cMod
	gMasteroffice = cOffice
	cWhseMod = LOWER("wh"+cMod)

	SELECT 0
	USE F:\3pl\DATA\mailmaster ALIAS MM
	LOCATE FOR MM.AcctName = "TKO " AND MM.office = cOffice AND MM.edi_type = "PL"
	IF FOUND()
		STORE TRIM(MM.AcctName) TO lcAccountname
		STORE TRIM(MM.basepath) TO lcPath
		IF lTesting
			WAIT WINDOW "File path is "+lcPath TIMEOUT 2
		ENDIF
		STORE TRIM(MM.archpath) TO lcArchivePath
		tsendto = IIF(MM.use_alt,sendtoalt,sendto)
		tcc = IIF(MM.use_alt,ccalt,cc)
		STORE TRIM(MM.fmask)    TO filemask
		STORE TRIM(MM.scaption) TO _SCREEN.CAPTION
		LOCATE
		LOCATE FOR edi_type = "MISC" AND taskname = "JOETEST"
		tsendtotest = IIF(MM.use_alt,sendtoalt,sendto)
		tcctest = IIF(MM.use_alt,ccalt,cc)
		LOCATE
		LOCATE FOR edi_type = "MISC" AND taskname = "GENERAL"
		tsendtoerr = IIF(MM.use_alt,sendtoalt,sendto)
		tccerr = IIF(MM.use_alt,ccalt,cc)
		USE IN MM
	ELSE
		WAIT WINDOW AT 10,10  "No parameters set for this acct# "+ALLTRIM(STR(Acctnum))+"  ---> Office "+lcOffice TIMEOUT 5
		THROW
	ENDIF

	tsendto = IIF(lTesting,tsendtotest,tsendto)
	tcc = IIF(lTesting,tcctest,tcc)

*USE (cUseFolder+"whgenpk") IN 0 ALIAS whgenpk
	useca("inwolog","wh")

	IF !lTesting
		useca("pl","wh")
	ELSE
		USE (cUseFolder+"pl") IN 0 ALIAS pl
	ENDIF

	xsqlexec("select * from account where inactive=0","account",,"qq")
	INDEX ON accountid TAG accountid
	SET ORDER TO

	cCustName = ""
	cPnp = ""
	lCtnAcct = ""

	cInfolder = IIF(lTesting,"f:\ftpusers\tko-"+IIF(cOffice = "C","ca","nj")+"\inbounds\fmitest\",lcPath)
	cArchfolder = lcArchivePath
	CD [&cInfolder]
	len1 = ADIR(ary1,"*.xls")

	cfilenamexx = (cInfolder+"temp1.xls")

	m.inwologid = 0
	m.wo_num = 0
	FOR ee = 1 TO len1
		xsqlexec("select * from inwolog where .f.","xinwolog",,"wh")

		SELECT xinwolog
		INDEX ON inwologid TAG inwologid

		IF !lTesting
			xsqlexec("select * from pl where .f.","xpl",,"wh")
		ELSE
			SELECT * FROM pl WHERE .F. INTO CURSOR xpl READWRITE
		ENDIF

		SELECT 0
		CREATE CURSOR intake (f1 c(50),f2 c(30),f3 c(30),f4 c(30),f5 c(30),f6 c(30),f7 c(30),f8 c(30),f9 c(30))
		IF FILE(cfilenamexx)
			DELETE FILE [&cfilenamexx]
		ENDIF
		cfilename = ALLTRIM(ary1[ee,1])
		cfullname = (cInfolder+cfilename)
		oExcel = CREATEOBJECT("Excel.Application")
		oWorkbook = oExcel.Workbooks.OPEN(cfullname)
		WAIT WINDOW "Now saving file "+cfilename NOWAIT NOCLEAR
		SET SAFETY OFF
		oWorkbook.SAVEAS(cfilenamexx,20) && re-saves Excel sheet as text file
*		oWorkbook.SAVEAS(cfilenamexx,39)  && re-saves Excel sheet as XL95/97 file
		WAIT CLEAR
		WAIT WINDOW "File save complete...continuing" TIMEOUT 1
		oWorkbook.CLOSE(.T.)
		oExcel.QUIT()
		RELEASE oExcel

		SELECT intake
		APPEND FROM [&cfilenamexx] TYPE DELIMITED WITH TAB
		DELETE FILE [&cfilenamexx]
		GO BOTT
		DO WHILE EMPTY(intake.f1)
			SKIP -1
		ENDDO
		nRec1 = RECNO('intake')
		DELETE FOR RECNO()>nRec1

		IF !lTesting
			IF cOffice = "C"
				LOCATE FOR UPPER(intake.f1) = "WAREHOUSE"
				IF UPPER(ALLTRIM(intake.f2)) = "FMINJ"
					COPY FILE [&cfullname] TO ("F:\FTPUSERS\TKO-NJ\Inbounds\"+cfilename)
					DELETE FILE [&cFullname]
					LOOP
				ENDIF

				IF UPPER(ALLTRIM(intake.f2)) = "FMIFL"
					COPY FILE [&cfullname] TO ("F:\FTPUSERS\TKO-FL\Inbounds\"+cfilename)
					DELETE FILE [&cFullname]
					LOOP
				ENDIF
			ENDIF
		ENDIF

		SELECT intake
		cArchfilename = (cArchfolder+cfilename)
		COPY FILE [&cfullname] TO [&carchfilename]

		LOCATE
		LOCATE FOR UPPER(intake.f6) = "DIST"
		SKIP 1 IN intake
		lPrepack = IIF(LEFT(intake.f6,1) = "P",.T.,.F.)
		lMB = IIF(LEFT(intake.f3,1) = "M",.t.,.f.)

		LOCATE
		LOCATE FOR UPPER(intake.f1) = "NOTES"
		lPrepack = IIF(UPPER(ALLTRIM(intake.f2))="CASEPACK",.t.,lPrepack)
		
		nAcctNum = IIF(lPrepack,5836,5865)
		nAcctNum = IIF(lMB,6705,nAcctNum)
		m.accountid = nAcctNum
		WAIT WINDOW "This is a "+IIF(lPrepack,"PREPACK","PnP")+" packing list" TIMEOUT 2

		SELECT account
		=SEEK(nAcctNum,'account','accountid')
		IF FOUND()
			cCustName = ALLTRIM(account.AcctName)
		ELSE
			cCustName = "NA"
		ENDIF
		IF cCustName = "NA"
			WAIT WINDOW "Houston, we have a problem..." TIMEOUT 2
			THROW
		ENDIF

		SELECT intake
		IF lTesting
			LOCATE
*			BROWSE
		ENDIF
		m.inwologid = m.inwologid +1
		m.wo_num = m.wo_num +1
		m.wo_date = DATE()
		m.date_rcvd = DATE()
		m.picknpack = .F.
		m.plid = 0
		m.AcctName = cCustName
		m.addby = "TOLLPROC"
		m.adddt = DATETIME()
		m.addproc = "TKO_INB"

		REPLACE ALL f1 WITH UPPER(f1) IN intake
		REPLACE ALL f4 WITH UPPER(f4) IN intake
		REPLACE ALL f5 WITH UPPER(f5) IN intake
		REPLACE ALL f3 WITH UPPER(f3) IN intake

		LOCATE
		LOCATE FOR UPPER(intake.f1) = "CONTAINER"
		m.container = ALLTRIM(intake.f2)
		m.comments = "FILENAME*"+cfilename
		m.addby = "TOLLPROC"
		m.adddt = DATETIME()
		m.addproc = "TKOPL"
		INSERT INTO xinwolog FROM MEMVAR

		LOCATE FOR "CARTON"$UPPER(intake.f2)
		IF !FOUND()
			WAIT WINDOW "CARTON cell not found!" TIMEOUT 10
			CLOSE DATA ALL
			THROW
		ENDIF

		nRec1 = RECNO()
		cVPO = "XYZ"
		SELECT DISTINCT f2 AS ctnnum FROM intake WHERE RECNO()>nRec1 AND !EMPTY(f2) INTO CURSOR tempctn
		SELECT DISTINCT f1 AS ponum FROM intake WHERE RECNO()>nRec1 AND !EMPTY(f1) INTO CURSOR tempponum
		SELECT tempponum
		SCAN
			REPLACE xinwolog.comments WITH xinwolog.comments+CHR(13)+"PONUM*"+ALLTRIM(tempponum.ponum) IN xinwolog
		ENDSCAN
		
*		lPrepack = IIF(ALLTRIM(m.Container) = 'NYKU5729479',.t.,lPrepack)
		IF lPrepack
			tko_prepackbkdn()
		ELSE
			tko_pnpbkdn()
		ENDIF

		IF lTesting
			SELECT xinwolog
			LOCATE
			BROWSE
			SELECT xpl
			LOCATE
			BROWSE
			CANCEL
		ENDIF

		DO m:\dev\prg\tko_inbound_import
		IF !lTesting
			DELETE FILE [&cfullname]
		ENDIF

	ENDFOR
	NormalExit = .T.

CATCH TO oErr
	IF !NormalExit
		SET STEP ON
		lnRec = 0
		ptError = .T.
		tsubject = "Inbound Upload Error ("+TRANSFORM(oErr.ERRORNO)+") for "+cCustName+" at "+TTOC(DATETIME())+"  on file "+cfilename
		tattach  = ""
		tmessage = "Error processing Inbound file,GENERIC INBOUND.... Filename: "+cfilename+CHR(13)
		tmessage = tmessage+CHR(13)+" Error at record "+TRANSFORM(lnRec)+CHR(13)+CHR(13)
		tmessage =tmessage+ "Try/CATCH Exeception Message:"+CHR(13)+;
			[  Error: ] + STR(oErr.ERRORNO) +CHR(13)+;
			[  LineNo: ] + STR(oErr.LINENO) +CHR(13)+;
			[  Message: ] + oErr.MESSAGE +CHR(13)+;
			[  Procedure: ] + oErr.PROCEDURE +CHR(13)+;
			[  Details: ] + oErr.DETAILS +CHR(13)+;
			[  StackLevel: ] + STR(oErr.STACKLEVEL) +CHR(13)+;
			[  LineContents: ] + oErr.LINECONTENTS+CHR(13)+;
			[  UserValue: ] + oErr.USERVALUE+CHR(13)+;
			[  Machine Name: ] + SYS(0)+CHR(13)+;
			[  Error Message :]+CHR(13)

		tsubject = "Inbound WO Error for "+cCustName+" at "+TTOC(DATETIME())+"  on file "+cfilename
		tattach  = ""
		tsendto = tsendtoerr
		tcc = tccerr

		tFrom    ="Toll WMS Inbound EDI System Operations <toll-edi-ops@tollgroup.com>"
		DO FORM m:\dev\frm\dartmail2 WITH tsendto,tFrom,tsubject,tcc,tattach,tmessage,"A"
		lCreateErr = .T.
	ENDIF
FINALLY
	WAIT WINDOW "All "+cCustName+" Inbound Complete...Exiting" TIMEOUT 2
	SELECT 0
	USE F:\edirouting\ftpsetup SHARED
	REPLACE chkbusy WITH .F. FOR ftpsetup.transfer = cTransfer
	USE IN ftpsetup
	CLOSE DATABASES ALL
ENDTRY
