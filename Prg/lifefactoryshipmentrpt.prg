
************************************************************************
* create csv file of daily Lifefactory shipment info
************************************************************************
*
* Build exe as F:\UTIL\LF\LIFEFACTORYSHIPMENTRPT.EXE
*
************************************************************************

LOCAL loLFShipmentRptProc, lnError, lcProcessName

utilsetup("LIFEFACTORYSHIPMENTRPT")

DECLARE INTEGER CreateMutex IN Win32API ;
	STRING @lpMutexAttributes, ;
	STRING bInitialOwner, ;
	STRING @lpName

DECLARE INTEGER GetLastError IN Win32API

* if running in EXE check to see if already running
IF _VFP.STARTMODE > 0 THEN
	lcProcessName = "LFShipmentRptProc"
	CreateMutex(0,CHR(1),@lcProcessName)
	lnError = GetLastError()
	IF lnError = 183
		WAIT WINDOW TIMEOUT 5 "This program is already running..."
		schedupdate()
		RETURN .F.
	ENDIF
ENDIF

loLFShipmentRptProc = CREATEOBJECT('LFShipmentRptProc')
loLFShipmentRptProc.MAIN()

schedupdate()

CLOSE DATABASES ALL

RETURN

#DEFINE LOGIT 1
#DEFINE WAITIT 2
#DEFINE NOWAITIT 4
#DEFINE SENDIT 8
#DEFINE CR CHR(13)
#DEFINE LF CHR(10)
#DEFINE CRLF CHR(13) + CHR(10)
#DEFINE RETURN_DATA_MANDATORY .F.
#DEFINE RETURN_DATA_NOT_MANDATORY .T.
#DEFINE CSVFORMAT 6
#DEFINE WRITE_ONLY_UNBUFFERED 11
#DEFINE COMMA ","

DEFINE CLASS LFShipmentRptProc AS CUSTOM

	cProcessName = 'LFShipmentRptProc'

	lTestMode = .F.  && .F. FOR NORMAL OPERATION, .T. TO SUPPRESS ALL BUT DEVELOPER EMAILS AND CHANGE INPUT TABLE, ETC.

	lAutoYield = .T.

	* date time props
	dToday = DATE()
	*dToday = {^2017-03-02}

	cStartTime = TTOC(DATETIME())

	* file/folder properties
	cOutputFolder = 'F:\UTIL\LF\REPORTS\'   && 'F:\FTPUSERS\LIFEFACTORY\945OUT\'
	cWHDataFolder = ''

	* processing properties
	cErrors = ''
	cSummaryInfo = ''
	nNumberOfErrors = 0

	* edi properties
	
	* wait window properties
	nWaitWindowTimeout = 2
	lWaitWindowIsOn = .T.

	* logfile properties
	lLoggingIsOn = .T.
	cLogFile = 'F:\UTIL\LF\LOGFILES\LFShipmentRptProc_log.txt'

	* INTERNAL email properties
	lSendInternalEmailIsOn = .T.
	cFrom = 'TGF Corporate <fmi-transload-ops@fmiint.com>'
	*cSendTo = 'ethan@lifefactory.com, lee@lifefactory.com, orders@lifefactory.com'
	cSendTo = 'opsreport@lifefactory.com, Greg@lifefactory.com'
	*cSendTo = 'susan@lifefactory.com, orders@lifefactory.com, Jason@lifefactory.com, Gina@lifefactory.com, ethan@lifefactory.com'
*	cSendTo = 'chris.malcolm@tollgroup.com'
	cCC = 'mbennett@fmiint.com'
*	ccc = "chris.malcolm@tollgroup.com"
	cSubject = 'LifeFactory Daily Shipment Summary for ' + TTOC(DATETIME())
	cAttach = ''
	cBodyText = ''

	cCOMPUTERNAME = ''
	cUSERNAME = ''


	FUNCTION INIT
		IF NOT DODEFAULT()
			RETURN .F.
		ENDIF
		WITH THIS
			*SET RESOURCE OFF
			CLOSE DATA
			DO setenvi
			SET CENTURY ON
			SET DATE TO ymd
			SET DECIMAL TO 0
			SET TALK OFF
			SET DELETED ON
			SET CONSOLE OFF
			SET EXCLUSIVE OFF
			SET SAFETY OFF
			SET EXACT OFF
			SET STATUS BAR ON
			SET SYSMENU OFF
			SET ENGINEBEHAVIOR 70
			_VFP.AUTOYIELD = .lAutoYield
			.cCOMPUTERNAME = GETENV("COMPUTERNAME")
			.cUSERNAME = GETENV("USERNAME")
			IF .lTestMode THEN
				.cLogFile = 'F:\UTIL\LF\LOGFILES\LFShipmentRptProc_LOG_TESTMODE.txt'
				.cSendTo = 'mbennett@fmiint.com'
				.cCC = ''
				*.cOutputFolder = 'F:\UTIL\LF\TESTOUT\'
			ENDIF
			.lLoggingIsOn = .lLoggingIsOn AND NOT EMPTY(.cLogFile)
		ENDWITH
	ENDFUNC


	FUNCTION DESTROY
		WITH THIS
			IF .lLoggingIsOn  THEN
				SET ALTERNATE OFF
				SET ALTERNATE TO
			ENDIF
		ENDWITH
		DODEFAULT()
	ENDFUNC


	FUNCTION MAIN
		WITH THIS

			LOCAL loError, ldToday, lcFileDateTime, lcCSVFile, lcFileRoot
			LOCAL oExcel, oWorkbook, oWorksheet, lnRow, lcRow, lcCountry, lcFTPFile, lcAttach, llArchived

			TRY

				.nNumberOfErrors = 0

				IF .lLoggingIsOn THEN
					SET ALTERNATE TO (.cLogFile) ADDITIVE
					SET ALTERNATE ON
				ENDIF
				
				* we are getting regular 'file in use' errors in WF, so hardcoding data dir 7/16/13 MB
				*.cWHDataFolder = WF("N",6034)
				.cWHDataFolder = 'F:\WHI\WHDATA\'				

				.TrackProgress('LF Daily Shipment Summary Process started....', LOGIT+SENDIT+NOWAITIT)
				.TrackProgress('COMPUTERNAME = ' + .cCOMPUTERNAME, LOGIT+SENDIT)
				.TrackProgress('USERNAME = ' + .cUSERNAME, LOGIT+SENDIT)
				.TrackProgress('WHData Folder = ' + .cWHDataFolder, LOGIT+SENDIT)
				.TrackProgress('PROJECT = LIFEFACTORYSHIPMENTRPT', LOGIT+SENDIT)				
				
				IF .lTestMode THEN
					.TrackProgress('TEST MODE', LOGIT+SENDIT)
				ENDIF
				
				ldToday = .dToday

				SET DATE TO mdy
				.cSubject = 'LifeFactory Daily Shipment Summary for ' + TRANSFORM(ldToday)
				SET DATE TO ymd
				
				IF USED('CURLF') THEN
					USE IN CURLF
				ENDIF

				
*!*					SELECT ;
*!*						A.SHIP_REF, ;
*!*						"SHIPPED" AS STATUS, ;
*!*						A.BOL_NO, ;
*!*						B.SHIPDATE, ;
*!*						B.CHARGE, ;
*!*						B.WEIGHT, ;
*!*						(ALLTRIM(B.CARRIER) + " " + ALLTRIM(B.BILLING) + " " + ALLTRIM(B.SERVICE)) AS METHOD, ;
*!*						B.COMPANY AS CONSIGNEE, ;
*!*						B.ADDRESS, ;
*!*						B.ADDRESS2, ;
*!*						(ALLTRIM(B.CITY) + ", " + B.STATE + " " + B.ZIP) AS CSZ, ;
*!*						(B.COUNTRY + "   ") AS COUNTRY, ;
*!*						C.STYLE AS SKU, ;
*!*						C.COLOR, ;
*!*						C.ID AS NAME, ;
*!*						C.TOTQTY ;
*!*						FROM (.cWHDataFolder + 'OUTSHIP') A ;
*!*						INNER JOIN (.cWHDataFolder + 'SHIPMENT') B ;
*!*						ON B.PICKTICKET = A.SHIP_REF ;
*!*						INNER JOIN (.cWHDataFolder + 'OUTDET') C ;
*!*						ON C.OUTSHIPID = A.OUTSHIPID ;
*!*						INTO CURSOR CURLF ;
*!*						WHERE A.ACCOUNTID = 6034 ;
*!*						AND B.SENT ;
*!*						AND B.SHIPDATE = ldToday ;
*!*						AND B.ACCOUNTID = 6034 ;
*!*						READWRITE

				* revised to use sql shipment table 11/22/2016
				xsqlexec("select * from shipment where mod='I' and accountid=6034","shipment",,"wh")

				xsqlexec("select * from outship where accountid=6034","outship",,"wh")
				xsqlexec("select * from outdet where accountid=6034","outdet",,"wh")
				
*!*					SELECT ;
*!*						A.SHIP_REF, ;
*!*						"SHIPPED" AS STATUS, ;
*!*						A.BOL_NO, ;
*!*						B.SHIPDATE, ;
*!*						B.CHARGE, ;
*!*						B.WEIGHT, ;
*!*						(ALLTRIM(B.CARRIER) + " " + ALLTRIM(B.BILLING) + " " + ALLTRIM(B.SERVICE)) AS METHOD, ;
*!*						B.COMPANY AS CONSIGNEE, ;
*!*						B.ADDRESS, ;
*!*						B.ADDRESS2, ;
*!*						(ALLTRIM(B.CITY) + ", " + B.STATE + " " + B.ZIP) AS CSZ, ;
*!*						(B.COUNTRY + "   ") AS COUNTRY, ;
*!*						C.STYLE AS SKU, ;
*!*						C.COLOR, ;
*!*						C.ID AS NAME, ;
*!*						C.TOTQTY ;
*!*						FROM (.cWHDataFolder + 'OUTSHIP') A ;
*!*						INNER JOIN SHIPMENT B ;
*!*						ON B.PICKTICKET = A.SHIP_REF ;
*!*						INNER JOIN (.cWHDataFolder + 'OUTDET') C ;
*!*						ON C.OUTSHIPID = A.OUTSHIPID ;
*!*						INTO CURSOR CURLF ;
*!*						WHERE A.ACCOUNTID = 6034 ;
*!*						AND B.SENT ;
*!*						AND B.SHIPDATE = ldToday ;
*!*						AND B.ACCOUNTID = 6034 ;
*!*						READWRITE
				
				SELECT ;
					A.SHIP_REF, ;
					"SHIPPED" AS STATUS, ;
					A.BOL_NO, ;
					B.SHIPDATE, ;
					B.CHARGE, ;
					B.WEIGHT, ;
					(ALLTRIM(B.CARRIER) + " " + ALLTRIM(B.BILLING) + " " + ALLTRIM(B.SERVICE)) AS METHOD, ;
					B.COMPANY AS CONSIGNEE, ;
					B.ADDRESS, ;
					B.ADDRESS2, ;
					(ALLTRIM(B.CITY) + ", " + B.STATE + " " + B.ZIP) AS CSZ, ;
					(B.COUNTRY + "   ") AS COUNTRY, ;
					C.STYLE AS SKU, ;
					C.COLOR, ;
					C.ID AS NAME, ;
					C.TOTQTY ;
					FROM OUTSHIP A ;
					INNER JOIN SHIPMENT B ;
					ON B.PICKTICKET = A.SHIP_REF ;
					INNER JOIN OUTDET C ;
					ON C.OUTSHIPID = A.OUTSHIPID ;
					INTO CURSOR CURLF ;
					WHERE A.ACCOUNTID = 6034 ;
					AND B.SENT ;
					AND B.SHIPDATE = ldToday ;
					AND B.ACCOUNTID = 6034 ;
					READWRITE
					
					SELECT CURLF
					SCAN
						lcCountry = ALLTRIM(CURLF.COUNTRY)
						DO CASE
							CASE lcCountry = '124'
								lcCountry = "CANADA"
							CASE lcCountry = '840'
								lcCountry = "USA"
							OTHERWISE
								* nothing
						ENDCASE
						REPLACE CURLF.COUNTRY WITH lcCountry
					ENDSCAN
					
*!*					SELECT CURLF
*!*					BROW
				
				lcFileDateTime = PADL(YEAR(ldToday),4,"0") + PADL(MONTH(ldToday),2,"0") + PADL(DAY(ldToday),2,"0") + STRTRAN(SUBSTR(TIME(),1,5),":","")

				WAIT WINDOW NOWAIT "Opening Excel..."
				oExcel = CREATEOBJECT("excel.application")
				oExcel.VISIBLE = .F.
				oExcel.DisplayAlerts = .F.
				oWorkbook = oExcel.workbooks.OPEN("F:\UTIL\LF\TEMPLATES\LFDAILYSHIPMENTS.XLS")
				oWorksheet = oWorkbook.Worksheets[1]
				lcFileRoot = "DAILYSHIPMENTS_" + lcFileDateTime
				lcCSVFile = .cOutputFolder + lcFileRoot
				
				lnRow = 1
				SELECT CURLF
				SCAN
					lnRow = lnRow + 1
					lcRow = LTRIM(STR(lnRow))
					oWorksheet.RANGE("A" + lcRow).VALUE = CURLF.SHIP_REF
					oWorksheet.RANGE("B" + lcRow).VALUE = CURLF.STATUS
					oWorksheet.RANGE("C" + lcRow).VALUE = CURLF.BOL_NO
					oWorksheet.RANGE("D" + lcRow).VALUE = CURLF.SHIPDATE
					oWorksheet.RANGE("E" + lcRow).VALUE = CURLF.CHARGE
					oWorksheet.RANGE("F" + lcRow).VALUE = CURLF.WEIGHT
					oWorksheet.RANGE("G" + lcRow).VALUE = CURLF.METHOD
					oWorksheet.RANGE("H" + lcRow).VALUE = CURLF.CONSIGNEE
					oWorksheet.RANGE("I" + lcRow).VALUE = CURLF.ADDRESS
					oWorksheet.RANGE("J" + lcRow).VALUE = CURLF.ADDRESS2
					oWorksheet.RANGE("K" + lcRow).VALUE = CURLF.CSZ
					oWorksheet.RANGE("L" + lcRow).VALUE = CURLF.COUNTRY
					oWorksheet.RANGE("M" + lcRow).VALUE = CURLF.SKU
					oWorksheet.RANGE("N" + lcRow).VALUE = CURLF.COLOR
					oWorksheet.RANGE("O" + lcRow).VALUE = CURLF.NAME
					oWorksheet.RANGE("P" + lcRow).VALUE = CURLF.TOTQTY
				ENDSCAN
				
				oWorkbook.SAVEAS(lcCSVFile,6)
				oExcel.QUIT()
				RELEASE ALL LIKE oWorkbook
				RELEASE ALL LIKE oExcel
				
				.cAttach = (lcCSVFile + ".csv")
				
				
				********************************************************************************************
				********************************************************************************************
				* also copy report to FTP reports folder per Chip 11/13/13.
				lcAttach = .cAttach
				
*!*					* make sure source file is not read-only
*!*					RUN ATTRIB -R &lcAttach.				
				
				lcFTPFile = 'F:\FTPUSERS\LIFEFACTORY\945OUT\DAILYSHIPMENT\' + lcFileRoot + '.csv'
				*lcFTPFile = 'C:\A\DAILYSHIPMENT\' + lcFileRoot + '.csv'

				* if output file exists, make sure it's not read-only
				llArchived = FILE(lcFTPFile)
				IF llArchived THEN
					RUN ATTRIB -R &lcFTPFile.
				ENDIF
				
				.TrackProgress('source file = ' + lcAttach,LOGIT+SENDIT)
				.TrackProgress('target file = ' + lcFTPFile,LOGIT+SENDIT)
				
				* do the copy
				COPY FILE (lcAttach) TO (lcFTPFile)
				
				IF NOT FILE(lcFTPFile) THEN
					.TrackProgress('WARNING: There was a problem creating FTP file: ' + lcFTPFile,LOGIT+SENDIT)
				ENDIF
				********************************************************************************************
				********************************************************************************************
								

				

				.TrackProgress('LF Daily Shipment Summary Process ended normally!',LOGIT+SENDIT)
				.cBodyText = "See attached Daily Shipment CSV file." + CRLF + CRLF + "<process log follows>" + ;
					CRLF + CRLF + .cBodyText

			CATCH TO loError
			
				IF TYPE('oExcel') = "O" AND NOT oExcel = NULL THEN
					oExcel.QUIT()
				ENDIF

				.TrackProgress('There was an error.',LOGIT+SENDIT)
				.TrackProgress('The Error # is: ' + TRANSFORM(loError.ERRORNO), LOGIT+SENDIT)
				.TrackProgress('The Error Message is: ' + TRANSFORM(loError.MESSAGE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred in Program: ' + TRANSFORM(loError.PROCEDURE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred at line #: ' + TRANSFORM(loError.LINENO), LOGIT+SENDIT)
				.nNumberOfErrors = .nNumberOfErrors + 1
				CLOSE DATA

			ENDTRY

			CLOSE DATA
			*CLOSE ALL
			WAIT CLEAR
			***************** INTERNAL email results ******************************
			.TrackProgress('About to send status email.',LOGIT)
			.TrackProgress('The logfile is: ' + .cLogFile, SENDIT)
			.TrackProgress('LF Daily Shipment Summary Process started: ' + .cStartTime, LOGIT+SENDIT)
			.TrackProgress('LF Daily Shipment Summary Process finished: ' + TTOC(DATETIME()), LOGIT+SENDIT)

			IF .nNumberOfErrors > 0 THEN
				.cSendTo = 'mbennett@fmiint.com'
			ENDIF

			IF .lSendInternalEmailIsOn THEN
				DO FORM dartmail2 WITH .cSendTo,.cFrom,.cSubject,.cCC,.cAttach,.cBodyText,"A"
				.TrackProgress('Sent status email.',LOGIT)
			ELSE
				.TrackProgress('Did not send status email: THIS.lSendInternalEmailIsOn = FALSE.',LOGIT)
			ENDIF
			
			IF .nNumberOfErrors = 0 THEN
				schedupdate()
			ENDIF

		ENDWITH
		RETURN
	ENDFUNC && main


	PROCEDURE LOGERROR
		LPARAMETERS tcError
		WITH THIS
			.TrackProgress(tcError,LOGIT+SENDIT)
			.cErrors = .cErrors + ALLTRIM(tcError) + CRLF
		ENDWITH
	ENDPROC


	PROCEDURE TrackProgress
		* do any combination of Wait Window, writing to logfile, and adding to body of email,
		* based on nFlags parameter.
		LPARAMETERS tcExpression, tnFlags
		WITH THIS
			IF BITAND(tnFlags,LOGIT) = LOGIT THEN
				IF .lLoggingIsOn THEN
					?
					? .cProcessName + "  " + TTOC(DATETIME()) + ": " + tcExpression
				ENDIF
			ENDIF
			IF .lWaitWindowIsOn THEN
				IF BITAND(tnFlags,NOWAITIT) = NOWAITIT THEN
					WAIT WINDOW tcExpression NOWAIT
				ENDIF
				IF BITAND(tnFlags,WAITIT) = WAITIT THEN
					WAIT WINDOW tcExpression TIMEOUT .nWaitWindowTimeout
				ENDIF
			ENDIF
			IF BITAND(tnFlags,SENDIT) = SENDIT THEN
				IF .lSendInternalEmailIsOn THEN
					.cBodyText = .cBodyText + tcExpression + CRLF + CRLF
				ENDIF
			ENDIF
		ENDWITH
	ENDPROC  &&  TrackProgress


	PROCEDURE SortArrayByDateTime
		* expects taInArray[] to be the result of an ADIR()
		LPARAMETERS taInArray, taOutArray
		EXTERNAL ARRAY taInArray, taOutArray
		LOCAL lnRows, lnCols, i, j
		* extending taInArray[] to process the files in date/time order
		lnRows = ALEN(taInArray,1)
		lnCols = ALEN(taInArray,2)
		* create taOutArray with one more column
		DIMENSION taOutArray[lnRows,lnCols + 1]
		* and fill it with taInArray columns AND (filedate + filetime) in new column
		* ( ACOPY() doesn't preserve elements properly because of different # of columns )
		FOR i = 1 TO lnRows
			FOR j = 1 TO lnCols
				taOutArray[i,j] = taInArray[i,j]
			ENDFOR
			taOutArray[i,lnCols + 1] = DTOS(taInArray[i,3]) + "_" + taInArray[i,4]
		ENDFOR
		* sort taOutArray on date/time column
		=ASORT(taOutArray,6)
	ENDPROC


ENDDEFINE
