* Create spreadsheet of Hours summary for Employees we are exporting from kronos to ADP Payroll
* for the previous pay period.
** Meant to be run after hours are reviewed and cleaned up; Monday pm or Tuesday am.

LOCAL loListKronosEmployees
loListKronosEmployees = CREATEOBJECT('ListKronosEmployees')
loListKronosEmployees.MAIN()
CLOSE DATABASES ALL
RETURN

#DEFINE LOGIT 1
#DEFINE WAITIT 2
#DEFINE NOWAITIT 4
#DEFINE SENDIT 8
#DEFINE CRLF CHR(13) + CHR(10)
#DEFINE RETURN_DATA_MANDATORY .F.
#DEFINE RETURN_DATA_NOT_MANDATORY .T.

DEFINE CLASS ListKronosEmployees AS CUSTOM

	cProcessName = 'ListKronosEmployees'

	lTestMode = .F.  && .F. FOR NORMAL OPERATION, .T. TO SUPPRESS ALL BUT DEVELOPER EMAILS AND CHANGE INPUT TABLE, ETC.

	lAutoYield = .T.

	cStartTime = TTOC(DATETIME())

	* connection properties
	nSQLHandle = 0

	* file properties
	nFileHandle = 0

	* date properties
	dtNow = DATETIME()
	dToday = DATE()
	cToday = DTOC(DATE())

	* wait window properties
	nWaitWindowTimeout = 2
	lWaitWindowIsOn = .T.

	* logfile properties
	lLoggingIsOn = .T.
	cLogFile = 'F:\UTIL\KRONOS\Logfiles\ListKronosEmployees_log.txt'

	* INTERNAL email properties
	lSendInternalEmailIsOn = .T.
	cFrom = 'FMI Corporate <fmicorporate@fmiint.com>'
	cSendTo = 'Mark Bennett <mbennett@fmiint.com>'
	*cSendTo = 'Lucille Waldrip <lwaldrip@fmiint.com>, Lori Guiliano <lguiliano@fmiint.com>, Mark Bennett <mbennett@fmiint.com>'
	cCC = ''
	cSubject = 'LIST KRONOS EMPLOYEES for: ' + TTOC(DATETIME())
	cAttach = ''
	cBodyText = ''

	cCOMPUTERNAME = ''
	cUSERNAME = ''

	FUNCTION INIT
		IF NOT DODEFAULT()
			RETURN .F.
		ENDIF
		WITH THIS
			CLEAR
			*SET RESOURCE OFF
			CLOSE DATA
			SET CENTURY ON
			SET DATE YMD
			SET HOURS TO 24
			SET ANSI ON
			SET TALK OFF
			SET DELETED ON
			SET CONSOLE OFF
			SET EXCLUSIVE OFF
			SET SAFETY OFF
			SET EXACT OFF
			SET STATUS BAR ON
			SET SYSMENU OFF
			SET ENGINEBEHAVIOR 70
			_VFP.AUTOYIELD = .lAutoYield
			.cCOMPUTERNAME = GETENV("COMPUTERNAME")
			.cUSERNAME = GETENV("USERNAME")
			IF .lTestMode THEN
				.cSendTo = 'Mark Bennett <mbennett@fmiint.com>'
				.cLogFile = 'F:\UTIL\KRONOS\Logfiles\ListKronosEmployees_log_TESTMODE.txt'
			ENDIF
			.lLoggingIsOn = .lLoggingIsOn AND NOT EMPTY(.cLogFile)
			IF .lLoggingIsOn THEN
				SET ALTERNATE TO (.cLogFile) ADDITIVE
				SET ALTERNATE ON
			ENDIF
			* re-init date properties after all SETTINGS
			.dtNow = DATETIME()
			.dToday = DATE()
			.cToday = DTOC(DATE())
		ENDWITH
	ENDFUNC


	FUNCTION DESTROY
		WITH THIS
			IF .lLoggingIsOn  THEN
				SET ALTERNATE OFF
				SET ALTERNATE TO
			ENDIF
		ENDWITH
		DODEFAULT()
	ENDFUNC


	FUNCTION MAIN
		WITH THIS
			LOCAL lnNumberOfErrors, lcSQL, loError, lcTopBodyText, llValid
			LOCAL lcXLFileName, ldStartDate, ldEndDate, lcStartDate, lcEndDate, ldDate
			LOCAL lcSQLStartDate, lcSQLEndDate, lcXLFileName
			LOCAL oExcel, oWorkbook, oWorksheet, lnRow, lcRow, lnStartRow, lcStartRow, lnEndRow, lnPP, lcPP, lnNN, lcNN, lcJJ
			LOCAL lcColsToProcess, lnColumn, lcBoldColumn, i, j, lcBoldRow, luCellValue, lcTargetDirectory, ldPayDate, lcFiletoSaveAs
			LOCAL lcFileDate, lcDivision, lcRateType, lcTitle, llSaveAgain, lcPayCode
			LOCAL lcList

			TRY
				lnNumberOfErrors = 0
				lcTopBodyText = ''

				*.TrackProgress('==================================================================================================================', LOGIT+SENDIT)
				.TrackProgress('LIST KRONOS EMPLOYEES process started....', LOGIT+SENDIT+NOWAITIT)
				.TrackProgress('COMPUTERNAME = ' + .cCOMPUTERNAME, LOGIT+SENDIT)
				.TrackProgress('USERNAME = ' + .cUSERNAME, LOGIT+SENDIT)
				.TrackProgress('==================================================================================================================', LOGIT+SENDIT)
				IF .lTestMode THEN
					.TrackProgress('', LOGIT)
					.TrackProgress('==================================================================================================================', LOGIT+SENDIT)
					.TrackProgress('                        TEST MODE                        ', LOGIT+SENDIT)
					.TrackProgress('==================================================================================================================', LOGIT+SENDIT)
				ENDIF
				.TrackProgress('', LOGIT)

				* calculate pay period start/end dates.
				* We go back from today until we hit a Saturday (DOW = 7) , which is the end day of the previous pay period.
				* then we subtract 6 days from that to get the prior Sunday (DOW = 1), which is the start day of the previous pay period.
				ldDate = .dToday
				DO WHILE DOW(ldDate,1) <> 7
					ldDate = ldDate - 1
				ENDDO
				ldEndDate = ldDate
				ldStartDate = ldDate - 6

				lcStartDate = STRTRAN(DTOC(ldStartDate),"/","-")
				lcEndDate = STRTRAN(DTOC(ldEndDate),"/","-")

				*	 create "YYYYMMDD" safe date format for use in SQL query
				* NOTE: for SQL we are going to use < EndDate ( not <= ), so we are going to add 1 to that date here for querying purposes.
				lcSQLStartDate = STRTRAN(DTOC(ldStartDate),"/","")
				lcSQLEndDate = STRTRAN(DTOC(ldEndDate + 1),"/","")

				.cSubject = 'List Kronos Employees for: ' + lcStartDate + " to " + lcEndDate


				* main SQL
				lcSQL = ;
					" SELECT " + ;
					" D.LABORLEV2NM AS DIVISION, " + ;
					" D.LABORLEV3NM AS DEPT, " + ;
					" D.LABORLEV7NM AS TIMERVWR, " + ;
					" C.FULLNM AS EMPLOYEE, " + ;
					" C.PERSONNUM AS FILE_NUM, " + ;
					" COUNT(*) AS COUNT " + ;
					" FROM WFCTOTAL A " + ;
					" JOIN WTKEMPLOYEE B " + ;
					" ON B.EMPLOYEEID = A.EMPLOYEEID " + ;
					" JOIN PERSON C " + ;
					" ON C.PERSONID = B.PERSONID " + ;
					" JOIN LABORACCT D " + ;
					" ON D.LABORACCTID = A.LABORACCTID " + ;
					" WHERE (A.ADJSTARTDTM >= '" + lcSQLStartDate + "')" + ;
					" AND (A.ADJSTARTDTM < '" + lcSQLEndDate + "')" + ;
					" AND (D.LABORLEV1NM = 'SXI') " + ;
					" GROUP BY D.LABORLEV2NM, D.LABORLEV3NM, D.LABORLEV7NM, C.FULLNM, C.PERSONNUM " + ;
					" ORDER BY 1, 2, 3, 4, 5, 6 "

				IF .lTestMode THEN
					.TrackProgress('', LOGIT+SENDIT)
					.TrackProgress('lcSQL =' + lcSQL, LOGIT+SENDIT)
				ENDIF

				.TrackProgress('Connecting to WTK System....', LOGIT+SENDIT)

				* get ss#s from ADP
				OPEN DATABASE F:\UTIL\KRONOS\KRONOSEDI

				.nSQLHandle = SQLCONNECT("KRONOSEDI","tkcsowner","tkcsowner")

				IF .nSQLHandle > 0 THEN
				
					IF USED('CURWTKHOURS') THEN
						USE IN CURWTKHOURS
					ENDIF

					IF .ExecSQL(lcSQL, 'CURWTKHOURS', RETURN_DATA_MANDATORY) THEN

						*SELECT CURWTKHOURS
						*BROWSE

						IF USED('KRONOSEMPS') THEN
							USE IN KRONOSEMPS
						ENDIF

						IF FILE('F:\UTIL\KRONOS\DATA\KRONOSEMPS.DBF') THEN
							DELETE FILE F:\UTIL\KRONOS\DATA\KRONOSEMPS.DBF
						ENDIF

						IF FILE('F:\UTIL\KRONOS\DATA\KRONOSEMPS.DBF') THEN
							.TrackProgress('Error deleting F:\UTIL\KRONOS\DATA\KRONOSEMPS.DBF', LOGIT+SENDIT)
						ENDIF

						IF USED('CURKRONOSEMPS') THEN
							USE IN CURKRONOSEMPS
						ENDIF
						IF USED('CURKRONOSEMPS2') THEN
							USE IN CURKRONOSEMPS2
						ENDIF

						SELECT DISTINCT ;
							LEFT(EMPLOYEE,30) AS EMPLOYEE, ;
							INT(VAL(FILE_NUM)) AS FILE_NUM, ;
							LEFT(TIMERVWR,4) AS TIMERVWR ;
							FROM CURWTKHOURS ;
							INTO CURSOR CURKRONOSEMPS ;
							WHERE TIMERVWR IN ;
							(SELECT TIMERVWR FROM F:\UTIL\KRONOS\DATA\TIMERVWR) ;
							ORDER BY 2

						*********!!!!!! copy to kronos data dir for use by foxpro programs!!!!!!
						SELECT CURKRONOSEMPS
						COPY TO F:\UTIL\KRONOS\DATA\KRONOSEMPS

						* create another cursor with dept & div
						SELECT DISTINCT ;
							LEFT(DIVISION,2) as DIVISION, ;
							LEFT(DEPT,4) as DEPT, ;
							LEFT(EMPLOYEE,30) AS EMPLOYEE, ;
							INT(VAL(FILE_NUM)) AS FILE_NUM, ;
							LEFT(TIMERVWR,4) AS TIMERVWR ;
							FROM CURWTKHOURS ;
							INTO CURSOR CURKRONOSEMPS2 ;
							WHERE TIMERVWR IN ;
							(SELECT TIMERVWR FROM F:\UTIL\KRONOS\DATA\TIMERVWR) ;
							ORDER BY 1, 2, 3

						*  list in email
						lcList = ""
						SELECT CURKRONOSEMPS2
						SCAN
							lcList = lcList + CURKRONOSEMPS2.DIVISION + "   " + CURKRONOSEMPS2.DEPT + "  " + CURKRONOSEMPS2.EMPLOYEE + "     " + TRANSFORM(CURKRONOSEMPS2.FILE_NUM) + "     " + CURKRONOSEMPS2.TIMERVWR + CRLF
						ENDSCAN
						.TrackProgress('DIV  DEPT  EMPLOYEE                           FILE#    TIMEREVIEWER', LOGIT+SENDIT)
						.TrackProgress(lcList, LOGIT+SENDIT)

						* list TIMERVWR recs
						lcList = ""
						SELECT TIMERVWR
						SCAN
							lcList = lcList + TIMERVWR.TIMERVWR + "          " + TIMERVWR.NAME + CRLF
						ENDSCAN
						.TrackProgress('TIMEREVIEWER#, NAME    ', LOGIT+SENDIT)
						.TrackProgress(lcList, LOGIT+SENDIT)


					ENDIF  &&  .ExecSQL(lcSQL, 'CURWTKEMPLOYEE', RETURN_DATA_MANDATORY)

					CLOSE DATABASES ALL

					IF NOT FILE('F:\UTIL\KRONOS\DATA\KRONOSEMPS.DBF') THEN
						.TrackProgress('Error creating F:\UTIL\KRONOS\DATA\KRONOSEMPS.DBF', LOGIT+SENDIT)
					ELSE
						.TrackProgress('Created F:\UTIL\KRONOS\DATA\KRONOSEMPS.DBF!', LOGIT+SENDIT)
					ENDIF

				ELSE
					* connection error
					.TrackProgress('Unable to connect to WTK System.', LOGIT+SENDIT)
				ENDIF   &&  .nSQLHandle > 0

			CATCH TO loError

				.TrackProgress('There was an error.',LOGIT+SENDIT)
				.TrackProgress('The Error # is: ' + TRANSFORM(loError.ErrorNo), LOGIT+SENDIT)
				.TrackProgress('The Error Message is: ' + TRANSFORM(loError.MESSAGE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred in Program: ' + TRANSFORM(loError.PROCEDURE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred at line #: ' + TRANSFORM(loError.LINENO), LOGIT+SENDIT)
				lnNumberOfErrors = lnNumberOfErrors + 1
				IF TYPE('oExcel') = 'O' AND NOT ISNULL(oExcel) THEN
					oExcel.QUIT()
				ENDIF
				CLOSE DATA
				CLOSE ALL

			ENDTRY

			CLOSE DATA
			WAIT CLEAR
			***************** INTERNAL email results ******************************
			.TrackProgress('About to send status email.',LOGIT)
			.TrackProgress('==================================================================================================================', SENDIT)
			.TrackProgress('The logfile is: ' + .cLogFile, SENDIT)
			.TrackProgress('LIST KRONOS EMPLOYEES process started: ' + .cStartTime, LOGIT+SENDIT)
			.TrackProgress('LIST KRONOS EMPLOYEES process finished: ' + TTOC(DATETIME()), LOGIT+SENDIT)

			IF .lSendInternalEmailIsOn THEN
				* try to trap error from not having dartmail dll's registered on user's pc...
				TRY
					DO FORM F:\MAIL\FORMS\dartmail2 WITH .cSendTo,.cFrom,.cSubject,.cCC,.cAttach,.cBodyText,"A"
					.TrackProgress('Sent status email.',LOGIT)
				CATCH TO loError
					=MESSAGEBOX('There was an error sending the status email. Please contact Mark Bennett.',0+64,"LIST KRONOS EMPLOYEES")
					.TrackProgress('There was an error sending the status email.',LOGIT)
					.TrackProgress('The Error # is: ' + TRANSFORM(loError.ErrorNo), LOGIT)
					.TrackProgress('The Error Message is: ' + TRANSFORM(loError.MESSAGE), LOGIT)
					.TrackProgress('The Error occurred in Program: ' + TRANSFORM(loError.PROCEDURE), LOGIT)
					.TrackProgress('The Error occurred at line #: ' + TRANSFORM(loError.LINENO), LOGIT)
					lnNumberOfErrors = lnNumberOfErrors + 1
					CLOSE DATA
				ENDTRY
			ELSE
				.TrackProgress('Did not send status email: THIS.lSendInternalEmailIsOn = FALSE.',LOGIT)
			ENDIF

		ENDWITH
		RETURN
	ENDFUNC && main


	FUNCTION ExecSQL
		LPARAMETERS tcSQL, tcCursorName, tlNoDataReturnedIsOkay
		LOCAL llRetval, lnResult
		WITH THIS
			* close target cursor if it's open
			IF USED(tcCursorName)
				USE IN (tcCursorName)
			ENDIF
			WAIT WINDOW "Fetching remote data; please wait..." NOWAIT
			lnResult = sqlexec(.nSQLHandle, tcSQL, tcCursorName)
			llRetval = ( lnResult > 0 )
			IF llRetval THEN
				* see if any data came back
				IF NOT tlNoDataReturnedIsOkay THEN
					IF (NOT USED(tcCursorName)) OR EOF(tcCursorName) THEN
						llRetval = .F.
						.TrackProgress('No data was returned by this query: ' +CRLF + tcSQL, LOGIT+SENDIT)
					ENDIF
				ENDIF
			ELSE
				.TrackProgress("Result Code [" + TRANSFORM(lnResult) + "] returned by this query: " + tcSQL, LOGIT+SENDIT)
			ENDIF
			WAIT CLEAR
			RETURN llRetval
		ENDWITH
	ENDFUNC


	PROCEDURE TrackProgress
		* do any combination of Wait Window, writing to logfile, and adding to body of email,
		* based on nFlags parameter.
		LPARAMETERS tcExpression, tnFlags
		WITH THIS
			IF BITAND(tnFlags,LOGIT) = LOGIT THEN
				IF .lLoggingIsOn THEN
					?
					? .cProcessName + "  " + TTOC(DATETIME()) + ": " + tcExpression
				ENDIF
			ENDIF
			IF .lWaitWindowIsOn THEN
				IF BITAND(tnFlags,NOWAITIT) = NOWAITIT THEN
					WAIT WINDOW tcExpression NOWAIT
				ENDIF
				IF BITAND(tnFlags,WAITIT) = WAITIT THEN
					WAIT WINDOW tcExpression TIMEOUT .nWaitWindowTimeout
				ENDIF
			ENDIF
			IF BITAND(tnFlags,SENDIT) = SENDIT THEN
				IF .lSendInternalEmailIsOn THEN
					.cBodyText = .cBodyText + tcExpression + CRLF + CRLF
				ENDIF
			ENDIF
		ENDWITH
	ENDPROC  &&  TrackProgress

ENDDEFINE

