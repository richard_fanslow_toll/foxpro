*!* bbc940_main.prg taken from m:\dev\prg\steelseries940_main.prg
Parameters cOfficeIn,nAcctNum


Public cDelimiter,nRun_num,cTranslateOption,cWhichFile,cOffice,cCustname,cPropername,cStorenum,lOK
Public cFilter,nRunID,lComplete,lTesting,m.UCCNUMBER,lEmail,lTestImport,emailcommentstr,cUseFolder,lBrowfiles,cMailName,cMod
Public ptid,ptctr,ptqty,xfile,cfilename,nUploadCount,pickticket_num_start,pickticket_num_end,nFileCount,units,cUCCNumber,cISA_Num
Public lcPath,lcArchivepath,NormalExit,lTestmail,LogCommentStr,lPick,lPrepack,lImported,lLoadSQL
Public m.addby,m.adddt,m.addproc,cTransfer,thisfile,lNextDay,lMonday,cForeignStr,lTestPTHIST
Public cErrMsg,dFileDate,nFileSize
Public llEmailTest
Public tsendto,tcc,tsendtoerr,tccerr,tsendtotest,tcctest,tsendtoalt,tccalt,tsendtopt,tccpt,tfrom,tattach
Store "" To tsendto,tcc,tsendtoerr, tccerr,tsendtotest,tcctest,tsendtoalt,tccalt,tsendtopt,tccpt,tfrom,tattach

Public Array a856(1)
Close Databases All
NormalExit = .F.
On Error Throw

cMailName="BBC Upload "
tsendto  = "joe.bianchi@tollgroup.com"
tsendtoerr = tsendto
tcc = "pgaidis@fmiint.com"
tccerr = tcc
cMailName = "BBC"
nAcctNum  = IIF(VARTYPE(nacctnum)#"N",6757,nAcctNum)

_Screen.Caption = cMailName
cErrMsg="Non-specific: In program body"

cOfficeIn ="L"
cOffice = cOfficeIn
cMod = "L" &&cOffice
llEmailTest = .f.
Try
  lTesting = .f.
  
  lOverridebusy = lTesting
  lOverridebusy = .F.

  lTestImport = lTesting
  lTestmail = lTesting
  lBrowfiles = lTesting
*  lBrowfiles = .t.
  goffice= cOfficeIn
  lLoadSQL = .T.

  Do m:\dev\prg\_setvars With lTesting
  cCustname = "BBC"  && Initializes
  cMailName = "BBC"

  Wait Window "At start of BBC 940 processing" Nowait
  Select 0
  Use F:\edirouting\ftpsetup In 0
  cTransfer = "940-BBC"
  If !lTesting
    Locate For ftpsetup.transfer = cTransfer And ftpsetup.Type = "GET"
    If !Found()
      Wait Window "940-BBC--> Process not found in FTPSetup.........exiting" Timeout 2
      NormalExit = .T.
      Throw
    Endif
    If ftpsetup.chkbusy And !lOverridebusy
      Wait Window "Process is flagged busy...returning" Timeout 1
      NormalExit = .T.
      Throw
    Else
      Replace chkbusy With .T., trig_time With Datetime() For transfer = cTransfer  In ftpsetup
      Use In ftpsetup
    Endif
  Endif

  lEmail = .T.
  LogCommentStr = ""


  tfrom = "Toll EDI Operations <toll-edi-ops@tollgroup.com>"
  Store " " To tattach,tsendto,tcc

  Select 0
  Use F:\3pl\Data\mailmaster Alias mm Shared
  Locate
  Locate For mm.edi_type = "940" And mm.accountid = nAcctNum And mm.office = cOffice
  If Found()
    Store Trim(mm.acctname) To cCustname
    Store Trim(mm.basepath) To lcPath
    Store Trim(mm.archpath) To lcArchivepath
    _Screen.Caption = Allt(mm.scaption)
*!*      STORE mm.testflag      TO lTest

    Store Trim(Iif(mm.use_alt,mm.sendtoalt,mm.sendto)) To tsendto
    Store Trim(Iif(mm.use_alt,mm.ccalt,mm.cc))         To tcc

    Locate
    Locate For mm.edi_type = "MISC" And mm.taskname = "JOETEST"
    Store Trim(Iif(mm.use_alt,mm.sendtoalt,mm.sendto)) To tsendtotest
    Store Trim(Iif(mm.use_alt,mm.ccalt,mm.cc)) To tcctest

    Locate
    Locate For mm.edi_type = "MISC" And mm.taskname = "GENERAL"
    Store Trim(Iif(mm.use_alt,mm.sendtoalt,mm.sendto)) To tsendtoerr
    Store Trim(Iif(mm.use_alt,mm.ccalt,mm.cc)) To tccerr
    Use In mm
  Else
    Use In mm
    Wait Window At 10,10  "No parameters set for this acct# "+Alltrim(Str(nAcctNum))+"  ---> Office "+cOffice Timeout 2
    Release All
    NormalExit = .F.
    Throw
  Endif

  lcPath = Iif(lTesting,Strtran(Upper(lcPath),"940IN","940INTEST"),lcPath)
  lcArchivepath = Iif(lTesting,Strtran(Upper(lcArchivepath),"940IN","940INTEST"),lcArchivepath)
  cUseName = "BBC"
  cPropername = cUseName

  _Screen.Caption = cPropername+" 940 Process"
  _Screen.WindowState = Iif(lTesting Or lOverridebusy,2,1)
  Clear
  Wait Window "Now setting up "+cPropername+" 940 process..." Timeout 2
*!*    cDelimiter = "*"
*!*    cTranslateOption = "TILDE"

  cfile = ""
  dXdate1 = ""
  dXxdate2 = Date()
  nFileCount = 0

  nRun_num = 999
  lnRunID = nRun_num

  If lTesting
*    cOffice = "P"
*    cMod = "P"
*    cUseFolder = "F:\WHP\WHDATA\"
  Else
    xReturn="XXX"
    Do m:\dev\prg\wf_alt With cOffice,nAcctNum
    cUseFolder=xReturn
  Endif

  Do ("m:\dev\PRG\BBC940_PROCESS")

  If !lTesting
    Select 0
    Use F:\edirouting\ftpsetup Shared
    Replace chkbusy With .F. ;
    FOR ftpsetup.Type = "GET" ;
    AND ftpsetup.transfer = "940-BBC" ;
    IN ftpsetup
  Endif

  NormalExit = .T.

Catch To oErr
  If NormalExit = .F.
    Assert .F. Message "In Catch section..."
    Set Step On
    tsubject = cMailName+" 940 Upload Error ("+Transform(oErr.ErrorNo)+") at "+Ttoc(Datetime())
    tsendto  = tsendtoerr
    tcc = tccerr
    tmessage = cMailName+" 940 Upload Error..... Please fix me........!"
    lcSourceMachine = Sys(0)
    lcSourceProgram = "from merkury940 group"
    Set Step On
    tmessage =tmessage+ "Try/CATCH Exeception Message:"+Chr(13)+;
    [  Error: ] + Str(oErr.ErrorNo) +Chr(13)+;
    [  LineNo: ] + Str(oErr.Lineno) +Chr(13)+;
    [  Message: ] + oErr.Message +Chr(13)+;
    [  Procedure: ] + oErr.Procedure +Chr(13)

    If !Empty(cErrMsg)
      tmessage =tmessage+Chr(13)+"Error found: "+cErrMsg
    Else
      tmessage =tmessage+[  Details: ] + oErr.Details +Chr(13)+;
      [  StackLevel: ] + Str(oErr.StackLevel) +Chr(13)+;
      [  LineContents: ] + oErr.LineContents+Chr(13)+;
      [  UserValue: ] + oErr.UserValue+Chr(13)+;
      [  Computer:  ] +lcSourceMachine+Chr(13)+;
      [  940 file:  ] +xfile+Chr(13)+;
      [  Program:   ] +lcSourceProgram
    Endif

    If llEmailTest
      Do Form m:\dev\frm\sendmail_au With tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A",.T.  && .t. means do not actually send this email
    Else
      Do Form m:\dev\frm\dartmail2 With tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
    Endif

  Else
    Wait Window At 10,10 "Normal Exit " Timeout 2
  Endif
Finally
  On Error
  Set Status Bar On
  Close Databases All
Endtry
