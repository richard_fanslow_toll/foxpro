lparameters xloc, xnoempty

if empty(xloc)
	return ""
endif

xcslocselect=select()

xsqlexec("select * from qcloc where office='"+goffice+"' and placename='"+padr(xloc,30)+"'","xqcloc",,"fx")

if reccount()#0
	xreturn=xqcloc.cs
else
	select csloc
	locate for office=goffice and location = left(xloc,20)
	if found()
		xreturn=trim(csloc.cs)
	else
		if validcs(xloc,.t.)
			xreturn=trim(xloc)
		else
			if xnoempty
				xreturn=xloc
			else
				xreturn=""
			endif
		endif
	endif
endif

select (xcslocselect)

return xreturn