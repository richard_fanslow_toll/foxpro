* run ADP NEW HIRES/TERMS REPORT based on passed parameter
* build EXE in F:\UTIL\HR\MONTHLYHIRESTERMS\

LPARAMETERS tcDedCode

runack("HIRESANDTERMS")

LOCAL loMonthlyHiresAndTermsReport
loMonthlyHiresAndTermsReport = CREATEOBJECT('MonthlyHiresAndTermsReport')
loMonthlyHiresAndTermsReport.MAIN()
CLOSE DATABASES ALL
RETURN

#DEFINE LOGIT 1
#DEFINE WAITIT 2
#DEFINE NOWAITIT 4
#DEFINE SENDIT 8
#DEFINE CRLF CHR(13) + CHR(10)
#DEFINE RETURN_DATA_MANDATORY .F.
#DEFINE RETURN_DATA_NOT_MANDATORY .T.
#DEFINE CSVFORMAT 6
#DEFINE WRITE_ONLY_UNBUFFERED 11
#DEFINE COMMA ","

DEFINE CLASS MonthlyHiresAndTermsReport AS CUSTOM

	cProcessName = 'MonthlyHiresAndTermsReport'

	lTestMode = .F.  && .F. FOR NORMAL OPERATION, .T. TO SUPPRESS ALL BUT DEVELOPER EMAILS AND CHANGE INPUT TABLE, ETC.

	dToday = DATE()
	*dToday = {^2005-02-01}  && to force report for January
	*dToday = {^2005-05-01}  && to force report for April
	*dToday = {^2005-06-01}  && to force report for May
	*dToday = {^2005-07-01}  && to force report for June

	lAutoYield = .T.

	cStartTime = TTOC(DATETIME())

	* deduction properties
	cDedCode = ''

	* connection properties
	nSQLHandle = 0

	* wait window properties
	nWaitWindowTimeout = 2
	lWaitWindowIsOn = .T.

	* logfile properties
	lLoggingIsOn = .T.
	cLogFile = 'F:\UTIL\HR\MONTHLYHIRESTERMS\LOGFILES\HIRESANDTERMS_log.txt'

	* INTERNAL email properties
	lSendInternalEmailIsOn = .T.
	cFrom = 'TGF Corporate <fmicorporate@fmiint.com>'
	cSendTo = 'Lucille.Waldrip@tollgroup.com,mariedesaye@fmiint.com,Lauren.Klaver@tollgroup.com'
	cCC = 'mbennett@fmiint.com'
	cSubject = ''
	cAttach = ''
	cBodyText = ''

	cCOMPUTERNAME = ''
	cUSERNAME = ''


	FUNCTION INIT
		IF NOT DODEFAULT()
			RETURN .F.
		ENDIF
		WITH THIS
			*SET RESOURCE OFF
			CLOSE DATA
			SET CENTURY OFF
			SET DATE AMERICAN
			SET HOURS TO 24
			SET ANSI ON
			SET TALK OFF
			SET DELETED ON
			SET CONSOLE OFF
			SET EXCLUSIVE OFF
			SET SAFETY OFF
			SET EXACT OFF
			SET STATUS BAR OFF
			SET SYSMENU OFF
			SET ENGINEBEHAVIOR 70
			_VFP.AUTOYIELD = .lAutoYield
			.cCOMPUTERNAME = GETENV("COMPUTERNAME")
			.cUSERNAME = GETENV("USERNAME")
			IF .lTestMode THEN
				.cLogFile = 'F:\UTIL\HR\MONTHLYHIRESTERMS\LOGFILES\HIRESANDTERMS_log_TESTMODE.txt'
				.cSendTo = 'mbennett@fmiint.com'
			ENDIF
			.lLoggingIsOn = .lLoggingIsOn AND NOT EMPTY(.cLogFile)
			IF .lLoggingIsOn THEN
				SET ALTERNATE TO (.cLogFile) ADDITIVE
				SET ALTERNATE ON
			ENDIF
		ENDWITH
	ENDFUNC


	FUNCTION DESTROY
		WITH THIS
			IF .lLoggingIsOn  THEN
				SET ALTERNATE OFF
				SET ALTERNATE TO
			ENDIF
		ENDWITH
		DODEFAULT()
	ENDFUNC


	FUNCTION MAIN
		WITH THIS
			LOCAL lnNumberOfErrors, lcSQL, loError, lnThisMonth, lnThisYear, lnLastMonth, lnLastYear
			LOCAL lcOutputFile
			TRY
				lnNumberOfErrors = 0

				.TrackProgress('', LOGIT)
				.TrackProgress('', LOGIT)
				.TrackProgress('=========================================================', LOGIT+SENDIT)
				.TrackProgress('ADP NEW HIRES/TERMS REPORT process started....', LOGIT+SENDIT+NOWAITIT)
				.TrackProgress('COMPUTERNAME = ' + .cCOMPUTERNAME, LOGIT+SENDIT)
				.TrackProgress('USERNAME = ' + .cUSERNAME, LOGIT+SENDIT)
				.TrackProgress('PROJECT = HIRESANDTERMS', LOGIT+SENDIT)
				.TrackProgress('=========================================================', LOGIT+SENDIT)
				IF .lTestMode THEN
					.TrackProgress('                        TEST MODE                        ', LOGIT+SENDIT)
					.TrackProgress('=========================================================', LOGIT+SENDIT)
				ENDIF
				.TrackProgress('', LOGIT)


					* construct month/year values for SELECT
					lnThisMonth = MONTH(.dToday)
					lnThisYear = YEAR(.dToday)

					IF lnThisMonth > 1 THEN
						lnLastMonth = lnThisMonth - 1
						lnLastYear = lnThisYear
					ELSE
						lnLastMonth = 12
						lnLastYear = lnThisYear - 1
					ENDIF

					.cSubject = 'ADP New Hires & Terminations for Month/Year: ' + TRANSFORM(lnLastMonth) + "/" + TRANSFORM(lnLastYear)
					lcOutputFile = "F:\UTIL\HR\MONTHLYHIRESTERMS\REPORTS\ADP_HIRES_TERMS_" + + TRANSFORM(lnLastMonth) + "-" + TRANSFORM(lnLastYear) + ".XLS"

					* get ss#s from ADP
					OPEN DATABASE F:\UTIL\ADPCONNECTION\CONNECT1

					.nSQLHandle = SQLCONNECT("PCPAYWIN","MBENNETT","VP737A")

					IF .nSQLHandle > 0 THEN

						lcSQL = ;
							"SELECT " + ;
							" STATUS, " + ;
							" COMPANYCODE AS ADPCOMP, " + ;
							" HOMEDEPARTMENT AS HOMEDEPT, " + ;
							" FILE# AS FILENUM, " + ;
							" NAME, " + ;
							" HIREDATE, " + ;
							" TERMINATIONDATE AS TERMDATE " + ;
							" FROM REPORTS.V_EMPLOYEE " + ;
							" WHERE (" + ;
							" {fn MONTH(HIREDATE)} = " + TRANSFORM(lnLastMonth) + ;
							" AND {fn YEAR(HIREDATE)} = " + TRANSFORM(lnLastYear) + ;
							" ) " + ;
							" OR (" + ;
							" {fn MONTH(TERMINATIONDATE)} = " + TRANSFORM(lnLastMonth) + ;
							" AND {fn YEAR(TERMINATIONDATE)} = " + TRANSFORM(lnLastYear) + ;
							" ) "

						IF .lTestMode THEN
							.TrackProgress('                        TEST MODE                        ', LOGIT+SENDIT)
							.TrackProgress('lcSQL =' + lcSQL, LOGIT+SENDIT)
						ENDIF

						IF USED('SQLCURSOR1') THEN
							USE IN SQLCURSOR1
						ENDIF

						IF .ExecSQL(lcSQL, 'SQLCURSOR1', RETURN_DATA_MANDATORY) THEN

							SELECT SQLCURSOR1
							SCAN
								IF ISNULL(HIREDATE) THEN
									REPLACE SQLCURSOR1.HIREDATE WITH {}
								ENDIF
								IF ISNULL(TERMDATE) THEN
									REPLACE SQLCURSOR1.TERMDATE WITH {}
								ENDIF
							ENDSCAN

							IF USED('SQLCURSOR2') THEN
								USE IN SQLCURSOR2
							ENDIF

							SELECT STATUS, ADPCOMP, HOMEDEPT, FILENUM, NAME, ;
								TTOD(HIREDATE) AS HIREDATE, ;
								TTOD(TERMDATE) AS TERMDATE ;
								FROM SQLCURSOR1 ;
								INTO CURSOR SQLCURSOR2 ;
								ORDER BY STATUS, ADPCOMP, HOMEDEPT, FILENUM

							SELECT SQLCURSOR2
							GOTO TOP
							*BROWSE

							COPY TO (lcOutputFile) XL5
							.TrackProgress('Output to spreadsheet: ' + lcOutputFile, LOGIT+SENDIT+NOWAITIT)

							IF FILE(lcOutputFile) THEN
								* attach output file to email
								.cAttach = lcOutputFile
								.cBodyText = "ADP employees with Hire Dates or Termination Dates in the last month are in the attached spreadsheet." + ;
									CRLF + CRLF + "(do not reply - this is an automated report)" + ;
									CRLF + CRLF + "<report log follows>" + ;
									CRLF + .cBodyText
							ELSE
								.TrackProgress('ERROR: unable to email spreadsheet: ' + lcOutputFile, LOGIT+SENDIT+NOWAITIT)
							ENDIF

							CLOSE DATABASES ALL
						ENDIF  &&  .ExecSQL(lcSQL, 'SQLCURSOR1', RETURN_DATA_MANDATORY)

						.TrackProgress('ADP NEW HIRES/TERMS REPORT process ended normally.', LOGIT+SENDIT+NOWAITIT)
					ELSE
						* connection error
						.TrackProgress('Unable to connect to ADP Payroll System.', LOGIT+SENDIT)
					ENDIF   &&  .nSQLHandle > 0

			CATCH TO loError

				.TrackProgress('There was an error.',LOGIT+SENDIT)
				.TrackProgress('The Error # is: ' + TRANSFORM(loError.ErrorNo), LOGIT+SENDIT)
				.TrackProgress('The Error Message is: ' + TRANSFORM(loError.MESSAGE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred in Program: ' + TRANSFORM(loError.PROCEDURE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred at line #: ' + TRANSFORM(loError.LINENO), LOGIT+SENDIT)
				lnNumberOfErrors = lnNumberOfErrors + 1
				CLOSE DATA

			ENDTRY

			CLOSE DATA
			WAIT CLEAR
			***************** INTERNAL email results ******************************
			.TrackProgress('About to send status email.',LOGIT)
			.TrackProgress('The logfile is: ' + .cLogFile, SENDIT)
			.TrackProgress('ADP NEW HIRES/TERMS REPORT process started: ' + .cStartTime, LOGIT+SENDIT)
			.TrackProgress('ADP NEW HIRES/TERMS REPORT process finished: ' + TTOC(DATETIME()), LOGIT+SENDIT)

			IF .lSendInternalEmailIsOn THEN
				DO FORM dartmail2 WITH .cSendTo,.cFrom,.cSubject,.cCC,.cAttach,.cBodyText,"A"
				.TrackProgress('Sent status email.',LOGIT)
			ELSE
				.TrackProgress('Did not send status email: THIS.lSendInternalEmailIsOn = FALSE.',LOGIT)
			ENDIF

		ENDWITH
		RETURN
	ENDFUNC && main


	FUNCTION ExecSQL
		LPARAMETERS tcSQL, tcCursorName, tlNoDataReturnedIsOkay
		LOCAL llRetval, lnResult
		WITH THIS
			* close target cursor if it's open
			IF USED(tcCursorName)
				USE IN (tcCursorName)
			ENDIF
			WAIT WINDOW "Fetching remote data; please wait..." NOWAIT
			lnResult = sqlexec(.nSQLHandle, tcSQL, tcCursorName)
			llRetval = ( lnResult > 0 )
			IF llRetval THEN
				* see if any data came back
				IF NOT tlNoDataReturnedIsOkay THEN
					IF (NOT USED(tcCursorName)) OR EOF(tcCursorName) THEN
						llRetval = .F.
						.TrackProgress('No data was returned by this query: ' +CRLF + tcSQL, LOGIT+SENDIT)
						.cSendTo = 'Mark Bennett <mbennett@fmiint.com>'
					ENDIF
				ENDIF
			ELSE
				.TrackProgress("Result Code [" + TRANSFORM(lnResult) + "] returned by this query: " + tcSQL, LOGIT+SENDIT)
				.cSendTo = 'Mark Bennett <mbennett@fmiint.com>'
			ENDIF
			WAIT CLEAR
			RETURN llRetval
		ENDWITH
	ENDFUNC


	PROCEDURE TrackProgress
		* do any combination of Wait Window, writing to logfile, and adding to body of email,
		* based on nFlags parameter.
		LPARAMETERS tcExpression, tnFlags
		WITH THIS
			IF BITAND(tnFlags,LOGIT) = LOGIT THEN
				IF .lLoggingIsOn THEN
					?
					? .cProcessName + "  " + TTOC(DATETIME()) + ": " + tcExpression
				ENDIF
			ENDIF
			IF .lWaitWindowIsOn THEN
				IF BITAND(tnFlags,NOWAITIT) = NOWAITIT THEN
					WAIT WINDOW tcExpression NOWAIT
				ENDIF
				IF BITAND(tnFlags,WAITIT) = WAITIT THEN
					WAIT WINDOW tcExpression TIMEOUT .nWaitWindowTimeout
				ENDIF
			ENDIF
			IF BITAND(tnFlags,SENDIT) = SENDIT THEN
				IF .lSendInternalEmailIsOn THEN
					.cBodyText = .cBodyText + tcExpression + CRLF + CRLF
				ENDIF
			ENDIF
		ENDWITH
	ENDPROC  &&  TrackProgress



ENDDEFINE

