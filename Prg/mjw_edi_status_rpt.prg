*****
SET EXCLUSIVE OFF 
SET DELETED ON 
SET TALK OFF  
SET SAFETY OFF
close data all
_screen.windowstate=1
set enginebehavior 70

do m:\dev\prg\_setvars
set deleted off

use f:\3pl\data\edi_trigger in 0

if usesqloutwo("J")
	xsqlexec("select bol_no as bolnumber, del_date, consignee, ship_ref as picticket, outship.wo_num as workordernumber, " + ;
		"storenum, style, color, id, upc, totqty from outship, outdet where outship.mod='J' and outship.outshipid=outdet.outshipid " + ;
		"and outship.accountid=6303 and units=1 and del_date>={"+dtoc(date()-30)+"}","xdy",,"wh")

	select bolnumber, del_date, consignee, picticket, workordernumber, storenum, style, color, id, upc, totqty, ;
		space(12) as date945, space(25) as file945, space(20) as edi_status, 0000.00 as cost, 0000.00 as rprice, ;
		space(25) as desc from xdy order by del_date, bolnumber desc into cursor mjstats readwrite

else
	use f:\whj\whdata\outship in 0
	use f:\whj\whdata\outdet in 0

	select outship.bol_no as bolnumber,del_date,consignee,outship.ship_ref as picticket,outship.wo_num as workordernumber,storenum,outdet.style, outdet.color,outdet.id, outdet.upc,outdet.totqty,;
		space(12) as date945, space(25) as file945,space(20) as edi_status, 0000.00 as cost, 0000.00 as rprice, space(25) as desc from outship left outer join outdet on outdet.outshipid = outship.outshipid;
		where outship.accountid = 6303 and units and !emptynul(del_date) and del_date >= date()-30 into cursor mjstats readwrite order by del_date,bol_no desc
endif

*!*	Select ship_ref as pickticket,0000 as unitsshipped,000 as ctns,000000 as storenum,Space(15) as cacctnum,Space(20) as desc,Space(15) as upc,00000.00 as cost, 0000.00 as price,;
*!*	trig_time,when_proc,bol,wo_num,fin_status as status,file945 from edi_trigger where ;
*!*	accountid= 6303 and when_proc > Date()-10 into cursor mjstats readwrite

select mjstats
scan

	=seek(bolnumber,"edi_trigger","bol")
	if found("edi_trigger")
		replace mjstats.date945 with dtoc(edi_trigger.when_proc)
		replace mjstats.file945   with justfname(edi_trigger.file945)
		replace edi_status        with edi_trigger.fin_status
	else

	endif

endscan
upcmastsql(6303)
select * from upcmast where accountid=6303 into cursor xupcmast readwrite
select xupcmast
index on upc tag upc
select mjstats
scan

	=seek(mjstats.upc,"xupcmast","upc")
	if found("xupcmast")
		replace mjstats.cost   with xupcmast.price
		replace mjstats.rprice with xupcmast.rprice
		replace mjstats.desc   with xupcmast.descrip
	else

	endif

endscan
select mjstats
export to s:\marcjacobsdata\reports\mj_wholesale_delivery_detail.xls type xls
copy to s:\marcjacobsdata\reports\mj_wholesale_delivery_detail type csv
select * from mjstats where consignee='GSI'  into cursor gsi readwrite
COPY TO  s:\marcjacobsdata\reports\gsi_delivery_detail type csv
if substr(time(),1,2)='08'
	if reccount() > 0

		export to s:\marcjacobsdata\reports\gsi_delivery_detail.xls type xls

		tsendto = "tmarg@fmiint.com,l.lee@marcjacobs.com,p.markovic@marcjacobs.com"
		tattach = "s:\marcjacobsdata\reports\GSI_Delivery_Detail.xls"
		tcc =""
		tfrom ="Toll/FMI EDI System Operations <fmi-transload-ops@fmiint.com>"
		tmessage = "GSI Delivery detail report for   "+ttoc(datetime())
		tsubject = "GSI Delivery detail report"
		do form m:\dev\frm\dartmail2 with tsendto,tfrom,tsubject, tcc,tattach,tmessage  ,"A"
	else
		tsendto = "tmarg@fmiint.com,l.lee@marcjacobs.com,p.markovic@marcjacobs.com"
		tattach = ""
		tcc =""
		tfrom ="Toll/FMI EDI System Operations <fmi-transload-ops@fmiint.com>"
		tmessage = "NO GSI Delivery detail report"+ttoc(datetime())
		tsubject = "NO GSI Delivery detail report exist"
		do form m:\dev\frm\dartmail2 with tsendto,tfrom,tsubject, tcc,tattach,tmessage  ,"A"
	endif
else 
endif

use in xupcmast
use in mjstats
use in edi_trigger


