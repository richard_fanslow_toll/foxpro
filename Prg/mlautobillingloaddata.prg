*******************************************************************************************************
* Load Mira Loma Auto Bill data
*******************************************************************************************************
LPARAMETERS tcMode
LOCAL loMLAUTOBILLINGProc, lcClient
_screen.Caption = "Loading ML Auto Billing Data..."
SET PROCEDURE TO M:\DEV\PRG\MLAUTOBILLINGCLASS

loMLAUTOBILLINGProc = CREATEOBJECT('MLAUTOBILLING')

IF NOT EMPTY(tcMode) AND UPPER(ALLTRIM(tcMode)) = "AUTO" THEN
	loMLAUTOBILLINGProc.SetAutoMode()
ENDIF

* specify client name
*!*	lcClient = "XYZ" && THIS IS A BAD CLIENT NAME  FOR TESTING
*!*	lcClient = "DSG"
*!*	lcClient = "SEARS"
lcClient = "DSG-LOCALDATA"
*!*	lcClient = "SEARS-LOCALDATA"

IF NOT loMLAUTOBILLINGProc.SetClient( lcClient ) THEN
	=MESSAGEBOX('Error Setting Client',0+16,'ML Auto Billing Load Data')
ENDIF

* for testing
loMLAUTOBILLINGProc.ShowDataFiles()
loMLAUTOBILLINGProc.ShowEMailBodyText()
**************************************************************

loMLAUTOBILLINGProc.Load()

RETURN
