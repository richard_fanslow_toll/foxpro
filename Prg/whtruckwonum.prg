* fills in the trucking WO # on the WMS inbound WO
* fills in the WMS inbound WO # on the trucking WO

* updates: blank inwo in mainwolog if the ctr/awb and/or accountid have changed - mvw 05/15/06

lparameters xwonum, xawb, xcontainer, xaccountid

xawb=strtran(left(xawb,13)," ","")
if !"-"$xawb
	xawb=left(xawb,3)+"-"+substr(xawb,4,4)+"-"+substr(xawb,8,4)
endif
xcontainer=left(xcontainer,10)

select wolog
scan for inwo = xwonum
	cfilter = iif(type="O","container=xcontainer","awb=xawb")
	if not (&cfilter) or accountid # xaccountid
		cfilter = iif(type="A",'(reference="'+alltrim(wolog.awb)+'" or reference="'+dispawb(wolog.awb)+'")','container="'+wolog.container+'"')

		xmod=wf(goffice,wolog.accountid,,,,,,.t.)
		
		xsqlexec("select * from inwolog where mod='"+xmod+"' and accountid="+transform(wolog.accountid)+" " + ;
			"and between(wo_date,{"+dtoc(wolog.wo_date-60)+"},{"+dtoc(wolog.wo_date+60)+")} and "+cfilter+" " + ;
			"order by wo_date desc","csrinwolog",,"wh")
			
		replace inwo with csrinwolog.wo_num in wolog
		use in csrinwolog

		info(xwonum,,xaccountid,,,,,,"container/awb or acct changed, wolog.inwo deleted")
	endif
endscan

do case
case !empty(xcontainer) and seek(xcontainer,"wolog","container")
	select wolog
	scan for wo_date>date()-50 and container=xcontainer && and accountid=xaccountid - removed dy 10/10/11 
		replace inwo with xwonum in wolog
		xtruckwonum=xtruckwonum+transform(wolog.wo_num)+" "
		if !trim(wolog.seal)$xtruckseal
			xtruckseal=xtruckseal+trim(wolog.seal)+" "
		endif
	endscan

case !empty(xcontainer) and seek(xcontainer,"wolog","awb")
	select wolog
	scan for wo_date>date()-50 and awb=xcontainer && and accountid=xaccountid
		replace inwo with xwonum in wolog
		xtruckwonum=xtruckwonum+transform(wolog.wo_num)+" "
		if !trim(wolog.seal)$xtruckseal
			xtruckseal=xtruckseal+trim(wolog.seal)+" "
		endif
	endscan
	
case !empty(xawb) and seek(left(xawb,11),"wolog","awb")
	select wolog
	scan for wo_date>date()-50 and awb=xawb && and accountid=xaccountid
		replace inwo with xwonum in wolog
		xtruckwonum=xtruckwonum+transform(wolog.wo_num)+" "
		if !trim(wolog.seal)$xtruckseal
			xtruckseal=xtruckseal+trim(wolog.seal)+" "
		endif
	endscan
endcase

