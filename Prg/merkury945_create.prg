
*!* Merkury 945 (Whse. Shipping Advice) Creation Program
*!* Creation Date: 12.04.2014 by Joe (Derived from JDL945_create program)

PARAMETERS cOffice,cBOL
DO m:\dev\prg\_setvars WITH .F.  &&lTesting

SET ASSERTS OFF
ASSERT .F. MESSAGE "Now at start of Merkury STANDARD 945 process"

PUBLIC ARRAY thisarray(1)
PUBLIC c_CntrlNum,c_GrpCntrlNum,cReturned,cWO_Num,cWO_NumStr,nWO_Num1,lTesting,lDoUPC,nAcctNum,cRunningProg
PUBLIC lEmail,dDateTimeCal,nFilenum,nWO_Xcheck,nOutshipid,cCharge,lDoSQL,dapptnum,tfrom,lDS,lDoCatch,lOverflow
PUBLIC cCity,cState,cZip,nOrigSeq,nOrigGrpSeq,tsendto,tcc,tsendtoerr,tccerr,cErrMsg,cTrkBOL,cSCAC,lLoadSQLBL
PUBLIC cMod,cMBOL,cEDIType,lParcelType,cFilenameShort,cFilenameHold,cFilenameOut,cFilenameArch,lcPath,cWO_NumList,cISA_Num

lTesting   = .f.  && Set to .t. for testing
lTestinput = .F. && lTesting  &&  Set to .t. for test input files only!
lTestFileOut = .f.  && s/b left as .F.

TRY
	IF lTesting
		CLOSE DATABASES ALL
	ELSE
		CLOSETABLES()
	ENDIF
	IF (EMPTY(cOffice) OR VARTYPE(cOffice) # "C")
		IF lTesting
			cOffice = "C"
		ELSE
			WAIT WINDOW "Missing Office" TIMEOUT 2
			THROW
		ENDIF
	ENDIF

	nAcctNum = 6561
	cRunningProg = "merkury945_create"
	cMBOL = ""
	cMod = IIF(cOffice = "N","I","2")
	cEDIType = "945"

	gOffice = cMod
	gMasterOffice = cOffice
	lLoadSQLBL = .T.

	IF lTestinput
		cUseFolder = "F:\WHP\WHDATA\"
		cUseFolder = "M:\DEV\WHDATA\"
	ELSE
		cUseFolder = "F:\WH"+cMod+"\WHDATA\"
	ENDIF
	cRetMsg = "X"

	IF VARTYPE(cBOL) = "L"
		IF !lTesting AND !lTestinput
			WAIT WINDOW "BOL not provided...terminating" TIMEOUT 2
			lCloseOutput = .F.
			cErrMsg = "NO BOL"
			THROW
		ELSE
			CLOSE DATABASES ALL
			IF !USED('edi_trigger')
				cEDIFolder = "F:\3PL\DATA\"
				USE (cEDIFolder+"edi_trigger") IN 0 ALIAS edi_trigger
			ENDIF
*!* TEST DATA AREA
			cBOL = "1ZYW27310306222072"
			cTime = DATETIME()
			lFedEx = .F.
		ENDIF
	ENDIF

	useca("ackdata","wh")

	IF USED('OUTSHIP')
		USE IN outship
	ENDIF
	IF USED('outdet')
		USE IN outdet
	ENDIF

	lParcelType = .F.

	IF !USED("scacs")
		USE ("f:\3pl\data\parcel_carriers") IN 0 ALIAS scacs ORDER TAG scac
	ENDIF

	xsqlexec("select * from outship where accountid = "+TRANSFORM(nAcctNum)+" and bol_no = '"+cBOL+"'",,,"wh")
	INDEX ON bol_no TAG bol_no
	INDEX ON ship_ref TAG ship_ref
	SET ORDER TO
	lOverflow =.F.

	IF cBOL = '08447020001299503'
		SET DELETED OFF
		lOverflow = .T.
	ENDIF

	selectoutdet()
	SELECT outdet
	INDEX ON outdetid TAG outdetid

	SELECT outship
	lParcelType = IIF(SEEK(outship.scac,"scacs","scac"),.T.,.F.)
*	WAIT WINDOW "Parcel SCAC check in scac table...lParcelType = "+IIF(lParceltype,"TRUE","FALSE") TIMEOUT 20
	lParcelType = IIF(ALLTRIM(outship.keyrec) = ALLTRIM(outship.bol_no),.F.,lParcelType)
*	WAIT WINDOW "BOL to Keyrec check...lParcelType = "+IIF(lParceltype,"TRUE","FALSE") TIMEOUT 20
	
	
	nWO_Num = outship.wo_num
	cSCAC = ALLTRIM(outship.scac)
	lDS = IIF((outship.scac = "MIMK" OR INLIST(outship.cacctnum,"GROUPDS","LIVSO10","SUPPORT","QUIB","STAPL")),.T.,.F.)

	lDoSQL = .T.  && commented this out try to get a 945 out 9/25/2017  PG
	lPick = .T.
	lPrepack = .F.
	lJCP = .F.
	lIsError = .F.
	lDoCatch = .T.
	cShip_ref = ""
	lCloseOutput = .T.
	nFilenum = 0
	cCharge = "0.00"
	cWO_Num = ""
	cWO_NumStr = ""
	cWO_NumList = ""
	cString = ""
	cErrMsg = ""
	cTrkBOL = ""
	nPTCtnTot = 0

	lFederated = .F.
	nOrigSeq = 0
	nOrigGrpSeq = 0
	cPPName = "MERKURY"
	WAIT WINDOW "At the start of "+cPPName+" 945 process..." NOWAIT
	tfrom = "TGF EDI Operations <toll-edi-ops@tollgroup.com>"
	tmessage=""
	tcc = ""
	lDoBOLGEN = .F.
	lSamples = .F.
	cBOL1 = ""
	lISAFlag = .T.
	lSTFlag = .T.
	nLoadid = 1
	lSQLMail = .F.

	CREATE CURSOR temp945 (accountid i,isa_num c(10),st_num c(10),wo_num i,bol_no c(20),ship_ref c(20),filename c(50),filedate T)
	cBOL=TRIM(cBOL)

	lEmail = .T.
	lTestMail = lTesting && Sends mail to Joe only
*	lTestMail = .t.
	lStandalone = lTesting

	STORE "LB" TO cWeightUnit
	cfd = "*"
	csegd = "~"
	nSegCtr = 0
	cterminator = ">" && Used at end of ISA segment

	DO m:\dev\prg\swc_cutctns_gen WITH cBOL


	WAIT WINDOW "Now preparing tables and variables" NOWAIT NOCLEAR

*!* SET CUSTOMER CONSTANTS
	ccustname = "MERKURY"  && Customer Identifier
	cX12 = "004010"  && X12 Standards Set used
	cMailName = "Merkury"
	SELECT 0
	USE F:\3pl\DATA\mailmaster ALIAS mm
	LOCATE FOR mm.edi_type = "945" AND mm.office = cOffice AND mm.accountid = nAcctNum
	tsendto = IIF(mm.use_alt,sendtoalt,sendto)
	tcc = IIF(mm.use_alt,ccalt,cc)
	lcPath = ALLTRIM(mm.basepath)
	lcHoldPath = ALLTRIM(mm.holdpath)
	lcArchivePath =  ALLTRIM(mm.archpath)
	LOCATE
	LOCATE FOR edi_type = "MISC" AND taskname = "GENERAL"
	tsendtoerr = IIF(mm.use_alt,sendtoalt,sendto)
	tccerr = IIF(mm.use_alt,ccalt,cc)
	tsendtotest = IIF(mm.use_alt,sendtoalt,sendto)
	tcctest = IIF(mm.use_alt,ccalt,cc)
	USE IN mm
	upcmastsql(6561)
	INDEX ON STYLE TAG STYLE

*!* SET OTHER CONSTANTS
	DO CASE
		CASE cOffice = "N"
			cCustLoc =  "NJ"
			cFMIWarehouse = ""
			cCustPrefix = "945j"
			cDivision = "New Jersey"
			cSF_Addr1  = "800 FEDERAL BLVD"
			cSF_CSZ    =  "CARTERET"+cfd+"NJ"+cfd+"07008"

		CASE cOffice = "M"
			cCustLoc =  "FL"
			cFMIWarehouse = ""
			cCustPrefix = "945f"
			cDivision = "Florida"
			cSF_Addr1  = "11400 NW 32ND AVE"
			cSF_CSZ    = "MIAMI"+cfd+"FL"+cfd+"33167"

		OTHERWISE
			cCustLoc =  "CA"
			cFMIWarehouse = ""
			cCustPrefix = "945c"
			cDivision = "California"
			cSF_Addr1  = "450 WESTMONT DRIVE"
			cSF_CSZ    = "SAN PEDRO"+cfd+"CA"+cfd+"90731"
	ENDCASE
	IF lTesting
		cCustPrefix = "945t"
	ENDIF

	cCustFolder = UPPER(ccustname)
	cOrigBOL = cBOL

	lNonEDI = .F.  && Assumes not a Non-EDI 945
	STORE "" TO lcKey
	STORE 0 TO alength,nLength
	cTime = DATETIME()
	cDelTime = SUBSTR(TTOC(cTime,1),9,4)
	cCarrierType = "M" && Default as "Motor Freight"
	dapptnum = ""

*!* SET OTHER CONSTANTS
	IF INLIST(cOffice,'C','1','2')
		dDateTimeCal = (DATETIME()-(3*3600))
	ELSE
		dDateTimeCal = DATETIME()
	ENDIF
	dt1 = TTOC(dDateTimeCal,1)
	dtmail = TTOC(dDateTimeCal)
	dt2 = dDateTimeCal
	cString = ""

	csendqual = "ZZ"
	csendid = "FMIX"+cCustLoc

	crecqual = "12"
	crecid = "2129315055"

	cdate = DTOS(DATE())
	cTruncDate = RIGHT(cdate,6)
	cTruncTime = SUBSTR(TTOC(dDateTimeCal,1),9,4)
	cfiledate = cdate+cTruncTime
	cOrig = "J"
	nSTCount = 0
	cStyle = ""
	cPTString = ""
	nTotCtnWt = 0
	nTotCtnCount = 0
	nUnitSum = 0
	cTargetStyle = ""

** PAD ID Codes
	csendidlong = PADR(csendid,15," ")
	crecidlong = PADR(crecid,15," ")

	cFilenameHold = (lcHoldPath+cCustPrefix+dt1+".txt")
	cFilenameShort = JUSTFNAME(cFilenameHold)
	cFilenameOut = IIF(lTesting or lTestFileOut, ("F:\FTPUSERS\Merkury\945OUT-Test\"+cFilenameShort),(lcPath+cFilenameShort))
	cFilenameArch = (lcArchivePath+cFilenameShort)
	nFilenum = FCREATE(cFilenameHold)

	SELECT outshipid,ship_ref ;
		FROM outship ;
		WHERE bol_no = cBOL ;
		AND accountid = nAcctNum ;
		INTO CURSOR tempsr
	SELECT tempsr
	WAIT WINDOW "There are "+ALLTRIM(STR(RECCOUNT()))+" outshipid's to check" NOWAIT

	SELECT outship
*!*		IF !SEEK(cBOL,'outship','bol_no')
*!*			ASSERT .F. MESSAGE "at missing BOL point"
*!*			WAIT WINDOW "BOL not found in OUTSHIP" TIMEOUT 2
*!*			cErrMsg = "BOL NOT FOUND"
*!*			THROW
*!*		ENDIF

	IF USED('BL')
		USE IN bl
	ENDIF

	csq1 = [select * from bl where bol_no = ']+cBOL+[']
	xsqlexec(csq1,,,"wh")
	SELECT  bl
	INDEX ON bol_no TAG bol_no

	IF !lTestinput AND lParcelType
		IF USED('SHIPMENT')
			USE IN shipment
		ENDIF

		IF USED('PACKAGE')
			USE IN package
		ENDIF

		xsqlexec("select * from shipment where 1=0",,,"wh")  && Sets up an empty cursor
		SELECT outship && temp cursor with all PTs on the BOL

		SCAN
			cSR = ALLTRIM(outship.ship_ref)
			lcQuery ="select * from shipment where right(rtrim(str(accountid)),4)  in (6561,9999) and pickticket = '"+cSR+"'"
			xsqlexec(lcQuery,"sm1",,"wh")
			SELECT sm1
			LOCATE
			IF RECCOUNT()=0
				cErrMsg = "NO SHIPMENT DATA"
				THROW
			ENDIF

			SELECT shipment
			APPEND FROM DBF('sm1')
			USE IN sm1
		ENDSCAN

		SELECT shipment
		INDEX ON shipmentid TAG shipmentid
		INDEX ON pickticket TAG pickticket

		LOCATE
		IF RECCOUNT("shipment") > 0
			xjfilter="shipmentid in ("
			SCAN
				nShipmentId = shipment.shipmentid
				xsqlexec("select * from package where shipmentid="+TRANSFORM(nShipmentId),,,"wh")

				IF RECCOUNT("package")>0
					xjfilter=xjfilter+TRANSFORM(shipmentid)+","
				ELSE
*					xjfilter="1=0"
				ENDIF
			ENDSCAN
			xjfilter=LEFT(xjfilter,LEN(xjfilter)-1)+")"

			xsqlexec("select * from package where "+xjfilter,,,"wh")
		ELSE
			xsqlexec("select * from package where .f.",,,"wh")
		ENDIF
		SELECT package

		INDEX ON ucc TAG ucc
		INDEX ON shipmentid TAG shipmentid
		SET ORDER TO TAG shipmentid

		SELECT shipment
		SET RELATION TO shipmentid INTO package
	ENDIF

	DO m:\dev\prg\swc_cutctns_gen WITH cBOL

&& Added code blocks to the following to overcome BOL vs. WO selection issues, 12.23.2005


	IF lDoSQL
		cSQL="SQL3"
		SELECT outship
		LOCATE
		IF USED("sqlwomerk")
			USE IN sqlwomerk
		ENDIF
		IF FILE("F:\3pl\DATA\sqlwomerk.dbf")
			DELETE FILE "F:\3pl\DATA\sqlwomerk.dbf"
		ENDIF
		SELECT bol_no,wo_num ;
			FROM outship ;
			WHERE outship.bol_no == cBOL ;
			AND outship.accountid = nAcctNum ;
			AND outship.wo_num # 5333631 ;
			GROUP BY 1,2 ;
			ORDER BY 1,2 ;
			INTO DBF F:\3pl\DATA\sqlwomerk
		USE IN sqlwomerk
		SELECT 0
		USE F:\3pl\DATA\sqlwomerk ALIAS sqlwomerk
		LOCATE

		IF EOF()
			cErrMsg = "NO SQL DATA"
			THROW
		ENDIF

		IF lTesting
			BROWSE
		ENDIF
		cRetMsg = ""
		ASSERT .F. MESSAGE "At SQL connect...debug"

		DO m:\dev\prg\sqlconnectmerk_bol  WITH nAcctNum,ccustname,cPPName,.T.,cOffice,.T.

		IF cRetMsg<>"OK"
			STORE cRetMsg TO cErrMsg
			THROW
		ENDIF

		SELECT vmerkurypp
		LOCATE
		IF lTesting
*			BROWSE
*			ASSERT .F.
		ENDIF

		IF EOF()
			WAIT WINDOW "SQL select data is EMPTY...error!" NOWAIT
			cErrMsg = "SQL DATA EMPTY"
			THROW
		ENDIF
	ENDIF
************************************************************************
* PG added this to do a pre-check on all the PTs fo a QTY check so the
* error email will indicate the which PTs have the issue
************************************************************************

	llAllQtysOK = .T.

*	cErrMsg ="Pick Tickets with Sql.totqty vs Outdet.totqty errors on shipment: "+cBOL+CHR(13)+"-------------------------------------------------------------------"+CHR(13)

	SELECT ship_ref,outshipid,shipins FROM outship WHERE outship.bol_no= cBOL AND !" OV"$outship.ship_ref INTO CURSOR outshipids
	SELECT outshipids
	SCAN
		SELECT outdet
		SET ORDER TO
		IF lOverflow
			SUM origqty TO nUnitTot1 FOR  units AND outdet.outshipid = outshipids.outshipid
		ELSE
			SUM totqty TO nUnitTot1 FOR  units AND outdet.outshipid = outshipids.outshipid
		ENDIF
		IF !USED("vmerkurypp")
			cErrMsg = "No Data in vmerkurypp"
			THROW
		ELSE
			SELECT vmerkurypp
		ENDIF

		IF lOverflow
			SET DELETED OFF
			SUM totqty TO nUnitTot2 FOR vmerkurypp.outshipid = outshipids.outshipid
			IF nUnitTot1<>nUnitTot2
				SET STEP ON
				llPType="UNK"
				IF "PICKPACK"$outshipids.shipins
					llPType = "PICKPACK"
				ENDIF
				IF "SCANPACK"$outshipids.shipins
					llPType = "SCANPACK"
				ENDIF
				IF "PREPACK"$outshipids.shipins
					llPType = "PREPACK "
				ENDIF
				SELECT vmerkurypp
				LOCATE FOR vmerkurypp.outshipid = outshipids.outshipid
				cErrMsg = cErrMsg+"Pick Ticket: "+ALLTRIM(outshipids.ship_ref)+" -("+llPType+")  Outdet totqty = "+PADR(ALLTRIM(TRANSFORM(nUnitTot1)),6," ")+"  SQL Cartons TotQty:"+PADR(ALLTRIM(TRANSFORM(nUnitTot2)),6," ")+CHR(13)
				llAllQtysOK = .F.
			ENDIF
		ENDIF
	ENDSCAN

	IF cBOL = '08447020001070294'
		llAllQtysOK = .T.
	ENDIF

	IF llAllQtysOK = .F.
		SET STEP ON
		tsendto ="pgaidis@fmiint.com,joe.bianchi@tollgroup.com"
		tcc=""
		tsubject ="Merkury Outdet/SQL Qty Errors on BOL/Trk#: "+cBOL+" at "+TTOC(DATETIME())
		tattach=""
		tmessage = cErrMsg
		DO FORM m:\dev\frm\dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"

		cErrMsg= "SQL/OTDET QTY ERR"
		DO ediupdate WITH cErrMsg,.T.
		THROW
	ENDIF
*************************************************************************
*  End of the PT/SQL check
*************************************************************************
	SELECT outship
	LOCATE
	WAIT CLEAR
	WAIT WINDOW "Now creating Header information" NOWAIT NOCLEAR

*!* HEADER LEVEL EDI DATA
	DO num_incr_isa

	cISA_Num = PADL(c_CntrlNum,9,"0")
	nISA_Num = INT(VAL(cISA_Num))

	cISACode = IIF(lTesting,"T","P")

	STORE "ISA"+cfd+"00"+cfd+"          "+cfd+"00"+cfd+"          "+cfd+csendqual+cfd+csendidlong+cfd+;
		crecqual+cfd+crecidlong+cfd+cTruncDate+cfd+cTruncTime+cfd+"U"+cfd+"00401"+cfd+;
		cISA_Num+cfd+"0"+cfd+cISACode+cfd+cterminator+csegd TO cString
	DO cstringbreak

	STORE "GS"+cfd+"SW"+cfd+csendid+cfd+crecid+cfd+cdate+cfd+cTruncTime+cfd+c_CntrlNum+;
		cfd+"X"+cfd+cX12+csegd TO cString
	DO cstringbreak

	SELECT outship
	LOCATE

	IF !lTesting
		IF EMPTY(cBOL)  && May be empty if triggered from PT screen, or FedEx shipment
			WAIT CLEAR
			WAIT WINDOW "MISSING Bill of Lading Number!" TIMEOUT 2
			cErrMsg = "BOL# EMPTY"
			THROW
		ELSE
			IF !SEEK(PADR(TRIM(cBOL),20),"outship","bol_no")
				WAIT CLEAR
				WAIT WINDOW "Invalid Bill of Lading Number - Not Found!" TIMEOUT 2
				cErrMsg = "BOL# NOT FOUND"
				THROW
			ENDIF
		ENDIF
	ENDIF

*************************************************************************
*1	PTIDSOURCE/OUTSHIP RECORD MAIN LOOP
*	Loops through individual OUTSHIP lines for BOL#
*************************************************************************
	WAIT CLEAR
	WAIT WINDOW "Now creating BOL#-based information..." NOWAIT NOCLEAR


	SELECT outship
	SET ORDER TO
	COUNT TO N FOR outship.bol_no = cBOL AND IIF(lTesting,!EMPTYnul(wo_date),!EMPTYnul(del_date))
	IF N=0
		cErrMsg = "INCOMP BOL"
		THROW
	ENDIF

	LOCATE
	cMissDel = ""
	nPTCount = 0

	oscanstr = "outship.bol_no = cBOL AND outship.accountid  = nAcctNum and IIF(lTesting,!EMPTYnul(wo_date),!EMPTYnul(del_date))"
	SELECT outship

	SCAN FOR &oscanstr
		alength = ALINES(apt,outship.shipins,.T.,CHR(13))
		SCATTER MEMVAR MEMO
		nWO_Num = m.wo_num
		cWO_Num = ALLTRIM(STR(nWO_Num))
		cShip_ref = ALLTRIM(m.ship_ref)
		IF " OV"$cShip_ref
			LOOP
		ENDIF
		nPTCount = nPTCount + 1

		IF "PENNEY"$UPPER(m.consignee) OR UPPER(m.consignee)="JCP"
			lJCP = .T.
		ENDIF
		nOutshipid = m.outshipid
		nWO_Num = m.wo_num
		cWO_Num = ALLTRIM(STR(m.wo_num))
		cPackType = ALLTRIM(segmentget(@apt,"PROCESSMODE",alength))

		lPrepack = IIF(cPackType#"PICKPACK",.T.,.F.)
		m.scac = ALLTRIM(outship.scac)
		IF EMPTY(m.scac)
			DO CASE
				CASE "SMARTPOST"$outship.ship_via
					m.scac = "FSP"
					REPLACE outship.scac WITH m.scac IN outship NEXT 1
					REPLACE edi_trigger.scac WITH m.scac IN edi_trigger NEXT 1
* delete when going live
				CASE "SUREPOST"$outship.ship_via
					m.scac = "UPMS"
					REPLACE outship.scac WITH m.scac IN outship NEXT 1
					REPLACE edi_trigger.scac WITH m.scac IN edi_trigger NEXT 1
* delete when going live
				CASE "USPS"$outship.ship_via AND "FIRST CLASS"$outship.ship_via
					m.scac = "SFIR"
					REPLACE outship.scac WITH m.scac IN outship NEXT 1
					REPLACE edi_trigger.scac WITH m.scac IN edi_trigger NEXT 1
* delete when going live
				OTHERWISE
					cErrMsg = "MISS PARCEL SCAC in PT "+cShip_ref
					THROW
			ENDCASE
		ENDIF

		cSCAC = m.scac

		IF !lParcelType
			cTrackNum = ALLT(m.keyrec)  && ProNum if available
			cCharge = "0.00"
		ELSE
*			cBOL = PADL(ALLT(STR(outship.wo_num)),17,'0')
			cTrackNum = ALLT(outship.bol_no)  && UPS Tracking Number
			IF !lTestinput
				ASSERT .F. MESSAGE "At UPS package charges"
				SELECT shipment
				LOCATE FOR (VAL(RIGHT(STR(shipment.accountid),4)) = nAcctNum) AND shipment.pickticket = PADR(cShip_ref,20)
				IF !FOUND()
					LOCATE FOR shipment.accountid = 9999 AND shipment.pickticket = PADR(cShip_ref,20)
					IF !FOUND()
						cErrMsg = "MISS UPS REC"+cShip_ref
						THROW
					ELSE
						cTrkBOL = ALLTRIM(package.trknumber)
						SELECT package
						LOCATE
						SUM pkgcharge TO nCharge FOR package.shipmentid = shipment.shipmentid
						cCharge = ALLT(STR(nCharge,8,2))
					ENDIF
				ELSE
					cTrkBOL = ALLTRIM(package.trknumber)
					SELECT package
					LOCATE
					SUM pkgcharge TO nCharge FOR package.shipmentid = shipment.shipmentid
					cCharge = ALLT(STR(nCharge,8,2))
				ENDIF
			ELSE
				cCharge = "25.50"
			ENDIF
		ENDIF

		nTotCtnCount = m.qty
		cPO_Num = ALLTRIM(m.cnee_ref)
		cShip_ref = ALLTRIM(m.ship_ref)

*!* Added this code to trap miscounts in OUTDET Units
		IF lPrepack
			SELECT outdet
			SET ORDER TO
			IF lOverflow
				SET DELETED OFF
				SUM origqty TO nCtnTot1 FOR !units AND outdet.outshipid = outship.outshipid
			ELSE
				SUM totqty TO nCtnTot1 FOR !units AND outdet.outshipid = outship.outshipid
			ENDIF


*!*	Check carton count
			IF lOverflow
				SET DELETED OFF
				SELECT DISTINCT ucc ;
					FROM vmerkurypp ;
					WHERE vmerkurypp.outshipid = outship.outshipid ;
					AND vmerkurypp.ucc#"CUTS" ;
					AND vmerkurypp.totqty > 0 ;
					INTO CURSOR tempsqlx
			ELSE
				SELECT DISTINCT ucc ;
					FROM vmerkurypp ;
					WHERE vmerkurypp.outshipid = outship.outshipid ;
					AND vmerkurypp.ucc#"CUTS" ;
					AND vmerkurypp.totqty > 0 ;
					INTO CURSOR tempsqlx
			ENDIF

			COUNT TO nCtnTot2
			USE IN tempsqlx
			IF nCtnTot1<>nCtnTot2
				ASSERT .F. MESSAGE "At CTN QTY error"
				SET STEP ON
				cErrMsg = "SQL CTNQTY ERR, PT "+cShip_ref
				THROW
			ENDIF
		ELSE
			SELECT outdet
			IF lOverflow
				SET DELETED OFF
				SUM origqty TO nUnitTot1 FOR outdet.units=.T. AND outdet.outshipid = outship.outshipid
			ELSE
				SUM totqty TO nUnitTot1 FOR outdet.units=.T. AND outdet.outshipid = outship.outshipid
			ENDIF

			SELECT vmerkurypp
			SUM vmerkurypp.totqty TO nUnitTot2 FOR vmerkurypp.outshipid = outship.outshipid AND vmerkurypp.accountid = nAcctNum
			IF nUnitTot1<>nUnitTot2
				ASSERT .F. MESSAGE "IN TOTQTY COMPARE"
				SET STEP ON
				cErrMsg = "SQL UNITQTY ERR, PT "+cShip_ref
				THROW
			ENDIF
		ENDIF

		SELECT outdet
		SET ORDER TO outdetid
		LOCATE

*!* End code addition

		IF ("PENNEY"$UPPER(outship.consignee) OR "KMART"$UPPER(outship.consignee) OR "K-MART"$UPPER(outship.consignee)) AND !lDS
			lApptFlag = .T.
		ELSE
			lApptFlag = .F.
		ENDIF

		IF lTestinput
			ddel_date = DATE()
			dapptnum = "99999"
			dapptdate = DATE()
		ELSE
			ddel_date = outship.del_date
			IF EMPTY(ddel_date)
				cMissDel = IIF(EMPTY(cMissDel),"The following PTs had no Delivery Dates:"+CHR(13)+TRIM(cShip_ref),cMissDel+CHR(13)+TRIM(cShip_ref))
			ENDIF
			dapptnum = ALLTRIM(outship.appt_num)

			IF EMPTY(dapptnum) && Penney/KMart Appt Number check
				IF !(lApptFlag)
					dapptnum = ""
				ELSE
					cErrMsg = "EMPTY APPT #"
					THROW
				ENDIF
			ENDIF
			dapptdate = outship.appt

			IF EMPTY(dapptdate)
				dapptdate = outship.del_date
			ENDIF
		ENDIF

		IF !(cWO_Num$cWO_NumStr)
			cWO_NumStr = IIF(EMPTY(cWO_NumStr),cWO_Num,cWO_NumStr+","+cWO_Num)
		ENDIF

		IF !(cWO_Num$cWO_NumList)
			cWO_NumList = IIF(EMPTY(cWO_NumList),cWO_Num,cWO_NumStr+CHR(13)+cWO_Num)
		ENDIF

		IF ALLTRIM(outship.SForCSZ) = ","
			BLANK FIELDS outship.SForCSZ NEXT 1 IN outship
		ENDIF

		cTRNum = ""
		cPRONum = ""

		cPRONum = outship.keyrec
		IF EMPTY(outship.keyrec)
			cPRONum = outship.appt_num
		ENDIF

*		cPTString = IIF(EMPTY(cPTString),m.consignee+" "+cShip_ref,cPTString+CHR(13)+m.consignee+" "+cShip_ref)

		m.CSZ = TRIM(m.CSZ)
		IF EMPTY(ALLT(STRTRAN(M.CSZ,",","")))
			WAIT CLEAR
			WAIT WINDOW "No SHIP-TO City/State/ZIP info...exiting" TIMEOUT 2
			cErrMsg = "NO CSZ INFO"
			THROW
		ENDIF
		IF !(", "$m.CSZ)
			m.CSZ = ALLTRIM(STRTRAN(m.CSZ,",",", "))
		ENDIF
		m.CSZ = ALLTRIM(STRTRAN(m.CSZ,"  "," "))
		cCity = ALLT(LEFT(TRIM(m.CSZ),AT(",",m.CSZ)-1))
		cStateZip = ALLT(SUBSTR(TRIM(m.CSZ),AT(",",m.CSZ)+1))
		cState = ALLT(LEFT(cStateZip,2))
		cZip = ALLT(SUBSTR(cStateZip,3))

		STORE "" TO cSForCity,cSForState,cSForZip
		IF !lFederated
			m.SForCSZ = ALLTRIM(STRTRAN(m.SForCSZ,"  "," "))
			nSpaces = OCCURS(" ",ALLTRIM(m.SForCSZ))
			IF nSpaces = 0
				m.SForCSZ = STRTRAN(m.SForCSZ,",","")
				cSForCity = ALLTRIM(m.SForCSZ)
				cSForState = ""
				cSForZip = ""
			ELSE
				nCommaPos = AT(",",m.SForCSZ)
				nLastSpace = AT(" ",m.SForCSZ,nSpaces)
				nMinusSpaces = IIF(nSpaces=1,0,1)
				IF ISALPHA(SUBSTR(TRIM(m.SForCSZ),AT(" ",TRIM(m.SForCSZ),nSpaces-nMinusSpaces)+1,2))
					cSForCity = LEFT(TRIM(m.SForCSZ),AT(",",m.SForCSZ)-1)
					cSForState = SUBSTR(m.SForCSZ,nCommaPos+2,2)
					cSForZip = TRIM(RIGHT(TRIM(m.SForCSZ),5))
					IF ISALPHA(cSForZip)
						cSForZip = ""
					ENDIF
				ELSE
					WAIT CLEAR
					WAIT WINDOW "NOT ALPHA: "+SUBSTR(TRIM(m.SForCSZ),AT(" ",m.SForCSZ,nSpaces-1)+1,2) TIMEOUT 3
					cSForCity = LEFT(TRIM(m.SForCSZ),AT(",",m.SForCSZ)-1)
					cSForState = SUBSTR(TRIM(m.SForCSZ),AT(" ",m.SForCSZ,nSpaces-2)+1,2)
					cSForZip = TRIM(RIGHT(TRIM(m.SForCSZ),5))
					IF ISALPHA(cSForZip)
						cSForZip = ""
					ENDIF
				ENDIF
			ENDIF
		ELSE
			cStoreName = segmentget(@apt,"STORENAME",alength)
		ENDIF

		DO num_incr_st
		WAIT CLEAR

		INSERT INTO temp945 (accountid,isa_num,st_num,wo_num,bol_no,ship_ref,filename,filedate) ;
			VALUES (m.accountid,c_CntrlNum,c_GrpCntrlNum,m.wo_num,m.bol_no,m.ship_ref,cFilenameShort,dt2)
&&&& added 1/23/17 TMARG to capture data for 997

		m.groupnum=c_CntrlNum
		m.isanum=cISA_Num
		m.transnum=PADL(c_GrpCntrlNum,9,"0")
		m.edicode="SW"
		m.accountid=m.accountid
		m.loaddt=DATE()
		m.loadtime=DATETIME()
		m.filename=cFilenameShort
		m.ship_ref=m.ship_ref
		insertinto("ackdata","wh",.T.)
		tu("ackdata")

		STORE "ST"+cfd+"945"+cfd+PADL(c_GrpCntrlNum,9,"0")+csegd TO cString
		DO cstringbreak
		nSTCount = nSTCount + 1
		nSegCtr = 1

		IF lParcelType
			cLongTrkNum = ALLTRIM(segmentget(@apt,"LONGTRKNUM",alength))  &&  Will use >20-character USPS code if present.
			cBOL_Send = IIF(LEN(cLongTrkNum)=0,cBOL,cLongTrkNum)
		ELSE
			cBOL_Send = cBOL
		ENDIF

		cShip_refW06 = ALLTRIM(segmentget(@apt,"OLDPT",alength))
		cPTString = IIF(EMPTY(cPTString),m.consignee+" "+cShip_ref+SPACE(5)+cShip_refW06,cPTString+CHR(13)+m.consignee+" "+cShip_ref+SPACE(5)+cShip_refW06)
		STORE "W06"+cfd+"N"+cfd+cShip_refW06+cfd+cdate+cfd+TRIM(cBOL_Send)+cfd+TRIM(cWO_Num)+cfd+cPO_Num+csegd TO cString
		DO cstringbreak
		nSegCtr = nSegCtr + 1

		nCtnNumber = 1  && Seed carton sequence count

		cStoreNum = m.dcnum
		IF EMPTY(cStoreNum)
			STORE "N1"+cfd+"ST"+cfd+ALLTRIM(m.consignee)+csegd TO cString
		ELSE
			STORE "N1"+cfd+"ST"+cfd+ALLTRIM(m.consignee)+cfd+"92"+cfd+cStoreNum+csegd TO cString
		ENDIF
		DO cstringbreak
		nSegCtr = nSegCtr + 1

		cCountry = segmentget(@apt,"COUNTRY",alength)
		cCountry = ALLT(cCountry)
		IF EMPTY(cCountry)
			cCountry = ALLTRIM(outship.country)
			IF EMPTY(cCountry)
				cCountry = "USA"
			ENDIF
		ENDIF
		STORE "N3"+cfd+TRIM(outship.address)+IIF(!EMPTY(outship.address2),ALLTRIM(outship.address2)+csegd,csegd) TO cString
		DO cstringbreak
		nSegCtr = nSegCtr + 1

		IF m.consignee = "TJX UK"
			STORE "N4*HERTS, ENGLAND*HT*WD17 1TX*GB"+csegd TO cString
		ELSE
			STORE "N4"+cfd+cCity+cfd+cState+cfd+cZip+cfd+cCountry+csegd TO cString
		ENDIF
		DO cstringbreak
		nSegCtr = nSegCtr + 1

		cSFStoreNum = segmentget(@apt,"SFSTORENUM",alength)
		IF EMPTY(cSFStoreNum)
			cSFStoreNum = ALLTRIM(m.sforstore)
		ENDIF
		IF !EMPTY(m.shipfor)
			IF EMPTY(ALLTRIM(m.sforstore))
				STORE "N1"+cfd+"Z7"+cfd+ALLTRIM(m.shipfor)+csegd TO cString
			ELSE
				STORE "N1"+cfd+"Z7"+cfd+ALLTRIM(m.shipfor)+cfd+"92"+cfd+cSFStoreNum+csegd TO cString
			ENDIF
			DO cstringbreak
			nSegCtr = nSegCtr + 1

			STORE "N3"+cfd+TRIM(m.sforaddr1)+csegd TO cString
			DO cstringbreak
			nSegCtr = nSegCtr + 1

			IF !EMPTY(cSForState)
				IF m.consignee = "TJX UK"
					STORE "N4*HERTS, ENGLAND*HT*WD17 1TX*GB"+csegd TO cString
				ELSE
					STORE "N4"+cfd+cCity+cfd+cState+cfd+cZip+cfd+"USA"+csegd TO cString
				ENDIF
			ELSE
				STORE "N4"+cfd+cSForCity+csegd TO cString
			ENDIF
			DO cstringbreak
			nSegCtr = nSegCtr + 1
		ENDIF

		FOR nom = 1 TO 3
			cNom = ICASE(nom=1,"SD",nom=2,"PF","MA")
			cNomLoc = cNom+"_N1"
			cNomInfo = ALLTRIM(segmentget(@apt,cNomLoc,alength))
*			cNomInfo = STRTRAN(cNomInfo,"|",cfd)
			IF EMPTY(cNomInfo)
				LOOP
			ENDIF

			STORE "N1"+cfd+cNom+cfd+cNomInfo+csegd TO cString
			DO cstringbreak
			nSegCtr = nSegCtr + 1

			cNomLoc = cNom+"_N2"
			cNomInfo = ALLTRIM(segmentget(@apt,cNomLoc,alength))
			cNomInfo = STRTRAN(cNomInfo,"|",cfd)
			IF !EMPTY(cNomInfo)
				STORE "N2"+cfd+cNomInfo+csegd TO cString
				DO cstringbreak
				nSegCtr = nSegCtr + 1
			ENDIF

			cNomLoc = cNom+"_N3"
			cNomInfo = ALLTRIM(segmentget(@apt,cNomLoc,alength))
			cNomInfo = STRTRAN(cNomInfo,"|",cfd)
			IF !EMPTY(cNomInfo)
				STORE "N3"+cfd+cNomInfo+csegd TO cString
				DO cstringbreak
				nSegCtr = nSegCtr + 1
			ENDIF

			cNomLoc = cNom+"_N4"
			cNomInfo = ALLTRIM(segmentget(@apt,cNomLoc,alength))
			cNomInfo = STRTRAN(cNomInfo,"|",cfd)
			IF !EMPTY(cNomInfo)
				IF m.consignee = "TJX UK"
					STORE "N4"+cfd+cNomInfo+cfd+"GB"+csegd TO cString
				ELSE
					STORE "N4"+cfd+cNomInfo+csegd TO cString
				ENDIF
				DO cstringbreak
				nSegCtr = nSegCtr + 1
			ENDIF
		ENDFOR
		RELEASE cNomLoc,cNomInfo

		STORE "N1"+cfd+"CA"+cfd+ALLTRIM(outship.ship_via)+cfd+"92"+cfd+ALLTRIM(outship.scac)+csegd TO cString
		DO cstringbreak
		nSegCtr = nSegCtr + 1

		IF !lParcelType  && not small package,,
			=SEEK(cBOL,"bl","bol_no")
			IF FOUND("bl")
				IF !EMPTY(bl.mblnum) && look to see if its a master BOL
					STORE "N9"+cfd+"MB"+cfd+ALLTRIM(bl.mblnum)+csegd TO cString
					DO cstringbreak
					nSegCtr = nSegCtr + 1

					STORE "N9"+cfd+"BM"+cfd+ALLTRIM(bl.bol_no)+csegd TO cString
					DO cstringbreak
					nSegCtr = nSegCtr + 1

					STORE "N9"+cfd+"CN"+cfd+ALLTRIM(cPRONum)+csegd TO cString
					DO cstringbreak
					nSegCtr = nSegCtr + 1
				ELSE && not a master so all segs are just the lcBOL
					STORE "N9"+cfd+"MB"+cfd+ALLTRIM(cBOL)+csegd TO cString
					DO cstringbreak
					nSegCtr = nSegCtr + 1

					STORE "N9"+cfd+"BM"+cfd+ALLTRIM(cBOL)+csegd TO cString
					DO cstringbreak
					nSegCtr = nSegCtr + 1

					STORE "N9"+cfd+"CN"+cfd+ALLTRIM(cPRONum)+csegd TO cString
					DO cstringbreak
					nSegCtr = nSegCtr + 1
				ENDIF

				IF !EMPTY(bl.trailer)
					STORE "N9"+cfd+"RU"+cfd+ALLTRIM(bl.trailer)+csegd TO cString
					DO cstringbreak
					nSegCtr = nSegCtr + 1
				ENDIF
				IF !EMPTY(bl.seal)
					STORE "N9"+cfd+"T5"+cfd+ALLTRIM(bl.seal)+csegd TO cString
					DO cstringbreak
					nSegCtr = nSegCtr + 1
				ENDIF
			ENDIF
		ENDIF
		
		SET STEP ON 
*	WAIT WINDOW "Parcel type = "+IIF(lParcelType, "TRUE","FALSE") TIMEOUT 20

		SELECT outship
		IF lParcelType && small package so all 3 segs are the lcBOL, tracking number
			cBOL = IIF(lDS,cTrkBOL,cBOL)
			STORE "N9"+cfd+"MB"+cfd+ALLTRIM(cBOL)+csegd TO cString
			DO cstringbreak
			nSegCtr = nSegCtr + 1

			STORE "N9"+cfd+"BM"+cfd+ALLTRIM(cBOL)+csegd TO cString
			DO cstringbreak
			nSegCtr = nSegCtr + 1

			STORE "N9"+cfd+"CN"+cfd+ALLTRIM(cBOL)+csegd TO cString
			DO cstringbreak
			nSegCtr = nSegCtr + 1

			STORE "N9"+cfd+"FC"+cfd+ALLT(cCharge)+csegd TO cString
			DO cstringbreak
			nSegCtr = nSegCtr + 1
		ENDIF

		cShipcode = ALLTRIM(segmentget(@apt,"SHIPCODE",alength))
		IF !EMPTY(cShipcode)
			STORE "N9"+cfd+"DP"+cfd+ALLTRIM(cShipcode)+csegd TO cString
			DO cstringbreak
			nSegCtr = nSegCtr + 1
		ENDIF

		cDiv = ALLTRIM(outship.div)
		IF !EMPTY(cDiv)
			STORE "N9"+cfd+"DV"+cfd+cDiv+csegd TO cString
			DO cstringbreak
			nSegCtr = nSegCtr + 1
		ENDIF
		RELEASE cDiv

		STORE "N9"+cfd+"CU"+cfd+ALLTRIM(outship.cacctnum)+csegd TO cString
		DO cstringbreak
		nSegCtr = nSegCtr + 1

		cCO = IIF("OV"$cShip_ref,ALLTRIM(LEFT(cShip_ref,AT(" ",cShip_ref,1))),cShip_ref)  && Reversed this from actual CO data to current PT#, JoeB, 03.22.2017
		cCO = IIF(ISALPHA(cShip_ref),ALLTRIM(SUBSTR(cShip_ref,2)),cShip_ref)
*		cCO = ALLTRIM(segmentget(@apt,"CO",alength))
		IF !EMPTY(cCO)
			STORE "N9"+cfd+"CO"+cfd+ALLTRIM(cCO)+csegd TO cString
			DO cstringbreak
			nSegCtr = nSegCtr + 1
		ENDIF


		STORE "G62"+cfd+"11"+cfd+TRIM(DTOS(ddel_date))+cfd+"D"+cfd+cDelTime+csegd TO cString  && Ship date/time
		DO cstringbreak
		nSegCtr = nSegCtr + 1

		SELECT scacs
		IF lTesting AND (EMPTY(m.scac) OR EMPTY(m.ship_via))
			STORE "TEST" TO m.scac
			STORE "TEST SHIPPER" TO m.ship_via
		ELSE
			IF !EMPTY(TRIM(outship.scac))
				STORE ALLTRIM(outship.scac) TO m.scac
				lFedEx = .F.
				SELECT scacs
				IF SEEK(m.scac,"scacs","scac")
					IF ALLTRIM(outship.keyrec) = ALLTRIM(outship.bol_no)
						lParcelType = .F.
					ELSE
						lParcelType = .T.
						lFedEx = scacs.fedex
					ENDIF
				ENDIF
				SELECT outship

				IF lParcelType OR lFedEx
					cCarrierType = "U" && Parcel as UPS or FedEx
				ENDIF
			ELSE
				WAIT CLEAR
				cErrMsg = "MISSING SCAC"
				THROW
			ENDIF
		ENDIF

		SELECT outship

		STORE "W27"+cfd+cCarrierType+cfd+TRIM(m.scac)+cfd+TRIM(m.ship_via)+csegd TO cString
		DO cstringbreak
		nSegCtr = nSegCtr + 1

*!*			IF lParcelType
*!*				STORE "G72"+cfd+"516"+cfd+"15"+REPLICATE(cfd,6)+ALLT(cCharge)+csegd TO cString
*!*				DO cstringbreak
*!*				nSegCtr = nSegCtr + 1
*!*			ENDIF

*************************************************************************
*2	DETAIL LOOP - MATCHING SHIP_REF FIELD (FROM OUTSHIP)
*************************************************************************
		WAIT CLEAR
		WAIT WINDOW "Now creating Detail information "+cShip_ref NOWAIT NOCLEAR
		SELECT vmerkurypp

		SELECT outdet
		SET ORDER TO TAG outdetid
		SELECT vmerkurypp
		SET RELATION TO outdetid INTO outdet

		SELECT vmerkurypp
		LOCATE
*!*			BROWSE FIELDS wo_num,ship_ref,outshipid
		LOCATE FOR vmerkurypp.ship_ref = TRIM(cShip_ref) AND vmerkurypp.outshipid = nOutshipid
		IF !FOUND()
			IF !lTesting
				WAIT WINDOW "PT "+cShip_ref+" NOT FOUND in vmerkurypp...ABORTING" TIMEOUT 2
				IF !lTesting
					lSQLMail = .T.
				ENDIF
				cErrMsg = "MISS PT-SQL: "+cShip_ref
				THROW
			ELSE
				LOOP
			ENDIF
		ENDIF

		vscanstr = "vmerkurypp.ship_ref = cShip_ref and vmerkurypp.outshipid = nOutshipid"

		SELECT vmerkurypp
		LOCATE
		LOCATE FOR &vscanstr
		cUCC= "XXX"
		DO WHILE &vscanstr
			lSkipBack = .T.

			IF vmerkurypp.totqty = 0  && Added to prevent zero quantity detail from adding, 08.14.2017, Joe
				SKIP 1 IN vmerkurypp
				LOOP
			ENDIF

			IF TRIM(vmerkurypp.ucc) <> cUCC
				STORE TRIM(vmerkurypp.ucc) TO cUCC
			ENDIF

			STORE "LX"+cfd+ALLTRIM(STR(nCtnNumber))+csegd TO cString   && Carton Seq. # (up to Carton total)
			lDoManSegment = .T.
			lDoPALSegment = .F.
			DO cstringbreak
			nSegCtr = nSegCtr + 1

			DO WHILE vmerkurypp.ucc = cUCC
				cDesc = ""
				SELECT outdet
				alength = ALINES(aptdet,outdet.printstuff,.T.,CHR(13))
*!* Prepack determination based on actual style info
				lPrepack945 = .F.
				cUCCNumber = vmerkurypp.ucc
				cUCCNumber = ALLTRIM(STRTRAN(TRIM(cUCCNumber)," ",""))

				IF lDoManSegment
					lDoManSegment = .F.
					nPTCtnTot =  nPTCtnTot + 1
					STORE "MAN"+cfd+"GM"+cfd+TRIM(cUCCNumber)+csegd TO cString  && SCC-14 Number (Wal-mart spec)
					DO cstringbreak
					nSegCtr = nSegCtr + 1
					IF lParcelType
						STORE "MAN"+cfd+"CP"+cfd+IIF(lDS,cTrkBOL,TRIM(cBOL))+csegd TO cString  && SCC-14 Number (Wal-mart spec)
						DO cstringbreak
						nSegCtr = nSegCtr + 1
					ENDIF
					nShipDetQty = INT(VAL(outdet.PACK))
					nUnitSum = nUnitSum + nShipDetQty

					IF (EMPTY(outdet.ctnwt) OR outdet.ctnwt = 0) AND outdet.totqty > 0
						cCtnWt = ALLTRIM(STR(INT(outship.weight/outship.ctnqty)))
						nTotCtnWt = nTotCtnWt + INT(VAL(cCtnWt))
					ELSE
						STORE ALLTRIM(STR(outdet.ctnwt)) TO cCtnWt
						nTotCtnWt = nTotCtnWt + outdet.ctnwt
					ENDIF
				ENDIF

				cStyle = TRIM(outdet.STYLE)
				IF EMPTY(cStyle)
					ASSERT .F. MESSAGE "EMPTY STYLE...DEBUG"
					IF lOverflow
						SET DELETED OFF
						SELECT vmerkurypp
						SET RELATION TO outdetid INTO outdet
						cStyle = TRIM(outdet.STYLE)
					ELSE
						WAIT WINDOW "Empty style in "+cShip_ref TIMEOUT 1
					ENDIF
				ENDIF

				cColor = TRIM(outdet.COLOR)
				cSize = TRIM(outdet.ID)
				lDoUPC = .T.
*				ASSERT .F. MESSAGE "At UPC extract...debug"
				cUPC = ALLTRIM(outdet.upc)
				IF (ISNULL(cUPC) OR EMPTY(cUPC) OR VAL(cUPC) = 0)
					cUPC = TRIM(vmerkurypp.upc)
					IF (ISNULL(cUPC) OR EMPTY(cUPC) OR VAL(cUPC) = 0)
						lDoUPC = .F.
					ENDIF
				ENDIF
				cItemNum = TRIM(outdet.custsku)
				nShipDetQty = vmerkurypp.totqty
				nOrigDetQty = vmerkurypp.qty
				cUnitCode = "EA"
				nShipStat = "SH"

				IF !SEEK(outdet.STYLE,"upcmast","style")
					lcLength = "9.00"
					lcWidth  = "9.00"
					lcHeight = "9.00"
					lcWeight = "15.00"
					lcCube    = "15.00"
					lcUPC     = "UNK"
				ELSE
					SELECT upcmast
					lcLength = getmemodata("info","LENGTH")
					lcWidth  = getmemodata("info","WIDTH")
					lcHeight = getmemodata("info","HEIGHT")
					lcWeight = TRANSFORM(upcmast.weight)
					lcCube   = TRANSFORM(upcmast.cube)
					lcUPC    = upcmast.upc
				ENDIF

				STORE "W12"+cfd+nShipStat+cfd+ALLTRIM(STR(nOrigDetQty))+cfd+ALLTRIM(STR(nShipDetQty))+cfd+ALLTRIM(STR(nOrigDetQty-nShipDetQty))+cfd+;
					cUnitCode+cfd+IIF(EMPTY(outdet.upc),ALLTRIM(lcUPC),ALLTRIM(outdet.upc))+cfd+"CB"+cfd+ALLTRIM(outdet.STYLE)+cfd+cfd+ALLTRIM(lcWeight)+cfd+"G"+cfd+"L"+cfd+cfd+cfd+cfd+cfd+"VN"+cfd+ALLTRIM(outdet.STYLE)+csegd TO cString
				DO cstringbreak
				nSegCtr = nSegCtr + 1


				STORE "N9"+cfd+"LN"+cfd+ALLTRIM(lcLength)+csegd TO cString
				DO cstringbreak
				nSegCtr = nSegCtr + 1

				STORE "N9"+cfd+"WD"+cfd+ALLTRIM(lcWidth)+csegd TO cString
				DO cstringbreak
				nSegCtr = nSegCtr + 1

				STORE "N9"+cfd+"HE"+cfd+ALLTRIM(lcHeight)+csegd TO cString
				DO cstringbreak
				nSegCtr = nSegCtr + 1

				STORE "N9"+cfd+"CUB"+cfd+ALLTRIM(lcCube)+csegd TO cString
				DO cstringbreak
				nSegCtr = nSegCtr + 1


*				ASSERT .f. MESSAGE "AT G69 LOOP"
				cDesc = ALLTRIM(segmentget(@aptdet,"MASTSTYLEDESC",alength))
				IF !EMPTY(cDesc)
					STORE "G69"+cfd+cDesc+csegd TO cString
					DO cstringbreak
					nSegCtr = nSegCtr + 1
				ENDIF

				cPkg = TRIM(segmentget(@aptdet,"PKG",alength))
				cPkgQty = TRIM(segmentget(@aptdet,"QTY",alength))
				IF !EMPTY(cPkg)
					STORE "N9"+cfd+"PKG"+cfd+cPkg+cfd+cPkgQty+csegd TO cString
					DO cstringbreak
					nSegCtr = nSegCtr + 1
				ENDIF

				SKIP 1 IN vmerkurypp
			ENDDO
			lSkipBack = .T.
			nCtnNumber = nCtnNumber + 1
*		nUnitSum = nUnitSum + 1
		ENDDO

*************************************************************************
*2^	END DETAIL MATCHING SHIP_REF LOOP
*************************************************************************
		WAIT CLEAR
		IF outship.cuft>0
			cCube = STR(outship.cuft,6,2)
		ELSE
			cCube = "0"
		ENDIF
		WAIT WINDOW "Now creating Section Closure information" NOWAIT NOCLEAR
		STORE "W03"+cfd+ALLTRIM(STR(nPTCtnTot))+cfd+ALLTRIM(STR(nTotCtnWt))+cfd+;
			cWeightUnit+cfd+cCube+cfd+"CF"+csegd TO cString   && Units sum, Weight sum, carton count

		nPTCtnTot = 0
		nTotCtnWt = 0
		nTotCtnCount = 0
		nUnitSum = 0
		IF lTesting
			FPUTS(nFilenum,cString)
		ELSE
			FWRITE(nFilenum,cString)
		ENDIF

		STORE  "SE"+cfd+ALLTRIM(STR(nSegCtr+2))+cfd+PADL(c_GrpCntrlNum,9,"0")+csegd TO cString
		IF lTesting
			FPUTS(nFilenum,cString)
		ELSE
			FWRITE(nFilenum,cString)
		ENDIF

		SELECT outship
		WAIT CLEAR
	ENDSCAN

*************************************************************************
*1^	END OUTSHIP MAIN LOOP
*************************************************************************

	DO close945
	=FCLOSE(nFilenum)

	IF !lTesting
		SELECT edi_trigger
		DO ediupdate WITH "945 CREATED",.F.
		SELECT edi_trigger
		LOCATE

		IF !USED("ftpedilog")
			SELECT 0
			USE F:\edirouting\ftpedilog ALIAS ftpedilog
			INSERT INTO ftpedilog (TRANSFER,ftpdate,filename,acct_name,TYPE) VALUES ("945-"+ccustname+"-"+cCustLoc,dt2,cFilenameHold,UPPER(ccustname),"945")
			USE IN ftpedilog
		ENDIF
	ENDIF

	WAIT CLEAR
	WAIT WINDOW cCustFolder+" 945 Process complete..." NOWAIT


*!* Create eMail confirmation message
	IF lTestMail
		tsendto = tsendtotest
		tcc = tcctest
	ENDIF

	cOutFolder = "FMI"+cCustLoc

	IF !lDS
		tsubject = cMailName+" 945 EDI File from TGF as of "+dtmail+" ("+cCustLoc+")"
		tattach = " "
		tmessage = "945 EDI Info from TGF, for division "+cDivision+", Toll BOL# "+IIF(lDS,cOrigBOL,cBOL)+CHR(13)
		tmessage = tmessage +"File Name "+cFilenameShort+CHR(13)
		tmessage = tmessage +IIF(lDS,"Tracking Number: "+TRIM(cBOL)+CHR(13),"")
		tmessage = tmessage + "For Work Orders: "+cWO_NumStr+","+CHR(13)
		tmessage = tmessage + "containing these "+ALLTRIM(STR(nPTCount))+" picktickets:"+CHR(13)+CHR(13)
		tmessage = tmessage + cPTString + CHR(13)+CHR(13)
		tmessage = tmessage +"has been created and will be transmitted ASAP."+CHR(13)+CHR(13)
		IF !EMPTY(cMissDel)
			tmessage = tmessage+CHR(13)+CHR(13)+cMissDel+CHR(13)+CHR(13)
		ENDIF
		IF lTesting OR lTestinput
			tmessage = tmessage + "This is a TEST 945"
		ELSE
			tmessage = tmessage + "If you have any questions, please eMail or call 732-750-9000 x217."
		ENDIF

		IF lEmail
			ASSERT .F. MESSAGE "At do mail stage...DEBUG"
			DO FORM m:\dev\frm\dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
		ENDIF
	ENDIF

	cFilenameArchive = UPPER("f:\ftpusers\"+cCustFolder+"\945OUT\ARCHIVE\"+cCustPrefix+dt1+".txt")

	RELEASE ALL LIKE c_CntrlNum,c_GrpCntrlNum
	WAIT CLEAR
	WAIT WINDOW cMailName+" 945 EDI File output complete" TIMEOUT 1
	lDoCatch = .F.

*!* Transfers files to correct output folders
	COPY FILE &cFilenameHold TO &cFilenameArch


	COPY FILE &cFilenameHold TO &cFilenameOut
	DELETE FILE &cFilenameHold
	SELECT temp945
	COPY TO "f:\3pl\data\temp945a.dbf"
	USE IN temp945
	SELECT 0
	USE "f:\3pl\data\temp945a.dbf" ALIAS temp945a
	SELECT 0
	USE "f:\3pl\data\pts_sent945.dbf" ALIAS pts_sent945
	APPEND FROM "f:\3pl\data\temp945a.dbf"
	USE IN pts_sent945
	USE IN temp945a
	DELETE FILE "f:\3pl\data\temp945a.dbf"
	IF !lTesting
*	asn_out_data()
	ENDIF

CATCH TO oErr
	IF lDoCatch
		ASSERT .F. MESSAGE "In Error CATCH"
		SET STEP ON
		IF EMPTY(cErrMsg)
			cErrMsg = "ERRHAND ERROR"
		ENDIF
		DO ediupdate WITH cErrMsg,.T.
		tsubject = ccustname+" Error ("+TRANSFORM(oErr.ERRORNO)+") at "+TTOC(DATETIME())
		tattach  = ""
		IF lTesting
			tsendto  = tsendtotest
			tcc = tcctest
		ELSE
			tsendto  = tsendtoerr
			tcc = tccerr
		ENDIF

		IF !lIsError
			tmessage = ccustname+" Error processing "+CHR(13)
			tmessage = tmessage+TRIM(PROGRAM())+CHR(13)

			tmessage =tmessage+ "Try/CATCH Exeception Message:"+CHR(13)+;
				[  Error: ] + STR(oErr.ERRORNO) +CHR(13)+;
				[  LineNo: ] + STR(oErr.LINENO) +CHR(13)+;
				[  Message: ] + oErr.MESSAGE +CHR(13)+;
				[  Procedure: ] + oErr.PROCEDURE +CHR(13)+;
				[  Details: ] + oErr.DETAILS +CHR(13)+;
				[  StackLevel: ] + STR(oErr.STACKLEVEL) +CHR(13)+;
				[  LineContents: ] + oErr.LINECONTENTS+CHR(13)+;
				[  UserValue: ] + oErr.USERVALUE+CHR(13)
			tmessage = tmessage+"Program: "+cRunningProg+CHR(13)
			tsubject = "945 EDI Poller Error at "+TTOC(DATETIME())
			tattach  = ""
			tcc=""
			ASSERT .F. MESSAGE "At do mail stage...DEBUG"
			DO FORM m:\dev\frm\dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
		ENDIF
		FCLOSE(nFilenum)
	ENDIF
FINALLY
	IF lTesting
		CLOSE DATA ALL
	ELSE
		CLOSETABLES()
	ENDIF
	ON ERROR
	SET DELETED ON
	WAIT CLEAR
	WAIT WINDOW "" TIMEOUT .3
ENDTRY

*** END OF CODE BODY

****************************
PROCEDURE close945
****************************
** Footer Creation

	STORE  "GE"+cfd+ALLTRIM(STR(nSTCount))+cfd+c_CntrlNum+csegd TO cString
	IF lTesting
		FPUTS(nFilenum,cString)
	ELSE
		FWRITE(nFilenum,cString)
	ENDIF

	STORE  "IEA"+cfd+"1"+cfd+PADL(c_CntrlNum,9,"0")+csegd TO cString
	IF lTesting
		FPUTS(nFilenum,cString)
	ELSE
		FWRITE(nFilenum,cString)
	ENDIF

	RETURN

****************************
PROCEDURE num_incr_isa
****************************

	IF !USED("serfile")
		USE ("f:\3pl\data\serial\"+ccustname+"945_serial") IN 0 ALIAS serfile
	ENDIF
	SELECT serfile
	IF lTesting AND lISAFlag
		nOrigSeq = serfile.seqnum
		lISAFlag = .F.
	ENDIF
	nISA_Num = serfile.seqnum
	c_CntrlNum = ALLTRIM(STR(serfile.seqnum))
	REPLACE serfile.seqnum WITH serfile.seqnum + 1 IN serfile
	SELECT outship
	RETURN

****************************
PROCEDURE num_incr_st
****************************

	IF !USED("serfile")
		USE ("f:\3pl\data\serial\"+ccustname+""945_serial") IN 0 ALIAS serfile
	ENDIF
	SELECT serfile
	IF lTesting AND lSTFlag
		nOrigGrpSeq = serfile.grpseqnum
		lSTFlag = .F.
	ENDIF
	c_GrpCntrlNum = ALLTRIM(STR(serfile.grpseqnum))
	REPLACE serfile.grpseqnum WITH serfile.grpseqnum + 1 IN serfile
	SELECT outship
	RETURN


****************************
PROCEDURE segmentget
****************************
	PARAMETER thisarray,lcKey,nLength

	FOR i = 1 TO nLength
		IF i > nLength
			EXIT
		ENDIF
		lnEnd= AT("*",thisarray[i])
		IF lnEnd > 0
			lcThisKey =TRIM(SUBSTR(thisarray[i],1,lnEnd-1))
			IF OCCURS(lcKey,lcThisKey)>0
				RETURN SUBSTR(thisarray[i],lnEnd+1)
				i = 1
			ENDIF
		ENDIF
	ENDFOR

	RETURN ""

****************************
PROCEDURE ediupdate
****************************
	PARAMETER cStatus,lIsError
	IF !lTesting
		SELECT edi_trigger
		nRec = RECNO()
		lDoCatch = .F.
		IF !lIsError
			SELECT edi_trigger
			SCAN FOR edi_trigger.bol = IIF(lDS,cOrigBOL,cBOL)  AND edi_trigger.accountid = nAcctNum
*				RLOCK('edi_trigger')
				REPLACE edi_trigger.processed WITH .T.,edi_trigger.proc945 WITH .T.,edi_trigger.file945 WITH cFilenameHold,edi_trigger.isa_num WITH cISA_Num,;
					edi_trigger.fin_status WITH "945 CREATED",edi_trigger.errorflag WITH .F.,edi_trigger.when_proc WITH DATETIME() NEXT 1
*				UNLOCK IN edi_trigger


			ENDSCAN
*			num_decrement() && moved this to the error condition
		ELSE
			SELECT edi_trigger
			SCAN FOR edi_trigger.bol = IIF(lDS,cOrigBOL,cBOL)  AND edi_trigger.accountid = nAcctNum
*				RLOCK('edi_trigger')
				REPLACE edi_trigger.processed WITH .T.,edi_trigger.proc945 WITH .F.,edi_trigger.file945 WITH "",;
					edi_trigger.fin_status WITH cStatus,edi_trigger.errorflag WITH .T. NEXT 1
*				UNLOCK IN edi_trigger
			ENDSCAN
			num_decrement()

			IF lCloseOutput
				=FCLOSE(nFilenum)
				ERASE &cFilenameHold
			ENDIF
		ENDIF
	ENDIF

	IF lIsError AND lEmail AND cStatus<>"SQL ERROR"
		tsubject = "945 Error in "+cMailName+" BOL "+TRIM(cBOL)+"(At PT "+cShip_ref+")"
		tattach = " "
		tsendto = tsendtoerr
		tcc = tccerr
		tmessage = "945 Processing for BOL# "+cBOL+", WO# "+cWO_Num+" produced this error: (Office: "+cOffice+"): "+cStatus+CHR(13)+"Check EDI_TRIGGER and re-run"
		IF "TOTQTY ERR"$cStatus
			tmessage = tmessage + CHR(13) + "At OUTSHIPID: "+ALLTRIM(STR(m.outshipid))
		ENDIF
		ASSERT .F. MESSAGE "At do mail stage...DEBUG"
		DO FORM m:\dev\frm\dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
	ENDIF

	IF USED('outship')
		USE IN outship
	ENDIF
	IF USED('outdet')
		USE IN outdet
	ENDIF

	IF USED('serfile')
		SELECT serfile
		IF lTesting
			REPLACE serfile.seqnum WITH nOrigSeq
			REPLACE serfile.grpseqnum WITH nOrigGrpSeq
		ENDIF
		USE IN serfile
	ENDIF
	IF USED('scacs')
		USE IN scacs
	ENDIF
	IF USED('mm')
		USE IN mm
	ENDIF
	IF USED('tempx')
		USE IN tempx
	ENDIF
	IF !lTesting
		SELECT edi_trigger
		LOCATE
	ENDIF

ENDPROC

****************************
PROCEDURE cstringbreak
****************************
	cLen = LEN(ALLTRIM(cString))
	IF ISNULL(cString)
		ASSERT .F. MESSAGE "At cStringBreak procedure"
	ENDIF
	IF lTesting
		FPUTS(nFilenum,cString)
	ELSE
		FWRITE(nFilenum,cString)
	ENDIF
ENDPROC


****************************
PROCEDURE cszbreak
****************************
	cCSZ = ALLT(m.CSZ)

	FOR ii = 5 TO 2 STEP -1
		cCSZ = STRTRAN(cCSZ,SPACE(ii),SPACE(1))
	ENDFOR
	cCSZ = STRTRAN(cCSZ,",","")
	len1 = LEN(ALLT(cCSZ))
	nSpaces = OCCURS(" ",cCSZ)

	IF nSpaces<2
		cErrMsg = "BAD CSZ INFO"
		THROW
	ENDIF

*	ASSERT .F. MESSAGE "In CSZ Breakout"
	FOR ii = nSpaces TO 1 STEP -1
		ii1 = ALLT(STR(ii))
		DO CASE
			CASE ii = nSpaces
				nPOS = AT(" ",cCSZ,ii)
				cZip = ALLT(SUBSTR(cCSZ,nPOS))
*				WAIT WINDOW "ZIP: "+cZip TIMEOUT 1
				nEndState = nPOS
			CASE ii = (nSpaces - 1)
				nPOS = AT(" ",cCSZ,ii)
				cState = ALLT(SUBSTR(cCSZ,nPOS,nEndState-nPOS))
*				WAIT WINDOW "STATE: "+cState TIMEOUT 1
				IF nSpaces = 2
					cCity = ALLT(LEFT(cCSZ,nPOS))
*					WAIT WINDOW "CITY: "+cCity TIMEOUT 1
					EXIT
				ENDIF
			OTHERWISE
				nPOS = AT(" ",cCSZ,ii)
				cCity = ALLT(LEFT(cCSZ,nPOS))
*				WAIT WINDOW "CITY: "+cCity TIMEOUT 2
		ENDCASE
	ENDFOR
ENDPROC

****************************
PROCEDURE num_decrement
****************************
*!* This procedure decrements the ISA/GS numbers in the counter
*!* in the event of failure and deletion of the current 945
	IF lCloseOutput
		IF !USED("serfile")
			USE ("f:\3pl\data\serial\"+ccustname+"945_serial") IN 0 ALIAS serfile
		ENDIF
		SELECT serfile
		REPLACE serfile.seqnum WITH serfile.seqnum - 1 IN serfile  && ISA number
		REPLACE serfile.grpseqnum WITH serfile.grpseqnum - 1 IN serfile  && GS number
		RETURN
	ENDIF
ENDPROC

PROCEDURE CLOSETABLES
************************
PROCEDURE CLOSETABLES
************************
	IF USED('outship')
		USE IN outship
	ENDIF
	IF USED('outdet')
		USE IN outdet
	ENDIF
	IF USED('scacs')
		USE IN scacs
	ENDIF
	IF USED('mm')
		USE IN mm
	ENDIF
	IF USED('tempx')
		USE IN tempx
	ENDIF
	IF USED('shipment')
		USE IN shipment
	ENDIF
	IF USED('package')
		USE IN package
	ENDIF
ENDPROC
