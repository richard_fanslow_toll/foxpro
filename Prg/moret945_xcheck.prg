CLOSE DATA ALL
lTesting = .F.

USE F:\3pl\DATA\edi_trigger IN 0 ALIAS edi_trigger SHARED
WAIT WINDOW "Now selecting today's sent 945s from EDI_TRIGGER" NOWAIT
SELECT file945,office,bol,.F. AS foundit ;
	FROM edi_trigger ;
	WHERE edi_trigger.moret = .T. ;
	AND accountid # 6303 ;
	AND edi_trigger.edi_type = '945' ;
	AND !edi_trigger.errorflag ;
	AND edi_trigger.processed ;
	AND TTOD(edi_trigger.when_proc) = DATE() ;
	GROUP BY 1,2,3 ;
	INTO CURSOR temp1 READWRITE

REPLACE temp1.file945 WITH UPPER(JUSTFNAME(ALLTRIM(temp1.file945))) IN temp1 ALL



FOR qp = 1 TO 2
	IF qp = 1
		cWhse = "SP"
	ELSE
		cWhse = "ML"
	ENDIF
	WAIT WINDOW "Now cross-matching Archived "+cWhse+" 945s with selected trigger File945s" NOWAIT
	CD ("F:\FTPUSERS\MORET-"+cWhse+"\945OUT\945ARCHIVE")
	len1 = ADIR(ARY1,"*.edi")
	ASORT(ARY1,3,-1,1)

	FOR qq = 1 TO len1
		dFiledate = ARY1[qq,3]
		IF dFiledate < DATE()
			EXIT
		ENDIF
		cFilename = UPPER(ALLTRIM(ARY1[qq,1]))
		SELECT temp1
		LOCATE FOR file945 = cFilename
		IF FOUND()
			REPLACE temp1.foundit WITH .T. IN temp1 NEXT 1
		ENDIF
	ENDFOR

ENDFOR

SELECT temp1
IF lTesting
	WAIT WINDOW "Now browsing to check for missing files" TIMEOUT 2
	BROWSE
ENDIF

SET STEP ON 
COUNT TO N FOR foundit=.F.
IF N > 0
	WAIT WINDOW "Now processing email for unfound 945s marked as processed..." TIMEOUT 5
	SELECT temp1
	LOCATE
	COPY TO h:\fox\moret945x.xls TYPE XL5 FOR foundit = .F.

	tsendto = "joe.bianchi@tollgroup.com"
	tcc = IIF(lTesting,"","maria.estrella@tollgroup.com")
	tcc = ""
	tsubject= "TGF Moret 945 Cross-check failure at "+TTOC(DATETIME())
	tattach = "h:\fox\moret945x.xls"
	tmessage = "The attached file contains 945 filenames and offices which did not get processed/sent after triggering."
	tmessage = tmessage+CHR(13)+"Have Joe check the Trigger table and retrigger the affected BOL(s) as needed."
	IF lTesting
		tmessage = tmessage+CHR(13)+CHR(13)+"This is a test mailing"
	ENDIF
		tFrom = "TGF EDI Operations <toll-edi-ops@tollgroup.com>"
		DO FORM m:\dev\frm\dartmail2 WITH tsendto,tFrom,tsubject,tcc,tattach,tmessage,"A"
		DELETE FILE h:\fox\moret945x.xls
ELSE
	WAIT WINDOW "No unsent Moret 945s for "+DTOC(DATE()) TIMEOUT 2
ENDIF

CLOSE DATA ALL
RETURN
