utilsetup("MARCJACOBSW832")
Close Data All

Public lTesting
Public Array a856(1)

lTesting = .F.
Do m:\dev\prg\_setvars With lTesting

If !lTesting
*	Use F:\wh\upcmast Alias upcmast In 0 Shared
	useca("upcmast","wh",,,"select * from upcmast where accountid = 6303",,"upcmastsql")
Else
	useca("upcmast","wh",,,"select * from upcmast where accountid = 6303",,"upcmastsql")
Endif
Set Step On

*!*	Select * ;
*!*		FROM upcmast ;
*!*		WHERE .F. ;
*!*		INTO Cursor tempmast Readwrite
*!*	Alter Table tempmast Drop Column upcmastid
lcCurrDir = ""

If !lTesting
	lcPath = 'F:\FTPUSERS\MJ_Wholesale\WMS_Inbound\Stylemaster\'
	lcArchivePath = 'F:\FTPUSERS\MJ_Wholesale\WMS_Inbound\Stylemaster\archive\'
	tsendto = "jkillen@fmiint.com,tmarg@fmiint.com"
	tcc = "tmarg@fmiint.com"
Else
	lcPath = 'F:\FTPUSERS\MJ_Wholesale_test\WMS_Inbound\Stylemaster\'
	lcArchivePath = 'F:\FTPUSERS\MJ_Wholesale_test\WMS_Inbound\Stylemaster\archive\'
	tsendto="tmarg@fmiint.com"
	tcc=""
Endif


Cd &lcPath
len1 = Adir(ary1,"*.*")
If len1 = 0
	Wait Window "No files found...exiting" Timeout 2
	Close Data All
	schedupdate()
	_Screen.Caption=gscreencaption
	On Error
	Return
Endif

lcAddedStr  ="Styles Added:"+Chr(13)
lcUpdateStr ="Styles Updated:"+Chr(13)
Set Step On
For xtodd = 1 To len1
	cFilename = Alltrim(ary1[xtodd,1])
	xfile = lcPath+cFilename
*****TM added
	Select * ;
		FROM upcmastsql ;
		WHERE .F. ;
		INTO Cursor tempmast Readwrite
	Alter Table tempmast Drop Column upcmastid
	Do m:\dev\prg\createx856a
	Do m:\dev\prg\loadedifile With xfile,"*","TILDE"

	m.addby = "FMI-PROC"
	m.adddt = Datetime()
	m.addproc = "MJW832"
	m.accountid = 6303
	m.pnp = .T.
	m.uom = "EA"

	Select x856


	Locate
	Scan
SET STEP ON 
		Do Case
		Case x856.segment = "LIN"
**reset in case segments do not exist for upc - mvw 03/19/14
			Store "" To m.sid,m.info,m.descrip
			Store 0 To m.price,m.rprice

			If x856.f2 = "UP"
				m.upc = Allt(x856.f3)
			Endif
			If x856.f4 = "ST"
				m.style = Allt(x856.f5)
			Endif
			If x856.f6 = "CL"
				m.color = Allt(x856.f7)
			Endif
			If x856.f8 = "SZ"
				m.id = Allt(x856.f9)
			Endif

			If x856.f22 = "SE"
				m.info = "SEASON*"+Allt(Upper(x856.f23))  &&Force uppercase
			Endif

			If x856.f2 = "UP"
				m.info = m.info+Chr(13)+"UPC*"+Allt(x856.f3)  &&ADD UPC TO INFO 3/27/14
			Endif


		Case x856.segment = "REF" And x856.f1 = "DIV"
			m.info = m.info+Chr(13)+"DIV*"+Allt(x856.f2)

		Case x856.segment = "REF" And x856.f1 = "MRC"       &&&Added 02/27/2013 AX2012 Classification TM
			m.info = m.info+Chr(13)+"MRC*"+Allt(x856.f2)

		Case x856.segment = "REF" And x856.f1 = "MRD"       &&&Added 02/27/2013 AX2012 Body Type TM
			m.info = m.info+Chr(13)+"MRD*"+Allt(x856.f2)

		Case x856.f2 = "UP"
			m.info = m.info+Chr(13)+"UPC*"+Allt(x856.f3)


		Case x856.segment = "PID" And x856.f1 = "F" And x856.f2 = "08"
			m.descrip = Allt(x856.f5)

		Case x856.segment = "PID" And x856.f1 = "F" And x856.f2 = "73"
			m.info = m.info+Chr(13)+"COLORDESC*"+Alltrim(Upper(x856.f5))

		Case x856.segment = "CTP" And x856.f2 = "RTL"
			m.rprice = Val(x856.f3)
			m.info = m.info+Chr(13)+"RPRICE*"+(x856.f3)
		Case x856.segment = "CTP" And x856.f2 = "STD"
			m.price = Val(x856.f3)

		Case x856.segment = "CTP" And x856.f2 = "PCA"
			m.info = m.info+Chr(13)+"SALEPRICE*"+Allt(x856.f3)

		Case x856.segment = "REF" And x856.f1 = "SI"       &&&Added 03/14/2014 AX2012 SID TM
			m.sid  = Allt(x856.f2)
			m.info = m.info+Chr(13)+"SID*"+Allt(x856.f2)

		Case x856.segment = "REF" And x856.f1 = "VR"       &&&Added 02/03/2016 SID may not be sent, VR will now be used TM
			Insert Into tempmast From Memvar
		Endcase
	Endscan
*!*		Select tempmast
*!*		Locate


*** we have read in this file into tempmast
*** now check for 6303
	Set Step On
	RecCtr = 0
	NumAdded = 0
	NumUpdated = 0
	m.problem=.F.
	Select upcmastsql
	Scatter Fields Except upcmastid Memvar Memo Blank
	Select tempmast && has all new records.......
**From tempmast we check for 6303 and add or update 6303 records
	Scan
		RecCtr = RecCtr +1
		Scatter Fields Except upcmastid Memvar Memo
		Select upcmastsql
		Locate For  accountid =tempmast.accountid And upc=tempmast.upc
*   =Seek(Str(accountid,4)+upc,"upcmast","upc")
*   =Seek(Str(accountid,4)+style+color+id,"upcmast","stylecolid")
		Wait Window At 10,10 "Checking Record # "+Transform(RecCtr) Nowait
		If !Found()
			NumAdded = NumAdded +1
************************TMARG
*!*				Select upcmast
*!*				Go Bott
*!*				m.adddt = Datetime()
*!*				Cd m:\dev
*!*				m.upcmastid=GENPK('UPCMAST','WHALL')
*!*				Insert Into upcmast  From Memvar
*!*				lcAddedStr = lcAddedStr +m.upc+"-"+m.style+"-"+m.color+"-"+m.id+Chr(13)
*!*				Insert Into upcmastsql  From Memvar
*!*			Else
*!*				Replace upcmast.upc          With tempmast.upc In upcmast
*!*				Replace upcmast.sid          With tempmast.sid In upcmast
*!*				Replace upcmast.price        With tempmast.price In upcmast
*!*	*			Replace upcmast.rprice       With tempmast.rprice In upcmast
*!*				Replace upcmast.Style        With tempmast.Style In upcmast
*!*				Replace upcmast.Color        With tempmast.Color In upcmast
*!*				Replace upcmast.Id           With tempmast.Id In upcmast
*!*				Replace upcmast.updatedt     With tempmast.adddt In upcmast
*!*				Replace upcmast.Descrip      With Upper(tempmast.Descrip) In upcmast
*!*				Replace upcmast.Info         With Upper(tempmast.Info) In upcmast
*!*				NumUpdated = NumUpdated +1
*!*				lcUpdateStr = lcUpdateStr +tempmast.upc+"-"+tempmast.Style+"-"+tempmast.Color+"-"+tempmast.Id+Chr(13)

*!*				Select upcmastsql
*!*				Locate For upcmastid=upcmast.upcmastid
*!*				If !Found()
			m.flag=.F.
			m.adddt = Datetime()
			m.updatedt = Datetime()
			m.upcmastid=sqlgenpk("upcmast","wh")
			Insert Into upcmastsql  From Memvar
*!*				Replace upcmastsql.upcmastid    With upcmast.upcmastid In upcmastsql
*!*				Replace upcmastsql.updatedt     With upcmast.updatedt In upcmastsql
*!*				Replace upcmastsql.adddt     With upcmast.adddt In upcmastsql
*!*					If m.problem=.F.
*!*						tsendto = "tmarg@fmiint.com,todd.margolin@tollgroup.com"
*!*						tcc =""
*!*						tattach = ""
*!*						tFrom ="Toll EDI System Operations <fmi-transload-ops@fmiint.com>"
*!*						tmessage = "Missing UPC master data in upcmastsql" +Dtoc (Date())
*!*						tSubject = "Missing UPC master data in upcmastsql"
*!*						Do Form m:\dev\frm\dartmail2 With tsendto,tFrom,tSubject, tcc,tattach,tmessage  ,"A"
*!*						m.problem=.T.
*!*					Else
*!*					Endif
		Else
			Set Step On
			m.updatedt = Datetime()
			Replace upcmastsql.updatedt     With m.updatedt In upcmastsql
			Replace upcmastsql.upc          With tempmast.upc In upcmastsql
			Replace upcmastsql.sid          With tempmast.sid In upcmastsql
			Replace upcmastsql.price        With tempmast.price In upcmastsql
			Replace upcmastsql.rprice       With tempmast.rprice In upcmastsql
			Replace upcmastsql.Style        With tempmast.Style In upcmastsql
			Replace upcmastsql.Color        With tempmast.Color In upcmastsql
			Replace upcmastsql.Id           With tempmast.Id In upcmastsql
			Replace upcmastsql.Descrip      With Upper(tempmast.Descrip) In upcmastsql
			Replace upcmastsql.Info         With Upper(tempmast.Info) In upcmastsql
			Replace upcmastsql.Flag         With .F. In upcmastsql
			Replace upcmastsql.whseloc      With '' In upcmastsql
		Endif
&& add in all the updates

	Endscan
	Set Step On
	tu("upcmastsql")
************************************** END   AX2012
	tattach = ""
	tFrom ="Toll/FMI EDI System Operations <fmi-transload-ops@fmiint.com>"
	tmessage = "Marc Jacobs Stylemaster updated at: "+Ttoc(Datetime())+Chr(13)+;
		Chr(13)+lcAddedStr+Chr(13)+;
		Chr(13)+lcUpdateStr

	tSubject = "Marc Jacobs Stylemaster Update"
	Do Form m:\dev\frm\dartmail2 With tsendto,tFrom,tSubject,tcc,tattach,tmessage,"A"
	Wait Window "All 832 Files finished processing...exiting" Timeout 2
	cLoadFile = (lcPath+cFilename)
	Use In tempmast
	Set Step On
	cArchiveFile = (lcArchivePath+cFilename)
	Copy File [&cLoadFile] To [&cArchiveFile]

	If !lTesting
		Delete File [&cLoadFile]
	Endif
Endfor
Close Data All

schedupdate()
_Screen.Caption=gscreencaption
On Error

