* Create 3 spreadsheets:
*  1 -- list of all Kronos employees (allemployees)
*  2 -- list of employees who will next be rolled out to Kronos (someemployees)
*  3 -- list of employees who should be getting exported to ADP Payroll from Kronos
* Meant to be run before the Kronos Payroll Report and FoxPro ADP export programs, 
* which use F:\UTIL\KRONOS\DATA\KRONOSEMPS.DBF created here.

LOCAL loListKronosEmployees
loListKronosEmployees = CREATEOBJECT('ListKronosEmployees')
loListKronosEmployees.MAIN()
CLOSE DATABASES ALL
RETURN

#DEFINE LOGIT 1
#DEFINE WAITIT 2
#DEFINE NOWAITIT 4
#DEFINE SENDIT 8
#DEFINE CRLF CHR(13) + CHR(10)
#DEFINE RETURN_DATA_MANDATORY .F.
#DEFINE RETURN_DATA_NOT_MANDATORY .T.

DEFINE CLASS ListKronosEmployees AS CUSTOM

	cProcessName = 'ListKronosEmployees'

	lTestMode = .F.  && .F. FOR NORMAL OPERATION, .T. TO SUPPRESS ALL BUT DEVELOPER EMAILS AND CHANGE INPUT TABLE, ETC.

	lAutoYield = .T.

	cStartTime = TTOC(DATETIME())

	* connection properties
	nSQLHandle = 0

	* file properties
	nFileHandle = 0

	* date properties
	dtNow = DATETIME()
	dToday = DATE()
	cToday = DTOC(DATE())
	dAsOfDate = DATE()

	* wait window properties
	nWaitWindowTimeout = 2
	lWaitWindowIsOn = .T.

	* logfile properties
	lLoggingIsOn = .T.
	cLogFile = 'F:\UTIL\KRONOS\Logfiles\ListKronosEmployees_log.txt'

	* INTERNAL email properties
	lSendInternalEmailIsOn = .T.
	cFrom = 'FMI Corporate <fmicorporate@fmiint.com>'
	cSendTo = 'Mark Bennett <mbennett@fmiint.com>'
	*cSendTo = 'Lucille Waldrip <lwaldrip@fmiint.com>, Lori Guiliano <lguiliano@fmiint.com>, Mark Bennett <mbennett@fmiint.com>'
	cCC = ''
	cSubject = 'LIST KRONOS EMPLOYEES for: ' + TTOC(DATETIME())
	cAttach = ''
	cBodyText = ''

	cCOMPUTERNAME = ''
	cUSERNAME = ''

	FUNCTION INIT
		IF NOT DODEFAULT()
			RETURN .F.
		ENDIF
		WITH THIS
			CLEAR
			*SET RESOURCE OFF
			CLOSE DATA
			SET CENTURY ON
			SET DATE YMD
			SET HOURS TO 24
			SET ANSI ON
			SET TALK OFF
			SET DELETED ON
			SET CONSOLE OFF
			SET EXCLUSIVE OFF
			SET SAFETY OFF
			SET EXACT OFF
			SET STATUS BAR ON
			SET SYSMENU OFF
			SET ENGINEBEHAVIOR 70
			_VFP.AUTOYIELD = .lAutoYield
			.cCOMPUTERNAME = GETENV("COMPUTERNAME")
			.cUSERNAME = GETENV("USERNAME")
			IF .lTestMode THEN
				.cSendTo = 'Mark Bennett <mbennett@fmiint.com>'
				.cLogFile = 'F:\UTIL\KRONOS\Logfiles\ListKronosEmployees_log_TESTMODE.txt'
			ENDIF
			.lLoggingIsOn = .lLoggingIsOn AND NOT EMPTY(.cLogFile)
			IF .lLoggingIsOn THEN
				SET ALTERNATE TO (.cLogFile) ADDITIVE
				SET ALTERNATE ON
			ENDIF
			* re-init date properties after all SETTINGS
			.dtNow = DATETIME()
			.dToday = DATE()
			.cToday = DTOC(DATE())
		ENDWITH
	ENDFUNC


	FUNCTION DESTROY
		WITH THIS
			IF .lLoggingIsOn  THEN
				SET ALTERNATE OFF
				SET ALTERNATE TO
			ENDIF
		ENDWITH
		DODEFAULT()
	ENDFUNC


	FUNCTION MAIN
		WITH THIS
			LOCAL lnNumberOfErrors, lcSQL, lcSQL2, lcADPSQL, loError, lcTopBodyText, llValid
			LOCAL lcXLFileName, ldStartDate, ldEndDate, lcStartDate, lcEndDate, ldDate
			LOCAL lcSQLStartDate, lcSQLEndDate, lcXLFileName
			LOCAL oExcel, oWorkbook, oWorksheet, lnRow, lcRow, lnStartRow, lcStartRow, lnEndRow, lnPP, lcPP, lnNN, lcNN, lcJJ
			LOCAL lcColsToProcess, lnColumn, lcBoldColumn, i, j, lcBoldRow, luCellValue, lcTargetDirectory, ldPayDate, lcFiletoSaveAs
			LOCAL lcFileDate, lcDivision, lcRateType, lcTitle, llSaveAgain, lcPayCode
			LOCAL lcList, lcTS_EffectiveFrom, lcTS_EffectiveTo

			TRY
				lnNumberOfErrors = 0
				lcTopBodyText = ''

				.TrackProgress('LIST KRONOS EMPLOYEES process started....', LOGIT+SENDIT+NOWAITIT)
				.TrackProgress('COMPUTERNAME = ' + .cCOMPUTERNAME, LOGIT+SENDIT)
				.TrackProgress('USERNAME = ' + .cUSERNAME, LOGIT+SENDIT)
				IF .lTestMode THEN
					.TrackProgress('                        TEST MODE                        ', LOGIT+SENDIT)
				ENDIF

				lcTS_EffectiveFrom   = "{ts '" + TRANSFORM(YEAR(.dAsOfDate)) + "-" + PADL(MONTH(.dAsOfDate),2,"0") + "-" + PADL(DAY(.dAsOfDate),2,"0") + " 00:00:00.0'}"
				lcTS_EffectiveTo = "{ts '" + TRANSFORM(YEAR(.dAsOfDate)) + "-" + PADL(MONTH(.dAsOfDate),2,"0") + "-" + PADL(DAY(.dAsOfDate),2,"0") + " 23:59:59.9'}"


				* get ss#s from ADP
				OPEN DATABASE F:\UTIL\ADPCONNECTION\CONNECT1

				.nSQLHandle = SQLCONNECT("PCPAYWIN","MBENNETT","VP737A")

				IF .nSQLHandle > 0 THEN

					lcADPSQL = ;
						"SELECT " + ;
						" {fn IFNULL(FILE#,0000)} AS FILE_NUM " + ;
						" FROM REPORTS.V_EMPLOYEE " + ;
						" WHERE STATUS <> 'T' "

					IF .lTestMode THEN
						.TrackProgress('lcADPSQL = ' + lcADPSQL, LOGIT+SENDIT)
					ENDIF

					IF USED('CURADPACTIVEEMPSPRE') THEN
						USE IN CURADPACTIVEEMPSPRE
					ENDIF
					IF USED('CURADPACTIVEEMPS') THEN
						USE IN CURADPACTIVEEMPS
					ENDIF

					IF .ExecSQL(lcADPSQL, 'CURADPACTIVEEMPSPRE', RETURN_DATA_MANDATORY) THEN

						SELECT ;
							INT(FILE_NUM) AS FILE_NUM ;
							FROM CURADPACTIVEEMPSPRE ;
							INTO CURSOR CURADPACTIVEEMPS ;
							ORDER BY 1

*!*		SELECT CURADPACTIVEEMPS
*!*		BROWSE

						* main SQL
				SET TEXTMERGE ON
				TEXT TO	lcSQL NOSHOW
SELECT DISTINCT
	P.PERSONNUM, P.FULLNM, L.LABORLEV1NM, L.LABORLEV2NM, L.LABORLEV3NM, L.LABORLEV7NM, L.LABORLEV7DSC
FROM PERSON P
INNER JOIN JAIDS J
ON J.PERSONID = P.PERSONID
INNER JOIN COMBHOMEACCT C
ON C.EMPLOYEEID = J.EMPLOYEEID
INNER JOIN LABORACCT L
ON L.LABORACCTID = C.LABORACCTID
WHERE J.PERSONID > 0
AND J.DELETEDSW = 0
AND C.EFFECTIVEDTM <= <<lcTS_EffectiveTo>>
AND C.EXPIRATIONDTM > <<lcTS_EffectiveFrom>>
AND L.LABORLEV1NM IN ('SXI','ZXU','AXA')
ORDER BY P.FULLNM
				ENDTEXT
				SET TEXTMERGE OFF

*!*										LEFT(HOMELABORLEVELNM7,4) AS TIMERVWR, ;
*!*										LEFT(HOMELABORLEVELDSC7,30) AS TRNAME, ;
*!*										LEFT(HOMELABORLEVELNM1,3) AS ADP_COMP, ;
*!*										LEFT(HOMELABORLEVELNM2,2) AS DIVISION, ;
*!*										LEFT(HOMELABORLEVELNM3,4) AS DEPT, ;
*!*										LEFT(FULLNM,30) AS EMPLOYEE, ;
*!*										INT(VAL(PERSONNUM)) AS FILE_NUM ;



						IF .lTestMode THEN
							.TrackProgress('', LOGIT+SENDIT)
							.TrackProgress('lcSQL =' + lcSQL, LOGIT+SENDIT)
							.TrackProgress('', LOGIT+SENDIT)
							.TrackProgress('lcSQL2 =' + lcSQL, LOGIT+SENDIT)
						ENDIF

						.TrackProgress('Connecting to WTK System....', LOGIT+SENDIT)

						* get ss#s from ADP
						OPEN DATABASE F:\UTIL\KRONOS\KRONOSEDI

						*.nSQLHandle = SQLCONNECT("KRONOSEDI","tkcsowner","tkcsowner")
						.nSQLHandle = SQLCONNECT("WTK_ON_PAYROLL2","sa","kr0n1tes")

						IF .nSQLHandle > 0 THEN

							IF USED('CUREMPACCTS') THEN
								USE IN CUREMPACCTS
							ENDIF

							IF .ExecSQL(lcSQL, 'CUREMPACCTS', RETURN_DATA_MANDATORY) THEN

								*SELECT CUREMPACCTS
								*BROWSE
								
								IF USED('KRONOSEMPS') THEN
									USE IN KRONOSEMPS
								ENDIF

								* delete DBFs
*!*									IF FILE('F:\UTIL\KRONOS\DATA\ALLKRONOSEMPS.DBF') THEN
*!*										DELETE FILE F:\UTIL\KRONOS\DATA\ALLKRONOSEMPS.DBF
*!*									ENDIF
*!*									IF FILE('F:\UTIL\KRONOS\DATA\SOMEKRONOSEMPS.DBF') THEN
*!*										DELETE FILE F:\UTIL\KRONOS\DATA\SOMEKRONOSEMPS.DBF
*!*									ENDIF
								IF FILE('F:\UTIL\KRONOS\DATA\KRONOSEMPS.DBF') THEN
									DELETE FILE F:\UTIL\KRONOS\DATA\KRONOSEMPS.DBF
								ENDIF

								* delete XLSs
*!*									IF FILE('F:\UTIL\KRONOS\DATA\ALLKRONOSEMPS.XLS') THEN
*!*										DELETE FILE F:\UTIL\KRONOS\DATA\ALLKRONOSEMPS.XLS
*!*									ENDIF
*!*									IF FILE('F:\UTIL\KRONOS\DATA\SOMEKRONOSEMPS.XLS') THEN
*!*										DELETE FILE F:\UTIL\KRONOS\DATA\SOMEKRONOSEMPS.XLS
*!*									ENDIF
								IF FILE('F:\UTIL\KRONOS\DATA\KRONOSEMPS.XLS') THEN
									DELETE FILE F:\UTIL\KRONOS\DATA\KRONOSEMPS.XLS
								ENDIF

								IF FILE('F:\UTIL\KRONOS\DATA\KRONOSEMPS.DBF') THEN
									.TrackProgress('Error deleting F:\UTIL\KRONOS\DATA\KRONOSEMPS.DBF', LOGIT+SENDIT)
								ENDIF

								IF USED('CURALLEMPS') THEN
									USE IN CURALLEMPS
								ENDIF
								IF USED('CURALLKRONOSEMPS') THEN
									USE IN CURALLKRONOSEMPS
								ENDIF
								IF USED('CURSOMEKRONOSEMPS') THEN
									USE IN CURSOMEKRONOSEMPS
								ENDIF
								IF USED('CURPAYEXPORTKRONOSEMPS') THEN
									USE IN CURPAYEXPORTKRONOSEMPS
								ENDIF
								
								
*!*					SELECT CUREMPACCTS
*!*					brow

*!*									* all ee's for review
*!*									SELECT ;
*!*										LEFT(HOMELABORLEVELNM7,4) AS TIMERVWR, ;
*!*										LEFT(HOMELABORLEVELDSC7,30) AS TRNAME, ;
*!*										LEFT(HOMELABORLEVELNM1,3) AS ADP_COMP, ;
*!*										LEFT(HOMELABORLEVELNM2,2) AS DIVISION, ;
*!*										LEFT(HOMELABORLEVELNM3,4) AS DEPT, ;
*!*										LEFT(PERSONFULLNAME,30) AS EMPLOYEE, ;
*!*										INT(VAL(PERSONNUM)) AS FILE_NUM ;
*!*										FROM CUREMPACCTS ;
*!*										INTO CURSOR CURALLKRONOSEMPS ;
*!*										WHERE INT(VAL(PERSONNUM)) IN ;
*!*										(SELECT FILE_NUM FROM CURADPACTIVEEMPS) ;
*!*										ORDER BY 1, 3, 4, 5, 6

*!*									* ssubset of ee's being rolled out next
*!*									SELECT ;
*!*										LEFT(HOMELABORLEVELNM7,4) AS TIMERVWR, ;
*!*										LEFT(HOMELABORLEVELDSC7,30) AS TRNAME, ;
*!*										LEFT(HOMELABORLEVELNM1,3) AS ADP_COMP, ;
*!*										LEFT(HOMELABORLEVELNM2,2) AS DIVISION, ;
*!*										LEFT(HOMELABORLEVELNM3,4) AS DEPT, ;
*!*										LEFT(PERSONFULLNAME,30) AS EMPLOYEE, ;
*!*										INT(VAL(PERSONNUM)) AS FILE_NUM ;
*!*										FROM CUREMPACCTS ;
*!*										INTO CURSOR CURSOMEKRONOSEMPS ;
*!*										WHERE INLIST(LEFT(HOMELABORLEVELNM4,3),'FL1') ;
*!*										AND INT(VAL(PERSONNUM)) IN ;
*!*										(SELECT FILE_NUM FROM CURADPACTIVEEMPS) ;
*!*										ORDER BY 1, 3, 4, 5, 6

*!*									* only ee's who would get exported to ADP from kronos based on F:\UTIL\KRONOS\DATA\TIMERVWR
*!*									SELECT ;
*!*										LEFT(HOMELABORLEVELNM7,4) AS TIMERVWR, ;
*!*										LEFT(HOMELABORLEVELDSC7,30) AS TRNAME, ;
*!*										LEFT(HOMELABORLEVELNM1,3) AS ADP_COMP, ;
*!*										LEFT(HOMELABORLEVELNM2,2) AS DIVISION, ;
*!*										LEFT(HOMELABORLEVELNM3,4) AS DEPT, ;
*!*										LEFT(PERSONFULLNAME,30) AS EMPLOYEE, ;
*!*										INT(VAL(PERSONNUM)) AS FILE_NUM ;
*!*										FROM CUREMPACCTS ;
*!*										INTO CURSOR CURPAYEXPORTKRONOSEMPS ;
*!*										WHERE LEFT(HOMELABORLEVELNM7,4) IN ;
*!*										(SELECT TIMERVWR FROM F:\UTIL\KRONOS\DATA\TIMERVWR WHERE LACTIVE) ;
*!*										AND INT(VAL(PERSONNUM)) IN ;
*!*										(SELECT FILE_NUM FROM CURADPACTIVEEMPS) ;
*!*										ORDER BY 1, 3, 4, 5, 6

								* revised 7/6/06 MB: Now selecting ee's even if they are 9999; also, no longer require Active status in ADP.
								* this is to correct problem where 9999's and/or those terminated in ADP are not listed
								* in kronosemps.dbf, and so get exported by Foxpro to ADP in batches.
								
								SELECT ;
									LEFT(LABORLEV7NM,4) AS TIMERVWR, ;
									LEFT(LABORLEV7DSC,30) AS TRNAME, ;
									LEFT(LABORLEV1NM,3) AS ADP_COMP, ;
									LEFT(LABORLEV2NM,2) AS DIVISION, ;
									LEFT(LABORLEV3NM,4) AS DEPT, ;
									LEFT(FULLNM,30) AS EMPLOYEE, ;
									INT(VAL(PERSONNUM)) AS FILE_NUM ;
									FROM CUREMPACCTS ;
									INTO CURSOR CURPAYEXPORTKRONOSEMPS ;
									WHERE LEFT(LABORLEV7NM,4) IN ;
									(SELECT TIMERVWR FROM F:\UTIL\KRONOS\DATA\TIMERVWR WHERE LACTIVE AND LHOURLY) ;
									OR LEFT(LABORLEV7NM,4) = '9999' ;
									ORDER BY 1, 3, 4, 5, 6
								
								* GET SALARIED AND HOURLY
								SELECT ;
									LEFT(LABORLEV7NM,4) AS TIMERVWR, ;
									LEFT(LABORLEV7DSC,30) AS TRNAME, ;
									LEFT(LABORLEV1NM,3) AS ADP_COMP, ;
									LEFT(LABORLEV2NM,2) AS DIVISION, ;
									LEFT(LABORLEV3NM,4) AS DEPT, ;
									LEFT(FULLNM,30) AS EMPLOYEE, ;
									INT(VAL(PERSONNUM)) AS FILE_NUM ;
									FROM CUREMPACCTS ;
									INTO CURSOR CURALLEMPS ;
									WHERE LEFT(LABORLEV7NM,4) IN ;
									(SELECT TIMERVWR FROM F:\UTIL\KRONOS\DATA\TIMERVWR WHERE LACTIVE) ;
									ORDER BY 1, 3, 4, 5, 6


*!*									* COPY DBF
*!*									SELECT CURALLKRONOSEMPS
*!*									*BROWSE
*!*									GOTO TOP
*!*									COPY TO F:\UTIL\KRONOS\DATA\ALLKRONOSEMPS

*!*									* COPY XLS
*!*									SELECT CURALLKRONOSEMPS
*!*									GOTO TOP
*!*									COPY TO F:\UTIL\KRONOS\DATA\ALLKRONOSEMPS.XLS XL5
*!*									USE

*!*									* COPY DBF
*!*									SELECT CURSOMEKRONOSEMPS
*!*									*BROWSE
*!*									GOTO TOP
*!*									COPY TO F:\UTIL\KRONOS\DATA\SOMEKRONOSEMPS

*!*									* COPY XLS
*!*									SELECT CURSOMEKRONOSEMPS
*!*									GOTO TOP
*!*									COPY TO F:\UTIL\KRONOS\DATA\SOMEKRONOSEMPS.XLS XL5
*!*									USE

								* COPY DBF
								SELECT CURPAYEXPORTKRONOSEMPS
								*BROWSE
								GOTO TOP
								COPY TO F:\UTIL\KRONOS\DATA\KRONOSEMPS FOR ADP_COMP = 'SXI'

								* COPY XLS
								SELECT CURPAYEXPORTKRONOSEMPS
								GOTO TOP
								COPY TO F:\UTIL\KRONOS\DATA\KRONOSEMPS.XLS XL5 FOR ADP_COMP = 'SXI'
								
								
								SELECT CURALLEMPS
								GOTO TOP
								COPY TO F:\UTIL\KRONOS\DATA\KRONOS_SALARIED_EMPS.XLS XL5 FOR ADP_COMP <> 'SXI'
								COPY TO F:\UTIL\KRONOS\DATA\KRONOS_ALL_EMPS.XLS XL5 && INCLUDES SALARIED, FOR REVIEW BY LUCILLE

								*!*							*  list in email
								*!*							lcList = ""
								*!*							SELECT CURSOMEKRONOSEMPS
								*!*							SCAN
								*!*								lcList = lcList + CURSOMEKRONOSEMPS.DIVISION + "   " + CURSOMEKRONOSEMPS.DEPT + "  " + CURSOMEKRONOSEMPS.EMPLOYEE + "     " + TRANSFORM(CURSOMEKRONOSEMPS.FILE_NUM) + "     " + CURSOMEKRONOSEMPS.TIMERVWR + CRLF
								*!*							ENDSCAN
								*!*							.TrackProgress('DIV  DEPT  EMPLOYEE                           FILE#    TIMEREVIEWER', LOGIT+SENDIT)
								*!*							.TrackProgress(lcList, LOGIT+SENDIT)

								*!*							* list TIMERVWR recs
								*!*							lcList = ""
								*!*							SELECT TIMERVWR
								*!*							SCAN
								*!*								lcList = lcList + TIMERVWR.TIMERVWR + "          " + TIMERVWR.NAME + CRLF
								*!*							ENDSCAN
								*!*							.TrackProgress('TIMEREVIEWER#, NAME    ', LOGIT+SENDIT)
								*!*							.TrackProgress(lcList, LOGIT+SENDIT)


							ENDIF  &&  .ExecSQL(lcSQL, 'CURWTKEMPLOYEE', RETURN_DATA_MANDATORY)

							CLOSE DATABASES ALL

							IF NOT FILE('F:\UTIL\KRONOS\DATA\KRONOSEMPS.DBF') THEN
								.TrackProgress('Error creating F:\UTIL\KRONOS\DATA\KRONOSEMPS.DBF', LOGIT+SENDIT)
							ELSE
								.TrackProgress('Created F:\UTIL\KRONOS\DATA\KRONOSEMPS.DBF!', LOGIT+SENDIT)
							ENDIF

							IF NOT FILE('F:\UTIL\KRONOS\DATA\KRONOSEMPS.xls') THEN
								.TrackProgress('Error creating F:\UTIL\KRONOS\DATA\KRONOSEMPS.xls', LOGIT+SENDIT)
							ELSE
								.TrackProgress('Created F:\UTIL\KRONOS\DATA\KRONOSEMPS.xls!', LOGIT+SENDIT)
								.cAttach = .cAttach + 'F:\UTIL\KRONOS\DATA\KRONOSEMPS.xls'
							ENDIF

*!*								IF NOT FILE('F:\UTIL\KRONOS\DATA\ALLKRONOSEMPS.DBF') THEN
*!*									.TrackProgress('Error creating F:\UTIL\KRONOS\DATA\ALLKRONOSEMPS.DBF', LOGIT+SENDIT)
*!*								ELSE
*!*									.TrackProgress('Created F:\UTIL\KRONOS\DATA\ALLKRONOSEMPS.DBF!', LOGIT+SENDIT)
*!*								ENDIF
*!*								IF NOT FILE('F:\UTIL\KRONOS\DATA\ALLKRONOSEMPS.XLS') THEN
*!*									.TrackProgress('Error creating F:\UTIL\KRONOS\DATA\ALLKRONOSEMPS.XLSF', LOGIT+SENDIT)
*!*								ELSE
*!*									.TrackProgress('Created F:\UTIL\KRONOS\DATA\ALLKRONOSEMPS.XLS!', LOGIT+SENDIT)
*!*									.cAttach = .cAttach + ',F:\UTIL\KRONOS\DATA\ALLKRONOSEMPS.XLS'
*!*								ENDIF

*!*								IF NOT FILE('F:\UTIL\KRONOS\DATA\SOMEKRONOSEMPS.DBF') THEN
*!*									.TrackProgress('Error creating F:\UTIL\KRONOS\DATA\SOMEKRONOSEMPS.DBF', LOGIT+SENDIT)
*!*								ELSE
*!*									.TrackProgress('Created F:\UTIL\KRONOS\DATA\SOMEKRONOSEMPS.DBF!', LOGIT+SENDIT)
*!*								ENDIF

*!*								IF NOT FILE('F:\UTIL\KRONOS\DATA\SOMEKRONOSEMPS.XLS') THEN
*!*									.TrackProgress('Error creating F:\UTIL\KRONOS\DATA\SOMEKRONOSEMPS.XLS', LOGIT+SENDIT)
*!*								ELSE
*!*									.TrackProgress('Created F:\UTIL\KRONOS\DATA\SOMEKRONOSEMPS.XLS!', LOGIT+SENDIT)
*!*									.cAttach = .cAttach + ',F:\UTIL\KRONOS\DATA\SOMEKRONOSEMPS.XLS'
*!*								ENDIF

						ELSE
							* connection error
							.TrackProgress('Unable to connect to WTK System.', LOGIT+SENDIT)
						ENDIF   &&  .nSQLHandle > 0

					ENDIF  &&  .ExecSQL(lcADPSQL, 'CURADPACTIVEEMPSPRE', RETURN_DATA_MANDATORY)

				ELSE
					* connection error
					.TrackProgress('Unable to connect to ADP Payroll System.', LOGIT+SENDIT)
				ENDIF   &&  .nSQLHandle > 0

				CLOSE DATABASES ALL

			CATCH TO loError

				.TrackProgress('There was an error.',LOGIT+SENDIT)
				.TrackProgress('The Error # is: ' + TRANSFORM(loError.ErrorNo), LOGIT+SENDIT)
				.TrackProgress('The Error Message is: ' + TRANSFORM(loError.MESSAGE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred in Program: ' + TRANSFORM(loError.PROCEDURE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred at line #: ' + TRANSFORM(loError.LINENO), LOGIT+SENDIT)
				lnNumberOfErrors = lnNumberOfErrors + 1
				IF TYPE('oExcel') = 'O' AND NOT ISNULL(oExcel) THEN
					oExcel.QUIT()
				ENDIF
				CLOSE DATA
				CLOSE ALL

			ENDTRY

			CLOSE DATA
			WAIT CLEAR
			***************** INTERNAL email results ******************************
			.TrackProgress('About to send status email.',LOGIT)
			.TrackProgress('==================================================================================================================', SENDIT)
			.TrackProgress('The logfile is: ' + .cLogFile, SENDIT)
			.TrackProgress('LIST KRONOS EMPLOYEES process started: ' + .cStartTime, LOGIT+SENDIT)
			.TrackProgress('LIST KRONOS EMPLOYEES process finished: ' + TTOC(DATETIME()), LOGIT+SENDIT)

			IF .lSendInternalEmailIsOn THEN
				* try to trap error from not having dartmail dll's registered on user's pc...
				TRY
					DO FORM dartmail2 WITH .cSendTo,.cFrom,.cSubject,.cCC,.cAttach,.cBodyText,"A"
					.TrackProgress('Sent status email.',LOGIT)
				CATCH TO loError
					=MESSAGEBOX('There was an error sending the status email. Please contact Mark Bennett.',0+64,"LIST KRONOS EMPLOYEES")
					.TrackProgress('There was an error sending the status email.',LOGIT)
					.TrackProgress('The Error # is: ' + TRANSFORM(loError.ErrorNo), LOGIT)
					.TrackProgress('The Error Message is: ' + TRANSFORM(loError.MESSAGE), LOGIT)
					.TrackProgress('The Error occurred in Program: ' + TRANSFORM(loError.PROCEDURE), LOGIT)
					.TrackProgress('The Error occurred at line #: ' + TRANSFORM(loError.LINENO), LOGIT)
					lnNumberOfErrors = lnNumberOfErrors + 1
					CLOSE DATA
				ENDTRY
			ELSE
				.TrackProgress('Did not send status email: THIS.lSendInternalEmailIsOn = FALSE.',LOGIT)
			ENDIF

		ENDWITH
		RETURN
	ENDFUNC && main


	FUNCTION ExecSQL
		LPARAMETERS tcSQL, tcCursorName, tlNoDataReturnedIsOkay
		LOCAL llRetval, lnResult
		WITH THIS
			* close target cursor if it's open
			IF USED(tcCursorName)
				USE IN (tcCursorName)
			ENDIF
			WAIT WINDOW "Fetching remote data; please wait..." NOWAIT
			lnResult = sqlexec(.nSQLHandle, tcSQL, tcCursorName)
			llRetval = ( lnResult > 0 )
			IF llRetval THEN
				* see if any data came back
				IF NOT tlNoDataReturnedIsOkay THEN
					IF (NOT USED(tcCursorName)) OR EOF(tcCursorName) THEN
						llRetval = .F.
						.TrackProgress('No data was returned by this query: ' +CRLF + tcSQL, LOGIT+SENDIT)
					ENDIF
				ENDIF
			ELSE
				.TrackProgress("Result Code [" + TRANSFORM(lnResult) + "] returned by this query: " + tcSQL, LOGIT+SENDIT)
			ENDIF
			WAIT CLEAR
			RETURN llRetval
		ENDWITH
	ENDFUNC


	PROCEDURE TrackProgress
		* do any combination of Wait Window, writing to logfile, and adding to body of email,
		* based on nFlags parameter.
		LPARAMETERS tcExpression, tnFlags
		WITH THIS
			IF BITAND(tnFlags,LOGIT) = LOGIT THEN
				IF .lLoggingIsOn THEN
					?
					? .cProcessName + "  " + TTOC(DATETIME()) + ": " + tcExpression
				ENDIF
			ENDIF
			IF .lWaitWindowIsOn THEN
				IF BITAND(tnFlags,NOWAITIT) = NOWAITIT THEN
					WAIT WINDOW tcExpression NOWAIT
				ENDIF
				IF BITAND(tnFlags,WAITIT) = WAITIT THEN
					WAIT WINDOW tcExpression TIMEOUT .nWaitWindowTimeout
				ENDIF
			ENDIF
			IF BITAND(tnFlags,SENDIT) = SENDIT THEN
				IF .lSendInternalEmailIsOn THEN
					.cBodyText = .cBodyText + tcExpression + CRLF + CRLF
				ENDIF
			ENDIF
		ENDWITH
	ENDPROC  &&  TrackProgress

ENDDEFINE

