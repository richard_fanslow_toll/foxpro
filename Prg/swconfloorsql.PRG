* called from outwo, stageman, staging

lparameters xfrom, xwonum

do case
case xfrom="STAGING"
	xswcloc="vswcloc"
	xoutship="outship"
	xfilter=".t."
case inlist(xfrom,"OUTWO","SHIPMENT")
	xswcloc="swcloc"
	xoutship="voutship"
	xfilter="wo_num="+transform(xwonum)
case inlist(xfrom,"STAGEMAN","STAGEPUTLXE")
	xswcloc="swcloc"
	xoutship="outship"
	xfilter="wo_num="+transform(xwonum)
endcase

select (xswcloc)
scan for &xfilter
	xonfloor=.t.
	
	if seek(stageloc,"whseloc","whseloc") and whseloc.loctype#"FLOOR"
		xonfloor=.f.
	else
		try
			xsqlexec("select * from swcstyle where swcdetid="+transform(&xswcloc..swcdetid),"xswcstyle",,"wh")
			if reccount()#0
				select (xoutship)
				locate for accountid=xswcstyle.accountid and ship_ref=xswcstyle.ship_ref
				if found()
					if !emptynul(del_date) or notonwip
						xonfloor=.f.
					endif
				endif
			endif
		catch
			xonfloor=.f.
		endtry
	endif

	do case
	case xonfloor and empty(&xswcloc..onfloordt)
		replace onfloordt with qdate(), onfloortm with time2chr(substr(ttoc(qdate(.t.)),12)) in (xswcloc)
	case !xonfloor and !empty(&xswcloc..onfloordt)
		replace onfloordt with {}, onfloortm with "" in (xswcloc)
	endcase
endscan
