*!* m:\dev\prg\moret940_bkdn.prg
WAIT WINDOW "Now in BKDN phase" nowait
ldoImport = .T.

*!* Added to check for the presence of mandatory elements in 940
CREATE CURSOR tempload (field1 c(254))
APPEND FROM [&xfile] TYPE SDF
LOCATE
*BROWSE timeout 2
COUNT TO nHD FOR LEFT(field1,2) = "HD"
IF nHD = 0
	cUserMessage = "No HEADER(HD) segments in file "+xfile
	DELETE FILE [&xfile]
	ldoImport = .F.
	RETURN
ENDIF
WAIT WINDOW "There are "+ALLT(STR(nHD))+" unique Headers in this file" nowait
LOCATE
COUNT TO nTT FOR LEFT(field1,2) = "TT"
IF nTT#nHD
	cUserMessage = "Missing terminator (TT) segments in file "+xfile
	WAIT WINDOW cUserMessage TIMEOUT 1
	lMoretSend = .T.
	THROW
ENDIF
WAIT WINDOW "Now checking for mandatory fields" NOWAIT
STORE 0 TO nHD,nVN,nCN,nST,nDT,nTT
*ASSERT .f.
SCAN
	DO CASE
		CASE LEFT(field1,2) = "HD"
			cShip_ref = SUBSTR(field1,6,6)
			nHD = nHD+1
		CASE LEFT(field1,2) = "VN"
			nVN = nVN+1
		CASE LEFT(field1,2) = "CN"
			nCN = nCN+1
		CASE LEFT(field1,2) = "ST"
			nST = nST+1
		CASE LEFT(field1,2) = "DT"
			nDT = nDT+1
		CASE LEFT(field1,2) = "TT"
			nTT = nTT+1
			IF nHD>0 AND nVN>0 AND nCN>0 AND nST>0 AND nDT>0 AND nTT>0
				WAIT WINDOW "All mandatory elements exist for PT "+cShip_ref NOWAIT
				STORE 0 TO nHD,nVN,nCN,nST,nDT,nTT
			ELSE
				cUserMessage = "Missing Segment(s) at PT "+cShip_ref+" "
				IF nHD = 0
					cUserMessage = cUserMessage+"HD"
				ENDIF
				IF nVN = 0
					cUserMessage = cUserMessage+",VN"
				ENDIF
				IF nCN = 0
					cUserMessage = cUserMessage+",CN"
				ENDIF
				IF nST = 0
					cUserMessage = cUserMessage+",ST"
				ENDIF
				IF nDT = 0
					cUserMessage = cUserMessage+",DT"
				ENDIF
				IF nTT = 0
					cUserMessage = cUserMessage+",TT"
				ENDIF
				lMoretSend = .T.
				THROW
			ENDIF
	ENDCASE
ENDSCAN
WAIT WINDOW "All mandatory structures present...exiting" nowait
USE IN tempload
*!* End addition

lBrowfiles = .F.
*ASSERT .F.
xhan = FOPEN(xfile)
IF xhan<1
	cUserMessage = "Unable to open file handle."
	THROW
ENDIF

m.pack = "1"
lDT = .F.
cCo = LEFT(ALLT(xfile),1)
nInLine = 0
nQty = 0
nSubPack = 0
m.weight = 0
cSDRecType =""
STORE 1 TO m.ptid,m.ptdetid
STORE DATE() TO m.ptdate

DO WHILE !FEOF(xhan)
	xdata=FGETS(xhan,300)
	xlinecode=LEFT(xdata,2)
	DO CASE
		CASE xlinecode='HD' && Header
			STORE 0 TO m.storenum
			STORE "" TO m.consignee,m.name,m.dcnum,m.address,m.address2,m.csz,m.dept
			STORE "" TO m.shipfor,m.sforstore,m.sforaddr1,m.sforaddr2,m.sforcsz
			lDoSub = .F.
			cCoCode = ALLT(SUBSTR(xdata,3,3))
			m.shipins = "COCODE*"+cCoCode
			m.ship_ref = ALLT(SUBSTR(xdata,6,6))
			cmOrder = ALLT(SUBSTR(xdata,12,6))
			IF !EMPTY(cmOrder)
				m.shipins = m.shipins+CHR(10)+"M_ORDER*"+cmOrder
			ENDIF
			cUCC = ALLT(SUBSTR(xdata,105,6))
			IF !EMPTY(cUCC)
				m.shipins = m.shipins+CHR(10)+"UCC*"+cUCC
			ENDIF
			cMR = ALLT(SUBSTR(xdata,41,5))
			IF !EMPTY(cMR)
				m.shipins = m.shipins+CHR(10)+"MR*"+cMR
			ENDIF
			m.vendor_num = ALLT(SUBSTR(xdata,76,20))
			m.cnee_ref = ALLT(SUBSTR(xdata,18,15))
			m.start = ALLT(SUBSTR(xdata,54,8))
			m.start = CTOD(SUBSTR(m.start,5,2)+"/"+SUBSTR(m.start,7,2)+"/"+LEFT(m.start,4))
			m.cancel = ALLT(SUBSTR(xdata,62,8))
			m.cancel = CTOD(SUBSTR(m.cancel,5,2)+"/"+SUBSTR(m.cancel,7,2)+"/"+LEFT(m.cancel,4))
			m.dept = ALLT(SUBSTR(xdata,70,6))
			m.duns = ALLT(SUBSTR(xdata,96,9))
			m.ship_via = ALLT(SUBSTR(xdata,111,35))
			m.scac = ALLT(SUBSTR(xdata,146,4))
			cCarrMode = ALLT(SUBSTR(xdata,150,2))
			cParty = ALLT(SUBSTR(xdata,234,2))
			IF !EMPTY(cCarrMode)
				m.shipins = m.shipins+CHR(10)+"CARRMODE*"+cCarrMode
				m.shipins = m.shipins+CHR(10)+"PARTY*"+cParty
			ENDIF
			m.shipins = m.shipins+CHR(10)+"PRIORITY*"+;
				IIF(ALLT(SUBSTR(xdata,233,1))='Y','Y','N')
			cFOB = ALLT(SUBSTR(xdata,236,30))
			IF !EMPTY(cFOB)
				m.shipins = m.shipins+CHR(10)+"FOB*"+cFOB
			ENDIF
			cFOBPymt = ALLT(SUBSTR(xdata,152,1))
			IF !EMPTY(cFOBPymt)
				m.shipins = m.shipins+CHR(10)+"FOBPYMT*"+cFOBPymt
			ENDIF
			m.shipins = m.shipins+CHR(10)+"CTNPREFIX*"+ALLT(SUBSTR(xdata,275,4))
			m.shipins = m.shipins+CHR(10)+"CONTENTLBL*"+;
				IIF(ALLT(SUBSTR(xdata,279,1))='P','Y','N')
			cDivCode = ALLT(SUBSTR(xdata,280,2))
			IF !EMPTY(cDivCode)
				m.shipins = m.shipins+CHR(10)+"DIVCODE*"+cDivCode
			ENDIF

		CASE xlinecode='IN'
			nInLine = nInLine + 1
			cInLine = ALLT(STR(nInLine))
			m.shipins = m.shipins+CHR(10)+"IN"+cInLine+"*"+ALLT(SUBSTR(xdata,3,55))

		CASE xdata='VN'  && Company ID Info
			m.shipins = m.shipins+CHR(10)+"VNUM*"+ALLT(SUBSTR(xdata,3,6))
			cVNCompany = ALLT(SUBSTR(xdata,9,35))
			IF "INTIMATES"$cVNCompany
				m.accountid = 5449
			ENDIF
			m.shipins = m.shipins+CHR(10)+"COMPANY*"+cVNCompany
			m.shipins = m.shipins+CHR(10)+"COADDR1*"+ALLT(SUBSTR(xdata,44,35))
			m.shipins = m.shipins+CHR(10)+"COADDR2*"+ALLT(SUBSTR(xdata,79,35))
			cCity = ALLT(SUBSTR(xdata,114,30))
			cState = ALLT(SUBSTR(xdata,144,2))
			cZip = ALLT(SUBSTR(xdata,146,9))
			cVNGLN = ALLT(SUBSTR(xdata,155,13))
			m.shipins = m.shipins+CHR(10)+"VNGLN*"+cVNGLN
			cCSZ = cCity+", "+cState+" "+cZip
			m.shipins = m.shipins+CHR(10)+"COCSZ*"+cCSZ

		CASE xdata='CN'  && Customer Info
			m.cacctnum = PADL(ALLT(SUBSTR(xdata,3,6)),6,"0")
			m.shipins = m.shipins+CHR(10)+"CUSTOMER*"+ALLT(SUBSTR(xdata,9,35))
			m.shipins = m.shipins+CHR(10)+"CUSADDR1*"+ALLT(SUBSTR(xdata,44,35))
			m.shipins = m.shipins+CHR(10)+"CUSADDR2*"+ALLT(SUBSTR(xdata,79,35))
			cCity = ALLT(SUBSTR(xdata,114,30))
			cState = ALLT(SUBSTR(xdata,144,2))
			cZip = ALLT(SUBSTR(xdata,146,9))
			cCNGLN = ALLT(SUBSTR(xdata,155,13))
			m.shipins = m.shipins+CHR(10)+"CNGLN*"+cCNGLN
			cCSZ = cCity+", "+cState+" "+cZip
			m.shipins = m.shipins+CHR(10)+"CUSCSZ*"+cCSZ

		CASE xdata='ST'  && Ship-to Info
			m.dcnum = PADL(ALLT(SUBSTR(xdata,3,6)),5,"0")
			m.storenum = INT(VAL(m.dcnum))
			m.consignee = ALLT(SUBSTR(xdata,9,35))
			STORE m.consignee TO m.name
			m.address = ALLT(SUBSTR(xdata,44,35))
			m.address2 = ALLT(SUBSTR(xdata,79,35))
			IF EMPTY(m.address) AND !EMPTY(m.address2)
				STORE m.address2 TO m.address
				STORE "" TO m.address2
			ENDIF
			cCity = ALLT(SUBSTR(xdata,114,30))
			cState = ALLT(SUBSTR(xdata,144,2))
			cZip = ALLT(SUBSTR(xdata,146,9))
			cSTGLN = ALLT(SUBSTR(xdata,155,13))
			m.shipins = m.shipins+CHR(10)+"STGLN*"+cSTGLN
			m.csz = cCity+", "+cState+" "+cZip

		CASE xdata='MF'  && Mark-for/Ship-for Info
			m.sforstore = PADL(ALLT(SUBSTR(xdata,3,6)),5,"0")
			m.shipfor = ALLT(SUBSTR(xdata,9,35))
			m.sforaddr1 = ALLT(SUBSTR(xdata,44,35))
			m.sforaddr2 = ALLT(SUBSTR(xdata,79,35))
			IF EMPTY(m.sforaddr1) AND !EMPTY(m.sforaddr2)
				STORE m.sforaddr2 TO m.sforaddr1
				STORE "" TO m.sforaddr2
			ENDIF
			cCity = ALLT(SUBSTR(xdata,114,30))
			cState = ALLT(SUBSTR(xdata,144,2))
			cZip = ALLT(SUBSTR(xdata,146,9))
			cMFGLN = ALLT(SUBSTR(xdata,155,13))
			m.shipins = m.shipins+CHR(10)+"MFGLN*"+cMFGLN
			m.sforcsz = cCity+", "+cState+" "+cZip

		CASE xdata='DT'  && Line Item Detail
			IF lDT
				INSERT INTO xptdet FROM MEMVAR
				m.pack = "1"
				nSubPack = 0
				m.printstuff = ""
				m.ptdetid = m.ptdetid + 1
			ENDIF
			lDT = .T.
			IF m.ship_ref = "A 9041"
				ASSERT .F. MESSAGE "At ship_ref DT point"
			ENDIF
			cPacktype = IIF(ALLT(SUBSTR(xdata,130,1))="P","PICKPACK","PREPACK")
			lPrepack = IIF(cPacktype="PREPACK",.T.,.F.)
			cSDRecType = ALLT(SUBSTR(xdata,138,1))
			m.batch_num = IIF(lPrepack,"C","U")
			IF !("PACKTYPE*"$m.shipins)
				m.shipins = m.shipins+CHR(10)+"PACKTYPE*"+cPacktype
			ENDIF
			IF lDoSub && AND cSDRecType # "C"
				INSERT INTO xptdet FROM MEMVAR
				m.pack = "1"
				nSubPack = 0
				m.printstuff = ""
				m.ptdetid = m.ptdetid + 1
			ENDIF
			IF lDT
				lDoSub = .F.
			ENDIF
*			ASSERT .F. MESSAGE "At DT section"
			m.linenum = ALLT(SUBSTR(xdata,3,3))
			lDoSub = IIF(m.linenum="000",.T.,.F.)
			m.style = ALLT(SUBSTR(xdata,6,12))
			m.printstuff = "STYLEDESC*"+ALLT(SUBSTR(xdata,18,80))
			m.custsku = ALLT(SUBSTR(xdata,98,20))
			cMastSKU = m.custsku
			m.upc = ALLT(SUBSTR(xdata,118,12))
			m.units = IIF(!lPrepack,.T.,.F.)
*			IF !lPrepack
			m.pack = "1"
*			ENDIF
			m.totqty = INT(VAL(SUBSTR(xdata,131,7)))
			STORE m.totqty TO m.origqty
			STORE m.totqty TO m.qty
			nQty = nQty + m.totqty
			IF DATE() < {^2008-01-29}
				cSDHeader = "SD*"+ALLT(SUBSTR(xdata,138,1))
			ELSE
				m.printstuff = "INNERPACK*"+ALLT(SUBSTR(xdata,138,7))
				m.printstuff = "CUSTGTIN*"+ALLT(SUBSTR(xdata,145,14))
				cSDHeader = "SD*"+ALLT(SUBSTR(xdata,159,1))
			ENDIF
			IF !lDT
			ELSE
				LOOP
			ENDIF

		CASE xdata = "SD"
			lDT = .F.
			m.printstuff = m.printstuff+CHR(10)+cSDHeader
			IF lPrepack OR cSDRecType = "C"
				lDoSub = .T.
			ENDIF
			IF lDoSub
				nSubPack = INT(VAL(SUBSTR(xdata,132,7)))
				m.printstuff = m.printstuff+CHR(10)+;
					"SUBLINE*"+ALLT(SUBSTR(xdata,3,3))+CHR(10)+;
					"SUBCUSTSKU*"+ALLT(SUBSTR(xdata,98,20))+CHR(10)+;
					"SUBSTYLE*"+ALLT(SUBSTR(xdata,6,12))+CHR(10)+;
					"SUBSTYLEDESC*"+ALLT(SUBSTR(xdata,18,80))+CHR(10)+;
					"SUBUOM*"+ALLT(SUBSTR(xdata,130,2))+CHR(10)+;
					"SUBPACK*"+ALLT(STR(nSubPack))+CHR(10)+;
					"SUBUPC*"+ALLT(SUBSTR(xdata,118,12))+CHR(10)+;
					"SUBCOLOR*"+ALLT(SUBSTR(xdata,139,10))+CHR(10)+;
					"SUBSIZE*"+ALLT(SUBSTR(xdata,149,10))+CHR(10)
				IF lPrepack
					m.pack = "1"
*					m.pack = ALLT(STR(INT(VAL(m.pack))+nSubPack))
				ENDIF
			ELSE
				INSERT INTO xptdet FROM MEMVAR
				m.printstuff = ""
				nSubPack = 0
				m.pack = ""
				m.ptdetid = m.ptdetid + 1
			ENDIF

		CASE xdata='TT'  && End of Pickticket Indicator
			nInLine = 0
			IF lDoSub OR lDT
				INSERT INTO xptdet FROM MEMVAR
				m.printstuff = ""
				m.ptdetid = m.ptdetid + 1
			ENDIF
			lDT = .F.
			INSERT INTO xpt FROM MEMVAR
			REPLACE xpt.qty WITH nQty IN xpt
			REPLACE xpt.origqty WITH xpt.qty IN xpt
			m.pack = ""
			nSubPack = 0
			m.shipins = ""
			nQty = 0
			m.ptid = m.ptid + 1

	ENDCASE
ENDDO

=FCLOSE(xhan)

SELECT ptid,units,COUNT(ptdetid) AS detcnt ;
	FROM xptdet ;
	GROUP BY 1,2 ;
	INTO CURSOR det1

SELECT 0
CREATE CURSOR detscan (ptid i)
SELECT det1
nPTID = 0
lUnits = .F.
SCAN
	IF nPTID<>det1.ptid
		nPTID = det1.ptid
		lUnits = det1.units
		LOOP
	ELSE
		IF lUnits<>det1.units
			INSERT INTO detscan (ptid) VALUES (det1.ptid)
		ENDIF
	ENDIF
ENDSCAN

*ASSERT .f. MESSAGE "At DETSCAN run..."
SELECT detscan
SCAN
	nPTID = detscan.ptid  && Original PTID
	SELECT xpt
	LOCATE
	CALCULATE MAX(xpt.ptid) TO m.ptid
	m.ptid = m.ptid+1
	STORE m.ptid  TO nPTID2 && New PTID
	LOCATE
	LOCATE FOR xpt.ptid = nPTID
	SCATTER MEMVAR FIELDS EXCEPT ptid MEMO
	INSERT INTO xpt FROM MEMVAR
	GO BOTTOM
	REPLACE xpt.ship_ref WITH TRIM(xpt.ship_ref)+" ~C" IN xpt  &&  Cases pickticket
*	ASSERT .f. MESSAGE "At Packtype correction"
	IF !("PACKTYPE*"$xpt.shipins)
		REPLACE xpt.shipins WITH "PACKTYPE*PREPACK"+CHR(10)+xpt.shipins IN xpt
	ELSE
		IF ("PACKTYPE*PICKPACK"$xpt.shipins)
			REPLACE xpt.shipins WITH ;
				STRTRAN(xpt.shipins,"PACKTYPE*PICKPACK","PACKTYPE*PREPACK") IN xpt
		ENDIF
	ENDIF

	REPLACE xpt.qty WITH 0,xpt.origqty WITH 0 IN xpt NEXT 1
	LOCATE
	LOCATE FOR xpt.ptid = nPTID
	REPLACE xpt.ship_ref WITH TRIM(xpt.ship_ref)+" ~U" IN xpt  && Units pickticket
	REPLACE xpt.qty WITH 0,xpt.origqty WITH 0,xpt.batch_num WITH "U" IN xpt NEXT 1
	IF !("PACKTYPE*"$xpt.shipins)
		REPLACE xpt.shipins WITH "PACKTYPE*PICKPACK"+CHR(10)+xpt.shipins IN xpt
	ELSE
		IF ("PACKTYPE*PREPACK"$xpt.shipins)
			REPLACE xpt.shipins WITH ;
				STRTRAN(xpt.shipins,"PACKTYPE*PREPACK","PACKTYPE*PICKPACK") IN xpt
		ENDIF
	ENDIF

	SELECT xptdet
	LOCATE
	SCAN FOR xptdet.ptid = nPTID
		IF !units
			REPLACE xptdet.ptid WITH nPTID2 IN xptdet
		ENDIF
	ENDSCAN
	SELECT xptdet
	LOCATE
	SUM xptdet.totqty TO nQty FOR xptdet.ptid = nPTID
	SELECT xpt
	REPLACE xpt.qty WITH nQty FOR xpt.ptid = nPTID IN xpt
	REPLACE xpt.origqty WITH xpt.qty FOR xpt.ptid = nPTID IN xpt
	SELECT xptdet
	LOCATE
	SUM xptdet.totqty TO nQty FOR xptdet.ptid = nPTID2
	SELECT xpt
	REPLACE xpt.qty WITH nQty FOR xpt.ptid = nPTID2 IN xpt
	REPLACE xpt.origqty WITH xpt.qty FOR xpt.ptid = nPTID2 IN xpt
	STORE 0 TO nPTID,nPTID2
ENDSCAN

IF lBrowfiles
	WAIT WINDOW "Now browsing XPT/XPTDET files..." TIMEOUT 1
	SELECT xpt
	SET ORDER TO
*	SET ORDER TO tag ship_ref
	LOCATE
	BROWSE
	SELECT xptdet
*	SET ORDER TO tag ptid
	LOCATE
	BROWSE
	IF MESSAGEBOX("Do you wish to upload this BKDN?",4+32,"Continue?") = 7
		CANCEL
		RETURN
	ENDIF
ENDIF
