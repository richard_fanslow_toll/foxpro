xindir="f:\ftpusers\PVH\850in\"
xoutdir="f:\ftpusers\toll\pvh\850out\"
*xoutdir="f:\ftpusers\toll\pvh\850test\"

xfilecnt=Adir(afiles,xindir+"*.edi")
if xfilecnt=0
	return
endif

xcolcnt=25
xtollcustcode="PVH" &&temporarily entered as "PVH" for testing 

if empty(xtollcustcode) or xtollcustcode="PVH"
	xFrom="TGFSYSTEM"
	if xtollcustcode="PVH"
		xSubject="PVH 850 ICON Transalation May Have A Test Toll Customer Code!! Must Change Before Going Live!"
	else
		xSubject="PVH 850 ICON Transalation Cannot Be Run - Empty Toll Customer Code!!"
	endif
	xbody=""
	do form dartmail2 with "mwinter@fmiint.com",xfrom,xsubject," ",,xbody,"a"

	if empty(xtollcustcode)
		return
	endif
endif

xseparator=chr(13)+chr(10)

use f:\sysdata\qqdata\unloccode in 0

select 0
create cursor x850 (field1 c(254),recno n(14,3))
index on recno tag recno

set century on
set hours to 24

create cursor xfilelist (file c(100), x c(10), moddt C(10), modtm c(10), moddttm t(1))
append from array afiles
replace all moddttm with ctot(moddt+" "+modtm)

xfilecreated=.f.
select * from xfilelist order by moddttm into cursor xfilelist
scan
	zap in x850

	**start with column headings
	xfilestr="order_no|order_split|order_status|client_cust_code|supplier_code|supplier_name|supplier_address_1|"+;
		"supplier_address_2|supplier_city|supplier_region_state|supplier_country|supplier_postcode|confirm_number|"+;
		"confirm_date|exw_required_by|delivery_required_by|description|order_date_time|order_value|order_currency_code|"+;
		"exchange_rate|incoterms|additional_terms|transport_mode|container_mode|milestone_tracking_date_exw|"+;
		"milestone_tracking_date_etd|milestone_tracking_date_eta|custom_milestone_est1|custom_milestone_act1|"+;
		"custom_milestone_est2|custom_milestone_act2|custom_milestone_est3|custom_milestone_act3|custom_milestone_est4|"+;
		"custom_milestone_act4|sending_agent|sending_agent_name|origin_port|destination_port|custom_text1|custom_text2|"+;
		"custom_text3|custom_text4|custom_text5|custom_date1|custom_date2|custom_decimal1|custom_decimal2|custom_decimal3|"+;
		"custom_decimal4|custom_decimal5|custom_flag1|custom_flag2|custom_flag3|custom_flag4|custom_flag5|contact1|"+;
		"contact2|country_code|notes|line_no|subline_no|linesplit_no|product|line_description|qty_ordered|innerpacks|"+;
		"outerpacks|unit_of_measure|itemprice|lineprice|linestatus|required_date|part_attrib1|part_attrib2|part_attrib3|"+;
		"line_custom_attrib1|line_custom_attrib2|line_custom_attrib3|line_custom_attrib4|line_custom_attrib5|"+;
		"line_custom_date1|line_custom_date2|line_custom_date3|line_custom_date4|line_custom_date5|line_custom_decimal1|"+;
		"line_custom_decimal2|line_custom_decimal3|line_custom_decimal4|line_custom_decimal5|line_custom_flag1|"+;
		"line_custom_flag2|line_custom_flag3|line_custom_flag4|line_custom_flag5|line_custom_text1|container_number|"+;
		"container_packing_order|country_of_origin|dg_substance|dg_flashpoint|weight|volume|weight_type|volume_type|"+;
		"special_instructions|additional_information|delivery_port|address_code|delivery_address_name|"+;
		"delivery_address_address1|delivery_address_address2|delivery_address_city|delivery_address_state|"+;
		"delivery_address_country|delivery_address_postcode"+chr(13)+chr(10)
	xhdrlen=len(xfilestr)

	xfilename = xindir+alltrim(xfilelist.file)
	xarchiveinfile = xindir+"archive\"+alltrim(xfilelist.file)

	**looks like seg terminator is already chr(13)+chr(10)
*!*		xfile=filetostr(xfilename)
*!*		xfile=strtran(xfile,"~",chr(13)+chr(10))
*!*		xtmpfile=xindir+"tmp"+strtran(strtran(strtran(ttoc(datetime()),"/",""),":","")," ","")+".tmp"
*!*		strtofile(xfile,xtmpfile)

	xchar=chr(13)+chr(10)
	select x850
	**no need for tmp if above is commented out
*!*		append from "&xtmpfile" type delimited with character &xchar
	**added try catch in attempt to avoid "file access denied" error, believe its coming 
	**  from the append if the file has yet to be completely ftp'd - mvw 06/29/12
	xerror=.f.
	try
		append from "&xfilename" type delimited with character &xchar
	catch
		xerror=.t.
	endtry

	if xerror
		loop
	endif

	replace all field1 with alltrim(upper(field1)), recno with recno()
	locate

	**same as above
*!*		delete file &xtmpfile

	**default delivery port UNLOC code required for Icon - default to LAX
	xdelivport="USLAX"

	**keep track of previous po line as we have seen instances where PVH has sent the same line number 
	**	multiple times, each containing different skus - mvw 02/14/112
	xprevlineno=""
	**need sln to be defined here now in case i need to combine 2 identical line numbers for same po - mvw 2/14/12
	xslncnt=1

	**string to hold the data for each po as we need to only add to xfilestr once a po is complete - mvw 01/16/13
	xpostr=""

	xerror=.f.
	do while !eof()
		do case
		case left(field1,3)="ISA"
		case left(field1,2)="GS"
		case left(field1,2)="ST"
		case left(field1,3)="BEG"
			xlevel="O"
			xaction=alltrim(substr(field1,at("*",field1,1)+1,at("*",field1,2)-(at("*",field1,1)+1)))
			**we expect only a 00 or 05 (new or replace)
			xaction=iif(inlist(xaction,"00","05"),"PLC","")
			if empty(xaction)
				xfrom ="TGFSYSTEM"
				xmessage = "Error in file '"+xfilename+"' for PO '"+xponum+"': Order status has unexpected value - expect only '00' or '05'. Entire file cannot be processed."
				do form dartmail2 With "mwinter@fmiint.com",xFrom,"PVH 850 Translation Error"," ","",xmessage,"A"

				xerror=.t.
				exit
			endif

			xponum=substr(field1,at("*",field1,3)+1,at("*",field1,4)-(at("*",field1,3)+1))
			xorderdt=right(alltrim(field1),len(alltrim(field1))-at("*",field1,5))
			xorderdt=left(xorderdt,4)+"-"+substr(xorderdt,5,2)+"-"+substr(xorderdt,7,2)
			xshipvia="SEA" &&hardcode SEA bc not provided
			**reset for every po change - mvw 02/14/12
			xprevlineno=""
			xpostr=""

			xhtserror=.f.

		case left(field1,3)="DTM" and substr(field1,5,3)="002"
			**had an issue where the segment was sent without a value ("DTM*002"), use po date if this occurs - mvw 10/23/14
			if occurs("*",field1)>1
				xexwreqdt=right(alltrim(field1),len(alltrim(field1))-at("*",field1,2))
				xexwreqdt=left(xexwreqdt,4)+"-"+substr(xexwreqdt,5,2)+"-"+substr(xexwreqdt,7,2)
			else
				xexwreqdt=xorderdt
			endif

		case left(field1,2)="N1" and substr(field1,4,2)="SE"
			xsuppliercode=right(alltrim(field1),len(alltrim(field1))-at("*",field1,4))
			xsuppliername=alltrim(substr(field1,at("*",field1,2)+1,at("*",field1,3)-(at("*",field1,2)+1)))

			skip
			store "" to xsupplieraddr1, xsupplieraddr2
			if left(field1,2)="N3" &&should have N3 but if not, leave blank
				if occurs("*",field1)=1
					xsupplieraddr1=right(alltrim(field1),len(alltrim(field1))-at("*",field1,1))
				else
					xsupplieraddr1=alltrim(substr(field1,at("*",field1,1)+1,at("*",field1,2)-(at("*",field1,1)+1)))
					xsupplieraddr2=right(alltrim(field1),len(alltrim(field1))-at("*",field1,2))
				endif
				skip
			endif

			if left(field1,2)="N3" &&if 2nd N3, skip past
				skip
			endif

			store "" to xsuppliercity, xsupplierstate, xsupplierzip, xsuppliercountry
			if left(field1,2)#"N4"
				skip -1 &&if for some reason not provided, skip back bc there is a skip at end of loop
			else
				xsuppliercity=alltrim(substr(field1,at("*",field1,1)+1,at("*",field1,2)-(at("*",field1,1)+1)))
				**if N4 comes only with city, need to use the right() function below - applies to other fields below as well
				if occurs("*",field1)=1
					xsuppliercity=right(alltrim(field1),len(alltrim(field1))-at("*",field1,1))
				endif
				
				xsupplierstate=alltrim(substr(field1,at("*",field1,2)+1,at("*",field1,3)-(at("*",field1,2)+1)))
				if occurs("*",field1)=2
					xsupplierstate=right(alltrim(field1),len(alltrim(field1))-at("*",field1,2))
				endif

				xsupplierzip=left(alltrim(substr(field1,at("*",field1,3)+1,at("*",field1,4)-(at("*",field1,3)+1))),5)
				if occurs("*",field1)=3
					xsupplierzip=left(right(alltrim(field1),len(alltrim(field1))-at("*",field1,3)),5)
				endif

				**if doesnt contain 4 *'s, then country is not provided. if so, default to CHINA as per Chris Lind - 11/29/11 mvw
				xsuppliercountry=iif(occurs("*",field1)<4,"CN",right(alltrim(field1),len(alltrim(field1))-at("*",field1,4)))
				**change to 3-digit iso country (unloccode.isocountry) - mvw 12/07/11
				=seek(xsuppliercountry,"unloccode","country")
				xsuppliercountry=unloccode.isocountry
			endif

		case left(field1,2)="N1" and substr(field1,4,2)="ST"
			xshiptocode=right(alltrim(field1),len(alltrim(field1))-at("*",field1,4))
			xshiptoname=alltrim(substr(field1,at("*",field1,2)+1,at("*",field1,3)-(at("*",field1,2)+1)))

			skip
			store "" to xshiptoaddr1, xshiptoaddr2
			if left(field1,2)="N3" &&should have N3 but if not, leave blank
				if occurs("*",field1)=1
					xshiptoaddr1=right(alltrim(field1),len(alltrim(field1))-at("*",field1,1))
				else
					xshiptoaddr1=alltrim(substr(field1,at("*",field1,1)+1,at("*",field1,2)-(at("*",field1,1)+1)))
					xshiptoaddr2=right(alltrim(field1),len(alltrim(field1))-at("*",field1,2))
				endif
				skip
			endif

			if left(field1,2)="N3" &&if 2nd N3, skip past
				skip
			endif

			store "" to xshiptocity, xshiptostate, xshiptozip, xshiptocountry
			if left(field1,2)#"N4"
				skip -1 &&if for some reason not provided, skip back bc there is a skip at end of loop
			else
				xshiptocity=alltrim(substr(field1,at("*",field1,1)+1,at("*",field1,2)-(at("*",field1,1)+1)))
				**if N4 comes only with city, need to use the right() function below - applies to other fields below as well
				if occurs("*",field1)=1
					xshiptocity=right(alltrim(field1),len(alltrim(field1))-at("*",field1,1))
				endif
				
				xshiptostate=alltrim(substr(field1,at("*",field1,2)+1,at("*",field1,3)-(at("*",field1,2)+1)))
				if occurs("*",field1)=2
					xshiptostate=right(alltrim(field1),len(alltrim(field1))-at("*",field1,2))
				endif

				xshiptozip=left(alltrim(substr(field1,at("*",field1,3)+1,at("*",field1,4)-(at("*",field1,3)+1))),5)
				if occurs("*",field1)=3
					xshiptozip=left(right(alltrim(field1),len(alltrim(field1))-at("*",field1,3)),5)
				endif

				**if doesnt contain 4 *'s, then country is not provided. if so default to US - mvw 11/29/11
				xshiptocountry=iif(occurs("*",field1)<4,"US",right(alltrim(field1),len(alltrim(field1))-at("*",field1,4)))
				**change to 3-digit iso country (unloccode.isocountry) - mvw 12/07/11
				=seek(xshiptocountry,"unloccode","country")
				xshiptocountry=unloccode.isocountry
			endif

		case left(field1,2)="N1" and substr(field1,4,2)="OT"
			xfobloccode=right(alltrim(field1),len(alltrim(field1))-at("*",field1,4))


		**start item level
		case left(field1,3)="PO1"
			xhdrstr=alltrim(xponum)+"|"+; &&A
				"|"+; &&column(s) B empty
				xaction+"|"+; &&C - changed from iif(xaction="C","CAN","PLC") as CLind says only change/place from guess ("C"=change, not cancel)
				xtollcustcode+"|"+;&&D
				xsuppliercode+"|"+; &&E
				xsuppliername+"|"+; &&F
				xsupplieraddr1+"|"+; &&G
				xsupplieraddr2+"|"+; &&H
				xsuppliercity+"|"+; &&I
				xsupplierstate+"|"+; &&J
				xsuppliercountry+"|"+; &&K
				xsupplierzip+"|"+; &&L
				"||"+; &&column(s) M-N empty
				xexwreqdt+"|"+; &&O
				"||"+; &&column(s) P-Q empty
				xorderdt+"|"+; &&R
				"|"+; &&column(s) S empty
				"USD"+"|"+; &&T
				"|"+; &&column(s) U empty
				"FOB|"+; &&V
				"|"+; &&column(s) W empty
				xshipvia+"|"+; &&X
				"||"+; &&column(s) Y-Z is empty
				"||"+; &&column(s) AA-AB is empty
				xexwreqdt+"|"+; &&AC
				"|||||||||"+; &&column(s) AD-AL empty
				xfobloccode+"|"+; &&AM
				xdelivport+"|"+; &&AN
				"|||||||||||||||||||||" &&column(s) AO-BI empty

			xlevel="I"
			xlineno=alltrim(substr(field1,at("*",field1,1)+1,at("*",field1,2)-(at("*",field1,1)+1)))

			**if duplicate line number for a given po, need to correct - mvw 02/14/12
			if xprevlineno==xlineno
*!*					messagebox("Duplicate line number ("+xlineno+") for po "+xponum+"!!",16,"Error")
*!*					set step on
				xslncnt=xslncnt+1
			else
				xslncnt=1
			endif

			xprevlineno=xlineno

			xstyle=alltrim(substr(field1,at("*",field1,7)+1,at("*",field1,8)-(at("*",field1,7)+1)))
			xqty=alltrim(substr(field1,at("*",field1,2)+1,at("*",field1,3)-(at("*",field1,2)+1)))
			xppk=iif(alltrim(substr(field1,at("*",field1,11)+1,at("*",field1,12)-(at("*",field1,11)+1)))="PPK",.t.,.f.)
			xupc=alltrim(substr(field1,at("*",field1,13)+1,at("*",field1,14)-(at("*",field1,13)+1)))

			if occurs("*",field1)=17
				xorigincountry=right(alltrim(field1),len(alltrim(field1))-at("*",field1,17))
			else
				xorigincountry=alltrim(substr(field1,at("*",field1,2)+1,at("*",field1,3)-(at("*",field1,2)+1)))
			endif

			**change to 3-digit iso country (unloccode.isocountry) - mvw 12/07/11
			**bc this now goes into edi enterprise as opposed to icon, send only 2-digit country code per Bill L - mvw 10/30/13
*!*				=seek(xorigincountry,"unloccode","country")
*!*				xorigincountry=unloccode.isocountry

			xunitprice="0.0000"
			**in case DTM*ZZZ segment is not sent, default to xreqdt to Date()+30 - mvw 04/26/13
			xreqdt=date()+30
			xreqdt=transform(year(xreqdt))+"-"+padl(transform(month(xreqdt)),2,"0")+"-"+padl(transform(day(xreqdt)),2,"0")

		case left(field1,3)="PID"
			if occurs("*",field1)>5
				xdescription=alltrim(substr(field1,at("*",field1,5)+1,at("*",field1,6)-(at("*",field1,5)+1)))
			else
				xdescription=right(alltrim(field1),len(alltrim(field1))-at("*",field1,5))
			endif

		case left(field1,3)="DTM" and substr(field1,5,3)="ZZZ"
			xreqdt=right(alltrim(field1),len(alltrim(field1))-at("*",field1,2))
			xreqdt=left(xreqdt,4)+"-"+substr(xreqdt,5,2)+"-"+substr(xreqdt,7,2)

		case left(field1,3)="SLN"
*			xhtserror=.f.

			**do while loop for multiple sln segs per po line
			**xslncnt reset now done at PO1 seg in case of need to combine identical line numbers for po - mvw 02/14/12
			*xslncnt=1
			do while .t.
				**only add 1 record for ppk, do not want to show the styles within the ppk - mvw 03/29/12
				if xppk and xslncnt>1
					**skip past the rest of the sln segs
					count while left(field1,3)="SLN"
					exit
				else
					**do not pass color/size for ppk's - mvw 03/29/12
					store "" to xpartattrib1, xpartattrib2
					if !xppk
						xpartattrib1=alltrim(substr(field1,at("*",field1,12)+1,at("*",field1,13)-(at("*",field1,12)+1)))
						xpartattrib2=alltrim(substr(field1,at("*",field1,14)+1,at("*",field1,15)-(at("*",field1,14)+1)))
					endif

					**predefine xpartattrib4-xpartattrib7, there are only 4 place hoilders for HTS numbers
					store "" to xpartattrib4, xpartattrib5, xpartattrib6, xpartattrib7

					skip
					if left(field1,3)="TC2"
						**need to allow for multiple HTS numbers (T2 segs) per line - mvw 02/10/12
	*					xpartattrib3=right(alltrim(field1),len(alltrim(field1))-at("*",field1,2))
						xcnt=4
						do while left(field1,3)="TC2"
							xvar="xpartattrib"+transform(xcnt)
							&xvar=right(alltrim(field1),len(alltrim(field1))-at("*",field1,2))

							**if HTS="999", send email below - mvw 10/06/14
							if &xvar="999"
								xhtserror=.t.
							endif

							xcnt=xcnt+1
							skip
						enddo
						skip -1
					else
						**no longer error out if no TC2 segemnt (hts number) as per CLIND, send blank - mvw 12/007/11
						skip -1

	*!*						xfrom ="TGFSYSTEM"
	*!*						xmessage = "Error in file '"+xfilename+"' for PO '"+xponum+"': SLN Segment found without an associated TC2 segment. Entire file cannot be processed."
	*!*						do form dartmail2 With "mwinter@fmiint.com",xFrom,"PVH 850 Translation Error"," ","",xmessage,"A"

	*!*						xerror=.t.
	*!*						exit
					endif

	*				xpartattrib3=right(alltrim(field1),len(alltrim(field1))-at("*",field1,2))

					**do not send NON-TOLL pos - for now just certain FOB's are loaded - 10/25/13
					**added CNSZX - mvw 11/27/13
					**changed to send all pos out of CHINA - mvw 12/17/13
					**added Hong Kong (HK) per Lorraine - mvw 10/15/14
*					if inlist(xfobloccode,"CNDLC","CNNGB","CNSHA","CNTAO","CNTSN","CNXMN","CNYTN","CNSZX")
					if inlist(xfobloccode,"CN","HK") or inlist(xponum,"RTTO990027","RTTC990027")
						**removed the "-" from the lineno as ICON could not accept it. Now line number "001" will go as "0011" 
						**  for the 1st SLN, "0012" for the 2nd and so on. The right-most character will need to be stripped 
						**  for the 856 going back to PVH - mvw 01/12/12
						*xlineno+"-"+transform(xslncnt)+"|"+ &&BJ

						**update xpostr not xfilestr in case dont want to send this po bc of error or any other reason - mvw 10/25/13
						xpostr=xpostr+xhdrstr+;
							xlineno+padl(transform(xslncnt),2,"0")+"|"+; &&BJ
							"||"+; &&column(s) BK-BL empty
							xstyle+"|"+; &&BM
							xdescription+"|"+; &&BN
							transform(xqty)+"|"+; &&BO
							"|||"+; &&column(s) BP-BR empty
							transform(xunitprice)+"|"+; &&BS
							"|"+; &&column(s) BT empty
							xaction+"|"+; &&BU
							xreqdt+"|"+; &&BV
							xpartattrib1+"|"+; &&BW
							xpartattrib2+"|"+; &&BX
							"|"+; &&BY - empty, had held the 1st HTS # - mvw 02/10/12
							xupc+"|"+; &&BZ
							xpartattrib4+"|"+; &&CA
							xpartattrib5+"|"+; &&CB
							xpartattrib6+"|"+; &&CC
							xpartattrib7+"|"+; &&CD
							"||||||||||||||||||"+; &&column(s) CE-CV empty
							xorigincountry+"|"+; &&CW
							"||||||||"+;&&column(s) CX-DE empty
							xdelivport+"|"+; &&DF
							xshiptocode+"|"+; &&DG
							xshiptoname+"|"+; &&DH
							xshiptoaddr1+"|"+; &&DI
							xshiptoaddr2+"|"+; &&DJ
							xshiptocity+"|"+; &&DK
							xshiptostate+"|"+; &&DL
							xshiptocountry+"|"+; &&DM
							xshiptozip+; &&DN
							chr(13)+chr(10)
					endif

					skip

					if left(field1,3)#"SLN"
						exit
					endif

					xslncnt=xslncnt+1
				endif
			enddo

			if xerror
				exit
			endif

*!*				**send an email if PO sent with HTS="999" - mvw 10/06/14
*!*				if xhtserror
*!*					xfrom="TOLLSYSTEM"
*!*					xsubject="Req Ex-Works "+xexwreqdt+", Style "+xstyle+", "+alltrim(xponum)
*!*					xmessage="Req Ex-Works: "+xexwreqdt+chr(13)+chr(10)+"Style: "+xstyle+chr(13)+chr(10)+"PO: "+alltrim(xponum)
*!*					do form dartmail2 With "mwinter@fmiint.com",xFrom,xsubject," ","",xmessage,"A"
*!*				endif

			skip -1

		case left(field1,3)="MEA" and substr(field1,5,2)="CT" &&remove MEA pack count, comes into lognet as carton cnt - mvw 11/05/09
		case left(field1,3)="CTT"
		case left(field1,3)="SE"
			**add xpostr to xfilestr once a po is complete in case of errors (line dups, etc) within a po - mvw 10/25/13
			xfilestr=xfilestr+xpostr

			**send an email if PO sent with HTS="999" - mvw 10/06/14
			**removed until Ryan gets back to me on how this is to be sent. If sent as is, sends too many emails - mvw 10/15/14
*!*				if xhtserror
*!*					xfrom="TOLLSYSTEM"
*!*					xsubject="Req Ex-Works "+xexwreqdt+", Style "+xstyle+", "+alltrim(xponum)
*!*					xmessage="Req Ex-Works: "+xexwreqdt+chr(13)+chr(10)+"Style: "+xstyle+chr(13)+chr(10)+"PO: "+alltrim(xponum)
*!*					do form dartmail2 With "mwinter@fmiint.com",xFrom,xsubject," ","",xmessage,"A"
*!*				endif

		endcase

		select x850
		skip
	enddo

	select x850
	locate

	**if xfilestr only contains header info, something went wrong! - mvw 07/05/12
	**it may now be that all the pos in this file were NON-TOLL pos, which we do not send... dont treat as error, just delete file and move on - mvw 10/25/13
*!*		if len(xfilestr)=xhdrlen
*!*			xerror=.t.
*!*		endif

	if !xerror
		**if pos from countries we do business with exist (ie, Bangladesh (list of countries above)), send file... else just delete the file - mvw 10/03/12
		if len(xfilestr)>xhdrlen
			do while .t.
				set date ymd
				xoutfile="PO_TGFDI_"+strtran(strtran(strtran(ttoc(datetime()),":","")," ",""),"/")+xtollcustcode+".txt"
				set date american

				xarchiveoutfile=xoutdir+"archive\"+xoutfile
				if !file(xarchiveoutfile)
					exit
				endif
			enddo

			xoutfile=xoutdir+xoutfile
			strtofile(xfilestr,xoutfile)
			strtofile(xfilestr,xarchiveoutfile)

			xfilecreated=.t.
		endif

		copy file &xfilename to &xarchiveinfile
		delete file &xfilename
	endif
endscan

if xfilecreated
	**create an ftpjob
	use f:\edirouting\ftpjobs in 0
	insert into ftpjobs (jobname, userid, jobtime) values ("PVH-850-TO-ICON","PVH850",datetime())
	use in ftpjobs
endif


use in x850
use in xfilelist
use in unloccode
