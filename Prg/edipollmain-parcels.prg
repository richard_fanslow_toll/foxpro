*runack("EDIPOLLER_PARCELS	")

* Initially set up only for Merkury parcels, may add the others in at some point.
* This is currently TBA, not active.

CLOSE DATABASES ALL
CLEAR ALL
RELEASE ALL
cPath = ["M:\DEV\PRG; M:\DEV\PROJ"]
SET PATH TO &cPath ADDITIVE

WITH _SCREEN
	.AUTOCENTER = .T.
	.WINDOWSTATE = 0
	.BORDERSTYLE = 1
	.WIDTH = 260
	.HEIGHT = 130
	.TOP = 850
	.LEFT = 940
	.CLOSABLE = .F.
	.MAXBUTTON = .F.
	.MINBUTTON = .F.
	.SCROLLBARS = 0
	.CAPTION = "PARCELS POLLER - EDI"
ENDWITH
TRY
	SET STEP ON
	DO m:\dev\prg\_setvars
	PUBLIC lSkipError
	lSkipError =  .F.
	SET STATUS BAR OFF
	SET SYSMENU OFF
	ON ESCAPE CANCEL
	ON ERROR THROW
	SELECT 0
	USE F:\3pl\DATA\edipollertrip SHARED ALIAS eparcels
	REPLACE emerk.parcels WITH .F.
	USE IN eparcels

	SELECT 0
	USE F:\3pl\DATA\last945time SHARED ALIAS parcelslast
	REPLACE merklast.checkflag WITH .T. FOR poller = "PARCELS" IN parcelslast
	REPLACE merklast.tries WITH 0 FOR poller = "PARCELS" IN parcelslast
	USE IN merklast
	ASSERT .F. MESSAGE "Going into poller run"
	DO FORM m:\dev\frm\edi_jobhandler_parcels

CATCH TO oErr
	ASSERT .F. MESSAGE "AT CATCH SECTION"
	IF !lSkipError
		tfrom    ="TGF EDI Processing Center <tgf-edi-ops@fmiint.com>"
		tsubject = "PARCELS Outbound EDI Poller Error ("+TRANSFORM(oErr.ERRORNO)+") at "+TTOC(DATETIME())
		tattach  = ""
		SELECT 0
		USE F:\3pl\DATA\mailmaster ALIAS mm
		LOCATE
		LOCATE FOR mm.edi_type = 'MISC' AND mm.taskname = "GENERAL"
		tsendto = IIF(mm.use_alt,sendtoalt,sendto)
		tcc = IIF(mm.use_alt,ccalt,cc)
		USE IN mm

		tmessage = "PARCELS 945 Poller Major Error..... Please fix me........!"
		lcSourceMachine = SYS(0)

		tmessage =tmessage+ "Try/CATCH Exeception Message:"+CHR(13)+;
			[  Error: ] + STR(oErr.ERRORNO) +CHR(13)+;
			[  LineNo: ] + STR(oErr.LINENO) +CHR(13)+;
			[  Message: ] + oErr.MESSAGE +CHR(13)+;
			[  Procedure: ] + oErr.PROCEDURE +CHR(13)+;
			[  Details: ] + oErr.DETAILS +CHR(13)+;
			[  StackLevel: ] + STR(oErr.STACKLEVEL) +CHR(13)+;
			[  LineContents: ] + oErr.LINECONTENTS+CHR(13)+;
			[  UserValue: ] + oErr.USERVALUE+CHR(13)+;
			[  Computer:  ] +lcSourceMachine+CHR(13)
		DO FORM m:\dev\FRM\dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
	ENDIF
	SELECT 0
	USE F:\3pl\DATA\edipollertrip SHARED ALIAS eparcels
	REPLACE eparcels.parcels WITH .T. IN eparcels
	CLOSE DATABASES ALL
FINALLY
	SET STATUS BAR ON
	ON ERROR
ENDTRY
