Parameters ptnum,dc

Do Case

Case dc == "830"

    Replace All shipto With "KOHL'S DC #00830" For pt=ptnum
    Replace All staddr1 With "WINCHESTER DC" For pt=ptnum
    Replace All shipto With "KOHL'S DC #00830" For pt=ptnum
    Replace All staddr1 With "WINCHESTER DC" For pt=ptnum
    Replace All staddr2 With "300 ADMIRAL BYRD DRIVE" For pt=ptnum
    Replace All STCSZ With "WINCHESTER VA, 22602" For pt=ptnum
    Replace All STORE_NUM With "830" For pt=ptnum
    Replace All DC_NUM With "00830" For pt=ptnum
    Wait window at 10,10 "DC 830 update for pt: "+ptnum 
    
Case dc == "865"
    Replace All shipto With "KOHL'S DC #00865" For pt=ptnum
    Replace All staddr1 With "ACTIVE DIST CENTER 865" For pt=ptnum
    Replace All staddr2 With "3440 STATE ROUTE 209" For pt=ptnum
    Replace All STCSZ With "WURTSBORO, NY 12790" For pt=ptnum
    Replace All STORE_NUM With "865" For pt=ptnum
    Replace All DC_NUM With "00865" For pt=ptnum
    Replace All ZIPBAR With "12790" For pt=ptnum
    Wait window at 10,10 "DC 865 update for pt: "+ptnum 

Case dc == "840"
    Replace All shipto With "KOHL'S DC #00840" For pt=ptnum
    Replace All staddr1 With "ACTIVE DIST CENTER 840" For pt=ptnum
    Replace All staddr2 With "2015 NE JEFFERSON STREET" For pt=ptnum
    Replace All STCSZ With "GRAIN VALLEY, MO 64029" For pt=ptnum
    Replace All STORE_NUM With "840" For pt=ptnum
    Replace All DC_NUM With "00840" For pt=ptnum
    Replace All ZIPBAR With "64029" For pt=ptnum
    Wait window at 10,10 "DC 840 update for pt: "+ptnum 

Case dc == "875"
    Replace All shipto With "KOHL'S DC #00875" For pt=ptnum
    Replace All staddr1 With "MACON DIST CENTER" For pt=ptnum
    Replace All staddr2 With "3030 AIRPORT ROAD EAST" For pt=ptnum
    Replace All STCSZ With "MACON, GA 31216" For pt=ptnum
    Replace All STORE_NUM With "875" For pt=ptnum
    Replace All DC_NUM With "00875" For pt=ptnum
    Replace All ZIPBAR With "31216" For pt=ptnum
    Wait window at 10,10 "DC 875 update for pt: "+ptnum 

Case dc == "890"
    Replace All shipto With "KOHL'S DC #00890" for pt=ptnum
    Replace All staddr1 With "KOHLS DIST CENTER 890" For pt=ptnum
    Replace All staddr2 With "4301 KOHLS DRIVE" For pt=ptnum
    Replace All STCSZ With "OTTAWA, IL 61350" For pt=ptnum
    Replace All STORE_NUM With "890" For pt=ptnum
    Replace All DC_NUM With "00890" For pt=ptnum
    Replace All ZIPBAR With "61350" For pt=ptnum
    Wait window at 10,10 "DC 890 update for pt: "+ptnum 

Case dc = "810"
    Replace All shipto With "KOHL'S DC #00810" For pt=ptnum
    Replace All staddr1 With "KOHLS DIST CENTER 810" For pt=ptnum
    Replace All staddr2 With "7855 COUNTRY ROAD 140" For pt=ptnum
    Replace All STCSZ With "FINDLAY, OH 45840" For pt=ptnum
    Replace All STORE_NUM With "810" For pt=ptnum
    Replace All DC_NUM With "00810" For pt=ptnum
    Replace All ZIPBAR With "45840" For pt=ptnum
    Wait window at 10,10 "DC 810 update for pt: "+ptnum 

Case dc == "85"
    Replace All shipto With "KOHL'S DC #0085" For pt=ptnum
    Replace All staddr1 With "KOHLS DIST CENTER 085" For pt=ptnum
    Replace All staddr2 With "NORTH 54 W 13901 WOODALE DR" For pt=ptnum
    Replace All STCSZ With "MENOMNEE FALLS, WI 53051, VA 22602" For pt=ptnum
    Replace All STORE_NUM With "85" For pt=ptnum
    Replace All DC_NUM With "00085" For pt=ptnum
    Replace All ZIPBAR With "53051" For pt=ptnum
    Wait window at 10,10 "DC 85 update for pt: "+ptnum 

Case dc == "855"
    Replace All shipto With "KOHL'S DC #0855-SB" For pt=ptnum
    Replace All staddr1 With "KOHLS DIST CENTER 855" For pt=ptnum
    Replace All staddr2 With "890 EAST MILL ST" For pt=ptnum
    Replace All STCSZ With "SAN BERNADINO, CA 92408" For pt=ptnum
    Replace All STORE_NUM With "855" For pt=ptnum
    Replace All DC_NUM With "00855" For pt=ptnum
    Replace All ZIPBAR With "92408" For pt=ptnum
    Wait window at 10,10 "DC 855 update for pt: "+ptnum 

Case dc == "860"
    Replace All shipto With "KOHL'S DC #00860" For pt=ptnum
    Replace All staddr1 With "KOHLS DIST CENTER 860" For pt=ptnum
    Replace All staddr2 With "1600 NORTH BUSINESS 45" For pt=ptnum
    Replace All STCSZ With "CORSICANA, TX 75110" For pt=ptnum
    Replace All STORE_NUM With "860" For pt=ptnum
    Replace All DC_NUM With "00860" For pt=ptnum
    Replace All ZIPBAR With "75110" For pt=ptnum
    Wait window at 10,10 "DC 860 update for pt: "+ptnum 
    
Otherwise
    Wait window at 10,10 "No update  for pt: "+ptnum 
    
Endcase
