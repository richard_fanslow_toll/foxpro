utilsetup("BCNY_RCV_REPORT")

set talk off

set safety off
close data all
set exclusive off
set enginebehavior 70
set deleted on
_screen.WindowState = 1

goffice='2'
useca("inwolog","wh",,,"mod='"+goffice+"' and  accountid in (6521,6221) and axdt ={}")

if reccount("inwolog")>0
	xjfilter="inwologid in ("
	select inwolog
	scan
		xjfilter=xjfilter+transform(inwologid)+","
	endscan
	xjfilter=left(xjfilter,len(xjfilter)-1)+")"
else
	xjfilter="1=0"
endif

xsqlexec("select * from indet where "+xjfilter,,,"wh")

upcmastsql(,,,,,"inlist(accountid,6221,6521)")
index on style tag style
index on upc tag upc

select inwologid,accountid,container,wo_num,acctname, availdt as rcv_date from inwolog where accountid=6521 and ;
	!inwolog.ppadj and !cyclecount and !returntostock and !physinv  and ;
	!emptynul(availdt) and emptynul(axdt) into cursor temp44 readwrite

select inwolog
**replace  axdt with datetime() for accountid=6521 and !inwolog.ppadj and !cyclecount and !returntostock and !physinv  and !emptynul(availdt) and emptynul(axdt)
select temp44
scan
	select inwolog
	set step on
	locate for inwologid=temp44.inwologid
	if found("inwolog")
		replace axdt with datetime()  for inwologid=temp44.inwologid  in inwolog
	endif   
ENDSCAN

tu('inwolog')

select a.*, style,color,id,totqty, cayset as po, po as tollpo ,  pack from temp44 a, indet b where a.inwologid=b.inwologid and units into cursor temp45 readwrite

select b.* from temp44 a, indet b where a.inwologid=b.inwologid and !units  into cursor temp54 readwrite

select a.*,b.pack from temp45 a left join temp54 b on alltrim(a.style)=alltrim(b.style) and alltrim(a.color)=alltrim(b.color);
	and alltrim(a.id)=alltrim(b.id) and alltrim(a.tollpo)=alltrim(b.po) and a.inwologid=b.inwologid into cursor temp77 readwrite

select a.*,upc,descrip from temp77 a left join upcmast b on a.style=b.style and a.color=b.color and a.id=b.id and a.accountid=b.accountid into cursor temp46 readwrite

replace all acctname with 'SYNCLAIRE BRANDS'
replace upc with style for isnull(upc)

select a.*,b.descrip as descrip2 from temp46 a left join upcmast b on a.upc=b.upc and inlist(b.accountid,6521,6221) and isnull(a.descrip) into cursor temp47 readwrite
replace descrip with descrip2 for isnull(descrip)

select accountid,container,wo_num,acctname,style,color,id,totqty,upc,descrip,po,rcv_date,pack_b as pack from temp47 into cursor temp48 readwrite
replace pack with '1' for isnull(pack)

select temp48
if reccount() > 0
	copy to "F:\FTPUSERS\Synclaire\Receiving\receiving_"+ttoc(datetime(),1) delimited with " " with character tab
*EXPORT TO F:\FTPUSERS\Synclaire\Receiving\receiving xls
else
endif
select inwologid,accountid,container,wo_num,acctname, availdt as rcv_date from inwolog where accountid=6221 and ;
	!inwolog.ppadj and !cyclecount and !returntostock and !physinv  and ;
	!emptynul(availdt) and emptynul(axdt) into cursor temp44 readwrite
select inwolog
*replace  axdt with datetime() for accountid=6221 and !inwolog.ppadj and !cyclecount and !returntostock and !physinv  and !emptynul(availdt) and emptynul(axdt)
select temp44
scan
	select inwolog
	set step on
	locate for inwologid=temp44.inwologid
	if found("inwolog")
		replace axdt with datetime()  for inwologid=temp44.inwologid  in inwolog
	endif   
ENDSCAN

tu('inwolog')

select a.*, style,color,id,totqty, cayset as po, po as tollpo ,  pack from temp44 a, indet b where a.inwologid=b.inwologid and units into cursor temp45 readwrite
select b.* from temp44 a, indet b where a.inwologid=b.inwologid and !units  into cursor temp54 readwrite

select a.*,b.pack from temp45 a left join temp54 b on alltrim(a.style)=alltrim(b.style) and alltrim(a.color)=alltrim(b.color);
	and alltrim(a.id)=alltrim(b.id) and alltrim(a.tollpo)=alltrim(b.po) and a.inwologid=b.inwologid into cursor temp77 readwrite

select a.*,upc,descrip from temp77 a left join upcmast b on a.style=b.style and a.color=b.color and a.id=b.id and a.accountid=b.accountid into cursor temp46 readwrite

replace upc with style for isnull(upc)
select a.*,b.descrip as descrip2 from temp46 a left join upcmast b on a.upc=b.upc and inlist(b.accountid,6521,6221) and isnull(a.descrip) into cursor temp47 readwrite
replace descrip with descrip2 for isnull(descrip)
select accountid,container,wo_num,acctname,style,color,id,totqty,upc,descrip,po,rcv_date, pack_b as pack from temp47 into cursor temp48 readwrite

replace pack with '1' for isnull(pack)
select temp48
if reccount() > 0
	copy to "F:\FTPUSERS\bcny\Receiving\receiving_"+ttoc(datetime(),1) delimited with " " with character tab
*EXPORT TO  F:\FTPUSERS\bcny\Receiving\receiving xls
else
endif
close data all


schedupdate()
_screen.caption=gscreencaption
on error
