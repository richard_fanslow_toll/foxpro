parameters lvisitor, lwebsite, linrcv, xuadet, xstrippedonly, xfromdt, xtodt, xautoemail, xuafilter
**xuafilter - trveor asked to have the wos remain on for 5 days after stripped - mvw 01/02/13

if !linrcv
	use f:\wo\wodata\wolog in 0
	use f:\wo\wodata\detail in 0
	use f:\wo\wodata\daily in 0
	use f:\wo\wodata\tdaily in 0
endif

select accountid,acctname,wo_num,wo_date,space(2) as office,type,loose,vessel,awb,container,brokref,quantity,pickedup,returned,seal as importseal,eta,cust_ref, ;
space(15) as trailer, ;
0000000 as inv_wo, ;
000000 as totqty, ;
{} as called, ;
{} as dispatched, ;
{} as ftexpires, ;
{} as avail, ;
space(40) as comments ;
from wolog where .f. ;
into cursor temp_wolog readwrite

**define here in case want to run this report for another report date - mvw 04/18/13
*xreportdt={04/15/13}
xreportdt=date()

do case
case linrcv
	xstartdt = xreportdt-7
case xaccountid=5687
	xstartdt = xreportdt-5
otherwise
	xstartdt = xreportdt-7
endcase

if type("xacctfilter")="U"
	xacctfilter="accountid="+transform(xaccountid)
endif

select account
scan for &xacctfilter and !inactive
**copy record ALL things are TRUE: TYPE # 'D', OFFICE = goffice,
**			 - both PICKEDUP AND INV_NUM are empty OR PICKEDUP is within last 4 days
**              as of 1/1/02 need to check INVOICE.DBF b/c wolog not updated with invoice info
**			 - NOT (TYPE = 'O' AND !LOOSE AND NOT both MTINYARD AND RETURNED empty) - which means
**				we remove full ctrs that have a MTINYARD date or a RETURNED date or BOTH

	xusedinwolog=.f.

	**UA report now run for all offices, ignore inwolog - mvw 08/10/16
	if !xuadet and !used("inwolog")
		xusedinwolog=.t.

		xsaveoffice=goffice

		**Bc Carson 2, ML, etc have an xoffice of "C" for the trucking filter, need to pass gmasteroffice when available - mvw 03/07/17
		goffice=whbldg(iif(type("gmasteroffice")="C",gmasteroffice,xoffice),account.accountid,,.t.)

		goffice=xsaveoffice
	endif

	xofficefilter=iif(empty(xoffice),""," and office = xoffice ")
	xfilter="accountid=account.accountid and (type#'D' or domdeliv)"+xofficefilter

	do case
	case xuadet and xstrippedonly
		**for UA report, only remove once its stripped as opposed to when the ctr is pickedup/returned - mvw 11/22/10
		xfilter=xfilter+"and between(stripped,{"+transform(xfromdt)+"},{"+transform(xtodt)+"})"
	case xuadet and xuafilter
		**for UA report, only remove once its stripped as opposed to when the ctr is pickedup/returned - mvw 11/22/10
		**xuafilter - trveor asked to have the wos remain on for 5 days after stripped - mvw 01/02/13
		**changed to drop full ctrs 5 days after retunred - mvw 02/27/13
*!*			xfilter=xfilter+"and (((empty(pickedup) and (empty(invnum) or (!empty(invnum) and invdt>xstartdt))) or pickedup>=xstartdt) and "+;
*!*				"!(type='O' and !loose and !(emptynul(mtinyard) and emptynul(returned))) or ((emptynul(stripped) or stripped>=xreportdt-5) and (empty(invnum) or (!empty(invnum) and invdt>xstartdt))))"
		xfilter=xfilter+"and (((emptynul(pickedup) and (empty(invnum) or (!empty(invnum) and invdt>xstartdt))) or pickedup>=xstartdt) and "+;
		"!(type='O' and !loose and ((!emptynul(mtinyard) and mtinyard<xreportdt-5) or (!emptynul(returned) and returned<xreportdt-5))) or "+;
		"((emptynul(stripped) or stripped>=xreportdt-5) and (empty(invnum) or (!empty(invnum) and invdt>xstartdt))))"
	case xuadet
		**for UA report, only remove once its stripped as opposed to when the ctr is pickedup/returned - mvw 11/22/10
		xfilter=xfilter+"and (((emptynul(pickedup) and (empty(invnum) or (!empty(invnum) and invdt>xstartdt))) or pickedup>=xstartdt) and "+;
		"!(type='O' and !loose and !(emptynul(mtinyard) and emptynul(returned))) or (emptynul(stripped) and (empty(invnum) or (!empty(invnum) and invdt>xstartdt))))"
	otherwise
		xfilter=xfilter+"and ((emptynul(pickedup) and (empty(invnum) or (!empty(invnum) and invdt>xstartdt))) or pickedup>=xstartdt) "+;
		"and !(type='O' and !loose and !(emptynul(mtinyard) and emptynul(returned)))"
	endcase

**needed to specify the fields needed as trailer c(8) was added to wolog, need trailer c(15) - mvw 04/01/15
*!*		select *, ;
*!*			space(15) as trailer, ;
*!*			0000000 as inv_wo, ;
*!*			000000 as totqty, ;
*!*			{} as called, ;
*!*			{} as dispatched, ;
*!*			{} as ftexpires, ;
*!*			{} as avail, ;
*!*			space(40) as comments ;
*!*			from wolog ;
*!*			where &xfilter ;
*!*			into cursor xtemp readwrite

	select accountid,acctname,wo_num,wo_date,office,type,loose,vessel,awb,container,brokref,quantity,pickedup,returned,seal as importseal,eta,cust_ref,inwo, ;
	space(15) as trailer, ;
	0000000 as inv_wo, ;
	000000 as totqty, ;
	{} as called, ;
	{} as dispatched, ;
	{} as ftexpires, ;
	{} as avail, ;
	space(40) as comments ;
	from wolog ;
	where &xfilter ;
	into cursor xtemp readwrite

	**this is to allow for wos for Moret accts to be linked to all other moret accts
	**changed to use gmoretacctlist because list is ever-changing - mvw 03/24/15
	if moretaccount("account.accountid")
		select wolog
		scan for wo_date>xreportdt-60 and inlist(accountid,5442,5451,6401,5452,5453,5443,5444,5445) &xofficefilter ;
			and ((emptynul(pickedup) and (empty(invnum) or (!empty(invnum) and invdt>xstartdt))) or pickedup>=xstartdt) ;
			and !(type='O' and !loose and !(emptynul(mtinyard) and emptynul(returned)))

			if xusedinwolog 	&&not used for UA - mvw 08/10/16
				if xsqlexec("select * from inwolog where accountid="+transform(account.accountid)+" and container='"+wolog.container+"'",,,"wh") # 0
					select wolog
					scatter memvar
**must save with accountid from the wms, not the trucking wo - mvw 03/24/15
					m.accountid=account.accountid
					m.acctname=account.acctname
					insert into xtemp from memvar
				else
					select inwolog
					locate for accountid=account.accountid and container=wolog.container
					if found()
						select wolog
						scatter memvar
**must save with accountid from the wms, not the trucking wo - mvw 03/24/15
						m.accountid=account.accountid

						m.acctname=account.acctname
						insert into xtemp from memvar
					endif
				endif
			endif
		endscan
	endif

	select xtemp
	scan
		blank fields comments

		if !empty(xtemp.pickedup)
			replace dispatched with pickedup in xtemp
		else
			select daily
			locate for wo_num=xtemp.wo_num and !undel
			if found()
				replace dispatched with daily.dispatched in xtemp
			endif
		endif

		if xusedinwolog	 &&not used for UA - mvw 08/10/16
			do case
			case xtemp.type = 'D' or !empty(xtemp.inwo)
				xsqlexec("select * from inwolog where wo_num="+transform(xtemp.inwo),,,"wh")
			case xtemp.type = 'A'
				xsqlexec("select * from inwolog where brokerref='"+xtemp.brokref+"' and reference='"+xtemp.awb+"' and accountid="+transform(xtemp.accountid),,,"wh")
			otherwise
				xsqlexec("select * from inwolog where container='"+xtemp.container+"' and wo_date>{"+dtoc(xreportdt-30)+"} and accountid="+transform(xtemp.accountid),,,"wh")
			endcase

			if reccount("inwolog") # 0
				replace inv_wo with inwolog.wo_num in xtemp

				if inwolog.quantity+inwolog.unitsinqty>0
					replace totqty with inwolog.quantity+inwolog.unitsinqty in xtemp
				endif
			endif
		endif

		xsqlexec("select * from availlog where wo_num="+transform(xtemp.wo_num)+" order by availlogid desc","csravaillog",,"wo")
		
		if reccount()>0
			replace called with nul2empty(csravaillog.called), comments with csravaillog.remarks, ftexpires with nul2empty(csravaillog.ftexpires) in xtemp

**added for Trevor (last called updated at avail = avail) - mvw 01/17/13
			locate for remarks = "AVAILABLE"
			replace avail with nul2empty(csravaillog.called) in xtemp
		endif

		do case
		case empty(xtemp.called) and !empty(xtemp.dispatched)
			replace called with xtemp.dispatched, comments with 'AVAILABLE', avail with xtemp.dispatched in xtemp
		case xtemp.type="A" and empty(xtemp.comments)
			replace comments with 'AVAILABLE AIR FREIGHT' in xtemp
		endcase

		if xtemp.loose
			select daily
			locate for wo_num=xtemp.wo_num and action='PU' and !undel
			if found()
				xtrailer=iif(!empty(trailer),trailer,iif(!empty(container),container,dis_truck))
				replace trailer with xtrailer in xtemp
			else
				select tdaily
				locate for wo_num=xtemp.wo_num and action='PU' and !undel
				if found()
					xtrailer=iif(!empty(trailer),trailer,iif(!empty(container),container,dis_truck))
					replace trailer with xtrailer in xtemp
				endif
			endif
		endif
	endscan

	select xtemp
	scan
		scatter memvar
		m.office=iif(m.office="C","CA",iif(m.office="M","FL","NJ"))
		insert into temp_wolog from memvar
	endscan

	if used("inwolog")
		use in inwolog

		if xautoemail
			try
				set database to wh
				close databases
			catch
			endtry
		endif
	endif
endscan

if xuadet &&grab extra data from detail (ie qty per delloc, etc) - mvw 04/12/10
	select w.wo_num,w.office,w.loose,w.awb,w.container,w.brokref,w.trailer,w.inv_wo,w.comments,w.avail,d.stripped,w.dispatched,w.pickedup,w.ftexpires,w.returned,;
	sum(iif(inlist(delloc,"DH-10","DH-50"),d.cbm,0.00)) as cbm1010, sum(iif(inlist(delloc,"DH-11","DH-51","DH-58"),d.cbm,0.00)) as cbm1040, ;
	sum(iif(delloc="NJ FMI HOLD",d.cbm,0.00)) as cbmnjhold, sum(iif(delloc="RIALTO TLOAD",d.cbm,0.00)) as cbmrialtotl, ;
	sum(iif(delloc="FMIW PNP",d.cbm,0.00)) as cbmwestpnp, sum(iif(delloc="FMIR PNP",d.cbm,0.00)) as cbmrialtopnp, ;
	sum(iif(delloc="FMI ",cbm,0.00)) as cbmxd, ;
	sum(iif(inlist(delloc,"DH-10","DH-50"),pl_qty,0.00)) as plqty1010, sum(iif(inlist(delloc,"DH-11","DH-51","DH-58"),pl_qty,0.00)) as plqty1040, ;
	sum(iif(delloc="NJ FMI HOLD",pl_qty,0)) as plqtynjhold, sum(iif(delloc="RIALTO TLOAD",pl_qty,0)) as plqtyrialtotl, ;
	sum(iif(delloc="FMIW PNP",pl_qty,0)) as plqtywestpnp, sum(iif(delloc="FMIR PNP",pl_qty,0)) as plqtyrialtopnp, ;
	sum(iif(delloc="FMI ",pl_qty,0)) as plqtyxd, ;
	sum(iif(!isnull(d.wo_num),1,0)) as skucnt, sum(pl_qty) as plqty, ;
	sum(d.cbm) as totcbm, max(dept) as dept, d.eta ;
	from temp_wolog w left join detail d on d.wo_num=w.wo_num group by w.wo_num ;
	into cursor temp_wolog readwrite
endif


if lwebsite or xautoemail
	goffname="All Offices"
	if !empty(xoffice)
		=seek(xoffice,"offices","locale")
		goffname=upper(dispcsz(offices.city, offices.state))
	endif
endif

select temp_wolog
delete for wo_date<xreportdt-120
go top

**need to exclude UA rpt as acctname and type are not included in cursor - mvw 12/09/11
if !xuadet
	index on acctname+type tag intlrpt
endif

if lwebsite &&create .pdf file put into MEMO field of PDF_FILE.DBF
	loadwebpdf("wointlrpt")
endif

if !linrcv
	use in wolog
	use in detail
	use in daily
	use in tdaily
endif