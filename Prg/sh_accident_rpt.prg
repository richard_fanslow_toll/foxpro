* SH_ACCIDENT_RPT 12/01/2015 MB

* automated version of shaccidentrpt.prg

* build as F:\UTIL\SHOPREPORTS\SH_ACCIDENT_RPT.EXE

LOCAL lTestMode
lTestMode = .F.

utilsetup("SH_ACCIDENT_RPT")

DECLARE INTEGER CreateMutex IN Win32API ;
	STRING @lpMutexAttributes, ;
	STRING bInitialOwner, ;
	STRING @lpName

DECLARE INTEGER GetLastError IN Win32API

* if running in EXE check to see if already running
IF _VFP.STARTMODE > 0 THEN
	lcProcessName = "SH_ACCIDENT_RPT"
	CreateMutex(0,CHR(1),@lcProcessName)
	lnError = GetLastError()
	IF lnError = 183
		WAIT WINDOW TIMEOUT 5 "This program is already running..."
		IF NOT lTestMode THEN
			schedupdate()
		ENDIF
		RETURN .F.
	ENDIF
ENDIF

loSH_ACCIDENT_RPT = CREATEOBJECT('SH_ACCIDENT_RPT')
loSH_ACCIDENT_RPT.MAIN( lTestMode )

IF NOT lTestMode THEN
	schedupdate()
ENDIF

CLOSE DATABASES ALL
RETURN

#DEFINE LOGIT 1
#DEFINE WAITIT 2
#DEFINE NOWAITIT 4
#DEFINE SENDIT 8
#DEFINE CRLF CHR(13) + CHR(10)
#DEFINE RETURN_DATA_MANDATORY .F.
#DEFINE RETURN_DATA_NOT_MANDATORY .T.
#DEFINE CSVFORMAT 6
#DEFINE WRITE_ONLY_UNBUFFERED 11
#DEFINE COMMA ","
#DEFINE CRLF CHR(13) + CHR(10)
#DEFINE HDECS 2
#DEFINE HOURLY_RATE 60
#DEFINE xlEdgeBottom 9
#DEFINE xlContinuous 1
#DEFINE xlMedium -4138

DEFINE CLASS SH_ACCIDENT_RPT AS CUSTOM

	cProcessName = 'SH_ACCIDENT_RPT'

	lTestMode = .F.  && .F. FOR NORMAL OPERATION, .T. TO SUPPRESS ALL BUT DEVELOPER EMAILS AND CHANGE INPUT TABLE, ETC.

	dToday = DATE()
	*dToday = {^2015-04-19}

	* processing properties


	lAutoYield = .T.

	cStartTime = TTOC(DATETIME())

	* wait window properties
	nWaitWindowTimeout = 2
	lWaitWindowIsOn = .T.

	* logfile properties
	lLoggingIsOn = .T.
	cLogFile = 'F:\UTIL\SHOPREPORTS\LOGFILES\SH_ACCIDENT_RPT_log.txt'

	* INTERNAL email properties
	lSendInternalEmailIsOn = .T.
	cFrom = 'mark.bennett@tollgroup.com'
	cSendTo = 'paul.bobucky@tollgroup.com, joe.chinsee@tollgroup.com, mike.drew@tollgroup.com'
	*cSendTo = 'mbennett@fmiint.com'
	cCC = 'mbennett@fmiint.com'
	cSubject = "Shop Accident Report for " + TRANSFORM(DATETIME())
	cAttach = ''
	cBodyText = ''
	cMGRSendTo = ''
	cMGRFrom = ''
	cMGRSubject = ''
	cMGRCC = ''
	cMGRAttach = ''
	cMGRBodyText = ''

	cCOMPUTERNAME = ''
	cUSERNAME = ''


	FUNCTION INIT
		IF NOT DODEFAULT()
			RETURN .F.
		ENDIF
		WITH THIS
			*SET RESOURCE OFF
			CLOSE DATA
			SET CENTURY ON
			*SET DATE AMERICAN
			*SET HOURS TO 24
			SET DECIMALS TO 0
			SET ANSI ON
			SET TALK OFF
			SET DELETED ON
			SET CONSOLE OFF
			SET EXCLUSIVE OFF
			SET SAFETY OFF
			SET EXACT OFF
			SET STATUS BAR ON
			SET SYSMENU OFF
			SET ENGINEBEHAVIOR 70
			SET MULTILOCKS ON
			SET STRICTDATE TO 0
			_VFP.AUTOYIELD = .lAutoYield
			.cCOMPUTERNAME = GETENV("COMPUTERNAME")
			.cUSERNAME = GETENV("USERNAME")
			.lLoggingIsOn = .lLoggingIsOn AND NOT EMPTY(.cLogFile)
		ENDWITH
	ENDFUNC


	FUNCTION DESTROY
		WITH THIS
			IF .lLoggingIsOn  THEN
				SET ALTERNATE OFF
				SET ALTERNATE TO
			ENDIF
		ENDWITH
		DODEFAULT()
	ENDFUNC


	FUNCTION MAIN
		LPARAMETERS tlTestMode
		WITH THIS

			LOCAL lnRow, lcRow, lcReportPeriod, lcFiletoSaveAs, lcSpreadsheetTemplate
			LOCAL oExcel, oWorkbook, oWorksheet, lcErr, lcTitle, loError, lnRate
			LOCAL lcStartRow, lcEndRow, lcTotalsRow, lnNumberOfErrors

			TRY
				lnNumberOfErrors = 0

				.lTestMode = tlTestMode

				IF .lTestMode THEN
					.cLogFile = 'F:\UTIL\SHOPREPORTS\LOGFILES\SH_ACCIDENT_RPT_log_TESTMODE.txt'
					.cSendTo = 'mbennett@fmiint.com'
				ENDIF

				IF .lLoggingIsOn THEN
					SET ALTERNATE TO (.cLogFile) ADDITIVE
					SET ALTERNATE ON
				ENDIF

				.cStartTime = TTOC(DATETIME())

				.TrackProgress('Shop Accidents Report process started....', LOGIT+SENDIT)
				.TrackProgress('COMPUTERNAME = ' + .cCOMPUTERNAME, LOGIT+SENDIT)
				.TrackProgress('USERNAME = ' + .cUSERNAME, LOGIT+SENDIT)
				.TrackProgress('PROJECT = SH_ACCIDENT_RPT', LOGIT+SENDIT)
				IF .lTestMode THEN
					.TrackProgress('                        TEST MODE                        ', LOGIT+SENDIT)
				ENDIF

				USE F:\SHOP\SHDATA\ordhdr IN 0
				USE F:\SHOP\SHDATA\trucks IN 0
				USE F:\SHOP\SHDATA\trailer IN 0
				USE F:\SHOP\SHDATA\CHASLIST IN 0
				USE F:\SHOP\SHDATA\vehmast IN 0


				*!*			TRY
				* make these visible in called procs
				PRIVATE pdStartDate, pdEndDate

				pdEndDate = .dToday - DAY(.dToday)  && last day of prior month
				pdStartDate = GOMONTH(pdEndDate + 1,-1)   && first day of prior month

				IF (MONTH(pdStartDate) = MONTH(pdEndDate)) AND (YEAR(pdStartDate) = YEAR(pdEndDate)) THEN
					lcReportPeriod = CMONTH(pdStartDate) + "_" + TRANSFORM(YEAR(pdStartDate))
				ELSE
					lcReportPeriod = STRTRAN(DTOC(pdStartDate),"/","-") + "_thru_" + STRTRAN(DTOC(pdEndDate),"/","-")
				ENDIF

				lcSpreadsheetTemplate = "F:\SHOP\SHDATA\ACCIDENT_RPT_TEMPLATE3.XLS"

				lcFiletoSaveAs = "F:\SHOP\AllocationReports\Shop_Accident_Report_for_" + lcReportPeriod + ".XLS"
				lcTitle = "Accident Report Spreadsheet for " + lcReportPeriod


				*!*			IF NOT USED('CHASLIST')
				*!*				USE SH!CHASLIST IN 0
				*!*			ENDIF

				*!*			IF NOT USED('ordhdr')
				*!*				USE SH!ordhdr IN 0
				*!*			ENDIF

				*!*			IF NOT USED('vehmast')
				*!*				USE SH!vehmast IN 0
				*!*			ENDIF

				*!*			IF NOT USED('trucks')
				*!*				USE SH!trucks IN 0
				*!*			ENDIF

				*!*			IF NOT USED('trailer')
				*!*				USE SH!trailer IN 0
				*!*			ENDIF

				IF USED('CURORDHDR') THEN
					USE IN CURORDHDR
				ENDIF

				IF USED('CURORDHDRPRE') THEN
					USE IN CURORDHDRPRE
				ENDIF

				IF USED('CURINSIDE') THEN
					USE IN CURINSIDE
				ENDIF

				IF USED('CUROUTSIDE') THEN
					USE IN CUROUTSIDE
				ENDIF

				IF USED('CURINCIDENT') THEN
					USE IN CURINCIDENT
				ENDIF

				* create cursor of selected order header records, with addtional fields for RATE & LABRCOST
				SELECT *, 0000.00 AS RATE, 000000.00 AS LABRCOST, "  " AS DIVISION ;
					FROM ordhdr ;
					INTO CURSOR CURORDHDRPRE ;
					WHERE (crdate >= pdStartDate) AND (crdate <= pdEndDate) ;
					AND (TYPE <> 'BLDG') ;
					AND (REPCAUSE = 'A') ;
					AND (NOT EMPTY(ORDNO)) ;
					ORDER BY crdate ;
					READWRITE

				IF (NOT USED('CURORDHDRPRE')) OR EOF('CURORDHDRPRE') THEN
					*=MESSAGEBOX("No accident data was found")
					.TrackProgress("=================================", LOGIT+SENDIT)
					.TrackProgress("====> No accident data was found!", LOGIT+SENDIT)
					.TrackProgress("=================================", LOGIT+SENDIT)
				ELSE

					* POPULATE RATE BY CRDATE AND CALC LABOR COST FOR EACH WO REC
					SELECT CURORDHDRPRE
					SCAN
						lnRate = .GetHourlyRateByCRDate( CURORDHDRPRE.crdate )
						REPLACE CURORDHDRPRE.RATE WITH lnRate, CURORDHDRPRE.LABRCOST WITH ( CURORDHDRPRE.tothours * lnRate )
					ENDSCAN

					SELECT CURORDHDRPRE
					SCAN

						WAIT WINDOW "checking Vehicle #" + TRANSFORM(CURORDHDRPRE.vehno) NOWAIT

						DO CASE
							CASE CURORDHDRPRE.TYPE == "TRUCK"
								* check trucks first

								SELECT trucks
								LOCATE FOR ALLTRIM(truck_num) == ALLTRIM(CURORDHDRPRE.vehno)
								IF FOUND() THEN
									REPLACE CURORDHDRPRE.DIVISION WITH trucks.DIVISION IN CURORDHDRPRE
									SELECT trucks
									LOCATE
									LOOP
								ENDIF

								SELECT trailer
								LOCATE FOR ALLTRIM(trail_num) == ALLTRIM(CURORDHDRPRE.vehno)
								IF FOUND() THEN
									REPLACE CURORDHDRPRE.DIVISION WITH trailer.DIVISION IN CURORDHDRPRE
									SELECT trailer
									LOCATE
									LOOP
								ENDIF

								SELECT vehmast
								LOCATE FOR ALLTRIM(vehno) == ALLTRIM(CURORDHDRPRE.vehno)
								IF FOUND() THEN
									REPLACE CURORDHDRPRE.DIVISION WITH vehmast.DIVISION IN CURORDHDRPRE
									SELECT vehmast
									LOCATE
									LOOP
								ENDIF

								SELECT CHASLIST
								LOCATE FOR ALLTRIM(CHASSIS) == ALLTRIM(CURORDHDRPRE.vehno)
								IF FOUND() THEN
									REPLACE CURORDHDRPRE.DIVISION WITH CHASLIST.DIVISION IN CURORDHDRPRE
									SELECT CHASLIST
									LOCATE
									LOOP
								ENDIF

							CASE CURORDHDRPRE.TYPE == "TRAILER"
								* check TRAILERS first

								SELECT trailer
								LOCATE FOR ALLTRIM(trail_num) == ALLTRIM(CURORDHDRPRE.vehno)
								IF FOUND() THEN
									REPLACE CURORDHDRPRE.DIVISION WITH trailer.DIVISION IN CURORDHDRPRE
									SELECT trailer
									LOCATE
									LOOP
								ENDIF

								SELECT trucks
								LOCATE FOR ALLTRIM(truck_num) == ALLTRIM(CURORDHDRPRE.vehno)
								IF FOUND() THEN
									REPLACE CURORDHDRPRE.DIVISION WITH trucks.DIVISION IN CURORDHDRPRE
									SELECT trucks
									LOCATE
									LOOP
								ENDIF

								SELECT vehmast
								LOCATE FOR ALLTRIM(vehno) == ALLTRIM(CURORDHDRPRE.vehno)
								IF FOUND() THEN
									REPLACE CURORDHDRPRE.DIVISION WITH vehmast.DIVISION IN CURORDHDRPRE
									SELECT vehmast
									LOCATE
									LOOP
								ENDIF

								SELECT CHASLIST
								LOCATE FOR ALLTRIM(CHASSIS) == ALLTRIM(CURORDHDRPRE.vehno)
								IF FOUND() THEN
									REPLACE CURORDHDRPRE.DIVISION WITH CHASLIST.DIVISION IN CURORDHDRPRE
									SELECT CHASLIST
									LOCATE
									LOOP
								ENDIF

							CASE CURORDHDRPRE.TYPE == "MISC"
								* check vehmast first

								SELECT vehmast
								LOCATE FOR ALLTRIM(vehno) == ALLTRIM(CURORDHDRPRE.vehno)
								IF FOUND() THEN
									REPLACE CURORDHDRPRE.DIVISION WITH vehmast.DIVISION IN CURORDHDRPRE
									SELECT vehmast
									LOCATE
									LOOP
								ENDIF

								SELECT trucks
								LOCATE FOR ALLTRIM(truck_num) == ALLTRIM(CURORDHDRPRE.vehno)
								IF FOUND() THEN
									REPLACE CURORDHDRPRE.DIVISION WITH trucks.DIVISION IN CURORDHDRPRE
									SELECT trucks
									LOCATE
									LOOP
								ENDIF

								SELECT trailer
								LOCATE FOR ALLTRIM(trail_num) == ALLTRIM(CURORDHDRPRE.vehno)
								IF FOUND() THEN
									REPLACE CURORDHDRPRE.DIVISION WITH trailer.DIVISION IN CURORDHDRPRE
									SELECT trailer
									LOCATE
									LOOP
								ENDIF

								SELECT CHASLIST
								LOCATE FOR ALLTRIM(CHASSIS) == ALLTRIM(CURORDHDRPRE.vehno)
								IF FOUND() THEN
									REPLACE CURORDHDRPRE.DIVISION WITH CHASLIST.DIVISION IN CURORDHDRPRE
									SELECT CHASLIST
									LOCATE
									LOOP
								ENDIF

							OTHERWISE
								* CHASSIS - check chaslist first

								SELECT CHASLIST
								LOCATE FOR ALLTRIM(CHASSIS) == ALLTRIM(CURORDHDRPRE.vehno)
								IF FOUND() THEN
									REPLACE CURORDHDRPRE.DIVISION WITH CHASLIST.DIVISION IN CURORDHDRPRE
									SELECT CHASLIST
									LOCATE
									LOOP
								ENDIF

								SELECT trucks
								LOCATE FOR ALLTRIM(truck_num) == ALLTRIM(CURORDHDRPRE.vehno)
								IF FOUND() THEN
									REPLACE CURORDHDRPRE.DIVISION WITH trucks.DIVISION IN CURORDHDRPRE
									SELECT trucks
									LOCATE
									LOOP
								ENDIF

								SELECT trailer
								LOCATE FOR ALLTRIM(trail_num) == ALLTRIM(CURORDHDRPRE.vehno)
								IF FOUND() THEN
									REPLACE CURORDHDRPRE.DIVISION WITH trailer.DIVISION IN CURORDHDRPRE
									SELECT trailer
									LOCATE
									LOOP
								ENDIF

								SELECT vehmast
								LOCATE FOR ALLTRIM(vehno) == ALLTRIM(CURORDHDRPRE.vehno)
								IF FOUND() THEN
									REPLACE CURORDHDRPRE.DIVISION WITH vehmast.DIVISION IN CURORDHDRPRE
									SELECT vehmast
									LOCATE
									LOOP
								ENDIF

						ENDCASE


					ENDSCAN
					WAIT CLEAR

					SELECT * FROM CURORDHDRPRE INTO CURSOR CURORDHDR ORDER BY DIVISION, crdate READWRITE

					SELECT * FROM CURORDHDRPRE INTO CURSOR CURINCIDENT WHERE (LCHARGINC OR NOT EMPTY(CIDRIVER)) ORDER BY crdate READWRITE

					SELECT DIVISION, SUM(LABRCOST + TOTPCOST + OUTSCOST) AS TOTCOST FROM CURORDHDRPRE INTO CURSOR CURINSIDE WHERE NOT ("OUTSIDE VENDOR" $ UPPER(MECHNAME)) GROUP BY 1 ORDER BY 1 READWRITE

					SELECT DIVISION, SUM(LABRCOST + TOTPCOST + OUTSCOST) AS TOTCOST FROM CURORDHDRPRE INTO CURSOR CUROUTSIDE WHERE ("OUTSIDE VENDOR" $ UPPER(MECHNAME)) GROUP BY 1 ORDER BY 1 READWRITE

					oExcel = CREATEOBJECT("excel.application")
					oExcel.displayalerts = .F.
					oExcel.VISIBLE = .F.

					oWorkbook = oExcel.workbooks.OPEN(lcSpreadsheetTemplate)
					oWorkbook.SAVEAS(lcFiletoSaveAs)

					oWorksheet = oWorkbook.Worksheets[1]
					oWorksheet.RANGE("A3","L1000").clearcontents()
					lnRow = 2
					lcStartRow = '3'

					oWorksheet.RANGE("A1").VALUE = lcTitle + " (inside costs)"

					SELECT CURORDHDR
					*SCAN FOR (OUTSCOST = 0.00)
					SCAN FOR NOT ("OUTSIDE VENDOR" $ UPPER(MECHNAME))

						lnRow = lnRow + 1
						lcRow = ALLTRIM(STR(lnRow))

						oWorksheet.RANGE("A"+lcRow).VALUE = CURORDHDR.ORDNO
						oWorksheet.RANGE("B"+lcRow).VALUE = CURORDHDR.crdate
						oWorksheet.RANGE("C"+lcRow).VALUE = CURORDHDR.TYPE
						oWorksheet.RANGE("D"+lcRow).VALUE = CURORDHDR.vehno
						oWorksheet.RANGE("E"+lcRow).VALUE = CURORDHDR.MAINTTYPE
						oWorksheet.RANGE("F"+lcRow).VALUE = CURORDHDR.REPCAUSE
						oWorksheet.RANGE("G"+lcRow).VALUE = CURORDHDR.MECHNAME
						oWorksheet.RANGE("H"+lcRow).VALUE = CURORDHDR.tothours
						oWorksheet.RANGE("I"+lcRow).VALUE = CURORDHDR.RATE
						oWorksheet.RANGE("J"+lcRow).VALUE = CURORDHDR.LABRCOST
						oWorksheet.RANGE("K"+lcRow).VALUE = CURORDHDR.TOTPCOST
						oWorksheet.RANGE("L"+lcRow).VALUE = CURORDHDR.OUTSCOST
						oWorksheet.RANGE("M"+lcRow).VALUE = "'" + CURORDHDR.DIVISION
						oWorksheet.RANGE("N"+lcRow).VALUE = "=SUM(J"+lcRow + "..L"+lcRow+")"
						oWorksheet.RANGE("O"+lcRow).VALUE = CURORDHDR.REMARKS
						oWorksheet.RANGE("P"+lcRow).VALUE = CURORDHDR.WORKDESC

					ENDSCAN

					IF lnRow > 2 THEN
						* We did scan records above

						* TOTAL COLUMN N
						lcEndRow = lcRow

						* UNDERLINE cell to be totaled...
						oWorksheet.RANGE("N" + lcEndRow).BORDERS(xlEdgeBottom).LineStyle = xlContinuous
						oWorksheet.RANGE("N" + lcEndRow).BORDERS(xlEdgeBottom).Weight = xlMedium

						lnRow = lnRow + 1
						lcTotalsRow = ALLTRIM(STR(lnRow))

						oWorksheet.RANGE("N" + lcTotalsRow).VALUE = "=SUM(N" + lcStartRow + "..N" + lcEndRow + ")"

					ENDIF && lnRow > 2

					*************************************************************************************************
					* add div summary
					lnRow = 2
					lcStartRow = '3'
					SELECT CURINSIDE
					SCAN

						lnRow = lnRow + 1
						lcRow = ALLTRIM(STR(lnRow))

						oWorksheet.RANGE("R"+lcRow).VALUE = "'" + CURINSIDE.DIVISION
						oWorksheet.RANGE("S"+lcRow).VALUE = CURINSIDE.TOTCOST

					ENDSCAN

					IF lnRow > 2 THEN
						* We did scan records above

						* TOTAL COLUMN S
						lcEndRow = lcRow

						* UNDERLINE cell to be totaled...
						oWorksheet.RANGE("S" + lcEndRow).BORDERS(xlEdgeBottom).LineStyle = xlContinuous
						oWorksheet.RANGE("S" + lcEndRow).BORDERS(xlEdgeBottom).Weight = xlMedium

						lnRow = lnRow + 1
						lcTotalsRow = ALLTRIM(STR(lnRow))

						oWorksheet.RANGE("S" + lcTotalsRow).VALUE = "=SUM(S" + lcStartRow + "..S" + lcEndRow + ")"

					ENDIF && lnRow > 2
					*************************************************************************************************


					oWorksheet = oWorkbook.Worksheets[2]
					oWorksheet.RANGE("A3","L1000").clearcontents()
					lnRow = 2
					lcStartRow = '3'

					oWorksheet.RANGE("A1").VALUE = lcTitle + " (outside costs)"

					SELECT CURORDHDR
					*SCAN FOR (OUTSCOST > 0.00)
					SCAN FOR ("OUTSIDE VENDOR" $ UPPER(MECHNAME))

						lnRow = lnRow + 1
						lcRow = ALLTRIM(STR(lnRow))

						oWorksheet.RANGE("A"+lcRow).VALUE = CURORDHDR.ORDNO
						oWorksheet.RANGE("B"+lcRow).VALUE = CURORDHDR.crdate
						oWorksheet.RANGE("C"+lcRow).VALUE = CURORDHDR.TYPE
						oWorksheet.RANGE("D"+lcRow).VALUE = CURORDHDR.vehno
						oWorksheet.RANGE("E"+lcRow).VALUE = CURORDHDR.MAINTTYPE
						oWorksheet.RANGE("F"+lcRow).VALUE = CURORDHDR.REPCAUSE
						oWorksheet.RANGE("G"+lcRow).VALUE = CURORDHDR.MECHNAME
						oWorksheet.RANGE("H"+lcRow).VALUE = CURORDHDR.tothours
						oWorksheet.RANGE("I"+lcRow).VALUE = CURORDHDR.RATE
						oWorksheet.RANGE("J"+lcRow).VALUE = CURORDHDR.LABRCOST
						oWorksheet.RANGE("K"+lcRow).VALUE = CURORDHDR.TOTPCOST
						oWorksheet.RANGE("L"+lcRow).VALUE = CURORDHDR.OUTSCOST
						oWorksheet.RANGE("M"+lcRow).VALUE = "'" + CURORDHDR.DIVISION
						oWorksheet.RANGE("N"+lcRow).VALUE = "=SUM(J"+lcRow + "..L"+lcRow+")"
						oWorksheet.RANGE("O"+lcRow).VALUE = CURORDHDR.REMARKS
						oWorksheet.RANGE("P"+lcRow).VALUE = CURORDHDR.WORKDESC

					ENDSCAN

					IF lnRow > 2 THEN
						* We did scan records above

						* TOTAL COLUMN N
						lcEndRow = lcRow

						* UNDERLINE cell to be totaled...
						oWorksheet.RANGE("N" + lcEndRow).BORDERS(xlEdgeBottom).LineStyle = xlContinuous
						oWorksheet.RANGE("N" + lcEndRow).BORDERS(xlEdgeBottom).Weight = xlMedium

						lnRow = lnRow + 1
						lcTotalsRow = ALLTRIM(STR(lnRow))

						oWorksheet.RANGE("N" + lcTotalsRow).VALUE = "=SUM(N" + lcStartRow + "..N" + lcEndRow + ")"

					ENDIF && lnRow > 2


					*************************************************************************************************
					* add div summary
					lnRow = 2
					lcStartRow = '3'
					SELECT CUROUTSIDE
					SCAN

						lnRow = lnRow + 1
						lcRow = ALLTRIM(STR(lnRow))

						oWorksheet.RANGE("R"+lcRow).VALUE = "'" + CUROUTSIDE.DIVISION
						oWorksheet.RANGE("S"+lcRow).VALUE = CUROUTSIDE.TOTCOST

					ENDSCAN

					IF lnRow > 2 THEN
						* We did scan records above

						* TOTAL COLUMN S
						lcEndRow = lcRow

						* UNDERLINE cell to be totaled...
						oWorksheet.RANGE("S" + lcEndRow).BORDERS(xlEdgeBottom).LineStyle = xlContinuous
						oWorksheet.RANGE("S" + lcEndRow).BORDERS(xlEdgeBottom).Weight = xlMedium

						lnRow = lnRow + 1
						lcTotalsRow = ALLTRIM(STR(lnRow))

						oWorksheet.RANGE("S" + lcTotalsRow).VALUE = "=SUM(S" + lcStartRow + "..S" + lcEndRow + ")"

					ENDIF && lnRow > 2
					*************************************************************************************************


					* 3rd tab for Chargeable Incident Work Orders

					oWorksheet = oWorkbook.Worksheets[3]
					oWorksheet.RANGE("A3","L1000").clearcontents()
					lnRow = 2
					lcStartRow = '3'

					oWorksheet.RANGE("A1").VALUE = lcTitle + " Chargeable Incidents"

					SELECT CURINCIDENT
					SCAN

						lnRow = lnRow + 1
						lcRow = ALLTRIM(STR(lnRow))

						oWorksheet.RANGE("A"+lcRow).VALUE = CURINCIDENT.ORDNO
						oWorksheet.RANGE("B"+lcRow).VALUE = CURINCIDENT.crdate
						oWorksheet.RANGE("C"+lcRow).VALUE = CURINCIDENT.TYPE
						oWorksheet.RANGE("D"+lcRow).VALUE = CURINCIDENT.vehno
						oWorksheet.RANGE("E"+lcRow).VALUE = CURINCIDENT.MAINTTYPE
						oWorksheet.RANGE("F"+lcRow).VALUE = CURINCIDENT.REPCAUSE
						oWorksheet.RANGE("G"+lcRow).VALUE = CURINCIDENT.MECHNAME
						oWorksheet.RANGE("H"+lcRow).VALUE = CURINCIDENT.tothours
						oWorksheet.RANGE("I"+lcRow).VALUE = CURINCIDENT.RATE
						oWorksheet.RANGE("J"+lcRow).VALUE = CURINCIDENT.LABRCOST
						oWorksheet.RANGE("K"+lcRow).VALUE = CURINCIDENT.TOTPCOST
						oWorksheet.RANGE("L"+lcRow).VALUE = CURINCIDENT.OUTSCOST
						oWorksheet.RANGE("M"+lcRow).VALUE = "'" + CURINCIDENT.DIVISION
						oWorksheet.RANGE("N"+lcRow).VALUE = "=SUM(J"+lcRow + "..L"+lcRow+")"

						oWorksheet.RANGE("O"+lcRow).VALUE = CURINCIDENT.REMARKS
						oWorksheet.RANGE("P"+lcRow).VALUE = CURINCIDENT.WORKDESC
						oWorksheet.RANGE("Q"+lcRow).VALUE = CURINCIDENT.CIDRIVER

					ENDSCAN

					IF lnRow > 2 THEN
						* We did scan records above

						* TOTAL COLUMN N
						lcEndRow = lcRow

						* UNDERLINE cell to be totaled...
						oWorksheet.RANGE("N" + lcEndRow).BORDERS(xlEdgeBottom).LineStyle = xlContinuous
						oWorksheet.RANGE("N" + lcEndRow).BORDERS(xlEdgeBottom).Weight = xlMedium

						lnRow = lnRow + 1
						lcTotalsRow = ALLTRIM(STR(lnRow))

						oWorksheet.RANGE("N" + lcTotalsRow).VALUE = "=SUM(N" + lcStartRow + "..N" + lcEndRow + ")"

					ENDIF && lnRow > 2

					*************************************************************************************************

					*MESSAGEBOX("The Accident Report Spreadsheet has been populated in Excel.",0 + 64,lcTitle)
					oWorkbook.SAVE()
					*oExcel.VISIBLE = .T.

					IF TYPE('oExcel') = 'O' AND NOT ISNULL(oExcel) THEN
						oExcel.QUIT()
					ENDIF

					.cAttach = lcFiletoSaveAs

				ENDIF  &&  (NOT USED('CURORDHDRPRE')) OR EOF('CURORDHDRPRE')


				*******************************************************************************************************************
				.TrackProgress('Shop Accident Report process ended normally.', LOGIT+SENDIT)

			CATCH TO loError

				.TrackProgress('There was an error.',LOGIT+SENDIT)
				.TrackProgress('The Error # is: ' + TRANSFORM(loError.ERRORNO), LOGIT+SENDIT)
				.TrackProgress('The Error Message is: ' + TRANSFORM(loError.MESSAGE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred in Program: ' + TRANSFORM(loError.PROCEDURE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred at line #: ' + TRANSFORM(loError.LINENO), LOGIT+SENDIT)
				lnNumberOfErrors = lnNumberOfErrors + 1
				.cSendTo = 'mbennett@fmiint.com'

				IF TYPE('oExcel') = 'O' AND NOT ISNULL(oExcel) THEN
					oExcel.QUIT()
				ENDIF

			ENDTRY

			WAIT CLEAR
			***************** INTERNAL email results ******************************
			.TrackProgress('About to send status email.',LOGIT+SENDIT)
			.TrackProgress('The logfile is: ' + .cLogFile, LOGIT+SENDIT)
			.TrackProgress('Shop Accident Report process started: ' + .cStartTime, LOGIT+SENDIT)
			.TrackProgress('Shop Accident Report process finished: ' + TTOC(DATETIME()), LOGIT+SENDIT)

			IF .lSendInternalEmailIsOn THEN
				DO FORM dartmail2 WITH .cSendTo,.cFrom,.cSubject,.cCC,.cAttach,.cBodyText,"A"
				.TrackProgress('Sent status email.',LOGIT)
			ELSE
				.TrackProgress('Did not send status email: THIS.lSendInternalEmailIsOn = FALSE.',LOGIT+SENDIT)
			ENDIF

		ENDWITH
		RETURN
	ENDFUNC && main


	FUNCTION GetHourlyRateByCRDate
		LPARAMETERS tdCRdate
		DO CASE
			CASE tdCRdate < {^2008-09-01}
				lnRate = 60.00
			OTHERWISE
				lnRate = 70.00
		ENDCASE
		RETURN lnRate
	ENDFUNC


	FUNCTION GetHourlyRateByDivision
		LPARAMETERS tcDivision
		DO CASE
			CASE INLIST(tcDivision,'05','52','56','57','58','71','74')
				lnRate = 60
			OTHERWISE
				lnRate = 60  && the default rate
		ENDCASE
		RETURN lnRate
	ENDFUNC


	PROCEDURE TrackProgress
		* do any combination of Wait Window, writing to logfile, and adding to body of email,
		* based on nFlags parameter.
		LPARAMETERS tcExpression, tnFlags
		WITH THIS
			IF BITAND(tnFlags,LOGIT) = LOGIT THEN
				IF .lLoggingIsOn THEN
					?
					? .cProcessName + "  " + TTOC(DATETIME()) + ": " + tcExpression
				ENDIF
			ENDIF
			IF .lWaitWindowIsOn THEN
				IF BITAND(tnFlags,NOWAITIT) = NOWAITIT THEN
					WAIT WINDOW tcExpression NOWAIT
				ENDIF
				IF BITAND(tnFlags,WAITIT) = WAITIT THEN
					WAIT WINDOW tcExpression TIMEOUT .nWaitWindowTimeout
				ENDIF
			ENDIF
			IF BITAND(tnFlags,SENDIT) = SENDIT THEN
				IF .lSendInternalEmailIsOn THEN
					.cBodyText = .cBodyText + tcExpression + CRLF + CRLF
				ENDIF
			ENDIF
		ENDWITH
	ENDPROC  &&  TrackProgress


ENDDEFINE
