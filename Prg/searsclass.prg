
*******************************************************************************************************
* SEARS load/bill class -- actually supports other clients such as DSG in the feed as of 1/6/08 MB
**********
* Supports Rack Room / Off Broadway Oct. 2014
*
* using sql gen pk 12/26/2017 MB
*
* MB 12/26/2017 - fixed problem where sum of split charges might be off by a cent (rounding problem)
*
* MB 03/13/201 - added Rack Room rates for years beginning 4/1/2018 and 4/1/2019 per RF392274
*
* 02/24/2018 MB: changed fmiint.com emails to Toll emails.
*******************************************************************************************************

#DEFINE LOGIT 1
#DEFINE WAITIT 2
#DEFINE NOWAITIT 4
#DEFINE SENDIT 8
#DEFINE CRLF CHR(13) + CHR(10)
#DEFINE LARGE_NEGATIVE_NUMBER -999999
#DEFINE ITPC_QTY_OFFSET 192
#DEFINE SEPARATOR_LINE "--------------------------------------------------"
*!*	#DEFINE	KMART_FOOTWEAR_DIVISION '035'
*!*	#DEFINE	SEARS_MENS_DIVISION '067'
*!*	#DEFINE	SEARS_WOMENS_DIVISION '054'
*!*	#DEFINE	SEARS_KIDS_DIVISION '036'
*!*	#DEFINE	SEARS_ATHLETIC_DIVISION '076'
*!*	#DEFINE	SEARS_LANDS_END_DIVISION '086'
#DEFINE	RITE_AID_DIVISION '00'
#DEFINE	KMART_FOOTWEAR_DIVISION '35'
#DEFINE	KMART_DIV042_DIVISION '42'
#DEFINE	KMART_NON_FOOTWEAR_DIVISION '48'
#DEFINE	SEARS_MENS_DIVISION '67'
#DEFINE	SEARS_WOMENS_DIVISION '54'
#DEFINE	SEARS_KIDS_DIVISION '36'
#DEFINE	SEARS_ATHLETIC_DIVISION '76'
#DEFINE	SEARS_LANDS_END_DIVISION '86'
#DEFINE	SEARS_TOWELS_DIVISION '96'
#DEFINE	SEARS_JEANS_DIVISION '41'
#DEFINE	SEARS_BELTS_DIVISION '33'
#DEFINE	SEARS_KIDS_JEANS_DIVISION '40'
#DEFINE	SEARS_PILLOWS_DIVISION '24'
#DEFINE	SEARS_BRAS_DIVISION '18'
#DEFINE	SEARS_FRAGRANCE_DIVISION '74'

#DEFINE RACKROOM_CHAS_DIVISION '200' 
#DEFINE RACKROOM_FOOTWEAR_DIVISION '100' 

#DEFINE BROADWAY_CHAS_DIVISION '200' 
#DEFINE BROADWAY_FOOTWEAR_DIVISION '100'

#DEFINE	FORSAN_MENS_DIVISION '67'


#DEFINE SHC_VALID_DIV_LIST "'18','24','33','35','67','54','36','76','86','00','48','96','41','40','42','74'"
#DEFINE RR_VALID_DIV_LIST "'100','200'"

DEFINE CLASS SEARS AS CUSTOM

	cProcessName = 'SEARS' 
	
	* multiclient properties 
	cClient = ''
	cTNodeFilter = ''
	* invoice properties
	lGenerateInvoice = .F.
	nSQLConnection = 0
	nAccountID = 0
	nBillToID = 0 
	nInvoiceWONum = 0
	cInvoiceMessage = ''
	dInvoiceDate = DATE()
	
	* test/mode of operation properties   
	
	lFatalError = .F.  && normally an error in each day will be trapped by the error handler and processing will go to the next day.  Set this to .T. in the code where we want to stop on an error.
	
	lUseTestDataForBilling = .F.  && .T. means we will use data from C: drive for test billing
	
	
	lTestInvoice = .T.   && .F. for normal operation with production data, .T. for testing with dev data 
	
	
	lTestMode = .F.  && .F. FOR NORMAL OPERATION, .T. TO SUPPRESS ALL BUT DEVELOPER EMAILS AND CHANGE INPUT PATH, ETC.
	lPackTables = .F.
	lSummaryOnly = .F.
	nTimeOutSeconds = 10 

	* billing-logic properties
	lEnableTransferRCNProcessing = .T.   &&& set Serec transfer logic on or off
	lTreatSortType41AsFullCase = .F.
	lFilterCURRCNsByDiv = .F.  && when .T., we do not show all RCNs for a client in the Receipt report section; we only show those RCNs that matched the Division filter.
	cInvDetCompany = 'A'  && default for Sears, will change for RackRoom/OffBroadway
	lSplitPickNPackInvoiceDetail = .T.  && set to TRUE to have separate Pick N Pack invoice detail records with GLcode 13, for TiltTray transactions.d
	
	lUsePrepackForManualUnitCounting = .T.  && set to .F. if we are rebilling data older than 10/1/06.
	* revised 12/23/08 to no longer use this -- code will work as if it were true.
	*!*		lUseRCNForRDCAKey = .T.   && may need to be set to .f. if we are rebilling data older than Aug 2006.
	lDotComProcessing = .F.
	cDotComDest_loc = '07840     '
	cDotComSummary = ''
	lDoSkuBreakdown = .F.  && set to .T. to break down non-xdock receipts by sku count ranges - was used for Footstar
	lTransferRCN = .F.
	lRackRoomProcessing = .F.
	
	* data loading properties
	lLoadingDeleteByDateRange = .T.
	lUseSQLInvPDF = .T.

	* PDF report properties
	lUseAmyMerge = .T.  && .F. to use Acrobat Professional for merging pdfs, .T. to use Darren's AmyMerge.prg (i.e., Amyuni)
	oAmyDoc = NULL
	cPDFFilename = ''
	*cPDFReportFRX = "M:\MARKB\RPT\ftpmdata"
	*cPDFReportFRX = "M:\DEV\RPT\FTPMDATA"
	cPDFReportFRX = ''
	cPDFWeeklyReportFRX = '' 
	cPDFFilenameWeekly = ''
	
	oAcrobat = NULL
	oAcrobat2 = NULL
	cBigPDFFile = "C:\TEMPFOX\CONSOLIDATED_BILL_" + STRTRAN(DTOC(DATE()),"/") + ".PDF"
	cInvoiceFRX = 'M:\DEV\RPT\INVOICEP.FRX'
	cInvoiceFRT = 'M:\DEV\RPT\INVOICEP.FRT'
	cInvoiceFRXPDF = 'C:\TEMPFOX\INVOICEPDF.FRX'
	cInvoiceFRTPDF = 'C:\TEMPFOX\INVOICEPDF.FRT'
	cInvoiceTempRpt = 'C:\TEMPFOX\INVOICEPDF'
	cBillReceiptDetailReport = ""
	cBillReceiptDetailReportNODATA = ""
	cBillShippedDetailReport = ""
	cBillShippedDetailReportNODATA = ""

	* Data Format properties
	lUseAugust2006Format = .F.

	lAutoYield = .T.

	cStartTime = TTOC(DATETIME())

	* path properties
	cInPath        = 'F:\FTPUSERS\SEARS\BILLDATAIN\'
	cInPathForGZIP = 'F:\FTPUSERS\SEARS\BILLDATAIN\*.gz'
	cArchivedPath  = 'F:\FTPUSERS\SEARS\BILLDATAIN\ARCHIVED\'
	cUnzippedPath  = 'F:\FTPUSERS\SEARS\BILLDATAIN\'
	cProcessedPath = 'F:\FTPUSERS\SEARS\BILLDATAIN\UNZIPPED\PROCESSED\'

	* for RATO / RATC backload
	cBackLoadRATOInPath        = 'F:\FTPUSERS\SEARS\RATOLOAD\'
	cBackLoadRATOArchivedPath  = 'F:\FTPUSERS\SEARS\RATOLOAD\ARCHIVED\'
	cBackLoadRATOUnzippedPath  = 'F:\FTPUSERS\SEARS\RATOLOAD\UNZIPPED\'
	cBackLoadRATOProcessedPath = 'F:\FTPUSERS\SEARS\RATOLOAD\PROCESSED\'

	* table properties
	cTransTable = 'F:\SEARS\PMDATA\FTPMDATA'
	cHistoryTable = 'F:\SEARS\PMDATA\FTHISTORY'
	cRCRCTable = 'F:\SEARS\PMDATA\RCRCHIST'
	cRCRZTable = 'F:\SEARS\PMDATA\RCRZHIST'
	cRDCATable = 'F:\SEARS\PMDATA\RDCAHIST'
	cRATOTable = 'F:\SEARS\PMDATA\RATOHIST'
	cRATCTable = 'F:\SEARS\PMDATA\RATCHIST'
	cINVHISTTable = 'F:\SEARS\PMDATA\DAILYINVHIST'
	cManualUnitsTable = 'F:\SEARS\PMDATA\MANUNITS'
	cRCNTRANSFERTable = 'F:\SEARS\PMDATA\TRANSFERS'
	cRETURNSTable = 'F:\SEARS\PMDATA\RETURNS'
	cIAADXFERTable = 'F:\SEARS\PMDATA\IAADXFER'

	* local vs remote data properties
	lLocalData = .F.

	* wait window properties
	nWaitWindowTimeout = 2
	lWaitWindowIsOn = .T.

	* logfile properties
	lLoggingIsOn = .T.
	cLogFile = 'F:\SEARS\LOGFILES\SEARSBILL_log.txt'
	cMemoDumpFile = 'F:\SEARS\LOGFILES\SEARSMEMORY_DUMP.txt'

	* INTERNAL email properties
	lSendInternalEmailIsOn = .T.
	cFrom = 'TGF Corporate <fmicorporate@fmiint.com>'
	*cSendTo = 'mark.bennett@tollgroup.com'
	cSendTo = 'mark.bennett@tollgroup.com'
	cLoadSendTo = 'mark.bennett@tollgroup.com'
	cCC = ''
	cSubject = ''
	cAttach = ''
	cBodyText = ''
	cSummaryOnlySendTo = 'mark.bennett@tollgroup.com'
	cSummaryOnlyCC = ''

	cCOMPUTERNAME = ''
	cUSERNAME = ''

	* EXTERNAL email properties - these are defaults
	lSendExternalEmailIsOn = .T.
	cClientFrom = 'TGF Corporate <fmicorporate@fmiint.com>'
	cClientSendTo = ''
	cClientCC = 'Mark Bennett <mark.bennett@tollgroup.com>'
	cClientSubject = ''
	cClientAttach = ''
	* NOTE: .cClientBodyText should be initialized to empty, and only populated if process
	* creates at least one Work order.
	cClientBodyText = ''

	* attachment properties
	lAttachSeparateReports = .F.
	
	* optional pdf reports/ XL attachments properties
	lAppendTransactionDetailToPDF = .F.
	lAttachExcelSummaryFiles = .F.

	* error handling properties
	nValidationErrors = 0
	nNumFilesFound = 0
	lTrappedError = .F.
	cInvalidDivisionList = ''

	* transation type count analysis properties
	nTotalRecs = 0
	nIAADTotalRecs = 0
	nIARNTotalRecs = 0
	nRIRITotalRecs = 0
	nAUADTotalRecs = 0
	nAUADOldRecs = 0
	nAUAD_8_2006_Recs = 0
	nAUADGoodRecs = 0
	nAUADBadRecs = 0
	
	nAUAD_10_XdockRecs = 0
	nAUAD_41_ManualRecs = 0
	nAUAD_51_FullRecs = 0
	nAUAD_71_TiltRecs = 0
	nAUAD_99_FindRecs = 0
	nAUAD_XX_UnknRecs = 0	
	
	nRBPCTotalRecs = 0
	nRCRCTotalRecs = 0
	nRDCATotalRecs = 0
	
	nAUAD_DotComRecs = 0
	nRDCA_DotComRecs = 0

	* date properties
	dBeginningTransdate = CTOD('')
	dEndingTransdate = CTOD('')
	dTransDate = CTOD('')
	cNoDataForTheseDates = ''
	cNoShippedDataForTheseDates = ''
	cNoReceiptDataForTheseDates = ''

	* rate properties
	nReceiptCrossDockPerCtnRate = 0.000
	nReceiptCrossDockCAPEXPerCtnRate = 0.000
	nReceiptNonXDockPerCtnRate  = 0.000					
	nReceiptNonXDockCAPEXPerCtnRate  = 0.000
						
	nReceiptNonXDockTRANSFERPerCtnRate  = 0.000  && TO SUPPORT SEREC TRANSFERS IN SUMMER 2013
						
	nShippedCrossDockPerCtnRate = 0.000
	nShippedCrossDockCAPEXPerCtnRate = 0.000				
	nManualPerUnitRate = 0.000
	
	nTiltPerUnitRate            = 0.000
	nTiltResidualsPerUnitRate   = 0.000
	nFullCasePerCtnRate         = 0.000

	nReceiptNonXDockPerUNITRate  = 0.000	&& to support July 214 Rack Room 'receiving (solids or pre-pack)' unit transactions	
	
	nReturnReceiptPerCtnRate = 0.000  && for return receipt transactions from San Pedro
	
	nTiltResidualsPerCartonRate = 0.000  && for Rack Room
	
	* new 'total by bill period' accumulators for Rack Room
	nReceiptCrossDockCtnsAll = 0
	nReceiptNonXDockUNITSAll = 0
	nReceiptNonXDockTRANSFERCtnsAll = 0
	nReturnReceiptCtnsAll = 0
	nTotalReceiptAll = 0
	nShippedCrossDockCtnsAll = 0
	nTiltUnitsAll = 0
	nTiltResidualUnitsAll = 0
	nTiltResidualCartonsAll = 0
	nFullCaseCtnsAll = 0
	nTotalShippedAll = 0
	nTotalChargesAll = 0	
					
	* these are not used any more
	nSKU_0_PerCtnRate        = 0.000
	nSKU_21_PerCtnRate       = 0.000
	nSKU_31_PerCtnRate       = 0.000
	nSKU_41_PerCtnRate       = 0.000
	*nManualPerUnitRate       = 0.000  && reactivated 4/15/11 mb
	nFindingsPerCtnRate      = 0.000
	
	* mode properties
	lAutoMode = .F.

	* Excel file properties
	cXLReceiptFilename = "C:\TEMPFOX\ReceiptsByDateAndCompany.XLS"
	cXLShippedFilename = "C:\TEMPFOX\ShippedByDateAndCompany.XLS"
	
	* Excel object/report properties
	cHandlingSummaryTemplate = 'F:\UTIL\MLAUTOBILLING\TEMPLATES\HandlingSummaryTemplate2.xls'
	cSummarySaveAs = ''
	oExcel = NULL
	oWorkbook = NULL
	oWorksheet = NULL
	nExcelSummaryRow = 0

	* test properties
	cExtraWhere = ''

	* edit box obj ref from calling form
	oEdtBox = NULL
	
	* SQL filters to support differentiating divisions (i.e. product lines) within client
	cIAAD_SQL_Where = ''
	cAUAD_SQL_Where = ''
	cRCRC_SQL_Where = ''
	cRDCA_SQL_Where = ''
	cRIRI_SQL_Where = ''
	cIARN_SQL_Where = ''
	cIAAD_SQL_Where = ''

	FUNCTION INIT
		IF NOT DODEFAULT()
			RETURN .F.
		ENDIF
		CLOSE DATABASES ALL
		*SET RESOURCE OFF
		SET CENTURY ON
		SET ANSI ON
		SET TALK OFF
		SET DELETED ON
		SET CONSOLE OFF
		SET EXCLUSIVE OFF
		SET SAFETY OFF
		SET EXACT OFF
		*SET STATUS BAR OFF
		SET SYSMENU OFF
		SET ENGINEBEHAVIOR 70
		SET MULTILOCKS ON
		SET DECIMALS TO 3
		SET MEMOWIDTH TO 65
		SET REPROCESS TO 10 SECONDS  && so RLOCK()s in code writing to ARGENPK will retry for xx seconds.

		PUBLIC gUserid, gProcess
		STORE "MLAUTOBILL" TO gUserid, gProcess

		WITH THIS
			_VFP.AUTOYIELD = .lAutoYield
			LOCAL llError
			IF .lTestMode THEN
				.cLogFile = 'F:\SEARS\LOGFILES\SEARSBILL_log_TESTMODE.txt'
			ENDIF
			* set up logging
			.lLoggingIsOn = .lLoggingIsOn AND NOT EMPTY(.cLogFile)
			IF .lLoggingIsOn THEN
				TRY
					SET ALTERNATE TO (.cLogFile) ADDITIVE
					SET ALTERNATE ON
				CATCH
					llError = .T.
					WAIT WINDOW TIMEOUT 60 "Could not access the logfile."
				ENDTRY
			ENDIF
			* don't instantiate object if we couldn't access the logfile,
			* which means the process is already running.
			IF llError THEN
				RETURN .F.
			ENDIF
			IF .lTestMode THEN
				*.cSendTo = 'mark.bennett@tollgroup.com'
				*.cClientSendTo = 'mark.bennett@tollgroup.com'
				*.cClientCC = 'mark.bennett@tollgroup.com'
				.cSendTo = 'mark.bennett@tollgroup.com'
				.cClientSendTo = 'mark.bennett@tollgroup.com'
				.cClientCC = 'mark.bennett@tollgroup.com'
				.cSummaryOnlySendTo = 'mark.bennett@tollgroup.com'
				.cSummaryOnlyCC = 'mark.bennett@tollgroup.com'
			ENDIF
			.cCOMPUTERNAME = GETENV("COMPUTERNAME")
			.cUSERNAME = GETENV("USERNAME")
			.nValidationErrors = 0
			.CreateALLRCNCursor()
			.CreateALLSHIPPEDCursor()
			.CreateInvoiceCursor()
			
			* added this logic because we want to pack occasionally, but not necessarily every day we load data.
			* so we'll pack if we load on a Friday
			IF DOW(DATE(),1) = 6 THEN
				.lPackTables = .T.
			ENDIF
			
			IF NOT .lTestInvoice THEN
				.cSendTo = 'mark.bennett@tollgroup.com'
				*!*		.cInvoiceFRX = 'M:\DEV\RPT\INVOICEPNEW.FRX'
				*!*		.cInvoiceFRT = 'M:\DEV\RPT\INVOICEPNEW.FRT'
			ENDIF
			
		ENDWITH
	ENDFUNC


	FUNCTION DESTROY
		WITH THIS
			IF .lLoggingIsOn  THEN
				SET ALTERNATE OFF
				SET ALTERNATE TO
			ENDIF
			SET STATUS BAR ON
			SET TALK ON
			.oAcrobat = NULL
			.oAcrobat2 = NULL
			.oEdtBox = NULL
			.oAmyDoc =NULL
		ENDWITH
		CLOSE DATA
		DODEFAULT()
	ENDFUNC


	FUNCTION SetAutoMode
		WITH THIS
			.lAutoMode = .T.
			* make sure we're creating separate pdf attachments, because we currently can't consolidate PDFs during autoload
			.lAttachSeparateReports = .T.
		ENDWITH
	ENDFUNC


	FUNCTION SetTestMode
		WITH THIS
			.lTestMode = .T.
		ENDWITH
	ENDFUNC


	FUNCTION SetLoadingDeleteByDateRangeOff
		WITH THIS
			.lLoadingDeleteByDateRange = .F.
		ENDWITH
	ENDFUNC
	

	FUNCTION SetClient
		LPARAMETERS tcClient
		LOCAL llRetVal, lcDivString
		llRetVal = .T.
		WITH THIS
			* construct subject line and report filenames by client
			.cClient = UPPER(ALLTRIM(tcClient))
			.cSubject = .cClient + ' Make Bill Process Results for: ' + TTOC(DATETIME())
			.cClientSubject = .cClient + ' Daily Bills processed on: ' + TTOC(DATETIME())
			.cPDFFilename = "C:\TEMPFOX\" + .cClient + "_BILL_SUMMARY"
			.cPDFReportFRX = "M:\DEV\RPT\" + .cClient + "DAILYBILLSUMMARY"
			.cPDFFilenameWeekly = "C:\TEMPFOX\" + .cClient + "_WEEKLY_BILL_SUMMARY"
			.lRackRoomProcessing = .F.
			DO CASE
				CASE tcClient == "KMART ALL"  && THIS IS FOR TEST/handling summary ONLY, SINCE WE BEGAN SPLITTING OUT FOOTWEAR VS NON-FOOTWEAR
					.cTNodeFilter = "KM"
					.nAccountID = 5837  && SEARS HOLDINGS
					.nInvoiceWONum = 65
					.lDotComProcessing = .F.
					.lAppendTransactionDetailToPDF = .T.
					.lAttachExcelSummaryFiles = .F. 

					*!*	.cBillReceiptDetailReport = "M:\DEV\RPT\SKUBKDN"
					*!*	.cBillReceiptDetailReportNODATA = "M:\DEV\RPT\SKUBKDNNODATA"
					.cBillReceiptDetailReport = "M:\DEV\RPT\MLAUTOBILLRECEIPTDETAIL"
					.cBillReceiptDetailReportNODATA = "M:\DEV\RPT\MLAUTOBILLRECEIPTDETAILNODATA"
					.cSummarySaveAs = 'F:\UTIL\MLAUTOBILLING\REPORTS\KMART HANDLING'
					
					.cBillShippedDetailReport = "M:\DEV\RPT\SHIPDETL"
					.cBillShippedDetailReportNODATA = "M:\DEV\RPT\SHIPDETLNODATA"
					
					* new version of report modified to support Tilt Residuals (which began coming in on 1/11/2010)
					.cPDFReportFRX = "M:\DEV\RPT\" + .cClient + "DAILYBILLSUMMARY2"  
					.cSummaryOnlySendTo = 'joni.golding@tollgroup.com, joana.burke@tollgroup.com'
					.cSummaryOnlyCC = 'carolyn.balcezak@tollgroup.com, jim.campanelli@tollgroup.com, mark.bennett@tollgroup.com'
					.cAUAD_SQL_Where = ''
					.cRCRC_SQL_Where = ''
					.cRDCA_SQL_Where = ''
					.cRIRI_SQL_Where = ''
					.cIARN_SQL_Where = ''
					.lTreatSortType41AsFullCase = .T.
					
				CASE tcClient == "KMART FOOTWEAR"
					.cTNodeFilter = "KM"
					.nAccountID = 5837  && SEARS HOLDINGS
					.nInvoiceWONum = 61
					.lDotComProcessing = .F.
					.lAppendTransactionDetailToPDF = .T.
					.lAttachExcelSummaryFiles = .F.
					*!*	.cBillReceiptDetailReport = "M:\DEV\RPT\SKUBKDN"
					*!*	.cBillReceiptDetailReportNODATA = "M:\DEV\RPT\SKUBKDNNODATA"
					.cBillReceiptDetailReport = "M:\DEV\RPT\MLAUTOBILLRECEIPTDETAIL"
					.cBillReceiptDetailReportNODATA = "M:\DEV\RPT\MLAUTOBILLRECEIPTDETAILNODATA"
					
					.cBillShippedDetailReport = "M:\DEV\RPT\SHIPDETL"
					.cBillShippedDetailReportNODATA = "M:\DEV\RPT\SHIPDETLNODATA"
					
					.cSummarySaveAs = 'F:\UTIL\MLAUTOBILLING\REPORTS\KMART FOOTWEAR HANDLING'
					* new version of report modified to support Tilt Residuals (which began coming in on 1/11/2010)
					.cPDFReportFRX = "M:\DEV\RPT\" + .cClient + "DAILYBILLSUMMARY2"  
					.cSummaryOnlySendTo = 'joni.golding@tollgroup.com'
					.cSummaryOnlyCC = 'cbalcezak@fmiint.com, jcampanelli@fmiint.com, mperez1@searshc.com, mark.bennett@tollgroup.com'
					.cAUAD_SQL_Where = " AND RIGHT(DIVISION,2) = '" + KMART_FOOTWEAR_DIVISION + "' "
					.cRCRC_SQL_Where = " AND RIGHT(DIVISION,2) = '" + KMART_FOOTWEAR_DIVISION + "' "
					.cRDCA_SQL_Where = " AND RIGHT(DIVISION,2) = '" + KMART_FOOTWEAR_DIVISION + "' "
					.cRIRI_SQL_Where = " AND RIGHT(DIVISION,2) = '" + KMART_FOOTWEAR_DIVISION + "' "
					.cIARN_SQL_Where = " AND RIGHT(DIVISION,2) = '" + KMART_FOOTWEAR_DIVISION + "' "
					.lTreatSortType41AsFullCase = .T.
					
				CASE tcClient == "KMART DIV042"
					.cTNodeFilter = "KM"
					.nAccountID = 5837  && SEARS HOLDINGS
					.nInvoiceWONum = 61
					.lDotComProcessing = .F.
					.lAppendTransactionDetailToPDF = .T.
					.lAttachExcelSummaryFiles = .F.
					*!*	.cBillReceiptDetailReport = "M:\DEV\RPT\SKUBKDN"
					*!*	.cBillReceiptDetailReportNODATA = "M:\DEV\RPT\SKUBKDNNODATA"
					.cBillReceiptDetailReport = "M:\DEV\RPT\MLAUTOBILLRECEIPTDETAIL"
					.cBillReceiptDetailReportNODATA = "M:\DEV\RPT\MLAUTOBILLRECEIPTDETAILNODATA"
					
					.cBillShippedDetailReport = "M:\DEV\RPT\SHIPDETL"
					.cBillShippedDetailReportNODATA = "M:\DEV\RPT\SHIPDETLNODATA"
					
					.cSummarySaveAs = 'F:\UTIL\MLAUTOBILLING\REPORTS\KMART FOOTWEAR HANDLING'
					* new version of report modified to support Tilt Residuals (which began coming in on 1/11/2010)
					.cPDFReportFRX = "M:\DEV\RPT\" + .cClient + "DAILYBILLSUMMARY2"  
					.cSummaryOnlySendTo = 'joni.golding@tollgroup.com'
					.cSummaryOnlyCC = 'cbalcezak@fmiint.com, jcampanelli@fmiint.com, mperez1@searshc.com, mark.bennett@tollgroup.com'
					.cAUAD_SQL_Where = " AND RIGHT(DIVISION,2) = '" + KMART_DIV042_DIVISION + "' "
					.cRCRC_SQL_Where = " AND RIGHT(DIVISION,2) = '" + KMART_DIV042_DIVISION + "' "
					.cRDCA_SQL_Where = " AND RIGHT(DIVISION,2) = '" + KMART_DIV042_DIVISION + "' "
					.cRIRI_SQL_Where = " AND RIGHT(DIVISION,2) = '" + KMART_DIV042_DIVISION + "' "
					.cIARN_SQL_Where = " AND RIGHT(DIVISION,2) = '" + KMART_DIV042_DIVISION + "' "
					.lTreatSortType41AsFullCase = .T.
					
				CASE tcClient == "KMART NON-FOOTWEAR"
					.cTNodeFilter = "KM"
					.nAccountID = 5837  && SEARS HOLDINGS
					.nInvoiceWONum = 64
					.lDotComProcessing = .F.
					.lAppendTransactionDetailToPDF = .T.
					.lAttachExcelSummaryFiles = .F.
					*!*	.cBillReceiptDetailReport = "M:\DEV\RPT\SKUBKDN"
					*!*	.cBillReceiptDetailReportNODATA = "M:\DEV\RPT\SKUBKDNNODATA"
					.cBillReceiptDetailReport = "M:\DEV\RPT\MLAUTOBILLRECEIPTDETAIL"
					.cBillReceiptDetailReportNODATA = "M:\DEV\RPT\MLAUTOBILLRECEIPTDETAILNODATA"
					
					.cBillShippedDetailReport = "M:\DEV\RPT\SHIPDETL"
					.cBillShippedDetailReportNODATA = "M:\DEV\RPT\SHIPDETLNODATA"
					
					.cSummarySaveAs = 'F:\UTIL\MLAUTOBILLING\REPORTS\KMART NON-FOOTWEAR HANDLING'
					* new version of report modified to support Tilt Residuals (which began coming in on 1/11/2010)
					.cPDFReportFRX = "M:\DEV\RPT\" + .cClient + "DAILYBILLSUMMARY2"  
					.cSummaryOnlySendTo = 'joni.golding@tollgroup.com'
					.cSummaryOnlyCC = 'cbalcezak@fmiint.com, jcampanelli@fmiint.com, mperez1@searshc.com, mark.bennett@tollgroup.com'
					*!*	.cAUAD_SQL_Where = " AND RIGHT(DIVISION,2) <> '" + KMART_FOOTWEAR_DIVISION + "' "
					*!*	.cRCRC_SQL_Where = " AND RIGHT(DIVISION,2) <> '" + KMART_FOOTWEAR_DIVISION + "' "
					*!*	.cRDCA_SQL_Where = " AND RIGHT(DIVISION,2) <> '" + KMART_FOOTWEAR_DIVISION + "' "
					*!*	.cRIRI_SQL_Where = " AND RIGHT(DIVISION,2) <> '" + KMART_FOOTWEAR_DIVISION + "' "
					*!*	.cIARN_SQL_Where = " AND RIGHT(DIVISION,2) <> '" + KMART_FOOTWEAR_DIVISION + "' "
					*!*	.cAUAD_SQL_Where = " AND RIGHT(DIVISION,2) <> '" + KMART_FOOTWEAR_DIVISION + "' " + " AND RIGHT(DIVISION,2) <> '" + KMART_DIV042_DIVISION + "' "
					*!*	.cRCRC_SQL_Where = " AND RIGHT(DIVISION,2) <> '" + KMART_FOOTWEAR_DIVISION + "' " + " AND RIGHT(DIVISION,2) <> '" + KMART_DIV042_DIVISION + "' "
					*!*	.cRDCA_SQL_Where = " AND RIGHT(DIVISION,2) <> '" + KMART_FOOTWEAR_DIVISION + "' " + " AND RIGHT(DIVISION,2) <> '" + KMART_DIV042_DIVISION + "' "
					*!*	.cRIRI_SQL_Where = " AND RIGHT(DIVISION,2) <> '" + KMART_FOOTWEAR_DIVISION + "' " + " AND RIGHT(DIVISION,2) <> '" + KMART_DIV042_DIVISION + "' "
					*!*	.cIARN_SQL_Where = " AND RIGHT(DIVISION,2) <> '" + KMART_FOOTWEAR_DIVISION + "' " + " AND RIGHT(DIVISION,2) <> '" + KMART_DIV042_DIVISION + "' "

					.cAUAD_SQL_Where = " AND RIGHT(DIVISION,2) = '" + KMART_NON_FOOTWEAR_DIVISION + "' "
					.cRCRC_SQL_Where = " AND RIGHT(DIVISION,2) = '" + KMART_NON_FOOTWEAR_DIVISION + "' "
					.cRDCA_SQL_Where = " AND RIGHT(DIVISION,2) = '" + KMART_NON_FOOTWEAR_DIVISION + "' "
					.cRIRI_SQL_Where = " AND RIGHT(DIVISION,2) = '" + KMART_NON_FOOTWEAR_DIVISION + "' "
					.cIARN_SQL_Where = " AND RIGHT(DIVISION,2) = '" + KMART_NON_FOOTWEAR_DIVISION + "' "

					.lTreatSortType41AsFullCase = .T.
					
				CASE tcClient == "DSG"
					.cTNodeFilter = "BD"
					.nAccountID = 5888  && Dean Street Group LLC
					.nInvoiceWONum = 62
					.lDotComProcessing = .F.
					.lAppendTransactionDetailToPDF = .T.
					.lAttachExcelSummaryFiles = .F.
					.cBillReceiptDetailReport = "M:\DEV\RPT\MLAUTOBILLRECEIPTDETAIL"
					.cBillReceiptDetailReportNODATA = "M:\DEV\RPT\MLAUTOBILLRECEIPTDETAILNODATA"
					
					.cBillShippedDetailReport = "M:\DEV\RPT\SHIPDETL"
					.cBillShippedDetailReportNODATA = "M:\DEV\RPT\SHIPDETLNODATA"
					
					.cSummarySaveAs = 'F:\UTIL\MLAUTOBILLING\REPORTS\DSG HANDLING'
					.cSummaryOnlySendTo = 'joni.golding@tollgroup.com'
					.cSummaryOnlyCC = 'cbalcezak@fmiint.com, jcampanelli@fmiint.com, mark.bennett@tollgroup.com'
					.cAUAD_SQL_Where = ''
					.cRCRC_SQL_Where = ''
					.cRDCA_SQL_Where = ''
					.cRIRI_SQL_Where = ''
					.cIARN_SQL_Where = ''
					.lTreatSortType41AsFullCase = .F.
					
				CASE tcClient == "FOOTLOCKER"
					.cTNodeFilter = "FL"
					.nAccountID = 5943  
					.nInvoiceWONum = 63
					.lDotComProcessing = .F.
					.lAppendTransactionDetailToPDF = .T.
					.lAttachExcelSummaryFiles = .F.
					.cBillReceiptDetailReport = "M:\DEV\RPT\MLAUTOBILLRECEIPTDETAIL"
					.cBillReceiptDetailReportNODATA = "M:\DEV\RPT\MLAUTOBILLRECEIPTDETAILNODATA"
					
					.cBillShippedDetailReport = "M:\DEV\RPT\SHIPDETL"
					.cBillShippedDetailReportNODATA = "M:\DEV\RPT\SHIPDETLNODATA"
					
					.cSummarySaveAs = 'F:\UTIL\MLAUTOBILLING\REPORTS\FOOTLOCKER HANDLING'
					.cSummaryOnlySendTo = 'RFranco@fmiint.com'
					.cSummaryOnlyCC = 'cbalcezak@fmiint.com, jcampanelli@fmiint.com, mark.bennett@tollgroup.com'
					.cAUAD_SQL_Where = ''
					.cRCRC_SQL_Where = ''
					.cRDCA_SQL_Where = ''
					.cRIRI_SQL_Where = ''
					.cIARN_SQL_Where = ''
					.lTreatSortType41AsFullCase = .F.
					
				CASE tcClient == "SEARS ALL"  
					* this is used in the MTD handling report
					.cTNodeFilter = "SR"
					.nAccountID = 5837
					.nInvoiceWONum = 66
					.lDotComProcessing = .F.
					.lAppendTransactionDetailToPDF = .T.
					.lAttachExcelSummaryFiles = .F.
					.cBillReceiptDetailReport = "M:\DEV\RPT\MLAUTOBILLRECEIPTDETAIL"
					.cBillReceiptDetailReportNODATA = "M:\DEV\RPT\MLAUTOBILLRECEIPTDETAILNODATA"
					
					.cBillShippedDetailReport = "M:\DEV\RPT\SHIPDETL"
					.cBillShippedDetailReportNODATA = "M:\DEV\RPT\SHIPDETLNODATA"
					
					.cSummarySaveAs = 'F:\UTIL\MLAUTOBILLING\REPORTS\SEARS HANDLING'
					*.cPDFReportFRX = "M:\DEV\RPT\SEARSDAILYBILLSUMMARYNOCAPEX" 
					*.cPDFReportFRX = "M:\DEV\RPT\SEARSDAILYBILLSUMMARYTRANSFER" 
					.cPDFReportFRX = "M:\DEV\RPT\SEARSDAILYBILLSUMMARYTRANSFER2" 
					.cSummaryOnlySendTo = 'joni.golding@tollgroup.com'
					.cSummaryOnlyCC = 'cbalcezak@fmiint.com, jcampanelli@fmiint.com, mperez1@searshc.com, mark.bennett@tollgroup.com'
					*!*	.cAUAD_SQL_Where = ''
					*!*	.cRCRC_SQL_Where = ''
					*!*	.cRDCA_SQL_Where = ''
					*!*	.cRIRI_SQL_Where = ''

					lcDivString = ;
						"'" + SEARS_MENS_DIVISION + "'," + ;
						"'" + SEARS_WOMENS_DIVISION + "'," + ; 
						"'" + SEARS_KIDS_DIVISION + "'," + ; 
						"'" + SEARS_ATHLETIC_DIVISION + "'," + ; 
						"'" + SEARS_TOWELS_DIVISION + "'," + ; 
						"'" + SEARS_JEANS_DIVISION + "'," + ; 
						"'" + SEARS_BELTS_DIVISION + "'," + ; 
						"'" + SEARS_KIDS_JEANS_DIVISION + "'," + ; 
						"'" + SEARS_BRAS_DIVISION + "'," + ; 
						"'" + SEARS_PILLOWS_DIVISION + "'," + ; 
						"'" + SEARS_FRAGRANCE_DIVISION + "'," + ; 
						"'" + SEARS_LANDS_END_DIVISION + "'"

					.cAUAD_SQL_Where = " AND INLIST(RIGHT(DIVISION,2)," +  lcDivString + ") "
					.cRCRC_SQL_Where = " AND INLIST(RIGHT(DIVISION,2)," +  lcDivString + ") "
					.cRDCA_SQL_Where = ''
					.cRIRI_SQL_Where = " AND INLIST(RIGHT(DIVISION,2)," +  lcDivString + ") "
					.cIARN_SQL_Where = " AND INLIST(RIGHT(DIVISION,2)," +  lcDivString + ") "
					.lTreatSortType41AsFullCase = .T.
					
				CASE tcClient == "SEARS FRAGRANCE"  
					.cTNodeFilter = "SR"
					.nAccountID = 5837
					.nInvoiceWONum = 66
					.lDotComProcessing = .F.
					.lAppendTransactionDetailToPDF = .T.
					.lAttachExcelSummaryFiles = .F.
					.cBillReceiptDetailReport = "M:\DEV\RPT\MLAUTOBILLRECEIPTDETAIL"
					.cBillReceiptDetailReportNODATA = "M:\DEV\RPT\MLAUTOBILLRECEIPTDETAILNODATA"
					
					.cBillShippedDetailReport = "M:\DEV\RPT\SHIPDETL"
					.cBillShippedDetailReportNODATA = "M:\DEV\RPT\SHIPDETLNODATA"
					
					.cSummarySaveAs = 'F:\UTIL\MLAUTOBILLING\REPORTS\SEARS HANDLING'
					*.cPDFReportFRX = "M:\DEV\RPT\SEARSDAILYBILLSUMMARYNOCAPEX" 
					*.cPDFReportFRX = "M:\DEV\RPT\SEARSDAILYBILLSUMMARYTRANSFER" 
					.cPDFReportFRX = "M:\DEV\RPT\SEARSDAILYBILLSUMMARYTRANSFER2" 
					.cSummaryOnlySendTo = 'joni.golding@tollgroup.com'
					.cSummaryOnlyCC = 'cbalcezak@fmiint.com, jcampanelli@fmiint.com, mperez1@searshc.com, mark.bennett@tollgroup.com'
					.cAUAD_SQL_Where = " AND RIGHT(DIVISION,2) = '" + SEARS_FRAGRANCE_DIVISION + "' "
					.cRCRC_SQL_Where = " AND RIGHT(DIVISION,2) = '" + SEARS_FRAGRANCE_DIVISION + "' "
					*.cRDCA_SQL_Where = " AND RIGHT(DIVISION,2) = '" + SEARS_FRAGRANCE_DIVISION + "' "
					.cRDCA_SQL_Where = ''
					.cRIRI_SQL_Where = " AND RIGHT(DIVISION,2) = '" + SEARS_FRAGRANCE_DIVISION + "' "
					.cIARN_SQL_Where = " AND RIGHT(DIVISION,2) = '" + SEARS_FRAGRANCE_DIVISION + "' "
					.lTreatSortType41AsFullCase = .T.
					
				CASE tcClient == "SEARS MENS"  
					.cTNodeFilter = "SR"
					.nAccountID = 5837
					.nInvoiceWONum = 66
					.lDotComProcessing = .F.
					.lAppendTransactionDetailToPDF = .T.
					.lAttachExcelSummaryFiles = .F.
					.cBillReceiptDetailReport = "M:\DEV\RPT\MLAUTOBILLRECEIPTDETAIL"
					.cBillReceiptDetailReportNODATA = "M:\DEV\RPT\MLAUTOBILLRECEIPTDETAILNODATA"
					
					.cBillShippedDetailReport = "M:\DEV\RPT\SHIPDETL"
					.cBillShippedDetailReportNODATA = "M:\DEV\RPT\SHIPDETLNODATA"
					
					.cSummarySaveAs = 'F:\UTIL\MLAUTOBILLING\REPORTS\SEARS HANDLING'
					*.cPDFReportFRX = "M:\DEV\RPT\SEARSDAILYBILLSUMMARYNOCAPEX" 
					*.cPDFReportFRX = "M:\DEV\RPT\SEARSDAILYBILLSUMMARYTRANSFER" 
					.cPDFReportFRX = "M:\DEV\RPT\SEARSDAILYBILLSUMMARYTRANSFER2" 
					.cSummaryOnlySendTo = 'joni.golding@tollgroup.com'
					.cSummaryOnlyCC = 'cbalcezak@fmiint.com, jcampanelli@fmiint.com, mperez1@searshc.com, mark.bennett@tollgroup.com'
					.cAUAD_SQL_Where = " AND RIGHT(DIVISION,2) = '" + SEARS_MENS_DIVISION + "' "
					.cRCRC_SQL_Where = " AND RIGHT(DIVISION,2) = '" + SEARS_MENS_DIVISION + "' "
					*.cRDCA_SQL_Where = " AND RIGHT(DIVISION,2) = '" + SEARS_MENS_DIVISION + "' "
					.cRDCA_SQL_Where = ''
					.cRIRI_SQL_Where = " AND RIGHT(DIVISION,2) = '" + SEARS_MENS_DIVISION + "' "
					.cIARN_SQL_Where = " AND RIGHT(DIVISION,2) = '" + SEARS_MENS_DIVISION + "' "
					.lTreatSortType41AsFullCase = .T.
					
				CASE tcClient == "SEARS WOMENS"  
					.cTNodeFilter = "SR"
					.nAccountID = 5837
					.nInvoiceWONum = 66
					.lDotComProcessing = .F.
					.lAppendTransactionDetailToPDF = .T.
					.lAttachExcelSummaryFiles = .F.
					.cBillReceiptDetailReport = "M:\DEV\RPT\MLAUTOBILLRECEIPTDETAIL"
					.cBillReceiptDetailReportNODATA = "M:\DEV\RPT\MLAUTOBILLRECEIPTDETAILNODATA"
					
					.cBillShippedDetailReport = "M:\DEV\RPT\SHIPDETL"
					.cBillShippedDetailReportNODATA = "M:\DEV\RPT\SHIPDETLNODATA"
					
					.cSummarySaveAs = 'F:\UTIL\MLAUTOBILLING\REPORTS\SEARS HANDLING'
					*.cPDFReportFRX = "M:\DEV\RPT\SEARSDAILYBILLSUMMARYNOCAPEX" 
					*.cPDFReportFRX = "M:\DEV\RPT\SEARSDAILYBILLSUMMARYTRANSFER" 
					.cPDFReportFRX = "M:\DEV\RPT\SEARSDAILYBILLSUMMARYTRANSFER2" 
					.cSummaryOnlySendTo = 'joni.golding@tollgroup.com'
					.cSummaryOnlyCC = 'cbalcezak@fmiint.com, jcampanelli@fmiint.com, mperez1@searshc.com, mark.bennett@tollgroup.com'
					.cAUAD_SQL_Where = " AND RIGHT(DIVISION,2) = '" + SEARS_WOMENS_DIVISION + "' "
					.cRCRC_SQL_Where = " AND RIGHT(DIVISION,2) = '" + SEARS_WOMENS_DIVISION + "' "
					*.cRDCA_SQL_Where = " AND RIGHT(DIVISION,2) = '" + SEARS_WOMENS_DIVISION + "' "
					.cRDCA_SQL_Where = ''
					.cRIRI_SQL_Where = " AND RIGHT(DIVISION,2) = '" + SEARS_WOMENS_DIVISION + "' "
					.cIARN_SQL_Where = " AND RIGHT(DIVISION,2) = '" + SEARS_WOMENS_DIVISION + "' "
					.lTreatSortType41AsFullCase = .T.
					
				CASE tcClient == "SEARS KIDS"  
					.cTNodeFilter = "SR"
					.nAccountID = 5837
					.nInvoiceWONum = 66
					.lDotComProcessing = .F.
					.lAppendTransactionDetailToPDF = .T.
					.lAttachExcelSummaryFiles = .F.
					.cBillReceiptDetailReport = "M:\DEV\RPT\MLAUTOBILLRECEIPTDETAIL"
					.cBillReceiptDetailReportNODATA = "M:\DEV\RPT\MLAUTOBILLRECEIPTDETAILNODATA"
					
					.cBillShippedDetailReport = "M:\DEV\RPT\SHIPDETL"
					.cBillShippedDetailReportNODATA = "M:\DEV\RPT\SHIPDETLNODATA"
					
					.cSummarySaveAs = 'F:\UTIL\MLAUTOBILLING\REPORTS\SEARS HANDLING'
					*.cPDFReportFRX = "M:\DEV\RPT\SEARSDAILYBILLSUMMARYNOCAPEX" 
					*.cPDFReportFRX = "M:\DEV\RPT\SEARSDAILYBILLSUMMARYTRANSFER" 
					.cPDFReportFRX = "M:\DEV\RPT\SEARSDAILYBILLSUMMARYTRANSFER2" 
					.cSummaryOnlySendTo = 'joni.golding@tollgroup.com'
					.cSummaryOnlyCC = 'cbalcezak@fmiint.com, jcampanelli@fmiint.com, mperez1@searshc.com, mark.bennett@tollgroup.com'
					.cAUAD_SQL_Where = " AND RIGHT(DIVISION,2) = '" + SEARS_KIDS_DIVISION + "' "
					.cRCRC_SQL_Where = " AND RIGHT(DIVISION,2) = '" + SEARS_KIDS_DIVISION + "' "
					*.cRDCA_SQL_Where = " AND RIGHT(DIVISION,2) = '" + SEARS_KIDS_DIVISION + "' "
					.cRDCA_SQL_Where = ''
					.cRIRI_SQL_Where = " AND RIGHT(DIVISION,2) = '" + SEARS_KIDS_DIVISION + "' "
					.cIARN_SQL_Where = " AND RIGHT(DIVISION,2) = '" + SEARS_KIDS_DIVISION + "' "
					.lTreatSortType41AsFullCase = .T.
					
				CASE tcClient == "SEARS ATHLETIC"  
					.cTNodeFilter = "SR"
					.nAccountID = 5837
					.nInvoiceWONum = 66
					.lDotComProcessing = .F.
					.lAppendTransactionDetailToPDF = .T.
					.lAttachExcelSummaryFiles = .F.
					.cBillReceiptDetailReport = "M:\DEV\RPT\MLAUTOBILLRECEIPTDETAIL"
					.cBillReceiptDetailReportNODATA = "M:\DEV\RPT\MLAUTOBILLRECEIPTDETAILNODATA"
					
					.cBillShippedDetailReport = "M:\DEV\RPT\SHIPDETL"
					.cBillShippedDetailReportNODATA = "M:\DEV\RPT\SHIPDETLNODATA"
					
					.cSummarySaveAs = 'F:\UTIL\MLAUTOBILLING\REPORTS\SEARS HANDLING'
					*.cPDFReportFRX = "M:\DEV\RPT\SEARSDAILYBILLSUMMARYNOCAPEX" 
					*.cPDFReportFRX = "M:\DEV\RPT\SEARSDAILYBILLSUMMARYTRANSFER" 
					.cPDFReportFRX = "M:\DEV\RPT\SEARSDAILYBILLSUMMARYTRANSFER2" 
					.cSummaryOnlySendTo = 'joni.golding@tollgroup.com'
					.cSummaryOnlyCC = 'cbalcezak@fmiint.com, jcampanelli@fmiint.com, mperez1@searshc.com, mark.bennett@tollgroup.com'
					.cAUAD_SQL_Where = " AND RIGHT(DIVISION,2) = '" + SEARS_ATHLETIC_DIVISION + "' "
					.cRCRC_SQL_Where = " AND RIGHT(DIVISION,2) = '" + SEARS_ATHLETIC_DIVISION + "' "
					*.cRDCA_SQL_Where = " AND RIGHT(DIVISION,2) = '" + SEARS_ATHLETIC_DIVISION + "' "
					.cRDCA_SQL_Where = ''
					.cRIRI_SQL_Where = " AND RIGHT(DIVISION,2) = '" + SEARS_ATHLETIC_DIVISION + "' "
					.cIARN_SQL_Where = " AND RIGHT(DIVISION,2) = '" + SEARS_ATHLETIC_DIVISION + "' "
					.lTreatSortType41AsFullCase = .T.
					
				CASE tcClient == "SEARS LANDS END"  
					.cTNodeFilter = "SR"
					.nAccountID = 5837
					.nInvoiceWONum = 66
					.lDotComProcessing = .F.
					.lAppendTransactionDetailToPDF = .T.
					.lAttachExcelSummaryFiles = .F.
					.cBillReceiptDetailReport = "M:\DEV\RPT\MLAUTOBILLRECEIPTDETAIL"
					.cBillReceiptDetailReportNODATA = "M:\DEV\RPT\MLAUTOBILLRECEIPTDETAILNODATA"
					
					.cBillShippedDetailReport = "M:\DEV\RPT\SHIPDETL"
					.cBillShippedDetailReportNODATA = "M:\DEV\RPT\SHIPDETLNODATA"
					
					.cSummarySaveAs = 'F:\UTIL\MLAUTOBILLING\REPORTS\SEARS HANDLING'
					*.cPDFReportFRX = "M:\DEV\RPT\SEARSDAILYBILLSUMMARYNOCAPEX" 
					*.cPDFReportFRX = "M:\DEV\RPT\SEARSDAILYBILLSUMMARYTRANSFER" 
					.cPDFReportFRX = "M:\DEV\RPT\SEARSDAILYBILLSUMMARYTRANSFER2" 
					.cSummaryOnlySendTo = 'joni.golding@tollgroup.com'
					.cSummaryOnlyCC = 'cbalcezak@fmiint.com, jcampanelli@fmiint.com, mperez1@searshc.com, mark.bennett@tollgroup.com'
					.cAUAD_SQL_Where = " AND RIGHT(DIVISION,2) = '" + SEARS_LANDS_END_DIVISION + "' "
					.cRCRC_SQL_Where = " AND RIGHT(DIVISION,2) = '" + SEARS_LANDS_END_DIVISION + "' "
					*.cRDCA_SQL_Where = " AND RIGHT(DIVISION,2) = '" + SEARS_LANDS_END_DIVISION + "' "
					.cRDCA_SQL_Where = ''
					.cRIRI_SQL_Where = " AND RIGHT(DIVISION,2) = '" + SEARS_LANDS_END_DIVISION + "' "
					.cIARN_SQL_Where = " AND RIGHT(DIVISION,2) = '" + SEARS_LANDS_END_DIVISION + "' "
					.lTreatSortType41AsFullCase = .T.
					
				CASE tcClient == "SEARS TOWELS"  
					.cTNodeFilter = "SR"
					.nAccountID = 5837
					.nInvoiceWONum = 66
					.lDotComProcessing = .F.
					.lAppendTransactionDetailToPDF = .T.
					.lAttachExcelSummaryFiles = .F.
					.cBillReceiptDetailReport = "M:\DEV\RPT\MLAUTOBILLRECEIPTDETAIL"
					.cBillReceiptDetailReportNODATA = "M:\DEV\RPT\MLAUTOBILLRECEIPTDETAILNODATA"
					
					.cBillShippedDetailReport = "M:\DEV\RPT\SHIPDETL"
					.cBillShippedDetailReportNODATA = "M:\DEV\RPT\SHIPDETLNODATA"
					
					.cSummarySaveAs = 'F:\UTIL\MLAUTOBILLING\REPORTS\SEARS HANDLING'
					*.cPDFReportFRX = "M:\DEV\RPT\SEARSDAILYBILLSUMMARYNOCAPEX" 
					*.cPDFReportFRX = "M:\DEV\RPT\SEARSDAILYBILLSUMMARYTRANSFER" 
					*.cPDFReportFRX = "M:\DEV\RPT\SEARSDAILYBILLSUMMARYMANUALUNITS" 
					.cPDFReportFRX = "M:\DEV\RPT\SEARSDAILYBILLSUMMARYTRANSFER2" 
					.cSummaryOnlySendTo = 'joni.golding@tollgroup.com'
					.cSummaryOnlyCC = 'cbalcezak@fmiint.com, jcampanelli@fmiint.com, mperez1@searshc.com, mark.bennett@tollgroup.com'
					
*!*						.cAUAD_SQL_Where = " AND RIGHT(DIVISION,2) = '" + SEARS_TOWELS_DIVISION + "' "
*!*						.cRCRC_SQL_Where = " AND RIGHT(DIVISION,2) = '" + SEARS_TOWELS_DIVISION + "' "
*!*						*.cRDCA_SQL_Where = " AND RIGHT(DIVISION,2) = '" + SEARS_TOWELS_DIVISION + "' "
*!*						.cRDCA_SQL_Where = ''
*!*						.cRIRI_SQL_Where = " AND RIGHT(DIVISION,2) = '" + SEARS_TOWELS_DIVISION + "' "
*!*						.cIARN_SQL_Where = " AND RIGHT(DIVISION,2) = '" + SEARS_TOWELS_DIVISION + "' "
					
					
					* adding Sears Pillows div 024 to the Towels billing, per Doug 8/4/14.
					lcDivString = ;
						"'" + SEARS_TOWELS_DIVISION + "'," + ; 
						"'" + SEARS_PILLOWS_DIVISION + "'" 

					.cAUAD_SQL_Where = " AND INLIST(RIGHT(DIVISION,2)," +  lcDivString + ") "
					.cRCRC_SQL_Where = " AND INLIST(RIGHT(DIVISION,2)," +  lcDivString + ") "
					.cRDCA_SQL_Where = ''
					.cRIRI_SQL_Where = " AND INLIST(RIGHT(DIVISION,2)," +  lcDivString + ") "
					.cIARN_SQL_Where = " AND INLIST(RIGHT(DIVISION,2)," +  lcDivString + ") "
					
					
					.lTreatSortType41AsFullCase = .T.
					
				CASE tcClient == "SEARS JEANS"  
					.cTNodeFilter = "SR"
					.nAccountID = 5837
					.nInvoiceWONum = 66
					.lDotComProcessing = .F.
					.lAppendTransactionDetailToPDF = .T.
					.lAttachExcelSummaryFiles = .F.
					.cBillReceiptDetailReport = "M:\DEV\RPT\MLAUTOBILLRECEIPTDETAIL"
					.cBillReceiptDetailReportNODATA = "M:\DEV\RPT\MLAUTOBILLRECEIPTDETAILNODATA"
					
					.cBillShippedDetailReport = "M:\DEV\RPT\SHIPDETL"
					.cBillShippedDetailReportNODATA = "M:\DEV\RPT\SHIPDETLNODATA"
					
					.cSummarySaveAs = 'F:\UTIL\MLAUTOBILLING\REPORTS\SEARS HANDLING'
					*.cPDFReportFRX = "M:\DEV\RPT\SEARSDAILYBILLSUMMARYNOCAPEX" 
					*.cPDFReportFRX = "M:\DEV\RPT\SEARSDAILYBILLSUMMARYTRANSFER" 
					*.cPDFReportFRX = "M:\DEV\RPT\SEARSDAILYBILLSUMMARYMANUALUNITS" 
					.cPDFReportFRX = "M:\DEV\RPT\SEARSDAILYBILLSUMMARYTRANSFER2" 
					.cSummaryOnlySendTo = 'joni.golding@tollgroup.com'
					.cSummaryOnlyCC = 'cbalcezak@fmiint.com, jcampanelli@fmiint.com, mperez1@searshc.com, mark.bennett@tollgroup.com'
					.cAUAD_SQL_Where = " AND RIGHT(DIVISION,2) = '" + SEARS_JEANS_DIVISION + "' "
					.cRCRC_SQL_Where = " AND RIGHT(DIVISION,2) = '" + SEARS_JEANS_DIVISION + "' "
					*.cRDCA_SQL_Where = " AND RIGHT(DIVISION,2) = '" + SEARS_JEANS_DIVISION + "' "
					.cRDCA_SQL_Where = ''
					.cRIRI_SQL_Where = " AND RIGHT(DIVISION,2) = '" + SEARS_JEANS_DIVISION + "' "
					.cIARN_SQL_Where = " AND RIGHT(DIVISION,2) = '" + SEARS_JEANS_DIVISION + "' "
					.lTreatSortType41AsFullCase = .T.
					
				CASE tcClient == "SEARS BELTS"  
					.cTNodeFilter = "SR"
					.nAccountID = 5837
					.nInvoiceWONum = 66
					.lDotComProcessing = .F.
					.lAppendTransactionDetailToPDF = .T.
					.lAttachExcelSummaryFiles = .F.
					.cBillReceiptDetailReport = "M:\DEV\RPT\MLAUTOBILLRECEIPTDETAIL"
					.cBillReceiptDetailReportNODATA = "M:\DEV\RPT\MLAUTOBILLRECEIPTDETAILNODATA"
					
					.cBillShippedDetailReport = "M:\DEV\RPT\SHIPDETL"
					.cBillShippedDetailReportNODATA = "M:\DEV\RPT\SHIPDETLNODATA"
					
					.cSummarySaveAs = 'F:\UTIL\MLAUTOBILLING\REPORTS\SEARS HANDLING'
					.cPDFReportFRX = "M:\DEV\RPT\SEARSDAILYBILLSUMMARYTRANSFER2" 
					.cSummaryOnlySendTo = 'joni.golding@tollgroup.com'
					.cSummaryOnlyCC = 'cbalcezak@fmiint.com, jcampanelli@fmiint.com, mperez1@searshc.com, mark.bennett@tollgroup.com'
					.cAUAD_SQL_Where = " AND RIGHT(DIVISION,2) = '" + SEARS_BELTS_DIVISION + "' "
					.cRCRC_SQL_Where = " AND RIGHT(DIVISION,2) = '" + SEARS_BELTS_DIVISION + "' "
					.cRDCA_SQL_Where = ''
					.cRIRI_SQL_Where = " AND RIGHT(DIVISION,2) = '" + SEARS_BELTS_DIVISION + "' "
					.cIARN_SQL_Where = " AND RIGHT(DIVISION,2) = '" + SEARS_BELTS_DIVISION + "' "
					.lTreatSortType41AsFullCase = .T.
					
				CASE tcClient == "SEARS BRAS"  
					.cTNodeFilter = "SR"
					.nAccountID = 5837
					.nInvoiceWONum = 66
					.lDotComProcessing = .F.
					.lAppendTransactionDetailToPDF = .T.
					.lAttachExcelSummaryFiles = .F.
					.cBillReceiptDetailReport = "M:\DEV\RPT\MLAUTOBILLRECEIPTDETAIL"
					.cBillReceiptDetailReportNODATA = "M:\DEV\RPT\MLAUTOBILLRECEIPTDETAILNODATA"
					
					.cBillShippedDetailReport = "M:\DEV\RPT\SHIPDETL"
					.cBillShippedDetailReportNODATA = "M:\DEV\RPT\SHIPDETLNODATA"
					
					.cSummarySaveAs = 'F:\UTIL\MLAUTOBILLING\REPORTS\SEARS HANDLING'
					.cPDFReportFRX = "M:\DEV\RPT\SEARSDAILYBILLSUMMARYTRANSFER2" 
					.cSummaryOnlySendTo = 'joni.golding@tollgroup.com'
					.cSummaryOnlyCC = 'cbalcezak@fmiint.com, jcampanelli@fmiint.com, mperez1@searshc.com, mark.bennett@tollgroup.com'
					.cAUAD_SQL_Where = " AND RIGHT(DIVISION,2) = '" + SEARS_BRAS_DIVISION + "' "
					.cRCRC_SQL_Where = " AND RIGHT(DIVISION,2) = '" + SEARS_BRAS_DIVISION + "' "
					.cRDCA_SQL_Where = ''
					.cRIRI_SQL_Where = " AND RIGHT(DIVISION,2) = '" + SEARS_BRAS_DIVISION + "' "
					.cIARN_SQL_Where = " AND RIGHT(DIVISION,2) = '" + SEARS_BRAS_DIVISION + "' "
					.lTreatSortType41AsFullCase = .T.
					
				CASE tcClient == "SEARS KIDS JEANS"  
					.cTNodeFilter = "SR"
					.nAccountID = 5837
					.nInvoiceWONum = 66
					.lDotComProcessing = .F.
					.lAppendTransactionDetailToPDF = .T.
					.lAttachExcelSummaryFiles = .F.
					.cBillReceiptDetailReport = "M:\DEV\RPT\MLAUTOBILLRECEIPTDETAIL"
					.cBillReceiptDetailReportNODATA = "M:\DEV\RPT\MLAUTOBILLRECEIPTDETAILNODATA"
					
					.cBillShippedDetailReport = "M:\DEV\RPT\SHIPDETL"
					.cBillShippedDetailReportNODATA = "M:\DEV\RPT\SHIPDETLNODATA"
					
					.cSummarySaveAs = 'F:\UTIL\MLAUTOBILLING\REPORTS\SEARS HANDLING'
					.cPDFReportFRX = "M:\DEV\RPT\SEARSDAILYBILLSUMMARYTRANSFER2" 
					.cSummaryOnlySendTo = 'joni.golding@tollgroup.com'
					.cSummaryOnlyCC = 'cbalcezak@fmiint.com, jcampanelli@fmiint.com, mperez1@searshc.com, mark.bennett@tollgroup.com'
					.cAUAD_SQL_Where = " AND RIGHT(DIVISION,2) = '" + SEARS_KIDS_JEANS_DIVISION + "' "
					.cRCRC_SQL_Where = " AND RIGHT(DIVISION,2) = '" + SEARS_KIDS_JEANS_DIVISION + "' "
					.cRDCA_SQL_Where = ''
					.cRIRI_SQL_Where = " AND RIGHT(DIVISION,2) = '" + SEARS_KIDS_JEANS_DIVISION + "' "
					.cIARN_SQL_Where = " AND RIGHT(DIVISION,2) = '" + SEARS_KIDS_JEANS_DIVISION + "' "
					.lTreatSortType41AsFullCase = .T.
					
				CASE tcClient == "SEARS OTHER"  
				
					.cTNodeFilter = "SR"
					.nAccountID = 5837
					.nInvoiceWONum = 66
					.lDotComProcessing = .F.
					.lAppendTransactionDetailToPDF = .T.
					.lAttachExcelSummaryFiles = .F.
					.cBillReceiptDetailReport = "M:\DEV\RPT\MLAUTOBILLRECEIPTDETAIL"
					.cBillReceiptDetailReportNODATA = "M:\DEV\RPT\MLAUTOBILLRECEIPTDETAILNODATA"
					
					.cBillShippedDetailReport = "M:\DEV\RPT\SHIPDETL"
					.cBillShippedDetailReportNODATA = "M:\DEV\RPT\SHIPDETLNODATA"
					
					.cSummarySaveAs = 'F:\UTIL\MLAUTOBILLING\REPORTS\SEARS HANDLING'
					*.cPDFReportFRX = "M:\DEV\RPT\SEARSDAILYBILLSUMMARYNOCAPEX" 
					*.cPDFReportFRX = "M:\DEV\RPT\SEARSDAILYBILLSUMMARYTRANSFER" 
					.cPDFReportFRX = "M:\DEV\RPT\SEARSDAILYBILLSUMMARYTRANSFER2" 
					.cSummaryOnlySendTo = 'joni.golding@tollgroup.com'
					.cSummaryOnlyCC = 'cbalcezak@fmiint.com, jcampanelli@fmiint.com, mperez1@searshc.com, mark.bennett@tollgroup.com'

					lcDivString = ;
						"'" + SEARS_MENS_DIVISION + "'," + ;
						"'" + SEARS_WOMENS_DIVISION + "'," + ; 
						"'" + SEARS_KIDS_DIVISION + "'," + ; 
						"'" + SEARS_ATHLETIC_DIVISION + "'," + ; 
						"'" + SEARS_TOWELS_DIVISION + "'," + ; 
						"'" + SEARS_JEANS_DIVISION + "'," + ; 
						"'" + SEARS_BELTS_DIVISION + "'," + ; 
						"'" + SEARS_KIDS_JEANS_DIVISION + "'," + ; 
						"'" + SEARS_BRAS_DIVISION + "'," + ; 
						"'" + SEARS_PILLOWS_DIVISION + "'," + ; 
						"'" + SEARS_FRAGRANCE_DIVISION + "'," + ; 
						"'" + SEARS_LANDS_END_DIVISION + "'"

					.cAUAD_SQL_Where = " AND (NOT INLIST(RIGHT(DIVISION,2)," +  lcDivString + ")) "
					.cRCRC_SQL_Where = " AND (NOT INLIST(RIGHT(DIVISION,2)," +  lcDivString + ")) "
					.cRDCA_SQL_Where = ''
					.cRIRI_SQL_Where = " AND (NOT INLIST(RIGHT(DIVISION,2)," +  lcDivString + ")) "
					.cIARN_SQL_Where = " AND (NOT INLIST(RIGHT(DIVISION,2)," +  lcDivString + ")) "
					.lTreatSortType41AsFullCase = .T.					
					
				CASE tcClient == "RACK ROOM CHAS"  
				
					.cInvDetCompany = 'R'  && per Darren 12/07/2015
					.cTNodeFilter = "RR"
					.nAccountID = 6550  &&  re-verify before invoicing!
					.nInvoiceWONum = 67
					.lDotComProcessing = .F.
					.lAppendTransactionDetailToPDF = .T.
					.lAttachExcelSummaryFiles = .F.
					
					.lFilterCURRCNsByDiv = .T.
					
					.lRackRoomProcessing = .T.
					
					.cBillReceiptDetailReport = "M:\DEV\RPT\MLAUTO_RR_BILLRECEIPTDETAIL2"
					.cBillReceiptDetailReportNODATA = "M:\DEV\RPT\MLAUTOBILLRECEIPTDETAILNODATA"
					
					.cBillShippedDetailReport = "M:\DEV\RPT\SHIPDETL_RR2"
					.cBillShippedDetailReportNODATA = "M:\DEV\RPT\SHIPDETLNODATA"
					
					.cSummarySaveAs = 'F:\UTIL\MLAUTOBILLING\REPORTS\RACKROOMHANDLING'
					.cPDFReportFRX = "M:\DEV\RPT\RACKROOMDAILYBILLSUMMARY" 
					
					.cPDFWeeklyReportFRX = "M:\DEV\RPT\RACKROOMWEEKLYBILLSUMMARY" 
					
					.cSummaryOnlySendTo = 'joni.golding@tollgroup.com, joana.burke@tollgroup.com, juan.Perez@tollgroup.com, Juan.Hernandez@tollgroup.com'
					.cSummaryOnlyCC = 'cbalcezak@fmiint.com, jcampanelli@fmiint.com, mark.bennett@tollgroup.com'
					
					.cAUAD_SQL_Where = " AND DIVISION == '" + RACKROOM_CHAS_DIVISION + "' "
					.cRCRC_SQL_Where = " AND DIVISION == '" + RACKROOM_CHAS_DIVISION + "' "
					.cRDCA_SQL_Where = ''
					.cRIRI_SQL_Where = " AND DIVISION == '" + RACKROOM_CHAS_DIVISION + "' "
					.cIARN_SQL_Where = " AND DIVISION == '" + RACKROOM_CHAS_DIVISION + "' "
					
					.lTreatSortType41AsFullCase = .T.
					
				CASE tcClient == "RACK ROOM FOOTWEAR"  
				
					.cInvDetCompany = 'R'  && per Darren 12/07/2015
					.cTNodeFilter = "RR"
					.nAccountID = 6550  &&  re-verify before invoicing!
					.nInvoiceWONum = 67
					.lDotComProcessing = .F.
					.lAppendTransactionDetailToPDF = .T.
					.lAttachExcelSummaryFiles = .F.
					
					.lFilterCURRCNsByDiv = .T.
					
					.lRackRoomProcessing = .T.
					
					.cBillReceiptDetailReport = "M:\DEV\RPT\MLAUTO_RR_BILLRECEIPTDETAIL2"
					.cBillReceiptDetailReportNODATA = "M:\DEV\RPT\MLAUTOBILLRECEIPTDETAILNODATA"
					
					.cBillShippedDetailReport = "M:\DEV\RPT\SHIPDETL_RR2"
					.cBillShippedDetailReportNODATA = "M:\DEV\RPT\SHIPDETLNODATA"
					
					.cSummarySaveAs = 'F:\UTIL\MLAUTOBILLING\REPORTS\RACKROOMHANDLING'
					.cPDFReportFRX = "M:\DEV\RPT\RACKROOMDAILYBILLSUMMARY" 
					
					.cPDFWeeklyReportFRX = "M:\DEV\RPT\RACKROOMWEEKLYBILLSUMMARY" 
					
					.cSummaryOnlySendTo = 'joni.golding@tollgroup.com, joana.burke@tollgroup.com, juan.Perez@tollgroup.com, Juan.Hernandez@tollgroup.com'
					.cSummaryOnlyCC = 'cbalcezak@fmiint.com, jcampanelli@fmiint.com, mark.bennett@tollgroup.com'
					
					.cAUAD_SQL_Where = " AND DIVISION == '" + RACKROOM_FOOTWEAR_DIVISION + "' "
					.cRCRC_SQL_Where = " AND DIVISION == '" + RACKROOM_FOOTWEAR_DIVISION + "' "
					.cRDCA_SQL_Where = ''
					.cRIRI_SQL_Where = " AND DIVISION == '" + RACKROOM_FOOTWEAR_DIVISION + "' "
					.cIARN_SQL_Where = " AND DIVISION == '" + RACKROOM_FOOTWEAR_DIVISION + "' "
					
					.lTreatSortType41AsFullCase = .T.
					
					
				CASE tcClient == "OFF BROADWAY CHAS"  
				
					.cInvDetCompany = 'R'  && per Darren 12/07/2015
					.cTNodeFilter = "OB"
					
					.nAccountID = 6567
					.nBillToID = 6550 && use RR id # per Jim Campanelli 9/25/14
					
					.nInvoiceWONum = 68
					.lDotComProcessing = .F.
					.lAppendTransactionDetailToPDF = .T.
					.lAttachExcelSummaryFiles = .F.
					
					.lFilterCURRCNsByDiv = .T.
					
					.lRackRoomProcessing = .T.
					
					.cBillReceiptDetailReport = "M:\DEV\RPT\MLAUTO_RR_BILLRECEIPTDETAIL2"
					.cBillReceiptDetailReportNODATA = "M:\DEV\RPT\MLAUTOBILLRECEIPTDETAILNODATA"
					
					.cBillShippedDetailReport = "M:\DEV\RPT\SHIPDETL_RR2"
					.cBillShippedDetailReportNODATA = "M:\DEV\RPT\SHIPDETLNODATA"
					
					.cSummarySaveAs = 'F:\UTIL\MLAUTOBILLING\REPORTS\RACKROOMHANDLING'
					
					.cPDFWeeklyReportFRX = "M:\DEV\RPT\RACKROOMWEEKLYBILLSUMMARY" 
					
					.cPDFReportFRX = "M:\DEV\RPT\RACKROOMDAILYBILLSUMMARY" 
					.cSummaryOnlySendTo = 'joni.golding@tollgroup.com, joana.burke@tollgroup.com, juan.Perez@tollgroup.com, Juan.Hernandez@tollgroup.com'
					.cSummaryOnlyCC = 'cbalcezak@fmiint.com, jcampanelli@fmiint.com, mark.bennett@tollgroup.com'
					
					.cAUAD_SQL_Where = " AND DIVISION == '" + BROADWAY_CHAS_DIVISION + "' "
					.cRCRC_SQL_Where = " AND DIVISION == '" + BROADWAY_CHAS_DIVISION + "' "
					.cRDCA_SQL_Where = ''
					.cRIRI_SQL_Where = " AND DIVISION == '" + BROADWAY_CHAS_DIVISION + "' "
					.cIARN_SQL_Where = " AND DIVISION == '" + BROADWAY_CHAS_DIVISION + "' "
					
					.lTreatSortType41AsFullCase = .T.
					
				CASE tcClient == "OFF BROADWAY FOOTWEAR"  
				
					.cInvDetCompany = 'R'  && per Darren 12/07/2015
					.cTNodeFilter = "OB"
					
					.nAccountID = 6567
					.nBillToID = 6550 && use RR id # per Jim Campanelli 9/25/14

					.nInvoiceWONum = 68
					.lDotComProcessing = .F.
					.lAppendTransactionDetailToPDF = .T.
					.lAttachExcelSummaryFiles = .F.
					
					.lFilterCURRCNsByDiv = .T.
					
					.lRackRoomProcessing = .T.
					
					.cBillReceiptDetailReport = "M:\DEV\RPT\MLAUTO_RR_BILLRECEIPTDETAIL2"
					.cBillReceiptDetailReportNODATA = "M:\DEV\RPT\MLAUTOBILLRECEIPTDETAILNODATA"
					
					.cBillShippedDetailReport = "M:\DEV\RPT\SHIPDETL_RR2"
					.cBillShippedDetailReportNODATA = "M:\DEV\RPT\SHIPDETLNODATA"
					
					.cSummarySaveAs = 'F:\UTIL\MLAUTOBILLING\REPORTS\RACKROOMHANDLING'
					
					.cPDFWeeklyReportFRX = "M:\DEV\RPT\RACKROOMWEEKLYBILLSUMMARY" 
					
					.cPDFReportFRX = "M:\DEV\RPT\RACKROOMDAILYBILLSUMMARY" 
					.cSummaryOnlySendTo = 'joni.golding@tollgroup.com, joana.burke@tollgroup.com, juan.Perez@tollgroup.com, Juan.Hernandez@tollgroup.com'
					.cSummaryOnlyCC = 'cbalcezak@fmiint.com, jcampanelli@fmiint.com, mark.bennett@tollgroup.com'
					
					.cAUAD_SQL_Where = " AND DIVISION == '" + BROADWAY_FOOTWEAR_DIVISION + "' "
					.cRCRC_SQL_Where = " AND DIVISION == '" + BROADWAY_FOOTWEAR_DIVISION + "' "
					.cRDCA_SQL_Where = ''
					.cRIRI_SQL_Where = " AND DIVISION == '" + BROADWAY_FOOTWEAR_DIVISION + "' "
					.cIARN_SQL_Where = " AND DIVISION == '" + BROADWAY_FOOTWEAR_DIVISION + "' "
					
					.lTreatSortType41AsFullCase = .T.

				CASE tcClient == "OFF BROADWAY ALL"  
					* this is only used in the MTD handling report	
					
					.cInvDetCompany = 'R'  && per Darren 12/07/2015
					.cHandlingSummaryTemplate = 'F:\UTIL\MLAUTOBILLING\TEMPLATES\OBHandlingSummaryTemplate.xls'					
								
					.cTNodeFilter = "OB"
					
					.nAccountID = 6567
					.nBillToID = 6550 && use RR id # per Jim Campanelli 9/25/14

					.nInvoiceWONum = 68
					.lDotComProcessing = .F.
					.lAppendTransactionDetailToPDF = .T.
					.lAttachExcelSummaryFiles = .F.
					
					.lFilterCURRCNsByDiv = .T.
					
					.lRackRoomProcessing = .T.
					
					.cBillReceiptDetailReport = "M:\DEV\RPT\MLAUTO_RR_BILLRECEIPTDETAIL2"
					.cBillReceiptDetailReportNODATA = "M:\DEV\RPT\MLAUTOBILLRECEIPTDETAILNODATA"
					
					.cBillShippedDetailReport = "M:\DEV\RPT\SHIPDETL_RR2"
					.cBillShippedDetailReportNODATA = "M:\DEV\RPT\SHIPDETLNODATA"
					
					.cSummarySaveAs = 'F:\UTIL\MLAUTOBILLING\REPORTS\OFFBROADWAYHANDLING'
					
					.cPDFWeeklyReportFRX = "M:\DEV\RPT\RACKROOMWEEKLYBILLSUMMARY" 
					
					.cPDFReportFRX = "M:\DEV\RPT\RACKROOMDAILYBILLSUMMARY" 
					.cSummaryOnlySendTo = 'joni.golding@tollgroup.com, joana.burke@tollgroup.com, juan.Perez@tollgroup.com, Juan.Hernandez@tollgroup.com'
					.cSummaryOnlyCC = 'cbalcezak@fmiint.com, jcampanelli@fmiint.com, mark.bennett@tollgroup.com'
					
					lcDivString = ;
						"'" + BROADWAY_FOOTWEAR_DIVISION  + "'," + ; 
						"'" + BROADWAY_CHAS_DIVISION  + "'"

					.cAUAD_SQL_Where = " AND INLIST(DIVISION," +  lcDivString + ") "
					.cRCRC_SQL_Where = " AND INLIST(DIVISION," +  lcDivString + ") "
					.cRDCA_SQL_Where = ''
					.cRIRI_SQL_Where = " AND INLIST(DIVISION," +  lcDivString + ") "
					.cIARN_SQL_Where = " AND INLIST(DIVISION," +  lcDivString + ") "
					
					.lTreatSortType41AsFullCase = .T.

				CASE tcClient == "RACK ROOM ALL"				
					* this is only used in the MTD handling report
					
					.cInvDetCompany = 'R'  && per Darren 12/07/2015
					.cHandlingSummaryTemplate = 'F:\UTIL\MLAUTOBILLING\TEMPLATES\RRHandlingSummaryTemplate.xls'	
									
					.cTNodeFilter = "RR"
					
					.nAccountID = 6550  &&  re-verify before invoicing!
					.nInvoiceWONum = 67
					.lDotComProcessing = .F.
					.lAppendTransactionDetailToPDF = .T.
					.lAttachExcelSummaryFiles = .F.
					
					.lFilterCURRCNsByDiv = .T.
					
					.lRackRoomProcessing = .T.
					
					.cBillReceiptDetailReport = "M:\DEV\RPT\MLAUTO_RR_BILLRECEIPTDETAIL2"
					.cBillReceiptDetailReportNODATA = "M:\DEV\RPT\MLAUTOBILLRECEIPTDETAILNODATA"
					
					.cBillShippedDetailReport = "M:\DEV\RPT\SHIPDETL_RR2"
					.cBillShippedDetailReportNODATA = "M:\DEV\RPT\SHIPDETLNODATA"
					
					.cSummarySaveAs = 'F:\UTIL\MLAUTOBILLING\REPORTS\RACKROOMHANDLING'
					.cPDFReportFRX = "M:\DEV\RPT\RACKROOMDAILYBILLSUMMARY" 
					
					.cPDFWeeklyReportFRX = "M:\DEV\RPT\RACKROOMWEEKLYBILLSUMMARY" 
					
					.cSummaryOnlySendTo = 'joni.golding@tollgroup.com, joana.burke@tollgroup.com, juan.Perez@tollgroup.com, Juan.Hernandez@tollgroup.com'
					.cSummaryOnlyCC = 'cbalcezak@fmiint.com, jcampanelli@fmiint.com, mark.bennett@tollgroup.com'
					
					lcDivString = ;
						"'" + RACKROOM_FOOTWEAR_DIVISION + "'," + ;
						"'" + RACKROOM_CHAS_DIVISION  + "'"

					.cAUAD_SQL_Where = " AND INLIST(DIVISION," +  lcDivString + ") "
					.cRCRC_SQL_Where = " AND INLIST(DIVISION," +  lcDivString + ") "
					.cRDCA_SQL_Where = ''
					.cRIRI_SQL_Where = " AND INLIST(DIVISION," +  lcDivString + ") "
					.cIARN_SQL_Where = " AND INLIST(DIVISION," +  lcDivString + ") "
										
					.lTreatSortType41AsFullCase = .T.
					
*!*					CASE tcClient == "FORSAN MENS"  
*!*						.cTNodeFilter = "FS"
*!*						.cInvDetCompany = 'R'  && per Darren 12/16/2015
*!*						.nAccountID = 6642
*!*						.nInvoiceWONum = 69    && per Darren 12/16/2015
*!*						.lDotComProcessing = .F.
*!*						.lAppendTransactionDetailToPDF = .T.
*!*						.lAttachExcelSummaryFiles = .F.
*!*						.cBillReceiptDetailReport = "M:\DEV\RPT\MLAUTOBILLRECEIPTDETAIL"
*!*						.cBillReceiptDetailReportNODATA = "M:\DEV\RPT\MLAUTOBILLRECEIPTDETAILNODATA"
*!*						
*!*						*.cBillShippedDetailReport = "M:\DEV\RPT\SHIPDETL"
*!*						.cBillShippedDetailReport = "M:\DEV\RPT\SHIPDETL_FORSAN"
*!*						.cBillShippedDetailReportNODATA = "M:\DEV\RPT\SHIPDETLNODATA"
*!*						
*!*						.cSummarySaveAs = 'F:\UTIL\MLAUTOBILLING\REPORTS\FORSANHANDLING'
*!*						.cPDFReportFRX = "M:\DEV\RPT\FORSANDAILYBILLSUMMARYTRANSFER" 
*!*						.cSummaryOnlySendTo = 'joni.golding@tollgroup.com'
*!*						.cSummaryOnlyCC = 'cbalcezak@fmiint.com, jcampanelli@fmiint.com, mperez1@searshc.com, mark.bennett@tollgroup.com'
*!*						.cIAAD_SQL_Where = " AND RIGHT(DIVISION,2) = '" + FORSAN_MENS_DIVISION + "' "
*!*						.cAUAD_SQL_Where = " AND RIGHT(DIVISION,2) = '" + FORSAN_MENS_DIVISION + "' "
*!*						.cRCRC_SQL_Where = " AND RIGHT(DIVISION,2) = '" + FORSAN_MENS_DIVISION + "' "
*!*						.cRDCA_SQL_Where = ''
*!*						.cRIRI_SQL_Where = " AND RIGHT(DIVISION,2) = '" + FORSAN_MENS_DIVISION + "' "
*!*						.cIARN_SQL_Where = " AND RIGHT(DIVISION,2) = '" + FORSAN_MENS_DIVISION + "' "
*!*						.cIAAD_SQL_Where = " AND RIGHT(DIVISION,2) = '" + FORSAN_MENS_DIVISION + "' "
					
				CASE tcClient == "FORSAN"  
					.cTNodeFilter = "FS"
					.cInvDetCompany = 'R'  && per Darren 12/16/2015
					.nAccountID = 6642
					.nInvoiceWONum = 69    && per Darren 12/16/2015
					.lDotComProcessing = .F.
					.lAppendTransactionDetailToPDF = .T.
					.lAttachExcelSummaryFiles = .F.
					.cBillReceiptDetailReport = "M:\DEV\RPT\MLAUTOBILLRECEIPTDETAIL"
					.cBillReceiptDetailReportNODATA = "M:\DEV\RPT\MLAUTOBILLRECEIPTDETAILNODATA"
					
					*.cBillShippedDetailReport = "M:\DEV\RPT\SHIPDETL"
					.cBillShippedDetailReport = "M:\DEV\RPT\SHIPDETL_FORSAN"
					.cBillShippedDetailReportNODATA = "M:\DEV\RPT\SHIPDETLNODATA"
					
					.cSummarySaveAs = 'F:\UTIL\MLAUTOBILLING\REPORTS\FORSANHANDLING'
					.cPDFReportFRX = "M:\DEV\RPT\FORSANDAILYBILLSUMMARYTRANSFER" 
					.cSummaryOnlySendTo = 'joni.golding@tollgroup.com'
					.cSummaryOnlyCC = 'cbalcezak@fmiint.com, jcampanelli@fmiint.com, mperez1@searshc.com, mark.bennett@tollgroup.com'
					.cIAAD_SQL_Where = ""
					.cAUAD_SQL_Where = ""
					.cRCRC_SQL_Where = ""
					.cRDCA_SQL_Where = ""
					.cRIRI_SQL_Where = ""
					.cIARN_SQL_Where = ""
					.cIAAD_SQL_Where = ""
					
				OTHERWISE
					.TrackProgress("**** ERROR: unexpected tcClient = " + TRANSFORM(tcClient), LOGIT+SENDIT)
					llRetVal = .F.
					
			ENDCASE
			
			.TrackProgress(".cIAAD_SQL_Where = " + .cIAAD_SQL_Where, LOGIT)
			.TrackProgress(".cAUAD_SQL_Where = " + .cAUAD_SQL_Where, LOGIT)
			.TrackProgress(".cRCRC_SQL_Where = " + .cRCRC_SQL_Where, LOGIT)
			.TrackProgress(".cRDCA_SQL_Where = " + .cRDCA_SQL_Where, LOGIT)
			.TrackProgress(".cRIRI_SQL_Where = " + .cRIRI_SQL_Where, LOGIT)
			.TrackProgress(".cIARN_SQL_Where = " + .cIARN_SQL_Where, LOGIT)
			.TrackProgress("FullCase processing = " + TRANSFORM(.lTreatSortType41AsFullCase), LOGIT)
			IF .lTestMode THEN
				.cSendTo = 'mark.bennett@tollgroup.com'
				.cClientSendTo = 'mark.bennett@tollgroup.com'
				.cClientCC = 'mark.bennett@tollgroup.com'
				.cSummaryOnlySendTo = 'mark.bennett@tollgroup.com'
				.cSummaryOnlyCC = 'mark.bennett@tollgroup.com'
			ENDIF
		ENDWITH
		RETURN llRetVal
	ENDFUNC
	

	FUNCTION SetUsePrepackForManualUnitCountingMode
		LPARAMETERS tlUsePrepackForManualUnitCounting
		WITH THIS
			.lUsePrepackForManualUnitCounting = tlUsePrepackForManualUnitCounting
			IF tlUsePrepackForManualUnitCounting THEN
				.TrackProgress("**** Using Prepack For Manual Unit Counting ****", LOGIT+SENDIT)
			ELSE
				.TrackProgress("**** Not using Prepack For Manual Unit Counting ****", LOGIT+SENDIT)
			ENDIF
		ENDWITH
	ENDFUNC


*!*		FUNCTION SetUseRCNForRDCAKeyMode
*!*			LPARAMETERS tlUseRCNForRDCAKey
*!*			WITH THIS
*!*				.lUseRCNForRDCAKey = tlUseRCNForRDCAKey
*!*				IF tlUseRCNForRDCAKey THEN
*!*					.TrackProgress("**** Using RCN as part of RDCA Key ****", LOGIT+SENDIT)
*!*				ELSE
*!*					.TrackProgress("**** Not using RCN as part of RDCA Key ****", LOGIT+SENDIT)
*!*				ENDIF
*!*			ENDWITH
*!*		ENDFUNC


	FUNCTION LOAD

		LOCAL lnNumZipFiles, lnNumFiles, laZipFiles[1,5], laFiles[1,5], laFilesSorted[1,6], i, j, k, l
		LOCAL lcZippedFile, lcUnZippedFile, lcCMD, llRetVal, lcError
		LOCAL lcArchivedFile, loError, lnTally, lcUnZippedFile

		WITH THIS

			TRY
				_SCREEN.CAPTION = .cClient + " Bill Data Load Process"
				.TrackProgress("---------- " + .cClient + " Transaction Load Process Started -------------", LOGIT+SENDIT)
				.TrackProgress('COMPUTERNAME = ' + .cCOMPUTERNAME, LOGIT+SENDIT)
				.TrackProgress('USERNAME = ' + .cUSERNAME, LOGIT+SENDIT)
				IF .lTestMode THEN
					.TrackProgress("-----------        TEST MODE        -------------", LOGIT+SENDIT)
				ENDIF
				
				IF NOT .lLoadingDeleteByDateRange THEN
					.TrackProgress("!!!!!!!WARNING!!!!!!", LOGIT+SENDIT)
					.TrackProgress(".lLoadingDeleteByDateRange = FALSE", LOGIT+SENDIT)
					.TrackProgress("DATA FOR THIS DATE RANGE WILL NOT BE DELETED", LOGIT+SENDIT)
				ENDIF				
				
				IF .lAutoMode THEN
					.TrackProgress("PROCESS MODE = AUTOMATIC", LOGIT+SENDIT)
				ELSE
					.TrackProgress("PROCESS MODE = MANUAL", LOGIT+SENDIT)
				ENDIF
				
				IF .VALIDATE() THEN

					*************************************************************
					*************************************************************
					* unzip any .gz files to the unzipped directory
					SET DEFAULT TO (.cInPath)
					lnNumZipFiles = ADIR(laZipFiles,"*.gz")

					IF lnNumZipFiles > 0 THEN
					
						* 1/17/2018 MB: changed C:\GZIP\GZIP.EXE to F:\UTIL\GZIP\GZIP.EXE

						* use GZIP Instead 10/30/06 MB !!!!
						IF FILE("F:\UTIL\GZIP\GZIP.EXE")
							* archive (copy) files first, because the unzipping process will overwrite them!
							FOR i = 1 TO lnNumZipFiles
								lcZippedFile = .cInPath + laZipFiles[i,1]
								lcArchivedFile = .cArchivedPath + laZipFiles[i,1]
								.TrackProgress('Archiving file: ' + lcZippedFile, LOGIT+WAITIT+SENDIT)
								IF FILE(lcZippedFile) THEN
									COPY FILE (lcZippedFile) TO (lcArchivedFile)
								ENDIF  &&  FILE(lcZippedFile)
							ENDFOR

							* run GZIP, which overwrites original files
							.TrackProgress('Using GZIP to decompress all .gz files in ' + .cInPath, LOGIT+WAITIT+SENDIT)
							lcCMD = "F:\UTIL\GZIP\GZIP -df " + .cInPathForGZIP
							*_cliptext = lcCMD
							RUN &lcCMD

						ELSE
							.TrackProgress('GZIP.EXE not installed on this computer - UNZIP operation aborted', LOGIT+WAITIT+SENDIT)
						ENDIF  &&  FILE("F:\UTIL\GZIP\GZIP.EXE")

					ELSE
						.TrackProgress('Found no Mira Loma Daily Bill .gz files to unzip!', LOGIT+WAITIT+SENDIT)
					ENDIF	&&  lnNumZipFiles > 0
					*************************************************************
					*************************************************************

					* get unzipped files to process - even if we didn't unzip any files there could be some
					* left over from previous error, etc.
					SET DEFAULT TO (.cUnzippedPath)
					*lnNumFiles = ADIR(laFiles,"*.*")
					lnNumFiles = ADIR(laFiles,"*.DOS")

					* save to property
					.nNumFilesFound = lnNumFiles

					IF lnNumFiles > 0 THEN

						.TrackProgress('Found ' + TRANSFORM(lnNumFiles) + ' unzipped files to process; starting Mira Loma Billing Load Process.', LOGIT+SENDIT)

						* sort file list by date/time
						.SortArrayByDateTime(@laFiles, @laFilesSorted)

						* open tables
						USE (.cTransTable) IN 0 ALIAS TRANSTABLE EXCLUSIVE

						* HISTORYTABLE stores the bill variables per process run.
						USE (.cHistoryTable) AGAIN IN 0 ALIAS HISTORYTABLE EXCLUSIVE
						
						* RCRCHIST stores all RCRC recs processed currently and previously.
						USE (.cRCRCTable) AGAIN IN 0 ALIAS RCRCHIST EXCLUSIVE

						*!*							* RCRZHIST stores all RCRZ recs processed currently and previously.
						*!*							* (RCRZ recs "undo" RCRC recs.)
						*!*							USE (.cRCRZTable) AGAIN IN 0 ALIAS RCRZHIST EXCLUSIVE

						* RDCAHIST stores all RDCA recs processed currently and previously.
						* (RDCA recs identify XDOCK received cases.)
						USE (.cRDCATable) AGAIN IN 0 ALIAS RDCAHIST EXCLUSIVE

						********************************************************************
						* open RATO and RATC tables in shared mode because other people may be accessing them.
						* Will try to open excl later for packing.

						* RATOHIST stores all RATO recs processed currently and previously.
						* (RATO recs identify "TRAILER OPENS".)
						*USE (.cRATOTable) AGAIN IN 0 ALIAS RATOHIST EXCLUSIVE
						USE (.cRATOTable) AGAIN IN 0 ALIAS RATOHIST SHARED

						* RATCHIST stores all RATC recs processed currently and previously.
						* (RATC recs identify "TRAILER CLOSES".)
						*USE (.cRATCTable) AGAIN IN 0 ALIAS RATCHIST EXCLUSIVE
						USE (.cRATCTable) AGAIN IN 0 ALIAS RATCHIST SHARED
						
						* RETURNS stores all Return Receipt recs processed currently and previously.						
						USE (.cRETURNSTable) AGAIN IN 0 ALIAS RETURNS EXCLUSIVE
						
						* IAADXFER stores transfers into shipping transactions, currently IAAD transactions from FORSAN.						
						USE (.cIAADXFERTable) AGAIN IN 0 ALIAS IAADXFER EXCLUSIVE
						********************************************************************
						
						* now process the files
						FOR i = 1 TO lnNumFiles

							lcUnZippedFile = .cUnzippedPath + laFilesSorted[i,1]
							lcProcessedFile = .cProcessedPath + laFilesSorted[i,1]
							*!*								lcBillFor = "SEARS Bill for: " + CRLF + ALLTRIM(laFilesSorted[i,1])

							IF FILE(lcUnZippedFile)

								.TrackProgress('Processing ' + lcUnZippedFile, LOGIT+SENDIT)

								* main function
								llRetVal = .LoadTransactionFile(lcUnZippedFile, i)

								IF llRetVal THEN
									* archive file
									COPY FILE (lcUnZippedFile) TO (lcProcessedFile)
									IF FILE(lcProcessedFile)
										DELETE FILE (lcUnZippedFile)
									ENDIF
								ENDIF

							ENDIF  &&  FILE(lcUnzippedFile)
						ENDFOR  &&  i = 1 TO lnNumFiles
						
						IF .lPackTables THEN

							.TrackProgress("Loaded " + TRANSFORM(lnNumFiles) + " 'Mira Loma Billing Transaction' files", LOGIT+SENDIT)
							WAIT CLEAR
							.TrackProgress("Packing RCRCHIST...", LOGIT+WAITIT+SENDIT)
							SELECT RCRCHIST
							PACK
							.TrackProgress("Packed RCRCHIST.", LOGIT+SENDIT)
							.TrackProgress("Packing TRANSTABLE...", LOGIT+WAITIT+SENDIT)
							SELECT TRANSTABLE
							PACK
							.TrackProgress("Packed TRANSTABLE.", LOGIT+SENDIT)

							.TrackProgress("Packing RDCAHIST...", LOGIT+WAITIT+SENDIT)
							SELECT RDCAHIST
							PACK
							.TrackProgress("Packed RDCAHIST.", LOGIT+SENDIT)

							.TrackProgress("Packing RETURNS...", LOGIT+WAITIT+SENDIT)
							SELECT RETURNS
							PACK
							.TrackProgress("Packed RETURNS.", LOGIT+SENDIT)

							********************************************************************
							* TRY to open EXCL and pack RATO/RATC tables...
							.TrackProgress("Packing RATOHIST...", LOGIT+WAITIT+SENDIT)
							USE IN RATOHIST
							TRY
								USE (.cRATOTable) IN 0 ALIAS RATOHIST EXCLUSIVE
								SELECT RATOHIST
								PACK
								.TrackProgress("Packed RATOHIST.", LOGIT+SENDIT)
							CATCH
								.TrackProgress("!!! WARNINF Could not open RATOHIST EXCL for PACKing.", LOGIT+SENDIT)
							ENDTRY

							.TrackProgress("Packing RATCHIST...", LOGIT+WAITIT+SENDIT)
							USE IN RATCHIST
							TRY
								USE (.cRATCTable) IN 0 ALIAS RATCHIST EXCLUSIVE
								SELECT RATCHIST
								PACK
								.TrackProgress("Packed RATCHIST.", LOGIT+SENDIT)
							CATCH
								.TrackProgress("!!! WARNING: Could not open RATCHIST EXCL for PACKing.", LOGIT+SENDIT)
							ENDTRY

							********************************************************************

						ENDIF  && .lPackTables THEN

					ELSE
						.TrackProgress('Found no Mira Loma Billing unzipped Transaction files to load!', LOGIT+WAITIT+SENDIT)
					ENDIF  &&  lnNumFiles > 0
					
					.ShowDataFiles()

					.TrackProgress('**************** TRANS-TYPE RECORD COUNT BREAKDOWN FOLLOWS **********************', LOGIT+SENDIT)
					
					.TrackProgress('Total Recs in TRANSFILE = ' + TRANSFORM(.nTotalRecs), LOGIT+SENDIT)					
					
					.TrackProgress('IAAD Total Recs = ' + TRANSFORM(.nIAADTotalRecs), LOGIT+SENDIT)					
					
					.TrackProgress('IARN Total Recs = ' + TRANSFORM(.nIARNTotalRecs), LOGIT+SENDIT)					
					.TrackProgress('RIRI Total Recs = ' + TRANSFORM(.nRIRITotalRecs), LOGIT+SENDIT)					
					
					.TrackProgress('AUAD Total Recs = ' + TRANSFORM(.nAUADTotalRecs), LOGIT+SENDIT)
					.TrackProgress('AUAD Original Recs = ' + TRANSFORM(.nAUADOldRecs), LOGIT+SENDIT)
					.TrackProgress('AUAD 8/2006 Recs = ' + TRANSFORM(.nAUAD_8_2006_Recs), LOGIT+SENDIT)
					.TrackProgress('AUAD Good Recs = ' + TRANSFORM(.nAUADGoodRecs), LOGIT+SENDIT)
					.TrackProgress('AUAD Bad Recs = ' + TRANSFORM(.nAUADBadRecs), LOGIT+SENDIT)
					
					.TrackProgress('nAUAD_10_XdockRecs = ' + TRANSFORM(.nAUAD_10_XdockRecs), LOGIT+SENDIT)
					.TrackProgress('nAUAD_41_ManualRecs = ' + TRANSFORM(.nAUAD_41_ManualRecs), LOGIT+SENDIT)
					.TrackProgress('nAUAD_51_FullRecs = ' + TRANSFORM(.nAUAD_51_FullRecs), LOGIT+SENDIT)
					.TrackProgress('nAUAD_71_TiltRecs = ' + TRANSFORM(.nAUAD_71_TiltRecs), LOGIT+SENDIT)
					.TrackProgress('nAUAD_99_FindRecs = ' + TRANSFORM(.nAUAD_99_FindRecs), LOGIT+SENDIT)
					.TrackProgress('nAUAD_XX_UnknRecs = ' + TRANSFORM(.nAUAD_XX_UnknRecs), LOGIT+SENDIT)
					
					.TrackProgress('nAUAD_DotComRecs = ' + TRANSFORM(.nAUAD_DotComRecs), LOGIT+SENDIT)
					.TrackProgress('nRDCA_DotComRecs = ' + TRANSFORM(.nRDCA_DotComRecs), LOGIT+SENDIT)
															
					.TrackProgress('RBPC Total Recs = ' + TRANSFORM(.nRBPCTotalRecs), LOGIT+SENDIT)
					.TrackProgress('RCRC Total Recs = ' + TRANSFORM(.nRCRCTotalRecs), LOGIT+SENDIT)
					.TrackProgress('RDCA Total Recs = ' + TRANSFORM(.nRDCATotalRecs), LOGIT+SENDIT)
					.TrackProgress('', LOGIT+SENDIT)
					.TrackProgress('Mira Loma Billing Load Process ended normally.', LOGIT+SENDIT)

				ELSE
					* failed validation - cannot continue
					.TrackProgress('Mira Loma Billing Load Process DID NOT RUN.', LOGIT+SENDIT)

				ENDIF  &&  .Validate()
				
				IF NOT EMPTY(.cInvalidDivisionList) THEN
					.cBodyText = "ERROR: INVALID DIVISIONS FOUND DURING LOAD: " + CRLF + .cInvalidDivisionList + CRLF + CRLF + .cBodyText
				ENDIF

			CATCH TO loError
				SET CONSOLE OFF
				.lTrappedError = .T.
				.TrackProgress('There was an error.',LOGIT+SENDIT)
				.TrackProgress('The Error # is: ' + TRANSFORM(loError.ERRORNO), LOGIT+SENDIT)
				.TrackProgress('The Error Message is: ' + TRANSFORM(loError.MESSAGE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred in Program: ' + TRANSFORM(loError.PROCEDURE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred at line #: ' + TRANSFORM(loError.LINENO), LOGIT+SENDIT)

			FINALLY

			ENDTRY

			WAIT CLEAR

			***************** INTERNAL email results ******************************
			.cSubject = 'Mira Loma Billing Load Process Results for: ' + TTOC(DATETIME())
			.TrackProgress('About to send status email.',LOGIT)
			IF .nValidationErrors > 0 THEN
				.TrackProgress('',LOGIT+SENDIT)
				.TrackProgress('!!!!!!!! There were ' + TRANSFORM(.nValidationErrors) + ' data validation errors; please examine the logfile for details.', LOGIT+SENDIT)
			ENDIF
			.TrackProgress('The logfile is: ' + .cLogFile, SENDIT)
			.TrackProgress('Mira Loma Billing Load Process started: ' + .cStartTime, LOGIT+SENDIT)
			.TrackProgress('Mira Loma Billing Load Process finished: ' + TTOC(DATETIME()), LOGIT+SENDIT)
			IF .lSendInternalEmailIsOn THEN
				*DO FORM F:\MAIL\FORMS\dartmail2 WITH .cSendTo,.cFrom,.cSubject,.cCC,.cAttach,.cBodyText,"A"
				DO FORM M:\DEV\FRM\dartmail2 WITH .cLoadSendTo,.cFrom,.cSubject,.cCC,.cAttach,.cBodyText,"A"
				.TrackProgress('Sent status email.',LOGIT)
			ELSE
				.TrackProgress('Did NOT send status email: THIS.lSendInternalEmailIsOn = FALSE.',LOGIT)
			ENDIF

			*!*	****************** send client email ? **********
			*!*	IF NOT EMPTY(.cClientBodyText) THEN
			*!*		DO FORM F:\MAIL\FORMS\dartmail2 WITH .cClientSendTo,.cClientFrom,.cClientSubject,.cClientCC,.cClientAttach,.cClientBodyText,"A"
			*!*		.TrackProgress('Sent client email.',LOGIT)
			*!*	ELSE
			*!*		.TrackProgress('Did NOT send client email: no work orders were created.',LOGIT)
			*!*	ENDIF

			CLOSE DATA

		ENDWITH  &&  THIS

		RETURN

	ENDFUNC && Load


	FUNCTION LoadTransactionFile
		LPARAMETERS	tcUnzippedFile, tnFileNumber

		LOCAL lnHandle, lcStr, lcTrans, lnRec, lcFType, lcTType, ltDateTime, lnSKUCnt, lnNumItems, i, lnQTY, llDoInsert, llRetVal
		LOCAL llPDFReportWasCreated, lcCentury, lcRootPDFFilename, lcFullPDFFilename, lnRCRCCount, ltMinDateTime, ltMaxDateTime
		LOCAL lnYYYYMMDDStart, lnHHMMSSStart, loError, lnUntrimmedLineLength, lnAUADOffset, llDeterminedRecordLength, lcPrePack
		LOCAL lcDotComDest_loc, lcSKUChars, lcPL_QTY, llIsNumeric, lnDummyCounter, lcMinDateTrans, lcMaxDateTrans

		STORE '?' TO lcMinDateTrans, lcMaxDateTrans
		
		lcDotComDest_loc = .cDotComDest_loc
		llRetVal = .T.
		lnDummyCounter = 0

		* do pre-pass on transaction file to get min/max tdatetime AND determine record format from record lengths
		WAIT WINDOW NOWAIT "Getting min/max transaction dates..."
		STORE (DATETIME() + (3600 * 24 * 365)) TO ltMinDateTime
		STORE (DATETIME() - (3600 * 24 * 365)) TO ltMaxDateTime
		lnHandle = FOPEN(tcUnzippedFile)
		.lUseAugust2006Format = .F.
		llDeterminedRecordLength = .F.
		DO WHILE NOT FEOF(lnHandle)

			*
			lcStr = FGETS(lnHandle,2048)
			lcTrans = SUBSTR(lcStr,1,4)
			lnUntrimmedLineLength = LEN(lcStr)
			
			********************************************************
			********************************************************
			**** 8/13/14: from now on, will assume it is the latest format 
			**** - it's too hard to try and determine which rec version
			.lUseAugust2006Format = .T.
			llDeterminedRecordLength = .T.
			********************************************************
			********************************************************


*!*				* we only need to do this check if we haven't already determined that its the August 2006 format
*!*				* NOTE: on 3/25/09 I began getting data with some recs that had extra fields at the end
*!*				* such as RCRC and 
*!*				IF NOT llDeterminedRecordLength THEN
*!*					.TrackProgress(lcTrans + ' lnUntrimmedLineLength = ' + TRANSFORM(lnUntrimmedLineLength),LOGIT+SENDIT)
*!*					DO CASE
*!*						CASE (lcTrans = "AUAD")
*!*							IF lnUntrimmedLineLength = 421 THEN
*!*								.lUseAugust2006Format = .T.
*!*							ENDIF
*!*							llDeterminedRecordLength = .T.
*!*						CASE lcTrans = "RATO"
*!*							IF lnUntrimmedLineLength = 203 THEN
*!*								.lUseAugust2006Format = .T.
*!*							ENDIF
*!*							llDeterminedRecordLength = .T.
*!*						CASE lcTrans = "RATC"
*!*							IF lnUntrimmedLineLength = 203 THEN
*!*								.lUseAugust2006Format = .T.
*!*							ENDIF
*!*							llDeterminedRecordLength = .T.
*!*						CASE lcTrans = "RBPO"
*!*							IF lnUntrimmedLineLength = 235 THEN
*!*								.lUseAugust2006Format = .T.
*!*							ENDIF
*!*							llDeterminedRecordLength = .T.
*!*						CASE lcTrans = "RBPC"
*!*							IF lnUntrimmedLineLength = 235 THEN
*!*								.lUseAugust2006Format = .T.
*!*							ENDIF
*!*							llDeterminedRecordLength = .T.
*!*						CASE lcTrans = "RCRC"
*!*							*IF lnUntrimmedLineLength = 172 THEN
*!*							IF INLIST(lnUntrimmedLineLength,172,192) THEN
*!*								.lUseAugust2006Format = .T.
*!*							ENDIF
*!*							llDeterminedRecordLength = .T.
*!*						CASE lcTrans = "RCRZ"
*!*							*IF lnUntrimmedLineLength = 172 THEN
*!*							IF INLIST(lnUntrimmedLineLength,172,192) THEN
*!*								.lUseAugust2006Format = .T.
*!*							ENDIF
*!*							llDeterminedRecordLength = .T.
*!*						CASE lcTrans = "RDCA"
*!*							*IF lnUntrimmedLineLength = 165 THEN
*!*							IF INLIST(lnUntrimmedLineLength,165,185) THEN
*!*								.lUseAugust2006Format = .T.
*!*							ENDIF
*!*							llDeterminedRecordLength = .T.
*!*						OTHERWISE
*!*							* nothing
*!*					ENDCASE
*!*				ENDIF &&  NOT .lUseAugust2006Format THEN


			* okay, now TRIM it
			*lcStr = UPPER(ALLTRIM(FGETS(lnHandle,2048)))
			lcStr = UPPER(ALLTRIM(lcStr))
			ltDateTime = CTOT(SUBSTR(lcStr,19,4) + "-" + SUBSTR(lcStr,23,2) + "-" + SUBSTR(lcStr,25,2) + "T" + ;
				SUBSTR(lcStr,27,2) + ":" + SUBSTR(lcStr,29,2) + ":" + SUBSTR(lcStr,31,2))
			IF ltDateTime <= ltMinDateTime THEN
				IF NOT EMPTY(ltDateTime) THEN
					ltMinDateTime = ltDateTime
					lcMinDateTrans = lcTrans
				ENDIF
			ENDIF
			IF ltDateTime >= ltMaxDateTime THEN
				IF NOT EMPTY(ltDateTime) THEN
					ltMaxDateTime = ltDateTime
					lcMaxDateTrans = lcTrans
				ENDIF
			ENDIF
		ENDDO  &&  WHILE NOT FEOF(lnHandle)
		=FCLOSE(lnHandle)
		.TrackProgress('MIN(tdatetime) = ' + TTOC(ltMinDateTime),LOGIT+SENDIT)
		.TrackProgress('TransType = ' + lcMinDateTrans,LOGIT+SENDIT)
		.TrackProgress('MAX(tdatetime) = ' + TTOC(ltMaxDateTime),LOGIT+SENDIT)
		.TrackProgress('TransType = ' + lcMaxDateTrans,LOGIT+SENDIT)

		IF llDeterminedRecordLength THEN
			.TrackProgress('Was able to determine if the August 2006 format is being used:',LOGIT+SENDIT)
			IF .lUseAugust2006Format THEN
				.TrackProgress('It IS the August 2006 format',LOGIT+SENDIT)
			ELSE
				.TrackProgress('It IS NOT the August 2006 format',LOGIT+SENDIT)
			ENDIF
		ELSE
			.TrackProgress('WARNING: Was NOT able to determine if the August 2006 format is being used.',LOGIT+SENDIT)
		ENDIF

		WAIT CLEAR

		******************************************************************************
		******************************************************************************
		IF .lLoadingDeleteByDateRange THEN
			* delete from history tables based on min/max tdatetime
			WAIT WINDOW NOWAIT "Deleting from history tables..."
			DELETE FROM RCRCHIST WHERE (TDATETIME >= ltMinDateTime) AND (TDATETIME <= ltMaxDateTime)
			DELETE FROM RATOHIST WHERE (TDATETIME >= ltMinDateTime) AND (TDATETIME <= ltMaxDateTime)
			DELETE FROM RATCHIST WHERE (TDATETIME >= ltMinDateTime) AND (TDATETIME <= ltMaxDateTime)
			DELETE FROM RDCAHIST WHERE (TDATETIME >= ltMinDateTime) AND (TDATETIME <= ltMaxDateTime)
			DELETE FROM TRANSTABLE WHERE (TDATETIME >= ltMinDateTime) AND (TDATETIME <= ltMaxDateTime)
			DELETE FROM RETURNS WHERE (TDATETIME >= ltMinDateTime) AND (TDATETIME <= ltMaxDateTime)
			DELETE FROM IAADXFER WHERE (TDATETIME >= ltMinDateTime) AND (TDATETIME <= ltMaxDateTime)
		ENDIF && .lLoadingDeleteByDateRange		
		******************************************************************************
		******************************************************************************

		*SET STEP ON
			
		CREATE CURSOR CURNEWTRANS (ctrans c(4)) && THIS WILL LOG UNEXPECTED TRANSACTION TYPES

		lnRec = 0
		lnHandle = FOPEN(tcUnzippedFile)
		DO WHILE NOT FEOF(lnHandle)

			lcStr = FGETS(lnHandle,2048)

			* get line length before trimming - used to determine which version of AUAD rec is being loaded
			lnUntrimmedLineLength = LEN(lcStr)

			* ok, now trim it
			lcStr = UPPER(ALLTRIM(lcStr))

			*!*	IF NOT (LEN(lcStr) > 10) THEN
			*!*		EXIT
			*!*	ENDIF

			lcTrans = SUBSTR(lcStr,1,4)

			lcFType = SUBSTR(lcStr,1,2)
			lcTType = SUBSTR(lcStr,3,2)

			ltDateTime = CTOT(SUBSTR(lcStr,19,4) + "-" + SUBSTR(lcStr,23,2) + "-" + SUBSTR(lcStr,25,2) + "T" + ;
				SUBSTR(lcStr,27,2) + ":" + SUBSTR(lcStr,29,2) + ":" + SUBSTR(lcStr,31,2))
				
			.nTotalRecs = .nTotalRecs + 1  && ADDED 1/19/2017 MB
			
			lnRec = lnRec + 1
			IF MOD(lnRec,1000) = 0 THEN
				WAIT WINDOW "In File: " + tcUnzippedFile + ", Processing record #" + STR(lnRec) NOWAIT
			ENDIF

			* fields common to all trans types
			m.TRANS = lcTrans
			m.FTYPE = lcFType
			m.TTYPE = lcTType
			m.TDATETIME = ltDateTime
			m.FNODE = SUBSTR(lcStr,33,2)
			m.TNODE = SUBSTR(lcStr,35,2)
			m.PROCESSED = SUBSTR(lcStr,37,2)
			m.CONTAINER = SUBSTR(lcStr,39,12)

			******************************************
			* rectype-specific processing
			llDoInsert = .F.

			* clear rectype-specific m.vars
			STORE "" TO m.COMPANY, m.PONUM, m.PO_TYPE, m.BOL_NO, m.SKU, m.CASEID, m.ORDER_TYPE, m.ASNNUM, m.SORT_TYPE, m.DEST_LOC, m.PO_LINEKEY
			STORE "" TO m.INV_STATUS, m.TRAILERLD, m.CONTTYPE, m.DEST_TYPE, m.ASNNUM, m.DIVISION, m.TRAILER_NO, m.CTRANS
			STORE 0 TO m.RCN, m.PL_QTY, m.CYCLE_NO, m.FILE_SEQ, m.REC_SEQ, m.CASE_WGT, m.CASE_CUBE, m.QTY_CASE, m.UNITS_PCSE
			STORE 0 TO m.NUM_CASES, m.CASEPKCASE, m.ORIG_QTY, m.RESID_QTY, m.UNIT_QTY, m.RRUNITS
			STORE 1 TO m.PREPACK
			STORE CTOD("  /  /  ") TO m.SHIPDATE, m.MELD_DATE
			STORE CTOT("") TO m.YARDINDT, m.RCNCMPDT
			STORE .F. TO m.ASN

			DO CASE

				CASE lcTrans = "RIRI"
					** this transaction identifies Residual units taken out of a case and put back in stock 
					** should be for Sears/KMart only. Added for 1/1/2010 MB
				
					.nRIRITotalRecs = .nRIRITotalRecs + 1
					
					m.CONTAINER = ""	&& this trans does not have container info
					m.COMPANY = SUBSTR(lcStr,39,2)
					m.CASEID = SUBSTR(lcStr,41,20)
					m.SKU = SUBSTR(lcStr,61,17)							
					m.ORIG_QTY = VAL(SUBSTR(lcStr,78,7))  && units in case prior to processing
					m.RESID_QTY = VAL(SUBSTR(lcStr,85,7))  && Residual Units (units returned to inventory)
					m.DIVISION = SUBSTR(lcStr,92,3)
					
					IF EMPTY(m.DIVISION) THEN
						m.DIVISION = "0" + SUBSTR(m.SKU,2,2)
						*.TrackProgress("ERROR: EMPTY DIVISION In File: " + tcUnzippedFile + " at record # " + STR(lnRec),LOGIT+SENDIT)
						*THROW
					ENDIF
					
					.CheckDivision( m.TNODE, m.DIVISION, "RIRI" )					

					llDoInsert = .T.

				CASE lcTrans = "RBPC"
				
					.nRBPCTotalRecs = .nRBPCTotalRecs + 1

					DO CASE
						CASE .lUseAugust2006Format
							m.COMPANY = SUBSTR(lcStr,51,2)
							m.PONUM = SUBSTR(lcStr,56,16)
							m.PO_TYPE = SUBSTR(lcStr,72,2)
							m.BOL_NO = SUBSTR(lcStr,133,16)
							m.PL_QTY = VAL(SUBSTR(lcStr,156,7))  && received qty
							m.RCN = VAL(SUBSTR(lcStr,194,8))
							m.SKU = SUBSTR(lcStr,205,17)
						OTHERWISE
							m.COMPANY = SUBSTR(lcStr,51,2)
							m.PONUM = SUBSTR(lcStr,56,8)
							m.PO_TYPE = SUBSTR(lcStr,64,2)
							m.BOL_NO = SUBSTR(lcStr,125,16)
							m.PL_QTY = VAL(SUBSTR(lcStr,148,7))  && received qty
							m.RCN = VAL(SUBSTR(lcStr,186,8))
							m.SKU = SUBSTR(lcStr,197,17)
					ENDCASE

					llDoInsert = .T.

					* not currently using RBPO but keeping in history in case we want to report on Open RCNs on a bill
				CASE lcTrans = "RBPO"

					DO CASE
						CASE .lUseAugust2006Format
							m.COMPANY = SUBSTR(lcStr,51,2)
							m.PONUM = SUBSTR(lcStr,56,16)
							m.PO_TYPE = SUBSTR(lcStr,72,2)
							m.BOL_NO = SUBSTR(lcStr,133,16)
							m.PL_QTY = VAL(SUBSTR(lcStr,149,7))  && expected qty
							m.RCN = VAL(SUBSTR(lcStr,194,8))
							m.SKU = SUBSTR(lcStr,205,17)
						OTHERWISE
							m.COMPANY = SUBSTR(lcStr,51,2)
							m.PONUM = SUBSTR(lcStr,56,8)
							m.PO_TYPE = SUBSTR(lcStr,64,2)
							m.BOL_NO = SUBSTR(lcStr,125,16)
							m.PL_QTY = VAL(SUBSTR(lcStr,141,7))  && expected qty
							m.RCN = VAL(SUBSTR(lcStr,186,8))
							m.SKU = SUBSTR(lcStr,197,17)
					ENDCASE

					llDoInsert = .T.

				CASE lcTrans = "AUAD"

					.nAUADTotalRecs = .nAUADTotalRecs + 1

					* Modified to support new rec layout 08/11/05 MB per Dennis.
					* determine which of new and old record formats, and load appropriately

					* modified to support addition of Bol_Vics field 1/11/06 MB

					* Define rec pos offsets and rec "versions"
					DO CASE
						CASE .lUseAugust2006Format
							.nAUAD_8_2006_Recs = .nAUAD_8_2006_Recs + 1
							lnAUADOffset = 0
						OTHERWISE
							* orig rec layout
							.nAUADOldRecs = .nAUADOldRecs + 1
							lnAUADOffset = 0
					ENDCASE


					DO CASE
						CASE .lUseAugust2006Format
							m.TRAILERLD = SUBSTR(lcStr,51,6)
							m.BOL_NO = SUBSTR(lcStr,57,16)
							m.SHIPDATE = CTOD(SUBSTR(lcStr,97,2)+"/"+SUBSTR(lcStr,99,2)+"/"+SUBSTR(lcStr,93,4))
							m.COMPANY = SUBSTR(lcStr,157,2)
							m.DEST_LOC = SUBSTR(lcStr,159,10) && NEW for dot-com
							m.CASEID = SUBSTR(lcStr,277,20)
							m.SKU = SUBSTR(lcStr,229,17)
							m.ORDER_TYPE = SUBSTR(lcStr,227,2)
							m.ASNNUM = SUBSTR(lcStr,127,30)
							m.PL_QTY = VAL(SUBSTR(lcStr,246,7))
							m.SORT_TYPE = SUBSTR(lcStr,297,2)
							m.DIVISION = SUBSTR(lcStr,362,3)
							
							IF EMPTY(m.DIVISION) THEN
								DO CASE
									CASE INLIST(m.TNODE,'RR','OB')
										* special handling for Rack Room until division is populated normally
										lcSKUChars = UPPER(SUBSTR(m.SKU,8,2))
										IF lcSKUChars == "CH" THEN
											m.DIVISION = "200" 
										ELSE
											m.DIVISION = "100" 
										ENDIF
									OTHERWISE
										* all other clients
										m.DIVISION = "0" + SUBSTR(m.SKU,2,2)
								ENDCASE
								*.TrackProgress("ERROR: EMPTY DIVISION In File: " + tcUnzippedFile + " at record # " + STR(lnRec),LOGIT+SENDIT)
								*THROW
							ENDIF
					
							.CheckDivision( m.TNODE, m.DIVISION, "AUAD" )					
							
						OTHERWISE
							* orig rec layout , use original positions with no offsets
							m.TRAILERLD = SUBSTR(lcStr,51,6)
							m.BOL_NO = SUBSTR(lcStr,57,16)
							m.SHIPDATE = CTOD(SUBSTR(lcStr,77,2)+"/"+SUBSTR(lcStr,79,2)+"/"+SUBSTR(lcStr,73,4))
							m.COMPANY = SUBSTR(lcStr,127,2)
							m.CASEID = SUBSTR(lcStr,229,20)
							m.SKU = SUBSTR(lcStr,181,17)
							m.ORDER_TYPE = SUBSTR(lcStr,179,2)
							m.ASNNUM = SUBSTR(lcStr,97,30)
							m.PL_QTY = VAL(SUBSTR(lcStr,198,7))
							m.SORT_TYPE = SUBSTR(lcStr,306,2)
					ENDCASE


					* see if we got valid sort type
					IF INLIST(m.SORT_TYPE,'10','11','41','51','35','61','71','72','99') THEN
						.nAUADGoodRecs = .nAUADGoodRecs + 1
					ELSE
						.nAUADBadRecs = .nAUADBadRecs + 1
						.TrackProgress('BAD SORT_TYPE [' + m.SORT_TYPE + '] at AUAD rec # ' + TRANSFORM(lnRec),LOGIT)
					ENDIF
					
					* count sort type for load stats
					DO CASE
						CASE m.SORT_TYPE = '10'
							.nAUAD_10_XdockRecs = .nAUAD_10_XdockRecs + 1
							* new dot-com processing 12/23/08 MB; count the dot-com recs on load
							IF m.DEST_LOC == lcDotComDest_loc THEN
								.nAUAD_DotComRecs = .nAUAD_DotComRecs + 1
							ENDIF
						CASE m.SORT_TYPE = '41'
							.nAUAD_41_ManualRecs = .nAUAD_41_ManualRecs + 1
						CASE m.SORT_TYPE = '51'
							.nAUAD_51_FullRecs = .nAUAD_51_FullRecs + 1
						CASE m.SORT_TYPE = '71'
							.nAUAD_71_TiltRecs = .nAUAD_71_TiltRecs + 1
						CASE m.SORT_TYPE = '99'
							.nAUAD_99_FindRecs = .nAUAD_99_FindRecs + 1
						OTHERWISE
							.nAUAD_XX_UnknRecs = .nAUAD_XX_UnknRecs + 1					
					ENDCASE

					llDoInsert = .T.
					
					* additional checks below that may negate the insert
					
					****************************************************************************************************
					* check pl_qty again, because it may have had asterisks in it, per Doug.
					
					llIsNumeric = .T.
					lcPL_QTY = SUBSTR(lcStr,246,7)
					
					FOR i = 1 TO 7
						IF NOT ISDIGIT(SUBSTR(lcPL_QTY,i,1)) THEN
							llIsNumeric = .F.
							EXIT
						ENDIF
					ENDFOR
					
					IF NOT llIsNumeric THEN										
						llDoInsert = .F.	&&   negate the insert !!!!!!!					
						.TrackProgress('BAD PL_QTY [' + lcPL_QTY + '] at AUAD rec # ' + TRANSFORM(lnRec),LOGIT+SENDIT)					
					ENDIF
					****************************************************************************************************
					

*!*					CASE lcTrans = "ITPC"
*!*						* These recs have multiple sets of 28-byte "case_items" fields following a 3-digit "num_items" field.
*!*						* We need to walk thru and accumulate the QTY field of each case_item,
*!*						* per Dennis. So QTY will be be at offsets ITPC_QTY_OFFSET, ITPC_QTY_OFFSET + 28, ITPC_QTY_OFFSET + 2 * 28, etc., for length of 7.
*!*						lnNumItems = VAL(SUBSTR(lcStr,168,3))
*!*						* get 1st QTY
*!*						lnQTY = VAL(SUBSTR(lcStr,ITPC_QTY_OFFSET,7))
*!*						* 'walk thru' and accumulate additional QTYs, if any
*!*						FOR i = 1 TO (lnNumItems - 1)
*!*							lnQTY = lnQTY + VAL(SUBSTR(lcStr,(ITPC_QTY_OFFSET + (28 * i)),7))
*!*						ENDFOR

*!*						m.SORT_TYPE = SUBSTR(lcStr,52,2)
*!*						m.RCN = VAL(SUBSTR(lcStr,54,8))
*!*						m.COMPANY = SUBSTR(lcStr,62,2)
*!*						m.PONUM = SUBSTR(lcStr,64,8)
*!*						m.DEST_LOC = SUBSTR(lcStr,72,5)
*!*						m.ORDER_TYPE = SUBSTR(lcStr,87,2)
*!*						m.CASEID = SUBSTR(lcStr,93,20)
*!*						m.SKU = SUBSTR(lcStr,175,17)  && this is the 1st SKU of several in ITPC rec.
*!*						m.PL_QTY = lnQTY

*!*						llDoInsert = .T.

				CASE INLIST(lcTrans,"RCRC","RCRZ")
				
					DO CASE
						CASE .lUseAugust2006Format
							m.CYCLE_NO = VAL(SUBSTR(lcStr,5,3))
							m.FILE_SEQ = VAL(SUBSTR(lcStr,8,4))
							m.REC_SEQ = VAL(SUBSTR(lcStr,12,7))
							m.COMPANY = SUBSTR(lcStr,51,2)
							m.PONUM = SUBSTR(lcStr,53,16)
							m.PO_TYPE = SUBSTR(lcStr,69,2)
							m.PO_LINEKEY = SUBSTR(lcStr,71,15)
							m.DIVISION = SUBSTR(lcStr,83,3)
							m.INV_STATUS = SUBSTR(lcStr,86,2)
							m.CASEID = SUBSTR(lcStr,88,20)
							m.CASE_WGT = VAL(SUBSTR(lcStr,108,5))
							m.CASE_CUBE = VAL(SUBSTR(lcStr,113,6))
							m.QTY_CASE = VAL(SUBSTR(lcStr,119,7))   && THIS WILL HOLD UNITS IN THE CARTON FOR RACKROOM/OFF BROADWAY
							m.MELD_DATE = CTOD(SUBSTR(lcStr,130,2)+"/"+SUBSTR(lcStr,132,2)+"/"+SUBSTR(lcStr,126,4))
							m.RCN = VAL(SUBSTR(lcStr,134,8))
							m.SKU = SUBSTR(lcStr,142,17)
							m.UNITS_PCSE = VAL(SUBSTR(lcStr,159,7))
							m.CASEPKCASE = VAL(SUBSTR(lcStr,166,7))
							
							IF EMPTY(m.DIVISION) THEN
								m.DIVISION = "0" + SUBSTR(m.SKU,2,2)
								*.TrackProgress("ERROR: EMPTY DIVISION In File: " + tcUnzippedFile + " at record # " + STR(lnRec),LOGIT+SENDIT)
								*THROW
							ENDIF
							
							***   m.RRUNITS = ??  RRUNITS will hold the units in the case, for the new Units-rated Rack Room/Off Broadway receipt transactions.
							*** NO - using QTY_CASE field instead!
					
							.CheckDivision( m.TNODE, m.DIVISION, "RCRC" )					
							
						OTHERWISE
							m.CYCLE_NO = VAL(SUBSTR(lcStr,5,3))
							m.FILE_SEQ = VAL(SUBSTR(lcStr,8,4))
							m.REC_SEQ = VAL(SUBSTR(lcStr,12,7))
							m.COMPANY = SUBSTR(lcStr,51,2)
							m.PONUM = SUBSTR(lcStr,53,8)
							m.PO_TYPE = SUBSTR(lcStr,61,2)
							m.PO_LINEKEY = SUBSTR(lcStr,63,15)
							m.INV_STATUS = SUBSTR(lcStr,78,2)
							m.CASEID = SUBSTR(lcStr,80,20)
							m.CASE_WGT = VAL(SUBSTR(lcStr,100,5))
							m.CASE_CUBE = VAL(SUBSTR(lcStr,105,6))
							m.QTY_CASE = VAL(SUBSTR(lcStr,111,7))
							m.MELD_DATE = CTOD(SUBSTR(lcStr,122,2)+"/"+SUBSTR(lcStr,124,2)+"/"+SUBSTR(lcStr,118,4))
							m.RCN = VAL(SUBSTR(lcStr,126,8))
							m.SKU = SUBSTR(lcStr,134,17)
							m.UNITS_PCSE = VAL(SUBSTR(lcStr,151,7))
							* 12/14/04 MB - num_cases is different from case_pack_case!
							*!*						m.NUM_CASES = VAL(SUBSTR(lcStr,158,7))
							m.CASEPKCASE = VAL(SUBSTR(lcStr,158,7))
					ENDCASE

					***************************************************************************
					* Do calculations to support "receiving" part of weekly flash report -- per Dennis 2/6/07 MB

					m.INVQTY = m.UNITS_PCSE * m.CASEPKCASE

					* but need to make INVQTY negative if deleted rec, per Dennis 2/28/07 MB
					IF lcTrans = "RCRZ" THEN
						m.INVQTY = -1 * ABS(m.INVQTY)
					ENDIF

					* if ABS(INVQTY) = 1 then add it to cartons_in
					*m.CTNSIN = IIF( m.INVQTY = 1, 1, 0 )
					m.CTNSIN = IIF( ABS(m.INVQTY) = 1, m.INVQTY, 0 )

					* if ABS(INVQTY) > 1 then add/SUBTRACT units (units = invqty * prepack)
					m.UNITSIN = IIF( ABS(m.INVQTY) > 1, m.INVQTY * m.PREPACK, 0 )
					***************************************************************************

					DO CASE
						CASE lcTrans = "RCRC"
							m.NUM_CASES = 1  &&  always, per Dennis 12/14/04 MB
							.nRCRCTotalRecs = .nRCRCTotalRecs + 1
							INSERT INTO RCRCHIST FROM MEMVAR
						CASE lcTrans = "RCRZ"
							* make NUM_CASES negative because it represents a deletion of RCRC rec 12/6/04 MB
							*m.NUM_CASES = -1 * ABS(m.NUM_CASES)
							m.NUM_CASES = -1  &&  always, per Dennis 12/14/04 MB
							*!*								INSERT INTO RCRZHIST FROM MEMVAR
							* this is new 12/6/04 - allowing us to total num_cases in RCRC and so account for deletions.
							INSERT INTO RCRCHIST FROM MEMVAR
						OTHERWISE
							* NOTHING
					ENDCASE

				CASE INLIST(lcTrans,"IARN","IAAD")  && PER DOUG 1/5/2016, THESE SHARE THE SAME FORMAT UP UNTIL THE FINAL FOUR FIELDS, WHICH ARE BLANK FOR IAAD
				
					* "IARN" - These are Inventory Adjustment return receipt recs from San Pedro to ML.
							* They will be treated as a new type of Receipt, with their own table for storage - RETURNS.DBF
							
					* "IAAD" - new full case shipping transaction for Forsan 12/15/2015
						* Altho they are not returns, we will store them in that table because they match its structure.
						
					m.CYCLE_NO = VAL(SUBSTR(lcStr,5,3))
					m.FILE_SEQ = VAL(SUBSTR(lcStr,8,4))
					m.REC_SEQ = VAL(SUBSTR(lcStr,12,7))
					
					m.COMPANY = SUBSTR(lcStr,39,2)
					m.PONUM = SUBSTR(lcStr,41,16)
					
					*m.PO_TYPE = SUBSTR(lcStr,69,2)
					*m.PO_LINEKEY = SUBSTR(lcStr,71,15)
					
					m.SKU = SUBSTR(lcStr,57,17)
					m.UNITS_PCSE = VAL(SUBSTR(lcStr,74,7))
					m.CASEPKCASE = VAL(SUBSTR(lcStr,81,7))
					
					m.INV_STATUS = SUBSTR(lcStr,90,2)
					
					m.UNIT_QTY = VAL(SUBSTR(lcStr,93,7))
					
					m.CASEID = SUBSTR(lcStr,108,20)
					
					*!*	* RUN-ONCE code to correct missing caseid from Doug
					*!*	IF (DATE() = {^2016-02-03}) AND (lcTrans = "IAAD") THEN
					*!*		* populate caseid with 661 distinct values, because Doug says there are 661 cases
					*!*		* there are 661 records coming in so we can just use a counter
					*!*		lnDummyCounter = lnDummyCounter + 1
					*!*		m.CASEID = "DMY" + ALLTRIM(STR(lnDummyCounter,6,0))
					*!*	ENDIF
					
					*m.TRAILER_NO = SUBSTR(lcStr,148,12)				
					
					IF EMPTY(m.DIVISION) THEN
						m.DIVISION = "0" + SUBSTR(m.SKU,2,2)
						*.TrackProgress("ERROR: EMPTY DIVISION In File: " + tcUnzippedFile + " at record # " + STR(lnRec),LOGIT+SENDIT)
						*THROW
					ENDIF
							
					***************************************************************************

					m.INVQTY = m.UNITS_PCSE * m.CASEPKCASE

*!*						m.CTNSIN = IIF( ABS(m.INVQTY) = 1, m.INVQTY, 0 )

*!*						* if ABS(INVQTY) > 1 then add/SUBTRACT units (units = invqty * prepack)
*!*						m.UNITSIN = IIF( ABS(m.INVQTY) > 1, m.INVQTY * m.PREPACK, 0 )
					***************************************************************************

					m.NUM_CASES = 1  &&  always, per Dennis 12/14/04 MB
					
					DO CASE
						CASE lcTrans = "IARN"
						
							.nIARNTotalRecs = .nIARNTotalRecs + 1
							
							m.TRAILER_NO = SUBSTR(lcStr,148,12)	&& THIS is not populated for IAAD...
										
							INSERT INTO RETURNS FROM MEMVAR
							
						CASE lcTrans = "IAAD"
						
							.nIAADTotalRecs = .nIAADTotalRecs + 1
							INSERT INTO IAADXFER FROM MEMVAR
							
						OTHERWISE
							*NOTHING
					ENDCASE
			
					*.CheckDivision( m.TNODE, m.DIVISION, "IARN" )					
					.CheckDivision( m.TNODE, m.DIVISION, lcTrans )					
					

				CASE lcTrans = "RDCA"

					.nRDCATotalRecs = .nRDCATotalRecs + 1
					
					DO CASE
						CASE .lUseAugust2006Format
							m.DEST_LOC = SUBSTR(lcStr,63,10) && NEW for dot-com
							m.DIVISION = SUBSTR(lcStr,84,3)
							m.CASEID = SUBSTR(lcStr,89,20)
							m.RCN = VAL(SUBSTR(lcStr,158,8))
					
							.CheckDivision( m.TNODE, m.DIVISION, "RDCA" )
												
						OTHERWISE
							m.CASEID = SUBSTR(lcStr,76,20)
					ENDCASE
					
					* new dot-com processing 12/23/08 MB; count the dot-com recs on load
					IF m.DEST_LOC == lcDotComDest_loc THEN
						.nRDCA_DotComRecs = .nRDCA_DotComRecs + 1
					ENDIF

					INSERT INTO RDCAHIST FROM MEMVAR

				CASE INLIST(lcTrans,"RATO","RATC")

					DO CASE
						CASE .lUseAugust2006Format
							m.ASNNUM = SUBSTR(lcStr,51,30)
							m.CONTTYPE = SUBSTR(lcStr,190,1)
							m.RCN = VAL(SUBSTR(lcStr,196,8))
							* get Yard check-in Datetime
							lnYYYYMMDDStart = 95
							lnHHMMSSStart = 89
						OTHERWISE
							m.ASNNUM = SUBSTR(lcStr,51,30)
							m.CONTTYPE = SUBSTR(lcStr,190,1)
							m.RCN = VAL(SUBSTR(lcStr,197,8))
							* get Yard check-in Datetime
							lnYYYYMMDDStart = 95
							lnHHMMSSStart = 89
					ENDCASE

					m.YARDINDT = ;
						CTOT(SUBSTR(lcStr,lnYYYYMMDDStart,4) + "-" + SUBSTR(lcStr,lnYYYYMMDDStart+4,2) + "-" + SUBSTR(lcStr,lnYYYYMMDDStart+6,2) + ;
						"T" + ;
						SUBSTR(lcStr,lnHHMMSSStart,2) + ":" + SUBSTR(lcStr,lnHHMMSSStart+2,2) + ":" + SUBSTR(lcStr,lnHHMMSSStart+4,2))

					DO CASE
						CASE lcTrans = "RATO"

							INSERT INTO RATOHIST FROM MEMVAR

						CASE lcTrans = "RATC"

							* also get RCN-COMPLETED DATETIME
							DO CASE
								CASE .lUseAugust2006Format
									lnYYYYMMDDStart = 158
									lnHHMMSSStart = 166
								OTHERWISE
									lnYYYYMMDDStart = 158
									lnHHMMSSStart = 166
							ENDCASE

							m.RCNCMPDT = ;
								CTOT(SUBSTR(lcStr,lnYYYYMMDDStart,4) + "-" + SUBSTR(lcStr,lnYYYYMMDDStart+4,2) + "-" + SUBSTR(lcStr,lnYYYYMMDDStart+6,2) + ;
								"T" + ;
								SUBSTR(lcStr,lnHHMMSSStart,2) + ":" + SUBSTR(lcStr,lnHHMMSSStart+2,2) + ":" + SUBSTR(lcStr,lnHHMMSSStart+4,2))

							INSERT INTO RATCHIST FROM MEMVAR

						OTHERWISE
							* NOTHING
					ENDCASE
					
				CASE INLIST(lcTrans,"IAIA","TCTC","ITPC","SESE")
					* Nothing - we will just ignore these trans types.

				OTHERWISE
					* LOG UNEXPECTED TRANSACTION TYPE
					SELECT CURNEWTRANS
					LOCATE FOR CTRANS = lcTrans
					IF NOT FOUND() THEN
						m.CTRANS = lcTrans
						INSERT INTO CURNEWTRANS FROM MEMVAR
					ENDIF

			ENDCASE

			IF llDoInsert THEN

				INSERT INTO TRANSTABLE FROM MEMVAR

				* activate block below if we get an error on the insert!

				*!*					TRY
				*!*						INSERT INTO TRANSTABLE FROM MEMVAR
				*!*					CATCH TO loError
				*!*						DISPLAY MEMORY TO FILE (.cMemoDumpFile) ADDITIVE NOCONSOLE
				*!*						.TrackProgress('There was an error.',LOGIT+SENDIT)
				*!*						.TrackProgress('The Error # is: ' + TRANSFORM(loError.ErrorNo), LOGIT+SENDIT)
				*!*						.TrackProgress('The Error Message is: ' + TRANSFORM(loError.MESSAGE), LOGIT+SENDIT)
				*!*						.TrackProgress('The Error occurred in Program: ' + TRANSFORM(loError.PROCEDURE), LOGIT+SENDIT)
				*!*						.TrackProgress('The Error occurred at line #: ' + TRANSFORM(loError.LINENO), LOGIT+SENDIT)
				*!*						.TrackProgress('About to THROW this ERROR', LOGIT+SENDIT)
				*!*						THROW loError
				*!*					ENDTRY
			ENDIF

		ENDDO  &&  WHILE NOT FEOF(lnHandle)
		=FCLOSE(lnHandle)
		
		
		***************************************************************************
		* do SQL to get summary stats from data just loaded, BY COMPANY (TNODE)
		
		SELECT TNODE, TRANS, COUNT(*) AS NUM_RECS ;
			FROM TRANSTABLE ;
			INTO CURSOR CURTRANSSUMMARY ;
			WHERE (TDATETIME >= ltMinDateTime) AND (TDATETIME <= ltMaxDateTime) ;
			GROUP BY 1, 2 ;
			ORDER BY 1, 2
		
		SELECT CURTRANSSUMMARY
		COPY TO C:\TEMPFOX\TRANSSUMMARY.XLS XL5
		.AddAttachment( "C:\TEMPFOX\TRANSSUMMARY.XLS" )
		
		SELECT TNODE, COUNT(*) AS NUM_RECS ;
			FROM RCRCHIST ;
			INTO CURSOR RCRCSUMMARY ;
			WHERE (TDATETIME >= ltMinDateTime) AND (TDATETIME <= ltMaxDateTime) ;
			GROUP BY 1 ;
			ORDER BY 1
		
		SELECT RCRCSUMMARY
		COPY TO C:\TEMPFOX\RCRCSUMMARY.XLS XL5
		.AddAttachment( "C:\TEMPFOX\RCRCSUMMARY.XLS" )
		
		SELECT TNODE, DIVISION, COUNT(*) AS NUM_RECS ;
			FROM TRANSTABLE ;
			INTO CURSOR CURDIVSUMMARY ;
			WHERE (TDATETIME >= ltMinDateTime) AND (TDATETIME <= ltMaxDateTime) ;
			AND (TNODE <> 'FL') ;
			GROUP BY 1, 2 ;
			ORDER BY 1, 2
		
		SELECT CURDIVSUMMARY 
		COPY TO C:\TEMPFOX\DIVSSUMMARY.XLS XL5
		.AddAttachment( "C:\TEMPFOX\DIVSSUMMARY.XLS" )
		
		
		IF FILE("C:\TEMPFOX\CURNEWTRANS.XLS") THEN
			DELETE FILE C:\TEMPFOX\CURNEWTRANS.XLS
		ENDIF

		SELECT TNODE, TRANS, COUNT(*) AS NUM_RECS ;
			FROM RETURNS ;
			INTO CURSOR CURRETURNSSUMMARY ;
			WHERE (TDATETIME >= ltMinDateTime) AND (TDATETIME <= ltMaxDateTime) ;
			GROUP BY 1, 2 ;
			ORDER BY 1, 2
		
		SELECT CURRETURNSSUMMARY
		COPY TO C:\TEMPFOX\RETURNSSUMMARY.XLS XL5
		.AddAttachment( "C:\TEMPFOX\RETURNSSUMMARY.XLS" )
		

		SELECT TNODE, TRANS, COUNT(*) AS NUM_RECS ;
			FROM IAADXFER ;
			INTO CURSOR CURIAADXFERSUMMARY ;
			WHERE (TDATETIME >= ltMinDateTime) AND (TDATETIME <= ltMaxDateTime) ;
			GROUP BY 1, 2 ;
			ORDER BY 1, 2
		
		SELECT CURIAADXFERSUMMARY
		COPY TO C:\TEMPFOX\IAADXFERSSUMMARY.XLS XL5
		.AddAttachment( "C:\TEMPFOX\IAADXFERSSUMMARY.XLS" )

		
		IF USED('CURNEWTRANS') THEN
			* REMOVE ANY EMPTY RECORDS
			SELECT CURNEWTRANS
			SCAN
				IF EMPTY(CTRANS) THEN
					DELETE IN CURNEWTRANS
				ENDIF
			ENDSCAN
		
			SELECT CURNEWTRANS
			LOCATE
			IF NOT EOF('CURNEWTRANS') THEN
				SELECT CURNEWTRANS
				COPY TO C:\TEMPFOX\CURNEWTRANS.XLS XL5
				.AddAttachment( "C:\TEMPFOX\CURNEWTRANS.XLS" )		
			ENDIF
			USE IN CURNEWTRANS
		ENDIF
		***************************************************************************		
		

		.TrackProgress('=========================================================================', LOGIT+SENDIT)
		.TrackProgress('Loaded ' + tcUnzippedFile, LOGIT+SENDIT)
		.TrackProgress('=========================================================================', LOGIT+SENDIT)

		RETURN llRetVal
	ENDFUNC  &&  LoadTransactionFile




	FUNCTION MakeBill
		LPARAMETERS tdTransDate
		***********************************************************************************
		***********************************************************************************

		WITH THIS

			*TRY
			
				.TrackProgress("-------------- " + .cClient + " Make Bill Process Started ---------------", LOGIT+SENDIT)
				
				*.ShowDataFiles()
				IF .lTestMode THEN
					.TrackProgress("-----------        TEST MODE        -------------", LOGIT)
				ENDIF
				*.TrackProgress("", LOGIT+SENDIT)
				IF .lAutoMode THEN
					.TrackProgress("PROCESS MODE = AUTOMATIC", LOGIT)
				ELSE
					.TrackProgress("PROCESS MODE = MANUAL", LOGIT)
				ENDIF
				
				IF .lSummaryOnly THEN
					.TrackProgress("SUMMARY ONLY MODE = ON", LOGIT)
				ENDIF
				
				IF .lFilterCURRCNsByDiv THEN
					.TrackProgress("Filter CURRCNs By Div = TRUE", LOGIT+SENDIT)
				ELSE
					.TrackProgress("Filter CURRCNs By Div = FALSE", LOGIT+SENDIT)
				ENDIF
				
				IF .lDotComProcessing THEN
					.TrackProgress("DOT-COM LOGIC = ACTIVE", LOGIT+SENDIT)
				ENDIF

				IF .lEnableTransferRCNProcessing THEN
					.TrackProgress("SEREC TRANSFER LOGIC = ACTIVE", LOGIT)
				ENDIF

				IF .lSplitPickNPackInvoiceDetail THEN
					.TrackProgress("Break Out Pick and Pack Charges = TRUE", LOGIT+SENDIT)
				ELSE
					.TrackProgress("Break Out Pick and Pack Charges = FALSE", LOGIT+SENDIT)
				ENDIF

				IF .lUseTestDataForBilling THEN
					.TrackProgress("USING TEST BILLING DATA", LOGIT+SENDIT)
					.TrackProgress("TURNED INVOICING OFF", LOGIT+SENDIT)
				ENDIF
				
				_SCREEN.CAPTION = .cClient + " Make Bill Process"
				
				* save param to properties
				.dTransDate = tdTransDate
				
				*******************************************************************
				* assign rates for trans date
				.SetRates( tdTransDate )
				*******************************************************************
				
				.TrackProgress("tdTransDate = " + TRANSFORM(tdTransDate), LOGIT+SENDIT)
				.TrackProgress(".nReceiptCrossDockPerCtnRate = " + TRANSFORM(.nReceiptCrossDockPerCtnRate), LOGIT+SENDIT)
				.TrackProgress(".nReceiptNonXDockPerCtnRate = " + TRANSFORM(.nReceiptNonXDockPerCtnRate), LOGIT+SENDIT)
				.TrackProgress(".nTiltPerUnitRate = " + TRANSFORM(.nTiltPerUnitRate), LOGIT+SENDIT)
				.TrackProgress(".nFullCasePerCtnRate = " + TRANSFORM(.nFullCasePerCtnRate), LOGIT+SENDIT)
				.TrackProgress(".nTiltResidualsPerCartonRate = " + TRANSFORM(.nTiltResidualsPerCartonRate), LOGIT+SENDIT)
				.TrackProgress(".nReturnReceiptPerCtnRate = " + TRANSFORM(.nReturnReceiptPerCtnRate), LOGIT+SENDIT)
				.TrackProgress(".nReceiptNonXDockPerUNITRate = " + TRANSFORM(.nReceiptNonXDockPerUNITRate), LOGIT+SENDIT)
				
				
				
				******************************************************************
				
				
				.TrackProgress("Making Bill for Transaction Date: " + DTOC(tdTransDate), LOGIT+SENDIT)
				.TrackProgress("", LOGIT)
				LOCAL lnHandle, lcStr, lcTrans, lnRec, lcFType, lcTType, ltDateTime, lnSKUCnt, lnNumItems, i, lnQTY, llDoInsert, llRetVal
				LOCAL llPDFReportWasCreated, lcCentury, lcRootPDFFilename, lcFullPDFFilename, lnRCRCCount, ltMinDateTime, ltMaxDateTime
				LOCAL loError, lnRCRZCount, lnCASEID, lnRCRCUNITS, lnRCRZUNITS, lnDeletedCount, lcExtraWhere, lcDotComDest_loc
				LOCAL lcTNodeFilter, lcExcelSummaryRow, llCapex
				LOCAL lcAUAD_SQL_Where, lcRCRC_SQL_Where, lcRDCA_SQL_Where, lcRIRI_SQL_Where, lcIARN_SQL_Where, lcIAAD_SQL_Where
				LOCAL lnCurrentRCN, lnRRUNITS, lnForsanIAADXferCtns, lnCASEIDXDOCK, lnTiltDollars, llMain, llSplit
				LOCAL lnRoundedTilt, lnRoundedTotalMinusTilt, lnMainTotal						
				
				lcDotComDest_loc = .cDotComDest_loc				
				llRetVal = .T.
				lcTNodeFilter = .cTNodeFilter
				
				lcAUAD_SQL_Where = .cAUAD_SQL_Where
				lcRCRC_SQL_Where = .cRCRC_SQL_Where
				lcRDCA_SQL_Where = .cRDCA_SQL_Where
				lcRIRI_SQL_Where = .cRIRI_SQL_Where
				lcIARN_SQL_Where = .cIARN_SQL_Where
				lcIAAD_SQL_Where = .cIAAD_SQL_Where

				* report variables
				* quantity vars
				PRIVATE pnShippedCrossDockCtns, pnSKU_0_Ctns, pnSKU_21_Ctns, pnSKU_31_Ctns, pnSKU_41_Ctns, pnFullCaseCtns
				PRIVATE pnTiltUnits, pnManualUnits, pnFindingsCtns, pnTotalShipped, pnTotalReceipt, pnReceiptCrossDockCtns
				PRIVATE pnReceiptDotComXDockCtns, pnShippedDotComXDockCtns, pnReceiptNonXDockCtns, pnSkuReceiptNonXDockCtns
				PRIVATE pnTotalReceiptCAPEX, pnTotalReceiptNonCapex, pnTotalShippedNonCapex, pnTotalShippedCAPEX, pnTiltResidualUnits
				PRIVATE pnReceiptNonXDockTRANSFERCtns, pnReturnReceiptCtns, pnTotalReceiptRETURNS, pn41FullCaseCtns, pnReceiptNonXDockUNITS
				PRIVATE pnTiltResidualCartons
				
				PRIVATE pnTotalCharges, pnTotalChargesNonCapex, pnTotalChargesCAPEX, pcBillFor, pcTitle
				pcBillFor = "For Transaction Date: " + DTOC(tdTransDate)
				pcTitle = .cClient + " BILL SUMMARY"

				* init quantity vars - they will actually be counted in later processing
				STORE 0 TO pnShippedCrossDockCtns, pnSKU_0_Ctns, pnSKU_21_Ctns, pnSKU_31_Ctns, pnSKU_41_Ctns, pnFullCaseCtns, pnTiltUnits, pnManualUnits
				STORE 0 TO pnFindingsCtns, pnReceiptCrossDockCtns, pnSkuReceiptNonXDockCtns, pnReceiptDotComXDockCtns, pnShippedDotComXDockCtns
				STORE 0 TO pnReceiptNonXDockCtns, pnTotalReceiptCAPEX, pnTotalReceiptNonCapex, pnTotalShippedNonCapex, pnTotalShippedCAPEX, pnTiltResidualUnits
				STORE 0 TO pnReceiptNonXDockTRANSFERCtns, pnReturnReceiptCtns, pn41FullCaseCtns, pnReceiptNonXDockUNITS, pnTiltResidualCartons

				* rate vars
				PRIVATE pnShippedCrossDockPerCtnRate, pnSKU_0_PerCtnRate, pnSKU_21_PerCtnRate, pnSKU_31_PerCtnRate, pnSKU_41_PerCtnRate, pnFullCasePerCtnRate
				PRIVATE pnTiltPerUnitRate, pnManualPerUnitRate, pnFindingsPerCtnRate, pnReceiptCrossDockPerCtnRate, pnTiltResidualsPerUnitRate
				PRIVATE pnReceiptNonXDockPerCtnRate, pnReceiptCrossDockCAPEXPerCtnRate, pnShippedCrossDockCAPEXPerCtnRate, pnReceiptNonXDockTRANSFERPerCtnRate 
				PRIVATE pnReturnReceiptPerCtnRate, pnReceiptNonXDockPerUNITRate, pnTiltResidualsPerCartonRate

				* assign the different rate types below
				pnReceiptCrossDockPerCtnRate	 = .nReceiptCrossDockPerCtnRate
				pnReceiptNonXDockPerCtnRate = .nReceiptNonXDockPerCtnRate
				pnReceiptNonXDockTRANSFERPerCtnRate = .nReceiptNonXDockTRANSFERPerCtnRate
				pnReturnReceiptPerCtnRate = .nReturnReceiptPerCtnRate
				
				pnReceiptCrossDockCAPEXPerCtnRate = .nReceiptCrossDockCAPEXPerCtnRate
				pnReceiptNonXDockCAPEXPerCtnRate = .nReceiptNonXDockCAPEXPerCtnRate
				
				pnSKU_0_PerCtnRate        = .nSKU_0_PerCtnRate
				pnSKU_21_PerCtnRate       = .nSKU_21_PerCtnRate
				pnSKU_31_PerCtnRate       = .nSKU_31_PerCtnRate
				pnSKU_41_PerCtnRate       = .nSKU_41_PerCtnRate
				
				pnShippedCrossDockPerCtnRate     = .nShippedCrossDockPerCtnRate
				pnShippedCrossDockCAPEXPerCtnRate = .nShippedCrossDockCAPEXPerCtnRate
				
				pnTiltPerUnitRate         	= .nTiltPerUnitRate
				pnTiltResidualsPerUnitRate 	= .nTiltResidualsPerUnitRate
				pnFullCasePerCtnRate      	= .nFullCasePerCtnRate
				
				pnManualPerUnitRate       = .nManualPerUnitRate
				pnFindingsPerCtnRate      = .nFindingsPerCtnRate
				
				pnReceiptNonXDockPerUNITRate = .nReceiptNonXDockPerUNITRate
				pnTiltResidualsPerCartonRate = .nTiltResidualsPerCartonRate

				* open tables
				USE (.cTransTable) IN 0 ALIAS TRANSTABLE EXCLUSIVE

				* RETURNS stores the return receipt transaction.
				USE (.cRETURNSTable) AGAIN IN 0 ALIAS RETURNS

				* IAADXFER stores the transfers to shipping transactions, currently for Forsan.
				USE (.cIAADXFERTable) AGAIN IN 0 ALIAS IAADXFER
				
				IF .lTestInvoice THEN
					.cHistoryTable = 'F:\SEARS\PMDATA\FTHISTORYTEST'
				ENDIF

				* HISTORYTABLE stores the bill variables per process run.
				USE (.cHistoryTable) AGAIN IN 0 ALIAS HISTORYTABLE

				* RCRCHIST stores all RCRC recs processed currently and previously.
				USE (.cRCRCTable) AGAIN IN 0 ALIAS RCRCHIST EXCLUSIVE

				* RDCAHIST stores all RDCA recs processed currently and previously.
				* (RDCA recs identify XDOCK received cases.)
				USE (.cRDCATable) AGAIN IN 0 ALIAS RDCAHIST EXCLUSIVE
					
				IF .lTestInvoice THEN
					.cINVHISTTable = 'F:\SEARS\PMDATA\DAILYINVHISTTEST'
				ENDIF
				
				* INVHIST TABLE holds "official" invoiced counts by trans date
				USE (.cINVHISTTable) AGAIN IN 0 ALIAS INVHIST EXCLUSIVE				
				
				USE (.cRCNTRANSFERTable) AGAIN IN 0 ALIAS CURTRANSFERRCNS EXCLUSIVE				

				*WAIT WINDOW "Creating TEMPCURSOR for date: " + DTOC(tdTransDate) + "..." NOWAIT
				.TrackProgress("Creating TEMPCURSOR for date: " + DTOC(tdTransDate) + "...", LOGIT+NOWAITIT)

				* create temp cursor for passed date
				IF USED('TEMPCURSOR') THEN
					USE IN TEMPCURSOR
				ENDIF

				lcExtraWhere = .cExtraWhere

				SELECT * ;
					FROM TRANSTABLE ;
					INTO CURSOR TEMPCURSOR ;
					WHERE TTOD(TDATETIME) = tdTransDate ;
					AND TNODE = lcTNodeFilter ;
					&lcExtraWhere ;
					READWRITE

				*WAIT WINDOW "Indexing TEMPCURSOR..." NOWAIT
				.TrackProgress("Indexing TEMPCURSOR...", LOGIT+NOWAITIT)

				SELECT TEMPCURSOR
				INDEX ON TRAILERLD 			TAG T1
				INDEX ON TTOD(TDATETIME) 	TAG T2
				INDEX ON TRANS 				TAG T3
				INDEX ON RCN 				TAG T4
				INDEX ON CONTAINER 			TAG T5
				INDEX ON COMPANY 			TAG T6
				INDEX ON SORT_TYPE 			TAG T7
				INDEX ON SKU 				TAG T8
				INDEX ON PONUM 				TAG T9
				INDEX ON CASEID 			TAG T10
				INDEX ON DEST_LOC 			TAG T11

				***********************************************************************************
				***********************************************************************************
				* RECEIPT TRANSACTIONS
				* Receiving/putaway:
				* containers received and put into storage.
				***********************************************************************************
				***********************************************************************************
				* Break down by # of SKUs per RCN.
				* look at the RBPC transactions; count the number
				* of Company/PO/SKU combinations in each group of Trailer/BOL

				.TrackProgress("Calculating SKU Breakdown and xdock receipt data...", LOGIT+NOWAITIT)

				* 1st get distinct RCN list for RBPC
				IF USED('curRCNs') THEN
					USE IN CURRCNS
				ENDIF

*************************************************************************************************************************************************************************
*************************************************************************************************************************************************************************

				IF .lFilterCURRCNsByDiv THEN
				
					SELECT tdTransDate AS TRANSDATE, RCN, CONTAINER, COMPANY, 00000000 AS UNITS, ;
						00000000 AS SKUSPERRCN, 00000000 AS CARTONS, ;
						00000000 AS XDUNITS, 00000000 AS XDCARTONS, ;
						00000000 AS XFERCTNS, ;
						00000000 AS RRUNITS, ;
						0000000000.000 AS SKU0, ;
						0000000000.000 AS SKU21, ;
						0000000000.000 AS SKU31, ;
						0000000000.000 AS SKU41 ;
						FROM TEMPCURSOR ;
						INTO CURSOR CURRCNS ;
						WHERE TRANS = "RBPC" ;
						AND RCN IN ( SELECT RCN FROM RCRCHIST WHERE (TNODE = lcTNodeFilter) &lcRCRC_SQL_Where. ) ;
						GROUP BY TRANSDATE, RCN, CONTAINER, COMPANY ;
						ORDER BY TRANSDATE, RCN, CONTAINER, COMPANY	;
						READWRITE
					
				ELSE
					* original, pre 8/20/14 logic
				
					SELECT tdTransDate AS TRANSDATE, RCN, CONTAINER, COMPANY, 00000000 AS UNITS, ;
						00000000 AS SKUSPERRCN, 00000000 AS CARTONS, ;
						00000000 AS XDUNITS, 00000000 AS XDCARTONS, ;
						00000000 AS XFERCTNS, ;
						00000000 AS RRUNITS, ;
						0000000000.000 AS SKU0, ;
						0000000000.000 AS SKU21, ;
						0000000000.000 AS SKU31, ;
						0000000000.000 AS SKU41 ;
						FROM TEMPCURSOR ;
						INTO CURSOR CURRCNS ;
						WHERE TRANS = "RBPC" ;
						GROUP BY TRANSDATE, RCN, CONTAINER, COMPANY ;
						ORDER BY TRANSDATE, RCN, CONTAINER, COMPANY	;
						READWRITE
					
				ENDIF


				* now get count of sku combinations per RCN, and count of cartons for matching RCNs from RCRCHIST

				*****************************************************
				*****************************************************
				*****************************************************
				*
				* NOTE: per Dennis, we now need to exclude any SKU, COMPANY, PONUM combinations which were 100% cross-dock
				* from the SKU Count! In other words, we will need to calc the 100% cross-dock # and subtract from lnSKUCnt.
				* 09/22/04 MB
				*
				*****************************************************
				*****************************************************
				*****************************************************

				STORE 0 TO pnSKU_0_Ctns, pnSKU_21_Ctns, pnSKU_31_Ctns, pnSKU_41_Ctns, pnSkuReceiptNonXDockCtns
				SELECT CURRCNS
				.TrackProgress("BEGIN Calculating SKU Breakdown and xdock receipt data: MAIN SCAN LOOP...", LOGIT)
				SCAN
*!*						IF USED('curSKUS') THEN
*!*							USE IN curSKUS
*!*						ENDIF

					* revised to use RCRC with 1 and -1 for NUM_CASES depending on whether it was as deleted rec from RCRZ
					* Note that "CASEID NOT IN (SELECT CASEID FROM RDCAHIST)" excludes XDOCK receipts from the count.

*!*						IF .lUseRCNForRDCAKey THEN
*!*							* make RCN part of RDCAHIST key

*!*							SELECT SKU, COMPANY, PONUM, SUM(NUM_CASES) AS RCRCCOUNT ;
*!*								FROM RCRCHIST ;
*!*								INTO CURSOR curSKUS ;
*!*								WHERE RCN = CURRCNS.RCN ;
*!*								AND CASEID NOT IN (SELECT CASEID FROM RDCAHIST WHERE RCN = CURRCNS.RCN) ;
*!*								GROUP BY 1, 2, 3 ;
*!*								HAVING RCRCCOUNT > 0 ;
*!*								READWRITE

*!*						ELSE

*!*							SELECT SKU, COMPANY, PONUM, SUM(NUM_CASES) AS RCRCCOUNT ;
*!*								FROM RCRCHIST ;
*!*								INTO CURSOR curSKUS ;
*!*								WHERE RCN = CURRCNS.RCN ;
*!*								AND CASEID NOT IN (SELECT CASEID FROM RDCAHIST) ;
*!*								GROUP BY 1, 2, 3 ;
*!*								HAVING RCRCCOUNT > 0 ;
*!*								READWRITE

*!*						ENDIF

					***********************************************************************************
					***********************************************************************************
					* calculate NON-XDOCK receipt data
					***********************************************************************************
					***********************************************************************************
					
					
					***********************************************************************************
					** added 6/18/2013 to determine if this is a serec transfer which needs to be counted and priced separately from other receipt non-xdocks
					lnCurrentRCN = CURRCNS.RCN
					*.TrackProgress("#####################    lnCurrentRCN = " + TRANSFORM(lnCurrentRCN), LOGIT+SENDIT)
					SELECT CURTRANSFERRCNS
					LOCATE FOR RCN = lnCurrentRCN
					.lTransferRCN = FOUND()	
					LOCATE	
					IF .lTransferRCN THEN
						.TrackProgress("##################### Transfer RCN = " + TRANSFORM(lnCurrentRCN), LOGIT+SENDIT)
					ENDIF			
					***********************************************************************************
					
					
					IF .lDoSkuBreakdown THEN
					
						IF USED('curSKUS') THEN
							USE IN curSKUS
						ENDIF
					
						SELECT SKU, COMPANY, PONUM, SUM(NUM_CASES) AS RCRCCOUNT ;
							FROM RCRCHIST ;
							INTO CURSOR curSKUS ;
							WHERE RCN = CURRCNS.RCN ;
							&lcRCRC_SQL_Where. ;
							AND CASEID NOT IN (SELECT CASEID FROM RDCAHIST WHERE RCN = CURRCNS.RCN &lcRDCA_SQL_Where.) ;
							GROUP BY 1, 2, 3 ;
							HAVING RCRCCOUNT > 0 ;
							READWRITE					

						lnSKUCnt = RECCOUNT('curSKUS')

						SELECT curSKUS
						LOCATE

						* update curRCNs
						REPLACE CURRCNS.SKUSPERRCN WITH lnSKUCnt
					
					ENDIF && .lDoSkuBreakdown

					IF USED('curRCRCCount') THEN
						USE IN curRCRCCount
					ENDIF

					**!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
					**************************************************************************************************
					**************************************************************************************************
					*** because of change from num_cases to CASEPKCASE below - to rerun any bill before 12/18/04
					*** that bill MUST FIRST BE RELOADED so that CASEPKCASE is properly populated. !!!!!!!!!!!!!!!!!!!
					**************************************************************************************************
					**************************************************************************************************
					**!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

*!*						IF .lUseRCNForRDCAKey THEN
*!*							* make RCN part of RDCAHIST key

*!*							SELECT SUM(NUM_CASES) AS RCRCCOUNT, SUM(UNITS_PCSE * CASEPKCASE) AS RCRCUNITS ;
*!*								FROM RCRCHIST ;
*!*								INTO CURSOR curRCRCCount ;
*!*								WHERE RCN = CURRCNS.RCN ;
*!*								AND CASEID NOT IN (SELECT CASEID FROM RDCAHIST WHERE RCN = CURRCNS.RCN) ;
*!*								AND COMPANY = CURRCNS.COMPANY

*!*						ELSE

*!*							SELECT SUM(NUM_CASES) AS RCRCCOUNT, SUM(UNITS_PCSE * CASEPKCASE) AS RCRCUNITS ;
*!*								FROM RCRCHIST ;
*!*								INTO CURSOR curRCRCCount ;
*!*								WHERE RCN = CURRCNS.RCN ;
*!*								AND CASEID NOT IN (SELECT CASEID FROM RDCAHIST) ;
*!*								AND COMPANY = CURRCNS.COMPANY

*!*						ENDIF



					SELECT SUM(NUM_CASES) AS RCRCCOUNT, SUM(UNITS_PCSE * CASEPKCASE) AS RCRCUNITS, SUM(QTY_CASE) AS RRUNITS ;
						FROM RCRCHIST ;
						INTO CURSOR curRCRCCount ;
						WHERE RCN = CURRCNS.RCN ;
						&lcRCRC_SQL_Where. ;
						AND CASEID NOT IN (SELECT CASEID FROM RDCAHIST WHERE RCN = CURRCNS.RCN &lcRDCA_SQL_Where.) ;
						AND COMPANY = CURRCNS.COMPANY


*!*						SELECT * ;
*!*							FROM RCRCHIST ;
*!*							INTO CURSOR curRCRCDETAIL ;
*!*							WHERE RCN = CURRCNS.RCN ;
*!*							&lcRCRC_SQL_Where. ;
*!*							AND CASEID NOT IN (SELECT CASEID FROM RDCAHIST WHERE RCN = CURRCNS.RCN &lcRDCA_SQL_Where.) ;
*!*							AND COMPANY = CURRCNS.COMPANY
*!*							
*!*	SELECT curRCRCDETAIL
*!*	COPY TO ('c:\a\rcn_' + TRANSFORM(CURRCNS.RCN) + '.xls') XLS
*!*	*throw
*!*	*!*	BROWSE

*!*	*!*	SET STEP ON

					SELECT curRCRCCount
					lnRCRCCount = curRCRCCount.RCRCCOUNT
					lnRCRCUNITS = curRCRCCount.RCRCUNITS
					lnRRUNITS = curRCRCCount.RRUNITS
					
					SELECT RCRCHIST
					LOCATE
					SELECT RDCAHIST
					LOCATE
					SELECT TEMPCURSOR
					LOCATE

					* update curRCNs
					*!*						REPLACE curRCNs.CARTONS WITH lnRCRCCount
*!*						REPLACE CURRCNS.CARTONS WITH lnRCRCCount, CURRCNS.UNITS WITH lnRCRCUNITS
					
					
					* added 6/26/14 MB
					IF .lRackRoomProcessing THEN
						**** rack room / off broadway specific processing.
						* Per Doug: XDock receipts will be treated as usual. 
						* But all non-XDock receipts will be rated with the new 'Solids or PrePack' per Unit rate INSTEAD of the usual per carton non-XDock rate.
						* so accumulate the non-XDock units which are in lnRRUNITS
						pnReceiptNonXDockUNITS = pnReceiptNonXDockUNITS + lnRRUNITS
						
						* update curRCNs
						REPLACE CURRCNS.CARTONS WITH lnRCRCCount, CURRCNS.UNITS WITH lnRCRCUNITS, CURRCNS.XFERCTNS WITH 0, CURRCNS.RRUNITS WITH lnRRUNITS IN CURRCNS
					
					ELSE
						* standard non-Rack Room client processing...					
						
						* accumulate receipt non xdock case total -- regardless of sku processing
						* this only applies to Sears lines, per Doug, so check the client before doing the new logic.
						IF .lEnableTransferRCNProcessing AND (LEFT(.cClient,5) == "SEARS") THEN
						
							* new logic 6/18/13

							IF .lTransferRCN THEN
								* new bucket
								pnReceiptNonXDockTRANSFERCtns = pnReceiptNonXDockTRANSFERCtns + lnRCRCCount
								* update curRCNs
								REPLACE CURRCNS.CARTONS WITH 0, CURRCNS.UNITS WITH lnRCRCUNITS, CURRCNS.XFERCTNS WITH lnRCRCCount IN CURRCNS
							ELSE
								* old bucket
								pnReceiptNonXDockCtns = pnReceiptNonXDockCtns + lnRCRCCount
								* update curRCNs
								REPLACE CURRCNS.CARTONS WITH lnRCRCCount, CURRCNS.UNITS WITH lnRCRCUNITS, CURRCNS.XFERCTNS WITH 0 IN CURRCNS
							ENDIF
							
						ELSE

							* standard logic, old bucket
							pnReceiptNonXDockCtns = pnReceiptNonXDockCtns + lnRCRCCount
							* update curRCNs
							REPLACE CURRCNS.CARTONS WITH lnRCRCCount, CURRCNS.UNITS WITH lnRCRCUNITS, CURRCNS.XFERCTNS WITH 0 IN CURRCNS
							
						ENDIF
						

						IF .lDoSkuBreakdown THEN

							DO CASE
								CASE (lnSKUCnt >= 0) AND (lnSKUCnt <= 20)
									pnSKU_0_Ctns = pnSKU_0_Ctns + lnRCRCCount
									* update curRCNs
									REPLACE CURRCNS.SKU0 WITH (lnRCRCCount * pnSKU_0_PerCtnRate)
								CASE (lnSKUCnt >= 21) AND (lnSKUCnt <= 30)
									pnSKU_21_Ctns = pnSKU_21_Ctns + lnRCRCCount
									* update curRCNs
									REPLACE CURRCNS.SKU21 WITH (lnRCRCCount * pnSKU_21_PerCtnRate)
								CASE (lnSKUCnt >= 31) AND (lnSKUCnt <= 40)
									pnSKU_31_Ctns = pnSKU_31_Ctns + lnRCRCCount
									* update curRCNs
									REPLACE CURRCNS.SKU31 WITH (lnRCRCCount * pnSKU_31_PerCtnRate)
								CASE (lnSKUCnt >= 41)
									pnSKU_41_Ctns = pnSKU_41_Ctns + lnRCRCCount
									* update curRCNs
									REPLACE CURRCNS.SKU41 WITH (lnRCRCCount * pnSKU_41_PerCtnRate)
								OTHERWISE
									* nothing
							ENDCASE

							*!*						*??????????????? do we need this ?????????????????????
							*!*						* UPDATE SKUSPERRCN in RCRCHIST
							*!*						REPLACE SKUSPERRCN WITH lnSKUCnt ;
							*!*							FOR RCN = curRCNs.RCN ;
							*!*							IN RCRCHIST
							*!*						*??????????????? do we need this ?????????????????????
							
						ENDIF && .lDoSkuBreakdown

					ENDIF  &&  .lRackRoomProcessing

					***********************************************************************************
					***********************************************************************************
					* calculate XDOCK receipt data
					***********************************************************************************
					***********************************************************************************
					** NOTE:  counts resulting from the SQL below include dot-com transactions which
					**        need to be changed from XDOCK to NON-XDOCK, if this is Sears billing.
					**        To implement this, will add an extra processing step to count the dot-com
					**        transactions. The resulting count will later be added to NON-XDOCK count, and
					**        subtracted from the XDOCK count. 

					IF USED('curRCRCCount') THEN
						USE IN curRCRCCount
					ENDIF

					**!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
					**************************************************************************************************
					**************************************************************************************************
					*** because of change from num_cases to CASEPKCASE below - to rerun any bill before 12/18/04
					*** that bill MUST FIRST BE RELOADED so that CASEPKCASE is properly populated. !!!!!!!!!!!!!!!!!!!
					**************************************************************************************************
					**************************************************************************************************
					**!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

*!*						IF .lUseRCNForRDCAKey THEN
*!*							* make RCN part of RDCAHIST key

*!*							SELECT SUM(NUM_CASES) AS RCRCCOUNT, SUM(UNITS_PCSE * CASEPKCASE) AS RCRCUNITS ;
*!*								FROM RCRCHIST ;
*!*								INTO CURSOR curRCRCCount ;
*!*								WHERE RCN = CURRCNS.RCN ;
*!*								AND CASEID IN (SELECT CASEID FROM RDCAHIST WHERE RCN = CURRCNS.RCN) ;
*!*								AND COMPANY = CURRCNS.COMPANY

*!*						ELSE

*!*							SELECT SUM(NUM_CASES) AS RCRCCOUNT, SUM(UNITS_PCSE * CASEPKCASE) AS RCRCUNITS ;
*!*								FROM RCRCHIST ;
*!*								INTO CURSOR curRCRCCount ;
*!*								WHERE RCN = CURRCNS.RCN ;
*!*								AND CASEID IN (SELECT CASEID FROM RDCAHIST) ;
*!*								AND COMPANY = CURRCNS.COMPANY

*!*						ENDIF

					* 1/29/09 MB:
					* multiply by UNITS_PCSE * CASEPKCASE * num_cases, which may be -1, to correct unit count for "back outs"
					* SELECT SUM(NUM_CASES) AS RCRCCOUNT, SUM(UNITS_PCSE * CASEPKCASE) AS RCRCUNITS ;
					
					SELECT SUM(NUM_CASES) AS RCRCCOUNT, SUM(UNITS_PCSE * CASEPKCASE * NUM_CASES) AS RCRCUNITS ;
						FROM RCRCHIST ;
						INTO CURSOR curRCRCCount ;
						WHERE RCN = CURRCNS.RCN ;
						&lcRCRC_SQL_Where. ;
						AND CASEID IN (SELECT CASEID FROM RDCAHIST WHERE RCN = CURRCNS.RCN &lcRDCA_SQL_Where.) ;
						AND COMPANY = CURRCNS.COMPANY

*!*	SELECT curRCRCCount
*!*	BROWSE

					SELECT curRCRCCount
					lnRCRCCount = curRCRCCount.RCRCCOUNT
					lnRCRCUNITS = curRCRCCount.RCRCUNITS

					************************************************************
					* REPLACES below not working without these LOCATES!  ?????
					SELECT RCRCHIST
					LOCATE
					SELECT RDCAHIST
					LOCATE
					SELECT TEMPCURSOR
					LOCATE
					************************************************************

					* update curRCNs
					REPLACE CURRCNS.XDCARTONS WITH lnRCRCCount, CURRCNS.XDUNITS WITH lnRCRCUNITS

					* acumulate receipt xdock case total
					pnReceiptCrossDockCtns = pnReceiptCrossDockCtns + lnRCRCCount

					***********************************************************************************
					***********************************************************************************					
					
					* Calculate Dot-com counts -- note addition of check for dest_loc in sub-select WHERE below.
					IF .lDotComProcessing THEN

						IF USED('curRCRCCount') THEN
							USE IN curRCRCCount
						ENDIF

						* 1/29/09 MB:
						* multiply by UNITS_PCSE * CASEPKCASE * num_cases, which may be -1, to correct unit count for "back outs"
						* SELECT SUM(NUM_CASES) AS RCRCCOUNT, SUM(UNITS_PCSE * CASEPKCASE) AS RCRCUNITS ;
						
						SELECT SUM(NUM_CASES) AS RCRCCOUNT, SUM(UNITS_PCSE * CASEPKCASE * NUM_CASES) AS RCRCUNITS ;
							FROM RCRCHIST ;
							INTO CURSOR curRCRCCount ;
							WHERE RCN = CURRCNS.RCN ;
							&lcRCRC_SQL_Where. ;
							AND CASEID IN (SELECT CASEID FROM RDCAHIST WHERE RCN = CURRCNS.RCN AND DEST_LOC == lcDotComDest_loc &lcRDCA_SQL_Where.) ;
							AND COMPANY = CURRCNS.COMPANY

						SELECT curRCRCCount
						lnRCRCCount = curRCRCCount.RCRCCOUNT
						lnRCRCUNITS = curRCRCCount.RCRCUNITS

						SELECT RCRCHIST
						LOCATE
						SELECT RDCAHIST
						LOCATE
						SELECT TEMPCURSOR
						LOCATE
						
						* update curRCNs counts to decrease XDock carton cnt and increase non-xdock carton cnt by same #
						REPLACE CURRCNS.XDCARTONS WITH (CURRCNS.XDCARTONS - lnRCRCCount), ;
							CURRCNS.CARTONS WITH (CURRCNS.CARTONS + lnRCRCCount), ;
							CURRCNS.XDUNITS WITH (CURRCNS.XDUNITS - lnRCRCUNITS), ;
							CURRCNS.UNITS WITH (CURRCNS.UNITS + lnRCRCUNITS)

						* accumulate receipt DOT-COM NON-XDOCK case total
						pnReceiptDotComXDockCtns = pnReceiptDotComXDockCtns + lnRCRCCount
					
					ENDIF  &&  .lDotComProcessing

				ENDSCAN
				
*!*					IF .lTESTMODE THEN
*!*						SELECT CURRCNS
*!*						BROWSE
*!*						COPY TO C:\A\CURRCNS.XLS XL5
*!*					ENDIF


				***********************************************************************************
				***********************************************************************************
				* RETURN RECEIPT TRANSACTIONS 
				* this total roll-up is for the billing pages

				IF USED('CURRETURNS') THEN
					USE IN CURRETURNS
				ENDIF

				SELECT SUM(NUM_CASES) AS IARNCOUNT, SUM(INVQTY) AS IARNUNITS ;
					FROM RETURNS ;
					INTO CURSOR CURRETURNS ;
					WHERE TTOD(TDATETIME) = tdTransDate ;
					AND TNODE = lcTNodeFilter ;
					&lcIARN_SQL_Where. ;
					READWRITE
					
				pnReturnReceiptCtns = CURRETURNS.IARNCOUNT

				* this more detailed roll-up is for the detail pages

				IF USED('CURRETURNSDET') THEN
					USE IN CURRETURNSDET
				ENDIF

				SELECT COMPANY, TRAILER_NO, SUM(NUM_CASES) AS RETURNCTNS ;
					FROM RETURNS ;
					INTO CURSOR CURRETURNSDET ;
					WHERE TTOD(TDATETIME) = tdTransDate ;
					AND TNODE = lcTNodeFilter ;
					&lcIARN_SQL_Where. ;
					GROUP BY COMPANY, TRAILER_NO ;
					ORDER BY COMPANY, TRAILER_NO ;
					READWRITE
					
				***********************************************************************************
				***********************************************************************************

				
				.TrackProgress("END Calculating SKU Breakdown and xdock receipt data: MAIN SCAN LOOP...", LOGIT)

				* accumulate curRCNs to support reporting on more than one transdate...
				.TrackProgress("Accumulating SKU Breakdowns for Transdate...", LOGIT+NOWAITIT)

				SELECT curAllRCNs
				APPEND FROM DBF('curRCNs')
				
				
*!*		SELECT CURRCNS
*!*		BROWSE
*!*		SELECT CURRETURNSDET
*!*		BROWSE 
				
				* ADD in return receipts info recs
				IF USED('CURRETURNSDET') AND NOT EOF('CURRETURNSDET') THEN
					SELECT CURRETURNSDET
					SCAN
						INSERT INTO curAllRCNs(TRANSDATE, COMPANY, CONTAINER, RETURNCTNS) VALUES (tdTransDate, CURRETURNSDET.COMPANY, CURRETURNSDET.TRAILER_NO, CURRETURNSDET.RETURNCTNS)					
					ENDSCAN
				ENDIF

				* ADDED 4/11/2017 MB to make the rates date sensitive for the received detail report
				SELECT curAllRCNs
				SCAN FOR (curAllRCNs.RNXDRATE + curAllRCNs.RXDOCKRATE + curAllRCNs.RRCTNRATE) = 0.00
					REPLACE curAllRCNs.RNXDRATE with pnReceiptNonXDockPerUNITRate, ;
						curAllRCNs.RXDOCKRATE with pnReceiptCrossDockPerCtnRate, ;
						curAllRCNs.RRCTNRATE with pnReturnReceiptPerCtnRate ;
						IN curAllRCNs
				ENDSCAN

				*!*					ENDIF && .lExcludeXDocks

				***********************************************************************************
				***********************************************************************************
				* SHIPPED TRANSACTIONS
				***********************************************************************************
				***********************************************************************************

				* Cross-dock:
				* To count actual shipped xdock, look at the
				* AUAD transactions for ORDER-TYPE = "DS" or "TN".
				* This would then represent actual xdock shipped.
				* Now using sort_type field instead of order_type 7/19/04 MB
				* NOTE: must group by Case-id to get actual # of cartons, per Dennis.

				.TrackProgress("Calculating Cross-Dock...", LOGIT+NOWAITIT)
				*WAIT WINDOW "Calculating Cross-Dock..." NOWAIT
				IF USED('curXDock') THEN
					USE IN curXDock
				ENDIF
				SELECT CASEID ;
					FROM TEMPCURSOR ;
					INTO CURSOR curXDock ;
					WHERE TRANS = "AUAD" AND SORT_TYPE = "10" ;
					&lcAUAD_SQL_Where. ;
					GROUP BY CASEID

				pnShippedCrossDockCtns = RECCOUNT('curXDock')
				
				
				************************************************************************************
				IF .lDotComProcessing THEN
				
					* Calculate Dot-com counts -- note addition of check for dest_loc in sub-select WHERE below.
					IF USED('curDotComXDock') THEN
						USE IN curDotComXDock
					ENDIF
					SELECT CASEID ;
						FROM TEMPCURSOR ;
						INTO CURSOR curDotComXDock ;
						WHERE TRANS = "AUAD" AND SORT_TYPE = "10" AND DEST_LOC == lcDotComDest_loc ;
						&lcAUAD_SQL_Where. ;
						GROUP BY CASEID

					pnShippedDotComXDockCtns = RECCOUNT('curDotComXDock')
					
				ENDIF  &&  .lDotComProcessing
				************************************************************************************
				

				.TrackProgress("Calculating Tilt Tray...", LOGIT+NOWAITIT)

				* PICK/PACK processing
				* # Units processed via Tilt Tray processing:
				SELECT TEMPCURSOR
				SUM PL_QTY FOR TRANS = "AUAD" AND SORT_TYPE = "71" ;
					&lcAUAD_SQL_Where. ;
					TO pnTiltUnits 


				.TrackProgress("Calculating Manual...", LOGIT+NOWAITIT)
				*WAIT WINDOW "Calculating Manual..." NOWAIT
				* # Units processed via Manual processing:
				SELECT TEMPCURSOR

				* 10/2/06 MB, to calc inner units based on prePack
				IF .lUsePrepackForManualUnitCounting THEN
					SUM (PL_QTY * PREPACK) FOR TRANS = "AUAD" AND SORT_TYPE = "41" ;
						&lcAUAD_SQL_Where. ;
						TO pnManualUnits
				ELSE
					SUM PL_QTY FOR TRANS = "AUAD" AND SORT_TYPE = "41" ;
						&lcAUAD_SQL_Where. ;
						TO pnManualUnits
				ENDIF

				.TrackProgress("Calculating Findings...", LOGIT+NOWAITIT)

				* # Units processed via Findings:
				SUM PL_QTY FOR TRANS = "AUAD" AND SORT_TYPE = "99" AND ORDER_TYPE = "FD" ;
					&lcAUAD_SQL_Where. ;
					TO pnFindingsCtns
				
*!*					* ADDED FOR 1/1/2010 mb to support Kmart/Sears residual unit processing
*!*					*IF .cClient = "KMART NON-FOOTWEAR" THEN
*!*					*IF INLIST(.cClient, "KMART ALL","KMART FOOTWEAR","KMART NON-FOOTWEAR") THEN
*!*					IF "KMART" $ .cClient OR ; 
*!*						"RACK ROOM" $ .cClient OR ;
*!*						"OFF BROADWAY" $ .cClient THEN
*!*							.TrackProgress("Calculating Residual Units...", LOGIT+NOWAITIT)
*!*							SUM RESID_QTY FOR TRANS = "RIRI" ;
*!*								&lcRIRI_SQL_Where. ;
*!*								TO pnTiltResidualUnits
*!*					ENDIF
				
				
				******  RESIDUAL PROCESSING - RR/OB WILL BILL BY CASE
				* SET UP A DO CASE HERE to replace above


				*SELECT TEMPCURSOR

				DO CASE
					CASE "KMART" $ .cClient
					
						.TrackProgress("Calculating Residual Units...", LOGIT+NOWAITIT)
						
						SELECT TEMPCURSOR
				
						SUM RESID_QTY FOR TRANS = "RIRI" ;
							&lcRIRI_SQL_Where. ;
							TO pnTiltResidualUnits
							
					CASE ("RACK ROOM" $ .cClient) OR ("OFF BROADWAY" $ .cClient)
					
						.TrackProgress("Calculating Residual Cartons...", LOGIT+NOWAITIT)
						
						* PER DOUG, WILL COUNT # OF RECORDS (= distinct COUNT OF caseids)  FOR RR/OB
						
						IF USED('curTiltResidualCartons') THEN
							USE IN curTiltResidualCartons
						ENDIF

						SELECT CASEID ;
							FROM TEMPCURSOR ;
							INTO CURSOR curTiltResidualCartons ;
							WHERE TRANS = "RIRI" ;
							&lcRIRI_SQL_Where. ;
							GROUP BY CASEID

						pnTiltResidualCartons = RECCOUNT('curTiltResidualCartons')				
						
						IF USED('curTiltResidualCartons') THEN
							USE IN curTiltResidualCartons
						ENDIF
							
					OTHERWISE
						* NOTHING
				ENDCASE

				SELECT TEMPCURSOR
				LOCATE

				***********************************************************************************
				***********************************************************************************
				* Full Case Replenishment:
				* To get actual shipped results by FullCase, join the AUAD transactions to the ITPC transactions by case-id,
				* where the ITPC record_cd (i.e. sort_type) = 51.
				*
				* 7/19/04 MB: Can use AUAD without join 
				* NOTE: must group by Case-id to get actual # of cartons, per Dennis.
				*
				* 12/15/2015: using IAAD transactions instead of AUAD for new client FORSAN
				
				.TrackProgress("Calculating Full Case...", LOGIT+NOWAITIT)				

				IF USED('curFullCase') THEN
					USE IN curFullCase
				ENDIF
				IF USED('cur41FullCase') THEN
					USE IN cur41FullCase
				ENDIF
				IF USED('curIAADXfers') THEN
					USE IN curIAADXfers
				ENDIF
				
				lnForsanIAADXferCtns = 0
						
				* special processing for Forsan IAAD xfers which are billed as AUAD full cases.
				IF ("FORSAN" $ .cClient)

					SELECT CASEID ;
						FROM IAADXFER ;
						INTO CURSOR curIAADXfers ;
						WHERE TTOD(TDATETIME) = tdTransDate ;
						AND TNODE = lcTNodeFilter ;
						&lcIAAD_SQL_Where. ;
						GROUP BY CASEID

					*WHERE TRANS = "IAAD" ;

					lnForsanIAADXferCtns = RECCOUNT('curIAADXfers')
					
				ENDIF  && ("FORSAN" $ .cClient)

				* standard processing

				SELECT CASEID ;
					FROM TEMPCURSOR ;
					INTO CURSOR curFullCase ;
					WHERE TRANS = "AUAD" ;
					AND SORT_TYPE = "51" ;
					&lcAUAD_SQL_Where. ;
					GROUP BY CASEID

				pnFullCaseCtns = RECCOUNT('curFullCase')

				IF .lTreatSortType41AsFullCase THEN
					* also treat sort type 41 as full case per Jim D. and Doug 10/8/2013 MB

					SELECT CASEID ;
						FROM TEMPCURSOR ;
						INTO CURSOR cur41FullCase ;
						WHERE TRANS = "AUAD" ;
						AND SORT_TYPE = "41" ;
						&lcAUAD_SQL_Where. ;
						GROUP BY CASEID

					pn41FullCaseCtns = RECCOUNT('cur41FullCase')

					* add the sort 41 case count to the standard full case count...
					pnFullCaseCtns = pnFullCaseCtns + pn41FullCaseCtns

				ELSE

					pn41FullCaseCtns = 0

				ENDIF
				
				* add Forsan IAAD xfers to full cases count already computed
				IF ("FORSAN" $ .cClient) AND (lnForsanIAADXferCtns > 0) THEN
					pnFullCaseCtns = pnFullCaseCtns + lnForsanIAADXferCtns
				ENDIF

				****************************************
				****************************************

				.TrackProgress("BEGIN get shipped info for shipped detail report...", LOGIT)
				* get shipped info for shipped detail report
				IF USED('curSHIPPED') THEN
					USE IN curSHIPPED
				ENDIF

				IF .lUsePrepackForManualUnitCounting THEN

					SELECT tdTransDate AS TRANSDATE, TRAILERLD, CONTAINER, COMPANY, ;
						000000000 AS TOTCASES, ;
						SUM(IIF(SORT_TYPE="10",1,0)) AS TOTXDOCK, ;
						SUM(IIF(SORT_TYPE="41",(PL_QTY * PREPACK),0)) AS TOTMANUAL, ;
						SUM(IIF(SORT_TYPE="51",1,0)) AS TOTFULL, ;
						SUM(IIF(SORT_TYPE="71",PL_QTY,0)) AS TOTTILT, ;
						SUM(IIF(SORT_TYPE="99",PL_QTY,0)) AS TOTFINDING ;
						FROM TEMPCURSOR ;
						INTO CURSOR curSHIPPED ;
						WHERE TRANS = "AUAD" ;
						&lcAUAD_SQL_Where. ;
						GROUP BY TRANSDATE, TRAILERLD, CONTAINER, COMPANY ;
						ORDER BY TRANSDATE, TRAILERLD, CONTAINER, COMPANY ;
						READWRITE

				ELSE

					* way we did it prior to 10/2/06 MB, not based on prePack
					SELECT tdTransDate AS TRANSDATE, TRAILERLD, CONTAINER, COMPANY, ;
						000000000 AS TOTCASES, ;
						SUM(IIF(SORT_TYPE="10",1,0)) AS TOTXDOCK, ;
						SUM(IIF(SORT_TYPE="41",PL_QTY,0)) AS TOTMANUAL, ;
						SUM(IIF(SORT_TYPE="51",1,0)) AS TOTFULL, ;
						SUM(IIF(SORT_TYPE="71",PL_QTY,0)) AS TOTTILT, ;
						SUM(IIF(SORT_TYPE="99",PL_QTY,0)) AS TOTFINDING ;
						FROM TEMPCURSOR ;
						INTO CURSOR curSHIPPED ;
						WHERE TRANS = "AUAD" ;
						&lcAUAD_SQL_Where. ;
						GROUP BY TRANSDATE, TRAILERLD, CONTAINER, COMPANY ;
						ORDER BY TRANSDATE, TRAILERLD, CONTAINER, COMPANY ;
						READWRITE

				ENDIF

				SELECT curSHIPPED
				SCAN
					IF USED('curCASEIDs') THEN
						USE IN curCASEIDs
					ENDIF
					
					SELECT CASEID ;
						FROM TEMPCURSOR ;
						INTO CURSOR curCASEIDs ;
						WHERE TRANS = "AUAD" ;
						&lcAUAD_SQL_Where. ;
						AND TTOD(TDATETIME) = curSHIPPED.TRANSDATE ;
						AND TRAILERLD = curSHIPPED.TRAILERLD ;
						AND CONTAINER = curSHIPPED.CONTAINER ;
						AND COMPANY = curSHIPPED.COMPANY ;
						GROUP BY CASEID
						
					lnCASEID = RECCOUNT('curCASEIDs')
					
					REPLACE curSHIPPED.TOTCASES WITH lnCASEID IN CURSHIPPED
					
					****************************************************************************************************
					****************************************************************************************************
					**** ADDED BLOCK 4/12/2017 TO TRY AND MAKE XDOCK CASES COUNT ACCURATE IN THE SHIPPED DETAIL REPORT
					
					IF USED('curCASEIDsXDOCK') THEN
						USE IN curCASEIDsXDOCK
					ENDIF
					
					SELECT CASEID ;
						FROM TEMPCURSOR ;
						INTO CURSOR curCASEIDsXDOCK ;
						WHERE TRANS = "AUAD" ;
						&lcAUAD_SQL_Where. ;
						AND TTOD(TDATETIME) = curSHIPPED.TRANSDATE ;
						AND TRAILERLD = curSHIPPED.TRAILERLD ;
						AND CONTAINER = curSHIPPED.CONTAINER ;
						AND COMPANY = curSHIPPED.COMPANY ;
						AND SORT_TYPE="10" ;
						GROUP BY CASEID
						
					lnCASEIDXDOCK = RECCOUNT('curCASEIDsXDOCK')
					
					REPLACE curSHIPPED.TOTXDOCK WITH lnCASEIDXDOCK IN CURSHIPPED
					****************************************************************************************************
					****************************************************************************************************
					
					
					
*!*						***************************************************************************************					
*!*						* need to populate field TOTRSDCTN with residual case counts - for Rack Room 
*!*						
*!*						IF USED('curCASEIDs') THEN
*!*							USE IN curCASEIDs
*!*						ENDIF
*!*						
*!*						SELECT CASEID ;
*!*							FROM TEMPCURSOR ;
*!*							INTO CURSOR curCASEIDs ;
*!*							WHERE TRANS = "RIRI" ;
*!*							&lcRIRI_SQL_Where. ;
*!*							AND TTOD(TDATETIME) = curSHIPPED.TRANSDATE ;
*!*							AND TRAILERLD = curSHIPPED.TRAILERLD ;
*!*							AND CONTAINER = curSHIPPED.CONTAINER ;
*!*							AND COMPANY = curSHIPPED.COMPANY ;
*!*							GROUP BY CASEID
*!*							
*!*						lnCASEID = RECCOUNT('curCASEIDs')
*!*						
*!*						REPLACE curSHIPPED.TOTRSDCTN WITH lnCASEID IN CURSHIPPED
					
					
					
	
					***************************************************************************************					
					IF .lTreatSortType41AsFullCase THEN
						* need to count the sort type 41 caseids for each row in the cursor, then add it the full case #'s
						
						IF USED('cur41CASEIDs') THEN
							USE IN cur41CASEIDs
						ENDIF
						
						SELECT CASEID ;
							FROM TEMPCURSOR ;
							INTO CURSOR cur41CASEIDs ;
							WHERE TRANS = "AUAD" ;
							&lcAUAD_SQL_Where. ;
							AND TTOD(TDATETIME) = curSHIPPED.TRANSDATE ;
							AND TRAILERLD = curSHIPPED.TRAILERLD ;
							AND CONTAINER = curSHIPPED.CONTAINER ;
							AND COMPANY = curSHIPPED.COMPANY ;
							AND SORT_TYPE = "41" ;
							GROUP BY CASEID
							
						lnCASEID = RECCOUNT('cur41CASEIDs')
						
						REPLACE curSHIPPED.TOTFULL WITH (curSHIPPED.TOTFULL + lnCASEID) IN CURSHIPPED
						
					ENDIF && .lTreatSortType41AsFullCase 
	
					
					***************************************************************************************					
					IF .lDotComProcessing THEN
					
						* Calculate & apply Dot-com adjustments to curSHIPPED.
						
						IF USED('curCASEIDs') THEN
							USE IN curCASEIDs
						ENDIF
						SELECT CASEID ;
							FROM TEMPCURSOR ;
							INTO CURSOR curCASEIDs ;
							WHERE TRANS = "AUAD" AND SORT_TYPE = "10" AND DEST_LOC == lcDotComDest_loc ;
							&lcAUAD_SQL_Where. ;
							AND TTOD(TDATETIME) = curSHIPPED.TRANSDATE ;
							AND TRAILERLD = curSHIPPED.TRAILERLD ;
							AND CONTAINER = curSHIPPED.CONTAINER ;
							AND COMPANY = curSHIPPED.COMPANY ;
							GROUP BY CASEID

						lnCASEID = RECCOUNT('curCASEIDs')
						REPLACE curSHIPPED.TOTXDOCK WITH (curSHIPPED.TOTXDOCK - lnCASEID), ;
							curSHIPPED.TOTFULL WITH (curSHIPPED.TOTFULL + lnCASEID) IN CURSHIPPED
						
					ENDIF  &&  .lDotComProcessing					
					***************************************************************************************					
					
				ENDSCAN
				LOCATE

				* accumulate curSHIPPEDs to support reporting on more than one transdate...
				.TrackProgress("Accumulating Shipped Detail for Transdate...", LOGIT+NOWAITIT)
				*WAIT WINDOW "Accumulating Shipped Detail for Transdate..." NOWAIT
				
				* ADD RECORD TO SHIPPED CURSOR IF pnFullCaseCtns > 0 ; curshipped gets appended to curallshipped at a later point.
				IF ("FORSAN" $ .cClient) AND (lnForsanIAADXferCtns > 0) THEN
					*INSERT INTO curAllSHIPPED(TRANSDATE, TOTFULL) VALUES (tdTransDate, pnFullCaseCtns)
					INSERT INTO curSHIPPED(TRANSDATE, TOTFULL) VALUES (tdTransDate, lnForsanIAADXferCtns)
				ENDIF				
				
				SELECT curAllSHIPPED
				APPEND FROM DBF('curSHIPPED')

				
				* ADD in Residual Case info recs IF > 0
				IF pnTiltResidualCartons > 0 THEN
					INSERT INTO curAllSHIPPED(TRANSDATE, TOTRSDCTN) VALUES (tdTransDate, pnTiltResidualCartons)
				ENDIF				

				
				* ADDED 4/11/2017 MB to make the rates date sensitive for the shipped detail report
				SELECT curAllSHIPPED
				SCAN FOR (curAllSHIPPED.SXDOCKRATE + curAllSHIPPED.TILTRATE + curAllSHIPPED.TILTRSRATE + curAllSHIPPED.FCASERATE) = 0.00
					REPLACE SXDOCKRATE WITH pnShippedCrossDockPerCtnRate, ;
						curAllSHIPPED.TILTRATE WITH pnTiltPerUnitRate, ;
						curAllSHIPPED.TILTRSRATE WITH pnTiltResidualsPerCartonRate, ;
						curAllSHIPPED.FCASERATE WITH pnFullCasePerCtnRate ;
						IN curAllSHIPPED
				ENDSCAN
				
				****************************************
				****************************************
				.TrackProgress("BEGIN calc report total charges, etc...", LOGIT+NOWAITIT)


				* calc report total charges, etc.

				************************************************************************
				************************************************************************
				* here we make count adjustments to handle dot-com SHIPPED transactions.
				
				* before
				*.TrackProgress("BEFORE DOT-COM: pnShippedCrossDockCtns = " + TRANSFORM(pnShippedCrossDockCtns), LOGIT+SENDIT+NOWAITIT)
				*.TrackProgress("BEFORE DOT-COM: pnFullCaseCtns = " + TRANSFORM(pnFullCaseCtns), LOGIT+SENDIT+NOWAITIT)
				*.TrackProgress("BEFORE DOT-COM: pnShippedDotComXDockCtns = " + TRANSFORM(pnShippedDotComXDockCtns), LOGIT+SENDIT+NOWAITIT)
				
				* subtract dot-com trans count from XDOCK Count
				pnShippedCrossDockCtns = pnShippedCrossDockCtns - pnShippedDotComXDockCtns
				
				* add dot-com trans count to Full Case count
				pnFullCaseCtns = pnFullCaseCtns + pnShippedDotComXDockCtns
				
				* after
				*.TrackProgress("AFTER DOT-COM: pnShippedCrossDockCtns = " + TRANSFORM(pnShippedCrossDockCtns), LOGIT+SENDIT+NOWAITIT)
				*.TrackProgress("AFTER DOT-COM: pnFullCaseCtns = " + TRANSFORM(pnFullCaseCtns), LOGIT+SENDIT+NOWAITIT)

				*.TrackProgress(TRANSFORM(pnShippedDotComXDockCtns) + " DOT-COM Shipped cases moved from XDOCK to FULL-CASE!", LOGIT+SENDIT+NOWAITIT)

				IF pnShippedDotComXDockCtns > 0 THEN
					.cDotComSummary = .cDotComSummary + TRANSFORM(tdTransDate) + ": " + ;
						TRANSFORM(pnShippedDotComXDockCtns) + " DOT-COM Shipped cases moved from XDOCK to FULL-CASE." + CRLF
				ENDIF
				
				pnTotalShippedCAPEX = (pnShippedCrossDockCtns * pnShippedCrossDockCAPEXPerCtnRate)
				
				pnTotalShippedNonCapex = ;
					(pnShippedCrossDockCtns * pnShippedCrossDockPerCtnRate) + ;
					(pnTiltUnits * pnTiltPerUnitRate) + ;
					(pnTiltResidualUnits * pnTiltResidualsPerUnitRate) + ;
					(pnTiltResidualCartons * pnTiltResidualsPerCartonRate) + ;
					(pnManualUnits * pnManualPerUnitRate) + ;
					(pnFindingsCtns * pnFindingsPerCtnRate) + ;
					(pnFullCaseCtns * pnFullCasePerCtnRate)
					

				IF .lDoSkuBreakdown THEN
				
					pnSkuReceiptNonXDockCtns = pnSKU_0_Ctns + pnSKU_21_Ctns + pnSKU_31_Ctns + pnSKU_41_Ctns
					
					.TrackProgress("SKU TEST: pnSkuReceiptNonXDockCtns = " + TRANSFORM(pnSkuReceiptNonXDockCtns) + ;
						"  pnReceiptNonXDockCtns = " + TRANSFORM(pnReceiptNonXDockCtns), LOGIT+SENDIT+NOWAITIT)
						
				ENDIF && .lDoSkuBreakdown
				
				
				
				************************************************************************
				************************************************************************
				* here we make count adjustments to handle dot-com RECEIPT transactions.
				
				* before
				*.TrackProgress("BEFORE DOT-COM: pnReceiptCrossDockCtns = " + TRANSFORM(pnReceiptCrossDockCtns), LOGIT+SENDIT+NOWAITIT)
				*.TrackProgress("BEFORE DOT-COM: pnSkuReceiptNonXDockCtns = " + TRANSFORM(pnSkuReceiptNonXDockCtns), LOGIT+SENDIT+NOWAITIT)
				*.TrackProgress("BEFORE DOT-COM: pnReceiptDotComXDockCtns = " + TRANSFORM(pnReceiptDotComXDockCtns), LOGIT+SENDIT+NOWAITIT)

				* subtract dot-com trans count from XDOCK count
				pnReceiptCrossDockCtns = pnReceiptCrossDockCtns - pnReceiptDotComXDockCtns

				* add dot-com trans count to NON-XDOCK count
				*pnSkuReceiptNonXDockCtns = pnSkuReceiptNonXDockCtns + pnReceiptDotComXDockCtns
				pnReceiptNonXDockCtns = pnReceiptNonXDockCtns + pnReceiptDotComXDockCtns
				
				* after
				*.TrackProgress("AFTER DOT-COM: pnReceiptCrossDockCtns = " + TRANSFORM(pnReceiptCrossDockCtns), LOGIT+SENDIT+NOWAITIT)
				*.TrackProgress("AFTER DOT-COM: pnSkuReceiptNonXDockCtns = " + TRANSFORM(pnSkuReceiptNonXDockCtns), LOGIT+SENDIT+NOWAITIT)
				*.TrackProgress("AFTER DOT-COM: pnSReceiptNonXDockCtns = " + TRANSFORM(pnReceiptNonXDockCtns), LOGIT+SENDIT+NOWAITIT)

				*.TrackProgress(TRANSFORM(pnReceiptDotComXDockCtns) + " DOT-COM Receipt cases moved from XDOCK to NON-XDOCK!", LOGIT+SENDIT+NOWAITIT)
				
				IF pnReceiptDotComXDockCtns > 0 THEN
					.cDotComSummary = .cDotComSummary + TRANSFORM(tdTransDate) + ": " + ;
						TRANSFORM(pnReceiptDotComXDockCtns) + " DOT-COM Receipt cases moved from XDOCK to NON-XDOCK." + CRLF
				ENDIF
				
*!*					pnTotalReceipt = ;
*!*						(pnReceiptCrossDockCtns * pnReceiptCrossDockPerCtnRate) + ;
*!*						(pnSKU_0_Ctns * pnSKU_0_PerCtnRate) + ;
*!*						(pnSKU_21_Ctns * pnSKU_21_PerCtnRate) + ;
*!*						(pnSKU_31_Ctns * pnSKU_31_PerCtnRate) + ;
*!*						(pnSKU_41_Ctns * pnSKU_41_PerCtnRate)

*!*					pnTotalReceipt = ;
*!*						(pnReceiptCrossDockCtns * pnReceiptCrossDockPerCtnRate) + ;
*!*						(pnSkuReceiptNonXDockCtns * pnReceiptNonXDockPerCtnRate) 
									
				pnTotalReceiptNonCapex = ;
					(pnReceiptCrossDockCtns * pnReceiptCrossDockPerCtnRate) + ;
					(pnReceiptNonXDockCtns * pnReceiptNonXDockPerCtnRate) + ;
					(pnReceiptNonXDockUNITS * pnReceiptNonXDockPerUNITRate) 

				pnTotalReceiptCAPEX = ;
					(pnReceiptCrossDockCtns * pnReceiptCrossDockCAPEXPerCtnRate) + ;
					(pnReceiptNonXDockCtns * pnReceiptNonXDockCAPEXPerCtnRate) 
					
				pnTotalReceiptRETURNS = ;
					(pnReturnReceiptCtns * pnReturnReceiptPerCtnRate)
					
				pnTotalReceipt = pnTotalReceiptNonCapex + pnTotalReceiptCAPEX + pnTotalReceiptRETURNS
				
				pnTotalShipped = pnTotalShippedNonCapex + pnTotalShippedCAPEX

				pnTotalChargesNonCapex = pnTotalShippedNonCapex + pnTotalReceiptNonCapex + pnTotalReceiptRETURNS
				
				pnTotalChargesCAPEX = pnTotalReceiptCAPEX + pnTotalShippedCAPEX
				
				*pnTotalCharges = pnTotalShipped + pnTotalReceipt
				pnTotalCharges = pnTotalChargesNonCapex + pnTotalChargesCAPEX
				
				* if generating invoice, update "official" trans detail table
				*IF .lGenerateInvoice AND (NOT .lTestInvoice) THEN
				IF .lGenerateInvoice THEN
					INSERT INTO INVHIST ( ;
						TNODE, ;
						CLIENT, ;
						TRANSDATE, ;
						CTNSIN, ;
						XDCTNSIN, ;
						RRCTNSIN, ;
						CTNSOUT, ;
						XDCTNSOUT, ;
						TILTOUT, ;
						XFERCTNS, ;
						TILTRSCTNS, ;
						LTEST, ;
						INVDATE ) ;
					VALUES ( ;
						.cTNodeFilter, ;
						.cClient, ;
						tdTransDate, ;
						pnReceiptNonXDockCtns, ;
						pnReceiptCrossDockCtns, ;
						pnReturnReceiptCtns, ;
						pnFullCaseCtns, ;
						pnShippedCrossDockCtns, ;
						pnTiltUnits, ;
						pnReceiptNonXDockTRANSFERCtns, ;
						pnTiltResidualCartons, ;
						.lTestInvoice, ;
						.dInvoiceDate )
				ENDIF

				****************************************
				****************************************
				* update daily history table -- unlike detail above, this includes $ 
				*IF .lGenerateInvoice AND (NOT .lTestInvoice) THEN
				IF .lGenerateInvoice THEN
					INSERT INTO HISTORYTABLE ( ;
						CLIENT, ;
						DATETIME, ;
						TRANSDATE, ;
						RETURNCTNS, ;
						SXDOCKCTNS, ;
						TNODE, ;
						RDOTCOM, ;
						RNXDCTNS, ;						
						RNXDUNITS, ;
						RNXDUNITRT, ;
						SDOTCOM, ;
						SXDOCKRATE, ;
						SXDCAPEX, ;
						RXDOCKCTNS, ;
						RXDOCKRATE, ;
						RXDCAPEX, ;
						RNXDRATE, ;
						RNXDCAPEX, ;
						SKU0CTNS, ;
						SKU0RATE, ;
						SKU21CTNS, ;
						SKU21RATE, ;
						SKU31CTNS, ;
						SKU31RATE, ;
						SKU41CTNS, ;
						SKU41RATE, ;
						FCASECTNS, ;
						FC41CTNS, ;
						FCASERATE, ;
						TILTUNITS, ;
						TILTRATE, ;
						TILTRSUNTS, ;
						TILTRSRATE, ;
						MANUNITS, ;
						MANRATE, ;
						FINDUNITS, ;
						FINDRATE, ;
						TOTSHPREG, ;
						TOTSHPCAPX, ;
						TOTSHIPPED, ;
						TOTRCTREG, ;
						TOTRCTCAPX, ;
						TOTRECEIPT, ;
						TOTRETURNS, ;
						TOTALCHG, ;
						XFERCTNS, ;
						TILTRSCTNS, ;
						TRSCTNRATE, ;
						LTEST, ;
						INVDATE ) ;
						VALUES ( ;
						.cClient, ;
						DATETIME(), ;
						tdTransDate, ;
						pnReturnReceiptCtns, ;
						pnShippedCrossDockCtns, ;
						.cTNodeFilter, ;
						pnReceiptDotComXDockCtns, ;
						pnReceiptNonXDockCtns, ;
						pnReceiptNonXDockUNITS, ;
						pnReceiptNonXDockPerUNITRate, ;						
						pnShippedDotComXDockCtns, ;
						pnShippedCrossDockPerCtnRate, ;
						pnShippedCrossDockCAPEXPerCtnRate, ;
						pnReceiptCrossDockCtns, ;
						pnReceiptCrossDockPerCtnRate, ;
						pnReceiptCrossDockCAPEXPerCtnRate, ;
						pnReceiptNonXDockPerCtnRate, ;
						pnReceiptNonXDockCAPEXPerCtnRate, ;
						pnSKU_0_Ctns, ;
						pnSKU_0_PerCtnRate, ;
						pnSKU_21_Ctns, ;
						pnSKU_21_PerCtnRate, ;
						pnSKU_31_Ctns, ;
						pnSKU_31_PerCtnRate, ;
						pnSKU_41_Ctns, ;
						pnSKU_41_PerCtnRate, ;
						pnFullCaseCtns, ;
						pn41FullCaseCtns, ;
						pnFullCasePerCtnRate, ;
						pnTiltUnits, ;
						pnTiltPerUnitRate, ;
						pnTiltResidualUnits, ;
						PnTiltResidualsPerUnitRate, ;
						pnManualUnits, ;
						pnManualPerUnitRate, ;
						pnFindingsCtns, ;
						pnFindingsPerCtnRate, ;
						pnTotalShippedNonCapex, ;
						pnTotalShippedCAPEX, ;
						pnTotalShipped, ;
						pnTotalReceiptNonCapex, ;
						pnTotalReceiptCAPEX, ;
						pnTotalReceipt, ;
						pnTotalReceiptRETURNS, ;
						pnTotalCharges, ;
						pnReceiptNonXDockTRANSFERCtns, ;
						pnTiltResidualCartons, ;
						pnTiltResidualsPerCartonRate, ;
						.lTestInvoice, ;
						.dInvoiceDate )
					ENDIF

				* accumulate weekly totals for Rack Room				
				.nReceiptCrossDockCtnsAll = .nReceiptCrossDockCtnsAll + pnReceiptCrossDockCtns
				.nReceiptNonXDockUNITSAll = .nReceiptNonXDockUNITSAll + pnReceiptNonXDockUNITS
				.nReceiptNonXDockTRANSFERCtnsAll = .nReceiptNonXDockTRANSFERCtnsAll + pnReceiptNonXDockTRANSFERCtns
				.nReturnReceiptCtnsAll = .nReturnReceiptCtnsAll + pnReturnReceiptCtns
				.nTotalReceiptAll = .nTotalReceiptAll + pnTotalReceipt
				.nShippedCrossDockCtnsAll = .nShippedCrossDockCtnsAll + pnShippedCrossDockCtns
				.nTiltUnitsAll = .nTiltUnitsAll + pnTiltUnits
				.nTiltResidualUnitsAll = .nTiltResidualUnitsAll + pnTiltResidualUnits
				.nTiltResidualCartonsAll = .nTiltResidualCartonsAll + pnTiltResidualCartons
				.nFullCaseCtnsAll = .nFullCaseCtnsAll + pnFullCaseCtns
				.nTotalShippedAll = .nTotalShippedAll + pnTotalShipped
				.nTotalChargesAll = .nTotalChargesAll + pnTotalCharges

					
				* UPDATE MANUAL SORTER UNITS TABLE -- WE'RE DOING THIS REGARDLESS OF INVOICE/TESTING MODE
				* SO IT CAN BE UPDATED JUST BY DOING A NON-INVOICE BILL RUN	
				
				* MANUALUNITSTABLE store manual sorter units counts for Joni -- billing
				IF NOT USED('MANUALUNITSTABLE') THEN
					USE (.cManualUnitsTable) AGAIN IN 0 ALIAS MANUALUNITSTABLE
				ENDIF
											
				SELECT MANUALUNITSTABLE
				LOCATE FOR (TRANSDATE = tdTransDate) AND (TNODE = .cTNodeFilter) AND (CLIENT = .cClient)
				IF NOT FOUND() THEN
					APPEND BLANK
				ENDIF
				REPLACE MANUALUNITSTABLE.TRANSDATE WITH tdTransDate, ;
					MANUALUNITSTABLE.TNODE WITH .cTNodeFilter, ;
					MANUALUNITSTABLE.CLIENT WITH .cClient, ;
					MANUALUNITSTABLE.MANUNITS WITH pnManualUnits, ;
					MANUALUNITSTABLE.INVDATE WITH DATE() ;
					IN MANUALUNITSTABLE

				*!*	* insert into Invoice cursor
				*!*	* These are daily totals: round to nearest cent per Dennis 12/28/07 MB
				*!*	* insert 2 line items , one for Non-Capex, one for Capex, 10/14/09 MB
				*!*	*INSERT INTO curInvoice (TRANSDATE, AMOUNT) VALUES (tdTransDate, ROUND(pnTotalCharges,2))
				*!*	llCapex = .F.
				*!*	INSERT INTO curInvoice (TRANSDATE, LCAPEX, AMOUNT) VALUES (tdTransDate, llCapex, ROUND(pnTotalChargesNonCapex,2))
				*!*	llCapex = .T.
				*!*	INSERT INTO curInvoice (TRANSDATE, LCAPEX, AMOUNT) VALUES (tdTransDate, llCapex, ROUND(pnTotalChargesCAPEX,2))
								
				IF .lSplitPickNPackInvoiceDetail THEN
					* new as of July 2017 processing - we need separate invoice records for handling vs pickNpack (tilt tray) per Juan.
					*
					* if there was tilt tray $ for the day, subtract it from the total charges for Handling record insert into curinvoice,
					* then insert tilt tray $ as charge for additional PickNPack record insert into curinvoice.
					* We'll need 1 Main and then 2 split records to make this work without changing the way the invoice currently looks, per Juan.
					*
					* otherwise, just insert the total charge as as Handling in one insert
					*
					llCapex = .F.
					lnTiltDollars = (pnTiltUnits * pnTiltPerUnitRate)
					
					
					
					IF (lnTiltDollars > 0.00) THEN
						IF (pnTotalChargesNonCapex > lnTiltDollars) THEN
							* there were tilt and non-tilt charges this day - WE NEED TO SPLIT the charges which means 3 records inserted all together							
						
							***************************************************************************************************************************************************
							* MB 12/26/2017: revised to resolve issue where the split lines don't exactly total to the Main line (sometimes off by 1 cent).
							* to do this, have changed the calcs so there are only 2 roundings of the 3 numbers involved, and third number calculated by subtraction of the 2 rounded #s.
							* This should cause proper totals of the split lines.
						
							*!*	* first need to enter a Main record with total $ which will be the one that gets displayed on the invoice
							*!*	llMain = .T.
							*!*	llSplit = .F.
							*!*	INSERT INTO curInvoice (TRANSDATE, LCAPEX, TRANSTYPE, LMAIN, LSPLIT, AMOUNT) VALUES (tdTransDate, llCapex, "MAIN", llMain, llSplit, ROUND((pnTotalChargesNonCapex),2))
							*!*	* now enter the split rec for tilt
							*!*	llMain = .F.
							*!*	llSplit = .T.
							*!*	INSERT INTO curInvoice (TRANSDATE, LCAPEX, TRANSTYPE, LMAIN, LSPLIT, AMOUNT) VALUES (tdTransDate, llCapex, "TILTTRAY", llMain, llSplit, ROUND(lnTiltDollars,2))
							*!*	* now enter the split rec for handling
							*!*	llMain = .F.
							*!*	llSplit = .T.
							*!*	INSERT INTO curInvoice (TRANSDATE, LCAPEX, TRANSTYPE, LMAIN, LSPLIT, AMOUNT) VALUES (tdTransDate, llCapex, "HANDLING", llMain, llSplit, ROUND((pnTotalChargesNonCapex - lnTiltDollars),2))

							lnMainTotal = ROUND((pnTotalChargesNonCapex),2)
							lnRoundedTilt = ROUND(lnTiltDollars,2)
							lnRoundedTotalMinusTilt = lnMainTotal - lnRoundedTilt
							
							* first need to enter a Main record with total $ which will be the one that gets displayed on the invoice
							llMain = .T.
							llSplit = .F.
							INSERT INTO curInvoice (TRANSDATE, LCAPEX, TRANSTYPE, LMAIN, LSPLIT, AMOUNT) VALUES (tdTransDate, llCapex, "MAIN", llMain, llSplit, lnMainTotal)
							* now enter the split rec for tilt
							llMain = .F.
							llSplit = .T.
							INSERT INTO curInvoice (TRANSDATE, LCAPEX, TRANSTYPE, LMAIN, LSPLIT, AMOUNT) VALUES (tdTransDate, llCapex, "TILTTRAY", llMain, llSplit, lnRoundedTilt)
							* now enter the split rec for handling
							llMain = .F.
							llSplit = .T.
							INSERT INTO curInvoice (TRANSDATE, LCAPEX, TRANSTYPE, LMAIN, LSPLIT, AMOUNT) VALUES (tdTransDate, llCapex, "HANDLING", llMain, llSplit, lnRoundedTotalMinusTilt)
							***************************************************************************************************************************************************
							
						ELSE
							* there were only tilt tray charges this day - WE DON'T NEED TO SPLIT
							* MB: added 3 lines below 7/26/2017 - to fix case where there was only tilt tray $ for day and no INVDET line was being produced.
							llMain = .F.
							llSplit = .F.
							INSERT INTO curInvoice (TRANSDATE, LCAPEX, TRANSTYPE, LMAIN, LSPLIT, AMOUNT) VALUES (tdTransDate, llCapex, "TILTTRAY", llMain, llSplit, ROUND(lnTiltDollars,2))
						ENDIF  &&  (pnTotalChargesNonCapex > lnTiltDollars)
					ELSE
						* WE DON'T NEED TO SPLIT; no tilt tray $ - total charges just go in as handling
						llMain = .F.
						llSplit = .F.
						INSERT INTO curInvoice (TRANSDATE, LCAPEX, TRANSTYPE, LMAIN, LSPLIT, AMOUNT) VALUES (tdTransDate, llCapex, "HANDLING", llMain, llSplit, ROUND(pnTotalChargesNonCapex,2))
					ENDIF  &&  (lnTiltDollars > 0.00)			
				ELSE
					* standard pre July 2017 processing
					llCapex = .F.
					llMain = .F.
					INSERT INTO curInvoice (TRANSDATE, LCAPEX, TRANSTYPE, LMAIN, LSPLIT, AMOUNT) VALUES (tdTransDate, llCapex, "HANDLING", llMain, llSplit, ROUND(pnTotalChargesNonCapex,2))
					IF (pnTotalChargesCAPEX > 0) THEN
						llCapex = .T.
						llMain = .F.
						llSplit = .F.
						INSERT INTO curInvoice (TRANSDATE, LCAPEX, TRANSTYPE, LMAIN, LSPLIT, AMOUNT) VALUES (tdTransDate, llCapex, "HANDLING", llMain, llSplit, ROUND(pnTotalChargesCAPEX,2))
					ENDIF
				ENDIF  &&  .lSplitPickNPackInvoiceDetail 

				IF .lSummaryOnly THEN
				
					DO CASE
						CASE INLIST(.cClient,'OFF BROADWAY ALL','RACK ROOM ALL')
						
							* add row to the Excel summary worksheet
							.nExcelSummaryRow = .nExcelSummaryRow + 1
							lcExcelSummaryRow = LTRIM(STR(.nExcelSummaryRow))
							
							* (note: the values below are exactly the same as appear on the bill report)
							
							.oWorksheet.RANGE("A" + lcExcelSummaryRow).VALUE = tdTransDate
							.oWorksheet.RANGE("B" + lcExcelSummaryRow).VALUE = pnReceiptCrossDockCtns
							.oWorksheet.RANGE("C" + lcExcelSummaryRow).VALUE = pnReceiptCrossDockPerCtnRate					
							.oWorksheet.RANGE("D" + lcExcelSummaryRow).VALUE = ROUND(pnReceiptCrossDockCtns * pnReceiptCrossDockPerCtnRate,2)
							
							.oWorksheet.RANGE("E" + lcExcelSummaryRow).VALUE = pnReceiptNonXDockUNITS
							.oWorksheet.RANGE("F" + lcExcelSummaryRow).VALUE = pnReceiptNonXDockPerUNITRate
							.oWorksheet.RANGE("G" + lcExcelSummaryRow).VALUE = ROUND(pnReceiptNonXDockUNITS * pnReceiptNonXDockPerUNITRate,2)
							
							.oWorksheet.RANGE("H" + lcExcelSummaryRow).VALUE = pnReturnReceiptCtns
							.oWorksheet.RANGE("I" + lcExcelSummaryRow).VALUE = pnReturnReceiptPerCtnRate
							.oWorksheet.RANGE("J" + lcExcelSummaryRow).VALUE = ROUND(pnReturnReceiptCtns * pnReturnReceiptPerCtnRate,2)
							
							.oWorksheet.RANGE("K" + lcExcelSummaryRow).VALUE = pnTiltUnits
							.oWorksheet.RANGE("L" + lcExcelSummaryRow).VALUE = pnTiltPerUnitRate
							.oWorksheet.RANGE("M" + lcExcelSummaryRow).VALUE = ROUND(pnTiltUnits * pnTiltPerUnitRate,2)
							
							.oWorksheet.RANGE("N" + lcExcelSummaryRow).VALUE = pnTiltResidualCartons
							.oWorksheet.RANGE("O" + lcExcelSummaryRow).VALUE = pnTiltResidualsPerCartonRate
							.oWorksheet.RANGE("P" + lcExcelSummaryRow).VALUE = ROUND(pnTiltResidualCartons * pnTiltResidualsPerCartonRate,2)

							.oWorksheet.RANGE("Q" + lcExcelSummaryRow).VALUE = pnFullCaseCtns
							.oWorksheet.RANGE("R" + lcExcelSummaryRow).VALUE = pnFullCasePerCtnRate
							.oWorksheet.RANGE("S" + lcExcelSummaryRow).VALUE = ROUND(pnFullCaseCtns * pnFullCasePerCtnRate,2)
							
							.oWorksheet.RANGE("T" + lcExcelSummaryRow).VALUE = ROUND(pnTotalCharges,2)
							
							* highlight alternate rows
							.oWorksheet.RANGE("A" + lcExcelSummaryRow,"T" + lcExcelSummaryRow).Interior.ColorIndex = IIF(((.nExcelSummaryRow % 2) = 1),15,0)
						
						OTHERWISE
							* SEARS/KMART/DSG
				
							* header
							.oWorksheet.RANGE("A1").VALUE = "Month-To-Date Handling Receivables (estimated) for " + ;
								.cClient + ", account # " + TRANSFORM(.nAccountID)
						
							* add row to the Excel summary worksheet
							.nExcelSummaryRow = .nExcelSummaryRow + 1
							lcExcelSummaryRow = LTRIM(STR(.nExcelSummaryRow))
							
							* (note: the values below are exactly the same as appear on the bill report)
							
							.oWorksheet.RANGE("A" + lcExcelSummaryRow).VALUE = tdTransDate
							.oWorksheet.RANGE("B" + lcExcelSummaryRow).VALUE = pnReceiptCrossDockCtns
							.oWorksheet.RANGE("C" + lcExcelSummaryRow).VALUE = pnReceiptCrossDockPerCtnRate					
							.oWorksheet.RANGE("D" + lcExcelSummaryRow).VALUE = ROUND(pnReceiptCrossDockCtns * pnReceiptCrossDockPerCtnRate,2)
							
							*.oWorksheet.RANGE("E" + lcExcelSummaryRow).VALUE = pnSkuReceiptNonXDockCtns
							.oWorksheet.RANGE("E" + lcExcelSummaryRow).VALUE = pnReceiptNonXDockCtns
							.oWorksheet.RANGE("F" + lcExcelSummaryRow).VALUE = pnReceiptNonXDockPerCtnRate
							*.oWorksheet.RANGE("G" + lcExcelSummaryRow).VALUE = ROUND(pnSkuReceiptNonXDockCtns * pnReceiptNonXDockPerCtnRate,2)
							.oWorksheet.RANGE("G" + lcExcelSummaryRow).VALUE = ROUND(pnReceiptNonXDockCtns * pnReceiptNonXDockPerCtnRate,2)
							
							.oWorksheet.RANGE("H" + lcExcelSummaryRow).VALUE = pnShippedCrossDockCtns
							.oWorksheet.RANGE("I" + lcExcelSummaryRow).VALUE = pnShippedCrossDockPerCtnRate
							.oWorksheet.RANGE("J" + lcExcelSummaryRow).VALUE = ROUND(pnShippedCrossDockCtns * pnShippedCrossDockPerCtnRate,2)
							
							.oWorksheet.RANGE("K" + lcExcelSummaryRow).VALUE = pnTiltUnits
							.oWorksheet.RANGE("L" + lcExcelSummaryRow).VALUE = pnTiltPerUnitRate
							.oWorksheet.RANGE("M" + lcExcelSummaryRow).VALUE = ROUND(pnTiltUnits * pnTiltPerUnitRate,2)
							
							.oWorksheet.RANGE("N" + lcExcelSummaryRow).VALUE = pnFullCaseCtns
							.oWorksheet.RANGE("O" + lcExcelSummaryRow).VALUE = pnFullCasePerCtnRate
							.oWorksheet.RANGE("P" + lcExcelSummaryRow).VALUE = ROUND(pnFullCaseCtns * pnFullCasePerCtnRate,2)

							.oWorksheet.RANGE("Q" + lcExcelSummaryRow).VALUE = pnReturnReceiptCtns
							.oWorksheet.RANGE("R" + lcExcelSummaryRow).VALUE = pnReturnReceiptPerCtnRate
							.oWorksheet.RANGE("S" + lcExcelSummaryRow).VALUE = ROUND(pnReturnReceiptCtns * pnReturnReceiptPerCtnRate,2)

							.oWorksheet.RANGE("T" + lcExcelSummaryRow).VALUE = pnReceiptNonXDockUNITS
							.oWorksheet.RANGE("U" + lcExcelSummaryRow).VALUE = pnReceiptNonXDockPerUNITRate
							.oWorksheet.RANGE("V" + lcExcelSummaryRow).VALUE = ROUND(pnReceiptNonXDockUNITS * pnReceiptNonXDockPerUNITRate,2)
							
							.oWorksheet.RANGE("W" + lcExcelSummaryRow).VALUE = ROUND(pnTotalCharges,2)
							
							* highlight alternate rows
							.oWorksheet.RANGE("A" + lcExcelSummaryRow,"W" + lcExcelSummaryRow).Interior.ColorIndex = IIF(((.nExcelSummaryRow % 2) = 1),15,0)
						
					
					ENDCASE
					
				
				ELSE
					* standard processing
					.TrackProgress("Creating PDF Reports...", LOGIT+NOWAITIT)
					*WAIT WINDOW "Creating PDF Reports..." NOWAIT
					* make dummy cursor to drive report, which is all variables
					IF USED('DUMMY') THEN
						USE IN DUMMY
					ENDIF
					CREATE CURSOR DUMMY (DUMMY C(1))
					SELECT DUMMY
					APPEND BLANK
					
	*SET STEP ON 

					lcRootPDFFilename = (.cPDFFilename) + "_" + STRTRAN(DTOC(tdTransDate),"/")
					
					.CreatePDFReport(lcRootPDFFilename, .cPDFReportFRX)
					
					* create a delay here because we were getting errors saying that it could not find the file just created, in .MergePDFFiles()
					WAIT WINDOW TIMEOUT .nTimeOutSeconds
					
					IF NOT .lAutoMode THEN
						.MergePDFFiles(lcRootPDFFilename + ".PDF")
					ENDIF

					IF .lAttachSeparateReports THEN
						* add to attachments
						.AddAttachment(lcRootPDFFilename + ".PDF")
					ENDIF

					* if there was no data, note it for later use in the comprehensive detail reports
					SELECT TEMPCURSOR
					LOCATE
					IF EOF() THEN
						.cNoDataForTheseDates = .cNoDataForTheseDates + SPACE(20) + DTOC(tdTransDate) + CRLF
					ENDIF
					LOCATE
					
					
					
					* must combine test of these 2 cursors because they are combined in CURALLRCNS cursor
					SELECT CURRCNS
					LOCATE
					
					SELECT CURRETURNSDET
					LOCATE
					
					IF EOF('CURRCNS') AND EOF('CURRETURNSDET') THEN
						.cNoReceiptDataForTheseDates = .cNoReceiptDataForTheseDates + SPACE(20) + DTOC(tdTransDate) + CRLF
					ENDIF
					LOCATE
					
					SELECT CURSHIPPED
					LOCATE
					*IF EOF() THEN
					* ADD CHECK FOR RESIDUAL CARTON COUNT, WHICH GETS ADDED TO CURSHIPPEDALL AT A LATER POINT
					IF EOF() AND (pnTiltResidualCartons = 0) THEN
						.cNoShippedDataForTheseDates = .cNoShippedDataForTheseDates + SPACE(20) + DTOC(tdTransDate) + CRLF
					ENDIF
					LOCATE

				
				ENDIF && .lSummaryOnly THEN

				.TrackProgress(.cClient + ' MakeBill() ended normally.', LOGIT)

				*!*	CATCH TO loError
				*!*		SET CONSOLE OFF
				*!*		.lTrappedError = .T.
				*!*		
				*!*		* let's make any error fatal, for now
				*!*		.lFatalError = .T.
				*!*		
				*!*		.TrackProgress('======================================================================',LOGIT+SENDIT)
				*!*		.TrackProgress('There was an error.',LOGIT+SENDIT)
				*!*		.TrackProgress('The Error # is: ' + TRANSFORM(loError.ERRORNO), LOGIT+SENDIT)
				*!*		.TrackProgress('The Error Message is: ' + TRANSFORM(loError.MESSAGE), LOGIT+SENDIT)
				*!*		.TrackProgress('The Error occurred in Program: ' + TRANSFORM(loError.PROCEDURE), LOGIT+SENDIT)
				*!*		.TrackProgress('The Error occurred at line #: ' + TRANSFORM(loError.LINENO), LOGIT+SENDIT)
				*!*		.TrackProgress('======================================================================',LOGIT+SENDIT)
				*!*		
				*!*		.cSubject = " !!!ERRORS!!! " + .cSubject
				*!*		.cAttach = ""
				*!*		
				*!*		IF .lFatalError THEN
				*!*			* send mark an email with the error
				*!*			DO FORM M:\DEV\FRM\dartmail2 WITH .cSendTo,.cFrom,.cSubject,.cCC,.cAttach,.cBodyText,"A"
				*!*		ENDIF

				*!*	FINALLY

				*!*	ENDTRY
				
				*!*	IF .lFatalError THEN
				*!*		RETURN TO MASTER
				*!*	ENDIF

			WAIT CLEAR

		ENDWITH

		* close data - NOTE: we are not closing curALLRCNs or curALLSHIPPED!
		IF USED('IAADXFER') THEN
			USE IN IAADXFER
		ENDIF
		IF USED('RETURNS') THEN
			USE IN RETURNS
		ENDIF
		IF USED('TEMPCURSOR') THEN
			USE IN TEMPCURSOR
		ENDIF
		IF USED('TRANSTABLE') THEN
			USE IN TRANSTABLE
		ENDIF
		IF USED('HISTORYTABLE') THEN
			USE IN HISTORYTABLE
		ENDIF
		IF USED('RCRCHIST') THEN
			USE IN RCRCHIST
		ENDIF
		IF USED('RDCAHIST') THEN
			USE IN RDCAHIST
		ENDIF
		IF USED('INVHIST') THEN
			USE IN INVHIST
		ENDIF
		IF USED('DUMMY') THEN
			USE IN DUMMY
		ENDIF
		IF USED('MANUALUNITSTABLE') THEN
			USE IN MANUALUNITSTABLE
		ENDIF
		IF USED('CURTRANSFERRCNS') THEN
			USE IN CURTRANSFERRCNS
		ENDIF
		IF USED('CURRETURNS') THEN
			USE IN CURRETURNS
		ENDIF
		RETURN llRetVal
	ENDFUNC  &&  MakeBill


	FUNCTION MakeBillForWeek
		* call this with from/to dates to do everything necessary
		LPARAMETERS tdFrom, tdTo, tlGenerateInvoice, loError
		WITH THIS
			TRY
				LOCAL ldReportDate, llRetVal, lcErrorText, lcFiletoSaveAs, lcXLFileName, lcExcelSummaryRow
				LOCAL lcEndRow, lcStartRow
				
				.TrackProgress("server = " + .cCOMPUTERNAME, LOGIT+SENDIT)				
				
				* zero out weekly accumulators...
				STORE 0 TO nReceiptCrossDockCtnsAll, nReceiptNonXDockUNITSAll,	nReceiptNonXDockTRANSFERCtnsAll, nReturnReceiptCtnsAll,	nTotalReceiptAll
				STORE 0 TO nShippedCrossDockCtnsAll, nTiltUnitsAll, nTiltResidualUnitsAll, nFullCaseCtnsAll, nTotalShippedAll, nTotalChargesAll, nTiltResidualCartonsAll	

				
				IF .lUseTestDataForBilling THEN
					* use copy of data on c: drive
					.UseLocalData()
					.TrackProgress("USING TEST BILLING DATA", LOGIT+SENDIT)
					.lGenerateInvoice = .F.					
					.TrackProgress("TURNING INVOICING OFF", LOGIT+SENDIT)
				ENDIF
				
				IF .lTestMode OR .lUseTestDataForBilling THEN
					.ShowDataFiles()
				ENDIF
				
				IF EMPTY(.cClient) OR EMPTY(.cTNodeFilter) THEN
					.TrackProgress("**** ERROR: the client has not been set!", LOGIT+SENDIT)
					RETURN
				ENDIF
				
				IF .lSummaryOnly THEN
					* do not open acrobat, we will not need it for reports.
					
					* open Excel and initialize output spreadsheet
					************************************************************
					************************************************************
					WAIT WINDOW NOWAIT "Opening Excel..."
					.oExcel = CREATEOBJECT("excel.application")
					.oExcel.VISIBLE = .F.
					.oWorkbook = .oExcel.workbooks.OPEN(.cHandlingSummaryTemplate)
					
					lcFiletoSaveAs = .cSummarySaveAs + ;
						" " + PADL(MONTH(tdFrom),2,"0") + "-" + PADL(DAY(tdFrom),2,"0") + "-" + PADL(YEAR(tdFrom),4,"0") + ;
						" thru " + ;
						" " + PADL(MONTH(tdTo),2,"0") + "-" + PADL(DAY(tdTo),2,"0") + "-" + PADL(YEAR(tdTo),4,"0")
						
					lcXLFileName = lcFiletoSaveAs + ".XLS"

					IF FILE(lcXLFileName) THEN
						DELETE FILE (lcXLFileName)
					ENDIF
					.oWorkbook.SAVEAS(lcFiletoSaveAs)
					.oWorksheet = .oWorkbook.Worksheets[1]
					
					.nExcelSummaryRow = 4
					lcStartRow = "5"
					************************************************************
					************************************************************
				
				
					* make sure consolidated pdf file is deleted from previous runs on same date
					IF FILE(.cBigPDFFile) THEN
						DELETE FILE (.cBigPDFFile)
					ENDIF

					* save params to properties
					.dBeginningTransdate = tdFrom
					.dEndingTransdate = tdTo
					.lGenerateInvoice = tlGenerateInvoice

					llRetVal = .T.
					ldReportDate = tdFrom
					DO WHILE ldReportDate <= tdTo
						* MakeBill() gets run once per date
						.MakeBill(ldReportDate)
						ldReportDate = ldReportDate + 1
					ENDDO
										
					************************************************************
					************************************************************
					* add totals line to spreadsheet
					lcEndRow = LTRIM(STR(.nExcelSummaryRow))
					.nExcelSummaryRow = .nExcelSummaryRow + 2
					lcExcelSummaryRow = LTRIM(STR(.nExcelSummaryRow))
					*.oWorksheet.RANGE("A" + lcExcelSummaryRow,"Q" + lcExcelSummaryRow).FONT.SIZE = 9
					
					DO CASE
						CASE INLIST(.cClient,'OFF BROADWAY ALL','RACK ROOM ALL')

						.oWorksheet.RANGE("A" + lcExcelSummaryRow).VALUE = "Totals:"
						.oWorksheet.RANGE("B" + lcExcelSummaryRow).formula = "=SUM(B" + lcStartRow + ":B" + lcEndRow + ")"
						.oWorksheet.RANGE("D" + lcExcelSummaryRow).formula = "=SUM(D" + lcStartRow + ":D" + lcEndRow + ")"
						.oWorksheet.RANGE("E" + lcExcelSummaryRow).formula = "=SUM(E" + lcStartRow + ":E" + lcEndRow + ")"
						.oWorksheet.RANGE("G" + lcExcelSummaryRow).formula = "=SUM(G" + lcStartRow + ":G" + lcEndRow + ")"
						.oWorksheet.RANGE("H" + lcExcelSummaryRow).formula = "=SUM(H" + lcStartRow + ":H" + lcEndRow + ")"
						.oWorksheet.RANGE("J" + lcExcelSummaryRow).formula = "=SUM(J" + lcStartRow + ":J" + lcEndRow + ")"
						.oWorksheet.RANGE("K" + lcExcelSummaryRow).formula = "=SUM(K" + lcStartRow + ":K" + lcEndRow + ")"
						.oWorksheet.RANGE("M" + lcExcelSummaryRow).formula = "=SUM(M" + lcStartRow + ":M" + lcEndRow + ")"
						.oWorksheet.RANGE("N" + lcExcelSummaryRow).formula = "=SUM(N" + lcStartRow + ":N" + lcEndRow + ")"
						.oWorksheet.RANGE("P" + lcExcelSummaryRow).formula = "=SUM(P" + lcStartRow + ":P" + lcEndRow + ")"
						.oWorksheet.RANGE("Q" + lcExcelSummaryRow).formula = "=SUM(Q" + lcStartRow + ":Q" + lcEndRow + ")"					
						.oWorksheet.RANGE("S" + lcExcelSummaryRow).formula = "=SUM(S" + lcStartRow + ":S" + lcEndRow + ")"					
						.oWorksheet.RANGE("T" + lcExcelSummaryRow).formula = "=SUM(T" + lcStartRow + ":T" + lcEndRow + ")"	
									
					OTHERWISE

						.oWorksheet.RANGE("A" + lcExcelSummaryRow).VALUE = "Totals:"
						.oWorksheet.RANGE("B" + lcExcelSummaryRow).formula = "=SUM(B" + lcStartRow + ":B" + lcEndRow + ")"
						.oWorksheet.RANGE("D" + lcExcelSummaryRow).formula = "=SUM(D" + lcStartRow + ":D" + lcEndRow + ")"
						.oWorksheet.RANGE("E" + lcExcelSummaryRow).formula = "=SUM(E" + lcStartRow + ":E" + lcEndRow + ")"
						.oWorksheet.RANGE("G" + lcExcelSummaryRow).formula = "=SUM(G" + lcStartRow + ":G" + lcEndRow + ")"
						.oWorksheet.RANGE("H" + lcExcelSummaryRow).formula = "=SUM(H" + lcStartRow + ":H" + lcEndRow + ")"
						.oWorksheet.RANGE("J" + lcExcelSummaryRow).formula = "=SUM(J" + lcStartRow + ":J" + lcEndRow + ")"
						.oWorksheet.RANGE("K" + lcExcelSummaryRow).formula = "=SUM(K" + lcStartRow + ":K" + lcEndRow + ")"
						.oWorksheet.RANGE("M" + lcExcelSummaryRow).formula = "=SUM(M" + lcStartRow + ":M" + lcEndRow + ")"
						.oWorksheet.RANGE("N" + lcExcelSummaryRow).formula = "=SUM(N" + lcStartRow + ":N" + lcEndRow + ")"
						.oWorksheet.RANGE("P" + lcExcelSummaryRow).formula = "=SUM(P" + lcStartRow + ":P" + lcEndRow + ")"
						.oWorksheet.RANGE("Q" + lcExcelSummaryRow).formula = "=SUM(Q" + lcStartRow + ":Q" + lcEndRow + ")"					
						.oWorksheet.RANGE("S" + lcExcelSummaryRow).formula = "=SUM(S" + lcStartRow + ":S" + lcEndRow + ")"					
						.oWorksheet.RANGE("T" + lcExcelSummaryRow).formula = "=SUM(T" + lcStartRow + ":T" + lcEndRow + ")"	
						.oWorksheet.RANGE("V" + lcExcelSummaryRow).formula = "=SUM(V" + lcStartRow + ":V" + lcEndRow + ")"	
						.oWorksheet.RANGE("W" + lcExcelSummaryRow).formula = "=SUM(W" + lcStartRow + ":W" + lcEndRow + ")"	
									
					ENDCASE
					
					* close output spreadsheet and then Excel
					.oWorkbook.SAVE()
					.oWorkbook.CLOSE()
					.oExcel.QUIT()
					
					.AddAttachment(lcXLFileName)
					************************************************************
					************************************************************
				
				ELSE
					* standard processing;
					* open acrobat, we will need it for reports				
					IF .OpenPDFEngine() THEN

						* make sure consolidated pdf file is deleted from previous runs on same date
						IF FILE(.cBigPDFFile) THEN
							DELETE FILE (.cBigPDFFile)
						ENDIF

						* save params to properties
						.dBeginningTransdate = tdFrom
						.dEndingTransdate = tdTo
						.lGenerateInvoice = tlGenerateInvoice

						llRetVal = .T.
						ldReportDate = tdFrom
						DO WHILE ldReportDate <= tdTo
							* MakeBill() gets run once per date
							.MakeBill(ldReportDate)
							ldReportDate = ldReportDate + 1
						ENDDO

						* the following routines are only run once after all dates have been processed
						
						
						*!*	* if rack room, run weekly bill summary...
						*!*	* disabled 4/11/2017 MB - see comments in the procedure itself.
						*!*	IF .lRackRoomProcessing THEN
						*!*		.CreateWeeklyBillSummary()
						*!*	ENDIF
						
						IF .lAppendTransactionDetailToPDF THEN					
							.CreateReceiptDetailPDF()
							.CreateShippedDetailPDF()					
						ENDIF && .lAppendTransactionDetailToPDF

*!*							IF .lAttachExcelSummaryFiles THEN					
*!*								.RollupSKUBreakdownDetailForXL()
*!*								.RollupShippedDetailForXL()					
*!*							ENDIF && .lAttachExcelSummaryFiles

						IF .lGenerateInvoice THEN
							.GenerateInvoice()
						ENDIF

*!*						ELSE
*!*							* error: we cannot proceed without 2 acrobat object refs!
*!*							lcErrorText = ""
*!*							IF NOT TYPE('.oAcrobat') = 'O' THEN
*!*								lcErrorText = lcErrorText + "oAcrobat is not an object!" + CRLF
*!*							ENDIF
*!*							IF ISNULL(.oAcrobat) THEN
*!*								lcErrorText = lcErrorText + "oAcrobat is NULL!" + CRLF
*!*							ENDIF
*!*							IF NOT TYPE('.oAcrobat2') = 'O' THEN
*!*								lcErrorText = lcErrorText + "oAcrobat2 is not an object!" + CRLF
*!*							ENDIF
*!*							IF ISNULL(.oAcrobat2) THEN
*!*								lcErrorText = lcErrorText + "oAcrobat2 is NULL!" + CRLF
*!*							ENDIF
*!*							.TrackProgress(lcErrorText, LOGIT+SENDIT)
						*=MESSAGEBOX(lcErrorText,0+16,"Acrobat error!")
					ENDIF  &&  .OpenPDFEngine()
					
				ENDIF && .lSummaryOnly THEN

			CATCH TO loError

				SET CONSOLE OFF
				.lTrappedError = .T.
				.lFatalError = .T.
				
				.TrackProgress('======================================================================',LOGIT+SENDIT)
				.TrackProgress('There was an error.',LOGIT+SENDIT)
				.TrackProgress('The Error # is: ' + TRANSFORM(loError.ERRORNO), LOGIT+SENDIT)
				.TrackProgress('The Error Message is: ' + TRANSFORM(loError.MESSAGE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred in Program: ' + TRANSFORM(loError.PROCEDURE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred at line #: ' + TRANSFORM(loError.LINENO), LOGIT+SENDIT)
				.TrackProgress('======================================================================',LOGIT+SENDIT)
				
				.cSubject = " !!!ERRORS!!! " + .cSubject
				.cAttach = ""
				
				IF .lFatalError THEN
					* send mark an email with the error
					DO FORM M:\DEV\FRM\dartmail2 WITH .cSendTo,.cFrom,.cSubject,.cCC,.cAttach,.cBodyText,"A"
				ENDIF

			FINALLY

			ENDTRY

			IF NOT .lFatalError THEN
				.SendEmail()
			ENDIF


		ENDWITH
		RETURN llRetVal
	ENDFUNC  &&  MakeBillForWeek


	FUNCTION GenerateInvoice
	
		WITH THIS
			IF .lTestInvoice THEN
				.TrackProgress('Generating FoxPro Invoice in DEV (NOT Production)...',LOGIT+WAITIT+SENDIT)
			ELSE
				.TrackProgress('Generating FoxPro Invoice in Production...',LOGIT+WAITIT+SENDIT)
			ENDIF
			PUBLIC gnocalc, gmakecheckspayableto, GSYSTEMMODULE 
			PRIVATE pnInvoiceID
			gnocalc = .T.  && this prevents an AR stored procedure from calculating invoice.invamt
			gmakecheckspayableto = "TGF Management Group Holdco Inc." 
			
			LOCAL llRetVal, lnAcctID, lnLineNum, lnTotalAmount
			llRetVal = .T.
			lnAcctID = .nAccountID

			* if we have defined a separate billtoID for a client, use that instead of the main accountid
			* needed to support Off Broadway, which should use the billtoid for Rack Room.
			IF .nBillToID = 0 THEN
				m.billtoid = lnAcctID
			ELSE
				m.billtoid = .nBillToID
			ENDIF

			SELECT curInvoice
			*SUM AMOUNT TO lnTotalAmount
			SUM AMOUNT TO lnTotalAmount FOR (NOT LSPLIT)  && to not count split $ because those are already part of the LMAIN curinvoice record $. 06/23/2017 MB

*!*				IF .lTestInvoice THEN
*!*					USE M:\DEV\ARDATA\invoice ALIAS vinvoice IN 0
*!*					USE M:\DEV\ARDATA\invdet ALIAS vinvdet IN 0				
*!*					*USE M:\DEV\ARDATA\ARGENPK AGAIN IN 200
*!*					USE M:\DEV\ARDATA\farhdr in 0
*!*					USE M:\DEV\ARDATA\farlin in 0
*!*					*USE M:\DEV\ARDATA\rate IN 0
*!*				ELSE

				* use sql invoice tables; per Darren the code below allows us to index vinvdet, which is needed by LOADPLAT.prg further down
			     OPEN DATABASE f:\watusi\ardata\arsql
			     userv("vinvoice","arsql")
				 cursorsetprop("buffering",5,"vinvoice")

			     userv("vinvdet","arsql")
			     CURSORSETPROP("buffering",3,"vinvdet")
			     SELECT vinvdet
			     INDEX ON invdetid tag invdetid
			     SET ORDER TO
			     CURSORSETPROP("BUFFERING",5,"VINVDET")

				*!*	* use foxpro invoice tables
				*!*	USE F:\WATUSI\ARDATA\invoice ALIAS vinvoice IN 0
				*!*	USE F:\WATUSI\ARDATA\invdet ALIAS vinvdet IN 0				

				*USE F:\WATUSI\ARDATA\ARGENPK AGAIN IN 200
				useca("farhdr","ar",.t.)
				useca("farlin","ar",.t.)

*!*				ENDIF
			
			xsqlexec("select * from account where inactive=0","account",,"qq")
			index on accountid tag accountid
			set order to

			xsqlexec("select * from acctbill","acctbill",,"qq")
			index on billtoid tag billtoid
			*index on accountid tag accountid
			*index on STR(billtoid,4)+STR(accountid,4) tag billacct

			=SEEK(lnAcctID,"account","accountid")
			*=SEEK(lnAcctID,"acctbill","accountid") && this is not right
			=SEEK(m.billtoid,"acctbill","billtoid")

			*******************************************************
			* added block per DY 05/03/2017
			xsqlexec("select * from division",,,"ar")
			index on company tag company
			set order to
			 
			xsqlexec("select * from glacct",,,"ar")
			index on glcode tag glcode
			set order to
			 
			xsqlexec("select * from glchart",,,"ar")
			index on code tag code
			index on glcode+company tag glcodecomp
			index on code+company tag codecomp
			set order to
			*******************************************************

			lcHeader = "REFERENCE: INVOICE FOR SHIPPING & RECEIVING  ACTIVITIES" + CRLF + ;
				SPACE(11) + "FOR PERIOD " + DTOC(.dBeginningTransdate) + " - " + DTOC(.dEndingTransdate) + CRLF + ;
				SPACE(11) + "FOR " + ALLTRIM(.cClient) + " (AS PER ATTACHED)"

			SELECT vinvoice
			SCATTER MEMVAR BLANK memo
			*m.invnum = PADL(sqlgenpk("invnum","wh",.t.),6,'0')
			m.invnum = PADL(sqlgenpk("invnum","ar",.t.),6,'0')
			
			lcInvoiceNum = m.invnum
			
	*SET STEP ON 

			*!*					m.wo_num = VAL(outbound.del_mfst)

			*!*	DO CASE
			*!*		CASE .cClient = "KM-FOOTWEAR"
			*!*			m.wo_num = 61  
			*!*		CASE .cClient = "DSG"
			*!*			m.wo_num = 62  && hardcode this way per Darren 11/18/08 MB
			*!*		OTHERWISE && FOOTLOCKER
			*!*			m.wo_num = 63  
			*!*	ENDCASE
			
			m.wo_num = .nInvoiceWONum

			m.accountid = lnAcctID
			
			* if we have defined a separate billtoID for a client, use that instead of the main accountid
			* needed to support Off Broadway, which should use the billtoid for Rack Room.
			IF .nBillToID = 0 THEN
				m.billtoid = lnAcctID
			ELSE
				m.billtoid = .nBillToID
			ENDIF
			
			m.acctname = account.acctname
			m.billname = acctbill.billname
			m.billname2 = acctbill.billname2
			m.address = acctbill.address
			m.city = acctbill.city
			m.state = acctbill.state
			m.zip = acctbill.zip
			m.billparty = acctbill.billparty
			m.header = lcHeader
			*m.invdt = DATE()
			m.invdt = .dInvoiceDate
			m.invamt = lnTotalAmount
			************************************
			* 10/19/04 MB
			*m.billparty = "CRAIG NESPER" && don't see why this is needed -- and it doesn't support multiple clients 3/2/09 MB
			m.visibleby = lnAcctID
			************************************
			STORE .cUSERNAME TO m.addby, m.updateby
			STORE DATE() TO m.adddt, m.updatedt
			*INSERT INTO vinvoice FROM MEMVAR
 
			INSERTINTO('vinvoice','ar',.t.)
*!*					pnInvoiceID = vinvoice.InvoiceID
*!*					m.invoiceid = pnInvoiceID

			lnLineNum = 0
			SELECT curInvoice
			SCAN FOR AMOUNT > 0
				*lnLineNum = lnLineNum + 1
				SELECT vinvdet
				SCATTER MEMVAR BLANK
				m.invoiceid = vinvoice.invoiceid
				m.invnum = vinvoice.invnum
				*m.linenum = lnLineNum
				IF curInvoice.LCAPEX THEN
					m.description = DTOC(curInvoice.TRANSDATE) + "    -  " + TRANSFORM(curInvoice.AMOUNT,"@$ 999,999.99") + " Capex Recovery"
					m.glcode = '19'
					m.COMPANY = .cInvDetCompany
				ELSE
					m.description = DTOC(curInvoice.TRANSDATE) + "    -  " + TRANSFORM(curInvoice.AMOUNT,"@$ 999,999.99")
					*!*	m.glcode = '13'
					* support breaking out pick and pack (tilt tray) as separate invoice detail records per Juan 06/21/2017						
					DO CASE
						CASE UPPER(ALLTRIM(curInvoice.transtype)) == "MAIN"
							m.glcode = ''
							m.COMPANY = ''
						CASE UPPER(ALLTRIM(curInvoice.transtype)) == "TILTTRAY"
							m.glcode = '31'
							m.COMPANY = .cInvDetCompany
						OTHERWISE
							* UPPER(ALLTRIM(curInvoice.transtype)) == "HANDLING"
							m.glcode = '13'
							m.COMPANY = .cInvDetCompany
					ENDCASE			
				ENDIF
				m.invdetamt = curInvoice.AMOUNT
				m.main = curInvoice.lmain
				m.split = curInvoice.lsplit
				IF m.split THEN
					* Blank out the description, not needed 
					* and do not increment the line number - because we want it to be the same as the Main or split record which came previously
					m.description = ''
				ELSE
					* increment the line number
					lnLineNum = lnLineNum + 1
				ENDIF
				m.linenum = lnLineNum
				*m.COMPANY = 'A'
				*m.COMPANY = .cInvDetCompany  && per Darren 12/07/2015
				*.TrackProgress('=====>>> m.COMPANY = ' + m.COMPANY,LOGIT+SENDIT)
				*m.invdetid = dygenpk("invdet","ar")
				STORE .cUSERNAME TO m.addby, m.updateby
				STORE DATE() TO m.adddt, m.updatedt
				STORE 'BILLSEARS' TO m.updproc, m.addproc
				*INSERT INTO vinvdet FROM MEMVAR
				INSERTINTO('vinvdet','ar',.t.)
			ENDSCAN

			SELECT vinvdet
			*SET FILTER TO invoiceid = pnInvoiceID
			*SET FILTER TO (invoiceid = pnInvoiceID) AND (NOT SPLIT) && ADDED 06/23/2017 so that split records will not show on the invoice pdf
			SET FILTER TO (NOT SPLIT) && 11/21/2017 MB all recs should have the same invoiceid so that filter is not needed
			GOTO TOP

			* create PDF version of invoice and merge it with consolidated bill...
			.PrintInvoiceToPDF()

*!*					SELECT vinvdet
*!*					SET FILTER TO invoiceid = pnInvoiceID

			*!*					.nSQLConnection = SQLCONNECT("PLATINUM2")
			*!*					IF .nSQLConnection > 0

			IF .lTestInvoice THEN
				* NOTHING
				.TrackProgress('NOT Sending invoice to Platinum...',LOGIT+WAITIT+SENDIT)
			ELSE
			
				SET STEP ON 
				
				* load invoice into Platinum
				.TrackProgress('Sending invoice to Platinum...',LOGIT+WAITIT+SENDIT)

				* turn off the filter - not needed any more and might crash TU()
				SELECT vinvdet
				SET FILTER TO 
				GOTO TOP

				*SELECT vinvdet
				*SET FILTER TO invoiceid = pnInvoiceID
				
				
				*************************************************************************************************************************
				DO loadplat  && this is Darren's program for loading the invoice into Platinum - you just use it as is - don't modify it!
				*************************************************************************************************************************
				
				
				* tableupate SQL invoice tables
				* only do the TU() on Invoice recs if the Invdet TU() succeeded 11/21/2017 MB
				IF tu("vinvdet") THEN
					.TrackProgress('SUCCESS: TU(vinvdet) returned .T.!',LOGIT+SENDIT)
					IF tu("vinvoice") THEN
						.TrackProgress('SUCCESS: TU(vinvoice) returned .T.!',LOGIT+SENDIT)
					ELSE
						* ERROR on table update
						.TrackProgress('ERROR: TU(vinvoice) returned .F.!',LOGIT+SENDIT)
						=AERROR(UPCERR)
						IF TYPE("UPCERR")#"U" THEN 
							.TrackProgress('Error Description = ' + UPCERR[1,2],LOGIT+SENDIT)
						ENDIF
					ENDIF
				ELSE
					* ERROR on table update
					.TrackProgress('ERROR: TU(vinvdet) returned .F.!',LOGIT+SENDIT)
					=AERROR(UPCERR)
					IF TYPE("UPCERR")#"U" THEN 
						.TrackProgress('Error Description = ' + UPCERR[1,2],LOGIT+SENDIT)
					ENDIF
				ENDIF
				
				
			*!*	IF tu("vinvoice") THEN
			*!*		.TrackProgress('SUCCESS: TU(vinvoice) returned .T.!',LOGIT+SENDIT)
			*!*	ELSE
			*!*		* ERROR on table update
			*!*		.TrackProgress('ERROR: TU(vinvoice) returned .F.!',LOGIT+SENDIT)
			*!*		=AERROR(UPCERR)
			*!*		IF TYPE("UPCERR")#"U" THEN 
			*!*			.TrackProgress('Error Description = ' + UPCERR[1,2],LOGIT+SENDIT)
			*!*		ENDIF
			*!*	ENDIF
			*!*	IF tu("vinvdet") THEN
			*!*		.TrackProgress('SUCCESS: TU(vinvdet) returned .T.!',LOGIT+SENDIT)
			*!*	ELSE
			*!*		* ERROR on table update
			*!*		.TrackProgress('ERROR: TU(vinvdet) returned .F.!',LOGIT+SENDIT)
			*!*		=AERROR(UPCERR)
			*!*		IF TYPE("UPCERR")#"U" THEN 
			*!*			.TrackProgress('Error Description = ' + UPCERR[1,2],LOGIT+SENDIT)
			*!*		ENDIF
			*!*	ENDIF
				
			ENDIF
			*!*					ELSE
			*!*						.TrackProgress('ERROR: Unable to connect to Platinum in GenerateInvoice()',LOGIT+SENDIT)
			*!*					ENDIF

			USE IN vinvoice
			USE IN vinvdet
			USE IN farhdr
			USE IN farlin
			*USE IN ARGENPK
			USE IN account
			USE IN acctbill
			USE IN GLCHART

			IF .lTestInvoice THEN
				.cInvoiceMessage = " - Generated Test Invoice # " + lcInvoiceNum
			ELSE
				.cInvoiceMessage = " - Generated Production Invoice # " + lcInvoiceNum
			ENDIF

			.TrackProgress('--------------------------------------------------------',LOGIT+SENDIT)
			.TrackProgress('--------------------------------------------------------',LOGIT+SENDIT)
			.TrackProgress('Generated FoxPro Invoice # ' + lcInvoiceNum,LOGIT+SENDIT)
			.TrackProgress('--------------------------------------------------------',LOGIT+SENDIT)
			.TrackProgress('--------------------------------------------------------',LOGIT+SENDIT)
		ENDWITH
		RETURN llRetVal
	ENDFUNC  &&  GenerateInvoice



	FUNCTION PrintInvoiceToPDF
		LOCAL llRetVal, lnSelect, lnLastPage, lcPDFFile,lcPDFTable, lcDataPath, lcDataBasePath, lcDataBase, lcPath, loAcrobat
		LOCAL lcBigPDFFile, lcInvoiceTempRpt, lnVinvoiceID
		llRetVal = .T.


		WITH THIS
			.TrackProgress('Printing FoxPro Invoice to PDF...',LOGIT+WAITIT+SENDIT)

			lnSelect = SELECT()

			IF USED('xrpt') THEN
				USE IN xrpt
			ENDIF

			CREATE CURSOR xrpt ( ;
				line1 C(65), ;
				line2 C(65), ;
				line3 C(65), ;
				line4 C(65), ;
				line5 C(65), ;
				line6 C(65), ;
				line7 C(65), ;
				line8 C(65), ;
				line9 C(65), ;
				line10 C(65), ;
				line11 C(65), ;
				line12 C(65), ;
				line13 C(65), ;
				line14 C(65), ;
				line15 C(65), ;
				line16 C(65), ;
				line17 C(65), ;
				line18 C(65), ;
				line19 C(65), ;
				line20 C(65), ;
				line21 C(65), ;
				line22 C(65), ;
				line23 C(65), ;
				line24 C(65), ;
				amt1 C(12), ;
				amt2 C(12), ;
				amt3 C(12), ;
				amt4 C(12), ;
				amt5 C(12), ;
				amt6 C(12), ;
				amt7 C(12), ;
				amt8 C(12), ;
				amt9 C(12), ;
				amt10 C(12), ;
				amt11 C(12), ;
				amt12 C(12), ;
				amt13 C(12), ;
				amt14 C(12), ;
				amt15 C(12), ;
				amt16 C(12), ;
				amt17 C(12), ;
				amt18 C(12), ;
				amt19 C(12), ;
				amt20 C(12), ;
				amt21 C(12), ;
				amt22 C(12), ;
				amt23 C(12), ;
				amt24 C(12))

			PUBLIC gheader1, gheader2, gheader3, gcInvNumBarCode
			IF EMPTY(vinvoice.billname2)
				gheader1=vinvoice.address
				gheader2=dispcsz(vinvoice.city,vinvoice.state,vinvoice.zip)
				gheader3=""
			ELSE
				gheader1=vinvoice.billname2
				gheader2=vinvoice.address
				gheader3=dispcsz(vinvoice.city,vinvoice.state,vinvoice.zip)
			ENDIF

			DECLARE xline[48]

			SELECT vinvdet
			*!*				xotherfilter=FILTER()
			*!*				IF !EMPTY(xotherfilter)
			*!*					SET FILTER TO (&xotherfilter) AND !SPLIT
			*!*				ELSE
			*!*					SET FILTER TO !SPLIT
			*!*				ENDIF
			
			GO TOP

			FOR xpage=1 TO 9999
				xline=""

				xcnt=0
				IF xpage=1
					IF vinvoice.wo_num>3
						xcnt=xcnt+2
						xline[1]="TGF WORK ORDER: "+LTRIM(STR(vinvoice.wo_num))
					ENDIF
					IF !vinvoice.acctname==vinvoice.billname OR vinvoice.accountid#vinvoice.billtoid
						xcnt=xcnt+2
						xline[3]="FOR THE ACCOUNT OF: "+vinvoice.acctname
					ENDIF
					FOR j=1 TO MEMLINES(vinvoice.HEADER)
						xline[j+xcnt] = MLINE(vinvoice.HEADER,j)
					NEXT
					xcnt=xcnt+MEMLINES(vinvoice.HEADER)+1
				ENDIF

				j=0
				SELECT vinvdet
				SCAN NEXT (24-xcnt)
					j=j+1
					IF vinvdet.DESCRIPTION#"."
						xline[j+xcnt] = vinvdet.DESCRIPTION
						IF vinvdet.invdetamt#0
							xline[j+xcnt+24] = "$"+TRANS(vinvdet.invdetamt,"@Z 999,999.99")
						ENDIF
					ENDIF
				ENDSCAN

				INSERT INTO xrpt FROM ARRAY xline

				IF EOF()
					EXIT
				ELSE
					IF xpage#1 OR xcnt<24
						SKIP 1 IN vinvdet
					ENDIF
					IF EOF()
						EXIT
					ENDIF
				ENDIF
			NEXT

			*!*				SELECT vinvdet
			*!*				IF !EMPTY(xotherfilter)
			*!*					SET FILTER TO &xotherfilter
			*!*				ELSE
			*!*					SET FILTER TO
			*!*				ENDIF
*SET STEP ON 


			gtotpages = xpage
			*gcompany = "TGF INC."  
			gcompany = "Toll Global Forwarding SCS (USA) Inc."
			xaddby = IIF(!EMPTY(vinvoice.addby),vinvoice.addby,gUserid)

			* 4/27/2018 MB for testing lockbox address changes on the invoices
			IF .lTestInvoice AND (UPPER(.cCOMPUTERNAME) = 'MBENNETT2') THEN
				*.cInvoiceFRX = 'M:\DEV\RPT\INVOICEP.FRX'
				*.cInvoiceFRT = 'M:\DEV\RPT\INVOICEP.FRT'
				.cInvoiceFRX = 'M:\DEV\RPT\INVOICEPTEST.FRX'
				.cInvoiceFRT = 'M:\DEV\RPT\INVOICEPTEST.FRT'
			ENDIF
			
			COPY FILE (.cInvoiceFRX) TO (.cInvoiceFRXPDF)  && 'C:\TEMPFOX\INVOICEPDF.FRX'
			COPY FILE (.cInvoiceFRT) TO (.cInvoiceFRTPDF)  && 'C:\TEMPFOX\INVOICEPDF.FRT'
			
			*WAIT WINDOW "Copying report form to temp folder...please wait" TIMEOUT 5

			lcInvoiceTempRpt = (.cInvoiceTempRpt) + "_" + STRTRAN(DTOC(.dTransDate),"/")  &&  'C:\TEMPFOX\INVOICEPDF' + "_" + STRTRAN(DTOC(.dTransDate),"/")
			
			SELECT xrpt

			.CreatePDFReport(lcInvoiceTempRpt, .cInvoiceTempRpt)

			lcPDFFile = lcInvoiceTempRpt + ".PDF"

			IF NOT .lAutoMode THEN
				*.MergePDFFiles(lcPDFFile)
				.MergeInvoicePDFFile(lcPDFFile)
			ENDIF

			IF .lAttachSeparateReports THEN
				* add to attachments
				.AddAttachment(lcPDFFile)
			ENDIF

			*************************************
			*************************************
			* load the PDF into AR PDF tables
			.TrackProgress('Loading FoxPro Invoice PDF...',LOGIT+WAITIT+SENDIT)
				
			* load consolidated PDF into AR, not just the summary page,
			lcBigPDFFile = .cBigPDFFile
			
			IF .lUseSQLInvPDF THEN
			
				PUBLIC GSYSTEMMODULE
				GSYSTEMMODULE = "INVPDF"
			
				IF .lTestInvoice THEN
					
					lnVinvoiceID = 333	
					
				ELSE
				
					lnVinvoiceID = vinvoice.invoiceid	
					
				ENDIF && .lTestInvoice
				
				.TrackProgress('======> VINVOICE.INVOICEID going into SQL INVPDF = ' + TRANSFORM(lnVinvoiceID),LOGIT+SENDIT)
			

				USECA("INVPDF","AR",.F.,,"SELECT * FROM INVPDF.DBO.INVPDF WHERE INVOICEID = " + transform(lnVinvoiceID))

				SELECT invpdf
				delete in invpdf
				INSERT INTO invpdf (invoiceid, addby, adddt) VALUES (lnVinvoiceID, "SEARS", datetime())
				APPEND MEMO pdf FROM (lcPDFFile) OVERWRITE
				tu("invpdf")

				=AERROR(PDFERR)
				IF (TYPE("PDFERR")#"U") AND INLIST(PDFERR[1,1],1526,1542,1543)
					x3winmsg(PDFERR[1,2])
				ENDIF					
			
			ELSE


*!*					IF .lTestInvoice THEN
*!*						lcDataBasePath = "M:\DEV\ARDATA\"
*!*						lcDataPath = "M:\DEV\ARDATA\"
*!*					ELSE
*!*						lcDataBasePath = "F:\WATUSI\ARDATA\"
*!*						lcDataPath = "G:\INVPDF\"
*!*					ENDIF
*!*					lcDataBase = lcDataBasePath + "AR"

*!*					* open and/or create PDF table if necessary

*!*					*open database f:\watusi\ardata\ar
*!*					OPEN DATABASE (lcDataBase)
*!*					lcPDFTable = "pdf" + dt2month(vinvoice.invdt,.T.)

*!*					IF NOT FILE(lcDataPath + lcPDFTable + ".dbf") THEN
*!*						CREATE TABLE (lcDataPath + lcPDFTable) FREE  ( ;
*!*							invoiceid i NOT NULL, ;
*!*							pdf M NOT NULL, ;
*!*							FLAG l NOT NULL)
*!*						INDEX ON invoiceid TAG invoiceid
*!*					ENDIF

*!*					IF NOT USED(lcPDFTable)
*!*						*USE ("ar!"+lcPDFTable) IN 0
*!*						USE (lcDataPath + lcPDFTable) IN 0
*!*					ENDIF
*!*					*************************************************************************************************
*!*					*************************************************************************************************
*!*					*************************************************************************************************

*!*					SELECT (lcPDFTable)
*!*					IF !SEEK(vinvoice.invoiceid,lcPDFTable,"invoiceid")
*!*						APPEND BLANK
*!*						REPLACE invoiceid WITH vinvoice.invoiceid
*!*					ENDIF

*!*					******************************************************************
*!*					******************************************************************
*!*					* load consolidated PDF into AR, not just the summary page,
*!*					* per Greg 03/31/06 MB
*!*					lcBigPDFFile = .cBigPDFFile
*!*					* put .pdf into memo field
*!*					*APPEND MEMO pdf FROM (lcPDFFile) OVERWRITE

*!*					* Consolidated PDF is now Invoice pdf 4/17/07 MB
*!*					APPEND MEMO pdf FROM (lcPDFFile) OVERWRITE
*!*					******************************************************************
				******************************************************************
				
	*!*				* the 'REPLACE pdfloaded WITH .T. IN vinvoice' below was failing; try closing the PDF table
	*!*				USE IN (lcPDFTable)
			ENDIF && .lUseSQLInvPDF

			* set flag in invoice table & save

			REPLACE pdfloaded WITH .T. IN vinvoice
			*=TABLEUPDATE(.T.,.T.,"vinvoice")

			COPY FILE (lcPDFFile) TO ("f:\watusi\pdfbak\" + vinvoice.invnum + "_" + STRTRAN(DTOC(.dTransDate),"/") + ".pdf")
			*************************************

			* add Consolidated Invoice file to attachments
			.AddAttachment(lcPDFFile)

			SELECT (lnSelect)

		ENDWITH
		RETURN llRetVal
	ENDFUNC  &&  PrintInvoiceToPDF


	FUNCTION CreatePDFReport
		* create PDF report
		LPARAMETERS tcRootPDFFilename, tcPDFReportFRX
		
		WITH THIS
					
			.TrackProgress('in CreatePDFReport tcRootPDFFilename=' + tcRootPDFFilename, LOGIT+SENDIT)
			.TrackProgress('in CreatePDFReport tcPDFReportFRX=' + tcPDFReportFRX, LOGIT+SENDIT)
			
			LOCAL llPDFReportWasCreated, lcRootPDFFilename, lcFullPDFFilename, lcPDFReportFRX
			lcCentury = SET("CENTURY")

			SET CENTURY OFF  && FOR XX/XX/XX FORMAT DATES ON REPORT
			lcRootPDFFilename = tcRootPDFFilename
			lcPDFReportFRX = tcPDFReportFRX

			lcFullPDFFilename = lcRootPDFFilename + ".PDF"

			llPDFReportWasCreated = .F.

			DO M:\DEV\PRG\RPT2PDF WITH lcPDFReportFRX, lcRootPDFFilename, llPDFReportWasCreated
			*DO M:\DEV\PRG\RPT2PDF_NOTRIES WITH lcPDFReportFRX, lcRootPDFFilename, llPDFReportWasCreated

			IF NOT llPDFReportWasCreated THEN
				.TrackProgress('ERROR - PDF file [' + lcFullPDFFilename + '] was not created',LOGIT+WAITIT+SENDIT)
			ENDIF

			*!*				* keep track of pdf report files for emailing later
			*!*				.AddAttachment(lcFullPDFFilename)

			SET PRINTER TO DEFAULT
			SET CENTURY &lcCentury
		ENDWITH
	ENDFUNC  &&  CreatePDFReport


	FUNCTION CreateInvoiceCursor
		LOCAL llRetVal
		llRetVal = .T.
		IF USED('curInvoice') THEN
			USE IN curInvoice
		ENDIF
		CREATE CURSOR curInvoice( ;
			TRANSDATE d, ;
			LCAPEX l, ;
			TRANSTYPE C(8), ;
			LMAIN l, ;
			LSPLIT l, ;
			AMOUNT N(15,2))
		RETURN llRetVal
	ENDFUNC


	FUNCTION CreateALLRCNCursor
		LOCAL llRetVal
		llRetVal = .T.
		IF USED('curAllRCNs') THEN
			USE IN curAllRCNs
		ENDIF
		CREATE CURSOR curAllRCNs ( ;
			TRANSDATE d, ;
			RCN i, ;
			CONTAINER C(12), ;
			COMPANY C(2), ;
			XDUNITS i, ;
			XDCARTONS i, ;
			UNITS i, ;
			RRUNITS i, ;
			SKUSPERRCN i, ;
			CARTONS i, ;
			XFERCTNS i, ;
			RETURNCTNS i, ;
			SKU0 N(16,3), ;
			SKU21 N(16,3), ;
			SKU31 N(16,3), ;
			SKU41 N(16,3), ;
			RNXDRATE N(6,3), ;
			RXDOCKRATE N(6,3), ;
			RRCTNRATE N(6,3) ;
			)
		RETURN llRetVal
	ENDFUNC


	FUNCTION CreateALLSHIPPEDCursor
		LOCAL llRetVal
		llRetVal = .T.
		IF USED('curAllSHIPPED') THEN
			USE IN curAllSHIPPED
		ENDIF
		CREATE CURSOR curAllSHIPPED ( ;
			TRANSDATE d, ;
			TRAILERLD C(6), ;
			CONTAINER C(12), ;
			COMPANY C(2), ;
			TOTCASES i, ;
			TOTXDOCK i, ;
			TOTMANUAL i, ;
			TOTRSDCTN i, ;
			TOTFULL i, ;
			TOTTILT i, ;
			TOTFINDING i, ;
			SXDOCKRATE N(6,3), ;
			TILTRATE N(6,3), ;
			TILTRSRATE N(6,3), ;
			FCASERATE N(6,3) ;
			)
		RETURN llRetVal
	ENDFUNC


	FUNCTION CreateReceiptDetailPDF
		WITH THIS
			LOCAL llRetVal, lcRootPDFFilename
			PRIVATE pcBillFor, pcNoDataForTheseDates, pnReceiptCrossDockPerCtnRate, pnReceiptNonXDockPerCtnRate, pnReceiptNonXDockPerUNITRate
			PRIVATE pnReturnReceiptPerCtnRate
			
			llRetVal = .T.
			* report on sku breakdown detail
			pcBillFor = "By Transaction Date"
			pnReceiptCrossDockPerCtnRate = .nReceiptCrossDockPerCtnRate
			pnReceiptNonXDockPerCtnRate = .nReceiptNonXDockPerCtnRate
			pnReturnReceiptPerCtnRate = .nReturnReceiptPerCtnRate
			pnReceiptNonXDockPerUNITRate = .nReceiptNonXDockPerUNITRate
			
			lcRootPDFFilename = "c:\tempfox\SKU_BREAKDOWN_DETAIL_" + STRTRAN(DTOC(DATE()),"/")
			
*!*				IF NOT EMPTY(.cNoDataForTheseDates) THEN
*!*					pcNoDataForTheseDates = "There was no transaction data for the following dates:" + CRLF + .cNoDataForTheseDates
*!*				ELSE
*!*					pcNoDataForTheseDates = ""
*!*				ENDIF
					
			IF NOT EMPTY(.cNoReceiptDataForTheseDates) THEN
				pcNoDataForTheseDates = "There was no Receipt transaction data for the following dates:" + CRLF + .cNoReceiptDataForTheseDates
			ELSE
				pcNoDataForTheseDates = ""
			ENDIF
			
			
			
			SELECT curAllRCNs
			COPY TO ("c:\tempfox\curallrcns" + DTOS(.dTransDate) +  ".xls") xl5
*!*				
*!*				
*!*				
*!*	SELECT curAllRCNs
*!*	BROWSE
*!*				
*!*				
			.TrackProgress(".dTransDate = " + TRANSFORM(.dTransDate), LOGIT+SENDIT)
			.TrackProgress("pnReceiptCrossDockPerCtnRate = " + TRANSFORM(pnReceiptCrossDockPerCtnRate), LOGIT+SENDIT)
			.TrackProgress("pnReceiptNonXDockPerCtnRate = " + TRANSFORM(pnReceiptNonXDockPerCtnRate), LOGIT+SENDIT)
			.TrackProgress("pnReturnReceiptPerCtnRate = " + TRANSFORM(pnReturnReceiptPerCtnRate), LOGIT+SENDIT)
			.TrackProgress("pnReceiptNonXDockPerUNITRate = " + TRANSFORM(pnReceiptNonXDockPerUNITRate), LOGIT+SENDIT)

			SELECT curAllRCNs
			GOTO TOP
			IF NOT EOF() THEN
				*.CreatePDFReport(lcRootPDFFilename, "M:\DEV\RPT\SKUBKDN")
				.CreatePDFReport(lcRootPDFFilename, .cBillReceiptDetailReport)
			ELSE
				* no data - run the NO DATA report
				* make dummy cursor to drive report
				IF USED('DUMMY') THEN
					USE IN DUMMY
				ENDIF
				CREATE CURSOR DUMMY (DUMMY C(1))
				SELECT DUMMY
				APPEND BLANK
				pcNoDataForTheseDates = "No receipt transaction data."
				*pcBillFor = "For Transaction Date: " + DTOC(.dTransDate)
				*.CreatePDFReport(lcRootPDFFilename, "M:\DEV\RPT\SKUBKDNNODATA")
				.CreatePDFReport(lcRootPDFFilename, .cBillReceiptDetailReportNODATA)
			ENDIF
			IF NOT .lAutoMode THEN
				.MergePDFFiles(lcRootPDFFilename + ".PDF")
			ENDIF
			IF .lAttachSeparateReports THEN
				* add to attachments
				.AddAttachment(lcRootPDFFilename + ".PDF")
			ENDIF
			.TrackProgress('Created SKU Breakdown Detail PDF from curALLRCNs.',LOGIT)
			.TrackProgress(.cClient + ' CreateReceiptDetailPDF() ended normally.', LOGIT+SENDIT)
		ENDWITH
		RETURN llRetVal
	ENDFUNC   &&  CreateReceiptDetailPDF


*!*		FUNCTION CreateSKUBreakdownDetailPDFXDOCK
*!*			WITH THIS
*!*				LOCAL llRetVal, lcRootPDFFilename
*!*				PRIVATE pcBillFor, pcNoDataForTheseDates, pnReceiptCrossDockPerCtnRate
*!*				llRetVal = .T.
*!*				* report on sku breakdown detail - XDOCK FIELDS ONLY
*!*				pcBillFor = "By Transaction Date"
*!*				pnReceiptCrossDockPerCtnRate = .nReceiptCrossDockPerCtnRate
*!*				lcRootPDFFilename = "c:\tempfox\SKU_BREAKDOWN_DETAIL_XDOCK_" + STRTRAN(DTOC(DATE()),"/")
*!*				IF NOT EMPTY(.cNoDataForTheseDates) THEN
*!*					pcNoDataForTheseDates = "There was no transaction data for the following dates:" + CRLF + .cNoDataForTheseDates
*!*				ELSE
*!*					pcNoDataForTheseDates = ""
*!*				ENDIF
*!*				SELECT curAllRCNs
*!*				GOTO TOP
*!*				IF NOT EOF() THEN
*!*					.CreatePDFReport(lcRootPDFFilename, "M:\DEV\RPT\SKUBKDNXDOCK")
*!*				ELSE
*!*					* no data - run the NO DATA report
*!*					* make dummy cursor to drive report
*!*					IF USED('DUMMY') THEN
*!*						USE IN DUMMY
*!*					ENDIF
*!*					CREATE CURSOR DUMMY (DUMMY C(1))
*!*					SELECT DUMMY
*!*					APPEND BLANK
*!*					*pcBillFor = "For Transaction Date: " + DTOC(.dTransDate)
*!*					.CreatePDFReport(lcRootPDFFilename, "M:\DEV\RPT\SKUBKDNNODATAXDOCK")
*!*				ENDIF
*!*				IF NOT .lAutoMode THEN
*!*					.MergePDFFiles(lcRootPDFFilename + ".PDF")
*!*				ENDIF
*!*				IF .lAttachSeparateReports THEN
*!*					* add to attachments
*!*					.AddAttachment(lcRootPDFFilename + ".PDF")
*!*				ENDIF
*!*				.TrackProgress('Created SKU XDOCK-ONLY Breakdown Detail PDF from curALLRCNs.',LOGIT)
*!*				.TrackProgress(.cClient + ' CreateSKUBreakdownDetailPDFXDOCK() ended normally.', LOGIT+SENDIT)
*!*			ENDWITH
*!*			RETURN llRetVal
*!*		ENDFUNC   &&  CreateSKUBreakdownDetailPDF


	FUNCTION CreateShippedDetailPDF
		WITH THIS
			LOCAL llRetVal, lcRootPDFFilename
			llRetVal = .T.
			PRIVATE pcBillFor, pcNoDataForTheseDates
			PRIVATE pnShippedCrossDockPerCtnRate, pnSKU_0_PerCtnRate, pnSKU_21_PerCtnRate, pnSKU_31_PerCtnRate, pnSKU_41_PerCtnRate, pnFullCasePerCtnRate
			PRIVATE pnTiltPerUnitRate, pnManualPerUnitRate, pnFindingsPerCtnRate, pnReceiptNonXDockPerUNITRate, pnTiltResidualsPerCartonRate
			pnShippedCrossDockPerCtnRate     = .nShippedCrossDockPerCtnRate
			pnSKU_0_PerCtnRate        = .nSKU_0_PerCtnRate
			pnSKU_21_PerCtnRate       = .nSKU_21_PerCtnRate
			pnSKU_31_PerCtnRate       = .nSKU_31_PerCtnRate
			pnSKU_41_PerCtnRate       = .nSKU_41_PerCtnRate
			pnFullCasePerCtnRate      = .nFullCasePerCtnRate
			pnTiltPerUnitRate         = .nTiltPerUnitRate
			pnManualPerUnitRate       = .nManualPerUnitRate
			pnFindingsPerCtnRate      = .nFindingsPerCtnRate
			pnReceiptNonXDockPerUNITRate = .nReceiptNonXDockPerUNITRate
			pnTiltResidualsPerCartonRate = .nTiltResidualsPerCartonRate

			* report on sku breakdown detail
			pcBillFor = "By Transaction Date"
			lcRootPDFFilename = "c:\tempfox\SHIPPED_DETAIL_" + STRTRAN(DTOC(DATE()),"/")
			
			*SELECT curAllSHIPPED
			*GOTO TOP

			*!*	SELECT curAllSHIPPED
			*!*	BROWSE
			*pcBillFor = "For Transaction Date: " + DTOC(.dTransDate)
			
*!*				IF NOT EMPTY(.cNoDataForTheseDates) THEN
*!*					pcNoDataForTheseDates = "There was no transaction data for the following dates:" + CRLF + .cNoDataForTheseDates
*!*				ELSE
*!*					pcNoDataForTheseDates = ""
*!*				ENDIF
					
			IF NOT EMPTY(.cNoShippedDataForTheseDates) THEN
				pcNoDataForTheseDates = "There was no Shipped transaction data for the following dates:" + CRLF + .cNoShippedDataForTheseDates
			ELSE
				pcNoDataForTheseDates = ""
			ENDIF
					
*!*	SELECT curAllSHIPPED
*!*	BROWSE
*!*	SET STEP ON 

			SELECT curAllSHIPPED
			COPY TO ("c:\tempfox\curAllSHIPPED" + DTOS(.dTransDate) +  ".xls") xl5


			SELECT curAllSHIPPED
			LOCATE

			IF NOT EOF() THEN
				*.CreatePDFReport(lcRootPDFFilename, "M:\DEV\RPT\SHIPDETL")
				.CreatePDFReport(lcRootPDFFilename, .cBillShippedDetailReport)
			ELSE
				* no data - run the NO DATA report
				* make dummy cursor to drive report
				IF USED('DUMMY') THEN
					USE IN DUMMY
				ENDIF
				CREATE CURSOR DUMMY (DUMMY C(1))
				SELECT DUMMY
				APPEND BLANK
				pcNoDataForTheseDates = "No shipped transaction data."
				*.CreatePDFReport(lcRootPDFFilename, "M:\DEV\RPT\SHIPDETLNODATA")
				.CreatePDFReport(lcRootPDFFilename, .cBillShippedDetailReportNODATA)
			ENDIF
			IF NOT .lAutoMode THEN
				.MergePDFFiles(lcRootPDFFilename + ".PDF")
			ENDIF
			IF .lAttachSeparateReports THEN
				* add to attachments
				.AddAttachment(lcRootPDFFilename + ".PDF")
			ENDIF
			.TrackProgress('Created Shipped Detail PDF from curALLSHIPPED.',LOGIT)
			.TrackProgress(.cClient + ' CreateShippedDetailPDF() ended normally.', LOGIT+SENDIT)
		ENDWITH
		RETURN llRetVal
	ENDFUNC   &&  CreateShippedDetailPDF



	FUNCTION CreateWeeklyBillSummary
		* WEEKLY BILL SUMMARY REPORT (NEW FOR RACK ROOM)
		*******************************************************************************************************************************
		**** no longer producing this sheet, because it is not correct if the rates change during the billed week. MB 4/11/2017	
		**** It will show the rates from the last day of the billing period, which in rare cases will be different from the prior days.
		*******************************************************************************************************************************
		
		WITH THIS
			LOCAL llRetVal, lcRootPDFFilename
			llRetVal = .T.

			.TrackProgress("Creating Weekly Bill Summary PDF Report Page...", LOGIT+NOWAITIT)

			PRIVATE pnReceiptCrossDockPerCtnRate, pnReceiptNonXDockPerUNITRate, pnReceiptNonXDockTRANSFERPerCtnRate, pnReturnReceiptPerCtnRate
			PRIVATE pnShippedCrossDockPerCtnRate, pnTiltPerUnitRate, pnTiltResidualsPerUnitRate, pnFullCasePerCtnRate

			PRIVATE pnReceiptCrossDockCtnsAll, pnReceiptNonXDockUNITSAll, pnReceiptNonXDockTRANSFERCtnsAll, pnReturnReceiptCtnsAll, pnTotalReceiptAll
			PRIVATE pnShippedCrossDockCtnsAll, pnTiltUnitsAll, pnTiltResidualUnitsAll, pnFullCaseCtnsAll, pnTotalShippedAll, pnTotalChargesAll
			PRIVATE pcBillFor, pcTitle, pnTiltResidualCartonsAll, pnTiltResidualsPerCartonRate
			
			* assign weekly totals for Rack Room to private report variables
			pnReceiptCrossDockCtnsAll = .nReceiptCrossDockCtnsAll
			pnReceiptNonXDockUNITSAll = .nReceiptNonXDockUNITSAll
			pnReceiptNonXDockTRANSFERCtnsAll = .nReceiptNonXDockTRANSFERCtnsAll
			pnReturnReceiptCtnsAll = .nReturnReceiptCtnsAll
			pnTotalReceiptAll = .nTotalReceiptAll
			pnShippedCrossDockCtnsAll = .nShippedCrossDockCtnsAll
			pnTiltUnitsAll = .nTiltUnitsAll
			pnTiltResidualUnitsAll = .nTiltResidualUnitsAll
			pnTiltResidualCartonsAll = .nTiltResidualCartonsAll
			pnFullCaseCtnsAll = .nFullCaseCtnsAll
			pnTotalShippedAll = .nTotalShippedAll
			pnTotalChargesAll = .nTotalChargesAll
			
			* assign rates for Rack Room to private report variables
			pnReceiptCrossDockPerCtnRate = .nReceiptCrossDockPerCtnRate
			pnReceiptNonXDockPerUNITRate = .nReceiptNonXDockPerUNITRate
			pnReceiptNonXDockTRANSFERPerCtnRate = .nReceiptNonXDockTRANSFERPerCtnRate
			pnReturnReceiptPerCtnRate = .nReturnReceiptPerCtnRate
			pnShippedCrossDockPerCtnRate = .nShippedCrossDockPerCtnRate
			pnTiltPerUnitRate = .nTiltPerUnitRate
			pnTiltResidualsPerUnitRate = .nTiltResidualsPerUnitRate
			pnTiltResidualsPerCartonRate = .nTiltResidualsPerCartonRate
			pnFullCasePerCtnRate = .nFullCasePerCtnRate


			pcBillFor = "For Period: " + DTOC(.dBeginningTransdate) + " - " + DTOC(.dEndingTransdate)
			pcTitle = .cClient + " WEEKLY BILL SUMMARY"
			
			* make dummy cursor to drive report, which is all variables
			IF USED('DUMMY') THEN
				USE IN DUMMY
			ENDIF
			CREATE CURSOR DUMMY (DUMMY C(1))
			SELECT DUMMY
			APPEND BLANK

			*SET STEP ON

			lcRootPDFFilename = (.cPDFFilenameWeekly) + "_" + STRTRAN(DTOC(.dBeginningTransdate),"/")

			.CreatePDFReport(lcRootPDFFilename, .cPDFWeeklyReportFRX)

			* create a delay here because we were getting errors saying that it could not find the file just created, in .MergePDFFiles()
			WAIT WINDOW TIMEOUT .nTimeOutSeconds

			IF NOT .lAutoMode THEN
				.MergePDFFiles(lcRootPDFFilename + ".PDF")
			ENDIF

			IF .lAttachSeparateReports THEN
				* add to attachments
				.AddAttachment(lcRootPDFFilename + ".PDF")
			ENDIF

			IF USED('DUMMY') THEN
				USE IN DUMMY
			ENDIF
			
		ENDWITH
		RETURN llRetVal
	ENDFUNC   &&  CreateWeeklyBillSummary



	FUNCTION SendEmail
		WITH THIS
			LOCAL llRetVal, lcInvoiceTempRpt, lcPDFFile
			llRetVal = .T.
			IF NOT EMPTY(.dBeginningTransdate) AND NOT EMPTY(.dEndingTransdate) THEN
				* show range of bill dates in subject
				.cSubject = .cClient + ' Bill Attachments for period ' + DTOC(.dBeginningTransdate) + " - " + DTOC(.dEndingTransdate) + " " + .cInvoiceMessage
			ELSE
				* show single bill date in subject
				.cSubject = .cClient + ' Bill Attachments for date ' + DTOC(.dTransDate) + " " + .cInvoiceMessage
			ENDIF
			.TrackProgress('About to send PDF Attachment(s) email.',LOGIT)
			IF .nValidationErrors > 0 THEN
				.TrackProgress('',LOGIT+SENDIT)
				.TrackProgress('!!!!!!!! There were ' + TRANSFORM(.nValidationErrors) + ' data validation errors; please examine the logfile for details.', LOGIT+SENDIT)
			ENDIF
			
			IF .lDotComProcessing THEN
				* PUT DOT-COM summary at top of body text
				IF EMPTY(.cDotComSummary) THEN
					.cDotComSummary = SEPARATOR_LINE + CRLF + "DOT-COM SUMMARY: no transactions processed." + ;
						CRLF + SEPARATOR_LINE + CRLF + CRLF
				ELSE
					.cDotComSummary = SEPARATOR_LINE + CRLF + "DOT-COM SUMMARY:" + CRLF + .cDotComSummary + ;
						SEPARATOR_LINE + CRLF + CRLF
				ENDIF
				
				.cBodyText = .cDotComSummary + .cBodyText
			ENDIF

			* if the flag to create invoice is TRUE, then the consolidated invoice was already attached added in PrintInvoiceToPDF() 4/17/07 MB
			* otherwise, attach it here 4/23/07 MB
			* add consolidated PDF file to attachments
			IF NOT .lGenerateInvoice THEN
				IF NOT .lAutoMode THEN
					IF NOT .lSummaryOnly THEN
						.AddAttachment(.cBigPDFFile)
					ENDIF
				ENDIF
			ENDIF

			.TrackProgress('The logfile is: ' + .cLogFile, SENDIT)
			.TrackProgress(.cClient + ' Make Bill Process started: ' + .cStartTime, LOGIT+SENDIT)
			.TrackProgress(.cClient + ' Make Bill Process finished: ' + TTOC(DATETIME()), LOGIT+SENDIT)
			
			* customize standard text etc., if running summary only report
			IF .lSummaryOnly THEN
				.cSubject = .cClient + ' handling summary for period ' + DTOC(.dBeginningTransdate) + " - " + DTOC(.dEndingTransdate)
				
				*.cBodyText = "See attached handling summary. Note: numbers after last Thursday may be inaccurate due to the data not having been fully loaded yet."
				
				
				
				.cBodyText = "See attached handling summary." + CRLF + CRLF + ;
				"PROJECT = MLAUTOBILLMTDRPT" + CRLF + CRLF + ;
				"SERVER = " + .cCOMPUTERNAME + CRLF + CRLF + ;
				"USER = " + .cUSERNAME

				.cSendTo = .cSummaryOnlySendTo
				.cCC = .cSummaryOnlyCC
			ENDIF
			
			IF .lSendInternalEmailIsOn THEN
				*DO FORM F:\MAIL\FORMS\dartmail2 WITH .cSendTo,.cFrom,.cSubject,.cCC,.cAttach,.cBodyText,"A"
				DO FORM M:\DEV\FRM\dartmail2 WITH .cSendTo,.cFrom,.cSubject,.cCC,.cAttach,.cBodyText,"A"
				.TrackProgress('Sent PDF Attachment(s) email.',LOGIT)
			ELSE
				.TrackProgress('Did NOT send PDF Attachment(s) email: THIS.lSendInternalEmailIsOn = FALSE.',LOGIT)
			ENDIF
		ENDWITH
		RETURN llRetVal
	ENDFUNC   &&  EmailAttachments


	FUNCTION AddAttachment
		LPARAMETERS tcFilename
		WITH THIS
			LOCAL llRetVal
			llRetVal = .T.
			* add to attachments
			IF FILE(tcFilename) THEN
				IF EMPTY(.cAttach) THEN
					.cAttach = tcFilename
				ELSE
					.cAttach = .cAttach + ";" + tcFilename
				ENDIF
			ELSE
				.TrackProgress('ERROR - XL file [' + tcFilename + '] could not be added to list of attachments',LOGIT+WAITIT+SENDIT)
			ENDIF
		ENDWITH
		RETURN llRetVal
	ENDFUNC


	PROCEDURE ClearAttachments
		WITH THIS
			.cAttach = ''
		ENDWITH
	ENDPROC


	PROCEDURE SetExtraWhere
		* sets extra where clause for main select for test purposes
		LPARAMETERS tcExtraWhere
		THIS.cExtraWhere = tcExtraWhere
	ENDPROC


	PROCEDURE TrackProgress
		* do any combination of Wait Window, writing to logfile, and adding to body of email,
		* based on nFlags parameter.
		LPARAMETERS tcExpression, tnFlags
		LOCAL loEdtBox
		SET CONSOLE OFF
		SET TALK OFF
		WITH THIS
			loEdtBox = .oEdtBox
			IF BITAND(tnFlags,LOGIT) = LOGIT THEN
				IF .lLoggingIsOn THEN
					?
					*? .cProcessName + "  " + TTOC(DATETIME()) + ": " + tcExpression
					? TTOC(DATETIME()) + ": " + tcExpression
				ENDIF
			ENDIF
			IF .lWaitWindowIsOn THEN
				IF BITAND(tnFlags,NOWAITIT) = NOWAITIT THEN
					WAIT WINDOW tcExpression NOWAIT
				ENDIF
				IF BITAND(tnFlags,WAITIT) = WAITIT THEN
					WAIT WINDOW tcExpression TIMEOUT .nWaitWindowTimeout
				ENDIF
			ENDIF
			IF BITAND(tnFlags,SENDIT) = SENDIT THEN
				IF .lSendInternalEmailIsOn THEN
					.cBodyText = .cBodyText + tcExpression + CRLF + CRLF
				ENDIF
			ENDIF
			IF TYPE('loEdtBox') = "O" AND NOT ISNULL(loEdtBox) THEN
				* append message to passed edtbox reference
				loEdtBox.VALUE = TTOC(DATETIME()) + ": " + tcExpression + CRLF + loEdtBox.VALUE
				loEdtBox.REFRESH()
			ENDIF
		ENDWITH
	ENDPROC  &&  TrackProgress


	PROCEDURE UseLocalData
		WITH THIS
			.lLocalData = .T.
			.cTransTable   = 'C:\SEARS\PMDATA\FTPMDATA'
			.cHistoryTable = 'C:\SEARS\PMDATA\FTHISTORY'
			.cRCRCTable    = 'C:\SEARS\PMDATA\RCRCHIST'
			.cRCRZTable    = 'C:\SEARS\PMDATA\RCRZHIST'
			.cRDCATable    = 'C:\SEARS\PMDATA\RDCAHIST'
			.cRATOTable    = 'C:\SEARS\PMDATA\RATOHIST'
			.cRATCTable    = 'C:\SEARS\PMDATA\RATCHIST'
			.cManualUnitsTable = 'C:\SEARS\PMDATA\MANUNITS'
			.cRCNTRANSFERTable = 'C:\SEARS\PMDATA\TRANSFERS'
			.cRETURNSTable = 'C:\SEARS\PMDATA\RETURNS'
			.cIAADXFERTable = 'C:\SEARS\PMDATA\IAADXFER'
			.ShowDataFiles()
			.TrackProgress('**** Using Local Data ****', LOGIT+SENDIT)
		ENDWITH
		RETURN
	ENDPROC


	PROCEDURE UseProductionData
		WITH THIS
			.lLocalData = .F.
			.cTransTable   = 'F:\SEARS\PMDATA\FTPMDATA'
			.cHistoryTable = 'F:\SEARS\PMDATA\FTHISTORY'
			.cRCRCTable    = 'F:\SEARS\PMDATA\RCRCHIST'
			.cRCRZTable    = 'F:\SEARS\PMDATA\RCRZHIST'
			.cRDCATable    = 'F:\SEARS\PMDATA\RDCAHIST'
			.cRATOTable    = 'F:\SEARS\PMDATA\RATOHIST'
			.cRATCTable    = 'F:\SEARS\PMDATA\RATCHIST'
			.cManualUnitsTable = 'F:\SEARS\PMDATA\MANUNITS'
			.ShowDataFiles()
			.TrackProgress('**** Using Production Data ****', LOGIT+SENDIT)
		ENDWITH
		RETURN
	ENDPROC


	PROCEDURE PackLocalData
		WITH THIS
			.UseLocalData()
			USE (.cTransTable) EXCLUSIVE
			ZAP
			USE (.cHistoryTable) EXCLUSIVE
			ZAP
			USE (.cRCRCTable) EXCLUSIVE
			ZAP
			USE (.cRCRZTable) EXCLUSIVE
			ZAP
			USE (.cRDCATable) EXCLUSIVE
			ZAP
			USE (.cRATOTable) EXCLUSIVE
			ZAP
			USE (.cRATCTable) EXCLUSIVE
			ZAP
*!*				USE (.cSTRGTable) EXCLUSIVE
*!*				ZAP
			USE
			.TrackProgress('**** PACKED Local Data ****', LOGIT+SENDIT)
		ENDWITH
		RETURN
	ENDPROC


	PROCEDURE PackProductionData
		
		RETURN
		
		WITH THIS
			.UseProductionData()
			USE (.cTransTable) EXCLUSIVE
			ZAP
			USE (.cHistoryTable) EXCLUSIVE
			ZAP
			USE (.cRCRCTable) EXCLUSIVE
			ZAP
			USE (.cRCRZTable) EXCLUSIVE
			ZAP
			USE (.cRDCATable) EXCLUSIVE
			ZAP
			USE (.cRATOTable) EXCLUSIVE
			ZAP
			USE (.cRATCTable) EXCLUSIVE
			ZAP
*!*				USE (.cSTRGTable) EXCLUSIVE
*!*				ZAP
			USE
			.TrackProgress('**** PACKED Production Data ****', LOGIT+SENDIT)
		ENDWITH
		RETURN
	ENDPROC


	PROCEDURE ShowDataFiles
		WITH THIS
			.TrackProgress(".cClient   = " + .cClient, LOGIT+SENDIT)
			.TrackProgress(".cTransTable   = " + .cTransTable, LOGIT+SENDIT)
			.TrackProgress(".cHistoryTable = " + .cHistoryTable, LOGIT+SENDIT)
			.TrackProgress(".cRCRCTable    = " + .cRCRCTable, LOGIT+SENDIT)
			.TrackProgress(".cRCRZTable    = " + .cRCRZTable, LOGIT+SENDIT)
			.TrackProgress(".cRDCATable    = " + .cRDCATable, LOGIT+SENDIT)
			.TrackProgress(".cRATOTable    = " + .cRATOTable, LOGIT+SENDIT)
			.TrackProgress(".cRATCTable    = " + .cRATCTable, LOGIT+SENDIT)
			.TrackProgress(".cManualUnitsTable = " + .cManualUnitsTable, LOGIT+SENDIT)
			.TrackProgress(".cRCNTRANSFERTable = " + .cRCNTRANSFERTable, LOGIT+SENDIT)
			.TrackProgress(".cRETURNSTable = " + .cRETURNSTable, LOGIT+SENDIT)
			.TrackProgress(".cIAADXFERTable = " + .cIAADXFERTable, LOGIT+SENDIT)
		ENDWITH
		RETURN
	ENDPROC


	PROCEDURE SetEditBoxReference
		LPARAMETERS toEdtBox
		THIS.oEdtBox = toEdtBox
	ENDPROC


	PROCEDURE SortArrayByDateTime
		* expects taInArray[] to be the result of an ADIR()
		LPARAMETERS taInArray, taOutArray
		EXTERNAL ARRAY taInArray, taOutArray
		LOCAL lnRows, lnCols, i, j
		* extending taInArray[] to process the files in date/time order
		lnRows = ALEN(taInArray,1)
		lnCols = ALEN(taInArray,2)
		* create taOutArray with one more column
		DIMENSION taOutArray[lnRows,lnCols + 1]
		* and fill it with taInArray columns AND (filedate + filetime) in new column
		* ( ACOPY() doesn't preserve elements properly because of different # of columns )
		FOR i = 1 TO lnRows
			FOR j = 1 TO lnCols
				taOutArray[i,j] = taInArray[i,j]
			ENDFOR
			taOutArray[i,lnCols + 1] = DTOS(taInArray[i,3]) + "_" + taInArray[i,4]
		ENDFOR
		* sort taOutArray on date/time column
		=ASORT(taOutArray,6)
	ENDPROC
	

	PROCEDURE SetSummaryOnly
		THIS.lSummaryOnly = .T.
	ENDPROC


	FUNCTION VALIDATE
		LOCAL llRetVal
		WITH THIS
			llRetVal = .T.
			*llRetVal = llRetVal AND .SOMEOTHERVALIDATION()
		ENDWITH
		RETURN llRetVal
	ENDFUNC


	PROCEDURE UseAugust2006Format
		WITH THIS
			.lUseAugust2006Format = .T.
		ENDWITH
		RETURN
	ENDPROC


	FUNCTION TestOpenAcrobat
		WITH THIS
			LOCAL lcErrorText, llRetVal
			IF .OpenPDFEngine() THEN
				=MESSAGEBOX("Acrobat opened okay.",0+16,"Acrobat Ok!")
				llRetVal = .T.
			ELSE
				* error: we cannot procede without 2 acrobat object refs!
				lcErrorText = ""
				IF NOT TYPE('.oAcrobat') = 'O' THEN
					lcErrorText = lcErrorText + "oAcrobat is not an object!" + CRLF
				ENDIF
				IF ISNULL(.oAcrobat) THEN
					lcErrorText = lcErrorText + "oAcrobat is NULL!" + CRLF
				ENDIF
				IF NOT TYPE('.oAcrobat2') = 'O' THEN
					lcErrorText = lcErrorText + "oAcrobat2 is not an object!" + CRLF
				ENDIF
				IF ISNULL(.oAcrobat2) THEN
					lcErrorText = lcErrorText + "oAcrobat2 is NULL!" + CRLF
				ENDIF
				.TrackProgress(lcErrorText, LOGIT+SENDIT)
				=MESSAGEBOX(lcErrorText,0+16,"Acrobat error!")
				llRetVal = .F.
			ENDIF
			.oAcrobat = NULL
			.oAcrobat2 = NULL
			RETURN llRetVal
		ENDWITH
	ENDFUNC


	FUNCTION OpenPDFEngine
		* must call this before calling MergePDFFiles() standalone
		LOCAL llRetVal, lcErrorText
		WITH THIS
			llRetVal = .T.
			IF .lUseAmyMerge THEN
				TRY
					.oAmyDoc = CREATEOBJECT("cdintfex.document")
					.TrackProgress("CREATEOBJECT = cdintfex.document", LOGIT+SENDIT)
				CATCH
					TRY
						.oAmyDoc = CREATEOBJECT("cdintf.document")
						.TrackProgress("CREATEOBJECT = cdintf.document", LOGIT+SENDIT)
					CATCH
						llRetVal = .F.
					ENDTRY
				ENDTRY
				lcErrorText = ""
				IF NOT TYPE('.oAmyDoc') = 'O' THEN
					lcErrorText = lcErrorText + "oAmyDoc is not an object!" + CRLF
				ENDIF
				IF ISNULL(.oAmyDoc) THEN
					lcErrorText = lcErrorText + "oAmyDoc is NULL!" + CRLF
				ENDIF
				.TrackProgress(lcErrorText, LOGIT+SENDIT)
			ELSE
				TRY
					.oAcrobat = CREATEOBJECT("AcroExch.PDDoc")
					.oAcrobat2 = CREATEOBJECT("AcroExch.PDDoc")
				CATCH
					llRetVal = .F.
				ENDTRY
				* error: we cannot proceed without 2 acrobat object refs!
				lcErrorText = ""
				IF NOT TYPE('.oAcrobat') = 'O' THEN
					lcErrorText = lcErrorText + "oAcrobat is not an object!" + CRLF
				ENDIF
				IF ISNULL(.oAcrobat) THEN
					lcErrorText = lcErrorText + "oAcrobat is NULL!" + CRLF
				ENDIF
				IF NOT TYPE('.oAcrobat2') = 'O' THEN
					lcErrorText = lcErrorText + "oAcrobat2 is not an object!" + CRLF
				ENDIF
				IF ISNULL(.oAcrobat2) THEN
					lcErrorText = lcErrorText + "oAcrobat2 is NULL!" + CRLF
				ENDIF
				.TrackProgress(lcErrorText, LOGIT+SENDIT)
			ENDIF
		ENDWITH
		RETURN llRetVal
	ENDFUNC


	FUNCTION AmyuniMerge
		LPARAMETER tcFile1, tcFile2, tcFile3
		LOCAL llRetVal
		llRetVal = .T.
		
		*!*	IF TYPE(".oAmyDoc")="U"
		*!*	TRY
		*!*		.oAmyDoc = CREATEOBJECT("cdintfex.document")
		*!*	CATCH
		*!*		.oAmyDoc = CREATEOBJECT("cdintf.document")
		*!*	ENDTRY
		*!*	ENDIF

		*!*	TRY
		*!*	.oAmyDoc.OPEN(tcFile1)
		*!*	.oAmyDoc.APPEND(tcFile2)
		*!*	.oAmyDoc.SAVE(tcFile3)
		*!*	CATCH
		*!*	COPY FILE (tcFile1) TO (tcFile3)
		*!*	ENDTRY

		.oAmyDoc.OPEN(tcFile1)
		.oAmyDoc.APPEND(tcFile2)
		.oAmyDoc.SAVE(tcFile3)
		
		RETURN llRetVal
	ENDFUNC


	FUNCTION MergePDFFiles
		LPARAMETERS tcPDFFile
		* we're always merging the passed file into the consolidated pdf file
		LOCAL llRetVal, loAcrobat, loAcrobat2, lcBigPDFFile, lnLastPage, lnInsertPages
		llRetVal = .T.
		WITH THIS
		
			lcBigPDFFile = .cBigPDFFile
			
			IF FILE(tcPDFFile) THEN
				* if the consolidated pdf does not exist, simply copy the passed pdf file to make it
				IF NOT FILE(lcBigPDFFile) THEN
					COPY FILE (tcPDFFile) TO (lcBigPDFFile)
				ELSE
					* consolidated pdf exists - we'll need to merge into it
					IF .lUseAmyMerge THEN
						.AmyuniMerge( lcBigPDFFile, tcPDFFile, lcBigPDFFile )
					ELSE
						loAcrobat = .oAcrobat
						loAcrobat2 = .oAcrobat2

						IF TYPE("loAcrobat") = "O" AND TYPE("loAcrobat2") = "O" AND NOT ISNULL(loAcrobat) AND NOT ISNULL(loAcrobat2) THEN

							* open consolidated pdf file to merge into
							loAcrobat.OPEN(lcBigPDFFile)
							lnLastPage = loAcrobat.GetNumPages - 1  && subtract 1 because Acrobat page-count is 0-based

							* open pdf file to merge from
							loAcrobat2.OPEN(tcPDFFile)
							lnInsertPages = loAcrobat2.GetNumPages

							* insert into consolidated pdf file
							loAcrobat.InsertPages(lnLastPage, loAcrobat2, 0, lnInsertPages, .T.)

							* close source file
							loAcrobat2.CLOSE

							* save target file (overwriting itself)
							loAcrobat.SAVE(1, lcBigPDFFile )

							* close target file
							loAcrobat.CLOSE

							.TrackProgress('Merged ' + tcPDFFile + ' into ' + lcBigPDFFile + ".", LOGIT)

						ELSE

							.TrackProgress('ERROR: Null Acrobat Object References in MergePDFFiles().', LOGIT)
							llRetVal = .F.

						ENDIF
						
					ENDIF && .lUseAmyMerge
						
				ENDIF  &&  NOT FILE(lcBigPDFFile)

			ELSE
				* we can't find the passed pdf filename
				.TrackProgress('ERROR: File ' + tcPDFFile + ' not found in MergePDFFiles().', LOGIT+SENDIT)
				llRetVal = .F.
				
				* Make this a fatal error which stops us from proceeding to next day. MB 09/18/2014
				.lFatalError = .T.
				THROW

			ENDIF && FILE(tcPDFFile)
		ENDWITH
		RETURN llRetVal
	ENDFUNC  && MergePDFFiles


	FUNCTION MergeInvoicePDFFile
		LPARAMETERS tcPDFFile
		* this will do the opposite of MergePDFFiles():
		* it will merge the consolidated pdf file into the passed file (which should be the invoice pdf),
		* in effect making the invoice the first, not last, page.
		* This should only be called with the Invoice file as the passed parameter!!!
		LOCAL llRetVal, loAcrobat, loAcrobat2, lcBigPDFFile, lnLastPage, lnInsertPages
		llRetVal = .T.
		WITH THIS
			lcBigPDFFile = .cBigPDFFile

			IF FILE(tcPDFFile) THEN
				IF NOT FILE(lcBigPDFFile) THEN
					* error: consolidated pdf file not found
					.TrackProgress('ERROR: File ' + lcBigPDFFile + ' not found in MergeInvoicePDFFile().', LOGIT+SENDIT)
					llRetVal = .F.
				ELSE
					* consolidated pdf exists - continue

					IF .lUseAmyMerge THEN
						.AmyuniMerge( tcPDFFile, lcBigPDFFile, tcPDFFile )
					ELSE

						loAcrobat = .oAcrobat
						loAcrobat2 = .oAcrobat2

						IF TYPE("loAcrobat") = "O" AND TYPE("loAcrobat2") = "O" AND NOT ISNULL(loAcrobat) AND NOT ISNULL(loAcrobat2) THEN

							* open invoice pdf file to merge into
							loAcrobat.OPEN(tcPDFFile)
							lnLastPage = loAcrobat.GetNumPages - 1  && subtract 1 because Acrobat page-count is 0-based

							* open consolidated pdf file to merge from
							loAcrobat2.OPEN(lcBigPDFFile)
							lnInsertPages = loAcrobat2.GetNumPages

							* insert into invoice pdf file
							loAcrobat.InsertPages(lnLastPage, loAcrobat2, 0, lnInsertPages, .T.)

							* close source file
							loAcrobat2.CLOSE

							* save target file (overwriting itself)
							loAcrobat.SAVE(1, tcPDFFile )

							* close target file
							loAcrobat.CLOSE

							.TrackProgress('Merged ' + lcBigPDFFile + ' into ' + tcPDFFile + ".", LOGIT)

						ELSE

							.TrackProgress('ERROR: Null Acrobat Object References in MergeInvoicePDFFile().', LOGIT)
							llRetVal = .F.

						ENDIF

					ENDIF && .lUseAmyMerge

				ENDIF  &&  NOT FILE(lcBigPDFFile)

			ELSE

				.TrackProgress('ERROR: File ' + tcPDFFile + ' not found in MergeInvoicePDFFile().', LOGIT+SENDIT)
				llRetVal = .F.

			ENDIF && FILE(tcPDFFile)
		ENDWITH
		RETURN llRetVal
	ENDFUNC  && MergeInvoicePDFFile


*!*		PROCEDURE AppendPDFFile
*!*			LPARAMETERS tcFromFile, tcToFile
*!*			* append first passed pdf file onto the second passed pdf file
*!*			LOCAL llRetVal, loAcrobat, loAcrobat2, lcFromFile, lcToFile, lnLastPage, lnInsertPages
*!*			llRetVal = .T.
*!*			WITH THIS
*!*				lcToFile = tcToFile
*!*				lcFromFile = tcFromFile

*!*				IF NOT FILE(lcFromFile) THEN
*!*					=MESSAGEBOX('ERROR: File ' + lcFromFile + ' not found in AppendPDFFile().')
*!*					RETURN .F.
*!*				ENDIF

*!*				IF NOT FILE(lcToFile) THEN
*!*					=MESSAGEBOX('ERROR: File ' + lcToFile + ' not found in AppendPDFFile().')
*!*					RETURN .F.
*!*				ENDIF

*!*				.OpenPDFEngine()

*!*				loAcrobat = .oAcrobat
*!*				loAcrobat2 = .oAcrobat2

*!*				IF TYPE("loAcrobat") = "O" AND TYPE("loAcrobat2") = "O" AND NOT ISNULL(loAcrobat) AND NOT ISNULL(loAcrobat2) THEN

*!*					* open pdf file to append onto
*!*					loAcrobat.OPEN(lcToFile)
*!*					lnLastPage = loAcrobat.GetNumPages - 1

*!*					* open pdf file to append onto the other file
*!*					loAcrobat2.OPEN(lcFromFile)
*!*					lnInsertPages = loAcrobat2.GetNumPages

*!*					* insert into target pdf file
*!*					loAcrobat.InsertPages(lnLastPage, loAcrobat2, 0, lnInsertPages, .T.)

*!*					* close source file
*!*					loAcrobat2.CLOSE

*!*					* save target file (overwriting itself)
*!*					loAcrobat.SAVE(1, lcToFile)

*!*					* close target file
*!*					loAcrobat.CLOSE

*!*					=MESSAGEBOX('Appended ' + lcFromFile + ' into ' + lcToFile + ".")

*!*				ELSE

*!*					=MESSAGEBOX('ERROR: Null Acrobat Object References in AppendPDFFile().')

*!*				ENDIF

*!*				loAcrobat = NULL
*!*				loAcrobat2 = NULL
*!*				.oAcrobat = NULL
*!*				.oAcrobat2 = NULL

*!*			ENDWITH
*!*		ENDPROC
	

	PROCEDURE SearsInvoiceSummary
		* MAKE SEARS MONTHLY SUMMARY BY INVOICE DATE FOR GREG
		* JUST FOR LATEST INVOICE (THE ONE JUST RUN)
		WITH THIS
			*IF NOT .lTestInvoice THEN
				LOCAL ldLatestTrans, ldFromDate, ldToDate, lcExcelFile
				USE (.cHistoryTable) AGAIN IN 0 ALIAS HISTORYTABLE
				* GET LATEST TRANSDATE FOR SEARS
				IF USED('CURLATESTTRANS') THEN
					USE IN CURLATESTTRANS
				ENDIF
				SELECT MAX(TRANSDATE) AS TRANSDATE ;
					FROM HISTORYTABLE ;
					INTO CURSOR CURLATESTTRANS ;
					WHERE TNODE = "KM" ;
					AND NOT LTEST
				SELECT CURLATESTTRANS
				LOCATE
				ldLatestTrans = CURLATESTTRANS.TRANSDATE
				IF INLIST(DAY(ldLatestTrans),28,29,30,31) THEN
					* the invoice was the last one of the month, continue
					* GET TRANS DATE RANGE					
					ldToDate = ldLatestTrans
					ldFromDate = ldLatestTrans - DAY(ldLatestTrans) + 1  && i.e., the first of the month invoiced
					
					* Get summary info for the range of trans dates
					IF USED('CURGREG') THEN
						USE IN CURGREG
					ENDIF
					SELECT ;
						ldFromDate AS FROMDATE, ;
						ldToDate AS TODATE, ;
						SUM(RXDOCKCTNS) AS INXDCTNS, ;
						SUM( RXDOCKCTNS * RXDOCKRATE ) AS INXDCHG, ;
						SUM(RNXDCTNS) AS INCTNS, ;
						SUM( RNXDCTNS * RNXDRATE ) AS INCHG, ;
						SUM(SXDOCKCTNS) AS OUTXDCTNS, ;
						SUM( SXDOCKCTNS * SXDOCKRATE ) AS OUTXDCHG, ;
						SUM(TILTUNITS) AS TILTUNITS, ;
						SUM( TILTUNITS * TILTRATE ) AS TILTCHG, ;
						SUM(FCASECTNS) AS FCASECTNS, ;
						SUM( FCASECTNS * FCASERATE ) AS FCASECHG, ;
						SUM(TOTALCHG) AS TOTALCHG ;
						FROM F:\SEARS\PMDATA\FTHISTORY ;
						INTO CURSOR CURGREG ;
						WHERE TNODE = "KM" ;
						AND BETWEEN(TRANSDATE,ldFromDate,ldToDate) ;
						AND NOT LTEST
						
					lcExcelFile = "C:\TEMPFOX\SEARS_INVOICE_SUMMARY.XLS"
					SELECT CURGREG
					*BROWSE
					COPY TO (lcExcelFile) XL5
					* send Mark the attachment via email
					.cSubject = "Sears Invoice Summary"
					.cBodyText = "Send to Greg DeSaye."
					.cSendTo = 'Mark Bennett <mark.bennett@tollgroup.com>'
					.cCC = ''
					.cAttach = lcExcelFile
					DO FORM M:\DEV\FRM\dartmail2 WITH .cSendTo,.cFrom,.cSubject,.cCC,.cAttach,.cBodyText,"A"
				ENDIF && INLIST(DAY(ldLatestTrans),28,29,30,31) THEN
			*ENDIF && NOT .LTESTINVOICE

			IF USED('CURGREG') THEN
				USE IN CURGREG
			ENDIF
			IF USED('CURLATESTTRANS') THEN
				USE IN CURLATESTTRANS
			ENDIF
			IF USED('HISTORYTABLE') THEN
				USE IN HISTORYTABLE
			ENDIF
		ENDWITH
		RETURN
	ENDPROC


	PROCEDURE SetInvoiceDate
		LPARAMETERS tdInvoiceDate
		THIS.dInvoiceDate = tdInvoiceDate
		THIS.TrackProgress('Invoice Date = ' + TRANSFORM(THIS.dInvoiceDate), LOGIT+SENDIT)
		*=MESSAGEBOX("Invoice Date set to " + TRANSFORM(THIS.dInvoiceDate))
		RETURN
	ENDPROC


	PROCEDURE SetRates
		LPARAMETERS tdTransactionDate
		WITH THIS
			DO CASE
				CASE INLIST(.cClient, "OFF BROADWAY CHAS", "OFF BROADWAY FOOTWEAR", "OFF BROADWAY ALL", "RACK ROOM CHAS", "RACK ROOM FOOTWEAR", "RACK ROOM ALL")				
					* NOTE
					* pre-pack, handling out = full case
					* receiving (solids or pre-pack) in UNITS is a new transaction which applies to non-Xock receipts			
					DO CASE
						CASE BETWEEN(tdTransactionDate,{^2014-01-01},{^2014-12-31})
						
							.nReceiptCrossDockPerCtnRate = 0.650
							.nShippedCrossDockPerCtnRate = 0.000   
							*.nShippedCrossDockPerCtnRate = 0.650   &&  Mike DeSaye says its .65 total, so I left that all on the receipt side.
							.nReceiptNonXDockPerCtnRate  = 0.600
							.nTiltPerUnitRate            = 0.470    && per Mike DeSaye 10/16/2014
							.nFullCasePerCtnRate         = 0.750
							.nReceiptCrossDockCAPEXPerCtnRate = 0.000
							.nReceiptNonXDockCAPEXPerCtnRate  = 0.000
							.nShippedCrossDockCAPEXPerCtnRate = 0.000
							*.nTiltResidualsPerUnitRate = 0.000
							.nTiltResidualsPerCartonRate = 0.870   && per Mike DeSaye 9/17/2014
							.nManualPerUnitRate = 0.000
							.nReturnReceiptPerCtnRate    = .nReceiptNonXDockPerCtnRate
							.nReceiptNonXDockPerUNITRate  = 0.080							
							
							.TrackProgress("**** Using 2014-01-01 " + .cClient + " rates ****", LOGIT+SENDIT)
							
						CASE BETWEEN(tdTransactionDate,{^2015-01-01},{^2015-12-31})
						
							.nReceiptCrossDockPerCtnRate = 0.650
							.nShippedCrossDockPerCtnRate = 0.000   
							*.nShippedCrossDockPerCtnRate = 0.650   &&  Mike DeSaye says its .65 total, so I left that all on the receipt side.
							.nReceiptNonXDockPerCtnRate  = 0.600
							.nTiltPerUnitRate            = 0.470    && per Mike DeSaye 10/16/2014
							.nFullCasePerCtnRate         = 0.750
							.nReceiptCrossDockCAPEXPerCtnRate = 0.000
							.nReceiptNonXDockCAPEXPerCtnRate  = 0.000
							.nShippedCrossDockCAPEXPerCtnRate = 0.000
							*.nTiltResidualsPerUnitRate = 0.000
							.nTiltResidualsPerCartonRate = 0.870   && per Mike DeSaye 9/17/2014
							.nManualPerUnitRate = 0.000
							.nReturnReceiptPerCtnRate    = .nReceiptNonXDockPerCtnRate
							.nReceiptNonXDockPerUNITRate  = 0.080						
							
							.TrackProgress("**** Using 2015-01-01 " + .cClient + " rates ****", LOGIT+SENDIT)
							
						CASE BETWEEN(tdTransactionDate,{^2016-01-01},{^2017-03-31})
						
							.nReceiptCrossDockPerCtnRate = 0.660	&& crossdock
							.nShippedCrossDockPerCtnRate = 0.000   	&& crossdock is being billed all on the receipt side
							.nReceiptNonXDockPerCtnRate  = 0.609	&& using this rate for Returns, see below
							.nTiltPerUnitRate            = 0.477 	&& pair picking charge, pick & ship   
							.nFullCasePerCtnRate         = 0.761  	&& Pre-pack, handling out
							.nReceiptCrossDockCAPEXPerCtnRate = 0.000
							.nReceiptNonXDockCAPEXPerCtnRate  = 0.000
							.nShippedCrossDockCAPEXPerCtnRate = 0.000
							.nTiltResidualsPerCartonRate = 0.883   && Residuals
							.nManualPerUnitRate = 0.000
							.nReturnReceiptPerCtnRate    = .nReceiptNonXDockPerCtnRate  && Returns, Receive
							.nReceiptNonXDockPerUNITRate  = 0.081  && Receiving (solids or pre pack)						

							.TrackProgress("**** tdTransactionDate = " + TRANSFORM(tdTransactionDate), LOGIT+SENDIT)
							
							.TrackProgress("**** Using 2016-01-01 " + .cClient + " rates ****", LOGIT+SENDIT)
							
						CASE BETWEEN(tdTransactionDate,{^2017-04-01},{^2018-03-31})
						
							.nReceiptCrossDockPerCtnRate = 0.676	&& crossdock
							.nShippedCrossDockPerCtnRate = 0.000   	&& crossdock is being billed all on the receipt side
							.nReceiptNonXDockPerCtnRate  = 0.624	&& using this rate for Returns, see below
							.nTiltPerUnitRate            = 0.489 	&& pair picking charge, pick & ship   
							.nFullCasePerCtnRate         = 0.780  	&& Pre-pack, handling out
							.nReceiptCrossDockCAPEXPerCtnRate = 0.000
							.nReceiptNonXDockCAPEXPerCtnRate  = 0.000
							.nShippedCrossDockCAPEXPerCtnRate = 0.000
							.nTiltResidualsPerCartonRate = 0.91   && Residuals
							.nManualPerUnitRate = 0.000
							.nReturnReceiptPerCtnRate    = .nReceiptNonXDockPerCtnRate  && Returns, Receive
							.nReceiptNonXDockPerUNITRate  = 0.083  && Receiving (solids or pre pack)						
							
							.TrackProgress("**** Using 2017-04-01 " + .cClient + " rates ****", LOGIT+SENDIT)
							
						CASE BETWEEN(tdTransactionDate,{^2018-04-01},{^2019-03-31})  && CASE added 3/13/2018 per RF392374 from Brian Freiberger.
						
							.nReceiptCrossDockPerCtnRate = 0.693	&& crossdock
							.nShippedCrossDockPerCtnRate = 0.000   	&& crossdock is being billed all on the receipt side
							.nReceiptNonXDockPerCtnRate  = 0.640	&& using this rate for Returns, see below
							.nTiltPerUnitRate            = 0.501 	&& pair picking charge, pick & ship   
							.nFullCasePerCtnRate         = 0.800  	&& Pre-pack, handling out
							.nReceiptCrossDockCAPEXPerCtnRate = 0.000
							.nReceiptNonXDockCAPEXPerCtnRate  = 0.000
							.nShippedCrossDockCAPEXPerCtnRate = 0.000
							.nTiltResidualsPerCartonRate = 0.928   && Residuals
							.nManualPerUnitRate = 0.000
							.nReturnReceiptPerCtnRate    = .nReceiptNonXDockPerCtnRate  && Returns, Receive
							.nReceiptNonXDockPerUNITRate  = 0.085  && Receiving (solids or pre pack)						
							
							.TrackProgress("**** Using 2018-04-01 " + .cClient + " rates ****", LOGIT+SENDIT)
							
						CASE BETWEEN(tdTransactionDate,{^2019-04-01},{^2020-03-31})  && CASE added 3/13/2018 per RF392374 from Brian Freiberger.
						
							.nReceiptCrossDockPerCtnRate = 0.710	&& crossdock
							.nShippedCrossDockPerCtnRate = 0.000   	&& crossdock is being billed all on the receipt side
							.nReceiptNonXDockPerCtnRate  = 0.656	&& using this rate for Returns, see below
							.nTiltPerUnitRate            = 0.514 	&& pair picking charge, pick & ship   
							.nFullCasePerCtnRate         = 0.820  	&& Pre-pack, handling out
							.nReceiptCrossDockCAPEXPerCtnRate = 0.000
							.nReceiptNonXDockCAPEXPerCtnRate  = 0.000
							.nShippedCrossDockCAPEXPerCtnRate = 0.000
							.nTiltResidualsPerCartonRate = 0.951   && Residuals
							.nManualPerUnitRate = 0.000
							.nReturnReceiptPerCtnRate    = .nReceiptNonXDockPerCtnRate  && Returns, Receive
							.nReceiptNonXDockPerUNITRate  = 0.087  && Receiving (solids or pre pack)						
							
							.TrackProgress("**** Using 2019-04-01 " + .cClient + " rates ****", LOGIT+SENDIT)
							
						OTHERWISE  &&  tdTransactionDate = {???}
							.TrackProgress("**** ERROR: unexpected transaction date for ' + .cClient + ' rates = " + TRANSFORM(tdTransactionDate), LOGIT+SENDIT)
					ENDCASE
					
				CASE .cClient = "DSG"
					DO CASE
						CASE BETWEEN(tdTransactionDate,{^2008-12-01},{^2011-07-31})
							.nReceiptCrossDockPerCtnRate = 0.230
							.nReceiptCrossDockCAPEXPerCtnRate = 0.000
							.nReceiptNonXDockPerCtnRate  = 0.560
							.nReceiptNonXDockCAPEXPerCtnRate  = 0.000
							.nShippedCrossDockPerCtnRate = 0.230
							.nShippedCrossDockCAPEXPerCtnRate = 0.000
							.nTiltPerUnitRate            = 0.000
							.nTiltResidualsPerUnitRate = 0.000
							.nFullCasePerCtnRate         = 1.060
							.TrackProgress("**** Using 2008-12-01 DSG rates ****", LOGIT+SENDIT)
						CASE BETWEEN(tdTransactionDate,{^2011-08-01},{^2011-12-31})
							* zeroed out xdock rates per Jim DeVeau 8/2/11 MB
							.nReceiptCrossDockPerCtnRate = 0.000
							.nReceiptCrossDockCAPEXPerCtnRate = 0.000
							.nReceiptNonXDockPerCtnRate  = 0.570
							.nReceiptNonXDockCAPEXPerCtnRate  = 0.000
							.nShippedCrossDockPerCtnRate = 0.000
							.nShippedCrossDockCAPEXPerCtnRate = 0.000
							.nTiltPerUnitRate            = 0.000
							.nTiltResidualsPerUnitRate = 0.000
							.nFullCasePerCtnRate         = 1.070
							.TrackProgress("**** Using 2011-08-01 DSG rates ****", LOGIT+SENDIT)
						CASE BETWEEN(tdTransactionDate,{^2012-01-01},{^2012-12-31})
							.nReceiptCrossDockPerCtnRate = 0.000
							.nReceiptCrossDockCAPEXPerCtnRate = 0.000
							.nReceiptNonXDockPerCtnRate  = 0.590
							.nReceiptNonXDockCAPEXPerCtnRate  = 0.000
							.nShippedCrossDockPerCtnRate = 0.000
							.nShippedCrossDockCAPEXPerCtnRate = 0.000
							.nTiltPerUnitRate            = 0.000
							.nTiltResidualsPerUnitRate = 0.000
							.nFullCasePerCtnRate         = 1.110
							.TrackProgress("**** Using 2012-01-01 DSG rates ****", LOGIT+SENDIT)
							*!*	CASE BETWEEN(tdTransactionDate,{^???},{^???})
						CASE BETWEEN(tdTransactionDate,{^2013-01-01},{^2013-12-31})
							.nReceiptCrossDockPerCtnRate = 0.000
							.nReceiptCrossDockCAPEXPerCtnRate = 0.000
							.nReceiptNonXDockPerCtnRate  = 0.610
							.nReceiptNonXDockCAPEXPerCtnRate  = 0.000
							.nShippedCrossDockPerCtnRate = 0.000
							.nShippedCrossDockCAPEXPerCtnRate = 0.000
							.nTiltPerUnitRate            = 0.000
							.nTiltResidualsPerUnitRate = 0.000
							.nFullCasePerCtnRate         = 1.140
							.nReturnReceiptPerCtnRate    = .nReceiptNonXDockPerCtnRate
							.TrackProgress("**** Using 2013-01-01 DSG rates ****", LOGIT+SENDIT)
							*!*	CASE BETWEEN(tdTransactionDate,{^???},{^???})
						CASE BETWEEN(tdTransactionDate,{^2014-01-01},{^2099-12-31})
							.nReceiptCrossDockPerCtnRate = 0.000
							.nReceiptCrossDockCAPEXPerCtnRate = 0.000
							.nReceiptNonXDockPerCtnRate  = 0.620
							.nReceiptNonXDockCAPEXPerCtnRate  = 0.000
							.nShippedCrossDockPerCtnRate = 0.000
							.nShippedCrossDockCAPEXPerCtnRate = 0.000
							.nTiltPerUnitRate            = 0.000
							.nTiltResidualsPerUnitRate = 0.000
							.nFullCasePerCtnRate         = 1.170
							.nReturnReceiptPerCtnRate    = .nReceiptNonXDockPerCtnRate
							.TrackProgress("**** Using 2014-01-01 DSG rates ****", LOGIT+SENDIT)
							*!*	CASE BETWEEN(tdTransactionDate,{^???},{^???})
						OTHERWISE  &&  tdTransactionDate = {???}
							.TrackProgress("**** ERROR: UNEXPECTED TRANSACTION DATE FOR DSG RATES = " + TRANSFORM(tdTransactionDate), LOGIT+SENDIT)
					ENDCASE
				CASE .cClient = "FOOTLOCKER"
					DO CASE
						CASE BETWEEN(tdTransactionDate,{^2009-03-01},{^2010-06-14})
							.nReceiptCrossDockPerCtnRate = 0.000
							.nReceiptCrossDockCAPEXPerCtnRate = 0.000
							.nReceiptNonXDockPerCtnRate  = 0.000
							.nReceiptNonXDockCAPEXPerCtnRate  = 0.000
							.nShippedCrossDockPerCtnRate = 0.500
							.nShippedCrossDockCAPEXPerCtnRate = 0.000
							.nTiltPerUnitRate            = 0.000
							.nTiltResidualsPerUnitRate = 0.000
							.nFullCasePerCtnRate         = 0.000
							.TrackProgress("**** Using 2009-03-01 FOOTLOCKER rates ****", LOGIT+SENDIT)
						CASE BETWEEN(tdTransactionDate,{^2010-06-15},{^2099-12-31})
							.nReceiptCrossDockPerCtnRate = 0.000
							.nReceiptCrossDockCAPEXPerCtnRate = 0.000
							.nReceiptNonXDockPerCtnRate  = 0.000
							.nReceiptNonXDockCAPEXPerCtnRate  = 0.000
							.nShippedCrossDockPerCtnRate = 0.570
							.nShippedCrossDockCAPEXPerCtnRate = 0.000
							.nTiltPerUnitRate            = 0.000
							.nTiltResidualsPerUnitRate = 0.000
							.nFullCasePerCtnRate         = 0.000
							.nReturnReceiptPerCtnRate    = .nReceiptNonXDockPerCtnRate
							.TrackProgress("**** Using 2010-06-15 FOOTLOCKER rates ****", LOGIT+SENDIT)
						OTHERWISE  &&  tdTransactionDate = {???}
							.TrackProgress("**** ERROR: UNEXPECTED TRANSACTION DATE FOR FOOTLOCKER RATES = " + TRANSFORM(tdTransactionDate), LOGIT+SENDIT)
					ENDCASE
				CASE .cClient = "KMART ALL"
					DO CASE
						CASE BETWEEN(tdTransactionDate,{^2008-12-01},{^2009-12-31})
							* modified 10/14/09 to break out CAPEX rates from other receipt rates.
							*!*	.nReceiptCrossDockPerCtnRate = 0.300
							*!*	.nReceiptCrossDockCAPEXPerCtnRate = 0.000
							*!*	.nReceiptNonXDockPerCtnRate  = 0.790
							*!*	.nReceiptNonXDockCAPEXPerCtnRate  = 0.000
							.nReceiptCrossDockPerCtnRate = 0.240
							.nReceiptCrossDockCAPEXPerCtnRate = 0.060
							.nReceiptNonXDockPerCtnRate  = 0.670
							.nReceiptNonXDockCAPEXPerCtnRate  = 0.120
							.nShippedCrossDockPerCtnRate = 0.240
							.nShippedCrossDockCAPEXPerCtnRate = 0.060
							.nTiltPerUnitRate            = 0.390
							.nTiltResidualsPerUnitRate = 0.000
							.nFullCasePerCtnRate         = 1.350
							.TrackProgress("**** Using 2008-12-01 KMART ALL rates ****", LOGIT+SENDIT)
						CASE BETWEEN(tdTransactionDate,{^2010-01-01},{^2010-05-31})
							.nReceiptCrossDockPerCtnRate = 0.250
							.nReceiptCrossDockCAPEXPerCtnRate = 0.050
							.nReceiptNonXDockPerCtnRate  = 0.690
							.nReceiptNonXDockCAPEXPerCtnRate  = 0.100
							.nShippedCrossDockPerCtnRate = 0.250
							.nShippedCrossDockCAPEXPerCtnRate = 0.050
							.nTiltPerUnitRate            = 0.400
							.nTiltResidualsPerUnitRate = 0.250
							.nFullCasePerCtnRate         = 1.400
							.TrackProgress("**** Using 2010-01-01 KMART ALL rates ****", LOGIT+SENDIT)
						CASE BETWEEN(tdTransactionDate,{^2010-06-01},{^2010-12-31})
							.nReceiptCrossDockPerCtnRate = 0.270
							.nReceiptCrossDockCAPEXPerCtnRate = 0.000
							.nReceiptNonXDockPerCtnRate  = 0.600
							.nReceiptNonXDockCAPEXPerCtnRate  = 0.000
							.nShippedCrossDockPerCtnRate = 0.270
							.nShippedCrossDockCAPEXPerCtnRate = 0.000
							.nTiltPerUnitRate            = 0.400
							.nTiltResidualsPerUnitRate = 0.250
							.nFullCasePerCtnRate         = 1.150
							.TrackProgress("**** Using 2010-06-01 KMART ALL rates ****", LOGIT+SENDIT)
						CASE BETWEEN(tdTransactionDate,{^2011-01-01},{^2011-08-31})
							.nReceiptCrossDockPerCtnRate = 0.270
							.nReceiptCrossDockCAPEXPerCtnRate = 0.000
							.nReceiptNonXDockPerCtnRate  = 0.600
							.nReceiptNonXDockCAPEXPerCtnRate  = 0.000
							.nShippedCrossDockPerCtnRate = 0.270
							.nShippedCrossDockCAPEXPerCtnRate = 0.000
							.nTiltPerUnitRate            = 0.400
							.nTiltResidualsPerUnitRate = 0.000
							.nFullCasePerCtnRate         = 1.150
							.nManualPerUnitRate = 0.000
							.TrackProgress("**** Using 2011-01-01 KMART ALL rates ****", LOGIT+SENDIT)
						CASE BETWEEN(tdTransactionDate,{^2011-09-01},{^2011-09-30})
							.nReceiptCrossDockPerCtnRate = 0.270
							.nReceiptCrossDockCAPEXPerCtnRate = 0.000
							.nReceiptNonXDockPerCtnRate  = 0.710
							.nReceiptNonXDockCAPEXPerCtnRate  = 0.000
							.nShippedCrossDockPerCtnRate = 0.270
							.nShippedCrossDockCAPEXPerCtnRate = 0.000
							.nTiltPerUnitRate            = 0.400
							.nTiltResidualsPerUnitRate = 0.000
							.nFullCasePerCtnRate         = 1.450
							.nManualPerUnitRate = 0.000
							.TrackProgress("**** Using 2011-09-01 KMART ALL rates ****", LOGIT+SENDIT)
						CASE BETWEEN(tdTransactionDate,{^2011-10-01},{^2012-12-31})
							.nReceiptCrossDockPerCtnRate = 0.500  && = 0.270
							.nReceiptCrossDockCAPEXPerCtnRate = 0.000
							.nReceiptNonXDockPerCtnRate  = 0.850  && = 0.710
							.nReceiptNonXDockCAPEXPerCtnRate  = 0.000
							.nShippedCrossDockPerCtnRate = 0.500  && = 0.270
							.nShippedCrossDockCAPEXPerCtnRate = 0.000
							.nTiltPerUnitRate            = 0.410  && = 0.400
							.nTiltResidualsPerUnitRate = 0.000
							.nFullCasePerCtnRate         = 1.300  && = 1.450
							.nManualPerUnitRate = 0.000
							.TrackProgress("**** Using 2011-10-01 " + .cClient + " rates ****", LOGIT+SENDIT)
						CASE BETWEEN(tdTransactionDate,{^2013-01-01},{^2013-12-31})
							.nReceiptCrossDockPerCtnRate = 0.520
							.nShippedCrossDockPerCtnRate = 0.520
							.nReceiptNonXDockPerCtnRate  = 0.890
							.nTiltPerUnitRate            = 0.430
							.nFullCasePerCtnRate         = 1.360
							.nReceiptCrossDockCAPEXPerCtnRate = 0.000
							.nReceiptNonXDockCAPEXPerCtnRate  = 0.000
							.nShippedCrossDockCAPEXPerCtnRate = 0.000
							.nTiltResidualsPerUnitRate = 0.000
							.nManualPerUnitRate = 0.000
							.nReturnReceiptPerCtnRate    = .nReceiptNonXDockPerCtnRate
							.TrackProgress("**** Using 2013-01-01 " + .cClient + " rates ****", LOGIT+SENDIT)
						CASE BETWEEN(tdTransactionDate,{^2014-01-01},{^2014-12-31})
							.nReceiptCrossDockPerCtnRate = 0.550
							.nShippedCrossDockPerCtnRate = 0.550
							.nReceiptNonXDockPerCtnRate  = 0.930
							.nTiltPerUnitRate            = 0.450
							.nFullCasePerCtnRate         = 1.420
							.nReceiptCrossDockCAPEXPerCtnRate = 0.000
							.nReceiptNonXDockCAPEXPerCtnRate  = 0.000
							.nShippedCrossDockCAPEXPerCtnRate = 0.000
							.nTiltResidualsPerUnitRate = 0.000
							.nManualPerUnitRate = 0.000
							.nReturnReceiptPerCtnRate    = .nReceiptNonXDockPerCtnRate
							.TrackProgress("**** Using 2014-01-01 " + .cClient + " rates ****", LOGIT+SENDIT)
						CASE BETWEEN(tdTransactionDate,{^2015-01-01},{^2015-12-31})
							.nReceiptCrossDockPerCtnRate = 0.570
							.nShippedCrossDockPerCtnRate = 0.570
							.nReceiptNonXDockPerCtnRate  = 0.970
							.nTiltPerUnitRate            = 0.470
							.nFullCasePerCtnRate         = 1.480
							.nReceiptCrossDockCAPEXPerCtnRate = 0.000
							.nReceiptNonXDockCAPEXPerCtnRate  = 0.000
							.nShippedCrossDockCAPEXPerCtnRate = 0.000
							.nTiltResidualsPerUnitRate = 0.000
							.nManualPerUnitRate = 0.000
							.nReturnReceiptPerCtnRate    = .nReceiptNonXDockPerCtnRate
							.TrackProgress("**** Using 2015-01-01 " + .cClient + " rates ****", LOGIT+SENDIT)
						CASE BETWEEN(tdTransactionDate,{^2016-01-01},{^2017-12-31})
							.nReceiptCrossDockPerCtnRate = 0.590
							.nShippedCrossDockPerCtnRate = 0.590
							.nReceiptNonXDockPerCtnRate  = 1.000
							.nTiltPerUnitRate            = 0.490
							.nFullCasePerCtnRate         = 1.530
							.nReceiptCrossDockCAPEXPerCtnRate = 0.000
							.nReceiptNonXDockCAPEXPerCtnRate  = 0.000
							.nShippedCrossDockCAPEXPerCtnRate = 0.000
							.nTiltResidualsPerUnitRate = 0.000
							.nManualPerUnitRate = 0.000
							.nReturnReceiptPerCtnRate    = .nReceiptNonXDockPerCtnRate
							.TrackProgress("**** Using 2016-01-01 " + .cClient + " rates ****", LOGIT+SENDIT)
						OTHERWISE  &&  tdTransactionDate = {???}
							.TrackProgress("**** ERROR: UNEXPECTED TRANSACTION DATE FOR KMART ALL RATES = " + TRANSFORM(tdTransactionDate), LOGIT+SENDIT)
					ENDCASE
				*CASE .cClient = "KMART FOOTWEAR"
				CASE INLIST(.cClient,"KMART FOOTWEAR","KMART DIV042")
					DO CASE
						CASE BETWEEN(tdTransactionDate,{^2008-12-01},{^2009-12-31})
							* modified 10/14/09 to break out CAPEX rates from other receipt rates.
							*!*	.nReceiptCrossDockPerCtnRate = 0.300
							*!*	.nReceiptCrossDockCAPEXPerCtnRate = 0.000
							*!*	.nReceiptNonXDockPerCtnRate  = 0.790
							*!*	.nReceiptNonXDockCAPEXPerCtnRate  = 0.000
							.nReceiptCrossDockPerCtnRate = 0.240
							.nReceiptCrossDockCAPEXPerCtnRate = 0.060
							.nReceiptNonXDockPerCtnRate  = 0.670
							.nReceiptNonXDockCAPEXPerCtnRate  = 0.120
							.nShippedCrossDockPerCtnRate = 0.240
							.nShippedCrossDockCAPEXPerCtnRate = 0.060
							.nTiltPerUnitRate            = 0.390
							.nTiltResidualsPerUnitRate = 0.000
							.nFullCasePerCtnRate         = 1.350
							.TrackProgress("**** Using 2008-12-01 KMART FOOTWEAR rates ****", LOGIT+SENDIT)
						CASE BETWEEN(tdTransactionDate,{^2010-01-01},{^2010-05-31})
							.nReceiptCrossDockPerCtnRate = 0.250
							.nReceiptCrossDockCAPEXPerCtnRate = 0.050
							.nReceiptNonXDockPerCtnRate  = 0.690
							.nReceiptNonXDockCAPEXPerCtnRate  = 0.100
							.nShippedCrossDockPerCtnRate = 0.250
							.nShippedCrossDockCAPEXPerCtnRate = 0.050
							.nTiltPerUnitRate            = 0.400
							.nTiltResidualsPerUnitRate = 0.250
							.nFullCasePerCtnRate         = 1.400
							.TrackProgress("**** Using 2010-01-01 KMART FOOTWEAR rates ****", LOGIT+SENDIT)
						CASE BETWEEN(tdTransactionDate,{^2010-06-01},{^2010-12-31})
							.nReceiptCrossDockPerCtnRate = 0.270
							.nReceiptCrossDockCAPEXPerCtnRate = 0.000
							.nReceiptNonXDockPerCtnRate  = 0.600
							.nReceiptNonXDockCAPEXPerCtnRate  = 0.000
							.nShippedCrossDockPerCtnRate = 0.270
							.nShippedCrossDockCAPEXPerCtnRate = 0.000
							.nTiltPerUnitRate            = 0.400
							.nTiltResidualsPerUnitRate = 0.250
							.nFullCasePerCtnRate         = 1.150
							.TrackProgress("**** Using 2010-06-01 KMART FOOTWEAR rates ****", LOGIT+SENDIT)
						CASE BETWEEN(tdTransactionDate,{^2011-01-01},{^2011-08-31})
							.nReceiptCrossDockPerCtnRate = 0.270
							.nReceiptCrossDockCAPEXPerCtnRate = 0.000
							.nReceiptNonXDockPerCtnRate  = 0.600
							.nReceiptNonXDockCAPEXPerCtnRate  = 0.000
							.nShippedCrossDockPerCtnRate = 0.270
							.nShippedCrossDockCAPEXPerCtnRate = 0.000
							.nTiltPerUnitRate            = 0.400
							.nTiltResidualsPerUnitRate = 0.000
							.nFullCasePerCtnRate         = 1.150
							.nManualPerUnitRate = 0.000
							.TrackProgress("**** Using 2011-01-01 KMART FOOTWEAR rates ****", LOGIT+SENDIT)
						CASE BETWEEN(tdTransactionDate,{^2011-09-01},{^2011-09-30})
							.nReceiptCrossDockPerCtnRate = 0.270
							.nReceiptCrossDockCAPEXPerCtnRate = 0.000
							.nReceiptNonXDockPerCtnRate  = 0.710
							.nReceiptNonXDockCAPEXPerCtnRate  = 0.000
							.nShippedCrossDockPerCtnRate = 0.270
							.nShippedCrossDockCAPEXPerCtnRate = 0.000
							.nTiltPerUnitRate            = 0.400
							.nTiltResidualsPerUnitRate = 0.000
							.nFullCasePerCtnRate         = 1.450
							.nManualPerUnitRate = 0.000
							.TrackProgress("**** Using 2011-09-01 KMART FOOTWEAR rates ****", LOGIT+SENDIT)
						CASE BETWEEN(tdTransactionDate,{^2011-10-01},{^2012-12-31})
							.nReceiptCrossDockPerCtnRate = 0.500  && = 0.270
							.nReceiptCrossDockCAPEXPerCtnRate = 0.000
							.nReceiptNonXDockPerCtnRate  = 0.850  && = 0.710
							.nReceiptNonXDockCAPEXPerCtnRate  = 0.000
							.nShippedCrossDockPerCtnRate = 0.500  && = 0.270
							.nShippedCrossDockCAPEXPerCtnRate = 0.000
							.nTiltPerUnitRate            = 0.410  && = 0.400
							.nTiltResidualsPerUnitRate = 0.000
							.nFullCasePerCtnRate         = 1.300  && = 1.450
							.nManualPerUnitRate = 0.000
							.TrackProgress("**** Using 2011-10-01 " + .cClient + " rates ****", LOGIT+SENDIT)
						CASE BETWEEN(tdTransactionDate,{^2013-01-01},{^2013-12-31})
							.nReceiptCrossDockPerCtnRate = 0.520
							.nShippedCrossDockPerCtnRate = 0.520
							.nReceiptNonXDockPerCtnRate  = 0.890
							.nTiltPerUnitRate            = 0.430
							.nFullCasePerCtnRate         = 1.360
							.nReceiptCrossDockCAPEXPerCtnRate = 0.000
							.nReceiptNonXDockCAPEXPerCtnRate  = 0.000
							.nShippedCrossDockCAPEXPerCtnRate = 0.000
							.nTiltResidualsPerUnitRate = 0.000
							.nManualPerUnitRate = 0.000
							.nReturnReceiptPerCtnRate    = .nReceiptNonXDockPerCtnRate
							.TrackProgress("**** Using 2013-01-01 " + .cClient + " rates ****", LOGIT+SENDIT)
						CASE BETWEEN(tdTransactionDate,{^2014-01-01},{^2014-12-31})
							.nReceiptCrossDockPerCtnRate = 0.550
							.nShippedCrossDockPerCtnRate = 0.550
							.nReceiptNonXDockPerCtnRate  = 0.930
							.nTiltPerUnitRate            = 0.450
							.nFullCasePerCtnRate         = 1.420
							.nReceiptCrossDockCAPEXPerCtnRate = 0.000
							.nReceiptNonXDockCAPEXPerCtnRate  = 0.000
							.nShippedCrossDockCAPEXPerCtnRate = 0.000
							.nTiltResidualsPerUnitRate = 0.000
							.nManualPerUnitRate = 0.000
							.nReturnReceiptPerCtnRate    = .nReceiptNonXDockPerCtnRate
							.TrackProgress("**** Using 2014-01-01 " + .cClient + " rates ****", LOGIT+SENDIT)
						CASE BETWEEN(tdTransactionDate,{^2015-01-01},{^2015-12-31})
							.nReceiptCrossDockPerCtnRate = 0.570
							.nShippedCrossDockPerCtnRate = 0.570
							.nReceiptNonXDockPerCtnRate  = 0.970
							.nTiltPerUnitRate            = 0.470
							.nFullCasePerCtnRate         = 1.480
							.nReceiptCrossDockCAPEXPerCtnRate = 0.000
							.nReceiptNonXDockCAPEXPerCtnRate  = 0.000
							.nShippedCrossDockCAPEXPerCtnRate = 0.000
							.nTiltResidualsPerUnitRate = 0.000
							.nManualPerUnitRate = 0.000
							.nReturnReceiptPerCtnRate    = .nReceiptNonXDockPerCtnRate
							.TrackProgress("**** Using 2015-01-01 " + .cClient + " rates ****", LOGIT+SENDIT)
						CASE BETWEEN(tdTransactionDate,{^2016-01-01},{^2017-12-31})
							.nReceiptCrossDockPerCtnRate = 0.590
							.nShippedCrossDockPerCtnRate = 0.590
							.nReceiptNonXDockPerCtnRate  = 1.000
							.nTiltPerUnitRate            = 0.490
							.nFullCasePerCtnRate         = 1.530
							.nReceiptCrossDockCAPEXPerCtnRate = 0.000
							.nReceiptNonXDockCAPEXPerCtnRate  = 0.000
							.nShippedCrossDockCAPEXPerCtnRate = 0.000
							.nTiltResidualsPerUnitRate = 0.000
							.nManualPerUnitRate = 0.000
							.nReturnReceiptPerCtnRate    = .nReceiptNonXDockPerCtnRate
							.TrackProgress("**** Using 2016-01-01 " + .cClient + " rates ****", LOGIT+SENDIT)
						OTHERWISE  &&  tdTransactionDate = {???}
							.TrackProgress("**** ERROR: UNEXPECTED TRANSACTION DATE FOR KMART FOOTWEAR RATES = " + TRANSFORM(tdTransactionDate), LOGIT+SENDIT)
					ENDCASE
				CASE .cClient = "KMART NON-FOOTWEAR"
					DO CASE
						CASE BETWEEN(tdTransactionDate,{^2008-12-01},{^2009-12-31})
							* modified 10/14/09 to break out CAPEX rates from other receipt rates.
							*!*	.nReceiptCrossDockPerCtnRate = 0.300
							*!*	.nReceiptCrossDockCAPEXPerCtnRate = 0.000
							*!*	.nReceiptNonXDockPerCtnRate  = 0.790
							*!*	.nReceiptNonXDockCAPEXPerCtnRate  = 0.000
							.nReceiptCrossDockPerCtnRate = 0.240
							.nReceiptCrossDockCAPEXPerCtnRate = 0.060
							.nReceiptNonXDockPerCtnRate  = 0.670
							.nReceiptNonXDockCAPEXPerCtnRate  = 0.120
							.nShippedCrossDockPerCtnRate = 0.240
							.nShippedCrossDockCAPEXPerCtnRate = 0.060
							.nTiltPerUnitRate            = 0.390
							.nTiltResidualsPerUnitRate = 0.000
							.nFullCasePerCtnRate         = 1.350
							.TrackProgress("**** Using 2008-12-01 KMART NON-FOOTWEAR rates ****", LOGIT+SENDIT)
						CASE BETWEEN(tdTransactionDate,{^2010-01-01},{^2010-05-31})
							.nReceiptCrossDockPerCtnRate = 0.250
							.nReceiptCrossDockCAPEXPerCtnRate = 0.050
							.nReceiptNonXDockPerCtnRate  = 0.690
							.nReceiptNonXDockCAPEXPerCtnRate  = 0.100
							.nShippedCrossDockPerCtnRate = 0.250
							.nShippedCrossDockCAPEXPerCtnRate = 0.050
							.nTiltPerUnitRate            = 0.400
							.nTiltResidualsPerUnitRate = 0.250
							.nFullCasePerCtnRate         = 1.400
							.TrackProgress("**** Using 2010-01-01 KMART NON-FOOTWEAR rates ****", LOGIT+SENDIT)
						CASE BETWEEN(tdTransactionDate,{^2010-06-01},{^2010-12-31})
							.nReceiptCrossDockPerCtnRate = 0.270
							.nReceiptCrossDockCAPEXPerCtnRate = 0.000
							.nReceiptNonXDockPerCtnRate  = 0.600
							.nReceiptNonXDockCAPEXPerCtnRate  = 0.000
							.nShippedCrossDockPerCtnRate = 0.270
							.nShippedCrossDockCAPEXPerCtnRate = 0.000
							.nTiltPerUnitRate            = 0.400
							.nTiltResidualsPerUnitRate = 0.250
							.nFullCasePerCtnRate         = 1.150
							.TrackProgress("**** Using 2010-06-01 KMART NON-FOOTWEAR rates ****", LOGIT+SENDIT)
						CASE BETWEEN(tdTransactionDate,{^2011-01-01},{^2011-08-31})
							.nReceiptCrossDockPerCtnRate = 0.270
							.nReceiptCrossDockCAPEXPerCtnRate = 0.000
							.nReceiptNonXDockPerCtnRate  = 0.600
							.nReceiptNonXDockCAPEXPerCtnRate  = 0.000
							.nShippedCrossDockPerCtnRate = 0.270
							.nShippedCrossDockCAPEXPerCtnRate = 0.000
							.nTiltPerUnitRate            = 0.400
							.nTiltResidualsPerUnitRate = 0.000
							.nFullCasePerCtnRate         = 1.150
							.nManualPerUnitRate = 0.000
							.TrackProgress("**** Using 2011-01-01 KMART NON-FOOTWEAR rates ****", LOGIT+SENDIT)
						CASE BETWEEN(tdTransactionDate,{^2011-09-01},{^2011-09-30})
							.nReceiptCrossDockPerCtnRate = 0.270
							.nReceiptCrossDockCAPEXPerCtnRate = 0.000
							.nReceiptNonXDockPerCtnRate  = 0.710
							.nReceiptNonXDockCAPEXPerCtnRate  = 0.000
							.nShippedCrossDockPerCtnRate = 0.270
							.nShippedCrossDockCAPEXPerCtnRate = 0.000
							.nTiltPerUnitRate            = 0.400
							.nTiltResidualsPerUnitRate = 0.000
							.nFullCasePerCtnRate         = 1.450
							.nManualPerUnitRate = 0.000
							.TrackProgress("**** Using 2011-09-01 KMART NON-FOOTWEAR rates ****", LOGIT+SENDIT)
						CASE BETWEEN(tdTransactionDate,{^2011-10-01},{^2012-12-31})
							.nReceiptCrossDockPerCtnRate = 0.500  && = 0.270
							.nReceiptCrossDockCAPEXPerCtnRate = 0.000
							.nReceiptNonXDockPerCtnRate  = 0.850  && = 0.710
							.nReceiptNonXDockCAPEXPerCtnRate  = 0.000
							.nShippedCrossDockPerCtnRate = 0.500  && = 0.270
							.nShippedCrossDockCAPEXPerCtnRate = 0.000
							.nTiltPerUnitRate            = 0.410  && = 0.400
							.nTiltResidualsPerUnitRate = 0.000
							.nFullCasePerCtnRate         = 1.300  && = 1.450
							.nManualPerUnitRate = 0.000
							.TrackProgress("**** Using 2011-10-01 " + .cClient + " rates ****", LOGIT+SENDIT)
						CASE BETWEEN(tdTransactionDate,{^2013-01-01},{^2013-12-31})
							.nReceiptCrossDockPerCtnRate = 0.520
							.nShippedCrossDockPerCtnRate = 0.520
							.nReceiptNonXDockPerCtnRate  = 0.890
							.nTiltPerUnitRate            = 0.430
							.nFullCasePerCtnRate         = 1.360
							.nReceiptCrossDockCAPEXPerCtnRate = 0.000
							.nReceiptNonXDockCAPEXPerCtnRate  = 0.000
							.nShippedCrossDockCAPEXPerCtnRate = 0.000
							.nTiltResidualsPerUnitRate = 0.000
							.nManualPerUnitRate = 0.000
							.nReturnReceiptPerCtnRate    = .nReceiptNonXDockPerCtnRate
							.TrackProgress("**** Using 2013-01-01 " + .cClient + " rates ****", LOGIT+SENDIT)
						CASE BETWEEN(tdTransactionDate,{^2014-01-01},{^2014-12-31})
							.nReceiptCrossDockPerCtnRate = 0.550
							.nShippedCrossDockPerCtnRate = 0.550
							.nReceiptNonXDockPerCtnRate  = 0.930
							.nTiltPerUnitRate            = 0.450
							.nFullCasePerCtnRate         = 1.420
							.nReceiptCrossDockCAPEXPerCtnRate = 0.000
							.nReceiptNonXDockCAPEXPerCtnRate  = 0.000
							.nShippedCrossDockCAPEXPerCtnRate = 0.000
							.nTiltResidualsPerUnitRate = 0.000
							.nManualPerUnitRate = 0.000
							.nReturnReceiptPerCtnRate    = .nReceiptNonXDockPerCtnRate
							.TrackProgress("**** Using 2014-01-01 " + .cClient + " rates ****", LOGIT+SENDIT)
						CASE BETWEEN(tdTransactionDate,{^2015-01-01},{^2015-12-31})
							.nReceiptCrossDockPerCtnRate = 0.570
							.nShippedCrossDockPerCtnRate = 0.570
							.nReceiptNonXDockPerCtnRate  = 0.970
							.nTiltPerUnitRate            = 0.470
							.nFullCasePerCtnRate         = 1.480
							.nReceiptCrossDockCAPEXPerCtnRate = 0.000
							.nReceiptNonXDockCAPEXPerCtnRate  = 0.000
							.nShippedCrossDockCAPEXPerCtnRate = 0.000
							.nTiltResidualsPerUnitRate = 0.000
							.nManualPerUnitRate = 0.000
							.nReturnReceiptPerCtnRate    = .nReceiptNonXDockPerCtnRate
							.TrackProgress("**** Using 2015-01-01 " + .cClient + " rates ****", LOGIT+SENDIT)
						CASE BETWEEN(tdTransactionDate,{^2016-01-01},{^2017-12-31})
							.nReceiptCrossDockPerCtnRate = 0.590
							.nShippedCrossDockPerCtnRate = 0.590
							.nReceiptNonXDockPerCtnRate  = 1.000
							.nTiltPerUnitRate            = 0.490
							.nFullCasePerCtnRate         = 1.530
							.nReceiptCrossDockCAPEXPerCtnRate = 0.000
							.nReceiptNonXDockCAPEXPerCtnRate  = 0.000
							.nShippedCrossDockCAPEXPerCtnRate = 0.000
							.nTiltResidualsPerUnitRate = 0.000
							.nManualPerUnitRate = 0.000
							.nReturnReceiptPerCtnRate    = .nReceiptNonXDockPerCtnRate
							.TrackProgress("**** Using 2016-01-01 " + .cClient + " rates ****", LOGIT+SENDIT)
						OTHERWISE  &&  tdTransactionDate = {???}
							.TrackProgress("**** ERROR: UNEXPECTED TRANSACTION DATE FOR KMART NON-FOOTWEAR RATES = " + TRANSFORM(tdTransactionDate), LOGIT+SENDIT)
					ENDCASE
				CASE INLIST(.cClient,"SEARS FRAGRANCE","SEARS LANDS END","SEARS ATHLETIC","SEARS KIDS","SEARS WOMENS","SEARS MENS","SEARS TOWELS","SEARS JEANS","SEARS BELTS","SEARS KIDS JEANS","SEARS BRAS","SEARS ALL")
					DO CASE
						CASE BETWEEN(tdTransactionDate,{^2011-01-01},{^2011-08-31})
							.nReceiptCrossDockPerCtnRate = 0.270
							.nReceiptCrossDockCAPEXPerCtnRate = 0.000
							.nReceiptNonXDockPerCtnRate  = 0.600
							.nReceiptNonXDockCAPEXPerCtnRate  = 0.000
							.nShippedCrossDockPerCtnRate = 0.270
							.nShippedCrossDockCAPEXPerCtnRate = 0.000
							.nTiltPerUnitRate            = 0.450
							.nTiltResidualsPerUnitRate = 0.000
							.nFullCasePerCtnRate         = 1.150
							.nManualPerUnitRate = 0.000
							.TrackProgress("**** Using 2011-01-01 " + .cClient + " rates ****", LOGIT+SENDIT)
						CASE BETWEEN(tdTransactionDate,{^2011-09-01},{^2011-09-30})
							.nReceiptCrossDockPerCtnRate = 0.270
							.nReceiptCrossDockCAPEXPerCtnRate = 0.000
							.nReceiptNonXDockPerCtnRate  = 0.710
							.nReceiptNonXDockCAPEXPerCtnRate  = 0.000
							.nShippedCrossDockPerCtnRate = 0.270
							.nShippedCrossDockCAPEXPerCtnRate = 0.000
							.nTiltPerUnitRate            = 0.450
							.nTiltResidualsPerUnitRate = 0.000
							.nFullCasePerCtnRate         = 1.450
							.nManualPerUnitRate = 0.000
							.TrackProgress("**** Using 2011-09-01 " + .cClient + " rates ****", LOGIT+SENDIT)
						CASE BETWEEN(tdTransactionDate,{^2011-10-01},{^2012-12-31})
							.nReceiptCrossDockPerCtnRate = 0.500  && = 0.270
							.nReceiptCrossDockCAPEXPerCtnRate = 0.000
							.nReceiptNonXDockPerCtnRate  = 0.850  && = 0.710
							.nReceiptNonXDockCAPEXPerCtnRate  = 0.000
							.nShippedCrossDockPerCtnRate = 0.500  && = 0.270
							.nShippedCrossDockCAPEXPerCtnRate = 0.000
							.nTiltPerUnitRate            = 0.410  && = 0.450
							.nTiltResidualsPerUnitRate = 0.000
							.nFullCasePerCtnRate         = 1.300  && = 1.450
							.nManualPerUnitRate = 0.000
							.TrackProgress("**** Using 2011-10-01 " + .cClient + " rates ****", LOGIT+SENDIT)
						CASE BETWEEN(tdTransactionDate,{^2013-01-01},{^2013-12-31})
							.nReceiptCrossDockPerCtnRate = 0.520
							.nShippedCrossDockPerCtnRate = 0.520
							.nReceiptNonXDockPerCtnRate  = 0.890
							.nTiltPerUnitRate            = 0.430
							.nFullCasePerCtnRate         = 1.360
							.nReceiptCrossDockCAPEXPerCtnRate = 0.000
							.nReceiptNonXDockCAPEXPerCtnRate  = 0.000
							.nShippedCrossDockCAPEXPerCtnRate = 0.000
							.nTiltResidualsPerUnitRate = 0.000
							.nManualPerUnitRate = 0.000
							.nReturnReceiptPerCtnRate    = .nReceiptNonXDockPerCtnRate
							.TrackProgress("**** Using 2013-01-01 " + .cClient + " rates ****", LOGIT+SENDIT)
						CASE BETWEEN(tdTransactionDate,{^2014-01-01},{^2014-12-31})
						
							.nReceiptCrossDockPerCtnRate = 0.550
							.nShippedCrossDockPerCtnRate = 0.550
							.nReceiptNonXDockPerCtnRate  = 0.930
							.nTiltPerUnitRate            = 0.450
							.nFullCasePerCtnRate         = 1.420
							.nReceiptCrossDockCAPEXPerCtnRate = 0.000
							.nReceiptNonXDockCAPEXPerCtnRate  = 0.000
							.nShippedCrossDockCAPEXPerCtnRate = 0.000
							.nTiltResidualsPerUnitRate = 0.000
							.nManualPerUnitRate = 0.000
							.nReturnReceiptPerCtnRate    = .nReceiptNonXDockPerCtnRate						
							
							.TrackProgress("**** Using 2014-01-01 " + .cClient + " rates ****", LOGIT+SENDIT)
						CASE BETWEEN(tdTransactionDate,{^2015-01-01},{^2015-12-31})
						
							.nReceiptCrossDockPerCtnRate = 0.570
							.nShippedCrossDockPerCtnRate = 0.570
							.nReceiptNonXDockPerCtnRate  = 0.970
							.nTiltPerUnitRate            = 0.470
							.nFullCasePerCtnRate         = 1.480
							.nReceiptCrossDockCAPEXPerCtnRate = 0.000
							.nReceiptNonXDockCAPEXPerCtnRate  = 0.000
							.nShippedCrossDockCAPEXPerCtnRate = 0.000
							.nTiltResidualsPerUnitRate = 0.000
							.nManualPerUnitRate = 0.000
							.nReturnReceiptPerCtnRate    = .nReceiptNonXDockPerCtnRate						
							
							.TrackProgress("**** Using 2015-01-01 " + .cClient + " rates ****", LOGIT+SENDIT)
							
						CASE BETWEEN(tdTransactionDate,{^2016-01-01},{^2017-12-31})
						
							.nReceiptCrossDockPerCtnRate = 0.590
							.nShippedCrossDockPerCtnRate = 0.590
							.nReceiptNonXDockPerCtnRate  = 1.000
							.nTiltPerUnitRate            = 0.490
							.nFullCasePerCtnRate         = 1.530
							.nReceiptCrossDockCAPEXPerCtnRate = 0.000
							.nReceiptNonXDockCAPEXPerCtnRate  = 0.000
							.nShippedCrossDockCAPEXPerCtnRate = 0.000
							.nTiltResidualsPerUnitRate = 0.000
							.nManualPerUnitRate = 0.000
							.nReturnReceiptPerCtnRate    = .nReceiptNonXDockPerCtnRate						
							
							.TrackProgress("**** Using 2016-01-01 " + .cClient + " rates ****", LOGIT+SENDIT)
						OTHERWISE  &&  tdTransactionDate = {???}
							.TrackProgress("**** ERROR: unexpected transaction date for SEARS rates = " + TRANSFORM(tdTransactionDate), LOGIT+SENDIT)
					ENDCASE
					
							*!*	**** to produce a 0 rate bill for Sales - ACTIVATE THIS!
							*!*	.nReceiptCrossDockPerCtnRate = 0.000
							*!*	.nShippedCrossDockPerCtnRate = 0.000
							*!*	.nReceiptNonXDockPerCtnRate  = 0.000
							*!*	.nTiltPerUnitRate            = 0.000
							*!*	.nFullCasePerCtnRate         = 0.000
							*!*	.nReceiptCrossDockCAPEXPerCtnRate = 0.000
							*!*	.nReceiptNonXDockCAPEXPerCtnRate  = 0.000
							*!*	.nShippedCrossDockCAPEXPerCtnRate = 0.000
							*!*	.nTiltResidualsPerUnitRate = 0.000
							*!*	.nManualPerUnitRate = 0.000
							*!*	.nReturnReceiptPerCtnRate    = 0.000
							

				CASE INLIST(.cClient, "FORSAN", "FORSAN MENS")				
					* NOTE
					DO CASE
						CASE BETWEEN(tdTransactionDate,{^2015-12-01},{^2016-12-31})
						
							.nReceiptCrossDockPerCtnRate = 0.000
							.nShippedCrossDockPerCtnRate = 0.000   
							.nReceiptNonXDockPerCtnRate  = 0.970
							.nTiltPerUnitRate            = 0.000 
							   
							.nFullCasePerCtnRate         = 1.420
							
							.nReceiptCrossDockCAPEXPerCtnRate = 0.000
							.nReceiptNonXDockCAPEXPerCtnRate  = 0.000
							.nShippedCrossDockCAPEXPerCtnRate = 0.000
							.nTiltResidualsPerCartonRate = 0.000   
							.nManualPerUnitRate = 0.000
							.nReturnReceiptPerCtnRate    = .nReceiptNonXDockPerCtnRate
							.nReceiptNonXDockPerUNITRate  = 0.000							
							
							.TrackProgress("**** Using 2015-12-01 " + .cClient + " rates ****", LOGIT+SENDIT)
							
						OTHERWISE  &&  tdTransactionDate = {???}
							.TrackProgress("**** ERROR: unexpected transaction date for ' + .cClient + ' rates = " + TRANSFORM(tdTransactionDate), LOGIT+SENDIT)
					ENDCASE

			ENDCASE
		ENDWITH
	ENDPROC  &&  SetRates

*!*		FUNCTION ThereIsDataForDate
*!*			* Determines if there is transaction data loaded for a passed date.
*!*			*
*!*			* Typically you pass this routine the date of the Monday following the last day of the week being invoiced,
*!*			* and it will return TRUE if there is any transaction data for that day.
*!*			*
*!*			* If it returns FALSE, it indicates that we have not yet loaded that Monday's transaction
*!*			* data, which typically arives in NJ on Monday afternoon, EST. In this case the invoice should
*!*			* NOT be generated because we are likely missing the transaction data for Friday afternoon
*!*			* or possibly Saturday. MB 08/16/04.
*!*			LPARAMETERS tdDate
*!*			LOCAL llRetVal
*!*			WITH THIS
*!*				.TrackProgress('Checking that all data has been loaded for the invoice period...',LOGIT+WAITIT+SENDIT)

*!*				IF USED('curISTHEREDATA') THEN
*!*					USE IN curISTHEREDATA
*!*				ENDIF

*!*				* open transaction tables
*!*				USE (.cTransTable) AGAIN IN 0 ALIAS TTISTHEREDATA

*!*				* see if there's any transaction data for the Monday
*!*				SELECT TTISTHEREDATA
*!*				LOCATE FOR TTOD(TDATETIME) = tdDate
*!*				llRetVal = FOUND()

*!*				* close tables/cursors
*!*				IF USED('TTISTHEREDATA') THEN
*!*					USE IN TTISTHEREDATA
*!*				ENDIF

*!*				IF USED('curISTHEREDATA') THEN
*!*					USE IN curISTHEREDATA
*!*				ENDIF

*!*			ENDWITH
*!*			RETURN llRetVal
*!*		ENDFUNC


	* revised 08/12/05 MB to add xdockcost field to receipts xls file
	FUNCTION RollupSKUBreakdownDetailForXL
		WITH THIS
			LOCAL llRetVal, lnReceiptCrossDockPerCtnRate
			llRetVal = .T.
			lnReceiptCrossDockPerCtnRate = .nReceiptCrossDockPerCtnRate
			* curAllRCNs
			IF USED('curSKUBreakdownDetailForXL') THEN
				USE IN curSKUBreakdownDetailForXL
			ENDIF
			SELECT ;
				TRANSDATE, ;
				COMPANY, ;
				SUM(XDCARTONS) AS XDOCKCASES, ;
				SUM(XDUNITS) AS XDOCKUNITS, ;
				SUM(CARTONS) AS OTHERCASES, ;
				SUM(UNITS) AS OTHERUNITS, ;
				(SUM(XDCARTONS) * lnReceiptCrossDockPerCtnRate) AS XDOCKCOST, ;
				SUM(SKU0) AS SKU0COST, ;
				SUM(SKU21) AS SKU21COST, ;
				SUM(SKU31) AS SKU31COST, ;
				SUM(SKU41) AS SKU41COST ;
				FROM curAllRCNs ;
				INTO CURSOR curSKUBreakdownDetailForXL ;
				GROUP BY TRANSDATE, COMPANY ;
				ORDER BY TRANSDATE, COMPANY

			SELECT curSKUBreakdownDetailForXL
			GOTO TOP
			COPY TO (.cXLReceiptFilename) XL5
			* add to attachments
			.AddAttachment(.cXLReceiptFilename)
			.TrackProgress('SEARS RollupSKUBreakdownDetailForXL() ended normally.', LOGIT+SENDIT)
		ENDWITH
		RETURN llRetVal
	ENDFUNC   &&  RollupSKUBreakdownDetailForXL



	FUNCTION RollupShippedDetailForXL
		WITH THIS
			LOCAL llRetVal, lcXLFilename, lnShippedCrossDockPerCtnRate, lnManualPerUnitRate, lnFullCasePerCtnRate, lnTiltPerUnitRate, lnFindingsPerCtnRate
			llRetVal = .T.
			lnShippedCrossDockPerCtnRate = .nShippedCrossDockPerCtnRate
			lnManualPerUnitRate = .nManualPerUnitRate
			lnFullCasePerCtnRate = .nFullCasePerCtnRate
			lnTiltPerUnitRate = .nTiltPerUnitRate
			lnFindingsPerCtnRate = .nFindingsPerCtnRate
			IF USED('curShippedDetailForXL') THEN
				USE IN curShippedDetailForXL
			ENDIF
			SELECT ;
				TRANSDATE, ;
				COMPANY, ;
				SUM(TOTCASES) AS TOTCASES, ;
				SUM(TOTXDOCK) AS TOTXDOCK, ;
				SUM(TOTXDOCK) * lnShippedCrossDockPerCtnRate AS XDOCKCHG, ;
				SUM(TOTTILT) AS TOTTILT, ;
				SUM(TOTTILT) * lnTiltPerUnitRate AS TILTCHG, ;
				SUM(TOTMANUAL) AS TOTMANUAL, ;
				SUM(TOTMANUAL) * lnManualPerUnitRate AS MANUALCHG, ;
				SUM(TOTRSDCTN) AS TOTRSDCTN, ;
				SUM(TOTFINDING) AS TOTFINDING, ;
				SUM(TOTFINDING) * lnFindingsPerCtnRate AS FINDINGCHG, ;
				SUM(TOTFULL) AS TOTFULL, ;
				SUM(TOTFULL) * lnFullCasePerCtnRate AS FULLCHG ;
				FROM curAllSHIPPED ;
				INTO CURSOR curShippedDetailForXL ;
				GROUP BY TRANSDATE, COMPANY ;
				ORDER BY TRANSDATE, COMPANY

			SELECT curShippedDetailForXL
			GOTO TOP
			COPY TO (.cXLShippedFilename) XL5
			* add to attachments
			.AddAttachment(.cXLShippedFilename)
			.TrackProgress('SEARS RollupShippedDetailForXL() ended normally.', LOGIT+SENDIT)
		ENDWITH
		RETURN llRetVal
	ENDFUNC   &&  RollupShippedDetailForXL

*!*		FUNCTION MakeBillForOneDate
*!*			* call this with a single date to do everything necessary
*!*			LPARAMETERS tdReportDate
*!*			LOCAL llRetVal
*!*			llRetVal = .T.
*!*			WITH THIS
*!*				*TRY
*!*				IF ISNULL(.oAcrobat) THEN
*!*					.oAcrobat = CREATEOBJECT("AcroExch.PDDoc")
*!*				ENDIF
*!*				IF ISNULL(.oAcrobat2) THEN
*!*					.oAcrobat2 = CREATEOBJECT("AcroExch.PDDoc")
*!*				ENDIF
*!*				*CATCH
*!*				*ENDTRY
*!*				.MakeBill(tdReportDate)
*!*				*!*				.CreateSKUBreakdownDetailPDF()
*!*				*!*				*!*				.CreateSKUBreakdownDetailPDFXDOCK()
*!*				*!*				.CreateShippedDetailPDF()
*!*				*!*				.RollupSKUBreakdownDetailForXL()
*!*				*!*				.RollupShippedDetailForXL()
*!*				.SendEmail()
*!*			ENDWITH
*!*			RETURN llRetVal
*!*		ENDFUNC  &&  MakeBillForOneDate


PROCEDURE DeleteDuplicateAUADRecords
	* THESE ARE SEVEN TRAILER LOADS THAT HAD 8327 DUPLICATE AUAD RECS IN 1/27/09 AND 1/28/09.
	* DOUG SAID I SHOULD DELETE THE RECS IN 1/28. MB 2/4/09.
	RETURN
	
	USE F:\SEARS\PMDATA\FTPMDATA EXCL	
	DELETE FOR INLIST(TRAILERLD,'X39419','X39429','X39431','X39450','X39452','X39467','X39477') AND TTOD(TDATETIME) = {^2009-01-28}
	PACK
	
	* to document for credit memo, extract deleted recs from backup
	SELECT * ;
		FROM C:\SEARS\PMDATA\FTPMDATA ;
		INTO CURSOR CURDUPES ;
		WHERE INLIST(TRAILERLD,'X39419','X39429','X39431','X39450','X39452','X39467','X39477') AND ;
		TTOD(TDATETIME) = {^2009-01-28} ;
		ORDER BY TDATETIME
	
	SELECT CURDUPES
	COPY TO C:\A\SEARSDUPES.XLS XL5
	CLOSE DATABASES 

	SELECT DISTINCT CONTAINER ;
		FROM C:\SEARS\PMDATA\FTPMDATA ;
		INTO CURSOR CURDUPES ;
		WHERE INLIST(TRAILERLD,'X39419','X39429','X39431','X39450','X39452','X39467','X39477') AND ;
		TTOD(TDATETIME) = {^2009-01-28} ;
		ORDER BY CONTAINER

	SELECT CURDUPES
	COPY TO C:\A\SEARSDUPES.XLS XL5
	CLOSE DATABASES 

	
ENDPROC

PROCEDURE DeleteDuplicateRCRCRecords
	* These ARE received caseids that Doug said I should remove from 2/19 load. MB 2/19/09.
	RETURN
	
	USE F:\SEARS\PMDATA\RCRCHIST EXCL
	BROW FOR INLIST(CASEID,'00007265302363384785','00007265302363384792','00007265302363384808', ;
		'00007265302363384761','00007265302363384778','00007265302363384716','00007265302363384723', ;
		'00007265302363384730','00007265302363384747','00007265302363384754')
	CLOSE DATA
	
ENDPROC


PROCEDURE SearsTrivia
	#DEFINE CRLF CHR(13) + CHR(10)
	LOCAL lcText
	lcText = ""
	
	CLOSE DATABASES
	
	SELECT ;
		SUM(RXDOCKCTNS+RNXDCTNS) AS CASES_IN, ;
		SUM(SXDOCKCTNS+FCASECTNS) AS CASES_OUT, ;
		SUM(TILTUNITS) AS TILTUNITS ;
	FROM F:\SEARS\pmdata\fthistory ;
	INTO CURSOR CURTRIVIA ;
	WHERE TNODE = "KM" ;
	AND TRANSDATE >= {^2009-01-01} ;
	AND NOT LTEST
	
	SELECT CURTRIVIA
	LOCATE	

	lcText = "Total cases received in 2009 = " + ALLTRIM(TRANSFORM(CURTRIVIA.CASES_IN))
	lcText =lcText + CRLF + "Total cases shipped in 2009 = " + ALLTRIM(TRANSFORM(CURTRIVIA.CASES_OUT))
	lcText =lcText + CRLF + "Total units processed through tilt tray in 2009 = " + ALLTRIM(TRANSFORM(CURTRIVIA.TILTUNITS))
		
	SELECT DISTINCT RCN ;
		FROM F:\SEARS\PMDATA\FTPMDATA ;
		INTO CURSOR CURRCNS ;
		WHERE TRANS = "RBPC" ;
		AND TNODE = "KM" ;
		AND TTOD(TDATETIME) >= {^2009-01-01}
		
	SELECT CURRCNS
	lcText =lcText + CRLF + "Total RCN'S completed in 2009 = " + ALLTRIM(TRANSFORM(RECCOUNT('CURRCNS')))
	
	SELECT DISTINCT TRAILERLD ;
		FROM F:\SEARS\PMDATA\FTPMDATA ;
		INTO CURSOR CURTRAILERS ;
		WHERE TRANS = "AUAD" ;
		AND TNODE = "KM" ;
		AND TTOD(TDATETIME) >= {^2009-01-01}
		
	lcText =lcText + CRLF + "Total trailer loads shipped in 2009 = " + ALLTRIM(TRANSFORM(RECCOUNT('CURTRAILERS')))
	
	SELECT DISTINCT CONTAINER ;
		FROM F:\SEARS\PMDATA\FTPMDATA ;
		INTO CURSOR CURCONTAINERS ;
		WHERE TRANS = "AUAD" ;
		AND TNODE = "KM" ;
		AND TTOD(TDATETIME) >= {^2009-01-01}
		
	lcText =lcText + CRLF + "Total containers shipped in 2009 = " + ALLTRIM(TRANSFORM(RECCOUNT('CURCONTAINERS')))
	
	_cliptext = lcText
	RETURN
ENDPROC


PROCEDURE IndexTables
*!*		* RCRCHIST
*!*		USE F:\SEARS\pmdata\rcrchist.dbf EXCL
*!*		DELETE TAG ALL
*!*		INDEX ON TDATETIME TAG TDATETIME
*!*		INDEX ON RCN TAG RCN
*!*		INDEX ON SKUSPERRCN TAG SKUSPERRCN
*!*		INDEX ON DELETED() TAG DEL
*!*		INDEX ON CONTAINER TAG CONTAINER
*!*		INDEX ON SKU TAG SKU
*!*		INDEX ON COMPANY TAG COMPANY
*!*		INDEX ON CASEID TAG CASEID
*!*		INDEX ON PONUM TAG PONUM
*!*		INDEX ON TTOD(TDATETIME) TAG TDATE
*!*		INDEX ON TNODE TAG TNODE
*!*		USE

*!*		* FTPMDATA
*!*		USE F:\SEARS\pmdata\ftpmdata.dbf EXCL
*!*		DELETE TAG ALL
*!*		INDEX ON TRANS TAG TRANS
*!*		INDEX ON SORT_TYPE TAG SORT_TYPE
*!*		INDEX ON CASEID TAG CASEID
*!*		INDEX ON DELETED() TAG DEL
*!*		INDEX ON ORDER_TYPE TAG ORDER_TYPE
*!*		INDEX ON RCN TAG RCN
*!*		INDEX ON TTOD(TDATETIME) TAG TDATE
*!*		INDEX ON DEST_LOC TAG DEST_LOC
*!*		INDEX ON TNODE TAG TNODE
*!*		INDEX ON TRAILERLD TAG TRAILERLD
*!*		INDEX ON DIVISION TAG DIVISION 

*!*		USE
*!*		* RATCHIST
*!*		USE F:\SEARS\pmdata\ratchist.dbf EXCL
*!*		DELETE TAG ALL
*!*		INDEX ON RCN TAG RCN
*!*		INDEX ON CONTAINER TAG CONTAINER
*!*		INDEX ON TDATETIME TAG TDATETIME
*!*		INDEX ON CONTTYPE TAG CONTTYPE
*!*		INDEX ON FNODE TAG FNODE
*!*		INDEX ON TNODE TAG TNODE
*!*		INDEX ON TTOD(TDATETIME) TAG TDATE
*!*		USE
*!*		* RATOHIST
*!*		USE F:\SEARS\pmdata\ratohist.dbf EXCL
*!*		DELETE TAG ALL
*!*		INDEX ON RCN TAG RCN
*!*		INDEX ON CONTAINER TAG CONTAINER
*!*		INDEX ON TDATETIME TAG TDATETIME
*!*		INDEX ON CONTTYPE TAG CONTTYPE
*!*		INDEX ON FNODE TAG FNODE
*!*		INDEX ON TNODE TAG TNODE
*!*		INDEX ON TTOD(TDATETIME) TAG TDATE
*!*		USE
*!*		* RCRZHIST
*!*		USE F:\SEARS\pmdata\rcrzhist.dbf EXCL
*!*		DELETE TAG ALL
*!*		INDEX ON TDATETIME TAG TDATETIME
*!*		INDEX ON RCN TAG RCN
*!*		INDEX ON SKUSPERRCN TAG SKUSPERRCN
*!*		INDEX ON DELETED() TAG DEL
*!*		INDEX ON CONTAINER TAG CONTAINER
*!*		INDEX ON TTOD(TDATETIME) TAG TDATE
*!*		INDEX ON TNODE TAG TNODE
*!*		USE
*!*		* RDCAHIST
*!*		USE F:\SEARS\pmdata\rdcahist.dbf EXCL
*!*		DELETE TAG ALL
*!*		INDEX ON TDATETIME TAG TDATETIME
*!*		INDEX ON CASEID TAG CASEID
*!*		INDEX ON RCN TAG RCN
*!*		INDEX ON DELETED() TAG DEL
*!*		INDEX ON TTOD(TDATETIME) TAG TDATE
*!*		INDEX ON DEST_LOC TAG DEST_LOC
*!*		USE
	
	RETURN
ENDPROC



PROCEDURE IndexLocalTables
	* RCRCHIST
	USE C:\SEARS\pmdata\rcrchist.dbf EXCL
	DELETE TAG ALL
	INDEX ON TDATETIME TAG TDATETIME
	INDEX ON RCN TAG RCN
	INDEX ON SKUSPERRCN TAG SKUSPERRCN
	INDEX ON DELETED() TAG DEL
	INDEX ON CONTAINER TAG CONTAINER
	INDEX ON SKU TAG SKU
	INDEX ON COMPANY TAG COMPANY
	INDEX ON CASEID TAG CASEID
	INDEX ON PONUM TAG PONUM
	INDEX ON TTOD(TDATETIME) TAG TDATE
	INDEX ON TNODE TAG TNODE
	USE

	* FTPMDATA
	USE C:\SEARS\pmdata\ftpmdata.dbf EXCL
	DELETE TAG ALL
	INDEX ON TRANS TAG TRANS
	INDEX ON SORT_TYPE TAG SORT_TYPE
	INDEX ON CASEID TAG CASEID
	INDEX ON DELETED() TAG DEL
	INDEX ON ORDER_TYPE TAG ORDER_TYPE
	INDEX ON RCN TAG RCN
	INDEX ON TTOD(TDATETIME) TAG TDATE
	INDEX ON DEST_LOC TAG DEST_LOC
	INDEX ON TNODE TAG TNODE
	INDEX ON TRAILERLD TAG TRAILERLD
	INDEX ON DIVISION TAG DIVISION 

	USE
	* RATCHIST
	USE C:\SEARS\pmdata\ratchist.dbf EXCL
	DELETE TAG ALL
	INDEX ON RCN TAG RCN
	INDEX ON CONTAINER TAG CONTAINER
	INDEX ON TDATETIME TAG TDATETIME
	INDEX ON CONTTYPE TAG CONTTYPE
	INDEX ON FNODE TAG FNODE
	INDEX ON TNODE TAG TNODE
	INDEX ON TTOD(TDATETIME) TAG TDATE
	USE
	* RATOHIST
	USE C:\SEARS\pmdata\ratohist.dbf EXCL
	DELETE TAG ALL
	INDEX ON RCN TAG RCN
	INDEX ON CONTAINER TAG CONTAINER
	INDEX ON TDATETIME TAG TDATETIME
	INDEX ON CONTTYPE TAG CONTTYPE
	INDEX ON FNODE TAG FNODE
	INDEX ON TNODE TAG TNODE
	INDEX ON TTOD(TDATETIME) TAG TDATE
	USE
	* RCRZHIST
	USE C:\SEARS\pmdata\rcrzhist.dbf EXCL
	DELETE TAG ALL
	INDEX ON TDATETIME TAG TDATETIME
	INDEX ON RCN TAG RCN
	INDEX ON SKUSPERRCN TAG SKUSPERRCN
	INDEX ON DELETED() TAG DEL
	INDEX ON CONTAINER TAG CONTAINER
	INDEX ON TTOD(TDATETIME) TAG TDATE
	INDEX ON TNODE TAG TNODE
	USE
	* RDCAHIST
	USE C:\SEARS\pmdata\rdcahist.dbf EXCL
	DELETE TAG ALL
	INDEX ON TDATETIME TAG TDATETIME
	INDEX ON CASEID TAG CASEID
	INDEX ON RCN TAG RCN
	INDEX ON DELETED() TAG DEL
	INDEX ON TTOD(TDATETIME) TAG TDATE
	INDEX ON DEST_LOC TAG DEST_LOC
	USE
	
	RETURN
ENDPROC

*!*	PROCEDURE GetManualUnits
*!*		* MAKE DATA RANGE PARAMETERS AND THEN EMAIL JONI THE INFO WHEN INVOICED!
*!*		SELECT transdate, tnode, manunits from F:\SEARS\pmdata\fthistory.dbf WHERE INLIST(tnode,"KM","SR") AND MANUNITS > 0 ORDER BY TRANSDATE, TNODE
*!*		COPY TO C:\A\ML_MANUAL_UNITS.XLS XL5
*!*	ENDPROC


* CODE TO GET MONTHLY INFO FOR STEVEN SYKES:
*!*	USE F:\SEARS\pmdata\fthistory.dbf AGAIN IN 0 ALIAS Fthistory
*!*	COPY TO c:\a\mlautobilldata_09_2011.xls XL5 FOR transdate >= {^2011-09-01} and transdate <= {^2011-09-30}

	PROCEDURE CheckDivision
		LPARAMETERS tcNode, tcDiv, tcFeed
		* for now we will only check KMart and Sears, Tnode = KM or SR; ignoring BD which is rite-aid
		LOCAL lcEntry
		WITH THIS
			*!*	IF INLIST(tcNode, "KM", "SR") THEN
			*!*		IF NOT RIGHT(tcDiv,2)$ SHC_VALID_DIV_LIST THEN
			*!*			lcEntry = "[" + tcDiv + "-" + tcFeed + "]"
			*!*			IF NOT lcEntry $ .cInvalidDivisionList THEN
			*!*				.cInvalidDivisionList = .cInvalidDivisionList + " " + lcEntry
			*!*			ENDIF
			*!*		ENDIF
			*!*	ENDIF
			DO CASE
				CASE INLIST(tcNode, "KM", "SR", "FS") && ADDED FORSAN HERE BECAUSE I THINK IT HAS SAME DIVISIONS AS SEARS/KMART 12/14/2015 MB
					IF NOT RIGHT(tcDiv,2)$ SHC_VALID_DIV_LIST THEN
						lcEntry = "[" + tcDiv + "-" + tcFeed + "]"
						IF NOT lcEntry $ .cInvalidDivisionList THEN
							.cInvalidDivisionList = .cInvalidDivisionList + " " + lcEntry
						ENDIF
					ENDIF
				CASE INLIST(tcNode, "RR", "OB") 
					IF NOT RIGHT(tcDiv,2)$ RR_VALID_DIV_LIST THEN
						lcEntry = "[" + tcDiv + "-" + tcFeed + "]"
						IF NOT lcEntry $ .cInvalidDivisionList THEN
							.cInvalidDivisionList = .cInvalidDivisionList + " " + lcEntry
						ENDIF
					ENDIF
			ENDCASE
		ENDWITH
		RETURN
	ENDPROC
	

	PROCEDURE FTPInvoiceSummaryRackroom
		LPARAMETERS	tdInvoiceDate
		LOCAL lcOutputFile, lcSendTo,lcFrom,lcSubject,lcCC,lcAttach,lcBodyText
		WITH THIS
			IF .lTestInvoice THEN
			
				WAIT WINDOW "Test invoice mode - NOT FTPing invoice data..." TIMEOUT 5	
						
			ELSE

				lcOutputFile = "F:\UTIL\MLAUTOBILLING\FTP\RR_OB_INVDATA_" + DTOS(tdInvoiceDate) + ".CSV"

				lcSendTo = 'doug@dhwachs.com'
				lcFrom = 'mark.bennett@tollgroup.com'
				lcSubject = 'Summary of Rack Room Invoicing'
				lcCC = 'mark.bennett@tollgroup.com'
				lcAttach = lcOutputFile
				lcBodyText = 'See attached CSV file for details of the current invoices.'			
				
				IF USED('CURINVDATA') THEN
					USE IN CURINVDATA
				ENDIF
				
				USE F:\SEARS\PMDATA\FTHISTORY AGAIN IN 0 ALIAS FTHISTORY
				
				SELECT * ;
					FROM FTHISTORY ;
					INTO CURSOR CURINVDATA ;
					WHERE INVDATE = tdInvoiceDate ;
					AND TNODE IN ('RR','OB') ;
					ORDER BY CLIENT, TRANSDATE
				
				*SELECT CURINVDATA
				*BROW	
				
				SELECT CURINVDATA
				COPY TO (lcOutputFile) TYPE CSV
				
				IF USED('CURINVDATA') THEN
					USE IN CURINVDATA
				ENDIF
				IF USED('FTHISTORY') THEN
					USE IN FTHISTORY
				ENDIF
				
				DO FORM dartmail2 WITH lcSendTo,lcFrom,lcSubject,lcCC,lcAttach,lcBodyText,"A"
				
				WAIT WINDOW "Emailed invoice data..." TIMEOUT 5	
						
			ENDIF
				
		ENDWITH
	ENDPROC
	
	
	PROCEDURE FTPInvoiceSummaryRackroomCUSTOMDATES
		LOCAL lcOutputFile, lcSendTo,lcFrom,lcSubject,lcCC,lcAttach,lcBodyText, ldFromDate, ldToDate
		
		ldFromDate = {^2017-01-01}
		ldToDate = DATE()
		lcOutputFile = "C:\A\RR_OB_INVDATA_" + DTOS(ldFromDate) + "-" + DTOS(ldToDate) + ".CSV"

		IF USED('CURINVDATA') THEN
			USE IN CURINVDATA
		ENDIF
		
		USE F:\SEARS\PMDATA\FTHISTORY AGAIN IN 0 ALIAS FTHISTORY
		
		SELECT * ;
			FROM FTHISTORY ;
			INTO CURSOR CURINVDATA ;
			WHERE BETWEEN(INVDATE,ldFromDate,ldToDate) ;
			AND TNODE IN ('RR','OB') ;
			ORDER BY CLIENT, TRANSDATE
		
		*SELECT CURINVDATA
		*BROW	
		
		SELECT CURINVDATA
		COPY TO (lcOutputFile) TYPE CSV
		
		IF USED('CURINVDATA') THEN
			USE IN CURINVDATA
		ENDIF
		IF USED('FTHISTORY') THEN
			USE IN FTHISTORY
		ENDIF
				
	ENDPROC
	

	PROCEDURE FTPInvoiceSummarySears
		LPARAMETERS	tdInvoiceDate
		LOCAL lcOutputFile, lcSendTo,lcFrom,lcSubject,lcCC,lcAttach,lcBodyText
		WITH THIS
			IF .lTestInvoice THEN
			
				WAIT WINDOW "Test invoice mode - NOT FTPing invoice data..." TIMEOUT 5	
						
			ELSE

				lcOutputFile = "F:\UTIL\MLAUTOBILLING\FTP\SHC_INVDATA_" + DTOS(tdInvoiceDate) + ".CSV"

				lcSendTo = 'doug@dhwachs.com'
				lcFrom = 'mark.bennett@tollgroup.com'
				lcSubject = 'Summary of Sears Invoicing'
				lcCC = 'mark.bennett@tollgroup.com'
				lcAttach = lcOutputFile
				lcBodyText = 'See attached CSV file for details of the current invoices.'			
				
				IF USED('CURINVDATA') THEN
					USE IN CURINVDATA
				ENDIF
				
				USE F:\SEARS\PMDATA\FTHISTORY AGAIN IN 0 ALIAS FTHISTORY
				
				SELECT * ;
					FROM FTHISTORY ;
					INTO CURSOR CURINVDATA ;
					WHERE INVDATE = tdInvoiceDate ;
					AND TNODE IN ('SR','KM') ;
					ORDER BY CLIENT, TRANSDATE
				
				*SELECT CURINVDATA
				*BROW	
				
				SELECT CURINVDATA
				COPY TO (lcOutputFile) TYPE CSV
				
				IF USED('CURINVDATA') THEN
					USE IN CURINVDATA
				ENDIF
				IF USED('FTHISTORY') THEN
					USE IN FTHISTORY
				ENDIF
				
				DO FORM dartmail2 WITH lcSendTo,lcFrom,lcSubject,lcCC,lcAttach,lcBodyText,"A"
				
				WAIT WINDOW "Emailed invoice data..." TIMEOUT 5	
						
			ENDIF	
		ENDWITH
	ENDPROC

ENDDEFINE


*!*	** 1/5/2017 query for Jon Bonny
*!*	SELECT ;
*!*	TNODE, ;
*!*	MONTH(TRANSDATE) AS MONTH, ;
*!*	YEAR(TRANSDATE) AS YEAR, ;
*!*	SUM(FCASECTNS) AS FCASECTNS, ;
*!*	SUM(TILTUNITS) AS TILTUNITS, ;
*!*	SUM(MANUNITS) AS MANUNITS, ;
*!*	SUM(RXDOCKCTNS) AS RXDOCKCTNS, ;
*!*	SUM(SXDOCKCTNS) AS SXDOCKCTNS ;
*!*	FROM F:\SEARS\PMDATA\FTHISTORY.DBF ;
*!*	INTO CURSOR CURTEMP ;
*!*	WHERE TNODE IN ('SR','KM') ;
*!*	AND (NOT LTEST) ;
*!*	AND BETWEEN(TRANSDATE,{^2016-01-01},{^2016-12-31}) ;
*!*	GROUP BY 1, 2, 3 ;
*!*	ORDER BY 1, 2

*!*	SELECT CURTEMP
*!*	COPY TO C:\A\SHC_2016_DATA.XLS XL5

*!*	** QUERY FOR JON BONNY / MIKE DESAYE
*SELECT *, MONTH(TRANSDATE) AS TMONTH, YEAR(TRANSDATE) AS TYEAR ;

*!*	SELECT * ;
*!*	FROM F:\SEARS\PMDATA\FTHISTORY.DBF ;
*!*	INTO CURSOR TEMP ;
*!*	WHERE TNODE IN ('SR','KM') ;
*!*	AND (NOT LTEST) ;
*!*	AND BETWEEN(TRANSDATE,{^2015-01-01},{^2016-09-15}) ;
*!*	ORDER BY TRANSDATE, TNODE

*!*	SELECT TEMP
*!*	COPY TO c:\a\shc_data_2015-2016.xls XL5

*!*	close data all


*!*	BROWSE

*!*	SELECT TYEAR, TMONTH, ;
*!*	SUM(RXDOCKCTNS) AS RXDOCKCTNS, ;
*!*	SUM(SXDOCKCTNS) AS SXDOCKCTNS, ;
*!*	SUM(RNXDCTNS) AS RNXDCTNS, ;
*!*	SUM(TILTUNITS) AS TILTUNITS, ;
*!*	SUM(FCASECTNS) AS FCASECTNS ;
*!*	FROM TEMP ;
*!*	INTO CURSOR TEMP2 ;
*!*	GROUP BY TYEAR, TMONTH ;
*!*	ORDER BY TYEAR, TMONTH

*!*	SELECT TEMP2
*!*	COPY TO c:\a\shc_data_20150929.xls XL5

*!*	*** QUERIES FOR RICHIE NAZZARO
*!*	SELECT * FROM FTHISTORY WHERE (NOT LTEST) AND TNODE IN ('SR','KM') AND YEAR(INVDATE) IN (2013,2014,2015) ORDER BY INVDATE, TRANSDATE


*!*	** code to retrieve sql invoice pdfs

*!*	xsqlexec("select * from invpdf.dbo.invpdf where invoiceid=333","xpdf",,"invpdf")
*!*	copy memo pdf to c:\a\testpdf.pdf


* get shc invoice history for Mike DeSaye and Jon Bonny
* USE F:\WATUSI\ARDATA\invoice ALIAS vinvoice IN 0
* USE F:\WATUSI\ARDATA\invdet ALIAS vinvdet IN 0

* LEFT(HEADER,200) AS HDR 

*!*	SELECT A.INVNUM, A.INVDT, A.INVAMT, LEFT(A.HEADER,50) AS HDR, B.* ;
*!*	FROM F:\WATUSI\ARDATA\invoice A ;
*!*	INNER JOIN F:\WATUSI\ARDATA\invdet B ;
*!*	ON B.INVOICEID = A.INVOICEID ;
*!*	INTO CURSOR CURSHCINFO ;
*!*	WHERE A.AccountID = 5837 ;
*!*	AND BETWEEN(A.INVDT,{^2015-01-01},{^2016-09-14}) ;
*!*	ORDER BY A.INVDT, A.INVNUM

*!*	SELECT CURSHCINFO
*!*	COPY TO C:\A\SHC_INVOICES_2015-2016.XLS XL5


