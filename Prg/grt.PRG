lparameters xcstype, xtw, xdate, xotherfilter, xrateheadid

xtw=iif(xtw="T","TRANSPORTATION","WAREHOUSING")
xotherfilter=iif(empty(xotherfilter),".t.",xotherfilter)
xrate=0

if !used("grthead")
	xsqlexec("select * from grthead",,,"ar")
endif
if !used("grt")
	xsqlexec("select * from grt",,,"ar")
endif
if !used("contract")
	xsqlexec("select * from contract",,,"ar")
endif

select contract
locate for rateheadid=xrateheadid and between(xdate,startdt,enddt)
if found()
	select * from grthead where startdt<contract.startdt and type=xtw order by startdt descending into cursor xgrthead
	select grthead
	locate for grtheadid=xgrthead.grtheadid
else
	select grthead
	locate for type=xtw and (emptynul(enddt) or xdate<=enddt) and xdate>=startdt
endif

select grt
locate for grtheadid=grthead.grtheadid and cstype=xcstype and &xotherfilter
if found()
	if grt.chgamt#0
		xrate=grt.chgamt
	else
		xrate=grt.chgpct
	endif
endif

return xrate