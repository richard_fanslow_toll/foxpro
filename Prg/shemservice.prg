* Report all trucks which have not had Emissions service (i.e., a WO with mainttype = E) within last 90 days.

PUBLIC gHeader
gHeader = "TGF"

LOCAL ldToday, ldElevenMonthsAgo, ldLatestServiceDate, llHadService, lnCurrentMileage, lcComment, lnDays, lnDaysInElevenMonths

ldToday = DATE()
ldElevenMonthsAgo = GOMONTH(ldToday,-11)
lnDaysInElevenMonths = ldToday - ldElevenMonthsAgo

lcComment = ''

IF USED('curEMService') THEN
	USE IN curEMService
ENDIF
IF USED('curEMServiceFinal') THEN
	USE IN curEMServiceFinal
ENDIF

IF NOT USED('ORDHDR') THEN
	USE SH!ORDHDR IN 0 ALIAS ORDHDR
ENDIF
SELECT ORDHDR
SET ORDER TO CRDATE

IF NOT USED('TRUCKS') THEN
	USE SH!TRUCKS IN 0 ALIAS TRUCKS
ENDIF

** new code 9/5/07 MB
WAIT WINDOW "Checking Trucks...." NOWAIT

SELECT "TRUCK" AS VEHTYPE, ;
	VEHNO, ;
	SPACE(50) AS COMMENT, ;
	SPACE(10) AS PADVEHNO, ;
	0000000 AS NDAYS, ;
	0000000 AS NDAYSDESC, ;
	MAX(CRDATE) AS LASTSVCE ;
	FROM ORDHDR ;
	WHERE INLIST(UPPER(MAINTTYPE),'E') AND (TYPE = 'TRUCK') ;
	AND VEHNO IN ( SELECT TRUCK_NUM AS VEHNO FROM TRUCKS WHERE ACTIVE ) ;
	AND NOT EMPTY(VEHNO) ;
	GROUP BY 1,2,3,4,5,6 ;
	UNION ALL ;
	SELECT "TRUCK" AS VEHTYPE, ;
	TRUCK_NUM AS VEHNO, ;
	SPACE(50) AS COMMENT, ;
	SPACE(10) AS PADVEHNO, ;
	0000000 AS NDAYS, ;
	0000000 AS NDAYSDESC, ;
	{} AS LASTSVCE ;
	FROM TRUCKS ;
	WHERE ACTIVE ;
	AND NOT EMPTY(TRUCK_NUM) ;
	AND ( TRUCK_NUM NOT IN ( SELECT VEHNO AS TRUCK_NUM FROM ORDHDR WHERE INLIST(UPPER(MAINTTYPE),'E') AND (TYPE = 'TRUCK') ) ) ;
	INTO CURSOR curEMService ;
	READWRITE ;

SELECT curEMService
SCAN
	IF EMPTY(curEMService.LASTSVCE) THEN
		lnDays = 999999
		lcComment = "Never had Emissions service"
	ELSE
		lnDays = ldToday - curEMService.LASTSVCE
		lcComment = TRANSFORM(lnDays) + " days since last Emissions service"
	ENDIF
	REPLACE curEMService.NDAYS WITH lnDays, ;
		curEMService.NDAYSDESC WITH (1000000 - lnDays), ;
		curEMService.COMMENT WITH lcComment, ;
		curEMService.PADVEHNO WITH PADL(ALLTRIM(curEMService.VEHNO),10,"0")
ENDSCAN

*prepare & run report
SELECT * ;
	FROM curEMService ;
	INTO CURSOR curEMServiceFinal ;
	WHERE NDAYS > lnDaysInElevenMonths ;
	ORDER BY VEHTYPE, NDAYSDESC, PADVEHNO

WAIT CLEAR

IF USED('curEMServiceFinal') AND NOT EOF('curEMServiceFinal') THEN
	SELECT curEMServiceFinal
	GOTO TOP
	*BROWSE
	KEYBOARD "{ctrl-f10}"
	REPORT FORM SHEMSERVICE PREVIEW NOCONSOLE
ELSE
	X3WINMSG("There are no Trucks which have not had Emissions service within last 90 days.")
ENDIF

IF USED('curEMService') THEN
	USE IN curEMService
ENDIF

IF USED('curEMServiceFinal') THEN
	USE IN curEMServiceFinal
ENDIF
