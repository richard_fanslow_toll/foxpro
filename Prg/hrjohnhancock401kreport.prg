* run John Hancock 401k export report FOR LATEST ADP PAYDATE
* usually running on Wednesday for prior Friday

* build EXE as F:\UTIL\JOHNHANCOCK\HRJOHNHANCOCK401KREPORT.EXE

LPARAMETERS tcMode

utilsetup("HRJOHNHANCOCK401KREPORT")

LOCAL loJohnHancock401k
loJohnHancock401k = CREATEOBJECT('JohnHancock401k')
loJohnHancock401k.MAIN( tcMode )

schedupdate()

CLOSE DATABASES ALL
RETURN

#DEFINE LOGIT 1
#DEFINE WAITIT 2
#DEFINE NOWAITIT 4
#DEFINE SENDIT 8
#DEFINE CRLF CHR(13) + CHR(10)
#DEFINE RETURN_DATA_MANDATORY .F.
#DEFINE RETURN_DATA_NOT_MANDATORY .T.
#DEFINE CSVFORMAT 6
#DEFINE WRITE_ONLY_UNBUFFERED 11
#DEFINE COMMA ","

DEFINE CLASS JohnHancock401k AS CUSTOM

	cProcessName = 'JohnHancock401k'

	lTestMode = .F.  && .F. FOR NORMAL OPERATION, .T. TO SUPPRESS ALL BUT DEVELOPER EMAILS AND CHANGE INPUT TABLE, ETC.

	* special processing properties
	cMode = ''  && FF or SCS
	**********************************************
	lUseDateRange = .F.
	* the following only apply if above prop is .T.
	cRangeStartDate = "2012-03-01"
	cRangeEndDate   = "2012-03-31"
	**********************************************
	lGetMissingDemoGraphicsFromFoxPro = .T.

	dToday = DATE()
	* forcing to a particular date will make report think that is the run date -- remember it should be AFTER the paydate you want to report on
	dToday = {^2013-12-31}

	lAutoYield = .T.

	cStartTime = TTOC(DATETIME())

	* deduction properties
	cDedCode = ''

	* connection properties
	nSQLHandle = 0

	* output file properties
	nCurrentOutputCSVFileHandle = -1

	* wait window properties
	nWaitWindowTimeout = 2
	lWaitWindowIsOn = .T.

	* logfile properties
	lLoggingIsOn = .T.
	cLogFile = 'F:\UTIL\JOHNHANCOCK\LOGFILES\JOHN_HANCOCK_401K_log.txt'

	* Deds K/O/N properties
	lGetOnlyLatestPaydate = .T.

	* INTERNAL email properties
	lSendInternalEmailIsOn = .T.
	cFrom = 'mark.bennett@tollgroup.com'
	cSendTo = 'Marie.Freiberger@Tollgroup.com'
	cCC = 'george.gereis@tollgroup.com, mark.bennett@tollgroup.com, lauren.klaver@Tollgroup.com'
	cSubject = 'John Hancock 401k CSV File'
	cAttach = ''
	cBodyText = ''

	cCOMPUTERNAME = ''
	cUSERNAME = ''


	FUNCTION INIT
		IF NOT DODEFAULT()
			RETURN .F.
		ENDIF
		WITH THIS
			*SET RESOURCE OFF
			CLOSE DATA
			SET CENTURY ON
			SET DATE AMERICAN
			SET HOURS TO 24
			SET ANSI ON
			SET TALK OFF
			SET DELETED ON
			SET CONSOLE OFF
			SET EXCLUSIVE OFF
			SET SAFETY OFF
			SET EXACT OFF
			SET STATUS BAR ON
			SET SYSMENU OFF
			SET ENGINEBEHAVIOR 70
			_VFP.AUTOYIELD = .lAutoYield
			.cCOMPUTERNAME = GETENV("COMPUTERNAME")
			.cUSERNAME = GETENV("USERNAME")
			IF .lTestMode THEN
				.cLogFile = 'F:\UTIL\JOHNHANCOCK\LOGFILES\JOHN_HANCOCK_401K_log_TESTMODE.txt'
				.cSendTo = 'Mark Bennett <mbennett@fmiint.com>'
				.cCC = ''
			ENDIF
			.lLoggingIsOn = .lLoggingIsOn AND NOT EMPTY(.cLogFile)
		ENDWITH
	ENDFUNC


	FUNCTION DESTROY
		WITH THIS
			IF .lLoggingIsOn  THEN
				SET ALTERNATE OFF
				SET ALTERNATE TO
			ENDIF
		ENDWITH
		DODEFAULT()
	ENDFUNC


	FUNCTION MAIN
		LPARAMETERS tcMode
		WITH THIS
			LOCAL lnNumberOfErrors, lcSQL, lcSQL2, lcSQL3, loError, lnPayMonth, lnPayYear, lnPayDay
			LOCAL ldEndingPaydate, lcFiletoSaveAs, lcSpreadsheetTemplate
			LOCAL oExcel, oWorkbook, oWorksheet
			LOCAL lcOutputFilename, lcOutputLine
			LOCAL lcTransactionNumber, lcSSN, lcDivNum, lcLast, lcFirst, lcMiddle, lcSuffix, lcBirthDate, lcGender
			LOCAL lcMarital, lcAddress1, lcAddress2, lcCity, lcState, lcZip, lcHomePhone, lcWorkPhone, lcWorkPhoneExt, lcCountryCode
			LOCAL ldHireDate, lcTermDate, lcRehireDate, lcEEContribution, lcEmployerMatch, lcLoanRepayment, lcYTDHoursWorked, lcYTDTotCompensation, lcYTDPlanComp
			LOCAL lcPreContrib, lcHighComp, lcBlanks, lcOfficer, lcPartDate, lcEligibleCode, lcContractNumber, lcName, lcROTHAmt
			LOCAL lcLoanID, lcHeaderLine, lcPayDate, lnTot401k, lcWhere1

			TRY
				lnNumberOfErrors = 0

				IF .lLoggingIsOn THEN
					SET ALTERNATE TO (.cLogFile) ADDITIVE
					SET ALTERNATE ON
				ENDIF

				.TrackProgress('', LOGIT)
				.TrackProgress('', LOGIT)
				.TrackProgress('=========================================================', LOGIT+SENDIT)
				.TrackProgress('John Hancock 401k CSV File process started....', LOGIT+SENDIT+NOWAITIT)
				.TrackProgress('COMPUTERNAME = ' + .cCOMPUTERNAME, LOGIT+SENDIT)
				.TrackProgress('USERNAME = ' + .cUSERNAME, LOGIT+SENDIT)
				.TrackProgress('PROJECT = HRJOHNHANCOCK401KREPORT', LOGIT+SENDIT)
				
				DO CASE
					CASE EMPTY(tcMode) OR (NOT INLIST(UPPER(ALLTRIM(tcMode)),'DEDS-4-21-O-P','DEDS-K-N-R'))
						.TrackProgress('Invalid or missing tcMode!', LOGIT)
						.cSendTo = 'Mark Bennett <mbennett@fmiint.com>'
						.cCC = ''
						THROW
					OTHERWISE
						.cMode = UPPER(ALLTRIM(tcMode))				
						.TrackProgress('tcMode = ' + tcMode, LOGIT+SENDIT)
				ENDCASE
				
				DO CASE
					CASE .cMode = 'DEDS-4-21-O-P'
						lcWhere1 = "'4','21','O','P'"
					CASE .cMode = 'DEDS-K-N-R'
						lcWhere1 = "'K','N','R'"
					OTHERWISE
						* nothing
				ENDCASE
				
				IF .lTestMode THEN
					.TrackProgress('                        TEST MODE                        ', LOGIT+SENDIT)
				ENDIF
				IF .lUseDateRange THEN
					.TrackProgress('                     DATE-RANGE MODE                     ', LOGIT+SENDIT)
				ENDIF
				.TrackProgress('', LOGIT)

				* okay to proceed.
				*!*					lnPayMonth = MONTH(.dToday)
				*!*					lnPayYear = YEAR(.dToday)
				*!*					lnPayDay = DAY(.dToday)

				* determine most recent Friday (i.e. payday) date
				ldEndingPaydate = .dToday
				DO WHILE DOW(ldEndingPaydate) <> 6
					ldEndingPaydate = ldEndingPaydate - 1
				ENDDO

*!*	*!*	* Hardcode to handle early pay dates
*!*	ldEndingPaydate = {^2013-12-31}

				* construct month/year values for SELECT
				lnPayMonth = MONTH(ldEndingPaydate)
				lnPayYear = YEAR(ldEndingPaydate)
				lnPayDay = DAY(ldEndingPaydate)

				lcSQLPayDate = PADL(lnPayYear,4,"0") + "-" + PADL(lnPayMonth,2,"0") + "-" + PADL(lnPayDay,2,"0")

				* get ss#s from ADP
				OPEN DATABASE F:\UTIL\ADPCONNECTION\CONNECT1

				.nSQLHandle = SQLCONNECT("PCPAYWIN","MBENNETT","VP737A")

				IF .nSQLHandle > 0 THEN

					*!*							" AND (CHECKVIEWPAYDATE >= DATE '2005-01-01') " + ;
					*!*							" AND (CHECKVIEWPAYDATE <= DATE '2005-10-21') "
					
*!*						IF .lUseDateRange THEN
*!*							lcSQL = ;
*!*								"SELECT " + ;
*!*								" COMPANYCODE AS ADPCOMP, " + ;
*!*								" NAME, " + ;
*!*								" SOCIALSECURITY# AS SS_NUM, " + ;
*!*								" CHECKVIEWPAYDATE AS PAYDATE, " + ;
*!*								" CHECKVIEWDEDCODE AS DEDCODE, " + ;
*!*								" CHECKVIEWDEDAMT AS DEDAMT, " + ;
*!*								" FILE# AS FILENUM " + ;
*!*								" FROM REPORTS.V_CHK_VW_DEDUCTION " + ;
*!*								" WHERE ((CHECKVIEWDEDCODE = 'K') OR (CHECKVIEWDEDCODE = 'N') OR (CHECKVIEWDEDCODE = 'R')) " + ;
*!*								" AND (CHECKVIEWPAYDATE >= DATE '" + .cRangeStartDate + "') " + ;
*!*								" AND (CHECKVIEWPAYDATE <= DATE '" + .cRangeEndDate + "') "
*!*						ELSE
*!*							lcSQL = ;
*!*								"SELECT " + ;
*!*								" COMPANYCODE AS ADPCOMP, " + ;
*!*								" NAME, " + ;
*!*								" SOCIALSECURITY# AS SS_NUM, " + ;
*!*								" CHECKVIEWPAYDATE AS PAYDATE, " + ;
*!*								" CHECKVIEWDEDCODE AS DEDCODE, " + ;
*!*								" CHECKVIEWDEDAMT AS DEDAMT, " + ;
*!*								" FILE# AS FILENUM " + ;
*!*								" FROM REPORTS.V_CHK_VW_DEDUCTION " + ;
*!*								" WHERE ((CHECKVIEWDEDCODE = 'K') OR (CHECKVIEWDEDCODE = 'N') OR (CHECKVIEWDEDCODE = 'R')) " + ;
*!*								" AND (CHECKVIEWPAYDATE = DATE '" + lcSQLPayDate + "') "
*!*						ENDIF

					IF .lUseDateRange THEN
						lcSQL = ;
							"SELECT " + ;
							" COMPANYCODE AS ADPCOMP, " + ;
							" NAME, " + ;
							" SOCIALSECURITY# AS SS_NUM, " + ;
							" CHECKVIEWPAYDATE AS PAYDATE, " + ;
							" CHECKVIEWDEDCODE AS DEDCODE, " + ;
							" CHECKVIEWDEDAMT AS DEDAMT, " + ;
							" FILE# AS FILENUM " + ;
							" FROM REPORTS.V_CHK_VW_DEDUCTION " + ;
							" WHERE (CHECKVIEWPAYDATE >= DATE '" + .cRangeStartDate + "') " + ;
							" AND (CHECKVIEWPAYDATE <= DATE '" + .cRangeEndDate + "') " + ;
							" AND (CHECKVIEWDEDCODE IN (" + lcWhere1 + ")) " + ;
							" ORDER BY NAME "
					ELSE
						lcSQL = ;
							"SELECT " + ;
							" COMPANYCODE AS ADPCOMP, " + ;
							" NAME, " + ;
							" SOCIALSECURITY# AS SS_NUM, " + ;
							" CHECKVIEWPAYDATE AS PAYDATE, " + ;
							" CHECKVIEWDEDCODE AS DEDCODE, " + ;
							" CHECKVIEWDEDAMT AS DEDAMT, " + ;
							" FILE# AS FILENUM " + ;
							" FROM REPORTS.V_CHK_VW_DEDUCTION " + ;
							" WHERE (CHECKVIEWPAYDATE = DATE '" + lcSQLPayDate + "') " + ;
							" AND (CHECKVIEWDEDCODE IN (" + lcWhere1 + ")) " + ;
							" ORDER BY NAME " 
					ENDIF

					*!*								" AND {fn MONTH(CHECKVIEWPAYDATE)} = " + TRANSFORM(lnPayMonth) + ;
					*!*								" AND {fn YEAR(CHECKVIEWPAYDATE)} = " + TRANSFORM(lnPayYear)

					lcSQL2 = ;
						"SELECT " + ;
						" NAME, " + ;
						" CUSTAREA3 AS WORKSITE, " + ;
						" LASTNAME, " + ;
						" FIRSTNAME, " + ;
						" SOCIALSECURITY# AS SS_NUM, " + ;
						" HOMEDEPARTMENT AS HOMEDEPT, " + ;
						" STREETLINE1 AS STREET1, " + ;
						" {fn IFNULL(STREETLINE2,' ')} AS STREET2, " + ;
						" CITY, " + ;
						" STATE, " + ;
						" ZIPCODE, " + ;
						" GENDER, " + ;
						" HIREDATE, " + ;
						" BIRTHDATE, " + ;
						" TERMINATIONDATE AS TERMDATE, " + ;
						" {FN IFNULL(ANNUALSALARY,0000000.00)} AS SALARY, " + ;
						" STATUS " + ;
						" FROM REPORTS.V_EMPLOYEE "

					IF .lUseDateRange THEN
						lcSQL3 = ;
							"SELECT " + ;
							" SOCIALSECURITY# AS SS_NUM, " + ;
							" CHECKVIEWPAYDATE AS PAYDATE, " + ;
							" {FN IFNULL(CHECKVIEWMEMOAMT,0.00)} AS MATCH_AMT " + ;
							" FROM REPORTS.V_CHK_VW_MEMO " + ;
							" WHERE (CHECKVIEWMEMOCD = 'K') " + ;
							" AND (CHECKVIEWPAYDATE >= DATE '" + .cRangeStartDate + "') " + ;
							" AND (CHECKVIEWPAYDATE <= DATE '" + .cRangeEndDate + "') "
					ELSE
						lcSQL3 = ;
							"SELECT " + ;
							" SOCIALSECURITY# AS SS_NUM, " + ;
							" CHECKVIEWPAYDATE AS PAYDATE, " + ;
							" {FN IFNULL(CHECKVIEWMEMOAMT,0.00)} AS MATCH_AMT " + ;
							" FROM REPORTS.V_CHK_VW_MEMO " + ;
							" WHERE (CHECKVIEWMEMOCD = 'K') " + ;
							" AND (CHECKVIEWPAYDATE = DATE '" + lcSQLPayDate + "') "
					ENDIF

					* COUNT # OF PAY WEEKS FOR SALARIED EE'S,
					* WILL BE USED TO CALCULATE TOTAL YTD HOURS
					IF .lUseDateRange THEN
						lcSQL4 = ;
							"SELECT " + ;
							" SOCIALSECURITY# AS SS_NUM, " + ;
							" COUNT(DISTINCT CHECKVIEWWEEK#) AS NUM_WEEKS " + ;
							" FROM REPORTS.V_CHK_VW_INFO " + ;
							" WHERE COMPANYCODE NOT IN ('SXI','E87') " + ;
							" AND (CHECKVIEWPAYDATE >= DATE '" + .cRangeStartDate + "') " + ;
							" AND (CHECKVIEWPAYDATE <= DATE '" + .cRangeEndDate + "') " + ;
							" GROUP BY SOCIALSECURITY# "
					ELSE
						lcSQL4 = ;
							"SELECT " + ;
							" SOCIALSECURITY# AS SS_NUM, " + ;
							" COUNT(DISTINCT CHECKVIEWWEEK#) AS NUM_WEEKS " + ;
							" FROM REPORTS.V_CHK_VW_INFO " + ;
							" WHERE {fn YEAR(CHECKVIEWPAYDATE)} = " + TRANSFORM(YEAR(ldEndingPaydate)) + ;
							" AND COMPANYCODE NOT IN ('SXI','E87') " + ;
							" GROUP BY SOCIALSECURITY# "
					ENDIF

					* count hours of various types for Hourly
					IF .lUseDateRange THEN
						lcSQL5 = ;
							"SELECT " + ;
							" SOCIALSECURITY# AS SS_NUM, " + ;
							" SUM({fn IFNULL(CHECKVIEWREGHOURS,0000.00)}) AS REG_HOURS, " + ;
							" SUM({fn IFNULL(CHECKVIEWOTHOURS,0000.00)}) AS OT_HOURS, " + ;
							" SUM({fn IFNULL(CHECKVIEWHOURSAMT,0000.00)}) AS OTHER_HRS " + ;
							" FROM REPORTS.V_CHK_VW_HOURS " + ;
							" WHERE COMPANYCODE IN ('SXI','E87') " + ;
							" AND (CHECKVIEWPAYDATE >= DATE '" + .cRangeStartDate + "') " + ;
							" AND (CHECKVIEWPAYDATE <= DATE '" + .cRangeEndDate + "') " + ;
							" GROUP BY SOCIALSECURITY# "
					ELSE
						lcSQL5 = ;
							"SELECT " + ;
							" SOCIALSECURITY# AS SS_NUM, " + ;
							" SUM({fn IFNULL(CHECKVIEWREGHOURS,0000.00)}) AS REG_HOURS, " + ;
							" SUM({fn IFNULL(CHECKVIEWOTHOURS,0000.00)}) AS OT_HOURS, " + ;
							" SUM({fn IFNULL(CHECKVIEWHOURSAMT,0000.00)}) AS OTHER_HRS " + ;
							" FROM REPORTS.V_CHK_VW_HOURS " + ;
							" WHERE {fn YEAR(CHECKVIEWPAYDATE)} = " + TRANSFORM(YEAR(ldEndingPaydate)) + ;
							" AND COMPANYCODE IN ('SXI','E87') " + ;
							" GROUP BY SOCIALSECURITY# "
					ENDIF

					IF .lUseDateRange THEN
						lcSQL6 = ;
							"SELECT " + ;
							" SOCIALSECURITY# AS SS_NUM, " + ;
							" SUM(CHECKVIEWGROSSPAYA) AS GROSSPAY, " + ;
							" COUNT(*) AS NUM_PAYS " + ;
							" FROM REPORTS.V_CHK_VW_INFO " + ;
							" WHERE (CHECKVIEWPAYDATE >= DATE '" + .cRangeStartDate + "') " + ;
							" AND (CHECKVIEWPAYDATE <= DATE '" + .cRangeEndDate + "') " + ;
							" GROUP BY SOCIALSECURITY# "
					ELSE
						lcSQL6 = ;
							"SELECT " + ;
							" SOCIALSECURITY# AS SS_NUM, " + ;
							" SUM(CHECKVIEWGROSSPAYA) AS GROSSPAY, " + ;
							" COUNT(*) AS NUM_PAYS " + ;
							" FROM REPORTS.V_CHK_VW_INFO " + ;
							" WHERE {fn YEAR(CHECKVIEWPAYDATE)} = " + TRANSFORM(YEAR(ldEndingPaydate)) + ;
							" GROUP BY SOCIALSECURITY# "
					ENDIF

					IF .lTestMode THEN
						.TrackProgress('                        TEST MODE                        ', LOGIT+SENDIT)
						.TrackProgress('lcSQL =' + lcSQL, LOGIT+SENDIT)
					ENDIF

					IF USED('SQLCURSOR1') THEN
						USE IN SQLCURSOR1
					ENDIF
					IF USED('CUREMPLOYEES') THEN
						USE IN CUREMPLOYEES
					ENDIF
					IF USED('CURKMATCH') THEN
						USE IN CURKMATCH
					ENDIF
					IF USED('CURYTDSALARY') THEN
						USE IN CURYTDSALARY
					ENDIF
					IF USED('CURYTDHOURLY') THEN
						USE IN CURYTDHOURLY
					ENDIF
					IF USED('CURYTDPAY') THEN
						USE IN CURYTDPAY
					ENDIF

					IF .ExecSQL(lcSQL, 'SQLCURSOR1', RETURN_DATA_MANDATORY) ;
							AND .ExecSQL(lcSQL2, 'CUREMPLOYEES', RETURN_DATA_MANDATORY) ;
							AND .ExecSQL(lcSQL3, 'CURKMATCH', RETURN_DATA_MANDATORY) ;
							AND .ExecSQL(lcSQL4, 'CURYTDSALARY', RETURN_DATA_NOT_MANDATORY) ;
							AND .ExecSQL(lcSQL5, 'CURYTDHOURLY', RETURN_DATA_MANDATORY) ;
							AND .ExecSQL(lcSQL6, 'CURYTDPAY', RETURN_DATA_MANDATORY) THEN

*!*	SELECT SQLCURSOR1
*!*	BROWSE

						IF USED('SQLCURSOR2') THEN
							USE IN SQLCURSOR2
						ENDIF

						* populate nulls
						SELECT CUREMPLOYEES
						SCAN
							IF ISNULL(CUREMPLOYEES.BIRTHDATE) THEN
								REPLACE CUREMPLOYEES.BIRTHDATE WITH {}
							ENDIF
							IF ISNULL(CUREMPLOYEES.HIREDATE) THEN
								REPLACE CUREMPLOYEES.HIREDATE WITH {}
							ENDIF
						ENDSCAN
						LOCATE

						** setup file names

						*lcSpreadsheetTemplate = "M:\MarkB\templates\JOHNHANCOCK401KREPORT_TEMPLATE.XLS"

						IF .lUseDateRange THEN
							*lcFiletoSaveAs = "F:\UTIL\JOHNHANCOCK\REPORTS\MetLife PDI File for " + .cRangeStartDate + " thru " + .cRangeEndDate + ".XLS"
							.cSubject = .cMode + ' John Hancock 401k CSV File, for Date Range: ' + .cRangeStartDate + ' thru ' + .cRangeEndDate
						ELSE
							*lcFiletoSaveAs = "F:\UTIL\JOHNHANCOCK\REPORTS\MetLife PDI File for " + STRTRAN(DTOC(ldEndingPaydate),"/","-") + ".XLS"
							.cSubject = .cMode + ' John Hancock 401k CSV File, for Pay Date: ' + DTOC(ldEndingPaydate)
						ENDIF

*!*							IF FILE(lcFiletoSaveAs) THEN
*!*								DELETE FILE (lcFiletoSaveAs)
*!*							ENDIF


						* sum deductions by ss_num, date, paycode for latest date
						* (summing only because their *might* conceivably be more than one check per date,
						* and we don't want to miss anything in later lookups)
						IF USED('SQLCURSOR2') THEN
							USE IN SQLCURSOR2
						ENDIF

						SELECT ;
							SS_NUM, ;
							DEDCODE, ;
							SUM(DEDAMT) AS DEDAMT ;
							FROM SQLCURSOR1 ;
							INTO CURSOR SQLCURSOR2 ;
							GROUP BY 1, 2


						* roll up matches for latest paydate
						IF USED('CURKMATCH2') THEN
							USE IN CURKMATCH2
						ENDIF

						SELECT ;
							SS_NUM, ;
							SUM(MATCH_AMT) AS MATCH_AMT ;
							FROM CURKMATCH ;
							INTO CURSOR CURKMATCH2 ;
							GROUP BY 1

						*!*		SELECT CURKMATCH2
						*!*		BROWSE


						*!*							SELECT SQLCURSOR2
						*!*							GOTO TOP
						*!*							BROWSE

						* now create main report cursor of distinct ss#s from sqlcursor1
						IF USED('CURJH') THEN
							USE IN CURJH
						ENDIF

						SELECT ;
							SS_NUM, ;
							ADPCOMP, ;
							SPACE(30) AS NAME, ;
							SPACE(3) AS WORKSITE, ;
							SPACE(20) AS LASTNAME, ;
							SPACE(20) AS FIRSTNAME, ;
							0000.00 AS AMT_401k, ;
							0000.00 AS TEST_401k, ;
							0000.00 AS MATCH_AMT, ;
							0000.00 AS LOAN_AMT, ;
							0000.00 AS ROTH_AMT, ;
							00000000000.00 AS YTDPAY, ;
							00000000000.00 AS SALARY, ;
							000000 AS YTDHOURS, ;
							SPACE(2) AS DIVISION, ;
							SPACE(30) AS STREET1, ;
							SPACE(30) AS STREET2, ;
							SPACE(20) AS CITY, ;
							SPACE(2) AS STATE, ;
							SPACE(10) AS ZIPCODE, ;
							SPACE(1) AS GENDER, ;
							SPACE(1) AS STATUS, ;
							{} AS HIREDATE, ;
							{} AS BIRTHDATE, ;
							{} AS TERMDATE ;
							FROM SQLCURSOR1 ;
							INTO CURSOR CURJH ;
							GROUP BY SS_NUM ;
							READWRITE

						SELECT CURJH
						SCAN
							* get 401K DED amount
							*!*	SELECT SQLCURSOR2
							*!*	LOCATE FOR (SS_NUM = CURJH.SS_NUM) AND (DEDCODE = 'K')
							*!*	IF FOUND() THEN
							*!*		REPLACE CURJH.AMT_401k WITH SQLCURSOR2.DEDAMT
							*!*	ENDIF
							*!*	LOCATE
							SELECT SQLCURSOR2
							SUM DEDAMT FOR (SS_NUM = CURJH.SS_NUM) AND INLIST(DEDCODE,'4','21','K') TO lnTot401k  && EXCLUDING ROTH HERE
							LOCATE
							REPLACE CURJH.AMT_401k WITH lnTot401k IN CURJH

							* get 401k Match amount
							SELECT CURKMATCH2
							LOCATE FOR (SS_NUM = CURJH.SS_NUM)
							IF FOUND() THEN
								REPLACE CURJH.MATCH_AMT WITH CURKMATCH2.MATCH_AMT
							ENDIF
							LOCATE


							* get Ded N amount (401k loan repayment)
							*!*	SELECT SQLCURSOR2
							*!*	LOCATE FOR (SS_NUM = CURJH.SS_NUM) AND (DEDCODE = 'N')
							*!*	IF FOUND() THEN
							*!*		REPLACE CURJH.LOAN_AMT WITH SQLCURSOR2.DEDAMT
							*!*	ENDIF
							*!*	LOCATE							
							SELECT SQLCURSOR2
							SUM DEDAMT FOR (SS_NUM = CURJH.SS_NUM) AND INLIST(DEDCODE,'N','O','P') TO lnTotLoan  
							LOCATE
							REPLACE CURJH.LOAN_AMT WITH lnTotLoan IN CURJH
							

							* get Ded R amount (Roth IRA deduction)
							SELECT SQLCURSOR2
							LOCATE FOR (SS_NUM = CURJH.SS_NUM) AND (DEDCODE = 'R')
							IF FOUND() THEN
								REPLACE CURJH.ROTH_AMT WITH SQLCURSOR2.DEDAMT
							ENDIF
							LOCATE

							* GET YTD HOURS
							IF INLIST(CURJH.ADPCOMP,'SXI','E87') THEN
								* HOURLY
								SELECT CURYTDHOURLY
								LOCATE FOR (SS_NUM = CURJH.SS_NUM)
								IF FOUND() THEN
									REPLACE CURJH.YTDHOURS WITH CURYTDHOURLY.REG_HOURS + CURYTDHOURLY.OT_HOURS + CURYTDHOURLY.OTHER_HRS
								ENDIF
								LOCATE

							ELSE
								* SALARIED
								* NOTE: EARLY IN EACH YEAR, CURYTDSALARY MAY NOT EXIST
								IF USED('CURYTDSALARY') AND NOT EOF('CURYTDSALARY') THEN
									SELECT CURYTDSALARY
									LOCATE FOR (SS_NUM = CURJH.SS_NUM)
									IF FOUND() THEN
										REPLACE CURJH.YTDHOURS WITH (CURYTDSALARY.NUM_WEEKS * 80)
									ENDIF
									LOCATE
								ENDIF
							ENDIF

							* GET YTD PAY
							SELECT CURYTDPAY
							LOCATE FOR (SS_NUM = CURJH.SS_NUM)
							IF FOUND() THEN
								REPLACE CURJH.YTDPAY WITH CURYTDPAY.GROSSPAY
							ENDIF
							LOCATE

							* get additional info from employee table:
							SELECT CUREMPLOYEES
							LOCATE FOR (SS_NUM = CURJH.SS_NUM)
							IF FOUND() THEN
								REPLACE ;
									CURJH.NAME WITH CUREMPLOYEES.NAME, ;
									CURJH.WORKSITE WITH CUREMPLOYEES.WORKSITE, ;
									CURJH.LASTNAME  WITH CUREMPLOYEES.LASTNAME, ;
									CURJH.FIRSTNAME WITH CUREMPLOYEES.FIRSTNAME, ;
									CURJH.DIVISION WITH LEFT(CUREMPLOYEES.HOMEDEPT,2), ;
									CURJH.STREET1 WITH CUREMPLOYEES.STREET1, ;
									CURJH.STREET2 WITH CUREMPLOYEES.STREET2, ;
									CURJH.CITY WITH CUREMPLOYEES.CITY, ;
									CURJH.STATE WITH CUREMPLOYEES.STATE, ;
									CURJH.ZIPCODE WITH CUREMPLOYEES.ZIPCODE, ;
									CURJH.GENDER WITH CUREMPLOYEES.GENDER, ;
									CURJH.STATUS WITH CUREMPLOYEES.STATUS, ;
									CURJH.SALARY WITH CUREMPLOYEES.SALARY, ;
									CURJH.HIREDATE WITH TTOD(CUREMPLOYEES.HIREDATE), ;
									CURJH.BIRTHDATE WITH TTOD(CUREMPLOYEES.BIRTHDATE)

								IF NOT ISNULL(CUREMPLOYEES.TERMDATE) THEN
									REPLACE CURJH.TERMDATE WITH TTOD(CUREMPLOYEES.TERMDATE)
								ENDIF
							ENDIF
							LOCATE
							
						ENDSCAN
						
*!*	IF .lTestMode THEN
*!*		SELECT CURJH
*!*		BROWSE
*!*		THROW
*!*	ENDIF

						IF .lGetMissingDemoGraphicsFromFoxPro THEN
							* for old, purged ADP employees, get additional info from Foxpro employee table.
							IF USED('CURFOXEMPLOYEE') THEN
								USE IN CURFOXEMPLOYEE
							ENDIF
							
							SELECT ;
								ALLTRIM(TRANSFORM(DIVISION)) AS DIVISION, ;
								IIF(ACTIVE,"A","T") AS STATUS, ;
								EMPLOYEE AS NAME, ;
								STRTRAN(SS_NUM,"-","") AS SS_NUM, ;
								HIRE_DATE AS HIREDATE, ;
								TERM_DATE AS TERMDATE, ;
								ADDRESS AS STREET1, ;
								CITY, ;
								STATE, ;
								ZIP AS ZIPCODE, ;
								DOB AS BIRTHDATE, ;
								SEXTYPE AS GENDER ;
							FROM F:\HR\HRDATA\EMPLOYEE ;
							INTO CURSOR CURFOXEMPLOYEE ;
							ORDER BY 2,4
													
							SELECT CURJH
							SCAN FOR EMPTY(NAME)
								SELECT CURFOXEMPLOYEE
								GOTO TOP
								LOCATE FOR (SS_NUM = CURJH.SS_NUM)
								IF FOUND() THEN
									REPLACE ;
										CURJH.NAME WITH CURFOXEMPLOYEE.NAME, ;
										CURJH.WORKSITE WITH CUREMPLOYEES.WORKSITE, ;
										CURJH.DIVISION WITH CURFOXEMPLOYEE.DIVISION, ;
										CURJH.STREET1 WITH CURFOXEMPLOYEE.STREET1, ;
										CURJH.CITY WITH CURFOXEMPLOYEE.CITY, ;
										CURJH.STATE WITH CURFOXEMPLOYEE.STATE, ;
										CURJH.ZIPCODE WITH CURFOXEMPLOYEE.ZIPCODE, ;
										CURJH.GENDER WITH CURFOXEMPLOYEE.GENDER, ;
										CURJH.STATUS WITH CURFOXEMPLOYEE.STATUS, ;
										CURJH.HIREDATE WITH CURFOXEMPLOYEE.HIREDATE, ;
										CURJH.BIRTHDATE WITH CURFOXEMPLOYEE.BIRTHDATE

									IF NOT ISNULL(CURFOXEMPLOYEE.TERMDATE) THEN
										REPLACE CURJH.TERMDATE WITH CURFOXEMPLOYEE.TERMDATE
									ENDIF
								ENDIF
								LOCATE							
							ENDSCAN
						ENDIF


						*** SORT HERE !!!!
						IF USED('CURJHCSV') THEN
							USE IN CURJHCSV
						ENDIF
						SELECT * FROM CURJH INTO CURSOR CURJHCSV ORDER BY NAME

*!*							***********************************************************************************
*!*							***********************************************************************************
*!*							** output to Excel spreadsheet
*!*							.TrackProgress('Creating spreadsheet: ' + lcFiletoSaveAs, LOGIT+SENDIT+NOWAITIT)

*!*							oExcel = CREATEOBJECT("excel.application")
*!*							oExcel.VISIBLE = .F.
*!*							oWorkbook = oExcel.workbooks.OPEN(lcSpreadsheetTemplate)
*!*							oWorkbook.SAVEAS(lcFiletoSaveAs)

*!*							oWorksheet = oWorkbook.Worksheets[1]
*!*							oWorksheet.RANGE("A4","AT2000").ClearContents()

*!*							LOCAL lnStartRow, lnRow, lnEndRow, lnRowsProcessed, lcStartRow, lcEndRow

*!*							lnStartRow = 4
*!*							lcStartRow = ALLTRIM(STR(lnStartRow))

*!*							lnEndRow = lnStartRow - 1
*!*							lnRow = lnStartRow - 1
						
*!*	SELECT CURJHCSV
*!*	COPY TO C:\A\INFO401K


*!*							SELECT CURJHCSV
*!*							SCAN
*!*								lnRow = lnRow + 1
*!*								lnEndRow = lnRow

*!*								lcRow = ALLTRIM(STR(lnRow))

*!*								oWorksheet.RANGE("A"+lcRow).VALUE = "D"
*!*								*oWorksheet.RANGE("B"+lcRow).VALUE = "108343"
*!*								oWorksheet.RANGE("B"+lcRow).VALUE = "455221-01"
*!*								oWorksheet.RANGE("C"+lcRow).VALUE = "'" + CURJHCSV.SS_NUM
*!*								oWorksheet.RANGE("D"+lcRow).VALUE = CURJHCSV.NAME
*!*								oWorksheet.RANGE("E"+lcRow).VALUE = "**A"
*!*								oWorksheet.RANGE("F"+lcRow).VALUE = CURJHCSV.AMT_401k
*!*								oWorksheet.RANGE("G"+lcRow).VALUE = "**D"
*!*								oWorksheet.RANGE("H"+lcRow).VALUE = CURJHCSV.MATCH_AMT

*!*								* change some loan ids per Karen 3/24/06 MB
*!*								*oWorksheet.RANGE("M"+lcRow).VALUE = "001"
*!*								DO CASE
*!*									CASE CURJHCSV.SS_NUM = '130542475'  && Manuel Gomez
*!*										oWorksheet.RANGE("M"+lcRow).VALUE = "002"
*!*									CASE CURJHCSV.SS_NUM = '611070558'  && Erica Nicholas
*!*										oWorksheet.RANGE("M"+lcRow).VALUE = "002"
*!*									OTHERWISE
*!*										* default value
*!*										oWorksheet.RANGE("M"+lcRow).VALUE = "001"
*!*								ENDCASE

*!*								oWorksheet.RANGE("N"+lcRow).VALUE = CURJHCSV.LOAN_AMT
*!*								oWorksheet.RANGE("U"+lcRow).VALUE = CURJHCSV.STREET1
*!*								oWorksheet.RANGE("V"+lcRow).VALUE = CURJHCSV.STREET2
*!*								oWorksheet.RANGE("X"+lcRow).VALUE = CURJHCSV.CITY
*!*								oWorksheet.RANGE("Y"+lcRow).VALUE = CURJHCSV.STATE
*!*								oWorksheet.RANGE("Z"+lcRow).VALUE = "'" + CURJHCSV.ZIPCODE
*!*								oWorksheet.RANGE("AA"+lcRow).VALUE = "USA"
*!*								oWorksheet.RANGE("AB"+lcRow).VALUE = CURJHCSV.GENDER
*!*								oWorksheet.RANGE("AC"+lcRow).VALUE = "'" + CURJHCSV.DIVISION
*!*								IF EMPTY(CURJHCSV.BIRTHDATE) THEN
*!*									oWorksheet.RANGE("AD"+lcRow).VALUE = "?"
*!*								ELSE
*!*									oWorksheet.RANGE("AD"+lcRow).VALUE = CURJHCSV.BIRTHDATE
*!*								ENDIF
*!*								*oWorksheet.RANGE("AE"+lcRow).VALUE = CURJHCSV.HIREDATE
*!*								IF EMPTY(CURJHCSV.HIREDATE) THEN
*!*									oWorksheet.RANGE("AE"+lcRow).VALUE = "?"
*!*								ELSE
*!*									oWorksheet.RANGE("AE"+lcRow).VALUE = CURJHCSV.HIREDATE
*!*								ENDIF
*!*								oWorksheet.RANGE("AH"+lcRow).VALUE = "'7"
*!*								
*!*								oWorksheet.RANGE("AJ"+lcRow).VALUE = CURJHCSV.YTDPAY

*!*							ENDSCAN

*!*							* populate header counts, sums, etc.
*!*							lnRowsProcessed = lnEndRow - lnStartRow + 1
*!*							lcEndRow = ALLTRIM(STR(lnEndRow))

*!*							* rec count
*!*							oWorksheet.RANGE("D2").VALUE = lnRowsProcessed

*!*							* sum $ columns
*!*							oWorksheet.RANGE("F2").VALUE = "=SUM(F4:F" + lcEndRow + ")"
*!*							oWorksheet.RANGE("H2").VALUE = "=SUM(H4:H" + lcEndRow + ")"
*!*							oWorksheet.RANGE("J2").VALUE = "=SUM(J4:J" + lcEndRow + ")"
*!*							oWorksheet.RANGE("L2").VALUE = "=SUM(L4:L" + lcEndRow + ")"
*!*							oWorksheet.RANGE("N2").VALUE = "=SUM(N4:N" + lcEndRow + ")"
*!*							oWorksheet.RANGE("P2").VALUE = "=SUM(P4:P" + lcEndRow + ")"
*!*							oWorksheet.RANGE("R2").VALUE = "=SUM(R4:R" + lcEndRow + ")"
*!*							oWorksheet.RANGE("T2").VALUE = "=SUM(T4:T" + lcEndRow + ")"

*!*							* paycheck date
*!*							IF .lUseDateRange THEN
*!*								oWorksheet.RANGE("V2").VALUE = "YTD thru " + .cRangeEndDate
*!*							ELSE
*!*								oWorksheet.RANGE("V2").VALUE = ldEndingPaydate
*!*							ENDIF

*!*							* SAVE AND QUIT EXCEL
*!*							oWorkbook.SAVE()
*!*							oExcel.QUIT()



						***********************************************************************************
						***********************************************************************************
						***********************************************************************************
						***********************************************************************************
						* Create output csv file
						
						IF .lUseDateRange THEN
							lcOutputFilename = "F:\UTIL\JOHNHANCOCK\REPORTS\" + .cMode + ;
								" SGL CONTRIB FILE " + .cRangeStartDate + " - " + .cRangeEndDate + ".CSV"
						ELSE
							lcOutputFilename = "F:\UTIL\JOHNHANCOCK\REPORTS\" + .cMode + ;
								" SGL CONTRIB FILE " + STRTRAN(DTOC(ldEndingPaydate),"/","-") + ".CSV"
						ENDIF

						* open output file
						*lcOutputFilename = "F:\UTIL\JOHNHANCOCK\REPORTS\SGL CONTRIB FILE " + STRTRAN(DTOC(ldEndingPaydate),"/","-") + ".CSV"
						.nCurrentOutputCSVFileHandle = FCREATE(lcOutputFilename)
						.TrackProgress('nCurrentOutputCSVFileHandle = ' + TRANSFORM(.nCurrentOutputCSVFileHandle), LOGIT+SENDIT)
						IF .nCurrentOutputCSVFileHandle < 0 THEN
							.TrackProgress('***** Error on FCREATE of: ' + lcOutputFilename, LOGIT+SENDIT)
						ELSE
							.TrackProgress('FCREATEed: ' + lcOutputFilename, LOGIT+SENDIT)
						ENDIF

						* write the header line
						lcHeaderLine = 'Trans#,Cont#,SSN#,"Participant Name (Lastname, Firstname)",Date (mmddyyyy),EEDEF,ERMAT,ERPS,EEROT,LoanID,LoanAmt'
						=FPUTS(.nCurrentOutputCSVFileHandle,lcHeaderLine)
						
						* create variables to drive detail lines
						SELECT CURJHCSV
						SCAN
							lcPayDate = STRTRAN(DTOC(ldEndingPaydate),"/","")
							lcLoanID = "1"  && how do I know if it is 1 or 2 or more?
							lcTransactionNumber = "505"
							lcContractNumber = "94343"
							lcSSN = CURJHCSV.SS_NUM
							lcDivNum = ""
							lcName = '"' + ALLTRIM(CURJHCSV.NAME) + '"'
							lcLast = CURJHCSV.LASTNAME
							lcFirst = CURJHCSV.FIRSTNAME
							lcMiddle = ""
							lcSuffix = ""
							lcBirthDate = CURJHCSV.BIRTHDATE
							lcGender = CURJHCSV.GENDER
							lcMarital = "??marital???"
							lcAddress1 = CURJHCSV.STREET1
							lcAddress2 = CURJHCSV.STREET2
							lcCity = CURJHCSV.CITY
							lcState = UPPER(CURJHCSV.STATE)
							lcZip = CURJHCSV.ZIPCODE
							lcHomePhone = ""
							lcWorkPhone = ""
							lcWorkPhoneExt = ""
							lcCountryCode = ""
							ldHireDate = CURJHCSV.HIREDATE
							IF CURJHCSV.STATUS = 'T' AND NOT ISNULL(CURJHCSV.TERMDATE) THEN
								lcTermDate = DTOC(CURJHCSV.TERMDATE)
							ELSE
								lcTermDate = ""
							ENDIF
							lcRehireDate = ""
							lcEEContribution = ALLTRIM(STR(CURJHCSV.AMT_401k,11,2))
							lcEmployerMatch = ALLTRIM(STR(CURJHCSV.MATCH_AMT,11,2))
							lcLoanRepayment = ALLTRIM(STR(CURJHCSV.LOAN_AMT,11,2))
							lcROTHAmt = ALLTRIM(STR(CURJHCSV.ROTH_AMT,11,2))
							lcZeroContrib = ALLTRIM(STR(0.00,11,2))
							lcYTDHoursWorked = ALLTRIM(STR(INT(CURJHCSV.YTDHOURS)))
							lcYTDTotCompensation = ALLTRIM(STR(CURJHCSV.YTDPAY,11,2))
							lcYTDPlanComp = lcYTDTotCompensation
							lcPreContrib = ALLTRIM(STR(0.00,11,2))
							
							IF CURJHCSV.SALARY > 80000 THEN
								lcHighComp = "Y"
							ELSE
								lcHighComp = "N"
							ENDIF

							IF INLIST(CURJHCSV.ADPCOMP,'AXA','E89') THEN
								lcOfficer = "Y"
							ELSE
								lcOfficer = "N"
							ENDIF
							lcBlanks = ""
							lcPartDate = "?part date?"
							lcEligibleCode = "?elig code?"
								
							* Write Detail Line for John Hancock
							lcOutputLine = ;
								.CleanUpField( lcTransactionNumber ) + "," + ;
								.CleanUpField( lcContractNumber ) + "," + ;
								.CleanUpField( lcSSN ) + "," + ;
								lcName + "," + ;
								.CleanUpField( lcPayDate ) + "," + ;
								.CleanUpField( lcEEContribution ) + "," + ;
								.CleanUpField( lcEmployerMatch ) + "," + ;
								.CleanUpField( lcZeroContrib ) + "," + ;
								.CleanUpField( lcROTHAmt ) + "," + ;
								.CleanUpField( lcLoanID ) + "," + ;
								.CleanUpField( lcLoanRepayment )

*!*	*** ADDING WORKSITE FOR MARIE								
*!*	lcOutputLine = lcOutputLine + "," +  CURJHCSV.WORKSITE								

*!*									.CleanUpField( lcName ) + "," + ;

							=FPUTS(.nCurrentOutputCSVFileHandle,lcOutputLine)
							
						ENDSCAN

						* close output file
						IF .nCurrentOutputCSVFileHandle > -1 THEN
							llRetval = FCLOSE(.nCurrentOutputCSVFileHandle)
						ENDIF

						***********************************************************************************
						***********************************************************************************
						***********************************************************************************
						***********************************************************************************


						IF FILE(lcOutputFilename) THEN
							* attach xls output file to email
							.cAttach = lcOutputFilename
							.cBodyText = "See attached John Hancock 401k CSV File." + ;
								CRLF + CRLF + "(do not reply - this is an automated report)" + ;
								CRLF + CRLF + "<report log follows>" + ;
								CRLF + .cBodyText
						ELSE
							.TrackProgress('ERROR: unable to email file: ' + lcOutputFilename, LOGIT+SENDIT+NOWAITIT)
						ENDIF
						
*!*							IF FILE(lcOutputFilename) THEN
*!*								* attach csv output file to email
*!*								.cAttach = .cAttach + "," + lcOutputFilename
*!*							ELSE
*!*								.TrackProgress('ERROR: unable to email spreadsheet: ' + lcOutputFilename, LOGIT+SENDIT+NOWAITIT)
*!*							ENDIF

						CLOSE DATABASES ALL
					ENDIF  &&  .ExecSQL(lcSQL, 'SQLCURSOR1', RETURN_DATA_MANDATORY)

					.TrackProgress('John Hancock 401k CSV File process ended normally.', LOGIT+SENDIT+NOWAITIT)
				ELSE
					* connection error
					.TrackProgress('Unable to connect to ADP Payroll System.', LOGIT+SENDIT)
				ENDIF   &&  .nSQLHandle > 0

			CATCH TO loError

				.TrackProgress('There was an error.',LOGIT+SENDIT)
				.TrackProgress('The Error # is: ' + TRANSFORM(loError.ERRORNO), LOGIT+SENDIT)
				.TrackProgress('The Error Message is: ' + TRANSFORM(loError.MESSAGE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred in Program: ' + TRANSFORM(loError.PROCEDURE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred at line #: ' + TRANSFORM(loError.LINENO), LOGIT+SENDIT)
				lnNumberOfErrors = lnNumberOfErrors + 1
				IF TYPE('oWorkbook') = "O" THEN
					oWorkbook.SAVE()
				ENDIF
				IF TYPE('oExcel') = "O" THEN
					oExcel.QUIT()
				ENDIF
				CLOSE DATABASES ALL

			ENDTRY

			CLOSE DATA
			WAIT CLEAR
			***************** INTERNAL email results ******************************
			.TrackProgress('About to send status email.',LOGIT)
			.TrackProgress('The logfile is: ' + .cLogFile, SENDIT)
			.TrackProgress('John Hancock 401k CSV File process started: ' + .cStartTime, LOGIT+SENDIT)
			.TrackProgress('John Hancock 401k CSV File process finished: ' + TTOC(DATETIME()), LOGIT+SENDIT)

			IF .lSendInternalEmailIsOn THEN
				DO FORM dartmail2 WITH .cSendTo,.cFrom,.cSubject,.cCC,.cAttach,.cBodyText,"A"
				.TrackProgress('Sent status email.',LOGIT)
			ELSE
				.TrackProgress('Did not send status email: THIS.lSendInternalEmailIsOn = FALSE.',LOGIT)
			ENDIF

		ENDWITH
		RETURN
	ENDFUNC && main


	FUNCTION CleanUpField
		* prepare field for inclusion in the string to be written with FPUTS().
		* especially critical to remove any commas, since that creates bogus column in CSV output file.
		LPARAMETERS tuValue
		LOCAL lcRetVal
		lcRetVal = STRTRAN(ALLTRIM(TRANSFORM(tuValue)),",","")
		RETURN lcRetVal
	ENDFUNC


	FUNCTION ExecSQL
		LPARAMETERS tcSQL, tcCursorName, tlNoDataReturnedIsOkay
		LOCAL llRetval, lnResult
		WITH THIS
			* close target cursor if it's open
			IF USED(tcCursorName)
				USE IN (tcCursorName)
			ENDIF
			WAIT WINDOW "Fetching remote data; please wait..." NOWAIT
			lnResult = SQLEXEC(.nSQLHandle, tcSQL, tcCursorName)
			llRetval = ( lnResult > 0 )
			IF llRetval THEN
				* see if any data came back
				IF NOT tlNoDataReturnedIsOkay THEN
					IF (NOT USED(tcCursorName)) OR EOF(tcCursorName) THEN
						llRetval = .F.
						.TrackProgress('No data was returned by this query: ' +CRLF + tcSQL, LOGIT+SENDIT)
						.cSendTo = 'Mark Bennett <mbennett@fmiint.com>'
					ENDIF
				ENDIF
			ELSE
				.TrackProgress("Result Code [" + TRANSFORM(lnResult) + "] returned by this query: " + tcSQL, LOGIT+SENDIT)
				.cSendTo = 'Mark Bennett <mbennett@fmiint.com>'
			ENDIF
			WAIT CLEAR
			RETURN llRetval
		ENDWITH
	ENDFUNC


	PROCEDURE TrackProgress
		* do any combination of Wait Window, writing to logfile, and adding to body of email,
		* based on nFlags parameter.
		LPARAMETERS tcExpression, tnFlags
		WITH THIS
			IF BITAND(tnFlags,LOGIT) = LOGIT THEN
				IF .lLoggingIsOn THEN
					?
					? .cProcessName + "  " + TTOC(DATETIME()) + ": " + tcExpression
				ENDIF
			ENDIF
			IF .lWaitWindowIsOn THEN
				IF BITAND(tnFlags,NOWAITIT) = NOWAITIT THEN
					WAIT WINDOW tcExpression NOWAIT
				ENDIF
				IF BITAND(tnFlags,WAITIT) = WAITIT THEN
					WAIT WINDOW tcExpression TIMEOUT .nWaitWindowTimeout
				ENDIF
			ENDIF
			IF BITAND(tnFlags,SENDIT) = SENDIT THEN
				IF .lSendInternalEmailIsOn THEN
					.cBodyText = .cBodyText + tcExpression + CRLF + CRLF
				ENDIF
			ENDIF
		ENDWITH
	ENDPROC  &&  TrackProgress

ENDDEFINE

