* invoice spreadsheet for Office Depot

utilsetup("ODINVOICE")

xsqlexec("select * from invoice where accountid=6493 and wo_num=31 and invdt>{"+dtoc(date()-6)+"}",,,"ar")

delete file h:\fox\odinvoice.xls

create cursor xrpt (invnum c(6), vendorid c(6), invdt d, invamt n(9,2), container c(11), feecode c(3), seqno n(4), invamt2 n(9,2))

m.vendorid="974967"
m.feecode="280"
m.seqno=0

select invoice
scan for accountid=6493 and wo_num=31 and invdt>date()-6
	m.invnum=invoice.invnum
	m.invdt=invoice.invdt

	xsqlexec("select wo_num, description, invdetamt from invdet where invoiceid="+transform(invoice.invoiceid),"xtemp",,"ar")
	
	scan
		m.invamt=invdetamt
		m.container=substr(description,19,11)

		skip
		m.invamt=m.invamt+invdetamt
		
		do while .t. and description#"WO" and !eof()
			skip
			m.invamt=m.invamt+invdetamt
		enddo
		
		if !eof()
			skip -1
		endif
		
		m.invamt2=m.invamt
		m.seqno=m.seqno+1
		insert into xrpt from memvar
	endscan
endscan

select xrpt
copy to h:\fox\odinvoice.xls xls

*

tsendto="GSOperations@officedepot.com"
*tsendto="dyoung@fmiint.com"
tcc=""
tattach="h:\fox\odinvoice.xls"
tFrom ="TGF EDI System Operations <transload-ops@fmiint.com>"
tSubject = "Weekly Toll Invoices"
tmessage = "See attached file"

Do Form dartmail2 With tsendto,tFrom,tSubject," ",tattach,tmessage,"A"

use in invoice
use in invdet

*

schedupdate()

_screen.Caption=gscreencaption
on error
