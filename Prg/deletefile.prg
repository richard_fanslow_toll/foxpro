* deletes files

lparameters xdir, xdays, xnosubfolders, xnowaitwindow, xdate, xfilename, xletter

xnumdel=0

if len(xdir)=1
	xdir=xdir+":\"
endif
	
if right(xdir,1)#"\"
	xdir=xdir+"\"
endif

do case
case !empty(xdate)
	xfilter="yy[j,3]<={"+dtoc(xdate)+"}"
case !empty(xfilename)
	xfilter="yy[j,1]='"+xfilename+"'"
case empty(xdays)
	xfilter=".t."
otherwise
	xfilter="date()-yy[j,3]>"+transform(xdays)
endcase

if empty(xletter)
	xfilter2=".t."
else
	xfilter2="inlist(xgeorge[i,1],xletter,upper(xletter))"
endif

create cursor xrpt (folder c(80))
insert into xrpt (folder) value (xdir)

if !xnosubfolders
	adir(xgeorge,xdir+"*.*","D")

*	try
		for i=3 to alen(xgeorge,1)
			if substr(xgeorge[i,5],5,1)="D" and &xfilter2 
				xsub1=xgeorge[i,1]
				insert into xrpt (folder) value (xdir+xsub1+"\")

				adir(xsam,xdir+xsub1+"\*.*","D")
				for j=3 to alen(xsam,1)
					if substr(xsam[j,5],5,1)="D"
						xsub2=xsam[j,1]
						insert into xrpt (folder) value (xdir+xsub1+"\"+xsub2+"\")

						adir(xlou,xdir+xsub1+"\"+xsub2+"\*.*","D")
						for k=3 to alen(xlou,1)
							if substr(xlou[k,5],5,1)="D"
								xsub3=xlou[k,1]
								insert into xrpt (folder) value (xdir+xsub1+"\"+xsub2+"\"+xsub3+"\")

								adir(xbob,xdir+xsub1+"\"+xsub2+"\"+xsub3+"\*.*","D")
								for l=3 to alen(xbob,1)
									if substr(xbob[l,5],5,1)="D"
										xsub4=xbob[l,1]
										insert into xrpt (folder) value (xdir+xsub1+"\"+xsub2+"\"+xsub3+"\"+xsub4+"\")
										
										adir(xted,xdir+xsub1+"\"+xsub2+"\"+xsub3+"\"+xsub4+"\*.*","D")
										for m=3 to alen(xted,1)
											if substr(xted[m,5],5,1)="D"
												xsub5=xted[m,1]
												insert into xrpt (folder) value (xdir+xsub1+"\"+xsub2+"\"+xsub3+"\"+xsub4+"\"+xsub5+"\")
											endif
										next
									endif
								next
							endif
						next
					endif
				next
			endif
		next
*	catch
*	endtry
endif

select xrpt
scan
	wait window folder nowait
	
	release xben
	
	xfolder=folder
	adir(xben,trim(folder)+"*.*","D")

	try 	&& some folders are inacessible on the S drive
		for i=3 to alen(xben,1)
			if substr(xben[i,5],5,1)#"D"
				adir(yy,trim(folder)+xben[i,1])
				if type("yy")#"U"
					for j=1 to alen(yy,1)
						if &xfilter
							xfile=trim(folder)+xben[i,1]
							if yy[j,5]="R"
								!Attrib -R &xfile
							endif

							xnumdel=xnumdel+1
							try
								delete file (xfile)
							catch
							endtry
							if !xnowaitwindow
								Wait window "deleting file......."+(trim(folder)+xben[i,1]) nowait 
							endif
						endif
					next
				endif
			endif
		next
	catch
	endtry
endscan

wait clear

*gunshot()

return xnumdel
? xnumdel