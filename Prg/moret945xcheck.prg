CLOSE DATABASES ALL
RELEASE ALL
PUBLIC lTesting,lMBOL
DO m:\dev\prg\_setvars WITH .T.

lTesting = .t.

SELECT 0
USE F:\3pl\DATA\mailmaster ALIAS mm SHARED
LOCATE FOR mm.edi_type = "MISC" AND mm.taskname = "GENERAL"
STORE TRIM(IIF(mm.use_alt,mm.sendtoalt,mm.sendto)) TO tsendto
STORE TRIM(IIF(mm.use_alt,mm.ccalt,mm.cc)) TO tcc
USE IN mm

USE F:\3pl\DATA\edi_trigger ALIAS edi_trigger SHARED NOUPDATE

WAIT WINDOW "Now selecting today's processed trigger records..." NOWAIT NOCLEAR
SELECT DISTINCT bol,office,PADR(UPPER(JUSTFNAME(file945)),35) AS file945,.F. AS foundit ;
	FROM edi_trigger ;
	WHERE edi_trigger.moret ;
	AND TTOD(when_proc) = DATE() ;
	AND edi_trigger.processed ;
	AND !edi_trigger.errorflag ;
	INTO CURSOR tempedi READWRITE
IF lTesting
	BROWSE
ENDIF
WAIT CLEAR


CREATE CURSOR temp945intake (bol c(20),filename c(80))

*	SET STEP ON
FOR i = 1 TO 4
	DO case
	case i = 1
		CD "F:\FTPUSERS\Moret-CR\945out\945archive\"
		CLOC = "CARSON"
		cOffice = "X"
	CASE i = 2
		CD "F:\FTPUSERS\Moret-FL\945out\945archive\"
		CLOC = "MIAMI"
		cOffice = "M"
	CASE i = 3
		CD "F:\FTPUSERS\Moret-ML\945out\945archive\"
		CLOC = "MIRA LOMA"
		cOffice = "L"
	CASE i = 4
		CD "F:\FTPUSERS\Moret-SP\945out\945archive\"
		CLOC = "SAN PEDRO"
		cOffice = "C"
	endcase

	len1 = ADIR(ary1,"*.edi")
	IF len1 = 0
		LOOP
	ENDIF

	STORE "" TO cBOL,cFilename
	WAIT WINDOW "Now checking "+CLOC+" 945 files..." NOWAIT
	FOR loops = 1 TO len1
		dDate = ary1[loops,3]
		IF dDate < DATE()
			LOOP
		ENDIF
		cFilename = ALLTRIM(ary1[loops,1])
		DO m:\dev\prg\createx856a
		SELECT x856
		APPEND FROM [&cFilename] TYPE DELIMITED WITH CHARACTER "*"
		lMBOL = .F.
		LOCATE FOR x856.segment = "N9" AND x856.f1 = "TN"
		IF !FOUND()
			LOCATE FOR x856.segment = "N9" AND x856.f1 = "MB"
			IF !FOUND()
				LOCATE FOR x856.segment = "N9" AND x856.f1 = "BM"
			ELSE
				lMBOL = .T.
			ENDIF
		ENDIF
		cBOL = ALLTRIM(x856.f2)
		INSERT INTO temp945intake (bol,filename) VALUES (cBOL,cFilename)
		STORE "" TO cBOL,cFilename
		USE IN x856
	ENDFOR
ENDFOR

SELECT temp945intake
LOCATE
IF lTesting
	BROWSE
ENDIF

*SET STEP ON
SELECT tempedi
SCAN
	SCATTER MEMVAR
	IF SEEK(ALLTRIM(m.bol),'edi_trigger','bol') AND edi_trigger.moret
		SELECT temp945intake
		LOCATE FOR temp945intake.bol = ALLTRIM(m.bol) AND UPPER(ALLTRIM(m.file945))=UPPER(ALLTRIM(temp945intake.filename))
		IF FOUND()
			REPLACE tempedi.foundit WITH .T. NEXT 1 IN tempedi
		ELSE
			WAIT WINDOW "BOL "+ALLTRIM(m.bol)+", File: "+ALLTRIM(m.file945)+" not found in Trigger extract"
			SET STEP ON
		ENDIF
	ENDIF
ENDSCAN
SELECT tempedi
LOCATE
IF lTesting
	BROWSE
ENDIF

COUNT TO N FOR !tempedi.foundit
IF N > 0  && If there are any BOLs not found
	tMessage = "There were missing 945 trigger/file matches:"+CHR(13)+CHR(13)+PADR("FILE NAME",40)+"BOL #"
	SCAN FOR !tempedi.foundit
		tMessage = tMessage+CHR(13)+PADR(ALLTRIM(tempedi.file945),40)+ALLTRIM(tempedi.bol)
	ENDSCAN
ELSE
	tMessage = "All trigger records in the EDI_TRIGGER table produced matched 945s"
ENDIF

tattach = ""
tsubject= "Toll 945 Cross-Check, " +TTOC(DATETIME())

tFrom ="Toll EDI Operations <toll-edi-ops@tollgroup.com>"
DO FORM m:\dev\frm\dartmail2 WITH tsendto,tFrom,tsubject,tcc,tattach,tMessage,"A"

WAIT CLEAR
CLOSE DATA ALL
RELEASE ALL
RETURN

