Parameters wonum

close data all 

DO m:\dev\prg\_setvars

xsqlexec("select * from pl where wo_num="+transform(wonum),,,"wh")

wonum = Val(Alltrim(Transform(wonum)))

Set EngineBehavior 70
Select accountid,style,"    " as div, Sum(Val(Alltrim(pack))*totqty) as totunits,Sum(totqty) as total_ctns, 000000 as qunits, 000000 as qc_ctns, "     " as qcpack, ;
 0000000 as qc_qty from pl where wo_num = wonum group by style into cursor temp readwrite 

Select temp
scan
  if upcmastsql(,temp.style)
    alength = ALINES(aupc,upcmast.INFO,.T.,CHR(13))
    cDiv = ALLTRIM(segmentget(@aupc,"DIV",alength))
    replace div with cDiv  in temp
  Else
    replace div with "UNK" in temp
  EndIf 
  
EndScan

Set Step On 

Select temp
Select * from temp where div ="002" into cursor sbhdata
Select sbhdata
If Reccount("sbhdata") = 0
  =MessageBox("No SBH\Kohls Goods to QC",0,"Moret QC Goods Selector")
  return
EndIf 

Select style,pack,totqty from pl where wo_num = wonum into cursor t2
Select temp
Scan
  Do case
    Case Between(totunits,1,8)
       samplesize = 2
    Case Between(totunits,9,15)
       samplesize = 3
    Case Between(totunits,16,25)
       samplesize = 5
    Case Between(totunits,26,50)
       samplesize = 8
    Case Between(totunits,51,90)
       samplesize = 13
    Case Between(totunits,91,150)
       samplesize = 20
    Case Between(totunits,151,280)
       samplesize = 32
    Case Between(totunits,281,500)
       samplesize = 50
    Case Between(totunits,501,1200)
       samplesize = 80
    Case Between(totunits,1201,3200)
       samplesize = 125
    Case Between(totunits,3201,10000)
       samplesize = 200
  EndCase
  replace qunits with samplesize
EndScan


&& now to select some cartons

*Set Step On 
Select temp  && we need qunits of goods
Scan
  Select t2
  Locate for style = temp.style and Val(pack)* totqty >= temp.qunits
  If Found()
    Ctns_needed = temp.qunits / Val(t2.pack)
    If Ctns_Needed > Int(Ctns_needed) 
      Ctns_Needed = Int(Ctns_needed) +1
    Else
      Ctns_Needed = Int(Ctns_needed)
    EndIf    
  Else
  
  EndIf 
  
  Select t2
  If Ctns_Needed <= totqty
    replace qc_ctns with Ctns_Needed in temp
    replace qcpack  with t2.pack in temp
    replace qc_qty  with qc_ctns * Val(Alltrim(qcpack)) in temp
    
  EndIf  
EndScan

Select temp
Sum totunits   to allunits 
Sum total_ctns to allctns 
Sum qunits     to allQunits 
Sum qc_ctns    to allqc_ctns
Sum qc_qty     to all_qc_qty

append blank
replace style      with "SUM DATA"
replace totunits   with allunits
replace total_ctns with allctns
replace qunits     with allqunits
replace qc_ctns    with allqc_ctns
replace qc_qty     with all_qc_qty


cfilename = "h:\fox\moretQC_"+Transform(wonum)+".xls"

Export to &cfilename type xls
=MessageBox("SBH\Kohls Goods to QC"+Chr(13)+"look at file: "+cfilename+Chr(13),0,"Moret QC Goods Selector")

return

****************************
PROCEDURE segmentget
****************************

  PARAMETER thisarray,lcKey,nLength

  FOR i = 1 TO nLength
    IF i > nLength
      EXIT
    ENDIF
    lnEnd= AT("*",thisarray[i])
    IF lnEnd > 0
      lcThisKey =TRIM(SUBSTR(thisarray[i],1,lnEnd-1))
      IF OCCURS(lcKey,lcThisKey)>0
        RETURN SUBSTR(thisarray[i],lnEnd+1)
        i = 1
      ENDIF
    ENDIF
  ENDFOR

  RETURN ""
ENDPROC