** This script will identify if "Send Receipt" was ticked after confirming the I/B 943 WO
** Send Receipt creates the edi944 file.
utilsetup("MIAMI_FILE_CHECKS")
set exclusive off
set deleted on
set talk off
set safety off   &&Allows for overwrite of existing files
*********************************************START INBOUND
goffice='M'
useca("inwolog","wh",,,"mod='"+goffice+"' and  accountid in (687,6356) and confirmdt>{"+dtoc(date()-30)+"}")
useca("indet","wh",,,"mod='"+goffice+"' and  accountid in (687,6356) and date_rcvd>{"+dtoc(date()-30)+"}")


select wo_num as wo, acct_ref as shipment_number, confirmdt as date_confirmed;
	from  inwolog where inlist(accountid ,687,6356) and confirmdt is NOT null and reference !='CANCEL' and confirmsent = .f.  ;
	into cursor temp1

if reccount() > 0
	export 	to  s:\miami\temp\miami_943_ib_wo_send_receipt type xls

	tsendto = "tmarg@fmiint.com,Joe.abbate@tollgroup.com,patricia.zelaya@tollgroup.com,todd.margolin@tollgroup.com"
	tattach = "s:\miami\temp\miami_943_IB_WO_send_receipt.xls"
	tcc =""
	tfrom ="Toll EDI System Operations <fmi-transload-ops@fmiint.com>"
	tmessage = "Miami IB WO Send Receipt not Ticked "+ttoc(datetime())+"           from miami_file_checks.prg"
	tsubject = "Miami IB WO Send Receipt not Ticked"
	do form m:\dev\frm\dartmail2 with tsendto,tfrom,tsubject, tcc,tattach,tmessage  ,"A"
endif

******************************** check for 944 in edi_trigger  NJ

if !used("edi_trigger")
	use f:\3pl\data\edi_trigger shared in 0
endif

select wo_num as wo, acct_ref as shipment_number, confirmdt as date_confirmed;
	from  inwolog where inlist(accountid ,687,6356) and confirmdt > date()-30  and reference !='CANCEL' and confirmsent = .t. ;
	into cursor temp2 readwrite
select * from temp2 a left join edi_trigger b on a.wo=b.wo_num into cursor c3 readwrite
select * from c3 where emptynul(when_proc) into cursor c4 readwrite
if reccount() > 0
	export to  s:\miami\temp\miami_missing_944 type xls

	tsendto = "Chris.Malcolm@tollgroup.com,Joe.abbate@tollgroup.com"
	tattach = "s:\miami\temp\miami_missing_944.xls"
	tcc =""
	tfrom ="Toll EDI System Operations <fmi-transload-ops@fmiint.com>"
	tmessage = "Miami 944 issue "+ttoc(datetime())+"           from maimi_file_checks.prg"
	tsubject = "Miami 944 issue"
	do form m:\dev\frm\dartmail2 with tsendto,tfrom,tsubject, tcc,tattach,tmessage  ,"A"
else
	wait window at 10,10 "No  data to report.........." timeout 2
	tsendto = "Chris.Malcolm@tollgroup.com"
	tattach = ""
	tcc =""
	tfrom ="Toll EDI System Operations <fmi-transload-ops@fmiint.com>"
	tmessage = "NO Miami 944 issue: "+ttoc(datetime())+"           from mj_944_create_confirm.prg"
	tsubject = "NO Miami 944 issue"
	do form m:\dev\frm\dartmail2 with tsendto,tfrom,tsubject, tcc,tattach,tmessage  ,"A"
endif

********************************************* OUTBOUND
goffice='M'
useca("outship","wh",,,"mod='"+goffice+"' and  accountid in (687,6356,4677) and wo_date>{"+dtoc(date()-180)+"}")

******************************************MISSING BOL
select * from outship where inlist(accountid,687,6356,4677) and del_date is NOT null and EMPTY(bol_no)  into cursor vmissingbol readwrite
if reccount() > 0
	export to  s:\miami\temp\missing_bol type xls

	tsendto = "Chris.Malcolm@tollgroup.com,Joe.abbate@tollgroup.com"
	tattach = "s:\miami\temp\missing_bol.xls"
	tcc =""
	tfrom ="Toll EDI System Operations <fmi-transload-ops@fmiint.com>"
	tmessage = "Miami Missing BOL for shipped PT"+ttoc(datetime())+"           from miami_file_checks.prg"
	tsubject = "Miami Missing BOL for shipped PT"
	do form m:\dev\frm\dartmail2 with tsendto,tfrom,tsubject, tcc,tattach,tmessage  ,"A"
endif

select accountid,ship_ref from outship where inlist(accountid,687,6356,4677) and del_date>date()-30 and !empty(bol_no) AND !INLIST(BOL_NO,'X04907304677771365','04907304677744093') into cursor t1 readwrite
select * from t1 a left join edi_trigger b on  a.ship_ref=b.ship_ref into cursor t2 readwrite
select * from t2 where isnull(edi_type) into cursor vmisspttrig readwrite
if reccount() > 0
	export to  s:\miami\temp\missing_pt_trigger type xls

	tsendto = "Chris.Malcolm@tollgroup.com,Joe.abbate@tollgroup.com"
	tattach = "s:\miami\temp\missing_pt_trigger.xls"
	tcc =""
	tfrom ="Toll EDI System Operations <fmi-transload-ops@fmiint.com>"
	tmessage = "Miami Missing PT in trigger"+ttoc(datetime())+"           from miami_file_checks.prg"
	tsubject = "Miami Missing PT in trigger"
	do form m:\dev\frm\dartmail2 with tsendto,tfrom,tsubject, tcc,tattach,tmessage  ,"A"
endif

select * from t2 where empty(when_proc) and fin_status != 'NO 945' into cursor not_processed945 readwrite
if reccount() > 0
	export to  s:\miami\temp\miami_945_trig_problem type xls
	tsendto = "Chris.Malcolm@tollgroup.com,Joe.abbate@tollgroup.com"
	tattach = "s:\miami\temp\miami_945_trig_problem.xls"
	tcc =""
	tfrom ="Toll EDI System Operations <fmi-transload-ops@fmiint.com>"
	tmessage = "Miami 945 trigger not processed"+ttoc(datetime())+"           from miami_file_checks.prg"
	tsubject = "Miami 945 trigger not processed"
	do form m:\dev\frm\dartmail2 with tsendto,tfrom,tsubject, tcc,tattach,tmessage  ,"A"
endif


select * from edi_trigger where inlist(accountid,687,6356,4677) and errorflag into cursor t4 readwrite
if reccount() > 0
	export to  s:\miami\temp\miami_945_trig_error type xls
	tsendto = "Chris.Malcolm@tollgroup.com,Joe.abbate@tollgroup.com"
	tattach = "s:\miami\temp\miami_945_trig_error.xls"
	tcc =""
	tfrom ="Toll EDI System Operations <fmi-transload-ops@fmiint.com>"
	tmessage = "Miami 945 trigger error"+ttoc(datetime())+"           from miami_file_checks.prg"
	tsubject = "Miami 945 trigger error"
	do form m:\dev\frm\dartmail2 with tsendto,tfrom,tsubject, tcc,tattach,tmessage  ,"A"
endif

***************************************** CHECK CTNUCC QUANTITIES AGAINST I/B WO
****CHECK PL CARTON COUNT VS CTNUCC
set step on

xsqlexec("select * from ctnucc where mod='M' and inlist(accountid,687,6356)",,,"wh")

useca('pl','wh',,,'m-m')
select wo_num from inwolog where inlist(accountid,687,6356,4677) and  emptynul(confirmdt) and wo_date>date()-5 into cursor vinwo readwrite
select b.wo_num,style,color,id,pack, totqty from vinwo a left join  pl b on a.wo_num=b.wo_num  into cursor vpl1 readwrite
select inwonum,style,color,id,pack,cnt(1) as count from vinwo a left join ctnucc b on  a.wo_num= b.inwonum group by inwonum,style,color,id,pack into cursor vctn readwrite
select * from vpl1 a full join vctn b on a.wo_num=b.inwonum and a.style=b.style and a.color=b.color and a.id=b.id and a.pack=b.pack into cursor c1 readwrite
replace count with 0 for isnull(count) in c1
replace totqty with 0 for isnull(totqty) in c1
select * from c1 where totqty!=count into cursor c2 readwrite
if reccount() > 0
	export to  s:\miami\temp\miami_ib_bad_856_load type xls
	tsendto = "Chris.Malcolm@tollgroup.com,Joe.abbate@tollgroup.com"
	tattach = "s:\miami\temp\miami_ib_bad_856_load.xls"
	tcc =""
	tfrom ="Toll EDI System Operations <fmi-transload-ops@fmiint.com>"
	tmessage = "Miami 856 load problem "+ttoc(datetime())+"           from miami_file_checks.prg"
	tsubject = "Miami 856 load problem"
	do form m:\dev\frm\dartmail2 with tsendto,tfrom,tsubject, tcc,tattach,tmessage  ,"A"
endif


select * from inwolog where inlist(accountid,687,6356,4677) and (confirmdt)>date()-5 into cursor vconfrm readwrite
select b.wo_num,style,color,id,pack, totqty from vconfrm a left join  indet b on a.wo_num=b.wo_num  into cursor vindet1 readwrite
select inwonum,style,color,id,pack,cnt(1) as count from vconfrm a left join ctnucc b on  a.wo_num= b.inwonum and !empty(rcvdt) group by inwonum,style,color,id,pack into cursor vctn1 readwrite
select * from vindet1 a full join vctn1 b on a.wo_num=b.inwonum and a.style=b.style and a.color=b.color and a.id=b.id and a.pack=b.pack into cursor c3 readwrite
replace count with 0 for isnull(count) in c3
replace totqty with 0 for isnull(totqty) in c3
select * from c3 where totqty!=count into cursor c4 readwrite
if reccount() > 0
	export to  s:\miami\temp\miami_bad_944_data type xls
	tsendto = "Chris.Malcolm@tollgroup.com,Joe.abbate@tollgroup.com"
	tattach = "s:\miami\temp\miami_bad_944_data.xls"
	tcc =""
	tfrom ="Toll EDI System Operations <fmi-transload-ops@fmiint.com>"
	tmessage = "Miami bad 944 data indet vs ctnucc "+ttoc(datetime())+"           from miami_file_checks.prg"
	tsubject = "Miami bad 944 data"
	do form m:\dev\frm\dartmail2 with tsendto,tfrom,tsubject, tcc,tattach,tmessage  ,"A"
endif



****CHECK PL SKU QUANTITY VS CTNUCC

wait window at 10,10 "all done...." timeout 2

schedupdate()
_screen.caption=gscreencaption
on error


