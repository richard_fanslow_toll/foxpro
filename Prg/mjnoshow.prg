* Marc Jacobs no show reports

utilsetup("MJNOSHOW")
Set Exclusive Off
Set Deleted On
Set Talk Off
Set Safety Off

goffice="J"

if holiday(date())
	return
endif
****changed TMARG 7/15/16 from xdate=date()-1.  Executes at 8PM and will report on no shows for current day
xdate=date()

xsqlexec("select * from account where inactive=0","account",,"qq")
index on accountid tag accountid
set order to

xsqlexec("select * from apptcall where accountid in (6303,6325,6543) and appt ={"+dtoc(xdate)+"} and status='NO SHOW'",,,"wh")

xsqlexec("select outship.accountid, ship_ref, outship.cnee_ref, ctnqty, weight, picked, " + ;
	"apptcall.appt, apptcall.appt_time, apptcall.called, apptcall.appt_num, " + ;
    "outship.ship_via, asncode, bol_no, consignee, address, address2, csz, outship.accountid " + ;
    "from outship, apptcall where outship.outshipid=apptcall.outshipid and del_date={} " + ;
    "and outship.mod='J' and inlist(outship.accountid,6303,6325,6543)","xtemp",,"wh")

select padr(acctname(accountid),30) as acctname, ship_ref, cnee_ref, ctnqty as qty, ;
    weight, picked, appt, appt_time, called, appt_num, ship_via, asncode, bol_no, ;
    consignee, address, address2, csz, accountid ;
    from xtemp into cursor xrptn

xsqlexec("select outship.accountid, ship_ref, outship.cnee_ref, ctnqty, weight, picked, " + ;
	"apptcall.appt, apptcall.appt_time, apptcall.called, apptcall.appt_num, " + ;
    "outship.ship_via, asncode, bol_no, consignee, address, address2, csz, outship.accountid " + ;
    "from outship, apptcall where outship.outshipid=apptcall.outshipid and del_date={} " + ;
    "and outship.mod='L' and inlist(outship.accountid,6303,6325,6543)","xtemp",,"wh")

select padr(acctname(accountid),30) as acctname, ship_ref, cnee_ref, ctnqty as qty, ;
    weight, picked, appt, appt_time, called, appt_num, ship_via, asncode, bol_no, ;
    consignee, address, address2, csz, accountid ;
    from xtemp into cursor xrptc

set step on
	    
SELECT * FROM xrptn UNION ALL SELECT * FROM xrptc INTO CURSOR xrpt readwrite
	    
tsendto="m.butakis@marcjacobs.com,  e.barnes@marcjacobs.com, g.hubbert@marcjacobs.com, o.batista@marcjacobs.com, J.HALPIN@marcjacobs.com, Jinsoo.KIM@marcjacobs.com, todd.margolin@tollgroup.com, S.Aubin@marcjacobs.com "
tfrom ="TGF <support@fmiint.com>"

select xrpt
count to aa
set step on 
if aa>0
	copy to h:\fox\noshow xls
	tattach = "h:\fox\noshow.xls"
	tmessage = "see attached file"
	tsubject = "No shows on "+dtoc(date())
else
	tattach = ""
	tmessage = ""
	tsubject = "There were no No Shows on "+dtoc(date())
endif

do form dartmail2 with tsendto,tfrom,tsubject," ",tattach,tmessage,"A"

use in apptcall
use in account

schedupdate()

_screen.caption=gscreencaption
on error

