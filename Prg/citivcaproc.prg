* Reformats, renames the Platinum VCARD files for CitiBank, and copies to the VCARD  FTP output folder.
* Runs a couple of times per night.
* EXE = F:\UTIL\CITIVCAPROC\CITIVCAPROC.EXE 
* 4/30/2018 MB: changed fmiint.com to toll email

LPARAMETERS tcVcardAlias

LOCAL loCITIVCAPROC

runack("CITIVCAPROC")

utilsetup("CITIVCAPROC")

_SCREEN.CAPTION = "Rewrite VCARD File for CitiBank" 

loCITIVCAPROC = CREATEOBJECT('CITIVCAPROC')  
loCITIVCAPROC.MAIN( tcVcardAlias )

schedupdate()

CLOSE DATABASES ALL

RETURN

#DEFINE LOGIT 1
#DEFINE WAITIT 2
#DEFINE NOWAITIT 4
#DEFINE SENDIT 8
#DEFINE NOCRLF 16
#DEFINE CRLF CHR(13) + CHR(10)
#DEFINE LF CHR(10)
#DEFINE RETURN_DATA_MANDATORY .F.
#DEFINE RETURN_DATA_NOT_MANDATORY .T.
#DEFINE DAILY_OT_MULTIPLIER 1.5
#DEFINE DAILY_OT20_MULTIPLIER 2.0
#DEFINE xlUp -4162

DEFINE CLASS CITIVCAPROC AS CUSTOM

	cProcessName = 'CITIVCAPROC'

	lTestMode = .F.  && .F. FOR NORMAL OPERATION, .T. TO SUPPRESS ALL BUT DEVELOPER EMAILS AND CHANGE INPUT TABLE, ETC.

	* wait window properties
	nWaitWindowTimeout = 2
	lWaitWindowIsOn = .T.

	* logfile properties
	lLoggingIsOn = .T.
	cLogFile = 'F:\UTIL\CITIVCAPROC\LOGFILES\CITIVCAPROC_log.txt'

	* date/time properties
	dToday = DATE()
	cStartTime = TTOC(DATETIME())

	* table properties
	cControlTable = 'F:\UTIL\CITIVCAPROC\DATA\VCACTRL'
	cSentTable = ''

	* file properties
	nNewFileHandle = -1
	nSourceFileHandle = -1
	
	* VCARD ALIAS properties
	cVCardAlias = ''

	* PLATINUM ACCOUNT # properties  -- NOT CURRENTLY USED!
	cLast4AcctNum = ''
	cFullAcctNum = ''

	* object properties
	oExcel = NULL
	oWorkbook = NULL
	oWorksheet = NULL

	* INTERNAL email properties
	lSendInternalEmailIsOn = .T.
	cFrom = 'mark.bennett@tollgroup.com'
	cSendTo = 'mark.bennett@tollgroup.com'
	cCC = ''
	cAttach = ''
	cBodyText = ''
	*cSubject = 'xxCitiBank Rewrite Platinum VCARD File process for ' + TRANSFORM(DATETIME())
	cSubject = 'VCARD to Citi File process for ' + TRANSFORM(DATETIME())

	cCOMPUTERNAME = ''
	cUSERNAME = ''

	FUNCTION INIT
		IF NOT DODEFAULT()
			RETURN .F.
		ENDIF
		WITH THIS
			CLEAR
			*SET RESOURCE OFF
			CLOSE DATA
			SET CENTURY ON
			SET DATE YMD
			SET HOURS TO 24
			SET ANSI ON
			SET TALK OFF
			SET DELETED ON
			SET CONSOLE OFF
			SET EXCLUSIVE OFF
			SET SAFETY OFF
			SET EXACT OFF
			SET STATUS BAR ON
			SET SYSMENU OFF
			SET ENGINEBEHAVIOR 70
			SET DECIMALS TO 4
			.cCOMPUTERNAME = GETENV("COMPUTERNAME")
			.cUSERNAME = GETENV("USERNAME")
			IF .lTestMode THEN
				.cSendTo = 'mark.bennett@tollgroup.com'
				.cCC = ''
				.cLogFile = 'F:\UTIL\CITIVCAPROC\LOGFILES\CITIVCAPROC_log_TESTMODE.txt'
			ENDIF
			.lLoggingIsOn = .lLoggingIsOn AND NOT EMPTY(.cLogFile)
			IF .lLoggingIsOn THEN
				SET ALTERNATE TO (.cLogFile) ADDITIVE
				SET ALTERNATE ON
			ENDIF
		ENDWITH
	ENDFUNC


	FUNCTION DESTROY
		WITH THIS
			IF .lLoggingIsOn  THEN
				SET ALTERNATE OFF
				SET ALTERNATE TO
			ENDIF
		ENDWITH
		DODEFAULT()
	ENDFUNC


	FUNCTION MAIN
		LPARAMETERS tcVCardAlias
		WITH THIS
			LOCAL lnNumberOfErrors, lcInputFolder, lcOutputFolder, laFiles[1,5], laFilesSorted[1,6], lcSourceFile, lcTargetFile
			LOCAL lnNumFiles, lnCurrentFile, lcString, lcNewFile, lcNewFileRoot, lcLineOut, lcLineIn, lcSourceFileName, lcArchivedFile
			LOCAL lnRow, lcRow, lnLastRowWithData

			TRY
				lnNumberOfErrors = 0
				lcTopBodyText = ''

				.TrackProgress('Rewrite VCARD File for CitiBank process started....', LOGIT+SENDIT)
				.TrackProgress('COMPUTERNAME = ' + .cCOMPUTERNAME, LOGIT+SENDIT)
				.TrackProgress('USERNAME = ' + .cUSERNAME, LOGIT+SENDIT)
				.TrackProgress('Control Table = ' + .cControlTable, LOGIT+SENDIT)
				.TrackProgress('Project =  CITIVCAPROC', LOGIT+SENDIT)

				IF .lTestMode THEN
					.TrackProgress('                        TEST MODE                        ', LOGIT+SENDIT)
				ENDIF

				IF EMPTY('tcVCardAlias') OR (TYPE('tcVCardAlias') <> 'C') THEN
					.TrackProgress('=======> ERROR: invalid or missing VCARDALIAS parameter!', LOGIT+SENDIT)
					THROW
				ENDIF

				tcVCardAlias = UPPER(ALLTRIM(tcVCardAlias))

				.TrackProgress("tcVCardAlias = '" + tcVCardAlias + "'", LOGIT+SENDIT)

				IF INLIST(tcVCardAlias,"PLATINUM","CARGO WISE") THEN
					.cVCardAlias = tcVCardAlias
					* we need to add BATCH to our alias because Citi changed it at their end. 1/29/2016 MB
					IF .cVCardAlias == "PLATINUM" THEN
						.cVCardAlias = "PLATINUM BATCH"
					ENDIF
				ELSE
					.TrackProgress('=======> ERROR: Unexpected tcVCardAlias value!', LOGIT+SENDIT)
					THROW
				ENDIF

				IF .lTestMode THEN
					lcInputFolder = 'F:\UTIL\CITIVCAPROC\PLATVCA\'
					lcArchiveFolder = 'F:\UTIL\CITIVCAPROC\PLATVCA\ARCHIVED\'
					lcOutputFolder = 'F:\UTIL\CITIVCAPROC\PLATVCA2\'
					.cSentTable = 'F:\UTIL\CITIVCAPROC\TESTDATA\TVCARDSENT'
				ELSE
					lcInputFolder = 'F:\FTPUSERS\CITIBANKOUT\PLATVCA\'
					lcArchiveFolder = 'F:\FTPUSERS\CITIBANKOUT\PLATVCA\ARCHIVED\'
					lcOutputFolder = 'F:\FTPUSERS\CITIBANKOUT\PLATVCA2\'
					.cSentTable = 'F:\UTIL\CITIVCAPROC\DATA\VCARDSENT'
				ENDIF

				.TrackProgress('Input Folder = ' + lcInputFolder, LOGIT+SENDIT)
				.TrackProgress('Archived Folder = ' + lcArchiveFolder, LOGIT+SENDIT)
				.TrackProgress('Output Folder = ' + lcOutputFolder, LOGIT+SENDIT)
				.TrackProgress('Sent Table = ' + .cSentTable, LOGIT+SENDIT)

				USE (.cControlTable) IN 0 ALIAS VCACTRLTABLE

				USE (.cSentTable) IN 0 ALIAS VCASENTTABLE

				lnNumFiles = ADIR(laFiles,(lcInputFolder + "*.CSV"))

				IF lnNumFiles > 0 THEN

					* open Excel
					.oExcel = CREATEOBJECT("Excel.Application")
					.oExcel.VISIBLE = .F.
					.oExcel.displayalerts = .F.

					* sort file list by date/time
					.SortArrayByDateTime(@laFiles, @laFilesSorted)

					FOR lnCurrentFile = 1 TO lnNumFiles

						lcSourceFileName = laFilesSorted[lnCurrentFile,1]

						*!*							* make sure source filename is of format VCAXXXX where XXXX is the passed last 4 digits of the account #
						*!*							IF NOT UPPER(LEFT(lcSourceFileName,7)) == ("VCA" + .cLast4AcctNum) THEN
						*!*
						*!*								.TrackProgress('WARNING: this source filename does not match the account # parameter: ' + lcSourceFileName + CRLF + 'This file will not be processed!', LOGIT+SENDIT)
						*!*
						*!*							ELSE
						* Looks like we have the correct file, ok to process it.

						* create new file in archived folder, will copy source file to ARCHIVED folder for processing....
						lcSourceFile = lcInputFolder + lcSourceFileName
						lcArchivedFile = lcArchiveFolder + lcSourceFileName

						.TrackProgress('Processing Source file: ' + lcSourceFile, LOGIT+SENDIT)

						*lcNewFileRoot = "CITIREQUESTFILE" + .cLast4AcctNum + "_" + DTOS(DATE()) + STRTRAN(TIME(),":","") + ".TXT"
						*lcNewFileRoot = "CITIREQUESTFILE_" + DTOS(DATE()) + STRTRAN(TIME(),":","") + ".CSV"

						lcNewFileRoot = "TOLLGI_FPCRQLP." + RIGHT(DTOS(DATE()),6) + STRTRAN(TIME(),":","") + ".csv"

						* note new file will be created in the input folder, will copy to ftp output folder later
						lcNewFile = lcArchiveFolder + lcNewFileRoot

						.TrackProgress('Intermediate Output file = ' + lcNewFile, LOGIT+SENDIT)

						lcTargetFile = lcOutputFolder + lcNewFileRoot

						.TrackProgress('Final Output file = ' + lcTargetFile, LOGIT+SENDIT)

						** append .TXT to target file if necessary
						*IF NOT UPPER(RIGHT(lcTargetFile,4)) == ".TXT" THEN
						*	lcTargetFile = lcTargetFile + ".TXT"
						*ENDIF

						* move source file to archived folder for further processing
						COPY FILE (lcSourceFile) TO (lcArchivedFile)
						
						
						* 12/12/2016 MB
						* adding a brief delay here, because for the last two weeks we are getting 'access denied' errors on the DELETE below. 
						* Maybe the network is slow at releasing the source file after the copy?
						WAIT WINDOW "Delaying 30 seconds after archiving source file..." TIMEOUT 30
						
						
						IF FILE(lcArchivedFile) THEN
							* delete the original source file in the source folder
							DELETE FILE (lcSourceFile)
						ELSE
							.TrackProgress('*** There was a problem copying [' + lcSourceFile + '] to [' + lcArchivedFile + ']', LOGIT+SENDIT)
							THROW
						ENDIF


						****************** add logic to create proper target filename !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

						**********************************************************************************************
						**********************************************************************************************

						.nNewFileHandle = FCREATE(lcNewFile)

						IF .nNewFileHandle < 1 THEN
							.TrackProgress('!!!! Error opening output file: ' + lcNewFile, LOGIT+SENDIT)
							THROW
						ENDIF
						**********************************************************************************************
						**********************************************************************************************


						* open source file FROM ARCHIVED FOLDER WHERE IT HAS BEEN MOVED
						*!*								.nSourceFileHandle = FOPEN(lcArchivedFile)

						*!*								IF .nSourceFileHandle < 1 THEN
						*!*									.TrackProgress('!!!! Error opening input file: ' + lcArchivedFile, LOGIT+SENDIT)
						*!*									THROW
						*!*								ENDIF

						* INIT VARIABLES
						LOCAL llHeaderWritten, lcHeader, lnRow
						lnRow = 0
						llHeaderWritten = .F.
						lcHeader = 'ActionType,RecordId,IssuerId,UserName,RequestId,MinPurchaseAmount,MaxPurchaseAmount,PurchaseCurrency,PurchaseType,' + ;
							'VCardAlias,SupplierName,SupplierEmail,MultiUse,ValidFrom,ValidTo,ValidFor,CDF_DocumentNumber,CDF_VendorID,CDF_VendorName,CDF_PaymentDate,CDF_InvoiceNumber'


						LOCAL lcPurchaseAmount, lcSupplierEmail, lcDocumentNumber, lcVendorID, lcVendorName, lcPaymentDate, lcInvoiceNumber, lcRecordID, lcSupplierEmail
						STORE '' TO lcPurchaseAmount, lcSupplierEmail, lcDocumentNumber, lcVendorID, lcVendorName, lcPaymentDate, lcInvoiceNumber, lcRecordID, lcSupplierEmail
						
						LOCAL lnPurchaseAmount
						STORE 0.00 TO lnPurchaseAmount
						
						LOCAL ldCDF_PaymentDate, luCDF_InvoiceNumber, luCDF_PaymentDate
						ldCDF_PaymentDate = CTOD('')

						LOCAL lcCDF_DocumentNumber, lcCDF_VendorID, lcCDF_VendorName, lcCDF_PaymentDate, lcCDF_InvoiceNumber
						STORE '' TO lcCDF_DocumentNumber, lcCDF_VendorID, lcCDF_VendorName, lcCDF_PaymentDate, lcCDF_InvoiceNumber

						LOCAL lcActionType, lcIssuerID, lcUserName, lcRequestID, lcPurchaseCurrency, lcPurchaseType, lcVCardAlias, lcSupplierName
						LOCAL lcMultiUse, lnValidForDays, lcValidFor, ldValidFromDate, ldValidToDate, lcValidFrom, lcValidTo

						lcActionType = 'CreateApprovedPurchase'
						
*!*								lcIssuerID = '5'
							
						IF .lTestMode THEN
							* test
							lcIssuerID = '5'
						ELSE
							* production
							lcIssuerID = '4'
						ENDIF
						
						*lcUserName = 'CSVbatchfileuser'
						lcUserName = 'tollusbatch'
						
						lcPurchaseCurrency = '840'
						
						lcPurchaseType = 'Toll Group Standard'
						
						lcVCardAlias = .cVCardAlias
						
						*lcSupplierName = 'CSV Test Generic Supplier'
						lcSupplierName = 'Toll Group Supplier'
						
						lcMultiUse = 'F'
						lnValidForDays = 14
						lcValidFor = ALLTRIM(STR(lnValidForDays)) + 'D'
						ldValidFromDate = .dToday
						ldValidToDate = ldValidFromDate + lnValidForDays
						* 2014-08-07 00:00:00.0 +0000
						* 2014-08-12 23:00:00.0 +0000
						lcValidFrom = ALLTRIM(STR(YEAR(ldValidFromDate))) + "-" + PADL(MONTH(ldValidFromDate),2,'0') + "-" + PADL(DAY(ldValidFromDate),2,'0') + ' 00:00:00.0 +0000'
						lcValidTo =   ALLTRIM(STR(YEAR(ldValidToDate)))   + "-" + PADL(MONTH(ldValidToDate),2,'0')   + "-" + PADL(DAY(ldValidToDate),2,'0')   + ' 23:59:00.0 +0000'
						
						* create a cursor which we will fill with data. If we get thru the entire output file creation process successfully, we will store the cursor
						* data in the SENT history table.
						SELECT * ;
							FROM VCASENTTABLE ;
							INTO CURSOR CURSENT ;
							WHERE .F. ;
							READWRITE

							* Open spreadsheet and extract data
							.oWorkbook = .oExcel.workbooks.OPEN(lcArchivedFile)
							.oWorksheet = .oWorkbook.worksheets[1]
													    
							* determine the last cell with data
						    lnLastRowWithData = .oWorksheet.Cells(.oWorksheet.Rows.Count, "A").End(xlUp).Row
							.TrackProgress('Last Row With Data = ' + TRANSFORM(lnLastRowWithData), LOGIT+SENDIT)							

							FOR lnRow = 2 TO lnLastRowWithData
							
								lcRow = ALLTRIM(STR(lnRow))
	
								.TrackProgress('====> Processing Row ' + lcRow, LOGIT+SENDIT)
							
								luColumnA = .oWorksheet.RANGE("A" + lcRow).VALUE
								IF ISNULL(luColumnA) THEN
									IF lnRow <= lnLastRowWithData THEN
										.TrackProgress('ERROR: unexpected NULL data at cell: A' + lcRow, LOGIT+SENDIT)
										THROW										
									ENDIF
									EXIT FOR
								ENDIF
								
								* RECORD ID SHOULD ALWAYS START AT 1, not 2, PER RICK DEPOLA 8/7/15. 
								* It should not count the header row.
								*lcRecordID = lcRow
								lcRecordID = ALLTRIM(STR(lnRow - 1))
								
								
								lcRequestID = .GetNextRequestID()

								lcCDF_DocumentNumber = ALLTRIM(.oWorksheet.RANGE("A" + lcRow).VALUE)

								lcCDF_VendorID = ALLTRIM(.oWorksheet.RANGE("B" + lcRow).VALUE)

								lcCDF_VendorName = ALLTRIM(.oWorksheet.RANGE("C" + lcRow).VALUE)

								lcSupplierEmail = ALLTRIM(.oWorksheet.RANGE("D" + lcRow).VALUE)

								luCDF_PaymentDate = .oWorksheet.RANGE("E" + lcRow).VALUE
								
								DO CASE
									CASE TYPE('luCDF_PaymentDate') = 'C'
										ldCDF_PaymentDate = luCDF_PaymentDate
									CASE TYPE('luCDF_PaymentDate') = 'D'
										lcCDF_PaymentDate = DTOC( luCDF_PaymentDate )
									CASE TYPE('luCDF_PaymentDate') = 'T'
										lcCDF_PaymentDate = DTOC( TTOD( luCDF_PaymentDate ) )
									OTHERWISE
										.TrackProgress('ERROR: unexpected data TYPE() for Payment Date # at cell: E' + lcRow, LOGIT+SENDIT)
										THROW										
								ENDCASE
								 
								luCDF_InvoiceNumber = .oWorksheet.RANGE("F" + lcRow).VALUE
								DO CASE
									* export from platinum should now be prefixing Invoice # with an apostrophe - so it should always be character 9/21/2015 MB
									*!*	CASE TYPE('luCDF_InvoiceNumber') = 'N'
									*!*		lcCDF_InvoiceNumber = ALLTRIM(STR( luCDF_InvoiceNumber ))
									CASE TYPE('luCDF_InvoiceNumber') = 'C'
										lcCDF_InvoiceNumber = ALLTRIM( luCDF_InvoiceNumber )
										* remove leading apostrophe
										IF LEFT(lcCDF_InvoiceNumber,1) = "'" THEN
											lcCDF_InvoiceNumber = ALLTRIM(SUBSTR(lcCDF_InvoiceNumber,2))
										ENDIF
									OTHERWISE
										.TrackProgress('ERROR: unexpected data TYPE() for Invoice # at cell: F' + lcRow, LOGIT+SENDIT)
										THROW										
								ENDCASE	
								.TrackProgress('lcCDF_InvoiceNumber = ' + lcCDF_InvoiceNumber, LOGIT+SENDIT)
								
								* 04/29/2016 - sometimes there is an embedded comma in the invoice # field, which causes problems processing the csv file because it makes extra columns.
								* Try to detect and handle that problem.
								* This is an example of a problem line in the csv:					
								*    P0000197,DRMED,DOCTORS MEDI CENTER,KRISTINV@DMC-NJ.COM,4/28/2016,'C, AQUILES 040616,-80
								* the comma in 'C, AQUILES 040616, the inv num field, cause inv num to be truncated to 'C, and pushes  AQUILES 040616 into the Purchase Amount field,
								* where it causes a type mismatch error, and it also pushes the correct purchase anmount -80 into column H, which is mot supposed to be used at all.
								* so
								* if value from Col G is _not_ numeric, and value from Col H _is_ numeric, we'll assume the comma problem happened
								
								LOCAL luColH, luColG, lnInvNumLen
								lnInvNumLen = LEN(ALLTRIM(lcCDF_InvoiceNumber))
								.TrackProgress('lnInvNumLen =' + TRANSFORM(lnInvNumLen), LOGIT+SENDIT)
								
								luColG = .oWorksheet.RANGE("G" + lcRow).VALUE
								luColH = .oWorksheet.RANGE("H" + lcRow).VALUE
								
								DO CASE
									CASE (TYPE('luColG') = 'N') AND (TYPE('luColH') != 'N') 
									 	* normal case, continue with standard processing
										lnPurchaseAmount = luColG
									CASE (TYPE('luColG') = 'C') AND (TYPE('luColH') = 'N') 
									 	* PRESUMABLY there was an extra comma in inv num field; get Purchase Amount from Col H instead, and append Col G value to previously gotten Inv Num value.
										lnPurchaseAmount = luColH
										lcCDF_InvoiceNumber = lcCDF_InvoiceNumber + luColG
										.TrackProgress('=====> Got Purchase Amount from Col H (extra comma in INVNUM field?)', LOGIT+SENDIT)
										.TrackProgress('Col G = ' + luColG, LOGIT+SENDIT)
										.TrackProgress('Col H = ' + TRANSFORM(luColH), LOGIT+SENDIT)
									OTHERWISE
										.TrackProgress('=====> UNRESOLVABLE TYPE ERRORS IN COLS G & H', LOGIT+SENDIT)
										THROW
								ENDCASE
								
								
*!*									lnPurchaseAmount = .oWorksheet.RANGE("G" + lcRow).VALUE
								lnPurchaseAmount = ABS( lnPurchaseAmount )  && NEEDED BECAUSE THE EXPORT HAS NEGATIVE AMOUNTS
								lcPurchaseAmount = ALLTRIM(STR(lnPurchaseAmount,10,2))
								
								lcLineOut = ;
									lcActionType + "," + ;
									lcRecordID + "," + ;
									lcIssuerID + "," + ;
									lcUserName + "," + ;
									lcRequestID + "," + ;
									lcPurchaseAmount + "," + ;
									lcPurchaseAmount + "," + ;
									lcPurchaseCurrency + "," + ;
									lcPurchaseType + "," + ;
									lcVCardAlias + "," + ;
									lcSupplierName + "," + ;
									.CleanUpField( lcSupplierEmail ) + "," + ;
									lcMultiUse + "," + ;
									lcValidFrom + "," + ;
									lcValidTo + "," + ;
									lcValidFor + "," + ;
									.CleanUpField( lcCDF_DocumentNumber ) + "," + ;
									.CleanUpField( lcCDF_VendorID ) + "," + ;
									.CleanUpField( lcCDF_VendorName ) + "," + ;
									.CleanUpField( lcCDF_PaymentDate ) + "," + ;
									.CleanUpField( lcCDF_InvoiceNumber )

								* write output Header line - ONLY ONCE
								IF NOT llHeaderWritten THEN
									=FPUTS(.nNewFileHandle, lcHeader)
									llHeaderWritten = .T.
								ENDIF

								* write output detail line
								=FPUTS(.nNewFileHandle ,lcLineOut)
								
								INSERT INTO CURSENT (REQUESTID, VENDORID, DOCNUM, INVNUM, AMOUNT, PYMTDATE, DETAILLINE) ;
								VALUES (lcRequestID, .CleanUpField( lcCDF_VendorID ), .CleanUpField( lcCDF_DocumentNumber ), .CleanUpField( lcCDF_InvoiceNumber ), lnPurchaseAmount, ;
									.CleanUpField( lcCDF_PaymentDate ), lnRow)								

							ENDFOR

							=FCLOSE(.nNewFileHandle)
							

						**************************************************************
						*!* Send file to ftp output directory for poller to pickup
						**************************************************************
						* 'copy file' changes case of filename to lower, so use alternate method from Joe B.
						* cStr = Filetostr(filename), then strtofile(cStr(upper(filename)) to another folder does it for Joe B.
						*COPY FILE (lcSourceFile) TO (lcTargetFile)
						lcString = FILETOSTR(lcNewFile)
						=STRTOFILE(lcString,UPPER(lcTargetFile))
						**************************************************************

						IF FILE(lcTargetFile) THEN
							.TrackProgress('SUCCESS: VCARD request file was copied to the FTP outbound folder!', LOGIT+SENDIT)
							.TrackProgress('Copied [' + lcNewFile + '] to [' + lcTargetFile + ']', LOGIT+SENDIT)
							
							* SUCCESS: save CURSENT to VCASENTTABLE; populate remaining fields first.
							SELECT CURSENT
							SCAN
								REPLACE CURSENT.SOURCE WITH lcSourceFileName, CURSENT.TARGET WITH lcNewFileRoot, CURSENT.SENTDT WITH DATETIME() IN CURSENT							
							ENDSCAN
							
							SELECT CURSENT
							SCAN
								SCATTER MEMVAR
								INSERT INTO VCASENTTABLE FROM MEMVAR							
							ENDSCAN
							
							.TrackProgress('The Sent table has been updated with the request transactions.', LOGIT+SENDIT)
							
						ELSE
							.TrackProgress('ERROR: Rewritten VCARD file was *NOT* copied to the FTP outbound folder!', LOGIT+SENDIT)
							.TrackProgress('*** There was a problem copying [' + lcNewFile + '] to [' + lcTargetFile + ']', LOGIT+SENDIT)
						ENDIF


						*!*							ENDIF && NOT UPPER(LEFT(,7)) == "VCARD" + .cLast4AcctNum THEN

						WAIT WINDOW "Pausing to create unique filenames..." TIMEOUT 65

					ENDFOR && lnCurrentFile = 1 TO lnNumFiles

					.oExcel.QUIT()
					.oExcel = NULL

					* One or more Vcard request files are in the ftp out folder, so trigger the upload to Citi.
					IF NOT .lTestMode THEN
						.TriggerVcardFileFTPUpload()
					ENDIF

					* if we got to here (i.e. we processed a Vcard file without errors), add the Finance folks to the email...
					.cSendTo = 'neil.devine@tollgroup.com, joe.cangelosi@tollgroup.com, george.gereis@tollgroup.com'
					.cCC = 'mark.bennett@tollgroup.com'

				ELSE

					* no files to process
					.TrackProgress('Found no files to process!', LOGIT+SENDIT)

				ENDIF  && lnNumFiles > 0


			CATCH TO loError

				.TrackProgress('There was an error.',LOGIT+SENDIT)
				.TrackProgress('The Error # is: ' + TRANSFORM(loError.ERRORNO), LOGIT+SENDIT)
				.TrackProgress('The Error Message is: ' + TRANSFORM(loError.MESSAGE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred in Program: ' + TRANSFORM(loError.PROCEDURE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred at line #: ' + TRANSFORM(loError.LINENO), LOGIT+SENDIT)
				lnNumberOfErrors = lnNumberOfErrors + 1
				
				IF TYPE('.oExcel') = 'O' AND NOT ISNULL(.oExcel) THEN
					.oExcel.QUIT()
				ENDIF

				IF .nNewFileHandle > 0 THEN
					=FCLOSE(.nNewFileHandle)
				ENDIF


			ENDTRY



			*!*				CLOSE DATABASES all

			WAIT CLEAR

			***************** INTERNAL email results ******************************
			.TrackProgress('About to send status email.',LOGIT)
			.TrackProgress('==================================================================================================================', SENDIT)
			.TrackProgress('The logfile is: ' + .cLogFile, SENDIT)
			.TrackProgress('Rewrite VCARD File for CitiBank process started: ' + .cStartTime, LOGIT+SENDIT)
			.TrackProgress('Rewrite VCARD File for CitiBank process finished: ' + TTOC(DATETIME()), LOGIT+SENDIT)

			IF .lSendInternalEmailIsOn THEN
				* try to trap error from not having dartmail dll's registered on user's pc...
				TRY
					DO FORM dartmail2 WITH .cSendTo,.cFrom,.cSubject,.cCC,.cAttach,.cBodyText,"A"
					.TrackProgress('Sent status email.',LOGIT)
				CATCH TO loError
					.TrackProgress('There was an error sending the status email.',LOGIT)
					.TrackProgress('The Error # is: ' + TRANSFORM(loError.ERRORNO), LOGIT)
					.TrackProgress('The Error Message is: ' + TRANSFORM(loError.MESSAGE), LOGIT)
					.TrackProgress('The Error occurred in Program: ' + TRANSFORM(loError.PROCEDURE), LOGIT)
					.TrackProgress('The Error occurred at line #: ' + TRANSFORM(loError.LINENO), LOGIT)
					lnNumberOfErrors = lnNumberOfErrors + 1
					*!*						CLOSE DATA
				ENDTRY

			ELSE
				.TrackProgress('Did not send status email: THIS.lSendInternalEmailIsOn = FALSE.',LOGIT)
			ENDIF

		ENDWITH
		RETURN
	ENDFUNC && main


	PROCEDURE TriggerVcardFileFTPUpload
		* trigger the FTP upload process
		INSERT INTO F:\edirouting\sftpjobs (jobname) VALUES ("TGF-VCARD-TO-CITI")
		.TrackProgress('Triggered FTP job TGF-VCARD-TO-CITI', LOGIT+SENDIT) 
		RETURN
	ENDPROC


	FUNCTION CleanUpField
		* prepare field for inclusion in the string to be written with FPUTS().
		* especially critical to remove any commas, since that creates bogus column in CSV output file.
		LPARAMETERS tcValue
		.TrackProgress('In CleanUpField(), tcValue = ' + TRANSFORM(tcValue),LOGIT)
		.TrackProgress('In CleanUpField(), TYPE of tcValue = ' + TYPE('tcValue'),LOGIT)
		LOCAL lcRetVal
		lcRetVal = STRTRAN(tcValue,",","")
		RETURN lcRetVal
	ENDFUNC


	FUNCTION GetNextRequestID
		LOCAL lnRequestID, lcRequestID

		SELECT VCACTRLTABLE
		lnRequestID = VCACTRLTABLE.requestid + 1
		REPLACE VCACTRLTABLE.requestid WITH lnRequestID IN VCACTRLTABLE

		lcRequestID = PADL(lnRequestID,6,'0')

		RETURN lcRequestID
	ENDFUNC


	FUNCTION ExecSQL
		LPARAMETERS tcSQL, tcCursorName, tlNoDataReturnedIsOkay
		LOCAL llRetval, lnResult
		WITH THIS
			* close target cursor if it's open
			IF USED(tcCursorName)
				USE IN (tcCursorName)
			ENDIF
			WAIT WINDOW "Fetching remote data; please wait..." NOWAIT
			lnResult = SQLEXEC(.nSQLHandle, tcSQL, tcCursorName)
			llRetval = ( lnResult > 0 )
			IF llRetval THEN
				* see if any data came back
				IF NOT tlNoDataReturnedIsOkay THEN
					IF (NOT USED(tcCursorName)) OR EOF(tcCursorName) THEN
						llRetval = .F.
						.TrackProgress('No data was returned by this query: ' +CRLF + tcSQL, LOGIT+SENDIT)
					ENDIF
				ENDIF
			ELSE
				.TrackProgress("Result Code [" + TRANSFORM(lnResult) + "] returned by this query: " + tcSQL, LOGIT+SENDIT)
			ENDIF
			WAIT CLEAR
			RETURN llRetval
		ENDWITH
	ENDFUNC


	PROCEDURE TrackProgress
		* do any combination of Wait Window, writing to logfile, and adding to body of email,
		* based on nFlags parameter.
		LPARAMETERS tcExpression, tnFlags
		LOCAL lcCRLF
		WITH THIS
			IF BITAND(tnFlags,NOCRLF) = NOCRLF THEN
				lcCRLF = ""
			ELSE
				lcCRLF = CRLF
			ENDIF
			IF BITAND(tnFlags,LOGIT) = LOGIT THEN
				IF .lLoggingIsOn THEN
					?
					? .cProcessName + "  " + TTOC(DATETIME()) + ": " + tcExpression
				ENDIF
			ENDIF
			IF .lWaitWindowIsOn THEN
				IF BITAND(tnFlags,NOWAITIT) = NOWAITIT THEN
					WAIT WINDOW tcExpression NOWAIT
				ENDIF
				IF BITAND(tnFlags,WAITIT) = WAITIT THEN
					WAIT WINDOW tcExpression TIMEOUT .nWaitWindowTimeout
				ENDIF
			ENDIF
			IF BITAND(tnFlags,SENDIT) = SENDIT THEN
				IF .lSendInternalEmailIsOn THEN
					.cBodyText = .cBodyText + tcExpression + CRLF + lcCRLF
				ENDIF
			ENDIF
		ENDWITH
	ENDPROC  &&  TrackProgress


	PROCEDURE SortArrayByDateTime
		* expects taInArray[] to be the result of an ADIR()
		LPARAMETERS taInArray, taOutArray
		EXTERNAL ARRAY taInArray, taOutArray
		LOCAL lnRows, lnCols, i, j
		* extending taInArray[] to process the files in date/time order
		lnRows = ALEN(taInArray,1)
		lnCols = ALEN(taInArray,2)
		* create taOutArray with one more column
		DIMENSION taOutArray[lnRows,lnCols + 1]
		* and fill it with taInArray columns AND (filedate + filetime) in new column
		* ( ACOPY() doesn't preserve elements properly because of different # of columns )
		FOR i = 1 TO lnRows
			FOR j = 1 TO lnCols
				taOutArray[i,j] = taInArray[i,j]
			ENDFOR
			taOutArray[i,lnCols + 1] = DTOS(taInArray[i,3]) + "_" + taInArray[i,4]
		ENDFOR
		* sort taOutArray on date/time column
		=ASORT(taOutArray,6)
	ENDPROC

ENDDEFINE


*!*							llCSV = .T.
*!*							IF llCSV THEN


							*SELECT CURSENT
							*BROWSE

*!*							ELSE


*!*								* read through source file, extracting and writing to new file
*!*								DO WHILE NOT FEOF(.nSourceFileHandle)

*!*									lcLineIn = ALLTRIM(FGETS(.nSourceFileHandle,200))

*!*									lcRecTypeCode = LEFT(lcLineIn,1)

*!*									lnRow = lnRow + 1
*!*									lcRecordID = ALLTRIM(STR(lnRow))
*!*									lcRequestID = .GetNextRequestID()

*!*									DO CASE
*!*										CASE lcRecTypeCode == '~'

*!*											*!*											* if we get to here with an empty Value Date, error out
*!*											*!*											IF EMPTY(lcValueDate) THEN
*!*											*!*												.TrackProgress('!!!!! ERROR: empty Value Date encountering Rec Type 6 - aborting process.', LOGIT+SENDIT)
*!*											*!*												THROW
*!*											*!*											ENDIF

*!*											* get Payment Amount
*!*											lcPurchaseAmount = SUBSTR(lcLineIn,2,10)

*!*											lcSupplierEmail = ALLTRIM( SUBSTR(lcLineIn,12,50) )

*!*											lcCDF_DocumentNumber = ALLTRIM( SUBSTR(lcLineIn,62,10) )

*!*											lcCDF_VendorID = ALLTRIM( SUBSTR(lcLineIn,72,10) )

*!*											lcCDF_VendorName = ALLTRIM( SUBSTR(lcLineIn,82,20) )

*!*											lcCDF_PaymentDate = SUBSTR(lcLineIn,102,8)

*!*											lcCDF_InvoiceNumber = ALLTRIM( SUBSTR(lcLineIn,110,10) )

*!*											*SET STEP ON

*!*											lcLineOut = ;
*!*												lcActionType + "," + ;
*!*												lcRecordID + "," + ;
*!*												lcIssuerID + "," + ;
*!*												lcUserName + "," + ;
*!*												lcRequestID + "," + ;
*!*												lcPurchaseAmount + "," + ;
*!*												lcPurchaseAmount + "," + ;
*!*												lcPurchaseCurrency + "," + ;
*!*												lcPurchaseType + "," + ;
*!*												lcVCardAlias + "," + ;
*!*												lcSupplierName + "," + ;
*!*												.CleanUpField( lcSupplierEmail ) + "," + ;
*!*												lcMultiUse + "," + ;
*!*												lcValidFrom + "," + ;
*!*												lcValidTo + "," + ;
*!*												lcValidFor + "," + ;
*!*												.CleanUpField( lcCDF_DocumentNumber ) + "," + ;
*!*												.CleanUpField( lcCDF_VendorID ) + "," + ;
*!*												.CleanUpField( lcCDF_VendorName ) + "," + ;
*!*												.CleanUpField( lcCDF_PaymentDate ) + "," + ;
*!*												.CleanUpField( lcCDF_InvoiceNumber )

*!*											* write output Header line - ONLY ONCE
*!*											IF NOT llHeaderWritten THEN
*!*												=FPUTS(.nNewFileHandle ,lcHeader)
*!*												llHeaderWritten  = .T.
*!*											ENDIF

*!*											* write output detail line
*!*											=FPUTS(.nNewFileHandle ,lcLineOut)


*!*										OTHERWISE
*!*											* unexpected rec type - just skip it

*!*									ENDCASE


*!*									*!*									* write output file
*!*									*!*									lcLineOut = lcLineIn
*!*									*!*									=FPUTS(.nNewFileHandle ,lcLineOut)

*!*								ENDDO

*!*								=FCLOSE(.nNewFileHandle)

*!*							ENDIF && llCSV

						*!*								=FCLOSE(.nSourceFileHandle)

