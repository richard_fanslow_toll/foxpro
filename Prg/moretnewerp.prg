* Moret file for new ERP accounts

utilsetup("MORETNEWERP")

if holiday(date())
	return
endif

xacctfilter="(inlist(accountid,"+gmoretacctlist+") or inlist(accountid,"+gmoretacctlist2+"))"


**high point (5452) still on old erp - mvw 01/09/12
**HPD now on new erp - mvw 05/07/12
*xacctfilter=xacctfilter+" and accountid#5452"

**added ONTARIO - mvw 10/15/15
**added CARSON - mvw 12/14/15
* removed Mira Loma dy 10/29/17
for xrun = 1 to 1
	do case
	case xrun=1
		goffice="7"
		xwhse="FMISP"
		xsqlexec("select * from inven where mod='7'",,,"wh")
		xsqlexec("select * from project where mod='7'",,,"wh")
	case xrun=2
		goffice="L"
		xwhse="FMIML"
		xsqlexec("select * from inven where mod='L'",,,"wh")
		xsqlexec("select * from project where mod='L'",,,"wh")
	endcase

	*

	**the else is JMI (01) - JMI is multiple accts (jockey, jmi kids, etc) - 01/09/12
	**Moret SK accts (6401-6410) added as divsion 06 - mvw 01/07/12
	select accountid,;
		iif(accountid=5451,"02",iif(accountid=5453,"03",iif(accountid=6262,"08",iif(accountid=6261,"05",iif(accountid=5452,"04",;
		iif(inlist(accountid,&gMoretSK),"06","01")))))) as company, ;
		"    " as div, ;
		xwhse as whse, ;
		style, ;
		space(len(color)) as color, ;
		totqty ;
		from inven ;
		where &xacctfilter ;
		and units ;
		and totqty#0 ; 
		order by style ;
		into cursor xrpt readwrite

	index on style tag stylecolid

	xsqlexec("select * from outship where mod='"+goffice+"' and pulled=1 and (notonwip=0 or (notonwip=1 and sp=1)) and del_date={} and "+xacctfilter,,,"wh")

	select outship
	**need to include sp wos as well - mvw 01/28/13
	scan for pulled and (!notonwip or (notonwip and sp)) and del_date={} and &xacctfilter
		**ignore sp wos that are already completed - mvw 01/28/13
		if notonwip and sp
			select project
			**must look in all wonum fields as multipl wos can be associated with a single project - mvw 04/12/13
			xwofilter="(wo_num=outship.wo_num or wonum1=outship.wo_num or wonum2=outship.wo_num or wonum3=outship.wo_num or "+;
				"wonum4=outship.wo_num or wonum5=outship.wo_num or wonum6=outship.wo_num or wonum7=outship.wo_num or "+;
				"wonum8=outship.wo_num or wonum9=outship.wo_num or wonum10=outship.wo_num or wonum11=outship.wo_num or "+;
				"wonum12=outship.wo_num or wonum13=outship.wo_num or wonum14=outship.wo_num or wonum15=outship.wo_num or wonum16=outship.wo_num or wonum17=outship.wo_num or wonum18=outship.wo_num or wonum19=outship.wo_num)"

			locate for accountid=outship.accountid and &xwofilter and completeddt={} 
			if !found()
				**need to include any wos that have not yet been added to project.dbf - mvw 02/26/13
				locate for accountid=outship.accountid and &xwofilter
				if found()
					loop
				endif
			endif
		endif

		xsqlexec("select * from outdet where outshipid="+transform(outship.outshipid)+" and totqty>0 and units=1",,,"wh")
		
		select outdet
		scan for outshipid=outship.outshipid and totqty>0 and units
			scatter memvar fields style, totqty, accountid
			**Moret SK accts (6401-6410) added as divsion 06 - mvw 01/07/12
			m.company=iif(accountid=5451,"02",iif(accountid=5453,"03",iif(accountid=6262,"08",iif(accountid=6261,"05",iif(accountid=5452,"04",;
				iif(inlist(accountid,&gMoretSK),"06","01"))))))

			select xrpt
			locate for accountid=m.accountid and style=m.style
			if !found()
				insert into xrpt from memvar
			else
				replace totqty with xrpt.totqty+m.totqty in xrpt
			endif
		endscan
	endscan

	replace all whse with xwhse in xrpt

	select xrpt
	set order to &&must turn off index for below update to work for like styles - mvw 06/26/12
	scan
		if upcmastsql(xrpt.accountid,xrpt.style)
			xdiv=getmemodata("info","DIV")
			replace div with xdiv in xrpt

			xmoretstyle=getmemodata("info","STYLE")
			if !(empty(xmoretstyle) or xmoretstyle="NA")
				replace style with xmoretstyle in xrpt
				xmoretcolor=getmemodata("info","COLOR")
				replace color with iif(xmoretcolor="NA","",xmoretcolor) in xrpt
			endif
		endif
	endscan

	select xrpt
	set order to stylecolid &&rest order for report - mvw 06/26/12
	copy fields except accountid to h:\pdf\moretnewerp.xls xls

	tsendto="lupe@moret.com,ed.kurowski@tollgroup.com,hak.ong@tollgroup.com,mwinter@fmiint.com"

	tcc=""
	if inlist(xwhse,"FMIML","FMION")
		tcc="jimmy.saracay@tollgroup.com"
	endif

	tattach="h:\pdf\moretnewerp.xls"
	tFrom="TGF <transload-ops@fmiint.com>"
	tmessage=""
	tSubject="Inventory file for "+xwhse

	Do Form dartmail2 With tsendto,tFrom,tSubject,tcc,tattach,tmessage,"A"
endfor

*

schedupdate()

_screen.Caption=gscreencaption
on error
