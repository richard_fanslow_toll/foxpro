*!* m:\dev\prg\bcny940edi_process.prg
WAIT WINDOW "Now in PROCESS Phase..." NOWAIT

*CD &lcPath
lc997Path = 'F:\FTPUSERS\BCNY\997IN\'

LogCommentStr = "LOADED"

ll940test = .f.

If ll940test
  xpath = "f:\ftpusers\bcny\940intest\"
  lnNum = Adir(tarray,xpath+"*.out")
Else
  lnNum = Adir(tarray,lcPath+"*.out")
Endif

*lnNum = ADIR(tarray,"*.out")

IF lnNum = 0
	WAIT WINDOW AT 10,10 "      No "+cCustname+" 940s to import     " TIMEOUT 3
	NormalExit = .T.
	THROW
ENDIF

ASSERT .F. MESSAGE "At initial input file loop"

FOR thisfile = 1  TO lnNum
  If ll940test
    xfile = xpath+tarray[thisfile,1] && +"."
  Else
    xfile = lcPath+tarray[thisfile,1] && +"."
  Endif
*	Xfile = lcPath+tarray[thisfile,1] && +"."

	cfilename = ALLTRIM(LOWER(tarray[thisfile,1]))
	nFileSize = VAL(TRANSFORM(tarray(thisfile,2)))
	IF cfilename = "toll_997"
		WAIT WINDOW "Moving 997 file..." NOWAIT
		c997file = lc997Path+cfilename
		DELETE FILE [&xfile]
		LOOP
	ENDIF
	WAIT WINDOW "" &message_timeout2

	WAIT WINDOW "Importing file: "+cfilename &message_timeout2
	lcStr = FILETOSTR(Xfile)
	cDelimiter = SUBSTR(lcStr,4,1)  && they are sending us either ^ or *
	RELEASE lcStr
	cTranslateOption = "CARROT"
	DO m:\dev\prg\loadedifile WITH Xfile,cDelimiter,cTranslateOption,cCustname

	SELECT x856
	LOCATE

	LOCATE
	LOCATE FOR x856.segment = "N9" AND x856.f1 = "AT"
	IF FOUND()
		IF x856.f2 = "SYNC"  && Synclaire 940
			LOCATE FOR ALLT(x856.segment) = "N9" AND ALLT(x856.f1) = "WH"  && Added to skip the 2nd move in the Synclaire 940 process.
			lCA = IIF(ALLT(x856.f2) = "CA",.T.,.F.)
			cMoveWhse = IIF(lCA,"CA","NJ")
			WAIT WINDOW "This is a SYNCLAIRE "+cMoveWhse+" file...moving..." TIMEOUT 2
			cSynclaireFile = ('F:\FTPUSERS\Synclaire\940IN-'+cMoveWhse+'\'+cfilename)
			COPY FILE [&xfile] TO [&cSynclaireFile]
			DELETE FILE [&xfile]
			RELEASE cMoveWhse
			LOOP
		ENDIF
	ENDIF

*!* Code below to move CA files to the correct folder for processing, added 06.12.2014
	IF !lTesting
*		SET STEP ON
		LOCATE FOR ALLT(x856.segment) = "N9" AND ALLT(x856.f1) = "WH"
		lCA = IIF(ALLT(x856.f2) = "CA",.T.,.F.)
		lCR = IIF(ALLT(x856.f2) = "CR",.T.,.F.)
		IF 	INLIST(cOffice,"N","I")  AND (lCA OR lCR) && Brought in to NJ folder, but a CA/CR file
			DO CASE
				CASE lCA OR lCR&& San Pedro
					WAIT WINDOW "File "+cfilename+" is a San Pedro file...moving" TIMEOUT 2
					RELEASE lCA
					lcPath2 = STRTRAN(ALLTRIM(lcPath),"NJ","CA")
					transfile = (lcPath2+cfilename)
					COPY FILE [&xfile] TO [&transfile]
					IF FILE(Xfile)
						DELETE FILE [&xfile]
					ENDIF
					LOOP
			ENDCASE
		ENDIF
	ENDIF

	LOCATE FOR x856.segment = "GS" AND x856.f1 = "RG"
	IF FOUND() && If a 754
		c754XferFile = ("F:\FTPUSERS\BCNY\754in\"+cfilename)
		COPY FILE [&xfile] TO [&c754XferFile]  && Moves 754 files to the correct folder
		DELETE FILE [&xfile]
		RELEASE c754XferFile
		LOOP
	ENDIF
	IF lTesting
		LOCATE
	ENDIF

	ArchiveFile  = (lcArchivePath+cfilename)
	COPY FILE [&xfile] TO [&ArchiveFile]
	c940XferFile = ("f:\ftpusers\bcny\940xfer\"+cfilename)
	COPY FILE [&xfile] TO [&c940XferFile]  && Creates a copy for 997s to run from

	DO create_pt_cursors WITH cUseFolder,lLoadSQL  && Added to prep for SQL Server transition

	DO ("m:\dev\prg\"+cCustname+"940edi_bkdn")

set step On 

	DO m:\dev\prg\all940_import

*	release_ptvars()

NEXT thisfile

WAIT CLEAR

&& now clean up the 940in archive files, delete for the upto the last 10 days
**deletefile(lcArchivePath,IIF(DATE()<{^2016-05-01},20,10))

WAIT "Entire "+cCustname+" 940 Process Now Complete" WINDOW &message_timeout2
NormalExit = .T.
