runack("EDIPOLLER_IN")

CLOSE DATABASES ALL
WITH _SCREEN
	.WINDOWSTATE = 0
	.BORDERSTYLE = 1
	.WIDTH = 260
	.HEIGHT = 210
	.CLOSABLE = .F.
	.MINBUTTON = .F.
	.MAXBUTTON = .t.
	.SCROLLBARS = 0
	.LEFT = 380
	.TOP = 570
	.CAPTION = "INBOUND POLLER - EDI"
ENDWITH

TRY
	PUBLIC normalexit
	normalexit=.F.
	DO m:\dev\prg\_setvars
	SET STATUS BAR OFF
	SET SYSMENU OFF
	on error throw

	SELECT 0
	USE F:\3pl\DATA\edipollertrip SHARED ALIAS ein
	REPLACE ein.main_in WITH .F.
	USE IN ein

	SELECT 0
	USE F:\3pl\DATA\last945time ALIAS inlast SHARED
	REPLACE checkflag WITH .T. FOR poller = "IN"
	REPLACE tries WITH 0 FOR poller = "IN"
	USE IN inlast

	DO FORM m:\dev\frm\edi_JOBHANDLER_IN

CATCH TO oErr
	ASSERT .F. MESSAGE "AT CATCH SECTION"
	tfrom    ="TGF EDI Processing Center <tgf-transload-ops@fmiint.com>"
	tsubject = "Inbound EDI Poller Error ("+TRANSFORM(oErr.ERRORNO)+") at "+TTOC(DATETIME())
	tattach  = ""
	SELECT 0
	USE F:\3pl\DATA\mailmaster ALIAS mm SHARED
	LOCATE FOR mm.edi_type = "MISC" AND mm.taskname = "GENERAL"
	lUseAlt = mm.use_alt
	tsendto = IIF(lUseAlt,mm.sendtoalt,mm.sendto)
	tcc = IIF(lUseAlt,mm.ccalt,mm.cc)
	USE IN mm

	tmessage = "Inbound 944 Poller Major Error..... Please fix me........!"
	lcSourceMachine = SYS(0)
SET STEP ON 
	tmessage =tmessage+ "Try/CATCH Exeception Message:"+CHR(13)+;
		[  Error: ] + STR(oErr.ERRORNO) +CHR(13)+;
		[  LineNo: ] + STR(oErr.LINENO) +CHR(13)+;
		[  Message: ] + oErr.MESSAGE +CHR(13)+;
		[  Procedure: ] + oErr.PROCEDURE +CHR(13)+;
		[  Details: ] + oErr.DETAILS +CHR(13)+;
		[  StackLevel: ] + STR(oErr.STACKLEVEL) +CHR(13)+;
		[  LineContents: ] + oErr.LINECONTENTS+CHR(13)+;
		[  UserValue: ] + oErr.USERVALUE+CHR(13)+;
		[  Computer:  ] +lcSourceMachine+CHR(13)
	DO FORM m:\dev\FRM\dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
	SELECT 0
	USE F:\3pl\DATA\edipollertrip SHARED ALIAS ein
	REPLACE ein.main_in WITH .T. IN ein
FINALLY
	CLOSE DATABASES ALL
	ON ERROR
	set status bar ON
ENDTRY
