lparameter xcity, xstate, xzip, xcsz, xcs

**xcsz: if not empty, need to parse out csz to memory variables (m.city, m.stste, m.zip, m.country) 
**		as opposed to creating and returning csz - mvw 01/23/08

if type("xcity")="L"
	xcity=''
endif
if type("xstate")="L"
	xstate=''
endif
if type("xzip")="L"
	xzip=''
endif

do case
case empty(xcsz) and empty(xstate) and empty(xcs)
	return trim(xcity)

case empty(xcsz) and empty(xcs)
	return trim(xcity)+iif(empty(xcity),"",", ")+xstate+" "+xzip

case !empty(xcsz)
	xcsz = strtran(xcsz,"PQ","QC")	&& fix Quebec

	store "" to m.zip,m.state,m.city,m.country
	aa=strtran(strtran(trim(xcsz),","," "),"."," ")
	for xdq=1 to 5
		aa=strtran(aa,"  "," ")
	next

	**HASTINGS ON HUDSON, NY (an actual town) was casuing error, so exclude that town specifically from Canada - mvw 01/06/15
	if ((" AB "$xcsz or " BC "$xcsz or " MB "$xcsz or " NB "$xcsz or " NL "$xcsz or " NS "$xcsz or " NT "$xcsz or " ON "$xcsz or " NU "$xcsz or " PE "$xcsz or " QC "$xcsz or " SK "$xcsz or " YK "$xcsz) ;
	or (",AB "$xcsz or ",BC "$xcsz or ",MB "$xcsz or ",NB "$xcsz or ",NL "$xcsz or ",NS "$xcsz or ",NT "$xcsz or ",ON "$xcsz or ",NU "$xcsz or ",PE "$xcsz or ",QC "$xcsz or ",SK "$xcsz or ",YK "$xcsz) ;
	or "QC,"$xcsz) and !("HASTINGS ON HUDSON"$xcsz)
		m.country="124"
		aa=strtran(aa,"-","")
		bb=occurs(" ",aa)

		if bb>1
			cc=at(" ",aa,bb)
			m.zip=substr(aa,cc+1,7)

			if len(trim(m.zip))<4
				cc=at(" ",aa,bb-1)
				m.zip=strtran(substr(aa,cc+1,7)," ","")
				dd=at(" ",aa,1)
			else
				dd=at(" ",aa,1)
			endif

			if bb=2
				m.state=substr(aa,dd+1,cc-dd-1)
				m.city=left(aa,dd-1)
			else
				ee=at(" ",aa,2)
				m.state=substr(aa,ee+1,2)
				m.city=left(aa,ee)
			endif
		endif
		
	else  && USA or other
		m.country="840"
		ee=occurs("-",aa)

		if ee>10
			aa=left(aa,ee-1)
		endif

		bb=occurs(" ",aa)
		if bb>1
			cc=at(" ",aa,bb)

			dd=at(" ",aa,bb-1)
			m.state=substr(aa,dd+1,cc-dd-1)
			m.city=left(aa,dd-1)

			if inlist(m.state,"AL","AK","AZ","AR","CA","CO","CT","DE","FL","GA","HI","ID","IL","IN","IA","KS","KY","LA") ;
			or inlist(m.state,"ME","MD","ME","MI","MN","MS","MO","MT","NE","NV","NH","NJ","NM","NY","NC","ND","OH","OK") ;
			or inlist(m.state,"OR","PA","PR","RI","SC","SD","TN","TX","UT","VT","VA","VI","WA","WV","WI","WY")
				m.zip=substr(aa,cc+1,5)
			else
				m.zip=substr(aa,cc+1)
			endif
		endif
	endif

otherwise
	aa=trim(xcs)
	m.state=right(aa,2)
	m.city=strtran(left(aa,len(aa)-3),",","")
	if !inlist(m.state,"AK","AL","AR","AZ","CA","CO","CT","DC","DE","FL","GA","IA","ID","IL","IN","KS","KY","LA","MA","MD","ME","MI","MN","MO","MS") ;
	and !inlist(m.state,"MT","NC","ND","NE","NH","NJ","NM","NV","NY","OH","OK","OR","PA","RI","SC","SD","TN","TX","UT","VA","VT","WA","WI","WV","WY")
		m.city=""
		m.state=""
	endif
endcase
