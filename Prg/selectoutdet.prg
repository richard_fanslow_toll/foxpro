PARAMETERS DoOV

csq1 = [select distinct outshipid from outship where bol_no = ']+cBOL+[']
xsqlexec(csq1,"oslist",,"wh")

IF RECCOUNT("oslist")>0
	LOCATE
	xjfilter="outshipid in ("
	SCAN
		xjfilter=xjfilter+TRANSFORM(outshipid)+","
	ENDSCAN
	xjfilter=LEFT(xjfilter,LEN(xjfilter)-1)+")"
ELSE
	xjfilter="1=0"
ENDIF

xsqlexec("select * from outdet where "+xjfilter,,,"wh")

IF  lOverflow = .T. OR (accountid = 6803 AND lDoOV = .T.)
*!* Added the zoutdet extract/append below to repair a problem with OV orders, 09.07.2017, Joe...ZOUTDET contains intact deleted OUTDET records
	xsqlexec("select * from zoutdet where "+xjfilter,,,"wh")
	SELECT outdet
	APPEND FROM DBF('zoutdet')
	USE IN zoutdet
ENDIF

SELECT outdet
LOCATE
SCAN FOR EMPTY(outdet.printstuff)
	noutdetid = outdet.outdetid			&& dy 12/26/17
	xsqlexec("select printstuff from zmemo where xtable='OUTDET' and xid = "+TRANSFORM(noutdetid),,,"wh")
	cps = zmemo.printstuff
	IF !EMPTY(cps)
		REPLACE outdet.printstuff WITH cps IN outdet NEXT 1
	ENDIF
ENDSCAN

USE IN oslist
RETURN
