*!* AGEGROUP 945 CSV (Whse. Shipping Advice) Creation Program
*!* Creation Date: 06.30.2016 by Joe

PARAMETERS cBOL,cOffice,nAcctNum

** try to define all variables that may be included in the "Catch Email in case of an error
** that is not the result of a deliberate Throw.......

PUBLIC nWO_num,cWO_Num,cWO_NumStr,cWO_NumOld,cShip_ref,cCustname,nUnitSum,lDoSQLConnect,cErrMsg,lTesting
PUBLIC lAGFilesout,lTestmail,lTestinput,lDoCompare,cTimeMsg,cBOLTest,tfrom,cMBOL,lDoUnitsLines,lcOutPath,cSubBOLDetail
PUBLIC cMod,cFilenameShort,cFilenameOut,cFilenameArch,lParcelType,cWO_NumList,lcPath,cEDIType,cISA_Num,lOverflow

lTesting = .F.
lTestinput = .F.
lDoSQLConnect = .T.
lDoCompare = !lTesting

cErrMsg ="TOP LEVEL"
cTimeMsg = IIF(lTesting,"TIMEOUT 2","NOWAIT")
lDoCatch = .T.
tsendtotest=""
tsendtoerr=""
tcctest=""

cCustname ="AGEGROUP"

DO m:\dev\prg\_setvars WITH lTesting

ON ESCAPE CANCEL

TRY
	STORE 0 TO  nFilenum,nSCCSuffix,nOrigSeq,nOrigGrpSeq
	STORE "" TO cWO_NumStr,cWO_NumList,cWO_Num,cShip_ref
	STORE "XXX" TO cWO_NumOld

	lIsError = .F.
	lDoCatch = .T.

	cPPName = "AGEGROUP"
	lISAFlag = .T.
	lSTFlag = .T.
	lJCPenney = .F.
	lCloseOutput = .T.
	lParcelType = .F.
	tfrom = "Toll EDI Operations <toll-edi-ops@tollgroup.com>"
*	cEDIType = "CSV"
	lOverflow = .F.

	IF TYPE("cOffice") = "L"
		cOffice = "Y"
	ENDIF

	IF lTesting
		CLOSE DATABASES ALL
		DO m:\dev\prg\lookups
		cBOL = "09226910000365759"
		cOffice = "Y"
	ENDIF
	IF EMPTY(cBOL) AND lTesting
		WAIT WINDOW "Empty BOL#" TIMEOUT 2
	ENDIF

*	nAcctNum = IIF(cOffice = "C",6769,1285)
	cISA_Num = RIGHT(ALLTRIM(cBOL),6)
	gMasterOffice = IIF(cOffice = "I","N",cOffice)
	cMod = ICASE(cOffice = "N","I",cOffice = "C","1",cOffice)
	gOffice = cMod
	cEDIType = "945"
	cBOL = ALLTRIM(cBOL)
	cMBOL = cBOL
	IF cBOL = "9999"
		lOverflow = .T. && Flag set for overflow processing
		SET DELETED OFF
	ENDIF
	cSubBOLDetail =  ""


*!* SET CUSTOMER CONSTANTS
	cCustname = "AGEGROUP"  && Customer Identifier
	cMailName = IIF(nAcctNum=6769,"Essential Brands","Age Group")

	SELECT 0

	lEmail = !lTesting
	lTestmail = lTesting && Sends mail to Joe only
	lAGFilesout = .T. && If true, copies output to FTP folder (default = .t.)

*!*	lAGFilesout = .F.
*!*	lTestMail = .T.

	STORE "L" TO cWeightUnit
	lPrepack = .F.
	lPick = !lPrepack
	xReturn = "XXX"

	WAIT WINDOW "Now preparing tables and variables" NOWAIT NOCLEAR

*!* SET OTHER CONSTANTS
	IF cOffice = "N"
		cCustLoc =  "NJ"
		cFMIWarehouse = ""
		cCustPrefix = "945agn"
		cDivision = "New Jersey"
		cSF_Addr1  = "800 FEDERAL BLVD"
		cSF_CSZ    =  "CARTERET*NJ*07008"

	ELSE
		cCustLoc =  "C2"
		cFMIWarehouse = ""
		cCustPrefix = "945agc"
		cDivision = "Carson II"
		cSF_Addr1  = "1000 E 223RD ST"
		cSFCITY    = "CARSON"
		cSFSTATE = "CA"
		cSFZIP = "90745"
	ENDIF

	dt1 = TTOC(DATETIME(),1)
	dtmail = TTOC(DATETIME())
	dt2 = DATETIME()
	cString = ""

	IF !USED("mm")
		USE F:\3pl\DATA\mailmaster ALIAS mm IN 0
	ENDIF
	SELECT mm
	LOCATE FOR edi_type = '945' AND mm.office = cOffice AND mm.accountid = nAcctNum
	IF !FOUND()
		cErrMsg = "MAIL GROUP NOT FOUND"
		THROW
	ENDIF
	tsendto = IIF(mm.use_alt,sendtoalt,mm.sendto)
	tcc = IIF(mm.use_alt,mm.ccalt,mm.cc)
	lcHoldpath = ALLTRIM(mm.holdpath)+"mbolhold\"

	CD &lcHoldpath
	DELETE FILE *.*

	cFilenameHold = (lcHoldpath+"ag945_"+TTOC(DATETIME(),1)+".csv")
	cFilename = cFilenameHold
	cFilenameShort = JUSTFNAME(cFilenameHold)
	lcArchPath = ALLTRIM(mm.archpath)
	cFilenameArch = (ALLTRIM(mm.archpath)+cFilenameShort)
	lcOutPath = IIF(lTesting,"F:\FTPUSERS\AGEGROUP\945out\test\",ALLTRIM(mm.basepath))
	lcPath = lcOutPath
	cFilenameOut = (lcOutPath+cFilenameShort)
*	nFilenum = FCREATE(cFilenameHold)

	LOCATE
	LOCATE FOR mm.edi_type = 'MISC' AND mm.taskname = 'GENERAL'
	tsendtotest = IIF(mm.use_alt,sendtoalt,mm.sendto)
	tcctest = IIF(mm.use_alt,mm.ccalt,mm.cc)
	tsendtoerr = IIF(mm.use_alt,sendtoalt,mm.sendto)
	tccerr = IIF(mm.use_alt,mm.ccalt,mm.cc)


	IF !USED("edi_trigger")
		USE F:\3pl\DATA\EDI_TRIGGER IN 0
	ENDIF

	SET DELETED ON
	IF USED('OUTSHIP')
		USE IN outship
	ENDIF
	csq1 = [select * from outship where accountid = ]+TRANSFORM(nAcctNum)+[ and office = ']+cOffice+[' and bol_no = ']+cBOL+[']
	xsqlexec(csq1,,,"wh")
	IF RECCOUNT() = 0
		cErrMsg = "MISS BOL "+cBOL
		DO ediupdate WITH cErrMsg,.T.
		THROW
	ENDIF
	INDEX ON bol_no TAG bol_no

	SELECT outship
	LOCATE
	SCAN FOR outship.bol_no = cBOL
		IF emptynul(outship.del_date)
			cErrMsg = "EMPTY/NULL DEL.DATES FOUND"
			DO ediupdate WITH cErrMsg,.T.
			THROW
		ENDIF
	ENDSCAN

	SCAN FOR outship.bol_no = cBOL
		cShip_ref = ALLTRIM(outship.ship_ref)
		IF cBOL = '09226910000362079'
			IF INLIST(cShip_ref,'01010214353 OV','01010214353 OV')
				LOOP
			ENDIF
			cShip_ref = STRTRAN(cShip_ref," OV","")
		ELSE
			IF "OV"$cShip_ref
				LOOP
			ENDIF
		ENDIF

		SELECT ship_ref ;
			FROM outship ;
			WHERE outship.ship_ref = cShip_ref ;
			INTO CURSOR tempptchk
		IF RECCOUNT('tempptchk') > 1 && confirms OV PT match to a PT
			SET DELETED OFF
			lOverflow = .T. && Flag set for overflow processing
			EXIT
		ENDIF
		SELECT outship
	ENDSCAN

	lKohlMBOL = .F.
	=SEEK(cBOL,"outship","bol_no")

	IF "KOHL"$outship.consignee
		xsqlexec("select blid,ship_ref from bldet where .f.","xpatt",,"wh")
		xsqlexec("select blid,bol_no from bl where mblnum = '"+cBOL+"'","xbl1",,"wh")
		SELECT xbl1
		IF RECCOUNT('xbl1') = 0
			WAIT WINDOW "No records in Kohl's MBOL data" TIMEOUT 2
			USE IN xbl1
		ELSE
			SCAN
				lKohlMBOL = .T.
				SELECT outship
				LOCATE
				SCAN
					nBLID = xbl1.blid
					xsqlexec("select blid,ship_ref from bldet where blid = "+TRANSFORM(nBLID),"xbldet",,"wh")
					SELECT xpatt
					APPEND FROM DBF('xbldet')
					USE IN xbldet
				ENDSCAN
				SELECT a.bol_no,b.ship_ref FROM xbl1 a, xpatt b WHERE a.blid = b.blid INTO CURSOR xbl READWRITE
			ENDSCAN
			USE IN xbl1
			USE IN xpatt
		ENDIF
*!*			IF lTesting
*!*			SELECT xbl
*!*			LOCATE
*!*			BROWSE
*!*			endif
	ENDIF

	cCustFolder = UPPER(cCustname)
	STORE "" TO lcKey
	STORE 0 TO alength,nLength

*!* SET OTHER CONSTANTS

	cdate = DTOS(DATE())
	ctruncdate = RIGHT(cdate,6)
	ctime = LEFT(TIME(DATE()),2)+SUBSTR(TIME(DATE()),4,2)
	cfiledate = cdate+ctime
	cStyle = ""
	cPTString = ""
	nTotCtnWt = 0
	nTotCtnCount = 0
	nUnitSum = 0

	IF	USED("parcel_carriers")
		USE IN parcel_carriers
	ENDIF
	USE F:\3pl\DATA\parcel_carriers IN 0 ALIAS parcel_carriers

	=SEEK(cBOL,'outship','bol_no')
	cShip_SCAC = ALLTRIM(outship.scac)
	nWO_num = outship.wo_num

	IF USED('OUTDET')
		USE IN outdet
	ENDIF

	selectoutdet()
	SELECT outdet
	INDEX ON outdetid TAG outdetid
	INDEX ON outshipid  TAG outshipid

	lParcelType = .F.
	lDoSQLUPS = .F.

	IF USED('sqlwo')
		USE IN sqlwo
	ENDIF
	IF FILE("F:\3pl\DATA\sqlwoag.dbf")
		DELETE FILE "F:\3pl\DATA\sqlwoag.dbf"
	ENDIF
	SELECT bol_no,wo_num ;
		FROM outship WHERE bol_no = cBOL ;
		GROUP BY 1,2 ;
		ORDER BY 1,2 ;
		INTO DBF F:\3pl\DATA\sqlwoag
	USE IN sqlwoag
	USE F:\3pl\DATA\sqlwoag IN 0 ALIAS sqlwo

	cRetMsg = ""

	DO m:\dev\prg\sqlconnect_bol  WITH nAcctNum,cCustname,cPPName,.T.,cOffice

	SELECT 0
	IF cRetMsg<>"OK"
		cErrMsg = cRetMsg
		DO ediupdate WITH cErrMsg,.T.
		THROW
	ENDIF

	swc_cutctns(cBOL)

	SELECT vagegrouppp
	INDEX ON outdetid TAG outdetid

	IF !lOverflow  && Overflow BOL *NOT* set (default)
		SELECT outship
		cScanStr2 = "outship.bol_no = cBOL"
		SCAN FOR &cScanStr2
			IF "OV"$outship.ship_ref
				LOOP
			ENDIF
			SELECT vagegrouppp
			m.ship_ref = IIF(cBOL ='09226910000362079',STRTRAN(ALLTRIM(outship.ship_ref)," OV",""),outship.ship_ref)
			SUM totqty TO nxRec FOR vagegrouppp.ship_ref = m.ship_ref
			SELECT outship
			IF nxRec # outship.qty
				WAIT WINDOW "SQL->OUTSHIP ctn qtsdiffer ("+TRANSFORM(nxRec)+" vs "+TRANSFORM(outship.qty)+")" TIMEOUT 5
				SET STEP ON
				RELEASE nxRec
				cErrMsg = "SQL QTY # OS QTY: "+ALLTRIM(m.ship_ref)
				DO ediupdate WITH cErrMsg,.T.
				THROW
			ENDIF
		ENDSCAN
	ENDIF

	IF !USED("scacs")
		xsqlexec("select * from scac","scacs",,"whall")
		INDEX ON scac TAG scac
	ENDIF

	IF USED('bl')
		USE IN bl
	ENDIF
	IF USED('bldet')
		USE IN bldet
	ENDIF

	SET STEP ON
	xsqlexec("select * from bl where accountid in ("+gagegroupaccounts+") and mblnum = '"+cMBOL+"'",,,"wh")
	nBLID = 0
	nSubRec = 0

	SELECT bl
	SCAN
		cFilenameHold = (lcHoldpath+"ag945_"+TTOC(DATETIME(),1)+".csv")
		cFilename = cFilenameHold
		cFilenameShort = JUSTFNAME(cFilenameHold)
		cFilenameArch = (lcArchPath+cFilenameShort)
		cFilenameOut = (lcOutPath+cFilenameShort)

		cBOL = TRIM(bl.bol_no)
		WAIT WINDOW "Now processing Sub-BOL #"+cBOL TIMEOUT 2
		IF cMBOL = '9999' AND cBOL # '04907304677451588'  && to select only a single sub-BOL to reprocess
			LOOP
		ENDIF
		nBLID = bl.blid
		SELECT 0
		IF USED('agtemplate')
			USE IN agtemplate
		ENDIF
		SELECT 0
		USE F:\3pl\DATA\agegroup945template.DBF ALIAS agtemplate
		SCATTER MEMVAR BLANK

		SELECT 0
		SELECT * FROM agtemplate WHERE .F. INTO CURSOR intake READWRITE

		CREATE CURSOR tempag945 (accountid i,isa_num c(10),st_num c(10),wo_num i,bol_no c(20),;
			ship_ref c(20),filename c(50),filedate T)

		xsqlexec("select * from bldet where mod='"+gOffice+"' and blid="+TRANSFORM(nBLID),,,"wh")
		SELECT bldet
		IF lTesting
*			BROWSE
		ENDIF

		LOCATE
		SCAN  && BLDET Scan
			cBLShip_ref = ALLTRIM(bldet.ship_ref)

			IF "OV"$cShip_ref
				LOOP
			ENDIF
			lCombPT = IIF(bldet.combinedpt,.T.,.F.)
			IF lCombPT
				bldetscanstr = "outship.combpt = PADR(cBLShip_ref,20) AND outship.accountid = nAcctNum and !DELETED()"
			ELSE
				bldetscanstr = "outship.ship_ref = PADR(cBLShip_ref,20) AND outship.accountid =nAcctNum and !DELETED()"
			ENDIF

			SELECT outship
			LOCATE
			WAIT CLEAR
			WAIT WINDOW "Now creating BOL#-based information..." NOWAIT NOCLEAR
			nPTCount = 0

			SCAN FOR &bldetscanstr  && Outship Scan
* Wait Window "" Timeout 2
				SCATTER FIELDS EXCEPT origqty MEMVAR MEMO
				cShip_ref = ALLTRIM(outship.ship_ref)
				nOutshipid = outship.outshipid

				m.qty = IIF(lOverflow,outship.origqty,m.qty)
				m.scac2 = m.scac
				lPrepack = IIF(outship.picknpack = .T.,.F.,.T.)

				IF EMPTY(m.del_date)
					LOOP
				ENDIF

				IF (" OV"$m.ship_ref) OR (RIGHT(ALLTRIM(m.ship_ref),1)=".")
					LOOP
				ENDIF

				IF m.qty = 0
*			LOOP
				ENDIF

				alength = ALINES(apt,outship.shipins,.T.,CHR(13))
				nPTCount = nPTCount + 1

				nWO_num = outship.wo_num
				cWO_Num = ALLTRIM(STR(nWO_num))
				IF TRIM(cWO_Num) <> TRIM(cWO_NumOld)
					STORE cWO_Num TO cWO_NumOld
					IF EMPTY(cWO_NumStr)
						STORE cWO_Num TO cWO_NumStr
					ELSE
						cWO_NumStr = (cWO_NumStr+", "+cWO_Num)
					ENDIF
					IF EMPTY(cWO_NumList)
						STORE cWO_Num TO cWO_NumList
					ELSE
						cWO_NumList = (cWO_NumList+CHR(13)+cWO_Num)
					ENDIF
				ENDIF

				cBOL2 = ""
				IF lKohlMBOL
					SELECT xbl
					LOCATE
					LOCATE FOR xbl.ship_ref = cShip_ref
					cBOL2 = ALLTRIM(xbl.bol_no)
					SELECT outship
				ENDIF

				cPTString = IIF(EMPTY(cPTString),m.consignee+" "+cShip_ref,cPTString+CHR(13)+m.consignee+" "+cShip_ref)
				nCtnNumber = 1  && Begin sequence count

				WAIT CLEAR
				WAIT WINDOW "Now creating Line Item information" NOWAIT NOCLEAR

				INSERT INTO tempag945 (accountid,isa_num,st_num,wo_num,bol_no,ship_ref,filename,filedate) ;
					VALUES (m.accountid,"","",m.wo_num,m.bol_no,m.ship_ref,cFilenameShort,dt2)

				STORE "AGE GROUP LTD." TO m.acctname,m.acctname2
				m.bol_no = IIF(lKohlMBOL,cBOL2,cBOL)
				m.shipfrom = "TOLL"
				m.shipdept = "CARSON II"
				m.sfromaddr = "1000 E 223RD ST"
				m.sfromcity = "CARSON"
				m.sfromst = "CA"
				m.sfromzip = "90745"
				m.sfromphone = "562-735-0600"
				m.BTFName = ALLTRIM(segmentget(@apt,"BTFNAME",alength))
				m.BTLName = ALLTRIM(segmentget(@apt,"BTLNAME",alength))
				m.BTCoName = ALLTRIM(segmentget(@apt,"BTCONAME",alength))
				m.BTAddr1 = ALLTRIM(segmentget(@apt,"BTADDR1",alength))
				m.BTAddr2 = ALLTRIM(segmentget(@apt,"BTADDR2",alength))
				m.BTCity = ALLTRIM(segmentget(@apt,"BTCITY",alength))
				m.BTState = ALLTRIM(segmentget(@apt,"BTSTATE",alength))
				m.BTZip = ALLTRIM(segmentget(@apt,"BTZIP",alength))
				m.BTCntry = ALLTRIM(segmentget(@apt,"BTCNTRY",alength))
				m.BTPhone = ALLTRIM(segmentget(@apt,"BTPHONE",alength))
				m.BTEmail = ALLTRIM(segmentget(@apt,"BTEMAIL",alength))

				m.STFName = ALLTRIM(segmentget(@apt,"STFNAME",alength))
				m.STLName = ALLTRIM(segmentget(@apt,"STLNAME",alength))
				m.STCity = ALLTRIM(segmentget(@apt,"STCITY",alength))
				m.STState = ALLTRIM(segmentget(@apt,"STSTATE",alength))
				m.STZip = ALLTRIM(segmentget(@apt,"STZIP",alength))
				m.STCntry = ALLTRIM(segmentget(@apt,"STCNTRY",alength))
				m.STPhone = ALLTRIM(segmentget(@apt,"STPHONE",alength))
				m.STEmail = ALLTRIM(segmentget(@apt,"STEMAIL",alength))

				m.SForCity = ALLTRIM(segmentget(@apt,"SFORCITY",alength))
				m.SForState = ALLTRIM(segmentget(@apt,"SFORSTATE",alength))
				m.SForZip = ALLTRIM(segmentget(@apt,"SFORZIP",alength))
				m.SForCntry = ALLTRIM(segmentget(@apt,"SFORCNTRY",alength))

				m.order = ALLTRIM(segmentget(@apt,"ORDER",alength))
				m.DCCode = ALLTRIM(segmentget(@apt,"DCCODE",alength))
				m.billing = ALLTRIM(segmentget(@apt,"BILLING",alength))
				m.freight = 0
				m.divdesc = m.div
				m.deptdesc = ALLTRIM(segmentget(@apt,"DEPTDESC",alength))
				m.deptdesc2 = m.deptdesc
				m.trknumber = IIF(lParcelType,ALLTRIM(m.bol_no),ALLTRIM(m.keyrec))

				SELECT outdet
				SUM totqty TO nPTUnitQty FOR outdet.outshipid = outship.outshipid AND outdet.units
				nWeight = ROUND(m.weight/nPTUnitQty,0)

*!*	This block added to determine if cartonization was done on units or non-units outdet lines, 07.25.2017, Joe
				SELECT vagegrouppp
				LOCATE
				LOCATE FOR vagegrouppp.outshipid = outship.outshipid
				nvodid = vagegrouppp.outdetid
				SELECT outdet
				=SEEK(nvodid,'outdet','outdetid')
				lDoUnitsLines = outdet.units
*!* End of added code

				IF lDoUnitsLines
					cDetScanStr = IIF(lPrepack,"outdet.outshipid = outship.outshipid AND units","outdet.outshipid = outship.outshipid")
				ELSE
					cDetScanStr = IIF(lPrepack,"outdet.outshipid = outship.outshipid AND !units","outdet.outshipid = outship.outshipid")
				ENDIF

				SELECT outdet
				LOCATE
				SCAN FOR &cDetScanStr
					SCATTER MEMVAR
					SELECT vagegrouppp
					LOCATE
					SCAN FOR vagegrouppp.outdetid = outdet.outdetid
						m.totqty = vagegrouppp.totqty
						m.ucc = ALLTRIM(vagegrouppp.ucc)

						alength2 = ALINES(aptdet,outdet.printstuff,.T.,CHR(13))
						IF alength2 > 1
							m.season = ALLTRIM(segmentget(@aptdet,"SEASON",alength2))
							m.desc = ALLTRIM(segmentget(@aptdet,"DESC",alength2))
							m.unitprice = VAL(ALLTRIM(segmentget(@aptdet,"UNITPRICE",alength2)))
							m.upc = ALLTRIM(segmentget(@aptdet,"ORIGUPC",alength2))
							IF EMPTY(m.upc)
								m.upc = ALLTRIM(outdet.upc)
							ENDIF

						ELSE
							m.season = "UNK"  && pg added 10/6/2016  printstuff was empty causing a crash
							m.desc = "UNK"
							m.unitprice = 0.00
						ENDIF

						m.totqty2 = m.totqty
*				m.weight = IIF(lPrepack,nWeight,nWeight*m.totqty)  && For prepacks, per-ctn wt. is avg. of PT weight; PnP is (unit wt x units) per det.line
						m.weight = (nWeight*m.totqty)  && All PnP now per Chris, 06.24.2017
						INSERT INTO intake FROM MEMVAR
					ENDSCAN
				ENDSCAN
				IF lTesting
*				SET STEP ON
				ENDIF
			ENDSCAN  && at end of outship loop
		ENDSCAN  && At end of BLDET loop


		SELECT intake
		IF lTesting
			LOCATE
		ENDIF

		LOCATE
		COPY TO F:\3pl\DATA\hold.CSV TYPE DELIMITED

		IF USED('ag945csv_out')
			USE IN ag945csv_out
		ENDIF
		USE F:\3pl\DATA\ag945csv_out IN 0
		SELECT * FROM ag945csv_out INTO CURSOR tempintake2 READWRITE
		APPEND FROM F:\3pl\DATA\ag945header2.CSV TYPE DELIMITED
		APPEND FROM F:\3pl\DATA\hold.CSV TYPE DELIMITED

		SET STEP ON
		COPY TO [&cFilenameHold] TYPE DELIMITED

*!* Create eMail confirmation message
		IF lTestmail
			tsendto = tsendtotest
			tcc = tcctest
		ENDIF

		cPTCount = ALLTRIM(STR(nPTCount))
		nPTCount = 0
		tsubject = cMailName+" 945 CSV File from TGF as of "+dtmail+" ("+cCustLoc+")"
		tattach = " "
		tmessage = "945 EDI Info from TGF for MBOL# "+TRIM(cMBOL)+CHR(13)
		tmessage = tmessage + "for Sub-BOL# "+TRIM(cBOL)+CHR(13)
		tmessage = tmessage + "(File: "+cFilenameShort+")"+CHR(13)
		tmessage = tmessage + "(WO# "+cWO_NumStr+"), containing these "+cPTCount+" picktickets:"+CHR(13)
		tmessage = tmessage + cPTString + CHR(13)
		tmessage = tmessage + "has been created and will be transmitted ASAP."+CHR(13)+CHR(13)
		tmessage = tmessage + "If you have any questions, please eMail or call 732-750-9000 x217."

		IF lEmail
			DO FORM m:\dev\frm\dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
		ENDIF
		cPTCount = ""
		cWO_NumStr = ""
		cPTString = ""

		RELEASE ALL LIKE c_CntrlNum,c_GrpCntrlNum
		WAIT CLEAR
		WAIT WINDOW cMailName+" 945 EDI File output complete"+CHR(13)+"for BOL# "+TRIM(cBOL) &cTimeMsg
		IF !lTesting
*!* asn_out_data()()
		ENDIF

	ENDSCAN

**********************************************************************************************

*!* Transfers files to correct output folders
	CD &lcHoldpath
	cHoldfiles = (lcHoldpath+"*.*")
	cArchfiles = (lcArchPath+"*.*")

	SET STEP ON
	COPY FILE [&cHoldfiles] TO [&cArchfiles]

	IF lAGFilesout
		cOutfiles = (lcOutPath+"*.*")
		COPY FILE [&cHoldfiles] TO [&cOutfiles]
		DELETE FILE *.*
	ENDIF

	IF !lTesting
		SELECT tempag945
		COPY TO "f:\3pl\data\tempag945a.dbf"
		USE IN tempag945
		SELECT 0
		USE "f:\3pl\data\tempag945a.dbf" ALIAS tempag945a
		SELECT 0
		USE "f:\3pl\data\pts_sent945.dbf" ALIAS pts_sent945
		APPEND FROM "f:\3pl\data\tempag945a.dbf"
		USE IN pts_sent945
		USE IN tempag945a
		DELETE FILE "f:\3pl\data\tempag945a.dbf"

		SELECT EDI_TRIGGER
		DO ediupdate WITH "945 CREATED",.F.

		IF !USED("ftpedilog")
			SELECT 0
			USE F:\edirouting\ftpedilog ALIAS ftpedilog
			INSERT INTO ftpedilog (TRANSFER,ftpdate,filename,acct_name,TYPE) VALUES ("945-"+cCustname+"-"+cCustLoc,dt2,cFilename,UPPER(cCustname),"945")
			USE IN ftpedilog
		ENDIF
	ENDIF

	WAIT CLEAR
	WAIT WINDOW cCustFolder+" 945 Process complete..." TIMEOUT 1

CATCH TO oErr
	IF lDoCatch
		SET STEP ON
		lEmail = .F.
		lCloseOutput = .F.

		IF !cErrMsg = "TOP LEVEL"
			DO ediupdate WITH ALLT(oErr.MESSAGE),.T.
		ENDIF

		tsubject = cCustname+" Error ("+TRANSFORM(oErr.ERRORNO)+") at "+TTOC(DATETIME())
		tattach  = ""
		tsendto = IIF(lTesting,tsendtotest,tsendtoerr)
		tcc= IIF(lTesting,tcctest,tccerr)

		tmessage = cCustname+" Error processing "+CHR(13)
		tmessage =tmessage+ "Try/CATCH Exeception Message:"+CHR(13)+;
			[  Error: ] + STR(oErr.ERRORNO) +CHR(13)+;
			[  LineNo: ] + STR(oErr.LINENO) +CHR(13)+;
			[  Message: ] + oErr.MESSAGE +CHR(13)+;
			[  Procedure: ] + oErr.PROCEDURE +CHR(13)+;
			[  Details: ] + oErr.DETAILS +CHR(13)+;
			[  StackLevel: ] + STR(oErr.STACKLEVEL) +CHR(13)+;
			[  LineContents: ] + oErr.LINECONTENTS+CHR(13)+;
			[  UserValue: ] + oErr.USERVALUE

		tsubject = "945 EDI Poller Error at "+TTOC(DATETIME())
		tattach  = ""
		DO FORM m:\dev\frm\dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
	ENDIF
FINALLY
*RELEASE tsendto,tfrom,tsubject,tcc,tattach,tmessage
	SET DELETED ON
	SET STATUS BAR ON
	ON ERROR
ENDTRY


*** END OF CODE BODY

****************************
PROCEDURE close945
****************************
** Footer Creation

	STORE  "GE"+cfd+ALLTRIM(STR(nSTCount))+cfd+c_CntrlNum+csegd TO cString
	IF lTesting
		FPUTS(nFilenum,cString)
	ELSE
		FWRITE(nFilenum,cString)
	ENDIF

	STORE  "IEA"+cfd+"1"+cfd+PADL(c_CntrlNum,9,"0")+csegd TO cString
	IF lTesting
		FPUTS(nFilenum,cString)
	ELSE
		FWRITE(nFilenum,cString)
	ENDIF

	RETURN

****************************
PROCEDURE num_incr_isa
****************************

	IF !USED("serfile")
		USE ("f:\3pl\data\serial\"+cCustname+"945_serial") IN 0 ALIAS serfile
	ENDIF
	SELECT serfile
	IF lTesting AND lISAFlag
		nOrigSeq = serfile.seqnum
		lISAFlag = .F.
	ENDIF
	nISA_Num = serfile.seqnum
	c_CntrlNum = ALLTRIM(STR(serfile.seqnum))
	REPLACE serfile.seqnum WITH serfile.seqnum + 1 IN serfile
	SELECT outship
	RETURN

****************************
PROCEDURE num_incr_st
****************************

	IF !USED("serfile")
		USE ("f:\3pl\data\serial\"+cCustname+"945_serial") IN 0 ALIAS serfile
	ENDIF
	SELECT serfile
	IF lTesting AND lSTFlag
		nOrigGrpSeq = serfile.grpseqnum
		lSTFlag = .F.
	ENDIF
	c_GrpCntrlNum = ALLTRIM(STR(serfile.grpseqnum))
	REPLACE serfile.grpseqnum WITH serfile.grpseqnum + 1 IN serfile
	SELECT outship
	RETURN


****************************
PROCEDURE segmentget
****************************

	PARAMETER thisarray,lcKey,nLength

	FOR i = 1 TO nLength
		IF i > nLength
			EXIT
		ENDIF
		lnEnd= AT("*",thisarray[i])
		IF lnEnd > 0
			lcThisKey =TRIM(SUBSTR(thisarray[i],1,lnEnd-1))
			IF OCCURS(lcKey,lcThisKey)>0
				RETURN SUBSTR(thisarray[i],lnEnd+1)
				i = 1
			ENDIF
		ENDIF
	ENDFOR

	RETURN ""

****************************
PROCEDURE ediupdate
****************************
	PARAMETER cStatus,lIsError
	lDoCatch = .F.
	SET STEP ON

	IF !lTesting
		SELECT EDI_TRIGGER
		nRec = RECNO()
*		DO m:\dev\prg\edistatusmove WITH cBOL
		LOCATE
		IF !lIsError
			REPLACE processed WITH .T.,proc945 WITH .T.,file945 WITH cFilename,;
				fin_status WITH cStatus,errorflag WITH .F.,when_proc WITH DATETIME(),isa_num WITH cISA_Num ;
				FOR EDI_TRIGGER.bol = cMBOL AND accountid = nAcctNum

*!*				xsqlexec("update edi_trigger set processed=1, proc945=1, file945='"+cFilename+"', " + ;
*!*					"fin_status='"+cStatus+"',errorflag=0, when_proc={"+TTOC(DATETIME())+"}, " + ;
*!*					"isa_num='"+cISA_Num+"' where bol='"+cBOL+"' and accountid="+TRANSFORM(nAcctNum),,,"stuff")
		ELSE
			REPLACE processed WITH .T.,proc945 WITH .F.,file945 WITH "",;
				fin_status WITH cStatus,errorflag WITH .T.,isa_num WITH cISA_Num ;
				FOR EDI_TRIGGER.bol = cMBOL AND accountid = nAcctNum

*!*				xsqlexec("update edi_trigger set processed=1, proc945=0, file945='', " + ;
*!*					"fin_status='"+cStatus+"', errorflag=1, isa_num='"+cISA_Num+"' " + ;
*!*					"where bol='"+cBOL+"' and accountid="+TRANSFORM(nAcctNum),,,"stuff")

		ENDIF
		IF lCloseOutput
*=FCLOSE(nFilenum)
		ENDIF
	ENDIF

	IF lIsError AND lEmail
		tsubject = "945 Error in AGE GROUP BOL "+TRIM(cBOL)+" (At PT "+cShip_ref+")"
		tattach = " "
		tsendto = tsendtoerr
		tcc = tccerr

		tmessage = "Generated from program: agegroup945_create, within EDI Outbound Poller"
		tmessage = tmessage + CHR(13) + "945 Processing for BOL# "+TRIM(cBOL)+"(WO# "+cWO_Num+"), produced this error: (Office: "+cOffice+"): "+cStatus+CHR(13)+"Check EDI_TRIGGER and re-run"
		IF cStatus = "MISS PT"
			tmessage = tmessage + CHR(13) + "This error means that this PT exists in OUTSHIP, but not in the Pickpack table. Please add this WO to SQL."
		ENDIF

		DO FORM m:\dev\frm\dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
	ENDIF

	IF USED('outship')
		USE IN outship
	ENDIF
	IF USED('outdet')
		USE IN outdet
	ENDIF

	IF USED('serfile')
		SELECT serfile
		IF lTesting
			REPLACE serfile.seqnum WITH nOrigSeq
			REPLACE serfile.grpseqnum WITH nOrigGrpSeq
		ENDIF
		USE IN serfile
	ENDIF

	IF USED('scacs')
		USE IN scacs
	ENDIF

	IF USED('mm')
		USE IN mm
	ENDIF

	IF !lTesting
		SELECT EDI_TRIGGER
		LOCATE
	ENDIF
	RETURN

****************************
PROCEDURE cstringbreak
****************************
	cLen = LEN(ALLTRIM(cString))
	IF lTesting
		FPUTS(nFilenum,cString)
	ELSE
		FWRITE(nFilenum,cString)
	ENDIF
	RETURN

****************************
PROCEDURE valreturn
****************************
	PARAMETERS cIdentifier
	cReturned = "c"+cIdentifier
	RELEASE ALL LIKE &cReturned
	PUBLIC &cReturned
	nGetline = ATCLINE(cIdentifier,outdet.printstuff)
	dataline = MLINE(outdet.printstuff,nGetline)
	STORE SUBSTR(dataline,AT("*",dataline)+1) TO &cReturned
	RETURN &cReturned
ENDPROC
