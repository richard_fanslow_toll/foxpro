parameter whichaccount
Set Exclusive Off
Set Deleted On
Set Talk Off
Set Safety Off
*whichaccount = 6303
*Set Step On 
whichaccount = Val(Alltrim(Transform(whichaccount)))

close data all 

goffice="J"

xsqlexec("select * from inven where mod='J'",,,"wh")

xsqlexec("select * from Project where mod='J' and completeddt={}",,,"wh")

**to run report from snapshot files, comment out the USE statements above and uncomment out below - mvw 03/19/13
*!*	Use f:\whj\whdata\snapshot\soutship alias outship In 0
*!*	Use f:\whj\whdata\snapshot\soutdet alias outdet In 0
*!*	Use f:\whj\whdata\project In 0
*!*	Use f:\whj\whdata\snapshot\sinven alias inven In 0



**to run report from snapshot files, comment out the USE statements above and uncomment out below - mvw 03/19/13
*!*	Use M:\BAK\whbak\whdata-j\outship alias outship In 0
*!*	Use M:\BAK\whbak\whdata-j\outdet alias outdet In 0
*!*	Use M:\BAK\whbak\whdata-j\project In 0
*!*	Use M:\BAK\whbak\whdata-j\inven alias inven In 0 



upcmastsql (6303)

Set Deleted on


**************************************** begin ALL accounts TM 01/16/2014
IF whichaccount = 9999
	select units, style, color, id, pack, availqty, allocqty, holdqty, 0000000 as pickedqty, 0000000 as spqty,000000 overallqty, Space(13) as upc,accountid,"     " As division , Space(13) as upc2 ;
	  from inven where inlist(accountid,6303,6325,6304,6305,6320,6321,6322,6323,6324,6364,6543) and units into cursor inventory readwrite

	index on Str(accountid,4)+transform(units)+style+color+id+pack tag match

**Set Step On 

	* inprocess is a cursor of all outship records where they are not yet delivered and also excluding "notonwip"

	xsqlexec("select outship.*, cast('NJ' as char(2)) as whse, style, color, id, pack, totqty from outship, outdet where outship.mod='J' " + ;
		"and outship.outshipid=outdet.outshipid and inlist(outship.accountid,6303,6325,6304,6305,6320,6321,6322,6323,6324,6543,6364) " + ;
		"and del_date={} and notonwip=0 and qty#0 and pulled=1","xdy",,,"wh")

	select *, sum(totqty) as pickedqty from xdy group by units, accountid, style, color, id, pack into cursor xunshipped
	
	select xunshipped
	scan
	  If seek(Str(accountid,4)+transform(units)+style+color+id+pack,"inventory","match")
	    replace pickedqty with xunshipped.pickedqty in inventory
	  Else
	  
	  Endif  
	endscan

	* uncompleted special projects

	xsqlexec("select outdet.accountid, outdet.wo_num, units, style, color, id, pack, totqty " + ;
		"from outdet, project where outdet.mod='J' " + ;
		"and outdet.wo_num=project.wo_num and completeddt={} " + ;
		"and inlist(project.accountid,6303,6325,6304,6305,6320,6321,6322,6323,6324,6543,6364,6543)","xdy",,"wh")

	select wo_num, units, style, color, id, pack, sum(totqty) as spqty ;
		from xdy group by 1,2,3,4,5,6,7 into cursor xsp

	select xsp
	scan
	  If seek(Str(accountid,4)+transform(units)+style+color+id+pack,"inventory","match")
	    replace spqty with xsp.spqty in inventory
	  Else
	  
	  Endif   
	endscan

**Set Step On 

	* copy and email inventory file

	select inventory
	delete for availqty=0 and pickedqty=0 and holdqty=0 and spqty=0 and allocqty=0
**SET STEP ON 
	Select inventory
	Scan &&for !Deleted()
	  Select upcmast
	  Do case
	    case InList(inventory.accountid,6324,6320,6364,6304)  && the SPE account, same master as Retail
	      Locate For upcmast.style = inventory.style and inventory.color = upcmast.color and inventory.id = upcmast.id And accountid = 6304  && use 6304 for the SPE Account
	   Case  InList(inventory.accountid,6325,6303,6305,2503,6543)
	      Locate For upcmast.style = inventory.style and inventory.color = upcmast.color and inventory.id = upcmast.id And accountid = 6303
	   Otherwise
	      Locate For upcmast.style = inventory.style and inventory.color = upcmast.color and inventory.id = upcmast.id And accountid = inventory.accountid
	  EndCase
	  


	  If Found()
	    replace inventory.upc with upcmast.upc in inventory
	    If Empty(upcmast.upc)
	      alength = ALINES(aupc,upcmast.INFO,.T.,CHR(13))
	      cUPC    = ALLTRIM(segmentget(@aupc,"UPC",alength))
	      replace inventory.upc2 with cUPC in inventory
	    EndIf   
	  Else
	    replace inventory.upc with "UNK"
	  EndIf   
	  
	************************begin division 12/3/2012 TM
	    If Empty( inventory.division)
	      alength = ALINES(aupc,upcmast.INFO,.T.,CHR(13))
	      lcdiv   = ALLTRIM(segmentget(@aupc,"DIV",alength))
	      Replace division With lcdiv In inventory 
	  Else
	    replace inventory.division with "UNK"
	  EndIf   
	************************end division 12/3/2012 TM  
		EndScan

	Select inventory
	SCAN
	  replace pickedqty WITH pickedqty+allocqty && added 12/18/12 by TM to account for alloc from inven
	  replace overallqty with availqty+holdqty+pickedqty+spqty
	EndScan

	**************************************** end ALL accounts TM 01/16/2014
else

**SET STEP ON 




**************************************** begin wholesale and damages TM 12/03/2012
IF whichaccount = 2503
	select units, style, color, id, pack, availqty, allocqty, holdqty, 0000000 as pickedqty, 0000000 as spqty,000000 overallqty, Space(13) as upc,accountid,"     " As division , Space(13) as upc2 ;
	  from inven where inlist(accountid,6303,6325) and units into cursor inventory readwrite

	index on Str(accountid,4)+transform(units)+style+color+id+pack tag match

	* inprocess is a cursor of all outship records where they are not yet delivered and also excluding "notonwip"

	xsqlexec("select outship.*, cast('NJ' as char(2)) as whse, units, style, color, id, pack, totqty from outship, outdet where outship.mod='J' " + ;
		"and outship.outshipid=outdet.outshipid and inlist(outship.accountid,6303,6325) " + ;
		"and del_date={} and notonwip=0 and qty#0 and pulled=1","xdy",,"wh")

	select *, sum(totqty) as pickedqty from xdy group by units, accountid, style, color, id, pack into cursor xunshipped
	
	select xunshipped
	scan
	  IF seek(Str(accountid,4)+transform(units)+style+color+id+pack,"inventory","match")
	  replace pickedqty with xunshipped.pickedqty in inventory
	  else
	  endif
	endscan

	* uncompleted special projects		dy 3/25/17 done this code run if so needs to be changed similar to above

	xsqlexec("select outdet.accountid, outdet.wo_num, units, style, color, id, pack, totqty " + ;
		"from outdet, project where outdet.mod='J' " + ;
		"and outdet.wo_num=project.wo_num and completeddt={} " + ;
		"and inlist(project.accountid,6303,6325)","xdy",,"wh")

	select wo_num, units, style, color, id, pack, sum(totqty) as spqty ;
		from xdy group by 1,2,3,4,5,6 into cursor xsp
	
	select xsp
	scan
	  IF seek(Str(accountid,4)+transform(units)+style+color+id+pack,"inventory","match")
	  replace spqty with xsp.spqty in inventory
	  	  else
	  endif
	endscan

	* copy and email inventory file

	select inventory
	delete for availqty=0 and pickedqty=0 and holdqty=0 and spqty=0 and allocqty=0

	Select inventory
	Scan &&for !Deleted()
	  Select upcmast
	  Do case
	    case InList(whichaccount,6324,6320,6304)  && the SPE account, same master as Retail
	      Locate For upcmast.style = inventory.style and inventory.color = upcmast.color and inventory.id = upcmast.id And accountid = 6304  && use 6304 for the SPE Account
	   Case  InList(whichaccount,6325,6303,6305,2503,6543)
	      Locate For upcmast.style = inventory.style and inventory.color = upcmast.color and inventory.id = upcmast.id And accountid = 6303
	   Otherwise
	      Locate For upcmast.style = inventory.style and inventory.color = upcmast.color and inventory.id = upcmast.id And accountid =  inventory.accountid
	  EndCase
	  


	  If Found()
	    replace inventory.upc with upcmast.upc in inventory
	    If Empty(upcmast.upc)
	      alength = ALINES(aupc,upcmast.INFO,.T.,CHR(13))
	      cUPC    = ALLTRIM(segmentget(@aupc,"UPC",alength))
	      replace inventory.upc2 with cUPC in inventory
	    EndIf   
	  Else
	    replace inventory.upc with "UNK"
	  EndIf   
	  
	************************begin division 12/3/2012 TM
	    If Empty( inventory.division)
	      alength = ALINES(aupc,upcmast.INFO,.T.,CHR(13))
	      lcdiv   = ALLTRIM(segmentget(@aupc,"DIV",alength))
	      Replace division With lcdiv In inventory 
	  Else
	    replace inventory.division with "UNK"
	  EndIf   
	************************end division 12/3/2012 TM  
		EndScan

	Select inventory
	SCAN
	  replace pickedqty WITH pickedqty+allocqty && added 12/18/12 by TM to account for alloc from inven
	  replace overallqty with availqty+holdqty+pickedqty+spqty
	EndScan

	**************************************** end wholesale and damages TM 12/03/2012
else
	* get inventory
	select units, style, color, id, pack, availqty, allocqty, holdqty, 0000000 as pickedqty, ;
		   0000000 as spqty,000000 overallqty, Space(13) as upc,accountid ,"     " As division , Space(13) as upc2 ;
	  from inven where accountid = whichaccount and units into cursor inventory readwrite

	index on Str(accountid,4)+transform(units)+style+color+id+pack tag match

	* inprocess is a cursor of all outship records where they are not yet delivered and also excluding "notonwip"

	xsqlexec("select outship.*, cast('NJ' as char(2)) as whse, units, style, color, id, pack, totqty from outship, outdet where outship.mod='J' " + ;
		"and outship.outshipid=outdet.outshipid and inlist(outship.accountid,6303,6325,6304,6305,6320,6321,6322,6323,6324,6543,6364) " + ;
		"and del_date={} and notonwip=0 and qty#0 and pulled=1","xdy",,"wh")

	select *, sum(totqty) as pickedqty from xdy group by units, accountid, style, color, id, pack into cursor xunshippedn
	
	select xunshipped
	scan
	   If seek(Str(accountid,4)+transform(units)+style+color+id+pack,"inventory","match")
	  replace pickedqty with xunshipped.pickedqty in inventory
	  	  else
	  endif
	endscan

	* uncompleted special projects		dy 3/25/17 done this code run if so needs to be changed similar to above

	xsqlexec("select outdet.accountid, outdet.wo_num, units, style, color, id, pack, totqty " + ;
		"from outdet, project where outdet.mod='J' " + ;
		"and outdet.wo_num=project.wo_num and completeddt={} " + ;
		"and inlist(project.accountid,6303,6325,6304,6305,6320,6321,6322,6323,6324,6543,6364,6543)","xdy",,"wh")

	select wo_num, units, style, color, id, pack, sum(totqty) as spqty ;
		from xdy group by 1,2,3,4,5,6,7 into cursor xsp
	
	select xsp
	scan
	 If seek(Str(accountid,4)+transform(units)+style+color+id+pack,"inventory","match")
	  replace spqty with xsp.spqty in inventory
	  	  else
	  endif
	endscan

	* copy and email inventory file

	select inventory
	delete For availqty=0 and pickedqty=0 and holdqty=0 and spqty=0 and allocqty=0 &&allocqty added 2/22/2013 missing records when allo >0

	Select inventory
	Scan &&for !Deleted()
	  Select upcmast
	  Do case
	    case InList(whichaccount,6324,6320,6304)  && the SPE account, same master as Retail
	      Locate For upcmast.style = inventory.style and inventory.color = upcmast.color and inventory.id = upcmast.id And accountid = 6304  && use 6304 for the SPE Account
	   Case  InList(whichaccount,6325,6303,6305,2503,6543)
	      Locate For upcmast.style = inventory.style and inventory.color = upcmast.color and inventory.id = upcmast.id And accountid = 6303
	   Otherwise
	      Locate For upcmast.style = inventory.style and inventory.color = upcmast.color and inventory.id = upcmast.id And accountid = whichaccount
	  EndCase
	  
	  If Found()
	    replace inventory.upc with upcmast.upc in inventory
	    If Empty(upcmast.upc)
	      alength = ALINES(aupc,upcmast.INFO,.T.,CHR(13))
	      cUPC    = ALLTRIM(segmentget(@aupc,"UPC",alength))
	      replace inventory.upc2 with cUPC in inventory
	    EndIf   
	  Else
	    replace inventory.upc with "UNK"
	  EndIf   

	************************begin division 12/3/2012 TM
	    If Empty( inventory.division)
	      alength = ALINES(aupc,upcmast.INFO,.T.,CHR(13))
	      lcdiv    = ALLTRIM(segmentget(@aupc,"DIV",alength))
	      Replace division With lcdiv In inventory
	  Else
	    replace inventory.division with "UNK"
	  EndIf   
	************************end division 12/3/2012 TM  
		EndScan

	Select inventory
	SCAN
	  replace pickedqty WITH pickedqty+allocqty  && added 12/18/12 by TM to account for alloc from inven
	  replace overallqty with availqty+ holdqty+pickedqty+spqty
	EndScan


	Select inventory
	Scan
	  replace overallqty with availqty+ holdqty+pickedqty+spqty
	EndScan
ENDIF   &&&&&&&&&TM
ENDIF  &&&&&TM 011614
Do case
  Case whichaccount = 6303
    acctnam = "Wholesale"
  Case whichaccount = 6304
    acctnam = "Retail"
  Case whichaccount = 6320
    acctnam = "SPE"
  Case whichaccount = 6321
    acctnam = "UK"
  Case whichaccount = 6322
    acctnam = "FR"
  Case whichaccount = 6323
    acctnam = "IT"
  Case whichaccount = 6305
    acctnam = "ECOMM"
  Case whichaccount = 6324
    acctnam = "RETAIL_DMG_"
  Case whichaccount = 6325
    acctnam = "WHOLESALE_DMG_"
  Case whichaccount = 6364
    acctnam = "EMP_SALE_"
  Case whichaccount = 2503
    acctnam = "Wholesale_and_Whsl_DMG_"
   Case whichaccount = 9999
    acctnam = "ALL_ACCOUNTS_"
  Otherwise  
    acctnam = "UNK"
 EndCase  

 export to "s:\marcjacobsdata\reports\inventorysyncfiles\BI\"+acctnam+"_Inventory_"+ttoc(datetime(),1) fields style,color,id,pack,availqty,pickedqty,holdqty,spqty,overallqty,upc,accountid,division TYPE xls
 IF whichaccount= 6303
	 COPY to s:\marcjacobsdata\reports\inventorysyncfiles\BI\Wholesale_Inventory.csv fields style,color,id,pack,availqty,pickedqty,holdqty,spqty,overallqty,upc,accountid,division  TYPE CSV
 ELSE
 	 IF whichaccount= 6325
	 	COPY to s:\marcjacobsdata\reports\inventorysyncfiles\BI\Wholesale_DMG_Inventory.csv fields style,color,id,pack,availqty,pickedqty,holdqty,spqty,overallqty,upc,accountid,division  TYPE CSV
 	ELSE
 	 	 IF whichaccount= 2503
	 	COPY to s:\marcjacobsdata\reports\inventorysyncfiles\BI\Wholesale_and_Whsl_DMG_Inventory.csv fields style,color,id,pack,availqty,pickedqty,holdqty,spqty,overallqty,upc,accountid,division  TYPE CSV
	 	ELSE
		ENDIF
 	ENDIF
 endif	

****************************
PROCEDURE segmentget
****************************

  PARAMETER thisarray,lcKey,nLength

  FOR i = 1 TO nLength
    IF i > nLength
      EXIT
    ENDIF
    lnEnd= AT("*",thisarray[i])
    IF lnEnd > 0
      lcThisKey =TRIM(SUBSTR(thisarray[i],1,lnEnd-1))
      IF OCCURS(lcKey,lcThisKey)>0
        RETURN SUBSTR(thisarray[i],lnEnd+1)
        i = 1
      ENDIF
    ENDIF
  ENDFOR

  RETURN ""
