* This program 
utilsetup("MJ_KPI_WIP")
Set Exclusive Off
Set Deleted On
Set Talk Off
Set Safety Off   &&Allows for overwrite of existing files


_screen.WindowState = 1
Close Databases All

**********************************START WIP
GOFFICE='J'

xsqlexec("select * from pt where mod='"+GOFFICE+"'",,,"wh")
Index On ptid Tag ptid
Set Order To

xsqlexec("select * from account where inactive=0","account",,"qq")
Index On accountid Tag accountid
Set Order To


Select dept, ship_ref, batch_num As div,consignee, cnee_ref, qty, ptdate, Start, Cancel, {}As called, {} routedfor, {} appt, scac, ;
	space(20) appt_num,Space(35) apptremarks, ship_via,Space(20) bol_no, 00000000 wo_num,{}  wo_date,{}  pulleddt,{}  picked,{} ;
	labeled,{} staged,000000  ctnqty,0000  outshipid,0000 As pt_age, 0000 As days_to_cncll, accountid As account,Space(2) As whse ;
	from pt ;
	where Inlist(accountid,6303,6304,6305,6320,6321,6322,6323,6324,6325,6364,6543) And qty!=0 	Into Cursor xrpt2 Readwrite

xsqlexec("select dept, ship_ref, batch_num as div, consignee, cnee_ref, qty, ptdate, start, cancel, called, " + ;
	"routedfor, appt, scac, appt_num, apptremarks, ship_via, bol_no, wo_num, wo_date, pulleddt, picked, labeled, " + ;
	"staged, ctnqty, outshipid, cast(0 as numeric(4,0)) as pt_age, cast(0 as numeric(4,0)) as days_to_cncl, " + ;
	"accountid as account, cast('' as char(2)) as whse from outship where mod='J' " + ;
	"and inlist(accountid,6303,6304,6305,6320,6321,6322,6323,6324,6325,6364,6543) and del_date={} and notonwip=0","xrpt1",,"wh")

Select * From xrpt1 Union All Select * From xrpt2 Into Cursor xrpt3 Readwrite
Replace All pt_age With (Date()-ptdate)
Replace All   days_to_cncl With 9999 For Cancel={01/01/1900}
Replace All days_to_cncl With (Cancel-Date()) For Cancel !={01/01/1900}
Replace All whse With 'NJ'
Select xrpt3.*, Space(15) As Status From xrpt3 Into Cursor XRPT4 Readwrite
Replace All Status With Iif(Emptynul(wo_date),"UNALLOCATED",Iif(!Emptynul(staged),"STAGED",Iif(!Emptynul(labeled),"LABELED",Iif(!Emptynul(picked),"PICKED",Iif(!Emptynul(pulleddt),"PULLED","ALLOCATED"))))) In XRPT4
Select *, Space(5) As AX_WHSE From XRPT4 Into Cursor xrptj Readwrite
*!*	replace ax_whse WITH '1000' FOR account =6303
*!*	replace ax_whse WITH '1200' FOR account =6304
*!*	replace ax_whse WITH '1320' FOR account =6305
*!*	replace ax_whse WITH '1310' FOR account =6320
*!*	replace ax_whse WITH '1221' FOR account =6321
*!*	replace ax_whse WITH '1222' FOR account =6322
*!*	replace ax_whse WITH '1223' FOR account =6323
*!*	replace ax_whse WITH '1250' FOR account =6324
*!*	replace ax_whse WITH '1151' FOR account =6325
*!*	replace ax_whse WITH '1400' FOR account =6364
*!*	replace ax_whse WITH '1600' FOR account =6543

Replace AX_WHSE With '10100' For account =6303
Replace AX_WHSE With '10180' For account =6325
Replace AX_WHSE With '10190' For account =6543


Scan
	xsqlexec("select called from apptcall where mod='J' and outshipid="+Transform(xrptj.outshipid),,,"wh")
	Calculate Min(called) To aa
	Replace called With aa In xrptj
Endscan


**************remove for CA   SEE BELOW UNCOMMENT OUT USE STATEMENTS
***SELECT * FROM xrptj INTO CURSOR xrpt READWRITE
********************start CA
GOFFICE='L'
xsqlexec("select * from pt where mod='"+GOFFICE+"'","ptc",,"wh")
Index On ptid Tag ptid
Set Order To

If !Used("account")
	xsqlexec("select * from account where inactive=0","account",,"qq")
	Index On accountid Tag accountid
	Set Order To
Endif

Select dept, ship_ref, batch_num As div,consignee, cnee_ref, qty, ptdate, Start, Cancel, {}As called, {} routedfor, {} appt, scac, ;
	space(20) appt_num,Space(35) apptremarks, ship_via,Space(20) bol_no, 00000000 wo_num,{}  wo_date,{}  pulleddt,{}  picked,{}  labeled,{} staged,000000  ctnqty,0000  outshipid, ;
	0000 As pt_age, 0000 As days_to_cncll, accountid As account,Space(2) As whse From ptc ;
	where Inlist(accountid,6303,6543,6325) 	Into Cursor xrpt2c Readwrite

xsqlexec("select dept, ship_ref, batch_num as div, consignee, cnee_ref, qty, ptdate, start, cancel, called, " + ;
	"routedfor, appt, scac, appt_num, apptremarks, ship_via, bol_no, wo_num, wo_date, pulleddt, picked, labeled, " + ;
	"staged, ctnqty, outshipid, cast(0 as numeric(4,0)) as pt_age, cast(0 as numeric(4,0)) as days_to_cncl, " + ;
	"accountid as account, cast('' as char(2)) as whse from outship where mod='L' " + ;
	"and inlist(accountid,6303,6304,6305,6320,6321,6322,6323,6324,6325,6364,6543) and del_date={} and notonwip=0","xrpt1",,"wh")

Select * From xrpt1c Union All Select * From xrpt2c Into Cursor xrpt3c Readwrite
Replace All pt_age With (Date()-ptdate)
Replace All   days_to_cncl With 9999 For Cancel={01/01/1900}
Replace All days_to_cncl With (Cancel-Date()) For Cancel !={01/01/1900}
Replace All whse With 'CA'
Select xrpt3c.*, Space(15) As Status From xrpt3c Into Cursor XRPT4c Readwrite
Replace All Status With Iif(Emptynul(wo_date),"UNALLOCATED",Iif(!Emptynul(staged),"STAGED",Iif(!Emptynul(labeled),"LABELED",Iif(!Emptynul(picked),"PICKED",Iif(!Emptynul(pulleddt),"PULLED","ALLOCATED"))))) In XRPT4c
Select *, Space(5) As AX_WHSE From XRPT4c Into Cursor xrptc Readwrite
Replace AX_WHSE With '11100' For account =6303
Replace AX_WHSE With '11180' For account =6325
Replace AX_WHSE With '11190' For account =6543

Scan
	xsqlexec("select called from apptcall where mod='L' and outshipid="+Transform(xrptc.outshipid),,,"wh")
	Calculate Min(called) To aa
	Replace called With aa In xrptc
Endscan


Select * From xrptc Union All Select * From xrptj Into Cursor xrpt

*!*	Use In account
*!*	Use In apptcall
*!*	Use In outshipc
*!*	IF USED("outship")
*!*	Use In outship
*!*	endif
*!*	Use In pt
*!*	Use In ptc

Select  xrpt
*copy To S:\MarcJacobsData\Reports\mjdashboard\mjkpiwip TYPE csv
copy To F:\FTPUSERS\MJ_Wholesale\OUT\reports\mjkpiwip TYPE csv
schedupdate()

_Screen.Caption=gscreencaption
On Error