*!* m:\dev\prg\alpha6_940_bkdn.prg
CLEAR
lCheckStyle = .T.
lPrepack = .F.  && Inventory to be kept as PnP
m.color=""
cUCCNumber = ""
cUsePack = ""
lcUCCSeed = 1

goffice="I"
gmasteroffice="N"

CREATE CURSOR tempsr1 (ship_ref c(20))
SELECT x856
LOCATE FOR x856.segment = "ISA"
cisa_num = ALLTRIM(x856.f13)

SCAN FOR x856.segment = "W05"
	m.ship_ref = ALLTRIM(x856.f2)
	INSERT INTO tempsr1 FROM MEMVAR
ENDSCAN
SELECT ship_ref,COUNT(ship_ref) AS cnt1 FROM tempsr1 GROUP BY ship_ref  INTO CURSOR tempsr2 HAVING cnt1 > 1
USE IN tempsr1
SELECT tempsr2

LOCATE
IF !EOF()
	COPY TO h:\fox\alpha6dups.XLS TYPE XL5
	tsubjectdup = "940 Received with Duplicated Orders"
	tmessagedup = "Duplicate Order #'s were found in the 940, and these were not loaded. Please send them in a new 940."
	tmessagedup = tmessagedup+CHR(13)+"File "+cFilename+",ISA # "+cisa_num
	tsendtodup  = tsendtoerr
	tccdup = tccerr+",chris.malcolm@tollgroup.com"
	tattachdup = "H:\FOX\alpha6dups.xls"
	DO FORM m:\dev\frm\dartmail2 WITH tsendtodup,tfrom,tsubjectdup,tccdup,tattachdup,tmessagedup,"A"
	IF FILE("H:\FOX\alpha6dups.xls")
		DELETE FILE "H:\FOX\alpha6dups.xls"
	ENDIF
ENDIF

SELECT x856
COUNT FOR TRIM(segment) = "ST" TO nSegCnt
cSegCnt = ALLTRIM(STR(nSegCnt))
SELECT x856
LOCATE

ptctr = 0

WAIT WINDOW "Now in 940 Breakdown" TIMEOUT 1

WAIT "There are "+cSegCnt+" P/Ts in this file" WINDOW TIMEOUT 1
SELECT x856
LOCATE

*!* Constants
m.acct_name = "ALPHA 6 DISTRIBUTORS"
STORE nAcctNum TO m.accountid
m.careof = " "
m.sf_addr1 = "36-20 34TH ST"
m.sf_csz = "LONG ISLAND CITY, NY 11106"
m.printed = .F.
m.print_by = ""
m.printdate = DATE()
*!* End constants

* removed dy 3/28/17
*!*	if usesqlack()	
*!*		useca("ackdata","wh")
*!*	else
*!*		If !Used("ackdata")
*!*		  Use F:\3pl\Data\ackdata In 0
*!*		Endif
*!*	endif

*!*	IF !USED("isa_num")
*!*		IF lTesting
*!*			USE F:\3pl\test\isa_num IN 0 ALIAS isa_num
*!*		ELSE
*!*			USE F:\3pl\DATA\isa_num IN 0 ALIAS isa_num
*!*		ENDIF
*!*	ENDIF

*!*	IF !USED("uploaddet")
*!*		IF lTesting
*!*			USE F:\ediwhse\test\uploaddet IN 0 ALIAS uploaddet
*!*		ELSE
*!*			USE F:\ediwhse\DATA\uploaddet IN 0 SHARED ALIAS uploaddet
*!*		ENDIF
*!*	ENDIF

SET STEP on
upcmastsql(5726)
INDEX ON STYLE TAG STYLE

SELECT xpt
DELETE ALL
SELECT xptdet
DELETE ALL

STORE 0 TO pt_total_eaches
STORE 0 TO pt_total_cartons
STORE 0 TO nCtnCount

CREATE CURSOR temperrmsg (ship_ref c(20), errmsg c(60))
SELECT x856
SET FILTER TO
LOCATE

STORE "" TO cisa_num

WAIT WINDOW "NOW IN INITAL HEADER BREAKDOWN" TIMEOUT 1
WAIT WINDOW "NOW IN INITAL HEADER BREAKDOWN" NOWAIT
*ASSERT .F. MESSAGE "At header bkdn...debug"
DO WHILE !EOF("x856")
	cSegment = TRIM(x856.segment)
	cCode = TRIM(x856.f1)

	IF TRIM(x856.segment) = "ISA"
		cisa_num = ALLTRIM(x856.f13)
		CurrentISANum = ALLTRIM(x856.f13)
		WAIT WINDOW "This is an Alpha6 PT upload" TIMEOUT 2
		ptctr = 0
		SELECT x856
		SKIP
		LOOP
	ENDIF

	IF TRIM(x856.segment) = "GS"
		cgs_num = ALLTRIM(x856.f6)
		lcCurrentGroupNum = ALLTRIM(x856.f6)
*!*			SELECT isa_num
*!*			LOCATE FOR isa_num.isa_num = cisa_num ;
*!*				AND isa_num.acct_num = nAcctNum
*!*			IF !FOUND()
*!*				APPEND BLANK
*!*				REPLACE isa_num.isa_num WITH cisa_num IN isa_num
*!*				REPLACE isa_num.acct_num WITH nAcctNum IN isa_num
*!*				REPLACE isa_num.office WITH cOffice IN isa_num
*!*				REPLACE isa_num.TYPE WITH "940" IN isa_num
*!*				REPLACE isa_num.ack WITH .F. IN isa_num
*!*				REPLACE isa_num.dateloaded WITH DATETIME() IN isa_num
*!*				REPLACE isa_num.filename WITH UPPER(TRIM(xfile)) IN isa_num
*!*			ENDIF
		SELECT x856
		SKIP
		LOOP
	ENDIF

	IF TRIM(x856.segment) = "ST" AND TRIM(x856.f1) = "940"
		SELECT x856
		SKIP
		LOOP
	ENDIF

	IF TRIM(x856.segment) = "W05"  && new shipment
		nCtnCount = 0
		m.style = ""
		SELECT xpt
		nDetCnt = 0
		APPEND BLANK

		REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"FILENAME*"+cFilename IN xpt
		REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"CONAME*ALPHA6" IN xpt
		REPLACE xpt.qty WITH 0 IN xpt
		REPLACE xpt.origqty WITH 0 IN xpt
		STORE 0 TO pt_total_eaches
		STORE 0 TO pt_total_cartons
		ptctr = ptctr +1
		WAIT WINDOW AT 10,10 "Now processing PT# "+ ALLTRIM(x856.f2)+" Number "+CHR(13)+TRANSFORM(ptctr)+" of "+cSegCnt NOWAIT

		REPLACE xpt.ptid       WITH ptctr IN xpt
		REPLACE xpt.accountid  WITH nAcctNum IN xpt
*		REPLACE xpt.office     WITH cOffice && "N"
		REPLACE xpt.ptdate     WITH DATE() IN xpt
		m.ship_ref = ALLTRIM(x856.f2)
		cship_ref = ALLTRIM(x856.f2)
		REPLACE xpt.ship_ref   WITH m.ship_ref IN xpt  && Pick Ticket
		m.pt = ALLTRIM(x856.f2)
		REPLACE xpt.cnee_ref   WITH ALLTRIM(x856.f3)  && cust PO
		SELECT x856
		SKIP
		LOOP
	ENDIF

	IF TRIM(x856.segment) = "N1" AND TRIM(x856.f1) = "ST" && Ship-to data
*!*			SELECT uploaddet
*!*			LOCATE
*!*			DO WHILE !FLOCK('uploaddet')
*!*				WAIT WINDOW "" NOWAIT
*!*			ENDDO
*!*			IF !EMPTY(cisa_num)
*!*				APPEND BLANK
*!*				REPLACE uploaddet.office    WITH cOffice
*!*				REPLACE uploaddet.cust_name WITH UPPER(TRIM(x856.f2))
*!*				REPLACE uploaddet.acct_num  WITH nAcctNum
*!*				REPLACE uploaddet.DATE      WITH DATETIME()
*!*				REPLACE uploaddet.isa_num   WITH cisa_num
*!*				REPLACE uploaddet.startpt   WITH xpt.ship_ref
*!*				REPLACE uploaddet.runid     WITH lnRunID
*!*			ENDIF
*!*			UNLOCK IN uploaddet

		cConsignee = UPPER(ALLTRIM(x856.f2))
		SELECT xpt
		m.st_name = cConsignee
		REPLACE xpt.consignee WITH  cConsignee IN xpt

		IF "WAL-MART DC"$cConsignee
			REPLACE xpt.dcnum WITH SUBSTR(cConsignee,13,5) IN xpt
		ENDIF

		IF "NORDSTROM"$cConsignee
			cStoreNum = ALLTRIM(x856.f4)
			REPLACE xpt.sforstore  WITH ALLTRIM(x856.f4) IN xpt
			cDCNUM = SUBSTR(ALLTRIM(cConsignee),1,4)
			REPLACE xpt.dcnum WITH cDCNUM IN xpt
			IF EMPTY(xpt.shipins)
				REPLACE xpt.shipins WITH "STORENUM*"+cStoreNum IN xpt
			ELSE
				REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"STORENUM*"+cStoreNum IN xpt
			ENDIF

			IF LEN(cStoreNum)>5
				REPLACE xpt.storenum  WITH VAL(RIGHT(cStoreNum,5)) IN xpt
			ELSE
				REPLACE xpt.storenum  WITH VAL(cStoreNum) IN xpt
			ENDIF
		ELSE
			cStoreNum = ALLTRIM(x856.f4)
			IF EMPTY(xpt.shipins)
				REPLACE xpt.shipins WITH "STORENUM*"+cStoreNum IN xpt
			ELSE
				REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"STORENUM*"+cStoreNum IN xpt
			ENDIF
			IF EMPTY(xpt.dcnum)
				REPLACE xpt.dcnum WITH cStoreNum IN xpt
			ENDIF
			IF LEN(cStoreNum)>5
				REPLACE xpt.storenum  WITH VAL(RIGHT(cStoreNum,5)) IN xpt
			ELSE
				REPLACE xpt.storenum  WITH VAL(cStoreNum) IN xpt
			ENDIF
		ENDIF

		SKIP 1 IN x856
		IF TRIM(x856.segment) = "N3"  && address info
			m.st_addr1 = UPPER(TRIM(x856.f1))
			m.st_addr2 = UPPER(TRIM(x856.f2))
			REPLACE xpt.address  WITH m.st_addr1
			REPLACE xpt.address2 WITH m.st_addr2
		ENDIF
		SKIP 1 IN x856
		IF TRIM(x856.segment) = "N4"  && address info
			m.zipbar = TRIM(x856.f3)
			m.st_csz = UPPER(TRIM(x856.f1))+", "+UPPER(TRIM(x856.f2))+" "+TRIM(x856.f3)
			REPLACE xpt.csz WITH m.st_csz IN xpt
		ENDIF
		SELECT x856
		SKIP
		LOOP
	ENDIF

	IF TRIM(x856.segment) = "N1" AND INLIST(TRIM(x856.f1),"SD","PF") && Returned data
		cNom = ALLTRIM(x856.f1)
		IF lTesting AND cNom = "SD"
*SET STEP ON
		ENDIF
		REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+cNom+"_N1*"+ALLTRIM(x856.f2) IN xpt
		SKIP 1 IN x856

		DO CASE
			CASE TRIM(x856.segment) = "N2"
				REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+cNom+"_N2*"+ALLTRIM(x856.f1) IN xpt
				SKIP 1 IN x856
				IF TRIM(x856.segment) = "N3"
					REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+cNom+"_N3*"+ALLTRIM(x856.f1)+"|"+ALLTRIM(x856.f2) IN xpt
					SKIP 1 IN x856
				ENDIF
				IF TRIM(x856.segment) = "N4"
					REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+cNom+"_N4*"+ALLTRIM(x856.f1)+"|"+ALLTRIM(x856.f2)+"|"+ALLTRIM(x856.f3) IN xpt
				ENDIF

			CASE TRIM(x856.segment) = "N3"
				REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+cNom+"_N3*"+ALLTRIM(x856.f1)+"|"+ALLTRIM(x856.f2) IN xpt
				SKIP 1 IN x856
				IF TRIM(x856.segment) = "N4"
					REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+cNom+"_N4*"+ALLTRIM(x856.f1)+"|"+ALLTRIM(x856.f2)+"|"+ALLTRIM(x856.f3) IN xpt
				ENDIF

			CASE TRIM(x856.segment) = "N4"
				REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+cNom+"_N4*"+ALLTRIM(x856.f1)+"|"+ALLTRIM(x856.f2)+"|"+ALLTRIM(x856.f3) IN xpt
		ENDCASE

		SELECT x856
		SKIP
		LOOP
	ENDIF

	IF (TRIM(x856.segment) = "N1" AND TRIM(x856.f1) = "MA")
		STORE UPPER(TRIM(x856.f2)) TO cMarkfor && will be overwritten if there is an N2 loop
		REPLACE xpt.shipfor WITH cMarkfor IN xpt
		cSFStoreNum = UPPER(ALLTRIM(x856.f4))
		REPLACE xpt.sforstore WITH cSFStoreNum IN xpt
		REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"SFSTORENUM*"+cSFStoreNum IN xpt  && DC or Store Number
		SKIP 1 IN x856
		DO CASE
			CASE TRIM(x856.segment) = "N2"  && address info
				STORE UPPER(TRIM(x856.f2)) TO cMarkfor
				REPLACE xpt.shipfor WITH cMarkfor IN xpt
				REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"SFSTORELONG*"+UPPER(ALLTRIM(x856.f1)) IN xpt
				REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"STORENAME*"+ALLTRIM(x856.f2) IN xpt
				REPLACE xpt.sforstore WITH ALLTRIM(x856.f1) IN xpt
				SKIP 1 IN x856
				IF TRIM(x856.segment) = "N3"  && address info
					REPLACE xpt.sforaddr1 WITH UPPER(TRIM(x856.f1)) IN xpt
					REPLACE xpt.sforaddr2  WITH UPPER(TRIM(x856.f2)) IN xpt
				ELSE
					cErrMsg = "Missing Ship-For Address Info (N2>N3), PT "+cship_ref
					WAIT WINDOW cMessage TIMEOUT 2
					SELECT xpt
					DELETE NEXT 1 IN xpt
					INSERT INTO temperrmsg (ship_ref,errmsg) VALUES (cship_ref,cErrMsg)
					SELECT x856
					DO WHILE(x856.segment) # "SE"
						SKIP
					ENDDO
					LOOP
				ENDIF
			CASE TRIM(x856.segment) = "N3"  && address info
				REPLACE xpt.sforaddr1 WITH UPPER(TRIM(x856.f1)) IN xpt
				REPLACE xpt.sforaddr2  WITH UPPER(TRIM(x856.f2)) IN xpt
			OTHERWISE
				cErrMsg = "Missing Ship-For Address Info (N1>N2), PT "+cship_ref
				WAIT WINDOW cErrMsg TIMEOUT 2
				SELECT xpt
				DELETE NEXT 1 IN xpt
				INSERT INTO temperrmsg (ship_ref,errmsg) VALUES (cship_ref,cErrMsg)
				SELECT x856
				DO WHILE(x856.segment) # "SE"
					SKIP
				ENDDO
				LOOP
		ENDCASE
		SKIP 1 IN x856
		IF TRIM(x856.segment) = "N4"  && address info
*!*				IF EMPTY(TRIM(x856.f2))
*!*				csforCSZ = TRIM(x856.f1)+", "+TRIM(x856.f4)+" "+TRIM(x856.f3)
*!*				else
			csforCSZ = TRIM(x856.f1)+", "+PADR(TRIM(x856.f2),2)+" "+TRIM(x856.f3)
*!*				endif
			REPLACE xpt.sforcsz WITH UPPER(csforCSZ) IN xpt
		ELSE
			cMessage = "Missing Ship-For CSZ Info "+cship_ref
			WAIT WINDOW cMessage TIMEOUT 2
			SELECT xpt
			DELETE NEXT 1 IN xpt
			INSERT INTO temperrmsg (ship_ref,errmsg) VALUES (cship_ref,cErrMsg)
			SELECT x856
			DO WHILE(x856.segment) # "SE"
				SKIP
			ENDDO
			LOOP
		ENDIF
	ENDIF

	IF TRIM(x856.segment) = "N9" AND TRIM(x856.f1) = "EM"  && customer email
		cEmail = ALLTRIM(x856.f2)
		REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"EMAIL*"+cEmail IN xpt
		SELECT x856
		SKIP
		LOOP
	ENDIF

	IF TRIM(x856.segment) = "N9" AND TRIM(x856.f1) = "DP"  && department
		REPLACE xpt.dept WITH ALLTRIM(x856.f2)
		SELECT x856
		SKIP
		LOOP
	ENDIF

	IF TRIM(x856.segment) = "G62" AND TRIM(x856.f1) == "37"  && start date
		lcDate = ALLTRIM(x856.f2)
		lxDate = SUBSTR(lcDate,5,2)+"/"+SUBSTR(lcDate,7,2)+"/"+LEFT(lcDate,4)
		ldDate = CTOD(lxDate)
		STORE ldDate TO dStart
		REPLACE xpt.START WITH ldDate IN xpt
		SELECT x856
		SKIP
		LOOP
	ENDIF

	IF TRIM(x856.segment) = "G62" AND TRIM(x856.f1) == "38"  && cancel date
		lcDate = ALLTRIM(x856.f2)
		lxDate = SUBSTR(lcDate,5,2)+"/"+SUBSTR(lcDate,7,2)+"/"+LEFT(lcDate,4)
		ldDate = CTOD(lxDate)
		STORE ldDate TO dCancel
		REPLACE xpt.CANCEL WITH ldDate IN xpt
		SELECT x856
		SKIP
		LOOP
	ENDIF

	IF TRIM(x856.segment) = "NTE" AND ALLTRIM(x856.f1) = "WHI"
	SET STEP ON 
		replace xpt.shipins WITH xpt.shipins+CHR(13)+"SPINS*"+ALLTRIM(x856.f2)
		SELECT x856
		SKIP
		LOOP
	ENDIF


	IF TRIM(x856.segment) = "W66"
	SET STEP ON 
		REPLACE xpt.terms WITH ALLTRIM(x856.f1)
		replace xpt.ship_via WITH ALLTRIM(x856.f5)
		SELECT x856
		SKIP
		LOOP
	ENDIF

	IF TRIM(x856.segment) = "LX" && start of PT detail, stay here until this PT is complete
		m.units = .T.
		WAIT "NOW IN DETAIL LOOP PROCESSING" WINDOW NOWAIT

		SELECT xptdet
		CALCULATE MAX(ptdetid) TO m.ptdetid
		m.ptdetid = m.ptdetid + 1

		SELECT xptdet
		SKIP 1 IN x856
		lnLinenum=1
		DO WHILE ALLTRIM(x856.segment) != "SE"
			cSegment = TRIM(x856.segment)
			cCode = TRIM(x856.f1)
			IF TRIM(x856.segment) = "W01" && Destination Quantity
				SELECT xptdet
				APPEND BLANK
				REPLACE xptdet.ptid WITH xpt.ptid IN xptdet
				REPLACE xptdet.ptdetid WITH m.ptdetid IN xptdet
				REPLACE xptdet.units WITH m.units IN xptdet
				REPLACE xptdet.linenum WITH ALLTRIM(STR(lnLinenum)) IN xptdet
				lnLinenum = lnLinenum + 1
				m.ptdetid = m.ptdetid + 1
				REPLACE xptdet.accountid WITH xpt.accountid IN xptdet
				cUsePack = "1"
				REPLACE xptdet.PACK WITH cUsePack IN xptdet
				m.casepack = INT(VAL(cUsePack))
				REPLACE xptdet.casepack WITH m.casepack IN xptdet

				REPLACE xptdet.totqty WITH INT(VAL(x856.f1)) IN xptdet
				REPLACE xpt.qty WITH xpt.qty + xptdet.totqty IN xpt
				REPLACE xpt.origqty WITH xpt.qty IN xpt
				REPLACE xptdet.origqty WITH xptdet.totqty IN xptdet

				IF EMPTY(TRIM(xptdet.printstuff))
					REPLACE xptdet.printstuff WITH "UOM*"+ALLTRIM(x856.f2) IN xptdet
				ELSE
					REPLACE xptdet.printstuff WITH xptdet.printstuff+CHR(13)+"UOM*"+ALLTRIM(x856.f2) IN xptdet
				ENDIF

				IF ALLTRIM(x856.f4) = "IN"
					REPLACE xptdet.STYLE WITH ALLTRIM(x856.f5) IN xptdet
					IF SEEK(xptdet.STYLE,"upcmast","style")
						REPLACE xptdet.upc WITH upcmast.upc IN xptdet
					ENDIF
				ENDIF

				IF ALLTRIM(x856.f6) = "UP"
					cUPCCode = ALLTRIM(x856.f7)
					REPLACE xptdet.printstuff WITH xptdet.printstuff+CHR(13)+"UPCCODE*"+cUPCCode IN xptdet
					REPLACE xptdet.upc WITH cUPCCode IN xptdet
				ENDIF
			ENDIF

			IF TRIM(x856.segment) = "G69" && desc
				cPrtstr = "DESC*"+UPPER(TRIM(x856.f1))
				REPLACE xptdet.printstuff WITH xptdet.printstuff+CHR(13)+cPrtstr IN xptdet
			ENDIF

			SKIP 1 IN x856
		ENDDO

		SELECT xptdet
	ENDIF

	SELECT x856
	IF !EOF()
		SKIP 1 IN x856
	ENDIF
ENDDO

SELECT xptdet

rollup()
WAIT WINDOW "At end of ROLLUP procedure" NOWAIT

DO m:\dev\prg\setuppercase940

SELECT tempsr2
LOCATE
SCAN
	SELECT xpt
	SCAN FOR xpt.ship_ref = tempsr2.ship_ref AND !DELETED()
		nPTID = xpt.ptid
		DELETE FOR xpt.ptid = nPTID
		SELECT xptdet
		DELETE FOR xpt.ptid = nPTID
	ENDSCAN
	SELECT tempsr2
ENDSCAN

IF lBrowfiles
	WAIT WINDOW "If tables are incorrect, CANCEL when XPTDET window opens"+CHR(13)+"Otherwise, will load PT table" TIMEOUT 2
	SELECT xpt
	COPY TO h:\xpt TYPE XL5
	LOCATE
	BROWSE
	SELECT xptdet
	COPY TO h:\xptdet TYPE XL5
	LOCATE
	BROWSE
*	cancel
ENDIF
SELECT temperrmsg
LOCATE
IF !EOF('temperrmsg')
	SELECT 0
	USE F:\3pl\DATA\mailmaster ALIAS mm SHARED
	LOCATE FOR mm.edi_type = "MISC" AND mm.taskname = IIF(lTesting,"JOETEST","DUPPTNOTICE")
	STORE TRIM(IIF(mm.use_alt,mm.sendtoalt,mm.sendto)) TO tsendtopt
	STORE TRIM(IIF(mm.use_alt,mm.ccalt,mm.cc)) TO tccpt
	USE IN mm
	cMailLoc = IIF(cOffice = "C","West",IIF(cOffice = "M","Florida","New Jersey"))
	tsubjectpt = "TGF "+cMailLoc+" "+cCustname+" Pickticket Error: " +TTOC(DATETIME())
	tmessagept = "The following PTs did not load due to the indicated errors:"+CHR(13)+CHR(13)+PADR("PICKTICKET",22)+"MESSAGE"+CHR(13)
	SELECT temperrmsg
	SCAN
		tmessagept = tmessagept+CHR(13)+PADR(ALLTRIM(temperrmsg.ship_ref),22)+ALLTRIM(temperrmsg.errmsg)
	ENDSCAN
	tmessagept = tmessagept+CHR(13)+CHR(13)+"Please resend the affected orders."
	tattach = ""
	DO FORM m:\dev\frm\dartmail2 WITH tsendtopt,tfrom,tsubjectpt,tccpt,tattach,tmessagept,"A"
ENDIF

WAIT WINDOW cCustname+" Breakdown Round complete..." TIMEOUT 2
SELECT x856
IF USED("PT")
	USE IN pt
ENDIF
IF USED("PTDET")
	USE IN ptdet
ENDIF
WAIT CLEAR
RETURN

******************************
PROCEDURE rollup
******************************
	ASSERT .F. MESSAGE "In rollup procedure"
	nChangedRecs = 0
	SELECT xptdet
	SET DELETED ON
	LOCATE
	IF lTesting
*BROWSE
	ENDIF
	cMsgStr = "PT/OSID/ODIDs changed: "
	SCAN FOR !DELETED()
		SCATTER MEMVAR
		nRecno = RECNO()
		LOCATE FOR xptdet.ptid = m.ptid ;
			AND xptdet.STYLE = m.style ;
			AND xptdet.COLOR = m.color ;
			AND xptdet.ID = m.id ;
			AND xptdet.PACK = m.pack ;
			AND xptdet.upc = m.upc ;
			AND RECNO('xptdet') > nRecno
		IF FOUND()
			SELECT xpt
			LOCATE FOR xpt.ptid = m.ptid
			IF !FOUND()
				WAIT WINDOW "PROBLEM!"
			ENDIF
			cship_ref = ALLT(xpt.ship_ref)
			IF nChangedRecs > 10
				cMsgStr = "More than 10 records rolled up"
			ELSE
				cMsgStr = cMsgStr+CHR(13)+cship_ref+" "+TRANSFORM(m.ptid)+" "+TRANSFORM(m.ptdetid)
			ENDIF
			nChangedRecs = nChangedRecs+1
			SELECT xptdet
			nTotqty1 = xptdet.totqty
			DELETE NEXT 1 IN xptdet
		ENDIF
		IF !EOF()
			GO nRecno
			REPLACE xptdet.totqty WITH xptdet.totqty+nTotqty1 IN xptdet
			REPLACE xptdet.origqty WITH xptdet.totqty IN xptdet
		ELSE
			GO nRecno
		ENDIF
	ENDSCAN

	IF nChangedRecs > 0
		WAIT WINDOW cMsgStr TIMEOUT 2
		WAIT WINDOW "There were "+TRANS(nChangedRecs)+" records rolled up" TIMEOUT 1
	ELSE
		WAIT WINDOW "There were NO records rolled up" TIMEOUT 1
	ENDIF

ENDPROC
