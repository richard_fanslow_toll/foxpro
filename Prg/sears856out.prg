*N1*VN*FMI*ZZ*FMI
*PER*CN**TE**FX**EM*SEARSEDI@FMIINT.COM
************************************************************************
* Create 856 FILES for Sears
* AUTHOR--:PG
* Modified by MB
************************************************************************
* SAVE EXE AS F:\UTIL\SEARSEDI\SEARS856OUT.EXE

#DEFINE CRLF CHR(13) + CHR(10)

utilsetup("SEARS856OUT")

TRY

	SET ENGINEBEHAVIOR 70
	SET BELL OFF
	SET CENT OFF
	SET CENTURY TO 19 ROLLOVER 80
	SET CPDIALOG OFF
	SET DATE TO AMERICAN
	SET DELETE ON
	SET ESCAPE OFF
	SET EXCLUSIVE OFF
	SET HEADING OFF
	SET HELP OFF
	SET INTENSITY ON
	SET NOTIFY OFF
	SET SAFETY OFF
	SET STATUS OFF
	SET STATUS BAR OFF
	SET TALK OFF
	SET TYPEAHEAD TO 20
	SET UNIQUE OFF

	SET MULTILOCKS ON
	SET CENTURY ON
	SET DATE TO YMD
	SET DECIMAL TO 0


	CLOSE DATA ALL
	STORE CHR(07) TO SEGTERMINATOR
	STORE "~" TO SEGTERMINATOR

	*Store "" to segterminator

	STORE "" TO FSTRING
	STORE 0 TO SHIPLEVEL

	LOCAL LOERROR, lnCartonCount, lcCOMPUTERNAME

	PUBLIC T856, GC856OUTFILE, TSHIPID,CSTRING,SLEVEL,PLEVEL,LNINTERCONTROLNUM,LCINTERCTRLNUM,STNUM,GN856CTR
	PUBLIC LCCODE,LCORIGIN,LCDISCHARGE, LCDOCRCPT, GLTESTINDICATOR, GLMOVETOAS2, GLTESTMODE
	PUBLIC GCUPLOADPATH, GCUPLOADFILE
	PUBLIC GCSENDTO, GCFROM, GCSUBJECT, GCCC, GCATTACH, GCBODYTEXT, GTSTARTTIME
	PUBLIC GCBSNSEGLIST, GCCNTRL856LIST, GCINTERCTRLLIST
	PUBLIC gnCartonCount, gnTotalWeight
	PUBLIC gnCtnCntAccum, gnShipWtAccum
	PUBLIC GNNUMFILES

	GTSTARTTIME = DATETIME()
	GCSENDTO = "mbennett@fmiint.com"
	GCFROM = "TGF Corporate Communication Department <transload-ops@fmiint.com>"
	GCSUBJECT = "Sears EDI 856 Process Results - " + TTOC(DATETIME())
	GCCC = ""
	GCATTACH = ""
	GCBODYTEXT = ""
	GLMOVETOAS2 = .T.
	GLTESTINDICATOR = .F.
	GCBSNSEGLIST = "GCBSNSEGLIST:"
	GCCNTRL856LIST = "GCCNTRL856LIST:"
  	GCINTERCTRLLIST = "GCINTERCTRLLIST:"
  	GNNUMFILES = 0

	******************************************************
	******************************************************
	GLTESTMODE = .F.

	IF GLTESTMODE THEN
		WAIT WINDOW "TEST MODE" TIMEOUT 1
		GCOUTPUTFOLDER = "C:\SEARSEDI\EDIDATAOUT\ARCHIVE\"
		LCPATH = "C:\SEARSEDI\RGTIDATAIN\"
		LCARCHIVEPATH = "C:\SEARSEDI\RGTIDATAIN\ARCHIVE\"
		GCUPLOADPATH = "C:\SEARSEDI\AS2UPLOAD\"
		GCSENDTO = "mbennett@fmiint.com"
		GCCC = ""
	ELSE
		GCOUTPUTFOLDER = "F:\FTPUSERS\SEARSEDI\EDIDATAOUT\ARCHIVE\"
		LCPATH = "F:\FTPUSERS\SEARSEDI\RGTIDATAIN\"
		LCARCHIVEPATH = "F:\FTPUSERS\SEARSEDI\RGTIDATAIN\ARCHIVE\"
		
		* per Renay & SHC EDI, 11/28/2016, send all 856s to the new SI channel.
		*GCUPLOADPATH = "F:\FTPUSERS\SEARSEDI\AS2UPLOAD\"
		GCUPLOADPATH = "F:\FTPUSERS\SEARSEDI-SI\AS2UPLOAD\"
		
*!*			GCSENDTO = 'dougwachs@fmiint.com,pgaidis@fmiint.com, mbennett@fmiint.com'
*!*			GCCC = 'pgaidis@fmiint.com, mbennett@fmiint.com'
		GCSENDTO = "mbennett@fmiint.com"
		GCCC = ""
	ENDIF
	******************************************************
	******************************************************

	LCCODE = "UNKN"
	LCORIGIN= "UNK"
	LCDISCHARGE= "UNK"
	LCDOCRCPT = "UNK"

	STORE 0 TO GN856CTR,STNUM,LNCTTCOUNT,LNSECOUNT
	STORE 0 TO HLCOUNT2,HLPACK,SPCNTP,HLSEGCNT,SEGCNT,SPCNT,SPCNT2,HLITEM,SPCNTI,LC_HLCOUNT,A,LINCNT,TOTLINCNT,SESEGCNT,SEGCNT

	STORE "" TO WORKORDERSPROCESSED,SUMMARYINFO,LC_856NUM,LC_CNUM

	* top level counter for unique HL segment counts
	STORE 0 TO HLCTR

	* use these to store overall counters
	STORE 0 TO SHIPMENTLEVELCOUNT,ORDERLEVELCOUNT,PACKLEVELCOUNT,ITEMLEVELCOUNT

	* use these to store overall counters
	STORE 0 TO THISSHIPMENTLEVELCOUNT,THISORDERLEVELCOUNT,THISPACKLEVELCOUNT,THISITEMLEVELCOUNT

	* used to store the value of the curent count for remembering the Parent HL Count
	STORE 0 TO CURRENTSHIPMENTLEVELCOUNT,CURRENTORDERLEVELCOUNT,CURRENTPACKLEVELCOUNT,CURRENTITEMLEVELCOUNT

	STORE .F. TO LLEDITUPDATE

	LCSTRING  = ""
	LINCNT = 1
	*******************************************************************************************
	JTIMEC = TIME()
	STORE SUBSTR(JTIMEC,1,2) TO J1
	STORE SUBSTR(JTIMEC,4,2) TO J2
	STORE SUBSTR(JTIMEC,7,2) TO J3
	LCTIME = J1+J2
	TDAY = DAY(DATE())
	STORE STR(TDAY,2,0)     TO TDAY1
	STORE DTOC(DATE())      TO HDATE
	STORE SUBSTR(HDATE,6,2) TO TMON
	STORE SUBSTR(HDATE,9,2) TO TDAY
	STORE SUBSTR(HDATE,1,2) TO TCENT
	STORE SUBSTR(HDATE,3,2) TO TYR
	STORE SUBSTR(HDATE,1,4) TO TWHOLEYR
	STORE SUBSTR(HDATE,1,2) TO D4
	STORE SUBSTR(HDATE,3,2) TO D1
	STORE SUBSTR(HDATE,6,2) TO D2
	STORE SUBSTR(HDATE,9,2) TO D3

	***************************
	*** Build Header Record ***
	***************************
	*tsendid   = 'FMIF'
	TSENDID    = 'FMI'
	TSENDID    = PADR(TSENDID,12," ")
	*TSENDCODE  = '2122441111'
	TSENDCODE  = '6111250050'
	******************************
	*TQUALCODE = '2122441111'
	TQUALCODE = '6111250050'
	TQUALCODE  = PADR(TQUALCODE,15," ")
	*tqualtype = '12'
	TQUALTYPE  = 'ZZ'
	
	lcCOMPUTERNAME = GETENV("COMPUTERNAME")

	TRACKPROGRESS("Open tables and setting up.........")
	TRACKPROGRESS("Project = SEARS856OUT")
	TRACKPROGRESS("lcCOMPUTERNAME = " + lcCOMPUTERNAME)
	*******************************************************************************************
	STORE 0 TO EDI_TESTING
	USE F:\SEARSEDI\DATA\CONTROLNUM AGAIN IN 0 ALIAS CONTROLNUM SHARED

	********************************************************************************************************
	**********************************************
	*** Build Hierarchical Level *** SHIPMENT LEVEL
	***********************************************
	WAIT WINDOW "Setting up the cursors......." NOWAIT

	CD &LCPATH

	GNNUMFILES = ADIR(TARRAY,"856_*.txt")
	IF GNNUMFILES = 0
		TRACKPROGRESS("No Sears 856 EDI files to import!")
	ELSE
		FOR THISFILE = 1  TO GNNUMFILES

			IF USED('shipdata') THEN
				USE IN SHIPDATA
			ENDIF

			CREATE CURSOR SHIPDATA (;
				BOL CHAR(16),;
				CTNQTY N(7),;
				SHIPWT N(9,2),;
				SHIPDATE CHAR(14),;
				PODATE CHAR(14),;
				SHIPTODC CHAR(10),;
				SHIPNAME CHAR(25),;
				PONUM CHAR(16),;
				DEPT CHAR(6),;
				VENDORID CHAR(9),;
				MARKFOR CHAR(10),;
				BILLTO CHAR(10),;
				CARTONID CHAR(20),;
				QTY N(7),;
				CUSTSKU CHAR(20),;
				STYLE CHAR(20),;
				ITEM CHAR(5),;
				SKU CHAR(3),;
				UPC CHAR(13))

			XFILE = LCPATH+TARRAY[thisfile,1]
			ARCHIVEFILE = LCARCHIVEPATH+TARRAY[thisfile,1]
*!*				GCUPLOADFILE = GCUPLOADPATH+TARRAY[thisfile,1]

			TRACKPROGRESS("Importing file: " + XFILE)
			LNHANDLE = FOPEN(XFILE)
			DO WHILE !FEOF(LNHANDLE)
				LCSTR= FGETS(LNHANDLE)
				SELECT SHIPDATA
				APPEND BLANK
				REPLACE SHIPDATA.BOL      WITH SUBSTR(LCSTR,1,16)
				REPLACE SHIPDATA.CTNQTY   WITH VAL(SUBSTR(LCSTR,17,7))  && 23
				REPLACE SHIPDATA.SHIPWT   WITH VAL(SUBSTR(LCSTR,24,9))/100  && 32  changed to divide by 100 per doug 4/5/11 MB
				REPLACE SHIPDATA.SHIPDATE WITH SUBSTR(LCSTR,33,14)      && 46
				REPLACE SHIPDATA.PODATE   WITH SUBSTR(LCSTR,33,14)      && 46
				REPLACE SHIPDATA.SHIPTODC WITH SUBSTR(LCSTR,47,10)      && 56
				REPLACE SHIPDATA.SHIPNAME WITH SUBSTR(LCSTR,57,25)      && 81
				REPLACE SHIPDATA.PONUM    WITH ALLTRIM(SUBSTR(LCSTR,82,16))      && 97
				REPLACE SHIPDATA.DEPT     WITH SUBSTR(LCSTR,98,6)       && 103
				REPLACE SHIPDATA.VENDORID WITH PADL(ALLTRIM(SUBSTR(LCSTR,104,9)),9,'0') && 112
				REPLACE SHIPDATA.MARKFOR  WITH SUBSTR(LCSTR,113,10)     && 122
				REPLACE SHIPDATA.BILLTO   WITH SUBSTR(LCSTR,123,10)     && 132
				REPLACE SHIPDATA.CARTONID WITH SUBSTR(LCSTR,133,20)     && 152
				REPLACE SHIPDATA.QTY      WITH VAL(SUBSTR(LCSTR,153,7)) && 159
				REPLACE SHIPDATA.CUSTSKU  WITH SUBSTR(LCSTR,160,20)     && 179
				REPLACE SHIPDATA.STYLE    WITH SUBSTR(LCSTR,180,20)     && 199

				REPLACE SHIPDATA.ITEM     WITH SUBSTR(LCSTR,163,5)      &&
				IF SUBSTR(ITEM,1,1)="0"
					REPLACE SHIPDATA.ITEM   WITH SUBSTR(LCSTR,164,4)      &&
				ENDIF
				REPLACE SHIPDATA.SKU      WITH SUBSTR(LCSTR,168,3)      &&

				REPLACE SHIPDATA.UPC      WITH SUBSTR(LCSTR,200,13)     &&
			ENDDO
			FCLOSE(LNHANDLE)

			*!*		IF glTestMode THEN
			*!*			SELECT shipdata
			*!*			BROWSE
			*!*		ENDIF
*!*	SELECT SHIPDATA
*!*	COPY TO C:\A\SHIPDATA
*!*	THROW			
*!*	SELECT DISTINCT BOL, MARKFOR, CARTONID, SHIPWT ;
*!*		FROM SHIPDATA INTO CURSOR TEMP ORDER BY 1,2,3
*!*	SELECT TEMP
*!*	COPY TO C:\A\STATS856.XLS XL5
*!*	THROW

			DELETE FOR EMPTY(BOL)
			INDEX ON MARKFOR+DEPT+CARTONID TAG UCCDEPT
			INDEX ON BOL TAG BOLPO  &&+ponum
			SET ORDER TO BOLPO
			SELECT DISTINCT BOL FROM SHIPDATA INTO CURSOR BOLINDEX
			SELECT BOLINDEX
			GOTO TOP
			SCAN
				*    Set Step On
				* BELOW NOT WORKING BECAUSE SELECT DOES NOT RESPECT SET FILTER
				*!*	SELECT SHIPDATA
				*!*	SET FILTER TO SHIPDATA.BOL = BOLINDEX.BOL
				*!*	SELECT DISTINCT CARTONID FROM SHIPDATA INTO CURSOR COUNTCTNS
				*!*	SELECT SHIPDATA
				*!*	REPLACE ALL CTNQTY WITH RECCOUNT("countctns")
				
				SELECT DISTINCT CARTONID, SHIPWT ;
					FROM SHIPDATA ;
					INTO CURSOR COUNTCTNS ;
					WHERE SHIPDATA.BOL = BOLINDEX.BOL ;
					ORDER BY 1
					
				SELECT COUNTCTNS
				gnCartonCount = RECCOUNT()
				SUM COUNTCTNS.SHIPWT TO gnTotalWeight

*!*					SELECT COUNTCTNS
*!*					COPY TO ("C:\A\TOT856" + ALLTRIM(BOLINDEX.BOL) + ".XLS") XL5	
				* init these BOL-specific accumulators of the cnts and wts in the inner TD1 segments...
				STORE 0 TO gnCtnCntAccum, gnShipWtAccum			
				
				SELECT SHIPDATA
				SET FILTER TO SHIPDATA.BOL = BOLINDEX.BOL
				GOTO TOP
				WRITE856()

				TRACKPROGRESS("gnCtnCntAccum = " + TRANSFORM(gnCtnCntAccum))
				TRACKPROGRESS("gnShipWtAccum = " + TRANSFORM(gnShipWtAccum))
				
			ENDSCAN
		
			&& here we should archive and delete the source files
			COPY FILE (XFILE) TO (ARCHIVEFILE)
			IF FILE(ARCHIVEFILE) THEN
				DELETE FILE (XFILE)
				TRACKPROGRESS("Successfully archived " + XFILE)
			ELSE
				TRACKPROGRESS("!! There was an error archiving " + XFILE)
			ENDIF
			
		NEXT
		
*!*			&& here we should archive and delete the source files
*!*			COPY FILE (XFILE) TO (ARCHIVEFILE)
*!*			IF FILE(ARCHIVEFILE) THEN
*!*				DELETE FILE (XFILE)
*!*				TRACKPROGRESS("Successfully archived " + XFILE)
*!*			ELSE
*!*				TRACKPROGRESS("!! There was an error archiving " + XFILE)
*!*			ENDIF
		
	ENDIF  && GNNUMFILES = 0

	TRACKPROGRESS("856 Process ended normally!")

CATCH TO LOERROR

	TRACKPROGRESS('There was an error.')
	TRACKPROGRESS('The Error # is: ' + TRANSFORM(LOERROR.ERRORNO))
	TRACKPROGRESS('The Error Message is: ' + TRANSFORM(LOERROR.MESSAGE))
	TRACKPROGRESS('The Error occurred in Program: ' + TRANSFORM(LOERROR.PROCEDURE))
	TRACKPROGRESS('The Error occurred at line #: ' + TRANSFORM(LOERROR.LINENO))
	CLOSE DATABASES ALL
	IF GLTESTMODE THEN
		GCSENDTO = 'mbennett@fmiint.com'
	ELSE
		GCSENDTO = 'dougwachs@fmiint.com, pgaidis@fmiint.com, mbennett@fmiint.com'
	ENDIF	
	* set this so that email runs if there was an error, even if no files were found
	IF GNNUMFILES = 0 THEN
		GNNUMFILES = 1
	ENDIF

ENDTRY

TRACKPROGRESS('Sears EDI 856 Process started: ' + TTOC(GTSTARTTIME))
TRACKPROGRESS('Sears EDI 856 Process finished: ' + TTOC(DATETIME()))
WAIT CLEAR

* Per Doug, only send email if we process files -- to support setting the process to run every hour, instead of once per day.
IF GNNUMFILES = 0 THEN
	WAIT WINDOW TIMEOUT 3 "No 856 files found!"
ELSE
	GCBODYTEXT = GCBSNSEGLIST + CHR(13) + GCCNTRL856LIST + CHR(13) + GCINTERCTRLLIST + CHR(13) + CHR(13) + GCBODYTEXT
	DO FORM DARTMAIL2 WITH GCSENDTO, GCFROM, GCSUBJECT, GCCC, GCATTACH, GCBODYTEXT, "A"
ENDIF

schedupdate()

RETURN

********************************************************************************************************************
* 856 Top Level
********************************************************************************************************************
PROCEDURE WRITE856

	GC856OUTFILE = "SEARSEDI"+ALLTRIM(STR(YEAR(DATE())))+PADL(ALLTRIM(STR(MONTH(DATE()))),2,"0")+;
		PADL(ALLTRIM(STR(DAY(DATE()))),2,"0")+PADL(ALLTRIM(STR(HOUR(DATETIME()))),2,"0")+;
		PADL(ALLTRIM(STR(MINUTE(DATETIME()))),2,"0")+PADL(ALLTRIM(STR(SEC(DATETIME()))),2,"0")+ALLTRIM(SYS(3))+".856"

	TRACKPROGRESS('GC856OUTFILE = ' + GC856OUTFILE)

	GCUPLOADFILE = GCUPLOADPATH + GC856OUTFILE

	TRACKPROGRESS('GCUPLOADFILE = ' + GCUPLOADFILE)

	GC856OUTFILE = GCOUTPUTFOLDER + GC856OUTFILE

	TRACKPROGRESS('GC856OUTFILE = ' + GC856OUTFILE)
	
	T856 = FCREATE(GC856OUTFILE)
	IF T856 < 0
		=FCLOSE(T856)
		CLOSE DATABASES ALL
		RETURN
	ENDIF

	WAIT WINDOW "856 Processing for Sears EDI............" NOWAIT

	WRITEHEADER()

	SHIPMENTHEADER()

	STORE 3 TO SEGCNT

	XERRORMSG="" &&may be updated in shipment_level() if error occurs

	IF !SHIPMENT_LEVEL()
		=FCLOSE(T856)
		DELETE FILE &GC856OUTFILE
		THROW
	ELSE
		XERROR=.F.
		XEMPTYFILE=.T.

		SELECT SHIPDATA
		SET ORDER TO UCCDEPT
		GOTO TOP

		CURRENT_DIV     = "!!"
		CURRENT_UCC     = "!!"
		CURRENT_MARKFOR = "!!"
		NEWORDER = .T.

		DO WHILE !EOF("shipdata")
			IF CURRENT_UCC != SHIPDATA.CARTONID AND SHIPDATA.DEPT = CURRENT_DIV AND SHIPDATA.MARKFOR = CURRENT_MARKFOR
				CURRENT_UCC = SHIPDATA.CARTONID
			ELSE
				IF (SHIPDATA.DEPT != CURRENT_DIV OR SHIPDATA.MARKFOR != CURRENT_MARKFOR)
					*       Or (current_ucc = shipdata.cartonid And shipdata.dept != current_div) or (shipdata.markfor != current_markfor)
					CURRENT_DIV     = SHIPDATA.DEPT
					CURRENT_UCC     = SHIPDATA.CARTONID
					CURRENT_MARKFOR = SHIPDATA.MARKFOR
					ORDER_LEVEL()
				ENDIF
			ENDIF

			DO WHILE SHIPDATA.MARKFOR = CURRENT_MARKFOR AND SHIPDATA.DEPT = CURRENT_DIV

				IF (SHIPDATA.DEPT != CURRENT_DIV OR SHIPDATA.MARKFOR != CURRENT_MARKFOR)
					EXIT
				ELSE
					PACK_LEVEL()
					DO WHILE SHIPDATA.CARTONID = CURRENT_UCC ;
							AND SHIPDATA.DEPT = CURRENT_DIV ;  && and its the same oder where an order is defined as DIV/Markfor
						AND SHIPDATA.MARKFOR = CURRENT_MARKFOR
						ITEM_LEVEL()
						SKIP 1 IN SHIPDATA
						IF !EOF("shipdata")
							IF  SHIPDATA.CARTONID != CURRENT_UCC &&OR (shipdata.dept != current_div OR shipdata.markfor = current_markfor)
								EXIT
							ENDIF
						ENDIF
					ENDDO
					CURRENT_UCC = SHIPDATA.CARTONID
				ENDIF
			ENDDO
			*current_markfor = shipdata.markfor
			*current_div = shipdata.dept
		ENDDO


		STORE "CTT*"+ALLTRIM(STR(LNCTTCOUNT,6,0))+SEGTERMINATOR TO CSTRING
		FPUTSTRING(T856,CSTRING)
		LCCTR = ALLTRIM(STR(TOTLINCNT))
		LINCNT = 1
		STORE 0 TO LNCTTCOUNT
		LC856CTR = ALLTRIM(STR(GN856CTR))
		LCSEGCTR = ALLTRIM(STR(SEGCNT+1))

		CSTRING ="SE*"+LCSEGCTR+"*"+STNUM+PADL(LC856CTR,4,"0")+SEGTERMINATOR
		FPUTSTRING(T856,CSTRING)

		SUMMARYINFO = ;
			"-----------------------------------------------------------------------------"+CHR(13)+;
			'856 file name....'+"---> "+LC_CNUM+CHR(13)+;
			'   created.......'+DTOC(DATE())+CHR(13)+;
			'   file.....: '+GC856OUTFILE+CHR(13)+;
			'-----------------------------------------------------------------------------'+CHR(13)+;
			'shipment level count.....'+STR(THISSHIPMENTLEVELCOUNT)+CHR(13)+;
			'   order level count.....'+STR(THISORDERLEVELCOUNT)+CHR(13)+;
			'    pack level count.....'+STR(THISPACKLEVELCOUNT)+CHR(13)+;
			'    item level count.....'+STR(THISITEMLEVELCOUNT)+CHR(13)+;
			'-----------------------------------------------------------------------------'+CHR(13)
			
		TrackProgress(SUMMARYINFO)
		
		LC856CTR = ALLTRIM(STR(GN856CTR))
		STORE "GE*"+LC856CTR+"*"+STNUM+SEGTERMINATOR TO CSTRING
		FPUTSTRING(T856,CSTRING)

		STORE "IEA*1*"+LCINTERCTRLNUM+SEGTERMINATOR TO CSTRING
		FPUTSTRING(T856,CSTRING)
		=FCLOSE(T856)
		WAIT WINDOW AT 10,10 " timing........." TIMEOUT 1
		
		* copy output file to upload folder
		IF GLMOVETOAS2 THEN
			COPY FILE (GC856OUTFILE) TO (GCUPLOADFILE)
		ENDIF
		
		IF FILE(GCUPLOADFILE) THEN
			TRACKPROGRESS("Copied upload file " + GCUPLOADFILE)
		ELSE
			TRACKPROGRESS("!! There was an error copying upload file " + GCUPLOADFILE)
		ENDIF
				
	ENDIF &&shipment_level()

	STORE "" TO WORKORDERSPROCESSED
	STORE 0 TO THISSHIPMENTLEVELCOUNT,THISORDERLEVELCOUNT,THISPACKLEVELCOUNT,THISITEMLEVELCOUNT, GN856CTR

	SET CENTURY OFF
	SET DATE TO AMERICAN

	RETURN

	********************************************************************************************************************
	* P A C K     L E V E L
	********************************************************************************************************************
PROC PACK_LEVEL
	HLCTR = HLCTR +1
	PACKLEVELCOUNT = PACKLEVELCOUNT+1
	THISPACKLEVELCOUNT = THISPACKLEVELCOUNT+1
	CURRENTPACKLEVELCOUNT = HLCTR
	LC_HLCOUNT = ALLTRIM(GETNUM(HLCTR))								&& overall HL counter

	*lc_parentcount = Alltrim(getnum(currentitemlevelcount))				&& the parent
	LC_PARENTCOUNT = ALLTRIM(GETNUM(CURRENTORDERLEVELCOUNT))				&& the parent

	STORE "HL*"+LC_HLCOUNT+"*"+LC_PARENTCOUNT+"*"+"P"+SEGTERMINATOR TO CSTRING
	FPUTSTRING(T856,CSTRING)
	****************************************************************************************************************
	SEGCNT=SEGCNT+1
	HLSEGCNT=HLSEGCNT+1

	* count the line items for the CTT segment counter
	LNCTTCOUNT = LNCTTCOUNT +1

	** MAN
	STORE "MAN*GM*"+ALLTRIM(SHIPDATA.CARTONID)+SEGTERMINATOR TO CSTRING
	FPUTSTRING(T856,CSTRING)
	SEGCNT=SEGCNT+1
ENDPROC &&pack_level


********************************************************************************************************************
* I T E M     L E V E L
********************************************************************************************************************
PROC ITEM_LEVEL
	LINCNT = LINCNT + 1
	HLCTR = HLCTR +1
	ITEMLEVELCOUNT = ITEMLEVELCOUNT+1
	THISITEMLEVELCOUNT = THISITEMLEVELCOUNT+1
	CURRENTITEMLEVELCOUNT = HLCTR
	LC_HLCOUNT = ALLTRIM(GETNUM(HLCTR)) 						&& overall HL counter
	*lc_parentcount = Alltrim(getnum(currentorderlevelcount))	&& the parent
	LC_PARENTCOUNT = ALLTRIM(GETNUM(CURRENTPACKLEVELCOUNT))	&& the parent

	STORE "HL*"+LC_HLCOUNT+"*"+LC_PARENTCOUNT+"*I"+SEGTERMINATOR TO CSTRING
	FPUTSTRING(T856,CSTRING)
	SEGCNT=SEGCNT+1
	HLSEGCNT=HLSEGCNT+1

	*Iif(Empty(shipdata.custsku),"VA*"+Alltrim(shipdata.custsku))+"*")
	*Iif(Empty(shipdata.style),"IN*"+Alltrim(shipdata.style))+"*")

	STORE "LIN*"+ALLTRIM(STR(SHIPDATA.QTY))+"*"+"IN*"+ALLTRIM(SHIPDATA.ITEM)+"*"+"IZ*"+ALLTRIM(SHIPDATA.SKU)+SEGTERMINATOR TO CSTRING

	*Store "LIN**"+Iif(Empty(shipdata.custsku),"**","VA*"+Alltrim(shipdata.custsku)+"*")+Iif(Empty(shipdata.style),"**","IN*"+Alltrim(shipdata.style)+"*")+;
	"UP*"+Alltrim(shipdata.upc)+segterminator To cstring
	FPUTSTRING(T856,CSTRING)
	SEGCNT=SEGCNT+1

	STORE "SN1**"+ALLTRIM(STR(SHIPDATA.QTY))+"*ST"+SEGTERMINATOR TO CSTRING
	FPUTSTRING(T856,CSTRING)
	SEGCNT=SEGCNT+1

	STORE "PRF*"+ALLTRIM(SHIPDATA.PONUM)+"*"+SUBSTR(ALLTRIM(SHIPDATA.SHIPDATE),1,8)+SEGTERMINATOR TO CSTRING
	FPUTSTRING(T856,CSTRING)
	SEGCNT=SEGCNT+1


ENDPROC &&item_level

********************************************************************************************************************
* O R D E R    L E V E L
********************************************************************************************************************
PROC ORDER_LEVEL
	PARAMETER LCDELLOC
	LOCAL lnCartonCount, lnTotalWeight, lcMarkfor, lcDEPT

	HLCTR = HLCTR +1
	ORDERLEVELCOUNT = ORDERLEVELCOUNT+1
	THISORDERLEVELCOUNT = THISORDERLEVELCOUNT+1
	LC_HLCOUNT = GETNUM(HLCTR)								&& overall HL counter
	LC_PARENTCOUNT = GETNUM(CURRENTSHIPMENTLEVELCOUNT)				&& the parent
	CURRENTORDERLEVELCOUNT = HLCTR

	STORE "HL*"+ALLTRIM(LC_HLCOUNT)+"*"+ALLTRIM(LC_PARENTCOUNT)+"*O"+SEGTERMINATOR TO CSTRING
	FPUTSTRING(T856,CSTRING)
	*!*	********************************************************************************************************************************
	SEGCNT=SEGCNT+1
	HLSEGCNT=HLSEGCNT+1


	** PRF
	*Store "PRF*"+Alltrim(shipdata.ponum)+segterminator To cstring
	*fputstring(t856,cstring)
	*segcnt=segcnt+1

	STORE "PID*S**VI*FL"+SEGTERMINATOR TO CSTRING
	FPUTSTRING(T856,CSTRING)
	SEGCNT=SEGCNT+1
	
	***********************************************************************************************************************************
	***********************************************************************************************************************************
	* get info for inner TD1 segments -- Renay told Doug that this is info rolled up at the Store (markfor) level, NOT the PONUM level!
	* However, I needed to add DEPT to the filter to have the numbers add up correctly - 3/11/11 MB
	IF USED('CURTD1CTNS') THEN
		USE IN CURTD1CTNS
	ENDIF
	IF USED('CURTD1SHIPWT') THEN
		USE IN CURTD1SHIPWT
	ENDIF
	* save current Markfor
	lcMARKFOR = SHIPDATA.MARKFOR
	* save current Dept
	lcDEPT = SHIPDATA.DEPT
	
	SELECT DISTINCT CARTONID, SHIPWT ;
		FROM SHIPDATA ;
		INTO CURSOR CURTD1CTNS ;
		WHERE SHIPDATA.BOL = BOLINDEX.BOL ;
		AND SHIPDATA.MARKFOR = lcMARKFOR ;
		AND SHIPDATA.DEPT = lcDEPT
		
	lnCartonCount = RECCOUNT('CURTD1CTNS')
	* accumulate so we can compare against the total file #s...
	gnCtnCntAccum = gnCtnCntAccum + lnCartonCount
	
	SELECT SUM(SHIPWT) AS TOTWT ;
		FROM CURTD1CTNS ;
		INTO CURSOR CURTD1SHIPWT
		
	lnTotalWeight = CURTD1SHIPWT.TOTWT
	* accumulate so we can compare against the total file #s...
	gnShipWtAccum = gnShipWtAccum + lnTotalWeight
	***********************************************************************************************************************************
	***********************************************************************************************************************************
	

	*STORE "TD1**"+ALLTRIM(STR(SHIPDATA.CTNQTY))+"****G*"+ALLTRIM(STR(SHIPDATA.SHIPWT))+"*LB"+SEGTERMINATOR TO CSTRING
	STORE "TD1**"+ALLTRIM(STR(lnCartonCount))+"****G*"+ALLTRIM(STR(lnTotalWeight))+"*LB"+SEGTERMINATOR TO CSTRING
	FPUTSTRING(T856,CSTRING)
	SEGCNT=SEGCNT+1

	STORE "REF*DP*"+ALLTRIM(SHIPDATA.DEPT)+SEGTERMINATOR TO CSTRING
	FPUTSTRING(T856,CSTRING)
	SEGCNT=SEGCNT+1

	STORE "REF*IA*"+ALLTRIM(SHIPDATA.VENDORID)+SEGTERMINATOR TO CSTRING
	FPUTSTRING(T856,CSTRING)
	SEGCNT=SEGCNT+1

	STORE "N1*BY**92*"+ALLTRIM(SHIPDATA.BILLTO)+SEGTERMINATOR TO CSTRING
	FPUTSTRING(T856,CSTRING)
	SEGCNT=SEGCNT+1

	STORE "N1*Z7**92*"+ALLTRIM(SHIPDATA.MARKFOR)+SEGTERMINATOR TO CSTRING
	FPUTSTRING(T856,CSTRING)
	SEGCNT=SEGCNT+1

ENDPROC &&order_level

********************************************************************************************************************
* S H I P M E N T     L E V E L
********************************************************************************************************************
PROC SHIPMENT_LEVEL
	* reset all at each shipment level
	STORE 0 TO SHIPMENTLEVELCOUNT
	STORE 0 TO ORDERLEVELCOUNT
	STORE 0 TO PACKLEVELCOUNT
	STORE 0 TO ITEMLEVELCOUNT
	STORE 0 TO HLCTR

	SHIPMENTLEVELCOUNT = SHIPMENTLEVELCOUNT +1
	THISSHIPMENTLEVELCOUNT = THISSHIPMENTLEVELCOUNT +1
	HLCTR = HLCTR +1
	CURRENTSHIPMENTLEVELCOUNT = HLCTR
	**currentshipmentlevelcount = shipmentlevelcount
	LC_HLCOUNT = ALLTRIM(GETNUM(HLCTR))
	STORE "HL*"+LC_HLCOUNT+"**S"+SEGTERMINATOR TO CSTRING
	FPUTSTRING(T856,CSTRING)
	********************************************************************************************************************
	SEGCNT=SEGCNT+1
	HLSEGCNT=HLSEGCNT+1

	**TD1 NEED TOTAL CTNS/WEIGHT/CUBE FOR TRIP
	*STORE "TD1**"+ALLTRIM(STR(SHIPDATA.CTNQTY))+"****G*"+ALLTRIM(STR(SHIPDATA.SHIPWT))+"*LB"+SEGTERMINATOR TO CSTRING
	STORE "TD1**"+ALLTRIM(STR(gnCartonCount))+"****G*"+ALLTRIM(STR(gnTotalWeight))+"*LB"+SEGTERMINATOR TO CSTRING
	FPUTSTRING(T856,CSTRING)
	SEGCNT=SEGCNT+1

	** TD5
	&&&
	STORE "TD5**2*FMIX"+SEGTERMINATOR TO CSTRING
	FPUTSTRING(T856,CSTRING)
	SEGCNT=SEGCNT+1

	** TD3
	&&&
	STORE "REF*BM*"+ALLTRIM(SHIPDATA.BOL)+SEGTERMINATOR   TO CSTRING  && FMI TRIP NO
	FPUTSTRING(T856,CSTRING)
	SEGCNT=SEGCNT+1

	STORE "DTM*011*"+SUBSTR(ALLTRIM(SHIPDATA.SHIPDATE),1,8)+SEGTERMINATOR   TO CSTRING  && hardcoded as per direction from lognet
	FPUTSTRING(T856,CSTRING)
	SEGCNT=SEGCNT+1

	STORE "N1*ST**92*"+ALLTRIM(SHIPDATA.SHIPTODC)+SEGTERMINATOR TO CSTRING && hardcoded as per direction from lognet
	FPUTSTRING(T856,CSTRING)
	SEGCNT=SEGCNT+1

	STORE "N1*SF*FMI"+SEGTERMINATOR TO CSTRING && hardcoded as per direction from lognet
	FPUTSTRING(T856,CSTRING)
	SEGCNT=SEGCNT+1

	**********************************************************************************************
	* hardcoded as per direction from Sears to add PER segment 6/1/2011 MB. Renay says this format is correct.
	* new segments:
	
	*N1*VN*FMI*ZZ*FMI
	*PER*CN**TE**FX**EM*SEARSEDI@FMIINT.COM

	STORE "N1*VN*FMI*ZZ*FMI"+SEGTERMINATOR TO CSTRING  
	FPUTSTRING(T856,CSTRING)
	SEGCNT=SEGCNT+1

	*STORE "PER*CN**TE**FX**EM*SEARSEDI@FMIINT.COM"+SEGTERMINATOR TO CSTRING 
	STORE "PER*CN*ITDEPT*TE*7327509000*FX*7327504338*EM*KMSHCEDI@FMIINT.COM"+SEGTERMINATOR TO CSTRING 
	FPUTSTRING(T856,CSTRING)
	SEGCNT=SEGCNT+1
	**********************************************************************************************


	RETURN
ENDPROC &&shipment_level

********************************************************************************************************
PROCEDURE SHIPMENTHEADER
	PARAMETERS TSHIPID

	GN856CTR = GN856CTR +1
	STORE "ST*856*"+STNUM+PADL(ALLTRIM(STR(GN856CTR)),4,"0")+SEGTERMINATOR TO CSTRING
	FPUTSTRING(T856,CSTRING)

	SELECT CONTROLNUM
	*LC_CNUM = PADL(ALLTRIM(STR(CONTROLNUM.BSNSEG)),5,"0")
	REPLACE CONTROLNUM.BSNSEG WITH (CONTROLNUM.BSNSEG + 1) IN CONTROLNUM

	LC_CNUM = PADL(ALLTRIM(STR(CONTROLNUM.BSNSEG)),5,"0")
	
	GCBSNSEGLIST = GCBSNSEGLIST + " " + LC_CNUM

	TSHIPID = PADR('FMILA'+SUBSTR(LC_CNUM,1,25),30)

	STORE "BSN*00*"+ALLTRIM(TSHIPID)+"*"+D4+D1+D2+D3+"*"+J1+J2+"*0001*OR"+SEGTERMINATOR TO CSTRING
	FPUTSTRING(T856,CSTRING)

	TOTLINCNT = 0
ENDPROC &&shipmentheader


********************************************************************************************************
PROCEDURE WRITEHEADER
	* get the next unique control number
	SELECT CONTROLNUM
	*STNUM = ALLTRIM(STR(CONTROLNUM.CNTRL856))
	*LC_856NUM = STNUM
	REPLACE CONTROLNUM.CNTRL856 WITH (CONTROLNUM.CNTRL856 + 1) IN CONTROLNUM

	STNUM = ALLTRIM(STR(CONTROLNUM.CNTRL856))
	LC_856NUM = STNUM
	
	GCCNTRL856LIST = GCCNTRL856LIST + " " + LC_856NUM

	
	*LNINTERCONTROLNUM = CONTROLNUM.INTERCTRL
	REPLACE CONTROLNUM.INTERCTRL WITH (CONTROLNUM.INTERCTRL + 1) IN CONTROLNUM

	LNINTERCONTROLNUM = CONTROLNUM.INTERCTRL
	LCINTERCTRLNUM = PADL(ALLTRIM(STR(LNINTERCONTROLNUM)),9,"0")

	GCINTERCTRLLIST = GCINTERCTRLLIST + " " + LCINTERCTRLNUM

	*  write out the 214 header

	*Store "ISA*00*          *00*          *ZZ*FMIF           *12*"+tqualcode+"*"+tyr+tmon+tday+"*"+lcTime+"*U*00401*"+lcInterctrlNum +"*0*P*;"+">"+segterminator to cstring
	STORE "ISA*00*          *00*          *ZZ*FMI            *12*"+TQUALCODE+"*"+TYR+TMON+TDAY+"*"+LCTIME+"*U*00401*"+LCINTERCTRLNUM +"*0*P*;"+SEGTERMINATOR TO CSTRING
	IF GLTESTINDICATOR = .T.
		STRTRAN(CSTRING,"*P*","*T*")
	ENDIF

	FPUTSTRING(T856,CSTRING)

	STORE "GS*SH*"+ALLTRIM(TSENDID)+"*"+ALLTRIM(TSENDCODE)+"*"+TWHOLEYR+TMON+TDAY+"*"+LCTIME+"*"+STNUM+"*X*004010"+SEGTERMINATOR TO CSTRING
	FPUTSTRING(T856,CSTRING)
ENDPROC


********************************************************************************************************
FUNCTION GETNUM
	PARAM PLCOUNT
	FIDNUM = IIF(BETWEEN(PLCOUNT,1,9),SPACE(11)+LTRIM(STR(PLCOUNT)),;
		IIF(BETWEEN(PLCOUNT,10,99),SPACE(10)+LTRIM(STR(PLCOUNT)),;
		IIF(BETWEEN(PLCOUNT,100,999),SPACE(9)+LTRIM(STR(PLCOUNT)),;
		IIF(BETWEEN(PLCOUNT,1000,9999),SPACE(8)+LTRIM(STR(PLCOUNT)),;
		IIF(BETWEEN(PLCOUNT,10000,99999),SPACE(7)+LTRIM(STR(PLCOUNT))," ")))))
	RETURN FIDNUM
ENDFUNC

********************************************************************************************************
PROC FPUTSTRING
	PARAM FILEHANDLE,FILEDATA
	FWRITE(FILEHANDLE,FILEDATA)
ENDPROC


********************************************************************************************************
PROC CHECKEMPTY
	PARAMETER LCDATAVALUE
	RETURN IIF(EMPTY(LCDATAVALUE),"UNKNOWN",LCDATAVALUE)
ENDPROC


********************************************************************************************************
PROC REVERTTABLES
	TABLEREVERT(.T.,"controlnum")
	TABLEREVERT(.T.,"asnwo")
	TABLEREVERT(.T.,"a856info")
	TABLEREVERT(.T.,"bkdnWO")
ENDPROC
********************************************************************************************************

PROCEDURE TRACKPROGRESS
	LPARAMETERS TCEXPRESSION
	IF LEN(TCEXPRESSION) <= 100 THEN
		WAIT WINDOW NOWAIT TCEXPRESSION
	ENDIF
	GCBODYTEXT = GCBODYTEXT + TCEXPRESSION + CRLF + CRLF
ENDPROC
