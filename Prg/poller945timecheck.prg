CLOSE DATABASES ALL
CLEAR
DO m:\dev\prg\_setvars
* ASSERT .F.
PUBLIC lError,cName,tsendto,tcc

utilsetup("POLLER945TIMECHECK")

_screen.WindowState=1
SELECT 0
USE F:\3pl\DATA\mailmaster alias mm
LOCATE FOR accountid = 9999 AND office = "X"
tsendto = IIF(mm.use_alt,mm.sendtoalt,mm.sendto)
tcc = IIF(mm.use_alt,mm.ccalt,mm.cc)
USE IN mm

USE F:\3pl\DATA\last945time
lError = .F.
cName = ""
dDT = DATETIME()
dDTBack = (DATETIME()-(30*60)) && 30-minute lag
cPollerDown = ""


FOR j = 1 TO 7
	DO CASE
		CASE j = 1
			cName = "MORET"
		CASE j = 2
			cName = "MISC214"
		CASE j = 3
			cName = "MAIN"
		CASE j = 4
			cName = "NANJING"
		CASE j = 5
			cName = "IN"
		CASE j = 6
			cName = "MJ"
		CASE j = 7
			cName = "MERKURY"
	ENDCASE

	LOCATE FOR POLLER = cName
	IF last945time.checktime < dDTBack AND checkflag = .T.
		cPollerDown = IIF(EMPTY(cPollerDown),cName,cPollerDown+CHR(13)+cName)
		IF last945time.tries = 3
			REPLACE last945time.checkflag WITH .F.
		ELSE
			REPLACE last945time.tries WITH (last945time.tries+1)
		ENDIF
		lError = .T.
	ENDIF
ENDFOR

IF lError
	tfrom = "TGF Warehouse Operations <warehouse-ops@fmiint.com>"
	tsubject = "NOTICE: Server TASK2, POLLER(S) DOWN"
	tattach = " "
	tmessage = "The EDI Poller(s) below are halted outside normal error-handling."
	tmessage = tmessage + CHR(13) + "Check and restart ASAP."
	tmessage = tmessage + CHR(13) + CHR(13) +cPollerDown
	DO FORM m:\dev\frm\dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
endif

schedupdate()

CLOSE DATABASES ALL
RETURN
