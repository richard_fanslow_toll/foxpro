PARAMETERS nAcctNum,cCustName,cPPName,lUCC,cOffice,lSQL3,lMoret,lDoCartons,lDoScanpack
IF VARTYPE(cOffice) # "C"
	cOffice = "C"
ENDIF
csql = "SQL4"
cSQLPass = ""
*IF lTesting
*SET STEP ON
*ENDIF
SELECT 0
USE F:\wh\sqlpassword
cSQLPass = ALLTRIM(sqlpassword.PASSWORD)
USE IN sqlpassword

SET ESCAPE ON
ON ESCAPE CANCEL
cFileOutName = "V"+TRIM(cPPName)+"pp"
nAcct = ALLTRIM(STR(nAcctNum))
IF USED("tempbc")
	USE IN tempbc
ENDIF

lcDSNLess="driver=SQL Server;server=tgfnjsql01;uid=SA;pwd=&cSQLPass;DATABASE=PICKPACK"

nHandle=SQLSTRINGCONNECT(m.lcDSNLess,.T.)
SQLSETPROP(0,'DispLogin',3)
SQLSETPROP(0,"dispwarnings",.F.)
WAIT WINDOW "Now processing "+cCustName+" SQL view...please wait" NOWAIT
IF nHandle<1 && bailout
	SET STEP ON
	WAIT WINDOW "Could not make SQL connection" TIMEOUT 3
	cRetMsg = "NO SQL CONNECT"
	RETURN cRetMsg
	THROW
ENDIF

lAppend = .F.

SELECT sqlwobcny
LOCATE

*!* Scans through all PTs within the OUTSHIP BOL#
SCAN
	nWO_num = sqlwobcny.wo_num
	nWo   = ALLTRIM(STR(nWO_Num))

	IF lTestinput
		WAIT WINDOW "SQL Records will be selected from LABELS" TIMEOUT 2
		lcQ1=[SELECT * FROM dbo.labels Labels]
		lcQ2 = [ WHERE Labels.wo_num = ]
		lcQ3 = " &nWO "
		IF lUCC
			lcQ6 = [order by Labels.ship_ref,Labels.outshipid,Labels.ucc]
		ELSE
			lcQ6 = [order by Labels.ship_ref,Labels.outshipid,Labels.CartonNum]
		ENDIF
		lcsql = lcQ1+lcQ2+lcQ3+lcQ6

	ELSE
		if usesql()
			if lucc
				xorderby="order by ship_ref, outshipid, ucc"
			else
				xorderby="order by ship_ref, outshipid, cartonnum"
			endif
			
			if xsqlexec("select * from cartons where wo_num="+nwo+" and ucc#'CUTS' "+xorderby,cFileOutName,,"pickpack") = 0
				ASSERT .F. MESSAGE "At NO RECORDS statement in SQLConnect"
				WAIT WINDOW "No "+cCustName+" records found" TIMEOUT 2
				cRetMsg = "NO SQL CONNECT"
				RETURN cRetMsg
			endif
		else
			lcQ1 = [SELECT * FROM dbo.cartons Cartons]
			lcQ2 = [ WHERE Cartons.wo_num = ]
			lcQ3 = " &nWO "
			lcQ4 = [ AND  Cartons.ucc <> ]
			lcQ5 = " 'CUTS' "
			IF lUCC
				lcQ6 = [order by Cartons.ship_ref,Cartons.outshipid,Cartons.ucc]
			ELSE
				lcQ6 = [order by Cartons.ship_ref,Cartons.outshipid,Cartons.CartonNum]
			ENDIF

			lcsql = lcQ1+lcQ2+lcQ3+lcQ4+lcQ5+lcQ6
		endif
	ENDIF

	if usesql()
	else
		llSuccess=SQLEXEC(nHandle,lcsql,cFileOutName)

		IF llSuccess<1  && no records -1 indicates no records and 1 indicates records found
			ASSERT .F. MESSAGE "At NO RECORDS statement in SQLConnect"
			WAIT WINDOW "No "+cCustName+" records found" TIMEOUT 2
			cRetMsg = "NO SQL CONNECT"
			RETURN cRetMsg
		ENDIF
		SQLCANCEL(nHandle)
	endif
	
	SELECT &cFileOutName
	IF lAppend = .F.
		lAppend = .T.
		IF lTesting
			LOCATE
			BROWSE
		ENDIF
		COPY TO ("F:\3pl\DATA\tempbc")
		USE ("F:\3pl\DATA\tempbc") IN 0 ALIAS tempbc
	ELSE
		LOCATE
		SCAN
			SCATTER MEMVAR
			INSERT INTO tempbc FROM MEMVAR
		ENDSCAN
	ENDIF
ENDSCAN
IF USED(cFileOutName)
	USE IN &cFileOutName
ENDIF

SELECT tempbc
LOCATE
IF nAcctNum = 6521
	IF lTestSplits OR cShip_ref = '2154668-A ~U'
		REPLACE tempbc.ship_ref WITH STRTRAN(tempbc.ship_ref,"~","%") ALL IN tempbc
	ENDIF
ENDIF

IF lUCC
	SELECT * FROM tempbc ;
		ORDER BY ship_ref,outshipid,ucc ;
		INTO CURSOR &cFileOutName READWRITE
ELSE
	SELECT * FROM tempbc ;
		ORDER BY ship_ref,outshipid,cartonnum ;
		INTO CURSOR &cFileOutName READWRITE
ENDIF
IF lTesting
	SELECT &cFileOutName
*BROWSE
ENDIF

USE IN tempbc
USE IN sqlwobcny
GO BOTT
WAIT CLEAR
*WAIT "" TIMEOUT 3
SQLDISCONNECT(nHandle)

cRetMsg = "OK"
RETURN cRetMsg
