* populate the inspects.dbf table from WO's where maintype = 'I' per Andrea.
LOCAL lnAnswer, lcNewDataDir, lcNewDatabase, lcInspectDBF

RETURN

lnAnswer = MESSAGEBOX("This import process will import inspections from the Work Orders! Continue?",4+48+256)
IF lnAnswer <> 6 THEN
	RETURN
ENDIF

CLOSE ALL
SET CONSOLE OFF
SET STATUS BAR OFF
SET TALK OFF
SET SAFETY OFF

*lcNewDataDir = "M:\DEV\SHDATA\"
lcNewDataDir = "F:\SHOP\SHDATA\"

lcNewDatabase = lcNewDataDir + "SH"

OPEN DATABASE (lcNewDatabase)

lcInspectDBF = lcNewDataDir + "INSPECTS"

USE SH!ORDHDR IN 0 ALIAS ORDHDR

SELECT ORDHDR
SCAN FOR MAINTTYPE = 'I' AND CRDATE > {^2002-12-31}
	WAIT WINDOW "Creating Inspection for WO # " + TRANSFORM(ORDHDR.ORDNO) NOWAIT
	m.equip_no = ORDHDR.VEHNO
	m.insp_date = ORDHDR.CRDATE
	INSERT INTO (lcInspectDBF) FROM MEMVAR
ENDSCAN

USE IN ORDHDR

WAIT CLEAR
CLOSE ALL
RETURN
