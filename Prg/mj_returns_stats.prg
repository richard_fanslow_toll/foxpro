utilsetup("MJ_RETURNS_STATS")
Set Exclusive Off
Set Deleted On
Set Talk Off
Set Safety Off
With _Screen
	.AutoCenter = .T.
	.WindowState = 0
	.BorderStyle = 1
	.Width = 320
	.Height = 210
	.Top = 290
	.Left = 110
	.Closable = .T.
	.MaxButton = .T.
	.MinButton = .T.
	.ScrollBars = 0
	.Caption = "MJ_RETURNS_STATS"
Endwith
GOFFICE='J'

xsqlexec("select * from cmtrans",,,"wh")
index on trolly tag trolly
set order to

goffice='J'
useca("inwolog","wh",,,"mod='"+goffice+"' and  accountid in (6303,6325,6543) and confirmdt>{"+dtoc(date()-90)+"}")

Select Sum(plunitsinqty) As confirmed_qty , Ttod(confirmdt) as confirmdt From inwolog Where Ttod(confirmdt)>=  Date()-10   And Container='PL'  GROUP BY confirmdt Into Cursor t1a Readwrite
SELECT SUM(confirmed_qty) as confirmed_qty , confirmdt FROM t1a GROUP BY confirmdt INTO CURSOR t1 READWRITE
If Reccount()=0
	Append Blank
	Replace confirmed_qty With 0 For Empty(confirmed_qty)
Endif
Replace confirmed_qty With 0 For Isnull(confirmed_qty)

Select  Sum(qty)As scanned_qty , Ttod(scantime) as scandt From cmtrans Where Ttod(scantime) >= Date()-10  And trolly='PL'  GROUP BY 2  Into Cursor t2 Readwrite
If Reccount()=0
	Append Blank
	Replace scanned_qty With 0 For Empty(scanned_qty)
Endif
Replace scanned_qty With 0 For Isnull(scanned_qty)

xsqlexec("select * from adj where mod='J' and totqty<0 and whseloc='PL' and adjdt >={"+dtoc(Date()-10)+"} and accountid#6325",,,"wh")
Select Sum(totqty)*-1 As qty1,adjdt as date From ADJ GROUP BY adjdt  Into Cursor t3 Readwrite

If Reccount()=0
	Append Blank
	Replace qty1 With 0 For Empty(qty1)
Endif
Replace qty1 With 0 For Isnull(qty1)
SET STEP ON 
Select Sum(plunitsinqty) As qty2 ,  tTOD(confirmdt) as date From inwolog Where Ttod(confirmdt) >=  Date()-10  And Container='PL'  And accountid=6325 GROUP BY confirmdt  Into Cursor t4A Readwrite
SELECT SUM(QTY2) as qty2 , date FROM t4a GROUP BY date INTO CURSOR t4 READWRITE
If Reccount()=0
	Append Blank
	Replace qty2 With 0 For Empty(qty2)
Endif
Replace qty2 With 0 For Isnull(qty2)
SELECT  qty1,qty2, t3.date, t4.date From t3 FULL join t4 ON t3.date=t4.date Into Cursor t5 Readwrite
REPLACE qty1 WITH 0 FOR ISNULL(qty1)
REPLACE qty2 WITH 0 FOR ISNULL(qty2)
REPLACE date_a WITH date_b FOR ISNULL(date_a)
REPLACE date_b WITH date_a FOR ISNULL(date_b)
Select qty1+qty2 As putaway_qty, date_a as date From t5 Into Cursor c1 Readwrite


SELECT * FROM t1 FULL JOIN t2 ON confirmdt=scandt INTO CURSOR c2 READWRITE 
REPLACE confirmed_qty WITH 0 FOR ISNULL(confirmed_qty)
REPLACE scanned_qty WITH 0 FOR ISNULL(scanned_qty)
REPLACE confirmdt WITH scandt FOR ISNULL(confirmdt)
REPLACE scandt WITH confirmdt FOR ISNULL(scandt)


SELECT * FROM c1 FULL JOIN c2 ON confirmdt=date INTO CURSOR c3 READWRITE 
REPLACE putaway_qty WITH 0 FOR ISNULL(putaway_qty)
REPLACE confirmed_qty WITH 0 FOR ISNULL(confirmed_qty)
REPLACE scanned_qty WITH 0 FOR ISNULL(scanned_qty)

REPLACE date WITH confirmdt FOR ISNULL(date)
REPLACE confirmdt WITH date FOR ISNULL(confirmdt)
REPLACE scandt WITH date FOR ISNULL(scandt)
SELECT scanned_qty,confirmed_qty, putaway_qty, date FROM c3 INTO CURSOR c3 ORDER BY date
SET STEP ON

*!*	IF RECCOUNT()=0
*!*	APPEND blank
*!*	REPLACE confirmed_qty WITH 0 FOR empty(confirmed_qty)
*!*	ENDIF
*!*	REPLACE confirmed_qty WITH 0 FOR ISNULL(confirmed_qty)

*!*	SELECT SUM(totqty)*-1 as putaway FROM adj WHERE adjdt= Date()-2 and whseloc='CART' AND TOTQTY<0 INTO CURSOR T3 READWRITE
*!*	IF RECCOUNT()=0
*!*	APPEND blank
*!*	REPLACE putaway WITH 0 FOR empty(putaway)
*!*	endif
*!*	REPLACE putaway WITH 0 FOR ISNULL(putaway)


*!*	Select *, scanned_qty+putaway_qty As total_return_units From t7 Into Cursor t8
*!*	If Reccount()=0
*!*		Append Blank
*!*		Replace total_return_units With 0 For Empty(total_return_units)
*!*	Endif
*!*	Replace  total_return_units With 0 For Isnull( total_return_units)
*!*	Set Step On

*!*	Select t8

If Reccount() > 0
	Export To "S:\MarcJacobsData\TEMP\return_stats"  Type Xls
	tsendto = "tmarg@fmiint.com,jim.killen@tollgroup.com"
	tattach = "S:\MarcJacobsData\TEMP\return_stats.xls"
	tcc =""
	tFrom ="Toll EDI System Operations <fmi-transload-ops@fmiint.com>"
	tmessage = "RETURN STATS FOR PREVIOUS LAST 10 DAYS:  " 
*		tmessage = "RETURN STATS FOR:  " +TTOC (Date()-1, 0)
	tSubject = "RETURN STATS COMPLETE FOR PREVIOUS LAST 10 DAYS"
	Do Form m:\dev\frm\dartmail2 With tsendto,tFrom,tSubject, tcc,tattach,tmessage  ,"A"
endif

Close Data All
schedupdate()
_Screen.Caption=gscreencaption
