**Time from container availibilty to container delivery
**  - Full ocean containers Only
**  - Times based on availLog.avail Vs. wolog.pickedUp
**
**Comments/Issues:
**  - if no availLog record found with a valid "avail" date, wo_date used in its stead
**  - if empty(pickedUp), wo_date used in its stead
Wait window "Gathering data for Availability To Delivered Spreadsheet..." nowait noclear

*xstartdt={1/1/18}
*xenddt={1/20/18}
*xaccountid=6638
*goffice="N"

dstartdt=xstartdt
denddt=xenddt

use f:\wo\wodata\wolog in 0

if empty(xaccountid)
	xacctfilter=".t."
else
	xacctfilter="accountid="+transform(xaccountid)
endif

xsqlexec("select wo_num, called, avail, remarks from availlog where "+xacctfilter+" and office='"+goffice+"'",,,"wo")
index on wo_num tag wo_num
set order to

Select w.*, w.accountid as acctNumFld, acctname as acctNameFld, Max(called) as lastCall, ;
	   called, Iif((emptynul(avail) and emptynul(called)), wo_date, Iif(Emptynul(avail), called, avail)) as avail ;
	from wolog w ;
		left join availlog a on w.wo_num = a.wo_num ;
	group by w.wo_num, a.called, a.avail, a.remarks ;
	order by w.acctname, w.wo_date ;
	where &xacctfilter and Between(w.delivered, xstartdt, xenddt) and w.office = goffice and type # "D" ;
  into cursor csrdetail

**availType: 0=avail, 1=wo_date, 2=last call date
Select c.*, Iif(IsNull(avail), 1, Iif(Empty(avail), 2, 0)) as availType, 00 as DateDiff ;
	from csrdetail c ;
		left join account a on a.accountid = c.accountid ;
	group by c.wo_num order by c.acctNameFld, c.wo_date ;
	where (IsNull(called) or called = lastCall) ;
  into cursor csrDetail readWrite

**calculate date differential into dateDiff - make necessary adjustments for weekends & FMI holidays
DO kpicalcDiff with "avail", "delivered", "dateDiff"

Locate
If Eof()
	strMsg = "No data found."
	=MessageBox(strMsg, 48, "Availability To Delivered")
Else
	Wait window "Creating Availability To Delivered Spreadsheet..." nowait noclear

	public oexcel
	oexcel = createobject("excel.application")

	oWorkbook=""
	DO xlsTimeGeneric with "", "delivered", "1", "", "Availability To Delivered", "Delivered Date Vs. Availablilty", ;
		" - Date range entered reflects container delivered dates.", "", "''", .t., .t.

	If lDetail
		Select csrTotals
		nWorksheet = 2
		Scan
			store tempfox+substr(sys(2015), 3, 10)+".xls" to ctmpfile
			Select *, Iif(type="A", awb, container) as field1 from csrDetail ;
				where Year(delivered) = csrTotals.year and Month(delivered) = csrTotals.month into cursor csrDet
			Copy to &cTmpFile fields acctNameFld, wo_num, type, field1, wo_date, avail, delivered, dateDiff type xl5

			DO xlsTimeGenDetail with cTmpFile, nWorkSheet, "H"

			With oWorkbook.worksheets[nWorksheet]
			  Select csrDet
			  Scan
				cRow = Alltrim(Str(Recno()+1))
				.Range("A"+cRow+":"+"G"+cRow).Interior.Color = Iif(Emptynul(delivered), RGB(255,255,128), Rgb(255,255,255))
				.Range("E"+cRow).Interior.Color = Iif(availType=0, .Range("E"+cRow).Interior.Color, ;
					Iif(availType=1, Rgb(255,0,0), Rgb(0,0,255)))
			  EndScan
			
			  .Range("A"+Alltrim(Str(Reccount("csrDet")+5))+":A"+Alltrim(Str(Reccount("csrDet")+5))).font.color = Rgb(255,0,0) &&red
			  .Range("A"+Alltrim(Str(Reccount("csrDet")+5))).Value = "Notes:"
			  .Range("A"+Alltrim(Str(Reccount("csrDet")+6))).Value = " - All available dates highlighted RED are actually w/o dates."
			  .Range("A"+Alltrim(Str(Reccount("csrDet")+7))).Value = " - All available dates highlighted BLUE are actually last call dates."
			EndWith

			nWorksheet = nWorksheet+1
			use in csrdet
		EndScan
	EndIf

	oWorkbook.Worksheets[1].range("A1").activate()
	oWorkbook.Save()
	oExcel.visible=.t.

	RELEASE oWorkbook, oExcel
EndIf


USE in wolog
USE in availLog
USE in csrDetail
If Used("csrTotals")
  USE in csrTotals
EndIf
