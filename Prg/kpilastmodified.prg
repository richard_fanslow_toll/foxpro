**kpiLastModified.prg
**
**
DO set_envi

Create Cursor csrDir (fileName c(15), fileSize n(20,0), modifyDt d(1), modifyTm c(20), modifyDtTm t(1))
=ADir(aKpiDir, "f:\kpi\kpi*.exe")
Insert into csrDir from array aKpiDir
Replace all modifyDtTm with Ctot(Dtoc(modifyDt)+" "+Alltrim(modifyTm))
Delete For Upper(fileName) = "KPI.EXE"
Delete For Upper(fileName) = "KPIREPORTS.EXE"

Select * from csrDir order by modifyDtTm desc into cursor csrLastModified
cFile = "f:\kpi\"+csrLastModified.fileName

Try
  Run /N &cFile

Catch
  For i = 1 to 99
	cNewFile = "f:\kpi\kpi"+Alltrim(Str(i))+".exe"
	If !File(cNewFile)
	  Exit
	EndIf
  EndFor

  Copy File &cFile to &cNewFile
  Run F:\MAIN\touch.exe &cNewFile
  Run /N &cNewFile
EndTry

Return
