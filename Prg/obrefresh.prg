lparameters xform

whdorefresh(xform,"OBBL")
whdorefresh(xform,"OBIN")
whdorefresh(xform,"OBOUT")


****
procedure whdorefresh

lparameters xform, xcheckform

if xform#xcheckform
	xdy = ascan(oforms.iaforminstances,xcheckform,1)
	if xdy#0
		xdy = asubscript(oforms.iaforminstances,xdy,1)
		osource = oforms.iaforminstances[xdy,1]
		osource.zzgetname()
	endif
endif
