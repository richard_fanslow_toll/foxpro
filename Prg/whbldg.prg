lparameters xoffice, xdyaccountid, xnoemail, xnomessage

=seek(xdyaccountid,"account","accountid")

do case
case xoffice="N"
	xbuilding=account.njmod
	
	do case
	case xbuilding="I"	&& Mod I
		xmod="I"	
	case xbuilding="J"	&& Mod J
		xmod="J"
	otherwise
		set step on 
		xmod="I"
		messagebox(acctname(xdyaccountid)+" is not in the Carteret warehouse","")
*		if !xnoemail
*			do ("email") with "dyoung@fmiint.com","WHBLDG: Account "+transform(xdyaccountid)+" has an incorrect building",xbuilding,,,,.t.,,,,,.t.,,.t.
*		endif
	endcase

case xoffice="C"
	xbuilding=account.building
	
	do case
	case xbuilding="4"	&& Mod A
		xmod="1"
	case inlist(xbuilding,"7","8")	&& Mod D
		xmod="2"
	case xbuilding="5"	&& Mod B
		xmod="6"
	case xbuilding="6"	&& Mod C
		xmod="5"
	case xbuilding="0"	&& Mod E
		xmod="7"
	case xbuilding="1"	&& Mod F
		xmod="8"
	case xbuilding="L"	&& Mira Loma
		xmod="L"
	otherwise
		if !xnomessage
			set step on
			xmod="1"
			messagebox(acctname(xdyaccountid)+" is not in the San Pedro warehouse","")
		endif
*		if !xnoemail
*			do ("email") with "dyoung@fmiint.com","WHBLDG: Account "+transform(xdyaccountid)+" has an incorrect building",xbuilding,,,,.t.,,,,,.t.,,.t.
*		endif
	endcase

case xoffice="K"
	xbuilding=account.kymod
	
	do case
	case xbuilding="K"	&& Ariat
		xmod="K"	
	case xbuilding="S"	&& Mod S	&& steelxyz
		xmod="S"
	otherwise
		set step on 
		xmod="K"
		messagebox(acctname(xdyaccountid)+" is not in the Louisville warehouse","")
*		if !xnoemail
*			do ("email") with "dyoung@fmiint.com","WHBLDG: Account "+transform(xdyaccountid)+" has an incorrect building",xbuilding,,,,.t.,,,,,.t.,,.t.
*		endif
	endcase
endcase

**added because receiving an error that xmod doesnt exist if send any office other than C, N, or K - mvw 02/27/17
if type('xmod')="U" and type("xoffice")="C"
	xmod=xoffice
endif

return xmod
