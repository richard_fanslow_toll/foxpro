*!*	COST TRENDING FOR MIKE DIVIRGILIO ET AL.  FOR DIVS 02, 03 & 55

* run Friday mornings.
* build exe as F:\UTIL\SHOPREPORTS\SHCOSTTRENDING.EXE

LOCAL lTestMode
lTestMode = .T.

IF NOT lTestMode THEN
	utilsetup("SHCOSTTRENDING")
ENDIF

DECLARE INTEGER CreateMutex IN Win32API ;
	STRING @lpMutexAttributes, ;
	STRING bInitialOwner, ;
	STRING @lpName

DECLARE INTEGER GetLastError IN Win32API

* if running in EXE check to see if already running
IF _VFP.STARTMODE > 0 THEN
	lcProcessName = "SHCOSTTRENDING"
	CreateMutex(0,CHR(1),@lcProcessName)
	lnError = GetLastError()
	IF lnError = 183
		WAIT WINDOW TIMEOUT 5 "This program is already running..."
		IF NOT lTestMode THEN
			schedupdate()
		ENDIF
		RETURN .F.
	ENDIF
ENDIF

loSHCOSTTRENDING = CREATEOBJECT('SHCOSTTRENDING')
loSHCOSTTRENDING.MAIN( lTestMode )

IF NOT lTestMode THEN
	schedupdate()
ENDIF

CLOSE DATABASES ALL
RETURN

#DEFINE LOGIT 1
#DEFINE WAITIT 2
#DEFINE NOWAITIT 4
#DEFINE SENDIT 8
#DEFINE CRLF CHR(13) + CHR(10)
#DEFINE RETURN_DATA_MANDATORY .F.
#DEFINE RETURN_DATA_NOT_MANDATORY .T.
#DEFINE CSVFORMAT 6
#DEFINE WRITE_ONLY_UNBUFFERED 11
#DEFINE COMMA ","
#DEFINE xlEdgeBottom 9
#DEFINE xlContinuous 1
#DEFINE xlMedium -4138

DEFINE CLASS SHCOSTTRENDING AS CUSTOM

	cProcessName = 'SHCOSTTRENDING'

	lTestMode = .T.  && .F. FOR NORMAL OPERATION, .T. TO SUPPRESS ALL BUT DEVELOPER EMAILS AND CHANGE INPUT TABLE, ETC.

	dToday = DATE()
	*dToday = {^2015-10-31}

	lAutoYield = .T.

	cStartTime = TTOC(DATETIME())
	
	dStartDate = {}
	dEndDate = {}
	oExcel = NULL
	oWorksheet = NULL
	oWorkbook = NULL
	
	cCurrentDiv = '00'
	nCurrentRow = 0
	nCurrentSheet = 0
	cReportPeriod = ''

	* wait window properties
	nWaitWindowTimeout = 2
	lWaitWindowIsOn = .T.

	* logfile properties
	lLoggingIsOn = .T.
	cLogFile = 'F:\UTIL\SHOPREPORTS\LOGFILES\SHCOSTTRENDING_log.txt'

	* INTERNAL email properties
	lSendInternalEmailIsOn = .T.
	cFrom = 'mark.bennett@tollgroup.com'
	cSendTo = 'mbennett@fmiint.com'
	cCC = ''
	cSubject = "Shop Costs Trending for " + TRANSFORM(DATETIME())
	cAttach = ''
	cBodyText = ''

	cCOMPUTERNAME = ''
	cUSERNAME = ''


	FUNCTION INIT
		IF NOT DODEFAULT()
			RETURN .F.
		ENDIF
		WITH THIS
			*SET RESOURCE OFF
			CLOSE DATA
			SET CENTURY ON
			*SET DATE AMERICAN
			*SET HOURS TO 24
			SET DECIMALS TO 2
			SET ANSI ON
			SET TALK OFF
			SET DELETED ON
			SET CONSOLE OFF
			SET EXCLUSIVE OFF
			SET SAFETY OFF
			SET EXACT OFF
			SET STATUS BAR ON
			SET SYSMENU OFF
			SET ENGINEBEHAVIOR 70
			SET MULTILOCKS ON
			_VFP.AUTOYIELD = .lAutoYield
			.cCOMPUTERNAME = GETENV("COMPUTERNAME")
			.cUSERNAME = GETENV("USERNAME")
			.lLoggingIsOn = .lLoggingIsOn AND NOT EMPTY(.cLogFile)
		ENDWITH
	ENDFUNC


	FUNCTION DESTROY
		WITH THIS
			IF .lLoggingIsOn  THEN
				SET ALTERNATE OFF
				SET ALTERNATE TO
			ENDIF
		ENDWITH
		DODEFAULT()
	ENDFUNC


	FUNCTION MAIN
		LPARAMETERS tlTestMode
		WITH THIS
			LOCAL lnRow, lcRow, lcFiletoSaveAs, lcSpreadsheetTemplate, lnNumberOfErrors
			LOCAL oExcel, oWorkbook, lcErr, lcTitle, loError, lnRate, ldToday
			LOCAL lcDivision, lnLastRow, lnRate, lcDetailBasisFile

			TRY
				lnNumberOfErrors = 0

				.lTestMode = tlTestMode

				IF .lTestMode THEN
					.cLogFile = 'F:\UTIL\SHOPREPORTS\LOGFILES\SHCOSTTRENDING_log_TESTMODE.txt'
					.cSendTo = 'mbennett@fmiint.com'
				ENDIF

				IF .lLoggingIsOn THEN
					SET ALTERNATE TO (.cLogFile) ADDITIVE
					SET ALTERNATE ON
				ENDIF

				.cStartTime = TTOC(DATETIME())

				.TrackProgress('Shop Costs Trending process started....', LOGIT+SENDIT)
				.TrackProgress('COMPUTERNAME = ' + .cCOMPUTERNAME, LOGIT+SENDIT)
				.TrackProgress('USERNAME = ' + .cUSERNAME, LOGIT+SENDIT)
				.TrackProgress('PROJECT = SHCOSTTRENDING', LOGIT+SENDIT)
				IF .lTestMode THEN
					.TrackProgress('                        TEST MODE                        ', LOGIT+SENDIT)
				ENDIF
				*******************************************************************************************************************
				
				.dStartDate = {^2015-04-01}
				.dEndDate = {^2016-03-31}
				.cReportPeriod = DTOS(.dStartDate) + " - " + DTOS(.dEndDate)

				lcSpreadsheetTemplate = "F:\UTIL\SHOPREPORTS\TEMPLATES\SHOPCOSTSTRENDING_TEMPLATE.XLSX"

				lcFiletoSaveAs = "F:\UTIL\SHOPREPORTS\REPORTS\SHOPCOSTSTRENDING_for_" + .cReportPeriod + ".XLSX"
				
				lcTitle = "Shop Costs Trending for " + .cReportPeriod
				.cSubject = lcTitle

				USE F:\SHOP\SHDATA\ordhdr IN 0
				USE F:\SHOP\SHDATA\trucks IN 0
				USE F:\SHOP\SHDATA\trailer IN 0
				USE F:\SHOP\SHDATA\chaslist IN 0
				USE F:\SHOP\SHDATA\mainttyp IN 0
				USE F:\SHOP\SHDATA\repcause IN 0

				SELECT A.ORDNO, A.VEHNO, NVL(B.MAINTDESC,"?") AS MAINTTYPE, NVL(C.DESCRIPT,"?") AS REPCAUSE, A.CRDATE, A.MECHNAME, A.TOTHOURS, A.TOTLCOST, A.TOTPCOST, ;
					A.OUTSCOST, A.OVNAME, A.OVCITY, A.OVSTATE, A.MILEAGE, A.VEHHOURS, LEFT(A.REMARKS,50) AS REMARKS, ;
					LEFT(A.WORKDESC,50) AS WORKDESC, A.TAXAMT, A.OILCOST, A.PARTCOST, A.TIRECOST, A.TYPE, ;
					A.COMPANY, A.OOPTRUKNUM, A.UPDATEBY, A.UPDATEDT, A.ADDBY, A.ADDDT, ;
					"  " AS DIVISION, 0000.00 AS RATE, 00 AS NMONTH, 0000 AS NYEAR, SPACE(3) AS CMONTH ;
					FROM ordhdr A ;
					LEFT OUTER JOIN mainttyp B ;
					ON B.MAINTTYPE = A.MAINTTYPE ;
					LEFT OUTER JOIN repcause C ;
					ON C.CODE = A.REPCAUSE ;					
					INTO CURSOR CURORDHDRPRE ;
					WHERE BETWEEN(A.crdate, .dStartDate, .dEndDate) ;
					AND (NOT EMPTY(A.ORDNO)) ;
					AND (NOT EMPTY(A.vehno)) ;
					AND A.TYPE IN ("TRUCK  ","TRAILER","CHASSIS") ;
					ORDER BY A.crdate ;
					READWRITE

				* POPULATE RATE BY CRDATE AND CALC LABOR COST FOR EACH WO REC
				SELECT CURORDHDRPRE
				SCAN
					lnRate = .GetHourlyRateByCRDate( CURORDHDRPRE.crdate )
					REPLACE CURORDHDRPRE.RATE WITH lnRate, CURORDHDRPRE.TOTLCOST WITH ( CURORDHDRPRE.tothours * lnRate ) IN CURORDHDRPRE
				ENDSCAN

				SELECT CURORDHDRPRE
				SCAN

					WAIT WINDOW "checking Vehicle #" + TRANSFORM(CURORDHDRPRE.vehno) NOWAIT

					DO CASE
						CASE ALLTRIM(CURORDHDRPRE.TYPE) == "TRUCK"
							SELECT trucks
							LOCATE FOR ALLTRIM(truck_num) == ALLTRIM(CURORDHDRPRE.vehno)
							IF FOUND() THEN
								REPLACE CURORDHDRPRE.DIVISION WITH trucks.DIVISION IN CURORDHDRPRE
							ELSE
								.TrackProgress('ERROR: TRUCK NOT FOUND: ' + CURORDHDRPRE.vehno, LOGIT+SENDIT)
							ENDIF
							SELECT trucks
							LOCATE
							LOOP
						CASE ALLTRIM(CURORDHDRPRE.TYPE) == "TRAILER"
							SELECT trailer
							LOCATE FOR ALLTRIM(trail_num) == ALLTRIM(CURORDHDRPRE.vehno)
							IF FOUND() THEN
								REPLACE CURORDHDRPRE.DIVISION WITH trailer.DIVISION IN CURORDHDRPRE
							ELSE
								.TrackProgress('ERROR: TRAILER NOT FOUND: ' + CURORDHDRPRE.vehno, LOGIT+SENDIT)
							ENDIF
							SELECT trailer
							LOCATE
							LOOP
						CASE ALLTRIM(CURORDHDRPRE.TYPE) == "CHASSIS"
							SELECT chaslist
							LOCATE FOR ALLTRIM(chassis) == ALLTRIM(CURORDHDRPRE.vehno)
							IF FOUND() THEN
								REPLACE CURORDHDRPRE.DIVISION WITH chaslist.DIVISION IN CURORDHDRPRE
							ELSE
								.TrackProgress('ERROR: CHASSIS NOT FOUND: ' + CURORDHDRPRE.vehno, LOGIT+SENDIT)
							ENDIF
							SELECT chaslist
							LOCATE
							LOOP
						OTHERWISE
							.TrackProgress('!!!! ERROR: VEHICLE TYPE IN CURORDHDRPRE', LOGIT+SENDIT)
							.cSendTo = 'mbennett@fmiint.com'
							.cCC = ''
							THROW
					ENDCASE
					
				ENDSCAN

				* POPULATE MONTH/YEAR FIELDS
				SELECT CURORDHDRPRE
				SCAN
					REPLACE CURORDHDRPRE.NMONTH WITH MONTH(CRDATE), CURORDHDRPRE.CMONTH WITH CMONTH(CRDATE), CURORDHDRPRE.NYEAR WITH YEAR(CRDATE) IN CURORDHDRPRE
				ENDSCAN

*!*	SELECT CURORDHDRPRE
*!*	browse
				* detail as needed
				lcDetailBasisFile = "C:\A\SHOPCOSTSTRENDING_DETAIL_for_" + .cReportPeriod + ".XLS"
				SELECT CURORDHDRPRE
				COPY TO (lcDetailBasisFile) XL5 && FOR (DIVISION = '02') AND (NMONTH = 2)
				
				

				WAIT CLEAR

				.oExcel = CREATEOBJECT("excel.application")
				.oExcel.displayalerts = .F.
				.oExcel.VISIBLE = .F.

				.oWorkbook = .oExcel.workbooks.OPEN(lcSpreadsheetTemplate) && 
				.oWorkbook.SAVEAS(lcFiletoSaveAs)
				
				
				.nCurrentSheet = 1
				.ProcessFirstTab()


				.cCurrentDiv = '02'
				.nCurrentSheet = 2
				.ProcessDivision()

				.cCurrentDiv = '03'
				.nCurrentSheet = 3
				.ProcessDivision()

				.cCurrentDiv = '55'
				.nCurrentSheet = 4
				.ProcessDivision()


				.oWorkbook.SAVE()

				.oExcel.QUIT()

*!*					
*!*					.cAttach = lcFiletoSaveAs


*!*					.cAttach = .cAttach + "," + lcDetailBasisFile
*!*					
*!*					
*!*					
*!*					*.CreateRowsMaintType()
*!*					
				
				
				
				*******************************************************************************************************************
				.TrackProgress('Shop Costs Trending process ended normally.', LOGIT+SENDIT)

			CATCH TO loError

				.TrackProgress('There was an error.',LOGIT+SENDIT)
				.TrackProgress('The Error # is: ' + TRANSFORM(loError.ERRORNO), LOGIT+SENDIT)
				.TrackProgress('The Error Message is: ' + TRANSFORM(loError.MESSAGE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred in Program: ' + TRANSFORM(loError.PROCEDURE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred at line #: ' + TRANSFORM(loError.LINENO), LOGIT+SENDIT)
				lnNumberOfErrors = lnNumberOfErrors + 1
				.cSendTo = 'mbennett@fmiint.com'

				IF TYPE('.oExcel') = 'O' AND NOT ISNULL(.oExcel) THEN
					.oExcel.QUIT()
				ENDIF

			ENDTRY

			WAIT CLEAR
			***************** INTERNAL email results ******************************
			.TrackProgress('About to send status email.',LOGIT+SENDIT)
			.TrackProgress('The logfile is: ' + .cLogFile, LOGIT+SENDIT)
			.TrackProgress('Shop Costs Trending process started: ' + .cStartTime, LOGIT+SENDIT)
			.TrackProgress('Shop Costs Trending process finished: ' + TTOC(DATETIME()), LOGIT+SENDIT)

			IF .lSendInternalEmailIsOn THEN
				DO FORM dartmail2 WITH .cSendTo,.cFrom,.cSubject,.cCC,.cAttach,.cBodyText,"A"
				.TrackProgress('Sent status email.',LOGIT)
			ELSE
				.TrackProgress('Did not send status email: THIS.lSendInternalEmailIsOn = FALSE.',LOGIT+SENDIT)
			ENDIF

		ENDWITH
		RETURN
	ENDFUNC && main
	
	
	FUNCTION ProcessDivision
		WITH THIS
		
			LOCAL lnRow, lcRow
		
			* TOTALS BY MONTH
			SELECT NYEAR, CMONTH, DIVISION, ;
			SUM (TOTLCOST + TOTPCOST) AS INSCOST, ;
			SUM (OUTSCOST) AS OUTSCOST, ;
			000.00 AS INPCT, ;
			000.00 AS OUTPCT, ;
			00000000.00 AS TOTCOST ;
			FROM CURORDHDRPRE ;
			INTO CURSOR CURTOTBYMONTH ;
			WHERE DIVISION = .cCurrentDiv ;
			GROUP BY NYEAR, CMONTH, DIVISION ;
			ORDER BY NYEAR, NMONTH, DIVISION ;
			READWRITE
			
			SELECT CURTOTBYMONTH 
			SCAN
				lnInside = CURTOTBYMONTH.INSCOST
				lnOutside = CURTOTBYMONTH.OUTSCOST
				lnTotal = lnInside + lnOutside
				IF lnTotal > 0 THEN
					lnOutPercent = (lnOutside / lnTotal) * 100
					lnInPercent = (lnInside / lnTotal) * 100
				ELSE
					lnOutPercent = 0
					lnInPercent = 0
				ENDIF
				REPLACE CURTOTBYMONTH.INPCT WITH lnInPercent, CURTOTBYMONTH.OUTPCT WITH lnOutPercent, CURTOTBYMONTH.TOTCOST WITH lnTotal IN CURTOTBYMONTH			
			ENDSCAN
			
*!*				SELECT CURTOTBYMONTH 
*!*				BROWSE
	
			.oWorksheet = .oWorkbook.Worksheets[.nCurrentSheet]
			*.oWorksheet.RANGE("A5","Q20").clearcontents()
			.oWorksheet.RANGE("C1").VALUE = "Reporting Period = " + .cReportPeriod
			
			lnRow = 3

			SELECT CURTOTBYMONTH 
			SCAN
				lnRow = lnRow + 1
				lcRow = ALLTRIM(STR(lnRow))
				.oWorksheet.RANGE("A"+lcRow).VALUE = CURTOTBYMONTH.NYEAR
				.oWorksheet.RANGE("B"+lcRow).VALUE = CURTOTBYMONTH.CMONTH
				.oWorksheet.RANGE("C"+lcRow).VALUE = CURTOTBYMONTH.DIVISION
				* LEAVE COL D BLANK
				.oWorksheet.RANGE("E"+lcRow).VALUE = CURTOTBYMONTH.INSCOST
				.oWorksheet.RANGE("F"+lcRow).VALUE = CURTOTBYMONTH.OUTSCOST
				.oWorksheet.RANGE("G"+lcRow).VALUE = CURTOTBYMONTH.TOTCOST
				.oWorksheet.RANGE("H"+lcRow).VALUE = CURTOTBYMONTH.INPCT
				.oWorksheet.RANGE("I"+lcRow).VALUE = CURTOTBYMONTH.OUTPCT			
			ENDSCAN		
			
			
		
			* TOTALS BY MONTH & REPCAUSE
			SELECT NYEAR, CMONTH, DIVISION, REPCAUSE, ;
			SUM (TOTLCOST + TOTPCOST) AS INSCOST, ;
			SUM (OUTSCOST) AS OUTSCOST, ;
			000.00 AS INPCT, ;
			000.00 AS OUTPCT, ;
			00000000.00 AS TOTCOST ;
			FROM CURORDHDRPRE ;
			INTO CURSOR CURTOTBYMONTHREP ;
			WHERE DIVISION = .cCurrentDiv ;
			GROUP BY NYEAR, CMONTH, DIVISION, REPCAUSE ;
			ORDER BY NYEAR, NMONTH, DIVISION, REPCAUSE ;
			READWRITE
			
			SELECT CURTOTBYMONTHREP 
			SCAN
				lnInside = CURTOTBYMONTHREP.INSCOST
				lnOutside = CURTOTBYMONTHREP.OUTSCOST
				lnTotal = lnInside + lnOutside
				IF lnTotal > 0 THEN
					lnOutPercent = (lnOutside / lnTotal) * 100
					lnInPercent = (lnInside / lnTotal) * 100
				ELSE
					lnOutPercent = 0
					lnInPercent = 0
				ENDIF
				REPLACE CURTOTBYMONTHREP.INPCT WITH lnInPercent, CURTOTBYMONTHREP.OUTPCT WITH lnOutPercent, CURTOTBYMONTHREP.TOTCOST WITH lnTotal IN CURTOTBYMONTHREP			
			ENDSCAN
			
			SELECT CURTOTBYMONTHREP 
			BROWSE	
			
			lnRow = lnRow + 2
			lcRow = ALLTRIM(STR(lnRow))
			.oWorksheet.RANGE("A"+lcRow).VALUE = 'Year'
			.oWorksheet.RANGE("B"+lcRow).VALUE = 'Month'
			.oWorksheet.RANGE("C"+lcRow).VALUE = 'Div'
			.oWorksheet.RANGE("D"+lcRow).VALUE = 'Repair Cause'
			.oWorksheet.RANGE("E"+lcRow).VALUE = 'Inside Cost'
			.oWorksheet.RANGE("F"+lcRow).VALUE = 'Outside Cost'
			.oWorksheet.RANGE("G"+lcRow).VALUE = 'Total Cost'
			.oWorksheet.RANGE("H"+lcRow).VALUE = 'Inside %'
			.oWorksheet.RANGE("I"+lcRow).VALUE = 'Outside %'	
			
			* do bold and underline of current row
			.oWorksheet.RANGE("A"+lcRow,"I"+lcRow).FONT.BOLD = .T.		
			* UNDERLINE cell columns to be totaled...
			.oWorksheet.RANGE("A"+lcRow,"I"+lcRow).BORDERS(xlEdgeBottom).LineStyle = xlContinuous
			.oWorksheet.RANGE("A"+lcRow,"I"+lcRow).BORDERS(xlEdgeBottom).Weight = xlMedium

			SELECT CURTOTBYMONTHREP 
			SCAN
				lnRow = lnRow + 1
				lcRow = ALLTRIM(STR(lnRow))
				.oWorksheet.RANGE("A"+lcRow).VALUE = CURTOTBYMONTHREP.NYEAR
				.oWorksheet.RANGE("B"+lcRow).VALUE = CURTOTBYMONTHREP.CMONTH
				.oWorksheet.RANGE("C"+lcRow).VALUE = CURTOTBYMONTHREP.DIVISION
				.oWorksheet.RANGE("D"+lcRow).VALUE = CURTOTBYMONTHREP.REPCAUSE
				.oWorksheet.RANGE("E"+lcRow).VALUE = CURTOTBYMONTHREP.INSCOST
				.oWorksheet.RANGE("F"+lcRow).VALUE = CURTOTBYMONTHREP.OUTSCOST
				.oWorksheet.RANGE("G"+lcRow).VALUE = CURTOTBYMONTHREP.TOTCOST
				.oWorksheet.RANGE("H"+lcRow).VALUE = CURTOTBYMONTHREP.INPCT
				.oWorksheet.RANGE("I"+lcRow).VALUE = CURTOTBYMONTHREP.OUTPCT			
			ENDSCAN		
			
					
	
		ENDWITH
		RETURN
	ENDFUNC && ProcessDivision
	
	
	FUNCTION ProcessFirstTab
		WITH THIS
		
			LOCAL lnRow, lcRow
		
			* TOTALS BY MONTH
			SELECT NYEAR, CMONTH, ;
			SUM (TOTLCOST + TOTPCOST) AS INSCOST, ;
			SUM (OUTSCOST) AS OUTSCOST, ;
			000.00 AS INPCT, ;
			000.00 AS OUTPCT, ;
			00000000.00 AS TOTCOST ;
			FROM CURORDHDRPRE ;
			INTO CURSOR CURTOTBYMONTH ;
			GROUP BY NYEAR, CMONTH ;
			ORDER BY NYEAR, NMONTH ;
			READWRITE
			
			SELECT CURTOTBYMONTH 
			SCAN
				lnInside = CURTOTBYMONTH.INSCOST
				lnOutside = CURTOTBYMONTH.OUTSCOST
				lnTotal = lnInside + lnOutside
				IF lnTotal > 0 THEN
					lnOutPercent = (lnOutside / lnTotal) * 100
					lnInPercent = (lnInside / lnTotal) * 100
				ELSE
					lnOutPercent = 0
					lnInPercent = 0
				ENDIF
				REPLACE CURTOTBYMONTH.INPCT WITH lnInPercent, CURTOTBYMONTH.OUTPCT WITH lnOutPercent, CURTOTBYMONTH.TOTCOST WITH lnTotal IN CURTOTBYMONTH			
			ENDSCAN
			
*!*				SELECT CURTOTBYMONTH 
*!*				BROWSE
	
			.oWorksheet = .oWorkbook.Worksheets[.nCurrentSheet]
			*.oWorksheet.RANGE("A5","Q20").clearcontents()
			.oWorksheet.RANGE("C1").VALUE = "Reporting Period = " + .cReportPeriod
			
			lnRow = 3

			SELECT CURTOTBYMONTH 
			SCAN
				lnRow = lnRow + 1
				lcRow = ALLTRIM(STR(lnRow))
				.oWorksheet.RANGE("A"+lcRow).VALUE = CURTOTBYMONTH.NYEAR
				.oWorksheet.RANGE("B"+lcRow).VALUE = CURTOTBYMONTH.CMONTH
				*.oWorksheet.RANGE("C"+lcRow).VALUE = CURTOTBYMONTH.DIVISION
				.oWorksheet.RANGE("D"+lcRow).VALUE = CURTOTBYMONTH.INSCOST
				.oWorksheet.RANGE("E"+lcRow).VALUE = CURTOTBYMONTH.OUTSCOST
				.oWorksheet.RANGE("F"+lcRow).VALUE = CURTOTBYMONTH.TOTCOST
				.oWorksheet.RANGE("G"+lcRow).VALUE = CURTOTBYMONTH.INPCT
				.oWorksheet.RANGE("H"+lcRow).VALUE = CURTOTBYMONTH.OUTPCT			
			ENDSCAN			
	
		ENDWITH
		RETURN
	ENDFUNC && ProcessFirstTab
	


	FUNCTION CreateRowsOneDate
		LPARAMETERS tnRow, tcDivision
		* Adam wants if to be each Sunday, not Friday, 3/3/2015
		* This version of the report assumes it is run on Sunday, and just reports on that Sunday.
		LOCAL lnRow, lcRow, lnInside, lnOutside, lnTotal, lnOutPercent, lcDivision, ldDate, lnInPercent

		ldDate = .dToday  && pdStartDate
		lnRow = tnRow

*!*			DO WHILE ldDate <= pdEndDate

			*IF DOW(ldDate,1) = 1 THEN
				*it's a Friday, query for that date - NO: SUNDAY
				
				lnRow = lnRow + 1
				lcRow = ALLTRIM(STR(lnRow))
				
*!*		SET STEP ON 

				* TRAILERS
				SELECT CURORDHDRPRE
				SUM (TOTLCOST + TOTPCOST) TO lnInside FOR BETWEEN(crdate, pdStartDate, pdEndDate) AND DIVISION = tcDivision AND TYPE = 'TRAILER'
				SUM (OUTSCOST) TO lnOutside FOR BETWEEN(crdate, pdStartDate, pdEndDate) AND DIVISION = tcDivision AND TYPE = 'TRAILER'
				lnTotal = lnInside + lnOutside
				IF lnTotal > 0 THEN
					lnOutPercent = lnOutside / lnTotal
					lnInPercent = lnInside / lnTotal
				ELSE
					lnOutPercent = 0
					lnInPercent = 0
				ENDIF
				oWorksheet.RANGE("A"+lcRow).VALUE = CDOW(pdEndDate)
				oWorksheet.RANGE("B"+lcRow).VALUE = pdEndDate
				oWorksheet.RANGE("C"+lcRow).VALUE = "'" + tcDivision
				
				oWorksheet.RANGE("D"+lcRow).VALUE = lnInside
				oWorksheet.RANGE("E"+lcRow).VALUE = lnOutside
				oWorksheet.RANGE("F"+lcRow).VALUE = lnTotal
				
				oWorksheet.RANGE("G"+lcRow).VALUE = lnInPercent 
				oWorksheet.RANGE("G"+lcRow).NumberFormat = "##0.00%"
				oWorksheet.RANGE("H"+lcRow).VALUE = lnOutPercent
				oWorksheet.RANGE("H"+lcRow).NumberFormat = "##0.00%"

				* TRUCKS
				SELECT CURORDHDRPRE
				SUM (TOTLCOST + TOTPCOST) TO lnInside FOR BETWEEN(crdate, pdStartDate, pdEndDate) AND DIVISION = tcDivision AND TYPE = 'TRUCK  '
				SUM (OUTSCOST) TO lnOutside FOR BETWEEN(crdate, pdStartDate, pdEndDate) AND DIVISION = tcDivision AND TYPE = 'TRUCK  '
				lnTotal = lnInside + lnOutside
				IF lnTotal > 0 THEN
					lnOutPercent = lnOutside / lnTotal
					lnInPercent = lnInside / lnTotal
				ELSE
					lnOutPercent = 0
					lnInPercent = 0
				ENDIF
				
				*oWorksheet.RANGE("A"+lcRow).VALUE = CDOW(pdEndDate)
				*oWorksheet.RANGE("B"+lcRow).VALUE = pdEndDate
				*oWorksheet.RANGE("C"+lcRow).VALUE = "'" + tcDivision
				
				oWorksheet.RANGE("J"+lcRow).VALUE = lnInside
				oWorksheet.RANGE("K"+lcRow).VALUE = lnOutside
				oWorksheet.RANGE("L"+lcRow).VALUE = lnTotal
				
				oWorksheet.RANGE("M"+lcRow).VALUE = lnInPercent 
				oWorksheet.RANGE("M"+lcRow).NumberFormat = "##0.00%"
				oWorksheet.RANGE("N"+lcRow).VALUE = lnOutPercent
				oWorksheet.RANGE("N"+lcRow).NumberFormat = "##0.00%"

				* CHASSIS
				SELECT CURORDHDRPRE
				SUM (TOTLCOST + TOTPCOST) TO lnInside FOR BETWEEN(crdate, pdStartDate, pdEndDate) AND DIVISION = tcDivision AND TYPE = 'CHASSIS'
				SUM (OUTSCOST) TO lnOutside FOR BETWEEN(crdate, pdStartDate, pdEndDate) AND DIVISION = tcDivision AND TYPE = 'CHASSIS'
				lnTotal = lnInside + lnOutside
				IF lnTotal > 0 THEN
					lnOutPercent = lnOutside / lnTotal
					lnInPercent = lnInside / lnTotal
				ELSE
					lnOutPercent = 0
					lnInPercent = 0
				ENDIF
				
				*oWorksheet.RANGE("A"+lcRow).VALUE = CDOW(pdEndDate)
				*oWorksheet.RANGE("B"+lcRow).VALUE = pdEndDate
				*oWorksheet.RANGE("C"+lcRow).VALUE = "'" + tcDivision
				
				oWorksheet.RANGE("P"+lcRow).VALUE = lnInside
				oWorksheet.RANGE("Q"+lcRow).VALUE = lnOutside
				oWorksheet.RANGE("R"+lcRow).VALUE = lnTotal
				
				oWorksheet.RANGE("S"+lcRow).VALUE = lnInPercent 
				oWorksheet.RANGE("S"+lcRow).NumberFormat = "##0.00%"
				oWorksheet.RANGE("T"+lcRow).VALUE = lnOutPercent
				oWorksheet.RANGE("T"+lcRow).NumberFormat = "##0.00%"
									
				* total trucks + trailers + chassis requested by Fran
				oWorksheet.RANGE("V"+lcRow).VALUE = "=SUM(D" + lcRow + ",J" + lcRow + ",P" + lcRow + ")"
				oWorksheet.RANGE("W"+lcRow).VALUE = "=SUM(E" + lcRow + ",K" + lcRow + ",Q" + lcRow + ")"
				oWorksheet.RANGE("X"+lcRow).VALUE = "=SUM(F" + lcRow + ",L" + lcRow + ",R" + lcRow + ")"
				
				lnInside = oWorksheet.RANGE("V"+lcRow).VALUE
				lnOutside = oWorksheet.RANGE("W"+lcRow).VALUE				
				lnTotal = oWorksheet.RANGE("X"+lcRow).VALUE
				IF lnTotal > 0 THEN
					lnOutPercent = lnOutside / lnTotal
					lnInPercent = lnInside / lnTotal
				ELSE
					lnOutPercent = 0
					lnInPercent = 0
				ENDIF
				oWorksheet.RANGE("Y"+lcRow).VALUE = lnInPercent
				oWorksheet.RANGE("Y"+lcRow).NumberFormat = "##0.00%"
				oWorksheet.RANGE("Z"+lcRow).VALUE = lnOutPercent
				oWorksheet.RANGE("Z"+lcRow).NumberFormat = "##0.00%"
				

*!*				ENDIF && DOW(ldDate,1) = 6

*!*				ldDate = ldDate + 1

*!*			ENDDO

		RETURN lnRow

	ENDFUNC  && CreateRowsOneDate


	FUNCTION CreateRowsMaintType
		*LPARAMETERS tnRow, tcDivision
		* loop through the Fridays in the month so far, running a query and populating a spreadsheet row for each
		* Adam wants if to be each Sunday, not Friday, 3/3/2015
		LOCAL lnRow, lcRow, lnInside, lnOutside, lnTotal, lnOutPercent, lcDivision, ldDate, lnInPercent

		ldDate = .dToday  && pdStartDate
		*lnRow = tnRow
		
		
		IF USED('CURORDHDRPRE2') THEN
			USE IN CURORDHDRPRE2
		ENDIF
		
		IF USED('CURORDHDRPRE3') THEN
			USE IN CURORDHDRPRE3
		ENDIF
		
*!*			SELECT * FROM CURORDHDRPRE INTO CURSOR CURORDHDRPRE2 WHERE BETWEEN(crdate, pdStartDate, ldDate)
*!*			SELECT CURORDHDRPRE2
*!*			BROWSE
		
		SELECT A.*, B.MAINTDESC ;
		FROM CURORDHDRPRE A ;
		LEFT OUTER JOIN mainttyp B ;
		ON B.MAINTTYPE = A.MAINTTYPE ;
		INTO CURSOR CURORDHDRPRE2 ;
		WHERE BETWEEN(crdate, pdStartDate, pdEndDate) ;
		AND DIVISION IN ('02','03','55') ;
		ORDER BY A.CRDATE

*!*			SELECT CURORDHDRPRE2
*!*			BROWSE
		
		SELECT DIVISION, TYPE, MAINTDESC, ;
			SUM(TOTLCOST + TOTPCOST) AS INCOST, ;
			SUM(OUTSCOST) AS OUTCOST, ;
			SUM(TOTLCOST + TOTPCOST + OUTSCOST) AS TOTCOST, ;
			000.00 AS INPCT, ;
			000.00 AS OUTPCT ;
		FROM CURORDHDRPRE2 ;
		INTO CURSOR CURORDHDRPRE3 ;
		GROUP BY 1,2,3 ;
		ORDER BY 1,2,3 ;
		READWRITE
		

		SELECT CURORDHDRPRE3
		SCAN
			lnTotal = CURORDHDRPRE3.TOTCOST
			lnInside = CURORDHDRPRE3.INCOST
			lnOutside = CURORDHDRPRE3.OUTCOST
			
			IF  lnTotal > 0 THEN
				lnOutPercent = lnOutside / lnTotal
				lnInPercent = lnInside / lnTotal
			ELSE
				lnOutPercent = 0
				lnInPercent = 0
			ENDIF
			
			REPLACE CURORDHDRPRE3.INPCT WITH lnInPercent, CURORDHDRPRE3.OUTPCT WITH lnOutPercent IN CURORDHDRPRE3
		ENDSCAN
		
*!*			SELECT CURORDHDRPRE3
*!*			BROWSE
		SELECT CURORDHDRPRE3
		COPY TO C:\A\DIV020355_BY_MAINTTYPE.XLS XL5
		

*!*			DO WHILE ldDate <= pdEndDate

			*IF DOW(ldDate,1) = 1 THEN
				*it's a Friday, query for that date - NO: SUNDAY
				
*!*					lnRow = lnRow + 1
*!*					lcRow = ALLTRIM(STR(lnRow))

*!*					* TRAILERS
*!*					SELECT CURORDHDRPRE
*!*					SUM (TOTLCOST + TOTPCOST) TO lnInside FOR BETWEEN(crdate, pdStartDate, ldDate) AND DIVISION = tcDivision AND TYPE = 'TRAILER'
*!*					SUM (OUTSCOST) TO lnOutside FOR BETWEEN(crdate, pdStartDate, ldDate) AND DIVISION = tcDivision AND TYPE = 'TRAILER'
*!*					lnTotal = lnInside + lnOutside
*!*					IF lnTotal > 0 THEN
*!*						lnOutPercent = lnOutside / lnTotal
*!*						lnInPercent = lnInside / lnTotal
*!*					ELSE
*!*						lnOutPercent = 0
*!*						lnInPercent = 0
*!*					ENDIF
*!*					oWorksheet.RANGE("A"+lcRow).VALUE = CDOW(ldDate)
*!*					oWorksheet.RANGE("B"+lcRow).VALUE = ldDate
*!*					oWorksheet.RANGE("C"+lcRow).VALUE = "'" + tcDivision
*!*					
*!*					oWorksheet.RANGE("D"+lcRow).VALUE = lnInside
*!*					oWorksheet.RANGE("E"+lcRow).VALUE = lnOutside
*!*					oWorksheet.RANGE("F"+lcRow).VALUE = lnTotal
*!*					
*!*					oWorksheet.RANGE("G"+lcRow).VALUE = lnInPercent 
*!*					oWorksheet.RANGE("G"+lcRow).NumberFormat = "##0.00%"
*!*					oWorksheet.RANGE("H"+lcRow).VALUE = lnOutPercent
*!*					oWorksheet.RANGE("H"+lcRow).NumberFormat = "##0.00%"

*!*					* TRUCKS
*!*					SELECT CURORDHDRPRE
*!*					SUM (TOTLCOST + TOTPCOST) TO lnInside FOR BETWEEN(crdate, pdStartDate, ldDate) AND DIVISION = tcDivision AND TYPE = 'TRUCK  '
*!*					SUM (OUTSCOST) TO lnOutside FOR BETWEEN(crdate, pdStartDate, ldDate) AND DIVISION = tcDivision AND TYPE = 'TRUCK  '
*!*					lnTotal = lnInside + lnOutside
*!*					IF lnTotal > 0 THEN
*!*						lnOutPercent = lnOutside / lnTotal
*!*						lnInPercent = lnInside / lnTotal
*!*					ELSE
*!*						lnOutPercent = 0
*!*						lnInPercent = 0
*!*					ENDIF
*!*					
*!*					*oWorksheet.RANGE("A"+lcRow).VALUE = CDOW(ldDate)
*!*					*oWorksheet.RANGE("B"+lcRow).VALUE = ldDate
*!*					*oWorksheet.RANGE("C"+lcRow).VALUE = "'" + tcDivision
*!*					
*!*					oWorksheet.RANGE("J"+lcRow).VALUE = lnInside
*!*					oWorksheet.RANGE("K"+lcRow).VALUE = lnOutside
*!*					oWorksheet.RANGE("L"+lcRow).VALUE = lnTotal
*!*					
*!*					oWorksheet.RANGE("M"+lcRow).VALUE = lnInPercent 
*!*					oWorksheet.RANGE("M"+lcRow).NumberFormat = "##0.00%"
*!*					oWorksheet.RANGE("N"+lcRow).VALUE = lnOutPercent
*!*					oWorksheet.RANGE("N"+lcRow).NumberFormat = "##0.00%"

*!*					* CHASSIS
*!*					SELECT CURORDHDRPRE
*!*					SUM (TOTLCOST + TOTPCOST) TO lnInside FOR BETWEEN(crdate, pdStartDate, ldDate) AND DIVISION = tcDivision AND TYPE = 'CHASSIS'
*!*					SUM (OUTSCOST) TO lnOutside FOR BETWEEN(crdate, pdStartDate, ldDate) AND DIVISION = tcDivision AND TYPE = 'CHASSIS'
*!*					lnTotal = lnInside + lnOutside
*!*					IF lnTotal > 0 THEN
*!*						lnOutPercent = lnOutside / lnTotal
*!*						lnInPercent = lnInside / lnTotal
*!*					ELSE
*!*						lnOutPercent = 0
*!*						lnInPercent = 0
*!*					ENDIF
*!*					
*!*					*oWorksheet.RANGE("A"+lcRow).VALUE = CDOW(ldDate)
*!*					*oWorksheet.RANGE("B"+lcRow).VALUE = ldDate
*!*					*oWorksheet.RANGE("C"+lcRow).VALUE = "'" + tcDivision
*!*					
*!*					oWorksheet.RANGE("P"+lcRow).VALUE = lnInside
*!*					oWorksheet.RANGE("Q"+lcRow).VALUE = lnOutside
*!*					oWorksheet.RANGE("R"+lcRow).VALUE = lnTotal
*!*					
*!*					oWorksheet.RANGE("S"+lcRow).VALUE = lnInPercent 
*!*					oWorksheet.RANGE("S"+lcRow).NumberFormat = "##0.00%"
*!*					oWorksheet.RANGE("T"+lcRow).VALUE = lnOutPercent
*!*					oWorksheet.RANGE("T"+lcRow).NumberFormat = "##0.00%"
*!*										
*!*					* total trucks + trailers + chassis requested by Fran
*!*					oWorksheet.RANGE("V"+lcRow).VALUE = "=SUM(D" + lcRow + ",J" + lcRow + ",P" + lcRow + ")"
*!*					oWorksheet.RANGE("W"+lcRow).VALUE = "=SUM(E" + lcRow + ",K" + lcRow + ",Q" + lcRow + ")"
*!*					oWorksheet.RANGE("X"+lcRow).VALUE = "=SUM(F" + lcRow + ",L" + lcRow + ",R" + lcRow + ")"
*!*					
*!*					lnInside = oWorksheet.RANGE("V"+lcRow).VALUE
*!*					lnOutside = oWorksheet.RANGE("W"+lcRow).VALUE				
*!*					lnTotal = oWorksheet.RANGE("X"+lcRow).VALUE
*!*					IF lnTotal > 0 THEN
*!*						lnOutPercent = lnOutside / lnTotal
*!*						lnInPercent = lnInside / lnTotal
*!*					ELSE
*!*						lnOutPercent = 0
*!*						lnInPercent = 0
*!*					ENDIF
*!*					oWorksheet.RANGE("Y"+lcRow).VALUE = lnInPercent
*!*					oWorksheet.RANGE("Y"+lcRow).NumberFormat = "##0.00%"
*!*					oWorksheet.RANGE("Z"+lcRow).VALUE = lnOutPercent
*!*					oWorksheet.RANGE("Z"+lcRow).NumberFormat = "##0.00%"
				

*!*				ENDIF && DOW(ldDate,1) = 6

*!*				ldDate = ldDate + 1

*!*			ENDDO

		RETURN lnRow

	ENDFUNC  && CreateRowsMaintType





	FUNCTION GetHourlyRateByCRDate
		LPARAMETERS tdCRdate
		DO CASE
			CASE tdCRdate < {^2008-09-01}
				lnRate = 60.00
			OTHERWISE
				lnRate = 70.00  
		ENDCASE
		RETURN lnRate
	ENDFUNC


	FUNCTION GetHourlyRateByDivision
		LPARAMETERS tcDivision
		DO CASE
			CASE INLIST(tcDivision,'05','52','56','57','58','71','74')
				lnRate = 60
			OTHERWISE
				lnRate = 60  && the default rate
		ENDCASE
		RETURN lnRate
	ENDFUNC


	PROCEDURE TrackProgress
		* do any combination of Wait Window, writing to logfile, and adding to body of email,
		* based on nFlags parameter.
		LPARAMETERS tcExpression, tnFlags
		WITH THIS
			IF BITAND(tnFlags,LOGIT) = LOGIT THEN
				IF .lLoggingIsOn THEN
					?
					? .cProcessName + "  " + TTOC(DATETIME()) + ": " + tcExpression
				ENDIF
			ENDIF
			IF .lWaitWindowIsOn THEN
				IF BITAND(tnFlags,NOWAITIT) = NOWAITIT THEN
					WAIT WINDOW tcExpression NOWAIT
				ENDIF
				IF BITAND(tnFlags,WAITIT) = WAITIT THEN
					WAIT WINDOW tcExpression TIMEOUT .nWaitWindowTimeout
				ENDIF
			ENDIF
			IF BITAND(tnFlags,SENDIT) = SENDIT THEN
				IF .lSendInternalEmailIsOn THEN
					.cBodyText = .cBodyText + tcExpression + CRLF + CRLF
				ENDIF
			ENDIF
		ENDWITH
	ENDPROC  &&  TrackProgress


ENDDEFINE
