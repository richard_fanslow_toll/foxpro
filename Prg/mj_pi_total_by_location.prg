utilsetup("MJ_PI_TOTAL_BY_LOCATION")
Set Exclusive Off
Set Deleted On
Set Talk Off
Set Safety Off 


If !Used("phys")
	xsqlexec("select * from phys where mod='J'",,,"wh")
ENDIF


SELECT accountid as ACCOUNT, whseloc as LOCATION, SUM(totqty) as QTY  FROM phys GROUP BY accountid, whseloc;
INTO CURSOR pitotloc



SELECT pitotloc
If Reccount() > 0
  Export To "S:\MarcJacobsData\Reports\Wholesale Physical\mj_pi_total_by_location_" + Ttoc(Datetime(), 1)  Type Xls
  tsendto = "tmarg@fmiint.com,M.RICCO@marcjacobs.com"
  tattach = ""
  tcc =""
  tFrom ="Toll EDI System Operations <fmi-transload-ops@fmiint.com>"
  tmessage = "mj_pi_total_by_location_"+Ttoc(Datetime())+"   file available in S:\MarcJacobsData\Reports\Wholesale Physical\mj_pi_total_by_location_"
  tSubject = "mj_pi_total_by_location executed"
  Do Form m:\dev\frm\dartmail2 With tsendto,tFrom,tSubject, tcc,tattach,tmessage  ,"A"
ELSE 
  tsendto = "tmarg@fmiint.com,M.RICCO@marcjacobs.com"
  tattach = ""
  tcc =""
  tFrom ="Toll EDI System Operations <fmi-transload-ops@fmiint.com>"
  tmessage = "mj_pi_total_by_location executed NO data_"+Ttoc(Datetime())
  tSubject = "mj_pi_total_by_location executed NO data"
  Do Form m:\dev\frm\dartmail2 With tsendto,tFrom,tSubject, tcc,tattach,tmessage  ,"A"
Endif

Close Data All
schedupdate()
_Screen.Caption=gscreencaption