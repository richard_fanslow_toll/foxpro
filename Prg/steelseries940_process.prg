*!* m:\dev\prg\steelseries940_process.prg

CD &lcPath

LogCommentStr = ""

delimchar = cDelimiter
lcTranOpt= cTranslateOption

lnNum = ADIR(tarray,"*.*")

IF lnNum = 0
	WAIT WINDOW "      No Steel Series 940s in folder "+lcPath+ "  to import     " TIMEOUT 3
	NormalExit = .T.
	THROW
ENDIF

FOR thisfile = 1  TO lnNum
	STORE "" TO xfile,cFilename
	STORE 0 TO nFileSize
	xfile = lcPath+tarray[thisfile,1] && +"."
	nFileSize = VAL(TRANSFORM(tarray(thisfile,2)))
	cFilename = ALLTRIM(LOWER(tarray[thisfile,1]))
	cForeignStr = ""
	cForeignStr = FILETOSTR(xfile)
	DO remove_foreign WITH cForeignStr
	xfile = STRTOFILE(cForeignStr,(lcPath+cFilename))
	xfile = lcPath+tarray[thisfile,1] && +"."
	dFileDate = tarray[thisfile,3]
	cFileTime = tarray[thisfile,4]

	USE F:\hr\hrdata\holidays IN 0
	SELECT * FROM holidays WHERE YEAR(holidays.holidaydt) = YEAR(DATE()) INTO CURSOR tempholidays
	SELECT tempholidays
	INDEX ON holidaydt TAG holidaydt
	USE IN holidays

	STORE .F. TO lNextDay,lMonday,l4Day
	select tempholidays
	LOCATE FOR holidaydt = dFileDate
	IF FOUND()
		DO CASE
			CASE DOW(dFileDate) = 6
				lMonday = .T.
			CASE  MONTH(dFileDate) = 11
				IF DOW(dFileDate) = 5
					l4Day =  .T.
				ELSE
					lMonday = .T.
				ENDIF
			OTHERWISE
				lNextDay = .T.
		ENDCASE
	ELSE
		DO CASE && Processing for PTDATE calculation
			CASE (INLIST(DOW(dFileDate),7,1)) OR (DOW(dFileDate) = 6 AND cFileTime>="12:00:00")
				lMonday = .T.  && Will make PTDATE the following Monday for all Sat.-Sun file dates.
			CASE (!INLIST(DOW(dFileDate),6,7,1)) AND (cFileTime>="12:00:00")
				lNextDay= .T.  && Will make PTDATE the following day for non-weekend
		ENDCASE
	ENDIF

	RELEASE cFileTime
	archivefile  = (lcArchivepath+cFilename)
	WAIT WINDOW "Importing file: "+cFilename NOWAIT

	DO m:\dev\prg\loadedifile WITH xfile,delimchar,lcTranOpt,cCustname

	IF lTesting
		SELECT x856
		LOCATE
*BROW
	ENDIF

	IF !lTesting
*		COPY FILE [&xfile] TO ("F:\ftpusers\SteelSeries\Translate\"+cfilename) && to create 997
	ENDIF

	SELECT x856
	LOCATE FOR x856.segment = 'GS'
	IF x856.f1 = "FA"
		WAIT WINDOW "This is a 997 FA file...moving to correct folder" TIMEOUT 1
		cfile997in = ("F:\ftpusers\"+cCustname+"\997in\"+cFilename)
		COPY FILE [&xfile] TO [&cfile997in]
		DELETE FILE [&xfile]
		LOOP
	ELSE
		LOCATE
	ENDIF

	DO create_pt_cursors WITH cUseFolder,lLoadSQL && Added to prep for SQL Server transition

	lOK = .T.
	DO ("m:\dev\prg\steelseries940_bkdn")
	IF lOK 
    Select xpt
    Go top
    If Inlist(Substr(xpt.ship_ref,1,1),"R","W")
      cUseFolder = "f:\whs\whdata\"
    Else
      cUseFolder = "f:\why\whdata\"
    Endif  
		DO m:\dev\prg\all940_import
	ENDIF

ENDFOR

&& now clean up the 940in archive files, delete for the upto the last 10 days
**deletefile(lcArchivepath,IIF(DATE()<{^2016-05-01},20,10))

WAIT CLEAR
WAIT "Entire "+cMailname+" 940 Process Now Complete" WINDOW TIMEOUT 3

**************************
PROCEDURE remove_foreign
**************************
	PARAMETERS cForeignStr
	IF lTesting
*		SET STEP ON
	ENDIF
	cForeignStr = STRTRAN(cForeignStr,CHR(195)+CHR(161),"a") && � as ó
	cForeignStr = STRTRAN(cForeignStr,CHR(195)+CHR(163),"a") && � as ã
	cForeignStr = STRTRAN(cForeignStr,CHR(195)+CHR(168),"e") && � as è
	cForeignStr = STRTRAN(cForeignStr,CHR(195)+CHR(169),"e") && � as é
	cForeignStr = STRTRAN(cForeignStr,CHR(195)+CHR(171),"e") && � as ë
	cForeignStr = STRTRAN(cForeignStr,CHR(195)+CHR(173),"i") && i as í
	cForeignStr = STRTRAN(cForeignStr,CHR(195)+CHR(175),"i") && � as ë
	cForeignStr = STRTRAN(cForeignStr,CHR(195)+CHR(179),"o") && � as á
	cForeignStr = STRTRAN(cForeignStr,CHR(195)+CHR(180),"o") && � as ô
	cForeignStr = STRTRAN(cForeignStr,CHR(195)+CHR(185),"u") && � as ù
	cForeignStr = STRTRAN(cForeignStr,CHR(195)+CHR(167),"s") && � as ç
	cForeignStr = STRTRAN(cForeignStr,CHR(194)+CHR(186),"o") && o as º
	cForeignStr = STRTRAN(cForeignStr,CHR(195)+CHR(137),"e") && o as º
	cForeignStr = STRTRAN(cForeignStr,CHR(196)+CHR(159),"g") && g as º
	cForeignStr = STRTRAN(cForeignStr,CHR(195)+CHR(182),"oe") && � as º
	cForeignStr = STRTRAN(cForeignStr,CHR(195)+CHR(170),"ei") && � as ª
	cForeignStr = STRTRAN(cForeignStr,CHR(195)+CHR(186),"u") && � as ú
	cForeignStr = STRTRAN(cForeignStr,CHR(195)+CHR(177),"n") && � as ñ
	cForeignStr = STRTRAN(cForeignStr,CHR(195)+CHR(129),"a") && � as Á
	cForeignStr = STRTRAN(cForeignStr,CHR(195)+CHR(145),"n") && � as Ñ
	cForeignStr = STRTRAN(cForeignStr,CHR(195)+CHR(131),"a") && � as Ã
	cForeignStr = STRTRAN(cForeignStr,CHR(194)+CHR(176),"o") && � as º
	cForeignStr = STRTRAN(cForeignStr,CHR(195)+CHR(184),"ae") && � as ø
	cForeignStr = STRTRAN(cForeignStr,"h`","h") && Typo, came in with a reverse apostrophe. Preventative check.
	cForeignStr = STRTRAN(cForeignStr,CHR(150),"-")
*	cForeignStr = UPPER(cForeignStr)
ENDPROC
