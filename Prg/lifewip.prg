* WIP for Lifefactory

utilsetup("LIFEWIP")

if holiday(date())
	return
endif

guserid="AUTO"
gwipbywo=.f.
gwipbybl=.f.
xpnpacct=.f.
xacctname="LIFEFACTORY"
xwip=.t.
xtitle="Work in Progress"
tattach=""

xsqlexec("select * from account where inactive=0","account",,"qq")
index on accountid tag accountid
set order to

goffice="I"

xsqlexec("select * from pt where mod='I' and accountid=6034",,,"wh",,,,,,,.t.)

xsqlexec("select ship_ref, cast(' ' as char(15)) as status, wo_num, wo_date, consignee, cnee_ref, ship_via, ctnqty, qty as unitqty, " + ;
	"start, cancel, called, appt, appt_time, appt_num, apptremarks, del_date, pulled, picked, labeled, staged, truckloaddt, ptdate " + ;
	"from outship where accountid=6034 and (del_date={} or del_date>{"+dtoc(date()-30)+"}) and notonwip=0","xrpt",,"wh")

replace all status with iif(!pulled,"NOT PULLED",iif(!emptynul(del_date),"DELIVERED",iif(!emptynul(truckloaddt),"LOADED",iif(!emptynul(staged),"STAGED",iif(!emptynul(labeled),"LABELED",iif(!emptynul(picked),"PICKED","PULLED"))))))

select ship_ref, "UNALLOCATED" as status, consignee, cnee_ref, ship_via, qty as unitqty, start, cancel, ptdate ;
	from pt ;
	where accountid=6034 ;
	order by ship_ref into cursor xrpt2
	
scan 
	scatter memvar
	insert into xrpt from memvar
endscan

delete file h:\fox\lifewip.xls
xfilename="h:\fox\lifewip.xls"

select xrpt
*set step on 
index on ship_ref tag ship_ref
copy fields ship_ref, status, wo_num, consignee, cnee_ref, ship_via, ctnqty, unitqty, ptdate, wo_date, pulled, staged, del_date, start, cancel to (xfilename) xls

tattach=xfilename
* MB added emails 12/21/2017 per CM
*tsendto="opsreport@lifefactory.com, mbennett@fmiint.com"
tsendto="Mike.Yeager@thermos.com, Sabrina.Patten@thermos.com, opsreport@lifefactory.com, mbennett@fmiint.com"
*tsendto="mark.bennett@tollgroup.com"
tcc=""
tFrom ="TGF Operations <fmi-transload-ops@fmiint.com>"
tmessage = "See attached file"
tSubject = "Daily WIP report"

Do Form dartmail2 With tsendto,tFrom,tSubject," ",tattach,tmessage,"A"

schedupdate()

_screen.Caption=gscreencaption
on error




