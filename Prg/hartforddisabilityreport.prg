* run HARTFORD DISABILITY REPORT 
* Build EXE as F:\UTIL\HR\HARTFORD\hdr.exe
*
* converted to use Insperity Data 11/04/2014 MB

runack("HDR")

LOCAL loHartfordDisabilityReport
loHartfordDisabilityReport = CREATEOBJECT('HartfordDisabilityReport')
loHartfordDisabilityReport.MAIN()
CLOSE DATABASES ALL
RETURN

#DEFINE LOGIT 1
#DEFINE WAITIT 2
#DEFINE NOWAITIT 4
#DEFINE SENDIT 8
#DEFINE CRLF CHR(13) + CHR(10)
#DEFINE RETURN_DATA_MANDATORY .F.
#DEFINE RETURN_DATA_NOT_MANDATORY .T.
#DEFINE CSVFORMAT 6
#DEFINE WRITE_ONLY_UNBUFFERED 11
#DEFINE COMMA ","

DEFINE CLASS HartfordDisabilityReport AS CUSTOM

	cProcessName = 'HartfordDisabilityReport'

	lTestMode = .F.  && .F. FOR NORMAL OPERATION, .T. TO SUPPRESS ALL BUT DEVELOPER EMAILS AND CHANGE INPUT TABLE, ETC.

	lAutoYield = .T.

	cToday = DTOC(DATE())

	cStartTime = TTOC(DATETIME())

	* connection properties
	nSQLHandle = 0

	* wait window properties
	nWaitWindowTimeout = 2
	lWaitWindowIsOn = .T.

	* logfile properties
	lLoggingIsOn = .T.
	cLogFile = 'F:\UTIL\HR\HARTFORD\LOGFILES\HDR_log.txt'

	* INTERNAL email properties
	lSendInternalEmailIsOn = .T.
	cFrom = 'mark.bennett@tollgroup.com'
	cSendTo = 'Abigail.Melaika@Tollgroup.com, lauren.wojcik@tollgroup.com'
	cCC = 'mbennett@fmiint.com'
	cSubject = 'Hartford Disability Report for ' + DTOC(DATE())
	cAttach = ''
	cBodyText = ''

	cCOMPUTERNAME = ''
	cUSERNAME = ''


	FUNCTION INIT
		IF NOT DODEFAULT()
			RETURN .F.
		ENDIF
		WITH THIS
			*SET RESOURCE OFF
			CLOSE DATA
			SET CENTURY ON
			SET DATE AMERICAN
			SET HOURS TO 24
			SET ANSI ON
			SET TALK OFF
			SET DELETED ON
			SET CONSOLE OFF
			SET EXCLUSIVE OFF
			SET SAFETY OFF
			SET EXACT OFF
			SET STATUS BAR ON
			SET SYSMENU OFF
			SET ENGINEBEHAVIOR 70
			_VFP.AUTOYIELD = .lAutoYield
			.cCOMPUTERNAME = GETENV("COMPUTERNAME")
			.cUSERNAME = GETENV("USERNAME")
			IF .lTestMode THEN
				.cLogFile = 'F:\UTIL\HR\HARTFORD\LOGFILES\HDR_log_TESTMODE.txt'
				.cSendTo = 'mbennett@fmiint.com'
				.cCC = ''
			ENDIF
			.lLoggingIsOn = .lLoggingIsOn AND NOT EMPTY(.cLogFile)
			IF .lLoggingIsOn THEN
				SET ALTERNATE TO (.cLogFile) ADDITIVE
				SET ALTERNATE ON
			ENDIF
		ENDWITH
	ENDFUNC


	FUNCTION DESTROY
		WITH THIS
			IF .lLoggingIsOn  THEN
				SET ALTERNATE OFF
				SET ALTERNATE TO
			ENDIF
		ENDWITH
		DODEFAULT()
	ENDFUNC


	FUNCTION MAIN
		WITH THIS
			LOCAL lnNumberOfErrors, lcSQL, lcSQL2, lcSQL3, loError, lnNYCount, lcNYGender
			TRY
				lnNumberOfErrors = 0

				.TrackProgress('HARTFORD DISABILITY REPORT process started....', LOGIT+SENDIT+NOWAITIT)
				.TrackProgress('COMPUTERNAME = ' + .cCOMPUTERNAME, LOGIT+SENDIT)
				.TrackProgress('USERNAME = ' + .cUSERNAME, LOGIT+SENDIT)
				.TrackProgress('PROJECT = HDR', LOGIT+SENDIT)
				IF .lTestMode THEN
					.TrackProgress('                        TEST MODE                        ', LOGIT+SENDIT)
				ENDIF

*!*					lcSQL = ;
*!*						"SELECT " + ;
*!*						" FIRSTNAME, " + ;
*!*						" LASTNAME, " + ;
*!*						" STATEWRKDINCODE AS WORKSTATE, " + ;
*!*						" BIRTHDATE, " + ;
*!*						" GENDER, " + ;
*!*						" HIREDATE, " + ;
*!*						" RATE1AMT AS SALARY, " + ;
*!*						" SOCIALSECURITY# AS SS_NUM, " + ;
*!*						" EEOCJOBCLASS AS JOBCLASS, " + ;
*!*						" {FN CONCAT(STREETLINE1,{FN CONCAT(' ',{FN IFNULL(STREETLINE2,'')})})} AS ADDRESS, " + ;
*!*						" CITY, " + ;
*!*						" ZIPCODE, " + ;
*!*						" STATE " + ;
*!*						" FROM REPORTS.V_EMPLOYEE " + ;
*!*						" WHERE STATUS <> 'T' " + ;
*!*						" AND COMPANYCODE IN ('E87','E88','E89') " + ;
*!*						" AND SOCIALSECURITY# <> '137226445' " + ;
*!*						" AND STATEWRKDINCODE <> 'NJ' " + ;
*!*						" AND STATEWRKDINCODE <> 'CA' "

*!*					lcSQL2 = ;
*!*						"SELECT " + ;
*!*						" COUNT(*) AS NYCOUNT " + ;
*!*						" FROM REPORTS.V_EMPLOYEE " + ;
*!*						" WHERE STATUS <> 'T' " + ;
*!*						" AND COMPANYCODE IN ('E87','E88','E89') " + ;
*!*						" AND STATEWRKDINCODE = 'NY' "

*!*					lcSQL3 = ;
*!*						"SELECT " + ;
*!*						" GENDER, " + ;
*!*						" COUNT(*) AS NYCOUNT " + ;
*!*						" FROM REPORTS.V_EMPLOYEE " + ;
*!*						" WHERE STATUS <> 'T' " + ;
*!*						" AND COMPANYCODE IN ('E87','E88','E89') " + ;
*!*						" AND STATEWRKDINCODE = 'NY' " + ;
*!*						" GROUP BY GENDER " + ;
*!*						" ORDER BY GENDER "


				USE F:\UTIL\INSPERITY\DATA\WORKSITES IN 0 ALIAS WORKSITES

				USE F:\UTIL\TIMESTAR\DATA\EEINFO.DBF IN 0 ALIAS EEINFO

				IF USED('CURHDR') THEN
					USE IN CURHDR
				ENDIF
				IF USED('CURNYCOUNT') THEN
					USE IN CURNYCOUNT
				ENDIF
				IF USED('CURNYGENDER') THEN
					USE IN CURNYGENDER
				ENDIF

				SELECT A.NAME, A.WORKSITE, B.WSTATE AS WORKSTATE, A.BIRTHDAY, A.GENDER, A.HIREDATE, A.HOURLYRATE AS SALARY, A.DEPTDESC, A.STREET, A.CITY, A.STATE, A.ZIP ;
					FROM EEINFO A ;
					LEFT OUTER JOIN WORKSITES B ;
					ON ALLTRIM(B.WWORKSITE) == ALLTRIM(A.WORKSITE) ;
					INTO CURSOR CURHDR ;
					WHERE STATUS <> 'T' ;
					AND B.WSTATE <> 'NJ' ;
					AND B.WSTATE <> 'CA' ;
					ORDER BY A.NAME
					

				SELECT COUNT(*) AS NYCOUNT ;
					FROM CURHDR ;
					INTO CURSOR CURNYCOUNT ;
					WHERE WORKSTATE = 'NY'


				SELECT GENDER, COUNT(*) AS NYCOUNT ;
					FROM CURHDR ;
					INTO CURSOR CURNYGENDER ;
					WHERE WORKSTATE = 'NY' ;
					GROUP BY GENDER ;
					ORDER BY GENDER



				SELECT CURNYCOUNT
				LOCATE
				lnNYCount = CURNYCOUNT.NYCOUNT

				lcNYGender = ""
				SELECT CURNYGENDER
				SCAN
					lcNYGender = lcNYGender + IIF(ALLTRIM(CURNYGENDER.GENDER)="M","Male   = ","Female = ") + TRANSFORM(CURNYGENDER.NYCOUNT) + CRLF
				ENDSCAN


				lcOutputFile = "F:\UTIL\HR\HARTFORD\REPORTS\HARTFORD_DISABILITY_" + STRTRAN(.cToday,"/","-") + ".XLS"

				SELECT CURHDR

				COPY TO (lcOutputFile) XL5
				.TrackProgress('Output to spreadsheet: ' + lcOutputFile, LOGIT+SENDIT+NOWAITIT)

				IF FILE(lcOutputFile) THEN
					* attach output file to email
					.cAttach = lcOutputFile
					.cBodyText = "See attached Hartford Disability report." + ;
						CRLF + CRLF + "Count of NY Employees = " + TRANSFORM(lnNYCount) + ;
						CRLF + lcNYGender + ;
						CRLF + CRLF + "(do not reply - this is an automated report)" + ;
						CRLF + CRLF + "<report log follows>" + ;
						CRLF + .cBodyText
				ELSE
					.TrackProgress('ERROR: unable to email spreadsheet: ' + lcOutputFile, LOGIT+SENDIT+NOWAITIT)
				ENDIF

				CLOSE DATABASES ALL

				.TrackProgress('HARTFORD DISABILITY REPORT process ended normally.', LOGIT+SENDIT+NOWAITIT)

			CATCH TO loError

				.TrackProgress('There was an error.',LOGIT+SENDIT)
				.TrackProgress('The Error # is: ' + TRANSFORM(loError.ERRORNO), LOGIT+SENDIT)
				.TrackProgress('The Error Message is: ' + TRANSFORM(loError.MESSAGE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred in Program: ' + TRANSFORM(loError.PROCEDURE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred at line #: ' + TRANSFORM(loError.LINENO), LOGIT+SENDIT)
				lnNumberOfErrors = lnNumberOfErrors + 1
				CLOSE DATA

			ENDTRY

			CLOSE DATA
			WAIT CLEAR
			***************** INTERNAL email results ******************************
			.TrackProgress('About to send status email.',LOGIT)
			.TrackProgress('The logfile is: ' + .cLogFile, SENDIT)
			.TrackProgress('HARTFORD DISABILITY REPORT process started: ' + .cStartTime, LOGIT+SENDIT)
			.TrackProgress('HARTFORD DISABILITY REPORT process finished: ' + TTOC(DATETIME()), LOGIT+SENDIT)

			IF .lSendInternalEmailIsOn THEN
				DO FORM dartmail2 WITH .cSendTo,.cFrom,.cSubject,.cCC,.cAttach,.cBodyText,"A"
				.TrackProgress('Sent status email.',LOGIT)
			ELSE
				.TrackProgress('Did not send status email: THIS.lSendInternalEmailIsOn = FALSE.',LOGIT)
			ENDIF

		ENDWITH
		RETURN
	ENDFUNC && main


	FUNCTION ExecSQL
		LPARAMETERS tcSQL, tcCursorName, tlNoDataReturnedIsOkay
		LOCAL llRetval, lnResult
		WITH THIS
			* close target cursor if it's open
			IF USED(tcCursorName)
				USE IN (tcCursorName)
			ENDIF
			WAIT WINDOW "Fetching remote data; please wait..." NOWAIT
			lnResult = SQLEXEC(.nSQLHandle, tcSQL, tcCursorName)
			llRetval = ( lnResult > 0 )
			IF llRetval THEN
				* see if any data came back
				IF NOT tlNoDataReturnedIsOkay THEN
					IF (NOT USED(tcCursorName)) OR EOF(tcCursorName) THEN
						llRetval = .F.
						.TrackProgress('No data was returned by this query: ' +CRLF + tcSQL, LOGIT+SENDIT)
						.cSendTo = 'Mark Bennett <mbennett@fmiint.com>'
					ENDIF
				ENDIF
			ELSE
				.TrackProgress("Result Code [" + TRANSFORM(lnResult) + "] returned by this query: " + tcSQL, LOGIT+SENDIT)
				.cSendTo = 'Mark Bennett <mbennett@fmiint.com>'
			ENDIF
			WAIT CLEAR
			RETURN llRetval
		ENDWITH
	ENDFUNC


	PROCEDURE TrackProgress
		* do any combination of Wait Window, writing to logfile, and adding to body of email,
		* based on nFlags parameter.
		LPARAMETERS tcExpression, tnFlags
		WITH THIS
			IF BITAND(tnFlags,LOGIT) = LOGIT THEN
				IF .lLoggingIsOn THEN
					?
					? .cProcessName + "  " + TTOC(DATETIME()) + ": " + tcExpression
				ENDIF
			ENDIF
			IF .lWaitWindowIsOn THEN
				IF BITAND(tnFlags,NOWAITIT) = NOWAITIT THEN
					WAIT WINDOW tcExpression NOWAIT
				ENDIF
				IF BITAND(tnFlags,WAITIT) = WAITIT THEN
					WAIT WINDOW tcExpression TIMEOUT .nWaitWindowTimeout
				ENDIF
			ENDIF
			IF BITAND(tnFlags,SENDIT) = SENDIT THEN
				IF .lSendInternalEmailIsOn THEN
					.cBodyText = .cBodyText + tcExpression + CRLF + CRLF
				ENDIF
			ENDIF
		ENDWITH
	ENDPROC  &&  TrackProgress


ENDDEFINE
