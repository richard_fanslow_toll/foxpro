PARAMETERS uccstub
CLEAR

IF VARTYPE(uccstub) # "C"
	WAIT WINDOW "No UCC stub submitted...using local values" TIMEOUT 2

	uccstub = "0000043475000867847"  && <--- Substitute UCC Number used here
	uccstub = STRTRAN(uccstub," ","")
ENDIF

nValue1 = 0  && "ODD" Digits
nValue2 = 0  && "EVEN" Digits
nValue3 = 0  && SUMMARY Value
nChkDigit = 0

FOR i = LEN(TRIM(uccstub)) TO 1 STEP -2
	nValue1 = nValue1 + INT(VAL(SUBSTR(uccstub,i,1)))
ENDFOR
nValue1 = nValue1 * 3

FOR i = LEN(TRIM(uccstub))-1 TO 1 STEP -2
	nValue2 = nValue2 + INT(VAL(SUBSTR(uccstub,i,1)))
ENDFOR
nValue3 = nValue1 + nValue2

IF UCCStub = '0000672356002957781'
*ET STEP ON 
ENDIF

nChkDigit = IIF(nValue3 > 0,10-(nValue3%10),nValue3)  && <--- The output check digit here
nChkDigit = IIF(nChkDigit = 10,0,nChkDigit)
cChkDigit = TRANSFORM(nChkDigit)
WAIT WINDOW "CHECK DIGIT FOR "+uccstub+": "+cChkDigit+CHR(13)+"has been added" TIMEOUT 1
cUCC2 = uccstub+cChkDigit
RETURN cUCC2
