* assembly report for Lifefactory

utilsetup("LIFEASSEMBLY")

xsqlexec("select * from account where inactive=0","account",,"qq")
index on accountid tag accountid
set order to

goffice="I"

xsqlexec("select projectid, projno, custrefno, wo_num, putwonum, startdt as started, completebydt as dueby from project where accountid=6034 and completeddt={}","xtemp",,"wh")

select *, space(254) as jobdesc, space(10) as style, 000000 as totqty from xtemp where .f. into cursor xrpt readwrite

select xtemp 
scan 
	xsqlexec("select * from projdet where projectid="+transform(xtemp.projectid),,,"wh",,,,.t.)
	
	scatter memvar
	m.jobdesc=""
	select projdet
	scan
		if !empty(m.jobdesc)
			m.jobdesc=m.jobdesc+crlf
		endif
		m.jobdesc=m.jobdesc+trim(projdet.description)
	endscan

	if !empty(m.putwonum)
		xsqlexec("select * from pl where wo_num="+transform(m.putwonum),,,"wh")
		
		select pl
		scan for wo_num=m.putwonum
			m.style=style
			m.totqty=totqty
			insert into xrpt from memvar
		endscan
	else
		xsqlexec("select * from outwolog where wo_num="+transform(m.wo_num),,,"wh")
		xsqlexec("select * from outcomp where outwologid="+transform(outwolog.outwologid),"outcomp",,"wh")
		scan
			m.style=style
			m.totqty=qty
			insert into xrpt from memvar
		endscan
	endif
endscan

select xrpt
replace all jobdesc with strtran(jobdesc,crlf,"  ***  ")
*set step on 
delete file h:\fox\lifeassemblies.csv
copy to h:\fox\lifeassemblies.csv csv

tattach="h:\fox\lifeassemblies.csv"
tsendto="Greg@Lifefactory.com, ethan@lifefactory.com, Vlad@lifefactory.com, joe@lifefactory.com, hannah@lifefactory.com"
*tsendto="mark.bennett@tollgroup.com"
tcc="mbennett@fmiint.com, chris.malcolm@tollgroup.com"
tFrom ="TGF Operations <fmi-transload-ops@fmiint.com>"
tmessage = "See attached file"
tSubject = "Assemblies Report"

*Do Form dartmail2 With tsendto,tFrom,tSubject," ",tattach,tmessage,"A"
Do Form dartmail2 With tsendto,tFrom,tSubject,tcc,tattach,tmessage,"A"

schedupdate()

_screen.Caption=gscreencaption
on error
