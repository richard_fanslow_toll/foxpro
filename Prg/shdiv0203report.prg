*!*	* create a report that gives a running tally of div 02 and 03 costs - for Fran - MTD expected to be run each Friday

*!*	Every Friday by month.

*!*	Sample of what I would like to see:  
*!*	 
*!*	Company 02 
*!*	Trailer Cost $45,000
*!*	Truck Cost $25,000

*!*	Over the road repairs % versus repairs made at our shop.

*!*	Same info requested for Company 3.
*
* Fran requested div 55 be added 01/23/2015
*
* Fran requested that Chassis be added 2/19/2015.

* 7/5/2017 MB: added support for a parameter which will tell report to run against the entire prior month, instead of month-to-date

* run Friday mornings.
* build exe as F:\UTIL\SHOPREPORTS\SHDIV0203REPORT.EXE

LPARAMETERS tcMode

LOCAL lTestMode, lcMode
lTestMode = .F.

IF NOT lTestMode THEN
	utilsetup("SHDIV0203REPORT")
ENDIF

DECLARE INTEGER CreateMutex IN Win32API ;
	STRING @lpMutexAttributes, ;
	STRING bInitialOwner, ;
	STRING @lpName

DECLARE INTEGER GetLastError IN Win32API

* if running in EXE check to see if already running
IF _VFP.STARTMODE > 0 THEN
	lcProcessName = "SHDIV0203REPORT"
	CreateMutex(0,CHR(1),@lcProcessName)
	lnError = GetLastError()
	IF lnError = 183
		WAIT WINDOW TIMEOUT 5 "This program is already running..."
		IF NOT lTestMode THEN
			schedupdate()
		ENDIF
		RETURN .F.
	ENDIF
ENDIF

lcMode = 'MONTH-TO-DATE'
IF (NOT EMPTY(tcMode)) THEN
	IF UPPER(ALLTRIM(tcMode)) == 'PRIORMONTH' THEN
		lcMode = 'PRIORMONTH'	
	ENDIF
ENDIF

loSHDIV0203REPORT = CREATEOBJECT('SHDIV0203REPORT')
loSHDIV0203REPORT.MAIN( lTestMode, lcMode )

IF NOT lTestMode THEN
	schedupdate()
ENDIF

CLOSE DATABASES ALL
RETURN

#DEFINE LOGIT 1
#DEFINE WAITIT 2
#DEFINE NOWAITIT 4
#DEFINE SENDIT 8
#DEFINE CRLF CHR(13) + CHR(10)
#DEFINE RETURN_DATA_MANDATORY .F.
#DEFINE RETURN_DATA_NOT_MANDATORY .T.
#DEFINE CSVFORMAT 6
#DEFINE WRITE_ONLY_UNBUFFERED 11
#DEFINE COMMA ","

DEFINE CLASS SHDIV0203REPORT AS CUSTOM

	cProcessName = 'SHDIV0203REPORT' 

	lTestMode = .F.  && .F. FOR NORMAL OPERATION, .T. TO SUPPRESS ALL BUT DEVELOPER EMAILS AND CHANGE INPUT TABLE, ETC.

	dToday = DATE()
	*dToday = {^2015-10-31}
	
	cMode = 'MONTH-TO-DATE'   && can be changed to PRIORMONTH via passed parameter

	lAutoYield = .T.

	cStartTime = TTOC(DATETIME())

	* wait window properties
	nWaitWindowTimeout = 2
	lWaitWindowIsOn = .T.

	* logfile properties
	lLoggingIsOn = .T.
	cLogFile = 'F:\UTIL\SHOPREPORTS\LOGFILES\SHDIV0203REPORT_log.txt'

	* INTERNAL email properties
	lSendInternalEmailIsOn = .T.
	cFrom = 'mark.bennett@tollgroup.com'
	cSendTo = 'fran.castro@tollgroup.com, mike.drew@tollgroup.com'
	cCC = 'mbennett@fmiint.com'
	cSubject = "Company 02, 03 & 55 MTD Shop Costs for " + TRANSFORM(DATETIME())
	cAttach = ''
	cBodyText = ''
	cMGRSendTo = ''
	cMGRFrom = ''
	cMGRSubject = ''
	cMGRCC = ''
	cMGRAttach = ''
	cMGRBodyText = ''

	cCOMPUTERNAME = ''
	cUSERNAME = ''


	FUNCTION INIT
		IF NOT DODEFAULT()
			RETURN .F.
		ENDIF
		WITH THIS
			*SET RESOURCE OFF
			CLOSE DATA
			SET CENTURY ON
			*SET DATE AMERICAN
			*SET HOURS TO 24
			SET DECIMALS TO 2
			SET ANSI ON
			SET TALK OFF
			SET DELETED ON
			SET CONSOLE OFF
			SET EXCLUSIVE OFF
			SET SAFETY OFF
			SET EXACT OFF
			SET STATUS BAR ON
			SET SYSMENU OFF
			SET ENGINEBEHAVIOR 70
			SET MULTILOCKS ON
			_VFP.AUTOYIELD = .lAutoYield
			.cCOMPUTERNAME = GETENV("COMPUTERNAME")
			.cUSERNAME = GETENV("USERNAME")
			.lLoggingIsOn = .lLoggingIsOn AND NOT EMPTY(.cLogFile)
		ENDWITH
	ENDFUNC


	FUNCTION DESTROY
		WITH THIS
			IF .lLoggingIsOn  THEN
				SET ALTERNATE OFF
				SET ALTERNATE TO
			ENDIF
		ENDWITH
		DODEFAULT()
	ENDFUNC


	FUNCTION MAIN
		LPARAMETERS tlTestMode, tcMode
		WITH THIS
			LOCAL lnRow, lcRow, lcReportPeriod, lcFiletoSaveAs, lcSpreadsheetTemplate, lnNumberOfErrors
			LOCAL oExcel, oWorkbook, lcErr, lcTitle, loError, lnRate, ldToday
			LOCAL lcDivision, lnLastRow, lnRate, lcDetailBasisFile

			TRY
				lnNumberOfErrors = 0

				.lTestMode = tlTestMode
				.cMode = tcMode

				IF .lTestMode THEN
					.cLogFile = 'F:\UTIL\SHOPREPORTS\LOGFILES\SHDIV0203REPORT_log_TESTMODE.txt'
					.cSendTo = 'mbennett@fmiint.com'
					.cCC = ''
				ENDIF

				IF .lLoggingIsOn THEN
					SET ALTERNATE TO (.cLogFile) ADDITIVE
					SET ALTERNATE ON
				ENDIF

				.cStartTime = TTOC(DATETIME())

				.TrackProgress('Shop Div 02, 03 & 55 Costs process started....', LOGIT+SENDIT)
				.TrackProgress('COMPUTERNAME = ' + .cCOMPUTERNAME, LOGIT+SENDIT)
				.TrackProgress('USERNAME = ' + .cUSERNAME, LOGIT+SENDIT)
				.TrackProgress('MODE = ' + .cMode, LOGIT+SENDIT)
				.TrackProgress('PROJECT = SHDIV0203REPORT', LOGIT+SENDIT)
				IF .lTestMode THEN
					.TrackProgress('                        TEST MODE                        ', LOGIT+SENDIT)
				ENDIF
				*******************************************************************************************************************
				* make these visible in called procs
				PRIVATE pdStartDate, pdEndDate, oWorksheet

				* START by selecting entire MTD; later we will just report on the fridays.
				ldToday = .dToday
				*ldToday = {^2015-04-30}  && FOR TESTING

*!*					pdStartDate = ldToday - DAY(ldToday) + 1
*!*					pdEndDate = ldToday


*!*					lcReportPeriod = CMONTH(pdStartDate) + "_" + TRANSFORM(YEAR(pdStartDate))				
				
*!*	*!*	* to force custom date range
*!*	pdStartDate = {^2017-06-01}
*!*	pdEndDate = {^2017-06-30}
*!*	lcReportPeriod = DTOS(pdStartDate) + " - " + DTOS(pdEndDate)

				* added this block 07/05/2017 MB
				DO CASE
					CASE .cMode = 'PRIORMONTH'
						* run against entire prior month
						pdEndDate = ldToday - DAY(ldToday)
						pdStartDate = GOMONTH(pdEndDate + 1,-1)
						lcReportPeriod = 'for ' + CMONTH(pdStartDate) + " " + TRANSFORM(YEAR(pdStartDate))				
					OTHERWISE
						* default behaviour: MONTH-TO-DATE
						pdStartDate = ldToday - DAY(ldToday) + 1
						pdEndDate = ldToday
						lcReportPeriod = "Month-to-date for " + CMONTH(pdStartDate) + " " + TRANSFORM(YEAR(pdStartDate))				
				ENDCASE

				*lcSpreadsheetTemplate = "F:\SHOP\SHDATA\DIV0203_RPT_TEMPLATE2.XLS"
				*lcSpreadsheetTemplate = "F:\SHOP\SHDATA\DIV0203_RPT_TEMPLATE3.XLS"
				lcSpreadsheetTemplate = "F:\SHOP\SHDATA\DIV0203_RPT_TEMPLATE4.XLS"

				lcFiletoSaveAs = "F:\SHOP\AllocationReports\DIV 02 03 55_Summary " + lcReportPeriod + ".XLS"
				lcTitle = "Company 02, 03 & 55 Shop Costs " + lcReportPeriod
				.cSubject = lcTitle

				USE F:\SHOP\SHDATA\ordhdr IN 0
				USE F:\SHOP\SHDATA\trucks IN 0
				USE F:\SHOP\SHDATA\trailer IN 0
				USE F:\SHOP\SHDATA\chaslist IN 0
				USE F:\SHOP\SHDATA\mainttyp IN 0

				SELECT *, "  " AS DIVISION, 0000.00 AS RATE ;
					FROM ordhdr ;
					INTO CURSOR CURORDHDRPRE ;
					WHERE BETWEEN(crdate, pdStartDate, pdEndDate) ;
					AND (NOT EMPTY(ORDNO)) ;
					AND (NOT EMPTY(vehno)) ;
					AND TYPE IN ("TRUCK  ","TRAILER","CHASSIS") ;
					ORDER BY crdate ;
					READWRITE

				* POPULATE RATE BY CRDATE AND CALC LABOR COST FOR EACH WO REC
				SELECT CURORDHDRPRE
				SCAN
					lnRate = .GetHourlyRateByCRDate( CURORDHDRPRE.crdate )
					REPLACE CURORDHDRPRE.RATE WITH lnRate, CURORDHDRPRE.TOTLCOST WITH ( CURORDHDRPRE.tothours * lnRate )
				ENDSCAN

				SELECT CURORDHDRPRE
				SCAN

					WAIT WINDOW "checking Vehicle #" + TRANSFORM(CURORDHDRPRE.vehno) NOWAIT

					DO CASE
						CASE ALLTRIM(CURORDHDRPRE.TYPE) == "TRUCK"
							SELECT trucks
							LOCATE FOR ALLTRIM(truck_num) == ALLTRIM(CURORDHDRPRE.vehno)
							IF FOUND() THEN
								REPLACE CURORDHDRPRE.DIVISION WITH trucks.DIVISION IN CURORDHDRPRE
							ELSE
								.TrackProgress('ERROR: TRUCK NOT FOUND: ' + CURORDHDRPRE.vehno, LOGIT+SENDIT)
							ENDIF
							SELECT trucks
							LOCATE
							LOOP
						CASE ALLTRIM(CURORDHDRPRE.TYPE) == "TRAILER"
							SELECT trailer
							LOCATE FOR ALLTRIM(trail_num) == ALLTRIM(CURORDHDRPRE.vehno)
							IF FOUND() THEN
								REPLACE CURORDHDRPRE.DIVISION WITH trailer.DIVISION IN CURORDHDRPRE
							ELSE
								.TrackProgress('ERROR: TRAILER NOT FOUND: ' + CURORDHDRPRE.vehno, LOGIT+SENDIT)
							ENDIF
							SELECT trailer
							LOCATE
							LOOP
						CASE ALLTRIM(CURORDHDRPRE.TYPE) == "CHASSIS"
							SELECT chaslist
							LOCATE FOR ALLTRIM(chassis) == ALLTRIM(CURORDHDRPRE.vehno)
							IF FOUND() THEN
								REPLACE CURORDHDRPRE.DIVISION WITH chaslist.DIVISION IN CURORDHDRPRE
							ELSE
								.TrackProgress('ERROR: CHASSIS NOT FOUND: ' + CURORDHDRPRE.vehno, LOGIT+SENDIT)
							ENDIF
							SELECT chaslist
							LOCATE
							LOOP
						OTHERWISE
							.TrackProgress('!!!! ERROR: VEHICLE TYPE IN CURORDHDRPRE', LOGIT+SENDIT)
							.cSendTo = 'mbennett@fmiint.com'
							.cCC = ''
							THROW
					ENDCASE
					
				ENDSCAN

*!*	SELECT CURORDHDRPRE
*!*	browse

				WAIT CLEAR

				oExcel = CREATEOBJECT("excel.application")
				oExcel.displayalerts = .F.
				oExcel.VISIBLE = .F.

				oWorkbook = oExcel.workbooks.OPEN(lcSpreadsheetTemplate)
				oWorkbook.SAVEAS(lcFiletoSaveAs)

				oWorksheet = oWorkbook.Worksheets[1]
				oWorksheet.RANGE("A5","Q20").clearcontents()
				oWorksheet.RANGE("D1").VALUE = lcReportPeriod

				lnRow = 4
				lcDivision = '02'
				*lnLastRow = .CreateRows(lnRow, lcDivision)
				lnLastRow = .CreateRowsOneDate(lnRow, lcDivision)

				lcDivision = '03'
				*lnLastRow = .CreateRows(lnLastRow + 1, lcDivision)
				lnLastRow = .CreateRowsOneDate(lnLastRow + 1, lcDivision)

				lcDivision = '55'
				*lnLastRow = .CreateRows(lnLastRow + 1, lcDivision)
				lnLastRow = .CreateRowsOneDate(lnLastRow + 1, lcDivision)


				oWorkbook.SAVE()

				IF TYPE('oExcel') = 'O' AND NOT ISNULL(oExcel) THEN
					oExcel.QUIT()
				ENDIF
				
				.cAttach = lcFiletoSaveAs

				* added for Adam
				lcDetailBasisFile = "F:\SHOP\AllocationReports\DIV 02 03 55 Detail " + lcReportPeriod + ".XLS"
				SELECT CURORDHDRPRE
				COPY TO (lcDetailBasisFile) XL5 FOR INLIST(DIVISION,'02','03','55')

				.cAttach = .cAttach + "," + lcDetailBasisFile
				
				
				
				*.CreateRowsMaintType()
				
				
				
				
				*******************************************************************************************************************
				.TrackProgress('Shop Div 02, 03 & 55 Costs process ended normally.', LOGIT+SENDIT)

			CATCH TO loError

				.TrackProgress('There was an error.',LOGIT+SENDIT)
				.TrackProgress('The Error # is: ' + TRANSFORM(loError.ERRORNO), LOGIT+SENDIT)
				.TrackProgress('The Error Message is: ' + TRANSFORM(loError.MESSAGE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred in Program: ' + TRANSFORM(loError.PROCEDURE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred at line #: ' + TRANSFORM(loError.LINENO), LOGIT+SENDIT)
				lnNumberOfErrors = lnNumberOfErrors + 1
				.cSendTo = 'mbennett@fmiint.com'

				IF TYPE('oExcel') = 'O' AND NOT ISNULL(oExcel) THEN
					oExcel.QUIT()
				ENDIF

			ENDTRY

			WAIT CLEAR
			***************** INTERNAL email results ******************************
			.TrackProgress('About to send status email.',LOGIT+SENDIT)
			.TrackProgress('The logfile is: ' + .cLogFile, LOGIT+SENDIT)
			.TrackProgress('Shop Div 02, 03 & 55 Costs process started: ' + .cStartTime, LOGIT+SENDIT)
			.TrackProgress('Shop Div 02, 03 & 55 Costs process finished: ' + TTOC(DATETIME()), LOGIT+SENDIT)

			IF .lSendInternalEmailIsOn THEN
				DO FORM dartmail2 WITH .cSendTo,.cFrom,.cSubject,.cCC,.cAttach,.cBodyText,"A"
				.TrackProgress('Sent status email.',LOGIT)
			ELSE
				.TrackProgress('Did not send status email: THIS.lSendInternalEmailIsOn = FALSE.',LOGIT+SENDIT)
			ENDIF

		ENDWITH
		RETURN
	ENDFUNC && main


	FUNCTION CreateRowsOneDate
		LPARAMETERS tnRow, tcDivision
		* Adam wants if to be each Sunday, not Friday, 3/3/2015
		* This version of the report assumes it is run on Sunday, and just reports on that Sunday.
		LOCAL lnRow, lcRow, lnInside, lnOutside, lnTotal, lnOutPercent, lcDivision, ldDate, lnInPercent

		ldDate = .dToday  && pdStartDate
		lnRow = tnRow

*!*			DO WHILE ldDate <= pdEndDate

			*IF DOW(ldDate,1) = 1 THEN
				*it's a Friday, query for that date - NO: SUNDAY
				
				lnRow = lnRow + 1
				lcRow = ALLTRIM(STR(lnRow))
				
*!*		SET STEP ON 

				* TRAILERS
				SELECT CURORDHDRPRE
				SUM (TOTLCOST + TOTPCOST) TO lnInside FOR BETWEEN(crdate, pdStartDate, pdEndDate) AND DIVISION = tcDivision AND TYPE = 'TRAILER'
				SUM (OUTSCOST) TO lnOutside FOR BETWEEN(crdate, pdStartDate, pdEndDate) AND DIVISION = tcDivision AND TYPE = 'TRAILER'
				lnTotal = lnInside + lnOutside
				IF lnTotal > 0 THEN
					lnOutPercent = lnOutside / lnTotal
					lnInPercent = lnInside / lnTotal
				ELSE
					lnOutPercent = 0
					lnInPercent = 0
				ENDIF
				oWorksheet.RANGE("A"+lcRow).VALUE = CDOW(pdEndDate)
				oWorksheet.RANGE("B"+lcRow).VALUE = pdEndDate
				oWorksheet.RANGE("C"+lcRow).VALUE = "'" + tcDivision
				
				oWorksheet.RANGE("D"+lcRow).VALUE = lnInside
				oWorksheet.RANGE("E"+lcRow).VALUE = lnOutside
				oWorksheet.RANGE("F"+lcRow).VALUE = lnTotal
				
				oWorksheet.RANGE("G"+lcRow).VALUE = lnInPercent 
				oWorksheet.RANGE("G"+lcRow).NumberFormat = "##0.00%"
				oWorksheet.RANGE("H"+lcRow).VALUE = lnOutPercent
				oWorksheet.RANGE("H"+lcRow).NumberFormat = "##0.00%"

				* TRUCKS
				SELECT CURORDHDRPRE
				SUM (TOTLCOST + TOTPCOST) TO lnInside FOR BETWEEN(crdate, pdStartDate, pdEndDate) AND DIVISION = tcDivision AND TYPE = 'TRUCK  '
				SUM (OUTSCOST) TO lnOutside FOR BETWEEN(crdate, pdStartDate, pdEndDate) AND DIVISION = tcDivision AND TYPE = 'TRUCK  '
				lnTotal = lnInside + lnOutside
				IF lnTotal > 0 THEN
					lnOutPercent = lnOutside / lnTotal
					lnInPercent = lnInside / lnTotal
				ELSE
					lnOutPercent = 0
					lnInPercent = 0
				ENDIF
				
				*oWorksheet.RANGE("A"+lcRow).VALUE = CDOW(pdEndDate)
				*oWorksheet.RANGE("B"+lcRow).VALUE = pdEndDate
				*oWorksheet.RANGE("C"+lcRow).VALUE = "'" + tcDivision
				
				oWorksheet.RANGE("J"+lcRow).VALUE = lnInside
				oWorksheet.RANGE("K"+lcRow).VALUE = lnOutside
				oWorksheet.RANGE("L"+lcRow).VALUE = lnTotal
				
				oWorksheet.RANGE("M"+lcRow).VALUE = lnInPercent 
				oWorksheet.RANGE("M"+lcRow).NumberFormat = "##0.00%"
				oWorksheet.RANGE("N"+lcRow).VALUE = lnOutPercent
				oWorksheet.RANGE("N"+lcRow).NumberFormat = "##0.00%"

				* CHASSIS
				SELECT CURORDHDRPRE
				SUM (TOTLCOST + TOTPCOST) TO lnInside FOR BETWEEN(crdate, pdStartDate, pdEndDate) AND DIVISION = tcDivision AND TYPE = 'CHASSIS'
				SUM (OUTSCOST) TO lnOutside FOR BETWEEN(crdate, pdStartDate, pdEndDate) AND DIVISION = tcDivision AND TYPE = 'CHASSIS'
				lnTotal = lnInside + lnOutside
				IF lnTotal > 0 THEN
					lnOutPercent = lnOutside / lnTotal
					lnInPercent = lnInside / lnTotal
				ELSE
					lnOutPercent = 0
					lnInPercent = 0
				ENDIF
				
				*oWorksheet.RANGE("A"+lcRow).VALUE = CDOW(pdEndDate)
				*oWorksheet.RANGE("B"+lcRow).VALUE = pdEndDate
				*oWorksheet.RANGE("C"+lcRow).VALUE = "'" + tcDivision
				
				oWorksheet.RANGE("P"+lcRow).VALUE = lnInside
				oWorksheet.RANGE("Q"+lcRow).VALUE = lnOutside
				oWorksheet.RANGE("R"+lcRow).VALUE = lnTotal
				
				oWorksheet.RANGE("S"+lcRow).VALUE = lnInPercent 
				oWorksheet.RANGE("S"+lcRow).NumberFormat = "##0.00%"
				oWorksheet.RANGE("T"+lcRow).VALUE = lnOutPercent
				oWorksheet.RANGE("T"+lcRow).NumberFormat = "##0.00%"
									
				* total trucks + trailers + chassis requested by Fran
				oWorksheet.RANGE("V"+lcRow).VALUE = "=SUM(D" + lcRow + ",J" + lcRow + ",P" + lcRow + ")"
				oWorksheet.RANGE("W"+lcRow).VALUE = "=SUM(E" + lcRow + ",K" + lcRow + ",Q" + lcRow + ")"
				oWorksheet.RANGE("X"+lcRow).VALUE = "=SUM(F" + lcRow + ",L" + lcRow + ",R" + lcRow + ")"
				
				lnInside = oWorksheet.RANGE("V"+lcRow).VALUE
				lnOutside = oWorksheet.RANGE("W"+lcRow).VALUE				
				lnTotal = oWorksheet.RANGE("X"+lcRow).VALUE
				IF lnTotal > 0 THEN
					lnOutPercent = lnOutside / lnTotal
					lnInPercent = lnInside / lnTotal
				ELSE
					lnOutPercent = 0
					lnInPercent = 0
				ENDIF
				oWorksheet.RANGE("Y"+lcRow).VALUE = lnInPercent
				oWorksheet.RANGE("Y"+lcRow).NumberFormat = "##0.00%"
				oWorksheet.RANGE("Z"+lcRow).VALUE = lnOutPercent
				oWorksheet.RANGE("Z"+lcRow).NumberFormat = "##0.00%"
				

*!*				ENDIF && DOW(ldDate,1) = 6

*!*				ldDate = ldDate + 1

*!*			ENDDO

		RETURN lnRow

	ENDFUNC  && CreateRowsOneDate


	FUNCTION CreateRowsMaintType
		*LPARAMETERS tnRow, tcDivision
		* loop through the Fridays in the month so far, running a query and populating a spreadsheet row for each
		* Adam wants if to be each Sunday, not Friday, 3/3/2015
		LOCAL lnRow, lcRow, lnInside, lnOutside, lnTotal, lnOutPercent, lcDivision, ldDate, lnInPercent

		ldDate = .dToday  && pdStartDate
		*lnRow = tnRow
		
		
		IF USED('CURORDHDRPRE2') THEN
			USE IN CURORDHDRPRE2
		ENDIF
		
		IF USED('CURORDHDRPRE3') THEN
			USE IN CURORDHDRPRE3
		ENDIF
		
*!*			SELECT * FROM CURORDHDRPRE INTO CURSOR CURORDHDRPRE2 WHERE BETWEEN(crdate, pdStartDate, ldDate)
*!*			SELECT CURORDHDRPRE2
*!*			BROWSE
		
		SELECT A.*, B.MAINTDESC ;
		FROM CURORDHDRPRE A ;
		LEFT OUTER JOIN mainttyp B ;
		ON B.MAINTTYPE = A.MAINTTYPE ;
		INTO CURSOR CURORDHDRPRE2 ;
		WHERE BETWEEN(crdate, pdStartDate, pdEndDate) ;
		AND DIVISION IN ('02','03','55') ;
		ORDER BY A.CRDATE

*!*			SELECT CURORDHDRPRE2
*!*			BROWSE
		
		SELECT DIVISION, TYPE, MAINTDESC, ;
			SUM(TOTLCOST + TOTPCOST) AS INCOST, ;
			SUM(OUTSCOST) AS OUTCOST, ;
			SUM(TOTLCOST + TOTPCOST + OUTSCOST) AS TOTCOST, ;
			000.00 AS INPCT, ;
			000.00 AS OUTPCT ;
		FROM CURORDHDRPRE2 ;
		INTO CURSOR CURORDHDRPRE3 ;
		GROUP BY 1,2,3 ;
		ORDER BY 1,2,3 ;
		READWRITE
		

		SELECT CURORDHDRPRE3
		SCAN
			lnTotal = CURORDHDRPRE3.TOTCOST
			lnInside = CURORDHDRPRE3.INCOST
			lnOutside = CURORDHDRPRE3.OUTCOST
			
			IF  lnTotal > 0 THEN
				lnOutPercent = lnOutside / lnTotal
				lnInPercent = lnInside / lnTotal
			ELSE
				lnOutPercent = 0
				lnInPercent = 0
			ENDIF
			
			REPLACE CURORDHDRPRE3.INPCT WITH lnInPercent, CURORDHDRPRE3.OUTPCT WITH lnOutPercent IN CURORDHDRPRE3
		ENDSCAN
		
*!*			SELECT CURORDHDRPRE3
*!*			BROWSE
		SELECT CURORDHDRPRE3
		COPY TO C:\A\DIV020355_BY_MAINTTYPE.XLS XL5
		

*!*			DO WHILE ldDate <= pdEndDate

			*IF DOW(ldDate,1) = 1 THEN
				*it's a Friday, query for that date - NO: SUNDAY
				
*!*					lnRow = lnRow + 1
*!*					lcRow = ALLTRIM(STR(lnRow))

*!*					* TRAILERS
*!*					SELECT CURORDHDRPRE
*!*					SUM (TOTLCOST + TOTPCOST) TO lnInside FOR BETWEEN(crdate, pdStartDate, ldDate) AND DIVISION = tcDivision AND TYPE = 'TRAILER'
*!*					SUM (OUTSCOST) TO lnOutside FOR BETWEEN(crdate, pdStartDate, ldDate) AND DIVISION = tcDivision AND TYPE = 'TRAILER'
*!*					lnTotal = lnInside + lnOutside
*!*					IF lnTotal > 0 THEN
*!*						lnOutPercent = lnOutside / lnTotal
*!*						lnInPercent = lnInside / lnTotal
*!*					ELSE
*!*						lnOutPercent = 0
*!*						lnInPercent = 0
*!*					ENDIF
*!*					oWorksheet.RANGE("A"+lcRow).VALUE = CDOW(ldDate)
*!*					oWorksheet.RANGE("B"+lcRow).VALUE = ldDate
*!*					oWorksheet.RANGE("C"+lcRow).VALUE = "'" + tcDivision
*!*					
*!*					oWorksheet.RANGE("D"+lcRow).VALUE = lnInside
*!*					oWorksheet.RANGE("E"+lcRow).VALUE = lnOutside
*!*					oWorksheet.RANGE("F"+lcRow).VALUE = lnTotal
*!*					
*!*					oWorksheet.RANGE("G"+lcRow).VALUE = lnInPercent 
*!*					oWorksheet.RANGE("G"+lcRow).NumberFormat = "##0.00%"
*!*					oWorksheet.RANGE("H"+lcRow).VALUE = lnOutPercent
*!*					oWorksheet.RANGE("H"+lcRow).NumberFormat = "##0.00%"

*!*					* TRUCKS
*!*					SELECT CURORDHDRPRE
*!*					SUM (TOTLCOST + TOTPCOST) TO lnInside FOR BETWEEN(crdate, pdStartDate, ldDate) AND DIVISION = tcDivision AND TYPE = 'TRUCK  '
*!*					SUM (OUTSCOST) TO lnOutside FOR BETWEEN(crdate, pdStartDate, ldDate) AND DIVISION = tcDivision AND TYPE = 'TRUCK  '
*!*					lnTotal = lnInside + lnOutside
*!*					IF lnTotal > 0 THEN
*!*						lnOutPercent = lnOutside / lnTotal
*!*						lnInPercent = lnInside / lnTotal
*!*					ELSE
*!*						lnOutPercent = 0
*!*						lnInPercent = 0
*!*					ENDIF
*!*					
*!*					*oWorksheet.RANGE("A"+lcRow).VALUE = CDOW(ldDate)
*!*					*oWorksheet.RANGE("B"+lcRow).VALUE = ldDate
*!*					*oWorksheet.RANGE("C"+lcRow).VALUE = "'" + tcDivision
*!*					
*!*					oWorksheet.RANGE("J"+lcRow).VALUE = lnInside
*!*					oWorksheet.RANGE("K"+lcRow).VALUE = lnOutside
*!*					oWorksheet.RANGE("L"+lcRow).VALUE = lnTotal
*!*					
*!*					oWorksheet.RANGE("M"+lcRow).VALUE = lnInPercent 
*!*					oWorksheet.RANGE("M"+lcRow).NumberFormat = "##0.00%"
*!*					oWorksheet.RANGE("N"+lcRow).VALUE = lnOutPercent
*!*					oWorksheet.RANGE("N"+lcRow).NumberFormat = "##0.00%"

*!*					* CHASSIS
*!*					SELECT CURORDHDRPRE
*!*					SUM (TOTLCOST + TOTPCOST) TO lnInside FOR BETWEEN(crdate, pdStartDate, ldDate) AND DIVISION = tcDivision AND TYPE = 'CHASSIS'
*!*					SUM (OUTSCOST) TO lnOutside FOR BETWEEN(crdate, pdStartDate, ldDate) AND DIVISION = tcDivision AND TYPE = 'CHASSIS'
*!*					lnTotal = lnInside + lnOutside
*!*					IF lnTotal > 0 THEN
*!*						lnOutPercent = lnOutside / lnTotal
*!*						lnInPercent = lnInside / lnTotal
*!*					ELSE
*!*						lnOutPercent = 0
*!*						lnInPercent = 0
*!*					ENDIF
*!*					
*!*					*oWorksheet.RANGE("A"+lcRow).VALUE = CDOW(ldDate)
*!*					*oWorksheet.RANGE("B"+lcRow).VALUE = ldDate
*!*					*oWorksheet.RANGE("C"+lcRow).VALUE = "'" + tcDivision
*!*					
*!*					oWorksheet.RANGE("P"+lcRow).VALUE = lnInside
*!*					oWorksheet.RANGE("Q"+lcRow).VALUE = lnOutside
*!*					oWorksheet.RANGE("R"+lcRow).VALUE = lnTotal
*!*					
*!*					oWorksheet.RANGE("S"+lcRow).VALUE = lnInPercent 
*!*					oWorksheet.RANGE("S"+lcRow).NumberFormat = "##0.00%"
*!*					oWorksheet.RANGE("T"+lcRow).VALUE = lnOutPercent
*!*					oWorksheet.RANGE("T"+lcRow).NumberFormat = "##0.00%"
*!*										
*!*					* total trucks + trailers + chassis requested by Fran
*!*					oWorksheet.RANGE("V"+lcRow).VALUE = "=SUM(D" + lcRow + ",J" + lcRow + ",P" + lcRow + ")"
*!*					oWorksheet.RANGE("W"+lcRow).VALUE = "=SUM(E" + lcRow + ",K" + lcRow + ",Q" + lcRow + ")"
*!*					oWorksheet.RANGE("X"+lcRow).VALUE = "=SUM(F" + lcRow + ",L" + lcRow + ",R" + lcRow + ")"
*!*					
*!*					lnInside = oWorksheet.RANGE("V"+lcRow).VALUE
*!*					lnOutside = oWorksheet.RANGE("W"+lcRow).VALUE				
*!*					lnTotal = oWorksheet.RANGE("X"+lcRow).VALUE
*!*					IF lnTotal > 0 THEN
*!*						lnOutPercent = lnOutside / lnTotal
*!*						lnInPercent = lnInside / lnTotal
*!*					ELSE
*!*						lnOutPercent = 0
*!*						lnInPercent = 0
*!*					ENDIF
*!*					oWorksheet.RANGE("Y"+lcRow).VALUE = lnInPercent
*!*					oWorksheet.RANGE("Y"+lcRow).NumberFormat = "##0.00%"
*!*					oWorksheet.RANGE("Z"+lcRow).VALUE = lnOutPercent
*!*					oWorksheet.RANGE("Z"+lcRow).NumberFormat = "##0.00%"
				

*!*				ENDIF && DOW(ldDate,1) = 6

*!*				ldDate = ldDate + 1

*!*			ENDDO

		RETURN lnRow

	ENDFUNC  && CreateRowsMaintType


*!*		FUNCTION CreateRows
*!*			LPARAMETERS tnRow, tcDivision
*!*			* loop through the Fridays in the month so far, running a query and populating a spreadsheet row for each
*!*			* Adam wants if to be each Sunday, not Friday, 3/3/2015
*!*			LOCAL lnRow, lcRow, lnInside, lnOutside, lnTotal, lnOutPercent, lcDivision, ldDate, lnInPercent

*!*			ldDate = pdStartDate
*!*			lnRow = tnRow

*!*			DO WHILE ldDate <= pdEndDate

*!*				*IF DOW(ldDate,1) = 6 THEN
*!*				IF DOW(ldDate,1) = 1 THEN
*!*					*it's a Friday, query for that date - NO: SUNDAY
*!*					lnRow = lnRow + 1
*!*					lcRow = ALLTRIM(STR(lnRow))

*!*					* TRAILERS
*!*					SELECT CURORDHDRPRE
*!*					SUM (TOTLCOST + TOTPCOST) TO lnInside FOR BETWEEN(crdate, pdStartDate, ldDate) AND DIVISION = tcDivision AND TYPE = 'TRAILER'
*!*					SUM (OUTSCOST) TO lnOutside FOR BETWEEN(crdate, pdStartDate, ldDate) AND DIVISION = tcDivision AND TYPE = 'TRAILER'
*!*					lnTotal = lnInside + lnOutside
*!*					IF lnTotal > 0 THEN
*!*						lnOutPercent = lnOutside / lnTotal
*!*						lnInPercent = lnInside / lnTotal
*!*					ELSE
*!*						lnOutPercent = 0
*!*						lnInPercent = 0
*!*					ENDIF
*!*					oWorksheet.RANGE("A"+lcRow).VALUE = CDOW(ldDate)
*!*					oWorksheet.RANGE("B"+lcRow).VALUE = ldDate
*!*					oWorksheet.RANGE("C"+lcRow).VALUE = "'" + tcDivision
*!*					
*!*					oWorksheet.RANGE("D"+lcRow).VALUE = lnInside
*!*					oWorksheet.RANGE("E"+lcRow).VALUE = lnOutside
*!*					oWorksheet.RANGE("F"+lcRow).VALUE = lnTotal
*!*					
*!*					oWorksheet.RANGE("G"+lcRow).VALUE = lnInPercent 
*!*					oWorksheet.RANGE("G"+lcRow).NumberFormat = "##0.00%"
*!*					oWorksheet.RANGE("H"+lcRow).VALUE = lnOutPercent
*!*					oWorksheet.RANGE("H"+lcRow).NumberFormat = "##0.00%"

*!*					* TRUCKS
*!*					SELECT CURORDHDRPRE
*!*					SUM (TOTLCOST + TOTPCOST) TO lnInside FOR BETWEEN(crdate, pdStartDate, ldDate) AND DIVISION = tcDivision AND TYPE = 'TRUCK  '
*!*					SUM (OUTSCOST) TO lnOutside FOR BETWEEN(crdate, pdStartDate, ldDate) AND DIVISION = tcDivision AND TYPE = 'TRUCK  '
*!*					lnTotal = lnInside + lnOutside
*!*					IF lnTotal > 0 THEN
*!*						lnOutPercent = lnOutside / lnTotal
*!*						lnInPercent = lnInside / lnTotal
*!*					ELSE
*!*						lnOutPercent = 0
*!*						lnInPercent = 0
*!*					ENDIF
*!*					
*!*					*oWorksheet.RANGE("A"+lcRow).VALUE = CDOW(ldDate)
*!*					*oWorksheet.RANGE("B"+lcRow).VALUE = ldDate
*!*					*oWorksheet.RANGE("C"+lcRow).VALUE = "'" + tcDivision
*!*					
*!*					oWorksheet.RANGE("J"+lcRow).VALUE = lnInside
*!*					oWorksheet.RANGE("K"+lcRow).VALUE = lnOutside
*!*					oWorksheet.RANGE("L"+lcRow).VALUE = lnTotal
*!*					
*!*					oWorksheet.RANGE("M"+lcRow).VALUE = lnInPercent 
*!*					oWorksheet.RANGE("M"+lcRow).NumberFormat = "##0.00%"
*!*					oWorksheet.RANGE("N"+lcRow).VALUE = lnOutPercent
*!*					oWorksheet.RANGE("N"+lcRow).NumberFormat = "##0.00%"

*!*					* CHASSIS
*!*					SELECT CURORDHDRPRE
*!*					SUM (TOTLCOST + TOTPCOST) TO lnInside FOR BETWEEN(crdate, pdStartDate, ldDate) AND DIVISION = tcDivision AND TYPE = 'CHASSIS'
*!*					SUM (OUTSCOST) TO lnOutside FOR BETWEEN(crdate, pdStartDate, ldDate) AND DIVISION = tcDivision AND TYPE = 'CHASSIS'
*!*					lnTotal = lnInside + lnOutside
*!*					IF lnTotal > 0 THEN
*!*						lnOutPercent = lnOutside / lnTotal
*!*						lnInPercent = lnInside / lnTotal
*!*					ELSE
*!*						lnOutPercent = 0
*!*						lnInPercent = 0
*!*					ENDIF
*!*					
*!*					*oWorksheet.RANGE("A"+lcRow).VALUE = CDOW(ldDate)
*!*					*oWorksheet.RANGE("B"+lcRow).VALUE = ldDate
*!*					*oWorksheet.RANGE("C"+lcRow).VALUE = "'" + tcDivision
*!*					
*!*					oWorksheet.RANGE("P"+lcRow).VALUE = lnInside
*!*					oWorksheet.RANGE("Q"+lcRow).VALUE = lnOutside
*!*					oWorksheet.RANGE("R"+lcRow).VALUE = lnTotal
*!*					
*!*					oWorksheet.RANGE("S"+lcRow).VALUE = lnInPercent 
*!*					oWorksheet.RANGE("S"+lcRow).NumberFormat = "##0.00%"
*!*					oWorksheet.RANGE("T"+lcRow).VALUE = lnOutPercent
*!*					oWorksheet.RANGE("T"+lcRow).NumberFormat = "##0.00%"
*!*										
*!*					* total trucks + trailers + chassis requested by Fran
*!*					oWorksheet.RANGE("V"+lcRow).VALUE = "=SUM(D" + lcRow + ",J" + lcRow + ",P" + lcRow + ")"
*!*					oWorksheet.RANGE("W"+lcRow).VALUE = "=SUM(E" + lcRow + ",K" + lcRow + ",Q" + lcRow + ")"
*!*					oWorksheet.RANGE("X"+lcRow).VALUE = "=SUM(F" + lcRow + ",L" + lcRow + ",R" + lcRow + ")"
*!*					
*!*					lnInside = oWorksheet.RANGE("V"+lcRow).VALUE
*!*					lnOutside = oWorksheet.RANGE("W"+lcRow).VALUE				
*!*					lnTotal = oWorksheet.RANGE("X"+lcRow).VALUE
*!*					IF lnTotal > 0 THEN
*!*						lnOutPercent = lnOutside / lnTotal
*!*						lnInPercent = lnInside / lnTotal
*!*					ELSE
*!*						lnOutPercent = 0
*!*						lnInPercent = 0
*!*					ENDIF
*!*					oWorksheet.RANGE("Y"+lcRow).VALUE = lnInPercent
*!*					oWorksheet.RANGE("Y"+lcRow).NumberFormat = "##0.00%"
*!*					oWorksheet.RANGE("Z"+lcRow).VALUE = lnOutPercent
*!*					oWorksheet.RANGE("Z"+lcRow).NumberFormat = "##0.00%"
*!*					

*!*				ENDIF && DOW(ldDate,1) = 6

*!*				ldDate = ldDate + 1

*!*			ENDDO

*!*			RETURN lnRow

*!*		ENDFUNC  && CreateRows




	FUNCTION GetHourlyRateByCRDate
		LPARAMETERS tdCRdate
		DO CASE
			CASE tdCRdate < {^2008-09-01}
				lnRate = 60.00
			OTHERWISE
				lnRate = 70.00  
		ENDCASE
		RETURN lnRate
	ENDFUNC


	FUNCTION GetHourlyRateByDivision
		LPARAMETERS tcDivision
		DO CASE
			CASE INLIST(tcDivision,'05','52','56','57','58','71','74')
				lnRate = 60
			OTHERWISE
				lnRate = 60  && the default rate
		ENDCASE
		RETURN lnRate
	ENDFUNC


	PROCEDURE TrackProgress
		* do any combination of Wait Window, writing to logfile, and adding to body of email,
		* based on nFlags parameter.
		LPARAMETERS tcExpression, tnFlags
		WITH THIS
			IF BITAND(tnFlags,LOGIT) = LOGIT THEN
				IF .lLoggingIsOn THEN
					?
					? .cProcessName + "  " + TTOC(DATETIME()) + ": " + tcExpression
				ENDIF
			ENDIF
			IF .lWaitWindowIsOn THEN
				IF BITAND(tnFlags,NOWAITIT) = NOWAITIT THEN
					WAIT WINDOW tcExpression NOWAIT
				ENDIF
				IF BITAND(tnFlags,WAITIT) = WAITIT THEN
					WAIT WINDOW tcExpression TIMEOUT .nWaitWindowTimeout
				ENDIF
			ENDIF
			IF BITAND(tnFlags,SENDIT) = SENDIT THEN
				IF .lSendInternalEmailIsOn THEN
					.cBodyText = .cBodyText + tcExpression + CRLF + CRLF
				ENDIF
			ENDIF
		ENDWITH
	ENDPROC  &&  TrackProgress


ENDDEFINE
