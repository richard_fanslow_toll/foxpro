* run CitiBank POSITIVE PAY SUMMARY REPORT for Yesterday's auto transactions.
* should be run early morning Tuesday thru Saturday.

* The report should look at translog and provider a high level summary of which companies sent how many transactions for the given date.
* Especially it should alert if an active company was NOT processed.
*
* 4/27/2018 MB: changed fmiint.com to toll email

* Build EXE as F:\BOA\CITIBANKPOSITIVEPAYREPORT.exe
utilsetup("CITIBANKPOSITIVEPAYREPORT")

LOCAL loPositivePayReport
loPositivePayReport = CREATEOBJECT('PositivePayReport')
loPositivePayReport.MAIN()
CLOSE DATABASES ALL

schedupdate()

RETURN

#DEFINE LOGIT 1
#DEFINE WAITIT 2
#DEFINE NOWAITIT 4
#DEFINE SENDIT 8
#DEFINE CRLF CHR(13) + CHR(10)
#DEFINE RETURN_DATA_MANDATORY .F.
#DEFINE RETURN_DATA_NOT_MANDATORY .T.
#DEFINE CSVFORMAT 6
#DEFINE WRITE_ONLY_UNBUFFERED 11
#DEFINE COMMA ","

DEFINE CLASS PositivePayReport AS CUSTOM

   cProcessName = 'PositivePayReport'

   lTestMode = .F.  && .F. FOR NORMAL OPERATION, .T. TO SUPPRESS ALL BUT DEVELOPER EMAILS AND CHANGE INPUT TABLE, ETC.

   lAutoYield = .T.

   * date time props
   dToday = DATE()
   *dToday = {^2014-03-08}

   cStartTime = TTOC(DATETIME())
   
   * data table properties
   cTranslogTable = 'F:\BOA\DATA\TRANSLOG'
   cCompaniesTable = 'F:\BOA\DATA\COMPANIES'

   * connection properties
   nSQLHandle = 0

   * wait window properties
   nWaitWindowTimeout = 2
   lWaitWindowIsOn = .T.

   * logfile properties
   lLoggingIsOn = .T.
   cLogFile = 'F:\BOA\LOGFILES\CitiBank_POS_PAY_REPORT_log.txt'

   * INTERNAL email properties
   lSendInternalEmailIsOn = .T.
   cFrom = 'TGF Corporate <fmicorporate@fmiint.com>'
   cSendTo = 'mark.bennett@tollgroup.com'
   cCC = 'pgaidis@fmiint.com'
   *cCC = ''
   cSubject = ''
   cAttach = ''
   cBodyText = ''

   cCOMPUTERNAME = ''
   cUSERNAME = ''


   FUNCTION INIT
      IF NOT DODEFAULT()
         RETURN .F.
      ENDIF
      WITH THIS
         *SET RESOURCE OFF
         CLOSE DATA
         SET CENTURY ON
         SET DATE AMERICAN
         SET HOURS TO 24
         SET ANSI ON
         SET TALK OFF
         SET DELETED ON
         SET CONSOLE OFF
         SET EXCLUSIVE OFF
         SET SAFETY OFF
         SET EXACT OFF
         SET STATUS BAR ON
         SET SYSMENU OFF
         SET ENGINEBEHAVIOR 70
         _VFP.AUTOYIELD = .lAutoYield
         .cCOMPUTERNAME = GETENV("COMPUTERNAME")
         .cUSERNAME = GETENV("USERNAME")
         IF .lTestMode THEN
            .cLogFile = 'F:\BOA\LOGFILES\CitiBank_POS_PAY_REPORT_LOG_TESTMODE.txt'
            .cSendTo = 'mark.bennett@tollgroup.com'
            .cCC = ''
         ENDIF
         .lLoggingIsOn = .lLoggingIsOn AND NOT EMPTY(.cLogFile)
      ENDWITH
   ENDFUNC


   FUNCTION DESTROY
      WITH THIS
         IF .lLoggingIsOn  THEN
            SET ALTERNATE OFF
            SET ALTERNATE TO
         ENDIF
      ENDWITH
      DODEFAULT()
   ENDFUNC


   FUNCTION MAIN
      WITH THIS
         LOCAL lnNumberOfErrors, lcTranslogTable, loError, lcCompaniesTable, ldToday, ldYesterday, lcCompany, lcYesterday
         LOCAL lcTransText
         TRY
            lnNumberOfErrors = 0
            
	         IF .lLoggingIsOn THEN
	            SET ALTERNATE TO (.cLogFile) ADDITIVE
	            SET ALTERNATE ON
	         ENDIF

            .TrackProgress('', LOGIT)
            .TrackProgress('', LOGIT)
            .TrackProgress('=========================================================', LOGIT+SENDIT)
            .TrackProgress('CitiBank POSITIVE PAY REPORT process started....', LOGIT+SENDIT+NOWAITIT)
            .TrackProgress('COMPUTERNAME = ' + .cCOMPUTERNAME, LOGIT+SENDIT)
            .TrackProgress('USERNAME = ' + .cUSERNAME, LOGIT+SENDIT)
            .TrackProgress('=========================================================', LOGIT+SENDIT)
            IF .lTestMode THEN
               .TrackProgress('                        TEST MODE                        ', LOGIT+SENDIT)
               .TrackProgress('=========================================================', LOGIT+SENDIT)
            ENDIF
            .TrackProgress('', LOGIT)
            
            ldToday = .dToday
            ldYesterday = ldToday - 1
            lcYesterday = DTOC(ldYesterday)
            
            .cSubject = 'CitiBank POSITIVE PAY Summary Report for ' + lcYesterday + ' Checks'  
            lcTranslogTable = .cTranslogTable
            lcCompaniesTable = .cCompaniesTable
            
            USE (lcTranslogTable) AGAIN IN 0 ALIAS BOATRANSLOG
            USE (lcCompaniesTable) AGAIN IN 0 ALIAS COMPANIESTABLE
            
            * scan through active companies to get results for yesterdays auto transfers
            * yesterdays auto transfers = those recs in translog which has both 'start' and 'end' = yesterday.
            lcTransText = "COMPANY     TRANSACTION INFORMATION"  + CRLF + ;
                          "--------    ----------------------------------------------------------------" + CRLF
            SELECT COMPANIESTABLE
            SCAN FOR lactive AND lcitibank
            	lcCompany = UPPER(ALLTRIM(COMPANIESTABLE.COMPANY))
            	SELECT BOATRANSLOG
            	LOCATE FOR (UPPER(ALLTRIM(COMPANY)) = lcCompany) AND (STARTDATE = ldYesterday) AND (UPPER(ALLTRIM(sentto)) == 'CITIBANK')
            	IF FOUND() THEN
            		SCAN FOR (UPPER(ALLTRIM(COMPANY)) = lcCompany) AND (STARTDATE = ldYesterday) AND (UPPER(ALLTRIM(sentto)) == 'CITIBANK')
	            		lcTransText = lcTransText + ;
	            			PADL(lcCompany,8," ") + " - # Checks: " + PADL(BOATRANSLOG.CHECKS,5,"0") + ;
	            			"    Made Output File: " + TRANSFORM(BOATRANSLOG.POPULATEOUTPUTFILE) + CRLF + CRLF
            		ENDSCAN
            	ELSE
            		lcTransText = lcTransText + ;
            			PADL(lcCompany,8," ") + " - ** WARNING no transactions found for " + lcYesterday + CRLF + CRLF
            	ENDIF
            ENDSCAN
            
            USE IN BOATRANSLOG
            USE IN COMPANIESTABLE
            
            .cBodyText = lcTransText + CRLF + CRLF + .cBodyText

         CATCH TO loError

            .TrackProgress('There was an error.',LOGIT+SENDIT)
            .TrackProgress('The Error # is: ' + TRANSFORM(loError.ErrorNo), LOGIT+SENDIT)
            .TrackProgress('The Error Message is: ' + TRANSFORM(loError.MESSAGE), LOGIT+SENDIT)
            .TrackProgress('The Error occurred in Program: ' + TRANSFORM(loError.PROCEDURE), LOGIT+SENDIT)
            .TrackProgress('The Error occurred at line #: ' + TRANSFORM(loError.LINENO), LOGIT+SENDIT)
            lnNumberOfErrors = lnNumberOfErrors + 1
            CLOSE DATA

         ENDTRY

         CLOSE DATA
         WAIT CLEAR
         ***************** INTERNAL email results ******************************
         .TrackProgress('About to send status email.',LOGIT)
         .TrackProgress('The logfile is: ' + .cLogFile, SENDIT)
         .TrackProgress('CitiBank POSITIVE PAY REPORT process started: ' + .cStartTime, LOGIT+SENDIT)
         .TrackProgress('CitiBank POSITIVE PAY REPORT process finished: ' + TTOC(DATETIME()), LOGIT+SENDIT)

         IF .lSendInternalEmailIsOn THEN
            DO FORM dartmail2 WITH .cSendTo,.cFrom,.cSubject,.cCC,.cAttach,.cBodyText,"A"
            .TrackProgress('Sent status email.',LOGIT)
         ELSE
            .TrackProgress('Did not send status email: THIS.lSendInternalEmailIsOn = FALSE.',LOGIT)
         ENDIF

      ENDWITH
      RETURN
   ENDFUNC && main


   FUNCTION ExecSQL
      LPARAMETERS tcSQL, tcCursorName, tlNoDataReturnedIsOkay
      LOCAL llRetval, lnResult
      WITH THIS
         * close target cursor if it's open
         IF USED(tcCursorName)
            USE IN (tcCursorName)
         ENDIF
         WAIT WINDOW "Fetching remote data; please wait..." NOWAIT
         lnResult = sqlexec(.nSQLHandle, tcSQL, tcCursorName)
         llRetval = ( lnResult > 0 )
         IF llRetval THEN
            * see if any data came back
            IF NOT tlNoDataReturnedIsOkay THEN
               IF (NOT USED(tcCursorName)) OR EOF(tcCursorName) THEN
                  llRetval = .F.
                  .TrackProgress('No data was returned by this query: ' +CRLF + tcSQL, LOGIT+SENDIT)
                  .cSendTo = 'Mark Bennett <mark.bennett@tollgroup.com>'
               ENDIF
            ENDIF
         ELSE
            .TrackProgress("Result Code [" + TRANSFORM(lnResult) + "] returned by this query: " + tcSQL, LOGIT+SENDIT)
            .cSendTo = 'Mark Bennett <mark.bennett@tollgroup.com>'
         ENDIF
         WAIT CLEAR
         RETURN llRetval
      ENDWITH
   ENDFUNC


   PROCEDURE TrackProgress
      * do any combination of Wait Window, writing to logfile, and adding to body of email,
      * based on nFlags parameter.
      LPARAMETERS tcExpression, tnFlags
      WITH THIS
         IF BITAND(tnFlags,LOGIT) = LOGIT THEN
            IF .lLoggingIsOn THEN
               ?
               ? .cProcessName + "  " + TTOC(DATETIME()) + ": " + tcExpression
            ENDIF
         ENDIF
         IF .lWaitWindowIsOn THEN
            IF BITAND(tnFlags,NOWAITIT) = NOWAITIT THEN
               WAIT WINDOW tcExpression NOWAIT
            ENDIF
            IF BITAND(tnFlags,WAITIT) = WAITIT THEN
               WAIT WINDOW tcExpression TIMEOUT .nWaitWindowTimeout
            ENDIF
         ENDIF
         IF BITAND(tnFlags,SENDIT) = SENDIT THEN
            IF .lSendInternalEmailIsOn THEN
               .cBodyText = .cBodyText + tcExpression + CRLF + CRLF
            ENDIF
         ENDIF
      ENDWITH
   ENDPROC  &&  TrackProgress


ENDDEFINE

