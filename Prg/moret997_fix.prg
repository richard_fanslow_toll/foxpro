*!* Moret OUTBOUND 997 processing
CLOSE DATABASES ALL
do m:\dev\prg\_setvars with .t.
CD "F:\FTPUSERS\MORET\hold997"
*cOutDir = "F:\FTPUSERS\MORET\OUT\"
cOutDir = "F:\FTPUSERS\MORETGROUP\OUT\"
len1 = 0
len1 = ADIR(ary1,'FMI997.edi')
IF len1 = 0
	WAIT WINDOW "No 997s to process"
	CANCEL
	RETURN
ENDIF
assert .f. message "At start of Moret997_fix process"
CREATE CURSOR temp1 (f1 c(254))
SELECT temp1
APPEND FROM "F:\FTPUSERS\MORET\hold997\fmi997.edi" TYPE SDF
COUNT TO nISA FOR f1 = "ISA"
WAIT WINDOW "There are "+TRANSFORM(nISA)+" ISA Segments to break out" TIMEOUT 2
LOCATE
*BROWSE TIMEOUT 3

SCAN FOR INLIST(f1,"ISA","GS")
	cF1 = TRIM(f1)
	IF f1 = "ISA"
		nPos = AT(SUBSTR(f1,AT("U*",f1)+2),f1)
		f1a = STUFF(f1,nPos,5,"00401")
*WAIT WINDOW f1a TIMEOUT 2
		REPLACE f1 WITH f1a
	ENDIF
	IF f1 = "GS"
		nPos = AT(SUBSTR(f1,AT("X*",f1)+2),f1)
		f1a = STUFF(f1,nPos,6,"004010")
*WAIT WINDOW f1a TIMEOUT 2
		REPLACE f1 WITH f1a
	ENDIF
ENDSCAN

LOCATE
cString = ""
SCAN
	cString = cString + ALLTRIM(f1)+CHR(13)
	IF f1 = "IEA"
		cFilenameout = cOutDir+"fmi997_"+TTOC(DATETIME(),1)+".edi"
		STRTOFILE(cString,cFilenameout)
		COPY FILE [&cFilenameout] TO ("f:\ftpusers\moret\997Archive\"+JUSTFNAME(cFilenameOut))
		WAIT WINDOW "" TIMEOUT 2
		cstring = ""
	ENDIF
ENDSCAN

*BROWSE
DELETE FILE "F:\FTPUSERS\MORET\hold997\fmi997.edi"
nISA = 0
CLOSE DATABASES ALL
RETURN
