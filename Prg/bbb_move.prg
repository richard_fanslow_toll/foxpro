* BBB_MOVE	
*
* copies Bed Bath transaction files from Mira Loma folder to the F: drive where they will be used to create 210 edi invoice files.
*
*
* build exe as F:\3PL\bbb_move.exe
*
* converted to use SendMail_au 12/20/2017 MB
*
* Fixed bug in the CATCH 3/15/2018 MB - removed reference to xfile which was sometimes undefined.
*
* 4/24/2018 MB: changed fmiint.com to Toll email.


PARAMETERS whichfolder

utilsetup("BBB_MOVE")

*whichfolder = "SDG"

LOCAL lcMessage
lcMessage = ''

WAIT WINDOW AT 10,10 "Now moving files for "+whichfolder TIMEOUT 1

IF whichfolder = "SDG"
	cDirIn = "\\MLS1\shared\sdbbbedi\"
	cDirOut = ("F:\FTPUSERS\BEDBATH\DATAINSDG\")
ELSE
	cDirIn = "\\MLS1\shared\bbbedi\"
	cDirOut = ("F:\FTPUSERS\BEDBATH\DATAIN\")
ENDIF

DO M:\DEV\PRG\_SETVARS WITH .T.
PUBLIC cfilename,cfileoutname,NormalExit

SET SAFETY OFF

TRY
	lTesting = .F.
	lOverrideBusy = .T.
	NormalExit = .F.

	CD &cDirIn
	
	nFound = ADIR(ary1,'*.*')
	IF nFound = 0
		*CLOSE DATABASES ALL
		WAIT WINDOW "No BBB files found to process...exiting" TIMEOUT 2
		lcMessage = "No BBB files found for " + whichfolder
		NormalExit = .T.
		THROW
	ELSE
		lcMessage = "# of files moved = " + TRANSFORM(nFound)
	ENDIF
	

	FOR i = 1 TO nFound

		cfilename = ary1[i,1]
		WAIT WINDOW "Processing "+cfilename+",  Moving file "+ALLTRIM(TRANSFORM(i))+"  out of "+ALLTRIM(TRANSFORM(nFound))+" Files........ to "+cDirOut   NOWAIT

		xfile = (cDirIn+cfilename)
		*		cString = FILETOSTR(xfile)

		*		WAIT WINDOW "File will be moved to folder: "+cDirOut NOWAIT
		cfileoutname = (ALLTRIM(cDirOut)+ALLTRIM(cfilename))
		IF !FILE(xfile)
			WAIT WINDOW AT 10,10 "file not found: "+xfile
			LOOP
		ENDIF
		COPY FILE(xfile) TO &cfileoutname 
		IF FILE(cfileoutname)
			IF FILE(xfile)
				DELETE FILE(xfile)
			ENDIF
		ENDIF
	ENDFOR

	WAIT WINDOW "BBB file move process complete" TIMEOUT 1

	NormalExit = .T.

	tsubject =" BBB File Move for " + whichfolder + " for " + TTOC(DATETIME())
	tsendto = 'mark.bennett@tollgroup.com'
	tcc = ''
	tmessage = "# of files moved = " + TRANSFORM(nFound)
	tattach  = ""
	tFrom = 'No Reply <noreply@tollgroup.com>'
	tBcc = ''
	*DO FORM M:\DEV\frm\dartmail2 WITH tsendto,tFrom,tsubject,tcc,tattach,tmessage,"A"
	DO FORM M:\DEV\frm\SendMail_au WITH tsendto, tFrom, tsubject, tcc, tattach, tmessage, "A", tBcc

	*!*		schedupdate()
	*!*		CLOSE DATABASES ALL

CATCH TO oErr
	IF NormalExit
		tsubject =" BBB File Move for " + whichfolder + " for " + TTOC(DATETIME())
		tmessage = lcMessage
		tsendto = 'mark.bennett@tollgroup.com'
		tcc = ''
		tattach  = ""
		tFrom = 'No Reply <noreply@tollgroup.com>'
		tBcc = ''
		*DO FORM M:\DEV\frm\dartmail2 WITH tsendto,tFrom,tsubject,tcc,tattach,tmessage,"A"
	  Do sendmail_au With tsendto,"noreply@tollgroup.com",tsubject,tcc,tattach,tmessage,""
             **Parameter lcTO, lcFrom,                lcSubject,lcCC,lcAttach,lcBody,lcBCC
	ELSE

		tsubject =" BBB File Move Error ("+TRANSFORM(oErr.ERRORNO)+") at "+TTOC(DATETIME())
		tattach  = ""
		tsendto = 'mark.bennett@tollgroup.com'
		tcc = ''
		tBcc = ''
		tmessage = "bbb File Move Error..... Please fix me........!"
		lcSourceMachine = SYS(0)
		lcSourceProgram = SYS(16)

		tmessage =tmessage+ "Try/CATCH Exeception Message:"+CHR(13)+;
			[  Message: ] + oErr.MESSAGE +CHR(13)+;
			[  Procedure: ] + oErr.PROCEDURE +CHR(13)+;
			[  LineNo: ] + STR(oErr.LINENO) +CHR(13)+;
			[  Error: ] + STR(oErr.ERRORNO) +CHR(13)+;
			[  Details: ] + oErr.DETAILS +CHR(13)+;
			[  StackLevel: ] + STR(oErr.STACKLEVEL) +CHR(13)+;
			[  LineContents: ] + oErr.LINECONTENTS+CHR(13)+;
			[  UserValue: ] + oErr.USERVALUE+CHR(13)+;
			[  Computer:  ] +lcSourceMachine+CHR(13)+;
			[  Program:   ] +lcSourceProgram

		tattach  = ""
		
    	Do sendmail_au With tsendto,"noreply@tollgroup.com",tsubject,tcc,tattach,tmessage,""

	ENDIF
ENDTRY
schedupdate(whichfolder)
CLOSE DATABASES ALL
RETURN
