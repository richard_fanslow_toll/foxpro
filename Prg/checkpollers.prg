runack("CHECKPOLLERS")

openclose()
_SCREEN.WINDOWSTATE=1
DO m:\dev\prg\_setvars
PUBLIC lnProcID,m0,m1,m2,cPollerListx,cFilenamex1,lDoMail
lTesting1 = .f.
lnProcID = 0
lDoMail = .F.
m0 = 256
m1 = 65536
m2 = 16777216

utilsetup("CHECKPOLLERS")

lReturn = .T.
SELECT 0
USE F:\3pl\DATA\edipollertrip ALIAS edi1 SHARED
ASSERT .f.
FOR i = 1 TO 10
	cField = FIELD(i)
	IF &cField = .T. && OR (cField = "Ariat")
		lReturn = .F.
		EXIT
	ENDIF
ENDFOR

IF lReturn
	schedupdate()
	openclose()
	RETURN
ENDIF

createcursor()  && procentry32
procID("x")

SELECT procentry32
DELETE FOR !("EXE"$UPPER(szExeFile))
LOCATE
IF lTesting1
	BROWSE
	RETURN
ENDIF

FOR i = 1 TO 10
	DO CASE
		CASE i = 1
			cFilenamex1 = "edipoller_in.exe"
			cPoller = "main_in"
		CASE i = 2
			cFilenamex1 = "edipoller_out.exe"
			cPoller = "main_out"
		CASE i = 3
			cFilenamex1 = "edipoller_aux-edi.exe"
			cPoller = "aux_edi"
		CASE i = 4
			cFilenamex1 = "edipoller_moret.exe"
			cPoller = "moret"
		CASE i = 5
			cFilenamex1 = "edipoller_nanjing.exe"
			cPoller = "nanjing"
		CASE i = 6
			cFilenamex1 = "edipoller_marcjacobs.exe"
			cPoller = "marcjacobs"
		CASE i = 7
			cFilenamex1 = "edipoller_ariat.exe"
			cPoller = "ariat"
		CASE i = 8
			cFilenamex1 = "edipoller_bcny.exe"
			cPoller = "bcny"
		CASE i = 9
			cFilenamex1 = "edipoller_2xu.exe"
			cPoller = "_2xu"
		CASE i = 10
			cFilenamex1 = "edipoller_merkury.exe"
			cPoller = "merkury"
*!*			CASE i = 11
*!*				cFilenamex1 = "edipoller_billabong.exe"
*!*				cPoller = "billabong"
	ENDCASE

	cPoller1 = "edi1."+cPoller
	WAIT WINDOW cPoller1 TIMEOUT 1
	SELECT edi1
	ASSERT .F.
	IF &cPoller1 = .F.
		IF !INLIST(DOW(DATE()),1,7)
			IF (BETWEEN(TIME(),"08:00:00","20:00:00"))
				WAIT WINDOW "Poller "+cPoller1+" is ok...looping" TIMEOUT 2
				LOOP
			ENDIF
		ENDIF
	ENDIF
	REPLACE &cPoller1 WITH .F.

	SELECT procentry32
	LOCATE FOR cFilenamex1$szExeFile
	IF !FOUND()
		lDoMail = .T.
		cPollerListx = IIF(EMPTY(cPollerListx),cFilenamex1,cPollerListx+CHR(13)+cFilenamex1)
		cWaitStr = "File "+cFilenamex1+" not found in running processes"+CHR(13)+"Restarting..."
		WAIT WINDOW cWaitStr TIMEOUT 2
		cFiledo = ("f:\3pl\proj\"+cFilenamex1)
		! /N &cFiledo
	ENDIF
ENDFOR
IF lDoMail
	mailout()
ELSE
	WAIT WINDOW "No pollers were in error" TIMEOUT 1
ENDIF

schedupdate()
openclose()

RELEASE ALL
RETURN


******************************************************************************************************************************
FUNCTION procID(tcProcessName)
******************************************************************************************************************************
** Function Name : Proc[ess] ID
** Purpose       : Finds the Process ID of the program in ?
** Description   :
** Parameter(s)  : Process (EXE file) name as string (i.e. "AcroRd32.EXE")
** Return        : Process ID if found running as LONG INTEGER, 0 otherwise.
** Side Effect(s): Who knows...
** Notes:        : Created by Lucian Constantin, March 2003, with minor editing/emhancements by Ilya Rabyy on 2004-03-29
******************************************************************************************************************************

	IF TYPE("tcProcessName") # "C"
		RETURN 0
	ENDIF

	LOCAL lcProcessName, lnBufSize, lcProcInfo, lnResult, lnProcID, th32DefaultHeapID
	LOCAL cntUsage, th32ProcessID, th32ModuleID, cntThreads, th32ParentProcessID, pcPriClassBase, dwFlags, szExeFile

	lcProcessName = ALLTRIM(tcProcessName)
	WAIT WINDOW "Checking process: "+lcProcessName TIMEOUT 2

	IF EMPTY(lcProcessName)
		RETURN 0
	ENDIF

&& Declare DLLs needed for the case
	DECLARE INTEGER CreateToolhelp32Snapshot IN Kernel32 INTEGER dwFlags, INTEGER th32ProcessID
	DECLARE INTEGER Process32First IN kernel32 INTEGER hSnapshot, STRING @lpPE
	DECLARE INTEGER Process32Next IN kernel32 INTEGER hSnapshot, STRING @ lpPE
	DECLARE INTEGER CloseHandle IN kernel32 INTEGER hObject

&& Structure used in Process32First() and Process32Next() functions
*!*   typedef struct tagPROCESSENTRY32 {
*!*     DWORD dwSize;         32
*!*     DWORD cntUsage;      32
*!*     DWORD th32ProcessID;      32
*!*     ULONG_PTR th32DefaultHeapID;   32
*!*     DWORD th32ModuleID;      32
*!*     DWORD cntThreads;      32
*!*     DWORD th32ParentProcessID;   32
*!*     LONG pcPriClassBase;      32
*!*     DWORD dwFlags;      32
*!*     TCHAR szExeFile[MAX_PATH];   260+1
*!*   } PROCESSENTRY32, *PPROCESSENTRY32;
&& Therefore, 288+ 261 = 549 - make it 550

&& Obtain a handle to the thread's snapshot
	hSnapshot=CreateToolhelp32Snapshot(0x00000002,0)
* hSnapShot = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0)

	lnBufSize = 550
	lcProcInfo = Num2DWORD(lnBufSize) + REPLICATE(CHR(0), lnBufSize -32)

&& Get the first process
	lnResult = Process32First(hSnapshot, @lcProcInfo)

	lnProcID = 0
&& Cycle through the rest of the processes until the one in ? found (or not...)
	DO WHILE lnResult # 0
&& We will use only the th32ProcessID and szExeFile, but will extract all just in case...
		cntUsage = Buf2DWORD(SUBSTR(lcProcInfo, 5, 4))
		th32ProcessID = Buf2DWORD(SUBSTR(lcProcInfo, 9, 4))
		th32DefaultHeapID = Buf2DWORD(SUBSTR(lcProcInfo, 13, 4))
		th32ModuleID = Buf2DWORD(SUBSTR(lcProcInfo, 17, 4))
		cntThreads = Buf2DWORD(SUBSTR(lcProcInfo, 21, 4))
		th32ParentProcessID = Buf2DWORD(SUBSTR(lcProcInfo, 25, 4))
		pcPriClassBase = Buf2DWORD(SUBSTR(lcProcInfo, 29, 4))
		dwFlags = Buf2DWORD(SUBSTR(lcProcInfo, 33, 4))
		szExeFile = STRTRAN(SUBSTR(lcProcInfo,37), CHR(0), "")
		SELECT procentry32
		APPEND BLANK
		GATHER MEMVAR

&& Check if we have found the process in ?
		IF ATC(lcProcessName, szExeFile) # 0
			lnProcID = th32ProcessID
*			EXIT  && DO WHILE...ENDDO cycle
		ENDIF
&& Reinit "FileName" part in lcProcInfo so on XP file name does not get cluttered
&& and in Win9x & ME could do the "Process32Next" corectly
		lcProcInfo= SUBSTR(lcProcInfo, 1, 36) + REPLICATE(CHR(0), lnBufSize - 36)
		lnResult = Process32Next(hSnapshot, @lcProcInfo)
	ENDDO
	SELECT procentry32
	= CloseHandle(hSnapshot)

	CLEAR DLLS CreateToolhelp32Snapshot, Process32First, Process32Next, CloseHandle

	RETURN lnProcID
ENDFUNC
******************************************************************************************************************************

******************************************************************************************************************************
FUNCTION Buf2DWORD(tcBuffer)
******************************************************************************************************************************
** Function Name : Buf[fer] 2 DWORD
** Purpose       :
** Description   :
** Parameter(s)  : Buffer as a Char*4 string
** Return        : DWORD type imitation as LONG INTEGER
** Side Effect(s):
** Notes:        :
******************************************************************************************************************************
&& RETURN ASC(SUBSTR(lcBuffer, 1, 1)) + ASC(SUBSTR(lcBuffer, 2, 1)) * 2^8 + ASC(SUBSTR(lcBuffer, 3, 1)) * 2^16 + ASC(SUBSTR(lcBuffer, 4,1)) * 2^24
	IF TYPE('tcBuffer') # "C"
		RETURN 0
	ENDIF

	tcBuffer = LEFT(ALLTRIM(tcBuffer), 4)

	LOCAL i, lnRet

	lnRet = 0
	FOR i = 0 TO 3
		lnRet = lnRet + ASC(SUBSTR(tcBuffer, i + 1, 1)) * 2^(8 * i)
	NEXT i

	RETURN lnRet
ENDFUNC
******************************************************************************************************************************

******************************************************************************************************************************
FUNCTION Num2DWORD(tnValue)
******************************************************************************************************************************
** Function Name :
** Purpose       :
** Description   :
** Parameter(s)  :
** Return        :
** Side Effect(s):
** Notes:        :
******************************************************************************************************************************
	LOCAL b0, b1, b2, b3

	b3 = INT(tnValue / m2)
	b2 = INT((tnValue - b3 * m2) / m1)
	b1 = INT((tnValue - b3 * m2 - b2 * m1) / m0)
	b0 = MOD(tnValue, m0)
	RETURN CHR(b0) + CHR(b1) + CHR(b2) + CHR(b3)
ENDFUNC

**********************
PROCEDURE createcursor
**********************
	CREATE CURSOR procentry32 ;
		(szExeFile c(150), ;
		cntUsage c(4), ;
		th32ProcessID c(4), ;
		th32DefaultHeapID c(4), ;
		th32ModuleID c(4), ;
		cntThreads c(4), ;
		th32ParentProcessID c(4), ;
		pcPriClassBase c(4), ;
		dwFlags c(4))
ENDPROC

**********************
PROCEDURE openclose
**********************
	CLEAR ALL
	CLOSE DATABASES ALL
	CLEAR
ENDPROC

**********************
PROCEDURE mailout
**********************
	tFrom ="TGF EDI Operations <toll-edi-ops@tollgroup.com>"
	tsubject= "NOTICE: Poller Restart"
	tmessage = "The following pollers failed and were restarted at "+TTOC(DATETIME())
	tmessage = tmessage +CHR(10)+CHR(10)+cPollerListx
	SELECT 0
	USE F:\3pl\DATA\mailmaster ALIAS mm SHARED 
	LOCATE FOR mm.edi_type = "MISC" AND mm.taskname = "GENERAL"  && Change this task as needed, or create a new one
	tsendto = ALLTRIM(IIF(mm.use_alt,mm.sendtoalt,mm.sendto))
	tcc = ALLTRIM(IIF(mm.use_alt,mm.ccalt,mm.cc))
	USE IN mm
	tattach = " "

	DO FORM m:\dev\frm\dartmail2 WITH tsendto,tFrom,tsubject,tcc,tattach,tmessage,"A"
ENDPROC
