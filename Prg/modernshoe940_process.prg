*!* m:\dev\prg\rowone940_process.prg
cOffice = "C"

CD &lcPath

LogCommentStr = ""

delimchar = cDelimiter
lcTranOpt= cTranslateOption

*lnNum = ADIR(tarray,"*.*")

set step On 
ll940test = lTesting


IF ll940test
  xpath = "f:\ftpusers\modernshoe\940in\test\"
  lnNum = ADIR(tarray,xpath+"*.*")
ELSE
  lnNum = ADIR(tarray,lcpath+"*.*")
ENDIF

SET STEP ON 
IF lnNum = 0
	WAIT WINDOW AT 10,10 "      No "+cCustname+" 940s to import     " TIMEOUT 3
	NormalExit = .T.
	THROW
ENDIF

FOR thisfile = 1  TO lnNum
  IF ll940test
    xfile = xpath+tarray[thisfile,1] && +"."
  ELSE
    xfile = lcpath+tarray[thisfile,1] && +"."
  ENDIF

*	Xfile = lcPath+tarray[thisfile,1] && +"."
	cfilename = LOWER(tarray[thisfile,1])

	nFileSize = VAL(TRANSFORM(tarray(thisfile,2)))
	COPY FILE [&xfile] TO ("F:\ftpusers\modernshoe\940xfer\"+cfilename) && to create 997
	WAIT WINDOW "Importing file: "+cfilename NOWAIT
	DO m:\dev\prg\loadedifile WITH Xfile,delimchar,lcTranOpt,cCustname

	SELECT x856
	LOCATE

	DO create_pt_cursors WITH cUseFolder,lLoadSQL && Added to prep for SQL Server transition
	archivefile  = (lcArchivepath+JUSTFNAME(Xfile))

	lOK = .T.
	DO ("m:\dev\prg\"+cCustname+"940_bkdn")


	IF lOK && If no divide-by-zero error
		DO "m:\dev\prg\all940_import"
	ELSE
		IF !FILE(archivefile)
			COPY FILE &Xfile TO &archivefile
		ENDIF
		IF FILE(Xfile)
			DELETE FILE [&xfile]
		ENDIF
		LOOP
	ENDIF

NEXT thisfile

&& now clean up the 940in archive files, delete for the upto the last 10 days
deletefile(lcArchivepath,IIF(DATE()<{^2016-05-01},20,10))

WAIT CLEAR

WAIT "Entire "+cCustname+" 940 Process Now Complete" WINDOW TIMEOUT 5
NormalExit = .T.
