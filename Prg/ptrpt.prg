parameters xfilename
** 06/15/2015 MB - changed to add style descript to the report per Maria E.

xfilename=iif(empty(xfilename),"",xfilename)
xacctname=acctname(xaccountid)

** MB added 'descrip' field 6/15/15
**added ptprice & country for billabong for web access - 06/07/17
create cursor xrpt ( ;
	xgroup c(20), ;
	ship_ref c(20), ;
	consignee c(35), ;
	address c(30), ;
	name c(30), ;
	csz c(30), ;
	cnee_ref c(25), ;
	dept c(10), ;
	start d, ;
	cancel d, ;
	ship_via c(30), ;
	qty n(6), ;
	weight n(7), ;
	cuft n(8,2), ;
	style c(20), ;
	color c(10), ;
	id c(10), ;
	pack c(10), ;
	descrip c(50), ;
	totqty n(6), ;
	ctnwt n(3), ;
	totwt n(6), ;
	totcube n(8,2), ;
	batch_num c(10), ;
	price n(10,2), ;
	vasstr c(20), ;
	country c(10))

cwhpath = wf(xoffice, xaccountid)
goffice=wf(xoffice, xaccountid,,,,,,.t.)

xsqlexec("select * from pt where mod='"+goffice+"'",,,"wh",,,,.t.,,,.t.)
xsqlexec("select * from ptdet where mod='"+goffice+"'",,,"wh",,,,.t.,,,.t.)
select pt
index on ptid tag ptid
set order to

select pt
scan for accountid = xaccountid and !finis
	scatter memvar fields ship_ref, consignee, name, address, csz, cnee_ref, ship_via, dept, start, cancel, qty, cuft, weight, batch_num

	**for billabong, need to get order type from shipins (PTTYPE) as batch_num is not always correct
	if inlist(xaccountid,&gbillabongaccts)
		xordertype=getmemodata("pt.shipins","PTTYPE")
		if xordertype#"NA"
			m.batch_num=xordertype
		endif	
	endif

	if xaccountid#4143
		m.xgroup=m.ship_ref
	endif

	xlen=at('D/', m.cnee_ref)
	if xlen#0
		m.dept=substr(m.cnee_ref,xlen+2,20-xlen+2)
		m.cnee_ref=substr(m.cnee_ref,1,xlen-1)
	endif

	select ptdet
	scan for ptid=pt.ptid
		scatter memvar fields style, color, id, pack, totqty, totwt, totcube, ctnwt
		do case
		case xaccountid=4143
			m.id=m.style
			m.style=m.cnee_ref
		case inlist(xaccountid,&gbillabongaccts)
			m.price=val(getmemodata("ptdet.printstuff","PRICE"))*m.totqty
			m.country=iif(inlist(pt.country,"US","CA"),pt.country,"OTHER")
		endcase
		insert into xrpt from memvar
	endscan
endscan

* populate style descript IN xrpt. MB 6/15/15
select xrpt
scan
	if upcmastsql(xaccountid, padr(xrpt.style,20))
		replace xrpt.descrip with upcmast.descrip in xrpt
	endif
endscan

if !empty(xfilename)
	select ship_ref as ptnum, cnee_ref as ponum,dept,consignee as shipto,start,cancel,ship_via as carrier,;
			qty as ptqty,style,color as id1,id as id2,descrip,pack,totqty as qty ;
		from xrpt into cursor xtemp
	copy to &xfilename type xls
	use in xtemp

else &&website
	=seek(xoffice,"offices","locale")
	xacctcareof="C/O "+offices.coname
	xacctaddress=upper(offices.address)
	xacctcsz=dispcsz(offices.city, offices.state, offices.zip)

	xstyleheader=xid1
	xcolorheader=xid2
	xidheader=xid3

	select xrpt
	if lworksht
		*!*	xfldlist="ship_ref,cnee_ref,dept,consignee,start,cancel,ship_via,qty,style,color,id,pack,totqty"
		*!*	xhdrlist="'WO #','PO #','Dept','Ship To','Start','Cancel','Routing','P/T Qty','&xid1','&xid2','&xid3','Pack/Size','Qty'"
		
		* added style Descript fields 6/15/15 MB
		xfldlist="ship_ref,cnee_ref,dept,consignee,start,cancel,ship_via,qty,style,color,id,descrip,pack,totqty"
		xhdrlist="'WO #','PO #','Dept','Ship To','Start','Cancel','Routing','P/T Qty','&xid1','&xid2','&xid3','Descript','Pack/Size','Qty'"

		do case
		case xaccountid=5687
			xfldlist=xfldlist+",batch_num"
			xhdrlist=xhdrlist+",'Type'"
		case inlist(xaccountid,&gbillabongaccts)
			xfldlist=xfldlist+",price,country,batch_num"
			xhdrlist=xhdrlist+",'Price','Country','Type'"
		endcase

		cReportName = "Unallocated Picktickets"
		do loadwebxls with .t., 1, &xhdrlist &&need to run with "DO" b/c possibility for too many parameters
	else
		loadwebpdf("whpt")
	endif
endif

use in pt
use in ptdet
use in xrpt
