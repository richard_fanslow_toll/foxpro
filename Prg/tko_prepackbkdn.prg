SELECT intake
m.accountid = 5836
STORE 0 TO m.plid,m.length,m.width,m.depth,m.ctncube,m.totcube,m.ctnwt,m.totwt,npo
STORE .f. TO m.wodelete,m.archived,m.flag

SELECT f3 AS STYLE, f4 as po1, f5 AS COLOR, f6 AS ID, f2 AS ctnnum ;
	FROM intake ;
	WHERE RECNO()>nRec1 AND !EMPTY(f1) ;
	GROUP BY 5,1,2,3,4 ;
	ORDER BY 1,2,3,4 ;
	INTO CURSOR tempctn2

SELECT STYLE, po1,COLOR, ID,COUNT(ctnnum) AS ctncount ;
	FROM tempctn2 ;
	GROUP BY 1,2,3,4 ;
	INTO CURSOR tempctn

SELECT f2 AS ctnnum, f3 AS STYLE, f4 as PO1,f5 AS COLOR,f6 AS ID,SUM(INT(VAL(f9))) AS PACK ;
	FROM intake ;
	WHERE RECNO()>nRec1 AND !EMPTY(f1) ;
	GROUP BY 1,2,3,4,5 INTO CURSOR temppack2 READWRITE

SELECT DISTINCT STYLE,po1,COLOR,ID,TRANSFORM(PACK) AS PACK ;
	FROM temppack2 ;
	INTO CURSOR temppack
LOCATE

SELECT f3 AS STYLE,f4 AS po1, f5 as COLOR,f6 AS ID,SPACE(2) AS PACK,99999999 AS ctnqty ;
	FROM intake ;
	WHERE RECNO()>nRec1 AND !EMPTY(f1) ;
	GROUP BY 1,2,3,4 INTO CURSOR tempgroup READWRITE

SELECT tempgroup
LOCATE
SCAN
	SELECT temppack
	LOCATE FOR temppack.STYLE = tempgroup.STYLE ;
		AND temppack.po1 = tempgroup.po1 ;
		AND temppack.COLOR = tempgroup.COLOR ;
		AND temppack.ID = tempgroup.ID
	REPLACE tempgroup.PACK WITH temppack.PACK IN tempgroup NEXT 1

	SELECT tempctn
	LOCATE FOR tempctn.STYLE = tempgroup.STYLE ;
		AND tempctn.po1 = tempgroup.po1 ;
		AND tempctn.COLOR = tempgroup.COLOR ;
		AND tempctn.ID = tempgroup.ID
	REPLACE tempgroup.ctnqty WITH tempctn.ctncount IN tempgroup NEXT 1
ENDSCAN

LOCATE
IF lTesting
*			BROW
*			SET STEP ON
ENDIF
m.plid = 0
npo = 0
SCAN
	SCATTER MEMVAR
	m.units = .F.
	m.totqty = m.ctnqty
	REPLACE xinwolog.plinqty WITH xinwolog.plinqty+m.ctnqty IN xinwolog
	REPLACE xinwolog.plunitsinqty WITH xinwolog.plunitsinqty+INT(VAL(m.pack)*m.ctnqty) IN xinwolog
	m.style = ALLTRIM(UPPER(m.style))
	IF !EMPTY(m.po1)
		m.style = m.style+"/"+ALLTRIM(m.po1)
	ENDIF
	m.color = ALLTRIM(UPPER(m.color))
	m.id = STRTRAN(ALLTRIM(UPPER(m.id))," ","")
	m.echo = "ORIGSTYLE*"+m.style+CHR(13)+"ORIGCOLOR*"+m.color+CHR(13)+"ORIGSIZE*"+m.id
	npo = npo+1
	m.po = TRANSFORM(npo)
	m.plid = m.plid+1
	m.addby = "TOLLPROC"
	m.adddt = DATETIME()
	m.addproc = "TKOPL"
	INSERT INTO xpl FROM MEMVAR
ENDSCAN

