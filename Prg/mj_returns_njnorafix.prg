utilsetup("MJ_RETURN_NJNORAFIX")
Set Exclusive Off
Set Deleted On
Set Talk Off
Set Safety Off

*use F:\wh\mjraret.dbf
useca('mjraret','wh',.t.)

useca("cartret","wh",.t.,,,,"mj_carton_returns")

xsqlexec("select * from cmtrans",,,"wh")
index on invloc tag invloc
set order to

SELECT ra,claim FROM mj_carton_returns WHERE ra='NJNO' AND CLAIM='2' INTO CURSOR T1 READWRITE
SELECT * FROM T1 A LEFT JOIN mjraret B ON A.CLAIM =B.RA INTO CURSOR T2 READWRITE
SELECT RA_A AS RA FROM T2 WHERE !ISNULL(RA_B) INTO CURSOR T3 READWRITE

SELECT T3
SCAN	
	SELECT mj_carton_returns
	LOCATE FOR RA=T3.ra 
	IF FOUND("mj_carton_returns")
	REPLACE NOTES WITH mj_carton_returns.RA IN mj_carton_returns
	EndIf   
ENDSCAN

SELECT T3
SCAN	
	SELECT mj_carton_returns
	LOCATE FOR RA=T3.ra 
	IF FOUND("mj_carton_returns")
	REPLACE RA WITH mj_carton_returns.CLAIM IN mj_carton_returns
	EndIf   
ENDSCAN

SELECT T3
SCAN	
	SELECT mj_carton_returns
	LOCATE FOR NOTES=T3.ra 
	IF FOUND("mj_carton_returns")
	REPLACE CLAIM WITH mj_carton_returns.NOTES IN mj_carton_returns
	EndIf   
ENDSCAN

tu("mj_carton_returns")

SELECT DISTINCT INVLOC FROM CMTRANS WHERE INVLOC='2' INTO CURSOR B1 READWRITE
SELECT RA,CLAIM,CARTON_ID FROM MJ_CARTON_RETURNS WHERE CLAIM='NJNO' INTO CURSOR T4 READWRITE
SELECT * FROM T4 A LEFT JOIN B1 B ON RA=INVLOC INTO CURSOR C1 READWRITE 
SELECT RA,CLAIM,CARTON_ID FROM C1 WHERE ISNULL(INVLOC) INTO CURSOR C2 READWRITE

If Reccount() > 0
EXPORT TO S:\MarcJacobsData\TEMP\njnora_to_be_reprocessed XLS

	tsendto = "tmarg@fmiint.com,yenny.garcia@tollgroup.com"
	tattach = "S:\MarcJacobsData\TEMP\njnora_to_be_reprocessed.XLS"
	tcc =""
	tFrom ="Toll EDI System Operations <fmi-transload-ops@fmiint.com>"
	tmessage = "NJNORA to be processed"
	tSubject = "NJNORA to be processed"
	Do Form m:\dev\frm\dartmail2 With tsendto,tFrom,tSubject, tcc,tattach,tmessage  ,"A"
Else
*!*	  tsendto = "tmarg@fmiint.com,yenny.garcia@tollgroup.com"
*!*	  tattach = ""
*!*	  tcc =""
*!*	  tFrom ="Toll EDI System Operations <fmi-transload-ops@fmiint.com>"
*!*	  tmessage = "MJ_RETURNS_WHLSALE  Wholesale returns scanned pallet wo not created_DO NOT EXIST "+Ttoc(Datetime())
*!*	  tSubject = "NO Wholesale returns scanned pallet wo not created EXIST "
*!*	  Do Form m:\dev\frm\dartmail2 With tsendto,tFrom,tSubject, tcc,tattach,tmessage  ,"A"
ENDIF

**************  scanned return with invalid RA
SELECT  ACCOUNTID,TROLLY, INVLOC, CARTON_ID, style,color,id,qty  FROM CMTRANS WHERE invloc='NJN' AND INWONUM=0 ORDER BY trolly INTO CURSOR T1 READWRITE
SELECT * FROM t1 a LEFT JOIN mj_carton_returns b ON a.carton_id=b.carton_id INTO CURSOR c6 READWRITE
DELETE FOR ra!='NJ'

If Reccount() > 0
copy To S:\MarcJacobsData\Reports\mjdashboard\njnora_details TYPE csv
EXPORT TO S:\MarcJacobsData\TEMP\njnora_details XLS

	tsendto = "tmarg@fmiint.com,v.gil@marcjacobs.com,L.LOPEZ@marcjacobs.com"
	tattach = "S:\MarcJacobsData\TEMP\njnora_details.XLS"
	tcc =""
	tFrom ="Toll EDI System Operations <fmi-transload-ops@fmiint.com>"
	tmessage = "NJNORA details - need valid RA"
	tSubject = "NJNORA details - need valid RA"
	Do Form m:\dev\frm\dartmail2 With tsendto,tFrom,tSubject, tcc,tattach,tmessage  ,"A"
Else

ENDIF



 Close Database All
 
 
 
	schedupdate()
	_Screen.Caption=gscreencaption