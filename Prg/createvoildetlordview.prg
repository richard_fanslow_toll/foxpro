LOCAL ThisView
ThisView = "voildetlord"

OPEN DATABASE F:\shop\shdata\sh

*CREATE SQL VIEW voildetlord AS ;

************************************************************************************
* PASTE VIEW GEN CODE BELOW

CREATE SQL VIEW voildetlord AS ;
SELECT Oildetl.oildetlid, Oildetl.ordhdrid, Oildetl.ordno, Oildetl.vehno,;
  Oildetl.oildate, Oildetl.partno, Oildetl.partdesc, Oildetl.oilqty,;
  Oildetl.unitdim, Oildetl.unitprice, Oildetl.pmordno, Oildetl.extprice;
 FROM ;
     sh!oildetl;
 WHERE  Oildetl.ordhdrid = ( ?tordhdrid )

DBSetProp(ThisView,"View","SendUpdates",.T.)
DBSetProp(ThisView,"View","BatchUpdateCount",1)
DBSetProp(ThisView,"View","CompareMemo",.T.)
DBSetProp(ThisView,"View","FetchAsNeeded",.F.)
DBSetProp(ThisView,"View","FetchMemo",.T.)
DBSetProp(ThisView,"View","FetchSize",100)
DBSetProp(ThisView,"View","MaxRecords",-1)
DBSetProp(ThisView,"View","Prepared",.F.)
DBSetProp(ThisView,"View","UpdateType",1)
DBSetProp(ThisView,"View","UseMemoSize",255)
DBSetProp(ThisView,"View","Tables","sh!oildetl")
DBSetProp(ThisView,"View","WhereType",1)

DBSetProp(ThisView+".oildetlid","Field","DataType","I")
DBSetProp(ThisView+".oildetlid","Field","UpdateName","sh!oildetl.oildetlid")
DBSetProp(ThisView+".oildetlid","Field","KeyField",.T.)
DBSetProp(ThisView+".oildetlid","Field","Updatable",.T.)

DBSetProp(ThisView+".ordhdrid","Field","DataType","I")
DBSetProp(ThisView+".ordhdrid","Field","UpdateName","sh!oildetl.ordhdrid")
DBSetProp(ThisView+".ordhdrid","Field","KeyField",.F.)
DBSetProp(ThisView+".ordhdrid","Field","Updatable",.T.)

DBSetProp(ThisView+".ordno","Field","DataType","N(6)")
DBSetProp(ThisView+".ordno","Field","UpdateName","sh!oildetl.ordno")
DBSetProp(ThisView+".ordno","Field","KeyField",.F.)
DBSetProp(ThisView+".ordno","Field","Updatable",.T.)

DBSetProp(ThisView+".vehno","Field","DataType","C(4)")
DBSetProp(ThisView+".vehno","Field","UpdateName","sh!oildetl.vehno")
DBSetProp(ThisView+".vehno","Field","KeyField",.F.)
DBSetProp(ThisView+".vehno","Field","Updatable",.T.)

DBSetProp(ThisView+".oildate","Field","DataType","D")
DBSetProp(ThisView+".oildate","Field","UpdateName","sh!oildetl.oildate")
DBSetProp(ThisView+".oildate","Field","KeyField",.F.)
DBSetProp(ThisView+".oildate","Field","Updatable",.T.)

DBSetProp(ThisView+".partno","Field","DataType","C(20)")
DBSetProp(ThisView+".partno","Field","UpdateName","sh!oildetl.partno")
DBSetProp(ThisView+".partno","Field","KeyField",.F.)
DBSetProp(ThisView+".partno","Field","Updatable",.T.)

DBSetProp(ThisView+".partdesc","Field","DataType","C(30)")
DBSetProp(ThisView+".partdesc","Field","UpdateName","sh!oildetl.partdesc")
DBSetProp(ThisView+".partdesc","Field","KeyField",.F.)
DBSetProp(ThisView+".partdesc","Field","Updatable",.T.)

DBSetProp(ThisView+".oilqty","Field","DataType","N(4)")
DBSetProp(ThisView+".oilqty","Field","UpdateName","sh!oildetl.oilqty")
DBSetProp(ThisView+".oilqty","Field","KeyField",.F.)
DBSetProp(ThisView+".oilqty","Field","Updatable",.T.)

DBSetProp(ThisView+".unitdim","Field","DataType","C(10)")
DBSetProp(ThisView+".unitdim","Field","UpdateName","sh!oildetl.unitdim")
DBSetProp(ThisView+".unitdim","Field","KeyField",.F.)
DBSetProp(ThisView+".unitdim","Field","Updatable",.T.)

DBSetProp(ThisView+".unitprice","Field","DataType","N(8,2)")
DBSetProp(ThisView+".unitprice","Field","UpdateName","sh!oildetl.unitprice")
DBSetProp(ThisView+".unitprice","Field","KeyField",.F.)
DBSetProp(ThisView+".unitprice","Field","Updatable",.T.)

DBSetProp(ThisView+".pmordno","Field","DataType","N(6)")
DBSetProp(ThisView+".pmordno","Field","UpdateName","sh!oildetl.pmordno")
DBSetProp(ThisView+".pmordno","Field","KeyField",.F.)
DBSetProp(ThisView+".pmordno","Field","Updatable",.T.)

DBSetProp(ThisView+".extprice","Field","DataType","N(10,2)")
DBSetProp(ThisView+".extprice","Field","UpdateName","sh!oildetl.extprice")
DBSetProp(ThisView+".extprice","Field","KeyField",.F.)
DBSetProp(ThisView+".extprice","Field","Updatable",.T.)




*****************************************************************************


CLOSE DATABASES

RETURN
