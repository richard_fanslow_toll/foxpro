utilsetup("JONES211")

SET SAFETY OFF
SET EXCLUSIVE OFF
PUBLIC CurrentConsignee

CurrentConsignee = ""

SET DEFAULT TO "f:\jones211\211_in\"
lcPath        = "f:\jones211\211_in\"
lcArchivePath = "f:\jones211\211_archive\"

DO m:\dev\prg\createx856

lnNum = ADIR(tarray,"*.xtn")

IF lnNum = 0
	WAIT WINDOW AT 10,10 "                  no 211 files to import               " TIMEOUT 2
else
	FOR thisfile = 1  TO lnNum
		xfile = lcPath+tarray[thisfile,1]+"."
		archivefile = lcArchivePath+tarray[thisfile,1]+"."
		isa_usefile = lcArchivePath+tarray[thisfile,1]
		WAIT WINDOW "Importing file: "+xfile NOWAIT
		IF FILE(xfile)
			DO m:\dev\prg\loadedifile WITH xfile,"*","NONE","UNK"
			Upload211(xfile)
			COPY FILE &xfile TO &archivefile
			IF FILE(archivefile)
				DELETE FILE &xfile
			ENDIF
		ENDIF
	NEXT thisfile
endif

lnNum = ADIR(tarray,"*.mfd")

IF lnNum = 0
	WAIT WINDOW AT 10,10 "                  no 211 files to import               " TIMEOUT 2
else
	FOR thisfile = 1  TO lnNum
		xfile = lcPath+tarray[thisfile,1]+"."
		archivefile = lcArchivePath+tarray[thisfile,1]+"."
		isa_usefile = lcArchivePath+tarray[thisfile,1]
		WAIT WINDOW "Importing file: "+xfile NOWAIT
		IF FILE(xfile)
			DO m:\dev\prg\loadedifile WITH xfile,"*","NONE","UNK"
			Upload211(xfile)
			COPY FILE &xfile TO &archivefile
			IF FILE(archivefile)
				DELETE FILE &xfile
			ENDIF
		ENDIF
	NEXT thisfile
endif

schedupdate()


*THIS.PARENT.cmdJ997_211Create.Click()


**********************************************************
PROCEDURE Upload211
PARAMETERS xfile

SET CENTURY OFF
SET DATE TO MDY

_SCREEN.CAPTION = "Now Processing 211 File :" +xfile

SELECT x856
GOTO TOP
STORE 0 TO lnTotalAdded
lnCurrentTripNumber =0
lcHLLevel = ""
CurrentTrailer = ""

IF !USED("csize")
	USE F:\jones211\data\csize IN 0 ALIAS csize
ENDIF

IF !USED("J211")
	USE F:\jones211\data\j211 IN 0 ALIAS j211
ENDIF

** Added by Joe ***********************
*!*	IF !USED("isa_num")
*!*		USE F:\ediwhse\DATA\isa_num IN 0
*!*	ENDIF
***************************************

ShipmentNumber = 1
llLookingForSF = .F.
ThisShipment = 0
origawb = ""
lcCurrentShipFrom = ""
CurrentBOL ="UNK"
ldDate = {^1899-01-01}
SET FILTER TO

GOTO 3
IF TRIM(x856.segment) = "ST" AND TRIM(x856.f1) = "211"

ELSE
	RETURN
ENDIF

GOTO TOP
*** Added by Joe to fill ISA_NUM table record ***
IF TRIM(x856.segment) = "ISA"
*!*		SELECT isa_num
*!*		APPEND BLANK
*!*		REPLACE isa_num.isa_num WITH ALLTRIM(x856.f13)
*!*		REPLACE isa_num.acct_num WITH 1
*!*		REPLACE isa_num.TYPE WITH '211'
*!*		REPLACE isa_num.ack WITH .F.
*!*		REPLACE isa_num.filename WITH isa_usefile
*!*		replace isa_num.filename WITH thisform.text3.Value
*!*		REPLACE isa_num.dateloaded WITH DATETIME()
ENDIF
*************************************************

SELECT x856
LOCATE FOR x856.segment = "N1" AND TRIM(x856.f1) = "SF"
IF FOUND()
	lcCurrentShipFrom =  TRIM(x856.f2)
ENDIF

SELECT x856
GOTO TOP

DO WHILE !EOF("x856")
	WAIT WINDOW "At the outer loop " NOWAIT

	IF TRIM(x856.segment) = "N1" AND TRIM(x856.f1) = "ST"
		CurrentConsignee = ALLTRIM(x856.f2)
	ENDIF

	IF TRIM(x856.segment) = "REF" AND TRIM(x856.f1) = "Y5"
		CurrentTrailer =ALLTRIM(x856.f2)
		CurrentBOL = ALLTRIM(x856.f3)
		lcDate = ALLTRIM(x856.f4)
		lcDate = SUBSTR(lcDate,3,2)+"/"+SUBSTR(lcDate,5,2)+"/"+SUBSTR(lcDate,1,2)
		ldDate = CTOD(lcDate)
	ENDIF

	IF TRIM(x856.segment) = "BOL"
		CurrentBOL = ALLTRIM(x856.f4)
		lcDate = ALLTRIM(x856.f4)
		lcDate = SUBSTR(lcDate,3,2)+"/"+SUBSTR(lcDate,5,2)+"/"+SUBSTR(lcDate,1,2)
		ldDate = CTOD(lcDate)
	ENDIF

	IF TRIM(x856.segment) = "MS2"
		CurrentTrailer =ALLTRIM(x856.f3)
	ENDIF

	IF TRIM(x856.segment) = "Q5" AND TRIM(x856.f1) = "AD"
		lcDate = ALLTRIM(x856.f2)
		lcDate = SUBSTR(lcDate,3,2)+"/"+SUBSTR(lcDate,5,2)+"/"+SUBSTR(lcDate,1,2)
		ldDate = CTOD(lcDate)
	ENDIF

	IF TRIM(x856.segment) = "N9" AND TRIM(x856.f1) = "CN"
		CurrentBOL = ALLTRIM(x856.f2)
	ENDIF

	IF TRIM(x856.segment) = "Q6"
		CurrentTotWeight = VAL(x856.f1)
		CurrentTotCtn    = VAL(x856.f4)
	ENDIF



	IF TRIM(x856.segment) = "BOL"
		CurrentBOL = ALLTRIM(x856.f3)
		lcDate = ALLTRIM(x856.f4)
		lcDate = SUBSTR(lcDate,3,2)+"/"+SUBSTR(lcDate,5,2)+"/"+SUBSTR(lcDate,1,2)
		ldDate = CTOD(lcDate)
	ENDIF

	IF TRIM(x856.segment) = "N1" AND TRIM(x856.f1) = "SF"
		llLookingForSF = .T.
	ENDIF

	IF TRIM(x856.segment) = "N4" AND llLookingForSF = .T.
		lcCurrentShipFrom = ALLTRIM(x856.f1)+", "+ALLTRIM(x856.f2)
		llLookingForSF = .F.
	ENDIF

	IF TRIM(x856.segment) = "N1" AND TRIM(x856.f1) = "ST"
		CurrentConsignee = ALLTRIM(x856.f2)
	ENDIF

	IF TRIM(x856.segment) = "MS2"
		CurrentTrailer = ALLTRIM(x856.f2)
	ENDIF

	IF TRIM(x856.segment) = "AT2"
		CurrentTotCtn    = VAL(x856.f1)
		CurrentTotWeight = VAL(x856.f5)
	ENDIF

	IF TRIM(x856.segment) = "SPO"
		SELECT j211
		APPEND BLANK
		REPLACE j211.trailer    WITH CurrentTrailer
		REPLACE j211.bol_num    WITH CurrentBOL
		REPLACE j211.dateloaded WITH DATE()
		REPLACE j211.shipfrom   WITH lcCurrentShipFrom
		REPLACE j211.shipdate   WITH ldDate
		REPLACE j211.dept       WITH ALLTRIM(x856.f2)
		REPLACE j211.consignee  WITH CurrentConsignee
		REPLACE j211.po_num     WITH ALLTRIM(x856.f1)
		REPLACE j211.ctn_qty    WITH VAL(x856.f4)
		REPLACE j211.weight     WITH VAL(x856.f6)
		REPLACE j211.tot_qty    WITH CurrentTotCtn
		REPLACE j211.tot_weight WITH CurrentTotWeight
		REPLACE j211.filename   WITH xfile
	ENDIF


	SELECT x856
	IF !EOF()
		SKIP 1 IN x856
	ENDIF
ENDDO
SELECT x856



**********************************************************
