*!* This is a generic form import program for CR8REC transfer sheets, etc.

*SET STEP ON
IF lTesting
	lTestMail = .T.
ENDIF

SELECT xinwolog
SET ORDER TO inwologid
LOCATE
STORE RECCOUNT() TO lnCount
WAIT WINDOW AT 10,10 "Container lines to load ...........  "+STR(lnCount) NOWAIT TIMEOUT 1

nUploadcount = 0
cRecs = ALLTRIM(STR(lnCount))

*ASSERT .F. MESSAGE "At XINWOLOG Import loop...debug"
cCtrString = PADR("WO NUM",10)+PADR("CONTAINER",15)+PADR("CTNS",6)+"PRS"+CHR(13)

IF lTesting
	STORE "P" TO cMod,cOffice
	STORE "whp" TO cWhseMod
ENDIF

SCAN  && Currently in XINWOLOG table
	cContainer = ALLT(xinwolog.CONTAINER)
	cCtnQty = ALLT(STR(xinwolog.plinqty))
	WAIT "AT XINWOLOG RECORD "+ALLTRIM(STR(RECNO()))+" OF "+cRecs WINDOW NOWAIT NOCLEAR
	SELECT inwolog
	SET STEP ON 
	IF !EMPTY(cContainer)
		xsqlexec("select * from inwolog where container = '+cContainer+'","xiw1",,"wh")
		SELECT xiw1
		IF RECCOUNT() > 0
			WAIT WINDOW "Container "+cContainer+" already exists in INWOLOG...looping" TIMEOUT 6
			RETURN
		ENDIF
	ENDIF

	lcWarehouse = "NJ1"

	SELECT xinwolog
	SCATTER MEMVAR MEMO
	IF lTesting
		SELECT inwolog
		SET ORDER TO
	ENDIF
	m.accountid = nAcctNum
	nwo_num = dygenpk("wonum",cWhseMod)
	m.wo_num = nwo_num
	cWO_Num = ALLT(STR(m.wo_num))
	m.mod = cMod
	m.office = cOffice
	m.wo_date = DATE()

	IF !lTesting
		insertinto("inwolog","wh",.T.)
	ENDIF

	nUploadcount = nUploadcount + 1

	SELECT xpl
	SUM totqty TO nGTUnits FOR xpl.units = .T.
	cGTUnits = ALLTRIM(STR(nGTUnits))
	cCtrString = cCtrString+CHR(13)+PADR(cWO_Num,10)+PADR(cContainer,15)+PADR(cCtnQty,6)+cGTUnits  && ,cUnitQty)

	SCAN FOR inwologid = xinwolog.inwologid
		SCATTER MEMVAR MEMO
		m.wo_num = nwo_num
		m.accountid =  nAcctNum
		m.date_rcvd = DATE()
		IF !lTesting
			m.mod = cMod
			m.office = cOffice
			m.inwologid = inwolog.inwologid
			insertinto("pl","wh",.T.)
		ELSE
			m.inwologid = inwolog.inwologid
			m.plid = dygenpk("pl",cWhseMod)
			INSERT INTO pl FROM MEMVAR
		ENDIF
	ENDSCAN
ENDSCAN

WAIT WINDOW AT 10,10 "Container records loaded...........  "+STR(nUploadcount) TIMEOUT 2
WAIT CLEAR

IF nUploadcount > 0

	IF !lTesting
		tu("inwolog")
		tu("pl")
	ENDIF

*	SET STEP ON
	tsubject= "Inbound WO(s) created for "+cCustname

	IF lTesting
		tsubject= tsubject+" (TEST) at "+TTOC(DATETIME())
	ELSE
		tsubject = tsubject+"(NJ) at "+TTOC(DATETIME())
	ENDIF
	tattach = ""
	tmessage = cCtrString
	tmessage = tmessage + CHR(13) + CHR(13) + "From file: "+cFilename
	IF lTesting
		tmessage = tmessage + CHR(13) + "Data is in F:\WHP\WHDATA"
	ENDIF
	tFrom ="Toll EDI Operations <toll-edi-ops@tollgroup.com>"
*	SET STEP ON &&TMARG
	IF lDoMail
		DO FORM m:\dev\frm\dartmail2 WITH tsendto,tFrom,tsubject,tcc,tattach,tmessage,"A"
	ENDIF
	cCtrString = ""
	WAIT cCustname+" Inbound Import Process Complete for file "+cFilename WINDOW TIMEOUT 2
ENDIF

RETURN
