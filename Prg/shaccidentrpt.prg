* create a worksheet of 'Accident' type work orders for Jim O'Neill
* show WO #, WO Date, Cost Center, $, comments

LPARAMETER tdStartDate, tdEndDate

#DEFINE CRLF CHR(13) + CHR(10)
#DEFINE HDECS 2
#DEFINE HOURLY_RATE 60
#DEFINE xlEdgeBottom 9
#DEFINE xlContinuous 1
#DEFINE xlMedium -4138


SET SAFETY OFF
SET DELETED ON
SET STRICTDATE TO 0

LOCAL lnRow, lcRow, lcReportPeriod, lcFiletoSaveAs, lcSpreadsheetTemplate
LOCAL oExcel, oWorkbook, oWorksheet, lcErr, lcTitle, loError, lnRate
LOCAL 	lcStartRow, lcEndRow, lcTotalsRow

TRY
	* make these visible in called procs
	PRIVATE pdStartDate, pdEndDate

	pdStartDate = tdStartDate
	pdEndDate = tdEndDate

	IF (MONTH(pdStartDate) = MONTH(pdEndDate)) AND (YEAR(pdStartDate) = YEAR(pdEndDate)) THEN
		lcReportPeriod = CMONTH(pdStartDate) + "_" + TRANSFORM(YEAR(pdStartDate))
	ELSE
		lcReportPeriod = STRTRAN(DTOC(pdStartDate),"/","-") + "_thru_" + STRTRAN(DTOC(pdEndDate),"/","-")
	ENDIF

	lcSpreadsheetTemplate = "F:\SHOP\SHDATA\ACCIDENT_RPT_TEMPLATE3.XLS"

	lcFiletoSaveAs = "F:\SHOP\AllocationReports\Shop_Accident_Report_for_" + lcReportPeriod + ".XLS"
	lcTitle = "Accident Report Spreadsheet for " + lcReportPeriod 


	IF NOT USED('CHASLIST')
		USE SH!CHASLIST IN 0
	ENDIF

	IF NOT USED('ordhdr')
		USE SH!ordhdr IN 0
	ENDIF
	
	IF NOT USED('vehmast')
		USE SH!vehmast IN 0
	ENDIF

	IF NOT USED('trucks')
		USE SH!trucks IN 0
	ENDIF

	IF NOT USED('trailer')
		USE SH!trailer IN 0
	ENDIF

	IF USED('CURORDHDR') THEN
		USE IN CURORDHDR
	ENDIF
	
	IF USED('CURORDHDRPRE') THEN
		USE IN CURORDHDRPRE
	ENDIF
	
	IF USED('CURINSIDE') THEN
		USE IN CURINSIDE
	ENDIF
	
	IF USED('CUROUTSIDE') THEN
		USE IN CUROUTSIDE
	ENDIF
	
	IF USED('CURINCIDENT') THEN
		USE IN CURINCIDENT
	ENDIF

	* create cursor of selected order header records, with addtional fields for RATE & LABRCOST
	SELECT *, 0000.00 AS RATE, 000000.00 AS LABRCOST, "  " AS DIVISION ;
		FROM ordhdr ;
		INTO CURSOR CURORDHDRPRE ;
		WHERE (crdate >= pdStartDate) AND (crdate <= pdEndDate) ;
		AND (TYPE <> 'BLDG') ;
		AND (REPCAUSE = 'A') ;
		AND (NOT EMPTY(ORDNO)) ;
		ORDER BY crdate ;
		READWRITE
		
	IF (NOT USED('CURORDHDRPRE')) OR EOF('CURORDHDRPRE') THEN
		=MESSAGEBOX("No accident data was found")
	ELSE

		* POPULATE RATE BY CRDATE AND CALC LABOR COST FOR EACH WO REC
		SELECT CURORDHDRPRE
		SCAN
			lnRate = GetHourlyRateByCRDate( CURORDHDRPRE.crdate )
			REPLACE CURORDHDRPRE.RATE WITH lnRate, CURORDHDRPRE.LABRCOST WITH ( CURORDHDRPRE.tothours * lnRate )
		ENDSCAN

		SELECT CURORDHDRPRE
		SCAN

			WAIT WINDOW "checking Vehicle #" + TRANSFORM(CURORDHDRPRE.vehno) NOWAIT

			DO CASE
				CASE CURORDHDRPRE.TYPE == "TRUCK"
					* check trucks first

					SELECT trucks
					LOCATE FOR ALLTRIM(truck_num) == ALLTRIM(CURORDHDRPRE.vehno)
					IF FOUND() THEN
						REPLACE CURORDHDRPRE.DIVISION WITH trucks.DIVISION IN CURORDHDRPRE
						SELECT trucks
						LOCATE
						LOOP
					ENDIF

					SELECT trailer
					LOCATE FOR ALLTRIM(trail_num) == ALLTRIM(CURORDHDRPRE.vehno)
					IF FOUND() THEN
						REPLACE CURORDHDRPRE.DIVISION WITH trailer.DIVISION IN CURORDHDRPRE
						SELECT trailer
						LOCATE
						LOOP
					ENDIF

					SELECT vehmast
					LOCATE FOR ALLTRIM(vehno) == ALLTRIM(CURORDHDRPRE.vehno)
					IF FOUND() THEN
						REPLACE CURORDHDRPRE.DIVISION WITH vehmast.DIVISION IN CURORDHDRPRE
						SELECT vehmast
						LOCATE
						LOOP
					ENDIF

					SELECT CHASLIST
					LOCATE FOR ALLTRIM(CHASSIS) == ALLTRIM(CURORDHDRPRE.vehno)
					IF FOUND() THEN
						REPLACE CURORDHDRPRE.DIVISION WITH CHASLIST.DIVISION IN CURORDHDRPRE
						SELECT CHASLIST
						LOCATE
						LOOP
					ENDIF

				CASE CURORDHDRPRE.TYPE == "TRAILER"
					* check TRAILERS first

					SELECT trailer
					LOCATE FOR ALLTRIM(trail_num) == ALLTRIM(CURORDHDRPRE.vehno)
					IF FOUND() THEN
						REPLACE CURORDHDRPRE.DIVISION WITH trailer.DIVISION IN CURORDHDRPRE
						SELECT trailer
						LOCATE
						LOOP
					ENDIF

					SELECT trucks
					LOCATE FOR ALLTRIM(truck_num) == ALLTRIM(CURORDHDRPRE.vehno)
					IF FOUND() THEN
						REPLACE CURORDHDRPRE.DIVISION WITH trucks.DIVISION IN CURORDHDRPRE
						SELECT trucks
						LOCATE
						LOOP
					ENDIF

					SELECT vehmast
					LOCATE FOR ALLTRIM(vehno) == ALLTRIM(CURORDHDRPRE.vehno)
					IF FOUND() THEN
						REPLACE CURORDHDRPRE.DIVISION WITH vehmast.DIVISION IN CURORDHDRPRE
						SELECT vehmast
						LOCATE
						LOOP
					ENDIF

					SELECT CHASLIST
					LOCATE FOR ALLTRIM(CHASSIS) == ALLTRIM(CURORDHDRPRE.vehno)
					IF FOUND() THEN
						REPLACE CURORDHDRPRE.DIVISION WITH CHASLIST.DIVISION IN CURORDHDRPRE
						SELECT CHASLIST
						LOCATE
						LOOP
					ENDIF

				CASE CURORDHDRPRE.TYPE == "MISC"
					* check vehmast first

					SELECT vehmast
					LOCATE FOR ALLTRIM(vehno) == ALLTRIM(CURORDHDRPRE.vehno)
					IF FOUND() THEN
						REPLACE CURORDHDRPRE.DIVISION WITH vehmast.DIVISION IN CURORDHDRPRE
						SELECT vehmast
						LOCATE
						LOOP
					ENDIF

					SELECT trucks
					LOCATE FOR ALLTRIM(truck_num) == ALLTRIM(CURORDHDRPRE.vehno)
					IF FOUND() THEN
						REPLACE CURORDHDRPRE.DIVISION WITH trucks.DIVISION IN CURORDHDRPRE
						SELECT trucks
						LOCATE
						LOOP
					ENDIF

					SELECT trailer
					LOCATE FOR ALLTRIM(trail_num) == ALLTRIM(CURORDHDRPRE.vehno)
					IF FOUND() THEN
						REPLACE CURORDHDRPRE.DIVISION WITH trailer.DIVISION IN CURORDHDRPRE
						SELECT trailer
						LOCATE
						LOOP
					ENDIF

					SELECT CHASLIST
					LOCATE FOR ALLTRIM(CHASSIS) == ALLTRIM(CURORDHDRPRE.vehno)
					IF FOUND() THEN
						REPLACE CURORDHDRPRE.DIVISION WITH CHASLIST.DIVISION IN CURORDHDRPRE
						SELECT CHASLIST
						LOCATE
						LOOP
					ENDIF

				OTHERWISE
					* CHASSIS - check chaslist first

					SELECT CHASLIST
					LOCATE FOR ALLTRIM(CHASSIS) == ALLTRIM(CURORDHDRPRE.vehno)
					IF FOUND() THEN
						REPLACE CURORDHDRPRE.DIVISION WITH CHASLIST.DIVISION IN CURORDHDRPRE
						SELECT CHASLIST
						LOCATE
						LOOP
					ENDIF

					SELECT trucks
					LOCATE FOR ALLTRIM(truck_num) == ALLTRIM(CURORDHDRPRE.vehno)
					IF FOUND() THEN
						REPLACE CURORDHDRPRE.DIVISION WITH trucks.DIVISION IN CURORDHDRPRE
						SELECT trucks
						LOCATE
						LOOP
					ENDIF

					SELECT trailer
					LOCATE FOR ALLTRIM(trail_num) == ALLTRIM(CURORDHDRPRE.vehno)
					IF FOUND() THEN
						REPLACE CURORDHDRPRE.DIVISION WITH trailer.DIVISION IN CURORDHDRPRE
						SELECT trailer
						LOCATE
						LOOP
					ENDIF

					SELECT vehmast
					LOCATE FOR ALLTRIM(vehno) == ALLTRIM(CURORDHDRPRE.vehno)
					IF FOUND() THEN
						REPLACE CURORDHDRPRE.DIVISION WITH vehmast.DIVISION IN CURORDHDRPRE
						SELECT vehmast
						LOCATE
						LOOP
					ENDIF

			ENDCASE
			
			*!*				IF CURORDHDRPRE.TYPE == "TRUCK" THEN
			*!*					
			*!*					* check trucks first			

			*!*					SELECT trucks
			*!*					LOCATE FOR ALLTRIM(truck_num) == ALLTRIM(CURORDHDRPRE.vehno)
			*!*					IF FOUND() THEN
			*!*						REPLACE CURORDHDRPRE.DIVISION WITH trucks.DIVISION IN CURORDHDRPRE
			*!*						SELECT trucks
			*!*						LOCATE
			*!*						LOOP
			*!*					ENDIF

			*!*					SELECT trailer
			*!*					LOCATE FOR ALLTRIM(trail_num) == ALLTRIM(CURORDHDRPRE.vehno)
			*!*					IF FOUND() THEN
			*!*						REPLACE CURORDHDRPRE.DIVISION WITH trailer.DIVISION IN CURORDHDRPRE
			*!*						SELECT trailer
			*!*						LOCATE
			*!*						LOOP
			*!*					ENDIF

			*!*					SELECT vehmast
			*!*					LOCATE FOR ALLTRIM(vehno) == ALLTRIM(CURORDHDRPRE.vehno)
			*!*					IF FOUND() THEN
			*!*						REPLACE CURORDHDRPRE.DIVISION WITH vehmast.DIVISION IN CURORDHDRPRE
			*!*						SELECT vehmast
			*!*						LOCATE
			*!*						LOOP
			*!*					ENDIF
			*!*				
			*!*				ELSE
			*!*					
			*!*					* check trailers first			

			*!*					SELECT trailer
			*!*					LOCATE FOR ALLTRIM(trail_num) == ALLTRIM(CURORDHDRPRE.vehno)
			*!*					IF FOUND() THEN
			*!*						REPLACE CURORDHDRPRE.DIVISION WITH trailer.DIVISION IN CURORDHDRPRE
			*!*						SELECT trailer
			*!*						LOCATE
			*!*						LOOP
			*!*					ENDIF

			*!*					SELECT trucks
			*!*					LOCATE FOR ALLTRIM(truck_num) == ALLTRIM(CURORDHDRPRE.vehno)
			*!*					IF FOUND() THEN
			*!*						REPLACE CURORDHDRPRE.DIVISION WITH trucks.DIVISION IN CURORDHDRPRE
			*!*						SELECT trucks
			*!*						LOCATE
			*!*						LOOP
			*!*					ENDIF

			*!*					SELECT vehmast
			*!*					LOCATE FOR ALLTRIM(vehno) == ALLTRIM(CURORDHDRPRE.vehno)
			*!*					IF FOUND() THEN
			*!*						REPLACE CURORDHDRPRE.DIVISION WITH vehmast.DIVISION IN CURORDHDRPRE
			*!*						SELECT vehmast
			*!*						LOCATE
			*!*						LOOP
			*!*					ENDIF
			*!*					
			*!*				ENDIF && CURORDHDRPRE.TYPE == "TRUCK" 

		ENDSCAN
		WAIT CLEAR
		
		SELECT * FROM CURORDHDRPRE INTO CURSOR CURORDHDR ORDER BY DIVISION, CRDATE READWRITE
		
		SELECT * FROM CURORDHDRPRE INTO CURSOR CURINCIDENT WHERE (LCHARGINC OR NOT EMPTY(CIDRIVER)) ORDER BY CRDATE READWRITE
		
		SELECT DIVISION, SUM(LABRCOST + TOTPCOST + OUTSCOST) AS TOTCOST FROM CURORDHDRPRE INTO CURSOR CURINSIDE WHERE NOT ("OUTSIDE VENDOR" $ UPPER(MECHNAME)) GROUP BY 1 ORDER BY 1 READWRITE

		SELECT DIVISION, SUM(LABRCOST + TOTPCOST + OUTSCOST) AS TOTCOST FROM CURORDHDRPRE INTO CURSOR CUROUTSIDE WHERE ("OUTSIDE VENDOR" $ UPPER(MECHNAME)) GROUP BY 1 ORDER BY 1 READWRITE

		oExcel = CREATEOBJECT("excel.application")
		oExcel.displayalerts = .F.
		oExcel.VISIBLE = .F.

		oWorkbook = oExcel.workbooks.OPEN(lcSpreadsheetTemplate)
		oWorkbook.SAVEAS(lcFiletoSaveAs)

		oWorksheet = oWorkbook.Worksheets[1]
		oWorksheet.RANGE("A3","L1000").clearcontents()
		lnRow = 2
		lcStartRow = '3'
		
		oWorksheet.RANGE("A1").VALUE = lcTitle + " (inside costs)"

		SELECT CURORDHDR
		*SCAN FOR (OUTSCOST = 0.00)
		SCAN FOR NOT ("OUTSIDE VENDOR" $ UPPER(MECHNAME))
		
			lnRow = lnRow + 1
			lcRow = ALLTRIM(STR(lnRow))

			oWorksheet.RANGE("A"+lcRow).VALUE = CURORDHDR.ORDNO
			oWorksheet.RANGE("B"+lcRow).VALUE = CURORDHDR.crdate
			oWorksheet.RANGE("C"+lcRow).VALUE = CURORDHDR.TYPE
			oWorksheet.RANGE("D"+lcRow).VALUE = CURORDHDR.VEHNO
			oWorksheet.RANGE("E"+lcRow).VALUE = CURORDHDR.MAINTTYPE
			oWorksheet.RANGE("F"+lcRow).VALUE = CURORDHDR.REPCAUSE
			oWorksheet.RANGE("G"+lcRow).VALUE = CURORDHDR.MECHNAME
			oWorksheet.RANGE("H"+lcRow).VALUE = CURORDHDR.tothours
			oWorksheet.RANGE("I"+lcRow).VALUE = CURORDHDR.RATE
			oWorksheet.RANGE("J"+lcRow).VALUE = CURORDHDR.LABRCOST
			oWorksheet.RANGE("K"+lcRow).VALUE = CURORDHDR.TOTPCOST
			oWorksheet.RANGE("L"+lcRow).VALUE = CURORDHDR.OUTSCOST
			oWorksheet.RANGE("M"+lcRow).VALUE = "'" + CURORDHDR.DIVISION
			oWorksheet.RANGE("N"+lcRow).VALUE = "=SUM(J"+lcRow + "..L"+lcRow+")"
			oWorksheet.RANGE("O"+lcRow).VALUE = CURORDHDR.REMARKS
			oWorksheet.RANGE("P"+lcRow).VALUE = CURORDHDR.WORKDESC

		ENDSCAN
		
		IF lnRow > 2 THEN
			* We did scan records above
		
			* TOTAL COLUMN N
			lcEndRow = lcRow
				
			* UNDERLINE cell to be totaled...
			oWorksheet.RANGE("N" + lcEndRow).BORDERS(xlEdgeBottom).LineStyle = xlContinuous
			oWorksheet.RANGE("N" + lcEndRow).BORDERS(xlEdgeBottom).Weight = xlMedium
			
			lnRow = lnRow + 1
			lcTotalsRow = ALLTRIM(STR(lnRow))
			
			oWorksheet.RANGE("N" + lcTotalsRow).VALUE = "=SUM(N" + lcStartRow + "..N" + lcEndRow + ")"
			
		ENDIF && lnRow > 2 
		
		*************************************************************************************************
		* add div summary
		lnRow = 2
		lcStartRow = '3'
		SELECT CURINSIDE
		SCAN
		
			lnRow = lnRow + 1
			lcRow = ALLTRIM(STR(lnRow))

			oWorksheet.RANGE("R"+lcRow).VALUE = "'" + CURINSIDE.DIVISION
			oWorksheet.RANGE("S"+lcRow).VALUE = CURINSIDE.TOTCOST
		
		ENDSCAN
		
		IF lnRow > 2 THEN
			* We did scan records above
			
			* TOTAL COLUMN S
			lcEndRow = lcRow
			
			* UNDERLINE cell to be totaled...
			oWorksheet.RANGE("S" + lcEndRow).BORDERS(xlEdgeBottom).LineStyle = xlContinuous
			oWorksheet.RANGE("S" + lcEndRow).BORDERS(xlEdgeBottom).Weight = xlMedium
			
			lnRow = lnRow + 1
			lcTotalsRow = ALLTRIM(STR(lnRow))
			
			oWorksheet.RANGE("S" + lcTotalsRow).VALUE = "=SUM(S" + lcStartRow + "..S" + lcEndRow + ")"
			
		ENDIF && lnRow > 2 
		*************************************************************************************************
		
		
		oWorksheet = oWorkbook.Worksheets[2]
		oWorksheet.RANGE("A3","L1000").clearcontents()
		lnRow = 2
		lcStartRow = '3'
		
		oWorksheet.RANGE("A1").VALUE = lcTitle + " (outside costs)"

		SELECT CURORDHDR
		*SCAN FOR (OUTSCOST > 0.00)
		SCAN FOR ("OUTSIDE VENDOR" $ UPPER(MECHNAME))
		
			lnRow = lnRow + 1
			lcRow = ALLTRIM(STR(lnRow))

			oWorksheet.RANGE("A"+lcRow).VALUE = CURORDHDR.ORDNO
			oWorksheet.RANGE("B"+lcRow).VALUE = CURORDHDR.crdate
			oWorksheet.RANGE("C"+lcRow).VALUE = CURORDHDR.TYPE
			oWorksheet.RANGE("D"+lcRow).VALUE = CURORDHDR.VEHNO
			oWorksheet.RANGE("E"+lcRow).VALUE = CURORDHDR.MAINTTYPE
			oWorksheet.RANGE("F"+lcRow).VALUE = CURORDHDR.REPCAUSE
			oWorksheet.RANGE("G"+lcRow).VALUE = CURORDHDR.MECHNAME
			oWorksheet.RANGE("H"+lcRow).VALUE = CURORDHDR.tothours
			oWorksheet.RANGE("I"+lcRow).VALUE = CURORDHDR.RATE
			oWorksheet.RANGE("J"+lcRow).VALUE = CURORDHDR.LABRCOST
			oWorksheet.RANGE("K"+lcRow).VALUE = CURORDHDR.TOTPCOST
			oWorksheet.RANGE("L"+lcRow).VALUE = CURORDHDR.OUTSCOST
			oWorksheet.RANGE("M"+lcRow).VALUE = "'" + CURORDHDR.DIVISION
			oWorksheet.RANGE("N"+lcRow).VALUE = "=SUM(J"+lcRow + "..L"+lcRow+")"
			oWorksheet.RANGE("O"+lcRow).VALUE = CURORDHDR.REMARKS
			oWorksheet.RANGE("P"+lcRow).VALUE = CURORDHDR.WORKDESC

		ENDSCAN
		
		IF lnRow > 2 THEN
			* We did scan records above
			
			* TOTAL COLUMN N
			lcEndRow = lcRow
			
			* UNDERLINE cell to be totaled...
			oWorksheet.RANGE("N" + lcEndRow).BORDERS(xlEdgeBottom).LineStyle = xlContinuous
			oWorksheet.RANGE("N" + lcEndRow).BORDERS(xlEdgeBottom).Weight = xlMedium
			
			lnRow = lnRow + 1
			lcTotalsRow = ALLTRIM(STR(lnRow))
			
			oWorksheet.RANGE("N" + lcTotalsRow).VALUE = "=SUM(N" + lcStartRow + "..N" + lcEndRow + ")"
		
		ENDIF && lnRow > 2 
		
		
		*************************************************************************************************
		* add div summary
		lnRow = 2
		lcStartRow = '3'
		SELECT CUROUTSIDE
		SCAN
		
			lnRow = lnRow + 1
			lcRow = ALLTRIM(STR(lnRow))

			oWorksheet.RANGE("R"+lcRow).VALUE = "'" + CUROUTSIDE.DIVISION
			oWorksheet.RANGE("S"+lcRow).VALUE = CUROUTSIDE.TOTCOST
		
		ENDSCAN
		
		IF lnRow > 2 THEN
			* We did scan records above
			
			* TOTAL COLUMN S
			lcEndRow = lcRow
			
			* UNDERLINE cell to be totaled...
			oWorksheet.RANGE("S" + lcEndRow).BORDERS(xlEdgeBottom).LineStyle = xlContinuous
			oWorksheet.RANGE("S" + lcEndRow).BORDERS(xlEdgeBottom).Weight = xlMedium
			
			lnRow = lnRow + 1
			lcTotalsRow = ALLTRIM(STR(lnRow))
			
			oWorksheet.RANGE("S" + lcTotalsRow).VALUE = "=SUM(S" + lcStartRow + "..S" + lcEndRow + ")"
		
		ENDIF && lnRow > 2 
		*************************************************************************************************


		* 3rd tab for Chargeable Incident Work Orders	
		
		oWorksheet = oWorkbook.Worksheets[3]
		oWorksheet.RANGE("A3","L1000").clearcontents()
		lnRow = 2
		lcStartRow = '3'
		
		oWorksheet.RANGE("A1").VALUE = lcTitle + " Chargeable Incidents"

		SELECT CURINCIDENT 
		SCAN 
		
			lnRow = lnRow + 1
			lcRow = ALLTRIM(STR(lnRow))

			oWorksheet.RANGE("A"+lcRow).VALUE = CURINCIDENT.ORDNO
			oWorksheet.RANGE("B"+lcRow).VALUE = CURINCIDENT.crdate
			oWorksheet.RANGE("C"+lcRow).VALUE = CURINCIDENT.TYPE
			oWorksheet.RANGE("D"+lcRow).VALUE = CURINCIDENT.VEHNO
			oWorksheet.RANGE("E"+lcRow).VALUE = CURINCIDENT.MAINTTYPE
			oWorksheet.RANGE("F"+lcRow).VALUE = CURINCIDENT.REPCAUSE
			oWorksheet.RANGE("G"+lcRow).VALUE = CURINCIDENT.MECHNAME
			oWorksheet.RANGE("H"+lcRow).VALUE = CURINCIDENT.tothours
			oWorksheet.RANGE("I"+lcRow).VALUE = CURINCIDENT.RATE
			oWorksheet.RANGE("J"+lcRow).VALUE = CURINCIDENT.LABRCOST
			oWorksheet.RANGE("K"+lcRow).VALUE = CURINCIDENT.TOTPCOST
			oWorksheet.RANGE("L"+lcRow).VALUE = CURINCIDENT.OUTSCOST
			oWorksheet.RANGE("M"+lcRow).VALUE = "'" + CURINCIDENT.DIVISION
			oWorksheet.RANGE("N"+lcRow).VALUE = "=SUM(J"+lcRow + "..L"+lcRow+")"

			oWorksheet.RANGE("O"+lcRow).VALUE = CURINCIDENT.REMARKS
			oWorksheet.RANGE("P"+lcRow).VALUE = CURINCIDENT.WORKDESC
			oWorksheet.RANGE("Q"+lcRow).VALUE = CURINCIDENT.CIDRIVER

		ENDSCAN
		
		IF lnRow > 2 THEN
			* We did scan records above
			
			* TOTAL COLUMN N
			lcEndRow = lcRow
			
			* UNDERLINE cell to be totaled...
			oWorksheet.RANGE("N" + lcEndRow).BORDERS(xlEdgeBottom).LineStyle = xlContinuous
			oWorksheet.RANGE("N" + lcEndRow).BORDERS(xlEdgeBottom).Weight = xlMedium
			
			lnRow = lnRow + 1
			lcTotalsRow = ALLTRIM(STR(lnRow))
			
			oWorksheet.RANGE("N" + lcTotalsRow).VALUE = "=SUM(N" + lcStartRow + "..N" + lcEndRow + ")"
		
		ENDIF && lnRow > 2 
		
		*************************************************************************************************
		
		

		

		MESSAGEBOX("The Accident Report Spreadsheet has been populated in Excel.",0 + 64,lcTitle)
		oWorkbook.SAVE()
		oExcel.VISIBLE = .T.

		IF TYPE('oExcel') = 'O' AND NOT ISNULL(oExcel) THEN
			oExcel.QUIT()
		ENDIF
	
	ENDIF  &&  (NOT USED('CURORDHDRPRE')) OR EOF('CURORDHDRPRE')

CATCH TO loError

	lcErr = 'There was an error.' + CRLF
	lcErr = lcErr + 'The Error # is: ' + TRANSFORM(loError.ERRORNO) + CRLF
	lcErr = lcErr + 'The Error Message is: ' + TRANSFORM(loError.MESSAGE) + CRLF
	lcErr = lcErr + 'The Error occurred in Program: ' + TRANSFORM(loError.PROCEDURE) + CRLF
	lcErr = lcErr + 'The Error occurred at line #: ' + TRANSFORM(loError.LINENO)
	MESSAGEBOX(lcErr,0+16,lcTitle)

	*oExcel.VISIBLE = .T.

	IF TYPE('oExcel') = 'O' AND NOT ISNULL(oExcel) THEN
		oExcel.QUIT()
	ENDIF

FINALLY

	IF USED('CURORDHDR') THEN
		USE IN CURORDHDR
	ENDIF
	
	IF USED('CURORDHDRPRE') THEN
		USE IN CURORDHDRPRE
	ENDIF
	
	IF USED('CURINSIDE') THEN
		USE IN CURINSIDE
	ENDIF
	
	IF USED('CUROUTSIDE') THEN
		USE IN CUROUTSIDE
	ENDIF

ENDTRY

RETURN


FUNCTION GetHourlyRateByCRDate
	LPARAMETERS tdCRdate
	DO CASE
		CASE tdCRdate < {^2008-09-01}
			lnRate = 60.00
		OTHERWISE
			lnRate = 70.00
	ENDCASE
	RETURN lnRate
ENDFUNC

*!*	***********************************************************
*!*	** code from shsummarize below

*!*	LOCAL lcCurrentdivision, lnRow, lcRow, lcReportPeriod, lcFiletoSaveAs, lcSpreadsheetTemplate
*!*	LOCAL oExcel, oWorkbook, oWorksheet, lcErr, lcTitle, loError, oWorkbookTABS, oWorksheetTABS, lnCurrentTABWorksheetNumber
*!*	LOCAL lnOverallhours, lnOveralloil, lnOveralltires, lnOverallparts, lnAllhours, lnAlloil, lnAlltires, lnAllparts
*!*	LOCAL lnTruckhours, lnTruckoil, lnTrucktires, lnTruckparts, lnTrailerhours, lnTraileroil, lnTrailertires, lnTrailerparts
*!*	LOCAL lnMischours, lnMiscoil, lnMisctires, lnMiscparts, lnTotalHours, lnTotalOil, lnTotalPart, lnTotalTire
*!*	LOCAL lcRightMostColumn, lcCompTotRow, lcDivision, lnFirstSubRow, lcFirstSubRow, lnLastSubRow, lcLastSubRow, lnFinalRow, lcFinalRow
*!*	LOCAL lnOutsideOil, lnOutsideTire, lnOutsidePart, lcSummaryHeadingsRow, lcLastRowInPrintArea, lcType, lcTotalRow, lnTotalRow
*!*	LOCAL lnOverallWashes, lnTotalWashes, lnAllWashes, lnTruckWashes, lnTrailerWashes, lnMiscWashes, llRateByDate
*!*	LOCAL lnTotalLabrCost, lnOverallLabrCost, lnAllLabrCost, lnTruckLabrCost, lnTrailerLabrCost, lnMiscLabrCost
*!*	LOCAL lnStartOfNewSummaryRow, lnTotalCost, lcSpreadsheetTemplateTABS, lcFiletoSaveAsTABS
*!*	LOCAL lnDivEndRow, lnDivStartRow, lcDivEndRow, lcDivStartRow


*!*	STORE 0 TO lnOverallhours, lnlnOveralloil, lnOveralltires, lnOverallparts, lnAllhours, lnAlloil, lnAlltires, lnAllparts, lnOverallDollars
*!*	STORE 0 TO lnTruckhours, lnTruckoil, lnTrucktires, lnTruckparts, lnTrailerhours, lnTraileroil, lnTrailertires, lnTrailerparts
*!*	STORE 0 TO lnMischours, lnMiscoil, lnMisctires, lnMiscparts, lnTotalHours, lnTotalOil, lnTotalPart, lnTotalTire
*!*	STORE 0 TO lnOutsideOil, lnOutsideTire, lnOutsidePart
*!*	STORE 0 TO lnOverallWashes, lnTotalWashes, lnAllWashes, lnTruckWashes, lnTrailerWashes, lnMiscWashes
*!*	STORE 0 TO lnTotalLabrCost, lnOverallLabrCost, lnAllLabrCost, lnTruckLabrCost, lnTrailerLabrCost, lnMiscLabrCost

*!*	llRateByDate = .T.

*!*	*!*	IF llRateByDate THEN

*!*	* new code wanted by Ken & Jimmy to rate WOs by CRDATE.
*!*	* Before 9/1/08, rate = $60/hour; 9/1/08 and after = $70/hour.

*!*	IF tlShowActive THEN
*!*		lcRightMostColumn = "L"
*!*	ELSE
*!*		lcRightMostColumn = "K"
*!*	ENDIF

*!*	* make these visible in called procs
*!*	PRIVATE pdStartDate, pdEndDate

*!*	pdStartDate = tdStartDate
*!*	pdEndDate = tdEndDate
*!*	IF (MONTH(pdStartDate) = MONTH(pdEndDate)) AND (YEAR(pdStartDate) = YEAR(pdEndDate)) THEN
*!*		lcReportPeriod = CMONTH(pdStartDate) + "_" + TRANSFORM(YEAR(pdStartDate))
*!*	ELSE
*!*		lcReportPeriod = STRTRAN(DTOC(pdStartDate),"/","-") + "_thru_" + STRTRAN(DTOC(pdEndDate),"/","-")
*!*	ENDIF

*!*	lcSpreadsheetTemplate = "F:\SHOP\SHDATA\MAINTEXP_TEMPLATE2.XLS"
*!*	lcSpreadsheetTemplateTABS = "F:\SHOP\SHDATA\MAINTEXP_TEMPLATE2_TABS.XLS"

*!*	lcFiletoSaveAs = "F:\SHOP\AllocationReports\Shop_WO_Allocation_for_" + lcReportPeriod + ".XLS"
*!*	lcFiletoSaveAsTABS = "F:\SHOP\AllocationReports\Shop_WO_Allocation_TABS_for_" + lcReportPeriod + ".XLS"
*!*	lcTitle = "WO Allocation Spreadsheet"

*!*	IF NOT USED('ordhdr')
*!*		USE SH!ordhdr IN 0
*!*	ENDIF

*!*	IF NOT USED('repdetl')
*!*		USE SH!repdetl IN 0 ORDER repdate
*!*	ENDIF

*!*	IF NOT USED('tiredetl')
*!*		USE SH!tiredetl IN 0 ORDER tiredate
*!*	ENDIF

*!*	IF NOT USED('oildetl')
*!*		USE SH!oildetl IN 0 ORDER oildate
*!*	ENDIF

*!*	IF NOT USED('vehmast')
*!*		USE SH!vehmast IN 0
*!*	ENDIF

*!*	IF NOT USED('trucks')
*!*		USE SH!trucks IN 0
*!*	ENDIF

*!*	IF NOT USED('trailer')
*!*		USE SH!trailer IN 0
*!*	ENDIF

*!*	CREATE CURSOR curCompTotals ( cDivision c(2), cRow c(5) )
*!*	CREATE CURSOR CURSUMMARY ( cDivision c(2), cType c(20), nTotalcost n(15,2) )

*!*	CheckCompanies()

*!*	CheckWOsums()

*!*	*SET STEP ON
*!*	TRY

*!*		IF USED('curVCT') THEN
*!*			USE IN curVCT
*!*		ENDIF

*!*		IF USED('CURORDHDR') THEN
*!*			USE IN CURORDHDR
*!*		ENDIF

*!*		* create cursor of selected order header records, with addtional fields for RATE & LABRCOST
*!*		SELECT *, 0000.00 AS RATE, 000000.00 AS LABRCOST ;
*!*			FROM ordhdr ;
*!*			INTO CURSOR CURORDHDR ;
*!*			WHERE (crdate >= pdStartDate) AND (crdate <= pdEndDate) ;
*!*			AND (TYPE <> 'BLDG') ;
*!*			AND (NOT EMPTY(ORDNO)) ;
*!*			READWRITE

*!*		* POPULATE RATE BY CRDATE AND CALC LABOR COST FOR EACH WO REC
*!*		SELECT CURORDHDR
*!*		SCAN
*!*			lnRate = GetHourlyRateByCRDate( CURORDHDR.crdate )
*!*			REPLACE CURORDHDR.RATE WITH lnRate, CURORDHDR.LABRCOST WITH ( CURORDHDR.tothours * lnRate )
*!*		ENDSCAN
*!*		*BROWSE
*!*		*select distinct vehno,company,type from ordhdr where type = "TRAILER" OR type = "TRUCK" order by vehno into cursor curVCT

*!*		*********************************************************************************************************************************
*!*		* get "active" status of each vehicle - for now we will just display it, but we may filter inactives out of this report
*!*		* 02/20/04 MB
*!*		*SELECT DISTINCT vehno, company, TYPE FROM ordhdr WHERE INLIST(TYPE,"TRAILER","TRUCK","MISC") ORDER BY vehno INTO CURSOR curVCT
*!*		* we are reporting on vehicles, not bldg maintenance
*!*		SELECT DISTINCT vehno, company, SPACE(2) AS DIVISION, TYPE, 'U' AS cActive ;
*!*			FROM ordhdr ;
*!*			INTO CURSOR curVCT ;
*!*			WHERE (crdate >= pdStartDate) AND (crdate <= pdEndDate) ;
*!*			AND (TYPE <> 'BLDG') ;
*!*			AND (NOT EMPTY(ORDNO)) ;
*!*			ORDER BY vehno ;
*!*			READWRITE

*!*	*!*	SELECT curVCT
*!*	*!*	brow

*!*		*********************************************************************************************************************************
*!*		*		WHERE INLIST(TYPE,"TRAILER","TRUCK","MISC") ;

*!*		IF USED('curVCT') AND RECCOUNT('curVCT') > 0 THEN

*!*			*********************************************************************************************************************************
*!*			* get "active" status of each vehicle - for now we will just display it, but we may filter inactives out of this report
*!*			* 02/20/04 MB
*!*			SELECT curVCT
*!*			GOTO TOP
*!*			SET EXACT ON

*!*			SCAN
*!*				WAIT WINDOW "checking Vehicle #" + TRANSFORM(curVCT.vehno) NOWAIT

*!*				* default to 'not found' so report can identify problem vehno's
*!*				REPLACE curVCT.DIVISION WITH " ", curVCT.TYPE WITH "UNKNOWN"

*!*				SELECT trucks
*!*				LOCATE FOR ALLTRIM(truck_num) = ALLTRIM(curVCT.vehno)
*!*				IF FOUND() THEN
*!*					REPLACE ;
*!*						curVCT.company WITH trucks.company, ;
*!*						curVCT.DIVISION WITH trucks.DIVISION, ;
*!*						curVCT.TYPE WITH "TRUCK", ;
*!*						curVCT.cActive WITH IIF(trucks.ACTIVE,"A","I")
*!*					LOOP
*!*				ENDIF
*!*				SELECT trucks
*!*				LOCATE

*!*				SELECT trailer
*!*				LOCATE FOR ALLTRIM(trail_num) = ALLTRIM(curVCT.vehno)
*!*				IF FOUND() THEN
*!*					REPLACE ;
*!*						curVCT.company WITH trailer.company, ;
*!*						curVCT.DIVISION WITH trailer.DIVISION, ;
*!*						curVCT.TYPE WITH "TRAILER", ;
*!*						curVCT.cActive WITH IIF(trailer.ACTIVE,"A","I")
*!*					LOOP
*!*				ENDIF
*!*				SELECT trailer
*!*				LOCATE

*!*				SELECT vehmast
*!*				LOCATE FOR ALLTRIM(vehno) = ALLTRIM(curVCT.vehno)
*!*				IF FOUND() THEN
*!*					REPLACE ;
*!*						curVCT.company WITH vehmast.company, ;
*!*						curVCT.DIVISION WITH vehmast.DIVISION, ;
*!*						curVCT.TYPE WITH "MISC", ;
*!*						curVCT.cActive WITH IIF(vehmast.ACTIVE,"A","I")
*!*					LOOP
*!*				ENDIF
*!*				SELECT vehmast
*!*				LOCATE

*!*			ENDSCAN
*!*			WAIT CLEAR

*!*	*!*	SELECT curVCT
*!*	*!*	brow

*!*			*********************************************************************************************************************************

*!*			oExcel = CREATEOBJECT("excel.application")
*!*			*oExcel.displayalerts = .F.
*!*			oExcel.VISIBLE = .F.

*!*			******************************************************************************************
*!*			******************************************************************************************
*!*			
*!*			* Open a spreadsheet to create a copy of the main spreadsheet with one Div per tab, per Jim O'Neill 8/13/10 MB
*!*			
*!*			oWorkbookTABS = oExcel.workbooks.OPEN(lcSpreadsheetTemplateTABS)
*!*			oWorkbookTABS.SAVEAS(lcFiletoSaveAsTABS)
*!*			lnCurrentTABWorksheetNumber = 1

*!*			*oWorksheetTABS = oWorkbookTABS.Worksheets[lnCurrentTABWorksheetNumber]
*!*			

*!*			******************************************************************************************
*!*			******************************************************************************************
*!*			
*!*			* this is the main spreadsheet...
*!*			
*!*			oWorkbook = oExcel.workbooks.OPEN(lcSpreadsheetTemplate)
*!*			oWorkbook.SAVEAS(lcFiletoSaveAs)

*!*			oWorksheet = oWorkbook.Worksheets[1]
*!*			oWorksheet.RANGE("A7","R3000").clearcontents()

*!*			IF (MONTH(pdStartDate) = MONTH(pdEndDate)) AND (YEAR(pdStartDate) = YEAR(pdEndDate)) THEN
*!*				oWorksheet.RANGE("I2").VALUE = CMONTH(pdStartDate) + " " + TRANSFORM(YEAR(pdStartDate))
*!*			ELSE
*!*				oWorksheet.RANGE("I2").VALUE = DTOC(pdStartDate)+" to "+DTOC(pdEndDate)
*!*			ENDIF

*!*			oWorksheet.RANGE("A7","R3000").FONT.bold = .F.

*!*			SELECT curVCT
*!*			INDEX ON (DIVISION + TYPE + vehno) TAG DTV
*!*			SET ORDER TO DTV

*!*			*lnRow = 30
*!*			lnRow = 50
*!*			lcRow = ALLTRIM(STR(lnRow))		
*!*			oWorksheet.RANGE("C"+lcRow).VALUE = "DETAIL BY VEHICLE TYPE, VEHICLE & COST CENTER FOLLOWS"
*!*			oWorksheet.RANGE("C"+lcRow).FONT.bold = .T.
*!*			lnRow = lnRow + 1
*!*			lnDivStartRow = lnRow

*!*			lcCurrentdivision = curVCT.DIVISION
*!*			SCAN
*!*				WAIT WINDOW NOWAIT "Populating Summarize spreadsheet for Cost Center " + lcCurrentdivision

*!*				IF lcCurrentdivision != curVCT.DIVISION
*!*				
*!*					lnRow = lnRow + 1
*!*					lcRow = ALLTRIM(STR(lnRow))

*!*					oWorksheet.RANGE("A"+lcRow).VALUE = ""
*!*					oWorksheet.RANGE("B"+lcRow).VALUE = "SubTotal"
*!*					oWorksheet.RANGE("C"+lcRow).VALUE = "Trucks"
*!*					oWorksheet.RANGE("D"+lcRow).VALUE = IIF(lnTruckoil>0,STR(lnTruckoil,8,2),"0")
*!*					oWorksheet.RANGE("E"+lcRow).VALUE = IIF(lnTrucktires>0,STR(lnTrucktires,8,2),"0")
*!*					oWorksheet.RANGE("F"+lcRow).VALUE = IIF(lnTruckparts>0,STR(lnTruckparts,8,2),"0")
*!*					oWorksheet.RANGE("G"+lcRow).VALUE = IIF(lnTruckWashes>0,STR(lnTruckWashes,8,2),"0")
*!*					oWorksheet.RANGE("H"+lcRow).VALUE = IIF(lnTruckhours>0,STR(lnTruckhours,8,HDECS),"0")

*!*					oWorksheet.RANGE("I"+lcRow).VALUE = IIF(lnTruckLabrCost>0,STR(lnTruckLabrCost,8,2),"0")
*!*					oWorksheet.RANGE("J"+lcRow).VALUE = "=+D" + lcRow + "+E" + lcRow + "+F" + lcRow + "+G" + lcRow + "+I" + lcRow
*!*					**********************************************************************************************

*!*					* format to suppress zeroes
*!*					oWorksheet.RANGE("D"+lcRow,"J"+lcRow).NumberFormat = "0;-0;;@"
*!*					**************************************************************************

*!*					oWorksheet.RANGE("B"+lcRow,"J"+lcRow).FONT.bold = .T.

*!*					lnRow = lnRow + 1
*!*					lcRow = ALLTRIM(STR(lnRow))

*!*					oWorksheet.RANGE("A"+lcRow).VALUE = ""
*!*					oWorksheet.RANGE("B"+lcRow).VALUE ="SubTotal"
*!*					oWorksheet.RANGE("C"+lcRow).VALUE = "Trailers"
*!*					oWorksheet.RANGE("D"+lcRow).VALUE = IIF(lnTraileroil>0,STR(lnTraileroil,8,2),"0")
*!*					oWorksheet.RANGE("E"+lcRow).VALUE = IIF(lnTrailertires>0,STR(lnTrailertires,8,2),"0")
*!*					oWorksheet.RANGE("F"+lcRow).VALUE = IIF(lnTrailerparts>0,STR(lnTrailerparts,8,2),"0")
*!*					oWorksheet.RANGE("G"+lcRow).VALUE = IIF(lnTrailerWashes>0,STR(lnTrailerWashes,8,2),"0")
*!*					oWorksheet.RANGE("H"+lcRow).VALUE = IIF(lnTrailerhours>0,STR(lnTrailerhours,8,HDECS),"0")

*!*					oWorksheet.RANGE("I"+lcRow).VALUE = IIF(lnTrailerLabrCost>0,STR(lnTrailerLabrCost,8,2),"0")
*!*					oWorksheet.RANGE("J"+lcRow).VALUE = "=+D" + lcRow + "+E" + lcRow + "+F" + lcRow + "+G" + lcRow + "+I" + lcRow
*!*					**********************************************************************************************

*!*					* format to suppress zeroes
*!*					oWorksheet.RANGE("D"+lcRow,"J"+lcRow).NumberFormat = "0;-0;;@"
*!*					**************************************************************************

*!*					oWorksheet.RANGE("B"+lcRow,"J"+lcRow).FONT.bold = .T.

*!*					lnRow = lnRow + 1
*!*					lcRow = ALLTRIM(STR(lnRow))

*!*					oWorksheet.RANGE("A"+lcRow).VALUE = ""
*!*					oWorksheet.RANGE("B"+lcRow).VALUE = "SubTotal"
*!*					oWorksheet.RANGE("C"+lcRow).VALUE = "Forklift"
*!*					oWorksheet.RANGE("D"+lcRow).VALUE = IIF(lnMiscoil>0,STR(lnMiscoil,8,2),"0")
*!*					oWorksheet.RANGE("E"+lcRow).VALUE = IIF(lnMisctires>0,STR(lnMisctires,8,2),"0")
*!*					oWorksheet.RANGE("F"+lcRow).VALUE = IIF(lnMiscparts>0,STR(lnMiscparts,8,2),"0")
*!*					oWorksheet.RANGE("G"+lcRow).VALUE = IIF(lnMiscWashes>0,STR(lnMiscWashes,8,2),"0")
*!*					oWorksheet.RANGE("H"+lcRow).VALUE = IIF(lnMischours>0,STR(lnMischours,8,HDECS),"0")

*!*					oWorksheet.RANGE("I"+lcRow).VALUE = IIF(lnMiscLabrCost>0,STR(lnMiscLabrCost,8,2),"0")
*!*					oWorksheet.RANGE("J"+lcRow).VALUE = "=+D" + lcRow + "+E" + lcRow + "+F" + lcRow + "+G" + lcRow + "+I" + lcRow
*!*					**********************************************************************************************

*!*					* format to suppress zeroes
*!*					oWorksheet.RANGE("D"+lcRow,"J"+lcRow).NumberFormat = "0;-0;;@"
*!*					**************************************************************************

*!*					oWorksheet.RANGE("B"+lcRow,"J"+lcRow).FONT.bold = .T.

*!*					lnRow = lnRow + 1
*!*					lnDivEndRow = lnRow
*!*					lcRow = ALLTRIM(STR(lnRow))

*!*					oWorksheet.RANGE("A"+lcRow).VALUE = ""
*!*					oWorksheet.RANGE("B"+lcRow).VALUE = "Cost Center Totals"
*!*					oWorksheet.RANGE("C"+lcRow).VALUE = ""
*!*					oWorksheet.RANGE("D"+lcRow).VALUE = IIF(lnAlloil>0,STR(lnAlloil,8,2),"0")
*!*					oWorksheet.RANGE("E"+lcRow).VALUE = IIF(lnAlltires>0,STR(lnAlltires,8,2),"0")
*!*					oWorksheet.RANGE("F"+lcRow).VALUE = IIF(lnAllparts>0,STR(lnAllparts,8,2),"0")
*!*					oWorksheet.RANGE("G"+lcRow).VALUE = IIF(lnAllWashes>0,STR(lnAllWashes,8,2),"0")
*!*					oWorksheet.RANGE("H"+lcRow).VALUE = IIF(lnAllhours>0,STR(lnAllhours,8,HDECS),"0")

*!*					************************************************************************
*!*					* save info for summary section at bottom of spreadsheet
*!*					INSERT INTO curCompTotals (cDivision, cRow) VALUES (lcCurrentdivision, lcRow)
*!*					************************************************************************

*!*					**********************************************************************************************
*!*					oWorksheet.RANGE("I"+lcRow).VALUE = IIF(lnAllLabrCost>0,STR(lnAllLabrCost,8,2),"0")

*!*					*!*						* accumulate cost center total dollars
*!*					*!*						lnOverallDollars = lnOverallDollars + lnAllLabrCost

*!*					oWorksheet.RANGE("J"+lcRow).VALUE = "=+D" + lcRow + "+E" + lcRow + "+F" + lcRow + "+G" + lcRow + "+I" + lcRow
*!*					**********************************************************************************************

*!*					* format to suppress zeroes
*!*					oWorksheet.RANGE("D"+lcRow,"J"+lcRow).NumberFormat = "0;-0;;@"
*!*					**************************************************************************

*!*					oWorksheet.RANGE("B"+lcRow,"J"+lcRow).FONT.bold = .T.

*!*					lnRow = lnRow + 1

*!*					******************************************************************************************************
*!*					* copy current Div section from main to TAB spreadsheet
*!*					lnCurrentTABWorksheetNumber = lnCurrentTABWorksheetNumber + 1
*!*					
*!*					*WAIT WINDOW 'lnCurrentTABWorksheetNumber = ' + TRANSFORM(lnCurrentTABWorksheetNumber)
*!*					
*!*					oWorksheetTABS = oWorkbookTABS.Worksheets[lnCurrentTABWorksheetNumber]
*!*					lcDivStartRow = ALLTRIM(STR(lnDivStartRow))
*!*					lcDivEndRow = ALLTRIM(STR(lnDivEndRow))
*!*					oWorksheet.RANGE("A" + lcDivStartRow + ":" + "P" + lcDivEndRow).COPY(oWorksheetTABS.RANGE("A6"))
*!*					lnDivStartRow = -1
*!*					* RENAME THE WORKSHEET TAB
*!*					oWorksheetTABS.Name = "DIV " + lcCurrentdivision
*!*					******************************************************************************************************

*!*					* reset division-level accumulators
*!*					STORE 0 TO ;
*!*						lnAllhours, lnAlloil, lnAlltires, lnAllparts, ;
*!*						lnTruckhours, lnTruckoil, lnTrucktires, lnTruckparts, ;
*!*						lnTrailerhours, lnTraileroil, lnTrailertires, lnTrailerparts, ;
*!*						lnMischours, lnMiscoil, lnMisctires, lnMiscparts, ;
*!*						lnAllWashes, lnTruckWashes, lnTrailerWashes, lnMiscWashes, ;
*!*						lnAllLabrCost, lnTruckLabrCost, lnTrailerLabrCost, lnMiscLabrCost

*!*					lcCurrentdivision = curVCT.DIVISION

*!*					*!*				ELSE  && lcCurrentdivision != curVCT.division
*!*				ENDIF  &&  lcCurrentdivision != curVCT.division

*!*				SELECT CURORDHDR
*!*				SUM ALL tothours,oilcost,partcost,tirecost,LABRCOST ;
*!*					FOR CURORDHDR.vehno = curVCT.vehno ;
*!*					TO lnTotalHours, lnTotalOil, lnTotalPart, lnTotalTire, lnTotalLabrCost

*!*				*********************************************************************************************
*!*				* 09/29/04 MB - add Outside vendor cost to appropriate categories
*!*				SUM ALL Outscost FOR CURORDHDR.vehno = curVCT.vehno AND CURORDHDR.mainttype = "T" TO lnOutsideTire
*!*				SUM ALL Outscost FOR CURORDHDR.vehno = curVCT.vehno AND CURORDHDR.mainttype = "O" TO lnOutsideOil

*!*				* changed to account for other maint types 8/22/07 MB
*!*				*SUM ALL Outscost FOR CURORDHDR.vehno = curVCT.vehno AND CURORDHDR.mainttype = "R" TO lnOutsidePart
*!*				SUM ALL Outscost FOR CURORDHDR.vehno = curVCT.vehno AND INLIST(CURORDHDR.mainttype,"R","P","I"," ") TO lnOutsidePart

*!*				SUM ALL Outscost FOR CURORDHDR.vehno = curVCT.vehno AND CURORDHDR.mainttype = "W" TO lnTotalWashes

*!*				lnTotalTire = lnTotalTire  + lnOutsideTire
*!*				lnTotalOil = lnTotalOil + lnOutsideOil
*!*				lnTotalPart = lnTotalPart + lnOutsidePart
*!*				*********************************************************************************************

*!*				lnOverallhours = lnOverallhours + lnTotalHours
*!*				lnlnOveralloil = lnlnOveralloil + lnTotalOil
*!*				lnOverallparts = lnOverallparts + lnTotalPart
*!*				lnOveralltires = lnOveralltires + lnTotalTire
*!*				lnOverallWashes = lnOverallWashes + lnTotalWashes
*!*				lnOverallLabrCost = lnOverallLabrCost + lnTotalLabrCost

*!*				lnAllhours = lnAllhours + lnTotalHours
*!*				lnAlloil   = lnAlloil   + lnTotalOil
*!*				lnAllparts = lnAllparts + lnTotalPart
*!*				lnAlltires = lnAlltires + lnTotalTire
*!*				lnAllWashes = lnAllWashes + lnTotalWashes
*!*				lnAllLabrCost = lnAllLabrCost + lnTotalLabrCost

*!*				IF curVCT.TYPE = "TRUCK"
*!*					lnTruckhours =lnTruckhours + lnTotalHours
*!*					lnTruckoil   =lnTruckoil + lnTotalOil
*!*					lnTrucktires =lnTrucktires + lnTotalTire
*!*					lnTruckparts =lnTruckparts + lnTotalPart
*!*					lnTruckWashes =lnTruckWashes + lnTotalWashes
*!*					lnTruckLabrCost = lnTruckLabrCost + lnTotalLabrCost
*!*				ENDIF

*!*				IF curVCT.TYPE = "TRAILER"
*!*					lnTrailerhours = lnTrailerhours + lnTotalHours
*!*					lnTraileroil   = lnTraileroil + lnTotalOil
*!*					lnTrailertires = lnTrailertires + lnTotalTire
*!*					lnTrailerparts = lnTrailerparts + lnTotalPart
*!*					lnTrailerWashes = lnTrailerWashes + lnTotalWashes
*!*					lnTrailerLabrCost = lnTrailerLabrCost + lnTotalLabrCost
*!*				ENDIF

*!*				* accumulate Unknown with Misc
*!*				IF INLIST(curVCT.TYPE,"MISC","UNKNOWN") THEN
*!*					lnMischours =lnMischours + lnTotalHours
*!*					lnMiscoil   =lnMiscoil + lnTotalOil
*!*					lnMisctires =lnMisctires + lnTotalTire
*!*					lnMiscparts =lnMiscparts + lnTotalPart
*!*					lnMiscWashes =lnMiscWashes + lnTotalWashes
*!*					lnMiscLabrCost = lnMiscLabrCost + lnTotalLabrCost
*!*				ENDIF

*!*				lnRow = lnRow+1			
*!*				lcRow = ALLTRIM(STR(lnRow))
*!*				
*!*				* make sure this var gets assigned only once for each division...reset to -1 in break section above
*!*				IF lnDivStartRow = -1 THEN
*!*					lnDivStartRow = lnRow
*!*				ENDIF

*!*				oWorksheet.RANGE("A"+lcRow).VALUE = "'" + curVCT.DIVISION
*!*				oWorksheet.RANGE("B"+lcRow).VALUE = curVCT.vehno

*!*				* to display "forklift" instead of "misc" 1/11/08 mb
*!*				*!*					DO CASE
*!*				*!*						CASE INLIST(ALLTRIM(curVCT.vehno),"1700","1705") AND UPPER(ALLTRIM(curVCT.TYPE)) == "TRAILER"
*!*				*!*							* flag trailers 1700, 1705 as "Container"
*!*				*!*							oWorksheet.RANGE("C"+lcRow).VALUE = "CONTAINER REPAIR"
*!*				*!*						CASE UPPER(ALLTRIM(curVCT.TYPE)) == "MISC"
*!*				*!*							oWorksheet.RANGE("C"+lcRow).VALUE = "FORKLIFT"
*!*				*!*						OTHERWISE
*!*				*!*							oWorksheet.RANGE("C"+lcRow).VALUE = curVCT.TYPE
*!*				*!*					ENDCASE
*!*				DO CASE
*!*					CASE INLIST(ALLTRIM(curVCT.vehno),"1700","1705") AND UPPER(ALLTRIM(curVCT.TYPE)) == "TRAILER"
*!*						* flag trailers 1700, 1705 as "Container"
*!*						lcType = "CONTAINER REPAIR"
*!*					CASE UPPER(ALLTRIM(curVCT.TYPE)) == "MISC"
*!*						lcType = "FORKLIFT"
*!*					OTHERWISE
*!*						lcType = curVCT.TYPE
*!*				ENDCASE
*!*				oWorksheet.RANGE("C"+lcRow).VALUE = lcType

*!*				oWorksheet.RANGE("D"+lcRow).VALUE = IIF(lnTotalOil>0,STR(lnTotalOil,8,2),"0")
*!*				oWorksheet.RANGE("E"+lcRow).VALUE = IIF(lnTotalTire>0,STR(lnTotalTire,8,2),"0")
*!*				oWorksheet.RANGE("F"+lcRow).VALUE = IIF(lnTotalPart>0,STR(lnTotalPart,8,2),"0")
*!*				oWorksheet.RANGE("G"+lcRow).VALUE = IIF(lnTotalWashes>0,STR(lnTotalWashes,8,2),"0")
*!*				oWorksheet.RANGE("H"+lcRow).VALUE = IIF(lnTotalHours>0,STR(lnTotalHours,8,HDECS),"0")
*!*				oWorksheet.RANGE("I"+lcRow).VALUE = IIF(lnTotalLabrCost>0,STR(lnTotalLabrCost,8,2),"0")
*!*				
*!*				lnTotalCost = lnTotalOil + lnTotalTire + lnTotalPart + lnTotalWashes + lnTotalLabrCost

*!*				INSERT INTO CURSUMMARY ( cDivision, cType, nTotalcost ) VALUES ( curVCT.DIVISION, lcType, lnTotalCost )

*!*				* format to suppress zeroes
*!*				oWorksheet.RANGE("D"+lcRow,"J"+lcRow).NumberFormat = "0;-0;;@"
*!*				**************************************************************************

*!*				* show "active" status of each vehicle - for now we will just display it, 02/20/04 MB
*!*				IF tlShowActive THEN
*!*					oWorksheet.RANGE(lcRightMostColumn+lcRow).VALUE = IIF(curVCT.cActive = "A","Active",IIF(curVCT.cActive = "I","Inactive","Unknown"))
*!*				ENDIF
*!*				*********************************************************************************************************************

*!*				*!*				ENDIF  &&  lcCurrentdivision != curVCT.division

*!*			ENDSCAN

*!*			lnRow = lnRow+1
*!*			lcRow = ALLTRIM(STR(lnRow))

*!*			oWorksheet.RANGE("A"+lcRow).VALUE=""
*!*			oWorksheet.RANGE("B"+lcRow).VALUE="SubTotal"
*!*			oWorksheet.RANGE("C"+lcRow).VALUE="Trucks"
*!*			oWorksheet.RANGE("D"+lcRow).VALUE=IIF(lnTruckoil>0,STR(lnTruckoil,8,2),"0")
*!*			oWorksheet.RANGE("E"+lcRow).VALUE=IIF(lnTrucktires>0,STR(lnTrucktires,8,2),"0")
*!*			oWorksheet.RANGE("F"+lcRow).VALUE=IIF(lnTruckparts>0,STR(lnTruckparts,8,2),"0")
*!*			oWorksheet.RANGE("G"+lcRow).VALUE=IIF(lnTruckWashes>0,STR(lnTruckWashes,8,2),"0")
*!*			oWorksheet.RANGE("H"+lcRow).VALUE=IIF(lnTruckhours>0,STR(lnTruckhours,8,HDECS),"0")

*!*			oWorksheet.RANGE("I"+lcRow).VALUE = IIF(lnTruckLabrCost>0,STR(lnTruckLabrCost,8,2),"0")
*!*			oWorksheet.RANGE("J"+lcRow).VALUE = "=+D" + lcRow + "+E" + lcRow + "+F" + lcRow + "+G" + lcRow + "+I" + lcRow
*!*			**********************************************************************************************

*!*			* format to suppress zeroes
*!*			oWorksheet.RANGE("D"+lcRow,"J"+lcRow).NumberFormat = "0;-0;;@"
*!*			**************************************************************************

*!*			oWorksheet.RANGE("B"+lcRow,"J"+lcRow).FONT.bold = .T.

*!*			lnRow = lnRow + 1
*!*			lcRow = ALLTRIM(STR(lnRow))

*!*			oWorksheet.RANGE("A"+lcRow).VALUE = ""
*!*			oWorksheet.RANGE("B"+lcRow).VALUE = "SubTotal"
*!*			oWorksheet.RANGE("C"+lcRow).VALUE = "Trailers"
*!*			oWorksheet.RANGE("D"+lcRow).VALUE = IIF(lnTraileroil>0,STR(lnTraileroil,8,2),"0")
*!*			oWorksheet.RANGE("E"+lcRow).VALUE = IIF(lnTrailertires>0,STR(lnTrailertires,8,2),"0")
*!*			oWorksheet.RANGE("F"+lcRow).VALUE = IIF(lnTrailerparts>0,STR(lnTrailerparts,8,2),"0")
*!*			oWorksheet.RANGE("G"+lcRow).VALUE = IIF(lnTrailerWashes>0,STR(lnTrailerWashes,8,2),"0")
*!*			oWorksheet.RANGE("H"+lcRow).VALUE = IIF(lnTrailerhours>0,STR(lnTrailerhours,8,HDECS),"0")

*!*			oWorksheet.RANGE("I"+lcRow).VALUE = IIF(lnTrailerLabrCost>0,STR(lnTrailerLabrCost,8,2),"0")
*!*			oWorksheet.RANGE("J"+lcRow).VALUE = "=+D" + lcRow + "+E" + lcRow + "+F" + lcRow + "+G" + lcRow + "+I" + lcRow
*!*			**********************************************************************************************

*!*			* format to suppress zeroes
*!*			oWorksheet.RANGE("D"+lcRow,"J"+lcRow).NumberFormat = "0;-0;;@"
*!*			**************************************************************************

*!*			oWorksheet.RANGE("B"+lcRow,"J"+lcRow).FONT.bold = .T.

*!*			lnRow = lnRow + 1
*!*			lcRow = ALLTRIM(STR(lnRow))

*!*			oWorksheet.RANGE("A"+lcRow).VALUE = ""
*!*			oWorksheet.RANGE("B"+lcRow).VALUE = "SubTotal"
*!*			oWorksheet.RANGE("C"+lcRow).VALUE = "Forklift"
*!*			oWorksheet.RANGE("D"+lcRow).VALUE = IIF(lnMiscoil>0,STR(lnMiscoil,8,2),"0")
*!*			oWorksheet.RANGE("E"+lcRow).VALUE = IIF(lnMisctires>0,STR(lnMisctires,8,2),"0")
*!*			oWorksheet.RANGE("F"+lcRow).VALUE = IIF(lnMiscparts>0,STR(lnMiscparts,8,2),"0")
*!*			oWorksheet.RANGE("G"+lcRow).VALUE = IIF(lnMiscWashes>0,STR(lnMiscWashes,8,2),"0")
*!*			oWorksheet.RANGE("H"+lcRow).VALUE = IIF(lnMischours>0,STR(lnMischours,8,HDECS),"0")

*!*			oWorksheet.RANGE("I"+lcRow).VALUE = IIF(lnMiscLabrCost>0,STR(lnMiscLabrCost,8,2),"0")
*!*			oWorksheet.RANGE("J"+lcRow).VALUE = "=+D" + lcRow + "+E" + lcRow + "+F" + lcRow + "+G" + lcRow + "+I" + lcRow
*!*			**********************************************************************************************

*!*			* format to suppress zeroes
*!*			oWorksheet.RANGE("D"+lcRow,"J"+lcRow).NumberFormat = "0;-0;;@"
*!*			**************************************************************************

*!*			oWorksheet.RANGE("B"+lcRow,"J"+lcRow).FONT.bold = .T.

*!*			lnRow = lnRow + 1
*!*			lnDivEndRow = lnRow
*!*			lcRow = ALLTRIM(STR(lnRow))

*!*			oWorksheet.RANGE("A"+lcRow).VALUE = ""
*!*			oWorksheet.RANGE("B"+lcRow).VALUE = "Cost Center Totals"
*!*			oWorksheet.RANGE("C"+lcRow).VALUE = ""
*!*			oWorksheet.RANGE("D"+lcRow).VALUE = IIF(lnAlloil>0,STR(lnAlloil,8,2),"0")
*!*			oWorksheet.RANGE("E"+lcRow).VALUE = IIF(lnAlltires>0,STR(lnAlltires,8,2),"0")
*!*			oWorksheet.RANGE("F"+lcRow).VALUE = IIF(lnAllparts>0,STR(lnAllparts,8,2),"0")
*!*			oWorksheet.RANGE("G"+lcRow).VALUE = IIF(lnAllWashes>0,STR(lnAllWashes,8,2),"0")
*!*			oWorksheet.RANGE("H"+lcRow).VALUE = IIF(lnAllhours>0,STR(lnAllhours,8,HDECS),"0")

*!*			************************************************************************
*!*			* save info for summary section at bottom of spreadsheet
*!*			INSERT INTO curCompTotals (cDivision, cRow) VALUES (lcCurrentdivision, lcRow)
*!*			************************************************************************

*!*			oWorksheet.RANGE("I"+lcRow).VALUE = IIF(lnAllLabrCost>0,STR(lnAllLabrCost,8,2),"0")
*!*			oWorksheet.RANGE("J"+lcRow).VALUE = "=+D" + lcRow + "+E" + lcRow + "+F" + lcRow + "+G" + lcRow + "+I" + lcRow
*!*			**********************************************************************************************

*!*			* format to suppress zeroes
*!*			oWorksheet.RANGE("D"+lcRow,"J"+lcRow).NumberFormat = "0;-0;;@"
*!*			**************************************************************************

*!*			oWorksheet.RANGE("B"+lcRow,"J"+lcRow).FONT.bold = .T.

*!*			lnRow = lnRow + 2
*!*			lcRow = ALLTRIM(STR(lnRow))

*!*			oWorksheet.RANGE("A"+lcRow).VALUE = ""
*!*			oWorksheet.RANGE("B"+lcRow).VALUE = "Report Period Totals"
*!*			oWorksheet.RANGE("C"+lcRow).VALUE = ""
*!*			oWorksheet.RANGE("D"+lcRow).VALUE = IIF(lnlnOveralloil>0,STR(lnlnOveralloil,8,2),"0")
*!*			oWorksheet.RANGE("E"+lcRow).VALUE = IIF(lnOveralltires>0,STR(lnOveralltires,8,2),"0")
*!*			oWorksheet.RANGE("F"+lcRow).VALUE = IIF(lnOverallparts>0,STR(lnOverallparts,8,2),"0")
*!*			oWorksheet.RANGE("G"+lcRow).VALUE = IIF(lnOverallWashes>0,STR(lnOverallWashes,8,2),"0")
*!*			oWorksheet.RANGE("H"+lcRow).VALUE = IIF(lnOverallhours>0,STR(lnOverallhours,8,HDECS),"0")

*!*			oWorksheet.RANGE("I"+lcRow).VALUE = IIF(lnOverallLabrCost>0,STR(lnOverallLabrCost,8,2),"0")
*!*			*oWorksheet.RANGE("I"+lcRow).VALUE = lnOverallDollars

*!*			oWorksheet.RANGE("J"+lcRow).VALUE = "=+D" + lcRow + "+E" + lcRow + "+F" + lcRow + "+G" + lcRow + "+I" + lcRow
*!*			**********************************************************************************************

*!*			* format to suppress zeroes
*!*			oWorksheet.RANGE("D"+lcRow,"J"+lcRow).NumberFormat = "0;-0;;@"
*!*			**************************************************************************

*!*			oWorksheet.RANGE("B"+lcRow,"J"+lcRow).FONT.bold = .T.

*!*			**********************************************************************************************
*!*			**********************************************************************************************
*!*			* new summary of division totals section 04/14/04 MB
*!*			lnRow = lnRow + 2
*!*			lnStartOfNewSummaryRow = lnRow + 2
*!*			lcRow = ALLTRIM(STR(lnRow))
*!*			lcSummaryHeadingsRow = lcRow
*!*			lcLastRowInPrintArea = ALLTRIM(STR(lnRow - 1))
*!*			oWorksheet.RANGE("F" + lcRow).VALUE = DTOC(pdStartDate)+" to "+DTOC(pdEndDate)
*!*			lnRow = lnRow + 2
*!*			lcRow = ALLTRIM(STR(lnRow))
*!*			oWorksheet.RANGE("A" + lcRow).VALUE = "Cost Ctr"
*!*			oWorksheet.RANGE("D" + lcRow).VALUE = "Oil"
*!*			oWorksheet.RANGE("E" + lcRow).VALUE = "Tires"
*!*			oWorksheet.RANGE("F" + lcRow).VALUE = "Parts"
*!*			oWorksheet.RANGE("G" + lcRow).VALUE = "Washes"
*!*			oWorksheet.RANGE("H" + lcRow).VALUE = "Hours"
*!*			*oWorksheet.RANGE("I" + lcRow).VALUE = "Rate"
*!*			oWorksheet.RANGE("I" + lcRow).VALUE = "Cost"
*!*			oWorksheet.RANGE("J" + lcRow).VALUE = "Total"
*!*			oWorksheet.RANGE("K" + lcRow).VALUE = "%"
*!*			oWorksheet.RANGE("A" + lcRow,"K" + lcRow).Interior.ColorIndex = 15

*!*			lnFirstSubRow = lnRow + 1
*!*			lcFirstSubRow = ALLTRIM(STR(lnFirstSubRow))
*!*			SELECT curCompTotals
*!*			lnTotalRow = lnRow + 2 + RECCOUNT()
*!*			lcTotalRow = ALLTRIM(STR(lnTotalRow))
*!*			SCAN
*!*				lcCompTotRow = ALLTRIM(curCompTotals.cRow)
*!*				lcDivision = ALLTRIM(curCompTotals.cDivision)
*!*				lnRow = lnRow + 1
*!*				lcRow = ALLTRIM(STR(lnRow))
*!*				lnLastSubRow = lnRow
*!*				lcLastSubRow = lcRow
*!*				oWorksheet.RANGE("A" + lcRow).VALUE = "'" + lcDivision
*!*				oWorksheet.RANGE("D" + lcRow).VALUE = oWorksheet.RANGE("D" + lcCompTotRow).VALUE
*!*				oWorksheet.RANGE("E" + lcRow).VALUE = oWorksheet.RANGE("E" + lcCompTotRow).VALUE
*!*				oWorksheet.RANGE("F" + lcRow).VALUE = oWorksheet.RANGE("F" + lcCompTotRow).VALUE
*!*				oWorksheet.RANGE("G" + lcRow).VALUE = oWorksheet.RANGE("G" + lcCompTotRow).VALUE
*!*				oWorksheet.RANGE("H" + lcRow).VALUE = oWorksheet.RANGE("H" + lcCompTotRow).VALUE
*!*				*oWorksheet.RANGE("I" + lcRow).VALUE = oWorksheet.RANGE("I" + lcCompTotRow).VALUE
*!*				oWorksheet.RANGE("I" + lcRow).VALUE = oWorksheet.RANGE("I" + lcCompTotRow).VALUE
*!*				oWorksheet.RANGE("J" + lcRow).VALUE = oWorksheet.RANGE("J" + lcCompTotRow).VALUE
*!*				oWorksheet.RANGE("K" + lcRow).VALUE = "=+J" + lcRow + "/J" + lcTotalRow
*!*				oWorksheet.RANGE("D" + lcRow,"J" + lcRow).NumberFormat = "0.00"
*!*				oWorksheet.RANGE("K" + lcRow).NumberFormat = "0%"
*!*			ENDSCAN

*!*			oWorksheet.RANGE("A" + lcTotalRow).VALUE = "Total Allocated"
*!*			oWorksheet.RANGE("D" + lcTotalRow).VALUE = "=SUM(D" + lcFirstSubRow + ":D" + lcLastSubRow + ")"
*!*			oWorksheet.RANGE("E" + lcTotalRow).VALUE = "=SUM(E" + lcFirstSubRow + ":E" + lcLastSubRow + ")"
*!*			oWorksheet.RANGE("F" + lcTotalRow).VALUE = "=SUM(F" + lcFirstSubRow + ":F" + lcLastSubRow + ")"
*!*			oWorksheet.RANGE("G" + lcTotalRow).VALUE = "=SUM(G" + lcFirstSubRow + ":G" + lcLastSubRow + ")"
*!*			oWorksheet.RANGE("H" + lcTotalRow).VALUE = "=SUM(H" + lcFirstSubRow + ":H" + lcLastSubRow + ")"
*!*			oWorksheet.RANGE("I" + lcTotalRow).VALUE = "=SUM(I" + lcFirstSubRow + ":I" + lcLastSubRow + ")"
*!*			oWorksheet.RANGE("J" + lcTotalRow).VALUE = "=SUM(J" + lcFirstSubRow + ":J" + lcLastSubRow + ")"
*!*			oWorksheet.RANGE("K" + lcTotalRow).VALUE = "=+J" + lcTotalRow + "/J" + lcTotalRow
*!*			oWorksheet.RANGE("D" + lcTotalRow,"J" + lcTotalRow).NumberFormat = "0.00"
*!*			oWorksheet.RANGE("K" + lcTotalRow).NumberFormat = "0%"

*!*			* add section for summary by div/type for Ken

*!*			SELECT cType, cDivision, SUM(nTotalcost) AS nTotalcost ;
*!*				FROM CURSUMMARY ;
*!*				INTO CURSOR CURSUMMARY2 ;
*!*				GROUP BY 1, 2 ;
*!*				ORDER BY 1, 2

*!*			lnRow = lnTotalRow + 2		
*!*			lcRow = ALLTRIM(STR(lnRow))
*!*			oWorksheet.RANGE("C"+lcRow).VALUE = "TOTAL COST SUMMARY BY TYPE & COST CENTER"
*!*			oWorksheet.RANGE("C"+lcRow).FONT.bold = .T.
*!*			SELECT CURSUMMARY2
*!*			SCAN
*!*				lnRow = lnRow + 1
*!*				lcRow = ALLTRIM(STR(lnRow))
*!*				oWorksheet.RANGE("A"+lcRow).VALUE = "'" + CURSUMMARY2.cDivision
*!*				oWorksheet.RANGE("C"+lcRow).VALUE = CURSUMMARY2.cType
*!*				oWorksheet.RANGE("J"+lcRow).VALUE = CURSUMMARY2.nTotalcost
*!*				oWorksheet.RANGE("J"+lcRow).NumberFormat = "0.00"
*!*			ENDSCAN
*!*			
*!*			lnFinalRow = lnRow
*!*			lcFinalRow = ALLTRIM(STR(lnFinalRow))

*!*			* MOVE 2 TOTALS sections UP TO TOP
*!*			oWorksheet.RANGE("A" + lcFirstSubRow + ":" + "K" + lcFinalRow).COPY(oWorksheet.RANGE("A6"))

*!*			******************************************************************************************************
*!*			* copy final Div section from main to TAB spreadsheet
*!*			lnCurrentTABWorksheetNumber = lnCurrentTABWorksheetNumber + 1
*!*			oWorksheetTABS = oWorkbookTABS.Worksheets[lnCurrentTABWorksheetNumber]
*!*			lcDivStartRow = ALLTRIM(STR(lnDivStartRow))
*!*			lcDivEndRow = ALLTRIM(STR(lnDivEndRow))
*!*			oWorksheet.RANGE("A" + lcDivStartRow + ":" + "P" + lcDivEndRow).COPY(oWorksheetTABS.RANGE("A6"))
*!*			* RENAME THE WORKSHEET TAB
*!*			oWorksheetTABS.Name = "DIV " + lcCurrentdivision
*!*			******************************************************************************************************

*!*			******************************************************************************************************
*!*			* copy summary stuff from main to 1st TAB spreadsheet
*!*			lnCurrentTABWorksheetNumber = 1
*!*			oWorksheetTABS = oWorkbookTABS.Worksheets[lnCurrentTABWorksheetNumber]
*!*			oWorksheet.RANGE("A" + lcFirstSubRow + ":" + "K" + lcFinalRow).COPY(oWorksheetTABS.RANGE("A6"))
*!*			******************************************************************************************************

*!*			oWorksheet.RANGE("A" + lcSummaryHeadingsRow + ":" + "K" + lcFinalRow).clearcontents
*!*			
*!*			* set print area
*!*			*oWorksheet.RANGE("A1",lcRightMostColumn+lcLastRowInPrintArea).SELECT
*!*			oWorksheet.PageSetup.PrintArea = "$A$1:$" + lcRightMostColumn +"$" + lcLastRowInPrintArea
*!*			


*!*			WAIT CLEAR

*!*			MESSAGEBOX("The Allocation Spreadsheet has been populated.",0 + 64,lcTitle)
*!*			oWorkbook.SAVE()
*!*			oWorkbookTABS.SAVE()
*!*			oExcel.VISIBLE = .T.
*!*			
*!*			
*!*			
*!*		ELSE
*!*			MESSAGEBOX("No Order Header data was found for that date range.",0 + 64,lcTitle)
*!*		ENDIF  && USED('curVCT') and RECCOUNT(curVCT) > 0

*!*	CATCH TO loError

*!*		lcErr = 'There was an error.' + CRLF
*!*		lcErr = lcErr + 'The Error # is: ' + TRANSFORM(loError.ERRORNO) + CRLF
*!*		lcErr = lcErr + 'The Error Message is: ' + TRANSFORM(loError.MESSAGE) + CRLF
*!*		lcErr = lcErr + 'The Error occurred in Program: ' + TRANSFORM(loError.PROCEDURE) + CRLF
*!*		lcErr = lcErr + 'The Error occurred at line #: ' + TRANSFORM(loError.LINENO)
*!*		MESSAGEBOX(lcErr,0+16,lcTitle)
*!*		*oExcel.VISIBLE = .T.
*!*		oExcel.QUIT()

*!*	FINALLY

*!*		IF USED('curVCT')
*!*			USE IN curVCT
*!*		ENDIF

*!*	ENDTRY
*!*		
*!*	*!*	ELSE   &&  llRateByDate

*!*	*!*		* code that used flat rate for all crdates follows. Was used through Sept 08.

*!*	*!*		IF tlShowActive THEN
*!*	*!*			lcRightMostColumn = "M"
*!*	*!*		ELSE
*!*	*!*			lcRightMostColumn = "L"
*!*	*!*		ENDIF

*!*	*!*		* make these visible in called procs
*!*	*!*		PRIVATE pdStartDate, pdEndDate

*!*	*!*		pdStartDate = tdStartDate
*!*	*!*		pdEndDate = tdEndDate
*!*	*!*		IF (MONTH(pdStartDate) = MONTH(pdEndDate)) AND (YEAR(pdStartDate) = YEAR(pdEndDate)) THEN
*!*	*!*			lcReportPeriod = CMONTH(pdStartDate) + "_" + TRANSFORM(YEAR(pdStartDate))
*!*	*!*		ELSE
*!*	*!*			lcReportPeriod = STRTRAN(DTOC(pdStartDate),"/","-") + "_thru_" + STRTRAN(DTOC(pdEndDate),"/","-")
*!*	*!*		ENDIF

*!*	*!*		lcSpreadsheetTemplate = "F:\SHOP\SHDATA\MAINTEXP_TEMPLATE.XLS"
*!*	*!*		*lcSpreadsheetTemplate = "M:\DEV\SHDATA\MAINTEXP_TEMPLATE.XLS"  && ACTIVATE FOR TESTING IN DEV

*!*	*!*		lcFiletoSaveAs = "F:\SHOP\AllocationReports\Shop_WO_Allocation_for_" + lcReportPeriod + ".XLS"
*!*	*!*		lcTitle = "WO Allocation Spreadsheet"

*!*	*!*		IF NOT USED('ordhdr')
*!*	*!*			USE SH!ordhdr IN 0
*!*	*!*		ENDIF

*!*	*!*		IF NOT USED('repdetl')
*!*	*!*			USE SH!repdetl IN 0 ORDER repdate
*!*	*!*		ENDIF

*!*	*!*		IF NOT USED('tiredetl')
*!*	*!*			USE SH!tiredetl IN 0 ORDER tiredate
*!*	*!*		ENDIF

*!*	*!*		IF NOT USED('oildetl')
*!*	*!*			USE SH!oildetl IN 0 ORDER oildate
*!*	*!*		ENDIF

*!*	*!*		IF NOT USED('vehmast')
*!*	*!*			USE SH!vehmast IN 0
*!*	*!*		ENDIF

*!*	*!*		IF NOT USED('trucks')
*!*	*!*			USE SH!trucks IN 0
*!*	*!*		ENDIF

*!*	*!*		IF NOT USED('trailer')
*!*	*!*			USE SH!trailer IN 0
*!*	*!*		ENDIF

*!*	*!*		CREATE CURSOR curCompTotals ( cDivision c(2), cRow c(5) )

*!*	*!*		CheckCompanies()

*!*	*!*		CheckWOsums()

*!*	*!*		*SET STEP ON
*!*	*!*		TRY

*!*	*!*			SELECT ordhdr
*!*	*!*			*select distinct vehno,company,type from ordhdr where type = "TRAILER" OR type = "TRUCK" order by vehno into cursor curVCT

*!*	*!*			*********************************************************************************************************************************
*!*	*!*			* get "active" status of each vehicle - for now we will just display it, but we may filter inactives out of this report
*!*	*!*			* 02/20/04 MB
*!*	*!*			*SELECT DISTINCT vehno, company, TYPE FROM ordhdr WHERE INLIST(TYPE,"TRAILER","TRUCK","MISC") ORDER BY vehno INTO CURSOR curVCT
*!*	*!*			* we are reporting on vehicles, not bldg maintenance
*!*	*!*			SELECT DISTINCT vehno, company, SPACE(2) AS DIVISION, TYPE, 'U' AS cActive ;
*!*	*!*				FROM ordhdr ;
*!*	*!*				INTO CURSOR curVCT ;
*!*	*!*				WHERE (crdate >= pdStartDate) AND (crdate <= pdEndDate) ;
*!*	*!*				AND (TYPE <> 'BLDG') ;
*!*	*!*				AND (NOT EMPTY(ORDNO)) ;
*!*	*!*				ORDER BY vehno ;
*!*	*!*				READWRITE

*!*	*!*			*********************************************************************************************************************************
*!*	*!*			*		WHERE INLIST(TYPE,"TRAILER","TRUCK","MISC") ;

*!*	*!*			IF USED('curVCT') AND RECCOUNT('curVCT') > 0 THEN

*!*	*!*				*********************************************************************************************************************************
*!*	*!*				* get "active" status of each vehicle - for now we will just display it, but we may filter inactives out of this report
*!*	*!*				* 02/20/04 MB
*!*	*!*				SELECT curVCT
*!*	*!*				GOTO TOP
*!*	*!*				SET EXACT ON

*!*	*!*				SCAN
*!*	*!*					WAIT WINDOW "checking Vehicle #" + TRANSFORM(curVCT.vehno) NOWAIT

*!*	*!*					* default to 'not found' so report can identify problem vehno's
*!*	*!*					REPLACE curVCT.DIVISION WITH " ", curVCT.TYPE WITH "UNKNOWN"

*!*	*!*					SELECT trucks
*!*	*!*					LOCATE FOR ALLTRIM(truck_num) = ALLTRIM(curVCT.vehno)
*!*	*!*					IF FOUND() THEN
*!*	*!*						REPLACE ;
*!*	*!*							curVCT.company WITH trucks.company, ;
*!*	*!*							curVCT.DIVISION WITH trucks.DIVISION, ;
*!*	*!*							curVCT.TYPE WITH "TRUCK", ;
*!*	*!*							curVCT.cActive WITH IIF(trucks.ACTIVE,"A","I")
*!*	*!*						LOOP
*!*	*!*					ENDIF
*!*	*!*					SELECT trucks
*!*	*!*					LOCATE

*!*	*!*					SELECT trailer
*!*	*!*					LOCATE FOR ALLTRIM(trail_num) = ALLTRIM(curVCT.vehno)
*!*	*!*					IF FOUND() THEN
*!*	*!*						REPLACE ;
*!*	*!*							curVCT.company WITH trailer.company, ;
*!*	*!*							curVCT.DIVISION WITH trailer.DIVISION, ;
*!*	*!*							curVCT.TYPE WITH "TRAILER", ;
*!*	*!*							curVCT.cActive WITH IIF(trailer.ACTIVE,"A","I")
*!*	*!*						LOOP
*!*	*!*					ENDIF
*!*	*!*					SELECT trailer
*!*	*!*					LOCATE

*!*	*!*					SELECT vehmast
*!*	*!*					LOCATE FOR ALLTRIM(vehno) = ALLTRIM(curVCT.vehno)
*!*	*!*					IF FOUND() THEN
*!*	*!*						REPLACE ;
*!*	*!*							curVCT.company WITH vehmast.company, ;
*!*	*!*							curVCT.DIVISION WITH vehmast.DIVISION, ;
*!*	*!*							curVCT.TYPE WITH "MISC", ;
*!*	*!*							curVCT.cActive WITH IIF(vehmast.ACTIVE,"A","I")
*!*	*!*						LOOP
*!*	*!*					ENDIF
*!*	*!*					SELECT vehmast
*!*	*!*					LOCATE

*!*	*!*				ENDSCAN
*!*	*!*				WAIT CLEAR

*!*	*!*				*********************************************************************************************************************************

*!*	*!*				oExcel = CREATEOBJECT("excel.application")
*!*	*!*				oExcel.VISIBLE = .F.
*!*	*!*				oWorkbook = oExcel.workbooks.OPEN(lcSpreadsheetTemplate)
*!*	*!*				oWorkbook.SAVEAS(lcFiletoSaveAs)

*!*	*!*				oWorksheet = oWorkbook.Worksheets[1]
*!*	*!*				oWorksheet.RANGE("A7","R1000").clearcontents()

*!*	*!*				IF (MONTH(pdStartDate) = MONTH(pdEndDate)) AND (YEAR(pdStartDate) = YEAR(pdEndDate)) THEN
*!*	*!*					oWorksheet.RANGE("J2").VALUE = CMONTH(pdStartDate) + " " + TRANSFORM(YEAR(pdStartDate))
*!*	*!*				ELSE
*!*	*!*					oWorksheet.RANGE("J2").VALUE = DTOC(pdStartDate)+" to "+DTOC(pdEndDate)
*!*	*!*				ENDIF

*!*	*!*				*oWorksheet.RANGE("B1","R2000").FONT.bold = .F.
*!*	*!*				oWorksheet.RANGE("A1","R2000").FONT.bold = .F.

*!*	*!*				SELECT ordhdr
*!*	*!*				SET FILTER TO (ordhdr.crdate >= pdStartDate) AND (ordhdr.crdate <=pdEndDate)
*!*	*!*				GOTO TOP

*!*	*!*				SELECT curVCT
*!*	*!*				*!*			INDEX ON (company + TYPE + vehno) TAG CTV
*!*	*!*				*!*			SET ORDER TO CTV
*!*	*!*				INDEX ON (DIVISION + TYPE + vehno) TAG DTV
*!*	*!*				SET ORDER TO DTV

*!*	*!*				*lnRow = 5
*!*	*!*				lnRow = 30
*!*	*!*				lcCurrentdivision = curVCT.DIVISION
*!*	*!*				SCAN
*!*	*!*					WAIT WINDOW NOWAIT "Populating Summarize spreadsheet for Cost Center " + lcCurrentdivision

*!*	*!*					IF lcCurrentdivision != curVCT.DIVISION

*!*	*!*						lnRow = lnRow + 1
*!*	*!*						lcRow = ALLTRIM(STR(lnRow))

*!*	*!*						oWorksheet.RANGE("A"+lcRow).VALUE = ""
*!*	*!*						oWorksheet.RANGE("B"+lcRow).VALUE = "SubTotal"
*!*	*!*						oWorksheet.RANGE("C"+lcRow).VALUE = "Trucks"
*!*	*!*						oWorksheet.RANGE("D"+lcRow).VALUE = IIF(lnTruckoil>0,STR(lnTruckoil,8,2),"0")
*!*	*!*						oWorksheet.RANGE("E"+lcRow).VALUE = IIF(lnTrucktires>0,STR(lnTrucktires,8,2),"0")
*!*	*!*						oWorksheet.RANGE("F"+lcRow).VALUE = IIF(lnTruckparts>0,STR(lnTruckparts,8,2),"0")
*!*	*!*						oWorksheet.RANGE("G"+lcRow).VALUE = IIF(lnTruckWashes>0,STR(lnTruckWashes,8,2),"0")
*!*	*!*						oWorksheet.RANGE("H"+lcRow).VALUE = IIF(lnTruckhours>0,STR(lnTruckhours,8,HDECS),"0")
*!*	*!*						*oWorksheet.RANGE("A"+lcRow,lcRightMostColumn+lcRow).Interior.ColorIndex = IIF(lnRow%2=1,15,0)

*!*	*!*						**************************************************************************
*!*	*!*						* format to suppress zeroes
*!*	*!*						oWorksheet.RANGE("D"+lcRow,"H"+lcRow).NumberFormat = "0;-0;;@"
*!*	*!*						**************************************************************************

*!*	*!*						**********************************************************************************************
*!*	*!*						* added 03-04-04 MB per Ken
*!*	*!*						oWorksheet.RANGE("I"+lcRow).VALUE = GetHourlyRateByDivision( lcCurrentdivision )  &&  HOURLY_RATE
*!*	*!*						oWorksheet.RANGE("J"+lcRow).VALUE = "=+I" + lcRow + "*H" + lcRow
*!*	*!*						oWorksheet.RANGE("K"+lcRow).VALUE = "=+D" + lcRow + "+E" + lcRow + "+F" + lcRow + "+G" + lcRow + "+J" + lcRow
*!*	*!*						**********************************************************************************************

*!*	*!*						oWorksheet.RANGE("B"+lcRow,"K"+lcRow).FONT.bold = .T.

*!*	*!*						lnRow = lnRow + 1
*!*	*!*						lcRow = ALLTRIM(STR(lnRow))

*!*	*!*						oWorksheet.RANGE("A"+lcRow).VALUE = ""
*!*	*!*						oWorksheet.RANGE("B"+lcRow).VALUE ="SubTotal"
*!*	*!*						oWorksheet.RANGE("C"+lcRow).VALUE = "Trailers"
*!*	*!*						oWorksheet.RANGE("D"+lcRow).VALUE = IIF(lnTraileroil>0,STR(lnTraileroil,8,2),"0")
*!*	*!*						oWorksheet.RANGE("E"+lcRow).VALUE = IIF(lnTrailertires>0,STR(lnTrailertires,8,2),"0")
*!*	*!*						oWorksheet.RANGE("F"+lcRow).VALUE = IIF(lnTrailerparts>0,STR(lnTrailerparts,8,2),"0")
*!*	*!*						oWorksheet.RANGE("G"+lcRow).VALUE = IIF(lnTrailerWashes>0,STR(lnTrailerWashes,8,2),"0")
*!*	*!*						oWorksheet.RANGE("H"+lcRow).VALUE = IIF(lnTrailerhours>0,STR(lnTrailerhours,8,HDECS),"0")
*!*	*!*						*oWorksheet.RANGE("A"+lcRow,lcRightMostColumn+lcRow).Interior.ColorIndex = IIF(lnRow%2=1,15,0)

*!*	*!*						**************************************************************************
*!*	*!*						* format to suppress zeroes
*!*	*!*						oWorksheet.RANGE("D"+lcRow,"H"+lcRow).NumberFormat = "0;-0;;@"
*!*	*!*						**************************************************************************

*!*	*!*						**********************************************************************************************
*!*	*!*						* added 03-04-04 MB per Ken
*!*	*!*						oWorksheet.RANGE("I"+lcRow).VALUE = GetHourlyRateByDivision( lcCurrentdivision )  &&  HOURLY_RATE
*!*	*!*						oWorksheet.RANGE("J"+lcRow).VALUE = "=+I" + lcRow + "*H" + lcRow
*!*	*!*						oWorksheet.RANGE("K"+lcRow).VALUE = "=+D" + lcRow + "+E" + lcRow + "+F" + lcRow + "+G" + lcRow + "+J" + lcRow
*!*	*!*						**********************************************************************************************

*!*	*!*						oWorksheet.RANGE("B"+lcRow,"K"+lcRow).FONT.bold = .T.

*!*	*!*						lnRow = lnRow + 1
*!*	*!*						lcRow = ALLTRIM(STR(lnRow))

*!*	*!*						oWorksheet.RANGE("A"+lcRow).VALUE = ""
*!*	*!*						oWorksheet.RANGE("B"+lcRow).VALUE = "SubTotal"
*!*	*!*						*oWorksheet.RANGE("C"+lcRow).VALUE = "Misc"
*!*	*!*						oWorksheet.RANGE("C"+lcRow).VALUE = "Forklift"
*!*	*!*						oWorksheet.RANGE("D"+lcRow).VALUE = IIF(lnMiscoil>0,STR(lnMiscoil,8,2),"0")
*!*	*!*						oWorksheet.RANGE("E"+lcRow).VALUE = IIF(lnMisctires>0,STR(lnMisctires,8,2),"0")
*!*	*!*						oWorksheet.RANGE("F"+lcRow).VALUE = IIF(lnMiscparts>0,STR(lnMiscparts,8,2),"0")
*!*	*!*						oWorksheet.RANGE("G"+lcRow).VALUE = IIF(lnMiscWashes>0,STR(lnMiscWashes,8,2),"0")
*!*	*!*						oWorksheet.RANGE("H"+lcRow).VALUE = IIF(lnMischours>0,STR(lnMischours,8,HDECS),"0")
*!*	*!*						*oWorksheet.RANGE("A"+lcRow,lcRightMostColumn+lcRow).Interior.ColorIndex = IIF(lnRow%2=1,15,0)

*!*	*!*						**************************************************************************
*!*	*!*						* format to suppress zeroes
*!*	*!*						oWorksheet.RANGE("D"+lcRow,"H"+lcRow).NumberFormat = "0;-0;;@"
*!*	*!*						**************************************************************************

*!*	*!*						**********************************************************************************************
*!*	*!*						* added 03-04-04 MB per Ken
*!*	*!*						oWorksheet.RANGE("I"+lcRow).VALUE = GetHourlyRateByDivision( lcCurrentdivision )  &&  HOURLY_RATE
*!*	*!*						oWorksheet.RANGE("J"+lcRow).VALUE = "=+I" + lcRow + "*H" + lcRow
*!*	*!*						oWorksheet.RANGE("K"+lcRow).VALUE = "=+D" + lcRow + "+E" + lcRow + "+F" + lcRow + "+G" + lcRow + "+J" + lcRow
*!*	*!*						**********************************************************************************************

*!*	*!*						oWorksheet.RANGE("B"+lcRow,"K"+lcRow).FONT.bold = .T.

*!*	*!*						lnRow = lnRow + 1
*!*	*!*						lcRow = ALLTRIM(STR(lnRow))

*!*	*!*						oWorksheet.RANGE("A"+lcRow).VALUE = ""
*!*	*!*						oWorksheet.RANGE("B"+lcRow).VALUE = "Cost Center Totals"
*!*	*!*						oWorksheet.RANGE("C"+lcRow).VALUE = ""
*!*	*!*						oWorksheet.RANGE("D"+lcRow).VALUE = IIF(lnAlloil>0,STR(lnAlloil,8,2),"0")
*!*	*!*						oWorksheet.RANGE("E"+lcRow).VALUE = IIF(lnAlltires>0,STR(lnAlltires,8,2),"0")
*!*	*!*						oWorksheet.RANGE("F"+lcRow).VALUE = IIF(lnAllparts>0,STR(lnAllparts,8,2),"0")
*!*	*!*						oWorksheet.RANGE("G"+lcRow).VALUE = IIF(lnAllWashes>0,STR(lnAllWashes,8,2),"0")
*!*	*!*						oWorksheet.RANGE("H"+lcRow).VALUE = IIF(lnAllhours>0,STR(lnAllhours,8,HDECS),"0")
*!*	*!*						*oWorksheet.RANGE("A"+lcRow,lcRightMostColumn+lcRow).Interior.ColorIndex = IIF(lnRow%2=1,15,0)

*!*	*!*						************************************************************************
*!*	*!*						* save info for summary section at bottom of spreadsheet
*!*	*!*						INSERT INTO curCompTotals (cDivision, cRow) VALUES (lcCurrentdivision, lcRow)
*!*	*!*						************************************************************************

*!*	*!*						**************************************************************************
*!*	*!*						* format to suppress zeroes
*!*	*!*						oWorksheet.RANGE("D"+lcRow,"H"+lcRow).NumberFormat = "0;-0;;@"
*!*	*!*						**************************************************************************

*!*	*!*						**********************************************************************************************
*!*	*!*						* added 03-04-04 MB per Ken
*!*	*!*						oWorksheet.RANGE("I"+lcRow).VALUE = GetHourlyRateByDivision( lcCurrentdivision )  &&  HOURLY_RATE
*!*	*!*						oWorksheet.RANGE("J"+lcRow).VALUE = "=+I" + lcRow + "*H" + lcRow

*!*	*!*						* accumulate cost center total dollars
*!*	*!*						lnOverallDollars = lnOverallDollars + ( oWorksheet.RANGE("I"+lcRow).VALUE * oWorksheet.RANGE("H"+lcRow).VALUE )

*!*	*!*						oWorksheet.RANGE("K"+lcRow).VALUE = "=+D" + lcRow + "+E" + lcRow + "+F" + lcRow + "+G" + lcRow + "+J" + lcRow
*!*	*!*						**********************************************************************************************

*!*	*!*						oWorksheet.RANGE("B"+lcRow,"K"+lcRow).FONT.bold = .T.

*!*	*!*						lnRow = lnRow + 1

*!*	*!*						* reset division-level accumulators
*!*	*!*						STORE 0 TO ;
*!*	*!*							lnAllhours, lnAlloil, lnAlltires, lnAllparts, ;
*!*	*!*							lnTruckhours, lnTruckoil, lnTrucktires, lnTruckparts, ;
*!*	*!*							lnTrailerhours, lnTraileroil, lnTrailertires, lnTrailerparts, ;
*!*	*!*							lnMischours, lnMiscoil, lnMisctires, lnMiscparts, ;
*!*	*!*							lnAllWashes, lnTruckWashes, lnTrailerWashes, lnMiscWashes

*!*	*!*						lcCurrentdivision = curVCT.DIVISION

*!*	*!*						*!*				ELSE  && lcCurrentdivision != curVCT.division
*!*	*!*					ENDIF  &&  lcCurrentdivision != curVCT.division

*!*	*!*					SELECT ordhdr
*!*	*!*					SUM ALL tothours,oilcost,partcost,tirecost FOR ordhdr.vehno = curVCT.vehno TO lnTotalHours, lnTotalOil, lnTotalPart, lnTotalTire

*!*	*!*					*********************************************************************************************
*!*	*!*					* 09/29/04 MB - add Outside vendor cost to appropriate categories
*!*	*!*					SUM ALL Outscost FOR ordhdr.vehno = curVCT.vehno AND ordhdr.mainttype = "T" TO lnOutsideTire
*!*	*!*					SUM ALL Outscost FOR ordhdr.vehno = curVCT.vehno AND ordhdr.mainttype = "O" TO lnOutsideOil

*!*	*!*					* changed to account for other maint types 8/22/07 MB
*!*	*!*					*SUM ALL Outscost FOR ordhdr.vehno = curVCT.vehno AND ordhdr.mainttype = "R" TO lnOutsidePart
*!*	*!*					SUM ALL Outscost FOR ordhdr.vehno = curVCT.vehno AND INLIST(ordhdr.mainttype,"R","P","I"," ") TO lnOutsidePart

*!*	*!*					SUM ALL Outscost FOR ordhdr.vehno = curVCT.vehno AND ordhdr.mainttype = "W" TO lnTotalWashes

*!*	*!*					lnTotalTire = lnTotalTire  + lnOutsideTire
*!*	*!*					lnTotalOil = lnTotalOil + lnOutsideOil
*!*	*!*					lnTotalPart = lnTotalPart + lnOutsidePart
*!*	*!*					*********************************************************************************************

*!*	*!*					lnOverallhours = lnOverallhours + lnTotalHours
*!*	*!*					lnlnOveralloil = lnlnOveralloil + lnTotalOil
*!*	*!*					lnOverallparts = lnOverallparts + lnTotalPart
*!*	*!*					lnOveralltires = lnOveralltires + lnTotalTire
*!*	*!*					lnOverallWashes = lnOverallWashes + lnTotalWashes

*!*	*!*					lnAllhours = lnAllhours + lnTotalHours
*!*	*!*					lnAlloil   = lnAlloil   + lnTotalOil
*!*	*!*					lnAllparts = lnAllparts + lnTotalPart
*!*	*!*					lnAlltires = lnAlltires + lnTotalTire
*!*	*!*					lnAllWashes = lnAllWashes + lnTotalWashes

*!*	*!*					IF curVCT.TYPE = "TRUCK"
*!*	*!*						lnTruckhours =lnTruckhours + lnTotalHours
*!*	*!*						lnTruckoil   =lnTruckoil + lnTotalOil
*!*	*!*						lnTrucktires =lnTrucktires + lnTotalTire
*!*	*!*						lnTruckparts =lnTruckparts + lnTotalPart
*!*	*!*						lnTruckWashes =lnTruckWashes + lnTotalWashes
*!*	*!*					ENDIF

*!*	*!*					IF curVCT.TYPE = "TRAILER"
*!*	*!*						lnTrailerhours = lnTrailerhours + lnTotalHours
*!*	*!*						lnTraileroil   = lnTraileroil + lnTotalOil
*!*	*!*						lnTrailertires = lnTrailertires + lnTotalTire
*!*	*!*						lnTrailerparts = lnTrailerparts + lnTotalPart
*!*	*!*						lnTrailerWashes = lnTrailerWashes + lnTotalWashes
*!*	*!*					ENDIF

*!*	*!*					* accumulate Unknown with Misc
*!*	*!*					*IF curVCT.TYPE = "MISC"
*!*	*!*					IF INLIST(curVCT.TYPE,"MISC","UNKNOWN") THEN
*!*	*!*						lnMischours =lnMischours + lnTotalHours
*!*	*!*						lnMiscoil   =lnMiscoil + lnTotalOil
*!*	*!*						lnMisctires =lnMisctires + lnTotalTire
*!*	*!*						lnMiscparts =lnMiscparts + lnTotalPart
*!*	*!*						lnMiscWashes =lnMiscWashes + lnTotalWashes
*!*	*!*					ENDIF

*!*	*!*					lnRow = lnRow+1
*!*	*!*					lcRow = ALLTRIM(STR(lnRow))

*!*	*!*					oWorksheet.RANGE("A"+lcRow).VALUE = "'" + curVCT.DIVISION
*!*	*!*					oWorksheet.RANGE("B"+lcRow).VALUE = curVCT.vehno

*!*	*!*					*!*	* flag trailers 1700, 1705 as "Container"
*!*	*!*					*!*	IF INLIST(ALLTRIM(curVCT.vehno),"1700","1705") AND UPPER(ALLTRIM(curVCT.TYPE)) == "TRAILER" THEN
*!*	*!*					*!*		oWorksheet.RANGE("C"+lcRow).VALUE = "CONTAINER REPAIR"
*!*	*!*					*!*	ELSE
*!*	*!*					*!*		oWorksheet.RANGE("C"+lcRow).VALUE = curVCT.TYPE
*!*	*!*					*!*	ENDIF

*!*	*!*					* to display "forklift" instead of "misc" 1/11/08 mb
*!*	*!*					DO CASE
*!*	*!*						CASE INLIST(ALLTRIM(curVCT.vehno),"1700","1705") AND UPPER(ALLTRIM(curVCT.TYPE)) == "TRAILER"
*!*	*!*							* flag trailers 1700, 1705 as "Container"
*!*	*!*							oWorksheet.RANGE("C"+lcRow).VALUE = "CONTAINER REPAIR"
*!*	*!*						CASE UPPER(ALLTRIM(curVCT.TYPE)) == "MISC"
*!*	*!*							oWorksheet.RANGE("C"+lcRow).VALUE = "FORKLIFT"
*!*	*!*						OTHERWISE
*!*	*!*							oWorksheet.RANGE("C"+lcRow).VALUE = curVCT.TYPE
*!*	*!*					ENDCASE

*!*	*!*					oWorksheet.RANGE("D"+lcRow).VALUE = IIF(lnTotalOil>0,STR(lnTotalOil,8,2),"0")
*!*	*!*					oWorksheet.RANGE("E"+lcRow).VALUE = IIF(lnTotalTire>0,STR(lnTotalTire,8,2),"0")
*!*	*!*					oWorksheet.RANGE("F"+lcRow).VALUE = IIF(lnTotalPart>0,STR(lnTotalPart,8,2),"0")
*!*	*!*					oWorksheet.RANGE("G"+lcRow).VALUE = IIF(lnTotalWashes>0,STR(lnTotalWashes,8,2),"0")
*!*	*!*					oWorksheet.RANGE("H"+lcRow).VALUE = IIF(lnTotalHours>0,STR(lnTotalHours,8,HDECS),"0")
*!*	*!*					*oWorksheet.RANGE("A"+lcRow,lcRightMostColumn+lcRow).Interior.ColorIndex = IIF(lnRow%2=1,15,0)

*!*	*!*					**************************************************************************
*!*	*!*					* format to suppress zeroes
*!*	*!*					oWorksheet.RANGE("D"+lcRow,"H"+lcRow).NumberFormat = "0;-0;;@"
*!*	*!*					**************************************************************************

*!*	*!*					*********************************************************************************************************************
*!*	*!*					* show "active" status of each vehicle - for now we will just display it, 02/20/04 MB
*!*	*!*					IF tlShowActive THEN
*!*	*!*						oWorksheet.RANGE(lcRightMostColumn+lcRow).VALUE = IIF(curVCT.cActive = "A","Active",IIF(curVCT.cActive = "I","Inactive","Unknown"))
*!*	*!*					ENDIF
*!*	*!*					*********************************************************************************************************************

*!*	*!*					*!*				ENDIF  &&  lcCurrentdivision != curVCT.division

*!*	*!*				ENDSCAN

*!*	*!*				lnRow = lnRow+1
*!*	*!*				lcRow = ALLTRIM(STR(lnRow))

*!*	*!*				oWorksheet.RANGE("A"+lcRow).VALUE=""
*!*	*!*				oWorksheet.RANGE("B"+lcRow).VALUE="SubTotal"
*!*	*!*				oWorksheet.RANGE("C"+lcRow).VALUE="Trucks"
*!*	*!*				oWorksheet.RANGE("D"+lcRow).VALUE=IIF(lnTruckoil>0,STR(lnTruckoil,8,2),"0")
*!*	*!*				oWorksheet.RANGE("E"+lcRow).VALUE=IIF(lnTrucktires>0,STR(lnTrucktires,8,2),"0")
*!*	*!*				oWorksheet.RANGE("F"+lcRow).VALUE=IIF(lnTruckparts>0,STR(lnTruckparts,8,2),"0")
*!*	*!*				oWorksheet.RANGE("G"+lcRow).VALUE=IIF(lnTruckWashes>0,STR(lnTruckWashes,8,2),"0")
*!*	*!*				oWorksheet.RANGE("H"+lcRow).VALUE=IIF(lnTruckhours>0,STR(lnTruckhours,8,HDECS),"0")
*!*	*!*				*oWorksheet.RANGE("A"+lcRow,lcRightMostColumn+lcRow).Interior.ColorIndex = IIF(lnRow%2=1,15,0)

*!*	*!*				**************************************************************************
*!*	*!*				* format to suppress zeroes
*!*	*!*				oWorksheet.RANGE("D"+lcRow,"H"+lcRow).NumberFormat = "0;-0;;@"
*!*	*!*				**************************************************************************

*!*	*!*				**********************************************************************************************
*!*	*!*				* added 03-04-04 MB per Ken
*!*	*!*				oWorksheet.RANGE("I"+lcRow).VALUE = GetHourlyRateByDivision( lcCurrentdivision )  &&  HOURLY_RATE
*!*	*!*				oWorksheet.RANGE("J"+lcRow).VALUE = "=+I" + lcRow + "*H" + lcRow
*!*	*!*				oWorksheet.RANGE("K"+lcRow).VALUE = "=+D" + lcRow + "+E" + lcRow + "+F" + lcRow + "+G" + lcRow + "+J" + lcRow
*!*	*!*				**********************************************************************************************

*!*	*!*				oWorksheet.RANGE("B"+lcRow,"K"+lcRow).FONT.bold = .T.

*!*	*!*				lnRow = lnRow + 1
*!*	*!*				lcRow = ALLTRIM(STR(lnRow))

*!*	*!*				oWorksheet.RANGE("A"+lcRow).VALUE = ""
*!*	*!*				oWorksheet.RANGE("B"+lcRow).VALUE = "SubTotal"
*!*	*!*				oWorksheet.RANGE("C"+lcRow).VALUE = "Trailers"
*!*	*!*				oWorksheet.RANGE("D"+lcRow).VALUE = IIF(lnTraileroil>0,STR(lnTraileroil,8,2),"0")
*!*	*!*				oWorksheet.RANGE("E"+lcRow).VALUE = IIF(lnTrailertires>0,STR(lnTrailertires,8,2),"0")
*!*	*!*				oWorksheet.RANGE("F"+lcRow).VALUE = IIF(lnTrailerparts>0,STR(lnTrailerparts,8,2),"0")
*!*	*!*				oWorksheet.RANGE("G"+lcRow).VALUE = IIF(lnTrailerWashes>0,STR(lnTrailerWashes,8,2),"0")
*!*	*!*				oWorksheet.RANGE("H"+lcRow).VALUE = IIF(lnTrailerhours>0,STR(lnTrailerhours,8,HDECS),"0")

*!*	*!*				**************************************************************************
*!*	*!*				* format to suppress zeroes
*!*	*!*				oWorksheet.RANGE("D"+lcRow,"H"+lcRow).NumberFormat = "0;-0;;@"
*!*	*!*				**************************************************************************

*!*	*!*				**********************************************************************************************
*!*	*!*				* added 03-04-04 MB per Ken
*!*	*!*				oWorksheet.RANGE("I"+lcRow).VALUE = GetHourlyRateByDivision( lcCurrentdivision )  &&  HOURLY_RATE
*!*	*!*				oWorksheet.RANGE("J"+lcRow).VALUE = "=+I" + lcRow + "*H" + lcRow
*!*	*!*				oWorksheet.RANGE("K"+lcRow).VALUE = "=+D" + lcRow + "+E" + lcRow + "+F" + lcRow + "+G" + lcRow + "+J" + lcRow
*!*	*!*				**********************************************************************************************

*!*	*!*				*oWorksheet.RANGE("A"+lcRow,lcRightMostColumn+lcRow).Interior.ColorIndex = IIF(lnRow%2=1,15,0)

*!*	*!*				oWorksheet.RANGE("B"+lcRow,"K"+lcRow).FONT.bold = .T.

*!*	*!*				lnRow = lnRow + 1
*!*	*!*				lcRow = ALLTRIM(STR(lnRow))

*!*	*!*				oWorksheet.RANGE("A"+lcRow).VALUE = ""
*!*	*!*				oWorksheet.RANGE("B"+lcRow).VALUE = "SubTotal"
*!*	*!*				*oWorksheet.RANGE("C"+lcRow).VALUE = "Misc"
*!*	*!*				oWorksheet.RANGE("C"+lcRow).VALUE = "Forklift"
*!*	*!*				oWorksheet.RANGE("D"+lcRow).VALUE = IIF(lnMiscoil>0,STR(lnMiscoil,8,2),"0")
*!*	*!*				oWorksheet.RANGE("E"+lcRow).VALUE = IIF(lnMisctires>0,STR(lnMisctires,8,2),"0")
*!*	*!*				oWorksheet.RANGE("F"+lcRow).VALUE = IIF(lnMiscparts>0,STR(lnMiscparts,8,2),"0")
*!*	*!*				oWorksheet.RANGE("G"+lcRow).VALUE = IIF(lnMiscWashes>0,STR(lnMiscWashes,8,2),"0")
*!*	*!*				oWorksheet.RANGE("H"+lcRow).VALUE = IIF(lnMischours>0,STR(lnMischours,8,HDECS),"0")
*!*	*!*				*oWorksheet.RANGE("A"+lcRow,lcRightMostColumn+lcRow).Interior.ColorIndex = IIF(lnRow%2=1,15,0)

*!*	*!*				**************************************************************************
*!*	*!*				* format to suppress zeroes
*!*	*!*				oWorksheet.RANGE("D"+lcRow,"H"+lcRow).NumberFormat = "0;-0;;@"
*!*	*!*				**************************************************************************

*!*	*!*				**********************************************************************************************
*!*	*!*				* added 03-04-04 MB per Ken
*!*	*!*				oWorksheet.RANGE("I"+lcRow).VALUE = GetHourlyRateByDivision( lcCurrentdivision )  &&  HOURLY_RATE
*!*	*!*				oWorksheet.RANGE("J"+lcRow).VALUE = "=+I" + lcRow + "*H" + lcRow
*!*	*!*				oWorksheet.RANGE("K"+lcRow).VALUE = "=+D" + lcRow + "+E" + lcRow + "+F" + lcRow + "+G" + lcRow + "+J" + lcRow
*!*	*!*				**********************************************************************************************

*!*	*!*				oWorksheet.RANGE("B"+lcRow,"K"+lcRow).FONT.bold = .T.

*!*	*!*				lnRow = lnRow + 1
*!*	*!*				lcRow = ALLTRIM(STR(lnRow))

*!*	*!*				oWorksheet.RANGE("A"+lcRow).VALUE = ""
*!*	*!*				oWorksheet.RANGE("B"+lcRow).VALUE = "Cost Center Totals"
*!*	*!*				oWorksheet.RANGE("C"+lcRow).VALUE = ""
*!*	*!*				oWorksheet.RANGE("D"+lcRow).VALUE = IIF(lnAlloil>0,STR(lnAlloil,8,2),"0")
*!*	*!*				oWorksheet.RANGE("E"+lcRow).VALUE = IIF(lnAlltires>0,STR(lnAlltires,8,2),"0")
*!*	*!*				oWorksheet.RANGE("F"+lcRow).VALUE = IIF(lnAllparts>0,STR(lnAllparts,8,2),"0")
*!*	*!*				oWorksheet.RANGE("G"+lcRow).VALUE = IIF(lnAllWashes>0,STR(lnAllWashes,8,2),"0")
*!*	*!*				oWorksheet.RANGE("H"+lcRow).VALUE = IIF(lnAllhours>0,STR(lnAllhours,8,HDECS),"0")
*!*	*!*				*oWorksheet.RANGE("A"+lcRow,lcRightMostColumn+lcRow).Interior.ColorIndex = IIF(lnRow%2=1,15,0)

*!*	*!*				************************************************************************
*!*	*!*				* save info for summary section at bottom of spreadsheet
*!*	*!*				INSERT INTO curCompTotals (cDivision, cRow) VALUES (lcCurrentdivision, lcRow)
*!*	*!*				************************************************************************

*!*	*!*				**************************************************************************
*!*	*!*				* format to suppress zeroes
*!*	*!*				oWorksheet.RANGE("D"+lcRow,"H"+lcRow).NumberFormat = "0;-0;;@"
*!*	*!*				**************************************************************************

*!*	*!*				**********************************************************************************************
*!*	*!*				* added 03-04-04 MB per Ken
*!*	*!*				oWorksheet.RANGE("I"+lcRow).VALUE = GetHourlyRateByDivision( lcCurrentdivision )  &&  HOURLY_RATE
*!*	*!*				oWorksheet.RANGE("J"+lcRow).VALUE = "=+I" + lcRow + "*H" + lcRow
*!*	*!*				oWorksheet.RANGE("K"+lcRow).VALUE = "=+D" + lcRow + "+E" + lcRow + "+F" + lcRow + "+G" + lcRow + "+J" + lcRow
*!*	*!*				**********************************************************************************************

*!*	*!*				oWorksheet.RANGE("B"+lcRow,"K"+lcRow).FONT.bold = .T.

*!*	*!*				lnRow = lnRow + 2
*!*	*!*				lcRow = ALLTRIM(STR(lnRow))

*!*	*!*				oWorksheet.RANGE("A"+lcRow).VALUE = ""
*!*	*!*				oWorksheet.RANGE("B"+lcRow).VALUE = "Report Period Totals"
*!*	*!*				oWorksheet.RANGE("C"+lcRow).VALUE = ""
*!*	*!*				oWorksheet.RANGE("D"+lcRow).VALUE = IIF(lnlnOveralloil>0,STR(lnlnOveralloil,8,2),"0")
*!*	*!*				oWorksheet.RANGE("E"+lcRow).VALUE = IIF(lnOveralltires>0,STR(lnOveralltires,8,2),"0")
*!*	*!*				oWorksheet.RANGE("F"+lcRow).VALUE = IIF(lnOverallparts>0,STR(lnOverallparts,8,2),"0")
*!*	*!*				oWorksheet.RANGE("G"+lcRow).VALUE = IIF(lnOverallWashes>0,STR(lnOverallWashes,8,2),"0")
*!*	*!*				oWorksheet.RANGE("H"+lcRow).VALUE = IIF(lnOverallhours>0,STR(lnOverallhours,8,HDECS),"0")
*!*	*!*				*oWorksheet.RANGE("A"+lcRow,lcRightMostColumn+lcRow).Interior.ColorIndex = IIF(lnRow%2=1,15,0)

*!*	*!*				**************************************************************************
*!*	*!*				* format to suppress zeroes
*!*	*!*				oWorksheet.RANGE("D"+lcRow,"H"+lcRow).NumberFormat = "0;-0;;@"
*!*	*!*				**************************************************************************

*!*	*!*				**********************************************************************************************
*!*	*!*				* added 03-04-04 MB per Ken
*!*	*!*				*oWorksheet.RANGE("I"+lcRow).VALUE = GetHourlyRateByDivision( lcCurrentdivision )  &&  HOURLY_RATE

*!*	*!*				oWorksheet.RANGE("J"+lcRow).VALUE = "=+I" + lcRow + "*H" + lcRow
*!*	*!*				oWorksheet.RANGE("J"+lcRow).VALUE = lnOverallDollars

*!*	*!*				oWorksheet.RANGE("K"+lcRow).VALUE = "=+D" + lcRow + "+E" + lcRow + "+F" + lcRow + "+G" + lcRow + "+J" + lcRow
*!*	*!*				**********************************************************************************************

*!*	*!*				oWorksheet.RANGE("B"+lcRow,"K"+lcRow).FONT.bold = .T.

*!*	*!*				**********************************************************************************************
*!*	*!*				**********************************************************************************************
*!*	*!*				* new summary of division totals section 04/14/04 MB
*!*	*!*				lnRow = lnRow + 2
*!*	*!*				lcRow = ALLTRIM(STR(lnRow))
*!*	*!*				lcSummaryHeadingsRow = lcRow
*!*	*!*				lcLastRowInPrintArea = ALLTRIM(STR(lnRow - 1))
*!*	*!*				oWorksheet.RANGE("F" + lcRow).VALUE = DTOC(pdStartDate)+" to "+DTOC(pdEndDate)
*!*	*!*				lnRow = lnRow + 2
*!*	*!*				lcRow = ALLTRIM(STR(lnRow))
*!*	*!*				oWorksheet.RANGE("A" + lcRow).VALUE = "Cost Ctr"
*!*	*!*				oWorksheet.RANGE("D" + lcRow).VALUE = "Oil"
*!*	*!*				oWorksheet.RANGE("E" + lcRow).VALUE = "Tires"
*!*	*!*				oWorksheet.RANGE("F" + lcRow).VALUE = "Parts"
*!*	*!*				oWorksheet.RANGE("G" + lcRow).VALUE = "Washes"
*!*	*!*				oWorksheet.RANGE("H" + lcRow).VALUE = "Hours"
*!*	*!*				oWorksheet.RANGE("I" + lcRow).VALUE = "Rate"
*!*	*!*				oWorksheet.RANGE("J" + lcRow).VALUE = "Cost"
*!*	*!*				oWorksheet.RANGE("K" + lcRow).VALUE = "Total"
*!*	*!*				oWorksheet.RANGE("L" + lcRow).VALUE = "%"
*!*	*!*				oWorksheet.RANGE("A" + lcRow,"L" + lcRow).Interior.ColorIndex = 15

*!*	*!*				lnFirstSubRow = lnRow + 1
*!*	*!*				lcFirstSubRow = ALLTRIM(STR(lnFirstSubRow))
*!*	*!*				SELECT curCompTotals
*!*	*!*				lnFinalRow = lnRow + 2 + RECCOUNT()
*!*	*!*				lcFinalRow = ALLTRIM(STR(lnFinalRow))
*!*	*!*				SCAN
*!*	*!*					lcCompTotRow = ALLTRIM(curCompTotals.cRow)
*!*	*!*					lcDivision = ALLTRIM(curCompTotals.cDivision)
*!*	*!*					lnRow = lnRow + 1
*!*	*!*					lcRow = ALLTRIM(STR(lnRow))
*!*	*!*					lnLastSubRow = lnRow
*!*	*!*					lcLastSubRow = lcRow
*!*	*!*					oWorksheet.RANGE("A" + lcRow).VALUE = "'" + lcDivision
*!*	*!*					oWorksheet.RANGE("D" + lcRow).VALUE = oWorksheet.RANGE("D" + lcCompTotRow).VALUE
*!*	*!*					oWorksheet.RANGE("E" + lcRow).VALUE = oWorksheet.RANGE("E" + lcCompTotRow).VALUE
*!*	*!*					oWorksheet.RANGE("F" + lcRow).VALUE = oWorksheet.RANGE("F" + lcCompTotRow).VALUE
*!*	*!*					oWorksheet.RANGE("G" + lcRow).VALUE = oWorksheet.RANGE("G" + lcCompTotRow).VALUE
*!*	*!*					oWorksheet.RANGE("H" + lcRow).VALUE = oWorksheet.RANGE("H" + lcCompTotRow).VALUE
*!*	*!*					oWorksheet.RANGE("I" + lcRow).VALUE = oWorksheet.RANGE("I" + lcCompTotRow).VALUE
*!*	*!*					oWorksheet.RANGE("J" + lcRow).VALUE = oWorksheet.RANGE("J" + lcCompTotRow).VALUE
*!*	*!*					oWorksheet.RANGE("K" + lcRow).VALUE = oWorksheet.RANGE("K" + lcCompTotRow).VALUE
*!*	*!*					oWorksheet.RANGE("L" + lcRow).VALUE = "=+K" + lcRow + "/K" + lcFinalRow
*!*	*!*					oWorksheet.RANGE("L" + lcRow).NumberFormat = "0%"
*!*	*!*				ENDSCAN

*!*	*!*				oWorksheet.RANGE("A" + lcFinalRow).VALUE = "Total Allocated"
*!*	*!*				oWorksheet.RANGE("D" + lcFinalRow).VALUE = "=SUM(D" + lcFirstSubRow + ":D" + lcLastSubRow + ")"
*!*	*!*				oWorksheet.RANGE("E" + lcFinalRow).VALUE = "=SUM(E" + lcFirstSubRow + ":E" + lcLastSubRow + ")"
*!*	*!*				oWorksheet.RANGE("F" + lcFinalRow).VALUE = "=SUM(F" + lcFirstSubRow + ":F" + lcLastSubRow + ")"
*!*	*!*				oWorksheet.RANGE("G" + lcFinalRow).VALUE = "=SUM(G" + lcFirstSubRow + ":G" + lcLastSubRow + ")"
*!*	*!*				oWorksheet.RANGE("H" + lcFinalRow).VALUE = "=SUM(H" + lcFirstSubRow + ":H" + lcLastSubRow + ")"
*!*	*!*				oWorksheet.RANGE("J" + lcFinalRow).VALUE = "=SUM(J" + lcFirstSubRow + ":J" + lcLastSubRow + ")"
*!*	*!*				oWorksheet.RANGE("K" + lcFinalRow).VALUE = "=SUM(K" + lcFirstSubRow + ":K" + lcLastSubRow + ")"
*!*	*!*				oWorksheet.RANGE("L" + lcFinalRow).VALUE = "=+K" + lcFinalRow + "/K" + lcFinalRow
*!*	*!*				oWorksheet.RANGE("L" + lcFinalRow).NumberFormat = "0%"
*!*	*!*				**********************************************************************************************
*!*	*!*				**********************************************************************************************

*!*	*!*				**********************************************************************************************
*!*	*!*				**********************************************************************************************
*!*	*!*				* MOVE TOTALS UP TO TOP PER KEN 11/02/05
*!*	*!*				*!*			RANGE("A24:K35").SELECT
*!*	*!*				*!*			SELECTION.COPY
*!*	*!*				*!*			RANGE("A4").SELECT
*!*	*!*				*!*			ActiveSheet.Paste

*!*	*!*				*!*			oWorksheet.RANGE("A" + lcFirstSubRow + ":" + "L" + lcFinalRow).SELECT
*!*	*!*				*!*			oWorksheet.SELECTION.COPY
*!*	*!*				*!*			oWorksheet.RANGE("A6").SELECT
*!*	*!*				*!*			oWorksheet.Paste

*!*	*!*				oWorksheet.RANGE("A" + lcFirstSubRow + ":" + "L" + lcFinalRow).COPY(oWorksheet.RANGE("A6"))
*!*	*!*				oWorksheet.RANGE("A" + lcSummaryHeadingsRow + ":" + "L" + lcFinalRow).clearcontents

*!*	*!*				*!*		Range("a1").Copy Destination:=Range("c3")
*!*	*!*				*!*		Range("a1").ClearContents

*!*	*!*				**********************************************************************************************
*!*	*!*				**********************************************************************************************


*!*	*!*				*!*			lnRow = lnFinalRow + 1
*!*	*!*				*!*			lcRow = ALLTRIM(STR(lnRow))

*!*	*!*				lcRow = lcLastRowInPrintArea

*!*	*!*				*oWorksheet.RANGE("A1","L"+lcRow).SELECT
*!*	*!*				*oWorksheet.RANGE("A1","M"+lcRow).SELECT
*!*	*!*				*oWorksheet.PageSetup.PrintArea = "$A$1:$L$" + lcRow
*!*	*!*				*oWorksheet.PageSetup.PrintArea = "$A$1:$M$" + lcRow
*!*	*!*				oWorksheet.RANGE("A1",lcRightMostColumn+lcRow).SELECT
*!*	*!*				oWorksheet.PageSetup.PrintArea = "$A$1:$" + lcRightMostColumn +"$" + lcRow

*!*	*!*				WAIT CLEAR

*!*	*!*				MESSAGEBOX("The Allocation Spreadsheet has been populated.",0 + 64,lcTitle)
*!*	*!*				oWorkbook.SAVE()
*!*	*!*				oExcel.VISIBLE = .T.
*!*	*!*			ELSE
*!*	*!*				MESSAGEBOX("No Order Header data was found for that date range.",0 + 64,lcTitle)
*!*	*!*			ENDIF  && USED('curVCT') and RECCOUNT(curVCT) > 0

*!*	*!*		CATCH TO loError

*!*	*!*			lcErr = 'There was an error.' + CRLF
*!*	*!*			lcErr = lcErr + 'The Error # is: ' + TRANSFORM(loError.ERRORNO) + CRLF
*!*	*!*			lcErr = lcErr + 'The Error Message is: ' + TRANSFORM(loError.MESSAGE) + CRLF
*!*	*!*			lcErr = lcErr + 'The Error occurred in Program: ' + TRANSFORM(loError.PROCEDURE) + CRLF
*!*	*!*			lcErr = lcErr + 'The Error occurred at line #: ' + TRANSFORM(loError.LINENO)
*!*	*!*			MESSAGEBOX(lcErr,0+16,lcTitle)
*!*	*!*			*oExcel.VISIBLE = .T.
*!*	*!*			oExcel.QUIT()

*!*	*!*		FINALLY

*!*	*!*			IF USED('curVCT')
*!*	*!*				USE IN curVCT
*!*	*!*			ENDIF

*!*	*!*		ENDTRY

*!*	*!*	ENDIF   &&  llRateByDate
*!*	RETURN

*!*	********************************************************************************************
*!*	* revised 02/23/04 MB
*!*	PROCEDURE CheckCompanies()

*!*		* Modified 2/28/06 to change WO's with blank Vehno's to type = BLDG

*!*		SELECT ordhdr
*!*		GOTO TOP
*!*		SET EXACT ON

*!*		SCAN FOR (crdate >= pdStartDate) AND (crdate <= pdEndDate)
*!*			WAIT WINDOW "checking Order #" + TRANSFORM(ordhdr.ORDNO) NOWAIT

*!*			* deactivated this because I don't want to change the ordhdr.type or ordhdr.company
*!*			* if we can't find the vehno in truck/trailer/vehmast. 02/24/04 MB
*!*			*REPLACE ordhdr.company WITH " ", ordhdr.TYPE WITH "UNKNOWN"

*!*			IF EMPTY(ordhdr.vehno) THEN

*!*				* no vehicle # - make it a BLDG maintenance WO
*!*				REPLACE ordhdr.company WITH "", ordhdr.TYPE WITH "BLDG"

*!*			ELSE

*!*				SELECT trucks
*!*				LOCATE FOR ALLTRIM(ordhdr.vehno) = ALLTRIM(trucks.truck_num)
*!*				IF FOUND() THEN
*!*					REPLACE ordhdr.company WITH trucks.company, ordhdr.TYPE WITH "TRUCK"
*!*					LOOP
*!*				ENDIF
*!*				SELECT trucks
*!*				LOCATE

*!*				SELECT trailer
*!*				LOCATE FOR ALLTRIM(ordhdr.vehno) = ALLTRIM(trailer.trail_num)
*!*				IF FOUND() THEN
*!*					REPLACE ordhdr.company WITH trailer.company, ordhdr.TYPE WITH "TRAILER"
*!*					LOOP
*!*				ENDIF
*!*				SELECT trailer
*!*				LOCATE

*!*				SELECT vehmast
*!*				LOCATE FOR ALLTRIM(ordhdr.vehno) = ALLTRIM(vehmast.vehno)
*!*				IF FOUND() THEN
*!*					REPLACE ordhdr.company WITH vehmast.company, ordhdr.TYPE WITH "MISC"
*!*					LOOP
*!*				ENDIF
*!*				SELECT vehmast
*!*				LOCATE

*!*			ENDIF &&  EMPTY(ordhdr.vehno)


*!*		ENDSCAN

*!*	ENDPROC

*!*	* OLD AS OF 2/23/04 MB
*!*	*!*	PROCEDURE CheckCompanies()

*!*	*!*		LOCAL llFound

*!*	*!*		SELECT ordhdr
*!*	*!*		GOTO TOP
*!*	*!*		SET EXACT ON

*!*	*!*		SCAN FOR (crdate >= pdStartDate) AND (crdate <= pdEndDate)
*!*	*!*			WAIT WINDOW "checking Order #" + TRANSFORM(ordhdr.ordno) NOWAIT

*!*	*!*			REPLACE ordhdr.TYPE WITH "MISC", ordhdr.company WITH "I" IN ordhdr

*!*	*!*			SELECT trucks
*!*	*!*			LOCATE FOR ALLTRIM(ordhdr.vehno) = ALLTRIM(trucks.truck_num)
*!*	*!*			IF FOUND()
*!*	*!*				REPLACE ordhdr.company WITH trucks.company, ordhdr.TYPE WITH "TRUCK"
*!*	*!*			ENDIF

*!*	*!*			SELECT trailer
*!*	*!*			LOCATE FOR ALLTRIM(ordhdr.vehno) = ALLTRIM(trailer.trail_num)
*!*	*!*			IF FOUND()
*!*	*!*				REPLACE ordhdr.company WITH trailer.company, ordhdr.TYPE WITH "TRAILER"
*!*	*!*			ENDIF
*!*	*!*
*!*	*!*		ENDSCAN

*!*	*!*	ENDPROC

*!*	********************************************************************************************
*!*	PROCEDURE CheckWOsums()

*!*		LOCAL lnTirePrice, lnRepairPrice, lnOilPrice

*!*		WAIT WINDOW "Summing up repair parts..." NOWAIT

*!*		SELECT repdetl
*!*		SET ORDER TO repdate
*!*		SCAN FOR (repdate  >= pdStartDate) AND (repdate <= pdEndDate)
*!*			REPLACE extprice WITH (repqty * unitprice)
*!*		ENDSCAN
*!*		SELECT repdetl
*!*		LOCATE

*!*		WAIT WINDOW "Summing up tire parts...." NOWAIT
*!*		SELECT tiredetl
*!*		SCAN FOR (tiredate >= pdStartDate) AND (tiredate <= pdEndDate)
*!*			REPLACE extprice WITH (tireqty * unitprice)
*!*		ENDSCAN
*!*		SELECT tiredetl
*!*		LOCATE

*!*		WAIT WINDOW "Summing up oil parts..." NOWAIT
*!*		SELECT oildetl
*!*		SCAN FOR (oildate >= pdStartDate) AND (oildate <= pdEndDate)
*!*			REPLACE extprice WITH (oilqty * unitprice)
*!*		ENDSCAN
*!*		SELECT oildetl
*!*		LOCATE

*!*		SELECT ordhdr
*!*		** 8/23/06 MB
*!*		*SCAN FOR (crdate >= pdStartDate) AND (crdate <= pdEndDate)
*!*		SCAN FOR (crdate >= pdStartDate) AND (crdate <= pdEndDate) AND (NOT EMPTY(ORDNO))

*!*			WAIT WINDOW "checking Work Order Sums for Order #" + TRANSFORM(ordhdr.ORDNO) + "..." NOWAIT

*!*			SELECT repdetl
*!*			GOTO TOP
*!*			SUM ALL repdetl.extprice FOR repdetl.ORDNO == ordhdr.ORDNO TO lnRepairPrice
*!*			SELECT repdetl
*!*			LOCATE
*!*			REPLACE ordhdr.partcost WITH lnRepairPrice IN ordhdr

*!*			SELECT tiredetl
*!*			GOTO TOP
*!*			SUM ALL tiredetl.extprice FOR tiredetl.ORDNO == ordhdr.ORDNO TO lnTirePrice
*!*			SELECT tiredetl
*!*			LOCATE
*!*			REPLACE ordhdr.tirecost WITH lnTirePrice IN ordhdr

*!*			SELECT oildetl
*!*			GOTO TOP
*!*			SUM ALL oildetl.extprice FOR oildetl.ORDNO == ordhdr.ORDNO TO lnOilPrice
*!*			SELECT oildetl
*!*			LOCATE
*!*			REPLACE ordhdr.oilcost WITH lnOilPrice IN ordhdr

*!*			REPLACE ordhdr.totpcost WITH (lnRepairPrice + lnTirePrice + lnOilPrice) IN ordhdr

*!*		ENDSCAN

*!*	ENDPROC


*!*	*!*	*!*	FUNCTION GetHourlyRateByCRDateAndDiv
*!*	*!*	*!*		LPARAMETERS tdCRdate, tcDivision
*!*	*!*	*!*		DO CASE
*!*	*!*	*!*			CASE tcDivision = '50' AND BETWEEN(tdCRdate,{^2013-07-01},{^2099-12-31})
*!*	*!*	*!*				lnRate = 0.00
*!*	*!*	*!*			CASE tdCRdate < {^2008-09-01}
*!*	*!*	*!*				lnRate = 60.00
*!*	*!*	*!*			OTHERWISE
*!*	*!*	*!*				lnRate = 70.00  
*!*	*!*	*!*		ENDCASE
*!*	*!*	*!*		RETURN lnRate
*!*	*!*	*!*	ENDFUNC


*!*	*!*	FUNCTION GetHourlyRateByCRDate
*!*	*!*		LPARAMETERS tdCRdate
*!*	*!*		DO CASE
*!*	*!*			CASE tdCRdate < {^2008-09-01}
*!*	*!*				lnRate = 60.00
*!*	*!*			OTHERWISE
*!*	*!*				lnRate = 70.00  
*!*	*!*		ENDCASE
*!*	*!*		RETURN lnRate
*!*	*!*	ENDFUNC


*!*	*!*	FUNCTION GetHourlyRateByDivision
*!*	*!*		LPARAMETERS tcDivision
*!*	*!*		DO CASE
*!*	*!*			CASE INLIST(tcDivision,'05','52','56','57','58','71','74')
*!*	*!*				lnRate = 60
*!*	*!*			OTHERWISE
*!*	*!*				lnRate = 60  && the default rate
*!*	*!*		ENDCASE
*!*	*!*		RETURN lnRate
*!*	*!*	ENDFUNC
*!*	*!*	********************************************************************************************
