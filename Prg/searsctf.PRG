* processes Sears clean truck fee spreadsheet 

close databases all
set deleted on
clear

use f:\wo\wodata\wolog in 0
use f:\wo\wodata\tdaily in 0
use f:\watusi\ardata\invoice in 0

create cursor xsears (remit c(4), container c(11))
create cursor xrpt (container c(11), invnum c(20))

xfilename = getfile("xls")

if empty(xfilename)
	return
endif

select xsears
if !appendxls(xfilename)
	x3winmsg("Format is incorrect")
	return
endif

xmsg=""

select xsears
scan for inlist(remit,"C0","C1")
	m.container=xsears.container
	select wolog
	locate for accountid=5837 and wo_date>gomonth(date(),-3) and container=xsears.container
	if !found()
		xmsg=xmsg+xsears.container+crlf
		m.invnum="CONTAINER NOT FOUND"
		insert into xrpt from memvar
	else
		select invoice
		locate for wo_num=wolog.wo_num and invnum="S"
		if found()
			m.invnum=invoice.invnum
		else
			m.invnum="INVOICE NOT FOUND"
		endif
		insert into xrpt from memvar
	endif
endscan

select xrpt
index on invnum tag invnum

xfilename="h:\fox\searsctf.pdf"
delete file (xfilename)
rpt2pdf("searsctf",xfilename)
wshshell=createobject("wscript.shell")
wshshell.run(xfilename)

if !empty(xmsg)
	email("Dyoung@fmiint.com","Sears clean truck container not found",xmsg,,,,.t.,,,,,.t.)
endif
