PARAMETERS xfile
#DEFINE xlExcel5 39

IF VARTYPE(xfile) = "L"
	WAIT WIND 'No file parameter passed...exiting'
	RETURN
ENDIF
WAIT WIND "File extension: "+JUSTEXT(xfile) TIME 2
xfile1short = JUSTFNAME(xfile)
xfileout = ADDBS(JUSTPATH(xfile))+JUSTSTEM(xfile)+".xls"

oExcel = CREATEOBJECT("Excel.Application")
oExcel.DisplayAlerts = .F.
oWorkbook = oExcel.Workbooks.OPEN(xfile,,.F.)
WAIT WINDOW "Workbook: "+xfile1short TIMEOUT 1
oWorkbook.SAVEAS(xfileout,39)  && re-saves file as Excel 5.0/95
oWorkbook.CLOSE()
oExcel.QUIT()
RELEASE oExcel

ASSERT .F. MESSAGE "At conclusion of Excel XLSX conversion...>>DEBUG<<"
*SET STEP ON
COPY FILE [&xfile] TO [&archivefile]
DELETE FILE [&xfile]
STORE xfileout TO xfile
WAIT WIND "New Excel file = "+xfile TIMEOUT 2
