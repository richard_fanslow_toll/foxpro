*!* m:\dev\prg\ragbone940_process.prg

CD &lcPath
DELETE FILE fox*.dbf  && Eliminates Foxuser files
LogCommentStr = ""

*!*	delimchar = cDelimiter
*!*	lcTranOpt= cTranslateOption

lnNum = ADIR(tarray,"*.*")

IF lnNum = 0
	WAIT WINDOW "No RAGBONE 940s in folder "+lcPath+ "  to import     " TIMEOUT 3
	NormalExit = .T.
	THROW
ENDIF

FOR thisfile = 1  TO lnNum
	STORE "" TO xfile,cFilename
	STORE 0 TO nFileSize
	xfile = lcPath+tarray[thisfile,1] && +"."
	nFileSize = VAL(TRANSFORM(tarray(thisfile,2)))
	cFilename = ALLTRIM(LOWER(tarray[thisfile,1]))
	xfile = lcPath+tarray[thisfile,1] && +"."
	dFileDate = tarray[thisfile,3]
	cFileTime = tarray[thisfile,4]

	archivefile  = (lcArchivepath+cFilename)
	WAIT WINDOW "Importing file: "+cFilename NOWAIT
	
	DO m:\dev\prg\createx856a
	IF lTesting
	DO m:\dev\prg\loadedifile WITH xfile,"*","PIPE4","RAGBONE"
	else
	DO m:\dev\prg\loadedifile WITH xfile,"*","PIPE3","RAGBONE"
	ENDIF 
	
	IF lTesting
		SELECT x856
		LOCATE
		BROW
	ENDIF

	IF !lTesting
*		COPY FILE [&xfile] TO ("F:\ftpusers\BBC\940Translate\"+cfilename) && to create 997
	ENDIF

	SELECT x856
	LOCATE FOR x856.segment = 'GS'
	IF x856.f1 = "FA"
		WAIT WINDOW "This is a 997 FA file...moving to correct folder" TIMEOUT 1
		cfile997in = ("F:\ftpusers\"+cCustname+"\997in\"+cFilename)
		COPY FILE [&xfile] TO [&cfile997in]
		DELETE FILE [&xfile]
		LOOP
	ELSE
		LOCATE
	ENDIF

	cUseFolder = "f:\whi\whdata\"
	DO create_pt_cursors WITH cUseFolder,lLoadSQL && Added to prep for SQL Server transition

	lOK = .T.
	DO ("m:\dev\prg\ragbone940_bkdn")
	
	IF lOK && If no divide-by-zero error
		DO m:\dev\prg\all940_import
	ENDIF

ENDFOR

&& now clean up the 940in archive files, delete for the upto the last 10 days
deletefile(lcArchivepath,20)

WAIT CLEAR
WAIT "Entire "+cMailname+" 940 Process Now Complete" WINDOW TIMEOUT 3