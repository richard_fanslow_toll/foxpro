*!* S3 943_CREATE.PRG
PARAMETERS  cGroup

PUBLIC lXfer943,cFilename,tsendto,tcc,tsendtoerr,tccerr,tsendtotest,tcctest,lIsError,nOrigAcctNum,nDivLoops,cInFolder,cWhse,nRecLU,lTesting
PUBLIC lDoMail,tfrom,tattach,cxErrMsg,NormalExit,Acctname,lPick,lBrowFiles,cTransfer,cArchivePath,lcPath,cContainer,cAcct_ref,cMod,cWhseMod
PUBLIC lDoSQL,gMasterOffice
PUBLIC ARRAY a856(1)

CLOSE DATABASES ALL
_SCREEN.CAPTION = "S3 943 UPLOAD"


lTesting = .T. && If TRUE, disables certain functions
lOverrideBusy = lTesting
*lOverrideBusy = .f.
lBrowFiles = lTesting
*lBrowfiles = .t.
IF VARTYPE(cGroup) # "C"
	cGroup = "VSR"
ENDIF

DO m:\dev\prg\_setvars WITH lTesting
ON ERROR THROW

*SET STEP ON

IF lTesting
	WAIT WINDOW "This is a "+IIF(lTesting,"TEST","PRODUCTION")+" upload" TIMEOUT 2
ENDIF

TRY
	DO m:\dev\prg\lookups
	tfrom = "Toll/FMI EDI Operations <toll-edi-ops@tollgroup.com>"
	tattach = " "
	cOffice = "C"
	cMod = "2"
	gOffice = cMod
	gMasterOffice = cOffice
	cWhseMod = LOWER("wh"+cMod)

	lDoSQL = .T.

	lTestmail = lTesting
	lBrowFiles = lTesting
	lIsError = .F.
	lDoMail = .T.  && If .t., sends mail
	NormalExit = .F.
	lPick = .F.
	nDivLoops = 0
	nRecLU = 0
	cMasterComments = ""
	cContainer = ""
	cReference = ""

	nAcctNum =6803
	cxErrMsg = ""
	cCustName = "S3 "+cGroup  && Customer Identifier (for folders, etc.)
	cMailName = cCustName
	cTransfer ="943-S3-"+cGroup

	IF !lTesting
		SELECT 0
		USE F:\edirouting\ftpsetup
		LOCATE FOR ftpsetup.transfer = cTransfer
		IF ftpsetup.chkbusy AND !lTesting AND !lOverrideBusy
			WAIT WINDOW "Process is busy...exiting" TIMEOUT 2
			CLOSE DATA ALL
			NormalExit = .T.
			THROW
		ENDIF
		REPLACE chkbusy WITH .T.,trig_time WITH DATETIME()  FOR transfer = cTransfer
		USE IN ftpsetup
	ENDIF

	setuprcvmail(cGroup)
	lcPath = IIF(lTesting,STRTRAN(UPPER(lcPath),"943IN","943INTEST"),lcPath)
	cArchivePath = IIF(lTesting,STRTRAN(UPPER(lcPath),"943IN","943INTEST"),cArchivePath)

	len1 = ADIR(ary1,lcPath+"*")
	IF len1 = 0
		WAIT WINDOW "There are no 943 files to process...exiting" TIMEOUT 2
		CLOSE DATABASES ALL
		NormalExit = .T.
		THROW
	ENDIF
	WAIT WINDOW "There are "+TRANSFORM(len1)+" 943 files to process" NOWAIT &&TIMEOUT 2


	FOR kk = 1 TO len1
		cContainer = ""
		cAcct_ref = ""
		cFilename = ALLTRIM(ary1[kk,1])
		cArchiveFile  = (cArchivePath+cFilename)
		dFiledate = ary1[kk,3]
		xfile = lcPath+cFilename

		DO m:\dev\prg\createx856a

		loadedifile(xfile,"*","NOTILDE","S3")

		SELECT x856
		LOCATE

		SELECT x856
		DELETE FOR INLIST(segment,"ISA","GS","ST","SE","GE","IEA")
		SET DELETED OFF
		GO BOTT
		RECALL
		SET DELETED ON
		SELECT x856
		GO TOP


		SELECT x856

		IF lTesting
			cUseFolder = "F:\WHP\WHDATA\"
		ELSE
			xReturn = "XXX"
			DO m:\dev\prg\wf_alt WITH cOffice,nAcctNum
			IF xReturn = "XXX"
				SET STEP ON
				cxErrMsg = "NO WHSE FOUND IN WMS"
				errormail()
				RETURN
			ENDIF
			cUseFolder = UPPER(xReturn)
		ENDIF

		useca("inwolog","wh")
		xsqlexec("select * from inwolog where .f.","xinwolog",,"wh")

		useca("pl","wh")
		xsqlexec("select * from pl where .f.","xpl",,"wh")

		useca("inrcv","wh")


		xsqlexec("select * from account where inactive=0","account",,"qq")
		INDEX ON accountid TAG accountid
		SET ORDER TO

		SELECT xinwolog
		SCATTER MEMVAR BLANK
		=SEEK(nAcctNum,'account','accountid')
		m.Acctname = account.Acctname
		m.accountid = account.accountid
		SELECT pl
		STORE "FILE943*"+cFilename TO m.comments
		STORE m.comments+CHR(13)+"FILETIME*"+TTOC(DATETIME()) TO m.comments
		STORE 0 TO m.inwologid,m.plid,npo,m.wo_num
		STORE "" TO m.echo,m.po
		STORE "TOLLPROC" TO m.addby
		STORE DATETIME() TO m.adddt
		STORE "S3_943" TO m.addproc
		STORE DATE() TO m.wo_date,m.date_rcvd
		m.po ="0"
		lcCurrentContainer = ""
		m.container =""

		llReturn = .F.

		SELECT x856
		DO WHILE !EOF("x856")

			DO CASE

			CASE x856.segment = "W06"
				m.acct_ref = ALLT(x856.f4)
				STORE m.acct_ref TO cAcct_ref
				m.ponum = ALLTRIM(x856.f6)
				m.comments = m.comments+CHR(13)+"PONUM*"+m.ponum
				m.office = cOffice
				m.mod = cMod
			CASE x856.segment = "W27"
				m.container = LEFT(ALLT(x856.f5),11)
				IF lcCurrentContainer  != m.container
					SELECT xinwolog
					LOCATE FOR CONTAINER = m.container
					IF FOUND()
						lcCurrentContainer  = m.container
						m.inwologid = xinwolog.inwologid
					ELSE
						m.wo_num = m.wo_num + 1
						m.inwologid = m.inwologid + 1
						INSERT INTO xinwolog FROM MEMVAR
						lcCurrentContainer  = m.container
					ENDIF
				ELSE

				ENDIF

			CASE x856.segment = "W04"
				lcEachType = ALLTRIM(x856.f2)
				m.plid = m.plid + 1
				m.totqty = INT(VAL(x856.f1))
				REPLACE xinwolog.plinqty WITH xinwolog.plinqty+m.totqty IN xinwolog
				m.units = IIF(x856.f2="CT",.F.,.T.)
				m.echo = "UPC*"+ALLT(x856.f5)
				m.style = ALLT(x856.f3)

				SKIP 1 IN x856
				IF TRIM(x856.segment) = "N9" AND TRIM(x856.f1) = "CL"
					m.color =  ALLT(x856.f2)
				ENDIF

				SKIP 1 IN x856
				IF TRIM(x856.segment) = "N9" AND TRIM(x856.f1) = "SZ"
					m.id =  ALLT(x856.f2)
				ENDIF

				SKIP 1 IN x856
				IF TRIM(x856.segment) = "W20"
					m.pack= TRANSFORM(VAL(ALLTRIM(x856.f1)))
				ENDIF

				m.echo = m.echo+CHR(13)+"PONUM*"+ALLTRIM(m.ponum)

				lnPackval= VAL(ALLT(x856.f1))

				DO CASE
				CASE m.totqty%lnPackval = 0 AND m.totqty >= lnPackval AND lcEachType = "EA"
					m.po = TRANSFORM(VAL(m.po)+1)
					m.totqty = m.totqty /lnPackval
					m.units = .F.
					INSERT INTO xpl FROM MEMVAR
					IF xpl.inwologid = 0
						REPLACE xpl.inwologid WITH 1 IN xpl
					ENDIF
					REPLACE xpl.ECHO WITH xpl.ECHO+CHR(13)+ALLTRIM(m.ponum)+"*"+TRANSFORM(m.totqty) IN xpl
					m.units = .T.
					m.pack="1"
					m.totqty = m.totqty*lnPackval
					m.plid = m.plid + 1
					m.office = cOffice
					m.mod = cMod

					INSERT INTO xpl FROM MEMVAR
					IF xpl.inwologid = 0
						REPLACE xpl.inwologid WITH 1 IN xpl
					ENDIF

				CASE m.totqty<lnPackval  AND lcEachType = "EA"
					lnThisQty = m.totqty
					m.po = TRANSFORM(VAL(m.po)+1)
					m.pack= TRANSFORM(m.totqty)  && was m.totqty
					m.totqty = 1
					m.units = .F.
					INSERT INTO xpl FROM MEMVAR
					IF xpl.inwologid = 0
						REPLACE xpl.inwologid WITH  1 IN xpl
					ENDIF
					REPLACE xpl.ECHO WITH xpl.ECHO+CHR(13)+ALLTRIM(m.ponum)+"*"+TRANSFORM(m.totqty) IN xpl
					m.units = .T.
					m.pack="1"
					m.totqty = lnThisQty
					m.plid = m.plid + 1
					INSERT INTO xpl FROM MEMVAR
					IF xpl.inwologid = 0
						REPLACE xpl.inwologid WITH 1 IN xpl
					ENDIF

				CASE m.totqty>lnPackval AND m.totqty%lnPackval != 0   AND lcEachType = "EA" && at least one carton with a residual carton
					m.po = TRANSFORM(VAL(m.po)+1)
					orig_eaches = m.totqty
					m.totqty = INT(m.totqty /lnPackval)
					lnleftover = orig_eaches-(m.totqty*lnPackval)

					m.units = .F.
					INSERT INTO xpl FROM MEMVAR  && #1, CTN
					IF xpl.inwologid = 0
						REPLACE xpl.inwologid WITH 1 IN xpl
					ENDIF
					REPLACE xpl.ECHO WITH xpl.ECHO+CHR(13)+ALLTRIM(m.ponum)+"*"+TRANSFORM(m.totqty) IN xpl
					m.units = .T.
					m.pack="1"
					m.totqty = m.totqty*lnPackval
					m.plid = m.plid + 1

					INSERT INTO xpl FROM MEMVAR   && #1, UNITS
					IF xpl.inwologid = 0
						REPLACE xpl.inwologid WITH 1 IN xpl
					ENDIF
					REPLACE xpl.ECHO WITH xpl.ECHO+CHR(13)+ALLTRIM(m.ponum)+"*"+TRANSFORM(m.totqty) IN xpl

					m.pack = ALLTRIM(TRANSFORM(lnleftover))
					m.totqty = 1
					m.plid = m.plid + 1
					m.units = .F.
					m.po = TRANSFORM(VAL(m.po)+1)

					INSERT INTO xpl FROM MEMVAR  && #2, CTN, RESIDUAL
					IF xpl.inwologid = 0
						REPLACE xpl.inwologid WITH  1 IN xpl
					ENDIF
					REPLACE xpl.ECHO WITH xpl.ECHO+CHR(13)+ALLTRIM(m.ponum)+"*"+TRANSFORM(m.totqty) IN xpl
					m.units = .T.
					m.pack="1"
					m.totqty = lnleftover
					m.plid = m.plid + 1
					INSERT INTO xpl FROM MEMVAR     && #2, UNITS, RESIDUAL
					IF xpl.inwologid = 0
						REPLACE xpl.inwologid WITH 1 IN xpl
					ENDIF
					REPLACE xpl.ECHO WITH xpl.ECHO+CHR(13)+ALLTRIM(m.ponum)+"*"+TRANSFORM(m.totqty) IN xpl

				CASE lcEachType = "CT"
					m.po = TRANSFORM(VAL(m.po)+1)
					m.totqty = m.totqty && the qty is the actual carton qty,
					m.units = .F.
					INSERT INTO xpl FROM MEMVAR
					IF xpl.inwologid = 0
						REPLACE xpl.inwologid WITH 1 IN xpl
					ENDIF
					REPLACE xpl.ECHO WITH xpl.ECHO+CHR(13)+ALLTRIM(m.ponum)+"*"+TRANSFORM(m.totqty) IN xpl
					m.units = .T.
					m.pack="1"
					m.totqty = m.totqty*lnPackval
					m.plid = m.plid + 1
					INSERT INTO xpl FROM MEMVAR
					IF xpl.inwologid = 0
						REPLACE xpl.inwologid WITH 1 IN xpl
					ENDIF

				ENDCASE
			ENDCASE
			SKIP 1 IN x856
		ENDDO


		IF lTesting
			SELECT xpl
		ENDIF

		xplrollup()

		IF lTesting OR lBrowFiles
			SELECT xinwolog
			DELETE FOR plinqty = 0
			LOCATE
			BROWSE
			SELECT xpl
			DELETE FOR totqty = 0
			LOCATE
			BROWSE
			IF lTesting
				CANCEL
			ENDIF
		ENDIF

		SELECT xinwolog
		LOCATE
		SCAN
			xAcct_Num =  xinwolog.accountid
			xAcct_ref = xinwolog.acct_ref
			xContainer = xinwolog.CONTAINER
			WAIT WINDOW "AT XINWOLOG RECORD "+ALLTRIM(STR(RECNO())) NOWAIT NOCLEAR
			csq1 = [select * from inwolog where accountid = ]+ALLTRIM(STR(xAcct_Num))+[ and container = ']+xContainer+[' and acct_ref = ']+xAcct_ref+[']
			xsqlexec(csq1,"i1",,"wh")
			IF RECCOUNT("i1")>0
				cWO = TRANSFORM(i1.wo_num)
				dupemail(cWO)
				SET STEP ON
				ninwologid = xinwolog.inwologid
				DELETE FOR xinwolog.inwologid = ninwologid
				SELECT xpl
				DELETE FOR xpl.inwologid = ninwologid
				SELECT xinwolog
				WAIT WINDOW "This set of values already exists in the INWOLOG table...looping" TIMEOUT 2
				LOOP
			ENDIF
		ENDSCAN
		nUploadCount = 0

		SELECT xinwolog
		LOCATE
		WAIT WINDOW "Currently in "+UPPER(IIF(lTesting,"test","production"))+" mode, base folder: "+cUseFolder TIMEOUT 2

		SCAN  FOR !DELETED() && Scanning xinwolog here
			SCATTER MEMVAR MEMO
			cPLCtns = ALLTRIM(STR(m.plinqty))
			cPLUnits = ALLTRIM(STR(m.plunitsinqty))
			nWO_num  = dygenpk("wonum",cWhseMod)

			cWO_Num = ALLTRIM(STR(nWO_num))
			ninwologid = dygenpk("inwolog",cWhseMod)

			m.wo_num = nWO_num
			SET ORDER TO
			insertinto("inwolog","wh",.T.)
			nUploadCount = nUploadCount+1

			SELECT xpl
			SCAN FOR inwologid=xinwolog.inwologid
				SCATTER FIELDS EXCEPT inwologid MEMVAR MEMO
				m.wo_num = nWO_num
				m.inwologid = inwolog.inwologid
				insertinto("pl","wh",.T.)
			ENDSCAN
*    goodmail()
		ENDSCAN

		IF nUploadCount = 0
			SET STEP ON
			IF FILE(xfile)
				COPY FILE [&xfile] TO [&cArchivefile]
				IF FILE(cArchiveFile) AND !lTesting
					DELETE FILE [&xfile]
				ENDIF
			ENDIF
			EXIT
		ENDIF

		COPY FILE [&xfile] TO [&cArchivefile]
		IF FILE(cArchiveFile) AND !lTesting
			DELETE FILE [&xfile]
		ENDIF

		cWO_Num = TRANSFORM(nWO_num)
		SELECT xinwolog
		cPLCtns = TRANSFORM(xinwolog.plinqty)
		cPLUnits = TRANSFORM(xinwolog.plunitsinqty)

		SELECT inwolog

		IF !EMPTY(inwolog.CONTAINER)
			SELECT pl
			COUNT TO xnumskus FOR !units AND wo_num=inwolog.wo_num

			xsqlexec("select * from inrcv where container='"+inwolog.CONTAINER+"' and status#'CONFIRMED'","xinrcv",,"wh")
			SELECT xinrcv
			IF RECCOUNT() > 0
				xsqlexec("update inrcv set inwonum="+TRANSFORM(inwolog.wo_num)+" where container='"+inwolog.CONTAINER+"' and status#'CONFIRMED'",,,"wh")
			ELSE
				SELECT inrcv
				SCATTER MEMVAR MEMO BLANK
				m.mod = cMod
				m.office = cOffice
				m.inrcvid=dygenpk("inrcv","whall")  && new get the ID value
				m.inwonum=inwolog.wo_num
				m.accountid=inwolog.accountid
				m.Acctname=inwolog.Acctname
				m.container=inwolog.CONTAINER
				m.status="NOT AVAILABLE"
				m.skus=xnumskus
				m.cartons=inwolog.plinqty
				m.newdt=DATE()
				m.addfrom="IN"
				m.confirmdt=empty2nul(m.confirmdt)
				m.newdt=empty2nul(m.newdt)
				m.dydt=empty2nul(m.dydt)
				m.firstavaildt=empty2nul(m.firstavaildt)
				m.apptdt=empty2nul(m.apptdt)
				m.pickedup=empty2nul(m.pickedup)

				INSERT INTO inrcv FROM MEMVAR
*      tu("inrcv")
			ENDIF

		ENDIF

	ENDFOR

	IF !lTesting
	tu("inwolog")
	tu("pl")
	endif

	WAIT WINDOW "All S3 943s processed...exiting" TIMEOUT 2

	closeftpsetup()

	NormalExit = .T.

CATCH TO oErr
	IF NormalExit = .F.
		ASSERT .F. MESSAGE "AT CATCH SECTION"
		SET STEP ON
		tsubject = cCustName+" 943 Upload Error ("+TRANSFORM(oErr.ERRORNO)+") at "+TTOC(DATETIME())
		tattach  = ""
		tsendto  = tsendtoerr
		tcc      = tccerr
		tmessage = cCustName+" 943 Upload Error"
		lcSourceMachine = SYS(0)
		lcSourceProgram = SYS(16)

		IF EMPTY(cxErrMsg)
			tmessage =tmessage+ "Try/CATCH Exeception Message:"+CHR(13)+;
				[  Error: ] + STR(oErr.ERRORNO) +CHR(13)+;
				[  Procedure: ] + oErr.PROCEDURE +CHR(13)+;
				[  LineNo: ] + STR(oErr.LINENO) +CHR(13)+;
				[  Message: ] + oErr.MESSAGE +CHR(13)+;
				[  Details: ] + oErr.DETAILS +CHR(13)+;
				[  StackLevel: ] + STR(oErr.STACKLEVEL) +CHR(13)+;
				[  LineContents: ] + oErr.LINECONTENTS+CHR(13)+;
				[  UserValue: ] + oErr.USERVALUE+CHR(13)+;
				[  Computer:  ] +lcSourceMachine+CHR(13)+;
				[  943 file:  ] +cFilename+CHR(13)+;
				[  Program:   ] +lcSourceProgram
		ELSE
			tmessage = tmessage + CHR(13) + CHR(13) + cxErrMsg
		ENDIF
		tattach  = ""
		tfrom    ="FMI EDI Processing Center <fmi-transload-ops@fmiint.com>"
		DO FORM m:\dev\FRM\dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
	ENDIF
FINALLY
	SET STATUS BAR ON
	DELETE FILE (xpath+'*.*')
	CLOSE DATABASES ALL
	ON ERROR
ENDTRY


****************************
PROCEDURE goodmail
****************************
tsubject = cMailName+" EDI FILE (943) Processed, Inv. WO"+cWO_Num
tmessage = REPLICATE("-",90)+CHR(13)
tmessage = tmessage+"Packinglist upload run from machine: "+SYS(0)+CHR(13)
tmessage = tmessage+REPLICATE("-",90)+CHR(13)
tmessage = tmessage+"Inbound Workorders created for "+ALLTRIM(m.Acctname)+", Loc: "+cWhse+CHR(13)
tmessage = tmessage+IIF(!EMPTY(cContainer),"Container: "+cContainer+CHR(13),"AWB :"+cReference)+CHR(13)
tmessage = tmessage+"Data from File : "+cFilename+CHR(13)
tmessage = tmessage+REPLICATE("-",90)+CHR(13)
tmessage = tmessage+"Acct. Ref: "+cAcct_ref+", WO# "+cWO_Num+SPACE(2)+"CTNS: "+cPLCtns+SPACE(2)+"UNITS: "+cPLUnits

IF lDoMail
	DO FORM m:\dev\frm\dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
ENDIF
ENDPROC

****************************
PROCEDURE dupemail
****************************
PARAMETERS cWO
tsubject = cMailName+" EDI FILE (943): Duplicate"
tmessage = REPLICATE("-",90)+CHR(13)
tmessage = tmessage+"Data from File : "+cFilename+CHR(13)
tmessage = tmessage+"For Account ID: "+TRANSFORM(nAcctNum)
tmessage = tmessage+ ", Container "+cContainer
tmessage = tmessage+ ", Acct. Reference "+cAcct_ref+CHR(13)
tmessage = tmessage+"already exists in the Inbound WO table, in Work Order "+cWO
tmessage = tmessage+CHR(13)+REPLICATE("-",90)+CHR(13)

IF lTesting
	SET STEP ON
ENDIF

tsendto="pgaidis@fmiint.com,mwinter@fmiint.com"

IF lDoMail
	DO FORM m:\dev\frm\dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
ENDIF
ENDPROC

****************************
PROCEDURE errormail
****************************
SET STEP ON
tsubject = cMailName+" EDI ERROR (943)"
tsendto = tsendtoerr
tcc = tccerr
tmessage = "File: "+cFilename
tmessage = tmessage+CHR(13)+cxErrMsg

DO FORM m:\dev\frm\dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
NormalExit = .T.
SET STEP ON
THROW
ENDPROC

****************************
PROCEDURE setuprcvmail
****************************
PARAMETERS cGroup

SELECT 0
USE F:\3pl\DATA\mailmaster ALIAS mm
LOCATE FOR mm.edi_type = "943" AND accountid = 6803 AND Acctname = "S3-"+cGroup
IF !FOUND()
	SET STEP ON
	cxErrMsg = "Office/Loc/Acct not in Mailmaster!"
	errormail()
ENDIF
STORE TRIM(mm.basepath) TO lcPath
STORE TRIM(mm.archpath) TO cArchivePath
lUseAlt = mm.use_alt
STORE IIF(lUseAlt,TRIM(mm.sendtoalt),TRIM(mm.sendto)) TO tsendto
STORE IIF(lUseAlt,TRIM(mm.ccalt),TRIM(mm.cc)) TO tcc
LOCATE
LOCATE FOR mm.edi_type = "MISC" AND mm.taskname = "GENERAL"
lUseAlt = mm.use_alt
STORE IIF(lUseAlt,TRIM(mm.sendtoalt),TRIM(mm.sendto)) TO tsendtotest
STORE IIF(lUseAlt,TRIM(mm.ccalt),TRIM(mm.cc)) TO tcctest
STORE IIF(lUseAlt,TRIM(mm.sendtoalt),TRIM(mm.sendto)) TO tsendtoerr
STORE IIF(lUseAlt,TRIM(mm.ccalt),TRIM(mm.cc)) TO tccerr
IF lTesting OR lTestmail
	tsendto = tsendtotest
	tcc = tcctest
ENDIF
USE IN mm
ENDPROC

***************************************
PROCEDURE closeftpsetup
***************************************
SELECT 0
USE F:\edirouting\ftpsetup
REPLACE chkbusy WITH .F. FOR transfer = cTransfer
USE IN ftpsetup
ENDPROC

***************************************
PROCEDURE assignofficelocs
***************************************
cOffice=IIF(INLIST(cOffice,"1","2","7"),"C",cOffice)
cWhseLoc = ICASE(cOffice = "M","FL",cOffice = "I","NJ",cOffice = "X","CR","CA")
cWhse = ICASE(cOffice = "M","MIAMI",cOffice = "I","CARTERET",cOffice = "X","CARSON","SAN PEDRO")
ENDPROC

***************************************
PROCEDURE xplrollup  && Added 02.20.2014, Joe, per Chris M.
***************************************
SELECT xpl
SELECT inwologid,STYLE,COLOR,ID,PACK, COUNT(1) AS cnt1 FROM xpl WHERE !units GROUP BY inwologid,STYLE,COLOR,ID,PACK INTO CURSOR tempxpl
SELECT tempxpl
LOCATE
IF lTesting
*		BROWSE FOR cnt1>1
ENDIF
LOCATE

SCAN FOR tempxpl.cnt1>1
	cStyle1 = ALLTRIM(tempxpl.STYLE)
	SELECT xpl
	LOCATE FOR xpl.inwologid = tempxpl.inwologid AND xpl.STYLE = tempxpl.STYLE AND xpl.COLOR = tempxpl.COLOR AND xpl.ID = tempxpl.ID AND xpl.PACK = tempxpl.PACK
	nrec1 = RECNO()
	REPLACE xpl.ECHO WITH xpl.ECHO+CHR(13)+"QTY*"+ALLT(STR(xpl.totqty)) IN xpl

	SCAN FOR !DELETED() AND  xpl.STYLE = tempxpl.STYLE AND xpl.COLOR = tempxpl.COLOR AND xpl.ID = tempxpl.ID AND xpl.PACK = tempxpl.PACK AND RECNO()>nrec1
		nrec2 = RECNO()
		SCATTER FIELDS ECHO MEMO MEMVAR
		nTotqty = xpl.totqty
		GO nrec1
		IF !"MULTILINES"$xpl.ECHO
			REPLACE xpl.ECHO WITH "MULTILINES"+CHR(13)+xpl.ECHO IN xpl
		ENDIF
		REPLACE xpl.ECHO WITH xpl.ECHO+CHR(13)+m.echo IN xpl
		REPLACE xpl.ECHO WITH xpl.ECHO+CHR(13)+"QTY*"+ALLT(STR(nTotqty)) IN xpl
		REPLACE xpl.totqty WITH xpl.totqty+nTotqty IN xpl
		lnPack = VAL(xpl.PACK )
		lnStyleTotal = xpl.totqty
		SKIP 1 IN xpl
		REPLACE xpl.totqty WITH lnStyleTotal*lnPack  IN xpl
		GO nrec2
		DELETE NEXT 2 IN xpl
	ENDSCAN

	STORE "" TO m.echo
ENDSCAN
ENDPROC
***************************************************************************************************************
