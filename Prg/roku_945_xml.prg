parameter lcBol


Close Data All

*lcBOL= "04070086674367233"
ltesting = .f.

*!*  If ltesting
*!*    Use F:\whp\whdata\ctnucc In 0
*!*    Use F:\whp\whdata\outship In 0
*!*    Use F:\whp\whdata\outdet In 0
*!*    Use F:\whp\whdata\shipment In 0
*!*    Use F:\whp\whdata\package In 0
*!*  Else
*!* Endif
If !Used("ctnucc")
  Use F:\whi\whdata\ctnucc In 0
endif
If !Used("outship")
  Use F:\whi\whdata\outship In 0
Endif 
If !Used("outdet")
  Use F:\whi\whdata\outdet In 0
endif
If !Used("shipment")
  Use F:\whi\whdata\shipment In 0
Endif 
If !Used("package")
  Use F:\whi\whdata\package In 0
Endif 

Select outship
Locate For bol_no=lcBOL

If !Found("outship")
  =Messagebox("No such BOL value",16,"ROKU 45 creator")
  Close Data All
  Return
Endif

********* create a 945 for ROKU
lnCost = 0.0

lcStr = ""

Select * From outship Where bol_no = lcBOL Into Cursor xpts

Select ship_ref From xpts Into Cursor xjustpts

Select xjustpts
Scan

  Select xpts

  Scan For ship_ref= xjustpts.ship_ref

    =Seek(Padr(Alltrim(xpts.ship_ref),20," "),"shipment","pickticket")
    If Found("shipment")
      llSmallPkg = .T.
      lnCost = shipment.charge
    Else
      llSmallPkg = .T.
    Endif


    lcStr1 = Filetostr("f:\roku\xmltemplates\header.txt")

    lcStr1 = Strtran(lcStr1,"<PICKTICKET>",Alltrim(xpts.ship_ref))
    lcStr1 = Strtran(lcStr1,"<PO>",Alltrim(xpts.cnee_ref))
    lcStr1 = Strtran(lcStr1,"<WONUM>",Alltrim(Transform(xpts.wo_num)+"-"+Alltrim(Transform(xpts.outshipid))))

    Select *,Space(10) As linenum,0000000 As ptid From ctnucc Where .F. Into Cursor ctns Readwrite

    Select ship_ref,outshipid From xpts Where bol_no = lcBOL And ship_ref = xjustpts.ship_ref Into Cursor pts

    Select pts
    Scan
      Select ucc,Style,serialno,xpts.outshipid As ptid,Space(10) As linenum From ctnucc Where ship_ref= xjustpts.ship_ref Into Cursor xtemp
      Select xtemp
      Scan
        Select xtemp
        Scatter Memvar
        Insert Into ctns From Memvar
      Endscan
    Endscan

*set step On

    Select ctns
    Scan
      Select outdet
      Locate For ptid = ctns.ptid And Style= ctns.Style
      Replace ctns.linenum With outdet.linenum In ctns
    Endscan


    lcStr2 = Filetostr("f:\roku\xmltemplates\serial.txt")

    lcSerialNumbers =""

    Select Count(1) as qty,style,serialno From ctns Group By style,serialno Into Cursor xxctns

    Select xxctns
    Go Top
    Scan For !Empty(serialno)
      lcStrTemp = Strtran(lcStr2,"<SERIALNUMBER>",Alltrim(xxctns.serialno))
      lcStrTemp = Strtran(lcStrTemp,"<STYLE>",Alltrim(xxctns.Style))
      lcSerialNumbers = lcSerialNumbers + lcStrTemp
    Endscan
 
    Select Count(1) as qty,style,serialno From ctns Group By style,serialno Into Cursor xxctns2
 
    Select xxctns2
    Go Top
    Scan For !Empty(serialno) And qty = 1
      lcStrTemp = Strtran(lcStr2,"<SERIALNUMBER>",Alltrim(xxctns2.serialno))
      lcStrTemp = Strtran(lcStrTemp,"<STYLE>",Alltrim(xxctns2.Style))
      lcSerialNumbers = lcSerialNumbers + lcStrTemp
    Endscan

    lcStr2 = lcSerialNumbers

    lcStr3 = Filetostr("f:\roku\xmltemplates\body.txt")

    If  llSmallPkg = .T.
      lcStr3 = Strtran(lcStr3,"<COST>",Alltrim(Str(lnCost,10,2)))
    Else
      lcStr3 = Strtran(lcStr3,"<COST>","")
    Endif
    lcStr3 = Strtran(lcStr3,"<SCAC>",Alltrim(xpts.scac))
    lcStr3 = Strtran(lcStr3,"<SHIPVIA>",Alltrim(xpts.ship_via))
    lcStr3 = Strtran(lcStr3,"<BOL>",Alltrim(xpts.bol_no))
    lcStr3 = Strtran(lcStr3,"<APPTNUM>",Alltrim(xpts.keyrec))
    lcStr3 = Strtran(lcStr3,"<QTY>",Alltrim(Str(xpts.ctnqty,10,2)))
    lcStr3 = Strtran(lcStr3,"<WEIGHT>",Alltrim(Str(xpts.weight,10,2)))
    lcStr3 = Strtran(lcStr3,"<DELDATE>",Alltrim(Ttoc(xpts.del_date,3)))


    lcStr4 = Filetostr("f:\roku\xmltemplates\items.txt")

    lcDetail = ""
    Select pts
    Scan
      Select outdet
      Scan For outdet.outshipid = xpts.outshipid And outdet.units
        lcStrTemp= lcStr4
        lcStrTemp = Strtran(lcStrTemp,"<STYLE>",Alltrim(outdet.Style))
        lcStrTemp = Strtran(lcStrTemp,"<TOTQTY>",Alltrim(Str(outdet.totqty,10,2)))
        lcStrTemp = Strtran(lcStrTemp,"<ORIGQTY>",Alltrim(Str(outdet.origqty,10,2)))
        lcStrTemp = Strtran(lcStrTemp,"<LINENUM>",Alltrim(outdet.linenum))
        lcDetail = lcDetail+lcStrTemp
      Endscan
    Endscan

    lcStr4 = lcDetail

*Set Step On

    lcStr5 = Filetostr("f:\roku\xmltemplates\package.txt")
    lcPackages=""
    Select ctns
    Go Top
    Scan
      Do Case
      Case "4400"$ctns.Style
        lcStrTemp = Strtran(lcStr5,   "<LENGTH>",Alltrim(Str(12.0,5,2)))
      Case "2710"$ctns.Style
        lcStrTemp = Strtran(lcStr5,   "<LENGTH>",Alltrim(Str(11.9,5,2)))
      Case "3700"$ctns.Style
        lcStrTemp = Strtran(lcStr5,   "<LENGTH>",Alltrim(Str(21.0,5,2)))
      Case "3710"$ctns.Style
        lcStrTemp = Strtran(lcStr5,   "<LENGTH>",Alltrim(Str(19.9,5,2)))
      Case "4620"$ctns.Style
        lcStrTemp = Strtran(lcStr5,   "<LENGTH>",Alltrim(Str(20.0,5,2)))
      Case "4630"$ctns.Style
        lcStrTemp = Strtran(lcStr5,   "<LENGTH>",Alltrim(Str(20.0,5,2)))
      Case "4640"$ctns.Style
        lcStrTemp = Strtran(lcStr5,   "<LENGTH>",Alltrim(Str(16.0,5,2)))
      Endcase

      Do Case
      Case "4400"$ctns.Style
        lcStrTemp = Strtran(lcStrTemp,"<WIDTH>",Alltrim(Str(17.0,5,2)))
      Case "2710"$ctns.Style
        lcStrTemp = Strtran(lcStrTemp,"<WIDTH>",Alltrim(Str(8.0,5,2)))
      Case "3700"$ctns.Style
        lcStrTemp = Strtran(lcStrTemp,"<WIDTH>",Alltrim(Str(10.9,5,2)))
      Case "3710"$ctns.Style
        lcStrTemp = Strtran(lcStrTemp,"<WIDTH>",Alltrim(Str(7.7,5,2)))
      Case "4620"$ctns.Style
        lcStrTemp = Strtran(lcStrTemp,"<WIDTH>",Alltrim(Str(8.0,5,2)))
      Case "4630"$ctns.Style
        lcStrTemp = Strtran(lcStrTemp,"<WIDTH>",Alltrim(Str(8.0,5,2)))
      Case "4640"$ctns.Style
        lcStrTemp = Strtran(lcStrTemp,"<WIDTH>",Alltrim(Str(6.0,5,2)))
      Endcase

      Do Case
      Case "4400"$ctns.Style
        lcStrTemp = Strtran(lcStrTemp,"<HEIGHT>",Alltrim(Str(10.0,5,2)))
      Case "2710"$ctns.Style
        lcStrTemp = Strtran(lcStrTemp,"<HEIGHT>",Alltrim(Str(8.0,5,2)))
      Case "3700"$ctns.Style
        lcStrTemp = Strtran(lcStrTemp,"<HEIGHT>",Alltrim(Str(8.6,5,2)))
      Case "3710"$ctns.Style
        lcStrTemp = Strtran(lcStrTemp,"<HEIGHT>",Alltrim(Str(6.3,5,2)))
      Case "4620"$ctns.Style
        lcStrTemp = Strtran(lcStrTemp,"<HEIGHT>",Alltrim(Str(8.0,5,2)))
      Case "4630"$ctns.Style
        lcStrTemp = Strtran(lcStrTemp,"<HEIGHT>",Alltrim(Str(8.0,5,2)))
      Case "4640"$ctns.Style
        lcStrTemp = Strtran(lcStrTemp,"<HEIGHT>",Alltrim(Str(8.7,5,2)))
      Endcase

      Do Case
      Case "4400"$ctns.Style
        lcStrTemp = Strtran(lcStrTemp,"<WEIGHT>",Alltrim(Str(9.5,5,2)))
      Case "2710"$ctns.Style
        lcStrTemp = Strtran(lcStrTemp,"<WEIGHT>",Alltrim(Str(10.3,5,2)))
      Case "3700"$ctns.Style
        lcStrTemp = Strtran(lcStrTemp,"<WEIGHT>",Alltrim(Str(8.3,5,2)))
      Case "3710"$ctns.Style
        lcStrTemp = Strtran(lcStrTemp,"<WEIGHT>",Alltrim(Str(6.58,5,2)))
      Case "4620"$ctns.Style
        lcStrTemp = Strtran(lcStrTemp,"<WEIGHT>",Alltrim(Str(10.0,5,2)))
      Case "4630"$ctns.Style
        lcStrTemp = Strtran(lcStrTemp,"<WEIGHT>",Alltrim(Str(10.0,5,2)))
      Case "4640"$ctns.Style
        lcStrTemp = Strtran(lcStrTemp,"<WEIGHT>",Alltrim(Str(7.70,5,2)))
      Endcase

      lcStrTemp = Strtran(lcStrTemp,"<STYLE>",Alltrim(ctns.Style))
      lcStrTemp = Strtran(lcStrTemp,"<UCC>",Alltrim(ctns.ucc))
      If  llSmallPkg = .T.
        =Seek(Padr(Alltrim(ctns.ucc),20," "),"package","ucc")
        If Found("package")
          lcStrTemp = Strtran(lcStrTemp,"<TRKNUM>",Alltrim(package.trknumber))
        Else
          lcStrTemp = Strtran(lcStrTemp,"<TRKNUM>",Alltrim(ctns.ucc))
        Endif
      Else
        lcStrTemp = Strtran(lcStrTemp,"<TRKNUM>",Alltrim(outship.apptnum))
      Endif

      Do Case
      Case "4400"$ctns.Style
        lcStrTemp = Strtran(lcStrTemp,"<PACK>","4")
      Case "2710"$ctns.Style
        lcStrTemp = Strtran(lcStrTemp,"<PACK>","8")
      Case "3700"$ctns.Style
        lcStrTemp = Strtran(lcStrTemp,"<PACK>","12")
      Case "3710"$ctns.Style
        lcStrTemp = Strtran(lcStrTemp,"<PACK>","8")
      Case "4620"$ctns.Style
        lcStrTemp = Strtran(lcStrTemp,"<PACK>","8")
      Case "4630"$ctns.Style
        lcStrTemp = Strtran(lcStrTemp,"<PACK>","8")
      Case "4640"$ctns.Style
        lcStrTemp = Strtran(lcStrTemp,"<PACK>","8")
      Endcase

      lcStrTemp = Strtran(lcStrTemp,"<LINENUM>",Alltrim(ctns.linenum))
      lcPackages = lcPackages + lcStrTemp
    Endscan

    lcStr5 = lcPackages

    lcStr6 = Filetostr("f:\roku\xmltemplates\endofitems.txt")

    lcStr7 = Filetostr("f:\roku\xmltemplates\footer.txt")

    lcStr = lcStr+lcStr1+lcStr2+lcStr3+lcStr4+lcStr6+lcStr5+lcStr7

  Endscan

Endscan

*lcFilename = "f:\ftpusers\roku-nj\945out\roku_945_"+TTOC(Datetime(),1)+".xml"
lcTime = Ttoc(Datetime(),1)
lcFilename = "f:\ftpusers\roku-nj\945out\roku_945_"+Alltrim(lcTime)+".xml"
lcFilename2 = "f:\ftpusers\roku-nj\945out\archive\roku_945_"+Alltrim(lcTime)+".xml"

lcOutputStr = "<WarehouseTransactions>"+Chr(13)+lcStr+Chr(13)+"</WarehouseTransactions>"
Strtofile(lcOutputStr,lcFilename)
Strtofile(lcOutputStr,lcFilename2)

Do ediupdate



If !Used("ctnucc")
  Use In ctnucc
endif
If !Used("outship")
  Use In outship
Endif 
If !Used("outdet")
  Use In outdet
endif
If !Used("shipment")
  Use In shipment
Endif 
If !Used("package")
  Use In package
Endif 

****************************
PROCEDURE ediupdate
****************************
 If !Used("edi_trigger")
   use f:\3pl\data\edi_trigger In 0
 Endif    
 SELECT edi_trigger
  REPLACE processed WITH .T.,proc945 WITH .T.,file945 WITH LcFilename,fin_status WITH "PROCESSED",errorflag WITH .F.,when_proc WITH DATETIME() ;
        FOR edi_trigger.bol = LcBOL AND accountid = 6674

Endproc 