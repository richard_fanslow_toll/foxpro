*****************************************************************************************
* Started out as 856 FILES TO send to QIVA
* AUTHOR--:PG
* DATE----:05/01/2002
* Modified much, now this takes in flat files and sends out 315 container status updates
* product activity data for Sears
*****************************************************************************************
Do setenvi
Set Century On
Set Date To ymd
Set Decimal To 0

xoutdir="f:\ftpusers\searsedi\EdiDataOut\"

Close Data All
Store "" To fstring

Public t856, tfile, cstring,lnInterControlnum,lcInterctrlNum

Store .F. To llEditUpdate

lcstring  = ""
lincnt = 1
******************************************************************************************
Wait Window "Open tables and setting up........." Nowait
*******************************************************************************************
Store 0 To EDI_testing
Use F:\searsedi\Data\controlnum In 0 Alias controlnum

********************************************************************************************************
**********************************************
*** Build Hierarchical Level *** SHIPMENT LEVEL
***********************************************
Wait Window "Setting up the cursors......." Nowait
lcpath ="f:\ftpusers\searsedi\RGTIdataIn\"
lcarchivepath ="f:\ftpusers\searsedi\RGTIdataIn\archive\"

close data all 

Use F:\searsedi\Data\asn310 In 0

xfile = "c:\tempfox\asn315.txt"

xfile = "em315_FMI_"+Alltrim(Str(Year(Date())))+Padl(Alltrim(Str(Month(Date()))),2,"0")+;
  PADL(Alltrim(Str(Day(Date()))),2,"0")+Padl(Alltrim(Str(Hour(Datetime()))),2,"0")+;
  padl(Alltrim(Str(Minute(Datetime()))),2,"0")+Padl(Alltrim(Str(Sec(Datetime()))),2,"0")+".txt"

xfile=xoutdir+xfile

lnHandle = fcreate(xfile)

lcHeader = "#KM315 FROM FMI TO KMART   "+Alltrim(Str(YEAR(Date())))+Alltrim(Str(MONTH(Date())))+Alltrim(Str(DAY(Date())))+" "+Substr(Strtran(Time(),":",""),1,4)+" "+"0000001"+Space(152)

Fputs(lnHandle,lcHeader)

Select asn310
Goto top
ii=0  
Scan for sent = .f.
  ii= ii+1
  lcDetail ="FM"+asn310.scac+Padr(Alltrim(asn310.container),15," ")+Padr(Alltrim(asn310.hbol),16," ")+Padr(Alltrim(asn310.hbol),16," ")+"R "+Space(20)+;
    "FM  "+Substr(Strtran(Time(),":",""),1,6)+Padr(Alltrim(asn310.disdc),5," ")+Padr(Alltrim(asn310.vessel),35," ")+Padr(Alltrim(asn310.voyage),10," ")+Space(16)+Alltrim(Str(YEAR(Date())))+Padl(Alltrim(Str(MONTH(Date()))),2,"0")+Padl(Alltrim(Str(DAY(Date()))),2,"0")+;
    "Y"+Space(40)
   Fputs(lnHandle,lcDetail)
   
endscan

lcFooter = "#EOT     1 "+Padl(Alltrim(Str(ii)),5," ")+" "+"0000001"+Space(175)

Fputs(lnHandle,lcFooter)

Fclose(lnHandle)

&& here we shouldarchive and delete the files
*!*	    If !File(archivefile)
*!*	      Copy File [&xfile] To [&archivefile]
*!*	    Endif
*    If !lTestImport
*	   erase [&xfile]
*    Endif



