* called from webrpt.scx
* called from cmdpackinglist.vcx which is in bl.scx, outwo.scx, scanpack.scx

**print packing list
**
**need to define xoutship and xoutdet prior to call - will give alias, ie xoutship="voutship"
**
Parameters xpnpacct, xnomsg

set step on 

if xplfrom="OUTWO"
	xoutship="voutship"
	xoutdet="voutdet"
else
	xoutship="outship"
	xoutdet="outdet"
endif

Create Cursor xrpt (xgroup c(20), acctname c(30), wo_num N(7), ship_ref c(20), consignee c(35), address c(30),address2 c(30), ;
	name c(30), csz c(40), cnee_ref c(25), dept c(10), wo_date d, Start d, Cancel d, ship_via c(30), qty N(6), ctnqty N(6), totunits N(7), ;
	weight N(6), cuft N(6), Style c(20), Color c(15), upc c(13), ;	&& to fit UPC for Jockey
	Id c(10), custsku c(50), styledesc c(50), colordesc c(20), Pack c(10), totqty N(8), sforstore c(10), duns c(10),;
	billtoname c(40), billtoaddress1 c(40), custorder c(25),billtoaddress2 c(40), billtocsz c(40), ordertype c(25), ordernumber c(25),;
	realcolor c(20), Reference c(25), ptqty N(8),salesorder c(25), custnumber c(25), dcnum c(10),scac c(6), cacctnum c(15), pickdate d)

xnormalok=.T.

*!*	Try		REMOVED DY 1/16/17
  Select (xoutship)
  Scan For &xfilter4 And accountid=outwolog.accountid
    xoutshipid=&xoutship..outshipid

    Scatter Memvar Fields wo_num, ship_ref, consignee, Name, address, address2, csz, cnee_ref, scac,shipins,dcnum,ship_via, dept, Start, cacctnum,picked,Cancel, qty, ctnqty, weight, cuft, sforstore, duns
    m.start=nul2empty(m.start)
    m.cancel=nul2empty(m.cancel)
    m.acctname=acctname(&xoutship..accountid)
    m.xgroup=m.ship_ref

    If Cons(&xoutship..consignee)="NEIMAN MARCUS" And Empty(m.duns)
      Do Case
      Case outwolog.accountid=5102
        m.duns="900456161"
      Otherwise
        If !xnomsg
          x3winmsg("The DUNS is missing - Neiman Marcus requires that it be printed on the packing list")
        Endif
      Endcase
    Endif

    lcbilltoname =getmemodata("shipins","BILLTONAME")
    m.billtoname = lcbilltoname
    m.pickdate = nul2empty(picked)

    lcbilltoaddress1 =getmemodata("shipins","BILLTOADDRESS1")
    If  lcbilltoaddress1 = "NA"
      m.billtoaddress1 =getmemodata("shipins","BILLTOADDR1")
    Else
      m.billtoaddress1 =  &xoutship..address
    Endif

    lcbilltoaddress2 =getmemodata("shipins","BILLTOADDRESS2")
    If  lcbilltoaddress2 = "NA"
       m.lcbilltoaddress2 =getmemodata("shipins","BILLTOADDR2")
    Else
      m.billtoaddress2 =  &xoutship..address2
    Endif

    lcbilltoCSZ =getmemodata("shipins","BILLTOCSZ")
    m.billtocsz = lcBilltoCSZ

    lcSalesorder =getmemodata("shipins","SALESORDER")
    If  lcSalesorder = "NA"
      m.salesorder =  ""
    Else
      m.salesorder= lcSalesorder
    Endif

    lcCustOrder =getmemodata("shipins","COMMENT")
    If  lcCustOrder = "NA"
      m.custorder =  ""
    Else
      m.custorder= lcCustOrder
    Endif

    lnunitqty =0

    Select (xoutdet)
**changed Thisform.zzpicknpackaccount to variable
*    Scan For outshipid=&xoutship..outshipid And !(Thisform.zzpicknpackaccount And !&xoutship..picknpack And units)
    Scan For outshipid=&xoutship..outshipid And !(xpnpacct And !&xoutship..picknpack And units)
      Scatter Memvar Fields Style, Color, Id, Pack,upc,custsku, totqty
      If outwolog.accountid=6034
        m.custsku=getmemodata("printstuff","DESC")
      Endif
      lcRealColor =getmemodata("printstuff","COLOR")
      m.realcolor = lcRealColor

      lcDesc =getmemodata("printstuff","DESC")
      m.styledesc = lcDesc

      m.ordertype   = "WEB Next day"
      m.ordernumber = &xoutship..ship_ref  &&"OrderNumber"
*      m.Reference   = cnee_ref &&"Reference"  && changed to cnee_ref PG 6/26/2017
      m.custnumber  = &xoutship..cacctnum &&"CustNumber"
      m.upc = &xoutdet..upc
      m.custsku = &xoutdet..custsku
      lnunitqty = lnunitqty +(m.totqty*Val(Pack))
      Insert Into xrpt From Memvar

	  If outwolog.accountid=6532
        Replace totqty With totqty*Val(Pack) In xrpt
	  endif
    Endscan

	**moved here from below (explained below) - mvw 10/23/14
    If outwolog.accountid=6532
      Replace qty With lnunitqty For ship_ref=m.ship_ref In xrpt

	**moved up into xoutdet loop bc otherwise as you scan thru each PT you increase the totqty for all previous PTs again - mvw 04/11/17
*!*	      Select xrpt
*!*	      Scan For Val(Pack) != 1		&& removed dy 3/23/17 restored dy 3/25/17 there is a problem here
*!*	        Replace totqty With totqty*Val(Pack) In xrpt
*!*	      Endscan
    Endif

	select (xoutship)
	locate for outshipid=xoutshipid
  Endscan
*!*	Catch
*!*	  If !xnomsg
*!*	    x3winmsg("You can't print the packing list from the BL screen because there's too many pick tickets")
*!*	  Endif
*!*	  xnormalok=.F.
*!*	Endtry

If !xnormalok
  Return .F.
Endif

**moved up into the pt scan, down here if multiple PTs are printed all of the PTs get the unit count from the last PT printed - mvw 10/23/14
*!*	If outwolog.accountid=6532
*!*	  Select xrpt
*!*	  replace All qty With lnUnitQty
*!*	Endif

If xplfrom="OUTWO"
  Go Top In voutship
  xjockey = (voutship.consignee="JOCKEY")
**changed to use account because thisform inaccessible now that its out of the class cmdpackinglist - mvw 08/15/14
*!*	  xstyleheader=Thisform.grdoutdet.column1.header1.Caption
*!*	  xcolorheader=Thisform.grdoutdet.column2.header1.Caption
*!*	  xidheader=Thisform.grdoutdet.column3.header1.Caption
  =Seek(voutship.accountid,"account","accountid")
  xstyleheader=Iif(Empty(account.styledesc),"Style",Trim(account.styledesc))
  xcolorheader=Trim(account.colordesc)
  xidheader=Trim(account.iddesc)
else
  select outship
  locate for wo_num=outwolog.wo_num
  xjockey = (outship.consignee="JOCKEY")
  =Seek(outwolog.accountid,"account","accountid")
  xstyleheader=Iif(Empty(account.styledesc),"Style",Trim(account.styledesc))
  xcolorheader=Trim(account.colordesc)
  xidheader=Trim(account.iddesc)
Endif

If xjockey
  xcolorheader="UPC"
  Select xrpt
  Scan
    Select outdet
    Locate For wo_num=outwolog.wo_num And Style=xrpt.Style
    If Found()
      Replace Color With outdet.upc In xrpt
    Endif
  Endscan
Endif

If inlist(outwolog.accountid,&gmjacctlist,6476)
  Select xrpt
  Scan
    If upcmastsql(outwolog.accountid,Style,Left(Color,10),Id)
      xcolor=getmemodata("INFO","COLORDESC")
      Replace custsku With upcmast.Descrip, colordesc With xcolor In xrpt
    Endif
  Endscan
Endif

If outwolog.accountid=6521
  Select xrpt
  Scan
    Replace wo_date With outwolog.wo_date
    Replace totunits With Val(xrpt.Pack)*xrpt.totqty In xrpt
    If upcmastsql(outwolog.accountid,Style)
      Replace styledesc With upcmast.Descrip In xrpt
    Endif
  Endscan
Endif

If outwolog.accountid=6182
  Select xrpt
  Scan
    Replace totunits With Val(xrpt.Pack)*xrpt.totqty In xrpt
    If Empty(styledesc)
      If upcmastsql(outwolog.accountid,Style)
        Replace styledesc With upcmast.Descrip In xrpt
      Endif
    Endif
  Endscan
Endif

* removed dy 1/28/16 was causing a numeric overflow

If outwolog.accountid=6532
*Set Step On
  Select ship_ref From xrpt Into Cursor temp_pt Group By ship_ref
  Select temp_pt
  Scan
    Select xrpt
    Sum xrpt.totqty To total_pt_units For xrpt.ship_ref = temp_pt.ship_ref
    Replace xrpt.ptqty With total_pt_units For xrpt.ship_ref = temp_pt.ship_ref
  Endscan
Endif

go top in xrpt

**added this because otherwise may not be on the correct record in &xoutship - mvw 08/16/16
select &xoutship
locate for wo_num=xrpt.wo_num and ship_ref=xrpt.ship_ref

Select xrpt
*m.wo_date=outwolog.wo_date

set step on 


Do Case
Case (Inlist(outwolog.accountid,5865) And "SHOPKO" $ &xoutship..consignee) Or outwolog.accountid=6034
  grptname="packlistwide"
Case Inlist(outwolog.accountid,6674)  && ROKU
  grptname="packlistwide"
Case inlist(outwolog.accountid,&gmjacctlist,6476)
  grptname="packlistmj"
Case outwolog.accountid=6182
  grptname="packlistbio"
Case outwolog.accountid=6617
  grptname="packlistbio"
Case outwolog.accountid=6521 And "NORDSTROM.COM"$&xoutship..consignee
  grptname="granvoyage_packlist"
Case outwolog.accountid=6532 And "PRIORITY*DTOC"$&xoutship..shipins
*goption ="PRINT"
  Select xrpt
  grptname="ariat_dtoc_packinglist"
Case outwolog.accountid=6532 And !"PRIORITY*DTOC"$&xoutship..shipins
  Select xrpt
*goption ="PRINT"
  grptname="ariat_b2b_packinglist"

Otherwise
  grptname="packlist"
Endcase
