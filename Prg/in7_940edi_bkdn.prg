*!* m:\dev\prg\in7_940edi_bkdn.prg
CLEAR
WAIT WINDOW "Now in BKDN Phase..." NOWAIT

lCheckStyle = .T.
lPick = .F.
lPrepack = .T.
units = IIF(lPick,.T.,.F.)

SELECT x856
COUNT FOR TRIM(segment) = "ST" TO nSegCnt
cSegCnt = ALLTRIM(STR(nSegCnt))


WAIT WINDOW "Now in 940 Breakdown" TIMEOUT 1
WAIT "There are "+cSegCnt+" P/Ts in this file" WINDOW TIMEOUT 1
SELECT x856
LOCATE

*!* Constants
m.acct_name = "IN7"
m.acct_num = nAcctNum
m.careof = " "
m.sf_addr1 = "530 7TH AVENUE"
m.sf_addr2 = "SUITE 602"
m.sf_csz = "NEW YORK, NY 10018"
m.printed = .F.
m.print_by = ""
m.printdate = DATE()
*!* End constants

*!*	IF !USED("uploaddet")
*!*		IF lTestrun
*!*			USE F:\ediwhse\test\uploaddet IN 0 ALIAS uploaddet
*!*		ELSE
*!*			USE F:\ediwhse\DATA\uploaddet IN 0 SHARED ALIAS uploaddet
*!*		ENDIF
*!*	ENDIF

SELECT xpt
DELETE ALL
SELECT xptdet
DELETE ALL

STORE "" TO m.isa_num,m.shipins,m.color
STORE 0 TO ptctr,pt_total_eaches,pt_total_cartons,nCtnCount

SELECT x856
SET FILTER TO
GOTO TOP

IF lTestrun
*	SET STEP ON
ENDIF

WAIT WINDOW "NOW IN INITIAL HEADER BREAKDOWN" TIMEOUT 1
WAIT WINDOW "NOW IN INITIAL HEADER BREAKDOWN" NOWAIT

nISA = 0  && To check for embedded FA envelopes.
DO WHILE !EOF("x856")
	cSegment = TRIM(x856.segment)
	cCode = TRIM(x856.f1)

	IF TRIM(x856.segment) = "ISA"
		IF nISA = 0
			nISA = 1
			m.isa_num = ALLTRIM(x856.f13)
			cISA_Num = ALLTRIM(x856.f13)
			SELECT x856
			SKIP
			LOOP
		ELSE  && If 2nd ISA is found
			SELECT x856
			DELETE REST
			GO BOTT
			LOOP
		ENDIF
	ENDIF

	IF (TRIM(x856.segment) = "GS") OR (TRIM(x856.segment) = "ST" AND TRIM(x856.f1) = "940")
		SELECT x856
		SKIP
		LOOP
	ENDIF

	IF TRIM(x856.segment) = "W05"  && new shipment
		cCustnotes = ""
		m.vendor_num = ""
		nCtnCount = 0
		nSHCount = 1
		nDetCnt = 0
		SELECT xpt
		APPEND BLANK
		REPLACE xpt.qty WITH 0 IN xpt
		REPLACE xpt.origqty WITH 0 IN xpt
		STORE 0 TO pt_total_eaches
		STORE 0 TO pt_total_cartons
		ptctr = ptctr +1
		WAIT WINDOW AT 10,10 "Now processing PT# "+ ALLTRIM(x856.f2)+;
			" Number "+CHR(13)+TRANSFORM(ptctr)+" of "+cSegCnt NOWAIT
		REPLACE xpt.ptid       WITH ptctr IN xpt
		REPLACE xpt.accountid  WITH nAcctNum IN xpt
		REPLACE xpt.ptdate     WITH DATE() IN xpt
		m.ship_ref = ALLTRIM(x856.f2)
		m.cnee_ref = ALLTRIM(x856.f3)
		REPLACE xpt.ship_ref   WITH m.ship_ref IN xpt  && Pick Ticket
		REPLACE xpt.cnee_ref WITH m.cnee_ref IN xpt  && Pick Ticket
		m.pt = ALLTRIM(x856.f2)
		SELECT x856
		SKIP
		LOOP
	ENDIF

	IF TRIM(x856.segment) = "N1" AND INLIST(TRIM(x856.f1),"CN","ST") && Ship-to data
*!*			SELECT uploaddet
*!*			LOCATE
*!*			IF !EMPTY(M.isa_num)
*!*				APPEND BLANK
*!*				REPLACE uploaddet.office    WITH cOffice
*!*				REPLACE uploaddet.cust_name WITH UPPER(TRIM(x856.f2))
*!*				REPLACE uploaddet.acct_num  WITH m.acct_num
*!*				REPLACE uploaddet.DATE      WITH DATETIME()
*!*				REPLACE uploaddet.isa_num   WITH m.isa_num
*!*				REPLACE uploaddet.startpt   WITH xpt.ship_ref
*!*				REPLACE uploaddet.runid     WITH lnRunID
*!*			ENDIF

		SELECT xpt
		m.st_name = TRIM(x856.f2)
		REPLACE xpt.NAME WITH UPPER(m.st_name) IN xpt
		REPLACE xpt.consignee WITH UPPER(m.st_name) IN xpt
		IF "MACY"$UPPER(m.st_name)
			lFederated = .T.
		ELSE
			lFederated = .F.
		ENDIF

		cStoreNum = ALLTRIM(x856.f4)
		IF EMPTY(xpt.shipins)
			REPLACE xpt.shipins WITH "STORENUM*"+cStoreNum IN xpt
		ELSE
			REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"STORENUM*"+cStoreNum IN xpt
		ENDIF
		REPLACE xpt.storenum  WITH VAL(RIGHT(cStoreNum,5)) IN xpt
		REPLACE xpt.dcnum WITH cStoreNum IN xpt

		SKIP 1 IN x856
		IF TRIM(x856.segment) = "N3"  && address info
			m.st_addr1 = UPPER(TRIM(x856.f1))
			m.st_addr2 = UPPER(TRIM(x856.f2))
			REPLACE xpt.address  WITH m.st_addr1 IN xpt
			REPLACE xpt.address2 WITH m.st_addr2 IN xpt
		ENDIF
		SKIP 1 IN x856
		IF TRIM(x856.segment) = "N4"  && address info
			IF !EMPTY(TRIM(x856.f3))
				m.zipbar = TRIM(x856.f3)
			ENDIF
			IF !EMPTY(TRIM(x856.f2))
				m.st_csz = UPPER(TRIM(x856.f1))+", "+UPPER(TRIM(x856.f2))+" "+TRIM(x856.f3)
			ELSE
				m.st_csz = UPPER(TRIM(x856.f1))
			ENDIF
			REPLACE xpt.csz WITH m.st_csz IN xpt
			cCountry = ALLT(x856.f4)
			REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"COUNTRY*"+cCountry IN xpt
		ENDIF
		SELECT x856
		SKIP
		LOOP
	ENDIF

	IF TRIM(x856.segment) = "N1" AND INLIST(TRIM(x856.f1),"BY","Z7") && Ship-for data
		csfor_name = TRIM(x856.f2)
		REPLACE xpt.shipfor WITH UPPER(csfor_name) IN xpt
		csforStoreNum = ALLTRIM(x856.f4)
		REPLACE xpt.sforstore WITH csforStoreNum IN xpt
		REPLACE xpt.dcnum WITH csforStoreNum IN xpt
		REPLACE xpt.storenum WITH INT(VAL(RIGHT(allt(xpt.dcnum),5))) IN xpt
		SKIP 1 IN x856
		IF TRIM(x856.segment) = "N3"  && address info
			m.sforaddr1 = UPPER(TRIM(x856.f1))
			m.sforaddr2 = UPPER(TRIM(x856.f2))
			REPLACE xpt.sforaddr1 WITH m.sforaddr1 IN xpt
			REPLACE xpt.sforaddr2 WITH m.sforaddr2 IN xpt
		ENDIF
		SKIP 1 IN x856
		IF TRIM(x856.segment) = "N4"  && address info
			IF !EMPTY(TRIM(x856.f2))
				m.sforcsz = UPPER(TRIM(x856.f1))+", "+UPPER(TRIM(x856.f2))+" "+TRIM(x856.f3)
			ELSE
				m.sforcsz = UPPER(TRIM(x856.f1))
			ENDIF
			REPLACE xpt.sforcsz WITH m.sforcsz IN xpt
			cCountry = ALLT(x856.f4)
			IF !EMPTY(cCountry)
				REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"SFCOUNTRY*"+cCountry IN xpt
			ENDIF
		ENDIF
		SELECT x856
		SKIP
		LOOP
	ENDIF

	IF TRIM(x856.segment) = "N9" AND TRIM(x856.f1) = "DP"
		m.dept = ALLT(x856.f2)
		REPLACE xpt.dept   WITH m.dept IN xpt  && Dept
		SELECT x856
		SKIP
		LOOP
	ENDIF

	IF TRIM(x856.segment) = "G62" AND TRIM(x856.f1) == "37"  && start date
		lcDate = ALLTRIM(x856.f2)
		lxDate = SUBSTR(lcDate,5,2)+"/"+SUBSTR(lcDate,7,2)+"/"+LEFT(lcDate,4)
		ldDate = CTOD(lxDate)
		REPLACE xpt.START WITH ldDate
		SELECT x856
		SKIP
		LOOP
	ENDIF

	IF TRIM(x856.segment) = "G62" AND TRIM(x856.f1) == "38"  && cancel date
		lcDate = ALLTRIM(x856.f2)
		lxDate = SUBSTR(lcDate,5,2)+"/"+SUBSTR(lcDate,7,2)+"/"+LEFT(lcDate,4)
		ldDate = CTOD(lxDate)
		REPLACE xpt.CANCEL WITH ldDate
		SELECT x856
		SKIP
		LOOP
	ENDIF

	IF TRIM(x856.segment) = "NTE" AND TRIM(x856.f1) = "SPH"  && Warehouse handling notes
		m.sh = "SPINS"+ALLT(STR(nSHCount))+"*"+ALLTRIM(x856.f2)  && ADDED THE ENUMERATION OF THE NOTES 4/17/2008
		nSHCount = nSHCount + 1
		REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+m.sh
		SELECT x856
		SKIP
		LOOP
	ENDIF

	IF TRIM(x856.segment) = "W66" && ship_via info
		REPLACE xpt.ship_via WITH ALLT(x856.f5)
		m.sh = "SPINS"+ALLT(STR(nSHCount))+"*SHIP VIA: "+ALLTRIM(x856.f5)  &&
		nSHCount = nSHCount + 1
		REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+m.sh
		SELECT x856
		SKIP
		LOOP
	ENDIF

	IF TRIM(x856.segment) = "LX" && start of PT detail, stay here until this PT is complete
		WAIT "NOW IN DETAIL LOOP PROCESSING" WINDOW NOWAIT
		SELECT xptdet
		m.linenum = TRIM(x856.f1)
		CALCULATE MAX(ptdetid) TO m.ptdetid
		m.ptdetid = m.ptdetid + 1
		m.printstuff = ""
		m.ptid = xpt.ptid
		m.accountid = nAcctNum
*!*			REPLACE xptdet.linenum WITH m.linenum
*!*			REPLACE xptdet.printstuff WITH ""
*!*			REPLACE xptdet.ptid      WITH xpt.ptid
*!*			REPLACE xptdet.ptdetid   WITH m.ptdetid
*!*			REPLACE xptdet.accountid WITH nAcctNum
		SKIP 1 IN x856

		llStoredtheStorenum = .F.

		cStyle = "XYZ"
		cColor = "ABC"
		DO WHILE ALLTRIM(x856.segment) != "SE"
			cSegment = TRIM(x856.segment)
			cCode = TRIM(x856.f1)

			IF TRIM(x856.segment) = "LX"
				m.linenum = TRIM(x856.f1)
				m.ptdetid = m.ptdetid + 1
			ENDIF

			IF TRIM(x856.segment) = "W01" && Destination Quantity
				m.totqty = INT(VAL(x856.f1))
				cInnerPack = ALLTRIM(x856.f1)

				IF ALLTRIM(x856.f4) = "IN"
					m.style = ALLTRIM(x856.f5)
					lDoSize = IIF(INLIST(m.style,"W6789","L4126"),.t.,.f.)
					IF m.style # cStyle
						STORE "" TO m.color,m.pack,m.printstuff
						STORE m.style TO cStyle
					ENDIF
				ENDIF

				SKIP 1 IN x856
				IF ALLTRIM(x856.segment) = "N9" AND ALLTRIM(x856.f1) = "VLC"
					STORE UPPER(ALLTRIM(x856.f2)) TO m.color
					IF m.color # cColor
						STORE "" TO m.pack,m.printstuff
						STORE m.color TO cColor
						nLD = 1
					ELSE
						nLD = nLD+1
					ENDIF
				ENDIF
				SKIP -1 IN x856

				IF !("940TYPE*EDI"$m.printstuff)
					m.printstuff = IIF(EMPTY(m.printstuff),"940TYPE*EDI",m.printstuff+CHR(13)+"940TYPE*EDI")
				ENDIF

				IF !"UNITCODE*"+ALLTRIM(x856.f2)$m.printstuff
					m.printstuff = IIF(EMPTY(m.printstuff),"UNITCODE*"+ALLTRIM(x856.f2),;
						m.printstuff+CHR(13)+"UNITCODE*"+ALLTRIM(x856.f2))
				ENDIF

				IF INLIST(ALLTRIM(x856.f6),"EN","UP","UK")
					cUPC = ALLTRIM(x856.f7)
				ELSE
					cUPC = ALLTRIM(x856.f3)
				ENDIF


				SKIP 2 IN x856
				IF ALLTRIM(x856.segment) = "N9" AND ALLTRIM(x856.f1) = "VSZ"
					STORE UPPER(ALLTRIM(x856.f2)) TO cSize
					m.id = IIF(lDoSize,cSize,"")
				ENDIF

				SKIP 1 IN x856
				IF ALLTRIM(x856.segment) = "N9" AND ALLTRIM(x856.f1) = "PJ"  && size number
					STORE ALLTRIM(x856.f2) TO cSizenum
				ENDIF

				SKIP 1 IN x856
				IF ALLTRIM(x856.segment) = "N9" AND ALLTRIM(x856.f1) = "PGC"
					STORE ALLTRIM(x856.f2) TO m.pack
					STORE INT(VAL(m.pack)) TO m.casepack
				ENDIF

				SKIP 1 IN x856
				IF TRIM(x856.segment) = "G69" && shipstyle
					cPrtstr = "DESC*"+UPPER(ALLTRIM(x856.f1))
					IF !cPrtstr$m.printstuff
						m.printstuff = IIF(EMPTY(m.printstuff),cPrtstr,m.printstuff+CHR(13)+cPrtstr)
					ENDIF
				ENDIF

				IF !("STORENUM*"$m.printstuff)
					cStoreInsert = "STORENUM*"+cStoreNum
					IF !cStoreInsert$m.printstuff
						m.printstuff = IIF(EMPTY(m.printstuff),cStoreInsert,m.printstuff+CHR(13)+cStoreInsert)
					ENDIF
				ENDIF

*				ASSERT .F. MESSAGE "At Variables upload: XPTDET...debug"

				m.printstuff = m.printstuff+CHR(13)+;
					"SUBDET"+"*"+TRANSFORM(nLD)+CHR(13)+;
					"SIZE*"+cSize+CHR(13)+;
					"SIZENUM*"+cSizenum+CHR(13)+;
					"UPC*"+cUPC+CHR(13)+;
					"INNERPACK*"+cInnerPack+CHR(13)

				SELECT xptdet
				IF lDoSize
				LOCATE FOR xptdet.ptid = xpt.ptid AND xptdet.STYLE = m.style ;
					AND xptdet.COLOR = m.color AND xptdet.id = m.id AND xptdet.PACK = m.pack
				else
				LOCATE FOR xptdet.ptid = xpt.ptid AND xptdet.STYLE = m.style ;
					AND xptdet.COLOR = m.color AND xptdet.PACK = m.pack
				endif
				IF !FOUND()
					APPEND BLANK
					IF lTesting
						REPLACE xptdet.ship_ref WITH xpt.ship_ref
					ENDIF
					GATHER MEMVAR FIELDS ptid,ptdetid,accountid,STYLE,COLOR,id,PACK,;
						casepack,units,printstuff MEMO
					REPLACE xptdet.totqty WITH m.totqty IN xptdet
					REPLACE xptdet.origqty WITH xptdet.totqty IN xptdet
					REPLACE xpt.qty WITH xpt.qty+xptdet.totqty IN xpt
					REPLACE xpt.origqty WITH xpt.qty IN xpt
					REPLACE xptdet.ptdetid WITH m.ptdetid IN xptdet
				ELSE
					GATHER MEMVAR FIELDS printstuff MEMO
					REPLACE xptdet.totqty  WITH (xptdet.totqty+m.totqty) IN xptdet
					REPLACE xptdet.origqty WITH xptdet.totqty IN xptdet
					REPLACE xpt.qty WITH xpt.qty+m.totqty IN xpt
					REPLACE xpt.origqty WITH xpt.qty IN xpt
				ENDIF
			ENDIF

			IF TRIM(x856.segment) = "W76" && Ctn Qty
*
			ENDIF

			SKIP 1 IN x856
		ENDDO
		STORE "" TO m.printstuff,m.style,m.color,m.pack

		SELECT xptdet
	ENDIF

	IF TRIM(x856.segment) = "LX"
		m.linenum = TRIM(x856.f1)
	ENDIF

	SELECT x856
	IF !EOF()
		SKIP 1 IN x856
	ENDIF
ENDDO

SELECT xptdet
REPLACE xptdet.totqty WITH (xptdet.totqty/xptdet.casepack) ALL
REPLACE xptdet.origqty WITH xptdet.totqty ALL
SELECT xpt
SCAN
	SELECT xptdet
	SUM totqty TO nPTQty FOR xptdet.ptid = xpt.ptid
	SELECT xpt
	REPLACE xpt.qty WITH nPTQty,xpt.origqty WITH nPTQty IN xpt NEXT 1
ENDSCAN

DO m:\dev\prg\setuppercase940

IF lBrowfiles
	WAIT WINDOW "Now browsing XPT/XPTDET files" TIMEOUT 2
	SELECT xpt
	LOCATE
	BROWSE
	SELECT xptdet
	LOCATE
	BROWSE
	IF MESSAGEBOX("Do you wish to continue?",4+16+256,"CONTINUE")=7
		CANCEL
		EXIT
	ENDIF
ENDIF

WAIT cCustname+" Breakdown Round complete..." WINDOW TIMEOUT 2
SELECT x856
IF USED("PT")
	USE IN pt
ENDIF
IF USED("PTDET")
	USE IN ptdet
ENDIF
WAIT CLEAR
RETURN
