utilsetup("SAMSUNG_832")
close data all

public ltesting
public array a856(1)

ltesting = .f.
do m:\dev\prg\_setvars with ltesting

*Use F:\wh\upcmast Alias upcmast In 0 Shared

useca("upcmast","wh",,,"select * from upcmast where accountid in (6649,6650,6651,6652)",,"upcmastsql")
SET STEP ON 

UQTY=0
numadded = 0
numupdated = 0
lccurrdir = ""

goffice="Y"
gmasteroffice="Y"

useca("ackdata","wh")

if !ltesting
	lcpath = 'F:\FTPUSERS\samsung\Stylemaster\'
	lcarchivepath = 'F:\FTPUSERS\samsung\Stylemaster\archive\'
	tsendto="todd.margolin@tollgroup.com"
	tcc="PGAIDIS@FMIINT.COM"
else
	lcpath = 'F:\FTPUSERS\samsung\Stylemaster\'
	lcarchivepath = 'F:\FTPUSERS\samsung\Stylemaster\archive\'
	tsendto="t.margolin@fmiint.com,PGAIDIS@FMIINT.COM"
	tcc=""
endif

cd &lcpath
len1 = adir(ary1,"*.*")
if len1 = 0
	wait window "No files found...exiting" timeout 2
	close data all
	schedupdate()
	_screen.caption=gscreencaption
	on error
	return
endif

lcaddedstr  ="Styles Added:"+chr(13)
lcupdatestr ="Styles Updated:"+chr(13)
ii=1

for thisfile = 1 to len1
	cfilename = alltrim(ary1[thisfile,1])
	xfile = lcpath+cfilename
******TM added 5/20/16
	select * ;
		from upcmastsql ;
		where .f. ;
		into cursor tempmast readwrite
	alter table tempmast drop column upcmastid
	do m:\dev\prg\createx856a
	do m:\dev\prg\loadedifile with xfile,"*","TILDE"

	m.addby = "FILE"+alltrim(transform(thisfile))
	m.adddt = datetime()
	m.updatedt = datetime()
	m.addproc = "SS832"
*	m.accountid = 6649
	m.pnp = .t.
	m.uom = "EA"
	m.info = ""
	lcitemtype =""
	prepackind=''
	select x856
	goto top
	scan
		do case
		case trim(x856.segment) = "ISA"
			cisa_num = alltrim(x856.f13)
		case trim(x856.segment) = "GS"
			cgs_num = alltrim(x856.f6)
			lccurrentgroupnum = alltrim(x856.f6)
		case trim(x856.segment) = "ST" and trim(x856.f1) = "832"
			m.groupnum=lcCurrentGroupNum
			m.isanum=cisa_num
			m.transnum=x856.f2
			m.edicode="SC"
			m.accountid=6649
			m.loaddt=date()
			m.loadtime=datetime()
			m.filename=xfile
			insertinto("ackdata","wh",.t.)
			tu("ackdata")
		case x856.segment = "BCT" and substr((x856.f3),1,2) = "08"
			m.accountid = 6650
*			m.info = m.info+chr(13)+"ACCOUNTID*"+'6650'
			zinfo = "ACCOUNTID*"+'6650'
			lcitemtype = upper(allt(x856.f9))
			m.itemtype= lcitemtype
		case x856.segment = "BCT" and substr((x856.f3),1,1) = "8"
			m.accountid = 6650
*			m.info = m.info+chr(13)+"ACCOUNTID*"+'6650'
			zinfo = "ACCOUNTID*"+'6650'
			lcitemtype = upper(allt(x856.f9))
			m.itemtype= lcitemtype
		case x856.segment = "BCT" and substr((x856.f3),1,2) = "16"
			m.accountid = 6651
*			m.info = m.info+chr(13)+"ACCOUNTID*"+'6651'
			zinfo = "ACCOUNTID*"+'6651'
			lcitemtype = upper(allt(x856.f9))
			m.itemtype= lcitemtype
		case x856.segment = "BCT" and substr((x856.f3),1,2) = "22"
			m.accountid = 6652
*			m.info = m.info+chr(13)+"ACCOUNTID*"+'6652'
			zinfo = "ACCOUNTID*"+'6652'
			lcitemtype = upper(allt(x856.f9))
			m.itemtype= lcitemtype
		case x856.segment = "BCT"
			m.accountid = 6649
*			m.info = m.info+chr(13)+"ACCOUNTID*"+'NODIVSENT'
			zinfo = "ACCOUNTID*"+'NODIVSENT'
			lcitemtype = upper(allt(x856.f9))
			m.itemtype= lcitemtype
*      m.info = m.info+Chr(13)+"TYPE*"+lcItemType
		case x856.segment = "LIN"
			insert into tempmast from memvar
			scatter memvar memo blank
			m.info = ""
			m.itemtype= lcitemtype
			m.info = m.info+"TYPE*"+lcitemtype
			m.info = m.info+chr(13)+zinfo  && added 6/20/16 to capture ACCOUNTID
			m.upc = allt(x856.f5)
			m.style=  allt(x856.f3)
			m.info = m.info+chr(13)+"ORIGSTYLE*"+allt(x856.f3)
			replace unitqty WITH UQTY IN tempmast
			uqty=0
			SET STEP ON 
**reset in case segments do not exist for upc - mvw 03/19/14
			store "" to m.sid,m.descrip
			store 0 to m.price,m.rprice

*!*				If x856.f4 = "UP"
*!*					m.upc = Allt(x856.f5)
*!*				Endif
*!*	      If x856.f2 = "VN"
*!*	        m.style = Allt(x856.f3)
*!*	      Endif
*!*	      If x856.f8 = "CM"
*!*	        m.info = m.info+Chr(13)+"NRFCOLOR*"+Allt(x856.f9)
*!*	* m.color = Allt(x856.f9)
*!*	      Endif
*!*	      If x856.f10 = "SM"
*!*	        m.info = m.info+Chr(13)+"NRFSIZE*"+Allt(x856.f11)
*!*	      Endif

*!*	      If x856.f22 = "SE"
*!*	        m.info = "SEASON*"+Allt(x856.f23)
*!*	      Endif

		case x856.segment = "PID" and x856.f1 = "F" and x856.f2 = "08"
			m.descrip = allt(x856.f5)
			m.info = m.info+chr(13)+"MASTSTYLEDESC*"+allt(x856.f5)
		case x856.segment = "PID" and x856.f1 = "F" and x856.f2 = "73"
			m.info = m.info+chr(13)+"COLORDESC*"+allt(x856.f5)
			m.color = allt(x856.f5)
		case x856.segment = "PID" and x856.f1 = "F" and x856.f2 = "74"
			m.id= x856.f5

		case x856.segment = "PID" and x856.f1 = "F" and x856.f2 = "92"
			m.info = m.info+chr(13)+"COLOR*"+allt(x856.f5)
		case x856.segment = "PID" and x856.f1 = "F" and x856.f2 = "DM"
			m.info = m.info+chr(13)+"DIMENSION*"+allt(x856.f5)

    Case x856.segment = "PID" And x856.f1 = "F" And x856.f2 = "LO"
      m.info = m.info+Chr(13)+"STYLETYPE*"+Allt(x856.f5)

*!*	    Case x856.segment = "PID" And x856.f1 = "F" And x856.f2 = "SC"
*!*	      m.coo=Allt(x856.f5)

		case x856.segment = "CTP" and x856.f2 = "UCP"
			m.price = val(x856.f3)

		case x856.segment = "CTP" and x856.f2 = "MSR"
			m.rprice = val(x856.f3)

		case x856.segment = "G55"
			m.weight = val(alltrim(transform(f23)))
		case x856.segment = "SLN" and x856.f9 = "UP"

			m.info = m.info+chr(13)+"SUBLINE*"+allt(x856.f1)
			m.info = m.info+chr(13)+"SUBCUSTSKU*"
			m.info = m.info+chr(13)+"SUBSTYLE*"
			m.info = m.info+chr(13)+"SUBUOM*"+allt(x856.f5)
			m.info = m.info+chr(13)+"SUBPACK*"+allt(x856.f4)
			UQTY=UQTY+VAL(allt(x856.f4))
			m.info = m.info+chr(13)+"SUBUPC*"+allt(x856.f10)
			m.info = m.info+chr(13)+"SUBCOLOR*"
			m.info = m.info+chr(13)+"SUBSIZE*"
			m.info = m.info+chr(13)
		case x856.segment = "CTT"
SET STEP ON 
			m.unitqty = UQTY
			insert into tempmast from memvar
			UQTY=ROUND(uqty,0)
			IF UQTY=0
			unitqty = 1
			ELSE 
			unitqty = UQTY
			endif
SET STEP ON 
*			Scatter Memvar Memo Blank
		endcase
	endscan

	set step on
	select tempmast
	replace unitqty WITH 1 FOR empty(unitqty)  IN tempmast
*!*		Scan
*!*			If 'SUBLINE' $tempmast.Info
*!*				prepackind='-PREPK'
*!*				Replace tempmast.Id With Alltrim(tempmast.Id)+Alltrim(prepackind)
*!*				replace tempmast.color WITH ''
*!*				prepackind=''
*!*			Endif   
*!*		Endscan

	scan
		replace pnp with .f. for 'SUBCU' $(info) and pnp
	endscan
	delete from tempmast where empty(style)
	locate


*  BROWSE

*** we have read ion this file into tempmast
***ow check for 6303

	recctr = 0


*!*	  Select tempmast
*!*	  Replace Color With Strtran(Color,"   "," ") For "   "$Alltrim(Color)
*!*	  Replace Color With Strtran(Color,"  "," ")  For "  "$Alltrim(Color)
*!*	  Replace Color With Substr(Color,1,4)+" "+Substr(Color,5)
*!*	  Replace Color With Substr(Color,1,4)+" "+Substr(Color,5) For Substr(Color,3,2)=".5"


	select tempmast && has all new records.......
**From tempmast we check for 6303 and add or update 6303 records
	scan
		recctr = recctr +1
		wait window at 10,10 "Checking Record # "+transform(recctr) nowait

		if !found()
			numadded = numadded +1
			select tempmast
			scatter fields except upcmastid memvar memo
			select upcmastsql
			go bott
			ii=ii+1

			select upcmastsql  && add the recored to SQL
			m.adddt = datetime()
			m.upcmastid=sqlgenpk("upcmast","wh")
			m.updatedt = datetime()

			select upcmastsql  && add the recored to SQL
			insert into upcmastsql from memvar
			lcaddedstr = lcaddedstr +m.upc+"-"+m.style+"-"+m.color+"-"+m.id+chr(13)
		else
			select upcmastsql
			locate for upcmastsql.accountid =tempmast.accountid and upcmastsql.upc=tempmast.upc
			if found("upcmastsql")
				replace upcmastsql.price    with tempmast.price   in upcmastsql
				replace upcmastsql.rprice   with tempmast.rprice  in upcmastsql
				replace upcmastsql.style    with tempmast.style   in upcmastsql
				replace upcmastsql.color    with tempmast.color   in upcmastsql
				replace upcmastsql.id       with tempmast.id      in upcmastsql
				replace upcmastsql.updatedt with tempmast.adddt   in upcmastsql
				replace upcmastsql.info     with tempmast.info    in upcmastsql
				SET STEP ON 
				replace upcmastsql.unitqty     with tempmast.unitqty    in upcmastsql
*!*					Replace upcmastsql.itemtype With tempmast.itemtype In upcmastsql
*!*					Replace upcmastsql.htscode  With tempmast.htscode  In upcmastsql
				replace upcmastsql.descrip  with tempmast.descrip  in upcmastsql
			else

				select tempmast
				scatter fields except upcmastid memvar memo
&&next 3 values from dbf upcmast to keep things in sync
				m.adddt = datetime()
				m.upcmastid=sqlgenpk("upcmast","wh")
				m.updatedt = datetime()
				select upcmastsql  && add the recored to SQL
				insert into upcmastsql from memvar
			endif
			numupdated = numupdated +1
			lcupdatestr = lcupdatestr +tempmast.upc+"-"+tempmast.style+"-"+tempmast.color+"-"+tempmast.id+chr(13)
&& add in all the updates
		endif
	endscan

*Set Step On

	select tempmast
	zap
	use in tempmast
	cloadfile = (lcpath+cfilename)
	carchivefile = (lcarchivepath+cfilename)
	copy file [&cLoadFile] to [&cArchiveFile]
	delete file [&cLoadFile]

endfor

tu("upcmastsql")  && push new records and record updates back into sql

tattach = ""
tfrom ="Toll EDI System Operations <toll-edi-ops@tollgroup.com>"
tmessage = "Samsung Stylemaster updated at: "+ttoc(datetime())+chr(13)+;
	chr(13)+lcaddedstr+chr(13)+;
	chr(13)+lcupdatestr

tsubject = "Samsung Stylemaster Update"
do form m:\dev\frm\dartmail2 with tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
wait window "All 832 Files finished processing...exiting" timeout 2

*Close Data All

schedupdate()
*_Screen.Caption=gscreencaption
on error
