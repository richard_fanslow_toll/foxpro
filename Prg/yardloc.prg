lparameters xyardloc

do case
case xyardloc="CT"
	xreturn="CARTERET"
case xyardloc="NK"
	xreturn="635 SECURE"
case xyardloc="PK"
	xreturn="PORT KEARNY"
case xyardloc="LI"
	xreturn="LINDEN"
case xyardloc="DC"
	xreturn="DCL"
case xyardloc="SP"
	xreturn="SAN PEDRO"
case xyardloc="WI"
	xreturn="WILMINGTON"
case xyardloc="CA"
	xreturn="CARSON"
case xyardloc="RI"
	xreturn="RIALTO"
case xyardloc="ML"
	xreturn="MIRA LOMA"
case xyardloc="PL"
	xreturn="PLG"
case xyardloc="DL"
	xreturn="DENSO LONG BEACH"
case xyardloc="OD"
	xreturn="ODW ONTARIO"
case xyardloc="BL"
	xreturn="BIG LOTS RANCHO CUCAMONGA"
case xyardloc="BS"
	xreturn="BROWN SHOE CHINO"
case xyardloc="MO"
	xreturn="MACY'S ONTARIO"
case xyardloc="CI"
	xreturn="CITY OF INDUSTRY"
case xyardloc="SG"
	xreturn="SG SAN BERARDINO"
otherwise
	xreturn="???"
endcase

return padr(xreturn,23)