*!* NANJING USA 945 PickPack MASTER BOL(Whse. Shipping Advice) Creation Program
*!* *** FOR DSDC Orders ONLY
*!* Creation Date: 08.24.2009 by Joe

PARAMETERS nWO_Num,cBOL,cShip_ref,cOffice,lPTFlag,lPickPackDSDC

lMismatch = .F.
PUBLIC ARRAY thisarray(1)
PUBLIC cWO_Num,cWO_NumStr,cWO_NumOld,cCustname,nUnitSum,lTesting,tsendto,tcc,tsendtoerr,tccerr,cUseFolder,lLoadSQLBL
PUBLIC cFilenameShort,cMBOL,lTestingMBOL,lTestmail,cPPName,lTestInput,nSubBillCnt,lBrowBOLs,cErrMsg,lNanFilesOut,lOverflow
PUBLIC cMod,cFilenameOut,cFilenameArch,cWO_NumList,lcPath,cEDIType,cArchiveFolder,cISA_Num,cSubBOLDetail,cProgName
cProgName = "nanjing945pp-dsdc_create_mbol.prg"

ON ERROR DEBUG
SET STEP ON
TRY
	tfrom = "Toll EDI Operations <toll-edi-ops@tollgroup.com>"
	nRec1 = 9999999
	SET ESCAPE ON
	ON ESCAPE CANCEL
	SET ASSERTS ON
	lIsError = .F.
	lDoCatch = .T.
	lDoManUC = .F.
	cPPName = "Nanjing"
	cErrMsg = ""
	IF !lPTFlag
		cShip_ref = ""
	ENDIF
	goffice = IIF(cOffice = "C","1",cOffice)

	lTestingMBOL = .F.
	lTestInput = .F.
	lBrowBOLs = IIF(lTestingMBOL,.T.,.F.)
	lBrowBOLs = .F.

	nFilenum = 0
	cSubBOLDetail = ""

	WAIT WINDOW "At the start of "+cPPName+" 945 PICKPACK (MBOL) process..." TIMEOUT 2

*	ASSERT .F. MESSAGE "In 945pp-dsdc_create, start...debug"
	IF lPickPackDSDC
		nAcctNum = 4694
	ELSE
		nAcctNum = 4610
	ENDIF
	lISAFlag = .T.
	lSTFlag = .T.
	lSQLMail = .F.
	lCloseOutput = .T.

	IF TYPE("cOffice") = "L"
		IF !lTestingMBOL
			WAIT WINDOW "Office not provided...terminating" TIMEOUT 3
			lCloseOutput = .F.
			cErrMsg = "No OFFICE"
			DO ediupdate WITH cErrMsg,.T.
			THROW
		ELSE
			CLOSE DATABASES ALL
			cOffice = "C"
			nWO_Num =  4013418
			cBOL = "04907314610018358"
		ENDIF
	ENDIF
	cMBOL = ""
	cMod = IIF(cOffice = "C","1",cOffice)
	goffice = cMod
	gMasterOffice = IIF(cOffice = "I","N",cOffice)
	lLoadSQLBL  = .T.

	cEDIType = "945"
	lParcelType = .F.
	lOverflow = .f.

	cBOL = ALLTRIM(cBOL)
	STORE cBOL TO cMBOL && Added to distinguish Master Bill # for flag clearing


	IF lTestingMBOL
		CREATE CURSOR temp945 (accountid i,isa_num c(10),st_num c(10),wo_num i,bol_no c(20),ship_ref c(20),filename c(50),filedate T)
		CREATE CURSOR qtycompare (wo_num i,ship_ref c(10), osqty i, ppqty i,pctdiff i)
	ENDIF

	IF TYPE("nWO_Num")<> "N"
		lCloseOutput = .F.
		cErrMsg = "BAD WO#"
		DO ediupdate WITH cErrMsg,.T.
		THROW
	ENDIF
	cWO_Num = ALLTRIM(STR(nWO_Num))

	lEmail = .T.
	lTestmail = IIF(lTestingMBOL,.T.,.F.) && Sends mail to Joe only
	lNanFilesOut = .T. && If true, auto-copies output to FTP folder (default = .f.)
	lStandalone = IIF(lTestingMBOL,.T.,.F.)
	STORE "LB" TO cWeightUnit
	lPrepack = .F.
	lPick = .T.

	cUseFolder = "F:\WHY\WHDATA\"
	IF lTestInput
		cUseFolder = "F:\WHP\WHDATA\"
	ENDIF
	cRetMsg = "X"

*!*	lNanFilesOut = .F.
*!*	lTestmail = .T.

	lSkipCompare= IIF(nAcctNum = 4694 AND INLIST(cOffice,"I","N"),.T.,.F.)
	IF lTestingMBOL OR cBOL = "04907454610141691"
		lSkipCompare = .T.
	ENDIF

	IF lSkipCompare
		WAIT WINDOW "Skipping Labels/Cartons Comparison..." TIMEOUT 1
	ELSE
		DO m:\dev\prg\sqldata-COMPARENAN WITH cUseFolder,cBOL,nWO_Num,cOffice,nAcctNum,cPPName
		IF cRetMsg<>"OK"
			lCloseOutput = .F.
			cErrMsg = "SQL ERROR"
			DO ediupdate WITH cErrMsg,.T.
			lDoCatch = .F.
			THROW
		ENDIF
	ENDIF

	PUBLIC c_CntrlNum,c_GrpCntrlNum,cReturned
	WAIT WINDOW "Now preparing tables and variables" NOWAIT

*!* SET CUSTOMER CONSTANTS
	cCustname = "NANJING"  && Customer Identifier
	cX12 = "004050"  && X12 Standards Set used
	cMailName = "Nanjing USA"
	SELECT 0
	USE F:\3pl\DATA\mailmaster ALIAS mm
	LOCATE FOR edi_type = '945' AND mm.office = cOffice AND mm.accountid = nAcctNum
	tsendto = IIF(mm.use_alt,sendtoalt,mm.sendto)
	tcc = IIF(mm.use_alt,mm.ccalt,mm.cc)
	LOCATE
	LOCATE FOR mm.office = 'X' AND mm.accountid = 9999
	tsendtoerr = IIF(mm.use_alt,sendtoalt,mm.sendto)
	tccerr = IIF(mm.use_alt,mm.ccalt,mm.cc)
	LOCATE
	LOCATE FOR mm.edi_type = 'MISC' AND mm.taskname = 'GENERAL'
	tsendtotest = IIF(mm.use_alt,sendtoalt,mm.sendto)
	tcctest = IIF(mm.use_alt,mm.ccalt,mm.cc)
	LOCATE
	LOCATE FOR mm.edi_type = 'MISC' AND mm.taskname = 'NANQTYERR'
	tsendtoqty = IIF(mm.use_alt,sendtoalt,mm.sendto)
	tccqty = IIF(mm.use_alt,mm.ccalt,mm.cc)


*!* SET OTHER CONSTANTS
	DO CASE
		CASE cOffice = "M"
			cCustLoc =  "FL"
			cFMIWarehouse = ""
			cCustPrefix = "945"+"f"
			cDivision = "Florida"
*		cFolder = "WHM"
			cSF_Addr1  = "11400 NW 32ND AVE"
			cSF_CSZ    = "MIAMI*FL*33167"

		CASE cOffice = "N"
			cCustLoc =  "NJ"
			cFMIWarehouse = ""
			cCustPrefix = "945"+"j"
			cDivision = "New Jersey"
*		cFolder = "WHN"
			cSF_Addr1  = "800 FEDERAL BLVD"
			cSF_CSZ    =  "CARTERET*NJ*07008"

		OTHERWISE
			cCustLoc =  "C2"
			cFMIWarehouse = ""
			cCustPrefix = "945"+"y"
			cDivision = "Carson2"
*		cFolder = "WHC"
			cSF_Addr1  = "1000 E 223RD ST"
			cSF_CSZ    = "CARSON*CA*90745"
	ENDCASE
	cCustFolder = UPPER(cCustname)  && +"-"+cCustLoc
	lNonEDI = .F.  && Assumes not a Non-EDI 945
	lDoPAL = .T.
	STORE "" TO lcKey
	STORE 0 TO alength,nLength

*!* SET OTHER CONSTANTS
	dt1 = TTOC(DATETIME(),1)
	dtmail = TTOC(DATETIME())
	dt2 = DATETIME()
	cString = ""

	csendqual = "ZZ"
	csendid = "FMIF"
	crecqual = "01"
	crecid = "654351030"
	cMBOLFolder = "f:\ftpusers\"+cCustFolder+"\945-Staging\mbolhold\"
	IF !lTesting
		CD &cMBOLFolder
		DELETE FILE *.*
	ENDIF
	cOutFolder = "F:\FTPUSERS\NANJING\945OUT\"

	cfd = CHR(0x07)
	csegd = "~"
	nSegCtr = 0
	cterminator = ">" && Used at end of ISA segment
	cdate = DTOS(DATE())
	ctruncdate = RIGHT(cdate,6)
	ctime = LEFT(TIME(DATE()),2)+SUBSTR(TIME(DATE()),4,2)
	cfiledate = cdate+ctime
	cOrig = "J"
	nSTCount = 0
	cStyle = ""
	cPTString = ""
	nTotCtnWt = 0
	nTotCtnCount = 0
	nUnitSum = 0
	cTargetStyle = ""

** PAD ID Codes
	csendidlong = PADR(csendid,15," ")
	crecidlong = PADR(crecid,15," ")

	IF !USED('OUTSHIP')
		xsqlexec("select * from outship where accountid = "+TRANSFORM(nAcctNum)+" and bol_no = '"+cBOL+"'",,,"wh")
		INDEX ON bol_no TAG bol_no
		INDEX ON outshipid TAG outshipid
		INDEX ON wo_num TAG wo_num
	ENDIF

	IF !USED('OUTDET')
		selectoutdet()
		SELECT outdet
		INDEX ON outdetid TAG outdetid
	ENDIF

	IF USED('BL')
		USE IN bl
	ENDIF
	IF lLoadSQLBL
		csq1 = [select * from bl where mblnum = ']+cMBOL+"'"
		xsqlexec(csq1,,,"wh")
	ELSE
		USE (cUseFolder+"BL") IN 0 ALIAS bl
	ENDIF

	SELECT bl
	LOCATE
	SELECT 0
	SELECT bol_no FROM bl WHERE bl.mblnum = cMBOL INTO CURSOR tempbl1 GROUP BY 1
	IF lBrowBOLs
		SELECT tempbl1
		BROWSE TIMEOUT 10
	ENDIF
	STORE RECCOUNT() TO nSubBillCnt
	IF nSubBillCnt = 0
		WAIT WINDOW "There are zero sub-bills in this BOL...debug"
	ENDIF
	USE IN tempbl1
	SELECT bl
	LOCATE

*!* Mail sending for all MBOL 945s
	tsubject2 = "NOTICE: Nanjing 945 MBOL Process"
	tattach2 = " "
	tsendto2 = tsendto
	tcc2 = tcc
	tmessage2 = "Potential files for Nanjing MBOL# "+cBOL+"...monitor output for correct number of files."
	tmessage2 = tmessage2 + CHR(13) + "Expected number of 945 files: "+ALLTRIM(STR(nSubBillCnt))
	tmessage2 = tmessage2 + CHR(13) + "Processed at "+TTOC(DATETIME())
	IF !lTestingMBOL
		DO FORM m:\dev\frm\dartmail2 WITH tsendto2,tfrom,tsubject2,tcc2,tattach2,tmessage2,"A"
	ENDIF
*!* End Mailing section


&& Added code blocks to the following to overcome BOL vs. WO selection issues, 12.23.2005
	SELECT outship
	IF USED("sqlwonanjing")
		USE IN sqlwonanjing
	ENDIF
	DELETE FILE "F:\3pl\DATA\sqlwonanjing.dbf"
	SELECT bol_no,wo_num ;
		FROM outship WHERE bol_no = cBOL ;
		GROUP BY 1,2 ;
		ORDER BY 1,2 ;
		INTO DBF F:\3pl\DATA\sqlwonanjing
	USE IN sqlwonanjing
	USE F:\3pl\DATA\sqlwonanjing IN 0 ALIAS sqlwonanjing
	SELECT sqlwonanjing
*!*	BROWSE
	cRetMsg = ""

	DO m:\dev\prg\sqlconnectnanjing_bol  WITH nAcctNum,cCustname,cPPName,.T.,cOffice && Amended for UCC number sequencing
	IF cRetMsg<>"OK"
		cErrMsg = cRetMsg
		DO ediupdate WITH cErrMsg,.T.
		THROW
	ENDIF

*!* Added this code block to remove zero-qty cartons from SQL cursor
	SELECT ucc,SUM(totqty) AS uccqty FROM vnanjingpp GROUP BY 1 INTO CURSOR tempchk HAVING uccqty=0
	SELECT tempchk
	LOCATE
	IF !EOF()
		SCAN
			STORE ALLT(ucc) TO cUCC
			SELECT vnanjingpp
			LOCATE
			DELETE FOR ucc = cUCC
		ENDSCAN
	ENDIF
	USE IN tempchk


	CREATE CURSOR tempqtyerr (ship_ref c(20),ctnqty i,totqty i,cqty i,uqty i)

	SELECT ship_ref,outshipid,ctnqty,IIF("PREPACK-REP"$outship.shipins,.T.,.F.) AS PRR ;
		FROM outship ;
		WHERE bol_no = cBOL ;
		INTO CURSOR out1

	SELECT a.*,INT(VAL(b.PACK)) AS npack,b.totqty AS rawqty,b.origqty,0 AS totqty2 ;
		FROM out1 a LEFT JOIN outdet b ;
		ON a.outshipid = b.outshipid ;
		INTO CURSOR out2

	SELECT ship_ref,ctnqty,SUM(rawqty) AS qty ;
		FROM out2 ;
		GROUP BY 1 ;
		INTO CURSOR temptotals

	SELECT temptotals
	SCAN
		cSRUse = ALLTRIM(temptotals.ship_ref)

		SELECT vnanjingpp
		SUM totqty TO m.uqty FOR vnanjingpp.ship_ref = cSRUse
		SELECT 0
		SELECT DISTINCT(ucc) FROM vnanjingpp WHERE vnanjingpp.ship_ref = cSRUse INTO CURSOR tempxyz
		m.cqty = RECCOUNT()
		USE IN tempxyz
		IF (uqty # temptotals.qty) OR (cqty # temptotals.ctnqty)
			INSERT INTO tempqtyerr (ship_ref,ctnqty,totqty,cqty,uqty) VALUES (cSRUse,temptotals.ctnqty,temptotals.qty,m.cqty,m.uqty)
		ENDIF
	ENDSCAN

	SELECT tempqtyerr
	LOCATE
	IF !EOF()
		IF lTesting
*			BROWSE
		ENDIF
		totqtyerr()
		cErrMsg = "OD/CZN QTY DISCREP."
		DO ediupdate WITH cErrMsg,.T.
		THROW
	ENDIF

	SELECT vnanjingpp
	SET RELATION TO outdetid INTO outdet

	IF !USED("scacs")
		xsqlexec("select * from scac","scacs",,"wh")
		INDEX ON scac TAG scac
	ENDIF

	SELECT outship
	LOCATE
	WAIT CLEAR
	WAIT WINDOW "Now creating Header information" NOWAIT

	SELECT outship
	LOCATE

	IF !SEEK(nWO_Num,"outship","wo_num")
		WAIT CLEAR
		WAIT WINDOW "Invalid Work Order Number - Not Found in OUTSHIP!" TIMEOUT 2
*	DO ediupdate WITH "WO# NOT FOUND",.T.
		THROW
	ENDIF

	LOCATE
	IF !lTestingMBOL
		IF EMPTY(cBOL)  && May be empty if triggered from PT screen, or FedEx shipment
			WAIT CLEAR
			WAIT WINDOW "MISSING Bill of Lading Number!" TIMEOUT 2
			cErrMsg = "BOL# EMPTY"
			DO ediupdate WITH cErrMsg,.T.
			THROW
		ELSE
			IF !SEEK(PADR(TRIM(cBOL),20),"outship","bol_no")
				WAIT CLEAR
				WAIT WINDOW "Invalid Bill of Lading Number - Not Found!" TIMEOUT 2
				cErrMsg = "BOL# NOT FOUND"
				DO ediupdate WITH cErrMsg,.T.
				THROW
			ENDIF
		ENDIF
	ENDIF
	SELECT outship
	SET ORDER TO
	LOCATE && in OUTSHIP

*************************************************************************
*1	PTIDSOURCE/OUTSHIP RECORD MAIN LOOP
*	Loops through individual OUTSHIP lines for BOL#
*************************************************************************

	WAIT CLEAR
	WAIT WINDOW "Now creating Master BOL#-based information...stand by" TIMEOUT 5  && Added as an intentional delay
	WAIT WINDOW "Now creating Master BOL#-based information..." NOWAIT NOCLEAR
	SELECT bl
	LOCATE
	cMissDel = ""
	bolscanstr = "bl.mblnum = cMBOL"
	COUNT TO nTotSubs FOR &bolscanstr
	nBLID = 0
	nFileID = 0
	STORE "" TO cWO_NumStr,cWO_NumList
	STORE "X" TO cWO_NumOld
	nSubCnt = 0

	LOCATE
	LOCATE FOR &bolscanstr

	SCAN FOR &bolscanstr
		WAIT WINDOW "" TIMEOUT 2
		cBOL = TRIM(bl.bol_no)
*		ASSERT .F. MESSAGE "In MBOL scan"
		CREATE CURSOR qtycompare (wo_num i,ship_ref c(10), osqty i, ppqty i,pctdiff i)
		CREATE CURSOR temp945 (accountid i,isa_num c(10),st_num c(10),wo_num i,bol_no c(20),ship_ref c(20),filename c(50),filedate T)
*!*		Insert MBOL/SBOL numbers below to run single sub-BOLs
		IF (cMBOL = "04907314610176931" AND cBOL # "04907314610176836")  && and lTesting
			LOOP
		ENDIF

		dt1 = TTOC(DATETIME(),1)

		cFilenameHold = ("f:\ftpusers\"+cCustFolder+"\945-Staging\mbolhold\"+cCustPrefix+dt1+".txt")
		cFilenameShort = JUSTFNAME(cFilenameHold)
		cArchiveFolder = UPPER("f:\ftpusers\"+cCustFolder+"\945OUT\945ARCHIVE\")
		lcPath = ("f:\ftpusers\"+cCustFolder+"\945OUT\")
		cFilenameArch = (cArchiveFolder+cFilenameShort)
		cFilenameOut = (lcPath+cFilenameShort)
		nFilenum = FCREATE(cFilenameHold)

		DO num_incr_isa
		headerfill()

		nAcctNum = bl.accountid
		nFileID = nFileID+1
		cFileID = "-"+ALLT(STR(nFileID))
		DO m:\dev\prg\swc_cutctns WITH cBOL

		nBLID = bl.blid
		nSubCnt = nSubCnt + 1

		xsqlexec("select * from bldet where mod='"+goffice+"' and blid="+TRANSFORM(nBLID),,,"wh")

		SELECT bldet
		nLoops = 0
		nPTCount = 0
		SCAN FOR bldet.blid = nBLID AND bldet.accountid = nAcctNum
			cBLShip_ref = ALLTRIM(bldet.ship_ref)
			lCombPT = IIF(bldet.combinedpt,.T.,.F.)

			nPTCount = nPTCount + 1
			IF lCombPT
				cOSScanStr = "outship.combpt = PADR(cBLShip_ref,20) AND outship.accountid = nAcctNum"
			ELSE
				IF cMBOL = '04907314610221932'
					cOSScanStr = "outship.ship_ref = PADR(cBLShip_ref,20) AND INLIST(outship.accountid,4610,4694)"
				ELSE
					cOSScanStr = "outship.ship_ref = PADR(cBLShip_ref,20) AND outship.accountid =nAcctNum"
				ENDIF
			ENDIF

			SELECT outship
			LOCATE FOR &cOSScanStr
			IF !FOUND()
				ASSERT .F. MESSAGE "Missing pickticket...Debug here"
				cErrMsg = "MISS OD SHIP_REF"
				DO ediupdate WITH cErrMsg,.T.
				THROW
			ENDIF
			SCAN FOR &cOSScanStr
				IF " OV"$outship.ship_ref
					LOOP
				ENDIF
				SCATTER MEMVAR MEMO
				cShip_ref = ALLTRIM(outship.ship_ref)

				IF "!!"$cShip_ref
					cBaseShip_ref = LEFT(cShip_ref,AT("!",cShip_ref,1)-1)
					cShip_ref2 = LEFT(cShip_ref,AT("!",cShip_ref,1)-4)
					nSuffix = INT(VAL(RIGHT(cBaseShip_ref,3)))
					DO CASE
						CASE ("!!D"$cShip_ref)
							cSuffix = PADL(ALLTRIM(STR(nSuffix+4)),3,'0')
						CASE ("!!C"$cShip_ref)
							cSuffix = PADL(ALLTRIM(STR(nSuffix+3)),3,'0')
						CASE ("!!B"$cShip_ref)
							cSuffix = PADL(ALLTRIM(STR(nSuffix+2)),3,'0')
						OTHERWISE
							cSuffix = PADL(ALLTRIM(STR(nSuffix+1)),3,'0')
					ENDCASE
					cShip_refedi = cShip_ref2+cSuffix
				ELSE
					cShip_refedi = cShip_ref
				ENDIF

				nWO_Num = outship.wo_num
				cWO_Num = ALLTRIM(STR(outship.wo_num))
				cPadWO_Num = PADR(cWO_Num,10)
				nTotCtnCount = m.qty
				cPO_Num = ALLTRIM(m.cnee_ref)
				alength = ALINES(apt,outship.shipins,.T.,CHR(13))
				lWalMart = IIF(("WALMART"$UPPER(m.consignee)) OR ("WAL-MART"$UPPER(m.consignee)),.T.,.F.)

				nOSQty = m.qty
				IF nOSQty = 0
					LOOP
				ENDIF

				IF TRIM(cWO_Num) <> TRIM(cWO_NumOld)
					STORE cWO_Num TO cWO_NumOld

					IF EMPTY(cWO_NumStr)
						STORE cWO_Num TO cWO_NumStr
					ELSE
						cWO_NumStr = (cWO_NumStr+", "+cWO_Num)
					ENDIF
					IF EMPTY(cWO_NumList)
						STORE cWO_Num TO cWO_NumList
					ELSE
						cWO_NumList = (cWO_NumList+CHR(13)+cWO_Num)
					ENDIF
				ENDIF

				nOutshipid = m.outshipid
				cPTString = IIF(EMPTY(cPTString),m.consignee+" "+cShip_ref,cPTString+CHR(13)+m.consignee+" "+cShip_ref)
				m.CSZ = TRIM(m.CSZ)

				IF EMPTY(M.CSZ)
					WAIT CLEAR
					WAIT WINDOW "No City/State/ZIP info...exiting" TIMEOUT 2
					cErrMsg = "BAD ADDRESS INFO"
					DO ediupdate WITH cErrMsg,.T.
					THROW
				ENDIF

				cCity = segmentget(@apt,"CITY",alength)
				cState = segmentget(@apt,"STATE",alength)
				cZip = segmentget(@apt,"ZIPCODE",alength)

				IF EMPTY(cCity) OR EMPTY(cState) OR EMPTY(cZip)
					m.CSZ = ALLTRIM(STRTRAN(m.CSZ,"  "," "))
					nSpaces = OCCURS(" ",ALLTRIM(m.CSZ))
					nCommaPos = AT(",",m.CSZ)
					nLastSpace = AT(" ",m.CSZ,nSpaces)

					IF ISALPHA(SUBSTR(TRIM(m.CSZ),AT(" ",m.CSZ,nSpaces-1)+1,2))
						cCity = LEFT(TRIM(m.CSZ),AT(",",m.CSZ)-1)
						cState = SUBSTR(m.CSZ,nCommaPos+2,2)
						cZip = TRIM(SUBSTR(m.CSZ,nLastSpace+1))
					ELSE
						WAIT CLEAR
						WAIT WINDOW "NOT ALPHA: "+SUBSTR(TRIM(m.CSZ),AT(" ",m.CSZ,nSpaces-1)+1,2) TIMEOUT 3
						cCity = LEFT(TRIM(m.CSZ),AT(",",m.CSZ)-1)
						cState = SUBSTR(TRIM(m.CSZ),AT(" ",m.CSZ,nSpaces-2)+1,2)
						cZip = TRIM(SUBSTR(m.CSZ,nLastSpace+1))
					ENDIF
				ENDIF
				nCtnNumber = 1  && Begin sequence count

				DO num_incr_st
				WAIT CLEAR
				WAIT WINDOW "Now creating Line Item information" NOWAIT NOCLEAR

				INSERT INTO temp945 (accountid,isa_num,st_num,wo_num,bol_no,ship_ref,filename,filedate) ;
					VALUES (m.accountid,c_CntrlNum,c_GrpCntrlNum,m.wo_num,m.bol_no,m.ship_ref,cFilenameShort,dt2)

				STORE "ST"+cfd+"945"+cfd+PADL(c_GrpCntrlNum,9,"0")+csegd TO cString
				DO cstringbreak
				nSTCount = nSTCount + 1
				nSegCtr = 1

				cShipmentID = RIGHT(TRIM(cMBOL),10)
				STORE "W06"+cfd+"N"+cfd+cShip_refedi+cfd+cdate+cfd+TRIM(cShipmentID)+cfd+TRIM(cWO_Num)+cfd+cPO_Num+csegd TO cString
				DO cstringbreak
				nSegCtr = nSegCtr + 1

				cStoreNum = segmentget(@apt,"STORENUM",alength)
				IF EMPTY(cStoreNum)
					cStoreNum = ALLTRIM(m.dcnum)
				ENDIF
				STORE "N1"+cfd+"ST"+cfd+ALLTRIM(m.consignee)+cfd+"92"+cfd+cStoreNum+csegd TO cString
				DO cstringbreak
				nSegCtr = nSegCtr + 1

				STORE "N1"+cfd+"BY"+cfd+ALLTRIM(m.shipfor)+cfd+"92"+cfd+ALLTRIM(m.sforstore)+csegd TO cString
				DO cstringbreak
				nSegCtr = nSegCtr + 1

				STORE "N9"+cfd+"BM"+cfd+TRIM(cBOL)+csegd TO cString
				DO cstringbreak
				nSegCtr = nSegCtr + 1

				STORE "N9"+cfd+"DP"+cfd+TRIM(outship.dept)+csegd TO cString
				DO cstringbreak
				nSegCtr = nSegCtr + 1

				cMR = segmentget(@apt,"MR",alength)
				STORE "N9"+cfd+"MR"+cfd+TRIM(cMR)+csegd TO cString
				DO cstringbreak
				nSegCtr = nSegCtr + 1

				cInvoice = IIF(EMPTY(ALLTRIM(m.keyrec)),ALLTRIM(m.ship_ref),ALLTRIM(m.keyrec))
				STORE "N9"+cfd+"CN"+cfd+TRIM(cInvoice)+csegd TO cString
				DO cstringbreak
				nSegCtr = nSegCtr + 1

				STORE "N9"+cfd+"IA"+cfd+TRIM(outship.vendor_num)+csegd TO cString
				DO cstringbreak
				nSegCtr = nSegCtr + 1

*!*				IF lTestingMBOL
*!*					ddel_date = DATE()
*!*					dapptnum = "99999"
*!*					dapptdate = DATE()
*!*				ELSE
				ddel_date = m.del_date
				IF emptynul(ddel_date)
					cErrMsg = "MISSING DELDATE"
					DO ediupdate WITH cErrMsg,.T.
					THROW
				ENDIF
				IF ddel_date<DATE()-7
					WAIT WINDOW "Delivery Date is wrong" NOWAIT
					ASSERT .F. MESSAGE "At old Del. Date"
				ELSE
					WAIT WINDOW "Delivery Date is ok" NOWAIT
				ENDIF

				dapptnum = IIF(EMPTY(m.appt_num),"UNK",TRIM(m.appt_num))
				dapptdate = m.appt
				IF EMPTY(dapptdate)
					dapptdate = m.del_date
				ENDIF
*!*				ENDIF

				STORE "N9"+cfd+"AO"+cfd+dapptnum+csegd TO cString
				DO cstringbreak
				nSegCtr = nSegCtr + 1

				cUseTime = "15300000"
				STORE "G62"+cfd+"11"+cfd+DTOS(ddel_date)+cfd+"9"+cfd+cUseTime+csegd TO cString  && Ship date
				DO cstringbreak
				nSegCtr = nSegCtr + 1

				SELECT scacs
				IF !EMPTY(TRIM(outship.scac))
					STORE outship.scac TO m.scac
				ELSE
					IF lTestingMBOL
						STORE "FMIX" TO m.scac
					ELSE
						IF UPPER(LEFT(outship.SHIP_VIA,3)) = "UPS"
							REPLACE outship.scac WITH "UPSN" IN outship
						ELSE
							cErrMsg = "MISSING SCAC"
							DO ediupdate WITH cErrMsg,.T.
							THROW
						ENDIF
					ENDIF
				ENDIF

				SELECT outship
				cShipType="J"
				STORE "W27"+cfd+cShipType+cfd+TRIM(m.scac)+cfd+TRIM(m.SHIP_VIA)+csegd TO cString
				DO cstringbreak
				nSegCtr = nSegCtr + 1

*************************************************************************
*2	DETAIL LOOP - MATCHING SHIP_REF FIELD (FROM OUTSHIP)
*************************************************************************
*		ASSERT .F. MESSAGE "At detail loop...debug"
				WAIT CLEAR
				WAIT WINDOW "Now creating Detail information "+cShip_ref NOWAIT NOCLEAR
				SELECT vnanjingpp
				IF nAcctNum = 4694
					COUNT TO nPPQty FOR ship_ref = TRIM(cShip_ref) AND outshipid = nOutshipid AND totqty <> 0 && totqty = 0, cut cartons
				ELSE
					SUM totqty TO nPPQty FOR ship_ref = TRIM(cShip_ref) AND outshipid = nOutshipid
				ENDIF

				INSERT INTO qtycompare (wo_num,ship_ref,osqty,ppqty) VALUES (nWO_Num,cShip_ref,nOSQty,nPPQty)
				SELECT qtycompare
				GO BOTT
				nPctdiff = INT(ABS((osqty-ppqty)/osqty)*100)
				REPLACE pctdiff WITH nPctdiff
				STORE 0 TO nOSQty,nPPQty,nPctdiff
				SELECT vnanjingpp
				LOCATE
				IF DATETIME()<DATETIME(2013,09,13,19,00,00)
					BROWSE TIMEOUT 40
				ENDIF

				LOCATE FOR ship_ref = TRIM(cShip_ref) AND outshipid = nOutshipid
				IF !FOUND()
					ASSERT .F. MESSAGE "At PT not found...debug"
					SET STEP ON
					IF !lTestingMBOL
						WAIT WINDOW "PT "+cShip_ref+" NOT FOUND in VNANJINGPP...ABORTING" TIMEOUT 2
						IF !lTestingMBOL
							lSQLMail = .T.
						ENDIF
						cErrMsg = "MISS PT: "+cShip_ref
						DO ediupdate WITH cErrMsg,.T.
						THROW
					ELSE
						LOOP
					ENDIF
				ENDIF

*!* Added this code to trap unmatched OUTDETIDs in OUTDET, Joe 11.22.2005
				SCAN FOR ship_ref = TRIM(cShip_ref) AND outshipid = nOutshipid
					IF EMPTY(outdet.outdetid)
						WAIT WINDOW "OUTDETID "+TRANSFORM(vnanjingpp.outdetid)+" NOT FOUND IN OUTDET!...CLOSING" TIMEOUT 3
						cErrMsg = "MISS OD-ID: "+TRANSFORM(vnanjingpp.outdetid)
						DO ediupdate WITH cErrMsg,.T.
						THROW
					ENDIF
				ENDSCAN

				VSCANSTR = "vnanjingpp.ship_ref = cShip_ref and vnanjingpp.outshipid = nOutshipid"
				SELECT vnanjingpp
				LOCATE FOR &VSCANSTR
				cUCC = "XXX"

				DO WHILE &VSCANSTR
					lSkipBack = .T.

					IF vnanjingpp.totqty = 0
						SKIP 1 IN vnanjingpp
						LOOP
					ENDIF

					IF vnanjingpp.ucc = 'CUTS'
						SKIP 1 IN vnanjingpp
						LOOP
					ENDIF

					IF TRIM(vnanjingpp.ucc) <> cUCC
						STORE TRIM(vnanjingpp.ucc) TO cUCC
					ENDIF

					STORE "LX"+cfd+ALLTRIM(STR(nCtnNumber))+csegd TO cString   && Carton Seq. # (up to Carton total)
					lDoManSegment = .T.
					DO cstringbreak
					nSegCtr = nSegCtr + 1
					DO WHILE vnanjingpp.ucc = cUCC

						IF lDoManSegment
*!*						IF lDoManUC
*!*	*						ASSERT .F. MESSAGE "At SCC WO#...debug"
*!*							c214 = vnanjingpp.scc214
*!*							c214 = TRIM(STRTRAN(TRIM(c214)," ",""))
*!*							lDoManSegment = .F.
*!*							STORE "MAN"+cfd+"UC"+cfd+TRIM(c214)+csegd TO cString  && UCC Number (Wal-mart DSDC only)
*!*							DO cstringbreak
*!*							nSegCtr = nSegCtr + 1
*!*						ELSE
							IF !EMPTY(cUCC) AND cUCC # "XXX"
								lDoManSegment = .F.
								STORE "MAN"+cfd+"GM"+cfd+TRIM(cUCC)+csegd TO cString  && UCC Number (Wal-mart DSDC only)
								DO cstringbreak
								nSegCtr = nSegCtr + 1
							ELSE
								WAIT CLEAR
								cMsg = "Empty UCC Number in vNanjingPP "+cShip_ref
								WAIT WINDOW cMsg TIMEOUT 2
								IF lTestingMBOL
									CLOSE DATABASES ALL
									=FCLOSE(nFilenum)
									ERASE &cFilenameHold
									THROW
								ELSE
									cErrMsg = "EMPTY UCC# in "+cShip_ref
									DO ediupdate WITH cErrMsg,.T.
									=FCLOSE(nFilenum)
									ERASE &cFilenameHold
									THROW
								ENDIF
							ENDIF
*!*						ENDIF
						ENDIF

						cColor = ""
						IF nAcctNum = 4694
							IF "ORIG_COLOR*"$outdet.printstuff
								valreturn("orig_color")
								STORE cOrig_color TO cColor
							ENDIF
						ELSE
							cColor = TRIM(outdet.COLOR)
						ENDIF
						IF EMPTY(ALLTRIM(cColor))
							IF "ORIG_COLOR*"$outdet.printstuff
								valreturn("orig_color")
								STORE cOrig_color TO cColor
							ENDIF
						ELSE
						ENDIF

						cSize = ""
						IF "ORIG_ID*"$outdet.printstuff
							valreturn("orig_id")
							STORE cOrig_id TO cSize
						ELSE
							IF EMPTY(TRIM(cSize))
								STORE TRIM(outdet.ID) TO cSize
							ENDIF
						ENDIF

						cStyle = ""
						IF "ORIG_STYLE*"$outdet.printstuff
							valreturn("orig_style")
							STORE cOrig_style TO cStyle
						ELSE
							IF EMPTY(cStyle)
								cStyle = TRIM(outdet.STYLE)
							ENDIF
						ENDIF

						cUPC = TRIM(outdet.upc)

						valreturn("PRINTSTYLE")

						nDetQty = IIF(nAcctNum = 4694,1,vnanjingpp.totqty)
						nOrigQty = IIF(nAcctNum = 4694,1,outdet.origqty)

						IF lTestingMBOL AND EMPTY(outdet.ctnwt)
							cCtnWt = "2"
							nTotCtnWt = nTotCtnWt + 2
						ELSE
							STORE ALLTRIM(STR(outdet.ctnwt)) TO cCtnWt
							nTotCtnWt = nTotCtnWt + outdet.ctnwt
						ENDIF

						IF EMPTY(cCtnWt) OR outdet.ctnwt=0
							nCtnWt = outship.weight/outship.ctnqty
							nTotCtnWt = nTotCtnWt + nCtnWt
							cCtnWt = ALLTRIM(STR(nCtnWt))
							IF EMPTY(cCtnWt)
								cErrMsg = "MISS CTNWT "+TRANSFORM(vnanjingpp.outdetid)
								DO ediupdate WITH cErrMsg,.T.
								=FCLOSE(nFilenum)
								ERASE &cFilenameHold
								THROW
							ENDIF
						ENDIF

						valreturn("UNITSTYPE")
						valreturn("W0104")
						valreturn("W0106")
						valreturn("W0115")
						cItemNum = outdet.custsku

						IF EMPTY(cUnitstype)
							cUnitstype = "EA"
						ENDIF

						IF EMPTY(cW0104)
							cW0104 = "IN"
						ENDIF

						IF EMPTY(cW0106)
							cW0106 = "UP"
						ENDIF

						IF EMPTY(cW0115)
							cW0115 = "VN"
						ENDIF

						IF nOrigQty=nDetQty
							nShipType = "CL"
						ELSE
							nShipType = "PR"
						ENDIF

						IF nDetQty>0
							nUnitSum = nUnitSum + nDetQty
							nDiff = nOrigQty-nDetQty
							IF nDiff < 0
								=FCLOSE(nFilenum)
								ERASE &cFilenameHold
								THROW
							ENDIF

							STORE "W12"+cfd+nShipType+cfd+ALLTRIM(STR(nOrigQty))+cfd+ALLTRIM(STR(nDetQty))+cfd+;
								ALLTRIM(STR(nDiff))+cfd+"EA"+cfd+cUPC+cfd+cW0104+cfd+TRIM(cItemNum)+;
								cfd+cfd+cCtnWt+cfd+cWeightUnit+REPLICATE(cfd,6)+cW0106+cfd+cUPC+;
								REPLICATE(cfd,3)+cW0115+cfd+cStyle+csegd TO cString  && Eaches/Style
							DO cstringbreak
							nSegCtr = nSegCtr + 1

							IF !EMPTY(cColor)
								STORE "N9"+cfd+"VCL"+cfd+cColor+csegd TO cString
								DO cstringbreak
								nSegCtr = nSegCtr + 1
							ENDIF

							IF nAcctNum = 4610
								IF !EMPTY(cSize)
									STORE "N9"+cfd+"VSZ"+cfd+cSize+csegd TO cString
									DO cstringbreak
									nSegCtr = nSegCtr + 1
								ENDIF
							ENDIF
						ENDIF
						SKIP 1 IN vnanjingpp
					ENDDO
					lSkipBack = .T.
					nCtnNumber = nCtnNumber + 1
				ENDDO

*************************************************************************
*2^	END DETAIL MATCHING SHIP_REF LOOP
*************************************************************************
				WAIT CLEAR
				WAIT WINDOW "Now creating Section Closure information" NOWAIT
				STORE "W03"+cfd+ALLTRIM(STR(nUnitSum))+cfd+ALLTRIM(STR(nTotCtnWt))+cfd+;
					cWeightUnit+csegd TO cString   && Units sum, Weight sum, carton count

				nTotCtnWt = 0
				nTotCtnCount = 0
				nUnitSum = 0
				FPUTS(nFilenum,cString)

				STORE  "SE"+cfd+ALLTRIM(STR(nSegCtr+2))+cfd+PADL(c_GrpCntrlNum,9,"0")+csegd TO cString
				FPUTS(nFilenum,cString)

				SELECT outship
			ENDSCAN
			WAIT CLEAR
			SELECT bldet
		ENDSCAN
		WAIT "" TIMEOUT 1
		ASSERT .F. MESSAGE "At end of MAIN BOL loop: Sub-BOL "+cBOL

*************************************************************************
*1^	END OUTSHIP MAIN LOOP
*************************************************************************
		close945()

		SELECT qtycompare
		LOCATE
*		ASSERT .F. MESSAGE "At Qty Comparison"
		IF !EOF()
			SUM qtycompare.osqty TO sum_os
			SUM qtycompare.ppqty TO sum_pp
			IF sum_pp <> sum_os
				LOCATE
				BROWSE FOR ppqty<>osqty TIMEOUT 60
				SET STEP ON
				lMismatch = .T.
				lEmail = .F.
				lNanFilesOut = .F.
				countmail()
				cErrMsg = "COUNTS OFF SBOL:"+cBOL
				DO ediupdate WITH cErrMsg,.T.
				ERASE &cFilenameHold
				lDoCatch = .F.
				THROW
			ENDIF
		ENDIF

		IF !USED("ftpedilog")
			SELECT 0
			USE F:\edirouting\ftpedilog ALIAS ftpedilog
		ENDIF
		SELECT ftpedilog
		INSERT INTO ftpedilog (TRANSFER,ftpdate,filename,acct_name,TYPE) VALUES ("945-"+cCustname+"-"+cCustLoc,dt2,cFilenameShort,UPPER(cCustname),"945")
		USE IN ftpedilog

		=FCLOSE(nFilenum)

*!* Transfers files to correct output folders
		IF lNanFilesOut AND !lTestingMBOL
			SELECT temp945
			COPY TO "f:\3pl\data\temp945nan.dbf"
			USE IN temp945
			SELECT 0
			USE "f:\3pl\data\temp945nan.dbf" ALIAS temp945nan
			SELECT 0
			USE "f:\3pl\data\pts_sent945.dbf" ALIAS pts_sent945
			APPEND FROM "f:\3pl\data\temp945nan.dbf"
			USE IN pts_sent945
			USE IN temp945nan
			DELETE FILE "f:\3pl\data\temp945nan.dbf"
		ENDIF


		IF !lTesting
			asn_out_data()
		ENDIF

		cWO_NumList = ""

*!* Create eMail confirmation message
		IF lTestmail
			tsendto = tsendtotest
			tcc = tcctest
		ENDIF
		cPTCount = ALLTRIM(STR(nPTCount))
		nPTCount = 0
		cTotSubs = ALLTRIM(STR(nTotSubs))
		cSubCnt = ALLTRIM(STR(nSubCnt))
		tsubject = cPPName+" 945 DSDC EDI File from TGF as of "+dtmail+" ("+cCustLoc+")"
		tattach = " "
		tmessage = "Filename: "+TRIM(cFilenameShort)+CHR(13)
		tmessage = tmessage + "Office: "+cOffice+CHR(13)
		tmessage = tmessage + "945 EDI Info from TGF, for MASTER BOL# "+TRIM(cMBOL)+CHR(13)
		tmessage = tmessage+ "Sub-BOL "+cSubCnt+" of "+cTotSubs+": "+TRIM(cBOL)+CHR(13)
		tmessage = tmessage + "(Within our WO: "+cWO_NumStr+"), "+CHR(13)
		tmessage = tmessage + "containing these "+cPTCount+" picktickets:"+CHR(13)
		tmessage = tmessage + cPTString + CHR(13)
		tmessage = tmessage +"has been created and will be transmitted ASAP."+CHR(13)+CHR(13)
		IF !EMPTY(cMissDel)
			tmessage = tmessage+CHR(13)+CHR(13)+cMissDel+CHR(13)+CHR(13)
		ENDIF
		tmessage = tmessage + "If you have any questions, please eMail or call 732-750-9000 x217."
		tmessage = tmessage+CHR(13)+"Run from prog: "+cProgName

		IF lEmail
			DO FORM m:\dev\frm\dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
		ENDIF

		WAIT CLEAR
		WAIT WINDOW cPPName+" 945 EDI File output complete for"+CHR(13)+"Sub-BOL: "+cBOL NOWAIT
		cPTString = ""

	ENDSCAN

	lNanFilesOut = .T.
	WAIT CLEAR
*COPY FILE (cMBOLFolder+"*.*") TO (cArchiveFolder+"*.*")
	IF lNanFilesOut
		CD &cMBOLFolder
		TRY
			COPY FILE (cMBOLFolder+"*.*") TO (cOutFolder+"*.*")
		CATCH TO oERR
			IF oERR.ERRORNO =  1102  && Can't create file
				DELETE FILE [&cFilenameStage]
				tsendto = tsendtoerr
				tcc = tccerr
				tsubject = "945 File Copy Error at "+TTOC(DATETIME())
				tattach  = ""
				tfrom = "Toll EDI Operations <toll-edi-ops@tollgroup.com>"
				tmessage = ALLTRIM(oERR.MESSAGE)
				DO FORM m:\dev\frm\dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
				THROW
			ENDIF
		ENDTRY
		DELETE FILE (cMBOLFolder+"*.*")
	ENDIF

	IF !lTestingMBOL AND !lTestInput
		SELECT edi_trigger
		DO ediupdate WITH "945 CREATED",.F.
		SELECT edi_trigger
		LOCATE
	ENDIF

	RELEASE ALL LIKE c_CntrlNum,c_GrpCntrlNum
	WAIT CLEAR

	WAIT WINDOW "All Sub-BOLs for MBOL "+cMBOL+" have been completed...exiting" TIMEOUT 1
	IF !lTestingMBOL
		SELECT edi_trigger
		IF lMismatch
			DO ediupdate WITH "945 COUNT OFF",.T.
		ELSE
			DO ediupdate WITH "945 CREATED",.F.
		ENDIF
		SELECT edi_trigger
		LOCATE
	ENDIF

CATCH TO oERR
	IF lDoCatch
		ASSERT .F. MESSAGE "In Error Catch"
		IF EMPTY(cErrMsg)
			SET STEP ON
			cErrorNo = IIF(EMPTY(oERR.ERRORNO),"999",TRANSFORM(oERR.ERRORNO))
			lEmail = .F.
			DO ediupdate WITH ALLTRIM(oERR.MESSAGE),.T.
			tsubject = cCustname+" Error ("+cErrorNo+") at "+TTOC(DATETIME())
			tattach  = ""
			tsendto  = IIF(lTestingMBOL,tsendtotest,tsendtoerr)
			tcc = IIF(lTestingMBOL,tcctest,tccerr)

			tmessage = cCustname+" Error processing "+CHR(13)

			tmessage =tmessage+ "Try/CATCH Exeception Message:"+CHR(13)+;
				[  Error: ] + cErrorNo +CHR(13)+;
				[  LineNo: ] + STR(oERR.LINENO) +CHR(13)+;
				[  Message: ] + oERR.MESSAGE +CHR(13)+;
				[  Procedure: ] + oERR.PROCEDURE +CHR(13)+;
				[  Details: ] + oERR.DETAILS +CHR(13)+;
				[  StackLevel: ] + STR(oERR.STACKLEVEL) +CHR(13)+;
				[  LineContents: ] + oERR.LINECONTENTS+CHR(13)+;
				[  UserValue: ] + oERR.USERVALUE

			tsubject = "945 EDI Poller Error at "+TTOC(DATETIME())
			tattach  = ""
			tfrom = "Toll EDI Operations <toll-edi-ops@tollgroup.com>"
			DO FORM m:\dev\frm\dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
		ENDIF
	ENDIF
FINALLY
	ON ESCAPE
	RELEASE lCloseOutput
*	RELEASE ALL LIKE t*
ENDTRY


*** END OF CODE BODY

****************************
PROCEDURE close945
****************************
** Footer Creation

	STORE  "GE"+cfd+ALLTRIM(STR(nSTCount))+cfd+c_CntrlNum+csegd TO cString
	FPUTS(nFilenum,cString)

	STORE  "IEA"+cfd+"1"+cfd+PADL(c_CntrlNum,9,"0")+csegd TO cString
	FPUTS(nFilenum,cString)

	nSTCount = 0
	RETURN

****************************
PROCEDURE num_incr_isa
****************************

	IF !USED("serfile")
		USE ("f:\3pl\data\serial\"+cCustname+"945_serial") IN 0 ALIAS serfile
	ENDIF
	SELECT serfile
	IF lTestingMBOL AND lISAFlag
		nOrigSeq = serfile.seqnum
		lISAFlag = .F.
	ENDIF
	nISA_Num = serfile.seqnum
	c_CntrlNum = ALLTRIM(STR(serfile.seqnum))
	REPLACE serfile.seqnum WITH serfile.seqnum + 1 IN serfile
	SELECT outship
	RETURN

****************************
PROCEDURE num_incr_st
****************************

	IF !USED("serfile")
		USE ("f:\3pl\data\serial\"+cCustname+"945_serial") IN 0 ALIAS serfile
	ENDIF
	SELECT serfile
	IF lTestingMBOL AND lSTFlag
		nOrigGrpSeq = serfile.grpseqnum
		lSTFlag = .F.
	ENDIF
	c_GrpCntrlNum = ALLTRIM(STR(serfile.grpseqnum))
	REPLACE serfile.grpseqnum WITH serfile.grpseqnum + 1 IN serfile
	SELECT outship
	RETURN


****************************
PROCEDURE segmentget
****************************

	PARAMETER thisarray,lcKey,nLength

	FOR i = 1 TO nLength
		IF i > nLength
			EXIT
		ENDIF
		lnEnd= AT("*",thisarray[i])
		IF lnEnd > 0
			lcThisKey =TRIM(SUBSTR(thisarray[i],1,lnEnd-1))
			IF OCCURS(lcKey,lcThisKey)>0
				RETURN SUBSTR(thisarray[i],lnEnd+1)
				i = 1
			ENDIF
		ENDIF
	ENDFOR

	RETURN ""

****************************
PROCEDURE ediupdate
****************************
	PARAMETER cStatus,lIsError
	lDoCatch = .F.

	IF !lTestingMBOL
		SELECT edi_trigger
		LOCATE
		IF EMPTY(cStatus)
			cStatus = "945 Created"
		ENDIF

		IF !lIsError
			IF lPTFlag
				REPLACE processed WITH .T.,proc945 WITH .T.,file945 WITH cFilenameHold,;
					fin_status WITH cStatus,errorflag WITH .F.,when_proc WITH DATETIME() ;
					FOR edi_trigger.ship_ref = cShip_ref AND accountid = nAcctNum
			ELSE
				REPLACE processed WITH .T.,proc945 WITH .T.,file945 WITH cFilenameHold,;
					fin_status WITH cStatus,errorflag WITH .F.,when_proc WITH DATETIME() ;
					FOR edi_trigger.bol = cMBOL && AND accountid = nAcctNum
			ENDIF
		ELSE
			IF lPTFlag
				REPLACE processed WITH .T.,proc945 WITH .F.,file945 WITH "",;
					fin_status WITH cStatus,errorflag WITH .T. ;
					FOR edi_trigger.ship_ref = cShip_ref AND accountid = nAcctNum
			ELSE
				REPLACE processed WITH .T.,proc945 WITH .F.,file945 WITH "",;
					fin_status WITH cStatus,errorflag WITH .T. ;
					FOR edi_trigger.bol = cMBOL && AND accountid = nAcctNum
			ENDIF
			IF lCloseOutput
				=FCLOSE(nFilenum)
				IF !lMismatch
					ERASE &cFilenameHold
				ENDIF
			ENDIF
		ENDIF
	ENDIF

	IF lIsError AND lEmail
		tsubject = "945 Error in NANJING/EA MBOL "+TRIM(cMBOL)+" (At PT "+cShip_ref+")"
		tattach = " "
		tsendto = tsendtoerr
		tcc = tccerr
		tmessage = "Generated from program: nanjing945pp-dsdc_create_mbol, within EDI Outbound Poller"
		tmessage = tmessage + CHR(13) + "945 Processing for MBOL# "+TRIM(cMBOL)
		tmessage = tmessage + CHR(13) + "(Sub-BOL# "+TRIM(cBOL)+", WO# "+cWO_Num+"), produced this error: (Office: "+cOffice+"): "+cStatus+CHR(13)+"Check EDI_TRIGGER and re-run"

		IF cStatus = "MISS PT"
			tmessage = tmessage + CHR(13) + "This error means that this PT exists in OUTSHIP, but not in the Pickpack table. Please add this WO to SQL."
		ENDIF

		IF TIME(DATE())>"20:00:00" OR INLIST(DOW(DATE()),1,7)
*			tcc = tcc+",jsb1956@yahoo.com"
		ENDIF
		DO FORM m:\dev\frm\dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
	ENDIF

	IF USED('outship')
		USE IN outship
	ENDIF
	IF USED('outdet')
		USE IN outdet
	ENDIF

	IF USED('serfile')
		SELECT serfile
		IF lTestingMBOL
			REPLACE serfile.seqnum WITH nOrigSeq
			REPLACE serfile.grpseqnum WITH nOrigGrpSeq
		ENDIF
		USE IN serfile
	ENDIF

	IF USED('scacs')
		USE IN scacs
	ENDIF

	IF USED('mm')
		USE IN mm
	ENDIF

	IF !lTestingMBOL
		SELECT edi_trigger
		LOCATE
	ENDIF

ENDPROC

****************************
PROCEDURE cstringbreak
****************************

	cLen = LEN(ALLTRIM(cString))
	IF cLen=0
		SET STEP ON
	ENDIF
	FPUTS(nFilenum,cString)
ENDPROC

****************************
PROCEDURE valreturn
****************************
	PARAMETERS cIdentifier
	cReturned = "c"+cIdentifier
	RELEASE ALL LIKE &cReturned
	PUBLIC &cReturned
	nGetline = ATCLINE(cIdentifier,outdet.printstuff)
	dataline = MLINE(outdet.printstuff,nGetline)
	STORE SUBSTR(dataline,AT("*",dataline)+1) TO &cReturned
	RETURN &cReturned
ENDPROC

****************************
PROCEDURE countmail
****************************
	SELECT qtycompare
	LOCATE
	ASSERT .F. MESSAGE "At ScanPack report error"
	tmessage = "SCAN PACK REPORT NOT RUN! [IN BOL# "+cBOL+"]"
	cString = PADR("WO NUM",10)+PADR("PICKTICKET",15)+PADR("O/S QTY",10)+PADR("P/P QTY",10)+PADR("QTY DIFF",10)+PADR("PCT DIFF",10)
	tmessage = tmessage + CHR(13) + cString
	IF !EOF()
		SCAN
			cWO_Num1 = PADR(ALLTRIM(STR(qtycompare.wo_num)),10)
			cShip_ref = PADR(qtycompare.ship_ref,15)
			cOSQty =  PADR(ALLTRIM(STR(qtycompare.osqty)),10)
			cPPqty = PADR(ALLTRIM(STR(qtycompare.ppqty)),10)
			cQtyDiff = PADR(ALLTRIM(STR(qtycompare.ppqty-qtycompare.osqty)),10)
			cPctDiff = PADR(ALLTRIM(STR(qtycompare.pctdiff)),10)
			IF cOSQty<>cPPqty
				tmessage = tmessage + CHR(13) +cWO_Num1 + cShip_ref + cOSQty + cPPqty + cQtyDiff + cPctDiff
			ENDIF
		ENDSCAN

		tsubject = "945 Count for NANJING/EA DSDC BOL "+TRIM(cBOL)
		tattach = " "
		tsendto = tsendtoerr
		tcc = tccerr
		IF TIME(DATE())>"20:00:00" OR INLIST(DOW(DATE()),1,7)
*			tcc = tcc+",jsb1956@yahoo.com"
		ENDIF

		DO FORM m:\dev\frm\dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
	ENDIF
ENDPROC

*********************
PROCEDURE headerfill
*********************
*!* HEADER LEVEL EDI DATA
	cISA_Num = PADL(c_CntrlNum,9,"0")
	nISA_Num = INT(VAL(cISA_Num))

	STORE "ISA"+cfd+"00"+cfd+"          "+cfd+"00"+cfd+"          "+cfd+csendqual+cfd+csendidlong+cfd+;
		crecqual+cfd+crecidlong+cfd+ctruncdate+cfd+ctime+cfd+"U"+cfd+"00401"+cfd+;
		cISA_Num+cfd+"0"+cfd+"P"+cfd+cterminator+csegd TO cString
	DO cstringbreak

	STORE "GS"+cfd+"SW"+cfd+csendid+cfd+crecid+cfd+cdate+cfd+ctime+cfd+c_CntrlNum+;
		cfd+"X"+cfd+cX12+csegd TO cString
	DO cstringbreak

ENDPROC

****************************
PROCEDURE totqtyerr
****************************
	SET STEP ON
	tsubject = "Cartons/Units Discrepancy, Outbound vs. Cartons Data"
	tmessage = "The following PT(s) had unmatched counts between the outbound ('O') and cartonization ('C') data:"+CHR(13)+CHR(13)
	tmessage = tmessage+PADR("PT#",20)+PADR("O.Ctns",10)+PADR("C.Ctns",10)+PADR("O.Units",10)+PADR("C.Units",10)+CHR(13)
	SELECT tempqtyerr
	SCAN
		cSRUse = ALLTRIM(tempqtyerr.ship_ref)
		cnctnqty = STR(tempqtyerr.ctnqty)
		cncqty = STR(tempqtyerr.cqty)
		cntotqty = STR(tempqtyerr.totqty)
		cnuqty = STR(tempqtyerr.uqty)
		DO CASE
			CASE (ctnqty # cqty) AND (totqty # uqty)
				cErrorLine = "Both Carton and Unit totals don't match"
			CASE (ctnqty # cqty)
				cErrorLine = "Only Carton totals don't match"
			CASE (totqty # uqty)
				cErrorLine = "Only Unit totals don't match"
		ENDCASE
		tmessage = tmessage+PADR(cSRUse,20)+PADR(cnctnqty,10)+PADR(cncqty,10)+PADR(cntotqty,10)+PADR(cnuqty,10)+" "+cErrorLine+CHR(13)
	ENDSCAN

	tmessage = tmessage+CHR(13)+"Please contact Joe IMMEDIATELY to confirm which numbers are correct."
	tsendto = IIF(lTesting,tsendtotest,tsendtoqty)
	tcc = IIF(lTesting,tcctest,tccqty)
	tattach = ""
	IF !lTesting
		DO FORM m:\dev\frm\dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
	ELSE
		SET STEP ON
	ENDIF
	USE IN tempqtyerr
ENDPROC
