* upcoming CPI increases

utilsetup("CPIINCREASE")

xsqlexec("select * from salesman","salesman",,"qq")
index on salesmanid tag salesmanid
index on salesman tag salesman
set order to

xsqlexec("select * from ratehead",,,"ar")

select acctname, padr(salesman(salesmanid),30) as salesman, iif(!emptynul(nextcpi),"Next CPI","Contract End") as type, ;
	iif(!emptynul(nextcpi),nextcpi,contractend) as zdate ;
	from ratehead ;
	where (!emptynul(nextcpi) or !emptynul(contractend)) and !inactive ;
	order by 4, 1 ;
	into cursor xrpt

xheader="Upcoming CPI Increases - Active Accounts"

rpt2pdf("contractdue","f:\auto\upcomingcpiincrease.pdf")

select acctname, padr(salesman(salesmanid),30) as salesman, iif(!emptynul(nextcpi),"Next CPI","Contract End") as type, ;
	iif(!emptynul(nextcpi),nextcpi,contractend) as zdate ;
	from ratehead ;
	where (!emptynul(nextcpi) or !emptynul(contractend)) and inactive ;
	order by 4, 1 ;
	into cursor xrpt

xheader="Upcoming CPI Increases - Inactive Accounts"

rpt2pdf("contractdue","f:\auto\upcomingcpiincrease-inactive.pdf")

set step on 
tsendto="steven.sykes@tollgroup.com, joe.desaye@tollgroup.com, anthony.alfieri@tollgroup.com, Brian.Freiberger@tollgroup.com, jon.bonny@tollgroup.com"
tattach = "f:\auto\upcomingcpiincrease.pdf,f:\auto\upcomingcpiincrease-inactive.pdf"
tfrom ="Autoproc <support@fmiint.com>"
tmessage = "see attached files"
tsubject = "Upcoming CPI Increases"
do form dartmail2 with tsendto,tfrom,tsubject," ",tattach,tmessage,"A"

*

schedupdate()

_screen.caption=gscreencaption
on error
