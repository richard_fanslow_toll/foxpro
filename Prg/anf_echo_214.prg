*******************************************************************************
* This is a process that takes 214s as received from JBHunt, strips off the
* ISA and GS enevelope information, adds in the TOLL header data and send it off
* to Amber Roar\ANF
* runs as a folder checker process.
* started 1/29/2016 PG
*******************************************************************************
Public c_CntrlNum

close data all 

On Error Do fmerror With Error(), Program(), Lineno(1), Sys(16),, .T.
do _setvars
  
lTesting = .F.
lOverridebusy = .f.

Select 0
Use F:\edirouting\FTPSETUP Shared
Locate For FTPSETUP.TRANSFER = "ANF-214-ECHO"
If Found()
  If chkbusy And !lOverridebusy
    Wait Window At 10,10 "ANF 214 ECHO PROCESS IS NOW BUSY..exiting" Timeout 2
    NormalExit = .T.
   return
  Endif
Endif
Replace chkbusy With .T. For FTPSETUP.TRANSFER = "ANF-214-ECHO"
*Use In FTPSETUP


cST_Name = "TGF INC."
cfd = "*"  && Field delimiter
csegd = Chr(0x0a) &&""  && Segment delimiter
cterminator = ">"  && Used at end of ISA segment

csendqual = "ZZ"  && Sender qualifier
csendid = "TGFSCS"  && Sender ID code
crecqual = "ZZ"  && Recip qualifier, was "08"
crecid = "ANFTLP"   && Recip ID Code, was "6113310214"

cDate = Dtos(Date())
cTruncDate = Right(cDate,6)
cTruncTime = Substr(Ttoc(Datetime(),1),9,4)

cETADate = Dtos(Date()+3)

cfiledate = cDate+cTruncTime
csendidlong = Padr(csendid,15," ")
crecidlong = Padr(crecid,15," ")



If lTesting
  cISACode = "T"
Else
  cISACode = "P"
Endif

nSTCount = 0
nSegCtr = 0
nLXNum = 1
Store "" To cEquipNum,cHAWB
dt1 = Ttoc(Datetime(),1)
dtmail = Ttoc(Datetime())
dt2 = Datetime()


lnNum = Adir(tarray,"f:\ftpusers\anf\jbhunt\*.*")
lcPath = "f:\ftpusers\anf\jbhunt\"
lcArchivePath ="f:\ftpusers\anf\jbhunt\archive\"

If lnNum=0
  Wait Window At 10,10 "No JBHubt\Anf 214s to echo\process now........... " Timeout 1
  Return
Endif
If lnNum >= 1
  For thisfile = 1  To lnNum
    xfile = lcPath+tarray[thisfile,1]  &&+"."
    archivefile = lcArchivePath+tarray[thisfile,1]  &&+"."
    Wait Window "Importing file: "+xfile Nowait

    Do BuildEcho214 With xfile

    Copy File &xfile To &archivefile
    If File(archivefile)
      Delete File &xfile
    Endif
  Next thisfile
Else
  Wait Window At 10,10  "          No 214s to Echo          " Timeout 1
Endif

If lnNum > 0
  Insert Into F:\EDIROUTING\sftpjobs (jobname,userid,jobtime) Values ("ANF_PUT_TO_AMBERROAD","ANF214ECHO",Datetime())
Endif 
************************************************************************************************************

Procedure BuildEcho214
Parameters xfile

Do num_incr_isa
cISA_Num = Padl(c_CntrlNum,9,"0")
Store "ISA"+cfd+"00"+cfd+"          "+cfd+"00"+cfd+"          "+cfd+csendqual+cfd+csendidlong+cfd+;
crecqual+cfd+crecidlong+cfd+cTruncDate+cfd+cTruncTime+cfd+"U"+cfd+"00401"+cfd+;
cISA_Num+cfd+"0"+cfd+cISACode+cfd+cterminator+csegd To cString


*xfile_in=Getfile()
lcIn = Filetostr(xfile)
lcIn = Strtran(lcIn,"~",csegd)

lcNewHdr = cString
lcNewFooter = "IEA*1*"+cISA_Num
lcIn = Substr(lcIN,107)

lcIN= Strtran(lcIn,"HJBT*TGFSCSP","TGFSCS*ANFTLP")

lcIN= lcNewHdr+lcIN

lnStart= Atc("IEA",lcIN)
lnLen =Len(lcIN)

lnIEA = lnLen-lnStart

lcIn = Substr(lcIN,1,lnLen-(lnIEA+1))
lcIn = lcIN + lcNewFooter+CHR(10)  && Added line feed per A&F, 02.05.2016, Joe

dt1 = Ttoc(Datetime(),1)
cFilename = ("f:\ftpusers\ANF\JBHuntOut\anf_JBHUNT_214_"+dt1+".edi")
Strtofile(lcIN,cFilename)

cFilename = ("f:\ftpusers\ANF\OUT\anf_JBHUNT_214_"+dt1+".edi")
Strtofile(lcIN,cFilename)


Wait Window At 10,10 "File: "+cFilename+" Echoed 214 from JBHunt sent to AnF" Timeout 2

Select ftpsetup
Locate for ftpsetup.transfer = "ANF-214-ECHO"
Replace chkbusy With .F. In ftpsetup

Endproc


****************************
Procedure num_incr_isa
****************************

If !Used("serfile")
  Use ("F:\3pl\data\serial\anf_serial") In 0 Alias serfile
Endif
Select serfile
If lTesting
  nOrigSeq = serfile.seqnum
  nOrigGrpSeq = serfile.grpseqnum
  lISAFlag = .F.
Endif
nISA_Num = serfile.seqnum
c_CntrlNum = Alltrim(Str(serfile.seqnum))
Replace serfile.seqnum With serfile.seqnum + 1 In serfile
Endproc
