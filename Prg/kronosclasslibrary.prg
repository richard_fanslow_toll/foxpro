* kronosclasslibrary.prg
* 
* author: Mark Bennett
*
* This class is instantiated by several Kronos time reporting programs to retrieve Temp agency specific info
*
* Modified 1/3/2018 to change agency codes for U & V to transition from SSG to Simplified Labor SS. Also added G for Chartwell NJ.
*
* Modified 1/30/2018 to add agency code F to support transition from Cor-Tech to Chartwell - Louisville.
*
* Modified 3/21/2018 MB to add support for associating an Excel template file with each agency for the new report needed by Adelene Teo.
*
* Modified Shift Calculation for Mira Loma 4/13/2018 MB

DEFINE CLASS TempAgency AS CUSTOM

	* temp co key props
	cTempCo = ""
	cTempCoName = ""

	* hours discrepancy testing props
	nMinHoursTest = 2
	nMaxHoursTest = 10

	* WAGE discrepancy testing props
	nMinWageTest = 8.00
	nMaxWageTest = 20.00

	* other properties
	lCheckBio = .T.
	cWeeklyTemplate = ''

	nPeriodEndDayNum = 1   && if 7, weekly pay period ends on a Saturday (Sunday - Saturday), if 1, it ends on a Sunday (Monday - Sunday)

	*lRoundWageAtEachStep = .T.  && .F. would only apply rounding after all wage*markups have been accumulated; If .T. it rounds to nearest penny at each step before accumulating (which seems to be the way TriState is doing it).
	nRoundingType = 1
	* 1 = standard rounding - round effective wage to nearest penny after multiplying the agency markup times base wage; also round daily $ to nearest penny
	* 2 = no rounding
	* 3 = SELECT ROUNDING = round product of base wage and agency markup and ot multiplier to nearest cent


	FUNCTION INIT
		LPARAMETERS tcTempCo
		IF NOT DODEFAULT()
			RETURN .F.
		ENDIF
		WITH THIS
			.nRoundingType = 1  && standard rounding
			DO CASE
				CASE tcTempCo == 'A'
					.cTempCo = tcTempCo
					.cTempCoName = 'Tri-State SP'
					.lCheckBio = .T.
					.nMinHoursTest = 2
					.nMaxHoursTest = 10
					.nPeriodEndDayNum = 1
				CASE tcTempCo == 'B'
					.cTempCo = tcTempCo
					.cTempCoName = 'Select Staffing - ML'
					.lCheckBio = .T.
					.nMinHoursTest = 2
					.nMaxHoursTest = 10
					.nPeriodEndDayNum = 1
				CASE tcTempCo == 'D'
					.cTempCo = tcTempCo
					.cTempCoName = 'Quality Temps - NJ'
					.lCheckBio = .T.
					.nMinHoursTest = 2
					.nMaxHoursTest = 10
					.nPeriodEndDayNum = 1
					.cWeeklyTemplate = 'Weekly Template-CARTERET'
				CASE tcTempCo == 'F'
					.cTempCo = tcTempCo
					.cTempCoName = 'Chartwell - Louisville'
					.lCheckBio = .T.
					.nMinHoursTest = 2
					.nMaxHoursTest = 10
					.nPeriodEndDayNum = 7
				CASE tcTempCo == 'G'
					.cTempCo = tcTempCo
					.cTempCoName = 'Chartwell SS - NJ'
					.lCheckBio = .T.
					.nMinHoursTest = 2
					.nMaxHoursTest = 10
					.nPeriodEndDayNum = 1
					.cWeeklyTemplate = 'Weekly Template-CARTERET'
				CASE tcTempCo == 'J'
					.cTempCo = tcTempCo
					.cTempCoName = 'Diamond Staffing NJ'
					.lCheckBio = .T.
					.nMinHoursTest = 2
					.nMaxHoursTest = 10
					.nPeriodEndDayNum = 1
				CASE tcTempCo == 'K'
					.cTempCo = tcTempCo
					.cTempCoName = 'Tri-State ML'
					.lCheckBio = .T.
					.nMinHoursTest = 2
					.nMaxHoursTest = 10
					.nPeriodEndDayNum = 1
				CASE tcTempCo == 'Q'
					.cTempCo = tcTempCo
					.cTempCoName = 'Cor-Tech - Louisville'
					.lCheckBio = .T.
					.nMinHoursTest = 2
					.nMaxHoursTest = 10
					.nPeriodEndDayNum = 7
				CASE tcTempCo == 'R'
					.cTempCo = tcTempCo
					.cTempCoName = 'Staffing Solutions - SP'
					.lCheckBio = .T.
					.nMinHoursTest = 2
					.nMaxHoursTest = 10
					.nPeriodEndDayNum = 1
				CASE tcTempCo == 'S'
					.cTempCo = tcTempCo
					.cTempCoName = 'Kamran Staffing - SP'
					.lCheckBio = .T.
					.nMinHoursTest = 2
					.nMaxHoursTest = 10
					.nPeriodEndDayNum = 1
				CASE tcTempCo == 'T'
					.cTempCo = tcTempCo
					.cTempCoName = 'Staffing Solutions - ML'
					.lCheckBio = .T.
					.nMinHoursTest = 2
					.nMaxHoursTest = 10
					.nPeriodEndDayNum = 1
				CASE tcTempCo == 'U'
					.cTempCo = tcTempCo
					.cTempCoName = 'Simplified Labor SS - Carson'  
					.lCheckBio = .T.
					.nMinHoursTest = 2
					.nMaxHoursTest = 10
					.nPeriodEndDayNum = 1
				CASE tcTempCo == 'V'
					.cTempCo = tcTempCo
					.cTempCoName = 'Simplified Labor SS - Carson 2' 
					.lCheckBio = .T.
					.nMinHoursTest = 2
					.nMaxHoursTest = 10
					.nPeriodEndDayNum = 1
					.cWeeklyTemplate = 'Weekly Template-CARSON2'
				CASE tcTempCo == 'W'
					.cTempCo = tcTempCo
					.cTempCoName = 'Allied Security - Louisville'
					.lCheckBio = .T.
					.nMinHoursTest = 2
					.nMaxHoursTest = 10
					.nPeriodEndDayNum = 7
				CASE tcTempCo == 'X'
					.cTempCo = tcTempCo
					.cTempCoName = 'Chartwell SS - SP'
					.lCheckBio = .T.
					.nMinHoursTest = 2
					.nMaxHoursTest = 10
					.nPeriodEndDayNum = 1
					.cWeeklyTemplate = 'Weekly Template-SAN PEDRO'
				CASE tcTempCo == 'Y'
					.cTempCo = tcTempCo
					.cTempCoName = 'Simplified Labor SS - ML'
					.lCheckBio = .T.
					.nMinHoursTest = 2
					.nMaxHoursTest = 10
					.nPeriodEndDayNum = 1
					.cWeeklyTemplate = 'Weekly Template-MIRA LOMA'
				CASE tcTempCo == 'Z'
					.cTempCo = tcTempCo
					.cTempCoName = 'The Job Store - Louisville'
					.lCheckBio = .T.
					.nMinHoursTest = 2
					.nMaxHoursTest = 10
					.nPeriodEndDayNum = 7
				OTHERWISE
					*.cTempCoName = ''
					* Do not instantiate object with unexpected Temp Agency Code
					RETURN .F.
			ENDCASE
		ENDWITH
	ENDFUNC


	FUNCTION DESTROY
		DODEFAULT()
	ENDFUNC
	

	FUNCTION GetTempCoName
		RETURN THIS.cTempCoName	
	ENDFUNC


	FUNCTION GetCalculatedShift
		LPARAMETERS tInpunchDTM
		LOCAL lcShift, lcTimePortion
		lcTimePortion = RIGHT(TTOC(tInpunchDTM),8)
		WITH THIS
			DO CASE
				CASE INLIST(.cTempCo,'D','G','J')
					DO CASE
						CASE lcTimePortion < "16:23:00"
							lcShift = "1"
						OTHERWISE
							lcShift = "2"
					ENDCASE
				CASE INLIST(.cTempCo,'F','Q','W','Z')
					DO CASE
						CASE lcTimePortion < "16:23:00"
							lcShift = "1"
						OTHERWISE
							lcShift = "2"
					ENDCASE
				CASE .cTempCo = 'R'  &&  Staffing Solutions Group - SP
					* revised per Ted Makros 12/16/2016 MB =
					*!*	1st shift - 8:00am
					*!*	2nd shift - 4:30pm
					*!*	3rd shift - 12:00am
					
					*!*	DO CASE
					*!*		CASE lcTimePortion < "15:00:00"
					*!*			lcShift = "1"
					*!*		CASE lcTimePortion < "20:00:00"
					*!*			lcShift = "2"
					*!*		OTHERWISE
					*!*			lcShift = "3"
					*!*	ENDCASE
					
					DO CASE
						CASE (lcTimePortion >= "02:30:00") AND (lcTimePortion < "15:30:00")
							lcShift = "1"
						CASE (lcTimePortion >= "15:30:00") AND (lcTimePortion < "23:00:00")
							lcShift = "2"
						CASE (lcTimePortion >= "23:00:00") OR (lcTimePortion < "02:30:00")
							lcShift = "3"
						OTHERWISE
							lcShift = "?"
					ENDCASE
				CASE INLIST(.cTempCo,'T')  && Staffing Solutions Group - ML
					DO CASE
						* 06/14/2017, changed per new shift from Steve Sykes:
						*!*	We are back to 

						*!*	6:00am - 2:30 pm
						*!*	2:00pm - 10:30 pm
						*!*	10:00 pm - 6:30 am
						*!*	CASE (lcTimePortion >= "04:30:00") AND (lcTimePortion < "13:00:00")
						*!*		lcShift = "1"
						*!*	CASE (lcTimePortion >= "13:00:00") AND (lcTimePortion < "21:00:00")
						*!*		lcShift = "2"
						*!*	OTHERWISE
						*!*		lcShift = "3"
							
						* 7/18/2017 per Steve Sykes
						CASE (lcTimePortion >= "03:00:00") AND (lcTimePortion < "14:30:00")
							lcShift = "1"
						OTHERWISE
							lcShift = "2"
							
					ENDCASE
				CASE .cTempCo = 'U'  &&  Simplified Labor SS - Carson
					DO CASE
						CASE lcTimePortion < "15:00:00"
							lcShift = "1"
						CASE lcTimePortion < "20:00:00"
							lcShift = "2"
						OTHERWISE
							lcShift = "3"
					ENDCASE
				CASE .cTempCo = 'V'  &&  Simplified Labor SS - Carson 2
					DO CASE
						CASE lcTimePortion < "15:00:00"
							lcShift = "1"
						CASE lcTimePortion < "20:00:00"
							lcShift = "2"
						OTHERWISE
							lcShift = "3"
					ENDCASE
				CASE INLIST(.cTempCo,'X')  &&  Chartwell - SP
					DO CASE
						CASE (lcTimePortion >= "02:30:00") AND (lcTimePortion < "15:30:00")
							lcShift = "1"
						CASE (lcTimePortion >= "15:30:00") AND (lcTimePortion < "23:00:00")
							lcShift = "2"
						CASE (lcTimePortion >= "23:00:00") OR (lcTimePortion < "02:30:00")
							lcShift = "3"
						OTHERWISE
							lcShift = "?"
					ENDCASE
				CASE INLIST(.cTempCo,'Y')  && Simplified - ML
					DO CASE
						*!*	*!*	* 7/18/2017 per Steve Sykes
						*!*	CASE (lcTimePortion >= "03:00:00") AND (lcTimePortion < "14:30:00")
						*!*		lcShift = "1"
						*!*	OTHERWISE
						*!*		lcShift = "2"
						*!*	* 8/14/2017 per Steve Sykes
						*!*	1st   6 am - 2:30 pm
						*!*	2nd   6 pm - 2:30 AM
						*!*	CASE (lcTimePortion >= "03:00:00") AND (lcTimePortion < "14:30:00")
						*!*		lcShift = "1"
						*!*	OTHERWISE
						*!*		lcShift = "2"
						* 8/15/2017 per Steve Sykes / Brian Freiberger
						*!*	We are doing 2, 10 hr shifts starting today which will run for the balance of the week.
						*!*	6am -430 pm
						*!*	6pm - 430 am
						*!*	CASE (lcTimePortion >= "04:30:00") AND (lcTimePortion < "16:30:00")
						*!*		lcShift = "1"
						* Per Joana 12/27/2017: first shift 10 hours starting at 5:00 Am and ending at 3:30Pm.  Second shift is running from 4:00PM - 1:30AM
*!*							CASE (lcTimePortion >= "00:15:00") AND (lcTimePortion < "12:15:00")
*!*								lcShift = "1"
*!*							OTHERWISE
*!*								lcShift = "2"
*!*							* 8/10/2017 per Steve Sykes
*!*							*1st   6 am - 2:30 pm
*!*							*2nd  2:00 PM - 10:30 pm
*!*							*3rd  10 pm - 6:30 am (next day)
*!*							CASE (lcTimePortion >= "04:00:00") AND (lcTimePortion < "13:00:00")
*!*								lcShift = "1"
*!*							CASE (lcTimePortion >= "13:00:00") AND (lcTimePortion < "21:00:00")
*!*								lcShift = "2"
*!*							CASE (lcTimePortion >= "21:00:00") OR (lcTimePortion < "04:00:00")
*!*								lcShift = "3"
*!*							OTHERWISE
*!*								lcShift = "?"

						* 4/13/2018 Instructions begin ============================================================================================
						*!*	Steve,

						*!*	We are working as follows:

						*!*	BBB
						*!*	1st  5:00 AM   -1:30 PM (8 hour shift) 5:00 AM-3:30 PM (10 hour shift)
						*!*	2nd 6:00 PM-2:30 PM(8 hour shift) 4:00 PM - 2:30 AM (10 hour shift)

						*!*	South Building

						*!*	5:00- 3:30 PM - Depends on the account but these are the hours that someone is on site for the accounts in the South.
						*!*	We are currently running a small 2nd shift on the South side to finish up the BBG transfers.
						*!*	We will more than likely open up a second shift for Bio World but we have not determined shift hours yet.
						*!*	Joana
						* 
						* make it so any IN Punch between 3:30pm and midnight is 2nd shift, otherwise 1st shift. 4/13/2018 MB
						CASE (lcTimePortion >= "15:30:00") AND (lcTimePortion <= "23:59:59")
							lcShift = "2"
						OTHERWISE
							lcShift = "1"
						* 4/13/2018 Instructions end ============================================================================================

					ENDCASE
				OTHERWISE
					lcShift = "?"
			ENDCASE
		ENDWITH
		RETURN lcShift
	ENDFUNC


	FUNCTION getTempAgencyMarkupREG
		LPARAMETERS tcSHORTNAME, tdPAYDATE, tcFILE_NUM
		* note: this is for Reg Hours only; separate function for OT markup
		LOCAL lnMarkup
		DO CASE
			CASE INLIST(THIS.cTempCo,'D')  && QUALITY TEMPS - NJ
				DO CASE
					CASE BETWEEN(tdPAYDATE,{^2013-01-01},{^2099-12-31})
						* flat 31% with no variations per Ronald 3/12/15.
						lnMarkup = 1.310
				ENDCASE
				
				* 09/25/2017: special processing requested by Jim Killen to change markup for Quality On-Site Sue Ortiz to 1.18
				IF ALLTRIM(tcFILE_NUM) = '27247' THEN
					DO CASE
						CASE BETWEEN(tdPAYDATE,{^2017-09-01},{^2099-12-31})
							lnMarkup = 1.1800
						OTHERWISE
							* nothing
					ENDCASE
				ENDIF

			CASE INLIST(THIS.cTempCo,'F')  && CHARTWELL SS - KY
				DO CASE
					CASE BETWEEN(tdPAYDATE,{^2018-02-09},{^2099-12-31})
						lnMarkup = 1.310
				ENDCASE

			CASE INLIST(THIS.cTempCo,'G')  && CHARTWELL SS - NJ
				DO CASE
					CASE BETWEEN(tdPAYDATE,{^2018-01-01},{^2099-12-31})
						lnMarkup = 1.310
				ENDCASE
				
			CASE INLIST(THIS.cTempCo,'J')  && Diamond Staffing - NJ (same markup as TriState ML per Rich Pacheco)
				DO CASE
					CASE BETWEEN(tdPAYDATE,{^2013-10-27},{^2015-01-04})
						lnMarkup = 1.2650
					CASE BETWEEN(tdPAYDATE,{^2015-01-05},{^2099-12-31})
						lnMarkup = 1.275
				ENDCASE
			CASE INLIST(THIS.cTempCo,'Q')  && COR-TECH Louisville
				DO CASE
					CASE BETWEEN(tdPAYDATE,{^2015-03-08},{^2099-12-31})
						IF "~REC" $ UPPER(tcSHORTNAME) THEN
							* temp was recruited; gets higher rate - per Anthony Cox at cor-tech
							lnMarkup = 1.400
						ELSE
							* temp was transitioned from CRSCO; regular rate
							lnMarkup = 1.350
						ENDIF
				ENDCASE
			CASE THIS.cTempCo = 'R'   && Staffing Solutions Group - San Pedro
				DO CASE
					CASE BETWEEN(tdPAYDATE,{^2015-03-08},{^2015-12-31})
						lnMarkup = 1.300
					CASE BETWEEN(tdPAYDATE,{^2016-01-01},{^2099-12-31})
						lnMarkup = 1.310
				ENDCASE
			CASE THIS.cTempCo = 'T'   && Staffing Solutions Group - Mira Loma
				DO CASE
					CASE BETWEEN(tdPAYDATE,{^2015-03-16},{^2015-12-31})
						lnMarkup = 1.300
					CASE BETWEEN(tdPAYDATE,{^2016-01-01},{^2099-12-31})
						lnMarkup = 1.310
				ENDCASE
			CASE THIS.cTempCo = 'U'   && Simplified Labor SS - Carson
				DO CASE
					CASE BETWEEN(tdPAYDATE,{^2015-10-01},{^2017-12-31}) && this was still SSG
						lnMarkup = 1.300
					CASE BETWEEN(tdPAYDATE,{^2018-01-01},{^2099-12-31})
						lnMarkup = 1.310
				ENDCASE
			CASE THIS.cTempCo = 'V'   && Simplified Labor SS - Carson 2
				DO CASE
					CASE BETWEEN(tdPAYDATE,{^2015-10-01},{^2017-12-31}) && this was still SSG
						lnMarkup = 1.300
					CASE BETWEEN(tdPAYDATE,{^2018-01-01},{^2099-12-31})
						lnMarkup = 1.310
				ENDCASE
			CASE THIS.cTempCo = 'W'   && Allied Security - Louisville
				lnMarkup = 1.00
			CASE THIS.cTempCo = 'X'   && Chartwell - San Pedro
				DO CASE
					CASE BETWEEN(tdPAYDATE,{^2017-07-01},{^2099-12-31})
						lnMarkup = 1.310
				ENDCASE
			CASE THIS.cTempCo = 'Y'   && Simplified Labor SS - Mira Loma
				DO CASE
					CASE BETWEEN(tdPAYDATE,{^2017-07-01},{^2099-12-31})
						lnMarkup = 1.310
				ENDCASE
			CASE THIS.cTempCo = 'Z'   && The Job Store - Louisville
				DO CASE
					CASE BETWEEN(tdPAYDATE,{^2017-10-01},{^2099-12-31})
						lnMarkup = 1.370
				ENDCASE
			OTHERWISE
				* unexpected temp co
				lnMarkup = -1
		ENDCASE
		RETURN lnMarkup
	ENDFUNC	&&  getTempAgencyMarkupREG


	FUNCTION getTempAgencyMarkupOT
		LPARAMETERS tcSHORTNAME, tdPAYDATE, tcFILE_NUM
		* note: this is for Reg Hours only; separate function below for OT markup
		LOCAL lnMarkup
		DO CASE
			CASE INLIST(THIS.cTempCo,'D')  && QUALITY TEMPS - NJ
				DO CASE
					CASE BETWEEN(tdPAYDATE,{^2013-01-01},{^2099-12-31})
						* flat 31% with no variations per Ronald 3/12/15.
						lnMarkup = 1.310
				ENDCASE
				
				* 09/25/2017: special processing requested by Jim Killen to change markup for Quality On-Site Sue Ortiz to 1.18
				IF ALLTRIM(tcFILE_NUM) = '27247' THEN
					DO CASE
						CASE BETWEEN(tdPAYDATE,{^2017-09-01},{^2099-12-31})
							lnMarkup = 1.1800
						OTHERWISE
							* nothing
					ENDCASE
				ENDIF
				
			CASE INLIST(THIS.cTempCo,'F')  && CHARTWELL SS - KY
				DO CASE
					CASE BETWEEN(tdPAYDATE,{^2018-02-09},{^2099-12-31})
						lnMarkup = 1.310
				ENDCASE
				
			CASE INLIST(THIS.cTempCo,'G')  && CHARTWELL SS - NJ
				DO CASE
					CASE BETWEEN(tdPAYDATE,{^2018-01-01},{^2099-12-31})
						lnMarkup = 1.310
				ENDCASE
				
			CASE INLIST(THIS.cTempCo,'J')  && Diamond Staffing NJ
				DO CASE
					CASE BETWEEN(tdPAYDATE,{^2013-10-27},{^2015-01-04})
						lnMarkup = 1.2650
					CASE BETWEEN(tdPAYDATE,{^2015-01-05},{^2099-12-31})
						lnMarkup = 1.255
				ENDCASE
			CASE INLIST(THIS.cTempCo,'Q')  && COR-TECH - Louisville
				DO CASE
					CASE BETWEEN(tdPAYDATE,{^2015-03-08},{^2099-12-31})
						lnMarkup = 1.350
				ENDCASE
			CASE THIS.cTempCo = 'R'   && Staffing Solutions Group - San Pedro
				DO CASE
					CASE BETWEEN(tdPAYDATE,{^2015-03-08},{^2099-12-31})
						lnMarkup = 1.290
				ENDCASE
			CASE THIS.cTempCo = 'T'   && Staffing Solutions Group - Mira Loma
				DO CASE
					CASE BETWEEN(tdPAYDATE,{^2015-03-16},{^2099-12-31})
						lnMarkup = 1.290
				ENDCASE
			CASE THIS.cTempCo = 'U'   && Simplified Labor SS - Carson
				DO CASE
					* 1/3/2018 MB
					CASE BETWEEN(tdPAYDATE,{^2015-03-08},{^2017-12-31})  && this was still SSG
						lnMarkup = 1.290
					CASE BETWEEN(tdPAYDATE,{^2018-01-01},{^2099-12-31})
						lnMarkup = 1.310
				ENDCASE
			CASE THIS.cTempCo = 'V'   && Simplified Labor SS - Carson 2
				DO CASE
					* 1/3/2018 MB
					CASE BETWEEN(tdPAYDATE,{^2015-03-08},{^2017-12-31})  && this was still SSG
						lnMarkup = 1.290
					CASE BETWEEN(tdPAYDATE,{^2018-01-01},{^2099-12-31})
						lnMarkup = 1.310
				ENDCASE
			CASE THIS.cTempCo = 'W'   && Allied Security - Louisville
				lnMarkup = 1.00
			CASE THIS.cTempCo = 'X'   && Chartwell - San Pedro
				DO CASE
					CASE BETWEEN(tdPAYDATE,{^2017-07-01},{^2099-12-31})
						lnMarkup = 1.310
				ENDCASE
			CASE THIS.cTempCo = 'Y'   && Simplified - Mira Loma
				DO CASE
					CASE BETWEEN(tdPAYDATE,{^2017-07-01},{^2099-12-31})
						lnMarkup = 1.310
				ENDCASE
			CASE THIS.cTempCo = 'Z'   && The Job Store - Louisville
				DO CASE
					CASE BETWEEN(tdPAYDATE,{^2017-10-01},{^2099-12-31})
						lnMarkup = 1.370
				ENDCASE
			OTHERWISE
				* unexpected temp co
				lnMarkup = -1
		ENDCASE
		RETURN lnMarkup
	ENDFUNC	&& getTempAgencyMarkupOT


	FUNCTION getShiftDiff
		LPARAMETERS tcCalcdShift  &&, tcDept, tnBaseWage
		LOCAL lnShiftDiff, lcDept, lcCalcdShift
		*lcDept = ALLTRIM( tcDept )
		lcCalcdShift = ALLTRIM( tcCalcdShift )
		DO CASE
			CASE INLIST(THIS.cTempCo,'D','J')
				* Quality rules, per Jim Lake 2/6/13.
				* basic rules for shift differential:
				* simply, if temp works 2nd shift, they get $0.50 extra
				DO CASE
					CASE (lcCalcdShift = '1')
						lnShiftDiff = 0.00
					OTHERWISE
						*lnShiftDiff = 0.50
						lnShiftDiff = 0.00  && PER rONALD/jIM 3/23/2015 - they will put shift diff in base wage
				ENDCASE
			CASE THIS.cTempCo = 'G'  && Chartwell SS - NJ
				lnShiftDiff = 0.00
			CASE INLIST(THIS.cTempCo,'F','Q','W','Z')
				* COR-TECH ky, Allied and the Job Store
				DO CASE
					CASE (lcCalcdShift = '1')
						lnShiftDiff = 0.00
					OTHERWISE
						lnShiftDiff = 0.00
				ENDCASE
			CASE THIS.cTempCo = 'R'
				lnShiftDiff = 0.00
			CASE THIS.cTempCo = 'S'
				lnShiftDiff = 0.00
			CASE THIS.cTempCo = 'T'
				lnShiftDiff = 0.00
			CASE THIS.cTempCo = 'U'
				lnShiftDiff = 0.00
			CASE THIS.cTempCo = 'V'
				lnShiftDiff = 0.00
			CASE THIS.cTempCo = 'X'
				lnShiftDiff = 0.00
			CASE THIS.cTempCo = 'Y'
				lnShiftDiff = 0.00
			OTHERWISE
				lnShiftDiff = 0.00
		ENDCASE
		RETURN lnShiftDiff
	ENDFUNC
	

	FUNCTION IsDivisionValid
		LPARAMETERS tcDivision
		WITH THIS
			LOCAL llRetVal
			DO CASE
				CASE (.cTempCo == 'D') AND (INLIST(tcDivision,'04','14','60'))
					llRetVal = .T.
				CASE (.cTempCo == 'F') AND (INLIST(tcDivision,'15'))
					llRetVal = .T.
				CASE (.cTempCo == 'G') AND (INLIST(tcDivision,'04','14','60'))
					llRetVal = .T.
				CASE (.cTempCo == 'J') AND (INLIST(tcDivision,'04','14','60'))
					llRetVal = .T.
				CASE (.cTempCo == 'Q') AND (INLIST(tcDivision,'15'))
					llRetVal = .T.
				CASE (.cTempCo == 'R') AND (INLIST(tcDivision,'05','52','53','54','55','56','57','58','59','60','61','62','68'))
					llRetVal = .T.
				CASE (.cTempCo == 'T') AND (INLIST(tcDivision,'50','57','59'))
					llRetVal = .T.
				CASE (.cTempCo == 'U') AND (INLIST(tcDivision,'66','67'))
					llRetVal = .T.
				CASE (.cTempCo == 'V') AND (INLIST(tcDivision,'69'))
					llRetVal = .T.
				CASE (.cTempCo == 'W') AND (INLIST(tcDivision,'15'))
					llRetVal = .T.
				CASE (.cTempCo == 'X') AND (INLIST(tcDivision,'05','52','53','55','56','58','59','60','61','62','68'))
					llRetVal = .T.
				CASE (.cTempCo == 'Y') AND (INLIST(tcDivision,'51','54','57','59'))
					llRetVal = .T.
				OTHERWISE
					llRetVal = .F.
			ENDCASE
		ENDWITH
		RETURN llRetVal
	ENDFUNC


ENDDEFINE


