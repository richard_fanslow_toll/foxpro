*!* m:\dev\prg\ariat940_process.prg

*CD &lcPath

LogCommentStr = ""

delimchar = "*"
lcTranOpt= "TILDE"

lnNum = ADIR(tarray,lcPath+"*.*")

*lnNum = ADIR(tarray,"f:\ftpusers\g-III\940in-ca\hold\*.*")


IF lnNum = 0
	WAIT WINDOW AT 10,10 "      No "+cCustname+" 940s to import     " TIMEOUT 3
	NormalExit = .T.
	THROW
ENDIF

FOR thisfile = 1  TO lnNum
	xfile = ALLTRIM(lcPath+tarray[thisfile,1])
	cfilename = LOWER(tarray[thisfile,1])
	nFileSize = VAL(TRANSFORM(tarray(thisfile,2)))
	!ATTRIB -R [&xfile]
	IF !lTesting
		COPY FILE [&xfile] TO ("F:\ftpusers\g-iii\940xfer\"+cfilename) && to create 997
	ENDIF
	WAIT WINDOW "Importing file: "+cfilename NOWAIT
	DO m:\dev\prg\createx856a
	DO m:\dev\prg\loadedifile WITH xfile,delimchar,lcTranOpt,cCustname

	SELECT x856
	LOCATE

	archivefile  = (lcArchivePath+cfilename)

	LOCATE
	LOCATE FOR TRIM(x856.segment) = "GS" AND TRIM(x856.f1) = "FA"
	IF FOUND()
		WAIT WINDOW "This is a 997, skip it......." TIMEOUT 2
		COPY FILE [&xfile] TO [&archivefile]
		DELETE FILE [&xfile]
		LOOP
	ENDIF

	LOCATE
	DO create_pt_cursors WITH cUseFolder,lLoadSQL && Added to prep for SQL Server transition

	lOK = .T.

	DO ("m:\dev\prg\G3_940_bkdn")
	DO m:\dev\prg\all940_import

	archivefile  = (lcArchivePath+cfilename)
*	release_ptvars()

ENDFOR

&& now clean up the 940in archive files, delete for the upto the last 10 days
deletefile(lcArchivePath,IIF(DATE()<{^2016-05-01},20,10))

WAIT CLEAR
WAIT WINDOW "All "+cCustname+" pickticket files uploaded" TIMEOUT 2



