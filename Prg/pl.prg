* generate Payroll Liability email for Jimmy Campanelli.
* We MUST run this on Wednesday after the payroll for the following Friday is available.
* But we do not know exactly when that will be, so we will schedule it to run periodically.
* We will set a flag in a data table to signal when it can stop running.

runack("PL")

LOCAL loPayrollLiabilityReport
loPayrollLiabilityReport = CREATEOBJECT('PayrollLiabilityReport')
loPayrollLiabilityReport.MAIN()
CLOSE DATABASES ALL
RETURN

#DEFINE LOGIT 1
#DEFINE WAITIT 2
#DEFINE NOWAITIT 4
#DEFINE SENDIT 8
#DEFINE CRLF CHR(13) + CHR(10)
#DEFINE RETURN_DATA_MANDATORY .F.
#DEFINE RETURN_DATA_NOT_MANDATORY .T.
#DEFINE CSVFORMAT 6
#DEFINE WRITE_ONLY_UNBUFFERED 11
#DEFINE COMMA ","

DEFINE CLASS PayrollLiabilityReport AS CUSTOM

	cProcessName = 'PayrollLiabilityReport'

	lTestMode = .T.  && .F. FOR NORMAL OPERATION, .T. TO SUPPRESS ALL BUT DEVELOPER EMAILS AND CHANGE INPUT TABLE, ETC.

	dToday = DATE()

	lAutoYield = .T.

	cStartTime = TTOC(DATETIME())

	* table properties
	cControlTable = "F:\UTIL\ADPREPORTS\DATA\PLCONTROL"

	* connection properties
	nSQLHandle = 0

	* wait window properties
	nWaitWindowTimeout = 2
	lWaitWindowIsOn = .T.

	* logfile properties
	lLoggingIsOn = .T.
	cLogFile = 'F:\UTIL\ADPREPORTS\LOGFILES\PAYROLL_LIABILITY_REPORT_log.txt'

	* INTERNAL email properties
	lSendInternalEmailIsOn = .T.
	cFrom = 'TGF Corporate <fmicorporate@fmiint.com>'
	*cSendTo = 'Mark Bennett <mbennett@fmiint.com>'
	cSendTo = 'Jim Campanelli <jcampanelli@fmiint.com>, Karen Waldrip <kwaldrip@fmiint.com>'
	cCC = 'Mark Bennett <mbennett@fmiint.com>'
	cSubject = 'Payroll Liability Report for ' + DTOC(DATE())
	cAttach = ''
	cBodyText = ''
	
	cSubjectM = ''
	cSendToM = ''
	cBodyTextM = ''
	cAttachM = ''

	cCOMPUTERNAME = ''
	cUSERNAME = ''


	FUNCTION INIT
		IF NOT DODEFAULT()
			RETURN .F.
		ENDIF
		WITH THIS
			*SET RESOURCE OFF
			CLOSE DATA
			SET CENTURY ON
			SET DATE YMD
			SET HOURS TO 24
			SET ANSI ON
			SET TALK OFF
			SET DELETED ON
			SET CONSOLE OFF
			SET EXCLUSIVE OFF
			SET SAFETY OFF
			SET EXACT OFF
			SET STATUS BAR ON
			SET SYSMENU OFF
			SET ENGINEBEHAVIOR 70
			_VFP.AUTOYIELD = .lAutoYield
			.cCOMPUTERNAME = GETENV("COMPUTERNAME")
			.cUSERNAME = GETENV("USERNAME")
			IF .lTestMode THEN
				.cLogFile = 'F:\UTIL\ADPREPORTS\LOGFILES\PAYROLL_LIABILITY_REPORT_log_TESTMODE.txt'
				.cSendTo = 'Mark Bennett <mbennett@fmiint.com>'
			ENDIF
			.lLoggingIsOn = .lLoggingIsOn AND NOT EMPTY(.cLogFile)
			IF .lLoggingIsOn THEN
				SET ALTERNATE TO (.cLogFile) ADDITIVE
				SET ALTERNATE ON
			ENDIF
			SET PROCEDURE TO VALIDATIONS ADDITIVE
		ENDWITH
	ENDFUNC


	FUNCTION DESTROY
		WITH THIS
			IF .lLoggingIsOn  THEN
				SET ALTERNATE OFF
				SET ALTERNATE TO
			ENDIF
		ENDWITH
		DODEFAULT()
	ENDFUNC


	FUNCTION MAIN
		WITH THIS
			LOCAL lnNumberOfErrors, lcSQL, lcControlTable, ldToday, ldNextPaydate
			LOCAL lcFiletoSaveAs, llAlreadyDone, llPaydateWasFound, lcSQLPAYDATE, lcManifest
			TRY
				lnNumberOfErrors = 0

				.TrackProgress('', LOGIT)
				.TrackProgress('', LOGIT)
				.TrackProgress('=========================================================', LOGIT+SENDIT)
				.TrackProgress("Payroll Liability REPORT process started....", LOGIT+SENDIT+NOWAITIT)
				.TrackProgress('COMPUTERNAME = ' + .cCOMPUTERNAME, LOGIT+SENDIT)
				.TrackProgress('USERNAME = ' + .cUSERNAME, LOGIT+SENDIT)
				.TrackProgress('=========================================================', LOGIT+SENDIT)
				IF .lTestMode THEN
					.TrackProgress('                        TEST MODE                        ', LOGIT+SENDIT)
					.TrackProgress('=========================================================', LOGIT+SENDIT)
				ENDIF
				.TrackProgress('', LOGIT)

				ldToday = .dToday
				*ldToday = .dToday + 7

				* calc next paydate -- since we always run this on Wednesday or Thursday or possibly Friday, simply add 0, 1 or 2 days to current date!
				DO CASE
					CASE DOW(ldToday) = 3 && Tuesday
						ldNextPaydate = ldToday + 3
					CASE DOW(ldToday) = 4 && Wednesday
						ldNextPaydate = ldToday + 2
					CASE DOW(ldToday) = 5 && Thursday
						ldNextPaydate = ldToday + 1
					CASE DOW(ldToday) = 6 && Friday
						ldNextPaydate = ldToday
					OTHERWISE
						* we're running on unexpected day of week and cannot continue
						ldNextPaydate = {}
				ENDCASE
		
*!*			* to force for special non-friday paydates		
*!*			ldNextPaydate = {^2009-12-31}

				IF EMPTY( ldNextPaydate ) THEN
					.TrackProgress('ERROR: empty ldNextPaydate!', LOGIT+SENDIT)
				ELSE

					.TrackProgress('Calculated next Paydate = ' + TRANSFORM(ldNextPaydate), LOGIT+SENDIT)

					.cSubject = 'Payroll Liability Report for PayDate: ' + TRANSFORM(ldNextPaydate)

					lcFiletoSaveAs = "F:\UTIL\ADPREPORTS\REPORTS\PAYROLL_LIABILITY_BY_COMPANY_" + STRTRAN(DTOC(ldNextPaydate),"/","-") + ".XLS"

					* Because we are scheduling this report to run multiple times over Wed/Thursday,
					* we want to see if we have already successfully run this report for the next paydate.
					* We will only continue if it has not yet run successfully.

					llAlreadyDone = .F.
					
					
					IF NOT .lTestMode THEN
						* check against control table

						* open control table
						lcControlTable = .cControlTable

						IF USED('ControlTable') THEN
							USE IN ControlTable
						ENDIF

						USE (lcControlTable) AGAIN IN 0 ALIAS ControlTable
						SELECT ControlTable
						LOCATE FOR PAYDATE = ldNextPaydate
						IF FOUND() THEN
							* we tried at last once before with this paydate; see if we are done with this paydate
							IF ControlTable.DONE THEN
								* yes, we are done
								llAlreadyDone = .T.
							ENDIF
						ELSE
							* this is the first try for this paydate; add a record for it in the control table
							m.PAYDATE = ldNextPaydate
							m.DONE = .F.
							m.TRIES = 0
							INSERT INTO ControlTable FROM MEMVAR
						ENDIF
						SELECT ControlTable
						LOCATE

					ENDIF && NOT .lTestMode THEN

					IF llAlreadyDone THEN
						* nothing to do; and don't bother sending an email
						.lSendInternalEmailIsOn = .F.
					ELSE
						* not yet run; make sure we send an email
						.lSendInternalEmailIsOn = .T.

						* get ss#s from ADP
						OPEN DATABASE F:\UTIL\ADPCONNECTION\CONNECT1

						.nSQLHandle = SQLCONNECT("PCPAYWIN","MBENNETT","VP737A")

						IF .nSQLHandle > 0 THEN

							* see if the paydate we are looking for exists in check info table yet
							llPaydateWasFound = .F.

							IF USED('sqlPAYDATE') THEN
								USE IN sqlPAYDATE
							ENDIF

							lcSQLPAYDATE = ;
								"SELECT " + ;
								" CHECKVIEWPAYDATE " + ;
								" FROM REPORTS.V_CHK_VW_INFO " + ;
								" WHERE CHECKVIEWPAYDATE = DATE '" + STRTRAN(DTOC(ldNextPaydate),"/","-") + "' "

							llPaydateWasFound = .ExecSQL(lcSQLPAYDATE, 'sqlPAYDATE', RETURN_DATA_MANDATORY)

							* we no longer need this cursor
							IF USED('sqlPAYDATE') THEN
								USE IN sqlPAYDATE
							ENDIF

							IF NOT llPaydateWasFound THEN
								* paydate was NOT found, log a try in the control record for this paydate
								SELECT ControlTable
								LOCATE FOR PAYDATE = ldNextPaydate
								IF FOUND() THEN
									REPLACE ControlTable.TRIES WITH (ControlTable.TRIES + 1), ;
										STATUS WITH "PayDate " + TRANSFORM(ldNextPaydate) + " not yet in ADP", ;
										ControlTable.WHENDONE WITH DATETIME()
								ELSE
									.TrackProgress('*** Error locating Paydate record for ' + TRANSFORM(ldNextPaydate) + ' in Control Table - step 1', LOGIT+SENDIT)
								ENDIF
								SELECT ControlTable
								LOCATE
							ELSE
								* paydate was found, run the report

								IF USED('sqlCHKINFO') THEN
									USE IN sqlCHKINFO
								ENDIF
								IF USED('sqlCHKINFO2') THEN
									USE IN sqlCHKINFO2
								ENDIF
								IF USED('sqlCHKINFO3') THEN
									USE IN sqlCHKINFO3
								ENDIF
								IF USED('sqlDEDUCT') THEN
									USE IN sqlDEDUCT
								ENDIF
								IF USED('sqlDEDUCT2') THEN
									USE IN sqlDEDUCT2
								ENDIF
								IF USED('sqlCOMPANYBKDN') THEN
									USE IN sqlCOMPANYBKDN
								ENDIF
								IF USED('sqlCOMPANYBKDNPRE') THEN
									USE IN sqlCOMPANYBKDNPRE
								ENDIF

								* get most check info from V_CHK_VW_INFO
								lcSQL = ;
									"SELECT " + ;
									" {fn IFNULL(CHECKVIEWFEDTAXAMT,0000000.00)} AS FEDTAX, " + ;
									" {fn IFNULL(CHECKVIEWLVLCTXAMT,0000000.00)} AS LVDLCLTAX, " + ;
									" {fn IFNULL(CHECKVIEWLVSTTXAMT,0000000.00)} AS LVDSTTAX, " + ;
									" {fn IFNULL(CHECKVIEWWKLCTXAMT,0000000.00)} AS WKDLCLTAX, " + ;
									" {fn IFNULL(CHECKVIEWWKSTTXAMT,0000000.00)} AS WKDSTTAX, " + ;
									" {fn IFNULL(CHECKVIEWMEDTAXAMT,0000000.00)} AS MEDTAX, " + ;
									" {fn IFNULL(CHECKVIEWSSTAXAMT,0000000.00)} AS SSTAX, " + ;
									" {fn IFNULL(CHECKVIEWSUISDIAMT,0000000.00)} AS SUITAX, " + ;
									" {fn IFNULL(CHECKVIEWNETPAYAMT,0000000.00)} AS NETPAY, " + ;
									" {fn IFNULL(CHECKVIEWGROSSPAYA,0000000.00)} AS GROSSPAY, " + ;
									" {fn IFNULL(CHECKVIEWREGERNING,0000000.00)} AS REGEARNING, " + ;
									" {fn IFNULL(CHECKVIEWOTEARNING,0000000.00)} AS OTEARNING, " + ;
									" {fn IFNULL(CHECKVIEWLVDLOCAL,'  ')} AS LVDLOCAL, " + ;
									" {fn IFNULL(CHECKVIEWLVDSTATE,'  ')} AS LVDSTATE, " + ;
									" {fn IFNULL(CHECKVIEWSUISDICD,'  ')} AS SUICODE, " + ;
									" {fn IFNULL(CHECKVIEWWKDLOCAL,'  ')} AS WKDLOCAL, " + ;
									" {fn IFNULL(CHECKVIEWWKDSTATE,'  ')} AS WKDSTATE, " + ;
									" {fn LEFT(CHECKVIEWHOMEDEPT,2)} AS DIVISION, " + ;
									" {fn SUBSTRING(CHECKVIEWHOMEDEPT,3,4)} AS DEPARTMENT, " + ;
									" CHECKVIEWPAYDATE AS PAYDATETIME, " + ;
									" CHECKVIEWPAYROLL# AS PAYROLLNUM, " + ;
									" CHECKVIEWWEEK# AS PAYROLLWEEK, " + ;
									" COMPANYCODE AS ADPCOMP, " + ;
									" SOCIALSECURITY# AS SSNUM, " + ;
									" FILE# AS FILENUM, " + ;
									" NAME " + ;
									" FROM REPORTS.V_CHK_VW_INFO " + ;
									" WHERE CHECKVIEWPAYDATE = DATE '" + STRTRAN(DTOC(ldNextPaydate),"/","-") + "' "

								* get deduction info
								lcSQL2 = ;
									" SELECT CHECKVIEWDEDCODE AS DEDCODE, " + ;
									" {fn IFNULL(CHECKVIEWDEDAMT,0000000.00)} AS DEDAMOUNT, " + ;
									" {fn LEFT(CHECKVIEWHOMEDEPT,2)} AS DIVISION, " + ;
									" {fn SUBSTRING(CHECKVIEWHOMEDEPT,3,4)} AS DEPARTMENT, " + ;
									" CHECKVIEWPAYDATE AS PAYDATETIME, " + ;
									" CHECKVIEWPAYROLL# AS PAYROLLNUM, " + ;
									" CHECKVIEWWEEK# AS PAYROLLWEEK, " + ;
									" COMPANYCODE AS ADPCOMP, " + ;
									" FILE# AS FILENUM " + ;
									" FROM REPORTS.V_chk_vw_deduction " + ;
									" WHERE CHECKVIEWPAYDATE = DATE '" + STRTRAN(DTOC(ldNextPaydate),"/","-") + "' "

								IF .ExecSQL(lcSQL, 'sqlCHKINFO', RETURN_DATA_MANDATORY) ;
										AND .ExecSQL(lcSQL2, 'sqlDEDUCT', RETURN_DATA_MANDATORY) THEN

									* filter deductions by selected PAYDATES AND AGGREGATE FOR FURTHER PROCESSING
									SELECT ;
										ADPCOMP, INT(VAL(DIVISION)) AS DIVISION, INT(VAL(DEPARTMENT)) AS DEPARTMENT, FILENUM, DEDCODE, ;
										SUM(DEDAMOUNT) AS DEDAMOUNT ;
										FROM sqlDEDUCT A INTO CURSOR sqlDEDUCT2 ;
										WHERE DEDAMOUNT <> 0.0 ;
										GROUP BY ADPCOMP, DIVISION, DEPARTMENT, FILENUM, DEDCODE ;
										ORDER BY ADPCOMP, DIVISION, DEPARTMENT, FILENUM, DEDCODE

									IF (NOT USED('sqlDEDUCT2')) OR EOF('sqlDEDUCT2') THEN
										*MESSAGEBOX("No DEDUCTIONS data was found",0+64,"Running Report")
										.TrackProgress("***** No DEDUCTIONS data was found", LOGIT+SENDIT)
										*RETURN
									ENDIF

									SELECT ;
										FEDTAX, ;
										LVDLCLTAX, ;
										LVDSTTAX, ;
										WKDLCLTAX, ;
										WKDSTTAX, ;
										MEDTAX, ;
										SSTAX, ;
										SUITAX, ;
										NETPAY, ;
										GROSSPAY, ;
										REGEARNING, ;
										OTEARNING, ;
										LVDLOCAL, ;
										LVDSTATE, ;
										SUICODE, ;
										WKDLOCAL, ;
										WKDSTATE, ;
										INT(VAL(DIVISION)) AS DIVISION, ;
										INT(VAL(DEPARTMENT)) AS DEPARTMENT, ;
										PAYDATETIME, ;
										ADPCOMP, ;
										SSNUM, ;
										FILENUM, ;
										NAME, ;
										0000000.00 AS CASTATETAX, ;
										0000000.00 AS NJSTATETAX, ;
										0000000.00 AS NYSTATETAX, ;
										0000000.00 AS OTHRSTTAX, ;
										0000000.00 AS LOCALTAX, ;
										0000000.00 AS CASUITAX, ;
										0000000.00 AS NJSUITAX, ;
										0000000.00 AS NYSUITAX, ;
										0000000.00 AS ALSUITAX, ;
										0000000.00 AS WVSUITAX, ;
										0000000.00 AS TNSUITAX, ;
										0000000.00 AS FLSUITAX, ;
										0000000.00 AS GASUITAX, ;
										0000000.00 AS NCSUITAX, ;
										0000000.00 AS SCSUITAX, ;
										0000000.00 AS PASUITAX, ;
										0000000.00 AS OTHRSUITAX, ;
										0000000.00 AS MEDMATCH, ;
										0000000.00 AS SSMATCH, ;
										SPACE(30) AS MASTERCOMP ;
										FROM sqlCHKINFO A INTO CURSOR sqlCHKINFO2 ;
										ORDER BY FILENUM ;
										READWRITE

									IF (NOT USED('sqlCHKINFO2')) OR EOF('sqlCHKINFO2') THEN
										*MESSAGEBOX("No CHECKINFO data was found",0+64,"Running Report")
										.TrackProgress("***** No CHECKINFO data was found", LOGIT+SENDIT)
										RETURN
									ENDIF

									* WHEN AN EMPLOYEE CHANGES SS#, CAUSING DIFFERENT ONE IN CHKINFO THAN IN EMPLOYEE TABLE, IT CAN CAUSE JOIN
									* PROBLEMS LATER ON. FIX ANY WE KNOW ABOUT HERE.
									SELECT sqlCHKINFO2
									SCAN
										* HECTOR VALENTIN
										IF sqlCHKINFO2.FILENUM = 2768 AND sqlCHKINFO2.SSNUM = '139675451' THEN
											REPLACE sqlCHKINFO2.SSNUM WITH '139625451'
										ENDIF
									ENDSCAN

									* populate the state-specific fields
									SELECT sqlCHKINFO2
									GOTO TOP
									SCAN
										STORE 0.0 TO lnCASTATETAX, lnNJSTATETAX, lnNYSTATETAX, lnLOCALTAX, lnCASUITAX, lnNJSUITAX, lnNYSUITAX, lnOTHRSTTAX, lnOTHRSUITAX
										STORE 0.0 TO lnALSUITAX, lnWVSUITAX, lnTNSUITAX, lnFLSUITAX, lnGASUITAX, lnNCSUITAX, lnSCSUITAX, lnPASUITAX
										* NOTE: the only local tax we have now is NYC City Tax, where v_chk_vw_info.CHECKVIEWLVDLOCAL or CHECKVIEWWKDLOCAL = '0022'
										* so for now we can represent lnLOCALTAX as NYC Taxes on reports. If we start seeing more codes we would have to create more
										* buckets to hold them.
										* MB 2/06/04.
										lnLOCALTAX = NVL(LVDLCLTAX,0.0) + NVL(WKDLCLTAX,0.0)

										DO CASE
											CASE LVDSTATE = 'CA'
												lnCASTATETAX = lnCASTATETAX + NVL(LVDSTTAX,0.0)
												*lnCASUITAX = lnCASUITAX + NVL(SUITAX,0.0)
											CASE LVDSTATE = 'NJ'
												lnNJSTATETAX = lnNJSTATETAX + NVL(LVDSTTAX,0.0)
												*lnNJSUITAX = lnNJSUITAX + NVL(SUITAX,0.0)
											CASE LVDSTATE = 'NY'
												lnNYSTATETAX = lnNYSTATETAX + NVL(LVDSTTAX,0.0)
												*lnNYSUITAX = lnNYSUITAX + NVL(SUITAX,0.0)
											OTHERWISE
												lnOTHRSTTAX = lnOTHRSTTAX + NVL(LVDSTTAX,0.0)
												*lnOTHRSUITAX = lnOTHRSUITAX + NVL(SUITAX,0.0)
										ENDCASE

										* BASING SUI TAX BREAKDOWN ON SUICODE, NOT LIVED-IN STATE, PER LUCILLE 04/07/04 MB
										DO CASE
											CASE SUICODE = '03'
												lnALSUITAX = lnALSUITAX + NVL(SUITAX,0.0)
											CASE SUICODE = '12'
												lnNCSUITAX = lnNCSUITAX + NVL(SUITAX,0.0)
											CASE SUICODE = '19'
												lnNYSUITAX = lnNYSUITAX + NVL(SUITAX,0.0)
											CASE SUICODE = '21'
												lnNJSUITAX = lnNJSUITAX + NVL(SUITAX,0.0)
											CASE SUICODE = '23'
												lnGASUITAX = lnGASUITAX + NVL(SUITAX,0.0)
											CASE SUICODE = '26'
												lnWVSUITAX = lnWVSUITAX + NVL(SUITAX,0.0)
											CASE SUICODE = '35'
												lnSCSUITAX = lnSCSUITAX + NVL(SUITAX,0.0)
											CASE SUICODE = '42'
												lnFLSUITAX = lnFLSUITAX + NVL(SUITAX,0.0)
											CASE SUICODE = '52'
												lnTNSUITAX = lnTNSUITAX + NVL(SUITAX,0.0)
											CASE SUICODE = '59'
												lnPASUITAX = lnPASUITAX + NVL(SUITAX,0.0)
											CASE SUICODE = '75'
												lnCASUITAX = lnCASUITAX + NVL(SUITAX,0.0)
											OTHERWISE
												lnOTHRSUITAX = lnOTHRSUITAX + NVL(SUITAX,0.0)
										ENDCASE

										DO CASE
											CASE WKDSTATE = 'CA'
												lnCASTATETAX = lnCASTATETAX + NVL(WKDSTTAX,0.0)
											CASE WKDSTATE = 'NJ'
												lnNJSTATETAX = lnNJSTATETAX + NVL(WKDSTTAX,0.0)
											CASE WKDSTATE = 'NY'
												lnNYSTATETAX = lnNYSTATETAX + NVL(WKDSTTAX,0.0)
											OTHERWISE
												lnOTHRSTTAX = lnOTHRSTTAX + NVL(WKDSTTAX,0.0)
										ENDCASE

										REPLACE CASTATETAX WITH lnCASTATETAX, ;
											NJSTATETAX WITH lnNJSTATETAX, ;
											NYSTATETAX WITH lnNYSTATETAX, ;
											OTHRSTTAX WITH lnOTHRSTTAX, ;
											LOCALTAX WITH lnLOCALTAX, ;
											CASUITAX WITH lnCASUITAX, ;
											NJSUITAX WITH lnNJSUITAX, ;
											NYSUITAX WITH lnNYSUITAX, ;
											ALSUITAX WITH lnALSUITAX, ;
											WVSUITAX WITH lnWVSUITAX, ;
											TNSUITAX WITH lnTNSUITAX, ;
											FLSUITAX WITH lnFLSUITAX, ;
											GASUITAX WITH lnGASUITAX, ;
											NCSUITAX WITH lnNCSUITAX, ;
											SCSUITAX WITH lnSCSUITAX, ;
											PASUITAX WITH lnPASUITAX, ;
											OTHRSUITAX WITH lnOTHRSUITAX

									ENDSCAN

									* Aggregate, disregarding the PAYDATETIME & worked/lived-in state fields since we no longer need them.
									* AND ADD SOME MORE FIELDS STILL TO BE POPULATED IN THE NEXT PHASE
									SELECT ;
										ADPCOMP, DIVISION, DEPARTMENT, FILENUM, SSNUM, NAME, ;
										SUM(FEDTAX) AS FEDTAX, ;
										SUM(MEDTAX) AS MEDTAX, ;
										SUM(SSTAX) AS SSTAX, ;
										SUM(CASTATETAX) AS CASTATETAX, ;
										SUM(NJSTATETAX) AS NJSTATETAX, ;
										SUM(NYSTATETAX) AS NYSTATETAX, ;
										SUM(OTHRSTTAX) AS OTHRSTTAX, ;
										SUM(LOCALTAX) AS LOCALTAX, ;
										SUM(CASUITAX) AS CASUITAX, ;
										SUM(NJSUITAX) AS NJSUITAX, ;
										SUM(NYSUITAX) AS NYSUITAX, ;
										SUM(ALSUITAX) AS ALSUITAX, ;
										SUM(WVSUITAX) AS WVSUITAX, ;
										SUM(TNSUITAX) AS TNSUITAX, ;
										SUM(FLSUITAX) AS FLSUITAX, ;
										SUM(GASUITAX) AS GASUITAX, ;
										SUM(NCSUITAX) AS NCSUITAX, ;
										SUM(SCSUITAX) AS SCSUITAX, ;
										SUM(PASUITAX) AS PASUITAX, ;
										SUM(OTHRSUITAX) AS OTHRSUITAX, ;
										SUM(NETPAY) AS NETPAY, ;
										0000000.00 AS DED_56_AMT, ;
										0000000.00 AS DED_67_AMT, ;
										0000000.00 AS DED_68_AMT, ;
										0000000.00 AS DED_69_AMT, ;
										0000000.00 AS DED_70_AMT, ;
										0000000.00 AS DED_71_AMT, ;
										0000000.00 AS DED_72_AMT, ;
										0000000.00 AS DED_73_AMT, ;
										0000000.00 AS DED_74_AMT, ;
										0000000.00 AS DED_75_AMT, ;
										0000000.00 AS DED_76_AMT, ;
										0000000.00 AS DED_77_AMT, ;
										0000000.00 AS DED_78_AMT, ;
										0000000.00 AS DED_A_AMT, ;
										0000000.00 AS DED_B_AMT, ;
										0000000.00 AS DED_C_AMT, ;
										0000000.00 AS DED_D_AMT, ;
										0000000.00 AS DED_E_AMT, ;
										0000000.00 AS DED_F_AMT, ;
										0000000.00 AS DED_H_AMT, ;
										0000000.00 AS DED_I_AMT, ;
										0000000.00 AS DED_K_AMT, ;
										0000000.00 AS DED_L_AMT, ;
										0000000.00 AS DED_M_AMT, ;
										0000000.00 AS DED_N_AMT, ;
										0000000.00 AS DED_O_AMT, ;
										0000000.00 AS DED_Q_AMT, ;
										0000000.00 AS DED_S_AMT, ;
										0000000.00 AS DED_T_AMT, ;
										0000000.00 AS DED_V_AMT, ;
										0000000.00 AS DED_W_AMT, ;
										0000000.00 AS DED_X_AMT, ;
										0000000.00 AS DED_Y_AMT, ;
										0000000.00 AS DED_Z_AMT, ;
										0000000.00 AS DED_OTHER, ;
										0000000.00 AS EARN_1_AMT, ;
										0000000.00 AS EARN_A_AMT, ;
										0000000.00 AS EARN_B_AMT, ;
										0000000.00 AS EARN_C_AMT, ;
										0000000.00 AS EARN_D_AMT, ;
										0000000.00 AS EARN_E_AMT, ;
										0000000.00 AS EARN_F_AMT, ;
										0000000.00 AS EARN_G_AMT, ;
										0000000.00 AS EARN_H_AMT, ;
										0000000.00 AS EARN_J_AMT, ;
										0000000.00 AS EARN_M_AMT, ;
										0000000.00 AS EARN_O_AMT, ;
										0000000.00 AS EARN_Q_AMT, ;
										0000000.00 AS EARN_R_AMT, ;
										0000000.00 AS EARN_S_AMT, ;
										0000000.00 AS EARN_U_AMT, ;
										0000000.00 AS EARN_V_AMT, ;
										0000000.00 AS EARN_Z_AMT, ;
										0000000.00 AS EARN_OTHER, ;
										SUM(OTEARNING) AS OTEARNING, ;
										SUM(REGEARNING) AS REGEARNING, ;
										SUM(GROSSPAY) AS GROSSPAY ;
										FROM sqlCHKINFO2 ;
										INTO CURSOR sqlCHKINFO3 ;
										GROUP BY ADPCOMP, DIVISION, DEPARTMENT, FILENUM, SSNUM, NAME ;
										ORDER BY ADPCOMP, DIVISION, DEPARTMENT, FILENUM, SSNUM, NAME ;
										READWRITE

									* NOW HORIZONTALIZE DEDUCTION DATA FROM sqlDEDUCT2 INTO sqlCHKINFO3

									************************************************************************
									* NOTE: adp deduction code validation table is : REPORTS.V_VALID_DED
									************************************************************************

									SELECT sqlDEDUCT2
									GOTO TOP

									lcMissingDEDCODEs = ""
									SCAN
										WAIT WINDOW NOWAIT "Updating Deductions for Employee Number " + TRANSFORM(sqlDEDUCT2.FILENUM)
										lcDEDCODE = UPPER(ALLTRIM(sqlDEDUCT2.DEDCODE))
										SELECT sqlCHKINFO3
										GOTO TOP
										LOCATE FOR ;
											ADPCOMP = sqlDEDUCT2.ADPCOMP ;
											AND DIVISION = sqlDEDUCT2.DIVISION ;
											AND DEPARTMENT = sqlDEDUCT2.DEPARTMENT ;
											AND FILENUM = sqlDEDUCT2.FILENUM
										IF FOUND() THEN
											DO CASE
												CASE INLIST(lcDEDCODE,'56','67','68','69','70','71','72','73','74','75','76','77','78') ;
														OR INLIST(lcDEDCODE,'A','B','C','D','E','F','H','I','K','L','M','N','O','Q','S','T','V','W','X','Y','Z')
													* these are deduction codes we can use "as is"
													lcCMD = "REPLACE sqlCHKINFO3.DED_" + lcDEDCODE + "_AMT WITH sqlDEDUCT2.DEDAMOUNT"
													*CASE INLIST(lcDEDCODE,'M')
													*	* these are deduction codes we need to reverse the sign on, i.e. MI&E is negative in ADP
													*	lcCMD = "REPLACE sqlCHKINFO3.DED_" + lcDEDCODE + "_AMT WITH (-1 * sqlDEDUCT2.DEDAMOUNT)"
												OTHERWISE
													* since there (theoretically) could be multiple deductions not accounted for in the list above,
													* we must accumulate, not replace, in sqlCHKINFO3.DED_OTHER
													lcCMD = "REPLACE sqlCHKINFO3.DED_OTHER WITH (sqlCHKINFO3.DED_OTHER + sqlDEDUCT2.DEDAMOUNT)"
													*lcMissingDEDCODEs = lcMissingDEDCODEs + lcDEDCODE + CRLF
													IF NOT ("~" + lcDEDCODE + "~") $ lcMissingDEDCODEs THEN
														lcMissingDEDCODEs = lcMissingDEDCODEs + ("~" + lcDEDCODE + "~") + CRLF
													ENDIF
											ENDCASE
											* do the replace
											&lcCMD
										ENDIF
									ENDSCAN
									WAIT CLEAR

									******************************************************************************
									******************************************************************************
									******************************************************************************
									* roll up data for GROSS PAY BY MASTER COMPANY

									SELECT SPACE(30) AS MASTERCOMP, * FROM sqlCHKINFO3 INTO CURSOR sqlCOMPANYBKDNPRE READWRITE

									* POPULATE MASTERCOMP FIELD
									SELECT sqlCOMPANYBKDNPRE
									SCAN
										*REPLACE sqlCOMPANYBKDNPRE.MASTERCOMP WITH .GetMasterCompany(sqlCOMPANYBKDNPRE.DIVISION)
										REPLACE sqlCOMPANYBKDNPRE.MASTERCOMP WITH GetMasterCompanyFromNumericDivision(sqlCOMPANYBKDNPRE.DIVISION)
									ENDSCAN

									SELECT ;
										MASTERCOMP, ;
										SUM(GROSSPAY)AS GROSSPAY, ;
										SUM(DED_A_AMT + DED_D_AMT + DED_H_AMT + DED_I_AMT + DED_K_AMT + DED_M_AMT + DED_N_AMT + DED_S_AMT + DED_T_AMT) AS DEDUCTIONS, ;
										000000.00 AS LIABILITY ;
										FROM sqlCOMPANYBKDNPRE ;
										INTO CURSOR sqlCOMPANYBKDN ;
										GROUP BY 1 ;
										ORDER BY 1 ;
										READWRITE

									SELECT sqlCOMPANYBKDN
									SCAN
										REPLACE sqlCOMPANYBKDN.LIABILITY WITH ( sqlCOMPANYBKDN.GROSSPAY - sqlCOMPANYBKDN.DEDUCTIONS )
									ENDSCAN

									SELECT sqlCOMPANYBKDN
									GOTO TOP
									COPY TO (lcFiletoSaveAs) XL5

									.cAttach = lcFiletoSaveAs

									IF NOT .lTestMode THEN
										* update control table to indicate success
										SELECT ControlTable
										LOCATE FOR PAYDATE = ldNextPaydate
										IF FOUND() THEN
											REPLACE ControlTable.DONE WITH .T., ;
												ControlTable.TRIES WITH (ControlTable.TRIES + 1), ;
												STATUS WITH "Successfully reported on PayDate " + TRANSFORM(ldNextPaydate), ;
												ControlTable.WHENDONE WITH DATETIME()
										ELSE
											.TrackProgress('*** Error locating Paydate record for ' + TRANSFORM(ldNextPaydate) + ' in Control Table - step 2', LOGIT+SENDIT)
										ENDIF
										SELECT ControlTable
										LOCATE
									ENDIF && NOT .lTestMode THEN

								ENDIF && THISFORM.execsql(lcSQL, 'sqlCHKINFO', RETURN_DATA_MANDATORY)
								
								
								*** CREATE A CHECK MANIFEST
								* ActiveWindow.SelectedSheets.HPageBreaks.Add Before:=ActiveCell
								
								lcSQLMANIFEST = ;
									"SELECT " + ;
									" B.CHECKVIEWCHKVCHRCD AS VOUCHERCD, " + ;
									" {fn LEFT(A.HOMEDEPARTMENT,2)} AS DIVISION, " + ; 
									" {fn SUBSTRING(A.HOMEDEPARTMENT,3,4)} AS DEPARTMENT, " + ;
									" A.NAME, " + ; 
									" A.CUSTAREA3 AS WORKSITE, " + ; 
									" A.FILE#, " + ;
									" B.CHECKVIEWPAYDATE AS PAYDATE, " + ;
									" A.COMPANYCODE AS ADP_COMP " + ; 
									" FROM REPORTS.V_EMPLOYEE A, REPORTS.V_CHK_VW_INFO B " + ;
									" WHERE CHECKVIEWPAYDATE = DATE '" + STRTRAN(DTOC(ldNextPaydate),"/","-") + "' " + ;
									" AND B.SOCIALSECURITY# = A.SOCIALSECURITY# " + ;
									" AND B.COMPANYCODE = A.COMPANYCODE " + ;
									" AND B.FILE# = A.FILE# " + ;
									" ORDER BY B.CHECKVIEWCHKVCHRCD, {fn LEFT(A.HOMEDEPARTMENT,2)}, {fn SUBSTRING(A.HOMEDEPARTMENT,3,4)}, A.NAME " 

								IF .ExecSQL(lcSQLMANIFEST, 'CURMANIFEST', RETURN_DATA_MANDATORY) THEN
								
									* TOP SORT: LIVE CHECKS FIRST 'C', THEN VOUCHERS 'V'
								
							
									lcManifest = 'F:\UTIL\ADPREPORTS\REPORTS\MANIFEST.XLS'
									
									.cSubjectM = 'Check Manifest for PayDate: ' + TRANSFORM(ldNextPaydate)
									.cSendToM = 'mbennett@fmiint.com'
									.cBodyTextM = 'See attached Check Manifest'
									.cAttachM = lcManifest
								
									SELECT CURMANIFEST
									COPY TO (lcManifest) XL5									

									DO FORM dartmail2 WITH .cSendToM,.cFrom,.cSubjectM,.cCC,.cAttachM,.cBodyTextM,"A"
									
								ELSE
									.TrackProgress("ERROR creating the Check Manifest.", LOGIT+SENDIT+NOWAITIT)
								ENDIF	
								
							ENDIF && llPaydateWasFound

							.TrackProgress("Payroll Liability REPORT process ended normally.", LOGIT+SENDIT+NOWAITIT)
						ELSE
							* connection error
							.TrackProgress('Unable to connect to ADP Payroll System.', LOGIT+SENDIT)
						ENDIF   &&  .nSQLHandle > 0

					ENDIF && llAlreadyDone

				ENDIF && NOT EMPTY( ldNextPaydate )

			CATCH TO loError

				.TrackProgress('There was an error.',LOGIT+SENDIT)
				.TrackProgress('The Error # is: ' + TRANSFORM(loError.ErrorNo), LOGIT+SENDIT)
				.TrackProgress('The Error Message is: ' + TRANSFORM(loError.MESSAGE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred in Program: ' + TRANSFORM(loError.PROCEDURE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred at line #: ' + TRANSFORM(loError.LINENO), LOGIT+SENDIT)
				lnNumberOfErrors = lnNumberOfErrors + 1
				IF TYPE('oWorkbook') = "O" THEN
					oWorkbook.SAVE()
				ENDIF
				IF TYPE('oExcel') = "O" THEN
					oExcel.QUIT()
				ENDIF
				CLOSE DATABASES ALL

			ENDTRY

			CLOSE DATA
			WAIT CLEAR
			***************** INTERNAL email results ******************************
			.TrackProgress('About to send status email.',LOGIT)
			.TrackProgress('The logfile is: ' + .cLogFile, SENDIT)
			.TrackProgress("Payroll Liability REPORT process started: " + .cStartTime, LOGIT+SENDIT)
			.TrackProgress("Payroll Liability REPORT process finished: " + TTOC(DATETIME()), LOGIT+SENDIT)

			IF .lSendInternalEmailIsOn THEN
				DO FORM dartmail2 WITH .cSendTo,.cFrom,.cSubject,.cCC,.cAttach,.cBodyText,"A"
				.TrackProgress('Sent status email.',LOGIT)
			ELSE
				.TrackProgress('Did not send status email: THIS.lSendInternalEmailIsOn = FALSE.',LOGIT)
			ENDIF

		ENDWITH
		RETURN
	ENDFUNC && main


*!*		FUNCTION GetMasterCompany
*!*			LPARAMETERS tnDivision
*!*			LOCAL lcRetVal
*!*			DO CASE
*!*				CASE INLIST(tnDivision,61)
*!*					lcRetVal = "Seamaster"
*!*				CASE INLIST(tnDivision,62)
*!*					lcRetVal = "MLI"
*!*				CASE INLIST(tnDivision,71,72,73,74)
*!*					lcRetVal = "TUG"
*!*				CASE INLIST(tnDivision,80)
*!*					lcRetVal = "AmerRussia"
*!*				CASE INLIST(tnDivision,90)
*!*					lcRetVal = "Summit Global Logistics"
*!*				OTHERWISE
*!*					lcRetVal = "FMI"
*!*			ENDCASE
*!*			RETURN lcRetVal
*!*		ENDFUNC


	FUNCTION ExecSQL
		LPARAMETERS tcSQL, tcCursorName, tlNoDataReturnedIsOkay
		LOCAL llRetval, lnResult
		WITH THIS
			* close target cursor if it's open
			IF USED(tcCursorName)
				USE IN (tcCursorName)
			ENDIF
			WAIT WINDOW "Fetching remote data; please wait..." NOWAIT
			lnResult = sqlexec(.nSQLHandle, tcSQL, tcCursorName)
			llRetval = ( lnResult > 0 )
			IF llRetval THEN
				* see if any data came back
				IF NOT tlNoDataReturnedIsOkay THEN
					IF (NOT USED(tcCursorName)) OR EOF(tcCursorName) THEN
						llRetval = .F.
						.TrackProgress('No data was returned by this query: ' +CRLF + tcSQL, LOGIT+SENDIT)
						.cSendTo = 'Mark Bennett <mbennett@fmiint.com>'
					ENDIF
				ENDIF
			ELSE
				.TrackProgress("Result Code [" + TRANSFORM(lnResult) + "] returned by this query: " + tcSQL, LOGIT+SENDIT)
				.cSendTo = 'Mark Bennett <mbennett@fmiint.com>'
			ENDIF
			WAIT CLEAR
			RETURN llRetval
		ENDWITH
	ENDFUNC


	PROCEDURE TrackProgress
		* do any combination of Wait Window, writing to logfile, and adding to body of email,
		* based on nFlags parameter.
		LPARAMETERS tcExpression, tnFlags
		WITH THIS
			IF BITAND(tnFlags,LOGIT) = LOGIT THEN
				IF .lLoggingIsOn THEN
					?
					? .cProcessName + "  " + TTOC(DATETIME()) + ": " + tcExpression
				ENDIF
			ENDIF
			IF .lWaitWindowIsOn THEN
				IF BITAND(tnFlags,NOWAITIT) = NOWAITIT THEN
					WAIT WINDOW tcExpression NOWAIT
				ENDIF
				IF BITAND(tnFlags,WAITIT) = WAITIT THEN
					WAIT WINDOW tcExpression TIMEOUT .nWaitWindowTimeout
				ENDIF
			ENDIF
			IF BITAND(tnFlags,SENDIT) = SENDIT THEN
				IF .lSendInternalEmailIsOn THEN
					.cBodyText = .cBodyText + tcExpression + CRLF + CRLF
				ENDIF
			ENDIF
		ENDWITH
	ENDPROC  &&  TrackProgress

ENDDEFINE

