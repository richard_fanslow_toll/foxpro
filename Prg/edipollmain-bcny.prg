*runack("EDIPOLLER_BCNY	")

CLOSE DATABASES ALL
CLEAR ALL
RELEASE ALL
cPath = ["M:\DEV\PRG; M:\DEV\PROJ"]
SET PATH TO &cPath ADDITIVE

WITH _SCREEN
	.AUTOCENTER = .T.
	.WINDOWSTATE = 0
	.BORDERSTYLE = 1
	.WIDTH = 260
	.HEIGHT = 210
	.TOP = 570
	.LEFT = 940
	.CLOSABLE = .F.
	.MAXBUTTON = .F.
	.MINBUTTON = .F.
	.SCROLLBARS = 0
	.CAPTION = "BCNY/SYNCLAIRE POLLER - EDI"
ENDWITH

TRY
	SET STEP ON
	DO m:\dev\prg\_setvars
	PUBLIC lSkipError
	lSkipError =  .F.
	SET STATUS BAR OFF
	SET SYSMENU OFF
	ON ESCAPE CANCEL
	SELECT 0
	USE F:\3pl\DATA\edipollertrip SHARED ALIAS ebcny
	REPLACE ebcny.bcny WITH .F.
	USE IN ebcny

	SELECT 0
	USE F:\3pl\DATA\last945time SHARED ALIAS bcnylast
	REPLACE bcnylast.checkflag WITH .T. FOR poller = "BCNY" IN bcnylast
	REPLACE bcnylast.tries WITH 0 FOR poller = "BCNY" IN bcnylast
	USE IN bcnylast
	ASSERT .F. MESSAGE "Going into poller run"
	DO FORM m:\dev\frm\edi_jobhandler_bcny

CATCH TO oErr
	ASSERT .F. MESSAGE "AT CATCH SECTION"
	IF !lSkipError
		tfrom    ="TGF EDI Processing Center <tgf-edi-ops@fmiint.com>"
		tsubject = "BCNY Outbound EDI Poller Error ("+TRANSFORM(oErr.ERRORNO)+") at "+TTOC(DATETIME())
		tattach  = ""
		SELECT 0
		USE F:\3pl\DATA\mailmaster ALIAS mm
		LOCATE
		LOCATE FOR mm.edi_type = 'MISC' AND mm.taskname = "GENERAL"
		tsendto = IIF(mm.use_alt,sendtoalt,sendto)
		tcc = IIF(mm.use_alt,ccalt,cc)
		USE IN mm

		tmessage = "BCNY 945 Poller Major Error..... Please fix me........!"
		lcSourceMachine = SYS(0)

		SELECT 0
		USE F:\3pl\DATA\edipollertrip SHARED ALIAS ebcny
		REPLACE ebcny.bcny WITH .T. IN ebcny
		
		tmessage =tmessage+ "Try/CATCH Exeception Message:"+CHR(13)+;
			[  Error: ] + STR(oErr.ERRORNO) +CHR(13)+;
			[  LineNo: ] + STR(oErr.LINENO) +CHR(13)+;
			[  Message: ] + oErr.MESSAGE +CHR(13)+;
			[  Procedure: ] + oErr.PROCEDURE +CHR(13)+;
			[  Details: ] + oErr.DETAILS +CHR(13)+;
			[  StackLevel: ] + STR(oErr.STACKLEVEL) +CHR(13)+;
			[  LineContents: ] + oErr.LINECONTENTS+CHR(13)+;
			[  UserValue: ] + oErr.USERVALUE+CHR(13)+;
			[  Computer:  ] +lcSourceMachine+CHR(13)
		DO FORM m:\dev\FRM\dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
	ENDIF
FINALLY
	CLOSE DATABASES ALL
	SET STATUS BAR ON
	ON ERROR
ENDTRY
