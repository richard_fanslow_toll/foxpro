*!* Billabong 945 Creation Program
*!* Creation Date: 04.18.2017, Joe from ROW ONE prog

PARAMETERS cBOL,nAcctNum

PUBLIC ARRAY thisarray(1)
PUBLIC c_CntrlNum,c_GrpCntrlNum,cReturned,cWO_Num,cWO_NumStr,nWO_Num1,lBBGFilesOut,lPrepack
PUBLIC nUnitsum,nCtnCount,tsendto,tcc,tsendtoerr,tccerr,lTesting,lTestinput,nPTCount,cUseFolder,lDoShipDups
PUBLIC nOrigSeq,nOrigGrpSeq,cProgname,cISA_Num,cRefBOL,cMailName,cBilling,lMBOL,lEmail,lOverflow
PUBLIC cMod,cMBOL,cEDIType,lParcelType,cFilenameShort,cFilenameOut,cFilenameArch,lcPath,cWO_NumList,dtTransferDT
*WAIT WINDOW "Billabong 945 Process" nowait
*ON ESCAPE
* ON ERROR DEBUG

SET STEP ON


guserid ="JOEB"
lOverflow = .F.
*
lTesting   = .F.  && Set to .t. for testing
lTestinput = .F. && lTesting  &&  Set to .t. for test input files only!
lEmail = .t.
dtTransferDT = DATETIME(2018,04,02,22,00,00)

DO m:\dev\prg\_setvars WITH lTesting
IF !lTesting
*  SET ASSERTS OFF
ENDIF

SET DELETED ON
cProgname = "billabong945_create"
IF lTesting
*  ASSERT .F. MESSAGE "At beginning of Billabong 945 creation"
ENDIF
SET ESCAPE ON
ON ESCAPE CANCEL
*ON ERROR debug
lIsError = .F.
lDoCatch = .T.
cShip_ref = ""
lCloseOutput = .T.
STORE 0 TO nFilenum,nOrigSeq,nOrigGrpSeq
cCarrierType = "M" && Default as "Motor Freight"
ccustname = ""
cWO_NumStr = ""
cWO_NumList = ""
cStatus=""
cRefBOL = '9999'
cUseFolder = ""
nCtnWt = 0
lDoShipDups = .F.

TRY
*  nAcctNum = 6744
	lFederated = .F.
	cPPName = "Billabong"
	tfrom = "TGF Warehouse Operations <toll-edi-ops@tollgroup.com>"
	tmessage=""
	tcc = ""
	lDoBOLGEN = .F.

	lISAFlag = .T.
	lSTFlag = .T.
	nLoadid = 1
	lSQLMail = .F.
	lDoCompare = IIF(lTesting OR lTestinput,.F.,.T.)
	lDoCompare = IIF(DATETIME()<dtTransferDT,.F.,lDoCompare)

	lDoCompare = .F.
	cOffice = "L"
	cMod = cOffice
	gMasterOffice = cOffice
	goffice = cMod
	cMBOL = ""
	cEDIType = "945"
	lParcelType = .F.
	cBOL = ALLTRIM(cBOL)

	IF VARTYPE(cBOL) = "L" OR LEFT(cBOL,2) = "PS"
		IF !lTesting AND !lTestinput
			WAIT WINDOW "BOL incorrect/missing...terminating" TIMEOUT 1
			lCloseOutput = .F.
			DO ediupdate WITH "MISS/WRONG BOL",.T.
			THROW
		ELSE
			CLOSE DATABASES ALL
			IF !USED('edi_trigger')
				cEDIFolder = "F:\3PL\DATA\"
				USE (cEDIFolder+"edi_trigger") IN 0 ALIAS edi_trigger
			ENDIF
			lookups()
*!* TEST DATA AREA
			cBOL = "1Z2W43820300107719"
			cTime = DATETIME()
			nAcctNum  =  6744
			lFedEx = .F.
		ENDIF
	ENDIF

	CREATE CURSOR temp945 (accountid i,isa_num c(10),st_num c(10),wo_num i,bol_no c(20),ship_ref c(20),filename c(50),filedate T)
	IF lTestinput
		cUseFolder = "F:\WHP\WHDATA\"
	ELSE
		cUseFolder = "F:\WHL\WHDATA\"
	ENDIF

	cSCAC = ""
	cErrMsg = ""

	swc_cutctns(cBOL)

	IF USED('OUTSHIP')
		USE IN outship
	ENDIF

	csq1 = [select * from outship where accountid = ]+TRANSFORM(nAcctNum)+[ and office = ']+cOffice+[' and bol_no = ']+cBOL+[']
	xsqlexec(csq1,,,"wh")
	IF RECCOUNT() = 0
		cErrMsg = "MISS BOL "+cBOL
		DO ediupdate WITH cErrMsg,.T.
		THROW
	ENDIF
	INDEX ON bol_no TAG bol_no
	INDEX ON wo_num TAG wo_num
	INDEX ON ship_ref TAG ship_ref

	SELECT outship

	IF SEEK(cBOL,'outship','bol_no')
		cShip_ref = ALLTRIM(outship.ship_ref)
		cSCAC = TRIM(outship.scac)
		lDoScanPack = IIF("PROCESSMODE*SCANPACK"$outship.shipins,.T.,.F.)
*    lLTL = IIF(INLIST(scac,'USTD','UTTK','UTDC'),.T.,.F.)
**TMARG changed 7/19/17
		lLTL = .F.
		lLTL = IIF(INLIST(scac,'UTTK'),.T.,.F.)
	ELSE
		SET STEP ON
		DO ediupdate WITH "BOL NOT FOUND",.T.
		THROW
	ENDIF

	SELECT outship
***changed to bypass Trade Direct  11/01/17 TMARG    UTTK','USTD','UTDC
*!*    SCAN FOR bol_no = cBOL AND accountid = nAcctNum AND "PICKPACK"$shipins And !Substr(cBOL,1,4)= "0491" AND scac !='UTTK'  AND scac !='USTD' AND scac !='UTDC'
*!*      lcQuery = "select * from updtgr where ship_ref='"+ALLTRIM(outship.ship_ref)+"' and accountid = "+TRANSFORM(nAcctNum)
*!*      xsqlexec(lcQuery,"updtgr",,"wh")
*!*      IF RECCOUNT("updtgr") = 0
*!*        SET STEP ON
*!*        DO ediupdate WITH "PT NOT COMPLETE: "+ALLTRIM(outship.ship_ref),.T.
*!*        THROW
*!*      ENDIF
*!*    ENDSCAN

&& Determine if this is a MBOL, so N9*MB and N9*BM segments can be added to the 945 PT loops
	lookups()
	lMBOL = .F.
*!* DO NOT CHANGE OR COMMENT OUT THE FOLLOWING LINES, FOR ANY REASON - Joe, 09.25.2017
	xsqlexec("select * from bl where mblnum = '"+cBOL+"' and accountid in ("+gbillabongaccts+")",,,"wh")
	SELECT bl
	LOCATE
	IF lTesting
*    BROWSE
	ENDIF


	IF RECCOUNT()>0
		lMBOL = .T.
		m.mbol = cBOL
		SELECT 0
		CREATE CURSOR mblgroup (mbol c(20),subbol c(20),blship_ref c(20))
		SELECT bl
		LOCATE
		SCAN
			m.subbol = ""
			m.subbol = ALLTRIM(bl.bol_no)
			nBLID = bl.blid
			csq = [select * from bldet where blid = ]+TRANSFORM(nBLID)
			xsqlexec(csq,,,"wh")
			xsqlexec("select * from bldet where blid="+TRANSFORM(nBLID) ,,,"wh")

			SELECT bldet
			LOCATE
			SCAN
				m.blship_ref = ""
				m.blship_ref = ALLTRIM(bldet.ship_ref)
				INSERT INTO mblgroup FROM MEMVAR
			ENDSCAN
		ENDSCAN
	ENDIF
	IF lTesting AND lMBOL
		SELECT mblgroup
		LOCATE
*    BROWSE
	ENDIF

	IF lMBOL
		SELECT DISTINCT subbol FROM mblgroup INTO CURSOR tempmbl
		SELECT tempmbl
		WAIT WINDOW "There are "+ALLTRIM(STR(RECCOUNT()))+" sub-BOLs in this MBOL" TIMEOUT 1
		USE IN tempmbl
	ENDIF

	cLTLBOL = ''
	lCTNSCACs = .F.

	=SEEK(cBOL,'outship','bol_no')
	IF INLIST(TRIM(outship.scac),'USTD','UTTK','UTDC')
		lCTNSCACs = .T.
		xsqlexec("select * from ctnucc where ship_ref = '"+cShip_ref+"' and mod = 'L'",,,"wh")

		SELECT ctnucc
		LOCATE FOR ctnucc.ship_ref = cShip_ref
		IF !FOUND()
			DO ediupdate WITH "CTNUCC MISS PT: "+cShip_ref,.T.
			THROW
		ENDIF

		SELECT * FROM ctnucc WHERE ctnucc.ship_ref = cShip_ref ORDER BY ship_ref,ucc,serialno INTO CURSOR tempctnucc
		SELECT tempctnucc
		cLTLBOL = ALLTRIM(tempctnucc.bol)
	ENDIF

	SELECT outship
	SCAN FOR outship.bol_no = cBOL
		IF EMPTY(outship.dcnum)
			REPLACE outship.dcnum WITH '9999' IN outship NEXT 1
			REPLACE outship.storenum WITH 9999 IN outship NEXT 1
		ENDIF
	ENDSCAN
	LOCATE

	SELECT DISTINCT ship_ref FROM outship WHERE bol_no = cBOL INTO CURSOR temp1bb
	SELECT temp1bb
	SELECT 0
	CREATE CURSOR ovpts (outshipid N(10))
	SELECT ovpts
	INDEX ON outshipid TAG outshipid

	SELECT temp1bb
	SCAN
		cShip_ref2 = ALLTRIM(temp1bb.ship_ref)
		SELECT outship
		SCAN FOR outship.ship_ref = cShip_ref2
			IF "OV"$outship.ship_ref
				INSERT INTO ovpts (outshipid) VALUES (outship.outshipid)
				WAIT WINDOW "Overflow PT Found at Ship_ref: "+ALLTRIM(outship.ship_ref) NOWAIT
				EXIT
			ENDIF
		ENDSCAN
	ENDSCAN
	USE IN temp1bb
	cRefBOL = IIF(lOverflow,cBOL,"9999")

	SELECT bol_no,wo_num ;
		FROM outship WHERE bol_no = cBOL ;
		AND accountid = nAcctNum ;
		GROUP BY 1,2 ;
		ORDER BY 1,2 ;
		INTO CURSOR sqldc
	LOCATE
	SCAN
		nWO_Num = sqldc.wo_num
		IF lDoCompare
			cRetMsg = "X"
			DO m:\dev\prg\sqldata-COMPARE WITH cUseFolder,cBOL,nWO_Num,cOffice,nAcctNum,cPPName
			IF cRetMsg<>"OK"
				lCloseOutput = .T.
				cWO_Num = ALLTRIM(STR(nWO_Num1))
				DO ediupdate WITH "QTY COMPARE ERROR",.T.
				THROW
			ENDIF
		ENDIF
	ENDSCAN

	IF USED('parcel_carriers')
		USE IN parcel_carriers
	ENDIF
	USE F:\3pl\DATA\parcel_carriers IN 0 ALIAS parcel_carriers

	SELECT outship
	=SEEK(cBOL,"outship","bol_no")
	nWO_Num = outship.wo_num
	cSCAC = ALLTRIM(outship.scac)
	cShip_ref = ALLTRIM(outship.ship_ref)

	lParcelLong = .F.
	lParcelType = IIF(SEEK(cSCAC,'parcel_carriers','scac'),.T.,.F.)
	IF lTesting
*    SET STEP ON
	ENDIF
	lParcelType = IIF(LEFT(cBOL,2)="PS",.F.,lParcelType)
	lParcelType = IIF(ALLTRIM(outship.keyrec) = ALLTRIM(outship.bol_no) ,.F.,lParcelType)
	lParcelType = IIF(lLTL,.F.,lParcelType)
	lParcelType = IIF(INLIST(scac,'UTTK','USTD','UTDC','EMSY'),.F.,lParcelType)

	cCarrierType = IIF(lParcelType,"U","M")

	IF lParcelType
*!* Added code block
		#IF 0
			xsqlexec("select distinct ship_ref from updtgr where accountid in ("+gbillabongaccts+")",,,"wh")  && Selects PT list from updtgr...will return if Parcel PT isn't in that list yet. Added per Paul, 10.04.2017, Joe.
			SELECT updtgr
			LOCATE FOR updtgr.ship_ref = cShip_ref
			IF !FOUND()
				USE IN updtgr
				DO ediupdate WITH "NOT IN UPDTGR",.T.
				THROW
			ENDIF

			IF USED('updtgr')
				USE IN updtgr
			ENDIF
		#ENDIF
*!* End added block

		SELECT outship
		LOCATE
		cScanParam = IIF(lParcelLong,"ship_ref = cShip_ref","bol_no = cBOL")
		SCAN FOR &cScanParam
			cShip_ref = outship.ship_ref
			alength = ALINES(apt,outship.shipins,.T.,CHR(13))
			cLTN = segmentget(@apt,"LONGTRKNUM",alength)
			IF INLIST(outship.scac,"USPS","PRRM","SPAR","SFIR","SPRI","SPAD")  OR LEFT(ALLTRIM(outship.ship_via),4) = "USPS" && USPS parcels
				IF EMPTY(ALLTRIM(cLTN)) AND cBOL # "CJ469389144US"
					SET STEP ON
					lParcelLong = .F.
					cErrMsg = "MISS LONGTRKNUM FOR USPS SHIPMENT"  && Will error the entire BOL.
					DO ediupdate WITH cErrMsg,.T.
					THROW
				ENDIF
			ENDIF
		ENDSCAN
	ENDIF

	USE IN parcel_carriers
	IF lParcelType AND LEN(ALLTRIM(cBOL)) = 17
		DO ediupdate WITH "PARCEL WITH WRONG BOL#",.T.
		THROW
	ENDIF

	IF VARTYPE(nWO_Num) # "N"
		lCloseOutput = .T.
		DO ediupdate WITH "BAD WO#",.T.
		THROW
	ENDIF
	cWO_Num = ALLTRIM(STR(nWO_Num))

	lTestMail = IIF(lEmail,lTesting,.F.) && Sends mail to Joe only
	IF lTesting
		lBBGFilesOut = .F.
	ELSE
		lBBGFilesOut = .T. && If true, copies output to FTP folder (default = .t.)
	ENDIF
	lStandalone = lTesting

*!*		lTestMail = .T.
*!*		lBBGFilesOut = .F.

	STORE "LB" TO cWeightUnit
	lPrepack = .F.
	WAIT WINDOW "Now preparing tables and variables" NOWAIT NOCLEAR

*!* SET CUSTOMER CONSTANTS
	ccustname = UPPER("Billabong")  && Customer Identifier
	cX12 = "004050"  && X12 Standards Set used
	cMailName = "Billabong"
	SELECT 0
	USE F:\3pl\DATA\mailmaster ALIAS mm SHARED
	IF lTestMail
		LOCATE FOR mm.edi_type = "MISC" AND mm.taskname = "JOETEST"
		lUseAlt = mm.use_alt
		tsendto = IIF(lUseAlt,mm.sendtoalt,mm.sendto)
		tcc = IIF(lUseAlt,mm.ccalt,mm.cc)
		tsendtoerr = tsendto
		tccerr = tcc
	ELSE
		LOCATE FOR mm.edi_type = "945" AND mm.office = "L" AND mm.accountid = 6718
		IF !FOUND()
			DO ediupdate WITH "MAILMASTER ACCT PROBLEM",.T.
			THROW
		ELSE
			lUseAlt = mm.use_alt
			tsendto = IIF(lUseAlt,mm.sendtoalt,mm.sendto)
			tcc = IIF(lUseAlt,mm.ccalt,mm.cc)

			LOCATE FOR mm.edi_type = "MISC" AND mm.taskname = "GENERAL"
			lUseAlt = mm.use_alt
			tsendtoerr = IIF(lUseAlt,mm.sendtoalt,mm.sendto)
			tccerr = IIF(lUseAlt,mm.ccalt,mm.cc)
		ENDIF
	ENDIF

	LOCATE FOR mm.edi_type = "945" AND mm.office = "L" AND mm.accountid = 6718
	lcHoldPath = ALLTRIM(mm.holdpath)
	lcArchivePath = ALLTRIM(mm.archpath)
	lcOutPath = ALLTRIM(mm.basepath)
	LOCATE
	USE IN mm

*!* SET OTHER CONSTANTS
	DO CASE
		CASE cOffice = "M"
			cCustLoc =  "FL"
			cFMIWarehouse = ""
			cCustPrefix = "945"+"f"
			cDivision = "Florida"
			cFolder = "WHM"
			cSF_Addr1  = "11400 NW 32ND AVE"
			cSF_CSZ    = "MIAMI+FL+33167"

		CASE cOffice = "N"
			cCustLoc =  "NJ"
			cFMIWarehouse = ""
			cCustPrefix = "945"+"j"
			cDivision = "New Jersey"
			cFolder = "WHN"
			cSF_Addr1  = "800 FEDERAL BLVD"
			cSF_CSZ    =  "CARTERET+NJ+07008"

		OTHERWISE
			cCustLoc =  "CA"
			cFMIWarehouse = ""
			cCustPrefix = "945"+"c"
			cDivision = "California"
			cFolder = "WHC"
			cSF_Addr1  = "450 WESTMONT DRIVE"
			cSF_CSZ    = "SAN PEDRO+CA+90731"
	ENDCASE
	cCustFolder = UPPER(ccustname)
	lNonEDI = .F.  && Assumes not a Non-EDI 945
	STORE "" TO lcKey
	STORE 0 TO alength,nLength
	cTime = DATETIME()
	cDelTime = SUBSTR(TTOC(cTime,1),9,4)
	cApptNum = ""

*!* SET OTHER CONSTANTS
	cString = ""

	csendqual = "ZZ"
	csendid = IIF(lTesting,"TOLLTEST","TGFUSA")
	crecqual = "ZZ"
	crecid = IIF(lTesting,"BBGTOLLTST","BBGTOLLPRD")

	cfd = "|"
	csegd = "~"
	cterminator = ">" && Used at end of ISA segment

	nSegCtr = 0

	dt1 = TTOC(DATETIME(),1)
	dtmail = TTOC(DATETIME())
	dt2 = DATETIME()

	cPacTime = dt2-(3*3600)
	cTruncTime = SUBSTR(TTOC(cPacTime,1),9,4)
	cDate = DTOS(TTOD(cPacTime))
	cTruncDate = RIGHT(cDate,6)
	cfiledate = cDate+cTruncTime
	cOrig = "J"
	nSTCount = 0
	cStyle = ""
	cPTString = ""
	nTotCtnWt = 0
	nTotCtnCount = 0
	nUnitsum = 0
	cTargetStyle = ""

** PAD ID Codes
	csendidlong = PADR(csendid,15," ")
	crecidlong = PADR(crecid,15," ")

	cFilenameHold = (lcHoldPath+cCustPrefix+dt1+".txt")
	cFilenameShort = JUSTFNAME(cFilenameHold)
	cFilenameOut = (lcOutPath+cFilenameShort)
	cFilenameArch = (lcArchivePath+cFilenameShort)
	nFilenum = FCREATE(cFilenameHold)

	IF USED('SHIPMENT')
		USE IN shipment
	ENDIF

	IF USED('PACKAGE')
		USE IN package
	ENDIF

*!*    dDate = IIF(INLIST(DOW(DATE()),7,1),DATE()-5,DATE()-3)
*!*    csq1 = [select * from shipment where right(rtrim(str(accountid)),4) in (]+gbillabongaccts+[) and shipdate >= {]+DTOC(dDate)+[}]
*!*    xsqlexec(csq1,,,"wh")

	xsqlexec("select * from shipment where 1=0",,,"wh")  && Sets up an empty cursor
	SELECT outship && temp cursor with all PTs on the BOL
	SCAN
		cSR = ALLTRIM(outship.ship_ref)
		xsqlexec("select * from shipment where right(rtrim(str(accountid)),4) in ("+gbillabongaccts+") and pickticket = '"+cSR+"'","sm1",,"wh")
		SELECT sm1
		LOCATE
		IF RECCOUNT()=0 AND lParcelType
			cErrMsg = "NO SHIPMENT DATA"
			THROW
		ENDIF

		SELECT shipment
		APPEND FROM DBF('sm1')
		USE IN sm1
	ENDSCAN
	RELEASE cSR

	SELECT shipment
	INDEX ON shipmentid TAG shipmentid
	INDEX ON pickticket TAG pickticket
	LOCATE

	IF RECCOUNT("shipment") > 0
		xjfilter="shipmentid in ("
		SCAN
			nShipmentId = shipment.shipmentid
			xsqlexec("select * from package where shipmentid="+TRANSFORM(nShipmentId),,,"wh")

			IF RECCOUNT("package")>0
				xjfilter=xjfilter+TRANSFORM(shipmentid)+","
			ELSE
*        xjfilter="1=0"
			ENDIF
		ENDSCAN
		xjfilter=LEFT(xjfilter,LEN(xjfilter)-1)+")"

		xsqlexec("select * from package where "+xjfilter,,,"wh")
	ELSE
		xsqlexec("select * from package where .f.",,,"wh")
	ENDIF
	SELECT package

	INDEX ON shipmentid TAG shipmentid
	SET ORDER TO TAG shipmentid

	IF !USED('outship')
*    ASSERT .F. MESSAGE "Found: OUTSHIP not open...debug"
		USE (cUseFolder+"outship") IN 0 ALIAS outship
	ENDIF
	IF !SEEK(cBOL,'outship','bol_no')
		WAIT WINDOW "BOL not found in OUTSHIP" TIMEOUT 1
		DO ediupdate WITH "BOL NOT FOUND",.T.
		THROW
	ENDIF

	SELECT dcnum FROM outship WHERE outship.bol_no = cBOL GROUP BY 1 INTO CURSOR tempx
	LOCATE

	IF EOF()
		DO ediupdate WITH "EMPTY DC/STORE#",.T.
		THROW
	ENDIF

	IF EMPTY(tempx.dcnum)
		USE IN tempx
		SELECT storenum FROM outship WHERE outship.bol_no = cBOL GROUP BY 1 INTO CURSOR tempx
		IF EOF()
			DO ediupdate WITH "EMPTY DC/STORE#",.T.
			THROW
		ENDIF

		IF EMPTY(tempx.storenum)
			DO ediupdate WITH "EMPTY DC/STORE#",.T.
			THROW
		ELSE
			IF RECCOUNT('tempx')  > 1 AND cBOL<>"04907314677854548"
				DO ediupdate WITH "MULTIPLE DC #'s",.T.
				THROW
			ENDIF
		ENDIF
	ENDIF

	IF USED('tempx')
		USE IN tempx
	ENDIF
	IF USED('OUTDET')
		USE IN outdet
	ENDIF

	selectoutdet()
	SELECT outdet
	INDEX ON outdetid TAG outdetid
	IF DATETIME()<dtTransferDT
		LOCATE
*		BROWSE
	ENDIF

&& Added code blocks to the following to overcome BOL vs. WO selection issues, 12.23.2005
	SELECT outship
	LOCATE

	IF USED("sqlptbbg")
		USE IN sqlptbbg
	ENDIF
	IF FILE("F:\3pl\DATA\sqlptbbg.dbf")
		DELETE FILE "F:\3pl\DATA\sqlptbbg.dbf"
	ENDIF

	SELECT bol_no,ship_ref ;
		FROM outship ;
		WHERE bol_no = cBOL ;
		AND INLIST(accountid,&gbillabongaccts) ;
		GROUP BY 1,2 ;
		ORDER BY 1,2 ;
		INTO DBF F:\3pl\DATA\sqlptbbg

	USE IN sqlptbbg
	SELECT 0
	USE F:\3pl\DATA\sqlptbbg ALIAS sqlptbbg
	LOCATE
	IF lTesting
		BROWSE
	ENDIF
	IF EOF()
		cErrMsg = "NO SQL DATA"
		DO ediupdate WITH cErrMsg,.T.
		THROW
	ENDIF
	cRetMsg = ""

	DO m:\dev\prg\sqlconnectbbg_pt  WITH nAcctNum,ccustname,cPPName,.T.,cOffice

	IF cRetMsg<>"OK"
		DO ediupdate WITH "SQLCONN: "+cRetMsg,.T.
		THROW
	ENDIF

	SELECT vbillabongpp
	IF lTesting
		cWinMsg = "Browsing SQL Data file"
		WAIT WINDOW cWinMsg TIMEOUT 1
		BROWSE TIMEOUT 20
	ENDIF

	#IF 0

		SELECT wo_num FROM vbillabongpp GROUP BY 1 INTO CURSOR temprunid1
		SELECT runid FROM vbillabongpp GROUP BY 1 INTO CURSOR temprunid2
		SELECT temprunid1
		STORE RECCOUNT() TO nWORecs
		SELECT temprunid2
		STORE RECCOUNT() TO nRIDRecs

		IF nWORecs#nRIDRecs
			DO ediupdate WITH "MULTIPLE RUNIDS",.T.
			THROW
		ENDIF
		USE IN temprunid1
		USE IN temprunid2
	#ENDIF

	IF !USED("scacs")
		xsqlexec("select * from scac","scacs",,"wh")
		INDEX ON scac TAG scac
	ENDIF

	SELECT outship
	LOCATE
	WAIT CLEAR
	WAIT WINDOW "Now creating Header information" NOWAIT NOCLEAR

*!* HEADER LEVEL EDI DATA
	DO num_incr_isa

	cISA_Num = PADL(c_CntrlNum,9,"0")
	nISA_Num = INT(VAL(cISA_Num))

	cISACode = IIF(lTesting,"T","P")

	SELECT outship
	crecidlong = PADR(crecid,15," ")

	nHdr = 0
	STORE "ISA"+cfd+"00"+cfd+"          "+cfd+"00"+cfd+"          "+cfd+csendqual+cfd+csendidlong+cfd+;
		crecqual+cfd+crecidlong+cfd+cTruncDate+cfd+cTruncTime+cfd+"U"+cfd+"00401"+cfd+;
		cISA_Num+cfd+"0"+cfd+cISACode+cfd+cterminator+csegd TO cString
	DO cstringbreak

	STORE "GS"+cfd+"SW"+cfd+csendid+cfd+crecid+cfd+cDate+cfd+cTruncTime+cfd+c_CntrlNum+;
		cfd+"X"+cfd+cX12+csegd TO cString
	DO cstringbreak

	SELECT outship
	LOCATE

	IF !SEEK(nWO_Num,'outship','wo_num')
		WAIT CLEAR
		WAIT WINDOW "Invalid Work Order Number - Not Found in OUTSHIP!" TIMEOUT 1
		DO ediupdate WITH "WO "+TRANSFORM(nWO_Num)+" NOT FOUND",.T.
		THROW
	ENDIF

	IF !lTesting
		IF EMPTY(cBOL)  && May be empty if triggered from PT screen, or FedEx shipment
			WAIT CLEAR
			WAIT WINDOW "MISSING Bill of Lading Number!" TIMEOUT 1
			DO ediupdate WITH "BOL# EMPTY",.T.
			THROW
		ELSE
			IF !SEEK(PADR(TRIM(cBOL),20),"outship","bol_no")
				WAIT CLEAR
				SET STEP ON
				WAIT WINDOW "Invalid Bill of Lading Number - Not Found!" TIMEOUT 1
				DO ediupdate WITH "BOL# NOT FOUND",.T.
				THROW
			ENDIF
		ENDIF
	ENDIF

*************************************************************************
*1  PTIDSOURCE/OUTSHIP RECORD MAIN LOOP
*  Loops through individual OUTSHIP lines for BOL#
*************************************************************************
	WAIT CLEAR
	WAIT WINDOW "Now creating BOL#-based information..." NOWAIT NOCLEAR

	SELECT outship
	IF !lTesting
		COUNT TO N FOR outship.bol_no = cBOL AND !EMPTYnul(del_date)
	ELSE
		COUNT TO N FOR outship.bol_no = cBOL
	ENDIF

	IF N=0
		DO ediupdate WITH "INCOMP BOL",.T.
		THROW
	ENDIF

	LOCATE
	cMissDel = ""

	oscanstr = "outship.bol_no = PADR(cBOL,20) AND !EMPTYnul(outship.del_date) AND INLIST(outship.accountid,&gbillabongaccts)"

	SELECT outship
	nPTCount = 0
	SCAN FOR &oscanstr
		xxShipRef = outship.ship_ref
		IF "OV"$outship.ship_ref
			LOOP
		ENDIF

		lPrepack = !outship.picknpack

		SCATTER MEMVAR MEMO

		nWO_Num = m.wo_num
		cWO_Num = ALLTRIM(STR(nWO_Num))
		nAcctNum = outship.accountid
		IF !(cWO_Num$cWO_NumStr)
			cWO_NumStr = IIF(EMPTY(cWO_NumStr),cWO_Num,cWO_NumStr+","+cWO_Num)
		ENDIF
		IF !(cWO_Num$cWO_NumList)
			cWO_NumList = IIF(EMPTY(cWO_NumList),cWO_Num,cWO_NumList+CHR(13)+cWO_Num)
		ENDIF

		cPO_Num = ALLTRIM(m.cnee_ref)
		cShip_ref = ALLTRIM(m.ship_ref)
		IF lMBOL
			SELECT mblgroup
			LOCATE FOR mblgroup.blship_ref = cShip_ref
			cSubBOL = ALLTRIM(mblgroup.subbol)
		ENDIF

		nPTCount = nPTCount+1

*!* Added this code to trap miscounts in OUTDET/SQL\
		WAIT WINDOW "Now summing outdet/SQL by outshipid" NOWAIT

		IF DATETIME()>dtTransferDT
			IF lPrepack
				IF SEEK(outship.outshipid,'ovpts','outshipid') OR cBOL = '04907315910258581'
					SET DELETED OFF
					cRefBOL = cBOL
				ELSE
					SET DELETED ON
				ENDIF
				SELECT outdet
				IF SET("Deleted") = "OFF"
					SUM origqty TO nUnitTot1 ;
						FOR !units ;
						AND outdet.outshipid = outship.outshipid ;
						AND outdet.wo_num = nWO_Num
				ELSE
					SUM totqty TO nUnitTot1 ;
						FOR !units ;
						AND outdet.outshipid = outship.outshipid ;
						AND outdet.wo_num = nWO_Num
				ENDIF
				SELECT vbillabongpp
				IF INLIST(nWO_Num,5049591)
					SUM (totqty/INT(VAL(PACK))) TO nUnitTot2 FOR vbillabongpp.outshipid = outship.outshipid
				ELSE
					COUNT TO nUnitTot2 FOR vbillabongpp.outshipid = outship.outshipid AND vbillabongpp.totqty > 0 AND vbillabongpp.ucc # "CUTS"
				ENDIF

				IF nUnitTot1<>nUnitTot2
					SET STEP ON
					ASSERT .F. MESSAGE "At SQL UNITQTY ERR"
					DO ediupdate WITH "SQL UNITQTY ERR-OSID "+TRANSFORM(outship.outshipid),.T.
					THROW
				ENDIF
			ELSE
*      ASSERT .f. MESSAGE "At OSID Qty scanning main loop"

				SELECT outdet
				SUM totqty TO nUnitTot1 ;
					FOR units ;
					AND outdet.outshipid = outship.outshipid ;
					AND outdet.wo_num = nWO_Num

				SELECT vbillabongpp
				SUM totqty TO nUnitTot2 ;
					FOR vbillabongpp.outshipid = outship.outshipid ;
					AND vbillabongpp.totqty > 0 ;
					AND vbillabongpp.ucc # 'CUTS'
				IF nUnitTot1<>nUnitTot2
					ASSERT .F. MESSAGE "At SQL TOTQTY ERR"
					SET STEP ON
					DO ediupdate WITH "SQLTQTY ERR-PIK, AT PT "+ALLTRIM(xxShipRef)+"-"+TRANSFORM(nUnitTot1)+"-"+TRANSFORM(nUnitTot2),.T.
					THROW
				ENDIF
			ENDIF
		ENDIF

		SELECT outdet
		SET ORDER TO outdetid
		LOCATE

*!* End code addition

		IF ("PENNEY"$UPPER(outship.consignee) OR "KMART"$UPPER(outship.consignee) OR "K-MART"$UPPER(outship.consignee) OR "AMAZON"$UPPER(outship.consignee))
			lApptFlag = .T.
		ELSE
			lApptFlag = .F.
		ENDIF

		IF lTestinput
			ddel_date = DATE()
			cApptNum = "99999"
			dapptdate = DATE()
		ELSE
			ddel_date = outship.del_date
			IF EMPTY(ddel_date)
				cMissDel = IIF(EMPTY(cMissDel),"The following PTs had no Delivery Dates:"+CHR(13)+TRIM(cShip_ref),cMissDel+CHR(13)+TRIM(cShip_ref))
			ENDIF

			cApptNum = ALLTRIM(outship.appt_num)

			IF lApptFlag AND EMPTY(cApptNum)
				DO ediupdate WITH "EMPTY AMAZON/APPT#, NO 945",.T.
				THROW
			ENDIF

			dapptdate = outship.appt

			IF EMPTY(dapptdate)
				dapptdate = outship.del_date
			ENDIF
		ENDIF

		IF ALLTRIM(outship.SForCSZ) = ","
			BLANK FIELDS outship.SForCSZ NEXT 1 IN outship
		ENDIF

		alength = ALINES(apt,outship.shipins,.T.,CHR(13))

		cTRNum = ""
		cPRONum = ""

*!* Removed condition of shipper either being Walmart or Amazon for PRO# transmission
		cKeyRec = ALLTRIM(outship.keyrec)
		cKeyRec = TRIM(STRTRAN(cKeyRec,"#",""))
		cPRONum = TRIM(STRTRAN(cKeyRec,"PR",""))
		IF lParcelType OR INLIST(scac,'UTTK','USTD','UTDC')
			cPRONum = ALLTRIM(outship.bol_no)
		ELSE
			IF EMPTY(cPRONum)
				DO ediupdate WITH "NO PRO# ENTERED, NO 945",.T.
				THROW
			ENDIF
		ENDIF

		nOutshipid = m.outshipid
		IF lParcelType
			cPTString = IIF(EMPTY(cPTString),m.consignee+" "+cBOL+" "+cShip_ref,cPTString+CHR(13)+m.consignee+" "+cBOL+" "+cShip_ref)
		ELSE
			cPTString = IIF(EMPTY(cPTString),m.consignee+" "+cShip_ref,cPTString+CHR(13)+m.consignee+" "+cShip_ref)
		ENDIF
		m.CSZ = TRIM(m.CSZ)
		DO CASE
			CASE m.CSZ = 'HAYWARD ,CA 94545'
				m.CSZ = 'HAYWARD, CA 94545'
			CASE m.CSZ = "CITY OF INDUSTRY ,CA 91745"
				m.CSZ = "CITY OF INDUSTRY, CA 91745"
			CASE m.CSZ = ","
				DO ediupdate WITH "MISSING CSZ",.T.
				THROW
			CASE !(", "$m.CSZ)
				DO ediupdate WITH "UNSPACED COMMA IN CSZ",.T.
				THROW
		ENDCASE

		IF EMPTY(M.CSZ)
			WAIT CLEAR
			WAIT WINDOW "No SHIP-TO City/State/ZIP info...exiting" TIMEOUT 1
			DO ediupdate WITH "NO CSZ ADDRESS INFO",.T.
			THROW
		ENDIF
		m.CSZ = ALLTRIM(STRTRAN(m.CSZ,"  "," "))
		nSpaces = OCCURS(" ",ALLTRIM(m.CSZ))
		IF nSpaces = 0
			WAIT WINDOW "SHIP-TO CSZ Segment Error" TIMEOUT 1
			DO ediupdate WITH "SHIP-TO CSZ ERR: "+cShip_ref,.T.
			THROW
		ELSE
			nCommaPos = AT(",",m.CSZ)
			nLastSpace = AT(" ",m.CSZ,nSpaces)
			IF ISALPHA(SUBSTR(TRIM(m.CSZ),AT(" ",m.CSZ,nSpaces-1)+1,2))
				cCity = LEFT(TRIM(m.CSZ),AT(",",m.CSZ)-1)
				cState = SUBSTR(m.CSZ,nCommaPos+2,2)
				cZip = TRIM(SUBSTR(m.CSZ,nLastSpace+1))
			ELSE
				WAIT CLEAR
				WAIT WINDOW "NOT ALPHA: "+SUBSTR(TRIM(m.CSZ),AT(" ",m.CSZ,nSpaces-1)+1,2) TIMEOUT 1
				cCity = LEFT(TRIM(m.CSZ),AT(",",m.CSZ)-1)
				cState = SUBSTR(TRIM(m.CSZ),AT(" ",m.CSZ,nSpaces-2)+1,2)
				cZip = TRIM(SUBSTR(m.CSZ,nLastSpace+1))
			ENDIF
		ENDIF

		STORE "" TO cSForCity,cSForState,cSForZip
		cStoreName = segmentget(@apt,"STORENAME",alength)
		IF !lFederated
			IF !EMPTY(M.SForCSZ)
				m.SForCSZ = ALLTRIM(STRTRAN(m.SForCSZ,"  "," "))
				nSpaces = OCCURS(" ",ALLTRIM(m.SForCSZ))
				IF nSpaces = 0
					m.SForCSZ = STRTRAN(m.SForCSZ,",","")
					cSForCity = ALLTRIM(m.SForCSZ)
					cSForState = ""
					cSForZip = ""
				ELSE
					nCommaPos = AT(",",m.SForCSZ)
					nLastSpace = AT(" ",m.SForCSZ,nSpaces)
					nMinusSpaces = IIF(nSpaces=1,0,1)
					IF ISALPHA(SUBSTR(TRIM(m.SForCSZ),AT(" ",TRIM(m.SForCSZ),nSpaces-nMinusSpaces)+1,2))
						cSForCity = LEFT(TRIM(m.SForCSZ),AT(",",m.SForCSZ)-1)
						cSForState = SUBSTR(m.SForCSZ,nCommaPos+2,2)
						cSForZip = ALLTRIM(SUBSTR(m.SForCSZ,nCommaPos+4))
						IF ISALPHA(cSForZip)
							cSForZip = ""
						ENDIF
					ELSE
						WAIT CLEAR
						WAIT WINDOW "NOT ALPHA: "+SUBSTR(TRIM(m.SForCSZ),AT(" ",m.SForCSZ,nSpaces-1)+1,2) TIMEOUT 1
						cSForCity = LEFT(TRIM(m.SForCSZ),AT(",",m.SForCSZ)-1)
						cSForState = SUBSTR(TRIM(m.SForCSZ),AT(" ",m.SForCSZ,nSpaces-2)+1,2)
						cSForZip = ALLTRIM(m.SForCSZ)
						IF ISALPHA(cSForZip)
							cSForZip = ""
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ELSE
			STORE "" TO cCity,cState,cZip
		ENDIF


		DO num_incr_st
		WAIT CLEAR
		WAIT WINDOW "Now creating Line Item information" NOWAIT NOCLEAR

		INSERT INTO temp945 (accountid,isa_num,st_num,wo_num,bol_no,ship_ref,filename,filedate) ;
			VALUES (m.accountid,c_CntrlNum,c_GrpCntrlNum,m.wo_num,m.bol_no,m.ship_ref,cFilenameShort,dt2)

		STORE "ST"+cfd+"945"+cfd+PADL(c_GrpCntrlNum,9,"0")+csegd TO cString
		DO cstringbreak
		nSTCount = nSTCount + 1
		nSegCtr = 1

		cSalesOrder = segmentget(@apt,"SALESORDER",alength)
		nHdr = 1
		nLine = 0
		STORE "W06"+cfd+"J"+cfd+cShip_ref+cfd+cDate+cfd+RIGHT(ALLTRIM(cWO_Num),7)+cfd+cfd+cPO_Num+cfd+cSalesOrder+cfd+"000000"+cfd+cfd+cfd+cfd+"51"+csegd TO cString
		DO cstringbreak
		nSegCtr = nSegCtr + 1
		nCtnNumber = 1  && Seed carton sequence count

		nHdr = 2
		cStoreNum = segmentget(@apt,"STORENUM",alength)
		STORE "N1"+cfd+"ST"+cfd+ALLTRIM(m.consignee)+cfd+"93"+cfd+cStoreNum+csegd TO cString
		DO cstringbreak
		nSegCtr = nSegCtr + 1
		IF EMPTY(M.ADDRESS2)
			STORE "N3"+cfd+ALLTRIM(M.ADDRESS)+csegd TO cString
		ELSE
			STORE "N3"+cfd+ALLTRIM(M.ADDRESS)+cfd+ALLTRIM(M.ADDRESS2)+csegd TO cString
		ENDIF
		DO cstringbreak
		nSegCtr = nSegCtr + 1

		cCountry = segmentget(@apt,"COUNTRY",alength)
		cCountry = ALLT(cCountry)
		IF EMPTY(cCountry)
			cCountry = "US"
		ENDIF
		STORE "N4"+cfd+cCity+cfd+cState+cfd+cZip+cfd+cCountry+csegd TO cString
		DO cstringbreak
		nSegCtr = nSegCtr + 1

		nHdr=3
		cRegion = segmentget(@apt,"REGION",alength)
		STORE "N1"+cfd+"UD"+cfd+ALLTRIM(cRegion)+csegd TO cString
		DO cstringbreak
		nSegCtr = nSegCtr + 1

		IF !EMPTY(ALLTRIM(m.shipfor))
			nHdr = 4
			STORE "N1"+cfd+"Z7"+cfd+ALLTRIM(m.shipfor)+cfd+"92"+cfd+ALLTRIM(m.sforstore)+csegd TO cString
			DO cstringbreak
			nSegCtr = nSegCtr + 1
			IF EMPTY(M.ADDRESS2)
				STORE "N3"+cfd+ALLTRIM(M.SFORADDR1)+csegd TO cString
			ELSE
				STORE "N3"+cfd+ALLTRIM(M.SFORADDR1)+cfd+ALLTRIM(M.SFORADDR2)+csegd TO cString
			ENDIF
			DO cstringbreak
			nSegCtr = nSegCtr + 1
			IF !EMPTY(cSForState)
				STORE "N4"+cfd+cSForCity+cfd+cSForState+cfd+cSForZip+cfd+"US"+csegd TO cString
			ELSE
				STORE "N4"+cfd+cSForCity+csegd TO cString
			ENDIF
			DO cstringbreak
			nSegCtr = nSegCtr + 1
		ENDIF

		cEDIShipTo = segmentget(@apt,"EDISHIPTO",alength)
		IF !EMPTY(cEDIShipTo)
			nHdr = 5
			STORE "N9"+cfd+"ST"+cfd+ALLTRIM(cEDIShipTo)+csegd TO cString
			DO cstringbreak
			nSegCtr = nSegCtr + 1
		ENDIF

		nHdr = 6
		cBrand = segmentget(@apt,"BRAND",alength)
		STORE "N9"+cfd+"3L"+cfd+ALLTRIM(cBrand)+csegd TO cString
		DO cstringbreak
		nSegCtr = nSegCtr + 1

		cCarrAcct = segmentget(@apt,"CARRACCT",alength)

		nHdr = 6
		cPTType = segmentget(@apt,"PTTYPE",alength)
		IF cPTType != "NA"
			STORE "N9"+cfd+"PHC"+cfd+ALLTRIM(cPTType)+csegd TO cString
			DO cstringbreak
			nSegCtr = nSegCtr + 1
		ENDIF

		IF !EMPTY(upsacctnum)
			STORE "N9"+cfd+"AAO"+cfd+ALLTRIM(upsacctnum)+csegd TO cString
			DO cstringbreak
			nSegCtr = nSegCtr + 1
		ENDIF

		nHdr = 7
		IF lMBOL
			STORE "N9"+cfd+"MB"+cfd+ALLTRIM(cBOL)+csegd TO cString
			DO cstringbreak
			nSegCtr = nSegCtr + 1
		ENDIF

		STORE "N9"+cfd+"BM"+cfd+IIF(lMBOL,ALLTRIM(cSubBOL),ALLTRIM(cBOL))+csegd TO cString
		DO cstringbreak
		nSegCtr = nSegCtr + 1

*    cDept = segmentget(@apt,"DEPT",alength)
&&    If !Empty(Alltrim(m.keyrec)) And m.keyrec#"00000"
		IF !lParcelType  && added TMARG 09/13/17 to bypass N9*CN for parcel
			STORE "N9"+cfd+"CN"+cfd+cPRONum+csegd TO cString
			DO cstringbreak
			nSegCtr = nSegCtr + 1
		ENDIF
&&    Endif

		IF !EMPTY(ALLTRIM(outship.appt_num))
			STORE "N9"+cfd+"BX"+cfd+ALLTRIM(outship.appt_num)+csegd TO cString
			DO cstringbreak
			nSegCtr = nSegCtr + 1
		ENDIF

		nHdr = 8
		IF outship.outshipid = 2298698
			SET STEP ON
		ENDIF

		IF INLIST(TRIM(outship.scac),'USTD','UTTK','UTDC') AND !EMPTY(cLTLBOL) && cLTLBOL = ctnucc.bol for trade direct
			STORE "N9"+cfd+"CNS"+cfd+ALLTRIM(cLTLBOL)+csegd TO cString
			DO cstringbreak
			nSegCtr = nSegCtr + 1
		ENDIF

		STORE "G62"+cfd+"TA"+cfd+DTOS(m.ptdate)+csegd TO cString
		DO cstringbreak
		nSegCtr = nSegCtr + 1

***added TMARG 7/19/17 del date
		nHdr = 9
		STORE "G62"+cfd+"86"+cfd+DTOS(m.del_date)+csegd TO cString
		DO cstringbreak
		nSegCtr = nSegCtr + 1

		lcPTType =segmentget(@apt,"PTTYPE",alength)

		IF lParcelType
			DO CASE
				CASE outship.terms = "BS"
					cBilling = "PP"
				CASE outship.terms = "BTP"
					cBilling = "TP"
				CASE outship.terms = "BR"
					cBilling = "CC"
				OTHERWISE
					cBilling = "PP"
			ENDCASE
		ENDIF

		IF lParcelType
			IF lTestinput
				cFreight = "2750"
			ELSE
				SELECT shipment
*        ASSERT .F. MESSAGE "At UPS billing and  freight calculation"

				SELECT shipment
				LOCATE FOR pickticket = cShip_ref
				IF !FOUND()
					WAIT WINDOW "PT# "+cShip_ref+" not found in shipment" TIMEOUT 1
					DO ediupdate WITH "PT not in Shipment",.T.
					THROW
				ENDIF

				SCAN FOR pickticket = cShip_ref
					cBilling1 = ALLTRIM(shipment.billing)
					cBilling1 = IIF(EMPTY(cBilling1),cBilling,cBilling1)
					cSMP = ICASE(cBilling1 = "COL","CC",cBilling1 = "PPD","PP",INLIST(cBilling1,"B3P","STP"),"TP","PU")
					SELECT package
					SUM package.pkgcharge TO nFreight FOR package.shipmentid = shipment.shipmentid
					cFreight = STRTRAN(ALLTRIM(STR(nFreight,10,2)),".","")
				ENDSCAN

				nHdr = 10
				STORE "W27"+cfd+"M"+cfd+ALLTRIM(m.scac)+cfd+ALLTRIM(m.ship_via)+cfd+cSMP+REPLICATE(cfd,5)+IIF(lLTL,"LT","PS")+csegd TO cString
				DO cstringbreak
				nSegCtr = nSegCtr + 1
			ENDIF

			IF !lTestinput AND lParcelType
				SELECT shipment
				=SEEK(cShip_ref,"shipment","pickticket")
				WAIT WINDOW "PT running: "+ALLTRIM(shipment.pickticket) NOWAIT
				SELECT shipmentid,uspstrknumber,trknumber,ucc,ref4 FROM package INTO CURSOR package1 READWRITE
				SELECT shipment
				SCAN FOR shipment.pickticket = cShip_ref
					SELECT shipmentid,uspstrknumber,trknumber,ucc,ref4 FROM package WHERE package.shipmentid=shipment.shipmentid INTO CURSOR package2
					SELECT package1
					APPEND FROM DBF('package2')
				ENDSCAN
				LOCATE
*        BROWSE
*!*          ELSE
*!*            SELECT shipmentid,trknumber,ucc,ref4 FROM package WHERE package.shipmentid=shipment.shipmentid INTO CURSOR package1 READWRITE
			ENDIF

			nHdr = 11
			STORE "G72"+cfd+"504"+cfd+"15"+REPLICATE(cfd,6)+ALLT(cFreight)+csegd TO cString
			DO cstringbreak
			nSegCtr = nSegCtr + 1
		ELSE
			cSMP = ICASE(cSCAC = 'USTD','PP',cSCAC = 'UTTK','PU','CC')
*      STORE "W27"+cfd+"M"+cfd+ALLTRIM(m.scac)+cfd+ALLTRIM(m.ship_via)+cfd+cSMP+csegd TO cString
***  Added TMARG 8/1/17 two lines below to capture W27 09
			xLTL = ICASE(cSCAC = 'USTD','PS',cSCAC = 'UTDC','PS',cSCAC = 'UTTK','LT', lParcelType = .F.,'LT','PS')
			STORE "W27"+cfd+"M"+cfd+ALLTRIM(m.scac)+cfd+ALLTRIM(m.ship_via)+cfd+cSMP+REPLICATE(cfd,5)+xLTL+csegd TO cString
			DO cstringbreak
			nSegCtr = nSegCtr + 1
		ENDIF

*************************************************************************
*2  DETAIL LOOP - MATCHING SHIP_REF FIELD (FROM OUTSHIP)
*************************************************************************
*    ASSERT .F. MESSAGE "In Detail Loop"
		STORE "LB" TO cWeightUnit
		WAIT CLEAR
		WAIT WINDOW "Now creating Detail information "+cShip_ref NOWAIT NOCLEAR
		SELECT vbillabongpp
		LOCATE
		SELECT outdet
		SET ORDER TO TAG outdetid

		IF cBOL = cRefBOL
			SET DELETED OFF
		ELSE
			SET DELETED ON
		ENDIF
		SELECT vbillabongpp
		SET RELATION TO outdetid INTO outdet

		LOCATE
		LOCATE FOR ship_ref = TRIM(cShip_ref) AND outshipid = nOutshipid
		IF !FOUND()
			IF !lTesting
				WAIT WINDOW "PT "+cShip_ref+" NOT FOUND in vbillabongpp...ABORTING" TIMEOUT 1
				IF !lTesting
					lSQLMail = .T.
				ENDIF
				DO ediupdate WITH "MISS PT-SQL: "+cShip_ref,.T.
				THROW
			ELSE
				LOOP
			ENDIF
		ENDIF

		scanstr = "vbillabongpp.ship_ref = cShip_ref and vbillabongpp.outshipid = nOutshipid"
		SELECT vbillabongpp
		LOCATE
		LOCATE FOR &scanstr
		cCartonNum= "XXX"

** DHW DEBUG 1
		IF lTesting
			SET STEP ON
		ENDIF
** DHW DEBUG 1


		DO WHILE &scanstr
			lDoManSegment = .T.
			lDoPALSegment = .T.
			lSkipBack = .T.

			IF TRIM(vbillabongpp.ucc) <> cCartonNum
				STORE TRIM(vbillabongpp.ucc) TO cCartonNum
			ENDIF

			STORE "LX"+cfd+ALLTRIM(STR(nCtnNumber))+csegd TO cString   && Carton Seq. # (up to Carton total)
			DO cstringbreak
			nSegCtr = nSegCtr + 1

			cDoString = "vbillabongpp.ucc = cCartonNum"

			DO WHILE &cDoString
**DHW DEBUG 2
				IF lTesting
					SET STEP ON
				ENDIF
**DHW DEBUG 2

				alength = ALINES(aptdet,outdet.printstuff,.T.,CHR(13))

				IF vbillabongpp.totqty < 1
					SKIP 1 IN vbillabongpp
					LOOP
				ENDIF

				cUCCNumber = vbillabongpp.ucc
*!*          nvRec=RECNO()
*!*
*!*          SUM vbillabongpp.ctnwt TO nPALWt FOR vbillabongpp.ucc = cUCCNumber
*!*          GO nvRec
				IF EMPTY(cUCCNumber) OR ISNULL(cUCCNumber)
					WAIT CLEAR
					WAIT WINDOW "Empty UCC Number in vbillabongpp "+cShip_ref TIMEOUT 1
					lSQLMail = .T.
					DO ediupdate WITH "EMPTY UCC# in "+cShip_ref,.T.
					THROW
				ENDIF
				cUCCNumber = TRIM(STRTRAN(TRIM(cUCCNumber)," ",""))

				IF lDoManSegment
					lDoPALSegment = .T.
					DO CASE
						CASE lParcelType
							IF lTestinput
								cTrkNumber = ALLTRIM(outship.bol_no)
							ELSE
								SELECT package1
*!* Added this block to capture multiple trk. #s without accompanying UCC #s, Joe 07.17.2017
								WAIT WINDOW ALLTRIM(STR(RECCOUNT())) NOWAIT
								COUNT TO nUCCx FOR !EMPTY(package1.ucc)
								COUNT TO nRef4x FOR !EMPTY(package1.ref4)
								lNoCount = IIF((nUCCx = 0 AND nRef4x = 0),.T.,.F.)
								RELEASE nUCCx,nRef4x
*
								LOCATE
								IF lNoCount
									GO nCtnNumber
									cTrkNumber = ALLTRIM(package1.trknumber)
*!* End added code block
								ELSE
									LOCATE FOR  package1.ref4 = cUCCNumber
									IF !FOUND()
										LOCATE FOR  package1.ucc = cUCCNumber
										IF FOUND()
											cTrkNumber = ALLTRIM(package1.trknumber)
										ELSE
											=SEEK(cShip_ref,"shipment","pickticket")
											LOCATE FOR package1.shipmentid = shipment.shipmentid
											IF FOUND()
												cTrkNumber = ALLTRIM(package1.trknumber)
											ELSE
												DO ediupdate WITH "Missing TRK # at "+cShip_ref,.T.
												THROW
											ENDIF
										ENDIF
									ELSE
										cTrkNumber = ALLTRIM(package1.trknumber)
									ENDIF
								ENDIF
							ENDIF
							nCtnNumber = nCtnNumber + 1

						CASE lLTL OR INLIST(TRIM(outship.scac),'USTD','UTTK','UTDC')
							SELECT tempctnucc
							LOCATE FOR tempctnucc.ucc = cUCCNumber
*
							IF outship.scac = 'UTTK' && Trade Direct LTL, no trk# required
								cTrkNumber = "NA" &&ALLTRIM(tempctnucc.subpro)
							ELSE
								cTrkNumber = ALLTRIM(tempctnucc.serialno)
							ENDIF
							SELECT vbillabongpp

							IF EMPTY(cTrkNumber)
								DO ediupdate WITH "No TRK# in CTNUCC for TD: "+cUCCNumber,.T.
								THROW
							ENDIF

						OTHERWISE
							cTrkNumber = cBOL

					ENDCASE
					lDoManSegment = .F.

****added  TMARG 7/19/17 to change TD scacs to   change lParcelType to .T.
&&    ORIGPTYPE=lParcelType
&&&   lParcelType =  IIF(INLIST(scac,'USTD','UTTK','UTDC'),.T., lParcelType)

					IF   INLIST(scac,'USTD','UTDC') OR lParcelType
						STORE "MAN"+cfd+"GM"+cfd+TRIM(cUCCNumber)+cfd+cfd+"CP"+cfd+cTrkNumber+csegd TO cString
					ELSE
						STORE "MAN"+cfd+"GM"+cfd+TRIM(cUCCNumber)+csegd TO cString
					ENDIF

&&        STORE "MAN"+cfd+"GM"+cfd+TRIM(cUCCNumber)+IIF(!lParcelType OR EMPTY(cTrkNumber) Or cTrkNumber="NA" ,csegd ,cfd+cfd+"CP"+cfd+cTrkNumber+csegd) TO cString  && SCC-14 Number (Wal-mart spec)

&&& SET PARCEL TYPE BACK TO ORIGINAL
&&          lParcelType =ORIGPTYPE

					DO cstringbreak
					nSegCtr = nSegCtr + 1
					nTotCtnCount = nTotCtnCount + 1

&& Added this block to take ctn wt from SQL data, per Todd, 07.07.2017, Joe
					IF lTestinput
						nCtnWt = 5
						cCtnWt = '5'
						nTotCtnWt = nTotCtnWt+nCtnWt
					ELSE

						SELECT vbillabongpp
						LOCATE FOR ucc = cUCCNumber
						IF FOUND()
							nCtnWt = vbillabongpp.ctnwt
							cCtnWt = ALLTRIM(STR(nCtnWt,10,1))
							IF nCtnWt = 0 OR ISNULL(nCtnWt)
								nCtnWt = outdet.ctnwt
								cCtnWt = ALLTRIM(STR(nCtnWt,10,1))
								IF nCtnWt = 0 OR ISNULL(nCtnWt)
									nCtnWt = outship.weight/outship.ctnqty
									cCtnWt = ALLTRIM(STR(nCtnWt,10,1))
									IF nCtnWt = 0 OR ISNULL(nCtnWt)
										DO ediupdate WITH "MISS CTN WT: PT "+cShip_ref,.T.
										THROW
									ENDIF
								ENDIF
							ENDIF
							nTotCtnWt = nTotCtnWt+nCtnWt
						ELSE
							nCtnWt = outdet.ctnwt
							cCtnWt = ALLTRIM(STR(nCtnWt,10,1))
							IF nCtnWt = 0 OR ISNULL(nCtnWt)
								nCtnWt = outship.weight/outship.ctnqty
								cCtnWt = ALLTRIM(STR(cCtnWt,10,1))
								IF nCtnWt = 0 OR ISNULL(nCtnWt)
									DO ediupdate WITH "MISS CTN WT: PT "+cShip_ref,.T.
									THROW
								ENDIF
							ENDIF
							nTotCtnWt = nTotCtnWt+nCtnWt
						ENDIF
					ENDIF
				ENDIF

				WAIT WINDOW "OD OUTDETID: "+TRANSFORM(outdet.outdetid)+CHR(13)+"V- OUTDETID: "+TRANSFORM(vbillabongpp.outdetid) NOWAIT
				IF vbillabongpp.outdetid = 18919526
					ASSERT .F. MESSAGE "At ODID 18919526..."
				ENDIF
				cColor = TRIM(outdet.COLOR)
				cSize = TRIM(outdet.ID)
				cUPC = TRIM(outdet.upc)
				IF (ISNULL(cUPC) OR EMPTY(cUPC))
					cUPC = TRIM(vbillabongpp.upc)
				ENDIF
				cLineNum = ALLTRIM(outdet.linenum)
				cStyle = TRIM(outdet.STYLE)
				cItemNum = TRIM(outdet.custsku)

				nODID = outdet.outdetid

				IF EMPTY(cLineNum)
					SET STEP ON
					DO ediupdate WITH "EMPTY LINE #s-ODID "+TRANSFORM(nODID),.T.
					THROW
				ENDIF

				nShipDetQty = vbillabongpp.totqty
				IF ISNULL(nShipDetQty)
					nShipDetQty = outdet.totqty
				ENDIF
				IF DATETIME()<dtTransferDT
					nShipDetQty = outdet.totqty
				ENDIF
				nUnitsum = nUnitsum + nShipDetQty

				nOrigDetQty = vbillabongpp.qty
				IF ISNULL(nOrigDetQty)
					nOrigDetQty = nShipDetQty
				ENDIF

				IF lDoPALSegment
					cPALWt = ALLTRIM(STR(nCtnWt,10,1))
					STORE "PAL"+REPLICATE(cfd,5)+cPALWt+cfd+cWeightUnit+csegd TO cString
					DO cstringbreak
					nSegCtr = nSegCtr + 1
					lDoPALSegment = .F.
				ENDIF

*!* Changed the following to utilize original 940 unit codes from Printstuff field
				cUnitCode = TRIM(segmentget(@aptdet,"UNITCODE",alength))
				IF EMPTY(cUnitCode)
					cUnitCode = "EA"
				ENDIF
				nShipStat = "CL"

*!* Added the following per Todd and Claudia @ BBG; we are using that quantity for all cartons. Added 08.16.2017, Joe
				nOrigDetQty = INT(VAL(TRIM(segmentget(@aptdet,"ORIGQTY",alength))))

				nLine = 1
				STORE "W12"+cfd+nShipStat+cfd+ALLTRIM(STR(nOrigDetQty))+cfd+ALLTRIM(STR(nShipDetQty))+cfd+cfd+cUnitCode+cfd+cfd+"UP"+cfd+TRIM(cUPC)+csegd TO cString
				DO cstringbreak
				nSegCtr = nSegCtr + 1

				nLine = 2
				STORE "N9"+cfd+"LI"+cfd+cLineNum+csegd TO cString
				DO cstringbreak
				nSegCtr = nSegCtr + 1

				nLine = 3
				STORE "N9"+cfd+"IX"+cfd+cStyle+csegd TO cString
				DO cstringbreak
				nSegCtr = nSegCtr + 1

				nLine = 4
				STORE "N9"+cfd+"CO"+cfd+ALLTRIM(outdet.COLOR)+csegd TO cString
				DO cstringbreak
				nSegCtr = nSegCtr + 1

				nLine = 5
				STORE "N9"+cfd+"IZ"+cfd+ALLTRIM(outdet.ID)+csegd TO cString
				DO cstringbreak
				nSegCtr = nSegCtr + 1

*!*          Moved to header per Todd, 07.07.2017, Joe
*!*          IF lLTL
*!*            STORE "N9"+cfd+"CNS"+cfd+cLTLBOL+csegd TO cString
*!*            DO cstringbreak
*!*            nSegCtr = nSegCtr + 1
*!*          ENDIF

* PO1|4|||19.95||IN|M215JHCW|BO|DEK|||OT|979012|ON|279012|PO|116038

				nLine = 6
				IF outdet.outdetid = 12497523
					ASSERT .F.
				ENDIF

				IF INLIST(lcPTType,"CON","WEB")
					alength = ALINES(aptdet,outdet.printstuff,.T.,CHR(13))
					IF lTesting
						WAIT WINDOW "At ODID: "+TRANSFORM(outdet.outdetid) NOWAIT
					ENDIF
					lcLinePT = segmentget(@aptdet,"LINECUSTPT",alength)
					lcLineORD = segmentget(@aptdet,"LINECUSTORD",alength)
					lcLinePO= segmentget(@aptdet,"LINECUSTPO",alength)

					STORE "N9"+cfd+"OT"+cfd+ALLTRIM(lcLinePT)+csegd TO cString
					DO cstringbreak
					nSegCtr = nSegCtr + 1
					STORE "N9"+cfd+"ON"+cfd+ALLTRIM(lcLineORD)+csegd TO cString
					DO cstringbreak
					nSegCtr = nSegCtr + 1
					STORE "N9"+cfd+"PO"+cfd+ALLTRIM(lcLinePO)+csegd TO cString
					DO cstringbreak
					nSegCtr = nSegCtr + 1
				ENDIF

				lDoExit = .F.
				IF !EOF()
					SKIP 1 IN vbillabongpp
				ELSE
					lDoExit = .T.
					EXIT
				ENDIF
			ENDDO

			lSkipBack = .T.

			IF lDoExit
				EXIT
			ENDIF

*      ASSERT .F. MESSAGE "At end of detail looping-debug"
		ENDDO

*************************************************************************
*2^  END DETAIL MATCHING SHIP_REF LOOP
*************************************************************************
		WAIT CLEAR
		WAIT WINDOW "Now creating Section Closure information" NOWAIT NOCLEAR
		STORE "W03"+cfd+ALLTRIM(STR(nUnitsum))+cfd+ALLTRIM(STR(nTotCtnWt,10,1))+cfd+;
			cWeightUnit+cfd+ALLTRIM(STR(outship.cuft,10,2))+cfd+"CF"+cfd+ALLTRIM(STR(nTotCtnCount))+cfd+"CT"+csegd TO cString   && Units sum, Weight sum, carton count

		nTotCtnWt = 0
		nTotCtnCount = 0
		nUnitsum = 0
		DO cstringbreak

		STORE  "SE"+cfd+ALLTRIM(STR(nSegCtr+2))+cfd+PADL(c_GrpCntrlNum,9,"0")+csegd TO cString
		DO cstringbreak

		SELECT outship
		WAIT CLEAR
	ENDSCAN


*************************************************************************
*1^  END OUTSHIP MAIN LOOP
*************************************************************************

	DO close945
	=FCLOSE(nFilenum)
*  ASSERT .F. MESSAGE "At end of main loop...debug here"
	IF !lTesting
		SELECT edi_trigger
		DO ediupdate WITH "945 CREATED",.F.
		SELECT edi_trigger
		LOCATE

		IF !USED("ftpedilog")
			SELECT 0
			USE F:\edirouting\ftpedilog ALIAS ftpedilog
			INSERT INTO ftpedilog (TRANSFER,ftpdate,filename,acct_name,TYPE) VALUES ("945-"+ccustname+"-"+cCustLoc,dt2,cFilenameHold,UPPER(ccustname),"945")
			USE IN ftpedilog
		ENDIF
	ENDIF

	WAIT CLEAR
*  WAIT WINDOW cCustFolder+" 945 Process complete..." TIMEOUT 1

*!* Create eMail confirmation message
	cOutFolder = "FMI"+cCustLoc

	tsubject = cMailName+" 945 EDI File from TGF as of "+dtmail+" ("+cCustLoc+")"
	tattach = " "
	tmessage = "945 EDI Info from TGF, file "+cFilenameShort+CHR(13)
	tmessage = tmessage + "Division "+cDivision+", BOL# "+TRIM(cBOL)+CHR(13)
	tmessage = tmessage + "For Work Orders: "+cWO_NumStr+","+CHR(13)
	tmessage = tmessage + "containing these "+TRANSFORM(nPTCount)+" picktickets:"+CHR(13)
	tmessage = tmessage + cPTString + CHR(13)
	tmessage = tmessage +"has been created and will be transmitted ASAP."+CHR(13)+CHR(13)
	IF !EMPTY(cMissDel)
		tmessage = tmessage+CHR(13)+CHR(13)+cMissDel+CHR(13)+CHR(13)
	ENDIF
	tmessage = tmessage + "If you have any questions, please eMail or call 732-750-9000 x217."

	IF lEmail
		DO FORM m:\dev\frm\dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
	ENDIF
	nPTCount = 0

	WAIT CLEAR
	WAIT WINDOW cMailName+" 945 EDI File output complete" TIMEOUT 1

*!* Transfers files to correct output folders
	COPY FILE [&cFilenameHold] TO [&cFilenameArch]

	IF lBBGFilesOut
		COPY FILE [&cFilenameHold] TO [&cFilenameOut]
		DELETE FILE [&cFilenameHold]
	ENDIF

	IF !lTesting
		SELECT temp945
		COPY TO "f:\3pl\data\temp945bbg.dbf"
		USE IN temp945
		SELECT 0
		USE "f:\3pl\data\temp945bbg.dbf" ALIAS temp945bbg
		SELECT 0
		USE "f:\3pl\data\pts_sent945.dbf" ALIAS pts_sent945
		APPEND FROM "f:\3pl\data\temp945bbg.dbf"
		USE IN pts_sent945
		USE IN temp945bbg
		DELETE FILE "f:\3pl\data\temp945bbg.dbf"
	ENDIF
	lDoCatch = .F.

	IF !lTesting
*  asn_out_data()
	ENDIF

	RELEASE ALL LIKE c_CntrlNum,c_GrpCntrlNum

CATCH TO oErr
	IF lDoCatch
*    ASSERT .F. MESSAGE "In error CATCH..."
		SET STEP ON
		lEmail = .F.
		DO ediupdate WITH UPPER(oErr.MESSAGE),.T.
		tsubject = ccustname+" Error ("+IIF(EMPTY(cErrMsg),TRANSFORM(oErr.MESSAGE),cErrMsg)+") at "+TTOC(DATETIME())
		tattach  = ""
		tsendto  = "pgaidis@fmiint.com"&&tsendtoerr
		tcc = tccerr

		tmessage = ccustname+" Error processing "+CHR(13)

		tmessage =tmessage+ "Try/CATCH Exeception Message:"+CHR(13)+;
			[  Error: ] + STR(oErr.ERRORNO) +CHR(13)+;
			[  LineNo: ] + STR(oErr.LINENO) +CHR(13)+;
			[  Message: ] + oErr.MESSAGE +CHR(13)+;
			[  Procedure: ] + oErr.PROCEDURE +CHR(13)+;
			[  Details: ] + oErr.DETAILS +CHR(13)+;
			[  StackLevel: ] + STR(oErr.STACKLEVEL) +CHR(13)+;
			[  LineContents: ] + oErr.LINECONTENTS+CHR(13)+;
			[  UserValue: ] + oErr.USERVALUE
		tmessage = tmessage + CHR(13)+"PROGRAM: "+cProgname
		IF !EMPTY(cStatus)
			tmessage = tmessage + CHR(13)+"Error: "+cStatus
		ENDIF
		tsubject = "945 EDI Poller Error at "+TTOC(DATETIME())
		tattach  = ""
		tfrom    ="TGF EDI 945 Poller Operations <transload-ops@tollgroup.com>"
		DO FORM m:\dev\frm\dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
	ENDIF
ENDTRY

*** END OF CODE BODY

****************************
PROCEDURE close945
****************************
** Footer Creation

	STORE  "GE"+cfd+ALLTRIM(STR(nSTCount))+cfd+c_CntrlNum+csegd TO cString
	DO cstringbreak

	STORE  "IEA"+cfd+"1"+cfd+PADL(c_CntrlNum,9,"0")+csegd TO cString
	DO cstringbreak
ENDPROC

****************************
PROCEDURE num_incr_isa
****************************

	IF !USED("serfile")
		USE ("f:\3pl\data\serial\"+ccustname+"945_serial") IN 0 ALIAS serfile
	ENDIF
	SELECT serfile
	IF lTesting AND lISAFlag
*    ASSERT .F. MESSAGE "At group seqnum...debug"
		nOrigSeq = serfile.seqnum
		lISAFlag = .F.
	ENDIF
	nISA_Num = serfile.seqnum
	c_CntrlNum = ALLTRIM(STR(serfile.seqnum))
	REPLACE serfile.seqnum WITH serfile.seqnum + 1 IN serfile
	SELECT outship
ENDPROC

****************************
PROCEDURE num_incr_st
****************************

	IF !USED("serfile")
		USE ("f:\3pl\data\serial\"+ccustname+""945_serial") IN 0 ALIAS serfile
	ENDIF
	SELECT serfile
	IF lTesting AND lSTFlag
		nOrigGrpSeq = serfile.grpseqnum
		lSTFlag = .F.
	ENDIF
	c_GrpCntrlNum = ALLTRIM(STR(serfile.grpseqnum))
	REPLACE serfile.grpseqnum WITH serfile.grpseqnum + 1 IN serfile
	SELECT outship
ENDPROC


****************************
PROCEDURE segmentget
****************************

	PARAMETER thisarray,lcKey,nLength

	FOR i = 1 TO nLength
		IF i > nLength
			EXIT
		ENDIF
		lnEnd= AT("*",thisarray[i])
		IF lnEnd > 0
			lcThisKey =TRIM(SUBSTR(thisarray[i],1,lnEnd-1))
			IF OCCURS(lcKey,lcThisKey)>0
				RETURN SUBSTR(thisarray[i],lnEnd+1)
				i = 1
			ENDIF
		ENDIF
	ENDFOR

	RETURN ""

****************************
PROCEDURE ediupdate
****************************
	PARAMETER cStatus,lIsError
*  lDoCatch = IIF(lIsError,.T.,.F.)
	lDoCatch = .F.
	IF !lTesting
		SELECT edi_trigger
		IF !lIsError
			IF cStatus = "JILDOR NO 945"
				REPLACE processed WITH .T.,proc945 WITH .F.,file945 WITH "",comments WITH comments+CHR(13)+"No 945 per Highline";
					fin_status WITH "NO 945 NEEDED",errorflag WITH .F.,when_proc WITH DATETIME() ;
					FOR edi_trigger.bol = cBOL AND INLIST(accountid,&gbillabongaccts)
			ELSE
				REPLACE processed WITH .T.,proc945 WITH .T.,file945 WITH cFilenameHold,isa_num WITH cISA_Num,;
					fin_status WITH "945 CREATED",errorflag WITH .F.,when_proc WITH DATETIME() ;
					FOR edi_trigger.bol = cBOL AND INLIST(accountid,&gbillabongaccts)
			ENDIF
		ELSE
			REPLACE processed WITH .T.,proc945 WITH .F.,file945 WITH "",;
				fin_status WITH cStatus,errorflag WITH .T. ;
				FOR edi_trigger.bol = cBOL AND INLIST(accountid,&gbillabongaccts)
			IF lCloseOutput
				=FCLOSE(nFilenum)
				ERASE &cFilenameHold
			ENDIF
		ENDIF
	ENDIF

	IF lIsError AND lEmail && AND cStatus # "SQL ERROR"
		tsubject = "945 Error in Billabong BOL "+TRIM(cBOL)+"(At PT "+cShip_ref+")"
		tattach = " "
		tsendto = ALLTRIM(tsendtoerr)
		tcc = ALLTRIM(tccerr)
		tmessage = "945 Processing for BOL# "+cBOL+", WO# "+cWO_Num+" produced this error: (Office: "+cOffice+"): "+cStatus+CHR(13)+"Check EDI_TRIGGER and re-run"
		IF "TOTQTY ERR"$cStatus
			tmessage = tmessage + CHR(13) + "At OUTSHIPID: "+ALLTRIM(STR(m.outshipid))
		ENDIF
		DO FORM m:\dev\frm\dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
	ENDIF

	IF USED('outship')
		USE IN outship
	ENDIF
	IF USED('outdet')
		USE IN outdet
	ENDIF
	IF USED('shipment')
		USE IN shipment
	ENDIF
	IF USED('package')
		USE IN package
	ENDIF

	IF USED('serfile')
		SELECT serfile
		IF lTesting
			IF nOrigSeq > 0
				REPLACE serfile.seqnum WITH nOrigSeq
			ENDIF
			IF nOrigGrpSeq > 0
				REPLACE serfile.grpseqnum WITH nOrigGrpSeq
			ENDIF
		ENDIF
		USE IN serfile
	ENDIF
	IF USED('scacs')
		USE IN scacs
	ENDIF
	IF USED('mm')
		USE IN mm
	ENDIF
	IF USED('tempx')
		USE IN tempx
	ENDIF
	IF USED('vbillabongpp')
		USE IN vbillabongpp
	ENDIF

	IF !lTesting
		SELECT edi_trigger
		LOCATE
	ENDIF
	IF lIsError
	ENDIF
ENDPROC

****************************
PROCEDURE cstringbreak
****************************
	cLen = LEN(ALLTRIM(cString))
	IF cLen = 0 OR ISNULL(cString)
		SET STEP ON
	ENDIF

*Fputs(nFilenum,cString)
	FWRITE(nFilenum,cString)
ENDPROC
