*!* Nautica 945 (Whse. Shipping Advice) Creation PROGRAM
*!* Creation Date: 05.11.2012 by PG (Derived from J Queen)

Parameters cOffice,cBOL,nAcctNum
Wait Window "Now at start of Nautica 945 process" Nowait &&TIMEOUT 1

Public Array thisarray(1)
Public c_CntrlNum,c_GrpCntrlNum,cReturned,cWO_Num,cWO_NumStr,nWO_Num1,lTesting,tsendtotest,tcctect,lTestinput
Public lEmail,lNautFilesOut,dDateTimeCal,nFilenum,nWO_Xcheck,nOutshipid,cCharge,lDoWM,cProgname,cISA_Num,cUseFolder
Public cCity,cState,cZip,nOrigSeq,nOrigGrpSeq,tsendto,tcc,tsendtoerr,tccerr,lFirstLoop,lJCPenney,lOverflow,nCtnNumber,tsendtoupc,tccupc
Public cMod,cMBOL,cEDIType,lParcelType,cFilenameShort,cFilenameOut,cFilenameArch,lcPath,cWO_NumList,cISA_Num

lTesting   = .F.  && Set to .t. for testing
lTestinput = .F. && lTesting  &&  Set to .t. for test input files only!

If lTesting
  Close Databases All
  Set Status Bar On
  _Screen.WindowState=2
Endif

Do m:\dev\prg\_setvars With lTesting
cProgname = "Nautica_create945"

lPick = .T.
lPrepack = .F.
lIsError = .F.
lDoCatch = .T.
cShip_ref = ""
lCloseOutput = .T.
nFilenum = 0
cCharge = "0.00"
ccustname = ""
cWO_NumStr = ""
cWO_NumList = ""
cString = ""
nPTCtnTot = 0
cShipMethodPymt = "PP"  && "prepaid", default.
lFirstLoop = .T.
nCtnNumber = 1  && Seed carton sequence count


*!* Note: For Wal-mart testing, be sure that there is a BOL and DEL_DATE filled in on the test OUTSHIP table,
*!* and that it matches the test data below

Try
  nOrigSeq = 0
  nOrigGrpSeq = 0
  cPPName = "Nautica"
*ASSERT .F. MESSAGE "At the start of "+cPPName+" 945 process..."
  tfrom = "TGF EDI Operations <edi-ops@tollgroup.com>"
  tmessage=""
  tcc = ""
  lDoBOLGEN = .F.
  lSamples = .F.
  cBOL1 = ""
  lISAFlag = .T.
  lSTFlag = .T.
  nLoadid = 1
  lSQLMail = .F.

  If Type("cOffice") = "L"
    If !lTesting And !lTestinput
      Wait Window "Office not provided...terminating" Timeout 2
      lCloseOutput = .F.
      Do ediupdate With "No OFFICE",.T.
      Throw
    Else
      Close Databases All
      Select 0
      If !Used('edi_trigger')
        cEDIFolder = "F:\3PL\DATA\"
        Use (cEDIFolder+"edi_trigger") In 0 Alias edi_trigger
      Endif
*!* TEST DATA AREA
      cBOL = "07315170000001395"
      cOffice = "M"
      nAcctNum = 687
      lFedEx = .F.
    Endif
  Endif
  Create Cursor temp945 (accountid i,isa_num c(10),st_num c(10),wo_num i,bol_no c(20),ship_ref c(20),filename c(50),filedate T)
  cBOL=Trim(cBOL)
  cMod = cOffice
  goffice = cMod
  cMBOL = ""
  cEDIType = "945"
  lParcelType = .F.

  lEmail = .T.
  lTestMail = lTesting && Sends mail to Joe only
  lNautFilesOut = !lTesting && If true, copies output to FTP folder (default = .t.)
  lStandalone = lTesting

*!*  lTestMail = .T.
*!*  lNautFilesOut = .F.

  Store "LB" To cWeightUnit
  cfd = "*"
  csegd = "~"
  nSegCtr = 0
  cterminator = ">" && Used at end of ISA segment

  Wait Window "Now preparing tables and variables" Nowait Noclear

*!* SET CUSTOMER CONSTANTS
  ccustname = "NAUTICA"  && Customer Identifier
  cX12 = "005010"  && X12 Standards Set used

  cMailName = "Nautica"
  Select 0
  Use F:\3pl\Data\mailmaster Alias mm
  Locate For mm.edi_type = '945' And mm.office = cOffice And mm.accountid = nAcctNum
  tsendto = Iif(mm.use_alt,mm.sendtoalt,mm.sendto)
  tcc = Iif(mm.use_alt,mm.ccalt,mm.cc)
  lcOutPath = Alltrim(mm.basepath)
  lcArchPath = Alltrim(mm.archpath)
  lcHoldPath = Alltrim(mm.holdpath)
  Locate
  Locate For mm.edi_type = 'MISC' And mm.taskname = "GENERAL"
  tsendtotest = Iif(mm.use_alt,mm.sendtoalt,mm.sendto)
  tcctest = Iif(mm.use_alt,mm.ccalt,mm.cc)
  tsendtoerr = Iif(mm.use_alt,mm.sendtoalt,mm.sendto)
  tccerr = Iif(mm.use_alt,mm.ccalt,mm.cc)
  Locate
  Locate For mm.edi_type = 'MISC' And accountid = 687 And mm.taskname = "MISSUPCS"
  tsendtoupc = Iif(mm.use_alt,mm.sendtoalt,mm.sendto)
  tccupc = Iif(mm.use_alt,mm.ccalt,mm.cc)
  Use In mm

*!* SET OTHER CONSTANTS
  cSF_Name = "TGF"
  Do Case
  Case cOffice = "N"
    cCustLoc =  "NJ"
    cFMIWarehouse = ""
    cCustPrefix = "945j"
    cDivision = "New Jersey"
    cSF_Addr1  = "800 FEDERAL BLVD"
    cSF_CSZ    =  "CARTERET"+cfd+"NJ"+cfd+"07008"

  Case cOffice = "M"
    cCustLoc =  "FL"
    cFMIWarehouse = ""
    cCustPrefix = "945f"
    cDivision = "Florida"
    cSF_Addr1  = "11400 NW 32ND AVE"
    cSF_CSZ    = "MIAMI"+cfd+"FL"+cfd+"33167"

  Otherwise
    cCustLoc =  "CA"
    cFMIWarehouse = ""
    cCustPrefix = "945c"
    cDivision = "California"
    cSF_Addr1  = "450 WESTMONT DRIVE"
    cSF_CSZ    = "SAN PEDRO"+cfd+"CA"+cfd+"90731"
  Endcase
  If lTesting
    cCustPrefix = "945t"
  Endif

  cCustFolder = Upper(ccustname)&&+&"-"+cCustLoc

  Store "" To lcKey
  Store 0 To alength,nLength
  cTime = Time()
  cDelTime = Strtran(Left(cTime,5),":","")
  cCarrierType = "M" && Default as "Motor Freight"
  dapptnum = ""

*!* SET OTHER CONSTANTS
  If Inlist(cOffice,'C','1','2')
    dDateTimeCal = (Datetime()-(3*3600))
  Else
    dDateTimeCal = Datetime()
  Endif
  dt1 = Ttoc(dDateTimeCal,1)
  dtmail = Ttoc(dDateTimeCal)
  dt2 = dDateTimeCal
  cString = ""

  crecid = "2122441111"  && Recipient EDI address
  csendqual = "ZZ"
  csendid = "TGF"
  crecqual = "12"

  cdate = Dtos(Date())
  cTruncDate = Right(cdate,6)
  cTruncTime = cDelTime
  cfiledate = cdate+cTruncTime
  cOrig = "J"
  nSTCount = 0
  cStyle = ""
  cPTString = ""
  nTotCtnWt = 0
  nTotCtnCount = 0
  nUnitSum = 0
  cTargetStyle = ""

** PAD ID Codes
  csendidlong = Padr(csendid,15," ")
  crecidlong = Padr(crecid,15," ")

  lcPath = lcOutPath
  cFilenameHold = (lcHoldPath+"MS945"+dt1+".txt")
  cFilenameShort = Justfname(cFilenameHold)
  cFilenameOut = (lcOutPath+cFilenameShort)
  cFilenameArch = (lcArchPath+cFilenameShort)
  nFilenum = Fcreate(cFilenameHold)

  If Used('OUTWOLOG')
    Use In outwolog
  Endif

  If Used('OUTSHIP')
    Use In outship
  Endif

  csq1 = [select * from outship where accountid = ]+Transform(nAcctNum)+[ and office = ']+cOffice+[' and bol_no = ']+cBOL+[']
  xsqlexec(csq1,,,"wh")
  If Reccount() = 0
    cErrMsg = "MISS BOL "+cBOL
    Do ediupdate With cErrMsg,.T.
    Throw
  Endif
  Index On bol_no Tag bol_no

  lOverflow = .F.
  Scan For outship.bol_no = cBOL
    If "OV"$outship.ship_ref
      lOverflow = .T.
      Exit
    Endif
  Endscan
  lOverflow = Iif(cBOL = '04907316097498432' And Date()={^2012-02-02},.T.,lOverflow)

  Select outshipid ;
  FROM outship ;
  WHERE bol_no = cBOL ;
  AND accountid = nAcctNum ;
  INTO Cursor tempsr
  Select tempsr
  Wait Window "There are "+Alltrim(Str(Reccount()))+" outshipid's to check" Nowait

  Select outship
  If !Seek(cBOL,'outship','bol_no')
    Wait Window "BOL not found in OUTSHIP" Timeout 2
    Do ediupdate With "BOL NOT FOUND",.T.
    Throw
  Else
    cPO_Num = Alltrim(outship.cnee_ref)
    cWO_Num = Alltrim(Str(outship.wo_num))
  Endif

  Replace outship.weight With 5*outship.ctnqty,outship.cuft With 2.5*outship.ctnqty ;
  FOR outship.bol_no = cBOL And outship.weight = 0 And !EMPTYnul(outship.del_date) ;
  IN outship

  If Used('OUTDET')
    Use In outdet
  Endif

  selectoutdet()
  Select outdet
  Index On outdetid Tag outdetid
  Locate

  If !Used("scacs")
    xsqlexec("select * from scac","scacs",,"wh")
    Index On scac Tag scac
  Endif
  If !Used("parcel_carriers")
    Use ("f:\3pl\data\parcel_carriers") In 0 Alias parcel_carriers Order Tag scac
  Endif

  If !lTestinput
    If Used('SHIPMENT')
      Use In shipment
    Endif

    csq1 = [select * from shipment where right(rtrim(str(accountid)),4) = ]+Transform(nAcctNum)
    xsqlexec(csq1,,,"wh")
    Index On shipmentid Tag shipmentid
    Index On pickticket Tag pickticket

    If Used('PACKAGE')
      Use In package
    Endif

    csq1 = [select * from package where accountid = ]+Transform(nAcctNum)
    xsqlexec(csq1,,,"wh")
    Index On shipmentid Tag shipmentid
    Set Order To Tag shipmentid

    Select shipment
    Set Relation To shipmentid Into package
  Endif

&& Added code blocks to the following to overcome BOL vs. WO selection issues, 12.23.2005
  cSQL="10.244.152.137"
  Select outship
  Locate
  If Used("sqlwo")
    Use In sqlwo
  Endif
  If File("F:\3pl\DATA\sqlwo.dbf")
    Delete File "F:\3pl\DATA\sqlwo.dbf"
  Endif
  Select bol_no,wo_num ;
  FROM outship ;
  WHERE bol_no == cBOL ;
  AND accountid = nAcctNum ;
  GROUP By 1,2 ;
  ORDER By 1,2 ;
  INTO Dbf F:\3pl\Data\sqlwo
  Use In sqlwo
  Select 0
  Use F:\3pl\Data\sqlwo Alias sqlwo
  If lTesting
*    BROWSE
  Endif

*  ASSERT .F. MESSAGE "At SQL CONNECT"
  cRetMsg = ""
  Do m:\dev\prg\sqlconnect_bol  With nAcctNum,ccustname,cPPName,.T.,cOffice,.T.
  If cRetMsg<>"OK"
    Do ediupdate With cRetMsg,.T.
    Throw
  Endif

*!* Added per Chris Malcolm to cover warehouse errors caused by deleting UPC numbers, 10.12.2016
  If lTesting
    Set Step On
  Endif
  Select ship_ref,ucc,Style,Color,Id,totqty From vnauticapp Where  .F. Into Cursor vvtemp1 ReadW
  Select vnauticapp
  Locate
  Scan For Empty(vnauticapp.upc)
    Scatter Fields ship_ref,ucc,Style,Color,Id,totqty Memvar
    Insert Into vvtemp1 From Memvar
  Endscan
  Select vvtemp1
  Locate
  If !Eof()
    Copy To h:\fox\missupcs.Xls Type Xl5
    missupcmail()
    cErrMsg = "Missing UPCs"
    Do ediupdate With cErrMsg,.T.
    Throw
  Endif
*!* End added code block

  Select vnauticapp
  Locate
  If lTesting
*BROWSE
*ASSERT .F.
  Endif
  If Reccount()=0
    Wait Window "SQL select data is EMPTY...error!" Timeout 2
    Do ediupdate With "NO SQL DATA",.T.
    Throw
  Endif

  Select outship
  Locate
  Wait Clear
  Wait Window "Now creating Header information" Nowait Noclear

*!* HEADER LEVEL EDI DATA
  Do num_incr_isa

  cISA_Num = Padl(c_CntrlNum,9,"0")
  nISA_Num = Int(Val(cISA_Num))

  If lTesting
    cISACode = "T"
  Else
    cISACode = "P"
  Endif

  Store "ISA"+cfd+"00"+cfd+"          "+cfd+"00"+cfd+"          "+cfd+csendqual+cfd+csendidlong+cfd+;
  crecqual+cfd+crecidlong+cfd+cTruncDate+cfd+cTruncTime+cfd+"U"+cfd+"00401"+cfd+;
  cISA_Num+cfd+"0"+cfd+cISACode+cfd+cterminator+csegd To cString
  Do cstringbreak

  Store "GS"+cfd+"SW"+cfd+csendid+cfd+crecid+cfd+cdate+cfd+cTruncTime+cfd+c_CntrlNum+;
  cfd+"X"+cfd+cX12+csegd To cString
  Do cstringbreak

*  DO num_incr_st
*  STORE "ST"+cfd+"945"+cfd+PADL(c_GrpCntrlNum,9,"0")+csegd TO cString
*  DO cstringbreak
*  nSTCount = nSTCount + 1
*  nSegCtr = 1

&& Removed ship_ref from W06 to W12, now one W06 loop per BOL/945 file.
&& moved this done to inside the scan loop, so we can have the PT in the W06
*STORE "W06"+cfd+"N"+cfd+Alltrim(outship.ship_ref)+cfd+cdate+cfd+TRIM(cBOL)+cfd+TRIM(cWO_Num)+cfd+cPO_Num+csegd TO cString
*DO cstringbreak
*nSegCtr = nSegCtr + 1

  Select outship
  Locate

  If !lTesting
    If Empty(cBOL)  && May be empty if triggered from PT screen, or FedEx shipment
      Wait Clear
      Wait Window "MISSING Bill of Lading Number!" Timeout 2
      Do ediupdate With "BOL# EMPTY",.T.
      Throw
    Else
      If !Seek(Padr(Trim(cBOL),20),"outship","bol_no")
        Wait Clear
        Wait Window "Invalid Bill of Lading Number - Not Found!" Timeout 2
        Do ediupdate With "BOL# NOT FOUND",.T.
        Throw
      Endif
    Endif
  Endif

*************************************************************************
*1  PTIDSOURCE/OUTSHIP RECORD MAIN LOOP
*  Loops through individual OUTSHIP lines for BOL#
*************************************************************************
  Wait Clear
  Wait Window "Now creating BOL#-based information..." Nowait Noclear
  Select outship
  Set Order To

  Count To N For outship.bol_no = cBOL And !EMPTYnul(del_date)
  If N=0
    Do ediupdate With "INCOMP BOL",.T.
    Throw
  Endif

  Locate
  cMissDel = ""

  oscanstr = "outship.accountid = nAcctNum and outship.bol_no = cBOL AND !EMPTY(outship.del_date)"

  Select outship
  Scan For &oscanstr

    Do num_incr_st
    Store "ST"+cfd+"945"+cfd+Padl(c_GrpCntrlNum,9,"0")+csegd To cString
    Do cstringbreak
    nSTCount = nSTCount + 1
    nSegCtr = 1

&& Removed ship_ref from W06 to W12, now one W06 loop per BOL/945 file.
    Store "W06"+cfd+"N"+cfd+Alltrim(outship.ship_ref)+cfd+cdate+cfd+Trim(cBOL)+cfd+Trim(cWO_Num)+cfd+Alltrim(outship.cnee_ref)+csegd To cString
    Do cstringbreak
    nSegCtr = nSegCtr + 1

    Scatter Memvar Memo
    lParcelType = Iif(Seek(m.scac,"parcel_carriers","scac"),.T.,.F.)
    Select outship

    lJCPenney = Iif("PENNEY"$outship.consignee,.T.,.F.)
    alength = Alines(apt,outship.shipins,.T.,Chr(13))
    If "OV"$outship.ship_ref
      Loop
    Endif
    nOutshipid = outship.outshipid
    nWO_Num = outship.wo_num
    If Type("nWO_Num")<> "N"
      lCloseOutput = .T.
      Do ediupdate With "BAD WO#",.T.
      Throw
    Endif
    cWO_Num = Alltrim(Str(nWO_Num))
*    lDoWM = .T.
    cShip_ref = Alltrim(outship.ship_ref)
    cPTString = Iif(Empty(cPTString),outship.consignee+" "+cShip_ref,cPTString+Chr(13)+m.consignee+" "+cShip_ref)
    If !(cWO_Num$cWO_NumList)
      cWO_NumList = Iif(Empty(cWO_NumList),cWO_Num,cWO_NumList+Chr(13)+cWO_Num)
    Endif
    If !lParcelType
      cTrackNum = Allt(outship.keyrec)  && ProNum if available
      cCharge = "0.00"
      If !(cWO_Num$cWO_NumStr)
        cWO_NumStr = Iif(Empty(cWO_NumStr),cWO_Num,cWO_NumStr+","+cWO_Num)
      Endif
    Else
      cTrackNum = Allt(outship.bol_no)  && UPS Tracking Number
*      cWO_NumStr = IIF(EMPTY(cWO_NumStr),cTrackNum,cWO_NumStr+","+cTrackNum)
      nCharge = 0
      Select shipment
      Locate For shipment.accountid = nAcctNum And shipment.pickticket = Padr(cShip_ref,20)
      If !Found()
        Locate For shipment.accountid = 9999 And shipment.pickticket = Padr(cShip_ref,20)
        If !Found()
          Do ediupdate With "MISS UPS REC"+cShip_ref,.T.
          Throw
        Else
          lSkipCharges = Iif(Inlist(shipment.billing,"COL","STP","B3P","C/B","BRC"),.T.,.F.)
          Select package
          Locate
          Sum pkgcharge To nCharge For package.shipmentid = shipment.shipmentid
          cCharge = Allt(Str(nCharge,8,2))
        Endif
      Else
        lSkipCharges = Iif(Inlist(shipment.billing,"COL","STP","B3P","C/B","BRC"),.T.,.F.)
        Select package
        Locate
        Sum pkgcharge To nCharge For package.shipmentid = shipment.shipmentid
        cCharge = Allt(Str(nCharge,8,2))
        Select trknumber ;
        FROM package ;
        WHERE package.shipmentid = shipment.shipmentid ;
        INTO Cursor temptrk
        Wait Window "There are "+Transform(Reccount('temptrk'))+" parcel tracking numbers in PT "+cShip_ref Timeout 2
        Locate
        Select package
      Endif
      If nCharge = 0 And !Inlist(outship.scac,"UPSL","UPCG","FDXG","FGC","FEG") And !lSkipCharges
        If lTesting
          cCharge = "25.50"
        Else
          cWinMsg = "UPS-EMPTY CHARGE"
          Assert .F. Message cWinMsg
          Do ediupdate With cWinMsg,.T.
          Throw
        Endif
      Endif
    Endif

    csq1 = [select * from outwolog where accountid = ]+Transform(nAcctNum)+[ and wo_num = ]+Transform(nWO_Num)
    xsqlexec(csq1,,,"wh")
    If Reccount() = 0
      cErrMsg = "MISS OUTWOLOG REC FOR WO# "+cWO_Num
      Do ediupdate With cErrMsg,.T.
      Throw
    Endif
    Select outwolog
    Index On wo_num Tag wo_num

    If Seek(outship.wo_num,"outwolog","wo_num")
      lPick = Iif(outwolog.picknpack,.T.,.F.)
      lPrepack = Iif(lPick,.F.,.T.)
    Endif

    Use In outwolog

    nTotCtnCount = outship.qty
    cPO_Num = Alltrim(outship.cnee_ref)
    cShip_ref = Alltrim(outship.ship_ref)

*!* Added this code to trap miscounts in OUTDET Units
    If lPrepack
      Select outdet
      Set Order To
      Sum totqty To nCtnTot1 For !units And outdet.outshipid = outship.outshipid
*!*  Check carton count
*      ASSERT .F. MESSAGE "In outdet/SQL tot check"
      Select Count(ucc) As cnt1 ;
      FROM vnauticapp ;
      WHERE vnauticapp.outshipid = outship.outshipid ;
      AND vnauticapp.totqty > 0 ;
      INTO Cursor tempsqlx
      Store tempsqlx.cnt1 To nCtnTot2
      Use In tempsqlx
      If nCtnTot1<>nCtnTot2
        Assert .F. Message "SQL CTNQTY Error"
        Set Step On
        Do ediupdate With "SQL CTNQTY ERR",.T.
        Throw
      Endif
    Else
      Select outdet
      Sum totqty To nUnitTot1 For units And outdet.outshipid = outship.outshipid
      Select vnauticapp
      Sum totqty To nUnitTot2 For vnauticapp.outshipid = outship.outshipid
      If nUnitTot1<>nUnitTot2
        Assert .F. Message "At SQL UNIT qty error"
        Set Step On
        Do ediupdate With "SQL UNITQTY ERR",.T.
        Throw
      Endif
    Endif

    Select outdet
    Set Order To outdetid
    Locate

*!* End code addition

    ddel_date = outship.del_date
    If lTestinput
      If Empty(ddel_date)
        ddel_date = Date()
      Endif
      dapptnum = "99999"
      dapptdate = Date()
    Else
      If Empty(ddel_date)
        cMissDel = Iif(Empty(cMissDel),"The following PTs had no Delivery Dates:"+Chr(13)+Trim(cShip_ref),cMissDel+Chr(13)+Trim(cShip_ref))
      Endif
      dapptnum = Alltrim(outship.appt_num)
      dapptdate = outship.appt

      If Empty(dapptdate)
        dapptdate = outship.del_date
      Endif
    Endif

    If !(cWO_Num$cWO_NumStr)
      cWO_NumStr = Iif(Empty(cWO_NumStr),cWO_Num,cWO_NumStr+","+cWO_Num)
    Endif

    If Alltrim(outship.SForCSZ) = ","
      Blank Fields outship.SForCSZ Next 1 In outship
    Endif

    cTRNum = ""
    cPRONum = ""

*    IF lFirstLoop
    m.CSZ = Trim(outship.CSZ)
    If Empty(Allt(Strtran(outship.CSZ,",","")))
      Wait Clear
      Wait Window "No SHIP-TO City/State/ZIP info...exiting" Timeout 2
      Do ediupdate With "NO CSZ INFO",.T.
      Throw
    Endif

    m.CSZ = Alltrim(Strtran(outship.CSZ,"  "," "))
    cCity = Allt(Left(Trim(outship.CSZ),At(",",outship.CSZ)-1))
    cStateZip = Allt(Substr(Trim(outship.CSZ),At(",",outship.CSZ)+1))
    cState = Allt(Left(cStateZip,2))
    cZip = Allt(Substr(cStateZip,3))

    Store "" To cSForCity,cSForState,cSForZip
    m.SForCSZ = Alltrim(Strtran(m.SForCSZ,"  "," "))
    nSpaces = Occurs(" ",Alltrim(m.SForCSZ))
    If nSpaces = 0
      m.SForCSZ = Strtran(m.SForCSZ,",","")
      cSForCity = Alltrim(m.SForCSZ)
      cSForState = ""
      cSForZip = ""
    Else
      nCommaPos = At(",",m.SForCSZ)
      nLastSpace = At(" ",m.SForCSZ,nSpaces)
      nMinusSpaces = Iif(nSpaces=1,0,1)
      If Isalpha(Substr(Trim(m.SForCSZ),At(" ",Trim(m.SForCSZ),nSpaces-nMinusSpaces)+1,2))
        cSForCity = Left(Trim(m.SForCSZ),At(",",m.SForCSZ)-1)
        cSForState = Substr(m.SForCSZ,nCommaPos+2,2)
        cSForZip = Trim(Right(Trim(m.SForCSZ),5))
        If Isalpha(cSForZip)
          cSForZip = ""
        Endif
      Else
        Wait Clear
        Wait Window "NOT ALPHA: "+Substr(Trim(m.SForCSZ),At(" ",m.SForCSZ,nSpaces-1)+1,2) Timeout 3
        cSForCity = Left(Trim(m.SForCSZ),At(",",m.SForCSZ)-1)
        cSForState = Substr(Trim(m.SForCSZ),At(" ",m.SForCSZ,nSpaces-2)+1,2)
        cSForZip = Trim(Right(Trim(m.SForCSZ),5))
        If Isalpha(cSForZip)
          cSForZip = ""
        Endif
      Endif
*     ENDIF

      Wait Clear
      Wait Window "Now creating Line Item information" Nowait Noclear

      Insert Into temp945 (accountid,isa_num,st_num,wo_num,bol_no,ship_ref,filename,filedate) ;
      VALUES (m.accountid,c_CntrlNum,c_GrpCntrlNum,m.wo_num,m.bol_no,m.ship_ref,cFilenameShort,dt2)

      Store "N1"+cfd+"SF"+cfd+Alltrim(cSF_Name)+cfd+"91"+cfd+"01"+csegd To cString
      Do cstringbreak
      nSegCtr = nSegCtr + 1

      Store "N3"+cfd+cSF_Addr1+csegd To cString
      Do cstringbreak
      nSegCtr = nSegCtr + 1

      Store "N4"+cfd+cSF_CSZ+csegd To cString
      Do cstringbreak
      nSegCtr = nSegCtr + 1

      nCtnNumber = 1  && Seed carton sequence count

*    ASSERT .F. MESSAGE "At Ship-to store...debug"
      cStoreNum = segmentget(@apt,"STORENUM",alength)
      If Empty(cStoreNum)
        cStoreNum = outship.dcnum
      Endif

      If Empty(cStoreNum)
        Store "N1"+cfd+"ST"+cfd+Alltrim(outship.consignee)+csegd To cString
      Else
        Store "N1"+cfd+"ST"+cfd+Alltrim(outship.consignee)+cfd+"92"+cfd+Alltrim(cStoreNum)+csegd To cString
      Endif
      Do cstringbreak
      nSegCtr = nSegCtr + 1

      cCountry = segmentget(@apt,"COUNTRY",alength)
      cCountry = Allt(cCountry)
      If Empty(cCountry)
        cCountry = "USA"
      Endif

      If Empty(Alltrim(outship.address2))
        Store "N3"+cfd+Trim(outship.address)+csegd To cString
      Else
        Store "N3"+cfd+Trim(outship.address)+cfd+Trim(outship.address2)+csegd To cString
      Endif
      Do cstringbreak
      nSegCtr = nSegCtr + 1

      Store "N4"+cfd+cCity+cfd+cState+cfd+cZip+cfd+cCountry+csegd To cString
      Do cstringbreak
      nSegCtr = nSegCtr + 1

      If !Empty(m.shipfor)
        cSFStoreNum = segmentget(@apt,"SFSTORENUM",alength)
        If Empty(cSFStoreNum)
          cSFStoreNum = Alltrim(m.sforstore)
        Endif
        If Empty(Alltrim(m.sforstore))
          Store "N1"+cfd+"Z7"+cfd+Alltrim(m.shipfor)+csegd To cString
        Else
          Store "N1"+cfd+"Z7"+cfd+Alltrim(m.shipfor)+cfd+"92"+cfd+cSFStoreNum+csegd To cString
        Endif
        Do cstringbreak
        nSegCtr = nSegCtr + 1

        If Empty(Alltrim(m.sforaddr2))
          Store "N3"+cfd+Trim(m.sforaddr1)+csegd To cString
        Else
          Store "N3"+cfd+Trim(m.sforaddr1)+cfd+Trim(m.sforaddr2)+csegd To cString
        Endif
        Do cstringbreak
        nSegCtr = nSegCtr + 1

        If !Empty(cSForState)
          Store "N4"+cfd+cSForCity+cfd+cSForState+cfd+cSForZip+cfd+"USA"+csegd To cString
        Else
          Store "N4"+cfd+cSForCity+csegd To cString
        Endif
        Do cstringbreak
        nSegCtr = nSegCtr + 1
      Endif

      Store "N9"+cfd+"ST"+cfd+Alltrim(m.consignee)+csegd To cString
      Do cstringbreak
      nSegCtr = nSegCtr + 1

      Store "N9"+cfd+"CO"+cfd+Alltrim(m.cnee_ref)+csegd To cString
      Do cstringbreak
      nSegCtr = nSegCtr + 1

      Store "N9"+cfd+"BM"+cfd+Trim(cBOL)+csegd To cString
      Do cstringbreak
      nSegCtr = nSegCtr + 1

      If (lParcelType Or (!lParcelType And !Empty(cTrackNum)))
        Store "N9"+cfd+"2I"+cfd+cTrackNum+csegd To cString
        Do cstringbreak
        nSegCtr = nSegCtr + 1
      Endif

      If !Empty(Alltrim(m.appt_num))
        Store "N9"+cfd+"AO"+cfd+Alltrim(m.appt_num)+csegd To cString
        Do cstringbreak
        nSegCtr = nSegCtr + 1
      Endif

      Store "G62"+cfd+"11"+cfd+Trim(Dtos(ddel_date))+cfd+"D"+cfd+cDelTime+csegd To cString  && Ship date/time
      Do cstringbreak
      nSegCtr = nSegCtr + 1

      If lTesting And (Empty(m.scac) Or Empty(m.ship_via))
        Store "RDWY" To m.scac
        Store "ROADWAY" To m.ship_via
      Else
        If !Empty(Trim(outship.scac)) And lParcelType
          Store Alltrim(outship.scac) To m.scac
          m.scac = Iif(Inlist(m.scac,"FED","FEG","FDXG"),"FDEG",m.scac)
          m.scac = Iif(Inlist(m.scac,"UPSL","UPSN","UPSS","UPGC"),"UPG",m.scac)
          m.scac = Iif(Inlist(m.scac,"UPWZ","UPOZ"),"UP2D",m.scac)
          lFedEx = .F.
          Select parcels
          If Seek(m.scac,"parcels","scac")
            cShipMethodPymt = Iif("COLLECT"$Upper(scacs.Name) Or "COLLECT"$Upper(scacs.DISPNAME),"CC","PP")
            If ("FEDERAL EXPRESS"$scacs.Name) Or ("FEDEX"$scacs.Name) Or ("FED EX"$scacs.Name)
              lFedEx = .T.
              If Seek(m.scac,"parcels","scac")
                If "FEDEX GROUND"$parcels.Name
                  m.scac = "FDEG"
                Endif
              Endif
            Endif
            Select outship

            If lParcelType Or lFedEx
              cCarrierType = "U" && Parcel as UPS or FedEx
            Endif
          Else
            Wait Clear
            Set Step On
            Do ediupdate With "MISSING SCAC",.T.
            Throw
          Endif
        Endif
      Endif

      Select outship

      If !Empty(cTRNum)
        Store "W27"+cfd+cCarrierType+cfd+Trim(m.scac)+cfd+Trim(m.ship_via)+cfd+;
        IIF(lParcelType,cShipMethodPymt,"")+cfd+"TL"+;
        REPLICATE(cfd,2)+cTRNum+csegd To cString
      Else
        Store "W27"+cfd+cCarrierType+cfd+Trim(m.scac)+cfd+Trim(m.ship_via)+;
        IIF(lParcelType,cfd+cShipMethodPymt,"")+csegd To cString
      Endif

      If !Empty(cString)
        Do cstringbreak
        nSegCtr = nSegCtr + 1
      Endif

      If lParcelType And !lSkipCharges
        Store "G72"+cfd+"516"+cfd+"15"+Replicate(cfd,6)+Allt(cCharge)+csegd To cString
        Do cstringbreak
        nSegCtr = nSegCtr + 1
      Endif

      lFirstLoop = .F.

    Endif

*************************************************************************
*2  DETAIL LOOP - MATCHING SHIP_REF FIELD (FROM OUTSHIP)
*************************************************************************
    If lOverflow
      Set Deleted Off
    Else
      Set Deleted On
    Endif

    Wait Clear
    Wait Window "Now creating Detail information "+cShip_ref Nowait Noclear
    Assert .F. Message "In detail loop...DEBUG"
    Select outdet
    Set Order To Tag outdetid
    Select vnauticapp
    Set Relation To outdetid Into outdet
    Locate
    Locate For vnauticapp.ship_ref = Trim(cShip_ref) And vnauticapp.outshipid = nOutshipid
    If !Found()
      If !lTesting
        Wait Window "PT "+cShip_ref+" NOT FOUND in vnauticapp...ABORTING" Timeout 2
        If !lTesting
          lSQLMail = .T.
        Endif
        Do ediupdate With "MISS PT-SQL: "+cShip_ref,.T.
        Throw
      Else
        Loop
      Endif
    Endif

    scanstr = "vnauticapp.ship_ref = cShip_ref and vnauticapp.outshipid = nOutshipid"

    Select vnauticapp
    Locate
    Locate For &scanstr
    cUCC= "XXX"
    Do While &scanstr

      If (Trim(vnauticapp.ucc) = 'CUTS' Or vnauticapp.totqty = 0)
        If !Eof()
          Skip 1 In vnauticapp
          Loop
        Else
          Exit
        Endif
      Endif

      If Trim(vnauticapp.ucc) <> cUCC
        Store Trim(vnauticapp.ucc) To cUCC
        Wait Window "UCC: "+cUCC Nowait
      Endif

      Store "LX"+cfd+Alltrim(Str(nCtnNumber))+csegd To cString   && Carton Seq. # (up to Carton total)
      lDoPALSegment = .F.
      Do cstringbreak
      nSegCtr = nSegCtr + 1

      lDoManSegment = .T.

      Do While vnauticapp.ucc = cUCC
        cDesc = ""
*        ASSERT .F. MESSAGE "At carton scan"
        Select outdet
        alength = Alines(aptdet,outdet.printstuff,.T.,Chr(13))
        cUCCNumber = vnauticapp.ucc
        cUCCNumber = Alltrim(Strtran(Trim(cUCCNumber)," ",""))

        If lDoManSegment
          lDoManSegment = .F.
          nPTCtnTot =  nPTCtnTot + 1
          Store "MAN"+cfd+"GM"+cfd+Trim(cUCCNumber)+csegd To cString  && SCC-14 Number (Wal-mart spec)
          Do cstringbreak
          nSegCtr = nSegCtr + 1
          If lParcelType
            Select temptrk
            cTrkNum = Alltrim(temptrk.trknumber)
            If !Eof()
              Skip 1 In temptrk
            Endif
            Store "MAN"+cfd+"CP"+cfd+Trim(cTrkNum)+csegd To cString  && Indiv. Tracking number
            Select vnauticapp
            Do cstringbreak
            nSegCtr = nSegCtr + 1
          Endif
        Endif

        nShipDetQty = Int(Val(outdet.Pack))
        nUnitSum = nUnitSum + nShipDetQty

        If Empty(outdet.ctnwt) Or outdet.ctnwt = 0
          cCtnWt = Alltrim(Str(Ceiling(outship.weight/outship.ctnqty)))
          If lTesting
            cCtnWt = "3"
          Else
            If Empty(cCtnWt) Or Int(Val(cCtnWt)) = 0
              If lParcelType
                Do ediupdate With "WEIGHT ERR: PT "+cShip_ref,.T.
                Throw
              Else
                cCtnWt = 5
              Endif
            Endif
          Endif
        Endif

        cColor = Upper(Trim(outdet.Color))
        cSize = Upper(Trim(outdet.Id))

&& added in getting the UOC value from the stylemster 01/23/2018  PG
        lcquery = "select upc from upcmast where accountid in (687,6356) and style = '"+outdet.style+"' and color ='"+outdet.Color+"' and id ='"+outdet.Id+"'"
        xsqlexec(lcquery,,,"wh")
        If Reccount("upcmast") = 0
          cUPC = Trim(outdet.upc)
          If (Isnull(cUPC) Or Empty(cUPC))
            cUPC = Trim(vnauticapp.upc)
          Endif
        Else
          cUPC = Trim(upcmast.upc)
        Endif

        cItemNum = Trim(outdet.custsku)
        cStyle = Trim(outdet.Style)
        If Empty(cStyle)
          Wait Window "Empty style in "+cShip_ref Nowait
        Endif

*        nShipDetQty = INT(VAL(vnauticapp.PACK))
        nShipDetQty = vnauticapp.totqty  && Using totqty vs. pack per Paul, 03.10.2010
        If Isnull(nShipDetQty) Or Empty(nShipDetQty) Or nShipDetQty = 0
          nShipDetQty = outdet.totqty
          If nShipDetQty = 0
            Skip 1 In vnauticapp
            Loop
          Endif
        Endif

*        nOrigDetQty = INT(VAL(vnauticapp.PACK))
        nOrigDetQty = vnauticapp.totqty  && Using totqty vs. pack per Paul, 03.10.2010
        If Isnull(nOrigDetQty) Or nOrigDetQty = 0
          nOrigDetQty = nShipDetQty
        Endif

*!* Changed the following to utilize original 940 unit codes from Printstuff field
        cUnitCode = Trim(segmentget(@aptdet,"UNITCODE",alength))
        If Empty(cUnitCode)
          cUnitCode = "CA"
        Endif

        If nOrigDetQty = nShipDetQty
          nShipStat = "CL"
        Else
          nShipStat = "CP"
        Endif

        cUnitCode = "CA"
        nOrigDetQty =1
        nOrigQty =1
        nShipDetQty=1

&& Changed W12 to include ship_ref, removed from W06.
        Store "W12"+cfd+nShipStat+cfd+Alltrim(Str(nOrigDetQty))+cfd+;
        ALLTRIM(Str(nShipDetQty))+cfd+Alltrim(Str(nOrigDetQty-nShipDetQty))+cfd+;
        cUnitCode+cfd+Alltrim(cUPC)+cfd+"VN"+cfd+cStyle+cfd+cShip_ref+csegd To cString
        Do cstringbreak
        nSegCtr = nSegCtr + 1

*        ASSERT .f. MESSAGE "AT G69 LOOP"
        cDesc = Alltrim(segmentget(@aptdet,"STYLEDESC",alength))
        If !Empty(cDesc)
          Store "G69"+cfd+cDesc+csegd To cString
          Do cstringbreak
          nSegCtr = nSegCtr + 1
        Endif

        cLabelCode = Alltrim(segmentget(@aptdet,"LABELCODE",alength))
        If !Empty(cLabelCode)
          Store "N9"+cfd+"LB"+cfd+cLabelCode+csegd To cString
          Do cstringbreak
          nSegCtr = nSegCtr + 1
        Endif

        If !Empty(cColor)
          Store "N9"+cfd+"VCL"+cfd+cColor+csegd To cString
          Do cstringbreak
          nSegCtr = nSegCtr + 1
        Endif

        If !Empty(cSize)
*          ASSERT .F. MESSAGE "In size population...debug"
          Store "N9"+cfd+"VDI"+cfd+cSize+csegd To cString
          Do cstringbreak
          nSegCtr = nSegCtr + 1
        Endif

        Select vnauticapp

        nWeight = (outship.weight/outship.ctnqty)
        If Empty(nWeight) Or nWeight = 0
          Do ediupdate With "EMPTY WT: "+cShip_ref,.T.
          Throw
        Else
          cWeight = Alltrim(Str(nWeight,10,1))
          nTotCtnWt = nTotCtnWt + nWeight
          Store "MEA"+cfd+"WT"+cfd+"L"+cfd+cWeight+csegd To cString
          Do cstringbreak
          nSegCtr = nSegCtr + 1
        Endif

        nVolCuFt = (outship.cuft/outship.ctnqty)
        If Empty(nVolCuFt) Or nVolCuFt = 0
          Do ediupdate With "EMPTY CU.FT: "+cShip_ref,.T.
          Throw
        Else
          cVolCuFt = Alltrim(Str(nVolCuFt,10,2))
          Store "MEA"+cfd+"SD"+cfd+"UCB"+cfd+cVolCuFt+csegd To cString
          Do cstringbreak
          nSegCtr = nSegCtr + 1
        Endif
        lDoManSegment = .F.
        Skip 1 In vnauticapp
      Enddo
      nCtnNumber = nCtnNumber + 1

    Enddo

*************************************************************************
*2^  END DETAIL MATCHING SHIP_REF LOOP
*************************************************************************
    cTotCtnWt = Alltrim(Str(nTotCtnWt,10,1))
    Store "W03"+cfd+Alltrim(Str(nPTCtnTot))+cfd+cTotCtnWt+cfd+;
    cWeightUnit+csegd To cString   && Units sum, Weight sum, carton count
    nSegCtr = nSegCtr + 1
    Fwrite(nFilenum,cString)

    Store  "SE"+cfd+Alltrim(Str(nSegCtr+1))+cfd+Padl(c_GrpCntrlNum,9,"0")+csegd To cString
    Fwrite(nFilenum,cString)

    Wait Clear
    nTotCtnWt = 0
    nPTCtnTot = 0
    Select outship
  Endscan


*************************************************************************
*1^  END OUTSHIP MAIN LOOP
*************************************************************************
  Assert .F. Message "At close of outship loop"
  Do close945
  =Fclose(nFilenum)
  If !lTesting
    Select edi_trigger
    Do ediupdate With "945 CREATED",.F.
    Select edi_trigger
    Locate

    If !Used("ftpedilog")
      Select 0
      Use F:\edirouting\ftpedilog Alias ftpedilog
      Insert Into ftpedilog (TRANSFER,ftpdate,filename,acct_name,Type) Values ("945-"+ccustname+"-"+cCustLoc,dt2,cFilenameShort,Upper(ccustname),"945")
      Use In ftpedilog
    Endif
  Endif

  Wait Clear
  Wait Window cCustFolder+" 945 Process complete..." Nowait

*!* Create eMail confirmation message
  If lTestMail
    tsendto = tsendtotest
    tcc = tcctest
  Endif

  tsubject = cMailName+" 945 EDI File from TGF as of "+dtmail+" ("+cCustLoc+")"
  tattach = " "
  tmessage = "945 EDI Info from TGF, file "+cFilenameShort+Chr(13)
  tmessage = tmessage + "Division "+cDivision+", BOL# "+Trim(cBOL)+Chr(13)
  tmessage = tmessage + "Work Orders: "+cWO_NumStr+","+Chr(13)
  tmessage = tmessage + "containing these picktickets:"+Chr(13)+Chr(13)
  tmessage = tmessage + cPTString + Chr(13)+Chr(13)
  tmessage = tmessage +"has been created and will be transmitted ASAP."+Chr(13)+Chr(13)
  If !Empty(cMissDel)
    tmessage = tmessage+Chr(13)+Chr(13)+cMissDel+Chr(13)+Chr(13)
  Endif
  If lTesting Or lTestinput
    tmessage = tmessage + "This is a TEST 945"
  Else
    tmessage = tmessage + "If you have any questions, please eMail or call 732-750-9000 x217."
  Endif

  If lEmail
    Do Form m:\dev\frm\dartmail2 With tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
  Endif

  Release All Like c_CntrlNum,c_GrpCntrlNum
  Wait Clear
  Wait Window cMailName+" 945 EDI File output complete" Timeout 1

*!* Transfers files to correct output folders
  Copy File &cFilenameHold To &cFilenameArch
  If lNautFilesOut And !lTesting
    Copy File &cFilenameHold To &cFilenameOut
    Delete File &cFilenameHold
    Select temp945
    Copy To "f:\3pl\data\temp945naut.dbf"
    Use In temp945
    Select 0
    Use "f:\3pl\data\temp945naut.dbf" Alias temp945naut
    Select 0
    Use "f:\3pl\data\pts_sent945.dbf" Alias pts_sent945
    Append From "f:\3pl\data\temp945naut.dbf"
    Use In pts_sent945
    Use In temp945naut
    Delete File "f:\3pl\data\temp945naut.dbf"
  Endif

*!* asn_out_data()()

Catch To oErr
  If lDoCatch
    Assert .F. Message "In Error CATCH"
    Set Step On
*    lEmail = .F.
    Do ediupdate With "ERRHAND ERROR",.T.
    tsubject = ccustname+" Error ("+Transform(oErr.ErrorNo)+") at "+Ttoc(Datetime())
    tattach  = ""
    If lTesting
      tsendto  = tsendtotest
      tcc = tcctest
    Else
      tsendto  = tsendtoerr
      tcc = tccerr
    Endif

    tmessage = ccustname+" Error processing "+Chr(13)

    tmessage =tmessage+ "Try/CATCH Exeception Message:"+Chr(13)+;
    [  Error: ] + Str(oErr.ErrorNo) +Chr(13)+;
    [  LineNo: ] + Str(oErr.Lineno) +Chr(13)+;
    [  Message: ] + oErr.Message +Chr(13)+;
    [  Procedure: ] + oErr.Procedure +Chr(13)+;
    [  Details: ] + oErr.Details +Chr(13)+;
    [  StackLevel: ] + Str(oErr.StackLevel) +Chr(13)+;
    [  LineContents: ] + oErr.LineContents+Chr(13)+;
    [  UserValue: ] + oErr.UserValue
    tmessage =tmessage+Chr(13)+cProgname

    tsubject = "945 EDI Poller Error at "+Ttoc(Datetime())
    tattach  = ""
    tcc=""
    tfrom    ="TGF EDI 945 Poller Operations <transload-ops@tollgroup.com>"
    Do Form m:\dev\frm\dartmail2 With tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
    If nFilenum > 0
      Fclose(nFilenum)
    Endif
  Endif
Finally
  If Used('OUTSHIP')
    Use In outship
  Endif
  If Used('OUTDET')
    Use In outdet
  Endif
  If Used('SHIPMENT')
    Use In shipment
  Endif
  If Used('PACKAGE')
    Use In package
  Endif
  If Used('SERFILE')
    Use In serfile
  Endif
  If Used('OUTWOLOG')
    Use In outwolog
  Endif

  On Error
  Wait Clear
  Release All Like l*

Endtry

*** END OF CODE BODY

****************************
Procedure close945
****************************
** Footer Creation

Wait Window "Now creating Section Closure information" Nowait Noclear
* cTotCtnWt = ALLTRIM(STR(nTotCtnWt,10,1))
* STORE "W03"+cfd+ALLTRIM(STR(nPTCtnTot))+cfd+cTotCtnWt+cfd+;
cWeightUnit+csegd TO cString   && Units sum, Weight sum, carton count
* nSegCtr = nSegCtr + 1

nPTCtnTot = 0
nTotCtnWt = 0
nTotCtnCount = 0
nUnitSum = 0
*  fwrite(nFilenum,cString)

* STORE  "SE"+cfd+ALLTRIM(STR(nSegCtr+1))+cfd+PADL(c_GrpCntrlNum,9,"0")+csegd TO cString
* fwrite(nFilenum,cString)

Store  "GE"+cfd+Alltrim(Str(nSTCount))+cfd+c_CntrlNum+csegd To cString
Fwrite(nFilenum,cString)

Store  "IEA"+cfd+"1"+cfd+Padl(c_CntrlNum,9,"0")+csegd To cString
Fwrite(nFilenum,cString)

Return

****************************
Procedure num_incr_isa
****************************

If !Used("serfile")
  Use ("f:\3pl\data\serial\nautica_945_serial") In 0 Alias serfile
Endif
Select serfile
If lTesting And lISAFlag
  nOrigSeq = serfile.seqnum
  lISAFlag = .F.
Endif
nISA_Num = serfile.seqnum
c_CntrlNum = Alltrim(Str(serfile.seqnum))
Replace serfile.seqnum With serfile.seqnum + 1 In serfile
Select outship
Return

****************************
Procedure num_incr_st
****************************

If !Used("serfile")
  Use ("f:\3pl\data\serial\nautica_945_serial") In 0 Alias serfile
Endif
Select serfile
If lTesting And lSTFlag
  nOrigGrpSeq = serfile.grpseqnum
  lSTFlag = .F.
Endif
c_GrpCntrlNum = Alltrim(Str(serfile.grpseqnum))
Replace serfile.grpseqnum With serfile.grpseqnum + 1 In serfile
Select outship
Return


****************************
Procedure segmentget
****************************

Parameter thisarray,lcKey,nLength

For i = 1 To nLength
  If i > nLength
    Exit
  Endif
  lnEnd= At("*",thisarray[i])
  If lnEnd > 0
    lcThisKey =Trim(Substr(thisarray[i],1,lnEnd-1))
    If Occurs(lcKey,lcThisKey)>0
      Return Substr(thisarray[i],lnEnd+1)
      i = 1
    Endif
  Endif
Endfor

Return ""

****************************
Procedure ediupdate
****************************
Parameter cStatus,lIsError
lDoCatch = .F.

If lIsError
*    SET STEP ON
Endif

If !lTesting
  Select edi_trigger
  nRec = Recno()
  If !lIsError
    Set Step On
    Replace processed With .T.,proc945 With .T.,file945 With cFilenameArch,isa_num With cISA_Num,;
    fin_status With "945 CREATED",errorflag With .F.,when_proc With Datetime() ;
    FOR edi_trigger.bol = cBOL And accountid = nAcctNum
  Else
    Replace processed With .T.,proc945 With .F.,file945 With "",;
    fin_status With cStatus,errorflag With .T. ;
    FOR edi_trigger.bol = cBOL And accountid = nAcctNum
    If lCloseOutput
      =Fclose(nFilenum)
      Erase &cFilenameHold
    Endif
  Endif
Endif

If lIsError And lEmail And cStatus<>"SQL ERROR"
  tsubject = "945 Error in "+cMailName+" BOL "+Trim(cBOL)+"(At PT "+cShip_ref+")"
  tattach = " "
  If lTesting
    tsendto = tsendtotest
    tcc = tcctest
  Else
    tsendto = tsendtoerr
    tcc = tccerr
  Endif
  tmessage = "945 Processing for BOL# "+cBOL+", WO# "+cWO_Num+" produced this error: (Office: "+cOffice+"): "+cStatus+Chr(13)+"Check EDI_TRIGGER and re-run"
  If "TOTQTY ERR"$cStatus
    tmessage = tmessage + Chr(13) + "At OUTSHIPID: "+Alltrim(Str(m.outshipid))
  Endif
  Do Form m:\dev\frm\dartmail2 With tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
Endif

If Used('outship')
  Use In outship
Endif
If Used('outdet')
  Use In outdet
Endif

If Used('serfile')
  Select serfile
  If lTesting
    Replace serfile.seqnum With nOrigSeq
    Replace serfile.grpseqnum With nOrigGrpSeq
  Endif
  Use In serfile
Endif
If Used('scacs')
  Use In scacs
Endif
If Used('parcels')
  Use In parcels
Endif
If Used('mm')
  Use In mm
Endif
If Used('tempx')
  Use In tempx
Endif
If !lTesting
  Select edi_trigger
  Locate
Endif

Endproc

****************************
Procedure cstringbreak
****************************
cLen = Len(Alltrim(cString))
If Isnull(cString) Or Empty(cString)
  Assert .F. Message "At cStringBreak procedure empty field...>>DEBUG<<"
Endif
*  FPUTS(nFilenum,cString)
Fwrite(nFilenum,cString)
cString = ""
Endproc


****************************
Procedure cszbreak
****************************
cCSZ = Allt(m.CSZ)

For ii = 5 To 2 Step -1
  cCSZ = Strtran(cCSZ,Space(ii),Space(1))
Endfor
cCSZ = Strtran(cCSZ,",","")
len1 = Len(Allt(cCSZ))
nSpaces = Occurs(" ",cCSZ)

If nSpaces<2
  Do ediupdate With "BAD CSZ INFO",.T.
  Throw
Endif

For ii = nSpaces To 1 Step -1
  ii1 = Allt(Str(ii))
  Do Case
  Case ii = nSpaces
    nPOS = At(" ",cCSZ,ii)
    cZip = Allt(Substr(cCSZ,nPOS))
    nEndState = nPOS
  Case ii = (nSpaces - 1)
    nPOS = At(" ",cCSZ,ii)
    cState = Allt(Substr(cCSZ,nPOS,nEndState-nPOS))
    If nSpaces = 2
      cCity = Allt(Left(cCSZ,nPOS))
      Exit
    Endif
  Otherwise
    nPOS = At(" ",cCSZ,ii)
    cCity = Allt(Left(cCSZ,nPOS))
  Endcase
Endfor
Endproc

****************************
Procedure missupcmail
****************************
If lTesting
  Set Step On
Endif

tsubjectupc = "945 UPC Error in "+cMailName+" BOL "+Trim(cBOL)
tattach = ("h:\fox\missupcs.xls")
If lTesting
  tsendtoupc = tsendtotest
  tccupc = tcctest
Endif
tmessage = "945 Processing for BOL# "+cBOL+" produced this error: (Office: "+cOffice+"): One or more cartons missing UPCs"
tmessage = tmessage+Chr(13)+"See attached list of cartons"
Do Form m:\dev\frm\dartmail2 With tsendtoupc,tfrom,tsubjectupc,tcc,tattach,tmessage,"A"
Delete File "h:\fox\missupcs.xls"
Endproc
