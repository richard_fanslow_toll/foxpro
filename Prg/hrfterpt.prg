* HRFTERPT.PRG
* produce MONTHLY FTE report.
* AUTHOR: Mark Bennett
*
* This is a massive rewrite of hrtgfstaffrpt12.prg.
*
**************************************************************************************************************************
* Jan 2018 MB: massive changes; needed to rewrite much code to allow allocation of FTE and Headcount across multiple 
* TimeStar Departments. This means changing Headcounts to be fractional, not Integer.
* Required changing so that both FTE and Headcounts come from the same cursor - a really big change.
* For RF373634 from Todd Wilen.
* also removed Report type "R" as the people who wanted that are long gone from Toll.
* And type Q is now handled by the older version of this program.
**************************************************************************************************************************


* Build EXE as F:\UTIL\HR\TGFSTAFFRPT\HRFTERPT.exe

LPARAMETERS tcType
LOCAL loTGFStaffingReport, ldToday, i, lcType
PRIVATE oExcel, oWorkbook

PUBLIC glFatalError
glFatalError = .F.

runack("HRFTERPT")

utilsetup("HRFTERPT")


IF EMPTY( tcType ) THEN
	lcType = "K"
ELSE
	lcType = UPPER(ALLTRIM( tcType ))
ENDIF

IF NOT INLIST( lcType,"K","H") THEN
	WAIT WINDOW TIMEOUT 60 "Invalid Type parameter!!"
	RETURN
ENDIF

* this is needed for anything we do...
oExcel = CREATEOBJECT("excel.application")
oExcel.displayalerts = .F.
oExcel.VISIBLE = .F.
oWorkbook = .F.

loTGFStaffingReport = CREATEOBJECT('TGFStaffingReport')

DO CASE

	CASE lcType = "K"
		* mode K (for Ken Kausner) = run his FTE report - 3 tabs - against prior month
		ldToday = DATE()  && this will cause Main() to process prior month

		*!*	********************************************
		*!*	* ACTIVATE TO FORCE DIFFERENT MONTH
		*ldToday = {^2017-11-01}
		*!*	********************************************
		
		
		loTGFStaffingReport.MAIN('GF', ldToday, lcType)

		IF NOT glFatalError THEN
			loTGFStaffingReport.MAIN('GL', ldToday, lcType)
		ENDIF
		
		IF NOT glFatalError THEN
			loTGFStaffingReport.MAIN('SS', ldToday, lcType)
		ENDIF
		
		IF NOT glFatalError THEN
			loTGFStaffingReport.MAIN('ALL', ldToday, lcType)
		ENDIF
		

	CASE lcType = "H"
		ldToday = DATE()  && this will cause Main() to process prior month
		*!*	********************************************
		*!*	* ACTIVATE TO FORCE DIFFERENT MONTH
		*ldToday = {^2014-02-01}
		*!*	********************************************
		loTGFStaffingReport.MAIN('GF', ldToday, lcType)

	OTHERWISE
		* nothing
ENDCASE

schedupdate()

CLOSE DATABASES ALL
RETURN



* DON'T CHANGE THE ORDER OF THESE: 'GF' MUST BE FIRST, 'ALL' MUST BE LAST!
*loTGFStaffingReport.MAIN('GF', ldToday)
*loTGFStaffingReport.MAIN('GL', ldToday)
*loTGFStaffingReport.MAIN('ALL', ldToday)


*!*	** activate below for multiple months...
*!*	ldToday = {^2010-01-01}
*!*	FOR i = 1 TO 30
*!*		ldToday = GOMONTH(ldToday,1)
*!*		loTGFStaffingReport.MAIN('ALL', ldToday)
*!*	NEXT



#DEFINE LOGIT 1
#DEFINE WAITIT 2
#DEFINE NOWAITIT 4
#DEFINE SENDIT 8
#DEFINE CRLF CHR(13) + CHR(10)
#DEFINE RETURN_DATA_MANDATORY .F.
#DEFINE RETURN_DATA_NOT_MANDATORY .T.
#DEFINE CSVFORMAT 6
#DEFINE WRITE_ONLY_UNBUFFERED 11
#DEFINE COMMA ","
#DEFINE xlEdgeBottom 9
#DEFINE xlContinuous 1
#DEFINE xlMedium -4138
#DEFINE xlFONT_RED -16776961

DEFINE CLASS TGFStaffingReport AS CUSTOM

	cProcessName = 'TGFStaffingReport'

	lTestMode = .F.  && .F. FOR NORMAL OPERATION, .T. TO SUPPRESS ALL BUT DEVELOPER EMAILS AND CHANGE INPUT TABLE, ETC. 

	lDoBIExport = .T.

	dToday = DATE()

	lAutoYield = .T.

	cStartTime = TTOC(DATETIME())

	* report properties
	cBadData = ''
	*nFTEHours_Hourly = 0
	*nFTEHours_Salaried = 0

	nFTEHoursSalaried = 173.3333
	nFTEHours = 160.0000  && Per Ken Kausner 3/4/13 MB

	nMilesToHoursFactor = 0.0167  && 1 / 60
	nOOMileage = 0.0
	nOOHeadCnt = 0
	nTempMarkUpFactor = 1.31  && 1.275
	nFTECeiling = 1.7  && this will flag folks with FTEs greater than this as possible errors.
	lThereWereBadFTEs = .F.
	* FILENAME PROPERTIES
	cDetailFileForKen = ''
	cBADFTEs = ''
	cBadFTEFilename = ''

	* table properties
	cDeptlookupTable = 'F:\UTIL\ADPREPORTS\DATA\DEPTTRANS.DBF'

	*** Processing Properties
	lAllocateDepts = .T.  && if true, use a configuration table to allocate Hours/Headcounts for certain people across multiple departments
	* usually set these to .F.
	lShowHours = .F.
	lSpecialAllWhere = .F.

	* connection properties
	nSQLHandle = 0

	* wait window properties
	nWaitWindowTimeout = 2
	lWaitWindowIsOn = .T.

	* logfile properties
	lLoggingIsOn = .T.
	cLogFile = 'F:\UTIL\HR\TGFSTAFFRPT\LOGFILES\TGFSTAFF_Report_log.txt'

	* INTERNAL email properties
	lSendInternalEmailIsOn = .T.
	cFrom = 'Mark.Bennett@Tollgroup.com'
	cSendTo = 'mbennett@fmiint.com'
	cCC = ''
	cSubject = 'FTE Info to Qlikview for ' + DTOC(DATE())
	cAttach = ''
	cBodyText = ''

	cCOMPUTERNAME = ''
	cUSERNAME = ''


	FUNCTION INIT
		IF NOT DODEFAULT()
			RETURN .F.
		ENDIF
		WITH THIS
			*SET RESOURCE OFF
			CLOSE DATA
			SET CENTURY ON
			SET DATE AMERICAN
			SET HOURS TO 24
			SET ANSI ON
			SET TALK OFF
			SET DELETED ON
			SET CONSOLE OFF
			SET EXCLUSIVE OFF
			SET SAFETY OFF
			SET EXACT OFF
			SET STATUS BAR ON
			SET SYSMENU OFF
			SET ENGINEBEHAVIOR 70
			_VFP.AUTOYIELD = .lAutoYield
			.cCOMPUTERNAME = GETENV("COMPUTERNAME")
			.cUSERNAME = GETENV("USERNAME")
			IF .lTestMode THEN
				.cLogFile = 'F:\UTIL\HR\TGFSTAFFRPT\LOGFILES\TGFSTAFF_Report_log_TESTMODE.txt'
				.cSendTo = 'mbennett@fmiint.com'
			ENDIF
			.lLoggingIsOn = .lLoggingIsOn AND NOT EMPTY(.cLogFile)
			IF .lLoggingIsOn THEN
				SET ALTERNATE TO (.cLogFile) ADDITIVE
				SET ALTERNATE ON
			ENDIF
			SET PROCEDURE TO VALIDATIONS ADDITIVE
		ENDWITH
	ENDFUNC


	FUNCTION DESTROY
		WITH THIS
			IF .lLoggingIsOn  THEN
				SET ALTERNATE OFF
				SET ALTERNATE TO
			ENDIF
		ENDWITH
		DODEFAULT()
	ENDFUNC
	
	
	PROCEDURE CreateCURHOURS2
		LPARAMETERS tdFromDate, tdToDate
		WITH THIS
			LOCAL ldFromDate, ldToDate, lcADPFromDate, lcADPToDate, lcSQLFromDate, lcSQLToDate, lcSQLKronosHourly, lnFTEHours, lnFTEHoursSalaried, lnTempMarkUpFactor
			LOCAL lcADP_COMP, lcDIVISION, lcDEPT, lcMGMTTYPE, lcWRKSITE, lcTIMETYPE, lcGENDER, lcTGDEPT, lcDEPTDESC, lnTotalHours, lnPercent, lnCodeHours, lnRegHours, lnOTHours 
			LOCAL lnWageAMT, lcAllocatedFileName, lnFTECeiling
			
			ldFromDate = tdFromDate
			ldToDate = tdToDate
			lcADPFromDate = "DATE'" + TRANSFORM(YEAR(ldFromDate)) + "-" + PADL(MONTH(ldFromDate),2,'0') + "-" + PADL(DAY(ldFromDate),2,'0') + "'"
			lcADPToDate = "DATE'" + TRANSFORM(YEAR(ldToDate)) + "-" + PADL(MONTH(ldToDate),2,'0') + "-" + PADL(DAY(ldToDate),2,'0') + "'"
			lnFTEHours = .nFTEHours
			lnFTEHoursSalaried = .nFTEHoursSalaried
			lnTempMarkUpFactor = .nTempMarkUpFactor
			lnFTECeiling = .nFTECeiling

				***************************************************

				* GATHER TEMP INFO FROM KRONOS

				.nSQLHandle = SQLCONNECT("WTK2","sa","kr0n1tes")

				IF .nSQLHandle > 0 THEN

					lcSQLFromDate = .GetSQLStartDateTimeString( ldFromDate )
					lcSQLToDate = .GetSQLEndDateTimeString( ldToDate )


					***** NOTE: we need to exclude 020655 from below, because they have no actual worked hours, so only Vac/sick etc. will
					***** show up, understating their hours and fte. Then we must add them to the Salaried query later on (basically treating them as salaried).

					SET TEXTMERGE ON
					TEXT TO	lcSQLKronosHourly NOSHOW
SELECT
A.APPLYDTM,
D.LABORLEV5NM AS AGENCYNUM,
D.LABORLEV1NM AS ADP_COMP,
D.LABORLEV2NM AS DIVISION,
D.LABORLEV3NM AS DEPT,
D.LABORLEV4NM AS WRKSITE,
C.FULLNM AS NAME,
'       ' AS FILE_NUM,
'B' AS MGMTTYPE,
'C' AS TIMETYPE,
'U' AS GENDER,
C.PERSONNUM,
E.NAME AS PAYCODEDESC,
(CASE WHEN E.NAME NOT IN ('Regular Hours','Overtime Hours' ,'Doubltime Hours') THEN (A.DURATIONSECSQTY / 3600.00) ELSE 0.000 END) AS CODEHOURS,
(CASE WHEN E.NAME = 'Regular Hours' THEN (A.DURATIONSECSQTY / 3600.00) ELSE 0.000 END) AS REGHOURS,
(CASE WHEN E.NAME IN ('Overtime Hours' ,'Doubltime Hours') THEN (A.DURATIONSECSQTY / 3600.00) ELSE 0.000 END) AS OTHOURS,
(A.DURATIONSECSQTY / 3600.00) AS TOTHOURS,
WAGEAMT
FROM WFCTOTAL A
JOIN WTKEMPLOYEE B
ON B.EMPLOYEEID = A.EMPLOYEEID
JOIN PERSON C
ON C.PERSONID = B.PERSONID
JOIN LABORACCT D
ON D.LABORACCTID = A.LABORACCTID
JOIN PAYCODE E
ON E.PAYCODEID = A.PAYCODEID
WHERE (A.APPLYDTM >= <<lcSQLFromDate>>)
AND (A.APPLYDTM <= <<lcSQLToDate>>)
AND (D.LABORLEV1NM IN ('TMP'))
AND E.NAME NOT IN ('FMLA','Unpaid Leave','Unpaid Hours','Unpaid Suspended','Voluntary Time Off')
AND C.PERSONNUM <> '999'
ORDER BY 1,2,3,4,6
					ENDTEXT
					SET TEXTMERGE OFF


					IF .lTestMode THEN
						.TrackProgress('lcSQLKronosHourly = ' + lcSQLKronosHourly, LOGIT+SENDIT)
					ENDIF

					IF .ExecSQL(lcSQLKronosHourly, 'CURHOURSPRE', RETURN_DATA_MANDATORY) THEN
						* okay

					ELSE
						* this should not really happen!						
						THROW
					ENDIF  && .ExecSQL

					=SQLDISCONNECT(.nSQLHandle)

				ENDIF && .nSQLHandle > 0 THEN

				* COPY FILE_NUM INTO LARGER 7-CHAR FIELD TO MATCH INSPERITY IDS
				SELECT CURHOURSPRE
				SCAN
					REPLACE CURHOURSPRE.FILE_NUM WITH CURHOURSPRE.PERSONNUM IN CURHOURSPRE
				ENDSCAN

				* GATHER HOURLY INFO FROM TIMESTAR and add it in
				SELECT ;
					WORKDATE AS APPLYDTM, ;
					'0' AS AGENCYNUM, ;
					ADP_COMP, ;
					DIVISION, ;
					DEPT, ;
					COLLAR AS MGMTTYPE, ;
					'F' AS TIMETYPE, ;
					'U' AS GENDER, ;
					WORKSITE AS WRKSITE, ;
					NAME, ;
					FILE_NUM, ;
					PAY_TYPE AS PAYCODEDESC, ;
					CODEHOURS, ;
					REGHOURS, ;
					OTHOURS, ;
					TOTHOURS, ;
					0.00 AS WAGEAMT ;
					FROM TIMEDATA ;
					INTO CURSOR CURTIMEDATAHOURLY ;
					WHERE (WORKDATE >= ldFromDate) ;
					AND (WORKDATE <= ldToDate) ;
					AND (NOT INLIST(PAY_TYPE,'FMLA','UNPAID','SUSPENSION')) ;
					ORDER BY 3,8,1


				SELECT CURHOURSPRE
				APPEND FROM DBF('CURTIMEDATAHOURLY')

				USE IN CURTIMEDATAHOURLY

				***************************************************
				***************************************************
				***************************************************

				* ROLL UP HOURS INFO
				SELECT ALLTRIM(ADP_COMP) AS ADP_COMP, ;
					NAME, ;
					INT(VAL(FILE_NUM)) AS FILE_NUM, ;
					DIVISION, ;
					DEPT, ;
					WRKSITE, ;
					MGMTTYPE, ;
					TTOD(APPLYDTM) AS PAYDATE, ;
					SUM(CODEHOURS) AS CODEHOURS, ;
					SUM(REGHOURS) AS REGHOURS, ;
					SUM(OTHOURS) AS OTHOURS, ;
					SUM(TOTHOURS) AS TOTHOURS, ;
					SUM(WAGEAMT) AS WAGEAMT, ;
					0.0000 AS FTE, ;
					lnFTEHours AS EXPFTE, ;
					GENDER, ;
					TIMETYPE, ;
					'   ' AS TGFDEPT, ;
					'                         ' AS DEPTDESC, ;
					'    ' AS GLACCTNO, ;
					00000000.00 AS ANNSALARY ;
					FROM CURHOURSPRE ;
					INTO CURSOR CURHOURS ;
					GROUP BY 1, 2, 3, 4, 5, 6, 7, 8 ;
					ORDER BY 1, 2, 8 ;
					READWRITE
					
				* get CURCUSTOM
				SELECT ;
					STATUS, ;
					ADP_COMP, ;
					NAME, ;
					INSPID AS FILE_NUM, ;
					GENDER, ;
					COLLAR AS MGMTTYPE, ;
					TOLLCLASS AS TGFDEPT, ;
					'F' AS TIMETYPE, ;
					ANNSALARY ;
					FROM EEINFO ;
					INTO CURSOR CURCUSTOM ;
					ORDER BY 1, 2, 3


				* POPULATE MISSING INFO FROM OTHER CURSOR AND ALSO CALC FTE
				SELECT CURHOURS
				SCAN FOR ADP_COMP <> 'TMP'
				
					IF INLIST(CURHOURS.ADP_COMP,'E88','E89') THEN
						* use salaried fte factor
						REPLACE CURHOURS.EXPFTE WITH lnFTEHoursSalaried IN CURHOURS
					ENDIF

					SELECT CURCUSTOM
					LOCATE FOR ;
						ALLTRIM(ADP_COMP) = ALLTRIM(CURHOURS.ADP_COMP) ;
						AND FILE_NUM = CURHOURS.FILE_NUM ;
						AND ALLTRIM(STATUS) <> 'T'

					IF FOUND() THEN
						REPLACE CURHOURS.TIMETYPE WITH ALLTRIM(CURCUSTOM.TIMETYPE), ;
							CURHOURS.GENDER WITH ALLTRIM(CURCUSTOM.GENDER), ;
							CURHOURS.ANNSALARY WITH CURCUSTOM.ANNSALARY ;
							IN CURHOURS

					ELSE
						* active status not found, try leave in case they are recent leave
						SELECT CURCUSTOM
						LOCATE FOR ;
							ALLTRIM(ADP_COMP) = ALLTRIM(CURHOURS.ADP_COMP) ;
							AND FILE_NUM = CURHOURS.FILE_NUM ;
							AND (ALLTRIM(STATUS) = 'L')

						IF FOUND() THEN
							REPLACE CURHOURS.TIMETYPE WITH ALLTRIM(CURCUSTOM.TIMETYPE), ;
								CURHOURS.GENDER WITH ALLTRIM(CURCUSTOM.GENDER), ;
								CURHOURS.ANNSALARY WITH CURCUSTOM.ANNSALARY ;
								IN CURHOURS
						ELSE
							* just search by file#, ignore status and companycode

							SELECT CURCUSTOM
							LOCATE FOR FILE_NUM = CURHOURS.FILE_NUM

							IF FOUND() THEN
								REPLACE CURHOURS.TIMETYPE WITH ALLTRIM(CURCUSTOM.TIMETYPE), ;
									CURHOURS.GENDER WITH ALLTRIM(CURCUSTOM.GENDER), ;
									CURHOURS.ANNSALARY WITH CURCUSTOM.ANNSALARY ;
									IN CURHOURS
							ELSE
								* nothing can be done
								SELECT CURCUSTOM
								LOCATE
							ENDIF

						ENDIF

					ENDIF
					
					* blank out annsalary for salaried, for privacy
					IF INLIST(CURHOURS.ADP_COMP,'E88','E89') THEN
						* use salaried fte factor
						REPLACE CURHOURS.ANNSALARY WITH 0.00 IN CURHOURS
					ENDIF

				ENDSCAN				

				SELECT CURHOURS
				SCAN
					REPLACE CURHOURS.FTE WITH ( CURHOURS.TOTHOURS / CURHOURS.EXPFTE) IN CURHOURS

* we can no longer check for bad FTE at this point, because the hours are no longer totaled by employee here.
* add section later for that check					
*!*						IF CURHOURS.FTE > .nFTECeiling THEN
*!*							llBadFTE = .T.
*!*						ENDIF

					* populate wageamt = 1/12th of annual salary for salaried emps, per Ken K. 11/12/12 MB
*!*						IF INLIST(CURHOURS.ADP_COMP,'E88','E89') THEN
*!*							REPLACE CURHOURS.WAGEAMT WITH (CURHOURS.ANNSALARY / 12)
*!*						ENDIF
					
					* populate wagemamt for hourly employees; per Dan Ludwig 10/14/2016.
					IF INLIST(CURHOURS.ADP_COMP,'E87') THEN
						* derive hourly rate from annual salary
						lnHourlyRate = CURHOURS.ANNSALARY / 2080
						* CODE HOURS and Regular Hours are straight time; multiply Overtime by 1.5 factor...
						lnWage = (lnHourlyRate * CURHOURS.CODEHOURS) + (lnHourlyRate * CURHOURS.REGHOURS) + (lnHourlyRate * CURHOURS.OTHOURS * 1.5) 						
						REPLACE CURHOURS.WAGEAMT WITH lnWage
					ENDIF
					

				ENDSCAN

				SELECT *, 1.00 AS HDCNT, .F. AS FLAGGED, .F. AS ALLOCATED ;
					FROM CURHOURS ;
					INTO CURSOR CURHOURS2 ;
					ORDER BY NAME, PAYDATE ;
					READWRITE

				* apply temp markup factor
				SELECT CURHOURS2
				SCAN FOR ADP_COMP = 'TMP'
					REPLACE CURHOURS2.WAGEAMT WITH ( lnTempMarkUpFactor * CURHOURS2.WAGEAMT )
				ENDSCAN

				
				
				***************************************************************************************************************
				* BELOW IS ALL NEW FOR THIS VERSION WHICH ALLOWS % REALLOCATION OF MULTIPLE TOLLCLASSES FOR DESIGNATED EMPLOYEES 
				*
				* 1st, when an employee/temp has multiple hours records, which could happen if for example they changed dept in the month in question (very common for the temps),
				* make ADP_COMP, DIVISION, DEPT, MGMTTYPE, WRKSITE, TIMETYPE, GENDER, TGDEPT, DEPTDESC the same as in the latest time record, for all records for that employee/temp.
				*
				* 2nd, for each employee, give them 1.00 headcount in their 1st record; 0.00 in all others.
				*
				* These changes will let us group and total fte and headcount any which way from the same cursor, i.e. CURHOURS2
				* 
				* 3rd, we will manipulate the records in CURHOURS2 for certain designated employees so that they might, for example, have .75 headcount and .75 * hours FTE in 1 dept, and 
				* .25 headcount and .25 * hours FTE  in another dept. this is for employees who are supposed to split their time between depts every month.
				
				* to speed up upcoming steps...
				SELECT CURHOURS2
				INDEX ON PAYDATE TAG PAYDATE
				INDEX ON FLAGGED TAG FLAGGED
				 
				
				* step 1 - get the latest paydate for each person
				SELECT DISTINCT ;
					A.FILE_NUM AS FILE_NUM, ;
					(SELECT MAX(PAYDATE) FROM CURHOURS2 WHERE FILE_NUM == A.FILE_NUM) AS LASTPAY ;
					FROM CURHOURS2 A ;
					INTO CURSOR CURDISTINCT ;
					ORDER BY A.FILE_NUM
					
*!*		SELECT CURDISTINCT
*!*		BROW
				
				* now get the data from the latest paydate for each person and update all other records for that person with that data
				SELECT CURDISTINCT
				SCAN
					WAIT WINDOW NOWAIT 'Processing ' + TRANSFORM(CURDISTINCT.FILE_NUM)
					SELECT CURHOURS2
					LOCATE FOR (FILE_NUM = CURDISTINCT.FILE_NUM) AND (PAYDATE = CURDISTINCT.LASTPAY)
					IF FOUND() THEN
						* FLAG THIS RECORD
						REPLACE CURHOURS2.FLAGGED WITH .T. IN CURHOURS2
						* get data values
						lcADP_COMP = CURHOURS2.ADP_COMP
						lcDIVISION = CURHOURS2.DIVISION
						lcDEPT = CURHOURS2.DEPT
						lcMGMTTYPE = CURHOURS2.MGMTTYPE
						lcWRKSITE = CURHOURS2.WRKSITE
						lcTIMETYPE = CURHOURS2.TIMETYPE
						lcGENDER = CURHOURS2.GENDER

						* update the other records for the person - and zero out the headcnt because it's already 1.00 in the main record
						SELECT CURHOURS2
						SCAN FOR (NOT FLAGGED) AND (FILE_NUM = CURDISTINCT.FILE_NUM)
							REPLACE CURHOURS2.HDCNT WITH 0.00, ;
								CURHOURS2.ADP_COMP WITH lcADP_COMP, ;
								CURHOURS2.DIVISION WITH lcDIVISION, ;
								CURHOURS2.DEPT WITH lcDEPT, ;
								CURHOURS2.MGMTTYPE WITH lcMGMTTYPE, ;
								CURHOURS2.WRKSITE WITH lcWRKSITE, ;
								CURHOURS2.TIMETYPE WITH lcTIMETYPE, ;
								CURHOURS2.GENDER WITH lcGENDER ;
								IN CURHOURS2						
						ENDSCAN
					
					ELSE
						.TrackProgress('====> ERROR finding in CURHOURS2: ' + TRANSFORM(CURDISTINCT.FILE_NUM) + '  ' + TRANSFORM(CURDISTINCT.PAYDATE), LOGIT+SENDIT)					
					ENDIF
				
				ENDSCAN
				WAIT CLEAR

				*****************************************************************
				* populate tgf depts and their descriptions (i.e. toll class code and descriptions), etc. from the new dept translation table
				SELECT CURHOURS2
				SCAN
					SELECT DEPTLOOKUPTABLE
					LOCATE FOR DEPT == ALLTRIM(CURHOURS2.DEPT)
					IF FOUND() THEN
						REPLACE CURHOURS2.TGFDEPT WITH DEPTLOOKUPTABLE.HCFTECODE, ;
							CURHOURS2.GLACCTNO WITH DEPTLOOKUPTABLE.GLACCTNO, ;
							CURHOURS2.DEPTDESC WITH DEPTLOOKUPTABLE.HCFTEDESC ;
							IN CURHOURS2
							
					ENDIF
				ENDSCAN

						
				* track some detail info for people with bad/missing data
				SELECT CURHOURS2
				SCAN FOR ('U' $ MGMTTYPE) OR ('U' $ TGFDEPT) OR (EMPTY(DEPTDESC))
					.cBadData = .cBadData + ALLTRIM(CURHOURS2.ADP_COMP) + ', ' + ALLTRIM(CURHOURS2.NAME) + ', ' + ALLTRIM(CURHOURS2.DEPT) + ', ' + TRANSFORM(CURHOURS2.FILE_NUM) + ;
						+ ', ' + CURHOURS2.MGMTTYPE + ', ' + CURHOURS2.TGFDEPT+ ', ' + CURHOURS2.DEPTDESC + CRLF
				ENDSCAN
				
				

				************************************************************
				*  01/19/2018 MB
				IF .lAllocateDepts THEN
					* ALLOCDEPT
					SELECT DISTINCT NAME, FILE_NUM ;
						FROM ALLOCDEPT ;
						INTO CURSOR CURALLOCLIST
					
					SELECT CURALLOCLIST
					SCAN
						.TrackProgress('====> Allocating across departments for : ' + CURALLOCLIST.NAME, LOGIT+SENDIT)
						* get total hours for each employee
						SELECT CURHOURS2
						SUM CODEHOURS TO lnCodeHours FOR (FILE_NUM = CURALLOCLIST.FILE_NUM)
						SUM REGHOURS TO lnRegHours FOR (FILE_NUM = CURALLOCLIST.FILE_NUM)
						SUM OTHOURS TO lnOTHours FOR (FILE_NUM = CURALLOCLIST.FILE_NUM)
						SUM TOTHOURS TO lnTotalHours FOR (FILE_NUM = CURALLOCLIST.FILE_NUM)
						SUM WAGEAMT TO lnWageAMT FOR (FILE_NUM = CURALLOCLIST.FILE_NUM)
						*  get info from current records, then delete them
						SELECT CURHOURS2
						LOCATE FOR (FILE_NUM = CURALLOCLIST.FILE_NUM) AND FLAGGED
						IF FOUND() THEN
							SCATTER MEMVAR
							DELETE FOR (FILE_NUM = CURALLOCLIST.FILE_NUM) IN CURHOURS2					
						ELSE
							.TrackProgress("=====> ERROR: unable to find flagged rec for " + CURALLOCLIST.NAME, LOGIT+SENDIT+NOWAITIT)						
						ENDIF
						* create the allocation records
						SELECT ALLOCDEPT
						SCAN FOR (FILE_NUM = CURALLOCLIST.FILE_NUM)
							lnPercent = ALLOCDEPT.PERCENT / 100
							m.ALLOCATED = .T.
							m.FLAGGED = .F.
							m.DEPT = ALLOCDEPT.DEPT
							m.MGMTTYPE = ALLOCDEPT.MGMTTYPE
							* Apportion the numerics
							m.CODEHOURS = lnPercent * lnCodeHours
							m.REGHOURS = lnPercent * lnRegHours
							m.OTHOURS = lnPercent * lnOTHours
							m.TOTHOURS = lnPercent * lnTotalHours
							m.WAGEAMT = lnPercent * lnWageAMT
							m.HDCNT = lnPercent
							* insert an allocated record
							INSERT INTO CURHOURS2 FROM MEMVAR					
						ENDSCAN					
					
					ENDSCAN

					SELECT CURHOURS2
					SCAN FOR ALLOCATED
						* repopulate the values which are derived from dept,
						SELECT DEPTLOOKUPTABLE
						LOCATE FOR DEPT == ALLTRIM(CURHOURS2.DEPT)
						IF FOUND() THEN
							REPLACE CURHOURS2.TGFDEPT WITH DEPTLOOKUPTABLE.HCFTECODE, ;
								CURHOURS2.GLACCTNO WITH DEPTLOOKUPTABLE.GLACCTNO, ;
								CURHOURS2.DEPTDESC WITH DEPTLOOKUPTABLE.HCFTEDESC ;
								IN CURHOURS2									
						ENDIF
						* and recalc the FTE based on reallocated total hoursdo
						REPLACE CURHOURS2.FTE WITH ( CURHOURS2.TOTHOURS / CURHOURS2.EXPFTE) IN CURHOURS2
					ENDSCAN
					
					* save the info on what was alllocated to a spreadsheet
					SELECT CURHOURS2
					*BROWSE FOR ALLOCATED
					lcAllocatedFileName = "F:\UTIL\HR\TGFSTAFFRPT\REPORTS\DETAIL\allocated_tgfhours.XLS"
					COPY TO ( lcAllocatedFileName ) XL5 FOR ALLOCATED
											
				ENDIF  && .lAllocateDepts 
				************************************************************				
				
				* create a spreadsheet with potentially bad FTE amounts. This can help identify big timesheet errors in hours, which are most likely to come from Kronos.
				* First sum the FTE by file_num
				SELECT NAME, FILE_NUM, ;
					SUM(FTE) AS TOTFTE ;
					FROM CURHOURS2 ;
					INTO CURSOR CURFTESUMM ;
					GROUP BY NAME, FILE_NUM ;
					ORDER BY TOTFTE DESCENDING ;
					HAVING SUM(FTE) > lnFTECeiling 
				
				IF USED('CURFTESUMM') AND NOT EOF('CURFTESUMM') THEN
					.cBadFTEFilename = "F:\UTIL\HR\TGFSTAFFRPT\REPORTS\DETAIL\POSSIBLE_BADFTE.XLS"
					SELECT CURFTESUMM
					COPY TO (.cBadFTEFilename) XL5
					USE IN CURFTESUMM
					.lThereWereBadFTEs = .T.
				ENDIF
		
		ENDWITH
		RETURN
	ENDPROC


	FUNCTION MAIN
		LPARAMETERS tcMode, tdToday, tcType
		WITH THIS
			LOCAL lnNumberOfErrors, lcSQL, lcSQL2, lcSQL3, lcSQL4, lcSQL5, lcSQL6, loError, ldToday, lnFTEHours
			LOCAL lcFiletoSaveAs, lcSpreadsheetTemplate, lcGrandTotalRow, lcWhere, lcDivWhere2
			LOCAL lnDay, ldFromDate, ldToDate, lcSQLFromDate, lcSQLToDate
			LOCAL lcSQLKronos, lcSuffix, lcDivWhere, lcDateSuffix
			LOCAL i, oWorksheet, lnStartRow, lcStartRow, lnRow, lcRow, lnTempMarkUpFactor
			LOCAL lnMgmtStartRow, lcMgmtStartRow, lnMgmtEndRow, lcMgmtEndRow, lnPreCasualRow, lcKronosDivWhere
			LOCAL lnNumHourlyPaydates, lnNumSalariedPaydates, lcBIFileName, lcSQLKronosHourly, lcSQLKronosTMP
			LOCAL lcFileName, lcDept, lcHCFTEEntityDetailFile, lcHCFTEEntitySummaryFile, lcHCFTEEntityMultDeptsFile, lcHeadCountBasis
			LOCAL lcSQLCNTHourly, lcSQLCNTSalaried, lcBusUnitWhere, lnMonth, lnYear1, lnYear2, lcFiscal, lcTimeStarWhere, lcLastFTERow
			LOCAL llBadFTE, lnHourlyRate, lnWage
			LOCAL lnAllocErrors

			TRY
				lnNumberOfErrors = 0
				
				************************************************************
				* Block added 01/16/2018 MB
				IF .lAllocateDepts THEN
				
					IF NOT USED('ALLOCDEPT') THEN
					
						* open the allocation configurations table
						USE F:\UTIL\ADPREPORTS\DATA\ALLOCDEPT IN 0 ALIAS ALLOCDEPT
					
						* check that the %s for everyone add up to 100
						IF USED('CURTESTALLOC') THEN
							USE IN CURTESTALLOC
						ENDIF
						SELECT ;
							NAME, SUM(PERCENT) AS PERCENT ;
							FROM ALLOCDEPT ;
							INTO CURSOR CURTESTALLOC ;
							GROUP BY NAME ;
							ORDER BY NAME
							
						SELECT CURTESTALLOC
						lnAllocErrors = 0
						SCAN FOR (PERCENT <> 100)
							.TrackProgress("=====> ERROR: invalid allocation for " + CURTESTALLOC.NAME, LOGIT+SENDIT+NOWAITIT)
							lnAllocErrors = lnAllocErrors + 1
						ENDSCAN
						
						IF (lnAllocErrors > 0) THEN
							glFatalError = .T.
							THROW
						ENDIF
						
					ENDIF  &&  NOT USED('ALLOCDEPT')
					
				ENDIF  && .lAllocateDepts 
				************************************************************


				IF NOT USED('DEPTLOOKUPTABLE') THEN
					USE (.cDeptlookupTable) IN 0 ALIAS DEPTLOOKUPTABLE
				ENDIF

				IF NOT USED('TIMEDATA') THEN
					USE F:\UTIL\TIMESTAR\DATA\TIMEDATA IN 0 ALIAS TIMEDATA
					* delete bogus employees
					SELECT TIMEDATA
					SCAN
						IF ALLTRIM(FILE_NUM) == '1' THEN
							DELETE
						ENDIF
						IF ALLTRIM(FILE_NUM) == '2' THEN
							DELETE
						ENDIF
					ENDSCAN
				ENDIF

				IF NOT USED('EEINFO') THEN
					USE F:\UTIL\TIMESTAR\DATA\EEINFO IN 0 ALIAS EEINFO
				ENDIF

				*.cBodyText = ''
				.cBadData = ''

				.dToday = tdToday
				ldToday = tdToday
				lnDay = DAY(tdToday)

				.TrackProgress("FTE Info to Qlikview process started....", LOGIT+SENDIT+NOWAITIT)
				.TrackProgress('COMPUTERNAME = ' + .cCOMPUTERNAME, LOGIT+SENDIT)
				.TrackProgress('USERNAME = ' + .cUSERNAME, LOGIT+SENDIT)
				.TrackProgress('tcMode = ' + tcMode, LOGIT+SENDIT)
				.TrackProgress('tcType = ' + tcType, LOGIT+SENDIT)
				.TrackProgress('tdToday = ' + TRANSFORM(tdToday), LOGIT+SENDIT)
				.TrackProgress('PROJECT = HRFTERPT', LOGIT+SENDIT)
				IF .lTestMode THEN
					.TrackProgress('====>TEST MODE', LOGIT+SENDIT)
				ENDIF
				************************************************************
				* Block added 01/16/2018 MB
				IF .lAllocateDepts THEN
					.TrackProgress('.lAllocateDepts = TRUE', LOGIT+SENDIT)
				ELSE
					.TrackProgress('.lAllocateDepts = FALSE', LOGIT+SENDIT)
				ENDIF  && .lAllocateDepts 
				************************************************************
				

				DO CASE
					CASE tcType = "K"
						lDoBIExport = .F.
					CASE tcType = "H"
						lDoBIExport = .F.
					OTHERWISE
						* nothing
				ENDCASE

				lnStartRow = 3
				lcStartRow = ALLTRIM(STR(lnStartRow))

				DO CASE
					CASE tcMode = "GF"
						lcDivWhere  =   "WHERE DIVISION IN ('20','21','22','23','24','25','26','27','29','30','32','33','34','37','38') "
						lcSuffix = "_GF"
						lcBusUnitWhere = "WHERE BUSUNIT = 'GF '"
					CASE tcMode = "GL"
						lcDivWhere  =   "WHERE DIVISION IN ('01','02','03','04','05','06','07','08','11','14','15','16','50','51','52','53','54','55','56','57','58','59','60','61','62','63','66','67','68','69') "
						*lcDivWhere  =   "WHERE DIVISION IN ('01','02','03','04','05','06','07','08','11','14','15','16','50','51','52','53','54') " + ;
										"OR DIVISION IN ('55','56','57','58','59','60','61','62','63','66','67','68','69') "
						lcSuffix = "_GL"
						lcBusUnitWhere = "WHERE BUSUNIT = 'GL'"
					CASE tcMode = "ALL"
						lcDivWhere = ""
						lcDivWhere2 = ""
						lcKronosDivWhere = ""
						lcTimeStarWhere = ""
						*!*							IF .lSpecialAllWhere THEN
						*!*								lcDivWhere = "WHERE DIVISION IN ('21','36','73') "
						*!*							ENDIF
						lcSuffix = "_ALL"
						lcBusUnitWhere = ""
					CASE tcMode = "SS"
						lcDivWhere  =   "WHERE DIVISION IN ('80','81','82','85','86','87','90','91','92','93') "
						lcSuffix = "_SS"
						lcBusUnitWhere = "WHERE BUSUNIT = 'SS'"
					OTHERWISE
						THROW
				ENDCASE

				.TrackProgress('lcSuffix = ' + lcSuffix, LOGIT+SENDIT)
				.TrackProgress('lcDivWhere = ' + lcDivWhere, LOGIT+SENDIT)

				ldToDate = ldToday - lnDay  && this is last day of prior month
				ldFromDate = ldToDate + 1  && this is 1st day of current month
				ldFromDate = GOMONTH(ldFromDate,-1)  && this is 1st day of prior month

				lcDateSuffix = "_" + DTOS(ldFromDate) + "-" + DTOS(ldToDate)
				lcSuffix = lcSuffix + lcDateSuffix
				.cSubject = 'FTE Info to Qlikview for ' + DTOC(ldFromDate) + " - " + DTOC(ldToDate)


				* WE ONLY WANT TO CREATE THIS CURSOR ONCE AT THE BEGINNING - IT WILL SUPPORT ALL THE FOLLOWING LOOPS
				IF NOT USED('CURHOURS2') THEN
					.CreateCURHOURS2(ldFromDate, ldToDate)
				ENDIF
				
				* activate this for testing if we have all divs covered in our where clauses for each business unit
				*!*	SELECT * ;
				*!*	FROM CURHOURS2 ;
				*!*	INTO CURSOR CURBADDIVS ;
				*!*	WHERE DIVISION NOT IN ('20','21','22','23','24','25','26','27','29','30','32','33','34','37','38') ;
				*!*	AND DIVISION NOT IN ('01','02','03','04','05','06','07','08','11','14','15','50','51','52','53','54','55','56','57','58','59','60','61','62','66','67','68','69') ;
				*!*	AND	DIVISION NOT IN ('80','81','82','85','86','87','90','91','92','93')

				*!*	SELECT CURBADDIVS
				*!*	BROWSE
				*!*	THROW
				
				
				
				SELECT * ;
					FROM CURHOURS2 ;
					INTO CURSOR CURHOURS3 ;
					&lcDivWhere. ;
					ORDER BY NAME, PAYDATE ;
					READWRITE
				
				
*!*					SELECT CURHOURS3
*!*					BROWSE

				SELECT CURHOURS3
				lcFileName = "F:\UTIL\HR\TGFSTAFFRPT\REPORTS\DETAIL\tgfhours" + lcSuffix + ".XLS"
				COPY TO ( lcFileName ) XL5

				IF LEFT( lcSuffix, 4 ) == "_ALL" THEN
					* we just copied the detail file which Ken now wants - keep track of the filename for later
					.cDetailFileForKen = lcFileName
				ENDIF

				*SELECT CURHOURS3
				*COPY TO ( "F:\UTIL\HR\TGFSTAFFRPT\REPORTS\DETAIL\tgfhours" + lcSuffix )


				*lcSpreadsheetTemplate = 'F:\UTIL\HR\TGFSTAFFRPT\TEMPLATES\TGFSTAFFING_TEMPLATE8.XLS'
				************************************************************
				* Block added 01/16/2018 MB
				IF .lAllocateDepts THEN
					lcSpreadsheetTemplate = 'F:\UTIL\HR\TGFSTAFFRPT\TEMPLATES\TGFSTAFFING_TEMPLATE9.XLS'
				ELSE
					lcSpreadsheetTemplate = 'F:\UTIL\HR\TGFSTAFFRPT\TEMPLATES\TGFSTAFFING_TEMPLATE8.XLS'
				ENDIF  && .lAllocateDepts 
				************************************************************

				lcFiletoSaveAs = 'F:\UTIL\HR\TGFSTAFFRPT\REPORTS\TGFSTAFFING' + lcDateSuffix + "_" + tcType + '.XLS'
				.TrackProgress('Creating spreadsheet: ' + lcFiletoSaveAs, LOGIT+SENDIT+NOWAITIT)

				* this block must only run once, when depends on mode...
				DO CASE
					CASE tcType = "K"
						.cSubject = 'FTE Staffing for ' + DTOC(ldFromDate) + " - " + DTOC(ldToDate)
						.cSendTo = 'Todd.Wilen@Tollgroup.com, Dan.Ludwig@Tollgroup.com, Abigail.Melaika@tollgroup.com'
						.cCC = 'Mark.Bennett@Tollgroup.com'
						IF tcMode = "GF" THEN
							* delete output file if it already exists...
							IF FILE(lcFiletoSaveAs) THEN
								DELETE FILE (lcFiletoSaveAs)
							ENDIF
							oWorkbook = oExcel.workbooks.OPEN(lcSpreadsheetTemplate)
							oWorkbook.SAVEAS(lcFiletoSaveAs)
						ENDIF
					CASE tcType = "H"
						.cSubject = 'GF FTE Staffing Report for ' + DTOC(ldFromDate) + " - " + DTOC(ldToDate)
						.cSendTo = 'Lauren.Wojcik@Tollgroup.com'
						.cCC = 'Mark.Bennett@Tollgroup.com'
						* delete output file if it already exists...s
						IF FILE(lcFiletoSaveAs) THEN
							DELETE FILE (lcFiletoSaveAs)
						ENDIF
						oWorkbook = oExcel.workbooks.OPEN(lcSpreadsheetTemplate)
						oWorkbook.SAVEAS(lcFiletoSaveAs)
					OTHERWISE
						* nothing
				ENDCASE

				IF .lTestMode THEN
					.cSendTo = 'Mark.Bennett@Tollgroup.com'
					.cCC = ''
				ENDIF


				* determine current worksheet
				DO CASE
					CASE tcMode = "GF"
						oWorksheet = oWorkbook.Worksheets[1]
					CASE tcMode = "GL"
						oWorksheet = oWorkbook.Worksheets[2]
					CASE tcMode = "SS"
						oWorksheet = oWorkbook.Worksheets[3]
					CASE tcMode = "ALL"
						oWorksheet = oWorkbook.Worksheets[4]
				ENDCASE


				IF USED('CURHOURS') THEN
					USE IN CURHOURS
				ENDIF
				IF USED('CURHOURSPRE') THEN
					USE IN CURHOURSPRE
				ENDIF
				IF USED('CURCUSTOM') THEN
					USE IN CURCUSTOM
				ENDIF
				IF USED('CURDIVLIST') THEN
					USE IN CURDIVLIST
				ENDIF
				IF USED('CURPAYDATESPRE') THEN
					USE IN CURPAYDATESPRE
				ENDIF
				IF USED('CURPAYDATES') THEN
					USE IN CURPAYDATES
				ENDIF
				IF USED('CURWHITE') THEN
					USE IN CURWHITE
				ENDIF
				IF USED('CURBLUE') THEN
					USE IN CURBLUE
				ENDIF
				IF USED('CURCNTTYPE') THEN
					USE IN CURCNTTYPE
				ENDIF
				IF USED('CURCNTGENDER') THEN
					USE IN CURCNTGENDER
				ENDIF
				IF USED('CURTEMPSPRE') THEN
					USE IN CURTEMPSPRE
				ENDIF
				IF USED('CURTEMPS') THEN
					USE IN CURTEMPS
				ENDIF

			* summarize for White Collar and Blue Collar 
			SELECT "WHITE COLLAR" AS MGMTTYPE, ;
				TGFDEPT, ;
				DEPTDESC, ;
				SUM(FTE) AS FTE, ;
				SUM(HDCNT) AS HEADCNT, ;
				SUM(TOTHOURS) AS TOTHOURS ;
				FROM CURHOURS3 ;
				INTO CURSOR CURWHITE ;
				WHERE CURHOURS3.MGMTTYPE = "W" ;
				GROUP BY 1, 2, 3 ;
				ORDER BY 1, 3 ;
				READWRITE

			SELECT "BLUE COLLAR" AS MGMTTYPE, ;
				TGFDEPT, ;
				DEPTDESC, ;
				SUM(FTE) AS FTE, ;
				SUM(HDCNT) AS HEADCNT, ;
				SUM(TOTHOURS) AS TOTHOURS ;
				FROM CURHOURS3 ;
				INTO CURSOR CURBLUE ;
				WHERE CURHOURS3.MGMTTYPE <> "W" ;
				GROUP BY 1, 2, 3 ;
				ORDER BY 1, 3 ;
				READWRITE

				SELECT CURWHITE
				COPY TO ( "F:\UTIL\HR\TGFSTAFFRPT\REPORTS\DETAIL\tgfwhitecollar" + lcSuffix + ".XLS" ) XL5
				SELECT CURBLUE
				COPY TO ( "F:\UTIL\HR\TGFSTAFFRPT\REPORTS\DETAIL\tgfblueecollar" + lcSuffix + ".XLS" ) XL5

				* summarize for part-time / full-ime
				SELECT TIMETYPE, ;
					SUM(FTE) AS FTE, ;
					SUM(HDCNT) AS HEADCNT ;
					FROM CURHOURS3 ;
					INTO CURSOR CURCNTTYPE ;
					GROUP BY 1 ;
					ORDER BY 1 ;
					READWRITE

				SELECT CURCNTTYPE
				COPY TO ( "F:\UTIL\HR\TGFSTAFFRPT\REPORTS\DETAIL\tgftimetype" + lcSuffix + ".XLS" ) XL5
				
*!*					IF llBadFTE THEN
*!*						.TrackProgress('=====> WARNING - there were some very HIGH FTEs!', LOGIT+SENDIT)
*!*						.cBADFTEs = "F:\UTIL\HR\TGFSTAFFRPT\REPORTS\DETAIL\HIGH_FTES" + lcSuffix + ".XLS"
*!*						SELECT CURHOURS3
*!*						COPY TO ( .cBADFTEs ) FOR (FTE > .nFTECeiling) XL5
*!*					ENDIF

				* summarize BY GENDER
				SELECT GENDER, ;
					SUM(FTE) AS FTE, ;
					SUM(HDCNT) AS HEADCNT ;
					FROM CURHOURS3 ;
					INTO CURSOR CURGENDER ;
					GROUP BY 1 ;
					ORDER BY 1 ;
					READWRITE

				SELECT CURGENDER
				COPY TO ( "F:\UTIL\HR\TGFSTAFFRPT\REPORTS\DETAIL\tgfgender"  + lcSuffix + ".XLS" ) XL5

				* round FTE values to one decimal place - per Ken K. 07/01/2015

				SELECT CURCNTTYPE
				SCAN
					REPLACE CURCNTTYPE.FTE WITH ROUND(CURCNTTYPE.FTE,1) IN CURCNTTYPE
				ENDSCAN

				SELECT CURGENDER
				SCAN
					REPLACE CURGENDER.FTE WITH ROUND(CURGENDER.FTE,1) IN CURGENDER
				ENDSCAN


				* populate the spreadsheet

				oWorksheet.RANGE("C1").VALUE = 'for Date Range: ' + DTOC(ldFromDate) + " - " + DTOC(ldToDate)

				oWorksheet.RANGE("A3").VALUE = "Mgmt. Type"
				oWorksheet.RANGE("B3").VALUE = "TGF Dept."
				oWorksheet.RANGE("C3").VALUE = "Dept. Description"
				oWorksheet.RANGE("D3").VALUE = "FTE"
				oWorksheet.RANGE("E3").VALUE = "Headcount"
				oWorksheet.RANGE("A3:E3").FONT.BOLD = .T.

				**** WHITE COLLAR

				SELECT CURWHITE

				LOCATE FOR ALLTRIM(TGFDEPT) == "RE"
				IF FOUND() THEN
					oWorksheet.RANGE("A4").VALUE = CURWHITE.MGMTTYPE
					oWorksheet.RANGE("B4").VALUE = CURWHITE.TGFDEPT
					oWorksheet.RANGE("C4").VALUE = CURWHITE.DEPTDESC
					oWorksheet.RANGE("D4").VALUE = CURWHITE.FTE
					oWorksheet.RANGE("E4").VALUE = CURWHITE.HEADCNT
				ENDIF

				LOCATE FOR ALLTRIM(TGFDEPT) == "BR"
				IF FOUND() THEN
					oWorksheet.RANGE("A5").VALUE = CURWHITE.MGMTTYPE
					oWorksheet.RANGE("B5").VALUE = CURWHITE.TGFDEPT
					oWorksheet.RANGE("C5").VALUE = CURWHITE.DEPTDESC
					oWorksheet.RANGE("D5").VALUE = CURWHITE.FTE
					oWorksheet.RANGE("E5").VALUE = CURWHITE.HEADCNT
				ENDIF

				LOCATE FOR ALLTRIM(TGFDEPT) == "AX"
				IF FOUND() THEN
					oWorksheet.RANGE("A6").VALUE = CURWHITE.MGMTTYPE
					oWorksheet.RANGE("B6").VALUE = CURWHITE.TGFDEPT
					oWorksheet.RANGE("C6").VALUE = CURWHITE.DEPTDESC
					oWorksheet.RANGE("D6").VALUE = CURWHITE.FTE
					oWorksheet.RANGE("E6").VALUE = CURWHITE.HEADCNT
				ENDIF

				LOCATE FOR ALLTRIM(TGFDEPT) == "AI"
				IF FOUND() THEN
					oWorksheet.RANGE("A7").VALUE = CURWHITE.MGMTTYPE
					oWorksheet.RANGE("B7").VALUE = CURWHITE.TGFDEPT
					oWorksheet.RANGE("C7").VALUE = CURWHITE.DEPTDESC
					oWorksheet.RANGE("D7").VALUE = CURWHITE.FTE
					oWorksheet.RANGE("E7").VALUE = CURWHITE.HEADCNT
				ENDIF

				LOCATE FOR ALLTRIM(TGFDEPT) == "SX"
				IF FOUND() THEN
					oWorksheet.RANGE("A8").VALUE = CURWHITE.MGMTTYPE
					oWorksheet.RANGE("B8").VALUE = CURWHITE.TGFDEPT
					oWorksheet.RANGE("C8").VALUE = CURWHITE.DEPTDESC
					oWorksheet.RANGE("D8").VALUE = CURWHITE.FTE
					oWorksheet.RANGE("E8").VALUE = CURWHITE.HEADCNT
				ENDIF

				LOCATE FOR ALLTRIM(TGFDEPT) == "SI"
				IF FOUND() THEN
					oWorksheet.RANGE("A9").VALUE = CURWHITE.MGMTTYPE
					oWorksheet.RANGE("B9").VALUE = CURWHITE.TGFDEPT
					oWorksheet.RANGE("C9").VALUE = CURWHITE.DEPTDESC
					oWorksheet.RANGE("D9").VALUE = CURWHITE.FTE
					oWorksheet.RANGE("E9").VALUE = CURWHITE.HEADCNT
				ENDIF

				IF (NOT (tcType = "H")) THEN
					LOCATE FOR ALLTRIM(TGFDEPT) == "TP"
					IF FOUND() THEN
						oWorksheet.RANGE("A10").VALUE = CURWHITE.MGMTTYPE
						oWorksheet.RANGE("B10").VALUE = CURWHITE.TGFDEPT
						oWorksheet.RANGE("C10").VALUE = CURWHITE.DEPTDESC
						oWorksheet.RANGE("D10").VALUE = CURWHITE.FTE
						oWorksheet.RANGE("E10").VALUE = CURWHITE.HEADCNT
					ENDIF
				ENDIF

				LOCATE FOR ALLTRIM(TGFDEPT) == "BK"
				IF FOUND() THEN
					oWorksheet.RANGE("A11").VALUE = CURWHITE.MGMTTYPE
					oWorksheet.RANGE("B11").VALUE = CURWHITE.TGFDEPT
					oWorksheet.RANGE("C11").VALUE = CURWHITE.DEPTDESC
					oWorksheet.RANGE("D11").VALUE = CURWHITE.FTE
					oWorksheet.RANGE("E11").VALUE = CURWHITE.HEADCNT
				ENDIF

				IF (NOT (tcType = "H")) THEN
				
					LOCATE FOR ALLTRIM(TGFDEPT) == "SP"
					IF FOUND() THEN
						oWorksheet.RANGE("A12").VALUE = CURWHITE.MGMTTYPE
						oWorksheet.RANGE("B12").VALUE = CURWHITE.TGFDEPT
						oWorksheet.RANGE("C12").VALUE = CURWHITE.DEPTDESC
						oWorksheet.RANGE("D12").VALUE = CURWHITE.FTE
						oWorksheet.RANGE("E12").VALUE = CURWHITE.HEADCNT
					ENDIF

					LOCATE FOR ALLTRIM(TGFDEPT) == "SL"
					IF FOUND() THEN
						oWorksheet.RANGE("A13").VALUE = CURWHITE.MGMTTYPE
						oWorksheet.RANGE("B13").VALUE = CURWHITE.TGFDEPT
						oWorksheet.RANGE("C13").VALUE = CURWHITE.DEPTDESC
						oWorksheet.RANGE("D13").VALUE = CURWHITE.FTE
						oWorksheet.RANGE("E13").VALUE = CURWHITE.HEADCNT
					ENDIF

					LOCATE FOR ALLTRIM(TGFDEPT) == "SS"
					IF FOUND() THEN
						oWorksheet.RANGE("A14").VALUE = CURWHITE.MGMTTYPE
						oWorksheet.RANGE("B14").VALUE = CURWHITE.TGFDEPT
						oWorksheet.RANGE("C14").VALUE = CURWHITE.DEPTDESC
						oWorksheet.RANGE("D14").VALUE = CURWHITE.FTE
						oWorksheet.RANGE("E14").VALUE = CURWHITE.HEADCNT
					ENDIF

					LOCATE FOR ALLTRIM(TGFDEPT) == "CA"
					IF FOUND() THEN
						oWorksheet.RANGE("A15").VALUE = CURWHITE.MGMTTYPE
						oWorksheet.RANGE("B15").VALUE = CURWHITE.TGFDEPT
						oWorksheet.RANGE("C15").VALUE = CURWHITE.DEPTDESC
						oWorksheet.RANGE("D15").VALUE = CURWHITE.FTE
						oWorksheet.RANGE("E15").VALUE = CURWHITE.HEADCNT
					ENDIF

					LOCATE FOR ALLTRIM(TGFDEPT) == "FI"
					IF FOUND() THEN
						oWorksheet.RANGE("A16").VALUE = CURWHITE.MGMTTYPE
						oWorksheet.RANGE("B16").VALUE = CURWHITE.TGFDEPT
						oWorksheet.RANGE("C16").VALUE = CURWHITE.DEPTDESC
						oWorksheet.RANGE("D16").VALUE = CURWHITE.FTE
						oWorksheet.RANGE("E16").VALUE = CURWHITE.HEADCNT
					ENDIF

					LOCATE FOR ALLTRIM(TGFDEPT) == "HR"
					IF FOUND() THEN
						oWorksheet.RANGE("A17").VALUE = CURWHITE.MGMTTYPE
						oWorksheet.RANGE("B17").VALUE = CURWHITE.TGFDEPT
						oWorksheet.RANGE("C17").VALUE = CURWHITE.DEPTDESC
						oWorksheet.RANGE("D17").VALUE = CURWHITE.FTE
						oWorksheet.RANGE("E17").VALUE = CURWHITE.HEADCNT
					ENDIF

					LOCATE FOR ALLTRIM(TGFDEPT) == "IT"
					IF FOUND() THEN
						oWorksheet.RANGE("A18").VALUE = CURWHITE.MGMTTYPE
						oWorksheet.RANGE("B18").VALUE = CURWHITE.TGFDEPT
						oWorksheet.RANGE("C18").VALUE = CURWHITE.DEPTDESC
						oWorksheet.RANGE("D18").VALUE = CURWHITE.FTE
						oWorksheet.RANGE("E18").VALUE = CURWHITE.HEADCNT
					ENDIF

					LOCATE FOR ALLTRIM(TGFDEPT) == "PS"
					IF FOUND() THEN
						oWorksheet.RANGE("A19").VALUE = CURWHITE.MGMTTYPE
						oWorksheet.RANGE("B19").VALUE = CURWHITE.TGFDEPT
						oWorksheet.RANGE("C19").VALUE = CURWHITE.DEPTDESC
						oWorksheet.RANGE("D19").VALUE = CURWHITE.FTE
						oWorksheet.RANGE("E19").VALUE = CURWHITE.HEADCNT
					ENDIF

					**** BLUE COLLAR

					SELECT CURBLUE

					LOCATE FOR ALLTRIM(TGFDEPT) == "TP"
					IF FOUND() THEN
						oWorksheet.RANGE("A20").VALUE = CURBLUE.MGMTTYPE
						oWorksheet.RANGE("B20").VALUE = CURBLUE.TGFDEPT
						oWorksheet.RANGE("C20").VALUE = CURBLUE.DEPTDESC
						oWorksheet.RANGE("D20").VALUE = CURBLUE.FTE
						oWorksheet.RANGE("E20").VALUE = CURBLUE.HEADCNT
					ENDIF

					LOCATE FOR ALLTRIM(TGFDEPT) == "SP"
					IF FOUND() THEN
						oWorksheet.RANGE("A21").VALUE = CURBLUE.MGMTTYPE
						oWorksheet.RANGE("B21").VALUE = CURBLUE.TGFDEPT
						oWorksheet.RANGE("C21").VALUE = CURBLUE.DEPTDESC
						oWorksheet.RANGE("D21").VALUE = CURBLUE.FTE
						oWorksheet.RANGE("E21").VALUE = CURBLUE.HEADCNT
					ENDIF
				
				ENDIF  &&  (NOT (tcType = "H"))

				* totals for the Mgmt Type section...
				oWorksheet.RANGE("D22").VALUE = "=SUM(D4:D21)"
				oWorksheet.RANGE("E22").VALUE = "=SUM(E4:E21)"

				* UNDERLINE cell columns to be totaled...
				oWorksheet.RANGE("D21:E21").BORDERS(xlEdgeBottom).LineStyle = xlContinuous
				oWorksheet.RANGE("D21:E21").BORDERS(xlEdgeBottom).Weight = xlMedium

				IF (NOT (tcType = "H")) THEN

					**** HEADCOUNT BY TYPE

					oWorksheet.RANGE("A24").VALUE = "Emp. Type"
					oWorksheet.RANGE("D24").VALUE = "FTE"
					oWorksheet.RANGE("E24").VALUE = "Headcount"
					oWorksheet.RANGE("A24:E24").FONT.BOLD = .T.

					SELECT CURCNTTYPE

					LOCATE FOR ALLTRIM(TIMETYPE) == "F"
					IF FOUND() THEN
						oWorksheet.RANGE("A25").VALUE = .GetTimeTypeDesc( CURCNTTYPE.TIMETYPE )  && IIF(CURCNTTYPE.TIMETYPE="F","Full Time","Part Time")
						oWorksheet.RANGE("D25").VALUE = CURCNTTYPE.FTE
						oWorksheet.RANGE("E25").VALUE = CURCNTTYPE.HEADCNT
					ENDIF

					LOCATE FOR ALLTRIM(TIMETYPE) == "C"
					IF FOUND() THEN
						oWorksheet.RANGE("A26").VALUE = .GetTimeTypeDesc( CURCNTTYPE.TIMETYPE )  && IIF(CURCNTTYPE.TIMETYPE="F","Full Time","Part Time")
						oWorksheet.RANGE("D26").VALUE = CURCNTTYPE.FTE
						oWorksheet.RANGE("E26").VALUE = CURCNTTYPE.HEADCNT
					ENDIF


					**** HEADCOUNT BY GENDER

					oWorksheet.RANGE("A28").VALUE = "Gender"
					oWorksheet.RANGE("D28").VALUE = "FTE"
					oWorksheet.RANGE("E28").VALUE = "Headcount"
					oWorksheet.RANGE("A28:E28").FONT.BOLD = .T.

					SELECT CURGENDER

					LOCATE FOR ALLTRIM(GENDER) == "M"
					IF FOUND() THEN
						oWorksheet.RANGE("A29").VALUE = CURGENDER.GENDER
						oWorksheet.RANGE("D29").VALUE = CURGENDER.FTE
						oWorksheet.RANGE("E29").VALUE = CURGENDER.HEADCNT
					ENDIF

					LOCATE FOR ALLTRIM(GENDER) == "F"
					IF FOUND() THEN
						oWorksheet.RANGE("A30").VALUE = CURGENDER.GENDER
						oWorksheet.RANGE("D30").VALUE = CURGENDER.FTE
						oWorksheet.RANGE("E30").VALUE = CURGENDER.HEADCNT
					ENDIF

					LOCATE FOR ALLTRIM(GENDER) == "U"
					IF FOUND() THEN
						oWorksheet.RANGE("A31").VALUE = CURGENDER.GENDER
						oWorksheet.RANGE("D31").VALUE = CURGENDER.FTE
						oWorksheet.RANGE("E31").VALUE = CURGENDER.HEADCNT
					ENDIF
				
				ENDIF  &&  (NOT (tcType = "H"))


				* add FTE by Worksite info on ALL tab, for Todd Wilen
				IF (tcMode = "ALL") THEN

					IF USED('CURFTEW') THEN
						USE IN CURFTEW
					ENDIF

					SELECT WRKSITE, ;
						SPACE(30) AS WRKDESC, ;
						SUM( IIF(ADP_COMP = 'TMP',FTE,0.00) ) AS TEMPFTE, ;
						SUM( IIF(ADP_COMP <> 'TMP',FTE,0.00) ) AS EEFTE, ;
						SUM( FTE ) AS TOTFTE, ;
						SUM( IIF(ADP_COMP = 'TMP',HDCNT,0.00) ) AS TEMPHCNT, ;
						SUM( IIF(ADP_COMP <> 'TMP',HDCNT,0.00) ) AS EEHCNT, ;
						SUM(HDCNT) AS TOTHCNT ;
						FROM CURHOURS3 ;
						INTO CURSOR CURFTEW ;
						GROUP BY WRKSITE ;
						ORDER BY WRKSITE ;
						READWRITE
						
*!*						SELECT CURFTEW
*!*						SCAN
*!*						
*!*							IF USED('CURHEADCNT') THEN
*!*								USE IN CURHEADCNT
*!*							ENDIF
*!*							
*!*							SELECT COUNT(DISTINCT FILE_NUM) AS HEADCNT ;
*!*							FROM CURHOURS ;
*!*							INTO CURSOR CURHEADCNT ;
*!*							WHERE ADP_COMP = 'TMP' ;
*!*							AND WRKSITE = CURFTEW.WRKSITE
*!*							
*!*							REPLACE CURFTEW.TEMPHCNT WITH CURHEADCNT.HEADCNT
*!*							
*!*							IF USED('CURHEADCNT') THEN
*!*								USE IN CURHEADCNT
*!*							ENDIF
*!*							
*!*							SELECT COUNT(DISTINCT FILE_NUM) AS HEADCNT ;
*!*							FROM CURHOURS ;
*!*							INTO CURSOR CURHEADCNT ;
*!*							WHERE ADP_COMP <> 'TMP' ;
*!*							AND WRKSITE = CURFTEW.WRKSITE
*!*							
*!*							REPLACE CURFTEW.EEHCNT WITH CURHEADCNT.HEADCNT
*!*							
*!*							
*!*							IF USED('CURHEADCNT') THEN
*!*								USE IN CURHEADCNT
*!*							ENDIF
*!*							
*!*							SELECT COUNT(DISTINCT FILE_NUM) AS HEADCNT ;
*!*							FROM CURHOURS ;
*!*							INTO CURSOR CURHEADCNT ;
*!*							WHERE WRKSITE = CURFTEW.WRKSITE
*!*							
*!*							REPLACE CURFTEW.TOTHCNT WITH CURHEADCNT.HEADCNT
*!*						
*!*						ENDSCAN	
					
					*SET STEP ON 
						
					
					* add worksite descriptions
					IF NOT USED('WORKSITES') THEN
						USE F:\UTIL\INSPERITY\DATA\WORKSITES.DBF IN 0 ALIAS WORKSITES
					ENDIF

					SELECT CURFTEW
					SCAN
						SELECT WORKSITES
						LOCATE FOR UPPER(ALLTRIM(WWORKSITE)) == UPPER(ALLTRIM(CURFTEW.WRKSITE))
						IF FOUND() THEN
							REPLACE CURFTEW.WRKDESC WITH WORKSITES.LOCNAME IN CURFTEW
						ENDIF
					ENDSCAN
					
					*SELECT CURFTEW
					*BROWSE
					
					* populate spreadsheet
					SELECT CURFTEW
					lnRow = 3
					SCAN
						lnRow = lnRow + 1
						lcRow = ALLTRIM(STR(lnRow))
						oWorksheet.RANGE("H" + lcRow).VALUE = ALLTRIM(CURFTEW.WRKSITE) + " - " + ALLTRIM(CURFTEW.WRKDESC)
						oWorksheet.RANGE("I" + lcRow).VALUE = CURFTEW.TEMPFTE
						oWorksheet.RANGE("J" + lcRow).VALUE = CURFTEW.EEFTE
						oWorksheet.RANGE("K" + lcRow).VALUE = CURFTEW.TOTFTE
						oWorksheet.RANGE("L" + lcRow).VALUE = CURFTEW.TEMPHCNT
						oWorksheet.RANGE("M" + lcRow).VALUE = CURFTEW.EEHCNT
						oWorksheet.RANGE("N" + lcRow).VALUE = CURFTEW.TOTHCNT
					ENDSCAN

					* UNDERLINE cell columns to be totaled...
					oWorksheet.RANGE("I" + lcRow + ":N" + lcRow).BORDERS(xlEdgeBottom).LineStyle = xlContinuous
					oWorksheet.RANGE("I" + lcRow + ":N" + lcRow).BORDERS(xlEdgeBottom).Weight = xlMedium
					
					lcLastFTERow = lcRow

					* totals ...
					lnRow = lnRow + 1
					lcRow = ALLTRIM(STR(lnRow))
					oWorksheet.RANGE("I" + lcRow).VALUE = "=SUM(I4:I" + lcLastFTERow + ")"
					oWorksheet.RANGE("J" + lcRow).VALUE = "=SUM(J4:J" + lcLastFTERow + ")"
					oWorksheet.RANGE("K" + lcRow).VALUE = "=SUM(K4:K" + lcLastFTERow + ")"
					oWorksheet.RANGE("L" + lcRow).VALUE = "=SUM(L4:L" + lcLastFTERow + ")"
					oWorksheet.RANGE("M" + lcRow).VALUE = "=SUM(M4:M" + lcLastFTERow + ")"
					oWorksheet.RANGE("N" + lcRow).VALUE = "=SUM(N4:N" + lcLastFTERow + ")"


				ENDIF && tcMode = "ALL"

				IF (tcType ="H") THEN
					* Delete all but the GF tab
					oWorksheet = oWorkbook.Worksheets[4]
					oWorksheet.delete()
					oWorksheet = oWorkbook.Worksheets[3]
					oWorksheet.delete()
					oWorksheet = oWorkbook.Worksheets[2]
					oWorksheet.delete()
					* SAVE AND QUIT EXCEL
					oWorkbook.SAVE()
					oExcel.QUIT()
					oWorkbook = NULL
					oExcel = NULL
					IF FILE(lcFiletoSaveAs) THEN
						.cAttach = lcFiletoSaveAs
						.cBodyText = "See attached report." + CRLF + CRLF + "<<REPORT LOG FOLLOWS>>" + CRLF + .cBodyText
					ELSE
						.TrackProgress("ERROR attaching " + lcFiletoSaveAs, LOGIT+SENDIT+NOWAITIT)
					ENDIF
				ENDIF

				* this block must get done only once, at the end of the process...
				IF (tcMode = "ALL") THEN

					* SAVE AND QUIT EXCEL
					oWorkbook.SAVE()
					oExcel.QUIT()
					oWorkbook = NULL
					oExcel = NULL

					DO CASE

						CASE tcType ="K"
						
							IF FILE(lcFiletoSaveAs) THEN
								.cAttach = lcFiletoSaveAs

								IF FILE(.cDetailFileForKen) THEN
									* add the detail file per Ken 12/10/12 MB
									.cAttach = .cAttach + "," + .cDetailFileForKen
								ELSE
									.TrackProgress("ERROR attaching: " + .cDetailFileForKen, LOGIT+SENDIT+NOWAITIT)
								ENDIF

								*!*	* BAD FTE FILE IS OPTIONAL AND WILL BE EMPTY NAME IF THERE ARE NO BAD FTES
								*!*	IF (NOT EMPTY(.cBADFTEs)) THEN
								*!*		IF FILE(.cBADFTEs) THEN
								*!*			* add the detail file per Ken 12/10/12 MB
								*!*			.cAttach = .cAttach + "," + .cBADFTEs
								*!*		ELSE
								*!*			.TrackProgress("ERROR attaching: " + .cBADFTEs, LOGIT+SENDIT+NOWAITIT)
								*!*		ENDIF
								*!*	ENDIF

								*!*	IF FILE(lcHeadCountBasis) THEN
								*!*		* add the adjusted headcount basis file per Ken 11/15/13 MB
								*!*		.cAttach = .cAttach + "," + lcHeadCountBasis
								*!*	ELSE
								*!*		.TrackProgress("ERROR attaching: " + lcHeadCountBasis, LOGIT+SENDIT+NOWAITIT)
								*!*	ENDIF

								.cBodyText = "See attached report." + CRLF + CRLF + "<<REPORT LOG FOLLOWS>>" + CRLF + .cBodyText
							ELSE
								.TrackProgress("ERROR attaching " + lcFiletoSaveAs, LOGIT+SENDIT+NOWAITIT)
							ENDIF

						OTHERWISE
							* NOTHING
					ENDCASE

					IF NOT EMPTY(.cBadData) THEN
						.cBadData = 'WARNING: the following people had bad or missing data elements:' + CRLF + CRLF + ;
							'ADP_COMP, NAME, DEPT, FILE_NUM, MGMTTYPE, TGFDEPT, DEPTDESC' + CRLF + .cBadData
						.cBodyText = .cBadData + CRLF + .cBodyText
					ENDIF
					
					IF .lThereWereBadFTEs THEN
						.cBodyText = "=====> WARNING: check the FTEs in " + .cBadFTEFilename + CRLF + .cBodyText
					ENDIF

				ENDIF && tcMode = "ALL"


				.TrackProgress("FTE Info to Qlikview process ended normally.", LOGIT+SENDIT+NOWAITIT)


			CATCH TO loError

				.TrackProgress('There was an error.',LOGIT+SENDIT)
				.TrackProgress('The Error # is: ' + TRANSFORM(loError.ERRORNO), LOGIT+SENDIT)
				.TrackProgress('The Error Message is: ' + TRANSFORM(loError.MESSAGE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred in Program: ' + TRANSFORM(loError.PROCEDURE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred at line #: ' + TRANSFORM(loError.LINENO), LOGIT+SENDIT)
				lnNumberOfErrors = lnNumberOfErrors + 1

				IF (tcMode = "ALL") THEN
					IF TYPE('oWorkbook') = "O" THEN
						oWorkbook.SAVE()
					ENDIF
					IF TYPE('oExcel') = "O" THEN
						oExcel.QUIT()
					ENDIF
				ENDIF

			ENDTRY

			WAIT CLEAR
			***************** INTERNAL email results ******************************

			* this block must get done only once, after final error trapping...
			*IF (tcMode = "ALL") OR (tcType ="H") THEN
			IF (tcMode = "ALL") OR (tcType ="H") OR glFatalError THEN  && 1/16/201 MB

				.TrackProgress('About to send status email.',LOGIT)
				.TrackProgress('The logfile is: ' + .cLogFile, SENDIT)
				.TrackProgress("FTE Info process started: " + .cStartTime, LOGIT+SENDIT)
				.TrackProgress("FTE Info process finished: " + TTOC(DATETIME()), LOGIT+SENDIT)

				IF .lSendInternalEmailIsOn THEN
					DO FORM dartmail2 WITH .cSendTo,.cFrom,.cSubject,.cCC,.cAttach,.cBodyText,"A"
					.TrackProgress('Sent status email.',LOGIT)
				ELSE
					.TrackProgress('Did not send status email: THIS.lSendInternalEmailIsOn = FALSE.',LOGIT)
				ENDIF

			ENDIF && tcMode = "ALL"

		ENDWITH
		RETURN
	ENDFUNC && main



*!*		PROCEDURE GetAdjustedHeadcounts
*!*			LPARAMETERS tcSuffix
*!*			WITH THIS

*!*				IF USED('CURHOURLYHEADCNT') THEN
*!*					USE IN CURHOURLYHEADCNT
*!*				ENDIF

*!*				SELECT ADP_COMP, ;
*!*					PAYDATE, ;
*!*					NAME, ;
*!*					INT(VAL(PERSONNUM)) AS FILE_NUM, ;
*!*					DIVISION, ;
*!*					DEPT, ;
*!*					MGMTTYPE, ;
*!*					TGFDEPT ;
*!*					FROM CURHOURLYHEADCNTPRE ;
*!*					INTO CURSOR CURHOURLYHEADCNT ;
*!*					ORDER BY PERSONNUM ;
*!*					READWRITE


*!*				*!*				SELECT CURHOURLYHEADCNT
*!*				*!*				BROWSE
*!*				*!*				SELECT CURSALARIEDHEADCNT
*!*				*!*				BROWSE

*!*				SELECT CURHOURLYHEADCNT
*!*				APPEND FROM DBF('CURSALARIEDHEADCNT')

*!*				SELECT CURHOURLYHEADCNT
*!*				APPEND FROM DBF('CUROTRHEADCNT')

*!*				IF USED('CURDISTINCT') THEN
*!*					USE IN CURDISTINCT
*!*				ENDIF

*!*				SELECT DISTINCT ;
*!*					A.FILE_NUM AS FILE_NUM, ;
*!*					(SELECT MAX(PAYDATE) FROM CURHOURLYHEADCNT WHERE FILE_NUM == A.FILE_NUM) AS LASTPAY ;
*!*					FROM CURHOURLYHEADCNT A ;
*!*					INTO CURSOR CURDISTINCT ;
*!*					ORDER BY A.FILE_NUM

*!*				*!*	SELECT CURDISTINCT
*!*				*!*	browse

*!*				IF USED('CURFINALHEADCNT') THEN
*!*					USE IN CURFINALHEADCNT
*!*				ENDIF

*!*				*  addded 01/18/2018 MB
*!*				IF .lAllocateDepts THEN
*!*					* new way - supporting fractional headcounts
*!*					SELECT DISTINCT A.*, ;
*!*						B.LASTPAY, 1.00 AS HDCNT ;
*!*						FROM CURHOURLYHEADCNT A ;
*!*						INNER JOIN ;
*!*						CURDISTINCT B ;
*!*						ON A.FILE_NUM == B.FILE_NUM ;
*!*						AND A.PAYDATE = B.LASTPAY ;
*!*						INTO CURSOR CURFINALHEADCNT ;
*!*						ORDER BY A.FILE_NUM ;
*!*						READWRITE
*!*				ELSE
*!*					* old way - using integer headcounts
*!*					SELECT DISTINCT A.*, ;
*!*						B.LASTPAY ;
*!*						FROM CURHOURLYHEADCNT A ;
*!*						INNER JOIN ;
*!*						CURDISTINCT B ;
*!*						ON A.FILE_NUM == B.FILE_NUM ;
*!*						AND A.PAYDATE = B.LASTPAY ;
*!*						INTO CURSOR CURFINALHEADCNT ;
*!*						ORDER BY A.FILE_NUM ;
*!*						READWRITE				
*!*				ENDIF  && .lAllocateDepts

*!*	*!*	SELECT CURFINALHEADCNT
*!*	*!*	browse

*!*				* populate tgf depts from the new dept translation table
*!*				SELECT CURFINALHEADCNT
*!*				SCAN
*!*					lcDept = ALLTRIM(CURFINALHEADCNT.DEPT)
*!*					SELECT DEPTLOOKUPTABLE
*!*					LOCATE FOR DEPT = lcDept
*!*					IF FOUND() THEN
*!*						REPLACE CURFINALHEADCNT.TGFDEPT WITH DEPTLOOKUPTABLE.HCFTECODE ;
*!*							IN CURFINALHEADCNT
*!*					ENDIF
*!*				ENDSCAN

*!*	* Took this out from SCAN above 12/05/2017 MB
*!*	* This is to support HR's entering mixed collartypes (MGMTTYPE) within departments in TimeStar.
*!*	* So removing this will allow us to keep the latest MGMTTYPE per person from the timedata,
*!*	* and not overwrite it with the MGMTTYPE defined in DEPTLOOKUPTABLE.
*!*	*
*!*	*			REPLACE CURFINALHEADCNT.MGMTTYPE WITH DEPTLOOKUPTABLE.COLLARTYPE, ;



*!*	*************************************************************************
*!*	*** adjust headcount table using the allocation table here!
*!*	*

*!*				SELECT CURFINALHEADCNT
*!*				COPY TO ( "F:\UTIL\HR\TGFSTAFFRPT\REPORTS\DETAIL\CURFINALHEADCNT" + tcSuffix )

*!*				SELECT CURFINALHEADCNT
*!*				LOCATE

*!*				*!*	SELECT CURFINALHEADCNT
*!*				*!*	browse

*!*				IF USED('CURWHITEADJ') THEN
*!*					USE IN CURWHITEADJ
*!*				ENDIF
*!*				IF USED('CURBLUEADJ') THEN
*!*					USE IN CURBLUEADJ
*!*				ENDIF

*!*				*  addded 01/18/2018 MB
*!*				IF .lAllocateDepts THEN
*!*					* new way - supporting fractional headcounts

*!*					* summarize for White Collar and Blue Collar adjustments - NEW METHOD
*!*					SELECT "WHITE COLLAR" AS MGMTTYPE, ;
*!*						TGFDEPT, ;
*!*						SUM(HDCNT) AS ADJHDCNT ;
*!*						FROM CURFINALHEADCNT ;
*!*						INTO CURSOR CURWHITEADJ ;
*!*						WHERE CURFINALHEADCNT.MGMTTYPE = "W" ;
*!*						GROUP BY 1, 2 ;
*!*						ORDER BY 1, 2

*!*					SELECT "BLUE COLLAR" AS MGMTTYPE, ;
*!*						TGFDEPT, ;
*!*						SUM(HDCNT) AS ADJHDCNT ;
*!*						FROM CURFINALHEADCNT ;
*!*						INTO CURSOR CURBLUEADJ ;
*!*						WHERE CURFINALHEADCNT.MGMTTYPE <> "W" ;
*!*						GROUP BY 1, 2 ;
*!*						ORDER BY 1, 2
*!*					
*!*					
*!*				ELSE
*!*					* old way - using integer headcounts

*!*					* summarize for White Collar and Blue Collar adjustments - NEW METHOD
*!*					SELECT "WHITE COLLAR" AS MGMTTYPE, ;
*!*						TGFDEPT, ;
*!*						COUNT(DISTINCT FILE_NUM) AS ADJHDCNT ;
*!*						FROM CURFINALHEADCNT ;
*!*						INTO CURSOR CURWHITEADJ ;
*!*						WHERE CURFINALHEADCNT.MGMTTYPE = "W" ;
*!*						GROUP BY 1, 2 ;
*!*						ORDER BY 1, 2

*!*					SELECT "BLUE COLLAR" AS MGMTTYPE, ;
*!*						TGFDEPT, ;
*!*						COUNT(DISTINCT FILE_NUM) AS ADJHDCNT ;
*!*						FROM CURFINALHEADCNT ;
*!*						INTO CURSOR CURBLUEADJ ;
*!*						WHERE CURFINALHEADCNT.MGMTTYPE <> "W" ;
*!*						GROUP BY 1, 2 ;
*!*						ORDER BY 1, 2
*!*					
*!*				ENDIF  && .lAllocateDepts


*!*				* summarize for White Collar and Blue Collar - OLD METHOD  
*!*				SELECT "WHITE COLLAR" AS MGMTTYPE, ;
*!*					TGFDEPT, ;
*!*					DEPTDESC, ;
*!*					SUM(FTE) AS FTE, ;
*!*					COUNT(DISTINCT FILE_NUM) AS HEADCNT, ;
*!*					0000 AS ADJHDCNT, ;
*!*					SUM(TOTHOURS) AS TOTHOURS ;
*!*					FROM CURHOURS3 ;
*!*					INTO CURSOR CURWHITE ;
*!*					WHERE CURHOURS3.MGMTTYPE = "W" ;
*!*					GROUP BY 1, 2, 3 ;
*!*					ORDER BY 1, 3 ;
*!*					READWRITE

*!*				SELECT "BLUE COLLAR" AS MGMTTYPE, ;
*!*					TGFDEPT, ;
*!*					DEPTDESC, ;
*!*					SUM(FTE) AS FTE, ;
*!*					COUNT(DISTINCT FILE_NUM) AS HEADCNT, ;
*!*					0000 AS ADJHDCNT, ;
*!*					SUM(TOTHOURS) AS TOTHOURS ;
*!*					FROM CURHOURS3 ;
*!*					INTO CURSOR CURBLUE ;
*!*					WHERE CURHOURS3.MGMTTYPE <> "W" ;
*!*					GROUP BY 1, 2, 3 ;
*!*					ORDER BY 1, 3 ;
*!*					READWRITE

*!*				*!*	SELECT CURWHITE
*!*				*!*	BROWSE
*!*				*!*	SELECT CURWHITEADJ
*!*				*!*	BROWSE

*!*				*** NOW
*!*				* adjust or 'correct' the OLD headcounts, eliminating duplicates caused by people being in more than 1 dept in the period
*!*				SELECT CURWHITE
*!*				SCAN
*!*					SELECT CURWHITEADJ
*!*					LOCATE FOR UPPER(ALLTRIM(TGFDEPT)) == UPPER(ALLTRIM(CURWHITE.TGFDEPT))
*!*					IF FOUND() THEN
*!*						REPLACE CURWHITE.ADJHDCNT WITH CURWHITEADJ.ADJHDCNT IN CURWHITE
*!*					ENDIF
*!*				ENDSCAN

*!*				*!*				SELECT CURWHITE
*!*				*!*				BROWSE

*!*				SELECT CURBLUE
*!*				SCAN
*!*					SELECT CURBLUEADJ
*!*					LOCATE FOR UPPER(ALLTRIM(TGFDEPT)) == UPPER(ALLTRIM(CURBLUE.TGFDEPT))
*!*					IF FOUND() THEN
*!*						REPLACE CURBLUE.ADJHDCNT WITH CURBLUEADJ.ADJHDCNT IN CURBLUE
*!*					ENDIF
*!*				ENDSCAN

*!*				*!*				SELECT CURBLUE
*!*				*!*				BROWSE

*!*				******************************************************************************************
*!*				******************************************************************************************
*!*				******************************************************************************************
*!*				*
*!*				* round FTE values to one decimal place - per Ken K. 07/01/2015

*!*				SELECT CURWHITE
*!*				SCAN
*!*					REPLACE CURWHITE.FTE WITH ROUND(CURWHITE.FTE,1) IN CURWHITE
*!*				ENDSCAN

*!*				SELECT CURBLUE
*!*				SCAN
*!*					REPLACE CURBLUE.FTE WITH ROUND(CURBLUE.FTE,1) IN CURBLUE
*!*				ENDSCAN

*!*				******************************************************************************************
*!*				******************************************************************************************
*!*				******************************************************************************************


*!*			ENDWITH
*!*		ENDPROC



	FUNCTION GetTimeTypeDesc
		LPARAMETERS tcTimeType
		LOCAL lcRetVal
		lcRetVal = "Unknown"
		DO CASE
			CASE tcTimeType = "F"
				lcRetVal = "Full Time"
			CASE tcTimeType = "P"
				lcRetVal = "Part Time"
			CASE tcTimeType = "C"
				lcRetVal = "Casual"
			CASE tcTimeType = "O"
				lcRetVal = "Contract"
		ENDCASE

		RETURN lcRetVal
	ENDFUNC  &&  GetTimeTypeDesc



	FUNCTION ExecSQL
		LPARAMETERS tcSQL, tcCursorName, tlNoDataReturnedIsOkay
		LOCAL llRetval, lnResult
		WITH THIS
			* close target cursor if it's open
			IF USED(tcCursorName)
				USE IN (tcCursorName)
			ENDIF
			WAIT WINDOW "Fetching remote data; please wait..." NOWAIT
			lnResult = SQLEXEC(.nSQLHandle, tcSQL, tcCursorName)
			llRetval = ( lnResult > 0 )
			IF llRetval THEN
				* see if any data came back
				IF NOT tlNoDataReturnedIsOkay THEN
					IF (NOT USED(tcCursorName)) OR EOF(tcCursorName) THEN
						llRetval = .F.
						.TrackProgress('No data was returned by this query: ' +CRLF + tcSQL, LOGIT+SENDIT)
						*.cSendTo = 'Mark Bennett <mbennett@fmiint.com>'
					ENDIF
				ENDIF
			ELSE
				.TrackProgress("Result Code [" + TRANSFORM(lnResult) + "] returned by this query: " + tcSQL, LOGIT+SENDIT)
				*.cSendTo = 'Mark Bennett <mbennett@fmiint.com>'
			ENDIF
			WAIT CLEAR
			RETURN llRetval
		ENDWITH
	ENDFUNC


	PROCEDURE TrackProgress
		* do any combination of Wait Window, writing to logfile, and adding to body of email,
		* based on nFlags parameter.
		LPARAMETERS tcExpression, tnFlags
		WITH THIS
			IF BITAND(tnFlags,LOGIT) = LOGIT THEN
				IF .lLoggingIsOn THEN
					?
					? .cProcessName + "  " + TTOC(DATETIME()) + ": " + tcExpression
				ENDIF
			ENDIF
			IF .lWaitWindowIsOn THEN
				IF BITAND(tnFlags,NOWAITIT) = NOWAITIT THEN
					WAIT WINDOW tcExpression NOWAIT
				ENDIF
				IF BITAND(tnFlags,WAITIT) = WAITIT THEN
					WAIT WINDOW tcExpression TIMEOUT .nWaitWindowTimeout
				ENDIF
			ENDIF
			IF BITAND(tnFlags,SENDIT) = SENDIT THEN
				IF .lSendInternalEmailIsOn THEN
					.cBodyText = .cBodyText + tcExpression + CRLF + CRLF
				ENDIF
			ENDIF
		ENDWITH
	ENDPROC  &&  TrackProgress


	FUNCTION GetSQLStartDateTimeString
		* returns a timestamp string for use in SQL queries
		LPARAMETERS tdDate
		LOCAL lcSetDate, lcSQLDateTimeString
		lcSetDate = SET('DATE')
		SET DATE YMD
		lcSQLDateTimeString   = "{ts '" + TRANSFORM(YEAR(tdDate)) + "-" + PADL(MONTH(tdDate),2,"0") + "-" + PADL(DAY(tdDate),2,"0") + " 00:00:00.0'}"
		SET DATE &lcSetDate.
		RETURN lcSQLDateTimeString
	ENDFUNC


	FUNCTION GetSQLEndDateTimeString
		* returns a timestamp string for use in SQL queries
		LPARAMETERS tdDate
		LOCAL lcSetDate, lcSQLDateTimeString
		lcSetDate = SET('DATE')
		SET DATE YMD
		lcSQLDateTimeString = "{ts '" + TRANSFORM(YEAR(tdDate)) + "-" + PADL(MONTH(tdDate),2,"0") + "-" + PADL(DAY(tdDate),2,"0") + " 23:59:59.9'}"
		SET DATE &lcSetDate.
		RETURN lcSQLDateTimeString
	ENDFUNC


ENDDEFINE
