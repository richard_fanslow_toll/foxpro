*!* Checks Marc Jacobs 945s for duplicates, deletes the dupes, then moves the remaining files to the OUT folder
SET EXCLUSIVE OFF 
SET DELETED ON 
SET TALK OFF  
SET SAFETY OFF
CLOSE DATA ALL
CLEAR

lTesting = .F.
lOverrideBusy = .T. && lTesting
DO m:\dev\prg\_setvars WITH lTesting

IF !lTesting
	SELECT 0
	USE F:\edirouting\FTPSETUP SHARED
	LOCATE FOR FTPSETUP.TRANSFER = "MJ-DUPECHK"
	IF FTPSETUP.chkbusy AND !lOverrideBusy
		WAIT WINDOW '940 Busy...try again later' TIMEOUT 2
		CLOSE DATABASES ALL
		CANCEL
		RETURN
	ENDIF
	REPLACE FTPSETUP.chkbusy WITH .T. FOR FTPSETUP.TRANSFER = "MJ-DUPECHK" IN FTPSETUP
ENDIF
WAIT WINDOW "" TIMEOUT 1

lcPath = IIF(lTesting,"F:\FTPUSERS\MarcJacobs\945-Staging\945-TestStaging\","F:\FTPUSERS\MarcJacobs\945-Staging\")
lcOutPath = IIF(lTesting,"F:\FTPUSERS\MarcJacobs\945-Staging\testout\","F:\FTPUSERS\MJ_Wholesale\OUT\")

CD &lcPath
lendir = ADIR(ary1,"*.txt")
IF lendir > 0
	WAIT WINDOW "Number of files to check: "+TRANSFORM(lendir) TIMEOUT 1
ELSE
	WAIT WINDOW "No files are stuck in folders" TIMEOUT 2
	closeftpsetup()
	CLOSE DATA ALL
	RETURN
ENDIF

SELECT 0
IF lTesting
	USE F:\3pl\DATA\mj_dupeboldaily EXCL
	*ZAP
	USE
ENDIF
USE F:\3pl\DATA\mj_dupeboldaily ALIAS dupes
DELETE FOR dupes.DATE < DATE() - 10

m.date = DATE()

SELECT 0
CREATE CURSOR temp1 (bol_no c(20),filename c(25),dtstamp c(14))
FOR zq = 1 TO lendir
	cFilename = ALLTRIM(ary1[zq,1])
	m.dtstamp = SUBSTR(cFilename,5,14)
	STORE cFilename TO m.filename
	xfile = (lcPath+cFilename)
	DO m:\dev\prg\createx856a
	DO m:\dev\prg\loadedifile WITH xfile,"*","TILDE","MARCJACOBS"

	SELECT x856
	APPEND FROM [&xfile] TYPE DELIMITED WITH CHARACTER "*"
	LOCATE
	IF lTesting
		*		BROWSE
	ENDIF
	LOCATE FOR x856.segment = "W06"
	m.bol_no = ALLT(x856.f4)
	INSERT INTO temp1 FROM MEMVAR
ENDFOR


*!* Next section checks for prior instances of the various BOLs
nDel = 0
SELECT temp1
SCAN
	IF SEEK(PADR(ALLTRIM(temp1.bol_no),20),'dupes','bol_no')
		IF dupes.DATE = DATE()
			xfile = lcPath+ALLT(temp1.filename)
			WAIT WINDOW "Deleting duplicate file "+ALLT(temp1.filename) TIMEOUT 2
			DELETE FILE [&xfile]
			DELETE NEXT 1 IN temp1
			nDel = nDel+1
			LOOP
		ELSE
			INSERT INTO dupes (bol_no,DATE) VALUES (temp1.bol_no,DATE())
		ENDIF
	ELSE
		INSERT INTO dupes (bol_no,DATE,DATETIME) VALUES (temp1.bol_no,DATE(),DATETIME())
	ENDIF
ENDSCAN

WAIT WINDOW "There were "+IIF(nDel > 0,TRANSFORM(nDel),"NO")+" duplicate files deleted" TIMEOUT 2

IF lTesting
	SELECT temp1
	LOCATE
	BROWSE
ENDIF

SET STEP ON
SELECT temp1
LOCATE
IF !EOF()
	SCAN FOR !DELETED()
		xfile = lcPath+ALLT(temp1.filename)
		cOutfile = lcOutPath+ALLT(temp1.filename)
		COPY FILE [&xfile] TO [&cOutfile]
		DELETE FILE [&xfile]
	ENDSCAN
ENDIF

closeftpsetup()
WAIT WINDOW "Marc Jacobs Duplicate File Check is complete" TIMEOUT 2

CLOSE DATA ALL

PROCEDURE closeftpsetup
IF !lTesting
	REPLACE FTPSETUP.chkbusy WITH .f. FOR FTPSETUP.TRANSFER = "MJ-DUPECHK" IN FTPSETUP
ENDIF

