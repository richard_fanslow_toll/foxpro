* This program extractsO/B shipping OS&D's for pre-invoiced MJ customers from previous day
utilsetup("BCNY_MISSING_945")
set exclusive off
set deleted on
set talk off
set safety off   &&Allows for overwrite of existing files

close databases all

SET STEP ON 

goffice="2"

if !used("edi_trigger")
	use f:\3pl\data\edi_trigger.dbf shared in 0
endif

xsqlexec("select accountid, wo_num, ship_ref from outship where mod='"+goffice+"' " + ;
	"and inlist(accountid,6521,6221) and delenterdt>{"+dtoc(date()-20)+"} " + ;
	"and delenterdt<{"+ttoc(datetime()-1290)+"} and bol_no#'NOTHING' " + ;
	"and !inlist(ship_ref,'4380328-A ','4380338-A') and notonwip=0","temp945",,"wh")

select distinct accountid,wo_num,ship_ref,processed,errorflag,fin_status,bol from edi_trigger where edi_type='945 ' and trig_time>=date()-20 and ;
	inlist(accountid,6521,6221) into cursor temp21
use in edi_trigger
select * from temp945 t left join temp21 e on t.accountid=e.accountid and t.wo_num=e.wo_num and t.ship_ref=e.ship_ref  into cursor temp22
select * from temp22 where isnull(accountid_b) into cursor temp23
select  temp23
if reccount() > 0
	export to "S:\BCNY\bcny_missing_945_trigger"  type xls
	tsendto = "doug.wachs@tollgroup.com,alma.navarro@Tollgroup.com,paul.gaidis@tollgroup.com,joe.bianchi@tollgroup.com"
	tattach =  "S:\BCNY\bcny_missing_945_trigger.xls"
	tcc =""
	tfrom ="Toll EDI System Operations <fmi-transload-ops@fmiint.com>"
	tmessage = "BCNY MISSING_945 In Trigger table  "+ttoc(datetime())
	tsubject = "BCNY MISSING_945 In Trigger table, CHECK POLLER"
	do form m:\dev\frm\dartmail2 with tsendto,tfrom,tsubject, tcc,tattach,tmessage,"A"

else
*		export TO "S:\BCNY\NO_pre_invoice_osnd_exist_"   TYPE xls
	tsendto = "pgaidis@fmiint.com"
	tattach = ""
	tcc =""
	tfrom ="Toll EDI System Operations <fmi-transload-ops@fmiint.com>"
	tmessage = "NO BCNY MISSING_945 In Trigger table exist "+ttoc(datetime())
	tsubject = "NO BCNY MISSING_945 In Trigger table exist"
	do form m:\dev\frm\dartmail2 with tsendto,tfrom,tsubject, tcc,tattach,tmessage,"A"
endif



set step on
** IDENTIFIES UNPROCESSED  945 RECORDS IN EDI TRIGGER
select * from temp22 where empty(fin_status)into cursor temp24 readwrite
select temp24
if reccount() > 0
	export to "S:\BCNY\945_missing\945_not_processed_in_edi_trigger"  type xls

	tsendto = "doug.wachs@tollgroup.com,alma.navarro@Tollgroup.com,paul.gaidis@tollgroup.com,joe.bianchi@tollgroup.com"
	tattach = "S:\BCNY\945_missing\945_not_processed_in_edi_trigger.xls"
	tcc =""
	tfrom ="Toll/FMI EDI System Operations <fmi-transload-ops@fmiint.com>"
	tmessage = "BCNY 945 Not processed In Trigger table_"+ttoc(datetime())
	tsubject = "BCNY 945 Not processed In Trigger table, CHECK POLLER"
	do form m:\dev\frm\dartmail2 with tsendto,tfrom,tsubject, tcc,tattach,tmessage,"A"
else
	tsendto = "pgaidis@fmiint.com"
	tattach = ""
	tcc =""
	tfrom ="Toll/FMI EDI System Operations <fmi-transload-ops@fmiint.com>"
	tmessage = "NO BCNY 945 unprocessed In Trigger table_"+ttoc(datetime())
	tsubject = "NO BCNY 945 unprocessed In Trigger table_exist"
	do form m:\dev\frm\dartmail2 with tsendto,tfrom,tsubject, tcc,tattach,tmessage,"A"
endif



set step on
select * from temp22 where errorflag into cursor temp25 readwrite
select temp25
if reccount() > 0
	export to "S:\BCNY\945_missing\945_with_errors"  type xls

	tsendto = "doug.wachs@tollgroup.com,gene.roebuck@tollgroup.com,paul.gaidis@tollgroup.com,joe.bianchi@tollgroup.com"
	tattach = "S:\BCNY\945_missing\945_with_errors.xls"
	tcc =""
	tfrom ="Toll/FMI EDI System Operations <fmi-transload-ops@fmiint.com>"
	tmessage = "BCNY 945 WITH ERRORS_"+ttoc(datetime())
	tsubject = "BCNY 945 WITH ERRORS"
	do form m:\dev\frm\dartmail2 with tsendto,tfrom,tsubject, tcc,tattach,tmessage,"A"
else
	tsendto = "pgaidis@fmiint.com"
	tattach = ""
	tcc =""
	tfrom ="Toll/FMI EDI System Operations <fmi-transload-ops@fmiint.com>"
	tmessage = "NO BCNY 945's with errors exist_"+ttoc(datetime())
	tsubject = "NO BCNY 945's with errors exist"
	do form m:\dev\frm\dartmail2 with tsendto,tfrom,tsubject, tcc,tattach,tmessage,"A"
endif


xsqlexec("select wo_num, ship_ref, del_date from outship where mod='"+goffice+"' " + ;
	"and inlist(accountid,6221,6521) and del_date>{"+dtoc(date()-30)+"} and empty(bol_no)","t1",,"wh")

select t1
if reccount() > 0
	export to "S:\BCNY\945_missing\BCNY_pt_missing_bol"  type xls

	tsendto = "doug.wachs@tollgroup.com,alma.navarro@Tollgroup.com,paul.gaidis@tollgroup.com,joe.bianchi@tollgroup.com"
	tattach = "S:\BCNY\945_missing\BCNY_pt_missing_bol.xls"
	tcc =""
	tfrom ="Toll/FMI EDI System Operations <fmi-transload-ops@fmiint.com>"
	tmessage = "BCNY PT MISSING BOL "+ttoc(datetime())
	tsubject = "BCNY PT MISSING BOL"
	do form m:\dev\frm\dartmail2 with tsendto,tfrom,tsubject, tcc,tattach,tmessage,"A"
else
	tsendto = "pgaidis@fmiint.com"
	tattach = ""
	tcc =""
	tfrom ="Toll/FMI EDI System Operations <fmi-transload-ops@fmiint.com>"
	tmessage = "NO BCNY PT MISSING BOL_"+ttoc(datetime())
	tsubject = "NO BCNY PT MISSING BOL exist"
	do form m:\dev\frm\dartmail2 with tsendto,tfrom,tsubject, tcc,tattach,tmessage,"A"
endif

close data all

wait window at 10,10 "all done...." timeout 2


schedupdate()
_screen.caption=gscreencaption
on error

