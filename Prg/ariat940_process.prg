*!* m:\dev\prg\ariat940_process.prg

lcUpErrPath = "F:\FTPUSERS\Ariat\940IN\hold\"

*lcUpErrPath = "F:\FTPUSERS\Ariat\940IN\"	&& dy 7/7/17

LogCommentStr = ""

delimchar = "*"
lcTranOpt= "TILDE"
*CD &lcPath

ll940test = .F.

IF lTesting
	xpath = "f:\ftpusers\ariat-test\940in\"
	lnNum = ADIR(tarray,xpath+"*.*")
ENDIF

IF ll940test
	xpath = "f:\ftpusers\ariat-test\940in\"
	lnNum = ADIR(tarray,xpath+"*.*")
ELSE
	lnNum = ADIR(tarray,lcpath+"*.*")
ENDIF

IF lnNum = 0
	WAIT WINDOW AT 10,10 "      No "+cCustname+" 940s to import     "  &WAITTIME
	NormalExit = .T.
	THROW
	ELSE
	WAIT WINDOW AT 10,10 TRANSFORM(lnNum)+" 940s to import     "  nowait
ENDIF


set step On 

FOR thisfile = 1  TO lnNum
	IF ll940test
		xfile = xpath+tarray[thisfile,1] && +"."
	ELSE
		xfile = lcpath+tarray[thisfile,1] && +"."
	ENDIF

	cfilename = LOWER(tarray[thisfile,1])
	nFileSize = VAL(TRANSFORM(tarray(thisfile,2)))
	archivefile  = (lcArchivePath+cfilename)
	!ATTRIB -R [&xfile]
	IF !lTesting
		COPY FILE [&xfile] TO ("F:\ftpusers\Ariat\940xfer\"+cfilename) && to create 997
	ENDIF
	WAIT WINDOW "Loading file: "+cfilename NOWAIT
	DO m:\dev\prg\loadedifile WITH xfile,delimchar,lcTranOpt,cCustname

	SELECT x856
	LOCATE
	LOCATE FOR TRIM(x856.segment) = "GS" AND TRIM(x856.f1) = "FA"
	IF FOUND()
		WAIT WINDOW "This is a 997, skip it......."  &WAITTIME &&TIMEOUT 2
		archivefile  = (lcArchivePath+cfilename)
		COPY FILE [&xfile] TO [&archivefile]
		DELETE FILE [&xfile]
		LOOP
	ENDIF

	LOCATE
	DO create_pt_cursors WITH cUseFolder && Added to prep for SQL Server transition
	lOK = .T.
	DO ("m:\dev\prg\"+cUsename+"940_bkdn")
	ASSERT .f. MESSAGE "At run of import"
	IF lOK && If no divide-by-zero error

********** added 2/9/2017 TMARG for ACADEMY and DICK'S if store=0 or empty, replace with DCNUM
		SELECT xpt
		REPLACE xpt.storenum WITH VAL(RIGHT(ALLTRIM(xpt.dcnum),5)) FOR INLIST(UPPER(xpt.consignee),'ACADEMY','DICK')  AND (ALLTRIM(TRANSFORM(xpt.storenum))=''  OR xpt.storenum='0') IN xpt

*!*			IF lTesting AND DATE() = {^2018-02-28}
*!*			DO m:\dev\prg\ariat940_import
*!*			ELSE
			DO ("m:\dev\prg\all940_import")
*!*			ENDIF
	ELSE
		lcUpErrFile = lcUpErrPath+cfilename
		COPY FILE [&xfile] TO [&lcuperrfile]
		DELETE FILE [&xfile]
	ENDIF

ENDFOR
WAIT WINDOW "All "+cCustname+" pickticket files uploaded"  &WAITTIME

&& now clean up the 940in archive files, delete for the upto the last 10 days
*WAIT WINDOW "Deleting older archived 940 files.............." nowait
*(deletefile(lcArchivePath,IIF(DATE()<{^2016-05-01},20,20))

WAIT CLEAR

