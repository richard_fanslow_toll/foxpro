* run this program after Ken archives Platinum

use h:\fox\userinfo in 0
xuserid=userinfo.userid
use in userinfo

if !inlist(xuserid,"FINANCE2","DYOUNG","PGAIDIS")
	x3winmsg(trim(xuserid)+" is not allowed to perform this operation")
	return
endif

if x3winmsg("Are you sure you want to synchronize ARTRAN & ARTRANH between Platinum & Foxpro?","","Y|N")="NO"
	return
endif

? datetime()

close databases all
set deleted on

* artranh

*!*	wait window "copy/zap artranh" nowait

*!*	use f:\watusi\ardata\artranh in 0 exclusive
*!*	select artranh
*!*	copy to h:\fox\artranh
*!*	zap

*!*	wait window "open platinum artranh" nowait

*!*	open database platinum
*!*	use platinum!artranh alias artranhp in 0
*!*	select artranhp
*!*	go bottom

*!*	wait window "reload fox artranh" nowait

*!*	select artranhp
*!*	scan
*!*		scatter memv
*!*		insert into artranh from memvar
*!*	endscan

*!*	? datetime()
*!*	? chr(7)

* artran

close databases all
set deleted on

wait window "copy/zap artran" nowait

*use f:\watusi\ardata\artran in 0 exclusive
*select artran
*copy to h:\fox\artran
*zap

use f:\watusi\ardata\artran in 0 
select artran
copy to h:\fox\artran
delete all

wait window "open platinum artran" nowait

open database f:\watusi\platinum
use platinum!artran_post alias artranp in 0
select artranp
go bottom

wait window "reload fox artran" nowait

select artranp
scan
	scatter memv
	insert into artran from memvar
endscan

email("Dyoung@fmiint.com","Platinum just got an enema - search for xmistranhdrinv in VAL6 and change the date",,,,,.t.,,,,,.t.,,.t.)

? datetime()
? chr(7)