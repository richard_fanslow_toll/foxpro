*!* m:\dev\prg\merkury940_bkdn.prg
&&CLEAR
lCheckStyle = .T.
lPrepack = .F.  && Inventory to be kept as PnP
m.color=""
cUCCNumber = ""
cUsePack = ""
lcUCCSeed = 1
lG62 =  .F.
cPO = ""

goffice=IIF(coffice = "C","2",coffice)
gmasteroffice=IIF(coffice = "I","N","C")

CREATE CURSOR tempsr1 (ship_ref c(20))
SELECT x856
LOCATE FOR x856.segment = "ISA"
cisa_num = ALLTRIM(x856.f13)


SCAN FOR x856.segment = "W05"
	m.ship_ref = ALLTRIM(x856.f2)
	INSERT INTO tempsr1 FROM MEMVAR
ENDSCAN
SELECT ship_ref,COUNT(ship_ref) AS cnt1 FROM tempsr1 GROUP BY ship_ref  INTO CURSOR tempsr2 HAVING cnt1 > 1
USE IN tempsr1
SELECT tempsr2

LOCATE
IF !EOF()
	COPY TO h:\fox\merkdups.XLS TYPE XL5
	tsubjectdup = "940 Received with Duplicated Orders"
	tmessagedup = "Duplicate Order #'s were found in the 940, and these were not loaded. Please send them in a new 940."
	tmessagedup = tmessagedup+CHR(13)+"File "+cFilename+",ISA # "+cisa_num
	tsendtodup  = tsendtoerr+",940@merkuryinnovations.com"
	tccdup = tccerr+",alma.navarro@tollgroup.com,alex.ochoa@tollgroup.com"
	tattachdup = "H:\FOX\merkdups.xls"
	DO FORM m:\dev\frm\dartmail2 WITH tsendtodup,tfrom,tsubjectdup,tccdup,tattachdup,tmessagedup,"A"
	IF FILE("H:\FOX\merkdups.xls")
		DELETE FILE "H:\FOX\merkdups.xls"
	ENDIF
ENDIF

SELECT x856
COUNT FOR TRIM(segment) = "ST" TO nSegCnt
cSegCnt = ALLTRIM(STR(nSegCnt))
SELECT x856
LOCATE

ptctr = 0

WAIT WINDOW "Now in 940 Breakdown" NOWAIT &&TIMEOUT 1

WAIT "There are "+cSegCnt+" P/Ts in this file" WINDOW TIMEOUT 1
SELECT x856
LOCATE

*!* Constants
m.acct_name = "MERKURY INNOVATIONS, LLC"
STORE nAcctNum TO m.accountid
m.careof = " "
m.sf_addr1 = "39 BROADWAY"
m.sf_csz = "NEW YORK, NY 10006"
m.printed = .F.
m.print_by = ""
m.printdate = DATE()
*!* End constants

IF !USED("shipcodes")
	USE F:\3PL\DATA\merkury_shipcodes IN 0 ALIAS shipcodes
ENDIF

*!*  IF !USED("isa_num")
*!*    IF lTesting
*!*      USE F:\3pl\test\isa_num IN 0 ALIAS isa_num
*!*    ELSE
*!*      USE F:\3pl\DATA\isa_num IN 0 ALIAS isa_num
*!*    ENDIF
*!*  ENDIF

*!*  IF !USED("uploaddet")
*!*    IF lTesting
*!*      USE F:\ediwhse\test\uploaddet IN 0 ALIAS uploaddet
*!*    ELSE
*!*      USE F:\ediwhse\DATA\uploaddet IN 0 SHARED ALIAS uploaddet
*!*    ENDIF
*!*  ENDIF

upcmastsql(6561)
INDEX ON STYLE TAG STYLE

SELECT xpt
DELETE ALL
SELECT xptdet
DELETE ALL

STORE 0 TO pt_total_eaches
STORE 0 TO pt_total_cartons
STORE 0 TO nCtnCount

CREATE CURSOR temperrmsg (ship_ref c(20), errmsg c(60))
SELECT x856
SET FILTER TO
LOCATE

STORE "" TO cisa_num

*WAIT WINDOW "NOW IN INITAL HEADER BREAKDOWN" TIMEOUT 1
WAIT WINDOW "NOW IN INITAL HEADER BREAKDOWN" NOWAIT
*ASSERT .F. MESSAGE "At header bkdn...debug"
DO WHILE !EOF("x856")
	cSegment = TRIM(x856.segment)
	cCode = TRIM(x856.f1)

	IF TRIM(x856.segment) = "ISA"
		cisa_num = ALLTRIM(x856.f13)
		CurrentISANum = ALLTRIM(x856.f13)
		WAIT WINDOW "This is a Merkury PT upload" NOWAIT &&TIMEOUT 2
		ptctr = 0
		SELECT x856
		SKIP
		LOOP
	ENDIF

	IF TRIM(x856.segment) = "GS"
		cgs_num = ALLTRIM(x856.f6)
		lcCurrentGroupNum = ALLTRIM(x856.f6)
*!*      SELECT isa_num
*!*      LOCATE FOR isa_num.isa_num = cisa_num ;
*!*        AND isa_num.acct_num = nAcctNum
*!*      IF !FOUND()
*!*        APPEND  BLANK
*!*        REPLACE isa_num.isa_num WITH cisa_num IN isa_num
*!*        REPLACE isa_num.acct_num WITH nAcctNum IN isa_num
*!*        REPLACE isa_num.office WITH cOffice IN isa_num
*!*        REPLACE isa_num.TYPE WITH "940" IN isa_num
*!*        REPLACE isa_num.ack WITH .F. IN isa_num
*!*        REPLACE isa_num.dateloaded WITH DATETIME() IN isa_num
*!*        REPLACE isa_num.filename WITH UPPER(TRIM(xfile)) IN isa_num
*!*      ENDIF
		SELECT x856
		SKIP
		LOOP
	ENDIF

	IF TRIM(x856.segment) = "ST" AND TRIM(x856.f1) = "940"
*!*      DO WHILE ISFLOCKED('ackdata')
*!*        LOOP
*!*      ENDDO
*!*      INSERT INTO ACKDATA (groupnum,isanum,transnum,edicode,accountid,loaddt,loadtime,filename) VALUES (lcCurrentGroupNum,CurrentISANum,ALLTRIM(x856.f2),"OW",6561,DATE(),DATETIME(),xfile)
		SELECT x856
		SKIP
		LOOP
	ENDIF

	IF TRIM(x856.segment) = "W05"  && new shipment
		nCtnCount = 0
		m.style = ""
		SELECT xpt
		nDetCnt = 0
		APPEND BLANK

		REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"FILENAME*"+cFilename IN xpt
		REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"CONAME*MERKURY" IN xpt
		REPLACE xpt.qty WITH 0 IN xpt
		REPLACE xpt.origqty WITH 0 IN xpt
		STORE 0 TO pt_total_eaches
		STORE 0 TO pt_total_cartons
		ptctr = ptctr +1
		WAIT WINDOW AT 10,10 "Now processing PT# "+ ALLTRIM(x856.f2)+" Number "+CHR(13)+TRANSFORM(ptctr)+" of "+cSegCnt NOWAIT

		REPLACE xpt.ptid       WITH ptctr IN xpt
		REPLACE xpt.accountid  WITH nAcctNum IN xpt
*    REPLACE xpt.office     WITH cOffice && "N"
		REPLACE xpt.ptdate     WITH DATE() IN xpt
		m.ship_ref = ALLTRIM(x856.f2)
		cShip_ref = ALLTRIM(x856.f2)
		REPLACE xpt.ship_ref   WITH m.ship_ref IN xpt  && Pick Ticket
		m.pt = ALLTRIM(x856.f2)

		REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"OLDPT*"+ALLTRIM(x856.f2) IN xpt

		REPLACE xpt.cnee_ref   WITH ALLTRIM(x856.f3)  && cust PO
		SELECT x856
		SKIP
		LOOP
	ENDIF

	IF TRIM(x856.segment) = "N1" AND TRIM(x856.f1) = "ST" && Ship-to data

		cConsignee = UPPER(ALLTRIM(x856.f2))
		SELECT xpt
		m.st_name = cConsignee
		REPLACE xpt.consignee WITH  cConsignee IN xpt

		IF "WAL-MART DC"$cConsignee
			REPLACE xpt.dcnum WITH SUBSTR(cConsignee,13,5) IN xpt
		ENDIF

		IF "NORDSTROM"$cConsignee
			cStoreNum = ALLTRIM(x856.f4)
			REPLACE xpt.sforstore  WITH ALLTRIM(x856.f4) IN xpt
			cDCNUM = SUBSTR(ALLTRIM(cConsignee),1,4)
			REPLACE xpt.dcnum WITH cDCNUM IN xpt
			IF EMPTY(xpt.shipins)
				REPLACE xpt.shipins WITH "STORENUM*"+cStoreNum IN xpt
			ELSE
				REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"STORENUM*"+cStoreNum IN xpt
			ENDIF

			IF LEN(cStoreNum)>5
				REPLACE xpt.storenum  WITH VAL(RIGHT(cStoreNum,5)) IN xpt
			ELSE
				REPLACE xpt.storenum  WITH VAL(cStoreNum) IN xpt
			ENDIF
		ELSE
			cStoreNum = ALLTRIM(x856.f4)
			IF EMPTY(xpt.shipins)
				REPLACE xpt.shipins WITH "STORENUM*"+cStoreNum IN xpt
			ELSE
				REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"STORENUM*"+cStoreNum IN xpt
			ENDIF
			IF EMPTY(xpt.dcnum)
				REPLACE xpt.dcnum WITH cStoreNum IN xpt
			ENDIF
			IF LEN(cStoreNum)>5
				REPLACE xpt.storenum  WITH VAL(RIGHT(cStoreNum,5)) IN xpt
			ELSE
				REPLACE xpt.storenum  WITH VAL(cStoreNum) IN xpt
			ENDIF
		ENDIF

		SKIP 1 IN x856
		IF TRIM(x856.segment) = "N3"  && address info
			m.st_addr1 = UPPER(TRIM(x856.f1))
			m.st_addr2 = UPPER(TRIM(x856.f2))
			REPLACE xpt.address  WITH m.st_addr1
			REPLACE xpt.address2 WITH m.st_addr2
		ENDIF
		SKIP 1 IN x856
		IF TRIM(x856.segment) = "N4"  && address info
			m.zipbar = TRIM(x856.f3)
			m.st_csz = UPPER(STRTRAN(TRIM(x856.f1),",",""))+", "+UPPER(TRIM(x856.f2))+" "+TRIM(x856.f3)
			m.salesrep=TRIM(x856.f3)
			REPLACE xpt.csz WITH m.st_csz IN xpt
			cCountry = IIF(EMPTY(ALLTRIM(x856.f4)),"USA",ALLTRIM(x856.f4))
			REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"COUNTRY*"+cCountry IN xpt
			REPLACE xpt.country WITH cCountry IN xpt
		ENDIF
		SELECT x856
		SKIP
		LOOP
	ENDIF

	IF TRIM(x856.segment) = "N1" AND INLIST(TRIM(x856.f1),"SD","PF") && Returned data
		cNom = ALLTRIM(x856.f1)
		IF lTesting AND cNom = "SD"

		ENDIF
		REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+cNom+"_N1*"+ALLTRIM(x856.f2) IN xpt
		SKIP 1 IN x856

		DO CASE
		CASE TRIM(x856.segment) = "N2"
			REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+cNom+"_N2*"+ALLTRIM(x856.f1) IN xpt
			SKIP 1 IN x856
			IF TRIM(x856.segment) = "N3"
				REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+cNom+"_N3*"+ALLTRIM(x856.f1)+"|"+ALLTRIM(x856.f2) IN xpt
				SKIP 1 IN x856
			ENDIF
			IF TRIM(x856.segment) = "N4"
				REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+cNom+"_N4*"+ALLTRIM(x856.f1)+"|"+ALLTRIM(x856.f2)+"|"+ALLTRIM(x856.f3) IN xpt
			ENDIF

		CASE TRIM(x856.segment) = "N3"
			REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+cNom+"_N3*"+ALLTRIM(x856.f1)+"|"+ALLTRIM(x856.f2) IN xpt
			SKIP 1 IN x856
			IF TRIM(x856.segment) = "N4"
				REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+cNom+"_N4*"+ALLTRIM(x856.f1)+"|"+ALLTRIM(x856.f2)+"|"+ALLTRIM(x856.f3) IN xpt
			ENDIF

		CASE TRIM(x856.segment) = "N4"
			REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+cNom+"_N4*"+ALLTRIM(x856.f1)+"|"+ALLTRIM(x856.f2)+"|"+ALLTRIM(x856.f3) IN xpt
		ENDCASE

		SELECT x856
		SKIP
		LOOP
	ENDIF

	IF TRIM(x856.segment) = "N1" AND INLIST(TRIM(x856.f1),"MA","Z7")
		STORE UPPER(TRIM(x856.f2)) TO cMarkfor && will be overwritten if there is an N2 loop
		REPLACE xpt.shipfor WITH cMarkfor IN xpt
		cSFStoreNum = UPPER(ALLTRIM(x856.f4))
		REPLACE xpt.sforstore WITH cSFStoreNum IN xpt
		REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"SFSTORENUM*"+cSFStoreNum IN xpt  && DC or Store Number
		SKIP 1 IN x856
		DO CASE
		CASE TRIM(x856.segment) = "N2"  && address info
			STORE UPPER(TRIM(x856.f2)) TO cMarkfor
			REPLACE xpt.shipfor WITH cMarkfor IN xpt
			REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"SFSTORELONG*"+UPPER(ALLTRIM(x856.f1)) IN xpt
			REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"STORENAME*"+ALLTRIM(x856.f2) IN xpt
			REPLACE xpt.sforstore WITH ALLTRIM(x856.f1) IN xpt
			SKIP 1 IN x856
			IF TRIM(x856.segment) = "N3"  && address info
				REPLACE xpt.sforaddr1 WITH UPPER(TRIM(x856.f1)) IN xpt
				REPLACE xpt.sforaddr2  WITH UPPER(TRIM(x856.f2)) IN xpt
			ELSE
				cErrMsg = "Missing Ship-For Address Info (N2>N3), PT "+cShip_ref
				WAIT WINDOW cMessage TIMEOUT 2
				SELECT xpt
				DELETE NEXT 1 IN xpt
				INSERT INTO temperrmsg (ship_ref,errmsg) VALUES (cShip_ref,cErrMsg)
				SELECT x856
				DO WHILE(x856.segment) # "SE"
					SKIP
				ENDDO
				LOOP
			ENDIF
		CASE TRIM(x856.segment) = "N3"  && address info
			REPLACE xpt.sforaddr1 WITH UPPER(TRIM(x856.f1)) IN xpt
			REPLACE xpt.sforaddr2  WITH UPPER(TRIM(x856.f2)) IN xpt
		OTHERWISE
			cErrMsg = "Missing Ship-For Address Info (N1>N2), PT "+cShip_ref
			WAIT WINDOW cErrMsg TIMEOUT 2
			SELECT xpt
			DELETE NEXT 1 IN xpt
			INSERT INTO temperrmsg (ship_ref,errmsg) VALUES (cShip_ref,cErrMsg)
			SELECT x856
			DO WHILE(x856.segment) # "SE"
				SKIP
			ENDDO
			LOOP
		ENDCASE
		SKIP 1 IN x856
		IF TRIM(x856.segment) = "N4"  && address info
*!*        IF EMPTY(TRIM(x856.f2))
*!*        csforCSZ = TRIM(x856.f1)+", "+TRIM(x856.f4)+" "+TRIM(x856.f3)
*!*        else
			csforCSZ = UPPER(STRTRAN(TRIM(x856.f1),",",""))+", "+UPPER(PADR(TRIM(x856.f2),2))+" "+TRIM(x856.f3)
*!*        endif
			REPLACE xpt.sforcsz WITH UPPER(csforCSZ) IN xpt
			IF TRIM(x856.f4) = "GB"
				REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"SFORCITY*"+ALLTRIM(x856.f1)
				REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"SFORSTATE*"+ALLTRIM(x856.f2)
				REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"SFORZIP*"+ALLTRIM(x856.f3)
				REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"SFORCTRY*"+ALLTRIM(x856.f4)
			ENDIF
		ELSE
			cMessage = "Missing Ship-For CSZ Info "+cShip_ref
			WAIT WINDOW cMessage TIMEOUT 2
			SELECT xpt
			DELETE NEXT 1 IN xpt
			INSERT INTO temperrmsg (ship_ref,errmsg) VALUES (cShip_ref,cErrMsg)
			SELECT x856
			DO WHILE(x856.segment) # "SE"
				SKIP
			ENDDO
			LOOP
		ENDIF
	ENDIF

	IF TRIM(x856.segment) = "PER" AND x856.f1 = "IC" && Personal contact
		IF x856.f3 = "TE"
			REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"PHONE*"+UPPER(ALLTRIM(x856.f4)) IN xpt
			SELECT x856
			SKIP
			LOOP
		ENDIF
	ENDIF

	IF TRIM(x856.segment) = "N9" AND TRIM(x856.f1) = "DV"  && customer div
		cDIV = ALLTRIM(x856.f2)
		REPLACE xpt.div WITH cDIV IN xpt
		REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"DIVISION*"+cDIV IN xpt
		SELECT x856
		SKIP
		LOOP
	ENDIF

	IF TRIM(x856.segment) = "N9" AND TRIM(x856.f1) = "MR"  && Walmart Order Type
		cOrderType = ALLTRIM(x856.f2)
		REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"ORDERTYPE*"+cOrderType IN xpt
		SELECT x856
		SKIP
		LOOP
	ENDIF

	IF TRIM(x856.segment) = "N9" AND TRIM(x856.f1) = "DP"  && customer div
		cDept = ALLTRIM(x856.f2)
		REPLACE xpt.dept WITH cDept IN xpt
		SELECT x856
		SKIP
		LOOP
	ENDIF

	IF TRIM(x856.segment) = "N9" AND TRIM(x856.f1) = "CM" && unknown
		cCM = ALLTRIM(x856.f2)
		REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"CM*"+cCM IN xpt
		SELECT x856
		SKIP
		LOOP
	ENDIF

	IF TRIM(x856.segment) = "N9" AND TRIM(x856.f1) = "IA" && unknown
		cIA = ALLTRIM(x856.f2)
		REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"IA*"+cIA IN xpt
		SELECT x856
		SKIP
		LOOP
	ENDIF

	IF TRIM(x856.segment) = "N9" AND TRIM(x856.f1) = "FM"  && unknown
		cFM = ALLTRIM(x856.f2)
		REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"FM*"+cFM IN xpt
		SELECT x856
		SKIP
		LOOP
	ENDIF

	IF TRIM(x856.segment) = "N9" AND TRIM(x856.f1) = "GR"  && unknown
		cGR = ALLTRIM(x856.f2)
		REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"GR*"+cGR IN xpt
		SELECT x856
		SKIP
		LOOP
	ENDIF

	IF TRIM(x856.segment) = "N9" AND TRIM(x856.f1) = "RF"  && unknown
		cRF = ALLTRIM(x856.f2)
		REPLACE xpt.vendor_num WITH cRF IN xpt
		REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"RF*"+cRF IN xpt
		SELECT x856
		SKIP
		LOOP
	ENDIF
****added 11/25/15 to capture N9*ZZ for label  TM
	IF TRIM(x856.segment) = "N9" AND TRIM(x856.f1) = "ZZ"  && unknown
		cZZ = ALLTRIM(x856.f2)
		REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"ZZ*"+cZZ IN xpt
		SELECT x856
		SKIP
		LOOP
	ENDIF

	IF TRIM(x856.segment) = "N9" AND TRIM(x856.f1) = "R2"  && comment for Groupon label
		cR2 = ALLTRIM(x856.f2)
		REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"COMMENT*"+cR2 IN xpt
		SELECT x856
		SKIP
		LOOP
	ENDIF

	IF TRIM(x856.segment) = "N9" AND TRIM(x856.f1) = "CU"  && Clear text clause (?)
		cCustDept = ALLTRIM(x856.f2)
		cCoPrefix = ICASE(cCustDept="5068","T",cCustDept="5064","S",cCustDept="5031","H",cCustDept="5024","D",cCustDept="5020","E","")
		cCoName = ICASE(cCOPrefix="H","HOME DEPOT",cCOPrefix="D","GROUPON",cCOPrefix="T","TOYS R US",cCOPrefix="S","SHOP CHNL",cCOPrefix="E","EBAGS","")
		replace xpt.batch_num WITH cCOName IN xpt  && Joe, per Mike/Alma
		REPLACE xpt.cacctnum WITH ALLTRIM(x856.f2) IN xpt  && PG taking a guess here
		REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"CUSTDEPT*"+cCustDept IN xpt
		SELECT x856
		SKIP
		LOOP
	ENDIF

	IF TRIM(x856.segment) = "N9" AND TRIM(x856.f1) = "CO"
		cCO = ALLTRIM(x856.f2)
		REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"CO*"+cCO IN xpt
*		m.ship_ref = cCoPrefix+ALLTRIM(x856.f2)
*		cShip_ref = cCoPrefix+ALLTRIM(x856.f2)
		m.ship_ref = ALLTRIM(x856.f2)
		cShip_ref = ALLTRIM(x856.f2)
		REPLACE xpt.ship_ref   WITH m.ship_ref IN xpt  && Pick Ticket
		m.pt = ALLTRIM(x856.f2)
		SELECT x856
		SKIP
		LOOP
	ENDIF

	IF TRIM(x856.segment) = "N9" AND TRIM(x856.f1) = "VR"  && vendor id number
		cVendID = ALLTRIM(x856.f2)
		REPLACE xpt.vendor_num WITH  cVendID IN xpt
		REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"VENDID*"+cVendID IN xpt
		SELECT x856
		SKIP
		LOOP
	ENDIF

	IF TRIM(x856.segment) = "N9" AND TRIM(x856.f1) = "EM"  && customer email, added 06.26.2015
		cEmail = ALLTRIM(x856.f2)
		REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"EMAIL*"+cDIV IN xpt
		SELECT x856
		SKIP
		LOOP
	ENDIF

	lG62 =  .F.
	IF TRIM(x856.segment) = "G62"
		IF TRIM(x856.f1) == IIF(lTesting,"04","10")  && start date
			lcDate = ALLTRIM(x856.f2)
			lxDate = SUBSTR(lcDate,5,2)+"/"+SUBSTR(lcDate,7,2)+"/"+LEFT(lcDate,4)
			ldDate = CTOD(lxDate)
			STORE ldDate TO dStart
			REPLACE xpt.START WITH ldDate IN xpt
			SELECT x856
			SKIP
			LOOP
		ENDIF

	ENDIF

	IF lG62
		IF dCancel<dStart  && Added to check for Start/Cancel errors...10/05/07

			SELECT xpt
*BROW
			errordates()
			USE IN x856
			lOK = .F.
			THROW
		ENDIF
	ENDIF


	IF TRIM(x856.segment) = "W66"
		IF INLIST(ALLTRIM(x856.f5),"PS","PL","FSP","UPMI","UPSSP")
			DO CASE

			CASE ALLTRIM(x856.f5) = "FSP" && Added this block per Darren, 04.15.2015
				REPLACE xpt.scac WITH "FSP" IN xpt
				REPLACE xpt.ship_via WITH "FEDEX SUREPOST" IN xpt
				IF INLIST(x856.f2,"L","S")
					REPLACE xpt.ship_via WITH ALLTRIM(xpt.ship_via)+ALLTRIM(x856.f2) IN xpt
				ENDIF
				REPLACE xpt.terms WITH ALLTRIM(x856.f1) IN xpt
				REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"SHIPCODE*"+ALLTRIM(x856.f5) IN xpt

			CASE ALLTRIM(x856.f5) = "UPMI" && Added this block per Darren, 04.15.2015
				REPLACE xpt.scac WITH "MIMK" IN xpt
				REPLACE xpt.ship_via WITH "UPS MAIL INNOVATIONS" IN xpt
				REPLACE xpt.terms WITH ALLTRIM(x856.f1) IN xpt
				REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"SHIPCODE*"+ALLTRIM(x856.f5) IN xpt

			CASE ALLTRIM(x856.f5) = "UPSSP" && Added this block per Darren, 04.15.2015
				REPLACE xpt.scac WITH "UPSN" IN xpt
				REPLACE xpt.ship_via WITH "UPS SUREPOST" IN xpt
				REPLACE xpt.terms WITH ALLTRIM(x856.f1) IN xpt
				REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"SHIPCODE*"+ALLTRIM(x856.f5) IN xpt

			CASE INLIST(ALLTRIM(x856.f5),"PS","PL") && Added this block PG, 12/16/2015 for smartpost services
				REPLACE xpt.scac WITH "FSP" IN xpt
				IF ALLTRIM(x856.f5)="PS"
					REPLACE xpt.ship_via WITH "FEDEX SUREPOST PARCEL" IN xpt
				ENDIF
				IF ALLTRIM(x856.f5)="PL"
					REPLACE xpt.ship_via WITH "FEDEX SUREPOST <1LB" IN xpt
				ENDIF
				REPLACE xpt.terms WITH ALLTRIM(x856.f1) IN xpt
				REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"SHIPCODE*"+ALLTRIM(x856.f5) IN xpt
			ENDCASE
		ELSE
			SELECT shipcodes
			LOCATE FOR shipcode = ALLTRIM(x856.f5)


			IF FOUND()
				IF ALLTRIM(x856.f1) ="TP"
					REPLACE xpt.terms WITH ALLTRIM(x856.f1) IN xpt
					REPLACE xpt.ship_via WITH ALLTRIM(shipcodes.ship_via)+"-3RD PARTY" IN xpt
				ELSE
					REPLACE xpt.terms WITH ALLTRIM(x856.f1) IN xpt
					REPLACE xpt.ship_via WITH ALLTRIM(shipcodes.ship_via) IN xpt
				ENDIF

				IF ALLTRIM(x856.f1) ="TP"
					REPLACE xpt.terms WITH ALLTRIM(x856.f1) IN xpt
				ENDIF

				REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"PAYMENT*"+ALLTRIM(x856.f1) IN xpt
				REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"SHIPMODE*"+ALLTRIM(x856.f2) IN xpt
				REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"SHIPCODE*"+ALLTRIM(x856.f5) IN xpt
				REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"SHIPPAYCODE*"+ALLTRIM(x856.f5) IN xpt
				IF ALLTRIM(x856.f6) ="0G"
					REPLACE xpt.upsacctnum WITH ALLTRIM(x856.f7) IN xpt
				ENDIF

				SELECT x856
				SKIP
				LOOP

*!*    REMOVED THE FOLLOWING TO ALLOW 940s TO BE UPLOADED EVEN WITH INCORRECT SCAC INFO. Joe, 03.20.2017
*!*        ELSE
*!*          cErrMsg = "SCAC "+ALLTRIM(x856.f5)+" NOT FOUND IN MERKURY_SHIPCODES TABLE...Check/Add"
*!*          SELECT xpt
*!*          DELETE NEXT 1 IN xpt
*!*          INSERT INTO temperrmsg (ship_ref,errmsg) VALUES (cship_ref,cErrMsg)
*!*          SELECT x856
*!*          DO WHILE(x856.segment) # "SE"
*!*            SKIP
*!*          ENDDO
*!*          LOOP
			ENDIF
		ENDIF
	ENDIF

	IF TRIM(x856.segment) = "LX" && start of PT detail, stay here until this PT is complete
*ASSERT .F. MESSAGE cship_ref+" Detail, Type = "+IIF(lPrepack,"Prepack","Pickpack")+"...debug"
		m.units = IIF(lPrepack,.F.,.T.)
		WAIT "NOW IN DETAIL LOOP PROCESSING" WINDOW NOWAIT

		SELECT xptdet
		CALCULATE MAX(ptdetid) TO m.ptdetid
		m.ptdetid = m.ptdetid + 1

		SELECT xptdet
		SKIP 1 IN x856
		lnLinenum=1
		DO WHILE ALLTRIM(x856.segment) != "SE"
			cSegment = TRIM(x856.segment)
			cCode = TRIM(x856.f1)

			IF TRIM(x856.segment) = "W01" && Destination Quantity
				SELECT xptdet
				APPEND BLANK
				REPLACE xptdet.ptid WITH xpt.ptid IN xptdet
				REPLACE xptdet.ptdetid WITH m.ptdetid IN xptdet
				REPLACE xptdet.units WITH m.units IN xptdet
				REPLACE xptdet.linenum WITH ALLTRIM(STR(lnLinenum)) IN xptdet
				lnLinenum = lnLinenum + 1
				m.ptdetid = m.ptdetid + 1
				REPLACE xptdet.accountid WITH xpt.accountid IN xptdet
				IF lPrepack
					cUsePack = "1"
*!*            cUsePack = ALLT(STR(INT(VAL(x856.f12))))
*!*            IF EMPTY(cUsePack)
*!*              WAIT WINDOW "Empty cUsePack at PT"+cship_ref TIMEOUT 3
*!*              NormalExit = .F.
*!*              errormail1()
*!*              USE IN x856
*!*              lOK = .F.
*!*              RETURN
*!*            ENDIF
					IF cUsePack == "1"
						cUsePack = ALLT(x856.f1)
					ENDIF
				ELSE
					cUsePack = "1"
				ENDIF
				cUsePack= "1"
				REPLACE xptdet.PACK WITH cUsePack IN xptdet
				m.casepack = INT(VAL(cUsePack))
				REPLACE xptdet.casepack WITH m.casepack IN xptdet
				m.divvalue = INT(VAL(ALLTRIM(x856.f1)))

				IF !lPrepack  && Pickpack
					REPLACE xptdet.totqty WITH INT(VAL(x856.f1)) IN xptdet
				ELSE
					REPLACE xptdet.totqty WITH (m.divvalue/m.casepack) IN xptdet
				ENDIF
				REPLACE xpt.qty WITH xpt.qty + xptdet.totqty IN xpt
				REPLACE xpt.origqty WITH xpt.qty IN xpt
				REPLACE xptdet.origqty WITH xptdet.totqty IN xptdet

				IF EMPTY(TRIM(xptdet.printstuff))
					REPLACE xptdet.printstuff WITH "UOM*"+ALLTRIM(x856.f2) IN xptdet
				ELSE
					REPLACE xptdet.printstuff WITH xptdet.printstuff+CHR(13)+"UOM*"+ALLTRIM(x856.f2) IN xptdet
				ENDIF

				cUPC = ""
				IF !EMPTY(ALLTRIM(x856.f3))
					cUPC = ALLTRIM(x856.f3)
					REPLACE xptdet.upc WITH cUPC IN xptdet
				ENDIF

				IF INLIST(ALLTRIM(x856.f4),"VN","VA")
					REPLACE xptdet.STYLE WITH ALLTRIM(x856.f5) IN xptdet
					IF SEEK(xptdet.STYLE,"upcmast","style") AND EMPTY(cUPC)
						cUPC = upcmast.upc
						REPLACE xptdet.upc WITH cUPC IN xptdet
					ENDIF
				ENDIF

				IF INLIST(ALLTRIM(x856.f6),"PI","SK","IN","CB")
					REPLACE xptdet.custsku WITH ALLTRIM(x856.f7) IN xptdet
					IF LEN(ALLTRIM(x856.f7)) > 15
						REPLACE xptdet.custsku WITH RIGHT(ALLTRIM(x856.f7),15) IN xptdet
					ENDIF
					DO CASE
					CASE ALLTRIM(x856.f6) = "CB"
						cMemoAdd = "ARTICLENUMBER*"   && Joann has a 18 digit value
					CASE ALLTRIM(x856.f6) = "SK"
						cMemoAdd = "STOCKKEEPING*"
					CASE ALLTRIM(x856.f6) = "IN"
						cMemoAdd = "ITEM NUMBER*"
					CASE ALLTRIM(x856.f6) = "PI"
						cMemoAdd = "PURCHCODE*"
					ENDCASE
					REPLACE xptdet.printstuff WITH xptdet.printstuff+CHR(13)+cMemoAdd+ALLTRIM(x856.f7) IN xptdet
				ENDIF

				cUPCCode = ALLTRIM(x856.f16)
				REPLACE xptdet.printstuff WITH xptdet.printstuff+CHR(13)+"UPCCODE*"+cUPCCode IN xptdet

				IF EMPTY(xptdet.upc)
					IF INLIST(cUPCCode,"UI","UK") AND EMPTY(cUPC)
						REPLACE xptdet.upc WITH ALLTRIM(x856.f16) IN xptdet
					ENDIF
				ENDIF

				IF !("STORENUM*"$xptdet.printstuff)
					REPLACE xptdet.printstuff WITH xptdet.printstuff+CHR(13)+"STORENUM*"+cStoreNum IN xptdet
				ENDIF
			ENDIF

			IF TRIM(x856.segment) = "G69" && desc
				cPrtstr = "DESC*"+UPPER(TRIM(x856.f1))
				REPLACE xptdet.printstuff WITH xptdet.printstuff+CHR(13)+cPrtstr IN xptdet
			ENDIF

			SKIP 1 IN x856
		ENDDO

		SELECT xptdet
	ENDIF

	SELECT x856
	IF !EOF()
		SKIP 1 IN x856
	ENDIF
ENDDO

SELECT xptdet

DO rollup_ptdet_units


WAIT WINDOW "At end of ROLLUP procedure" NOWAIT

DO m:\dev\prg\setuppercase940

SELECT tempsr2
LOCATE
SCAN
	SELECT xpt
	SCAN FOR xpt.ship_ref = tempsr2.ship_ref AND !DELETED()
		nPTID = xpt.ptid
		DELETE FOR xpt.ptid = nPTID
		SELECT xptdet
		DELETE FOR xpt.ptid = nPTID
	ENDSCAN
	SELECT tempsr2
ENDSCAN

IF lBrowfiles
	WAIT WINDOW "If tables are incorrect, CANCEL when XPTDET window opens"+CHR(13)+"Otherwise, will load PT table" TIMEOUT 2
	SELECT xpt
	COPY TO h:\xpt TYPE XL5
	LOCATE
	BROWSE
	SELECT xptdet
	COPY TO h:\xptdet TYPE XL5
	LOCATE
	BROWSE
*  cancel
ENDIF
SELECT temperrmsg
LOCATE
IF !EOF('temperrmsg')
	SELECT 0
	USE F:\3pl\DATA\mailmaster ALIAS mm SHARED
	LOCATE FOR mm.edi_type = "MISC" AND mm.taskname = IIF(lTesting,"JOETEST","MERKPTERR")
	STORE TRIM(IIF(mm.use_alt,mm.sendtoalt,mm.sendto)) TO tsendtopt
	STORE TRIM(IIF(mm.use_alt,mm.ccalt,mm.cc)) TO tccpt
	USE IN mm
	cMailLoc = IIF(coffice = "C","West",IIF(coffice = "N","New Jersey","Florida"))
	tsubjectpt = "TGF "+cMailLoc+" "+cCustname+" Pickticket Error: " +TTOC(DATETIME())
	tmessagept = "The following PTs did not load due to the indicated errors:"+CHR(13)+CHR(13)+PADR("PICKTICKET",22)+"MESSAGE"+CHR(13)
	SELECT temperrmsg
	SCAN
		tmessagept = tmessagept+CHR(13)+PADR(ALLTRIM(temperrmsg.ship_ref),22)+ALLTRIM(temperrmsg.errmsg)
	ENDSCAN
	tmessagept = tmessagept+CHR(13)+CHR(13)+"Please resend the affected orders."
	tattach = ""
	DO FORM m:\dev\frm\dartmail2 WITH tsendtopt,tfrom,tsubjectpt,tccpt,tattach,tmessagept,"A"
ENDIF

WAIT WINDOW cCustname+" Breakdown Round complete..." NOWAIT &&TIMEOUT 2
SELECT x856
IF USED("PT")
	USE IN pt
ENDIF
IF USED("PTDET")
	USE IN ptdet
ENDIF
WAIT CLEAR
RETURN

*************************************************************************************************
*!* Error Mail procedure - W01*12 Segment
*************************************************************************************************
PROCEDURE errormail1

IF lEmail
	SELECT 0
	USE F:\3pl\DATA\mailmaster ALIAS mm SHARED
	LOCATE FOR mm.edi_type = "MISC" AND mm.taskname = "GENERAL"
	STORE TRIM(IIF(mm.use_alt,mm.sendtoalt,mm.sendto)) TO tsendto
	STORE TRIM(IIF(mm.use_alt,mm.ccalt,mm.cc)) TO tcc
	USE IN mm
	cMailLoc = IIF(coffice = "C","West",IIF(coffice = "N","New Jersey","Florida"))
	tsubject= "TGF "+cMailLoc+" "+cCustname+" Pickticket Error: " +TTOC(DATETIME())
	tmessage = "File: "+cFilename+CHR(13)+" is missing W01*12 segment(s), causing a divide by zero error."
	tmessage = tmessage + "Please re-create and resend this file."
	DO FORM m:\dev\frm\dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
ENDIF
ENDPROC

*************************************************************************************************
*!* Error Mail procedure - Cancel Before Start Date
*************************************************************************************************
PROCEDURE errordates

IF lEmail
	tsendto = tsendtotest
	tcc =  tcctest
	cMailLoc = IIF(coffice = "C","West",IIF(coffice = "N","New Jersey","Florida"))
	tsubject= "TGF "+cMailLoc+" "+cCustname+" Pickticket Error: " +TTOC(DATETIME())
	tmessage = "File: "+cFilename+CHR(13)+" has Cancel date(s) which precede Start date(s)"
	tmessage = tmessage + "File needs to be checked and possibly resubmitted."
	DO FORM m:\dev\frm\dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
ENDIF
ENDPROC
**********************************************************************************************************8
