* SHCPMREPORT 04/23/2015
* Create a utiliy report that gives a cost per mile info for Trucks, from the shop system.
* For Fran C. and Bill O'Brien; Ryan Brenan as of 3/3/2016

* Break out inside, outside and accident costs.

* Use same burdened labor rates as in the financial shop reports.

LOCAL lTestMode
lTestMode = .T.

utilsetup("SHCPMREPORT")

DECLARE INTEGER CreateMutex IN Win32API ;
	STRING @lpMutexAttributes, ;
	STRING bInitialOwner, ;
	STRING @lpName

DECLARE INTEGER GetLastError IN Win32API

* if running in EXE check to see if already running
IF _VFP.STARTMODE > 0 THEN
	lcProcessName = "SHCPMREPORT"
	CreateMutex(0,CHR(1),@lcProcessName)
	lnError = GetLastError()
	IF lnError = 183
		WAIT WINDOW TIMEOUT 5 "This program is already running..."
		IF NOT lTestMode THEN
			schedupdate()
		ENDIF
		RETURN .F.
	ENDIF
ENDIF

loSHCPMREPORT = CREATEOBJECT('SHCPMREPORT')
loSHCPMREPORT.MAIN( lTestMode )

IF NOT lTestMode THEN
	schedupdate()
ENDIF

CLOSE DATABASES ALL
RETURN

#DEFINE LOGIT 1
#DEFINE WAITIT 2
#DEFINE NOWAITIT 4
#DEFINE SENDIT 8
#DEFINE CRLF CHR(13) + CHR(10)
#DEFINE RETURN_DATA_MANDATORY .F.
#DEFINE RETURN_DATA_NOT_MANDATORY .T.
#DEFINE CSVFORMAT 6
#DEFINE WRITE_ONLY_UNBUFFERED 11
#DEFINE COMMA ","

DEFINE CLASS SHCPMREPORT AS CUSTOM

	cProcessName = 'SHCPMREPORT'

	lTestMode = .T.  && .F. FOR NORMAL OPERATION, .T. TO SUPPRESS ALL BUT DEVELOPER EMAILS AND CHANGE INPUT TABLE, ETC.

	dToday = DATE()
	*dToday = {^2015-04-19}
	
	* processing properties
	*************************************************************************************************************
	lCalcDateRange = .T.   && FALSE = process all work orders; TRUE = just for the date range entered in the code.
	lCorrectMileages = .F.
	*cVehicleType = 'TRAILER'
	*cVehicleType = 'TRUCK'
	cVehicleType = 'MISC'
	*************************************************************************************************************


	lAutoYield = .T.

	cStartTime = TTOC(DATETIME())

	* wait window properties
	nWaitWindowTimeout = 2
	lWaitWindowIsOn = .T.

	* logfile properties
	lLoggingIsOn = .T.
	cLogFile = 'F:\UTIL\SHOPREPORTS\LOGFILES\SHCPMREPORT_log.txt'

	* INTERNAL email properties
	lSendInternalEmailIsOn = .T.
	cFrom = 'mark.bennett@tollgroup.com'
	cSendTo = 'mbennett@fmiint.com'
	cCC = ''
	cSubject = "Vehicle Cost-per-Mile Report for " + TRANSFORM(DATETIME())
	cAttach = ''
	cBodyText = ''
	cMGRSendTo = ''
	cMGRFrom = ''
	cMGRSubject = ''
	cMGRCC = ''
	cMGRAttach = ''
	cMGRBodyText = ''

	cCOMPUTERNAME = ''
	cUSERNAME = ''


	FUNCTION INIT
		IF NOT DODEFAULT()
			RETURN .F.
		ENDIF
		WITH THIS
			*SET RESOURCE OFF
			CLOSE DATA
			SET CENTURY ON
			*SET DATE AMERICAN
			*SET HOURS TO 24
			SET DECIMALS TO 3
			SET ANSI ON
			SET TALK OFF
			SET DELETED ON
			SET CONSOLE OFF
			SET EXCLUSIVE OFF
			SET SAFETY OFF
			SET EXACT OFF
			SET STATUS BAR ON
			SET SYSMENU OFF
			SET ENGINEBEHAVIOR 70
			SET MULTILOCKS ON
			_VFP.AUTOYIELD = .lAutoYield
			.cCOMPUTERNAME = GETENV("COMPUTERNAME")
			.cUSERNAME = GETENV("USERNAME")
			.lLoggingIsOn = .lLoggingIsOn AND NOT EMPTY(.cLogFile)
		ENDWITH
	ENDFUNC


	FUNCTION DESTROY
		WITH THIS
			IF .lLoggingIsOn  THEN
				SET ALTERNATE OFF
				SET ALTERNATE TO
			ENDIF
		ENDWITH
		DODEFAULT()
	ENDFUNC


	FUNCTION MAIN
		LPARAMETERS tlTestMode
		WITH THIS
			LOCAL lnRow, lcRow, lcReportPeriod, lcFiletoSaveAs, lcSpreadsheetTemplate, lnNumberOfErrors
			LOCAL oExcel, oWorkbook, lcErr, lcTitle, loError, lnRate, ldToday
			LOCAL lcDivision, lnLastRow, lnRate, lcDetailBasisFile, lnPercent, lnDifference
			LOCAL ldStartCRDate, lcDateRangeWhere, lnFirstMileage				
			LOCAL lnMaxMileage, lnLastMileage, lnLCost, lnPCost, lnOutscost, lnALCost, lnAPCost, lnAOutscost, lnTireCost, lnATireCost

			TRY
				lnNumberOfErrors = 0

				.lTestMode = tlTestMode

				IF .lTestMode THEN
					.cLogFile = 'F:\UTIL\SHOPREPORTS\LOGFILES\SHCPMREPORT_log_TESTMODE.txt'
					.cSendTo = 'mbennett@fmiint.com'
				ENDIF

				IF .lLoggingIsOn THEN
					SET ALTERNATE TO (.cLogFile) ADDITIVE
					SET ALTERNATE ON
				ENDIF

				.cStartTime = TTOC(DATETIME())

				.TrackProgress('Vehicle Cost-per-mile process started....', LOGIT+SENDIT)
				.TrackProgress('COMPUTERNAME = ' + .cCOMPUTERNAME, LOGIT+SENDIT)
				.TrackProgress('USERNAME = ' + .cUSERNAME, LOGIT+SENDIT)
				.TrackProgress('PROJECT = SHCPMREPORT', LOGIT+SENDIT)
				IF .lTestMode THEN
					.TrackProgress('                        TEST MODE                        ', LOGIT+SENDIT)
				ENDIF
				.TrackProgress('Vehicle Type = ' + .cVehicleType, LOGIT+SENDIT)

				ldToday = .dToday

				USE F:\SHOP\SHDATA\ORDHDR IN 0

				DO CASE
					CASE .cVehicleType == 'TRAILER'
				
						USE F:\SHOP\SHDATA\trailer IN 0
						
					CASE .cVehicleType == 'TRUCK'
				
						USE F:\SHOP\SHDATA\trucks IN 0
						
					CASE .cVehicleType == 'MISC'
				
						USE F:\SHOP\SHDATA\VEHMAST IN 0
						
					OTHERWISE
						THROW
				ENDCASE

*!*					* list truck wo's that are missing mileage
*!*					SELECT ORDNO, VEHNO, CRDATE ;
*!*						FROM ORDHDR ;
*!*						INTO CURSOR CURNOMILEAGE ;
*!*						WHERE UPPER(ALLTRIM(TYPE)) = "TRUCK" ;
*!*						AND MILEAGE = 0 ;
*!*						AND NOT EMPTY(VEHNO) ;
*!*						ORDER BY VEHNO

*!*					SELECT CURNOMILEAGE
*!*					BROWSE

				WAIT WINDOW "Selecting data..." NOWAIT
				
				IF 	.lCalcDateRange THEN
				
					*ldStartCRDate = GOMONTH(ldToday,-24)  && 2 Years back per Bill O'Brien
					*ldEndCRDate = DATE()
					
					ldStartCRDate = {^2016-04-01}
					*ldEndCRDate = GOMONTH(ldStartCRDate,12) - 1
					ldEndCRDate = {^2018-03-31}
					m.CPMYEAR = YEAR(ldStartCRDate)
					
					*lcDateRangeWhere = " AND CRDATE >= ldStartCRDate "
					lcDateRangeWhere = " AND BETWEEN(CRDATE,ldStartCRDate,ldEndCRDate) "
					
					.TrackProgress('NOTE: calculating costs for Work Orders between ' + TRANSFORM(ldStartCRDate) + ' and ' + TRANSFORM(ldEndCRDate), LOGIT+SENDIT)
				ELSE
					lcDateRangeWhere = ""
				ENDIF

					
				SELECT VEHNO, CRDATE, REPCAUSE, MILEAGE, TOTHOURS, TOTLCOST, TOTPCOST, TIRECOST, OUTSCOST, 0000.00 AS RATE, MECHNAME, OVNAME, OVCITY, WORKDESC ;
					FROM ORDHDR ;
					INTO CURSOR CURORDHDRPRE ;
					WHERE UPPER(ALLTRIM(TYPE)) = .cVehicleType ;
					AND NOT EMPTY(VEHNO) ;
					&lcDateRangeWhere ;
					ORDER BY CRDATE ;
					READWRITE

* add back in to see only active trucks
*!*						AND ALLTRIM(VEHNO) IN (SELECT ALLTRIM(TRUCK_NUM)AS VEHNO FROM TRUCKS WHERE ACTIVE) ;  

				WAIT WINDOW "Calculating labor costs..." NOWAIT
				
				* POPULATE RATE BY CRDATE AND CALC LABOR COST FOR EACH WO REC
				SELECT CURORDHDRPRE
				SCAN
					lnRate = .GetHourlyRateByCRDate( CURORDHDRPRE.CRDATE )
					REPLACE CURORDHDRPRE.RATE WITH lnRate, CURORDHDRPRE.TOTLCOST WITH ( CURORDHDRPRE.tothours * lnRate )
				ENDSCAN
				
				WAIT WINDOW "Getting distinct Vehicle list..." NOWAIT
				
				SELECT VEHNO, 0000000000 AS MAXMILES, 0000000000 AS BEGNMILES, 0000000000 AS ENDMILES, 0000000000 AS TOTMILEAGE, ;
				SPACE(100) AS COMMENT, 0000.000 AS TOTCPM, 0000.000 AS CPM, 0000.000 AS ACPM, ;
				00000000.00 AS LCOST, 00000000.00 AS PCOST, 00000000.00 AS TIRECOST,  00000000.00 AS OUTSCOST, ;
				00000000.00 AS ALCOST, 00000000.00 AS APCOST, 00000000.00 AS AOUTSCOST, 00000000.00 AS TOTACOSTS ;
				FROM CURORDHDRPRE ;
				INTO CURSOR CURCMP ;
				GROUP BY VEHNO ;
				ORDER BY VEHNO ;
				READWRITE

				WAIT CLEAR
				
				
				WAIT WINDOW "Populating mileages in Vehicle list..." NOWAIT
				
				SELECT CURCMP
				SCAN

					IF USED('CURMAXMILES') THEN
						USE IN CURMAXMILES
					ENDIF
					
					* populate the max() mileage from all work orders in the selected date range
					SELECT MAX(MILEAGE) AS MILEAGE ;
					FROM CURORDHDRPRE ;
					INTO CURSOR CURMAXMILES ;
					WHERE ALLTRIM(VEHNO) == ALLTRIM(CURCMP.VEHNO)  && ;
					** AND CRDATE > {^2015-01-01}

					SELECT CURMAXMILES
					LOCATE
					IF EOF() THEN
						lnMaxMileage = 0
					ELSE
						lnMaxMileage = CURMAXMILES.MILEAGE
					ENDIF

					*REPLACE CURCMP.MAXMILES WITH lnMaxMileage IN CURCMP
					
					IF USED('CURENDMILES') THEN
						USE IN CURENDMILES
					ENDIF					
					
					* ALTERNATIVELY, populate the mileage from the latest work order for each vehicle
					* NOTE: sometimes wo's from outside repairs have entered 1 for mileage, so now filtering those out.
					SELECT MAX(MILEAGE) AS MILEAGE ;
					FROM CURORDHDRPRE ;
					INTO CURSOR CURENDMILES ;
					WHERE ALLTRIM(VEHNO) == ALLTRIM(CURCMP.VEHNO) ;
					AND CRDATE IN (SELECT MAX(CRDATE) FROM CURORDHDRPRE WHERE ALLTRIM(VEHNO) == ALLTRIM(CURCMP.VEHNO) AND MILEAGE > 1)
					
					SELECT CURENDMILES
					lnLastMileage = CURENDMILES.MILEAGE
					
					IF USED('CURBEGNMILES') THEN
						USE IN CURBEGNMILES
					ENDIF					
					
					* get the earliest mileage  chronologically in date range
					SELECT MIN(MILEAGE) AS MILEAGE ;
					FROM CURORDHDRPRE ;
					INTO CURSOR CURBEGNMILES ;
					WHERE ALLTRIM(VEHNO) == ALLTRIM(CURCMP.VEHNO) ;
					AND CRDATE IN (SELECT MIN(CRDATE) FROM CURORDHDRPRE WHERE ALLTRIM(VEHNO) == ALLTRIM(CURCMP.VEHNO) AND MILEAGE > 1)
					
					SELECT CURBEGNMILES
					lnFirstMileage = CURBEGNMILES.MILEAGE
					
					
					* update cursor
					REPLACE CURCMP.BEGNMILES WITH lnFirstMileage, ;
						CURCMP.ENDMILES WITH lnLastMileage ;
						CURCMP.MAXMILES WITH lnMaxMileage ;
						IN CURCMP		
					
								
				
				ENDSCAN	
				
				
				*****************************************************************************************
				IF .lCorrectMileages THEN
					* make mileage corrections to what came from the wo's
					USE F:\UTIL\SHOPREPORTS\DATA\MILEAGE.DBF IN 0 ALIAS MILEAGE
					SELECT MILEAGE
					SCAN
						SELECT CURCMP
						LOCATE FOR UPPER(ALLTRIM(VEHNO)) == UPPER(ALLTRIM(MILEAGE.VEHNO))
						IF FOUND() THEN
							REPLACE CURCMP.ENDMILES WITH MILEAGE.MILEAGE IN CURCMP
							.TrackProgress('!!!! Corrected Mileage for Vehicle # = ' + MILEAGE.VEHNO, LOGIT+SENDIT)
						ENDIF
					ENDSCAN
					IF USED('MILEAGE') THEN
						USE IN MILEAGE
					ENDIF
				ENDIF && .lCorrectMileages
				*****************************************************************************************
							

				SELECT CURCMP
				lnPercent = 0.05
				SCAN
					lnTotMileage = CURCMP.ENDMILES - CURCMP.BEGNMILES
					IF (lnTotMileage < 0) THEN
						REPLACE CURCMP.COMMENT WITH "Ending mileage < beginning mileage; Validate Mileages." IN CURCMP
*!*						ELSE
*!*							lnDifference = ABS((CURCMP.MAXMILES - CURCMP.ENDMILES) / CURCMP.MAXMILES)
*!*							IF lnDifference > lnPercent THEN
*!*								REPLACE CURCMP.COMMENT WITH "Max Mileage vs. Latest Mileage: Difference > " + TRANSFORM(lnPercent * 100) + "%; Validate Mileage" IN CURCMP
*!*							ENDIF
					ENDIF
				ENDSCAN				

				WAIT WINDOW "Populating costs in Vehicle list..." NOWAIT
								
				SELECT CURCMP
				SCAN
					SELECT CURORDHDRPRE
					SUM TOTLCOST TO lnLCost    FOR (ALLTRIM(VEHNO) == ALLTRIM(CURCMP.VEHNO)) AND (REPCAUSE <> "A")
					SUM TOTPCOST TO lnPCost    FOR (ALLTRIM(VEHNO) == ALLTRIM(CURCMP.VEHNO)) AND (REPCAUSE <> "A")
					SUM OUTSCOST TO lnOutscost FOR (ALLTRIM(VEHNO) == ALLTRIM(CURCMP.VEHNO)) AND (REPCAUSE <> "A")
					
					*SUM TIRECOST TO lnTirecost FOR (ALLTRIM(VEHNO) == ALLTRIM(CURCMP.VEHNO)) AND (REPCAUSE <> "A")
					SUM TIRECOST TO lnTirecost FOR (ALLTRIM(VEHNO) == ALLTRIM(CURCMP.VEHNO))
				
					SUM TOTLCOST TO lnALCost    FOR (ALLTRIM(VEHNO) == ALLTRIM(CURCMP.VEHNO)) AND (REPCAUSE = "A")
					SUM TOTPCOST TO lnAPCost    FOR (ALLTRIM(VEHNO) == ALLTRIM(CURCMP.VEHNO)) AND (REPCAUSE = "A")
					SUM OUTSCOST TO lnAOutscost FOR (ALLTRIM(VEHNO) == ALLTRIM(CURCMP.VEHNO)) AND (REPCAUSE = "A")
					*SUM TIRECOST TO lnATirecost FOR (ALLTRIM(VEHNO) == ALLTRIM(CURCMP.VEHNO)) AND (REPCAUSE = "A")
				
					SELECT CURORDHDRPRE
					LOCATE
					
					REPLACE CURCMP.LCOST WITH lnLCost, ;
						CURCMP.PCOST WITH lnPCost, ;
						CURCMP.TIRECOST WITH lnTirecost, ;
						CURCMP.OUTSCOST WITH lnOutscost, ;
						CURCMP.ALCOST WITH lnALCost, ;
						CURCMP.APCOST WITH lnAPCost, ;
						CURCMP.AOUTSCOST WITH lnAOutscost ;
						IN CURCMP
					
				ENDSCAN				


				WAIT WINDOW "Populating costs-per-mile in Vehicle list..." NOWAIT
												
				SELECT CURCMP
				SCAN
				
					*lnTotMileage = CURCMP.MAXMILES
					*lnTotMileage = CURCMP.ENDMILES
					lnTotMileage = CURCMP.ENDMILES - CURCMP.BEGNMILES
					
					IF lnTotMileage > 0 THEN
					
						lnTotNonACosts = CURCMP.LCOST  + CURCMP.PCOST  + CURCMP.OUTSCOST 	
						lnTotAcosts    = CURCMP.ALCOST + CURCMP.APCOST + CURCMP.AOUTSCOST
						lnTotAllCosts  = lnTotNonACosts + lnTotAcosts
					
						lnCPM    = lnTotNonACosts / lnTotMileage
						lnACPM   = lnTotAcosts / lnTotMileage
						lnTotCPM = lnTotAllCosts / lnTotMileage
					
						REPLACE ;
							CURCMP.CPM WITH lnCPM, ;
							CURCMP.ACPM WITH lnACPM, ;
							CURCMP.TOTCPM WITH lnTotCPM, ;
							CURCMP.TotAcosts WITH lnTotAcosts ;
							IN CURCMP
					
					ENDIF && lnTotMileage > 0

					REPLACE ;
						CURCMP.totmileage WITH lnTotMileage ;
						IN CURCMP						
								
				ENDSCAN
				
				* join to get remaining info from appropriate table; Only ACTIVE or not?
				DO CASE
					CASE .cVehicleType == 'TRAILER'
				
						SELECT A.*, B.DIVISION, B.PLATE, B.YEAR, B.MODEL ;
						FROM CURCMP A ;
						LEFT OUTER JOIN TRAILER B ;
						ON ALLTRIM(B.TRAIL_NUM) == ALLTRIM(A.VEHNO) ;
						INTO CURSOR CURCMP2 ;
						WHERE B.ACTIVE ;
						ORDER BY B.DIVISION, A.VEHNO
						
					CASE .cVehicleType == 'TRUCK'
				
						SELECT A.*, B.ACTIVE, B.DIVISION, B.PLATE, B.YEAR, B.MODEL, RIGHT(ALLTRIM(B.VIN),6) AS VIN ;
						FROM CURCMP A ;
						LEFT OUTER JOIN TRUCKS B ;
						ON ALLTRIM(B.TRUCK_NUM) == ALLTRIM(A.VEHNO) ;
						INTO CURSOR CURCMP2 ;
						ORDER BY B.DIVISION, A.VEHNO
						
					CASE .cVehicleType == 'MISC'
				
						SELECT A.*, ' ' AS DIVISION,  ' ' AS PLATE, B.YEAR, B.MODEL ;
						FROM CURCMP A ;
						LEFT OUTER JOIN VEHMAST B ;
						ON ALLTRIM(B.VEHNO) == ALLTRIM(A.VEHNO) ;
						INTO CURSOR CURCMP2 ;
						WHERE B.ACTIVE ;
						ORDER BY B.DIVISION, A.VEHNO
						
					OTHERWISE
						THROW
				ENDCASE

*!*							WHERE INLIST(B.DIVISION,'03','55') ;
				
*!*					SELECT m.CPMYEAR AS CPMYEAR, DIVISION, VEHNO, YEAR, MODEL, MAXMILES, BEGNMILES, ENDMILES, TOTMILEAGE, CPM, ACPM, TOTCPM, LCOST, PCOST, OUTSCOST, TOTACOSTS, COMMENT  ;
				
				SELECT DIVISION, VEHNO, YEAR, MODEL, BEGNMILES, ENDMILES, TOTMILEAGE, CPM, ACPM, TOTCPM, LCOST, PCOST, TIRECOST, OUTSCOST, TOTACOSTS, COMMENT ;
				FROM CURCMP2 ;
				INTO CURSOR CURCMP3 ;
				ORDER BY DIVISION, VEHNO
				
	
*!*				SELECT CURCMP2
*!*				BROWSE
				
				* get the detail of wo's that can help with mileage discrepancies
				SELECT VEHNO, CRDATE, MECHNAME, OVNAME, OVCITY, MILEAGE, LEFT(WORKDESC,50) AS WORKDESC ;
					FROM CURORDHDRPRE ;
					INTO CURSOR CURDETAIL ;
					ORDER BY VEHNO, CRDATE
				
				SELECT CURCMP3
				IF 	.lCalcDateRange THEN
					COPY TO ( "C:\A\" + .cVehicleType + "_CPM_SUMMARY_" + DTOS(ldStartCRDate) + "-" + DTOS(ldEndCRDate) + ".XLS" ) XL5				
					SELECT CURDETAIL
					COPY TO ( "C:\A\" + .cVehicleType + "_CPM_DETAIL_" + DTOS(ldStartCRDate) + "-" + DTOS(ldEndCRDate) + ".XLS" ) XL5		
				ELSE
					COPY TO ("C:\A\" + .cVehicleType + "_COST_PER_MILE.XLS") XL5	 			
				ENDIF
	
*!*					* COLLECT INFO FROM MULTIPLE YEAR-RANGE RUNS INTO ONE BIG TABLE FOR RYAN BRENNAN
*!*					IF FILE('F:\UTIL\SHOPREPORTS\DATA\TRUCKCPM.DBF') THEN
*!*						USE F:\UTIL\SHOPREPORTS\DATA\TRUCKCPM.DBF IN 0 ALIAS TRUCKCPM
*!*						SELECT CURCMP3
*!*						SCAN
*!*							SCATTER MEMVAR MEMO
*!*							INSERT INTO TRUCKCPM FROM MEMVAR
*!*						ENDSCAN
*!*					ELSE
*!*						SELECT CURCMP3
*!*						COPY TO F:\UTIL\SHOPREPORTS\DATA\TRUCKCPM
*!*					ENDIF
				
				
					


*!*					* identify trucks not found
*!*					SELECT CURORDHDRPRE
*!*					SCAN

*!*						WAIT WINDOW "checking Vehicle #" + TRANSFORM(CURORDHDRPRE.VEHNO) NOWAIT

*!*						SELECT trucks
*!*						LOCATE FOR ALLTRIM(truck_num) == ALLTRIM(CURORDHDRPRE.VEHNO)
*!*						IF FOUND() THEN
*!*							* ok
*!*						ELSE
*!*							.TrackProgress('ERROR: TRUCK NOT FOUND: ' + CURORDHDRPRE.VEHNO, LOGIT+SENDIT)
*!*						ENDIF

*!*					ENDSCAN







				*!*					oExcel = CREATEOBJECT("excel.application")
				*!*					oExcel.displayalerts = .F.
				*!*					oExcel.VISIBLE = .F.

				*!*					oWorkbook = oExcel.workbooks.OPEN(lcSpreadsheetTemplate)
				*!*					oWorkbook.SAVEAS(lcFiletoSaveAs)

				*!*					oWorksheet = oWorkbook.Worksheets[1]
				*!*					oWorksheet.RANGE("A5","Q20").clearcontents()
				*!*					oWorksheet.RANGE("D1").VALUE = lcReportPeriod



				*!*					oWorkbook.SAVE()

				*!*					IF TYPE('oExcel') = 'O' AND NOT ISNULL(oExcel) THEN
				*!*						oExcel.QUIT()
				*!*					ENDIF

				*!*					.cAttach = lcFiletoSaveAs







				*******************************************************************************************************************
				.TrackProgress('Vehicle Cost-per-mile process ended normally.', LOGIT+SENDIT)

			CATCH TO loError

				.TrackProgress('There was an error.',LOGIT+SENDIT)
				.TrackProgress('The Error # is: ' + TRANSFORM(loError.ERRORNO), LOGIT+SENDIT)
				.TrackProgress('The Error Message is: ' + TRANSFORM(loError.MESSAGE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred in Program: ' + TRANSFORM(loError.PROCEDURE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred at line #: ' + TRANSFORM(loError.LINENO), LOGIT+SENDIT)
				lnNumberOfErrors = lnNumberOfErrors + 1
				.cSendTo = 'mbennett@fmiint.com'

				IF TYPE('oExcel') = 'O' AND NOT ISNULL(oExcel) THEN
					oExcel.QUIT()
				ENDIF

			ENDTRY

			WAIT CLEAR
			***************** INTERNAL email results ******************************
			.TrackProgress('About to send status email.',LOGIT+SENDIT)
			.TrackProgress('The logfile is: ' + .cLogFile, LOGIT+SENDIT)
			.TrackProgress('Vehicle Cost-per-mile process started: ' + .cStartTime, LOGIT+SENDIT)
			.TrackProgress('Vehicle Cost-per-mile process finished: ' + TTOC(DATETIME()), LOGIT+SENDIT)

			IF .lSendInternalEmailIsOn THEN
				DO FORM dartmail2 WITH .cSendTo,.cFrom,.cSubject,.cCC,.cAttach,.cBodyText,"A"
				.TrackProgress('Sent status email.',LOGIT)
			ELSE
				.TrackProgress('Did not send status email: THIS.lSendInternalEmailIsOn = FALSE.',LOGIT+SENDIT)
			ENDIF

		ENDWITH
		RETURN
	ENDFUNC && main


	FUNCTION GetHourlyRateByCRDate
		LPARAMETERS tdCRdate
		DO CASE
			CASE tdCRdate < {^2008-09-01}
				lnRate = 60.00
			OTHERWISE
				lnRate = 70.00
		ENDCASE
		RETURN lnRate
	ENDFUNC


	FUNCTION GetHourlyRateByDivision
		LPARAMETERS tcDivision
		DO CASE
			CASE INLIST(tcDivision,'05','52','56','57','58','71','74')
				lnRate = 60
			OTHERWISE
				lnRate = 60  && the default rate
		ENDCASE
		RETURN lnRate
	ENDFUNC


	PROCEDURE TrackProgress
		* do any combination of Wait Window, writing to logfile, and adding to body of email,
		* based on nFlags parameter.
		LPARAMETERS tcExpression, tnFlags
		WITH THIS
			IF BITAND(tnFlags,LOGIT) = LOGIT THEN
				IF .lLoggingIsOn THEN
					?
					? .cProcessName + "  " + TTOC(DATETIME()) + ": " + tcExpression
				ENDIF
			ENDIF
			IF .lWaitWindowIsOn THEN
				IF BITAND(tnFlags,NOWAITIT) = NOWAITIT THEN
					WAIT WINDOW tcExpression NOWAIT
				ENDIF
				IF BITAND(tnFlags,WAITIT) = WAITIT THEN
					WAIT WINDOW tcExpression TIMEOUT .nWaitWindowTimeout
				ENDIF
			ENDIF
			IF BITAND(tnFlags,SENDIT) = SENDIT THEN
				IF .lSendInternalEmailIsOn THEN
					.cBodyText = .cBodyText + tcExpression + CRLF + CRLF
				ENDIF
			ENDIF
		ENDWITH
	ENDPROC  &&  TrackProgress
	
	
	PROCEDURE getMileagesForJoeC
	
		CLOSE DATABASES ALL

		USE F:\SHOP\SHDATA\ORDHDR IN 0
		USE F:\SHOP\SHDATA\trucks IN 0

		SELECT VEHNO, CRDATE, REPCAUSE, MILEAGE, TOTHOURS, TOTLCOST, TOTPCOST, OUTSCOST, 0000.00 AS RATE, MECHNAME, OVNAME, OVCITY, WORKDESC ;
			FROM ORDHDR ;
			INTO CURSOR CURORDHDRPRE ;
			WHERE UPPER(ALLTRIM(TYPE)) = "TRUCK" ;
			AND NOT EMPTY(VEHNO) ;
			AND YEAR(CRDATE) = 2016 ;
			AND MILEAGE > 1 ;
			ORDER BY VEHNO, CRDATE ;
			READWRITE
			
			
		* MANUALLY DELETE ANY 'BAD' MILEAGE RECS BEFORE PROCEEDING
		SELECT CURORDHDRPRE
		COPY TO C:\A\TEMP_MILES
		
		

*!*			SELECT A.*, B.DIVISION, B.PLATE, B.YEAR, B.MODEL ;
*!*			FROM C:\A\TEMP_MILES A ;
*!*			LEFT OUTER JOIN TRUCKS B ;
*!*			ON ALLTRIM(B.TRUCK_NUM) == ALLTRIM(A.VEHNO) ;
*!*			INTO CURSOR CURCMP2 ;
*!*			WHERE B.ACTIVE ;
*!*			ORDER BY a.VEHNO, a.CRDATE
		
		
		SELECT VEHNO, ;
		(SELECT MAX(MILEAGE) FROM C:\A\TEMP_MILES WHERE (VEHNO == A.VEHNO) AND CRDATE <= {^2016-03-31}) AS MILES0331, ;
		(SELECT MAX(MILEAGE) FROM C:\A\TEMP_MILES WHERE (VEHNO == A.VEHNO) AND CRDATE <= {^2016-08-31}) AS MILES0831 ;
		FROM C:\A\TEMP_MILES A ;
		INTO CURSOR CURMILES ;
		GROUP BY 1 ;
		ORDER BY 1
		
		SELECT B.DIVISION, A.*, B.PLATE, B.YEAR, B.MODEL ;
		FROM CURMILES A ;
		LEFT OUTER JOIN TRUCKS B ;
		ON ALLTRIM(B.TRUCK_NUM) == ALLTRIM(A.VEHNO) ;
		INTO CURSOR CURFINAL ;
		WHERE B.ACTIVE ;
		ORDER BY B.DIVISION, a.VEHNO
		
*!*			SELECT CURFINAL
*!*			BROW
		
		
		SELECT CURFINAL
		COPY TO C:\A\TRUCKING_MILES_MARCH_VS_AUGUST_2016.XLS XL5
		
	
	ENDPROC
*!*			(SELECT MAX(MILEAGE) WHERE MILEAGE <= {^2016-03-31}) AS MILES0331, ;


ENDDEFINE
