* create or find PPADJ inbound WO when adding P&P carton/unit records into indet
* called from inven, ppdropariat, rcinven, wave, wavedropariat, wavelxe, wavelxeariat

lparameters xaccountid, xstyle, xcolor, xid, xpack, xwhseloc, xctnqty, xwavenum, xadjinven, xrackonhold, xwavepartial, xholdtype

if !empty(xwavenum) or (type("gprocess")="C" and gprocess="WAVELXE")
	xinven="inven"
	xinvenloc="invenloc"
else
	xwavenum=0
	xinven="vinven"
	xinvenloc="vinvenloc"
endif

xdeleted=set("deleted",1)
set deleted on 

if xwhseloc="CYCLE"
	xrackloc="CYCLEU"
else
	xrackloc="RACK"
endif

xinvenrecno=recno(xinven)
xinvenlocrecno=recno(xinvenloc)

ppinwosql(xaccountid, xstyle, xcolor, xid, xpack, xwhseloc, xctnqty, xwavenum, xadjinven, xrackonhold, xwavepartial, xholdtype)

try
	go xinvenrecno in &xinven
	go xinvenlocrecno in &xinvenloc
catch
endtry

return
