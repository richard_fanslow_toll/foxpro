* create headcount / fte by TGF data elements against prior month

* Build EXE as F:\UTIL\HR\HRTGFOVIEWRPT\HRTGFOVIEWRPT.exe

runack("HRTGFOVIEWRPT")

LOCAL loTGFOVERVIEWREPORT
PRIVATE oExcel, oWorkbook

oExcel = CREATEOBJECT("excel.application")
oExcel.displayalerts = .F.
oExcel.VISIBLE = .F.
oWorkbook = .F.

loTGFOVERVIEWREPORT = CREATEOBJECT('TGFOVERVIEWREPORT')

loTGFOVERVIEWREPORT.MAIN()

CLOSE DATABASES ALL
RETURN

#DEFINE LOGIT 1
#DEFINE WAITIT 2
#DEFINE NOWAITIT 4
#DEFINE SENDIT 8
#DEFINE CRLF CHR(13) + CHR(10)
#DEFINE RETURN_DATA_MANDATORY .F.
#DEFINE RETURN_DATA_NOT_MANDATORY .T.
#DEFINE CSVFORMAT 6
#DEFINE WRITE_ONLY_UNBUFFERED 11
#DEFINE COMMA ","
#DEFINE xlEdgeBottom 9
#DEFINE xlContinuous 1
#DEFINE xlMedium -4138
#DEFINE xlFONT_RED -16776961

DEFINE CLASS TGFOVERVIEWREPORT AS CUSTOM

	cProcessName = 'TGFOVERVIEWREPORT'

	lTestMode = .T.  && .F. FOR NORMAL OPERATION, .T. TO SUPPRESS ALL BUT DEVELOPER EMAILS AND CHANGE INPUT TABLE, ETC.

	dToday = DATE()

	lAutoYield = .T.

	cStartTime = TTOC(DATETIME())

	* report properties
	nStartingMonth = 1
*!*		nMinSalaried = 220
*!*		nMinHourly = 475

	* connection properties
	nSQLHandle = 0

	* wait window properties
	nWaitWindowTimeout = 2
	lWaitWindowIsOn = .T.

	* logfile properties
	lLoggingIsOn = .T.
	cLogFile = 'F:\UTIL\HR\HRTGFOVIEWRPT\LOGFILES\TGFOVIEW_Report_log.txt'

	* INTERNAL email properties
	lSendInternalEmailIsOn = .T.
	cFrom = 'Mark.Bennett@Tollgroup.com'
	cSendTo = 'Neil.Devine@Tollgroup.com, Ken.Kausner@Tollgroup.com'
	cCC = 'mbennett@fmiint.com'
	cSubject = 'TGF Overview Report for ' + DTOC(DATE())
	cAttach = ''
	cBodyText = ''

	cCOMPUTERNAME = ''
	cUSERNAME = ''


	FUNCTION INIT
		IF NOT DODEFAULT()
			RETURN .F.
		ENDIF
		WITH THIS
			*SET RESOURCE OFF
			CLOSE DATA
			SET CENTURY ON
			SET DATE AMERICAN
			SET HOURS TO 24
			SET ANSI ON
			SET TALK OFF
			SET DELETED ON
			SET CONSOLE OFF
			SET EXCLUSIVE OFF
			SET SAFETY OFF
			SET EXACT OFF
			SET STATUS BAR ON
			SET SYSMENU OFF
			SET ENGINEBEHAVIOR 70
			_VFP.AUTOYIELD = .lAutoYield
			.cCOMPUTERNAME = GETENV("COMPUTERNAME")
			.cUSERNAME = GETENV("USERNAME")
			IF .lTestMode THEN
				.cLogFile = 'F:\UTIL\HR\HRTGFOVIEWRPT\LOGFILES\TGFOVIEW_Report_log_TESTMODE.txt'
				.cSendTo = 'mbennett@fmiint.com'
				.cCC = ''
			ENDIF
			.lLoggingIsOn = .lLoggingIsOn AND NOT EMPTY(.cLogFile)
			IF .lLoggingIsOn THEN
				SET ALTERNATE TO (.cLogFile) ADDITIVE
				SET ALTERNATE ON
			ENDIF
		ENDWITH
	ENDFUNC


	FUNCTION DESTROY
		WITH THIS
			IF .lLoggingIsOn  THEN
				SET ALTERNATE OFF
				SET ALTERNATE TO
			ENDIF
		ENDWITH
		DODEFAULT()
	ENDFUNC


	FUNCTION MAIN
		WITH THIS
			LOCAL lnNumberOfErrors, lcSQL, lcSQL2, lcSQL3, loError, ldToday
			LOCAL lcFiletoSaveAs, lcSpreadsheetTemplate, lcGrandTotalRow, lcWhere
			LOCAL lnDay, ldFromDate, ldToDate, lcSQLFromDate, lcSQLToDate, lcADPFromDate, lcADPToDate
			LOCAL lcSQLKronos, lcSuffix, lcDivWhere, lcDateSuffix
			LOCAL i, oWorksheet, lnStartRow, lcStartRow, lnRow, lcRow
			LOCAL lnMgmtStartRow, lcMgmtStartRow, lnMgmtEndRow, lcMgmtEndRow, lnPreCasualRow
			LOCAL lnStartingMonth, lnFTE_Salaried, lnFTE_Hourly, lnMinSalaried, lnMinHourly
			LOCAL lnNumHourlyPaydates, lnNumSalariedPaydates, lnFTEHours_Hourly, lnFTEHours_Salaried
			LOCAL lnSalariedPays, lnHourlyPays, lnTotTempHours, lcCell1, lcCell2, lcCategory, lcCollar, lnTotFTE

			TRY
				lnNumberOfErrors = 0

				.TrackProgress("TGF Overview Report process started....", LOGIT+SENDIT+NOWAITIT)
				.TrackProgress('COMPUTERNAME = ' + .cCOMPUTERNAME, LOGIT+SENDIT)
				.TrackProgress('USERNAME = ' + .cUSERNAME, LOGIT+SENDIT)
				.TrackProgress('PROJECT = HRTGFOVIEWRPT', LOGIT+SENDIT)
				IF .lTestMode THEN
					.TrackProgress('*** TEST MODE ***', LOGIT+SENDIT)
				ENDIF

*!*					lnMinSalaried = .nMinSalaried
*!*					lnMinHourly = .nMinHourly

				lnStartRow = 3
				lcStartRow = ALLTRIM(STR(lnStartRow))

				lcSuffix = "_ALL"

				LOCAL ldLastDayPrior, ldFirstDayCurrent

				.TrackProgress('lcSuffix = ' + lcSuffix, LOGIT+SENDIT)

				lnStartingMonth = .nStartingMonth				
				ldToday = .dToday
				
				lnDay = DAY(ldToday)

				ldLastDayPrior = ldToday - lnDay  && this is last day of prior month
				ldFirstDayCurrent = ldLastDayPrior + 1  && this is 1st day of current month
				*ldFromDate = GOMONTH(ldFromDate,-1)  && this is 1st day of prior month

				ldFromDate = ldFirstDayCurrent
				DO WHILE MONTH(ldFromDate) > lnStartingMonth
					ldFromDate = GOMONTH(ldFromDate,-1)
				ENDDO

				ldToDate = ldToday

* ACTIVATE TO FORCE DATES
IF .lTestMode THEN
	ldFromDate = {^2012-03-01}
	ldToDate = {^2012-03-31}
ENDIF

				lcDateSuffix = "_" + DTOS(ldFromDate) + "-" + DTOS(ldToDate)
				lcSuffix = lcSuffix + lcDateSuffix
				.cSubject = 'TGF Overview YTD for ' + DTOC(ldToday)

				lcADPFromDate = "DATE'" + TRANSFORM(YEAR(ldFromDate)) + "-" + PADL(MONTH(ldFromDate),2,'0') + "-" + PADL(DAY(ldFromDate),2,'0') + "'"
				lcADPToDate = "DATE'" + TRANSFORM(YEAR(ldToDate)) + "-" + PADL(MONTH(ldToDate),2,'0') + "-" + PADL(DAY(ldToDate),2,'0') + "'"

				lcSpreadsheetTemplate = 'F:\UTIL\HR\HRTGFOVIEWRPT\TEMPLATES\TGFOVIEW_TEMPLATE.XLS'
				lcFiletoSaveAs = 'F:\UTIL\HR\HRTGFOVIEWRPT\REPORTS\TGFOVIEW' + lcDateSuffix + '.XLS'
				.TrackProgress('Creating spreadsheet: ' + lcFiletoSaveAs, LOGIT+SENDIT+NOWAITIT)

				* delete output file if it already exists...
				IF FILE(lcFiletoSaveAs) THEN
					DELETE FILE (lcFiletoSaveAs)
				ENDIF

				*************************************************************************************
				* FIRST GET TEMP INFO FROM KRONOS
				*************************************************************************************
				.nSQLHandle = SQLCONNECT("WTK2","sa","kr0n1tes")

				IF .nSQLHandle > 0 THEN

					lcSQLFromDate = .GetSQLStartDateTimeString( ldFromDate )
					lcSQLToDate = .GetSQLEndDateTimeString( ldToDate )

					SET TEXTMERGE ON
					TEXT TO	lcSQLKronos NOSHOW
SELECT
SUM((A.DURATIONSECSQTY / 3600.00)) AS HOURS
FROM WFCTOTAL A
JOIN WTKEMPLOYEE B
ON B.EMPLOYEEID = A.EMPLOYEEID
JOIN PERSON C
ON C.PERSONID = B.PERSONID
JOIN LABORACCT D
ON D.LABORACCTID = A.LABORACCTID
JOIN PAYCODE E
ON E.PAYCODEID = A.PAYCODEID
WHERE (A.APPLYDTM >= <<lcSQLFromDate>>)
AND (A.APPLYDTM <= <<lcSQLToDate>>)
AND (D.LABORLEV1NM IN ('TMP'))
					ENDTEXT
					SET TEXTMERGE OFF

					IF .lTestMode THEN
						.TrackProgress('lcSQLKronos = ' + lcSQLKronos, LOGIT+SENDIT)
					ENDIF

					IF .ExecSQL(lcSQLKronos, 'CURTEMPSPRE', RETURN_DATA_NOT_MANDATORY) THEN

						SELECT CURTEMPSPRE
						lnTotTempHours = CURTEMPSPRE.HOURS 
						
					ENDIF  && .ExecSQL

					=SQLDISCONNECT(.nSQLHandle)
				ELSE
					.TrackProgress('Unable to connect to kronos.', LOGIT+SENDIT)
					THROW
				ENDIF && .nSQLHandle > 0 THEN


				.nSQLHandle = SQLCONNECT("PCPAYWIN","MBENNETT","VP737A")

				IF .nSQLHandle > 0 THEN

					lcWhere = ""

					SET TEXTMERGE ON
					TEXT TO	lcSQL NOSHOW
SELECT
STATUS,
COMPANYCODE AS ADP_COMP,
NAME,
FILE# AS FILE_NUM,
GENDER,
{fn ifnull({fn SUBSTRING(CUSTAREA1,1,1)},'?') AS MGMTTYPE,
{fn ifnull({fn SUBSTRING(CUSTAREA1,2,3)},'???') AS TGFDEPT,
{fn ifnull({fn SUBSTRING(CUSTAREA2,1,1)},'F') AS TIMETYPE
FROM REPORTS.V_EMPLOYEE
WHERE COMPANYCODE IN ('E87','E88','E89')
ORDER BY 1, 2, 3
					ENDTEXT
					SET TEXTMERGE OFF



					SET TEXTMERGE ON
					TEXT TO	lcSQL2 NOSHOW
SELECT
COMPANYCODE AS ADP_COMP,
FILE# AS FILE_NUM,
CHECKVIEWHOMEDEPT AS HOMEDEPT,
CHECKVIEWPAYDATE AS PAYDATE,
{FN IFNULL(SUM(CHECKVIEWHOURSAMT),0.00)} AS CODEHOURS,
{FN IFNULL(MAX(CHECKVIEWREGHOURS),0.00)} AS REGHOURS,
{FN IFNULL(MAX(CHECKVIEWOTHOURS),0.00)} AS OTHOURS
FROM REPORTS.V_CHK_VW_HOURS
WHERE CHECKVIEWPAYDATE >= <<lcADPFromDate>>
AND CHECKVIEWPAYDATE <= <<lcADPToDate>>
GROUP BY COMPANYCODE, FILE#, CHECKVIEWHOMEDEPT, CHECKVIEWPAYDATE
ORDER BY 1,2,4
					ENDTEXT
					SET TEXTMERGE OFF



					SET TEXTMERGE ON
					TEXT TO	lcSQL3 NOSHOW
SELECT
DISTINCT
COMPANYCODE AS ADP_COMP,
SOCIALSECURITY# AS SS_NUM,
CHECKVIEWPAYDATE AS PAYDATE
FROM REPORTS.V_CHK_VW_INFO
WHERE CHECKVIEWPAYDATE >= <<lcADPFromDate>>
AND CHECKVIEWPAYDATE <= <<lcADPToDate>>
ORDER BY 1,2,3
					ENDTEXT
					SET TEXTMERGE OFF


					IF .lTestMode THEN
						.TrackProgress('                        TEST MODE                        ', LOGIT+SENDIT)
						.TrackProgress('lcSQL =' + lcSQL, LOGIT+SENDIT)
						.TrackProgress('lcSQL2 =' + lcSQL2, LOGIT+SENDIT)
						.TrackProgress('lcSQL3 =' + lcSQL3, LOGIT+SENDIT)
					ENDIF

					IF USED('CURCUSTOM') THEN
						USE IN CURCUSTOM
					ENDIF
					IF USED('CURHOURSPRE') THEN
						USE IN CURHOURSPRE
					ENDIF
					IF USED('CURHOURS') THEN
						USE IN CURHOURS
					ENDIF
					IF USED('CURTEMPSPRE') THEN
						USE IN CURTEMPSPRE
					ENDIF
					IF USED('CURSALARIEDCNT') THEN
						USE IN CURSALARIEDCNT
					ENDIF
					IF USED('CURHOURLYCNT') THEN
						USE IN CURHOURLYCNT
					ENDIF
					IF USED('CURPAYDATESPRE') THEN
						USE IN CURPAYDATESPRE
					ENDIF
					IF USED('CURPAYDATES') THEN
						USE IN CURPAYDATES
					ENDIF
				
					IF NOT USED('NOTPAYDATES') THEN
						USE F:\UTIL\HR\DATA\NOTPAYDATES.DBF IN 0 ALIAS NOTPAYDATES
					ENDIF

					IF .ExecSQL(lcSQL, 'CURCUSTOM', RETURN_DATA_MANDATORY) AND ;
							.ExecSQL(lcSQL2, 'CURHOURSPRE', RETURN_DATA_MANDATORY) AND ;
							.ExecSQL(lcSQL3, 'CURPAYDATESPRE', RETURN_DATA_MANDATORY) THEN
							
							
						* detemine FTE hours based on salaried and hourly paydates in the data.	
						* WHERE excludes bogus paydates that inflate expected fte hours.	
						SELECT ;
							ADP_COMP, ;
							PAYDATE, ;
							COUNT(*) AS HEADCOUNT ;
							FROM CURPAYDATESPRE ;
							INTO CURSOR CURPAYDATES ;
							WHERE TTOD(PAYDATE) NOT IN ;
							(SELECT PAYDATE FROM NOTPAYDATES WHERE FLAGGED) ;
							GROUP BY ADP_COMP, PAYDATE ;
							ORDER BY ADP_COMP, PAYDATE 

IF .lTestMode THEN							
	SELECT CURPAYDATES
	BROWSE
ENDIF
						
						SELECT CURPAYDATES
						COUNT FOR (ADP_COMP = 'E87') AND (HEADCOUNT > 400) TO lnNumHourlyPaydates
						COUNT FOR (ADP_COMP = 'E88') AND (HEADCOUNT > 150) TO lnNumSalariedPaydates
				
						.TrackProgress('lnNumHourlyPaydates = ' + TRANSFORM(lnNumHourlyPaydates), LOGIT+SENDIT)
						.TrackProgress('lnNumSalariedPaydates = ' + TRANSFORM(lnNumSalariedPaydates), LOGIT+SENDIT)							

						lnFTEHours_Hourly = lnNumHourlyPaydates * 40
						lnFTEHours_Salaried = lnNumSalariedPaydates * 80

						.TrackProgress('lnFTEHours_Hourly = ' + TRANSFORM(lnFTEHours_Hourly), LOGIT+SENDIT)
						.TrackProgress('lnFTEHours_Salaried = ' + TRANSFORM(lnFTEHours_Salaried), LOGIT+SENDIT)
						
						
						* DEFAULT SALARIED TO 80 HOURS FOR EACH SALARIED PAY BEFORE WE ROLL UP THE DATES
						SELECT CURHOURSPRE
						SCAN
							IF (CURHOURSPRE.ADP_COMP <> 'E87') THEN
								REPLACE CURHOURSPRE.REGHOURS WITH 80
							ENDIF
						ENDSCAN

IF .lTestMode THEN							
	SELECT CURHOURSPRE
	BROW						
ENDIF							
							

						SELECT ALLTRIM(ADP_COMP) AS ADP_COMP, ;
							'                              ' AS NAME, ;
							FILE_NUM, ;
							ALLTRIM(HOMEDEPT) AS HOMEDEPT, ;
							SUM(CODEHOURS + REGHOURS + OTHOURS) AS TOTHOURS, ;
							1.00 AS HEADCNT, ;
							1.00 AS FTE, ;
							' ' AS GENDER, ;
							' ' AS MGMTTYPE, ;
							' ' AS TIMETYPE, ;
							'   ' AS TGFDEPT, ;
							'Other                         ' AS TGFCATEG ;
							FROM CURHOURSPRE ;
							INTO CURSOR CURHOURS ;
							GROUP BY 1, 2, 3, 4 ;
							ORDER BY 1, 2, 3, 4 ;
							READWRITE

*!*							* determine FTE Hours for Hourly
*!*							SELECT "HOURLY" AS TYPE, ;
*!*								PAYDATE, ;
*!*								UPPER(CDOW(PAYDATE)) AS WEEKDAY, ;
*!*								COUNT(DISTINCT FILE_NUM) AS HEADCOUNT ;
*!*								FROM CURHOURSPRE ;
*!*								INTO CURSOR CURHOURLYCNT ;
*!*								WHERE INLIST(ADP_COMP,'E87') ;
*!*								GROUP BY 1, 2, 3 ;
*!*								ORDER BY 1, 2, 3

*!*							*!*	SELECT CURHOURLYCNT
*!*							*!*	BROWSE

*!*							SELECT CURHOURLYCNT
*!*							COUNT TO lnHourlyPays FOR (HEADCOUNT > lnMinHourly) AND INLIST(WEEKDAY,"THURSDAY","FRIDAY")
*!*							lnFTE_Hourly = lnHourlyPays * 40

						* POPULATE MISSING INFO FROM OTHER CURSOR

						SELECT CURHOURS
						SCAN
							SELECT CURCUSTOM
							LOCATE FOR ;
								ALLTRIM(ADP_COMP) = ALLTRIM(CURHOURS.ADP_COMP) ;
								AND FILE_NUM = CURHOURS.FILE_NUM ;
								AND ALLTRIM(STATUS) <> 'T'

							IF FOUND() THEN
								REPLACE CURHOURS.TIMETYPE WITH ALLTRIM(CURCUSTOM.TIMETYPE), ;
									CURHOURS.MGMTTYPE WITH ALLTRIM(CURCUSTOM.MGMTTYPE), ;
									CURHOURS.GENDER WITH ALLTRIM(CURCUSTOM.GENDER), ;
									CURHOURS.NAME WITH ALLTRIM(CURCUSTOM.NAME), ;
									CURHOURS.TGFDEPT WITH ALLTRIM(CURCUSTOM.TGFDEPT) ;
									IN CURHOURS
							ELSE
								* active status not found, try terminated in case they are recent term
								SELECT CURCUSTOM
								LOCATE FOR ;
									ALLTRIM(ADP_COMP) = ALLTRIM(CURHOURS.ADP_COMP) ;
									AND FILE_NUM = CURHOURS.FILE_NUM ;
									AND ALLTRIM(STATUS) = 'T'

								IF FOUND() THEN
									REPLACE CURHOURS.TIMETYPE WITH ALLTRIM(CURCUSTOM.TIMETYPE), ;
										CURHOURS.MGMTTYPE WITH ALLTRIM(CURCUSTOM.MGMTTYPE), ;
										CURHOURS.GENDER WITH ALLTRIM(CURCUSTOM.GENDER), ;
										CURHOURS.NAME WITH ALLTRIM(CURCUSTOM.NAME), ;
										CURHOURS.TGFDEPT WITH ALLTRIM(CURCUSTOM.TGFDEPT) ;
										IN CURHOURS
								ELSE
									* NOTHING CAN BE DONE
									SELECT CURCUSTOM
									LOCATE
								ENDIF

							ENDIF

*!*								SELECT CURHOURS
*!*								* for hourly, calc FTE; for salaried leave as 1.0
*!*								DO CASE
*!*									CASE (CURHOURS.ADP_COMP = 'E87') AND (NOT CURHOURS.HOMEDEPT = '020655')
*!*										* EMPLOYEE IS HOURLY, AND NOT AN OTR DRIVER -- OVERWRITE DEFAULT FTE
*!*										REPLACE CURHOURS.FTE WITH ( CURHOURS.TOTHOURS / lnFTE_Hourly ) IN CURHOURS
*!*									OTHERWISE
*!*										* NOTHING
*!*								ENDCASE

						ENDSCAN

						* CALC FTE
						SELECT CURHOURS
						SCAN
							DO CASE
								CASE (CURHOURS.ADP_COMP = 'E87') AND (NOT CURHOURS.HOMEDEPT = '020655')
									* EMPLOYEE IS HOURLY, AND NOT AN OTR DRIVER -- OVERWRITE DEFAULT FTE
									REPLACE CURHOURS.FTE WITH ( CURHOURS.TOTHOURS / lnFTEHours_Hourly ) IN CURHOURS
								CASE INLIST(CURHOURS.ADP_COMP,'E88','E89')
									REPLACE CURHOURS.FTE WITH ( CURHOURS.TOTHOURS / lnFTEHours_Salaried ) IN CURHOURS
								OTHERWISE
									* NOTHING
							ENDCASE
						ENDSCAN

						USE F:\UTIL\ADPREPORTS\DATA\TGFOVIEW IN 0 ALIAS TGFOVIEW


						* populate TGF Category
						SELECT CURHOURS
						SCAN
							SELECT TGFOVIEW
							LOCATE FOR (COLLAR = UPPER(ALLTRIM(CURHOURS.MGMTTYPE))) AND (UPPER(ALLTRIM(CURHOURS.TGFDEPT)) $ TGFOVIEW.TGFDEPTs)
							IF FOUND() THEN
								REPLACE CURHOURS.TGFCATEG WITH TGFOVIEW.CATEGORY
							ENDIF

						ENDSCAN

						*!*	SELECT CURHOURS
						*!*	BROWSE
						
						* INSERT TEMP HOURS INTO CURHOURS; CATEGORY - WAREHOUSE, COLLAR = BLUE
						PRIVATE m.FTE, m.MGMTTYPE, m.TGFCATEG
						*m.FTE = (lnTotTempHours / lnFTE_Hourly)
						m.FTE = (lnTotTempHours / lnFTEHours_Hourly)
						m.MGMTTYPE = "B"
						m.TGFCATEG = "WAREHOUSE (TEMPS)"
						INSERT INTO CURHOURS FROM MEMVAR


						SELECT CURHOURS
						COPY TO ( "F:\UTIL\HR\HRTGFOVIEWRPT\REPORTS\DETAIL\tgfhours" + lcSuffix + ".XLS" ) XL5

						=SQLDISCONNECT(.nSQLHandle)

						* populate the spreadsheet
						oWorkbook = oExcel.workbooks.OPEN(lcSpreadsheetTemplate)
						oWorkbook.SAVEAS(lcFiletoSaveAs)

						oWorksheet = oWorkbook.Worksheets[1]

						oWorksheet.RANGE("B3").VALUE = DTOC(ldFromDate) + " - " + DTOC(ldToDate)						

						SELECT TGFOVIEW
						SCAN
							lcCell1 = UPPER(ALLTRIM(TGFOVIEW.CELL1))
							lcCell2 = UPPER(ALLTRIM(TGFOVIEW.CELL2))
							lcCategory = UPPER(ALLTRIM(TGFOVIEW.CATEGORY))
							lcCollar = UPPER(ALLTRIM(TGFOVIEW.COLLAR))
							
							SELECT CURHOURS
							SUM FTE FOR (UPPER(ALLTRIM(MGMTTYPE)) = lcCollar) AND (UPPER(ALLTRIM(TGFCATEG)) = lcCategory) TO lnTotFTE								
							
							oWorksheet.RANGE(lcCell1).VALUE = lcCategory
							oWorksheet.RANGE(lcCell2).VALUE = lnTotFTE

						ENDSCAN
						*!*							* Ken wants totals for the Mgmt Type section...
						*!*							oWorksheet.RANGE("D" + lcRow).VALUE = "=SUM(D" + lcMgmtStartRow + ":D" + lcMgmtEndRow + ")"
						*!*							oWorksheet.RANGE("E" + lcRow).VALUE = "=SUM(E" + lcMgmtStartRow + ":E" + lcMgmtEndRow + ")"

						*!*							* UNDERLINE cell columns to be totaled...
						*!*							oWorksheet.RANGE("D" + lcMgmtEndRow + ":E" + lcMgmtEndRow).BORDERS(xlEdgeBottom).LineStyle = xlContinuous
						*!*							oWorksheet.RANGE("D" + lcMgmtEndRow + ":E" + lcMgmtEndRow).BORDERS(xlEdgeBottom).Weight = xlMedium

					ENDIF  &&  .ExecSQL(lcSQL, 'SQLCURSOR1', RETURN_DATA_MANDATORY)

					.TrackProgress("TGF Overview Report process ended normally.", LOGIT+SENDIT+NOWAITIT)

				ELSE
					* connection error
					.TrackProgress('Unable to connect to ADP Payroll System.', LOGIT+SENDIT)
				ENDIF   &&  .nSQLHandle > 0

			CATCH TO loError

				.TrackProgress('There was an error.',LOGIT+SENDIT)
				.TrackProgress('The Error # is: ' + TRANSFORM(loError.ERRORNO), LOGIT+SENDIT)
				.TrackProgress('The Error Message is: ' + TRANSFORM(loError.MESSAGE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred in Program: ' + TRANSFORM(loError.PROCEDURE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred at line #: ' + TRANSFORM(loError.LINENO), LOGIT+SENDIT)
				lnNumberOfErrors = lnNumberOfErrors + 1
				IF TYPE('oWorkbook') = "O" THEN
					oWorkbook.SAVE()
				ENDIF
				IF TYPE('oExcel') = "O" THEN
					oExcel.QUIT()
				ENDIF
				CLOSE DATABASES ALL

			ENDTRY

			CLOSE DATA
			WAIT CLEAR
			***************** INTERNAL email results ******************************


			IF (lnNumberOfErrors = 0) THEN
				* SAVE AND QUIT EXCEL
				oWorkbook.SAVE()
				oExcel.QUIT()
			ENDIF

			IF FILE(lcFiletoSaveAs) THEN
				.cAttach = lcFiletoSaveAs
				.cBodyText = "See attached report." + CRLF + CRLF + "<<REPORT LOG FOLLOWS>>" + CRLF + .cBodyText
			ELSE
				.TrackProgress("ERROR attaching " + lcFiletoSaveAs, LOGIT+SENDIT+NOWAITIT)
			ENDIF

			.TrackProgress('About to send status email.',LOGIT)
			.TrackProgress('The logfile is: ' + .cLogFile, SENDIT)
			.TrackProgress("TGF Overview Report process started: " + .cStartTime, LOGIT+SENDIT)
			.TrackProgress("TGF Overview Report process finished: " + TTOC(DATETIME()), LOGIT+SENDIT)

			IF .lSendInternalEmailIsOn THEN
				DO FORM dartmail2 WITH .cSendTo,.cFrom,.cSubject,.cCC,.cAttach,.cBodyText,"A"
				.TrackProgress('Sent status email.',LOGIT)
			ELSE
				.TrackProgress('Did not send status email: THIS.lSendInternalEmailIsOn = FALSE.',LOGIT)
			ENDIF

		ENDWITH
		RETURN
	ENDFUNC && main


	FUNCTION ExecSQL
		LPARAMETERS tcSQL, tcCursorName, tlNoDataReturnedIsOkay
		LOCAL llRetval, lnResult
		WITH THIS
			* close target cursor if it's open
			IF USED(tcCursorName)
				USE IN (tcCursorName)
			ENDIF
			WAIT WINDOW "Fetching remote data; please wait..." NOWAIT
			lnResult = SQLEXEC(.nSQLHandle, tcSQL, tcCursorName)
			llRetval = ( lnResult > 0 )
			IF llRetval THEN
				* see if any data came back
				IF NOT tlNoDataReturnedIsOkay THEN
					IF (NOT USED(tcCursorName)) OR EOF(tcCursorName) THEN
						llRetval = .F.
						.TrackProgress('No data was returned by this query: ' +CRLF + tcSQL, LOGIT+SENDIT)
						.cSendTo = 'Mark Bennett <mbennett@fmiint.com>'
					ENDIF
				ENDIF
			ELSE
				.TrackProgress("Result Code [" + TRANSFORM(lnResult) + "] returned by this query: " + tcSQL, LOGIT+SENDIT)
				.cSendTo = 'Mark Bennett <mbennett@fmiint.com>'
			ENDIF
			WAIT CLEAR
			RETURN llRetval
		ENDWITH
	ENDFUNC


	PROCEDURE TrackProgress
		* do any combination of Wait Window, writing to logfile, and adding to body of email,
		* based on nFlags parameter.
		LPARAMETERS tcExpression, tnFlags
		WITH THIS
			IF BITAND(tnFlags,LOGIT) = LOGIT THEN
				IF .lLoggingIsOn THEN
					?
					? .cProcessName + "  " + TTOC(DATETIME()) + ": " + tcExpression
				ENDIF
			ENDIF
			IF .lWaitWindowIsOn THEN
				IF BITAND(tnFlags,NOWAITIT) = NOWAITIT THEN
					WAIT WINDOW tcExpression NOWAIT
				ENDIF
				IF BITAND(tnFlags,WAITIT) = WAITIT THEN
					WAIT WINDOW tcExpression TIMEOUT .nWaitWindowTimeout
				ENDIF
			ENDIF
			IF BITAND(tnFlags,SENDIT) = SENDIT THEN
				IF .lSendInternalEmailIsOn THEN
					.cBodyText = .cBodyText + tcExpression + CRLF + CRLF
				ENDIF
			ENDIF
		ENDWITH
	ENDPROC  &&  TrackProgress


	FUNCTION GetSQLStartDateTimeString
		* returns a timestamp string for use in SQL queries
		LPARAMETERS tdDate
		LOCAL lcSetDate, lcSQLDateTimeString
		lcSetDate = SET('DATE')
		SET DATE YMD
		lcSQLDateTimeString   = "{ts '" + TRANSFORM(YEAR(tdDate)) + "-" + PADL(MONTH(tdDate),2,"0") + "-" + PADL(DAY(tdDate),2,"0") + " 00:00:00.0'}"
		SET DATE &lcSetDate.
		RETURN lcSQLDateTimeString
	ENDFUNC


	FUNCTION GetSQLEndDateTimeString
		* returns a timestamp string for use in SQL queries
		LPARAMETERS tdDate
		LOCAL lcSetDate, lcSQLDateTimeString
		lcSetDate = SET('DATE')
		SET DATE YMD
		lcSQLDateTimeString = "{ts '" + TRANSFORM(YEAR(tdDate)) + "-" + PADL(MONTH(tdDate),2,"0") + "-" + PADL(DAY(tdDate),2,"0") + " 23:59:59.9'}"
		SET DATE &lcSetDate.
		RETURN lcSQLDateTimeString
	ENDFUNC


ENDDEFINE
