**connect to sql
**
**NOTE: define nhandle prior to calling this function
parameters xsqlserver, xdb

**this sqlsetprop disallows the connection dialog box from appearing if the sqlstrinconnect() fails - mvw 12/28/11
sqlsetprop(0,'DispLogin',3)
sqlsetprop(0,"dispwarnings",.F.)

lcDSNLess="driver=SQL Server;server=&xsqlserver;uid=SA;pwd=&gsqlpassword;DATABASE=&xdb"

xretval=Sqlstringconnect(m.lcDSNLess,.T.)
If xretval<=0 && bailout
	Messagebox("No SQL Connection ("+xsqlserver+") could be established... Call MIS",48,"Error")
endif

return xretval
