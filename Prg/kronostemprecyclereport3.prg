* Create spreadsheet of the latest activity date for Temp Workers.
** Those with no activity at all, or very old activity are candidates for license recycling.

* add Louisville temp agencies 9/18/2014 MB

* 11/18/2016 rewritten to use less complicated SQL statements, because they are often hanging recently (Oct-Nov 2016) for no obvious reason.

* changed SSG U and V for the Carson offices to Chartwell Staffing 1/4/2018 MB. Also added G for Chartwell NJ.

* modified 3/23/2018 MB to add Carina Contreras to U & V reports.
* modified 4/9/2018 to removed Ed Kurowski from the Y Mira Loma report.

* EXEs go in F:\UTIL\KRONOS\

LPARAMETERS tcTempCo

utilsetup("KRONOSTEMPRECYCLEREPORT")

LOCAL loKronosTempRecycleReport, lcTempCo 


loKronosTempRecycleReport = CREATEOBJECT('KronosTempRecycleReport')
IF TYPE('tcTempCo') = "L" THEN
	tcTempCo = "?"
ENDIF
lcTempCo = ALLTRIM(tcTempCo)
loKronosTempRecycleReport.SetTempCo( lcTempCo )
loKronosTempRecycleReport.MAIN()

schedupdate()

CLOSE DATABASES ALL

RETURN

#DEFINE LOGIT 1
#DEFINE WAITIT 2
#DEFINE NOWAITIT 4
#DEFINE SENDIT 8
#DEFINE CRLF CHR(13) + CHR(10)
#DEFINE LF CHR(10)
#DEFINE RETURN_DATA_MANDATORY .F.
#DEFINE RETURN_DATA_NOT_MANDATORY .T.

DEFINE CLASS KronosTempRecycleReport AS CUSTOM

	cProcessName = 'KronosTempRecycleReport'

	lTestMode = .F.  && .F. FOR NORMAL OPERATION, .T. TO SUPPRESS ALL BUT DEVELOPER EMAILS AND CHANGE INPUT TABLE, ETC.

	***********************   SET REPORT TYPE BEFORE BUILDING EXE   **********************
	**************************************************************************************
	**************************************************************************************

	* which payroll server, and associated connections, to hit
	lUsePAYROLL2Server = .T.

	lAutoYield = .T.

	*!*		* !!! When you set this flag to .T. make the exe = KronosTempRecycleReport-TUG.EXE !!!!!
	*!*		lDoSpecialTUGReport = .F.

	cStartTime = TTOC(DATETIME())

	* connection properties
	nSQLHandle = 0

	* file properties
	nFileHandle = 0

	* table properties
	*cFoxProTimeReviewerTable = "F:\UTIL\KRONOS\DATA\TIMERVWR"
	
	* report filter properties
	cTempCo = ""
	cTempCoName = ""

	* date properties
	dtNow = DATETIME()

	dToday = DATE()
	* to force different pay weeks
	*dToday = {^2009-05-26}

	* wait window properties
	nWaitWindowTimeout = 2
	lWaitWindowIsOn = .T.

	* logfile properties
	lLoggingIsOn = .T.
	cLogFile = 'F:\UTIL\KRONOS\Logfiles\KronosTempRecycleReport_log.txt'

	* INTERNAL email properties
	lSendInternalEmailIsOn = .T.
	cFrom = 'FMI Corporate <fmicorporate@fmiint.com>'
	cSendTo = 'mbennett@fmiint.com'
	cCC = ''
	cSubject = 'Kronos Termination Candidates Report for: ' + DTOC(DATE())
	cAttach = ''
	cBodyText = ''

	cCOMPUTERNAME = ''
	cUSERNAME = ''

	FUNCTION INIT
		IF NOT DODEFAULT()
			RETURN .F.
		ENDIF
		WITH THIS
			CLEAR
			*SET RESOURCE OFF
			CLOSE DATA
			SET CENTURY ON
			SET DATE AMERICAN
			SET HOURS TO 24
			SET ANSI ON
			SET TALK OFF
			SET DELETED ON
			SET CONSOLE OFF
			SET EXCLUSIVE OFF
			SET SAFETY OFF
			SET EXACT OFF
			SET STATUS BAR ON
			SET SYSMENU OFF
			SET ENGINEBEHAVIOR 70
			_VFP.AUTOYIELD = .lAutoYield
			.cCOMPUTERNAME = GETENV("COMPUTERNAME")
			.cUSERNAME = GETENV("USERNAME")
			IF .lTestMode THEN
				.cSendTo = 'Mark Bennett <mbennett@fmiint.com>'
				.cLogFile = 'F:\UTIL\KRONOS\Logfiles\KronosTempRecycleReport_log_TESTMODE.txt'
			ENDIF
			.lLoggingIsOn = .lLoggingIsOn AND NOT EMPTY(.cLogFile)
		ENDWITH
	ENDFUNC


	FUNCTION DESTROY
		WITH THIS
			IF .lLoggingIsOn  THEN
				SET ALTERNATE OFF
				SET ALTERNATE TO
			ENDIF
		ENDWITH
		DODEFAULT()
	ENDFUNC


	FUNCTION MAIN
		WITH THIS
			LOCAL lnNumberOfErrors, lcSQL, lcSQL2, lcSQL3, lcSQL4, loError, llValid
			LOCAL lcXLFileName, lcTempCoWhere, lcTempCoWhere2, lcTempCoWhere3, lcTestModeWhere, lcTestModeWhere2
			LOCAL oExcel, oWorkbook, oWorksheet, lnRow, lcRow
			LOCAL lcColsToProcess, lnColumn, lcBoldColumn, i, j, lcBoldRow, luCellValue, lcTargetDirectory, lcFiletoSaveAs
			LOCAL lcFileDate, lcTempCo, lcTitle, llSaveAgain, lcFileDate
			LOCAL lcSendTo, lcCC, lcBodyText, ld14DaysAgo, lcTopBodytext, lt14DaysAgo
			LOCAL lcSQLActivePersons, lcPersonnum

			TRY
				lnNumberOfErrors = 0

				IF .lLoggingIsOn THEN
					SET ALTERNATE TO (.cLogFile) ADDITIVE
					SET ALTERNATE ON
				ENDIF

				.TrackProgress('Kronos Termination Candidates Report process started....', LOGIT+SENDIT+NOWAITIT)
				.TrackProgress('COMPUTERNAME = ' + .cCOMPUTERNAME, LOGIT+SENDIT)
				.TrackProgress('USERNAME = ' + .cUSERNAME, LOGIT+SENDIT)
				.TrackProgress('PROJECT = KRONOSTEMPRECYCLEREPORT', LOGIT+SENDIT)
				IF .lTestMode THEN
					.TrackProgress('                        TEST MODE                        ', LOGIT+SENDIT)
				ENDIF
				
				IF EMPTY(.cTempCoName) THEN
					.TrackProgress('Missing or Invalid Temp CO Parameter!', LOGIT+SENDIT)
					THROW	
				ENDIF

				ld14DaysAgo = .dToday - 14
				SET DATE YMD				
				lt14DaysAgo   = "{ts '" + TRANSFORM(YEAR(ld14DaysAgo)) + "-" + PADL(MONTH(ld14DaysAgo),2,"0") + "-" + PADL(DAY(ld14DaysAgo),2,"0") + " 00:00:00.0'}"
				SET DATE AMERICAN

				
*!*					IF FILE('C:\TEMPFOX\TEMPS_TO_TERM.XLS') THEN
*!*						DELETE FILE C:\TEMPFOX\TEMPS_TO_TERM.XLS
*!*					ENDIF
								
				IF USED('CUR_ACTIVE') THEN
					USE IN CUR_ACTIVE
				ENDIF
								
				IF USED('CUR_ACTIVE2') THEN
					USE IN CUR_ACTIVE2
				ENDIF
								
				IF NOT .lTestMode THEN
					* assign Send to based on TempCo (which indicates temp agency)
					DO CASE
						CASE .cTempCo = 'D'
							.cSendTo = 'Marty.Cuniff@tollgroup.com, Bill.Marsh@tollgroup.com, Jim.Lake@tollgroup.com, sueoqualitytemps@outlook.com'
							.cCC = 'mbennett@fmiint.com'
						CASE .cTempCo = 'Q'
							.cSendTo = 'Marty.Cuniff@tollgroup.com, Bill.Marsh@tollgroup.com, Jim.Lake@tollgroup.com, toll@chartwellstaff.com'
							.cCC = 'mbennett@fmiint.com'
						CASE .cTempCo = 'Q'
							.cSendTo = 'cheri.foster@tollgroup.com, dwayne.sutton@tollgroup.com, robin.ragg@tollgroup.com, cshields@cor-tech.net, jthrasher@cor-tech.net, ealvarez@cor-tech.net, rlayton@cor-tech.net'
							.cCC = 'mbennett@fmiint.com'
						CASE .cTempCo = 'R'
							.cSendTo = 'ted.makros@tollgroup.com, jcano@thessg.com, malvarez@thessg.com'
							.cCC = 'mbennett@fmiint.com'
						CASE .cTempCo = 'T'
							.cSendTo = 'jsantisteban@thessg.com, joni.golding@tollgroup.com'
							.cCC = 'mbennett@fmiint.com'
						CASE .cTempCo = 'U'
							.cSendTo = 'juan.santisteban@yourstaffingfirm.com, carina.contreras@yourstaffingfirm.com, martha.diaz@yourstaffingfirm.com, Sandra.Senbol@yourstaffingfirm.com'
							.cCC = 'mbennett@fmiint.com, maria.sanchez@yourstaffingfirm.com, Cynthia.Stopani@yourstaffingfirm.com, crystal.hernandez@yourstaffingfirm.com'
						CASE .cTempCo = 'V'
							.cSendTo = 'juan.santisteban@yourstaffingfirm.com, carina.contreras@yourstaffingfirm.com, martha.diaz@yourstaffingfirm.com, Sandra.Senbol@yourstaffingfirm.com'
							.cCC = 'mbennett@fmiint.com, maria.sanchez@yourstaffingfirm.com, Cynthia.Stopani@yourstaffingfirm.com, crystal.hernandez@yourstaffingfirm.com'
						CASE .cTempCo = 'X'
							.cSendTo = 'toll@chartwellstaff.com'
							.cCC = 'mbennett@fmiint.com'
						CASE .cTempCo = 'Y'
							.cSendTo = 'Sandra.Senbol@yourstaffingfirm.com, maria.sanchez@yourstaffingfirm.com, joni.golding@tollgroup.com, wendy.hernandez@yourstaffingfirm.com, eloisa.ramirez@yourstaffingfirm.com'
							.cCC = 'mbennett@fmiint.com'
						OTHERWISE
							.cSendTo = 'mbennett@fmiint.com'
							.cCC = ""
					ENDCASE
				ENDIF

				.cSubject = 'Termination Candidates Report for ' + .cTempCoName + ' ' +  DTOC(.dToday)

				* main SQL
*!*				SET TEXTMERGE ON

*!*				TEXT TO	lcSQL NOSHOW
*!*				
*!*	SELECT
*!*	A.PERSONNUM AS FILE_NUM, 
*!*	A.APPLYDATE AS WORKDATE, 
*!*	A.PAYCODENAME 
*!*	FROM VP_TOTALS A 
*!*	WHERE (A.LABORLEVELNAME1 = 'TMP') 
*!*	<<lcTempCoWhere>> 
*!*	AND A.PERSONNUM IN (SELECT PERSONNUM FROM VP_EMPLOYEEV42 WHERE (EMPLOYMENTSTATUS = 'Active') AND (HOMELABORLEVELNM1 = 'TMP')) 
*!*	AND (A.PAYCODETYPE = 'P') 
*!*	AND (A.TIMEINSECONDS > 0) 
*!*	AND (A.PAYCODENAME = 'Regular Hours') 
*!*	AND A.APPLYDATE > <<lt14DaysAgo>>
*!*	ORDER BY A.APPLYDATE, A.PERSONNUM 
*!*						
*!*				ENDTEXT
*!*				
*!*				SET TEXTMERGE OFF

*!*				SET TEXTMERGE ON
*!*				TEXT TO	lcSQL NOSHOW

*!*	SELECT
*!*	DISTINCT
*!*	A.PERSONFULLNAME AS NAME,
*!*	A.PERSONNUM AS FILE_NUM, 
*!*	(SELECT MAX(APPLYDATE) FROM VP_TOTALS WHERE (PERSONNUM = A.PERSONNUM) AND (PAYCODETYPE = 'P') AND (TIMEINSECONDS > 0) AND (PAYCODENAME = 'Regular Hours')) AS LASTPUNCHDATE
*!*	FROM VP_TOTALS A 
*!*	WHERE (A.LABORLEVELNAME1 = 'TMP') 
*!*	<<lcTempCoWhere>> 
*!*	AND A.PERSONNUM IN (SELECT PERSONNUM FROM VP_EMPLOYEEV42 WHERE (EMPLOYMENTSTATUS = 'Active') AND (HOMELABORLEVELNM1 = 'TMP')) 
*!*	AND A.PERSONNUM NOT IN			
*!*	(SELECT
*!*	PERSONNUM AS FILE_NUM 
*!*	FROM VP_TOTALS
*!*	WHERE (LABORLEVELNAME1 = 'TMP') 
*!*	<<lcTempCoWhere>> 
*!*	AND PERSONNUM IN (SELECT PERSONNUM FROM VP_EMPLOYEEV42 WHERE (EMPLOYMENTSTATUS = 'Active') AND (HOMELABORLEVELNM1 = 'TMP')) 
*!*	AND (PAYCODETYPE = 'P') 
*!*	AND (TIMEINSECONDS > 0) 
*!*	AND (PAYCODENAME = 'Regular Hours') 
*!*	AND APPLYDATE > <<lt14DaysAgo>>)
*!*						
*!*				ENDTEXT			
*!*				SET TEXTMERGE OFF
				* filter by TempCo if there was a passed TempCo parameters
				
				IF NOT EMPTY(.cTempCo) THEN
				
					lcTempCoWhere = " AND (A.LABORLEVELNAME5 = '" + .cTempCo + "') "
					lcTempCoWhere2 = " AND (HOMELABORLEVELNM5 = '" + .cTempCo + "') "					
					lcTempCoWhere3 = " AND (LABORLEVELNAME5 = '" + .cTempCo + "') "
				ELSE
					lcTempCoWhere = ""
					lcTempCoWhere2 = ""
					lcTempCoWhere3 = ""
				ENDIF

* added <<lcTempCoWhere2>> below so that folks whose primary account has changed to a different office are excluded 10/31/2016 MB
* also took DISTINCT our of main sql 11/3/2016, to try and speed up the query, which was taking 12 minutes in some cases!

*!*				SET TEXTMERGE ON
*!*				TEXT TO	lcSQL NOSHOW

*!*	SELECT
*!*	A.PERSONFULLNAME AS NAME,
*!*	A.PERSONNUM AS FILE_NUM, 
*!*	A.LABORLEVELNAME2 AS DIVISION, 
*!*	(SELECT MAX(APPLYDATE) FROM VP_TOTALS WHERE (PERSONNUM = A.PERSONNUM) AND (PAYCODETYPE = 'P') AND (TIMEINSECONDS > 0) AND (PAYCODENAME = 'Regular Hours')) AS LASTPUNCHDATE
*!*	FROM VP_TOTALS A 
*!*	WHERE (A.LABORLEVELNAME1 = 'TMP') 
*!*	<<lcTempCoWhere>> 
*!*	AND A.PERSONNUM IN (SELECT PERSONNUM FROM VP_EMPLOYEEV42 WHERE (EMPLOYMENTSTATUS = 'Active') AND (HOMELABORLEVELNM1 = 'TMP') <<lcTempCoWhere2>>) 
*!*	AND A.PERSONNUM NOT IN			
*!*	(SELECT PERSONNUM AS FILE_NUM FROM VP_TOTALS
*!*	WHERE (LABORLEVELNAME1 = 'TMP')<<lcTempCoWhere3>> 
*!*	AND PERSONNUM IN (SELECT PERSONNUM FROM VP_EMPLOYEEV42 WHERE (EMPLOYMENTSTATUS = 'Active') AND (HOMELABORLEVELNM1 = 'TMP')) 
*!*	AND (PAYCODETYPE = 'P') 
*!*	AND (TIMEINSECONDS > 0) 
*!*	AND (PAYCODENAME = 'Regular Hours') 
*!*	AND APPLYDATE > <<lt14DaysAgo>>)
*!*						
*!*				ENDTEXT			
*!*				SET TEXTMERGE OFF



			SET TEXTMERGE ON
			TEXT TO	lcSQLActivePersons NOSHOW

SELECT
PERSONFULLNAME AS NAME,	
PERSONNUM,
HOMELABORACCTNAME AS LABRACCT,
EMPLOYMENTSTATUS AS STATUS
FROM VP_EMPLOYEEV42
WHERE HOMELABORLEVELNM1 = 'TMP'<<lcTempCoWhere2>>
AND EMPLOYMENTSTATUS IN ('Active')
ORDER BY PERSONFULLNAME
					
			ENDTEXT			
			SET TEXTMERGE OFF


				*IF .lTestMode THEN
					.TrackProgress('', LOGIT+SENDIT)
					.TrackProgress('lcSQLActivePersons =' + lcSQLActivePersons, LOGIT+SENDIT)
				*ENDIF

				.TrackProgress('Connecting to WTK System....', LOGIT+SENDIT)
		
				WAIT WINDOW NOWAIT "Preparing data..."

*!*					* access kronos
*!*					OPEN DATABASE F:\UTIL\KRONOS\KRONOSEDI

				.nSQLHandle = SQLCONNECT("WTK2","sa","kr0n1tes")

				IF .nSQLHandle > 0 THEN

					IF .ExecSQL(lcSQLActivePersons, 'CUR_ACTIVE', RETURN_DATA_MANDATORY, .T.) THEN


						SELECT NAME, PERSONNUM, LABRACCT, STATUS, {} AS PUNCHDATE ;
							FROM CUR_ACTIVE ;
							INTO CURSOR CUR_TO_TERMINATE ;
							READWRITE

						*SELECT CUR_TO_TERMINATE 
						*BROWSE
						
						*THROW

						* LOOP THRU ACTIVE ASSOCIATES AND RETRIEVE THE LATEST PAY CODE DATE FOR EACH					
						SELECT CUR_TO_TERMINATE 
						SCAN
						
							WAIT WINDOW NOWAIT CUR_TO_TERMINATE.NAME
						
							lcPersonnum = CUR_TO_TERMINATE.PERSONNUM
								
							SET TEXTMERGE ON
							TEXT TO	lcSQLMax NOSHOW
							
SELECT MAX(APPLYDATE) AS APPLYDATE 
FROM VP_TOTALS
WHERE (PERSONNUM = '<<lcPersonnum>>') AND (PAYCODETYPE = 'P') AND (TIMEINSECONDS > 0)
					
							ENDTEXT			
							SET TEXTMERGE OFF

							IF USED('CURMAXAPPLY') THEN
								USE IN CURMAXAPPLY
							ENDIF
							
							IF .ExecSQL(lcSQLMax, 'CURMAXAPPLY', RETURN_DATA_MANDATORY, .F.) THEN		
							
								IF USED('CURMAXAPPLY') AND NOT EOF('CURMAXAPPLY') THEN
									REPLACE CUR_TO_TERMINATE.PUNCHDATE WITH NVL(CURMAXAPPLY.APPLYDATE,{}) IN CUR_TO_TERMINATE	
								ELSE
									REPLACE CUR_TO_TERMINATE.PUNCHDATE WITH {} IN CUR_TO_TERMINATE	
								ENDIF
							
							ENDIF
						
						ENDSCAN
	
						WAIT CLEAR
	
	
	
*!*		SELECT CUR_TO_TERMINATE 
*!*		BROWSE
*!*		
*!*		
*!*		
*!*		
*!*		
*!*		
*!*		
*!*		THROW			
						SELECT * ;
						FROM CUR_TO_TERMINATE ;
						INTO CURSOR CUR_TO_TERMINATE2 ;
						WHERE NOT EMPTY(PUNCHDATE ) ;
						AND (PUNCHDATE < ld14DaysAgo) ;
						ORDER BY PUNCHDATE 

*!*		SELECT CUR_TO_TERMINATE2
*!*		BROWSE


						IF USED('CUR_TO_TERMINATE2') AND NOT EOF('CUR_TO_TERMINATE2') THEN
						
							lcTopBodytext = "To preserve Timekeeper licenses, please terminate any temps who have not punched recently." + CRLF + ;
								"The attached spreadsheet lists temps who have not punched in the last 2 weeks." + CRLF
						
							WAIT WINDOW NOWAIT "Opening Excel..."
							oExcel = CREATEOBJECT("excel.application")
							oExcel.VISIBLE = .F.
							oExcel.DisplayAlerts = .F.
							oWorkbook = oExcel.workbooks.OPEN("F:\UTIL\KRONOS\TEMPLATES\KronosTempRecycleReport.XLSX")

							WAIT WINDOW NOWAIT "Looking for target directory..."
							* see if target directory exists, and, if not, create it, e.g. F:\timeclk\adpfiles\01-07-2005
							lcFileDate = PADL(MONTH(.dToday),2,"0") + "-"  + PADL(DAY(.dToday),2,"0") + "-" + PADL(YEAR(.dToday),4,"0")
							lcTargetDirectory = "F:\UTIL\KRONOS\TEMPTIMEREPORTS\" && + lcFileDate + "\"

							lcFiletoSaveAs = lcTargetDirectory + .cTempCoName + " Recycling " + lcFileDate
							lcXLFileName = lcFiletoSaveAs + ".XLSX"

							* create directory if it doesn't exist
							IF NOT DIRECTORY(lcTargetDirectory) THEN
								MKDIR (lcTargetDirectory)
								WAIT WINDOW TIMEOUT 1 "Made Target Directory " + lcTargetDirectory
							ENDIF

							* if target directory exists, save there
							llSaveAgain = .F.
							IF DIRECTORY(lcTargetDirectory) THEN
								IF FILE(lcXLFileName) THEN
									DELETE FILE (lcXLFileName)
								ENDIF
								oWorkbook.SAVEAS(lcFiletoSaveAs,51)
								* set flag to save again after sheet is populated
								llSaveAgain = .T.
							ENDIF

							WAIT WINDOW NOWAIT "Building Temp Recycling spreadsheet..."

							lnRow = 2
							lnStartRow = lnRow + 1
							lcStartRow = ALLTRIM(STR(lnStartRow))

							oWorksheet = oWorkbook.Worksheets[1]
							oWorksheet.RANGE("A" + lcStartRow,"D500").clearcontents()

							oWorksheet.RANGE("A" + lcStartRow,"D500").FONT.SIZE = 11
							oWorksheet.RANGE("A" + lcStartRow,"D500").FONT.NAME = "Arial"
							oWorksheet.RANGE("A" + lcStartRow,"D500").FONT.bold = .F.
														
							oWorksheet.RANGE("A1").VALUE = .cSubject

							
							SELECT CUR_TO_TERMINATE2
							SCAN
								lnRow = lnRow + 1
								lcRow = LTRIM(STR(lnRow))
								oWorksheet.RANGE("A" + lcRow).VALUE = CUR_TO_TERMINATE2.NAME
								oWorksheet.RANGE("B" + lcRow).VALUE = "'" + CUR_TO_TERMINATE2.PERSONNUM
								oWorksheet.RANGE("C" + lcRow).VALUE = CUR_TO_TERMINATE2.PUNCHDATE
								oWorksheet.RANGE("D" + lcRow).VALUE = 'Active'
								oWorksheet.RANGE("E" + lcRow).VALUE = CUR_TO_TERMINATE2.LABRACCT
								oWorksheet.RANGE("A" + lcRow,"E" + lcRow).Interior.ColorIndex = IIF(((lnRow % 2) = 1),15,0)
							ENDSCAN
							
	*!*							lnRow = lnRow + 1
	*!*							lcRow = LTRIM(STR(lnRow))

							oWorksheet.PageSetup.PrintArea = "$A$"  + lcStartRow + ":$E$" + lcRow

							* save again
							IF llSaveAgain THEN
								oWorkbook.SAVE()
							ENDIF
							oWorkbook.CLOSE()

							oExcel.QUIT()

							IF FILE(lcXLFileName) THEN
								.cAttach = lcXLFileName
								.TrackProgress('Created Temp Recycling Report File : ' + lcXLFileName, LOGIT+SENDIT)
								.TrackProgress('Kronos Termination Candidates Report process ended normally.', LOGIT+SENDIT+NOWAITIT)
							ELSE
								.TrackProgress('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!', LOGIT+SENDIT)
								.TrackProgress("ERROR: There was a problem creating Temp Recycling File : " + lcXLFileName, LOGIT+SENDIT)
								.TrackProgress('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!', LOGIT+SENDIT)
							ENDIF
						ELSE
							lcTopBodytext = "There are no temps available to recycle -- please contact Mark Bennett if you need to bring in more temps."
						ENDIF  && USED('CUR_TO_TERMINATE2')
						
						.cBodyText = lcTopBodytext + CRLF + CRLF + "======= report log follows ========" + CRLF + CRLF + .cBodyText

*!*							IF FILE('C:\TEMPFOX\TEMPS_TO_TERM.XLS') THEN
*!*								.cAttach = .cAttach + ";C:\TEMPFOX\TEMPS_TO_TERM.XLS"
*!*							ENDIF

					ENDIF  &&  .ExecSQL(...)

					CLOSE DATABASES ALL
				ELSE
					* connection error
					.TrackProgress('Unable to connect to WTK System.', LOGIT+SENDIT)
				ENDIF   &&  .nSQLHandle > 0
			CATCH TO loError

				.TrackProgress('There was an error.',LOGIT+SENDIT)
				.TrackProgress('The Error # is: ' + TRANSFORM(loError.ERRORNO), LOGIT+SENDIT)
				.TrackProgress('The Error Message is: ' + TRANSFORM(loError.MESSAGE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred in Program: ' + TRANSFORM(loError.PROCEDURE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred at line #: ' + TRANSFORM(loError.LINENO), LOGIT+SENDIT)
				lnNumberOfErrors = lnNumberOfErrors + 1
				IF TYPE('oExcel') = 'O' AND NOT ISNULL(oExcel) THEN
					oExcel.QUIT()
				ENDIF
				CLOSE DATA
				CLOSE ALL

			ENDTRY

			CLOSE DATA
			WAIT CLEAR
			***************** INTERNAL email results ******************************
			.TrackProgress('About to send status email.',LOGIT)
			.TrackProgress('==================================================================================================================', SENDIT)
			.TrackProgress('The logfile is: ' + .cLogFile, SENDIT)
			.TrackProgress('Kronos Termination Candidates Report process started: ' + .cStartTime, LOGIT+SENDIT)
			.TrackProgress('Kronos Termination Candidates Report process finished: ' + TTOC(DATETIME()), LOGIT+SENDIT)

			IF .lSendInternalEmailIsOn THEN
				* try to trap error from not having dartmail dll's registered on user's pc...

				TRY
					DO FORM dartmail2 WITH .cSendTo,.cFrom,.cSubject,.cCC,.cAttach,.cBodyText,"A"
					.TrackProgress('Sent status email.',LOGIT)
				CATCH TO loError
					*=MESSAGEBOX('There was an error sending the status email. Please contact Mark Bennett.',0+64,"KRONOS Temp Time")
					.TrackProgress('There was an error sending the status email.',LOGIT)
					.TrackProgress('The Error # is: ' + TRANSFORM(loError.ERRORNO), LOGIT)
					.TrackProgress('The Error Message is: ' + TRANSFORM(loError.MESSAGE), LOGIT)
					.TrackProgress('The Error occurred in Program: ' + TRANSFORM(loError.PROCEDURE), LOGIT)
					.TrackProgress('The Error occurred at line #: ' + TRANSFORM(loError.LINENO), LOGIT)
					lnNumberOfErrors = lnNumberOfErrors + 1
					CLOSE DATA
				ENDTRY

			ELSE
				.TrackProgress('Did not send status email: THIS.lSendInternalEmailIsOn = FALSE.',LOGIT)
			ENDIF

		ENDWITH
		RETURN
	ENDFUNC && main


	FUNCTION ExecSQL
		LPARAMETERS tcSQL, tcCursorName, tlNoDataReturnedIsOkay, tlLogSQLErrorInEmail
		LOCAL llRetval, lnResult
		WITH THIS
			* close target cursor if it's open
			IF USED(tcCursorName)
				USE IN (tcCursorName)
			ENDIF
			WAIT WINDOW "Fetching remote data; please wait..." NOWAIT
			lnResult = SQLEXEC(.nSQLHandle, tcSQL, tcCursorName)
			llRetval = ( lnResult > 0 )
			IF llRetval THEN
				* see if any data came back
				IF NOT tlNoDataReturnedIsOkay THEN
					IF (NOT USED(tcCursorName)) OR EOF(tcCursorName) THEN
						llRetval = .F.
						.TrackProgress('No data was returned by this query: ' +CRLF + tcSQL, LOGIT+SENDIT)
					ENDIF
				ENDIF
			ELSE
				IF tlLogSQLErrorInEmail THEN
					.TrackProgress("Result Code [" + TRANSFORM(lnResult) + "] returned by this query: " + tcSQL, LOGIT+SENDIT)
				ENDIF
			ENDIF
			WAIT CLEAR
			RETURN llRetval
		ENDWITH
	ENDFUNC


	PROCEDURE TrackProgress
		* do any combination of Wait Window, writing to logfile, and adding to body of email,
		* based on nFlags parameter.
		LPARAMETERS tcExpression, tnFlags
		WITH THIS
			IF BITAND(tnFlags,LOGIT) = LOGIT THEN
				IF .lLoggingIsOn THEN
					?
					? .cProcessName + "  " + TTOC(DATETIME()) + ": " + tcExpression
				ENDIF
			ENDIF
			IF .lWaitWindowIsOn THEN
				IF BITAND(tnFlags,NOWAITIT) = NOWAITIT THEN
					WAIT WINDOW tcExpression NOWAIT
				ENDIF
				IF BITAND(tnFlags,WAITIT) = WAITIT THEN
					WAIT WINDOW tcExpression TIMEOUT .nWaitWindowTimeout
				ENDIF
			ENDIF
			IF BITAND(tnFlags,SENDIT) = SENDIT THEN
				IF .lSendInternalEmailIsOn THEN
					.cBodyText = .cBodyText + tcExpression + CRLF + CRLF
				ENDIF
			ENDIF
		ENDWITH
	ENDPROC  &&  TrackProgress
	
	
	PROCEDURE SetTempCo
		LPARAMETERS tcTempCo
		WITH THIS
			DO CASE
				CASE tcTempCo = 'A'
					.cTempCo = tcTempCo
					.cTempCoName = 'Tri-State SP '
					.TrackProgress('Set TempCo = ' + tcTempCo,LOGIT+SENDIT)
					.TrackProgress('Set TempCoName = ' + .cTempCoName,LOGIT+SENDIT)
				CASE tcTempCo = 'B'
					.cTempCo = tcTempCo
					.cTempCoName = 'Select Staffing ML '
					.TrackProgress('Set TempCo = ' + tcTempCo,LOGIT+SENDIT)
					.TrackProgress('Set TempCoName = ' + .cTempCoName,LOGIT+SENDIT)
				CASE tcTempCo = 'D'
					.cTempCo = tcTempCo
					.cTempCoName = 'Quality NJ'
					.TrackProgress('Set TempCo = ' + tcTempCo,LOGIT+SENDIT)
					.TrackProgress('Set TempCoName = ' + .cTempCoName,LOGIT+SENDIT)
				CASE tcTempCo = 'J'
					.cTempCo = tcTempCo
					.cTempCoName = 'Diamond Staffing - NJ'
					.TrackProgress('Set TempCo = ' + tcTempCo,LOGIT+SENDIT)
					.TrackProgress('Set TempCoName = ' + .cTempCoName,LOGIT+SENDIT)
				CASE tcTempCo = 'K'
					.cTempCo = tcTempCo
					.cTempCoName = 'Tri-State ML'
					.TrackProgress('Set TempCo = ' + tcTempCo,LOGIT+SENDIT)
					.TrackProgress('Set TempCoName = ' + .cTempCoName,LOGIT+SENDIT)
				CASE tcTempCo = 'L'
					.cTempCo = tcTempCo
					.cTempCoName = 'CRSCO - Louisville'
					.TrackProgress('Set TempCo = ' + tcTempCo,LOGIT+SENDIT)
					.TrackProgress('Set TempCoName = ' + .cTempCoName,LOGIT+SENDIT)
				CASE tcTempCo = 'N'
					.cTempCo = tcTempCo
					.cTempCoName = 'LaborReady - Louisville'
					.TrackProgress('Set TempCo = ' + tcTempCo,LOGIT+SENDIT)
					.TrackProgress('Set TempCoName = ' + .cTempCoName,LOGIT+SENDIT)
				CASE tcTempCo = 'O'
					.cTempCo = tcTempCo
					.cTempCoName = 'Express Staffing - Louisville'
					.TrackProgress('Set TempCo = ' + tcTempCo,LOGIT+SENDIT)
					.TrackProgress('Set TempCoName = ' + .cTempCoName,LOGIT+SENDIT)
				CASE tcTempCo = 'P'
					.cTempCo = tcTempCo
					.cTempCoName = 'Packaging & More LLC - Louisville'
					.TrackProgress('Set TempCo = ' + tcTempCo,LOGIT+SENDIT)
					.TrackProgress('Set TempCoName = ' + .cTempCoName,LOGIT+SENDIT)
				CASE tcTempCo == 'Q'
					.cTempCo = tcTempCo
					.cTempCoName = 'Cor-Tech - Louisville'
					.TrackProgress('Set TempCo = ' + tcTempCo,LOGIT+SENDIT)
					.TrackProgress('Set TempCoName = ' + .cTempCoName,LOGIT+SENDIT)
				CASE tcTempCo == 'R'
					.cTempCo = tcTempCo
					.cTempCoName = 'Staffing Solutions - SP'
					.TrackProgress('Set TempCo = ' + tcTempCo,LOGIT+SENDIT)
					.TrackProgress('Set TempCoName = ' + .cTempCoName,LOGIT+SENDIT)
				CASE INLIST(tcTempCo,'S')
					.cTempCo = tcTempCo
					.cTempCoName = 'Kamran Staffing - ML'
					.TrackProgress('Set TempCo = ' + tcTempCo,LOGIT+SENDIT)
					.TrackProgress('Set TempCoName = ' + .cTempCoName,LOGIT+SENDIT)
				CASE tcTempCo == 'T'
					.cTempCo = tcTempCo
					.cTempCoName = 'Staffing Solutions - ML'
					.TrackProgress('Set TempCo = ' + tcTempCo,LOGIT+SENDIT)
					.TrackProgress('Set TempCoName = ' + .cTempCoName,LOGIT+SENDIT)
				CASE tcTempCo == 'U'
					.cTempCo = tcTempCo
					.cTempCoName = 'Simplified Labor Staffing Solutions - Carson'
					.TrackProgress('Set TempCo = ' + tcTempCo,LOGIT+SENDIT)
					.TrackProgress('Set TempCoName = ' + .cTempCoName,LOGIT+SENDIT)
				CASE tcTempCo == 'V'
					.cTempCo = tcTempCo
					.cTempCoName = 'Simplified Labor Staffing Solutions - Carson 2'
					.TrackProgress('Set TempCo = ' + tcTempCo,LOGIT+SENDIT)
					.TrackProgress('Set TempCoName = ' + .cTempCoName,LOGIT+SENDIT)
				CASE tcTempCo == 'X'
					.cTempCo = tcTempCo
					.cTempCoName = 'Chartwell Staffing Solutions - San Pedro'
					.TrackProgress('Set TempCo = ' + tcTempCo,LOGIT+SENDIT)
					.TrackProgress('Set TempCoName = ' + .cTempCoName,LOGIT+SENDIT)
				CASE tcTempCo == 'Y'
					.cTempCo = tcTempCo
					.cTempCoName = 'Simplified Labor Staffing Solutions - Mira Loma'
					.TrackProgress('Set TempCo = ' + tcTempCo,LOGIT+SENDIT)
					.TrackProgress('Set TempCoName = ' + .cTempCoName,LOGIT+SENDIT)
				OTHERWISE
					.TrackProgress('INVALID TempCo = ' + tcTempCo,LOGIT+SENDIT)
					.cTempCoName = ''
			ENDCASE
		ENDWITH
	ENDPROC
	

ENDDEFINE
