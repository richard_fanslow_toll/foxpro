*!* Multi-field output (see extract_bills.prg for only MBOL/HBOL)
CLOSE DATABASES ALL
DO m:\dev\prg\_setvars

lTesting = .F. && Default = .f.
lTestmail = .f.

CD F:\EFREIGHT\AMSXML

lEmail = .f.
thistime = TTOC(DATETIME(),1)
cOutFile = ("f:\efreight\temp\efreight"+thistime+".xls")
cArchiveExcel = ("f:\efreight\amsxml\excel\efreight"+thistime+".xls")

USE F:\EFREIGHT\AMSXML\Sent\FILESPROC IN 0 ALIAS FILESPROC
CREATE CURSOR data_out (mbol c(30),hbol c(30),consignee c(40),carr_scac c(10),vessel c(30),;
	voyage c(10),onboard c(15),groupcode c(20),filename c(50),mlinenum i,hlinenum i)
SELECT 0
ADIR(ARY1,"*.XML")
LEN1 = ALEN(ARY1,1)

FOR i = 1 TO LEN1
	CREATE CURSOR TEMP1 (FIELD1 c(120))
	STORE "" TO mbol,hbol,voyage,vessel,onboard,carr_scac,groupcode
	cFilename = ALLTRIM(ARY1[i,1])
	cArchiveXML = ("f:\efreight\amsxml\sent\"+cFilename)
	STORE cFilename TO m.filename
	cFiledate = DTOC(ARY1[i,3])
	WAIT WINDOW "Processing file: "+cFilename NOWAIT
	SELECT FILESPROC
	LOCATE FOR FILESPROC.filename = PADR(cFilename,50)
	IF !FOUND()
		APPEND BLANK
		REPLACE FILESPROC.filename WITH cFilename,FILESPROC.processed WITH .T.
	ENDIF

	SELECT TEMP1
	APPEND FROM &cFilename TYPE DELIMITED
	REPLACE ALL FIELD1 WITH ALLTRIM(FIELD1)
	LOCATE
	IF RECCOUNT()<3
		WAIT WINDOW "Empty file: "+cFilename NOWAIT
		LOOP
	ELSE
*		? cFilename+"  "+cFiledate
	ENDIF

*	BROWSE
	SCAN
		IF FIELD1 = "<voyage_no>"
			m.voyage = STRTRAN(STRTRAN(FIELD1,"<voyage_no>",""),"</voyage_no>","")
		ENDIF

		IF FIELD1 = "<vessel_name>"
			m.vessel = STRTRAN(STRTRAN(FIELD1,"<vessel_name>",""),"</vessel_name>","")
		ENDIF

		IF FIELD1 = "<on_board_date>"
			m.onboard = STRTRAN(STRTRAN(FIELD1,"<on_board_date>",""),"</on_board_date>","")
			m.onboard = LEFT(ALLTRIM(m.onboard),10)
		ENDIF

		IF FIELD1 = "<carrier_scac_code>"
			m.carr_scac = STRTRAN(STRTRAN(FIELD1,"<carrier_scac_code>",""),"</carrier_scac_code>","")
		ENDIF

		IF FIELD1 = "<master_bl_no>"
			m.mbol = STRTRAN(STRTRAN(FIELD1,"<master_bl_no>",""),"</master_bl_no>","")
			m.mlinenum = RECNO()
		ENDIF
		IF FIELD1 = "<hbl_no>"
			m.hbol = STRTRAN(STRTRAN(FIELD1,"<hbl_no>",""),"</hbl_no>","")
			m.hlinenum = RECNO()
		ENDIF

		IF FIELD1 = "<company_name>"
			m.consignee = STRTRAN(STRTRAN(FIELD1,"<company_name>",""),"</company_name>","")
			m.consignee = STRTRAN(m.consignee,"&amp;","&")
		ENDIF

		IF LEFT(FIELD1,11) = "<group_code"
			IF FIELD1 = '<group_code xml:space="preserve">'
				m.groupcode = "UNKNOWN"
			ELSE
				m.groupcode = STRTRAN(STRTRAN(FIELD1,"<group_code>",""),"</group_code>","")
			ENDIF
			IF !EMPTY(m.mbol)
				INSERT INTO data_out FROM MEMVAR
			ENDIF
			STORE "" TO mbol,consignee,hbol,voyage,vessel,onboard,carr_scac,groupcode
		ENDIF
	ENDSCAN
	COPY FILE &cFilename TO &cArchiveXML
	DELETE FILE &cFilename
ENDFOR

SELECT data_out
LOCATE
SELECT mbol,consignee,hbol,carr_scac,vessel,voyage,onboard,groupcode ;
	FROM data_out ;
	INTO CURSOR TEMP1
IF lTesting
	BROWSE
ENDIF
WAIT WINDOW "Process complete...Now mailing results file" TIMEOUT 2


COPY TO &cOutFile TYPE XL5
IF !lTesting
	COPY TO &cArchiveExcel TYPE XL5
ENDIF

tattach = cOutFile
tsubject = "EFreight Voyage Sheet "+DTOC(DATE())
tmessage = "Attached is the Excel Voyage Sheet"
IF lTestmail OR lTesting
	tmessage = tmessage+CHR(13)+CHR(13)+SPACE(10)+"*** THIS IS A TEST MAILING ***"
	tmessage = tmessage+CHR(13)+CHR(13)+"CHECK FOR CONTENT AND LAYOUT"
ENDIF
tFrom ="TGF WMS Operations <tgf-transload-ops@fmiint.com>"
IF lTestmail
	USE F:\3pl\DATA\mailmaster ALIAS mm SHARED
	LOCATE FOR mm.office = "X" AND mm.accountid = 9999
	lUseAlt = mm.use_alt
	tsendto = IIF(lUseAlt,mm.sendtoalt,mm.sendto)
	tcc = IIF(lUseAlt,mm.ccalt,mm.cc)
	USE IN mm
ELSE
	tsendto = "wtakenouchi@summitgl.com,gpedersen@summitgl.com"
	tcc = "pgaidis@fmiint.com"
ENDIF

IF lEmail
DO FORM m:\dev\frm\dartmail2 WITH tsendto,tFrom,tsubject,tcc,tattach,tmessage,"A"
endif

DELETE FILE &cOutFile

