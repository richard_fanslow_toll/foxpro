* report on TWIC epration dates that are within 4 months, or are blank.
* for NJ & CA Local Drivers and OwnerOp Drivers 
* programmed by MB
* Build EXE as F:\UTIL\HR\TWICSTATUS\TWICSTATUSRPT.EXE

LOCAL lTestMode
lTestMode = .F.

IF NOT lTestMode THEN
	utilsetup("TWICSTATUSRPT")
ENDIF

DECLARE INTEGER CreateMutex IN Win32API ;
	STRING @lpMutexAttributes, ;
	STRING bInitialOwner, ;
	STRING @lpName

DECLARE INTEGER GetLastError IN Win32API

* if running in EXE check to see if already running
IF _VFP.STARTMODE > 0 THEN
	lcProcessName = "TWICSTATUSRPT"
	CreateMutex(0,CHR(1),@lcProcessName)
	lnError = GetLastError()
	IF lnError = 183
		WAIT WINDOW TIMEOUT 5 "This program is already running..."
		IF NOT lTestMode THEN
			SCHEDUPDATE()
		ENDIF
		RETURN .F.
	ENDIF
ENDIF

loTWICSTATUSRPT = CREATEOBJECT('TWICSTATUSRPT')
loTWICSTATUSRPT.MAIN( lTestMode )

IF NOT lTestMode THEN
	SCHEDUPDATE()
ENDIF

CLOSE DATABASES ALL
RETURN

#DEFINE LOGIT 1
#DEFINE WAITIT 2
#DEFINE NOWAITIT 4
#DEFINE SENDIT 8
#DEFINE CRLF CHR(13) + CHR(10)
#DEFINE RETURN_DATA_MANDATORY .F.
#DEFINE RETURN_DATA_NOT_MANDATORY .T.
#DEFINE CSVFORMAT 6
#DEFINE WRITE_ONLY_UNBUFFERED 11
#DEFINE COMMA ","

DEFINE CLASS TWICSTATUSRPT AS CUSTOM

	cProcessName = 'TWICSTATUSRPT'

	lTestMode = .F.  && .F. FOR NORMAL OPERATION, .T. TO SUPPRESS ALL BUT DEVELOPER EMAILS AND CHANGE INPUT TABLE, ETC.

	dToday = DATE()

	lAutoYield = .T.

	cStartTime = TTOC(DATETIME())

	* connection properties
	nSQLHandle = 0

	* folder properties

	* data properties
	
	* processing properties

	* Excel properties
	oExcel = NULL

	* wait window properties
	nWaitWindowTimeout = 2
	lWaitWindowIsOn = .T.

	* logfile properties
	lLoggingIsOn = .T.
	cLogFile = 'F:\UTIL\HR\TWICSTATUS\LOGFILES\TWICSTATUSRPT_log.txt'

	* INTERNAL email properties
	lSendInternalEmailIsOn = .T.
	cFrom = 'fmicorporate@fmiint.com'
	cSendTo = 'Mike.Drew@Tollgroup.com, Diana.Landeros@Tollgroup.com'
	cCC = 'mbennett@fmiint.com'
	cSubject = 'TWIC Expiration Status Report for ' + TRANSFORM(DATETIME())
	cAttach = ''
	cBodyText = ''
	cMGRSendTo = ''
	cMGRFrom = ''
	cMGRSubject = ''
	cMGRCC = ''
	cMGRAttach = ''
	cMGRBodyText = ''

	cCOMPUTERNAME = ''
	cUSERNAME = ''


	FUNCTION INIT
		IF NOT DODEFAULT()
			RETURN .F.
		ENDIF
		WITH THIS
			*SET RESOURCE OFF
			CLOSE DATA
			SET CENTURY ON
			*SET DATE AMERICAN
			*SET HOURS TO 24
			SET DECIMALS TO 0
			SET ANSI ON
			SET TALK OFF
			SET DELETED ON
			SET CONSOLE OFF
			SET EXCLUSIVE OFF
			SET SAFETY OFF
			SET EXACT OFF
			SET STATUS BAR ON
			SET SYSMENU OFF
			SET ENGINEBEHAVIOR 70
			SET MULTILOCKS ON
			_VFP.AUTOYIELD = .lAutoYield
			.cCOMPUTERNAME = GETENV("COMPUTERNAME")
			.cUSERNAME = GETENV("USERNAME")
			.lLoggingIsOn = .lLoggingIsOn AND NOT EMPTY(.cLogFile)
		ENDWITH
	ENDFUNC


	FUNCTION DESTROY
		WITH THIS
			IF .lLoggingIsOn  THEN
				SET ALTERNATE OFF
				SET ALTERNATE TO
			ENDIF
		ENDWITH
		IF TYPE('.oExcel') = 'O' AND NOT ISNULL(.oExcel) THEN
			.oExcel.QUIT()
		ENDIF
		DODEFAULT()
	ENDFUNC


	FUNCTION MAIN
		LPARAMETERS tlTestMode
		WITH THIS
			LOCAL lnNumberOfErrors, ldToday, ld4MonthsFromToday, lcSpreadSheetFile

			TRY
				lnNumberOfErrors = 0

				.lTestMode = tlTestMode

				IF .lTestMode THEN
					.cLogFile = 'F:\UTIL\HR\TWICSTATUS\LOGFILES\TWICSTATUSRPT_log_TESTMODE.txt'
					.cSendTo = 'mbennett@fmiint.com'
				ENDIF
				IF .lLoggingIsOn THEN
					SET ALTERNATE TO (.cLogFile) ADDITIVE
					SET ALTERNATE ON
				ENDIF

				.cStartTime = TTOC(DATETIME())

				.TrackProgress('TWIC Expiration Status Report process started....', LOGIT+SENDIT)
				.TrackProgress('COMPUTERNAME = ' + .cCOMPUTERNAME, LOGIT+SENDIT)
				.TrackProgress('USERNAME = ' + .cUSERNAME, LOGIT+SENDIT)
				.TrackProgress('PROJECT = TWICSTATUSRPT', LOGIT+SENDIT)
				IF .lTestMode THEN
					.TrackProgress('                        TEST MODE                        ', LOGIT+SENDIT)
				ENDIF
				
				IF USED('CURNJ') THEN
					USE IN CURNJ
				ENDIF
				IF USED('CURCA') THEN
					USE IN CURCA
				ENDIF
				IF USED('CUROO') THEN
					USE IN CUROO
				ENDIF
				
				ldToday = .dToday
				ld4MonthsFromToday = GOMONTH(ldToday,4)
				
				* NJ Local Drivers
				SELECT PADR("NJ LOCAL DRIVER",20) AS TYPE, FIRST, LAST, TWICEXPIRE ;
					FROM F:\HR\HRDATA\EMPLOYEE ;
					INTO CURSOR CURNJ ;
					WHERE ACTIVE ;
					AND DIVISION = 3 AND DEPT = 650 ;
					AND ( EMPTYnul(TWICEXPIRE) OR  ( NOT EMPTYnul(TWICEXPIRE) AND (TWICEXPIRE < ld4MonthsFromToday) ) ) ;
					ORDER BY LAST, FIRST ;
					READWRITE				
				
				* CA Local Drivers
				SELECT PADR("CA LOCAL DRIVER",20) AS TYPE, FIRST, LAST, TWICEXPIRE ;
					FROM F:\HR\HRDATA\EMPLOYEE ;
					INTO CURSOR CURCA ;
					WHERE ACTIVE ;
					AND DIVISION = 55 AND DEPT = 650 ;
					AND ( EMPTYnul(TWICEXPIRE) OR  ( NOT EMPTYnul(TWICEXPIRE) AND (TWICEXPIRE < ld4MonthsFromToday) ) ) ;
					ORDER BY LAST, FIRST ;
					READWRITE
									
				* OwnerOp Drivers
				xsqlexec("select first, last, twicexpire from odrivers where active=1","xtemp",,"oo")
				
				SELECT PADR("OWNER-OP DRIVER",20) AS TYPE, FIRST, LAST, TWICEXPIRE ;
					FROM xtemp ;
					INTO CURSOR CUROO ;
					WHERE (EMPTYNUL(TWICEXPIRE) OR (NOT EMPTYNUL(TWICEXPIRE) AND (TWICEXPIRE < ld4MonthsFromToday))) ;
					ORDER BY LAST, FIRST ;
					READWRITE
					
				* blank out NULL twicexpire dates in CUROO
				SELECT CUROO
				SCAN
					IF ISNULL(CUROO.TWICEXPIRE) THEN
						REPLACE CUROO.TWICEXPIRE WITH {} IN CUROO
					ENDIF
				ENDSCAN
										
				* COMBINE ALL CURSORS
				SELECT CURNJ
				APPEND FROM DBF('CURCA')
				APPEND FROM DBF('CUROO')
				
				lcSpreadSheetFile = "F:\UTIL\HR\TWICSTATUS\REPORTS\TWIC_STATUS_" + DTOS(ldToday) + ".XLS"
				
				SELECT CURNJ
				COPY TO (lcSpreadSheetFile) XL5
				
				IF FILE(lcSpreadSheetFile) THEN
					.cAttach = lcSpreadSheetFile
				ELSE
					.TrackProgress('!!!There was an error attaching file ' + lcSpreadSheetFile, LOGIT+SENDIT)
				ENDIF

				CLOSE DATABASES ALL				

				.TrackProgress('TWIC Expiration Status Report process ended normally.', LOGIT+SENDIT)
				
				.cBodyText = "Drivers in the attached spreadsheet have either blank TWIC dates or TWIC dates that expire within 4 months." + CRLF + CRLF + "<< processing log follows >>" + CRLF + CRLF + .cBodyText

			CATCH TO loError

				.TrackProgress('There was an error.',LOGIT+SENDIT)
				.TrackProgress('The Error # is: ' + TRANSFORM(loError.ERRORNO), LOGIT+SENDIT)
				.TrackProgress('The Error Message is: ' + TRANSFORM(loError.MESSAGE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred in Program: ' + TRANSFORM(loError.PROCEDURE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred at line #: ' + TRANSFORM(loError.LINENO), LOGIT+SENDIT)
				lnNumberOfErrors = lnNumberOfErrors + 1
				.lSendInternalEmailIsOn = .T.
				IF TYPE('.oExcel') = 'O' AND NOT ISNULL(.oExcel) THEN
					.oExcel.QUIT()
				ENDIF

			ENDTRY

			WAIT CLEAR
			***************** INTERNAL email results ******************************
			.TrackProgress('About to send status email.',LOGIT+SENDIT)
			.TrackProgress('The logfile is: ' + .cLogFile, LOGIT+SENDIT)
			.TrackProgress('TWIC Expiration Status Report process started: ' + .cStartTime, LOGIT+SENDIT)
			.TrackProgress('TWIC Expiration Status Report process finished: ' + TTOC(DATETIME()), LOGIT+SENDIT)

			IF .lSendInternalEmailIsOn THEN
				DO FORM dartmail2 WITH .cSendTo,.cFrom,.cSubject,.cCC,.cAttach,.cBodyText,"A"
				.TrackProgress('Sent status email.',LOGIT)
			ELSE
				.TrackProgress('Did not send status email: THIS.lSendInternalEmailIsOn = FALSE.',LOGIT+SENDIT)
			ENDIF

		ENDWITH
		RETURN
	ENDFUNC && main



*!*		FUNCTION ExecSQL
*!*			LPARAMETERS tcSQL, tcCursorName, tlNoDataReturnedIsOkay
*!*			LOCAL llRetval, lnResult
*!*			WITH THIS
*!*				* close target cursor if it's open
*!*				IF USED(tcCursorName)
*!*					USE IN (tcCursorName)
*!*				ENDIF
*!*				WAIT WINDOW "Fetching remote data; please wait..." NOWAIT
*!*				lnResult = SQLEXEC(.nSQLHandle, tcSQL, tcCursorName)
*!*				llRetval = ( lnResult > 0 )
*!*				IF llRetval THEN
*!*					* see if any data came back
*!*					IF NOT tlNoDataReturnedIsOkay THEN
*!*						IF (NOT USED(tcCursorName)) OR EOF(tcCursorName) THEN
*!*							llRetval = .F.
*!*							.TrackProgress('No data was returned by this query: ' +CRLF + tcSQL, LOGIT+SENDIT)
*!*							.cSendTo = 'mbennett@fmiint.com'
*!*						ENDIF
*!*					ENDIF
*!*				ELSE
*!*					.TrackProgress("Result Code [" + TRANSFORM(lnResult) + "] returned by this query: " + tcSQL, LOGIT+SENDIT)
*!*					.cSendTo = 'mbennett@fmiint.com'
*!*				ENDIF
*!*				WAIT CLEAR
*!*				RETURN llRetval
*!*			ENDWITH
*!*		ENDFUNC


	PROCEDURE TrackProgress
		* do any combination of Wait Window, writing to logfile, and adding to body of email,
		* based on nFlags parameter.
		LPARAMETERS tcExpression, tnFlags
		WITH THIS
			IF BITAND(tnFlags,LOGIT) = LOGIT THEN
				IF .lLoggingIsOn THEN
					?
					? .cProcessName + "  " + TTOC(DATETIME()) + ": " + tcExpression
				ENDIF
			ENDIF
			IF .lWaitWindowIsOn THEN
				IF BITAND(tnFlags,NOWAITIT) = NOWAITIT THEN
					WAIT WINDOW tcExpression NOWAIT
				ENDIF
				IF BITAND(tnFlags,WAITIT) = WAITIT THEN
					WAIT WINDOW tcExpression TIMEOUT .nWaitWindowTimeout
				ENDIF
			ENDIF
			IF BITAND(tnFlags,SENDIT) = SENDIT THEN
				IF .lSendInternalEmailIsOn THEN
					.cBodyText = .cBodyText + tcExpression + CRLF + CRLF
				ENDIF
			ENDIF
		ENDWITH
	ENDPROC  &&  TrackProgress

ENDDEFINE
