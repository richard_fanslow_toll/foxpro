* HRTGFSTAFFRPT8.PRG
* produce FTE report or do daily FTE export to Qlikview, depending on passed parameter.
* rewritten to base headcount on Kronos calendar month data
* instead of ADP pay date data.

**************************************************************************************************************************
**  new version 8/19/2013 which uses f:\util\adpreports\data\depttrans.dbf to look up TGF Dept Descriptions by dept code,
** instead of using the custom fields in the HR employee table.
** Per Ken's request (he gave me the translation table). 8/2013
** tcType = 'R' for this
**************************************************************************************************************************

**************************************************************************************************************************
**  new version 11/2013 which does a lot of extra processing to prevent the duplicate headcounts of tgf depts caused by
**  people switching depts during the month.
**************************************************************************************************************************

**************************************************************************************************************************
**  new version 2/2014 which uses Insperity/Timestar data instead of ADP/Kronos, for employees.
**************************************************************************************************************************

**************************************************************************************************************************
**  new version 8/2014 which adds a 4th tab replicating a H/K format for Ken.
**************************************************************************************************************************


* Build EXE as F:\UTIL\HR\TGFSTAFFRPT\HRTGFSTAFFRPT.exe

LPARAMETERS tcType
LOCAL loTGFStaffingReport, ldToday, i, lcType
PRIVATE oExcel, oWorkbook

runack("HRTGFSTAFFRPT")

utilsetup("HRTGFSTAFFRPT")

IF EMPTY( tcType ) THEN
	lcType = "Q"
ELSE
	lcType = UPPER(ALLTRIM( tcType ))
ENDIF

IF NOT INLIST( lcType,"Q","K","R") THEN
	WAIT WINDOW TIMEOUT 60 "Invalid Type parameter!!"
	RETURN
ENDIF

* this is needed for anything we do...
oExcel = CREATEOBJECT("excel.application")
oExcel.displayalerts = .F.
oExcel.VISIBLE = .F.
oWorkbook = .F.

loTGFStaffingReport = CREATEOBJECT('TGFStaffingReport')

DO CASE
	CASE lcType = "Q"
		* mode Q = do 2 daily exports - 1 tab each - to Qlikview
		* one is MTD current month; second is entire prior month (if we are in first week of a month)

		ldToday = DATE()
		*ldToday = {^2012-07-02}

		IF INLIST(DAY(ldToday),1,2,3,4,5,6,7) THEN
			* process prior month
			loTGFStaffingReport.MAIN('ALL', ldToday, lcType)
		ENDIF

		* always process current month (i.e. MTD)
		ldToday = GOMONTH(ldToday,1)
		loTGFStaffingReport.MAIN('ALL', ldToday, lcType)

	CASE lcType = "K"
		* mode K (for Ken Kausner) = run his FTE report - 3 tabs - against prior month
		ldToday = DATE()  && this will cause Main() to process prior month

		*!*	********************************************
		*!*	* ACTIVATE TO FORCE DIFFERENT MONTH
		*ldToday = {^2014-08-01}
		*!*	********************************************
		loTGFStaffingReport.MAIN('FF', ldToday, lcType)
		loTGFStaffingReport.MAIN('SCS', ldToday, lcType)
		loTGFStaffingReport.MAIN('ADMIN', ldToday, lcType)
		loTGFStaffingReport.MAIN('ALL', ldToday, lcType)

	CASE lcType = "R"
		ldToday = DATE()  && this will cause Main() to process prior month
		*!*	********************************************
		*!*	* ACTIVATE TO FORCE DIFFERENT MONTH
		ldToday = {^2014-02-01}
		*!*	********************************************
		loTGFStaffingReport.MAIN('ALL', ldToday, lcType)

	OTHERWISE
		* nothing
ENDCASE

schedupdate()

CLOSE DATABASES ALL
RETURN



* DON'T CHANGE THE ORDER OF THESE: 'FF' MUST BE FIRST, 'ALL' MUST BE LAST!
*loTGFStaffingReport.MAIN('FF', ldToday)
*loTGFStaffingReport.MAIN('SCS', ldToday)
*loTGFStaffingReport.MAIN('ALL', ldToday)


*!*	** activate below for multiple months...
*!*	ldToday = {^2010-01-01}
*!*	FOR i = 1 TO 30
*!*		ldToday = GOMONTH(ldToday,1)
*!*		loTGFStaffingReport.MAIN('ALL', ldToday)
*!*	NEXT



#DEFINE LOGIT 1
#DEFINE WAITIT 2
#DEFINE NOWAITIT 4
#DEFINE SENDIT 8
#DEFINE CRLF CHR(13) + CHR(10)
#DEFINE RETURN_DATA_MANDATORY .F.
#DEFINE RETURN_DATA_NOT_MANDATORY .T.
#DEFINE CSVFORMAT 6
#DEFINE WRITE_ONLY_UNBUFFERED 11
#DEFINE COMMA ","
#DEFINE xlEdgeBottom 9
#DEFINE xlContinuous 1
#DEFINE xlMedium -4138
#DEFINE xlFONT_RED -16776961

DEFINE CLASS TGFStaffingReport AS CUSTOM

	cProcessName = 'TGFStaffingReport'

	lTestMode = .T.  && .F. FOR NORMAL OPERATION, .T. TO SUPPRESS ALL BUT DEVELOPER EMAILS AND CHANGE INPUT TABLE, ETC.

	lDoBIExport = .T.

	dToday = DATE()

	lAutoYield = .T.

	cStartTime = TTOC(DATETIME())

	* report properties
	cBadData = ''
	*nFTEHours_Hourly = 0
	*nFTEHours_Salaried = 0

	*nFTEHours = 173.33
	nFTEHours = 160.00  && Per Ken Kausner 3/4/13 MB

	nMilesToHoursFactor = 0.0167  && 1 / 60
	nOOMileage = 0.0
	nOOHeadCnt = 0
	nTempMarkUpFactor = 1.275

	* FILENAME PROPERTIES
	cDetailFileForKen = ''

	* table properties
	cDeptlookupTable = 'F:\UTIL\ADPREPORTS\DATA\DEPTTRANS.DBF'

	*** usually set these to .F.
	lShowHours = .F.
	lSpecialAllWhere = .F.

	* connection properties
	nSQLHandle = 0

	* wait window properties
	nWaitWindowTimeout = 2
	lWaitWindowIsOn = .T.

	* logfile properties
	lLoggingIsOn = .T.
	cLogFile = 'F:\UTIL\HR\TGFSTAFFRPT\LOGFILES\TGFSTAFF_Report_log.txt'

	* INTERNAL email properties
	lSendInternalEmailIsOn = .T.
	cFrom = 'Mark.Bennett@Tollgroup.com'
	cSendTo = 'mbennett@fmiint.com'
	cCC = ''
	cSubject = 'FTE Info to Qlikview for ' + DTOC(DATE())
	cAttach = ''
	cBodyText = ''

	cCOMPUTERNAME = ''
	cUSERNAME = ''


	FUNCTION INIT
		IF NOT DODEFAULT()
			RETURN .F.
		ENDIF
		WITH THIS
			*SET RESOURCE OFF
			CLOSE DATA
			SET CENTURY ON
			SET DATE AMERICAN
			SET HOURS TO 24
			SET ANSI ON
			SET TALK OFF
			SET DELETED ON
			SET CONSOLE OFF
			SET EXCLUSIVE OFF
			SET SAFETY OFF
			SET EXACT OFF
			SET STATUS BAR ON
			SET SYSMENU OFF
			SET ENGINEBEHAVIOR 70
			_VFP.AUTOYIELD = .lAutoYield
			.cCOMPUTERNAME = GETENV("COMPUTERNAME")
			.cUSERNAME = GETENV("USERNAME")
			IF .lTestMode THEN
				.cLogFile = 'F:\UTIL\HR\TGFSTAFFRPT\LOGFILES\TGFSTAFF_Report_log_TESTMODE.txt'
				.cSendTo = 'mbennett@fmiint.com'
			ENDIF
			.lLoggingIsOn = .lLoggingIsOn AND NOT EMPTY(.cLogFile)
			IF .lLoggingIsOn THEN
				SET ALTERNATE TO (.cLogFile) ADDITIVE
				SET ALTERNATE ON
			ENDIF
			SET PROCEDURE TO VALIDATIONS ADDITIVE
		ENDWITH
	ENDFUNC


	FUNCTION DESTROY
		WITH THIS
			IF .lLoggingIsOn  THEN
				SET ALTERNATE OFF
				SET ALTERNATE TO
			ENDIF
		ENDWITH
		DODEFAULT()
	ENDFUNC


	FUNCTION MAIN
		LPARAMETERS tcMode, tdToday, tcType
		WITH THIS
			LOCAL lnNumberOfErrors, lcSQL, lcSQL2, lcSQL3, lcSQL4, lcSQL5, lcSQL6, loError, ldToday, lnFTEHours
			LOCAL lcFiletoSaveAs, lcSpreadsheetTemplate, lcGrandTotalRow, lcWhere, lcDivWhere2
			LOCAL lnDay, ldFromDate, ldToDate, lcSQLFromDate, lcSQLToDate, lcADPFromDate, lcADPToDate
			LOCAL lcSQLKronos, lcSuffix, lcDivWhere, lcDateSuffix
			LOCAL i, oWorksheet, lnStartRow, lcStartRow, lnRow, lcRow, lnTempMarkUpFactor
			LOCAL lnMgmtStartRow, lcMgmtStartRow, lnMgmtEndRow, lcMgmtEndRow, lnPreCasualRow, lcKronosDivWhere
			LOCAL lnNumHourlyPaydates, lnNumSalariedPaydates, lcBIFileName, lcSQLKronosHourly, lcSQLKronosTMP
			LOCAL lcFileName, lcDept, lcHCFTEEntityDetailFile, lcHCFTEEntitySummaryFile, lcHCFTEEntityMultDeptsFile, lcHeadCountBasis
			LOCAL lcSQLCNTHourly, lcSQLCNTSalaried, lcBusUnitWhere, lnMonth, lnYear1, lnYear2, lcFiscal

			TRY
				lnNumberOfErrors = 0

				IF NOT USED('DEPTLOOKUPTABLE') THEN
					USE (.cDeptlookupTable) IN 0 ALIAS DEPTLOOKUPTABLE
				ENDIF

				IF NOT USED('TIMEDATA') THEN
					USE F:\UTIL\TIMESTAR\DATA\TIMEDATA IN 0 ALIAS TIMEDATA
					* delete bogus employees
					SELECT TIMEDATA
					SCAN
						IF ALLTRIM(FILE_NUM) == '1' THEN
							DELETE
						ENDIF
						IF ALLTRIM(FILE_NUM) == '2' THEN
							DELETE
						ENDIF
					ENDSCAN
				ENDIF

				IF NOT USED('EEINFO') THEN
					USE F:\UTIL\TIMESTAR\DATA\EEINFO IN 0 ALIAS EEINFO
				ENDIF

				lnFTEHours = .nFTEHours

				lnTempMarkUpFactor = .nTempMarkUpFactor

				.cBodyText = ''
				.cBadData = ''

				.dToday = tdToday
				ldToday = tdToday
				lnDay = DAY(tdToday)

				.TrackProgress("FTE Info to Qlikview process started....", LOGIT+SENDIT+NOWAITIT)
				.TrackProgress('COMPUTERNAME = ' + .cCOMPUTERNAME, LOGIT+SENDIT)
				.TrackProgress('USERNAME = ' + .cUSERNAME, LOGIT+SENDIT)
				.TrackProgress('tcMode = ' + tcMode, LOGIT+SENDIT)
				.TrackProgress('tcType = ' + tcType, LOGIT+SENDIT)
				.TrackProgress('tdToday = ' + TRANSFORM(tdToday), LOGIT+SENDIT)
				.TrackProgress('PROJECT = HRTGFSTAFFRPT', LOGIT+SENDIT)
				IF .lTestMode THEN
					.TrackProgress('                        TEST MODE                        ', LOGIT+SENDIT)
				ENDIF

				DO CASE
					CASE tcType = "Q"
						lDoBIExport = .T.
					CASE tcType = "K"
						lDoBIExport = .F.
					CASE tcType = "R"
						lDoBIExport = .F.
					OTHERWISE
						* nothing
				ENDCASE

				lnStartRow = 3
				lcStartRow = ALLTRIM(STR(lnStartRow))

				DO CASE
					CASE tcMode = "FF"
						lcDivWhere  = "AND {fn LEFT(CHECKVIEWHOMEDEPT,2)} IN ('20','21','22','23','24','25','26','27','28','29','30','31','32','35','36','37','38','39','71','72','73') "
						lcDivWhere2 = "AND {fn LEFT(A.CHECKVIEWHOMEDEPT,2)} IN ('20','21','22','23','24','25','26','27','28','29','30','31','32','35','36','37','38','39','71','72','73') "
						lcKronosDivWhere =            "AND D.LABORLEV2NM IN ('20','21','22','23','24','25','26','27','28','29','30','31','32','35','36','37','38','39','71','72','73') "
						lcSuffix = "_FF"
						lcBusUnitWhere = "AND BUSUNIT = 'FF '"
					CASE tcMode = "SCS"
						lcDivWhere  = "AND {fn LEFT(CHECKVIEWHOMEDEPT,2)} IN ('01','02','03','04','05','06','07','11','14','15','49','50','51','52','53','54','55','56','57','58','59','60','61','62') "
						lcDivWhere2 = "AND {fn LEFT(A.CHECKVIEWHOMEDEPT,2)} IN ('01','02','03','04','05','06','07','11','14','15','49','50','51','52','53','54','55','56','57','58','59','60','61','62') "
						lcKronosDivWhere =            "AND D.LABORLEV2NM IN ('01','02','03','04','05','06','07','11','14','15','49','50','51','52','53','54','55','56','57','58','59','60','61','62') "
						lcSuffix = "_NON_FF"
						lcBusUnitWhere = "AND BUSUNIT = 'SCS'"
					CASE tcMode = "ALL"
						lcDivWhere = ""
						lcDivWhere2 = ""
						lcKronosDivWhere = ""
						*!*							IF .lSpecialAllWhere THEN
						*!*								lcDivWhere = "AND {fn LEFT(CHECKVIEWHOMEDEPT,2)} IN ('21','36','73') "
						*!*								lcKronosDivWhere = "AND D.LABORLEV2NM IN ('21','36','73') "
						*!*							ENDIF
						lcSuffix = "_ALL"
						lcBusUnitWhere = ""
					CASE tcMode = "ADMIN"
						lcDivWhere  = "AND {fn LEFT(CHECKVIEWHOMEDEPT,2)} IN ('85','90','91','92') "
						lcDivWhere2 = "AND {fn LEFT(A.CHECKVIEWHOMEDEPT,2)} IN ('85','90','91','92') "
						lcKronosDivWhere =            "AND D.LABORLEV2NM IN ('85','90','91','92') "
						lcSuffix = "_ADMIN"
						lcBusUnitWhere = "AND BUSUNIT = 'ADM'"
					OTHERWISE
						THROW
				ENDCASE

				.TrackProgress('lcSuffix = ' + lcSuffix, LOGIT+SENDIT)
				.TrackProgress('lcDivWhere = ' + lcDivWhere, LOGIT+SENDIT)

				ldToDate = ldToday - lnDay  && this is last day of prior month
				ldFromDate = ldToDate + 1  && this is 1st day of current month
				ldFromDate = GOMONTH(ldFromDate,-1)  && this is 1st day of prior month

				lcDateSuffix = "_" + DTOS(ldFromDate) + "-" + DTOS(ldToDate)
				lcSuffix = lcSuffix + lcDateSuffix
				.cSubject = 'FTE Info to Qlikview for ' + DTOC(ldFromDate) + " - " + DTOC(ldToDate)


				lcADPFromDate = "DATE'" + TRANSFORM(YEAR(ldFromDate)) + "-" + PADL(MONTH(ldFromDate),2,'0') + "-" + PADL(DAY(ldFromDate),2,'0') + "'"
				lcADPToDate = "DATE'" + TRANSFORM(YEAR(ldToDate)) + "-" + PADL(MONTH(ldToDate),2,'0') + "-" + PADL(DAY(ldToDate),2,'0') + "'"

				lcSpreadsheetTemplate = 'F:\UTIL\HR\TGFSTAFFRPT\TEMPLATES\TGFSTAFFING_TEMPLATE5.XLS'
				lcFiletoSaveAs = 'F:\UTIL\HR\TGFSTAFFRPT\REPORTS\TGFSTAFFING' + lcDateSuffix + "_" + tcType + '.XLS'
				.TrackProgress('Creating spreadsheet: ' + lcFiletoSaveAs, LOGIT+SENDIT+NOWAITIT)


				* this block must only run once, when depends on mode...
				DO CASE
					CASE tcType = "Q"
						IF tcMode = "ALL" THEN
							* delete output file if it already exists...
							IF FILE(lcFiletoSaveAs) THEN
								DELETE FILE (lcFiletoSaveAs)
							ENDIF
							oWorkbook = oExcel.workbooks.OPEN(lcSpreadsheetTemplate)
							oWorkbook.SAVEAS(lcFiletoSaveAs)
						ENDIF
					CASE tcType = "K"
						.cSubject = 'FTE Staffing for ' + DTOC(ldFromDate) + " - " + DTOC(ldToDate)
						.cSendTo = 'Ken.Kausner@Tollgroup.com'
						.cCC = 'Mark.Bennett@Tollgroup.com'
						IF tcMode = "FF" THEN
							* delete output file if it already exists...
							IF FILE(lcFiletoSaveAs) THEN
								DELETE FILE (lcFiletoSaveAs)
							ENDIF
							oWorkbook = oExcel.workbooks.OPEN(lcSpreadsheetTemplate)
							oWorkbook.SAVEAS(lcFiletoSaveAs)
						ENDIF
					CASE tcType = "R"
						.cSubject = 'H/C & FTE by Entity Report for ' + DTOC(ldFromDate) + " - " + DTOC(ldToDate)
						.cSendTo = 'Ken.Kausner@Tollgroup.com'
						.cCC = 'Mark.Bennett@Tollgroup.com'
						* delete output file if it already exists...s
						IF FILE(lcFiletoSaveAs) THEN
							DELETE FILE (lcFiletoSaveAs)
						ENDIF
						oWorkbook = oExcel.workbooks.OPEN(lcSpreadsheetTemplate)
						oWorkbook.SAVEAS(lcFiletoSaveAs)
					OTHERWISE
						* nothing
				ENDCASE

				IF .lTestMode THEN
					.cSendTo = 'Mark.Bennett@Tollgroup.com'
					.cCC = ''
				ENDIF


				* determine current worksheet
				DO CASE
					CASE tcMode = "FF"
						oWorksheet = oWorkbook.Worksheets[1]
					CASE tcMode = "SCS"
						oWorksheet = oWorkbook.Worksheets[2]
					CASE tcMode = "ADMIN"
						oWorksheet = oWorkbook.Worksheets[3]
					CASE tcMode = "ALL"
						oWorksheet = oWorkbook.Worksheets[4]
				ENDCASE


				IF USED('CURHOURS') THEN
					USE IN CURHOURS
				ENDIF
				IF USED('CURHOURS2') THEN
					USE IN CURHOURS2
				ENDIF
				IF USED('CURHOURSPRE') THEN
					USE IN CURHOURSPRE
				ENDIF


				***************************************************

				* GATHER TEMP INFO FROM KRONOS

				.nSQLHandle = SQLCONNECT("WTK2","sa","kr0n1tes")

				IF .nSQLHandle > 0 THEN

					lcSQLFromDate = .GetSQLStartDateTimeString( ldFromDate )
					lcSQLToDate = .GetSQLEndDateTimeString( ldToDate )


					***** NOTE: we need to exclude 020655 from below, because they have no actual worked hours, so only Vac/sick etc.d will
					***** show up, understating their hours and fte. Then we must add them to the Salaried query later on (basically treating them as salaried).

					SET TEXTMERGE ON
					TEXT TO	lcSQLKronosHourly NOSHOW
SELECT
A.APPLYDTM,
D.LABORLEV5NM AS AGENCYNUM,
D.LABORLEV1NM AS ADP_COMP,
D.LABORLEV2NM AS DIVISION,
D.LABORLEV3NM AS DEPT,
C.FULLNM AS NAME,
'       ' AS FILE_NUM,
C.PERSONNUM,
E.NAME AS PAYCODEDESC,
(CASE WHEN E.NAME NOT IN ('Regular Hours','Overtime Hours' ,'Doubltime Hours') THEN (A.DURATIONSECSQTY / 3600.00) ELSE 0.000 END) AS CODEHOURS,
(CASE WHEN E.NAME = 'Regular Hours' THEN (A.DURATIONSECSQTY / 3600.00) ELSE 0.000 END) AS REGHOURS,
(CASE WHEN E.NAME IN ('Overtime Hours' ,'Doubltime Hours') THEN (A.DURATIONSECSQTY / 3600.00) ELSE 0.000 END) AS OTHOURS,
(A.DURATIONSECSQTY / 3600.00) AS TOTHOURS,
WAGEAMT
FROM WFCTOTAL A
JOIN WTKEMPLOYEE B
ON B.EMPLOYEEID = A.EMPLOYEEID
JOIN PERSON C
ON C.PERSONID = B.PERSONID
JOIN LABORACCT D
ON D.LABORACCTID = A.LABORACCTID
JOIN PAYCODE E
ON E.PAYCODEID = A.PAYCODEID
WHERE (A.APPLYDTM >= <<lcSQLFromDate>>)
AND (A.APPLYDTM <= <<lcSQLToDate>>)
AND (D.LABORLEV1NM IN ('TMP'))
AND E.NAME NOT IN ('FMLA','Unpaid Leave','Unpaid Hours','Unpaid Suspended','Voluntary Time Off')
AND C.PERSONNUM <> '999'
AND NOT ((D.LABORLEV2NM = '02') AND (D.LABORLEV3NM = '0655'))
<<lcKronosDivWhere>>
ORDER BY 1,2,3,4,6
					ENDTEXT
					SET TEXTMERGE OFF

					SET TEXTMERGE ON
					TEXT TO	lcSQLCNTHourly NOSHOW
SELECT
D.LABORLEV1NM AS ADP_COMP,
A.APPLYDTM AS PAYDATE,
C.FULLNM AS NAME,
C.PERSONNUM,
D.LABORLEV2NM AS DIVISION,
D.LABORLEV3NM AS DEPT,
'B' AS MGMTTYPE,
'TP' AS TGFDEPT
FROM WFCTOTAL A
JOIN WTKEMPLOYEE B
ON B.EMPLOYEEID = A.EMPLOYEEID
JOIN PERSON C
ON C.PERSONID = B.PERSONID
JOIN LABORACCT D
ON D.LABORACCTID = A.LABORACCTID
JOIN PAYCODE E
ON E.PAYCODEID = A.PAYCODEID
WHERE (A.APPLYDTM >= <<lcSQLFromDate>>)
AND (A.APPLYDTM <= <<lcSQLToDate>>)
AND (D.LABORLEV1NM IN ('TMP'))
AND E.NAME NOT IN ('FMLA','Unpaid Leave','Unpaid Hours','Unpaid Suspended','Voluntary Time Off')
AND C.PERSONNUM <> '999'
AND NOT ((D.LABORLEV2NM = '02') AND (D.LABORLEV3NM = '0655'))
<<lcKronosDivWhere>>
ORDER BY 1,2
					ENDTEXT
					SET TEXTMERGE OFF

					IF .lTestMode THEN
						.TrackProgress('lcSQLKronosHourly = ' + lcSQLKronosHourly, LOGIT+SENDIT)
					ENDIF

					IF USED('CURHOURLYHEADCNTPRE') THEN
						USE IN CURHOURLYHEADCNTPRE
					ENDIF
					IF USED('CURHOURLYHEADCNT2') THEN
						USE IN CURHOURLYHEADCNT2
					ENDIF

					IF .ExecSQL(lcSQLKronosHourly, 'CURHOURSPRE', RETURN_DATA_MANDATORY) AND ;
							.ExecSQL(lcSQLCNTHourly, 'CURHOURLYHEADCNTPRE', RETURN_DATA_MANDATORY) THEN
						* okay

					ELSE
						* we're in a business unit, like FF, that has no one in kronos, so we don't have CURHOURSPRE or CURHOURLYHEADCNTPRE to work with.
						* so create them as cursors...

						CREATE CURSOR CURHOURSPRE ( APPLYDTM T, AGENCYNUM C(1), ADP_COMP C(3), DIVISION C(2), DEPT C(4), ;
							NAME C(20), FILE_NUM C(7), PERSONNUM C(4), PAYCODEDESC C(20), CODEHOURS N(6,3), REGHOURS N(6,3), OTHOURS N(6,3), TOTHOURS N(6,3), WAGEAMT N(7,2) )

						*!*	A.APPLYDTM,
						*!*	D.LABORLEV5NM AS AGENCYNUM,
						*!*	D.LABORLEV1NM AS ADP_COMP,
						*!*	D.LABORLEV2NM AS DIVISION,
						*!*	D.LABORLEV3NM AS DEPT,
						*!*	C.FULLNM AS NAME,
						*!*	'       ' AS FILE_NUM,
						*!*	C.PERSONNUM,
						*!*	E.NAME AS PAYCODEDESC,
						*!*	0.000 as CODEHOURS,
						*!*	 AS REGHOURS,
						*!*	 AS OTHOURS,
						*!*	AS TOTHOURS,
						*!*	WAGEAMT

						CREATE CURSOR CURHOURLYHEADCNTPRE( ADP_COMP C(3), PAYDATE D, NAME C(20), PERSONNUM C(4), DIVISION C(2), DEPT C(4), MGMTTYPE C(1), TGFDEPT C(2) )

						*!*	D.LABORLEV1NM AS ADP_COMP,
						*!*	A.APPLYDTM AS PAYDATE,
						*!*	C.FULLNM AS NAME,
						*!*	C.PERSONNUM,
						*!*	D.LABORLEV2NM AS DIVISION,
						*!*	D.LABORLEV3NM AS DEPT,
						*!*	'B' AS MGMTTYPE,
						*!*	'TP' AS TGFDEPT

					ENDIF  && .ExecSQL

					=SQLDISCONNECT(.nSQLHandle)

				ENDIF && .nSQLHandle > 0 THEN

				*!*	IF .lTestMode
				*!*		SELECT CURHOURLYHEADCNTPRE
				*!*		BROWSE
				*!*	ENDIF

*!*					IF ldFromDate > {^2013-06-30} THEN
					* WE WANT TO DELETE ANY DIV 59 DATA AFTER 6/30/2013 - Because Underarmour took over Rialto on 7/1/2013 but kept using our clocks for a few weeks.
					DO CASE
						CASE INLIST(tcType,"K","Q","R")
*!*								SELECT CURHOURSPRE
*!*								SCAN
*!*									IF (DIVISION = '59') THEN
*!*										DELETE
*!*									ENDIF

*!*								ENDSCAN

							* ALSO COPY FILE_NUM INTO LARGER 7-CHAR FIELD TO MATCH INSPERITY IDS
							SELECT CURHOURSPRE
							SCAN
								REPLACE CURHOURSPRE.FILE_NUM WITH CURHOURSPRE.PERSONNUM IN CURHOURSPRE
							ENDSCAN


*!*								SELECT CURHOURLYHEADCNTPRE
*!*								SCAN
*!*									IF (DIVISION = '59') THEN
*!*										DELETE
*!*									ENDIF
*!*								ENDSCAN

						OTHERWISE
							* nothing
					ENDCASE
*!*					ENDIF  && ldFromDate > {^2013-06-30}

				***************************************************
				***************************************************
				***************************************************

				* GATHER HOURLY INFO FROM TIMESTAR and add it in
				SELECT ;
					WORKDATE AS APPLYDTM, ;
					'0' AS AGENCYNUM, ;
					ADP_COMP, ;
					DIVISION, ;
					DEPT, ;
					NAME, ;
					FILE_NUM, ;
					PAY_TYPE AS PAYCODEDESC, ;
					CODEHOURS, ;
					REGHOURS, ;
					OTHOURS, ;
					TOTHOURS, ;
					0.00 AS WAGEAMT ;
					FROM TIMEDATA ;
					INTO CURSOR CURTIMEDATAHOURLY ;
					WHERE (WORKDATE >= ldFromDate) ;
					AND (WORKDATE <= ldToDate) ;
					AND (ADP_COMP = 'E87') ;
					AND (NOT INLIST(PAY_TYPE,'FMLA','UNPAID','SUSPENSION')) ;
					AND NOT ((DIVISION = '02') AND (DEPT = '0655')) ;
					&lcBusUnitWhere ;
					ORDER BY 1,6,8

				*!*	SELECT CURTIMEDATAHOURLY
				*!*	BROWSE

				SELECT CURHOURSPRE
				APPEND FROM DBF('CURTIMEDATAHOURLY')

				USE IN CURTIMEDATAHOURLY

				*!*	SELECT CURHOURSPRE
				*!*	BROWSE
				*!*	THROW

				*!*	SELECT
				*!*	A.APPLYDTM,
				*!*	D.LABORLEV5NM AS AGENCYNUM,
				*!*	D.LABORLEV1NM AS ADP_COMP,
				*!*	D.LABORLEV2NM AS DIVISION,
				*!*	D.LABORLEV3NM AS DEPT,
				*!*	C.FULLNM AS NAME,
				*!*	C.PERSONNUM AS FILE_NUM,
				*!*	E.NAME AS PAYCODEDESC,
				*!*	(CASE WHEN E.NAME NOT IN ('Regular Hours','Overtime Hours' ,'Doubltime Hours') THEN (A.DURATIONSECSQTY / 3600.00) ELSE 0.000 END) AS CODEHOURS,
				*!*	(CASE WHEN E.NAME = 'Regular Hours' THEN (A.DURATIONSECSQTY / 3600.00) ELSE 0.000 END) AS REGHOURS,
				*!*	(CASE WHEN E.NAME IN ('Overtime Hours' ,'Doubltime Hours') THEN (A.DURATIONSECSQTY / 3600.00) ELSE 0.000 END) AS OTHOURS,
				*!*	(A.DURATIONSECSQTY / 3600.00) AS TOTHOURS,
				*!*	WAGEAMT
				*!*	FROM WFCTOTAL A
				*!*	JOIN WTKEMPLOYEE B
				*!*	ON B.EMPLOYEEID = A.EMPLOYEEID
				*!*	JOIN PERSON C
				*!*	ON C.PERSONID = B.PERSONID
				*!*	JOIN LABORACCT D
				*!*	ON D.LABORACCTID = A.LABORACCTID
				*!*	JOIN PAYCODE E
				*!*	ON E.PAYCODEID = A.PAYCODEID
				*!*	WHERE (A.APPLYDTM >= <<lcSQLFromDate>>)
				*!*	AND (A.APPLYDTM <= <<lcSQLToDate>>)
				*!*	AND (D.LABORLEV1NM IN ('TMP'))
				*!*	AND E.NAME NOT IN ('FMLA','Unpaid Leave','Unpaid Hours','Unpaid Suspended','Voluntary Time Off')
				*!*	AND C.PERSONNUM <> '999'
				*!*	AND NOT ((D.LABORLEV2NM = '02') AND (D.LABORLEV3NM = '0655'))
				*!*	<<lcKronosDivWhere>>
				*!*	ORDER BY 1,2,3,4,6


				***************************************************
				***************************************************
				***************************************************


				***************************************************
				***************************************************
				***************************************************
				IF USED('CURTIMESTARHEADCNT') THEN
					USE IN CURTIMESTARHEADCNT
				ENDIF

				* GATHER HOURLY count INFO FROM TIMESTAR and add it in
				SELECT ;
					ADP_COMP, ;
					WORKDATE AS PAYDATE, ;
					NAME, ;
					FILE_NUM AS PERSONNUM, ;
					DIVISION, ;
					DEPT, ;
					COLLAR AS MGMTTYPE, ;
					TOLLCLASS AS TGFDEPT ;
					FROM TIMEDATA ;
					INTO CURSOR CURTIMESTARHEADCNT ;
					WHERE (WORKDATE >= ldFromDate) ;
					AND (WORKDATE <= ldToDate) ;
					AND (ADP_COMP = 'E87') ;
					AND (NOT INLIST(PAY_TYPE,'FMLA','UNPAID','SUSPENSION')) ;
					AND NOT ((DIVISION = '02') AND (DEPT = '0655')) ;
					&lcBusUnitWhere ;
					ORDER BY 1,2

				*!*	SELECT
				*!*	D.LABORLEV1NM AS ADP_COMP,
				*!*	A.APPLYDTM AS PAYDATE,
				*!*	C.FULLNM AS NAME,
				*!*	C.PERSONNUM,
				*!*	D.LABORLEV2NM AS DIVISION,
				*!*	D.LABORLEV3NM AS DEPT,
				*!*	'B' AS MGMTTYPE,
				*!*	'TP' AS TGFDEPT
				*!*	FROM WFCTOTAL A
				*!*	JOIN WTKEMPLOYEE B
				*!*	ON B.EMPLOYEEID = A.EMPLOYEEID
				*!*	JOIN PERSON C
				*!*	ON C.PERSONID = B.PERSONID
				*!*	JOIN LABORACCT D
				*!*	ON D.LABORACCTID = A.LABORACCTID
				*!*	JOIN PAYCODE E
				*!*	ON E.PAYCODEID = A.PAYCODEID
				*!*	WHERE (A.APPLYDTM >= <<lcSQLFromDate>>)
				*!*	AND (A.APPLYDTM <= <<lcSQLToDate>>)
				*!*	AND (D.LABORLEV1NM IN ('TMP'))
				*!*	AND E.NAME NOT IN ('FMLA','Unpaid Leave','Unpaid Hours','Unpaid Suspended','Voluntary Time Off')
				*!*	AND C.PERSONNUM <> '999'
				*!*	AND NOT ((D.LABORLEV2NM = '02') AND (D.LABORLEV3NM = '0655'))
				*!*	<<lcKronosDivWhere>>
				*!*	ORDER BY 1,2

				*!*	SELECT CURTIMESTARHEADCNT
				*!*	BROWSE

				SELECT CURHOURLYHEADCNTPRE
				APPEND FROM DBF('CURTIMESTARHEADCNT')

				USE IN CURTIMESTARHEADCNT

				*!*	SELECT CURHOURLYHEADCNTPRE
				*!*	BROWSE
				*!*	THROW



				***************************************************
				***************************************************
				***************************************************

				* ROLL UP HOURS INFO
				SELECT ALLTRIM(ADP_COMP) AS ADP_COMP, ;
					NAME, ;
					INT(VAL(FILE_NUM)) AS FILE_NUM, ;
					ALLTRIM(DIVISION) + ALLTRIM(DEPT) AS HOMEDEPT, ;
					ldFromDate AS PAYDATE, ;
					SUM(CODEHOURS) AS CODEHOURS, ;
					SUM(REGHOURS) AS REGHOURS, ;
					SUM(OTHOURS) AS OTHOURS, ;
					SUM(TOTHOURS) AS TOTHOURS, ;
					SUM(WAGEAMT) AS WAGEAMT, ;
					0.00 AS FTE, ;
					1.00 AS HEADCNT, ;
					lnFTEHours AS EXPFTE, ;
					'U' AS GENDER, ;
					'U' AS MGMTTYPE, ;
					'U' AS TIMETYPE, ;
					'U  ' AS TGFDEPT, ;
					'                         ' AS DEPTDESC, ;
					'    ' AS GLACCTNO, ;
					00000000.00 AS ANNSALARY ;
					FROM CURHOURSPRE ;
					INTO CURSOR CURHOURS ;
					GROUP BY 1, 2, 3, 4 ;
					ORDER BY 1, 2, 3, 4 ;
					READWRITE

				* CONVERT KRONOS ADP_COMPS TO ADP ADP_COMPS
				SELECT CURHOURS
				SCAN
					IF NOT INLIST(CURHOURS.ADP_COMP,'E87','E88','E89') THEN
						REPLACE CURHOURS.ADP_COMP WITH GetNewADPCompany( ADP_COMP ) IN CURHOURS
					ENDIF
				ENDSCAN

				* POPULATE MISSING TEMP INFO
				SELECT CURHOURS
				SCAN FOR ADP_COMP = 'TMP'

					REPLACE CURHOURS.GENDER WITH 'U', ;
						CURHOURS.TIMETYPE WITH 'C' ;
						IN CURHOURS

				ENDSCAN


				IF USED('CURHEADCOUNT') THEN
					USE IN CURHEADCOUNT
				ENDIF
				IF USED('CURCUSTOM') THEN
					USE IN CURCUSTOM
				ENDIF
				IF USED('CURDIVLIST') THEN
					USE IN CURDIVLIST
				ENDIF
				IF USED('CURSALPRE') THEN
					USE IN CURSALPRE
				ENDIF
				IF USED('CURSALPRE2') THEN
					USE IN CURSALPRE2
				ENDIF
				IF USED('CURPAYDATESPRE') THEN
					USE IN CURPAYDATESPRE
				ENDIF
				IF USED('CURPAYDATES') THEN
					USE IN CURPAYDATES
				ENDIF
				IF USED('CURWHITE') THEN
					USE IN CURWHITE
				ENDIF
				IF USED('CURBLUE') THEN
					USE IN CURBLUE
				ENDIF
				IF USED('CURCNTTYPE') THEN
					USE IN CURCNTTYPE
				ENDIF
				IF USED('CURCNTGENDER') THEN
					USE IN CURCNTGENDER
				ENDIF
				IF USED('CURTEMPSPRE') THEN
					USE IN CURTEMPSPRE
				ENDIF
				IF USED('CURTEMPS') THEN
					USE IN CURTEMPS
				ENDIF
				IF USED('CURSALARIEDHEADCNT') THEN
					USE IN CURSALARIEDHEADCNT
				ENDIF
				IF USED('CURSALARIEDHEADCNT2') THEN
					USE IN CURSALARIEDHEADCNT2
				ENDIF

				*!*						IF .ExecSQL(lcSQL, 'CURCUSTOM', RETURN_DATA_MANDATORY) AND ;
				*!*								.ExecSQL(lcSQLCNTSalaried, 'CURSALARIEDHEADCNT', RETURN_DATA_MANDATORY) AND ;
				*!*								.ExecSQL(lcSQL4, 'CURSALPRE', RETURN_DATA_MANDATORY) THEN

				*!*								.ExecSQL(lcSQL2, 'CURHOURSPRE', RETURN_DATA_MANDATORY) AND ;


				*!*	SELECT CURHOURS
				*!*	BROW

				* get CURSALPRE
				SELECT ;
					ADP_COMP, ;
					INSPID AS FILE_NUM, ;
					NAME, ;
					(DIVISION + DEPT) AS HOMEDEPT, ;
					1.00 AS HEADCNT, ;
					0.00 AS CODEHOURS, ;
					160.00 AS REGHOURS, ;
					0.00 AS OTHOURS, ;
					160.00 AS TOTHOURS, ;
					GENDER, ;
					COLLAR AS MGMTTYPE, ;
					'F' AS TIMETYPE, ;
					TOLLCLASS AS TGFDEPT ;
					FROM EEINFO ;
					INTO CURSOR CURSALPRE ;
					WHERE ((ADP_COMP <> 'E87') OR ((DIVISION + DEPT) = '020655')) ;
					AND STATUS <> 'T' ;
					&lcBusUnitWhere ;
					ORDER BY 1, 3


				*!*	SELECT
				*!*	DISTINCT
				*!*	COMPANYCODE AS ADP_COMP,
				*!*	FILE# AS FILE_NUM,
				*!*	NAME,
				*!*	CHECKVIEWHOMEDEPT AS HOMEDEPT,
				*!*	1.00 AS HEADCNT,
				*!*	0.00 AS CODEHOURS,
				*!*	160.00 AS REGHOURS,
				*!*	0.00 AS OTHOURS,
				*!*	160.00 AS TOTHOURS,
				*!*	'U' AS GENDER,
				*!*	'U' AS MGMTTYPE,
				*!*	'U' AS TIMETYPE,
				*!*	'U  ' AS TGFDEPT
				*!*	FROM REPORTS.V_CHK_VW_INFO
				*!*	WHERE ((COMPANYCODE <> 'E87') OR (CHECKVIEWHOMEDEPT = '020655'))
				*!*	AND CHECKVIEWPAYDATE >= <<lcADPFromDate>>
				*!*	AND CHECKVIEWPAYDATE <= <<lcADPToDate>>
				*!*	AND {FN IFNULL(CHECKVIEWGROSSPAYA,0.00)} > 0.00
				*!*	<<lcDivWhere>>
				*!*	ORDER BY FILE#

				*!*	SELECT CURSALPRE
				*!*	BROWSE
				*!*	THROW


				* get CURSALARIEDHEADCNT
				* we don't have access to a check info table, so we are going to get this from	EEINFO, and hardcode the paydate as the from date

				SELECT ;
					ADP_COMP, ;
					ldFromDate AS PAYDATE, ;
					NAME, ;
					INSPID AS FILE_NUM, ;
					DIVISION, ;
					DEPT, ;
					COLLAR AS MGMTTYPE, ;
					TOLLCLASS AS TGFDEPT ;
					FROM EEINFO  ;
					INTO CURSOR CURSALARIEDHEADCNT ;
					WHERE ((ADP_COMP <> 'E87') OR ((DIVISION + DEPT) = '020655')) ;
					AND STATUS <> 'T' ;
					&lcBusUnitWhere ;
					ORDER BY 3, 2

				*!*	SELECT CURSALARIEDHEADCNT
				*!*	BROWSE
				*!*	THROW


				*!*	SELECT
				*!*	A.COMPANYCODE AS ADP_COMP,
				*!*	A.CHECKVIEWPAYDATE AS PAYDATE,
				*!*	A.NAME,
				*!*	A.FILE# AS FILE_NUM,
				*!*	{fn left(A.CHECKVIEWHOMEDEPT,2)} AS DIVISION,
				*!*	{fn SUBSTRING(A.CHECKVIEWHOMEDEPT,3,4)} AS DEPT,
				*!*	'U' AS MGMTTYPE,
				*!*	'U  ' AS TGFDEPT
				*!*	FROM REPORTS.V_CHK_VW_INFO A
				*!*	WHERE ((A.COMPANYCODE <> 'E87') OR (A.CHECKVIEWHOMEDEPT = '020655'))
				*!*	AND A.CHECKVIEWPAYDATE >= <<lcADPFromDate>>
				*!*	AND A.CHECKVIEWPAYDATE <= <<lcADPToDate>>
				*!*	AND {FN IFNULL(A.CHECKVIEWGROSSPAYA,0.00)} > 0.00
				*!*	<<lcDivWhere2>>
				*!*	ORDER BY A.FILE#


				* get CURCUSTOM
				SELECT ;
					STATUS, ;
					ADP_COMP, ;
					NAME, ;
					INSPID AS FILE_NUM, ;
					GENDER, ;
					COLLAR AS MGMTTYPE, ;
					TOLLCLASS AS TGFDEPT, ;
					'F' AS TIMETYPE, ;
					ANNSALARY ;
					FROM EEINFO ;
					INTO CURSOR CURCUSTOM ;
					ORDER BY 1, 2, 3

				*!*	SELECT CURCUSTOM
				*!*	BROWSE
				*!*	THROW



*!*					* WE WANT TO DELETE ANY DIV 59 DATA AFTER 6/30/2013 - Because Underarmour took over Rialto on 7/1/2013 but kept using our clocks for a few weeks.
*!*					IF ldFromDate > {^2013-06-30} THEN
*!*						DO CASE

*!*							CASE INLIST(tcType,"K","Q","R")
*!*								SELECT CURSALPRE
*!*								SCAN
*!*									IF LEFT(HOMEDEPT,2) = '59' THEN
*!*										DELETE
*!*									ENDIF
*!*								ENDSCAN

*!*								SELECT CURSALARIEDHEADCNT
*!*								SCAN
*!*									IF DIVISION = '59' THEN
*!*										DELETE
*!*									ENDIF
*!*								ENDSCAN

*!*							OTHERWISE
*!*								* nothing
*!*						ENDCASE
*!*					ENDIF


				*!*							* We need to adjust FTE in cursalpre for those who appear more than once
				*!*							IF USED('CURDUPES') THEN
				*!*								USE IN CURDUPES
				*!*							ENDIF

				*!*							SELECT FILE_NUM, COUNT(*) AS HEADCNT ;
				*!*								FROM CURSALPRE ;
				*!*								INTO CURSOR CURDUPES ;
				*!*								GROUP BY 1

				*!*							IF USED('CURDUPES') AND NOT EOF('CURDUPES') THEN
				*!*								SELECT CURDUPES
				*!*								SCAN FOR HEADCNT > 1
				*!*									SELECT CURSALPRE
				*!*									SCAN FOR FILE_NUM == CURDUPES.FILE_NUM
				*!*										REPLACE CURSALPRE.REGHOURS WITH ( CURSALPRE.REGHOURS / CURDUPES.HEADCNT ), ;
				*!*											CURSALPRE.TOTHOURS WITH ( CURSALPRE.TOTHOURS / CURDUPES.HEADCNT ) ;
				*!*											IN CURSALPRE
				*!*									ENDSCAN
				*!*								ENDSCAN
				*!*							ENDIF

				*!*							IF USED('CURDUPES') THEN
				*!*								USE IN CURDUPES
				*!*							ENDIF

				*!*	SELECT CURSALPRE
				*!*	BROWSE


				* Add salaried info to CURHOURS, which has only hourly in it right now...
				SELECT CURSALPRE
				SCAN
					SCATTER MEMVAR
					m.PAYDATE = ldFromDate
					m.EXPFTE = lnFTEHours
					m.ANNSALARY = 00000000.00
					INSERT INTO CURHOURS FROM MEMVAR
				ENDSCAN

				*!*						ENDIF && .ExecSQL(lcSQL, 'CURCUSTOM', RETURN_DATA_MANDATORY)

				* POPULATE MISSING INFO FROM OTHER CURSOR AND ALSO CALC FTE
				SELECT CURHOURS
				SCAN FOR ADP_COMP <> 'TMP'

					REPLACE CURHOURS.TIMETYPE WITH "F" IN CURHOURS

					SELECT CURCUSTOM
					LOCATE FOR ;
						ALLTRIM(ADP_COMP) = ALLTRIM(CURHOURS.ADP_COMP) ;
						AND FILE_NUM = CURHOURS.FILE_NUM ;
						AND ALLTRIM(STATUS) <> 'T'

					IF FOUND() THEN
						REPLACE CURHOURS.TIMETYPE WITH ALLTRIM(CURCUSTOM.TIMETYPE), ;
							CURHOURS.GENDER WITH ALLTRIM(CURCUSTOM.GENDER), ;
							CURHOURS.NAME WITH ALLTRIM(CURCUSTOM.NAME), ;
							CURHOURS.ANNSALARY WITH CURCUSTOM.ANNSALARY ;
							IN CURHOURS

						*!*									* for full-time / part-time flag, if it's not a P, make it an F
						*!*									IF NOT UPPER(ALLTRIM(CURCUSTOM.TIMETYPE)) == "P" THEN
						*!*										REPLACE CURHOURS.TIMETYPE WITH "F" IN CURHOURS
						*!*									ENDIF

					ELSE
						* active status not found, try leave in case they are recent leave
						SELECT CURCUSTOM
						LOCATE FOR ;
							ALLTRIM(ADP_COMP) = ALLTRIM(CURHOURS.ADP_COMP) ;
							AND FILE_NUM = CURHOURS.FILE_NUM ;
							AND (ALLTRIM(STATUS) = 'L')

						IF FOUND() THEN
							REPLACE CURHOURS.TIMETYPE WITH ALLTRIM(CURCUSTOM.TIMETYPE), ;
								CURHOURS.GENDER WITH ALLTRIM(CURCUSTOM.GENDER), ;
								CURHOURS.NAME WITH ALLTRIM(CURCUSTOM.NAME), ;
								CURHOURS.ANNSALARY WITH CURCUSTOM.ANNSALARY ;
								IN CURHOURS

							*!*										* for full-time / part-time flag, if it's not a P, make it an F
							*!*										IF NOT UPPER(ALLTRIM(CURCUSTOM.TIMETYPE)) == "P" THEN
							*!*											REPLACE CURHOURS.TIMETYPE WITH "F" IN CURHOURS
							*!*										ENDIF

						ELSE
							* just search by file#, ignore status and companycode

							SELECT CURCUSTOM
							LOCATE FOR FILE_NUM = CURHOURS.FILE_NUM

							IF FOUND() THEN
								REPLACE CURHOURS.TIMETYPE WITH ALLTRIM(CURCUSTOM.TIMETYPE), ;
									CURHOURS.GENDER WITH ALLTRIM(CURCUSTOM.GENDER), ;
									CURHOURS.NAME WITH ALLTRIM(CURCUSTOM.NAME), ;
									CURHOURS.ANNSALARY WITH CURCUSTOM.ANNSALARY ;
									IN CURHOURS

								*!*											* for full-time / part-time flag, if it's not a P, make it an F
								*!*											IF NOT UPPER(ALLTRIM(CURCUSTOM.TIMETYPE)) == "P" THEN
								*!*												REPLACE CURHOURS.TIMETYPE WITH "F" IN CURHOURS
								*!*											ENDIF

							ELSE
								* nothing can be done
								SELECT CURCUSTOM
								LOCATE
							ENDIF

						ENDIF

					ENDIF

				ENDSCAN

				*!*		SELECT CURHOURS
				*!*		BROW
				*!*	SET STEP ON
				*****************************************************************
				* populate tgf depts from the new dept translation table
				SELECT CURHOURS
				SCAN
					lcDept = RIGHT(ALLTRIM(CURHOURS.HOMEDEPT),4)
					SELECT DEPTLOOKUPTABLE
					LOCATE FOR DEPT = lcDept
					IF FOUND() THEN
						REPLACE CURHOURS.MGMTTYPE WITH DEPTLOOKUPTABLE.COLLARTYPE, ;
							CURHOURS.TGFDEPT WITH DEPTLOOKUPTABLE.HCFTECODE, ;
							CURHOURS.GLACCTNO WITH DEPTLOOKUPTABLE.GLACCTNO, ;
							CURHOURS.DEPTDESC WITH DEPTLOOKUPTABLE.HCFTEDESC ;
							IN CURHOURS
					ENDIF
				ENDSCAN
				*****************************************************************

				*!*	IF (tcMode = "ALL") THEN

				*!*		SELECT CURHOURS
				*!*		BROWSE
				*!*		THROW
				*!*
				*!*	ENDIF

				* CALC FTE
				SELECT CURHOURS
				SCAN

					* NOW THEY ARE ALL TREATED THE SAME 07/03/2012 MB
					REPLACE CURHOURS.FTE WITH ( CURHOURS.TOTHOURS / lnFTEHours ) IN CURHOURS

					* populate wageamt = 1/12th of annual salary for salAried emps, per Ken K. 11/12/12 MB
					IF INLIST(CURHOURS.ADP_COMP,'E88','E89') THEN
						REPLACE CURHOURS.WAGEAMT WITH (CURHOURS.ANNSALARY / 12)
					ENDIF

				ENDSCAN

				SELECT * ;
					FROM CURHOURS ;
					INTO CURSOR CURHOURS2 ;
					ORDER BY ADP_COMP, NAME ;
					READWRITE

				* apply temp markup factor
				SELECT CURHOURS2
				SCAN FOR ADP_COMP = 'TMP'
					REPLACE CURHOURS2.WAGEAMT WITH ( lnTempMarkUpFactor * CURHOURS2.WAGEAMT )
				ENDSCAN

				* populate salaries for E87 and E88
				SELECT CURHOURS2
				SCAN FOR NOT (ADP_COMP = 'TMP')
					SELECT EEINFO
					LOCATE FOR INSPID = CURHOURS2.FILE_NUM
					IF FOUND() THEN
						REPLACE CURHOURS2.ANNSALARY WITH EEINFO.ANNSALARY IN CURHOURS2
					ENDIF
				ENDSCAN

				SELECT EEINFO
				LOCATE



				IF (tcMode = "ALL") THEN

					*SELECT CURHOURS2
					*BROW

					* track some detail info for people with bad/missing data
					SELECT CURHOURS2
					SCAN FOR ('U' $ MGMTTYPE) OR ('U' $ TGFDEPT) OR (EMPTY(DEPTDESC))
						.cBadData = .cBadData + ALLTRIM(CURHOURS2.ADP_COMP) + ', ' + ALLTRIM(CURHOURS2.NAME) + ', ' + RIGHT(ALLTRIM(CURHOURS2.HOMEDEPT),4) + ', ' + TRANSFORM(CURHOURS2.FILE_NUM) + ;
							+ ', ' + CURHOURS2.MGMTTYPE + ', ' + CURHOURS2.TGFDEPT+ ', ' + CURHOURS2.DEPTDESC + CRLF
					ENDSCAN

				ENDIF



				IF tcType = "R" THEN
					* generate hc/fte entity report for Ken and Bob
					IF USED('CURHCFTEPRE') THEN
						USE IN CURHCFTEPRE
					ENDIF
					IF USED('CURHCFTE') THEN
						USE IN CURHCFTE
					ENDIF
					IF USED('CURMULTDEPTSPRE') THEN
						USE IN CURMULTDEPTSPRE
					ENDIF
					IF USED('CURMULTDEPTS') THEN
						USE IN CURMULTDEPTS
					ENDIF

					lcHCFTEEntityDetailFile  = "F:\UTIL\HR\TGFSTAFFRPT\REPORTS\HC_FTE_ENTITY_DETAIL" + lcSuffix + ".XLS"
					lcHCFTEEntitySummaryFile = "F:\UTIL\HR\TGFSTAFFRPT\REPORTS\HC_FTE_ENTITY_SUMMARY" + lcSuffix + ".XLS"
					lcHCFTEEntityMultDeptsFile = "F:\UTIL\HR\TGFSTAFFRPT\REPORTS\HC_FTE_ENTITY_MULTDEPTS" + lcSuffix + ".XLS"

					* this file may not always be created, so delete any old ones if they are there...
					IF FILE(lcHCFTEEntityMultDeptsFile) THEN
						DELETE FILE (lcHCFTEEntityMultDeptsFile)
					ENDIF

					SELECT ;
						ADP_COMP, ;
						NAME, ;
						FILE_NUM, ;
						LEFT(ALLTRIM(HOMEDEPT),2) AS DIVISION, ;
						RIGHT(ALLTRIM(HOMEDEPT),4) AS DEPT, ;
						GLACCTNO, ;
						(GLACCTNO + "-0000-" + LEFT(ALLTRIM(HOMEDEPT),2)) AS GLCODE, ;
						HEADCNT, ;
						FTE ;
						FROM CURHOURS2 ;
						INTO CURSOR CURHCFTEPRE ;
						WHERE ALLTRIM(ADP_COMP) <> 'TMP' ;
						ORDER BY 1, 2 ;
						READWRITE

					SELECT ;
						NAME, ;
						FILE_NUM, ;
						COUNT(*) AS COUNT ;
						FROM CURHCFTEPRE ;
						INTO CURSOR CURMULTDEPTSPRE ;
						GROUP BY NAME, FILE_NUM ;
						ORDER BY NAME, FILE_NUM ;
						HAVING COUNT > 1

					IF USED('CURMULTDEPTSPRE') AND NOT EOF('CURMULTDEPTSPRE') THEN

						SELECT * ;
							FROM CURHCFTEPRE ;
							INTO CURSOR CURMULTDEPTS ;
							WHERE FILE_NUM IN (SELECT FILE_NUM FROM CURMULTDEPTSPRE) ;
							ORDER BY NAME

						SELECT CURMULTDEPTS
						COPY TO (lcHCFTEEntityMultDeptsFile) XL5

					ENDIF




					*!*				SELECT CURHCFTEPRE
					*!*				BROWSE
					SELECT CURHCFTEPRE
					COPY TO (lcHCFTEEntityDetailFile) XL5

					SELECT ;
						DEPT, ;
						GLCODE, ;
						SUM(FTE) AS FTE, ;
						SUM(HEADCNT) AS HEADCNT ;
						FROM CURHCFTEPRE ;
						INTO CURSOR CURHCFTE ;
						GROUP BY 1, 2 ;
						ORDER BY 1, 2 ;
						READWRITE

					*!*				SELECT CURHCFTE
					*!*				BROWSE

					SELECT CURHCFTE
					COPY TO (lcHCFTEEntitySummaryFile) XL5


				ENDIF  && tcType = "R"

				SELECT CURHOURS2
				lcFileName = "F:\UTIL\HR\TGFSTAFFRPT\REPORTS\DETAIL\tgfhours" + lcSuffix + ".XLS"
				*COPY TO ( "F:\UTIL\HR\TGFSTAFFRPT\REPORTS\DETAIL\tgfhours" + lcSuffix + ".XLS" ) XL5
				COPY TO ( lcFileName ) XL5

				IF LEFT( lcSuffix, 4 ) == "_ALL" THEN
					* we just copied the detail file which Ken now wants - keep track of the filename for later
					.cDetailFileForKen = lcFileName
				ENDIF

				*SELECT CURHOURS2
				*COPY TO ( "F:\UTIL\HR\TGFSTAFFRPT\REPORTS\DETAIL\tgfhours" + lcSuffix )


				*******************************************************************************************************
				*******************************************************************************************************
				IF (tcMode = "ALL") AND .lDoBIExport THEN

					* create data for Qlikview

					*.CreateBIData()
					*!*		SELECT CURHOURSPRE3
					*!*		BROW
					* NOTE: the copying and renaming below is needed to make the 1st tab of the spreadsheet have the name "sheet1".
					* Because VFP will give that tab the same name as the spreadhseet filename via COPY TO ... XL5 command. MB 05/11/2012.

					IF .lTestMode THEN
						lcBIFileName = "F:\UTIL\HR\TGFSTAFFRPT\REPORTS\BIDATA\ADPFTE" + lcSuffix + ".XLS"
						SELECT CURHOURS2
						COPY TO F:\UTIL\HR\TGFSTAFFRPT\REPORTS\BIDATA\Sheet1.XLS XL5
						IF FILE( lcBIFileName ) THEN
							DELETE FILE ( lcBIFileName )
						ENDIF
						RENAME F:\UTIL\HR\TGFSTAFFRPT\REPORTS\BIDATA\Sheet1.XLS TO ( lcBIFileName )
						.TrackProgress('Created ' + lcBIFileName + ' (TEST Qlikview Folder)!', LOGIT+SENDIT)
					ELSE
						lcBIFileName = "S:\QLIKVIEW\ADP\ADPFTE" + lcSuffix + ".XLS"
						SELECT CURHOURS2
						COPY TO S:\QLIKVIEW\ADP\Sheet1.XLS XL5
						IF FILE( lcBIFileName ) THEN
							DELETE FILE ( lcBIFileName )
						ENDIF
						RENAME S:\QLIKVIEW\ADP\Sheet1.XLS TO ( lcBIFileName )
						.TrackProgress('Created ' + lcBIFileName + ' (PROD Qlikview Folder)!', LOGIT+SENDIT)
					ENDIF

					*SELECT CURHOURS2
					*COPY TO ( "F:\UTIL\HR\TGFSTAFFRPT\REPORTS\BIDATA\ADPFTE" + lcSuffix )
				ENDIF  &&  (tcMode = "ALL") AND .lDoBIExport
				*******************************************************************************************************
				*******************************************************************************************************

				* RECORD DISTINCT DIVISIONS - FOR DEBUG PURPOSES
				SELECT DISTINCT LEFT(HOMEDEPT,2) AS DIV ;
					FROM CURHOURS2 ;
					INTO CURSOR CURDIVLIST ;
					ORDER BY 1

				SELECT CURDIVLIST
				COPY TO ( "F:\UTIL\HR\TGFSTAFFRPT\REPORTS\DETAIL\DIV_LIST" + lcSuffix + ".XLS" ) XL5

				* RECORD DISTINCT INFO - FOR DEBUG PURPOSES
				SELECT A.NAME, A.FILE_NUM, A.HOMEDEPT  ;
					FROM CURHOURS2 A ;
					INTO CURSOR CURHEADCOUNT ;
					WHERE EXISTS ;
					(SELECT NAME FROM CURHOURS2 WHERE NAME = A.NAME GROUP BY NAME HAVING COUNT(*) > 1) ;
					ORDER BY A.NAME

				SELECT CURHEADCOUNT
				COPY TO ( "F:\UTIL\HR\TGFSTAFFRPT\REPORTS\DETAIL\HEADCOUNT" + lcSuffix + ".XLS" ) XL5


				********************************************************
				********************************************************
				********************************************************
				.GetAdjustedHeadcounts()
				********************************************************
				********************************************************
				********************************************************




				lcHeadCountBasis = "F:\UTIL\HR\TGFSTAFFRPT\REPORTS\DETAIL\HeadCountBasis" + lcSuffix + ".XLS"
				SELECT CURFINALHEADCNT
				COPY TO (lcHeadCountBasis) XL5



				SELECT CURWHITE
				COPY TO ( "F:\UTIL\HR\TGFSTAFFRPT\REPORTS\DETAIL\tgfwhitecollar" + lcSuffix + ".XLS" ) XL5
				SELECT CURBLUE
				COPY TO ( "F:\UTIL\HR\TGFSTAFFRPT\REPORTS\DETAIL\tgfblueecollar" + lcSuffix + ".XLS" ) XL5

				* summarize for part-time / full-ime
				SELECT TIMETYPE, ;
					SUM(FTE) AS FTE, ;
					COUNT(DISTINCT FILE_NUM) AS HEADCNT ;
					FROM CURHOURS2 ;
					INTO CURSOR CURCNTTYPE ;
					GROUP BY 1 ;
					ORDER BY 1

				SELECT CURCNTTYPE
				COPY TO ( "F:\UTIL\HR\TGFSTAFFRPT\REPORTS\DETAIL\tgftimetype" + lcSuffix + ".XLS" ) XL5


				* summarize BY GENDER
				SELECT GENDER, ;
					SUM(FTE) AS FTE, ;
					COUNT(DISTINCT FILE_NUM) AS HEADCNT ;
					FROM CURHOURS2 ;
					INTO CURSOR CURGENDER ;
					GROUP BY 1 ;
					ORDER BY 1

				SELECT CURGENDER
				COPY TO ( "F:\UTIL\HR\TGFSTAFFRPT\REPORTS\DETAIL\tgfgender"  + lcSuffix + ".XLS" ) XL5

				*!*							=SQLDISCONNECT(.nSQLHandle)


				**********************************************************************************
				**********************************************************************************
				**********************************************************************************

				* populate the spreadsheet

				oWorksheet.RANGE("C1").VALUE = 'for Date Range: ' + DTOC(ldFromDate) + " - " + DTOC(ldToDate)

				lnRow = lnStartRow
				lcRow = ALLTRIM(STR(lnRow))
				lnMgmtStartRow = lnRow + 1
				lcMgmtStartRow = ALLTRIM(STR(lnMgmtStartRow))
				oWorksheet.RANGE("A"+lcRow).VALUE = "Mgmt. Type"
				oWorksheet.RANGE("B"+lcRow).VALUE = "TGF Dept."
				oWorksheet.RANGE("C"+lcRow).VALUE = "Dept. Description"
				oWorksheet.RANGE("D"+lcRow).VALUE = "FTE"
				oWorksheet.RANGE("E"+lcRow).VALUE = "Headcount"
				IF .lShowHours THEN
					oWorksheet.RANGE("F"+lcRow).VALUE = "Tot Hours"
				ENDIF
				oWorksheet.RANGE("A"+lcRow+":F"+lcRow).FONT.BOLD = .T.

				SELECT CURWHITE
				SCAN
					lnRow = lnRow + 1
					lcRow = ALLTRIM(STR(lnRow))
					oWorksheet.RANGE("A"+lcRow).VALUE = CURWHITE.MGMTTYPE
					oWorksheet.RANGE("B"+lcRow).VALUE = CURWHITE.TGFDEPT
					oWorksheet.RANGE("C"+lcRow).VALUE = CURWHITE.DEPTDESC
					oWorksheet.RANGE("D"+lcRow).VALUE = CURWHITE.FTE
					*oWorksheet.RANGE("E"+lcRow).VALUE = CURWHITE.HEADCNT
					oWorksheet.RANGE("E"+lcRow).VALUE = CURWHITE.ADJHDCNT
					IF .lShowHours THEN
						oWorksheet.RANGE("F"+lcRow).VALUE = CURWHITE.TOTHOURS
					ENDIF
				ENDSCAN

				SELECT CURBLUE
				SCAN
					lnRow = lnRow + 1
					lcRow = ALLTRIM(STR(lnRow))
					oWorksheet.RANGE("A"+lcRow).VALUE = CURBLUE.MGMTTYPE
					oWorksheet.RANGE("B"+lcRow).VALUE = CURBLUE.TGFDEPT
					oWorksheet.RANGE("C"+lcRow).VALUE = CURBLUE.DEPTDESC
					oWorksheet.RANGE("D"+lcRow).VALUE = CURBLUE.FTE
					*oWorksheet.RANGE("E"+lcRow).VALUE = CURBLUE.HEADCNT
					oWorksheet.RANGE("E"+lcRow).VALUE = CURBLUE.ADJHDCNT
					IF .lShowHours THEN
						oWorksheet.RANGE("F"+lcRow).VALUE = CURBLUE.TOTHOURS
					ENDIF
				ENDSCAN

				lnMgmtEndRow = lnRow
				lcMgmtEndRow = ALLTRIM(STR(lnMgmtEndRow))

				lnRow = lnRow + 1
				lcRow = ALLTRIM(STR(lnRow))

				* Ken wants totals for the Mgmt Type section...
				oWorksheet.RANGE("D" + lcRow).VALUE = "=SUM(D" + lcMgmtStartRow + ":D" + lcMgmtEndRow + ")"
				oWorksheet.RANGE("E" + lcRow).VALUE = "=SUM(E" + lcMgmtStartRow + ":E" + lcMgmtEndRow + ")"

				* UNDERLINE cell columns to be totaled...
				oWorksheet.RANGE("D" + lcMgmtEndRow + ":E" + lcMgmtEndRow).BORDERS(xlEdgeBottom).LineStyle = xlContinuous
				oWorksheet.RANGE("D" + lcMgmtEndRow + ":E" + lcMgmtEndRow).BORDERS(xlEdgeBottom).Weight = xlMedium

				lnRow = lnRow + 2
				lcRow = ALLTRIM(STR(lnRow))
				oWorksheet.RANGE("A"+lcRow).VALUE = "Emp. Type"
				oWorksheet.RANGE("B"+lcRow).VALUE = "FTE"
				oWorksheet.RANGE("C"+lcRow).VALUE = "Headcount"
				oWorksheet.RANGE("A"+lcRow+":C"+lcRow).FONT.BOLD = .T.

				SELECT CURCNTTYPE
				SCAN
					lnRow = lnRow + 1
					lcRow = ALLTRIM(STR(lnRow))
					oWorksheet.RANGE("A"+lcRow).VALUE = .GetTimeTypeDesc( CURCNTTYPE.TIMETYPE )  && IIF(CURCNTTYPE.TIMETYPE="F","Full Time","Part Time")
					oWorksheet.RANGE("B"+lcRow).VALUE = CURCNTTYPE.FTE
					oWorksheet.RANGE("C"+lcRow).VALUE = CURCNTTYPE.HEADCNT
				ENDSCAN

				lnPreCasualRow = lnRow  && save row for filling in temp data later

				* final roll up
				*!*							SELECT ;
				*!*								"Casual" AS TYPE, ;
				*!*								SUM(TOTHOURS) AS TOTHOURS, ;
				*!*								SUM(FTE) AS FTE, ;
				*!*								SUM(HEADCNT) AS HEADCNT ;
				*!*								FROM CURHOURS2 ;
				*!*								INTO CURSOR CURTMPHEADCNT ;
				*!*								WHERE ADP_COMP = 'TMP'

				SELECT ;
					"Casual" AS TYPE, ;
					SUM(TOTHOURS) AS TOTHOURS, ;
					SUM(FTE) AS FTE, ;
					COUNT(DISTINCT FILE_NUM) AS HEADCNT ;
					FROM CURHOURS2 ;
					INTO CURSOR CURTMPHEADCNT ;
					WHERE ADP_COMP = 'TMP'

				SELECT CURTMPHEADCNT
				COPY TO ("F:\UTIL\HR\TGFSTAFFRPT\REPORTS\DETAIL\TGFTEMPS"  + lcSuffix + ".XLS") XL5

				lnRow = lnRow + 2
				lcRow = ALLTRIM(STR(lnRow))
				oWorksheet.RANGE("A"+lcRow).VALUE = "Gender"
				oWorksheet.RANGE("B"+lcRow).VALUE = "FTE"
				oWorksheet.RANGE("C"+lcRow).VALUE = "Headcount"
				oWorksheet.RANGE("A"+lcRow+":C"+lcRow).FONT.BOLD = .T.

				SELECT CURGENDER
				SCAN
					lnRow = lnRow + 1
					lcRow = ALLTRIM(STR(lnRow))
					oWorksheet.RANGE("A"+lcRow).VALUE = CURGENDER.GENDER
					oWorksheet.RANGE("B"+lcRow).VALUE = CURGENDER.FTE
					oWorksheet.RANGE("C"+lcRow).VALUE = CURGENDER.HEADCNT
				ENDSCAN
				
				
				**********************************************************
				**********************************************************
				IF tcMode = "ALL" THEN
					* populate 5th tab with format from H/K
					oWorksheet = oWorkbook.Worksheets[5]
					
					**** calculate FISCAL YEAR and Period
					lnMonth = MONTH(ldFromDate)
					
					IF INLIST(lnMonth,7,8,9,10,11,12) THEN
						lnYear1 = YEAR(ldFromDate)
					ELSE
						lnYear1 = YEAR(ldFromDate) - 1
					ENDIF
					lnYear2 = lnYear1 + 1
					lcFiscal = ALLTRIM(TRANSFORM(lnYear1)) + "/" + ALLTRIM(TRANSFORM(lnYear2))
					
					oWorksheet.RANGE("B4").VALUE = lcFiscal
					oWorksheet.RANGE("B5").VALUE = CMONTH(ldFromDate)
					
					
					**** WHITE COLLAR
					
					SELECT CURWHITE
					
					LOCATE FOR ALLTRIM(TGFDEPT) == "RE"
					IF FOUND() THEN
						oWorksheet.RANGE("B15").VALUE = CURWHITE.FTE
						oWorksheet.RANGE("C15").VALUE = CURWHITE.ADJHDCNT
					ENDIF
					
					LOCATE FOR ALLTRIM(TGFDEPT) == "BR"
					IF FOUND() THEN
						oWorksheet.RANGE("B16").VALUE = CURWHITE.FTE
						oWorksheet.RANGE("C16").VALUE = CURWHITE.ADJHDCNT
					ENDIF
					
					LOCATE FOR ALLTRIM(TGFDEPT) == "AX"
					IF FOUND() THEN
						oWorksheet.RANGE("B21").VALUE = CURWHITE.FTE
						oWorksheet.RANGE("C21").VALUE = CURWHITE.ADJHDCNT
					ENDIF
					
					LOCATE FOR ALLTRIM(TGFDEPT) == "AI"
					IF FOUND() THEN
						oWorksheet.RANGE("B22").VALUE = CURWHITE.FTE
						oWorksheet.RANGE("C22").VALUE = CURWHITE.ADJHDCNT
					ENDIF
					
					LOCATE FOR ALLTRIM(TGFDEPT) == "SX"
					IF FOUND() THEN
						oWorksheet.RANGE("B23").VALUE = CURWHITE.FTE
						oWorksheet.RANGE("C23").VALUE = CURWHITE.ADJHDCNT
					ENDIF
					
					LOCATE FOR ALLTRIM(TGFDEPT) == "SI"
					IF FOUND() THEN
						oWorksheet.RANGE("B24").VALUE = CURWHITE.FTE
						oWorksheet.RANGE("C24").VALUE = CURWHITE.ADJHDCNT
					ENDIF
					
					LOCATE FOR ALLTRIM(TGFDEPT) == "TP"
					IF FOUND() THEN
						oWorksheet.RANGE("B28").VALUE = CURWHITE.FTE
						oWorksheet.RANGE("C28").VALUE = CURWHITE.ADJHDCNT
					ENDIF
					
					LOCATE FOR ALLTRIM(TGFDEPT) == "BK"
					IF FOUND() THEN
						oWorksheet.RANGE("B30").VALUE = CURWHITE.FTE
						oWorksheet.RANGE("C30").VALUE = CURWHITE.ADJHDCNT
					ENDIF
					
					LOCATE FOR ALLTRIM(TGFDEPT) == "SP"
					IF FOUND() THEN
						oWorksheet.RANGE("B33").VALUE = CURWHITE.FTE
						oWorksheet.RANGE("C33").VALUE = CURWHITE.ADJHDCNT
					ENDIF
					
					LOCATE FOR ALLTRIM(TGFDEPT) == "SL"
					IF FOUND() THEN
						oWorksheet.RANGE("B37").VALUE = CURWHITE.FTE
						oWorksheet.RANGE("C37").VALUE = CURWHITE.ADJHDCNT
					ENDIF
					
					LOCATE FOR ALLTRIM(TGFDEPT) == "SS"
					IF FOUND() THEN
						oWorksheet.RANGE("B38").VALUE = CURWHITE.FTE
						oWorksheet.RANGE("C38").VALUE = CURWHITE.ADJHDCNT
					ENDIF
					
					LOCATE FOR ALLTRIM(TGFDEPT) == "CA"
					IF FOUND() THEN
						oWorksheet.RANGE("B42").VALUE = CURWHITE.FTE
						oWorksheet.RANGE("C42").VALUE = CURWHITE.ADJHDCNT
					ENDIF
					
					LOCATE FOR ALLTRIM(TGFDEPT) == "FI"
					IF FOUND() THEN
						oWorksheet.RANGE("B43").VALUE = CURWHITE.FTE
						oWorksheet.RANGE("C43").VALUE = CURWHITE.ADJHDCNT
					ENDIF
					
					LOCATE FOR ALLTRIM(TGFDEPT) == "HR"
					IF FOUND() THEN
						oWorksheet.RANGE("B44").VALUE = CURWHITE.FTE
						oWorksheet.RANGE("C44").VALUE = CURWHITE.ADJHDCNT
					ENDIF
					
					LOCATE FOR ALLTRIM(TGFDEPT) == "IT"
					IF FOUND() THEN
						oWorksheet.RANGE("B45").VALUE = CURWHITE.FTE
						oWorksheet.RANGE("C45").VALUE = CURWHITE.ADJHDCNT
					ENDIF
					
					LOCATE FOR ALLTRIM(TGFDEPT) == "PS"
					IF FOUND() THEN
						oWorksheet.RANGE("B46").VALUE = CURWHITE.FTE
						oWorksheet.RANGE("C46").VALUE = CURWHITE.ADJHDCNT
					ENDIF
					
					**** BLUE COLLAR

					SELECT CURBLUE
					
					LOCATE FOR ALLTRIM(TGFDEPT) == "TP"
					IF FOUND() THEN
						oWorksheet.RANGE("B61").VALUE = CURBLUE.FTE
						oWorksheet.RANGE("C61").VALUE = CURBLUE.ADJHDCNT
					ENDIF					
					
					LOCATE FOR ALLTRIM(TGFDEPT) == "SP"
					IF FOUND() THEN
						oWorksheet.RANGE("B65").VALUE = CURBLUE.FTE
						oWorksheet.RANGE("C65").VALUE = CURBLUE.ADJHDCNT
					ENDIF
					
					
					**** HEADCOUNT BY TYPE
					
					SELECT CURCNTTYPE
					
					LOCATE FOR ALLTRIM(TIMETYPE) == "F"
					IF FOUND() THEN
						oWorksheet.RANGE("B71").VALUE = CURCNTTYPE.FTE
						oWorksheet.RANGE("C71").VALUE = CURCNTTYPE.HEADCNT
					ENDIF
					
					LOCATE FOR ALLTRIM(TIMETYPE) == "C"
					IF FOUND() THEN
						oWorksheet.RANGE("B75").VALUE = CURCNTTYPE.FTE
						oWorksheet.RANGE("C75").VALUE = CURCNTTYPE.HEADCNT
					ENDIF		
					
										
					**** HEADCOUNT BY GENDER
					
					SELECT CURGENDER
					
					LOCATE FOR ALLTRIM(GENDER) == "M"
					IF FOUND() THEN
						oWorksheet.RANGE("B79").VALUE = CURGENDER.FTE
						oWorksheet.RANGE("C79").VALUE = CURGENDER.HEADCNT
					ENDIF
					
					LOCATE FOR ALLTRIM(GENDER) == "F"
					IF FOUND() THEN
						oWorksheet.RANGE("B80").VALUE = CURGENDER.FTE
						oWorksheet.RANGE("C80").VALUE = CURGENDER.HEADCNT
					ENDIF
					
					LOCATE FOR ALLTRIM(GENDER) == "U"
					IF FOUND() THEN
						oWorksheet.RANGE("B81").VALUE = CURGENDER.FTE
						oWorksheet.RANGE("C81").VALUE = CURGENDER.HEADCNT
					ENDIF
					

				ENDIF && tcMode = "ALL" THEN
				**********************************************************
				**********************************************************

				*CLOSE DATABASES ALL
				*!*						ENDIF  &&  .ExecSQL(lcSQL, 'SQLCURSOR1', RETURN_DATA_MANDATORY)
				*!*					ELSE
				*!*						* connection error
				*!*						.TrackProgress('Unable to connect to ADP Payroll System.', LOGIT+SENDIT)
				*!*					ENDIF   &&  .nSQLHandle > 0

				.TrackProgress("FTE Info to Qlikview process ended normally.", LOGIT+SENDIT+NOWAITIT)


			CATCH TO loError

				.TrackProgress('There was an error.',LOGIT+SENDIT)
				.TrackProgress('The Error # is: ' + TRANSFORM(loError.ERRORNO), LOGIT+SENDIT)
				.TrackProgress('The Error Message is: ' + TRANSFORM(loError.MESSAGE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred in Program: ' + TRANSFORM(loError.PROCEDURE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred at line #: ' + TRANSFORM(loError.LINENO), LOGIT+SENDIT)
				lnNumberOfErrors = lnNumberOfErrors + 1

				IF (tcMode = "ALL") THEN
					IF TYPE('oWorkbook') = "O" THEN
						oWorkbook.SAVE()
					ENDIF
					IF TYPE('oExcel') = "O" THEN
						oExcel.QUIT()
					ENDIF
					*CLOSE DATABASES ALL
				ENDIF

			ENDTRY

			*CLOSE DATA
			WAIT CLEAR
			***************** INTERNAL email results ******************************

			* this block must get done only once, at the end of the process...
			IF (tcMode = "ALL") THEN

				IF (lnNumberOfErrors = 0) THEN
					* SAVE AND QUIT EXCEL
					oWorkbook.SAVE()
					oExcel.QUIT()

					DO CASE

						CASE tcType ="K"
							IF FILE(lcFiletoSaveAs) THEN
								.cAttach = lcFiletoSaveAs

								IF FILE(.cDetailFileForKen) THEN
									* add the detail file per Ken 12/10/12 MB
									.cAttach = .cAttach + "," + .cDetailFileForKen
								ELSE
									.TrackProgress("ERROR attaching: " + .cDetailFileForKen, LOGIT+SENDIT+NOWAITIT)
								ENDIF

								IF FILE(lcHeadCountBasis) THEN
									* add the adjusted headcount basis file per Ken 11/15/13 MB
									.cAttach = .cAttach + "," + lcHeadCountBasis
								ELSE
									.TrackProgress("ERROR attaching: " + lcHeadCountBasis, LOGIT+SENDIT+NOWAITIT)
								ENDIF

								.cBodyText = "See attached report." + CRLF + CRLF + "<<REPORT LOG FOLLOWS>>" + CRLF + .cBodyText
							ELSE
								.TrackProgress("ERROR attaching " + lcFiletoSaveAs, LOGIT+SENDIT+NOWAITIT)
							ENDIF

						CASE tcType ="R"
							IF FILE(lcHCFTEEntitySummaryFile) THEN
								.cAttach = lcHCFTEEntitySummaryFile

								IF FILE(lcHCFTEEntityDetailFile) THEN
									.cAttach = .cAttach + "," + lcHCFTEEntityDetailFile

									IF FILE(lcHCFTEEntityMultDeptsFile) THEN

										.cAttach = .cAttach + "," + lcHCFTEEntityMultDeptsFile

										.cBodyText = "See attached reports." + CRLF + CRLF + ;
											"WARNING: some people were in more than 1 div/dept in the time period. " + ;
											"This will cause total summary headcount by entity to be greater than overall total headcount. " + ;
											"See attached MULTDEPTS spreadsheet for a list of these people." + CRLF + CRLF + "<<REPORT LOG FOLLOWS>>" + CRLF + .cBodyText

									ELSE
										.cBodyText = "See attached reports." + CRLF + CRLF + "<<REPORT LOG FOLLOWS>>" + CRLF + .cBodyText
									ENDIF

								ELSE
									.TrackProgress("ERROR attaching " + lcHCFTEEntityDetailFile, LOGIT+SENDIT+NOWAITIT)
								ENDIF

								*.cBodyText = "See attached reports." + CRLF + CRLF + "<<REPORT LOG FOLLOWS>>" + CRLF + .cBodyText

							ELSE
								.TrackProgress("ERROR attaching " + lcHCFTEEntitySummaryFile, LOGIT+SENDIT+NOWAITIT)
							ENDIF

						OTHERWISE
							* NOTHING
					ENDCASE

					IF NOT EMPTY(.cBadData) THEN
						.cBadData = 'WARNING: the following people had bad or missing data elements:' + CRLF + CRLF + ;
							'ADP_COMP, NAME, DEPT, FILE_NUM, MGMTTYPE, TGFDEPT, DEPTDESC' + CRLF + .cBadData
						.cBodyText = .cBadData + CRLF + .cBodyText
					ENDIF

				ENDIF  &&  (lnNumberOfErrors = 0)

				.TrackProgress('About to send status email.',LOGIT)
				.TrackProgress('The logfile is: ' + .cLogFile, SENDIT)
				.TrackProgress("FTE Info process started: " + .cStartTime, LOGIT+SENDIT)
				.TrackProgress("FTE Info process finished: " + TTOC(DATETIME()), LOGIT+SENDIT)

				IF .lSendInternalEmailIsOn THEN
					DO FORM dartmail2 WITH .cSendTo,.cFrom,.cSubject,.cCC,.cAttach,.cBodyText,"A"
					.TrackProgress('Sent status email.',LOGIT)
				ELSE
					.TrackProgress('Did not send status email: THIS.lSendInternalEmailIsOn = FALSE.',LOGIT)
				ENDIF && tcMode = "ALL"
			ENDIF

		ENDWITH
		RETURN
	ENDFUNC && main



	PROCEDURE GetAdjustedHeadcounts
		WITH THIS

			IF USED('CURHOURLYHEADCNT') THEN
				USE IN CURHOURLYHEADCNT
			ENDIF

			SELECT ADP_COMP, ;
				PAYDATE, ;
				NAME, ;
				INT(VAL(PERSONNUM)) AS FILE_NUM, ;
				DIVISION, ;
				DEPT, ;
				MGMTTYPE, ;
				TGFDEPT ;
				FROM CURHOURLYHEADCNTPRE ;
				INTO CURSOR CURHOURLYHEADCNT ;
				ORDER BY PERSONNUM ;
				READWRITE


			*!*				SELECT CURHOURLYHEADCNT
			*!*				BROWSE
			*!*				SELECT CURSALARIEDHEADCNT
			*!*				BROWSE

			SELECT CURHOURLYHEADCNT
			APPEND FROM DBF('CURSALARIEDHEADCNT')
			*BROWSE

			IF USED('CURDISTINCT') THEN
				USE IN CURDISTINCT
			ENDIF

			SELECT DISTINCT ;
				A.FILE_NUM AS FILE_NUM, ;
				(SELECT MAX(PAYDATE) FROM CURHOURLYHEADCNT WHERE FILE_NUM = A.FILE_NUM) AS LASTPAY ;
				FROM CURHOURLYHEADCNT A ;
				INTO CURSOR CURDISTINCT ;
				ORDER BY A.FILE_NUM

			*!*	SELECT CURDISTINCT
			*!*	browse

			IF USED('CURFINALHEADCNT') THEN
				USE IN CURFINALHEADCNT
			ENDIF

			SELECT A.*, ;
				B.LASTPAY ;
				FROM CURHOURLYHEADCNT A ;
				INNER JOIN ;
				CURDISTINCT B ;
				ON A.FILE_NUM = B.FILE_NUM ;
				AND A.PAYDATE = B.LASTPAY ;
				INTO CURSOR CURFINALHEADCNT ;
				ORDER BY A.FILE_NUM ;
				READWRITE

			*!*	SELECT CURFINALHEADCNT
			*!*	browse

			* populate tgf depts from the new dept translation table
			SELECT CURFINALHEADCNT
			SCAN
				lcDept = ALLTRIM(CURFINALHEADCNT.DEPT)
				SELECT DEPTLOOKUPTABLE
				LOCATE FOR DEPT = lcDept
				IF FOUND() THEN
					REPLACE CURFINALHEADCNT.MGMTTYPE WITH DEPTLOOKUPTABLE.COLLARTYPE, ;
						CURFINALHEADCNT.TGFDEPT WITH DEPTLOOKUPTABLE.HCFTECODE ;
						IN CURFINALHEADCNT
				ENDIF
			ENDSCAN

			SELECT CURFINALHEADCNT
			LOCATE

			*!*	SELECT CURFINALHEADCNT
			*!*	browse

			IF USED('CURWHITEADJ') THEN
				USE IN CURWHITEADJ
			ENDIF
			IF USED('CURBLUEADJ') THEN
				USE IN CURBLUEADJ
			ENDIF

			* summarize for White Collar and Blue Collar adjustments - NEW METHOD
			SELECT "WHITE COLLAR" AS MGMTTYPE, ;
				TGFDEPT, ;
				COUNT(DISTINCT FILE_NUM) AS ADJHDCNT ;
				FROM CURFINALHEADCNT ;
				INTO CURSOR CURWHITEADJ ;
				WHERE CURFINALHEADCNT.MGMTTYPE = "W" ;
				GROUP BY 1, 2 ;
				ORDER BY 1, 2

			SELECT "BLUE COLLAR" AS MGMTTYPE, ;
				TGFDEPT, ;
				COUNT(DISTINCT FILE_NUM) AS ADJHDCNT ;
				FROM CURFINALHEADCNT ;
				INTO CURSOR CURBLUEADJ ;
				WHERE CURFINALHEADCNT.MGMTTYPE <> "W" ;
				GROUP BY 1, 2 ;
				ORDER BY 1, 2

			*!*	SELECT CURFINALHEADCNT
			*!*	browse


			* summarize for White Collar and Blue Collar - OLD METHOD
			SELECT "WHITE COLLAR" AS MGMTTYPE, ;
				TGFDEPT, ;
				DEPTDESC, ;
				SUM(FTE) AS FTE, ;
				COUNT(DISTINCT FILE_NUM) AS HEADCNT, ;
				0000 AS ADJHDCNT, ;
				SUM(TOTHOURS) AS TOTHOURS ;
				FROM CURHOURS2 ;
				INTO CURSOR CURWHITE ;
				WHERE CURHOURS2.MGMTTYPE = "W" ;
				GROUP BY 1, 2, 3 ;
				ORDER BY 1, 3 ;
				READWRITE

			SELECT "BLUE COLLAR" AS MGMTTYPE, ;
				TGFDEPT, ;
				DEPTDESC, ;
				SUM(FTE) AS FTE, ;
				COUNT(DISTINCT FILE_NUM) AS HEADCNT, ;
				0000 AS ADJHDCNT, ;
				SUM(TOTHOURS) AS TOTHOURS ;
				FROM CURHOURS2 ;
				INTO CURSOR CURBLUE ;
				WHERE CURHOURS2.MGMTTYPE <> "W" ;
				GROUP BY 1, 2, 3 ;
				ORDER BY 1, 3 ;
				READWRITE

			*!*	SELECT CURWHITE
			*!*	BROWSE
			*!*	SELECT CURWHITEADJ
			*!*	BROWSE

			*** NOW
			* adjust or 'correct' the OLD headcounts, eliminating duplicates caused by people being in more than 1 dept in the period
			SELECT CURWHITE
			SCAN
				SELECT CURWHITEADJ
				LOCATE FOR UPPER(ALLTRIM(TGFDEPT)) == UPPER(ALLTRIM(CURWHITE.TGFDEPT))
				IF FOUND() THEN
					REPLACE CURWHITE.ADJHDCNT WITH CURWHITEADJ.ADJHDCNT IN CURWHITE
				ENDIF
			ENDSCAN

			*!*				SELECT CURWHITE
			*!*				BROWSE

			SELECT CURBLUE
			SCAN
				SELECT CURBLUEADJ
				LOCATE FOR UPPER(ALLTRIM(TGFDEPT)) == UPPER(ALLTRIM(CURBLUE.TGFDEPT))
				IF FOUND() THEN
					REPLACE CURBLUE.ADJHDCNT WITH CURBLUEADJ.ADJHDCNT IN CURBLUE
				ENDIF
			ENDSCAN

			*!*				SELECT CURBLUE
			*!*				BROWSE


		ENDWITH
	ENDPROC



	FUNCTION GetTimeTypeDesc
		LPARAMETERS tcTimeType
		LOCAL lcRetVal
		lcRetVal = "Unknown"
		DO CASE
			CASE tcTimeType = "F"
				lcRetVal = "Full Time"
			CASE tcTimeType = "P"
				lcRetVal = "Part Time"
			CASE tcTimeType = "C"
				lcRetVal = "Casual"
			CASE tcTimeType = "O"
				lcRetVal = "Contract"
		ENDCASE

		RETURN lcRetVal
	ENDFUNC  &&  GetTimeTypeDesc



	FUNCTION ExecSQL
		LPARAMETERS tcSQL, tcCursorName, tlNoDataReturnedIsOkay
		LOCAL llRetval, lnResult
		WITH THIS
			* close target cursor if it's open
			IF USED(tcCursorName)
				USE IN (tcCursorName)
			ENDIF
			WAIT WINDOW "Fetching remote data; please wait..." NOWAIT
			lnResult = SQLEXEC(.nSQLHandle, tcSQL, tcCursorName)
			llRetval = ( lnResult > 0 )
			IF llRetval THEN
				* see if any data came back
				IF NOT tlNoDataReturnedIsOkay THEN
					IF (NOT USED(tcCursorName)) OR EOF(tcCursorName) THEN
						llRetval = .F.
						.TrackProgress('No data was returned by this query: ' +CRLF + tcSQL, LOGIT+SENDIT)
						*.cSendTo = 'Mark Bennett <mbennett@fmiint.com>'
					ENDIF
				ENDIF
			ELSE
				.TrackProgress("Result Code [" + TRANSFORM(lnResult) + "] returned by this query: " + tcSQL, LOGIT+SENDIT)
				*.cSendTo = 'Mark Bennett <mbennett@fmiint.com>'
			ENDIF
			WAIT CLEAR
			RETURN llRetval
		ENDWITH
	ENDFUNC


	PROCEDURE TrackProgress
		* do any combination of Wait Window, writing to logfile, and adding to body of email,
		* based on nFlags parameter.
		LPARAMETERS tcExpression, tnFlags
		WITH THIS
			IF BITAND(tnFlags,LOGIT) = LOGIT THEN
				IF .lLoggingIsOn THEN
					?
					? .cProcessName + "  " + TTOC(DATETIME()) + ": " + tcExpression
				ENDIF
			ENDIF
			IF .lWaitWindowIsOn THEN
				IF BITAND(tnFlags,NOWAITIT) = NOWAITIT THEN
					WAIT WINDOW tcExpression NOWAIT
				ENDIF
				IF BITAND(tnFlags,WAITIT) = WAITIT THEN
					WAIT WINDOW tcExpression TIMEOUT .nWaitWindowTimeout
				ENDIF
			ENDIF
			IF BITAND(tnFlags,SENDIT) = SENDIT THEN
				IF .lSendInternalEmailIsOn THEN
					.cBodyText = .cBodyText + tcExpression + CRLF + CRLF
				ENDIF
			ENDIF
		ENDWITH
	ENDPROC  &&  TrackProgress


	FUNCTION GetSQLStartDateTimeString
		* returns a timestamp string for use in SQL queries
		LPARAMETERS tdDate
		LOCAL lcSetDate, lcSQLDateTimeString
		lcSetDate = SET('DATE')
		SET DATE YMD
		lcSQLDateTimeString   = "{ts '" + TRANSFORM(YEAR(tdDate)) + "-" + PADL(MONTH(tdDate),2,"0") + "-" + PADL(DAY(tdDate),2,"0") + " 00:00:00.0'}"
		SET DATE &lcSetDate.
		RETURN lcSQLDateTimeString
	ENDFUNC


	FUNCTION GetSQLEndDateTimeString
		* returns a timestamp string for use in SQL queries
		LPARAMETERS tdDate
		LOCAL lcSetDate, lcSQLDateTimeString
		lcSetDate = SET('DATE')
		SET DATE YMD
		lcSQLDateTimeString = "{ts '" + TRANSFORM(YEAR(tdDate)) + "-" + PADL(MONTH(tdDate),2,"0") + "-" + PADL(DAY(tdDate),2,"0") + " 23:59:59.9'}"
		SET DATE &lcSetDate.
		RETURN lcSQLDateTimeString
	ENDFUNC


ENDDEFINE
