lparameter xtime, xnomilitarytime

xdisptime=space(8)

if gmilitarytime and !xnomilitarytime
	if !empty(xtime)
		xdisptime = left(chr2time(xtime),5)+" MT"
	endif
else
	xhh=padl(trans(val(left(xtime,2))),2)
	xmm=substr(xtime,3,2)
	xampm=upper(substr(xtime,5,1))

	if between(val(xhh),1,12) and between(xmm,"00","59") and inlist(xampm,"A","P")
		xdisptime = xhh+":"+xmm+" "+xampm+"M"
	endif
endif

return xdisptime