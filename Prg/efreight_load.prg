PUBLIC mbol,hbl_no,ams_hbl_id,cValue,cTable
STORE "XYZ" TO mbol,hbl_no,ams_hbl_id,cValue,cTable
CLOSE ALL
CLEAR

lTesting = .T.
IF lTesting
	SET EXCLUSIVE ON
ELSE
	SET EXCLUSIVE OFF
ENDIF

CD "f:\efreight\data\"
SELECT 0
USE efreightmain ALIAS efreightmain
ZAP
SELECT 0
USE efreightcontainer ALIAS efreightcontainer
ZAP
SELECT 0
USE efreightaddress ALIAS efreightaddress
ZAP
SET EXCLUSIVE OFF



IF lTesting
	CD "f:\efreight\AMSXML\Sent"
ELSE
	CD "f:\efreight\AMSXML\"
ENDIF

ADIR(ary1,"*.xml")
len1 = ALEN(ary1,1)
*ASSERT .F.

FOR i = 1 TO len1
	cFilename = ary1[i,1]
	WAIT WINDOW cFilename TIMEOUT 1
	SELECT 0
	CREATE CURSOR temp1 (field1 c(150))
	APPEND FROM &cFilename TYPE SDF
	REPLACE ALL field1 WITH ALLTRIM(field1)

	lFound = .F.
	SCAN
		IF ALLTRIM(field1) = "<AMSOUT>" && Populated file indicator
			lFound = .T.
			EXIT
		ENDIF
	ENDSCAN

	IF !lFound && If missing the AMSOUT segment
		WAIT WINDOW "Empty file...looping" NOWAIT
		?cFilename+" - EMPTY"
		LOOP
	ELSE
		?cFilename
	ENDIF

	LOCATE

	WAIT WINDOW "Now scanning "+cFilename NOWAIT
	SCAN
		IF "<t_ams_hbl"$LOWER(temp1.field1)
			DO mainbkdn
			IF lTesting
*				RETURN
			ENDIF
		ENDIF

		IF "<t_ams_container"$LOWER(temp1.field1)
			SELECT efreightcontainer
			DO ctrbkdn
			IF lTesting
*				RETURN
			ENDIF
		ENDIF

		IF "<t_cvm_notify"$LOWER(temp1.field1)
			SELECT efreightaddress
			DO addrbkdn WITH "NOTIFY"
			IF lTesting
*				RETURN
			ENDIF
		ENDIF

		IF "<t_cvm_shipper"$LOWER(temp1.field1)
			SELECT efreightaddress
			DO addrbkdn WITH "SHIPPER"
		ENDIF

		IF "<t_cvm_consignee"$LOWER(temp1.field1)
			SELECT efreightaddress
			DO addrbkdn WITH "CONSIGNEE"
		ENDIF

	ENDSCAN

ENDFOR

SELECT efreightmain
SET ORDER TO ams_hbl_id   && AMS_HBL_ID
SELECT efreightcontainer
SET ORDER TO ams_hbl_id   && AMS_HBL_ID
SELECT efreightaddress
SET ORDER TO ams_hbl_id   && AMS_HBL_ID
SELECT efreightmain
SET RELATION TO ams_hbl_id INTO efreightaddress
SET RELATION TO ams_hbl_id INTO efreightcontainer
BROWSE

RETURN

*************************
PROCEDURE mainbkdn
*************************
cTable = "efreightmain"
SELECT &cTable
WAIT WINDOW "Now running replacements to "+cTable NOWAIT
STORE "" TO m.mbol,m.hbl_no,m.ams_hbl_id
APPEND BLANK

SELECT temp1
SKIP 1 IN temp1

lContinue = .T.
DO WHILE lContinue
	IF ("<t_cvm_notify"$LOWER(temp1.field1)) OR ("<t_cvm_shipper"$LOWER(temp1.field1)) ;
			OR ("<t_ams_container"$LOWER(temp1.field1)) OR ("<t_cvm_consignee"$LOWER(temp1.field1)) ;
			OR ("</t_ams_hbl"$LOWER(temp1.field1)) OR ("</amsout"$LOWER(temp1.field1))
		lContinue = .F.
		EXIT
	ENDIF

	DO CASE
	CASE LOWER(temp1.field1) = "<voyage_no"
		cReplField = "VOYAGE"
		cFieldType = "C"
	CASE LOWER(temp1.field1) = "<vessel_name"
		cReplField = "VESSELNAME"
		cFieldType = "C"
	CASE LOWER(temp1.field1) = "<port_of_loading_code"
		cReplField = "PORTLOADCD"
		cFieldType = "C"
	CASE LOWER(temp1.field1) = "<port_of_unlading_code"
		cReplField = "PORTUNLDCD"
		cFieldType = "C"
	CASE LOWER(temp1.field1) = "<port_of_loading"
		cReplField = "PORTLOAD"
		cFieldType = "C"
	CASE LOWER(temp1.field1) = "<port_of_unlading"
		cReplField = "PORTUNLD"
		cFieldType = "C"
	CASE LOWER(temp1.field1) = "<on_board_date"
		cReplField = "ONBD_DTC"
		cFieldType = "D"
	CASE LOWER(temp1.field1) = "<eta_date"
		cReplField = "ETA_DTC"
		cFieldType = "D"
	CASE LOWER(temp1.field1) = "<agent_code"
		cReplField = "AGENTCD"
		cFieldType = "C"
	CASE LOWER(temp1.field1) = "<issuer_scac_code"
		cReplField = "ISS_SCACCD"
		cFieldType = "C"
	CASE LOWER(temp1.field1) = "<carrier_scac_code"
		cReplField = "CAR_SCACCD"
		cFieldType = "C"
	CASE LOWER(temp1.field1) = "<import_agent_code"
		cReplField = "IMPAGTCD"
		cFieldType = "C"
	CASE LOWER(temp1.field1) = "<master_doc_code"
		cReplField = "MDOCCD"
		cFieldType = "C"
	CASE LOWER(temp1.field1) = "<master_bl_no"
		cReplField = "MBOL"
		cFieldType = "C"
	CASE LOWER(temp1.field1) = "<lcl_sw"
		cReplField = "LCL_SW"
		cFieldType = "L"
	CASE LOWER(temp1.field1) = "<in_bond_entry_type"
		cReplField = "IBENT_TYPE"
		cFieldType = "C"
	CASE LOWER(temp1.field1) = "<hbl_no"
		cReplField = "HBL_NO"
		cFieldType = "C"
	CASE LOWER(temp1.field1) = "<total_package"
		cReplField = "TOT_PKG"
		cFieldType = "N"
	CASE LOWER(temp1.field1) = "<total_weight_kg"
		cReplField = "TOTWT_KG"
		cFieldType = "N"
	CASE LOWER(temp1.field1) = "<total_volumn_cbm"
		cReplField = "TOTVOL_CBM"
		cFieldType = "N"
	CASE LOWER(temp1.field1) = "<place_of_receipt"
		cReplField = "PL_RECEIPT"
		cFieldType = "C"
	CASE LOWER(temp1.field1) = "<shipper_id"
		cReplField = "SHIP_ID"
		cFieldType = "C"
	CASE LOWER(temp1.field1) = "<consignee_id"
		cReplField = "CONSIGN_ID"
		cFieldType = "C"
	CASE LOWER(temp1.field1) = "<description"
		IF !"</description"$LOWER(temp1.field1)
			DO WHILE !("</description"$LOWER(temp1.field1))
				cValue = cValue + ALLTRIM(STRTRAN(STRTRAN(temp1.field1,"<description>",""),"</description>",""))+CHR(13)
				IF !EOF()
					SKIP IN temp1
				ELSE
					EXIT
				ENDIF
			ENDDO
			cValue = cValue + ALLTRIM(STRTRAN(STRTRAN(temp1.field1,"<description>",""),"</description>",""))+CHR(13)
		ELSE
			startpos = AT(">",temp1.field1,1)+1
			endpos = AT("<",temp1.field1,2)-1
			IF (endpos = -1) OR (startpos = -1) && Indicates an end-tagged single field, no value
				cValue = ""
			ELSE
				insertlen = (endpos-startpos)+1
				cValue = ALLTRIM(SUBSTR(temp1.field1,startpos,insertlen))
			ENDIF
		ENDIF
		cReplField = "DESC"
		cFieldType = "M"
	CASE LOWER(temp1.field1) = "<marking"
		IF cFilename = "128207645465909600.xml"
			ASSERT .F.
		ENDIF
		IF !"</marking"$LOWER(temp1.field1)
			cValue = ""
			DO WHILE !("</marking"$LOWER(temp1.field1))
				cValue = cValue + ALLTRIM(STRTRAN(STRTRAN(temp1.field1,"<marking>",""),"</marking>",""))+CHR(13)
				IF !EOF('temp1')
					SKIP IN temp1
				ENDIF
			ENDDO
			cValue = cValue + ALLTRIM(STRTRAN(STRTRAN(temp1.field1,"<marking>",""),"</marking>",""))+CHR(13)
		ELSE
			startpos = AT(">",temp1.field1,1)+1
			endpos = AT("<",temp1.field1,2)-1
			IF (endpos = -1) OR (startpos = -1) && Indicates an end-tagged single field, no value
				cValue = ""
			ELSE
				insertlen = (endpos-startpos)+1
				cValue = ALLTRIM(SUBSTR(temp1.field1,startpos,insertlen))
			ENDIF
		ENDIF
		cReplField = "MARKING"
		cFieldType = "M"
	CASE LOWER(temp1.field1) = "<notify_id"
		cReplField = "NOTIFY_ID"
		cFieldType = "C"
	CASE LOWER(temp1.field1) = "<hbl_doc_code"
		cReplField = "HBL_DOCCD"
		cFieldType = "C"
	CASE LOWER(temp1.field1) = "<country_code"
		cReplField = "CTRYOFORIG"
		cFieldType = "C"
	CASE LOWER(temp1.field1) = "<country_of_origin"
		cReplField = "CTRYOFORIG"
		cFieldType = "C"
	CASE LOWER(temp1.field1) = "<submit_date"
*		ASSERT .f.
		cReplField = "SUBMITDTC"
		cFieldType = "D"
	CASE LOWER(temp1.field1) = "<place_of_delivery"
*		ASSERT .F.
		cReplField = "PLOFDELIV"
		cFieldType = "C"
	CASE LOWER(temp1.field1) = "<ams_hbl_id"
		ASSERT .F.
		cReplField = "AMS_HBL_ID"
		cFieldType = "C"
	CASE LOWER(temp1.field1) = "<status_id"
		cReplField = "STATUS_ID"
		cFieldType = "C"
	CASE LEFT(temp1.field1,19) = "</place of delivery"
		SKIP 1 IN temp
		LOOP
	OTHERWISE
		cRec = ALLTRIM(STR(RECNO('temp1')))
		WAIT WINDOW "Field: "+LOWER(temp1.field1)+" at Rec. #"+cRec+" is not assigned" TIMEOUT 2
*		SET STEP ON
		IF !EOF()
			SKIP 1 IN temp1
		ELSE
			EXIT
		ENDIF
		LOOP
	ENDCASE
	fieldextract(cReplField,cFieldType)
	IF !EOF()
		SKIP 1 IN temp1
	ENDIF
ENDDO
SELECT &cTable
*BROWSE

ENDPROC

*************************
PROCEDURE ctrbkdn
*************************
cTable = "efreightcontainer"
SELECT &cTable
WAIT WINDOW "Now running replacements to "+cTable NOWAIT
APPEND BLANK
GATHER MEMVAR FIELDS mbol,hbl_no,ams_hbl_id

SELECT temp1
SKIP 1 IN temp1

lContinue = .T.
DO WHILE lContinue
	IF ("</t_cvm_notify"$LOWER(temp1.field1)) OR ("<t_cvm_shipper"$LOWER(temp1.field1)) ;
			OR ("</t_ams_container"$LOWER(temp1.field1)) OR ("<t_cvm_consignee"$LOWER(temp1.field1)) ;
			OR ("</amsout"$LOWER(temp1.field1))
		lContinue = .F.
		EXIT
	ENDIF

	DO CASE
	CASE LOWER(temp1.field1) = "<ams_hbl_id"
		cReplField = "AMS_HBL_ID"
		cFieldType = "C"
	CASE LOWER(temp1.field1) = "<container_no"
		cReplField = "CONTAINER"
		cFieldType = "C"
	CASE LOWER(temp1.field1) = "<container_seal_no"
		cReplField = "CTRSEALNO"
		cFieldType = "C"
	CASE LOWER(temp1.field1) = "<container_equipment_code"
		cReplField = "CTREQPCD"
		cFieldType = "C"
	CASE LOWER(temp1.field1) = "<container_equipment_type"
		cReplField = "CTREQPTYPE"
		cFieldType = "C"
	CASE LOWER(temp1.field1) = "<container_service_code"
		cReplField = "CTRSVCCD"
		cFieldType = "C"
	CASE LOWER(temp1.field1) = "<hbl_value"
		cReplField = "HBL_VALUE"
		cFieldType = "N"
	CASE LOWER(temp1.field1) = "<weight"
		cReplField = "WEIGHT"
		cFieldType = "N"
	CASE LOWER(temp1.field1) = "<description"
		IF !"</description"$LOWER(temp1.field1)
			DO WHILE !("</description"$LOWER(temp1.field1))
				cValue = cValue + ALLTRIM(STRTRAN(STRTRAN(temp1.field1,"<description>",""),"</description>",""))+CHR(13)
				SKIP IN temp1
			ENDDO
			cValue = cValue + ALLTRIM(STRTRAN(STRTRAN(temp1.field1,"<description>",""),"</description>",""))+CHR(13)
		ELSE
			startpos = AT(">",temp1.field1,1)+1
			endpos = AT("<",temp1.field1,2)-1
			IF (endpos = -1) OR (startpos = -1) && Indicates an end-tagged single field, no value
				cValue = ""
			ELSE
				insertlen = (endpos-startpos)+1
				cValue = ALLTRIM(SUBSTR(temp1.field1,startpos,insertlen))
			ENDIF
		ENDIF
		cReplField = "DESC"
		cFieldType = "M"
	CASE LOWER(temp1.field1) = "<marking"
		IF !"</marking"$LOWER(temp1.field1)
			SKIP IN temp1
			cValue = ""
			DO WHILE !("</marking"$LOWER(temp1.field1))
				cValue = cValue + ALLTRIM(STRTRAN(STRTRAN(temp1.field1,"<marking>",""),"</marking>",""))+CHR(13)
				SKIP IN temp1
			ENDDO
			cValue = cValue + ALLTRIM(STRTRAN(STRTRAN(temp1.field1,"<marking>",""),"</marking>",""))+CHR(13)
		ELSE
			startpos = AT(">",temp1.field1,1)+1
			endpos = AT("<",temp1.field1,2)-1
			IF (endpos = -1) OR (startpos = -1) && Indicates an end-tagged single field, no value
				cValue = ""
			ELSE
				insertlen = (endpos-startpos)+1
				cValue = ALLTRIM(SUBSTR(temp1.field1,startpos,insertlen))
			ENDIF
		ENDIF
		cReplField = "MARKING"
		cFieldType = "M"
	CASE LOWER(temp1.field1) = "<manifest_unit_code"
		cReplField = "MFSTUNITCD"
		cFieldType = "C"
	CASE LOWER(temp1.field1) = "<country_code"
		cReplField = "COUNTRYCD"
		cFieldType = "C"
	CASE LOWER(temp1.field1) = "<fda_item_sw"
		cReplField = "FDA_ITEMSW"
		cFieldType = "L"
	OTHERWISE
		cRec = ALLTRIM(STR(RECNO('temp1')))
		WAIT WINDOW "Field: "+LOWER(temp1.field1)+" at Rec. #"+cRec+" is not assigned" TIMEOUT 1
*		SET STEP ON
		SKIP 1 IN temp1
		LOOP
	ENDCASE
	fieldextract(cReplField,cFieldType)
	IF !EOF()
		SKIP 1 IN temp1
	ENDIF
ENDDO
SELECT &cTable
*BROWSE

ENDPROC

*************************
PROCEDURE addrbkdn
*************************
PARAMETERS cBlockType
cTable = "efreightaddress"
WAIT WINDOW "Now running replacements to "+cTable+" ("+cBlockType+")" NOWAIT
SELECT &cTable
m.type = ""
STORE cBlockType TO m.type
APPEND BLANK
GATHER MEMVAR FIELDS TYPE,mbol,hbl_no,ams_hbl_id

SELECT temp1
SKIP 1 IN temp1

lContinue = .T.
DO WHILE lContinue
	IF ("</t_cvm_notify"$LOWER(temp1.field1)) OR ("</t_cvm_shipper"$LOWER(temp1.field1)) ;
			OR ("<t_ams_container"$LOWER(temp1.field1)) OR ("</t_cvm_consignee"$LOWER(temp1.field1)) ;
			OR ("</amsout"$LOWER(temp1.field1))
		lContinue = .F.
		EXIT
	ENDIF

	DO CASE
	CASE LOWER(temp1.field1) = "<cvm_address_id"
		cReplField = "CVMADDRID"
		cFieldType = "C"
	CASE LOWER(temp1.field1) = "<agent_code"
		cReplField = "AGENTCD"
		cFieldType = "C"
	CASE LOWER(temp1.field1) = "<cv_address_code"
		cReplField = "CVADDRCD"
		cFieldType = "C"
	CASE LOWER(temp1.field1) = "<company_name"
		cReplField = "COMPANY"
		cFieldType = "C"
	CASE LOWER(temp1.field1) = "<phone_area"
		cReplField = "PHONE_AREA"
		cFieldType = "C"
	CASE LOWER(temp1.field1) = "<phone"
		cReplField = "PHONE"
		cFieldType = "C"
	CASE LOWER(temp1.field1) = "<ein"
		cReplField = "EIN"
		cFieldType = "C"
	CASE LOWER(temp1.field1) = "<address1"
		cReplField = "ADDRESS1"
		cFieldType = "C"
	CASE LOWER(temp1.field1) = "<address2"
		cReplField = "ADDRESS2"
		cFieldType = "C"
	CASE LOWER(temp1.field1) = "<city"
		cReplField = "CITY"
		cFieldType = "C"
	CASE LOWER(temp1.field1) = "<postal_code"
*		ASSERT .f.
		cReplField = "POSTALCODE"
		cFieldType = "C"
	CASE LOWER(temp1.field1) = "<region"
		cReplField = "REGION"
		cFieldType = "C"
	CASE LOWER(temp1.field1) = "<country_code"
		cReplField = "COUNTRYCD"
		cFieldType = "C"
	CASE LOWER(temp1.field1) = "<sub_group_code"
		cReplField = "SUBGRPCD"
		cFieldType = "C"
	CASE LOWER(temp1.field1) = "<group_code"
		cReplField = "GROUPCD"
		cFieldType = "C"
	OTHERWISE
		IF EMPTY(ALLTRIM(temp1.field1))
			EXIT
		ENDIF
	OTHERWISE
		cRec = ALLTRIM(STR(RECNO('temp1')))
		WAIT WINDOW "Field: "+LOWER(temp1.field1)+" at Rec. #"+cRec+" is not assigned"
		SET STEP ON
		SKIP 1 IN temp1
		LOOP
	ENDCASE
	fieldextract(cReplField,cFieldType)
	IF !EOF()
		SKIP 1 IN temp1
	ENDIF
ENDDO
SELECT &cTable
*BROWSE

ENDPROC

*************************
PROCEDURE fieldextract
*************************
PARAMETERS cReplField,cFieldType
WAIT WINDOW "Now doing field replacements in "+cTable NOWAIT
*ASSERT .f.
SELECT &cTable

IF "/>"$temp1.field1
	cValue = ""
ELSE
	startpos = AT(">",temp1.field1,1)+1
	endpos = AT("<",temp1.field1,2)-1


	IF cFieldType # "M"
		IF (endpos = -1) OR (startpos = -1) && Indicates an end-tagged single field, no value
			cValue = ""
		ELSE
			insertlen = (endpos-startpos)+1
			cValue = ALLTRIM(SUBSTR(temp1.field1,startpos,insertlen))
			cValue = STRTRAN(cValue,"&amp;","&")
		ENDIF
	ENDIF
ENDIF
SELECT &cTable

IF cTable = "efreightmain"
	DO CASE
	CASE cReplField = "MBOL"
		STORE cValue TO m.mbol
*	WAIT WINDOW "NOTICE: Storing value for "+cReplField+": "+m.mbol TIMEOUT 1
	CASE cReplField = "HBL_NO"
		STORE cValue TO m.hbl_no
*	WAIT WINDOW "NOTICE: Storing value for "+cReplField+": "+m.hbl_no TIMEOUT 1
	CASE cReplField = "AMS_HBL_ID"
		STORE cValue TO m.ams_hbl_id
	ENDCASE
ENDIF

DO CASE
CASE INLIST(cFieldType,"C","M")
*	INSERT INTO (cTable) (&cReplField) VALUES (cValue)
	REPLACE (cReplField) WITH (cValue)
	cValue = ""

CASE cFieldType = "L"
	lValue = IIF(cValue="false",.F.,.T.)
	REPLACE (cReplField) WITH (lValue)
	RELEASE ALL LIKE lValue

CASE cFieldType = "N"
	nValue = VAL(cValue)
*	INSERT INTO (cTable) (&cReplField) VALUES (nValue)
	REPLACE (cReplField) WITH (nValue)
	RELEASE ALL LIKE nValue

CASE cFieldType = "D"
	REPLACE (cReplField) WITH (cValue)
*	INSERT INTO (cTable) (&cReplField) VALUES (cValue)  && Character value
	cReplField = STRTRAN(cReplField,"DTC","DTM")
	SET DECIMALS TO 0
	nYear = VAL(LEFT(cValue,4))
	nMon = VAL(SUBSTR(cValue,6,2))
	nDay = VAL(SUBSTR(cValue,9,2))
	nHour = VAL(SUBSTR(cValue,12,2))
	nMin = VAL(SUBSTR(cValue,15,2))
	nSec = VAL(SUBSTR(cValue,18,2))
	dValue = DATETIME(nYear,nMon,nDay,nHour,nMin,nSec)
*	INSERT INTO (cTable) (&cReplField) VALUES (dValue)  && Datetime value
	REPLACE (cReplField) WITH (dValue)
	SET DECIMALS TO 2
	RELEASE ALL LIKE dValue

ENDCASE

cReplField = ""
SELECT temp1
ENDPROC
