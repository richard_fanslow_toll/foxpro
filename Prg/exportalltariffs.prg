if x3winmsg("Are you sure you want to export the tariffs for all accounts?",,"Y|N")="NO"
	return
endif

try
	md h:\tariff
catch
endtry

delete file h:\tariff\*.*

xsqlexec("select * from tariff","xdytariff",,"ar")

select xdyratehead
count to xdy for !inactive
i=0

scan for !inactive
	i=i+1
	wait window transform(i)+" of "+transform(xdy) nowait
	
	select xdytariff
	calculate max(zdate) to aa for rateheadid=xdyratehead.rateheadid
	if !empty(aa)
		locate for rateheadid=xdyratehead.rateheadid and zdate=aa
		if xdytariff.xls
			xext="xls"
		else
			xext="pdf"
		endif
		copy memo tariff to ("h:\tariff\"+strtran(strtran(strtran(strtran(strtran(strtran(trim(xdyratehead.acctname),"/"," "),".",""),"'",""),",",""),"-"," "),"?","")+"."+xext)
	endif
endscan

use in xdyratehead
use in xdytariff

clear
x3winmsg("Files exported to h:\tariff")