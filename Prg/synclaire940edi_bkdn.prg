*!* m:\dev\prg\synclaire940edi_bkdn.prg
CLEAR
WAIT WINDOW "Now in BKDN Phase..." NOWAIT

lCheckStyle = .T.
lPick = .F.
lPrepack = .T.
units = IIF(lPick,.T.,.F.)
STORE "" TO cUnitofMeas,m.linenum,cDesc,cInnerpack,cUnitstype,cUOM,cOrigWt,cUOW,cOrigVol,cUOV,cInsertPOColor
STORE "" TO lcStyle,lcColor,lcSize,lcQty,lcUPC,lcSKU,lcDesc

goffice="2"
gmasteroffice="C"

IF !lTesting
	useca("ackdata","wh")
ENDIF

SELECT x856
LOCATE
IF lTesting AND lBrowfiles
	BROWSE
ENDIF
LOCATE FOR TRIM(x856.segment) = "W20"
lW20 = IIF(FOUND(),.T.,.F.)
cShip_via2 = ""

LOCATE
COUNT FOR TRIM(segment) = "ST" TO nSegCnt
cSegCnt = ALLTRIM(STR(nSegCnt))

WAIT WINDOW "Now in 940 Breakdown" &message_timeout2
WAIT WINDOW "There are "+cSegCnt+" P/Ts in this file" &message_timeout2
SELECT x856
LOCATE

*!* Constants
m.acct_name = "SYNCLAIRE"
m.acct_num = nAcctNum
m.careof = " "
m.sf_addr1 = "25 NEWBRIDGE ROAD"
m.sf_addr2 = "SUITE 405"
m.sf_csz = "HICKSVILLE, NY 11801"
m.printed = .F.
m.print_by = ""
m.printdate = DATE()
m.custsku = ""
lJCPMail = .T.
*!* End constants

*!*  If !Used("uploaddet")
*!*        If lTesting
*!*              Use F:\ediwhse\test\uploaddet In 0 Alias uploaddet
*!*        Else
*!*              Use F:\ediwhse\Data\uploaddet In 0 Shared Alias uploaddet
*!*        Endif
*!*  Endif

IF USED('syncxref')
	USE IN syncxref
ENDIF
USE "f:\3pl\data\consolidator-xref" IN 0 ALIAS syncxref

SELECT xpt
DELETE ALL
SELECT xptdet
DELETE ALL

STORE "" TO m.isa_num,m.shipins,m.color,m.id
STORE 0 TO ptctr,pt_total_eaches,pt_total_cartons,nCtnCount

lcCurrentGroupNum = ""
CurrentISANum = ""

SELECT x856
SET FILTER TO
LOCATE

WAIT WINDOW "NOW IN INITIAL HEADER BREAKDOWN" &message_timeout2

ptidctr    =0
ptdetidctr =0

nISA = 0  && To check for embedded FA envelopes.
lJCP = .F.

DO WHILE !EOF("x856")
	cSegment = TRIM(x856.segment)
	cCode = TRIM(x856.f1)

	IF TRIM(x856.segment) = "ISA"
		IF nISA = 0
			nISA = 1
			m.isa_num = ALLTRIM(x856.f13)
			cISA_Num = ALLTRIM(x856.f13)
			SELECT x856
			SKIP
			LOOP
		ENDIF
	ENDIF

	IF (TRIM(x856.segment) = "GS")
		cgs_num = ALLTRIM(x856.f6)
		lcCurrentGroupNum = ALLTRIM(x856.f6)
		SELECT x856
		SKIP
		LOOP
	ENDIF

	IF TRIM(x856.segment) = "ST" AND TRIM(x856.f1) = "940"
		m.groupnum=lcCurrentGroupNum
		m.isanum=cISA_Num
		m.transnum=x856.f2
		m.edicode="OW"
		m.accountid=6521
		m.loaddt=DATE()
		m.loadtime=DATETIME()
		m.filename=xfile
		IF !lTesting
			insertinto("ackdata","wh",.T.)
		ENDIF
		SELECT x856
		SKIP
		LOOP
	ENDIF

	IF (TRIM(x856.segment) = "W05")
		lAddPrintstuff = .F.
		m.ship_ref = ALLTRIM(x856.f2)
		STORE m.ship_ref TO cShip_ref
		lSMUL = IIF("SMUL"$cShip_ref,.T.,.F.)
		STORE m.ship_ref TO cShip_refOrig

		lDoLineInsert = .T.
		ptdetctr = 0
		ptidctr  = ptidctr    +1
		ptctr = ptctr +1
		cCustnotes = ""
		m.vendor_num = ""
		nCtnCount = 0
		nSHCount = 1
		nDelCount = 1
		nDetCnt = 0
		IF !lTesting
			REPLACE ACKDATA.ship_ref WITH ALLTRIM(x856.f2) IN ACKDATA
		ENDIF
		SELECT xpt
		APPEND BLANK
		REPLACE xpt.ship_ref   WITH m.ship_ref IN xpt  && Pick Ticket
		IF lTesting
*      BROWSE NOWAIT
		ENDIF

		REPLACE xpt.accountid WITH nAcctNum IN xpt
		REPLACE xpt.shipins WITH "FILENAME*"+cFilename IN xpt
		REPLACE xpt.qty WITH 0 IN xpt
		REPLACE xpt.origqty WITH 0 IN xpt
		STORE 0 TO pt_total_eaches
		STORE 0 TO pt_total_cartons
		WAIT WINDOW AT 10,10 "Now processing PT# "+ ALLTRIM(x856.f2)+" Number "+CHR(13)+TRANSFORM(ptctr)+" of "+cSegCnt NOWAIT
		REPLACE xpt.ptid       WITH ptidctr IN xpt
		REPLACE xpt.ptdate     WITH DATE() IN xpt
		lSplitPT = .F.

		IF SEEK(m.ship_ref,'ptdata','ship_ref')
			IF ptdata.splitpt
				lSplitPT = .T.
			ENDIF
		ENDIF
		nUnitID = 0
		IF lSplitPT
			nUnitID = xpt.ptid
		ENDIF
		m.cnee_ref = ALLTRIM(x856.f3)
		cInsertPOColor = IIF(lSMUL,m.cnee_ref,"")
		REPLACE xpt.cnee_ref WITH m.cnee_ref IN xpt  && PO#
		m.pt = ALLTRIM(x856.f2)
		REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"RELEASENUM*"+ALLTRIM(x856.f5) IN xpt
		REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"W0504*"+ALLTRIM(x856.f4) IN xpt  && Added per Wei, to go back in the W0608 element, if present.
		SELECT x856
		SKIP 1
		LOOP
	ENDIF

	IF TRIM(x856.segment) = "N1" AND INLIST(TRIM(x856.f1),"ST") && Ship-to data
		lN2 = .F.
		cConsignee = UPPER(TRIM(x856.f2))  && sometimes the ship-to is a DC or Unit name So use the BT r BY for the Consignee
		lSyncWhls = IIF(cConsignee = "SYNCLAIRE WHOLESALE",.T.,.F.)
		m.st_name = UPPER(TRIM(x856.f2))

		REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"SHIPTO*"+STRTRAN(cConsignee,"'","`") IN xpt
		REPLACE xpt.consignee WITH cConsignee IN xpt

		cDCNum = ALLTRIM(x856.f4)
		REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"STORENUM*"+cDCNum IN xpt && Full-length DC Number
		REPLACE xpt.dcnum WITH RIGHT(cDCNum,10) IN xpt


		cStorenum1 = ALLTRIM(cDCNum)
		IF !ISALPHA(cStorenum1)
			nStoreNum = INT(VAL(LEFT(cStorenum1,5)))
		ELSE
			nStoreNum = 0
		ENDIF

		lCons = .F.
		IF ("BUY"$m.st_name AND "BABY"$m.st_name) OR ("BBB"$m.st_name) && BUY BUY BABY/BBB
			ASSERT .F. MESSAGE "At store number load...debug"
			lCons = IIF(SEEK(nStoreNum,'syncxref','storenum'),.T.,.F.)
		ENDIF

		REPLACE xpt.storenum WITH nStoreNum IN xpt
		RELEASE cStorenum1

		SKIP 1 IN x856
		IF TRIM(x856.segment) = "N2"
			IF lSyncWhls
				REPLACE xpt.consignee WITH "SYNWH-"+ALLTRIM(x856.f1) IN xpt
				REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"N2NAME*"+ALLTRIM(x856.f1) IN xpt
				SKIP 1 IN x856
			ELSE
				SKIP 1 IN x856
			ENDIF
		ENDIF

		IF TRIM(x856.segment) = "N3"  && address info
			m.st_addr1 = UPPER(TRIM(x856.f1))
			m.st_addr2 = UPPER(TRIM(x856.f2))
			REPLACE xpt.address  WITH m.st_addr1 IN xpt
			REPLACE xpt.address2 WITH IIF(lN2,cConsignee,m.st_addr2) IN xpt
		ENDIF
		SKIP 1 IN x856
		IF TRIM(x856.segment) = "N4"  && address info
			IF !EMPTY(TRIM(x856.f3))
				m.zipbar = TRIM(x856.f3)
			ENDIF
			IF !EMPTY(TRIM(x856.f2))
				m.st_csz = UPPER(TRIM(x856.f1))+", "+UPPER(TRIM(x856.f2))+" "+TRIM(x856.f3)
			ELSE
				m.st_csz = UPPER(TRIM(x856.f1))
			ENDIF
			REPLACE xpt.csz WITH m.st_csz IN xpt
			cCountry = ALLT(x856.f4)
			REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"COUNTRY*"+cCountry IN xpt
		ENDIF
		SELECT x856
		SKIP
		LOOP
	ENDIF

	IF ALLTRIM(x856.segment) = "PER"   && Added per Tom and Synclaire, 11.09.2017, Joe
		IF ALLTRIM(x856.f1) = "RE" AND !EMPTY(ALLTRIM(x856.f2))
			REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"RESPONDENT*"+ALLTRIM(x856.f2) IN xpt
		ENDIF
		IF ALLTRIM(x856.f3) = "TE" AND !EMPTY(ALLTRIM(x856.f4))
			REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"TELEPHONE*"+ALLTRIM(x856.f4) IN xpt
		ENDIF
		IF ALLTRIM(x856.f5) = "EM" AND !EMPTY(ALLTRIM(x856.f6))
			REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"EMAIL*"+ALLTRIM(x856.f6) IN xpt
		ENDIF
	ENDIF

	IF TRIM(x856.segment) = "N1" AND TRIM(x856.f1)="SF" AND INLIST(TRIM(x856.f3),"1","9")
		REPLACE xpt.duns WITH ALLTRIM(x856.f4) IN xpt
		REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"DUNSQUAL*"+ALLTRIM(x856.f3) IN xpt
		REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"DUNSNUMBER*"+ALLTRIM(x856.f4) IN xpt
		SELECT x856
		SKIP
		LOOP
	ENDIF

	IF TRIM(x856.segment) = "N1" AND ALLTRIM(x856.f1) = "BT" && load bill toinfo for Macys packing list
		REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"BILLTONAME*"+TRIM(x856.f2) IN xpt
		SKIP 1 IN x856
		IF TRIM(x856.segment) = "N2"
			REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"BILLTOADDR*"+TRIM(x856.f1) IN xpt
			SKIP 1 IN x856
			IF TRIM(x856.segment) = "N4"  && Billto city,state,zip info
				IF !EMPTY(TRIM(x856.f3))
					xxzipbar = TRIM(x856.f3)
				ENDIF
				IF !EMPTY(TRIM(x856.f2))
					xxst_csz = UPPER(TRIM(x856.f1))+", "+UPPER(TRIM(x856.f2))+" "+TRIM(x856.f3)
				ELSE
					xxst_csz = UPPER(TRIM(x856.f1))
				ENDIF
				REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"BILLTOCSZ*"+TRIM(xxst_csz) IN xpt
			ENDIF
		ELSE
			SKIP -1 IN x856
		ENDIF
		SELECT x856
		SKIP
		LOOP
	ENDIF

	IF TRIM(x856.segment) = "N1" AND ALLTRIM(x856.f1) = "BY" && look for the Consignee as the Shipto might be a DC name
		nStoreNum2 = 0
		cStoreNum2 = ALLTRIM(x856.f4)
		IF !ISALPHA(cStoreNum2)
			nStoreNum2 = INT(VAL(LEFT(cStoreNum2,5)))
		ELSE
			nStoreNum2 = 0
		ENDIF
		REPLACE xpt.storenum WITH nStoreNum2 IN xpt
		REPLACE xpt.sforstore WITH RIGHT(ALLTRIM(x856.f4),10) IN xpt
		REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"SFSTORENUM*"+TRIM(x856.f4) IN xpt
		REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"BYNAME*"+TRIM(x856.f2) IN xpt
		REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"N1-BY*"+TRIM(x856.f4) IN xpt
		SELECT x856
		SKIP
		LOOP
	ENDIF

	IF TRIM(x856.segment) = "N1" AND INLIST(TRIM(x856.f1),"Z7") && Ship-for data
		csfor_name = TRIM(x856.f2)
		csfor_name = STRTRAN(csfor_name," - ","-")
		csfor_name = STRTRAN(csfor_name,", ",",")
		csfor_name = STRTRAN(csfor_name,"#","")

		REPLACE xpt.shipfor WITH UPPER(csfor_name) IN xpt
		csforStoreNum = ALLTRIM(x856.f4)
		REPLACE xpt.sforstore WITH csforStoreNum IN xpt
		SKIP 1 IN x856
		IF TRIM(x856.segment) = "N3"  && address info
			m.sforaddr1 = UPPER(TRIM(x856.f1))
			m.sforaddr2 = UPPER(TRIM(x856.f2))
			REPLACE xpt.sforaddr1 WITH m.sforaddr1 IN xpt
			REPLACE xpt.sforaddr2 WITH m.sforaddr2 IN xpt
		ENDIF
		SKIP 1 IN x856
		IF TRIM(x856.segment) = "N4"  && address info
			IF !EMPTY(TRIM(x856.f2))
				m.sforcsz = UPPER(TRIM(x856.f1))+", "+UPPER(TRIM(x856.f2))+" "+TRIM(x856.f3)
			ELSE
				m.sforcsz = UPPER(TRIM(x856.f1))
			ENDIF
			REPLACE xpt.sforcsz WITH m.sforcsz IN xpt
			cCountry = ALLT(x856.f4)
			IF !EMPTY(cCountry)
				REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"SFCOUNTRY*"+cCountry IN xpt
			ENDIF
		ENDIF
		SELECT x856
		SKIP
		LOOP
	ENDIF

	IF TRIM(x856.segment) = "N9"
		DO CASE
			CASE TRIM(x856.f1) = "DP"
				m.dept = ALLT(x856.f2)
*        cFOB = ALLT(x856.f3)
				REPLACE xpt.dept WITH m.dept IN xpt  && Dept
				cDeptName = ALLT(x856.f3)
				REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"DEPT*"+m.dept IN xpt
				REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"DEPTNAME*"+cDeptName IN xpt
*        REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"FOB*"+cFOB IN xpt   && removed 6/25/14 TM as per Wei

			CASE TRIM(x856.f1) = "IA"
				REPLACE xpt.vendor_num WITH ALLT(x856.f2)
				REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"VENDORNUM*"+ALLT(x856.f2) IN xpt
				REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"PAYMENT*"+ALLT(x856.f2) IN xpt

			CASE TRIM(x856.f1) = "SN"
				REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"SNNUM*"+ALLT(x856.f2) IN xpt

			CASE TRIM(x856.f1) = "PK"
				REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"PKNUM*"+ALLT(x856.f2) IN xpt

			CASE TRIM(x856.f1) = "VN"
				REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"VNINFO*"+ALLT(x856.f2) IN xpt

			CASE TRIM(x856.f1) = "VI"
				REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"VINUM*"+ALLT(x856.f2) IN xpt

			CASE TRIM(x856.f1) = "TP"
				lAddPrintstuff = IIF(UPPER(x856.f2) = "SAM E",.T.,lAddPrintstuff)
				lAddPrintstuff = IIF(UPPER(x856.f2) = "DILLARDS",.T.,lAddPrintstuff)  && 3/27/2018 MB to have color and size stored in printstuff for Dillard's
				REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"TPINFO*"+ALLT(x856.f2) IN xpt

			CASE TRIM(x856.f1) = "SCA"
				REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"SCAINFO*"+ALLT(x856.f2) IN xpt
				cShip_via2 = ALLT(x856.f2)

			CASE TRIM(x856.f1) = "DTM"
				REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"DTMINFO*"+ALLT(x856.f2) IN xpt

			CASE TRIM(x856.f1) = "ST"
				REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"STINFO*"+STRTRAN(ALLT(x856.f2),"'","`") IN xpt

			CASE TRIM(x856.f1) = "WH"
				REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"WHINFO*"+ALLT(x856.f2) IN xpt

			CASE TRIM(x856.f1) = "AT"
				REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"ATINFO*"+ALLT(x856.f2) IN xpt

			CASE TRIM(x856.f1) = "TI"
				REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"EDI-INFO*"+ALLT(x856.f2) IN xpt

&& 3 more added 1/25/2017  PG

			CASE TRIM(x856.f1) = "W9"
				REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"MSG1*"+ALLT(x856.f2) IN xpt

      CASE TRIM(x856.f1) = "GW"
        REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"GIFTWRAP*"+ALLT(x856.f2) IN xpt

			CASE TRIM(x856.f1) = "RM"
				REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"RETURNMETHOD*"+ALLT(x856.f2) IN xpt

			CASE TRIM(x856.f1) = "RP"
				REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"RETURNTIME*"+ALLT(x856.f2) IN xpt

			CASE TRIM(x856.f1) = "FOB"
				REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"FOB*"+ALLT(x856.f2) IN xpt
				REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"GROUPNO*"+ALLT(x856.f3) IN xpt

			CASE TRIM(x856.f1) = "14"
				REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"SERV_LVL*"+ALLT(x856.f2) IN xpt

			CASE TRIM(x856.f1) = "4F"
				REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"ACCTNUM*"+ALLT(x856.f2) IN xpt
				REPLACE xpt.upsacctnum WITH ALLT(x856.f2) IN xpt

&& and then even more added 4/17/2017  PG for SAKs pack list
			CASE TRIM(x856.f1) = "LA"
				REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"TRANSNUMBER*"+ALLT(x856.f2) IN xpt

			CASE TRIM(x856.f1) = "19"
				REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"ORDERTYPE*"+ALLT(x856.f2) IN xpt

			CASE TRIM(x856.f1) = "4N"
				REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"PAYTYPE*"+ALLT(x856.f2) IN xpt

			CASE TRIM(x856.f1) = "W9"
				REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"GIFTORDER*"+UPPER(ALLT(x856.f3)) IN xpt

			CASE TRIM(x856.f1) = "E9"
				REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"E9STRING*"+ALLT(x856.f2)+ALLT(x856.f3) IN xpt

			CASE TRIM(x856.f1) = "CO"
				REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"CUSTORDERNUMBER*"+UPPER(ALLT(x856.f3)) IN xpt




*!*        CASE TRIM(x856.f1) = "SMU" AND TRIM(x856.f2) = "Y"
*!*          IF !"SMU"$xpt.ship_ref
*!*            REPLACE xpt.ship_ref   WITH ALLTRIM(xpt.ship_ref)+"-SMU" IN xpt
*!*          ENDIF

			CASE TRIM(x856.f1) = "TS"
				REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"TAXRATE*"+ALLT(x856.f2) IN xpt

			CASE TRIM(x856.f1) = "RN"
				REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"RESNUM*"+ALLT(x856.f2) IN xpt

			CASE TRIM(x856.f1) = "GW"
				REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"GIFTWRAP*YES" IN xpt

			CASE TRIM(x856.f1) = "DQ"
				REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"DELCHARGE*"+ALLT(x856.f2) IN xpt

			CASE TRIM(x856.f1) = "TX"
				REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"SALESTAX*"+ALLT(x856.f2) IN xpt
		ENDCASE
		SELECT x856
		SKIP
		LOOP
	ENDIF

	IF TRIM(x856.segment) = "G62" AND TRIM(x856.f1) == "37"  && Start date
		lcDate = ALLTRIM(x856.f2)
		lxDate = SUBSTR(lcDate,5,2)+"/"+SUBSTR(lcDate,7,2)+"/"+LEFT(lcDate,4)
		ldDate = CTOD(lxDate)
		REPLACE xpt.START WITH ldDate IN xpt
		SELECT x856
		SKIP
		LOOP
	ENDIF

	IF TRIM(x856.segment) = "G62" AND TRIM(x856.f1) == "38"  && cancel date
		lcDate = ALLTRIM(x856.f2)
		lxDate = SUBSTR(lcDate,5,2)+"/"+SUBSTR(lcDate,7,2)+"/"+LEFT(lcDate,4)
		ldDate = CTOD(lxDate)
		REPLACE xpt.CANCEL WITH ldDate IN xpt
		SELECT x856
		SKIP
		LOOP
	ENDIF

	IF TRIM(x856.segment) = "NTE"  && Warehouse handling notes
		IF TRIM(x856.f1) = "COM"
			REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"COMPANY*"+ALLTRIM(x856.f2) IN xpt
		ELSE
			m.sh = "SPINS"+ALLT(STR(nSHCount))+"*"+ALLTRIM(x856.f2)  && ADDED THE ENUMERATION OF THE NOTES 4/17/2008
			nSHCount = nSHCount + 1
			REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+m.sh IN xpt
		ENDIF
		SELECT x856
		SKIP
		LOOP
	ENDIF

	IF TRIM(x856.segment) = "W66" && usually, ship_via info
		REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"PMT_TYPE*"+ALLTRIM(x856.f1) IN xpt
		cShip_via = ALLT(x856.f5)
		cShip_via = IIF(lCons,LEFT(cShip_via,26)+"-CONS",cShip_via)  &&  Added 07.07.2014 per Maria R.

		REPLACE xpt.ship_via WITH cShip_via IN xpt
		IF !EMPTY(x856.f5)
			m.sh = "SPINS"+ALLT(STR(nSHCount))+"*SHIP VIA: "+ALLTRIM(x856.f5)  &&
			nSHCount = nSHCount + 1
			REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+m.sh
		ENDIF
		IF ALLTRIM(x856.f1)="TP"
			REPLACE xpt.ship_via WITH ALLTRIM(xpt.ship_via)+"-THIRD PARTY" IN xpt
		ENDIF
		IF ALLTRIM(x856.f1)="CC"
			REPLACE xpt.ship_via WITH ALLTRIM(xpt.ship_via)+"-COLLECT" IN xpt
		ENDIF
		IF ALLTRIM(x856.f1)="PP"
			REPLACE xpt.ship_via WITH ALLTRIM(xpt.ship_via)+"-PREPAID" IN xpt
		ENDIF

		SELECT x856
		SKIP
		LOOP
	ENDIF

	IF TRIM(x856.segment) = "LX" && start of PT detail, stay here until this PT is complete
		WAIT "NOW IN DETAIL LOOP PROCESSING" WINDOW NOWAIT
		SELECT xptdet
		CALCULATE MAX(ptdetid) TO m.ptdetid
		m.ptid = xpt.ptid
		m.ptdetid = m.ptdetid + 1
		m.printstuff = ""
		m.accountid = xpt.accountid
		SKIP 1 IN x856

		m.printstuff2=""
		m.printstuff3 = ""
		DO WHILE .T.  &&ALLTRIM(x856.segment) != "SE"
			DO CASE
				CASE TRIM(x856.segment) = "LX"
					m.ptdetid = m.ptdetid + 1
					SELECT xpt
					SCATTER MEMVAR MEMO BLANK FIELDS EXCEPT ptdetid,ptid,accountid

				CASE TRIM(x856.segment) = "W01" && Destination Quantity
					SELECT x856
					SKIP 1 IN x856
					lNoDetPrepack = IIF(x856.f1 # "UP",.T.,.F.)
					SKIP -1 IN x856
					cUOM = ""
					cUOM = ALLTRIM(x856.f2)
					m.printstuff = "FILENAME*"+cFilename
					m.printstuff = m.printstuff+CHR(13)+"ORIGQTY*"+ALLTRIM(x856.f1)
					m.printstuff = m.printstuff+CHR(13)+"UOM*"+cUOM

					IF (INLIST(cUOM,"CA","CS","AS")  OR ALLTRIM(x856.f4) = "PK") && added this because sometimes full casesa come in as either "CA" or "CS"
						lPrepack = .T.
						m.units = .F.
					ELSE
						lPrepack = .F.
						m.units = .T.
					ENDIF

*m.units = !lPrepack

					IF lSplitPT  && Only in Synclaire
						lPrepack = IIF(!INLIST(cUOM,"EA","PR") OR ALLTRIM(x856.f4) = "PK",.T.,.F.)
						m.units = !lPrepack
					ENDIF
					lcQty     = ALLTRIM(x856.f1)

					DO CASE
						CASE ALLTRIM(x856.f4) = "UP"
							lcUPC   = ALLTRIM(x856.f5)
						CASE ALLTRIM(x856.f6) = "UP"
							lcUPC   = ALLTRIM(x856.f7)
						CASE ALLTRIM(x856.f15) = "UP"
							lcUPC   = ALLTRIM(x856.f16)
					ENDCASE

					lcStyle = ""
					DO CASE
						CASE ALLTRIM(x856.f4) = "VN"
							lcStyle   = ALLTRIM(x856.f5)
						CASE ALLTRIM(x856.f6) = "VN"
							lcStyle   = ALLTRIM(x856.f7)
						CASE ALLTRIM(x856.f15) = "VN"
							lcStyle   = ALLTRIM(x856.f16)
					ENDCASE
					IF nAcctNum = 6521 AND !EMPTY(lcStyle)
						m.printstuff = IIF(EMPTY(m.printstuff),"ORIGSTYLE*"+lcStyle,m.printstuff+CHR(13)+"ORIGSTYLE*"+lcStyle)
					ENDIF

					lcStyle   = lcUPC

					lcCustSKU = ""
					DO CASE
						CASE ALLTRIM(x856.f4) = "IN"
							lcCustSKU   = ALLTRIM(x856.f5)
						CASE ALLTRIM(x856.f6) = "IN"
							lcCustSKU   = ALLTRIM(x856.f7)
						CASE ALLTRIM(x856.f15) = "IN"
							lcCustSKU   = ALLTRIM(x856.f16)
						OTHERWISE
							lcCustSKU   = "UNK"
					ENDCASE

					IF lPrepack
						lcPack = "1"
						IF ALLTRIM(x856.f4) = "PK"
							lcPack  = ALLTRIM(x856.f5)
							m.printstuff = IIF(EMPTY(m.printstuff),"PKPACK*"+lcPack,m.printstuff+CHR(13)+"PKPACK*"+lcPack)
						ENDIF
					ENDIF

					m.totqty = INT(VAL(x856.f1))

					nRec1 = RECNO('x856')

					IF !lSplitPT
						IF lPrepack   && Standard Prepacks
							INSERT INTO xptdet (ptid,ptdetid,accountid,ship_ref,STYLE,COLOR,PACK,units,totqty,origqty,upc,custsku) ;
								VALUES (xpt.ptid,m.ptdetid,xpt.accountid,xpt.ship_ref,lcUPC,cInsertPOColor,lcPack,.F.,VAL(lcQty),VAL(lcQty),lcUPC,lcCustSKU)
							IF lNoDetPrepack
								m.printstuff = m.printstuff+CHR(13)+"ORIGUPC*"+lcUPC
							ENDIF
							REPLACE xptdet.printstuff WITH m.printstuff IN xptdet
							REPLACE xptdet.units WITH .F. IN xptdet
							SKIP 1 IN x856
							IF ALLT(x856.segment) = "N9" AND ALLT(x856.f1) # "V"
								detailprepackfill()
							ELSE
								SKIP-1 IN x856
							ENDIF
						ELSE
							cShip_ref = IIF(lSplitPT AND !"%"$xpt.ship_ref, ALLT(xpt.ship_ref)+" %U",cShip_ref)
							INSERT INTO xptdet (ptid,ptdetid,accountid,ship_ref,STYLE,COLOR,PACK,units,totqty,origqty,upc,custsku) ;
								VALUES (IIF(lSplitPT,nUnitID,xpt.ptid),m.ptdetid,xpt.accountid,IIF(lSplitPT,cShip_ref,xpt.ship_ref),lcStyle,cInsertPOColor,"1",m.units,VAL(lcQty),VAL(lcQty),lcUPC,lcCustSKU)
							m.printstuff = m.printstuff+CHR(13)+"ORIGUPC*"+lcUPC
							REPLACE xptdet.printstuff WITH m.printstuff IN xptdet
							REPLACE xptdet.units WITH .T. IN xptdet
						ENDIF
					ELSE
						IF !lPrepack && PnP detail
							cShip_ref = IIF(lSplitPT AND !"%"$xpt.ship_ref, ALLT(xpt.ship_ref)+" %U",cShip_ref)
							INSERT INTO xptdet (ptid,ptdetid,accountid,ship_ref,STYLE,COLOR,PACK,units,totqty,origqty,upc,custsku) ;
								VALUES (IIF(lSplitPT,nUnitID,xpt.ptid),m.ptdetid,xpt.accountid,IIF(lSplitPT,cShip_ref,xpt.ship_ref),lcStyle,cInsertPOColor,"1",m.units,VAL(lcQty),VAL(lcQty),lcUPC,lcCustSKU)
							m.printstuff = m.printstuff+CHR(13)+"ORIGUPC*"+lcUPC
							REPLACE xptdet.printstuff WITH m.printstuff IN xptdet
							REPLACE xptdet.units WITH .T. IN xptdet
						ELSE
							IF lTesting
								WAIT WINDOW "At a prepack load" TIMEOUT 1
							ENDIF
							IF lDoLineInsert
								lDoLineInsert = .F.
								SELECT xpt
								nCtnID = xpt.ptid
								REPLACE xpt.ship_ref WITH ALLTRIM(xpt.ship_ref)+" %U" IN xpt
								m.ptid = xpt.ptid+1
								SCATTER FIELDS EXCEPT ptid MEMO MEMVAR
								APPEND BLANK
								GATHER MEMVAR MEMO
								nCtnID = 0
								nCtnID = xpt.ptid
								REPLACE xpt.ship_ref WITH STRTRAN(ALLTRIM(xpt.ship_ref),"%U","%C") IN xpt
							ENDIF
							SELECT x856
*SKIP 1 IN x856
							INSERT INTO xptdet (ptid,ptdetid,accountid,ship_ref,STYLE,PACK,units,totqty,origqty,upc,custsku) ;
								VALUES (IIF(lSplitPT,nCtnID,xpt.ptid),m.ptdetid,xpt.accountid,xpt.ship_ref,lcUPC,lcPack,.F.,VAL(lcQty),VAL(lcQty),lcUPC,lcCustSKU)
							REPLACE xptdet.units WITH .F. IN xptdet
							IF !lNoDetPrepack
								detailprepackfill()
							ELSE
								m.printstuff = m.printstuff+CHR(13)+"ORIGUPC*"+lcUPC
								REPLACE xptdet.printstuff WITH m.printstuff IN xptdet
							ENDIF
							ptidctr = ptidctr+1
						ENDIF
					ENDIF

				CASE ALLTRIM(x856.segment) = "G69"
					REPLACE xptdet.printstuff WITH ALLT(xptdet.printstuff)+CHR(13)+"DESC*"+ALLTRIM(x856.f1) IN xptdet
					m.printstuff2  = m.printstuff2+CHR(13)+"DESC*"+ALLTRIM(x856.f1)
					m.printstuff3 = "DESC*"+ALLTRIM(x856.f1)

		        CASE ALLTRIM(x856.segment) = "N9" AND ALLTRIM(x856.f1) = "POP"
		          REPLACE xptdet.printstuff WITH ALLT(xptdet.printstuff)+CHR(13)+"POP*"+ALLTRIM(x856.f2) IN xptdet

				CASE ALLTRIM(x856.segment) = "N9" AND ALLTRIM(x856.f1) = "LI"
					m.printstuff2 = m.printstuff2+CHR(13)+"LINENUM*"+ALLTRIM(x856.f2)
					REPLACE xptdet.linenum WITH ALLTRIM(x856.f2) IN xptdet

				CASE ALLTRIM(x856.segment) = "N9" AND ALLTRIM(x856.f1) = "VSZ"
					m.printstuff2 = m.printstuff2+CHR(13)+"ORIGSIZE*"+ALLTRIM(x856.f2)
					m.printstuff3 = m.printstuff3+CHR(13)+"ORIGSIZE*"+ALLTRIM(x856.f2)

				CASE ALLTRIM(x856.segment) = "N9" AND ALLTRIM(x856.f1) = "VCL"
					m.printstuff2 = m.printstuff2+CHR(13)+"ORIGCOLOR*"+ALLTRIM(x856.f2)
					m.printstuff3 = m.printstuff3+CHR(13)+"ORIGCOLOR*"+ALLTRIM(x856.f2)
					IF lAddPrintstuff
						REPLACE xptdet.printstuff WITH ALLT(xptdet.printstuff)+CHR(13)+CHR(13)+m.printstuff3 IN xptdet
						m.printstuff3 = ""
					ENDIF

				CASE TRIM(x856.segment) = "N9" AND UPPER(ALLTRIM(x856.f1))="PTEXT1"
					REPLACE xptdet.printstuff WITH xptdet.printstuff+CHR(13)+"SALUTATION1*"+UPPER(ALLTRIM(x856.f2)) IN xptdet

				CASE TRIM(x856.segment) = "N9"  AND UPPER(ALLTRIM(x856.f1))="PTEXT2"
					REPLACE xptdet.printstuff WITH xptdet.printstuff+CHR(13)+"SALUTATION2*"+UPPER(ALLTRIM(x856.f2)) IN xptdet

				CASE TRIM(x856.segment) = "N9"  AND UPPER(ALLTRIM(x856.f1))="PRETURNFLAG"
					REPLACE xptdet.printstuff WITH xptdet.printstuff+CHR(13)+"RETURNFLAG*"+UPPER(ALLTRIM(x856.f2))  IN xptdet

				CASE TRIM(x856.segment) = "AMT"  AND UPPER(ALLTRIM(x856.f1))="F7"
					REPLACE xptdet.printstuff WITH xptdet.printstuff+CHR(13)+"TAXVALUE*"+UPPER(ALLTRIM(x856.f2)) IN xptdet

				CASE TRIM(x856.segment) = "AMT"  AND UPPER(ALLTRIM(x856.f1))="OH"
					REPLACE xptdet.printstuff WITH xptdet.printstuff+CHR(13)+"HANDLINGVALUE*"+UPPER(ALLTRIM(x856.f2)) IN xptdet

				CASE ALLTRIM(x856.segment) = "N9" AND ALLTRIM(x856.f1) = "PR"
					REPLACE xptdet.printstuff WITH ALLT(xptdet.printstuff)+CHR(13)+"PRICE*"+ALLTRIM(x856.f2) IN xptdet

				CASE ALLT(x856.segment) = "XXSE"  && see below, PG SE from W20
					#IF 0
						IF !lPrepack AND !lSplitPT && PnP detail
							cShip_ref = IIF(lSplitPT AND !"%"$xpt.ship_ref, ALLT(xpt.ship_ref)+" %U",cShip_ref)
							INSERT INTO xptdet (ptid,ptdetid,accountid,ship_ref,STYLE,PACK,units,totqty,origqty,upc,custsku) ;
								VALUES (IIF(lSplitPT,nUnitID,xpt.ptid),m.ptdetid,xpt.accountid,IIF(lSplitPT,cShip_ref,xpt.ship_ref),lcStyle,"1",m.units,VAL(lcQty),VAL(lcQty),lcUPC,lcCustSKU)
							m.printstuff = m.printstuff+CHR(13)+"ORIGUPC*"+lcUPC
							REPLACE xptdet.printstuff WITH m.printstuff IN xptdet
							REPLACE xptdet.units WITH .T. IN xptdet
						ENDIF
						EXIT
					#ENDIF

				CASE ALLTRIM(x856.segment) = "SE" && WAS W20, PG 02/06/2015 The code following was added on 08.06.2014 per Wei @ Spring Systems
					DO CASE
						CASE cUOM = "AS" && Assortments are the only UOM affected
							cInnerpack = ""
							cInnerpack = ALLTRIM(x856.f1)
							nInnerPack = INT(VAL(cInnerpack))
							REPLACE xptdet.printstuff WITH ALLT(xptdet.printstuff)+m.printstuff2+CHR(13)+"INNERPACK*"+cInnerpack IN xptdet
						CASE !lNoDetPrepack
							REPLACE xptdet.printstuff WITH ALLT(xptdet.printstuff)+m.printstuff2+CHR(13)+"INNERPACK*"+cInnerpack IN xptdet
					ENDCASE
					cInnerpack = ""
					EXIT  && added here

			ENDCASE
			SELECT x856
			IF !EOF()
				SKIP 1 IN x856
			ENDIF
		ENDDO
		SELECT xptdet
	ENDIF

	SELECT x856
	IF !EOF()
		SKIP 1 IN x856
	ENDIF
ENDDO

IF !lTesting
	tu("ackdata")
ENDIF

SELECT xpt
LOCATE

SCAN
	SELECT xptdet
	detqty =0

	SCAN FOR xptdet.ptid = xpt.ptid
		detqty = detqty + xptdet.totqty
	ENDSCAN
	REPLACE xpt.qty     WITH detqty IN xpt
	REPLACE xpt.origqty WITH detqty IN xpt
ENDSCAN

IF !lTesting
	dupcheck()  && If dupcheck fails, file will be deleted and no 940 upload will occur.
ENDIF

DO m:\dev\prg\setuppercase940

SELECT xpt
replace xpt.shipins WITH STRTRAN(xpt.shipins,"'","`")

IF lBrowfiles
	WAIT WINDOW "Now browsing XPT/XPTDET files" &message_timeout2
	SELECT xpt
	LOCATE
	BROWSE FOR !DELETED()
	SELECT xptdet
	LOCATE
	BROWSE FOR !DELETED()
	IF MESSAGEBOX("Do you wish to continue?",4+16+256,"CONTINUE")=7
		CANCEL
		EXIT
	ENDIF
ENDIF

WAIT cCustname+" Breakdown Round complete..." WINDOW &message_timeout2
IF lJCP
	DO jcpmail
ENDIF


set step On 

SELECT x856
WAIT CLEAR
RETURN

**********************
PROCEDURE jcpmail
**********************
	IF lJCPMail
		SELECT 0
		USE F:\3pl\DATA\mailmaster ALIAS mm SHARED
		LOCATE FOR edi_type = "MISC" AND taskname = "GENERAL"
		tsendtojcp = ALLTRIM(IIF(mm.use_alt,mm.sendtoalt,mm.sendto))
		tccjcp = ALLTRIM(IIF(mm.use_alt,mm.ccalt,mm.cc))
		USE IN mm
		cMailLoc = IIF(cOffice = "C","West",IIF(cOffice = "N","New Jersey","Florida"))
		tsubject= cCustname+" JCPenney Pickticket Upload: " +TTOC(DATETIME())
		tattach = ""
		tmessage = "Uploading Consignee: JCPenney Picktickets for "+cCustname
		tmessage = tmessage+CHR(13)+"Filename: "+cFilename
		tmessage = tmessage+CHR(13)+"Check PT table for correct UOM and BT number stored"
		tfrom ="Toll EDI Operations <toll-edi-ops@tollgroup.com>"
    Do sendmail_au With tsendtojcp,"noreply@tollgroup.com",tsubject,tccjcp,tattach,tmessage,""
           **Parameter lcTO, lcFrom,                lcSubject,lcCC,lcAttach,lcBody,lcBCC
		lJCPMail = .F.
	ENDIF
ENDPROC

***************************
PROCEDURE detailprepackfill
***************************
	SELECT xptdet
	ptdetidctr = ptdetidctr +1
	REPLACE xptdet.units      WITH .F.        IN xptdet
	REPLACE xptdet.printstuff WITH m.printstuff IN xptdet

	DO WHILE !INLIST(x856.segment,"SE","LX")
		IF TRIM(x856.segment) = "N9" AND TRIM(x856.f1) = "UP"
			cSubUPC = TRIM(x856.f2)
			cSubQty = TRIM(x856.f3)
			cSubGroup = TRIM(x856.f7)

			lensg = LEN(cSubGroup)
			runzq = OCCURS(">",cSubGroup)
			IF lensg > 0
				FOR zq = 1 TO runzq
					zq1 = ALLT(STR(zq))
					nPos&zq1 = AT(">",cSubGroup,zq)
				ENDFOR
				RELEASE ALL LIKE zq*
				STORE "" TO cStyleQual,cStyle
				cStyleQual = LEFT(cSubGroup,nPOS1-1)
				cStyle = IIF(runzq>1,SUBSTR(cSubGroup,nPOS1+1,nPOS2-nPOS1-1),SUBSTR(cSubGroup,nPOS1+1))
				STORE "" TO cColorQual,cColor,cSizeQual,cSize
				IF runzq > 1
					cColorQual = IIF(runzq>2,SUBSTR(cSubGroup,nPOS2+1,nPOS3-nPOS2-1),"")
					cColor = IIF(runzq>3,SUBSTR(cSubGroup,nPOS3+1,nPOS4-nPOS3-1),SUBSTR(cSubGroup,nPOS3+1))
					cSizeQual = IIF(runzq>3,SUBSTR(cSubGroup,nPOS4+1,nPOS5-nPOS4-1),"")
					cSize = IIF(runzq>4,SUBSTR(cSubGroup,nPOS5+1),"")
				ENDIF
			ENDIF
			REPLACE xptdet.printstuff WITH xptdet.printstuff+CHR(13)+CHR(13)+"SUBUPC*"+cSubUPC IN xptdet
			REPLACE xptdet.printstuff WITH xptdet.printstuff+CHR(13)+"SUBQTY*"+cSubQty IN xptdet

			IF !EMPTY(cSubGroup)
				REPLACE xptdet.printstuff WITH xptdet.printstuff+CHR(13)+"STYQUAL*"+cStyleQual IN xptdet
				REPLACE xptdet.printstuff WITH xptdet.printstuff+CHR(13)+"STYLE*"+cStyle IN xptdet
				IF !EMPTY(cColorQual)
					REPLACE xptdet.printstuff WITH xptdet.printstuff+CHR(13)+"COLQUAL*"+cColorQual IN xptdet
					REPLACE xptdet.printstuff WITH xptdet.printstuff+CHR(13)+"COLOR*"+cColor IN xptdet
				ENDIF
				IF !EMPTY(cSizeQual)
					REPLACE xptdet.printstuff WITH xptdet.printstuff+CHR(13)+"SZQUAL*"+cSizeQual IN xptdet
					REPLACE xptdet.printstuff WITH xptdet.printstuff+CHR(13)+"SIZE*"+cSize IN xptdet
				ENDIF
			ENDIF
			IF lTesting
				cFullDet = "FULLDET*N9*UP*"+TRIM(x856.f2)+"*"+TRIM(x856.f3)+"*"+TRIM(x856.f4)+"*"+TRIM(x856.f5)+"*"+TRIM(x856.f6)+"*"+TRIM(x856.f7)
				REPLACE xptdet.printstuff WITH xptdet.printstuff+CHR(13)+CHR(13)+cFullDet
			ENDIF
		ENDIF
		SKIP 1 IN x856
	ENDDO
	SKIP -1 IN x856

ENDPROC

********************
PROCEDURE dupcheck && Check for duplicates
********************
	SELECT xpt

	SELECT ptid,upc,STYLE,PACK,COUNT(upc) AS cnt1 ;
		FROM xptdet ;
		GROUP BY ptid,upc,PACK ;
		INTO CURSOR tempdet1 ;
		HAVING cnt1 > 1
	LOCATE

	IF !EOF()
		SELECT a.office,a.ship_ref AS pickticket,b.STYLE,b.PACK,b.upc,b.cnt1 AS linecount ;
			FROM xpt a,tempdet1 b ;
			WHERE a.ptid = b.ptid ;
			INTO CURSOR tempdupes
		LOCATE
    xxStr = ""
    Select tempdupes
    Scan
      xxStr = xxStr + Alltrim(pickticket)+"  "+Alltrim(style)+"  "+Alltrim(pack)+"  "+Alltrim(upc)+"  "+Alltrim(Transform(linecount))+Chr(13)
    endscan

		Export To "h:\fox\dupedetail.xls" TYPE XLS

		USE IN tempdet1
		USE IN tempdupes
		IF lTesting
			tsendto ="joe.bianchi@tollgroup.com"
			tcc=""
		ELSE
			tsendto =tsendtodups
			tcc= tccdups
		ENDIF
		tsubject = "Error: Synclaire Duplicate Detail Lines"
		tmessage = "Symclaire 940 Upload Error!"
		tmessage = tmessage+CHR(13)+"The attached Excel file contains duplicate line items by PT"
		tmessage = tmessage+CHR(13)+"From file "+cFilename+CHR(13)+"This  Entire 940 file and all the included picktickets will not be uploaded... Please advise........"
    tmessage= tmessage+Chr(13)+xxStr 
		tattach  = "h:\fox\dupedetail.xls" 
 
    Wait Window At 10,10 "sending the dup email message....." Timeout 2
  
 *  Do sendmail_au With tsendto,"noreply@tollgroup.com",tsubject,tcc,tattach,tmessage,""
              **Parameter lcTO, lcFrom,                lcSubject,lcCC,lcAttach,lcBody,lcBCC

   DO FORM m:\dev\frm\dartmail2 WITH tsendto,tFrom,tsubject,tcc,tattach,tmessage,"A"


		lDoImport = .F.
		cErrMsg = "Failed duplicate line check"
		COPY FILE [&xfile] TO ("F:\FTPUSERS\Synclaire\hold\"+cFilename)
		DELETE FILE [&xfile]
		THROW
	ENDIF
*!* End added code block

ENDPROC
