*!* m:\dev\prg\in7_940_main.prg

PARAMETERS cOfficeIn
CLOSE DATA ALL
PUBLIC nAcctNum,cDelimiter,nRun_num,cTranslateOption,cWhichFile,cOffice,cCustname,cPropername,cStorenum
PUBLIC cFilter,nRunID,lComplete,lTesting,m.UCCNUMBER,lEmail,lTestImport,lTestRun,emailcommentstr,cUseFolder
PUBLIC ptid,ptctr,ptqty,xfile,cfilename,UploadCount,pickticket_num_start,pickticket_num_end,nFileCount,units
PUBLIC cOldScreen,tsendto,tcc,lcPath,lcArchivePath,nPTQty,cPTQty,archivefile,lBrowfiles
PUBLIC tsendtoerr,tccerr,cOfficeLoc,lLoadSQL,cMod,cISA_Num,nFileSize
PUBLIC ARRAY a856(1)

IF VARTYPE(cOfficeIn)="L"
	cOfficeIn = "I"
	cOffice = "I"
	cOfficeLoc = "NJ"
ENDIF

STORE "" TO cOldScreen
STORE _SCREEN.CAPTION TO cOldScreen
cCustname = "IN7"
cUseName = "IN7"
cOfficeLoc = " "
cPropername = "IN7"
nAcctNum  = 6059
NormalExit = .F.

STORE cPropername+" 940 Process" TO _SCREEN.CAPTION
CLEAR
WAIT WINDOW "Now setting up "+cPropername+" 940 process..." TIMEOUT 2
DO m:\dev\prg\_setvars WITH .T.

lTesting = .F.
lTestImport = lTesting
lTestRun = .F.
lOverridebusy = lTesting && Will override FTPSETUP chkbusy flag if .t.
lOverridebusy = .t.
lBrowfiles = lTesting  && Will allow browsing of XPT/XPTDET cursors before upload if .t.
*lBrowfiles = .T.
lEmail = .T.

_SCREEN.WINDOWSTATE = IIF(lTesting OR lTestImport OR lOverridebusy,2,1)

TRY
	DO m:\dev\prg\createx856a

	cDelimiter = "*"
	cTranslateOption = "NONE"
	cfile = ""

	dXdate1 = ""
	dXxdate2 = DATE()
	nFileCount = 0

	nRun_num = 999
	lnRunID = nRun_num
	cOffice = cOfficein
	cMod = cOffice
	gOffice = cMod
	lLoadSQL = .t.

	SELECT 0
	USE F:\edirouting\FTPSETUP SHARED
	LOCATE FOR FTPSETUP.TRANSFER = "940-IN7-EDI-NJ"
	ASSERT .F.
	IF FOUND()
		IF chkbusy AND !lOverridebusy
			WAIT WINDOW "940 Process is busy...exiting" TIMEOUT 2
			NormalExit = .T.
			THROW
		ENDIF
	ENDIF
	REPLACE chkbusy WITH .T.,trig_time WITH DATETIME() FOR FTPSETUP.TRANSFER = "940-IN7-EDI-NJ"
	USE IN FTPSETUP

*	ASSERT .F. MESSAGE "At MM Config load"
	SELECT 0
	USE F:\3pl\DATA\mailmaster ALIAS mm SHARED
	LOCATE FOR mm.accountid = nAcctNum AND mm.office = cOffice ;
		AND edi_type = "940"
	IF FOUND()
		STORE TRIM(mm.basepath) TO lcPath
		STORE TRIM(mm.archpath) TO lcArchivePath
		tsendto = IIF(mm.use_alt,TRIM(mm.sendtoalt),TRIM(mm.sendto))
		tcc = IIF(mm.use_alt,TRIM(mm.ccalt),TRIM(mm.cc))
		STORE TRIM(mm.scaption) TO thiscaption
		STORE mm.testflag 	   TO lTest
		_SCREEN.CAPTION = thiscaption
		LOCATE FOR (mm.accountid = 9999) AND (mm.office = "X")
		tsendtoerr = IIF(mm.use_alt,TRIM(mm.sendtoalt),TRIM(mm.sendto))
		tccerr = IIF(mm.use_alt,TRIM(mm.ccalt),TRIM(mm.cc))
		USE IN mm
	ELSE
		WAIT WINDOW AT 10,10  "No parameters set for this acct# "+ALLTRIM(STR(nAcctNum))+"  ---> Office "+lcOffice TIMEOUT 2
		USE IN mm
		NormalExit = .T.
		THROW
	ENDIF

	IF lTesting
		lcPath = "F:\0-Picktickets\IN7-NJ\Test\"
		lcArchivePath = "F:\0-Picktickets\archive\IN7-NJ\Test\"
		cUseFolder = "F:\WHP\WHDATA\"
		WAIT WINDOW "Test data...Importing into WHP tables" TIMEOUT 1
	ELSE
		xReturn = "XXX"
		DO m:\dev\prg\wf_alt WITH cOffice,nAcctNum
		SET STEP ON 
		cUseFolder = UPPER(xReturn)
	ENDIF

	DO ("m:\dev\PRG\"+cCustname+"_940edi_PROCESS")

	SELECT 0
	USE F:\edirouting\FTPSETUP SHARED
	REPLACE chkbusy WITH .F. FOR FTPSETUP.TRANSFER = "940-IN7-EDI-NJ"
	USE IN FTPSETUP

CATCH TO oErr
	IF !NormalExit
		ASSERT .F. MESSAGE "At CATCH..."
		tsubject = "IN7 940 "+cOfficeLoc+" Upload Error ("+TRANSFORM(oErr.ERRORNO)+") at "+TTOC(DATETIME())
		tattach  = ""
		tsendto  = tsendtoerr
		tcc = tccerr
		tmessage = "Bugaboo 940 "+cOfficeLoc+" Upload Error..... Please fix me........!"
		lcSourceMachine = SYS(0)
		lcSourceProgram = SYS(16)

		tmessage =tmessage+ "Try/CATCH Exeception Message:"+CHR(13)+;
			[  Message: ] + oErr.MESSAGE +CHR(13)+;
			[  Procedure: ] + oErr.PROCEDURE +CHR(13)+;
			[  LineNo: ] + STR(oErr.LINENO) +CHR(13)+;
			[  Error: ] + STR(oErr.ERRORNO) +CHR(13)+;
			[  Details: ] + oErr.DETAILS +CHR(13)+;
			[  StackLevel: ] + STR(oErr.STACKLEVEL) +CHR(13)+;
			[  LineContents: ] + oErr.LINECONTENTS+CHR(13)+;
			[  UserValue: ] + oErr.USERVALUE+CHR(13)+;
			[  Computer:  ] +lcSourceMachine+CHR(13)+;
			[  940 file:  ] +xfile+CHR(13)+;
			[  Program:   ] +lcSourceProgram

		tattach  = ""
		tFrom ="TGF Warehouse Operations <warehouse-ops@fmiint.com>"
		DO FORM m:\dev\frm\dartmail2 WITH tsendto,tFrom,tsubject,tcc,tattach,tmessage,"A"
	ELSE
		WAIT WINDOW AT 10,10 "Normal Exit " TIMEOUT 2
	ENDIF
FINALLY
	CLOSE DATABASES ALL
ENDTRY
