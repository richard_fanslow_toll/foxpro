lparameters xprojectname

*xsqlexec("update project set lastrun={"+dtoc(datetime())+"}, computer='"+getenv("COMPUTERNAME")+"', " + ;
	"actfolder='"+set("default")+sys(2003)+"' where projname='"+padr(xprojectname,30)+"'",,,"bt")

set exclusive off
try
	use f:\bugtrack\btdata\project alias btproj in 0

	if seek(padr(xprojectname,30),"btproj","projname")
		replace lastrun with datetime(), computer with getenv("COMPUTERNAME"), actfolder with set("default")+sys(2003) in btproj
	endif
	
	use in btproj
	
catch
	try
		use in btproj
	catch
	endtry
endtry