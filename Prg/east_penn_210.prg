*!* East Penn 210 (derived from a 214 routine)
*!* Created 2013.12.16, Paul

PARAMETERS cInv_Num
PUBLIC c_CntrlNum,c_GrpCntrlNum,cWO_Num,cGroupName,cCustName,cFilename,div214,cST_CSZ,cPrgName
PUBLIC tsendto,tcc,tsendtoerr,tccerr,cDelName,cMacysnet,cTripid,cMailName,cFin_status,cString,nHandle
DIMENSION thisarray(1)
cPrgName  = "EAST PENN 210"

lTesting = .F.

DO m:\dev\prg\_setvars WITH lTesting
ON ESCAPE CANCEL

IF lTesting
	SET STEP ON
ENDIF

IF USED('invoice')
	USE IN invoice
endif

xsqlexec("select * from invoice where invnum='"+PADR(ALLTRIM(cInv_Num),6)+"'",,,"ar")

cString = ""

IF FOUND()
	nWo_num = invoice.wo_num
	cWO_Num = ALLTRIM(STR(nWo_num))
ELSE
	WAIT WINDOW "Invoice number "+cInv_Num+" not found in Invoice table!" TIMEOUT 2
	ediupdate("MISS INV #",.T.)
ENDIF
USE IN invoice

closefiles()
IF !USED('account')
	xsqlexec("select * from account where inactive=0","account",,"qq")
	INDEX ON accountid TAG accountid
	SET ORDER TO
ENDIF

STORE "" TO cFilename,cShip_ref,cST_CSZ,cMacysnet,cTripid,cLeg,cFileInfo,cFin_status
lIsError = .F.
lDoCatch = .T.
lEmail = .T.
lDoError = .F.
lFilesOut = .T.
nAcctNum = 6293
cOffice = "I"

tfrom = "TOLL Express Operations <tgf-express-ops@tollgroup.com>"
SELECT 0
USE F:\3pl\DATA\mailmaster ALIAS mm
LOCATE FOR mm.edi_type = "MISC" AND mm.taskname = "GENERAL"
tsendto = IIF(mm.use_alt,mm.ccalt,mm.cc)  && Reversed here to put CC into SENDTO
tcc = IIF(mm.use_alt,mm.sendtoalt,mm.sendto)
USE IN mm
tattach = ""

cCustName = "EASTPENN"
cCustFolder = "EASTPENN"
cCustPrefix = "EASTPENN"+LOWER(cOffice)
dtmail = TTOC(DATETIME())
dt2 = DATETIME()
cMailName = PROPER("EASTPENN")
lcpath = "f:\ftpusers\EASTPENN\210-Staging\"
CD &lcpath

*!* Open Tables

xsqlexec("select * from fxwolog where wo_num="+cWO_Num,"xfxwolog",,"fx")
IF RECCOUNT()=0
	DO ediupdate WITH "NO FXWOLOG WO# MATCH: "+cWO_Num,.T.
	THROW
ELSE
**no longer use trip rate, use charge for linehail and otherchg for stop off - mvw 01/27/14
*		lnTripRate = fxwolog.loadedrate
	xcharge=xfxwolog.charge
	xstopchg=xfxwolog.otherchg
	xBOL = xfxwolog.shipref
ENDIF

xsqlexec("select * from fxtrips where wo_num="+cWO_Num+" and deadhead=0 and pu=1","xtrip",,"fx")
IF RECCOUNT()=0
	DO ediupdate WITH "NO FXTRIP WO# MATCH: "+cWO_Num,.T.
	THROW
ENDIF

xpickedup=xtrip.pickedup
**changed from puatime to picktime (was taking appt time incorrectly) - mvw 03/26/14
xpicktime=xtrip.picktime

xsqlexec("select * from fxtrips where wo_num="+cWO_Num+" and deadhead=0 and del=1 order by fxtripsid desc","xtrip",,"fx")
IF RECCOUNT()=0
	DO ediupdate WITH "NO DELIV REC IN FXTRIP: "+cWO_Num,.T.
	THROW
ENDIF

SELECT xtrip

**if incorrectly entered as pickedup>delivered, use pickedup as delivered - mvw 03/26/14
xdelivered=IIF(xtrip.delivered#xpickedup,xpickedup,xtrip.delivered)

**changed from delatime to delvtime (was taking appt time incorrectly) - mvw 03/26/14
xdelvtime=xtrip.delvtime

**if both or 1 of the times are empty, follow below case - mvw 03/26/14
DO CASE
	CASE EMPTY(xpicktime) AND EMPTY(xdelvtime)
		STORE "1200P" TO xpicktime, xdelvtime
	CASE EMPTY(xpicktime)
		xpicktime=IIF(xpickedup=xdelivered,xdelvtime,"1200P")
	CASE EMPTY(xdelvtime)
		xdelvtime=IIF(xpickedup=xdelivered,xpicktime,"1200P")
ENDCASE

xdelname=xtrip.delname
xdeladdress=xtrip.deladdress
xdelcsz=xtrip.delcsz
xdelstate=xtrip.delstate

USE IN xtrip

xsqlexec("select * from invoice where invnum='"+xfxwolog.invnum+"'",,,"ar")

IF !FOUND()
	DO ediupdate WITH "NO Invoice for Trip: "+cWO_Num,.T.
	THROW
ELSE
	lnInvAmt = invoice.invamt
	xinvid = invoice.invoiceid
	
	xsqlexec("select * from invdet where invoiceid = "+transform(xinvid)+" and glcode = '29' and split=0",,,"ar")
	
	IF FOUND()
		lnStart = AT("PER",invdet.DESCRIPTION)
		xfuel=invdetamt
		lnFuelSurCharge = ROUND(VAL(ALLTRIM(SUBSTR(invdet.DESCRIPTION,lnStart-5,4))),2)
	ELSE
		xfuel=0.00
		lnFuelSurCharge=0.00
	ENDIF

**match the linehaul charge with fxwolog.charge - if not match use invdet amount
	xsqlexec("select * from invdet where invoiceid = "+transform(xinvid)+" and description='CHARGE' and chgtype='LINEHAUL' and split=0",,,"ar")
	
	IF FOUND() AND xcharge#invdetamt
		xcharge=invdetamt
	ENDIF

*!*			**match the linehaul charge with fxwolog.charge - if not match use invdet amount
*!*			sum invdetamt to xotherchg for invoiceid = xinvid AND !(description='CHARGE' and chgtype="LINEHAUL") and glcode#"29" and !split
*!*			if found() and xstopchg#xotherchg
*!*				xstopchg=xotherchg
*!*			endif
ENDIF

IF lnInvAmt#xcharge+xfuel+xstopchg
	tattach  = ""
	tmessage = "Error processing East Penn 210 for invoice "+ALLTRIM(invnum)+": Invoice totals do not match totals in express."+CHR(13)+CHR(13)+;
		"Invoice will need to be retriggered once corrections have been made."

	tsubject = "East Penn 210 Creation Error"
	tsendtoerr ="fran.castro@tollgroup.com,melissa.paone@tollgroup.com"
	tccerr="pgaidis@fmiint.com,mwinter@fmiint.com"
	tfrom    ="TGF EDI 210 Process Ops <toll-edi-ops@tollgroup.com>"
	DO FORM dartmail2 WITH tsendtoerr,tfrom,tsubject,tccerr,tattach,tmessage,"A"

	DO ediupdate WITH "TOTAL INVOICE AMOUNT DOESNT MATCH DETAIL",.T.
	THROW
ENDIF

lnTripMiles = xfxwolog.totmiles-xfxwolog.totdhmiles

cterminator = ">"  && Used at end of ISA segment
cfd = "*"  && Field delimiter
csegd = CHR(13) && CR
csendqual = "02"  && Sender qualifier
csendid = "FMIF"  && Sender ID code
crecqual = "ZZ"  && Recip qualifier
crecid = "TLI94EP"   && Recip ID Code, production (05.11.2010)

cDate = DTOS(DATE())
cTruncDate = RIGHT(cDate,6)
cTruncTime = SUBSTR(TTOC(DATETIME(),1),9,4)
cfiledate = cDate+cTruncTime
csendidlong = PADR(csendid,15," ")
crecidlong = PADR(crecid,15," ")
cString = ""

nSTCount = 0
nSegCtr = 0
nLXNum = 1

cISACode = IIF(lTesting,"T","P")

WAIT CLEAR
WAIT WINDOW "Now creating East Penn 210 information..." NOWAIT

xsqlexec("select * from fxtrips where wo_num="+cWO_Num+" AND deadhead=0 AND !empty(fxtrips.deladdress)","xfxtrips",,"fx")	 && Final Destination

xsqlexec("select * from fxtrips Where wo_num="+cWO_Num+" and deadhead=0 order by leg asc","temp",,"fx")
GO BOTT

xdelname    = ALLTRIM(temp.delname)
xdeladdress = ALLTRIM(temp.deladdress)
xdelcsz     = ALLTRIM(temp.delcsz)
xDelCity    = ALLTRIM(LEFT(xdelcsz,AT(",",xdelcsz,1)-1))
xdelstate   = ALLTRIM(SUBSTR(xdelcsz,AT(",",xdelcsz,1)+1,3))
xDelZip     = ALLTRIM(SUBSTR(xdelcsz,AT(",",xdelcsz,1)+4))
xDelCityState = xDelCity+", "+xdelstate

*!* More Variables

dt1 = TTOC(DATETIME(),1)
cFilename = ("210"+"-"+"FMIF"+"-"+dt1+".edi")
cFilenameShort = JUSTFNAME(cFilename)
cFilename2 = ("f:\ftpusers\"+cCustFolder+"\210OUT\archive\"+cFilenameShort)
cFilename3 = ("f:\ftpusers\"+cCustFolder+"\210OUT\"+cFilenameShort)
nHandle = FCREATE(cFilename)

DO num_incr_isa
cISA_Num = PADL(c_CntrlNum,9,"0")

STORE "ISA"+cfd+"00"+cfd+"          "+cfd+"00"+cfd+"          "+cfd+csendqual+cfd+csendidlong+cfd+;
	crecqual+cfd+crecidlong+cfd+cTruncDate+cfd+cTruncTime+cfd+"U"+cfd+"00200"+cfd+;
	cISA_Num+cfd+"0"+cfd+cISACode+cfd+cterminator+csegd TO cString
DO cstringbreak

STORE "GS"+cfd+"QM"+cfd+csendid+cfd+crecid+cfd+cDate+cfd+cTruncTime+cfd+c_CntrlNum+;
	cfd+"X"+cfd+"004010VICS"+csegd TO cString
DO cstringbreak

DO num_incr_st
STORE "ST"+cfd+"210"+cfd+PADL(c_GrpCntrlNum,9,"0")+csegd TO cString
DO cstringbreak
nSTCount = nSTCount + 1
nSegCtr = nSegCtr + 1
SET CENTURY ON
SET DATE YMD
**changed B312 element to minimum of p/u date and deliv date - mvw 02/04/14
*	STORE "B3"+cfd+cWO_Num+cfd+ALLTRIM(fxwolog.invnum)+cfd+"NONE"+cfd+"PP"+cfd+"L"+cfd+DTOS(fxwolog.inventerdt)+cfd+ALLTRIM(STR(lnInvAmt*100,10))+;
*		cfd+cfd+DTOS(xdelivered)+cfd+"035"+cfd+"FMIF"+cfd+DTOS(DATE())+csegd TO cString

*!* Added these two lines to counter NULL values
xpickedup = IIF(ISNULL(xpickedup),CTOD("  /  /    "),xpickedup)
xdelivered = IIF(ISNULL(xdelivered),CTOD("  /  /    "),xdelivered)

STORE "B3"+cfd+cWO_Num+cfd+ALLTRIM(xfxwolog.invnum)+cfd+ALLTRIM(xBOL)+cfd+"PP"+cfd+"L"+cfd+DTOS(xfxwolog.inventerdt)+cfd+ALLTRIM(STR(lnInvAmt*100,10))+;
	cfd+cfd+DTOS(xdelivered)+cfd+"035"+cfd+"FMIF"+cfd+DTOS(MIN(xpickedup,xdelivered))+csegd TO cString
DO cstringbreak
nSegCtr = nSegCtr + 1

DO CASE
	CASE "A"$xpicktime
		xpicktime=LEFT(xpicktime,4)
**if 12-1259A, its 0000-0059 - mvw 03/26/14
		xpicktime=IIF(xpicktime="12","00"+RIGHT(xpicktime,2),xpicktime)
	CASE xpicktime="12" &&1200P-1259P
		xpicktime=LEFT(xpicktime,4)
	OTHERWISE
		npicktime = INT(VAL(xpicktime))
		npicktime = npicktime + 1200
		xpicktime = TRANSFORM(npicktime)
ENDCASE

**if time = "2400", change to "0000" per Dimas (Translogistics) - mvw 03/19/14
**no longer needed with above case statement - mvw 03/26/14
*	cpicktime=iif(cpicktime="2400","0000",cpicktime)

STORE "G62"+cfd+"86"+cfd+DTOS(xpickedup)+cfd+"8"+cfd+xpicktime+csegd TO cString
DO cstringbreak
nSegCtr = nSegCtr + 1

DO CASE
	CASE "A"$xdelvtime
		xdelvtime=LEFT(xdelvtime,4)
**if 12-1259A, its 0000-0059 - mvw 03/26/14
		xdelvtime=IIF(xdelvtime="12","00"+RIGHT(xdelvtime,2),xdelvtime)
	CASE xdelvtime="12" &&1200P-1259P
		xdelvtime=LEFT(xdelvtime,4)
	OTHERWISE
		ndelvtime = INT(VAL(xdelvtime))
		ndelvtime = ndelvtime + 1200
		xdelvtime = TRANSFORM(ndelvtime)
ENDCASE

**if time = "2400", change to "0000" per Dimas (Translogistics) - mvw 03/19/14
**no longer needed with above case statement - mvw 03/26/14
*	cDELTime=iif(cDELTime="2400","0000",cDELTime)

STORE "G62"+cfd+"35"+cfd+DTOS(xdelivered)+cfd+"9"+cfd+xdelvtime+csegd TO cString
DO cstringbreak
nSegCtr = nSegCtr + 1

STORE "N1"+cfd+"SH"+cfd+"EAST PENN MANUFACTURING"+cfd+"25"+cfd+"33456"+csegd TO cString
DO cstringbreak
nSegCtr = nSegCtr + 1

STORE "N3"+cfd+"50 JEFFERSON ST"+csegd TO cString
DO cstringbreak
nSegCtr = nSegCtr + 1

STORE "N4"+cfd+"TOPTON"+cfd+"PA"+cfd+"19562"+csegd TO cString
DO cstringbreak
nSegCtr = nSegCtr + 1

STORE "N1"+cfd+"CN"+cfd+ALLTRIM(xdelname)+csegd TO cString
DO cstringbreak
nSegCtr = nSegCtr + 1

STORE "N3"+cfd+ALLTRIM(xdeladdress)+csegd TO cString
DO cstringbreak
nSegCtr = nSegCtr + 1

lnCityend = AT(",",xdelcsz)
lcPuCity = SUBSTR(xdelcsz,1,lnCityend-1)
lcPuZip = RIGHT(ALLTRIM(xdelcsz),5)

STORE "N4"+cfd+lcPuCity+cfd+ALLTRIM(xdelstate)+cfd+lcPuZip+csegd TO cString
DO cstringbreak
nSegCtr = nSegCtr + 1

STORE "N1"+cfd+"BT"+cfd+"EAST PENN MANUFACTURING"+cfd+"25"+cfd+"33456"+csegd TO cString
DO cstringbreak
nSegCtr = nSegCtr + 1

STORE "N3"+cfd+"TRANSLOGISTICS, INC"+csegd TO cString
DO cstringbreak
nSegCtr = nSegCtr + 1

STORE "N4"+cfd+"BIRDSBORO"+cfd+"PA"+cfd+"19508"+csegd TO cString
DO cstringbreak
nSegCtr = nSegCtr + 1

STORE "N7"+cfd++cfd+ALLTRIM(xfxwolog.trailer)+cfd+cfd+cfd+cfd+cfd+cfd+cfd+cfd+cfd+"TL"+csegd TO cString
DO cstringbreak
nSegCtr = nSegCtr + 1

**Freight Charges

STORE "LX"+cfd+"1"+csegd TO cString
DO cstringbreak
nSegCtr = nSegCtr + 1

STORE "L5"+cfd+"1"+cfd+"FREIGHT"+csegd TO cString
DO cstringbreak
nSegCtr = nSegCtr + 1

**no longer use miles * rate, use fxwolog.charge - mvw 01/27/14
**	STORE "L1"+cfd+"1"+cfd+TRANSFORM(ROUND(lnTripMiles*lnTripRate,2))+cfd+"FR"+cfd+TRANSFORM(ROUND((lnTripMiles*lnTripRate*100),0))+cfd+cfd+cfd+cfd+"400"+csegd TO cString
STORE "L1"+cfd+"1"+cfd+ALLTRIM(STR(xcharge,10,2))+cfd+"FR"+cfd+TRANSFORM(xcharge*100)+cfd+cfd+cfd+cfd+"400"+csegd TO cString
DO cstringbreak
nSegCtr = nSegCtr + 1

**Fuel Surcharge

STORE "LX"+cfd+"2"+csegd TO cString
DO cstringbreak
nSegCtr = nSegCtr + 1

STORE "L5"+cfd+"2"+cfd+"FUEL SURCHARGE - MILEAGE"+csegd TO cString
DO cstringbreak
nSegCtr = nSegCtr + 1

STORE "L0"+cfd+"2"+cfd+TRANSFORM(lnTripMiles)+cfd+"DM"+csegd TO cString
DO cstringbreak
nSegCtr = nSegCtr + 1

STORE "L1"+cfd+"2"+cfd+TRANSFORM(lnFuelSurCharge)+cfd+"PM"+cfd+TRANSFORM(xfuel*100)+cfd+cfd+cfd+cfd+"FUE"+csegd TO cString
DO cstringbreak
nSegCtr = nSegCtr + 1

**Stop Off Charge
**added 01/27/14 mvw
IF !EMPTY(xstopchg)
	STORE "LX"+cfd+"3"+csegd TO cString
	DO cstringbreak
	nSegCtr = nSegCtr + 1

	STORE "L5"+cfd+"3"+cfd+"STOP OFF CHARGE"+csegd TO cString
	DO cstringbreak
	nSegCtr = nSegCtr + 1

*		STORE "L1"+cfd+"3"+cfd+alltrim(str(xstopchg,10,2))(xstopchg,10,2))(xstopchg,10,2))(xstopchg,10,2))+cfd+"FR"+cfd+TRANSFORM(xstopchg*100)+cfd+cfd+cfd+cfd+"SOC"+csegd TO cString
	STORE "L1"+cfd+"3"+cfd+ALLTRIM(STR(xstopchg,10,2))+cfd+"FR"+cfd+TRANSFORM(xstopchg*100)+cfd+cfd+cfd+cfd+"SOC"+csegd TO cString
	DO cstringbreak
	nSegCtr = nSegCtr + 1
ENDIF


STORE "L3"+cfd+cfd+cfd+cfd+cfd+ALLTRIM(STR(lnInvAmt*100,10))+csegd TO cString
DO cstringbreak
nSegCtr = nSegCtr + 1

STORE "SE"+cfd+ALLTRIM(STR(nSegCtr+1))+cfd+PADL(c_GrpCntrlNum,9,"0")+csegd TO cString
DO cstringbreak

STORE  "GE"+cfd+ALLTRIM(STR(nSTCount))+cfd+c_CntrlNum+csegd TO cString
DO cstringbreak

STORE  "IEA"+cfd+"1"+cfd+PADL(c_CntrlNum,9,"0")+csegd TO cString
DO cstringbreak

lClosedFile=FCLOSE(nHandle)

IF lClosedFile AND lTesting
	WAIT WINDOW "LL File closed successfully" TIMEOUT 1
*		MODIFY FILE &cFilename
	RELEASE nHandle
ENDIF

DO ediupdate WITH "210 CREATED",.F.

IF lFilesOut
	COPY FILE &cFilename TO &cFilename2
	COPY FILE &cFilename TO &cFilename3
	DELETE FILE &cFilename
ENDIF
IF !lTesting
	IF !USED("ftpedilog")
		SELECT 0
		USE F:\edirouting\ftpedilog ALIAS ftpedilog
		INSERT INTO ftpedilog (TRANSFER,ftpdate,filename,acct_name,TYPE) ;
			VALUES ("214-"+cCustName,dt2,cFilename,UPPER(cCustName),"214")
		USE IN ftpedilog
	ENDIF
ENDIF

**create an ftpjob if this is the last edi_trigger rec for an east penn 210 - mvw 02/06/14
SELECT edi_trigger
xrecno=RECNO()
LOCATE FOR accountid=6293 AND edi_type="210" AND !processed WHILE !EOF()

IF !FOUND()
	xused=USED("ftpjobs")
	IF !xused
		USE F:\edirouting\ftpjobs IN 0
	ENDIF
	INSERT INTO ftpjobs (jobname, USERID, jobtime) VALUES ("210-EASTPENN","AUTO",DATETIME())
	IF !xused
		USE IN ftpjobs
	ENDIF
ENDIF
SELECT edi_trigger
LOCATE

*!* Finish processing
IF lTesting
	tsubject = "EAST PENN 210 *TEST* EDI from TGF as of "+dtmail
ELSE
	tsubject = "EAST PENN 210 EDI from TGF as of "+dtmail
ENDIF

tmessage = cMailName+" 214 EDI FILE (TGF trucking WO# "+cWO_Num+")"
tmessage = tmessage+CHR(13)+"has been created for the following:"+CHR(13)
tmessage = tmessage+CHR(13)+cFileInfo
IF lTesting
	tmessage = tmessage + CHR(13)+CHR(13)+"This is a TEST RUN..."
ENDIF

IF lEmail
	DO FORM dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
ENDIF
RELEASE ALL LIKE c_CntrlNum,c_GrpCntrlNum
RELEASE ALL LIKE nOrigSeq,nOrigGrpSeq
WAIT CLEAR
WAIT WINDOW cMailName+" 210 EDI File output complete" AT 20,60 TIMEOUT 2
closefiles()

*!*	CATCH TO oErr
*!*		IF lDoCatch
*!*			ASSERT .F. MESSAGE "In CATCH...debug"
*!*			lEmail = .T.

*!*			tattach  = ""
*!*			tmessage = cCustName+" Error processing "+CHR(13)
*!*			tmessage = tmessage+TRIM(PROGRAM())+CHR(13)

*!*			tmessage =tmessage+ "Try/CATCH Exeception Message:"+CHR(13)+;
*!*				[  Error: ] + STR(oErr.ERRORNO) +CHR(13)+;
*!*				[  LineNo: ] + STR(oErr.LINENO) +CHR(13)+;
*!*				[  Message: ] + oErr.MESSAGE +CHR(13)+;
*!*				[  Procedure: ] + oErr.PROCEDURE +CHR(13)+;
*!*				[  Details: ] + oErr.DETAILS +CHR(13)+;
*!*				[  StackLevel: ] + STR(oErr.STACKLEVEL) +CHR(13)+;
*!*				[  LineContents: ] + oErr.LINECONTENTS+CHR(13)+;
*!*				[  UserValue: ] + oErr.USERVALUE

*!*			tsubject = "214 Process Error at "+TTOC(DATETIME())
*!*			tattach  = ""
*!*			tccerr="joe.bianchi@tollgroup.com"
*!*			tsendtoerr ="pgaidis@fmiint.com,mwinter@fmiint.com"
*!*			tfrom    ="TGF EDI 210 Process Ops <toll-edi-ops@tollgroup.com>"
*!*			DO FORM dartmail2 WITH tsendtoerr,tfrom,tsubject,tccerr,tattach,tmessage,"A"
*!*			lEmail = .F.
*!*		ENDIF
*!*	FINALLY
*!*		closefiles()
*!*	ENDTRY

&& END OF MAIN CODE SECTION

**************************
PROCEDURE ediupdate
**************************
	PARAMETERS cFin_status,lIsError
*	ASSERT .F. MESSAGE "In EDIUPDATE"
*	SET STEP ON
	lDoCatch = .F.
	SELECT edi_trigger
	LOCATE
	IF lIsError
		SET STEP ON 
		REPLACE edi_trigger.processed WITH .T.,edi_trigger.created WITH .F.,;
			edi_trigger.fin_status WITH cFin_status,edi_trigger.errorflag WITH .T.;
			edi_trigger.when_proc WITH DATETIME() ;
			FOR edi_trigger.bol = cInv_Num AND edi_trigger.accountid = 6293 ;
			AND edi_trigger.edi_type ="210 "
		closefiles()
		IF cFin_status # "TOTAL INVOICE AMOUNT DOESNT MATCH DETAIL"  && No need for Joe & Paul to receive this notice
			errormail(cFin_status)
		ENDIF
		=FCLOSE(nHandle)
		IF FILE(cFilename)
			DELETE FILE [&cFilename]
		ENDIF
		THROW
	ELSE
		REPLACE edi_trigger.processed WITH .T.,edi_trigger.created WITH .T.,;
			edi_trigger.when_proc WITH DATETIME(),;
			edi_trigger.fin_status WITH "210 CREATED";
			edi_trigger.errorflag WITH .F.,file214 WITH cFilename ;
			FOR edi_trigger.bol = cInv_Num AND edi_trigger.accountid = 6293 ;
			AND edi_trigger.edi_type ="210 "
	ENDIF
ENDPROC

****************************
PROCEDURE errormail
****************************
	PARAMETERS cFin_status
	SET STEP ON
	lDoCatch = .F.

	ASSERT .F. MESSAGE "At Errormail...Debug"
	tsubject = cCustName+" 210 EDI File Error"
	tmessage = "210 EDI File Error, WO# "+cWO_Num+CHR(13)
	tmessage = tmessage+CHR(13)+CHR(13)+"Error: "+cFin_status

	IF lTesting
		tmessage = tmessage + CHR(13)+CHR(13)+"This is a TEST run."
	ENDIF
	IF lEmail
		DO FORM dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
	ENDIF
ENDPROC

*********************
PROCEDURE closefiles
*********************
	IF USED('ftpedilog')
		USE IN ftpedilog
	ENDIF
	IF USED('serfile')
		USE IN serfile
	ENDIF
	IF USED('fedcodes')
		USE IN FEDCODES
	ENDIF
	IF USED('account')
		USE IN account
	ENDIF
	IF USED('xref')
		USE IN xref
	ENDIF
ENDPROC

****************************
PROCEDURE num_incr_isa
****************************

	IF !USED("serfile")
		USE ("F:\3pl\data\serial\eastpenn_210_serial") IN 0 ALIAS serfile
	ENDIF
	SELECT serfile
	IF lTesting
		nOrigSeq = serfile.seqnum
		nOrigGrpSeq = serfile.grpseqnum
		lISAFlag = .F.
	ENDIF
	nISA_Num = serfile.seqnum
	c_CntrlNum = ALLTRIM(STR(serfile.seqnum))
	REPLACE serfile.seqnum WITH serfile.seqnum + 1 IN serfile
ENDPROC

****************************
PROCEDURE num_incr_st
****************************

	IF !USED("serfile")
		USE ("F:\3pl\data\serial\EASTPENN_210_serial") IN 0 ALIAS serfile
	ENDIF
	SELECT serfile
	IF lTesting
		nOrigSeq = serfile.seqnum
		nOrigGrpSeq = serfile.grpseqnum
		lSTFlag = .F.
	ENDIF
	c_GrpCntrlNum = ALLTRIM(STR(serfile.grpseqnum))
	REPLACE serfile.grpseqnum WITH serfile.grpseqnum + 1 IN serfile
ENDPROC

****************************
PROCEDURE segmentget
****************************

	PARAMETER thisarray,lcKey,nLength

	FOR i = 1 TO nLength
		IF i > nLength
			EXIT
		ENDIF
		lnEnd= AT("*",thisarray[i])
		IF lnEnd > 0
			lcThisKey =TRIM(SUBSTR(thisarray[i],1,lnEnd-1))
			IF OCCURS(lcKey,lcThisKey)>0
				RETURN SUBSTR(thisarray[i],lnEnd+1)
				i = 1
			ENDIF
		ENDIF
	ENDFOR

	RETURN ""

****************************
PROCEDURE close214
****************************
	STORE  "SE"+cfd+ALLTRIM(STR(nSegCtr+1))+cfd+PADL(c_GrpCntrlNum,9,"0")+csegd TO cString
	DO cstringbreak

	STORE  "GE"+cfd+ALLTRIM(STR(nSTCount))+cfd+c_CntrlNum+csegd TO cString
	DO cstringbreak

	STORE  "IEA"+cfd+"1"+cfd+PADL(c_CntrlNum,9,"0")+csegd TO cString
	DO cstringbreak

ENDPROC


****************************
PROCEDURE cstringbreak
****************************
*	ASSERT .f. MESSAGE "In cStringBreak proc"
	cLen = LEN(ALLTRIM(cString))
	FPUTS(nHandle,cString)
	RETURN
