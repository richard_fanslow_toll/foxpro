************************************************************
*  FUNCTION getwonumber()
************************************************************
FUNCTION getwonumber()
LOCAL lnWo,lnRetVal
lcRetval=""
 DO WHILE .T.
   lcBOL=INPUTBOX('Lifefactory Work Order: ',"Enter a Work Order to Create Carton Labels........")   && WO num has to be an integer
   lcRetVal=lcBOL
    IF Empty(lcBOL)  && either nothing was put into the box or cancel was pressed
      lnAnswer=MESSAGEBOX("There is no WO entered or Cancel was pressed"+ CHR(13)+;
                 "   Do you want to exit program ?",36)
      IF lnAnswer=6  && yes exit
       RETURN lcRetval  && return 0
      ELSE  && try again
       LOOP
      Endif
    ENDIF
    RETURN lcRetVal
 ENDDO
EndFunc
************************************************************
