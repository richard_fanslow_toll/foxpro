PUBLIC ltesting

**test(.t.,.t.)  && DHW


coffice = "N"
*!*	ltesting = .t.
*!*	ltestimport =.t.
nAcctNum = 6303
gOffice = "N"
cMod ="J"

SET STEP ON 

**fileDir = 'M:\doug\MarcJacobs\'  &&DHW
fileDir = 'f:\ftpusers\mj_ecom\ecomm_in\'
CD &fileDir
archDir = fileDir + '\Archive\'
ackDir = 'f:\ftpusers\mj_ecom\radial_ack\'
**ackDir = 'M:\doug\MarcJacobs\'  &&DHW


CLOSE DATABASES ALL

CREATE CURSOR ordhead;
	(RecordID C(2),;
	ExtWarehouseNum C(3),;
	FulfillmentLoc C(5),;
	OrdInvoiceNum C(14),;
	CustOrdNum C(30),;
	InvoiceDate c(20),;
	OrdEntryDate c(20),;
	ExpDeliveryDate c(20),;
	ExpediteFlag C(1),;
	BillToCustID c(20),;
	BillToName C(60),;
	BillToAddr1 C(50),;
	BillToAddr2 C(50),;
	BillToAddr3 C(50),;
	BillToCity C(30),;
	BillToState C(10 ),;
	BillToCountry C(10),;
	BillToZip C(20),;
  	BillToPhone C(20),;
	ShipToName C(60),;
	ShipToAddr1 C(50),;
	ShipToAddr2 C(50),;
	ShipToAddr3 C(50),;
	ShipToCity C(30),;
	ShipToState C(10),;
	ShipToCountry C(10),;
	ShipToZip C(20),;
	ShiptoPhone C(20),;
	CarrierCode C(4),;
	CarrierMode C(6),;
	InvoiceTotal c(20),;
	TotalRetail c(20),;
	TotalFreight c(20),;
	TotalTax c(20),;
	TotalGiftWrapChg c(20),;
	TotalCustom c(20),;
	TenderRef C(8),;
	AddlShipmentFlag C(1),;
	FileCreateDate c(20),;
	PackslipData C(100))

CREATE CURSOR ordDetail;
	(RecordID C(2),;
	OrdInvoiceNum C(14),;
	OrdLineNum c(20),;
	PartNum C(15),;
	OrdQty N(7,0),;
	Retail c(20),;
	Tax c(20),;
	Credit c(20),;
	Customization c(20),;
	GiftWrapChg c(20),;
	GiftWrapID C(15),;
	Misc1 C(60),;
	Misc2 C(30),;
	Misc3 C(30),;
	Misc4 C(30),;
	Misc5 C(30),;
	Misc6 C(30),;
	Misc7 C(30),;
	Misc8 C(30),;
	Misc9 C(30),;
	Misc10 C(30),;
	FileCreateDate N(14,0))
 
 CREATE CURSOR ordComment;
 	(RecordID  C(2),;
	OrdInvoiceNum C(14),;
	OrdLineNum c(10),;
	CommentSeq N(4,0),;
	CommentType C(2),;
	Comment C(254),;
	FileCreateDate N(14,0))
	
CREATE CURSOR trailerRec;
	(RecordID C(2),;
	H1Count N(9,0),;
	D1Count N(9,0),;
	C1Count N(9,0),;
	BatchID C(10))


*!*************************************************************
*!*	Read input files into separate cursors for each record type
*!*************************************************************
LOCAL inFileHandle

fileCnt = ADIR(fileList, fileDir+"ESSORD2*.dat")
IF fileCnt = 0
	WAIT WINDOW "No files found... exiting" TIMEOUT 4
	ON ERROR
	RETURN
ENDIF

xsqlexec("select * from pt where .f.","xpt",,"wh")
xsqlexec("select * from ptdet where .f.","xptdet",,"wh")

Index on ptid Tag ptid
	
	
lnptid    = 1
lnptdetid = 1
MJAcct    = 6303
MJOffice  = 'N'
MJMod     = 'J'

*xsqlexec("select * from upcmast where accountid = 6303",,,"wh")

*set step on

FOR i = 1 TO fileCnt
	thisfile = ALLTRIM(fileList[i,1])
	infile   = fileDir + thisfile
	archfile = archDir + thisfile + '.' + TTOC(DATETIME(),1)
	errorfile = fileDir + thisfile + '.ERROR'
*!*		errorMsg = 'UNable to open file ' + infile
	
	xfile = infile
	ArchiveFile = archfile
	
	
	
	ZAP IN ordhead
	ZAP IN ordDetail
	ZAP IN ordComment
	ZAP IN trailerRec
	
	inFileHandle = FOPEN(infile)
	IF inFileHandle = -1
		?'Unable to open file ' + infile
		RETURN
	ENDIF

	CLEAR 

	LOCAL inLine
	
	DO WHILE NOT FEOF(inFileHandle)
		inLine = UPPER(FGETS(inFileHandle,1024))
		nRows = ALINES(laData, inLine,.f., '|')

		DO CASE 
			CASE laData(1) = 'H1'
				SELECT ordhead
			CASE laData(1) = 'D1'
				SELECT ordDetail
			CASE laData(1) = 'C1'
				SELECT ordComment
			CASE laData(1) = 'T1'
				SELECT trailerRec
			OTHERWISE
				?'Unknown record type'
		ENDCASE 
		APPEND FROM array laData 
		CLEAR 	
	ENDDO 

	FCLOSE(inFileHandle)

	*!*****************************************************************
	*!* Error checking download data                                   
	*!*****************************************************************
	SELECT custordnum, COUNT(custordnum) thiscnt FROM ordhead GROUP BY custordnum HAVING thiscnt > 1 INTO CURSOR edchk
	errcnt = RECCOUNT()
	IF errcnt > 0
		fileError = .t.
	ELSE 
		fileError = .f.
	ENDIF 
	
	IF fileError
		RENAME (infile) TO (errorFile)
		RETURN
	ELSE
		COPY FILE (infile) TO (archfile)
		DELETE FILE (infile)
	ENDIF 

	*!*****************************************************************
	*!* Load data into temporary pt and ptdet structures               
	*!*****************************************************************
 
	SELECT ordhead
	SCAN
		SELECT xpt
		APPEND BLANK
		REPLACE ptid      WITH lnptid   In xpt
		REPLACE accountid WITH MJAcct   In xpt
		REPLACE office    WITH MJOffice In xpt
		REPLACE mod       WITH MJMod    In xpt
		REPLACE ptdate    WITH Date()   In xpt
    	REPLACE start     WITH Date()   In xpt
		REPLACE consignee WITH ordhead.ShipToName In xpt
		
		crInt = VAL(ordhead.CustOrdNum)
		IF crInt > 0
			crStr = TRANSFORM(crInt)
		ELSE
			crStr = RIGHT(RTRIM(ordhead.CustOrdNum), 20) 
		ENDIF 

*!*		As per L. Lopez at MJ, we had this backwards.  OrdInvoiceNum should be pick ticket
*!*		and CustOrdNum should be PO (cnee_ref)
*!*			REPLACE ship_ref  WITH ptStr In xpt
*!*			REPLACE cnee_ref  WITH ordhead.OrdInvoiceNum  In xpt

		REPLACE ship_ref  WITH ordhead.OrdInvoiceNum  In xpt
		REPLACE cnee_ref  WITH crStr                  In xpt

		REPLACE ship_via  WITH ordhead.CarrierMode In xpt
		REPLACE address   WITH ordhead.ShipToAddr1 In xpt
		REPLACE address2  WITH ordhead.ShipToAddr2 In xpt
		REPLACE csz       WITH Alltrim(ordhead.ShipToCity)+", "+Alltrim(ordhead.ShipToState)+" "+Alltrim(ordhead.ShipToZip) In xpt
    	REPLACE country   WITH ordhead.ShipToCountry In xpt
	
   		REPLACE shipins   WITH "EXTWHNO*"+ordhead.ExtWarehouseNum+CHR(13)                In xpt
    	REPLACE shipins   WITH shipins+"FULFILLOC*"    +ordhead.FulfillmentLoc+CHR(13)   in xpt
    	REPLACE shipins   WITH shipins+"LONG_SHIPREF*" +ordhead.OrdInvoiceNum+CHR(13)    in xpt
    	REPLACE shipins   WITH shipins+"LONG_CNEEREF*" +ordhead.CustOrdNum+CHR(13)       in xpt
    	REPLACE shipins   WITH shipins+"BILLTOCUSTID*" +ordhead.billtocustid+CHR(13)     in xpt
    	REPLACE shipins   WITH shipins+"BILLTONAME*"   +ordhead.billtoname+CHR(13)       in xpt
    	REPLACE shipins   WITH shipins+"BILLTOADDR1*"  +ordhead.billtoaddr1+CHR(13)      in xpt
    	REPLACE shipins   WITH shipins+"BILLTOADDR2*"  +ordhead.billtoaddr2+CHR(13)      in xpt
    	REPLACE shipins   WITH shipins+"BILLTOADDR3*"  +ordhead.billtoaddr3+CHR(13)      in xpt
    	REPLACE shipins   WITH shipins+"BILLTOCITY*"   +ordhead.billtocity+CHR(13)       in xpt
    	REPLACE shipins   WITH shipins+"BILLTOSTATE*"  +ordhead.billtostate+CHR(13)      in xpt
    	REPLACE shipins   WITH shipins+"BILLTOCOUNTRY*"+ordhead.billtocountry+CHR(13)    in xpt
    	REPLACE shipins   WITH shipins+"BILLTOZIP*"    +ordhead.billtozip+CHR(13)        in xpt
    	REPLACE shipins   WITH shipins+"BILLTOCSZ*"    +Alltrim(ordhead.billtocity)+", "+Alltrim(ordhead.billtostate)+" "+alltrim(ordhead.billtozip)+CHR(13)        in xpt
    	REPLACE shipins   WITH shipins+"BILLTOPHONE*"  +ordhead.billtophone+CHR(13)      in xpt
    	REPLACE shipins   WITH shipins+"CARRIERCODE*"  +ordhead.carriercode+CHR(13)      in xpt
    	REPLACE shipins   WITH shipins+"CARRIERMODE*"  +ordhead.carriermode+CHR(13)      in xpt
    	REPLACE shipins   WITH shipins+"INVTOTAL*"     +ordhead.invoicetotal+CHR(13)     in xpt
    	REPLACE shipins   WITH shipins+"TOTALRETAIL*"  +ordhead.totalretail+CHR(13)      in xpt
    	REPLACE shipins   WITH shipins+"TOTALFREIGHT*" +ordhead.totalfreight+CHR(13)     in xpt
    	REPLACE shipins   WITH shipins+"TOTALTAX*"     +ordhead.totaltax+CHR(13)         in xpt
    	REPLACE shipins   WITH shipins+"GIFTWRAPCHG*"  +ordhead.totalgiftwrapchg+CHR(13) in xpt
    	REPLACE shipins   WITH shipins+"TOTALCUSTOM*"  +ordhead.totalcustom+CHR(13)      in xpt
    	REPLACE shipins   WITH shipins+"TENDERREF*"    +ordhead.tenderref+CHR(13)        in xpt
    	REPLACE shipins   WITH shipins+"ADDLSHIPFLAG*" +ordhead.AddlShipmentFlag+CHR(13) in xpt
    	REPLACE shipins   WITH shipins+"FILEDATE*"     +ordhead.filecreatedate+CHR(13)   in xpt
    	REPLACE shipins   WITH shipins+"PACKSLIPDATA*" +ordhead.packslipdata+CHR(13)     in xpt


*SET STEP ON 



		SELECT orddetail
		LOCATE FOR OrdInvoiceNum = ordhead.OrdInvoiceNum
		IF !EMPTY(orddetail.GiftWrapID) 
			SELECT xpt
			REPLACE shipins WITH shipins + CHR(13) + "GIFTWRAPID*"+orddetail.GiftWrapID			
		ENDIF 
		
		SELECT MIN(ordlinenum) as useline FROM ordComment WHERE OrdInvoiceNum = ordhead.OrdInvoiceNum AND ;
		                               CommentType   = 'G' ;
			INTO CURSOR minLin
		
		SELECT * FROM ordComment WHERE OrdInvoiceNum = ordhead.OrdInvoiceNum AND ;
		                               CommentType   = 'G' AND ordlinenum = minLin.useline;
			INTO CURSOR coms
		comLines = RECCOUNT()
			
		IF (comLines > 0)
			SELECT xpt 
			REPLACE shipins WITH shipins + CHR(13) + "MSGLINES*"+TRANSFORM(comLines)
		ENDIF 
		SELECT coms
		GO TOP 
		comCount = 1
		SCAN
			SELECT xpt
			REPLACE shipins WITH shipins+ CHR(13)+"MSG_"+TRANSFORM(comCount)+"*"+RTRIM(coms.comment)
			comCount = comCount+1
		endscan		
				
		SELECT xpt
		IF ordhead.ExpediteFlag = 'Y'
			REPLACE batch_num WITH "ECOMEXPD"
*!*			REPLACE ship_via  WITH "UPS-ECOM-EXPD"
		ELSE 
			REPLACE batch_num WITH "ECOM"
*!*			REPLACE ship_via  WITH "UPS-ECOM"
		ENDIF
		
		DO CASE 
			CASE ordhead.carriercode = "UPSX"
				REPLACE ship_via WITH "UPS-ECOM-1-DAY"
			CASE ordhead.carriercode = "UPS2"
				REPLACE ship_via WITH "UPS-ECOM-2-DAY"
			CASE ordhead.carriercode = "UPS3"
				REPLACE ship_via WITH "UPS-ECOM-3-DAY"
			CASE ordhead.carriercode = "BLNX"
				REPLACE ship_via WITH "BORDERLINX"
			OTHERWISE 
				REPLACE ship_via WITH "UPS-ECOM"
		ENDCASE 
		
		REPLACE updateby  WITH "DWACHS"
		REPLACE scac      WITH "UPSN"

		ordQty = 0
*!*			SET STEP ON
*!*		SELECT * FROM ordDetail WHERE OrdInvoiceNum = ordhead.OrdInvoiceNum INTO CURSOR od


*		SELECT ordDetail.*, style, color, id FROM ordDetail ;
		LEFT JOIN upcmast ON ordDetail.partnum = upcmast.upc ;
			WHERE OrdInvoiceNum = ordhead.OrdInvoiceNum INTO CURSOR od		
		*SELECT od
    Select ordDetail
		GO TOP 
		Scan For OrdInvoiceNum = ordhead.OrdInvoiceNum
			SELECT xptdet
			APPEND BLANK
			REPLACE ptdetid	  WITH lnptdetid IN xptdet
			REPLACE ptid      WITH lnptid In xptdet
			REPLACE accountid WITH MJAcct In xptdet
			REPLACE office    WITH MJOffice In xptdet
			REPLACE mod       WITH MJMod In xptdet
			
			ptInt = VAL(ordhead.CustOrdNum)
			IF ptInt > 0
				ptStr = TRANSFORM(ptInt)
			ELSE
				ptStr = RIGHT(RTRIM(ordhead.CustOrdNum), 20) 
			ENDIF 
						
*!*			REPLACE ship_ref  WITH RIGHT(ordhead.CustOrdNum,20) In xptdet
			REPLACE ship_ref  WITH ptStr                In xptdet
			REPLACE linenum   WITH ordDetail.OrdLineNum IN xptdet
			REPLACE units     WITH .t.                  IN xptdet
			REPLACE upc       WITH ordDetail.PartNum    IN xptdet
			REPLACE custsku   WITH ordDetail.PartNum    IN xptdet

    		xsqlexec("select * from upcmast where accountid = 6303 and upc = '"+xptdet.upc+"'",,,"wh")

      		If Reccount("upcmast") = 0
        		Messagebox("no UPC value",0,"UPClookup")
        		close data all
        		return
      		Endif 

      		REPLACE style      WITH upcmast.style    IN xptdet
      		REPLACE color      WITH upcmast.color    IN xptdet
      		REPLACE id         WITH upcmast.id       IN xptdet
			REPLACE totqty     WITH ordDetail.OrdQty IN xptdet
      		REPLACE origqty    WITH ordDetail.OrdQty IN xptdet
			REPLACE pack       WITH '1'              IN xptdet
      		REPLACE printstuff WITH printstuff+"RETAIL*"+ordDetail.Retail+Chr(13)           IN xptdet
      		REPLACE printstuff WITH printstuff+"TAX*"   +ordDetail.Tax+Chr(13)              IN xptdet
      		REPLACE printstuff WITH printstuff+"CREDIT*"+ordDetail.Credit+Chr(13)           IN xptdet
      		REPLACE printstuff WITH printstuff+"CUSTOM*"+ordDetail.Customization+Chr(13)    IN xptdet
      		REPLACE printstuff WITH printstuff+"GIFTWRAPCHG*"+ordDetail.GiftWrapChg+Chr(13) IN xptdet
      		REPLACE printstuff WITH printstuff+"GIFTWRAPID*"+ordDetail.GiftWrapID+Chr(13)   IN xptdet

			REPLACE updateby   WITH "DWACHS" IN xptdet
			
			SELECT * FROM ordComment WHERE OrdInvoiceNum = ordDetail.OrdInvoiceNum AND ;
			                               OrdLineNum    = ordDetail.OrdLineNum    AND ;
			                               CommentType   = 'G' ;
			INTO CURSOR coms
			comLines = RECCOUNT()
			
			IF (comLines > 0)
				SELECT xptdet 
				REPLACE printstuff WITH printstuff + CHR(13) + "COMMENTS*"+TRANSFORM(comLines) In xptdet
			ENDIF 
	
  			SELECT coms
			GO TOP 
			comCount = 1
			SCAN
				SELECT xptdet
				REPLACE printstuff WITH printstuff + CHR(13)+"COMMENT_"+TRANSFORM(comCount)+"*"+RTRIM(coms.comment) In xptdet
				comCount = comCount+1
			endscan
			ordQty = ordQty + ordDetail.OrdQty
			lnptdetid = lnptdetid + 1
		ENDSCAN
	
		SELECT xpt
		REPLACE qty WITH ordQty
		REPLACE origqty WITH ordQty
		lnptid = lnptid + 1
	ENDSCAN 

	batchID = trailerRec.BatchID
	
	myTimestamp = TTOC(DATETIME(),1)

	SELECT ordinvoicenum, myTimeStamp as TimeStamp, COUNT(*) as ordLines, 00 as ordComments, batchID as Batch ;
	FROM orddetail ;
	GROUP BY ordinvoicenum ; 
	INTO CURSOR ack readwrite             

********************************************************************************************************8
** now the order acknoledgement file
********************************************************************************************************8
	SELECT ack


SET STEP ON && Look for me


	SCAN
		SELECT COUNT(*) as thisCnt ;
		FROM ordComment ;
		WHERE ordComment.OrdInvoiceNum = ack.Ordinvoicenum ;
		INTO CURSOR comnt
	
		SELECT comnt
		GO TOP 
		REPLACE ordComments WITH comnt.thisCnt IN ack
	ENDSCAN

	ackFile = ackDir + 'ESSORDACK' + myTimeStamp + '.dat'
	SELECT ack
	COPY TO (ackFile) DELIMITED WITH "" WITH CHARACTER '|'
	WAIT TIMEOUT 1
ENDFOR
********************************************************************************************************8

SELECT xpt
BROWSE LAST NOWAIT 
SELECT xptdet
BROWSE LAST NOWAIT 

*************************************************************************
** Last second edit checks                                               
*************************************************************************
**SELECT ship_ref, COUNT(ship_ref) FROM xpt GROUP BY ship_ref


set step On 
*!* Do all940_import
all940_import(.t.)