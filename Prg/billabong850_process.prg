*!* m:\dev\prg\billabong850_process.prg
Wait Window "Now in PROCESS Phase..." Nowait
If lTesting
*SET STEP ON
Endif

Cd &lcPath
*!*	lc997Path = 'F:\FTPUSERS\BILLABONG\997IN\'

LogCommentStr = "LOADED"

ll940test = .f.
If ll940test
  xpath = "F:\FTPUSERS\Billabong\850IN\850Test\"
  lnNum = Adir(tarray,xpath+"*.*")
Else
  lnNum = Adir(tarray,"*.*")
Endif

If lnNum = 0
	Wait Window At 10,10 "      No "+cCustname+" 850s to import     " Timeout 3
	NormalExit = .T.
	Throw
Endif

*ASSERT .F. MESSAGE "At initial input file loop"

For thisfile = 1  To lnNum
  If ll940test
    xfile = xpath+tarray[thisfile,1] && +"."
  Else
    xfile = lcPath+tarray[thisfile,1] && +"."
  Endif
	cfilename = Alltrim(Lower(tarray[thisfile,1]))
	nFileSize = Val(Transform(tarray(thisfile,2)))

	Wait Window "" &message_timeout2

	Wait Window "Importing file: "+cfilename &message_timeout2

	lcStr = Filetostr(Xfile)
	cDelimiter = Substr(lcStr,4,1)
	Release lcStr
	Do m:\dev\prg\createx856a
	Select x856
	Append From &Xfile Type Delimited With Character &cDelimiter
	Locate
	If lTesting
*	BROWSE TIMEOUT 10
	Endif

	ArchiveFile  = (lcArchivePath+cfilename)
	Copy File [&xfile] To [&ArchiveFile]
	c850XferFile = ("f:\ftpusers\billabong\850xfer\"+cfilename)

	If !lTesting
		Copy File [&xfile] To [&c850XferFile]  && Creates a copy for 997s to run from
	Endif

	Do create_pt_cursors With cUseFolder,lLoadSQL  && Added to prep for SQL Server transition

	Do ("m:\dev\prg\"+cCustname+"850_bkdn")


*!*	**ADDED TMARG 09/14/17 to update po box to USPS
*!*	USE f:\uncodes\uncodes.dbf IN 0
*!*	SELECT DISTINCT STATE FROM UNCODES WHERE CNTRYCODE='US' INTO CURSOR T1 READWRITE
*!*	DELETE FOR  STATE=' '
*!*	SELECT xpt
*pos1=ATC(',',csz)
*SUBSTR(csz,pos1+2,2)

*!*	select t1
*!*	scan 
*!*	select xpt
*!*	locate for ALLTRIM(SUBSTR(csz,pos1+2,2))=ALLTRIM(t1.state)
*!*	SET STEP ON 
*!*	if found()
*!*		Replace ship_via With 'US POSTAL SERVICE', scac With 'USPS' For batch_num !='TD' and inlist(scac,'FEDZ','FD',' FEG','UP')  And address='PO BOX' In xpt
*!*		Replace ship_via With 'US POSTAL SERVICE', scac With 'USPS' For batch_num !='TD' and inlist(scac,'FEDZ','FD',' FEG','UP')  And address='P.O .BOX' In xpt
*!*		Replace ship_via With 'US POSTAL SERVICE', scac With 'USPS' For batch_num !='TD' and inlist(scac,'FEDZ','FD',' FEG','UP')  And address='PO. BOX' In xpt
*!*		Replace ship_via With 'US POSTAL SERVICE', scac With 'USPS' For batch_num !='TD' and inlist(scac,'FEDZ','FD',' FEG','UP')  And address='P.O BOX' In xpt
*!*		Replace ship_via With 'US POSTAL SERVICE', scac With 'USPS' For batch_num !='TD' and inlist(scac,'FEDZ','FD',' FEG','UP')  And address='P O BOX' In xpt
*!*		Replace ship_via With 'US POSTAL SERVICE', scac With 'USPS' For batch_num !='TD' and inlist(scac,'FEDZ','FD',' FEG','UP')  And address='P.O.BOX' In xpt
*!*		Replace ship_via With 'US POSTAL SERVICE', scac With 'USPS' For batch_num !='TD' and inlist(scac,'FEDZ','FD',' FEG','UP')  And address='PO BOX' In xpt
*!*		Replace ship_via With 'US POSTAL SERVICE', scac With 'USPS' For batch_num !='TD' and inlist(scac,'FEDZ','FD',' FEG','UP')  And address='P.O.  BOX' In xpt
*!*	 endif
*!*	endscan

	Do m:\dev\prg\all940_import

Next thisfile

Wait Clear

If !lTesting
&& now clean up the 850in archive files, delete for the upto the last 10 days
	deletefile(lcArchivePath,10)
Endif

Wait "Entire "+cCustname+" 850 Process Now Complete" Window &message_timeout2
NormalExit = .T.
