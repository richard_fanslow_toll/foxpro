

close data all
set date american
set tableprompt off
set tablevalidate to 0
set deleted on
set enginebehavior 70
set exclusive off
set escape on
set exact off
set safety off
set multilocks on
set talk off
sys(3054,0)
set asserts on
on escape cancel

set deleted on

use f:\wh\sqlpassword in 0
gsqlpassword = alltrim(sqlpassword.password)
use in sqlpassword

cserver = "tgfnjsql01"
sqlsetprop(0,'DispLogin',3)
sqlsetprop(0,"dispwarnings",.f.)

lcdsnless="driver=SQL Server;server=&cServer;uid=SA;pwd=&gsqlpassword;DATABASE=PICKPACK"
nhandle=sqlstringconnect(m.lcdsnless,.t.)
if nhandle<=0 && bailout
	wait window at 6,2 "No Data Connection.."+chr(13)+" Call MIS...."
	lcquery = [Could not make connection to sp3 SQL server]+chr(13)+"   "+lcdsnless
	throw
	return
endif

goffice="K"

xsqlexec("select * from pt where mod='K'",,,"wh")
xsqlexec("select * from ptdet where mod='K'",,,"wh")
index on ptid tag ptid
set order to

use f:\3pl\data\edi_trigger in 0

select pt.ship_ref as ordernumber, {  /  /    } as del_date,sum(ptdet.totqty) as ordered,0000 as shipped,0000 as qty945,space(6) as editrig from pt left outer join ptdet on ptdet.ptid = pt.ptid;
where pt.accountid = 6532 group by 1 into cursor temp1 readwrite


use f:\whk\whdata\outship in 0
use f:\whk\whdata\outdet in 0

select outship.ship_ref as ordernumber,del_date,sum(outdet.origqty) as ordered,sum(outdet.totqty) as shipped,0000 as qty945,space(6) as editrig from outship left outer join outdet on outdet.outshipid = outship.outshipid;
where outship.accountid = 6532 and outdet.units=.t. and (emptynul(del_date) or del_date >= date()-31) and cancelrts=.f. and bol_no != "NOTHING SHIPPED" group by 1 into cursor temp2 readwrite


select temp2
scan
	if emptynul(del_date)
		replace shipped with 0
	endif
endscan

select temp2
scan
	select temp2
	scatter memvar
	select temp1
	append blank
	gather memvar
endscan

select temp1
select * from temp1 into cursor temp2 order by del_date

select * from temp1 where emptynul(del_date) or del_date >= date()-31 into cursor temp2 readwrite order by ordernumber

select temp2
scan for !emptynul(del_date)
	select edi_trigger
	locate for accountid=6532 and ship_ref = temp2.ordernumber
	if !found()
		replace temp2.editrig with "NO"
	endif

	if found() and fin_status = "945 CREATED"
		replace temp2.editrig with "OK"
	else
		replace temp2.editrig with "ERROR"
	endif
endscan

select temp2
scan for !emptynul(del_date)
	if usesql()
		xsqlexec("select ship_ref, sum(totqty) as shipqty from cartons where ship_ref='"+temp2.ordernumber+"' " + ;
			"and accountid=6532 group by ship_ref","tempdata",,"pickpack")
	else
		lcquery  = [select ship_ref,Sum(totqty) as shipqty from cartons where ship_ref=']+temp2.ordernumber+[' and accountid = 6532 group by ship_ref]
		if sqlexec(nhandle,lcquery,"tempdata")#1
			wait window at 6,2 "Error Cartons SQL query.........." timeout 1
			sqldisconnect(nhandle)
		endif
	endif
	if reccount("tempdata") >= 1
		replace temp2.qty945 with tempdata.shipqty in temp2
	else
		replace temp2.qty945 with 0 in temp2
	endif
endscan

select temp2

select * from temp2 where !emptynul(del_date) and editrig!= "OK" into cursor edierrs
select * from temp2 where !emptynul(del_date) and shipped != qty945 into cursor ediqtyerrs

if reccount("edierrs") >=1 or reccount("ediqtyerrs") >=1
	cerrmsg = ""
	select edierrs
	scan
		cerrmsg = cerrmsg+ordernumber+" Deldate: "+dtoc(del_date)+"   Ordered:"+transform(ordered)+"   Shipped: "+transform(shipped)+"   945 qty:"+transform(qty945) +"   EDI Status:  "+editrig+chr(13)
	endscan
	select ediqtyerrs
	scan
		cerrmsg = cerrmsg+ordernumber+" Deldate: "+dtoc(del_date)+"   Ordered:"+transform(ordered)+"   Shipped: "+transform(shipped)+"   945 qty:"+transform(qty945) +"   EDI Status:   "+editrig+chr(13)
	endscan
	tfrom ="TOLL EDI Operations <toll-edi-ops@tollgroup.com>"
	tcc="pgaidis@fmiint.com"
	tmessage="Below Ariat deliveries may have some issues "+chr(13)+"-----------------------------------------------------------------------------------"+chr(13)+cerrmsg
	tsendto ="pgaidis@fmiint.com"
	tattach=""
	do form m:\dev\frm\dartmail2 with tsendto,tfrom,"Ariat Louisville 945\Delivery Issues",tcc,tattach,tmessage,"A"
else
	tfrom ="TOLL EDI Operations <toll-edi-ops@tollgroup.com>"
	tcc="pgaidis@fmiint.com"
	tmessage="All Ariat deliveries NO Issues ! "
	tsendto ="pgaidis@fmiint.com"
	tattach  = ""
	do form m:\dev\frm\dartmail2 with tsendto,tfrom,"Ariat Louisville -- No  945\Delivery Issues",tcc,tattach,tmessage,"A"

endif

select temp2
select ordernumber,del_date,ordered,shipped from temp2 order by ordernumber into cursor temp3

select temp3
copy to h:\fox\ariat_del_status.csv type csv
*Return

tattach  = "h:\fox\ariat_del_status.csv"
tfrom ="TOLL EDI Operations <toll-edi-ops@tollgroup.com>"
tcc="pgaidis@fmiint.com"
tmessage="See attached report.........for any changes contact Paul Gaidis"
tsendto ="Mack.Osswald@Ariat.Com,Kay.Diack@Ariat.Com,Joshua.Milos@Ariat.Com"
*tsendto ="pgaidis@fmiint.com"
do form m:\dev\frm\dartmail2 with tsendto,tfrom,"Ariat Louisville Warehouse Delivery Stats",tcc,tattach,tmessage,"A"

*DO FORM m:\dev\frm\dartmail2 WITH "pgaidis@fmiint.com",tFrom,"Ariat Louisville Warehouse Delivery Stats",tcc,tattach,tmessage,"A"