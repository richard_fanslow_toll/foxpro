Wait Window "Now in IMPORT Phase..." Nowait
EmailcommentStr = ""

cOfficeImp = Iif(cOffice = "N","I",cOffice)
nUploadCount = 0
m.acct_num = nAcctNum
cPTString = ""

If lTestImport Or lTesting
  cUseFolder = "F:\WHP\WHDATA\"
Endif

guserid = "ALL940RV"
SET STEP ON 
useca("pt","wh")
useca("ptdet","wh")

If Used('outship')
  Use In outship
Endif

Select xpt
Set Order To
Locate
lnCount = 0
Store Reccount() To lnCount
Wait Window "Picktickets to load ...........  "+Str(lnCount) Nowait &&TIMEOUT 2

If nAcctNum = 6561
  cPTString = Padr("CONSIGNEE",42)+Padr("PO NUM",27)+Padr("CANCEL",12)+Padr("PICKTICKET",22)+Padr("QTY",6)+"SALESORDER"+Chr(13)
Else
  cPTString = Padr("CONSIGNEE",42)+Padr("PO NUM",27)+Padr("CANCEL",12)+Padr("PICKTICKET",22)+"QTY"+Chr(13)
Endif

Assert .F. Message "At PT upload scan"

gMasterOffice = Iif(Inlist(cOffice,"J","I"),"N",cOffice)
gOffice = cMod

** init some variables for the extra PTID verification  PG 04/04/2017
xptid =0
xdisaster = .F.

Scan
  Wait "AT XPT RECORD "+Alltrim(Str(Recno())) Window Nowait Noclear
*!* Check for existence of PT in outship
*!* First choice does a select by PT

    csq1 = [select * from outship where accountid = ]+Transform(nAcctNum)+[ and office = ']+cOfficeImp+[' and ship_ref = ']+Padr(Alltrim(xpt.ship_ref),20)+[']
    xsqlexec(csq1,"tempos1",,"wh",,,,,,.T.)
    If Reccount('tempos1')>0
      Wait Window "Found in outship...looping" Nowait
      Use In tempos1
      Select xpt
      Loop
    Endif

  Select pt
  xShip_ref = xpt.ship_ref
  lcQuery = [select * from pt where accountid = ]+Transform(nAcctNum)+[ and ship_ref = ']+xShip_ref+[']
  xsqlexec(lcQuery,"p1",,"wh")
  Select p1
  Locate
  If Reccount('p1') > 0
    Select xpt
    Loop
  Endif
  Use In p1

  Select xpt
  Scatter Memvar Memo Blank

  If nAcctNum = 6561
    cPTString = cPTString+Chr(13)+Padr(Allt(xpt.consignee),42)+Padr(Alltrim(xpt.cnee_ref),27)+Padr(Dtoc(xpt.Cancel),12)+Padr(Alltrim(xpt.ship_ref),22)+Padr(Alltrim(Str(xpt.qty)),6)+cCO
  Else
    cPTString = cPTString+Chr(13)+Padr(Allt(xpt.consignee),42)+Padr(Alltrim(xpt.cnee_ref),27)+Padr(Dtoc(xpt.Cancel),12)+Padr(Alltrim(xpt.ship_ref),22)+Alltrim(Str(xpt.qty))
  Endif
  nPTID = xpt.ptid
  Scatter Fields Except ptid Memvar Memo
  m.adddt = Datetime()
  m.addby = "TGF-PROC"
  m.addproc = "940UPLOAD"

  Select pt
  m.office = cOffice
  m.mod = cMod
  insertinto("pt","wh",.T.)

  If xptid!=0 And xptid=pt.ptid && check to make sure that xptid has changed since the last scan loop, if not then abort and alert
    xdisaster = .T.
  Endif

  nUploadCount = nUploadCount  +1

  Select xptdet
  Scatter Memvar Memo Blank
  Scan For ptid=xpt.ptid
    Scatter Memvar Memo
    If !lTesting
      m.office = cOffice
      m.mod = cMod
    Endif
    m.accountid = xpt.accountid
    m.ship_ref = xpt.ship_ref
    m.adddt = Datetime()
    m.addby = "TGF-PROC"
    m.addproc = "940UPLOAD"
    m.ptid=pt.ptid
    insertinto("ptdet","wh",.T.)
  Endscan
  xptid= pt.ptid

  If !xdisaster
    If !tu("pt")  && something went wrong during the tabelupdate most likely a PTID issue
      tsubject= "EDI PT Upload Error, Table Update Error for "+cCustname+" Pickticket Upload: " +Ttoc(Datetime())
      tattach = ""
      tmessage = "PT upload error for "+cCustname+Chr(13)+"From File: "+cfilename+Chr(13)+"and PTID = "+Transform(xptid)
      tFrom ="TGF EDI Operations <toll-edi-ops@tollgroup.com>"
      tsendto = "pgaidis@fmiint.com,joe.bianchi@tollgroup.com,Todd.Margolin@tollgroup.com,Mike.Winter@tollgroup.com"
      tcc = "darren.young@tollgroup.com"
      Do Form m:\dev\frm\dartmail2 With tsendto,tFrom,tsubject,tcc,tattach,tmessage,"A"
      Return

    Else
      If !tu("ptdet")
        Delete In pt
        tu("pt")
        tsubject= "EDI PTDET Upload Error, MIxed PTDETIDs for "+cCustname+" Pickticket Upload: " +Ttoc(Datetime())
        tattach = ""
        tmessage = "PTDET upload error for "+cCustname+Chr(13)+"From File: "+cfilename+Chr(13)+"and PTID = "+Transform(xptid)
        tFrom ="TGF EDI Operations <toll-edi-ops@tollgroup.com>"
        tsendto = "pgaidis@fmiint.com,joe.bianchi@tollgroup.com,Todd.Margolin@tollgroup.com,Mike.Winter@tollgroup.com"
        tcc = "darren.young@tollgroup.com"
        Do Form m:\dev\frm\dartmail2 With tsendto,tFrom,tsubject,tcc,tattach,tmessage,"A"
        Return
      Endif
    Endif
  Endif

  If xdisaster
    tsubject= "EDI PT Upload Error, MIxed PTIDs for "+cCustname+" Pickticket Upload: " +Ttoc(Datetime())
    tattach = ""
    tmessage = "PT upload error for "+cCustname+Chr(13)+"From File: "+cfilename+Chr(13)+"and PTID = "+Transform(xptid)
    tFrom ="TGF EDI Operations <toll-edi-ops@tollgroup.com>"
    tsendto = "pgaidis@fmiint.com,joe.bianchi@tollgroup.com,Todd.Margolin@tollgroup.com,Mike.Winter@tollgroup.com"
    tcc = "darren.young@tollgroup.com"
    Do Form m:\dev\frm\dartmail2 With tsendto,tFrom,tsubject,tcc,tattach,tmessage,"A"
    Return
  Endif
&& OK now process the next pickticket
Endscan

Select ptid,ship_ref From ptdet Group By ptid,ship_ref Into Cursor xpts

If nUploadCount > 0
  pthist("P")
  pthist("E")
Endif

Select xpt
Locate
pickticket_num_start = xpt.ship_ref
Goto Bott
pickticket_num_end = xpt.ship_ref
Locate
Copy File [&xfile] To [&ArchiveFile]

Delete File [&xfile]

If nUploadCount > 0
  Wait Window "Picktickets uploaded...........  "+Str(nUploadCount) Nowait &&TIMEOUT 2
Else
  Wait Window "No New Picktickets uploaded from file "+xfile Nowait &&TIMEOUT 2
Endif

Wait "940 Import Round Complete" Window Nowait && TIMEOUT 2
Wait Clear

Use In pt
Use In ptdet
If Used("outship")
  Use In outship
Endif

currfile = xfile
Do Case
Case cOffice = "C"
  cOfficeGroup = "SP"
Case Inlist(cOffice,"L","Z")
  cOfficeGroup = "ML"
Case Inlist(cOffice,"K","S")
  cOfficeGroup = "KY"
Case Inlist(cOffice,"J","I","N")
  cOfficeGroup = "NJ"
Case cOffice = "M"
  cOfficeGroup = "FL"
Case Inlist(cOffice,"X","Y")
  cOfficeGroup = "CR"
Otherwise
  Select 0
  Use F:\3pl\Data\mailmaster Alias mm Shared
  Locate For edi_type = "MISC" And taskname = "GENERAL"
  tsendto = Alltrim(Iif(mm.use_alt,mm.sendtoalt,mm.sendto))
  tcc = Alltrim(Iif(mm.use_alt,mm.ccalt,mm.cc))
  Use In mm
  cOfficeGroup = "XX"
Endcase
If lTesting
  tcc = ""
Endif

EmailcommentStr = EmailcommentStr+Chr(13)+cPTString

*************************************************************************************************
*!* E-Mail process
*************************************************************************************************

Assert .F. Message "At email"
If nUploadCount > 0
  cPTQty = Allt(Str(nUploadCount))
*SET STEP ON
  If lEmail
    If cOfficeGroup = "XX"
      tcc = ""
    Endif
    cMailLoc = Icase(Inlist(cOffice,"L","Z"),"Mira Loma",Inlist(cOffice,"K","S"),"Louisville",Inlist(cOffice,"Y","Z"),"Carson",cOffice = "M","Miami",Inlist(cOffice,"I","N","J"),"New Jersey","San Pedro")
    tsubject= "TGF "+cMailLoc+" "+cCustname+" Pickticket Upload: " +Ttoc(Datetime())
    tattach = ""
    tmessage = "Uploaded "+cPTQty+" Picktickets for "+cCustname+Chr(13)+"From File: "+cfilename+Chr(13)
    tmessage = tmessage + EmailcommentStr
    tFrom ="TGF EDI Operations <toll-edi-ops@tollgroup.com>"
    If lTesting Or lTestImport
      tmessage = tmessage+Chr(13)+"*TEST DATA* - LOADED INTO F:\WHP TABLES"
    Endif
    If nAcctNum != 6532
      Do Form m:\dev\frm\dartmail2 With tsendto,tFrom,tsubject,tcc,tattach,tmessage,"A"
    Endif
  Endif
  nUploadCount = 0
  cPTQty = ""
Endif
*************************************************************************************************

Return
