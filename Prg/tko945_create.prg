*!* TKO 945 (Whse. Shipping Advice) Creation Program
*!* Creation Date: 12.03.2008 by Joe (Derived from Cleatskins 945_create program)

PARAMETERS accountid,cBOL,cShip_ref,cOffice
ASSERT .F. MESSAGE "Now at start of TKO STANDARD 945 process"
WAIT WINDOW "" TIMEOUT 2


&& do m:\dev\prg\tko945_create With 5836,"00005836123456780","","C"   

PUBLIC ARRAY thisarray(1)
PUBLIC c_CntrlNum,c_GrpCntrlNum,cReturned,cWO_Num,cWO_NumStr,nWO_Num1,lTesting,lDoUPC,lTestmail,cStatus,lTestinput
PUBLIC lEmail,lFilesout,dDateTimeCal,nFilenum,nWO_Xcheck,nOutshipid,cCharge,lDoSQL,dapptnum,cRefBOL
PUBLIC cCity,cState,cZip,nOrigSeq,nOrigGrpSeq,tsendto,tcc,tsendtoerr,tccerr,cProgname,cRolledRecs
PUBLIC cErrMsg,cStatus,ccustname,cFilenameShort,cFilenameArch,cFilenameOut,lOverflow
PUBLIC cMod,cMBOL,cEDIType,lParcelType,cFilenameShort,cFilenameOut,cFilenameArch,lcPath,cWO_NumList,cISA_Num

cErrMsg ="Nothing Yet"
cStatus ="UNK"
ccustname= "TKO"
cMBOL = ""
lParcelType = .F.

SET DELETED ON

lTesting   = .F.  && Set to .t. for testing
lTestinput = .F. &&lTesting &&  Set to .t. for test input files only!
*ON ERROR error_routine()
IF lTesting
	CLOSE DATABASES ALL
ENDIF

DO m:\dev\prg\_setvars WITH lTesting
cProgname = "tko945_create"

lDoSQL = .T.
lPick = .F.
lPrepack = .F.
lJCP = .F.
lIsError = .F.
lDoCatch = .T.
cShip_ref = ""
lCloseOutput = .T.
nFilenum = 0
cCharge = "0.00"
cWO_NumStr = ""
cString = ""
cErrMsg = ""
nPTCtnTot = 0
*cRefBOL = '04070085836398917'
cRefBOL = '99999999999999999'

*!* Note: For Wal-mart testing, be sure that there is a BOL and DEL_DATE filled in on the test OUTSHIP table,
*!* and that it matches the test data below

TRY
	lFederated = .F.
	IF VARTYPE(accountid)="L" AND lTesting
		nAcctNum = 5836
	ELSE
		STORE accountid TO nAcctNum
	ENDIF
	nOrigSeq = 0
	nOrigGrpSeq = 0
	cPPName = "TKO"
	WAIT WINDOW "At the start of "+cPPName+" 945 process..." NOWAIT
	tfrom = "TGF EDI Operations <edi-ops@tollgroup.com>"
	tmessage=""
	tcc = ""
	lDoBOLGEN = .F.
	lSamples = .F.
	cBOL1 = ""
	lISAFlag = .T.
	lSTFlag = .T.
	nLoadid = 1
	lSQLMail = .F.
	lOverflow = .F.

  cRetMsg = "X"

  lEmail = .T.
  lTestmail = lTesting && Sends mail to Joe only
  lFilesout = !lTesting  && If true, copies output to FTP folder (default = .t.)
  lStandalone = lTesting

	IF TYPE("cOffice") = "L"
		IF !lTesting AND !lTestinput
			WAIT WINDOW "Office not provided...terminating" TIMEOUT 2
			lCloseOutput = .F.
			cErrMsg = "NO OFFICE"
			DO ediupdate WITH cErrMsg,.T.
			THROW
		ELSE
			CLOSE DATABASES ALL
			DO m:\dev\prg\lookups
			IF !USED('edi_trigger')
				cEDIFolder = "F:\3PL\DATA\"
				USE (cEDIFolder+"edi_trigger") IN 0 ALIAS edi_trigger
			ENDIF
*!* TEST DATA AREA
			nAcctNum = 5836
			cBOL = "04917525836099206" &&
			cShip_ref = ""
			cOffice = "C"
			cTime = DATETIME()
			lFedEx = .F.
		ENDIF
	ENDIF
	cOffice = UPPER(cOffice)
	cMod = Icase(cOffice = "C","2",cOffice = "N","I","M")
	gOffice = cMod
	gMasteroffice = cOffice

	cEDIType = "945"

	CREATE CURSOR temp945 (accountid i,isa_num c(10),st_num c(10),wo_num i,bol_no c(20),ship_ref c(20),filename c(50),filedate T)
	cBOL=TRIM(cBOL)
*	ASSERT .F. MESSAGE "At Warehouse folder selection"

*!*		lTestmail = .T.
*lFilesout = .F.

	DO m:\dev\prg\swc_cutctns WITH cBOL

	STORE "LB" TO cWeightUnit
	cfd = "*"
	csegd = "~"
	nSegCtr = 0
	cterminator = ">" && Used at end of ISA segment

	WAIT WINDOW "Now preparing tables and variables" NOWAIT NOCLEAR

*!* SET CUSTOMER CONSTANTS
	ccustname = "TKO"  && Customer Identifier
	cX12 = "004010"  && X12 Standards Set used
	cMailName = "TKO"

	IF USED('mm')
		USE IN mm
	ENDIF
	SELECT 0
	USE F:\3pl\DATA\mailmaster ALIAS mm SHARED
	LOCATE
*	ASSERT .F. MESSAGE "In mailmaster"
	IF lTestmail OR lTesting
		WAIT WINDOW "Looking up test mail recipients" TIMEOUT 2
		LOCATE FOR mm.edi_type = "MISC" AND mm.taskname = "GENERAL"
	ELSE
		LOCATE FOR mm.edi_type = "945" AND mm.office = cOffice AND mm.accountid = 5836
	ENDIF
	tsendto = IIF(mm.use_alt,mm.sendtoalt,mm.sendto)
	tcc = IIF(mm.use_alt,mm.ccalt,mm.cc)
	LOCATE
	LOCATE FOR mm.edi_type = "MISC" AND mm.taskname = "GENERAL"
	tsendtoerr = IIF(mm.use_alt,mm.sendtoalt,mm.sendto)
	tccerr = IIF(mm.use_alt,mm.ccalt,mm.cc)
	USE IN mm

*!* SET OTHER CONSTANTS
	DO CASE
		CASE INLIST(cOffice,"N","I")
			cCustLoc =  "NJ"
			cFMIWarehouse = ""
			cCustPrefix = "945j"
			cDivision = "New Jersey"
			cSF_Addr1  = "800 FEDERAL BLVD"
			cSF_CSZ    =  "CARTERET"+cfd+"NJ"+cfd+"07008"

		CASE cOffice = "M"
			cCustLoc =  "FL"
			cFMIWarehouse = ""
			cCustPrefix = "945f"
			cDivision = "Florida"
			cSF_Addr1  = "11400 NW 32ND AVE"
			cSF_CSZ    = "MIAMI"+cfd+"FL"+cfd+"33167"

		OTHERWISE
			cCustLoc =  "CA"
			cFMIWarehouse = ""
			cCustPrefix = "945c"
			cDivision = "California"
			cSF_Addr1  = "450 WESTMONT DRIVE"
			cSF_CSZ    = "SAN PEDRO"+cfd+"CA"+cfd+"90731"
	ENDCASE
	IF lTesting
		cCustPrefix = "945t"
	ENDIF

	cCustFolder = UPPER(ccustname)+"-"+cCustLoc

	lNonEDI = .F.  && Assumes not a Non-EDI 945
	STORE "" TO lcKey
	STORE 0 TO alength,nLength
	cTime = DATETIME()
	cDelTime = SUBSTR(TTOC(cTime,1),9,4)
	cCarrierType = "M" && Default as "Motor Freight"
	dapptnum = ""

*!* SET OTHER CONSTANTS
	IF INLIST(cOffice,'C','1','2')
		dDateTimeCal = (DATETIME()-(3*3600))
	ELSE
		dDateTimeCal = DATETIME()
	ENDIF
	dt1 = TTOC(dDateTimeCal,1)
	dtmail = TTOC(dDateTimeCal)
	dt2 = dDateTimeCal
	cString = ""

	csendqual = "ZZ"
	csendid = "FMIX"
	crecqual = "ZZ"
	crecid = "TKO"

	cdate = DTOS(DATE())
	cTruncDate = RIGHT(cdate,6)
	cTruncTime = SUBSTR(TTOC(dDateTimeCal,1),9,4)
	cfiledate = cdate+cTruncTime
	cOrig = "J"
	nSTCount = 0
	cStyle = ""
	cPTString = ""
	nTotCtnWt = 0
	nTotCtnCount = 0
	nUnitSum = 0
	cTargetStyle = ""

** PAD ID Codes
	csendidlong = PADR(csendid,15," ")
	crecidlong = PADR(crecid,15," ")

	lcPath = ("f:\ftpusers\"+cCustFolder+"\945OUT\")
	cFilename = ("f:\ftpusers\"+cCustFolder+"\945-Staging\"+cCustPrefix+dt1+".txt")
	cFilenameShort = JUSTFNAME(cFilename)
	cFilenameOut = (lcPath+cFilenameShort)
	cFilenameArch = ("f:\ftpusers\"+cCustFolder+"\945OUT\Archive\"+cFilenameShort)
	nFilenum = FCREATE(cFilename)

	IF USED('OUTSHIP')
		USE IN outship
	ENDIF
		csq1 = [select * from outship where accountid = ]+TRANSFORM(nAcctNum)+[ and office = ']+cOffice+[' and bol_no = ']+cBOL+[']
		xsqlexec(csq1,,,"wh")
		IF RECCOUNT() = 0
			cErrMsg = "MISS BOL "+cBOL
			DO ediupdate WITH cErrMsg,.T.
			THROW
		ENDIF
		INDEX ON bol_no TAG bol_no
		INDEX on wo_num TAG wo_num
		INDEX on ship_ref TAG ship_ref

	lOverflow = .F.
	SELECT DISTINCT ship_ref FROM outship WHERE bol_no = cBOL INTO CURSOR temp1tko
	SELECT temp1tko
	SCAN
		IF lOverflow
			EXIT
		ENDIF
		cShip_ref = ALLTRIM(temp1tko.ship_ref)
		SELECT outship
		SCAN FOR outship.ship_ref = cShip_ref
			IF "OV"$outship.ship_ref
				WAIT WINDOW "Overflow PT Found at Ship_ref: "+ALLTRIM(outship.ship_ref) TIMEOUT 5
				lOverflow = .T.
				EXIT
			ENDIF
		ENDSCAN
	ENDSCAN
	USE IN temp1tko
	cRefBOL = IIF(lOverflow,cBOL,"9999")

	SELECT outshipid ;
		FROM outship ;
		WHERE bol_no = cBOL ;
		AND accountid = nAcctNum ;
		INTO CURSOR tempsr
	SELECT tempsr
	WAIT WINDOW "There are "+ALLTRIM(STR(RECCOUNT()))+" outshipid's to check" NOWAIT

	SELECT outship
	IF !SEEK(cBOL,'outship','bol_no')
		ASSERT .F. MESSAGE "at missing BOL point"
		WAIT WINDOW "BOL not found in OUTSHIP" TIMEOUT 2
		cErrMsg = "BOL NOT FOUND"
		DO ediupdate WITH cErrMsg,.T.
		THROW
	ELSE
		nWO_num = outship.wo_num
		cSCAC = ALLTRIM(outship.scac)
		IF USED('parcel_carriers')
			USE IN parcel_carriers
		ENDIF

		SELECT 0
		USE F:\3pl\DATA\parcel_carriers ALIAS parcel_carriers
		lParcelType = IIF(SEEK(cSCAC,'parcel_carriers','scac'),.T.,.F.)
		lParcelType = IIF(ALLTRIM(cBOL)=ALLTRIM(outship.keyrec),.F.,lParcelType)
		USE IN parcel_carriers
	ENDIF
	IF !lTesting AND lParcelType=.T.
		IF !USED('tko_wohold')
			USE F:\3pl\DATA\tko_wohold IN 0
		ENDIF
		nWO_numOld = tko_wohold.wo_num
		IF nWO_num = nWO_numOld
			lDoSQL = .F.
		ELSE
			REPLACE wo_num WITH nWO_num IN tko_wohold
		ENDIF
		USE IN tko_wohold
	ENDIF


	IF USED('OUTDET')
		USE IN outdet
	ENDIF
		selectoutdet()
		SELECT outdet
		INDEX ON outdetid TAG outdetid

	IF !USED("scacs")
		xsqlexec("select * from scac","scacs",,"wh")
		INDEX ON scac TAG scac
	ENDIF

	IF !lTesting AND !lTestinput
		IF USED('SHIPMENT')
			USE IN shipment
		ENDIF

		IF USED('PACKAGE')
			USE IN package
		ENDIF

		dDate = DATE()-5
		csq1 = [select * from shipment where (right(rtrim(str(accountid)),4) in (]+gtkoaccounts+[) or accountid = 9999) and shipdate >= {]+DTOC(dDate)+[}]
		xsqlexec(csq1,,,"wh")
		INDEX ON shipmentid TAG shipmentid
		INDEX ON pickticket TAG pickticket

		SELECT shipment
		LOCATE
		IF RECCOUNT("shipment") > 0
			xjfilter="shipmentid in ("
			SCAN
				nShipmentId = shipment.shipmentid
				xsqlexec("select * from package where shipmentid="+TRANSFORM(nShipmentId),,,"wh")

				IF RECCOUNT("package")>0
					xjfilter=xjfilter+TRANSFORM(shipmentid)+","
*!*					ELSE
*!*						xjfilter="1=0"
				ENDIF
			ENDSCAN
			xjfilter=LEFT(xjfilter,LEN(xjfilter)-1)+")"

			xsqlexec("select * from package where "+xjfilter,,,"wh")
		ELSE
			xsqlexec("select * from package where .f.",,,"wh")
		ENDIF
		SELECT package
		INDEX ON shipmentid TAG shipmentid
		SET ORDER TO TAG shipmentid

		SELECT shipment
		SET RELATION TO shipmentid INTO package
	ENDIF

&& Added code blocks to the following to overcome BOL vs. WO selection issues, 12.23.2005
	IF lDoSQL
		cSQL="SQL3"
		SELECT outship
		LOCATE
		IF USED("sqlwo")
			USE IN sqlwo
		ENDIF
		IF FILE("F:\3pl\DATA\sqlwo.dbf")
			DELETE FILE "F:\3pl\DATA\sqlwo.dbf"
		ENDIF
		SELECT bol_no,wo_num ;
			FROM outship ;
			WHERE bol_no = PADR(TRIM(cBOL),20) ;
			AND INLIST(accountid,&gtkoaccounts);
			GROUP BY 1,2 ;
			ORDER BY 1,2 ;
			INTO DBF F:\3pl\DATA\sqlwo
		USE IN sqlwo
		SELECT 0
		USE F:\3pl\DATA\sqlwo ALIAS sqlwo
		LOCATE
		IF EOF()
			SET STEP ON
			cErrMsg = "NO SQL DATA"
			DO ediupdate WITH cErrMsg,.T.
			THROW
		ENDIF
		IF lTesting && OR DATETIME() < DATETIME(2010,10,21,14,00,00)
*			BROWSE
*			ASSERT .F.
		ENDIF
		cRetMsg = ""
*		ASSERT .F. MESSAGE "At SQL connect...debug"
		DO m:\dev\prg\sqlconnect_bol  WITH nAcctNum,ccustname,cPPName,.T.,cOffice,.T.
		IF cRetMsg<>"OK"
			STORE cRetMsg TO cErrMsg
			DO ediupdate WITH cErrMsg,.T.
			THROW
		ENDIF
		SELECT vtkopp
		LOCATE
		IF lTesting && OR DATETIME() < DATETIME(2010,10,21,14,00,00)
*			BROWSE
*			ASSERT .F.
		ENDIF
		IF EOF()
			WAIT WINDOW "SQL select data is EMPTY...error!" NOWAIT
			cErrMsg = "SQL DATA EMPTY"
			DO ediupdate WITH cErrMsg,.T.
			THROW
		ENDIF
	ENDIF

	SELECT outship
	LOCATE
	WAIT CLEAR
	WAIT WINDOW "Now creating Header information" NOWAIT NOCLEAR

*!* HEADER LEVEL EDI DATA
	DO num_incr_isa

	cISA_Num = PADL(c_CntrlNum,9,"0")
	nISA_Num = INT(VAL(cISA_Num))

	IF lTesting
		cISACode = "T"
	ELSE
		cISACode = "P"
	ENDIF

	STORE "ISA"+cfd+"00"+cfd+"          "+cfd+"00"+cfd+"          "+cfd+csendqual+cfd+csendidlong+cfd+;
		crecqual+cfd+crecidlong+cfd+cTruncDate+cfd+cTruncTime+cfd+"U"+cfd+"00401"+cfd+;
		cISA_Num+cfd+"0"+cfd+cISACode+cfd+cterminator+csegd TO cString
	DO cstringbreak

	STORE "GS"+cfd+"SW"+cfd+csendid+cfd+crecid+cfd+cdate+cfd+cTruncTime+cfd+c_CntrlNum+;
		cfd+"X"+cfd+cX12+csegd TO cString
	DO cstringbreak

	SELECT outship
	LOCATE

	IF !SEEK(nWO_num,'outship','wo_num')
		WAIT CLEAR
		WAIT WINDOW "Invalid Work Order Number - Not Found in OUTSHIP!" TIMEOUT 2
		cErrMsg = "WO NOT FOUND"
		DO ediupdate WITH cErrMsg,.T.
		THROW
	ENDIF

	IF !lTesting
		IF EMPTY(cBOL)  && May be empty if triggered from PT screen, or FedEx shipment
			WAIT CLEAR
			WAIT WINDOW "MISSING Bill of Lading Number!" TIMEOUT 2
			cErrMsg = "BOL# EMPTY"
			DO ediupdate WITH cErrMsg,.T.
			THROW
		ELSE
			IF !SEEK(PADR(TRIM(cBOL),20),"outship","bol_no")
				WAIT CLEAR
				WAIT WINDOW "Invalid Bill of Lading Number - Not Found!" TIMEOUT 2
				cErrMsg = "BOL# NOT FOUND"
				DO ediupdate WITH cErrMsg,.T.
				THROW
			ENDIF
		ENDIF
	ENDIF

*************************************************************************
*1	PTIDSOURCE/OUTSHIP RECORD MAIN LOOP
*	Loops through individual OUTSHIP lines for BOL#
*************************************************************************
	WAIT CLEAR
	WAIT WINDOW "Now creating BOL#-based information..." NOWAIT NOCLEAR

	SELECT outship
	SET ORDER TO
	COUNT TO N FOR outship.bol_no = cBOL AND !EMPTYnul(del_date)
	IF N=0
		cErrMsg = "INCOMP BOL, Del dates"
		DO ediupdate WITH cErrMsg,.T.
		THROW
	ENDIF

	LOCATE
	cMissDel = ""
	nPTCount = 0
	nTCnt = 0

	oscanstr = "outship.bol_no = PADR(TRIM(cBOL),20) AND !EMPTYnul(del_date)"
	SELECT outship
	SET ORDER TO TAG ship_ref
	LOCATE FOR &oscanstr

*	ASSERT .F. MESSAGE "In Outship scan"
	SCAN FOR &oscanstr
		alength = ALINES(apt,outship.shipins,.T.,CHR(13))
		SCATTER MEMVAR MEMO
		lSolidGroup = IIF("PACKGROUP*SOLIDPACK"$m.shipins,.T.,.F.)
		cShip_ref = ALLTRIM(m.ship_ref)
		cConsignee = UPPER(ALLTRIM(outship.consignee))
		lSears = IIF("SEARS"$cConsignee OR "KMART"$cConsignee OR "K-MART"$cConsignee,.T.,.F.)
		lAAFES = IIF("AAFES"$cConsignee OR "ARMED FORCES"$cConsignee,.T.,.F.)

		IF "OV"$cShip_ref
			LOOP
		ENDIF

		lSolidPack = IIF("PACK940*SOLIDPACK"$outship.shipins,.T.,.F.)
		nPTCount = nPTCount + 1

		IF "PENNEY"$UPPER(m.consignee) OR UPPER(m.consignee)="JCP"
			lJCP = .T.
		ENDIF
		nOutshipid = m.outshipid
		nWO_num = m.wo_num
		cWO_Num = ALLTRIM(STR(nWO_num))
		cPackType = ALLTRIM(segmentget(@apt,"PROCESSMODE",alength))
		lPrepack = IIF(cPackType#"PICKPACK",.T.,.F.)

		IF !lParcelType
			cTracknum = IIF(EMPTY(ALLTRIM(outship.bol_no)),ALLTRIM(m.keyrec),ALLTRIM(outship.bol_no))
*			cTrackNum = ALLT(m.keyrec)  && ProNum if available
			cCharge = "0.00"
			IF !(cWO_Num$cWO_NumStr)
				cWO_NumStr = IIF(EMPTY(cWO_NumStr),cWO_Num,cWO_NumStr+","+cWO_Num)
			ENDIF
		ELSE
*			cBOL = PADL(ALLT(STR(outship.wo_num)),17,'0')
			cTracknum = ALLT(outship.bol_no)  && UPS Tracking Number
			cWO_NumStr = IIF(EMPTY(cWO_NumStr),cTracknum,cWO_NumStr+","+cTracknum)
			IF !lTesting
*				ASSERT .F. MESSAGE "At UPS package charges"
				SET STEP ON 
				SELECT shipment
				LOCATE FOR (INLIST(shipment.accountid,9999,&gtkoaccounts) AND shipment.pickticket = PADR(cShip_ref,20))
				IF !FOUND()
					LOCATE FOR shipment.accountid = 9999 AND shipment.pickticket = PADR(cShip_ref,20)
					IF !FOUND()
						cErrMsg = "MISS UPS REC "+cShip_ref
						DO ediupdate WITH cErrMsg,.T.
						THROW
					ELSE
						SELECT package
						LOCATE
						SUM pkgcharge TO nCharge FOR package.shipmentid = shipment.shipmentid
						cCharge = ALLT(STR(nCharge,8,2))
					ENDIF
				ELSE
					SELECT package
					LOCATE
					SUM pkgcharge TO nCharge FOR package.shipmentid = shipment.shipmentid
					cCharge = ALLT(STR(nCharge,8,2))
				ENDIF
			ELSE
				cCharge = "25.50"
			ENDIF
		ENDIF

		nTotCtnCount = m.qty
		cPO_Num = ALLTRIM(m.cnee_ref)
		cShip_ref = ALLTRIM(m.ship_ref)

*!* Added this code to trap miscounts in OUTDET Units
*		ASSERT .F. MESSAGE "In outdet/SQL tot check...>DEBUG HERE<"
		IF lPrepack
			SELECT outdet
			SET ORDER TO
			SET DELETED ON
			IF lOverflow
				SET DELETED OFF
				SUM origqty TO nCtnTot1 FOR !units AND outdet.outshipid = outship.outshipid
			ELSE
				IF INLIST(cOffice,"I","N") AND nAcctNum = 6705
					SUM totqty TO nCtnTot1 FOR units AND outdet.outshipid = outship.outshipid 
				ELSE
					SUM totqty TO nCtnTot1 FOR !units AND outdet.outshipid = outship.outshipid
				ENDIF
			ENDIF

*!*	Check carton count
			SET STEP ON 
			IF INLIST(cOffice,"I","N") AND nAcctNum = 6705
				SELECT SUM(totqty) AS cnt1 ;
					FROM vtkopp ;
					WHERE vtkopp.outshipid = outship.outshipid ;
					AND vtkopp.ucc#"CUTS";
									INTO CURSOR tempsqlx
*!*	**TMARG changed two lines above from this  8/11/17  changed back 8/15/17
*!*						AND vtkopp.ucc#"CUTS" and vtkopp.totqty>0
			ELSE
				SELECT COUNT(ucc) AS cnt1 ;
					FROM vtkopp ;
					WHERE vtkopp.outshipid = outship.outshipid ;
					AND vtkopp.ucc#"CUTS" AND vtkopp.totqty>0;
					INTO CURSOR tempsqlx
			ENDIF
			STORE tempsqlx.cnt1 TO nCtnTot2
			USE IN tempsqlx
			IF nCtnTot1<>nCtnTot2
				ASSERT .F. MESSAGE "At CTN QTY error...>>DEBUG<<"
				SET STEP ON
				cErrMsg = "SQL CTNQTY ERR, OSID "+TRANSFORM(outship.outshipid)
				DO ediupdate WITH cErrMsg,.T.
				THROW
			ENDIF
		ELSE
			SELECT outdet
			SUM IIF(lOverflow,origqty,totqty) TO nUnitTot1 FOR outdet.units=.T. AND outdet.outshipid = outship.outshipid

			SELECT vtkopp
			SUM vtkopp.totqty TO nUnitTot2 FOR vtkopp.outshipid = outship.outshipid AND vtkopp.accountid = outship.accountid
			IF nUnitTot1<>nUnitTot2
				ASSERT .F. MESSAGE "IN TOTQTY COMPARE ERROR"
				SET STEP ON
				cErrMsg = "SQL UNITQTY ERR, OSID "+TRANSFORM(outship.outshipid)
				DO ediupdate WITH cErrMsg,.T.
				THROW
			ENDIF
		ENDIF

		SELECT outdet
		SET ORDER TO outdetid
		LOCATE

*!* End code addition

		IF ("PENNEY"$UPPER(outship.consignee) OR "KMART"$UPPER(outship.consignee) OR "K-MART"$UPPER(outship.consignee)) AND !lParcelType
			lApptFlag = .T.
		ELSE
			lApptFlag = .F.
		ENDIF

		IF lTestinput
			ddel_date = DATE()
			dapptnum = "99999"
			dapptdate = DATE()
		ELSE
			ddel_date = outship.del_date
			IF EMPTY(ddel_date)
				cMissDel = IIF(EMPTY(cMissDel),"The following PTs had no Delivery Dates:"+CHR(10)+TRIM(cShip_ref),cMissDel+CHR(10)+TRIM(cShip_ref))
			ENDIF
			dapptnum = ALLTRIM(outship.appt_num)

			IF EMPTY(dapptnum) && Penney/KMart Appt Number check
				IF !(lApptFlag)
					dapptnum = ""
				ELSE
					cErrMsg = "EMPTY APPT #"
					DO ediupdate WITH cErrMsg,.T.
					THROW
				ENDIF
			ENDIF
			dapptdate = outship.appt

			IF EMPTY(dapptdate)
				dapptdate = outship.del_date
			ENDIF
		ENDIF

		IF !(cWO_Num$cWO_NumStr)
			cWO_NumStr = IIF(EMPTY(cWO_NumStr),cWO_Num,cWO_NumStr+","+cWO_Num)
		ENDIF

		IF ALLTRIM(outship.SForCSZ) = ","
			BLANK FIELDS outship.SForCSZ NEXT 1 IN outship
		ENDIF

		cTRNum = ""
		cPRONum = ""

		IF (("WALMART"$outship.consignee) OR ("WAL-MART"$outship.consignee) OR ("WAL MART"$outship.consignee) OR ("CABELA"$outship.consignee))
			IF LEFT(outship.keyrec,2) = "PR"
				STORE STRTRAN(TRIM(keyrec),"PR","") TO cPRONum
			ENDIF
			IF LEFT(outship.keyrec,2) = "TR"
				STORE TRIM(keyrec) TO cTRNum
			ENDIF
		ENDIF

		cPTString = IIF(EMPTY(cPTString),m.consignee+" "+cShip_ref,cPTString+CHR(13)+CHR(10)+m.consignee+" "+cShip_ref)

		m.CSZ = TRIM(m.CSZ)
		IF EMPTY(ALLT(STRTRAN(M.CSZ,",","")))
			WAIT CLEAR
			WAIT WINDOW "No SHIP-TO City/State/ZIP info...exiting" TIMEOUT 2
			cErrMsg = "NO CSZ INFO"
			DO ediupdate WITH cErrMsg,.T.
			THROW
		ENDIF
		IF !(", "$m.CSZ)
			m.CSZ = ALLTRIM(STRTRAN(m.CSZ,",",", "))
		ENDIF
		m.CSZ = ALLTRIM(STRTRAN(m.CSZ,"  "," "))
		cCity = ALLT(LEFT(TRIM(m.CSZ),AT(",",m.CSZ)-1))
		cStateZip = ALLT(SUBSTR(TRIM(m.CSZ),AT(",",m.CSZ)+1))
		cState = ALLT(LEFT(cStateZip,2))
		cZip = ALLT(SUBSTR(cStateZip,3))

		STORE "" TO cSForCity,cSForState,cSForZip
		IF !lFederated
			m.SForCSZ = ALLTRIM(STRTRAN(m.SForCSZ,"  "," "))
			nSpaces = OCCURS(" ",ALLTRIM(m.SForCSZ))
			IF nSpaces = 0
				m.SForCSZ = STRTRAN(m.SForCSZ,",","")
				cSForCity = ALLTRIM(m.SForCSZ)
				cSForState = ""
				cSForZip = ""
			ELSE
				nCommaPos = AT(",",m.SForCSZ)
				nLastSpace = AT(" ",m.SForCSZ,nSpaces)
				nMinusSpaces = IIF(nSpaces=1,0,1)
				IF ISALPHA(SUBSTR(TRIM(m.SForCSZ),AT(" ",TRIM(m.SForCSZ),nSpaces-nMinusSpaces)+1,2))
					cSForCity = LEFT(TRIM(m.SForCSZ),AT(",",m.SForCSZ)-1)
					cSForState = SUBSTR(m.SForCSZ,nCommaPos+2,2)
					cSForZip = TRIM(RIGHT(TRIM(m.SForCSZ),5))
					IF ISALPHA(cSForZip)
						cSForZip = ""
					ENDIF
				ELSE
					WAIT CLEAR
					WAIT WINDOW "NOT ALPHA: "+SUBSTR(TRIM(m.SForCSZ),AT(" ",m.SForCSZ,nSpaces-1)+1,2) TIMEOUT 3
					cSForCity = LEFT(TRIM(m.SForCSZ),AT(",",m.SForCSZ)-1)
					cSForState = SUBSTR(TRIM(m.SForCSZ),AT(" ",m.SForCSZ,nSpaces-2)+1,2)
					cSForZip = TRIM(RIGHT(TRIM(m.SForCSZ),5))
					IF ISALPHA(cSForZip)
						cSForZip = ""
					ENDIF
				ENDIF
			ENDIF
		ELSE
			cStoreName = segmentget(@apt,"STORENAME",alength)
		ENDIF

		DO num_incr_st
		WAIT CLEAR
		WAIT WINDOW "Now creating Line Item information" NOWAIT NOCLEAR

		INSERT INTO temp945 (accountid,isa_num,st_num,wo_num,bol_no,ship_ref,filename,filedate) ;
			VALUES (m.accountid,c_CntrlNum,c_GrpCntrlNum,m.wo_num,m.bol_no,m.ship_ref,cFilenameShort,dt2)

		STORE "ST"+cfd+"945"+cfd+PADL(c_GrpCntrlNum,9,"0")+csegd TO cString
		DO cstringbreak
		nSTCount = nSTCount + 1
		nSegCtr = 1

		cShip_refW06 = IIF("OV"$cShip_ref,ALLTRIM(LEFT(cShip_ref,AT(" ",cShip_ref,1))),cShip_ref)
		cShip_refW06 = STRTRAN(cShip_refW06,"-DS","")

		STORE "W06"+cfd+"N"+cfd+cShip_refW06+cfd+cdate+cfd+TRIM(cBOL)+cfd+TRIM(cWO_Num)+cfd+cPO_Num+csegd TO cString
		DO cstringbreak
		nSegCtr = nSegCtr + 1

		nCtnNumber = 1  && Seed carton sequence count

*		ASSERT .F. MESSAGE "At Ship-to store...debug"
		cStoreNum = segmentget(@apt,"GLN",alength)
		IF EMPTY(cStoreNum)
			cStoreNum = segmentget(@apt,"STORENUM",alength)
		ENDIF

		IF EMPTY(cStoreNum)
			cStoreNum = m.dcnum
			IF EMPTY(cStoreNum)
				STORE "N1"+cfd+"CN"+cfd+ALLTRIM(m.consignee)+csegd TO cString
			ENDIF
		ELSE
			STORE "N1"+cfd+"CN"+cfd+ALLTRIM(m.consignee)+cfd+"92"+cfd+cStoreNum+csegd TO cString
		ENDIF
		DO cstringbreak
		nSegCtr = nSegCtr + 1

		cCountry = segmentget(@apt,"COUNTRY",alength)
		cCountry = ALLT(cCountry)
		IF EMPTY(cCountry)
			cCountry = "USA"
		ENDIF

		STORE "N3"+cfd+TRIM(outship.address)+csegd TO cString
		DO cstringbreak
		nSegCtr = nSegCtr + 1

		STORE "N4"+cfd+cCity+cfd+cState+cfd+cZip+cfd+cCountry+csegd TO cString
		DO cstringbreak
		nSegCtr = nSegCtr + 1

		cSFStoreNum = segmentget(@apt,"SFSTORENUM",alength)
		IF EMPTY(cSFStoreNum)
			cSFStoreNum = ALLTRIM(m.sforstore)
		ENDIF
		IF !EMPTY(m.shipfor)
			IF EMPTY(ALLTRIM(m.sforstore))
				STORE "N1"+cfd+"Z7"+cfd+ALLTRIM(m.shipfor)+csegd TO cString
			ELSE
				STORE "N1"+cfd+"Z7"+cfd+ALLTRIM(m.shipfor)+cfd+"92"+cfd+cSFStoreNum+csegd TO cString
			ENDIF
			DO cstringbreak
			nSegCtr = nSegCtr + 1

			STORE "N3"+cfd+TRIM(m.sforaddr1)+csegd TO cString
			DO cstringbreak
			nSegCtr = nSegCtr + 1

			IF !EMPTY(cSForState)
				STORE "N4"+cfd+cSForCity+cfd+cSForState+cfd+cSForZip+cfd+"USA"+csegd TO cString
			ELSE
				STORE "N4"+cfd+cSForCity+csegd TO cString
			ENDIF
			DO cstringbreak
			nSegCtr = nSegCtr + 1
		ENDIF

		cContact = ALLTRIM(segmentget(@apt,"CONTACT",alength))
		IF !EMPTY(cContact)
			STORE "PER"+cfd+"ST"+cfd+cContact+csegd TO cString
			DO cstringbreak
			nSegCtr = nSegCtr + 1
		ENDIF

		cCompany1 = ALLTRIM(segmentget(@apt,"COMPANY",alength))
		IF !EMPTY(cCompany1)
			STORE "N9"+cfd+"CO"+cfd+cCompany1+csegd TO cString
			DO cstringbreak
			nSegCtr = nSegCtr + 1
		ENDIF

		STORE "N1"+cfd+"BT"+cfd+ALLTRIM(m.consignee)+cfd+"92"+cfd+cStoreNum+csegd TO cString
		DO cstringbreak
		nSegCtr = nSegCtr + 1

		cCountry = segmentget(@apt,"COUNTRY",alength)
		cCountry = ALLT(cCountry)
		IF EMPTY(cCountry)
			cCountry = "USA"
		ENDIF

		STORE "N3"+cfd+TRIM(outship.address)+csegd TO cString
		DO cstringbreak
		nSegCtr = nSegCtr + 1

		STORE "N4"+cfd+cCity+cfd+cState+cfd+cZip+cfd+cCountry+csegd TO cString
		DO cstringbreak
		nSegCtr = nSegCtr + 1

		cDP = ALLTRIM(outship.dept)
		IF !EMPTY(cDP)
			STORE "N9"+cfd+"DP"+cfd+cDP+csegd TO cString
			DO cstringbreak
			nSegCtr = nSegCtr + 1
		ENDIF

		cIA = ALLTRIM(outship.vendor_num)
		IF !EMPTY(cIA)
			STORE "N9"+cfd+"IA"+cfd+cIA+csegd TO cString
			DO cstringbreak
			nSegCtr = nSegCtr + 1
		ENDIF

		cMR = ALLTRIM(segmentget(@apt,"MR",alength))
		IF !EMPTY(cMR)
			STORE "N9"+cfd+"MR"+cfd+cMR+csegd TO cString
			DO cstringbreak
			nSegCtr = nSegCtr + 1
		ENDIF

		IF !EMPTY(ALLTRIM(dapptnum)) && AND lJCP  * Changed to all shipments, per Maria E, 05.17.2016
			STORE "N9"+cfd+"AO"+cfd+dapptnum+csegd TO cString
			DO cstringbreak
			nSegCtr = nSegCtr + 1
		ENDIF

		IF !EMPTY(ALLTRIM(cPRONum))
			STORE "N9"+cfd+"CN"+cfd+ALLTRIM(cPRONum)+csegd TO cString
			DO cstringbreak
			nSegCtr = nSegCtr + 1
		ELSE
			IF lAAFES
				STORE "N9"+cfd+"CN"+cfd+ALLTRIM(cBOL)+csegd TO cString
				DO cstringbreak
				nSegCtr = nSegCtr + 1
			ENDIF
		ENDIF

		STORE "N9"+cfd+"CT"+cfd+ALLTRIM(STR(m.ctnqty))+csegd TO cString
		DO cstringbreak
		nSegCtr = nSegCtr + 1

		nTotWt = IIF(m.weight>0,m.weight,m.ctnqty*3)
		STORE "N9"+cfd+"CTW"+cfd+ALLTRIM(STR(nTotWt))+csegd TO cString
		DO cstringbreak
		nSegCtr = nSegCtr + 1

		STORE "G62"+cfd+"11"+cfd+TRIM(DTOS(ddel_date))+cfd+"D"+cfd+cDelTime+csegd TO cString  && Ship date/time
		DO cstringbreak
		nSegCtr = nSegCtr + 1

		SELECT scacs
*		SET STEP ON
		IF lTesting AND (EMPTY(m.scac) OR EMPTY(m.ship_via))
			STORE "TEST" TO m.scac
			STORE "TEST SHIPPER" TO m.ship_via
		ELSE
			IF !EMPTY(TRIM(outship.scac))
				STORE TRIM(outship.scac) TO m.scac
				lFedEx = .F.
				SELECT scacs
				IF SEEK(m.scac,"scacs","scac")
					IF ("FEDERAL EXPRESS"$NAME) OR ("FEDEX"$NAME)
						lFedEx = .T.
						IF m.scac = "FGC"
							m.scac = "FDEG"
						ENDIF
					ENDIF
					SELECT outship

					IF lParcelType OR lFedEx
						cCarrierType = "U" && Parcel as UPS or FedEx
					ENDIF
				ELSE
					WAIT CLEAR
					cErrMsg = "MISSING SCAC"
					DO ediupdate WITH cErrMsg,.T.
					THROW
				ENDIF
			ENDIF
		ENDIF

		SELECT outship

		IF !EMPTY(cTRNum)
			STORE "W27"+cfd+cCarrierType+cfd+TRIM(m.scac)+cfd+TRIM(m.ship_via)+REPLICATE(cfd,2)+"TL"+;
				REPLICATE(cfd,2)+cTRNum+csegd TO cString
		ELSE
			STORE "W27"+cfd+cCarrierType+cfd+TRIM(m.scac)+cfd+TRIM(m.ship_via)+csegd TO cString
		ENDIF
		DO cstringbreak
		nSegCtr = nSegCtr + 1

*!*			IF lParcelType
*!*				STORE "G72"+cfd+"516"+cfd+"15"+REPLICATE(cfd,6)+ALLT(cCharge)+csegd TO cString
*!*				DO cstringbreak
*!*				nSegCtr = nSegCtr + 1
*!*			ENDIF

*************************************************************************
*2	DETAIL LOOP - MATCHING SHIP_REF FIELD (FROM OUTSHIP)
*************************************************************************
		WAIT CLEAR
		WAIT WINDOW "Now creating Detail information "+cShip_ref NOWAIT NOCLEAR

		SELECT outdet
		SET ORDER TO TAG outdetid
		SELECT vtkopp
		SET RELATION TO outdetid INTO outdet

		SELECT vtkopp
		LOCATE
*!*			BROWSE FIELDS wo_num,ship_ref,outshipid
		LOCATE FOR vtkopp.ship_ref = TRIM(cShip_ref) AND vtkopp.outshipid = nOutshipid
		IF !FOUND()
			IF !lTesting
				WAIT WINDOW "PT "+cShip_ref+" NOT FOUND in vtkopp...ABORTING" TIMEOUT 2
				SET STEP ON
				IF !lTesting
					lSQLMail = .T.
				ENDIF
				cErrMsg = "MISS PT-SQL: "+cShip_ref
				DO ediupdate WITH cErrMsg,.T.
				THROW
			ELSE
				LOOP
			ENDIF
		ENDIF

		SELECT ucc,SUM(totqty) AS sumtq ;
			FROM vtkopp ;
			GROUP BY 1 ;
			INTO CURSOR tempzerouccs READWRITE ;
			HAVING sumtq = 0
		INDEX ON ucc TAG ucc

		vscanstr = "vtkopp.ship_ref = cShip_ref and vtkopp.outshipid = nOutshipid"
		SELECT vtkopp
		LOCATE
		LOCATE FOR &vscanstr
		cUCC= "XXX"
		nZCnt = 0

		DO WHILE &vscanstr
			IF lOverflow
				SET DELETED OFF
			ELSE
				SET DELETED ON
			ENDIF
			IF vtkopp.COLOR = 'BLACK SOOT'
*		SET STEP ON
			ENDIF
			lSkipBack = .T.
			SELECT outdet
			nODDetQty = IIF(lOverflow,outdet.origqty,outdet.totqty)
			IF nODDetQty = 0
				SET STEP ON
				IF !EOF('vtkopp')
					SKIP 1 IN vtkopp
					LOOP
				ELSE
*					EXIT
				ENDIF
			ENDIF

			IF TRIM(vtkopp.ucc) <> cUCC
				STORE TRIM(vtkopp.ucc) TO cUCC
			ENDIF

			IF SEEK(cUCC,'tempzerouccs','ucc')
				nZCnt = nZCnt + 1
				SKIP 1 IN vtkopp
				LOOP
			ENDIF

			STORE "LX"+cfd+ALLTRIM(STR(nCtnNumber))+csegd TO cString   && Carton Seq. # (up to Carton total)
			lDoManSegment = .T.
			lDoPALSegment = .F.
			DO cstringbreak
			nSegCtr = nSegCtr + 1

			DO WHILE vtkopp.ucc = cUCC
				cDesc = ""
				SELECT outdet
				alength = ALINES(aptdet,outdet.printstuff,.T.,CHR(13))
*!* Prepack determination based on actual style info

				lPrepack945 = .F.
				DO CASE
					CASE (("PACKGROUP*SOLIDPACK"$UPPER(outship.shipins)) OR ("PACKGROUP*CASEPACK"$UPPER(outship.shipins)))
						lPrepack945 = .T.
					CASE ("SINGLES"$UPPER(outship.shipins) OR "PIECES"$UPPER(outship.shipins))
						lPrepack945 = .F.
					CASE "PENNEY"$outship.consignee && AND DATE()={^2009-04-14}
						lPrepack945 = .T.
					CASE "SEARS"$outship.consignee
						lPrepack945 = .T.
					CASE outship.consignee = "SAM"
						lPrepack945 = .F.
					CASE "SOLIDPACK"$outdet.printstuff
						lPrepack945 = .F.
					CASE "PREPACK"$outdet.printstuff
						lPrepack945 =.T.
					CASE ("-"$outdet.STYLE AND INLIST(SUBSTR(outdet.STYLE,AT("-",outdet.STYLE,1)-3,1),"S","P"))
						lPrepack945 = .T.
					CASE ("/"$outdet.STYLE AND INLIST(SUBSTR(outdet.STYLE,AT("/",outdet.STYLE,1)-3,1),"S","P"))  && Added due to style discreps
						lPrepack945 = .T.
					CASE (INLIST(SUBSTR(ALLTRIM(outdet.STYLE),LEN(ALLTRIM(outdet.STYLE))-2,1),"S","P"))
						lPrepack945 = .T.
					OTHERWISE
						lPrepack945 = .F.
				ENDCASE
				cUCCNumber = vtkopp.ucc
				cUCCNumber = ALLTRIM(STRTRAN(TRIM(cUCCNumber)," ",""))

				IF lDoManSegment
					lDoManSegment = .F.
					STORE "MAN"+cfd+"GM"+cfd+TRIM(cUCCNumber)+csegd TO cString  && SCC-14 Number (Wal-mart spec)
					DO cstringbreak
					nSegCtr = nSegCtr + 1
					IF lParcelType
						STORE "MAN"+cfd+"CP"+cfd+TRIM(m.bol_no)+csegd TO cString  && SCC-14 Number (Wal-mart spec)
						DO cstringbreak
						nSegCtr = nSegCtr + 1
					ENDIF
					nShipDetQty = INT(VAL(outdet.PACK))
					nUnitSum = nUnitSum + nShipDetQty

					IF (EMPTY(outdet.ctnwt) OR outdet.ctnwt = 0) AND IIF(lOverflow,outdet.totqty,outdet.origqty)> 0
						cCtnWt = ALLTRIM(STR(INT(CEILING(outship.weight)/outship.ctnqty)))
						IF lTesting
							cCtnWt = "3"
						ELSE
							IF (EMPTY(cCtnWt) OR INT(VAL(cCtnWt)) = 0)
								cCtnWt = '1'
							ENDIF
						ENDIF
						nTotCtnWt = nTotCtnWt + INT(VAL(cCtnWt))
					ELSE
						STORE ALLTRIM(STR(outdet.ctnwt)) TO cCtnWt
						nTotCtnWt = nTotCtnWt + outdet.ctnwt
					ENDIF
				ENDIF

				cStyle = TRIM(outdet.STYLE)
				IF EMPTY(cStyle)
					IF lOverflow
						SET DELETED OFF
						SELECT vtkopp
						SET RELATION TO outdetid INTO outdet
						cStyle = TRIM(outdet.STYLE)
						IF EMPTY(cStyle)
							ASSERT .F. MESSAGE "TRY #2: EMPTY STYLE...DEBUG"
							WAIT WINDOW "Empty style in "+cShip_ref TIMEOUT 1
						ENDIF
					ELSE
						ASSERT .F. MESSAGE "EMPTY STYLE...DEBUG"
						WAIT WINDOW "Empty style in "+cShip_ref TIMEOUT 1
					ENDIF
				ENDIF

				lDoUPC = .T.
*				ASSERT .F. MESSAGE "At UPC extract...debug"
				cUPC = TRIM(segmentget(@aptdet,"UPC",alength))
				IF EMPTY(cUPC)
					cUPC = ALLTRIM(outdet.upc)
					IF (ISNULL(cUPC) OR EMPTY(cUPC) OR VAL(cUPC) = 0)
						cUPC = TRIM(vtkopp.upc)
						IF (ISNULL(cUPC) OR EMPTY(cUPC) OR VAL(cUPC) = 0)
							lDoUPC = .F.
						ENDIF
					ENDIF
				ENDIF

				cItemNum = TRIM(outdet.custsku)
				nShipDetQty = vtkopp.totqty
				IF ISNULL(nShipDetQty) && OR EMPTY(nShipDetQty)
					nShipDetQty = IIF(lOverflow,outdet.totqty,outdet.origqty)
				ENDIF

				nOrigDetQty = vtkopp.qty
				IF ISNULL(nOrigDetQty)
					nOrigDetQty = nShipDetQty
				ENDIF

				IF lPrepack AND lPrepack945 AND !lSolidGroup && Changed 01.13.2009 per Paul (was "1" for both); reverted 02.06.2009
					IF INLIST(cShip_ref,'SPS-1074052','SPS-1074051')
						nOrigDetQty = 10
					ELSE
						nOrigDetQty = 1
					ENDIF
					STORE nOrigDetQty TO nShipDetQty
				ENDIF

				IF lSolidPack AND !lSolidGroup
					nOrigDetQty = INT(VAL(outdet.PACK))
					STORE nOrigDetQty TO nShipDetQty
				ENDIF

				IF lSolidGroup  && NewPack
					nOrigDetQty = vtkopp.totqty
					STORE nOrigDetQty TO nShipDetQty
				ENDIF

*!* Changed the following to utilize original 940 unit codes from Printstuff field
				cUnitCode = TRIM(segmentget(@aptdet,"UNITCODE",alength))
				cUnitType = TRIM(segmentget(@aptdet,"UNITTYPE",alength))
*				SET STEP ON
				IF EMPTY(cUnitCode)
					cUnitCode = cUnitType
					IF EMPTY(cUnitCode)
						IF lPrepack AND !lSolidPack
							cUnitCode = "CA"
						ELSE
							cUnitCode = "EA"
						ENDIF
					ENDIF
				ENDIF

				IF nOrigDetQty = nShipDetQty
					nShipStat = "CC"
				ELSE
					nShipStat = "CP"
				ENDIF

				IF lDoUPC
					STORE "W12"+cfd+nShipStat+cfd+ALLTRIM(STR(nOrigDetQty))+cfd+;
						ALLTRIM(STR(nShipDetQty))+cfd+ALLTRIM(STR(nOrigDetQty-nShipDetQty))+cfd+;
						cUnitCode+cfd+ALLTRIM(outdet.custsku)+cfd+"VN"+cfd+cUPC+csegd TO cString
				ELSE
					STORE "W12"+cfd+nShipStat+cfd+ALLTRIM(STR(nOrigDetQty))+cfd+;
						ALLTRIM(STR(nShipDetQty))+cfd+ALLTRIM(STR(nOrigDetQty-nShipDetQty))+cfd+;
						cUnitCode+cfd+ALLTRIM(outdet.custsku)+csegd TO cString
				ENDIF
				DO cstringbreak
				nSegCtr = nSegCtr + 1

*				ASSERT .f. MESSAGE "AT G69 LOOP"
				cDesc = ALLTRIM(segmentget(@aptdet,"MASTSTYLEDESC",alength))
				IF !EMPTY(cDesc)
					STORE "G69"+cfd+cDesc+csegd TO cString
					DO cstringbreak
					nSegCtr = nSegCtr + 1
				ENDIF

				cStyle = TRIM(outdet.STYLE)
				IF !EMPTY(cStyle)
					STORE "N9"+cfd+"ST"+cfd+cStyle+csegd TO cString
					DO cstringbreak
					nSegCtr = nSegCtr + 1
				ENDIF

				cColor = ""
				cColor = ALLTRIM(segmentget(@aptdet,"ORIGCOLOR",alength))
				cColor = IIF(EMPTY(cColor),ALLTRIM(outdet.COLOR),cColor)
				IF !EMPTY(cColor)
					STORE "N9"+cfd+"CL"+cfd+cColor+csegd TO cString
					DO cstringbreak
					nSegCtr = nSegCtr + 1
				ENDIF

				cSize = TRIM(outdet.ID)
				IF !EMPTY(cSize)
					STORE "N9"+cfd+"SZ"+cfd+cSize+csegd TO cString
					DO cstringbreak
					nSegCtr = nSegCtr + 1
				ENDIF

				cPkg = TRIM(segmentget(@aptdet,"PKG",alength))
				cPkgQty = TRIM(segmentget(@aptdet,"QTY",alength))
				IF !EMPTY(cPkg)
					STORE "N9"+cfd+"PKG"+cfd+cPkg+cfd+cPkgQty+csegd TO cString
					DO cstringbreak
					nSegCtr = nSegCtr + 1
				ENDIF

				SKIP 1 IN vtkopp
			ENDDO
			SET DELETED ON
			lSkipBack = .T.
			nCtnNumber = nCtnNumber + 1
*		nUnitSum = nUnitSum + 1
		ENDDO
		nTCnt = nTCnt + nZCnt

*************************************************************************
*2^	END DETAIL MATCHING SHIP_REF LOOP
*************************************************************************
		WAIT CLEAR
		IF outship.cuft>0
			cCube = STR(outship.cuft,6,2)
		ELSE
			cCube = "0"
		ENDIF
		WAIT WINDOW "Now creating Section Closure information" NOWAIT NOCLEAR
		nPTCtnTot = outship.ctnqty
		STORE "W03"+cfd+ALLTRIM(STR(nPTCtnTot))+cfd+ALLTRIM(STR(nTotCtnWt))+cfd+;
			cWeightUnit+cfd+cCube+cfd+"CF"+csegd TO cString   && Units sum, Weight sum, carton count

		nPTCtnTot = 0
		nTotCtnWt = 0
		nTotCtnCount = 0
		nUnitSum = 0
		FPUTS(nFilenum,cString)

		STORE  "SE"+cfd+ALLTRIM(STR(nSegCtr+2))+cfd+PADL(c_GrpCntrlNum,9,"0")+csegd TO cString
		FPUTS(nFilenum,cString)

		SELECT outship
		WAIT CLEAR
	ENDSCAN
	WAIT WINDOW "Grand Total, skipped UCCs: "+TRANSFORM(nTCnt) TIMEOUT 1

*************************************************************************
*1^	END OUTSHIP MAIN LOOP
*************************************************************************

	DO close945
	=FCLOSE(nFilenum)

	IF !lTesting
		SELECT edi_trigger
		DO ediupdate WITH "945 CREATED",.F.
		SELECT edi_trigger
		LOCATE

		IF !USED("ftpedilog")
			SELECT 0
			USE F:\edirouting\ftpedilog ALIAS ftpedilog
			INSERT INTO ftpedilog (TRANSFER,ftpdate,filename,acct_name,TYPE) VALUES ("945-"+ccustname+"-"+cCustLoc,dt2,cFilename,UPPER(ccustname),"945")
			USE IN ftpedilog
		ENDIF
	ENDIF

	WAIT CLEAR
	WAIT WINDOW cCustFolder+" 945 Process complete..." NOWAIT

*!* Create eMail confirmation message
	cOutFolder = "FMI"+cCustLoc

	tsubject = cMailName+" 945 EDI File from TGF as of "+dtmail+" ("+cCustLoc+")"
	tattach = " "
	tmessage = "945 EDI Info from TGF, for division "+cDivision+", BOL# "+TRIM(cBOL)+CHR(13)
	tmessage = tmessage + "Filename: "+cFilenameShort+CHR(13)
	tmessage = tmessage + "For Work Orders: "+cWO_NumStr+","+CHR(13)
	tmessage = tmessage + "containing these "+ALLTRIM(STR(nPTCount))+" picktickets:"+CHR(13)+CHR(13)
	tmessage = tmessage + cPTString + CHR(13)+CHR(13)
	tmessage = tmessage +"has been created and will be transmitted ASAP."+CHR(13)+CHR(13)
	IF !EMPTY(cMissDel)
		tmessage = tmessage+CHR(13)+CHR(13)+cMissDel+CHR(13)+CHR(13)
	ENDIF

	IF nTCnt > 0
		tmessage = tmessage+CHR(13)+"*** There were "+TRANSFORM(nTCnt)+" zero qty cartons cut from this BOL. ***"+CHR(13)
	ENDIF

	IF lTesting OR lTestinput
		tmessage = tmessage + "This is a TEST 945"
	ELSE
		tmessage = tmessage + "If you have any questions, please eMail or call 732-750-9000 x217."
	ENDIF

	IF lEmail
		ASSERT .F. MESSAGE "At do mail stage...DEBUG"
		DO FORM m:\dev\frm\dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
	ENDIF

	cFilenameArchive = UPPER("f:\ftpusers\"+cCustFolder+"\945OUT\ARCHIVE\"+cCustPrefix+dt1+".txt")

	RELEASE ALL LIKE c_CntrlNum,c_GrpCntrlNum
	WAIT CLEAR
	WAIT WINDOW cMailName+" 945 EDI File output complete" TIMEOUT 1

*!* Transfers files to correct output folders
	COPY FILE [&cFilename] TO [&cFilenameArch]

set step On 

	IF lFilesout=.T. AND !lTesting
		COPY FILE [&cFilename] TO [&cFilenameOut]
		DELETE FILE &cFilename
		SELECT temp945
		COPY TO "f:\3pl\data\temp945a.dbf"
		USE IN temp945
		SELECT 0
		USE "f:\3pl\data\temp945a.dbf" ALIAS temp945a
		SELECT 0
		USE "f:\3pl\data\pts_sent945.dbf" ALIAS pts_sent945
		APPEND FROM "f:\3pl\data\temp945a.dbf"
		USE IN pts_sent945
		USE IN temp945a
		DELETE FILE "f:\3pl\data\temp945a.dbf"
	ENDIF

	*!* asn_out_data()()

CATCH TO oErr
	IF lDoCatch
		ASSERT .F. MESSAGE "In Error CATCH"
		SET STEP ON

		IF BETWEEN(oErr.LINENO,659,662)
			cErrMsg = "BAD CSZ INFO "+cBOL
		ENDIF
		IF !lTesting
			DO ediupdate WITH cErrMsg,.T.
		ENDIF
		tsubject = ccustname+" Error ("+TRANSFORM(oErr.ERRORNO)+") at "+TTOC(DATETIME())
		tattach  = ""
		IF  lTesting
			tsendto  = tsendtoerr
			tcc = tccerr
*			tsendto = "pgaidis@fmiint.com"
*			tcc =""
		ENDIF

		IF !lIsError
			tmessage = ccustname+" Error processing "+CHR(13)

			tmessage =tmessage+ "Try/CATCH Exeception Message:"+CHR(13)+;
				[  Error: ] + STR(oErr.ERRORNO) +CHR(13)+;
				[  LineNo: ] + STR(oErr.LINENO) +CHR(13)+;
				[  Message: ] + oErr.MESSAGE +CHR(13)+;
				[  Procedure: ] + oErr.PROCEDURE +CHR(13)+;
				[  Details: ] + oErr.DETAILS +CHR(13)+;
				[  StackLevel: ] + STR(oErr.STACKLEVEL) +CHR(13)+;
				[  LineContents: ] + oErr.LINECONTENTS+CHR(13)+;
				[  UserValue: ] + oErr.USERVALUE+CHR(13)+;
				[  ErrMgs:    ]+ cErrMsg
			tmessage =tmessage+CHR(13)+cProgname

			tsubject = "945 EDI Poller Error at "+TTOC(DATETIME())
			tattach  = ""
			tcc=""
			tfrom    ="TGF EDI 945 Poller Operations <transload-ops@tollgroup.com>"
			ASSERT .F. MESSAGE "At do mail stage...DEBUG"
			DO FORM m:\dev\frm\dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
		ENDIF
		FCLOSE(nFilenum)
	ENDIF
FINALLY
	IF USED('OUTSHIP')
		USE IN outship
	ENDIF
	IF USED('OUTDET')
		USE IN outdet
	ENDIF
	IF USED('SHIPMENT')
		USE IN shipment
	ENDIF
	IF USED('PACKAGE')
		USE IN package
	ENDIF
	IF USED('SERFILE')
		USE IN serfile
	ENDIF

	ON ERROR
	WAIT CLEAR

ENDTRY
*** END OF CODE BODY

****************************
PROCEDURE close945
****************************
** Footer Creation

	STORE  "GE"+cfd+ALLTRIM(STR(nSTCount))+cfd+c_CntrlNum+csegd TO cString
	FPUTS(nFilenum,cString)

	STORE  "IEA"+cfd+"1"+cfd+PADL(c_CntrlNum,9,"0")+csegd TO cString
	FPUTS(nFilenum,cString)

	RETURN

****************************
PROCEDURE num_incr_isa
****************************

	IF !USED("serfile")
		USE ("f:\3pl\data\serial\"+ccustname+"945_serial") IN 0 ALIAS serfile
	ENDIF
	SELECT serfile
	IF lTesting AND lISAFlag
		nOrigSeq = serfile.seqnum
		lISAFlag = .F.
	ENDIF
	nISA_Num = serfile.seqnum
	c_CntrlNum = ALLTRIM(STR(serfile.seqnum))
	REPLACE serfile.seqnum WITH serfile.seqnum + 1 IN serfile
	SELECT outship
	RETURN

****************************
PROCEDURE num_incr_st
****************************

	IF !USED("serfile")
		USE ("f:\3pl\data\serial\"+ccustname+""945_serial") IN 0 ALIAS serfile
	ENDIF
	SELECT serfile
	IF lTesting AND lSTFlag
		nOrigGrpSeq = serfile.grpseqnum
		lSTFlag = .F.
	ENDIF
	c_GrpCntrlNum = ALLTRIM(STR(serfile.grpseqnum))
	REPLACE serfile.grpseqnum WITH serfile.grpseqnum + 1 IN serfile
	SELECT outship
	RETURN


****************************
PROCEDURE segmentget
****************************

	PARAMETER thisarray,lcKey,nLength

	FOR i = 1 TO nLength
		IF i > nLength
			EXIT
		ENDIF
		lnEnd= AT("*",thisarray[i])
		IF lnEnd > 0
			lcThisKey =TRIM(SUBSTR(thisarray[i],1,lnEnd-1))
			IF OCCURS(lcKey,lcThisKey)>0
				RETURN SUBSTR(thisarray[i],lnEnd+1)
				i = 1
			ENDIF
		ENDIF
	ENDFOR

	RETURN ""

****************************
PROCEDURE ediupdate
****************************
	PARAMETER cStatus,lIsError
*	lDoCatch = .F.

	IF lTesting
		RETURN
	ENDIF
	IF !lTesting
		SELECT edi_trigger
		nRec = RECNO()
		IF !lIsError
			REPLACE processed WITH .T.,proc945 WITH .T.,file945 WITH cFilename,isa_num WITH cISA_Num,;
				fin_status WITH "945 CREATED",errorflag WITH .F.,when_proc WITH DATETIME() ;
				FOR edi_trigger.bol = cBOL AND accountid = nAcctNum
			lDoCatch = .F.
		ELSE
			REPLACE processed WITH .T.,proc945 WITH .F.,file945 WITH "",;
				fin_status WITH cStatus,errorflag WITH .T. ;
				FOR edi_trigger.bol = cBOL AND accountid = nAcctNum
			num_decrement()

			IF lCloseOutput
				=FCLOSE(nFilenum)
				ERASE &cFilename
			ENDIF
		ENDIF
	ENDIF

	IF lIsError AND lEmail AND !INLIST(cStatus,"SQL ERROR","UNHANDLED ERROR")
		SET STEP ON
		tsubject = "945 Error in "+cMailName+" BOL "+TRIM(cBOL)+"(At PT "+cShip_ref+")"
		tattach = " "
		tsendto = tsendtoerr
		tcc = tccerr
		tmessage = "945 Processing for BOL# "+cBOL+", WO# "+cWO_Num+" produced this error: (Office: "+cOffice+"): "+cStatus+CHR(13)+"Check EDI_TRIGGER and re-run"
		IF "TOTQTY ERR"$cStatus
			tmessage = tmessage + CHR(13) + "At OUTSHIPID: "+ALLTRIM(STR(m.outshipid))
		ENDIF
		ASSERT .F. MESSAGE "At do mail stage...DEBUG"
		DO FORM m:\dev\frm\dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
	ENDIF

	IF USED('outship')
		USE IN outship
	ENDIF
	IF USED('outdet')
		USE IN outdet
	ENDIF

	IF USED('serfile')
		SELECT serfile
		IF lTesting
			REPLACE serfile.seqnum WITH nOrigSeq
			REPLACE serfile.grpseqnum WITH nOrigGrpSeq
		ENDIF
		USE IN serfile
	ENDIF
	IF USED('scacs')
		USE IN scacs
	ENDIF
	IF USED('mm')
		USE IN mm
	ENDIF
	IF USED('tempx')
		USE IN tempx
	ENDIF
	IF !lTesting
		SELECT edi_trigger
		LOCATE
	ENDIF

ENDPROC

****************************
PROCEDURE cstringbreak
****************************
	cLen = LEN(ALLTRIM(cString))
	IF ISNULL(cString)
		ASSERT .F. MESSAGE "At cStringBreak procedure"
	ENDIF
	FPUTS(nFilenum,cString)
ENDPROC


****************************
PROCEDURE cszbreak
****************************
	cCSZ = ALLT(m.CSZ)

	FOR ii = 5 TO 2 STEP -1
		cCSZ = STRTRAN(cCSZ,SPACE(ii),SPACE(1))
	ENDFOR
	cCSZ = STRTRAN(cCSZ,",","")
	len1 = LEN(ALLT(cCSZ))
	nSpaces = OCCURS(" ",cCSZ)

	IF nSpaces<2
		cErrMsg = "BAD CSZ INFO"
		DO ediupdate WITH cErrMsg,.T.
		THROW
	ENDIF

*	ASSERT .F. MESSAGE "In CSZ Breakout"
	FOR ii = nSpaces TO 1 STEP -1
		ii1 = ALLT(STR(ii))
		DO CASE
			CASE ii = nSpaces
				nPOS = AT(" ",cCSZ,ii)
				cZip = ALLT(SUBSTR(cCSZ,nPOS))
*				WAIT WINDOW "ZIP: "+cZip TIMEOUT 1
				nEndState = nPOS
			CASE ii = (nSpaces - 1)
				nPOS = AT(" ",cCSZ,ii)
				cState = ALLT(SUBSTR(cCSZ,nPOS,nEndState-nPOS))
*				WAIT WINDOW "STATE: "+cState TIMEOUT 1
				IF nSpaces = 2
					cCity = ALLT(LEFT(cCSZ,nPOS))
*					WAIT WINDOW "CITY: "+cCity TIMEOUT 1
					EXIT
				ENDIF
			OTHERWISE
				nPOS = AT(" ",cCSZ,ii)
				cCity = ALLT(LEFT(cCSZ,nPOS))
*				WAIT WINDOW "CITY: "+cCity TIMEOUT 2
		ENDCASE
	ENDFOR
ENDPROC

****************************
PROCEDURE num_decrement
****************************
*!* This procedure decrements the ISA/GS numbers in the counter
*!* in the event of failure and deletion of the current 945
	IF lCloseOutput
		IF !USED("serfile")
			USE ("f:\3pl\data\serial\"+ccustname+"945_serial") IN 0 ALIAS serfile
		ENDIF
		SELECT serfile
		REPLACE serfile.seqnum WITH serfile.seqnum - 1 IN serfile  && ISA number
		REPLACE serfile.grpseqnum WITH serfile.grpseqnum - 1 IN serfile  && GS number
		RETURN
	ENDIF
ENDPROC


****************************
PROCEDURE error_routine
****************************
	DO ediupdate WITH "UNHANDLED ERROR",.T.
	lDoCatch = .T.
	THROW
ENDPROC