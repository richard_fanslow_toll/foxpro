* inserts a records into a table/cursoradapter/view

*   gets unique ID field
*   fills in blank data for missing m. variables
*   changes blanks to null for date & datetime
*   sets updateby & addby
*   if WH table fills in office & mod

* 1. xcursor - cursor
* 2. xsystemmodule - database
* 3. xforceid - generate next ID value
* 4. xnovalid - used for intial data import - doesn't change updateby... fields
* 5. xnooffice - don't fill in office & mod in WH database

lparameters xcursor, xsystemmodule, xforceid, xnovalid, xnooffice

if type("xsystemmodule")="C"
	xsystemmodule=LOWER(xsystemmodule)
	if xsystemmodule="wh"
		xgenpk="whall"
	else
		xgenpk=xsystemmodule
	endif
endif

if type("xsystemmodule")="C" and upper(xsystemmodule)="WH" and !xnooffice
	m.office=gmasteroffice
	m.mod=goffice
	
*!*		if (type("gsystemmodule")="C" and gsystemmodule="RC") and inlist(m.office,"N") and guserid="DYOUNG"
*!*			set step on 
*!*			if !inlist(lower(xcursor),"whseloc","modacct","prod","dedemail","ufaddbk","cnee","vupsacctcomm","vinvoice","vinvdet","invpdf","vupcmast","invoice","invdet","editrigger") and vartype(accountid)="N" and vartype(office)="C" and vartype(mod)="C" 	
*!*				xfailed=.f.
*!*				try
*!*					xsqlexec("select * from modacct where accountid="+transform(m.accountid)+" and office='"+m.office+"'",,,"wh",,,,.t.)
*!*					m.mod=modacct.mod
*!*				catch
*!*					xfailed=.t.
*!*				endtry

*!*				if xfailed or empty(m.mod)
*!*					emailx("Dyoung@fmiint.com",,"INFO: xtumod not set in INSERTINTO - "+m.mod+crlf+xcursor+crlf+transform(m.accountid)+crlf+m.office,,.t.)
*!*				endif
*!*			endif
*!*		else
*!*			m.mod=goffice
*!*		endif
endif

afields(apreinsert,xcursor)

for qqqq=1 to alen(apreinsert,1)
	xpifield=trim(apreinsert[qqqq,1])
	
	do case
	case qqqq=1 and apreinsert[qqqq,2]="I" 
		if xforceid or ((type("m."+xpifield)="U" or m.&xpifield=0) and !empty(xsystemmodule))		&& changed dy 5/13/16, 6/14/16
*		if type("m."+xpifield)="U" or m.&xpifield=0		&& removed dy 4/26/16
*		if !empty(xsystemmodule)		&& removed dy 4/26/16
			if upper(xcursor)="V"	&& view
				xdycursor=substr(xcursor,2)
			else
				xdycursor=xcursor
			endif

			if inlist(xgenpk,"fx","ar","oo","qq","wh","bt","wo") or inlist(xcursor,"upcmast","vupcmast")
				m.&xpifield=sqlgenpk(xdycursor,xgenpk)
			else
				if type("m."+xpifield)="C"
*					email("Dyoung@fmiint.com","INFO: trapped m.&xpifield is a character 1 in insertinto",,,,,.t.,,,,,.t.,,.t.)
					release m.&xpifield
				ENDIF
				m.&xpifield=dygenpk(xdycursor,xgenpk)
			endif
		endif
*	case xnovalid
		* do nothing - used when loading data  dy 11/16/16
	case apreinsert[qqqq,2]="L"
		if type("m."+xpifield)="U"
			m.&xpifield=.f.
		endif
	case inlist(apreinsert[qqqq,2],"N","I")
		if type("m."+xpifield)="U"
			m.&xpifield=0
		endif
	case inlist(apreinsert[qqqq,2],"D","T")		&& un re-commented dy 2/20/17
*!*			if type("m."+xpifield)="U"
*!*				m.&xpifield=.null.
*!*			else
*!*				if empty(m.&xpifield)
*!*					m.&xpifield=.null.
*!*				endif
*!*			endif
		if type("m."+xpifield)="U" or empty(m.&xpifield)
			m.&xpifield=.null.
		endif
	otherwise
		if type("m."+xpifield)="U"
			m.&xpifield=""
		endif
	endcase
next

**populate update/add fields - mvw 04/04/16
if type("guserid")="C" and !empty(guserid) and !xnovalid
	xupdatebyprocess=iif(type("gprocess")="C" and !emptynul(gprocess),gprocess,"???")
	m.updateby=iif(type("m.updateby")#"U" and !emptynul(m.updateby),m.updateby,guserid)
	m.updatedt=datetime()
	m.updproc=iif(type("m.updproc")#"U" and !emptynul(m.updproc),m.updproc,xupdatebyprocess)
	m.addby=iif(type("m.addby")#"U" and !emptynul(m.addby),m.addby,guserid)
	m.adddt=datetime()
	m.addproc=iif(type("m.addproc")#"U" and !emptynul(m.addproc),m.addproc,xupdatebyprocess)
endif

m.deleteby=""
m.deletedt=.null.
m.delproc=""

if type("m.inwonum")="L"	&& dy 9/8/16
	m.inwonum=0
endif

*if type("m."+xpifield)="C"
*	email("Dyoung@fmiint.com","INFO: trapped m.&xpifield is a character 2 in insertinto",,,,,.t.,,,,,.t.,,.t.)
*	release m.&xpifield
*	m.&xpifield=dygenpk(xdycursor,xgenpk)
*endif

if type("m.adjid")="C"
	email("Dyoung@fmiint.com","INFO: trapped m.adjid is a character in insertinto",,,,,.t.,,,,,.t.,,.t.)
	m.adjid=dygenpk("adj",xgenpk)
endif

insert into &xcursor from memvar

release m.updatedt, m.updateby, m.adddt, m.addby
