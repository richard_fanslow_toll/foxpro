CLOSE DATA ALL
CLEAR
nDaysBack = 1

PUBLIC xfile
lTesting = .f.

lDoDetail = .F. && Default = .f.
lDoMail = .t. && Default = .t.
xfile = "c:\tempfox\uasummary.xls"

DO m:\dev\prg\_setvars WITH lTesting

DO CASE
	CASE DOW(DATE()-nDaysBack) = 1
		nDaysBack = nDaysBack+2
	CASE DOW(DATE()-nDaysBack) = 7
		nDaysBack = nDaysBack+1
ENDCASE

SET DATE LONG
IF lTesting
	? "Processing list from date "+DTOC(DATE()-nDaysBack)
ELSE
	WAIT WINDOW "Processing list from date "+DTOC(DATE()-nDaysBack) TIMEOUT 2
ENDIF

USE F:\3pl\DATA\edi_trigger ALIAS edi_trigger
SELECT IIF(edi_type='945X','CROSSDOCK','TRANSLOAD') AS TYPE,bol AS trip,trig_time,when_proc,PADR(JUSTFNAME(file945),40) AS sent_file ;
	FROM edi_trigger ;
	WHERE accountid = 5687 ;
	AND INLIST(edi_type,'945X','945T') ;
	AND TTOD(trig_time) = DATE()-nDaysBack ;
	GROUP BY 1,2 ;
	INTO CURSOR tempshipped READWRITE
IF lDoDetail
	LOCATE
	BROW
	COPY FIELDS trip,TYPE,sent_file TO TYPE XL5
ELSE
	nRecs = RECCOUNT()
	cNOF = IIF(nRecs = 0,"no",TRANSFORM(nRecs))
	WAIT WINDOW "Number of files: "+cNOF TIMEOUT 2
ENDIF

SELECT 0
USE F:\3pl\DATA\mailmaster ALIAS mm
IF lTesting
	LOCATE FOR mm.accountid = 9999 AND taskname = "ERROR"
ELSE
	LOCATE FOR mm.accountid = 5687 AND taskname = "SHIPSUMMARY"
ENDIF

tFrom ="TGF Warehouse Operations <toll-warehouse-ops@tollgroup.com>"
tsendto = IIF(mm.use_alt,TRIM(mm.sendtoalt),TRIM(mm.sendto))
tcc = IIF(mm.use_alt,TRIM(mm.ccalt),TRIM(mm.cc))
tattach = ""
tsubject = "Under Armour Prior Day Shipment Notices for "+DTOC(DATE())

IF lDoDetail
	tmessage = "Under Armour Prior Day Shipment Notices were sent per the attached Excel file"
	tattach = xfile
ELSE
	tmessage = "There were "+cNOF+" Under Armour shipment notices sent yesterday, "+DTOC(DATE()-1)
ENDIF

IF lDoMail
DO FORM m:\dev\frm\dartmail2 WITH tsendto,tFrom,tsubject,tcc,tattach,tmessage,"A"
endif

IF lDoDetail
	DELETE FILE [&xfile]
ENDIF

SET DATE AMERICAN
CLOSE DATABASES ALL
CLEAR ALL

