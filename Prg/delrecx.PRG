if gdevelopment
	return .t.
endif

xtable=alias()
m.deltable=xtable
m.delid=&xtable..&xtable.id
m.deletedt=datetime()

if type("guserid")="C" and !empty(guserid)
	m.deleteby=guserid
else
	m.deleteby="???"
endif

if type("gprocess")="C" and !empty(gprocess)
	m.delproc=gprocess
else
	m.delproc="???"
endif

if !used("delrec")
	do case
	case !glivedata
		use ("m:\dev\"+gsystemmodule+"data\delrec") in 0
	case gsystemmodule="WH"
		use (wf(goffice)+"delrec") alias delrec in 0
	otherwise
		use (gsystemmodule+"data\delrec") in 0
	endcase
endif

insert into delrec from memvar