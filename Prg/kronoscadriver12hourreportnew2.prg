* Create spreadsheet of Hours summary for CA Drivers for the previous pay period
* BY DAY, NOT ROLLED UP FOR THE WEEK!
** Meant to be run after hours are reviewed and cleaned up; Monday pm or Tuesday am.
* Modified to run daily against prior day if passed a 'BETTY' parameter, for Betty Mendoza.
* Modified to run against prior month if passed a 'STEVE' parameter, for Steve Sykes.

* 02/18/2014: modified to use TimeStar data instead of Kronos.

* 4/30/2018 MB: changed fmiint.com email to Toll

* EXE = F:\UTIL\KRONOS\KRONOSCADRIVERTIMEREPORT.EXE

LPARAMETERS tcMode

LOCAL loKronosCADriverTimeReport, lcMode

IF EMPTY( tcMode ) THEN
	lcMode = 'STANDARD'
ELSE
	lcMode = UPPER(ALLTRIM(tcMode))
ENDIF

runack("KRONOSCADRIVERTIMEREPORT")

utilsetup("KRONOSCADRIVERTIMEREPORT")

loKronosCADriverTimeReport = CREATEOBJECT('KronosCADriverTimeReport')
loKronosCADriverTimeReport.MAIN( lcMode )

=SchedUpdate()

CLOSE DATABASES ALL
RETURN

#DEFINE LOGIT 1
#DEFINE WAITIT 2
#DEFINE NOWAITIT 4
#DEFINE SENDIT 8
#DEFINE NOCRLF 16
#DEFINE CRLF CHR(13) + CHR(10)
#DEFINE LF CHR(10)
#DEFINE RETURN_DATA_MANDATORY .F.
#DEFINE RETURN_DATA_NOT_MANDATORY .T.
#DEFINE DAILY_OT_MULTIPLIER 1.5
#DEFINE DAILY_OT20_MULTIPLIER 2.0

DEFINE CLASS KronosCADriverTimeReport AS CUSTOM

	cProcessName = 'KronosCADriverTimeReport'

	lTestMode = .F.  && .F. FOR NORMAL OPERATION, .T. TO SUPPRESS ALL BUT DEVELOPER EMAILS AND CHANGE INPUT TABLE, ETC.

	* which payroll server, and associated connections, to hit
	lUsePAYROLL2Server = .T.

	lAutoYield = .T.

	cStartTime = TTOC(DATETIME())

	* mode properties
	cMode = ""

	* connection properties
	nSQLHandle = 0

	* file properties
	nFileHandle = 0

	* sql/report filter properties
	cTempCoWhere = ""
	cTempCoWhere2 = ""
	cTempCo = ""
	cTempCoName = ""
	cExtraWhere = ""
	cExtraWhere2 = ""
	* hours discrepancy testing props
	nMinHoursTest = 4
	nMaxHoursTest = 12

	* date properties
	dtNow = DATETIME()

	dEndDate = {}
	dStartDate = {}
	
	dToday = DATE()
	* to force different pay weeks
	*dToday = {^2017-08-01}

	cToday = DTOC(DATE())

	* wait window properties
	nWaitWindowTimeout = 2
	lWaitWindowIsOn = .T.

	* logfile properties
	lLoggingIsOn = .T.
	cLogFile = 'F:\UTIL\KRONOS\Logfiles\KronosCADriverTimeReport_log.txt'

	* processing properties
	lCheckBio = .T.

	* INTERNAL email properties
	lSendInternalEmailIsOn = .T.
	cFrom = 'mark.bennett@tollgroup.com'
	*cSendTo = 'cnevares@fmiint.com, hvalentin@fmiint.com, carlina.wix@tollgroup.com'
	cSendTo = ''
	cCC = 'mark.bennett@tollgroup.com'
	cSubject = ''
	cAttach = ''
	cBodyText = ''

	cCOMPUTERNAME = ''
	cUSERNAME = ''

	FUNCTION INIT
		IF NOT DODEFAULT()
			RETURN .F.
		ENDIF
		WITH THIS
			CLEAR
			*SET RESOURCE OFF
			CLOSE DATA
			SET CENTURY ON
			SET DATE YMD
			SET HOURS TO 12
			SET ANSI ON
			SET TALK OFF
			SET DELETED ON
			SET CONSOLE OFF
			SET EXCLUSIVE OFF
			SET SAFETY OFF
			SET EXACT OFF
			SET STATUS BAR ON
			SET SYSMENU OFF
			SET ENGINEBEHAVIOR 70
			SET DECIMALS TO 4
			_VFP.AUTOYIELD = .lAutoYield
			.cCOMPUTERNAME = GETENV("COMPUTERNAME")
			.cUSERNAME = GETENV("USERNAME")
			IF .lTestMode THEN
				.cSendTo = 'mark.bennett@tollgroup.com'
				.cCC = ''
				.cLogFile = 'F:\UTIL\KRONOS\Logfiles\KronosCADriverTimeReport_log_TESTMODE.txt'
			ENDIF
			.lLoggingIsOn = .lLoggingIsOn AND NOT EMPTY(.cLogFile)
			IF .lLoggingIsOn THEN
				SET ALTERNATE TO (.cLogFile) ADDITIVE
				SET ALTERNATE ON
			ENDIF
		ENDWITH
	ENDFUNC


	FUNCTION DESTROY
		WITH THIS
			IF .lLoggingIsOn  THEN
				SET ALTERNATE OFF
				SET ALTERNATE TO
			ENDIF
		ENDWITH
		DODEFAULT()
	ENDFUNC


	FUNCTION MAIN
		LPARAMETERS tcMode
		WITH THIS
			LOCAL lnNumberOfErrors, lcSQL, lcSQL2, lcSQL3, lcSQL4, lcSQL5, loError, lcTopBodyText, llValid
			LOCAL lcXLFileName, ldStartDate, ldEndDate, lcStartDate, lcEndDate, ldDate
			LOCAL lcSQLStartDate, lcSQLEndDate, lcXLFileName, ldYesterday
			LOCAL oExcel, oWorkbook, oWorksheet, lnRow, lcRow, lnStartRow, lcStartRow, lnEndRow, lnPP, lcPP, lnNN, lcNN, lcJJ
			LOCAL lcColsToProcess, lnColumn, lcBoldColumn, i, j, lcBoldRow, luCellValue, lcTargetDirectory, ldPayDate, lcFiletoSaveAs
			LOCAL lcFileDate, lcDivision, lcTitle, llSaveAgain, lcPayCode, lcSpecialWhere
			LOCAL lcSQLToday, lcSQLTomorrow, ldToday, ldTomorrow, lcExcluded9999s, lcFoxProTimeReviewerTable, lcMissingApprovals
			LOCAL lcAuditSubject, oWorkbookAUDIT, oWorksheetAUDIT, lcSendTo, lcCC, lcBodyText, lcXLFileNameAUDIT, lcTotalRow
			LOCAL lcTRWhere, lnStraightHours, lnStraightDollars, lnOTDollars, lcFilePrefix
			LOCAL ldPass2Date, lcPrevFileNum, lnPrevFileNumStartRow, lnPrevFileNumEndRow, lcPrevFileNumStartRow, lcPrevFileNumEndRow
			LOCAL lcOrderBy, lcPrevDept, lnPrevDeptStartRow, lnPrevDeptEndRow, lcPrevDeptStartRow, lcPrevDeptEndRow, lnTempMarkup
			LOCAL lcPrevDivision, lnPrevDivisionStartRow, lcPrevDivisionStartRow, lnPrevDivisionEndRow, lcPrevDivisionEndRow
			LOCAL lnShiftDiff, lcTopBodyText, lcXLSummaryFileName, lnWage, lnPay, lcRootPDFFilename
			LOCAL ltInpunch, lnTotHours, lcPRIMACCTCHANGED, lcNoBioFileNum, lcUnscannable, lcMode


			TRY
				lnNumberOfErrors = 0
				lcTopBodyText = ''

				.cMode = tcMode

				.TrackProgress('CA Driver Time Report process started....', LOGIT+SENDIT)
				.TrackProgress('COMPUTERNAME = ' + .cCOMPUTERNAME, LOGIT+SENDIT)
				.TrackProgress('USERNAME = ' + .cUSERNAME, LOGIT+SENDIT)
				.TrackProgress('Project =  KRONOSCADRIVERTIMEREPORT', LOGIT+SENDIT)
				.TrackProgress('Program =  kronoscadriver12hourreportnew2.prg', LOGIT+SENDIT)
				.TrackProgress('.cMode = ' + .cMode, LOGIT+SENDIT)
				IF .lTestMode THEN
					.TrackProgress('                        TEST MODE                        ', LOGIT+SENDIT)
				ENDIF

				IF NOT INLIST(.cMode,'BETTY','STANDARD','STEVE','PRIORMONTH') THEN
					.TrackProgress('====>ERROR: unexpected mode parameter!', LOGIT+SENDIT)
					THROW
				ENDIF

				ldToday = .dToday

				*!*					DO CASE
				*!*						CASE .cMode = 'BETTY'
				*!*							* nothing, leave today = today

				*!*						CASE .cMode = 'STANDARD'
				*!*							* set up report so that if if runs on Sunday, Monday or Tuesday it reports prior week, otherwise current week.
				*!*							IF INLIST(DOW(ldToday,1),4,5,6,7) THEN
				*!*								ldToday = ldToday + 7
				*!*							ENDIF
				*!*
				*!*						CASE .cMode = 'STEVE'
				*!*							* nothing, leave today = today

				*!*						OTHERWISE
				*!*							* nothing
				*!*					ENDCASE

				ldTomorrow = ldToday + 1
				ldYesterday = ldToday - 1

				DO CASE
					CASE .cMode = 'BETTY'
						* report on yesterday

						ldStartDate = ldYesterday
						ldEndDate = ldYesterday

					CASE .cMode = 'STANDARD'

						* calculate pay period start/end dates.
						* We go back from today until we hit a Saturday (DOW = 7) , which is the end day of the previous pay period.
						* then we subtract 6 days from that to get the prior Sunday (DOW = 1), which is the start day of the previous pay period.
						ldDate = ldToday
						DO WHILE DOW(ldDate,1) <> 7
							ldDate = ldDate - 1
						ENDDO
						ldEndDate = ldDate
						ldStartDate = ldDate - 6
						
*!*							****************************************************
*!*							****************************************************
*!*							* activate to force a special date range
*!*							ldStartDate = {^2017-09-24}
*!*							ldEndDate = {^2017-10-21}
*!*							****************************************************
*!*							****************************************************

					CASE .cMode = 'PRIORMONTH'

						* calculate start/end dates for prior month.
						ldEndDate = ldToday - DAY(ldToday)
						ldStartDate = GOMONTH(ldEndDate + 1, -1)

					CASE .cMode = 'STEVE'

						* calculate start/end dates for prior month.
						ldEndDate = ldToday - DAY(ldToday)
						ldStartDate = GOMONTH(ldEndDate + 1, -1)

					OTHERWISE
						THROW
				ENDCASE

				IF NOT .lTestMode THEN
					DO CASE
						CASE .cMode = 'BETTY'
							.cSendTo = 'Betty.Mendoza@Tollgroup.com'
							.cCC = 'mark.bennett@tollgroup.com'
						CASE .cMode = 'STANDARD'
							.cSendTo = 'diana.landeros@tollgroup.com, cesar.nevares@tollgroup.com'
							.cCC = 'mark.bennett@tollgroup.com'
						CASE .cMode = 'STEVE'
							.cSendTo = 'Mark.Bennett@Tollgroup.com'
							.cCC = 'mark.bennett@tollgroup.com'
						CASE .cMode = 'PRIORMONTH'
							.cSendTo = 'Diana.Landeros@Tollgroup.com'
							.cCC = 'mark.bennett@tollgroup.com'
						OTHERWISE
							*NOTHING
					ENDCASE
				ENDIF


*!*	***********************************
*!*	** force custom date ranges here
*!*	ldStartDate = {^2015-03-01}
*!*	ldEndDate = {^2015-03-28}
***********************************


				.dEndDate = ldEndDate
				.dStartDate = ldStartDate

				lcSpecialWhere = ""
				************************************************************

				SET DATE AMERICAN

				lcStartDate = STRTRAN(DTOC(ldStartDate),"/","-")
				lcEndDate = STRTRAN(DTOC(ldEndDate),"/","-")

				SET DATE YMD

				*	 create "YYYYMMDD" safe date format for use in SQL query
				* NOTE: for SQL we are going to use < EndDate ( not <= ), so we are going to add 1 to that date here for querying purposes.
				lcSQLStartDate = STRTRAN(DTOC(ldStartDate),"/","")
				lcSQLEndDate = STRTRAN(DTOC(ldEndDate + 1),"/","")
				lcSQLToday = STRTRAN(DTOC(ldToday),"/","")
				lcSQLTomorrow = STRTRAN(DTOC(ldTomorrow),"/","")

				SET DATE AMERICAN

				DO CASE
					CASE .cMode = 'BETTY'
						.cSubject = 'CA Driver Time for: ' + lcStartDate
					CASE .cMode = 'STANDARD'
						.cSubject = 'CA Driver Time for: ' + lcStartDate + " to " + lcEndDate
					CASE .cMode = 'PRIORMONTH'
						.cSubject = 'CA Driver Time for: ' + lcStartDate + " to " + lcEndDate
					CASE .cMode = 'STEVE'
						.cSubject = 'CA Driver Time for: ' + lcStartDate + " to " + lcEndDate
					OTHERWISE
						*NOTHING
				ENDCASE

				USE F:\UTIL\TIMESTAR\DATA\TIMEDATA.DBF IN 0 ALIAS TIMEDATA

				IF USED('CURDRIVERS') THEN
					USE IN CURDRIVERS
				ENDIF

				SELECT ;
					DIVISION, ;
					DEPT, ;
					NAME, ;
					FILE_NUM, ;
					WORKDATE, ;
					SUM(REGHOURS) AS REGHOURS, ;
					SUM(OTHOURS) AS OTHOURS, ;
					SUM(DTHOURS) AS DTHOURS, ;
					SUM(REGHOURS + OTHOURS + DTHOURS) AS TOTHOURS, ;
					CTOT('') AS INPUNCH, ;
					CTOT('') AS OUTPUNCH ;
					FROM TIMEDATA ;
					INTO CURSOR CURDRIVERS ;
					WHERE (DIVISION = '55') ;
					AND (DEPT = '0650') ;
					AND (WORKDATE >= ldStartDate) ;
					AND (WORKDATE <= ldEndDate) ;
					GROUP BY DIVISION, DEPT, NAME, FILE_NUM, WORKDATE ;
					ORDER BY NAME, WORKDATE ;
					READWRITE


				WAIT WINDOW NOWAIT "Preparing data..."

				SELECT CURDRIVERS
				GOTO TOP
				IF EOF() THEN
					.TrackProgress("There was no data to export!", LOGIT+SENDIT)
				ELSE
				
				
					* populate in and out punches in the cursor
					SELECT CURDRIVERS
					SCAN
					
						WAIT WINDOW NOWAIT 'Getting punches for ' + CURDRIVERS.NAME + '...'
					
						IF USED('CURIN') THEN 
							USE IN CURIN
						ENDIF
						
						IF USED('CUROUT') THEN 
							USE IN CUROUT
						ENDIF
						
						* GET IN PUNCH FOR WORKDAY
						SELECT MIN(INPUNCHDT) AS INPUNCH ;
						FROM TIMEDATA ;
						INTO CURSOR CURIN ;
						WHERE (INPUNCHNM = 'IND') AND (FILE_NUM = CURDRIVERS.FILE_NUM) AND (WORKDATE = CURDRIVERS.WORKDATE)
						
						* GET OUT PUNCH FOR WORKDAY
						SELECT MAX(OUTPUNCHDT) AS OUTPUNCH ;
						FROM TIMEDATA ;
						INTO CURSOR CUROUT ;
						WHERE (OUTPUNCHNM = 'OUT') AND (FILE_NUM = CURDRIVERS.FILE_NUM) AND (WORKDATE = CURDRIVERS.WORKDATE)
						
						REPLACE CURDRIVERS.INPUNCH WITH CURIN.INPUNCH, CURDRIVERS.OUTPUNCH WITH CUROUT.OUTPUNCH IN CURDRIVERS
					
					ENDSCAN
					
					WAIT CLEAR

*!*	SELECT CURDRIVERS
*!*	BROWSE

					DO CASE
						CASE .cMode = 'STEVE'
						
							* just create a summary-by-driver file for steve
							
							lcFilePrefix = "CA Driver Monthly Summary "
							lcFileDate = PADL(MONTH(ldToday),2,"0") + "-"  + PADL(DAY(ldToday),2,"0") + "-" + PADL(YEAR(ldToday),4,"0")
							lcTargetDirectory = "F:\UTIL\KRONOS\CADRIVERTIMEREPORTS\"
							lcFiletoSaveAs = lcTargetDirectory + lcFilePrefix + " " + lcFileDate
							lcXLFileName = lcFiletoSaveAs + ".XLS"

							lcTopBodyText = "See attached summary-by-driver spreadsheet for the prior month."
						
							IF USED('CURSTEVE') THEN
								USE IN CURSTEVE
							ENDIF
							
							SELECT NAME, SUM(TOTHOURS) AS TOTHOURS, 0 AS SHIFT ;
								FROM CURDRIVERS ;
								INTO CURSOR CURSTEVE ;
								GROUP BY 1, 3 ;
								ORDER BY 1
								
							*SELECT CURSTEVE
							*BROWSE
							
							IF FILE(lcXLFileName) THEN
								DELETE FILE (lcXLFileName)
							ENDIF
							
							SELECT CURSTEVE
							COPY TO (lcXLFileName) XL5

						OTHERWISE
						
							* STANDARD SPREADSHEET
							lcFilePrefix = "CA Driver Time "
							*ldPayDate = ldEndDate + 6
							lcFileDate = DTOS(ldStartDate) + "-"  + DTOS(ldEndDate)

							lcTargetDirectory = "F:\UTIL\KRONOS\CADRIVERTIMEREPORTS\" && + lcFileDate + "\"

							lcFiletoSaveAs = lcTargetDirectory + lcFilePrefix + " " + lcFileDate
							lcXLFileName = lcFiletoSaveAs + ".XLS"

							* create primary, detail spreadsheet

							WAIT WINDOW NOWAIT "Opening Excel..."
							oExcel = CREATEOBJECT("excel.application")
							oExcel.VISIBLE = .F.
							oWorkbook = oExcel.workbooks.OPEN("F:\UTIL\KRONOS\TEMPLATES\KRONOSCADRIVERTIMEREPORT2.XLS")

							* if target directory exists, save there
							llSaveAgain = .F.
							IF DIRECTORY(lcTargetDirectory) THEN
								IF FILE(lcXLFileName) THEN
									DELETE FILE (lcXLFileName)
								ENDIF
								oWorkbook.SAVEAS(lcFiletoSaveAs)
								* set flag to save again after sheet is populated
								llSaveAgain = .T.
							ENDIF
							***********************************************************************************************************************
							***********************************************************************************************************************
							WAIT WINDOW NOWAIT "Building CA DRIVER Time spreadsheet..."

							lnRow = 3
							lnStartRow = lnRow + 1
							lcStartRow = ALLTRIM(STR(lnStartRow))

							oWorksheet = oWorkbook.Worksheets[1]
							oWorksheet.RANGE("A" + lcStartRow,"P10000").clearcontents()

							oWorksheet.RANGE("A" + lcStartRow,"P10000").FONT.SIZE = 10
							oWorksheet.RANGE("A" + lcStartRow,"P10000").FONT.NAME = "Arial"
							oWorksheet.RANGE("A" + lcStartRow,"P10000").FONT.bold = .F.

							lcTitle = "CA Driver Time" + LF + ;
								" for: " + lcStartDate + " - " + lcEndDate

							oWorksheet.RANGE("A1").VALUE = lcTitle

							* main scan/processing
							SELECT CURDRIVERS
							SCAN
								* NO CHANGES; just advance a line
								lnRow = lnRow + 1

								lcRow = LTRIM(STR(lnRow))
								oWorksheet.RANGE("A" + lcRow).VALUE = "'" + ALLTRIM(CURDRIVERS.DIVISION)
								oWorksheet.RANGE("B" + lcRow).VALUE = "'" + ALLTRIM(CURDRIVERS.DEPT)
								oWorksheet.RANGE("C" + lcRow).VALUE = CURDRIVERS.NAME
								oWorksheet.RANGE("D" + lcRow).VALUE = CURDRIVERS.FILE_NUM
								oWorksheet.RANGE("E" + lcRow).VALUE = CURDRIVERS.WORKDATE
								oWorksheet.RANGE("F" + lcRow).VALUE = CURDRIVERS.REGHOURS
								oWorksheet.RANGE("G" + lcRow).VALUE = CURDRIVERS.OTHOURS
								oWorksheet.RANGE("H" + lcRow).VALUE = CURDRIVERS.TOTHOURS
								
								IF NOT EMPTYNUL(CURDRIVERS.INPUNCH) THEN
									oWorksheet.RANGE("J" + lcRow).VALUE = CURDRIVERS.INPUNCH
								ENDIF
								
								IF NOT EMPTYNUL(CURDRIVERS.OUTPUNCH) THEN
									oWorksheet.RANGE("K" + lcRow).VALUE = CURDRIVERS.OUTPUNCH
								ENDIF
								
								* hightlight hours if hours for shift were outside expected bounds
								lnTotHours = CURDRIVERS.TOTHOURS
								IF (lnTotHours < .nMinHoursTest) AND (lnTotHours > 0) THEN
									oWorksheet.RANGE("A" + lcRow,"K" + lcRow).FONT.ColorIndex = 3
									oWorksheet.RANGE("A" + lcRow,"K" + lcRow).FONT.bold = .T.
								ENDIF
								IF (lnTotHours > .nMaxHoursTest) THEN
									oWorksheet.RANGE("A" + lcRow,"K" + lcRow).FONT.ColorIndex = 3
									oWorksheet.RANGE("A" + lcRow,"K" + lcRow).FONT.bold = .T.
								ENDIF

							ENDSCAN


							oWorksheet.PageSetup.PrintArea = "$A$"  + lcStartRow + ":$K$" + lcRow

							*******************************************************************************************
							*******************************************************************************************

							* save again
							IF llSaveAgain THEN
								oWorkbook.SAVE()
							ENDIF
							oWorkbook.CLOSE()

							oExcel.QUIT()
							
							lcTopBodyText = "Shifts > " + TRANSFORM(.nMaxHoursTest,"99.9") + " hours worked are bolded and highlighted in red."

					ENDCASE

					IF FILE(lcXLFileName) THEN
						.cAttach = lcXLFileName
						.TrackProgress('Attached file : ' + lcXLFileName, LOGIT+SENDIT)
						.TrackProgress('CA Driver Time process ended normally.', LOGIT+SENDIT+NOWAITIT)
					ELSE
						.TrackProgress('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!', LOGIT+SENDIT)
						.TrackProgress("ERROR: There was a problem attaching file : " + lcXLFileName, LOGIT+SENDIT)
						.TrackProgress('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!', LOGIT+SENDIT)
					ENDIF

					IF .lTestMode THEN
						.cBodyText = lcTopBodyText + CRLF + CRLF + "<processing log follows>" + CRLF + CRLF + .cBodyText
					ELSE
						.cBodyText = lcTopBodyText + CRLF + CRLF + .cBodyText
					ENDIF

				ENDIF  && EOF() CURDRIVERS


				*CLOSE DATABASES ALL



			CATCH TO loError

				.TrackProgress('There was an error.',LOGIT+SENDIT)
				.TrackProgress('The Error # is: ' + TRANSFORM(loError.ERRORNO), LOGIT+SENDIT)
				.TrackProgress('The Error Message is: ' + TRANSFORM(loError.MESSAGE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred in Program: ' + TRANSFORM(loError.PROCEDURE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred at line #: ' + TRANSFORM(loError.LINENO), LOGIT+SENDIT)
				lnNumberOfErrors = lnNumberOfErrors + 1
				IF TYPE('oExcel') = 'O' AND NOT ISNULL(oExcel) THEN
					oExcel.QUIT()
				ENDIF
				*CLOSE DATA
				*CLOSE ALL

			ENDTRY

			CLOSE DATA
			WAIT CLEAR
			***************** INTERNAL email results ******************************
			.TrackProgress('About to send status email.',LOGIT)
			.TrackProgress('==================================================================================================================', SENDIT)
			.TrackProgress('The logfile is: ' + .cLogFile, SENDIT)
			.TrackProgress('Kronos CA Driver Time process started: ' + .cStartTime, LOGIT+SENDIT)
			.TrackProgress('Kronos CA Driver Time process finished: ' + TTOC(DATETIME()), LOGIT+SENDIT)

			IF .lSendInternalEmailIsOn THEN
				* try to trap error from not having dartmail dll's registered on user's pc...
				TRY
					DO FORM dartmail2 WITH .cSendTo,.cFrom,.cSubject,.cCC,.cAttach,.cBodyText,"A"
					.TrackProgress('Sent status email.',LOGIT)
				CATCH TO loError
					*=MESSAGEBOX('There was an error sending the status email. Please contact Mark Bennett.',0+64,"Kronos CA Driver Time")
					.TrackProgress('There was an error sending the status email.',LOGIT)
					.TrackProgress('The Error # is: ' + TRANSFORM(loError.ERRORNO), LOGIT)
					.TrackProgress('The Error Message is: ' + TRANSFORM(loError.MESSAGE), LOGIT)
					.TrackProgress('The Error occurred in Program: ' + TRANSFORM(loError.PROCEDURE), LOGIT)
					.TrackProgress('The Error occurred at line #: ' + TRANSFORM(loError.LINENO), LOGIT)
					lnNumberOfErrors = lnNumberOfErrors + 1
					*CLOSE DATA
				ENDTRY

			ELSE
				.TrackProgress('Did not send status email: THIS.lSendInternalEmailIsOn = FALSE.',LOGIT)
			ENDIF

		ENDWITH
		RETURN
	ENDFUNC && main




	PROCEDURE TrackProgress
		* do any combination of Wait Window, writing to logfile, and adding to body of email,
		* based on nFlags parameter.
		LPARAMETERS tcExpression, tnFlags
		LOCAL lcCRLF
		WITH THIS
			IF BITAND(tnFlags,NOCRLF) = NOCRLF THEN
				lcCRLF = ""
			ELSE
				lcCRLF = CRLF
			ENDIF
			IF BITAND(tnFlags,LOGIT) = LOGIT THEN
				IF .lLoggingIsOn THEN
					?
					? .cProcessName + "  " + TTOC(DATETIME()) + ": " + tcExpression
				ENDIF
			ENDIF
			IF .lWaitWindowIsOn THEN
				IF BITAND(tnFlags,NOWAITIT) = NOWAITIT THEN
					WAIT WINDOW tcExpression NOWAIT
				ENDIF
				IF BITAND(tnFlags,WAITIT) = WAITIT THEN
					WAIT WINDOW tcExpression TIMEOUT .nWaitWindowTimeout
				ENDIF
			ENDIF
			IF BITAND(tnFlags,SENDIT) = SENDIT THEN
				IF .lSendInternalEmailIsOn THEN
					.cBodyText = .cBodyText + tcExpression + CRLF + lcCRLF
				ENDIF
			ENDIF
		ENDWITH
	ENDPROC  &&  TrackProgress


	FUNCTION CreatePDFReport
		* create PDF report
		LPARAMETERS tcRootPDFFilename, tcPDFReportFRX

		WITH THIS

			.TrackProgress('in CreatePDFReport tcRootPDFFilename=' + tcRootPDFFilename, LOGIT)
			.TrackProgress('in CreatePDFReport tcPDFReportFRX=' + tcPDFReportFRX, LOGIT)

			LOCAL llPDFReportWasCreated, lcRootPDFFilename, lcFullPDFFilename, lcPDFReportFRX
			lcCentury = SET("CENTURY")

			SET CENTURY OFF  && FOR XX/XX/XX FORMAT DATES ON REPORT
			lcRootPDFFilename = tcRootPDFFilename
			lcPDFReportFRX = tcPDFReportFRX

			lcFullPDFFilename = lcRootPDFFilename + ".PDF"

			llPDFReportWasCreated = .F.

			DO M:\DEV\PRG\RPT2PDF WITH lcPDFReportFRX, lcRootPDFFilename, llPDFReportWasCreated

			IF NOT llPDFReportWasCreated THEN
				.TrackProgress('ERROR - PDF file [' + lcFullPDFFilename + '] was not created',LOGIT+WAITIT+SENDIT)
			ENDIF

			*!*				* keep track of pdf report files for emailing later
			*!*				.AddAttachment(lcFullPDFFilename)

			SET PRINTER TO DEFAULT
			SET CENTURY &lcCentury
		ENDWITH
	ENDFUNC  &&  CreatePDFReport


ENDDEFINE
