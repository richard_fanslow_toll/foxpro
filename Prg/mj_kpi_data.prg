* This program identifies when a a "A" whseloc was last pulled
utilsetup("MJ_KPI_DATA")
set exclusive off
set deleted on
set talk off
set safety off   &&Allows for overwrite of existing files


**********************************START KPI RETURNS
**********************************START NJ
_screen.windowstate = 1
close databases all
goffice='J'
useca("inwolog","wh",,,"mod='"+goffice+"' and  accountid in (6303,6325,6543) and confirmdt>{"+dtoc(date()-90)+"}")
useca("indet","wh",,,"mod='"+goffice+"' and  accountid in (6303,6325,6543) and date_rcvd>{"+dtoc(date()-90)+"}")

upcmastsql (6303)
select a.wo_num,substr(unbilledcomment,1,2)+'/'+substr(unbilledcomment,3,2)+'/20'+substr(unbilledcomment,5,2)as phy_rcpt_dt, ttod(confirmdt) as wms_rcpt_dt,;
	000000 as whse, a.accountid as wms_loc,acctname,reference as ra,unbilledcomment as cartonid, space(4) as shp_car, alltrim(style)+'-'+alltrim(color) as item, id as size, totqty,;
	style,color from inwolog a , indet b where  inlist(a.accountid,6303,6325,6543) and zreturns and a.inwologid=b.inwologid and container='PL'  and ttod(confirmdt) >=date()-60 into cursor n1 readwrite
replace all whse with 10100 for wms_loc=6303 in n1
replace all whse with 10180 for wms_loc=6325 in n1
replace all whse with 10190 for wms_loc=6543 in n1

**********************************START ML
use in inwolog
use in indet
goffice='L'
useca("inwolog","wh",,,"mod='"+goffice+"' and  accountid in (6303,6325,6543) and confirmdt>{"+dtoc(date()-90)+"}")
useca("indet","wh",,,"mod='"+goffice+"' and  accountid in (6303,6325,6543) and date_rcvd>{"+dtoc(date()-90)+"}")

select a.wo_num,substr(unbilledcomment,1,2)+'/'+substr(unbilledcomment,3,2)+'/20'+substr(unbilledcomment,5,2)as phy_rcpt_dt,ttod(confirmdt) as wms_rcpt_dt,;
	000000 as whse, a.accountid as wms_loc,acctname,reference as ra,unbilledcomment as cartonid, space(4) as shp_car, alltrim(style)+'-'+alltrim(color) as item, id as size, totqty,;
	style,color from inwolog a , indet b where inlist(a.accountid,6303,6325,6543) and zreturns  and a.inwologid=b.inwologid and container='ML' and ttod(confirmdt) >=date()-60 into cursor m1 readwrite
replace all whse with 11100 for wms_loc=6303 in m1
replace all whse with 11180 for wms_loc=6325 in m1
replace all whse with 11190 for wms_loc=6543 in m1

select * from n1 union all select * from m1  into cursor t1 readwrite

select wo_num,phy_rcpt_dt, wms_rcpt_dt, whse, wms_loc,acctname,ra,cartonid,shp_car,upc,item,size,totqty from t1 a left join upcmast b on a.style=b.style and a.color=b.color and a.size=b.id and b.accountid=6303 into cursor c1 readwrite
useca('mjraret','wh',.t.)
*!*	if !used("mj_ra_return")
*!*		use f:\wh\mj_ra_return.dbf in 0
*!*	endif

useca("cartret","wh",.t.,,,,"mj_carton_returns")

*********outslip
select * from c1 where  substr(ra,1,1)='2' and substr(ra,7,1)=' '  and substr(ra,6,1)!=' '   into cursor vout readwrite
select wo_num,phy_rcpt_dt, wms_rcpt_dt, whse, wms_loc,acctname, cust_acct, b.claim as ra, b.ra as cust_ref,a.cartonid,shp_car,upc,item,size,totqty from vout a left join mjraret b on a.ra=b.ra ;
	into cursor vout2 readwrite

*********NON outslip  10-  RA
select * from  c1 where substr(ra,1,3)='10-'  and !(inlist(substr(ra,4,1),'6','7')  and substr(ra,14,1)=' '  ) into cursor nonout10 readwrite
select wo_num,phy_rcpt_dt, wms_rcpt_dt, whse, wms_loc,acctname, cust_acct, a.ra,a.cartonid,shp_car,upc,item,size,totqty from  nonout10 a left join mjraret b on a.ra=b.ra ;
	into cursor nonout10_2 readwrite
select wo_num,phy_rcpt_dt, wms_rcpt_dt, whse, wms_loc,acctname, cust_acct, a.ra, b.claim,a.cartonid,shp_car,upc,item,size,totqty from  nonout10_2 a left join mj_carton_returns b on a.ra=b.ra ;
	and a.cartonid=b.carton_id into cursor nonout10_3 readwrite

*********NON outslip  6 or 7 RA
select * from  c1 where substr(ra,1,3)='10-'  and (inlist(substr(ra,4,1),'6','7')  and substr(ra,14,1)=' '  )  into cursor nonout67 readwrite
select wo_num,phy_rcpt_dt, wms_rcpt_dt, whse, wms_loc,acctname, cust_acct, a.ra,a.cartonid,shp_car,upc,item,size,totqty from  nonout67 a left join mjraret b on a.ra=b.ra ;
	into cursor nonout67_2 readwrite
select wo_num,phy_rcpt_dt,wms_rcpt_dt, whse, wms_loc,acctname, cust_acct, a.ra, b.claim,a.cartonid,shp_car,upc,item,size,totqty from  nonout67_2 a left join mj_carton_returns b on alltrim(a.ra)=alltrim(b.ra) ;
	and a.cartonid=b.carton_id into cursor nonout67_3 readwrite
select wo_num,phy_rcpt_dt,wms_rcpt_dt, whse, wms_loc,acctname, cust_acct, substr(ra,4,9) as ra, claim, cartonid,shp_car,upc,item,size,totqty from  nonout67_3 into cursor nonout67_4 readwrite

select * from vout2 union select * from nonout10_3 union select * from nonout67_4 into cursor final readwrite

*DHW*
SET STEP ON
*DHW*
select *, space(5) as whs_rtl from final into cursor final2 readwrite
replace whs_rtl with 'RTL' for cust_ref='2'
replace whs_rtl with 'WHLS' for cust_ref !='2'
if reccount() > 0
	select whse, wms_loc as account, wo_num,phy_rcpt_dt, dtoc(wms_rcpt_dt) as wms_rcpt_dt, acctname,cust_acct, ra, cust_ref, cartonid, shp_car, upc, item,size, totqty,whs_rtl  from final2 into cursor final2a readwrite
*	copy to s:\marcjacobsdata\reports\mjdashboard\mjkpireturns60 type csv
*DHW*	copy to F:\FTPUSERS\MJ_Wholesale\OUT\reports\mjkpireturns60 type csv
	copy to H:\mjkpireturns_ytd type csv

else
endif
select whse, wms_loc as account, wo_num,phy_rcpt_dt, dtoc(wms_rcpt_dt) as wms_rcpt_dt, acctname,cust_acct, ra, cust_ref, cartonid, shp_car, upc, item,size, totqty,whs_rtl  from final2 where wms_rcpt_dt >=date()-1 into cursor final3 readwrite
*copy to s:\marcjacobsdata\reports\mjdashboard\mjkpireturns type csv
*DHW* copy to F:\FTPUSERS\MJ_Wholesale\OUT\reports\mjkpireturns type csv
copy to H:\mjkpireturns type csv

xday=day(date())
if xday=1
	xmonth=month(date())-1
	xyear=year(date())
	if xmonth=0
		xmonth=12
		xyear=year(date())-1
	else
	endif
	select whse, wms_loc as account, wo_num,phy_rcpt_dt, dtoc(wms_rcpt_dt) as wms_rcpt_dt, acctname,cust_acct, ra, cust_ref, cartonid, shp_car, upc, item,size, totqty,whs_rtl  from final2 where month(wms_rcpt_dt)=xmonth and year(wms_rcpt_dt)=xyear into cursor final4 readwrite
*	copy to s:\marcjacobsdata\reports\mjdashboard\mjkpireturnsmnth type csv
	copy to F:\FTPUSERS\MJ_Wholesale\OUT\reports\mjkpireturnsmnth type csv

else
endif
use in inwolog
use in indet
use in final3
use in final2
use in final2a
use in final
use in nonout67_4
use in nonout67_3
use in nonout67_2
use in nonout67
use in nonout10_3
use in nonout10_2
use in nonout10
use in vout2
use in vout
use in mj_carton_returns
use in mjraret
use in c1
use in t1
use in m1
use in n1
**********************************END KPI RETURNS

**********************************START KPI RECEIVING
goffice='J'
useca("inwolog","wh",,,"mod='"+goffice+"' and  accountid in (6303,6325,6543) and confirmdt>{"+dtoc(date()-90)+"}")
useca("indet","wh",,,"mod='"+goffice+"' and  accountid in (6303,6325,6543) and date_rcvd>{"+dtoc(date()-90)+"}")

select space(6) as whse,inwolog.wo_num,confirmdt ,container,acct_ref,brokerref,comments,indet.style, indet.color,indet.id,indet.totqty,;
	space(5) as div , inwolog.accountid as wms_loc from inwolog ,  indet  where indet.inwologid = inwolog.inwologid;
	and inlist (inwolog.accountid ,6303,6325,6543) and units and !emptynul(inwolog.confirmdt) and confirmdt >= date()-60 and !zreturns and reference !='CANCELED'  into cursor n1 readwrite
replace all whse with '10100' for wms_loc=6303 in n1
replace all whse with '10180' for wms_loc=6325 in n1
replace all whse with '10190' for wms_loc=6543 in n1
select a.*,alltrim(descrip) as descrip , info  from n1 a left join upcmast b on a.style=b.style and a.color=b.color and a.id=b.id  into cursor nj1 readwrite
***RTS
select space(6) as whse,inwolog.wo_num,confirmdt ,container,acct_ref,brokerref,comments,indet.style, indet.color,indet.id,indet.totqty,;
	space(5) as div , inwolog.accountid as wms_loc from inwolog ,  indet  where indet.inwologid = inwolog.inwologid;
	and inlist (inwolog.accountid ,6303,6325,6543) and units and !emptynul(inwolog.confirmdt) and confirmdt >= date()-60 and !zreturns and reference ='CANCELED'  into cursor n2 readwrite
replace all whse with '10100' for wms_loc=6303 in n2
replace all whse with '10180' for wms_loc=6325 in n2
replace all whse with '10190' for wms_loc=6543 in n2
select a.*,alltrim(descrip) as descrip , info  from n2 a left join upcmast b on a.style=b.style and a.color=b.color and a.id=b.id  into cursor nj2 readwrite


use in inwolog
use in indet
goffice='L'
useca("inwolog","wh",,,"mod='"+goffice+"' and  accountid in (6303,6325,6543) and confirmdt>{"+dtoc(date()-90)+"}")
useca("indet","wh",,,"mod='"+goffice+"' and  accountid in (6303,6325,6543) and date_rcvd>{"+dtoc(date()-90)+"}")

select space(6) as whse, inwolog.wo_num,confirmdt,container,acct_ref,brokerref,comments,indet.style, indet.color,indet.id,indet.totqty,;
	space(5) as div , inwolog.accountid as wms_loc from inwolog ,  indet  where indet.inwologid = inwolog.inwologid;
	and inlist (inwolog.accountid ,6303,6325,6543) and units and !emptynul(inwolog.confirmdt) and confirmdt >= date()-60 and !zreturns and reference !='CANCELED'  into cursor m1 readwrite
replace all whse with '11100' for wms_loc=6303 in m1
replace all whse with '11180' for wms_loc=6325 in m1
replace all whse with '11190' for wms_loc=6543 in m1
select a.*,alltrim(descrip) as descrip , info  from m1 a left join upcmast b on a.style=b.style and a.color=b.color and a.id=b.id  into cursor ml1 readwrite
***RTS
select space(6) as whse, inwolog.wo_num,confirmdt,container,acct_ref,brokerref,comments,indet.style, indet.color,indet.id,indet.totqty,;
	space(5) as div , inwolog.accountid as wms_loc from inwolog ,  indet  where indet.inwologid = inwolog.inwologid;
	and inlist (inwolog.accountid ,6303,6325,6543) and units and !emptynul(inwolog.confirmdt) and confirmdt >= date()-60 and !zreturns and reference ='CANCELED'  into cursor m2 readwrite
replace all whse with '11100' for wms_loc=6303 in m2
replace all whse with '11180' for wms_loc=6325 in m2
replace all whse with '11190' for wms_loc=6543 in m2
select a.*,alltrim(descrip) as descrip , info  from m2 a left join upcmast b on a.style=b.style and a.color=b.color and a.id=b.id  into cursor ml2 readwrite

select * from nj1 union all select * from ml1 into cursor c1 readwrite
replace all div with getmemodata("info","DIV") in c1
select * from c1 where confirmdt >=date()-1 into cursor c2 readwrite
select  whse, wo_num,dtoc(confirmdt) as confirmdt ,container,acct_ref,brokerref, style, color, id, totqty,div from c2 order by confirmdt into cursor c3 readwrite
select c3
*copy to s:\marcjacobsdata\reports\mjdashboard\mjkpirec type csv
copy to F:\FTPUSERS\MJ_Wholesale\OUT\reports\mjkpirec type csv
select whse, wo_num,dtoc(confirmdt) as confirmdt ,container,acct_ref,brokerref, style,  color, id, totqty,div from c1 order by confirmdt into cursor c4 readwrite
select c4
*copy to s:\marcjacobsdata\reports\mjdashboard\mjkpirec60 type csv
copy to F:\FTPUSERS\MJ_Wholesale\OUT\reports\mjkpirec60 type csv
xday=day(date())
if xday=1
	xmonth=month(date())-1
	xyear=year(date())
	if xmonth=0
		xmonth=12
		xyear=year(date())-1
	else
	endif
	select * from c1 where month(confirmdt)=xmonth and year(confirmdt)=xyear into cursor c5 readwrite
	select whse, wo_num,dtoc(confirmdt) as confirmdt ,container,acct_ref,brokerref, style,  color, id, totqty,div from c5 order by confirmdt into cursor c6 readwrite
*	copy to s:\marcjacobsdata\reports\mjdashboard\mjkpirecmnth type csv
	copy to F:\FTPUSERS\MJ_Wholesale\OUT\reports\mjkpirecmnth type csv
else
endif



select * from nj2 union all select * from ml2 into cursor d1 readwrite
replace all div with getmemodata("info","DIV") in d1
select * from d1 where confirmdt >=date()-1 into cursor d2 readwrite
select  whse, wo_num,dtoc(confirmdt) as confirmdt ,container,acct_ref,brokerref, style, color, id, totqty,div from d2 order by confirmdt into cursor d3 readwrite
select d3
*copy to s:\marcjacobsdata\reports\mjdashboard\mjkpirts type csv
copy to F:\FTPUSERS\MJ_Wholesale\OUT\reports\mjkpirts type csv
select whse, wo_num,dtoc(confirmdt) as confirmdt ,container,acct_ref,brokerref, style,  color, id, totqty,div from d1 order by confirmdt into cursor d4 readwrite
select d4
*copy to s:\marcjacobsdata\reports\mjdashboard\mjkpirts60 type csv
copy to F:\FTPUSERS\MJ_Wholesale\OUT\reports\mjkpirts60 type csv
xday=day(date())
if xday=1
	xmonth=month(date())-1
	xyear=year(date())
	if xmonth=0
		xmonth=12
		xyear=year(date())-1
	else
	endif
	select * from d1 where month(confirmdt)=xmonth and year(confirmdt)=xyear into cursor d5 readwrite
	select whse, wo_num,dtoc(confirmdt) as confirmdt ,container,acct_ref,brokerref, style,  color, id, totqty,div from d5 order by confirmdt into cursor d6 readwrite
*	copy to s:\marcjacobsdata\reports\mjdashboard\mjkpirtsmnth type csv
	copy to F:\FTPUSERS\MJ_Wholesale\OUT\reports\mjkpirtsmnth type csv
else
endif

use in inwolog
use in indet
**********************************END KPI RECEIVING
**********************************START OUTBOUND
xsqlexec("select cast('' as char(6)) as whse, accountid, consignee, ship_ref, del_date, qty, batch_num as div, bol_no as bol " + ;
	"from outship where mod='J' and del_date>={"+dtoc(date()-60)+"} and cancelrts=0 and accountid#6453","n1",,"wh")

replace all whse with '10100' for accountid=6303 in n1
replace all whse with '10180' for accountid=6325 in n1
replace all whse with '10190' for accountid=6543 in n1

xsqlexec("select cast('' as char(6)) as whse, accountid, consignee, ship_ref, del_date, qty, batch_num as div, bol_no as bol " + ;
	"from outship where mod='J' and del_date>={"+dtoc(date()-60)+"} and cancelrts=0 and accountid#6453","n1",,"wh")

replace all whse with '11100' for accountid=6303 in m1
replace all whse with '11180' for accountid=6325 in m1
replace all whse with '11190' for accountid=6543 in m1


select * from n1 union all select * from m1 into cursor c1 readwrite

if !used("editrigger")
	use f:\3pl\data\edi_trigger shared in 0
endif

select a.*, trig_time,when_proc, fin_status,file945 from c1 a left join edi_trigger b on a.ship_ref=b.ship_ref order by del_date  into cursor c2 readwrite
select c2
scan
	if file945='F'
		replace file945 with justfname(file945)
	endif
endscan

select * from c2 where del_date  >=date()-1 order by del_date into cursor c3 readwrite
select whse,accountid as account, consignee,ship_ref,dtoc(del_date) as del_date, qty,div,bol, trig_time,when_proc,  fin_status as status, file945 from c3 into cursor c3a readwrite
*copy to s:\marcjacobsdata\reports\mjdashboard\mjkpioutbound type csv
copy to F:\FTPUSERS\MJ_Wholesale\OUT\reports\mjkpioutbound type csv

select  c2
select whse,accountid as account, consignee,ship_ref,dtoc(del_date) as del_date, qty,div,bol, trig_time,when_proc,  fin_status as status, file945 from c2 into cursor c2a readwrite
*copy to s:\marcjacobsdata\reports\mjdashboard\mjkpioutbound60 type csv
copy to F:\FTPUSERS\MJ_Wholesale\OUT\reports\mjkpioutbound60 type csv

xday=day(date())
if xday=1
	xmonth=month(date())-1
	xyear=year(date())
	if xmonth=0
		xmonth=12
		xyear=year(date())-1
	else
	endif
	select * from c2 where month(del_date)=xmonth and year(del_date)=xyear into cursor c5 readwrite
	select * from c5 order by del_date into cursor c6 readwrite
	select whse,accountid as account, consignee,ship_ref,dtoc(del_date) as del_date, qty,div,bol, trig_time,when_proc,  fin_status as status, file945 from c6 into cursor c6a readwrite
*	copy to s:\marcjacobsdata\reports\mjdashboard\mjkpioutboundmnth type csv
	copy to F:\FTPUSERS\MJ_Wholesale\OUT\reports\mjkpioutboundmnth type csv
else
endif

if used("outship")
	use in outship
endif

set step on

**********************************END OUTBOUND
**********************************START WIP
*!*		GOFFICE='J'
*!*		If usesql2()
*!*			xsqlexec("select * from pt where mod='"+GOFFICE+"'",,,"wh")
*!*			Index On ptid Tag ptid
*!*			Set Order To
*!*		Else
*!*			If !Used("pt")
*!*				Use F:\whj\whdata\pt.Dbf In 0
*!*			EndiF
*!*		Endif
*!*	If !Used("outship")
*!*		Use F:\whj\whdata\OUTSHIP.Dbf In 0
*!*	Endif

*!*	xsqlexec("select * from account where inactive=0","account",,"qq")
*!*	Index On accountid Tag accountid
*!*	Set Order To


*!*	Select dept, ship_ref, batch_num As div,consignee, cnee_ref, qty, ptdate, Start, Cancel, {}As called, {} routedfor, {} appt, scac, ;
*!*		space(20) appt_num,Space(35) apptremarks, ship_via,Space(20) bol_no, 00000000 wo_num,{}  wo_date,{}  pulleddt,{}  picked,{} ;
*!*		labeled,{} staged,000000  ctnqty,0000  outshipid,0000 As pt_age, 0000 As days_to_cncll, accountid As account,Space(2) As whse ;
*!*		from pt ;
*!*		where Inlist(accountid,6303,6304,6305,6320,6321,6322,6323,6324,6325,6364,6543) And qty!=0 	Into Cursor xrpt2 Readwrite

*!*	Select dept, ship_ref, batch_num As div,consignee, cnee_ref, qty, ptdate, Start, Cancel, called, routedfor, appt, scac, ;
*!*		appt_num, apptremarks, ship_via, bol_no, wo_num, wo_date, pulleddt, picked, labeled, staged, ctnqty, outshipid,0000 As pt_age, 0000 As days_to_cncl, accountid As account,Space(2) As whse ;
*!*		from OUTSHIP ;
*!*		where Inlist(accountid,6303,6304,6305,6320,6321,6322,6323,6324,6325,6364,6543) And Emptynul(del_date) And !notonwip ;
*!*		into Cursor xrpt1 Readwrite

*!*	Select * From xrpt1 Union All Select * From xrpt2 Into Cursor xrpt3 Readwrite
*!*	Replace All pt_age With (Date()-ptdate)
*!*	Replace All   days_to_cncl With 9999 For Cancel={01/01/1900}
*!*	Replace All days_to_cncl With (Cancel-Date()) For Cancel !={01/01/1900}
*!*	Replace All whse With 'NJ'
*!*	Select xrpt3.*, Space(15) As Status From xrpt3 Into Cursor XRPT4 Readwrite
*!*	Replace All Status With Iif(Emptynul(wo_date),"UNALLOCATED",Iif(!Emptynul(staged),"STAGED",Iif(!Emptynul(labeled),"LABELED",Iif(!Emptynul(picked),"PICKED",Iif(!Emptynul(pulleddt),"PULLED","ALLOCATED"))))) In XRPT4
*!*	Select *, Space(5) As AX_WHSE From XRPT4 Into Cursor xrptj Readwrite
*!*	*!*	replace ax_whse WITH '1000' FOR account =6303
*!*	*!*	replace ax_whse WITH '1200' FOR account =6304
*!*	*!*	replace ax_whse WITH '1320' FOR account =6305
*!*	*!*	replace ax_whse WITH '1310' FOR account =6320
*!*	*!*	replace ax_whse WITH '1221' FOR account =6321
*!*	*!*	replace ax_whse WITH '1222' FOR account =6322
*!*	*!*	replace ax_whse WITH '1223' FOR account =6323
*!*	*!*	replace ax_whse WITH '1250' FOR account =6324
*!*	*!*	replace ax_whse WITH '1151' FOR account =6325
*!*	*!*	replace ax_whse WITH '1400' FOR account =6364
*!*	*!*	replace ax_whse WITH '1600' FOR account =6543

*!*	Replace AX_WHSE With '10100' For account =6303
*!*	Replace AX_WHSE With '10180' For account =6325
*!*	Replace AX_WHSE With '10190' For account =6543


*!*	Scan
*!*		xsqlexec("select called from apptcall where mod='J' and outshipid="+Transform(xrptj.outshipid),,,"wh")
*!*		Calculate Min(called) To aa
*!*		Replace called With aa In xrptj
*!*	Endscan


*!*	**************remove for CA   SEE BELOW UNCOMMENT OUT USE STATEMENTS
*!*	***SELECT * FROM xrptj INTO CURSOR xrpt READWRITE
*!*	********************start CA
*!*		GOFFICE='L'
*!*		If usesql2()
*!*			xsqlexec("select * from pt where mod='"+GOFFICE+"'","ptc",,"wh")
*!*			Index On ptid Tag ptid
*!*			Set Order To
*!*		Else
*!*	If !Used("ptc")
*!*		Use F:\whl\whdata\pt.Dbf In 0  Alias ptc
*!*	Endif
*!*		Endif


*!*	If !Used("account")
*!*		xsqlexec("select * from account where inactive=0","account",,"qq")
*!*		Index On accountid Tag accountid
*!*		Set Order To
*!*	Endif
*!*	Use In OUTSHIP
*!*	If !Used("outshipc")
*!*		Use F:\whl\whdata\OUTSHIP.Dbf In 0  Alias outshipc
*!*	Endif

*!*	Select dept, ship_ref, batch_num As div,consignee, cnee_ref, qty, ptdate, Start, Cancel, {}As called, {} routedfor, {} appt, scac, ;
*!*		space(20) appt_num,Space(35) apptremarks, ship_via,Space(20) bol_no, 00000000 wo_num,{}  wo_date,{}  pulleddt,{}  picked,{}  labeled,{} staged,000000  ctnqty,0000  outshipid, ;
*!*		0000 As pt_age, 0000 As days_to_cncll, accountid As account,Space(2) As whse From ptc ;
*!*		where Inlist(accountid,6303,6543,6325) 	Into Cursor xrpt2c Readwrite

*!*	Select dept, ship_ref, batch_num As div,consignee, cnee_ref, qty, ptdate, Start, Cancel, called, routedfor, appt, scac, ;
*!*		appt_num, apptremarks, ship_via, bol_no, wo_num, wo_date, pulleddt, picked, labeled, staged, ctnqty, outshipid, ;
*!*		0000 As pt_age, 0000 As days_to_cncl, accountid As account,Space(2) As whse From outshipc ;
*!*		where Inlist(accountid,6303,6543,6325) And Emptynul(del_date) And !notonwip ;
*!*		into Cursor xrpt1c Readwrite

*!*	Select * From xrpt1c Union All Select * From xrpt2c Into Cursor xrpt3c Readwrite
*!*	Replace All pt_age With (Date()-ptdate)
*!*	Replace All   days_to_cncl With 9999 For Cancel={01/01/1900}
*!*	Replace All days_to_cncl With (Cancel-Date()) For Cancel !={01/01/1900}
*!*	Replace All whse With 'CA'
*!*	Select xrpt3c.*, Space(15) As Status From xrpt3c Into Cursor XRPT4c Readwrite
*!*	Replace All Status With Iif(Emptynul(wo_date),"UNALLOCATED",Iif(!Emptynul(staged),"STAGED",Iif(!Emptynul(labeled),"LABELED",Iif(!Emptynul(picked),"PICKED",Iif(!Emptynul(pulleddt),"PULLED","ALLOCATED"))))) In XRPT4c
*!*	Select *, Space(5) As AX_WHSE From XRPT4c Into Cursor xrptc Readwrite
*!*	Replace AX_WHSE With '11100' For account =6303
*!*	Replace AX_WHSE With '11180' For account =6325
*!*	Replace AX_WHSE With '11190' For account =6543

*!*	Scan
*!*		xsqlexec("select called from apptcall where mod='L' and outshipid="+Transform(xrptc.outshipid),,,"wh")
*!*		Calculate Min(called) To aa
*!*		Replace called With aa In xrptc
*!*	Endscan


*!*	Select * From xrptc Union All Select * From xrptj Into Cursor xrpt

*!*	Use In account
*!*	Use In apptcall
*!*	Use In outshipc
*!*	IF USED("outship")
*!*	Use In outship
*!*	endif
*!*	Use In pt
*!*	Use In ptc

*!*	Select  xrpt
*!*	copy To S:\MarcJacobsData\Reports\mjdashboard\mjkpiwip TYPE csv
**********************************END WIP
*!*	Use In xrpt
*!*	Use In xrptc
*!*	Use In XRPT4c
*!*	Use In xrpt3c
*!*	Use In xrpt2c
*!*	Use In xrpt1c
*!*	Use In xrptj
*!*	Use In XRPT4
*!*	Use In xrpt3
*!*	Use In xrpt2
*!*	Use In xrpt1
*!*	Use In c3
*!*	Use In c2
*!*	Use In edi_trigger
*!*	Use In C1
*!*	Use In M1
*!*	Use In N1
*!*	Use In d4
*!*	Use In d3
*!*	Use In d2
*!*	Use In d1
*!*	Use In c4
*!*	Use In ml2
*!*	Use In m2
*!*	Use In ml1
*!*	Use In nj2
*!*	Use In n2
*!*	Use In nj1
**********************************START ONHAND

goffice='J'

xsqlexec("select outship.accountid, style, color, id, totqty as pulledqty from outship, outdet where outship.mod='J' " + ;
	"and outship.outshipid=outdet.outshipid and cancelrts=0 and pulleddt#{} and del_date={} and bol_no#'NOTH' " + ;
	"and outship.accountid#6453 and notonwip=0","p1a",,"wh")

xsqlexec("select * from inven where mod='J'",,,"wh")
xsqlexec("select * from invenloc where mod='J'",,,"wh")
goffice='J'
useca("indet","wh",,,"mod='"+goffice+"' and  accountid in (6303,6325,6543) and date_rcvd>{"+dtoc(date()-90)+"}")

select accountid,style,color,id,totqty,holdqty,allocqty,availqty, space(3) as div from inven where totqty+holdqty+allocqty!=0 and accountid !=6453 into cursor t1 readwrite
select accountid,style,color,id,sum(pulledqty) as pulledqty from p1a group by 1,2,3,4 into cursor p1 readwrite
select * from t1 a full join p1 b on a.accountid=b.accountid and a.style=b.style and a.color=b.color and a.id=b.id into cursor p2 readwrite
replace accountid_a with accountid_b for isnull(accountid_a)
replace style_a with style_b for isnull(style_a)
replace color_a with color_b for isnull(color_a)
replace id_a with id_b for isnull(id_a)
replace totqty with 0 for isnull(totqty)
replace holdqty with 0 for isnull(holdqty)
replace allocqty with 0 for isnull(allocqty)
replace availqty with 0 for isnull(availqty)
replace pulledqty with 0 for isnull(pulledqty)
select accountid_a as accountid, style_a as style, color_a as color,id_a as id,totqty+pulledqty as totqty,holdqty,allocqty,pulledqty, availqty,div from p2 into cursor p3 readwrite

select a.*, info,upc from p3 a left join upcmast b on a.style=b.style and a.color=b.color and a.id=b.id and b.accountid=6303 into cursor c1 readwrite
replace all div with getmemodata("info","DIV")
select * from c1 into cursor n1 readwrite

xsqlexec("select * from outship where mod='J' and inlist(accountid,6303,6325,6543) and wo_date>{"+dtoc(date()-120)+"}",,,"wh")
xsqlexec("select * from outdet where mod='J' and inlist(accountid,6303,6325,6543) and wo_date>{"+dtoc(date()-120)+"}",,,"wh")

select a.*,outshipid from n1 a left join outdet b on a.accountid=b.accountid and a.style=b.style and a.color=b.color and a.id=b.id and !inlist(a.accountid,6453) into cursor n2 readwrite
select a.*, del_date from n2 a left join outship b on a.outshipid=b.outshipid into cursor n3 readwrite

select accountid,style,color,id,totqty,holdqty,allocqty, pulledqty, availqty,div,upc,max(del_date) from n3 group by accountid,style,color,id,totqty,holdqty,allocqty,pulledqty, availqty,div,upc into cursor n4 readwrite
select space(6) as whse,a.*,b.max_del_date as last_shpd from c1 a left join n4 b on a.accountid=b.accountid and a.style=b.style and a.color=b.color and a.id=b.id into cursor n5 readwrite
select  whse,a.accountid,a.style,a.color,a.id,a.totqty,holdqty,allocqty, pulledqty, availqty,div,upc ,last_shpd, max(date_rcvd) as date_rcvd  from n5 a left join indet b on a.accountid=b.accountid and a.style=b.style;
	and a.color=b.color and a.id=b.id group by whse, a.accountid,a.style,a.color,a.id,a.totqty,holdqty,allocqty,pulledqty,availqty,div,upc,last_shpd into cursor n6 readwrite

***Select Space(6) As whse,a.*,date_rcvd From n5 a Left Join indet b On a.accountid=b.accountid And a.Style=b.Style And a.Color=b.Color And a.Id=b.Id Group By WHSE, a.accountid,a.Style,a.Color,a.Id,a.totqty,holdqty,allocqty,availqty,div,upc,last_shpd Into Cursor n6 Readwrite
replace all whse with '10100' for accountid=6303 in n6
replace all whse with '10180' for accountid=6325 in n6
replace all whse with '10190' for accountid=6543 in n6
*******added TMARG 7/6/16 to capture QC quantity
****remove quantity in QC location from 6303 and into 10160
select accountid,style,color,id,locqty as qcqty from invenloc where 'QC' $whseloc and locqty!=0 into cursor qc1 readwrite
if reccount() > 0
	select a.*,qcqty from n6 a full join qc1 b on a.accountid=b.accountid and a.style=b.style and a.color=b.color and a.id=b.id into cursor n7 readwrite
	replace qcqty with 0 for isnull(qcqty) in n7
	select whse, accountid, style,color,id,totqty-qcqty as totqty,holdqty-qcqty as holdqty, allocqty,pulledqty,availqty,div,upc,last_shpd,date_rcvd,qcqty from n7 into cursor n8 readwrite
	replace qcqty with 0 for !isnull(qcqty) in n8

	select space(6) as whse, accountid,style,color,id,qcqty as totqty,qcqty as holdqty,0000 as allocqty,0000 as pulledqty, 0000 as availqty, space(3) as div,space(14) as upc,date() as last_shpd, date() as date_rcvd,qcqty from qc1 into cursor qc2 readwrite
	replace last_shpd with {  /  /    } for !isnull(last_shpd)
	replace date_rcvd with {  /  /    } for !isnull(date_rcvd)
	replace all whse with '10100' for accountid=6303 in qc2
	replace all whse with '10180' for accountid=6325 in qc2
	replace all whse with '10190' for accountid=6543 in qc2
	select a.*,b.upc as upc2, info from qc2 a left join upcmast b on a.style=b.style and a.color=b.color and a.id=b.id into cursor qc3 readwrite
	replace upc with upc2 for empty(upc)
	replace all div with getmemodata("info","DIV")
	select whse, accountid,style,color,id,totqty,holdqty, allocqty, pulledqty, availqty, div, upc, last_shpd,  date_rcvd,qcqty from qc3 into cursor qc4 readwrite
	select * from n8 union all select * from qc4 into cursor n6a readwrite


else
	select *, 00000 as qcqty from n6 into cursor n6a readwrite
endif
select whse,space(6) as ax_whse, style,color,id,totqty,holdqty,allocqty,pulledqty,availqty,div,upc,last_shpd,date_rcvd,qcqty from n6a into cursor n6b readwrite
replace ax_whse with '10160' for qcqty!=0 in n6b
replace ax_whse with '10100' for qcqty=0 and whse='10100' in n6b
replace ax_whse with '10180' for qcqty=0 and whse='10180' in n6b
replace ax_whse with '10190' for qcqty=0 and whse='10190' in n6b

use in outship
use in outdet
use in inven
use in invenloc
use in indet
*****************************ML

xsqlexec("select outship.accountid, style, color, id, totqty as pulledqty from outship, outdet where outship.mod='L' " + ;
	"and outship.outshipid=outdet.outshipid and cancelrts=0 and pulleddt#{} and del_date={} and bol_no#'NOTH' " + ;
	"and outship.accountid#6453 and notonwip=0","q1a",,"wh")

xsqlexec("select * from inven where mod='L'",,,"wh")
xsqlexec("select * from invenloc where mod='L'",,,"wh")

useca("indet","wh",,,"mod='L' and  accountid in (6303,6325,6543) and date_rcvd>{"+dtoc(date()-90)+"}")

select accountid,style,color,id,totqty,holdqty,allocqty, availqty, space(3) as div from inven where totqty+holdqty+allocqty!=0 and inlist(accountid,6303,6325,6543) into cursor t2 readwrite

select accountid,style,color,id,sum(pulledqty) as pulledqty from q1a group by 1,2,3,4 into cursor q1 readwrite
select * from t2 a full join q1 b on a.accountid=b.accountid and a.style=b.style and a.color=b.color and a.id=b.id into cursor q2 readwrite
replace accountid_a with accountid_b for isnull(accountid_a)
replace style_a with style_b for isnull(style_a)
replace color_a with color_b for isnull(color_a)
replace id_a with id_b for isnull(id_a)
replace totqty with 0 for isnull(totqty)
replace holdqty with 0 for isnull(holdqty)
replace allocqty with 0 for isnull(allocqty)
replace availqty with 0 for isnull(availqty)
replace pulledqty with 0 for isnull(pulledqty)
select accountid_a as accountid, style_a as style, color_a as color,id_a as id,totqty+pulledqty as totqty,holdqty,allocqty,pulledqty, availqty,div from q2 into cursor q3 readwrite


select a.*, info,upc from q3 a left join upcmast b on a.style=b.style and a.color=b.color and a.id=b.id and b.accountid=6303 into cursor c2 readwrite
replace all div with getmemodata("info","DIV")
select * from c2  into cursor m1 readwrite

xsqlexec("select * from outship where mod='L' and inlist(accountid,6303,6325,6543) and wo_date>{"+dtoc(date()-120)+"}",,,"wh")
xsqlexec("select * from outdet where mod='L' and inlist(accountid,6303,6325,6543) and wo_date>{"+dtoc(date()-120)+"}",,,"wh")

select a.*,outshipid from m1 a left join outdet b on a.accountid=b.accountid and a.style=b.style and a.color=b.color and a.id=b.id and inlist(a.accountid,6303,6325,6543) into cursor m2 readwrite
select a.*, del_date from m2 a left join outship b on a.outshipid=b.outshipid into cursor m3 readwrite

select accountid,style,color,id,totqty,holdqty,allocqty,  pulledqty, availqty,div,upc,max(del_date) as last_shpd from m3 group by accountid,style,color,id,totqty,holdqty,allocqty,pulledqty, availqty,div,upc into cursor m4 readwrite
select a.*, last_shpd from c2 a left join m4 b on a.accountid=b.accountid and a.style=b.style and a.color=b.color and a.id=b.id into cursor m5 readwrite
select space(6) as whse,a.*,max(date_rcvd) as date_rcvd  from m5 a left join indet b on a.accountid=b.accountid and a.style=b.style and a.color=b.color and a.id=b.id group by whse, a.accountid,a.style,a.color,a.id,a.totqty,holdqty,allocqty,availqty,div,upc,last_shpd into cursor m6 readwrite


***Select Space(6) As whse,a.*,date_rcvd From m5 a Left Join indet b On a.accountid=b.accountid And a.Style=b.Style And a.Color=b.Color And a.Id=b.Id Group By a.accountid,a.Style,a.Color,a.Id,a.totqty,holdqty,allocqty,availqty,div,upc,last_shpd Into Cursor m6 Readwrite
replace all whse with '11100' for accountid=6303 in m6
replace all whse with '11180' for accountid=6325 in m6
replace all whse with '11190' for accountid=6543 in m6
*******added TMARG 7/6/16 to capture QC quantity
*!*	select accountid,style,color,id,locqty as qcqty from invenloc where 'QC' $whseloc and locqty!=0 into cursor qc2 readwrite
*!*	if reccount() > 0
*!*		select a.*,qcqty from m6 a full join qc2 b on a.accountid=b.accountid and a.style=b.style and a.color=b.color and a.id=b.id into cursor m7 readwrite
*!*		replace qcqty with 0 for isnull(qcqty)
*!*		select whse, accountid, style,color,id,totqty,holdqty-qcqty as holdqty, allocqty,pulledqty,availqty,div,upc,last_shpd,date_rcvd,qcqty from m7 into cursor m6a readwrite
*!*	else
*!*		select *, 00000 as qcqty from m6 into cursor m6a readwrite
*!*	ENDIF


****remove quantity in QC location from 6303 and into 11160
select accountid,style,color,id,locqty as qcqty from invenloc where 'QC' $whseloc and locqty!=0 into cursor qc1m readwrite
if reccount() > 0
	select a.*,qcqty from m6 a full join qc1m b on a.accountid=b.accountid and a.style=b.style and a.color=b.color and a.id=b.id into cursor m7 readwrite
	replace qcqty with 0 for isnull(qcqty) in m7
	select whse, accountid, style,color,id,totqty-qcqty as totqty,holdqty-qcqty as holdqty, allocqty,pulledqty,availqty,div,upc,last_shpd,date_rcvd,qcqty from m7 into cursor m8 readwrite
	replace qcqty with 0 for !isnull(qcqty) in m8

	select space(6) as whse, accountid,style,color,id,qcqty as totqty,qcqty as holdqty,0000 as allocqty,0000 as pulledqty, 0000 as availqty, space(3) as div,space(14) as upc,date() as last_shpd, date() as date_rcvd,qcqty from qc1m into cursor qc2m readwrite
	replace last_shpd with {  /  /    } for !isnull(last_shpd) in qc2m
	replace date_rcvd with {  /  /    } for !isnull(date_rcvd) in qc2m
	replace all whse with '11100' for accountid=6303 in qc2m
	replace all whse with '11180' for accountid=6325 in qc2m
	replace all whse with '11190' for accountid=6543 in qc2m
	select a.*,b.upc as upc2, info from qc2m a left join upcmast b on a.style=b.style and a.color=b.color and a.id=b.id into cursor qc3m readwrite
	replace upc with upc2 for empty(upc) in qc3m
	replace all div with getmemodata("info","DIV")  in qc3m
	select whse, accountid,style,color,id,totqty,holdqty, allocqty, pulledqty, availqty, div, upc, last_shpd,  date_rcvd,qcqty from qc3m into cursor qc4m readwrite
	select * from m8 union all select * from qc4m into cursor m6a readwrite
else
	select whse, accountid,style,color,id,totqty,holdqty, allocqty, pulledqty, availqty, div, upc, last_shpd,  date_rcvd, 00000 as qcqty from m6 into cursor m6a readwrite
endif
select whse,space(6) as ax_whse, style,color,id,totqty,holdqty,allocqty,pulledqty,availqty,div,upc,last_shpd,date_rcvd,qcqty from m6a into cursor m6b readwrite
replace ax_whse with '11160' for qcqty!=0 in m6b
replace ax_whse with '11100' for qcqty=0 and whse='11100' in m6b
replace ax_whse with '11180' for qcqty=0 and whse='11180' in m6b
replace ax_whse with '11190' for qcqty=0 and whse='11190' in m6b

select * from n6b union all select * from m6b into cursor final readwrite
select final

*copy to s:\marcjacobsdata\reports\mjdashboard\mjkpionhand type csv
copy to F:\FTPUSERS\MJ_Wholesale\OUT\reports\mjkpionhand type csv
close data all



*************************************KPI return carton stats

goffice='J'
useca("inwolog","wh",,,"mod='"+goffice+"' and  accountid in (6303,6325,6543) and confirmdt>{"+dtoc(date()-90)+"}")

useca("cartret","wh",.t.,,,,"mj_carton_returns")

useca('mjraret','wh',.t.)
*!*	if !used("mj_ra_return")
*!*		use f:\wh\mj_ra_return.dbf in 0
*!*	endif
***This shows return carton status upto 5 days after carton is processed
select distinct ra,consignee from mjraret into cursor tempra readwrite
select * from mj_carton_returns where createdt > {11/16/2014} into cursor temp11 readwrite
select a.*,wo_num,axdt from temp11 a left join inwolog b on a.carton_id=b.unbilledcomment into cursor temp11a readwrite

use in inwolog
goffice='L'
useca("inwolog","wh",,,"mod='"+goffice+"' and  accountid in (6303,6325,6543) and confirmdt>{"+dtoc(date()-90)+"}")

select a.*,b.wo_num,b.axdt as laxdt from temp11a a left join inwolog b on a.carton_id=b.unbilledcomment into cursor temp11b readwrite
replace wo_num_a with wo_num_b for isnull(wo_num_a) and !isnull(wo_num_b) in temp11b
replace axdt with laxdt for isnull(axdt) and !isnull(laxdt) in temp11b
delete from temp11b where axdt<date()-6 and !emptynul(axdt)

select * from temp11b a left join  tempra b on a.ra=b.ra and !empty(consignee) into cursor temp12 readwrite
select dtoc(createdt) as date_rcvd,ra_a as rma,claim,consignee,space(10) as consignee_address,unit_qty, carton_qty,carton_id, space(20) as status,refurb_loc,cartonloc, wo_num_a as wo_num from temp12 into cursor temp13 readwrite
select * from temp13 where (substr(rma,1,1)!='2' and substr(rma,7,1)!=' ') and substr(rma,1,3)!='JAM' and substr(rma,1,3)!='GSI'  into cursor temp14

xsqlexec("select carton_id, inwonum from cmtrans","xdy",,"wh")
select carton_id ,inwonum from xdy group by 1,2 into cursor cmtrans2 readwrite

select * from temp14 a left join cmtrans2 b on a.carton_id=b.carton_id into cursor temp15 readwrite

replace status with 'SCANNED' for !isnull(carton_id_b)
replace status with 'CARTON RCVD' for isnull(carton_id_b)
replace status with 'IN REFURB' for isnull(carton_id_b) and !empty(refurb_loc)
replace status with 'TO BE SCANNED TO PL' for isnull(carton_id_b) and !empty(cartonloc)
replace status with 'WO CREATED' for !isnull(inwonum) and (inwonum)!=0
select a.*,confirmdt,axdt,brokerref from temp15 a left join inwolog b on a.inwonum=b.wo_num into cursor temp16 readwrite
replace status with 'WO CONFIRMED' for !isnull(confirmdt)
replace status with 'AX INPUT CREATED' for !isnull(axdt)
replace status with 'NO AX INPUT' for brokerref='NO A'
delete for axdt <date()-6
select date_rcvd, rma,claim, consignee, consignee_address, unit_qty,carton_qty,carton_id_a as carton_id,status,wo_num from temp16 into cursor temp17 readwrite
replace rma with substr(rma,4,15)  for inlist(rma,'10-6','10-7')  in temp17
select temp17
if reccount() > 0
	copy to "F:\FTPUSERS\MJ_Wholesale\OUT\reports\mjkpirtncartonstats "  type csv
else
ENDIF

********************************************START CYCLE COUNT WHSE LOC	
useca ("WHSELOC",'wh',,,'m-j')
useca ("invenloc",'wh',,,'m-j')	&& dy 7/9/17

SELECT whseloc FROM whseloc WHERE whseloc !='PL' ORDER BY whseloc INTO CURSOR t1 READWRITE
SELECT accountid, WHSELOC ,SUM(LOCQTY) AS LOCQTY FROM INVENLOC WHERE LOCQTY!=0 GROUP BY WHSELOC INTO CURSOR V1 READWRITE
SELECT ACCOUNTID, A.WHSELOC,LOCQTY , SPACE(5) AS INVENTORY FROM T1 A LEFT JOIN V1 B ON A.WHSELOC=B.WHSELOC INTO CURSOR CI1 READWRITE
REPLACE inventory WITH 'YES' FOR !ISNULL(LOCQTY)
REPLACE inventory WITH 'NO' FOR ISNULL(LOCQTY)
DELETE FOR ACCOUNTID=6453
SELECT ACCOUNTID,  WHSELOC, INVENTORY FROM CI1 INTO CURSOR CI2 READWRITE

*COPY TO "S:\MarcJacobsData\Reports\mjdashboard\whseloc" type csv
COPY TO "F:\FTPUSERS\MJ_Wholesale\OUT\reports\whseloc" type csv

	
close data all 

wait window at 10,10 "all done...." timeout 2
close data all

schedupdate()
_screen.caption=gscreencaption
on error
