*!*  2XU EDI 945 (Whse. Shipping Advice) Creation Program
*!* Creation Date: 07.18.2016 by PG (Derived from Steelseries945_create program)

PARAMETERS cBOL
WAIT WINDOW "Now at start of 2XU EDI 945 process" NOWAIT

PUBLIC ARRAY thisarray(1)
PUBLIC c_CntrlNum,c_GrpCntrlNum,cReturned,cWO_Num,cWO_NumStr,nWO_Num1,lTesting,lDoUPC,nAcctNum
PUBLIC lEmail,dDateTimeCal,nFilenum,nWO_Xcheck,nOutshipid,cCharge,lDoSQL,dapptnum,tfrom,cMailName,lSetDelDate,lOverflow
PUBLIC cCity,cState,cZip,nOrigSeq,nOrigGrpSeq,tsendto,tcc,tsendtoerr,tccerr,cISA_Num,cErrMsg,lAmazon,cCustname,lDoScanpack,nDaysBack
PUBLIC cMod,cMBOL,cEDIType,lParcelType,cFilenameShort,cFilenameOut,cFilenameHold,cFilenameArch,lcPath,cWO_NumList,cISA_Num,cShip_ref
SET REPROCESS TO 500

lTesting    = .f.  && Set to .t. for testing
lTestinput  = .f.  &&  Set to .t. for test input files only!
lEmail      = .t.
lTestMail   = lTesting && Sends mail to Joe only
lStandalone = lTesting

lSetDelDate = .f. && Normally true ; set to .f., removes 10-day restriction for re-running older 945s., using next parameter, nDaysBack.
nDaysBack = 120  && Will only work if lSetDelDate=.f.
lDoScanpack = .t.
cShip_ref = ""

IF lTesting
	CLOSE DATABASES ALL
	DO m:\dev\prg\lookups
ENDIF
DO m:\dev\prg\_setvars WITH lTesting

tsendto  = ""
tsendtotest=""
tcc = ""
tcctest=""


TRY
	lDoSQL = .T.
	lPick = .F.
	lPrepack = .F.
	lJCP = .F.
	lIsError = .F.
	lDoCatch = .T.
	cShip_ref = ""
	lCloseOutput = .T.
	nFilenum = 0
	cCharge = "0.00"
	cWO_NumStr = ""
	cWO_NumList = ""
	cString = ""
	cErrMsg = ""
	nPTCtnTot = 0

	cOffice = "Y"
	cMod = cOffice
	gMasterOffice = cOffice
	gOffice = cMod

	cMBOL = ""
	cEDIType = "945"
	lParcelType = .F.

	nAcctNum = 6665
	guserid = "JOEB"

	lFederated = .F.
	nOrigSeq = 0
	nOrigGrpSeq = 0
	cPPName = "2XU"
	cMailName = "2XU"
	WAIT WINDOW "At the start of "+cMailName+" 945 process..." NOWAIT
	tfrom = "TGF EDI Operations <toll-edi-ops@tollgroup.com>"
	tmessage=""
	tcc = ""
	lDoBOLGEN = .F.
	lSamples = .F.
	cBOL1 = ""
	lISAFlag = .T.
	lSTFlag = .T.
	nLoadid = 1
	lSQLMail = .F.
	lOverflow = .f.
	
	IF VARTYPE(cBOL) = "L"
		IF !lTesting AND !lTestinput
			WAIT WINDOW "BOL# not provided...terminating" TIMEOUT 2
			lCloseOutput = .F.
			cErrMsg = "NO BOL"
			THROW
		ELSE
			CLOSE DATABASES ALL
			IF !USED('edi_trigger')
				cEDIFolder = "F:\3PL\DATA\"
				USE (cEDIFolder+"edi_trigger") IN 0 ALIAS edi_trigger
			ENDIF
*!* TEST DATA AREA
			cBOL = "1ZX133V00255010017" && thru 1ZX133V00255010017
			cTime = DATETIME()
		ENDIF
	ENDIF
	CREATE CURSOR temp945 (accountid i,isa_num c(10),st_num c(10),wo_num i,bol_no c(20),ship_ref c(20),filename c(50),filedate T)
	cBOL=TRIM(cBOL)

	IF USED('OUTSHIP')
		USE IN outship
	ENDIF

	csq1 = [select * from outship where accountid = ]+TRANSFORM(nAcctNum)+[ and office = ']+cOffice+[' and bol_no = ']+cBOL+[']
	xsqlexec(csq1,,,"wh")
	IF RECCOUNT() = 0
		cErrMsg = "MISS BOL "+cBOL
		DO ediupdate WITH cErrMsg,.T.
		THROW
	ENDIF
	INDEX ON bol_no TAG bol_no

	IF !SEEK(cBOL,'outship','bol_no')
		cErrMsg = "BOL not found in outship"
		THROW
	ENDIF
	nWO_Num = outship.wo_num
	cSCAC = ALLTRIM(outship.scac)
	cKeyrec = ALLTRIM(outship.keyrec)
	
	SELECT outship
	lScanpack = .f.
	lNotScanpack = .f.
	SCAN
	alength = ALINES(apt,outship.shipins,.T.,CHR(13))
	cPackType = ALLTRIM(segmentget(@apt,"PROCESSMODE",alength))
	IF cPackType = "SCANPACK"
	lScanPack = .t.
	ELSE
	lNotScanpack = .t.
	endif
	ENDSCAN
	IF lScanpack AND lNotScanpack  && Mixed order pack types on the same BOL
		cErrMsg = "Mixed Pack Types on BOL"
		THROW
	ENDIF

	IF USED('parcel_carriers')
		USE IN parcel_carriers
	ENDIF

	USE F:\3pl\DATA\parcel_carriers IN 0 ALIAS parcel_carriers

	lParcelType = IIF(SEEK(cSCAC,'parcel_carriers','scac'),.T.,.F.)
	lParcelType = IIF(cBOL = cKeyrec,.F.,lParcelType)

	IF lTesting
*		lParcelType = .T.
	ENDIF

	STORE "LB" TO cWeightUnit
	cfd = "*"
	csegd = "~"
	nSegCtr = 0
	cterminator = ">" && Used at end of ISA segment

	WAIT WINDOW "Now preparing tables and variables" NOWAIT NOCLEAR

*!* SET CUSTOMER CONSTANTS
	cCustname = "2XU"  && Customer Identifier
	cX12 = "004010"  && X12 Standards Set used
	SELECT 0
	USE F:\3pl\DATA\mailmaster ALIAS mm
	LOCATE FOR mm.edi_type = "945" AND mm.office = "Y" AND mm.accountid = nAcctNum
	IF !FOUND()
		WAIT WINDOW "No entry in mailmaster........." TIMEOUT 2
		cErrMsg = "No entry in mailmaster"
		lIsError = .F.
		THROW
	ENDIF
	tsendto = IIF(mm.use_alt,sendtoalt,sendto)
	tcc = IIF(mm.use_alt,ccalt,cc)
	IF lTesting
		cOutPath = ALLTRIM(mm.basepath)
		cHoldPath = ALLTRIM(mm.holdpath)
		cArchivePath = ALLTRIM(mm.archpath)
	ELSE
		cOutPath = ALLTRIM(mm.basepath)
		cHoldPath = ALLTRIM(mm.holdpath)
		cArchivePath = ALLTRIM(mm.archpath)
	ENDIF
	LOCATE
	LOCATE FOR edi_type = "MISC" AND taskname = "GENERAL"
	tsendtoerr = IIF(mm.use_alt,sendtoalt,sendto)
	tccerr = IIF(mm.use_alt,ccalt,cc)
	tsendtotest = IIF(mm.use_alt,sendtoalt,sendto)
	tcctest = IIF(mm.use_alt,ccalt,cc)

	USE IN mm

*!* SET OTHER CONSTANTS
	DO CASE
		CASE cOffice = "L"
			cCustLoc =  "CA"
			cCustPrefix = "945c"
			cDivision = "California"
			cSF_Addr1  = "3355 DULLES DR"
			cSF_CSZ    = "MIRA LOMA"+cfd+"CA"+cfd+"91752"

		CASE cOffice = "M"
			cCustLoc =  "FL"
			cCustPrefix = "945f"
			cDivision = "Florida"
			cSF_Addr1  = "8815 NW 33RD STREET SUITE 110"
			cSF_CSZ    = "DORAL"+cfd+"FL"+cfd+"33172"

		CASE cOffice = "C"
			cCustLoc =  "CA"
			cCustPrefix = "945c"
			cDivision = "California"
			cSF_Addr1  = "400 WESTMONT DRIVE"
			cSF_CSZ    = "SAN PEDRO"+cfd+"CA"+cfd+"90731"

		CASE cOffice = "Y"
			cCustLoc =  "CA"
			cCustPrefix = "945c"
			cDivision = "California"
			cSF_Addr1  = "1000 E 223RD ST"
			cSF_CSZ    = "CARSON"+cfd+"CA"+cfd+"90745"

		CASE cOffice = "I"
			cCustLoc =  "NJ"
			cCustPrefix = "945j"
			cDivision = "New Jersey"
			cSF_Addr1  = "800 FEDERAL BLVD"
			cSF_CSZ    =  "CARTERET"+cfd+"NJ"+cfd+"07008"

		OTHERWISE
			cCustLoc =  "KY"
			cCustPrefix = "945s"
			cDivision = "Kentucky"
			cSF_Addr1  = "7501 WINSTEAD DR"
			cSF_CSZ    =  "LOUISVILLE"+cfd+"KY"+cfd+"40258"
	ENDCASE
	IF lTesting
		cCustPrefix = "945t"
	ENDIF

	cCustFolder = UPPER(cCustname)

	STORE "" TO lcKey
	STORE 0 TO alength,nLength
	cTime = DATETIME()
	cDelTime = SUBSTR(TTOC(cTime,1),9,4)
	cCarrierType = "M" && Default as "Motor Freight"
	dapptnum = ""

*!* SET OTHER CONSTANTS
	IF INLIST(cOffice,'C','1','2')
		dDateTimeCal = (DATETIME()-(3*3600))
	ELSE
		dDateTimeCal = DATETIME()
	ENDIF
	dt1 = TTOC(dDateTimeCal,1)
	dtmail = TTOC(dDateTimeCal)
	dt2 = dDateTimeCal
	cString = ""

	csendqual = "ZZ"
	csendid = "TGFPROD"
	crecqual = "XU"
	crecid = "2XU"

	cdate = DTOS(DATE())
	cTruncDate = RIGHT(cdate,6)
	cTruncTime = SUBSTR(TTOC(dDateTimeCal,1),9,4)
	cfiledate = cdate+cTruncTime
	cOrig = "J"
	nSTCount = 0
	cStyle = ""
	cPTString = ""
	nTotCtnWt = 0
	nTotCtnCount = 0
	nUnitSum = 0
	cTargetStyle = ""

** PAD ID Codes
	csendidlong = PADR(csendid,15," ")
	crecidlong = PADR(crecid,15," ")

	lcPath = ALLTRIM(cOutPath)
	cFilenameHold = (ALLTRIM(cHoldPath)+cCustPrefix+dt1+".txt")
	cFilenameShort = JUSTFNAME(cFilenameHold)
	cFilenameOut = IIF(lTesting,('F:\FTPUSERS\2XU\945-Test\'+cFilenameShort),(ALLTRIM(cOutPath)+cFilenameShort))
	cFilenameArch = (cArchivePath+cFilenameShort)
	nFilenum = FCREATE(cFilenameHold)

	SELECT outship
	IF !SEEK(cBOL,'outship','bol_no')
		ASSERT .F. MESSAGE "at missing BOL point"
		WAIT WINDOW "BOL not found in OUTSHIP" TIMEOUT 2
		cErrMsg = "BOL NOT FOUND-OUTSHIP"
		THROW
	ENDIF

	DO m:\dev\prg\swc_cutctns WITH cBOL

&&&added below 1/27/17 TMARG duplicate BOL's in outship
	SELECT outshipid ;
		FROM outship ;
		WHERE bol_no = cBOL ;
		AND accountid = nAcctNum ;
		AND del_date> IIF(lSetDelDate,DATE()-10,DATE()-nDaysBack) ;
		INTO CURSOR tempsr
	SELECT tempsr
	WAIT WINDOW "There are "+ALLTRIM(STR(RECCOUNT()))+" outshipid's to check" NOWAIT

	IF USED('OUTDET')
		USE IN outdet
	ENDIF
	
	**DHW**
*	SET STEP on

	selectoutdet()
	SELECT outdet
	INDEX ON outdetid TAG outdetid
	
	IF !USED("scacs")
		xsqlexec("select * from scac","scacs",,"wh")
		INDEX ON scac TAG scac
	ENDIF

	IF lParcelType
		IF USED('SHIPMENT')
			USE IN shipment
		ENDIF

		IF USED('PACKAGE')
			USE IN package
		ENDIF
	
*!*			dDate = DATE()-15
*!*			csq1 = [select * from shipment where accountid =]+TRANSFORM(nAcctNum)+[ and shipdate >= {]+DTOC(dDate)+[}]
*!*			xsqlexec(csq1,,,"wh")
		
		xsqlexec("select * from shipment where 1=0",,,"wh")  && Sets up an empty cursor
		SELECT outship && temp cursor with all PTs on the BOL
		SCAN
			cSR = ALLTRIM(outship.ship_ref)
			xsqlexec("select * from shipment where right(rtrim(str(accountid)),4) ="+TRANSFORM(nAcctNum)+" and pickticket = '"+cSR+"'","sm1",,"wh")
			SELECT sm1
			LOCATE
			IF RECCOUNT()=0
				cErrMsg = "NO SHIPMENT DATA"
				THROW
			ENDIF

			SELECT shipment
			APPEND FROM DBF('sm1')
			USE IN sm1
		ENDSCAN
		RELEASE cSR
		
		SELECT shipment
		INDEX ON shipmentid TAG shipmentid
		INDEX ON pickticket TAG pickticket
		LOCATE

		IF RECCOUNT("shipment") > 0
			xjfilter="shipmentid in ("
			SCAN
				nShipmentId = shipment.shipmentid
				xsqlexec("select * from package where shipmentid="+TRANSFORM(nShipmentId),,,"wh")

				IF RECCOUNT("package")>0
					xjfilter=xjfilter+TRANSFORM(shipmentid)+","
				ELSE
*					xjfilter="1=0"
				ENDIF
			ENDSCAN
			xjfilter=LEFT(xjfilter,LEN(xjfilter)-1)+")"

			xsqlexec("select * from package where "+xjfilter,,,"wh")
		ELSE
			xsqlexec("select * from package where .f.",,,"wh")
		ENDIF

		INDEX ON shipmentid TAG shipmentid
		INDEX ON ucc TAG ucc
		SET ORDER TO TAG shipmentid

		SELECT shipment
		LOCATE
		SET RELATION TO shipmentid INTO package
	ENDIF

&& Added code blocks to the following to overcome BOL vs. WO selection issues, 12.23.2005

	IF lDoSQL
		csql="tgfnjsql01"
		SELECT outship
		LOCATE
		IF USED("sqlwo2xu")
			USE IN sqlwo2xu
		ENDIF
		IF FILE("F:\3pl\DATA\sqlwo2xu.dbf")
			DELETE FILE "F:\3pl\DATA\sqlwo2xu.dbf"
		ENDIF
&&&del_date code below added 1/27/17 TMARG duplicate BOL's in outship

		SELECT bol_no,wo_num ;
			FROM outship ;
			WHERE bol_no = cBOL ;
			AND accountid = nAcctNum ;
			AND del_date> IIF(lSetDelDate,DATE()-10,DATE()-nDaysBack) ;
			GROUP BY 1,2 ;
			ORDER BY 1,2 ;
			INTO DBF F:\3pl\DATA\sqlwo2xu

		USE IN sqlwo2xu
		SELECT 0
		USE F:\3pl\DATA\sqlwo2xu ALIAS sqlwo2xu
		LOCATE
		IF EOF()
			cErrMsg = "NO SQL DATA"
			THROW
		ENDIF
		cRetMsg = ""
		SELECT outship	
		LOCATE FOR bol_no=  sqlwo2xu.bol_no AND wo_num= sqlwo2xu.wo_num
		IF FOUND("outship")
			lDoScanpack = IIF("PROCESSMODE*SCANPACK"$outship.shipins,.t.,.f.)
			lDoScanpack = .t.
			DO m:\dev\prg\sqlconnect2xu_bol  WITH nAcctNum,cCustname,cPPName,.T.,cOffice,.T.,,,lDoScanpack
			IF cRetMsg<>"OK"
				STORE cRetMsg TO cErrMsg
				THROW
			ENDIF
			SELECT v2xupp
			LOCATE
			IF EOF()
				WAIT WINDOW "SQL select data is EMPTY...error!" NOWAIT
*				SET STEP ON
				cErrMsg = "SQL DATA EMPTY"
				THROW
			ENDIF
		ELSE
			WAIT WINDOW "SQL select data is EMPTY...error!" NOWAIT
*			SET STEP ON
			cErrMsg = "SQL DATA EMPTY"
			THROW
		ENDIF
	ENDIF

	SELECT outship
	LOCATE
	WAIT CLEAR
	WAIT WINDOW "Now creating Header information" NOWAIT NOCLEAR

*!* HEADER LEVEL EDI DATA
	DO num_incr_isa

	cISA_Num = PADL(c_CntrlNum,9,"0")
	nISA_Num = INT(VAL(cISA_Num))

	cISACode = IIF(lTesting,"T","P")

	STORE "ISA"+cfd+"00"+cfd+"          "+cfd+"00"+cfd+"          "+cfd+csendqual+cfd+csendidlong+cfd+;
		crecqual+cfd+crecidlong+cfd+cTruncDate+cfd+cTruncTime+cfd+"U"+cfd+"00401"+cfd+;
		cISA_Num+cfd+"0"+cfd+cISACode+cfd+cterminator+csegd TO cString
	DO cstringbreak

	STORE "GS"+cfd+"SW"+cfd+csendid+cfd+crecid+cfd+cdate+cfd+cTruncTime+cfd+c_CntrlNum+;
		cfd+"X"+cfd+cX12+csegd TO cString
	DO cstringbreak

	SELECT outship
	LOCATE

*************************************************************************
*1  PTIDSOURCE/OUTSHIP RECORD MAIN LOOP
*  Loops through individual OUTSHIP lines for BOL#
*************************************************************************
	WAIT CLEAR
	WAIT WINDOW "Now creating BOL#-based information..." NOWAIT NOCLEAR

	SELECT outship
	SET ORDER TO
	COUNT TO N FOR outship.bol_no = cBOL AND !EMPTYnul(del_date)
	IF N=0
		cErrMsg = "INCOMP BOL"
		THROW
	ENDIF

	LOCATE
	cMissDel = ""
	nPTCount = 0
***changed 1/27/17 TMARG to fix dupe BOL's
*	oscanstr = "outship.bol_no = cBOL AND !EMPTYnul(del_date)"
	oscanstr = "outship.bol_no = cBOL AND outship.accountid = nAcctNum and del_date> IIF(lSetDelDate,Date()-10,DATE()-nDaysBack)"
	SELECT outship
	LOCATE FOR &oscanstr
	SCAN FOR &oscanstr
		nWO_Num = outship.wo_num
		cWO_Num = ALLTRIM(STR(nWO_Num))
		alength = ALINES(apt,outship.shipins,.T.,CHR(13))
		SCATTER MEMVAR MEMO
		cShip_ref = ALLTRIM(m.ship_ref)
		IF "OV"$cShip_ref
			LOOP
		ENDIF
		nPTCount = nPTCount + 1

		lJCP = IIF("PENNEY"$UPPER(m.consignee) OR UPPER(m.consignee)="JCP",.T.,.F.)
		lAmazon  = IIF("AMAZON"$UPPER(m.consignee) OR UPPER(m.consignee)="AMAZON",.T.,.F.)
		lZappo = IIF("ZAPPO"$UPPER(m.consignee) OR UPPER(m.consignee)="ZAPPO",.T.,.F.)
		lDicks  = IIF("DICK"$UPPER(m.consignee) OR UPPER(m.consignee)="DICK",.T.,.F.)
		nOutshipid = m.outshipid
		nWO_Num = m.wo_num
		cWO_Num = ALLTRIM(STR(m.wo_num))
		cPackType = ALLTRIM(segmentget(@apt,"PROCESSMODE",alength))

		lPrepack = IIF(cPackType#"PICKPACK",.T.,.F.)

		lPrepack = .F.

		IF !(cWO_Num$cWO_NumList)
			cWO_NumList = IIF(EMPTY(cWO_NumList),cWO_Num,cWO_NumList+CHR(13)+cWO_Num)
		ENDIF
		IF !lParcelType
			cTrackNum = ALLT(m.keyrec)  && ProNum if available
			cCharge = "0.00"
			IF !(cWO_Num$cWO_NumStr)
				cWO_NumStr = IIF(EMPTY(cWO_NumStr),cWO_Num,cWO_NumStr+","+cWO_Num)
			ENDIF
		ELSE
*      cBOL = PADL(ALLT(STR(outship.wo_num)),17,'0')
			cTrackNum = ALLTRIM(segmentget(@apt,"LONGTRKNUM",alength))
			IF EMPTY(cTrackNum)
				cTrackNum = ALLT(outship.bol_no)  && UPS Tracking Number
			ENDIF
			cWO_NumStr = IIF(EMPTY(cWO_NumStr),cTrackNum,cWO_NumStr+","+cTrackNum)

			IF !lTestinput
				WAIT WINDOW "At UPS package charges..." NOWAIT
				SELECT shipment
				LOCATE FOR INT(VAL(RIGHT(STR(shipment.accountid),4))) = nAcctNum AND shipment.pickticket = PADR(cShip_ref,20)
				IF !FOUND()
					LOCATE FOR shipment.accountid = 9999 AND shipment.pickticket = PADR(cShip_ref,20)
					IF !FOUND()
						cErrMsg = "MISS UPS REC "+cShip_ref
						THROW
					ELSE
						SELECT package
						LOCATE
						SUM pkgcharge TO nCharge FOR package.shipmentid = shipment.shipmentid
						cCharge = ALLT(STR(nCharge,8,2))
					ENDIF
				ELSE
					SELECT package
					LOCATE
					SUM pkgcharge TO nCharge FOR package.shipmentid = shipment.shipmentid
					cCharge = ALLT(STR(nCharge,8,2))
				ENDIF
			ELSE
				cCharge = "25.50"
			ENDIF
		ENDIF

		nTotCtnCount = m.qty
		cPO_Num = ALLTRIM(m.cnee_ref)
		cShip_ref = ALLTRIM(m.ship_ref)

*!* Added this code to trap miscounts in OUTDET Units
*    ASSERT .F. MESSAGE "In outdet/SQL tot check"
		IF lPrepack
			SELECT outdet
			SET ORDER TO
			SUM totqty TO nCtnTot1 FOR !units AND outdet.outshipid = outship.outshipid

*!*  Check carton count
			SELECT COUNT(ucc) AS cnt1 ;
				FROM v2xupp ;
				WHERE v2xupp.outshipid = outship.outshipid ;
				AND v2xupp.ucc#"CUTS" ;
				AND v2xupp.totqty > 0 ;
				INTO CURSOR tempsqlx
			STORE tempsqlx.cnt1 TO nCtnTot2
			USE IN tempsqlx
			IF nCtnTot1<>nCtnTot2
				ASSERT .F. MESSAGE "At CTN QTY error"
				SET STEP ON
				cErrMsg = "SQL CTNQTY ERR- O/D QTY: "+TRANSFORM(nCtnTot1)+", SQL QTY: "+TRANSFORM(nCtnTot2)
				THROW
			ENDIF
		ELSE
			SELECT outdet
			nODRec = RECNO()
			SUM totqty TO nUnitTot1 FOR outdet.units=.T. AND outdet.outshipid = outship.outshipid
			SELECT outdet
			IF !EOF()
				GO nODRec
			ELSE
				GO BOTT
			ENDIF
			SELECT v2xupp
			nVRec = RECNO()
			SUM v2xupp.totqty TO nUnitTot2 FOR v2xupp.outshipid = outship.outshipid AND v2xupp.accountid = nAcctNum AND v2xupp.ucc # "CUTS"
			IF !EOF()
				GO nVRec
			ELSE
				GO BOTT
			ENDIF
			IF nUnitTot1<>nUnitTot2
				ASSERT .F. MESSAGE "IN TOTQTY COMPARE"
				SET STEP ON
				cErrMsg = "SQL UNITQTY ERR- O/D QTY: "+TRANSFORM(nUnitTot1)+", SQL QTY: "+TRANSFORM(nUnitTot2)
				THROW
			ENDIF
		ENDIF

		SELECT outdet
		SET ORDER TO outdetid
		LOCATE

*!* End code addition

		lApptFlag = IIF(lAmazon,.T.,.F.)

		IF lTestinput AND EMPTY(outship.appt_num)
			ddel_date = DATE()
			dapptnum = "99999"
			dapptdate = DATE()
		ELSE
			ddel_date = outship.del_date
			IF EMPTY(ddel_date)
				cMissDel = IIF(EMPTY(cMissDel),"The following PTs had no Delivery Dates:"+CHR(10)+TRIM(cShip_ref),cMissDel+CHR(10)+TRIM(cShip_ref))
			ENDIF
			dapptnum = ALLTRIM(outship.appt_num)

			IF EMPTY(dapptnum) AND (lAmazon OR lZappo OR lDicks)
				IF !(lApptFlag)
					dapptnum = ""
				ELSE
					cErrMsg = "EMPTY APPT #"
					THROW
				ENDIF
			ENDIF
			dapptdate = outship.appt

			IF EMPTY(dapptdate)
				dapptdate = outship.del_date
			ENDIF
		ENDIF

		IF !(cWO_Num$cWO_NumStr)
			cWO_NumStr = IIF(EMPTY(cWO_NumStr),cWO_Num,cWO_NumStr+","+cWO_Num)
		ENDIF

		IF ALLTRIM(outship.SForCSZ) = ","
			BLANK FIELDS outship.SForCSZ NEXT 1 IN outship
		ENDIF

		cTRNum = ""
		cPRONum = ""

		IF (("WALMART"$outship.consignee) OR ("WAL-MART"$outship.consignee) OR ("WAL MART"$outship.consignee))
			IF LEFT(outship.keyrec,2) = "PR"
				STORE TRIM(keyrec) TO cPRONum
			ENDIF
			IF LEFT(outship.keyrec,2) = "TR"
				STORE TRIM(keyrec) TO cTRNum
			ENDIF
		ENDIF

		cPTString = IIF(EMPTY(cPTString),m.consignee+" "+cShip_ref,cPTString+CHR(13)+CHR(10)+m.consignee+" "+cShip_ref)

		m.CSZ = TRIM(m.CSZ)
		IF EMPTY(ALLT(STRTRAN(M.CSZ,",","")))
			WAIT CLEAR
			WAIT WINDOW "No SHIP-TO City/State/ZIP info...exiting" TIMEOUT 2
			cErrMsg = "NO CSZ INFO"
			THROW
		ENDIF
		IF !(", "$m.CSZ)
			m.CSZ = ALLTRIM(STRTRAN(m.CSZ,",",", "))
		ENDIF
		m.CSZ = ALLTRIM(STRTRAN(m.CSZ,"  "," "))
		cCity = ALLT(LEFT(TRIM(m.CSZ),AT(",",m.CSZ)-1))
		cStateZip = ALLT(SUBSTR(TRIM(m.CSZ),AT(",",m.CSZ)+1))
		cState = ALLT(LEFT(cStateZip,2))
		cZip = ALLT(SUBSTR(cStateZip,3))

		STORE "" TO cSForCity,cSForState,cSForZip
		m.SForCSZ = ALLTRIM(STRTRAN(m.SForCSZ,"  "," "))
		nSpaces = OCCURS(" ",ALLTRIM(m.SForCSZ))
		IF nSpaces = 0
			m.SForCSZ = STRTRAN(m.SForCSZ,",","")
			cSForCity = ALLTRIM(m.SForCSZ)
			cSForState = ""
			cSForZip = ""
		ELSE
			nCommaPos = AT(",",m.SForCSZ)
			nLastSpace = AT(" ",m.SForCSZ,nSpaces)
			nMinusSpaces = IIF(nSpaces=1,0,1)
			IF ISALPHA(SUBSTR(TRIM(m.SForCSZ),AT(" ",TRIM(m.SForCSZ),nSpaces-nMinusSpaces)+1,2))
				cSForCity = LEFT(TRIM(m.SForCSZ),AT(",",m.SForCSZ)-1)
				cSForState = SUBSTR(m.SForCSZ,nCommaPos+2,2)
				cSForZip = TRIM(RIGHT(TRIM(m.SForCSZ),5))
				IF ISALPHA(cSForZip)
					cSForZip = ""
				ENDIF
			ELSE
				WAIT CLEAR
				WAIT WINDOW "NOT ALPHA: "+SUBSTR(TRIM(m.SForCSZ),AT(" ",m.SForCSZ,nSpaces-1)+1,2) TIMEOUT 3
				cSForCity = LEFT(TRIM(m.SForCSZ),AT(",",m.SForCSZ)-1)
				cSForState = SUBSTR(TRIM(m.SForCSZ),AT(" ",m.SForCSZ,nSpaces-2)+1,2)
				cSForZip = TRIM(RIGHT(TRIM(m.SForCSZ),5))
				IF ISALPHA(cSForZip)
					cSForZip = ""
				ENDIF
			ENDIF
		ENDIF

		DO num_incr_st
		WAIT CLEAR
		WAIT WINDOW "Now creating Line Item information" NOWAIT NOCLEAR

		INSERT INTO temp945 (accountid,isa_num,st_num,wo_num,bol_no,ship_ref,filename,filedate) ;
			VALUES (m.accountid,c_CntrlNum,c_GrpCntrlNum,m.wo_num,m.bol_no,m.ship_ref,cFilenameShort,dt2)

		STORE "ST"+cfd+"945"+cfd+PADL(c_GrpCntrlNum,9,"0")+csegd TO cString
		DO cstringbreak
		nSTCount = nSTCount + 1
		nSegCtr = 1

		cBOLUse = ""
		IF lParcelType
*			cBOLUse = ALLTRIM(segmentget(@apt,"LONGTRKNUM",alength))
		ENDIF

*!*			IF EMPTY(cBOLUse)
		cBOLUse = TRIM(cBOL)
*!*			ENDIF


		STORE "W06"+cfd+"N"+cfd+cShip_ref+cfd+cdate+cfd+TRIM(cBOLUse)+cfd+TRIM(cWO_Num)+cfd+cPO_Num+csegd TO cString
		DO cstringbreak
		nSegCtr = nSegCtr + 1

		nCtnNumber = 1  && Seed carton sequence count

*    ASSERT .F. MESSAGE "At Ship-to store...debug"
		STORE "N1"+cfd+"ST"+cfd+ALLTRIM(m.consignee)+csegd TO cString  && default this to just the consignee

		cStoreNum = segmentget(@apt,"GLN",alength)
		IF EMPTY(cStoreNum)
			cStoreNum = segmentget(@apt,"STORENUM",alength)
		ENDIF

		IF !EMPTY(cStoreNum)
			STORE "N1"+cfd+"ST"+cfd+ALLTRIM(m.consignee)+cfd+"92"+cfd+cStoreNum+csegd TO cString
		ENDIF
		DO cstringbreak
		nSegCtr = nSegCtr + 1

*!*      cCountry = segmentget(@apt,"COUNTRY",alength)
*!*      cCountry = ALLT(cCountry)
*!*      IF EMPTY(cCountry)
*!*        cCountry = "USA"
*!*      ENDIF

		STORE "N3"+cfd+TRIM(outship.address)+csegd TO cString
		DO cstringbreak
		nSegCtr = nSegCtr + 1

		STORE "N4"+cfd+cCity+cfd+cState+cfd+cZip+csegd TO cString &&+cfd+cCountry
		DO cstringbreak
		nSegCtr = nSegCtr + 1

		cSFStoreNum = segmentget(@apt,"SFSTORENUM",alength)
		IF EMPTY(cSFStoreNum)
			cSFStoreNum = ALLTRIM(m.sforstore)
		ENDIF
		IF !EMPTY(m.shipfor)
			IF EMPTY(ALLTRIM(m.sforstore))
				STORE "N1"+cfd+"Z7"+cfd+ALLTRIM(m.shipfor)+csegd TO cString
			ELSE
				STORE "N1"+cfd+"Z7"+cfd+ALLTRIM(m.shipfor)+cfd+"92"+cfd+cSFStoreNum+csegd TO cString
			ENDIF
			DO cstringbreak
			nSegCtr = nSegCtr + 1

			STORE "N3"+cfd+TRIM(m.sforaddr1)+csegd TO cString
			DO cstringbreak
			nSegCtr = nSegCtr + 1

			IF !EMPTY(cSForState)
				STORE "N4"+cfd+cSForCity+cfd+cSForState+cfd+cSForZip+cfd+"USA"+csegd TO cString
			ELSE
				STORE "N4"+cfd+cSForCity+csegd TO cString
			ENDIF
			DO cstringbreak
			nSegCtr = nSegCtr + 1
		ENDIF

		IF !EMPTY(outship.keyrec)
			STORE "N9"+cfd+"P8"+cfd+ALLTRIM(outship.keyrec)+csegd TO cString
			DO cstringbreak
			nSegCtr = nSegCtr + 1
		ENDIF

		IF !EMPTY(outship.appt_num)
			STORE "N9"+cfd+"LO"+cfd+ALLTRIM(outship.appt_num)+csegd TO cString
			DO cstringbreak
			nSegCtr = nSegCtr + 1
		ENDIF

		STORE "G62"+cfd+"11"+cfd+TRIM(DTOS(ddel_date))+cfd+"D"+cfd+cDelTime+csegd TO cString  && Ship date/time
		DO cstringbreak
		nSegCtr = nSegCtr + 1

		SELECT scacs
		IF lTesting AND (EMPTY(m.scac) OR EMPTY(m.ship_via))
			STORE "TEST" TO m.scac
			STORE "TEST SHIPPER" TO m.ship_via
		ELSE
			IF !EMPTY(TRIM(outship.scac))
				STORE ALLTRIM(outship.scac) TO m.scac
				lFedEx = .F.
				SELECT scacs
				IF SEEK(m.scac,"scacs","scac")
					IF ("FEDERAL EXPRESS"$NAME) OR ("FEDEX"$NAME)
						lFedEx = .T.
						IF m.scac = "FGC"
							m.scac = "FDEG"
						ENDIF
					ENDIF
					SELECT outship

					IF lParcelType OR lFedEx
						cCarrierType = "U" && Parcel as UPS or FedEx
					ENDIF
				ELSE
*        WAIT CLEAR
*        cErrMsg = "MISSING SCAC"
*        THROW
				ENDIF
			ENDIF
		ENDIF

		SELECT outship

		IF !EMPTY(cTRNum)
			STORE "W27"+cfd+cCarrierType+cfd+TRIM(m.scac)+cfd+TRIM(m.ship_via)+REPLICATE(cfd,2)+"TL"+;
				REPLICATE(cfd,2)+cTRNum+csegd TO cString
		ELSE
			STORE "W27"+cfd+cCarrierType+cfd+TRIM(m.scac)+cfd+TRIM(m.ship_via)+csegd TO cString
		ENDIF
		DO cstringbreak
		nSegCtr = nSegCtr + 1

		IF lParcelType
			STORE "G72"+cfd+"516"+cfd+"15"+REPLICATE(cfd,6)+ALLT(cCharge)+csegd TO cString
			DO cstringbreak
			nSegCtr = nSegCtr + 1
		ENDIF

*************************************************************************
*2  DETAIL LOOP - MATCHING SHIP_REF FIELD (FROM OUTSHIP)
*************************************************************************
		WAIT CLEAR
		WAIT WINDOW "Now creating Detail information "+cShip_ref NOWAIT NOCLEAR
		SELECT v2xupp

		SELECT outdet
		SET ORDER TO TAG outdetid
		SELECT v2xupp
		SET RELATION TO outdetid INTO outdet

		SELECT v2xupp
		LOCATE
*!*      BROWSE FIELDS wo_num,ship_ref,outshipid
		LOCATE FOR v2xupp.ship_ref = TRIM(cShip_ref) AND v2xupp.outshipid = nOutshipid
		IF !FOUND()
			IF !lTesting
				WAIT WINDOW "PT "+cShip_ref+" NOT FOUND in v2xupp...ABORTING" TIMEOUT 2
				IF !lTesting
					lSQLMail = .T.
				ENDIF
				cErrMsg = "MISS PT-SQL: "+cShip_ref
				THROW
			ELSE
				LOOP
			ENDIF
		ENDIF

		vscanstr = "v2xupp.ship_ref = cShip_ref and v2xupp.outshipid = nOutshipid"

		SELECT v2xupp
		LOCATE
		LOCATE FOR &vscanstr
		cUCC= "XXX"
		DO WHILE &vscanstr
			lSkipBack = .T.
			IF ALLTRIM(v2xupp.ucc) = "CUTS"
				SKIP 1 IN v2xupp
				LOOP
			ENDIF

			IF TRIM(v2xupp.ucc) <> cUCC
				STORE TRIM(v2xupp.ucc) TO cUCC
			ENDIF

			STORE "LX"+cfd+ALLTRIM(STR(nCtnNumber))+csegd TO cString   && Carton Seq. # (up to Carton total)
			lDoManSegment = .T.
			lDoPALSegment = .F.
			DO cstringbreak
			nSegCtr = nSegCtr + 1

			DO WHILE v2xupp.ucc = cUCC
				cDesc = ""
				SELECT outdet
				alength = ALINES(aptdet,outdet.printstuff,.T.,CHR(13))
				lPrepack945 = .F.
				cUCCNumber = v2xupp.ucc
				cUCCNumber = ALLTRIM(STRTRAN(TRIM(cUCCNumber)," ",""))

				IF lDoManSegment
					lDoManSegment = .F.
					nPTCtnTot =  nPTCtnTot + 1
					nCtnWeight = 0
					STORE "MAN"+cfd+"GM"+cfd+TRIM(cUCCNumber)+csegd TO cString  && SCC-14 Number (Wal-mart spec)
					DO cstringbreak
					nSegCtr = nSegCtr + 1

					IF lParcelType
						lnCtnWeight = 0
						SELECT shipment
						LOCATE FOR shipment.accountid = nAcctNum AND shipment.pickticket = PADR(cShip_ref,20)
						IF !FOUND()
							cErrMsg = "MISS UPS REC"+cShip_ref
							THROW
						ELSE
							SELECT package
							LOCATE FOR package.ucc = cUCC
							lnCtnWeight = package.pkgweight
						ENDIF

						SELECT shipment
						STORE "PAL"+cfd+cfd+cfd+cfd+cfd+ALLTRIM(STR(lnCtnWeight,6,2))+cfd+"LB"+csegd TO cString  && carton weight
						DO cstringbreak
						nSegCtr = nSegCtr + 1
					ELSE
						nShipDetQty = INT(VAL(outdet.PACK))
						nUnitSum = nUnitSum + nShipDetQty

						IF (EMPTY(outdet.ctnwt) OR outdet.ctnwt = 0) AND outdet.totqty > 0
							cCtnWt = ALLTRIM(STR(INT(CEILING(outship.weight/outship.ctnqty))))
							IF lTesting
								cCtnWt = "3"
							ELSE
								IF (EMPTY(cCtnWt) OR INT(VAL(cCtnWt)) = 0)
									cErrMsg = "WGT ERR, PT: "+cShip_ref
									THROW
								ENDIF
							ENDIF
							nTotCtnWt = nTotCtnWt + INT(VAL(cCtnWt))
						ELSE
							STORE ALLTRIM(STR(outdet.ctnwt)) TO cCtnWt
							nTotCtnWt = nTotCtnWt + outdet.ctnwt
						ENDIF
						nCtnWt = VAL(cCtnWt)
						STORE "PAL"+cfd+cfd+cfd+cfd+cfd+ALLTRIM(STR(nCtnWt,6,2))+cfd+"LB"+csegd TO cString  && carton weight
						DO cstringbreak
						nSegCtr = nSegCtr + 1
					ENDIF
				ENDIF

				IF lParcelType
					SELECT package
					=SEEK(PADR(TRIM(cUCCNumber),20),"package","ucc")
					IF FOUND()
						STORE "N9"+cfd+"TR"+cfd+ALLTRIM(package.trknumber)+csegd TO cString
					ELSE
						STORE "N9"+cfd+"TR"+cfd+"UNK"+csegd TO cString
					ENDIF
				ELSE
					STORE "N9"+cfd+"TR"+cfd+ALLTRIM(outship.keyrec)+csegd TO cString
				ENDIF

				DO cstringbreak
				nSegCtr = nSegCtr + 1
				SELECT v2xupp

				cStyle = TRIM(outdet.STYLE)
				IF EMPTY(cStyle)
					ASSERT .F. MESSAGE "EMPTY STYLE...DEBUG"
					WAIT WINDOW "Empty style in "+cShip_ref TIMEOUT 1
				ENDIF

				lDoUPC = .T.
*        ASSERT .F. MESSAGE "At UPC extract...debug"
				cUPC = ""
				cUPC = ALLTRIM(outdet.upc)
				IF (ISNULL(cUPC) OR EMPTY(cUPC) OR VAL(cUPC) = 0)
					cUPC = TRIM(v2xupp.upc)
				ENDIF
				cItemNum = TRIM(outdet.custsku)
				nShipDetQty = v2xupp.totqty
				IF ISNULL(nShipDetQty) OR  nShipDetQty = 0 && OR EMPTY(nShipDetQty)
					SET STEP ON
					nShipDetQty = outdet.totqty
				ENDIF

				nOrigDetQty = v2xupp.qty
				IF ISNULL(nOrigDetQty) OR nOrigDetQty = 0
					SET STEP ON
					nOrigDetQty = outdet.origqty
				ENDIF

*!* Changed the following to utilize original 940 unit codes from Printstuff field
				cUnitCode = TRIM(segmentget(@aptdet,"UNITCODE",alength))
				IF EMPTY(cUnitCode)
					cUnitCode = "EA"
				ENDIF

				IF nOrigDetQty = nShipDetQty
					nShipStat = "CC"
				ELSE
					nShipStat = "CP"
				ENDIF

				IF lDoUPC
					STORE "W12"+cfd+nShipStat+cfd+ALLTRIM(STR(nOrigDetQty))+cfd+;
						ALLTRIM(STR(nShipDetQty))+cfd+ALLTRIM(STR(nOrigDetQty-nShipDetQty))+cfd+;
						cUnitCode+cfd+cUPC+cfd+"IN"+cfd+ALLTRIM(outdet.STYLE)+IIF(!EMPTY(ALLTRIM(outdet.custsku)),REPLICATE(cfd,9)+"VN"+cfd+ALLTRIM(outdet.custsku)+csegd,csegd) TO cString
				ELSE
					STORE "W12"+cfd+nShipStat+cfd+ALLTRIM(STR(nOrigDetQty))+cfd+;
						ALLTRIM(STR(nShipDetQty))+cfd+ALLTRIM(STR(nOrigDetQty-nShipDetQty))+cfd+;
						cUnitCode+cfd+ALLTRIM(outdet.custsku)+csegd TO cString
				ENDIF
				DO cstringbreak
				nSegCtr = nSegCtr + 1

				STORE "N9"+cfd+"CL"+cfd+ALLTRIM(outdet.COLOR)+csegd TO cString
				DO cstringbreak
				nSegCtr = nSegCtr + 1

				STORE "N9"+cfd+"SZ"+cfd+ALLTRIM(outdet.ID)+csegd TO cString
				DO cstringbreak
				nSegCtr = nSegCtr + 1

*        ASSERT .f. MESSAGE "AT G69 LOOP"
				cDesc = ALLTRIM(segmentget(@aptdet,"DESC",alength))
				IF !EMPTY(cDesc)
					STORE "G69"+cfd+cDesc+csegd TO cString
					DO cstringbreak
					nSegCtr = nSegCtr + 1
				ENDIF

				SKIP 1 IN v2xupp
				SET DELETED ON
			ENDDO
			lSkipBack = .T.
			nCtnNumber = nCtnNumber + 1
*    nUnitSum = nUnitSum + 1
		ENDDO

*************************************************************************
*2^  END DETAIL MATCHING SHIP_REF LOOP
*************************************************************************
		WAIT CLEAR
		IF outship.cuft>0
			cCube = ALLTRIM(STR(outship.cuft,6,2))
		ELSE
			cCube = "0"
		ENDIF
		WAIT WINDOW "Now creating Section Closure information" NOWAIT NOCLEAR
		STORE "W03"+cfd+ALLTRIM(STR(nPTCtnTot))+cfd+ALLTRIM(STR(nTotCtnWt))+cfd+;
			cWeightUnit+cfd+cCube+cfd+"CF"+csegd TO cString   && Units sum, Weight sum, carton count

		nPTCtnTot = 0
		nTotCtnWt = 0
		nTotCtnCount = 0
		nUnitSum = 0
		FPUTS(nFilenum,cString)

		STORE  "SE"+cfd+ALLTRIM(STR(nSegCtr+2))+cfd+PADL(c_GrpCntrlNum,9,"0")+csegd TO cString
		FPUTS(nFilenum,cString)

		SELECT outship
		WAIT CLEAR
	ENDSCAN

*************************************************************************
*1^  END OUTSHIP MAIN LOOP
*************************************************************************

	DO close945
	=FCLOSE(nFilenum)

	IF !lTesting
		IF !USED("edi_trigger")
			USE F:\3pl\DATA\edi_trigger IN 0
		ENDIF
		SELECT edi_trigger
		DO ediupdate WITH "945 CREATED",.F.
		SELECT edi_trigger
		LOCATE

		IF !USED("ftpedilog")
			SELECT 0
			USE F:\edirouting\ftpedilog ALIAS ftpedilog
			INSERT INTO ftpedilog (TRANSFER,ftpdate,filename,acct_name,TYPE) VALUES ("945-"+cCustname+"-"+cCustLoc,dt2,cFilenameHold,UPPER(cCustname),"945")
			USE IN ftpedilog
		ENDIF
	ENDIF

	WAIT CLEAR
	WAIT WINDOW cCustFolder+" 945 Process complete..." NOWAIT

*!* Create eMail confirmation message
	IF lTestMail
		tsendto = tsendtotest
		tcc = tcctest
	ENDIF

	cOutFolder = "FMI"+cCustLoc

	tsubject = cMailName+" 945 EDI File from TGF as of "+dtmail+" ("+cCustLoc+")"
	tattach = " "
	tmessage = "945 EDI Info from TGF, for division "+cDivision+", BOL# "+TRIM(cBOL)+CHR(10)
	tmessage = tmessage + "For Work Orders: "+cWO_NumStr+","+CHR(10)
	tmessage = tmessage + "containing these "+ALLTRIM(STR(nPTCount))+" picktickets:"+CHR(10)+CHR(13)
	tmessage = tmessage + cPTString + CHR(10)+CHR(13)
	tmessage = tmessage +"has been created and will be transmitted ASAP."+CHR(10)+CHR(10)
	IF !EMPTY(cMissDel)
		tmessage = tmessage+CHR(10)+CHR(10)+cMissDel+CHR(10)+CHR(10)
	ENDIF
	IF lTesting OR lTestinput
		tmessage = tmessage + "This is a TEST 945"
	ELSE
		tmessage = tmessage + "If you have any questions, please eMail or call 732-750-9000 x217."
	ENDIF

	IF lEmail
		WAIT WINDOW "At do mail stage..." NOWAIT
		DO FORM m:\dev\frm\dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
	ENDIF

	cFilenameArchive = UPPER("f:\ftpusers\"+cCustFolder+"\945OUT\ARCHIVE\"+cCustPrefix+dt1+".txt")

	RELEASE ALL LIKE c_CntrlNum,c_GrpCntrlNum
	WAIT CLEAR
	WAIT WINDOW cMailName+" 945 EDI File output complete" TIMEOUT 1
	lDoCatch = .F.
	
	
*!* Transfers files to correct output folders
	COPY FILE [&cFilenameHold] TO [&cFilenameArch]

	IF lTesting
*		  SET STEP ON
	ELSE
		WAIT WINDOW "" TIMEOUT 1
	COPY FILE [&cFilenameHold] TO [&cFilenameOut]
	DELETE FILE [&cFilenameHold]
	ENDIF

	IF !lTesting
		SELECT temp945
		COPY TO "f:\3pl\data\temp945ss.dbf"
		USE IN temp945
		SELECT 0
		USE "f:\3pl\data\temp945ss.dbf" ALIAS temp945ss
		SELECT 0
		USE "f:\3pl\data\pts_sent945.dbf" ALIAS pts_sent945
		APPEND FROM "f:\3pl\data\temp945ss.dbf"
		USE IN pts_sent945
		USE IN temp945ss
		DELETE FILE "f:\3pl\data\temp945ss.dbf"
	ENDIF

	IF !lTesting
		asn_out_data()
	ENDIF


CATCH TO oErr
	IF lDoCatch
		WAIT WINDOW  "In Error CATCH" TIMEOUT 2
		SET STEP ON
		IF EMPTY(ALLTRIM(cErrMsg))
			cErrMsg = ALLTRIM(oErr.Message)
		ENDIF

		IF lTesting
			tsendto  = tsendtotest
			tcc = tcctest
		ELSE
			tsendto  = tsendtoerr
			tcc = tccerr
			tsubject = cMailName+" Error ("+TRANSFORM(oErr.ERRORNO)+") at "+TTOC(DATETIME())
			tattach  = ""
			DO ediupdate WITH cErrMsg,.T.
		ENDIF

		IF !lIsError
			tmessage = cCustname+" Error processing "+CHR(13)
			tmessage = tmessage+TRIM(PROGRAM())+CHR(13)

			tmessage =tmessage+ "Try/CATCH Exeception Message:"+CHR(13)+;
				[  Error: ] + STR(oErr.ERRORNO) +CHR(13)+;
				[  LineNo: ] + STR(oErr.LINENO) +CHR(13)+;
				[  Message: ] + oErr.MESSAGE +CHR(13)+;
				[  Procedure: ] + oErr.PROCEDURE +CHR(13)+;
				[  Details: ] + oErr.DETAILS +CHR(13)+;
				[  StackLevel: ] + STR(oErr.STACKLEVEL) +CHR(13)+;
				[  LineContents: ] + oErr.LINECONTENTS+CHR(13)+;
				[  ErrMsg: ] + cErrMsg+CHR(13)+;
				[  UserValue: ] + oErr.USERVALUE

			tsubject = "945 EDI Poller Error at "+TTOC(DATETIME())
			tattach  = ""
			tcc=""
			ASSERT .F. MESSAGE "At do mail stage...DEBUG"
			DO FORM m:\dev\frm\dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
		ENDIF
		FCLOSE(nFilenum)
	ENDIF
FINALLY
	IF USED('OUTSHIP')
		USE IN outship
	ENDIF
	IF USED('OUTDET')
		USE IN outdet
	ENDIF
	IF USED('SHIPMENT')
		USE IN shipment
	ENDIF
	IF USED('PACKAGE')
		USE IN package
	ENDIF
	IF USED('SERFILE')
		USE IN serfile
	ENDIF

	WAIT CLEAR

ENDTRY

*** END OF CODE BODY

****************************
PROCEDURE close945
****************************
** Footer Creation

	STORE  "GE"+cfd+ALLTRIM(STR(nSTCount))+cfd+c_CntrlNum+csegd TO cString
	FPUTS(nFilenum,cString)

	STORE  "IEA"+cfd+"1"+cfd+PADL(c_CntrlNum,9,"0")+csegd TO cString
	FPUTS(nFilenum,cString)

	RETURN

****************************
PROCEDURE num_incr_isa
****************************

	IF USED("serfile")
		USE IN serfile
	ENDIF
	USE ("f:\3pl\data\serial\2XUserial") IN 0 ALIAS serfile

	SELECT serfile
	IF lTesting AND lISAFlag
		nOrigSeq = serfile.seqnum
		lISAFlag = .F.
	ENDIF
	nISA_Num = serfile.seqnum
	c_CntrlNum = ALLTRIM(STR(serfile.seqnum))
	REPLACE serfile.seqnum WITH serfile.seqnum + 1 IN serfile
	SELECT outship
	RETURN

****************************
PROCEDURE num_incr_st
****************************

	IF USED("serfile")
		USE IN serfile
	ENDIF
	USE ("f:\3pl\data\serial\2XUserial") IN 0 ALIAS serfile

	SELECT serfile
	IF lTesting AND lSTFlag
		nOrigGrpSeq = serfile.grpseqnum
		lSTFlag = .F.
	ENDIF
	c_GrpCntrlNum = ALLTRIM(STR(serfile.grpseqnum))
	REPLACE serfile.grpseqnum WITH serfile.grpseqnum + 1 IN serfile
	SELECT outship
	RETURN


****************************
PROCEDURE segmentget
****************************
	PARAMETER thisarray,lcKey,nLength

	FOR i = 1 TO nLength
		IF i > nLength
			EXIT
		ENDIF
		lnEnd= AT("*",thisarray[i])
		IF lnEnd > 0
			lcThisKey =TRIM(SUBSTR(thisarray[i],1,lnEnd-1))
			IF OCCURS(lcKey,lcThisKey)>0
				RETURN SUBSTR(thisarray[i],lnEnd+1)
				i = 1
			ENDIF
		ENDIF
	ENDFOR

	RETURN ""

****************************
PROCEDURE ediupdate
****************************
	PARAMETER cStatus,lIsError

	IF !lTesting
		SELECT edi_trigger
		nRec = RECNO()
		IF !lIsError
			REPLACE processed WITH .T.,proc945 WITH .T.,file945 WITH cFilenameArch,isa_num WITH cISA_Num,;
				fin_status WITH "945 CREATED",errorflag WITH .F.,when_proc WITH DATETIME() ;
				FOR edi_trigger.bol = cBOL AND accountid = nAcctNum
			
*			xsqlexec("update edi_trigger set processed=1, proc945=1, file945='"+cFilenameArch+"', " + ;
				"isa_num='"+cISA_Num+"', fin_status='945 CREATED', errorflag=0, when_proc={"+ttoc(datetime())+"} " + ;
				"where bol='"+cBOL+"' and accountid="+transform(nAcctNum),,,"stuff")

			lDoCatch = .F.
		ELSE
			lDoCatch = .F.
			REPLACE processed WITH .T.,proc945 WITH .F.,file945 WITH "",;
				fin_status WITH cStatus,errorflag WITH .T. ;
				FOR edi_trigger.bol = cBOL AND accountid = nAcctNum

*			xsqlexec("update edi_trigger set processed=1, proc945=0, file945='', fin_status='"+cstatus+"', " + ;
				"errorflag=1 where bol='"+cBOL+"' and accountid="+transform(nAcctNum),,,"stuff")

*			num_decrement()

			IF lCloseOutput
				=FCLOSE(nFilenum)
				ERASE &cFilenameHold
			ENDIF
		ENDIF
	ENDIF

	IF lIsError AND lEmail AND cStatus<>"SQL ERROR"	
*		SET STEP ON 
		tsubject = "945 Error in "+cMailName+" BOL "+TRIM(cBOL)+"(At PT "+cShip_ref+")"
		tattach = " "
		tsendto = tsendtoerr
		tcc = tccerr
		tmessage = "945 Processing for BOL# "+cBOL+", WO# "+cWO_Num+" produced this error: (Office: "+cOffice+"): "+cStatus+CHR(10)+"Check EDI_TRIGGER and re-run"
		IF "TOTQTY ERR"$cStatus
			tmessage = tmessage + CHR(10) + "At OUTSHIPID: "+ALLTRIM(STR(m.outshipid))
		ENDIF
		ASSERT .F. MESSAGE "At do mail stage...DEBUG"
		DO FORM m:\dev\frm\dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
	ENDIF

	IF USED('outship')
		USE IN outship
	ENDIF
	IF USED('outdet')
		USE IN outdet
	ENDIF

	IF USED('serfile')
		SELECT serfile
		IF lTesting
			REPLACE serfile.seqnum WITH nOrigSeq
			REPLACE serfile.grpseqnum WITH nOrigGrpSeq
		ENDIF
		USE IN serfile
	ENDIF
	IF USED('scacs')
		USE IN scacs
	ENDIF
	IF USED('mm')
		USE IN mm
	ENDIF
	IF USED('tempx')
		USE IN tempx
	ENDIF
	IF !lTesting
		SELECT edi_trigger
		LOCATE
	ENDIF

ENDPROC

****************************
PROCEDURE cstringbreak
****************************
	cLen = LEN(ALLTRIM(cString))
	IF ISNULL(cString)
		ASSERT .F. MESSAGE "At cStringBreak procedure"
	ENDIF
	FPUTS(nFilenum,cString)
ENDPROC


****************************
PROCEDURE cszbreak
****************************
	cCSZ = ALLT(m.CSZ)

	FOR ii = 5 TO 2 STEP -1
		cCSZ = STRTRAN(cCSZ,SPACE(ii),SPACE(1))
	ENDFOR
	cCSZ = STRTRAN(cCSZ,",","")
	len1 = LEN(ALLT(cCSZ))
	nSpaces = OCCURS(" ",cCSZ)

	IF nSpaces<2
		cErrMsg = "BAD CSZ INFO"
		THROW
	ENDIF

*  ASSERT .F. MESSAGE "In CSZ Breakout"
	FOR ii = nSpaces TO 1 STEP -1
		ii1 = ALLT(STR(ii))
		DO CASE
			CASE ii = nSpaces
				nPOS = AT(" ",cCSZ,ii)
				cZip = ALLT(SUBSTR(cCSZ,nPOS))
*        WAIT WINDOW "ZIP: "+cZip TIMEOUT 1
				nEndState = nPOS
			CASE ii = (nSpaces - 1)
				nPOS = AT(" ",cCSZ,ii)
				cState = ALLT(SUBSTR(cCSZ,nPOS,nEndState-nPOS))
*        WAIT WINDOW "STATE: "+cState TIMEOUT 1
				IF nSpaces = 2
					cCity = ALLT(LEFT(cCSZ,nPOS))
*          WAIT WINDOW "CITY: "+cCity TIMEOUT 1
					EXIT
				ENDIF
			OTHERWISE
				nPOS = AT(" ",cCSZ,ii)
				cCity = ALLT(LEFT(cCSZ,nPOS))
*        WAIT WINDOW "CITY: "+cCity TIMEOUT 2
		ENDCASE
	ENDFOR
ENDPROC

****************************
PROCEDURE num_decrement
****************************
*!* This procedure decrements the ISA/GS numbers in the counter
*!* in the event of failure and deletion of the current 945
	IF lCloseOutput
		IF !USED("serfile")
			USE ("f:\3pl\data\serial\"+cCustname+"serial") IN 0 ALIAS serfile
		ENDIF
		SELECT serfile
		REPLACE serfile.seqnum WITH serfile.seqnum - 1 IN serfile  && ISA number
		REPLACE serfile.grpseqnum WITH serfile.grpseqnum - 1 IN serfile  && GS number
		RETURN
	ENDIF
ENDPROC
