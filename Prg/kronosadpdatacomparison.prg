* create spreadsheet showing discrepancies betweeen Kronos and ADP for certain important fields:
*	Companycode
*	Division
*	Department
*	Worksite
*	Status


*** exe = F:\UTIL\ADPEDI\KRONOSADPDATACOMPARISON.EXE

LOCAL loKronosADPDataComparison
loKronosADPDataComparison = CREATEOBJECT('KronosADPDataComparison')
loKronosADPDataComparison.MAIN()
CLOSE DATABASES ALL
RETURN

#DEFINE LOGIT 1
#DEFINE WAITIT 2
#DEFINE NOWAITIT 4
#DEFINE SENDIT 8
#DEFINE CRLF CHR(13) + CHR(10)
#DEFINE RETURN_DATA_MANDATORY .F.
#DEFINE RETURN_DATA_NOT_MANDATORY .T.
#DEFINE CSVFORMAT 6
#DEFINE WRITE_ONLY_UNBUFFERED 11
#DEFINE COMMA ","

DEFINE CLASS KronosADPDataComparison AS CUSTOM

  cProcessName = 'KronosADPDataComparison'

  lTestMode = .T.  && .F. FOR NORMAL OPERATION, .T. TO SUPPRESS ALL BUT DEVELOPER EMAILS AND CHANGE INPUT TABLE, ETC.

  lAutoYield = .T.

  cStartTime = TTOC(DATETIME())
  cFileDate = STRTRAN(DTOC(DATE()),"/","_")

  * connection properties
  nSQLHandle = 0

  * object ref properties
  oExcel = NULL

  * processing properties
  cTopEmailText = ""

  * wait window properties
  nWaitWindowTimeout = 2
  lWaitWindowIsOn = .T.

  * logfile properties
  lLoggingIsOn = .T.
  cLogFile = 'F:\UTIL\ADPEDI\Logfiles\KronosADPDataComparison_log.txt'

  * INTERNAL email properties
  lSendInternalEmailIsOn = .T.
  cFrom = 'FMI Corporate <fmicorporate@fmiint.com>'
  *cSendTo = 'Mark Bennett <mbennett@fmiint.com>'
  cSendTo = 'Lucille Waldrip <lwaldrip@fmiint.com>'
  cCC = 'Mark Bennett <mbennett@fmiint.com>'
  cSubject = 'Kronos ADP Data Comparison, Results for: ' + TTOC(DATETIME())
  cAttach = ''
  cBodyText = ''

  cCOMPUTERNAME = ''
  cUSERNAME = ''

  FUNCTION INIT
    IF NOT DODEFAULT()
      RETURN .F.
    ENDIF
    WITH THIS
      *SET RESOURCE OFF
      CLOSE DATA
      SET CENTURY ON
      SET DATE AMERICAN
      SET HOURS TO 24
      SET DECIMALS TO 2
      SET ANSI ON
      SET TALK OFF
      SET DELETED ON
      SET CONSOLE OFF
      SET EXCLUSIVE OFF
      SET SAFETY OFF
      SET EXACT ON
      SET STATUS BAR OFF
      SET SYSMENU OFF
      SET ENGINEBEHAVIOR 70
      _VFP.AUTOYIELD = .lAutoYield
      .cCOMPUTERNAME = GETENV("COMPUTERNAME")
      .cUSERNAME = GETENV("USERNAME")
      IF .lTestMode THEN
        .cSendTo = 'Mark Bennett <mbennett@fmiint.com>'
        .cCC = ''
        .cLogFile = 'F:\UTIL\ADPEDI\Logfiles\KronosADPDataComparison_log_TESTMODE.txt'
      ENDIF
      .lLoggingIsOn = .lLoggingIsOn AND NOT EMPTY(.cLogFile)
      IF .lLoggingIsOn THEN
        SET ALTERNATE TO (.cLogFile) ADDITIVE
        SET ALTERNATE ON
      ENDIF
    ENDWITH
  ENDFUNC


  FUNCTION DESTROY
    WITH THIS
      IF .lLoggingIsOn  THEN
        SET ALTERNATE OFF
        SET ALTERNATE TO
      ENDIF
      SET STATUS BAR ON
    ENDWITH
    DODEFAULT()
  ENDFUNC


  FUNCTION MAIN
    WITH THIS
      LOCAL lnNumberOfErrors, lcSQL, lcSQL2, loError, lcErrorMsg, lcSQLWTK
      TRY
        lnNumberOfErrors = 0

        .TrackProgress('', LOGIT)
        .TrackProgress('', LOGIT)
        .TrackProgress('=========================================================', LOGIT+SENDIT)
        .TrackProgress('Kronos ADP Data Comparison process started....', LOGIT+SENDIT+NOWAITIT)
        .TrackProgress('COMPUTERNAME = ' + .cCOMPUTERNAME, LOGIT+SENDIT)
        .TrackProgress('USERNAME = ' + .cUSERNAME, LOGIT+SENDIT)
        .TrackProgress('=========================================================', LOGIT+SENDIT)
        IF .lTestMode THEN
          .TrackProgress('', LOGIT)
          .TrackProgress('=========================================================', LOGIT+SENDIT)
          .TrackProgress('                        TEST MODE                        ', LOGIT+SENDIT)
          .TrackProgress('=========================================================', LOGIT+SENDIT)
        ENDIF

        SET PROCEDURE TO VALIDATIONS ADDITIVE


        * get data from ADP
        .TrackProgress('Connecting to ADP System....', LOGIT+SENDIT)
        *OPEN DATABASE F:\UTIL\ADPCONNECTION\CONNECT1

        .nSQLHandle = SQLCONNECT("PCPAYWIN","MBENNETT","VP737A")

        IF .nSQLHandle > 0 THEN

          * we UNION so that we get all Active status plus all Terminated that are not active in
          * another company. We don't want to have both an Active rec and Terminated rec for
          * same employee, because then which one do we compare with Kronos?

          lcSQL = ;
            "SELECT " + ;
            " {fn IFNULL(COMPANYCODE,'???')} AS ADP_COMP, " + ;
            " {fn LEFT(HOMEDEPARTMENT,2)} AS DIVISION, " + ;
            " {fn SUBSTRING(HOMEDEPARTMENT,3,4)} AS DEPARTMENT, " + ;
            " FILE# AS FILE_NUM, " + ;
            " NAME, " + ;
            " {fn IFNULL(CUSTAREA3,'???')} AS WORKSITE, " + ;
            " TERMINATIONDATE AS TERMDATE, " + ;
            " {fn IFNULL(CLOCK#,'?????')} AS CLOCK_NUM, " + ;
            " STATUS " + ;
            " FROM REPORTS.V_EMPLOYEE " + ;
            " WHERE COMPANYCODE IN ('E87','E88','E89') " + ;
            " AND STATUS <> 'T' " + ;
            "UNION ALL " + ;
            "SELECT " + ;
            " {fn IFNULL(COMPANYCODE,'???')} AS ADP_COMP, " + ;
            " {fn LEFT(HOMEDEPARTMENT,2)} AS DIVISION, " + ;
            " {fn SUBSTRING(HOMEDEPARTMENT,3,4)} AS DEPARTMENT, " + ;
            " FILE# AS FILE_NUM, " + ;
            " NAME, " + ;
            " {fn IFNULL(CUSTAREA3,'???')} AS WORKSITE, " + ;
            " TERMINATIONDATE AS TERMDATE, " + ;
            " {fn IFNULL(CLOCK#,'?????')} AS CLOCK_NUM, " + ;
            " STATUS " + ;
            " FROM REPORTS.V_EMPLOYEE " + ;
            " WHERE COMPANYCODE IN ('E87','E88','E89') " + ;
            " AND STATUS = 'T' " + ;
            " AND FILE# NOT IN (SELECT FILE# FROM REPORTS.V_EMPLOYEE WHERE STATUS <> 'T' AND COMPANYCODE IN ('E87','E88','E89')) " + ;
            "ORDER BY NAME "

		  * this select will simply give all ADP employees, so that we can compare vs wtk to resolve discrepancies
          lcSQL2 = ;
            "SELECT " + ;
            " {fn IFNULL(COMPANYCODE,'???')} AS ADP_COMP, " + ;
            " {fn LEFT(HOMEDEPARTMENT,2)} AS DIVISION, " + ;
            " {fn SUBSTRING(HOMEDEPARTMENT,3,4)} AS DEPARTMENT, " + ;
            " FILE# AS FILE_NUM, " + ;
            " NAME, " + ;
            " {fn IFNULL(CUSTAREA3,'???')} AS WORKSITE, " + ;
            " {fn IFNULL(CLOCK#,'?????')} AS CLOCK_NUM, " + ;
            " STATUS, " + ;
            " HIREDATE, " + ;
            " TERMINATIONDATE AS TERMDATE " + ;
            " FROM REPORTS.V_EMPLOYEE " + ;
            " WHERE COMPANYCODE IN ('E87','E88','E89') " + ;
            " ORDER BY NAME, FILE#, COMPANYCODE, STATUS "

            *" AND FILE# NOT IN (SELECT FILE# FROM REPORTS.V_EMPLOYEE WHERE STATUS = 'A' AND COMPANYCODE IN ('E87','E88','E89')) " + ;

          IF USED('ADPCURSOR1') THEN
            USE IN ADPCURSOR1
          ENDIF
          IF USED('ADPCURSOR2') THEN
            USE IN ADPCURSOR2
          ENDIF
          IF USED('ADPCURSORALL') THEN
            USE IN ADPCURSORALL
          ENDIF
          IF USED('ADPCURSORALL2') THEN
            USE IN ADPCURSORALL2
          ENDIF

          IF .ExecSQL(lcSQL, 'ADPCURSOR1', RETURN_DATA_MANDATORY) AND .ExecSQL(lcSQL2, 'ADPCURSORALL', RETURN_DATA_MANDATORY) THEN

            SELECT ;
              GetOldADPCompany(ADP_COMP) AS ADP_COMP, ;
              DIVISION, ;
              DEPARTMENT, ;
              INT(FILE_NUM) AS FILE_NUM, ;
              NAME, ;
              LEFT(WORKSITE,3) AS WORKSITE, ;
              TERMDATE, ;
              CLOCK_NUM, ;
              STATUS ;
              FROM ADPCURSOR1 ;
              INTO CURSOR ADPCURSOR2 ;
              ORDER BY NAME ;
              READWRITE

            * clean up null termdates
            SELECT ADPCURSOR2
            SCAN
              IF ISNULL(ADPCURSOR2.TERMDATE) THEN
                REPLACE ADPCURSOR2.TERMDATE WITH CTOD('')
              ENDIF
            ENDSCAN

            SELECT ;
              GetOldADPCompany(ADP_COMP) AS ADP_COMP, ;
              DIVISION, ;
              DEPARTMENT, ;
              INT(FILE_NUM) AS FILE_NUM, ;
              NAME, ;
              LEFT(WORKSITE,3) AS WORKSITE, ;
              CLOCK_NUM, ;
              STATUS, ;
              HIREDATE, ;
              TERMDATE ;
              FROM ADPCURSORALL ;
              INTO CURSOR ADPCURSORALL2 ;
              ORDER BY NAME, FILE_NUM, ADP_COMP, STATUS ;
              READWRITE

            * clean up null termdates
            SELECT ADPCURSORALL2
            SCAN
              IF ISNULL(ADPCURSORALL2.TERMDATE) THEN
                REPLACE ADPCURSORALL2.TERMDATE WITH CTOD('')
              ENDIF
            ENDSCAN

*!*	SELECT ADPCURSORALL2
*!*	COPY TO F:\UTIL\ADPEDI\REPORTS\ALLADP.XLS XL5


          ENDIF  &&  .ExecSQL(lcSQL, 'ADPCURSOR1', RETURN_DATA_MANDATORY)

          .TrackProgress('', LOGIT+SENDIT)
          .TrackProgress('Kronos ADP Data Comparison process ended normally.', LOGIT+SENDIT+NOWAITIT)
          WAIT CLEAR

          =SQLDISCONNECT(.nSQLHandle)

        ELSE
          * connection error
          .TrackProgress('Unable to connect to ADP Payroll System.', LOGIT+SENDIT)
        ENDIF   &&  .nSQLHandle > 0


        * get data from KRONOS
        .TrackProgress('Connecting to WTK System....', LOGIT+SENDIT)

        lcSQLWTK = ;
          " SELECT " + ;
          " PERSONNUM AS FILE_NUM, " + ;
          " PERSONFULLNAME AS NAME, " + ;
          " HOMELABORLEVELNM1 AS ADP_COMP, " + ;
          " HOMELABORLEVELNM2 AS DIVISION, " + ;
          " HOMELABORLEVELNM3 AS DEPARTMENT, " + ;
          " HOMELABORLEVELNM4 AS WORKSITE, " + ;
          " EMPLOYMENTSTATUS AS STATUS, " + ;
		  " COMPANYHIREDTM AS HIREDATE, " + ;
		  " {fn IFNULL(ACCRUALPRFLNAME,'')}AS ACPROFILE, " + ;
		  " EMPLOYMENTSTATUSDT AS TERMDATE " + ;
          " FROM VP_EMPLOYEEV42 " + ;
          " WHERE HOMELABORLEVELNM1 IN ('SXI','ZXU','AXA') " + ;
          " ORDER BY PERSONFULLNAME, PERSONNUM, HOMELABORLEVELNM1, STATUS "

        OPEN DATABASE F:\UTIL\KRONOS\KRONOSEDI

        .nSQLHandle = SQLCONNECT("WTK_ON_PAYROLL2","sa","kr0n1tes")

        IF .nSQLHandle > 0 THEN

          IF USED('KRONOSCURSOR1') THEN
            USE IN KRONOSCURSOR1
          ENDIF
          IF USED('KRONOSCURSOR2') THEN
            USE IN KRONOSCURSOR2
          ENDIF

          IF .ExecSQL(lcSQLWTK, 'KRONOSCURSOR1', RETURN_DATA_MANDATORY) THEN
          
*!*	    SELECT KRONOSCURSOR1
*!*	    BROWSE

            SELECT ;
              INT(VAL(FILE_NUM)) AS FILE_NUM, ;
              LEFT(ADP_COMP,3) AS ADP_COMP, ;
              NAME, ;
              LEFT(DIVISION,2) AS DIVISION, ;
              LEFT(DEPARTMENT,4) AS DEPARTMENT, ;
              LEFT(WORKSITE,3) AS WORKSITE , ;
              LEFT(STATUS,1) AS STATUS, ;
              TTOD(HIREDATE) AS HIREDATE, ;
              TTOD(TERMDATE) AS TERMDATE, ;
              ACPROFILE ;
              FROM KRONOSCURSOR1 ;
              INTO CURSOR KRONOSCURSOR2 ;
              ORDER BY NAME ;
              READWRITE

          ENDIF

          REPLACE ALL STATUS WITH "L" FOR STATUS = "I" IN KRONOSCURSOR2

          *!*	  SELECT KRONOSCURSOR2
          *!*	  BROW
          
          SELECT KRONOSCURSOR2
		  SCAN
		    IF STATUS <> 'T' THEN
		      REPLACE TERMDATE WITH CTOD('')
		    ENDIF
		  ENDSCAN
          
*!*	SELECT KRONOSCURSOR2
*!*	COPY TO F:\UTIL\ADPEDI\REPORTS\ALLWTK.XLS XL5


        ELSE
          * connection error
          .TrackProgress('Unable to connect to WTK System.', LOGIT+SENDIT)
        ENDIF   &&  .nSQLHandle > 0

        =SQLDISCONNECT(.nSQLHandle)


        *!*	            WAIT WINDOW NOWAIT "Opening Excel..."
        *!*	            .oExcel = CREATEOBJECT("excel.application")

        *!*	            .oExcel.VISIBLE = .lTestMode


        *!*	        IF TYPE('.oExcel') = 'O' AND NOT ISNULL(.oExcel) THEN
        *!*	          .oExcel.QUIT()
        *!*	        ENDIF


*!*	SELECT ADPCURSOR2
*!*	brow
*!*	SELECT KRONOSCURSOR2
*!*	brow


        * scan through adp cursor and compare it with kronos cursor
        * write a warning message if any of the key fields are different,
        * or if the adp file# can't be found in kronos.
        LOCAL lcADPInfo, lcKronosInfo, lcComment, lcList

        lcList = ""
        SELECT ADPCURSOR2
        SCAN
          lcComment = ""
          lcADPInfo = "IN ADP    -- File#: " + TRANSFORM(ADPCURSOR2.FILE_NUM) + "  " + ADPCURSOR2.ADP_COMP + ;
            "  " + ADPCURSOR2.DIVISION + ;
            "  " + ADPCURSOR2.DEPARTMENT + ;
            "  " + ADPCURSOR2.WORKSITE + ;
            "  " + ADPCURSOR2.STATUS + ;
            "  " + PADR(ADPCURSOR2.CLOCK_NUM,25) + ;
            "   " + ALLTRIM(ADPCURSOR2.NAME)

          IF ADPCURSOR2.STATUS = 'T' THEN
            lcADPInfo = lcADPInfo + " -- TermDate = " + TRANSFORM(TTOD(ADPCURSOR2.TERMDATE))
          ENDIF

          SELECT KRONOSCURSOR2
          LOCATE FOR FILE_NUM = ADPCURSOR2.FILE_NUM
          IF FOUND() THEN
            * compare fields
            * first check if both status are 'T' if so, there is no point in doing any more for this person

            IF (ADPCURSOR2.STATUS = 'T') AND (KRONOSCURSOR2.STATUS = 'T') THEN
              * nothing: skip further processing this file#
            ELSE
              * continue checking

              IF ADPCURSOR2.ADP_COMP <> KRONOSCURSOR2.ADP_COMP THEN
                lcComment = lcComment + "ADP_COMP "
              ENDIF
              IF ADPCURSOR2.DIVISION <> KRONOSCURSOR2.DIVISION THEN
                lcComment = lcComment + "DIVISION "
              ENDIF
              IF ADPCURSOR2.DEPARTMENT <> KRONOSCURSOR2.DEPARTMENT THEN
                lcComment = lcComment + "DEPARTMENT "
              ENDIF
              IF ADPCURSOR2.WORKSITE <> KRONOSCURSOR2.WORKSITE THEN
                lcComment = lcComment + "WORKSITE "
              ENDIF
              IF ADPCURSOR2.STATUS <> KRONOSCURSOR2.STATUS THEN
                lcComment = lcComment + "STATUS "
              ENDIF
              
              IF (ADPCURSOR2.CLOCK_NUM = 'XN50X') AND (NOT EMPTY(KRONOSCURSOR2.ACPROFILE)) THEN
                lcComment = lcComment + "PART-TIME IN ADP but has A KRONOS ACCRUALS RULE"
              ENDIF
              
*!*	              IF (NOT (ADPCURSOR2.CLOCK_NUM = 'XN50X')) AND EMPTY(KRONOSCURSOR2.ACPROFILE) THEN
*!*	                lcComment = lcComment + "FULL-TIME IN ADP but has NO KRONOS ACCRUALS RULE"
*!*	              ENDIF

              IF NOT EMPTY(lcComment) THEN

                * there was at least one mismatch
                lcComment = "Mismatches on: " + lcComment

                lcKronosInfo = "IN KRONOS -- File#: " + TRANSFORM(KRONOSCURSOR2.FILE_NUM) + "  " + KRONOSCURSOR2.ADP_COMP + ;
                  "  " + KRONOSCURSOR2.DIVISION + ;
                  "  " + KRONOSCURSOR2.DEPARTMENT + ;
                  "  " + KRONOSCURSOR2.WORKSITE + ;
                  "  " + KRONOSCURSOR2.STATUS + ;
                  "  " + LEFT(KRONOSCURSOR2.ACPROFILE,25) + ;
                  "   " + ALLTRIM(KRONOSCURSOR2.NAME)

                lcList = lcList + lcADPInfo + CRLF + lcKronosInfo + CRLF + lcComment + CRLF + CRLF

              ENDIF

            ENDIF  && (ADPCURSOR2.STATUS = 'T') AND (KRONOSCURSOR2.STATUS = 'T')

          ELSE
            lcComment = "File# not found in Kronos!"
            lcList = lcList + lcADPInfo + CRLF + lcComment + CRLF + CRLF
          ENDIF

        ENDSCAN

        .TrackProgress(lcList,LOGIT+SENDIT)

        CLOSE DATABASES ALL

      CATCH TO loError
        WAIT CLEAR
        .TrackProgress('There was an error.',LOGIT+SENDIT)
        .TrackProgress('The Error # is: ' + TRANSFORM(loError.ERRORNO), LOGIT+SENDIT)
        .TrackProgress('The Error Message is: ' + TRANSFORM(loError.MESSAGE), LOGIT+SENDIT)
        .TrackProgress('The Error occurred in Program: ' + TRANSFORM(loError.PROCEDURE), LOGIT+SENDIT)
        .TrackProgress('The Error occurred at line #: ' + TRANSFORM(loError.LINENO), LOGIT+SENDIT)
        lnNumberOfErrors = lnNumberOfErrors + 1
        IF TYPE('.oExcel') = 'O' AND NOT ISNULL(.oExcel) THEN
          .oExcel.QUIT()
        ENDIF
        CLOSE DATA

      ENDTRY

      CLOSE DATA
      WAIT CLEAR
      ***************** INTERNAL email results ******************************
      .TrackProgress('About to send status email.',LOGIT)
      .TrackProgress('The logfile is: ' + .cLogFile, SENDIT)
      .TrackProgress('Kronos ADP Data Comparison process started: ' + .cStartTime, LOGIT+SENDIT)
      .TrackProgress('Kronos ADP Data Comparison process finished: ' + TTOC(DATETIME()), LOGIT+SENDIT)

      IF .lSendInternalEmailIsOn THEN
        * try to trap error from not having dartmail dll's registered on user's pc...
        TRY
          DO FORM dartmail2 WITH .cSendTo,.cFrom,.cSubject,.cCC,.cAttach,.cBodyText,"A"
          .TrackProgress('Sent status email.',LOGIT)
        CATCH TO loError
          *=MESSAGEBOX('There was an error sending the status email. Please contact Mark Bennett.',0+64,"Kronos ADP Data Comparison")
          .TrackProgress('There was an error sending the status email.',LOGIT)
          .TrackProgress('The Error # is: ' + TRANSFORM(loError.ERRORNO), LOGIT)
          .TrackProgress('The Error Message is: ' + TRANSFORM(loError.MESSAGE), LOGIT)
          .TrackProgress('The Error occurred in Program: ' + TRANSFORM(loError.PROCEDURE), LOGIT)
          .TrackProgress('The Error occurred at line #: ' + TRANSFORM(loError.LINENO), LOGIT)
          lnNumberOfErrors = lnNumberOfErrors + 1
          IF TYPE('.oExcel') = 'O' AND NOT ISNULL(.oExcel) THEN
            .oExcel.QUIT()
          ENDIF
          CLOSE DATA
        ENDTRY
      ELSE
        .TrackProgress('Did not send status email: THIS.lSendInternalEmailIsOn = FALSE.',LOGIT)
      ENDIF

    ENDWITH
    RETURN
  ENDFUNC && main


  PROCEDURE SortArrayByDateTime
    * expects taInArray[] to be the result of an ADIR()
    LPARAMETERS taInArray, taOutArray
    EXTERNAL ARRAY taInArray, taOutArray
    LOCAL lnRows, lnCols, i, j
    * extending taInArray[] to process the files in date/time order
    lnRows = ALEN(taInArray,1)
    lnCols = ALEN(taInArray,2)
    * create taOutArray with one more column
    DIMENSION taOutArray[lnRows,lnCols + 1]
    * and fill it with taInArray columns AND (filedate + filetime) in new column
    * ( ACOPY() doesn't preserve elements properly because of different # of columns )
    FOR i = 1 TO lnRows
      FOR j = 1 TO lnCols
        taOutArray[i,j] = taInArray[i,j]
      ENDFOR
      taOutArray[i,lnCols + 1] = DTOS(taInArray[i,3]) + "_" + taInArray[i,4]
    ENDFOR
    * sort taOutArray on date/time column
    =ASORT(taOutArray,6)
  ENDPROC


  FUNCTION ExecSQL
    LPARAMETERS tcSQL, tcCursorName, tlNoDataReturnedIsOkay
    LOCAL llRetval, lnResult
    WITH THIS
      * close target cursor if it's open
      IF USED(tcCursorName)
        USE IN (tcCursorName)
      ENDIF
      WAIT WINDOW "Fetching remote data; please wait..." NOWAIT
      lnResult = SQLEXEC(.nSQLHandle, tcSQL, tcCursorName)
      llRetval = ( lnResult > 0 )
      IF llRetval THEN
        * see if any data came back
        IF NOT tlNoDataReturnedIsOkay THEN
          IF (NOT USED(tcCursorName)) OR EOF(tcCursorName) THEN
            llRetval = .F.
            .TrackProgress('No data was returned by this query: ' +CRLF + tcSQL, LOGIT+SENDIT)
          ENDIF
        ENDIF
      ELSE
        .TrackProgress("Result Code [" + TRANSFORM(lnResult) + "] returned by this query: " + tcSQL, LOGIT+SENDIT)
      ENDIF
      WAIT CLEAR
      RETURN llRetval
    ENDWITH
  ENDFUNC


  PROCEDURE TrackProgress
    * do any combination of Wait Window, writing to logfile, and adding to body of email,
    * based on nFlags parameter.
    LPARAMETERS tcExpression, tnFlags
    WITH THIS
      IF BITAND(tnFlags,LOGIT) = LOGIT THEN
        IF .lLoggingIsOn THEN
          ?
          ? .cProcessName + "  " + TTOC(DATETIME()) + ": " + tcExpression
        ENDIF
      ENDIF
      IF .lWaitWindowIsOn THEN
        IF BITAND(tnFlags,NOWAITIT) = NOWAITIT THEN
          WAIT WINDOW tcExpression NOWAIT
        ENDIF
        IF BITAND(tnFlags,WAITIT) = WAITIT THEN
          WAIT WINDOW tcExpression TIMEOUT .nWaitWindowTimeout
        ENDIF
      ENDIF
      IF BITAND(tnFlags,SENDIT) = SENDIT THEN
        IF .lSendInternalEmailIsOn THEN
          .cBodyText = .cBodyText + tcExpression + CRLF + CRLF
        ENDIF
      ENDIF
    ENDWITH
  ENDPROC  &&  TrackProgress

ENDDEFINE

