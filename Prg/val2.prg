* validate WMS data
 
utilsetup("VAL2")

store "" to guserid, gprocess
gdevelopment=.f.
xjobno=0
xsystem="VAL2"

external array xdbf

use f:\auto\val2 exclusive
zap
use

*

valset("find badly pulled P&P WO's")

xsqlexec("select * from account where inactive=0","account",,"qq")
index on accountid tag accountid
set order to

select whoffice
scan for !test and !novalid and picknpack
	xoffice=whoffice.office
	goffice=xoffice
	xfolder=whoffice.folder+"\whdata\"
	xpnpaccount=strtran(pnpacct(xoffice),"accountid","inwolog.accountid")
	xindetpnpaccount=strtran(xpnpaccount,"inwolog","indet")

	xsqlexec("select * from inwolog where mod='"+goffice+"' and "+xpnpaccount+" and archived=0 and updatedt>{"+dtoc(date()-1)+"}",,,"wh")
	xsqlexec("select * from indet where mod='"+goffice+"' and "+xindetpnpaccount+" and !empty(po)",,,"wh")
	index on wo_num tag wo_num
	index on po tag po
	index on units tag units
	index on removeqty tag removeqty
	index on totqty tag totqty
	set order to
	
	select inwolog.wo_num, inwolog.wo_date, inwolog.accountid, indetid, po, units, style, color, ;
		id, pack, totqty, indet.removeqty, 000000 as unitremoveqty, .f. as bad ;
		from inwolog, indet where inwolog.inwologid=indet.inwologid ;
		and &xpnpaccount and !units and !empty(po) and !inwolog.archived and pack#"XDOCK" and inwolog.updatedt>date()-5 ;
		group by inwolog.wo_num, po into cursor xjones readwrite

	select xjones
	scan
		select indet
		sum removeqty to xunitremoveqty for wo_num=xjones.wo_num and po=xjones.po and units
		replace unitremoveqty with xunitremoveqty in xjones

		if xjones.removeqty*val(xjones.pack)#xjones.unitremoveqty
			replace bad with .t. in xjones
		else
			if xjones.removeqty=0
				locate for wo_num=xjones.wo_num and po=xjones.po and units and removeqty#0
			else
				locate for wo_num=xjones.wo_num and po=xjones.po and units and removeqty%xjones.removeqty#0
				if !found()
					locate for wo_num=xjones.wo_num and po=xjones.po and units and removeqty=0
				endif
				if !found() and xjones.totqty=xjones.removeqty
					locate for wo_num=xjones.wo_num and po=xjones.po and units and totqty#removeqty
				endif
				if !found() and xjones.totqty#xjones.removeqty
					locate for wo_num=xjones.wo_num and po=xjones.po and units and totqty=removeqty
				endif
			endif
			replace bad with found("indet") in xjones
		endif
	endscan

	count for bad	&& stop auto-fixing dy 3/23/15
	if _tally>0
*!*			select xjones
*!*			scan for bad
*!*				select indet
*!*				scan for wo_num=xjones.wo_num and po=xjones.po and units
*!*					xremoveqty = (xjones.removeqty*val(xjones.pack)) * (indet.totqty/(xjones.totqty*val(xjones.pack)))
*!*					replace removeqty with xremoveqty in indet
*!*					select inloc
*!*					locate for indetid=indet.indetid and whseloc="RACK"
*!*					if found()
*!*						replace removeqty with xremoveqty in inloc 
*!*					else
*!*						locate for indetid=indet.indetid and whseloc="CYCLEU"
*!*						if found()
*!*							replace removeqty with xremoveqty in inloc 
*!*						endif
*!*					endif
*!*				endscan
*!*			endscan

		select xjones
		copy to ("f:\auto\xjones"+xoffice) for bad
		xtally=_tally
		email("Dyoung@fmiint.com","VAL "+transform(xtally)+" badly pulled P&P WO's, office "+xoffice,"xjones"+xoffice,,,,.t.,,,,,.t.)
	endif

	use in inwolog
	use in indet
endscan

*

use f:\auto\val2 
go bottom
replace zendtime with datetime()
replace all seconds with zendtime-zstarttime
use 

schedupdate()

_screen.Caption=gscreencaption
on error

