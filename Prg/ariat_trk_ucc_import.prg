* ARIAT_TRK_UCC_IMPORT

* daily import of Ariat UCC and Tracking # info from Ariat's lvups (shared) folder; places that info in F:\WHK\WHDATA\CTNUCC.DBF
* the info is for shipments going to Canada.

* programmed by MB; spec'd by Paul G.

****************************************************************************************************************
***** NOTE: assumes drive P: is mapped to \\dc3\home\lvups; have mapped that on TASK4, so schedule it on TASK4!!
****************************************************************************************************************
*
* Revised 4/21/2016 MB to not send an email if it finds no files to process, because of the email flooding problem.
*
* MB: added WHSQL class lib 1/15/2018 - needed for it to run as EXE

* build exe as F:\UTIL\ARIAT\ARIAT_TRK_UCC_IMPORT.EXE
*
* Added GETMOD.PRG 4/17/2018 because TU() was crashing without it in project. Rebuilt EXE.

GMASTEROFFICE='K' && added 1/15/2018 MB to prevent errors in INSERTINTO()

GOFFICE='K'
LOCAL lTestMode
lTestMode = .F.
guserid = "ARIATTRKUCC"

IF NOT lTestMode THEN
	utilsetup("ARIAT_TRK_UCC_IMPORT")
ENDIF

DECLARE INTEGER CreateMutex IN Win32API ;
	STRING @lpMutexAttributes, ;
	STRING bInitialOwner, ;
	STRING @lpName

DECLARE INTEGER GetLastError IN Win32API

* if running in EXE check to see if already running
IF _VFP.STARTMODE > 0 THEN
	lcProcessName = "ARIAT_TRK_UCC_IMPORT"
	CreateMutex(0,CHR(1),@lcProcessName)
	lnError = GetLastError()
	IF lnError = 183
		WAIT WINDOW TIMEOUT 5 "This program is already running..."
		IF NOT lTestMode THEN
			schedupdate()
		ENDIF
		RETURN .F.
	ENDIF
	_screen.WindowState = IIF(lTestMode,2,1)  && added 1/28 to stop this prog from blocking the Task2 screen while producing emails.
ENDIF

loARIAT_TRK_UCC_IMPORT = CREATEOBJECT('ARIAT_TRK_UCC_IMPORT')
loARIAT_TRK_UCC_IMPORT.MAIN( lTestMode )

IF NOT lTestMode THEN
	schedupdate()
ENDIF

CLOSE DATABASES ALL
RETURN

#DEFINE LOGIT 1
#DEFINE WAITIT 2
#DEFINE NOWAITIT 4
#DEFINE SENDIT 8
#DEFINE CRLF CHR(13) + CHR(10)
#DEFINE RETURN_DATA_MANDATORY .F.
#DEFINE RETURN_DATA_NOT_MANDATORY .T.
#DEFINE CSVFORMAT 6
#DEFINE WRITE_ONLY_UNBUFFERED 11
#DEFINE COMMA ","

DEFINE CLASS ARIAT_TRK_UCC_IMPORT AS CUSTOM

	cProcessName = 'ARIATTRKUCC'

	lTestMode = .F.  && .F. FOR NORMAL OPERATION, .T. TO SUPPRESS ALL BUT DEVELOPER EMAILS AND CHANGE INPUT TABLE, ETC.

	dToday = DATE()

	lAutoYield = .T.

	cStartTime = TTOC(DATETIME())

	* connection properties
	nSQLHandle = 0

*!*		* folder properties
*!*		cInputFolder    = 'P:\'  &&  assumes drive P: is mapped to \\dc3\home\lvups
*!*		cArchivedFolder = 'P:\ARCHIVED\'
*!*		cHoldingFolder  = 'P:\HOLDING\'
	
	* this way we do not require any drive mappings on client pcs
	cInputFolder    = '\\dc3\home\lvups\' 
	cArchivedFolder = '\\dc3\home\lvups\ARCHIVED\'
	cHoldingFolder  = '\\dc3\home\lvups\HOLDING\'

	* data properties
	cAddProc = "ARIATTRKUCC"
	cUCCTable = ''

	* processing properties
	nRow = 0
	cDupeUCCMsg = ''

	* Excel properties
	oExcel = NULL

	* wait window properties
	nWaitWindowTimeout = 2
	lWaitWindowIsOn = .T.

	* logfile properties
	lLoggingIsOn = .T.
	cLogFile = 'F:\UTIL\ARIAT\LOGFILES\ARIAT_TRK_UCC_IMPORT_log.txt'

	* INTERNAL email properties
	lSendInternalEmailIsOn = .T.
	cFrom = 'mark.bennett@tollgroup.com'
	cSendTo = 'paul.gaidis@tollgroup.com, mbennett@fmiint.com'
	*cSendTo = 'mbennett@fmiint.com'
	cCC = ''
	cSubject = 'Ariat Tracking #/UCC Import for ' + TRANSFORM(DATETIME())
	cAttach = ''
	cBodyText = ''
	cMGRSendTo = ''
	cMGRFrom = ''
	cMGRSubject = ''
	cMGRCC = ''
	cMGRAttach = ''
	cMGRBodyText = ''

	cCOMPUTERNAME = ''
	cUSERNAME = ''


	FUNCTION INIT
		IF NOT DODEFAULT()
			RETURN .F.
		ENDIF
		WITH THIS
			*SET RESOURCE OFF
			CLOSE DATA
			SET CENTURY ON
			*SET DATE AMERICAN
			*SET HOURS TO 24
			SET DECIMALS TO 2
			SET ANSI ON
			SET TALK OFF
			SET DELETED ON
			SET CONSOLE OFF
			SET EXCLUSIVE OFF
			SET SAFETY OFF
			SET EXACT OFF
			SET STATUS BAR ON
			SET SYSMENU OFF
			SET ENGINEBEHAVIOR 70
			SET MULTILOCKS ON
			_VFP.AUTOYIELD = .lAutoYield
			.cCOMPUTERNAME = GETENV("COMPUTERNAME")
			.cUSERNAME = GETENV("USERNAME")
			IF .lTestMode THEN
				.cLogFile = 'F:\UTIL\ARIAT\LOGFILES\ARIAT_TRK_UCC_IMPORT_log_TESTMODE.txt'
				.cSendTo = 'mbennett@fmiint.com'
			ENDIF
			.lLoggingIsOn = .lLoggingIsOn AND NOT EMPTY(.cLogFile)
			IF .lLoggingIsOn THEN
				SET ALTERNATE TO (.cLogFile) ADDITIVE
				SET ALTERNATE ON
			ENDIF
		ENDWITH
	ENDFUNC


	FUNCTION DESTROY
		WITH THIS
			IF .lLoggingIsOn  THEN
				SET ALTERNATE OFF
				SET ALTERNATE TO
			ENDIF
		ENDWITH
		IF TYPE('.oExcel') = 'O' AND NOT ISNULL(.oExcel) THEN
			.oExcel.QUIT()
		ENDIF
		DODEFAULT()
	ENDFUNC


	FUNCTION MAIN
		LPARAMETERS tlTestMode
		WITH THIS
			LOCAL lnNumberOfErrors, laFilesSorted[1,6], lnNumFiles, lnCurrentFile, lcSourceFile, lcArchivedFile, lcHoldingFile, lnCtr
			LOCAL lcFileName, lnUpdCtr, lcDupeFile, lcCharge, lnDecimal

			PRIVATE m.ACCOUNTID, m.SHIP_REF, m.SERIALNO, m.ASNFILE, m.UCC, m.REFERENCE
			PRIVATE m.ADDBY, m.ADDDT, m.ADDPROC, m.UPDATEBY, m.UPDPROC

			TRY
				lnNumberOfErrors = 0
				
				.lTestMode = tlTestMode

				.cStartTime = TTOC(DATETIME())

				.TrackProgress('Ariat Tracking #/UCC Import process started....', LOGIT+SENDIT)
				.TrackProgress('COMPUTERNAME = ' + .cCOMPUTERNAME, LOGIT+SENDIT)
				.TrackProgress('USERNAME = ' + .cUSERNAME, LOGIT+SENDIT)
				.TrackProgress('PROJECT = ARIAT_TRK_UCC_IMPORT', LOGIT+SENDIT)
				IF .lTestMode THEN
					.TrackProgress('                        TEST MODE                        ', LOGIT+SENDIT)
				ENDIF
				.TrackProgress('.cInputFolder = ' + .cInputFolder, LOGIT+SENDIT)
				.TrackProgress('.cArchivedFolder = ' + .cArchivedFolder, LOGIT+SENDIT)
				.TrackProgress('.cHoldingFolder = ' + .cHoldingFolder, LOGIT+SENDIT)

				
				IF .lTestMode THEN
					.cUCCTable = 'F:\UTIL\ARIAT\DATA\CTNUCC.DBF'
					.cSendTo = 'mbennett@fmiint.com'
				else
					useca("ctnucc","wh")
					.cucctable="UCCCTN SQL"
				ENDIF
				
				.TrackProgress('.cUCCTable = ' + .cUCCTable, LOGIT+SENDIT)

				lnNumFiles = ADIR(laFiles,(.cInputFolder + "AriatExport*.CSV"))

				IF lnNumFiles > 0 THEN
				
					* sort file list by date/time
					.SortArrayByDateTime(@laFiles, @laFilesSorted)

					FOR lnCurrentFile = 1 TO lnNumFiles
					
						IF USED('CURALLDUPEUCC') THEN
							USE IN CURALLDUPEUCC
						ENDIF
						CREATE CURSOR CURALLDUPEUCC (UCC C(20), CNT I, ASNFILE C(50))
						
						IF USED('CURTRKUCC') THEN
							USE IN CURTRKUCC
						ENDIF
						
						* create an empty temp cursor to load the csv file into...
						SELECT * FROM F:\UTIL\ARIAT\TRKUCCCSV ;
						INTO CURSOR CURTRKUCC ;
						WHERE .F. ;
						READWRITE
						

						lcSourceFile = .cInputFolder + laFilesSorted[lnCurrentFile,1]
						lcArchivedFile = .cArchivedFolder + laFilesSorted[lnCurrentFile,1]
						lcHoldingFile = .cHoldingFolder + laFilesSorted[lnCurrentFile,1]

						.TrackProgress('lcSourceFile = ' + lcSourceFile,LOGIT+SENDIT)
						.TrackProgress('lcArchivedFile = ' + lcArchivedFile,LOGIT+SENDIT)
						.TrackProgress('lcHoldingFile = ' + lcHoldingFile,LOGIT+SENDIT)

						
						.TrackProgress('Processing file: ' + lcSourceFile,LOGIT+SENDIT)

*!*							* move source file into Holding folder so that, if an error causes the program to abort,
*!*							* the folderscan process does not keep reprocessing the bad source file in an infinite loop.

						* if the file already exists in the Holding folder, make sure it is not read-only.
						llArchived = FILE(lcHoldingFile)
						IF llArchived THEN
							RUN ATTRIB -R &lcHoldingFile.
							.TrackProgress('   Used ATTRIB -R for re-archiving : ' + lcHoldingFile,LOGIT+SENDIT)
						ENDIF

						* copy the source file to the holding folder for processing
						COPY FILE (lcSourceFile) TO (lcHoldingFile)



						**************************************************************************************
						**************************************************************************************
						**************************************************************************************
						* then process from the file once it is in the Holding folder
												
						STORE .cAddProc TO m.ADDPROC
						STORE .cUSERNAME TO m.ADDBY

						STORE "" TO m.UPDPROC, m.UPDATEBY
						STORE {} TO m.UPDATEDT
					
						m.ACCOUNTID = 6532
						m.ASNFILE = lcSourceFile

						SELECT CURTRKUCC
						APPEND FROM (lcHoldingFile) CSV
						
						*****************************************
						* check for duplicate UCC #s - Paul says there should be none, but if they incorrectly scan multiple cartons with the same UCC #,
						* that will cause them.
						IF USED('CURDUPEUCC') THEN
							USE IN CURDUPEUCC
						ENDIF
						
						SELECT UCC, COUNT(*) AS CNT, SPACE(50) AS ASNFILE ;
						FROM CURTRKUCC ;
						INTO CURSOR CURDUPEUCC ;
						GROUP BY UCC ;
						ORDER BY UCC ;
						HAVING CNT > 1 ;
						READWRITE
						
						IF USED('CURDUPEUCC') AND NOT EOF('CURDUPEUCC') THEN
							SELECT CURDUPEUCC
							REPLACE ALL ASNFILE WITH lcSourceFile IN CURDUPEUCC
							*BROWSE
							SELECT CURALLDUPEUCC
							APPEND FROM DBF('CURDUPEUCC') 
						ENDIF
						
						
						* for now, we think we are getting charges already rolled up by ship_ref - if they are processing the cartons
						* correctly, so disable the logic below. MB 12/3/2014.
						*!*	********************************************* 12/3/2014
						*!*	* Per PG, we need to roll up the charges by pick tickets (ship_ref). 
						*!*	* We'll replace all charges per pick ticket in the main cursor with the total.
						*!*	IF USED('CURSHIPREF') THEN
						*!*		USE IN CURSHIPREF
						*!*	ENDIF

						*!*	* sum charge by ship_ref where there are more than one record with the same ship_ref
						*!*	SELECT SHIP_REF, SUM(CHARGE) AS TOTCHARGE, COUNT(*) AS CNT ;
						*!*	FROM CURTRKUCC ;
						*!*	INTO CURSOR CURSHIPREF ;
						*!*	WHERE (NOT EMPTY(UCC)) AND (NOT EMPTY(SHIP_REF)) ;
						*!*	GROUP BY SHIP_REF ;
						*!*	ORDER BY SHIP_REF ;
						*!*	HAVING CNT > 1 

						*!*	IF USED('CURSHIPREF') AND NOT EOF('CURSHIPREF') THEN
						*!*		SELECT CURSHIPREF
						*!*		SCAN
						*!*			SELECT CURTRKUCC
						*!*			SCAN FOR SHIP_REF == CURSHIPREF.SHIP_REF
						*!*				REPLACE CURTRKUCC.CHARGE WITH CURSHIPREF.TOTCHARGE IN CURTRKUCC
						*!*			ENDSCAN						
						*!*		ENDSCAN
						*!*	ENDIF
						********************************************* 12/3/2014
						
						
						lnCtr = 0
						lnUpdCtr = 0
						
						SELECT CURTRKUCC
						SCAN FOR (NOT EMPTY(UCC)) AND (NOT EMPTY(SHIP_REF))
						
							SCATTER MEMVAR	
							
							
							* Paul wants to store the Charge as string with no decimal in CTNUCC.REFERENCE, so convert it from n (6,2).
							* TRANSFORM() IS REMOVING DECIMAL AND TRAILING ZEROES IN CHARGES LIKE 15.00, WHICH IS NOT WHAT WE WANT. Use STR instead.
							*lcCharge = ALLTRIM(TRANSFORM(CURTRKUCC.CHARGE))  
							lcCharge = ALLTRIM(STR(CURTRKUCC.CHARGE,6,2))
							* cut down to 2 places past the decimal
							lnDecimal = AT(".",lcCharge)
							lcCharge = SUBSTR(lcCharge,1,lnDecimal + 2)  
							* remove the decimal
							m.REFERENCE	= STRTRAN(lcCharge,".","")
							
							xsqlexec("select * from ctnucc where mod='K' and accountid="+transform(m.accountid)+" and ucc='"+padr(m.ucc,20)+"'","xctnucc",,"wh")
							
							* 1/15/2018 MB: found() is not right here since we are not using LOCATE
							*if found() 
							IF USED('xctnucc') AND (NOT EOF('xctnucc')) THEN
								* the record was found in ctnucc
								lnupdctr = lnupdctr + 1
								xsqlexec("update ctnucc set ship_ref='"+m.ship_ref+"', serialno='"+m.serialno+"', asnfile='"+m.asnfile+"', " + ;
									"reference='"+m.reference+"' where ctnuccid="+transform(xctnucc.ctnuccid),,,"wh")
							else
								lnctr = lnctr + 1
								insertinto("ctnucc","wh",.t.)
							endif
							
							.TrackProgress('Processed UCC = ' + m.UCC + '  Serial # = ' + m.SERIALNO + '  REFERENCE = ' + m.REFERENCE + '  SHIP_REF = ' + m.SHIP_REF,LOGIT+SENDIT)
													
						ENDSCAN
						
*!*	SELECT CTNUCC
*!*	BROWSE
						
*!*							**************************************************************************************
*!*							**************************************************************************************
*!*							**************************************************************************************

					
						* tableupate SQL - added 1/15/2018 MB
						IF tu("CTNUCC") THEN
							.TrackProgress('SUCCESS: TU(CTNUCC) returned .T.!',LOGIT+SENDIT)
						ELSE
							* ERROR on table update
							.TrackProgress('ERROR: TU(CTNUCC) returned .F.!',LOGIT+SENDIT)
							=AERROR(UPCERR)
							IF TYPE("UPCERR")#"U" THEN 
								.TrackProgress('Error Description = ' + UPCERR[1,2],LOGIT+SENDIT)
							ENDIF
						ENDIF

						.TrackProgress(TRANSFORM(lnCtr) + ' INSERTS were made.',LOGIT+SENDIT)
						.TrackProgress(TRANSFORM(lnUpdCtr) + ' UPDATES were made.',LOGIT+SENDIT)

						* if the file already exists in the archive folder, make sure it is not read-only.
						* this is to prevent errors we get copying into archive on a resend of an already-archived file.
						* Necessary because the files in the archive folders were sometimes marked as read-only (not sure why)
						llArchived = FILE(lcArchivedFile)
						IF llArchived THEN
							RUN ATTRIB -R &lcArchivedFile.
							.TrackProgress('   Used ATTRIB -R for re-archiving : ' + lcHoldingFile,LOGIT+SENDIT)
							* deleted the archived file 
							DELETE FILE (lcArchivedFile)
						ENDIF						

						* archive the holding file
						COPY FILE (lcHoldingFile) TO (lcArchivedFile)
						
						* delete the holding file
						IF FILE(lcArchivedFile) THEN
							DELETE FILE (lcHoldingFile)
						ENDIF

						WAIT CLEAR

					ENDFOR && lnCurrentFile = 1 TO lnNumFiles
					
					
					IF USED('CURALLDUPEUCC') AND NOT EOF('CURALLDUPEUCC') THEN						
						.cDupeUCCMsg = 'ERROR: duplicate UCC #s were loaded - see attached spreadsheet.'						
						lcDupeFile = 'F:\UTIL\ARIAT\REPORTS\ARIAT_DUPE_UCCS_' + DTOS(DATE()) + '.XLS'
						SELECT CURALLDUPEUCC
						COPY TO (lcDupeFile) XL5
						.cAttach = lcDupeFile						
						.cBodyText = .cDupeUCCMsg + CRLF + CRLF + CRLF + .cBodyText						
					ENDIF
					
					* always delete the source file
					*IF FILE(lcHoldingFile) THEN
					RUN ATTRIB -R &lcSourceFile.
					.TrackProgress('   Used ATTRIB -R for deleting : ' + lcSourceFile,LOGIT+SENDIT)
					DELETE FILE (lcSourceFile)		&&& activate this after testing!!!!
					*ENDIF
					
				ELSE
					.TrackProgress('Found no files in the ARIAT source folder', LOGIT+SENDIT)
					.lSendInternalEmailIsOn = .F.
				ENDIF

				.TrackProgress('Ariat Tracking #/UCC Import process ended normally.', LOGIT+SENDIT)

			CATCH TO loError

				.TrackProgress('There was an error.',LOGIT+SENDIT)
				.TrackProgress('The Error # is: ' + TRANSFORM(loError.ERRORNO), LOGIT+SENDIT)
				.TrackProgress('The Error Message is: ' + TRANSFORM(loError.MESSAGE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred in Program: ' + TRANSFORM(loError.PROCEDURE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred at line #: ' + TRANSFORM(loError.LINENO), LOGIT+SENDIT)
				*.TrackProgress('SPREADSHEET ROW = ' + TRANSFORM(.nRow), LOGIT+SENDIT)
				lnNumberOfErrors = lnNumberOfErrors + 1
				.lSendInternalEmailIsOn = .T.
				IF TYPE('.oExcel') = 'O' AND NOT ISNULL(.oExcel) THEN
					.oExcel.QUIT()
				ENDIF

			ENDTRY

			WAIT CLEAR
			***************** INTERNAL email results ******************************
			.TrackProgress('About to send status email.',LOGIT+SENDIT)
			.TrackProgress('The logfile is: ' + .cLogFile, LOGIT+SENDIT)
			.TrackProgress('Ariat Tracking #/UCC Import process started: ' + .cStartTime, LOGIT+SENDIT)
			.TrackProgress('Ariat Tracking #/UCC Import process finished: ' + TTOC(DATETIME()), LOGIT+SENDIT)

			IF .lSendInternalEmailIsOn THEN
				DO FORM dartmail2 WITH .cSendTo,.cFrom,.cSubject,.cCC,.cAttach,.cBodyText,"A"
				.TrackProgress('Sent status email.',LOGIT)
			ELSE
				.TrackProgress('Did not send status email: THIS.lSendInternalEmailIsOn = FALSE.',LOGIT+SENDIT)
			ENDIF

		ENDWITH
		RETURN
	ENDFUNC && main

* dy 2/23/18
*!*		FUNCTION ExecSQL
*!*			LPARAMETERS tcSQL, tcCursorName, tlNoDataReturnedIsOkay
*!*			LOCAL llRetval, lnResult
*!*			WITH THIS
*!*				* close target cursor if it's open
*!*				IF USED(tcCursorName)
*!*					USE IN (tcCursorName)
*!*				ENDIF
*!*				WAIT WINDOW "Fetching remote data; please wait..." NOWAIT
*!*				lnResult = SQLEXEC(.nSQLHandle, tcSQL, tcCursorName)
*!*				llRetval = ( lnResult > 0 )
*!*				IF llRetval THEN
*!*					* see if any data came back
*!*					IF NOT tlNoDataReturnedIsOkay THEN
*!*						IF (NOT USED(tcCursorName)) OR EOF(tcCursorName) THEN
*!*							llRetval = .F.
*!*							.TrackProgress('No data was returned by this query: ' +CRLF + tcSQL, LOGIT+SENDIT)
*!*							.cSendTo = 'mbennett@fmiint.com'
*!*						ENDIF
*!*					ENDIF
*!*				ELSE
*!*					.TrackProgress("Result Code [" + TRANSFORM(lnResult) + "] returned by this query: " + tcSQL, LOGIT+SENDIT)
*!*					.cSendTo = 'mbennett@fmiint.com'
*!*				ENDIF
*!*				WAIT CLEAR
*!*				RETURN llRetval
*!*			ENDWITH
*!*		ENDFUNC


	PROCEDURE TrackProgress
		* do any combination of Wait Window, writing to logfile, and adding to body of email,
		* based on nFlags parameter.
		LPARAMETERS tcExpression, tnFlags
		WITH THIS
			IF BITAND(tnFlags,LOGIT) = LOGIT THEN
				IF .lLoggingIsOn THEN
					?
					? .cProcessName + "  " + TTOC(DATETIME()) + ": " + tcExpression
				ENDIF
			ENDIF
			IF .lWaitWindowIsOn THEN
				IF BITAND(tnFlags,NOWAITIT) = NOWAITIT THEN
					WAIT WINDOW tcExpression NOWAIT
				ENDIF
				IF BITAND(tnFlags,WAITIT) = WAITIT THEN
					WAIT WINDOW tcExpression TIMEOUT .nWaitWindowTimeout
				ENDIF
			ENDIF
			IF BITAND(tnFlags,SENDIT) = SENDIT THEN
				IF .lSendInternalEmailIsOn THEN
					.cBodyText = .cBodyText + tcExpression + CRLF + CRLF
				ENDIF
			ENDIF
		ENDWITH
	ENDPROC  &&  TrackProgress


	PROCEDURE SortArrayByDateTime
		* expects taInArray[] to be the result of an ADIR()
		LPARAMETERS taInArray, taOutArray
		EXTERNAL ARRAY taInArray, taOutArray
		LOCAL lnRows, lnCols, i, j
		* extending taInArray[] to process the files in date/time order
		lnRows = ALEN(taInArray,1)
		lnCols = ALEN(taInArray,2)
		* create taOutArray with one more column
		DIMENSION taOutArray[lnRows,lnCols + 1]
		* and fill it with taInArray columns AND (filedate + filetime) in new column
		* ( ACOPY() doesn't preserve elements properly because of different # of columns )
		FOR i = 1 TO lnRows
			FOR j = 1 TO lnCols
				taOutArray[i,j] = taInArray[i,j]
			ENDFOR
			taOutArray[i,lnCols + 1] = DTOS(taInArray[i,3]) + "_" + taInArray[i,4]
		ENDFOR
		* sort taOutArray on date/time column
		=ASORT(taOutArray,6)
	ENDPROC

ENDDEFINE
