*!* This is for 315D FILES ONLY!
PARAMETERS nWO_Num,cOffice,nAcctNum,dtTrigTime
PUBLIC c_CntrlNum,c_GrpCntrlNum,cWO_Num,cGroupName,cDivision,cCustName,cFilename,div214,cST_CSZ
PUBLIC tsendto,tcc,cTrigDate,cTrigTime,cProcType,cArrival,cLocation,lNoRecs,nRecTrig,cPrgName
ON ESCAPE CANCEL
cPrgName = "AERO 315"

lTesting = .F.
DO m:\dev\prg\_setvars WITH lTesting
lFilesOut = !lTesting
ON ERROR THROW

cFilename = ""
lIsError = .F.
lNoRecs = .F.
lDoCatch = .T.
nFilenum = 0
cProcType = "D"  && 315A = Arrival, 315D = Delivery
cEventCode = IIF(cProcType="D","035","860")
ASSERT .F. MESSAGE "At start of 315"+cProcType+" Process"

TRY
	cWO_Num = ""
	lEmail = .T.
	lDoError = .F.
	IF VARTYPE(nWO_Num)= "L"
		IF lTesting
			CLOSE DATABASES ALL
			lFilesOut = .F.
			nWO_Num =    1330755
			nAcctNum = 980
			cOffice = "C"
			dtTrigTime = DATETIME()-2400
		ELSE
			WAIT WINDOW "No params specified" TIMEOUT 2
			lDoCatch = .F.
			THROW
		ENDIF
	ENDIF
	cLocName = ICASE(cOffice="C","CA",cOffice="L","ML",cOffice="M","FL","NJ")
	cTrigDate = DTOS(TTOD(dtTrigTime))
	cTrigTime = LEFT(RIGHT(TTOC(dtTrigTime,1),6),4)

	cDivision = IIF(cOffice="C","California",IIF(cOffice="M","Miami","New Jersey"))
	cOffCity = IIF(cOffice="C","SAN PEDRO",IIF(cOffice="M","MIAMI","CARTERET"))
	cOffState = IIF(cOffice="C","CA",IIF(cOffice="M","FL","NJ"))

	xReturn = "XXX"
	DO m:\dev\prg\wf_alt WITH cOffice,nAcctNum
	IF EMPTY(ALLTRIM(xReturn))
		DO errormail WITH "NO WHSE"
		THROW
	ENDIF
	cUseFolder = UPPER(xReturn)

	cWO_Num = ALLTRIM(STR(nWO_Num))

	DIMENSION thisarray(1)

	tfrom = "Toll Operations <toll-edi-ops@tollgroup.com>"
	SELECT 0
	USE F:\3pl\DATA\mailmaster ALIAS mm
	LOCATE FOR mm.edi_type = "MISC" AND mm.taskname = "GENERAL"
	lUseAlt = mm.use_alt
	tsendto = IIF(lUseAlt,sendtoalt,sendto)
	tcc = IIF(lUseAlt,ccalt,cc)
	USE IN mm
	tsendtoerr = tsendto
	tccerr = tcc
	tattach = " "

	IF !lTesting
	SELECT edi_trigger
	nRecTrig = RECNO()
	endif

	cCustName = "AEROPOSTALE"
	cCustFolder = "AEROPOSTALE"
	cCustPrefix = "AP315"+LOWER(cProcType)+"_"+LOWER(cOffice)
	cMailName = PROPER(cCustName)

*!* Open Tables
	cDetailErrors = ""
	IF USED('detail')
		USE IN DETAIL
	ENDIF

	xsqlexec("select * from dellocs",,,"wo")
	index on str(accountid,4)+location tag acct_loc
	set order to

	SELECT 0
	USE F:\WO\WODATA\DETAIL ALIAS DETAIL SHARED NOUPDATE
	LOCATE
	COUNT TO N FOR DETAIL.wo_num = nWO_Num
	IF N = 0
		misswomail()
		DO ediupdate WITH "Missing WO",.T.
		lDoCatch = .F.
		THROW
	ENDIF

	COUNT TO nDet1 FOR DETAIL.wo_num = nWO_Num
	nScanDet1 = 0
	SCAN FOR DETAIL.wo_num = nWO_Num
		IF EMPTY(ALLTRIM(DETAIL.arrival))
			nScanDet1 = nScanDet1+1
			cPOErr = ALLTRIM(DETAIL.po_num)
			cDetailErrors = IIF(EMPTY(cDetailErrors),cPOErr,cDetailErrors+CHR(13)+cPOErr)
		ENDIF
	ENDSCAN
	IF !EMPTY(cDetailErrors)
		IF nDet1 = nScanDet1
			cDetailErrors = cDetailErrors+CHR(13)+"No 315 will be sent for WO# "+TRANSFORM(nWO_Num)
			DO missdetmail WITH cDetailErrors
			lDoCatch = .F.
			DO ediupdate WITH "NO 315 POSSIBLE",.T.
			THROW
		ENDIF
	ENDIF
	SELECT arrival,delloc ;
		FROM DETAIL ;
		WHERE wo_num = nWO_Num ;
		GROUP BY 1 ;
		INTO CURSOR temparrivalsx READWRITE

	SCAN
		cLocation = ALLTRIM(temparrivalsx.delloc)
		cAcctArr = STR(nAcctNum,4)+cLocation
		IF SEEK(cAcctArr,'dellocs','acct_loc')
			IF !INLIST(dellocs.city,"ONTARIO","WALTON","SOUTH RIVER")
				DELETE NEXT 1 IN temparrivalsx
			ENDIF
		ELSE
			ASSERT .F. MESSAGE "LOC not found"
			DO ediupdate WITH "MISS LOC",.T.
			DO errormail WITH "MISSLOC"
			lDoCatch = .F.
			THROW
		ENDIF
	ENDSCAN

	LOCATE
	IF EOF()  && No records left in the temp file
		DO ediupdate WITH "NO RECS",.F.
		lDoCatch = .F.
		THROW
	ENDIF

	SELECT arrival FROM temparrivalsx INTO CURSOR temparrivals
	USE IN temparrivalsx

	SELECT 0
	IF !USED('wolog')
		USE F:\WO\WODATA\wolog ALIAS wolog ORDER TAG wo_num SHARED NOUPDATE
	ENDIF
	IF !SEEK(nWO_Num,'wolog','wo_num')
		ASSERT .F. MESSAGE "WO# not found"
		DO errormail WITH "MISSWO"
		THROW
	ENDIF
	cContainer = IIF(EMPTY(wolog.CONTAINER),ALLTRIM(wolog.awb),ALLTRIM(wolog.CONTAINER))
	IF EMPTY(cContainer)
*!*				ASSERT .F. MESSAGE "In Empty wolog Container"
*!*				IF wolog.TYPE # "D"
*!*					DO errormail WITH "MISSEQUIP"
*!*				ENDIF
*!*				THROW
	ENDIF

	cST_Name = "TGF INC."
	cfd = "*"  && Field delimiter
	csegd = "�"  && Segment delimiter
	cterminator = ">"  && Used at end of ISA segment

	csendqual = "ZZ"  && Sender qualifier
	csendid = "FMIF"  && Sender ID code
	crecqual = "ZZ"  && Recip qualifier, was "08"
	crecid = "OHLGFML"   && Recip ID Code, was "6113310214"
	cDate = DTOS(DATE())
	cTruncDate = RIGHT(cDate,6)
	cTruncTime = SUBSTR(TTOC(DATETIME(),1),9,4)
	cfiledate = cDate+cTruncTime
	csendidlong = PADR(csendid,15," ")
	crecidlong = PADR(crecid,15," ")

	IF lTesting
		cISACode = "T"
	ELSE
		cISACode = "P"
	ENDIF

	SELECT temparrivals
	LOCATE
	SCAN
		cArrival = ALLTRIM(temparrivals.arrival)
		cString = ""

		nSTCount = 0
		nSegCtr = 0
		nLXNum = 1
		STORE "" TO cEquipNum,cHAWB
		dt1 = TTOC(DATETIME(),1)
		dtmail = TTOC(DATETIME())
		dt2 = DATETIME()

		cFilename = ("f:\ftpusers\"+cCustFolder+"\315-Staging\"+cCustPrefix+dt1+".txt")
		cFilenameShort = JUSTFNAME(cFilename)
		cFilename2 = ("f:\ftpusers\"+cCustFolder+"\315OUT\archive\"+cFilenameShort)
		cFilename3 = ("f:\ftpusers\"+cCustFolder+"\315OUT\"+cFilenameShort)
		nFilenum = FCREATE(cFilename)

		DO num_incr_isa
		cISA_Num = PADL(c_CntrlNum,9,"0")
		STORE "ISA"+cfd+"00"+cfd+"          "+cfd+"00"+cfd+"          "+cfd+csendqual+cfd+csendidlong+cfd+;
			crecqual+cfd+crecidlong+cfd+cTruncDate+cfd+cTruncTime+cfd+"U"+cfd+"00401"+cfd+;
			cISA_Num+cfd+"0"+cfd+cISACode+cfd+cterminator+csegd TO cString
		DO cstringbreak

		STORE "GS"+cfd+"QO"+cfd+csendid+cfd+crecid+cfd+cDate+cfd+cTruncTime+cfd+c_CntrlNum+;
			cfd+"X"+cfd+"004010"+csegd TO cString
		DO cstringbreak

		DO num_incr_st
		STORE "ST"+cfd+"315"+cfd+PADL(c_GrpCntrlNum,9,"0")+csegd TO cString
		DO cstringbreak
		nSTCount = nSTCount + 1
		nSegCtr = nSegCtr + 1

		STORE "B4"+cfd+cfd+cfd+"AD"+cfd+cTrigDate+cfd+cTrigTime+csegd TO cString
		DO cstringbreak
		nSegCtr = nSegCtr + 1

		STORE "N9"+cfd+"EQ"+cfd+cArrival+csegd TO cString
*		STORE "N9"+cfd+"EQ"+cfd+cContainer+csegd TO cString
		DO cstringbreak
		nSegCtr = nSegCtr + 1

		STORE "N9"+cfd+"IT"+cfd+"2052"+csegd TO cString  && Changed per Aero mail, 06.17.2010
		DO cstringbreak
		nSegCtr = nSegCtr + 1

		STORE "N9"+cfd+"WO"+cfd+cWO_Num+csegd TO cString
		DO cstringbreak
		nSegCtr = nSegCtr + 1

		STORE "R4"+cfd+"D"+cfd+"D"+cfd+cLocName+csegd TO cString
		DO cstringbreak
		nSegCtr = nSegCtr + 1

		STORE "DTM"+cfd+cEventCode+cfd+cTrigDate+cfd+cTrigTime+cfd+"LT"+csegd TO cString
		DO cstringbreak
		nSegCtr = nSegCtr + 1

*!* Finish processing
		DO close315
		IF nFilenum>0
			=FCLOSE(nFilenum)
		ENDIF

		DO ediupdate WITH "315"+cProcType+" CREATED",.F.

		IF lFilesOut
			SET STEP ON 
			COPY FILE &cFilename TO &cFilename2
			COPY FILE &cFilename TO &cFilename3
			DELETE FILE &cFilename
		ENDIF

		IF !lTesting
			IF !USED("ftpedilog")
				SELECT 0
				USE F:\edirouting\ftpedilog ALIAS ftpedilog
				INSERT INTO ftpedilog (TRANSFER,ftpdate,filename,acct_name,TYPE) VALUES ("214-"+cCustName,dt2,cFilename,UPPER(cCustName),"214")
				USE IN ftpedilog
			ENDIF
		ENDIF

		IF lTesting
			tsubject = "Aeropostale 315"+cProcType+" *TEST* EDI from TGF as of "+dtmail
		ELSE
			tsubject = "Aeropostale 315"+cProcType+" EDI from TGF as of "+dtmail
		ENDIF

		tmessage = "315"+cProcType+" EDI Info from TGF-"+cOffState+", Truck WO# "+;
			cWO_Num+", Arrival "+cArrival+CHR(13)
		tmessage = tmessage +" has been created."+CHR(13)+"(Filename: "+cFilenameShort+")"
		IF lTesting
			tmessage = tmessage + CHR(13)+CHR(13)+"This is a TEST RUN..."
		ENDIF

		IF lEmail
			DO FORM m:\dev\frm\dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
		ENDIF

		WAIT WINDOW cMailName+" 315"+cProcType+" EDI File complete for WO# "+cWO_Num+", Arrv. "+cArrival AT 20,60 TIMEOUT 2
	ENDSCAN

	RELEASE ALL LIKE c_CntrlNum,c_GrpCntrlNum
	RELEASE ALL LIKE nOrigSeq,nOrigGrpSeq

	WAIT CLEAR
	WAIT WINDOW "All "+cMailName+" 315"+cProcType+" EDI File output complete" AT 20,60 TIMEOUT 2

	closefiles()

CATCH TO oErr
	IF lDoCatch
		SET STEP ON 
		lEmail = .T.
		DO ediupdate WITH "ERRHAND ERROR",.T.
		tmessage = cCustName+" Error processing "+CHR(13)

		tmessage =tmessage+ "Try/CATCH Exeception Message:"+CHR(13)+;
			[  Error: ] + STR(oErr.ERRORNO) +CHR(13)+;
			[  LineNo: ] + STR(oErr.LINENO) +CHR(13)+;
			[  Message: ] + oErr.MESSAGE +CHR(13)+;
			[  Procedure: ] + oErr.PROCEDURE +CHR(13)+;
			[  Details: ] + oErr.DETAILS +CHR(13)+;
			[  StackLevel: ] + STR(oErr.STACKLEVEL) +CHR(13)+;
			[  LineContents: ] + oErr.LINECONTENTS+CHR(13)+;
			[  UserValue: ] + oErr.USERVALUE

		tsendto = tsendtoerr
		tcc = tccerr
		tsubject = "315"+cProcType+" EDI Poller Error at "+TTOC(DATETIME())
		tattach  = ""
		tfrom    ="TGF EDI 315"+cProcType+" Poller Operations <toll-edi-ops@tollgroup.com>"
		DO FORM m:\dev\frm\dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
		lEmail = .F.
	ENDIF
FINALLY
	closefiles()
			SELECT edi_trigger
		GO nRecTrig


	ON ERROR
	lTesting = .F.
ENDTRY

&& END OF MAIN CODE SECTION



****************************
PROCEDURE num_incr_isa
****************************

	IF !USED("serfile")
		USE ("F:\3pl\data\serial\"+cCustName+"315_serial") IN 0 ALIAS serfile
	ENDIF
	SELECT serfile
	IF lTesting
		nOrigSeq = serfile.seqnum
		nOrigGrpSeq = serfile.grpseqnum
		lISAFlag = .F.
	ENDIF
	nISA_Num = serfile.seqnum
	c_CntrlNum = ALLTRIM(STR(serfile.seqnum))
	REPLACE serfile.seqnum WITH serfile.seqnum + 1 IN serfile
	SELECT wolog
ENDPROC

****************************
PROCEDURE num_incr_st
****************************

	IF !USED("serfile")
		USE ("F:\3pl\data\serial\"+cCustName+"315_serial") IN 0 ALIAS serfile
	ENDIF
	SELECT serfile
	IF lTesting
		nOrigSeq = serfile.seqnum
		nOrigGrpSeq = serfile.grpseqnum
		lSTFlag = .F.
	ENDIF
	c_GrpCntrlNum = ALLTRIM(STR(serfile.grpseqnum))
	REPLACE serfile.grpseqnum WITH serfile.grpseqnum + 1 IN serfile
	SELECT wolog
ENDPROC

****************************
PROCEDURE close315
****************************
	STORE  "SE"+cfd+ALLTRIM(STR(nSegCtr+1))+cfd+PADL(c_GrpCntrlNum,9,"0")+csegd TO cString
	DO cstringbreak

	STORE  "GE"+cfd+ALLTRIM(STR(nSTCount))+cfd+c_CntrlNum+csegd TO cString
	DO cstringbreak

	STORE  "IEA"+cfd+"1"+cfd+PADL(c_CntrlNum,9,"0")+csegd TO cString
	DO cstringbreak

ENDPROC

****************************
PROCEDURE cstringbreak
****************************
	cLen = LEN(ALLTRIM(cString))
	FPUTS(nFilenum,cString)
ENDPROC

****************************
PROCEDURE errormail
****************************
	PARAMETERS msgtype
	ASSERT .F. MESSAGE "In ERRORMAIL"
	lDoCatch = .F.

	ASSERT .F. MESSAGE "At Errormail...Debug"
	tsubject = "Aeropostale 315 EDI File Error"
	tmessage = "Aeropostale 315 EDI Info, WO# "+cWO_Num+CHR(10)
	tsendto = tsendtoerr
	tcc = tccerr

	DO CASE
		CASE msgtype = "MISSGROUP"
			tmessage = tmessage + "There was no Group Name in WO# "+cWO_Num
			DO ediupdate WITH msgtype,.T.
		CASE msgtype = "MISSWO"
			tmessage = tmessage + "WO# "+cWO_Num+" does not exist in F:\WO\WODATA\WOLOG."
			DO ediupdate WITH msgtype,.T.
		CASE msgtype = "MISSEQUIP"
			tmessage = tmessage + "Equipment not specified for WO# "+cWO_Num+CHR(10)
			tmessage = tmessage + "Data in WOLOG must be corrected."
			DO ediupdate WITH msgtype,.T.
		CASE msgtype = "BAD TRIPID"
			tmessage = tmessage + "Trip ID does not exist in F:\WO\WODATA\WOLOG."
			DO ediupdate WITH msgtype,.T.
		CASE msgtype = "MISSLOC"
			tmessage = tmessage + "Location "+cLocation+" does NOT EXIST IN DELLOCS for Acct. 980"
			DO ediupdate WITH msgtype,.T.
	ENDCASE

	IF lTesting
		tmessage = tmessage + CHR(10)+CHR(10)+"This is TEST DATA only..."
	ENDIF
	IF lEmail
		DO FORM m:\dev\frm\dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
	ENDIF
ENDPROC

**************************
PROCEDURE ediupdate
**************************
	PARAMETERS cFin_status, lIsError
	ASSERT .F. MESSAGE "At EDIUPDATE process"
	IF nFilenum>0
		=FCLOSE(nFilenum)
	ENDIF
	IF cFin_status = "NO RECS"
		lNoRecs = .T.
	ENDIF

	IF !lTesting
		SELECT edi_trigger
		LOCATE
		IF lIsError
			lErrorFlag = IIF(cFin_status = "NO 315 POSSIBLE",.F.,.T.)
			REPLACE edi_trigger.processed WITH .T.,edi_trigger.created WITH .F.,;
				edi_trigger.fin_status WITH cFin_status,edi_trigger.errorflag WITH lErrorFlag ;
				edi_trigger.when_proc WITH DATETIME() ;
				FOR edi_trigger.wo_num = nWO_Num AND edi_type="315"+cProcType

*			xsqlexec("update edi_trigger set processed=1, created=0, fin_status='"+cfin_status+"' " + ;
				"errorflag="+transform(lerrorflag)+", when_proc={"+ttoc(datetime())+"}, " + ;
				"where wo_num="+transform(nwo_num)+" and edi_type='315'"+cproctype,,,"stuff")

			IF FILE(cFilename)
				DELETE FILE &cFilename
			ENDIF
		ELSE
			IF lNoRecs
				REPLACE edi_trigger.processed WITH .T.,edi_trigger.created WITH .F.,proc214 WITH .T.;
					edi_trigger.when_proc WITH DATETIME(),;
					edi_trigger.fin_status WITH "NO 315 NEEDED";
					edi_trigger.errorflag WITH .F.,file214 WITH "" ;
					FOR edi_trigger.wo_num = nWO_Num AND edi_type="315"+cProcType

*				xsqlexec("update edi_trigger set processed=1, created=0, proc214=1, when_proc={"+ttoc(datetime())+"}, " + ; 
					"fin_status='no 315 needed', errorflag=0, file214='' " + ;
					"where wo_num="+transform(nwo_num)+" and edi_type='315'"+cproctype,,,"stuff")

				IF FILE(cFilename)
					DELETE FILE &cFilename
				ENDIF
			ELSE
				REPLACE edi_trigger.processed WITH .T.,edi_trigger.created WITH .T.,proc214 WITH .T.;
					edi_trigger.when_proc WITH DATETIME(),;
					edi_trigger.fin_status WITH "315 CREATED";
					edi_trigger.errorflag WITH .F.,file214 WITH cFilename ;
					FOR edi_trigger.wo_num = nWO_Num AND edi_type="315"+cProcType

*				xsqlexec("update edi_trigger set processed=1, created=1, proc214=1, when_proc={"+ttoc(datetime())+"}, " + ; 
					"fin_status='315 created', errorflag=0, file214='"+cfilename+"' " + ;
					"where wo_num="+transform(nwo_num)+" and edi_type='315'"+cproctype,,,"stuff")

			ENDIF
		ENDIF
	ENDIF

*!* Added 09/18/2014, Joe
	IF !lIsError
		SELECT 0
		USE F:\edirouting\ftpjobs
		INSERT INTO ftpjobs (jobname,USERID,jobtime) VALUES (IIF(lTesting,"315-TEST-OHL","315-OHL"),"tollglobal",DATETIME())
		USE IN ftpjobs
	ENDIF
ENDPROC


*********************
PROCEDURE closefiles
*********************
	IF USED('wolog')
		USE IN wolog
	ENDIF
	IF USED('dellocs')
		USE IN dellocs
	ENDIF
ENDPROC

*********************
PROCEDURE missdetmail
*********************
	PARAMETERS cDetailErrors
	SELECT 0
	USE F:\3pl\DATA\mailmaster ALIAS mm SHARED
	LOCATE FOR edi_type = "MISC" AND taskname = "MISS315INFO"
	lUseAlt = mm.use_alt
	tsendto = IIF(lUseAlt,sendtoalt,sendto)
	tcc = IIF(lUseAlt,ccalt,cc)
	USE IN mm
	tsubject = cCustName+" Detail Error at "+TTOC(DATETIME())
	tattach  = ""
	tmessage = "315"+cProcType+" processing shows missing Detail arrivals in the following PO(s):"
	tmessage = tmessage+CHR(13)+cDetailErrors
	tattach = " "
	IF lEmail
		DO FORM m:\dev\frm\dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
	ENDIF
ENDPROC

*********************
PROCEDURE misswomail
*********************
	SELECT 0
	USE F:\3pl\DATA\mailmaster ALIAS mm SHARED
	LOCATE FOR edi_type = "MISC" AND taskname = "MISS315INFO"
	lUseAlt = mm.use_alt
	tsendto = IIF(lUseAlt,sendtoalt,sendto)
	tcc = IIF(lUseAlt,ccalt,cc)
	USE IN mm
	tsubject = cCustName+" Detail Error at "+TTOC(DATETIME())
	tattach  = ""
	tmessage = "315"+cProcType+" processing shows no Detail records for WO# "+cWO_Num+", please determine cause."
	tattach = " "
	IF lEmail
		DO FORM m:\dev\frm\dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
	ENDIF
ENDPROC
