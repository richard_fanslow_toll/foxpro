*!* m:\dev\prg\agegroup940_process.prg

Wait Window "Default folder is "+lcpath Timeout 2
Cd [&lcpath]

lctranspath    = "f:\ftpusers\"+ccustname+"\940translate\"
lc997path    = Iif(Inlist(coffice,"Y","C"),("f:\ftpusers\"+ccustname+"\997in\"),("f:\ftpusers\"+ccustname+"-nj\997in\"))

logcommentstr = ""
delimchar = cdelimiter
lctranopt= ctranslateoption

lnnum = Adir(tarray,"*.csv")

If lnnum = 0
  Wait Window At 10,10 "      No "+ccustname+" 940s to import     " Timeout 3
  normalexit = .T.
  Return
Endif

Wait Window "At 940 files loop..." Nowait
For thisfile = 1  To lnnum
  cISA_Num = Ttoc(Datetime(),1)

  Wait "" Timeout 2
  xfile = lcpath+Trim(tarray[thisfile,1])
  cfilename = Lower(Trim(tarray[thisfile,1]))
  nfilesize = Val(Transform(tarray(thisfile,2)))
  lrlfile = .F.
  If lrlfile
    Wait Window "This is an RL-type file" Timeout 1
  Endif
*!*    ArchName = IIF(lRLFile,JUSTFNAME(Xfile),JUSTSTEM(Xfile)+TTOC(DATETIME(),1)+JUSTEXT(Xfile))
*!*    transname = IIF(!lRLFile,JUSTFNAME(Xfile),JUSTSTEM(Xfile)+".edi")
  c997name = (lc997path+cfilename)

  If Len(Trim(tarray[thisfile,1]))<14
    archivefile = lcarchivepath+archname
  Else
    archivefile = lcarchivepath+cfilename
  Endif

  holdfile = lcholdpath+cfilename

  If !File(xfile)
    Loop
  Endif

  transfile = lctranspath++Trim(tarray[thisfile,1])
  If !ltesting
*    COPY FILE [&xfile] TO [&Transfile]  && creates 997 file
  Endif

  Copy File [&xfile] To [&archivefile]
  Wait "" Timeout 2

  Wait Window "Importing file: "+cfilename Nowait
  Use F:\3pl\Data\agegroup940csvtemplate.Dbf In 0 Alias aginfile

  Select * From aginfile Where .F. Into Cursor ag940file Readwrite
  Set Date Mdy
  Use In aginfile
***********************************************************************
***** load the EXCEL\CSV data into the template 
  Select ag940file
  Append From [&xfile] Type Csv
  Replace csz With Alltrim(stcity)+", "+Alltrim(ststate)+" "+Allt(stzip) All
  Replace sforcsz With Alltrim(sfcity)+", "+Alltrim(sfstate)+" "+Allt(sfzip) All
  Replace shipins With shipins1 All

***********************************************************************
&& create the xpt and xptdet cursors

  If Used("pt")
    Use In pt
  Endif
  If Used("ptdet")
    Use In ptdet
  Endif
  xsqlexec("select * from pt where .f.","xpt",,"wh")
  Select xpt
  Index On ship_ref Tag ship_ref
  Index On ptid Tag ptid
  Set Order To Tag ship_ref
  xsqlexec("select * from ptdet where .f.","xptdet",,"wh")
  Select xpt

***********************************************************************
  ldoImport = .T.
  Do ("m:\dev\prg\agegroup940_bkdn")

  If !ldoImport
    Loop
  Endif

***********************************************************************
  cISA_Num = Alltrim(Str(dygenpk("ag940isa","whall")))
  SET STEP ON 
  Do m:\dev\prg\all940_import

***********************************************************************
  If !File(archivefile)
    Copy File [&xfile] To [&archivefile]
  Endif

***********************************************************************
  If File(xfile) && AND !ltesting
    Delete File [&xfile]
  Endif

Endfor

If !ltesting
  deletefile(lcarchivepath,20)
Endif

Wait Clear
Wait "Entire "+ccustname+" 940 Process Now Complete" Window Timeout 2
