PARAMETERS cErrorMsg
tsubject = "945 Process Error"
tattach = " "
SELECT 0
USE F:\3pl\DATA\mailmaster ALIAS mm
LOCATE FOR mm.edi_type = "MISC" AND mm.taskname = "GENERAL"
tsendto = IIF(mm.use_alt,sendtoalt,sendto)
tcc = IIF(mm.use_alt,ccalt,cc)
USE IN mm
tmessage = "945 Processing had an error: "+CHR(13)+TRIM(cErrorMsg)+CHR(13)+"EDI_TRIGGER will be reset and retried in 15 minutes."
tfrom = "Toll Operations <toll-edi-ops@tollgroup.com>"
DO FORM m:\dev\frm\dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
SELECT edi_trigger
REPLACE processed WITH .T. NEXT 1
RETURN