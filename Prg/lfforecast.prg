* LFFORECAST.PRG
* programmer: Mark Bennett
* Automate the building of an Assembly Schedule from a forecast spreadsheet supplied by LF.
* For: Chris Malcolm

* ====> this program is no longer used - its code has been move to form LFFCAST 2/15/2017 mb

LOCAL lnError, lcProcessName, loLF_FORECAST, lTestMode

lTestMode = .T.

IF NOT lTestMode THEN
	runack("LF_FORECAST")
ENDIF

DECLARE INTEGER CreateMutex IN Win32API ;
	STRING @lpMutexAttributes, ;
	STRING bInitialOwner, ;
	STRING @lpName

DECLARE INTEGER GetLastError IN Win32API

* if running in EXE check to see if already running
IF _VFP.STARTMODE > 0 THEN
	lcProcessName = "LF_FORECAST"
	CreateMutex(0,CHR(1),@lcProcessName)
	lnError = GetLastError()
	IF lnError = 183
		WAIT WINDOW TIMEOUT 10 "Process is already running..."
		RETURN .F.
	ENDIF
ENDIF

utilsetup("LF_FORECAST")

loLF_FORECAST = CREATEOBJECT('LF_FORECAST')
loLF_FORECAST.MAIN( lTestMode )

IF NOT lTestMode THEN
	schedupdate()
ENDIF

CLOSE DATABASES ALL
RETURN

#DEFINE LOGIT 1
#DEFINE WAITIT 2
#DEFINE NOWAITIT 4
#DEFINE SENDIT 8
#DEFINE CRLF CHR(13) + CHR(10)
#DEFINE LF CHR(10)
#DEFINE RETURN_DATA_MANDATORY .F.
#DEFINE RETURN_DATA_NOT_MANDATORY .T.

DEFINE CLASS LF_FORECAST AS CUSTOM

	cProcessName = 'LF_FORECAST'

	lTestMode = .T.  && .F. FOR NORMAL OPERATION, .T. TO SUPPRESS ALL BUT DEVELOPER EMAILS AND CHANGE INPUT TABLE, ETC.

	lAutoYield = .T.
	cStartTime = TTOC(DATETIME())

	* connection properties
	nSQLHandle = 0

	* file properties
	nFileHandle = 0

	cForecastTable = 'F:\UTIL\LF\FORECAST\DATA\LFFCAST'

	cXLSfolder = 'F:\UTIL\LF\FORECAST\XLS_DOCS\'

	cXLSOUTfolder = 'F:\UTIL\LF\FORECAST\XLS_OUT\'

	* date properties
	dtNow = DATETIME()

	dToday = DATE()

	* processing props
	nMonth = 0
	nYear = 0
	nAccountid = 6034
	oExcel = NULL
	cRow = ''

	* wait window properties
	nWaitWindowTimeout = 2
	lWaitWindowIsOn = .T.

	* logfile properties
	lLoggingIsOn = .T.
	cLogFile = 'F:\UTIL\LF\LOGFILES\LF_FORECAST_LOG.TXT'

	* INTERNAL email properties
	lSendInternalEmailIsOn = .T.
	cFrom = 'mark.bennett@tollgroup.com'
	cSendTo = 'mbennett@fmiint.com'
	cCC = ''
	cSubject = 'LifeFactory Forecast Spreadsheet for: ' + DTOC(DATE())
	cAttach = ''
	cBodyText = ''

	cCOMPUTERNAME = ''
	cUSERNAME = ''

	FUNCTION INIT
		IF NOT DODEFAULT()
			RETURN .F.
		ENDIF
		WITH THIS
			CLEAR
			*SET RESOURCE OFF
			CLOSE DATA
			SET CENTURY ON
			SET DATE AMERICAN
			SET HOURS TO 24
			SET ANSI ON
			SET TALK OFF
			SET DELETED ON
			SET CONSOLE OFF
			SET EXCLUSIVE OFF
			SET SAFETY OFF
			SET EXACT OFF
			SET STATUS BAR ON
			SET SYSMENU OFF
			SET ENGINEBEHAVIOR 70
			_VFP.AUTOYIELD = .lAutoYield
			.cCOMPUTERNAME = GETENV("COMPUTERNAME")
			.cUSERNAME = GETENV("USERNAME")
			IF .lTestMode THEN
				.cSendTo = 'mbennett@fmiint.com'
				.cLogFile = 'F:\UTIL\LF\Logfiles\LF_FORECAST_log_TESTMODE.txt'
			ENDIF
			.lLoggingIsOn = .lLoggingIsOn AND NOT EMPTY(.cLogFile)
			IF .lLoggingIsOn THEN
				SET ALTERNATE TO (.cLogFile) ADDITIVE
				SET ALTERNATE ON
			ENDIF
		ENDWITH
	ENDFUNC


	FUNCTION DESTROY
		WITH THIS
			IF .lLoggingIsOn  THEN
				SET ALTERNATE OFF
				SET ALTERNATE TO
			ENDIF
		ENDWITH
		DODEFAULT()
	ENDFUNC


	FUNCTION MAIN
		LPARAMETERS tlTestMode

		WITH THIS

			.lTestMode = tlTestMode

			LOCAL lcXLSfolder, lnNumberOfErrors, lcXLSFile

			lnNumberOfErrors = 0

			TRY
				.TrackProgress('LF Forecast Assembly Process started....', LOGIT+SENDIT+NOWAITIT)
				.TrackProgress('COMPUTERNAME = ' + .cCOMPUTERNAME, LOGIT+SENDIT)
				.TrackProgress('USERNAME = ' + .cUSERNAME, LOGIT+SENDIT)
				.TrackProgress('PROJECT = LFFORECAST', LOGIT+SENDIT)				
			
				PUBLIC guserid
				guserid = "MARK"
								
				USE (.cForecastTable) SHARED IN 0 ALIAS LFFCAST

				* eventually we will allow user to define these
				.nMonth = MONTH(.dToday)
				.nYear = YEAR(.dToday)
				
				* force for testing
				.nMonth = 12
				.nYear = 2016

				lcXLSFile = .cXLSOUTfolder + 'LFFORECAST_' + TRANSFORM(.nMonth) + "-" + TRANSFORM(.nYear) + '.XLS'

*!*					.CreateStyleSummary() 

*!*					.CreateAssembly() 

*!*					.CreatePTNeed() 

*!*					.CreateMaterialBuild() 

*!*					IF .LoadForecastXLS() THEN  
*!*						SELECT LFFCAST
*!*						BROWSE							
*!*					ENDIF
*!*					
*!*					.UpdateTGFINVcolumn()

*!*					.UpdateINASScolumn()
*!*						
*!*					.UpdateINVPASScolumn()
*!*						
*!*					.UpdatePTNEEDcolumn()
*!*						
*!*					.UpdatePTSHORTcolumn()
*!*						
*!*					.UpdateIALPNcolumn()
*!*						
*!*					.UpdateTRFCcolumn()
*!*						
*!*					.UpdateTRTGcolumn()
				
				
				*.UpdateMPASScolumn()
				
				.UpdateMULTIPLEcolumn()
				
				*.UpdateBOTTLEScolumn()  && must call after FORASS column is entered manually
				
				SELECT LFFCAST
				COPY TO (lcXLSFile) XL5 FOR (MONTH = .nMonth) AND (YEAR = .nYear)
										


				.TrackProgress('LF Forecast Assembly Process ended normally.', LOGIT+SENDIT)

			CATCH TO loError

				.TrackProgress('There was an error.',LOGIT+SENDIT)
				.TrackProgress('The Error # is: ' + TRANSFORM(loError.ERRORNO), LOGIT+SENDIT)
				.TrackProgress('The Error Message is: ' + TRANSFORM(loError.MESSAGE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred in Program: ' + TRANSFORM(loError.PROCEDURE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred at line #: ' + TRANSFORM(loError.LINENO), LOGIT+SENDIT)
				
				.TrackProgress('.cRow = ' + .cRow, LOGIT+SENDIT)
				
				lnNumberOfErrors = lnNumberOfErrors + 1
				IF TYPE('.oExcel') = 'O' AND NOT ISNULL(.oExcel) THEN
					.oExcel.QUIT()
				ENDIF

			ENDTRY

			CLOSE DATA
			WAIT CLEAR
			***************** INTERNAL email results ******************************
			.TrackProgress('About to send status email.',LOGIT)
			.TrackProgress('==================================================================================================================', SENDIT)
			.TrackProgress('The logfile is: ' + .cLogFile, SENDIT)
			.TrackProgress('Qualcomm Driver Punch Import process started: ' + .cStartTime, LOGIT+SENDIT)
			.TrackProgress('Qualcomm Driver Punch Import process finished: ' + TTOC(DATETIME()), LOGIT+SENDIT)

			IF .lSendInternalEmailIsOn THEN
				* try to trap error from not having dartmail dll's registered on user's pc...
				TRY
					DO FORM dartmail2 WITH .cSendTo,.cFrom,.cSubject,.cCC,.cAttach,.cBodyText,"A"
					.TrackProgress('Sent status email.',LOGIT)
				CATCH TO loError
					.TrackProgress('There was an error sending the status email.',LOGIT)
					.TrackProgress('The Error # is: ' + TRANSFORM(loError.ERRORNO), LOGIT)
					.TrackProgress('The Error Message is: ' + TRANSFORM(loError.MESSAGE), LOGIT)
					.TrackProgress('The Error occurred in Program: ' + TRANSFORM(loError.PROCEDURE), LOGIT)
					.TrackProgress('The Error occurred at line #: ' + TRANSFORM(loError.LINENO), LOGIT)
					lnNumberOfErrors = lnNumberOfErrors + 1
				ENDTRY

			ELSE
				.TrackProgress('Did not send status email: THIS.lSendInternalEmailIsOn = FALSE.',LOGIT)
			ENDIF

		ENDWITH
		RETURN
	ENDFUNC && main
	
	
	PROCEDURE UpdateMPASScolumn
		WITH THIS

			.TrackProgress('Updating MPASS Column....', LOGIT+SENDIT+NOWAITIT)
			* Max Potential Assembly    Material Builds - put into excel, vlookup by SKU to fill in Qty 
			
			LOCAL lnMonth, lnYear			

			lnMonth = .nMonth
			lnYear = .nYear
			
			SELECT LFFCAST
			SCAN FOR (MONTH = lnMonth) AND (YEAR = lnYear)
				SELECT CURMATERIALBUILD
				LOCATE FOR ALLTRIM(STYLE) == ALLTRIM(LFFCAST.SKU)
				IF FOUND() THEN
					REPLACE LFFCAST.MPASS WITH CURMATERIALBUILD.TOTAVAILQTY IN LFFCAST
				ENDIF	
			ENDSCAN

		ENDWITH
		RETURN
	ENDPROC  &&  UpdateMPASScolumn
	
	
	PROCEDURE UpdateBOTTLEScolumn
		WITH THIS

			.TrackProgress('Updating BOTTLES Column....', LOGIT+SENDIT+NOWAITIT)
			* UOM multiplied by For Assembly Qty - only if a bottle   
			
			* NOTE: FORASS column data is entered manually, so we must be able to call this method on demand.
			
			LOCAL lnMonth, lnYear			

			lnMonth = .nMonth
			lnYear = .nYear
			
			SELECT LFFCAST
			SCAN FOR (MONTH = lnMonth) AND (YEAR = lnYear) AND ('BOTTLE' $ UPPER(DESCRIPT))				
				REPLACE LFFCAST.BOTTLES WITH (LFFCAST.UOM * LFFCAST.FORASS) IN LFFCAST
			ENDSCAN

		ENDWITH
		RETURN
	ENDPROC  &&  UpdateBOTTLEScolumn
	
	
	PROCEDURE UpdateMULTIPLEcolumn
		WITH THIS

			.TrackProgress('Updating MULTIPLE Column....', LOGIT+SENDIT+NOWAITIT)
			*Multiple Units per pallet - this is constant (unless they make a change in the SKU's) -  from UOM column of Style Master?
			
			LOCAL lnMonth, lnYear			

			lnMonth = .nMonth
			lnYear = .nYear
			
			xsqlexec("select accountid, style, uom from upcmast where accountid = 6034","lfupcmast",,"wh")
			* activate below when there is data in UIC field
			*xsqlexec("select style, uom from upcmast where accountid = 6034 and Uom > 0","lfupcmast",,"wh")
			
			SELECT LFFCAST
			SCAN FOR (MONTH = lnMonth) AND (YEAR = lnYear)
				SELECT lfupcmast
				LOCATE FOR ALLTRIM(STYLE) == ALLTRIM(LFFCAST.SKU)
				IF FOUND() THEN
					REPLACE LFFCAST.MULTIPLE WITH lfupcmast.UOM IN LFFCAST
				ENDIF	
			ENDSCAN

		ENDWITH
		RETURN
	ENDPROC  &&  UpdateMULTIPLEcolumn
	
	
	PROCEDURE UpdateTRTGcolumn
		WITH THIS

			.TrackProgress('Updating TO REACH TARGET Column....', LOGIT+SENDIT+NOWAITIT)
			*To Reach Target        Selected Month Target minus Inv+Assembly
			
			LOCAL lnMonth, lnYear			

			lnMonth = .nMonth
			lnYear = .nYear
			
			SELECT LFFCAST
			SCAN FOR (MONTH = lnMonth) AND (YEAR = lnYear)
				REPLACE LFFCAST.TRTG WITH (LFFCAST.TARGCURR - LFFCAST.INVPASS) IN LFFCAST
			ENDSCAN

		ENDWITH
		RETURN
	ENDPROC  &&  UpdateTRTGcolumn
	
	
	PROCEDURE UpdateTRFCcolumn
		WITH THIS

			.TrackProgress('Updating TO REACH FORECAST Column....', LOGIT+SENDIT+NOWAITIT)
			*To Reach Forecast: Selected Month Forecast minus Inv+Assembly
			
			LOCAL lnMonth, lnYear			

			lnMonth = .nMonth
			lnYear = .nYear
			
			SELECT LFFCAST
			SCAN FOR (MONTH = lnMonth) AND (YEAR = lnYear)
				REPLACE LFFCAST.TRFC WITH (LFFCAST.FORECURR - LFFCAST.INVPASS) IN LFFCAST
			ENDSCAN

		ENDWITH
		RETURN
	ENDPROC  &&  UpdateTRFCcolumn


	PROCEDURE CreatePTNeed
		WITH THIS
			.TrackProgress('Creating PT NEED table....', LOGIT+SENDIT+NOWAITIT)
			* based on code in form PICKTICK; code in Check Inventory button calls ZZINVCHECK which seems to have the code we will need.
			* we will update field PTNEED in the forecast table from the report cursor.
			LOCAL lcXLSfolder, lcSXLSFile
			lcXLSfolder = .cXLSfolder
			lcXLSFile = lcXLSfolder + 'PTNEED_' + DTOS(.dToday) + '.XLS'

			xsqlexec("select * from invenloc where mod='I' and accountid=6034",,,"wh")
			index on str(accountid,4)+trans(units)+style+color+id+pack tag match
			set order to

			xsqlexec("select * from pt where mod='I' and accountid = 6034","pt",,"wh")
			xsqlexec("select * from ptdet where mod='I' and accountid = 6034","vptdet",,"wh")
			

			****************************************************************************************************
			****************************************************************************************************
			* code from zzdataenv...
			****************************************************************************************************
			****************************************************************************************************


			SELECT ptid, accountid, combgroup, ptdate, consignee, ship_ref, combpt, ship_via, cnee_ref, goh, qty, origqty, weight, ;
				cuft, NAME, address, address2, csz, storenum, dept,	START, CANCEL, cacctnum, eaches, duns, salesrep, div, dcnum, ;
				vendor_num, batch_num, terms, upsacctnum, scac, shipins, rout_desc, rout_code, shipfor, sforstore, sforaddr1, ;
				sforaddr2, sforcsz, invcheck, finis, combinedpt, ptprinted, residential, ;
				expshipdt, web, COMMENT, carrcode, CONTAINER, specinst, b2, country, vas, ;
				v01, v02, v03, v04, v05, v06, v07, v08, v09, v10, v11, v12, v13, v14, v15, v16, v17, v18, v19, v20, v21, v22, v23, v24, v25, v26, v27, v28, v29, v30, v31, v32, v33, ;
				updateby, updatedt, addby, adddt, ;
				SPACE(5) AS zip, "  " AS state, .T. AS MARK, .F. AS noinventory, .F. AS nomarkstyle, ;
				.F. AS units, .F. AS fullcase, .F. AS unslot, .F. AS completebo, .F. AS updatemark, .F. AS addmark ;
				FROM pt INTO CURSOR vpt READWRITE

			*!*	userv("vptdet","whall")

			SELECT vpt
			INDEX ON ptid TAG ptid
			INDEX ON STR(accountid,4)+ship_ref TAG acctpt
			INDEX ON batch_num TAG batch_num
			INDEX ON vendor_num TAG vendor_num
			INDEX ON combgroup TAG combgroup
			INDEX ON TRANSFORM(MARK)+ship_ref TAG MARK
			INDEX ON consignee TAG consignee
			INDEX ON NAME TAG NAME
			INDEX ON ship_via TAG ship_via

			INDEX ON cnee_ref TAG cnee_ref

			INDEX ON qty TAG qty
			INDEX ON START TAG START
			INDEX ON CANCEL TAG CANCEL
			INDEX ON csz TAG csz
			INDEX ON ptdate TAG ptdate
			INDEX ON state TAG state
			INDEX ON CONTAINER TAG CONTAINER
			INDEX ON ship_ref TAG ship_ref

			SELECT vptdet

			INDEX ON ptid TAG ptid
			INDEX ON ptdetid TAG ptdetid
			INDEX ON STYLE TAG STYLE
			INDEX ON COLOR TAG COLOR
			INDEX ON ID TAG ID
			INDEX ON STYLE+COLOR+ID TAG stylecolid
			INDEX ON PACK TAG PACK
			
			****************************************************************************************************
			****************************************************************************************************
			* code from ZZINVCHECK
			****************************************************************************************************
			****************************************************************************************************

			gvisitor = .F.
			xunmark = .F.
			xinven="inven"

			CREATE CURSOR xrpt (units l, STYLE c(20), COLOR c(10), ID c(10), PACK c(10), ;
				custsku c(12), DESC c(35), needqty N(6), onhandqty N(6), shortqty N(6), onholdqty N(6))

			INDEX ON TRANS(units)+STYLE+COLOR+ID+PACK TAG match

			m.shortqty=0
			tt=0

			*!*	thisform.zzremoveorderandfilter(11)

			SELECT vpt
			SCAN FOR MARK
				tt=tt+1
				IF tt/10=INT(tt/10)
					WAIT WINDOW "Inventory Check: Pick Ticket #"+TRANS(tt) NOWAIT
				ENDIF

				xptid=vpt.ptid
				xcuft=0
				xweight=0

				SELECT vptdet
				SCAN FOR ptid=xptid
					SCATTER MEMVAR MEMO
					m.needqty=m.totqty

					IF SEEK(TRANS(m.units)+m.style+m.color+m.id+m.pack,"xrpt","match")
						IF xunmark AND xrpt.needqty+m.needqty>xrpt.onhandqty
							REPLACE MARK WITH .F. IN vpt
							*THISFORM.grdpt.REFRESH
							*MESSAGEBOX("PT "+ALLTRIM(vpt.ship_ref)+" has been unmarked.",16,"Unmark")
							*EXIT
						ENDIF

						REPLACE needqty WITH xrpt.needqty+m.needqty IN xrpt
					ELSE
						m.onhandqty=0
						IF SEEK(STR(vpt.accountid,4)+TRANS(m.units)+m.style+m.color+m.id+m.pack,xinven,"match")
							m.onhandqty=&xinven..availqty
						ENDIF

						IF xunmark AND m.needqty>m.onhandqty
							REPLACE MARK WITH .F. IN vpt
							*MESSAGEBOX("PT "+ALLTRIM(vpt.ship_ref)+" has been unmarked.",16,"Unmark")
							*EXIT
						ENDIF

						INSERT INTO xrpt FROM MEMVAR
					ENDIF

					IF !gvisitor AND SEEK(STR(vpt.accountid,4)+TRANS(m.units)+m.style+m.color+m.id+m.pack,xinven,"match")
						xcuft=xcuft+&xinven..ctncube*vptdet.totqty
						xweight=xweight+&xinven..ctnwt*vptdet.totqty

						REPLACE LENGTH WITH &xinven..LENGTH, WIDTH WITH &xinven..WIDTH, depth WITH &xinven..depth, ;
							ctnwt WITH &xinven..ctnwt, ctncube WITH &xinven..ctncube IN vptdet
					ENDIF
				ENDSCAN

				IF !gvisitor
					REPLACE cuft WITH xcuft, weight WITH xweight, updatemark WITH .T. IN vpt
				ENDIF
			ENDSCAN

			SELECT xrpt
			LOCATE FOR needqty>onhandqty
			IF FOUND()
				REPLACE shortqty WITH MIN(needqty-onhandqty,needqty) FOR needqty>onhandqty IN xrpt
				SELECT vpt
				LOCATE FOR ptid=xptid

				* fill in onholdqty field for styles that are short

				SELECT xrpt
				SCAN FOR shortqty>0
					IF SEEK(STR(accountid,4)+TRANSFORM(units)+STYLE+COLOR+ID+PACK,"inven","match")
						REPLACE onholdqty WITH inven.holdqty IN xrpt
					ENDIF
				ENDSCAN

			ENDIF
			
			SELECT * FROM XRPT ;
			INTO CURSOR CURPTNEED ;
			ORDER BY STYLE, COLOR ;
			READWRITE

			* save the xls file for documentation...
			SELECT CURPTNEED
			COPY TO (lcXLSFile) XL5
		ENDWITH
	ENDPROC  && CreatePTNeed



	PROCEDURE CreateAssembly
		WITH THIS
			.TrackProgress('Creating Assembly table....', LOGIT+SENDIT+NOWAITIT)
			* based on form lifebuild; we will uopdate field INASS in the forecast table
			LOCAL lcXLSfolder, lcSXLSFile
			lcXLSfolder = .cXLSfolder
			lcXLSFile = lcXLSfolder + 'ASSEMBLY_' + DTOS(.dToday) + '.XLS'

			USE F:\WHI\WHDATA\outwolog IN 0 SHARED

			xsqlexec("select projectid, projno, custrefno, wo_num, putwonum from project where accountid=6034 and completeddt={}","xtemp",,"wh")

			SELECT *, SPACE(254) AS jobdesc, SPACE(10) AS STYLE, 000000 AS totqty FROM xtemp WHERE .F. INTO CURSOR CURASSEMBLY READWRITE

			SELECT xtemp
			SCAN
				xsqlexec("select * from projdet where projectid="+TRANSFORM(xtemp.projectid),"projdet",,"wh",,,,.T.)

				SCATTER MEMVAR
				m.jobdesc=""
				SELECT projdet
				SCAN FOR projectid=xtemp.projectid
					IF !EMPTY(m.jobdesc)
						m.jobdesc=m.jobdesc+crlf
					ENDIF
					m.jobdesc=m.jobdesc+TRIM(projdet.DESCRIPTION)
				ENDSCAN

				IF !EMPTY(m.putwonum)
					xsqlexec("select * from pl where wo_num="+TRANSFORM(m.putwonum),,,"wh")

					SELECT pl
					SCAN FOR wo_num=m.putwonum
						m.style=STYLE
						m.totqty=totqty
						INSERT INTO CURASSEMBLY FROM MEMVAR
					ENDSCAN
				ELSE
					=SEEK(m.wo_num,"outwolog","wo_num")
					xsqlexec("select * from outcomp where outwologid="+TRANSFORM(outwolog.outwologid),"outcomp",,"wh")
					SCAN
						m.style=STYLE
						m.totqty=qty
						INSERT INTO CURASSEMBLY FROM MEMVAR
					ENDSCAN
				ENDIF
			ENDSCAN

			*SELECT CURASSEMBLY
			*BROW

			* save the xls file for documentation...
			SELECT CURASSEMBLY
			COPY TO (lcXLSFile) XL5
		ENDWITH
	ENDPROC  && CreateAssembly
	
	
	PROCEDURE UpdateIALPNcolumn
		WITH THIS
		
			.TrackProgress('Updating INV + ASS LESS PT NEED Column....', LOGIT+SENDIT+NOWAITIT)
			* Inv + Ass Less PT Need:          Column I minus J  (Inv+Assembly minus PT Need)
			
			LOCAL lnMonth, lnYear			

			lnMonth = .nMonth
			lnYear = .nYear
			
			SELECT LFFCAST
			SCAN FOR (MONTH = lnMonth) AND (YEAR = lnYear)
				REPLACE LFFCAST.IALPN WITH (LFFCAST.INVPASS - LFFCAST.PTNEED) IN LFFCAST
			ENDSCAN

		ENDWITH
		RETURN
	ENDPROC  &&  UpdateIALPNcolumn
	
	
	PROCEDURE UpdateINVPASScolumn
		WITH THIS
		
			.TrackProgress('Updating INVENTORY PLUS ASSEMBLY Column....', LOGIT+SENDIT+NOWAITIT)
			
			LOCAL lnMonth, lnYear			

			lnMonth = .nMonth
			lnYear = .nYear
			
			SELECT LFFCAST
			SCAN FOR (MONTH = lnMonth) AND (YEAR = lnYear)
				REPLACE LFFCAST.INVPASS WITH (LFFCAST.TGFINV + LFFCAST.INASS) IN LFFCAST
			ENDSCAN

		ENDWITH
		RETURN
	ENDPROC  &&  UpdateINVPASScolumn

		
	PROCEDURE UpdateTGFINVcolumn
		WITH THIS
		
			.TrackProgress('Updating INVENTORY Column....', LOGIT+SENDIT+NOWAITIT)
			
			LOCAL lnMonth, lnYear
			
			*** now get data to populate TGF Inventory column - tgfinv - from cursor CURSTYLESUMMARY - which should still be open from when it was created
			** also:
			*!*		Only Styles that start with a Number and the Qty is the TotQty, not Available Qty.
			*!*	Also ignoring any styles with "-EU" and "-DMG"

			lnMonth = .nMonth
			lnYear = .nYear
			
			SELECT LFFCAST
			SCAN FOR (MONTH = lnMonth) AND (YEAR = lnYear)
				SELECT CURSTYLESUMMARY
				LOCATE FOR ALLTRIM(STYLE) == ALLTRIM(LFFCAST.SKU)
				IF FOUND() THEN
					REPLACE LFFCAST.TGFINV WITH CURSTYLESUMMARY.TOTQTY IN LFFCAST
				ENDIF	
			ENDSCAN

		ENDWITH
		RETURN
	ENDPROC  &&  UpdateTGFINVcolumn

	
	
	PROCEDURE UpdateINASScolumn
		WITH THIS
		
			.TrackProgress('Updating IN ASSEMBLY Column....', LOGIT+SENDIT+NOWAITIT)
			
			LOCAL lnMonth, lnYear
			
			*** now get data to populate TGF Inventory column - tgfinv - from cursor CURSTYLESUMMARY - which should still be open from when it was created
			** also:
			*!*		Only Styles that start with a Number and the Qty is the TotQty, not Available Qty.
			*!*	Also ignoring any styles with "-EU" and "-DMG"

			lnMonth = .nMonth
			lnYear = .nYear
			
			SELECT LFFCAST
			SCAN FOR (MONTH = lnMonth) AND (YEAR = lnYear)
				SELECT CURASSEMBLY
				LOCATE FOR ALLTRIM(STYLE) == ALLTRIM(LFFCAST.SKU)
				IF FOUND() THEN
					REPLACE LFFCAST.INASS WITH CURASSEMBLY.TOTQTY IN LFFCAST
				ENDIF	
			ENDSCAN

		ENDWITH
		RETURN
	ENDPROC  &&  UpdateINASScolumn
	
	
	PROCEDURE UpdatePTNEEDcolumn
		WITH THIS
		
			.TrackProgress('Updating PTNEED Column....', LOGIT+SENDIT+NOWAITIT)
			
			LOCAL lnMonth, lnYear

			lnMonth = .nMonth
			lnYear = .nYear
			
			SELECT LFFCAST
			SCAN FOR (MONTH = lnMonth) AND (YEAR = lnYear)
				SELECT CURPTNEED
				LOCATE FOR ALLTRIM(STYLE) == ALLTRIM(LFFCAST.SKU)
				IF FOUND() THEN
					REPLACE LFFCAST.PTNEED WITH CURPTNEED.NEEDQTY IN LFFCAST
				ENDIF	
			ENDSCAN

		ENDWITH
		RETURN
	ENDPROC  &&  UpdatePTNEEDcolumn
	
	
	PROCEDURE UpdatePTSHORTcolumn
		WITH THIS
		
			.TrackProgress('Updating PTSHORT Column....', LOGIT+SENDIT+NOWAITIT)
			
			LOCAL lnMonth, lnYear, lnShort
			*!*	Also ignoring any styles with "-EU" and "-DMG"

			lnMonth = .nMonth
			lnYear = .nYear
			
			SELECT LFFCAST
			SCAN FOR (MONTH = lnMonth) AND (YEAR = lnYear)
				SELECT CURPTNEED
				LOCATE FOR ALLTRIM(STYLE) == ALLTRIM(LFFCAST.SKU)
				IF FOUND() THEN
					* per Chris Malcolm, determine Short qty by subtracting sum of ONHAND + ONHOLD from needqty;
					* if the result is negative, make it zero.
					lnShort = CURPTNEED.NEEDQTY - (CURPTNEED.ONHANDQTY + CURPTNEED.ONHOLDQTY)
					IF lnShort < 0 THEN
						lnShort = 0
					ENDIF
					REPLACE LFFCAST.PTSHORT WITH lnShort IN LFFCAST
				ENDIF	
			ENDSCAN

		ENDWITH
		RETURN
	ENDPROC  &&  UpdatePTSHORTcolumn



			*** now get data to populate In Assembly column - inass
			*** This is code taken from form 'lifebuild'


	FUNCTION LoadForecastXLS
		WITH THIS

		LOCAL lcForeCastFile, loWorkbook, loWorksheet1, loWorksheet2, lnMonth, lnYear, ldToday, lcDecimals
		LOCAL i, lcCol, lcRow, lcMonthForecastCol, lcMonthTargetCol, luValue, lnMaxRow, lnRow, lnCol
		LOCAL llRetVal, lcSkuType


		lcDecimals = SET('DECIMALS')
		SET DECIMALS TO 0

		ldToday = .dToday
		llRetVal = .T.

		lnMonth = .nMonth
		lnYear = .nYear
		
		USE F:\WHI\WHDATA\WHGENPK IN 0 SHARED ALIAS generatepk


		* open the forecast table; warn if this month/year already exist
		IF NOT USED('LFFCAST') THEN
			USE (.cForecastTable) SHARED IN 0 ALIAS LFFCAST
		ENDIF
		SELECT LFFCAST
		LOCATE FOR (MONTH = lnMonth) AND (YEAR = lnYear)
		IF FOUND() THEN
			=MESSAGEBOX("Data for " + TRANSFORM(lnMonth) + "/" + TRANSFORM(lnYear) + " has already been loaded!")
			llRetVal = .F.
		ELSE
			* okay to continue
			lcForeCastFile = GETFILE("XLSX","File Name:","Open",0,"Select the Forecast Spreadsheet to load")

			* OPEN Excel
			.oExcel = CREATEOBJECT("excel.application")
			.oExcel.displayalerts = .F.
			.oExcel.VISIBLE = .F.

			loWorkbook = .oExcel.workbooks.OPEN(lcForeCastFile)
			loWorksheet1 = loWorkbook.Worksheets[1]
			loWorksheet2 = loWorkbook.Worksheets[2]

			* We need to determine which column is the Forecast # for the current month, and which column is the Target # for the current month.
			lcMonthForecastCol = ""
			lcMonthTargetCol = ""
			lcRow = "2"
			FOR lnCol = 5 TO 17  && not sure if this will be consistent..
				lcCol = GetXLAlphaColumn(lnCol)
				luValue = loWorksheet1.RANGE(lcCol + lcRow).VALUE
				IF (TYPE('luValue') == 'N') AND (luValue = lnMonth) THEN
					IF EMPTY(lcMonthForecastCol) THEN
						lcMonthForecastCol = lcCol
					ELSE
						* We've already found forecast col; this must be target col
						lcMonthTargetCol = lcCol
						EXIT FOR
					ENDIF
				ENDIF
			ENDFOR

			IF EMPTY(lcMonthForecastCol) OR EMPTY(lcMonthTargetCol) THEN
				=MESSAGEBOX("Could not identify the Forecast and Target columns for month = " + TRANSFORM(lnMonth)+ "!")
				llRetVal = .F.
			ELSE
				* okay to continue
				lnMaxRow = 1000   && Check with CM as to how many rows.


				* load WORKSHEET 2 into temporary cursor - it will allow us to lookup the UOMs IN WORKSHEET 1 by SKU
				CREATE CURSOR CURUOMS (SKU c(20), UOM i)
				
				FOR lnRow = 2 TO lnMaxRow
				
					lcRow = ALLTRIM(STR(lnRow))
					.cRow = lcRow

					m.SKU = NVL(loWorksheet2.RANGE("A"+lcRow).VALUE,'')

					IF EMPTY(m.SKU) THEN
						* we are past the last data row! exit the loop...
						EXIT FOR
					ENDIF
					
					lcSkuType = TYPE('m.SKU')
					
					DO CASE
						CASE lcSkuType = 'N'
							m.SKU = UPPER(ALLTRIM(STR(m.SKU,20,0)))
						CASE lcSkuType = 'C'
							m.SKU = UPPER(ALLTRIM(m.SKU))
						OTHERWISE
							.TrackProgress('ERROR: INVALID SKU TYPE = ' + lcSkuType, LOGIT+SENDIT)
							THROW
					ENDCASE

					m.UOM = NVL(loWorksheet2.RANGE("B"+lcRow).VALUE,0)

					INSERT INTO CURUOMS FROM MEMVAR

				ENDFOR

				* load WORKSHEET 1 into table
				FOR lnRow = 3 TO lnMaxRow
					lcRow = ALLTRIM(STR(lnRow))
					.cRow = lcRow

					m.SKU = NVL(loWorksheet1.RANGE("A"+lcRow).VALUE,0)

					IF EMPTY(m.SKU) THEN
						* we are past the last data row! exit the loop...
						EXIT FOR
					ENDIF
					
					lcSkuType = TYPE('m.SKU')
					
					DO CASE
						CASE lcSkuType = 'N'
							m.SKU = UPPER(ALLTRIM(STR(m.SKU,20,0)))
						CASE lcSkuType = 'C'
							m.SKU = UPPER(ALLTRIM(m.SKU))
						OTHERWISE
							.TrackProgress('ERROR: INVALID SKU TYPE = ' + lcSkuType, LOGIT+SENDIT)
							THROW
					ENDCASE

					*!*	IF TYPE('m.SKU') = 'N' THEN
					*!*		m.SKU = UPPER(ALLTRIM(STR(m.SKU,0)))
					*!*	ELSE
					*!*		m.SKU = UPPER(ALLTRIM(m.SKU))
					*!*	ENDIF

					* LOOKUP THE UOM
					SELECT CURUOMS
					LOCATE FOR ALLTRIM(CURUOMS.SKU) == ALLTRIM(m.SKU)
					IF FOUND() THEN
						m.UOM = CURUOMS.UOM
					ENDIF

					m.MONTH = lnMonth

					m.YEAR = lnYear

					m.DESCRIPT = NVL(loWorksheet1.RANGE("B"+lcRow).VALUE,'')

					m.DISCO = NOT EMPTY(NVL(loWorksheet1.RANGE("C"+lcRow).VALUE,''))

					m.FORECURR = NVL(loWorksheet1.RANGE(lcMonthForecastCol+lcRow).VALUE,0)

					m.TARGCURR = NVL(loWorksheet1.RANGE(lcMonthTargetCol+lcRow).VALUE,0)
					
					m.LFFCASTID = dygenpk("lffcast","whi")					

					INSERT INTO LFFCAST FROM MEMVAR

				ENDFOR

			ENDIF  &&  EMPTY() OR EMPTY()

			.oExcel.QUIT()

		ENDIF  && FOUND()

		SET DECIMALS TO &lcDecimals.

		ENDWITH
		RETURN llRetVal
	ENDFUNC  &&  LoadForecastXLS


	PROCEDURE CreateStyleSummary
		WITH THIS
			.TrackProgress('Creating Style Summary table....', LOGIT+SENDIT+NOWAITIT)
			* based on cmdStyleSummary.zzcreatereport() in form Inven
			LOCAL lcXLSfolder, lcStyleXLSFile
			lcXLSfolder = .cXLSfolder
			lcStyleXLSFile = lcXLSfolder + 'STYLESUMMARY_' + DTOS(.dToday) + '.XLS'

			taccountid = 6034
			tstyle="%"
			tcolor="%"
			tid="%"
			tpack="%"
			tspecfilter="%"

			OPEN DATABASE F:\WHI\WHDATA\WH.DBC

			USE vinven IN 0
			USE vinvenloc IN 0

			xwhseloc=""
			xnumctn=999999
			xdays=0
			xnorcvdays=0
			xinvopt=""
			xholdtype=""
			xmincuft=0.00
			xmaxcuft=0.00
			xlastdeldt=.F.

			xacctname = 'Life Factory'  && xgetacct.acctname

			xheader = "Style Summary"

			CREATE CURSOR CURSTYLESUMMARY ( ;
				date_rcvd d, ;
				units l, ;
				STYLE c(20), ;
				COLOR c(10), ;
				ID c(10), ;
				PACK c(10), ;
				totqty N(6), ;
				allocqty N(6), ;
				holdqty N(6), ;
				availqty N(6), ;
				reserveqty N(6), ;
				DIMEN c(10), ;
				ctncube N(6,2), ;
				ctnwt N(4,0), ;
				LENGTH N(3), ;
				WIDTH N(3), ;
				depth N(3), ;
				lastdeldate d, ;
				ship_ref c(20))

			xfilter=".t."

			xfilter2=".t."

			**do not display availqty/allocqty if filter set (will not be accurate) - mvw 03/01/06
			xprintavail = (xfilter#".T." OR !EMPTY(xwhseloc))

			SELECT vinven
			SCAN FOR totqty<xnumctn AND date_rcvd<=qdate()-xdays AND &xfilter2 AND totqty#0 AND units
				SCATTER MEMVAR

				IF xfilter#".T." OR !EMPTY(xwhseloc)
					SELECT vinvenloc
					SUM locqty TO m.totqty FOR invenid=vinven.invenid AND &xfilter AND whseloc=xwhseloc
					IF m.totqty=0
						LOOP
					ENDIF
				ENDIF
				m.dimen=TRANS(LENGTH)+"x"+TRANS(WIDTH)+"x"+TRANS(depth)

				INSERT INTO CURSTYLESUMMARY FROM MEMVAR
			ENDSCAN

			SELECT CURSTYLESUMMARY
			INDEX ON STYLE+COLOR+ID+PACK TAG ZORDER
			*BROWSE

			* save the xls file for documentation...
			SELECT CURSTYLESUMMARY
			COPY TO (lcStyleXLSFile) XL5
		ENDWITH
	ENDPROC  && CreateStyleSummary


	PROCEDURE CreateMaterialBuild
		WITH THIS
			.TrackProgress('Creating Material Build table....', LOGIT+SENDIT+NOWAITIT)
			* based on form COMPON; we will use this data to update field MPASS in the forecast table
			LOCAL lcXLSfolder, lcSXLSFile
			lcXLSfolder = .cXLSfolder
			lcXLSFile = lcXLSfolder + 'MATERIAL_BUILD_' + DTOS(.dToday) + '.XLS'
		
			IF NOT USED('INVEN') THEN
				xsqlexec("select * from invenloc where mod='I' and accountid=6034",,,"wh")
				index on str(accountid,4)+trans(units)+style+color+id+pack tag match
				set order to
			ENDIF			

			xsqlexec("select * from comphead where mod='I'","comphead",,"wh")
			xsqlexec("select * from compon where mod='I'","compon",,"wh")
							
			* code below is from compon form, zzcreatereport(); only change was to hardcode the accountid to 6034				
			select ;
				comphead.accountid, ;
				comphead.style as masterstyle, ;
				00000000000 as canmake, ;
				comphead.compgrp, ;
				compon.style, ;
				compon.compqty, ;
				compon.noinven, ;
				.f. as sub, ;
				000000 as availqty, ;
				000000 as buildableqty, ;
				000000 as totavailqty ;
				from comphead, compon ;
				where comphead.compheadid=compon.compheadid ;
				and comphead.accountid=6034 ;
				order by compgrp, masterstyle ;
				into cursor xrpt readwrite

			select xrpt
			scan 
				select inven
				locate for units and accountid=xrpt.accountid and style=xrpt.style and color#"DAMAGE" and availqty>0
				if found()
					replace availqty with inven.availqty in xrpt
				endif
			endscan

			select masterstyle from xrpt group by 1 into cursor xtemp
			scan 
				xcanmake=99999
				select xrpt
				scan for masterstyle=xtemp.masterstyle and !noinven and compqty#0
					xcanmake = min(xcanmake,int(availqty/compqty))
				endscan
				replace canmake with xcanmake in xrpt for masterstyle=xtemp.masterstyle
			endscan

			select * from xrpt into cursor xrpt2

			select xrpt
			scan 
				select xrpt2
				locate for masterstyle=xrpt.style
				if found()
					replace buildableqty with xrpt2.canmake, sub with .t. in xrpt
				endif
			endscan

			replace all totavailqty with availqty+buildableqty in xrpt

			select masterstyle from xrpt where buildableqty>0 group by 1 into cursor xtemp
			scan 
				xcanmake=99999
				select xrpt
				scan for masterstyle=xtemp.masterstyle and !noinven and compqty#0
					xcanmake = min(xcanmake,int(totavailqty/compqty))
				endscan
				replace canmake with xcanmake in xrpt for masterstyle=xtemp.masterstyle
			endscan

			SELECT * FROM XRPT INTO CURSOR CURMATERIALBUILD
			USE IN XRPT

			*SELECT CURMATERIALBUILD
			*BROW

			* save the xls file for documentation...
			SELECT CURMATERIALBUILD
			COPY TO (lcXLSFile) XL5
		ENDWITH
	ENDPROC  && CreateMaterialBuild


	PROCEDURE TrackProgress
		* do any combination of Wait Window, writing to logfile, and adding to body of email,
		* based on nFlags parameter.
		LPARAMETERS tcExpression, tnFlags
		WITH THIS
			IF BITAND(tnFlags,LOGIT) = LOGIT THEN
				IF .lLoggingIsOn THEN
					?
					? .cProcessName + "  " + TTOC(DATETIME()) + ": " + tcExpression
				ENDIF
			ENDIF
			IF .lWaitWindowIsOn THEN
				IF BITAND(tnFlags,NOWAITIT) = NOWAITIT THEN
					WAIT WINDOW tcExpression NOWAIT
				ENDIF
				IF BITAND(tnFlags,WAITIT) = WAITIT THEN
					WAIT WINDOW tcExpression TIMEOUT .nWaitWindowTimeout
				ENDIF
			ENDIF
			IF BITAND(tnFlags,SENDIT) = SENDIT THEN
				IF .lSendInternalEmailIsOn THEN
					.cBodyText = .cBodyText + tcExpression + CRLF + CRLF
				ENDIF
			ENDIF
		ENDWITH
	ENDPROC  &&  TrackProgress


ENDDEFINE














*!*	*****************************************8

*!*	SET DELETED ON

*!*	LOCAL loExcel, lcForeCastFile, loWorkbook, loWorksheet1, loWorksheet2, lnMonth, lnYear, ldToday, lcDecimals
*!*	LOCAL i, lcCol, lcRow, lcMonthForecastCol, lcMonthTargetCol, luValue, lnMaxRow, lnRow, lnCol

*!*	lcDecimals = SET('DECIMALS')
*!*	SET DECIMALS TO 0

*!*	ldToday = DATE()

*!*	DO StyleSummary

*!*	RETURN

*!*	* eventually we will pass these in as parameters
*!*	lnMonth = MONTH(ldToday)
*!*	lnYear = YEAR(ldToday)

*!*	* open the forecast table; warn if this month/year already exist
*!*	USE F:\UTIL\LF\FORECAST\LFFCAST SHARED IN 0 ALIAS LFFCAST
*!*	SELECT LFFCAST
*!*	LOCATE FOR (month = lnMonth) AND (year = lnYear)
*!*	IF FOUND() THEN
*!*		=MESSAGEBOX("Data for " + TRANSFORM(lnMonth) + "/" + TRANSFORM(lnYear) + " has already been loaded!")
*!*	ELSE
*!*		* okay to continue
*!*		lcForeCastFile = GETFILE("XLSX","File Name:","Open",0,"Select the Forecast Spreadsheet to load")

*!*		* OPEN Excel
*!*		loExcel = CREATEOBJECT("excel.application")
*!*		loExcel.displayalerts = .F.
*!*		loExcel.VISIBLE = .F.

*!*		loWorkbook = loExcel.workbooks.OPEN(lcForeCastFile)
*!*		loWorksheet1 = loWorkbook.Worksheets[1]
*!*		loWorksheet2 = loWorkbook.Worksheets[2]
*!*
*!*		* We need to determine which column is the Forecast # for the current month, and which column is the Target # for the current month.
*!*		lcMonthForecastCol = ""
*!*		lcMonthTargetCol = ""
*!*		lcRow = "2"
*!*		FOR lnCol = 5 TO 17  && not sure if this will be consistent..
*!*			lcCol = GetXLAlphaColumn(lnCol)
*!*			luValue = loWorksheet1.RANGE(lcCol + lcRow).VALUE
*!*			IF (TYPE('luValue') == 'N') AND (luValue = lnMonth) THEN
*!*				IF EMPTY(lcMonthForecastCol) THEN
*!*					lcMonthForecastCol = lcCol
*!*				ELSE
*!*					* We've already found forecast col; this must be target col
*!*					lcMonthTargetCol = lcCol
*!*					EXIT FOR
*!*				ENDIF
*!*			ENDIF
*!*		ENDFOR
*!*
*!*		IF EMPTY(lcMonthForecastCol) OR EMPTY(lcMonthTargetCol) THEN
*!*			=MESSAGEBOX("Could not identify the Forecast and Target columns for month = " + TRANSFORM(lnMonth)+ "!")
*!*		ELSE
*!*			* okay to continue
*!*			lnMaxRow = 1000   && Check with CM as to how many rows.
*!*
*!*			* load WORKSHEET 1 into table
*!*			FOR lnRow = 3 TO lnMaxRow
*!*				lcRow = ALLTRIM(STR(lnRow))
*!*
*!*				m.SKU = NVL(loWorksheet1.RANGE("A"+lcRow).VALUE,0)
*!*
*!*				IF EMPTY(m.SKU) THEN
*!*					* we are past the last data row! exit the loop...
*!*					EXIT FOR
*!*				ENDIF
*!*
*!*				m.MONTH = lnMonth
*!*
*!*				m.YEAR = lnYear
*!*
*!*				* convert sku to char
*!*				m.SKU = TRANSFORM(m.SKU)
*!*
*!*				m.DESCRIPT = NVL(loWorksheet1.RANGE("B"+lcRow).VALUE,'')
*!*
*!*				m.DISCO = NOT EMPTY(NVL(loWorksheet1.RANGE("C"+lcRow).VALUE,''))
*!*
*!*				m.UOM = NVL(loWorksheet1.RANGE("D"+lcRow).VALUE,0)
*!*
*!*				m.FORECURR = NVL(loWorksheet1.RANGE(lcMonthForecastCol+lcRow).VALUE,0)
*!*
*!*				m.TARGCURR = NVL(loWorksheet1.RANGE(lcMonthTargetCol+lcRow).VALUE,0)
*!*
*!*				INSERT INTO LFFCAST FROM MEMVAR
*!*
*!*
*!*
*!*			ENDFOR
*!*
*!*			SELECT LFFCAST
*!*			BROW
*!*
*!*
*!*
*!*
*!*
*!*
*!*
*!*		ENDIF  &&  EMPTY() OR EMPTY()
*!*
*!*
*!*		*** now get data to populate TGF Inventory column - tgfinv
*!*		*** This is code taken from form 'inven'
*!*		*** with option 'units only'
*!*	** also:
*!*	*!*		Only Styles that start with a Number and the Qty is the TotQty, not Available Qty.
*!*	*!*	Also ignoring any styles with "-EU" and "-DMG"

*!*
*!*
*!*
*!*
*!*
*!*		*** now get data to populate In Assembly column - inass
*!*		*** This is code taken from form 'lifebuild'
*!*
*!*
*!*
*!*
*!*
*!*		*SET STEP ON
*!*
*!*	ENDIF  && FOUND()

*!*	IF USED('LFFCAST') THEN
*!*		USE IN LFFCAST
*!*	ENDIF

*!*	SET DECIMALS TO &lcDecimals.

*!*	loExcel.quit()

*!*	RETURN

