* WIP for BBC

utilsetup("BBC_WIP")
Set Exclusive Off
Set Deleted On
Set Talk Off
Set Safety Off
*!*	if holiday(date())
*!*		return
*!*	endif

guserid="AUTO"
gwipbywo=.F.
gwipbybl=.F.
xpnpacct=.F.
xwip=.T.
xtitle="Work in Progress"
GOFFICE='L'

xsqlexec("select * from pt where mod='"+GOFFICE+"'",,,"wh")
Index On ptid Tag ptid
Set Order To

xsqlexec("select * from account where inactive=0","account",,"qq")
Index On accountid Tag accountid
Set Order To

xsqlexec("select * from outship where mod='L' and (del_date={}  OR del_date>={"+DTOC(DATE()-7)+"})and notonwip=0",,,"wh")


Select dept, ship_ref, batch_num As div,consignee, cnee_ref, qty, ptdate, Start, Cancel, {}As called, {} routedfor, {} appt, scac, ;
	space(20) appt_num,Space(35) apptremarks, ship_via,Space(20) bol_no, 00000000 wo_num,{}  wo_date,{}  pulleddt,{}  picked,{} ;
	labeled,{} staged, {} del_date ,000000  ctnqty,0000  outshipid,0000 As pt_age, 0000 As days_to_cncll, accountid As account,Space(2) As WHSE ;
	from pt ;
	where Inlist(accountid,6757) And qty!=0 	Into Cursor xrpt2 Readwrite

Select dept, ship_ref, batch_num As div,consignee, cnee_ref, qty, ptdate, Start, Cancel, called, routedfor, appt, scac, ;
	appt_num, apptremarks, ship_via, bol_no, wo_num, wo_date, pulleddt, picked, labeled, staged, del_date, ctnqty, outshipid,0000 As pt_age, 0000 As days_to_cncl, accountid As account,Space(2) As WHSE ;
	from outship ;
	where Inlist(accountid,6757) And !notonwip And (emptynul(del_date) OR del_date>=DATE()-7) ;
	into Cursor xrpt1 Readwrite

Select * From xrpt1 Union All Select * From xrpt2 Into Cursor xrpt3 Readwrite
Replace All pt_age With (Date()-ptdate) FOR (Date()-ptdate)>0
Replace All   days_to_cncl With 9999 For Cancel={01/01/1900}
Replace All days_to_cncl With (Cancel-Date()) For Cancel !={01/01/1900}
Replace All WHSE With 'ML'
Select xrpt3.*, Space(15) As Status From xrpt3 Into Cursor XRPT4 Readwrite
Replace All Status With Iif(emptynul(wo_date),"UNALLOCATED",Iif(!emptynul(staged),"STAGED",Iif(!emptynul(labeled),"LABELED",Iif(!emptynul(picked),"PICKED",Iif(!emptynul(pulleddt),"PULLED","ALLOCATED"))))) In XRPT4
replace status WITH "SHIPPED" FOR (del_date)>{01/01/1900} In XRPT4


Select * From XRPT4 Into Cursor xrptj Readwrite
Scan
	xsqlexec("select called from apptcall where mod='L' and outshipid="+Transform(xrptj.outshipid),,,"wh")
	Calculate Min(called) To aa
	Replace called With aa In xrptj
Endscan


*!*	Delete File F:\Auto\ragwip.Xls
*!*	Delete File F:\Auto\ragwip.xlsx

*!*	Select xrptj
*!*	Index On consignee+ship_ref Tag ZOrder
*!*	Copy To F:\Auto\ragwip.Xls Xls

*!*	oexcel=Createobject("excel.application")
*!*	oexcel.Visible=.F.
*!*	oworkbook=oexcel.workbooks.Open("f:\auto\ragwip.xls")
*!*	oworkbook.SaveAs("f:\auto\ragwip.xlsx",51)
*!*	oexcel.Quit()
*!*	Release oexcel

*!*	tsendto="todd.margolin@tollgroup.com,Greg.Hubbert@rag-bone.com"
*!*	tattach = "F:\FTPUSERS\RagBone\reports\ragnbone_wip.xls"
*!*	tfrom ="TGFSYSTEM"
*!*	tmessage = "see attached file"
*!*	tsubject = "Work in Progress spreadsheet"

*!*	Do Form dartmail2 With tsendto,tfrom,tsubject," ",tattach,tmessage,"A"
*!*	SELECT xrptj
*!*	SET STEP ON 
*!*			If Reccount() > 0 
*!*			
*!*					
*!*		    copy to F:\FTPUSERS\BBCRW\reports\wip\bbc_wip.xls type xls
*!*	            xfile='F:\FTPUSERS\BBCRW\reports\wip\bbc_wip.xls'
*!*	            oexcel = createobject("excel.application")
*!*	            oexcel.displayalerts = .f.
*!*	            oworkbook = oexcel.workbooks.open(xfile,,.f.)
*!*	            xfile=xfile+"x"
*!*	            oworkbook.saveas(xfile) && "39" converts the save format to excel95/97
*!*	            oworkbook.close()
*!*	            oexcel.quit()
SET STEP ON
SELECT xrptj
select *, iif(empty(ptdate),"",dtoc(ptdate)) as cptdate from xrptj INTO CURSOR c1 READWRITE
select *, iif(empty(start),"",dtoc(start)) as cstart from c1 INTO CURSOR c2 READWRITE
select *, iif(empty(cancel),"",dtoc(cancel)) as ccancel from c2 INTO CURSOR c3 READWRITE
select *, iif(empty(called),"",dtoc(called)) as ccalled from c3 INTO CURSOR c4 READWRITE
select *, iif(empty(routedfor),"",dtoc(routedfor)) as crouted from c4 INTO CURSOR c5 READWRITE
select *, iif(empty(appt),"",dtoc(appt)) as cappt from c5 INTO CURSOR c6 READWRITE
select *, iif(empty(wo_date),"",dtoc(wo_date)) as cwodate from c6 INTO CURSOR c7 READWRITE
select *, iif(empty(pulleddt),"",dtoc(pulleddt)) as cpulleddt FROM c7 INTO CURSOR c8 READWRITE
select *, iif(empty(picked),"",dtoc(picked)) as cpicked from c8 INTO CURSOR c9 READWRITE
select *, iif(empty(labeled),"",dtoc(labeled)) as clabeled from c9 INTO CURSOR c10 READWRITE
select *, iif(empty(staged),"",dtoc(staged)) as cstaged from c10 INTO CURSOR c11 READWRITE
select *, iif(empty(del_date),"",dtoc(del_date)) as cdeldate from c11 INTO CURSOR c12 READWRITE

Select dept, ship_ref, div,consignee, cnee_ref, qty, cptdate as ptdate, cStart as start, cCancel as cancel, ccalled as called, crouted as routed, cappt as appt, scac, ;
	appt_num, apptremarks, ship_via, bol_no, wo_num, cwodate as wo_date, cpulleddt as pulleddt, cpicked as picked, clabeled as labeled,  cstaged as staged, cdeldate as del_date, ctnqty, outshipid, pt_age,  days_to_cncl,  account, WHSE,status ;
	from c12 INTO CURSOR c13 READWRITE 

SET STEP ON 		
SELECT c13		
	If Reccount() > 0 	
		export TO "F:\FTPUSERS\BBCRW\reports\wip\bbc_wip"   TYPE xl5
		COPY TO F:\FTPUSERS\BBCRW\reports\wip\bbc_wip.csv TYPE csv
		tsendto = "tmarg@fmiint.com"
		tattach = "F:\FTPUSERS\BBCRW\reports\wip\bbc_wip.xls" 
		tcc =""                                                                                                                               
		tFrom ="Toll EDI System Operations <fmi-transload-ops@fmiint.com>"                                
		tmessage = "BBC WIP_:"+Ttoc(Datetime())        
		tSubject = "BBC WIP COMPLETE"                                                           
		Do Form m:\dev\frm\dartmail2 With tsendto,tFrom,tSubject, tcc,tattach,tmessage  ,"A"     
			
	ELSE 


	ENDIF
close data all 



schedupdate()

Try
	_Screen.Caption=gscreencaption
Catch
Endtry

On Error