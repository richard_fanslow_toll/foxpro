Parameters xfile

wait "Calerdon Parse 850 Processing..." window nowait noclear

Set Century On
Set Date To Mdy

*!*  860 BCH Types
*!*  00=Original
*!*  01=Cancellation
*!*  02=Add
*!*  03=Delete
*!*  04=Change
*!*  05=Replace
*!*  06=Confirmation
*!*  07=Duplicate
*!*  08=Status
*!*  10=Not Found
*!*  11=Response
*!*  12=Not Processed
*!*  13=Request
*!*  14=Advance Notification
*!*  15=Re-Submission
*!*  16=Proposed
*!*  17=Cancel, to be Reissued
*!*  18=Reissue
*!*  19=Seller initiated change
*!*  20=Final Transmission
*!*  21=Trans on Hold
*!*  22=Information Copy
*!*  24=Draft
*!*  25=Incremental
*!*  26=Replace - Specified Buyers Parts Only
*!*  27=Verify
*!*  28=Query
*!*  30=Renewal
*!*  31=Allowance/Addition
*!*  32=Recovery/Deduction
*!*  33=Request for Payment
*!*  34=Payment Declined
*!*  35=Request Authority
*!*  36=Authority to Deduct (Reply)
*!*  37=Authority Declined (Reply)
*!*  38=No Financial Value
*!*  39=Response to Proposed Trip Plan
*!*  40=Commitment Advice
*!*  41=Corrected and Verified
*!*  42=Temporary Record
*!*  43=Request Permission to Service
*!*  44=Rejection
*!*  45=Follow-up
*!*  46=Cancellation with Refund
*!*  47=Transfer
*!*  48=Suspended
*!*  49=Original - No Response Necessary
*!*  50=Register
*!*  51=Historical Inquiry
*!*  52=Response to Historical Inquiry
*!*  53=Completion
*!*  54=Approval
*!*  55=Excavation
*!*  56=Expiration Notification
*!*  5C=Chargeable Resubmission
*!*  77=Simulation Exercise
*!*  CN=Completion Notification
*!*  CO=Corrected
*!*  EX=Final Loading Configuration
*!*  GR=Granted
*!*  PR=Proposed Loading Configuration
*!*  RH=Release Hold
*!*  RV=Revised Loading Configuration
*!*  SU=Status Update
*!*  ZZ=Mutually Defined



*!*  860 POC codes
*!*  AI=Add Additional Item(s)
*!*  CA=Changes To Line Items
*!*  CB=Change of Date Terms
*!*  CC=Changes To Terms
*!*  CE=Changes To Item Level Allowance/Charges
*!*  CF=Cancel Previously Transmitted Purchase Order
*!*  CG=Changes To Total Level Allowance/Charges
*!*  CH=Change To Original Confirmation Of Original Announcement
*!*  CI=Change To Confirmation Of Revised Announcement
*!*  CT=Change of Dates
*!*  DI=Delete Item(s)
*!*  MU=(Multiple) For Unit Price Quantity Reschedule Change
*!*  NC=Concurrent Item (No Change)
*!*  OA=Original Confirmation Of Revised Announcement
*!*  OC=Original Confirmation Of Original Announcement
*!*  PC=Price Change
*!*  PQ=Unit Price/Quantity Change
*!*  PR=Unit Price/Reschedule Change
*!*  QD=Quantity Decrease
*!*  QI=Quantity Increase
*!*  RA=Replace Mode of Shipment
*!*  RB=Replace All Dates
*!*  RC=Reject Item Change
*!*  RE=Replacement Item
*!*  RM=Replacement Item with Modifications
*!*  RQ=Reschedule/Quantity Change
*!*  RS=Reschedule
*!*  RZ=Replace All Values
*!*  TI=Transfer Item

testing=.f.
llmanual = .F.
xfilecreated = .F.

If testing
  If !Used("podata")
    Use F:\ftpusers\calderon\Datatest\podata In 0
  Endif
Endif

If !testing
  If !Used("podata")
    Use F:\ftpusers\calderon\Data\podata In 0
  Endif
Endif

If !Used("unloccode")
  Use F:\sysdata\qqdata\unloccode In 0
Endif
If !Used("vendors")
  Use F:\ftpusers\calderon\Data\vendors In 0
Endif
If !Used("uom")
  Use F:\sysdata\qqdata\uom In 0
Endif

Select * From podata Into Cursor temppo Where .F. Readwrite


If llmanual
  If xfile = .F.
    xfile = Getfile()
    Do parsepodata
  Endif
Else

  lcPath        =Iif(testing,"f:\ftpusers\Calderon\850test\testcasein\","f:\ftpusers\Calderon\850in\")
 
  lcOutPath     =Iif(testing,"f:\ftpusers\calderon\850test\outfiles\","f:\ftpusers\toll\Calderon\850out\")
 * lcOutPath     =Iif(testing,"f:\ftpusers\calderon\850test\outfiles\","f:\ftpusers\calderon\850test\outfiles\")
  lcArchivePath =Iif(testing,"f:\ftpusers\Calderon\850test\testcasein\archive\","f:\ftpusers\Calderon\850in\archive\")
  x997dir       ="f:\ftpusers\Calderon\850-997\"

*  Cd &lcPath  &&F:\ftpusers\calderon\850In\archive

  lnNum = Adir(tarray,lcPath+"*.edi")

  If lnNum = 0
    NormalExit = .T.
	**throw errors out, not in a try statement - mvw 07/09/15
*    Throw

    Use in podata
	Use in unloccode
	Use in vendors
	Use in uom

	wait clear
	return
  Endif

  For thisfile = 1  To lnNum
    xfile = Alltrim(lcPath+tarray[thisfile,1])
    cfilename = Lower(tarray[thisfile,1])

    xfilecreated = .T.
    Wait Window "Calerdon Parse 850 Processing... Importing file: "+cfilename noclear nowait
    Do parsepodata

    Select temppo
    Goto Top

******* here an original PO, write the file and append then data to the podata file
    If editype = "850"
      Do writefile
      Select temppo
      Zap
    Endif

    If editype = "860"

      If temppo.bchcode = "01"   && total delete of a PO
        Select temppo
        Do writefile
        Select temppo
        Replace All Deleted With .T.
        Replace All canceldt With Date()
        Select temppo
        Go Top
        Scan
          Select temppo
          Scatter Memvar
          Select podata
          Append Blank
          Gather Memvar
        Endscan
        Select temppo
        Zap
      Endif

      If temppo.bchcode != "01"   && NOT total delete of a PO
        Select temppo
        Scan For itemaction = "CAN"
          Replace Deleted   With .T.
          Replace canceldt With Date()
        Endscan

        Select temppo
        Goto Top
        Select podata
        Delete For po_num = temppo.po_num

        Select temppo
        Goto Top
        Do writefile

        Select temppo
        Go Top
        Scan
          Select temppo
          Scatter Memvar
          Select podata
          Append Blank
          Gather Memvar
        Endscan
        Select temppo
        Zap

      Endif
    Endif

    archivefile  = (lcArchivePath+cfilename)
    If !File(archivefile)
      Copy File [&xfile] To [&archivefile]
    Endif
    If File(xfile)
      Delete File [&xfile]
    Endif
  Endfor

  If xfilecreated And !testing 
    Use F:\edirouting\ftpjobs In 0
    xjob=Iif(testing,"CALD-TEST-850-ICON","CALD-850-ICON")
    Insert Into ftpjobs (jobname, Userid, jobtime) Values (xjob,"CALD850",Datetime())
    Use In ftpjobs
  Endif
Endif

Use in podata
Use in unloccode
Use in vendors
Use in uom

wait clear



************************************************************************88888
Procedure parsepodata

Do createx856
Wait Window "Calerdon Parse 850 Processing... Importing file: "+xfile noclear Nowait
Select x856
Goto Top


Select podata
Scatter Memvar Blank

**default delivery port UNLOC code required for Icon - default to LAX
m.delivport="USNYC"

**no CUR segment sent for CALDERON
m.currency="USD"

**no TD5 for shipvia for CALDERON, always SEA
m.shipvia="SEA"

**no incoterms included, so default to "FOB" as is mandatory for ICON - 04/10/13
m.incoterms="FOB"


Do loadedifile With xfile,"*","PIPE"

Select x856
Set Filter To
Goto Top

m.adddt=Datetime()

Do While !Eof("x856")
  If Trim(x856.segment) = "ISA"
    m.isa = Alltrim(x856.f13)
    m.loaddt = Datetime()
    m.loadtm = Datetime()
    m.filename   = Upper(xfile)
    Select x856
    Skip 1 In x856
    Loop
  Endif

  If Inlist(x856.segment,"GS")
    Select x856
    Skip 1 In x856
    Loop
  Endif

  If Inlist(x856.segment,"ST")
    Select x856
    m.editype= Alltrim(x856.f1)
    m.po_num = Alltrim(x856.f2)

    If m.editype ="850"  && on 850s always default to false,850 are original
      m.z860rcvd=.F.
      m.z860sent=.F.
      m.hdraction ="PLC"
    Endif

    If m.editype ="860"  && on 850s always default to false,850 are original
      m.z860rcvd=.T.
&& need to do a lookup into the PO data to see if an 860 has alreday been sent against this PO
      If m.editype ="860"
        Select podata
        Locate For Padr(Alltrim(podata.po_num),10," ") = Padr(Alltrim(m.po_num),10," ") And z860sent
        If Found("podata")
          m.z860sent=.T.  && we have already sent an 860
        Else
          m.z860sent=.F.  && no record of having already sent an 860
        Endif
      Endif
    Endif
    Skip 1 In x856
    Loop
  Endif

  If Inlist(Trim(x856.segment),"BCH")
    m.transcode = Alltrim(x856.f1)
    m.changecode =Alltrim(x856.f1)
    m.bchcode = Alltrim(x856.f1)
    Do Case
    Case m.bchcode = "01"
      m.bchtext="CANCEL"
      m.hdraction ="CAN"
    Case m.bchcode = "02"
      m.bchtext="ADD"
      m.hdraction ="ADD"
    Case m.bchcode = "03"
      m.bchtext="DELETE"
      m.hdraction ="DEL"
    Case m.bchcode = "04"
      m.bchtext="CHANGE"
      m.hdraction ="PLC"
    Case m.bchcode = "05"
      m.bchtext="REPLACE"
      m.hdraction ="REP"
    Otherwise
      m.bchtext="UNK"
      m.hdraction ="UNK"
    Endcase

    Select x856
    Skip 1 In x856
    Loop
  Endif

  If Inlist(Trim(x856.segment),"BEG")
    If Alltrim(x856.f1) = "00"
      m.hdr ="PLC"
      m.lineaction = "PLC"
    Endif
    m.transcode = Alltrim(x856.f1)
    m.changecode =Alltrim(x856.f1)

    Select x856
    Skip 1 In x856
    Loop
  Endif


  If Trim(x856.segment) = "ITD"
    Select x856
    Skip 1 In x856
    Loop
  Endif

  If Trim(x856.segment) = "DTM" And Alltrim(x856.f1)="002"
    m.reqdt = Substr(Alltrim(x856.f2),1,4)+"-"+Substr(Alltrim(x856.f2),5,2)+"-"+Substr(Alltrim(x856.f2),7)
    Select x856
    Skip 1 In x856
    Loop
  Endif

  If Trim(x856.segment) = "DTM" And Alltrim(x856.f1)="004"
    m.orderdt = Substr(Alltrim(x856.f2),1,4)+"-"+Substr(Alltrim(x856.f2),5,2)+"-"+Substr(Alltrim(x856.f2),7)
    Select x856
    Skip 1 In x856
    Loop
  Endif

  If Trim(x856.segment) = "PER" And Alltrim(x856.f1)="BD"
    m.contact = Alltrim(x856.f2)
    Select x856
    Skip 1 In x856
    Loop
  Endif

  If Trim(x856.segment) = "TD5"
    Select x856
    Skip 1 In x856
    Loop
  Endif

  If Trim(x856.segment) = "N1" And Alltrim(x856.f1)="ST"
    m.shiptoname =  Upper(Alltrim(x856.f2))
    m.shiptocode =  Alltrim(x856.f3)
    Select x856
    Skip 1 In x856
    If Trim(x856.segment) = "N2" Or Trim(x856.segment) = "N3"
      m.shiptoadd1=  Upper(Alltrim(x856.f1))
    Endif

    Skip 1 In x856

    If Trim(x856.segment) = "N3"
      m.shiptoadd2=  Upper(Alltrim(x856.f1))
    Endif
    Skip 1 In x856

    If Trim(x856.segment) = "N4"
      m.shiptocity=  Upper(Alltrim(x856.f1))
      m.shiptost  =  Alltrim(x856.f2)
      m.shiptoctry=  Upper(Alltrim(x856.f4))
      m.shiptozip =  Alltrim(x856.f3)
    Endif
    Skip 1 In x856
    Loop
  Endif

  If Trim(x856.segment) = "REF" And Upper(Trim(x856.f1)) = "IA"
    m.vendorid = Val(Trim(x856.f2))
    Select vendors
    Locate For m.vendorid = vendors.vendorid
    If Found()
      m.suppname=Alltrim(vendors.vendorname)
      m.suppaddr1=Alltrim(vendors.physaddr1)
      m.suppaddr2=Alltrim(vendors.physaddr2)

      m.suppcity=Alltrim(vendors.physcity)
      m.suppst=Alltrim(vendors.physstate)
      m.suppzip=Alltrim(vendors.physzip)
      m.suppcntry=Alltrim(vendors.pcountry)

      Select unloccode
      Locate For Name=m.suppcity And country=m.suppcntry
      Do Case
      Case Found()
        m.originport=Alltrim(country)+Alltrim(unloccode)
      Otherwise
**default UNLOC code for origin port for CALDERON to PKKHI, per rebecca
        m.originport="PKKHI"
      Endcase
    Else
&& errormsg here
    Endif
    Select x856
  Endif

  If Trim(x856.segment) = "REF" And Upper(Trim(x856.f1)) = "ST"

  Endif

  If Inlist(Trim(x856.segment),"PO1","POC")
*    Do While !Trim(x856.f1) ="CR"
    Do While Trim(x856.segment)!="CTT")

      If Trim(x856.segment)="PO1"
        m.item       = Trim(x856.f7)
        m.style      = Trim(x856.f7)
        m.unitprice  = Trim(x856.f4)
        m.lineno     = Trim(x856.f1)
        m.origlineno= m.lineno
        m.itemaction = "PLC"
        Skip 1 In x856
        Loop
      Endif

      If Trim(x856.segment)="POC"
        m.lineaction = Trim(x856.f2)
        m.lineno =Trim(x856.f1)
        Do Case
        Case m.lineaction = "DI"
          m.itemaction = "CAN"
        Case m.lineaction = "CA"
          m.itemaction = "PLC"
        Case Inlist(m.lineaction,"00","05","06","04")
          m.itemaction = "PLC"
        Otherwise
          m.item = "UNK"
        Endcase
        m.changecode = Trim(x856.f2)
        m.item       = Trim(x856.f9)
        m.style      = Trim(x856.f9)
        m.unitprice  = Trim(x856.f6)
        m.origlineno= m.lineno
        Skip 1 In x856
        Loop
      Endif

      If Trim(x856.segment) = "PID"
        m.descrip = Upper(Alltrim(x856.f5))
        Skip 1 In x856
        Loop
      Endif

      If Trim(x856.segment) = "REF" And Upper(Trim(x856.f1)) = "MF"
        m.hts = Alltrim(x856.f2)
        Skip 1 In x856
        Loop
      Endif

      If Trim(x856.segment) = "SCH"
        m.qty = Alltrim(x856.f1)
        m.uom = Alltrim(x856.f2)
        m.itemreqdt= Substr(Alltrim(x856.f6),5,2)+"-"+Substr(Alltrim(x856.f6),7,2)+"-"+Substr(Alltrim(x856.f6),1,4)
        m.schdlineno=Alltrim(x856.f11)
        If !Empty(m.schdlineno)
          m.lineno = m.origlineno+Padl(Alltrim(m.schdlineno),3,"0")
        Endif
        Skip 1 In x856
*        Loop
      Endif

      If Trim(x856.segment) = "REF" And Upper(Trim(x856.f1)) = "CR"
        If Trim(x856.f1) ="CR" And  Trim(x856.f2) ="1"
          m.hotflag = ""
        Endif
        If Trim(x856.f1) ="CR" And  Trim(x856.f2) ="2"
          m.hotflag = "TRUE"
        Endif
        Skip 1 In x856
        If Trim(x856.segment) = "REF" And Upper(Trim(x856.f1)) = "ZZ"

        Else
          Skip -1 In x856
        Endif

        Insert Into temppo From Memvar
        Skip 1 In x856
        Loop
      Endif
    Enddo
  Endif  && end of a PO


  Select x856
  Skip 1 In x856

  If Eof()
    Exit
  Endif
Enddo &&!eof("x856")

Select temppo
Scan
  Select temppo
  Scatter Memvar
  Select podata
  Append Blank
  Gather Memvar
Endscan

Endproc

**************************************************************************************************************************8
Procedure writefile


xhdrstr="order_no|order_split|order_status|client_cust_code|supplier_code|supplier_name|supplier_address_1|"+;
"supplier_address_2|supplier_city|supplier_region_state|supplier_country|supplier_postcode|confirm_number|"+;
"confirm_date|exw_required_by|delivery_required_by|description|order_date_time|order_value|order_currency_code|"+;
"exchange_rate|incoterms|additional_terms|transport_mode|container_mode|milestone_tracking_date_exw|"+;
"milestone_tracking_date_etd|milestone_tracking_date_eta|custom_milestone_est1|custom_milestone_act1|"+;
"custom_milestone_est2|custom_milestone_act2|custom_milestone_est3|custom_milestone_act3|custom_milestone_est4|"+;
"custom_milestone_act4|sending_agent|sending_agent_name|origin_port|destination_port|custom_text1|custom_text2|"+;
"custom_text3|custom_text4|custom_text5|custom_date1|custom_date2|custom_decimal1|custom_decimal2|custom_decimal3|"+;
"custom_decimal4|custom_decimal5|custom_flag1|custom_flag2|custom_flag3|custom_flag4|custom_flag5|contact1|"+;
"contact2|country_code|notes|line_no|subline_no|linesplit_no|product|line_description|qty_ordered|innerpacks|"+;
"outerpacks|unit_of_measure|itemprice|lineprice|linestatus|required_date|part_attrib1|part_attrib2|part_attrib3|"+;
"line_custom_attrib1|line_custom_attrib2|line_custom_attrib3|line_custom_attrib4|line_custom_attrib5|"+;
"line_custom_date1|line_custom_date2|line_custom_date3|line_custom_date4|line_custom_date5|line_custom_decimal1|"+;
"line_custom_decimal2|line_custom_decimal3|line_custom_decimal4|line_custom_decimal5|line_custom_flag1|"+;
"line_custom_flag2|line_custom_flag3|line_custom_flag4|line_custom_flag5|line_custom_text1|container_number|"+;
"container_packing_order|country_of_origin|dg_substance|dg_flashpoint|weight|volume|weight_type|volume_type|"+;
"special_instructions|additional_information|delivery_port|address_code|delivery_address_name|"+;
"delivery_address_address1|delivery_address_address2|delivery_address_city|delivery_address_state|"+;
"delivery_address_country|delivery_address_postcode"+Chr(13)+Chr(10)

xfilestr=xhdrstr
llStrtran = .F.

Select temppo
Goto Top
thispo= temppo.po_num


Scan
*  Scatter Memvar Blank
  Scatter Memvar
  xfilestr=xfilestr+;
  Alltrim(m.po_num)+"|"+; &&A
  "|"+; &&column(s) B empty
  m.itemaction+"|"+; &&C
  "CALD"+"|"+;&&D  might need to update this
  Alltrim(Transform(m.vendorid))+"|"+; &&E
  m.suppname+"|"+; &&F
  m.suppaddr1+"|"+; &&G
  m.suppaddr2+"|"+; &&H
  m.suppcity+"|"+; &&I
  m.suppst+"|"+; &&J
  m.suppcntry+"|"+; &&K
  m.suppzip+"|"+; &&L
  "||"+; &&column(s) M-N empty
  m.reqdt+"|"+; &&O
  "||"+; &&column(s) P-Q empty
  m.orderdt+"|"+; &&R
  "|"+; &&column(s) S empty
  m.currency+"|"+; &&T
  "|"+; &&column(s) U empty
  m.incoterms+"|"+; &&V
  "|"+; &&column(s) W empty
  m.shipvia+"|"+; &&X
  "||"+; &&column(s) Y-Z is empty
  "|"+; &&AA
  "|"+; &&column(s) AB is empty
  m.reqdt+"|"+; &&AC
  "|||||||||"+; &&column(s) AD-AL empty
  m.originport+"|"+; &&AM
  m.delivport+"|"+; &&AN
  "|"+; &&AO
  "|"+; &&AP
  "|"+; &&AQ
  "|||||||||"+; &&column(s) AR-AZ
  Iif(m.editype="860","Y","N")+"|"+; &&BA (860 flag)
  Iif(m.z860sent,"Y","N")+"|"+; &&BB (860 sent previously flag)
  "|||"+; &&column(s) BC-BE empty
  m.contact+"|"+; &&BF
  "|"+; &&BG
  "|"+; &&BH
  m.msg+"|"+; &&BI
  m.lineno+"|"+; &&BJ
  "||"+; &&column(s) BK-BL empty
  m.style+"|"+; &&BM
  Iif (llStrtran,Strtran(m.descrip,","," "),m.descrip)+"|"+; &&BN
  Transform(m.qty)+"|"+; &&BO
  "||"+; &&BP-BQ empty
  m.uom+"|"+; &&BR
  Transform(m.unitprice)+"|"+; &&BS
  "|"+; &&column(s) BT empty
  m.itemaction+"|"+; &&BU
  m.itemreqdt+"|"+; &&BV
  "|"+; &&BW
  "|"+; &&BX - empty, only need 2 attributes style and sku
  "|"+; &&BY - empty, had held the 1st HTS # - mvw 02/10/12
  m.hts+"|"+; &&BZ
  m.size+"|"+; &&CA
  m.service+"|"+; &&CB
  m.extqty+"|"+; &&CC
  "|"+; &&CD
  "||||||||||"+; &&CE-CN
  m.hotflag+"|"+; &&CO
  "|||||||"+; &&column(s) CP-CV empty
  "|"+; &&CW
  "||||||||"+;&&column(s) CX-DE empty
  m.delivport+"|"+; &&DF
  m.shiptocode+"|"+; &&DG
  Iif (llStrtran,Strtran(m.shiptoname,","," "),m.shiptoname)+"|"+; &&DH
  Iif (llStrtran,Strtran(m.shiptoadd1,","," "),m.shiptoadd1)+"|"+; &&DI
  Iif (llStrtran,Strtran(m.shiptoadd2,","," "),m.shiptoadd2)+"|"+; &&DJ
  m.shiptocity+"|"+; &&DK
  m.shiptost+"|"+; &&DL
  m.shiptoctry+"|"+; &&DM
  m.shiptozip+; &&DN
  Chr(13)+Chr(10)


Endscan

If llStrtran
  xfilestr = Strtran(xfilestr,"|",",")
Endif

Set Date Mdy
If llStrtran = .T.
  xoutfile="PO_TGFDI_"+Strtran(Strtran(Strtran(Ttoc(Datetime()),":","")," ",""),"/")+"CALD"+".csv"
Else
  xoutfile="PO_TGFDI_"+Strtran(Strtran(Strtran(Ttoc(Datetime()),":","")," ",""),"/")+"CALD"+"_"+Alltrim(thispo)+"_"+".txt"
Endif
Set Date american

Strtofile(xfilestr,lcOutPath+xoutfile)

wait clear

