PARAMETERS cOffice
lEmail = .T.
lNewPT = .T.
CD &lcPath
lc997Path = ("f:\ftpusers\Nanjing\940translate\")

waitstr = "Processing records for "+cProperName+" - "+cOfficename
WAIT WINDOW waitstr TIMEOUT 1

set step on

ll940test = .f.
If ll940test
  xpath = "F:\0-picktickets\nanjing\test\"
  lnNum = Adir(tarray,xpath+"*.*")
Else
  lnNum = Adir(tarray,lcPath+"*.*")
Endif

*lnNum = ADIR(tarray,"*.*")
WAIT WINDOW "There are "+TRANSFORM(lnNum)+" files to process" TIMEOUT 2
IF lTestRun
	SET STEP ON
ENDIF

WAIT WINDOW "" TIMEOUT 2  && Added for timing problem resolution, 11.15.2016, Joe

IF lnNum = 0
	WAIT WINDOW AT 10,20 "No "+cProperName+" 940's to import for "+cOfficename TIMEOUT 3
	NormalExit = .T.
	THROW
ENDIF
*
FOR thisfile = 1  TO lnNum
	cFilename = tarray[thisfile,1]
	nFileSize = VAL(TRANSFORM(tarray(thisfile,2)))
	
  If ll940test
    xfile = xpath+ tarray[thisfile,1]
  Else
    xfile = lcPath+tarray[thisfile,1] && +"."
  endif
 &&xfile = lcPath+cFilename
	archivefile = lcArchivePath+cFilename
	in997file = ("F:\ftpusers\nanjing\997in\"+JUSTSTEM(xfile)+".997")
	in754file = ("F:\ftpusers\nanjing\754in\"+JUSTSTEM(xfile)+".txt")
	cStringIn = FILETOSTR(xfile)
	cFieldDelim = SUBSTR(cStringIn,4,1)
	FOR i = 1 TO LEN(cStringIn)
		IF SUBSTR(cStringIn,i,1) = ">"
			cSegDelim = SUBSTR(cStringIn,(i+1),1)
			EXIT
		ENDIF
	ENDFOR

	cStringIn = STRTRAN(cStringIn,cSegDelim,CHR(13))
	STRTOFILE(cStringIn,"f:\3pl\data\tempnan.txt")
	RELEASE ALL LIKE cStringIn,cSegDelim
	SELECT 0

	SELECT x856
	APPEND FROM "f:\3pl\data\tempnan.txt" TYPE DELIMITED WITH CHARACTER &cFieldDelim
	DELETE FILE "f:\3pl\data\tempnan.txt"
	RELEASE ALL LIKE cFieldDelim
	locate
	LOCATE FOR x856.segment = "GS"
	IF FOUND()
		DO CASE
			CASE x856.f1 = "FA"  && 997 file
				COPY FILE [&xfile] TO [&in997file]
				DELETE FILE [&xfile]
				LOOP
			CASE x856.f1 = "RG"
				COPY FILE [&xfile] TO [&in754file]
				DELETE FILE [&xfile]
				LOOP
		ENDCASE
	ENDIF

*	?xfile
	cFilename = tarray[thisfile,1]
	archivefile = lcArchivePath+tarray[thisfile,1]  &&+"."
	to997file = lc997Path+tarray[thisfile,1]
	WAIT WINDOW "Importing file: "+xfile NOWAIT
	IF FILE(xfile)
		lcStrx = FILETOSTR(xfile)
		cDelimiter = SUBSTR(lcStrx,4,1)
		RELEASE ALL LIKE lcStrx

*!* To load from a wrapped file snippet, uncomment the following section,
*!* and comment the "loadedifile" line below it
*	ASSERT .f. MESSAGE "at createx856a"
*	DO m:\dev\prg\createx856a
*	APPEND FROM f:\0-picktickets\nanjing\20120814141601a.940 TYPE DELIMITED WITH CHARACTER "*"

DO m:\dev\prg\loadedifile WITH xfile,cDelimiter,cTranslateOption,cCustName
*DO m:\dev\prg\loadedifile WITH xfile,cDelimiter,"NONE",cCustName

*!* Added 1/13/05 to recreate temp tables with each round
*!* Eliminates a temp file size issue
		IF USED('xptdet')
			USE IN xptdet
		ENDIF
		RELEASE ALL LIKE APT
		RELEASE ALL LIKE APTDET

		DO create_pt_cursors WITH cUseFolder,lLoadSQL && Added to prep for SQL Server transition
		SELECT xpt

		INDEX ON ship_ref TAG ship_ref
		INDEX ON ptid TAG ptid
		SET ORDER TO ship_ref

*!* Added this code to eliminate a special character problem
		cTransString = FILETOSTR(xfile)
		cTransString = STRTRAN(cTransString,"`","")
		STRTOFILE(cTransString,to997file)
		RELEASE ALL LIKE cTransString
*!* End of added code
		

		IF !lTestTableLoad
			COPY FILE [&xfile] TO [&archivefile]
		ENDIF

		DO (cUseDir+"BKDN") WITH cOffice,cFilename

		IF !l997
			DO m:\dev\prg\all940_import
*			release_ptvars()
		ENDIF

		l997 = .F.
	ENDIF

	IF USED('xpt')
		USE IN xpt
	ENDIF
	IF USED('xptdet')
		USE IN xptdet
	ENDIF

NEXT thisfile

&& now clean up the 940in archive files, delete for the upto the last 10 days
deletefile(lcArchivePath,IIF(DATE()<{^2016-05-01},20,20))

WAIT CLEAR
WAIT WINDOW "ALL "+UPPER(cProperName)+" 940 files finished processing for: "+cOfficename+"..." TIMEOUT 3

