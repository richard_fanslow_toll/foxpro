*!* m:\dev\prg\bioworld940_bkdn.prg
CLEAR
WAIT WINDOW "Now in BKDN Phase..." NOWAIT

lCheckStyle = .T.
lPick = .F.
lPrepack = .T.
units = IIF(lPick,.T.,.F.)

SELECT x856
COUNT FOR TRIM(segment) = "ST" TO nSegCnt
cSegCnt = ALLTRIM(STR(nSegCnt))

WAIT WINDOW "Now in 940 Breakdown" &message_timeout2
WAIT WINDOW "There are "+cSegCnt+" P/Ts in this file" &message_timeout2
SELECT x856
LOCATE

*!* Constants
m.acct_name = "BIOWORLD"
m.acct_num = nAcctNum
m.careof = " "
m.sf_addr1 = "2111 W. WALNUT HILL LANE"
m.sf_addr2 = ""
m.sf_csz = "IRVING, TX 75038"
m.printed = .F.
m.print_by = ""
m.printdate = DATE()
m.custsku = ""
m.size_scale = ""

lJCPMail = .T.
*!* End constants

*!*	If !Used("uploaddet")
*!*		If lTesting
*!*			Use F:\ediwhse\test\uploaddet In 0 Alias uploaddet
*!*		Else
*!*			Use F:\ediwhse\Data\uploaddet In 0 Shared Alias uploaddet
*!*		Endif
*!*	Endif

SELECT xpt
DELETE ALL
SELECT xptdet
DELETE ALL

STORE "" TO m.isa_num,m.shipins,m.color,m.id
STORE 0 TO ptctr,pt_total_eaches,pt_total_cartons,nCtnCount

SELECT x856
SET FILTER TO
GOTO TOP

WAIT WINDOW "NOW IN INITIAL HEADER BREAKDOWN" &message_timeout2

ptidctr    =0
ptdetidctr =0

nISA = 0  && To check for embedded FA envelopes.
lJCP = .F.
lcBYName ="UNK"

DO WHILE !EOF("x856")
	cSegment = TRIM(x856.segment)
	cCode = TRIM(x856.f1)

	IF TRIM(x856.segment) = "ISA"
		IF nISA = 0
			nISA = 1
			m.isa_num = ALLTRIM(x856.f13)
			cISA_num = m.isa_num
			cRecid = ALLTRIM(x856.f6)  && Added 08.05.2014, Joe
			SELECT x856
			SKIP
			LOOP
		ENDIF
	ENDIF

	IF (TRIM(x856.segment) = "GS") OR (TRIM(x856.segment) = "ST" AND TRIM(x856.f1) = "940")
		SELECT x856
		SKIP
		LOOP
	ENDIF


	IF (TRIM(x856.segment) = "W05")
		coffice='C'
		cMod = "5"
		m.office = coffice
		m.mod = cMod

		lHotTopic = .F.
		ptdetctr = 0
		ptidctr  = ptidctr    +1
		cCustnotes = ""
		m.vendor_num = ""
		nCtnCount = 0
		nSHCount = 1
		nDelCount = 1
		nDetCnt = 0
		SELECT xpt
		APPEND BLANK
		REPLACE xpt.shipins WITH "FILENAME*"+cFilename IN xpt
		REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"RECID*"+cRecid IN xpt
		REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"GROUPNAME*"+IIF(cRecid='079360796',"NOLAN","BIOWORLD") IN xpt
		REPLACE xpt.office WITH coffice IN xpt
		REPLACE xpt.MOD WITH cMod IN xpt
		REPLACE xpt.qty WITH 0 IN xpt
		REPLACE xpt.origqty WITH 0 IN xpt
		STORE 0 TO pt_total_eaches
		STORE 0 TO pt_total_cartons
		ptctr = ptctr +1
		WAIT WINDOW "Now processing PT# "+ ALLTRIM(x856.f2)+" Number "+CHR(13)+TRANSFORM(ptctr)+" of "+cSegCnt NOWAIT
		REPLACE xpt.ptid       WITH ptidctr IN xpt
		REPLACE xpt.accountid  WITH nAcctNum IN xpt
		REPLACE xpt.ptdate     WITH DATE() IN xpt
		m.ship_ref = ALLTRIM(x856.f2)
		m.cnee_ref = ALLTRIM(x856.f3)
		REPLACE xpt.ship_ref   WITH m.ship_ref IN xpt  && Pick Ticket
		REPLACE xpt.cnee_ref WITH m.cnee_ref IN xpt  && Pick Ticket
		m.pt = ALLTRIM(x856.f2)
		SELECT x856
		SKIP 1
		LOOP
	ENDIF

	IF TRIM(x856.segment) = "N1" AND INLIST(TRIM(x856.f1),"ST") && Ship-to data
		lN2 = .F.
*!*			If !lTesting
*!*				Select uploaddet
*!*				Locate
*!*				If !Empty(M.isa_num)
*!*					Append Blank
*!*					Replace uploaddet.office    With cOffice              In uploaddet
*!*					Replace uploaddet.cust_name With Upper(Trim(x856.f2)) In uploaddet
*!*					Replace uploaddet.acct_num  With m.acct_num           In uploaddet
*!*					Replace uploaddet.Date      With Datetime()           In uploaddet
*!*					Replace uploaddet.isa_num   With m.isa_num            In uploaddet
*!*					Replace uploaddet.startpt   With xpt.ship_ref         In uploaddet
*!*					Replace uploaddet.runid     With lnRunID              In uploaddet
*!*				Endif
*!*			Endif

		cConsignee = UPPER(STRTRAN(TRIM(x856.f2),"'","`"))  && sometimes the ship-to is a DC or Unit name So use the BT r BY for the Consignee
		REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"SHIPTO*"+cConsignee IN xpt
		IF lTesting
			REPLACE xpt.consignee WITH cConsignee IN xpt
		ENDIF

		cStoreNum = ALLTRIM(x856.f4)
		REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"STORENUM*"+cStoreNum IN xpt && Full-length DC Number

		IF ALLTRIM(STRTRAN(x856.f2," ","")) = ALLTRIM(STRTRAN(x856.f4," ",""))
			REPLACE xpt.consignee WITH lcBYName IN xpt
		ENDIF

&&    Replace xpt.sforstore  With cStoreNum In xpt
		REPLACE xpt.dcnum WITH cStoreNum IN xpt

		cStorenum1 = ALLTRIM(cStoreNum)
		DO WHILE LEFT(cStorenum1,1) = '0'
			cStorenum1 = ALLTRIM(SUBSTR(cStorenum1,2))
		ENDDO
		REPLACE xpt.storenum WITH INT(VAL(LEFT(cStorenum1,5))) IN xpt
		RELEASE cStorenum1

		SKIP 1 IN x856
		IF TRIM(x856.segment) = "N2"
			lN2 = .T.
			REPLACE xpt.consignee WITH ALLTRIM(x856.f1)  IN xpt
			SKIP 1 IN x856
*loop
		ENDIF

		IF TRIM(x856.segment) = "N3"  && address info
			m.st_addr1 = UPPER(TRIM(x856.f1))
			m.st_addr2 = UPPER(TRIM(x856.f2))
			REPLACE xpt.address  WITH m.st_addr1 IN xpt
			REPLACE xpt.address2 WITH IIF(lN2,cConsignee,m.st_addr2) IN xpt
		ENDIF
		SKIP 1 IN x856
		IF TRIM(x856.segment) = "N4"  && address info
			IF !EMPTY(TRIM(x856.f3))
				m.zipbar = TRIM(x856.f3)
			ENDIF
			IF !EMPTY(TRIM(x856.f2))
				REPLACE x856.f1 WITH STRTRAN(x856.f1,",","")  &&added 2/10/2017 TMARG to remove comma from city
				m.st_csz = UPPER(TRIM(x856.f1))+", "+UPPER(TRIM(x856.f2))+" "+TRIM(x856.f3)
			ELSE
				REPLACE x856.f1 WITH STRTRAN(x856.f1,",","")  &&added 2/10/2017 TMARG to remove comma from city
				m.st_csz = UPPER(TRIM(x856.f1))
			ENDIF
			REPLACE xpt.csz WITH m.st_csz IN xpt
			cCountry = ALLT(x856.f4)
			REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"COUNTRY*"+cCountry IN xpt
		ENDIF
		SELECT x856
		SKIP
		LOOP
	ENDIF

	IF TRIM(x856.segment) = "N1" AND TRIM(x856.f1)="SF"
		IF ALLTRIM(x856.f4) = "MIRA-LOMA"
			coffice = "L"
			cMod = "L"
		ELSE
			coffice = "C"
			cMod = "5"
		ENDIF

		gMasterOffice = coffice
		gOffice = cMod
		m.office = coffice
		m.mod = cMod
		replace xpt.office WITH m.office IN xpt NEXT 1
		replace xpt.mod WITH m.mod IN xpt NEXT 1
		
		REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"VENDORNAME*"+ALLTRIM(x856.f2) IN xpt
		IF INLIST(TRIM(x856.f3),"1","9")
			REPLACE xpt.duns WITH ALLTRIM(x856.f4) IN xpt
			REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"DUNSQUAL*"+ALLTRIM(x856.f3) IN xpt
			REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"DUNSNUMBER*"+ALLTRIM(x856.f4) IN xpt
		ENDIF
		SELECT x856
		SKIP
		LOOP
	ENDIF

	IF TRIM(x856.segment) = "N1" AND INLIST(TRIM(x856.f1),"BT","BY") && look for the Consignee as the Shipto might be a DC name
		IF TRIM(x856.f1)= "BT"
			REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"BILLTO*"+UPPER(STRTRAN(TRIM(x856.f2),"'","`")) IN xpt
			REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"BILLDC*"+TRIM(x856.f4) IN xpt
			REPLACE xpt.cacctnum WITH ALLTRIM(x856.f4) IN xpt
		ENDIF
		IF TRIM(x856.f1)= "BY"
			lcBYName = TRIM(x856.f2)
			REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"BYNAME*"+UPPER(STRTRAN(TRIM(x856.f2),"'","`")) IN xpt
			REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"N1-BY*"+TRIM(x856.f4) IN xpt
		ENDIF
*			REPLACE xpt.NAME      WITH ALLTRIM(x856.f2) IN xpt
		REPLACE xpt.consignee WITH ALLTRIM(x856.f2)  IN xpt
		IF "PENNEY"$xpt.consignee
			DO jcpmail
		ENDIF
	ENDIF
	lHotTopic = IIF(xpt.consignee = "HOT TOPIC",.T.,.F.)

	IF TRIM(x856.segment) = "N1" AND INLIST(TRIM(x856.f1),"Z7") && Ship-for data
		csfor_name = TRIM(x856.f2)
		csfor_name = STRTRAN(csfor_name," - ","-")
		csfor_name = STRTRAN(csfor_name,", ",",")
		csfor_name = STRTRAN(csfor_name,"#","")

		REPLACE xpt.shipfor WITH UPPER(csfor_name) IN xpt
		csforStoreNum = ALLTRIM(x856.f4)
		REPLACE xpt.sforstore WITH csforStoreNum IN xpt
		REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"SFSTOREFULL*"+csforStoreNum IN xpt

		SKIP 1 IN x856
		IF TRIM(x856.segment) = "N3"  && address info
			m.sforaddr1 = UPPER(TRIM(x856.f1))
			m.sforaddr2 = UPPER(TRIM(x856.f2))
			REPLACE xpt.sforaddr1 WITH m.sforaddr1 IN xpt
			REPLACE xpt.sforaddr2 WITH m.sforaddr2 IN xpt
		ENDIF
		SKIP 1 IN x856
		IF TRIM(x856.segment) = "N4"  && address info
			IF !EMPTY(TRIM(x856.f2))
				REPLACE x856.f1 WITH STRTRAN(x856.f1,",","")  &&added 2/10/2017 TMARG to remove comma from city
				m.sforcsz = UPPER(TRIM(x856.f1))+", "+UPPER(TRIM(x856.f2))+" "+TRIM(x856.f3)
			ELSE
				REPLACE x856.f1 WITH STRTRAN(x856.f1,",","")  &&added 2/10/2017 TMARG to remove comma from city
				m.sforcsz = UPPER(TRIM(x856.f1))
			ENDIF
			REPLACE xpt.sforcsz WITH m.sforcsz IN xpt
			cCountry = ALLT(x856.f4)
			IF !EMPTY(cCountry)
				REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"SFCOUNTRY*"+cCountry IN xpt
			ENDIF
		ENDIF
		SELECT x856
		SKIP
		LOOP
	ENDIF

	IF TRIM(x856.segment) = "N9"
		DO CASE
			CASE TRIM(x856.f1) = "IA"
				REPLACE xpt.vendor_num WITH ALLT(x856.f2)
				REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"VENDORNUM*"+ALLT(x856.f2) IN xpt
				SELECT x856
				SKIP
				LOOP

			CASE TRIM(x856.f1) = "BT"
				REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"BUSUNIT*"+ALLT(x856.f2) IN xpt
				SELECT x856
				SKIP
				LOOP

			CASE TRIM(x856.f1) = "RU"  && Added per Siggy for Toys R Us
				REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"ROUTENUM*"+ALLT(x856.f2) IN xpt
				SELECT x856
				SKIP
				LOOP

			CASE TRIM(x856.f1) = "RL"
				REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"RLNUM*"+ALLT(x856.f2) IN xpt
				SELECT x856
				SKIP
				LOOP

			CASE TRIM(x856.f1) = "PHC"
				REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"PHCNUM*"+ALLT(x856.f2) IN xpt
				SELECT x856
				SKIP
				LOOP

			CASE TRIM(x856.f1) = "ID"
				cIDNum = ALLTRIM(x856.f2)
				REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"IDNUM*"+cIDNum IN xpt
				SELECT x856
				SKIP
				LOOP

			CASE TRIM(x856.f1) = "IZ"
				cMeyerType = ALLTRIM(x856.f2)
				REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"MEYERTYPE*"+cMeyerType  IN xpt
				SELECT x856
				SKIP
				LOOP

			CASE TRIM(x856.f1) = "DP"
				m.dept = ALLT(x856.f2)
				cFOB = ALLT(x856.f3)
				REPLACE xpt.dept WITH m.dept IN xpt  && Dept
				cDeptName = ALLT(x856.f3)
				REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"DEPT*"+m.dept IN xpt
				REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"DEPTNAME*"+cDeptName IN xpt
				REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"FOB*"+cFOB IN xpt
				SELECT x856
				SKIP
				LOOP

			CASE TRIM(x856.f1) = "LU"
				cLocCode = ALLT(x856.f2)
				REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"LOCCODE*"+cLocCode IN xpt
				IF !EMPTY(x856.f3)
					cStorename = ALLT(x856.f3)
					REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"STORENAME*"+cStorename IN xpt
				ENDIF
				SELECT x856
				SKIP
				LOOP

			CASE TRIM(x856.f1) = "CT"
				lcCode = ALLT(x856.f2)
				REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"CT*"+lcCode IN xpt
				SELECT x856
				SKIP
				LOOP

			CASE TRIM(x856.f1) = "19"
				lcCode = ALLT(x856.f2)
				REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"DIVCODE*"+lcCode IN xpt
				REPLACE xpt.consignee WITH ALLTRIM(xpt.consignee)+"-"+ALLTRIM(lcCode) IN xpt
				SELECT x856
				SKIP
				LOOP

			CASE TRIM(x856.f1) = "MR"
				cMR = ALLT(x856.f2)
				REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"MR*"+cMR IN xpt
				SELECT x856
				SKIP
				LOOP

			CASE TRIM(x856.f1) = "IA"
				m.cacctnum = ALLT(x856.f2)
				REPLACE xpt.cacctnum WITH m.cacctnum IN xpt
				SELECT x856
				SKIP
				LOOP

			CASE TRIM(x856.f1) = "ZM"
				nAcctNum = IIF(x856.f2 = "NOL",6554,6182)
				REPLACE xpt.accountid WITH nAcctNum IN xpt
				cZM = ALLT(x856.f2)
				REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"COID*"+cZM IN xpt
				SELECT x856
				SKIP
				LOOP

			CASE TRIM(x856.f1) = "XY" && ECOMM order identifier segment
				cEcomm = ALLT(x856.f2)
				REPLACE xpt.shipins WITH "++ ECOMM ORDER ++"+CHR(13)+xpt.shipins IN xpt
				SELECT x856
				SKIP
				LOOP
		ENDCASE
	ENDIF

	IF TRIM(x856.segment) = "G62" AND TRIM(x856.f1) == "10"  && Start date
		lcDate = ALLTRIM(x856.f2)
		lxDate = SUBSTR(lcDate,5,2)+"/"+SUBSTR(lcDate,7,2)+"/"+LEFT(lcDate,4)
		ldDate = CTOD(lxDate)
		REPLACE xpt.START WITH ldDate
		SELECT x856
		SKIP
		LOOP
	ENDIF

	IF TRIM(x856.segment) = "G62" AND TRIM(x856.f1) == "01"  && cancel date
		lcDate = ALLTRIM(x856.f2)
		lxDate = SUBSTR(lcDate,5,2)+"/"+SUBSTR(lcDate,7,2)+"/"+LEFT(lcDate,4)
		ldDate = CTOD(lxDate)
		REPLACE xpt.CANCEL WITH ldDate
		SELECT x856
		SKIP
		LOOP
	ENDIF

	IF TRIM(x856.segment) = "G62" AND TRIM(x856.f1) == "04"  && purchase order date
		lxDate = ALLTRIM(x856.f2)
		REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"PODATE*"+lxDate IN xpt
		SELECT x856
		SKIP
		LOOP
	ENDIF

	IF TRIM(x856.segment) = "G62" AND TRIM(x856.f1) == "54"  && requested ship date
		lxDate = ALLTRIM(x856.f2)
		REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"DNLDATE*"+lxDate IN xpt  && DELIVER NO LATER THAN DATE
		SELECT x856
		SKIP
		LOOP
	ENDIF

	IF TRIM(x856.segment) = "NTE"  && Warehouse handling notes
		IF TRIM(x856.f1) = "COM"
			REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"COMPANY*"+ALLTRIM(x856.f2) IN xpt
*!*				CASE TRIM(x856.f1) = "DEL"
*!*					m.del = "DELIV.NOTES"+ALLT(STR(nDelCount))+"*"+ALLTRIM(x856.f2)  && ADDED THE ENUMERATION OF DEL NOTES 4/3/2014
*!*					nDelCount = nDelCount + 1
*!*					REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+m.del IN xpt
*!*				OTHERWISE && TRIM(x856.f1) = "WHI"
		ELSE
			m.sh = "SPINS"+ALLT(STR(nSHCount))+"*"+ALLTRIM(x856.f2)  && ADDED THE ENUMERATION OF THE NOTES 4/17/2008
			nSHCount = nSHCount + 1
			REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+m.sh IN xpt
		ENDIF
		SELECT x856
		SKIP
		LOOP
	ENDIF

	IF TRIM(x856.segment) = "W66" && usually, ship_via info
		IF !EMPTY(x856.f5)
			REPLACE xpt.ship_via WITH ALLT(x856.f5)
			m.sh = "SPINS"+ALLT(STR(nSHCount))+"*SHIP VIA: "+ALLTRIM(x856.f5)  &&
			nSHCount = nSHCount + 1
			REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+m.sh
		ENDIF
		SELECT x856
		SKIP
		LOOP
	ENDIF

	IF TRIM(x856.segment) = "LX" && start of PT detail, stay here until this PT is complete
		WAIT "NOW IN DETAIL LOOP PROCESSING" WINDOW NOWAIT
		SELECT xptdet
		CALCULATE MAX(ptdetid) TO m.ptdetid
		m.ptdetid = m.ptdetid + 1
		m.printstuff = ""
		m.ptid = xpt.ptid
		m.accountid = nAcctNum
		m.units = .F.
		SKIP 1 IN x856

		llStoredtheStorenum = .F.

		cStyle = "XYZ"
		STORE "" TO lcSize,lcStyle,lcColor,lcQty,lcUPC,lcSKU,lcDesc
		IF lTesting
*SET STEP ON
		ENDIF
		DO WHILE ALLTRIM(x856.segment) != "SE"
			cSegment = TRIM(x856.segment)
			cCode = TRIM(x856.f1)

			DO CASE

				CASE TRIM(x856.segment) = "LX"
					m.ptdetid = m.ptdetid + 1
					SELECT xpt
					SCATTER MEMVAR MEMO BLANK FIELDS EXCEPT ptdetid,ptid,accountid
					m.id =""

				CASE TRIM(x856.segment) = "W01" && Destination Quantity
					SELECT x856
					nRec1 = RECNO()
					DO WHILE TRIM(x856.segment) # "W20"
						SKIP 1 IN x856
					ENDDO
					IF TRIM(x856.segment)="W20"
						cUnitofMeas = ALLTRIM(x856.f3)
						cUnitsType = "UNITCODE*"+ALLTRIM(x856.f3)
						lPrepack = IIF(cUnitsType = "CA",.T.,.F.)
					ENDIF
					GO nRec1

					m.totqty = INT(VAL(x856.f1))
					IF EMPTY(m.printstuff)
						m.printstuff = "ORIGQTY*"+ALLTRIM(x856.f1)
					ELSE
						m.printstuff = m.printstuff+CHR(13)+"ORIGQTY*"+ALLTRIM(x856.f1)
					ENDIF

					IF ALLTRIM(x856.f4) = "VN"
						m.style = ALLTRIM(x856.f5)
						STORE m.style TO cStyle
					ENDIF
					lcQty   = ALLTRIM(x856.f1)
					lcStyle = ALLTRIM(x856.f5)
					lcSKU   = ALLTRIM(x856.f7)
					cUPC   = ALLTRIM(x856.f16)
					m.upc   = lcUPC
					m.custsku = ALLTRIM(x856.f7)
					IF lTesting

					ENDIF
					m.printstuff = m.printstuff+CHR(13)+"CUSTSKU*"+ALLTRIM(x856.f7)

					IF EMPTY(cUPC)
						m.upc = ALLTRIM(x856.f7)  && preload this to UPC incase the UP qualifier is not used.
						cUPC = ALLTRIM(x856.f7)
					ENDIF


* pg CHANGED THIS 5/28/2014 not sure if this is correct

					cUPCCode = ""
					IF INLIST(ALLTRIM(x856.f15),"UP","UK")
						cUPCCode = ALLTRIM(x856.f15)
						m.printstuff = m.printstuff+CHR(13)+"UPCCODE*"+cUPCCode
						m.upc = ALLTRIM(x856.f16)
						cUPC = ALLTRIM(x856.f16)
						IF ALLTRIM(x856.f15)= "UK"
							m.printstuff = m.printstuff+CHR(13)+"GTIN*"+cUPC
							cUPC = ""
						ENDIF
					ENDIF
					IF !EMPTY(cUPC) AND cUPCCode#"UK"
						m.printstuff = m.printstuff+CHR(13)+"ORIGUPC*"+cUPC && added to avoid losing the UPC data to CUSTSKU
					ENDIF

				CASE ALLTRIM(x856.segment) = "N9" AND ALLTRIM(x856.f1) = "LI"
					m.linenum = ALLTRIM(x856.f2)

				CASE ALLTRIM(x856.segment) = "NTE" AND ALLTRIM(x856.f1) = "WHI"  AND SUBSTR(ALLTRIM(x856.f2),1,2) != "00"
					lcSize = ALLTRIM(x856.f2)
					m.printstuff = m.printstuff+CHR(13)+"ITEMSIZE*"+lcSize

				CASE ALLTRIM(x856.segment) = "N9" AND TRIM(x856.f1) = "PC"
					m.printstuff = m.printstuff+CHR(13)+"INNERPACK*"+ALLT(x856.f2)

				CASE ALLTRIM(x856.segment) = "N9" AND ALLTRIM(x856.f1) = "PK"
					m.printstuff = (m.printstuff+CHR(13)+"PACKBKDN*"+ALLTRIM(x856.f2))
					m.sizescale = ""
					m.size_scale = ALLT(x856.f2)

				CASE ALLTRIM(x856.segment) = "N9" AND TRIM(x856.f1) = "PG"
					cLabelClass = ALLTRIM(x856.f2)
					m.printstuff = (m.printstuff+CHR(13)+"LABELCLASS*"+cLabelClass)

				CASE ALLTRIM(x856.segment) = "N9" AND TRIM(x856.f1) = "DP"
					cDeptDesc = ALLTRIM(x856.f2)
					m.printstuff = (m.printstuff+CHR(13)+"DEPTDESC*"+cDeptDesc)

				CASE ALLTRIM(x856.segment) = "N9" AND ALLTRIM(x856.f1) = "IZ" && Unknown
					lcSize = ALLTRIM(x856.f2)
					m.printstuff = (m.printstuff+CHR(13)+"BUYERSIZE*"+ALLTRIM(x856.f2))

				CASE ALLTRIM(x856.segment) = "N9" AND TRIM(x856.f1) = "CL"
					m.printstuff = m.printstuff+CHR(13)+"ORIGCOLOR*"+ALLT(x856.f2)

				CASE ALLTRIM(x856.segment) = "N9" AND TRIM(x856.f1) = "LB"
					m.printstuff = m.printstuff+CHR(13)+"SPECWT*"+ALLT(x856.f2)

				CASE ALLTRIM(x856.segment) = "N9" AND ALLTRIM(x856.f1) = "PO"
					IF "TARGET"$xpt.consignee AND ALLTRIM(x856.f2) = "NOT_TAGGED"
						m.color = ""
					ELSE
						m.color = ALLTRIM(x856.f2)
					ENDIF

				CASE ALLTRIM(x856.segment) = "N9" AND ALLTRIM(x856.f1) = "CO"
					m.printstuff = (m.printstuff+CHR(13)+"COO*"+ALLTRIM(x856.f2))

				CASE ALLTRIM(x856.segment) = "N9" AND ALLTRIM(x856.f1) = "SR"
					m.printstuff = (m.printstuff+CHR(13)+"STOREREADY*"+ALLTRIM(x856.f2))

				CASE ALLTRIM(x856.segment) = "N9" AND ALLTRIM(x856.f1) = "PR"
					IF ALLTRIM(x856.f2) = "YES"
						m.printstuff = (m.printstuff+CHR(13)+"PRETICKET*"+ALLTRIM(x856.f2))
					ENDIF

				CASE ALLTRIM(x856.segment) = "N9" AND ALLTRIM(x856.f1) = "DS"
					m.printstuff = (m.printstuff+CHR(13)+"DS*"+ALLTRIM(x856.f2))

				CASE ALLTRIM(x856.segment) = "N9" AND ALLTRIM(x856.f1) = "ZZ" && Unknown
					m.printstuff = (m.printstuff+CHR(13)+"SIZE*"+ALLTRIM(x856.f2))
					m.id = ""  &&ALLTRIM(x856.f2)

				CASE TRIM(x856.segment) = "G69" && shipstyle
					cDesc = "DESC*"+UPPER(ALLTRIM(x856.f1))
					lcDesc =UPPER(ALLTRIM(x856.f1))

				CASE TRIM(x856.segment) = "W20"  && not sure what to do here so if there is a difference its easy to change PG 08-06-13
					lcColor = ALLTRIM(x856.f10)
					m.pack = "1"
					lnInnerPackValue = 1
					m.casepack = INT(VAL(m.pack))
					cInnerPack1 = "W20_1*"+ALLTRIM(x856.f1)
					cInnerPack2 = "W20_2*"+ALLTRIM(x856.f2)
					cUOM = "ORIG_UOM*"+ALLTRIM(x856.f3)
					cOrigWt = "ORIG_WT*"+ALLTRIM(x856.f4)
					cUOW = "ORIG_UOW*"+IIF(ALLTRIM(x856.f5)='L','LB','KG')
					cOrigVol = "ORIG_VOL*"+ALLTRIM(x856.f8)
					cUOV = "ORIG_UOV*"+ALLTRIM(x856.f9)

					IF !EOF()
						SKIP 1 IN x856
						IF TRIM(x856.segment) = "AMT"
							m.printstuff = (m.printstuff+CHR(13)+"PRICE*"+ALLTRIM(x856.f2))
						ELSE
							SKIP -1 IN x856
						ENDIF
					ENDIF

					m.printstuff = "FILENAME*"+cFilename+CHR(13)+m.printstuff
					SELECT xptdet
					APPEND BLANK
					ptdetidctr = ptdetidctr +1
					REPLACE xptdet.office WITH coffice IN xptdet
					REPLACE xptdet.MOD WITH cMod IN xptdet
					IF cUnitofMeas = "CA"
						REPLACE xptdet.units      WITH .F.        IN xptdet
						REPLACE xptdet.ptid       WITH ptidctr    IN xptdet
						REPLACE xptdet.ptdetid    WITH ptdetidctr IN xptdet
						REPLACE xptdet.accountid  WITH xpt.accountid IN xptdet
						REPLACE xptdet.STYLE      WITH m.style    IN xptdet
						REPLACE xptdet.COLOR      WITH m.color    IN xptdet
						IF lHotTopic
							REPLACE xptdet.ID         WITH ALLTRIM(xpt.cnee_ref) IN xptdet
						ELSE
							REPLACE xptdet.ID         WITH m.id IN xptdet
						ENDIF
						REPLACE xptdet.PACK       WITH "1"        IN xptdet
						REPLACE xptdet.custsku    WITH m.custsku  IN xptdet
						REPLACE xptdet.casepack   WITH 1          IN xptdet
						REPLACE xptdet.linenum    WITH m.linenum  IN xptdet
						REPLACE xptdet.upc        WITH m.upc      IN xptdet
						REPLACE xptdet.totqty     WITH m.totqty   IN xptdet
						REPLACE xptdet.size_scale WITH m.size_scale IN xptdet
						REPLACE xptdet.origqty    WITH xptdet.totqty IN xptdet
						REPLACE xptdet.printstuff WITH m.printstuff IN xptdet
						REPLACE xptdet.printstuff WITH xptdet.printstuff +CHR(13)+cDesc IN xptdet
						REPLACE xptdet.printstuff WITH xptdet.printstuff +CHR(13)+cInnerPack1 IN xptdet
						REPLACE xptdet.printstuff WITH xptdet.printstuff +CHR(13)+cInnerPack2 IN xptdet
						REPLACE xptdet.printstuff WITH xptdet.printstuff +CHR(13)+cUnitsType IN xptdet
						REPLACE xptdet.printstuff WITH xptdet.printstuff +CHR(13)+cUOM IN xptdet
						REPLACE xptdet.printstuff WITH xptdet.printstuff +CHR(13)+cOrigWt IN xptdet
						REPLACE xptdet.printstuff WITH xptdet.printstuff +CHR(13)+cUOW IN xptdet
						REPLACE xptdet.printstuff WITH xptdet.printstuff +CHR(13)+cOrigVol IN xptdet
						REPLACE xptdet.printstuff WITH xptdet.printstuff +CHR(13)+cUOV IN xptdet
						REPLACE xptdet.printstuff WITH xptdet.printstuff +CHR(13)+"SUB*"+ALLTRIM(lcStyle)+"%"+ALLTRIM(lcColor)+"%"+ALLTRIM(lcSize)+"%"+ALLTRIM(lcQty)+"%"+ALLTRIM(lcUPC)+"%"+ALLTRIM(lcSKU)+"%"+ALLTRIM(lcDesc)
					ELSE
						REPLACE xptdet.units      WITH .T.        IN xptdet
						REPLACE xptdet.ptid       WITH ptidctr    IN xptdet
						REPLACE xptdet.ptdetid    WITH ptdetidctr IN xptdet
						REPLACE xptdet.accountid  WITH xpt.accountid IN xptdet
						REPLACE xptdet.STYLE      WITH m.style    IN xptdet
						REPLACE xptdet.COLOR      WITH m.color    IN xptdet
						IF lHotTopic
							REPLACE xptdet.ID         WITH ALLTRIM(xpt.cnee_ref) IN xptdet
						ELSE
							REPLACE xptdet.ID         WITH m.id IN xptdet
						ENDIF
						REPLACE xptdet.PACK       WITH "1"        IN xptdet
						REPLACE xptdet.custsku    WITH m.custsku  IN xptdet
						REPLACE xptdet.casepack   WITH 1          IN xptdet
						REPLACE xptdet.linenum    WITH m.linenum  IN xptdet
						REPLACE xptdet.upc        WITH m.upc      IN xptdet
						REPLACE xptdet.totqty     WITH m.totqty   IN xptdet
						REPLACE xptdet.origqty    WITH xptdet.totqty IN xptdet
						REPLACE xptdet.size_scale WITH m.size_scale IN xptdet
						REPLACE xptdet.printstuff WITH m.printstuff IN xptdet
						REPLACE xptdet.printstuff WITH xptdet.printstuff +CHR(13)+cDesc IN xptdet
						REPLACE xptdet.printstuff WITH xptdet.printstuff +CHR(13)+cInnerPack1 IN xptdet
						REPLACE xptdet.printstuff WITH xptdet.printstuff +CHR(13)+cInnerPack2 IN xptdet
						REPLACE xptdet.printstuff WITH xptdet.printstuff +CHR(13)+cUnitsType  IN xptdet
						REPLACE xptdet.printstuff WITH xptdet.printstuff +CHR(13)+cUOM IN xptdet
						REPLACE xptdet.printstuff WITH xptdet.printstuff +CHR(13)+cOrigWt IN xptdet
						REPLACE xptdet.printstuff WITH xptdet.printstuff +CHR(13)+cUOW IN xptdet
						REPLACE xptdet.printstuff WITH xptdet.printstuff +CHR(13)+cOrigVol IN xptdet
						REPLACE xptdet.printstuff WITH xptdet.printstuff +CHR(13)+cUOV IN xptdet
						REPLACE xptdet.printstuff WITH xptdet.printstuff +CHR(13)+"SUB*"+ALLTRIM(lcStyle)+"%"+ALLTRIM(lcColor)+"%"+ALLTRIM(lcSize)+"%"+ALLTRIM(lcQty)+"%"+ALLTRIM(lcUPC)+"%"+ALLTRIM(lcSKU)+"%"+ALLTRIM(lcDesc)
					ENDIF
*SET STEP ON
					SELECT upcdet
					LOCATE FOR ALLTRIM(upc) = ALLTRIM(xptdet.upc)
					IF !FOUND()
						SELECT upcdet
						SCATTER MEMVAR MEMO BLANK
						m.accountid=6182
						m.upc=xptdet.upc
						m.style=lcStyle
						m.color=lcColor
						m.size=lcSize
						m.sku=lcSKU
						m.desc=lcDesc
						insertinto("upcdet","wh",.T.)
					ENDIF

					STORE "" TO m.printstuff,m.style,m.color,m.pack,m.custsku,lcStyle,lcColor,lcSize,lcDesc,lcUPC
					IF lTesting
*BROWSE
					ENDIF

					SELECT x856

				CASE TRIM(x856.segment) = "W76" && Ctn Qty
					nCheckQty = INT(VAL(x856.f1))
					IF nCheckQty # xpt.qty
						REPLACE xpt.qty WITH nCheckQty IN xpt
						REPLACE xpt.origqty WITH xpt.qty IN xpt
					ENDIF
			ENDCASE

			SKIP 1 IN x856
		ENDDO

		SELECT xptdet
	ENDIF

	SELECT x856
	IF !EOF()
		SKIP 1 IN x856
	ENDIF
ENDDO

SELECT xptdet
*!* Added the rollup process due to Bioworld changing 940 format and sending dup. detail lines. 10.13.2015
rollup()
SELECT xptdet
REPLACE xptdet.origqty WITH xptdet.totqty ALL IN xptdet
DO m:\dev\prg\setuppercase940

WAIT WINDOW "At end of ROLLUP procedure" NOWAIT

IF lBrowfiles
	WAIT WINDOW "Now browsing XPT/XPTDET files" &message_timeout2
	SELECT xpt
	LOCATE
	BROWSE
	SELECT xptdet
	LOCATE
	BROWSE
	IF MESSAGEBOX("Do you wish to continue?",4+16+256,"CONTINUE")=7
		CANCEL
		EXIT
	ENDIF
ENDIF

WAIT cCustname+" Breakdown Round complete..." WINDOW &message_timeout2
IF lJCP
	DO jcpmail
ENDIF

SELECT x856
IF USED("PT")
	USE IN pt
ENDIF
IF USED("PTDET")
	USE IN ptdet
ENDIF
WAIT CLEAR
RETURN

**********************
PROCEDURE jcpmail
**********************
	IF lJCPMail
		SELECT 0
		USE F:\3pl\DATA\mailmaster ALIAS mm SHARED
		LOCATE FOR edi_type = "MISC" AND taskname = "GENERAL"
		tsendtojcp = ALLTRIM(IIF(mm.use_alt,mm.sendtoalt,mm.sendto))
		tccjcp = ALLTRIM(IIF(mm.use_alt,mm.ccalt,mm.cc))
		USE IN mm
		cMailLoc = IIF(coffice = "C","West",IIF(coffice = "N","New Jersey","Florida"))
		tsubjectjcp = cCustname+" JCPenney Pickticket Upload: " +TTOC(DATETIME())
		tattach = ""
		tmessagejcp = "Uploading Consignee: JCPenney Picktickets for "+cCustname
		tmessagejcp = tmessagejcp+CHR(13)+"Filename: "+cFilename
		tmessagejcp = tmessagejcp+CHR(13)+"Check PT table for correct UOM and BT number stored"
		tFrom ="Toll EDI Operations <toll-edi-ops@tollgroup.com>"
		DO FORM m:\dev\frm\dartmail2 WITH tsendtojcp,tFrom,tsubjectjcp,tccjcp,tattach,tmessagejcp,"A"
		lJCPMail = .F.
	ENDIF
ENDPROC

******************************
PROCEDURE rollup
******************************
	ASSERT .F. MESSAGE "In rollup procedure"
	nChangedRecs = 0
	SELECT xptdet
	SET DELETED ON
	LOCATE
	cMsgStr = "PT/OSID/ODIDs changed: "
	nTotqty1 = 0
	SCAN FOR !DELETED()
		SCATTER MEMVAR
		nRecno = RECNO()
*!* Added some code to run through each line item rather than assuming only one duplicated line is involved.
		SKIP 1 IN xptdet
		DO WHILE !EOF()
			IF xptdet.ptid = m.ptid ;
					AND xptdet.STYLE = m.style ;
					AND xptdet.COLOR = m.color ;
					AND xptdet.ID = m.id ;
					AND xptdet.PACK = m.pack ;
					AND xptdet.upc = m.upc
				nTotqty1 = nTotqty1+xptdet.totqty
				DELETE NEXT 1 IN xptdet
				nChangedRecs = nChangedRecs+1
			ENDIF
			SKIP 1 IN xptdet
		ENDDO
		GO nRecno
		REPLACE xptdet.totqty WITH xptdet.totqty+nTotqty1 IN xptdet
		nTotqty1 = 0
	ENDSCAN

	IF nChangedRecs > 0
		WAIT WINDOW "There were "+TRANS(nChangedRecs)+" records rolled up" TIMEOUT 1
	ELSE
		WAIT WINDOW "There were NO records rolled up" TIMEOUT 1
	ENDIF

ENDPROC
