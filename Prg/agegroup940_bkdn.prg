*!* m:\dev\prg\agegroup_bkdn.prg

IF nAcctNum = 6769
WAIT WINDOW "This is an Essential Brands SP 940 upload" TIMEOUT 2
else
WAIT WINDOW "This is an Age Group-"+IIF(cOffice = "Y","Carson II","New Jersey")+" 940 upload" TIMEOUT 2
endif
CLEAR

lcheckstyle = .T.
lprepack = .T.
lpick = .F.
lsolidpack = .F.
ljcp = .F.
lfederated = .F.
units = .F.
ptctr = 0
m.isa_num = ""
m.color=""
cdcnum = ""

lcerrmessage =" "

SELECT ag940file
IF lTesting && OR lOverridebusy
	LOCATE
	BROWSE TIMEOUT 60
ENDIF

SELECT DISTINCT ship_ref FROM ag940file INTO CURSOR tempxx1
csegcnt = ALLTRIM(STR(RECCOUNT('tempxx1')))
USE IN tempxx1

WAIT WINDOW "Now in 940 Breakdown" TIMEOUT 1
WAIT "There are "+csegcnt+" P/Ts in this file" WINDOW TIMEOUT 1
SELECT ag940file
LOCATE

*!* Constants
m.acct_name = ccustname
m.acct_num = nacctnum
m.careof = " "
m.sf_addr1 = "C/O TGF INC."
m.sf_addr2 = "1000 E 223RD ST"
m.sf_csz = "CARSON, CA 90745"
m.printed = .F.
m.print_by = ""
m.printdate = DATE()
*!* End constants

SELECT xpt
SCATTER MEMVAR MEMO BLANK
DELETE ALL
SELECT xptdet
SCATTER MEMVAR MEMO BLANK
DELETE ALL

STORE 0 TO pt_total_eaches
STORE 0 TO pt_total_cartons
STORE 0 TO nctncount

WAIT WINDOW "NOW IN INITIAL HEADER BREAKDOWN" TIMEOUT 1
WAIT WINDOW "NOW IN INITIAL HEADER BREAKDOWN" NOWAIT

cship_ref = "XYZ"
m.accountid = nacctnum
m.ptdate = DATE()
m.addby = "TOLLPROC"
m.adddt = DATETIME()
m.addproc = "AG940"
STORE 0 TO m.ptid,m.ptdetid
SELECT xpt
SCATTER MEMVAR MEMO
SELECT xptdet
SCATTER MEMVAR MEMO


CREATE CURSOR tempmissupcs (pickticket c(20),upc c(20),filename c(50))
m.filename = cfilename

SELECT ag940file  && the template file for the CSV file to be loaded into
LOCATE

*!*	NOTE: NO "SET STEP ON" inside SCAN loops

SCAN
	SCATTER FIELDS origupc,ship_ref MEMVAR
	cUPC = ALLTRIM(m.origupc)
	m.pickticket = m.ship_ref
	RELEASE m.ship_ref

	IF lTesting = .T.
		useca("UPCMAST","wh",,,"a-1285")
	ELSE
		sq1 = [select style from upcmast where upc = ']+cUPC+[' and accountid = ]+TRANSFORM(nAcctNum)
		xsqlexec(sq1,"upcstyle1",,"wh")
	ENDIF
	LOCATE
	IF EOF()
		INSERT INTO tempmissupcs FROM MEMVAR
	ENDIF
ENDSCAN
RELEASE sq1

SELECT tempmissupcs
LOCATE
*******************************************************************************************************
** if there are missing UPCs in the style master send an email and abort the upload
IF !EOF()
	cexcelfile = ("h:\fox\missingupcs_"+RIGHT(TTOC(DATETIME(),1),6)+".xls")
	COPY TO (cexcelfile) TYPE XL5

	cmailloc = ICASE(cOffice = "M","Florida",INLIST(cOffice,"N","I"),"New Jersey","Carson II")

	IF ltestmail
		tsendto = "joe.bianchi@tollgroup.com"
		tcc = ""
	ELSE
		tsendto = tsendtostyle
		tcc = tccstyle
	ENDIF
	tsubject= "TGF "+cmailloc+" "+ccustname+" Style Master Issues:" +TTOC(DATETIME())
	tattach = cexcelfile
	tmessage = "The attached file contains UPC(s) which are not in Toll's Style Master"
	tmessage = tmessage+CHR(13)+"Please send style updates for these, and retransmit the 940 file."
	tfrom ="TGF Warehouse Operations <toll-warehouse-ops@tollgroup.com>"
	IF ltestmail
		tmessage = tmessage+CHR(13)+CHR(13)+"THIS IS A TEST OF THE MISSING UPC MAILING"
	ENDIF

	DO FORM m:\dev\frm\dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"

	DELETE FILE &cexcelfile

	COPY FILE [&xfile] TO [&holdfile]
	DELETE FILE [&xfile]
	ldoimport = .F.
	RETURN
ENDIF
*******************************************************************************************************
** file in the xpt and xptdet cursors

SELECT ag940file
LOCATE
SELECT DISTINCT ship_ref FROM ag940file INTO CURSOR temppt
LOCATE

SELECT temppt
LOCATE

SCAN
	cship_ref = temppt.ship_ref
	SELECT ag940file
	LOCATE FOR ag940file.ship_ref = cship_ref
	SCATTER MEMVAR MEMO
	m.accountid = nacctnum
	m.shipins = "FILENAME*"+cfilename+CHR(13)+ALLTRIM(m.shipins)

	m.shipins = m.shipins+CHR(13)+"STFNAME*"+ALLTRIM(m.stfn)
	m.shipins = m.shipins+CHR(13)+"STLNAME*"+ALLTRIM(m.stln)
	m.shipins = m.shipins+CHR(13)+"STCITY*"+ALLTRIM(m.stcity)
	m.shipins = m.shipins+CHR(13)+"STSTATE*"+ALLTRIM(m.ststate)
	m.shipins = m.shipins+CHR(13)+"STZIP*"+ALLTRIM(m.stzip)
	m.shipins = m.shipins+CHR(13)+"STCNTRY*"+ALLTRIM(m.country)
	m.shipins = m.shipins+CHR(13)+"STPHONE*"+ALLTRIM(m.stphone)
	m.shipins = m.shipins+CHR(13)+"STEMAIL*"+ALLTRIM(m.stemail)

	m.shipins = m.shipins+CHR(13)+"BTFNAME*"+ALLTRIM(m.btfn)
	m.shipins = m.shipins+CHR(13)+"BTLNAME*"+ALLTRIM(m.btln)
	m.shipins = m.shipins+CHR(13)+"BTCONAME*"+ALLTRIM(m.billto)
	m.shipins = m.shipins+CHR(13)+"BTADDR1*"+ALLTRIM(m.btaddr1)
	m.shipins = m.shipins+CHR(13)+"BTADDR2*"+ALLTRIM(m.btaddr2)
	m.shipins = m.shipins+CHR(13)+"BTCITY*"+ALLTRIM(m.btcity)
	m.shipins = m.shipins+CHR(13)+"BTSTATE*"+ALLTRIM(m.btstate)
	m.shipins = m.shipins+CHR(13)+"BTZIP*"+ALLTRIM(m.btzip)
	m.shipins = m.shipins+CHR(13)+"BTCNTRY*"+ALLTRIM(m.btctry)
	m.shipins = m.shipins+CHR(13)+"BTPHONE*"+ALLTRIM(m.btphone)
	m.shipins = m.shipins+CHR(13)+"BTEMAIL*"+ALLTRIM(m.btemail)

	m.shipins = m.shipins+CHR(13)+"SFORCITY*"+ALLTRIM(m.sfcity)
	m.shipins = m.shipins+CHR(13)+"SFORSTATE*"+ALLTRIM(m.sfstate)
	m.shipins = m.shipins+CHR(13)+"SFORZIP*"+ALLTRIM(m.sfzip)
	m.shipins = m.shipins+CHR(13)+"SFORCNTRY*"+ALLTRIM(m.sfctry)

	m.shipins = m.shipins+CHR(13)+"DCCODE*"+ALLTRIM(m.dcnum)
	m.shipins = m.shipins+CHR(13)+"BILLING*"+ALLTRIM(m.billopt)
	m.shipins = m.shipins+CHR(13)+"ORDER*"+ALLTRIM(m.cacctnum)
	m.shipins = m.shipins+CHR(13)+"DEPTDESC*"+ALLTRIM(m.deptdesc)
	m.shipins = m.shipins+CHR(13)+"FOB*"+ALLTRIM(m.deptdesc)  &&& Added 9/27/16 TMARG to capture Belk FOB
	m.specinst = UPPER(ALLTRIM(m.batch_num))
	m.dcnum = IIF(INT(VAL(m.dcnum))=0,ALLTRIM(STR(m.storenum)),m.dcnum)
	m.div = UPPER(m.div)
	m.origqty = m.qty
	m.ptid = m.ptid+1
	m.addby = "TOLLPROC"
	m.adddt = DATETIME()
	m.addproc = "AG940"
	m.ptdate = DATE()
	m.office = cOffice
	m.mod = cmod
	INSERT INTO xpt FROM MEMVAR

	LOCATE
*	BROWSE
	IF lTesting
*		set step on
	ENDIF

	m.qty = 0
	IF lTesting OR lOverridebusy
*		SET STEP ON
	ENDIF
	SCAN FOR ag940file.ship_ref = cship_ref
		SELECT xptdet
		SCATTER FIELDS EXCEPT ptdetid,ptdate MEMVAR MEMO BLANK
		SELECT ag940file
		SCATTER MEMVAR MEMO
		m.ptdate = DATE()

*!*			COMMENTED SECTIONS ARE TO ALIGN CARSON II UPLOADS WITH NJ-PNP, 06.23.2017, Joe
*!*			IF cOffice = "Y"
*!*				sq1 = [select style from upcmast where upc = ']+ALLTRIM(m.upc)+[' and accountid = 1285]
*!*				xsqlexec(sq1,"upcstyle1",,"wh")
*!*				m.style = ALLTRIM(upcstyle1.STYLE)
*!*				USE IN upcstyle1
*!*			ELSE

		m.style = ALLTRIM(m.style)+ALLTRIM(m.sku200)

*!*			ENDIF

		SELECT ag940file
		IF m.id # "*N"

*!*				IF (m.packtype # "CP") AND cOffice = "Y"
*!*					m.upc = ALLTRIM(m.upc)
*!*					sq1 = [select * from upcmast where upc = ']+ALLTRIM(m.upc)+[' and accountid = 1285]
*!*					xsqlexec(sq1,"upcmast1",,"wh")
*!*					lineat = ATLINE("CARTONUNIT",upcmast1.INFO)
*!*					cunitnum = MLINE(upcmast1.INFO,lineat)
*!*					nunitnum = INT(VAL(SUBSTR(ALLTRIM(cunitnum),AT("*",cunitnum,1)+1)))
*!*					IF nunitnum=0
*!*						nunitnum=1
*!*					ENDIF
*!*					m.totqty = INT(m.totqty/nunitnum)
*!*					m.pack = ALLTRIM(STR(nunitnum))
*!*					RELEASE nunitnum,cunitnum,sq1,lineat,cstyle
*!*					USE IN upcmast1
*!*				ELSE

				m.pack = "1"
				
*!*				ENDIF

				m.qty = m.qty + m.totqty  && changed to reflect correct detail quantity sum
				REPLACE xpt.qty WITH m.qty NEXT 1 IN xpt
				REPLACE xpt.origqty WITH xpt.qty NEXT 1 IN xpt
				m.casepack = INT(VAL(m.pack))
				m.shipstyle= ALLTRIM(m.style)
				m.printstuff = "FILENAME*"+cfilename
				m.printstuff = m.printstuff+CHR(13)+"GTIN*"+m.gtin
				m.printstuff = m.printstuff+CHR(13)+"SEASON*"+ALLTRIM(m.season)
				m.printstuff = m.printstuff+CHR(13)+"DESC*"+ALLTRIM(m.desc)
				m.printstuff = m.printstuff+CHR(13)+"UNITPRICE*"+ALLTRIM(STR(m.unitprice))
				m.printstuff = m.printstuff+CHR(13)+"CUSTSKU*"+ALLTRIM(m.custsku)  &&& added 2/22/17 TMARG to capture >15 char custsku for Hanes
				*!* Added the next two lines to provide a "scanning" UPC code, but store the original UPC for the 945. 03.26.2018. Joe
				m.printstuff = m.printstuff+CHR(13)+"ORIGUPC*"+m.origupc
				m.upc = IIF(EMPTY(ALLTRIM(m.labelupc)),ALLTRIM(m.origupc),ALLTRIM(m.labelupc))
				*!* 
				
				m.ptid = xpt.ptid
				m.accountid = xpt.accountid
				m.origqty = m.totqty
				m.ptdetid = m.ptdetid+1
				m.addby = "TOLLPROC"
				m.adddt = DATETIME()
				m.addproc = "AG940"
				m.office = cOffice
				m.mod = cmod
*!*					m.units = IIF(cOffice = "Y",.F.,.T.)
				m.units = .t.
				INSERT INTO xptdet FROM MEMVAR
			ENDIF
		ENDSCAN
		m.qty = 0
	ENDSCAN

	rollup_ptdet_units()
	
	
	IF lbrowfiles
		SELECT xpt
		LOCATE
		BROWSE
		SELECT xptdet
		REPLACE xptdet.upc WITH PADL(ALLTRIM(xptdet.upc),14,'0') ALL
		LOCATE
		BROWSE
		IF MESSAGEBOX("Do you wish to continue?",4+16+256,"CONTINUE")=7
			CANCEL
			RETURN
		ENDIF
	ENDIF

	WAIT ccustname+" Breakdown Round complete..." WINDOW TIMEOUT 2
	IF USED("PT")
		USE IN pt
	ENDIF
	IF USED("PTDET")
		USE IN ptdet
	ENDIF
	WAIT CLEAR
