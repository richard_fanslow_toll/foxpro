**This script identifies when a 945 has not been created for a PT.  

utilsetup("AGEGROUP_MISSING_945")
Set Exclusive Off
Set Deleted On
Set Talk Off
Set Safety Off 

WITH _SCREEN	
	.AUTOCENTER = .T.
	.WINDOWSTATE = 0
	.BORDERSTYLE = 1
	.WIDTH = 320
	.HEIGHT = 210
	.TOP = 200
	.LEFT = 100
	.CLOSABLE = .T.
	.MAXBUTTON = .T.
	.MINBUTTON = .T.
	.SCROLLBARS = 0
	.CAPTION = "AGEGROUP_MISSING_945"
ENDWITH	

goffice="Y"

If !Used("edi_trigger")
  use f:\3pl\data\edi_trigger shared In 0
ENDIF

xsqlexec("select accountid, wo_num, ship_ref from outship where mod='"+goffice+"' " + ;
	"and accountid=1285 and delenterdt>{"+dtoc(date()-20)+"} and delenterdt<{"+ttoc(datetime()-1290)+"}","temp945",,"wh")

SELECT * FROM edi_trigger where INLIST(accountid,1285) AND edi_type='945' INTO CURSOR t1 READWRITE
USE IN edi_trigger
SELECT * FROM temp945 a LEFT JOIN t1 b ON a.accountid=b.accountid AND a.wo_num=b.wo_num AND a.ship_ref=b.ship_ref INTO CURSOR c1 READWRITE
SELECT * FROM c1 where ISNULL(fin_status) INTO CURSOR c2 READWRITE 
SELECT c2
If Reccount() > 0
  Export To "S:\agegroup\temp\agegroup_945_trigger_not_created"  Type Xls
  tsendto = "tmarg@fmiint.com,todd.margolin@tollgroup.com,paul.gaidis@tollgroup.com,joe.bianchi@tollgroup.com"
  tattach = "S:\agegroup\temp\agegroup_945_trigger_not_created.xls"
  tcc =""
  tFrom ="Toll/FMI EDI System Operations <fmi-transload-ops@fmiint.com>"
  tmessage = "AGEGROUP_945_trigger_not_created"+Ttoc(Datetime())
  tSubject = "AGEGROUP_945_trigger_not_created, CHECK POLLER"
  Do Form m:\dev\frm\dartmail2 With tsendto,tFrom,tSubject, tcc,tattach,tmessage  ,"A"
ELSE 
  tsendto = "tmarg@fmiint.com"
  tattach = ""
  tcc =""
  tFrom ="Toll/FMI EDI System Operations <fmi-transload-ops@fmiint.com>"
  tmessage = "NO_MISSING_AGEGROUP_945_triggers_exist"+Ttoc(Datetime())
  tSubject = "NO_MISSING_AGEGROUP_945_triggers_exist"
  Do Form m:\dev\frm\dartmail2 With tsendto,tFrom,tSubject, tcc,tattach,tmessage  ,"A"
ENDIF


SELECT DISTINCT ACCOUNTID,WO_NUM,SHIP_REF FROM t1 WHERE EDI_TYPE='945 '  AND INLIST(accountid,1285);
 AND !errorflag AND (empty(fin_status) OR EMPTY(when_proc)) AND fin_status !='NO 945' INTO CURSOR TEMP21
SELECT  TEMP21
If Reccount() > 0
  Export To "S:\agegroup\temp\agegroup_945_trigger_created_not_processed"  Type Xls

  tsendto = "todd.margolin@tollgroup.com,paul.gaidis@tollgroup.com,joe.bianchi@tollgroup.com"
  tattach = "S:\agegroup\temp\agegroup_945_trigger_created_not_processed.xls"
  tcc =""
  tFrom ="Toll/FMI EDI System Operations <fmi-transload-ops@fmiint.com>"
  tmessage = "AGEGROUP_945_trigger_created_not_processed"+Ttoc(Datetime())
  tSubject = "AGEGROUP_945_trigger_created_not_processed"
  Do Form m:\dev\frm\dartmail2 With tsendto,tFrom,tSubject, tcc,tattach,tmessage  ,"A"
ELSE 
  tsendto = "tmarg@fmiint.com"
  tattach = ""
  tcc =""
  tFrom ="Toll/FMI EDI System Operations <fmi-transload-ops@fmiint.com>"
  tmessage = "NO_unprocessed_945s_for_AGEGROUP"+Ttoc(Datetime())
  tSubject = "NO_unprocessed_945s_for_AGEGROUP"
  Do Form m:\dev\frm\dartmail2 With tsendto,tFrom,tSubject, tcc,tattach,tmessage  ,"A"
ENDIF


SELECT * FROM t1 WHERE EDI_TYPE='945 '  AND INLIST(accountid,1285);
 AND errorflag INTO CURSOR TEMP21
SELECT  TEMP21
If Reccount() > 0
  Export To "S:\agegroup\temp\agegroup_trigger_error"  Type Xls

  tsendto = "tmarg@fmiint.com,todd.margolin@tollgroup.com,paul.gaidis@tollgroup.com,joe.bianchi@tollgroup.com"
  tattach = "S:\agegroup\temp\agegroup_trigger_error.xls"
  tcc =""
  tFrom ="Toll/FMI EDI System Operations <fmi-transload-ops@fmiint.com>"
  tmessage = "AGEGROUP_945_trigger_error"+Ttoc(Datetime())
  tSubject = "AGEGROUP_945_trigger_error"
  Do Form m:\dev\frm\dartmail2 With tsendto,tFrom,tSubject, tcc,tattach,tmessage  ,"A"
ELSE 
  tsendto = "tmarg@fmiint.com"
  tattach = ""
  tcc =""
  tFrom ="Toll/FMI EDI System Operations <fmi-transload-ops@fmiint.com>"
  tmessage = "NO_AGEGROUP_trigger_errors"+Ttoc(Datetime())
  tSubject = "NO_AGEGROUP_trigger_errors"
  Do Form m:\dev\frm\dartmail2 With tsendto,tFrom,tSubject, tcc,tattach,tmessage  ,"A"
 endif 
  
xsqlexec("select wo_num, ship_ref, del_date from outship where mod='"+goffice+"' " + ;
	"and accountid=1285 and del_date>{"+dtoc(date()-30)+"} and empty(bol_no)","t1",,"wh")

Select t1
If Reccount() > 0
  Export To "S:\agegroup\temp\agegroup_del_date_missing_bol"  Type Xls

  tsendto = "tmarg@fmiint.com,todd.margolin@tollgroup.com,paul.gaidis@tollgroup.com,joe.bianchi@tollgroup.com"
  tattach = "S:\agegroup\temp\agegroup_del_date_missing_bol.xls"
  tcc =""
  tFrom ="Toll/FMI EDI System Operations <fmi-transload-ops@fmiint.com>"
  tmessage = "AGEGROUP_PT missing BOL error"+Ttoc(Datetime())
  tSubject = "AGEGROUP_PT missing BOL error"
  Do Form m:\dev\frm\dartmail2 With tsendto,tFrom,tSubject, tcc,tattach,tmessage  ,"A"
ELSE 
  tsendto = "tmarg@fmiint.com"
  tattach = ""
  tcc =""
  tFrom ="Toll/FMI EDI System Operations <fmi-transload-ops@fmiint.com>"
  tmessage = "NO_AGEGROUP_PT missing BOL error"+Ttoc(Datetime())
  tSubject = "NO_AGEGROUP_PT missing BOL error"
  Do Form m:\dev\frm\dartmail2 With tsendto,tFrom,tSubject, tcc,tattach,tmessage  ,"A"
Endif

Close Data All
schedupdate()
_Screen.Caption=gscreencaption