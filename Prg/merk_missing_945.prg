
utilsetup("MERK_MISSING_945")
Set Exclusive Off
Set Deleted On
Set Talk Off
Set Safety Off   &&Allows for overwrite of existing files

Close Databases All
lTesting = .f.

goffice="2"

If !Used("edi_trigger")
	Use F:\3pl\Data\edi_trigger.Dbf Shared In 0
Endif

*Wait window at 10,10 "Performing the first query...." nowait

xsqlexec("select accountid, wo_num, ship_ref, consignee from outship where mod='"+goffice+"' " + ;
	"and accountid=6561 and bol_no <> '08447020001455169' and delenterdt>{"+dtoc(date()-5)+"} and delenterdt<{"+ttoc(datetime()-3600)+"}","temp945",,"wh")

delete for 'SAMPLE' $(consignee)
delete for Inlist(SHIP_REF,'0247272','0248503','0249695')

DELETE FOR ' OV' $(ship_ref)

SELECT * FROM TEMP945 A LEFT JOIN EDI_TRIGGER B ON A.SHIP_REF=B.SHIP_REF AND B.ACCOUNTID=6561 INTO CURSOR T1 READWRITE
SELECT * FROM t1 WHERE ISNULL(edi_type) INTO CURSOR t3 READWRITE 

If Reccount() > 0
	Export To "S:\merkury\temp\merk_missing_945_trigger"  Type Xls
	tsendto = IIF(lTesting,"doug.wachs@tollgroup.com","alma.navarro@tollgroup.com,Maria.Estrella@Tollgroup.com,paul.gaidis@tollgroup.com,joe.bianchi@tollgroup.com")
	tattach = "S:\merkury\temp\merk_missing_945_trigger.xls"
	tcc =""
	tFrom ="Toll EDI System Operations <fmi-transload-ops@fmiint.com>"
	tmessage = "MERKURY MISSING_945 In Trigger table  "+Ttoc(Datetime())
	tSubject = "The attached MERKURY shipments are MISSING in the Trigger table. Either the triggers were not processed, or delivery dates were just entered."
	Do Form m:\dev\frm\dartmail2 With tsendto,tFrom,tSubject, tcc,tattach,tmessage,"A"

Else
	tsendto =  "doug.wachs@tollgroup.com"
	tattach = ""
	tcc =""
	tFrom ="Toll EDI System Operations <fmi-transload-ops@fmiint.com>"
	tmessage = "NO MERKURY MISSING_945 In Trigger table exist "+Ttoc(Datetime())
	tSubject = "NO MERKURY MISSING_945 In Trigger table exist"
	Do Form m:\dev\frm\dartmail2 With tsendto,tFrom,tSubject, tcc,tattach,tmessage,"A"
Endif



Set Step On
** IDENTIFIES UNPROCESSED  945 RECORDS IN EDI TRIGGER
SELECT * FROM edi_trigger WHERE Inlist(ACCOUNTID,6561)  AND Empty(FIN_STATUS) Into Cursor temp24 Readwrite
Select temp24
If Reccount() > 0
	Export To "S:\merkury\temp\945_not_processed_in_edi_trigger"  Type Xls
	tsendto = IIF(lTesting,"joe.bianchi@tollgroup.com","doug.wachs@tollgroup.com,alma.navarro@tollgroup.com,Maria.Estrella@Tollgroup.com,paul.gaidis@tollgroup.com,joe.bianchi@tollgroup.com")
	tattach = "S:\merkury\temp\945_not_processed_in_edi_trigger.xls"
	tcc =""
	tFrom ="Toll/FMI EDI System Operations <fmi-transload-ops@fmiint.com>"
	tmessage = "MERKURY 945 Not processed In Trigger table_"+Ttoc(Datetime())
	tSubject = "MERKURY 945 Not processed In Trigger table, CHECK POLLER"
	Do Form m:\dev\frm\dartmail2 With tsendto,tFrom,tSubject, tcc,tattach,tmessage  ,"A"
Else
	tsendto = IIF(lTesting,"joe.bianchi@tollgroup.com","doug.wachs@tollgroup.com")
	tattach = ""
	tcc =""
	tFrom ="Toll/FMI EDI System Operations <fmi-transload-ops@fmiint.com>"
	tmessage = "NO MERKURY 945 unprocessed In Trigger table_"+Ttoc(Datetime())
	tSubject = "NO MERKURY 945 unprocessed In Trigger table_exist"
	Do Form m:\dev\frm\dartmail2 With tsendto,tFrom,tSubject, tcc,tattach,tmessage  ,"A"
Endif



Set Step On
SELECT * FROM edi_trigger WHERE Inlist(ACCOUNTID,6561)  AND errorflag Into Cursor temp25 Readwrite
Select temp25
If Reccount() > 0
	Export To "S:\merkury\temp\merk_945_with_errors"  Type Xls

	tsendto = IIF(lTesting,"joe.bianchi@tollgroup.com","doug.wachs@tollgroup.com,alma.navarro@tollgroup.com,Maria.Estrella@Tollgroup.com,paul.gaidis@tollgroup.com,joe.bianchi@tollgroup.com")
	tattach = "S:\merkury\temp\merk_945_with_errors.xls"
	tcc =""
	tFrom ="Toll/FMI EDI System Operations <fmi-transload-ops@fmiint.com>"
	tmessage = "MERKURY 945 WITH ERRORS_"+Ttoc(Datetime())
	tSubject = "MERKURY 945 WITH ERRORS"
	Do Form m:\dev\frm\dartmail2 With tsendto,tFrom,tSubject, tcc,tattach,tmessage  ,"A"
Else
	tsendto = IIF(lTesting, "joe.bianchi@tollgroup.com","doug.wachs@tollgroup.com")
	tattach = ""
	tcc =""
	tFrom ="Toll/FMI EDI System Operations <fmi-transload-ops@fmiint.com>"
	tmessage = "NO MERKURY 945's with errors exist_"+Ttoc(Datetime())
	tSubject = "NO MERKURY 945's with errors exist"
	Do Form m:\dev\frm\dartmail2 With tsendto,tFrom,tSubject, tcc,tattach,tmessage  ,"A"
Endif

xsqlexec("select wo_num, ship_ref, del_date from outship where mod='"+goffice+"' " + ;
	"and accountid=6561 and del_date>{"+dtoc(date()-30)+"} and empty(bol_no)","t1",,"wh")

Select t1
If Reccount() > 0
	Export To "S:\merkury\temp\merk_del_date_missing_bol"  Type Xls

	tsendto = IIF(lTesting,"joe.bianchi@tollgroup.com","doug.wachs@tollgroup.com,alma.navarro@tollgroup.com,Maria.Estrella@Tollgroup.com,paul.gaidis@tollgroup.com,joe.bianchi@tollgroup.com")
	tattach = "S:\merkury\temp\merk_del_date_missing_bol.xls"
	tcc =""
	tFrom ="Toll/FMI EDI System Operations <fmi-transload-ops@fmiint.com>"
	tmessage = "MERKURY PTs missing BOL "+Ttoc(Datetime())
	tSubject = "MERKURY PTs missing BOL"
	Do Form m:\dev\frm\dartmail2 With tsendto,tFrom,tSubject, tcc,tattach,tmessage  ,"A"
Else
	tsendto = IIF(lTesting, "joe.bianchi@tollgroup.com","doug.wachs@tollgroup.com")
	tattach = ""
	tcc =""
	tFrom ="Toll/FMI EDI System Operations <fmi-transload-ops@fmiint.com>"
	tmessage = "NO MERKURY PTs missing BOL  "+Ttoc(Datetime())
	tSubject = "NO MERKURY PTs missing BOL exist"
	Do Form m:\dev\frm\dartmail2 With tsendto,tFrom,tSubject, tcc,tattach,tmessage  ,"A"
Endif

Close Data All

Wait Window At 10,10 "all done...." Timeout 2


schedupdate()
_Screen.Caption=gscreencaption
On Error

