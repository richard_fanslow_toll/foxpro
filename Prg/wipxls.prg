**wipXls.prg
**
**create a 6 page work in progress worksheet
**  page 1 - Undelivered Freight (Not UPS/RPS)
**  page 2 - Delivered Last 10 Days (Not UPS/RPS)
**  page 3 - Picked & Unshipped (Not UPS/RPS)
**  page 4 - UPS/RPS - Undelivered Freight
**  page 5 - UPS/RPS - Delivered Last 10 Days
**  page 6 - UPS/RPS - Picked & Unshipped
**  page 7 - All delivered (Pony only)
**
Parameters nAcctNum, cOffice, lWebsite, lStyleDetail

If lWebsite
	xsqlexec("select * from account where inactive=0","account",,"qq")
	index on accountid tag accountid
	set order to
else
	coffice=goffice
EndIf

Select account
Locate for accountid = nAcctNum
STORE acctName TO cAccountName

If lWebsite
  USE IN ACCOUNT
EndIf

If lWebsite
	USE &main.offices Order priority
	Locate for LOCALE = Upper(cOffice)
	cOfficeName = CITY
	USE IN offices

	cWhPath = wf(coffice, nAcctNum)
	Open Database &cWhPath.wh

	if usesqloutwo()
	else
		USE wh!outship In 0
		USE wh!outdet In 0
	endif
Else
	cOfficeName="TGF Inc. "
	do case
	case inlist(cOffice,"N","I","J")
		cOfficeName=cOfficeName+"(Carteret)."
	case inlist(coffice,"1","2","5","6","7")
		cOfficeName=cOfficeName+"(San Pedro)"
	case coffice="L"
		cOfficeName=cOfficeName+"(Mira Loma)"
	case cOffice="M"
		cOfficeName=cOfficeName+"(Miami)"
	endcase
EndIf

cUniqueId = Iif(lWebsite, ALLTRIM(STR(REPORTS.UNIQUE_ID)), SUBSTR(SYS(2015), 3, 10))

PUBLIC oExcel
oExcel = CreateObject("Excel.Application")

STORE "h:\fox\"+cUniqueId TO cFile
COPY FILE f:\web_reports\wipXls.XLS TO &cFile

oWorkbook = oExcel.Workbooks.Open(cFile)

lDataFound = .f.

cups    = "(inlist(ship_via,"+xsmallpackagecarriers+") and !inlist(ship_via,"+xnotsmallpackagecarriers+"))"
cnotups = "(!inlist(ship_via,"+xsmallpackagecarriers+") or inlist(ship_via,"+xnotsmallpackagecarriers+"))"

**Page 1
cFilter = "del_date={} and notOnWip=.f. and ctnQty # 0"
Store Iif(writePage(cFilter, cNotUps, 1), .t., lDataFound) to lDataFound

**Page 2
cFilter = "(del_date>={"+dtoc(DATE()-10)+"} or del_date={}) and notOnWip=.f. and ctnQty # 0"
Store Iif(writePage(cFilter, cNotUps, 2), .t., lDataFound) to lDataFound

**Page 3
cFilter = "del_date={} and PICKED#{} and notOnWip=.f. and ctnQty # 0"
Store Iif(writePage(cFilter, cNotUps, 3), .t., lDataFound) to lDataFound

**Page 4
cFilter = "del_date={} and notOnWip=.f. and ctnQty # 0"
Store Iif(writePage(cFilter, cUps, 4), .t., lDataFound) to lDataFound

**Page 5
cFilter = "(del_date>={"+dtoc(DATE()-10)+"} or del_date={}) and notOnWip=.f. and ctnQty # 0"
Store Iif(writePage(cFilter, cUps, 5), .t., lDataFound) to lDataFound

**Page 6
cFilter = "del_date={} and PICKED={} and notOnWip=.f. and ctnQty # 0"
Store Iif(writePage(cFilter, cUps, 6), .t., lDataFound) to lDataFound

*

oWorkbook.sheets[1].activate()
oWorkbook.Save()

If lWebsite
	oWorkbook.Close()
	oExcel.Quit()
	RELEASE oWorkbook, oExcel

	SELECT PDF_FILE
	INSERT INTO PDF_FILE (UNIQUE_ID) VALUES (REPORTS.UNIQUE_ID)

	If !lDataFound &&need something to distinguish from "poller down"
		REPLACE MEMO WITH "NO DATA"
	Else
		Append MEMO MEMO FROM &cFile OVERWRITE
	EndIf 
Else
	oExcel.VISIBLE = .T.
	RELEASE oWorkbook, oExcel
endif

If lWebsite
	Set database to wh
	Close Databases 
EndIf

*****
*****
Procedure writePage
Parameters cFilter, cUpsFilter, nPgNum

	**page 5 (ups, last 10 days) must show individual pts.
	If lStyleDetail
		if usesqloutwo()
			xsqlexec("select outship.*, totqty, style, color, id from outship " + ;
				"where outship.outshipid=outdet.outshipid and outship.mod='"+goffice+"'" + ;
				"and accountid="+transform(nAcctNum)+" and "+cFilter,"xdytemp",,"wh")

			select *, totQty as qtyFld, iif(combinedPt and nPgNum # 5, combPt, ship_ref) as ptNum, ;
				Left(Dtoc(start),5) as startView, Left(Dtoc(min(cancel)),5) as cancelView, ;
				Left(Dtoc(picked),5) as pickedView, Left(Dtoc(Max(called)),5) as calledView, ;
				Left(Dtoc(Max(appt)),5) as apptView, Left(Dtoc(del_date),5) as delView, ;
				Iif(sp, "Y", "N") as spView from xdytemp where &cUpsFilter ;
				group by ptNum, style, color, id into cursor csrOutship

		else
			Select os.*, od.totQty as qtyFld, iif(combinedPt and nPgNum # 5, combPt, ship_ref) as ptNum, ;
					Left(Dtoc(start),5) as startView, Left(Dtoc(min(cancel)),5) as cancelView, ;
					Left(Dtoc(picked),5) as pickedView, Left(Dtoc(Max(called)),5) as calledView, ;
					Left(Dtoc(Max(appt)),5) as apptView, Left(Dtoc(del_date),5) as delView, ;
					Iif(sp, "Y", "N") as spView, od.style, od.color, od.id ;
				from outship os left join outdet od on os.outshipid = od.outshipid ;
				where os.accountId = nAcctNum and &cFilter and &cUpsFilter group by ptNum, style, color, id ;
			  into cursor csrOutship
		endif

	else
		if usesqloutwo()
			xsqlexec("select * from outship where mod='"+goffice+"' " + ;
				"where accountid='"+transform(nAcctNum)+" and "+cFilter,"xdytemp",,"wh")

			Select *, ctnQty as qtyFld, iif(combinedPt and nPgNum # 5, combPt, ship_ref) as ptNum, ;
				"Asst" as style, "" as color, "" as id, ;
				Left(Dtoc(start),5) as startView, Left(Dtoc(min(cancel)),5) as cancelView, ;
				Left(Dtoc(picked),5) as pickedView, Left(Dtoc(Max(called)),5) as calledView, ;
				Left(Dtoc(Max(appt)),5) as apptView, Left(Dtoc(del_date),5) as delView, ;
				Iif(sp, "Y", "N") as spView ;
				from xdytemp where &cUpsFilter group by ptNum into cursor csrOutship

		else
			Select *, ctnQty as qtyFld, iif(combinedPt and nPgNum # 5, combPt, ship_ref) as ptNum, "Asst" as style, "" as color, "" as id, ;
				Left(Dtoc(start),5) as startView, Left(Dtoc(min(cancel)),5) as cancelView, ;
				Left(Dtoc(picked),5) as pickedView, Left(Dtoc(Max(called)),5) as calledView, ;
				Left(Dtoc(Max(appt)),5) as apptView, Left(Dtoc(del_date),5) as delView, ;
				Iif(sp, "Y", "N") as spView ;
				from outship ;
				where accountId = nAcctNum and &cFilter and &cUpsFilter group by ptNum ;
			  into cursor csrOutship
		endif
	EndIf

	lData = .f.
	If !Eof()
		lData = .t.

		nRecCnt = Reccount()
		If Reccount() > 65528 &&excel's row limit
			nRecCnt = 65528
			strMsg = "The record count for this account's style detail WIP exceeds excel's limit. Only the first 65,535 records will be displayed."
			=messagebox(strMsg, 16, "TGF Inc.")
		EndIf

		Store "ptNum, wo_num, consignee, cnee_ref, style, color, id, ship_via, qtyFld, startView, " + ;
			  "cancelView, pickedView, calledView, apptView, appt_time, appt_num, " + ;
			  Iif(InList(nPgNum, 2, 5), "delView, ", "") + "spView" ;
			to cFieldList

		Store "h:\fox\"+SUBSTR(SYS(2015), 3, 10) TO cTmpFile
		Copy To &cTmpFile FIELDS &cFieldList TYPE XL5

		oTmpWorkbook = oExcel.Workbooks.Open(cTmpFile)
		**start at row 2 (A2) to eliminate headers, nRecCnt+1 to account for header row
		Store Iif(InList(nPgNum, 2, 5), "O", "N") to cRCol
		oTmpWorkbook.ActiveSheet.Range("A2:"+cRCol+Alltrim(Str(nRecCnt+1))).copy()

		oWorkbook.Sheets[nPgNum].Range("B4").Value = "Account: "+ALLTRIM(cAccountName)+SPACE(3)+"Office: "+Alltrim(cOfficeName)
		oWorkbook.Sheets[nPgNum].Range("A6").pasteSpecial()

		**next 2 lines used to avoid an excel warning msg stating that the clipboard is full... 
		**  these 2 lines empty the clipboard automatically
		oExcel.cutCopyMode = "False"
		oExcel.cutCopyMode = "True"
		oTmpWorkbook.Close()
		RELEASE oTmpWorkbook
	EndIf

	USE in csrOutship

Return lData
*****
