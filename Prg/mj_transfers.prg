* This program extracts cycle count data from previous day
utilsetup("MJ_TRANSFERS")
SET EXCLUSIVE OFF 
SET DELETED ON 
SET TALK OFF  
SET SAFETY OFF   &&Allows for overwrite of existing files

close databases all

If !Used("xfer")
  Use F:\whj\whdata\xfer.dbf In 0 
ENDIF

If !Used("xferdet")
  Use F:\whj\whdata\xferdet.dbf In 0 
ENDIF

*Wait window at 10,10 "Performing the first query...." nowait
SELECT xfernum as Transfer,b.updatedt as date,fromacctid, toacctid,style,color,id,qty  FROM xfer a, xferdet b WHERE a.xferid=b.xferid ;
INTO CURSOR transfer1 readwrite
REPLACE fromacctid WITH 1000 FOR fromacctid=6303
REPLACE fromacctid WITH 1200 FOR fromacctid=6304
REPLACE fromacctid WITH 1320 FOR fromacctid=6305
REPLACE fromacctid WITH 1310 FOR fromacctid=6320
REPLACE fromacctid WITH 1221 FOR fromacctid=6321
REPLACE fromacctid WITH 1222 FOR fromacctid=6322
REPLACE fromacctid WITH 1223 FOR fromacctid=6323
REPLACE fromacctid WITH 1252 FOR fromacctid=6324
REPLACE fromacctid WITH 1150 FOR fromacctid=6325
REPLACE toacctid WITH 1000 FOR toacctid=6303
REPLACE toacctid WITH 1200 FOR toacctid=6304
REPLACE toacctid WITH 1320 FOR toacctid=6305
REPLACE toacctid WITH 1310 FOR toacctid=6320
REPLACE toacctid WITH 1221 FOR toacctid=6321
REPLACE toacctid WITH 1222 FOR toacctid=6322
REPLACE toacctid WITH 1223 FOR toacctid=6323
REPLACE toacctid WITH 1252 FOR toacctid=6324
REPLACE toacctid WITH 1150 FOR toacctid=6325
SELECT transfer1
	If Reccount() > 0 
		export TO "S:\MarcJacobsData\Reports\transfers\transfers"  TYPE xls
		tsendto = "tmarg@fmiint.com"
		tattach = "" 
		tcc =""                                                                                                                               
		tFrom ="Toll EDI System Operations <fmi-transload-ops@fmiint.com>"                                
		tmessage = "TRANSFERS REPORT IS COMPLETE FOR: "+Ttoc(Datetime())        
		tSubject = "TRANSFERS REPORT IS COMPLETE"                                                           
		Do Form m:\dev\frm\dartmail2 With tsendto,tFrom,tSubject, tcc,tattach,tmessage  ,"A"     
			
	ELSE 
		export TO "S:\MarcJacobsData\Reports\transfers\transfers"   TYPE xls
		tsendto = "tmarg@fmiint.com"
		tattach = "" 
		tcc =""                                                                                                                               
		tFrom ="Toll EDI System Operations <fmi-transload-ops@fmiint.com>"                                
		tmessage = "CYCLE COUNT COMPLETE_: "+Ttoc(Datetime())        
		tSubject = "CYCLE COUNT COMPLETE"                                                           
		Do Form m:\dev\frm\dartmail2 With tsendto,tFrom,tSubject, tcc,tattach,tmessage  ,"A"   

	ENDIF
close data all 

Wait window at 10,10 "all done...." timeout 2


schedupdate()
_screen.Caption=gscreencaption
on error

