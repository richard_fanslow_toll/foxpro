*!* This program cross-checks the record counts for
*!* the SQL LABELS and CARTONS tables by BOL/WO

PARAMETERS cUseFolder,cBOLIn,cOffice,nAcctNum,cPPName,cSQL
PUBLIC cFileType1,lLabelRecs
SET ESCAPE ON
ON ESCAPE CANCEL
IF EMPTY(cSQL)
	cSQL = "tgfnjsql01"
ENDIF

SELECT 0
USE F:\wh\sqlpassword
cSQLPass = ALLTRIM(sqlpassword.PASSWORD)
USE IN sqlpassword

*!* nDoVar = 2, will change WO#
*!* nDoVar = 3, will change Carton count
nDoVar = 0
lError = .F.
lLabelRecs = .F.
lCarton0 = .F.

*ASSERT .F. MESSAGE "In SQL-COMPARE routine"

cFileOutName1 = "L"+TRIM(cPPName)
cFileOutName2 = "C"+TRIM(cPPName)
nAcct = ALLTRIM(STR(nAcctNum))

IF lTestInput
*		AND accountid = nAcctNum
	SELECT IIF("SQL3"$shipins,.F.,.T.) AS scanpack,bol_no,wo_num ;
		FROM outship WHERE bol_no = cBOLIn ;
		GROUP BY 1,2,3 ;
		ORDER BY 1,2,3 ;
		INTO DBF F:\3pl\DATA\sqlwocompmoret
ELSE
*		AND accountid = nAcctNum
	SELECT IIF("FLS1"$shipins OR "SQL5"$shipins,.F.,.T.) AS scanpack,bol_no,wo_num ;
		FROM outship WHERE bol_no = cBOLIn ;
		GROUP BY 1,2,3 ;
		ORDER BY 1,2,3 ;
		INTO DBF F:\3pl\DATA\sqlwocompmoret
ENDIF

IF lTesting OR DATETIME()<DATETIME(2013,01,24,15,00,00)
	BROWSE
ENDIF

*!* Scans through all WOs within the OUTSHIP BOL#

FOR iz = 1 TO 2
	SELECT sqlwocompmoret
	LOCATE
	IF iz = 1
		lSQL3 = .F.
	ELSE
		lSQL3 = .T.
	ENDIF
	SET FILTER TO sqlwocompmoret.scanpack = lSQL3
*	ASSERT .f. MESSAGE "At SCANPACK/NON-SCANPACK loops...debug HERE"

	SCAN
*!*			DO CASE
*!*	*!*				CASE lTesting
*!*	*!*					lcDSNLess="driver=SQL Server;server=sql3;uid=SA;pwd=&cSQLPass;DATABASE=PICKPACK"
*!*				CASE INLIST(cOffice,"I","N","J")
*!*					lcDSNLess="driver=SQL Server;server=&csql;uid=SA;pwd=&cSQLPass;DATABASE=PICKPACK"
*!*				CASE INLIST(cOffice,"M")
*!*					lcDSNLess="driver=SQL Server;server=fls2;uid=SA;pwd=&cSQLPass;DATABASE=PICKPACK"
*!*				CASE cOffice = "L"
*!*					lcDSNLess="driver=SQL Server;server=sql5;uid=SA;pwd=&cSQLPass;DATABASE=PICKPACK"
*!*				CASE INLIST(cOffice,"C","E","O")
*!*					IF lSQL3
*!*						lcDSNLess="driver=SQL Server;server=&csql;uid=SA;pwd=&cSQLPass;DATABASE=PICKPACK"
*!*					ELSE
*!*						lcDSNLess="driver=SQL Server;server=sql5;uid=SA;pwd=&cSQLPass;DATABASE=PICKPACK"	&& dy 2/18/18
*!*					ENDIF
*!*			ENDCASE

		lcDSNLess="driver=SQL Server;server=tgfnjsql01;uid=SA;pwd=&cSQLPass;DATABASE=PICKPACK"

		nHandle=SQLSTRINGCONNECT(m.lcDSNLess,.T.)
		SQLSETPROP(nHandle,"DispLogin",3)
		WAIT WINDOW "Now processing "+cPPName+" SQL view...please wait" NOWAIT

		IF nHandle=0 && bailout
			WAIT WINDOW "Could not make SQL connection" TIMEOUT 3
			closefiles()
			cRetMsg = "NO SQL CONNECT"
			RETURN cRetMsg
		ENDIF

		nWO_Num1 = sqlwocompmoret.wo_num
		IF nDoVar = 2 AND lTesting
*			nWO_Num = 652885
		ENDIF
		cWo   = ALLTRIM(STR(nWO_Num1))
		lcQ3 = " &cWo "
		WAIT WINDOW "Now scanning WO# "+cWo NOWAIT

		if usesql()
			xsqlexec("select * from labels where wo_num="+cwo,cFileOutName1,,"pickpack")
			llsuccess=1
		else
			lcQ1=[SELECT * FROM dbo.labels Labels]
			lcQ2 = [ WHERE Labels.wo_num = ]
			lcsql = lcQ1+lcQ2+lcQ3
			llSuccess=SQLEXEC(nHandle,lcsql,cFileOutName1)
		endif
		
		successrun("L")

		IF lError
			do case
			case llabelrecs
				cretmsg = "LR"
			case lcarton0
				cretmsg = "CARTON0"
			otherwise
				cretmsg = "SQL ERROR-LBL"
			endcase
			RETURN cRetMsg
		ENDIF

		if usesql()
			xsqlexec("select * from cartons where wo_num="+cwo,cFileOutName2,,"pickpack")
			llsuccess=1
		else
			lcQ1 = [SELECT * FROM dbo.cartons Cartons]
			lcQ2 = [ WHERE Cartons.wo_num = ]
			lcsql = lcQ1+lcQ2+lcQ3
			llSuccess=SQLEXEC(nHandle,lcsql,cFileOutName2)
		endif
		
		successrun("C")

		IF lError
			cRetMsg = "SQL ERROR-CTN"
			RETURN cRetMsg
		ENDIF

		SELECT &cFileOutName1 && Labels
		COUNT TO nLabels
		cCountLabels = ALLTRIM(STR(nLabels))
		SELECT &cFileOutName2 && Cartons
		COUNT TO nCartons
		IF nDoVar = 3 AND lTesting
			nCartons = nCartons - 5
		ENDIF
		cCountCartons = ALLTRIM(STR(nCartons))

		IF nLabels <> nCartons && Mismatched count
			SET STEP ON
			DO CASE
				CASE nCartons = 0
					ASSERT .F. MESSAGE "At Zero Carton determination, WO "+cWo
					SQLCANCEL(nHandle)
					SQLDISCONNECT(nHandle)
					cRetMsg = "CARTON0-"+cWo
					RETURN cRetMsg
				CASE lSQL3 AND ((cOffice = "N" AND nAcctNum=5446) OR INLIST(cOffice,"L","C")) AND nLabels = 0
					WAIT WINDOW "Correct zero-label COUNT(MORET SQL3 data)" NOWAIT
				OTHERWISE
					errormail(1)
					cRetMsg = "LBL/CTN UNEQUAL, WO "+cWo
					RETURN cRetMsg
			ENDCASE
		ENDIF
		SQLCANCEL(nHandle)
		SQLDISCONNECT(nHandle)
	ENDSCAN
ENDFOR

WAIT WINDOW "Labels and Cartons had equal record counts...continuing" NOWAIT

closefiles()

cRetMsg = "OK"
RETURN cRetMsg

**********************
PROCEDURE successrun
	PARAMETERS cFileType1
*	ASSERT .f. MESSAGE "In Labels/Cartons verification...debug"
	cFileWithError = IIF(cFileType1="C","CARTONS","LABELS")
	DO CASE
		CASE INLIST(llSuccess,0,-1)  && no records 0 indicates no records and 1 indicates records found
			WAIT WINDOW "No SQL Connection" TIMEOUT 3
			errormail(2)
			RETURN
		CASE RECCOUNT()=0
			IF lSQL3 AND (INLIST(cOffice,"L","C") OR (cOffice = "N" AND INLIST(nAcctNum,5446)))
				WAIT WINDOW "No records correctly found in LABELS - MORET" NOWAIT
			ELSE
				IF lSQL3 AND cFileType1#"C"
					WAIT WINDOW "No "+cPPName+" records found for this WO# "+ALLTRIM(STR(nWO_Num1)) TIMEOUT 3
					ASSERT .F. MESSAGE "Zero Record Count flagged in "+cFileWithError+"...debug"
					errormail(3)
				ELSE
					lCarton0 = .T.
				ENDIF
				RETURN
			ENDIF
		CASE RECCOUNT()>0 AND nAcctNum#5452 AND lSQL3
			IF lSQL3 AND cOffice = "L" AND cFileType1 = "L"
				WAIT WINDOW cPPName+" LABELS records were found for this WO# "+ALLTRIM(STR(nWO_Num1)) TIMEOUT 3
				lLabelRecs = .T.
				errormail(4)
				RETURN
			ENDIF
	ENDCASE
ENDPROC

**********************
PROCEDURE errormail
	PARAMETERS nType
	lError = .T.
	DO CASE
		CASE nType = 1
			tmessage = "The SQL counts (Labels vs. Cartons) did not match for this BOL."
			tmessage = tmessage + CHR(13) +;
				"Count at WO# "+cWo+": "+cCountLabels+" (Labels) <> "+cCountCartons+" (Cartons)."
			tmessage = tmessage + CHR(13) + "Office: "+cOffice+", Account: "+ALLTRIM(STR(nAcctNum))
		CASE nType = 2
			tmessage = "SQL could not properly connect while processing WO "+cWo
			tmessage = tmessage + CHR(13) + "Office: "+cOffice+", Account: "+ALLTRIM(STR(nAcctNum))
		CASE nType = 3
			tmessage = "No records were found in the SQL data for this BOL at WO# "+cWo
			tmessage = tmessage + CHR(13) + "Office: "+cOffice+", Account: "+ALLTRIM(STR(nAcctNum))
			tmessage = tmessage + CHR(13) + "Error occurred in "+cFileWithError
		OTHERWISE
			tmessage = "Records were found in the MORET SQL LABELS for this BOL at WO# "+cWo
			tmessage = tmessage + CHR(13) + "Office: "+cOffice+", Account: "+ALLTRIM(STR(nAcctNum))
			tmessage = tmessage + CHR(13) + "Error occurred in "+cFileWithError
			tmessage = tmessage + CHR(13) + CHR(13) + "This error must be corrected immediately,"
			tmessage = tmessage + CHR(13) + "or the 945 can't be created."
			stopmoretwo()
	ENDCASE
	IF nType = 2
		tmessage = tmessage + CHR(13) + "Processing stopped at WO# "+cWo
	ENDIF

	tfrom = "TGF Warehouse Operations <warehouse-ops@fmiint.com>"
	tsubject = "SQL ERROR(Labels-Cartons) at BOL#: "+cBOLIn+", WO# "+ALLTRIM(STR(nWO_Num1))
	SELECT 0
	USE F:\3pl\DATA\mailmaster ALIAS mm
	LOCATE FOR mm.edi_type = "MISC" AND mm.taskname = "GENERAL"
	tsendtoerr = ALLTRIM(IIF(mm.Use_Alt,mm.sendtoalt,mm.sendto))
	tccerr = " "
	LOCATE
*!*				LOCATE FOR mm.edi_type = "MISC" AND mm.taskname = "MORETOTHERR"  AND mm.office = cOffice  && Added Office, 08.13.2012
*!*				tccother = ALLTRIM(IIF(mm.Use_Alt,mm.ccalt,mm.cc))
	tccother = ""
	USE IN mm
	tsendto = tsendtoerr
	IF lLabelRecs
		tcc = tccother
	ENDIF
	tattach = " "
	tcc = tccother
	DO FORM m:\dev\frm\dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
	closefiles()
	SQLCANCEL(nHandle)
	SQLDISCONNECT(nHandle)
ENDPROC

**********************
PROCEDURE closefiles
	IF USED(cFileOutName1)
		USE IN &cFileOutName1
	ENDIF
	IF USED(cFileOutName2)
		USE IN &cFileOutName2
	ENDIF
	IF USED('sqlwocompmoret')
		USE IN sqlwocompmoret
	ENDIF
	RETURN

**********************
PROCEDURE stopmoretwo
	IF !lTesting
		SELECT edi_trigger
		LOCATE
		REPLACE processed WITH .T.,errorflag WITH .T.,fin_status WITH "RECS IN LABELS" ;
			FOR edi_trigger.wo_num = nWO_Num AND edi_trigger.accountid = nAcctNum
		LOCATE
	ENDIF
ENDPROC