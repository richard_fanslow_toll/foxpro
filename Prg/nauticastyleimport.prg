* import NAUTICA styles
* programmed by MB 1/12/2018
* build exe as ?
*
* Revised 1/16/2018 MB to load VF/Nautica and Kipling styles with different accountids from single spreadsheet

LOCAL lTestMode
lTestMode = .T.

guserid = "STYLEIMP"

IF NOT lTestMode THEN
	utilsetup("NAUTICASTYLEIMPORT")
ENDIF

DECLARE INTEGER CreateMutex IN Win32API ;
	STRING @lpMutexAttributes, ;
	STRING bInitialOwner, ;
	STRING @lpName

DECLARE INTEGER GetLastError IN Win32API

* if running in EXE check to see if already running
IF _VFP.STARTMODE > 0 THEN
	lcProcessName = "NAUTICASTYLEIMPORT"
	CreateMutex(0,CHR(1),@lcProcessName)
	lnError = GetLastError()
	IF lnError = 183
		WAIT WINDOW TIMEOUT 5 "This program is already running..." 
		IF NOT lTestMode THEN
			schedupdate()
		ENDIF
		RETURN .F.
	ENDIF
ENDIF

loNAUTICASTYLEIMPORT = CREATEOBJECT('NAUTICASTYLEIMPORT') 
loNAUTICASTYLEIMPORT.MAIN( lTestMode )

IF NOT lTestMode THEN
	schedupdate()
ENDIF

CLOSE DATABASES ALL
RETURN

#DEFINE LOGIT 1
#DEFINE WAITIT 2
#DEFINE NOWAITIT 4
#DEFINE SENDIT 8
#DEFINE CRLF CHR(13) + CHR(10)
#DEFINE RETURN_DATA_MANDATORY .F.
#DEFINE RETURN_DATA_NOT_MANDATORY .T.
#DEFINE CSVFORMAT 6
#DEFINE WRITE_ONLY_UNBUFFERED 11
#DEFINE COMMA ","

DEFINE CLASS NAUTICASTYLEIMPORT AS CUSTOM

	cProcessName = 'NAUTICASTYLEIMPORT'

	lTestMode = .T.  && .F. FOR NORMAL OPERATION, .T. TO SUPPRESS ALL BUT DEVELOPER EMAILS AND CHANGE INPUT TABLE, ETC.

	dToday = DATE()

	lAutoYield = .T.

	cStartTime = TTOC(DATETIME())

	* connection properties
	nSQLHandle = 0

	* folder properties

	cInputFolder1 = ''
	cArchiveFolder = ''
	cOrigFolder = ''	

*!*		* data properties
*!*		cUPCMastTable = ''
	
	nAccountID = 687
	cAddProc = "NAUTSTYLE"
	tADDDT = DATETIME()
	lPNP = .F.
	cDESCRIP = ''
	cSTYLE = ''
	cCOLOR = ''
	cID = ''
	nUPCMASTID = 0
	cUPC = ''
	nWEIGHT = 0
	nCUBE = 0
	nUIC = 0
	cINFO = ''
	
	* processing properties
	lDeleteFoundRecs = .F.  && if .T. then when we find a key match we will delete existing rec and insert the new one. If .F. we will simply not insert when we find a new match.
	nRow = 0

	* Excel properties
	oExcel = NULL

	* wait window properties
	nWaitWindowTimeout = 2
	lWaitWindowIsOn = .T.

	* logfile properties
	lLoggingIsOn = .T.
	cLogFile = 'F:\UTIL\NAUTICA\STYLES\LOGFILES\NAUTICASTYLEIMPORT_log.txt'

	* INTERNAL email properties
	lSendInternalEmailIsOn = .T.
	cFrom = 'TGF Corporate <fmicorporate@fmiint.com>'
	cSendTo = 'mbennett@fmiint.com'
	cCC = ''
	cSubject = 'NAUTICA Style Import for ' + TRANSFORM(DATETIME())
	cAttach = ''
	cBodyText = ''
	cMGRSendTo = ''
	cMGRFrom = ''
	cMGRSubject = ''
	cMGRCC = ''
	cMGRAttach = ''
	cMGRBodyText = ''

	cCOMPUTERNAME = ''
	cUSERNAME = ''


	FUNCTION INIT
		IF NOT DODEFAULT()
			RETURN .F.
		ENDIF
		WITH THIS
			*SET RESOURCE OFF
			CLOSE DATA
			SET CENTURY ON
			*SET DATE AMERICAN
			*SET HOURS TO 24
			SET DECIMALS TO 2
			SET ANSI ON
			SET TALK OFF
			SET DELETED ON
			SET CONSOLE OFF
			SET EXCLUSIVE OFF
			SET SAFETY OFF
			SET EXACT OFF
			SET STATUS BAR ON
			SET SYSMENU OFF
			SET ENGINEBEHAVIOR 70
			SET MULTILOCKS ON
			_VFP.AUTOYIELD = .lAutoYield
			.cCOMPUTERNAME = GETENV("COMPUTERNAME")
			.cUSERNAME = GETENV("USERNAME")
			.lLoggingIsOn = .lLoggingIsOn AND NOT EMPTY(.cLogFile)
		ENDWITH
	ENDFUNC


	FUNCTION DESTROY
		WITH THIS
			IF .lLoggingIsOn  THEN
				SET ALTERNATE OFF
				SET ALTERNATE TO
			ENDIF
		ENDWITH
		IF TYPE('.oExcel') = 'O' AND NOT ISNULL(.oExcel) THEN
			.oExcel.QUIT()
		ENDIF
		DODEFAULT()
	ENDFUNC


	FUNCTION MAIN
		LPARAMETERS tlTestMode
		WITH THIS
			LOCAL lnNumberOfErrors, lnAccountID, lnNumFiles1, lnNumFiles2, laFiles[1,5]

			TRY

				lnNumberOfErrors = 0
				
				.cStartTime = TTOC(DATETIME())

				.lTestMode = tlTestMode

				IF .lTestMode THEN
					.cLogFile = 'F:\UTIL\NAUTICA\STYLES\LOGFILES\NAUTICASTYLEIMPORT_log_TESTMODE.txt'
					.cSendTo = 'mbennett@fmiint.com'
				ENDIF
				
				IF .lLoggingIsOn THEN
					SET ALTERNATE TO (.cLogFile) ADDITIVE
					SET ALTERNATE ON
				ENDIF
				
				IF .lTestMode THEN
					.cInputFolder1 = 'F:\UTIL\NAUTICA\STYLES\TESTINPUT\'
					.cArchiveFolder = 'F:\UTIL\NAUTICA\STYLES\TESTINPUT\ARCHIVED\'
					.cOrigFolder = 'F:\UTIL\NAUTICA\STYLES\TESTINPUT\ORIG\'
				ELSE
					.cInputFolder1 = 'F:\FTPUSERS\NAUTICA\STYLEMASTER\'
					.cArchiveFolder = 'F:\FTPUSERS\NAUTICA\STYLEMASTER\ARCHIVED\'
					.cOrigFolder = 'F:\FTPUSERS\NAUTICA\STYLEMASTER\ORIG\'	
				ENDIF
				

				.TrackProgress('NAUTICA Style Import process started....', LOGIT+SENDIT)
				.TrackProgress('COMPUTERNAME = ' + .cCOMPUTERNAME, LOGIT+SENDIT)
				.TrackProgress('USERNAME = ' + .cUSERNAME, LOGIT+SENDIT)
				.TrackProgress('PROJECT = NAUTICASTYLEIMPORT', LOGIT+SENDIT)
				IF .lTestMode THEN
					.TrackProgress('                        TEST MODE                        ', LOGIT+SENDIT)
				ENDIF
				.TrackProgress('.cInputFolder1 = ' + .cInputFolder1, LOGIT+SENDIT)
				.TrackProgress('.cArchiveFolder = ' + .cArchiveFolder, LOGIT+SENDIT)
				.TrackProgress('.cOrigFolder = ' + .cOrigFolder, LOGIT+SENDIT)
				
				* OPEN Excel
				.oExcel = CREATEOBJECT("excel.application")
				.oExcel.displayalerts = .F.
				.oExcel.VISIBLE = .F.

				* for sql
				* 1/16/2018
				*useca("upcmast","wh",,,"select * from upcmast where accountid = " + ALLTRIM(STR(.nAccountID,4,0)),,"upcmastsql")
				useca("upcmast","wh",,,"select * from upcmast where inlist(accountid,687,6356)",,"upcmastsql")
				SELECT "upcmastsql"
				GO bottom
			
				* process first source folder
				.ProcessFolder( .cInputFolder1, .cArchiveFolder, .cOrigFolder )
				
*!*					SELECT UPCMAST
*!*					BROWSE

				*.UpdateMeasures( .cInputFolder1, .cArchiveFolder, .cOrigFolder )

				CLOSE DATABASES ALL
				
				.oExcel.Quit()

				.TrackProgress('NAUTICA Style Import process ended normally.', LOGIT+SENDIT)

			CATCH TO loError

				.TrackProgress('There was an error.',LOGIT+SENDIT)
				.TrackProgress('The Error # is: ' + TRANSFORM(loError.ERRORNO), LOGIT+SENDIT)
				.TrackProgress('The Error Message is: ' + TRANSFORM(loError.MESSAGE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred in Program: ' + TRANSFORM(loError.PROCEDURE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred at line #: ' + TRANSFORM(loError.LINENO), LOGIT+SENDIT)
				.TrackProgress('SPREADSHEET ROW = ' + TRANSFORM(.nRow), LOGIT+SENDIT)
				lnNumberOfErrors = lnNumberOfErrors + 1
				.lSendInternalEmailIsOn = .T.
				IF TYPE('.oExcel') = 'O' AND NOT ISNULL(.oExcel) THEN
					.oExcel.QUIT()
				ENDIF

			ENDTRY

			WAIT CLEAR
			***************** INTERNAL email results ******************************
			.TrackProgress('About to send status email.',LOGIT+SENDIT)
			.TrackProgress('The logfile is: ' + .cLogFile, LOGIT+SENDIT)
			.TrackProgress('NAUTICA Style Import process started: ' + .cStartTime, LOGIT+SENDIT)
			.TrackProgress('NAUTICA Style Import process finished: ' + TTOC(DATETIME()), LOGIT+SENDIT)

			IF .lSendInternalEmailIsOn THEN
				DO FORM dartmail2 WITH .cSendTo,.cFrom,.cSubject,.cCC,.cAttach,.cBodyText,"A"
				.TrackProgress('Sent status email.',LOGIT)
			ELSE
				.TrackProgress('Did not send status email: THIS.lSendInternalEmailIsOn = FALSE.',LOGIT+SENDIT)
			ENDIF

		ENDWITH
		RETURN
	ENDFUNC && main


	PROCEDURE ProcessFolder
		LPARAMETERS tcInputFolder, tcArchivedFolder, tcOrigFolder
		WITH THIS
			LOCAL laFiles[1,5], laFilesSorted[1,6], lnNumFiles, lnCurrentFile, lcSourceFile, lcArchivedFile, lcOrigFile
			LOCAL lnHandle, lcStr, lnCtr, oWorkbook, oWorksheet, lnMaxRow, lnRow, lcRow, lnDupeDelCtr, lnDupeCtr
			LOCAL llArchived, lnColumnF, lcColumnD, lnLength, lnWidth, lnHeight
			LOCAL lcGTINString, j, lnNumLines, lcInfoLine

			
			PRIVATE	m.UPCMASTID, m.ACCOUNTID, m.STYLE, m.PNP, m.DESCRIP, m.UPC, m.WEIGHT, m.CUBE, m.UIC, m.INFO
			STORE 0 TO m.UPCMASTID, m.WEIGHT, m.CUBE, m.UIC
			STORE '' TO m.STYLE, m.DESCRIP, m.UPC, m.INFO
			STORE .T. TO m.PNP

			m.ACCOUNTID = .nAccountID
			

			************************************************************************
			*** for sql
			PRIVATE m.ADDPROC, m.UPDPROC, m.ADDBY, m.ADDDT, m.UPDATEBY, m.UPDATEDT
			STORE .cAddProc TO m.ADDPROC, m.UPDPROC, m.ADDBY, m.UPDATEBY
			STORE .tADDDT TO m.ADDDT, m.UPDATEDT
			************************************************************************

			lnNumFiles = ADIR(laFiles,(tcInputFolder + "*.xlsX")) 

			IF lnNumFiles > 0 THEN

				* sort file list by date/time
				.SortArrayByDateTime(@laFiles, @laFilesSorted)

				* send an email only if files were found
				.lSendInternalEmailIsOn = .T.

				FOR lnCurrentFile = 1 TO lnNumFiles

					lcSourceFile = tcInputFolder + laFilesSorted[lnCurrentFile,1]
					lcArchivedFile = tcArchivedFolder + laFilesSorted[lnCurrentFile,1]
					lcOrigFile = tcOrigFolder + laFilesSorted[lnCurrentFile,1]

					.TrackProgress('lcSourceFile = ' + lcSourceFile,LOGIT+SENDIT)
					.TrackProgress('lcArchivedFile = ' + lcArchivedFile,LOGIT+SENDIT)
					.TrackProgress('lcOrigFile = ' + lcOrigFile,LOGIT+SENDIT)
					
					
					* move source file into ORIG folder so that, if an error causes the program to abort, the folderscan process does not keep reprocessing the bad source file in an infinite loop
					
					* if the file already exists in the orig folder, make sure it is not read-only.
					llArchived = FILE(lcOrigFile)
					IF llArchived THEN
						RUN ATTRIB -R &lcOrigFile.
						.TrackProgress('   Used ATTRIB -R for re-archiving : ' + lcOrigFile,LOGIT+SENDIT)
					ENDIF

					* archive the source file
					COPY FILE (lcSourceFile) TO (lcOrigFile)

					* always delete the source file
					*IF FILE(lcOrigFile) THEN
						DELETE FILE (lcSourceFile)
					*ENDIF
					
					
					* then process from the file once it is in the ORIG folder
					oWorkbook = .oExcel.workbooks.OPEN(lcOrigFile)
					oWorksheet = oWorkbook.Worksheets[1]  

					lnMaxRow = 5000
					
					lnCtr = 0
					lnDupeCtr = 0
					lnDupeDelCtr = 0
					
					
					*FOR lnRow = 3 TO lnMaxRow   && start at row x to skip header line(s)
					FOR lnRow = 2 TO lnMaxRow   && header increased to 3 lines in 2nd file for 6/15/2017
					
						.nRow = lnRow
					
						WAIT WINDOW NOWAIT 'Processing Spreadsheet Row: ' + TRANSFORM(lnRow)
						
						lcRow = ALLTRIM(STR(lnRow))
						* If the upc field is empty in the current row, we assume we are past the last row with data in it.
					
						m.STYLE = NVL(oWorksheet.RANGE("D"+lcRow).VALUE,'')
						
						*m.DESCRIP = NVL(oWorksheet.RANGE("B"+lcRow).VALUE,'')											
						
						m.UPC = NVL(oWorksheet.RANGE("B"+lcRow).VALUE,'')
						m.UPC = ALLTRIM(m.UPC)
						
						m.COLOR = ALLTRIM(NVL(oWorksheet.RANGE("E"+lcRow).VALUE,''))
						
						m.ID = ALLTRIM(NVL(oWorksheet.RANGE("F"+lcRow).VALUE,''))
												
						IF ( ISNULL(m.STYLE) OR EMPTY(m.STYLE) ) THEN
							* we are past the last data row! exit the loop...
							.TrackProgress('====> Empty Style encountered at row: ' + lcRow,LOGIT+SENDIT)
							EXIT FOR
						ENDIF
						
						* added 1/16/2018
						m.DIVISION = UPPER(ALLTRIM(NVL(oWorksheet.RANGE("C"+lcRow).VALUE,'')))
						
						* 1/16/2018 MB: make this TRUE per Juan Rocio.
						*m.PNP = .F.
						m.PNP = .T.
						
						* padr for exact comparison with upcmast fields
						m.UPC = PADR(ALLTRIM(m.UPC),14,' ')
						
						m.STYLE = PADR(ALLTRIM(m.STYLE),20,' ')

						*****************************************************************
						* for sql, save memvars gotten from Excel spreadsheet to properties for later recovery
						.lPNP = m.PNP
						.cDESCRIP = m.DESCRIP
						.cSTYLE = m.STYLE
						.cCOLOR = m.COLOR
						.cUPC = m.UPC
						.cID = m.ID
						.nWEIGHT = m.WEIGHT
						.nCUBE = m.CUBE
						.nUIC = m.UIC
						.cINFO = m.INFO
						*****************************************************************
						
						* save this to property for possible use below after scatter memvar
						*.nUPCMASTID = m.UPCMASTID
						
						* added 1/16/2018 MB: N means VF/Nautica; P means Kipling
						IF m.DIVISION = 'N' THEN
							m.ACCOUNTID = 687
						ELSE
							m.ACCOUNTID = 6356
						ENDIF
						.nAccountID = m.ACCOUNTID
						
						* sql updating below
						SELECT upcmastsql

						LOCATE FOR (ACCOUNTID = m.ACCOUNTID) ;
							AND (UPC == m.UPC)
							
						IF FOUND() THEN
								
							REPLACE upcmastsql.STYLE WITH m.STYLE, upcmastsql.PNP WITH m.PNP, upcmastsql.COLOR WITH m.COLOR, upcmastsql.ID WITH m.ID, ;
								upcmastsql.UPDPROC WITH m.UPDPROC, upcmastsql.UPDATEBY WITH m.UPDATEBY, upcmastsql.UPDATEDT WITH m.UPDATEDT IN upcmastsql
								
							lnDupeCtr = lnDupeCtr + 1
							
						ELSE
						
							SELECT upcmastsql
							SCATTER MEMVAR MEMO BLANK

							* restore memvars from properties
							STORE .cAddProc TO m.ADDPROC, m.UPDPROC, m.ADDBY, m.UPDATEBY 
							STORE .tADDDT TO m.ADDDT, m.UPDATEDT
							
							m.PNP = .lPNP
							m.DESCRIP = .cDESCRIP
							m.STYLE = .cSTYLE
							m.COLOR = .cCOLOR
							m.ID = .cID
							m.UPC = .cUPC
							m.ACCOUNTID = .nAccountID
							m.WEIGHT = .nWEIGHT
							m.CUBE = .nCUBE 
							m.UIC = .nUIC
							m.INFO = .cINFO 
							
							IF .lTestMode THEN
								m.UPCMASTID = 0
							ELSE					
								m.upcmastid=sqlgenpk("upcmast","wh")
							ENDIF
							*m.UPCMASTID = .nUPCMASTID
							
							INSERT INTO upcmastsql FROM MEMVAR
							lnCtr = lnCtr + 1
						ENDIF
						*****************************************************************	

						
					ENDFOR
					
					oWorkbook.close()
					
					IF .lTestMode THEN
						SELECT upcmastsql
						BROWSE
						.TrackProgress('TEST MODE: did not TU(upcmastsql)',LOGIT+SENDIT)
					ELSE					
						* tableupate SQL
						IF tu("upcmastsql") THEN
							.TrackProgress('SUCCESS: TU(upcmastsql) returned .T.!',LOGIT+SENDIT)
						ELSE
							* ERROR on table update
							.TrackProgress('ERROR: TU(upcmastsql) returned .F.!',LOGIT+SENDIT)
							=AERROR(UPCERR)
							IF TYPE("UPCERR")#"U" THEN 
								.TrackProgress('Error Description = ' + UPCERR[1,2],LOGIT+SENDIT)
							ENDIF
						ENDIF
					ENDIF

					.TrackProgress(TRANSFORM(lnCtr ) + ' INSERTS were made from ' + lcOrigFile,LOGIT+SENDIT)
					.TrackProgress(TRANSFORM(lnDupeCtr) + ' found records were updated ',LOGIT+SENDIT)

					* if the file already exists in the archive folder, make sure it is not read-only.
					* this is to prevent errors we get copying into archive on a resend of an already-archived file.
					* Necessary because the files in the archive folders were sometimes marked as read-only (not sure why)
					llArchived = FILE(lcArchivedFile)
					IF llArchived THEN
						RUN ATTRIB -R &lcArchivedFile.
						*.TrackProgress('   Used ATTRIB -R for re-archiving : ' + lcSourceFile,LOGIT+SENDIT)
						.TrackProgress('   Used ATTRIB -R for re-archiving : ' + lcOrigFile,LOGIT+SENDIT)
					ENDIF

					* archive the source file
					COPY FILE (lcOrigFile) TO (lcArchivedFile)

					* delete the source file
					IF FILE(lcArchivedFile) THEN
						DELETE FILE (lcOrigFile)
					ENDIF
			
					WAIT CLEAR

				ENDFOR && lnCurrentFile = 1 TO lnNumFiles
			ELSE
				.TrackProgress('Found no files in the NAUTICA source folder', LOGIT+SENDIT)
			ENDIF

		ENDWITH
		RETURN
	ENDPROC  &&  ProcessFolder
	


*!*		FUNCTION ExecSQL
*!*			LPARAMETERS tcSQL, tcCursorName, tlNoDataReturnedIsOkay
*!*			LOCAL llRetval, lnResult
*!*			WITH THIS
*!*				* close target cursor if it's open
*!*				IF USED(tcCursorName)
*!*					USE IN (tcCursorName)
*!*				ENDIF
*!*				WAIT WINDOW "Fetching remote data; please wait..." NOWAIT
*!*				lnResult = SQLEXEC(.nSQLHandle, tcSQL, tcCursorName)
*!*				llRetval = ( lnResult > 0 )
*!*				IF llRetval THEN
*!*					* see if any data came back
*!*					IF NOT tlNoDataReturnedIsOkay THEN
*!*						IF (NOT USED(tcCursorName)) OR EOF(tcCursorName) THEN
*!*							llRetval = .F.
*!*							.TrackProgress('No data was returned by this query: ' +CRLF + tcSQL, LOGIT+SENDIT)
*!*							.cSendTo = 'mbennett@fmiint.com'
*!*						ENDIF
*!*					ENDIF
*!*				ELSE
*!*					.TrackProgress("Result Code [" + TRANSFORM(lnResult) + "] returned by this query: " + tcSQL, LOGIT+SENDIT)
*!*					.cSendTo = 'mbennett@fmiint.com'
*!*				ENDIF
*!*				WAIT CLEAR
*!*				RETURN llRetval
*!*			ENDWITH
*!*		ENDFUNC


	PROCEDURE TrackProgress
		* do any combination of Wait Window, writing to logfile, and adding to body of email,
		* based on nFlags parameter.
		LPARAMETERS tcExpression, tnFlags
		WITH THIS
			IF BITAND(tnFlags,LOGIT) = LOGIT THEN
				IF .lLoggingIsOn THEN
					?
					? .cProcessName + "  " + TTOC(DATETIME()) + ": " + tcExpression
				ENDIF
			ENDIF
			IF .lWaitWindowIsOn THEN
				IF BITAND(tnFlags,NOWAITIT) = NOWAITIT THEN
					WAIT WINDOW tcExpression NOWAIT
				ENDIF
				IF BITAND(tnFlags,WAITIT) = WAITIT THEN
					WAIT WINDOW tcExpression TIMEOUT .nWaitWindowTimeout
				ENDIF
			ENDIF
			IF BITAND(tnFlags,SENDIT) = SENDIT THEN
				IF .lSendInternalEmailIsOn THEN
					.cBodyText = .cBodyText + tcExpression + CRLF + CRLF
				ENDIF
			ENDIF
		ENDWITH
	ENDPROC  &&  TrackProgress


	PROCEDURE SortArrayByDateTime
		* expects taInArray[] to be the result of an ADIR()
		LPARAMETERS taInArray, taOutArray
		EXTERNAL ARRAY taInArray, taOutArray
		LOCAL lnRows, lnCols, i, j
		* extending taInArray[] to process the files in date/time order
		lnRows = ALEN(taInArray,1)
		lnCols = ALEN(taInArray,2)
		* create taOutArray with one more column
		DIMENSION taOutArray[lnRows,lnCols + 1]
		* and fill it with taInArray columns AND (filedate + filetime) in new column
		* ( ACOPY() doesn't preserve elements properly because of different # of columns )
		FOR i = 1 TO lnRows
			FOR j = 1 TO lnCols
				taOutArray[i,j] = taInArray[i,j]
			ENDFOR
			taOutArray[i,lnCols + 1] = DTOS(taInArray[i,3]) + "_" + taInArray[i,4]
		ENDFOR
		* sort taOutArray on date/time column
		=ASORT(taOutArray,6)
	ENDPROC

ENDDEFINE
