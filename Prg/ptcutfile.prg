_screen.Caption = "P/T Cut File Creation"

if (type("gldor")="L" and !gldor)
	dstart=date()-90
	dend=date()
else
	dstart=date()-1
	dend=date()
endif

xsqlexec("select * from account where inactive=0","account",,"qq")
index on accountid tag accountid
set order to

**added creative rec (6416,6417,6418) - mvw 05/15/13
**removed creative rec per maria E - mvw 12/16/13
**added synclaire per Maria - mvw 12/16/15
**added 2xu (6665) per Maria - mvw 11/09/16
**added Merkury (6561) per Maria - mvw 01/18/17

for xxcounter = 1 to 6
	**include partial cuts for some accounts - mvw 03/27/18
	xincludepartials=.f.
	**option for xls format vs flat file - mvw 03/29/18
	xexcel=.f.

	do case
	case xxcounter=1 &&Moret
		xsqlexec("select * from ptbak where mod='7'",,,"wh")
		goffice="7"
		xsqlexec("select * from pt where mod='7'",,,"wh")
		index on str(accountid,4)+ship_ref tag acctpt
		xacctfilter="(inlist(accountid,"+gMoretAcctList+") or inlist(accountid,"+gMoretAcctList2+"))"
		xfolder="f:\moret\ptcutfiles\"
		xfilename=xfolder+"cuts"+strtran(strtran(strtran(ttoc(datetime()),"/",""),":","")," ","")+".txt"
		xsubject='Moret P/T Cuts: '+dtoc(dstart)

	case xxcounter=2 &&Nanjing CA
		xsqlexec("select * from ptbak where mod='1'",,,"wh")
		goffice="1"
		xsqlexec("select * from pt where mod='1'",,,"wh")
		index on str(accountid,4)+ship_ref tag acctpt
		xacctfilter="inlist(accountid,4610,4694)"
		xfolder="f:\nanjing\ptcutfiles\"
		xfilename=xfolder+"cacuts"+strtran(strtran(strtran(ttoc(datetime()),"/",""),":","")," ","")+".txt"
		xsubject='Nanjing CA P/T Cuts: '+dtoc(dstart)
		xincludepartials=.t.
		xexcel=.t.

	case xxcounter=3 &&Nanjing NJ
		xsqlexec("select * from ptbak where mod='I'",,,"wh")
		goffice="I"
		xsqlexec("select * from pt where mod='I'",,,"wh")
		index on str(accountid,4)+ship_ref tag acctpt
		xacctfilter="inlist(accountid,4610,4694)"
		xfolder="f:\nanjing\ptcutfiles\"
		xfilename=xfolder+"njcuts"+strtran(strtran(strtran(ttoc(datetime()),"/",""),":","")," ","")+".txt"
		xsubject='Nanjing NJ P/T Cuts: '+dtoc(dstart)
		xincludepartials=.t.
		xexcel=.t.

	case xxcounter=4 &&Synclaire - internal
		xsqlexec("select * from ptbak where mod='2'",,,"wh")
		goffice="2"
		xsqlexec("select * from pt where mod='2'",,,"wh")
		index on str(accountid,4)+ship_ref tag acctpt
		xacctfilter="inlist(accountid,6521)"
		xfolder="f:\synclaire\ptcutfiles\"
		xfilename=xfolder+"cacuts"+strtran(strtran(strtran(ttoc(datetime()),"/",""),":","")," ","")+".txt"
		xsubject='Synclaire P/T Cuts: '+dtoc(dstart)

	case xxcounter=5 &&2XU
		xsqlexec("select * from ptbak where mod='Y'",,,"wh")
		goffice="Y"
		xsqlexec("select * from pt where mod='Y'",,,"wh")
		index on str(accountid,4)+ship_ref tag acctpt
		xacctfilter="inlist(accountid,6665)"
		xfolder="f:\2xu\ptcutfiles\"
		xfilename=xfolder+"cacuts"+strtran(strtran(strtran(ttoc(datetime()),"/",""),":","")," ","")+".txt"
		xsubject='2XU P/T Cuts: '+dtoc(dstart)
		xincludepartials=.t.

	case xxcounter=6 &&Merkury
		xsqlexec("select * from ptbak where mod='2'",,,"wh")
		goffice="2"
		xsqlexec("select * from pt where mod='2'",,,"wh")
		index on str(accountid,4)+ship_ref tag acctpt
		xacctfilter="inlist(accountid,6561)"
		xfolder="f:\merkury\ptcutfiles\"
		xfilename=xfolder+"cacuts"+strtran(strtran(strtran(ttoc(datetime()),"/",""),":","")," ","")+".txt"
		xsubject='Merkury P/T Cuts: '+dtoc(dstart)
	endcase

	xfilestr=""

	**filter out pt = "SL" (sku lookup), yesenia created manual pts for sku lookups and then deletes - mvw 03/21/13
	xslpt=padr("SL",len(ptbak.ship_ref))
	select accountid, ship_ref, qty, cancel, consignee, cnee_ref from ptbak where &xacctfilter and between(adddt,dstart,dend) and ship_ref#xslpt into cursor xtemp readwrite

	if xexcel
		**in case forget to change extension above
		xfilename=strtran(lower(xfilename),".txt",".xls")
		select *, space(20) as acctname, space(20) as comment, space(20) as style, space(10) as color, space(10) as id, 00000 as cutqty from xtemp into cursor xrpt readwrite
	endif

	**added to remove deleted pt's that have either been recalled or reloaded after there deletion - mvw 09/16/08
	delete in xtemp for seek(str(xtemp.accountid,4)+xtemp.ship_ref,"pt","acctpt")

	select xtemp
	scan
		if xsqlexec("select outshipid from outship where mod='"+goffice+"' and accountid="+transform(xtemp.accountid)+" and ship_ref='"+xtemp.ship_ref+"'","xtemp2",,"wh") # 0
			delete in xtemp
		endif
	endscan

	scan
		if xexcel
			scatter memvar
			m.comment="P/T Screen"
			m.acctname=acctname(m.accountid)
			insert into xrpt from memvar
		else
			do case
			case xxcounter=4 &&Synclaire
				xfilestr=xfilestr+ship_ref+space(1)+acctname(accountid)+space(1)+consignee+space(1)+transform(qty)+space(1)+transform(cancel)+space(1)+"P/T Screen"+chr(13)+chr(10)
			case xxcounter=5 &&add qty for 2XU
				xfilestr=xfilestr+ship_ref+space(1)+acctname(accountid)+space(1)+cnee_ref+space(1)+transform(qty)+space(1)+"P/T Screen"+chr(13)+chr(10)
			otherwise
				xfilestr=xfilestr+ship_ref+space(1)+acctname(accountid)+space(1)+"P/T Screen"+chr(13)+chr(10)
			endcase
		endif
	endscan
	use in xtemp

	xsqlexec("select accountid, ship_ref, qty, cancel, consignee, cnee_ref from outship where "+xacctfilter+" " + ;
		"and picked={"+dtoc(dstart)+"} and bol_no='NOTHING SHIPPED'","xtemp",,"wh")
	
	scan
		if xexcel
			scatter memvar
			m.comment="Floor"
			m.acctname=acctname(m.accountid)
			insert into xrpt from memvar
		else
			do case
			case xxcounter=4 &&Synclaire
				xfilestr=xfilestr+ship_ref+space(1)+acctname(accountid)+space(1)+consignee+space(1)+transform(qty)+space(1)+transform(cancel)+space(1)+"Floor"+chr(13)+chr(10)
			case xxcounter=5 &&add qty for 2XU
				xfilestr=xfilestr+ship_ref+space(1)+acctname(accountid)+space(1)+cnee_ref+space(1)+transform(qty)+space(1)+"Floor"+chr(13)+chr(10)
			otherwise
				xfilestr=xfilestr+ship_ref+space(1)+acctname(accountid)+space(1)+"Floor"+chr(13)+chr(10)
			endcase
		endif
	endscan
	use in xtemp

	**include partial cuts for some accounts
	if xincludepartials
		**need to use outship/cuts instead of outship/outdet in case of overflow PTs - mvw 04/03/18
*!*			xsqlexec("select ship_ref, cancel, consignee, cnee_ref, qty, s.origqty as ptorigqty, d.* from outship s left join outdet d on d.outshipid=s.outshipid where "+strtran(xacctfilter,"accountid","s.accountid")+;
*!*				" and del_date={"+dtoc(dstart)+"} and s.qty<s.origqty and notonwip=0 and d.totqty<d.origqty","xtemp",,"wh")
		xsqlexec("select cancel, consignee, cnee_ref, qty, s.origqty as ptorigqty, d.* from outship s left join cut d on d.wo_num=s.wo_num and d.ship_ref=s.ship_ref where "+strtran(xacctfilter,"accountid","s.accountid")+;
			" and del_date={"+dtoc(dstart)+"} and s.qty<s.origqty and notonwip=0 order by s.ship_ref","xtemp",,"wh")

		scan for !emptynul(cutid)
			if xexcel
				scatter memvar
				m.comment="Partial Cut"
				m.acctname=acctname(m.accountid)
				m.qty=m.ptorigqty-m.qty
*				m.cutyqty=m.origqty-m.totqty
				insert into xrpt from memvar
			else
				do case
				case xxcounter=5 &&format for 2XU
*					xfilestr=xfilestr+ship_ref+space(1)+acctname(accountid)+space(1)+cnee_ref+space(1)+transform(ptorigqty-qty)+space(1)+"Partial Cut"+" - "+alltrim(style)+" "+alltrim(color)+" "+alltrim(id)+space(1)+transform(origqty-totqty)+chr(13)+chr(10)
					xfilestr=xfilestr+ship_ref+space(1)+acctname(accountid)+space(1)+cnee_ref+space(1)+transform(ptorigqty-qty)+space(1)+"Partial Cut"+" - "+alltrim(style)+" "+alltrim(color)+" "+alltrim(id)+space(1)+transform(cutqty)+chr(13)+chr(10)
				endcase
			endif
		endscan
		use in xtemp
	endif

	select 0
	use f:\3pl\data\mailmaster
	locate for &xacctfilter and taskname="PTCUTFILE"

	xto=alltrim(sendto)+iif(empty(sendtoalt),"",", "+alltrim(sendtoalt))
	xcc=alltrim(cc)+iif(empty(ccalt),"",", "+alltrim(ccalt))
	use in mailmaster

	xfrom="TGFSYSTEM"
	xto=strtran(xto,",",";") &&change to semicolon for email.prg
	xcc=strtran(xcc,",",";") &&change to semicolon for email.prg
	xmessage="Report Attached"

	do case
	case (xexcel and reccount("xrpt")=0) or (!xexcel and empty(xfilestr))
		xmessage="No cuts to report."
		emailx(xto,xcc,xsubject,xmessage)
	case xexcel
		select xrpt
		copy field ship_ref,acctname,cnee_ref,qty,comment,style,color,id,cutqty to &xfilename type xl5

		oexcel=createobject("excel.application")
		oexcel.displayalerts = .f.
		oworkbook=oexcel.workbooks.open(xfilename)
		osheet=oworkbook.worksheets[1]

		xsheet="osheet"
		exceltext("A1","PT No","bold center middle",,,,"RGB(255,255,128)")
		exceltext("B1","Account","bold center middle",,,,"RGB(255,255,128)")
		exceltext("C1","PO No","bold center middle",,,,"RGB(255,255,128)")
		exceltext("D1","PT Cut Qty","bold center middle",,,,"RGB(255,255,128)")
		exceltext("E1","Cut Type","bold center middle",,,,"RGB(255,255,128)")
		exceltext("F1","Style","bold center middle",,,,"RGB(255,255,128)")
		exceltext("G1","","bold center middle",,,,"RGB(255,255,128)")
		exceltext("H1","","bold center middle",,,,"RGB(255,255,128)")
		exceltext("I1","Cut Qty","bold center middle",,,,"RGB(255,255,128)")

		oworkbook.worksheets[1].Range("A1:I"+transform(Reccount("xrpt")+1)).Columns.autoFit()

		oworkbook.save()
		oexcel.quit()
		release osheet, oworkbook, oexcel
	
		emailx(xto,xcc,xsubject,xmessage,,xfilename)
	otherwise
		strtofile(xfilestr,xfilename)
		emailx(xto,xcc,xsubject,xmessage,,xfilename)
	endcase

	try
		set database to wh
		close databases
	catch
	endtry
endfor

use in account

gunshot()