* this only runs from the inbound WO screen
* used to run from stylechg  dy 11/7/16

lparameters xfrom, xwonum, xpicknpack
set step on 
if xpicknpack and (inwolog.quantity#0 or inwolog.container="PPADJ")
	return .f.
endif
 
llBugaboo=.f.

Do case
  Case xfrom="INWO" And inwolog.accountid != 5102
	xindet="vindet"
	xinloc="vinloc"
	xindetfilter="transform(units)+style+color+id+pack = transform(m.units)+m.style+m.color+m.id+m.pack"
  Otherwise
	xindet="indet"
	xinloc="inloc"
	xindetfilter="wo_num=xwonum and str(accountid,4)+transform(units)+style+color+id+pack = str(m.accountid,4)+transform(m.units)+m.style+m.color+m.id+m.pack"
Endcase 

select (xindet)
xxindetid=indetid
scatter memvar

do while .t.
	select (xindet)
	locate for &xindetfilter and indetid#m.indetid && and po=m.po 
	if !found()
		exit
	endif

	xduptotqty=totqty
	xdupremoveqty=removeqty
	xdupindetid=indetid
	xduptotwt=totwt
	xduptotcube=totcube
	delete 
	
	locate for indetid=xxindetid
	xcurrtotwt=totwt
	xcurrtotcube=totcube
	
	replace totqty with totqty+xduptotqty, ;
			removeqty with removeqty+xdupremoveqty, ;
			remainqty with totqty-removeqty, ;
			totwt with totqty*ctnwt, ;
			totcube with totqty*ctncube

    if llBugaboo=.t.
      replace id With "" && clear out the PO Number
    Endif 

	xchgwt=totwt-xcurrtotwt-xduptotwt
	xchgcube=totcube-xcurrtotcube-xduptotcube

	if xchgwt#0 or xchgcube#0
		if xfrom#"INWO"
			=seek(inwologid,"inwolog","inwologid")
		endif
		replace weight with inwolog.weight+xchgwt,;
				cuft with inwolog.cuft+xchgcube in inwolog
	endif
	
	select (xinloc)
	scan for indetid=xdupindetid
		xxinlocid=inlocid
		scatter memvar
		m.indetid=xxindetid
		delete
		locate for indetid=m.indetid and whseloc=m.whseloc and palletid=m.palletid
		if found()
			replace locqty with locqty+m.locqty, removeqty with removeqty+m.removeqty, remainqty with locqty-removeqty
		else
			insertinto(xinloc,"wh",.t.)
		endif
		go top
	endscan
	m.totqty=0
	m.locqty=0
enddo

select (xindet)
locate for indetid=xxindetid
