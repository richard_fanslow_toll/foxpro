* HRINSPERITYCENSUS.PRG
* census for Insperity

* Build EXE as F:\UTIL\HR\F:\UTIL\HR\FSA\HRFSADEDSRPT.exe
LOCAL loHRINSPERITYCENSUS

loHRINSPERITYCENSUS = CREATEOBJECT('HRINSPERITYCENSUS')

loHRINSPERITYCENSUS.MAIN()

CLOSE DATABASES ALL
RETURN


#DEFINE LOGIT 1
#DEFINE WAITIT 2
#DEFINE NOWAITIT 4
#DEFINE SENDIT 8
#DEFINE CRLF CHR(13) + CHR(10)
#DEFINE RETURN_DATA_MANDATORY .F.
#DEFINE RETURN_DATA_NOT_MANDATORY .T.
#DEFINE CSVFORMAT 6
#DEFINE WRITE_ONLY_UNBUFFERED 11
#DEFINE COMMA ","
#DEFINE xlEdgeBottom 9
#DEFINE xlContinuous 1
#DEFINE xlMedium -4138
#DEFINE xlFONT_RED -16776961

DEFINE CLASS HRINSPERITYCENSUS AS CUSTOM

	cProcessName = 'HRINSPERITYCENSUS'

	lTestMode = .T.  && .F. FOR NORMAL OPERATION, .T. TO SUPPRESS ALL BUT DEVELOPER EMAILS AND CHANGE INPUT TABLE, ETC.

	dToday = DATE()

	lAutoYield = .T.

	cStartTime = TTOC(DATETIME())
	
	* PROCESSING PROPERTIES
	*lOnlyUnion = .F.
	lOnlyUnion = .T.

	* connection properties
	nSQLHandle = 0

	* wait window properties
	nWaitWindowTimeout = 2
	lWaitWindowIsOn = .T.

	* logfile properties
	lLoggingIsOn = .T.
	cLogFile = 'F:\UTIL\INSPERITY\LOGFILES\HRINSPERITYCENSUS_log.txt'

	* INTERNAL email properties
	lSendInternalEmailIsOn = .T.
	cFrom = 'Mark.Bennett@Tollgroup.com'
	cSendTo = 'mbennett@fmiint.com'
	cCC = ''
	cSubject = ''
	cAttach = ''
	cBodyText = ''

	cCOMPUTERNAME = ''
	cUSERNAME = ''


	FUNCTION INIT
		IF NOT DODEFAULT()
			RETURN .F.
		ENDIF
		WITH THIS
			*SET RESOURCE OFF
			CLOSE DATA
			SET DECIMALS TO 4
			SET CENTURY ON
			SET DATE AMERICAN
			SET HOURS TO 24
			SET ANSI ON
			SET TALK OFF
			SET DELETED ON
			SET CONSOLE OFF
			SET EXCLUSIVE OFF
			SET SAFETY OFF
			SET EXACT OFF
			SET STATUS BAR ON
			SET SYSMENU OFF
			SET ENGINEBEHAVIOR 70
			_VFP.AUTOYIELD = .lAutoYield
			.cCOMPUTERNAME = GETENV("COMPUTERNAME")
			.cUSERNAME = GETENV("USERNAME")
			IF .lTestMode THEN
				.cLogFile = 'F:\UTIL\INSPERITY\LOGFILES\HRINSPERITYCENSUS_log_TESTMODE.txt'
				.cSendTo = 'mbennett@fmiint.com'
			ENDIF
			.lLoggingIsOn = .lLoggingIsOn AND NOT EMPTY(.cLogFile)
			IF .lLoggingIsOn THEN
				SET ALTERNATE TO (.cLogFile) ADDITIVE
				SET ALTERNATE ON
			ENDIF
			SET PROCEDURE TO VALIDATIONS ADDITIVE
		ENDWITH
	ENDFUNC


	FUNCTION DESTROY
		WITH THIS
			IF .lLoggingIsOn  THEN
				SET ALTERNATE OFF
				SET ALTERNATE TO
			ENDIF
		ENDWITH
		DODEFAULT()
	ENDFUNC


	FUNCTION MAIN
		WITH THIS
			LOCAL lnNumberOfErrors, lcSQL, lcSQL2, loError, ldToday, lcUnionWhere
			LOCAL lcFiletoSaveAs, lcSpreadsheetTemplate
			LOCAL ldFromDate, ldToDate, lcADPFromDate, lcADPToDate, ldTermCheckDate, lcADPTermCheckDate
			LOCAL lcDateSuffix, lnTotalFSA, ldEligDate, lcHomeDept
			LOCAL i, oExcel, oWorkbook, oWorksheet, lnStartRow, lcStartRow, lnRow, lcRow
			LOCAL lcFirstName, lcNewFirstName, lcLastName, lnSpacePos, lcMI, lnMaxAmount

			TRY
				lnNumberOfErrors = 0

				.TrackProgress("INSPERITY CENSUS process started....", LOGIT+SENDIT+NOWAITIT)
				.TrackProgress('COMPUTERNAME = ' + .cCOMPUTERNAME, LOGIT+SENDIT)
				.TrackProgress('USERNAME = ' + .cUSERNAME, LOGIT+SENDIT)
				.TrackProgress('PROJECT = HRINSPERITYCENSUS', LOGIT+SENDIT)
				IF .lTestMode THEN
					.TrackProgress('                        TEST MODE                        ', LOGIT+SENDIT)
				ENDIF

				lcDateSuffix = DTOC(.dToday)
				.cSubject = 'INSPERITY CENSUS for ' + lcDateSuffix

				*lcSpreadsheetTemplate = 'F:\UTIL\INSPERITY\TEMPLATES\MERGE MASTER.XLSX'
				lcSpreadsheetTemplate = 'F:\UTIL\INSPERITY\TEMPLATES\EMPLOYEE DATA SPREADSHEET 2.XLSX'
				
				
				lcFiletoSaveAs = 'F:\UTIL\INSPERITY\REPORTS\EMPLOYEE DATA SPREADSHEET ' + STRTRAN(lcDateSuffix,"/","")
				lcSaveFile = lcFiletoSaveAs + '.XLSX'
				
				.TrackProgress('Creating spreadsheet: ' + lcSaveFile, LOGIT+SENDIT+NOWAITIT)

				IF .lOnlyUnion THEN
					lcUnionWhere = " AND A.HOMEDEPARTMENT IN ('020655','030650','040660','550650') "
				ELSE
					lcUnionWhere = ""
				ENDIF
					
				* delete output file if it already exists...
				IF FILE(lcSaveFile) THEN
					DELETE FILE (lcSaveFile)
				ENDIF

				* now connect to ADP...

				.nSQLHandle = SQLCONNECT("PCPAYWIN","MBENNETT","VP737A")

				IF .nSQLHandle > 0 THEN

					lcWhere = ""

					* THIS FOLLOWING SQL GETS ALL ACTIVE EMPLOYEES

					SET TEXTMERGE ON
					TEXT TO	lcSQL NOSHOW
SELECT
A.COMPANYCODE AS ADP_COMP,
{fn LEFT(A.HOMEDEPARTMENT,2)} AS DIVISION,
{fn SUBSTRING(A.HOMEDEPARTMENT,3,4)} AS DEPARTMENT,
C.DESCRIPTION AS DEPTDESC,
A.LASTNAME,
A.FIRSTNAME,
' ' AS MI,
A.NAME,
A.FILE# AS FILE_NUM,
A.HIREDATE,
{fn LEFT(A.CUSTAREA3,3)} AS WORKSITE,
{fn LEFT(A.CUSTAREA1,1)} AS COLLAR,
{fn SUBSTRING(A.CUSTAREA1,2,2)} AS DEPT2,
'No ' AS UNIONYN,
A.GENDER,
{FN CONCAT(A.STREETLINE1,{FN CONCAT(' ',{FN IFNULL(A.STREETLINE2,'')})})} AS ADDRESS,
A.CITY,
A.STATE,
A.ZIPCODE,
A.BIRTHDATE,
{FN IFNULL(A.RATE1AMT,0.00)} AS RATE1AMT,
A.SOCIALSECURITY# AS SS_NUM,
A.AREACODEPHONE# AS PHONE,
A.EEOCJOBCLASS AS JOBCLASS,
B.DESCRIPTION_TX AS JOBDESC,
{FN IFNULL(a.ANNUALSALARY,0.00)} AS ANNSALARY,
'   ' AS EXEMPT,
'         ' AS PAYFREQ,
'             ' AS PAYTYPE,
0000.00 AS HOURLYPAY,
0000.00 AS PRDSALARY,
CASE WHEN {FN IFNULL(A.CUSTAREA2,' ')} = 'P' THEN 'PART-TIME' ELSE 'FULL-TIME' END AS TIMETYPE
FROM REPORTS.V_EMPLOYEE A, PCPAYSYS.T_CO_JOB_CLASS B, REPORTS.V_DEPARTMENT C
WHERE (B.CO_C(+) = A.COMPANYCODE) AND (B.EEOC_JOB_CLASS_C(+) = A.EEOCJOBCLASS)
AND (C.COMPANYCODE(+) = A.COMPANYCODE) AND (C.DEPARTMENT(+) = A.HOMEDEPARTMENT)
AND A.STATUS <> 'T'
<<lcUnionWhere>>
ORDER BY 1,2,3,5
					ENDTEXT
					SET TEXTMERGE OFF


					IF USED('CUREEINFOPRE') THEN
						USE IN CUREEINFOPRE
					ENDIF
					IF USED('CUREEINFO') THEN
						USE IN CUREEINFO
					ENDIF
					
					USE F:\UTIL\INSPERITY\DATA\WORKSITES.DBF IN 0 ALIAS WORKSITES

					IF .lTestMode THEN
						.TrackProgress('lcSQL = ' + lcSQL, LOGIT+SENDIT)
					ENDIF

					IF .ExecSQL(lcSQL, 'CUREEINFOPRE', RETURN_DATA_MANDATORY) THEN

						*!*	SELECT CUREEINFOPRE
						*!*	BROWSE
						*!*	THROW


						SELECT CUREEINFOPRE
						SCAN

							* remove middle initial if there is one and place in its own field
							lcFirstName = ALLTRIM(CUREEINFOPRE.FIRSTNAME)
							lnSpacePos = AT(' ',lcFirstName)
							IF lnSpacePos > 0 THEN
								* replace First name with everything to the left of the space
								lcNewFirstName = LEFT(lcFirstName, lnSpacePos - 1)
								REPLACE CUREEINFOPRE.FIRSTNAME WITH lcNewFirstName IN CUREEINFOPRE

								* replace MI with 1st char of everything to the right of the space
								lcMI = UPPER(LEFT(ALLTRIM(SUBSTR(lcFirstName, lnSpacePos)),1))
								IF NOT EMPTY(lcMI) THEN
									REPLACE CUREEINFOPRE.MI WITH lcMI IN CUREEINFOPRE
								ENDIF
							ENDIF

						ENDSCAN
						

						* correct invalid worksites, etc.
						SELECT CUREEINFOPRE
						SCAN
						
							IF ALLTRIM(UPPER(CUREEINFOPRE.WORKSITE)) == 'IL2' THEN
								REPLACE CUREEINFOPRE.WORKSITE WITH 'IL1' IN CUREEINFOPRE
							ENDIF
							IF ALLTRIM(UPPER(CUREEINFOPRE.WORKSITE)) == 'NY2' THEN
								REPLACE CUREEINFOPRE.WORKSITE WITH 'NY1' IN CUREEINFOPRE
							ENDIF

							IF CUREEINFOPRE.ADP_COMP = 'E87' THEN
								IF CUREEINFOPRE.DEPARTMENT = '0655' THEN
									* OTR DRIVER
									REPLACE CUREEINFOPRE.EXEMPT WITH 'No ', ;
										CUREEINFOPRE.PAYFREQ WITH  'WEEKLY', ;
										CUREEINFOPRE.PAYTYPE WITH  'Mileage-based', ;
										CUREEINFOPRE.HOURLYPAY WITH 0.0, ;
										CUREEINFOPRE.PRDSALARY WITH 0.0 ;
										IN CUREEINFOPRE
								ELSE
									* REGULAR HOURLY
									REPLACE CUREEINFOPRE.EXEMPT WITH 'No ', ;
										CUREEINFOPRE.PAYFREQ WITH  'WEEKLY', ;
										CUREEINFOPRE.PAYTYPE WITH  'HOURLY', ;
										CUREEINFOPRE.HOURLYPAY WITH CUREEINFOPRE.RATE1AMT, ;
										CUREEINFOPRE.PRDSALARY WITH (CUREEINFOPRE.RATE1AMT * 40) ;
										IN CUREEINFOPRE
								ENDIF
							ELSE
								* SALARIED
								REPLACE CUREEINFOPRE.EXEMPT WITH 'Yes', ;
									CUREEINFOPRE.PAYFREQ WITH  'BI-WEEKLY', ;
									CUREEINFOPRE.PAYTYPE WITH  'SALARY', ;
									CUREEINFOPRE.HOURLYPAY WITH (CUREEINFOPRE.RATE1AMT / 80), ;
									CUREEINFOPRE.PRDSALARY WITH CUREEINFOPRE.RATE1AMT ;
									IN CUREEINFOPRE
							ENDIF

							lcHomeDept = ALLTRIM(CUREEINFOPRE.DIVISION) + ALLTRIM(CUREEINFOPRE.DEPARTMENT)
							IF INLIST(lcHomeDept,'020655','030650','040660','550650') THEN
								* they are in the Union
								REPLACE CUREEINFOPRE.UNIONYN WITH 'Yes' IN CUREEINFOPRE
							ENDIF
*!*							
						ENDSCAN
						
						* JOIN TO GET WORKSITE INFO
						SELECT A.*, B.* ;
							FROM CUREEINFOPRE A ;
							LEFT OUTER JOIN ;
							WORKSITES B ;
							ON B.WWORKSITE = A.WORKSITE ;
							INTO CURSOR CUREEINFO ;
							ORDER BY A.LASTNAME ;
							READWRITE
							
							
							

*!*	SELECT CUREEINFO
*!*	BROWSE
*!*	THROW

						SELECT CUREEINFO

						oExcel = CREATEOBJECT("excel.application")
						oExcel.displayalerts = .F.
						oExcel.VISIBLE = .F.
						oWorkbook = oExcel.workbooks.OPEN(lcSpreadsheetTemplate)
						
						
						oWorkbook.SAVEAS(lcFiletoSaveAs)
						oWorksheet = oWorkbook.Worksheets[1]

						lnRow = 1

						SELECT CUREEINFO
						SCAN
							lnRow = lnRow + 1
							lcRow = ALLTRIM(STR(lnRow))
							oWorksheet.RANGE("B"+lcRow).VALUE = CUREEINFO.FIRSTNAME
							oWorksheet.RANGE("C"+lcRow).VALUE = CUREEINFO.MI
							oWorksheet.RANGE("D"+lcRow).VALUE = CUREEINFO.LASTNAME
							oWorksheet.RANGE("E"+lcRow).VALUE = "'" + CUREEINFO.SS_NUM
							oWorksheet.RANGE("F"+lcRow).VALUE = "'" + RIGHT(CUREEINFO.SS_NUM,4)
							oWorksheet.RANGE("G"+lcRow).VALUE = TTOD(CUREEINFO.BIRTHDATE)
							oWorksheet.RANGE("H"+lcRow).VALUE = CUREEINFO.GENDER
							oWorksheet.RANGE("I"+lcRow).VALUE = CUREEINFO.NAME
							* col  J is going to be their Insperity # - no relation to our filenum
							*oWorksheet.RANGE("J"+lcRow).VALUE = "'" + TRANSFORM(CUREEINFO.FILE_NUM)
							oWorksheet.RANGE("K"+lcRow).VALUE = CUREEINFO.ADDRESS
							oWorksheet.RANGE("L"+lcRow).VALUE = CUREEINFO.CITY
							*oWorksheet.RANGE("M"+lcRow).VALUE = CUREEINFO.COUNTY
							oWorksheet.RANGE("N"+lcRow).VALUE = CUREEINFO.STATE
							oWorksheet.RANGE("O"+lcRow).VALUE = "'" + CUREEINFO.ZIPCODE
							
							oWorksheet.RANGE("P"+lcRow).VALUE = 'Actual'
							oWorksheet.RANGE("Q"+lcRow).VALUE = CUREEINFO.WSTREET
							oWorksheet.RANGE("R"+lcRow).VALUE = CUREEINFO.WCITY
							oWorksheet.RANGE("S"+lcRow).VALUE = CUREEINFO.WSTATE
							oWorksheet.RANGE("T"+lcRow).VALUE = CUREEINFO.WCOUNTY
							oWorksheet.RANGE("U"+lcRow).VALUE = "'" + CUREEINFO.WZIP
							oWorksheet.RANGE("V"+lcRow).VALUE = CUREEINFO.HIREDATE
							oWorksheet.RANGE("W"+lcRow).VALUE = {^2014-01-21}  && HIRE DATE WITH INSPERITY
							oWorksheet.RANGE("X"+lcRow).VALUE = CUREEINFO.JOBDESC						
							oWorksheet.RANGE("Y"+lcRow).VALUE = 'No'						
							oWorksheet.RANGE("AE"+lcRow).VALUE = 'No'						
							oWorksheet.RANGE("AF"+lcRow).VALUE = "'" + CUREEINFO.EXEMPT						

							oWorksheet.RANGE("AG"+lcRow).VALUE = "'" + CUREEINFO.TIMETYPE							
							oWorksheet.RANGE("AH"+lcRow).VALUE = "'" + CUREEINFO.PAYFREQ
							oWorksheet.RANGE("AI"+lcRow).VALUE = "'" + CUREEINFO.PAYTYPE
							oWorksheet.RANGE("AJ"+lcRow).VALUE = CUREEINFO.HOURLYPAY
							
							oWorksheet.RANGE("AK"+lcRow).VALUE = CUREEINFO.ANNSALARY
							oWorksheet.RANGE("AL"+lcRow).VALUE = CUREEINFO.PRDSALARY
							oWorksheet.RANGE("AM"+lcRow).VALUE = 'No'
							oWorksheet.RANGE("AO"+lcRow).VALUE = CUREEINFO.UNIONYN
							oWorksheet.RANGE("AP"+lcRow).VALUE = 'No'
							oWorksheet.RANGE("AQ"+lcRow).VALUE = 'No'
							oWorksheet.RANGE("AR"+lcRow).VALUE = CUREEINFO.ADP_COMP							
							oWorksheet.RANGE("AS"+lcRow).VALUE = "'" + CUREEINFO.DIVISION
							oWorksheet.RANGE("AT"+lcRow).VALUE = "'" + CUREEINFO.DEPARTMENT
							oWorksheet.RANGE("AU"+lcRow).VALUE = CUREEINFO.WORKSITE
							
							
							
*!*								oWorksheet.RANGE("P"+lcRow).VALUE = CUREEINFO.PHONE
*!*								
*!*								*oWorksheet.RANGE("AB"+lcRow).VALUE = 'Actual'
*!*								*oWorksheet.RANGE("AC"+lcRow).VALUE = CUREEINFO.WSTREET
*!*								*oWorksheet.RANGE("AD"+lcRow).VALUE = CUREEINFO.WCITY
*!*								*oWorksheet.RANGE("AE"+lcRow).VALUE = CUREEINFO.WSTATE
*!*								*oWorksheet.RANGE("AF"+lcRow).VALUE = CUREEINFO.WCOUNTY
*!*								*oWorksheet.RANGE("AG"+lcRow).VALUE = "'" + CUREEINFO.WZIP
*!*								oWorksheet.RANGE("AH"+lcRow).VALUE = CUREEINFO.WPHONE
*!*								oWorksheet.RANGE("AI"+lcRow).VALUE = CUREEINFO.WFAX
*!*								oWorksheet.RANGE("AK"+lcRow).VALUE = CUREEINFO.WORKSITE
*!*								
*!*								
*!*								oWorksheet.RANGE("AQ"+lcRow).VALUE = "'" + CUREEINFO.JOBCLASS
*!*								
*!*								*oWorksheet.RANGE("AV"+lcRow).VALUE = "'" + CUREEINFO.EXEMPT

*!*								oWorksheet.RANGE("BC"+lcRow).VALUE = 'No'
*!*								oWorksheet.RANGE("BE"+lcRow).VALUE = CUREEINFO.UNIONYN
*!*								oWorksheet.RANGE("BF"+lcRow).VALUE = 'No'
*!*								oWorksheet.RANGE("BG"+lcRow).VALUE = 'No'

*!*								oWorksheet.RANGE("BH"+lcRow).VALUE = CUREEINFO.ADP_COMP
*!*								
*!*								oWorksheet.RANGE("BI"+lcRow).VALUE = CUREEINFO.DIVISION
*!*								oWorksheet.RANGE("BJ"+lcRow).VALUE = CUREEINFO.DEPARTMENT
*!*								oWorksheet.RANGE("BK"+lcRow).VALUE = CUREEINFO.WORKSITE
*!*								oWorksheet.RANGE("BL"+lcRow).VALUE = CUREEINFO.DEPT2
*!*								oWorksheet.RANGE("BM"+lcRow).VALUE = CUREEINFO.COLLAR


							
						ENDSCAN

						IF TYPE('oWorkbook') = "O" THEN
							oWorkbook.SAVE()
							oWorkbook = NULL
						ENDIF
						IF TYPE('oExcel') = "O" THEN
							oExcel.QUIT()
							oExcel = NULL
						ENDIF

						IF FILE(lcSaveFile) THEN
							.cAttach = lcSaveFile
							.cBodyText = "See attached report." + CRLF + CRLF + "<<REPORT LOG FOLLOWS>>" + CRLF + .cBodyText
						ELSE
							.TrackProgress("ERROR attaching " + lcSaveFile, LOGIT+SENDIT+NOWAITIT)
						ENDIF

					=SQLDISCONNECT(.nSQLHandle)

				ENDIF  &&  .ExecSQL(lcSQL, 'SQLCURSOR1', RETURN_DATA_MANDATORY)
			ELSE
				* connection error
				.TrackProgress('Unable to connect to ADP Payroll System.', LOGIT+SENDIT)
			ENDIF   &&  .nSQLHandle > 0

			.TrackProgress("INSPERITY CENSUS process ended normally.", LOGIT+SENDIT+NOWAITIT)


		CATCH TO loError

			.TrackProgress('There was an error.',LOGIT+SENDIT)
			.TrackProgress('The Error # is: ' + TRANSFORM(loError.ERRORNO), LOGIT+SENDIT)
			.TrackProgress('The Error Message is: ' + TRANSFORM(loError.MESSAGE), LOGIT+SENDIT)
			.TrackProgress('The Error occurred in Program: ' + TRANSFORM(loError.PROCEDURE), LOGIT+SENDIT)
			.TrackProgress('The Error occurred at line #: ' + TRANSFORM(loError.LINENO), LOGIT+SENDIT)
			lnNumberOfErrors = lnNumberOfErrors + 1

			IF TYPE('oWorkbook') = "O" AND NOT ISNULL(oWorkbook) THEN
				oWorkbook.SAVE()
			ENDIF
			IF TYPE('oExcel') = "O" AND NOT ISNULL(oExcel) THEN
				oExcel.QUIT()
			ENDIF

			CLOSE DATABASES ALL

		ENDTRY

		WAIT CLEAR
		***************** INTERNAL email results ******************************


		.TrackProgress('About to send status email.',LOGIT)
		.TrackProgress('The logfile is: ' + .cLogFile, SENDIT)
		.TrackProgress("INSPERITY CENSUS process started: " + .cStartTime, LOGIT+SENDIT)
		.TrackProgress("INSPERITY CENSUS process finished: " + TTOC(DATETIME()), LOGIT+SENDIT)

		IF .lSendInternalEmailIsOn THEN
			DO FORM dartmail2 WITH .cSendTo,.cFrom,.cSubject,.cCC,.cAttach,.cBodyText,"A"
			.TrackProgress('Sent status email.',LOGIT)
		ELSE
			.TrackProgress('Did not send status email: THIS.lSendInternalEmailIsOn = FALSE.',LOGIT)
		ENDIF

	ENDWITH
	RETURN
ENDFUNC && main


	FUNCTION ExecSQL
		LPARAMETERS tcSQL, tcCursorName, tlNoDataReturnedIsOkay
		LOCAL llRetval, lnResult
		WITH THIS
			* close target cursor if it's open
			IF USED(tcCursorName)
				USE IN (tcCursorName)
			ENDIF
			WAIT WINDOW "Fetching remote data; please wait..." NOWAIT
			lnResult = SQLEXEC(.nSQLHandle, tcSQL, tcCursorName)
			llRetval = ( lnResult > 0 )
			IF llRetval THEN
				* see if any data came back
				IF NOT tlNoDataReturnedIsOkay THEN
					IF (NOT USED(tcCursorName)) OR EOF(tcCursorName) THEN
						llRetval = .F.
						.TrackProgress('No data was returned by this query: ' +CRLF + tcSQL, LOGIT+SENDIT)
						.cSendTo = 'Mark Bennett <mbennett@fmiint.com>'
					ENDIF
				ENDIF
			ELSE
				.TrackProgress("Result Code [" + TRANSFORM(lnResult) + "] returned by this query: " + tcSQL, LOGIT+SENDIT)
				.cSendTo = 'Mark Bennett <mbennett@fmiint.com>'
			ENDIF
			WAIT CLEAR
			RETURN llRetval
		ENDWITH
	ENDFUNC


	PROCEDURE TrackProgress
		* do any combination of Wait Window, writing to logfile, and adding to body of email,
		* based on nFlags parameter.
		LPARAMETERS tcExpression, tnFlags
		WITH THIS
			IF BITAND(tnFlags,LOGIT) = LOGIT THEN
				IF .lLoggingIsOn THEN
					?
					? .cProcessName + "  " + TTOC(DATETIME()) + ": " + tcExpression
				ENDIF
			ENDIF
			IF .lWaitWindowIsOn THEN
				IF BITAND(tnFlags,NOWAITIT) = NOWAITIT THEN
					WAIT WINDOW tcExpression NOWAIT
				ENDIF
				IF BITAND(tnFlags,WAITIT) = WAITIT THEN
					WAIT WINDOW tcExpression TIMEOUT .nWaitWindowTimeout
				ENDIF
			ENDIF
			IF BITAND(tnFlags,SENDIT) = SENDIT THEN
				IF .lSendInternalEmailIsOn THEN
					.cBodyText = .cBodyText + tcExpression + CRLF + CRLF
				ENDIF
			ENDIF
		ENDWITH
	ENDPROC  &&  TrackProgress


	FUNCTION GetSQLStartDateTimeString
		* returns a timestamp string for use in SQL queries
		LPARAMETERS tdDate
		LOCAL lcSetDate, lcSQLDateTimeString
		lcSetDate = SET('DATE')
		SET DATE YMD
		lcSQLDateTimeString   = "{ts '" + TRANSFORM(YEAR(tdDate)) + "-" + PADL(MONTH(tdDate),2,"0") + "-" + PADL(DAY(tdDate),2,"0") + " 00:00:00.0'}"
		SET DATE &lcSetDate.
		RETURN lcSQLDateTimeString
	ENDFUNC


	FUNCTION GetSQLEndDateTimeString
		* returns a timestamp string for use in SQL queries
		LPARAMETERS tdDate
		LOCAL lcSetDate, lcSQLDateTimeString
		lcSetDate = SET('DATE')
		SET DATE YMD
		lcSQLDateTimeString = "{ts '" + TRANSFORM(YEAR(tdDate)) + "-" + PADL(MONTH(tdDate),2,"0") + "-" + PADL(DAY(tdDate),2,"0") + " 23:59:59.9'}"
		SET DATE &lcSetDate.
		RETURN lcSQLDateTimeString
	ENDFUNC


ENDDEFINE
