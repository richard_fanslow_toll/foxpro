* WIP for Marc Jacobs

utilsetup("MJMULTLOC")

if holiday(date())
  return
endif

guserid="AUTO"

xsqlexec("select * from account where inactive=0","account",,"qq")
index on accountid tag accountid
set order to

xsqlexec("select * from phys where mod='J'",,,"wh")

create cursor xx (acctname c(30), style c(20), color c(10), id c(10), numlocs n(3), ;
  loc1 c(7), qty1 n(7), loc2 c(7), qty2 n(7), loc3 c(7), qty3 n(7), loc4 c(7), qty4 n(7), loc5 c(7), qty5 n(7), ;
  loc6 c(7), qty6 n(7), loc7 c(7), qty7 n(7), loc8 c(7), qty8 n(7), loc9 c(7), qty9 n(7), loc10 c(7), qty10 n(7), ;
  loc11 c(7), qty11 n(7), loc12 c(7), qty12 n(7), loc13 c(7), qty13 n(7), loc14 c(7), qty14 n(7), loc15 c(7), qty15 n(7), ;
  loc16 c(7), qty16 n(7), loc17 c(7), qty17 n(7), loc18 c(7), qty18 n(7), loc19 c(7), qty19 n(7), loc20 c(7), qty20 n(7), ;
  loc21 c(7), qty21 n(7), loc22 c(7), qty22 n(7), loc23 c(7), qty23 n(7), loc24 c(7), qty24 n(7), loc25 c(7), qty25 n(7), ;
  loc26 c(7), qty26 n(7), loc27 c(7), qty27 n(7), loc28 c(7), qty28 n(7), loc29 c(7), qty29 n(7), loc30 c(7), qty30 n(7), ;
  loc31 c(7), qty31 n(7), loc32 c(7), qty32 n(7), loc33 c(7), qty33 n(7), loc34 c(7), qty34 n(7), loc35 c(7), qty35 n(7), ;
  loc36 c(7), qty36 n(7), loc37 c(7), qty37 n(7), loc38 c(7), qty38 n(7), loc39 c(7), qty39 n(7), loc40 c(7), qty40 n(7), ;
  loc41 c(7), qty41 n(7), loc42 c(7), qty42 n(7), loc43 c(7), qty43 n(7), loc44 c(7), qty44 n(7), loc45 c(7), qty45 n(7), ;
  loc46 c(7), qty46 n(7), loc47 c(7), qty47 n(7), loc48 c(7), qty48 n(7), loc49 c(7), qty49 n(7), loc50 c(7), qty50 n(7), ;
  loc51 c(7), qty51 n(7), loc52 c(7), qty52 n(7), loc53 c(7), qty53 n(7), loc54 c(7), qty54 n(7), loc55 c(7), qty55 n(7), ;
  loc56 c(7), qty56 n(7), loc57 c(7), qty57 n(7), loc58 c(7), qty58 n(7), loc59 c(7), qty59 n(7), loc60 c(7), qty60 n(7), ;
  loc61 c(7), qty61 n(7), loc62 c(7), qty62 n(7), loc63 c(7), qty63 n(7), loc64 c(7), qty64 n(7), loc65 c(7), qty65 n(7), ;
  loc66 c(7), qty66 n(7), loc67 c(7), qty67 n(7), loc68 c(7), qty68 n(7), loc69 c(7), qty69 n(7), loc70 c(7), qty70 n(7), ;
  loc71 c(7), qty71 n(7), loc72 c(7), qty72 n(7), loc73 c(7), qty73 n(7), loc74 c(7), qty74 n(7), loc75 c(7), qty75 n(7), ;
  loc76 c(7), qty76 n(7), loc77 c(7), qty77 n(7), loc78 c(7), qty78 n(7), loc79 c(7), qty79 n(7), loc80 c(7), qty80 n(7), ;
  loc81 c(7), qty81 n(7), loc82 c(7), qty82 n(7), loc83 c(7), qty83 n(7), loc84 c(7), qty84 n(7), loc85 c(7), qty85 n(7), ;
  loc86 c(7), qty86 n(7), loc87 c(7), qty87 n(7), loc88 c(7), qty88 n(7), loc89 c(7), qty89 n(7), loc90 c(7), qty90 n(7), ;
  loc91 c(7), qty91 n(7), loc92 c(7), qty92 n(7), loc93 c(7), qty93 n(7), loc94 c(7), qty94 n(7), loc95 c(7), qty95 n(7), ;
  loc96 c(7), qty96 n(7), loc97 c(7), qty97 n(7), loc98 c(7), qty98 n(7), loc99 c(7), qty99 n(7), loc100 c(7), qty100 n(7))

*

select ;
  padr(acctname(phys.accountid),30) as acctname, ;
  phys.accountid, ;
  style, ;
  color, ;
  id, ;
  totqty, ;
  whseloc,  ;
  .f. as keep ;
  from phys ;
  where units ;
  and totqty>0 ;
  and !inlist(whseloc,"XRACK","CYCLE","CUT","RACK","FLOWU","???U") ;
  order by 1,3,4,5,6 ;
  into cursor xrpt readwrite

select acctname, style, color, id from xrpt group by 1,2,3,4 having count(1)>1 into cursor xtemp

scan 
  replace keep with .t. for acctname=xtemp.acctname and style=xtemp.style and color=xtemp.color and id=xtemp.id in xrpt
endscan

select xrpt
delete for !keep
index on acctname+style+color+id+str(999999-totqty,6)+whseloc tag zorder

xstyle="xyz"
xcolor="xyz"
xid="xyz"
xacctname="xyz"

select xrpt
scan 
  if xacctname#acctname or xstyle#style or xcolor#color or xid#id
    if xstyle#"xyz"
      insert into xx from memvar
    endif
    select xx
    scatter memvar blank
    select xrpt
    scatter memvar 
    xstyle=style
    xcolor=color
    xid=id
    xacctname=acctname
    i=0
  endif
  
  i=i+1
  ii=transform(i)
  m.numlocs=m.numlocs+1
  m.loc&ii=xrpt.whseloc
  m.qty&ii=xrpt.totqty
endscan

insert into xx from memvar

select xx
index on str(999-numlocs,3)+acctname+style+color+id+whseloc tag zorder
set step on 
copy to s:\marcjacobsdata\reports\mjmultloc_pi.xls xls

*

use in account
use in phys

schedupdate()

_screen.Caption=gscreencaption
on error
