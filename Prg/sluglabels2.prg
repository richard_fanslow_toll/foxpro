runack("SLUGLABELS")

_screen.WindowState = 2 &&maximized

SET SAFETY OFF
CLEAR ALL
RELEASE ALL
PUBLIC nQtyAll,nQtyE1,nQtyE2,nQtyE3,nQtyE4,nQtyE5,nQtyF1,nQtyF2,nQtyF3,nQtyF4,nQtyF5,cTypeOfRun,nLabelCode
PUBLIC lE1,lE2,lE3,lE4,lE5,lF1,lF2,lF3,lF4,lF5,cAlpha,cPrtName,lTesting

STORE 0 TO nQtyAll,nQtyE1,nQtyE2,nQtyE3,nQtyE4,nQtyE5,nQtyF1,nQtyF2,nQtyF3,nQtyF4,nQtyF5
STORE .F. TO lE1,lE2,lE3,lE4,lE5,lF1,lF2,lF3,lF4,lF5
STORE "" TO cAlpha
STORE "ALL" TO cTypeOfRun
STORE "\\DC3\CA91" TO cPrtName
SET TableValidate TO 0
lTesting = .F.
cDirUsed = "F:\SlugLabels\"


IF !USED("slugstruc")
	SELECT 0
	USE (cDirUsed+"slugstruc") ALIAS slugstruc EXCLUSIVE
ENDIF
COPY STRUCTURE TO (cDirUsed+"sluglabels")
USE IN slugstruc

SELECT 0
USE (cDirUsed+"sluglabels") ALIAS sluglabels EXCLUSIVE
ZAP
USE (cDirUsed+"sluglabels") ALIAS sluglabels SHARED
SELECT sluglabels

DO FORM (cDirUsed+"sluglabels2")
READ EVENTS
IF lTesting
*	SET STEP ON
ENDIF

IF EMPTY(cPrtName)
	CLOSE DATABASES ALL
	RETURN
ENDIF

DO CASE
	CASE cTypeOfRun = "MANUAL"
		SELECT 0
		USE (cDirUsed+"slugmanual") ALIAS slugmanual EXCLUSIVE
		ZAP
		USE (cDirUsed+"slugmanual") ALIAS slugmanual SHARED
		SELECT slugmanual
		numstr = "nQtyAll"
		IF lTesting AND &numstr > 3
			&numstr = 3
		ENDIF
		FOR CNT = 1 TO &numstr
			INSERT INTO slugmanual (labelcode) VALUES (nLabelCode)
		ENDFOR
	CASE cTypeOfRun = "ALL"
		FOR labtype = 1 TO 2
			IF labtype = 1
				cAlpha = "E"
				cLongname = "END/FINAL"
			ELSE
				cAlpha = "F"
				cLongname = "FRONT/COMIENZO"
			ENDIF

			FOR W = 1 TO 5
				W1 = ALLTRIM(STR(W))
				numstr = "nQtyAll"
				IF lTesting AND &numstr > 5
					&numstr = 5
				ENDIF
				FOR CNT = 1 TO &numstr
					LABEL1 = TRIM(cAlpha)+W1
					INSERT INTO sluglabels (labelnum,linenum,longname) VALUES (LABEL1,W1,cLongname)
				ENDFOR
			ENDFOR
		ENDFOR

	CASE cTypeOfRun = "E"
		cAlpha = "E"
		cLongname = "END/FINAL"
		FOR W = 1 TO 5
			W1 = ALLTRIM(STR(W))
			numstr = "nQtyAll"
			IF lTesting AND &numstr > 5
				&numstr = 5
			ENDIF
			FOR CNT = 1 TO &numstr
				LABEL1 = TRIM(cAlpha)+W1
				INSERT INTO sluglabels (labelnum,linenum,longname) VALUES (LABEL1,W1,cLongname)
			ENDFOR
		ENDFOR

	CASE cTypeOfRun = "F"
		cAlpha = "F"
		cLongname = "FRONT/COMIENZO"
		FOR W = 1 TO 5
			W1 = ALLTRIM(STR(W))
			numstr = "nQtyAll"
			IF lTesting AND &numstr > 5
				&numstr = 5
			ENDIF
			FOR CNT = 1 TO &numstr
				LABEL1 = TRIM(cAlpha)+W1
				INSERT INTO sluglabels (labelnum,linenum,longname) VALUES (LABEL1,W1,cLongname)
			ENDFOR
		ENDFOR

	CASE cTypeOfRun = "SEL"
		FOR alphatype = 1 TO 2
			IF alphatype = 1
				cAlpha = "E"
				cLongname = "END/FINAL"
			ELSE
				cAlpha = "F"
				cLongname = "FRONT/COMIENZO"
			ENDIF

			FOR W = 1 TO 5
				W1 = ALLTRIM(STR(W))
				chkstr = "l"+cAlpha+W1
				numstr = "nQty"+cAlpha+W1
				IF lTesting AND &numstr > 5
					&numstr = 5
				ENDIF

				IF &chkstr
					FOR CNT = 1 TO &numstr
						LABEL1 = TRIM(cAlpha)+W1
						INSERT INTO sluglabels (labelnum,linenum,longname) VALUES (LABEL1,W1,cLongname)
					ENDFOR
				ENDIF

			ENDFOR
		ENDFOR
	OTHERWISE
		MESSAGEBOX("Invalid Type of Run Code...Aborting",16,"No Valid Type Code")
		CLOSE ALL
		RETURN
ENDCASE

IF lTESTING
*SET STEP ON 
ENDIF

cFileNum = SYS(3)
IF cTypeOfRun = "MANUAL"
	COPY TO (cDirUsed+"slugmanual"+cFilenum)
	USE IN slugmanual
	USE (cDirUsed+"slugmanual"+cFilenum)
	IF lTesting
*		BROWSE
	ENDIF
ELSE
	COPY TO (cDirUsed+"sluglabel"+cFilenum)
	USE IN sluglabels
	USE (cDirUsed+"sluglabel"+cFilenum)
ENDIF

IF lTesting
*BROWSE
ENDIF

CLOSE TABLES ALL

IF lTesting
*RETURN
ENDIF

*!* SYSPOLLER DATA SETUP

nCount = OCCURS("\",cPrtName)
IF lTesting
	gUserID = "JBIANCHI"
ELSE
	gUserID = "SLUGS"
ENDIF

IF cTypeOfRun = "MANUAL"
	cLabelType = "SLUGMANUAL"
	labelfile = (cDirUsed+"slugmanual"+cFilenum)
ELSE
	cLabelType= "SLUGLABELS"
	labelfile = (cDirUsed+"sluglabel"+cFilenum)
ENDIF

lnAcctNum = 0
gOffice = ""

USE F:\SYSPOLLER\prtpolldata IN 0 ORDER TAG JOBNAME ALIAS prtpolldata SHARED
SELECT prtpolldata

IF SEEK(cLabelType)
	IF lTesting
		REPLACE prt_name WITH "\\DC3\MIS2_L1"
	ELSE
		REPLACE prt_name WITH TRIM(cPrtName)
	ENDIF

	IF lTesting
*	brow
	ENDIF

	USE IN prtpolldata
ELSE
	MESSAGEBOX("JOBNAME "+cLabelType+" NOT FOUND...ABORTING",16,"JOBNAME MISSING")
	USE IN syspoller
	RETURN
ENDIF

IF !USED("sysjobs")
	USE F:\syspoller\sysjobs IN 0 ALIAS sysjobs SHARED
ENDIF

SELECT sysjobs
INSERT INTO sysjobs (JOBNAME,USERID,parameter1,acct_num,office,useprinter) VALUES (cLabelType,gUserID,labelfile,lnAcctNum,gOffice,cPrtName)
USE IN sysjobs
RETURN
