*!* MARC JACOBS RPro RCV Voucher CREATOR
* This creates a RVC Voucher for a shipnment going from MJ Wholesale--> to a MJ Retail Store
*
* When this takes place the following happens:
*  1) a 945 is created out of MJ Wholesale to tell Blue Cherry that goods have been shipped.
*  2) This 945 will trigger a REtail Pro RCV Voucher to tell RPro that the Store#29 has received the goods.
*  3) Then an Outslip will be triggered to transfer the goods from Store#29 to the Destination Store
*
*  Note: Store#29 is the FMI warehouse, this is a virtual store in Retail Pro
*

parameters cbol,coffice

if !used("edi_trigger")
	use f:\3pl\data\edi_trigger in 0
endif

if !used("mjfiles")
	use f:\wh\mjfiles in 0
endif

nacctnum = 6304
*cOffice= "J"



closedata()
*DO m:\dev\prg\_setvars

public lxfer944,cwhse,cfilename,cfilename2,tsendto,tcc,tsendtoerr,tccerr,liserror,ljoemail
public lemail,tfrom

set century on
set date ymd

set step on

tfrom = "TGF Warehouse Operations <fmi-warehouse-ops@fmiint.com>"

ltesting = .f. && If TRUE, disables certain functions
lusepl = .f.
liserror = .f.

if coffice='J'
	cofficeused="J"
endif

if coffice='L'
	cofficeused="L"
endif

goffice=cofficeused

ccontainer = ""

xreturn = "XXX"
do m:\dev\prg\wf_alt with coffice,nacctnum
if xreturn = "XXX"
	wait window "No Bldg. # Provided...closing" timeout 3
*  Do ediupdate With .T.,"NO BLDG #"
	return
endif

cusefolder = upper(xreturn)
use f:\wh\upcmast in 0 alias upcmast

ltestinput = .f.
lbarcheck = .f.

select 0
use f:\3pl\data\mailmaster alias mm shared
locate for mm.accountid = nacctnum ;
and mm.office = coffice ;
and mm.edi_type = "944"
store trim(mm.acctname) to ccustname
store trim(mm.basepath) to lcoutpath
store trim(mm.holdpath) to lcholdpath
store trim(mm.archpath) to lcarchivepath
lusealt = mm.use_alt
tsendto = iif(lusealt,mm.sendtoalt,mm.sendto)
tcc = iif(lusealt,mm.ccalt,mm.cc)
locate
locate for mm.accountid = 9999 and mm.office = "X"
lusealt = mm.use_alt
tsendtoerr = iif(lusealt,mm.sendtoalt,mm.sendto)
tccerr = iif(lusealt,mm.ccalt,mm.cc)
use in mm

dimension thisarray(1)
if coffice='J'
	cwhse = "CARTERET"
endif

if coffice='L'
	cwhse = "MIRA LOMA"
endif

*!* SET CUSTOMER CONSTANTS
ccustname = "MARC JACOBS"  && Customer Identifier (for folders, etc.)
cmailname = "MARC JACOBS"  && Proper name for eMail, etc.
czladdress = "101 SPRING STREET"
czlcity = "NEW YORK"
czlstate = "NY"
czlzip = "10002"

ccustprefix = "MJR" && Customer Prefix (for output file names)

dt1 = ttoc(datetime(),1)
dt2 = datetime()
dtmail = ttoc(datetime())

*!* SET OTHER CONSTANTS
nctnnumber = 1
cstring = ""
cdate = dtos(date())
ctruncdate = right(cdate,6)
ctime = left(time(date()),2)+substr(time(date()),4,2)
cfiledate = cdate+ctime
if coffice="J"
	corig = "J"
endif
if coffice="L"
	corig = "L"
endif
cdate = dtos(date())
ctruncdate = right(cdate,6)
ctime = left(time(date()),2)+substr(time(date()),4,2)
cqtyrec = "1"
cctypeused = ""
nqtyexpected = 0

xsqlexec("select outshipid, ship_ref, wo_num, cnee_ref from outship where mod='"+goffice+"' and bol_no='"+cbol+"' and accountid=6303","pts",,"wh")

if type("cPT")="C"
	select pts
	set filter to ship_ref = cpt
endif

select pts
ptctr = 0
goto top

set step on

scan

** first check to see that on this pick ticket there are more than just personnal items
** if a pic ticket has only personnal items skip the entire pick ticket
** otherwise just process the non=personnal items, personnal items have a "ZZ" prefix

	xsqlexec("select * from outdet where outdet.outshipid="+transform(pts.outshipid)+" and units=1",,,"wh")
	select * from outdet where !substr(style,1,2) ="ZZ" into cursor pitems

	if reccount("pitems") >= 1

	else
* Wait Window At 10,10 "skipping PT "+pts.ship_ref timeout 1
		loop
	endif

	if !inlist(substr(pts.cnee_ref,1,4),"INFA","INAF") && uncommented this on 02/24/2014, vouchers for non-INFA PTs were being sent.  PG
		ptctr = ptctr+1
		loop
	endif

	ptctr = ptctr+1

** new slip/voucher number

	select mjfiles
	=seek(pts.ship_ref,"mjfiles","pickticket")
	if found("mjfiles")
		lcmodifiedslipnumber= '1'+mjfiles.voucher
		lcmodifiedslipsid = "6304"+mjfiles.voucher
	else
		lcmodifiedslipnumber= alltrim(transform(genpk("RPRONUM","whall",,,.t.,,.t.)))
		lcmodifiedslipsid  = '6304'+alltrim(lcmodifiedslipnumber)
	endif


*!*	  If Len(Alltrim(cBOL))= 17  && OK, we have a regular BOL Number
*!*	    lcModifiedSlipSID    = Substr(Alltrim(cBOL),7,17)  && drop off the leading digits

*!*	    lcModifiedSlipNumber = Right(Alltrim(cBOL),6)  && take 9 digits but then look for any leading zero
*!*	    If Substr(lcModifiedSlipNumber,1,1) ="0"
*!*	      lcModifiedSlipNumber = Right(Alltrim(cBOL),5)
*!*	    Endif

*!*	    If Substr(lcModifiedSlipNumber,1,1) ="0"
*!*	      lcModifiedSlipNumber = Right(Alltrim(cBOL),4)
*!*	    Endif
*!*	  Endif


*!*	  If Substr(Alltrim(cBOL),1,2)= "1Z"  && OK, we have a UPS Tracking#
*!*	    lcModifiedSlipSID    = Substr(Alltrim(cBOL),9,17)  && UPS drop off the 4 leading "1ZAX"
*!*	    lcModifiedSlipNumber = Right(Alltrim(cBOL),7)
*!*	    If Substr(lcModifiedSlipNumber,1,1) ="0"
*!*	      lcModifiedSlipNumber = Right(Alltrim(cBOL),6)
*!*	    Endif
*!*	    If Substr(lcModifiedSlipNumber,1,1) ="0"
*!*	      lcModifiedSlipNumber = Right(Alltrim(cBOL),5)
*!*	    Endif
*!*	    If Substr(lcModifiedSlipNumber,1,1) ="0"
*!*	      lcModifiedSlipNumber = Right(Alltrim(cBOL),4)
*!*	    Endif
*!*	    If Substr(lcModifiedSlipNumber,1,1) ="0"
*!*	      lcModifiedSlipNumber = Right(Alltrim(cBOL),3)
*!*	    Endif
*!*	    If Substr(lcModifiedSlipNumber,1,1) ="0"
*!*	      lcModifiedSlipNumber = Right(Alltrim(cBOL),2)
*!*	    Endif
*!*	  Endif

*!*	  If Inlist(Substr(Alltrim(cBOL),1,3),"518","207","572")
*!*	    lcModifiedSlipSID    = Alltrim(cBOL)  && FEDEX, take the entire number
*!*	    lcModifiedSlipNumber = Right(Alltrim(cBOL),7)
*!*	    If Substr(lcModifiedSlipNumber,1,1) ="0"
*!*	      lcModifiedSlipNumber = Right(Alltrim(cBOL),6)
*!*	    Endif
*!*	    If Substr(lcModifiedSlipNumber,1,1) ="0"
*!*	      lcModifiedSlipNumber = Right(Alltrim(cBOL),5)
*!*	    Endif
*!*	  Endif

*!*	  If Type("cSuffix")="C"
*!*	    lcModifiedSlipNumber = Alltrim(lcModifiedSlipNumber)+Alltrim(cSuffix)
*!*	    lcModifiedSlipSID    = Alltrim(lcModifiedSlipSID)   +Alltrim(cSuffix)   && manual suffix for single off voucher creatation
*!*	  Else
*!*	    lcModifiedSlipNumber = Alltrim(lcModifiedSlipNumber)+"0"+Alltrim(Transform(ptctr))
*!*	    lcModifiedSlipSID    = Alltrim(lcModifiedSlipSID)   +"0"+Alltrim(Transform(ptctr))   && add a "0" for SID uniqueness, the Outslip adds a "1"
*!*	  Endif

*!*	  If Substr(lcModifiedSlipNumber,1,1)="0"
*!*	    lcModifiedSlipNumber = "9"+lcModifiedSlipNumber
*!*	    lcMessage = "Slip Number Issue at "+lcModifiedSlipNumber
*!*	    Insert Into F:\WH\MJ_issues (Status,Type,voucher,pickticket,bol,When,Message) Values ("OPEN","UNK",lcModifiedSlipNumber,"UNK",cBOL,Datetime(),lcMessage)
*!*	*    NormalExit = .F.
*!*	*    Exit
*!*	  Endif

*!*	&& OK, here we check to see that the voucher# is no longer than 9 digits, this happens when there are more than 100 PTs on a BOL

*!*	  If Len(Alltrim(lcModifiedSlipNumber)) >=10
*!*	    lcModifiedSlipNumber = Right(Alltrim(lcModifiedSlipNumber),9)
*!*	  Endif

*!*	&& OK, here we check to see that the slip SID# does not begin with 1 "0", if so add a "9" prefix
*!*	  If Substr(lcModifiedSlipSID,1,1) ="0"
*!*	    lcModifiedSlipSID = "9"+Alltrim(lcModifiedSlipSID)
*!*	  Endif



	if ltesting
		cfilename = ccustprefix+ttoc(datetime(),1)+"voucher.xml"
		cfilenameout = ("F:\FTPUSERS\MJ_RETAIL_test\WMS_OUTBOUND\VOUCHERS\REGULAR\"+cfilename)
		carchivefile = ("F:\FTPUSERS\MJ_RETAIL_test\WMS_OUTBOUND\VOUCHERS\REGULAR\ARCHIVE\"+cfilename)
		cfilenamehold = ("F:\FTPUSERS\MJ_RETAIL_test\WMS_OUTBOUND\VOUCHERS\REGULAR\STAGING\"+cfilename)
		lnhandle2=fcreate(cfilenamehold)
	else
		cfilename = ccustprefix+ttoc(datetime(),1)+"voucher.xml"
		cfilenameout = ("F:\FTPUSERS\MJ_RETAIL\WMS_OUTBOUND\VOUCHERS\REGULAR\"+cfilename)
		carchivefile = ("F:\FTPUSERS\MJ_RETAIL\WMS_OUTBOUND\VOUCHERS\REGULAR\ARCHIVE\"+cfilename)
		cfilenamehold = ("F:\FTPUSERS\MJ_RETAIL\WMS_OUTBOUND\VOUCHERS\REGULAR\STAGING\"+cfilename)
		lnhandle2=fcreate(cfilenamehold)
	endif

	if lnhandle2 <=0
		messagebox("cant create the outbound file at "+cfilenamehold,16,"Voucher Creator")
		return
	endif

	numwrite = fputs(lnhandle2,[<?xml version="1.0" encoding="utf-8"?>])
	numwrite = fputs(lnhandle2,"<DOCUMENT>")
	numwrite = fputs(lnhandle2,"<VOUCHERS>")

* here are all the XML string templates
	if coffice ="J"
		lcorigheader = filetostr("F:\3PL\data\MJRCVVoucherHDR.txt")
	endif

	if coffice ="L"
		lcorigheader = filetostr("F:\3PL\data\MJRCVVoucherHDR_L.txt")
	endif

	lcorigdetail = filetostr("F:\3PL\data\mjrcvvoucherdet.txt")
	lcorigfooter = filetostr("F:\3PL\data\mjrcvvoucherfoot.txt")

	ttrigtime= datetime()
	lcshipdate=strtran(ttoc(ttrigtime),"/","-")
	lcshipdate=strtran(lcshipdate," ","T")
	lcshipdate=strtran(lcshipdate,"TPM","")
	lcshipdate=alltrim(strtran(lcshipdate,"TAM",""))

	lcheader = lcorigheader
	lcheader = strtran(lcorigheader,"VOUCHERSID",lcmodifiedslipsid)
	lcheader = strtran(lcheader,"CREATEDATE",lcshipdate)
	lcheader = strtran(lcheader,"VOUCHERNUMBER",lcmodifiedslipnumber)
	lcheader = strtran(lcheader,"VOUTYPE","0")
	lcheader = strtran(lcheader,"VENDORCODE","MJI")
	numwrite = fputs(lnhandle2,lcheader)

	xsqlexec("select * from outship where mod='"+goffice+"' and bol_no='"+cbol+"'",,,"wh")

	ttrigtime= datetime()
	lcshipdate=strtran(ttoc(ttrigtime),"/","-")
	lcshipdate=strtran(lcshipdate," ","T")
	lcshipdate=strtran(lcshipdate,"TPM","")
	lcshipdate=alltrim(strtran(lcshipdate,"TAM",""))

	numwrite = fputs(lnhandle2,"<VOU_COMMENTS>")
	bol_comment= [BOL# ]+cbol
	wo_comment = [TOLL WO# ]+alltrim(transform(pts.wo_num))
	pt_comment = [Wholesale PT# ]+alltrim(transform(pts.ship_ref))

	numwrite = fputs(lnhandle2,[<VOU_COMMENT comment_no="1" comments="]+bol_comment+[ WtoR]+["/>])
	numwrite = fputs(lnhandle2,[<VOU_COMMENT comment_no="2" comments="]+wo_comment+["/>])
	numwrite = fputs(lnhandle2,[<VOU_COMMENT comment_no="3" comments="]+pt_comment+["/>])

	numwrite = fputs(lnhandle2,"</VOU_COMMENTS>")
	numwrite = fputs(lnhandle2,"<VOU_ITEMS>")

	normalexit = .t.

*  ASSERT .F. MESSAGE "At Verification scan...>>debug<<"
	nlinenum = 1
*Scan For outship.bol_no = cBOL And outship.qty>0
	alength = alines(apt,outship.shipins,.t.,chr(13))
	cship_ref = alltrim(outship.ship_ref)
	if "OV"$cship_ref
		loop
	endif

	insert into f:\wh\mjfiles (type,voucher,pickticket,bol,filename,when) values ("WtoR",lcmodifiedslipnumber,pts.ship_ref,cbol,cfilename,datetime())

	lcdetail = lcorigdetail


	xsqlexec("select * from outdet where outshipid="+transform(pts.outshipid)+" and units=1",,,"wh")

	select outdet
	scan for outdet.outshipid = pts.outshipid  and units and !substr(style,1,2) ="ZZ" && used to be outship.outshipid
		clinenum = alltrim(transform(nlinenum))
		select upcmast

		locate for upcmast.upc = outdet.upc and accountid = 6303

		if !found()
			tsubject = "Marc Jacobs WtoR Voucher Creation Error at "+ttoc(datetime())+" for BOL: "+cbol
			tattach  = ""
			tsendto  = "pgaidis@fmiint.com,tmarg@fmiint.com"
			tcc = ""
			tmessage = "Cant find Style: "+alltrim(outdet.style)+" - "+alltrim(outdet.color)+" - "+alltrim(outdet.id)+" - "+alltrim(outdet.upc)+" in the stylemaster"
			tfrom    ="Toll EDI Outslip Poller Operations <fmi-transload-ops@fmiint.com>"
			do form m:\dev\frm\dartmail2 with tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
			normalexit = .f.
*   exit

*     cItemsID = "NOT IN STYLEMASTER"
			citemsid =   alltrim(outdet.style)+"-"+alltrim(outdet.color)+"-"+alltrim(outdet.id)
			cprice = "999.99"
			ccost =  "999.99"
			cupc = alltrim(outdet.upc)
			cqty = alltrim(transform(outdet.totqty))
		else
			alength = alines(aupc,upcmast.info,.t.,chr(13))
			citemsid = upcmast.sid &&Alltrim(segmentget(@aupc,"ITEMSID",alength))
			cprice = alltrim(transform(upcmast.rprice,"99999.99"))
			ccost = alltrim(transform(upcmast.price,"99999.99"))
			cupc = alltrim(outdet.upc)
			cqty = alltrim(transform(outdet.totqty))
			if len(alltrim(citemsid)) <= 2
				lcmessage = "Item SID Issue at "+cupc
				insert into f:\wh\mj_issues (status,type,voucher,pickticket,bol,when,message) values ("OPEN",bolsuffix,lcmodifiedslipnumber,outship.ship_ref,cbol,datetime(),lcmessage)
				normalexit = .f.
				exit
			endif
		endif

		lcdetail = lcorigdetail
		clinenum = alltrim(transform(nlinenum))
		lcdetail = strtran(lcdetail,"ITEMPOSITION",clinenum)
		lcdetail = strtran(lcdetail,"ITEMSID",citemsid)
		lcdetail = strtran(lcdetail,"QTY",cqty)
		lcdetail = strtran(lcdetail,"PRICE",cprice)
		lcdetail = strtran(lcdetail,"COST",ccost)
		lcdetail = strtran(lcdetail,"UPC",cupc)
		numwrite = fputs(lnhandle2,lcdetail)
		nlinenum = nlinenum+1
		select outdet
	endscan
*Endscan

* write out the XML footer elements
	numwrite = fputs(lnhandle2,"</VOU_ITEMS>")
	numwrite = fputs(lnhandle2,"</VOUCHER>")
	numwrite = fputs(lnhandle2,lcorigfooter)
	=fclose(lnhandle2)

	if normalexit = .f.
		wait "MJR Outslip Creation error file deleted:"+chr(13)+cfilename window at 45,60 timeout 2
		delete file &cfilenamehold
	else
		wait "MJR Outslip Creation process complete:"+chr(13)+cfilename window at 45,60 timeout 2
		if file(cfilenamehold)
			copy file &cfilenamehold to &cfilenameout
			copy file &cfilenamehold to &carchivefile
			erase &cfilenamehold
		endif
	endif
	select edi_trigger
	locate
	replace edi_trigger.comments with "VOUCHERFILE*"+upper(cfilename) for edi_trigger.ship_ref = cship_ref ;
	and edi_trigger.accountid = nacctnum and edi_trigger.edi_type = '945M'
endscan
closedata()
return

************************************************************************
************************************************************************
*!* END OF MAIN PROGRAM
************************************************************************
****************************
procedure cstringbreak
****************************
fputs(nfilenum,cstring)
return

****************************
procedure num_incr
****************************
select serfile
replace serfile.seqnum with serfile.seqnum + 1 in serfile
replace serfile.grpseqnum with serfile.grpseqnum + 1 in serfile
c_cntrlnum = alltrim(str(serfile.seqnum))
c_grpcntrlnum = alltrim(str(serfile.grpseqnum))
return

****************************
procedure closedata()
****************************
if used('upcmast')
	use in upcmast
endif
if used('outship')
	use in outship
endif
if used('outdet')
	use in outdet
endif

endproc

*******************************************************888
procedure joemail
****************************
if liserror
	tsubject = cmailname+" EDI ERROR (944), Inv. WO"+cwo_num
	if cfin = "NO PO DATA IN ECHO"
		select 0
		use f:\3pl\data\mailmaster alias mm
		locate for mm.edi_type = "MISC" and mm.taskname = "MODSHOEPODATA"
		lusealt= iif(mm.use_alt,.t.,.f.)
		tsendto = iif(lusealt,sendtoalt,sendto)
		tcc = iif(lusealt,ccalt,cc)
		use in mm
	endif
else
	tsubject = cmailname+" EDI FILE (944) Created, Inv. WO"+cwo_num
endif

tattach = " "
if (ctrk_wo = "0" or empty(ctrk_wo))
	cthistrk_wo = "MISSING"
else
	cthistrk_wo = ctrk_wo
endif
tmessage = "Inv. WO #: "+cwo_num+chr(10)
if empty(cerrmsg)
	tmessage = tmessage + cfin
else
	tmessage = tmessage + cerrmsg
endif
if cfin = "NO PO DATA IN ECHO"
	tmessage = tmessage + chr(13)+chr(13)+"Container/Airbill: "+ccontainer
endif

*SET STEP ON
do form m:\dev\frm\dartmail2 with tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
if !liserror
*    transitionmail()
endif
endproc


****************************
procedure transitionmail
****************************
return

tsendto = "joe.bianchi@tollgroup.com"
tcc = ""
tfrom = "TGF Warehouse Operations <fmi-warehouse-ops@fmiint.com>"
tsubject = ccustname+" 944 output ready"
tmessage = "A 944 for WO# "+cwo_num+"was created as"
tmessage = tmessage + "f:\ftpusers\"+ccustname+"\944OUT\transition\prl"+dt1+".944."
tmessage = tmessage + "Check file and drop into 944OUT folder if correctly created."

do form m:\dev\frm\dartmail2 with tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
endproc

****************************
procedure segmentget
****************************

parameter thisarray,lckey,nlength

for i = 1 to nlength
	if i > nlength
		exit
	endif
	lnend= at("*",thisarray[i])
	if lnend > 0
		lcthiskey =trim(substr(thisarray[i],1,lnend-1))
		if occurs(lckey,lcthiskey)>0
			return substr(thisarray[i],lnend+1)
			i = 1
		endif
	endif
endfor

return ""
endproc
