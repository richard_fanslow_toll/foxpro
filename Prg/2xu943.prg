
coffice='Y'
acctnum=6665
edi_type = "PL"

*Parameters coffice,acctnum
* call this like this: DO m:\dev\prg\2xu943.prg WITH "C", 6665

*runack("WMSINBOUND")
ltesting = .F.
DO m:\dev\prg\_setvars WITH ltesting
IF VARTYPE(acctnum) = "C"
	acctnum = INT(VAL(acctnum))
ENDIF

PUBLIC goffice,tsendto,tcc,lcaccountname,lcartonacct,ldogeneric,lcreateerr,pname,nacctnum,lholdfile,ldomix,nlinestart,cmod
PUBLIC m.style,m.color,m.id,m.size,m.pack,ctransfer,ltest,lallpicks,lcwhpath,lnormalexit,lcerrormessage,newacctnum
PUBLIC cacctname,lacctname,xacctnum,lprepack,nprepacks,npickpacks,tfrom,lprocerror,thisfile,lcpath,ldoslerror
PUBLIC cslerror,lpik,lpre,cmessage,cfilename,ltestmail,lmoderr,xfile,cmyfile,lxlsx,archivefile,ldeldbf,plqty1,plqty2,lcerrormessage

STORE "" TO m.style,m.color,m.id,m.size,m.pack,lcbrokerref,cmessage,cfilename,xfile

CLOSE DATABASES ALL
SET PATH TO
SET PATH TO "M:\DEV\PRG\"
DO m:\dev\prg\_setvars WITH .T.
gmasteroffice ="Y"
gsystemmodule = "wh"

SET STATUS BAR ON
ON ESCAPE CANCEL
SET tablevalidate TO 0
SET TALK OFF
SET enginebehavior 70
SET ESCAPE ON
SET SAFETY OFF
SET MULTILOCKS ON
SET DELETED ON
SET EXCLUSIVE OFF

DO m:\dev\prg\lookups
ccustname = '2XU'
lcerrormessage=""
ltest = .F.  && This directs output to F:\WHP\WHDATA test files (default = .f.)
ltestmail = .F. &&lTest && Causes mail to go to default test recipient(s)
lholdfile = .F. && Causes error CATCH message; on completion with no errors, changed to .t. (default = .f.)
lbrowse = lholdfile  && If this is set with lHoldFile, browsing will occur at various points (default = .f.)
loverridebusy = IIF(lholdfile OR ltest,.T.,.F.) && If this is set, CHKBUSY flag in FTPSETUP will be ignored (default = .f.)

*loverridebusy = .T.

lallpicks = .F.  && If this is set, all inbounding will be done as units...should not be used (default = .f.)
ldoslerror = .F.  && This flag will be set if there is missing info in the Courtaulds inbound sheet
lxlsx = .F.
ldeldbf = .T.

IF TYPE("acctnum") = "C"
	acctnum = VAL(acctnum)
ENDIF

STORE acctnum TO nacctnum

tfrom = "TGF WMS Inbound EDI System Operations <inbound-ops@fmiint.com>"
ldogeneric = .F.
lcreateerr = .F.
lmainmail = .T.
tsendto = ""
tcc = ""
cslerror = ""
lcartonacct = .F.
SELECT 0
USE F:\edirouting\ftpsetup
*  ASSERT .f. MESSAGE "In FTPSETUP list"

m.office = coffice
DO CASE
	CASE coffice = "Y" AND acctnum = 6665
		ctransfer = "PL-2XU-CA"
		cmod = "Y"
	OTHERWISE
		ctransfer = "XXX"  && For Modern Shoe, etc.
ENDCASE

ASSERT .F. MESSAGE "At ftpsetup stage"
LOCATE FOR UPPER(ftpsetup.transfer) = UPPER(ctransfer)
IF FOUND()
	IF !lholdfile AND !lbrowse AND !loverridebusy
		IF ftpsetup.chkbusy=.T.
			SET STEP ON
			WAIT WINDOW AT 10,10  "This transfer is busy...will try again later."+CHR(13)+ctransfer TIMEOUT 1
			lnormalexit = .T.
			THROW
		ELSE
			REPLACE chkbusy WITH .T.,trig_time WITH DATETIME()  IN ftpsetup NEXT 1
		ENDIF
	ENDIF
ENDIF

IF ltest
	WAIT WINDOW "This is a test inbound upload into WHP tables" TIMEOUT 2
ENDIF

IF USED('mm')
	USE IN mm
ENDIF
SELECT 0
USE F:\3pl\DATA\mailmaster ALIAS mm
LOCATE FOR (mm.accountid = acctnum) AND (mm.office = coffice) AND edi_type = "PL"

IF FOUND()
	STORE TRIM(mm.acctname) TO lcaccountname
	STORE TRIM(mm.basepath) TO lcpath
	STORE TRIM(mm.archpath) TO lcarchivepath
	tsendto = ALLTRIM(IIF(mm.use_alt,mm.sendtoalt,mm.sendto))
	tcc = ALLTRIM(IIF(mm.use_alt,mm.ccalt,mm.cc))
	STORE TRIM(mm.fmask)    TO filemask
	STORE TRIM(mm.progname) TO pname
	STORE TRIM(mm.scaption) TO _SCREEN.CAPTION
	STORE mm.ctnacct    TO lcartonacct
	STORE mm.dogeneric TO ldogeneric
	LOCATE FOR edi_type = "MISC" AND taskname = "GENERAL"
	tsendtoerr = ALLTRIM(IIF(mm.use_alt,mm.sendtoalt,mm.sendto))
	tccerr = ALLTRIM(IIF(mm.use_alt,mm.ccalt,mm.cc))
	tsendtotest = ALLTRIM(IIF(mm.use_alt,mm.sendtoalt,mm.sendto))
	tcctest = ALLTRIM(IIF(mm.use_alt,mm.ccalt,mm.cc))
	USE IN mm
ELSE
	lnormalexit=.T.
	WAIT WINDOW AT 10,10  "No parameters set for this acct# "+ALLTRIM(STR(acctnum))+"  ---> Office "+coffice TIMEOUT 2
	THROW
ENDIF

**********************************************************************************************
CD &lcpath

IF ltest OR lholdfile
	tsendto = tsendtotest
	tcc = tcctest
ENDIF

WAIT WINDOW AT 10,10  "Now setting up a WMS Inbound for "+lcaccountname+"  ---> Office "+coffice NOWAIT

SELECT 0

xsqlexec("select * from account where inactive=0","account",,"qq")
INDEX ON accountid TAG accountid

SELECT account

IF SEEK(acctnum,"account","accountid")
	cacctname= account.acctname
ELSE
	lnormalexit=.T.
	WAIT WINDOW AT 10,10 "No account name on file.........." TIMEOUT 2
	CLOSE DATABASES ALL
	THROW
ENDIF

xreturn = "X"
DO m:\dev\prg\wf_alt WITH coffice,acctnum
lcwhpath = UPPER(xreturn)
IF xreturn = "X"
	WAIT WINDOW "No such account for this office" TIMEOUT 2
	THROW
ENDIF
***********************************************start 2xu
IF INLIST(acctnum,6665)
	lcwhpath = "F:\why\whdata\"
ENDIF

************************added TM
IF !ltesting
	lcpath = "F:\FTPUSERS\2XU\943IN\"
	lcarchivepath = 'F:\FTPUSERS\2XU\943IN\archive\'
*  tsendto="todd.margolin@tollgroup.com"
*  tcc="PGAIDIS@FMIINT.COM"
ELSE
	lcpath = "F:\FTPUSERS\2XU\943IN\"
	lcarchivepath = 'F:\FTPUSERS\2XU\943IN\archive\'
	tsendto="todd.margolin@tollgroup.com"
*  tcc="PGAIDIS@FMIINT.COM"
ENDIF
************************************************************
CD &lcpath
len1 = ADIR(tarray,"*.xlsx")
IF len1 = 0
	WAIT WINDOW "No files found...exiting" TIMEOUT 2
	CLOSE DATA ALL
	schedupdate()
	_SCREEN.CAPTION="2XU Packing List Upload.............."
	ON ERROR
	RETURN
ENDIF

if usesqlwo()
else
	USE F:\wo\wodata\wolog IN 0 ALIAS wolog
endif

goffice='Y'
useca("inwolog","wh",,,"mod='"+goffice+"' and  accountid ="+TRANSFORM(acctnum) )
useca("pl","wh")
xsqlexec("select * from pl where .f.","xpl",,"wh")

FOR thisfile = 1 TO len1
***  cfilename = alltrim(ary1[thisfile,1])
	cfilename = ALLTRIM(tarray[thisfile,1])
	xfile = lcpath+cfilename
	llcontinue = .T.
	TRY
		xfile = lcpath+ALLTRIM(tarray[thisfile,1])
		IF INLIST(UPPER(JUSTEXT(xfile)),"TMP","DBF")
			LOOP
		ENDIF

		attrfile = '"'+lcpath+ALLTRIM(tarray[thisfile,1])+'"'
		cfilename = JUSTFNAME(xfile)
		!ATTRIB -r &attrfile  && Removes read-only flag from file to allow deletion
		WAIT WINDOW "Filename = "+xfile TIMEOUT 1
		CREATE CURSOR tempmain (a c(50), ;
			b c(30), ;
			c c(30), ;
			d c(50), ;
			e c(30), ;
			F c(30), ;
			g c(30), ;
			h c(30), ;
			i c(30), ;
			j c(30), ;
			k c(30))

		xfilecsv = JUSTSTEM(JUSTFNAME(xfile))+".csv"
		DO m:\dev\prg\excel_to_csv WITH cfilename
		SELECT tempmain
		APPEND FROM [&xfilecsv] TYPE CSV
		DO  doimport
		archivefile=lcarchivepath+cfilename
		COPY FILE [&xfile] TO [&archivefile]
		xfile= LOWER(xfile)
		DELETE FILE  [&xfile]
		xfile = STRTRAN(xfile,"xlsx","csv")
		DELETE FILE  [&xfile]
		goodmail()

	CATCH TO oerr
		ASSERT .F. MESSAGE "In internal Catch Section (Not main loop)...debug"
		IF lnormalexit = .F.
			tmessage = "Account: "+ALLTRIM(STR(acctnum))+" (Internal loop)"
			tmessage = tmessage+CHR(13)+"TRY/CATCH Exeception Message:"+CHR(13)+;
				[  Error: ] + STR(oerr.ERRORNO) +CHR(13)+;
				[  LineNo: ] + STR(oerr.LINENO) +CHR(13)+;
				[  Message: ] + oerr.MESSAGE +CHR(13)+;
				[  Procedure: ] + oerr.PROCEDURE +CHR(13)+;
				[  Details: ] + oerr.DETAILS +CHR(13)+;
				[  StackLevel: ] + STR(oerr.STACKLEVEL) +CHR(13)+;
				[  LineContents: ] + LEFT(oerr.LINECONTENTS,50)+CHR(13)+;
				[  UserValue: ] + oerr.USERVALUE+CHR(13)

			tsubject = "Error Loading Inbound File "+xfile
			tsendto = tsendtoerr
			DO CASE
				CASE INLIST(coffice,"C","Y") AND INLIST(nacctnum,4610,4694,6665)  && Nanjing
*            CASE cOffice = "Y" AND INLIST(nAcctNum,4610,4694)  && Nanjing
					tcc = ""
				CASE coffice = "C" AND nacctnum = 1285  && AgeGroup
					tcc = "yvalencia@fmiint.com"
				OTHERWISE
					tcc = tccerr
			ENDCASE
			tattach = ""
			DO FORM m:\dev\frm\dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"

			MESSAGEBOX(tmessage,0,"EDI Error",5000)

			WAIT CLEAR
			llcontinue = .F.
		ELSE
			WAIT WINDOW "Normal Exit..." TIMEOUT 2
		ENDIF
		SET STATUS BAR ON
	ENDTRY
NEXT

SELECT ftpsetup
REPLACE ftpsetup.chkbusy WITH .F. IN ftpsetup NEXT 1


**************************************************************try
PROCEDURE doimport


	IF EMPTY(goffice) AND ltest OR lholdfile
		goffice = "1"
	ELSE
		goffice = coffice
	ENDIF
	STORE "" TO m.style,m.color,m.id,m.size,m.pack

	SELECT * FROM inwolog WHERE .F. INTO CURSOR xinwolog READWRITE
	SELECT tempmain

	DELETE FOR EMPTY(a)
	REPLACE a WITH UPPER(a) FOR !EMPTY(a)
	REPLACE b WITH UPPER(b) FOR !EMPTY(b)
	REPLACE c WITH UPPER(c) FOR !EMPTY(c)
	REPLACE d WITH UPPER(d) FOR !EMPTY(d)
	REPLACE e WITH UPPER(e) FOR !EMPTY(e)
	REPLACE F WITH UPPER(F) FOR !EMPTY(F)
	REPLACE g WITH UPPER(g) FOR !EMPTY(g)

	COUNT TO RECNO


	SELECT tempmain
	GO TOP
*****************************************************************start loop for multiple files here
*Set Step On

	SELECT * FROM inwolog WHERE .F. INTO CURSOR xinwolog READWRITE
	recctr = 0

	recctr = recctr +1
	WAIT WINDOW AT 10,10 "Checking Record # "+TRANSFORM(recctr) NOWAIT

	m.adddt=DATETIME()
	m.updatedt=DATETIME()
	m.addby='2XU943'
	m.updateby='2XU943'
	m.accountid=6665
	m.addproc='2XU943'
	m.updproc='2XU943'
	SELECT tempmain
	SCAN
		DO CASE
			CASE tempmain.a = 'ON DATE'
				m.wo_date=CTOD(tempmain.b)
			CASE tempmain.a = 'CONTAINER'
				m.container= tempmain.b
			CASE tempmain.a = 'ACCOUNT REF'
				m.acct_ref= tempmain.b
			CASE tempmain.a = 'BROKER REF'
				m.brokerref= tempmain.b
			CASE tempmain.a = 'VESSEL/AWB'
				m.reference= tempmain.c
			CASE tempmain.a = 'SEAL'
				m.seal= tempmain.b
			CASE tempmain.a = 'PRINT COMMENTS'
				IF !EMPTY(ALLTRIM(tempmain.b))
					m.printcomments= ALLTRIM(tempmain.b)
				ENDIF
		ENDCASE
	ENDSCAN

	INSERT INTO xinwolog FROM MEMVAR

********************************

	SELECT * FROM pl WHERE .F. INTO CURSOR xpl READWRITE

	SELECT c AS STYLE,d AS COLOR, e AS ID, b AS ctngrp, 0000 AS ctnqty, F AS upc, g AS PACK, ROUND(SUM(VAL(g)),0) AS totqty,;
		MAX(a) AS pallet FROM tempmain WHERE !EMPTY(g) AND g!='QTY' GROUP BY 1,2,3,4,5 ORDER BY ctngrp INTO CURSOR t1 READWRITE

	SELECT t1
	SCAN
		IF AT("~",ctngrp) > 0
			loval=VAL(SUBSTR(ctngrp,1,(AT("~",ctngrp))-1))
			hival=VAL(SUBSTR(ctngrp,(AT("~",ctngrp))+1))
			REPLACE ctnqty WITH (hival-loval)+1 IN t1
		ELSE
			REPLACE ctnqty WITH 1 IN t1
		ENDIF
	ENDSCAN

	recctr = 0
	poctr=0
	plid=0
	m.units = .F.

	m.adddt=DATETIME()
	m.updatedt=DATETIME()
	m.addby='2XU943'
	m.updateby='2XU943'
	m.accountid=6665
	m.acctname='2XU'
	m.addproc='2XU943'
	m.updproc='2XU943'

	SELECT *, SUM(totqty) AS linetot, COUNT(1) numrecs  FROM t1 GROUP BY ctngrp INTO CURSOR ctntypes READWRITE

	SELECT ctntypes
	SCAN

		poctr=poctr+1
		plid=plid+1
		recctr = recctr +1
		m.units = .F.
		INSERT INTO xpl FROM MEMVAR

		REPLACE STYLE WITH ctntypes.STYLE IN xpl
		REPLACE COLOR WITH ctntypes.COLOR IN xpl
		IF ctntypes.numrecs = 1
			REPLACE ID WITH ctntypes.ID IN xpl
			REPLACE PACK WITH t1.PACK IN xpl
		ELSE
			REPLACE ID WITH "ASST-"+ALLTRIM(TRANSFORM(dygenpk("PLCTR","why")))  IN xpl
		ENDIF

		REPLACE PACK WITH TRANSFORM(ctntypes.linetot) IN xpl
		xtotqty= ctntypes.linetot
		REPLACE totqty WITH ctntypes.ctnqty IN xpl
		REPLACE xpl.po WITH ALLTRIM(STR(poctr)) IN xpl
		REPLACE ECHO WITH (ctntypes.pallet) IN xpl
		REPLACE cayset WITH ctntypes.ctngrp IN xpl

		SELECT t1
		SCAN FOR ctngrp = ctntypes.ctngrp
			m.units = .T.
			INSERT INTO xpl FROM MEMVAR
			REPLACE STYLE WITH t1.STYLE IN xpl
			REPLACE COLOR WITH t1.COLOR IN xpl
			REPLACE ID WITH t1.ID IN xpl
			REPLACE PACK WITH '1' IN xpl
			REPLACE units WITH .T. IN xpl
			REPLACE cayset WITH ctntypes.ctngrp IN xpl
			IF ctntypes.ctnqty  > 1
				REPLACE xpl.totqty WITH ctntypes.ctnqty * t1.totqty IN xpl
			ELSE
				REPLACE xpl.totqty WITH t1.totqty IN xpl
			ENDIF
			REPLACE xpl.po WITH ALLTRIM(STR(poctr)) IN xpl
			REPLACE ECHO WITH (t1.pallet) IN xpl
		ENDSCAN

	ENDSCAN

*Set Step On

	SELECT xpl
	SUM(totqty) TO xunitsqty  FOR units
	SUM(totqty) TO xcartonsqty FOR !units

	nuploadcount = 0

	SELECT xinwolog
	SCAN  && Scanning xinwolog here
		m.plunitsinqty=xunitsqty
		m.plinqty=xcartonsqty
		m.quantity=xcartonsqty
		m.inwologid=dygenpk("INWOLOG","why") && was inwologid
		m.wo_num   =dygenpk("WONUM","why")  && wonum was lower case
		m.wo_date = DATE()
		nwo_num = m.wo_num
		insertinto("inwolog","wh",.T.)

		newinwologid = m.inwologid
		SET STEP ON
		SELECT xpl
		SCAN FOR xpl.inwologid = xinwolog.inwologid
			SCATTER MEMVAR MEMO
			m.addby = "TOLLPROC"
			m.adddt = DATETIME()
			m.addproc = "2XU 943 UPLOAD"
			m.inwologid = inwolog.inwologid
			m.accountid = 6665
			m.wo_num = nwo_num
			m.office = "Y"  &&gMasterOffice
			m.mod = "Y"
			insertinto("pl","wh",.T.)
		ENDSCAN
	ENDSCAN
	tu("pl")
	tu('inwolog')

*  endscan

ENDPROC
******************************
PROCEDURE closedata
******************************

	IF USED('inwolog')
		USE IN inwolog
	ENDIF

	IF USED('pl')
		USE IN pl
	ENDIF

	IF USED('upcmast')
		USE IN upcmast
	ENDIF

	IF USED('upcmastsql')
		USE IN upcmastsql
	ENDIF

	IF USED('account')
		USE IN account
	ENDIF

*IF USED('whgenpk')
*	USE IN whgenpk
*ENDIF
ENDPROC

*****************************
PROCEDURE slerrormail
******************************
	IF USED('mm')
		USE IN mm
	ENDIF
	SELECT 0
	USE F:\3pl\DATA\mailmaster ALIAS mm
	LOCATE FOR accountid = 4677 AND taskname = "EXCELERROR"
	lusealt = mm.use_alt
	tsendto = ALLTRIM(IIF(lusealt,mm.sendtoalt,mm.sendto))
	tcc = ALLTRIM(IIF(lusealt,mm.ccalt,mm.cc))
	USE IN mm
	tsubject = "Missing information in Inbound Excel sheet"
	tattach = ""
	tmessage = "The following required FIELD(s) is/are missing data:"
	tmessage = tmessage+CHR(13)+cslerror
	DO FORM m:\dev\frm\dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
ENDPROC

******************************
PROCEDURE smerrmail
******************************
&& Used for all GENERIC INBOUND accounts
	IF USED('account')
		USE IN account
	ENDIF
	SELECT 0
	xsqlexec("select * from account where inactive=0","account",,"qq")
	INDEX ON accountid TAG accountid
	SET ORDER TO
	IF SEEK(nacctnum,'account','accountid')
		cacctname = ALLTRIM(account.acctname)
	ENDIF

	IF USED('mm')
		USE IN mm
	ENDIF
	SELECT 0
	USE F:\3pl\DATA\mailmaster ALIAS mm
	IF !ltest
		IF nacctnum # 6137
			LOCATE FOR mm.accountid = nacctnum AND mm.office = goffice AND mm.edi_type = 'PL'
		ELSE
			LOCATE FOR mm.accountid = nacctnum AND mm.taskname = 'SMMAIL'
		ENDIF
		lusealt = mm.use_alt
		tsendto = ALLTRIM(IIF(lusealt,mm.sendtoalt,mm.sendto))
		tcc = ALLTRIM(IIF(lusealt,mm.ccalt,mm.cc))
	ELSE
		LOCATE FOR mm.edi_type = "MISC" AND mm.taskname = "GENERAL"
		lusealt = mm.use_alt
		tsendto = ALLTRIM(IIF(lusealt,mm.sendtoalt,mm.sendto))
		tcc = ALLTRIM(IIF(lusealt,mm.ccalt,mm.cc))
	ENDIF
	USE IN mm
	tsubject = "Style Errors in Inbound Excel sheet: "+cacctname
	tattach = ""
	tmessage = "File Name: "+IIF(!EMPTY(cfilename),cfilename,JUSTFNAME(xfile))
	tmessage = tmessage+CHR(13)+CHR(13)+"The following Style/Color/Size combination(s) is/are not in our Style Master."
	tmessage = tmessage+CHR(13)+"Please send these Style Master Updates, then RE-TRANSMIT the PL file. Thanks."+CHR(13)
	tmessage = tmessage+CHR(13)+PADR("STYLE",22)+PADR("COLOR",12)+"SIZE"
	tmessage = tmessage+CHR(13)+REPLICATE("=",38)
	SELECT sm_err
	SCAN
		cstyle = PADR(ALLTRIM(sm_err.STYLE),22)
		ccolor = PADR(ALLTRIM(sm_err.COLOR),12)
		csize = ALLTRIM(sm_err.ID)
		tmessage = tmessage+CHR(13)+cstyle+ccolor+csize
	ENDSCAN
	USE IN sm_err
	DO FORM m:\dev\frm\dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
ENDPROC

*************************
PROCEDURE goodmail
*************************
	tsubject= "TGF "+ccustname+" Packing List Upload: " +TTOC(DATETIME())
	tattach = ""
	tmessage = "Packing List uploaded for "+ccustname+CHR(13)+"From File: "+cfilename+CHR(13)
	tfrom ="TGF EDI Operations <toll-edi-ops@tollgroup.com>"
	IF ltesting
		tmessage = tmessage+CHR(13)+"*TEST DATA*"
	ENDIF
	DO FORM m:\dev\frm\dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
