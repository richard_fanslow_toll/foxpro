* SHCPMREPORT-MISC 03/28/2016
* Create a utiliy report that gives a cost per mile info for VEHICLEs, from the shop system.
* For Fran C. and Bill O'Brien; Ryan Brenan as of 3/3/2016

* Break out inside, outside and accident costs.

* Use same burdened labor rates as in the financial shop reports.

LOCAL lTestMode
lTestMode = .T.

utilsetup("SHCPMREPORT")

DECLARE INTEGER CreateMutex IN Win32API ;
	STRING @lpMutexAttributes, ;
	STRING bInitialOwner, ;
	STRING @lpName

DECLARE INTEGER GetLastError IN Win32API

* if running in EXE check to see if already running
IF _VFP.STARTMODE > 0 THEN
	lcProcessName = "SHCPMREPORT"
	CreateMutex(0,CHR(1),@lcProcessName)
	lnError = GetLastError()
	IF lnError = 183
		WAIT WINDOW TIMEOUT 5 "This program is already running..."
		IF NOT lTestMode THEN
			schedupdate()
		ENDIF
		RETURN .F.
	ENDIF
ENDIF

loSHCPMREPORT = CREATEOBJECT('SHCPMREPORT')
loSHCPMREPORT.MAIN( lTestMode )

IF NOT lTestMode THEN
	schedupdate()
ENDIF

CLOSE DATABASES ALL
RETURN

#DEFINE LOGIT 1
#DEFINE WAITIT 2
#DEFINE NOWAITIT 4
#DEFINE SENDIT 8
#DEFINE CRLF CHR(13) + CHR(10)
#DEFINE RETURN_DATA_MANDATORY .F.
#DEFINE RETURN_DATA_NOT_MANDATORY .T.
#DEFINE CSVFORMAT 6
#DEFINE WRITE_ONLY_UNBUFFERED 11
#DEFINE COMMA ","

DEFINE CLASS SHCPMREPORT AS CUSTOM

	cProcessName = 'SHCPMREPORT'

	lTestMode = .T.  && .F. FOR NORMAL OPERATION, .T. TO SUPPRESS ALL BUT DEVELOPER EMAILS AND CHANGE INPUT TABLE, ETC.

	dToday = DATE()
	*dToday = {^2015-04-19}
	
	* processing properties
	*************************************************************************************************************
	lCalcDateRange = .T.   && FALSE = process all work orders; TRUE = just for the date range entered in the code.
	lCorrectMileages = .F.
	*************************************************************************************************************


	lAutoYield = .T.

	cStartTime = TTOC(DATETIME())

	* wait window properties
	nWaitWindowTimeout = 2
	lWaitWindowIsOn = .T.

	* logfile properties
	lLoggingIsOn = .T.
	cLogFile = 'F:\UTIL\SHOPREPORTS\LOGFILES\SHCPMREPORT_log.txt'

	* INTERNAL email properties
	lSendInternalEmailIsOn = .T.
	cFrom = 'mark.bennett@tollgroup.com'
	cSendTo = 'mbennett@fmiint.com'
	cCC = ''
	cSubject = "Vehicle Cost-per-Mile Report for " + TRANSFORM(DATETIME())
	cAttach = ''
	cBodyText = ''
	cMGRSendTo = ''
	cMGRFrom = ''
	cMGRSubject = ''
	cMGRCC = ''
	cMGRAttach = ''
	cMGRBodyText = ''

	cCOMPUTERNAME = ''
	cUSERNAME = ''


	FUNCTION INIT
		IF NOT DODEFAULT()
			RETURN .F.
		ENDIF
		WITH THIS
			*SET RESOURCE OFF
			CLOSE DATA
			SET CENTURY ON
			*SET DATE AMERICAN
			*SET HOURS TO 24
			SET DECIMALS TO 3
			SET ANSI ON
			SET TALK OFF
			SET DELETED ON
			SET CONSOLE OFF
			SET EXCLUSIVE OFF
			SET SAFETY OFF
			SET EXACT OFF
			SET STATUS BAR ON
			SET SYSMENU OFF
			SET ENGINEBEHAVIOR 70
			SET MULTILOCKS ON
			_VFP.AUTOYIELD = .lAutoYield
			.cCOMPUTERNAME = GETENV("COMPUTERNAME")
			.cUSERNAME = GETENV("USERNAME")
			.lLoggingIsOn = .lLoggingIsOn AND NOT EMPTY(.cLogFile)
		ENDWITH
	ENDFUNC


	FUNCTION DESTROY
		WITH THIS
			IF .lLoggingIsOn  THEN
				SET ALTERNATE OFF
				SET ALTERNATE TO
			ENDIF
		ENDWITH
		DODEFAULT()
	ENDFUNC


	FUNCTION MAIN
		LPARAMETERS tlTestMode
		WITH THIS
			LOCAL lnRow, lcRow, lcReportPeriod, lcFiletoSaveAs, lcSpreadsheetTemplate, lnNumberOfErrors
			LOCAL oExcel, oWorkbook, lcErr, lcTitle, loError, lnRate, ldToday
			LOCAL lcDivision, lnLastRow, lnRate, lcDetailBasisFile, lnPercent, lnDifference
			LOCAL ldStartCRDate, lcDateRangeWhere

			TRY
				lnNumberOfErrors = 0

				.lTestMode = tlTestMode

				IF .lTestMode THEN
					.cLogFile = 'F:\UTIL\SHOPREPORTS\LOGFILES\SHCPMREPORT_log_TESTMODE.txt'
					.cSendTo = 'mbennett@fmiint.com'
				ENDIF

				IF .lLoggingIsOn THEN
					SET ALTERNATE TO (.cLogFile) ADDITIVE
					SET ALTERNATE ON
				ENDIF

				.cStartTime = TTOC(DATETIME())

				.TrackProgress('Vehicle Cost-per-mile process started....', LOGIT+SENDIT)
				.TrackProgress('COMPUTERNAME = ' + .cCOMPUTERNAME, LOGIT+SENDIT)
				.TrackProgress('USERNAME = ' + .cUSERNAME, LOGIT+SENDIT)
				.TrackProgress('PROJECT = SHCPMREPORT', LOGIT+SENDIT)
				IF .lTestMode THEN
					.TrackProgress('                        TEST MODE                        ', LOGIT+SENDIT)
				ENDIF

				ldToday = .dToday

				USE F:\SHOP\SHDATA\ORDHDR IN 0
				USE F:\SHOP\SHDATA\VEHMAST IN 0

				WAIT WINDOW "Selecting data..." NOWAIT
				
				IF 	.lCalcDateRange THEN
				
					*ldStartCRDate = GOMONTH(ldToday,-24)  && 2 Years back per Bill O'Brien
					ldStartCRDate = {^1998-01-01}
					
					lcDateRangeWhere = " AND CRDATE >= ldStartCRDate "
					.TrackProgress('NOTE: calculating costs for Work Orders on or after ' + TRANSFORM(ldStartCRDate), LOGIT+SENDIT)
				ELSE
					lcDateRangeWhere = ""
				ENDIF

					
				SELECT VEHNO, CRDATE, REPCAUSE, MILEAGE, TOTHOURS, TOTLCOST, TOTPCOST, OUTSCOST, 0000.00 AS RATE, MECHNAME, OVNAME, OVCITY, WORKDESC ;
					FROM ORDHDR ;
					INTO CURSOR CURORDHDRPRE ;
					WHERE UPPER(ALLTRIM(TYPE)) = "MISC" ;
					AND NOT EMPTY(VEHNO) ;
					&lcDateRangeWhere ;
					ORDER BY CRDATE ;
					READWRITE

				WAIT WINDOW "Calculating labor costs..." NOWAIT
				
				* POPULATE RATE BY CRDATE AND CALC LABOR COST FOR EACH WO REC
				SELECT CURORDHDRPRE
				SCAN
					lnRate = .GetHourlyRateByCRDate( CURORDHDRPRE.CRDATE )
					REPLACE CURORDHDRPRE.RATE WITH lnRate, CURORDHDRPRE.TOTLCOST WITH ( CURORDHDRPRE.tothours * lnRate )
				ENDSCAN
				
				WAIT WINDOW "Getting distinct VEHICLE list..." NOWAIT
				
				SELECT VEHNO, 0000000000 AS MAXMILES, 0000000000 AS LASTMILES, SPACE(100) AS COMMENT, 0000.000 AS TOTCPM, 0000.000 AS CPM, 0000.000 AS ACPM, ;
				00000000.00 AS LCOST, 00000000.00 AS PCOST,  00000000.00 AS OUTSCOST, ;
				00000000.00 AS ALCOST, 00000000.00 AS APCOST, 00000000.00 AS AOUTSCOST, 00000000.00 AS TOTACOSTS ;
				FROM CURORDHDRPRE ;
				INTO CURSOR CURCMP ;
				GROUP BY VEHNO ;
				ORDER BY VEHNO ;
				READWRITE

				WAIT CLEAR
				
				
				WAIT WINDOW "Populating mileages in vehicle list..." NOWAIT
				
				LOCAL lnMaxMileage, lnLastMileage, lnLCost, lnPCost, lnOutscost, lnALCost, lnAPCost, lnAOutscost
				
				SELECT CURCMP
				SCAN
					* populate the max() mileage from all work orders
					* JUST LOOK 1 YEAR BACK FOR THIS!
					SELECT MAX(MILEAGE) AS MILEAGE ;
					FROM CURORDHDRPRE ;
					INTO CURSOR CURMAXMILES ;
					WHERE ALLTRIM(VEHNO) == ALLTRIM(CURCMP.VEHNO) ;
					AND CRDATE > {^2015-01-01}

					SELECT CURMAXMILES
					LOCATE
					IF EOF() THEN
						lnMaxMileage = 0
					ELSE
						lnMaxMileage = CURMAXMILES.MILEAGE
					ENDIF

					REPLACE CURCMP.MAXMILES WITH lnMaxMileage IN CURCMP

					IF USED('CURMAXMILES') THEN
						USE IN CURMAXMILES
					ENDIF
					
					* ALTERNATIVELY, populate the mileage from the latest work order for each vehicle
					* NOTE: sometimes wo's from outside repairs have entered 1 for mileage, so now filtering those out.
					SELECT MAX(MILEAGE) AS MILEAGE ;
					FROM CURORDHDRPRE ;
					INTO CURSOR CURLASTMILES ;
					WHERE ALLTRIM(VEHNO) == ALLTRIM(CURCMP.VEHNO) ;
					AND CRDATE IN (SELECT MAX(CRDATE) FROM CURORDHDRPRE WHERE ALLTRIM(VEHNO) == ALLTRIM(CURCMP.VEHNO) AND MILEAGE > 1)
					
					SELECT CURLASTMILES
					lnLastMileage = CURLASTMILES.MILEAGE
					
					REPLACE CURCMP.LASTMILES WITH lnLastMileage IN CURCMP					
					
					IF USED('CURLASTMILES') THEN
						USE IN CURLASTMILES
					ENDIF					
				
				ENDSCAN	
				
				
				*****************************************************************************************
				IF .lCorrectMileages THEN
					* make mileage corrections to what came from the wo's
					USE F:\UTIL\SHOPREPORTS\DATA\MILEAGE.DBF IN 0 ALIAS MILEAGE
					SELECT MILEAGE
					SCAN
						SELECT CURCMP
						LOCATE FOR UPPER(ALLTRIM(VEHNO)) == UPPER(ALLTRIM(MILEAGE.VEHNO))
						IF FOUND() THEN
							REPLACE CURCMP.LASTMILES WITH MILEAGE.MILEAGE IN CURCMP
							.TrackProgress('!!!! Corrected Mileage for vehicle # = ' + MILEAGE.VEHNO, LOGIT+SENDIT)
						ENDIF
					ENDSCAN
					IF USED('MILEAGE') THEN
						USE IN MILEAGE
					ENDIF
				ENDIF && .lCorrectMileages
				*****************************************************************************************
							

				SELECT CURCMP
				lnPercent = 0.05
				SCAN
					lnDifference = ABS((CURCMP.MAXMILES - CURCMP.LASTMILES) / CURCMP.MAXMILES)
					IF lnDifference > lnPercent THEN
						REPLACE CURCMP.COMMENT WITH "Max Mileage vs. Latest Mileage: Difference > " + TRANSFORM(lnPercent * 100) + "%; Validate Mileage" IN CURCMP
					ENDIF
				ENDSCAN				

				WAIT WINDOW "Populating costs in vehicle list..." NOWAIT
								
				SELECT CURCMP
				SCAN
					SELECT CURORDHDRPRE
					SUM TOTLCOST TO lnLCost    FOR (ALLTRIM(VEHNO) == ALLTRIM(CURCMP.VEHNO)) AND (REPCAUSE <> "A")
					SUM TOTPCOST TO lnPCost    FOR (ALLTRIM(VEHNO) == ALLTRIM(CURCMP.VEHNO)) AND (REPCAUSE <> "A")
					SUM OUTSCOST TO lnOutscost FOR (ALLTRIM(VEHNO) == ALLTRIM(CURCMP.VEHNO)) AND (REPCAUSE <> "A")
				
					SUM TOTLCOST TO lnALCost    FOR (ALLTRIM(VEHNO) == ALLTRIM(CURCMP.VEHNO)) AND (REPCAUSE = "A")
					SUM TOTPCOST TO lnAPCost    FOR (ALLTRIM(VEHNO) == ALLTRIM(CURCMP.VEHNO)) AND (REPCAUSE = "A")
					SUM OUTSCOST TO lnAOutscost FOR (ALLTRIM(VEHNO) == ALLTRIM(CURCMP.VEHNO)) AND (REPCAUSE = "A")
				
					SELECT CURORDHDRPRE
					LOCATE
					
					REPLACE CURCMP.LCOST WITH lnLCost, ;
						CURCMP.PCOST WITH lnPCost, ;
						CURCMP.OUTSCOST WITH lnOutscost, ;
						CURCMP.ALCOST WITH lnALCost, ;
						CURCMP.APCOST WITH lnAPCost, ;
						CURCMP.AOUTSCOST WITH lnAOutscost ;
						IN CURCMP
					
				ENDSCAN				


				WAIT WINDOW "Populating costs-per-mile in vehicle list..." NOWAIT
				
				LOCAL lnTotAllCosts, lnTotNonACosts, lnTotAcosts, lnCPM, lnACPM, lnTotCPM, lnTotMileage
								
				SELECT CURCMP
				SCAN
				
					*lnTotMileage = CURCMP.MAXMILES
					lnTotMileage = CURCMP.LASTMILES
					
					IF lnTotMileage > 0 THEN
					
						lnTotNonACosts = CURCMP.LCOST  + CURCMP.PCOST  + CURCMP.OUTSCOST 	
						lnTotAcosts    = CURCMP.ALCOST + CURCMP.APCOST + CURCMP.AOUTSCOST
						lnTotAllCosts  = lnTotNonACosts + lnTotAcosts
					
						lnCPM    = lnTotNonACosts / lnTotMileage
						lnACPM   = lnTotAcosts / lnTotMileage
						lnTotCPM = lnTotAllCosts / lnTotMileage
					
						REPLACE ;
							CURCMP.CPM WITH lnCPM, ;
							CURCMP.ACPM WITH lnACPM, ;
							CURCMP.TOTCPM WITH lnTotCPM, ;
							CURCMP.TotAcosts WITH lnTotAcosts ;
							IN CURCMP
					
					ENDIF && lnTotMileage > 0	
								
				ENDSCAN
				
				* join to get remaining info from vehicles table; AT THE SAME TIME FILTERING FOR ONLY ACTIVE vehicleS
				
				SELECT A.*, B.DIVISION, B.YEAR, B.MODEL ;
				FROM CURCMP A ;
				LEFT OUTER JOIN VEHMAST B ;
				ON ALLTRIM(B.VEHNO) == ALLTRIM(A.VEHNO) ;
				INTO CURSOR CURCMP2 ;
				WHERE B.ACTIVE ;
				ORDER BY A.VEHNO
				
				SELECT DIVISION, VEHNO, YEAR, MODEL, MAXMILES, LASTMILES, CPM, ACPM, TOTCPM, LCOST, PCOST, OUTSCOST, TOTACOSTS, COMMENT  ;
				FROM CURCMP2 ;
				INTO CURSOR CURCMP3 ;
				ORDER BY DIVISION, VEHNO
				
	
*!*				SELECT CURCMP2
*!*				BROWSE
				
				* get the detail of VEHICLE wo's that can help with mileage discrepancies
				SELECT VEHNO, CRDATE, MECHNAME, OVNAME, OVCITY, MILEAGE, LEFT(WORKDESC,50) AS WORKDESC ;
					FROM CURORDHDRPRE ;
					INTO CURSOR CURDETAIL ;
					ORDER BY VEHNO, CRDATE
				
				SELECT CURCMP3
				IF 	.lCalcDateRange THEN
					COPY TO ( "C:\A\VEHICLE_CPM_SUMMARY_BEGINNING_" + DTOS(ldStartCRDate) + ".XLS" ) XL5				
					SELECT CURDETAIL
					COPY TO ( "C:\A\VEHICLE_MILEAGE_YEARS_BEGINNING_" + DTOS(ldStartCRDate) + ".XLS" ) XL5				
				ELSE
					COPY TO C:\A\VEHICLE_COST_PER_MILE.XLS XL5				
				ENDIF
				


				*******************************************************************************************************************
				.TrackProgress('VEHICLE Cost-per-mile process ended normally.', LOGIT+SENDIT)

			CATCH TO loError

				.TrackProgress('There was an error.',LOGIT+SENDIT)
				.TrackProgress('The Error # is: ' + TRANSFORM(loError.ERRORNO), LOGIT+SENDIT)
				.TrackProgress('The Error Message is: ' + TRANSFORM(loError.MESSAGE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred in Program: ' + TRANSFORM(loError.PROCEDURE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred at line #: ' + TRANSFORM(loError.LINENO), LOGIT+SENDIT)
				lnNumberOfErrors = lnNumberOfErrors + 1
				.cSendTo = 'mbennett@fmiint.com'

				IF TYPE('oExcel') = 'O' AND NOT ISNULL(oExcel) THEN
					oExcel.QUIT()
				ENDIF

			ENDTRY

			WAIT CLEAR
			***************** INTERNAL email results ******************************
			.TrackProgress('About to send status email.',LOGIT+SENDIT)
			.TrackProgress('The logfile is: ' + .cLogFile, LOGIT+SENDIT)
			.TrackProgress('VEHICLE Cost-per-mile process started: ' + .cStartTime, LOGIT+SENDIT)
			.TrackProgress('VEHICLE Cost-per-mile process finished: ' + TTOC(DATETIME()), LOGIT+SENDIT)

			IF .lSendInternalEmailIsOn THEN
				DO FORM dartmail2 WITH .cSendTo,.cFrom,.cSubject,.cCC,.cAttach,.cBodyText,"A"
				.TrackProgress('Sent status email.',LOGIT)
			ELSE
				.TrackProgress('Did not send status email: THIS.lSendInternalEmailIsOn = FALSE.',LOGIT+SENDIT)
			ENDIF

		ENDWITH
		RETURN
	ENDFUNC && main


	FUNCTION GetHourlyRateByCRDate
		LPARAMETERS tdCRdate
		DO CASE
			CASE tdCRdate < {^2008-09-01}
				lnRate = 60.00
			OTHERWISE
				lnRate = 70.00
		ENDCASE
		RETURN lnRate
	ENDFUNC


	FUNCTION GetHourlyRateByDivision
		LPARAMETERS tcDivision
		DO CASE
			CASE INLIST(tcDivision,'05','52','56','57','58','71','74')
				lnRate = 60
			OTHERWISE
				lnRate = 60  && the default rate
		ENDCASE
		RETURN lnRate
	ENDFUNC


	PROCEDURE TrackProgress
		* do any combination of Wait Window, writing to logfile, and adding to body of email,
		* based on nFlags parameter.
		LPARAMETERS tcExpression, tnFlags
		WITH THIS
			IF BITAND(tnFlags,LOGIT) = LOGIT THEN
				IF .lLoggingIsOn THEN
					?
					? .cProcessName + "  " + TTOC(DATETIME()) + ": " + tcExpression
				ENDIF
			ENDIF
			IF .lWaitWindowIsOn THEN
				IF BITAND(tnFlags,NOWAITIT) = NOWAITIT THEN
					WAIT WINDOW tcExpression NOWAIT
				ENDIF
				IF BITAND(tnFlags,WAITIT) = WAITIT THEN
					WAIT WINDOW tcExpression TIMEOUT .nWaitWindowTimeout
				ENDIF
			ENDIF
			IF BITAND(tnFlags,SENDIT) = SENDIT THEN
				IF .lSendInternalEmailIsOn THEN
					.cBodyText = .cBodyText + tcExpression + CRLF + CRLF
				ENDIF
			ENDIF
		ENDWITH
	ENDPROC  &&  TrackProgress


ENDDEFINE
