* Lifefactory rebuild report

utilsetup("LIFEBASE")

if holiday(date())
	return
endif

goffice="I"

xsqlexec("select * from inven where accountid=6034 and availqty<replenamt and availqty>0",,,"wh")

* 

select ;
	style, ;
	color, ;
	id, ;
	replenamt as base, ;
	fillamt as rebuild, ;
	availqty ;
	from inven ;
	where accountid=6034 ;
	and availqty<replenamt and availqty>0 ;
	order by style, color, id ;
	into cursor xrpt readwrite

if reccount("xrpt")>0
	copy to "h:\fox\lifebase.xls" type xls

	locate

	rpt2pdf("lifebase","h:\fox\lifebase.pdf")

	tsendto="cmalcolm@fmiint.com, jlake@fmiint.com, wschiele@fmiint.com, Jason@lifefactory.com, Leandra.murchison@tollgroup.com, Greg@Lifefactory.com, ethan@lifefactory.com"
*	tsendto="cmalcolm@fmiint.com"
	tcc=""
	tattach="h:\fox\lifebase.pdf,h:\fox\lifebase.xls"
	tFrom="TGF Operations  <tgfoperations@fmiint.com>"
	tmessage=""
	tSubject="Lifefactory rebuild report"

	Do Form dartmail2 With tsendto,tFrom,tSubject,tcc,tattach,tmessage,"A"
endif

use in inven

*

schedupdate()

_screen.Caption=gscreencaption
on error
