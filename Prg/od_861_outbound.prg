Parameters lcBol,storelist,lcWhichRoute

Set Step On 

Public c_CntrlNum,c_GrpCntrlNum,cWO_Num,cGroupName,cDivision,cCustName,cFilename,div214,cST_CSZ
Public tsendto,tcc,tsendtoerr,tccerr,cFin_Status,lDoCatch,lDoJfilesout

If Empty(lcBol) Or Empty(storelist)
  Messagebox("Need proper parameters",0,"OD Outbound-861")
  Return
Endif 

lTesting = .F.
lEmail = !lTesting

Do m:\dev\prg\_setvars With lTesting

If lTesting
  Close Data All
Endif

lDofilesout=.T.

cCustName="OfficeDepot"
cFilename = ""
lFClose = .T.
lIsError = .F.
lDoCatch = .T.
cShip_ref = ""
cST_CSZ = ""
cFin_Status = ""
nFilenum = 0
dt1	 = Ttoc(Datetime(),1)
cFin_Status = "UNEXPECTED"

*ASSERT .f.
Try
  cSystemName = "UNASSIGNED"
  nAcctNum = 6494
  cWO_Num = ""
  lDoError = .F.

  xReturn = "XX"
  If !lTesting
    Do m:\dev\prg\wf_alt With "C",6494
    cUseFolder = Upper(xReturn)
  Else
    cUseFolder = "f:\whp\whdata\"
  Endif

  If !Used("ctnucc")
    Use (cUseFolder+"ctnucc") In 0  Shared
  Endif   

*Set Step On 

  Select * From ctnucc where Inlist(store,&storelist) and totqty = 1 And !asnstatus = "S" into Cursor oCartons
  
  If Reccount("oCartons") =0
     Messagebox("No Cartons avaiable for shipment...........",0,"Office Depot Outbound 861 ") 
     lDocatch = .f.
     throw
  Endif 
  
  
*   "0818","0934","0963","0935","2210","0942"
*   "0814","0874","0846","2099","0494","0908"
*   "0965","2304","0609","2371","2522"
 
  Select Store From ocartons Into Cursor instores Group By Store

  Select 0
  Use F:\3pl\Data\mailmaster Alias mm Shared
  Locate For edi_type = "861" And accountid = nAcctNum
  Scatter Memvar
  _Screen.Caption = Alltrim(mm.scaption)
  tsendto = Iif(mm.use_alt,mm.sendtoalt,mm.sendto)
  tcc = Iif(mm.use_alt,mm.ccalt,mm.cc)

  cFilename      = Alltrim(mm.holdpath)+("ODOUT_"+dt1+"_"+lcWhichRoute+".EDI")
  cFilenameShort = Justfname(cFilename)
  cFilenameArch  = (Alltrim(mm.archpath)+cFilenameShort)
  cFilenameOut   = (Alltrim(mm.basepath)+cFilenameShort)
  Locate
  Locate For mm.edi_type = "MISC" And mm.taskname = "GENERAL"
  tsendtoerr = Iif(mm.use_alt,mm.sendtoalt,mm.sendto)
  tccerr = Iif(mm.use_alt,mm.ccalt,mm.cc)
  Use In mm

  tfrom = "TGF Warehouse Operations <warehouse-ops@fmiint.com>"
  tattach = ""

  Select 0
  nFilenum = Fcreate(cFilename)
  If !nFilenum > 0
    cFin_Status = "Cant Create file"
    Throw
  Endif

  cfd = "*"  && Field delimiter
  csegd = ""  && Segment delimiter
  cterminator = ">"  && Used at end of ISA segment

  csendqual = "ZZ"  && Sender qualifier
  csendid = "TGF"   && Sender ID code
  crecqual = "ZZ"   && Recip qualifier
  If lTesting
    crecid = "1ODAS2"   && Recip ID Code
  Else
    crecid = "OFFICEDEPOT"   && Recip ID Code
  Endif
  cDate = Dtos(Date())
  cTruncDate = Right(cDate,6)
  cTruncTime = Substr(Ttoc(Datetime(),1),9,4)
  cfiledate = cDate+cTruncTime
  csendidlong = Padr(csendid,15," ")
  crecidlong = Padr(crecid,15," ")
  cString = ""

  nSTCount = 0
  nSegCtr = 0
  nLXNum = 1

  Store "" To cEquipNum,cB10

  Wait Clear
  Wait Window "Now creating an Office Depot 861.........." Nowait

  cISACode = Iif(lTesting,"T","P")

  Do num_incr_isa
  cISA_Num = Padl(c_CntrlNum,9,"0")

  Store "ISA"+cfd+"00"+cfd+"          "+cfd+"00"+cfd+"          "+cfd+csendqual+cfd+csendidlong+cfd+;
  crecqual+cfd+crecidlong+cfd+cTruncDate+cfd+cTruncTime+cfd+"U"+cfd+"00401"+cfd+;
  cISA_Num+cfd+"0"+cfd+cISACode+cfd+":"+csegd To cString  && +cfd+cterminator
  Do cstringbreak

  Store "GS"+cfd+"RC"+cfd+csendid+cfd+crecid+cfd+cDate+cfd+cTruncTime+cfd+c_CntrlNum+;
  cfd+"X"+cfd+"004010"+csegd To cString
  Do cstringbreak

  Do num_incr_st

  Store "ST"+cfd+"861"+cfd+Padl(c_GrpCntrlNum,9,"0")+csegd To cString
  Do cstringbreak
  nSegCtr = nSegCtr + 1

  nSTCount = nSTCount + 1

  Select instores
  Scan

    Select * From ocartons Where store = instores.Store Into Cursor cartons
    lnCtnCount = Reccount("cartons")

    cRefID = "BROKERREF"

&& 00003 = Pooler BOL #
    Store "BRA"+cfd+Alltrim(lcBol)+cfd+cDate+cfd+"00"+cfd+"3"+csegd To cString
    Do cstringbreak
    nSegCtr = nSegCtr + 1


    Store "DTM"+cfd+"242"+cfd+cDate+cfd+cTruncTime+csegd To cString
    Do cstringbreak
    nSegCtr = nSegCtr + 1

    Store "DTM"+cfd+"243"+cfd+cDate+cfd+cTruncTime+csegd To cString
    Do cstringbreak
    nSegCtr = nSegCtr + 1

    Store "TD1"+cfd+"CTN"+cfd+Alltrim(Transform(lnCtnCount))+csegd To cString  && number od Ctns on the BOL
    Do cstringbreak
    nSegCtr = nSegCtr + 1

    Store "TD5"+cfd+cfd+cfd+cfd+"M"+csegd To cString  && Motor
    Do cstringbreak
    nSegCtr = nSegCtr + 1

*    Store "TD3"+cfd+"TL"+cfd+cfd+Alltrim(bl.trailer)+csegd To cString  && Motor

    Store "TD3"+cfd+"TL"+cfd+cfd+Alltrim(lcBOL)+csegd To cString  && Motor
    Do cstringbreak
    nSegCtr = nSegCtr + 1

    Store "N1"+cfd+"WH"+cfd+cfd+"91"+cfd+"3061"+csegd To cString  && DC
    Do cstringbreak
    nSegCtr = nSegCtr + 1

    Store "N1"+cfd+"7P"+cfd+cfd+"91"+cfd+"306100002"+csegd To cString  && Pool Agent
    Do cstringbreak
    nSegCtr = nSegCtr + 1

    Store "N1"+cfd+"SN"+cfd+cfd+"91"+cfd+Alltrim(Transform(cartons.Store))+csegd To cString  && Store number
    Do cstringbreak
    nSegCtr = nSegCtr + 1

    lnGoodStoreCtns =0  && OK, lets count the Ctns for this store
    Select cartons
    Scan
      If cartonstatus = "07"
        lnGoodStoreCtns = lnGoodStoreCtns +1
      Endif   
    Endscan

    lnDamageStoreCtns =0  && OK, lets count the Ctns for this store
    Select cartons
    Scan
      If cartonstatus = "01"
        lnDamageStoreCtns = lnDamageStoreCtns +1
      Endif   
    Endscan
    
    If lnGoodStoreCtns > 0
      Store "RCD"+cfd+Alltrim(Transform(nSegCtr))+cfd+cfd+cfd+cfd+cfd+Alltrim(Transform(lnGoodStoreCtns))+cfd+"CT"+cfd+"07"+csegd To cString  && one ctn short
      Do cstringbreak
      nSegCtr = nSegCtr + 1
    Endif   

    If lnDamageStoreCtns > 0
      Store "RCD"+cfd+Alltrim(Transform(nSegCtr+1))+cfd+cfd+cfd+cfd+cfd+Alltrim(Transform(lnDamageStoreCtns))+cfd+"CT"+cfd+"01"+csegd To cString  && one ctn short
      Do cstringbreak
      nSegCtr = nSegCtr + 1
    Endif   

    Store "REF"+cfd+"BM"+cfd+Alltrim(lcBol)+csegd To cString  
    Do cstringbreak
    nSegCtr = nSegCtr + 1

    Select cartons
    Scan

      Store "MAN"+cfd+"AI"+cfd+cartons.ucc+csegd To cString  && Ctn numbers
      Do cstringbreak
      nSegCtr = nSegCtr + 1

    Endscan

&& closing segments
  Endscan

  Store  "SE"+cfd+Alltrim(Str(nSegCtr+1))+cfd+Padl(c_GrpCntrlNum,9,"0")+csegd To cString
  Do cstringbreak
  Store  "GE"+cfd+"1"+cfd+c_CntrlNum+csegd To cString
  Do cstringbreak
  Store  "IEA"+cfd+"1"+cfd+Padl(c_CntrlNum,9,"0")+csegd To cString
  Do cstringbreak

  If nFilenum>0
    =Fclose(nFilenum)
  Endif

  Copy File &cFilename To &cFilenameArch

  If lDofilesout
    Copy File &cFilename To &cFilenameOut
    Delete File &cFilename
  Endif

  Select oCartons
  lcTotalShipped  = Reccount("oCartons")
  
  Select oCartons
  Scan
    Select ctnucc
      && close this ASN as after its been received we should not receive any more cartons
    replace asnstatus With "S" , out861dt With Date(),out861sent With .t., out861file With Justfname(cFilename) For ctnucc.ucc = oCartons.ucc
  Endscan 

  If !lTesting
    If !Used("edi_trigger")
      use f:\3pl\data\edi_trigger In 0
    Endif    
    Insert Into edi_trigger (edi_type,accountid,office,processed,errorflag,fin_status,trig_time,when_proc,bol,consignee,trig_from,file945,outshipid) Values ;
                            ("861",6494,"C",.t.,.f.,"861 Outbound",Datetime(),Datetime(),lcBOL,"Office Depot","ODSystem",cFilename,lcTotalShipped )
  Endif

  && now mark the pallets as shipped as well
  
*  If !Used("pallets")
*    Use f:\wh\pallets In 0
*  Endif 
  Select instores
  Scan
*    Select pallets
*    replace pallets.status With "S" For instores.store = pallets.store In pallets

    xsqlexec("update pallets set status = 'S' where store = "+instores.store,"xpt",,"wh")

  Endscan 
    
  tsubject = "Office Depot Outbound 861 EDI from Toll"
  tmessage = "861-EDI Info from TGF-CA"+Chr(13)
  tmessage = tmessage + "for Office Depot has been created."+Chr(13)+"Filename: "+cFilename+Chr(13)+"861 contains "+Transform(lcTotalShipped)+" Cartons" 
  Do Form dartmail2 With tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"

Catch To oErr
*  Set Step On
  If lDoCatch
    lEmail = .T.

    If cFin_Status !="UNEXPECTED"
      Do ediupdate With cFin_Status,.T.
    Endif

    tsubject = cCustName+" Error ("+Transform(oErr.ErrorNo)+") at "+Ttoc(Datetime())
    tattach  = ""
    tsendto  = tsendtoerr
    tmessage = tccerr

    tmessage =tmessage+ "Try/CATCH Exeception Message:"+Chr(13)+;
    [  Error: ] + Str(oErr.ErrorNo) +Chr(13)+;
    [  LineNo: ] + Str(oErr.Lineno) +Chr(13)+;
    [  Message: ] + oErr.Message +Chr(13)+;
    [  Procedure: ] + oErr.Procedure +Chr(13)+;
    [  Details: ] + oErr.Details +Chr(13)+;
    [  StackLevel: ] + Str(oErr.StackLevel) +Chr(13)+;
    [  LineContents: ] + oErr.LineContents+Chr(13)+;
    [  UserValue: ] + oErr.UserValue

    tmessage = tmessage+Chr(13)+cFin_Status

    tsubject = "214 EDI Poller Error at "+Ttoc(Datetime())
    tattach  = ""
    tcc=""
    tfrom    ="TGF EDI 214 Poller Operations <transload-ops@fmiint.com>"
    Do Form dartmail2 With tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
    lEmail = .F.
  Endif
Finally
  Set Hours To 12
  Fclose(nFilenum)
  closefiles()
Endtry

&& END OF MAIN CODE SECTION



****************************
Procedure num_incr_isa
****************************

If !Used("serfile")
  Use ("f:\3pl\data\serial\"+cCustName+"_serial") In 0 Alias serfile
Endif

Select serfile
If lTesting
  nOrigSeq = serfile.seqnum
  nOrigGrpSeq = serfile.grpseqnum
  lISAFlag = .F.
Endif

nISA_Num = serfile.seqnum
c_CntrlNum = Alltrim(Str(serfile.seqnum))
Replace serfile.seqnum With serfile.seqnum + 1 In serfile
Use In serfile

Endproc

****************************
Procedure num_incr_st
****************************

If !Used("serfile")
  Use ("f:\3pl\data\serial\"+cCustName+"_serial") In 0 Alias serfile
Endif

Select serfile
If lTesting
  nOrigSeq = serfile.seqnum
  nOrigGrpSeq = serfile.grpseqnum
  lSTFlag = .F.
Endif

c_GrpCntrlNum = Alltrim(Str(serfile.grpseqnum))
Replace serfile.grpseqnum With serfile.grpseqnum + 1 In serfile
Use In serfile

Endproc

****************************
Procedure segmentget
****************************

Parameter thisarray,lcKey,nLength

For i = 1 To nLength
  If i > nLength
    Exit
  Endif
  lnEnd= At("*",thisarray[i])
  If lnEnd > 0
    lcThisKey =Trim(Substr(thisarray[i],1,lnEnd-1))
    If Occurs(lcKey,lcThisKey)>0
      Return Substr(thisarray[i],lnEnd+1)
      i = 1
    Endif
  Endif
Endfor

Return ""

****************************
Procedure close214
****************************
Store  "SE"+cfd+Alltrim(Str(nSegCtr+1))+cfd+Padl(c_GrpCntrlNum,9,"0")+csegd To cString
Do cstringbreak

Store  "GE"+cfd+Alltrim(Str(nSTCount))+cfd+c_CntrlNum+csegd To cString
Do cstringbreak

Store  "IEA"+cfd+"1"+cfd+Padl(c_CntrlNum,9,"0")+csegd To cString
Do cstringbreak

Return

****************************
Procedure cstringbreak
****************************
cLen = Len(Alltrim(cString))
Fputs(nFilenum,cString)
Return



**************************
Procedure ediupdate
**************************
Parameters cFin_Status, lIsError,lFClose
If !lTesting
  Select edi_trigger
  Locate
  If lIsError
    Replace edi_trigger.processed With .T.,edi_trigger.created With .F.,;
    edi_trigger.fin_status With cFin_Status,edi_trigger.errorflag With .T.;
    edi_trigger.when_proc With Datetime() ;
    FOR edi_trigger.wo_num = nWO_Num And edi_trigger.edi_type = "214"
    If nFilenum>0
      =Fclose(nFilenum)
    Endif
    If File(cFilename)
      Delete File &cFilename
    Endif
    closefiles()
  Else
    Replace edi_trigger.processed With .T.,edi_trigger.created With .T.,proc214 With .T.;
    edi_trigger.when_proc With Datetime(),;
    edi_trigger.fin_status With cStatusCode+" 214 CREATED";
    edi_trigger.errorflag With .F.,file214 With cFilename ;
    FOR edi_trigger.wo_num = nWO_Num
  Endif
Endif
Endproc

*********************
Procedure closefiles
*********************
If Used('ftpedilog')
  Use In ftpedilog
Endif
If Used('serfile')
  Use In serfile
Endif
If Used('wolog')
  Use In wolog
Endif
If Used('xref')
  Use In xref
Endif
If Used('ftpjobs')
  Use In ftpjobs
Endif
Endproc
