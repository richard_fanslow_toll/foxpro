*!* m:\dev\prg\billabong850_main.prg

PARAMETERS cOfficeIn
CLOSE DATA ALL
PUBLIC nAcctNum,cDelimiter,nRun_num,cTranslateOption,cWhichFile,cOffice,cCustname,cPropername,cStorenum
PUBLIC cFilter,nRunID,lComplete,lTesting,m.UCCNUMBER,lEmail,lTestImport,emailcommentstr,cUseFolder,tFrom
PUBLIC ptid,ptctr,ptqty,xfile,cfilename,UploadCount,pickticket_num_start,pickticket_num_end,nFileCount,units
PUBLIC cOldScreen,tsendto,tcc,lcPath,lcArchivePath,nPTQty,cPTQty,archivefile,lBrowfiles,cMod,nFileSize,lLoadSQL
PUBLIC tsendtoerr,tccerr,cOfficeLoc,ldeletefile,cErrMsg,lJCP,lJCPMail,lN2,m.custsku,cBillabongXfer,lDoLineInsert,cISA_Num
PUBLIC ARRAY a856(1)

PUBLIC message_timeout,message_timeout2

message_timeout ="nowait"
message_timeout2 ="timeout 2"
cErrMsg = ""
*SET STEP ON 
IF VARTYPE(cOfficeIn)="L"
	cOfficeIn = "L"
	cOfficeLoc = "ML"
ENDIF
ON ERROR debug

cOffice = cOfficeIn

lTesting = .f.
lTestImport = lTesting
lOverridebusy = lTesting && Will override FTPSETUP chkbusy flag if .t.
lBrowfiles = lTesting  && Will allow browsing of XPT/XPTDET cursors before upload if .t.
lEmail = .t.
lOverridebusy = .f.
lBrowfiles = .f.
tFrom ="TGF EDI Operations <toll-edi-ops@tollgroup.com>"

DO setenvi

_SCREEN.WINDOWSTATE = IIF(lTesting OR lTestImport OR lOverridebusy,2,1)

STORE "" TO cOldScreen
STORE _SCREEN.CAPTION TO cOldScreen
cCustname = "BILLABONG"
cUseName = PROPER(cCustname)
cOfficeLoc = " "
cPropername = cUseName
nAcctNum  = 6718
cMod = "L"
gOffice = cMod
lLoadSQL = .t.


xfile = ""
cErrMsg = ""
ldeletefile = .T.
NormalExit = .F.
lN2 = .F.
tsendto =""
tsendtoerr =""
tcc=""
tccerr=""
cBillabongxfer = "850-BILLABONG"
STORE cPropername+" 850 Process" TO _SCREEN.CAPTION
CLEAR
WAIT WINDOW "Now setting up "+cPropername+" 850 process..." &message_timeout

TRY
	DO m:\dev\prg\createx856a

	cfile = ""
	dXdate1 = ""
	dXxdate2 = DATE()
	nFileCount = 0

	nRun_num = 999
	lnRunID = nRun_num

	IF !lTesting
	SELECT 0
	USE F:\edirouting\FTPSETUP SHARED
	LOCATE FOR FTPSETUP.TRANSFER = cBillabongXfer
	IF FOUND()
		IF chkbusy AND !lOverridebusy
			WAIT WINDOW cPropername+" 850 Process is busy...exiting" TIMEOUT 2
			NormalExit = .T.
			THROW
		ENDIF
	ENDIF
	REPLACE chkbusy WITH .T.,trig_time WITH DATETIME()  FOR FTPSETUP.TRANSFER = cBillabongXfer
	USE IN FTPSETUP
	endif

*	ASSERT .F. MESSAGE "At MM Config load"
	SELECT 0
	USE F:\3pl\DATA\mailmaster ALIAS mm SHARED
	LOCATE FOR mm.accountid = nAcctNum AND mm.office = cOffice AND edi_type = "850"
	SET STEP ON 
	IF FOUND()
		STORE TRIM(mm.basepath) TO lcPath
		STORE TRIM(mm.archpath) TO lcArchivePath
			IF mm.use_alt
			tsendto = TRIM(mm.sendtoalt)
			tcc = TRIM(mm.ccalt)
			else
			tsendto = TRIM(mm.sendto)
			tcc = TRIM(mm.cc)
			endif
		STORE TRIM(mm.scaption) TO thiscaption
		STORE mm.testflag 	   TO lTest
		_SCREEN.CAPTION = thiscaption
		IF lTesting
		locate
		LOCATE FOR (mm.edi_type = "MISC") AND (mm.taskname = "JOETEST")
			IF mm.use_alt
			tsendto = TRIM(mm.sendtoalt)
			tcc = TRIM(mm.ccalt)
			else
			tsendto = TRIM(mm.sendto)
			tcc = TRIM(mm.cc)
			endif
		ENDIF
		LOCATE FOR (mm.edi_type = "MISC") AND (mm.taskname = "GENERAL")
			IF mm.use_alt
			tsendtoerr = TRIM(mm.sendtoalt)
			tccerr = TRIM(mm.ccalt)
			else
			tsendtoerr = TRIM(mm.sendto)
			tccerr = TRIM(mm.cc)
			endif
		USE IN mm
	ELSE
		WAIT WINDOW AT 10,10  "No parameters set for this acct# "+ALLTRIM(STR(nAcctNum))+"  ---> Office "+cOffice TIMEOUT 2
		USE IN mm
		NormalExit = .T.
		THROW
	ENDIF

	IF lTesting
		lcPath = "F:\FTPUSERS\BILLABONG\850IN\850TEST\"
		lcArchivePath = "F:\FTPUSERS\BILLABONG\850IN\850TEST\archive\"
		cUseFolder = "F:\WHP\WHDATA\"
		WAIT WINDOW "Test data...Importing into WHP tables"  &message_timeout
	ELSE
		xReturn = "XXX"
		DO m:\dev\prg\wf_alt WITH cOffice,nAcctNum
		cUseFolder = UPPER(xReturn)
	ENDIF

	DO ("m:\dev\PRG\"+cCustname+"850_PROCESS")

	IF !lTesting
	SELECT 0
	USE F:\edirouting\FTPSETUP SHARED
	REPLACE chkbusy WITH .F. FOR FTPSETUP.TRANSFER = cBillabongXfer
	USE IN FTPSETUP
	endif

CATCH TO oErr
	IF !NormalExit
		ASSERT .F. MESSAGE "At CATCH..."
		SET STEP ON

		tsubject = cCustname+" 940 "+cOfficeLoc+" Upload Error ("+TRANSFORM(oErr.ERRORNO)+") at "+TTOC(DATETIME())
		tattach  = ""
		tsendto  = tsendtoerr
		tcc = tccerr
		tmessage = cCustname+" 940 "+cOfficeLoc+" Upload Error..... Please fix me........!"+CHR(13)+cErrMsg
		lcSourceMachine = SYS(0)
		lcSourceProgram = SYS(16)

		tmessage =tmessage+ "Try/CATCH Exeception Message:"+CHR(13)+;
			[  Message: ] + oErr.MESSAGE +CHR(13)+;
			[  Procedure: ] + oErr.PROCEDURE +CHR(13)+;
			[  LineNo: ] + STR(oErr.LINENO) +CHR(13)+;
			[  Error: ] + STR(oErr.ERRORNO) +CHR(13)+;
			[  Details: ] + oErr.DETAILS +CHR(13)+;
			[  StackLevel: ] + STR(oErr.STACKLEVEL) +CHR(13)+;
			[  LineContents: ] + oErr.LINECONTENTS+CHR(13)+;
			[  UserValue: ] + oErr.USERVALUE+CHR(13)+;
			[  Computer:  ] +lcSourceMachine+CHR(13)+;
			[  940 file:  ] +xfile+CHR(13)+;
			[  Program:   ] +lcSourceProgram

		tattach  = ""
		tFrom ="TGF EDI Operations <toll-edi-ops@tollgroup.com>"
		DO FORM m:\dev\frm\dartmail2 WITH tsendto,tFrom,tsubject,tcc,tattach,tmessage,"A"
	ELSE
		WAIT WINDOW AT 10,10 "Normal Exit " TIMEOUT 2
		SELECT 0
		USE F:\edirouting\FTPSETUP SHARED
		REPLACE chkbusy WITH .F. FOR FTPSETUP.TRANSFER = cBillabongXfer
		USE IN FTPSETUP

	ENDIF
FINALLY
	CLOSE DATABASES ALL
ENDTRY
