* import Ariat consignee routing info

* programmed by MB; spec'd by Juan R.

* processed adapted from: import_ariat_cons.prg

* build exe s F:\UTIL\ARIAT\ARIATCONSIMPORT.EXE

* this will be called from the folderscan process when files are detected in the source folder.
*
* 4/25/2018 MB: changed fmiint.com to my Toll email.
*
*ARIATCONSIMPORT

LOCAL lTestMode
lTestMode = .F.

guserid = "ARIATCONSIMP"

IF NOT lTestMode THEN
	utilsetup("ARIATCONSIMPORT")
ENDIF

DECLARE INTEGER CreateMutex IN Win32API ;
	STRING @lpMutexAttributes, ;
	STRING bInitialOwner, ;
	STRING @lpName

DECLARE INTEGER GetLastError IN Win32API

* if running in EXE check to see if already running
IF _VFP.STARTMODE > 0 THEN
	lcProcessName = "ARIATCONSIMPORT"
	CreateMutex(0,CHR(1),@lcProcessName)
	lnError = GetLastError()
	IF lnError = 183
		WAIT WINDOW TIMEOUT 5 "This program is already running..."
		IF NOT lTestMode THEN
			schedupdate()
		ENDIF
		RETURN .F.
	ENDIF
ENDIF

loARIATCONSIMPORT = CREATEOBJECT('ARIATCONSIMPORT')
loARIATCONSIMPORT.MAIN( lTestMode )

IF NOT lTestMode THEN
	schedupdate()
ENDIF

CLOSE DATABASES ALL
RETURN

#DEFINE LOGIT 1
#DEFINE WAITIT 2
#DEFINE NOWAITIT 4
#DEFINE SENDIT 8
#DEFINE CRLF CHR(13) + CHR(10)
#DEFINE RETURN_DATA_MANDATORY .F.
#DEFINE RETURN_DATA_NOT_MANDATORY .T.
#DEFINE CSVFORMAT 6
#DEFINE WRITE_ONLY_UNBUFFERED 11
#DEFINE COMMA ","

DEFINE CLASS ARIATCONSIMPORT AS CUSTOM

	cProcessName = 'ARIATCONS'

	lTestMode = .F.  && .F. FOR NORMAL OPERATION, .T. TO SUPPRESS ALL BUT DEVELOPER EMAILS AND CHANGE INPUT TABLE, ETC.

	dToday = DATE()

	lAutoYield = .T.

	cStartTime = TTOC(DATETIME())

	* connection properties
	nSQLHandle = 0

	* folder properties
	cInputFolder    = 'F:\FTPUSERS\ARIAT\FTPIN\ROUTING\'
	cArchivedFolder = 'F:\FTPUSERS\ARIAT\FTPIN\ROUTING\ARCHIVED\'
	cHoldingFolder  = 'F:\FTPUSERS\ARIAT\FTPIN\ROUTING\HOLDING\'

	* data properties
	cAddProc = "ARIATCONS"

	* processing properties
	lDeleteFoundRecs = .F.  && if .T. then when we find a key match we will delete existing rec and insert the new one. If .F. we will simply not insert when we find a new match.
	nRow = 0

	* Excel properties
	oExcel = NULL

	* wait window properties
	nWaitWindowTimeout = 2
	lWaitWindowIsOn = .T.

	* logfile properties
	lLoggingIsOn = .T.
	cLogFile = 'F:\UTIL\ARIAT\LOGFILES\ARIATCONSIMPORT_log.txt'

	* INTERNAL email properties
	lSendInternalEmailIsOn = .T.
	cFrom = 'mark.bennett@tollgroup.com'
	cSendTo = 'mark.bennett@tollgroup.com'
	cCC = ''
	cSubject = 'Ariat Consignee Routing Import for ' + TRANSFORM(DATETIME())
	cAttach = ''
	cBodyText = ''
	cMGRSendTo = ''
	cMGRFrom = ''
	cMGRSubject = ''
	cMGRCC = ''
	cMGRAttach = ''
	cMGRBodyText = ''

	cCOMPUTERNAME = ''
	cUSERNAME = ''


	FUNCTION INIT
		IF NOT DODEFAULT()
			RETURN .F.
		ENDIF
		WITH THIS
			*SET RESOURCE OFF
			CLOSE DATA
			SET CENTURY ON
			*SET DATE AMERICAN
			*SET HOURS TO 24
			SET DECIMALS TO 0
			SET ANSI ON
			SET TALK OFF
			SET DELETED ON
			SET CONSOLE OFF
			SET EXCLUSIVE OFF
			SET SAFETY OFF
			SET EXACT OFF
			SET STATUS BAR ON
			SET SYSMENU OFF
			SET ENGINEBEHAVIOR 70
			SET MULTILOCKS ON
			_VFP.AUTOYIELD = .lAutoYield
			.cCOMPUTERNAME = GETENV("COMPUTERNAME")
			.cUSERNAME = GETENV("USERNAME")
			IF .lTestMode THEN
				.cLogFile = 'F:\UTIL\ARIAT\LOGFILES\ARIATCONSIMPORT_log_TESTMODE.txt'
			ENDIF
			.lLoggingIsOn = .lLoggingIsOn AND NOT EMPTY(.cLogFile)
			IF .lLoggingIsOn THEN
				SET ALTERNATE TO (.cLogFile) ADDITIVE
				SET ALTERNATE ON
			ENDIF
		ENDWITH
	ENDFUNC


	FUNCTION DESTROY
		WITH THIS
			IF .lLoggingIsOn  THEN
				SET ALTERNATE OFF
				SET ALTERNATE TO
			ENDIF
		ENDWITH
		IF TYPE('.oExcel') = 'O' AND NOT ISNULL(.oExcel) THEN
			.oExcel.QUIT()
		ENDIF
		DODEFAULT()
	ENDFUNC


	FUNCTION MAIN
		LPARAMETERS tlTestMode
		WITH THIS
			LOCAL lnNumberOfErrors, laFilesSorted[1,6], lnNumFiles, lnCurrentFile, lcSourceFile, lcArchivedFile, lcHoldingFile, lcConsignee, lnCtr
			LOCAL lcFileName, llFirstPass

			TRY
				lnNumberOfErrors = 0

				.lTestMode = tlTestMode
				
				llFirstPass = .T.

				.cStartTime = TTOC(DATETIME())

				.TrackProgress('Ariat Consignee Routing Import process started....', LOGIT+SENDIT)
				.TrackProgress('COMPUTERNAME = ' + .cCOMPUTERNAME, LOGIT+SENDIT)
				.TrackProgress('USERNAME = ' + .cUSERNAME, LOGIT+SENDIT)
				.TrackProgress('PROJECT = ARIATCONSIMPORT', LOGIT+SENDIT)
				IF .lTestMode THEN
					.TrackProgress('                        TEST MODE                        ', LOGIT+SENDIT)
				ENDIF
				.TrackProgress('.cInputFolder = ' + .cInputFolder, LOGIT+SENDIT)
				.TrackProgress('.cArchivedFolder = ' + .cArchivedFolder, LOGIT+SENDIT)
				.TrackProgress('.cHoldingFolder = ' + .cHoldingFolder, LOGIT+SENDIT)

				*!*					* OPEN Excel
				*!*					.oExcel = CREATEOBJECT("excel.application")
				*!*					.oExcel.displayalerts = .F.
				*!*					.oExcel.VISIBLE = .F.


				lnNumFiles = ADIR(laFiles,(.cInputFolder + "CU*.TXT"))
				*lnNumFiles = ADIR(laFiles,(.cInputFolder + "*.CSV"))

				IF lnNumFiles > 0 THEN

*!*						USE F:\UTIL\ARIAT\DATA\ARIATCONS IN 0 EXCLUSIVE ALIAS ARIATCONS
*!*						
*!*						IF .lTestMode THEN
*!*							USE F:\UTIL\ARIAT\DATA\CONSK IN 0 ALIAS CONS
*!*						ELSE
*!*							useca("cons","wh",,,"office='K'")						
*!*						endif
*!*						
*!*						open database f:\wh\whall

*!*						ZAP IN ariatcons
*!*						DELETE ALL IN cons  

					* sort file list by date/time
					.SortArrayByDateTime(@laFiles, @laFilesSorted)

					FOR lnCurrentFile = 1 TO lnNumFiles

						lcSourceFile = .cInputFolder + laFilesSorted[lnCurrentFile,1]
						lcArchivedFile = .cArchivedFolder + laFilesSorted[lnCurrentFile,1]
						lcHoldingFile = .cHoldingFolder + laFilesSorted[lnCurrentFile,1]

						.TrackProgress('lcSourceFile = ' + lcSourceFile,LOGIT+SENDIT)
						.TrackProgress('lcArchivedFile = ' + lcArchivedFile,LOGIT+SENDIT)
						.TrackProgress('lcHoldingFile = ' + lcHoldingFile,LOGIT+SENDIT)


						* move source file into Holding folder so that, if an error causes the program to abort,
						* the folderscan process does not keep reprocessing the bad source file in an infinite loop.

						* if the file already exists in the Holding folder, make sure it is not read-only.
						llArchived = FILE(lcHoldingFile)
						IF llArchived THEN
							RUN ATTRIB -R &lcHoldingFile.
							.TrackProgress('   Used ATTRIB -R for re-archiving : ' + lcHoldingFile,LOGIT+SENDIT)
						ENDIF

						* archive the source file
						COPY FILE (lcSourceFile) TO (lcHoldingFile)

						* always delete the source file
						*IF FILE(lcHoldingFile) THEN
						DELETE FILE (lcSourceFile)
						*ENDIF


						**************************************************************************************
						**************************************************************************************
						**************************************************************************************
						* then process from the file once it is in the Holding folder
						
						IF llFirstPass THEN
							* Open files and other init - just once
							llFirstPass = .F.
							
							USE F:\UTIL\ARIAT\DATA\ARIATCONS IN 0 EXCLUSIVE ALIAS ARIATCONS
							
							IF .lTestMode THEN
								USE F:\UTIL\ARIAT\DATA\CONSK IN 0 ALIAS CONS
							ELSE
								useca("cons","wh",,,"office='K'")						
							endif
							
							open database f:\wh\whall

							ZAP IN ariatcons
							DELETE ALL IN cons  
						ENDIF

						SELECT ariatcons
						*APPEND FROM (lcHoldingFile) CSV
						APPEND FROM (lcHoldingFile) DELIMITED WITH TAB
						* delete the 1st header record
						SELECT ariatcons
						LOCATE
						DELETE
						
						* remove 2 leading zeroes from ACCTNUM which is making it 10 chars instead of the expected 8
						SELECT ARIATCONS
						SCAN
							IF LEFT(ARIATCONS.ACCTNUM,2) = "00" THEN
								REPLACE ARIATCONS.ACCTNUM WITH SUBSTR(ARIATCONS.ACCTNUM,3) IN ARIATCONS
							ENDIF
						ENDSCAN
						
						

*!*							SELECT ariatcons
*!*							brow

						lcConsignee = "~xyz@"

						lnCtr = 0
						
						SELECT ariatcons
						SCAN
							IF consignee#lcConsignee
								IF lcConsignee#"~xyz@"
									m.flag = .f.
									m.updateby = .cUSERNAME
									m.updproc = "ARIATCONS"
									m.addproc = "ARIATCONS"
									m.addby = "ARIATCONS"
									m.adddt = DATETIME()
									m.consid = sqlgenpk("cons","wh")
									m.office="K"
									INSERT INTO cons FROM MEMVAR
									lnCtr = lnCtr + 1
								ENDIF
								SCATTER MEMVAR
								*m.comment=TRIM(TEXT)
								m.comment = IIF(EMPTY(shipto),"","(" + ALLTRIM(shipto) + ") ") + TRIM(TEXT)
							ELSE

								*m.comment = m.comment+ ' ' + trim(text)+chr(13)+chr(10)
								*m.comment = m.comment+ ' ' + IIF(EMPTY(shipto),"","(" + ALLTRIM(shipto) + ") ") +  TRIM(TEXT)+CHR(13)+CHR(10)
								IF EMPTY(m.comment) THEN
									m.comment = IIF(EMPTY(shipto),"","(" + ALLTRIM(shipto) + ") ") + TRIM(TEXT)
								ELSE
									m.comment = m.comment + CHR(13)+CHR(10) + IIF(EMPTY(shipto),"","(" + ALLTRIM(shipto) + ") ") + TRIM(TEXT)
								ENDIF

							ENDIF
							lcConsignee = consignee
						ENDSCAN
						
						m.consid=sqlgenpk("cons","wh")
						m.office="K"
						INSERT INTO cons FROM MEMVAR
						lnCtr = lnCtr + 1

*!*							SELECT consk
*!*							BROW

						**************************************************************************************
						**************************************************************************************
						**************************************************************************************


						.TrackProgress(TRANSFORM(lnCtr) + ' INSERTS were made from ' + lcHoldingFile,LOGIT+SENDIT)

						* if the file already exists in the archive folder, make sure it is not read-only.
						* this is to prevent errors we get copying into archive on a resend of an already-archived file.
						* Necessary because the files in the archive folders were sometimes marked as read-only (not sure why)
						llArchived = FILE(lcArchivedFile)
						IF llArchived THEN
							RUN ATTRIB -R &lcArchivedFile.
							.TrackProgress('   Used ATTRIB -R for re-archiving : ' + lcHoldingFile,LOGIT+SENDIT)
						ENDIF

						* archive the holding file
						COPY FILE (lcHoldingFile) TO (lcArchivedFile)

						* delete the holding file
						IF FILE(lcArchivedFile) THEN
							DELETE FILE (lcHoldingFile)
						ENDIF

						WAIT CLEAR

					ENDFOR && lnCurrentFile = 1 TO lnNumFiles

					
					lcFileName = 'F:\UTIL\ARIAT\REPORTS\ARIAT_CONSK_' + DTOS(.dToday) + '.XLS'
					SELECT ACCTNUM, CONSIGNEE, LEFT(COMMENT,254) AS COMMENT, ADDBY, ADDDT FROM CONS INTO CURSOR CONSK2 
					SELECT CONSK2
					COPY TO (lcFileName) XL5
					.cAttach = lcFileName
					
					IF NOT tu("CONS") THEN
						=AERROR(CONSERR)
						IF TYPE("CONSERR")#"U" THEN && and inlist(aerrq[1,1],1526,1542,1543)
							*x3winmsg(CONSERR[1,2])
							.TrackProgress('CONS TABLEUPDATE ERROR : ' + CONSERR[1,2],LOGIT+SENDIT)
							THROW
						ENDIF
						llSentFXTRIPS = .F.
					ENDIF
					
				ELSE
					.TrackProgress('Found no files in the ARIAT source folder', LOGIT+SENDIT)
				ENDIF

				.TrackProgress('Ariat Consignee Routing Import process ended normally.', LOGIT+SENDIT)

			CATCH TO loError

				.TrackProgress('There was an error.',LOGIT+SENDIT)
				.TrackProgress('The Error # is: ' + TRANSFORM(loError.ERRORNO), LOGIT+SENDIT)
				.TrackProgress('The Error Message is: ' + TRANSFORM(loError.MESSAGE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred in Program: ' + TRANSFORM(loError.PROCEDURE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred at line #: ' + TRANSFORM(loError.LINENO), LOGIT+SENDIT)
				*.TrackProgress('SPREADSHEET ROW = ' + TRANSFORM(.nRow), LOGIT+SENDIT)
				lnNumberOfErrors = lnNumberOfErrors + 1
				.lSendInternalEmailIsOn = .T.
				IF TYPE('.oExcel') = 'O' AND NOT ISNULL(.oExcel) THEN
					.oExcel.QUIT()
				ENDIF

			ENDTRY

			WAIT CLEAR
			***************** INTERNAL email results ******************************
			.TrackProgress('About to send status email.',LOGIT+SENDIT)
			.TrackProgress('The logfile is: ' + .cLogFile, LOGIT+SENDIT)
			.TrackProgress('Ariat Consignee Routing Import process started: ' + .cStartTime, LOGIT+SENDIT)
			.TrackProgress('Ariat Consignee Routing Import process finished: ' + TTOC(DATETIME()), LOGIT+SENDIT)

			IF .lSendInternalEmailIsOn THEN
				DO FORM dartmail2 WITH .cSendTo,.cFrom,.cSubject,.cCC,.cAttach,.cBodyText,"A"
				.TrackProgress('Sent status email.',LOGIT)
			ELSE
				.TrackProgress('Did not send status email: THIS.lSendInternalEmailIsOn = FALSE.',LOGIT+SENDIT)
			ENDIF

		ENDWITH
		RETURN
	ENDFUNC && main


	FUNCTION ExecSQL
		LPARAMETERS tcSQL, tcCursorName, tlNoDataReturnedIsOkay
		LOCAL llRetval, lnResult
		WITH THIS
			* close target cursor if it's open
			IF USED(tcCursorName)
				USE IN (tcCursorName)
			ENDIF
			WAIT WINDOW "Fetching remote data; please wait..." NOWAIT
			lnResult = SQLEXEC(.nSQLHandle, tcSQL, tcCursorName)
			llRetval = ( lnResult > 0 )
			IF llRetval THEN
				* see if any data came back
				IF NOT tlNoDataReturnedIsOkay THEN
					IF (NOT USED(tcCursorName)) OR EOF(tcCursorName) THEN
						llRetval = .F.
						.TrackProgress('No data was returned by this query: ' +CRLF + tcSQL, LOGIT+SENDIT)
						.cSendTo = 'mark.bennett@tollgroup.com'
					ENDIF
				ENDIF
			ELSE
				.TrackProgress("Result Code [" + TRANSFORM(lnResult) + "] returned by this query: " + tcSQL, LOGIT+SENDIT)
				.cSendTo = 'mark.bennett@tollgroup.com'
			ENDIF
			WAIT CLEAR
			RETURN llRetval
		ENDWITH
	ENDFUNC


	PROCEDURE TrackProgress
		* do any combination of Wait Window, writing to logfile, and adding to body of email,
		* based on nFlags parameter.
		LPARAMETERS tcExpression, tnFlags
		WITH THIS
			IF BITAND(tnFlags,LOGIT) = LOGIT THEN
				IF .lLoggingIsOn THEN
					?
					? .cProcessName + "  " + TTOC(DATETIME()) + ": " + tcExpression
				ENDIF
			ENDIF
			IF .lWaitWindowIsOn THEN
				IF BITAND(tnFlags,NOWAITIT) = NOWAITIT THEN
					WAIT WINDOW tcExpression NOWAIT
				ENDIF
				IF BITAND(tnFlags,WAITIT) = WAITIT THEN
					WAIT WINDOW tcExpression TIMEOUT .nWaitWindowTimeout
				ENDIF
			ENDIF
			IF BITAND(tnFlags,SENDIT) = SENDIT THEN
				IF .lSendInternalEmailIsOn THEN
					.cBodyText = .cBodyText + tcExpression + CRLF + CRLF
				ENDIF
			ENDIF
		ENDWITH
	ENDPROC  &&  TrackProgress


	PROCEDURE SortArrayByDateTime
		* expects taInArray[] to be the result of an ADIR()
		LPARAMETERS taInArray, taOutArray
		EXTERNAL ARRAY taInArray, taOutArray
		LOCAL lnRows, lnCols, i, j
		* extending taInArray[] to process the files in date/time order
		lnRows = ALEN(taInArray,1)
		lnCols = ALEN(taInArray,2)
		* create taOutArray with one more column
		DIMENSION taOutArray[lnRows,lnCols + 1]
		* and fill it with taInArray columns AND (filedate + filetime) in new column
		* ( ACOPY() doesn't preserve elements properly because of different # of columns )
		FOR i = 1 TO lnRows
			FOR j = 1 TO lnCols
				taOutArray[i,j] = taInArray[i,j]
			ENDFOR
			taOutArray[i,lnCols + 1] = DTOS(taInArray[i,3]) + "_" + taInArray[i,4]
		ENDFOR
		* sort taOutArray on date/time column
		=ASORT(taOutArray,6)
	ENDPROC

ENDDEFINE
