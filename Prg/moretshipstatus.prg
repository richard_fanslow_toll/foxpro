PARAMETERS lwebsite

PUBLIC lTesting
lTesting = .F.

IF lTesting
	dDate=DATE()-4
ELSE
	dDate = DATE()-1
ENDIF

IF !USED('edi_trigger')
	cEDIFolder = "F:\3PL\DATA\"
	USE (cEDIFolder+"edi_trigger") IN 0 ALIAS edi_trigger
ENDIF

xsqlexec("select * from scac","scac",,"wh")
index on scac tag scac		

IF !lwebsite
	xsqlexec("select * from account where inactive=0","account",,"qq")
	index on accountid tag accountid
	set order to
ENDIF

FOR osused = 1 TO 2
	cChar = IIF(osused = 1,"L","N")
	cWhse = IIF(cChar = "L","ML","NJ")
	USE ("F:\wh"+cChar+"\whdata\outship") IN 0

	SELECT cWhse AS warehouse,a.accountid,c.acctname,a.appt,;
		a.ship_ref AS pickticket,a.consignee,a.scac,;
		TRIM(b.dispname) AS ship_via,;
		IIF(EMPTY(a.del_date),.F.,.T.) AS delivered,;
		.F. AS confirmed ;
		FROM outship a,scac b,account c;
		WHERE BETWEEN(appt,dDate,DATE()) ;
		AND a.scac = b.scac ;
		AND (BETWEEN(a.accountid,5446,5453) OR INLIST(a.accountid,5633,5741,5742,5743)) ;
		AND a.accountid = c.accountid ;
		INTO CURSOR tempout&cChar READWRITE

	USE IN outship
	SET DATABASE TO wh
	CLOSE DATABASES &&closes wh database
ENDFOR

SELECT tempoutl
APPEND FROM DBF('tempoutn')
USE IN tempoutn

SELECT * ;
	FROM tempoutl ;
	ORDER BY warehouse,accountid,pickticket ;
	INTO CURSOR tempout READWRITE
USE IN tempoutl

SELECT tempout
CALCULATE MAX(LEN(ALLTRIM(pickticket))) TO nMaxPT
CALCULATE MAX(LEN(ALLTRIM(ship_via))) TO nMaxSV
CALCULATE MAX(LEN(ALLTRIM(consignee))) TO nMaxCon
CALCULATE MAX(LEN(ALLTRIM(acctname))) TO nMaxAcct

ALTER TABLE tempout ALTER COLUMN pickticket c(nMaxPT+1)
ALTER TABLE tempout ALTER COLUMN ship_via c(nMaxSV+1)
ALTER TABLE tempout ALTER COLUMN consignee c(nMaxCon+1)
ALTER TABLE tempout ALTER COLUMN acctname c(nMaxAcct+1)

SELECT tempout
LOCATE
SCAN
	cShip_ref= tempout.pickticket
	nAcctNum = tempout.accountid
	SELECT edi_trigger
	LOCATE FOR ship_ref=cShip_ref AND accountid=nAcctNum
	IF FOUND()
		IF !EMPTY(confirm_dt)
			SELECT tempout
			REPLACE confirmed WITH .T. NEXT 1 IN tempout
		ENDIF
	ENDIF
ENDSCAN

SELECT tempout
LOCATE

DO CASE
	CASE EOF() AND lwebsite
		INSERT INTO pdf_file (unique_id) VALUES (reports.unique_id)
		SELECT pdf_file
		REPLACE MEMO WITH "NO DATA"

	CASE EOF()
		=MESSAGEBOX("No data found.")

	OTHERWISE
		STORE tempfox+SUBSTR(SYS(2015), 3, 10)+".xls" TO cfile
		COPY TO &cfile TYPE XL5
		IF lwebsite
			SELECT pdf_file
			INSERT INTO pdf_file (unique_id) VALUES (reports.unique_id)
			**in order to avoid "VFPOLEDB" "provider ran out of memory" errors for large files (>2mb) no longer move file 
			**  thru linked server, just copy file directly to \\iis2\apps\CargoTrackingWeb\reports\ - mvw 03/08/13
			*append memo memo from &cfile overwrite
			copy file "&cfile" to \\iis2\apps\CargoTrackingWeb\reports\*.*
			replace memo with "FILE COPIED TO REPORT FOLDER" in pdf_file
		ENDIF
ENDCASE


USE IN edi_trigger
USE IN scac
USE IN tempout

IF !lwebsite
	USE IN account
ENDIF
