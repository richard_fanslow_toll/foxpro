*******************************************************************************
*Program    : form method SendUpdate
*Author		: Richard (Fanz) Fanslow
*        	: Toll Group
*Project    : 
*Created    : 20180124
*                            :
*Copyright  : � 2017, Toll
*Description: 
*
*20180124	Fanz - 	Adding in new heartbeat feature
*					sendupdate Pass Location = SYS(0) and AppName = SYS(16)and SQL Server name="TGFNJSQL01"
*					thisform.sendupdate(Alltrim(upper(sys(0)))Alltrim(upper(sys(16))),)
*
********************************************************************************
PARAMETERS lcLocation,lcAppName,lcServer
gnconnhandle= Sqlstringconnect("Driver=SQL Server;Server=" + lcServer + ";UID=sa;PWD=B%g23!7#$;Database=Carteret")
if gnconnhandle>0
	okproperties1= SQLSetprop(gnconnhandle, 'Displogin',3)
	okproperties2= SQLSetprop(gnconnhandle, 'asynchronous', .F.)
	lcSql = ""
	lcSql = lcSql + "exec [carteret].[dbo].[sp_TaskAudit] '" + lcLocation +"','" + lcAppName + "'"
	lcSql = lcSql + ";"
	*=STRTOFILE(lcSql,"M:\Fanz\" + SYS(3) + ".log")
	gConnected = SQLExec(gnconnhandle, lcSql)
	gConnected = SQLDisconnect(gnconnhandle)
endif

**removed disconnect(0) as this will disconnect all existing connections - mvw 02/20/18
*=sqldisconnect(0)

return
