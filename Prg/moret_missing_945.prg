**This script identifies when a 945 has not been created for a PT.  

utilsetup("MORET_MISSING_945")
Set Exclusive Off
Set Deleted On
Set Talk Off
Set Safety Off 

WITH _SCREEN	
	.AUTOCENTER = .T.
	.WINDOWSTATE = 0
	.BORDERSTYLE = 1
	.WIDTH = 320
	.HEIGHT = 210
	.TOP = 200
	.LEFT = 100
	.CLOSABLE = .T.
	.MAXBUTTON = .T.
	.MINBUTTON = .T.
	.SCROLLBARS = 0
	.CAPTION = "MORET_MISSING_945"
ENDWITH	

* removed Mod I  dy 3/29/17

*!*	If !Used("outship")
*!*	  Use F:\whi\whdata\outship.Dbf In 0
*!*	  
*!*	ENDIF
*!*	If !Used("edi_trigger")
*!*	  use f:\3pl\data\edi_trigger shared In 0
*!*	ENDIF
*!*	SET STEP ON
*!*	SELECT outship
*!*	SELECT edi_trigger
*!*	SELECT ACCOUNTID,WO_NUM,SHIP_REF FROM outship where INLIST(accountid,6313,6402,6403,6405,6406,6407,6492,6603,6648) AND  delenterdt >DATE()-20  and delenterdt<datetime()-1290;
*!*	 into CURSOR temp945 
*!*	SELECT DISTINCT ACCOUNTID,WO_NUM,SHIP_REF FROM EDI_TRIGGER WHERE EDI_TYPE='945 ' AND TRIG_TIME>date()-90 AND  INLIST(accountid,6313,6402,6403,6405,6406,6407,6492,6603,6648);
*!*	 INTO CURSOR TEMP21
*!*	SELECT * FROM temp945 t LEFT JOIN TEMP21 e ON t.accountid=e.accountid AND t.wo_num=e.wo_num AND t.ship_ref=e.ship_ref  INTO CURSOR temp22
*!*	SELECT * FROM TEMP22 WHERE ISNULL(ACCOUNTID_B) INTO CURSOR TEMP23
*!*	SELECT  TEMP23
*!*	If Reccount() > 0
*!*	  Export To "S:\moret\missing_945\moret_945_trigger_not_created"  Type Xls
*!*	  tsendto = "Maria.Estrella@Tollgroup.com,todd.margolin@tollgroup.com,paul.gaidis@tollgroup.com"
*!*	  tattach = "S:\moret\missing_945\moret_945_trigger_not_created.xls"
*!*	  tcc =""
*!*	  tFrom ="Toll/FMI EDI System Operations <fmi-transload-ops@fmiint.com>"
*!*	  tmessage = "MORET_945_trigger_not_created"+Ttoc(Datetime())
*!*	  tSubject = "MORET_945_trigger_not_created, CHECK POLLER"
*!*	  Do Form m:\dev\frm\dartmail2 With tsendto,tFrom,tSubject, tcc,tattach,tmessage  ,"A"
*!*	ELSE 
*!*	  tsendto = "tmarg@fmiint.com,Maria.Estrella@Tollgroup.com"
*!*	  tattach = ""
*!*	  tcc =""
*!*	  tFrom ="Toll/FMI EDI System Operations <fmi-transload-ops@fmiint.com>"
*!*	  tmessage = "NO_MISSING_MORET_945_triggers_exist"+Ttoc(Datetime())
*!*	  tSubject = "NO_MISSING_MORET_945_triggers_exist"
*!*	  Do Form m:\dev\frm\dartmail2 With tsendto,tFrom,tSubject, tcc,tattach,tmessage  ,"A"
*!*	ENDIF

*!*	SELECT  * FROM EDI_TRIGGER WHERE EDI_TYPE='945 ' AND TRIG_TIME>date()-90 AND EMPTY(when_proc) AND INLIST(accountid,6313,6402,6403,6405,6406,6407,6492,6603,6648);
*!*	 INTO CURSOR TEMP21
*!*	If Reccount() > 0
*!*	  Export To "S:\moret\missing_945\moret_945_trigger_created_not_processed"  Type Xls

*!*	  tsendto = "Maria.Estrella@Tollgroup.com,todd.margolin@tollgroup.com,paul.gaidis@tollgroup.com"
*!*	  tattach = "S:\moret\missing_945\moret_945_trigger_created_not_processed.xls"
*!*	  tcc =""
*!*	  tFrom ="Toll/FMI EDI System Operations <fmi-transload-ops@fmiint.com>"
*!*	  tmessage = "MORET_945_trigger_created_not_processed"+Ttoc(Datetime())
*!*	  tSubject = "MORET_945_trigger_created_not_processed"
*!*	  Do Form m:\dev\frm\dartmail2 With tsendto,tFrom,tSubject, tcc,tattach,tmessage  ,"A"
*!*	ELSE 
*!*	  tsendto = "tmarg@fmiint.com,Maria.Estrella@Tollgroup.com"
*!*	  tattach = ""
*!*	  tcc =""
*!*	  tFrom ="Toll/FMI EDI System Operations <fmi-transload-ops@fmiint.com>"
*!*	  tmessage = "NO_unprocessed_945s_for_MORET"+Ttoc(Datetime())
*!*	  tSubject = "NO_unprocessed_945s_for_MORET"
*!*	  Do Form m:\dev\frm\dartmail2 With tsendto,tFrom,tSubject, tcc,tattach,tmessage  ,"A"
*!*	ENDIF


*!*	SELECT * FROM EDI_TRIGGER WHERE EDI_TYPE='945 '  AND  INLIST(accountid,6313,6402,6403,6405,6406,6407,6492,6603,6648);
*!*	 AND errorflag INTO CURSOR TEMP21
*!*	SELECT  TEMP21
*!*	If Reccount() > 0
*!*	  Export To "S:\moret\missing_945\moret_trigger_error"  Type Xls
*!*	  tsendto = "Maria.Estrella@Tollgroup.com,todd.margolin@tollgroup.com,paul.gaidis@tollgroup.com"
*!*	  tattach = "S:\moret\missing_945\moret_trigger_error.xls"
*!*	  tcc =""
*!*	  tFrom ="Toll/FMI EDI System Operations <fmi-transload-ops@fmiint.com>"
*!*	  tmessage = "MORET_945_trigger_error"+Ttoc(Datetime())
*!*	  tSubject = "MORET_945_trigger_error"
*!*	  Do Form m:\dev\frm\dartmail2 With tsendto,tFrom,tSubject, tcc,tattach,tmessage  ,"A"
*!*	ELSE 
*!*	  tsendto = "tmarg@fmiint.com,Maria.Estrella@Tollgroup.com"
*!*	  tattach = ""
*!*	  tcc =""
*!*	  tFrom ="Toll/FMI EDI System Operations <fmi-transload-ops@fmiint.com>"
*!*	  tmessage = "NO_MORET_trigger_errors"+Ttoc(Datetime())
*!*	  tSubject = "NO_MORET_trigger_errors"
*!*	  Do Form m:\dev\frm\dartmail2 With tsendto,tFrom,tSubject, tcc,tattach,tmessage  ,"A"
*!*	ENDIF

*!*	*************************************START del_date populated, missing BOL

goffice="Y"

if usesqloutwo()
else
	If !Used("outship")
	  Use F:\whY\whdata\outship.Dbf In 0
	endif
endif

Wait window at 10,10 "Performing the first query...." nowait

if usesqloutwo()
	xsqlexec("select wo_num, ship_ref, del_date from outship where mod='"+goffice+"' " + ;
		"and inlist(accountid,6313,6402,6403,6405,6406,6407,6492,6603,6648) " + ;
		"and del_date>{"+dtoc(date()-90)+"} and empty(bol_no)","temp21",,"wh")
else
	SELECT wo_num, ship_ref,bol_no, del_date  from  outship where  inlist(accountid,6313,6402,6403,6405,6406,6407,6492,6603,6648);
	 AND del_date>date()-90  and EMPTY(bol_no) INTO CURSOR temp21
endif

SELECT  TEMP21
If Reccount() > 0
  Export To "S:\moret\missing_945\moret_empty_bol"  Type Xls
  tsendto = "Maria.Estrella@Tollgroup.com,todd.margolin@tollgroup.com,paul.gaidis@tollgroup.com"
  tattach = "S:\moret\missing_945\moret_empty_bol.xls"
  tcc =""
  tFrom ="Toll/FMI EDI System Operations <fmi-transload-ops@fmiint.com>"
  tmessage = "MORET SHIPPED PT MISSING BOL"+Ttoc(Datetime())
  tSubject = "MORET SHIPPED PT MISSING BOL error"
  Do Form m:\dev\frm\dartmail2 With tsendto,tFrom,tSubject, tcc,tattach,tmessage  ,"A"
ELSE 
  tsendto = "tmarg@fmiint.com,Maria.Estrella@Tollgroup.com"
  tattach = ""
  tcc =""
  tFrom ="Toll/FMI EDI System Operations <fmi-transload-ops@fmiint.com>"
  tmessage = "NO_MORET SHIPPED PT MISSING BOL"+Ttoc(Datetime())
  tSubject = "NO_MORET SHIPPED PT MISSING BOL"
  Do Form m:\dev\frm\dartmail2 With tsendto,tFrom,tSubject, tcc,tattach,tmessage  ,"A"
ENDIF		
*************************************END del_date populated, missing BOL
Close Data All
schedupdate()
_Screen.Caption=gscreencaption