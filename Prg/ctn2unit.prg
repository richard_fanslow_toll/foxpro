* converts cartons into units
* called from hhlocchgmj, hhlocchgmjwrist, ppdropariat, pppulllxe, pprep, whlocchglxe

lparameters xaccountid, xstyle, xcolor, xid, xpack, xctnqty, xfromloc, xtoloc, xfrom, xloctable, xoutwonum, xdropid

xdyctnqty=xctnqty	&& trying to fix strange problem dy 3/27/15

if empty(xfrom)
	xfrom=""
endif
if empty(xoutwonum)
	xoutwonum=0
endif
if empty(xdropid)
	xdropid=0
endif

**for troubleshooting multiple evtries into ADJ - mvw 07/08/15
if xfrom="PPDROP"
	info(xfrom,,,xaccountid,,,,,,"Ctn2Unit from "+xfrom,,xstyle,xcolor,xid,xpack,xctnqty)
endif
 
do case
case xfrom="INWO"
	xindet="vindet"
	xinloc="vinloc"
	xc2ufilter="transform(units)+style+color+id+pack = transform(.f.)+xstyle+xcolor+xid+xpack"
otherwise
	xindet="xc2uindet"
	xinloc="xc2uinloc"
	xc2ufilter="!units and pack='"+xpack+"'"

	indetfill(xaccountid, , xstyle, xcolor, xid, xpack, .t., , .t., .t., "remainqty>0")		&& creates a cursor
	select xc2uindet
	index on indetid tag indetid
	select xc2uinloc
	index on inlocid tag inlocid
endcase

xorigctnqty=xdyctnqty

m.accountid=xaccountid
m.adjdt=qdate()

if xfrom="REPLENISHMENT TO SLOTTING OVERFLOW"
	m.comment=xfrom
else
	m.comment="P&P CARTONS CONVERTED TO UNITS - "+xfrom
endif

select (xindet)
scan for remainqty>0 and xdyctnqty>0 and &xc2ufilter
	xzzindetid=&xindet..indetid

	select (xinloc)
	scan for indetid=&xindet..indetid and whseloc=xfromloc and remainqty>0 and xdyctnqty>0
		xqqinlocid=&xinloc..inlocid
		xqqindetid=&xindet..indetid

		xuseqty=min(xdyctnqty,&xinloc..remainqty)
		xdyctnqty=xdyctnqty-xuseqty

		xwo_num=&xindet..wo_num
		xpo=&xindet..po
		xindettotqty=&xindet..totqty

		gnocalc=.t.
		m.offset=.f.
		m.units=.f.
		m.style=xstyle
		m.color=xcolor
		m.id=xid
		m.pack=xpack
		m.totqty=-xuseqty
		m.removeqty=0
		m.remainqty=m.totqty
		m.inadj=(m.totqty>0)
		m.whseloc=xfromloc
		m.addby=''
		m.adddt={}
		
		insertinto("adj","wh",.t.)		&& carton removal
		xadjid=adj.adjid
		
		gnocalc=.f.

		if xfrom#"INWO"
			xonhold=invenadj(,,,"wo_num="+transform(xwo_num)+" and po='"+xpo+"'")
		endif

		select (xindet)
		scan for units and wo_num=xwo_num and po=xpo
			xflowindetid=&xindet..indetid
			xflowqty=xuseqty/xindettotqty*&xindet..totqty

			scatter memvar
			if xloctable
				if seek(m.style+m.color+m.id,"loctable","match")
					xtoloc=loctable.whseloc
				else
					xtoloc="FLOWU"
				endif
			endif

			m.offset=.t.
			m.totqty=-xflowqty
			m.removeqty=0
			m.remainqty=m.totqty
			m.inadj=(m.totqty>0)
			m.whseloc="RACK"
			m.addby=""
			m.adddt={}
			m.totcube=0

			insertinto("adj","wh",.t.)		&& unit removal from RACK
			xadjid=adj.adjid

			if xfrom#"INWO"
				invenadj(,,,"wo_num="+transform(xwo_num)+" and po='"+xpo+"'")
			endif

			m.totqty=xflowqty
			m.removeqty=0
			m.remainqty=m.totqty
			m.inadj=(m.totqty>0)
			m.whseloc=xtoloc
			m.addby=""
			m.adddt={}
			m.totcube=0

			insertinto("adj","wh",.t.)		&& unit add into slot location
			xadjid=adj.adjid

			invenadj(,,(inlist(xtoloc,"BSTAT","PSTAT")),"wo_num="+transform(xwo_num)+" and po='"+xpo+"'")
			
			=seek(xflowindetid,xindet,"indetid")
		endscan
		=seek(xqqinlocid,xinloc,"inlocid")
		=seek(xqqindetid,xindet,"indetid")
	endscan
	=seek(xzzindetid,xindet,"indetid")
endscan

if xdyctnqty>0
	set step on 
*	if xfrom#"INWO"
*		if seek(STR(xaccountid,4)+".F."+xstyle+xcolor+xid+xpack,"vinven","match")
*			fixinven()
*		endif
*	endif
*	if goffice#"K"	&& dy 5/11/15 strange error there's no problem but xdyctnqty is not zero
*		xbody=transform(xdyctnqty)+crlf+transform(xorigctnqty)+crlf+transform(xaccountid)+crlf+whstyle(,xstyle,xcolor,xid,xpack)+crlf+xfromloc+crlf+xtoloc+crlf+transform(xdropid)
*		email("dyoung@fmiint.com","P&P location change mismatch from "+xfrom,xbody,,,,.t.,,,,,.t.,,.t.)
*		return .f.
*	endif
endif
