* pass in KRONOS SQL, returns and open cursor of the results.
LPARAMETERS tcSQL, tcCursorName

IF EMPTY(tcSQL) OR EMPTY(tcCursorName) THEN
	WAIT WINDOW TIMEOUT 5 "You must pass 2 parameters!"
	RETURN
ENDIF

LOCAL lnSQLHandle, lnResult

* get ss#s from ADP
OPEN DATABASE F:\UTIL\KRONOS\KRONOSEDI
lnSQLHandle = SQLCONNECT("WTK_ON_PAYROLL2","sa","kr0n1tes")

IF lnSQLHandle > 0 THEN

	IF USED(tcCursorName) THEN
		USE IN (tcCursorName)
	ENDIF

	lnResult = SQLEXEC(lnSQLHandle, tcSQL, tcCursorName)
	IF ( lnResult > 0 ) THEN
		* see if any data came back
		IF (NOT USED(tcCursorName)) OR EOF(tcCursorName) THEN
			WAIT WINDOW TIMEOUT 5 'No data was returned by this query'
		ENDIF
	ELSE
		WAIT WINDOW TIMEOUT 5  "Result Code [" + TRANSFORM(lnResult) + "] returned by this query"
	ENDIF
ELSE
	WAIT WINDOW TIMEOUT 5 'Unable to connect to Kronos!'
ENDIF  &&  lnSQLHandle > 0

CLOSE DATABASES 

RETURN
