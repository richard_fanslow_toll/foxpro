lparameter xtime, xnumeric

if emptynul(xtime)
	return space(8)
endif

xhh=left(xtime,2)
xmm=substr(xtime,3,2)
xampm=upper(substr(xtime,5,1))

if !between(xhh,"01","12") or !between(xmm,"00","59") or !inlist(xampm,"A","P")
	return space(8)
endif

if xhh="12"
	xhh=transform(val(xhh)-12,"@L 99")
endif
if xampm="P"
	xhh=trans(val(xhh)+12,"@L 99")
endif

if !xnumeric
	return xhh+":"+xmm+":00"
else
	return val(xhh+transform(val(xmm)/60*100,"@L 99.99"))
endif
