lparameter xshowmiami

xonactmsg=.f.

create cursor xtemp ( ;
	accountid i, ;
	billtoid i, ;
	company c(1), ;
	docnum c(6), ;
	invnum c(5), ;
	docdt d, ;
	origdocdt d, ;
	doctype c(7), ;
	wo_num n(7), ;
	amt1 n(12,2), ;
	amt2 n(12,2), ;
	amt3 n(12,2), ;
	amt4 n(12,2), ;
	amt5 n(12,2), ;
	amt6 n(12,2), ;
	amt7 n(12,2), ;
	amt8 n(12,2), ;
	amt9 n(12,2), ;
	amt10 n(12,2), ;
	paid n(12,2), ;
	balance n(10,2), ;
	onactcomp c(1))

select xrpt
scan
	scatter memvar
	if isdigit(invnum) or accountid=5837
		if xsqlexec("select * from invoice where invnum='"+invnum+"'","invoice",,"ar")
			xtotamt1=amt1
			xtotamt2=amt2
			xtotamt3=amt3
			xtotamt4=amt4
			xtotamt5=amt5
			xtotamt6=amt6
			xtotamt7=amt7
			xtotamt8=amt8
			xtotamt9=amt9
			xtotamt10=amt10
			xtotpaid=paid
			store 0 to xcurramt1, xcurramt2, xcurramt3, xcurramt4, xcurramt5, xcurramt6, xcurramt7, xcurramt8, xcurramt9, xcurramt10, xcurrpaid

			tinvoiceid=invoice.invoiceid
			requery("vinvdet")
			select vinvdet
			count for !main and invdetamt#0 to xtotrecs
			i=0
			scan for !main and invdetamt#0
				i=i+1
				m.company=company
				
				if i#xtotrecs
					m.amt1=round(invdetamt/invoice.invamt*xtotamt1,2)
					m.amt2=round(invdetamt/invoice.invamt*xtotamt2,2)
					m.amt3=round(invdetamt/invoice.invamt*xtotamt3,2)
					m.amt4=round(invdetamt/invoice.invamt*xtotamt4,2)
					m.amt5=round(invdetamt/invoice.invamt*xtotamt5,2)
					m.amt6=round(invdetamt/invoice.invamt*xtotamt6,2)
					m.amt7=round(invdetamt/invoice.invamt*xtotamt7,2)
					m.amt8=round(invdetamt/invoice.invamt*xtotamt8,2)
					m.amt9=round(invdetamt/invoice.invamt*xtotamt9,2)
					m.amt10=round(invdetamt/invoice.invamt*xtotamt10,2)
					m.paid=round(invdetamt/invoice.invamt*xtotpaid,2)
					xcurramt1=xcurramt1+m.amt1
					xcurramt2=xcurramt2+m.amt2
					xcurramt3=xcurramt3+m.amt3
					xcurramt4=xcurramt4+m.amt4
					xcurramt5=xcurramt5+m.amt5
					xcurramt6=xcurramt6+m.amt6
					xcurramt7=xcurramt7+m.amt7
					xcurramt8=xcurramt8+m.amt8
					xcurramt9=xcurramt9+m.amt9
					xcurramt10=xcurramt10+m.amt10
					xcurrpaid=xcurrpaid+m.paid
					insert into xtemp from memvar
				else
					m.amt1=xtotamt1-xcurramt1
					m.amt2=xtotamt2-xcurramt2
					m.amt3=xtotamt3-xcurramt3
					m.amt4=xtotamt4-xcurramt4
					m.amt5=xtotamt5-xcurramt5
					m.amt6=xtotamt6-xcurramt6
					m.amt7=xtotamt7-xcurramt7
					m.amt8=xtotamt8-xcurramt8
					m.amt9=xtotamt9-xcurramt9
					m.amt10=xtotamt10-xcurramt10
					m.paid=xtotpaid-xcurrpaid
					insert into xtemp from memvar
				endif						
			endscan
		else
			if !empty(onactcomp)
				m.company=onactcomp
			else
				m.company="F"
				if docnum#"Z" and !xonactmsg
					xonactmsg=.t.
*					x3winmsg([Some On Account transactions don't specify a company - Go to "Maintenance - On Account Transactions".])
				endif
			endif
			insert into xtemp from memvar
		endif
	else
		if !empty(onactcomp)
			m.company=onactcomp
		else
			m.company="F"
			if docnum#"Z" and !xonactmsg
				xonactmsg=.t.
*					x3winmsg([Some On Account transactions don't specify a company - Go to "Maintenance - On Account Transactions".])
			endif
		endif
		insert into xtemp from memvar
	endif
endscan	
