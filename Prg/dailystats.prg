**prtDailyStats.prg
**
**prinet the conveyor daily stat sheet
**
Parameters lAuto, nSysId, cSysDesc, dStartDt, dEndDt, lSlug, cAcctFilter, cAcctName, nFormat, lEmail, cEmailTo

If lAuto
  USE f:\transload\tldata\barcodes In 0
  USE f:\transload\tldata\bcSlug In 0
EndIf

**get column no. of scanTm field for order of csrBarcodes
=AFields(aFieldList, "barcodes")
nFieldCol = 1
For nCol = 1 to Alen(aFieldList, 1)
  Store Iif(Upper(aFieldList[nCol,1]) = "SCANTM", nCol, nFieldCol) to nFieldCol
EndFor
*****

cFieldCol = Alltrim(Str(nFieldCol))
tStartTm = Dtot(dStartDt)
tEndTm = Dtot(dEndDt+1)

*!*	Select b.*, Ttod(scanTm) as scanDt, Hour(scanTm) as scanHr from barcodes b ;
*!*			left join detail d on b.detailId = d.detailId ;
*!*		  where scanTm >= tStartTm and scanTm < tEndTm and sysId = nSysId &cAcctFilter and ;
*!*				!duplicate and !manualScan ;
*!*		order by &cFieldCol ;
*!*	  into cursor csrBarcodes

cSlug = ""
If lSlug = .t.
  cSlug = "union select b.*, Ttod(scanTm) as scanDt, Hour(scanTm) as scanHr, Round(Hour(scantm)+Minute(scantm)/60, 2) as scanMin ;"+;
		  "from bcSlug b left join detail d on b.detailId = d.detailId ;"+;
	      "where scanTm >= tStartTm and scanTm < tEndTm and sysId = nSysId &cAcctFilter "
EndIf

If Empty(cAcctFilter)
	Select b.*, Ttod(scanTm) as scanDt, Hour(scantm) as scanHr, Round(Hour(scantm)+Minute(scantm)/60, 2) as scanMin ;
			from barcodes b ;
		  where scanTm >= tStartTm and scanTm < tEndTm and sysId = nSysId and ;
				!duplicate and !(manualScan and !noRead) ;
		&cSlug ; &&if slugged cartons are to be included
		order by &cFieldCol ;
	  into cursor csrBarcodes
Else
	cAcctFilter = "and "+cAcctFilter
	Select b.*, Ttod(scanTm) as scanDt, Hour(scantm) as scanHr, Round(Hour(scantm)+Minute(scantm)/60, 2) as scanMin ;
			from barcodes b left join detail d on b.detailId = d.detailId ;
		  where scanTm >= tStartTm and scanTm < tEndTm and sysId = nSysId &cAcctFilter and ;
				!duplicate and !(manualScan and !noRead) ;
		&cSlug ; &&if slugged cartons are to be included
		order by &cFieldCol ;
	  into cursor csrBarcodes
EndIf

Select *, cnt(1) as bcCnt from csrBarcodes group by scanDt, scanHr into cursor csrBarcodes

If Reccount("csrBarcodes") = 0
	If !lAuto
	  strMsg = "Nothing to report..."
	  =MESSAGEBOX(strMsg, 0, gcMsgTitle)
	Else
	  Close Databases all
	EndIf

	Return
EndIf


*****Start Report Writing
SET MARGIN TO 0

lsTimeString = "Run Date: "+Dtoc(Date())+"  Run Time: "+Ttoc(Datetime(),2)+"     "+chr(13)+chr(13)

Store "" TO lsCompanyName

lsReportName = "Daily Conveyor Statistics"
lsGroupByString = lsReportName+Iif(Empty(cAcctFilter), ": All Accounts", ": "+cAcctName)+Chr(13)+;
	"Note: Numbers shown are a snapshot of the hour prior."

CREATE CURSOR c_Report (cr_SortBy I, cr_AcctName C(50), cr_GroupBy C(250), ;
						cr_CO_Name C(50), cr_RptName C(200), ;
						cr_FooterName C(200), cr_Header M, cr_Details M)

If nFormat = 1
	lsHeader = chr(13)+Space(18)+"1:00    2:00    3:00    4:00    5:00    6:00    7:00    8:00    9:00   10:00   11:00   12:00"

	**insert header info
	INSERT INTO c_Report (cr_SortBy, cr_GroupBy, cr_AcctName, cr_Header, ;
						  cr_CO_Name, cr_RptName, cr_FooterName, cr_Details) ;
		   VALUES (0, lsTimeString+lsGroupByString, "", lsHeader, lsCompanyName, ;
				   lsReportName, lsGroupByString, "")

	**insert detail records
	Select csrBarcodes

set step on 


	Locate
	DO while !Eof()
		lsDetails = Chr(13)+Dtoc(scanDt)+" AM:  "
		dScanDt = scanDt
		For nHour = 0 to 23
		  lsDetails = lsDetails+Space(2)+Iif(scanHr = nHour, Str(bcCnt,6,0), Str(0,6,0))
		  lsDetails = lsDetails+Iif(nHour # 11, "", Chr(13)+Space(9)+"PM:  ")
		  If scanDt = dScanDt and scanHr = nHour
			Skip
		  EndIf
		EndFor
		
		lsDetails = lsDetails
		Insert INTO c_Report (cr_SortBy, cr_GroupBy, cr_AcctName, cr_CO_Name, ;
							  cr_RptName, cr_FooterName, cr_Header, cr_Details) ;
			   VALUES (1, lsTimeString+lsGroupByString, "", lsCompanyName, ;
					   lsReportName, lsGroupByString, lsHeader, lsDetails)
	EndDo
Else
	DO case
	  Case nSysId = 1000
		cFilter1 = "Between(scanMin, 7, 15.99)" &&7AM (7) - 3:59PM (15.99)
		**ugliest so everything in between1 & 3 = 2
**		cFilter2 = "scanMin >= 16 or scanMin <= 0.49" &&4PM (16) - 11:59PM (infinity) or 12AM (0) - 12.29AM (0.49)
		cFilter3 = "Between(scanMin, .50, 6.99)" &&12:30AM (0.50) - 6:59AM (6.99)

	  Case nSysId = 1008
		cFilter1 = "Between(scanMin, 7, 15.99)" &&7AM (7) - 3:59PM (15.99)
		cFilter3 = ".f." &&no 3rd shift
	EndCase

	Select sum(bcCnt) as bcCnt, scanDt, Iif(&cFilter1, 1, Iif(&cFilter3, 3, 2)) as shift ;
		from csrBarcodes group by scanDt, shift into cursor csrBarcodes

	lsHeader = chr(13)+Space(19)+"    Shift 1       Shift 2       Shift 3       Daily Total"
	**insert header info
	INSERT INTO c_Report (cr_SortBy, cr_GroupBy, cr_AcctName, cr_Header, ;
						  cr_CO_Name, cr_RptName, cr_FooterName, cr_Details) ;
		   VALUES (0, lsTimeString+lsGroupByString, "", lsHeader, lsCompanyName, ;
				   lsReportName, lsGroupByString, "")

	**insert detail records
	Select csrBarcodes
	Locate

	DO while !Eof()
		lsDetails = Chr(13)+Dtoc(scanDt)+":"+Space(13)
		dScanDt = scanDt
		nDailyTot = 0
		For nShift = 1 to 3
		  lsDetails = lsDetails+Iif(shift = nShift, Str(bcCnt,6,0), Str(0,6,0))+Space(8)
		  nDailyTot = nDailyTot + Iif(shift = nShift, bcCnt, 0)
		  Skip Iif(scanDt = dScanDt and shift = nShift, 1, 0)
		EndFor

		lsDetails = lsDetails+Space(2)+Str(nDailyTot,6,0)
		Insert INTO c_Report (cr_SortBy, cr_GroupBy, cr_AcctName, cr_CO_Name, ;
							  cr_RptName, cr_FooterName, cr_Header, cr_Details) ;
			   VALUES (1, lsTimeString+lsGroupByString, "", lsCompanyName, ;
					   lsReportName, lsGroupByString, lsHeader, lsDetails)
	EndDo
EndIf


&&print report
SELECT c_Report

If !lAuto
	Set Printer to default
	REPORT FORM PORTRAIT PREVIEW
EndIf


If lEmail
	cOutFileName = "DailyStats"
	lFileCreated = .f.
	DO frxToPdf with .t., "portrait", "h:\pdf\", cOutFileName

	If !lFileCreated
		If !lAuto
			strMsg = "PDF was not created. File will not be sent."
			=MessageBox(strMsg, 16, gcMsgTitle)
		EndIf
	Else
		cTo = cEmailTo
		cCc = ' '
		cSubject = "Daily Sorter Stats: "+Dtoc(dStartDt)+"-"+Dtoc(dEndDt)+" ("+cSysDesc+")"
		cAttach = "h:\pdf\"+cOutFileName
		cFrom ="TGF WMS Operations <fmicorporate@fmiint.com>"
		cMessage = cSubject
		DO FORM dartmail WITH cTo,cFrom,cSubject,cCc,cAttach,cMessage,Iif(lAuto, "A", "")
	EndIf
EndIf

If lAuto
  Close Databases all
EndIf
