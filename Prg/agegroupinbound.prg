PARAMETERS cOffice
CLOSE DATA ALL
PUBLIC lTesting,lTestImport,lBrowfiles,NormalExit,tfrom,tsendto,tcc,cMessage,nAcct_Num,cUseFolder,goffice,cArchivefilename
PUBLIC cfilename,lDupFile,cDivErrMessage,lDivErrNotice,tsendtoerr,tccerr,tsendtotest,tcctest,lDoHeader,tattach

lTesting = .F.
lTestImport = lTesting
lOverridebusy = lTesting
lBrowfiles = lTesting
lDupFile = .F.
* lBrowfiles = .t.
lOverridebusy = lTesting
lOverridebusy = .T.

DO m:\dev\prg\_setvars WITH lTesting

*_SCREEN.WINDOWSTATE = IIF(lTesting,2,1)
_SCREEN.CAPTION = "Age Group Inbound"
nAcct_Num = 1285
NormalExit = .F.
cDivErrMessage = ""
tattach = ""

cMailname = "Age Group"
STORE "" TO cMessage,tsendto,tcc,tsendtotest,tcctest,tsendtoerr,tccerr

TRY
	tfrom = "Toll EDI Operations <toll-edi-ops@tollgroup.com>"
	IF VARTYPE(cOffice) = "L"
		cOffice = "Y"
	ENDIF
	goffice = cOffice
	lcErrorMessage = ""

	IF !lTesting
		SELECT 0
		USE F:\edirouting\ftpsetup SHARED
		cTransfer = "PL-AGEGROUP-"
		LOCATE FOR ftpsetup.transfer = cTransfer AND ftpsetup.TYPE = "GET"
		IF ftpsetup.chkbusy AND !lOverridebusy
			ASSERT .F. MESSAGE "At chkbusy flag in FTPSETUP"
			WAIT WINDOW AT 10,10 "File in Use...Will try later" TIMEOUT 2
			NormalExit = .T.
			THROW
		ENDIF
		REPLACE chkbusy WITH .T. FOR ftpsetup.transfer = cTransfer AND ftpsetup.TYPE = "GET" IN ftpsetup
		USE IN ftpsetup
	ENDIF

	IF lTestImport
		WAIT WINDOW "This is a TEST import into WHP" TIMEOUT 2
		cUseFolder = "F:\WHP\WHDATA\"
	ELSE
		xReturn = "XXX"
		DO m:\dev\prg\wf_alt WITH cOffice,nAcct_Num
		cUseFolder = UPPER(xReturn)
	ENDIF

*	ASSERT .F. MESSAGE "At mail population"
	SELECT 0
	USE F:\3pl\DATA\mailmaster ALIAS mm SHARED
	LOCATE FOR mm.edi_type = "PL" AND mm.accountid = nAcct_Num AND mm.office = cOffice
	IF FOUND()
		STORE TRIM(mm.acctname) TO cAccountName
		STORE TRIM(mm.basepath) TO lcPath
		STORE TRIM(mm.archpath) TO lcArchivePath
		STORE mm.testflag TO lTest
		STORE TRIM(IIF(mm.use_alt,mm.sendtoalt,mm.sendto)) TO tsendto
		STORE TRIM(IIF(mm.use_alt,mm.ccalt,mm.cc)) TO tcc
		LOCATE
		LOCATE FOR mm.edi_type = "MISC" AND mm.taskname = "GENERAL"
		STORE TRIM(IIF(mm.use_alt,mm.sendtoalt,mm.sendto)) TO tsendtotest
		STORE TRIM(IIF(mm.use_alt,mm.ccalt,mm.cc)) TO tcctest
		STORE TRIM(IIF(mm.use_alt,mm.sendtoalt,mm.sendto)) TO tsendtoerr
		STORE TRIM(IIF(mm.use_alt,mm.ccalt,mm.cc)) TO tccerr
		USE IN mm
	ELSE
		WAIT WINDOW AT 10,10  "No parameters set for this acct# "+ALLTRIM(STR(nAcct_Num))+"  ---> Office "+cOffice TIMEOUT 2
		NormalExit = .T.
		THROW
	ENDIF

	IF lTesting
		STORE tsendtotest TO tsendto,tsendtoerr
		STORE tcctest TO tcc,tccerr
		lcPath = lcPath + "\Test\"
	ENDIF

*!*		xsqlexec("select * from inwolog where 
*!*		USE (cUseFolder+"INWOLOG") IN 0 ALIAS inwolog
*!*		SELECT * FROM inwolog WHERE .F. INTO CURSOR xinwolog READWRITE
*!*		USE (cUseFolder+"PL") IN 0 ALIAS pl
*!*		SELECT * FROM pl WHERE .F. INTO CURSOR xpl READWRITE
*!*		USE (cUseFolder+"whgenpk") IN 0 ALIAS whgenpk
*!*		USE F:\sysdata\qqdata\account IN 0 ALIAS account
*!*		USE F:\wo\wodata\wolog IN 0 ALIAS wolog
*!*		USE F:\wh2\whdata\inrcv IN 0 ALIAS inrcv
*!*		IF lTesting
		SELECT inwolog
		DELETE FOR accountid = 1285
		SELECT pl
		DELETE FOR accountid = 1285
	ENDIF

	SELECT 0
	USE F:\3pl\DATA\agegroupinbdtemplate ALIAS template1
	SELECT * FROM template1 WHERE .F. INTO CURSOR aginbd READWRITE
	USE IN template1

	CD &lcPath
	DELETE FILE *.DBF
	IF ADIR(ary1,"*.xls") = 0
		WAIT WINDOW "There are no files to process...exiting" TIMEOUT 2
		NormalExit = .T.
		THROW
	ENDIF

	len1 = ALEN(ary1,1)
	WAIT WINDOW "There are "+TRANSFORM(len1)+" PL files to process" TIMEOUT 1

*	ASSERT .F. MESSAGE "At start of file looping...DEBUG"
	FOR i = 1 TO len1
		xfile = lcPath+ALLTRIM(ary1[i,1])
		cfilename = ALLTRIM(ary1[i,1])
		WAIT WINDOW "File is "+cfilename TIMEOUT 2
		cArchivefilename = lcArchivePath+cfilename
		COPY FILE [&xfile] TO [&cArchiveFilename]
		lcRunFile = "c:\tempfox\excelrun.xls"
		oExcel = CREATEOBJECT("Excel.Application")
		oExcel.DisplayAlerts = .F.
		oWorkbook = oExcel.Workbooks.OPEN(xfile,,.F.)
		nSheets = oWorkbook.Worksheets.COUNT
		SET SAFETY OFF
		FOR jj = 1 TO 1
			oWorksheet = oWorkbook.Worksheets[jj]
			cWSName = UPPER(ALLTRIM(oWorksheet.NAME))
			WAIT WINDOW "Worksheet: "+cWSName TIMEOUT 1
			oWorkbook2 = oExcel.Workbooks.ADD
			oWorksheet.COPY(oWorkbook2.ActiveSheet)
			oWorkbook2.Worksheets[2].DELETE
			oWorkbook2.SAVEAS(lcRunFile,39)
			oWorkbook2.CLOSE(.T.)
			RELEASE oWorkbook2
		ENDFOR
		oWorkbook.CLOSE(.T.)
		oExcel.QUIT()
		RELEASE oExcel

		SELECT 0
		CREATE CURSOR tempctrs (CONTAINER c(25),STYLE c(20),wo_num c(10))
		SELECT tempctrs
		INDEX ON CONTAINER TAG CONTAINER

		SELECT aginbd
		APPEND FROM [&lcRunFile] TYPE XL8
		IF FILE(lcRunFile)
			DELETE FILE [&lcRunFile]
		ENDIF
		DELETE FOR (EMPTY(ALLTRIM(aginbd.acct_ref)) OR UPPER(aginbd.acct_ref) = "SHIPMENT #")
		REPLACE aginbd.COLOR WITH LEFT(ALLTRIM(aginbd.fullcolor),AT(" ",aginbd.fullcolor,1)-1) ALL
		REPLACE aginbd.ID WITH LEFT(ALLTRIM(aginbd.pactype),5) ALL
		IF lTesting
			BROWSE
			SET STEP ON
		ENDIF

		REPLACE aginbd.cPACK WITH TRANSFORM(aginbd.unitsqty/aginbd.ctnqty) ALL
		LOCATE
		IF lTesting
*			BROWSE
		ENDIF

		SELECT CONTAINER,COUNT(aginbd.CONTAINER) FROM aginbd GROUP BY 1 INTO CURSOR multictr
		lMultipleCtr = IIF(RECCOUNT()>1,.T.,.F.)
		SELECT CONTAINER,COUNT(aginbd.CONTAINER) AS ctrcount FROM aginbd GROUP BY 1 INTO CURSOR ctrmultiple HAVING ctrcount>1
		lCtrMultilines = IIF(RECCOUNT()>1,.T.,.F.)

		SELECT aginbd
		LOCATE
		cCtrname = ""
		m.addby = "TGF-PROC"
		m.adddt = DATETIME()
		m.addproc = "AGINBD2"
		m.wo_date = DATE()
		m.date_rcvd = DATE()
		m.accountid = nAcct_Num
		m.acctname = "AGE GROUP INC."
		m.inwologid = 0
		m.plid = 0
		nPO = 0
*		ASSERT .F. MESSAGE "At AGINBD scan"
		lDivErrNotice = .F.

		SCAN
			SCATTER MEMVAR
			IF cCtrname = ALLTRIM(m.container)
				lDoHeader = .F.
			ELSE
				STORE ALLTRIM(m.container) TO cCtrname
				lDoHeader = .T.
				m.inwologid = m.inwologid+1
			ENDIF

			IF aginbd.unitsqty = 0 OR aginbd.ctnqty = 0
				cMessage = "Either the units or carton qty is wrong"
				cMessage = cMessage+CHR(13)+"This is usually due to a missing or offset column in the Excel sheet."
				cMessage = cMessage+CHR(13)+"Retrieve file from Archive folder to re-run."
				DELETE FILE [&xfile]
				THROW
			ENDIF

*!* Added style concatenation per Maria, 07.17.2012
			m.consignee = ALLTRIM(m.consignee)
			m.consignee = IIF(UPPER(m.consignee)="*N","",m.consignee)
			m.style = ALLTRIM(m.style)+m.consignee
*!* End of added code

			IF MOD(aginbd.unitsqty,aginbd.ctnqty)>0
				lDivErrNotice = .T.
				INSERT INTO tempctrs (CONTAINER,STYLE) VALUES (ALLTRIM(m.container),ALLTRIM(m.style))
			ENDIF

			IF lDoHeader
				m.comments = "FILE*"+cfilename
				m.comments = m.comments+CHR(13)+"ETADATE*"+ALLTRIM(m.eta)
				INSERT INTO xinwolog FROM MEMVAR
			ENDIF
			REPLACE xinwolog.plunitsinqty WITH xinwolog.plunitsinqty+m.unitsqty IN xinwolog
			REPLACE xinwolog.plinqty WITH xinwolog.plinqty+m.ctnqty IN xinwolog
			nPO = nPO+1
			m.po = TRANSFORM(nPO)
			m.plid = m.plid+1
			m.echo = "DESC*"+ALLTRIM(m.desc)+CHR(13)+"FULLCOLOR*"+ALLTRIM(m.fullcolor)
			m.units = .F.
			m.pack = aginbd.cPACK
			m.totqty = m.ctnqty
			INSERT INTO xpl FROM MEMVAR
			IF cOffice # "C"
				m.plid = m.plid+1
				m.units = .T.
				m.totqty = m.unitsqty
				m.pack = '1'
				INSERT INTO xpl FROM MEMVAR
			ENDIF
		ENDSCAN

		SELECT xpl
		IF lTesting
			WAIT WINDOW "At duplicate check code...debug" TIMEOUT 1
*			SET STEP ON
		ENDIF
		IF !lMultipleCtr
			IF lCtrMultilines
			ELSE
				SELECT STYLE,COLOR,ID,PACK,1 AS CNT FROM xpl WHERE !units INTO CURSOR xpl1
				SELECT STYLE,COLOR,ID,PACK,SUM(CNT) AS cnt1 FROM xpl1 GROUP BY 1,2,3,4 INTO CURSOR xpl2
				LOCATE
				IF lTesting
					BROWSE FOR cnt1 > 1
					SET STEP ON
				ENDIF
				SCAN FOR cnt1 > 1
					SCATTER MEMVAR
					SELECT xpl
					LOCATE FOR xpl.STYLE = m.style AND xpl.PACK = m.pack AND xpl.COLOR = m.color AND xpl.ID = m.id AND !units
					IF EOF()
						EXIT
					ENDIF
					nRec = RECNO()
					nCtns = 0
					nUnits = 0
					SCAN FOR xpl.STYLE = m.style AND xpl.COLOR = m.color AND xpl.ID = m.id AND xpl.PACK = m.pack AND !units AND RECNO() > nRec AND !DELETED()
						nCtns = nCtns + xpl.totqty
*!*					SKIP 1 IN xpl
*!*					nUnits = nUnits + xpl.totqty
*!*					SKIP -1 IN xpl
*!*					DELETE NEXT 2 IN xpl
						DELETE NEXT 1 IN xpl
					ENDSCAN
					SELECT xpl
					GO nRec
					REPLACE xpl.totqty WITH xpl.totqty+nCtns IN xpl
*!*				SKIP 1 IN xpl
*!*				REPLACE xpl.totqty WITH xpl.totqty+nUnits IN xpl
				ENDSCAN
			ENDIF
		ENDIF

		SELECT xpl
		rollup()

		IF lBrowfiles OR lTesting
			SELECT xinwolog
			LOCATE
			BROWSE
			SELECT xpl
			LOCATE
			BROWSE
		ENDIF
*		CANCEL

		WAIT WINDOW AT 10,10 "Age Group Inbound: Creating the work order(s)." NOWAIT
		ASSERT .F. MESSAGE "In INWOLOG/PL insertion scan"
		cPLString = PADR("WO#",10)+PADR("CONTAINER",20)+PADR("CTNS",10)+"UNITS"

		SELECT xinwolog
		LOCATE
		SCAN
			STORE 0 TO nCartons,nUnits
			STORE xinwolog.inwologid TO oldiwid
			SCATTER MEMVAR MEMO
			WAIT WINDOW "Now checking for duplicate inbounds..." NOWAIT NOCLEAR
			SELECT inwolog
			LOCATE FOR inwolog.accountid = nAcct_Num AND inwolog.CONTAINER = ALLTRIM(m.container) AND inwolog.acct_ref = m.acct_ref
			IF FOUND() AND inwolog.wo_date > DATE()-5
				ASSERT .F. MESSAGE "In pre-existing WO"
				SET STEP ON
				cFoundWO = ALLTRIM(STR(inwolog.wo_num))
				WAIT CLEAR
				cErrString = "Acct: "+TRANSFORM(nAcct_Num)+", AWB/Ctr: "+IIF(EMPTY(m.container),ALLTRIM(m.reference),ALLTRIM(m.container))+;
					", Acct Ref. "+ALLTRIM(m.acct_ref)+" already exists in WO# "+cFoundWO+"."
				WAIT WINDOW cErrString TIMEOUT 3
				IF !lTesting
					DELETE FILE [&xfile]
				ENDIF
				lDupFile = .T.
				IF EMPTY(lcErrorMessage)
					lcErrorMessage = cErrString
				ELSE
					lcErrorMessage = lcErrorMessage + CHR(10) + CHR(10) + cErrString
				ENDIF
				NormalExit = .F.
				THROW
			ENDIF
			SELECT xinwolog
			WAIT CLEAR
			nCartons = nCartons+m.plinqty
			nUnits = nUnits+plunitsinqty

			m.inwologid = sqlgenpk("inwologid","wh")
			m.inwologid1 = m.inwologid

			m.wo_num1 = dygenpk("wonum","wh"+goffice)

			m.refer_num=whrefnum(m.accountid)
			STORE m.wo_num1 TO m.wo_num
			INSERT INTO inwolog FROM MEMVAR

			SELECT xpl
			LOCATE
			SCAN FOR xpl.inwologid = oldiwid
				SCATTER FIELDS EXCEPT plid MEMVAR MEMO
				m.wo_num = m.wo_num1
				m.inwologid = m.inwologid1
				m.po      = UPPER(ALLTRIM(xpl.po))
				m.style   = UPPER(ALLTRIM(xpl.STYLE))
				m.pack    = ALLTRIM(xpl.PACK)
				m.totqty  = xpl.totqty
				m.plinqty = xpl.totqty
				m.plopenqty = m.plinqty
				m.plid = sqlgenpk("pl","wh")
				INSERT INTO pl FROM MEMVAR
			ENDSCAN
*			SET STEP ON
			cPLString = cPLString+CHR(13)+PADR(TRANSFORM(m.wo_num1),10)+PADR(ALLTRIM(m.container),20)+PADR(TRANSFORM(nCartons),10)+TRANSFORM(nUnits)
			SELECT xinwolog
		ENDSCAN
		SELECT inwolog
		LOCATE
		SCAN FOR inwolog.accountid = nAcct_Num AND inwolog.wo_date = DATE()
			IF SEEK(ALLTRIM(inwolog.CONTAINER),'tempctrs','container')
				REPLACE tempctrs.wo_num WITH TRANSFORM(inwolog.wo_num) IN tempctrs
			ENDIF
		ENDSCAN

*!* Added below 01.23.2015 per Darren
		SELECT inwolog
		STORE "" TO xtruckwonum, xtruckseal
		DO "m:\dev\prg\whtruckwonum.prg" WITH 0, inwolog.REFERENCE, inwolog.CONTAINER, inwolog.accountid
		REPLACE inwolog.truckwonum WITH xtruckwonum, inwolog.seal WITH xtruckseal IN inwolog

		lnormalexit = .T.
		SELECT tempctrs
		LOCATE
		IF !EOF()
			DivErrmail()
		ENDIF
		goodmail()

		IF !lTesting
			SELECT 0
			USE F:\edirouting\ftpsetup SHARED
			cTransfer = ("PL-AGEGROUP-"+IIF(cOffice = "C","CA","NJ"))
			LOCATE FOR ftpsetup.transfer = cTransfer AND ftpsetup.TYPE = "GET"
			REPLACE chkbusy WITH .F. FOR ftpsetup.transfer = cTransfer AND ftpsetup.TYPE = "GET" IN ftpsetup
			USE IN ftpsetup
		ENDIF

		IF FILE(cArchivefilename) AND !lTesting
			DELETE FILE [&xfile]
		ENDIF
		WAIT WINDOW "All files finished processing...exiting" TIMEOUT 2
	ENDFOR

CATCH TO oErr
	IF NormalExit = .F.
		SET STEP ON
		tsubject = cMailname+" PL Upload Error ("+TRANSFORM(oErr.ERRORNO)+") at "+TTOC(DATETIME())
		tattach  = ""
		tsendto  = tsendtoerr
		tcc = tccerr
		tmessage = cMailname+" PL Upload Error..... Please fix me........!"
		lcSourceMachine = SYS(0)
		lcSourceProgram = SYS(16)

		tmessage =tmessage+ "Try/CATCH Exeception Message:"+CHR(13)+;
			[  Error: ] + STR(oErr.ERRORNO) +CHR(13)+;
			[  LineNo: ] + STR(oErr.LINENO) +CHR(13)+;
			[  Message: ] + oErr.MESSAGE +CHR(13)+;
			[  Procedure: ] + oErr.PROCEDURE +CHR(13)+;
			[  Details: ] + oErr.DETAILS +CHR(13)+;
			[  StackLevel: ] + STR(oErr.STACKLEVEL) +CHR(13)+;
			[  LineContents: ] + oErr.LINECONTENTS+CHR(13)+;
			[  UserValue: ] + oErr.USERVALUE+CHR(13)+;
			[  Computer:  ] +lcSourceMachine+CHR(13)+;
			[  940 file:  ] +cfilename+CHR(13)+;
			[  Program:   ] +lcSourceProgram

		IF !EMPTY(cMessage)
			tmessage = tmessage+CHR(13)+"Filename: "+cfilename+CHR(13)+cMessage+CHR(13)+CHR(13)
		ENDIF

		IF lDupFile AND !lTesting
			SET STEP ON
			SELECT 0
			USE F:\edirouting\ftpsetup SHARED
			cTransfer = ("PL-AGEGROUP-"+IIF(cOffice = "C","CA","NJ"))
			LOCATE FOR ftpsetup.transfer = cTransfer AND ftpsetup.TYPE = "GET"
			REPLACE chkbusy WITH .F. FOR ftpsetup.transfer = cTransfer AND ftpsetup.TYPE = "GET" IN ftpsetup
			USE IN ftpsetup

			IF lTesting
				SELECT 0
				USE F:\3pl\DATA\mailmaster ALIAS mm SHARED
				LOCATE FOR edi_type = "MISC" AND taskname = "AGINBDDUP"
				STORE TRIM(IIF(mm.use_alt,mm.sendtoalt,mm.sendto)) TO tsendto
				STORE TRIM(IIF(mm.use_alt,mm.ccalt,mm.cc)) TO tcc
			ELSE
				tsendto = tsendtoerr
				tcc = tccerr
			ENDIF
			tmessage = tmessage+CHR(13)+lcErrorMessage+CHR(13)+CHR(13)
			cWhseLoc = ICASE(cOffice = "C","CA",cOffice = "N","NJ",cOffice = "M","FL","ML")
			tsubject = "Inbound WO Duplication ("+cWhseLoc+")- Age Group at "+TTOC(DATETIME())+"  in file "+cfilename
		ENDIF
		DO FORM m:\dev\FRM\dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
	ELSE
		WAIT WINDOW AT 10,10 "Normal Exit " TIMEOUT 2
	ENDIF
FINALLY
	ON ERROR
	CLOSE DATABASES ALL
ENDTRY

********************
PROCEDURE goodmail
********************
	SET STEP ON
	tsubject = cMailname+" PL File Upload at "+TTOC(DATETIME())+IIF(lTesting," *TEST*","")
	tattach = ""
	tmessage = cMailname+" PL File: "+cfilename+" uploaded, containing the following:"
	tmessage = tmessage+CHR(13)+cPLString
	DO FORM m:\dev\FRM\dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
ENDPROC

********************
PROCEDURE DivErrmail
********************
	cDivErrMessage = PADR("WO#",15)+PADR("CONTAINER",20)+"STYLE"
	SELECT tempctrs
	SCAN
		SCATTER MEMVAR
		cDivErrMessage = cDivErrMessage+CHR(13)+PADR(m.wo_num,15)+PADR(ALLTRIM(m.container),20)+ALLTRIM(M.style)+CHR(13)
	ENDSCAN
*	SET STEP ON
	IF lTesting
		tsendtox = tsendtotest
		tccx = tcctest
	ELSE
		SELECT 0
		USE F:\3pl\DATA\mailmaster ALIAS mm
		LOCATE FOR accountid = nAcct_Num AND taskname = "AGINBDPACK"
		STORE TRIM(IIF(mm.use_alt,mm.sendtoalt,mm.sendto)) TO tsendtox
		STORE TRIM(IIF(mm.use_alt,mm.ccalt,mm.cc)) TO tccx
		USE IN mm
	ENDIF
	tsubject = cMailname+" Pack Division Error at "+TTOC(DATETIME())
	tattach = ""
	tmessage = cMailname+" packing list "+cfilename+" contained the following pack division errors:"
	tmessage = tmessage+CHR(13)+cDivErrMessage
	DO FORM m:\dev\FRM\dartmail2 WITH tsendtox,tfrom,tsubject,tccx,tattach,tmessage,"A"
	RELEASE cDivErrMessage
ENDPROC

*************************
PROCEDURE rollup
*************************
*!* Rolls up post-creation XPL data by style/color/id/pack. Modified from Darren's code.

	SELECT plid FROM xpl WHERE units GROUP BY po HAVING COUNT(plid)>1 INTO CURSOR xmusical      && don't merge units if there is a musical in the pl

	IF RECCOUNT()=0
		SELECT xpl
		SCAN FOR !units
			xxplid=plid
			xpo=po
			SCATTER MEMVAR
			DO WHILE .T.
				LOCATE FOR units=m.units AND STYLE=m.style AND COLOR=m.color AND ID=m.id AND PACK=m.pack AND plid#m.plid
				IF !FOUND()
					EXIT
				ENDIF
				xduptotqty=totqty
				xdelpo=po
				DELETE
				LOCATE FOR plid=xxplid
				REPLACE totqty WITH totqty+xduptotqty
				LOCATE FOR po=xdelpo
				xduptotqty=totqty
				DELETE
				LOCATE FOR po=xpo AND units
				REPLACE totqty WITH totqty+xduptotqty
			ENDDO
			LOCATE FOR plid=xxplid
		ENDSCAN
	ENDIF

ENDPROC
