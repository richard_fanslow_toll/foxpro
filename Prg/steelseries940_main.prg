*!* m:\dev\prg\steelseries940_main.prg
PARAMETERS cOfficeIn

PUBLIC nAcctNum,cDelimiter,nRun_num,cTranslateOption,cWhichFile,cOffice,cCustname,cPropername,cStorenum,lOK,tsendtopt,tccpt
PUBLIC cFilter,nRunID,lComplete,lTesting,m.UCCNUMBER,lEmail,lTestImport,emailcommentstr,cUseFolder,lBrowfiles,cMailName,cMod
PUBLIC ptid,ptctr,ptqty,xfile,cfilename,nUploadCount,pickticket_num_start,pickticket_num_end,nFileCount,units,cUCCNumber,cISA_Num
PUBLIC lcPath,lcArchivepath,tsendto,tcc,NormalExit,lTestmail,LogCommentStr,lPick,lPrepack,tsendtoerr,tccerr,lImported,lLoadSQL
PUBLIC m.addby,m.adddt,m.addproc,cTransfer,tfrom,tattach,thisfile,tsendtotest,tcctest,lNextDay,lMonday,cForeignStr,lTestPTHIST
PUBLIC cErrMsg,dFileDate,nFileSize
Public llEmailTest

PUBLIC ARRAY a856(1)
CLOSE DATABASES ALL
NormalExit = .F.
ON ERROR THROW

cMailName="Steel Series 940 Upload "
tsendto  = "pgaidis@fmiint.com"
tsendtoerr = tsendto
tcc = "pgaidis@fmiint.com"
tccerr = tcc
cMailName = "Steel Series"
nAcctNum  = 6612

_SCREEN.CAPTION = cMailName

llEmailTest=.t.  && uese the new Dartmail

cErrMsg="Non-specific: In program body"

IF VARTYPE(cOfficeIn) # "C"
cOfficeIn ="Y"
endif
cOffice = cOfficeIn
cMod = "Y" &&cOffice 	&& steelxyz

llEmailTest = .t.
   
TRY
	lTesting = .f.
	lTestPTHIST = .f.
	lTestImport = lTesting
	lTestmail = lTesting
	lOverridebusy = lTesting
	lBrowfiles = lTesting
	lOverridebusy = lTesting
*lOverridebusy = .t.
	lBrowfiles = .f.

	lLoadSQL = .t.

	DO m:\dev\prg\_setvars WITH lTesting

	cCustname = "STEELSERIES"
	cMailName = "Steel Series"
	nAcctNum  = 6612

	ASSERT .F. MESSAGE "At start of Steel Series 940 processing"
	SELECT 0
	USE F:\edirouting\ftpsetup SHARED
	cTransfer = "940-STEELSERIES-C2"
	IF !lTesting
		LOCATE FOR ftpsetup.transfer = cTransfer AND ftpsetup.TYPE = "GET"
		IF ftpsetup.chkbusy AND !lOverridebusy
			WAIT WINDOW "Process is flagged busy...returning" TIMEOUT 1
			NormalExit = .T.
			THROW
		ELSE
			REPLACE chkbusy WITH .T.,trig_time WITH DATETIME() ;
				FOR ftpsetup.TYPE = "GET" ;
				AND ftpsetup.transfer = cTransfer ;
				IN ftpsetup
			USE IN ftpsetup
		ENDIF
	ENDIF

	lEmail = .T.
	LogCommentStr = ""


	tfrom = "Toll EDI Operations <toll-edi-ops@tollgroup.com>"
	STORE " " TO tattach,tsendto,tcc


	SELECT 0
	USE F:\3pl\DATA\mailmaster ALIAS mm SHARED
	LOCATE FOR mm.edi_type = "940" AND mm.accountid = nAcctNum AND mm.office = cOffice
	IF FOUND()
		STORE TRIM(mm.acctname) TO cCustname
		STORE TRIM(mm.basepath) TO lcPath
		STORE TRIM(mm.archpath) TO lcArchivepath
		_SCREEN.CAPTION = ALLT(mm.scaption)
*!*			STORE mm.testflag 	   TO lTest
		STORE TRIM(IIF(mm.use_alt,mm.sendtoalt,mm.sendto)) TO tsendto
		STORE TRIM(IIF(mm.use_alt,mm.ccalt,mm.cc)) TO tcc
		LOCATE
		LOCATE FOR mm.edi_type = "MISC" AND mm.taskname = "GENERAL"
		STORE TRIM(IIF(mm.use_alt,mm.sendtoalt,mm.sendto)) TO tsendtotest
		STORE TRIM(IIF(mm.use_alt,mm.ccalt,mm.cc)) TO tcctest
		tsendtoerr = tsendtotest
		tccerr = tcctest
		USE IN mm
	ELSE
		USE IN mm
		WAIT WINDOW AT 10,10  "No parameters set for this acct# "+ALLTRIM(STR(nAcctnum))+"  ---> Office "+cOffice TIMEOUT 2
		RELEASE ALL
		NormalExit = .F.
		THROW
	ENDIF

	lcPath = IIF(lTesting,STRTRAN(UPPER(lcPath),"PROD","UAT"),lcPath)
	lcArchivepath = IIF(lTesting,STRTRAN(UPPER(lcArchivepath),"PROD","UAT"),lcArchivepath)
	cUseName = "STEEL SERIES"
	cPropername = PROPER(cUseName)

	_SCREEN.CAPTION = cPropername+" 940 Process"
	_SCREEN.WINDOWSTATE = IIF(lTesting OR lOverridebusy,2,1)
	CLEAR
	WAIT WINDOW "Now setting up "+cPropername+" 940 process..." TIMEOUT 2

	DO m:\dev\prg\createx856a
	SELECT x856

	cDelimiter = "*"
	cTranslateOption = "TILDE"

	cfile = ""
	cForeignStr = ""

	dXdate1 = ""
	dXxdate2 = DATE()
	nFileCount = 0

	nRun_num = 999
	lnRunID = nRun_num

	IF lTesting
		cUseFolder = "F:\WHP\WHDATA\"
	ELSE
		xReturn="XXX"
		wf(cOffice,nAcctNum)
		cUseFolder=xReturn
	ENDIF

	DO ("m:\dev\PRG\STEELSERIES940_PROCESS")

	IF !lTesting
		SELECT 0
		USE F:\edirouting\ftpsetup SHARED
		REPLACE chkbusy WITH .F. ;
			FOR ftpsetup.TYPE = "GET" ;
			AND ftpsetup.transfer = "940-STEELSERIES-C2" ;
			IN ftpsetup
	ENDIF

	NormalExit = .T.

CATCH TO oErr
	IF NormalExit = .F.
		ASSERT .F. MESSAGE "In Catch section..."
		SET STEP ON
		tsubject = cMailName+" 940 Upload Error ("+TRANSFORM(oErr.ERRORNO)+") at "+TTOC(DATETIME())
		tsendto  = tsendtoerr
		tcc = tccerr
		tmessage = cMailName+" 940 Upload Error..... Please fix me........!"
		lcSourceMachine = SYS(0)
		lcSourceProgram = "from merkury940 group"
		tmessage =tmessage+ "Try/CATCH Exeception Message:"+CHR(13)+;
			[  Error: ] + STR(oErr.ERRORNO) +CHR(13)+;
			[  LineNo: ] + STR(oErr.LINENO) +CHR(13)+;
			[  Message: ] + oErr.MESSAGE +CHR(13)+;
			[  Procedure: ] + oErr.PROCEDURE +CHR(13)

		IF !EMPTY(cErrMsg)
			tmessage =tmessage+CHR(13)+"Error found: "+cErrMsg
		ELSE
			tmessage =tmessage+[  Details: ] + oErr.DETAILS +CHR(13)+;
				[  StackLevel: ] + STR(oErr.STACKLEVEL) +CHR(13)+;
				[  LineContents: ] + oErr.LINECONTENTS+CHR(13)+;
				[  UserValue: ] + oErr.USERVALUE+CHR(13)+;
				[  Computer:  ] +lcSourceMachine+CHR(13)+;
				[  940 file:  ] +xfile+CHR(13)+;
				[  Program:   ] +lcSourceProgram
		ENDIF

		DO FORM m:\dev\frm\dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
*****added TMarg   08/17/17
	IF !lTesting
		SELECT 0
		USE F:\edirouting\ftpsetup SHARED
		REPLACE chkbusy WITH .F. ;
			FOR ftpsetup.TYPE = "GET" ;
			AND ftpsetup.transfer = "940-STEELSERIES-KY" ;
			IN ftpsetup
	ENDIF
	ELSE
		WAIT WINDOW AT 10,10 "Normal Exit " TIMEOUT 2
	ENDIF
FINALLY
	ON ERROR
	SET STATUS BAR ON
	CLOSE DATABASES ALL
ENDTRY
