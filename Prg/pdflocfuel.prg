with &xsheet

.name="Local Trucking FSC"
.shapes.AddPicture("f:\watusi\xls\toll.bmp",.f.,.t.,5,3,130,36)
.range("A1:D1").Columns.AutoFit

try
.pagesetup.topmargin = .2
.pagesetup.bottommargin = .2
.pagesetup.leftmargin = 2
.pagesetup.rightmargin = .2
catch
endtry

.columns[1].columnwidth=25
.columns[2].columnwidth=15
.columns[3].columnwidth=12
.range("a1").rowheight=27
.range("a2").rowheight=20
.range("a3:f3").merge()

endwith

exceltext("a3","LOCAL TRUCKING FUEL SURCHARGE","bold center","18")
exceltext("c3","DOE Fuel Price","bold center middle","10")
exceltext("d3","FSC Per Mile","bold center middle","10")

excelline("b5:c5","bottom")

exceltext("b5","DOE Fuel Price","bold center middle")
exceltext("c5","FSC","bold center middle")

xline=5

select locfuel
scan 
	xline=xline+1
	excelline("b"+transform(xline),"left")
	excelline("c"+transform(xline),"left")
	excelline("d"+transform(xline),"left")

	exceltext("b"+transform(xline),transform(pricefrom,"9.99")+" to "+transform(priceto,"9.999"),"bold center",,"10")
	exceltext("c"+transform(xline),transform(fsc)+"%","bold center","10")
endscan

excelline("b"+transform(xline+1)+":c"+transform(xline+1),"top")

exceltext("a"+transform(xline+2),"NOTE: For every $0.10 increase above the $4.299 per gallon price, a corresponding")
exceltext("a"+transform(xline+3),"            increase of 1% is added to the Fuel Surcharge Rate")
exceltext("a"+transform(xline+5),"NOTE: Local Trucking Fuel Surcharges will be based upon the DOE average for the area")
exceltext("a"+transform(xline+6),"            in which the work is performed.")
exceltext("b"+transform(xline+8),"San Pedro, CA")
exceltext("c"+transform(xline+8),"California PADD")
exceltext("b"+transform(xline+9),"Carteret, NJ")
exceltext("c"+transform(xline+9),"Central Atlantic PADD")
exceltext("b"+transform(xline+10),"Miami, FL")
exceltext("c"+transform(xline+10),"Lower Atlantic PADD")
exceltext("a"+transform(xline+18),"  ISSUED BY: TOLL GLOBAL FORWARDING SCS (USA) INC. 800 FEDERAL BLVD, CARTERET NJ 07008",,8)
