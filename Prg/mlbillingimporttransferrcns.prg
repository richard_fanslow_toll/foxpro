* import Serec transfers from Joni to support ML billing

CLOSE DATABASES ALL

SET DELETED ON

LOCAL oExcel, oWorkbook, lTestMode, lcTRANSFERTable, oWorksheet, lnRow, lcRow, lcString, lcString2, lcString3, lnDelCtr
lnDelCtr = 0

*SET DECIMALS TO 0

lTestMode = .F.

lcTRANSFERTable = 'F:\SEARS\PMDATA\TRANSFERS.DBF'

USE (lcTRANSFERTable) IN 0 ALIAS TRANSFERS

SELECT TRANSFERS
BROWSE 

oExcel = CREATEOBJECT("excel.application")
oExcel.displayalerts = .F.
oExcel.visible = .F.
oWorkbook = oExcel.workbooks.OPEN("F:\UTIL\MLAUTOBILLING\SERECTRANSFERS\Soft Homes Transfer from Serec.xlsx")
oWorksheet = oWorkbook.Worksheets[1]

FOR lnRow = 4 TO 200
	lcRow = ALLTRIM(STR(lnRow ))
	
	m.RCN = oWorksheet.RANGE("C" + lcRow).VALUE	
	m.COMPLETED = oWorksheet.RANGE("I" + lcRow).VALUE	

	IF ISNULL(m.COMPLETED) THEN
		m.COMPLETED = CTOD('')
	ENDIF
	
	IF ISNULL(m.RCN) OR EMPTY(m.RCN) THEN
		* cannot proceed
		EXIT FOR
	ELSE	
		m.RCN = INT(m.RCN)
		SELECT TRANSFERS
		LOCATE FOR (RCN == m.RCN) 
		IF NOT FOUND() THEN
			INSERT INTO TRANSFERS FROM MEMVAR
			WAIT WINDOW TIMEOUT 2 "Row " + lcRow + ": inserted " + TRANSFORM(m.RCN) + " " + TRANSFORM(m.COMPLETED)				
		ENDIF	
	ENDIF
ENDFOR

SELECT TRANSFERS
BROWSE 

IF TYPE('oExcel') = 'O' AND NOT ISNULL(oExcel) THEN
	oExcel.QUIT()
ENDIF

CLOSE DATABASES ALL

RETURN
