lparameters xxtable, xsystemmodule

if empty(xsystemmodule)
	xsystemmodule=gsystemmodule
else
	xsystemmodule=upper(xsystemmodule)
endif

if !used("systables")
	use f:\common\systables in 0
endif

if !seek(padr(upper(xxtable),25)+xsystemmodule,"systables","tablesys")
	email("Dyoung@fmiint.com","INFO: table not found in systables",,,,,.t.,,,,,.t.,,.t.)
endif

return systables.remote
