**This scriptwill identify files not FTP'd successfully
utilsetup("2XU_FTP_FILES_CONTROL")
Set Exclusive Off
Set Deleted On
Set Talk Off
Set Safety Off 
WITH _SCREEN	
	.AUTOCENTER = .T.
	.WINDOWSTATE = 0
	.BORDERSTYLE = 1
	.WIDTH = 320
	.HEIGHT = 210
	.TOP = 290
	.LEFT = 110
	.CLOSABLE = .T.
	.MAXBUTTON = .t.
	.MINBUTTON = .T.
	.SCROLLBARS = 0
	.CAPTION = "2XU_FTP_FILES_CONTROL"
ENDWITH	


If !Used("edi_trigger")
  use f:\3pl\data\edi_trigger shared In 0
ENDIF

*xsqlexec("select * from ftplog where dd>={"+dtoc(DATE()-10)+"}",,,"wh")

If !Used("ftplog")
  use f:\wh\ftplog shared In 0
ENDIF
SELECT * FROM ftplog WHERE dd>=DATE()-10 INTO CURSOR a2xuftplog
USE IN ftplog

SELECT edi_trigger
SELECT file945, SPACE(30) as trig945 FROM edi_trigger where INLIST(accountid,6665) AND edi_type='945 ' AND WHEN_proc > DATE()-4;
	and WHEN_proc<datetime()-4000  into cursor lookup945 readwrite

*xsqlexec("select file945, cast('' as char(30)) as trig945 from edi_trigger where accountid=6665 " + ;
	"and edi_type='945 ' and when_proc>{"+dtoc(date()-4)+"}	and when_proc<{"+ttoc(datetime()-4000)+"}","lookup945",,"stuff")

USE IN edi_trigger


SELECT  lookup945
scan
pos1=ATC('945c',file945)
replace trig945 with substr(file945,pos1,36) 
ENDSCAN


SET STEP ON 
select distinct trig945, SPACE(5) as found from lookup945 where !EMPTY(trig945) into cursor lookup945_2 READWRITE 
replace trig945 WITH LOWER(trig945) FOR !EMPTY(trig945) IN lookup945_2
*!*	COPY FILE \\app1\fmisys\FTPLogs\*.txt TO \\app1\fmisys\FTPLogs\copy\ftpcopybcny.txt
*!*	CREATE CURSOR ftplogtmb (logdata C(200))
*!*	APPE from \\app1\fmisys\FTPLogs\copy\ftpcopybcny.txt TYPE DELI
SET STEP ON 
Select *, SPACE(40) as file945  FROM a2xuftplog WHERE 'Sent file' $logdata AND '2xu\945' $logdata AND '.txt successfully' $logdata  INTO CURSOR log945a READWRITE
scan
pos1=ATC('945out',logdata)
replace file945 with substr(logdata,pos1+7,22) 
ENDSCAN


SELECT lookup945_2
SCAN 
SELECT log945a
LOCATE FOR file945=lookup945_2.trig945 
IF FOUND('log945a')
replace found WITH 'YES' IN LOOKUP945_2
ELSE
ENDIF
ENDSCAN

SELECT* FROM LOOKUP945_2 where found !='YES' INTO CURSOR missing945 readwrite
*!*DELETE file \\app1\fmisys\FTPLogs\copy\ftpcopybcny.txt
SET STEP ON 

SELECT missing945

If Reccount() > 0
  Export To "S:\2XU\missing_945\945_FTP_controls"  Type Xls
  tsendto = "todd.margolin@tollgroup.com"
  tattach = "S:\2XU\missing_945\945_FTP_controls.xls"
  tcc =""
  tFrom ="Toll EDI System Operations <fmi-transload-ops@fmiint.com>"
  tmessage = "2XU missing 945s from FTPlog "+Ttoc(Datetime())
  tSubject = "2XU missing 945s from FTPlog"
  Do Form m:\dev\frm\dartmail2 With tsendto,tFrom,tSubject, tcc,tattach,tmessage  ,"A"
ELSE 
  tsendto = "tmarg@fmiint.com"
  tattach = ""
  tcc =""
  tFrom ="Toll EDI System Operations <fmi-transload-ops@fmiint.com>"
  tmessage = "NO_2XU missing successful 945 FTP "+Ttoc(Datetime())
  tSubject = "NO_2XU missing successful 945 FTP_exist"
  Do Form m:\dev\frm\dartmail2 With tsendto,tFrom,tSubject, tcc,tattach,tmessage  ,"A"
ENDIF



Close Data All
schedupdate()
_Screen.Caption=gscreencaption