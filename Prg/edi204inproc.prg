
***********************************************************************************************************
* Process LOAD TENDERS LIKE BigLots & Federated-Macy's EDI 204s.
*
* 204 = 'Motor Carrier Load Tender'
*
* will need to generate EDI 990s - 'Response to Load Tender' from LOADTENDER screen, when user Accepts or Declines
*
* Build EXE as F:\UTIL\LOADTENDER\EDI204INPROC.EXE
*
* now supports multiple clients via parameter 2/13/12 MB  -  "BIGLOTS" OR "VF"
*
* Added support for VF 4/22/13 MB
* 
* Made Big-Lots specific wo and stops data tables to keep them separate from the Macy's & Vanity Fair data 5/16/13 MB
*
* We began receiving Macy's 997s in the 204 folder on 6/2/2015. 
* Since we don't use them, I modified the code to immediately copy them to the archived folder. MB 6/3/2015
*
***********************************************************************************************************
LPARAMETERS tcClient

runack("EDI204INPROC")

LOCAL loEdi204ImportProcess, lnError, lcProcessName, llTestMode

llTestMode = .F.

IF NOT llTestMode THEN
	utilsetup("EDI204INPROC")
ENDIF

DECLARE INTEGER CreateMutex IN Win32API ;
	STRING @lpMutexAttributes, ;
	STRING bInitialOwner, ;
	STRING @lpName

DECLARE INTEGER GetLastError IN Win32API

* if running in EXE check to see if already running
IF _VFP.STARTMODE > 0 THEN
	lcProcessName = "EDI204INPROC"
	CreateMutex(0,CHR(1),@lcProcessName)
	lnError = GetLastError()
	IF lnError = 183
		WAIT WINDOW TIMEOUT 30 "EDI 204 Import process is already running..."
		IF NOT llTestMode THEN
			schedupdate()
		ENDIF
		RETURN .F.
	ENDIF
ENDIF

*!*	WAIT WINDOW TIMEOUT 600 "in temporary wait state..."

_SCREEN.Caption = "EDI204INPROC"

loEdi204ImportProcess = CREATEOBJECT('Edi204ImportProcess')
loEdi204ImportProcess.MAIN( tcClient, llTestMode )

IF NOT llTestMode THEN
	schedupdate()
ENDIF

CLOSE DATABASES ALL


RETURN

#DEFINE LOGIT 1
#DEFINE WAITIT 2
#DEFINE NOWAITIT 4
#DEFINE SENDIT 8
#DEFINE CR CHR(13)
#DEFINE LF CHR(10)
#DEFINE CRLF CHR(13) + CHR(10)
#DEFINE RETURN_DATA_MANDATORY .F.
#DEFINE RETURN_DATA_NOT_MANDATORY .T.
#DEFINE CSVFORMAT 6
#DEFINE WRITE_ONLY_UNBUFFERED 11
#DEFINE COMMA ","

DEFINE CLASS Edi204ImportProcess AS CUSTOM

	cProcessName = 'Edi204ImportProcess'

	lTestMode = .F.  && .F. FOR NORMAL OPERATION, .T. TO SUPPRESS ALL BUT DEVELOPER EMAILS AND CHANGE INPUT TABLE, ETC.

	lAutoYield = .F.

	* date time props
	dToday = DATE()

	cStartTime = TTOC(DATETIME())

	* file/folder properties
	cInputFolder = ''
	cArchiveFolder = ''
	cHoldFolder = ''
	cInputFileSpec = ''

	* table properties
	* these are working tables that are always used regardless of TEST vs LIVE mode.
	
	*c204ProcTripsTable = 'F:\UTIL\LOADTENDER\DATA\TRIPS204'  && old table no longer used 5/16/13 MB
	c204ProcHeaderTable = 'F:\UTIL\LOADTENDER\DATA\WOLOG204'
	c204ProcStopsTable = 'F:\UTIL\LOADTENDER\DATA\STOPS204'
	c204ProcControlTable = 'F:\UTIL\LOADTENDER\DATA\CNTRL204'

	* THESE ARE THE FINAL DESTINATION TABLES, BUT THEY WILL BE POPULATED BY THE 204 FORM THAT RENEE WILL USE...
	* F:\EXPRESS\FXDATA\FXWOLOG
	* F:\EXPRESS\FXDATA\FXTRIPS

	* processing properties
	cEDIType = "999"
	cThrowDescription = ''
	nOutRecs = 0
	cShipIDList = "Ship ID's processed:"
	nFilesProcessed = 0
	nAccountID = 0
	cAcctname = ""
	n204Count = 0
	lExpress = .T.  && if false it means it is local trucking
	nNewWoNum = 0  && this will be populated if we need to create a Trucking Work Order
	n990FileHandle = 0
	nNumFiles = 0
	lMadeA990 = .F.
	cReplacedStatus = ''
	nReplacedWO_Num = 0
	cReplacedFILE990 = ''
	dReplacedRESPDATE = CTOD('')
	cReplacedRESPTIME = SPACE(5)
	lReplace204 = .F.

	* wait window properties
	nWaitWindowTimeout = 2
	lWaitWindowIsOn = .T.

	* logfile properties
	lLoggingIsOn = .T.
	cLogFile = ''

	* INTERNAL email properties
	lSendInternalEmailIsOn = .T.
	cFrom = 'TGF Corporate <tgf-transload-ops@fmiint.com>'
	cSendTo = 'mbennett@fmiint.com'
	cCC = 'mbennett@fmiint.com'
	cInternalSendTo = 'mbennett@fmiint.com'
	cInternalCC = 'mbennett@fmiint.com'
	cSubject = 'EDI 204 Import Process Results for ' + TTOC(DATETIME())
	cAttach = ''
	cBodyText = ''
	cTopBodyText = ''
	cSpecialMessage = ''
	
	cCOMPUTERNAME = ''
	cUSERNAME = ''


	FUNCTION INIT
		IF NOT DODEFAULT()
			RETURN .F.
		ENDIF
		WITH THIS
			*SET RESOURCE OFF
			SET EXCLUSIVE OFF
			CLOSE DATA
			SET HOURS TO 24
			DO setenvi
			SET CENTURY ON
			SET DATE YMD
			SET DECIMAL TO 0
			SET TALK OFF
			SET DELETED ON
			SET CONSOLE OFF
			SET EXCLUSIVE OFF
			SET SAFETY OFF
			SET EXACT OFF
			SET STATUS BAR ON
			SET SYSMENU OFF
			SET ENGINEBEHAVIOR 70
			_VFP.AUTOYIELD = .lAutoYield
			.cCOMPUTERNAME = GETENV("COMPUTERNAME")
			.cUSERNAME = GETENV("USERNAME")
		ENDWITH
	ENDFUNC


	FUNCTION DESTROY
		WITH THIS
			IF .lLoggingIsOn  THEN
				SET ALTERNATE OFF
				SET ALTERNATE TO
			ENDIF
		ENDWITH
		DODEFAULT()
	ENDFUNC


	FUNCTION MAIN
		LPARAMETERS tcClient, tlTestMode
		WITH THIS

			TRY

				.lTestMode = tlTestMode

				LOCAL lnNumberOfErrors, laFiles[1,5], laFilesSorted[1,6]
				LOCAL lnNumFiles, lnCurrentFile, lcSourceFile, lcArchiveFile, lcTargetFile
				LOCAL lcEDISourceFile, lcEDIArchiveFile, lcOutstrx, lcDelimChar, llArchived, lcPrepend

				lnNumberOfErrors = 0

				IF NOT .SetClient( tcClient ) THEN
					THROW
				ENDIF

				.lLoggingIsOn = .lLoggingIsOn AND NOT EMPTY(.cLogFile)

				IF .lLoggingIsOn THEN
					SET ALTERNATE TO (.cLogFile) ADDITIVE
					SET ALTERNATE ON
				ENDIF

				.TrackProgress('EDI 204 Import Process process started....', LOGIT+SENDIT+NOWAITIT)
				.TrackProgress('tcClient = ' + tcClient, LOGIT+SENDIT)


				.TrackProgress('COMPUTERNAME = ' + .cCOMPUTERNAME, LOGIT+SENDIT)
				.TrackProgress('USERNAME = ' + .cUSERNAME, LOGIT+SENDIT)
				.TrackProgress('PROJECT = EDI204InProc', LOGIT+SENDIT)
				IF .lTestMode THEN
					.TrackProgress('*** TEST MODE ***', LOGIT+SENDIT)
				ENDIF

				.TrackProgress('.cInputFolder = ' + .cInputFolder, LOGIT+SENDIT)
				.TrackProgress('.cHoldFolder = ' + .cHoldFolder, LOGIT+SENDIT)
				.TrackProgress('.cArchiveFolder = ' + .cArchiveFolder, LOGIT+SENDIT)
				.TrackProgress('.c204ProcHeaderTable = ' + .c204ProcHeaderTable, LOGIT+SENDIT)
*!*					.TrackProgress('.c204ProcTripsTable = ' + .c204ProcTripsTable, LOGIT+SENDIT)
				.TrackProgress('.c204ProcStopsTable = ' + .c204ProcStopsTable, LOGIT+SENDIT)
				.TrackProgress('.c204ProcControlTable = ' + .c204ProcControlTable, LOGIT+SENDIT)
				.TrackProgress('.cSendTo = ' + .cSendTo, LOGIT+SENDIT)
				.TrackProgress('.cCC = ' + .cCC, LOGIT+SENDIT)


				***************************************************************************************************************
				* process any files in the input folder.
				***************************************************************************************************************
				* PRIVATE VARs which will be constant
				PRIVATE m.ADDPROC, m.UPDPROC, m.ACCOUNTID, m.ACCTNAME, m.SRCFILE, m.PROC204CTR, m.lExpress, m.LTEST
				STORE "EDI204" TO m.ADDPROC, m.UPDPROC
				m.ACCOUNTID = .nAccountID
				m.ACCTNAME = .cAcctname
				m.lExpress = .lExpress
				m.SRCFILE = ''
				m.PROC204CTR = 0

				m.LTEST = .lTestMode  && TO STORE TEST MODE IN WOLOG204

				lnNumFiles = ADIR(laFiles,(.cInputFolder + .cInputFileSpec))

				IF lnNumFiles > 0 THEN

					.nNumFiles = lnNumFiles

					PRIVATE cDelimiter, cTranslateOption

					cDelimiter = "*"
					cTranslateOption = "NONE"
					DO createx856a

					USE ( .c204ProcHeaderTable ) IN 0 ALIAS WOLOG204
					USE ( .c204ProcStopsTable ) IN 0 ALIAS STOPS204
					USE ( .c204ProcControlTable ) IN 0 ALIAS CTRL204  && THIS TABLE WILL HOLD THE LATEST WOLOG204ID INTEGER

					* OPEN UP THE TRUCKING WOLOG
					* IF IT'S A BIG LOTS 204, THEY DO NOT PROVIDE PICK UP LOC, SO WE WILL TRY TO FIND THE CONTAINER
					* IN TRUCKING WOLOG AND RETRIEVE PICK UP LOC FROM THERE
					IF .lTestMode THEN
						USE M:\DEV\WODATA\WOLOG IN 0 ALIAS TRUCKWOLOG
						USE M:\DEV\WODATA\WOGENPK IN 0 ALIAS GENERATEPK
					ELSE
						USE F:\WO\WODATA\WOLOG AGAIN IN 0 ALIAS TRUCKWOLOG
						USE F:\WO\WODATA\WOGENPK AGAIN IN 0 ALIAS GENERATEPK
					ENDIF

					* Open EDI Trigger Table which will be used to generate 214s

					IF .lTestMode THEN
						USE F:\UTIL\LOADTENDER\TESTDATA\EDI_TRIGGER IN 0 ALIAS EDI_TRIGGER
					ELSE
						USE F:\3PL\DATA\EDI_TRIGGER IN 0 ALIAS EDI_TRIGGER SHARED
						useca("edi_trigger","stuff",,,"select * from edi_trigger where .F.",,"sqleditrigger")
					ENDIF

					* get unique process counter so we can tell exactly which table records were processed in this run -- necessary for some processing later on.
					m.PROC204CTR = .GetNextProc204Ctr()
					.TrackProgress('m.PROC204CTR = ' + TRANSFORM(m.PROC204CTR), LOGIT+SENDIT)

					* sort file list by date/time
					.SortArrayByDateTime(@laFiles, @laFilesSorted)

					FOR lnCurrentFile = 1 TO lnNumFiles

						.nFilesProcessed = .nFilesProcessed + 1

						lcEDISourceFile = .cInputFolder + laFilesSorted[lnCurrentFile,1]
						*lcEDIArchiveFile = .cArchiveFolder + laFilesSorted[lnCurrentFile,1]
						
						***********************************************************************************************************************
						*** block added 7/23/13 MB
						*** Because this process runs constantly via a poller, we should move 204s out of the initial folder for processing.
						*** If we do not do that, a problem with a file being processed could generate 1 error email per minute!
						
						* create filename for the holding folder
						lcHoldFile = .cHoldFolder + laFilesSorted[lnCurrentFile,1]
						
						* copy file from input folder to holding folder
						COPY FILE (lcEDISourceFile) TO (lcHoldFile)
						
						* if copy was successful, delete orig file from the input folder
						IF FILE(lcHoldFile) THEN
							DELETE FILE(lcEDISourceFile)
							.TrackProgress("Copied source file to the Hold Folder as: " + lcHoldFile, LOGIT+SENDIT)
						ELSE
							.TrackProgress("*** Error copying source file to the Hold Folder as: " + lcHoldFile, LOGIT+SENDIT)
							THROW
						ENDIF
						
						* update source filename
						lcEDISourceFile = lcHoldFile

						***********************************************************************************************************************
						
						
						m.SRCFILE = laFilesSorted[lnCurrentFile,1]

						.nOutRecs = 0  && tracking # of recs appended to either table, to try and identify empty files

						.TrackProgress("Processing EDI file: " + lcEDISourceFile,LOGIT+SENDIT+NOWAITIT)

						lcOutstrx = FILETOSTR(lcEDISourceFile)

						IF SUBSTR(lcOutstrx,1,3) = "ISA" THEN
							* continue processing

							lcDelimChar = SUBSTR(lcOutstrx,4,1)
							*lcEOLChar = SUBSTR(lcOutstrx,106,1)

							DO CASE
								CASE .nAccountID = 647  && MACYS
									DO loadedifile WITH lcEDISourceFile, lcDelimChar, "85H2"
								CASE .nAccountID = 4717  && BIG LOTS
									DO loadedifile WITH lcEDISourceFile, lcDelimChar, "TILDE"
								CASE .nAccountID = 687  && VF SPORTSWEAR
									DO loadedifile WITH lcEDISourceFile, lcDelimChar, "TILDE"
							ENDCASE

							*!*	SELECT x856
							*!*	BROWSE
							*!*	THROW

							.process204()

							* define archived filename
							DO CASE
								CASE .nAccountID = 647  && MACYS
									* the Macy's 204 filenames are not very descriptive; we will prefix date/time info to the name when archiving
									lcPrepend = DTOS(DATE()) + STRTRAN(TIME(),":","") + "_"
									lcEDIArchiveFile = .cArchiveFolder + lcPrepend + laFilesSorted[lnCurrentFile,1]
								OTHERWISE
									lcEDIArchiveFile = .cArchiveFolder + laFilesSorted[lnCurrentFile,1]
							ENDCASE

							*IF NOT .lTestMode THEN
							* archive and then delete the file.
							llArchived = FILE(lcEDIArchiveFile)
							IF llArchived THEN
								RUN ATTRIB -R &lcEDIArchiveFile.
								.TrackProgress('   Used ATTRIB -R for re-archiving : ' + lcEDISourceFile,LOGIT+SENDIT)
							ENDIF

							* archive source file
							COPY FILE (lcEDISourceFile) TO (lcEDIArchiveFile)

							* delete original source file if the copy was made...
							IF FILE(lcEDIArchiveFile) THEN
								DELETE FILE(lcEDISourceFile)
								.TrackProgress("Archived file: " + lcEDISourceFile + ' to ' + .cArchiveFolder,LOGIT+SENDIT)
							ELSE
								.TrackProgress("!! There were errors archiving file: " + lcEDISourceFile,LOGIT+SENDIT)
							ENDIF
							*ENDIF
						ELSE
							.TrackProgress("ERROR: " + lcEDISourceFile + " is not an EDI File!",LOGIT+SENDIT)
						ENDIF

						*.TrackProgress('=========================================================', LOGIT+SENDIT)

					ENDFOR && lnCurrentFile = 1 TO lnNumFiles
					
					
					
					* do a table update only if we added recs to sqleditrigger
					SELECT sqleditrigger
					LOCATE
					IF EOF('sqleditrigger') THEN
						.TrackProgress('===> sqleditrigger has no records', LOGIT+SENDIT)
					ELSE
						* TU() if not in test mode
						IF .lTestMode THEN
							.TrackProgress('TEST MODE: did not TU(sqleditrigger)',LOGIT+SENDIT)
							SELECT sqleditrigger
							BROWSE
						ELSE					
							* tableupate SQL
							IF tu("sqleditrigger") THEN
								.TrackProgress('SUCCESS: TU(sqleditrigger) returned .T.!',LOGIT+SENDIT)
							ELSE
								* ERROR on table update
								.TrackProgress('ERROR: TU(sqleditrigger) returned .F.!',LOGIT+SENDIT)
								=AERROR(UPCERR)
								IF TYPE("UPCERR")#"U" THEN 
									.TrackProgress('Error Description = ' + UPCERR[1,2],LOGIT+SENDIT)
								ENDIF
							ENDIF
						ENDIF  &&  .lTestMode					
					ENDIF  &&  EOF('sqleditrigger')
					
					

					IF .lMadeA990 THEN
						DO CASE
							CASE .nAccountID = 4717  && BIGLOTS
								.TriggerFTPUploadBIGLOTS()
							OTHERWISE
								* Nothing
						ENDCASE
					ENDIF  &&  .lMadeA990
					
					* reset switch
					.lMadeA990 = .F.

				ELSE
					.TrackProgress('Found no files in the input folder', LOGIT+SENDIT)
					* don't send an email if we processed no files -- to support running this proc every half hour per Doug.
				ENDIF

				.TrackProgress('EDI 204 Import process ended normally!',LOGIT+SENDIT)

				*!*					.cBodyText = .cISAList + CRLF + CRLF + .cTopBodyText + CRLF + ;
				*!*						"<Report log follows>" + CRLF + CRLF + .cBodyText

			CATCH TO loError

				.TrackProgress('There was an error.',LOGIT+SENDIT)
				.TrackProgress('The Error # is: ' + TRANSFORM(loError.ERRORNO), LOGIT+SENDIT)
				.TrackProgress('The Error Message is: ' + TRANSFORM(loError.MESSAGE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred in Program: ' + TRANSFORM(loError.PROCEDURE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred at line #: ' + TRANSFORM(loError.LINENO), LOGIT+SENDIT)
				.TrackProgress(.cThrowDescription, LOGIT+SENDIT)
				lnNumberOfErrors = lnNumberOfErrors + 1
				CLOSE DATA

				IF .n990FileHandle > 0 THEN
					=FCLOSE(.n990FileHandle)
				ENDIF

				IF .lTestMode THEN
					.cSendTo = 'mbennett@fmiint.com'
				ELSE
					.cSendTo = 'mbennett@fmiint.com,pgaidis@fmiint.com'
				ENDIF

			ENDTRY

			CLOSE DATABASES ALL

			WAIT CLEAR
			***************** INTERNAL email results ******************************
			.TrackProgress('About to send status email.',LOGIT)
			.TrackProgress('The logfile is: ' + .cLogFile, SENDIT)
			.TrackProgress('EDI 204 Import Process process started: ' + .cStartTime, LOGIT+SENDIT)
			.TrackProgress('EDI 204 Import Process process finished: ' + TTOC(DATETIME()), LOGIT+SENDIT)
			
			.TrackProgress('lnNumberOfErrors = ' + TRANSFORM(lnNumberOfErrors), LOGIT+SENDIT)
			.TrackProgress('.nFilesProcessed = ' + TRANSFORM(.nFilesProcessed), LOGIT+SENDIT)
			
			
			
			
			
			
			
			DO CASE
				CASE (lnNumberOfErrors > 0)
					.TrackProgress('CASE (lnNumberOfErrors > 0)', LOGIT+SENDIT)
					* send email only to Mark if there was an error
					.lSendInternalEmailIsOn = .T.
					.cInternalSendTo = 'mbennett@fmiint.com'
					.cInternalCC = 'mbennett@fmiint.com'
					.cSubject = 'ERRORS in EDI 204 Process for ' + .cAcctname + '  ' + TTOC(DATETIME())
				CASE (.nFilesProcessed > 0) AND (NOT .nAccountID = 4717)
					.TrackProgress('CASE (.nFilesProcessed > 0) AND (NOT .nAccountID = 4717)', LOGIT+SENDIT)
					* if 204s were processed and client is not Biglots, which sends an email from another code section, send status email.
					.lSendInternalEmailIsOn = .T.
					.cSubject = 'EDI 204 Process Results for ' + .cAcctname + '  ' + TTOC(DATETIME())
					*.cBodyText = "204s were received - please check the Load Tenders screen." + CRLF + CRLF + "<<report log follows>>" + CRLF + CRLF + .cBodyText
					
					.cTopBodyText = "204s were received - please check the Load Tenders screen." + CRLF + .cTopBodyText
					.cBodyText = .cTopBodyText + CRLF + .cSpecialMessage + CRLF + CRLF + "<<report log follows>>" + CRLF + CRLF + .cBodyText
					
					
				CASE (.nFilesProcessed > 0) AND (.nAccountID = 4717)
					.TrackProgress('CASE (.nFilesProcessed > 0) AND (.nAccountID = 4717)', LOGIT+SENDIT)
					* if 204s were processed and client is Biglots, send regular status email.
					.lSendInternalEmailIsOn = .T.
					.cSubject = 'EDI 204 Process Results for ' + .cAcctname + '  ' + TTOC(DATETIME())
				OTHERWISE
					.TrackProgress('CASE OTHERWISE', LOGIT+SENDIT)
					 * no files processedand no errors - do not send email.
					.lSendInternalEmailIsOn = .F.			
			ENDCASE
			
			.TrackProgress('.cInternalSendTo = ' + .cInternalSendTo, LOGIT+SENDIT)

			IF .lSendInternalEmailIsOn THEN
				*!*	*IF (.nFilesProcessed > 0) OR (lnNumberOfErrors > 0) THEN
				*!*	IF .nNumFiles > 0 THEN
				*!*		.cSubject = 'EDI 204 Process Results for ' + .cAcctname + '  ' + TTOC(DATETIME())
				*!*	ELSE
				*!*		.cSubject = 'No Files Found! EDI 204 for ' + .cAcctname + '  ' + TTOC(DATETIME())
				*!*	ENDIF
				DO FORM dartmail2 WITH .cInternalSendTo,.cFrom,.cSubject,.cInternalCC,.cAttach,.cBodyText,"A"
				.TrackProgress('Sent status email.',LOGIT)
				*ENDIF
			ELSE
				.TrackProgress('Did not send status email: THIS.lSendInternalEmailIsOn = FALSE.',LOGIT)
			ENDIF

		ENDWITH
		RETURN
	ENDFUNC && main


	FUNCTION GetNextWolog204ID
		* 	Changed to make this ID common across all accounts 4/11/12 MB.
		LOCAL lnRetval, lcAlias
		lcAlias = ALIAS()

		SELECT CTRL204
		*LOCATE FOR CACCTNAME = .cAcctName
		*LOCATE FOR nAccountID = .nAccountID
		LOCATE FOR ALLTRIM(CACCTNAME) = 'ALLACCOUNTS'
		IF FOUND() THEN
			lnRetval = CTRL204.wologctr + 1
			REPLACE CTRL204.wologctr WITH lnRetval IN CTRL204
		ELSE
			.cThrowDescription = 'ERROR: account ALLACCOUNTS not found in CTRL204 in GetNextWolog204ID()!'
			THROW
		ENDIF

		SELECT ( lcAlias )
		RETURN lnRetval
	ENDFUNC


	FUNCTION GetNextProc204Ctr
		* 	Changed to make this ID common across all accounts 4/11/12 MB.
		LOCAL lnRetval, lcAlias
		lcAlias = ALIAS()

		SELECT CTRL204
		*LOCATE FOR CACCTNAME = .cAcctName
		*LOCATE FOR nAccountID = .nAccountID
		LOCATE FOR ALLTRIM(CACCTNAME) = 'ALLACCOUNTS'
		IF FOUND() THEN
			lnRetval = CTRL204.PROC204CTR + 1
			REPLACE CTRL204.PROC204CTR WITH lnRetval IN CTRL204
		ELSE
			.cThrowDescription = 'ERROR: account ALLACCOUNTS not found in CTRL204 in GetNextProc204Ctr()!'
			THROW
		ENDIF

		SELECT ( lcAlias )
		RETURN lnRetval
	ENDFUNC


	FUNCTION ConvertYYYYMMDDStringToDate
		LPARAMETERS tcYYYYMMDDString
		* this assumes we are already in SET DATE YMD mode...
		LOCAL ldDate
		ldDate = CTOD( LEFT(tcYYYYMMDDString,4) + "/" + SUBSTR(tcYYYYMMDDString,5,2) + "/" + SUBSTR(tcYYYYMMDDString,7,2) )
		RETURN ldDate
	ENDFUNC


	PROCEDURE process204
		WITH THIS

			LOCAL lnPULOCRecNum, lnMinSTOP, lnMaxSTOP, lcORIGIN, lcDESTINATIO, lcORIGSTATE, lcDESTSTATE
			LOCAL lcStopType, lnWolog204ID, lnBrokref, lcNotes, lcStatus, lcMessage, lcWOLogNotes, ldOneMonthAgo
			LOCAL lcFrom, lcSendTo, lcCC, lcSubject, lcAttach, lcBodyText, lc990, lcWO, lcSubSuffix

			* Private HEADER Vars which need to get reset
			PRIVATE m.SHIPREF, m.BROKREF, m.SCAC, m.EDIType, m.WOLOG204ID, m.PURPCODE, m.RESPDATE, m.RESPTIME, m.STATUS, m.ADDDT, m.UPDATEDT
			PRIVATE m.VESSEL, m.MBLNUM, m.CONTAINER, m.TRPULOC, m.TRRETLOC, m.TRFROMLOC, m.TRTOLOC, m.PICKEDUP, m.DELIVERED, m.APPT, m.APPTTM
			PRIVATE m.FOUNDWO, m.PONUM
			STORE "" TO m.SHIPREF, m.BROKREF, m.SCAC, m.EDIType, m.PURPCODE, m.RESPTIME, m.VESSEL, m.MBLNUM, m.CONTAINER, m.TRPULOC, m.TRRETLOC
			STORE "" TO m.APPTTM, m.PONUM
			STORE 0 TO m.WOLOG204ID, m.TRFROMLOC, m.TRTOLOC, m.WO_NUM
			STORE .F. TO m.FOUNDWO
			STORE CTOD('') TO m.RESPDATE, m.ADDDT, m.UPDATEDT, m.PICKEDUP, m.DELIVERED, m.APPT, m.WO_DATE
			m.STATUS = "OPEN"

			* Private DETAIL Vars which need to get reset
			PRIVATE m.STOP, m.WEIGHT, m.PULOC, m.DELLOC, m.PUAPPT, m.PUATIME, m.PU, m.DEL, m.DELAPPT, m.DELATIME
			PRIVATE m.PUATIME1, m.DELATIME1, m.QUANTITY, m.QTY_TYPE, m.LOADTYPE, m.CUBE
			PRIVATE m.PUNAME, m.PUPHONE, m.PUEMAIL, m.PUADDRESS, m.PUCSZ
			*PRIVATE m.DELNAME, m.DELPHONE, m.DELEMAIL, m.DELADDRESS, m.DELCSZ
			STORE 0 TO m.STOP, m.WEIGHT, m.QUANTITY, m.CUBE
			STORE "" TO m.PULOC, m.DELLOC, m.PUATIME, m.DELATIME, m.PUATIME1, m.DELATIME1, m.QTY_TYPE, m.LOADTYPE
			STORE "" TO m.PUNAME, m.PUPHONE, m.PUEMAIL, m.PUADDRESS, m.PUCSZ
			*STORE "" TO m.DELNAME, m.DELPHONE, m.DELEMAIL, m.DELADDRESS, m.DELCSZ
			STORE .F. TO m.PU, m.DEL
			STORE CTOD('') TO m.PUAPPT, m.DELAPPT

			SELECT x856
			SCAN
				DO CASE
					CASE ALLTRIM(x856.segment) == "ST" AND ALLTRIM(x856.f1) == "204"
						* we're at beginning of 204 segment - set up ctrl #s and counters
						m.EDIType = "204"
						m.CTRLNUM = VAL(ALLTRIM(x856.f2))  && sender control #
						m.WOLOG204ID = .GetNextWolog204ID()
						.n204Count = .n204Count + 1
						.TrackProgress('***** New 204 Encountered, Sender CTRL# = ' + TRANSFORM(m.CTRLNUM), LOGIT+SENDIT)
					CASE ALLTRIM(x856.segment) == "SE"
						* we're at end of 204 segment - insert the final STOP (which had no S5 seg to trigger it), then insert HEADER record
						* BUT DON'T DO STOPS INSERT FOR BIGLOTS 2/23/12 M.B.
						m.ADDDT = DATETIME()
						IF NOT (.nAccountID = 4717) THEN && BIGLOTS
							INSERT INTO STOPS204 FROM MEMVAR
							.TrackProgress('Trip Detail Insert: STOP =' + TRANSFORM(m.STOP) + ',  LOC = ' + m.PULOC,LOGIT+SENDIT)
						ENDIF && NOT (.nAccountID = 4717)

						IF .nAccountID = 4717 THEN
							m.TRTOLOC = 1  && TO LOC #1 FOR BIGLOTS = TREMONT PA DC
						ENDIF
						
						* if MACY's 204 check to see if it was a Replace. If it was, use the status and WO_NUM of the replaced wolog204 record.
						IF .nAccountID = 647 THEN  && MACY's
							IF .lReplace204 THEN
								m.STATUS = .cReplacedStatus
								m.WO_NUM = .nReplacedWO_Num
								m.FILE990 = .cReplacedFILE990
								m.RESPDATE = .dReplacedRESPDATE
								m.RESPTIME = .cReplacedRESPTIME
								.cTopBodyText = "A new 204 replaced the one for " + m.SHIPREF + " - no action is required for it." + CRLF + CRLF + .cTopBodyText
								m.NOTES = "Replaces the 204 for " + m.SHIPREF + " already processed."
							ENDIF
						ENDIF

						INSERT INTO WOLOG204 FROM MEMVAR
						.TrackProgress('Wolog Header Insert: SHIPREF = ' + m.SHIPREF + ',  RESPOND DATE = ' + TRANSFORM(m.RESPDATE) + ' ' + m.RESPTIME,LOGIT+SENDIT)

						* added this logic just after insert of Header info, for Biglots
						IF .nAccountID = 4717  && BIGLOTS
							*IF EMPTY(m.TRPULOC) THEN  && RIGHT NOW THIS IS ALWAYS EMPTY :)

							* per PG, try to get info from TRUCKWOLOG
							IF NOT EMPTY( m.CONTAINER ) THEN

								ldOneMonthAgo = DATE() - 30

								.TrackProgress('Searching WOLOG for CONTAINER = ' + m.CONTAINER + ',  WOLOG204ID = ' + TRANSFORM(WOLOG204.WOLOG204ID),LOGIT+SENDIT)
								SELECT TRUCKWOLOG
								LOCATE FOR (CONTAINER = m.CONTAINER) AND (WO_DATE > ldOneMonthAgo)
								m.FOUNDWO = FOUND()

								IF m.FOUNDWO THEN

									m.WO_NUM = TRUCKWOLOG.WO_NUM
									m.WO_DATE = TRUCKWOLOG.WO_DATE
									m.TRPULOC = TRUCKWOLOG.PULOC
									m.TRRETLOC = TRUCKWOLOG.RETLOC
									m.TRFROMLOC = TRUCKWOLOG.FROMLOC
									m.TRTOLOC = TRUCKWOLOG.TOLOC
									m.PICKEDUP = TRUCKWOLOG.PICKEDUP
									m.DELIVERED = TRUCKWOLOG.DELIVERED
									m.APPT = TRUCKWOLOG.APPT
									m.APPTTM = TRUCKWOLOG.APPTTM
									lcNotes = "Found existing Trucking WO: " + TRANSFORM(m.WO_NUM) + CRLF
									.TrackProgress("Found existing Trucking WO: " + TRANSFORM(m.WO_NUM),LOGIT+SENDIT)

									***********************************************************
									* UPDATE FIELDS IN WOLOG
									***********************************************************

									lcWOLogNotes = ALLTRIM(TRUCKWOLOG.NOTES)
									*IF NOT EMPTY(m.PONUM) THEN
									* ADD PO NUM INFO PER DARREN
									IF EMPTY(lcWOLogNotes) THEN
										*lcWOLogNotes = "PO*" + m.PONUM
										lcWOLogNotes = "PO*" + m.PONUM + CRLF + "APPT*" + m.SHIPREF
									ELSE
										IF (NOT ("PO*" + m.PONUM) $ lcWOLogNotes) THEN
											lcWOLogNotes = lcWOLogNotes + CRLF + "PO*" + m.PONUM
										ENDIF
										IF (NOT ("APPT*" + m.SHIPREF) $ lcWOLogNotes) THEN
											lcWOLogNotes = lcWOLogNotes + CRLF + "APPT*" + m.SHIPREF
										ENDIF
									ENDIF
									*ENDIF
									
									*****************************************************************************************************************
									* PER DARREN 10/13/14: do not replace BROKREF with SHIPREF; do not update the notes with the replacement, either.
									*****************************************************************************************************************

									*!*	IF NOT EMPTY(TRUCKWOLOG.BROKREF) THEN
									*!*		IF (NOT ALLTRIM(TRUCKWOLOG.BROKREF) == ALLTRIM(M.SHIPREF)) THEN
									*!*			* note existing BROKREF in Notes field before overwrite BROKREF with what we got from 204, per PG 03/23/2012 MB
									*!*			lcWOLogNotes = lcWOLogNotes + CRLF + 'Previous BROKREF was = ' + ALLTRIM(TRUCKWOLOG.BROKREF)
									*!*		ENDIF
									*!*	ENDIF

									*!*	REPLACE TRUCKWOLOG.BROKREF WITH M.SHIPREF, ;
									*!*		TRUCKWOLOG.PONUM WITH m.PONUM, ;
									*!*		TRUCKWOLOG.APPTNUM WITH M.SHIPREF, ;
									*!*		TRUCKWOLOG.UPDATEBY WITH m.UPDPROC, ;
									*!*		TRUCKWOLOG.UPDATEDT WITH DATETIME(), ;
									*!*		TRUCKWOLOG.NOTES WITH lcWOLogNotes, ;
									*!*		TRUCKWOLOG.UPDPROC WITH m.UPDPROC ;
									*!*		IN TRUCKWOLOG

									REPLACE TRUCKWOLOG.PONUM WITH m.PONUM, ;
										TRUCKWOLOG.APPTNUM WITH M.SHIPREF, ;
										TRUCKWOLOG.UPDATEBY WITH m.UPDPROC, ;
										TRUCKWOLOG.UPDATEDT WITH DATETIME(), ;
										TRUCKWOLOG.NOTES WITH lcWOLogNotes, ;
										TRUCKWOLOG.UPDPROC WITH m.UPDPROC ;
										IN TRUCKWOLOG										
									*****************************************************************************************************************
										
								ELSE
									*set to UNKNOWN
									*m.TRPULOC = "UNKNOWN"
									*.TrackProgress('ERROR: unable to determine PULOC for PROC204CTR = ' + TRANSFORM(m.PROC204CTR) + ',  WOLOG204ID = ' + TRANSFORM(WOLOG204.WOLOG204ID),LOGIT+SENDIT)
									lcNotes = "Did not find existing Trucking WO" + CRLF
									.TrackProgress("Did not find an existing Trucking WO",LOGIT+SENDIT)
								ENDIF  &&  m.FOUNDWO

								SELECT WOLOG204
								REPLACE WO_NUM WITH m.WO_NUM, ;
									WO_DATE WITH m.WO_DATE, ;
									TRPULOC WITH m.TRPULOC, ;
									TRRETLOC WITH m.TRRETLOC, ;
									TRFROMLOC WITH m.TRFROMLOC, ;
									TRTOLOC WITH m.TRTOLOC, ;
									PICKEDUP WITH m.PICKEDUP, ;
									DELIVERED WITH m.DELIVERED, ;
									APPT WITH m.APPT, ;
									APPTTM WITH m.APPTTM, ;
									NOTES WITH lcNotes, ;
									FOUNDWO WITH m.FOUNDWO ;
									IN WOLOG204

								* prep status email to concerned parties...
								lcFrom = 'TGF Corporate <tgf-transload-ops@fmiint.com>'
								*lcSendTo = 'mbennett@fmiint.com'
								lcSendTo = .cSendTo
								lcCC = .cCC
								lcSubject = 'Load Tender Results for ' + .cAcctname + '  ' + TTOC(DATETIME())
								lcSubSuffix = ''
								lcAttach = ''
								lcBodyText = ''
								lc990 = ''
								lcWO = ''


								*IF 	WOLOG204.FOUNDWO THEN
								IF m.FOUNDWO THEN

									* now, if picked up and delivered dates are both empty, we send an ACCEPT 990
									* else we send a DECLINE 990

									IF EMPTY(WOLOG204.PICKEDUP) AND EMPTY(WOLOG204.DELIVERED) THEN
										lcStatus = "ACCEPTED"
									ELSE
										lcStatus = "DECLINED"
									ENDIF

									lcNotes = ALLTRIM(WOLOG204.NOTES) + CRLF + ;
										"PICKEDUP = " + TRANSFORM(WOLOG204.PICKEDUP) + ", DELIVERED = " + ;
										TRANSFORM(WOLOG204.DELIVERED) + " => " + lcStatus
									REPLACE NOTES WITH lcNotes IN WOLOG204

									IF lcStatus = "ACCEPTED" THEN
										IF .Create990( lcStatus ) THEN
											lcBodyText = 'We produced a 990 to accept load tender for Container ' + ALLTRIM(m.CONTAINER) + '.' + CRLF + CRLF + ;
												'NOTE: we found existing Work Order ' + TRANSFORM(m.WO_NUM) + ' for this container.'
										ELSE
											lcBodyText = 'We found existing Work Order ' + TRANSFORM(m.WO_NUM) + ' for Container ' + ALLTRIM(m.CONTAINER) + '.' + CRLF + CRLF + ;
												'But there was an error producing the 990 to accept the load tender!'
											lcSubSuffix = ' (THERE WERE ERRORS)'
										ENDIF
									ELSE
										* lcStatus = "DECLINED"
										IF .Create990( lcStatus ) THEN
											lcBodyText = 'We produced a 990 to decline load tender for Container ' + ALLTRIM(m.CONTAINER) + ;
												' because both Picked Up and Delivered dates for existing Work Order ' + TRANSFORM(m.WO_NUM) + ' were not empty.' + CRLF + ;
												'Picked Up = ' + TRANSFORM(WOLOG204.PICKEDUP) + CRLF + ;
												'Delivered = ' + TRANSFORM(WOLOG204.DELIVERED)
										ELSE
											lcBodyText = 'We tried to produce a 990 to decline load tender for Container ' + ALLTRIM(m.CONTAINER) + ;
												' because both Picked Up and Delivered dates for existing Work Order ' + TRANSFORM(m.WO_NUM) + ' were not empty.' + CRLF + ;
												'Picked Up = ' + TRANSFORM(WOLOG204.PICKEDUP) + CRLF + ;
												'Delivered = ' + TRANSFORM(WOLOG204.DELIVERED) + CRLF + CRLF + ;
												'But there was an error producing the 990!'
											lcSubSuffix = ' (THERE WERE ERRORS)'
										ENDIF
									ENDIF

								ELSE

									* create a trucking WO if 'ignore' cases do not apply
									* if successful creation of WO, send an accept 990
									
									DO CASE
									
										CASE INLIST(ALLTRIM(m.VESSEL),"OOCL","K LINE","CMA")
											* per Darren 6/4/15 - if it's one of these shipping lines, do NOT create a WO or 990 - basically we're just going to ignore the 204
											
											lcWO = 'We ignored a 204 with Vessel = ' + ALLTRIM(m.VESSEL) + '.' + CRLF
											lc990 = 'We did *not* produce a 990.'
									
										CASE INLIST(ALLTRIM(m.MBLNUM),"CMDU","KKFU","YASV")
											* per Darren 3/9/15 - if it's one of these master bills, do NOT create a WO or 990 - basically we're just going to ignore the 204
											*IF INLIST(ALLTRIM(m.MBLNUM),"CMDU","KKFU","YASV") THEN
											
											lcWO = 'We ignored a 204 with Master Bill # = ' + ALLTRIM(m.MBLNUM) + '.' + CRLF
											lc990 = 'We did *not* produce a 990.'
											
										OTHERWISE

											lcStatus = "ACCEPTED"

											IF .CreateTruckingWorkOrder() THEN

												lcWO = 'We made new Work Order # ' + TRANSFORM(.nNewWoNum) + ;
													' to accept load tender for Container ' + ALLTRIM(m.CONTAINER) + '.' + CRLF + CRLF + ;
													'NOTE: this is an incomplete Work Order!' + CRLF + CRLF + ;
													'Please edit it and enter at least the folowing: ' + CRLF + CRLF + ;
													'     Bill To' + CRLF + ;
													'     Container Size' + CRLF + ;
													'     Carrier' + CRLF + ;
													'     Pickup Loc' + CRLF + ;
													'     Return Loc'

												* we created WO successfully, now create the 990...
												IF .Create990( lcStatus ) THEN
													lc990 = 'We produced a 990 to accept load tender for Container ' + ALLTRIM(m.CONTAINER) + '.'  + CRLF + CRLF
												ELSE
													lc990 = 'ERROR producing a 990 to accept load tender for Container ' + ALLTRIM(m.CONTAINER) + '.' + CRLF
													lcSubSuffix = ' (THERE WERE ERRORS)'
												ENDIF

											ELSE
												lcWO = 'ERROR producing a new Work Order to accept load tender for Container ' + ALLTRIM(m.CONTAINER) + '.' + CRLF
												lc990 = 'We did *not* produce a 990.'
												lcSubSuffix = ' (THERE WERE ERRORS)'
											ENDIF

									ENDCASE
									*ENDIF && INLIST(ALLTRIM(m.MBLNUM),"CMDU","KKFU","YASV") THEN
									
									lcBodyText = lc990 + lcWO

								ENDIF  && m.FOUNDWO

								lcSubject = lcSubject + lcSubSuffix

								IF .lTestMode THEN
									lcBodyText = "*** TEST MODE ***" + 	CRLF + CRLF + lcBodyText
								ENDIF

								* send an email
								DO FORM dartmail2 WITH lcSendTo,lcFrom,lcSubject,lcCC,lcAttach,lcBodyText,"A"

							ELSE
								.TrackProgress('ERROR: unable to determine Container for PROC204CTR = ' + TRANSFORM(m.PROC204CTR) + ',  WOLOG204ID = ' + TRANSFORM(WOLOG204.WOLOG204ID),LOGIT+SENDIT)
								lcNotes = "ERROR: unable to determine Container from the 204!" + CRLF
								REPLACE NOTES WITH lcNotes IN WOLOG204
							ENDIF
							*ENDIF  &&  EMPTY(m.TRPULOC)
						ENDIF  && .nAccountID = 4717  && BIGLOTS


						.clear_variables( "HEADER" )
						.clear_variables( "DETAIL" )
						
					CASE ALLTRIM(x856.segment) == "B2"
						m.SCAC = ALLTRIM(x856.f2)
						m.SHIPREF = ALLTRIM(x856.f4)
						DO CASE
							CASE .nAccountID = 647  && MACYS
								lnBrokref = INT(VAL(STRTRAN(m.SHIPREF,"M",""))) + 1
								m.BROKREF = ALLTRIM(TRANSFORM(lnBrokref))
							CASE .nAccountID = 4717  && BIGLOTS
								m.BROKREF = m.SHIPREF
							CASE .nAccountID = 687  && VF SPORTSWEAR
								m.BROKREF = m.SHIPREF  &&   ??????????????????????????????????
						ENDCASE
					CASE ALLTRIM(x856.segment) == "B2A"
						m.PURPCODE = ALLTRIM(x856.f1)
						.TrackProgress('m.PURPCODE = ' + m.PURPCODE,LOGIT+SENDIT)
						.lReplace204 = .F.  && will set to .T. if this turns out to be a replace.
						* purpose codes
						* 00 = original -- we just insert the records
						* 01 = cancel -- we will look for an already inserted record and change its status to withdrawn
						* 05 = replace  -- we will look for an already inserted record, delete it and reinsert recs with new info
						DO CASE
							CASE m.PURPCODE = '00'
								* nothing, do default processing of inserting Header/detail records
							CASE m.PURPCODE = '01'
								* we will look for already inserted records and change their status to WITHDRAWN
								SELECT WOLOG204
								LOCATE FOR SHIPREF = m.SHIPREF
								IF FOUND() THEN
									SELECT WOLOG204
									SCAN FOR SHIPREF = m.SHIPREF
										lnWolog204ID = WOLOG204.WOLOG204ID

										SELECT STOPS204
										REPLACE STOPS204.STATUS WITH "WITHDRAWN", ;
											STOPS204.UPDATEDT WITH DATETIME(), ;
											STOPS204.UPDPROC WITH m.UPDPROC ;
											FOR WOLOG204ID = lnWolog204ID IN STOPS204

										SELECT WOLOG204
										REPLACE WOLOG204.STATUS WITH "WITHDRAWN", ;
											WOLOG204.UPDATEDT WITH DATETIME(), ;
											WOLOG204.UPDPROC WITH m.UPDPROC ;
											FOR WOLOG204ID = lnWolog204ID IN WOLOG204

										.TrackProgress('Withdrew SHIPREF = ' + m.SHIPREF,LOGIT+SENDIT)
										.cSpecialMessage = .cSpecialMessage + 'SHIPREF = ' + m.SHIPREF + ' was withdrawn!' + CRLF
									ENDSCAN
								ELSE
									.TrackProgress('WARNING: Failed Withdrawal for SHIPREF = ' + m.SHIPREF,LOGIT+SENDIT)
								ENDIF
								* We don't want to insert anything in this case -- we basically want to skip the rest of this ST segment.
								* So we'll clear all variables and skip past SE segment, which is the last seg that triggers inserts
								.clear_variables( "HEADER" )
								.clear_variables( "DETAIL" )
								SELECT x856
								DO WHILE NOT EOF('x856')
									SKIP
									* exit loop if we encounter any segment which follows SE
									IF INLIST(ALLTRIM(x856.segment),"GE","IEA","ISA","GS","ST") THEN
										EXIT
									ENDIF
								ENDDO
								LOOP && return to top of scan loop
							CASE m.PURPCODE = '05'
								* we will look for an already inserted record, delete the header and detail recs,
								* then default processing will reinsert them with new info
								SELECT WOLOG204
								LOCATE FOR SHIPREF = m.SHIPREF
								IF FOUND() THEN
									
									* SAVE THIS INFO BECAUSE WE WILL USE IT IN THE NEW (REPLACING) WOLOG204 RECORD, WHEN WE INSERT IT 
									.cReplacedStatus = WOLOG204.STATUS
									.nReplacedWO_Num = WOLOG204.WO_NUM
									.cReplacedFILE990 = WOLOG204.FILE990
									.dReplacedRESPDATE = WOLOG204.RESPDATE
									.cReplacedRESPTIME = WOLOG204.RESPTIME
									* and set supporting flag
									.lReplace204 = .T.

									lnWolog204ID = WOLOG204.WOLOG204ID
									SELECT STOPS204
									DELETE FOR WOLOG204ID = lnWolog204ID IN STOPS204
									SELECT WOLOG204
									DELETE FOR WOLOG204ID = lnWolog204ID IN WOLOG204
									.TrackProgress('Replaced SHIPREF = ' + m.SHIPREF,LOGIT+SENDIT)
								ELSE
									.TrackProgress('WARNING: Failed Replace Action deletion for SHIPREF = ' + m.SHIPREF,LOGIT+SENDIT)
								ENDIF
							OTHERWISE
						ENDCASE
					CASE ALLTRIM(x856.segment) == "OID" AND ALLTRIM(x856.f3) == "PO"
						m.PONUM = ALLTRIM(x856.f2)
					CASE ALLTRIM(x856.segment) == "L11" AND ALLTRIM(x856.f2) == "VN"
						m.VESSEL = ALLTRIM(x856.f1)
						
					CASE ALLTRIM(x856.segment) == "L11" AND ALLTRIM(x856.f2) == "CR" AND .nAccountID = 687  && VF SPORTSWEAR       ????????????????????????//
						m.VESSEL = ALLTRIM(x856.f1)						
						
					CASE ALLTRIM(x856.segment) == "L11" AND ALLTRIM(x856.f2) == "BM"
						m.MBLNUM = ALLTRIM(x856.f1)
					CASE ALLTRIM(x856.segment) == "L11" AND ALLTRIM(x856.f2) == "CN"
						m.CONTAINER = ALLTRIM(x856.f1)
					CASE ALLTRIM(x856.segment) == "G62" AND ALLTRIM(x856.f1) == "64"
						* "MUST RESPOND BY" DATE & TIME
						m.RESPDATE = .ConvertYYYYMMDDStringToDate( ALLTRIM(x856.f2) )
						m.RESPTIME = .MakeAmPmTime( ALLTRIM(x856.f4) )
					CASE ALLTRIM(x856.segment) == "G61" AND ALLTRIM(x856.f1) == "IC" AND ALLTRIM(x856.f3) == "TE"
						m.PUNAME = ALLTRIM(x856.f2)
						m.PUPHONE = ALLTRIM(x856.f4)
					CASE ALLTRIM(x856.segment) == "G61" AND ALLTRIM(x856.f1) == "IC" AND ALLTRIM(x856.f3) == "EM"
						IF EMPTY(m.PUNAME) THEN
							m.PUNAME = ALLTRIM(x856.f2)
						ENDIF
						m.PUEMAIL = ALLTRIM(x856.f4)
					CASE ALLTRIM(x856.segment) == "S5"
						* we encountered a STOP segment
						* if current Stop # = 0, then this is the first for this 204 - just continue
						* otherwise, write a STOP detail rec before proceeding

						IF m.STOP = 0 THEN
							* continue
						ELSE
							* insert DETAIL, BUT NOT FOR BIGLOTS 2/23/12 M.B.
							IF NOT (.nAccountID = 4717) THEN && BIGLOTS
								m.ADDDT = DATETIME()
								INSERT INTO STOPS204 FROM MEMVAR
								.TrackProgress('Trip Detail Insert: STOP =' + TRANSFORM(m.STOP) + ',  PULOC = ' + m.PULOC + ',  DELLOC = ' + m.DELLOC,LOGIT+SENDIT)
								.clear_variables( "DETAIL" )
							ENDIF && NOT (.nAccountID = 4717)
						ENDIF

						m.STOP = VAL(ALLTRIM(x856.f1))
						lcStopType = UPPER(ALLTRIM(x856.f2))
						*!*	DO CASE
						*!*		CASE lcStopType = "CL"
						*!*			m.LOADTYPE = "COMPLETE LOAD"
						*!*		CASE lcStopType = "PL"
						*!*			m.LOADTYPE = "PART LOAD"
						*!*		CASE lcStopType = "CU"
						*!*			m.LOADTYPE = "COMPLETE UNLOAD"
						*!*		CASE lcStopType = "PU"
						*!*			m.LOADTYPE = "PART UNLOAD"
						*!*		OTHERWISE
						*!*			m.LOADTYPE = "?"
						*!*	ENDCASE
						DO CASE
							CASE INLIST(lcStopType,"CL","PL")
								m.LOADTYPE = "PICKUP"
							CASE INLIST(lcStopType,"CU","PU")
								m.LOADTYPE = "DELIVER"
							OTHERWISE
								m.LOADTYPE = "?"
						ENDCASE

						m.WEIGHT = VAL(ALLTRIM(x856.f3))
						m.QUANTITY = VAL(ALLTRIM(x856.f5))
						m.CUBE = VAL(ALLTRIM(x856.f7))
						IF UPPER(ALLTRIM(x856.f6)) == "CT" THEN
							m.QTY_TYPE = "CARTONS"
						ENDIF

					CASE ALLTRIM(x856.segment) == "G62" AND ALLTRIM(x856.f1) == "10"
						* "PICK UP" DATE & TIME
						m.PUAPPT = .ConvertYYYYMMDDStringToDate( ALLTRIM(x856.f2) )
						m.PUATIME = .MakeAmPmTime( ALLTRIM(x856.f4) )
						m.PUATIME1 = .MakeAmPmTime( ALLTRIM(x856.f4) )
						m.PU = .T.
						m.DEL = .F.
						*CASE ALLTRIM(x856.segment) == "G62" AND ALLTRIM(x856.f1) == "70"
						* ADDED 68 FOR BIGLOTS...
					CASE ALLTRIM(x856.segment) == "G62" AND ( (ALLTRIM(x856.f1) == "68") OR (ALLTRIM(x856.f1) == "70") )
						* "DELIVERY" DATE & TIME
						m.PUAPPT = .ConvertYYYYMMDDStringToDate( ALLTRIM(x856.f2) )
						m.PUATIME = .MakeAmPmTime( ALLTRIM(x856.f4) )
						m.PUATIME1 = .MakeAmPmTime( ALLTRIM(x856.f4) )
						m.PU = .F.
						m.DEL = .T.
					CASE ALLTRIM(x856.segment) == "N1" AND ALLTRIM(x856.f1) == "SF"
						m.SHIPTYPE = "SHIP FROM"
						m.DCNAME = ALLTRIM(x856.f2)
					CASE ALLTRIM(x856.segment) == "N1" AND ALLTRIM(x856.f1) == "ST"
						m.SHIPTYPE = "SHIP TO"
						m.DCNAME = ALLTRIM(x856.f2)
					CASE ALLTRIM(x856.segment) == "N3"
						m.PUADDRESS = ALLTRIM(x856.f1)
					CASE ALLTRIM(x856.segment) == "N4"
						*m.PULOC = ALLTRIM(x856.f1) + " " + ALLTRIM(x856.f2)
						*m.PUCSZ = ALLTRIM(x856.f1) + " " + ALLTRIM(x856.f2) + " " + ALLTRIM(x856.f3)
						m.PULOC = ALLTRIM(x856.f1) + ", " + ALLTRIM(x856.f2)
						m.PUCSZ = ALLTRIM(x856.f1) + ", " + ALLTRIM(x856.f2) + " " + ALLTRIM(x856.f3)
						* this could be the Pick up loc for a new leg, or the Delivery Loc

					OTHERWISE
						* NOTHING
				ENDCASE

			ENDSCAN  && OF x856

			*!*				IF USED('x856') THEN
			*!*					USE IN x856
			*!*				ENDIF

			DO CASE
				CASE .nAccountID = 4717  && BIGLOTS
					* nothing;
					* this processing is not needed for Biglots -- they do not have multiple stops, and we basically hardcode DEST as Tremont PA
				OTHERWISE
					* now do some processing of the tables we inserted into, to get things that were hard to get during the scan of x856 cursor.
					* get Origin, Destinatio, Origstate, Deststate for the Header recs.
					* First, for each Header record, search the STOPS204 recs for the PULOC from earliest STOP (should be #1)
					* and the DELLOC from the last STOP (could be any #)
					SELECT WOLOG204
					SCAN FOR PROC204CTR = m.PROC204CTR
						IF USED('CURMINSTOP') THEN
							USE IN CURMINSTOP
						ENDIF
						IF USED('CURMAXSTOP') THEN
							USE IN CURMAXSTOP
						ENDIF
						STORE "" TO lcORIGIN, lcDESTINATIO, lcORIGSTATE, lcDESTSTATE
						SELECT MIN(STOP) AS MINSTOP ;
							FROM STOPS204 ;
							INTO CURSOR CURMINSTOP ;
							WHERE PROC204CTR = m.PROC204CTR ;
							AND WOLOG204ID = WOLOG204.WOLOG204ID
						IF USED('CURMINSTOP') AND NOT EOF('CURMINSTOP') THEN
							lnMinSTOP = CURMINSTOP.MINSTOP
							SELECT STOPS204
							LOCATE FOR (STOP = lnMinSTOP) AND (PROC204CTR = m.PROC204CTR) AND (WOLOG204ID = WOLOG204.WOLOG204ID)
							IF FOUND() THEN
								lcORIGIN = ALLTRIM(STOPS204.PULOC)
								lcORIGSTATE = RIGHT(lcORIGIN,2)
							ENDIF
						ENDIF
						SELECT MAX(STOP) AS MAXSTOP ;
							FROM STOPS204 ;
							INTO CURSOR CURMAXSTOP ;
							WHERE PROC204CTR = m.PROC204CTR ;
							AND WOLOG204ID = WOLOG204.WOLOG204ID
						IF USED('CURMAXSTOP') AND NOT EOF('CURMAXSTOP') THEN
							lnMaxSTOP = CURMAXSTOP.MAXSTOP
							SELECT STOPS204
							LOCATE FOR (STOP = lnMaxSTOP) AND (PROC204CTR = m.PROC204CTR) AND (WOLOG204ID = WOLOG204.WOLOG204ID)
							IF FOUND() THEN
								lcDESTINATIO = ALLTRIM(STOPS204.PULOC)
								lcDESTSTATE = RIGHT(lcDESTINATIO,2)
							ENDIF
						ENDIF

						* log the errors
						IF EMPTY(lcORIGIN) THEN
							.TrackProgress('ERROR: unable to determine Origin for PROC204CTR = ' + TRANSFORM(m.PROC204CTR) + ',  WOLOG204ID = ' + TRANSFORM(WOLOG204.WOLOG204ID),LOGIT+SENDIT)
						ENDIF
						IF EMPTY(lcDESTINATIO) THEN
							.TrackProgress('ERROR: unable to determine Destination for PROC204CTR = ' + TRANSFORM(m.PROC204CTR) + ',  WOLOG204ID = ' + TRANSFORM(WOLOG204.WOLOG204ID),LOGIT+SENDIT)
						ENDIF

						* update the header origin/destination data fields
						REPLACE WOLOG204.ORIGIN WITH lcORIGIN, WOLOG204.ORIGSTATE WITH lcORIGSTATE, ;
							WOLOG204.DESTINATIO WITH lcDESTINATIO, WOLOG204.DESTSTATE WITH lcDESTSTATE ;
							IN WOLOG204

					ENDSCAN  && WOLOG204
			ENDCASE
		ENDWITH
	ENDPROC && process204



	FUNCTION Create990
		LPARAMETERS tcStatus
		LOCAL lc990OutputFolder, lc990FileName, lc990FileNameRoot
		LOCAL lcString, lcFd, lcSegd, lcSENDQUAL, lcSENDID, lcRECQUAL, lcSendIDlong, lcRECID, lcRecIDlong
		LOCAL lcTerminator, lcDate, lcTruncDate, lcTime1, lcTime, lcX12, lcSCAC, lcActionCode
		LOCAL lcProdQual, lcISA_Num, lcISA_Num2, lc_CntrlNum, lc_GrpCntrlNum, lnISA_Num
		LOCAL lcWoNum, lnAllSegCtr, lnSTSegCtr, lnGSSegCtr, lcVICS, lc_GrpCntrlNum2
		LOCAL lcStatus, lcPreFix, lcShipRef, lc_InterChglNum

		WAIT WINDOW TIMEOUT 2 "Creating 990 Load Tender Response with status = "  +  tcStatus
		* create 310 below


		WITH THIS

			*!*		IF .lTestMode THEN
			*!*			RETURN .F.  && to simulate an error in this method, for testing
			*!*		ENDIF

			lcActionCode = UPPER(LEFT(tcStatus,1))  && i.e., A or D for accepted or declined

			*!*		IF lcActionCode = "A" THEN
			*!*			lcWoNum = TRANSFORM(.NewWoNum)  && this would have been set in .CreateWorkOrder() for an Accept
			*!*		ELSE
			*!*			lcWoNum = ""
			*!*		ENDIF

			* get control #s, etc.
			USE F:\UTIL\LOADTENDER\DATA\CNTRL990 IN 0 ALIAS CNTRL990
			SELECT CNTRL990
			LOCATE FOR nAccountID = .nAccountID

			IF NOT FOUND() THEN
				.TrackProgress('ERROR: unable to find Control Record for AccountID = ' + TRANSFORM(.nAccountID),LOGIT+SENDIT)
				USE IN CNTRL990
				RETURN .F.
			ENDIF

			REPLACE CNTRL990.isanum WITH (CNTRL990.isanum + 1), ;
				CNTRL990.interchg WITH (CNTRL990.interchg + 1), ;
				CNTRL990.groupcntrl WITH (CNTRL990.groupcntrl + 1) IN CNTRL990

			lcISA_Num      = TRANSFORM(CNTRL990.isanum)
			lc_InterChglNum    = TRANSFORM(CNTRL990.interchg)
			lc_GrpCntrlNum = TRANSFORM(CNTRL990.groupcntrl)  && THIS CONTROL # WOULD BE USED FOR MULTIPLE ST SEGMENTS, IF WE HAD THEM (WE'RE NOT PLANNING TO)

			USE IN CNTRL990
			*****************************************************************************************

			lnISA_Num = INT(VAL(lcISA_Num))

			* set Values based on Account
			DO CASE
				CASE .nAccountID = 4717  && BIGLOTS

					lcShipRef = ALLTRIM(WOLOG204.SHIPREF)
					lcPreFix = "BL990_"
					lcFd = "*"  && field delimiter
					lcSegd = "~"  && segment delimiter
					lcTerminator = ">" && Used at end of ISA segment
					lcSENDQUAL = "02"
					lcSENDID = "FMIT"
					lcRECQUAL = "ZZ"
					lcDate = DTOS(DATE())
					lcTruncDate = RIGHT(lcDate,6)
					lcTime1 = TIME(DATE())
					lcTime = LEFT(lcTime1,2) + SUBSTR(lcTime1,4,2)
					lcTime2 = STRTRAN(lcTime1,":","")
					lcTime2 = STRTRAN(lcTime2,".","")
					lcX12 = "00401"  && X12 Standards Set used
					lcSCAC = "FMIT"  && FMI TRUCKING
					lcVICS = "004010"
					lcISA_Num = ALLTRIM(lcISA_Num)
					lcISA_Num2 = PADL(lcISA_Num,9,"0")

					** lc_GrpCntrlNum2 should not be less than 4 in length, per Mike Miller of hubgroup, on behalf of Biglots/Unyson
					lc_GrpCntrlNum2 = ALLTRIM(lc_GrpCntrlNum)
					IF LEN(lc_GrpCntrlNum2) < 4 THEN
						lc_GrpCntrlNum2 = PADL(lc_GrpCntrlNum2,4,'0')
					ENDIF

					IF .lTestMode THEN
						lc990OutputFolder = 'F:\UTIL\LOADTENDER\BIGLOTS\TEST990OUT\'
						lcProdQual = "T"
						lcRECID = "BLASI"
					ELSE
						lc990OutputFolder = 'F:\FTPUSERS\BIGLOTS\OUT\'
						lcProdQual = "P"
						lcRECID = "BLASI"
					ENDIF

				CASE .nAccountID = 647  && MACYS

					lcShipRef = ALLTRIM(WOLOG204.BROKREF)
					lcPreFix = "MACYS990_"
					lcFd = "*"  && field delimiter
					lcSegd = CHR(0x85)  && segment delimiter
					lcTerminator = ">" && Used at end of ISA segment
					lcSENDQUAL = "ZZ"
					lcSENDID = "FMIF"
					lcRECQUAL = "08"
					lcDate = DTOS(DATE())
					lcTruncDate = RIGHT(lcDate,6)
					lcTime1 = TIME(DATE())
					lcTime = LEFT(lcTime1,2) + SUBSTR(lcTime1,4,2)
					lcTime2 = STRTRAN(lcTime1,":","")
					lcTime2 = STRTRAN(lcTime2,".","")
					*lcX12 = "00501"  && X12 Standards Set used
					lcX12 = "00200"  && per PG
					lcSCAC = "FMIX"  && FMI EXPRESS
					lcVICS = "005010VICS"
					lcISA_Num = PADL(lcISA_Num,9,"0")
					lc_GrpCntrlNum2 = PADL(lc_GrpCntrlNum,9,"0")

					IF .lTestMode THEN
						lc990OutputFolder = 'F:\UTIL\LOADTENDER\MACYS\TEST990OUT\'
						lcProdQual = "T"
						lcRECID = "6113310099"
					ELSE
						lc990OutputFolder = 'F:\FTPUSERS\FED204\990OUT\'
						lcProdQual = "P"
						lcRECID = "6113310204"
					ENDIF

				OTHERWISE
					* ERROR
					.TrackProgress('ERROR: Invalid Account # = ' + TRANSFORM(.nAccountID),LOGIT+SENDIT)
					RETURN .F.
			ENDCASE


			** PAD ID Codes for the long version
			lcSendIDlong = PADR(lcSENDID,15," ")
			lcRecIDlong = PADR(lcRECID,15," ")

			*lc990FileNameRoot = "test990_" + lcDate + lcTime2 + ".TXT"
			lc990FileNameRoot = lcPreFix + lcDate + lcTime2 + ".EDI"
			lc990FileName = lc990OutputFolder  +  lc990FileNameRoot

			REPLACE WOLOG204.FILE990 WITH lc990FileName IN WOLOG204

			* open file for writing
			.n990FileHandle = FCREATE(lc990FileName)

			IF .n990FileHandle  = -1 THEN
				.TrackProgress("There was an error FCREATEing " + lc990FileName,LOGIT+SENDIT)
				RETURN .F.
			ELSE
				* file has been opened for low-level access, OK TO CONTINUE

				*****************************************************************************************

				STORE 0 TO lnAllSegCtr, lnSTSegCtr, lnGSSegCtr

				* write ISA segment
				STORE "ISA" + lcFd + "00" + lcFd + "          " + lcFd + "00" + lcFd + "          " + lcFd + lcSENDQUAL + lcFd + lcSendIDlong + lcFd + ;
					lcRECQUAL + lcFd + lcRecIDlong + lcFd + lcTruncDate + lcFd + lcTime + lcFd + "U" + lcFd + lcX12 + lcFd + ;
					lcISA_Num2 + lcFd + "0" + lcFd + lcProdQual + lcFd + lcTerminator + lcSegd TO lcString
				FPUTS(.n990FileHandle,lcString)

				* write GS segment
				STORE "GS" + lcFd + "GF" + lcFd + lcSENDID + lcFd + lcRECID + lcFd + lcTruncDate + lcFd + lcTime + lcFd + ;
					lc_InterChglNum + lcFd + "X" + lcFd + lcVICS + lcSegd TO lcString
				FPUTS(.n990FileHandle,lcString)
				lnGSSegCtr = lnGSSegCtr + 1

				* write ST segment
				*STORE "ST" + lcFd + "990" + lcFd + PADL(lc_GrpCntrlNum,9,"0") + lcSegd TO lcString
				STORE "ST" + lcFd + "990" + lcFd + lc_GrpCntrlNum2 + lcSegd TO lcString
				FPUTS(.n990FileHandle,lcString)
				lnAllSegCtr = lnAllSegCtr + 1
				lnSTSegCtr = lnSTSegCtr + 1

				* write B1 segment
				* (note - field 5 left blank per 990 spec)
				*STORE "B1" + lcFd + lcSCAC + lcFd + tcShipRef + lcFd + lcDate + lcFd + lcActionCode + lcFd + lcFd + tcReasonCode + lcSegd TO lcString
				DO CASE
					CASE .nAccountID = 4717  && BIGLOTS
						* FOR BIGLOTS PER 4010 SPEC, CAN STOP B1 SEG AFTER ACTION CODE -- NO NEED FOR REASON ON A DECLINE.
						*!*	IF lcActionCode = "A" THEN
						*!*		STORE "B1" + lcFd + lcSCAC + lcFd + lcShipRef + lcFd + lcDate + lcFd + lcActionCode + lcSegd TO lcString
						*!*	ELSE
						*!*		STORE "B1" + lcFd + lcSCAC + lcFd + lcShipRef + lcFd + lcDate + lcFd + lcActionCode + lcFd + lcFd + tcReasonCode + lcSegd TO lcString
						*!*	ENDIF
						STORE "B1" + lcFd + lcSCAC + lcFd + lcShipRef + lcFd + lcDate + lcFd + lcActionCode + lcSegd TO lcString
					CASE .nAccountID = 647  && MACYS
						STORE "B1" + lcFd + lcSCAC + lcFd + lcShipRef + lcFd + lcDate + lcFd + lcActionCode + lcFd + lcFd + tcReasonCode + lcSegd TO lcString
				ENDCASE
				FPUTS(.n990FileHandle,lcString)
				lnAllSegCtr = lnAllSegCtr + 1

				IF lcActionCode = "A" THEN
					* write L11 segment for an Accept
					* (NOTE: CO = Customer Order Number)
					DO CASE
						CASE .nAccountID = 4717  && BIGLOTS
							* nothing -- no L11 segment needed
						CASE .nAccountID = 647  && MACYS
							STORE "L11" + lcFd + lcWoNum + lcFd + "CO" + lcSegd TO lcString
							FPUTS(.n990FileHandle,lcString)
							lnAllSegCtr = lnAllSegCtr + 1
					ENDCASE
				ENDIF

				* write SE segment
				lnAllSegCtr = lnAllSegCtr + 1
				*STORE "SE" + lcFd + ALLTRIM(STR(lnAllSegCtr)) + lcFd + PADL(lc_GrpCntrlNum,9,"0") + lcSegd TO lcString
				STORE "SE" + lcFd + ALLTRIM(STR(lnAllSegCtr)) + lcFd + lc_GrpCntrlNum2 + lcSegd TO lcString
				FPUTS(.n990FileHandle,lcString)

				* write GE segment
				STORE  "GE" + lcFd + ALLTRIM(STR(lnSTSegCtr)) + lcFd + lc_InterChglNum + lcSegd TO lcString
				FPUTS(.n990FileHandle,lcString)

				* write IEA segment
				STORE  "IEA" + lcFd + ALLTRIM(STR(lnGSSegCtr)) + lcFd + lcISA_Num2 + lcSegd TO lcString
				FPUTS(.n990FileHandle,lcString)

				* force Windows to flush buffers immediately
				=FFLUSH(.n990FileHandle, .T.)

				* close the 990 file
				IF FCLOSE(.n990FileHandle) THEN
					.n990FileHandle = 0
				ELSE
					.TrackProgress("There was an error FCLOSEing " + lc990FileName,LOGIT+SENDIT)
				ENDIF

				IF FILE(lc990FileName) THEN
					lcStatus = "990 ["  +  lc990FileName  +  "] was successfully created"
					lcNotes = ALLTRIM(WOLOG204.NOTES) + CRLF + lcStatus
					REPLACE NOTES WITH lcNotes IN WOLOG204
					
					.lMadeA990 = .T.

					* Moved outside of loop processing so it is done only once per process 4/5/12 MB
					*.TriggerFTPUploadBIGLOTS()

				ELSE
					lcStatus = "ERROR: 990 ["  +  lc990FileName  +  "] was not created"
					.TrackProgress("ERROR: 990 ["  +  lc990FileName  +  "] was not created",LOGIT+SENDIT)
					lcNotes = ALLTRIM(WOLOG204.NOTES) + CRLF + lcStatus
					REPLACE NOTES WITH lcNotes IN WOLOG204
					RETURN .F.
				ENDIF

			ENDIF && .n990FileHandle  = -1

		ENDWITH

		RETURN .T.
	ENDFUNC && Create990


	FUNCTION CreateTruckingWorkOrder
		PRIVATE m.WOLOGID, m.WO_NUM, m.WO_DATE, m.OFFICE
		LOCAL lcStatus, lcNotes

		* create Work order and child trips below
		WAIT WINDOW TIMEOUT 1 "Creating Trucking Work Order..."

		WITH THIS

			* get WOLOGID and WO_NUM for the insert
			SELECT GENERATEPK
			LOCATE FOR GPK_PK = "WOLOG"
			IF FOUND() THEN
				REPLACE GPK_CURRENTNUMBER WITH (GPK_CURRENTNUMBER + 1) IN GENERATEPK
				m.WOLOGID = GENERATEPK.GPK_CURRENTNUMBER
			ELSE
				.TrackProgress("Error getting new WOLOGID",LOGIT+SENDIT)
				RETURN .F.
			ENDIF

			SELECT GENERATEPK
			LOCATE FOR GPK_PK = "WONUM"
			IF FOUND() THEN
				REPLACE GPK_CURRENTNUMBER WITH (GPK_CURRENTNUMBER + 1) IN GENERATEPK
				m.WO_NUM = GENERATEPK.GPK_CURRENTNUMBER

				* save to property for use at higher level
				.nNewWoNum = m.WO_NUM
			ELSE
				.TrackProgress("Error getting new WO_NUM",LOGIT+SENDIT)
				RETURN .F.
			ENDIF

			*******************************************************************
			* if we get to here we can do the insert

			* DON'T CARRY THESE VARIABLES OVER FROM CALLING PROG -- JUST RESET
			m.WO_DATE = DATE()
			m.UPDPROC = ""
			m.UPDATEDT = {}
			m.UPDATEBY = ""
			m.ADDDT = DATETIME()

			* force these for Big Lots Trucking WO
			m.OFFICE = "N"
			m.TYPE = 'O'
			m.QUANTITY = 1
			m.BILLTOID = 0
			m.TOLOC = 1  && TO LOC #1 FOR BIGLOTS = TREMONT PA DC
			m.tocs = "TREMONT, PA"
			m.QTY_TYPE = "CONTRS"
			m.PULOC = ""
			m.RETLOC = ""
			m.REMARKS = "For " + m.PUADDRESS + " " + m.PUPHONE
			m.NOTES = "PO*" + m.PONUM + CRLF + "APPT*" + m.SHIPREF
			m.APPTNUM = m.SHIPREF
			m.ADDBY = .cUSERNAME
			m.ADDPROC = "EDI204"
			m.wostatus = "NOT AVAILABLE"
			m.priority = 5

			INSERT INTO TRUCKWOLOG FROM MEMVAR

			lcStatus = "Created WO # " + TRANSFORM(.nNewWoNum)
			lcNotes = ALLTRIM(WOLOG204.NOTES) + CRLF + "PO*" + m.PONUM + CRLF + lcStatus
			
			REPLACE WO_NUM WITH .nNewWoNum, ;
				WO_DATE WITH m.WO_DATE, ;
				NOTES WITH lcNotes ;
				IN WOLOG204

			*******************************************************************
			*******************************************************************

			*IF .lTestMode THEN

			* per PG, insert into EDI Trigger
			*!*	  Big Lots Acct: 4717

			*!*	  We need to trigger a 214 upon receipt of a 204 where that 204 creates a new work order.

			*!*	Some sample code as below.......

			*!*	This trigger will send BigLots an estimated appointment date of the current date +6

			*!*	Select edi_trigger
			*!*	Scatter memvar memo blank
			*!*	Append Blank

			*!*	m.accountid = 4717
			*!*	m.consignee = "BIG LOTS"
			*!*	m.trig_from  = "MANUAL"
			*!*	m.trig_time  = Datetime()
			*!*	m.trig_by    = "PGAIDIS"
			*!*	m.processed  = .F.
			*!*	m.style      = "AB"
			*!*	m.edi_type   = "214B"
			*!*	m.wo_num     = Val(Alltrim(Transform(work order number)))

			PRIVATE m.ACCOUNTID, m.consignee, m.trig_from, m.trig_by, m.processed, m.style, m.edi_type, m.ship_ref

			DO CASE
				CASE .nAccountID = 4717  && BIGLOTS

					
					*!*	* THIS DOESN'T WORK BECAUSE IT BlANKS SOME M.VARS USED IN CALLING PROC...
					*!*	SCATTER MEMVAR MEMO BLANK

					*!* SELECT EDI_TRIGGER
					*!*	m.ACCOUNTID  = .nAccountID
					*!*	m.consignee  = "BIG LOTS"
					*!*	m.trig_from  = "MANUAL"
					*!*	m.trig_time  = DATETIME()
					*!*	m.trig_by    = "PGAIDIS"
					*!*	m.processed  = .F.
					*!*	m.style      = "AB"
					*!*	m.edi_type   = "214B"
					*!*	m.WO_NUM     = .nNewWoNum
					*!*	m.ship_ref   = "INIT"
					*!*	m.office     = "N"

					*!*	INSERT INTO EDI_TRIGGER FROM MEMVAR
					
					SELECT EDI_TRIGGER
					APPEND BLANK
					REPLACE ACCOUNTID WITH .nAccountID, ;
						consignee WITH "BIG LOTS", ;
						trig_from WITH "MANUAL", ;
						trig_time WITH DATETIME(), ;
						trig_by WITH "PGAIDIS", ;
						processed WITH .F., ;
						style WITH "AB", ;
						edi_type WITH "214B", ;
						WO_NUM WITH .nNewWoNum, ;
						ship_ref WITH "INIT", ;
						office WITH "N" ;
						IN EDI_TRIGGER

					.TrackProgress('Did FoxPro EDI_TRIGGER 214 Insert!',LOGIT+SENDIT)
					
					* add a record to sql edi_trigger
					SELECT sqleditrigger
					SCATTER MEMVAR MEMO
					
					m.ACCOUNTID  = .nAccountID
					m.consignee  = "BIG LOTS"
					m.trig_from  = "MANUAL"
					m.trig_time  = DATETIME()
					m.trig_by    = "PGAIDIS"
					m.processed  = .F.
					m.style      = "AB"
					m.edi_type   = "214B"
					m.WO_NUM     = .nNewWoNum
					m.ship_ref   = "INIT"
					m.office     = "N"
					m.edi_triggerid=sqlgenpk("edi_trigger","stuff")
					
					insertinto("sqleditrigger","stuff")
					
					.TrackProgress('Did SQL EDI_TRIGGER 214 Insert!',LOGIT+SENDIT)

				OTHERWISE
					* nothng
			ENDCASE

			*ENDIF && .lTestMode

			*******************************************************************
			*******************************************************************

		ENDWITH

		RETURN .T.
	ENDFUNC && CreateTruckingWorkOrder


	PROCEDURE clear_variables
		LPARAMETERS tcWhichtype
		WITH THIS

			IF INLIST(tcWhichtype,"ALL","HEADER")
				STORE "" TO m.SHIPREF, m.BROKREF, m.SCAC, m.EDIType, m.PURPCODE, m.PULOC, m.DELLOC, m.RESPTIME, m.VESSEL, m.MBLNUM, m.CONTAINER, m.PONUM
				STORE "" TO m.TRPULOC, m.TRRETLOC, m.APPTTM
				STORE 0 TO m.TRFROMLOC, m.TRTOLOC, m.WO_NUM
				STORE CTOD('') TO m.RESPDATE, m.ADDDT, m.UPDATEDT, m.PICKEDUP, m.DELIVERED, m.APPT, m.WO_DATE
				STORE .F. TO m.FOUNDWO
			ENDIF

			IF INLIST(tcWhichtype,"ALL","DETAIL")
				STORE 0 TO m.STOP, m.WEIGHT, m.QUANTITY, m.CUBE
				STORE "" TO m.PULOC, m.DELLOC, m.PUATIME, m.DELATIME, m.PUATIME1, m.DELATIME1, m.QTY_TYPE, m.LOADTYPE
				STORE "" TO m.PUNAME, m.PUPHONE, m.PUEMAIL, m.PUADDRESS, m.PUCSZ
				*STORE "" TO m.DELNAME, m.DELPHONE, m.DELEMAIL, m.DELADDRESS, m.DELCSZ
				STORE .F. TO m.PU, m.DEL
				STORE CTOD('') TO m.PUAPPT, m.DELAPPT
			ENDIF

		ENDWITH
	ENDPROC  && clear_variables


	FUNCTION SetClient
		LPARAMETERS tcClient
		LOCAL llRetVal, lcMinutes
		llRetVal = .T.
		*lcMinutes = "_" + SUBSTR(TIME(),4,2)
		
		* get accountid for clients...
		*USE F:\SYSDATA\QQDATA\account

		IF EMPTY( tcClient ) THEN
			llRetVal = .F.
			.TrackProgress('ERROR - missing or invalid CLIENT parameter!',LOGIT+SENDIT)
		ELSE
			DO CASE
				CASE UPPER(ALLTRIM( tcClient )) == "MACYS"
					.cLogFile = 'F:\UTIL\LOADTENDER\MACYS\LOGFILES\Edi204ImportProcess_LOG_' + ALLTRIM(.cCOMPUTERNAME) + '.TXT'
					.nAccountID = 647
					.cAcctname = "FEDERATED LOGISTICS - MACY'S"
					.lExpress = .T.
					.cInputFileSpec = '204*.EDI'
					
					.cInputFolder = 'F:\FTPUSERS\MACYS\204IN\'
					.cArchiveFolder = 'F:\FTPUSERS\MACYS\204IN\ARCHIVE\'
					.cHoldFolder = 'F:\FTPUSERS\MACYS\HOLD204\'
					
					.cSendTo = 'mbennett@fmiint.com'
					.cCC = ''
					*.cInternalSendTo = 'renee.zamanski@tollgroup.com, don.kistner@tollgroup.com, fran.castro@tollgroup.com, sharon.strojny@tollgroup.com'
					.cInternalSendTo = 'renee.zamanski@tollgroup.com, rzamanski@fmiint.com, sharon.strojny@tollgroup.com, don.kistner@tollgroup.com'
					.cInternalCC = 'mbennett@fmiint.com'
					IF .lTestMode THEN
						.cInputFolder = 'F:\UTIL\LOADTENDER\MACYS\TEST204IN\'
						.cArchiveFolder = 'F:\UTIL\LOADTENDER\MACYS\TEST204IN\ARCHIVED\'
						.cHoldFolder = 'F:\UTIL\LOADTENDER\MACYS\HOLD204\'
						
						*.cInputFolder = 'F:\FTPUSERS\FED204\204IN\'
						*.cArchiveFolder = 'F:\FTPUSERS\FED204\204IN\ARCHIVE\'
						.cSendTo = 'mbennett@fmiint.com'
						.cCC = 'mbennett@fmiint.com'
						.cInternalSendTo = 'mbennett@fmiint.com'
						.cInternalCC = 'mbennett@fmiint.com'
						.cLogFile = 'F:\UTIL\LOADTENDER\MACYS\LOGFILES\Edi204ImportProcess_LOG_TESTMODE_' + ALLTRIM(.cCOMPUTERNAME) + '.TXT'
					ENDIF
				CASE UPPER(ALLTRIM( tcClient )) == "BIGLOTS"
				
					* changed 5/16/13 MB
					* we will put BIGLOTS data in its own separate folder because there is a lot of it and it is not used by the LOADTENDER form.
					.c204ProcHeaderTable = 'F:\UTIL\LOADTENDER\BIGLOTS\DATA\BLWOLOG204'
					.c204ProcStopsTable = 'F:\UTIL\LOADTENDER\BIGLOTS\DATA\BLSTOPS204'
					
					.cLogFile = 'F:\UTIL\LOADTENDER\BIGLOTS\LOGFILES\Edi204ImportProcess_LOG_' + ALLTRIM(.cCOMPUTERNAME) + '.TXT'
					.nAccountID = 4717
					.cAcctname = "BIG LOTS"
					.lExpress = .F.  &&  per PG, we will need to create Trucking Work Orders
					
					.cInputFolder = 'F:\FTPUSERS\BIGLOTS\IN\'
					.cArchiveFolder = 'F:\FTPUSERS\BIGLOTS\IN\ARCHIVE\'
					.cHoldFolder = 'F:\FTPUSERS\BIGLOTS\HOLD204\'
					
					.cInputFileSpec = 'BLASIFMIT*.EDI'
					.cSendTo = 'mdivirgilio@fmiint.com,nadine.donovan@tollgroup.com,ipagurek@fmiint.com,ocruz@fmiint.com,mdrew@fmiint.com,mike.drew@tollgroup.com'
					.cCC = 'mbennett@fmiint.com,pgaidis@fmiint.com'
					.cInternalSendTo = 'mbennett@fmiint.com'
					.cInternalCC = 'mbennett@fmiint.com'
					IF .lTestMode THEN
						.cInputFolder = 'F:\UTIL\LOADTENDER\BIGLOTS\TEST204IN\'
						.cArchiveFolder = 'F:\UTIL\LOADTENDER\BIGLOTS\TEST204IN\ARCHIVED\'
						.cHoldFolder = 'F:\UTIL\LOADTENDER\BIGLOTS\HOLD204\'
						
						.cSendTo = 'mbennett@fmiint.com'
						.cCC = 'mbennett@fmiint.com'
						.cInternalSendTo = 'mbennett@fmiint.com'
						.cInternalCC = 'mbennett@fmiint.com'
						.cLogFile = 'F:\UTIL\LOADTENDER\BIGLOTS\LOGFILES\Edi204ImportProcess_LOG_TESTMODE_' + ALLTRIM(.cCOMPUTERNAME) + '.TXT'
					ENDIF
				CASE UPPER(ALLTRIM( tcClient )) == "VF"
					.cLogFile = 'F:\UTIL\LOADTENDER\VF\LOGFILES\Edi204ImportProcess_LOG_' + ALLTRIM(.cCOMPUTERNAME) + '.TXT'
					.nAccountID = 687
					.cAcctname = "VF SPORTSWEAR"
					.lExpress = .F.  &&  per PG, we will need to create Trucking Work Orders
					.cInputFolder = 'F:\FTPUSERS\VF\204IN\'
					.cArchiveFolder = 'F:\FTPUSERS\VF\204IN\ARCHIVE\'
					.cHoldFolder = 'F:\FTPUSERS\VF\HOLD204\'
					
					.cInputFileSpec = '*.TXT'   &&& ??????????????????????????????????
					.cSendTo = 'mdivirgilio@fmiint.com, nadine.donovan@tollgroup.com, irene.pagurek@tollgroup.com, olga.cruz@tollgroup.com, mike.drew@tollgroup.com'
					.cCC = 'mbennett@fmiint.com,pgaidis@fmiint.com'
					.cInternalSendTo = 'mbennett@fmiint.com'
					.cInternalCC = 'mbennett@fmiint.com'
					IF .lTestMode THEN
						.cInputFolder = 'F:\UTIL\LOADTENDER\VF\TEST204IN\'
						.cArchiveFolder = 'F:\UTIL\LOADTENDER\VF\TEST204IN\ARCHIVED\'
						.cHoldFolder = 'F:\UTIL\LOADTENDER\VF\HOLD204\'
						
						.cSendTo = 'mbennett@fmiint.com'
						.cCC = 'mbennett@fmiint.com'
						.cInternalSendTo = 'mbennett@fmiint.com'
						.cInternalCC = 'mbennett@fmiint.com'
						.cLogFile = 'F:\UTIL\LOADTENDER\BIGLOTS\LOGFILES\Edi204ImportProcess_LOG_TESTMODE_' + ALLTRIM(.cCOMPUTERNAME) + '.TXT'
					ENDIF
				OTHERWISE
					llRetVal = .F.
					.TrackProgress('ERROR - Unexpected CLIENT parameter = ' + tcClient,LOGIT+SENDIT)
			ENDCASE
		ENDIF && EMPTY( tcClient )

		RETURN llRetVal
	ENDFUNC


	PROCEDURE TriggerFTPUploadBIGLOTS
		* trigger the FTP upload process
		IF .lTestMode THEN
			*!*	INSERT INTO F:\edirouting\ftpjobs (jobname) VALUES ("990-TO-BIGLOTS-TEST")
			.TrackProgress('Triggered FTP job 990-TO-BIGLOTS-TEST', LOGIT+SENDIT)
		ELSE
			* production
			INSERT INTO F:\edirouting\ftpjobs (jobname) VALUES ("990-TO-BIGLOTS-PROD")
			.TrackProgress('Triggered FTP job 990-TO-BIGLOTS-PROD', LOGIT+SENDIT)
		ENDIF
		RETURN
	ENDPROC


	FUNCTION MakeAmPmTime
		LPARAMETERS tcTimeString
		* tcTimeString is 24 hour HHMM, like 0907 or 1506; convert to 12 hour plus A or P
		LOCAL lcHours, lcMinutes, lcRetVal

		lcHours = LEFT(tcTimeString,2)
		lcMinutes = RIGHT(tcTimeString,2)

		DO CASE
			CASE lcHours = "12"
				lcRetVal = lcHours + lcMinutes + "P"
			CASE lcHours = "00"
				lcRetVal = "12" + lcMinutes + "A"
			CASE lcHours < "12"
				lcRetVal = lcHours + lcMinutes + "A"
			OTHERWISE
				lcRetVal = PADL(VAL(lcHours)-12,2,'0')+ lcMinutes + "P"
		ENDCASE

		RETURN lcRetVal
	ENDFUNC


	PROCEDURE TrackProgress
		* do any combination of Wait Window, writing to logfile, and adding to body of email,
		* based on nFlags parameter.
		LPARAMETERS tcExpression, tnFlags
		WITH THIS
			IF BITAND(tnFlags,LOGIT) = LOGIT THEN
				IF .lLoggingIsOn THEN
					?
					? .cProcessName + "  " + TTOC(DATETIME()) + ": " + tcExpression
				ENDIF
			ENDIF
			IF .lWaitWindowIsOn THEN
				IF BITAND(tnFlags,NOWAITIT) = NOWAITIT THEN
					WAIT WINDOW tcExpression NOWAIT
				ENDIF
				IF BITAND(tnFlags,WAITIT) = WAITIT THEN
					WAIT WINDOW tcExpression TIMEOUT .nWaitWindowTimeout
				ENDIF
			ENDIF
			IF BITAND(tnFlags,SENDIT) = SENDIT THEN
				*IF .lSendInternalEmailIsOn THEN
				.cBodyText = .cBodyText + tcExpression + CRLF + CRLF
				*ENDIF
			ENDIF
		ENDWITH
	ENDPROC  &&  TrackProgress


	PROCEDURE SortArrayByDateTime
		* expects taInArray[] to be the result of an ADIR()
		LPARAMETERS taInArray, taOutArray
		EXTERNAL ARRAY taInArray, taOutArray
		LOCAL lnRows, lnCols, i, j
		* extending taInArray[] to process the files in date/time order
		lnRows = ALEN(taInArray,1)
		lnCols = ALEN(taInArray,2)
		* create taOutArray with one more column
		DIMENSION taOutArray[lnRows,lnCols + 1]
		* and fill it with taInArray columns AND (filedate + filetime) in new column
		* ( ACOPY() doesn't preserve elements properly because of different # of columns )
		FOR i = 1 TO lnRows
			FOR j = 1 TO lnCols
				taOutArray[i,j] = taInArray[i,j]
			ENDFOR
			taOutArray[i,lnCols + 1] = DTOS(taInArray[i,3]) + "_" + taInArray[i,4]
		ENDFOR
		* sort taOutArray on date/time column
		=ASORT(taOutArray,6)
	ENDPROC


ENDDEFINE

*!*		FUNCTION CombineIntoDateTime
*!*			LPARAMETERS tcDateString, tcTimeString
*!*			* take separate date and time strings and return a date-time variable
*!*			* tcDateString like YYYYMMDD
*!*			* tcTimeString like 1506 (24 hour time)
*!*			*   2011/06/16 14:53:44
*!*			WITH THIS
*!*				LOCAL ltDateTime, lcYear, lcMonth, lcDay, lcTimeString, lcHour, lcMinute, lcDateTime
*!*				lcYear = LEFT(tcDateString,4)
*!*				lcMonth = SUBSTR(tcDateString,5,2)
*!*				lcDay = RIGHT(tcDateString,2)
*!*				lcTimeString = PADL(tcTimeString,4,'0')
*!*				lcHour = LEFT(lcTimeString,2)
*!*				lcMinute = RIGHT(lcTimeString,2)
*!*				lcDateTime = lcYear + "/" + lcMonth + "/" + lcDay  + " " + lcHour + ":" + lcMinute + ":00"
*!*				ltDateTime = CTOT(lcDateTime)
*!*			ENDWITH
*!*			RETURN ltDateTime
*!*		ENDFUNC
