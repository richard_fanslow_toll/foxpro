lparameters xdriver

if !empty(xdriver)
	select employee
	locate for active and employee=xdriver
	if found() and !empty(dispmsg)
		xdrivermsg=.t.
		if x3winmsg(trim(xdriver)+" - "+dispmsg+crlf+crlf+"entered by "+username(dispmsgenterby)+" on "+dtoc(dispmsgdt) + ;
			crlf+crlf+crlf+crlf+"Would you like to remove this message?"+crlf,,"Y|N")="YES"
				replace dispmsg with "", dispmsgenterby with "", dispmsgdt with {} in employee
		endif
	else
		xsqlexec("select * from odrivers where driver='"+xdriver+"' and active=1 and !empty(dispmsg)","xodrivers",,"oo")
		if reccount()#0
			x3winmsg(trim(xdriver)+" - "+dispmsg+crlf+crlf+"entered by "+username(dispmsgenterby)+" on "+dtoc(dispmsgdt))
		endif
	endif
endif

