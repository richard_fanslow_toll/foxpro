*tlocaldriver
*activity for a driver for any period
*plus estimate of revenue produced
*against estimated number of hours worked per day.

*Pier Moves:
*Driver gets 100% of the move if he picks the container up, delivers and returns the empty same day.
*driver gets a percentage of the move if he only picks up, or delivers, or brings container back to FMI, or returns empty to the
*pier.  The percentage is based on how many moves it took to perform the work, if two then 50%, if three then 33%, etc.

*OUTBOUND TO CONSOL
*DRIVER ON DELIVERY GETS 2/3 OF THE  TOTAL MANIFESTED REVENUE (1/6 TO THE PICKUP AND 1/6 LEFT TO COVER WAREHOUSING COSTS)

*shuttles from JFK to NJ get all the Trucking posting if the account is transloaded in Carteret.
*if the account requires a further local delivery, the revenue is split into two moves.

*the total billing is taken from invdet except for rated manifests.
*Rated manifests are bulk billed and there is no table defining the individual posting of work orders within the manifest.
*for this reason, the orig_wo has to be rated and posting determined from the rate table.

*this program is called from a form that defines the dates and whether all drivers or a single driver is indicated

parameters tstart,tend,tdriver
store tstart  to tstartdate
store tend to tenddate
if tdriver=space(30)
	store .t. to alldrivers
else
	store .f. to alldrivers
endif



if alldrivers
	select * from f:\wo\wodata\tdaily where between(dispatched,tstart,tend) and office='N' and !empty(dis_driver) and !dis_driver='FMI' and !retloc='TURN' into cursor ttdaily
else
	select * from f:\wo\wodata\tdaily where dis_driver=tdriver and between(dispatched,tstart,tend) and office='N' and !retloc='TURN' into cursor ttdaily
endif

copy to h:\fox\ttdaily
use in tdaily
use in ttdaily
use h:\fox\ttdaily exclusive
index on dis_driver+str(year(dispatched),4,0)+str(month(dispatched),2,0)+str(day(dispatched),2,0) tag all
go top


use f:\wo\wodata\tdaily order dispatched in 0
use f:\wo\wodata\manifest order orig_wo in 0
use f:\wo\wodata\wolog order wo_num in 0

xsqlexec("select * from dellocs",,,"wo")
index on str(accountid,4)+location tag acct_loc
set order to

xsqlexec("select * from airfrt",,,"wo")
xsqlexec("select * from rates",,,"wo")

* remove due to SQL upgrade dy 11/15/16
*use f:\whi\whdata\inwolog order wo_num in 0

email("Dyoung@fmiint.com","tlocaldriver ran",,,,,.t.,,,,,.t.,,.t.)

create cursor localdr (driver c(30), dispatched d, wo_num n(10,0), acctname c(30), action c(20),trailer c(10),container c(10), revenue n(10,2), revapply n(10,2), pcnt n(10,2), nummoves n(10,0), thisdriv n(10,0), avgrevpday n(10,0),repo c(100))

select ttdaily

do while !eof()
	store dispatched to tdate
	store dis_driver to tdriver

	do while dis_driver=tdriver and dispatched=tdate and !eof()

		store recno() to trec
		store wo_num to two
		store trailer to ttrailer
		store container to tcontainer
		select localdr
		if two=0
			locate for driver=tdriver and wo_num=two and (trailer=ttrailer or container=tcontainer) and dispatched=tdate
		else
			locate for driver=tdriver and wo_num=two and dispatched=tdate
		endif

		if found()
			select ttdaily
			skip
			loop
		endif
		store space(50) to trepo
		select ttdaily
		store 0 to ttrev


*If the action is a reposition
		if two=0 and action='RT'
			store 'REPO' to tname

			store at(",",startloc) to tnum
			if tnum>0
				store substr(startloc,1,tnum-1) to tstartloc
			else
				store alltrim(startloc) to tstartloc
			endif

			store at(",",delloc) to tnum
			if tnum>0
				store substr(delloc,1,tnum-1) to tdelloc
			else
				store alltrim(delloc) to tdelloc
			endif
			store alltrim(trailer)+'-'+alltrim(tstartloc)+'-'+alltrim(tdelloc) to trepo

			store 1 to trevenueactions
			store 1 to tthisdriver
			store 'RT' to taction
			store 0 to ttrev

		else &&not repo


			store acctname to tname


*If the action is Outbound to Consolidators

			if acctname='TO CONSOLIDATORS'

				tthisdriver=2
				trevenueactions=3
*Since we do not dispatch the pickup of cargo for "Outbound to COnsolidators", we need to search the f:\outbound\obdata\inbound file
*for an record with the same inbound container or pu trailer, and if found, add one more action to this container or trailer.

				if usesqlbl()
					xsqlexec("select * from delivery where del_mfst='"+transform(two),,,"wo")
				else
					use f:\wo\wodata\delivery in 0
					select delivery
					set filter to val(del_mfst)=two
					go top
				endif
				
				if !eof()
					copy to h:\fox\obtemp while val(del_mfst)=two

					use h:\fox\obtemp in 0 exclusive
					select obtemp
					index on deliverto tag dt unique
					go top
					if at(",",deliverto)>0
						store substr(deliverto,1,at(",",deliverto)-1) to tdeliverto
					else
						trepo=alltrim(deliverto)
					endif

					skip
					do while !eof()
						if at(",",deliverto)>0
							trepo=trepo+','+substr(deliverto,1,at(",",deliverto)-1)
						else
							trepo=trepo+alltrim(deliverto)
						endif
						skip
					enddo
					use in obtemp

				endif
				use in delivery
				select ttdaily


			else &&not outbound to consol





*determine how many revenue actions were performed for this work order, and if they were performed by the same driver

				select tdaily
				set order to wo_num
				seek two
				store 0 to trevenueactions,tthisdriver,ttrev
				store space(20) to taction


				do while wo_num=two and !eof()
					if (action='MT' and retloc='TURNED OVER') or inlist(action,"MN","RT","XX") or (action='MT' and emptynul(returned))
						skip
						loop
					endif
					if dis_driver=tdriver
						do case
						case empty(trepo) and action='PU' and !empty(puloc)
							store at(",",startloc) to tnum
							if tnum>0
								store substr(startloc,1,tnum-1) to tstartloc
							else
								store alltrim(startloc) to tstartloc
							endif

							store at(",",endloc) to tnum
							if tnum>0
								store substr(endloc,1,tnum-1) to tendloc
							else
								store alltrim(endloc) to tendloc
							endif

							store at(",",puloc) to tnum
							if tnum>0
								store substr(puloc,1,tnum-1) to tpuloc
							else
								store alltrim(puloc) to tpuloc
							endif

							store alltrim(tpuloc)+' '+alltrim(tstartloc)+'-'+alltrim(tendloc) to trepo

						case action='PU' and !empty(puloc) and !empty(trepo)
							store at(",",startloc) to tnum
							if tnum>0
								store substr(startloc,1,tnum-1) to tstartloc
							else
								store alltrim(startloc) to tstartloc
							endif

							store at(",",endloc) to tnum
							if tnum>0
								store substr(endloc,1,tnum-1) to tendloc
							else
								store alltrim(endloc) to tendloc
							endif

							store at(",",puloc) to tnum
							if tnum>0
								store substr(puloc,1,tnum-1) to tpuloc
							else
								store alltrim(puloc) to tpuloc
							endif

							store alltrim(trepo)+'/'+alltrim(tpuloc)+'-'+alltrim(tstartloc)+'-'+alltrim(tendloc) to trepo

						case empty(trepo) and action='DL' and !empty(droploc) and startloc='CARTERET'
							store at(",",startloc) to tnum
							if tnum>0
								store substr(startloc,1,tnum-1) to tstartloc
							else
								store alltrim(startloc) to tstartloc
							endif

							store at(",",droploc) to tnum
							if tnum>0
								store substr(droploc,1,tnum-1) to tdroploc
							else
								store alltrim(droploc) to tdroploc
							endif

							store alltrim(tstartloc)+'-'+alltrim(tdroploc)+' '+alltrim(endloc) to trepo


						case action='DL' and !empty(droploc) and startloc='CARTERET' and !empty(trepo)

							store at(",",startloc) to tnum
							if tnum>0
								store substr(startloc,1,tnum-1) to tstartloc
							else
								store alltrim(startloc) to tstartloc
							endif

							store at(",",droploc) to tnum
							if tnum>0
								store substr(droploc,1,tnum-1) to tdroploc
							else
								store alltrim(droploc) to tdroploc
							endif

							store alltrim(trepo)+'/'+alltrim(tstartloc)+'-'+alltrim(tdroploc)+' '+alltrim(endloc) to trepo

						case empty(trepo) and action='DL' and empty(droploc) and startloc='CARTERET'
							store at(",",startloc) to tnum
							if tnum>0
								store substr(startloc,1,tnum-1) to tstartloc
							else
								store alltrim(startloc) to tstartloc
							endif

							store at(",",endloc) to tnum
							if tnum>0
								store substr(endloc,1,tnum-1) to tendloc
							else
								store alltrim(endloc) to tendloc
							endif






							store alltrim(tstartloc)+'-'+alltrim(retloc)+' '+alltrim(tendloc) to trepo

						case action='DL' and empty(droploc) and startloc='CARTERET' and !empty(trepo)
							store at(",",startloc) to tnum
							if tnum>0
								store substr(startloc,1,tnum-1) to tstartloc
							else
								store alltrim(startloc) to tstartloc
							endif

							store at(",",endloc) to tnum
							if tnum>0
								store substr(endloc,1,tnum-1) to tendloc
							else
								store alltrim(endloc) to tendloc
							endif





							store alltrim(trepo)+'/'+alltrim(tstartloc)+'-'+alltrim(retloc)+' '+alltrim(tendloc) to trepo


						case empty(trepo) and action='MT'
							store at(",",startloc) to tnum
							if tnum>0
								store substr(startloc,1,tnum-1) to tstartloc
							else
								store alltrim(startloc) to tstartloc
							endif

							store at(",",endloc) to tnum
							if tnum>0
								store substr(endloc,1,tnum-1) to tendloc
							else
								store alltrim(endloc) to tendloc
							endif

							if endloc='CARTERET'
								store alltrim(tstartloc)+'-'+alltrim(tendloc) to trepo
							else
								store alltrim(tstartloc)+'-'+alltrim(retloc)+' '+alltrim(tendloc) to trepo
							endif


						case action='MT' and !empty(trepo)
							store at(",",startloc) to tnum
							if tnum>0
								store substr(startloc,1,tnum-1) to tstartloc
							else
								store alltrim(startloc) to tstartloc
							endif

							store at(",",endloc) to tnum
							if tnum>0
								store substr(endloc,1,tnum-1) to tendloc
							else
								store alltrim(endloc) to tendloc
							endif

							if endloc='CARTERET'
								store alltrim(trepo)+'/'+alltrim(tstartloc)+'-'+alltrim(tendloc) to trepo
							else
								store alltrim(trepo)+'/'+alltrim(tstartloc)+'-'+alltrim(retloc)+' '+alltrim(tendloc) to trepo
							endif


						case action='SH'
							store 'JFK Shuttle on Trailer: '+alltrim(trailer) to trepo

						endcase



						trevenueactions=trevenueactions+1
						taction=taction+action+','
*IF this is a work order that is manifested out on a trip number, add one to the
*revenue actions
						select manifest
						set order to orig_wo
						seek two
						if !eof()
							trevenueactions=trevenueactions+1
						endif




						tthisdriver=tthisdriver+1

					else
						trevenueactions=trevenueactions+1
					endif
					select tdaily

					skip
				enddo

*for loose ocean pickups, add one revenue action for the warehouse
				select wolog
				seek two
				if type='O' and loose
					trevenueactions=trevenueactions+1
				endif



				store alltrim(taction) to taction
				store len(taction) to tlen
				if substr(taction,tlen,1)=','
					store substr(taction,1,tlen-1) to taction
				endif


			endif

*Ok, at this point you have a work order for a driver on a specific day with the number of actions,
*now get the revenue posted

*If Manifested on a domestic work order, it is Jones or GV which we are delivering to Gilbert and we get a
*flat trailer rate plus fuel billed on the domestic work order.
*Get the charge from INVDET





*Get the posting to "T" for this work order
			xsqlexec("select * from invdet where wo_num="+transform(two),,,"ar")

			scan
				if ((val(glcode)>=5 and val(glcode)<=10) or (val(glcode)>=41 and val(glcode)<=42) or (glcode='29' and !company='X')) and (!company='J' and !company=' ')
*SPECIAL BACKWARD COMPAT FOR BECTON EXPORT LOADS WHERE WE ONLY BILLED BECTON $77 AND THEN
*ON A CONSOLIDATED BILL FROM HORIZON LINES, BILLED ANOTHER $650 PLUS $115 FUEL, BUT INSTEAD OF USING
*A MULTIPLE WORK ORDER ROUTINE, THE ACCT DEPT BILLED MANUALLY SO NOTHING RECORDED FOR EACH WORK ORDER.

					if xaccountid=268 and at('650',description)>0
						skip
					else
						ttrev=ttrev+invdetamt
					endif

					if invdetamt=77.00 and xaccountid=268
						ttrev=ttrev+375
					endif
				endif
				skip
			endscan

*If you cannot locate the work order in the invdet table,
*1. it is possibly not billed yet
*2. it may be billed on a rated manifest


			if ttrev=0
				select wolog
				seek two

				if !empty(invnum)
					store invnum to tinvnum
*make sure you do not pick up a rated manifest here
					select manifest
					set order to wo_num
					seek two
					set order to orig_wo

					if eof()
						tinwo=0
*						select inwolog
*						seek two
*						store wo_num to tinwo

						select invdet
						set order to invnum
						seek tinvnum
						do while invnum=tinvnum and !eof()
							if (wo_num=two or xwo_num=two or wo_num=tinwo or xwo_num=tinwo) and ((val(glcode)>=5 and val(glcode)<=10) or (val(glcode)>=41 and val(glcode)<=42) or (glcode='29' and !company='X')) and (!company='J' and !company=' ')
								ttrev=ttrev+invdetamt
							endif
							skip
						enddo
						set order to xwo_num

					endif
				endif
			endif





*Check if an outbound to consolidators invoice


			if ttrev=0
				store 0 to delmfstcharges
				do getob

				if delmfstcharges>0
					store "OB" to taction
					trevenueactions=3
					store delmfstcharges to ttrev
					thisdriv=2
					trevenueactions=3
				endif
			endif



*Rated Manifest
* if the Work ORder is an orig_wo,
*only a portion of the rated manifest applies, and since there is no table to get the
*proportionate posting, calculate it (MIKE WINTER is developing) for air shipment splits

			select manifest
			set order to orig_wo
			seek two


			if ttrev=0 and !eof()
				store 0 to totcharge,tcharge,jcharge
				store "ORIG" to origortrip
				do ratedmfst with totcharge,tcharge,jcharge,origortrip
				store tcharge to ttrev
			endif

*this is a rated manifest with no air freight splits
			if ttrev=0
				select manifest
				set order to orig_wo
				seek two
				do while orig_wo=two and !eof()
					ttrev=ttrev+pucharge+fuelchg+drayage
					skip
				enddo


			endif


*If the work order is delivery of a full trip,

*the delivery gets a prorated portion of all the revenue in the trip.


			if ttrev=0
*first determine if two is a trip
				select manifest
				set order to wo_num
				seek two
				if !eof()
*If it is a trip, was it actually billed on the trip as a rated work manifest?
					select invdet
					set order to xwo_num
					seek two
					if eof()
						set order to wo_num
						seek two
						set order to xwo_num
					endif

					if !eof()
						store two to originalworkorder
						copy to h:\fox\temptrip while (wo_num=two or xwo_num=two)
						use h:\fox\temptrip in 0 exclusive
						select temptrip
						go top
						if wo_num=0
							replace wo_num with xwo_num while !eof()
						else
							replace xwo_num with wo_num while !eof()
						endif
						index on wo_num tag orig unique
						go top
						tthisdriv=1
						trevenueactions=2

						do while !eof()
							store wo_num to two


							store 0 to totcharge,tcharge,jcharge
							store "TRIP" to origortrip
							do ratedmfst with totcharge,tcharge,jcharge,origortrip
							ttrev=ttrev+tcharge
							select temptrip
							skip
						enddo
						store originalworkorder to two

						use in temptrip
					endif
				endif
			endif


*this is a rated manifest with no air freight splits
			if ttrev=0
				select manifest
				set order to orig_wo
				seek two
				do while orig_wo=two and !eof()
					ttrev=ttrev+pucharge+fuelchg+drayage
					skip
				enddo


			endif


*if this is a manifested delivery for any account that is billed on the original work
*orders (like Nine West or Jones), you have to get the charges for each portion of the manifest
*and add them up
*assume three revenue actions, one to pickup, one to the warehouse, one to deliver

* removed dy 7/24/12 doesn't work

*!*				if ttrev=0
*!*					tthisdriv=1
*!*					trevenueactions=3
*!*					select manifest
*!*					set order to wo_num
*!*					seek two
*!*					copy to h:\fox\trip2 while wo_num=two
*!*					use h:\fox\trip2 in 0 index 
*!*					select trip2
*!*					index on orig_wo tag orig_wo unique
*!*					go top

*!*					do while !eof()
*!*						store orig_wo to thiswo
*!*						select invdet
*!*						set order to xwo_num
*!*						seek thiswo
*!*						if eof()
*!*							set order to wo_num
*!*							seek two
*!*						endif

*!*						if !eof()
*!*							thisrev=0
*!*							do while (xwo_num=thiswo or wo_num=thiswo) and !eof()
*!*								if ((val(glcode)>=5 and val(glcode)<=10) or (val(glcode)>=41 and val(glcode)<=42) or (glcode='29' and !company='X')) and (!company='J' and !company=' ')
*!*									thisrev=thisrev+invdetamt
*!*								endif
*!*								skip
*!*							enddo
*!*							set order to xwo_num
*!*	*now you have the entire post amount for the orig_wo, but it is possible not all of it applies to
*!*	*this trip, so prorate it
*!*							select manifest
*!*							set order to orig_wo
*!*							seek thiswo
*!*							sum quantity to t2 while orig_wo=thiswo
*!*							seek thiswo
*!*							sum quantity to t3 for wo_num=two while orig_wo=thiswo
*!*							store t3/t2 to tpercent
*!*							ttrev=ttrev+(thisrev*tpercent)
*!*						endif
*!*						select trip2
*!*						skip
*!*					enddo
*!*					use in trip2
*!*				endif

			endif  &&repo or not


		select localdr
		if trevenueactions>0
			tpcnt=tthisdriver/trevenueactions
		else
			tpcnt=0
		endif

		append blank
		replace wo_num with two
		replace driver with tdriver
		replace dispatched with tdate
		replace thisdriv with tthisdriver
		replace action with taction
		replace acctname with tname
		replace revenue with ttrev
		replace revapply with ttrev*tpcnt
		replace nummoves with trevenueactions
		replace pcnt with tpcnt
		replace repo with trepo


		select ttdaily
		goto trec
		skip
	enddo

* now check the same date in Express

	xsqlexec("select * from fxtrips where dispatched={"+dtoc(tdate)+"}","xfxtrips",,"fx")

	scan
*************************************************************************
		if dis_driver=tdriver
			store recno() to trec
			store wo_num to two

			xsqlexec("select * from fxwolog where wo_num="+transform(two),"xfxwolog",,"fx")
			store acctname to tname
			

*determine how many revenue actions were performed for this work order, and if they were performed by the same driver

			xsqlexec("select * from fxtrips where wo_num="+transform(two)+" and inlist(dvrcompany,' ','T')","xfxtrips2",,"fx")

			store 0 to trevenueactions,tthisdriver,ttrev

			store "EXPRESS MOVE " to taction
			scan
				if dis_driver=tdriver
					trevenueactions=trevenueactions+1
					tthisdriver=tthisdriver+1
				else
					trevenueactions=trevenueactions+1
				endif
			endscan
			
			if trevenueactions=0
				store 1 to trevenueactions
			endif
			if tthisdriver=1
				store 1 to tthisdriver
			endif

			if !tname='MACY'
				select manifest
				seek two
				do while wo_num=two and !eof()
					ttrev=ttrev+pucharge+fuelchg+drayage
					skip
				enddo
			endif

			if ttrev=0 and !tname='MACY'
				select manifest
				set order to orig_wo
				seek two
				do while orig_wo=two and !eof()
					ttrev=ttrev+pucharge+fuelchg+drayage
					skip
				enddo
				set order to wo_num
			endif

			if ttrev=0
				select invdet
				seek two
				do while xwo_num=two and !eof()
					store 0 to thisinv,xinv
					if !company=' '
						thisinv=thisinv+invdetamt
					endif
					if company='X'
						xinv=xinv+invdetamt
					endif

					if ((val(glcode)>=5 and val(glcode)<=10) or (val(glcode)>=41 and val(glcode)<=42) or (company='X')) and (!company='J' and !company=' ')
						ttrev=ttrev+invdetamt
					endif
					skip
				enddo

				if ttrev=0
					locate for wo_num=two
					if !eof()
						do while wo_num=two and !eof()
							if ((val(glcode)>=5 and val(glcode)<=10) or (val(glcode)>=41 and val(glcode)<=42) or (company='X')) and (!company='J' and !company=' ')
								ttrev=ttrev+invdetamt
							endif
							skip

						enddo

					endif
				endif

				if ttrev=0
					select wolog
					seek two
					if empty(invnum)
						ttrev=worevenue
					endif
				endif

			endif




			select localdr
			tpcnt=tthisdriver/trevenueactions
			append blank
			replace wo_num with two
			replace driver with tdriver
			replace dispatched with tdate
			replace thisdriv with tthisdriver
			replace nummoves with trevenueactions
			replace action with taction
			replace acctname with tname
			replace revenue with ttrev
			replace revapply with ttrev*tpcnt
			replace nummoves with trevenueactions
			replace thisdriv with tthisdriver
			replace pcnt with tpcnt

		endif
	endscan

	select ttdaily

enddo  && for all drivers




select localdr
go top
do while !eof()
	store driver to tdriver
	store recno() to trec
	store 0 to tdays,revthisdriver

	do while driver=tdriver and !eof()
		store dispatched to tdate
		tdays=tdays+1
		do while driver=tdriver and dispatched=tdate and !eof()
			revthisdriver=revthisdriver+revapply
			skip
		enddo
	enddo

	store recno() to lastrec
	if eof()
		store .t. to lastrecord
	else
		store .f. to lastrecord
	endif

	goto trec
	replace avgrevpday with revthisdriver/tdays


	if lastrecord
		exit
	else
		goto lastrec
	endif
enddo

select localdr
index on driver+str(year(dispatched),4,0)+str(month(dispatched),2,0)+str(day(dispatched),2,0) tag dispatched

store 'Report Dates: From '+dtoc(tstartdate)+' To: '+dtoc(tenddate) to tdateexpression
select localdr
report form tlocaldriver preview

use in ttdaily
use in tdaily
use in manifest
use in wolog
use in invdet
use in localdr
use in airfrt
use in dellocs
use in rates
*use in inwolog





procedure ratedmfst
parameters totcharge,tcharge,jcharge,origortrip

select manifest
set order to orig_wo
seek two

store delloc to tdelloc
if origortrip="TRIP"
	sum quantity,weight for wo_num=originalworkorder to tqty,twt while orig_wo=two

else
	sum quantity,weight to tqty,twt while orig_wo=two
endif
seek two


select dellocs
locate for accountid=manifest.accountid and location=tdelloc
store city to tcity

select rates

if manifest.qty_type='GOH'
	set filter to ratetype='PUCHARGE' and chgtype='PICKUP' and accountid=manifest.accountid and office='N' and type=manifest.type and (emptynul(canceldt) or canceldt>date()) and delcity=tcity and per='GOH'
	locate
else
	set filter to ratetype='PUCHARGE' and chgtype='PICKUP' and accountid=manifest.accountid and office='N' and type=manifest.type and (emptynul(canceldt) or canceldt>date()) and delcity=tcity and (per='CARTON' or per='CWT' or per='KG')
	locate
	if eof()
		set filter to ratetype='PUCHARGE' and chgtype='PICKUP' and accountid=manifest.accountid and office='N' and type=manifest.type and (emptynul(canceldt) or canceldt>date()) and (per='CARTON' or per='CWT' or per='KG')
	endif
	if eof()
		set filter to ratetype='PUCHARGE' and chgtype='PICKUP' and accountid=manifest.accountid and office='N' and type=manifest.type and (emptynul(canceldt) or canceldt>date()) and (per='CONTAINER')
	endif


endif
locate
store 0 to charget,chargej
do case
case per='CONTAINER'
	tcharge=rate
	if postco1='T'
		charget=postamt1
	endif
	if postco2='J'
		chargej=postamtj
	endif

case per='CARTON' or per='GOH'
	store tqty*rate to tcharge
	if tcharge<minrate and minrate>0
		store minrate to tcharge

	endif
	if tcharge>maxrate and maxrate>0
		store maxrate to tcharge
	endif

	if postco1='J' and  tcharge>minrate
		chargej=chargej+ postamt1*tqty
	endif
	if postco1='J' and tcharge=minrate
		chargej=chargej+ minpostamt1
	endif

	if postco1='T' and  tcharge>minrate
		charget=charget+ postamt1*tqty
	endif
	if postco1='T' and tcharge=minrate
		charget=charget+ minpostamt1
	endif

	if postco2='J' and  tcharge>minrate
		chargej=chargej+ postamt2*tqty
	endif
	if postco2='J' and tcharge=minrate
		chargej=chargej+ minpostamt2
	endif

	if postco2='T' and  tcharge>minrate
		charget=charget+ postamt2*tqty
	endif
	if postco2='T' and tcharge=minrate
		charget=charget+ minpostamt2
	endif



case per='CWT'
	store (twt/100)*rate to tcharge
	if tcharge<minrate and minrate>0
		store minrate to tcharge
	endif
	if tcharge>maxrate and maxrate>0
		store maxrate to tcharge
	endif



	if postco1='J' and  tcharge>minrate
		chargej=chargej+ postamt1*(twt/100)
	endif
	if postco1='J' and tcharge=minrate
		chargej=chargej+ minpostamt1
	endif

	if postco1='T' and  tcharge>minrate
		charget=charget+ postamt1*(twt/100)
	endif
	if postco1='T' and tcharge=minrate
		charget=charget+ minpostamt1
	endif

	if postco2='J' and  tcharge>minrate
		chargej=chargej+ postamt2*(twt/100)
	endif
	if postco2='J' and tcharge=minrate
		chargej=chargej+ minpostamt2
	endif

	if postco2='T' and  tcharge>minrate
		charget=charget+ postamt2*(twt/100)
	endif
	if postco2='T' and tcharge=minrate
		charget=charget+ minpostamt2
	endif






case per='KG'
	store (twt/2.2046)*rate to tcharge
	if tcharge<minrate and minrate>0
		store minrate to tcharge
	endif
	if tcharge>maxrate and maxrate>0
		store maxrate to tcharge
	endif

	if postco1='J' and  tcharge>minrate
		chargej=chargej+ postamt1*(twt/2.2046)
	endif
	if postco1='J' and tcharge=minrate
		chargej=chargej+ minpostamt1
	endif

	if postco1='T' and  tcharge>minrate
		charget=charget+ postamt1*(twt/2.2046)
	endif
	if postco1='T' and tcharge=minrate
		charget=charget+ minpostamt1
	endif

	if postco2='J' and  tcharge>minrate
		chargej=chargej+ postamt2*(twt/2.2046)
	endif
	if postco2='J' and tcharge=minrate
		chargej=chargej+ minpostamt2
	endif

	if postco2='T' and  tcharge>minrate
		charget=charget+ postamt2*(twt/2.2046)
	endif
	if postco2='T' and tcharge=minrate
		charget=charget+ minpostamt2
	endif

endcase


*YOU STILL NEED TO CHECK FOR THE LD3 CHARGES IN AIRFRT
store 0 to totctrcharge,tctrcharge,jctrcharge
select airfrt
set order to wo_num
seek two
store ld3 to xxld3
store ld9 to xxld9
store other to xxother
store xxld9+xxother to xxother

select rates
set filter to ratetype="PUCHARGE" and chgtype="PICKUP" and accountid=manifest.accountid and office="N" and type=manifest.type and (emptynul(canceldt) or canceldt>date()) and inlist(per,"M1","LD3","LD7","LD9","10'")
locate


if xxld3>0
	locate for per='LD3'
	if !eof()
		totctrcharge=totctrcharge+(rate*xxld3)
		if postco1='T'
			tctrcharge=tctrcharge+(postamt1*xxld3)
		endif
		if postco1='J'
			jctrcharge=jctrcharge+(postamt1*xxld3)
		endif
		if postco2='T'
			tctrcharge=tctrcharge+(postamt1*xxld3)
		endif
		if postco2='J'
			jctrcharge=jctrcharge+(postamt1*xxld3)
		endif

	endif
endif



if xxother>0
	locate for per="M1" or per="LD7" or per="LD9" or per="10' or PER='GOHBOX"
	if !eof()
		totctrcharge=totctrcharge+(rate*xxother)
		if postco1='T'
			tctrcharge=tctrcharge+(postamt1*xxother)
		endif
		if postco1='J'
			jctrcharge=jctrcharge+(postamt1*xxother)
		endif
		if postco2='T'
			tctrcharge=tctrcharge+(postamt1*xxother)
		endif
		if postco2='J'
			jctrcharge=jctrcharge+(postamt1*xxother)
		endif

	endif
endif







totcharge=totcharge+totctrcharge
tcharge=charget+tctrcharge
jcharge=chargej+jctrcharge

endproc



procedure getob


store 0 to delmfstcharges
store two to tmfst

if usesqlbl()
	xsqlexec("select * from delivery where del_mfst='"+transform(two),,,"wo")
	xsqlexec("select * from bl",,,"wo")
	index on blid tag blid
	set order to
else
	use f:\wo\wodata\bl order blid  in 0
	use f:\wo\wodata\delivery in 0
	select delivery
	set filter to del_mfst=alltrim(str(two))
	go top
endif

do while !eof()
	store blid to tid
	select bl
	seek tid
	delmfstcharges=delmfstcharges+charge
	select delivery
	skip
enddo

use in bl
use in delivery
*!*	SELECT delivery
*!*	COPY TO h:\fox\tmpdel FOR del_mfst=ALLTRIM(STR(tmfst))
*!*	USE
*!*	USE h:\fox\tmpdel IN 0
*!*	SELECT tmpdel
*!*	GO top
*!*	DO WHILE !EOF()
*!*	STORE blid TO tblid
*!*	SELECT bl
*!*	SEEK tblid



*!*	*****
*!*	*get the rate
*!*		m.acctname=acctname
*!*		m.cnee=cnee
*!*		m.deliverto=deliverto
*!*		xrecno=recno()
*!*		xfilter="recno()=xrecno"
*!*
*!*		select rate

*!*		if !","$m.deliverto or m.deliverto="WILKES"
*!*			xfound = getrate(bl.accountid,,"OUTBOUND",goffice,bl.ofd_date,"per='GARMENT' and stop='CONSOL'")
*!*		else
*!*			xfound = getrate(bl.accountid,,"OUTBOUND",goffice,bl.ofd_date,"per='GARMENT' and stop=m.deliverto")
*!*			if !xfound
*!*				xfound = getrate(bl.accountid,,"OUTBOUND",goffice,bl.ofd_date,"per='GARMENT' and stop=trim(m.deliverto)")
*!*				if !xfound
*!*					xfound = getrate(bl.accountid,,"OUTBOUND",goffice,bl.ofd_date,"per='GARMENT' and stop=right(trim(m.deliverto),2)")
*!*				endif
*!*			endif
*!*		endif
*!*		if !found()
*!*			getrate(bl.accountid,,"OUTBOUND",goffice,bl.ofd_date,"per='GARMENT'")
*!*		endif

*!*		tgohrate=rate
*!*		tgohxrate=xrate
*!*		tgohfrate=frate
*!*		tgohtrate=trate
*!*		tgohmin=min

*!*		if (!","$m.deliverto or m.deliverto="WILKES") and (bl.accountid#333 or m.deliverto#"KIDS")
*!*			xfound = getrate(bl.accountid,,"OUTBOUND",goffice,bl.ofd_date,"per='CARTON' and stop='CONSOL'")
*!*		else
*!*			xfound = getrate(bl.accountid,,"OUTBOUND",goffice,bl.ofd_date,"per='CARTON' and stop=m.deliverto")
*!*			if !xfound
*!*				if bl.qty_type="PALLETS"
*!*					xfound = getrate(bl.accountid,,"OUTBOUND",goffice,bl.ofd_date,"per='CWT' and stop=trim(m.deliverto)")
*!*					if !xfound
*!*						xfound = getrate(bl.accountid,,"OUTBOUND",goffice,bl.ofd_date,"per='CARTON' and stop=right(trim(m.deliverto),2)")
*!*					endif
*!*				ELSE

*!*					xfound = getrate(bl.accountid,,"OUTBOUND",goffice,bl.ofd_date,"per='CARTON' and stop=trim(m.deliverto)")
*!*					if !xfound
*!*						xfound = getrate(bl.accountid,,"OUTBOUND",goffice,bl.ofd_date,"per='CARTON' and stop=right(trim(m.deliverto),2)")
*!*					endif
*!*				endif
*!*			endif
*!*		endif

*!*		if !xfound
*!*			xfound = getrate(bl.accountid,,"OUTBOUND",goffice,bl.ofd_date,"per='CWT' and empty(stop) and between(bl.del_wt,fromwt,towt)")
*!*			if !xfound
*!*				xfound = getrate(bl.accountid,,"OUTBOUND",goffice,bl.ofd_date,"per='CWT' and stop=trim(m.deliverto) and between(bl.del_wt,fromwt,towt)")
*!*				if !xfound
*!*					xfound = getrate(bl.accountid,,"OUTBOUND",goffice,bl.ofd_date,"per='CWT' and stop=trim(m.deliverto)")
*!*				endif
*!*			endif
*!*		endif

*!*		if !xfound
*!*			xfound = getrate(bl.accountid,,"OUTBOUND",goffice,bl.ofd_date,"per='CONTAINER' and empty(stop)")
*!*			if !xfound
*!*				xfound = getrate(bl.accountid,,"OUTBOUND",goffice,bl.ofd_date,"per='CONTAINER' and stop=trim(m.deliverto)")
*!*			endif
*!*		endif

*!*		if !xfound
*!*
*!*			xfound = getrate(bl.accountid,,"OUTBOUND",goffice,bl.ofd_date,"per='CARTON' and empty(stop)")
*!*		endif

*!*		if !found()
*!*			x3winmsg("No Outbound rate found for "+trim(bl.acctname)+" delivery to "+m.deliverto)
*!*			email("Dyoung@fmiint.com","INFO: no OB to Cons rate found",bl.acctname+crlf+m.deliverto,,,,.t.,,,,,.t.,,.t.)
*!*		endif

*!*		tctnrate=rate
*!*		tctnxrate=xrate
*!*		tctnfrate=frate
*!*		tctntrate=trate
*!*		tctnmin=min
*!*		tctnper=per
*!*		tctnmax=max
*!*		tcwtrate=cwtrate
*!*		t100rate=specrate


*!*			select bl
*!*			xrecno=recno()
*!*			STORE 0 TO xgohqty,xctnqty
*!*			IF qty_type="G" and &xfilter
*!*			store rcv_qty to xgohqty
*!*			else
*!*			if tctnper="CWT" AND qty_type#"G" AND &xfilter
*!*				STORE rcv_wt/100 to xctnqty
*!*			else
*!*				STORE rcv_qty to xctnqty
*!*			ENDIF
*!*			ENDIF
*!*
*!*				xctnrate=tctnrate

*!*
*!*			xamt=xgohqty*tgohrate+xctnqty*xctnrate

*!*			if tcwtrate#0 AND qty_type#"G" AND &xfilter
*!*				STORE rcv_wt/100 to xcwtqty
*!*				xcwtamt=tcwtrate*xcwtqty
*!*			else
*!*				xcwtamt=0
*!*			endif



*!*			do case

*!*			case xcwtamt>xamt

*!*					m.bl_num=bl_num
*!*					m.qty=rcv_qty
*!*					m.qty_type=qty_type
*!*					m.weight=rcv_wt
*!*					m.rate=tcwtrate
*!*					m.charge=(m.weight/100)*m.rate
*!*					if m.charge<tctnmin
*!*						m.rate=xctnrate
*!*						m.charge=tctnmin
*!*						m.minmax='MINIMUM'
*!*					else
*!*						m.minmax=''
*!*					endif
*!*					*insert into xrpt from memvar
*!*					delmfstcharges=delmfstcharges+m.charge


*!*
*!*			case xamt>tctnmax and tctnmax#0
*!*					m.bl_num=bl_num
*!*					m.qty=rcv_qty
*!*					m.qty_type=qty_type
*!*					m.weight=rcv_wt
*!*					m.rate=0
*!*
*!*						m.minmax="MAXIMUM"
*!*						m.charge=tctnmax

*!*
*!*	*				insert into xrpt from memvar
*!*					delmfstcharges=delmfstcharges+m.charge

*!*
*!*			case xamt<tctnmin and tctnmin#0
*!*					m.bl_num=bl_num
*!*					m.qty=rcv_qty
*!*					m.qty_type=qty_type
*!*					m.weight=rcv_wt
*!*					m.rate=0

*!*						m.minmax="MINIMUM"
*!*						m.charge=tctnmin
*!*	*				insert into xrpt from memvar
*!*					delmfstcharges=delmfstcharges+m.charge
*!*
*!*			otherwise

*!*					m.bl_num=bl_num
*!*					m.qty=rcv_qty
*!*					m.qty_type=qty_type
*!*					m.weight=rcv_wt
*!*					if qty_type="GOH"
*!*						m.rate=tgohrate
*!*					else
*!*						m.rate=xctnrate
*!*					endif
*!*					m.minmax=''
*!*					if tctnper="CWT"
*!*						m.charge=(m.weight/100)*m.rate
*!*					else
*!*						m.charge=m.qty*m.rate
*!*					endif
*!*	*				insert into xrpt from memvar
*!*					delmfstcharges=delmfstcharges+m.charge
*!*			endcase





*!*	*****

*!*	SELECT tmpdel
*!*	SKIP
*!*	ENDDO



*!*	USE IN rate
*!*	USE IN bl
*!*	USE IN tmpdel
*!*USE IN charges

endproc


***delmfstcharges are your charges
****procedure is getrate

procedure getrate
lparameters xaccountid, xbilltoid, xtype, xoffice, xdate, xotherfilter, xnewratemessage

if empty(xbilltoid)
	xbilltofilter=".t."
else
	xbilltofilter="billtoid=xbilltoid"
endif

if empty(xotherfilter)
	xotherfilter=".t."
endif

if inlist(xoffice,"1","2","5","6","7")
	xoffice="C"
endif

if used("fmirates")
	select fmirates
	xratetable="fmirates"
else
	if !used("rate")
		use f:\watusi\ardata\rate in 0
	endif
	select rate
	xratetable="rate"
endif



locate for substr(acctname,1,17)=substr(m.acctname,1,17) and type=xtype ;
and rateheadid=rate.rateheadid ;
and &xbilltofilter ;
and office=xoffice ;
and &xotherfilter ;
and (emptynul(cancels) or xdate<=cancels) ;
and xdate>=effective


if !found()
	locate for substr(acctname,1,17)=substr(m.acctname,1,17) and type=xtype ;
	and rateheadid=rate.rateheadid ;
	and &xbilltofilter ;
	and office="A" ;
	and &xotherfilter ;
	and (emptynul(cancels) or xdate<=cancels) ;
	and xdate>=effective

endif

if !found() and !empty(xbilltoid)
	locate for substr(acctname,1,17)=substr(m.acctname,1,17) and type=xtype ;
	and rateheadid=rate.rateheadid ;
	and empty(billtoid) ;
	and office=xoffice ;
	and &xotherfilter ;
	and (emptynul(cancels) or xdate<=cancels) ;
	and xdate>=effective


	if !found()
		if !found() and !empty(xbilltoid)
			locate for type=xtype ;
			and rateheadid=rate.rateheadid ;
			and empty(billtoid) ;
			and office="A" ;
			and &xotherfilter ;
			and (emptynul(cancels) or xdate<=cancels) ;
			and xdate>=effective

		endif
	endif
endif

if found() and rate.rateid#0


	return .t.
else
	return .f.
endif

endproc
