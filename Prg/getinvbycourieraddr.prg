* GetInvByCourierAddr
*
* author: Mark Bennett
*
* create date: 04/27/2018
*
* based on date, returns the text that should go under 'By Courier' in the invoices.
*
* for RF 408861

LPARAMETERS tdDate

#DEFINE CRLF CHR(13) + CHR(10)

LOCAL lcText
lcText = ''

DO CASE
	CASE tdDate >= {^2018-06-01}
		lcText = ;
			'First Data Remittance Services' + CRLF + ;
			'TGF Management Group Holdco Inc. / Box 11113' + CRLF + ;
			'400 White Clay Center Drive' + CRLF + ;
			'Newark, DE 19711'	
	OTHERWISE
		lcText = ;
			'TGF Management Group Holdco Inc.' + CRLF + ;
			'Lockbox #11113' + CRLF + ;
			'11150 South Avenue' + CRLF + ;
			'Staten Island, NY 10314'	
ENDCASE

RETURN lcText





