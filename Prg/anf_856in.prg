PUBLIC xfile,asn_origin,TranslateOption,lcScreenCaption,tsendto,tcc,NormalExit
*!* Derived from the tjx_856in project, Joe, 10.30.2015

utilsetup("ANF_856IN")

xtest=.f.

xfile = ""

NormalExit = .F.
lcScreenCaption = "A&F ASN Uploader Process... Processing File: "
_SCREEN.CAPTION = lcScreenCaption

WAIT WINDOW AT 10,10 " Now uploading A&F 856's........." TIMEOUT 2
TranslateOption = "CR_TILDE" &&"NONE"
asn_origin = "ANF"

use f:\anf\data\anfdetail in 0
use f:\anf\data\podata in 0
use f:\sysdata\qqdata\unloccode in 0

if xtest
	use m:\dev\wodata\wolog in 0
	use m:\dev\wodata\detail in 0
	use m:\dev\wodata\dellocs in 0
	use m:\dev\wodata\sizes in 0
	use m:\dev\wodata\master in 0
	use m:\dev\wodata\mastdet in 0
else
	use f:\wo\wodata\wolog in 0
	use f:\wo\wodata\detail in 0

	xsqlexec("select * from dellocs",,,"wo")
	index on str(accountid,4)+location tag acct_loc
	set order to
	
	useca("master","wo")
	useca("mastdet","wo")
	
	xsqlexec("select * from sizes",,,"wo")
endif

select 0
use f:\3pl\data\mailmaster alias mm
locate for mm.edi_type  = '856' and mm.accountid = 6576
tsendto = iif(mm.use_alt,mm.sendtoalt,mm.sendto)
tcc = iif(mm.use_alt,mm.ccalt,mm.cc)
lcpath = alltrim(mm.basepath)
lcarchivepath = alltrim(mm.archpath)
lc997path = alltrim(mm.holdpath)
use in mm

WAIT WINDOW AT 10,10  "Processing ANF 856s............." TIMEOUT 1

delimiter = "*"

m.dateloaded = datetime()

lnNum = ADIR(tarray,"f:\ftpusers\anf\asnin\*.*")

IF lnNum >= 1
	xdateloaded=datetime()

	FOR thisfile = 1  TO lnNum
		SELECT *, space(2) as filetype FROM anfdetail WHERE .F. INTO CURSOR asndata READWRITE
		SCATTER MEMVAR MEMO BLANK
		DO createx856
		xfile = lcPath+tarray[thisfile,1]  &&+"."
		archivefile = lcArchivePath+tarray[thisfile,1]  &&+"."
		to997file = lc997Path+tarray[thisfile,1]
		WAIT WINDOW "Importing file: "+xfile NOWAIT
		DO loadedifile WITH xfile,"*",TranslateOption,"JONES856"

		**only process 856 files - mvw 01/11/16
		select x856
		locate for segment='ST'
		if f1#"856"
			loop
		endif

		DO ImportANFASN

		COPY FILE "&xfile" TO "&to997file"
		COPY FILE "&xfile" TO "&archivefile"
		IF FILE("&archivefile")
			DELETE FILE "&xfile"
		ENDIF
		USE IN x856
	NEXT thisfile

	**grab all data that was just loaded
	select * from anfdetail where dateloaded=xdateloaded into cursor xdetail

	**create wos for containers that do not yet created
	select *, sum(pl_qty) as woqty, sum(weight) as wowt, sum(weight) as wocbm from xdetail group by container into cursor xctr
	scan
		scatter memvar

		guserid=iif(type('guserid')="C" and guserid="WINTER",guserid,"856IN")

		xwocreated=.f.
		select wolog
		locate for accountid=6576 and container=left(xctr.container,10) and wo_date>=date()-45
		if !found()
			xwocreated=.t.

			m.wo_date=date()
			m.billtoid=m.accountid
			m.wotype=iif(m.type='O',"OCEAN FULL","AIR")
			m.ssco=left(m.brokref,4)
			m.mblnum=m.brokref
			m.quantity=xctr.woqty
			m.weight=xctr.wowt
			m.cbm=xctr.wocbm
			m.seal=getmemodata("xctr.suppdata","SEAL",,.t.)
			m.importseal=m.seal
			m.priority=5
			m.yardloc='SP'
			m.delloc=iif(m.office="N","CARTERET, NJ","SAN PEDRO, CA")
			m.tocs=iif(m.office="N","CARTERET, NJ","SAN PEDRO, CA")
			m.toloc=iif(m.office="N",3,1)

			m.notes=""
			select brokref from xdetail where container=xctr.container and brokref#xctr.brokref group by brokref into cursor xtemp
			if !eof()
				m.notes="ADDITIONAL MBOL:"+chr(13)
				scan
					m.notes=m.notes+alltrim(brokref)+chr(13)
				endscan
			endif
			use in xtemp

			xfilestr=filetostr(strtran(xctr.filename,"\ASNIN\","\ASNIN\ARCHIVE\"))
			m.pdf2="EDI317*"+strtran(left(xfilestr,at("HL*2*1*E",xfilestr)-2),chr(10),"~")

			m.wo_num=genpk("wonum","wo",,,.T.,,.T.)
			m.wologid=genpk("wolog","wo",,,.T.,,.T.)
			m.deletedt={}
			insert into wolog from memvar

			**create a cfs wo
			m.ship_num=m.accountid
			m.shipper=m.acctname
			m.importseal=m.seal
			m.ctr_size=m.size

			insertinto("master","wo",.t.)
			tu("master")
			
			xsubject="Abercrombie WO Created"
			xbody="Abercrombie WO "+transform(wolog.wo_num)+" was created for container "+alltrim(wolog.container)+chr(13)+;
				"Office: "+alltrim(wolog.office)+chr(13)+;
				"ETA: "+transform(wolog.eta)
			if wolog.office="C"
				xto="mwinter@fmiint.com,juan@fmiint.com,TGFAmericas.USCAANFCSR@tollgroup.com"
			else
				xto="mwinter@fmiint.com,juan@fmiint.com,TGFAmericas.USNJANFCSR@tollgroup.com"
			endif

			xcc=""
			do form dartmail2 WITH xto,"noreply@tollgroup.com",xsubject,xcc,,xbody,"A"
		endif

		**do not load any of the new detail if the container is already picked up
		if !empty(wolog.pickedup)
			xfile="h:\pdf\detail.xls"
			if file(xfile)
				delete file &xfile
			endif
			select xdetail
			copy to &xfile for container=xctr.container type xls
			xsubject="Import Error: Abercrombie Breakdown Upload After Pick Up"
			xbody="An ASN was received for updates to the breakdown of WO "+transform(wolog.wo_num)+" after container pick up! Cannot automatically update breakdown."+chr(13)+chr(13)+"File attached."
			if wolog.office="C"
				xto="mwinter@fmiint.com,juan@fmiint.com,TGFAmericas.USCAANFCSR@tollgroup.com"
			else
				xto="mwinter@fmiint.com,juan@fmiint.com,TGFAmericas.USNJANFCSR@tollgroup.com"
			endif

			xcc=""
			do form dartmail2 WITH xto,"noreply@tollgroup.com",xsubject,xcc,xfile,xbody,"A"
		else
			select * from xdetail where container=xctr.container group by brokref into cursor xtemp
			scan
				select detail
				locate for wo_num=wolog.wo_num and brokref=xtemp.brokref

				**do not upload bkdn if that brokref (mbol) already exists, updates must be done manually
				xfound=.f.
				if found()
					xfound=.t.
					if type('guserid')='C' and guserid='WINTER' and messagebox("Master BOL already exists. Delete and replace?",4+16+256,"Dup")=6
						delete for wo_num=wolog.wo_num and brokref=xtemp.brokref in detail
						delete for wologid=wolog.wologid and mbl=xtemp.brokref in mastdet
						xfound=.f.
					endif
				endif

				if xfound
					xfile="h:\pdf\detail.xls"
					if file(xfile)
						delete file &xfile
					endif

					replace duplicate with .t. for container=xctr.container and brokref=xtemp.brokref and dateloaded=xdateloaded in anfdetail

					select xdetail
					copy to &xfile for container=xctr.container and brokref=xtemp.brokref type xls
					xsubject="Import Error: Abercrombie Breakdown Update Must Be Done Manually"
					xbody="An ASN was received for updates to the breakdown of WO "+transform(wolog.wo_num)+", MBOL "+alltrim(xtemp.brokref)+". These updates must be done manually."+chr(13)+chr(13)+"File attached."

					if detail.office="C"
						xto="mwinter@fmiint.com,juan@fmiint.com,TGFAmericas.USCAANFCSR@tollgroup.com"
					else
						xto="mwinter@fmiint.com,juan@fmiint.com,TGFAmericas.USNJANFCSR@tollgroup.com"
					endif

					xcc=""
					do form dartmail2 WITH xto,"noreply@tollgroup.com",xsubject,xcc,xfile,xbody,"A"
				else
					select * from xdetail where container=xctr.container and brokref=xtemp.brokref into cursor xdetail2 readwrite
					alter table xdetail2 drop column detailid
					=crtbkdn("xdetail2","detail",,.t.)
					use in xdetail2

					**add detail to mastdet
					select *, sum(pl_qty) as totqty, sum(cbm) as total_cbm, sum(totalkgs) as total_kgs, sum(weight) as total_lbs from detail ;
						where wo_num=wolog.wo_num and brokref=xtemp.brokref group by hawb into cursor xdetail2
					scan
						scatter memvar
						m.acct_num=m.accountid
						m.acct_name=m.acctname
						m.mbl=m.brokref
						m.hbl=right(alltrim(m.hawb),15) &&take last 15 bc hbl is only c(15)
						m.quantity=m.totqty

						xsqlexec("select masterid from master where wo_num="+transform(wolog.wo_num),"xmaster",,"wo")
						m.masterid=xmaster.masterid
						insertinto("mastdet","wo",.t.)
						tu("mastdet")
					endscan
					use in xdetail2

					replace fmiloaded with datetime(), wo_num with wolog.wo_num for container=xctr.container and brokref=xtemp.brokref in anfdetail

					**if this is a new file for a ctr and wo was previously created, need to add additional brokrefs to wolog.notes
					if !xwocreated and !(alltrim(xtemp.brokref)$wolog.notes)
						xnotes=iif("ADDITIONAL MBOL:"$wolog.notes,"","ADDITIONAL MBOL:"+chr(13))+alltrim(xtemp.brokref)
						replace notes with notes+iif(empty(wolog.notes) or asc(right(alltrim(wolog.notes),1))=13,"",chr(13))+xnotes in wolog
					endif
				endif
			endscan
			use in xtemp
		endif
	endscan
	use in xctr
	use in xdetail
ENDIF

WAIT WINDOW "ANF 856IN Process Complete" TIMEOUT 2
NormalExit = .T.

schedupdate()

close data all