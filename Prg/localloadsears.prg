

*******************************************************************************************************
* Process SEARS pm data to produce a bill.
*
*******************************************************************************************************
LPARAMETERS tcMode

LOCAL loSEARSProc

SET PROCEDURE TO M:\DEV\PRG\SEARSCLASS

loSEARSProc = CREATEOBJECT('SEARS')

loSEARSProc.UseLocalData()

loSEARSProc.Load()

RETURN
