*!* SYNCLAIRE 945 EDI MBOL (Whse. Shipping Advice) Creation Program
*!* Creation Date: 09.15.2011 by Joe

PARAMETERS cBOL,cOffice,ctime1

RELEASE m.groupnum,m.transnum,m.isanum,m.edicode,m.ship_ref,m.processed,m.accountid,m.loaddt,m.loadtime,m.filename,m.fafilename
PUBLIC ARRAY thisarray(1)
PUBLIC c_CntrlNum,c_GrpCntrlNum,cReturned,cWO_Num,cWO_NumStr,nWO_Num1,cPRONum,cTRNum,cFilenameShort,nBOLQty,nBOLWt,cCustFolder,cRolledRecs
PUBLIC lEmail,lDoSynFilesOut,dDateTimeCal,nFilenum,tsendto,tcc,tsendtoerr,tccerr,nPTCount,cTrkNumber,cFreight,nCuFT,cFilenameStage,cPTString
PUBLIC cxCoCode,lSQL3,lDoCatch,cTruncDate,cTruncTime,cDate,dt1,dt2,dtmail,cString,cMBOL,cISA_Num,cFilenameOut,cShip_refSH,cStatus,cSubBOLDetail
PUBLIC cShip_ref_out,lTesting,lDoUpdate,nCutUCCs,cLoadid,cHoldFolder,cOutFolder,lOverflow,lToys,tsendtozero,tcczero,cRetMsg,cArchFolder,lScanpack
PUBLIC lJCPenney,lMacy,lWalmart,dRundate,nOrigSeq,nOrigGrpSeq,lAAFES,nAcctNum,cMod,cFilenameHold,lParcelType,lcPath,cWO_NumList,cEDIType,lAckdata,lUseSQLBL

PUBLIC m.groupnum,m.transnum,m.isanum,m.edicode,m.ship_ref,m.processed,m.accountid,m.loaddt,m.loadtime,m.filename,m.fafilename  && For 997 ack, per Todd, 08.08.2014
WAIT WINDOW "In Synclaire MBOL process" TIMEOUT 2

lTesting   = .F.  && Set to .t. for testing (default = .f.)
lTestinput = .F.  && Set to .t. for test input files only! (default = .f.)
lTestSplits = .F.
lSplitPT = .F.
lDoFirstRound = .T.
lAckdata = .T.
SET REPROCESS TO 500
IF DATETIME()<DATETIME(2017,11,02,14,30,00)
	ON ERROR DEBUG
ELSE
	ON ERROR THROW
ENDIF

goffice = cOffice
WAIT WINDOW "This is a "+IIF(lTesting,"Test","Production")+" 945 MBOL run" TIMEOUT 3
lDoCatch = .T.
DO m:\dev\prg\_setvars WITH lTesting
ON ESCAPE CANCEL
cOffice = IIF(lTesting,"C",cOffice)
nAcctNum = 6521
SET REPROCESS TO 500

cMod = IIF(cOffice = "C","2",cOffice)
goffice = cMod
gMasterOffice = cOffice
lUseSQLBL = .T.

cEDIType = "945"
cSubBOLDetail = ""

m.accountid = nAcctNum
m.edicode = "SW"
m.fafilename = ""
lParcelType = .F.

cfd = "*"
csegd = "^"
nSubCnt = 0
nTotSubs = 0
cStatus = ""
dRundate = DATE()-5
lScanpack = .F.
cRELEASENUM = ""
cW0504 = ""

TRY
	DO CASE
		CASE cOffice = "M"
			cCustLoc =  "FL"
			cFMIWarehouse = ""
			cDivision = "Florida"
			cSF_Addr1  = "11400 NW 32ND AVE"
			cSF_CSZ    = "MIAMI"+cfd+"FL"+cfd+"33167"

		CASE cOffice = "N"
			cCustLoc =  "NJ"
			cFMIWarehouse = ""
			cDivision = "New Jersey"
			cSF_Addr1  = "800 FEDERAL BLVD"
			cSF_CSZ    =  "CARTERET"+cfd+"NJ"+cfd+"07008"

		OTHERWISE
			cCustLoc =  "SP"
			cFMIWarehouse = ""
			cDivision = "San Pedro"
			cSF_Addr1  = "450 WESTMONT DRIVE"
			cSF_CSZ    = "SAN PEDRO"+cfd+"CA"+cfd+"90731"
	ENDCASE

	WAIT WINDOW "At start of MBOL process" NOWAIT
	lTestMail = lTesting && Sends mail to Joe only
	lDoSynFilesOut = !lTesting && IIF(lTesting ,.F.,.T.) && If true, copies output to FTP folder (defaults to .t.)

	lDoCartons = IIF(DATE() = dRundate,.F.,.T.) && Set to false to scan SQL Labels file (default = .t.)
	lDoCompare = IIF(DATE() = dRundate,.F.,.T.)

*!*    lTestMail = .t.
*!*	    lDoSynFilesOut = .f.

	lStandalone = lTesting
	ON ERROR THROW
	lBrowVPP = lTesting
	lBrowBOLs = .F.
	lDoUpdate = .T.
	lUCC = .T.  && Set to false to order SQL results by carton ID instead of UCC (default = .t.)
	lEmail = .T.

	lSkipCompare = !lDoCartons
	lSkipCompare = IIF(cBOL = '04907316521335197',.t.,lSkipCompare)

	IF !lDoUpdate
		lTestMail = .T.
		lDoSynFilesOut = .F.
	ENDIF

	STORE "" TO cShip_ref,cErrMsg,tattach,ccustname,cWO_NumStr,cWO_NumList,tcc,tmessage,cFreight,lcKey
	STORE "" TO cStyle,cTargetStyle,cString,cPTString,cMBOL,cShip_ref_out,cShip_refSH,cLoadid
	STORE .F. TO lFederated,lIsError,lDoBOLGEN,lSamples,lSQLMail,lNonEDI
	STORE .T. TO lCloseOutput,lISAFlag,lSTFlag
	STORE "L" TO cWeightUnit
	STORE "LB" TO cWeightUnit2
	STORE "CF" TO cCubeUnit
	cPPName = "Synclaire"
	cMailName = cPPName
	tfrom = "Toll Warehouse Operations <toll-edi-ops@tollgroup.com>"
	nLoadid = 1
	nCutUCCs = 0
	STORE 0 TO nFilenum,alength,nLength,nSegCtr,nSTCount,nTotCtnWt,nTotCtnCount,nUnitSum
	cterminator = ">" && Used at end of ISA segment
	cOrig = "J"
	lPrepack = .F.
	cCoNum = ""
	lNewPT = .T.

*!* SET CUSTOMER CONSTANTS
	ccustname = "SYNCLAIRE"  && Customer Identifier
	cX12 = "004010"  && X12 Standards Set used
	SELECT 0

	WAIT WINDOW "At the start of SYNCLAIRE EDI MBOL 945 process..." TIMEOUT 1

	DO m:\dev\prg\lookups

	IF lTesting OR lTestinput
		CLOSE DATABASES ALL
		SELECT 0
		IF !USED('edi_trigger')
			cEDIFolder = "F:\3PL\DATA\"
			USE (cEDIFolder+"edi_trigger") IN 0 ALIAS edi_trigger
		ENDIF
*!* TEST DATA AREA
		nAcctNum = 6521
		cBOL = "04907316521068293" && Master BOL #
		cOffice = "C"
		ctime1 = DATETIME()
		lFedEx = .F.
	ENDIF

	cBOL=TRIM(cBOL)
	cOffice = IIF(cOffice = '2','C',cOffice)
	IF VARTYPE(ctime1) = "L"
		ctime1 = DATETIME()
	ENDIF

	cUseFolder = "F:\WH2\WHDATA\"
	IF lTestinput
		cUseFolder = "F:\WHP\WHDATA\"
	ENDIF

	IF lTesting
		lDoSynFilesOut = .F.
	ENDIF

	WAIT WINDOW "Now preparing tables and variables" NOWAIT NOCLEAR

	IF USED('mm')
		USE IN mm
	ENDIF
	SELECT 0
	USE F:\3pl\DATA\mailmaster ALIAS mm SHARED
	LOCATE FOR mm.edi_type = "945" AND mm.acctname = "SYNCLAIRE-EDI-MBOL" AND accountid = nAcctNum
	IF !FOUND()
		cErrMsg = "MISS MAIL DATA"
		DO ediupdate WITH cErrMsg,.T.
		THROW
	ENDIF
	lUseAlt = mm.use_alt
	tsendto = ALLTRIM(IIF(lUseAlt,mm.sendtoalt,mm.sendto))
	tcc = ALLTRIM(IIF(lUseAlt,mm.ccalt,mm.cc))
	cHoldFolder = ALLTRIM(mm.holdpath)
	cArchFolder = ALLTRIM(mm.archpath)
	cOutFolder = ALLTRIM(mm.basepath)
	lcPath = cOutFolder
	IF lTesting OR lTestMail
		LOCATE FOR mm.edi_type = "MISC" AND mm.taskname = "GENERAL"
		lUseAlt = mm.use_alt
		tsendto = ALLTRIM(IIF(lUseAlt,mm.sendtoalt,mm.sendto))
		tcc = ALLTRIM(IIF(lUseAlt,mm.ccalt,mm.cc))
		tsendtoerr = ALLTRIM(IIF(lUseAlt,mm.sendtoalt,mm.sendto))
		tccerr = ALLTRIM(IIF(lUseAlt,mm.ccalt,mm.cc))
	ELSE
		LOCATE FOR mm.edi_type = "MISC" AND mm.taskname = "GENERAL"
		lUseAlt = mm.use_alt
		tsendtozero = ALLTRIM(IIF(lUseAlt,mm.sendtoalt,mm.sendto))
		tcczero = ALLTRIM(IIF(lUseAlt,mm.ccalt,mm.cc))
		tsendtotest = ALLTRIM(IIF(lUseAlt,mm.sendtoalt,mm.sendto))
		tcctest = ALLTRIM(IIF(lUseAlt,mm.ccalt,mm.cc))
		tsendtoerr = tsendtotest
		tccerr = tcctest
	ENDIF
	USE IN mm

*!* SET OTHER CONSTANTS

	cWhseAbbrev = 'TOLL'+cCustLoc
	cCustFolder = UPPER(ccustname)+"\945OUT\"
	cDelTime = SUBSTR(TTOC(ctime1,1),9,4)
	cCarrierType = "M" && Default as "Motor Freight"
	dApptNum = ""

*!* SET OTHER CONSTANTS
	csendqual = "ZZ"
	IF lTesting = .T.
		csendid = "TGF-US-TEST"
	ELSE
		csendid = "TGF-US-"+ICASE(cOffice = "I","NJ",cOffice = "M","FL","SP")
	ENDIF

** PAD ID Codes
	csendidlong = PADR(csendid,15," ")

	CD &cHoldFolder
	DELETE FILE *.*

	STORE cBOL TO cMBOL
	closedata()

	xsqlexec("select * from outship where accountid = "+TRANSFORM(nAcctNum)+" and office = '"+cOffice+"' and bol_no = '"+cBOL+"'",,,"wh")

	IF RECCOUNT() = 0
		cErrMsg = "MISS BOL "+cBOL
		DO ediupdate WITH cErrMsg,.T.
		THROW
	ENDIF
	INDEX ON bol_no TAG bol_no
	INDEX ON ship_ref TAG ship_ref
	INDEX ON wo_num TAG wo_num

	selectoutdet()
	SELECT outdet
	INDEX ON outdetid TAG outdetid

	csq1 = [select * from bl where mblnum = ']+cMBOL+[']
	xsqlexec(csq1,,,"wh")

	useca("ackdata","wh")

	SELECT 0
	CREATE CURSOR tempinfo (subbol c(20),ptcount c(10),ptstring m)

	SELECT bl
	LOCATE
	SELECT 0
	IF lUseSQLBL
		SELECT bol_no FROM bl INTO CURSOR tempbl1 GROUP BY 1
	ELSE
		SELECT bol_no FROM bl WHERE bl.mblnum = cMBOL INTO CURSOR tempbl1 GROUP BY 1
	ENDIF
	IF lBrowBOLs
		SELECT tempbl1
		LOCATE
		BROWSE TIMEOUT 10
	ENDIF
	STORE RECCOUNT() TO nSubBillCnt
	IF nSubBillCnt = 0
		cErrMsg = "NO SUBBOLS: "+cMBOL
		ediupdate(cErrMsg,.T.)
	ENDIF
	cSubBillCnt = ALLTRIM(STR(nSubBillCnt))
	USE IN tempbl1
	SELECT bl
	LOCATE

*!* Mail notice for all MBOL 945s
	IF lTesting
		WAIT CLEAR
		WAIT WINDOW "Expected number of 945 files: "+cSubBillCnt TIMEOUT 2
	ELSE
		tsubject2 = "NOTICE: Synclaire AMT 945 MBOL Process"
		tattach2 = " "
		SELECT 0
		USE F:\3pl\DATA\mailmaster ALIAS mm SHARED
		LOCATE FOR edi_type = "MISC" AND taskname = "GENERAL"
		lUseAlt = mm.use_alt
		tsendto2 = ALLTRIM(IIF(lUseAlt,mm.sendtoalt,mm.sendto))
		tcc2 = ALLTRIM(IIF(lUseAlt,mm.ccalt,mm.cc))
		IF lTesting
			tcc2 = ""
		ENDIF
		USE IN mm

		tmessage2 = "Potential files for Synclaire MBOL# "+cBOL+"...monitor output for correct number of files."
		tmessage2 = tmessage2 + CHR(13) + "Office "+cOffice+", Expected number of 945 files: "+cSubBillCnt
		tmessage2 = tmessage2 + CHR(13) + "Processed at "+TTOC(DATETIME())
		DO FORM m:\dev\FRM\dartmail2 WITH tsendto2,tfrom,tsubject2,tcc2,tattach2,tmessage2,"A"
	ENDIF
*!* End Mailing section

	SELECT 0
	CREATE CURSOR tempov (ship_ref c(20))

	lOverflow = .F.
	DO m:\dev\prg\overflowpt_xcheck WITH cBOL

	IF lOverflow
		WAIT WINDOW "There are one or more OV PTs attached to this Master BOL" TIMEOUT 2
	ENDIF

	SELECT outship
	IF !SEEK(cBOL,'outship','bol_no')
		WAIT WINDOW "BOL not found in OUTSHIP" TIMEOUT 2
		DO ediupdate WITH "BOL NOT IN O/S",.T.
		THROW
	ENDIF

	SELECT dcnum FROM outship WHERE outship.bol_no = cBOL GROUP BY 1 INTO CURSOR tempx
	LOCATE
	IF EOF()
		cErrMsg = "EMPTY DC/STORE#"
		ediupdate(cErrMsg,.T.)
		THROW
	ENDIF
	IF USED('tempx')
		USE IN tempx
	ENDIF

	nWO_Num = outship.wo_num
	cWO_Num = ALLTRIM(STR(nWO_Num))
	cShip_ref = ALLTRIM(outship.ship_ref)
	cRetMsg = " "

	csql = "tgfnjsql01"

*!*		SELECT outdet
*!*		LOCATE
*!*		SELECT outship
*!*		LOCATE
*!*		IF SEEK(cBOL,'outship','bol_no')
*!*			alength = ALINES(apt,outship.shipins)  && Outship SHIPINS array
*!*			cSQLServer = segmentget(@apt,'SQLSERVER',alength)
*!*			cSQL = IIF(LEFT(cSQLServer,3)="SQL","SQL4",cSQLServer)
*!*			IF !lTesting AND !lSkipCompare
*!*				DO m:\dev\prg\sqldata-compare_bcny WITH cUseFolder,cBOL,nWO_Num,cOffice,nAcctNum,cPPName
*!*				IF cRetMsg<>"OK"
*!*					lCloseOutput = .T.
*!*					DO ediupdate WITH cRetMsg,.T. && "SQL ERROR, WO "+cWO_Num,.T.
*!*					THROW
*!*				ENDIF
*!*			ENDIF
*!*		ENDIF

	IF USED("sqlptbcny")
		USE IN sqlptbcny
	ENDIF
	DELETE FILE "F:\3pl\DATA\sqlptbcny.dbf"
	SELECT DISTINCT ship_ref,IIF("SCANPACK"$shipins,.T.,.F.) AS SCANPACK ;
		FROM outship ;
		WHERE outship.bol_no = cBOL ;
		INTO DBF "F:\3pl\DATA\sqlptbcny.dbf"

	WAIT WIND "Now in SQL Connect process..." NOWAIT NOCLEAR
	cRetMsg = ""
	SET STEP ON 
	lDoCartons = IIF(cMBOL = '04907316521300454',.f.,lDoCartons)
	DO m:\dev\prg\sqlconnect_bol_synclaire  WITH nAcctNum,ccustname,cPPName,.T.,cOffice,.f.,.f.,lDoCartons
	IF cRetMsg<>"OK"
		WAIT WINDOW "Error in cartonization extract" TIMEOUT 2
		DO ediupdate WITH cRetMsg,.T.
		THROW
	ENDIF

	IF lTesting OR lTestMail
		SELECT vsynclairepp
		LOCATE
*    BROWSE TIMEOUT 20
	ENDIF

	IF lBrowVPP
		DEFINE WINDOW WINSML FROM 3,3 TO 40,60 FLOAT
		SELECT vsynclairepp
		LOCATE
*    BROWSE IN WINDOW WINSML
	ENDIF

	IF !USED("scacs")
		xsqlexec("select * from scac","scacs",,"wh")
		INDEX ON scac TAG scac
	ENDIF

	SELECT outship
	SET ORDER TO
	LOCATE
	WAIT CLEAR
	WAIT WINDOW "Now creating Header information" TIMEOUT 1

	IF !SEEK(nWO_Num,'outship','wo_num')
		WAIT CLEAR
		WAIT WINDOW "Invalid Work Order Number - Not Found in OUTSHIP!" TIMEOUT 2
		DO ediupdate WITH "WO NOT FOUND",.T.
		THROW
	ENDIF


*************************************************************************
*   OUTSHIP RECORD MAIN LOOP
*  Loops through individual OUTSHIP lines for BOL#
*************************************************************************
	WAIT CLEAR
	WAIT WINDOW "Now creating MBOL#-based information..." NOWAIT NOCLEAR

	SELECT 0
	CREATE CURSOR mfilesout945 (ship_ref c(20),filename c(60),isa_num c(9))
	SELECT mfilesout945
	INDEX ON ship_ref TAG ship_ref

	SELECT outship
	SET ORDER TO
	COUNT TO N FOR outship.bol_no = cBOL AND !EMPTYnul(del_date)
	IF N=0
		DO ediupdate WITH "INCOMP BOL, Del Date",.T.
		THROW
	ENDIF
	LOCATE

	WAIT CLEAR
	WAIT WINDOW "Now retrieving Master BOL#-based information..." NOWAIT NOCLEAR
	nSubBillCnt = 0
	nAtNewSubBill = 0
	SELECT bl
	LOCATE
	cMissDel = ""
	nBLID = 0
	nFileID = 0
	lMultiAcct = .F.
	bolscanstr = "bl.mblnum = cMBOL and bl.accountid = nAcctNum"
	IF lUseSQLBL
		csq1 = [select accountid from bl where mblnum = ']+cMBOL+[' and accountid = ]+TRANSFORM(nAcctNum)+[ group by accountid]
		xsqlexec(csq1,"tempacctid",,"wh")
	ELSE
		SELECT DISTINCT accountid ;
			FROM bl ;
			WHERE &bolscanstr ;
			INTO CURSOR tempacctid
	ENDIF

	SELECT tempacctid
	IF RECCOUNT() > 1
		WAIT CLEAR
		WAIT WINDOW "There are multiple accounts in this Master BOL" TIMEOUT 2
		lMultiAcct = .T.
		cAcctStr = ""
		LOCATE
		SCAN
			IF EMPTY(cAcctStr)
				cAcctStr = ALLTRIM(STR(tempacctid.accountid))
			ELSE
				cAcctStr = cAcctStr+","+ALLTRIM(STR(tempacctid.accountid))
			ENDIF
		ENDSCAN
	ENDIF

	USE IN tempacctid

	lDoISAHeader = .T.
	SELECT bl
	COUNT TO nTotSubs FOR &bolscanstr
	cTotSubs = TRANSFORM(nTotSubs)
	WAIT WINDOW "Will scan through "+cTotSubs+" sub-BOLs..." TIMEOUT 1
	LOCATE FOR &bolscanstr
	SCAN FOR &bolscanstr
*!* HEADER LEVEL EDI DATA
		SELECT 0
		SELECT * FROM "f:\3pl\data\ackdatashell" WHERE .F. INTO CURSOR tempack READWRITE
		SELECT bl
		DO num_incr_isa

		cISA_Num = PADL(c_CntrlNum,9,"0")
		nISA_Num = INT(VAL(cISA_Num))
		m.isanum = cISA_Num
		m.groupnum = c_CntrlNum

		IF lTesting
			cISACode = "T"
		ELSE
			cISACode = "P"
		ENDIF
		nBOLQty = 0
		nBOLWt  = 0

		cRolledRecs = ""
		CREATE CURSOR temp945 (accountid i,isa_num c(10),st_num c(10),wo_num i,bol_no c(20),ship_ref c(20),filename c(50),filedate T)
		renewdatetime()
		WAIT WINDOW "" TIMEOUT 2
		nAcctNum = bl.accountid
		cBOL = ALLTRIM(bl.bol_no)
		cSubBOLDetail = IIF(EMPTY(cSubBOLDetail),cBOL,cSubBOLDetail+CHR(13)+cBOL)

		cWO = ALLTRIM(STR(nWO_Num))
		nBLID = bl.blid

		IF lTesting
			WAIT WINDOW "Now processing sub-BOL "+cBOL TIMEOUT 2
		ENDIF

*!* This section added to trap cut sub-BOLs or PTs
		WAIT WINDOW "Check for cut orders..." TIMEOUT 2
		CREATE CURSOR addpts (ship_ref c(20),del_date d,delenterdt T)

		TRY
			csq1 = [select * from bldet where mod=']+goffice+[' and blid=]+TRANSFORM(nBLID)+[ and accountid = ]+TRANSFORM(nAcctNum)
			xsqlexec(csq1,,,"wh")
		CATCH TO oErr
			WAIT WINDOW "Try/Catch at Line 482 error"
			THROW
		ENDTRY

		SELECT ship_ref,combinedpt FROM bldet WHERE bldet.blid = nBLID INTO CURSOR tmpbldet
		LOCATE
		SCAN
			IF tmpbldet.combinedpt
				SELECT ship_ref,del_date,IIF(EMPTYnul(delenterdt),blenterdt,delenterdt) AS delenterdt FROM outship WHERE outship.combpt = tmpbldet.ship_ref INTO CURSOR t1
			ELSE
				SELECT ship_ref,del_date,IIF(EMPTYnul(delenterdt),blenterdt,delenterdt) AS delenterdt FROM outship WHERE outship.ship_ref = tmpbldet.ship_ref INTO CURSOR t1
			ENDIF
			SELECT addpts
			APPEND FROM DBF('t1')
			USE IN t1
		ENDSCAN
		SELECT addpts
		nRecAP= RECCOUNT('addpts')
		COUNT TO nempties FOR EMPTY(addpts.del_date) AND EMPTY(addpts.delenterdt)
		USE IN addpts
		IF nempties = nRecAP
			LOOP
		ENDIF
*!* End of addition

		DO m:\dev\prg\swc_cutctns WITH cBOL

		SELECT bl

*!*    Insert MBOL/SBOL numbers below to run single sub-BOLs
		IF (cMBOL = "00866943000228471" AND cBOL # "00866943000228143")  && and lTesting
			LOOP
		ENDIF


		WAIT CLEAR
		IF lTesting
			WAIT WINDO "At BL MBOL scan. MBOL: "+cMBOL+", SubBOL: "+cBOL NOWAIT
		ELSE
			WAIT WINDO "At BL MBOL scan. MBOL: "+cMBOL+", SubBOL: "+cBOL TIME 2
		ENDIF
		SCATTER FIELDS accountid MEMVAR

		nAtNewSubBill = nAtNewSubBill + 1
		cAtNewSubBill = ALLTRIM(STR(nAtNewSubBill))
		nFileID = nFileID+1
		cFileID = "-"+ALLT(STR(nFileID))

		IF lDoISAHeader
			lDoISAHeader = .F.
			crecqual = "01"
			crecid = "087607334"
			crecidlong = PADR(crecid,15," ")

			STORE "" TO cFilenameShort,cFilenameStage,cFilenameSend,cFilenameArch
			cFilenameShort = cCoNum+"_"+cWhseAbbrev+"_945_"+TTOC(DATETIME(),1)+".edi"
			cFilenameHold = (cHoldFolder+cFilenameShort)
			cFilenameOut = (cOutFolder+cFilenameShort)
			m.filename = cFilenameOut
			cFilenameArch = (cArchFolder+cFilenameShort)

			nFilenum = FCREATE(cFilenameHold)
			IF nFilenum=(-1)
				WAIT WINDOW "Can't create LL File" TIMEOUT 2
				DO ediupdate WITH .T.,"BAD LL FILE CREATE"
			ENDIF

			STORE "ISA"+cfd+"00"+cfd+"          "+cfd+"00"+cfd+"          "+cfd+csendqual+cfd+csendidlong+cfd+;
				crecqual+cfd+crecidlong+cfd+cTruncDate+cfd+cTruncTime+cfd+"U"+cfd+"00401"+cfd+;
				cISA_Num+cfd+"0"+cfd+cISACode+cfd+cterminator+csegd TO cString
			DO cstringbreak

			STORE "GS"+cfd+"SW"+cfd+csendid+cfd+crecid+cfd+cDate+cfd+cTruncTime+cfd+c_CntrlNum+;
				cfd+"X"+cfd+cX12+csegd TO cString
			DO cstringbreak
		ENDIF

		cOSScanStr=""
		WAIT WINDOW "" TIMEOUT 3

		SELECT 0
		CREATE CURSOR zeroqty (consignee c(40),ship_ref c(20),cnee_ref c(25))

		SELECT bldet
		LOCATE FOR blid=nBLID
		IF !FOUND()
			SET STEP ON
			cErrMsg = "BLID ERR1: "+ALLTRIM(STR(nBLID))
			ediupdate(cErrMsg,.T.)
		ENDIF
		SELECT 0
		CREATE CURSOR temppt (ship_ref c(20))
		nSubCnt = nSubCnt + 1

		SELECT bldet
		WAIT WINDOW "Scanning BLID: "+TRANSFORM(nBLID) NOWAIT
		LOCATE
		nPTCount = 0
		SCAN FOR bldet.blid = nBLID AND bldet.detqty > 0
			IF nBLID = 3690500
				SET STEP ON
			ENDIF
			CREATE CURSOR holdsplits (ship_ref1 c(10))
			SELECT temppt
			INDEX ON ship_ref TAG ship_ref
*      ASSERT .F. MESSA "AT BLDET SCAN...DEBUG HERE!!!"
			cBLShip_ref = ALLTRIM(bldet.ship_ref)
			lCombPT = IIF(bldet.combinedpt,.T.,.F.)
			nAcctNumDet = bldet.accountid
			IF lMultiAcct  && If multiple accounts in a single MBOL.
				IF lCombPT
					cOSScanStr = "outship.combpt = PADR(cBLShip_ref,20)" && AND INLIST(OUTSHIP.ACCOUNTID,&cAcctStr)"
				ELSE
					cOSScanStr = "outship.ship_ref = PADR(cBLShip_ref,20)" && AND INLIST(OUTSHIP.ACCOUNTID,&cAcctStr)"
				ENDIF
			ELSE
				IF lCombPT
					cOSScanStr = "outship.combpt = PADR(cBLShip_ref,20) AND OUTSHIP.ACCOUNTID=nAcctNumDet"
				ELSE
					cOSScanStr = "outship.ship_ref = PADR(cBLShip_ref,20) AND OUTSHIP.ACCOUNTID=nAcctNumDet"
				ENDIF
			ENDIF


			SELECT outship
			LOCATE FOR &cOSScanStr
			IF !FOUND()
				ASSERT .F. MESSAGE "OS-BLDET Mismatch...debug"
				SET STEP ON
				cErrMsg = "No OS/BLDET match, SUBBOL "+cBOL
				ediupdate(cErrMsg,.T.)
				THROW
			ENDIF

			SELECT outship
			LOCATE
			SUM(outship.ctnqty) TO nSubBOLQty FOR &cOSScanStr
			LOCATE
			SUM(outship.weight) TO nSubBOLWt FOR &cOSScanStr
			IF EMPTY(nBOLWt) AND (lTestinput OR lTesting)
				nBOLWt = 2*nBOLQty
			ENDIF
			nBOLQty = nBOLQty + nSubBOLQty
			nBOLWt  = nBOLWt  + nSubBOLWt
			cShip_refLast = "XXXX"
			SELECT outship
			LOCATE FOR &cOSScanStr
			WAIT WIND "Scanning for outship.ship_ref =  "+cBLShip_ref NOWAIT
			SCAN FOR &cOSScanStr
				lScanpack = IIF("PROCESSMODE*SCANPACK"$outship.shipins,.T.,.F.)
				IF ((" OV"$outship.ship_ref) OR (outship.qty = 0) OR (outship.ctnqty = 0 AND !lMasterpack))
					IF outship.qty = 0
						INSERT INTO zeroqty (consignee,ship_ref,cnee_ref) VALUES (outship.consignee,outship.ship_ref,outship.cnee_ref)
					ENDIF
					LOOP
				ENDIF
				cConsignee = ALLT(outship.consignee)
				lSaks = IIF(cConsignee = "SAKS.COM",.T.,.F.)
				lSCACConv = IIF("KOHLS.COM"$cConsignee OR "BELK.COM"$cConsignee OR "QVC"$cConsignee OR "TOYS"$cConsignee OR "BBB"$cConsignee OR "BB&B"$cConsignee OR ("BED"$cConsignee AND "BATH"$cConsignee),.T.,.F.)
				IF lSCACConv  && If one of the selected SCAC conversion consignees...
					cSCAINFO = ALLTRIM(segmentget(@apt,"SCAINFO",alength)) && Added this section per Todd, 08.18.2014
*          cSCAINFO = LEFT(cSCAINFO, AT("~",cSCAINFO)-1)
					cShip_SCAC = cSCAINFO
					IF lSCACConv AND LEFT(cSCAINFO,3) = "UNS"
						SET STEP ON
						SELECT bcny_scac_conv
						LOCATE FOR bcny_scac_conv.scac_940 = cSCAINFO AND bcny_scac_conv.scac = ALLTRIM(outship.scac)
						IF FOUND()
							cShip_SCAC = ALLTRIM(bcny_scac_conv.scac_945)
						ELSE  && Will send a notification mail to Todd if the SCAC is unmatched
							cErrMsg = "NO CONVERSION SCAC"
							DO ediupdate WITH cErrMsg,.T.
							THROW
						ENDIF
					ENDIF
				ENDIF

				SCATTER MEMVAR
				nWO_Num = outship.wo_num
				cShip_ref = ALLTRIM(outship.ship_ref)
				IF lToys
					SUM cuft TO nCuFT FOR bol_no = cBOL
					cCuFt = ALLTRIM(STR(CEILING(nCuFT)))
					RELEASE nCuFT
					GO nRec
				ENDIF
				cShip_refUse = IIF(lSplitPT,ALLTRIM(LEFT(cShip_ref,AT("~",ALLTRIM(cShip_ref))-1)),cShip_ref)
				lNewPT = IIF(cShip_refLast = cShip_refUse,.F.,.T.)
				STORE cShip_refUse TO cShip_refLast

				lMasterpack = outship.masterpack
				nPTCount = nPTCount + 1
				cPO = ALLTRIM(outship.cnee_ref)
				IF EMPTY(cShip_ref)
					DO ediupdate WITH "EMPTY PT-WO "+cWO_Num,.T.
					THROW
				ENDIF
				INSERT INTO mfilesout945 (ship_ref,filename,isa_num) VALUES (cShip_ref,cFilenameShort,cISA_Num)

				alength = ALINES(apt,outship.shipins)  && Outship SHIPINS array
				cFOBPymt = segmentget(@apt,'FOBPYMT',alength)
				cFOB = segmentget(@apt,'FOB',alength)
				lSplitPT = IIF("~"$cShip_ref,.T.,.F.)
				lSplitPT = IIF(cMBOL#'88784233000012886',lSplitPT,.F.)
				IF lSplitPT
					WAIT WINDOW "This is a SPLIT PT" TIMEOUT 1
				ELSE
					tx = 1
				ENDIF

				IF "~"$cShip_ref AND lSplitPT
					m.ship_ref1 = LEFT(cShip_ref,AT("~",cShip_ref)-1)
					SELECT holdsplits
					LOCATE FOR holdsplits.ship_ref1 = m.ship_ref1
					IF !FOUND()
						INSERT INTO holdsplits FROM MEMVAR
						SELECT outship
					ENDIF
				ENDIF

				IF lSplitPT
					cShip_ref_out = ALLT(LEFT(cShip_ref,AT('~',cShip_ref)-1))
					cAltShip_ref = IIF("U"$cShip_ref,cShip_ref_out+" ~C",cShip_ref_out+" ~U")
					IF SEEK(cShip_ref_out,'temppt','ship_ref')
						LOOP
					ELSE
						INSERT INTO temppt (ship_ref) VALUES (cShip_ref_out)
					ENDIF
				ELSE
					STORE cShip_ref TO cShip_ref_out
				ENDIF

				IF EMPTY(ALLT(m.appt_time))
					cApptTime = "153000"
				ELSE
					nApptTime = INT(VAL(LEFT(m.appt_time,4)))
					IF SUBSTR(m.appt_time,5,1)="P"
						IF BETWEEN(nApptTime,1200,1259)
							cApptTime = ALLT(STR(nApptTime))+"00"
						ELSE
							cApptTime = ALLT(STR(nApptTime + 1200))+"00"
						ENDIF
					ELSE
						cApptTime = PADL(ALLT(STR(nApptTime)),4,"0")+"00"
					ENDIF
				ENDIF

*!* Added this code to trap miscounts in OUTDET Units

				xsqlexec("select * from outwolog where wo_num = "+TRANSFORM(m.wo_num),,,"wh")
				IF RECCOUNT("outwolog") = 0
					ASSERT .F. MESSAGE "In unfound WOLOG record...DEBUG"
					SET STEP ON
					DO ediupdate WITH "NO WO-OWOLOG "+TRANSFORM(m.wo_num),.T.
					THROW
				ENDIF
				lPick = outwolog.picknpack
				lPrepack = !lPick
				WAIT WINDOW "This is a "+IIF(lPrepack,"Pre-pack","PnP")+" order" NOWAIT

				IF lPrepack
					SET DELETED ON
					IF lOverflow
						SELECT tempov
						LOCATE FOR tempov.ship_ref = cShip_ref
						IF FOUND()
							SET DELETED OFF
						ENDIF
					ENDIF

					SELECT outdet
					SET ORDER TO

					SUM IIF(lOverflow,outdet.origqty,outdet.totqty) TO nUnitTot1 FOR units AND outdet.outshipid = outship.outshipid

					SUM (VAL(PACK)*IIF(lOverflow,outdet.origqty,outdet.totqty)) TO nUnitTot2 FOR !units AND outdet.outshipid = outship.outshipid

					IF nUnitTot1<>nUnitTot2
						SET STEP ON
						WAIT WINDOW "TOTQTY ERROR AT PT "+cShip_ref+" "+ALLTRIM(TRANSFORM(nUnitTot1))+" "+ALLTRIM(TRANSFORM(nUnitTot2)) TIMEOUT 5
						DO ediupdate WITH "OUTDET UNITQTY ERR"+" SQL= "+ALLTRIM(TRANSFORM(nUnitTot1))+" OS= "+ALLTRIM(TRANSFORM(nUnitTot2)),.T.
						THROW
					ENDIF

					SELECT vsynclairepp
					SUM totqty TO nUnitTot2 FOR vsynclairepp.outshipid = outship.outshipid AND vsynclairepp.totqty > 0
					IF nUnitTot1<>nUnitTot2
						SET STEP ON
						DO ediupdate WITH "SQL TOTQTY ERR "+TRANSFORM(outship.wo_num)+" SQL= "+ALLTRIM(TRANSFORM(nUnitTot1))+" OS= "+ALLTRIM(TRANSFORM(nUnitTot2)),.T.
						THROW
					ENDIF
				ENDIF
				SET DELETED ON

				cPTString = IIF(EMPTY(cPTString),PADR(ALLTRIM(m.consignee),42)+cShip_ref_out,cPTString+CHR(13)+;
					PADR(ALLTRIM(m.consignee),42)+cShip_ref_out)
*nPTCount = nPTCount+1

				nCuFT = CEILING(outship.cuft)
				IF EMPTY(nCuFT)
					IF lTesting
						nCuFT = (2*outship.qty)
					ELSE
						DO ediupdate WITH "EMPTY CU.FT PT: "+cShip_ref,.T.
						THROW
					ENDIF
				ENDIF
				lMacy = IIF("MACY"$m.consignee,.T.,.F.)
				lJCPenney = IIF(("JCP"$m.consignee OR "PENNEY"$m.consignee),.T.,.F.)
				lWalmart = IIF((m.consignee="WAL" AND "MART"$m.consignee),.T.,.F.)
				lToys = IIF("TOYS"$m.consignee,.T.,.F.)
				lDillard = IIF("DILLARD"$m.consignee,.T.,.F.)
				lGap = IIF("GAP"$m.consignee,.T.,.F.)
				lAAFES = IIF(("ARMY"$m.consignee AND "AIR"$m.consignee AND "FORCE"$m.consignee),.T.,.F.)
				IF lAAFES
					WAIT WINDOW "THIS IS AN ARMY/AIR FORCE EXCHANGE ORDER" TIMEOUT 1
				ENDIF

				SELECT outdet
				SET ORDER TO TAG outdetid
				SELECT vsynclairepp
				SET RELATION TO outdetid INTO outdet
				SELECT outship

				cWO_Num = ALLTRIM(STR(m.wo_num))
				IF !(cWO_Num$cWO_NumStr)
					cWO_NumStr = IIF(EMPTY(cWO_NumStr),cWO_Num,cWO_NumStr+","+cWO_Num)
				ENDIF
				IF !(cWO_Num$cWO_NumList)
					cWO_NumList = IIF(EMPTY(cWO_NumList),cWO_Num,cWO_NumList+CHR(13)+cWO_Num)
				ENDIF

				nTotCtnCount = m.qty
				cPO_Num = ALLTRIM(m.cnee_ref)

				IF (("PENNEY"$UPPER(outship.consignee)) OR ("KMART"$UPPER(outship.consignee)) OR ("K-MART"$UPPER(outship.consignee)))
					lApptFlag = .T.
				ELSE
					lApptFlag = .F.
				ENDIF

				ddel_date = outship.del_date
				dexpdel_date = outship.expdeldt  && Expected delivery date (at facility)
				IF EMPTY(dexpdel_date)
					dexpdel_date = DATE()+3
				ENDIF

				IF lTestinput
					IF EMPTY(ddel_date)
						ddel_date = DATE()
					ENDIF
					dApptNum = "99999"
					dapptdate = DATE()
				ELSE
					ddel_date = outship.del_date
					IF EMPTY(ddel_date)
						cMissDel = IIF(EMPTY(cMissDel),"The following PTs had no Delivery Dates:"+CHR(13)+TRIM(cShip_ref),cMissDel+CHR(13)+TRIM(cShip_ref))
					ENDIF
					dApptNum = ALLTRIM(outship.appt_num)

					IF !lTesting
						IF EMPTY(dApptNum) && Penney/KMart Appt Number check
							IF !(lApptFlag)
								dApptNum = ""
							ELSE
								DO ediupdate WITH "EMPTY APPT #",.T.
								THROW
							ENDIF
						ENDIF
					ELSE
						dApptNum = "99999"
					ENDIF
					dapptdate = outship.appt

					IF EMPTY(dapptdate)
						dapptdate = outship.del_date
					ENDIF
				ENDIF

				IF ALLTRIM(outship.SForCSZ) = ","
					BLANK FIELDS outship.SForCSZ NEXT 1 IN outship
				ENDIF

				alength = ALINES(apt,outship.shipins,.T.,CHR(13))

				cTRNum = ""
				cPRONum = ""
				STORE STRTRAN(TRIM(outship.keyrec),"PR","") TO cPRONum  && Changed to always use as PRO# per Juan, 10.21.2013

				nOutshipid = m.outshipid
				m.CSZ = TRIM(m.CSZ)
				IF OCCURS(" ",m.CSZ) < 1
					SET STEP ON
					cErrMsg = "TOO FEW SPACES-CSZ "+cShip_ref
					ediupdate(cErrMsg,.T.)
					THROW
				ENDIF

				IF EMPTY(M.CSZ)
					WAIT CLEAR
					WAIT WINDOW "No SHIP-TO City/State/ZIP info...exiting" TIMEOUT 2
					DO ediupdate WITH "BAD ADDRESS INFO",.T.
					THROW
				ENDIF
				IF !lAAFES
					m.CSZ = TRIM(STRTRAN(m.CSZ,"  "," "))
				ENDIF
				IF !","$m.CSZ
					DO ediupdate WITH "NO COMMA CSZ-"+cShip_ref,.T.
					THROW
				ENDIF
				nSpaces = OCCURS(' ',m.CSZ)
				IF nSpaces < 1
					WAIT WINDOW "Ship-to CSZ Segment Error" TIMEOUT 3
					DO ediupdate WITH "Ship-to CSZ: "+cShip_ref,.T.
					THROW
				ELSE
					nCommaPos = AT(",",m.CSZ)
					nLastSpace = AT(" ",m.CSZ,nSpaces)
					IF ISALPHA(SUBSTR(TRIM(m.CSZ),AT(" ",m.CSZ)+1)) OR lAAFES
						cCity = LEFT(TRIM(m.CSZ),AT(",",m.CSZ)-1)
						cState = SUBSTR(m.CSZ,nCommaPos+2,2)
						cZip = TRIM(SUBSTR(m.CSZ,nLastSpace+1))
					ELSE
						WAIT CLEAR
						WAIT WINDOW "NOT ALPHA: "+SUBSTR(TRIM(m.CSZ),AT(" ",m.CSZ)+1) TIMEOUT 3
						cCity = LEFT(TRIM(m.CSZ),AT(",",m.CSZ)-1)
						cState = SUBSTR(TRIM(m.CSZ),AT(" ",m.CSZ)+1,2)
						cZip = TRIM(SUBSTR(m.CSZ,nLastSpace+1))
					ENDIF
				ENDIF

				STORE "" TO cSForCity,cSForState,cSForZip
				IF !lFederated
					IF !EMPTY(M.SForCSZ)
						m.SForCSZ = TRIM(STRTRAN(m.SForCSZ,"  "," "))
						nSpaces = OCCURS(" ",TRIM(m.SForCSZ))
						IF nSpaces = 0
							m.SForCSZ = STRTRAN(m.SForCSZ,",","")
							cSForCity = ALLTRIM(m.SForCSZ)
							cSForState = ""
							cSForZip = ""
						ELSE
							nCommaPos = AT(",",m.SForCSZ)
							nLastSpace = AT(" ",m.SForCSZ,nSpaces)
							nMinusSpaces = IIF(nSpaces=1,0,1)
							IF ISALPHA(SUBSTR(TRIM(m.SForCSZ),AT(" ",TRIM(m.SForCSZ),nSpaces-nMinusSpaces)+1,2)) OR lAAFES
								cSForCity = LEFT(TRIM(m.SForCSZ),AT(",",m.SForCSZ)-1)
								cSForState = SUBSTR(m.SForCSZ,nCommaPos+2,2)
								cSForZip = TRIM(RIGHT(TRIM(m.SForCSZ),5))
								IF ISALPHA(cSForZip)
									cSForZip = ""
								ENDIF
							ELSE
								WAIT CLEAR
								WAIT WINDOW "NOT ALPHA: "+SUBSTR(TRIM(m.SForCSZ),AT(" ",m.SForCSZ,nSpaces-1)+1,2) TIMEOUT 3
								cSForCity = LEFT(TRIM(m.SForCSZ),AT(",",m.SForCSZ)-1)
								cSForState = SUBSTR(TRIM(m.SForCSZ),AT(" ",m.SForCSZ,nSpaces-2)+1,2)
								cSForZip = TRIM(RIGHT(TRIM(m.SForCSZ),5))
								IF ISALPHA(cSForZip)
									cSForZip = ""
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ELSE
					cStoreName = segmentget(@apt,"STORENAME",alength)
				ENDIF

				m.ctnqty = IIF(lMasterpack,1,outship.ctnqty)
				m.weight = outship.weight
				IF lSplitPT
					nRepLoops = 2
					SELECT outship
					nRec = RECNO()
					LOCATE
					LOCATE FOR outship.ship_ref = PADR(TRIM(cAltShip_ref),20)
					IF !FOUND()
						cErrMsg = "Problem: Alternate PT "+cAltShip_ref+" not found"
						WAIT WINDOW cErrMsg TIMEOUT 5
						ediupdate(cErrMsg,.T.)
						THROW

					ENDIF
					IF lTestinput
						IF outship.weight = 0
							REPLACE outship.weight WITH 2*IIF(lMasterpack,1,outship.ctnqty)
						ENDIF
					ENDIF
					m.ctnqty = m.ctnqty + IIF(lMasterpack,1,outship.ctnqty)
					m.weight = m.weight + outship.weight
					nAltOutshipid = outship.outshipid
					GO nRec
				ELSE
					nRepLoops = 1
				ENDIF

				DO num_incr_st
				WAIT CLEAR
				WAIT WINDOW "Now creating Line Item information" NOWAIT NOCLEAR

				INSERT INTO temp945 (accountid,isa_num,st_num,wo_num,bol_no,ship_ref,filename,filedate) ;
					VALUES (m.accountid,c_CntrlNum,c_GrpCntrlNum,m.wo_num,m.bol_no,m.ship_ref,cFilenameShort,dt2)

				m.transnum = PADL(c_GrpCntrlNum,9,"0")
				STORE "ST"+cfd+"945"+cfd+PADL(c_GrpCntrlNum,9,"0")+csegd TO cString
				DO cstringbreak
				nSTCount = nSTCount + 1
				nSegCtr = 1

				insertinto("ackdata","wh",.T.)
				tu("ackdata")

				IF EMPTY(cShip_ref_out)
					WAIT WIND "MISSING SHIP REF IN W06...DEBUG!!"
					SET STEP ON
				ENDIF
				cRELEASENUM = segmentget(@apt,"RELEASENUM",alength)
				cW0504 = segmentget(@apt,"W0504",alength)

				cShip_ref_out = IIF(cMBOL#'00863233000393273',cShip_ref_out,LEFT(cShip_ref_out,8))
				STORE "W06"+cfd+"N"+cfd+cShip_ref_out+cfd+cDate+cfd+TRIM(cMBOL)+cfd+TRIM(cWO_Num)+cfd+cPO_Num+IIF(!EMPTY(cRELEASENUM),cfd+cRELEASENUM,"")+ICASE(!EMPTY(cRELEASENUM) AND !EMPTY(cW0504),cfd+cW0504+csegd,EMPTY(cRELEASENUM) AND !EMPTY(cW0504),REPLICATE(cfd,2)+cW0504+csegd,csegd) TO cString
				DO cstringbreak
				nSegCtr = nSegCtr + 1
				nCtnNumber = 1  && Seed carton sequence count

				cDCNum = ALLTRIM(m.dcnum)
				cDCNumStored = ALLTRIM(segmentget(@apt,"STORENUM",alength))
				IF cDCNum # cDCNumStored
					cDCNum = cDCNumStored
				ENDIF
				IF EMPTY(TRIM(cDCNum))
					IF (!EMPTY(m.storenum) AND m.storenum<>0)
						cDCNum = ALLTRIM(STR(m.storenum))
					ENDIF
					IF EMPTY(cDCNum)
						DO ediupdate WITH "MISS STORENUM",.T.
						THROW
					ENDIF
				ENDIF

				STORE "N1"+cfd+"SF"+cfd+"FMI INTERNATIONAL INC. -"+UPPER(cDivision)+cfd+"92"+cfd+"FMI"+cCustLoc+csegd TO cString
				DO cstringbreak
				nSegCtr = nSegCtr + 1

				IF !lFederated
					STORE "N3"+cfd+TRIM(cSF_Addr1)+csegd TO cString
					DO cstringbreak
					nSegCtr = nSegCtr + 1

					STORE "N4"+cfd+cSF_CSZ+cfd+"USA"+csegd TO cString
					DO cstringbreak
					nSegCtr = nSegCtr + 1
				ENDIF

				STORE "N1"+cfd+"ST"+cfd+ALLTRIM(m.consignee)+cfd+"92"+cfd+cDCNum+csegd TO cString
				DO cstringbreak
				nSegCtr = nSegCtr + 1

				STORE "N3"+cfd+TRIM(outship.address)+csegd TO cString
				DO cstringbreak
				nSegCtr = nSegCtr + 1

				STORE "N4"+cfd+cCity+cfd+cState+cfd+cZip+cfd+"USA"+csegd TO cString
				DO cstringbreak
				nSegCtr = nSegCtr + 1

				cSforstore = TRIM(m.sforstore)
				IF !EMPTY(m.sforstore)
					STORE "N1"+cfd+"BY"+cfd+ALLTRIM(m.shipfor)+cfd+"92"+cfd+TRIM(m.sforstore)+csegd TO cString
					DO cstringbreak
					nSegCtr = nSegCtr + 1
				ENDIF

				STORE "N9"+cfd+"MB"+cfd+ALLTRIM(cMBOL)+csegd TO cString
				DO cstringbreak
				nSegCtr = nSegCtr + 1

				STORE "N9"+cfd+"BM"+cfd+ALLTRIM(cBOL)+csegd TO cString
				DO cstringbreak
				nSegCtr = nSegCtr + 1

				cDept = ALLT(outship.dept)
				cDept = STRTRAN(cDept,"<>","#")
				IF EMPTY(cDept)
					cDept = segmentget(@apt,"DEPT",alength)
				ENDIF
				IF !EMPTY(cDept)
					STORE "N9"+cfd+"DP"+cfd+TRIM(outship.dept)+csegd TO cString
					DO cstringbreak
					nSegCtr = nSegCtr + 1
				ENDIF

				cVendnum = ""
				cVendnum = segmentget(@apt,"VENDORNUM",alength) && Added for 10+ character vendor numbers
				cVendnum = STRTRAN(cVendnum,"<>","#")
				IF EMPTY(cVendnum)
					cVendnum = TRIM(outship.vendor_num)
				ENDIF
				IF !EMPTY(cVendnum)
					STORE "N9"+cfd+"IA"+cfd+cVendnum+csegd TO cString
					DO cstringbreak
					nSegCtr = nSegCtr + 1
				ENDIF

				cSNNum = segmentget(@apt,"SNNUM",alength)
				cSNNum = STRTRAN(cSNNum,"<>","#")
				IF !EMPTY(cSNNum)
					STORE "N9"+cfd+"SN"+cfd+TRIM(cSNNum)+csegd TO cString
					DO cstringbreak
					nSegCtr = nSegCtr + 1
				ENDIF

				cPKNum = segmentget(@apt,"PKNUM",alength)
				cPKNum = STRTRAN(cPKNum,"<>","#")
				IF !EMPTY(cPKNum)
					STORE "N9"+cfd+"PK"+cfd+TRIM(cPKNum)+csegd TO cString
					DO cstringbreak
					nSegCtr = nSegCtr + 1
				ENDIF

				cVNInfo = segmentget(@apt,"VNINFO",alength)
				cVNInfo = STRTRAN(cVNInfo,"<>","#")
				IF !EMPTY(cVNInfo)
					STORE "N9"+cfd+"VN"+cfd+TRIM(cVNInfo)+csegd TO cString
					DO cstringbreak
					nSegCtr = nSegCtr + 1
				ENDIF

				cVINum = segmentget(@apt,"VINUM",alength)
				cVINum = STRTRAN(cVINum,"<>","#")
				IF !EMPTY(cVINum)
					STORE "N9"+cfd+"VI"+cfd+TRIM(cVINum)+csegd TO cString
					DO cstringbreak
					nSegCtr = nSegCtr + 1
				ENDIF

				cTPInfo = segmentget(@apt,"TPINFO",alength)
				cTPInfo = STRTRAN(cTPInfo,"<>","#")
				IF !EMPTY(cTPInfo)
					STORE "N9"+cfd+"TP"+cfd+TRIM(cTPInfo)+csegd TO cString
					DO cstringbreak
					nSegCtr = nSegCtr + 1
				ENDIF

				cSCAINFO = segmentget(@apt,"SCAINFO",alength)
				cSCAINFO = STRTRAN(cSCAINFO,"<>","#")
				IF !EMPTY(cSCAINFO)
					STORE "N9"+cfd+"SCA"+cfd+TRIM(cSCAINFO)+csegd TO cString
					DO cstringbreak
					nSegCtr = nSegCtr + 1
				ENDIF

				cSTInfo = segmentget(@apt,"STINFO",alength)
				cSTInfo = STRTRAN(cSTInfo,"<>","#")
				IF !EMPTY(cSTInfo)
					STORE "N9"+cfd+"ST"+cfd+TRIM(cSTInfo)+csegd TO cString
					DO cstringbreak
					nSegCtr = nSegCtr + 1
				ENDIF

				cWHInfo = segmentget(@apt,"WHINFO",alength)
				cWHInfo = STRTRAN(cWHInfo,"<>","#")
				IF !EMPTY(cWHInfo)
					STORE "N9"+cfd+"WH"+cfd+TRIM(cWHInfo)+csegd TO cString
					DO cstringbreak
					nSegCtr = nSegCtr + 1
				ENDIF

				cSMU = segmentget(@apt,"SMU",alength)
				IF !EMPTY(cSMU)
					STORE "N9"+cfd+"SMU"+cfd+TRIM(cSMU)+csegd TO cString
					DO cstringbreak
					nSegCtr = nSegCtr + 1
				ENDIF

				cATInfo = segmentget(@apt,"ATINFO",alength)
				IF !EMPTY(cATInfo)
					STORE "N9"+cfd+"AT"+cfd+TRIM(cATInfo)+csegd TO cString
					DO cstringbreak
					nSegCtr = nSegCtr + 1
				ENDIF

				cEDIInfo = segmentget(@apt,"EDI-INFO",alength)
				IF !EMPTY(cEDIInfo)
					STORE "N9"+cfd+"TI"+cfd+TRIM(cEDIInfo)+csegd TO cString
					DO cstringbreak
					nSegCtr = nSegCtr + 1
				ENDIF

				cLoadid = ALLTRIM(outship.appt_num)
				IF lToys
					cLoadid = "NA"
					STORE "N9"+cfd+"LO"+cfd+TRIM(cLoadid)+csegd TO cString
					DO cstringbreak
					nSegCtr = nSegCtr + 1
				ELSE
					STORE "N9"+cfd+"LO"+cfd+TRIM(cLoadid)+csegd TO cString
					DO cstringbreak
					nSegCtr = nSegCtr + 1
				ENDIF

				SELECT outship

				cShipType      =""
				cTrailerNumber =""
				cProNumber     =""
				cTerms         =""

				cShip_SCAC = outship.scac
				cShip_SCAC = IIF(lSaks AND outship.scac = "FHOM","FEDH",cShip_SCAC)

				IF lTesting
					cTrailerNumber = ""
					cProNumber = "123456"
					cTerms = "PP"
				ELSE

					SELECT bl
					LOCATE FOR bol_no = cBOL
					IF FOUND()
						cShip_SCAC    = IIF(!EMPTY(bl.scac) AND !lSCACConv,bl.scac,cShip_SCAC)
						cTrailerNumber= LEFT(ALLTRIM(bl.trailer),10)
						cProNumber    = bl.pronumber
						DO CASE
							CASE bl.terms ="1"
								cTerms = "PP"
							CASE bl.terms ="2"
								cTerms = "CC"
							OTHERWISE
								cTerms = ""
						ENDCASE
					ELSE
						cErrMsg = "MISSING BL Data"
						DO ediupdate WITH cErrMsg,.T.
						THROW
					ENDIF
				ENDIF

				IF EMPTYnul(outship.del_date)
					IF lJCPenney
						cErrMsg = "MISSING DELDATE"
						DO ediupdate WITH cErrMsg,.T.
						THROW
					ELSE
						ddel_date = outship.appt
					ENDIF
				ELSE
					ddel_date = outship.del_date
					IF EMPTY(ddel_date)
						IF lTesting
							ddel_date = DATE()
						ELSE
							SELECT edi_trigger
							LOCATE
							LOCATE FOR accountid = nAcctNum AND ship_ref = cShip_ref AND bol = cBOL AND edi_type = "945"
							IF FOUND()
								REPLACE fin_status WITH "MISSING DELDATE",processed WITH .T.,errorflag WITH .T.
								tsubject = "945 Error in BCNY/EA BOL "+TRIM(cBOL)+" (At PT "+cShip_ref+")"
								tattach = " "
								tsendto = tsendtoerr
								tcc = tccerr
								tmessage = "945 Processing for WO# "+TRIM(cWO_Num)+" produced this error: (Office: "+cOffice+"): "+cStatus+CHR(13)+"Please notify Joe of actual status for reprocessing ASAP."
								DO FORM m:\dev\frm\dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
							ENDIF
							LOOP
						ENDIF
					ENDIF

					dapptdate = outship.appt
					IF EMPTY(dapptdate)
						dapptdate = outship.del_date
					ENDIF

				ENDIF

				IF EMPTYnul(outship.del_date) OR EMPTYnul(outship.START) OR EMPTYnul(outship.CANCEL)
					cErrMsg = "EMPTY "+ICASE(EMPTYnul(outship.del_date),"DELDATE",EMPTYnul(outship.START),"START","CANCEL")
					DO ediupdate WITH cErrMsg,.T.
					THROW
				ENDIF

				cUseTime = "153000"
				STORE "G62"+cfd+"11"+cfd+DTOS(del_date)+cfd+"9"+cfd+cUseTime+csegd TO cString  && Ship date
				DO cstringbreak
				nSegCtr = nSegCtr + 1

				cUseTime = "153000"
				STORE "G62"+cfd+"67"+cfd+DTOS(del_date)+cfd+"9"+cfd+cUseTime+csegd TO cString  && Sched ship date
				DO cstringbreak
				nSegCtr = nSegCtr + 1

				STORE "G62"+cfd+"10"+cfd+DTOS(outship.START)+csegd TO cString  && Ship date
				DO cstringbreak
				nSegCtr = nSegCtr + 1

				alength = ALINES(apt,outship.shipins,.T.,CHR(13))
				cPODate = segmentget(@apt,"PODATE",alength)
				IF !EMPTY(cPODate)
					STORE "G62"+cfd+"04"+cfd+cPODate+csegd TO cString  && Ship date
					DO cstringbreak
					nSegCtr = nSegCtr + 1
				ENDIF

				cDNLDate = segmentget(@apt,"DNLDATE",alength)
				IF !EMPTY(cDNLDate)
					STORE "G62"+cfd+"54"+cfd++DTOS(del_date)+csegd TO cString  && Ship date
					DO cstringbreak
					nSegCtr = nSegCtr + 1
				ENDIF

				SELECT outship

				cShipType="M"

				DO CASE
					CASE EMPTY(cTerms) AND EMPTY(cTrailerNumber)
						STORE "W27"+cfd+cShipType+cfd+ALLTRIM(cShip_SCAC)+cfd+ALLTRIM(outship.ship_via)+csegd TO cString
					CASE !EMPTY(cTerms) AND EMPTY(cTrailerNumber)
						STORE "W27"+cfd+cShipType+cfd+ALLTRIM(cShip_SCAC)+cfd+ALLTRIM(outship.ship_via)+cfd+ALLTRIM(cTerms)+csegd TO cString
					OTHERWISE
						STORE "W27"+cfd+cShipType+cfd+ALLTRIM(cShip_SCAC)+cfd+ALLTRIM(outship.ship_via)+cfd+ALLTRIM(cTerms)+cfd+cfd+cfd+ALLTRIM(cTrailerNumber)+csegd TO cString
				ENDCASE
				DO cstringbreak
				nSegCtr = nSegCtr + 1

*************************************************************************
*2  DETAIL LOOP - MATCHING SHIP_REF FIELD (FROM OUTSHIP)
**********4***************************************************************
				WAIT CLEAR
				WAIT WINDOW "Now creating Detail information "+cShip_ref NOWAIT NOCLEAR
				SELECT vsynclairepp
				LOCATE
				LOCATE FOR ship_ref = TRIM(cShip_ref) AND outshipid = nOutshipid
				IF !FOUND()

					IF !lTesting
						WAIT WINDOW "PT "+cShip_ref+" NOT FOUND in vsynclairepp...ABORTING" &cTimeMsg
						IF !lTesting
							cErrMsg = "MISS PT: "+cShip_ref+" "+ALLTRIM(TRANSFORM(nOutshipid))
							DO ediupdate WITH cErrMsg,.T.
						ENDIF
						=FCLOSE(nFilenum)
						ERASE &cFilename
						THROW
					ELSE
						LOOP
					ENDIF
				ENDIF

*!* Added this code to trap unmatched OUTDETIDs in OUTDET, Joe 11.22.2005
				IF nWO_Num = 5123502
					SET DELETED OFF
				ENDIF

				SCAN FOR vsynclairepp.ship_ref = TRIM(cShip_ref) AND vsynclairepp.outshipid = nOutshipid AND vsynclairepp.totqty#0
					IF EMPTY(outdet.outdetid)
						SET STEP ON
						WAIT WINDOW "OUTDETID "+TRANSFORM(vsynclairepp.outdetid)+" NOT FOUND IN OUTDET!...CLOSING" &cTimeMsg
						cErrMsg = "MISS ODID/WO: "+TRANSFORM(vsynclairepp.outdetid)+"/"+TRANSFORM(vsynclairepp.wo_num)
						DO ediupdate WITH cErrMsg,.T.
						=FCLOSE(nFilenum)
						ERASE &cFilename
						THROW
					ENDIF
				ENDSCAN

				SCANSTR = "vsynclairepp.ship_ref = cShip_ref and vsynclairepp.outshipid = nOutshipid and !DELETED()"
				IF nWO_Num = 5123502
					SET DELETED OFF
				ENDIF

				SELECT vsynclairepp
				LOCATE FOR &SCANSTR
				cCartonNum= "XXX"
				cUCC="XXX"

				DO WHILE &SCANSTR
					lSkipBack = .T.

					IF vsynclairepp.totqty = 0 OR vsynclairepp.ucc = 'CUTS'
						SKIP 1 IN vsynclairepp
						LOOP
					ENDIF

					IF vsynclairepp.ucc = 'CUTS'
						SKIP 1 IN vsynclairepp
						LOOP
					ENDIF

					IF TRIM(vsynclairepp.ucc) <> cCartonNum
						STORE TRIM(vsynclairepp.ucc) TO cCartonNum
					ENDIF

					STORE "LX"+cfd+ALLTRIM(STR(nCtnNumber))+csegd TO cString   && Carton Seq. # (up to Carton total)
					lDoManSegment = .T.
					DO cstringbreak
					nSegCtr = nSegCtr + 1

					cDoString = "vsynclairepp.ucc = cCartonNum"
					DO WHILE &cDoString
						IF vsynclairepp.totqty = 0
							SKIP 1 IN vsynclairepp
							LOOP
						ENDIF

						IF lDoManSegment
							cUCC = TRIM(vsynclairepp.ucc)
							IF !EMPTY(cUCC) AND cUCC # "XXX"
								lDoManSegment = .F.
								cPRONum = IIF(lTesting,"999888777",ALLTRIM(outship.keyrec))
								IF lToys AND !EMPTY(cPRONum)
									STORE "MAN"+cfd+"GM"+cfd+TRIM(cUCC)+cfd+cfd+"ZZ"+cfd+cPRONum+csegd TO cString  && UCC Number (Wal-mart DSDC only)
								ELSE
									STORE "MAN"+cfd+"GM"+cfd+TRIM(cUCC)+csegd TO cString  && UCC Number (Wal-mart DSDC only)
								ENDIF
								DO cstringbreak
								nSegCtr = nSegCtr + 1
** now write out the saved inner and pack info that was uploaded from the 940, if we have the data........
								alength = ALINES(apt,outdet.printstuff,.T.,CHR(13))
								cPalPack  = ALLTRIM(segmentget(@apt,"W20_1",alength))
								cPalInner = ALLTRIM(segmentget(@apt,"W20_2",alength))
								cOrigWt = ALLTRIM(segmentget(@apt,"ORIG_WT",alength))
								cUOW = IIF(EMPTY(cOrigWt),"",ALLTRIM(segmentget(@apt,"ORIG_UOW",alength)))
								cOrigVol = ALLTRIM(segmentget(@apt,"ORIG_VOL",alength))
								cUOV = IIF(EMPTY(cOrigVol),"",ALLTRIM(segmentget(@apt,"ORIG_UOV",alength)))
*            STORE "PAL"+cfd+cfd+cfd+cfd+ALLTRIM(cPalPack)+cfd+cfd+cfd+cfd+cfd+cfd+cfd+cOrigWt+cfd+cUOW+cfd+cOrigVol+cfd+cUOV+cfd+cfd+ALLTRIM(cPalInner)+csegd TO cString
*            DO cstringbreak
*            nSegCtr = nSegCtr + 1
							ELSE
								WAIT CLEAR
								WAIT WINDOW "Empty UCC Number in vsynclairepp "+cShip_ref &cTimeMsg
								IF lTesting
									ASSERT .F. MESSAGE "EMPTY UCC...DEBUG"
									CLOSE DATABASES ALL
									=FCLOSE(nFilenum)
									ERASE &cFilename
									THROW
								ELSE
									cErrMsg = "EMPTY UCC# in "+cShip_ref
									DO ediupdate WITH cErrMsg,.T.
									=FCLOSE(nFilenum)
									ERASE &cFilename
									THROW
								ENDIF
							ENDIF
						ENDIF

						alength = ALINES(aptdet,outdet.printstuff,.T.,CHR(13))
						lEndAtSubqty = IIF(!("SIZE"$outdet.printstuff),.T.,.F.)
						IF lTesting
							WAIT WINDOW "Length of APTDET: "+ALLTRIM(STR(alength)) NOWAIT
							WAIT CLEAR
						ENDIF
						cUPCCode = segmentget(@aptdet,"UPCCODE",alength)
						cColor = TRIM(outdet.COLOR)
						STORE ALLTRIM(outdet.ID) TO cSize
						cStyle = TRIM(outdet.STYLE)
						cUPC = IIF(lTesting,TRIM(vsynclairepp.upc),TRIM(outdet.upc))
						cGTIN = ""
						IF LEN(ALLTRIM(cUPC)) # 12
							IF cUPCCode = "UK"
								cGTIN = TRIM(outdet.upc)
							ENDIF
							cUPC = ""
						ENDIF
						cStyle = ALLTRIM(outdet.STYLE)

						cItemNum = segmentget(@aptdet,"CUSTSKU",alength)
						IF EMPTY(ALLT(cItemNum))
							cItemNum = ALLTRIM(outdet.custsku)
						ENDIF
						IF EMPTY(ALLTRIM(cItemNum))
							cItemNum = "UNK"
						ENDIF

						nDetQty = vsynclairepp.totqty
						nOrigQty = vsynclairepp.qty

						IF EMPTY(cItemNum) AND !lSplitPT
							SET STEP ON
							cErrMsg = "MISS ItNum, Style "+cStyle
							DO ediupdate WITH cErrMsg,.T.
							=FCLOSE(nFilenum)
							ERASE &cFilename
							THROW
						ENDIF

						cUOM = segmentget(@aptdet,"UOM",alength)
						IF EMPTY(cUOM)
							cUOM = segmentget(@aptdet,"UNITCODE",alength)
						ENDIF
						lPrepack = IIF(cUOM = "EA",.F.,.T.)

						IF lTesting AND EMPTY(outdet.ctnwt)
							cCtnWt = IIF(lPrepack,"5","2")
							nTotCtnWt = nTotCtnWt + INT(VAL(cCtnWt))
						ELSE
							STORE ALLTRIM(STR(outdet.ctnwt)) TO cCtnWt
							nTotCtnWt = nTotCtnWt + outdet.ctnwt

							IF EMPTY(cCtnWt) OR outdet.ctnwt=0
								nCtnWt = outship.weight/outship.ctnqty
								cCtnWt = ALLTRIM(STR(nCtnWt))
								nTotCtnWt = nTotCtnWt + nCtnWt
								IF EMPTY(cCtnWt)
									cErrMsg = "MISS CTNWT "+TRANSFORM(vsynclairepp.outdetid)
									DO ediupdate WITH cErrMsg,.T.
									=FCLOSE(nFilenum)
									ERASE &cFilename
									THROW
								ENDIF
							ENDIF
						ENDIF

						IF nOrigQty=nDetQty
							nShipType = "CL"
						ELSE
							nShipType = "PR"
						ENDIF
						IF nDetQty>0
							nUnitSum = nUnitSum + nDetQty
							cUPC = IIF(lGap,"",cUPC)
							nOriqqty = INT(VAL(segmentget(@aptdet,"ORIGQTY",alength)))
							IF lPrepack AND ("SUBUPC"$outdet.printstuff OR "FULLDET"$outdet.printstuff)
								STORE "W12"+cfd+nShipType+cfd+ALLTRIM(STR(nOrigQty))+cfd+ALLTRIM(STR(nDetQty))+cfd+;
									ALLTRIM(STR(nOrigQty-nDetQty))+cfd+cUOM+cfd+cUPC+cfd+cUOM+cfd+cUPC+REPLICATE(cfd,2)+cCtnWt+cfd+"G"+cfd+cWeightUnit+csegd TO cString  && Eaches/Style
								DO cstringbreak
								nSegCtr = nSegCtr + 1
								cDesc = segmentget(@aptdet,"DESC",alength)
								IF !EMPTY(cDesc)
									STORE "G69"+cfd+cDesc+csegd TO cString
									DO cstringbreak
									nSegCtr = nSegCtr + 1
								ENDIF

								lenarydet = ALEN(aptdet,1)
								IF "FULLDET"$outdet.printstuff
									FOR irq = 1 TO lenarydet
										arystr = SUBSTR(UPPER(ALLTRIM(aptdet[irq])),1)
										IF "FULLDET"$arystr
											cSubCmt = ALLTRIM(SUBSTR(ALLTRIM(arystr),AT("*",arystr,1)+1))
											IF RIGHT(ALLTRIM(cSubCmt),4) = "****"
												cSubCmt = LEFT(cSubCmt,AT("*",cSubCmt,4)-1)
											ENDIF
											STORE cSubCmt+csegd TO cString
											DO cstringbreak
											nSegCtr = nSegCtr + 1
											RELEASE cSubCmt
										ELSE
											LOOP
										ENDIF
									ENDFOR
								ELSE
									CREATE CURSOR tempdetail (STYLE c(20),COLOR c(20),SIZE c(15),subqty i)
									FOR irq = 1 TO lenarydet
										arystr = SUBSTR(UPPER(ALLTRIM(aptdet[irq])),1)
										DO CASE
											CASE EMPTY(arystr)
												LOOP
											CASE LEFT(arystr,6)= "SUBUPC"
												cSubUPC = SUBSTR(ALLTRIM(arystr),AT("*",arystr)+1)
											CASE LEFT(arystr,6) = "SUBQTY"  && Write to cString at this point
												cSubqty = SUBSTR(ALLTRIM(arystr),AT("*",arystr)+1)
												IF lEndAtSubqty
													STORE "N9"+cfd+"UP"+cfd+cSubUPC+cfd+cSubqty+csegd TO cString
													DO cstringbreak
													nSegCtr = nSegCtr + 1
													LOOP
												ENDIF
											CASE LEFT(arystr,5) = "STYLE"
												cStyle = SUBSTR(ALLTRIM(arystr),AT("*",arystr)+1)
											CASE LEFT(arystr,5)= "COLOR"
												cColor = SUBSTR(ALLTRIM(arystr),AT("*",arystr)+1)
											CASE LEFT(arystr,4) = "SIZE"
												cSize = SUBSTR(ALLTRIM(arystr),AT("*",arystr)+1)
												IF !lEndAtSubqty
													STORE "N9"+cfd+"UP"+cfd+cSubUPC+cfd+cSubqty+IIF(!EMPTY(cStyle),REPLICATE(cfd,4)+"IN>"+cStyle+">CL>"+cColor+">SZ>"+cSize+csegd,csegd) TO cString
													DO cstringbreak
													nSegCtr = nSegCtr + 1
												ENDIF
										ENDCASE
									ENDFOR
								ENDIF
							ELSE
								cPKPack = ""
								cPKPack = segmentget(@aptdet,"PKPACK",alength)

								cStyle = segmentget(@aptdet,"ORIGSTYLE",alength)
								IF cUPCCode = "UK" AND EMPTY(cUPC) AND !EMPTY(cGTIN) && GTIN data
									STORE "W12"+cfd+nShipType+cfd+ALLTRIM(STR(nOrigQty))+cfd+ALLTRIM(STR(nDetQty))+cfd+;
										ALLTRIM(STR(nOrigQty-nDetQty))+cfd+cUOM+cfd+cUPC+cfd+IIF(!EMPTY(cStyle),"VN","")+cfd+TRIM(cStyle)+;
										cfd+cfd+cCtnWt+cfd+"G"+cfd+cWeightUnit+REPLICATE(cfd,5)+IIF(!EMPTY(cItemNum),"IN","")+cfd+ALLTRIM(cItemNum)+cfd+cfd+cfd+cUPCCode+cfd+cGTIN+csegd TO cString  && Eaches/Style
								ELSE
									STORE "W12"+cfd+nShipType+cfd+ALLTRIM(STR(nOrigQty))+cfd+ALLTRIM(STR(nDetQty))+cfd+;
										ALLTRIM(STR(nOrigQty-nDetQty))+cfd+cUOM+cfd+cUPC+cfd+IIF(!EMPTY(cStyle),"VN","")+cfd+TRIM(cStyle)+;
										cfd+IIF(!EMPTY(cPKPack),ALLTRIM(outdet.PACK),"")+cfd+cCtnWt+cfd+"G"+cfd+cWeightUnit+REPLICATE(cfd,5)+IIF(!EMPTY(cItemNum),"IN","")+cfd+ALLTRIM(cItemNum)+csegd TO cString  && Eaches/Style
								ENDIF
								DO cstringbreak
								nSegCtr = nSegCtr + 1
							ENDIF

							cDesc = segmentget(@aptdet,"DESC",alength)
							IF !EMPTY(cDesc)
								STORE "G69"+cfd+cDesc+csegd TO cString
								DO cstringbreak
								nSegCtr = nSegCtr + 1
							ENDIF

							cColor = segmentget(@aptdet,"ORIGCOLOR",alength)
							IF !EMPTY(cColor)
								STORE "N9"+cfd+"CL"+cfd+cColor+csegd TO cString
								DO cstringbreak
								nSegCtr = nSegCtr + 1
							ENDIF

							IF !EMPTY(cItemNum)
								STORE "N9"+cfd+"IN"+cfd+cItemNum+csegd TO cString
								DO cstringbreak
								nSegCtr = nSegCtr + 1
							ENDIF

							cSize = segmentget(@aptdet,"ORIGSIZE",alength)
							IF !EMPTY(cSize)
								STORE "N9"+cfd+"SZ"+cfd+cSize+csegd TO cString
								DO cstringbreak
								nSegCtr = nSegCtr + 1
							ENDIF

							cBuyerNum = segmentget(@aptdet,"BUYERSIZE",alength)
							IF !EMPTY(cBuyerNum)
								STORE "N9"+cfd+"IZ"+cfd+cBuyerNum+csegd TO cString
								DO cstringbreak
								nSegCtr = nSegCtr + 1
							ENDIF

							IF cUOM = "AS"
								cInnerpack =  ""
								cInnerpack = segmentget(@aptdet,"INNERPACK",alength)
								IF !EMPTY(cInnerpack)
									STORE "QTY"+cfd+"38"+cfd+cInnerpack+cfd+cUOM+csegd TO cString
									DO cstringbreak
									nSegCtr = nSegCtr + 1
								ENDIF
							ENDIF
						ENDIF

						SKIP 1 IN vsynclairepp
					ENDDO
					lSkipBack = .T.
					nCtnNumber = nCtnNumber + 1
				ENDDO

*!*          IF lNewPT
*!*            IF !lDoFirstRound
*!*              LOOP
*!*            ELSE
*!*              lDoFirstRound = .F.
*!*              IF !lSplitPT
*!*                EndSEloop()
*!*              ENDIF
*!*            ENDIF
*!*          ELSE
*!*            EndSEloop()
*!*          ENDIF

*************************************************************************
*2^  END DETAIL MATCHING SHIP_REF LOOP
*************************************************************************
				SELECT outship
				WAIT CLEAR
				WAIT WINDOW "Now creating Section Closure information" NOWAIT NOCLEAR
				nCuFT = CEILING(nCuFT)
				STORE "W03"+cfd+ALLTRIM(STR(nUnitSum))+cfd+ALLTRIM(STR(nTotCtnWt))+cfd+;
					cWeightUnit2+cfd+ALLTRIM(STR(nCuFT,10,0))+cfd+cCubeUnit+csegd TO cString   && Units sum, Weight sum, carton count

				nTotCtnWt = 0
				nTotCtnCount = 0
				nUnitSum = 0
				FPUTS(nFilenum,cString)
				STORE  "SE"+cfd+ALLTRIM(STR(nSegCtr+2))+cfd+PADL(c_GrpCntrlNum,9,"0")+csegd TO cString
				FPUTS(nFilenum,cString)

				SELECT outship
				WAIT CLEAR
				SET DELETED ON
				IF tx => 2 AND nRepLoops = 2
					GO nRec
				ENDIF
*        ASSERT .F. MESSA "At end of outship loop"
			ENDSCAN
		ENDSCAN

*************************************************************************
*1^  END OUTSHIP MAIN LOOP
*************************************************************************

		DO close945
		=FCLOSE(nFilenum)
		SELECT tempack
		IF lTesting
			LOCATE
*      BROW
		ENDIF
		USE IN tempack

		IF !lTesting
			SELECT edi_trigger
			LOCATE

			IF !USED("ftpedilog")
				SELECT 0
				USE F:\edirouting\ftpedilog ALIAS ftpedilog
				INSERT INTO ftpedilog (TRANSFER,ftpdate,filename,acct_name,TYPE) VALUES ("945-"+ccustname+"-"+cCustLoc,dt2,cFilenameShort,UPPER(ccustname),"945")
				USE IN ftpedilog
			ENDIF
		ENDIF

		WAIT CLEAR
		WAIT WINDOW cCustFolder+" 945 Process complete for subBOL "+cBOL TIMEOUT 1

*!* Create eMail confirmation message

		RELEASE ALL LIKE c_CntrlNum,c_GrpCntrlNum
		WAIT CLEAR

		IF !EMPTY(cRolledRecs)
			tsubject = cMailName+" 945 "+IIF(lTesting,"TEST ","")+"AMT EDI File from Toll as of "+dtmail+" ("+cCustLocMail+")"
			tmessage = "The following PT(s) from BOL# "+cBOL+" had style rollups in the Cartons data"+CHR(13)+CHR(13)+cRolledRecs
			IF lEmail
				DO FORM m:\dev\frm\dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
			ENDIF
		ENDIF


*!* Transfers files to correct output folders
		IF !lTesting AND lAckdata
			SELECT temp945
			COPY TO "f:\3pl\data\temp945syn.dbf"
			USE IN temp945
			SELECT 0
			USE "f:\3pl\data\temp945syn.dbf" ALIAS temp945syn
			SELECT 0
			USE "f:\3pl\data\pts_sent945.dbf" ALIAS pts_sent945
			APPEND FROM "f:\3pl\data\temp945syn.dbf"
			USE IN pts_sent945
			USE IN temp945syn
			DELETE FILE "f:\3pl\data\temp945syn.dbf"
		ENDIF
		cPTCount = TRANSFORM(nPTCount)
		cTotSubs = TRANSFORM(nTotSubs)
		cSubCnt = TRANSFORM(nSubCnt)

		INSERT INTO tempinfo (subbol,ptcount,ptstring) VALUES (cBOL,cPTCount,cPTString)
		WAIT CLEAR
		WAIT WINDOW cMailName+" 945 EDI File output complete for"+CHR(13)+"Sub-BOL: "+cBOL NOWAIT
		cWO_NumList = ""

		cPTString = ""
		nPTCount = 0
*    ASSERT .F. MESSAGE "At end of BL scan loop"
		lDoISAHeader = .T.
	ENDSCAN
*  asn_out_data()

	IF lDoSynFilesOut AND !lTesting AND !lTestinput
		CD &cHoldFolder
		TRY
			COPY FILE (cHoldFolder+"*.*") TO (cOutFolder+"*.*")
			COPY FILE (cHoldFolder+"*.*") TO (cArchFolder+"*.*")
			DELETE FILE (cHoldFolder+"*.*")
		CATCH TO oErr
*!* Added this xsqlexec statement to delete MBOL records if the process bombs out before completion
			lcQuery = [delete from edioutlog where accountid = ]+TRANSFORM(nAcctNum)+[ and bol = ']+cMBOL+[']
*      xsqlexec(lcQuery,,,"stuff")
			IF oErr.ERRORNO =  1102  && Can't create file
				DELETE FILE [&cFilenameHold]
				tsendto = tsendtoerr
				tcc = tccerr
				tsubject = "945 File Copy Error at "+TTOC(DATETIME())
				tattach  = ""
				tfrom    ="Toll EDI 945 Poller Operations <toll-edi-ops@tollgroup.com>"
				tmessage = ALLTRIM(oErr.MESSAGE)
				DO FORM m:\dev\frm\dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
				THROW
			ENDIF
		ENDTRY
	ENDIF

	WAIT WINDOW "" TIMEOUT 2
	cCustLocMail = cCustLoc
	tsubject = cMailName+" 945 "+IIF(lTesting,"TEST ","")+"EDI MBOL File from Toll as of "+dtmail+" ("+cCustLocMail+")"
	tattach = " "
	tmessage = "Filename: "+TRIM(cFilenameShort)+CHR(13)
	tmessage = tmessage+"Office: "+cCustLoc+CHR(13)
	tmessage = tmessage+"945 EDI Info from Toll, for Synclaire, Master BOL# "+ALLTRIM(cMBOL)
	tmessage = tmessage+" (Within our WO: "+cWO_NumStr+"), "+CHR(13)
	tmessage = tmessage + "for the following "+cTotSubs+" Sub-BOLs/PTs:"+CHR(13)+CHR(13)
	tmessage = tmessage + PADR("SUB-BOL",22)+"PT COUNT"+CHR(13)
	SELECT tempinfo
	IF lTesting
		COPY TO c:\tempfox\tempinfo
	ENDIF
	SCAN
		tmessage = tmessage + PADR(ALLTRIM(tempinfo.subbol),22)+tempinfo.ptcount+CHR(13)
		tmessage = tmessage + ALLTRIM(tempinfo.ptstring)+CHR(13)+CHR(13)
	ENDSCAN
	tmessage = tmessage +CHR(13)+"has been created and will be transmitted ASAP."+CHR(13)+CHR(13)
	IF !EMPTY(cMissDel)
		tmessage = tmessage+CHR(13)+CHR(13)+cMissDel+CHR(13)+CHR(13)
	ENDIF

	tmessage = tmessage + "If you have any questions, please eMail or call 732-750-9000 x217."
	IF lEmail
		DO FORM m:\dev\frm\dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
	ENDIF

	SELECT zeroqty
	IF RECCOUNT()>0
		tsendto = tsendtozero
		tcc = tcczero
		tsubject = cMailName+" Zero Qty PTs in MBOL "+cMBOL
		tmessage = "The following picktickets on MBOL# "+cMBOL+", BOL# "+cBOL+" show zero quantity in the outbound table:"+CHR(13)
		tmessage = PADR("CONSIGNEE",42)+PADR("PT#",22)+"PO#"+CHR(13)
		SCAN
			tmessage = tmessage+CHR(13)+PADR(ALLT(zeroqty.consignee),42)+PADR(ALLT(zeroqty.ship_ref),22)+ALLT(zeroqty.cnee_ref)
		ENDSCAN
		IF lEmail
			DO FORM m:\dev\frm\dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
		ENDIF
	ENDIF

	IF USED('zeroqty')
		USE IN zeroqty
	ENDIF
	cString = ""

	SELECT edi_trigger
	DO ediupdate WITH "945 CREATED",.F.
	lDoCatch = .F.

CATCH TO oErr
	IF lDoCatch
		WAIT WINDOW "In catch...debug" TIMEOUT 2
		SET STEP ON
*!* Added this xsqlexec statement to delete MBOL records if the process bombs out before completion
		lcQuery = [delete from edioutlog where accountid = ]+TRANSFORM(nAcctNum)+[ and mbol = ']+cMBOL+[']
*    xsqlexec(lcQuery,,,"stuff")
		cSpecialMsg=""
		DO CASE
			CASE BETWEEN(oErr.LINENO,1035,1046)
				cSpecialMsg="SHIPFOR CSZ error"
			CASE BETWEEN(oErr.LINENO,1006,1008)
				cSpecialMsg="SHIPTO CSZ error"
			CASE cRetMsg # "OK"
				cSpecialMsg = "SQL MSG: "+cRetMsg
		ENDCASE
		IF !EMPTY(cStatus) AND !EMPTY(cSpecialMsg)
			DO ediupdate WITH IIF(EMPTY(cSpecialMsg),UPPER(ALLTRIM(oErr.MESSAGE)),UPPER(cSpecialMsg)),.T.
			tsendto = tsendtoerr
			tcc = tccerr
			tsubject = ccustname+" Error ("+TRANSFORM(oErr.ERRORNO)+") at "+TTOC(DATETIME())
			tattach  = ""

			tmessage = ccustname+" Error processing "+CHR(13)

			tmessage =tmessage+ "Try/CATCH Exeception Message:"+CHR(13)+;
				[  Error: ] + STR(oErr.ERRORNO) +CHR(13)+;
				[  LineNo: ] + STR(oErr.LINENO) +CHR(13)+;
				[  Message: ] + oErr.MESSAGE +CHR(13)+;
				[  Procedure: ] + oErr.PROCEDURE +CHR(13)+;
				[  Details: ] + oErr.DETAILS +CHR(13)+;
				[  StackLevel: ] + STR(oErr.STACKLEVEL) +CHR(13)+;
				[  LineContents: ] + oErr.LINECONTENTS+CHR(13)+;
				[  UserValue: ] + oErr.USERVALUE

			tsubject = "945 EDI Poller Error at "+TTOC(DATETIME())
			tattach  = ""
			tfrom    ="Toll EDI 945 Poller Operations <toll-edi-ops@tollgroup.com>"
			DO FORM m:\dev\frm\dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
		ENDIF
	ENDIF
FINALLY
	WAIT WINDOW "Entire Synclaire MBOL process complete for"+CHR(13)+"MBOL: "+cMBOL TIMEOUT 2
	SET DELETED ON
	ON ERROR
	IF lTesting
		CLOSE DATABASES ALL
	ENDIF
ENDTRY

*** END OF CODE BODY

****************************
PROCEDURE close945
****************************
** Footer Creation

	STORE  "GE"+cfd+ALLTRIM(STR(nSTCount))+cfd+c_CntrlNum+csegd TO cString
	FPUTS(nFilenum,cString)
	nSTCount = 0

	STORE  "IEA"+cfd+"1"+cfd+PADL(c_CntrlNum,9,"0")+csegd TO cString
	FPUTS(nFilenum,cString)

ENDPROC

****************************
PROCEDURE num_incr_isa
****************************

	IF !USED("serfile")
		USE ("f:\3pl\data\serial\Synclaire945mbol_serial") IN 0 ALIAS serfile
	ENDIF
	SELECT serfile
	IF lTesting AND lISAFlag
		nOrigSeq = serfile.seqnum
		lISAFlag = .F.
	ENDIF
	nISA_Num = serfile.seqnum
	c_CntrlNum = ALLTRIM(STR(serfile.seqnum))
	REPLACE serfile.seqnum WITH serfile.seqnum + 1 IN serfile
	SELECT outship
ENDPROC

****************************
PROCEDURE num_incr_st
****************************

	IF !USED("serfile")
		USE ("f:\3pl\data\serial\"+ccustname+""945_serial") IN 0 ALIAS serfile
	ENDIF
	SELECT serfile
	IF lTesting AND lSTFlag
		nOrigGrpSeq = serfile.grpseqnum
		lSTFlag = .F.
	ENDIF
	c_GrpCntrlNum = ALLTRIM(STR(serfile.grpseqnum))
	REPLACE serfile.grpseqnum WITH serfile.grpseqnum + 1 IN serfile
	SELECT outship
ENDPROC


****************************
PROCEDURE segmentget
****************************

	PARAMETER thisarray,lcKey,nLength

	FOR i = 1 TO nLength
		IF i > nLength
			EXIT
		ENDIF
		lnEnd= AT("*",thisarray[i])
		IF lnEnd > 0
			lcThisKey =TRIM(SUBSTR(thisarray[i],1,lnEnd-1))
			IF OCCURS(lcKey,lcThisKey)>0
				RETURN SUBSTR(thisarray[i],lnEnd+1)
				i = 1
			ENDIF
		ENDIF
	ENDFOR

	RETURN ""

****************************
PROCEDURE ediupdate
****************************
	PARAMETER cStatus,lIsError
	lDoCatch = IIF(!EMPTY(cRetMsg) AND !EMPTY(cStatus),.T.,.F.)
*SET STEP ON
	ASSERT .F. MESSAGE "In EDIUpdate process"
	IF !lTesting
		SELECT edi_trigger
		IF !lIsError
			REPLACE edi_trigger.processed WITH .T.,edi_trigger.proc945 WITH .T.,edi_trigger.file945 WITH UPPER(cFilenameArch),;
				edi_trigger.masterbill WITH .T.,edi_trigger.fin_status WITH "945 CREATED",edi_trigger.errorflag WITH .F.,;
				edi_trigger.when_proc WITH DATETIME() ;
				FOR edi_trigger.bol = cMBOL AND !edi_trigger.processed
		ELSE
			REPLACE edi_trigger.processed WITH .T.,edi_trigger.proc945 WITH .F.,edi_trigger.file945 WITH "",;
				edi_trigger.masterbill WITH .T.,edi_trigger.fin_status WITH cStatus,edi_trigger.errorflag WITH .T. ;
				FOR edi_trigger.bol = cMBOL AND !edi_trigger.processed
			IF lCloseOutput
				=FCLOSE(nFilenum)
			ENDIF
		ENDIF
	ENDIF

*!*  "This section runs the file945 renaming in edi_trigger (using current BOL#) in the triggers is the MBOL#"
	IF !lTesting
*SET STEP ON
		SELECT mfilesout945
		LOCATE
		IF !EOF() AND !lIsError
			WAIT WINDOW "Now updating actual 945 file names for MBOL "+cMBOL TIMEOUT 1
			ASSERT .F. MESSAGE "In File Name update for MBOL"
			SELECT edi_trigger
			LOCATE
			SCAN FOR edi_trigger.bol = cMBOL
				cxEDIShip_ref = ALLTRIM(edi_trigger.ship_ref)
				SELECT mfilesout945
				IF SEEK(cxEDIShip_ref,'mfilesout945','ship_ref')
					cxEDIFilename = ALLTRIM(mfilesout945.filename)
					cxEDIISA_Num = ALLTRIM(mfilesout945.isa_num)
				ENDIF
				REPLACE edi_trigger.file945 WITH cxEDIFilename,edi_trigger.isa_num WITH cxEDIISA_Num NEXT 1 IN edi_trigger
			ENDSCAN
			USE IN mfilesout945
			RELEASE ALL LIKE cxEDI*
		ENDIF
	ENDIF

	IF lIsError AND lEmail
		tsubject = "945 Error in SYNCLAIRE BOL "+TRIM(cBOL)+"(At PT "+cShip_ref+")"
		tattach = " "
		IF !lTesting AND !lTestinput
			tsendto = tsendtoerr
			tcc = tccerr
		ENDIF
		tmessage = "945 Processing for"+CHR(13)
		tmessage = tmessage+CHR(13)+"MBOL# "+cMBOL+", sub-BOL# "+cBOL+", WO# "+ALLTRIM(STR(outship.wo_num))+" produced this error: (Office: "+cOffice+"): "+;
			ALLTRIM(cStatus)+CHR(13)+"Check EDI_TRIGGER and re-run"
		IF "TOTQTY ERR"$cStatus
			tmessage = tmessage + CHR(13) + "At OUTSHIPID: "+ALLTRIM(STR(m.outshipid))
		ENDIF

		DO FORM m:\dev\frm\dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
	ENDIF
	closedata()
ENDPROC

****************************
PROCEDURE cstringbreak
****************************
	cLen = LEN(ALLTRIM(cString))
	FPUTS(nFilenum,cString)
ENDPROC

****************************
PROCEDURE closedata
****************************
	IF USED('outship')
		USE IN outship
	ENDIF
	IF USED('outdet')
		USE IN outdet
	ENDIF
	IF USED('outwolog')
		USE IN outwolog
	ENDIF
	IF USED('vsynclairepp')
		USE IN vsynclairepp
	ENDIF
	IF USED('scacs')
		USE IN scacs
	ENDIF
	IF USED('mm')
		USE IN mm
	ENDIF
	IF USED('tempx')
		USE IN tempx
	ENDIF
	IF USED('ackdata')
		USE IN ackdata
	ENDIF
	IF USED('bl')
		USE IN bl
	ENDIF
	IF USED('bldet')
		USE IN bldet
	ENDIF
	IF !lTesting
		SELECT edi_trigger
		LOCATE
	ENDIF
	IF USED('serfile')
		SELECT serfile
		IF lTesting
			REPLACE serfile.seqnum WITH nOrigSeq
			REPLACE serfile.grpseqnum WITH nOrigGrpSeq
		ENDIF
		USE IN serfile
	ENDIF
ENDPROC

****************************
PROCEDURE renewdatetime
****************************
	IF INLIST(cOffice,'L','C','E','1','2','7')
		dDateTimeCal = (DATETIME()-(3*3600))
	ELSE
		dDateTimeCal = DATETIME()
	ENDIF
	dt1 = TTOC(dDateTimeCal,1)
	dtmail = TTOC(dDateTimeCal)
	dt2 = dDateTimeCal
	cDate = DTOS(DATE())
	m.loaddt  = DATE()
	m.loadtime = dt2
	cTruncDate = RIGHT(cDate,6)
	cTruncTime = SUBSTR(dt1,9,4)
ENDPROC

****************************
PROCEDURE EndSEloop
****************************
	WAIT CLEAR
	WAIT WINDOW "Now creating Section Closure information" NOWAIT NOCLEAR
	IF lToys
		STORE "W03"+cfd+ALLTRIM(STR(nUnitSum))+cfd+ALLTRIM(STR(nTotCtnWt))+cfd+;
			"LB"+cfd+cCuFt+cfd+"CF"+csegd TO cString   && Units sum, Weight sum, Volume sum, carton count
	ELSE
		STORE "W03"+cfd+ALLTRIM(STR(nUnitSum))+cfd+ALLTRIM(STR(nTotCtnWt))+cfd+;
			"LB"+csegd TO cString   && Units sum, Weight sum, carton count
	ENDIF
	FPUTS(nFilenum,cString)

	STORE  "SE"+cfd+ALLTRIM(STR(nSegCtr+2))+cfd+PADL(c_GrpCntrlNum,9,"0")+csegd TO cString
	FPUTS(nFilenum,cString)
	nTotCtnWt = 0
	nTotCtnCount = 0
	nUnitSum = 0
ENDPROC
