#DEFINE CRLF CHR(13) + CHR(10)
LOCAL lcCentury, lnYearsInService, lnTotOil, lnTotParts, lnTotTires, lcVehYear, lcVEHNO, lcTABLE, lcStartingVehno
LOCAL lnStartingVehno, lnEndingVehno, lcVEHNOWHERE, lnTotMileage
LOCAL ARRAY laOrdhdr(1)

lcCentury = SET('CENTURY')

SET CENTURY ON
SET TALK OFF
SET CONSOLE OFF

* these will be used in SELECTS for trucks & trailers
lnStartingVehno = 0
lnEndingVehno = 999999

CLOSE DATABASES ALL

USE F:\SHOP\SHDATA\ordhdr IN 0
USE F:\SHOP\SHDATA\repdetl IN 0
USE F:\SHOP\SHDATA\tiredetl IN 0
USE F:\SHOP\SHDATA\oildetl IN 0

STORE 0 TO lnTotOil, lnTotParts, lnTotTires

IF USED('temp') THEN
	USE IN temp
ENDIF
IF USED('temp2') THEN
	USE IN temp2
ENDIF

* create temp2 cursor which drives report
SELECT * FROM ordhdr WHERE .F. INTO CURSOR temp2 READWRITE

ALTER TABLE temp2 ADD COLUMN VIN C(25)
ALTER TABLE temp2 ADD COLUMN PLATE C(10)
ALTER TABLE temp2 ADD COLUMN start_mile N(10)
ALTER TABLE temp2 ADD COLUMN end_mile   N(10)
ALTER TABLE temp2 ADD COLUMN totmileage N(10)
ALTER TABLE temp2 ADD COLUMN st_date    d
ALTER TABLE temp2 ADD COLUMN end_date   d
ALTER TABLE temp2 ADD COLUMN service    N(4,2)
ALTER TABLE temp2 ADD COLUMN oilsvc     N(4)
ALTER TABLE temp2 ADD COLUMN repairsvc  N(4)
ALTER TABLE temp2 ADD COLUMN tiresvc    N(4)
ALTER TABLE temp2 ADD COLUMN pmsvc      N(4)
ALTER TABLE temp2 ADD COLUMN wsvc       N(4)
ALTER TABLE temp2 ADD COLUMN washsvc    N(4)
ALTER TABLE temp2 ADD COLUMN vehtype    C(8)
ALTER TABLE temp2 ADD COLUMN vehyear    C(4)

INDEX ON VEHNO TAG VEHNO
INDEX ON MAINTTYPE TAG MAINTTYPE

USE F:\SHOP\SHDATA\TRUCKS IN 0 ALIAS TRUCKS

************************************************************
************************************************************
* get cursor of vehicle #s to drive multi-vehicle report

WAIT WINDOW NOWAIT "Retrieving list of TRUCK #s...."

IF USED('CURVEHNORANGE') THEN
	USE IN CURVEHNORANGE
ENDIF

lcVEHNO = "PADR(ALLTRIM(TRUCK_NUM),6,' ')"  && needed because TRUCK_NUM is c(8) and all other vehno's are c(6)
lcTABLE = "TRUCKS"
* treat vehno's as numeric in Select
lcVEHNOWHERE = "VAL(TRUCK_NUM)"
lcStartingVehno = "lnStartingVehno"
lcEndingVehno = "lnEndingVehno"

SELECT DISTINCT &lcVEHNO AS TCVEHNO ;
	FROM &lcTABLE ;
	INTO CURSOR CURVEHNORANGE ;
	WHERE &lcVEHNOWHERE >= &lcStartingVehno ;
	AND &lcVEHNOWHERE <= &lcEndingVehno ;
	AND Active ;
	ORDER BY 1 ;
	READWRITE


************************************************************


IF (NOT USED('CURVEHNORANGE')) OR EOF('CURVEHNORANGE') THEN
	=MESSAGEBOX("No Work Orders were found for the range of vehicles.",0+16,"Truck Mileage Report")
	SET CENTURY &lcCentury
	USE IN temp2
	RETURN
ENDIF


SELECT CURVEHNORANGE
SCAN
	* determine vehicle year from master vehicle tables
	lcVehYear = "?"
	SELECT TRUCKS
	LOCATE FOR truck_num = CURVEHNORANGE.TCVEHNO
	m.VIN = ''
	m.PLATE = ''
	IF FOUND() THEN
		lcVehYear = TRUCKS.YEAR
		m.VIN = TRUCKS.VIN
		m.PLATE = TRUCKS.PLATE
	ENDIF

	IF SEEK(CURVEHNORANGE.TCVEHNO,"ordhdr","vehno")
		SELECT ordhdr
		SCAN FOR VEHNO = CURVEHNORANGE.TCVEHNO
			WAIT WINDOW "Processing TRUCK #: "+ALLTRIM(CURVEHNORANGE.TCVEHNO)+", Work Order: "+TRANS(ordno) NOWAIT

			SCATTER MEMVAR
			INSERT INTO temp2 FROM MEMVAR

*!*				SELECT oildetl
*!*				SUM (oilqty * unitprice) TO lnTotOil FOR ordno = ordhdr.ordno

*!*				SELECT repdetl
*!*				SUM (repqty * unitprice) TO lnTotParts FOR ordno = ordhdr.ordno

*!*				SELECT tiredetl
*!*				SUM (tireqty * unitprice) TO lnTotTires FOR ordno = ordhdr.ordno

*!*				REPLACE tirecost WITH lnTotTires, partcost WITH lnTotParts, oilcost WITH lnTotOil IN temp2
		ENDSCAN

		SELECT VEHNO, crdate, mileage FROM ordhdr WHERE VEHNO = CURVEHNORANGE.TCVEHNO ORDER BY crdate ;
			INTO CURSOR curorders READWRITE

		* finding most recent WO
		GOTO BOTTOM IN curorders
		REPLACE ALL end_mile WITH curorders.mileage, end_date WITH curorders.crdate FOR VEHNO = CURVEHNORANGE.TCVEHNO IN temp2

		* finding oldest WO
		GOTO TOP IN curorders

		SELECT temp2
		REPLACE ALL start_mile WITH curorders.mileage, st_date WITH curorders.crdate FOR VEHNO = CURVEHNORANGE.TCVEHNO
		SKIP -1
		lnYearsInService = (temp2.end_date - temp2.st_date)/365
		REPLACE ALL temp2.service WITH lnYearsInService FOR VEHNO = CURVEHNORANGE.TCVEHNO


*!*			SELECT temp2
*!*			COUNT FOR (MAINTTYPE = "P") AND (VEHNO = CURVEHNORANGE.TCVEHNO) TO pmService
*!*			COUNT FOR (MAINTTYPE = "O") AND (VEHNO = CURVEHNORANGE.TCVEHNO) TO oilService
*!*			COUNT FOR (MAINTTYPE = "R") AND (VEHNO = CURVEHNORANGE.TCVEHNO) TO repairService
*!*			COUNT FOR (MAINTTYPE = "T") AND (VEHNO = CURVEHNORANGE.TCVEHNO) TO tireService
*!*			STORE 0 TO wartyService
*!*			COUNT FOR (MAINTTYPE = "W") AND (VEHNO = CURVEHNORANGE.TCVEHNO) TO WashService

*!*			REPLACE ALL temp2.oilsvc WITH oilService, ;
*!*				temp2.repairsvc WITH repairService, ;
*!*				temp2.tiresvc   WITH tireService, ;
*!*				temp2.wsvc      WITH wartyService, ;
*!*				temp2.pmsvc     WITH pmService, ;
*!*				temp2.washsvc   WITH WashService, ;
*!*				temp2.vehyear   WITH lcVehYear ;
*!*				FOR VEHNO = CURVEHNORANGE.TCVEHNO

		REPLACE ALL temp2.vehyear WITH lcVehYear FOR VEHNO = CURVEHNORANGE.TCVEHNO

		USE IN curorders
	ENDIF  &&  found()
ENDSCAN  && of curvehnorange

SELECT temp2
REPLACE ALL vehtype WITH "TRUCK"
LOCATE

* added 8/22/07 MB
SELECT temp2
SCAN
	lnTotMileage = temp2.end_mile - temp2.start_mile
	IF lnTotMileage < 1 THEN
		lnTotMileage = 1
	ENDIF
	REPLACE temp2.totmileage WITH lnTotMileage
ENDSCAN

* order correctly
SELECT * FROM temp2 ORDER BY VEHNO, ordno INTO CURSOR temp

WAIT CLEAR

IF EOF('temp') THEN
	=MESSAGEBOX("No data was found for the range of vehicles.",0+16,"Truck Mileage Report")
ELSE
	KEYBOARD '{CTRL+F10}' CLEAR

	*!*		IF tcReportMode = "FULL"
	*!*			report form veh_hist_range_rpt preview noconsole
	*!*		else
	*!*			report form veh_hist_range_rpt_summary preview noconsole
	*!*		ENDIF

	USE IN temp
*!*		SELECT ;
*!*			VEHNO, ;
*!*			vehyear, ;
*!*			vehtype, ;
*!*			VIN, PLATE, ;
*!*			st_date, ;
*!*			end_date, ;
*!*			MAX(service) AS service, ;
*!*			MAX(mileage) AS mileage, ;
*!*			MAX(VEHHOURS) AS VEHHOURS, ;
*!*			MAX(start_mile) AS start_mile, ;
*!*			MAX(end_mile) AS end_mile, ;
*!*			MAX(totmileage) AS totmileage, ;
*!*			SUM(TOTLCOST) AS TOTAL_LABOR, ;
*!*			SUM(tirecost) AS TOTAL_TIRES, ;
*!*			SUM(oilcost) AS TOTAL_OIL, ;
*!*			SUM(partcost) AS TOTAL_PARTS, ;
*!*			SUM(OUTSCOST) AS TOTAL_OUTSCOST ;
*!*			FROM temp2 ;
*!*			INTO CURSOR temp ;
*!*			GROUP BY VIN, PLATE, VEHNO, vehyear, vehtype, st_date, end_date ;
*!*			ORDER BY VEHNO

	SELECT ;
		VEHNO, ;
		vehyear, ;
		vehtype, ;
		VIN, PLATE, ;
		MAX(mileage) AS mileage ;
		FROM temp2 ;
		INTO CURSOR temp ;
		GROUP BY VEHNO, vehyear, vehtype, VIN, PLATE ;
		ORDER BY VEHNO

	SELECT temp
	COPY TO C:\A\TRUCK_MILEAGE.XLS XL5
	*!*				SELECT TEMP
	*!*				REPORT FORM veh_hist_range_rpt_oneline PREVIEW NOCONSOLE

ENDIF  &&  eof('temp')

USE IN temp
USE IN temp2
USE IN CURVEHNORANGE

SET CENTURY &lcCentury
RETURN
