**This script identifies when a 945 has not been created for a PT.

utilsetup("STEELSERIES_MISSING_945")
set exclusive off
set deleted on
set talk off
set safety off

with _screen
	.autocenter = .t.
	.windowstate = 0
	.borderstyle = 1
	.width = 320
	.height = 210
	.top = 200
	.left = 100
	.closable = .t.
	.maxbutton = .t.
	.minbutton = .t.
	.scrollbars = 0
	.caption = "STEELSERIES_MISSING_945"
endwith

schedupdate()

goffice="S"	&& steelxyz

if !used("edi_trigger")
	use f:\3pl\data\edi_trigger shared in 0
endif

*!*	SELECT ACCOUNTID,WO_NUM,SHIP_REF FROM outship where INLIST(accountid,6532) AND  delenterdt >DATE()-20  and delenterdt<datetime()-1290;
*!*	 AND !INLIST(ship_ref,'0082389802','0082442242','0082587747','0082499503' ,'0082423603','0082367184','0082700041','0082632495','0082416752','0082967840','0083078040') into CURSOR temp945
*!*	SELECT DISTINCT ACCOUNTID,WO_NUM,SHIP_REF FROM EDI_TRIGGER WHERE EDI_TYPE='945 ' AND TRIG_TIME>date()-90 AND INLIST(accountid,6532);
*!*	 AND !INLIST(ship_ref,'0082389802','0082442242','0082587747','0082499503' ,'0082423603','0082367184','0082700041','0082632495','0082416752','0082967840','0083078040') INTO CURSOR TEMP21
*!*	SELECT * FROM temp945 t LEFT JOIN TEMP21 e ON t.accountid=e.accountid AND t.wo_num=e.wo_num AND t.ship_ref=e.ship_ref  INTO CURSOR temp22
*!*	SELECT * FROM TEMP22 WHERE ISNULL(ACCOUNTID_B) INTO CURSOR TEMP23
*!*	SELECT  TEMP23
SET STEP ON 
if usesqloutwo()
	xsqlexec("select accountid, wo_num, ship_ref from outship where mod='"+goffice+"' " + ;
		"and accountid=6612 and delenterdt>{"+dtoc(date()-20)+"} and cancelrts='F' and delenterdt<{"+ttoc(datetime()-3600)+"}","temp945",,"wh")
else
	select accountid,wo_num,ship_ref,consignee,del_date from outship where inlist(accountid,6612) and  delenterdt >date()-20  and delenterdt<datetime()-3600;
		and !empty(bol_no) AND !cancelrts into cursor temp945
endif
SET STEP ON 
select * from edi_trigger where inlist(accountid,6612) and edi_type='945'  into cursor t1 readwrite
select * from temp945 a left join t1  b on a.accountid=b.accountid and a.wo_num=b.wo_num and a.ship_ref=b.ship_ref into cursor c1 readwrite
select * from c1 where isnull(edi_type) into cursor c2 readwrite

if reccount() > 1
	export to "S:\steelseries\missing_945\steelseries_945_trigger_not_created"  type xls
	tsendto = "todd.margolin@tollgroup.com,paul.gaidis@tollgroup.com,Maria.Estrella@Tollgroup.com,joe.bianchi@tollgroup.com,juan.rocio@tollgroup.com,doug.wachs@tollgroup.com"
	tattach = "S:\steelseries\missing_945\steelseries_945_trigger_not_created.xls"
	tcc =""
	tfrom ="Toll/FMI EDI System Operations <fmi-transload-ops@fmiint.com>"
	tmessage = "steelseries_945_trigger_not_created"+ttoc(datetime())
	tsubject = "steelseries_945_trigger_not_created, CHECK POLLER"
	do form m:\dev\frm\dartmail2 with tsendto,tfrom,tsubject, tcc,tattach,tmessage  ,"A"
else
	tsendto = "todd.margolin@tollgroup.com"
	tattach = ""
	tcc =""
	tfrom ="Toll/FMI EDI System Operations <fmi-transload-ops@fmiint.com>"
	tmessage = "NO_MISSING_steelseries_945_triggers_exist"+ttoc(datetime())
	tsubject = "NO_MISSING_steelseries_945_triggers_exist"
	do form m:\dev\frm\dartmail2 with tsendto,tfrom,tsubject, tcc,tattach,tmessage  ,"A"
endif


select distinct accountid,wo_num,ship_ref,fin_status from t1 where edi_type='945'  and inlist(accountid,6612);
	and !errorflag and   (empty(fin_status) or empty(when_proc)) AND fin_status !='NO 945' into cursor temp21 READWRITE 
	DELETE FOR fin_status='945 CREATED' IN temp21
select * from temp21 where fin_status !='NO 945 ' into cursor temp22
select  temp22
if reccount() > 0
	export to "S:\steelseries\missing_945\steelseries_945_trigger_created_not_processed"  type xls

	tsendto = "todd.margolin@tollgroup.com,paul.gaidis@tollgroup.com,joe.bianchi@tollgroup.com,doug.wachs@tollgroup.com"
	tattach = "S:\steelseries\missing_945\steelseries_945_trigger_created_not_processed.xls"
	tcc =""
	tfrom ="Toll/FMI EDI System Operations <fmi-transload-ops@fmiint.com>"
	tmessage = "steelseries_945_trigger_created_not_processed"+ttoc(datetime())
	tsubject = "steelseries_945_trigger_created_not_processed"
	do form m:\dev\frm\dartmail2 with tsendto,tfrom,tsubject, tcc,tattach,tmessage  ,"A"
else
	tsendto = "tmarg@fmiint.com"
	tattach = ""
	tcc =""
	tfrom ="Toll/FMI EDI System Operations <fmi-transload-ops@fmiint.com>"
	tmessage = "NO_unprocessed_945s_for_steelseries"+ttoc(datetime())
	tsubject = "NO_unprocessed_945s_for_steelseries"
	do form m:\dev\frm\dartmail2 with tsendto,tfrom,tsubject, tcc,tattach,tmessage  ,"A"
endif



select * from t1 where edi_type='945 '  and inlist(accountid,6612);
	and errorflag into cursor temp21
select  temp21
if reccount() > 0
	export to "S:\steelseries\missing_945\steelseries_trigger_error"  type xls

	tsendto = "tmarg@fmiint.com,todd.margolin@tollgroup.com,paul.gaidis@tollgroup.com,joe.bianchi@tollgroup.com,doug.wachs@tollgroup.com"
	tattach = "S:\steelseries\missing_945\steelseries_trigger_error.xls"
	tcc =""
	tfrom ="Toll/FMI EDI System Operations <fmi-transload-ops@fmiint.com>"
	tmessage = "steelseries_945_trigger_error"+ttoc(datetime())
	tsubject = "steelseries_945_trigger_error"
	do form m:\dev\frm\dartmail2 with tsendto,tfrom,tsubject, tcc,tattach,tmessage  ,"A"
else
	tsendto = "tmarg@fmiint.com"
	tattach = ""
	tcc =""
	tfrom ="Toll/FMI EDI System Operations <fmi-transload-ops@fmiint.com>"
	tmessage = "NO_steelseries_trigger_errors"+ttoc(datetime())
	tsubject = "NO_steelseries_trigger_errors"
	do form m:\dev\frm\dartmail2 with tsendto,tfrom,tsubject, tcc,tattach,tmessage  ,"A"
endif

if usesqloutwo()
	xsqlexec("select wo_num, ship_ref, consignee, del_date from outship where mod='"+goffice+"' " + ;
		"and accountid=6612 and del_date>{"+dtoc(date()-120)+"} and empty(bol_no)","temp23",,"wh")
else
	select accountid,wo_num,ship_ref,consignee,del_date from outship where inlist(accountid,6612) and !empty(del_date) AND !cancelrts and empty(bol_no) and del_date>date()-120 ;
		into cursor temp23
endif
DELETE FOR ship_ref='TRUCK' AND ship_ref='CONTAINER'
select  temp23
if reccount() > 0
	export to "S:\steelseries\missing_945\steelseries_missing_bol"  type xls

	tsendto = "tmarg@fmiint.com,todd.margolin@tollgroup.com,paul.gaidis@tollgroup.com,joe.bianchi@tollgroup.com,doug.wachs@tollgroup.com"
	tattach = "S:\steelseries\missing_945\steelseries_missing_bol.xls"
	tcc =""
	tfrom ="Toll/FMI EDI System Operations <fmi-transload-ops@fmiint.com>"
	tmessage = "steelseries_missing_bol"+ttoc(datetime())
	tsubject = "steelseries_missing_bol"
	do form m:\dev\frm\dartmail2 with tsendto,tfrom,tsubject, tcc,tattach,tmessage  ,"A"
else
	tsendto = "tmarg@fmiint.com"
	tattach = ""
	tcc =""
	tfrom ="Toll/FMI EDI System Operations <fmi-transload-ops@fmiint.com>"
	tmessage = "NO_steelseries_missing_bol"+ttoc(datetime())
	tsubject = "NO_steelseries_missing_bol"
	do form m:\dev\frm\dartmail2 with tsendto,tfrom,tsubject, tcc,tattach,tmessage  ,"A"
endif
close data all

_screen.caption=gscreencaption
