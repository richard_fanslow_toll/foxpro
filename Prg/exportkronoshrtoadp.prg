* export department list from ADP into Foxpro (for use by revtime.scx)

*** exe = F:\UTIL\ADPEDI\exportkronoshrtoadp.exe

LOCAL loExportKronosHRToADP
*!*	IF NOT INLIST(UPPER(ALLTRIM(GETENV("COMPUTERNAME"))),"MBENNETT","HR1") THEN
*!*		=MESSAGEBOX("The Employee Export must be run from Lucille's PC!",0+16,"Export FoxPro Employees")
*!*		RETURN
*!*	ENDIF
loExportKronosHRToADP = CREATEOBJECT('ExportKronosHRToADP')
loExportKronosHRToADP.MAIN()
CLOSE DATABASES ALL
RETURN

#DEFINE LOGIT 1
#DEFINE WAITIT 2
#DEFINE NOWAITIT 4
#DEFINE SENDIT 8
#DEFINE CRLF CHR(13) + CHR(10)
#DEFINE RETURN_DATA_MANDATORY .F.
#DEFINE RETURN_DATA_NOT_MANDATORY .T.
#DEFINE CSVFORMAT 6
#DEFINE WRITE_ONLY_UNBUFFERED 11
#DEFINE COMMA ","

DEFINE CLASS ExportKronosHRToADP AS CUSTOM

	cProcessName = 'ExportKronosHRToADP'

	lTestMode = .F.  && .F. FOR NORMAL OPERATION, .T. TO SUPPRESS ALL BUT DEVELOPER EMAILS AND CHANGE INPUT TABLE, ETC.

	lAutoYield = .T.

	cStartTime = TTOC(DATETIME())

	dOneMonthAgo = GOMONTH(DATE(),-1)

	* connection properties
	nSQLHandle = 0

	* folder properties
	cInPath = 'F:\UTIL\ADPEDI\FROMKRONOSHR\'
	cADPPath = 'C:\ADP\PCPW\ADPDATA\'
	cArchiveSourceCSVPath = 'F:\UTIL\ADPEDI\FROMKRONOSHR\SOURCEARCHIVED\'
	cArchiveTargetCSVPath = 'F:\UTIL\ADPEDI\FROMKRONOSHR\TARGETARCHIVED\'
	cFileDate = STRTRAN(DTOC(DATE()),"/","_")

	* object ref properties
	oExcel = NULL

	* processing properties
	lOnlyOutputNewEmployees = .T.
	lProcessEmployeeChanges = .F.
	nNumberOfOutputFiles = 0
	cTopEmailText = ""
	lOutputAXAFile = .F.
	lOutputSXIFile = .F.
	lOutputZXUFile = .F.
	cNewHireList = ''

	* output file properties
	nCurrentOutputCSVFileHandle = -1
	cOutputCSVHeaderLine1 = 'CO CODE,FILE #,SOCIAL SECURITY NUMBER,EMPLOYEE LAST NAME,EMPLOYEE FIRST NAME,ADDRESS LINE 1,ADDRESS LINE 2,CITY,STATE POSTAL CODE,'
	*cOutputCSVHeaderLine2 = 'ZIP CODE,GENDER,HOME DEPARTMENT,RATE TYPE,RATE 1 AMOUNT,HIRE STATUS,HIRE DATE,BIRTH DATE,WORKED STATE TAX CODE,SUI/SDI TAX JURISDICTION CODE'
	cOutputCSVHeaderLine2 = 'ZIP CODE,GENDER,HOME DEPARTMENT,RATE TYPE,RATE 1 AMOUNT,HIRE STATUS,HIRE DATE,BIRTH DATE,WORKED STATE TAX CODE'

	* wait window properties
	nWaitWindowTimeout = 2
	lWaitWindowIsOn = .T.

	* logfile properties
	lLoggingIsOn = .T.
	cLogFile = 'F:\UTIL\ADPEDI\Logfiles\ExportKronosHRToADP_log.txt'

	* INTERNAL email properties
	lSendInternalEmailIsOn = .T.
	cFrom = 'FMI Corporate <fmicorporate@fmiint.com>'
	*cSendTo = 'Mark Bennett <mbennett@fmiint.com>'
	cSendTo = 'Lucille Waldrip <lwaldrip@fmiint.com>'
	cCC = 'Stephanie Kochanski <SKochanski@fmiint.com>, Mark Bennett <mbennett@fmiint.com>'
	cSubject = 'EXPORT KRONOS HR TO ADP, Results for: ' + TTOC(DATETIME())
	cAttach = ''
	cBodyText = ''

	cCOMPUTERNAME = ''
	cUSERNAME = ''

	FUNCTION INIT
		IF NOT DODEFAULT()
			RETURN .F.
		ENDIF
		WITH THIS
			*SET RESOURCE OFF
			CLOSE DATA
			SET CENTURY OFF
			SET DATE AMERICAN
			SET HOURS TO 24
			SET DECIMALS TO 2
			SET ANSI ON
			SET TALK OFF
			SET DELETED ON
			SET CONSOLE OFF
			SET EXCLUSIVE OFF
			SET SAFETY OFF
			SET EXACT ON
			SET STATUS BAR OFF
			SET SYSMENU OFF
			SET ENGINEBEHAVIOR 70
			_VFP.AUTOYIELD = .lAutoYield
			.cCOMPUTERNAME = GETENV("COMPUTERNAME")
			.cUSERNAME = GETENV("USERNAME")
			IF .lTestMode THEN
				.cSendTo = 'Mark Bennett <mbennett@fmiint.com>'
				.cCC = ''
				.cLogFile = 'F:\UTIL\ADPEDI\Logfiles\ExportKronosHRToADP_log_TESTMODE.txt'
			ENDIF
			.lLoggingIsOn = .lLoggingIsOn AND NOT EMPTY(.cLogFile)
			IF .lLoggingIsOn THEN
				SET ALTERNATE TO (.cLogFile) ADDITIVE
				SET ALTERNATE ON
			ENDIF
		ENDWITH
	ENDFUNC


	FUNCTION DESTROY
		WITH THIS
			IF .lLoggingIsOn  THEN
				SET ALTERNATE OFF
				SET ALTERNATE TO
			ENDIF
			SET STATUS BAR ON
		ENDWITH
		DODEFAULT()
	ENDFUNC


	FUNCTION MAIN
		WITH THIS
			LOCAL lnNumberOfErrors, lcSQL, loError, lcErrorMsg
			TRY
				lnNumberOfErrors = 0

				.TrackProgress('', LOGIT)
				.TrackProgress('', LOGIT)
				.TrackProgress('=========================================================', LOGIT+SENDIT)
				.TrackProgress('EXPORT KRONOS HR TO ADP process started....', LOGIT+SENDIT+NOWAITIT)
				.TrackProgress('COMPUTERNAME = ' + .cCOMPUTERNAME, LOGIT+SENDIT)
				.TrackProgress('USERNAME = ' + .cUSERNAME, LOGIT+SENDIT)
				.TrackProgress('=========================================================', LOGIT+SENDIT)
				IF .lTestMode THEN
					.TrackProgress('', LOGIT)
					.TrackProgress('=========================================================', LOGIT+SENDIT)
					.TrackProgress('                        TEST MODE                        ', LOGIT+SENDIT)
					.TrackProgress('=========================================================', LOGIT+SENDIT)
				ENDIF
				IF .lOnlyOutputNewEmployees THEN
					.TrackProgress('=========================================================', LOGIT+SENDIT)
					.TrackProgress('               MODE = Only Output New Employees          ', LOGIT+SENDIT)
					.TrackProgress('=========================================================', LOGIT+SENDIT)
				ENDIF
				IF NOT .lProcessEmployeeChanges THEN
					.TrackProgress('=========================================================', LOGIT+SENDIT)
					.TrackProgress('           MODE = Do not process Employee Changes        ', LOGIT+SENDIT)
					.TrackProgress('=========================================================', LOGIT+SENDIT)
				ENDIF


				*!*					*******************************************************
				*!*					*******************************************************
				*!*
				*!*					IF .f. THEN

				*!*						* get ss#s from ADP

				*!*						.nSQLHandle = SQLCONNECT("KronosHR","MBENNETT1804","MBENNETT8196")

				*!*						IF .nSQLHandle > 0 THEN

				*!*							*!*	lcSQL = ;
				*!*							*!*		" Select " + ;
				*!*							*!*		" CAST(CoCode AS CHAR), " + ;
				*!*							*!*		" CAST(empno AS CHAR), " + ;
				*!*							*!*		" CAST(positionidno AS CHAR) AS FILE_NUM, " + ;
				*!*							*!*		" CAST(persontaxidno AS CHAR), " + ;
				*!*							*!*		" CAST(lastname AS CHAR), " + ;
				*!*							*!*		" CAST(firstname AS CHAR), " + ;
				*!*							*!*		" CAST(personaddress1 AS CHAR), " + ;
				*!*							*!*		" CAST(personaddress2 AS CHAR), " + ;
				*!*							*!*		" CAST(personaddresscity AS CHAR), " + ;
				*!*							*!*		" CAST(localcodeabbreviation AS CHAR), " + ;
				*!*							*!*		" CAST(personaddresspostalcode AS CHAR), " + ;
				*!*							*!*		" CAST(Gender AS CHAR), " + ;
				*!*							*!*		" CAST(ORGANIZATIONCODE AS CHAR), " + ;
				*!*							*!*		" payrate, " + ;
				*!*							*!*		" CAST(employeestatusidno AS CHAR), " + ;
				*!*							*!*		" lasthiredate, " + ;
				*!*							*!*		" terminationdate, " + ;
				*!*							*!*		" birthdate, " + ;
				*!*							*!*		" CAST(WORKED_ST AS CHAR), " + ;
				*!*							*!*		" CAST(TAXJC AS CHAR) " + ;
				*!*							*!*		" FROM FMI_ADP_INTERFACE " + ;
				*!*							*!*		" WHERE employeestatusidno = -10131 "

				*!*							*!*	lcSQL = ;
				*!*							*!*		" SELECT " + ;
				*!*							*!*		" CAST(empno AS CHAR) AS FILE_NUM, " + ;
				*!*							*!*		" CAST(lastname AS CHAR) AS LASTNAME, " + ;
				*!*							*!*		" CAST(firstname AS CHAR) AS FIRSTNAME " + ;
				*!*							*!*		" FROM FMI_ADP_INTERFACE " + ;
				*!*							*!*		" WHERE employeestatusidno = -10131 "
				*!*
				*!*							lcSQL = ;
				*!*								" SELECT " + ;
				*!*								" empno AS FILE_NUM, " + ;
				*!*								" LASTNAME, " + ;
				*!*								" FIRSTNAME " + ;
				*!*								" FROM FMI_ADP_INTERFACE " + ;
				*!*								" WHERE employeestatusidno = -10131 "
				*!*
				*!*							IF USED('HRCURSOR1') THEN
				*!*								USE IN HRCURSOR1
				*!*							ENDIF

				*!*							IF .ExecSQL(lcSQL, 'HRCURSOR1', RETURN_DATA_MANDATORY) THEN
				*!*
				*!*								SELECT HRCURSOR1
				*!*								BROWSE
				*!*
				*!*							ENDIF  &&  .ExecSQL(lcSQL, 'SQLCURSOR1', RETURN_DATA_MANDATORY)
				*!*
				*!*						ELSE
				*!*							* connection error
				*!*							.TrackProgress('Unable to connect to Kronos HR System.', LOGIT+SENDIT)
				*!*						ENDIF   &&  .nSQLHandle > 0
				*!*
				*!*						CLOSE DATABASES
				*!*
				*!*						X = 2*.F.
				*!*
				*!*
				*!*					ENDIF

				*******************************************************
				*******************************************************

				* get ss#s from ADP
				*OPEN DATABASE F:\UTIL\ADPCONNECTION\CONNECT1

				.nSQLHandle = SQLCONNECT("PCPAYWIN","MBENNETT","VP737A")

				IF .nSQLHandle > 0 THEN

					lcSQL = ;
						"SELECT " + ;
						" {fn IFNULL(COMPANYCODE,'???')} AS ADPCOMP, " + ;
						" FILE# AS FILE_NUM, " + ;
						" SOCIALSECURITY# AS SS_NUM, " + ;
						" LASTNAME, " + ;
						" FIRSTNAME, " + ;
						" {fn IFNULL(STREETLINE1,' ')} AS STREETLINE1, " + ;
						" {fn IFNULL(STREETLINE2,' ')} AS STREETLINE2, " + ;
						" CITY, " + ;
						" STATE, " + ;
						" ZIPCODE, " + ;
						" GENDER, " + ;
						" {fn IFNULL(HOMEDEPARTMENT,'??????')} AS HOMEDEPT, " + ;
						" RATETYPE, " + ;
						" {fn IFNULL(RATE1AMT,000000.00)} AS RATE1AMT, " + ;
						" STATUS, " + ;
						" HIREDATE, " + ;
						" BIRTHDATE, " + ;
						" STATEWRKDINCODE, " + ;
						" SUISDITAXJURISCD " + ;
						" FROM REPORTS.V_EMPLOYEE "

					IF USED('SQLCURSOR1') THEN
						USE IN SQLCURSOR1
					ENDIF
					IF USED('SQLCURSOR2') THEN
						USE IN SQLCURSOR2
					ENDIF

					IF .ExecSQL(lcSQL, 'SQLCURSOR1', RETURN_DATA_MANDATORY) THEN


						************************* MAIN PROCESSING FOLLOWS ***********************************

						* do some formatting to match Kronos output CSV files
						SELECT ;
							ADPCOMP, ;
							FILE_NUM, ;
							(LEFT(SS_NUM,3) + "-" + SUBSTR(SS_NUM,4,2) + "-" + RIGHT(SS_NUM,4)) AS SS_NUM, ;
							LASTNAME, ;
							FIRSTNAME, ;
							STREETLINE1, ;
							STREETLINE2, ;
							CITY, ;
							STATE, ;
							ZIPCODE, ;
							GENDER, ;
							HOMEDEPT, ;
							RATETYPE, ;
							RATE1AMT, ;
							STATUS, ;
							IIF(ISNULL(HIREDATE),{},TTOD(HIREDATE)) AS HIREDATE, ;
							IIF(ISNULL(BIRTHDATE),{},TTOD(BIRTHDATE)) AS BIRTHDATE, ;
							STATEWRKDINCODE, ;
							SUISDITAXJURISCD ;
							FROM SQLCURSOR1 ;
							INTO CURSOR SQLCURSOR2 ;
							ORDER BY 1, 2 ;
							READWRITE

						SELECT SQLCURSOR2
						INDEX ON ADPCOMP TAG ADPCOMP
						INDEX ON FILE_NUM TAG FILE_NUM
						SELECT SQLCURSOR2
						SET ORDER TO
						GO TOP

						WAIT WINDOW NOWAIT "Opening Excel..."
						.oExcel = CREATEOBJECT("excel.application")

						.oExcel.VISIBLE = .lTestMode

						WAIT WINDOW NOWAIT "Processing AXA..."
						.ProcessKronosOutputFiles( "AXA" )

						WAIT WINDOW NOWAIT "Processing SXI..."
						.ProcessKronosOutputFiles( "SXI" )

						WAIT WINDOW NOWAIT "Processing ZXU..."
						.ProcessKronosOutputFiles( "ZXU" )

						* delete RESTART FILES IN ADP FOLDER, IF THEY EXIST
						IF FILE(.cADPPath + "RESTART.PRAXAEMP.CSV") THEN
							DELETE FILE (.cADPPath + "RESTART.PRAXAEMP.CSV")
						ENDIF
						IF FILE(.cADPPath + "RESTART.PRSXIEMP.CSV") THEN
							DELETE FILE (.cADPPath + "RESTART.PRSXIEMP.CSV")
						ENDIF
						IF FILE(.cADPPath + "RESTART.PRZXUEMP.CSV") THEN
							DELETE FILE (.cADPPath + "RESTART.PRZXUEMP.CSV")
						ENDIF


						* construct text for top of email
						IF .nNumberOfOutputFiles > 0 THEN
							IF NOT EMPTY(.cNewHireList) THEN
								.cTopEmailText = "New Employees have been exported from Kronos HR:" + CRLF + CRLF + ;
									.cNewHireList + CRLF + ;
									"Please import employee info into ADP." + CRLF + CRLF + "<process log follows>" + CRLF + CRLF
							ELSE
								.cTopEmailText = "New Employee Information has been exported from Kronos HR." + CRLF + CRLF + ;
									"Please import employee info into ADP." + CRLF + CRLF + "<process log follows>" + CRLF + CRLF
							ENDIF
						ELSE
							.cTopEmailText = "No Kronos HR --> ADP Export files were created!" + CRLF + CRLF + ;
								"<process log follows>" + CRLF + CRLF
						ENDIF

						.cBodyText = .cTopEmailText + .cBodyText

						IF .lTestMode THEN
							.TrackProgress(.cBodyText, LOGIT)
						ENDIF

						IF TYPE('.oExcel') = 'O' AND NOT ISNULL(.oExcel) THEN
							.oExcel.QUIT()
						ENDIF
						************************* END OF MAIN PROCESSING ***********************************

						CLOSE DATABASES ALL
					ENDIF  &&  .ExecSQL(lcSQL, 'SQLCURSOR1', RETURN_DATA_MANDATORY)

					.TrackProgress('', LOGIT+SENDIT)
					.TrackProgress('EXPORT KRONOS HR TO ADP process ended normally.', LOGIT+SENDIT+NOWAITIT)
					WAIT CLEAR

				ELSE
					* connection error
					.TrackProgress('Unable to connect to ADP Payroll System.', LOGIT+SENDIT)
				ENDIF   &&  .nSQLHandle > 0

			CATCH TO loError
				WAIT CLEAR
				.TrackProgress('There was an error.',LOGIT+SENDIT)
				.TrackProgress('The Error # is: ' + TRANSFORM(loError.ERRORNO), LOGIT+SENDIT)
				.TrackProgress('The Error Message is: ' + TRANSFORM(loError.MESSAGE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred in Program: ' + TRANSFORM(loError.PROCEDURE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred at line #: ' + TRANSFORM(loError.LINENO), LOGIT+SENDIT)
				lnNumberOfErrors = lnNumberOfErrors + 1
				IF TYPE('.oExcel') = 'O' AND NOT ISNULL(.oExcel) THEN
					.oExcel.QUIT()
				ENDIF
				* close output file, if necessary
				IF .nCurrentOutputCSVFileHandle >= 0 THEN
					.CloseOutputCSVFile()
				ENDIF
				CLOSE DATA

			ENDTRY

			CLOSE DATA
			WAIT CLEAR
			***************** INTERNAL email results ******************************
			.TrackProgress('About to send status email.',LOGIT)
			.TrackProgress('The logfile is: ' + .cLogFile, SENDIT)
			.TrackProgress('EXPORT KRONOS HR TO ADP process started: ' + .cStartTime, LOGIT+SENDIT)
			.TrackProgress('EXPORT KRONOS HR TO ADP process finished: ' + TTOC(DATETIME()), LOGIT+SENDIT)

			IF .lSendInternalEmailIsOn THEN
				IF (.nNumberOfOutputFiles = 0) OR .lTestMode THEN
					.cSendTo = 'Mark Bennett <mbennett@fmiint.com>'
				ENDIF
				* try to trap error from not having dartmail dll's registered on user's pc...
				TRY
					DO FORM dartmail2 WITH .cSendTo,.cFrom,.cSubject,.cCC,.cAttach,.cBodyText,"A"
					.TrackProgress('Sent status email.',LOGIT)
				CATCH TO loError
					*=MESSAGEBOX('There was an error sending the status email. Please contact Mark Bennett.',0+64,"EXPORT KRONOS HR TO ADP")
					.TrackProgress('There was an error sending the status email.',LOGIT)
					.TrackProgress('The Error # is: ' + TRANSFORM(loError.ERRORNO), LOGIT)
					.TrackProgress('The Error Message is: ' + TRANSFORM(loError.MESSAGE), LOGIT)
					.TrackProgress('The Error occurred in Program: ' + TRANSFORM(loError.PROCEDURE), LOGIT)
					.TrackProgress('The Error occurred at line #: ' + TRANSFORM(loError.LINENO), LOGIT)
					lnNumberOfErrors = lnNumberOfErrors + 1
					IF TYPE('.oExcel') = 'O' AND NOT ISNULL(.oExcel) THEN
						.oExcel.QUIT()
					ENDIF
					CLOSE DATA
				ENDTRY
			ELSE
				.TrackProgress('Did not send status email: THIS.lSendInternalEmailIsOn = FALSE.',LOGIT)
			ENDIF

		ENDWITH
		RETURN
	ENDFUNC && main


	PROCEDURE ProcessKronosOutputFiles
		LPARAMETERS tcADPCOMP
		LOCAL lnNumFiles, laFiles[1,5], laFilesSorted[1,6], lcSourceCSVFile, lcArchivedCSVFile, lcTargetCSVFile
		LOCAL oWorkbook, oWorksheet, lnRow, lcRow, i, j, llEmployeeChanged, lcChangeReason, llWroteOutputHeader, lcEmployeeIdentifier
		LOCAL lcADPCOMP, lnFileNum, lcSS_NUM, lcLASTNAME, lcFIRSTNAME, lcAddress1, lcAddress2, lcCity, lcState, lcZip
		LOCAL lcGender, lcHOMEDEPT, llNewEmployee, llNeedToOutputEmployee, lcOutputLine, lcRateType, lnRate1Amt
		LOCAL lcStatus, ldHireDate, ldBirthDate, lcSTATEWRKDINCODE, lcSUISDITAXJURISCD, ldOneMonthAgo, lcExcludeReason

		WITH THIS

			ldOneMonthAgo = .dOneMonthAgo

			* get files to process
			SET DEFAULT TO (.cInPath)
			lnNumFiles = ADIR(laFiles, tcADPCOMP + "*.CSV")

			*SET STEP ON

			IF lnNumFiles > 0

				.TrackProgress('', LOGIT+SENDIT)
				.TrackProgress('Found ' + TRANSFORM(lnNumFiles) + ' ' + tcADPCOMP + ' files; starting KronosHRToADP process.', LOGIT+SENDIT)

				* sort file list by date/time
				.SortArrayByDateTime(@laFiles, @laFilesSorted)

				* if multiple files were found, delete all but most recent, and then process the most recent file
				* this will work fine because each file is a complete set of employees for current adp_company,
				* and we will gain nothing by processing the older files.

				FOR i = 1 TO lnNumFiles

					lcSourceCSVFile = .cInPath + laFilesSorted[i,1]

					IF i = lnNumFiles THEN
						* it's the latest file; process
						.TrackProgress('Processing: ' + lcSourceCSVFile, LOGIT+SENDIT)

						lcArchivedCSVFile = .cArchiveSourceCSVPath + laFilesSorted[i,1]
						lcTargetCSVFile = .cADPPath + laFilesSorted[i,1]
						lcOutputFilename = .cADPPath + "PR" + tcADPCOMP + "EMP.CSV"
						lcTargetArchivedFilename = .cArchiveTargetCSVPath + "PR" + tcADPCOMP + "EMP_" + .cFileDate + ".CSV"

						* Open the Source CSV file in Excel
						oWorkbook = .oExcel.workbooks.OPEN( lcSourceCSVFile )
						oWorksheet = oWorkbook.Worksheets[1]

						llWroteOutputHeader = .F.
						.nCurrentOutputCSVFileHandle = -1


						FOR lnRow = 2 TO 2000
							llEmployeeChanged = .F.
							llNewEmployee = .F.
							lcChangeReason = ""
							lcRow = LTRIM(STR(lnRow))

							lcADPCOMP = oWorksheet.RANGE("A" + lcRow).VALUE
							lnFileNum = oWorksheet.RANGE("B" + lcRow).VALUE
							lcLASTNAME = ALLTRIM(oWorksheet.RANGE("D" + lcRow).VALUE)

							* stop if we're at end of spreadsheet
							IF ISNULL(lnFileNum) OR ISNULL(lcADPCOMP) OR EMPTY(lnFileNum) OR EMPTY(lcADPCOMP) THEN
								EXIT FOR
							ENDIF

							* Get Status and Hiredate for next check 3/7/08 MB.
							lcStatus = ALLTRIM(oWorksheet.RANGE("O" + lcRow).VALUE)
							ldHireDate = oWorksheet.RANGE("P" + lcRow).VALUE
							IF ISNULL(ldHireDate) OR EMPTY(ldHireDate) THEN
								ldHireDate = {}
							ENDIF
							IF TYPE('ldHireDate') = 'T' THEN
								ldHireDate = TTOD(ldHireDate)
							ENDIF

							* If we only want to process New Employees (which it looks like we are stuck doing),
							* only process for Status = 'A' and Hiredate not more than one month in the past.
							* This is necessary to prevent employees "purged" in ADP from being erroneously exported.
							IF ((lcStatus = 'A') AND (ldHireDate > ldOneMonthAgo) AND .lOnlyOutputNewEmployees) ;
								OR (NOT .lOnlyOutputNewEmployees) THEN
								* okay to process

								*SET STEP ON

								lcSS_NUM = ALLTRIM(oWorksheet.RANGE("C" + lcRow).VALUE)
								lcLASTNAME = ALLTRIM(oWorksheet.RANGE("D" + lcRow).VALUE)
								lcFIRSTNAME = ALLTRIM(oWorksheet.RANGE("E" + lcRow).VALUE)
								lcAddress1 = ALLTRIM(oWorksheet.RANGE("F" + lcRow).VALUE)
								lcAddress2 = oWorksheet.RANGE("G" + lcRow).VALUE
								IF ISNULL(lcAddress2) THEN
									lcAddress2 = ' '
								ELSE
									lcAddress2 = ALLTRIM(TRANSFORM(lcAddress2))
								ENDIF
								lcCity = ALLTRIM(oWorksheet.RANGE("H" + lcRow).VALUE)
								lcState = ALLTRIM(oWorksheet.RANGE("I" + lcRow).VALUE)
								lcZip = PADL(INT(oWorksheet.RANGE("J" + lcRow).VALUE),5,"0")
								lcGender = ALLTRIM(oWorksheet.RANGE("K" + lcRow).VALUE)
								lcHOMEDEPT = PADL(INT(oWorksheet.RANGE("L" + lcRow).VALUE),6,"0")
								lcRateType = ALLTRIM(oWorksheet.RANGE("M" + lcRow).VALUE)
								lnRate1Amt = oWorksheet.RANGE("N" + lcRow).VALUE
								ldBirthDate = oWorksheet.RANGE("Q" + lcRow).VALUE
								IF ISNULL(ldBirthDate) OR EMPTY(ldBirthDate) THEN
									ldBirthDate = {}
								ENDIF
								IF TYPE('ldBirthDate') = 'T' THEN
									ldBirthDate = TTOD(ldBirthDate)
								ENDIF

								*!*
								*!*				IF (NOT INLIST(TYPE('ldBirthDate'),'D','T')) OR ISNULL(ldBirthDate) THEN
								*!*					SET STEP ON
								*!*				ENDIF

								lcSTATEWRKDINCODE = ALLTRIM(oWorksheet.RANGE("R" + lcRow).VALUE)
								lcSUISDITAXJURISCD = PADL(INT(oWorksheet.RANGE("S" + lcRow).VALUE),2,"0")

								***********************************************************
								*  9/18/07 MB
								* special processing for Over-The-Road drivers
								* set their ratetype to 'N' and their rate1amt to 0.00
								IF lcHOMEDEPT = "020655" THEN
									lcRateType = "N"
									lnRate1Amt = 0.00
								ENDIF
								***********************************************************

								* if we got to here, ok to continue
								* search SQLCURSOR2 to find COMPANY/file#
								* if found() then determine if anything changed, otherwise flag it as new employee
								SELECT SQLCURSOR2
								lcEmployeeIdentifier = lcADPCOMP + " " + TRANSFORM(lnFileNum) + " :  " + lcLASTNAME + ", " + lcFIRSTNAME + CRLF

								LOCATE FOR (ADPCOMP = lcADPCOMP) AND (FILE_NUM = lnFileNum)
								IF NOT FOUND() THEN
									llEmployeeChanged = .T.
									llNewEmployee = .T.
									lcChangeReason = lcChangeReason + "New HR ADPCOMP / File#: " + lcEmployeeIdentifier
									.cNewHireList = .cNewHireList + lcEmployeeIdentifier
								ELSE
									llNewEmployee = .F.
									IF .lProcessEmployeeChanges THEN
										IF ( lcSS_NUM <> SQLCURSOR2.SS_NUM ) THEN
											llEmployeeChanged = .T.
											lcChangeReason = lcChangeReason + "New SS# for " + lcEmployeeIdentifier
											lcChangeReason = lcChangeReason + ;
												"     HR: " + lcSS_NUM + "     ADP: " + SQLCURSOR2.SS_NUM + CRLF
										ENDIF
										IF ( lcLASTNAME <> SQLCURSOR2.LASTNAME ) THEN
											llEmployeeChanged = .T.
											lcChangeReason = lcChangeReason + "New Last Name for " + lcEmployeeIdentifier
											lcChangeReason = lcChangeReason + ;
												"     HR: " + lcLASTNAME + "     ADP: " + SQLCURSOR2.LASTNAME + CRLF
										ENDIF
										* disable this test because in Kronos HR we filtered out middle initials that are in ADP,
										* causing multiple hits.
										*!*										IF ( lcFIRSTNAME <> SQLCURSOR2.FIRSTNAME ) THEN
										*!*											llEmployeeChanged = .T.
										*!*											lcChangeReason = lcChangeReason + "New First Name for " + lcEmployeeIdentifier
										*!*										ENDIF
										IF ( ALLTRIM(lcAddress1) + ALLTRIM(lcAddress2) ) <> ( ALLTRIM(SQLCURSOR2.STREETLINE1) + ALLTRIM(SQLCURSOR2.STREETLINE2) ) THEN
											llEmployeeChanged = .T.
											lcChangeReason = lcChangeReason + "New Street Address for " + lcEmployeeIdentifier
											lcChangeReason = lcChangeReason + ;
												"     HR: " + ALLTRIM(lcAddress1) + ALLTRIM(lcAddress2) + "     ADP: " + ALLTRIM(SQLCURSOR2.STREETLINE1) + ALLTRIM(SQLCURSOR2.STREETLINE2) + CRLF
										ENDIF
										IF ( lcCity <> ALLTRIM(SQLCURSOR2.CITY) ) THEN
											llEmployeeChanged = .T.
											lcChangeReason = lcChangeReason + "New City for " + lcEmployeeIdentifier
											lcChangeReason = lcChangeReason + ;
												"     HR: " + lcCity + "     ADP: " + ALLTRIM(SQLCURSOR2.CITY) + CRLF
										ENDIF
										IF ( lcState <> SQLCURSOR2.STATE ) THEN
											llEmployeeChanged = .T.
											lcChangeReason = lcChangeReason + "New State for " + lcEmployeeIdentifier
											lcChangeReason = lcChangeReason + ;
												"     HR: " + lcState + "     ADP: " + SQLCURSOR2.STATE + CRLF
										ENDIF
										IF ( lcZip <> ALLTRIM(SQLCURSOR2.ZIPCODE) ) THEN
											llEmployeeChanged = .T.
											lcChangeReason = lcChangeReason + "New Zip Code for " + lcEmployeeIdentifier
											lcChangeReason = lcChangeReason + ;
												"     HR: " + lcZip + "     ADP: " + ALLTRIM(SQLCURSOR2.ZIPCODE) + CRLF
										ENDIF
										IF ( lcGender <> SQLCURSOR2.GENDER ) THEN
											llEmployeeChanged = .T.
											lcChangeReason = lcChangeReason + "New Gender for " + lcEmployeeIdentifier
											lcChangeReason = lcChangeReason + ;
												"     HR: " + lcGender + "     ADP: " + SQLCURSOR2.GENDER + CRLF
										ENDIF
										IF ( lcHOMEDEPT <> SQLCURSOR2.HOMEDEPT ) THEN
											llEmployeeChanged = .T.
											lcChangeReason = lcChangeReason + "New Home Dept for " + lcEmployeeIdentifier
											lcChangeReason = lcChangeReason + ;
												"     HR: " + lcHOMEDEPT + "     ADP: " + SQLCURSOR2.HOMEDEPT + CRLF
										ENDIF
										IF ( lcRateType <> SQLCURSOR2.RATETYPE ) THEN
											llEmployeeChanged = .T.
											lcChangeReason = lcChangeReason + "New Rate Type for " + lcEmployeeIdentifier
											lcChangeReason = lcChangeReason + ;
												"     HR: " + lcRateType + "     ADP: " + SQLCURSOR2.RATETYPE + CRLF
										ENDIF
										IF ( ALLTRIM(STR(lnRate1Amt,9,2)) <> ALLTRIM(STR(SQLCURSOR2.RATE1AMT,9,2)) ) THEN
											llEmployeeChanged = .T.
											lcChangeReason = lcChangeReason + "New Rate1Amt for " + lcEmployeeIdentifier
											lcChangeReason = lcChangeReason + ;
												"     HR: " + ALLTRIM(STR(lnRate1Amt,9,2)) + "     ADP: " + ALLTRIM(STR(SQLCURSOR2.RATE1AMT,9,2)) + CRLF
										ENDIF
										IF ( lcStatus <> SQLCURSOR2.STATUS ) THEN
											llEmployeeChanged = .T.
											lcChangeReason = lcChangeReason + "New Status for " + lcEmployeeIdentifier
											lcChangeReason = lcChangeReason + ;
												"     HR: " + lcStatus + "     ADP: " + SQLCURSOR2.STATUS + CRLF
										ENDIF
										IF ( ldHireDate <> SQLCURSOR2.HIREDATE ) THEN
											llEmployeeChanged = .T.
											lcChangeReason = lcChangeReason + "New Hire Date for " + lcEmployeeIdentifier
											lcChangeReason = lcChangeReason + ;
												"     HR: " + TRANSFORM(ldHireDate) + "     ADP: " + TRANSFORM(SQLCURSOR2.HIREDATE) + CRLF
										ENDIF
										IF ( ldBirthDate <> SQLCURSOR2.BIRTHDATE ) THEN
											llEmployeeChanged = .T.
											lcChangeReason = lcChangeReason + "New Birth Date for " + lcEmployeeIdentifier
											lcChangeReason = lcChangeReason + ;
												"     HR: " + TRANSFORM(ldBirthDate) + "     ADP: " + TRANSFORM(SQLCURSOR2.BIRTHDATE) + CRLF
										ENDIF
										IF ( lcSTATEWRKDINCODE <> SQLCURSOR2.STATEWRKDINCODE ) THEN
											llEmployeeChanged = .T.
											lcChangeReason = lcChangeReason + "New State-Worked-In Code for " + lcEmployeeIdentifier
											lcChangeReason = lcChangeReason + ;
												"     HR: " + lcSTATEWRKDINCODE + "     ADP: " + SQLCURSOR2.STATEWRKDINCODE + CRLF
										ENDIF
										*!*	IF ( lcSUISDITAXJURISCD <> SQLCURSOR2.SUISDITAXJURISCD ) THEN
										*!*		llEmployeeChanged = .T.
										*!*		lcChangeReason = lcChangeReason + "New SUI/SDI Tax Code for " + lcEmployeeIdentifier
										*!*		lcChangeReason = lcChangeReason + ;
										*!*			"     HR: " + lcSUISDITAXJURISCD + "     ADP: " + SQLCURSOR2.SUISDITAXJURISCD + CRLF
										*!*	ENDIF
									ENDIF && .lProcessEmployeeChanges THEN

								ENDIF  && NOT FOUND()


								IF llEmployeeChanged THEN
									.TrackProgress(lcChangeReason, LOGIT+SENDIT)
								ENDIF

								* determine if need to output, based on property settings
								llNeedToOutputEmployee = (.lOnlyOutputNewEmployees AND llNewEmployee) ;
									OR ( (NOT .lOnlyOutputNewEmployees) AND llEmployeeChanged)

								IF llNeedToOutputEmployee THEN

									IF NOT llWroteOutputHeader THEN
										.CreateOutputCSVFile(tcADPCOMP)
										llWroteOutputHeader = .WriteOutputHeader()
									ENDIF
									* Write Detail Line

									lcOutputLine = ;
										.CleanUpField(lcADPCOMP) + "," + ;
										.CleanUpField(lnFileNum) + "," + ;
										.CleanUpField(lcSS_NUM) + "," + ;
										.CleanUpField(lcLASTNAME) + "," + ;
										.CleanUpField(lcFIRSTNAME) + "," + ;
										.CleanUpField(lcAddress1) + "," + ;
										.CleanUpField(lcAddress2) + "," + ;
										.CleanUpField(lcCity) + "," + ;
										.CleanUpField(lcState) + "," + ;
										.CleanUpField(lcZip) + "," + ;
										.CleanUpField(lcGender) + "," + ;
										.CleanUpField(lcHOMEDEPT) + "," + ;
										.CleanUpField(lcRateType) + "," + ;
										.CleanUpField(ALLTRIM(STR(lnRate1Amt,9,2))) + "," + ;
										.CleanUpField(lcStatus) + "," + ;
										.CleanUpField(TRANSFORM(ldHireDate)) + "," + ;
										.CleanUpField(TRANSFORM(ldBirthDate)) + "," + ;
										.CleanUpField(lcSTATEWRKDINCODE)

									=FPUTS(.nCurrentOutputCSVFileHandle,lcOutputLine)

									*** removed tax jurisdiction per Lucille 5/8/08 MB because they were not always right
									*** .CleanUpField(lcSUISDITAXJURISCD)

								ENDIF  &&  llEmployeeChanged

							ELSE
								* ee was excluded because not active or had old hiredate
								* don't log for now...
									*!*	lcExcludeReason = "EXCLUDED: " + lcADPCOMP + " " + PADL(lnFileNum,4,'0') + " " + lcLASTNAME + ;
									*!*		+ " Status = " + lcStatus + " Hiredate = " + TRANSFORM(ldHireDate)
									*!*	.TrackProgress(lcExcludeReason, LOGIT)
							ENDIF  &&  ((lcStatus = 'A') AND (ldHireDate > ldOneMonthAgo)) OR (NOT .lOnlyOutputNewEmployees)
							
						ENDFOR  &&  lnRow = 2 TO 2000


						* we're done with source CSV file
						oWorkbook.CLOSE()

						* close output file
						.CloseOutputCSVFile()

						* COPY the source csv file to the 'archived' folder
						* It does no harm to leave it there in case we want to rerun the export, and
						* when newer files are produced, it will be deleted.
						IF FILE(lcSourceCSVFile) THEN
							COPY FILE (lcSourceCSVFile) TO (lcArchivedCSVFile)
						ENDIF
						*IF FILE(lcArchivedCSVFile) THEN
						*	DELETE FILE (lcSourceCSVFile) RECYCLE
						*ENDIF

						* copy output file (renamed so it is unique) to TARGETARCHIVED folder on F:
						IF FILE(lcOutputFilename) THEN
							COPY FILE (lcOutputFilename) TO (lcTargetArchivedFilename)
						ENDIF

					ELSE
						* just delete the old file
						DELETE FILE (lcSourceCSVFile)
						.TrackProgress('Deleted old file: ' + lcSourceCSVFile, LOGIT+SENDIT)
					ENDIF

					.TrackProgress('=========================================================', LOGIT+SENDIT)


				ENDFOR  &&  i = lnNumFiles

			ENDIF  && lnNumFiles > 0
		ENDWITH
	ENDPROC


	FUNCTION CleanUpField
		* prepare field for inclusion in the string to be written with FPUTS().
		* especially critical to remove any commas, since that creates bogus column in CSV output file.
		LPARAMETERS tuValue
		LOCAL lcRetVal
		lcRetVal = STRTRAN(ALLTRIM(TRANSFORM(tuValue)),",","")
		RETURN lcRetVal
	ENDFUNC


	FUNCTION CreateOutputCSVFile
		LPARAMETERS tcADPCOMP
		WITH THIS
			LOCAL lcOutputFilename, llRetval
			lcOutputFilename = .cADPPath + "PR" + tcADPCOMP + "EMP.CSV"
			llRetval = .T.
			.nCurrentOutputCSVFileHandle = FCREATE(lcOutputFilename)
			IF .nCurrentOutputCSVFileHandle < 0 THEN
				.TrackProgress('***** Error on FCREATE of: ' + lcOutputFilename, LOGIT+SENDIT)
				llRetval = .F.
			ELSE
				.TrackProgress('FCREATEed: ' + lcOutputFilename, LOGIT+SENDIT)
				.nNumberOfOutputFiles = .nNumberOfOutputFiles + 1
				DO CASE
					CASE tcADPCOMP = "AXA"
						.lOutputAXAFile = .T.
					CASE tcADPCOMP = "SXI"
						.lOutputSXIFile = .T.
					CASE tcADPCOMP = "ZXU"
						.lOutputZXUFile = .T.
					OTHERWISE
						* nothing
				ENDCASE
			ENDIF
			.TrackProgress('nCurrentOutputCSVFileHandle = ' + TRANSFORM(.nCurrentOutputCSVFileHandle), LOGIT+SENDIT)
		ENDWITH
		RETURN llRetval
	ENDFUNC


	FUNCTION WriteOutputHeader
		WITH THIS
			LOCAL llRetval, lnBytesWritten
			* note: no CRLF on first part of header line
			llRetval = .T.
			lnBytesWritten = FWRITE(.nCurrentOutputCSVFileHandle,.cOutputCSVHeaderLine1)
			.TrackProgress('FWROTE # of bytes: ' + TRANSFORM(lnBytesWritten), LOGIT+SENDIT)
			lnBytesWritten = FWRITE(.nCurrentOutputCSVFileHandle,.cOutputCSVHeaderLine2 + CRLF)
			.TrackProgress('FWROTE # of bytes: ' + TRANSFORM(lnBytesWritten), LOGIT+SENDIT)
		ENDWITH
		RETURN llRetval
	ENDFUNC


	FUNCTION CloseOutputCSVFile
		WITH THIS
			LOCAL llRetval
			llRetval = .T.
			IF .nCurrentOutputCSVFileHandle > -1 THEN
				llRetval = FCLOSE(.nCurrentOutputCSVFileHandle)
			ENDIF
		ENDWITH
		RETURN llRetval
	ENDFUNC


	PROCEDURE SortArrayByDateTime
		* expects taInArray[] to be the result of an ADIR()
		LPARAMETERS taInArray, taOutArray
		EXTERNAL ARRAY taInArray, taOutArray
		LOCAL lnRows, lnCols, i, j
		* extending taInArray[] to process the files in date/time order
		lnRows = ALEN(taInArray,1)
		lnCols = ALEN(taInArray,2)
		* create taOutArray with one more column
		DIMENSION taOutArray[lnRows,lnCols + 1]
		* and fill it with taInArray columns AND (filedate + filetime) in new column
		* ( ACOPY() doesn't preserve elements properly because of different # of columns )
		FOR i = 1 TO lnRows
			FOR j = 1 TO lnCols
				taOutArray[i,j] = taInArray[i,j]
			ENDFOR
			taOutArray[i,lnCols + 1] = DTOS(taInArray[i,3]) + "_" + taInArray[i,4]
		ENDFOR
		* sort taOutArray on date/time column
		=ASORT(taOutArray,6)
	ENDPROC


	FUNCTION ExecSQL
		LPARAMETERS tcSQL, tcCursorName, tlNoDataReturnedIsOkay
		LOCAL llRetval, lnResult
		WITH THIS
			* close target cursor if it's open
			IF USED(tcCursorName)
				USE IN (tcCursorName)
			ENDIF
			WAIT WINDOW "Fetching remote data; please wait..." NOWAIT
			lnResult = SQLEXEC(.nSQLHandle, tcSQL, tcCursorName)
			llRetval = ( lnResult > 0 )
			IF llRetval THEN
				* see if any data came back
				IF NOT tlNoDataReturnedIsOkay THEN
					IF (NOT USED(tcCursorName)) OR EOF(tcCursorName) THEN
						llRetval = .F.
						.TrackProgress('No data was returned by this query: ' +CRLF + tcSQL, LOGIT+SENDIT)
					ENDIF
				ENDIF
			ELSE
				.TrackProgress("Result Code [" + TRANSFORM(lnResult) + "] returned by this query: " + tcSQL, LOGIT+SENDIT)
			ENDIF
			WAIT CLEAR
			RETURN llRetval
		ENDWITH
	ENDFUNC


	PROCEDURE TrackProgress
		* do any combination of Wait Window, writing to logfile, and adding to body of email,
		* based on nFlags parameter.
		LPARAMETERS tcExpression, tnFlags
		WITH THIS
			IF BITAND(tnFlags,LOGIT) = LOGIT THEN
				IF .lLoggingIsOn THEN
					?
					? .cProcessName + "  " + TTOC(DATETIME()) + ": " + tcExpression
				ENDIF
			ENDIF
			IF .lWaitWindowIsOn THEN
				IF BITAND(tnFlags,NOWAITIT) = NOWAITIT THEN
					WAIT WINDOW tcExpression NOWAIT
				ENDIF
				IF BITAND(tnFlags,WAITIT) = WAITIT THEN
					WAIT WINDOW tcExpression TIMEOUT .nWaitWindowTimeout
				ENDIF
			ENDIF
			IF BITAND(tnFlags,SENDIT) = SENDIT THEN
				IF .lSendInternalEmailIsOn THEN
					.cBodyText = .cBodyText + tcExpression + CRLF + CRLF
				ENDIF
			ENDIF
		ENDWITH
	ENDPROC  &&  TrackProgress

ENDDEFINE

