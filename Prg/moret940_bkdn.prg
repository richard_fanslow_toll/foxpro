PARAMETERS cFilename

lDoColor = .T.
lLX = .F.
lPick = .T.
lPrepack = .F.
lSolidPack = .F.
lDoPKG = .F.

SELECT x856
SET FILTER TO
LOCATE FOR x856.segment = "ISA"

DO CASE
	CASE x856.f5 = "ZZ" AND x856.f6 = "SBHINTMTS"
		nAcctNum = 5451
	CASE x856.f5 = "ZZ" AND x856.f6 = "HIGHPOINT"
		nAcctNum = 5452
	CASE x856.f5 = "ZZ" AND x856.f6 = "HBESTLMTD"
		nAcctNum = 5453
	CASE x856.f5 = "ZZ" AND x856.f6 = "BOZKEELLC"
		nAcctNum = 6262
	CASE x856.f5 = "ZZ" AND x856.f6 = "REGENTSUT"
		nAcctNum = 6261
	CASE x856.f5 = "ZZ" AND x856.f6 = "MORET00SK"
		nAcctNum = 6403
	CASE x856.f5 = "ZZ" AND x856.f6 = "BOYMEETSG"
		nAcctNum = 6572
	CASE x856.f5 = "ZZ" AND x856.f6 = "SPRAYMORET"
		nAcctNum = 6648
ENDCASE

LOCATE
COUNT FOR TRIM(segment) = "W05" TO nSegCnt
LOCATE

cSegCnt = ALLTRIM(STR(nSegCnt))
WAIT "There are "+cSegCnt+" P/Ts in this file" WINDOW TIMEOUT 2
nPTqty = nSegCnt
nTotPT = 1
STORE 0 TO pt_total_cartons,nINSegnum,nCtnCount,ptctr,nTotWt,nTotCube,m.PTDETID,lnEaches,nRepQty,lPackQty
STORE "" TO cStoreNum,cBillTo,m.isa_num,m.printstuff


*!*	IF !USED("uploaddet")
*!*		IF lTesting OR lTestUploaddet
*!*			USE F:\3pl\test\uploaddet IN 0 ALIAS uploaddet
*!*		ELSE
*!*			USE F:\ediwhse\DATA\uploaddet IN 0 SHARED ALIAS uploaddet
*!*		ENDIF
*!*	ENDIF

SELECT xpt
DELETE ALL
SELECT xptdet
DELETE ALL

SELECT x856
LOCATE
IF lTesting
*	BROWSE
ENDIF

DO WHILE !EOF("x856")
	cSegment = TRIM(x856.segment)
	cF1 = TRIM(x856.f1)
*	ASSERT .f. MESSAGE "At Segment Loop in x856...DEBUG"

	IF TRIM(x856.segment) = "ISA"
		cISA_Num = ALLTRIM(x856.f13)
		SELECT x856
		IF !EOF()
			SKIP 1 IN x856
			LOOP
		ENDIF
	ENDIF

	IF INLIST(TRIM(x856.segment),"GS","ST")
		SELECT x856
		IF !EOF()
			SKIP 1 IN x856
			LOOP
		ENDIF
	ENDIF

	IF TRIM(x856.segment) = "W05"  && new shipment
		STORE "" TO cShip_ref,cCNEE_ref
		lAsst = .F.
		nSubline = 0
		SELECT xpt
		APPEND BLANK
		STORE 0 TO pt_total_cartons
		nXPTQty = 0
		ptctr = ptctr +1
		REPLACE xpt.shipins   WITH "ISANUM*"+cISA_Num+CHR(13)+"FILE940*"+JUSTFNAME(xfile) IN xpt
		REPLACE xpt.addby     WITH "FMI PROC" IN xpt
		REPLACE xpt.adddt     WITH DATETIME() IN xpt
		REPLACE xpt.ptid      WITH ptctr IN xpt
		REPLACE xpt.ptdate    WITH DATE() IN xpt
		REPLACE xpt.accountid WITH nAcctNum IN xpt
		cShip_ref = ALLTRIM(x856.f2)
		m.ship_ref = ALLTRIM(cShip_ref)
		REPLACE xpt.ship_ref WITH cShip_ref IN xpt && Pickticket
		cCNEE_ref = ""
		cCNEE_ref = UPPER(ALLTRIM(x856.f3))
		REPLACE xpt.cnee_ref WITH cCNEE_ref IN xpt  && Customer PO
		WAIT CLEAR
		waitstr = "Now processing PT # "+cShip_ref+CHR(13)+"FILE: "+cFilename
		WAIT WINDOW AT 10,10  waitstr NOWAIT
	ENDIF

	IF TRIM(x856.segment) = "GE"
*
	ENDIF

	IF TRIM(x856.segment) = "N1" AND INLIST(TRIM(x856.f1),"ST","CN")
		cStoreNum = UPPER(ALLTRIM(x856.f4))
		cConsignee = UPPER(ALLT(x856.f2))
		cDCNum = ""
		REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"STORENUM*"+cStoreNum IN xpt  && DC or Store Number
		REPLACE xpt.dcnum WITH cStoreNum IN xpt
		REPLACE xpt.storenum WITH INT(VAL(RIGHT(ALLT(xpt.dcnum),5))) IN xpt
		REPLACE xpt.consignee WITH cConsignee IN xpt NEXT 1
		REPLACE xpt.NAME WITH xpt.consignee IN xpt NEXT 1

		SKIP 1 IN x856
		DO CASE
			CASE TRIM(x856.segment) = "N2"  && address info
				IF !lTesting
					cStoreNum = UPPER(ALLTRIM(x856.f1))
					cStoreName = UPPER(ALLTRIM(x856.f2))
					IF !"FAC*"$xpt.shipins AND !EMPTY(cStoreNum)
						REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"FAC*"+cStoreNum IN xpt
					ENDIF
*					REPLACE xpt.dcnum WITH cStoreNum IN xpt
				ENDIF

				SKIP 1 IN x856
				IF TRIM(x856.segment) = "N3"  && address info
					REPLACE xpt.address WITH UPPER(TRIM(x856.f1)) IN xpt
					REPLACE xpt.address2  WITH UPPER(TRIM(x856.f2)) IN xpt
				ELSE
					cMessage = "Missing Ship-To Address Info"
					WAIT WINDOW cMessage TIMEOUT 2
					NormalExit = .F.
					THROW
				ENDIF

			CASE TRIM(x856.segment) = "N3"  && address info
				REPLACE xpt.address WITH UPPER(TRIM(x856.f1)) IN xpt
				REPLACE xpt.address2  WITH UPPER(TRIM(x856.f2)) IN xpt
			OTHERWISE
				cMessage = "Missing Ship-To Address Info"
				WAIT WINDOW cMessage TIMEOUT 2
				NormalExit = .F.
				THROW
		ENDCASE

		SKIP 1 IN x856
		IF TRIM(x856.segment) = "N4"  && address info
			m.country = TRIM(x856.f4)
*!*				IF EMPTY(TRIM(x856.f2))
*!*				cCSZ = TRIM(x856.f1)+", "+TRIM(x856.f4)+" "+TRIM(x856.f3)
*!*				else
			cCSZ = TRIM(x856.f1)+", "+PADR(TRIM(x856.f2),2)+" "+TRIM(x856.f3)
*!*				endif
			REPLACE xpt.csz WITH UPPER(cCSZ) IN xpt
		ELSE
			cMessage = "Missing Ship-To CSZ Info"
			WAIT WINDOW cMessage TIMEOUT 2
			NormalExit = .F.
			THROW
		ENDIF
	ENDIF

	IF (TRIM(x856.segment) = "N1" AND TRIM(x856.f1) = "Z7")
		STORE UPPER(TRIM(x856.f2)) TO cMarkfor && will be overwritten if there is an N2 loop
		REPLACE xpt.shipfor WITH cMarkfor IN xpt
		cSFStoreNum = UPPER(ALLTRIM(x856.f4))
		REPLACE xpt.sforstore WITH cSFStoreNum IN xpt
		REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"SFSTORENUM*"+cSFStoreNum IN xpt  && DC or Store Number
		SKIP 1 IN x856
		DO CASE
			CASE TRIM(x856.segment) = "N2"  && address info
				STORE UPPER(TRIM(x856.f2)) TO cMarkfor
				REPLACE xpt.shipfor WITH cMarkfor IN xpt
				REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"SFSTORELONG*"+UPPER(ALLTRIM(x856.f1)) IN xpt
				REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"STORENAME*"+ALLTRIM(x856.f2) IN xpt
				REPLACE xpt.sforstore WITH ALLTRIM(x856.f1) IN xpt
				SKIP 1 IN x856
				IF TRIM(x856.segment) = "N3"  && address info
					REPLACE xpt.sforaddr1 WITH UPPER(TRIM(x856.f1)) IN xpt
					REPLACE xpt.sforaddr2  WITH UPPER(TRIM(x856.f2)) IN xpt
				ELSE
					cMessage = "Missing Ship-For Address Info (N2>N3), PT "+cShip_ref
					WAIT WINDOW cMessage TIMEOUT 2
					SET STEP ON
					NormalExit = .F.
					THROW
				ENDIF
			CASE TRIM(x856.segment) = "N3"  && address info
				REPLACE xpt.sforaddr1 WITH UPPER(TRIM(x856.f1)) IN xpt
				REPLACE xpt.sforaddr2  WITH UPPER(TRIM(x856.f2)) IN xpt
			OTHERWISE
				cMessage = "Missing Ship-For Address Info (N1>N2), PT "+cShip_ref
				WAIT WINDOW cMessage TIMEOUT 2
				NormalExit = .F.
				THROW
		ENDCASE
		SKIP 1 IN x856
		IF TRIM(x856.segment) = "N4"  && address info
*!*				IF EMPTY(TRIM(x856.f2))
*!*				csforCSZ = TRIM(x856.f1)+", "+TRIM(x856.f4)+" "+TRIM(x856.f3)
*!*				else
			csforCSZ = TRIM(x856.f1)+", "+PADR(TRIM(x856.f2),2)+" "+TRIM(x856.f3)
*!*				endif
			REPLACE xpt.sforcsz WITH UPPER(csforCSZ) IN xpt
		ELSE
			cMessage = "Missing Ship-For CSZ Info"
			WAIT WINDOW cMessage TIMEOUT 2
			NormalExit = .F.
			THROW
		ENDIF
	ENDIF

	IF TRIM(x856.segment) = "N9" AND TRIM(x856.f1) = "DP" && Dept Num.
		cDept = TRIM(x856.f2)
		REPLACE xpt.dept WITH cDept IN xpt
		cFOB =  ALLTRIM(x856.f3)
		IF !EMPTY(cFOB)
			REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"FOB*"+cFOB IN xpt
		ENDIF
	ENDIF

	IF TRIM(x856.segment) = "N9" AND TRIM(x856.f1) = "UC" && Cust UCC#
		REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"UCC*"+ALLTRIM(x856.f2)
	ENDIF

	IF TRIM(x856.segment) = "N9" AND TRIM(x856.f1) = "IA" && Vendor Num.
		REPLACE xpt.vendor_num WITH ALLTRIM(x856.f2) IN xpt
		cDUNS =  ALLTRIM(x856.f3)
		IF !EMPTY(cDUNS)
			REPLACE xpt.DUNS WITH cDUNS IN xpt
		ENDIF
	ENDIF

	IF TRIM(x856.segment) = "N9" AND TRIM(x856.f1) = "CN" && C/Acct Num.
		REPLACE xpt.cacctnum WITH ALLTRIM(x856.f2) IN xpt
		lLids = IIF(ALLTRIM(x856.f3)="LIDS",.T.,.F.)
	ENDIF

	IF TRIM(x856.segment) = "N9" AND TRIM(x856.f1) = "OT" && Cust Order Type
		REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"ORDERTYPE*"+ALLTRIM(x856.f2) IN xpt
		REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"ORDERTYPE2*"+ALLTRIM(x856.f3) IN xpt
	ENDIF

	IF TRIM(x856.segment) = "N9" AND TRIM(x856.f1) = "CR" AND lLids && Cust PO Num for LIDS
		REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"LIDSCODE*"+ALLTRIM(x856.f2) IN xpt
	ENDIF

	IF ALLTRIM(x856.segment) = "N9" AND ALLTRIM(x856.f1) = "DV"
		ASSERT .F. MESSAGE "IN moret divs...DEBUG"
		cDiv = ALLTRIM(x856.f2)
		nDiv = INT(VAL(cDiv))
		IF USED('mdivs')
			USE IN mdivs
		ENDIF
		SELECT 0
		USE F:\3pl\DATA\moretedidivs ALIAS mdivs
		LOCATE
		LOCATE FOR mdivs.conum = cCoNum AND mdivs.divcode = cDiv
		IF FOUND()
			cCoCode = ALLTRIM(mdivs.cocode)
			m.accountid = mdivs.accountid
			REPLACE xpt.accountid WITH m.accountid IN xpt
			REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"COCODE*"+cCoCode+CHR(13)+"DIVCODE*"+cDiv IN xpt
		ELSE
			SET STEP ON
			WAIT WINDOW "No Company found with div# "+cDiv TIMEOUT 2
			THROW
		ENDIF
	ENDIF

	IF TRIM(x856.segment) = "N9" AND TRIM(x856.f1) = "RE" && Replenishment
		REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"REPLENISHMENT*Y" IN xpt
	ENDIF

	IF TRIM(x856.segment) = "G62" AND TRIM(x856.f1) == "37" && Start Date
		cXdate1 = TRIM(x856.f2)
		datestrconversion()
		REPLACE xpt.START WITH dxdate2 IN xpt
	ENDIF

	IF TRIM(x856.segment) = "G62" AND TRIM(x856.f1) == "38" && Cancel Date
		cXdate1 = TRIM(x856.f2)
		datestrconversion()
		REPLACE xpt.CANCEL WITH dxdate2 IN xpt
	ENDIF

	IF TRIM(x856.segment) = "NTE"  && Notes
		IF ALLTRIM(x856.f1) = "OTH"
			nINSegnum = nINSegnum + 1
			cINSegnum = "IN"+ALLTRIM(STR(nINSegnum))+"*"
			IF !EMPTY(ALLTRIM(x856.f2))
				REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+cINSegnum+ALLTRIM(x856.f2) IN xpt
			ENDIF
		ENDIF
	ENDIF

	IF TRIM(x856.segment) = "W66"  && Shipping info
		REPLACE xpt.ship_via WITH ALLTRIM(x856.f5) IN xpt
		REPLACE xpt.scac WITH ALLTRIM(x856.f10) IN xpt
		REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"CARRMODE*"+ALLTRIM(x856.f2) IN xpt
		REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"FOBPYMT*"+ALLTRIM(x856.f1) IN xpt
	ENDIF

*********************************************
*!* XPTDET Update Section
*********************************************
	lLoopSeg = "LX"
	cUPC = ""
	IF TRIM(x856.segment) = lLoopSeg && start of PT detail, stay here until this PT is complete
		nTotQty = 0
		lLX = .T.

		DO WHILE TRIM(x856.segment) != "SE"
			m.printstuff = ""
			cxSegment = TRIM(x856.segment)
			cxField01 = TRIM(x856.f1)

			IF TRIM(x856.segment) = "LX"
				SKIP 1 IN x856
				LOOP
			ENDIF

			IF TRIM(x856.segment) = "W01"
				lConverted = IIF(TRIM(x856.f14)="ZZ",.T.,.F.)
				m.printstuff = ""
*				m.upc = ALLTRIM(x856.f7)
				cOrigStyle = ALLT(x856.f5)
				cSepChar = "-"
				IF lConverted
					nDashes = OCCURS(cSepChar,cOrigStyle)
					IF nDashes = 0
						cSepChar = "/"
						nDashes = OCCURS(cSepChar,cOrigStyle)
						IF nDashes = 0
							WAIT WINDOW "No / or -, Converted style "+cOrigStyle+", @ PT "+cShip_ref TIMEOUT 2
							THROW
						ENDIF
					ENDIF
					m.style = LEFT(cOrigStyle,AT(cSepChar,cOrigStyle,nDashes)-1)
					cColorOut = ALLTRIM(SUBSTR(cOrigStyle,AT(cSepChar,cOrigStyle)+1))
					m.printstuff = IIF(EMPTY(m.printstuff),"COLOROUT*"+cColorOut,m.printstuff+CHR(13)+"COLOROUT*"+cColorOut)
				ELSE
					ASSERT .F. MESSAGE "At unconverted style"
					FOR uy = 1 TO 2
						cSepChar = IIF(uy = 1,'-','/')
						m.style = STRTRAN(cOrigStyle,cSepChar,"")
					ENDFOR
				ENDIF
				m.printstuff = IIF(EMPTY(m.printstuff),"ORIGSTYLE*"+cOrigStyle,m.printstuff+CHR(13)+"ORIGSTYLE*"+cOrigStyle)
				m.totqty = INT(VAL(x856.f1))
				m.origqty = m.totqty
				nTotQty = nTotQty + m.totqty
				m.printstuff = IIF(EMPTY(m.printstuff),"UNITCODE*"+TRIM(x856.f2),m.printstuff+CHR(13)+"UNITCODE*"+TRIM(x856.f2))
				SELECT xptdet
				APPEND BLANK
				m.PTDETID=m.PTDETID+1
				GATHER MEMVAR MEMO
				REPLACE xptdet.addby WITH "FMI PROC" IN xptdet
				REPLACE xptdet.adddt WITH DATETIME() IN xptdet
				REPLACE xptdet.ptid      WITH xpt.ptid IN xptdet
				REPLACE xptdet.accountid WITH xpt.accountid IN xptdet
				nPack = 0
				nXPTQty = nXPTQty + m.totqty
				lnEaches = 0
			ENDIF
			SELECT x856

			IF TRIM(x856.segment) = "G69"
				IF EMPTY(xptdet.printstuff)
					REPLACE xptdet.printstuff WITH "MASTSTYLEDESC*"+TRIM(x856.f1) IN xptdet
				ELSE
					REPLACE xptdet.printstuff WITH xptdet.printstuff+CHR(13)+"MASTSTYLEDESC*"+TRIM(x856.f1) IN xptdet
				ENDIF
			ENDIF

			IF TRIM(x856.segment) = "N9" AND TRIM(x856.f1) = "ZZ"  && Subdetail
				DO WHILE TRIM(x856.segment) = "N9" AND TRIM(x856.f1) = "ZZ"
					nSubline = nSubline + 1
					cSubline = PADL(ALLTRIM(STR(nSubline)),3,"0")
					cSubdata = ALLTRIM(x856.f2)
					nAmp1 = AT("%",cSubdata,1)
					nAmp2 = AT("%",cSubdata,2)
					cSubStyle = ALLTRIM(LEFT(cSubdata,nAmp1-1))
					cSubColor = SUBSTR(cSubdata,nAmp1+1,(nAmp2-1-nAmp1))
					cSubSize = ALLTRIM(SUBSTR(cSubdata,nAmp2+1))

					cSubdata = ALLTRIM(x856.f3)
					nAmp1 = AT("%",cSubdata,1)
					nAmp2 = AT("%",cSubdata,2)
					cSubUPC = ALLTRIM(LEFT(cSubdata,nAmp1-1))
					STORE cSubUPC TO cUPC
					REPLACE xptdet.upc WITH cUPC IN xptdet
					cSubCustsku = SUBSTR(cSubdata,nAmp1+1,(nAmp2-1-nAmp1))
					cSubPack = ALLT(SUBSTR(cSubdata,nAmp2+1))

					cPrintstuff&cSubline = "SUBLINE*"+cSubline
					cPrintstuff&cSubline = cPrintstuff&cSubline+CHR(13)+"SUBCUSTSKU*"+cSubCustsku
					cPrintstuff&cSubline = cPrintstuff&cSubline+CHR(13)+"SUBSTYLE*"+cSubStyle
					cPrintstuff&cSubline = cPrintstuff&cSubline+CHR(13)+"SUBUOM*UNITMEAS"
					cPrintstuff&cSubline = cPrintstuff&cSubline+CHR(13)+"SUBPACK*"+cSubPack
					cPrintstuff&cSubline = cPrintstuff&cSubline+CHR(13)+"SUBUPC*"+cSubUPC
					cPrintstuff&cSubline = cPrintstuff&cSubline+CHR(13)+"SUBCOLOR*"+cSubColor
					cPrintstuff&cSubline = cPrintstuff&cSubline+CHR(13)+"SUBSIZE*"+cSubSize
					cPrintstuff&cSubline = cPrintstuff&cSubline+CHR(13)
*						WAIT WINDOW [cPrintstuff&cSubline]+" = "+cPrintstuff&cSubline TIMEOUT 2

					IF EMPTY(xptdet.printstuff)
						REPLACE xptdet.printstuff WITH cPrintstuff&cSubline IN xptdet
					ELSE
						REPLACE xptdet.printstuff WITH xptdet.printstuff+CHR(13)+cPrintstuff&cSubline IN xptdet
					ENDIF
					SKIP 1 IN x856
				ENDDO
			ENDIF
*					ASSERT .F. MESSAGE "At end of N9ZZ loops...debug"

			IF TRIM(x856.segment) = "N9" AND TRIM(x856.f1) = "UP"  && Master UPC
				cUPC = ALLTRIM(x856.f2)
				REPLACE xptdet.upc WITH cUPC IN xptdet
			ENDIF

			IF TRIM(x856.segment) = "N9" AND TRIM(x856.f1) = "IN"  && Customer Item #
				cCustsku = ALLTRIM(x856.f2)
				REPLACE xptdet.custsku WITH cCustsku IN xptdet
			ENDIF

			IF TRIM(x856.segment) = "N9" AND TRIM(x856.f1) = "UK"  && GTIN
				cGTIN = ALLTRIM(x856.f2)
				REPLACE xptdet.printstuff WITH xptdet.printstuff+CHR(13)+"CUSTGTIN*"+cGTIN IN xptdet
			ENDIF

			IF TRIM(x856.segment) = "N9" AND TRIM(x856.f1) = "PGC"  && Pkg code for labels
				cPGC = ALLTRIM(x856.f2)
				REPLACE xptdet.printstuff WITH xptdet.printstuff+CHR(13)+"PGC*"+cPGC IN xptdet
			ENDIF

			IF TRIM(x856.segment) = "N9" AND TRIM(x856.f1) = "PK"  && Pack type
				cPacktype = ALLTRIM(x856.f2)
				lUnits = IIF(cPacktype="PG",.T.,.F.)
				lPrepack = IIF(lUnits,.F.,.T.)
				IF !("PACKTYPE*"$xpt.shipins)
					REPLACE xpt.shipins WITH xpt.shipins+CHR(13)+"PACKTYPE*"+;
						IIF(lPrepack,"PREPACK","PICKPACK") IN xpt
				ENDIF
				IF !("PACKTYPE*"$xptdet.printstuff)
					REPLACE xptdet.printstuff WITH xptdet.printstuff+CHR(13)+"PACKTYPE*"+;
						IIF(lPrepack,"PREPACK","PICKPACK") IN xptdet
				ENDIF
				REPLACE xptdet.printstuff WITH STRTRAN(xptdet.printstuff,"UNITMEAS",cPacktype) NEXT 1 IN xptdet
				REPLACE xpt.batch_num WITH  IIF(lPrepack,"C","U") IN xpt
				REPLACE xpt.goh WITH  IIF(lPrepack,.F.,.T.) IN xpt
				REPLACE xptdet.units WITH lUnits NEXT 1 IN xptdet
			ENDIF

			IF TRIM(x856.segment) = "W76"
*				ASSERT .F. MESSAGE "At W76 segment...debug"
				REPLACE xpt.EACHES WITH INT(VAL(TRIM(x856.f1))) IN xpt
				IF lPrepack
					REPLACE xpt.qty WITH INT(VAL(x856.f1)) IN xpt
				ELSE
					REPLACE xpt.qty WITH nTotQty IN xpt
				ENDIF
				REPLACE xpt.origqty WITH xpt.qty IN xpt
				SELECT xptdet
				REPLACE xptdet.PACK WITH "1",xptdet.casepack WITH 1 ;
					FOR xptdet.ptid = xpt.ptid IN xptdet
				cXPTQty = 0
				lLX = .F.
				STORE 0 TO nTotWt,nTotCube,pt_total_cartons
			ENDIF

			SELECT xptdet
			IF xptdet.ptid = 0
				DELETE IN xptdet
			ENDIF

			SELECT x856
			IF !EOF()
				SKIP 1 IN x856
			ENDIF
		ENDDO  && SEGMENT NOW = 'SE'
	ENDIF
	IF !EOF()
		SKIP 1 IN x856
	ENDIF
ENDDO


SELECT xptdet
IF lTesting AND lBrowfiles
	BROWSE
ENDIF

*!* Added the following for splitting by pack type into new ~ Picktickets
SELECT ptid,units,COUNT(PTDETID) AS detcnt ;
	FROM xptdet ;
	GROUP BY 1,2 ;
	INTO CURSOR det1

SELECT 0
CREATE CURSOR detscan (ptid i)
SELECT det1
IF lTesting AND lBrowfiles
	WAIT WINDOW "Browsing detail extract" TIMEOUT 3
	BROWSE
ENDIF
nPTID = 0
lUnits = .F.
SCAN
	IF nPTID<>det1.ptid
		nPTID = det1.ptid
		lUnits = det1.units
		LOOP
	ELSE
		IF lUnits<>det1.units
			INSERT INTO detscan (ptid) VALUES (det1.ptid)
		ENDIF
	ENDIF
ENDSCAN

SELECT xpt
SET ORDER TO ptid

SELECT detscan
LOCATE
IF lTesting AND lBrowfiles
	WAIT WINDOW "Browsing detail scan-splits file" TIMEOUT 3
	BROWSE
	SET STEP ON 
ENDIF

SCAN
	nPTID = detscan.ptid  && Original PTID
	SELECT xpt
	LOCATE
	CALCULATE MAX(xpt.ptid) TO m.ptid
	m.ptid = m.ptid+1
	STORE m.ptid  TO nPTID2 && New PTID
	LOCATE
	LOCATE FOR xpt.ptid = nPTID
	SCATTER MEMVAR FIELDS EXCEPT ptid MEMO
	INSERT INTO xpt FROM MEMVAR
	GO BOTTOM
	REPLACE xpt.batch_num WITH "C",xpt.ship_ref WITH TRIM(xpt.ship_ref)+" ~C" IN xpt  &&  Cases pickticket
	REPLACE xpt.goh WITH .F. IN xpt
*	ASSERT .f. MESSAGE "At Packtype correction"
	IF !("PACKTYPE*"$xpt.shipins)
		REPLACE xpt.shipins WITH "PACKTYPE*PREPACK"+CHR(13)+xpt.shipins IN xpt
	ELSE
		IF ("PACKTYPE*PICKPACK"$xpt.shipins)
			REPLACE xpt.shipins WITH ;
				STRTRAN(xpt.shipins,"PACKTYPE*PICKPACK","PACKTYPE*PREPACK") IN xpt
		ENDIF
	ENDIF

	REPLACE xpt.qty WITH 0,xpt.origqty WITH 0 IN xpt NEXT 1
	LOCATE
	LOCATE FOR xpt.ptid = nPTID
	REPLACE xpt.ship_ref WITH TRIM(xpt.ship_ref)+" ~U" IN xpt  && Units pickticket
	REPLACE xpt.goh WITH .T. IN xpt
	REPLACE xpt.qty WITH 0,xpt.origqty WITH 0,xpt.batch_num WITH "U" IN xpt NEXT 1
	IF !("PACKTYPE*"$xpt.shipins)
		REPLACE xpt.shipins WITH "PACKTYPE*PICKPACK"+CHR(13)+xpt.shipins IN xpt
	ELSE
		IF ("PACKTYPE*PREPACK"$xpt.shipins)
			REPLACE xpt.shipins WITH ;
				STRTRAN(xpt.shipins,"PACKTYPE*PREPACK","PACKTYPE*PICKPACK") IN xpt
		ENDIF
	ENDIF

	SELECT xptdet
	LOCATE
	SCAN FOR xptdet.ptid = nPTID
		IF !units
			REPLACE xptdet.ptid WITH nPTID2 IN xptdet
		ENDIF
	ENDSCAN
	SELECT xptdet
	LOCATE
	SUM xptdet.totqty TO nQty FOR xptdet.ptid = nPTID
	SELECT xpt
	REPLACE xpt.qty WITH nQty FOR xpt.ptid = nPTID IN xpt
	REPLACE xpt.origqty WITH xpt.qty FOR xpt.ptid = nPTID IN xpt
	SELECT xptdet
	LOCATE
	SUM xptdet.totqty TO nQty FOR xptdet.ptid = nPTID2
	SELECT xpt
	REPLACE xpt.qty WITH nQty FOR xpt.ptid = nPTID2 IN xpt
	REPLACE xpt.origqty WITH xpt.qty FOR xpt.ptid = nPTID2 IN xpt
	STORE 0 TO nPTID,nPTID2
ENDSCAN
*!* End added Splits code

SELECT xpt
DELETE FOR xpt.ptid = 0

DO m:\dev\prg\setuppercase940

WAIT CLEAR
IF lBrowfiles
	WAIT WINDOW "Now displaying XPT/XPTDET cursors..."+CHR(13)+"CANCEL at XPTDET if desired." TIMEOUT 2
	SELECT xpt
	SET ORDER TO
	LOCATE
	BROWSE
	SELECT xptdet
	SET ORDER TO
	LOCATE
	BROWSE
	IF MESSAGEBOX("Do you wish to continue?",4+32+256,"Browse Files Box")<>6
		CANCEL
	ENDIF
ENDIF

WAIT CLEAR
WAIT cMailName+" BKDN process complete for file "+xfile WINDOW TIMEOUT 2
lLX = .F.
lnEaches = 0
lPick = .F.
nRepQty = 0
m.isa_num = ""
SELECT x856
cTotPT = ALLTRIM(STR(nTotPT))
lNewPT = .T.
RETURN

******************************
PROCEDURE datestrconversion
******************************
	cXdate1 = TRIM(cXdate1)
	dxdate2 = CTOD(SUBSTR(cXdate1,5,2)+"/"+RIGHT(cXdate1,2)+"/"+LEFT(cXdate1,4))
ENDPROC

