PARAMETERS cOffice
PUBLIC m.accountid,NormalExit,cUseFolder,tsendto,tsendtoerr,tcc,tccerr,cTransfer,lDoMail,cCustName,cMod,cWO_Num,lDoSQL,cWhseMod,xnumskus

CLOSE DATABASES ALL
DO m:\dev\prg\lookups

lTesting = .f.
NormalExit = .F.
DO m:\dev\prg\_setvars WITH .T.
lDoMail = .T.
lOverridebusy =lTesting
lOverridebusy = .t.

IF VARTYPE(cOffice) # "C"
	cOffice = "N"
ENDIF

nAcctNum = 6561
m.accountid = nAcctNum
cMod = IIF(cOffice = "C","2","I")
cWO_Num = ""
cWhseMod = LOWER("wh"+cMod)
gOffice = cMod
gMasteroffice = cOffice
xnumskus = 0

SELECT 0
USE F:\edirouting\FTPSETUP SHARED
cTransfer = "PL-MERKURY" && Will suspend all Merkury PL reattempts regardless of office

IF !lTesting
	LOCATE FOR FTPSETUP.TRANSFER = cTransfer
	ASSERT .F.
	IF FOUND() AND chkbusy = .T. AND !lOverridebusy
		WAIT WINDOW "MERKURY INBOUND UPLOAD Process is busy...exiting" TIMEOUT 2
		NormalExit = .F.
		RETURN
	ENDIF

	REPLACE chkbusy WITH .T.,trig_time WITH DATETIME()  FOR FTPSETUP.TRANSFER = cTransfer
	USE IN FTPSETUP
ENDIF

TRY

	IF lTesting
		WAIT WINDOW "Running as TEST" TIMEOUT 2
		cUseFolder = "F:\WHP\WHDATA\"
	ELSE
		cUseFolder = "F:\WH"+cMod+"\WHDATA\"
	ENDIF

	SELECT 0
	USE F:\3pl\DATA\mailmaster ALIAS mm
	LOCATE FOR mm.accountid = nAcctNum AND mm.office = cOffice AND mm.edi_type = "PL"
	IF FOUND()
		STORE TRIM(mm.AcctName) TO lcAccountname
		STORE TRIM(mm.basepath) TO lcPath
		IF lTesting
			WAIT WINDOW "File path is "+lcPath TIMEOUT 2
		ENDIF
		STORE TRIM(mm.archpath) TO lcArchPath
		STORE TRIM(mm.scaption) TO _SCREEN.CAPTION
		IF lTesting
			LOCATE FOR edi_type = "MISC" AND taskname = "GENERAL"
		ENDIF
		tsendto = IIF(mm.use_alt,sendtoalt,sendto)
		tcc = IIF(mm.use_alt,ccalt,cc)
		LOCATE
		LOCATE FOR edi_type = "MISC" AND taskname = "GENERAL"
		tsendtoerr = IIF(mm.use_alt,sendtoalt,sendto)
		tccterr = IIF(mm.use_alt,ccalt,cc)
		USE IN mm
	ELSE
		WAIT WINDOW AT 10,10  "No parameters set for this acct# "+ALLTRIM(STR(nAcctNum))+"  ---> Office "+cOffice TIMEOUT 5
		THROW
	ENDIF

*	USE (cUseFolder+"whgenpk") IN 0 ALIAS whgenpk
	useca("inwolog","wh")
	useca("pl","wh")

	USE "f:\wo\wodata\wolog" IN 0 ALIAS wolog
	useca("inrcv","wh")
	SELECT inrcv
	APPEND BLANK
	SCATTER MEMVAR MEMO
	DELETE NEXT 1 IN inrcv

	xsqlexec("select * from account where inactive=0","account",,"qq")
	INDEX ON accountid TAG accountid
	SET ORDER TO

	cCustName = ""
	cPnp = ""
	lCtnAcct = ""

	SELECT account
	=SEEK(nAcctNum,'account','accountid')
	cPnp = "pnp"+IIF(cOffice="I","N",cOffice)
	IF FOUND()
		cCustName = ALLTRIM(account.AcctName)
		lCtnAcct = !(account.&cPnp)
	ELSE
		cCustName = "NA"
		lCtnAcct = .T.
	ENDIF

	cInfolder = IIF(lTesting,lcPath+"\test\",lcPath)

	WAIT WINDOW "Document folder: "+cInfolder TIMEOUT 1
	cArchfolder = lcArchPath
	CD [&cInfolder]
	IF FILE("temp1.csv")
		DELETE FILE "temp1.csv"
	ENDIF
****convert to csv

	len1 = ADIR(ary1,"*.csv")
	IF len1 = 0
		WAIT WINDOW "No files to process" TIMEOUT 2
	ENDIF

	cfilenamexx = (cInfolder+"temp1.csv")

	IF FILE(cfilenamexx)
		DELETE FILE [&cfilenamexx]
	ENDIF

	FOR filelist = 1 TO len1
		xsqlexec("select * from inwolog where .f.","x1",,"wh")
		SELECT * FROM x1 INTO CURSOR xinwolog READWRITE
		USE IN x1

		SELECT xinwolog
		INDEX ON inwologid TAG inwologid

		xsqlexec("select * from pl where .f.","xpl",,"wh")

		SELECT 0
		CREATE CURSOR intake (f1 c(10),f2 c(40),f3 c(20),f4 c(30),f5 c(30),f6 c(10),f7 c(10),f8 c(15),f9 c(10))

		IF FILE(cfilenamexx)
			DELETE FILE [&cfilenamexx]
		ENDIF
		cfilename = ALLTRIM(ary1[filelist,1])
		cfullname = (cInfolder+cfilename)
		cArchfilename = (cArchfolder+cfilename)
		COPY FILE [&cfullname] TO [&carchfilename]
		oExcel = CREATEOBJECT("Excel.Application")
		oWorkbook = oExcel.Workbooks.OPEN(cfullname)
		WAIT WINDOW "Now saving file "+cfilename NOWAIT NOCLEAR
		SET SAFETY OFF
		oWorkbook.SAVEAS(cfilenamexx,6)  && re-saves Excel sheet as CSV file
		WAIT CLEAR
		WAIT WINDOW "File save complete...continuing" TIMEOUT 1
		oWorkbook.CLOSE(.T.)
		oExcel.QUIT()
		RELEASE oExcel

		SELECT intake
		APPEND FROM [&cfilenamexx] TYPE CSV
		DELETE FOR EMPTY(f2) IN intake
		GO BOTT
		IF lTesting
			LOCATE
			BROWSE
		ENDIF
		m.wo_date = DATE()
		m.date_rcvd = DATE()
		m.picknpack = .F.
		m.wo_num = 1
		m.inwologid = 1
		m.plid = 0
		m.AcctName = cCustName
		m.addby = "TOLLPROC"
		m.adddt = DATETIME()
		m.addproc = "MERKINB"

		SCAN
			IF '.' $(f9)
				pos1=ATC('.',f9)
				REPLACE f9 WITH SUBSTR(f9,1,pos1-1)
			ENDIF
		ENDSCAN


		SELECT xinwolog
*		APPEND BLANK

		nRec1 = RECNO()

		IF lTesting
			SET STEP ON
		ENDIF
		SELECT UPPER(ALLTRIM(f8)) AS CONTAINER,f3 AS cayset,UPPER(ALLTRIM(f4)) AS STYLE, UPPER(ALLTRIM(f5)) AS DESC,UPPER(ALLTRIM(f2)) AS ConsName,;
			INT(VAL(STRTRAN(f7,",",""))) AS ctnqty,INT(VAL(STRTRAN(f6,",",""))) AS unitqty,ALLTRIM(STRTRAN(f9,",","")) AS cpack ;
			FROM intake ;
			INTO CURSOR temppl READWRITE

		SELECT temppl
		LOCATE
		IF lTesting
			BROW
			SET STEP ON
		ENDIF

		SELECT xinwolog
		SCATTER MEMVAR MEMO BLANK
		SELECT xpl
		SCATTER MEMVAR MEMO BLANK

		m.plid = 0
		npo = 0
		lDoInsert = .T.
		SELECT temppl
		SCAN
			SCATTER MEMVAR
			m.pack = temppl.cpack
			m.units = .T.
			nRemain = 0
			IF m.ctnqty = 0
				SET STEP ON
			ENDIF
			lnOverFlowQty =0

			lnExpUnits = m.ctnqty*VAL(m.pack)
			IF lnExpUnits > m.unitqty
				lnOverFlowQty = lnExpUnits-m.unitqty
				m.unitqty = m.unitqty-lnOverFlowQty
				m.ctnqty = m.ctnqty -1
			ENDIF

			m.totqty = m.unitqty
			IF lnOverFlowQty > 0
				m.totqty2 = lnOverFlowQty
				cPack2 = ALLTRIM(STR(lnOverFlowQty))
			ENDIF

			m.mod = cMod
			m.office = cOffice

			IF lDoInsert
				lDoInsert = .F.
				INSERT INTO xinwolog FROM MEMVAR
			ENDIF
			REPLACE xinwolog.plinqty WITH xinwolog.plinqty+m.ctnqty IN xinwolog
			REPLACE xinwolog.plunitsinqty WITH  xinwolog.plunitsinqty+m.unitqty IN xinwolog
			REPLACE xinwolog.CONTAINER WITH m.container IN xinwolog
			IF !("VENDOR*"$xinwolog.comments)
				REPLACE xinwolog.comments WITH "VENDOR*"+m.ConsName IN xinwolog
			ENDIF

			m.style = ALLTRIM(m.style)
*!*					m.color = ALLTRIM(m.color)
*!*					m.id = ALLTRIM(m.id)
			m.echo = "ORIGSTYLE*"+m.style &&+CHR(13)+"ORIGCOLOR*"+m.color+CHR(13)+"ORIGSIZE*"+m.id
			npo = npo+1
			m.po = TRANSFORM(npo)

			m.plid = m.plid+1
			m.units = .F.
			m.totqty = m.ctnqty
			INSERT INTO xpl FROM MEMVAR

			m.mod = cMod
			m.office = cOffice
			m.plid = m.plid+1
			m.units = .T.
			m.totqty = m.unitqty
			m.pack ="1"
			INSERT INTO xpl FROM MEMVAR

			IF lnOverFlowQty > 0
				npo = npo+1
				m.po = TRANSFORM(npo)
				m.totqty  = m.totqty2
				m.pack = cPack2
				m.units = .F.
				m.totqty = 1
				m.plid = m.plid+1
				INSERT INTO xpl FROM MEMVAR

				m.units = .T.
				m.plid = m.plid+1
				m.pack ="1"
				INSERT INTO xpl FROM MEMVAR
			ENDIF
		ENDSCAN

		IF lTesting
			WAIT WINDOW "Now browsing cursors" TIMEOUT 2
			SELECT xinwolog
			LOCATE
			BROWSE
			SELECT xpl
			LOCATE
			BROWSE
		ENDIF

		DO m:\dev\prg\merkury_genericinbound_import

		xsqlexec("select * from inrcv where container='"+inwolog.CONTAINER+"' and status#'CONFIRMED'","xinrcv",,"wh")
		SELECT xinrcv
		IF RECCOUNT() > 0
			xsqlexec("update inrcv set inwonum="+TRANSFORM(inwolog.wo_num)+" where container='"+inwolog.CONTAINER+"' and status#'CONFIRMED'",,,"wh")
		ELSE
			SELECT inrcv
			SCATTER MEMVAR MEMO BLANK
			m.mod = cMod
			m.office = cOffice
			m.inrcvid=dygenpk("inrcv","whall")  && new get the ID value
			m.inwonum=inwolog.wo_num
			m.accountid=inwolog.accountid
			m.AcctName=inwolog.AcctName
			m.container=inwolog.CONTAINER
			m.status="NOT AVAILABLE"
			m.skus=xnumskus
			m.cartons=inwolog.plinqty
			m.newdt=DATE()
			m.addfrom="IN"
			m.confirmdt=empty2nul(m.confirmdt)
			m.newdt=empty2nul(m.newdt)
			m.dydt=empty2nul(m.dydt)
			m.firstavaildt=empty2nul(m.firstavaildt)
			m.apptdt=empty2nul(m.apptdt)
			m.pickedup=empty2nul(m.pickedup)

			INSERT INTO inrcv FROM MEMVAR
			tu("inrcv")
		ENDIF

		IF !lTesting
			DELETE FILE [&cfullname]
		ENDIF

	ENDFOR

	NormalExit = .T.


	IF FILE("temp1.csv")
		DELETE FILE "temp1.csv"
	ENDIF

CATCH TO oErr
	IF !NormalExit
		SET STEP ON
		lnRec = 0
		ptError = .T.
		tsubject = "Inbound Upload Error ("+TRANSFORM(oErr.ERRORNO)+") for "+cCustName+" at "+TTOC(DATETIME())+"  on file "+cfilename
		tattach  = ""
		tmessage = "Error processing Inbound file,GENERIC INBOUND.... Filename: "+cfilename+CHR(13)
		tmessage = tmessage+CHR(13)+" Error at record "+TRANSFORM(lnRec)+CHR(13)+CHR(13)
		tmessage =tmessage+ "Try/CATCH Exeception Message:"+CHR(13)+;
			[  Error: ] + STR(oErr.ERRORNO) +CHR(13)+;
			[  LineNo: ] + STR(oErr.LINENO) +CHR(13)+;
			[  Message: ] + oErr.MESSAGE +CHR(13)+;
			[  Procedure: ] + oErr.PROCEDURE +CHR(13)+;
			[  Details: ] + oErr.DETAILS +CHR(13)+;
			[  StackLevel: ] + STR(oErr.STACKLEVEL) +CHR(13)+;
			[  LineContents: ] + oErr.LINECONTENTS+CHR(13)+;
			[  UserValue: ] + oErr.USERVALUE+CHR(13)+;
			[  Machine Name: ] + SYS(0)+CHR(13)+;
			[  Error Message :]+CHR(13)

		tsubject = "Inbound WO Error for "+cCustName+"(Office: "+cOffice+") at "+TTOC(DATETIME())+"  on file "+cfilename
		tattach  = ""
		tsendto = tsendtoerr
		tcc = tccerr

		tFrom    ="Toll WMS Inbound EDI System Operations <toll-edi-ops@tollgroup.com>"
		DO FORM m:\dev\frm\dartmail2 WITH tsendto,tFrom,tsubject,tcc,tattach,tmessage,"A"
		lCreateErr = .T.
	ENDIF
FINALLY
	WAIT WINDOW "All "+cCustName+" Inbound Complete...Exiting" TIMEOUT 2
	SELECT 0
	USE F:\edirouting\FTPSETUP SHARED
	REPLACE chkbusy WITH .F. FOR FTPSETUP.TRANSFER = cTransfer
	USE IN FTPSETUP
	CLOSE DATABASES ALL
ENDTRY
