parameters xaccountid, xstartdt, xenddt, xwostartdt, xwoenddt, xnotonwip, xnumskus

do case
case xaccountid=9999
	xfilter=".t."
case xaccountid>10000
	xfilter="inlist(outship.accountid,"
	select acctdet
	scan for acctgrpid=xaccountid-10000
		xfilter=xfilter+transform(acctdet.accountid)+","
	endscan
	xfilter=left(xfilter,len(xfilter)-1)+")"
otherwise
	xfilter="outship.accountid="+trans(xaccountid)
endcase

if !empty(xwostartdt)
	xfilter2="between(outship.wo_date,{"+dtoc(xwostartdt)+"},{"+dtoc(xwoenddt)+"})"
else
	xfilter2=".t."
endif

if !empty(xstartdt)
	xfilter3="between(del_date,{"+dtoc(xstartdt)+"},{"+dtoc(xenddt)+"})"
else
	xfilter3=".t."
endif

if xnotonwip
	xfilter4="outship.notonwip=.t."
else
	xfilter4=".t."
endif

set step on

xsqlexec("select outship.wo_num, style, color, id " + ;
	"from outship, outdet where outship.outshipid=outdet.outshipid and outship."+gmodfilter+" " + ;
	"and "+xfilter+" and "+xfilter2+" and "+xfilter3+" and "+xfilter4,"xdytemp",,"wh")

select wo_num, style, color, id, count(1) as numskus ;
	from xdytemp group by 1,2,3,4 into cursor xtemp

select wo_num, count(1) as numskus from xtemp group by 1 into cursor xnumskus

if xnumskus
	grptname="outwosku"
	select xnumskus

else
	grptname="outwodet"

	xsqlexec("select outship.wo_num, del_date, outship.accountid, ship_ref, ctnqty, style, " + ;
		"color, id, pack, totqty from outship, outdet where outship.outshipid=outdet.outshipid " + ;
		"and outship."+gmodfilter+" and "+xfilter+" and "+xfilter2+" and "+xfilter3+" and "+xfilter4,"xdytemp",,"wh")

	select wo_num, del_date, accountid, acctname(accountid) as acctname, ship_ref, ctnqty, style, color, id, ;
		pack, totqty, space(10) as styledesc, space(10) as colordesc, space(10) as iddesc, 000000 as numskus ;
		from xdytemp order by 4, 1 into cursor xrpt readwrite

	xwonum=0
	select xrpt
	scan 	
		if wo_num=xwonum
			replace del_date with {}, ship_ref with "", ctnqty with 0 in xrpt
		endif
		xwonum=wo_num
		=seek(accountid,"account","accountid")
		replace styledesc with iif(!empty(account.styledesc),account.styledesc,"Style"), colordesc with account.colordesc, iddesc with account.iddesc in xrpt
	endscan

	select xnumskus
	scan 
		select xrpt
		locate for wo_num=xnumskus.wo_num
		replace numskus with xnumskus.numskus
	endscan

	select xrpt
endif