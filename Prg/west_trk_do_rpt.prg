*!*	Daily delivery order report for west trucking. Requested by Rich Nazzaro.

*!*	WEST_TRK_DO_RPT

* build exe as F:\UTIL\TRUCKING\WEST_TRK_DO_RPT.EXE
* 4/24/2018  MB: changed mbennett@fmiint.com to my toll email.

LOCAL lTestMode
lTestMode = .F.

IF NOT lTestMode THEN
	utilsetup("WEST_TRK_DO_RPT")
ENDIF

DECLARE INTEGER CreateMutex IN Win32API ;
	STRING @lpMutexAttributes, ;
	STRING bInitialOwner, ;
	STRING @lpName

DECLARE INTEGER GetLastError IN Win32API

* if running in EXE check to see if already running
IF _VFP.STARTMODE > 0 THEN
	lcProcessName = "WEST_TRK_DO_RPT"
	CreateMutex(0,CHR(1),@lcProcessName)
	lnError = GetLastError()
	IF lnError = 183
		WAIT WINDOW TIMEOUT 5 "This program is already running..."
		IF NOT lTestMode THEN
			schedupdate()
		ENDIF
		RETURN .F.
	ENDIF
ENDIF

loWEST_TRK_DO_RPT = CREATEOBJECT('WEST_TRK_DO_RPT')
loWEST_TRK_DO_RPT.MAIN( lTestMode )

IF NOT lTestMode THEN
	schedupdate()
ENDIF

CLOSE DATABASES ALL
RETURN

#DEFINE LOGIT 1
#DEFINE WAITIT 2
#DEFINE NOWAITIT 4
#DEFINE SENDIT 8
#DEFINE CRLF CHR(13) + CHR(10)
#DEFINE RETURN_DATA_MANDATORY .F.
#DEFINE RETURN_DATA_NOT_MANDATORY .T.
#DEFINE CSVFORMAT 6
#DEFINE WRITE_ONLY_UNBUFFERED 11
#DEFINE COMMA ","

DEFINE CLASS WEST_TRK_DO_RPT AS CUSTOM

	cProcessName = 'WEST_TRK_DO_RPT'

	lTestMode = .T.  && .F. FOR NORMAL OPERATION, .T. TO SUPPRESS ALL BUT DEVELOPER EMAILS AND CHANGE INPUT TABLE, ETC.

	dToday = DATE()

	lAutoYield = .T.

	cStartTime = TTOC(DATETIME())

	* wait window properties
	nWaitWindowTimeout = 2
	lWaitWindowIsOn = .T.

	* logfile properties
	lLoggingIsOn = .T.
	cLogFile = 'F:\UTIL\TRUCKING\LOGFILES\WEST_TRK_DO_RPT_log.txt'
	
	* processing properties
	nRow = 0

	* INTERNAL email properties
	lSendInternalEmailIsOn = .T.
	cFrom = 'fmi-transload-ops@fmiint.com'
	*cSendTo = 'brian.howver@tollgroup.com, michael.divirgilio@tollgroup.com'
	cSendTo = 'mark.bennett@tollgroup.com'
	cCC = ''
	cSubject = "Delivery Order Report for " + TRANSFORM(DATETIME())
	cAttach = ''
	cBodyText = ''
	cMGRSendTo = ''
	cMGRFrom = ''
	cMGRSubject = ''
	cMGRCC = ''
	cMGRAttach = ''
	cMGRBodyText = ''

	cCOMPUTERNAME = ''
	cUSERNAME = ''


	FUNCTION INIT
		IF NOT DODEFAULT()
			RETURN .F.
		ENDIF
		WITH THIS
			*SET RESOURCE OFF
			CLOSE DATA
			SET CENTURY OFF
			SET DATE MDY
			*SET HOURS TO 24
			SET DECIMALS TO 0
			SET ANSI ON
			SET TALK OFF
			SET DELETED ON
			SET CONSOLE OFF
			SET EXCLUSIVE OFF
			SET SAFETY OFF
			SET EXACT OFF
			SET STATUS BAR ON
			SET SYSMENU OFF
			SET ENGINEBEHAVIOR 70
			SET MULTILOCKS ON
			_VFP.AUTOYIELD = .lAutoYield
			.cCOMPUTERNAME = GETENV("COMPUTERNAME")
			.cUSERNAME = GETENV("USERNAME")
			.lLoggingIsOn = .lLoggingIsOn AND NOT EMPTY(.cLogFile)
		ENDWITH
	ENDFUNC


	FUNCTION DESTROY
		WITH THIS
			IF .lLoggingIsOn  THEN
				SET ALTERNATE OFF
				SET ALTERNATE TO
			ENDIF
		ENDWITH
		DODEFAULT()
	ENDFUNC


	FUNCTION MAIN
		LPARAMETERS tlTestMode
		WITH THIS
			LOCAL lcRow, lcReportPeriod, lcFiletoSaveAs, lcSpreadsheetTemplate, lnNumberOfErrors
			LOCAL oExcel, oWorkbook, lcErr, lcTitle, loError, lcNumRows, ldToday, ldStartDate, ldBlankDate

			TRY
			
				lnNumberOfErrors = 0

				.lTestMode = tlTestMode

				IF .lTestMode THEN
					.cLogFile = 'F:\UTIL\TRUCKING\LOGFILES\WEST_TRK_DO_RPT_log_TESTMODE.txt'
					.cSendTo = 'mark.bennett@tollgroup.com'
					.cCC = ''
				ENDIF

				IF .lLoggingIsOn THEN
					SET ALTERNATE TO (.cLogFile) ADDITIVE
					SET ALTERNATE ON
				ENDIF

				.cStartTime = TTOC(DATETIME())

				.TrackProgress('Delivery Order Report process started....', LOGIT+SENDIT)
				.TrackProgress('COMPUTERNAME = ' + .cCOMPUTERNAME, LOGIT+SENDIT)
				.TrackProgress('USERNAME = ' + .cUSERNAME, LOGIT+SENDIT)
				.TrackProgress('PROJECT = WEST_TRK_DO_RPT', LOGIT+SENDIT)
				IF .lTestMode THEN
					.TrackProgress('                        TEST MODE                        ', LOGIT+SENDIT)
				ENDIF
				*******************************************************************************************************************
				.TrackProgress('Selecting data...', LOGIT+SENDIT+NOWAITIT)
				
				ldBlankDate = CTOD('')
				
				ldToday = .dToday				
				ldStartDate = GOMONTH(ldToday, -1) 
				
*!*	* to force a custom range of dates
*!*	ldToday = {^2016-07-21}
*!*	ldStartDate = {^2016-04-01}				
				
				lcReportPeriod = DTOS(ldStartDate) + '-' + DTOS(ldToday)
				
				
				IF USED('CURDO') THEN
					USE IN CURDO
				ENDIF

				SELECT A.VESSEL, A.CONTAINER, A.PULOC, A.SIZE, A.SSCO, A.WO_DATE, A.WOTYPE, ;
				NVL(A.ETA,ldBlankDate) AS ETA, ;
				NVL(A.AVAILDT,ldBlankDate) AS AVAILDT, ;
				NVL(A.LASTFREEDT,ldBlankDate) AS LASTFREEDT, ;
				A.ACCTNAME, A.OFFICE, A.WO_NUM  ;
				FROM F:\WO\WODATA\WOLOG.DBF A ;
				INTO CURSOR CURDO ;
				WHERE BETWEEN(A.WO_DATE,ldStartDate,ldToday) ;
				AND EMPTY(A.PICKEDUP) ;    
				AND A.OFFICE = 'C' ;
				AND NOT EMPTY(VESSEL) ;
				AND NOT EMPTY(CONTAINER) ;
				ORDER BY 1, 2
				
				
                                                 && BETWEEN(A.WO_DATE,{^2014-10-01},{^2014-10-31}) ;
                                                 

				*SELECT CURDO
				*BROWSE
			
				
				IF USED('CURDO') AND NOT EMPTY('CURDO') THEN
				
					lcNumRows = ALLTRIM(TRANSFORM(RECCOUNT('CURDO') + 1))			
				
					lcSpreadsheetTemplate = 'F:\UTIL\TRUCKING\TEMPLATES\WEST_TRK_DO_RPT_TEMPLATE.XLS'
					lcFiletoSaveAs = 'F:\UTIL\TRUCKING\REPORTS\WEST_TRK_DO_RPT_' + lcReportPeriod + '.XLS'
					
					IF FILE(lcFiletoSaveAs) THEN
						DELETE FILE (lcFiletoSaveAs)
					ENDIF
					
					.TrackProgress('Creating Excel report...', LOGIT+SENDIT+NOWAITIT)

					oExcel = CREATEOBJECT("excel.application")
					oExcel.displayalerts = .F.
					oExcel.VISIBLE = .F.

					oWorkbook = oExcel.workbooks.OPEN(lcSpreadsheetTemplate)

					oWorksheet = oWorkbook.Worksheets[1]
					
					oWorksheet.RANGE("A2","Q" + lcNumRows).clearcontents()
					
					oWorkbook.SAVEAS(lcFiletoSaveAs)
					
					*oWorksheet.RANGE("D1").VALUE = lcReportPeriod

					.nRow = 1
					
	*SET STEP ON 

					SELECT CURDO
					SCAN
						.nRow = .nRow + 1
						lcRow = ALLTRIM(STR(.nRow))
						
*!*							IF .nRow = 13 then
*!*								SET STEP ON 
*!*							ENDIF
*!*							x = TYPE('CURDO.LASTFREEDT')
						
						oWorksheet.RANGE("A"+lcRow).VALUE = CURDO.VESSEL
						oWorksheet.RANGE("B"+lcRow).VALUE = CURDO.CONTAINER
						oWorksheet.RANGE("C"+lcRow).VALUE = CURDO.PULOC
						oWorksheet.RANGE("D"+lcRow).VALUE = CURDO.SIZE
						oWorksheet.RANGE("E"+lcRow).VALUE = CURDO.SSCO
						oWorksheet.RANGE("F"+lcRow).VALUE = CURDO.WO_DATE
						oWorksheet.RANGE("G"+lcRow).VALUE = CURDO.WOTYPE
						
						* strange, I am getting OLE errors if I try to assign a blank date to a cell....thus the if test is needed.
						IF NOT EMPTYNUL(CURDO.ETA) THEN
							oWorksheet.RANGE("H"+lcRow).VALUE = CURDO.ETA
						ENDIF
						
						IF NOT EMPTYNUL(CURDO.AVAILDT) THEN
							oWorksheet.RANGE("I"+lcRow).VALUE = CURDO.AVAILDT
						ENDIF
						
						IF NOT EMPTYNUL(CURDO.LASTFREEDT) THEN
							oWorksheet.RANGE("J"+lcRow).VALUE = DTOC(CURDO.LASTFREEDT)  && changed to dtoc() of the date because this line was crashing for no reason I could discover 8/21/2017 MB
						ENDIF
						
						oWorksheet.RANGE("K"+lcRow).VALUE = CURDO.ACCTNAME
						oWorksheet.RANGE("L"+lcRow).VALUE = CURDO.OFFICE
						oWorksheet.RANGE("M"+lcRow).VALUE = CURDO.WO_NUM	
					ENDSCAN

					oWorkbook.SAVE()

					IF TYPE('oExcel') = 'O' AND NOT ISNULL(oExcel) THEN
						oExcel.QUIT()
					ENDIF
					
					.cAttach = lcFiletoSaveAs
					
					.cBodyText = 'See attached delivery order report.' + CRLF + CRLF +  '<<report log follows>>' + CRLF + .cBodyText					
				
				ELSE
				
					.TrackProgress('No data was found to report!', LOGIT+SENDIT)
				
				ENDIF && USED('CURDO') AND NOT EMPTY('CURDO') THEN
				
				WAIT CLEAR
				

				.TrackProgress('Delivery Order Report process ended normally.', LOGIT+SENDIT)

			CATCH TO loError
				
				*oWorkbook.SAVE()

				.TrackProgress('There was an error.',LOGIT+SENDIT)
				.TrackProgress('The Error # is: ' + TRANSFORM(loError.ERRORNO), LOGIT+SENDIT)
				.TrackProgress('The Error Message is: ' + TRANSFORM(loError.MESSAGE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred in Program: ' + TRANSFORM(loError.PROCEDURE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred at line #: ' + TRANSFORM(loError.LINENO), LOGIT+SENDIT)
				.TrackProgress('.nRow = ' + TRANSFORM(.nRow), LOGIT+SENDIT)
				lnNumberOfErrors = lnNumberOfErrors + 1
				.cSendTo = 'mark.bennett@tollgroup.com'

				IF TYPE('oExcel') = 'O' AND NOT ISNULL(oExcel) THEN
					oExcel.QUIT()
				ENDIF

			ENDTRY

			WAIT CLEAR
			***************** INTERNAL email results ******************************
			.TrackProgress('About to send status email.',LOGIT+SENDIT)
			.TrackProgress('The logfile is: ' + .cLogFile, LOGIT+SENDIT)
			.TrackProgress('Delivery Order Report process started: ' + .cStartTime, LOGIT+SENDIT)
			.TrackProgress('Delivery Order Report process finished: ' + TTOC(DATETIME()), LOGIT+SENDIT)

			IF .lSendInternalEmailIsOn THEN
				DO FORM dartmail2 WITH .cSendTo,.cFrom,.cSubject,.cCC,.cAttach,.cBodyText,"A"
				.TrackProgress('Sent status email.',LOGIT)
			ELSE
				.TrackProgress('Did not send status email: THIS.lSendInternalEmailIsOn = FALSE.',LOGIT+SENDIT)
			ENDIF

		ENDWITH
		RETURN
	ENDFUNC && main



	PROCEDURE TrackProgress
		* do any combination of Wait Window, writing to logfile, and adding to body of email,
		* based on nFlags parameter.
		LPARAMETERS tcExpression, tnFlags
		WITH THIS
			IF BITAND(tnFlags,LOGIT) = LOGIT THEN
				IF .lLoggingIsOn THEN
					?
					? .cProcessName + "  " + TTOC(DATETIME()) + ": " + tcExpression
				ENDIF
			ENDIF
			IF .lWaitWindowIsOn THEN
				IF BITAND(tnFlags,NOWAITIT) = NOWAITIT THEN
					WAIT WINDOW tcExpression NOWAIT
				ENDIF
				IF BITAND(tnFlags,WAITIT) = WAITIT THEN
					WAIT WINDOW tcExpression TIMEOUT .nWaitWindowTimeout
				ENDIF
			ENDIF
			IF BITAND(tnFlags,SENDIT) = SENDIT THEN
				IF .lSendInternalEmailIsOn THEN
					.cBodyText = .cBodyText + tcExpression + CRLF + CRLF
				ENDIF
			ENDIF
		ENDWITH
	ENDPROC  &&  TrackProgress
	
	
	
	PROCEDURE ForAlBenki
	
		LOCAL lcMonth
	
		SELECT A.WO_DATE, SPACE(6) AS WO_MONTH, A.VESSEL, A.CONTAINER, A.ACCTNAME, A.ETA   ;
		FROM F:\WO\WODATA\WOLOG.DBF A ;
		INTO CURSOR CURDO ;
		WHERE BETWEEN(A.WO_DATE,{^2014-07-01},{^2014-12-31}) ;
		AND A.OFFICE = 'C' ;
		AND NOT EMPTY(VESSEL) ;
		AND NOT EMPTY(CONTAINER) ;
		ORDER BY 1, 2, 3 ;
		READWRITE

		SELECT CURDO
		SCAN
			lcMonth = PADL(MONTH(CURDO.WO_DATE),2,'0') + STR(YEAR(CURDO.WO_DATE),4,0)
			REPLACE CURDO.WO_MONTH WITH lcMonth IN CURDO		
		ENDSCAN
		
		SELECT CURDO
		COPY TO F:\UTIL\TRUCKING\FOR_AL_BENKI\CONTAINER_INFO_2014.XLS XL5
		
		CLOSE DATABASES ALL
		
		
		SELECT A.WO_DATE, SPACE(6) AS WO_MONTH, A.VESSEL, A.CONTAINER, A.ACCTNAME, A.ETA   ;
		FROM F:\WO\WODATA\WOLOG.DBF A ;
		INTO CURSOR CURDO ;
		WHERE BETWEEN(A.WO_DATE,{^2015-01-01},{^2015-05-31}) ;
		AND A.OFFICE = 'C' ;
		AND NOT EMPTY(VESSEL) ;
		AND NOT EMPTY(CONTAINER) ;
		ORDER BY 1, 2, 3 ;
		READWRITE

		SELECT CURDO
		SCAN
			lcMonth = PADL(MONTH(CURDO.WO_DATE),2,'0') + STR(YEAR(CURDO.WO_DATE),4,0)
			REPLACE CURDO.WO_MONTH WITH lcMonth IN CURDO		
		ENDSCAN
		
		SELECT CURDO
		COPY TO F:\UTIL\TRUCKING\FOR_AL_BENKI\CONTAINER_INFO_2015.XLS XL5
		
		CLOSE DATABASES ALL
	
		RETURN
		
	ENDPROC && ForAlBenki


ENDDEFINE
