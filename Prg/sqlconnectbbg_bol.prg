PARAMETERS nAcctNum,cCustName,cPPName,lUCC,cOffice,lSQL3,lMoret,lDoCartons,lDoScanpack
IF VARTYPE(cOffice) # "C"
	cOffice = "C"
ENDIF
csql = "tgfnjsql01"
cSQLPass = ""

DO m:\dev\prg\lookups
SELECT 0
USE F:\wh\sqlpassword
cSQLPass = ALLTRIM(sqlpassword.PASSWORD)
USE IN sqlpassword

SET ESCAPE ON
ON ESCAPE CANCEL
cFileOutName = "V"+TRIM(cPPName)+"pp"
nAcct = ALLTRIM(STR(nAcctNum))
IF USED("temp1")
	USE IN temp1
ENDIF

lookups()

lcDSNLess="driver=SQL Server;server=tgfnjsql01;uid=SA;pwd=&cSQLPass;DATABASE=PICKPACK"

nHandle=SQLSTRINGCONNECT(m.lcDSNLess,.T.)
SQLSETPROP(0,'DispLogin',3)
SQLSETPROP(0,"dispwarnings",.F.)
WAIT WINDOW "Now processing "+cCustName+" SQL view...please wait" NOWAIT
IF nHandle<1 && bailout
	SET STEP ON
	WAIT WINDOW "Could not make SQL connection" TIMEOUT 3
	cRetMsg = "NO SQL CONNECT"
	RETURN cRetMsg
	THROW
ENDIF

lAppend = .F.

SELECT sqlwobbg

*!* Scans through all WOs within the OUTSHIP BOL#

SCAN
	nWO_Num = sqlwobbg.wo_num
	nWo   = ALLTRIM(STR(nWO_Num))

*!* Changed select to drop accountid requirement, 12.30.2005
	IF (lTestinput OR DATETIME()<DATETIME(2017,10,23,23,50,00))

		WAIT WINDOW "SQL Records will be selected from LABELS" TIMEOUT 2
		lcQ1=[SELECT * FROM dbo.labels Labels]
		lcQ2 = [ WHERE Labels.wo_num = ]
		lcQ3 = " &nWo "
		IF lUCC
			lcQ6 = [order by Labels.ship_ref,Labels.outshipid,Labels.ucc]
		ELSE
			lcQ6 = [order by Labels.ship_ref,Labels.outshipid,Labels.CartonNum]
		ENDIF
		lcsql = lcQ1+lcQ2+lcQ3+lcQ6

	ELSE
		WAIT WINDOW "SQL Records will be selected from CARTONS" TIMEOUT 1
		
		if usesql()
			if lucc
				xorderby="order by ship_ref, outshipid, ucc"
			else
				xorderby="order by ship_ref, outshipid, cartonnum"
			endif
			
			if xsqlexec("select * from cartons where wo_num="+nwo+" and totqty>0 "+xorderby,cFileOutName,,"pickpack") = 0
				ASSERT .F. MESSAGE "At NO RECORDS statement in SQLConnect"
				WAIT WINDOW "No "+cCustName+" records found" TIMEOUT 2
				cRetMsg = "NO SQL CONNECT"
				RETURN cRetMsg
			endif

		else
			lcQ1 = [SELECT * FROM dbo.cartons Cartons]
			lcQ2 = [ WHERE Cartons.wo_num = ]
			lcQ3 = " &nWo "
			lcQ4 = [ AND  Cartons.totqty > ]
			lcQ5 = [ 0 ]
			DO CASE
				CASE nAcctNum = 6532
					lcQ6 = [order by Cartons.ship_ref,Cartons.outshipid,Cartons.ucc,Cartons.outdetid]
				CASE lUCC
					lcQ6 = [order by Cartons.ship_ref,Cartons.outshipid,Cartons.ucc]
				OTHERWISE
					lcQ6 = [order by Cartons.ship_ref,Cartons.outshipid,Cartons.CartonNum]
			ENDCASE

			lcsql = lcQ1+lcQ2+lcQ3+lcQ4+lcQ5+lcQ6
		endif
	ENDIF

	if usesql()
	else
		llSuccess=SQLEXEC(nHandle,lcsql,cFileOutName)

		IF llSuccess=0  && no records 0 indicates no records and 1 indicates records found
			ASSERT .F. MESSAGE "At NO RECORDS statement in SQLConnect"
			WAIT WINDOW "No "+cCustName+" records found" TIMEOUT 2
			cRetMsg = "NO SQL CONNECT"
			RETURN cRetMsg
		ENDIF
		SQLCANCEL(nHandle)
	endif

	IF lAppend = .F.
		lAppend = .T.
		SELECT &cFileOutName
		IF lTesting
*!*			ASSERT .f. MESSAGE "In SQLConnect, browsing 'V' file"
*!*			BROWSE
		ENDIF
		COPY TO ("F:\3pl\DATA\temp1")
		USE ("F:\3pl\DATA\temp1") IN 0 ALIAS temp1
	ELSE
		SELECT &cFileOutName
		SCAN
			SCATTER MEMVAR
			INSERT INTO temp1 FROM MEMVAR
		ENDSCAN
	ENDIF
ENDSCAN
IF USED(cFileOutName)
	USE IN &cFileOutName
ENDIF

SELECT temp1
LOCATE
IF nAcctNum = 6521
	IF lTestSplits
		REPLACE temp1.ship_ref WITH STRTRAN(temp1.ship_ref,"~","%") ALL IN temp1
	ENDIF
ENDIF

IF lUCC
	SELECT * FROM temp1 ;
		ORDER BY ship_ref,outshipid,ucc ;
		INTO CURSOR &cFileOutName READWRITE
ELSE
	SELECT * FROM temp1 ;
		ORDER BY ship_ref,outshipid,cartonnum ;
		INTO CURSOR &cFileOutName READWRITE
ENDIF
IF lTesting
	SELECT &cFileOutName
*BROWSE
ENDIF

USE IN temp1
USE IN sqlwobbg
GO BOTT
WAIT CLEAR
*WAIT "" TIMEOUT 3
SQLDISCONNECT(nHandle)

cRetMsg = "OK"
RETURN cRetMsg