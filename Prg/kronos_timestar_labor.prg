*  a process which can support multiple reports asking for hours and $ broken down in various ways


* added Kronos-TimeStar Labor Reports for Steve Sykes
*
* To be run early each month against prior month.
*
* EXEs = F:\UTIL\KRONOS\KRONOS_TIMESTAR_LABOR.EXE

LPARAMETERS tcMode

LOCAL loKRONOS_TIMESTAR_LABOR, lcMode

runack("KRONOS_TIMESTAR_LABOR")

utilsetup("KRONOS_TIMESTAR_LABOR")

IF EMPTY(tcMode) THEN
	lcMode = "BASE"
ELSE
	lcMode = UPPER(ALLTRIM(tcMode))
ENDIF


loKRONOS_TIMESTAR_LABOR = CREATEOBJECT('KRONOS_TIMESTAR_LABOR')
loKRONOS_TIMESTAR_LABOR.MAIN( lcMode )


schedupdate()

CLOSE DATABASES ALL
RETURN

#DEFINE LOGIT 1
#DEFINE WAITIT 2
#DEFINE NOWAITIT 4
#DEFINE SENDIT 8
#DEFINE CRLF CHR(13) + CHR(10)
#DEFINE LF CHR(10)
#DEFINE RETURN_DATA_MANDATORY .F.
#DEFINE RETURN_DATA_NOT_MANDATORY .T.
#DEFINE DAILY_OT_MULTIPLIER 1.5
#DEFINE DAILY_OT20_MULTIPLIER 2.0
#DEFINE xlEdgeBottom 9
#DEFINE xlContinuous 1
#DEFINE xlMedium -4138

DEFINE CLASS KRONOS_TIMESTAR_LABOR AS CUSTOM

	cProcessName = 'KRONOS_TIMESTAR_LABOR'

	lTestMode = .F.  && .F. FOR NORMAL OPERATION, .T. TO SUPPRESS ALL BUT DEVELOPER EMAILS AND CHANGE INPUT TABLE, ETC.

	lAutoYield = .T.

	* processing properties
	cMode = ''


	* connection properties
	nSQLHandle = 0

	* file properties
	nFileHandle = 0

	* date properties
	dtNow = DATETIME()

	dToday = DATE()
	* to force different pay weeks
	*dToday = {^2015-04-01}

	cToday = DTOC(DATE())

	cStartTime = TTOC(DATETIME())

	* wait window properties
	nWaitWindowTimeout = 2
	lWaitWindowIsOn = .T.

	* logfile properties
	lLoggingIsOn = .T.
	cLogFile = 'F:\UTIL\KRONOS\Logfiles\KRONOS_TIMESTAR_LABOR_log.txt'

	* INTERNAL email properties
	lSendInternalEmailIsOn = .T.
	cFrom = 'mark.bennett@tollgroup.com'
	cSendTo = 'Steven.Sykes@Tollgroup.com'
	cCC = 'mbennett@fmiint.com'
	cSubject = 'Kronos-TimeStar Labor Report'
	cAttach = ''
	cBodyText = ''

	cCOMPUTERNAME = ''
	cUSERNAME = ''

	FUNCTION INIT
		IF NOT DODEFAULT()
			RETURN .F.
		ENDIF
		WITH THIS
			CLEAR
			*SET RESOURCE OFF
			CLOSE DATA
			SET DECIMALS TO 0
			SET CENTURY ON
			SET DATE AMERICAN
			SET HOURS TO 24
			SET ANSI ON
			SET TALK OFF
			SET DELETED ON
			SET CONSOLE OFF
			SET EXCLUSIVE OFF
			SET SAFETY OFF
			SET EXACT OFF
			SET STATUS BAR ON
			SET SYSMENU OFF
			SET ENGINEBEHAVIOR 70
			_VFP.AUTOYIELD = .lAutoYield
			.cCOMPUTERNAME = GETENV("COMPUTERNAME")
			.cUSERNAME = GETENV("USERNAME")
			IF .lTestMode THEN
				.cSendTo = 'mbennett@fmiint.com'
				.cLogFile = 'F:\UTIL\KRONOS\Logfiles\KRONOS_TIMESTAR_LABOR_log_TESTMODE.txt'
			ENDIF
			.lLoggingIsOn = .lLoggingIsOn AND NOT EMPTY(.cLogFile)
			IF .lLoggingIsOn THEN
				SET ALTERNATE TO (.cLogFile) ADDITIVE
				SET ALTERNATE ON
			ENDIF
		ENDWITH
	ENDFUNC


	FUNCTION DESTROY
		WITH THIS
			IF .lLoggingIsOn  THEN
				SET ALTERNATE OFF
				SET ALTERNATE TO
			ENDIF
		ENDWITH
		DODEFAULT()
	ENDFUNC


	FUNCTION MAIN
		LPARAMETERS tcMode
		WITH THIS
			LOCAL lnNumberOfErrors, lcSQL, ldToday, ldFromDate, ldToDate, lcSQLFromDate, lcSQLToDate
			LOCAL lcMonthYear, lcOutputFileBase, lcMonthYear3, lcMonthYear2
			LOCAL lnWageAmt, lnPayTypeFactor, lnMUFactor, lcSpecialWhere, lcMJ_TMPSummaryByAssociate, lcMJ_TGFSummaryByEmployee

			TRY
				lnNumberOfErrors = 0

				.TrackProgress('Kronos-TimeStar Labor Report process started....', LOGIT+SENDIT+NOWAITIT)
				.TrackProgress('COMPUTERNAME = ' + .cCOMPUTERNAME, LOGIT+SENDIT)
				.TrackProgress('USERNAME = ' + .cUSERNAME, LOGIT+SENDIT)
				.TrackProgress('PROJECT = KRONOS_TIMESTAR_LABOR', LOGIT+SENDIT)
				.TrackProgress('MODE = ' + tcMode, LOGIT+SENDIT)
				IF .lTestMode THEN
					.TrackProgress('                        TEST MODE                        ', LOGIT+SENDIT)
				ENDIF

				IF EMPTY(tcMode) OR (NOT INLIST(tcMode,"BASE","CTT_SSYKES")) THEN
					.TrackProgress('Missing or Invalid tcMode = ' + TRANSFORM(tcMode), LOGIT+SENDIT)
					THROW
				ELSE
					.cMode = tcMode
				ENDIF

				ldToday = .dToday
				ldToDate = ldToday - DAY(ldToday)  && this is last day of prior month
				ldFromDate = GOMONTH(ldToDate + 1,-1)

*!*					*!*	**** to force other date range	**************************************************
*!*					ldFromDate = {^2015-01-01}
*!*					ldToDate   = {^2015-01-31}
*!*					*!*	**********************************************************************************


				lcMonthYear = PROPER(CMONTH(ldFromDate)) + " " + TRANSFORM(YEAR(ldFromDate))
				lcMonthYear2 = TRANSFORM(ldFromDate) + " - " + TRANSFORM(ldToDate)
				lcMonthYear3 = DTOS(ldFromDate) + "-" + DTOS(ldToDate)

				.cSubject = 'Kronos-TimeStar Labor Report for: ' + lcMonthYear2
				lcOutputFileBase = "F:\UTIL\KRONOS\LABORREPORTS\BASE_" + lcMonthYear3 + ".CSV"
				lcOutputFile2 = "F:\UTIL\KRONOS\LABORREPORTS\" + .cMode + "_" + lcMonthYear3 + ".CSV"

				lcMJ_TMPSummaryByAssociate = "F:\UTIL\KRONOS\LABORREPORTS\MJ_TMP_SMRY_BY_ASSOCIATE_" + lcMonthYear3 + ".CSV"
				lcMJ_TGFSummaryByEmployee  = "F:\UTIL\KRONOS\LABORREPORTS\MJ_TGF_SMRY_BY_EMPLOYEE_" + lcMonthYear3 + ".CSV"

				lcSQLFromDate = .GetSQLStartDateTimeString( ldFromDate )
				lcSQLToDate = .GetSQLEndDateTimeString( ldToDate )

				IF USED('CURWAGE') THEN
					USE IN CURWAGE
				ENDIF
				IF USED('CURFINAL') THEN
					USE IN CURFINAL
				ENDIF
				IF USED('CURWTKHOURSPRE') THEN
					USE IN CURWTKHOURSPRE
				ENDIF
				IF USED('CURTIMEDATA') THEN
					USE IN CURTIMEDATA
				ENDIF


				****** NOTE: only getting TMP below - would need to add SXI back in if run for earlier than 2014
				
				lcSpecialWhere = " "
				*lcSpecialWhere = " AND (D.LABORLEV4NM IN ('NJ1','NJ2')) "

				SET TEXTMERGE ON

				TEXT TO	lcSQL NOSHOW


SELECT
A.EMPLOYEEID,
C.PERSONNUM AS FILE_NUM,
D.LABORLEV1NM AS ADP_COMP,
D.LABORLEV2NM AS DIVISION,
D.LABORLEV3NM AS DEPT,
D.LABORLEV3DSC AS DEPTNAME,
D.LABORLEV4NM AS WORKSITE,
C.FULLNM AS EMPLOYEE,
A.APPLYDTM,
E.NAME AS PAYCODE,
(A.DURATIONSECSQTY / 3600.00) AS TOTHOURS,
0000.00 AS HOURLYRATE,
1.00 AS MUFACTOR,
1.00 AS PTFACTOR,
000000.00 AS WAGEAMT
FROM WFCTOTAL A
JOIN WTKEMPLOYEE B
ON B.EMPLOYEEID = A.EMPLOYEEID
JOIN PERSON C
ON C.PERSONID = B.PERSONID
JOIN LABORACCT D
ON D.LABORACCTID = A.LABORACCTID
JOIN PAYCODE E
ON E.PAYCODEID = A.PAYCODEID
WHERE (A.APPLYDTM >= <<lcSQLFromDate>>)
AND (A.APPLYDTM <= <<lcSQLToDate>>)
AND (D.LABORLEV1NM IN ('TMP'))
AND E.NAME IN ('Regular Hours','Shift Diff 1 Hours','Overtime Hours','Doubltime Hours','Guaranteed Hours')
<<lcSpecialWhere>>
ORDER BY C.FULLNM, A.APPLYDTM, E.NAME

				ENDTEXT

				SET TEXTMERGE OFF


*AND (D.LABORLEV1NM IN ('TMP'))

				*!*	AND (D.LABORLEV1NM IN ('SXI','TMP'))
				*!*	AND (D.LABORLEV4NM IN ('NJ1','NJ2'))
				*!*	AND E.NAME IN ('Regular Hours','Overtime Hours','Guaranteed Hours')



*****====> Wage select below doesn't work well when looking at monthly time periods; it will give 0 wage for someone hired during the month in question.
*****====> Changed to use TO DATE  7/10/2015 MB

*!*					SET TEXTMERGE ON

*!*					TEXT TO	lcSQLWage NOSHOW

*!*	SELECT
*!*	EMPLOYEEID,
*!*	BASEWAGEHOURLYAMT AS WAGE,
*!*	EFFECTIVEDTM,
*!*	EXPIRATIONDTM
*!*	FROM BASEWAGERTHIST
*!*	WHERE <<lcSQLFromDate>> BETWEEN EFFECTIVEDTM AND EXPIRATIONDTM
*!*	ORDER BY EMPLOYEEID DESC, EXPIRATIONDTM DESC

*!*					ENDTEXT

*!*					SET TEXTMERGE OFF


				SET TEXTMERGE ON

				TEXT TO	lcSQLWage NOSHOW

SELECT
EMPLOYEEID,
BASEWAGEHOURLYAMT AS WAGE,
EFFECTIVEDTM,
EXPIRATIONDTM
FROM BASEWAGERTHIST
WHERE <<lcSQLToDate>> BETWEEN EFFECTIVEDTM AND EXPIRATIONDTM
ORDER BY EMPLOYEEID DESC, EXPIRATIONDTM DESC

				ENDTEXT

				SET TEXTMERGE OFF


				IF .lTestMode THEN
					.TrackProgress('', LOGIT+SENDIT)
					.TrackProgress('lcSQL =' + lcSQL, LOGIT+SENDIT)
					.TrackProgress('lcSQLWage =' + lcSQLWage, LOGIT+SENDIT)
				ENDIF

				.nSQLHandle = SQLCONNECT("WTK2","sa","kr0n1tes")

				IF .nSQLHandle > 0 THEN
				
					WAIT WINDOW NOWAIT "Getting Temp Data from Kronos..."

					IF .ExecSQL(lcSQL, 'CURWTKHOURSPRE', RETURN_DATA_MANDATORY) AND ;
							.ExecSQL(lcSQLWage, 'CURWAGE', RETURN_DATA_MANDATORY) THEN

						*!*	SELECT CURWAGE
						*!*	BROWSE
						*!*	THROW

						* populate HOURLYRATE and wageamt in CURWTKHOURSPRE
						WAIT WINDOW NOWAIT "Populating HOURLYRATE and wageamt in Temp Data from Kronos..."

						SELECT CURWTKHOURSPRE
						SCAN
							SELECT CURWAGE
							LOCATE FOR EMPLOYEEID = CURWTKHOURSPRE.EMPLOYEEID
							IF FOUND() THEN
								REPLACE CURWTKHOURSPRE.HOURLYRATE WITH CURWAGE.WAGE IN CURWTKHOURSPRE
							ENDIF
						ENDSCAN

						SELECT CURWTKHOURSPRE
						SCAN
							lnMUFactor = IIF(ADP_COMP = 'TMP',1.31,1.00)
							lnPayTypeFactor = .GetPayTypeFactor(CURWTKHOURSPRE.PAYCODE)
							lnWageAmt = CURWTKHOURSPRE.HOURLYRATE * lnMUFactor * lnPayTypeFactor * CURWTKHOURSPRE.TOTHOURS
							REPLACE CURWTKHOURSPRE.WAGEAMT WITH lnWageAmt, ;
								CURWTKHOURSPRE.MUFACTOR WITH lnMUFactor ;
								CURWTKHOURSPRE.PTFACTOR WITH lnPayTypeFactor ;
								IN CURWTKHOURSPRE
						ENDSCAN


						*!*	SELECT CURWTKHOURSPRE
						*!*	BROWSE



						* get SXI data from Insperity
						WAIT WINDOW NOWAIT "Getting Hourly Data from TimeStar..."
						
						USE F:\UTIL\TIMESTAR\DATA\TIMEDATA.DBF IN 0 ALIAS TIMEDATA

						IF USED('CURTIMEDATA') THEN
							USE IN CURTIMEDATA
						ENDIF

						SELECT ;
							ADP_COMP, ;
							DIVISION, ;
							DEPT, ;
							'                         ' AS DEPTNAME, ;
							WORKSITE, ;
							FILE_NUM, ;
							NAME AS EMPLOYEE, ;
							WORKDATE AS APPLYDTM, ;
							PAY_TYPE AS PAYCODE, ;
							TOTHOURS, ;
							0000.00 AS HOURLYRATE, ;
							1.00 AS MUFACTOR, ;
							1.00 AS PTFACTOR, ;
							000000.00 AS WAGEAMT ;
							FROM TIMEDATA ;
							INTO CURSOR CURTIMEDATA ;
							WHERE (WORKDATE >= ldFromDate) ;
							AND (WORKDATE <= ldToDate) ;
							AND (ADP_COMP = 'E87') ;
							AND ALLTRIM(PAY_TYPE) IN ('DOUBLE','REGULAR','OVERTIME','GUARANTEE','WRKDHOL') ;
							ORDER BY NAME, WORKDATE, PAY_TYPE ;
							READWRITE

						* populate HOURLYRATE and wageamt in CURTIMEDATA
						WAIT WINDOW NOWAIT "Populating HOURLYRATE and wageamt in Hourly Data from TimeStar..."
						
						USE F:\UTIL\TIMESTAR\DATA\EEINFO.DBF IN 0 ALIAS EEINFO SHARED

						SELECT CURTIMEDATA
						SCAN
							SELECT EEINFO
							LOCATE FOR INSPID = VAL(CURTIMEDATA.FILE_NUM)
							IF FOUND() THEN
								REPLACE CURTIMEDATA.HOURLYRATE WITH EEINFO.HOURLYRATE IN CURTIMEDATA
							ENDIF
						ENDSCAN

						SELECT CURTIMEDATA
						SCAN
							lnPayTypeFactor = .GetPayTypeFactor(CURTIMEDATA.PAYCODE)
							lnWageAmt = CURTIMEDATA.HOURLYRATE * lnPayTypeFactor * CURTIMEDATA.TOTHOURS
							REPLACE CURTIMEDATA.WAGEAMT WITH lnWageAmt, ;
								CURTIMEDATA.PTFACTOR WITH lnPayTypeFactor ;
								IN CURTIMEDATA
						ENDSCAN

						IF USED('EEINFO') THEN
							USE IN EEINFO
						ENDIF


						*!*	SELECT CURTIMEDATA
						*!*	BROWSE FOR PAYCODE = 'DOUBLE'
						*!*	THROW


						* populate dept desc in CURTIMEDATA
						USE F:\UTIL\ADPREPORTS\DATA\DEPTTRANS.DBF IN 0 ALIAS DEPTTRANS

						SELECT CURTIMEDATA
						SCAN
							SELECT DEPTTRANS
							LOCATE FOR DEPT = CURTIMEDATA.DEPT
							IF FOUND() THEN
								REPLACE CURTIMEDATA.DEPTNAME WITH DEPTTRANS.DESC IN CURTIMEDATA
							ENDIF
						ENDSCAN

						IF USED('DEPTTRANS') THEN
							USE IN DEPTTRANS
						ENDIF
						
						
						*******************************************************************
						*******************************************************************
						
						* MAKE MJ SUMMARY-BY-EMPLOYEE SPREADSHEETS FOR STEVE SYKES.
						
						* TMPS
						SELECT DIVISION, EMPLOYEE AS ASSOCIATE, DEPTNAME, SUM(TOTHOURS) AS TOTHOURS, MAX(HOURLYRATE)AS HOURLYRATE, SUM(WAGEAMT) AS WAGEAMT ;
						FROM CURWTKHOURSPRE ;
						INTO CURSOR CURMJTEMPS ;
						WHERE DIVISION IN ('04','14') ;
						GROUP BY DIVISION, EMPLOYEE, DEPTNAME ;
						ORDER BY DIVISION, EMPLOYEE, DEPTNAME
						
						SELECT CURMJTEMPS
						COPY TO (lcMJ_TMPSummaryByAssociate) CSV
						
						* TMPS
						SELECT DIVISION, EMPLOYEE, DEPTNAME, SUM(TOTHOURS) AS TOTHOURS, MAX(HOURLYRATE)AS HOURLYRATE, SUM(WAGEAMT) AS WAGEAMT ;
						FROM CURTIMEDATA ;
						INTO CURSOR CURMJEES ;
						WHERE DIVISION IN ('04','14') ;
						GROUP BY DIVISION, EMPLOYEE, DEPTNAME ;
						ORDER BY DIVISION, EMPLOYEE, DEPTNAME
						
						SELECT CURMJEES
						COPY TO (lcMJ_TGFSummaryByEmployee) CSV				
						
						*******************************************************************
						*******************************************************************
						
						

						* add CURTIMEDATA to CURWTKHOURSPRE
						SELECT CURWTKHOURSPRE
						APPEND FROM DBF('CURTIMEDATA')

						SELECT CURWTKHOURSPRE
						COPY TO (lcOutputFileBase) CSV
						*COPY TO (lcOutputFileBase) XL5 FOR dept = '0646'
						
						
						.TrackProgress('Output to spreadsheet: ' + lcOutputFileBase, LOGIT+SENDIT+NOWAITIT)

						WAIT CLEAR
						
*!*	SELECT CURWTKHOURSPRE
*!*	BROW
*!*	THROW
						DO CASE
							CASE .cMode = "BASE"

								IF FILE(lcOutputFileBase) THEN
									* attach output file to email
									.cAttach = lcOutputFileBase
								ELSE
									.TrackProgress('ERROR: unable to email spreadsheet: ' + lcOutputFileBase, LOGIT+SENDIT+NOWAITIT)
								ENDIF

							CASE .cMode = "CTT_SSYKES"
							
								SELECT PADR(lcMonthYear2,30) AS DATERANGE, ;
									ADP_COMP, ;
									DIVISION, ;
									DEPT, ;
									DEPTNAME, ;
									WORKSITE, ;
									SUM(TOTHOURS) AS TOTHOURS, ;
									SUM(WAGEAMT) AS TOTWAGES ;
									FROM CURWTKHOURSPRE ;
									INTO CURSOR CURFINAL ;
									WHERE WORKSITE IN ('NJ1','NJ2') ;
									GROUP BY 1, 2, 3, 4, 5, 6 ;
									ORDER BY 4, 3 ;
									READWRITE

								SELECT CURFINAL
								COPY TO (lcOutputFile2) CSV
								.TrackProgress('Output to spreadsheet: ' + lcOutputFile2, LOGIT+SENDIT+NOWAITIT)

								IF FILE(lcOutputFileBase) AND FILE(lcOutputFile2) THEN
									* attach output file to email
									*.cAttach = lcOutputFile2
									.cAttach = lcOutputFileBase
									.cAttach = .cAttach + "," + lcOutputFile2
								ELSE
									.TrackProgress('ERROR: unable to email spreadsheets: ' + lcOutputFileBase + ', ' + lcOutputFile2, LOGIT+SENDIT+NOWAITIT)
								ENDIF
							

							OTHERWISE
								* nothing
						ENDCASE

						.TrackProgress('Kronos-TimeStar Labor Report process ended normally.', LOGIT+SENDIT)


					ENDIF  &&  .ExecSQL(lcSQL, 'CURWTKEMPLOYEE', RETURN_DATA_MANDATORY)

					CLOSE DATABASES ALL


				ELSE
					* connection error
					.TrackProgress('Unable to connect to WTK System.', LOGIT+SENDIT)
				ENDIF   &&  .nSQLHandle > 0

			CATCH TO loError

				.TrackProgress('There was an error.',LOGIT+SENDIT)
				.TrackProgress('The Error # is: ' + TRANSFORM(loError.ERRORNO), LOGIT+SENDIT)
				.TrackProgress('The Error Message is: ' + TRANSFORM(loError.MESSAGE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred in Program: ' + TRANSFORM(loError.PROCEDURE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred at line #: ' + TRANSFORM(loError.LINENO), LOGIT+SENDIT) 
				lnNumberOfErrors = lnNumberOfErrors + 1
				CLOSE DATABASES ALL

			ENDTRY

			CLOSE DATA
			WAIT CLEAR
			***************** INTERNAL email results ******************************
			.TrackProgress('About to send status email.',LOGIT)
			.TrackProgress('==================================================================================================================', SENDIT)
			.TrackProgress('The logfile is: ' + .cLogFile, SENDIT)
			.TrackProgress('Kronos-TimeStar Labor Report process started: ' + .cStartTime, LOGIT+SENDIT)
			.TrackProgress('Kronos-TimeStar Labor Report process finished: ' + TTOC(DATETIME()), LOGIT+SENDIT)

			IF .lSendInternalEmailIsOn THEN
				* try to trap error from not having dartmail dll's registered on user's pc...
				TRY
					DO FORM dartmail2 WITH .cSendTo,.cFrom,.cSubject,.cCC,.cAttach,.cBodyText,"A"
					.TrackProgress('Sent status email.',LOGIT)
				CATCH TO loError
					.TrackProgress('There was an error sending the status email.',LOGIT)
					.TrackProgress('The Error # is: ' + TRANSFORM(loError.ERRORNO), LOGIT)
					.TrackProgress('The Error Message is: ' + TRANSFORM(loError.MESSAGE), LOGIT)
					.TrackProgress('The Error occurred in Program: ' + TRANSFORM(loError.PROCEDURE), LOGIT)
					.TrackProgress('The Error occurred at line #: ' + TRANSFORM(loError.LINENO), LOGIT)
					lnNumberOfErrors = lnNumberOfErrors + 1
					CLOSE DATA
				ENDTRY

			ELSE
				.TrackProgress('Did not send status email: THIS.lSendInternalEmailIsOn = FALSE.',LOGIT)
			ENDIF

		ENDWITH
		RETURN
	ENDFUNC && main


	FUNCTION GetPayTypeFactor
		LPARAMETERS tcPayCode
		LOCAL lcPayCode, lnPayTypeFactor
		lcPayCode = UPPER(ALLTRIM(tcPayCode))
		DO CASE
			CASE INLIST(lcPayCode,'OVERTIME HOURS','OVERTIME','WRKDHOL')
				lnPayTypeFactor = 1.50
			CASE INLIST(lcPayCode,'DOUBLTIME HOURS','DOUBLE')
				lnPayTypeFactor = 2.00
			OTHERWISE
				lnPayTypeFactor = 1.00
		ENDCASE
		RETURN lnPayTypeFactor
	ENDFUNC


	FUNCTION GetSQLStartDateTimeString
		* returns a timestamp string for use in SQL queries
		LPARAMETERS tdDate
		LOCAL lcSetDate, lcSQLDateTimeString
		lcSetDate = SET('DATE')
		SET DATE YMD
		lcSQLDateTimeString   = "{ts '" + TRANSFORM(INT(YEAR(tdDate))) + "-" + PADL(MONTH(tdDate),2,"0") + "-" + PADL(DAY(tdDate),2,"0") + " 00:00:00.0'}"
		SET DATE &lcSetDate.
		RETURN lcSQLDateTimeString
	ENDFUNC


	FUNCTION GetSQLEndDateTimeString
		* returns a timestamp string for use in SQL queries
		LPARAMETERS tdDate
		LOCAL lcSetDate, lcSQLDateTimeString
		lcSetDate = SET('DATE')
		SET DATE YMD
		lcSQLDateTimeString = "{ts '" + TRANSFORM(INT(YEAR(tdDate))) + "-" + PADL(MONTH(tdDate),2,"0") + "-" + PADL(DAY(tdDate),2,"0") + " 23:59:59.9'}"
		SET DATE &lcSetDate.
		RETURN lcSQLDateTimeString
	ENDFUNC


	FUNCTION ExecSQL
		LPARAMETERS tcSQL, tcCursorName, tlNoDataReturnedIsOkay
		LOCAL llRetval, lnResult
		WITH THIS
			* close target cursor if it's open
			IF USED(tcCursorName)
				USE IN (tcCursorName)
			ENDIF
			WAIT WINDOW "Fetching remote data; please wait..." NOWAIT
			lnResult = SQLEXEC(.nSQLHandle, tcSQL, tcCursorName)
			llRetval = ( lnResult > 0 )
			IF llRetval THEN
				* see if any data came back
				IF NOT tlNoDataReturnedIsOkay THEN
					IF (NOT USED(tcCursorName)) OR EOF(tcCursorName) THEN
						llRetval = .F.
						.TrackProgress('No data was returned by this query: ' +CRLF + tcSQL, LOGIT+SENDIT)
					ENDIF
				ENDIF
			ELSE
				.TrackProgress("Result Code [" + TRANSFORM(lnResult) + "] returned by this query: " + tcSQL, LOGIT+SENDIT)
			ENDIF
			WAIT CLEAR
			RETURN llRetval
		ENDWITH
	ENDFUNC


	PROCEDURE TrackProgress
		* do any combination of Wait Window, writing to logfile, and adding to body of email,
		* based on nFlags parameter.
		LPARAMETERS tcExpression, tnFlags
		WITH THIS
			IF BITAND(tnFlags,LOGIT) = LOGIT THEN
				IF .lLoggingIsOn THEN
					?
					? .cProcessName + "  " + TTOC(DATETIME()) + ": " + tcExpression
				ENDIF
			ENDIF
			IF .lWaitWindowIsOn THEN
				IF BITAND(tnFlags,NOWAITIT) = NOWAITIT THEN
					WAIT WINDOW tcExpression NOWAIT
				ENDIF
				IF BITAND(tnFlags,WAITIT) = WAITIT THEN
					WAIT WINDOW tcExpression TIMEOUT .nWaitWindowTimeout
				ENDIF
			ENDIF
			IF BITAND(tnFlags,SENDIT) = SENDIT THEN
				IF .lSendInternalEmailIsOn THEN
					.cBodyText = .cBodyText + tcExpression + CRLF + CRLF
				ENDIF
			ENDIF
		ENDWITH
	ENDPROC  &&  TrackProgress


ENDDEFINE
