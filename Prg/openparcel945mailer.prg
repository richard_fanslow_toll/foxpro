CLOSE DATA ALL
USE F:\3pl\DATA\trigger945accts IN 0 ALIAS trigaccts
lTestThis = .F.

USE F:\3pl\DATA\parcel_carriers IN 0 ALIAS parcels ORDER TAG scac NOUPDATE


FOR i = 1 TO 14
	DO CASE
		CASE i = 1
			cWhse1 = "1"
			cMod = "SP MOD A"
			cMsg= "Open Parcel 945 Tracker, SP MOD A"
		CASE i = 2
			cWhse1 = "2"
			cMod = "SP MOD D"
			cMsg= "Open Parcel 945 Tracker, SP MOD D"
		CASE i = 3
			cWhse1 = "5"
			cMod = "SP MOD C"
			cMsg= "Open Parcel 945 Tracker, SP MOD C"
		CASE i = 4
			cWhse1 = "6"
			cMod = "SP MOD B"
			cMsg= "Open Parcel 945 Tracker, SP MOD B"
		CASE i = 5 && Moret SP
			cWhse1 = "7"
			cMod = "SP MOD E"
			cMsg= "Open Parcel 945 Tracker, SP MOD E"
		CASE i = 6 && Mira Loma
			cWhse1 = "L"
			cMod = "MIRA LOMA"
			cMsg= "Open Parcel 945 Tracker, ML"
		CASE i = 7 && Carson
			cWhse1 = "X"
			cMod = "CARSON"
			cMsg= "Open Parcel 945 Tracker, CR"
		CASE i = 8
			cWhse1 = "M"
			cMod = "FLORIDA"
			cMsg= "Open Parcel 945 Tracker, FL"
		CASE i = 9
			cWhse1 = "I"
			cMod = "NJ-I"
			cMsg= "Open Parcel 945 Tracker, NJ MOD I"
		CASE i = 10 && NJ Marc Jacobs
			cWhse1 = "J"
			cMod = "NJ-J"
			cMsg= "Open Parcel 945 Tracker, NJ MOD J"
		CASE i = 11 && Ariat - Louisville
			cWhse1 = "K"
			cMod = "KY-K"
			cMsg= "Open Parcel 945 Tracker, KY MOD K"
		CASE i = 12 && Steel Series		
			cWhse1 = "S"
			cMod = "KY-S"
			cMsg= "Open Parcel 945 Tracker, KY MOD S"
		CASE i = 13 && Original Carson
			cWhse1 = "X"
			cMod = "CARSON-OTHER"
			cMsg= "Open Parcel 945 Tracker, CARSON-OTHER"
		CASE i = 14 && Carson 2
			cWhse1 = "Y"
			cMod = "CARSON 2"
			cMsg= "Open Parcel 945 Tracker, CARSON 2"
		CASE i = 15 && Toronto, Canada
			cWhse1 = "Z"
			cMod = "TORONTO"
			cMsg= "Open Parcel 945 Tracker, TORONTO"
	ENDCASE

	gOffice = cWhse1
	WAIT WINDOW cMsg NOWAIT
	IF USED('outship')
		USE IN outship
	ENDIF

	xsqlexec("select * from outship where mod = '"+cWhse1+"' and del_date > {"+DTOC(DATE()-7)+"}",,,"wh")

	IF i = 1
		SELECT accountid,("WH"+cWhse1) AS whse,ship_ref,del_date,consignee,bol_no,scac ;
			FROM outship ;
			WHERE outship.del_date > DATE()-3 ;
			AND !EMPTY(outship.del_date) ;
			AND EMPTY(outship.bol_no) ;
			AND !("SAMPLE "$outship.consignee) ;
			AND !("SAMPLES"$outship.consignee) ;
			AND !("SAMPLE"$outship.ship_ref) ;
			AND !("DAMAGE"$outship.ship_ref) ;
			INTO CURSOR tempos1 READWRITE
	ELSE
		SELECT 0
		SELECT accountid,("WH"+cWhse1) AS whse,ship_ref,del_date,consignee,bol_no,scac ;
			FROM outship ;
			WHERE outship.del_date > DATE()-3 ;
			AND !EMPTY(del_date) ;
			AND EMPTY(outship.bol_no) ;
			AND !("SAMPLE "$outship.consignee) ;
			AND !("SAMPLES"$outship.consignee) ;
			AND !("SAMPLE"$outship.ship_ref) ;
			AND !("DAMAGE"$outship.ship_ref) ;
			INTO CURSOR tempos2
		SELECT tempos1
		APPEND FROM DBF('tempos2')
	ENDIF

ENDFOR

IF lTestThis
	WAIT WINDOW "Now browsing parcel no-BOL selections" TIMEOUT 1
	BROW
ENDIF

SELECT tempos1
LOCATE
SCAN FOR !DELETED()
	SELECT trigaccts
	LOCATE FOR trigaccts.accountid = tempos1.accountid
	IF !FOUND()
		DELETE NEXT 1 IN tempos1
	ENDIF
ENDSCAN

LOCATE
SCAN FOR !DELETED()
	IF !SEEK(tempos1.scac,'parcels','scac')
		DELETE NEXT 1 IN tempos1
	ENDIF
ENDSCAN


SELECT tempos1
COUNT TO nRecTot FOR !DELETED()
IF nRecTot > 0
	WAIT WINDOW "There are parcel shipments which have open BOLs" TIMEOUT 3
	COPY TO h:\fox\missbols TYPE XL5
	SELECT 0
	USE F:\3pl\DATA\mailmaster ALIAS mm
	SELECT mm
	LOCATE FOR edi_type = "MISC" AND taskname = "GENERAL"
	tsubject = "Missing BOLs for Parcel 945, run  at "+TTOC(DATETIME())
	tattach  = "h:\fox\missbols.xls"
	tSendto = IIF(mm.Use_Alt,mm.sendtoalt,mm.sendto)
	tCC = IIF(mm.Use_Alt,mm.ccalt,mm.cc)
	tmessage =  "The attached file "
	DO FORM m:\dev\frm\dartmail2 WITH tSendto,tfrom,tsubject,tCC,tattach,tmessage,"A"
	DELETE FILE "h:\fox\missbols.xls"
ELSE
	WAIT WINDOW "All parcel shipments have triggered normally" TIMEOUT 2
ENDIF
CLOSE DATA ALL
RETURN
