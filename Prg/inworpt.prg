parameters xaccountid, xstartdt, xenddt, xtype, xdetail, xreference, xotherfilter
**PLEASE NOTE: any changes here need to be accounted for in webrpt!

if type("xreference")="L"
	xreference=""
endif

do case
case xaccountid=9999
	xfilter=".t."
case xaccountid>10000
	xfilter="inlist(inwolog.accountid,"
	select acctdet
	scan for acctgrpid=xaccountid-10000
		xfilter=xfilter+transform(acctdet.accountid)+","
	endscan
	xfilter=left(xfilter,len(xfilter)-1)+")"
otherwise
	xfilter="inwolog.accountid="+trans(xaccountid)
endcase

if empty(xotherfilter)
	xfilter2=".t."
else
	xfilter2=xotherfilter
endif

if !empty(xstartdt)
	**changed filter from wo_date to confirmdt - 02/19/09 mvw
	xfilter3="between(confirmdt,{"+dtoc(xstartdt)+"},{"+dtoc(xenddt+1)+"})"
else
	xfilter3=".t."
endif

if !empty(xreference)
	xfilter4='(reference = "'+xreference+'" or container = "'+xreference+'")'
else
	xfilter4=".t."
endif

if xdetail
	xpnpaccount=strtran(pnpacct(goffice),"accountid","xaccountid")		&& dy 1/31/12
	if &xpnpaccount
		xunitsfilter="units=.t."
	else
		xunitsfilter=".t."
	endif

	grptname="inwodet"
	
	xsqlexec("select inwolog.wo_num, confirmdt, inwolog.accountid, inwolog.acctname, " + ;
		"container, acct_ref, reference, pickupdt, offholddt, quantity, printcomments, " + ;
		"units, style, color, id, pack, totqty, echo " + ;
		"from inwolog, indet where inwolog.inwologid=indet.inwologid and inwolog."+gmodfilter+" " + ;
		"and "+xfilter+" and "+xfilter2+" and "+xfilter3+" and "+xfilter4+" and "+xunitsfilter+" " + ;
		"and inwolog.cyclecount=0 and inwolog.ppadj=0","xtemp",,"wh")

	select wo_num, ttod(confirmdt) as wo_date, accountid, acctname, container, ;
		acct_ref, reference, space(25) as reference2, pickupdt, offholddt, quantity, ;
		strtran(strtran(left(printcomments,80),chr(10)," "),chr(13)," ") as printcomments, ;
		style, color, id, pack, totqty, iif(units,"UNITS","CARTONS") as type, ;
		space(10) as styledesc, space(10) as colordesc, space(10) as iddesc,  0000 as numskus, echo ;
		from xtemp order by acctname, wo_num into cursor xrpt readwrite
	
	xwonum=0
	select xrpt
	scan 	
		if xaccountid=6182 and wo_num#xwonum
			replace reference2 with getmemodata("echo","VENDORNAME",,.T.)
		endif
		if wo_num=xwonum
			blank fields wo_date,container,quantity,printcomments,reference2 in xrpt
		endif
		xwonum=wo_num
		=seek(accountid,"account","accountid")
		replace styledesc with iif(!empty(account.styledesc),account.styledesc,"Style"), colordesc with account.colordesc, iddesc with account.iddesc in xrpt
	endscan

else
	grptname="inworpt"
	
	xsqlexec("select wo_num, confirmdt, accountid, acctname, sp, zreturns, plinqty, " + ;
		"container, acct_ref, reference, pickupdt, offholddt, quantity, printcomments, " + ;
		"returntostock, cyclecount, physinv, ppadj, transfer " + ;
		"from inwolog where "+gmodfilter+" and "+xfilter+" and "+xfilter2+" " + ;
		"and "+xfilter3+" and "+xfilter4+" and cyclecount=0 and ppadj=0","xtemp",,"wh")

	select wo_num, ttod(confirmdt) as wo_date, acctname, container, reference, ;
		plinqty, quantity, printcomments, offholddt, pickupdt, 0000 as numskus, sp, zreturns, ;
		returntostock, cyclecount, physinv, ppadj, transfer ;
		from xtemp order by wo_num into cursor xrpt readwrite
			
	scan 
		xsqlexec("select wo_num from indet where wo_num="+transform(xrpt.wo_num)+" and units=0",,,"wh")
		replace numskus with reccount("indet") in xrpt			
				
		do case
		case xtype=6
			replace printcomments with "PU: "+dtoc(xrpt.pickupdt)+"  "+xrpt.printcomments in xrpt
		case xtype=7
			replace printcomments with "ETA: "+dtoc(xrpt.offholddt)+"  "+xrpt.printcomments in xrpt
		endcase
	endscan
endif	

select xrpt

do case
case xtype=6
	index on dtos(pickupdt)+str(wo_num,7) tag zorder
case xtype=7
	index on dtos(offholddt)+str(wo_num,7) tag zorder
endcase
