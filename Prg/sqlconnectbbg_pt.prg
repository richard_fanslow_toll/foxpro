PARAMETERS nAcctNum,cCustName,cPPName,lUCC,cOffice

IF VARTYPE(cOffice) # "C"
	cOffice = "L"
ENDIF
cSQLPass = ""

SELECT 0
USE F:\wh\sqlpassword
cSQLPass = ALLTRIM(sqlpassword.PASSWORD)
USE IN sqlpassword

SET ESCAPE ON
ON ESCAPE CANCEL
cFileOutName = LOWER("v"+cCustName+"pp")
IF USED("temp1bbg")
	USE IN temp1bbg
ENDIF

lookups()

lAppend = .F.

SELECT sqlptbbg
LOCATE

SCAN
	lcDSNLess="driver=SQL Server;server=tgfnjsql01;uid=SA;pwd=&cSQLPass;DATABASE=PICKPACK"
	nHandle=SQLSTRINGCONNECT(m.lcDSNLess,.T.)
	SQLSETPROP(0,'DispLogin',3)
	SQLSETPROP(0,"dispwarnings",.F.)
	WAIT WINDOW "Now processing "+cCustName+" SQL view...please wait" NOWAIT
	IF nHandle<1 && bailout
		SET STEP ON
		WAIT WINDOW "Could not make SQL connection" TIMEOUT 3
		cRetMsg = "NO SQL CONNECT"
		RETURN cRetMsg
		THROW
	ENDIF


	nPT = "'"+ALLTRIM(sqlptbbg.ship_ref)+"'"
	lUseLabels = .f.
	IF lTestinput OR DATETIME()<DATETIME(2018,04,03,12,30,00)
		lUseLabels = .t.
		WAIT WINDOW "SQL Records will be selected from LABELS" TIMEOUT 2
		lcQ1=[SELECT * FROM dbo.labels Labels]
		lcQ2 = [ WHERE Labels.ship_ref == ]
		lcQ3 = " &nPT "
		lcQ4 = [ AND  Labels.totqty > ]
		lcQ5 = [ 0 ]
		lcQ6 = [ AND Labels.accountid in (]
		lcQ7 = gbillabongaccts+[) ]
		IF lUCC
			lcQ8 = [order by Labels.ship_ref,Labels.outshipid,Labels.ucc]
		ELSE
			lcQ8 = [order by Labels.ship_ref,Labels.outshipid,Labels.CartonNum]
		ENDIF
		lcsql = lcQ1+lcQ2+lcQ3+lcQ4+lcQ5+lcQ6+lcQ7+lcQ8

	ELSE
		WAIT WINDOW "SQL Records will be selected from CARTONS" TIMEOUT 1
		
		if usesql()
			if lucc
				xorderby="order by ship_ref, outshipid, ucc"
			else
				xorderby="order by ship_ref, outshipid, cartonnum"
			endif
			
			xsqlexec("select * from cartons where ship_ref='"+PADR(alltrim(sqlptbbg.ship_ref),20)+"' and totqty>0 and ucc <> 'CUTS' and inlist(accountid,"+gbillabongaccts+") "+xorderby,cFileOutName,,"pickpack")
			IF RECCOUNT() = 0
				ASSERT .F. MESSAGE "At NO RECORDS statement in SQLConnect"
				WAIT WINDOW "No "+cCustName+" records found" TIMEOUT 2
				cRetMsg = "NO SQL CONNECT"
				RETURN cRetMsg
			ENDIF
		else
			lcQ1 = [SELECT * FROM dbo.cartons Cartons]
			lcQ2 = [ WHERE Cartons.ship_ref = ]
			lcQ3 = " &nPT "
			lcQ4 = [ AND  Cartons.totqty > ]
			lcQ5 = [ 0 ]
			lcQ6 = [ AND Cartons.accountid in (]
			lcQ7 = gbillabongaccts+[) ]

			IF lUCC
				lcQ8 = [order by Cartons.ship_ref,Cartons.outshipid,Cartons.ucc]
			ELSE
				lcQ8 = [order by Cartons.ship_ref,Cartons.outshipid,Cartons.CartonNum]
			ENDIF
			lcsql = lcQ1+lcQ2+lcQ3+lcQ4+lcQ5+lcQ6+lcQ7+lcQ8
		endif
	ENDIF

	if usesql() AND !lUseLabels
	else
		llSuccess=SQLEXEC(nHandle,lcsql,cFileOutName)

		IF llSuccess < 1  && no records 0 indicates no records and 1 indicates records found
			ASSERT .F. MESSAGE "At NO RECORDS statement in SQLConnect"
			WAIT WINDOW "No "+cCustName+" records found" TIMEOUT 2
			cRetMsg = "NO SQL CONNECT"
			RETURN cRetMsg
		ENDIF
		SQLCANCEL(nHandle)
	endif
	
	IF lAppend = .F.
		lAppend = .T.
		SELECT &cFileOutName
		IF lTesting
*			BROWSE
		ENDIF
		COPY TO ("F:\3pl\DATA\temp1bbg") FOR !DELETED()
		USE ("F:\3pl\DATA\temp1bbg") IN 0 ALIAS temp1bbg
	ELSE
		SELECT &cFileOutName
		SCAN
			SCATTER MEMVAR
			INSERT INTO temp1bbg FROM MEMVAR
		ENDSCAN
	ENDIF
ENDSCAN

IF USED(cFileOutName)
	USE IN &cFileOutName
ENDIF

SELECT temp1bbg
LOCATE

IF lUCC
	SELECT * FROM temp1bbg ;
		ORDER BY ship_ref,outshipid,ucc ;
		INTO CURSOR &cFileOutName READWRITE
ELSE
	SELECT * FROM temp1bbg ;
		ORDER BY ship_ref,outshipid,cartonnum ;
		INTO CURSOR &cFileOutName READWRITE
ENDIF

USE IN temp1bbg
DELETE FILE F:\3pl\DATA\temp1bbg.DBF
USE IN sqlptbbg
GO BOTT
WAIT CLEAR

SQLDISCONNECT(nHandle)
cRetMsg = "OK"
RETURN cRetMsg
