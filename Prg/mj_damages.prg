**This script will capture all files that have been successfully FTP'd
utilsetup("MJ_DAMAGES")
Set Exclusive Off
Set Deleted On
Set Talk Off
Set Safety Off 
WITH _SCREEN	
	.AUTOCENTER = .T.
	.WINDOWSTATE = 0
	.BORDERSTYLE = 1
	.WIDTH = 320
	.HEIGHT = 210
	.TOP = 290
	.LEFT = 110
	.CLOSABLE = .T.
	.MAXBUTTON = .t.
	.MINBUTTON = .T.
	.SCROLLBARS = 0
	.CAPTION = "MJ_DAMAGES"
ENDWITH	

******************NJ 
goffice='J'
useca("inwolog","wh",,,"mod='"+goffice+"' and  accountid in (6303, 6325,6543)")
useca("indet","wh",,,"mod='"+goffice+"' and  accountid in (6303, 6325,6543)")

******************NJ I/B Return
SELECT a.accountid,SPACE(4) AS AXWHSE,  DTOC(confirmdt) AS TRANSDATE, style,color,id,totqty, SPACE(100) AS notes, container as carton_label , reference as RA_outslip FROM inwolog a, ;
 indet b WHERE a.inwologid=b.inwologid AND INLIST(a.accountid,6324,6325) AND zreturns AND !EMPTYnul(confirmdt) AND confirmdt>=DATE()-6 INTO CURSOR vibnj READWRITE
  **confirmdt>=DATE()-6
  **confirmdt>={10/01/2015} and  confirmdt<{11/01/2015}
REPLACE ALL notes WITH 'RETURN:  Reference '+carton_label+'RA/Outslip '+RA_outslip
replace axwhse with '1250' for accountid=6324
replace axwhse with '1151' for accountid=6325
******************NJ I/B
SELECT a.accountid,SPACE(4) AS AXWHSE,DTOC(confirmdt) AS TRANSDATE, style,color,id,totqty, SPACE(100) AS notes,brokerref as carton_label , reference as RA_outslip  FROM inwolog a, indet b WHERE a.inwologid=b.inwologid AND;
 INLIST(a.accountid,6324,6325) AND !zreturns AND !EMPTYnul(confirmdt) and confirmdt>=DATE()-6 INTO CURSOR vib1 READWRITE
  ** confirmdt>=DATE()-4
replace axwhse with '1250' for accountid=6324
replace axwhse with '1151' for accountid=6325
replace ALL notes with 'I/B receiving:  '+RA_outslip+carton_label IN vib1
replace ALL carton_label WITH '' IN vib1
replace ALL RA_outslip  WITH '' IN vib1
use in indet
use in inwolog

******************ML
goffice='L'
useca("inwolog","wh",,,"mod='"+goffice+"' and  accountid in (6303, 6325,6543)")
useca("indet","wh",,,"mod='"+goffice+"' and  accountid in (6303, 6325,6543)")

******************ML I/B Return
SELECT a.accountid,SPACE(4) AS AXWHSE, DTOC(confirmdt) AS TRANSDATE, style,color,id,totqty, SPACE(100) AS notes, container as carton_label , reference as RA_outslip FROM inwolog a, ;
 indet b WHERE a.inwologid=b.inwologid AND INLIST(a.accountid,6324,6325) AND zreturns AND !EMPTYnul(confirmdt) and confirmdt>=DATE()-6 INTO CURSOR vibca READWRITE
  ** confirmdt>=DATE()-4
REPLACE ALL notes WITH 'RETURN:  Reference '+carton_label+'RA/Outslip '+RA_outslip
replace axwhse with '1152' for accountid=6325
select * from vibnj union all select * from vibca into cursor vib readwrite
******************ML I/B
SELECT a.accountid,SPACE(4) AS AXWHSE,DTOC(confirmdt) AS TRANSDATE, style,color,id,totqty, SPACE(100) AS notes,brokerref as carton_label , reference as RA_outslip  FROM inwolog a, indet b WHERE a.inwologid=b.inwologid AND;
 INLIST(a.accountid,6324,6325) AND !zreturns AND !EMPTYnul(confirmdt) and confirmdt>=DATE()-6 INTO CURSOR vib2 READWRITE
 ** confirmdt>=DATE()-4
replace axwhse with '1152' for accountid=6325
replace ALL notes with 'I/B receiving:  '+RA_outslip+carton_label IN vib2
replace ALL carton_label WITH '' IN vib2
replace ALL RA_outslip  WITH '' IN vib2
select * from vib1 union all select * from vib2 into cursor vib3 readwrite

select accountid as wmsacct, axwhse, transdate,style,color,id,totqty,notes,carton_label,  RA_outslip from vib into cursor vibreturn readwrite
select accountid as wmsacct, axwhse, transdate,style,color,id,totqty,notes,carton_label,  RA_outslip from vib3 into cursor vibreceipt readwrite

******************NJ ADJ
goffice="J"
xsqlexec("select * from adj where mod='J' and adjdt>={"+dtoc(date()-6)+"} and inlist(accountid,6324,6325)",,,"wh")

SELECT accountid,SPACE(4) AS AXWHSE, DTOC(adjdt) as transdate, style,color,id,totqty, 'ADJ:  '+comment AS notes from adj where INLIST(accountid,6324,6325) ;
 AND !('UNIT MOVED FROM' $(COMMENT)) AND !('UNITS MOVED FROM' $(COMMENT)) AND !('RETURN PUTAWAY' $(COMMENT)) AND !('WAREHOUSE LOCATION CHANGED ';
 $(COMMENT)) AND !('PHYSICAL ADJUSTMENT'  $(COMMENT)) AND adjdt>=DATE()-6 and !offset AND adjtype !='LOCATION CHANGE' into cursor vadjnj readwrite
 ** adjdt>=DATE()-6
 **adjdt>={10/01/2015} and  adjdt<{11/01/2015}
replace axwhse with '1250' for accountid=6324
replace axwhse with '1151' for accountid=6325
use in adj

******************ML ADJ
goffice="L"
xsqlexec("select * from adj where mod='L' and adjdt>={"+dtoc(date()-6)+"} and inlist(accountid,6324,6325)",,,"wh")

SELECT accountid,SPACE(4) AS AXWHSE, DTOC(adjdt) as transdate, style,color,id,totqty, 'ADJ:  '+comment AS notes from adj where INLIST(accountid,6324,6325) ;
 AND !('UNIT MOVED FROM' $(COMMENT)) AND !('UNITS MOVED FROM' $(COMMENT)) AND !('RETURN PUTAWAY' $(COMMENT)) AND !('WAREHOUSE LOCATION CHANGED ';
 $(COMMENT)) AND !('PHYSICAL ADJUSTMENT'  $(COMMENT)) AND  adjdt>=DATE()-6   and !offset AND adjtype !='LOCATION CHANGE' into cursor vadjca readwrite
 ** adjdt>=DATE()-4
replace axwhse with '1152' for accountid=6325
select * from vadjnj union all select * from vadjca into cursor vadj1 readwrite
SELECT *, SPACE(11) as carton_label, SPACE(30) as RA_outslip FROM vadj1 INTO CURSOR vadj READWRITE 
select * from vadj union all select * from vibreturn union all select * from vibreceipt into cursor damage_trans readwrite

********************************************************************   NON SQL
*!*	If !Used("mj_ra_return")
*!*	USE F:\wh\mj_ra_return.dbf IN 0
*!*	ENDIF

*!*	SELECT a.*,consignee FROM damage_trans a LEFT JOIN mj_ra_return b ON a.ra_outslip=b.ra ORDER BY transdate INTO CURSOR damage_trans2 readwrite
*!*	SELECT damage_trans2
*!*	 
*!*		If Reccount() > 0 
*!*			export TO "S:\MarcJacobsData\Reports\damages\mj_damages"  TYPE xls
*!*			export TO "S:\MarcJacobsData\Reports\damages\mj_damages_"+ttoc(datetime(),1)  TYPE xls
*!*			tsendto = "tmarg@fmiint.com"
*!*			tattach = "S:\MarcJacobsData\Reports\damages\mj_damages.xls" 
*!*			tcc =""                                                                                                                               
*!*			tFrom ="Toll EDI System Operations <fmi-transload-ops@fmiint.com>"        
*!*			tmessage = "MJ DAMAGES FOR WEEKENDING:  " +dTOC (DATE())                           
*!*			tSubject = "MJ DAMAGES COMPLETE"                                                           
*!*			Do Form m:\dev\frm\dartmail2 With tsendto,tFrom,tSubject, tcc,tattach,tmessage  ,"A"     
*!*				
*!*		ELSE 
*!*	*		export TO "S:\MarcJacobsData\Reports\InventorySyncFiles\OB_OSnD\NO_pre_invoice_osnd_exist_" + TTOC(DATE()-1, 0)  TYPE xls
*!*			tsendto = "tmarg@fmiint.com"
*!*			tattach = "" 
*!*			tcc =""                                                                                                                               
*!*			tFrom ="Toll EDI System Operations <fmi-transload-ops@fmiint.com>"                                
*!*			tmessage = "NO MJ DAMAGES:  "+dTOC (DATE())         
*!*			tSubject = "NO MJ DAMAGES"                                                           
*!*			Do Form m:\dev\frm\dartmail2 With tsendto,tFrom,tSubject, tcc,tattach,tmessage  ,"A"   

*!*		ENDIF

*!*	schedupdate()
*!*	_Screen.Caption=gscreencaption


********************************************************************   SQL

useca('mjraret','wh',.t.)

SELECT a.*,consignee FROM damage_trans a LEFT JOIN mjraret b ON a.ra_outslip=b.ra ORDER BY transdate INTO CURSOR damage_trans2 readwrite
SELECT damage_trans2
 
	If Reccount() > 0 
		export TO "S:\MarcJacobsData\Reports\damages\mj_damages"  TYPE xls
		export TO "S:\MarcJacobsData\Reports\damages\mj_damages_"+ttoc(datetime(),1)  TYPE xls
		tsendto = "tmarg@fmiint.com"
		tattach = "S:\MarcJacobsData\Reports\damages\mj_damages.xls" 
		tcc =""                                                                                                                               
		tFrom ="Toll EDI System Operations <fmi-transload-ops@fmiint.com>"        
		tmessage = "MJ DAMAGES FOR WEEK ENDING:  " +dTOC (DATE())                           
		tSubject = "MJ DAMAGES COMPLETE"                                                           
		Do Form m:\dev\frm\dartmail2 With tsendto,tFrom,tSubject, tcc,tattach,tmessage  ,"A"     
			
	ELSE 
*		export TO "S:\MarcJacobsData\Reports\InventorySyncFiles\OB_OSnD\NO_pre_invoice_osnd_exist_" + TTOC(DATE()-1, 0)  TYPE xls
		tsendto = "tmarg@fmiint.com"
		tattach = "" 
		tcc =""                                                                                                                               
		tFrom ="Toll EDI System Operations <fmi-transload-ops@fmiint.com>"                                
		tmessage = "NO MJ DAMAGES:  "+dTOC (DATE())         
		tSubject = "NO MJ DAMAGES"                                                           
		Do Form m:\dev\frm\dartmail2 With tsendto,tFrom,tSubject, tcc,tattach,tmessage  ,"A"   

	ENDIF

schedupdate()
_Screen.Caption=gscreencaption