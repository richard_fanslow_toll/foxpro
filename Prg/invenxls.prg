* create an excel spreadsheet of an account's inventory
*
parameters xspecial, xcustcode
**xspecial - if called from bugaboo or ariat's custom report menu, need style descriptions from upcmast - mvw 06/19/13
**xcustcode - if called from bioworld custom reports, need to make sure the customer code exists in the style - mvw 07/30/15

do case
case xspecial and inlist(xaccountid,6221,6521)
	xoffice="C"
	goffice="2"
case xspecial and xaccountid=6532
	xoffice="K"
	goffice="K"
case !empty(xcustcode) and xaccountid=6182
	xoffice="C"
	goffice="5"
endcase

xfilter=iif(!empty(xcustcode),'"'+xcustcode+'" $ style',".t.")
xacctname=acctname(xaccountid)

select offices
locate for locale=xoffice and priority=1
cofficename=offices.city

cwhpath = wf(xoffice, xaccountid)
open database &cwhpath.wh


**pull the grab of upc/descrip into a join vs scanning through inven and looking in upcmast for each line item - mvw 12/12/17

**for bugaboo/ariat special, add style descriptions from upcmast - mvw 06/19/13
xincludeupc=.f.
do case
case xspecial
	xsqlexec("select i.*, u.descrip from inven i left join upcmast u on u.accountid=i.accountid and u.style=i.style where i.mod='"+goffice+"' and i.accountid="+transform(xaccountid)+" and i.totqty#0","xdytemp",,"wh")
**add upc for RAG & BONE (6699) - mvw 12/12/17
**added for S3 accounts - mvw 01/02/18
case inlist(xaccountid,6699,&gs3accounts)
	xsqlexec("select i.*, u.upc from inven i left join upcmast u on u.accountid=i.accountid and u.style=i.style and u.color=i.color and u.id=i.id where i.mod='"+goffice+"' and i.accountid="+transform(xaccountid)+" and i.totqty#0","xdytemp",,"wh")
	xincludeupc=.t.
otherwise
	xsqlexec("select * from inven where mod='"+goffice+"' and accountid="+transform(xaccountid)+" and totqty#0","xdytemp",,"wh")
endcase


*!*	select *, iif(units, "Units", "") as "cType", ttod(updatedt) as updt_date, ;
*!*		"" as blank1, "" as blank2, "" as blank3,space(50) as descrip, space(14) as upc ;
*!*		from xdytemp where &xfilter order by style, color, id into cursor csrinventory readwrite

select *, iif(units, "Units", "     ") as "cType", ttod(updatedt) as updt_date, ;
	"" as blank1, "" as blank2, "" as blank3 ;
	from xdytemp where &xfilter order by style, color, id into cursor csrinventory readwrite


*!*	if xspecial
*!*		scan
*!*			if upcmastsql(csrinventory.accountid,csrinventory.style)
*!*				replace descrip with upcmast.descrip in csrinventory
*!*			endif
*!*		endscan
*!*	endif

**end change - mvw 12/12/17

insert into pdf_file (unique_id) values (reports.unique_id)

do case
case eof() &&need something to distinguish from "poller down"
	select pdf_file
	replace memo with "NO DATA"

case cfiletype = "TABLE"
	store tempfox+alltrim(str(reports.unique_id))+".dbf" to cfile
	copy to &cfile

	select pdf_file
	**in order to avoid "VFPOLEDB" "provider ran out of memory" errors for large files (>2mb) no longer move file 
	**  thru linked server, just copy file directly to \\iis2\apps\CargoTrackingWeb\reports\ - mvw 03/08/13
	*append memo memo from &cfile overwrite
	copy file "&cfile" to \\iis2\apps\CargoTrackingWeb\reports\*.*
	replace memo with "FILE COPIED TO REPORT FOLDER" in pdf_file

case cfiletype = "WORKSHEET"
	nreccnt = reccount()

	store tempfox+"2" to ctmpfile
	xfieldlist="style, color, id, pack, ctype, blank1, length, width, depth, blank2, totqty, allocqty, holdqty, availqty, blank3, date_rcvd, updt_date"
	xtocol="P"
	xxfile="f:\webrpt\invenxls.xls"
	do case
	case xspecial
		xfieldlist="style, color, id, pack, descrip, ctype, blank1, length, width, depth, blank2, totqty, allocqty, holdqty, availqty, blank3, date_rcvd, updt_date"
		xtocol="Q"
		xxfile="f:\webrpt\invenxlsdesc.xls"
	case xincludeupc
		xfieldlist="style, color, id, upc, pack, ctype, blank1, length, width, depth, blank2, totqty, allocqty, holdqty, availqty, blank3, date_rcvd, updt_date"
		xtocol="Q"
		xxfile="f:\webrpt\invenxlsupc.xls"
	endcase

	copy to &ctmpfile fields &xfieldlist type xls

	public oexcel
	oexcel = createobject("Excel.Application")
	oexcel.displayalerts = .f.
	otmpworkbook = oexcel.workbooks.open(ctmpfile)

	**start at row 2 (A2) to eliminate headers, nRecCnt+1 to account for header row
	oexcel.activesheet.range("A2:"+xtocol+alltrim(str(nreccnt+1))).copy()

	store tempfox+alltrim(str(reports.unique_id))+".xls" to cfile
	copy file &xxfile to &cfile

	oworkbook = oexcel.workbooks.open(cfile)
	oexcel.activesheet.range("C3").value = "Account: "+alltrim(xacctname)+;
	space(3)+"Office: "+cofficename

	nrownum = 7
	oexcel.activesheet.range("A"+alltrim(str(nrownum))).pastespecial()

	oexcel.activesheet.range("A5").value=xid1
	oexcel.activesheet.range("B5").value=xid2
	oexcel.activesheet.range("C5").value=xid3

	oworkbook.save()
	oworkbook.close()
	otmpworkbook.close()
	oexcel.quit()
	release otmpworkbook, oworkbook, oexcel

	select pdf_file
	**in order to avoid "VFPOLEDB" "provider ran out of memory" errors for large files (>2mb) no longer move file 
	**  thru linked server, just copy file directly to \\iis2\apps\CargoTrackingWeb\reports\ - mvw 03/08/13
	*append memo memo from &cfile overwrite
	copy file "&cfile" to \\iis2\apps\CargoTrackingWeb\reports\*.*
	replace memo with "FILE COPIED TO REPORT FOLDER" in pdf_file
endcase

use in csrinventory
