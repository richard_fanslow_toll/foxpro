* create spreadsheet showing ee info for Kronos and ADP
* send monthly to Office Managers


*** EXE = F:\UTIL\ADPEDI\KRONOSADPEMPLOYEELIST.EXE

LOCAL loKRONOSADPEMPLOYEELIST
loKRONOSADPEMPLOYEELIST = CREATEOBJECT('KRONOSADPEMPLOYEELIST')
loKRONOSADPEMPLOYEELIST.MAIN()
CLOSE DATABASES ALL
RETURN

#DEFINE LOGIT 1
#DEFINE WAITIT 2
#DEFINE NOWAITIT 4
#DEFINE SENDIT 8
#DEFINE CRLF CHR(13) + CHR(10)
#DEFINE RETURN_DATA_MANDATORY .F.
#DEFINE RETURN_DATA_NOT_MANDATORY .T.
#DEFINE CSVFORMAT 6
#DEFINE WRITE_ONLY_UNBUFFERED 11
#DEFINE COMMA ","

DEFINE CLASS KRONOSADPEMPLOYEELIST AS CUSTOM

	cProcessName = 'KRONOSADPEMPLOYEELIST'

	lTestMode = .T.  && .F. FOR NORMAL OPERATION, .T. TO SUPPRESS ALL BUT DEVELOPER EMAILS AND CHANGE INPUT TABLE, ETC.

	lAutoYield = .T.

	cStartTime = TTOC(DATETIME())
	cFileDate = STRTRAN(DTOC(DATE()),"/","_")
	dToday = DATE()

	* connection properties
	nSQLHandle = 0

	* object ref properties
	oExcel = NULL

	* processing properties
	cTopEmailText = ""

	* table properties
	cDeptlookupTable = 'F:\UTIL\ADPREPORTS\DATA\DEPTTRANS.DBF'

	* wait window properties
	nWaitWindowTimeout = 2
	lWaitWindowIsOn = .T.

	* logfile properties
	lLoggingIsOn = .T.
	cLogFile = 'F:\UTIL\ADPEDI\Logfiles\KRONOSADPEMPLOYEELIST_log.txt'

	* INTERNAL email properties
	lSendInternalEmailIsOn = .T.
	cFrom = 'FMI Corporate <fmicorporate@fmiint.com>'
	cSendTo = 'mbennett@fmiint.com'
	cCC = ''
	cSubject = 'Employee Data Validation Report, Results for: ' + TTOC(DATETIME())
	cAttach = ''
	cBodyText = ''

	cCOMPUTERNAME = ''
	cUSERNAME = ''

	FUNCTION INIT
		IF NOT DODEFAULT()
			RETURN .F.
		ENDIF
		WITH THIS
			*SET RESOURCE OFF
			CLOSE DATA
			SET CENTURY ON
			SET DATE AMERICAN
			SET HOURS TO 24
			SET DECIMALS TO 2
			SET ANSI ON
			SET TALK OFF
			SET DELETED ON
			SET CONSOLE OFF
			SET EXCLUSIVE OFF
			SET SAFETY OFF
			SET EXACT ON
			SET STATUS BAR OFF
			SET SYSMENU OFF
			SET ENGINEBEHAVIOR 70
			_VFP.AUTOYIELD = .lAutoYield
			.cCOMPUTERNAME = GETENV("COMPUTERNAME")
			.cUSERNAME = GETENV("USERNAME")
			IF .lTestMode THEN
				.cSendTo = 'mbennett@fmiint.com'
				.cCC = ''
				.cLogFile = 'F:\UTIL\ADPEDI\Logfiles\KRONOSADPEMPLOYEELIST_log_TESTMODE.txt'
			ENDIF
			.lLoggingIsOn = .lLoggingIsOn AND NOT EMPTY(.cLogFile)
			IF .lLoggingIsOn THEN
				SET ALTERNATE TO (.cLogFile) ADDITIVE
				SET ALTERNATE ON
			ENDIF
		ENDWITH
	ENDFUNC


	FUNCTION DESTROY
		WITH THIS
			IF .lLoggingIsOn  THEN
				SET ALTERNATE OFF
				SET ALTERNATE TO
			ENDIF
			SET STATUS BAR ON
		ENDWITH
		DODEFAULT()
	ENDFUNC


	FUNCTION MAIN
		WITH THIS
			LOCAL lnNumberOfErrors, lcSQL, lcSQL2, loError, lcErrorMsg, lcSQLWTK
			LOCAL lcSpreadsheetTemplate, lcFiletoSaveAs, lcSaveFile, lcDateSuffix, lnRow, lcRow, lcComment

			TRY
				lnNumberOfErrors = 0

				.TrackProgress('Employee Data Validation Report process started....', LOGIT+SENDIT+NOWAITIT)
				.TrackProgress('COMPUTERNAME = ' + .cCOMPUTERNAME, LOGIT+SENDIT)
				.TrackProgress('USERNAME = ' + .cUSERNAME, LOGIT+SENDIT)
				.TrackProgress('PROJECT = KRONOSADPEMPLOYEELIST', LOGIT+SENDIT)
				IF .lTestMode THEN
					.TrackProgress('                        TEST MODE                        ', LOGIT+SENDIT)
				ENDIF

				SET PROCEDURE TO VALIDATIONS ADDITIVE

				lcDateSuffix = DTOC(.dToday)
				.cSubject = 'Employee Data Validation Report for: ' + lcDateSuffix

				lcSpreadsheetTemplate = 'F:\UTIL\ADPEDI\TEMPLATES\KRONOSADPEMPLOYEELIST_TEMPLATE.XLS'

				lcFiletoSaveAs = 'F:\UTIL\ADPEDI\REPORTS\KRONOSADPEMPLOYEELIST_' + STRTRAN(lcDateSuffix,"/","")

				lcSaveFile = lcFiletoSaveAs + '.XLS'

				*USE F:\UTIL\ADPREPORTS\DATA\TGFDEPTS.DBF IN 0 ALIAS TGFDEPTS

				USE F:\UTIL\INSPERITY\DATA\WORKSITES.DBF IN 0 ALIAS WORKSITES
				
				USE (.cDeptlookupTable) IN 0 ALIAS DEPTLOOKUP


				* get data from ADP
				.TrackProgress('Connecting to ADP System....', LOGIT+SENDIT)
				*OPEN DATABASE F:\UTIL\ADPCONNECTION\CONNECT1

				.nSQLHandle = SQLCONNECT("PCPAYWIN","MBENNETT","VP737A")

				IF .nSQLHandle > 0 THEN


*!*						SET TEXTMERGE ON
*!*						TEXT TO	lcSQL NOSHOW
*!*	SELECT
*!*	A.COMPANYCODE AS ADP_COMP,
*!*	{fn LEFT(A.HOMEDEPARTMENT,2)} AS DIVISION,
*!*	{fn SUBSTRING(A.HOMEDEPARTMENT,3,4)} AS DEPARTMENT,
*!*	C.DESCRIPTION AS DEPTDESC,
*!*	A.NAME,
*!*	A.FILE# AS FILE_NUM,
*!*	A.CUSTAREA3 AS WORKSITE
*!*	FROM REPORTS.V_EMPLOYEE A, PCPAYSYS.T_CO_JOB_CLASS B, REPORTS.V_DEPARTMENT C
*!*	WHERE (B.CO_C(+) = A.COMPANYCODE) AND (B.EEOC_JOB_CLASS_C(+) = A.EEOCJOBCLASS)
*!*	AND (C.COMPANYCODE(+) = A.COMPANYCODE) AND (C.DEPARTMENT(+) = A.HOMEDEPARTMENT)
*!*	AND A.STATUS <> 'T'
*!*	ORDER BY 1,2,3,5

*!*						ENDTEXT
*!*						SET TEXTMERGE OFF
					SET TEXTMERGE ON
					
					TEXT TO	lcSQL NOSHOW
					
SELECT
A.COMPANYCODE AS ADP_COMP,
{fn LEFT(A.HOMEDEPARTMENT,2)} AS DIVISION,
{fn SUBSTRING(A.HOMEDEPARTMENT,3,4)} AS DEPARTMENT,
A.NAME,
A.FILE# AS FILE_NUM,
A.CUSTAREA3 AS WORKSITE
FROM REPORTS.V_EMPLOYEE A
WHERE A.STATUS <> 'T'
ORDER BY 1,2,3,5

					ENDTEXT
					SET TEXTMERGE OFF


*!*	{fn LEFT(A.CUSTAREA1,1)} AS COLLAR,
*!*	{fn SUBSTRING(A.CUSTAREA1,2,2)} AS TGFDEPT

					*" AND FILE# NOT IN (SELECT FILE# FROM REPORTS.V_EMPLOYEE WHERE STATUS = 'A' AND COMPANYCODE IN ('E87','E88','E89')) " + ;

					IF USED('ADPCURSOR1') THEN
						USE IN ADPCURSOR1
					ENDIF
					IF USED('ADPCURSOR2') THEN
						USE IN ADPCURSOR2
					ENDIF
					IF USED('ADPCURSOR3') THEN
						USE IN ADPCURSOR3
					ENDIF
					IF USED('ADPCURSORALL') THEN
						USE IN ADPCURSORALL
					ENDIF
					IF USED('ADPCURSORALL2') THEN
						USE IN ADPCURSORALL2
					ENDIF
					IF USED('CURFINAL') THEN
						USE IN CURFINAL
					ENDIF

					IF .ExecSQL(lcSQL, 'ADPCURSOR1', RETURN_DATA_MANDATORY) THEN


						* JOIN to get worksite Name, TGF dept desc
						SELECT A.*, B.LOCNAME, C.DESC AS DEPTDESC, C.HCFTECODE AS TGFDEPT, C.HCFTEDESC AS TGFDESC, C.COLLARTYPE AS COLLAR  ;
							FROM ADPCURSOR1 A ;
							LEFT OUTER JOIN ;
							WORKSITES B ;
							ON B.WWORKSITE = A.WORKSITE ;
							LEFT OUTER JOIN ;
							DEPTLOOKUP C ;
							ON C.DEPT = A.DEPARTMENT ;
							INTO CURSOR ADPCURSOR2 ;
							ORDER BY A.NAME ;
							READWRITE
							
*!*			SELECT ADPCURSOR2
*!*			brow
*!*			THROW

					ENDIF  &&  .ExecSQL(lcSQL, 'ADPCURSOR1', RETURN_DATA_MANDATORY)

					=SQLDISCONNECT(.nSQLHandle)

				ELSE
					* connection error
					.TrackProgress('Unable to connect to ADP Payroll System.', LOGIT+SENDIT)
				ENDIF   &&  .nSQLHandle > 0


				* get data from KRONOS
				.TrackProgress('Connecting to WTK System....', LOGIT+SENDIT)

				lcSQLWTK = ;
					" SELECT " + ;
					" PERSONNUM AS FILE_NUM, " + ;
					" PERSONFULLNAME AS NAME, " + ;
					" HOMELABORLEVELNM1 AS ADP_COMP, " + ;
					" HOMELABORLEVELNM2 AS DIVISION, " + ;
					" HOMELABORLEVELNM3 AS DEPARTMENT, " + ;
					" HOMELABORLEVELNM4 AS WORKSITE " + ;
					" FROM VP_EMPLOYEEV42 " + ;
					" WHERE HOMELABORLEVELNM1 IN ('SXI','ZXU','AXA') " + ;
					" AND EMPLOYMENTSTATUS <> 'Terminated' " + ;
					" AND PERSONNUM NOT IN ('999') " + ;
					" ORDER BY PERSONFULLNAME, PERSONNUM, HOMELABORLEVELNM1 "

*!*						" AND EMPLOYMENTSTATUS = 'Active' " + ;

				*OPEN DATABASE F:\UTIL\KRONOS\KRONOSEDI

				.nSQLHandle = SQLCONNECT("WTK2","sa","kr0n1tes")

				IF .nSQLHandle > 0 THEN

					IF USED('KRONOSCURSOR1') THEN
						USE IN KRONOSCURSOR1
					ENDIF
					IF USED('KRONOSCURSOR2') THEN
						USE IN KRONOSCURSOR2
					ENDIF

					IF .ExecSQL(lcSQLWTK, 'KRONOSCURSOR1', RETURN_DATA_MANDATORY) THEN

						*!*	    SELECT KRONOSCURSOR1
						*!*	    BROWSE

						SELECT ;
							INT(VAL(FILE_NUM)) AS FILE_NUM, ;
							GetNewADPCompany(LEFT(ADP_COMP,3)) AS ADP_COMP, ;
							NAME, ;
							LEFT(DIVISION,2) AS DIVISION, ;
							LEFT(DEPARTMENT,4) AS DEPARTMENT, ;
							LEFT(WORKSITE,3) AS WORKSITE ;
							FROM KRONOSCURSOR1 ;
							INTO CURSOR KRONOSCURSOR2 ;
							ORDER BY NAME ;
							READWRITE

					ENDIF


					*!*	SELECT KRONOSCURSOR2
					*!*	BROWSE

					* add kronos fields to adp cursor
					SELECT A.*, SPACE(100) AS COMMENT, ;
						NVL(B.ADP_COMP,'') AS KADP_COMP, ;
						NVL(B.DIVISION,'') AS KDIVISION, ;
						NVL(B.DEPARTMENT,'') AS KDEPARTMENT, ;
						NVL(B.WORKSITE,'') AS KWORKSITE ;
						FROM ADPCURSOR2 A ;
						LEFT OUTER JOIN ;
						KRONOSCURSOR2 B ;
						ON B.FILE_NUM = A.FILE_NUM ;
						INTO CURSOR CURFINAL ;
						ORDER BY A.WORKSITE, A.NAME ;
						READWRITE


					*!*	        * scan through adp cursor and compare it with kronos cursor
					*!*	        * write a warning message if any of the key fields are different,
					*!*	        * or if the adp file# can't be found in kronos.
					SELECT CURFINAL
					SCAN
						IF EMPTY(CURFINAL.KADP_COMP + CURFINAL.KDIVISION) THEN
							lcComment = "Employee is terminated in Kronos!"
						ELSE
							lcComment = ""
							IF CURFINAL.ADP_COMP <> CURFINAL.KADP_COMP THEN
								lcComment = lcComment + "ADP Company  "
							ENDIF
							IF CURFINAL.DIVISION <> CURFINAL.KDIVISION THEN
								lcComment = lcComment + "Division  "
							ENDIF
							IF CURFINAL.DEPARTMENT <> CURFINAL.KDEPARTMENT THEN
								lcComment = lcComment + "Department  "
							ENDIF
							IF CURFINAL.WORKSITE <> CURFINAL.KWORKSITE THEN
								lcComment = lcComment + "Worksite  "
							ENDIF
						ENDIF

						IF NOT EMPTY(lcComment) THEN
							REPLACE CURFINAL.COMMENT WITH lcComment IN CURFINAL
						ENDIF
					ENDSCAN


					WAIT WINDOW NOWAIT "Opening Excel and populating spreadsheet..."

					oExcel = CREATEOBJECT("excel.application")
					oExcel.displayalerts = .F.
					oExcel.VISIBLE = .F.
					oWorkbook = oExcel.workbooks.OPEN(lcSpreadsheetTemplate)

					oWorkbook.SAVEAS(lcFiletoSaveAs)
					oWorkbook.SAVE()
					oWorksheet = oWorkbook.Worksheets[1]
					oWorksheet.RANGE("A3","Z1000").clearcontents()

					lnRow = 2

					SELECT CURFINAL
					SCAN
						lnRow = lnRow + 1
						lcRow = ALLTRIM(STR(lnRow))
						oWorksheet.RANGE("A"+lcRow).VALUE = CURFINAL.WORKSITE
						oWorksheet.RANGE("B"+lcRow).VALUE = CURFINAL.LOCNAME
						oWorksheet.RANGE("C"+lcRow).VALUE = CURFINAL.NAME
						oWorksheet.RANGE("D"+lcRow).VALUE = CURFINAL.ADP_COMP
						oWorksheet.RANGE("E"+lcRow).VALUE = CURFINAL.DIVISION
						oWorksheet.RANGE("F"+lcRow).VALUE = CURFINAL.DEPARTMENT
						oWorksheet.RANGE("G"+lcRow).VALUE = CURFINAL.DEPTDESC
						oWorksheet.RANGE("H"+lcRow).VALUE = CURFINAL.TGFDEPT
						oWorksheet.RANGE("I"+lcRow).VALUE = CURFINAL.TGFDESC
						oWorksheet.RANGE("J"+lcRow).VALUE = CURFINAL.COLLAR
						oWorksheet.RANGE("K"+lcRow).VALUE = CURFINAL.FILE_NUM

						oWorksheet.RANGE("M"+lcRow).VALUE = CURFINAL.KWORKSITE
						oWorksheet.RANGE("N"+lcRow).VALUE = CURFINAL.KADP_COMP
						oWorksheet.RANGE("O"+lcRow).VALUE = CURFINAL.KDIVISION
						oWorksheet.RANGE("P"+lcRow).VALUE = CURFINAL.KDEPARTMENT

						IF NOT EMPTY(CURFINAL.COMMENT) THEN
							oWorksheet.RANGE("Q"+lcRow).VALUE = CURFINAL.COMMENT
							oWorksheet.RANGE("A" + lcRow + "..Q" + lcRow).FONT.ColorIndex = 3
							oWorksheet.RANGE("A" + lcRow + "..Q" + lcRow).FONT.bold = .T.
						ENDIF

					ENDSCAN

					IF TYPE('oWorkbook') = "O" THEN
						oWorkbook.SAVE()
						oWorkbook.CLOSE()
						oWorkbook = NULL
					ENDIF
					IF TYPE('oExcel') = "O" THEN
						oExcel.QUIT()
						oExcel = NULL
					ENDIF

					IF FILE(lcSaveFile) THEN
						.cAttach = lcSaveFile
						.cBodyText = "Station Managers: please review your employee data for accuracy and report any needed corrections to Human Resources." + CRLF + CRLF + "<<REPORT LOG FOLLOWS>>" + CRLF + .cBodyText
					ELSE
						.TrackProgress("ERROR attaching " + lcSaveFile, LOGIT+SENDIT+NOWAITIT)
					ENDIF

				ELSE
					* connection error
					.TrackProgress('Unable to connect to WTK System.', LOGIT+SENDIT)
				ENDIF   &&  .nSQLHandle > 0

				=SQLDISCONNECT(.nSQLHandle)


				CLOSE DATABASES ALL

			CATCH TO loError
				WAIT CLEAR
				.TrackProgress('There was an error.',LOGIT+SENDIT)
				.TrackProgress('The Error # is: ' + TRANSFORM(loError.ERRORNO), LOGIT+SENDIT)
				.TrackProgress('The Error Message is: ' + TRANSFORM(loError.MESSAGE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred in Program: ' + TRANSFORM(loError.PROCEDURE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred at line #: ' + TRANSFORM(loError.LINENO), LOGIT+SENDIT)
				lnNumberOfErrors = lnNumberOfErrors + 1
				IF TYPE('.oExcel') = 'O' AND NOT ISNULL(.oExcel) THEN
					.oExcel.QUIT()
				ENDIF
				CLOSE DATA

			ENDTRY

			CLOSE DATA
			WAIT CLEAR
			***************** INTERNAL email results ******************************
			.TrackProgress('About to send status email.',LOGIT)
			.TrackProgress('The logfile is: ' + .cLogFile, SENDIT)
			.TrackProgress('Employee Data Validation Report process started: ' + .cStartTime, LOGIT+SENDIT)
			.TrackProgress('Employee Data Validation Report process finished: ' + TTOC(DATETIME()), LOGIT+SENDIT)

			IF .lSendInternalEmailIsOn THEN
				* try to trap error from not having dartmail dll's registered on user's pc...
				TRY
					DO FORM dartmail2 WITH .cSendTo,.cFrom,.cSubject,.cCC,.cAttach,.cBodyText,"A"
					.TrackProgress('Sent status email.',LOGIT)
				CATCH TO loError
					*=MESSAGEBOX('There was an error sending the status email. Please contact Mark Bennett.',0+64,"Employee Data Validation Report")
					.TrackProgress('There was an error sending the status email.',LOGIT)
					.TrackProgress('The Error # is: ' + TRANSFORM(loError.ERRORNO), LOGIT)
					.TrackProgress('The Error Message is: ' + TRANSFORM(loError.MESSAGE), LOGIT)
					.TrackProgress('The Error occurred in Program: ' + TRANSFORM(loError.PROCEDURE), LOGIT)
					.TrackProgress('The Error occurred at line #: ' + TRANSFORM(loError.LINENO), LOGIT)
					lnNumberOfErrors = lnNumberOfErrors + 1
					IF TYPE('.oExcel') = 'O' AND NOT ISNULL(.oExcel) THEN
						.oExcel.QUIT()
					ENDIF
					CLOSE DATA
				ENDTRY
			ELSE
				.TrackProgress('Did not send status email: THIS.lSendInternalEmailIsOn = FALSE.',LOGIT)
			ENDIF

		ENDWITH
		RETURN
	ENDFUNC && main


	PROCEDURE SortArrayByDateTime
		* expects taInArray[] to be the result of an ADIR()
		LPARAMETERS taInArray, taOutArray
		EXTERNAL ARRAY taInArray, taOutArray
		LOCAL lnRows, lnCols, i, j
		* extending taInArray[] to process the files in date/time order
		lnRows = ALEN(taInArray,1)
		lnCols = ALEN(taInArray,2)
		* create taOutArray with one more column
		DIMENSION taOutArray[lnRows,lnCols + 1]
		* and fill it with taInArray columns AND (filedate + filetime) in new column
		* ( ACOPY() doesn't preserve elements properly because of different # of columns )
		FOR i = 1 TO lnRows
			FOR j = 1 TO lnCols
				taOutArray[i,j] = taInArray[i,j]
			ENDFOR
			taOutArray[i,lnCols + 1] = DTOS(taInArray[i,3]) + "_" + taInArray[i,4]
		ENDFOR
		* sort taOutArray on date/time column
		=ASORT(taOutArray,6)
	ENDPROC


	FUNCTION ExecSQL
		LPARAMETERS tcSQL, tcCursorName, tlNoDataReturnedIsOkay
		LOCAL llRetval, lnResult
		WITH THIS
			* close target cursor if it's open
			IF USED(tcCursorName)
				USE IN (tcCursorName)
			ENDIF
			WAIT WINDOW "Fetching remote data; please wait..." NOWAIT
			lnResult = SQLEXEC(.nSQLHandle, tcSQL, tcCursorName)
			llRetval = ( lnResult > 0 )
			IF llRetval THEN
				* see if any data came back
				IF NOT tlNoDataReturnedIsOkay THEN
					IF (NOT USED(tcCursorName)) OR EOF(tcCursorName) THEN
						llRetval = .F.
						.TrackProgress('No data was returned by this query: ' +CRLF + tcSQL, LOGIT+SENDIT)
					ENDIF
				ENDIF
			ELSE
				.TrackProgress("Result Code [" + TRANSFORM(lnResult) + "] returned by this query: " + tcSQL, LOGIT+SENDIT)
			ENDIF
			WAIT CLEAR
			RETURN llRetval
		ENDWITH
	ENDFUNC


	PROCEDURE TrackProgress
		* do any combination of Wait Window, writing to logfile, and adding to body of email,
		* based on nFlags parameter.
		LPARAMETERS tcExpression, tnFlags
		WITH THIS
			IF BITAND(tnFlags,LOGIT) = LOGIT THEN
				IF .lLoggingIsOn THEN
					?
					? .cProcessName + "  " + TTOC(DATETIME()) + ": " + tcExpression
				ENDIF
			ENDIF
			IF .lWaitWindowIsOn THEN
				IF BITAND(tnFlags,NOWAITIT) = NOWAITIT THEN
					WAIT WINDOW tcExpression NOWAIT
				ENDIF
				IF BITAND(tnFlags,WAITIT) = WAITIT THEN
					WAIT WINDOW tcExpression TIMEOUT .nWaitWindowTimeout
				ENDIF
			ENDIF
			IF BITAND(tnFlags,SENDIT) = SENDIT THEN
				IF .lSendInternalEmailIsOn THEN
					.cBodyText = .cBodyText + tcExpression + CRLF + CRLF
				ENDIF
			ENDIF
		ENDWITH
	ENDPROC  &&  TrackProgress

ENDDEFINE
