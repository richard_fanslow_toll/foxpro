* returns the folder for WMS data
*
* xaccountid is only needed if xoffice is C, N or K
*
* if root=.f., returns f:\whx\whdata\
* if root=.t., returns f:\whx\ 

lparameters xwfoffice, xdyaccountid2, xroot, xarchive, xmod, xfrom, xnoerrormessage, xsqlmod

xdyaccountid=xdyaccountid2

if type("gforceoffice")="U"
	public gforceoffice
endif

**when called from webrpt, kpirpt, etc., xignorexoffice is defined and set to .t. to ignore "xoffice" for office "K" - mvw 09/02/15
if type("xignorexoffice")="U"
	xignorexoffice=.f.
endif

if empty(xfrom)
	xfrom="?"
endif

xwfoffice=upper(xwfoffice)

local xreturn, xh

if !used("whoffice")
	xsqlexec("select * from whoffice",,,"wh")
	index on office tag office
	set order to
endif

**added code to retun the mod from whoffices - mvw 11/04/11
if xmod
	if !seek(xwfoffice,"whoffice","office") or empty(whoffice.mod)
		return xwfoffice
	else
		return alltrim(whoffice.mod)
	endif
endif
*****end - mvw 11/04/11

**open account if not already open - mvw 06/08/15
xacctused=used("account")
if !xacctused
	xsqlexec("select * from account where inactive=0",,,"qq")
	index on accountid tag accountid
	set order to
endif

if type("glivedata")="L" and !glivedata
  If xwfoffice="P"
    xreturn="f:\whp\whdata\"
  else
	  xreturn="m:\dev\whdata\"
  Endif 
else
	xh=iif(xarchive,"h","")

	do case
	case xwfoffice="X"
		=seek("X","whoffice","office")
		xreturn=whoffice.folder+"\whdata"+xh+"\"
	
	case xwfoffice="S"	&& steelxyz
		=seek("S","whoffice","office")
		xreturn=whoffice.folder+"\whdata"+xh+"\"
	
	case (type("goffice")="C" and goffice="K") or (type("xoffice")="C" and xoffice="K" and !xignorexoffice)
		=seek("K","whoffice","office")
		xreturn=whoffice.folder+"\whdata"+xh+"\"

	case !empty(gforceoffice)
		=seek(gforceoffice,"whoffice","office")
		gforceoffice=.f.
		xreturn=whoffice.folder+"\whdata"+xh+"\"

	case !inlist(xwfoffice,"N","C","K")
		=seek(xwfoffice,"whoffice","office")
		xreturn=whoffice.folder+"\whdata"+xh+"\"

	case type("xdyaccountid")="N" and xdyaccountid=4484		&& Sun Express does trucking work for Nanjing
		=seek("1","whoffice","office")
		xreturn=whoffice.folder+"\whdata"+xh+"\"

	**ignore for bioworld (6182) - mvw 01/29/18
	case xwfoffice="C" and seek(xdyaccountid,"account","accountid") and account.mlinven and !moretaccount("xdyaccountid") and xdyaccountid#6182 	&& Mira Loma except for Moret
		=seek("L","whoffice","office")
		xreturn=whoffice.folder+"\whdata"+xh+"\"

	case type("xdyaccountid")="N" and inlist(xdyaccountid,5442,5451,6401,5452,5453)	&& Moret trucking
		if xwfoffice="N"	
			=seek("I","whoffice","office")
		else
			=seek("7","whoffice","office")
		endif
		xreturn=whoffice.folder+"\whdata"+xh+"\"

**removed - this code doesnt work for steel series which is master office "K" but mod "S" - mvw 04/07/16
*!*		case xwfoffice="K"
*!*			xreturn="f:\whk\whdata\"
	
	otherwise
		if empty(xdyaccountid)
			messagebox("Account # missing - system will crash now")
			do ("email") with "dyoung@fmiint.com","WF: Account # missing",xwfoffice,,,,.t.,,,,,.t.,,.t.
		else
			**return focus back to existing alias - mvw 12/01/15
			xalias=alias()
			xsqlexec("select * from account","lazyboy",,"qq")
			index on accountid tag accountid
			set order to
			if !empty(xalias)
				select &xalias
			endif

			if seek(xdyaccountid,"lazyboy","accountid")
				do case
				case xwfoffice="N" and (lazyboy.njmod="I" or empty(lazyboy.njmod))
					=seek("I","whoffice","office")
				case xwfoffice="N" and lazyboy.njmod="J"
					=seek("J","whoffice","office")
				case xwfoffice="C" and lazyboy.building="4"
					=seek("1","whoffice","office")
				case xwfoffice="C" and inlist(lazyboy.building,"7","8")
					=seek("2","whoffice","office")
				case xwfoffice="C" and lazyboy.building="6"
					=seek("5","whoffice","office")
				case xwfoffice="C" and lazyboy.building="5"
					=seek("6","whoffice","office")
				case xwfoffice="C" and lazyboy.building="0"
					=seek("7","whoffice","office")
				case xwfoffice="C" and lazyboy.building="1"
					=seek("8","whoffice","office")
				case xwfoffice="C" and lazyboy.building="L"
					=seek("L","whoffice","office")
				case xwfoffice="C" and lazyboy.building="Y"
					=seek("Y","whoffice","office")
				case xwfoffice="K" and lazyboy.kymod="K"
					=seek("K","whoffice","office")
				case xwfoffice="K" and lazyboy.kymod="S"	&& steelxyz
					=seek("S","whoffice","office")
				case xwfoffice="Z" and lazyboy.kymod="Z"	&& toronto, joeb, per juan 09.20.2017
					=seek("Z","whoffice","office")
				otherwise
					if xnoerrormessage
						return ""
					else
						set step on 
						do ("email") with "dyoung@fmiint.com","WF: bad building",xwfoffice+" - "+lazyboy.njmod+" - "+lazyboy.building+crlf+xfrom+crlf+transform(xdyaccountid),,,,.t.,,,,,.t.,,.t.
					endif
				endcase
				xreturn=whoffice.folder+"\whdata"+xh+"\"
			
			else
				messagebox("Account # "+transform(xaccountid)+" not found - system will now crash")
*				do ("email") with "dyoung@fmiint.com","WF: Account not found",xwfoffice+str(xaccountid,4),,,,.t.,,,,,.t.,,.t.
			endif
			
			use in lazyboy
		endif
	endcase
endif

if xsqlmod
	xreturn=substr(xreturn,6,1)
endif

if xroot
	if type("glivedata")="U" or glivedata
		xreturn=strtran(xreturn,"\whdata"+xh,"")
	else
		try
			xreturn=strtran(xreturn,"\whdata"+xh,"")
		catch
			xreturn="f:\wh"+goffice+"\"
		endtry
	endif
endif

if !xacctused
	use in account
endif

return xreturn