**insert record into f:\wh\waveinfo
parameters m.formname, m.waveid, m.wavenum, m.wo_num, m.accountid, m.office, m.ship_ref, m.ucc, m.system, m.message, m.details, m.userid, m.style, m.color, m.id, m.upc, m.pack, m.qty

if !used("waveinfo")
	return
endif

if cursorgetprop("Buffering","waveinfo")#5
	cursorsetprop("Buffering",5,"waveinfo")
endif

m.formname=iif(empty(m.formname),"",m.formname)
m.waveid=iif(empty(m.waveid),0,m.waveid)
m.wavenum=iif(empty(m.wavenum),0,m.wavenum)
m.wo_num=iif(empty(m.wo_num),0,m.wo_num)
m.accountid=iif(empty(m.accountid),0,m.accountid)
m.office=iif(empty(m.office),"",m.office)
m.ship_ref=iif(empty(m.ship_ref),"",m.ship_ref)
m.ucc=iif(empty(m.ucc),"",m.ucc)
m.message=iif(empty(m.message),"",m.message)
m.details=iif(empty(m.details),"",m.details)
m.style=iif(empty(m.style),"",m.style)
m.color=iif(empty(m.color),"",m.color)
m.id=iif(empty(m.id),"",m.id)
m.upc=iif(empty(m.upc),"",m.upc)
m.pack=iif(empty(m.pack),"",m.pack)
m.qty=iif(empty(m.qty),0,m.qty)
m.userid=iif(empty(m.userid),guserid,m.userid)
m.zdate=date()
m.zdatetime=datetime()
m.addby=guserid
m.adddt=datetime()
m.addproc=iif(type("gprocess")="C",gprocess,"")

release m.details

*release m.formname,m.waveid,m.wavenum,m.wo_num,m.accountid,m.office,m.ship_ref,m.ucc,m.message,m.details,m.style,m.color,m.id,m.upc,m.pack,m.qty,m.userid,m.zdate,m.zdatetime,m.addby,m.adddt,m.addproc

**if error inserting record, send emil - mvw 09/19/14

insertinto("waveinfo","wh",.t.)
tu("waveinfo")
