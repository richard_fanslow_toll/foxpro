* import the old Fleet data into the new Shop database.
LOCAL lnAnswer, lcNewDataDir, lcNewDatabase

=MESSAGEBOX("Please make a backup of the Shop Data before proceeding",0+16)
lnAnswer = MESSAGEBOX("This import process will overwrite existing shop data! Continue?",4+48+256)
IF lnAnswer <> 6 THEN
	RETURN
ENDIF

CLOSE ALL
SET CONSOLE OFF
SET STATUS BAR OFF
SET TALK OFF
SET SAFETY OFF

*lcNewDataDir = "M:\DEV\SHDATA\"
lcNewDataDir = "F:\SHOP\SHDATA\"

lcNewDatabase = lcNewDataDir + "SH"

OPEN DATABASE (lcNewDatabase)

***********************************************************
* initialize tables

* primary key table
USE SH!generatepk
*  reset key to 0 only for those tables we are reloading (added new col gpkclear to indicate this)
REPLACE ALL gpk_currentnumber WITH 0 FOR gpk_clear

*!*	* remove TRUCKS & TRAILERS FROM SH.DBC!generatepk because they each now have their own dbc in the project.
*!*	USE SH!generatepk
*!*	REPLACE ALL gpk_pk WITH "XXXTRUCKS" FOR ALLTRIM(gpk_pk) == "TRUCKS"
*!*	REPLACE ALL gpk_pk WITH "XXXTRAILER" FOR ALLTRIM(gpk_pk) == "TRAILER"
*!*	* leaving generatepk open intentionally...

SELECT 0

*!*	* Oildetl
*!*	WAIT WINDOW "Importing Oildetl" NOWAIT
*!*	USE SH!OILDETL EXCLUSIVE
*!*	ZAP
*!*	APPEND FROM F:\FLEET\OILDETL FOR NOT DELETED()

*!*	* Ordhdr
*!*	WAIT WINDOW "Importing Ordhdr" NOWAIT
*!*	USE SH!ORDHDR EXCLUSIVE
*!*	ZAP
*!*	APPEND FROM F:\FLEET\ORDHDR FOR NOT DELETED()

*!*	* Mechanic
*!*	WAIT WINDOW "Importing Mechanic" NOWAIT
*!*	USE SH!MECHANIC EXCLUSIVE
*!*	ZAP
*!*	APPEND FROM F:\FLEET\MECHANIC FOR NOT DELETED() AND NOT EMPTY(NAME)

*!*	* Parts
*!*	WAIT WINDOW "Importing Parts" NOWAIT
*!*	USE SH!PARTS EXCLUSIVE
*!*	ZAP
*!*	APPEND FROM F:\FLEET\PARTS FOR NOT DELETED() AND NOT EMPTY(PARTNO)

*!*	* Vehparts?

*!*	* Repdetl
*!*	WAIT WINDOW "Importing Repdetl" NOWAIT
*!*	USE SH!REPDETL EXCLUSIVE
*!*	ZAP
*!*	APPEND FROM F:\FLEET\REPDETL FOR NOT DELETED()

*!*	* Tiredetl
*!*	WAIT WINDOW "Importing Tiredetl" NOWAIT
*!*	USE SH!TIREDETL EXCLUSIVE
*!*	ZAP
*!*	APPEND FROM F:\FLEET\TIREDETL FOR NOT DELETED()

*!*	* Vehmast
*!*	WAIT WINDOW "Importing Vehmast" NOWAIT
*!*	USE SH!VEHMAST EXCLUSIVE
*!*	ZAP
*!*	APPEND FROM F:\FLEET\VEHMAST FOR NOT DELETED()

*!*	* Washes
*!*	WAIT WINDOW "Importing Washes" NOWAIT
*!*	USE SH!WASHES EXCLUSIVE
*!*	ZAP
*!*	APPEND FROM F:\FLEET\WASHES FOR NOT DELETED()

*!*	* Trucks
*!*	WAIT WINDOW "Importing Trucks" NOWAIT
*!*	USE SH!TRUCKS EXCLUSIVE
*!*	ZAP
*!*	APPEND FROM F:\TRUCK\TRDATA\TRUCKS FOR NOT DELETED()

*!*	* Trailers
*!*	WAIT WINDOW "Importing Trailers" NOWAIT
*!*	USE SH!TRAILER EXCLUSIVE
*!*	ZAP
*!*	APPEND FROM F:\TRAILER\TXDATA\TRAILER FOR NOT DELETED()

* Inspections (no existing data in old system so just zap it)
WAIT WINDOW "Initializing Inspections" NOWAIT
USE SH!INSPECTS EXCLUSIVE
ZAP

USE

***********************************************************
* POPULATE THE EMPTY ORDHDRID FIELD IN DETAIL TABLES
USE SH!ORDHDR IN 0


WAIT WINDOW "Back-filling ordhdrid's" NOWAIT
USE SH!OILDETL IN 0
SELECT OILDETL
SCAN
	SELECT ORDHDR
	LOCATE FOR ordno = OILDETL.ordno
	IF FOUND() THEN
		REPLACE OILDETL.ordhdrid WITH ORDHDR.ordhdrid
	ENDIF
ENDSCAN
USE IN OILDETL

USE SH!REPDETL IN 0
SELECT REPDETL
SCAN
	SELECT ORDHDR
	LOCATE FOR ordno = REPDETL.ordno
	IF FOUND() THEN
		REPLACE REPDETL.ordhdrid WITH ORDHDR.ordhdrid
	ENDIF
ENDSCAN
USE IN REPDETL

USE SH!TIREDETL IN 0
SELECT TIREDETL
SCAN
	SELECT ORDHDR
	LOCATE FOR ordno = TIREDETL.ordno
	IF FOUND() THEN
		REPLACE TIREDETL.ordhdrid WITH ORDHDR.ordhdrid
	ENDIF
ENDSCAN
USE IN TIREDETL

***********************************************************

* Set blank Parts.grpcodes to "O" (OTHER)
WAIT WINDOW "Back-filling empty grpcode's in PARTS.DBF" NOWAIT
USE SH!PARTS IN 0
SELECT PARTS
REPLACE ALL grpcode WITH "O" FOR EMPTY(grpcode)
USE IN PARTS

* Populate the zero grpcodeid AND BLANK GRPDESC in imported Parts.dbf
WAIT WINDOW "Back-filling grpcodeid's in PARTS.DBF" NOWAIT
USE SH!grpcode IN 0
USE SH!PARTS IN 0
SELECT PARTS
SCAN
	SELECT grpcode
	LOCATE FOR grpcode = PARTS.grpcode
	IF FOUND() THEN
		REPLACE PARTS.grpcodeid WITH grpcode.grpcodeid, PARTS.grpdesc WITH grpcode.grpdesc
	ENDIF
ENDSCAN
USE IN PARTS
USE IN grpcode

* Populate blank partsid in repdetl, oildetl, tiredetl
WAIT WINDOW "Back-filling partsid's in REPDETL.DBF" NOWAIT
USE SH!PARTS IN 0
USE SH!REPDETL IN 0
SELECT REPDETL
SCAN
	SELECT PARTS
	LOCATE FOR PARTNO == REPDETL.PARTNO
	IF FOUND() THEN
		REPLACE REPDETL.partsid WITH PARTS.partsid
	ENDIF
ENDSCAN
USE IN REPDETL
USE IN PARTS

WAIT WINDOW "Back-filling partsid's in OILDETL.DBF" NOWAIT
USE SH!PARTS IN 0
USE SH!OILDETL IN 0
SELECT OILDETL
SCAN
	SELECT PARTS
	LOCATE FOR PARTNO == OILDETL.PARTNO
	IF FOUND() THEN
		REPLACE OILDETL.partsid WITH PARTS.partsid
	ENDIF
ENDSCAN
USE IN OILDETL
USE IN PARTS

WAIT WINDOW "Back-filling partsid's in TIREDETL.DBF" NOWAIT
USE SH!PARTS IN 0
USE SH!TIREDETL IN 0
SELECT TIREDETL
SCAN
	SELECT PARTS
	LOCATE FOR PARTNO == TIREDETL.PARTNO
	IF FOUND() THEN
		REPLACE TIREDETL.partsid WITH PARTS.partsid
	ENDIF
ENDSCAN
USE IN TIREDETL
USE IN PARTS


* UPDATE GENPKEY TABLE WITH MAX ORDNO FOR ORDHDR
* necessary because old table has ORDNO field populated so default trigger doesn't fire.
SELECT MAX(ordno) AS ordno FROM SH!ORDHDR INTO CURSOR curMaxOrdno
SELECT curMaxOrdno
GOTO TOP
SELECT generatepk
REPLACE ALL gpk_currentnumber WITH curMaxOrdno.ordno FOR gpk_pk = "ORDNO"
USE IN curMaxOrdno

* UPDATE GENPKEY TABLE WITH MAX TRUCKSID FOR TRUCKS
* necessary because old table has trucksid field populated so default trigger doesn't fire.
SELECT MAX(TRUCKSID) AS TRUCKSID FROM SH!TRUCKS INTO CURSOR curMaxID
SELECT curMaxID
GOTO TOP
SELECT generatepk
REPLACE ALL gpk_currentnumber WITH curMaxID.TRUCKSID FOR gpk_pk = "TRUCKS"
USE IN curMaxID
IF USED('TRUCKS') then
	USE IN TRUCKS
ENDIF

* UPDATE GENPKEY TABLE WITH MAX TRAILERID FOR TRAILER
* necessary because old table has trailerid field populated so default trigger doesn't fire.
SELECT MAX(TRAILERID) AS TRAILERID FROM SH!TRAILER INTO CURSOR curMaxID
SELECT curMaxID
GOTO TOP
SELECT generatepk
REPLACE ALL gpk_currentnumber WITH curMaxID.TRAILERID FOR gpk_pk = "TRAILER"
USE IN curMaxID
IF USED('TRAILER') then
	USE IN TRAILER
ENDIF

**************************************************************
* 01/26/04 MB
* fill in empty 'type' field in ordhdr

USE SH!TRUCKS IN 0 ALIAS TRUCKS
USE SH!TRAILER IN 0 ALIAS TRAILER

SELECT ORDHDR
GOTO TOP
SET EXACT ON

SCAN FOR EMPTY(TYPE)

	WAIT WINDOW "Populating Type field in Order #" + TRANSFORM(ORDHDR.ordno) NOWAIT

	REPLACE ORDHDR.TYPE WITH "MISC", ORDHDR.company WITH "I" IN ORDHDR

	SELECT TRUCKS
	LOCATE FOR ALLTRIM(ORDHDR.vehno) = ALLTRIM(TRUCKS.truck_num)
	IF FOUND()
		REPLACE ORDHDR.company WITH TRUCKS.company, ORDHDR.TYPE WITH "TRUCK"
	ENDIF

	SELECT TRAILER
	LOCATE FOR ALLTRIM(ORDHDR.vehno) = ALLTRIM(TRAILER.trail_num)
	IF FOUND()
		REPLACE ORDHDR.company WITH TRAILER.company, ORDHDR.TYPE WITH "TRAILER"
	ENDIF

ENDSCAN
USE IN TRUCKS
USE IN TRAILER
**************************************************************

*********************************************************************************************
* 01/27/04 MB
* Make all repdates, tiredates, oildates = their WO create-date (they don't match in old system)
* THIS MUST BE DONE AFTERTHE BACKFILL OF THE ORDRHDRID'S!!!
USE SH!REPDETL IN 0
USE SH!OILDETL IN 0
USE SH!TIREDETL IN 0
SELECT ORDHDR
SCAN

	WAIT WINDOW "Correcting repair/oil/tire dates for Order #" + TRANSFORM(ORDHDR.ordno) NOWAIT
	
	SELECT REPDETL
	SCAN FOR ordhdrid = ORDHDR.ordhdrid
		REPLACE REPDATE WITH ORDHDR.CRDATE
	ENDSCAN

	SELECT OILDETL
	SCAN FOR ordhdrid = ORDHDR.ordhdrid
		REPLACE OILDATE WITH ORDHDR.CRDATE
	ENDSCAN

	SELECT TIREDETL
	SCAN FOR ordhdrid = ORDHDR.ordhdrid
		REPLACE TIREDATE WITH ORDHDR.CRDATE
	ENDSCAN

ENDSCAN
USE IN OILDETL
USE IN TIREDETL
USE IN REPDETL
*********************************************************************************************


WAIT CLEAR
CLOSE ALL
RETURN
