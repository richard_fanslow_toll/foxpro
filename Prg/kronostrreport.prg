* Create spreadsheet of Hours summary for Employees we are exporting from kronos to ADP Payroll
* for the previous pay period.

*** BY TIMEREVIEWER, and email to each just his people ****

** Meant to be run after hours are reviewed and cleaned up; Monday pm or Tuesday am.
* Pass "Y" as parameter to indicate 2nd shift reporting.
LPARAMETERS tcParameter

runack("KRONOSTRREPORT")

LOCAL loKronosTRReport, llShift2, lcTRNum, llSpecial

llSpecial = .T.

IF NOT llSpecial THEN
	utilsetup("KRONOSTRREPORT")
ENDIF


IF llSpecial THEN

	loKronosTRReport = CREATEOBJECT('KronosTRReport')
	loKronosTRReport.SetSpecial()
	USE F:\UTIL\KRONOS\DATA\TIMERVWR.DBF AGAIN IN 0 ALIAS RVWRLIST
	SELECT RVWRLIST
	SCAN FOR SPECIAL AND LACTIVE AND LHOURLY
		loKronosTRReport.MAIN( RVWRLIST.TIMERVWR, RVWRLIST.NAME, RVWRLIST.EMAIL, RVWRLIST.CC )
	ENDSCAN

ELSE

	lcTRNum = ""
	llShift2 = .F.

	IF NOT EMPTY(tcParameter) THEN
		DO CASE
			CASE UPPER(ALLTRIM(tcParameter)) = "Y"
				llShift2 = .T.
			OTHERWISE
				lcTRNum = ALLTRIM(tcParameter)
		ENDCASE
	ENDIF

	*!*	IF NOT INLIST(UPPER(ALLTRIM(GETENV("COMPUTERNAME"))),"MBENNETT","HR1") THEN
	*!*		=MESSAGEBOX("The Employee Export must be run from Lucille's PC!",0+16,"Export FoxPro Employees")
	*!*		RETURN
	*!*	ENDIF
	loKronosTRReport = CREATEOBJECT('KronosTRReport')


	USE F:\UTIL\KRONOS\DATA\TIMERVWR.DBF AGAIN IN 0 ALIAS RVWRLIST
	SELECT RVWRLIST

	DO CASE
		CASE llShift2
			SCAN FOR LACTIVE AND LSHIFT2 AND LHOURLY
				loKronosTRReport.MAIN( RVWRLIST.TIMERVWR, RVWRLIST.NAME, RVWRLIST.EMAIL, RVWRLIST.CC )
			ENDSCAN
		CASE NOT EMPTY(lcTRNum)
			* a single TR
			SCAN FOR (TIMERVWR = lcTRNum)
				loKronosTRReport.MAIN( RVWRLIST.TIMERVWR, RVWRLIST.NAME, RVWRLIST.EMAIL, RVWRLIST.CC )
			ENDSCAN
		OTHERWISE
			* normal case
			SCAN FOR LACTIVE AND (NOT LSHIFT2) AND LHOURLY
				loKronosTRReport.MAIN( RVWRLIST.TIMERVWR, RVWRLIST.NAME, RVWRLIST.EMAIL, RVWRLIST.CC )
			ENDSCAN
	ENDCASE
ENDIF && llSpecial

IF NOT llSpecial THEN
	schedupdate()
ENDIF

CLOSE DATABASES ALL
RETURN

#DEFINE LOGIT 1
#DEFINE WAITIT 2
#DEFINE NOWAITIT 4
#DEFINE SENDIT 8
#DEFINE CRLF CHR(13) + CHR(10)
#DEFINE RETURN_DATA_MANDATORY .F.
#DEFINE RETURN_DATA_NOT_MANDATORY .T.

DEFINE CLASS KronosTRReport AS CUSTOM

	cProcessName = 'KronosTRReport'

	lSpecial = .F.

	lTestMode = .T.  && .F. FOR NORMAL OPERATION, .T. TO SUPPRESS ALL BUT DEVELOPER EMAILS AND CHANGE INPUT TABLE, ETC.

	lAutoYield = .T.

	cStartTime = TTOC(DATETIME())

	* Excel properties
	oExcel = NULL

	* connection properties
	nSQLHandle = 0

	* file properties
	nFileHandle = 0

	* table properties
	cFoxProTimeReviewerTable = "F:\UTIL\KRONOS\DATA\TIMERVWR"

	* date properties
	dtNow = DATETIME()
	dToday = DATE()
	cToday = DTOC(DATE())

	* wait window properties
	nWaitWindowTimeout = 2
	lWaitWindowIsOn = .T.

	* logfile properties
	lLoggingIsOn = .T.
	cLogFile = 'F:\UTIL\KRONOS\Logfiles\KronosTRReport_log.txt'

	* INTERNAL email properties
	lSendInternalEmailIsOn = .T.
	cFrom = 'FMI Corporate <fmicorporate@fmiint.com>'
	cSendTo = 'mbennett@fmiint.com'
	cCC = 'mbennett@fmiint.com'
	cSubject = 'KRONOS TIME REVIEWER REPORT for: ' + TTOC(DATETIME())
	cAttach = ''
	cBodyText = ''

	cCOMPUTERNAME = ''
	cUSERNAME = ''

	FUNCTION INIT
		IF NOT DODEFAULT()
			RETURN .F.
		ENDIF
		WITH THIS
			LOCAL loError
			CLEAR
			*SET RESOURCE OFF
			CLOSE DATA
			SET CENTURY ON
			SET DATE YMD
			SET HOURS TO 24
			SET ANSI ON
			SET TALK OFF
			SET DELETED ON
			SET CONSOLE OFF
			SET EXCLUSIVE OFF
			SET SAFETY OFF
			SET EXACT OFF
			SET STATUS BAR ON
			SET SYSMENU OFF
			SET ENGINEBEHAVIOR 70
			_VFP.AUTOYIELD = .lAutoYield
			.cCOMPUTERNAME = GETENV("COMPUTERNAME")
			.cUSERNAME = GETENV("USERNAME")
			IF .lTestMode THEN
				.cSendTo = 'Mark Bennett <mbennett@fmiint.com>'
				.cCC = ''
				.cLogFile = 'F:\UTIL\KRONOS\Logfiles\KronosTRReport_log_TESTMODE.txt'
			ENDIF
			.lLoggingIsOn = .lLoggingIsOn AND NOT EMPTY(.cLogFile)
			IF .lLoggingIsOn THEN
				SET ALTERNATE TO (.cLogFile) ADDITIVE
				SET ALTERNATE ON
			ENDIF
			* re-init date properties after all SETTINGS
			.dtNow = DATETIME()
			.dToday = DATE()
			.cToday = DTOC(DATE())
			.cAttach = ''

			WAIT WINDOW NOWAIT "Opening Excel..."
			TRY
				.oExcel = CREATEOBJECT("excel.application")
				.oExcel.VISIBLE = .F.
			CATCH
				.oExcel = NULL
			ENDTRY
		ENDWITH
	ENDFUNC


	FUNCTION DESTROY
		WITH THIS
			IF .lLoggingIsOn  THEN
				SET ALTERNATE OFF
				SET ALTERNATE TO
			ENDIF
			IF TYPE('.oExcel') = 'O' AND NOT ISNULL(.oExcel) THEN
				.oExcel.QUIT()
			ENDIF
		ENDWITH
		DODEFAULT()
	ENDFUNC


	FUNCTION MAIN

		LPARAMETERS tcTRNumber, tcTRName, tcTReMail, tcCC

		LOCAL lnNumberOfErrors, lcSQL, lcSQL2, loError, lcTopBodyText, llValid
		LOCAL lcXLFileName, ldStartDate, ldEndDate, lcStartDate, lcEndDate, ldDate
		LOCAL lcSQLStartDate, lcSQLEndDate, lcXLFileName, lcSpecialtext
		LOCAL oWorkbook, oWorksheet, lnRow, lcRow, lnStartRow, lcStartRow, lnEndRow, lnPP, lcPP, lnNN, lcNN, lcJJ
		LOCAL lcColsToProcess, lnColumn, lcBoldColumn, i, j, lcBoldRow, luCellValue, lcTargetDirectory, ldPayDate, lcFiletoSaveAs
		LOCAL lcFileDate, lcDivision, lcRateType, lcTitle, llSaveAgain, lcPayCode, lcOrderBy
		LOCAL lcTRName, lcTReMail, lcTRNumber, lcCC, lcTestCC, lcFoxProTimeReviewerTable, lcMissingApprovals

		WITH THIS

			TRY
				lnNumberOfErrors = 0
				lcTopBodyText = ''
				lcTRNumber = tcTRNumber
				lcTRName = ALLTRIM(tcTRName)
				lcTReMail = ALLTRIM(tcTReMail)
				lcCC = ALLTRIM(tcCC)

				* repeat some setup
				SET CONSOLE OFF
				SET TALK OFF
				*.cCC = 'Mark Bennett <mbennett@fmiint.com>, Lucille Waldrip <lwaldrip@fmiint.com>'
				.cCC = 'mbennett@fmiint.com'
				.cStartTime = TTOC(DATETIME())
				.dtNow = DATETIME()
				.cAttach = ''

				IF .lTestMode THEN
					.cSendTo = 'mbennett@fmiint.com'
					IF NOT EMPTY(lcCC) THEN
						lcTestCC = lcCC + ", " + .cCC
					ELSE
						lcTestCC = .cCC
					ENDIF
					.cCC = ""
				ELSE
					.cSendTo = lcTReMail
					IF NOT EMPTY(lcCC) THEN
						.cCC = lcCC + ", " + .cCC
					ENDIF
				ENDIF

				.cBodyText = ""

				*.TrackProgress('==================================================================================================================', LOGIT+SENDIT)
				.TrackProgress('KRONOS TIME REVIEWER REPORT process started....', LOGIT+SENDIT+NOWAITIT)
				.TrackProgress('COMPUTERNAME = ' + .cCOMPUTERNAME, LOGIT+SENDIT)
				.TrackProgress('USERNAME = ' + .cUSERNAME, LOGIT+SENDIT)
				.TrackProgress('==================================================================================================================', LOGIT+SENDIT)
				IF .lTestMode THEN
					.TrackProgress('', LOGIT)
					.TrackProgress('==================================================================================================================', LOGIT+SENDIT)
					.TrackProgress('                        TEST MODE                        ', LOGIT+SENDIT)
					.TrackProgress('Time Reviewer # = ' + lcTRNumber, LOGIT+SENDIT)
					.TrackProgress('Time Reviewer Name = ' + lcTRName, LOGIT+SENDIT)
					.TrackProgress('cSendTo would be: ' + lcTReMail, LOGIT+SENDIT)
					.TrackProgress('cCC would be: ' + lcTestCC, LOGIT+SENDIT)
					.TrackProgress('==================================================================================================================', LOGIT+SENDIT)
				ENDIF
				.TrackProgress('', LOGIT)

				* calculate pay period start/end dates.
				* We go back from today until we hit a Saturday (DOW = 7) , which is the end day of the previous pay period.
				* then we subtract 6 days from that to get the prior Sunday (DOW = 1), which is the start day of the previous pay period.
				ldDate = .dToday


***************************************************
* force different date 
*IF .lTestMode THEN
*!*		ldDate = {^2013-12-30}
*ENDIF
***************************************************
				
				
				DO WHILE DOW(ldDate,1) <> 7
					ldDate = ldDate - 1
				ENDDO
				ldEndDate = ldDate
				ldStartDate = ldDate - 6

*!*	********************************
*!*	* force custom date range
*!*	IF .lTestMode THEN
*!*		ldStartDate = {^2012-11-01}
*!*		ldEndDate = {^2012-11-30}
*!*	ENDIF
*!*	********************************


				SET DATE AMERICAN

				lcStartDate = STRTRAN(DTOC(ldStartDate),"/","-")
				lcEndDate = STRTRAN(DTOC(ldEndDate),"/","-")

				SET DATE YMD

				*	 create "YYYYMMDD" safe date format for use in SQL query
				* NOTE: for SQL we are going to use < EndDate ( not <= ), so we are going to add 1 to that date here for querying purposes.
				lcSQLStartDate = STRTRAN(DTOC(ldStartDate),"/","")
				lcSQLEndDate = STRTRAN(DTOC(ldEndDate + 1),"/","")

				.cSubject = 'KRONOS TIME REVIEWER REPORT for ' + lcTRName + ': ' + lcStartDate + " to " + lcEndDate

				IF USED('CURWTKHOURS') THEN
					USE IN CURWTKHOURS
				ENDIF

				IF USED('CURAPPROVALS') THEN
					USE IN CURAPPROVALS
				ENDIF

				IF USED('CURMISSINGAPPROVALS') THEN
					USE IN CURMISSINGAPPROVALS
				ENDIF


				* open FoxPro Time Reviewer table
				IF USED('TIMERVWRTABLE') THEN
					USE IN TIMERVWRTABLE
				ENDIF
				lcFoxProTimeReviewerTable = .cFoxProTimeReviewerTable

				USE (lcFoxProTimeReviewerTable) AGAIN IN 0 ALIAS TIMERVWRTABLE


				* main SQL
				
				IF lcTRNumber == 'BRIA' THEN
					* special report for Brian: he wants to see Divs 02, 03, 04, 06, 07
					lcSQL = ;
						" SELECT " + ;
						" D.LABORLEV2NM AS DIVISION, " + ;
						" D.LABORLEV3NM AS DEPT, " + ;
						" D.LABORLEV7NM AS TIMERVWR, " + ;
						" C.FULLNM AS EMPLOYEE, " + ;
						" C.PERSONNUM AS FILE_NUM, " + ;
						" C.PERSONID, " + ;
						" E.ABBREVIATIONCHAR AS PAYCODE, " + ;
						" E.NAME AS PAYCODEDESC, " + ;
						" (SUM(A.DURATIONSECSQTY) / 3600.00) AS TOTHOURS, " + ;
						" SUM(A.MONEYAMT) AS TOTPAY " + ;
						" FROM WFCTOTAL A " + ;
						" JOIN WTKEMPLOYEE B " + ;
						" ON B.EMPLOYEEID = A.EMPLOYEEID " + ;
						" JOIN PERSON C " + ;
						" ON C.PERSONID = B.PERSONID " + ;
						" JOIN LABORACCT D " + ;
						" ON D.LABORACCTID = A.LABORACCTID " + ;
						" JOIN PAYCODE E " + ;
						" ON E.PAYCODEID = A.PAYCODEID " + ;
						" WHERE (A.APPLYDTM >= '" + lcSQLStartDate + "')" + ;
						" AND (A.APPLYDTM < '" + lcSQLEndDate + "')" + ;
						" AND (D.LABORLEV1NM = 'SXI') " + ;
						" AND (D.LABORLEV2NM IN ('02','03','04','06','07')) " + ;
						" AND (D.LABORLEV7NM <> 9999)" + ;
						" GROUP BY D.LABORLEV2NM, D.LABORLEV3NM, D.LABORLEV7NM, C.FULLNM, C.PERSONNUM, C.PERSONID, E.ABBREVIATIONCHAR, E.NAME " + ;
						" ORDER BY 1, 2, 3, 4, 5, 7 "					
				ELSE
					* standard case
					lcSQL = ;
						" SELECT " + ;
						" D.LABORLEV2NM AS DIVISION, " + ;
						" D.LABORLEV3NM AS DEPT, " + ;
						" D.LABORLEV7NM AS TIMERVWR, " + ;
						" C.FULLNM AS EMPLOYEE, " + ;
						" C.PERSONNUM AS FILE_NUM, " + ;
						" C.PERSONID, " + ;
						" E.ABBREVIATIONCHAR AS PAYCODE, " + ;
						" E.NAME AS PAYCODEDESC, " + ;
						" (SUM(A.DURATIONSECSQTY) / 3600.00) AS TOTHOURS, " + ;
						" SUM(A.MONEYAMT) AS TOTPAY " + ;
						" FROM WFCTOTAL A " + ;
						" JOIN WTKEMPLOYEE B " + ;
						" ON B.EMPLOYEEID = A.EMPLOYEEID " + ;
						" JOIN PERSON C " + ;
						" ON C.PERSONID = B.PERSONID " + ;
						" JOIN LABORACCT D " + ;
						" ON D.LABORACCTID = A.LABORACCTID " + ;
						" JOIN PAYCODE E " + ;
						" ON E.PAYCODEID = A.PAYCODEID " + ;
						" WHERE (A.APPLYDTM >= '" + lcSQLStartDate + "')" + ;
						" AND (A.APPLYDTM < '" + lcSQLEndDate + "')" + ;
						" AND (D.LABORLEV1NM = 'SXI') " + ;
						" AND (D.LABORLEV7NM = " + lcTRNumber + ")" + ;
						" GROUP BY D.LABORLEV2NM, D.LABORLEV3NM, D.LABORLEV7NM, C.FULLNM, C.PERSONNUM, C.PERSONID, E.ABBREVIATIONCHAR, E.NAME " + ;
						" ORDER BY 1, 2, 3, 4, 5, 7 "					
				ENDIF && lcTRNumber == 'BRIA'

				* SQL for getting Dates approved by Managers for the pay period
				lcSQL2 = ;
					" SELECT " + ;
					" PERSONID, APPROVALDTM, MANAGERID " + ;
					" FROM MGRAPPROVAL " + ;
					" WHERE ( APPROVALDTM >= '" + lcSQLStartDate + "' )" + ;
					"   AND ( APPROVALDTM <= '" + lcSQLEndDate + "' )" + ;
					" ORDER BY PERSONID, APPROVALDTM "

				* 5/16/06 MB: Now using APPLYDTM instead of ADJSTARTDTM to solve problem where hours crossing Midnight Saturday were not being reported.
				*					" WHERE (A.ADJSTARTDTM >= '" + lcSQLStartDate + "')" + ;
				*					" AND (A.ADJSTARTDTM < '" + lcSQLEndDate + "')" + ;

				IF .lTestMode THEN
					.TrackProgress('', LOGIT+SENDIT)
					.TrackProgress('lcSQL =' + lcSQL, LOGIT+SENDIT)
					.TrackProgress('', LOGIT+SENDIT)
					.TrackProgress('lcSQL2 =' + lcSQL2, LOGIT+SENDIT)
				ENDIF

				.TrackProgress('Connecting to WTK System....', LOGIT+SENDIT)

				* get ss#s from ADP
				OPEN DATABASE F:\UTIL\KRONOS\KRONOSEDI

				*.nSQLHandle = SQLCONNECT("KRONOSEDI","tkcsowner","tkcsowner")
				.nSQLHandle = SQLCONNECT("WTK_ON_PAYROLL2","sa","kr0n1tes")

				IF USED('CURWTKHOURS') THEN
					USE IN CURWTKHOURS
				ENDIF

				IF .nSQLHandle > 0 THEN

					*IF .ExecSQL(lcSQL, 'CURWTKHOURS', RETURN_DATA_MANDATORY) THEN
					IF .ExecSQL(lcSQL, 'CURWTKHOURS', RETURN_DATA_NOT_MANDATORY) AND ;
							.ExecSQL(lcSQL2, 'CURAPPROVALS', RETURN_DATA_NOT_MANDATORY) THEN

						IF USED('CURWTKHOURS') AND (NOT EOF('CURWTKHOURS')) THEN


*!*	SELECT CURWTKHOURS
*!*	BROWSE

							SELECT CURWTKHOURS
							INDEX ON DIVISION TAG TDIV
							INDEX ON DEPT TAG TDEPT
							INDEX ON EMPLOYEE TAG TEMPLOYEE
							INDEX ON FILE_NUM TAG TFILE_NUM

							****** horizontalize cursor so paycodes are in separate columns

							IF USED('CUREMPLOYEES') THEN
								USE IN CUREMPLOYEES
							ENDIF
							
							* SORT REPORT BY EE NAME FOR CESAR, PER CARLINA 5/4/2012 mb
							IF INLIST(lcTRNumber,'3105') THEN
								lcOrderBy = "EMPLOYEE"
							ELSE
								lcOrderBy = "FILE_NUM"
							ENDIF

							SELECT ;
								DIVISION, ;
								DEPT, ;
								EMPLOYEE, ;
								FILE_NUM, ;
								PERSONID, ;
								TIMERVWR, ;
								0000.00 AS REG_HRS, ;
								0000.00 AS GTD_HRS, ;
								0000.00 AS SD1_HRS, ;
								0000.00 AS SPP_HRS, ;
								0000.00 AS OT_HRS, ;
								0000.00 AS OT20_HRS, ;
								0000.00 AS SICK_HRS, ;
								0000.00 AS VAC_HRS, ;
								0000.00 AS VAC_RLVR_HRS, ;
								0000.00 AS HDAY_HRS, ;
								0000.00 AS PP_VAC_HRS, ;
								0000.00 AS PP_HDAY_HRS, ;
								0000.00 AS PP_BDAY_HRS, ;
								0000.00 AS OFFICE_CLOSED_HRS, ;
								0000.00 AS BDAY_HRS, ;
								0000.00 AS PERS_HRS, ;
								0000.00 AS BDAY_ADV, ;
								0000.00 AS COMP_HRS, ;
								0000.00 AS JURY_HRS, ;
								0000.00 AS BREAVE_HRS, ;
								0000.00 AS OTHER_HRS, ;
								0000.00 AS RETRO_OTH, ;
								0000.00 AS OTHER_PAY, ;
								0000.00 AS CADRVR_PAY, ;
								0000.00 AS PREMLUNCH ;
								FROM CURWTKHOURS ;
								INTO CURSOR CUREMPLOYEES ;
								WHERE TIMERVWR IN ;
								(SELECT TIMERVWR FROM TIMERVWRTABLE WHERE LHOURLY) ;
								GROUP BY DIVISION, DEPT, EMPLOYEE, FILE_NUM ;
								ORDER BY &lcOrderBy. ;
								READWRITE
								

							lcMissingApprovals = ""
							IF USED('CURAPPROVALS') AND NOT EOF('CURAPPROVALS') THEN
								* we got list of approved dates from Kronos...
								* identify CUREMPLOYEES employees who did not have approvals
								SELECT EMPLOYEE,  LEFT(FILE_NUM,4) AS FILE_NUM, LEFT(TIMERVWR,4) AS TIMERVWR, PERSONID, SPACE(30) AS TRNAME ;
									FROM CUREMPLOYEES ;
									INTO CURSOR CURMISSINGAPPROVALS ;
									WHERE PERSONID NOT IN ( SELECT PERSONID FROM CURAPPROVALS ) ;
									ORDER BY EMPLOYEE ;
									READWRITE

								IF USED('CURMISSINGAPPROVALS') AND NOT EOF('CURMISSINGAPPROVALS') THEN

									* populate Time Reviewer name column
									SELECT CURMISSINGAPPROVALS
									SCAN
										SELECT TIMERVWRTABLE
										LOCATE FOR TIMERVWR == CURMISSINGAPPROVALS.TIMERVWR
										IF FOUND() THEN
											REPLACE CURMISSINGAPPROVALS.TRNAME WITH ALLTRIM(TIMERVWRTABLE.NAME)
										ENDIF
									ENDSCAN

									SELECT TIMERVWRTABLE
									LOCATE

									* build a list for email body
									SELECT CURMISSINGAPPROVALS
									SCAN
										lcMissingApprovals = lcMissingApprovals + CRLF + ;
											LEFT(CURMISSINGAPPROVALS.EMPLOYEE,40) + " " + ;
											CURMISSINGAPPROVALS.FILE_NUM + "  " + ;
											CURMISSINGAPPROVALS.TRNAME + "  # " + ;
											CURMISSINGAPPROVALS.TIMERVWR
									ENDSCAN

									IF NOT EMPTY(lcMissingApprovals) THEN
										lcMissingApprovals = CRLF + ;
											"WARNING: the following employees' timecards have not been approved!" + CRLF + CRLF + ;
											"Please complete their timecard reviews and be sure to approve each timecard." + CRLF + ;
											"If you edit any timecards, let Mark Bennett know, so he can rerun this report." + CRLF + CRLF + ;
											"( but you don't need to tell Mark if all you did was approve the timecards )." + CRLF + CRLF + ;
											"NAME                                     FILE# TIME REVIEWER" + CRLF + ;
											"---------------------------------------- ----- --------------------------------------" + ;
											lcMissingApprovals
									ENDIF

								ENDIF
							ENDIF  &&  USED('CURAPPROVALS') AND NOT EOF('CURAPPROVALS')



							* populate hours/code fields in main cursor
							SELECT CUREMPLOYEES
							SCAN
								SCATTER MEMVAR
								STORE 0000.00 TO ;
									m.REG_HRS, ;
									m.GTD_HRS, ;
									m.SD1_HRS, ;
									m.SPP_HRS, ;
									m.OT_HRS, ;
									m.OT20_HRS, ;
									m.SICK_HRS, ;
									m.VAC_HRS, ;
									m.VAC_RLVR_HRS, ;
									m.HDAY_HRS, ;
									m.PP_VAC_HRS, ;
									m.PP_HDAY_HRS, ;
									m.PERS_HRS, ;
									m.PP_BDAY_HRS, ;
									m.OFFICE_CLOSED_HRS, ;
									m.BDAY_HRS, ;
									m.BDAY_ADV, ;
									m.COMP_HRS, ;
									m.JURY_HRS, ;
									m.BREAVE_HRS, ;
									m.OTHER_HRS, ;
									m.RETRO_OTH, ;
									m.OTHER_PAY, ;
									m.CADRVR_PAY, ;
									m.PREMLUNCH
									
								SELECT CURWTKHOURS
								SCAN FOR ;
										DIVISION = CUREMPLOYEES.DIVISION AND ;
										DEPT = CUREMPLOYEES.DEPT AND ;
										EMPLOYEE = CUREMPLOYEES.EMPLOYEE AND ;
										FILE_NUM = CUREMPLOYEES.FILE_NUM
									lcPayCode = UPPER(ALLTRIM(CURWTKHOURS.PAYCODE))
									DO CASE
										CASE lcPayCode == "REG"
											m.REG_HRS = m.REG_HRS + CURWTKHOURS.TOTHOURS
										CASE lcPayCode == "OTM"
											m.OT_HRS = m.OT_HRS + CURWTKHOURS.TOTHOURS
										CASE lcPayCode == "DTM"
											m.OT20_HRS = m.OT20_HRS + CURWTKHOURS.TOTHOURS
										CASE INLIST(lcPayCode,"VAC","VH7","VAD","VA7")
											m.VAC_HRS = m.VAC_HRS + CURWTKHOURS.TOTHOURS
										CASE lcPayCode == "RVP"
											* Vacation Rollovers
											m.VAC_RLVR_HRS = m.VAC_RLVR_HRS + CURWTKHOURS.TOTHOURS
										CASE INLIST(lcPayCode,"HOL","HAD")
											m.HDAY_HRS = m.HDAY_HRS + CURWTKHOURS.TOTHOURS
										*CASE lcPayCode == "SIC"
										CASE INLIST(lcPayCode,"SIC","SC7","CDS")
											m.SICK_HRS = m.SICK_HRS + CURWTKHOURS.TOTHOURS
										CASE INLIST(lcPayCode,"BDY","BAD")
											m.BDAY_HRS = m.BDAY_HRS + CURWTKHOURS.TOTHOURS
											IF lcPayCode == "BAD"
												m.BDAY_ADV = m.BDAY_ADV + CURWTKHOURS.TOTHOURS
											ENDIF
										CASE INLIST(lcPayCode,"PRS","PAD")
											m.PERS_HRS = m.PERS_HRS + CURWTKHOURS.TOTHOURS
										CASE lcPayCode == "CMP"
											m.COMP_HRS = m.COMP_HRS + CURWTKHOURS.TOTHOURS
										CASE lcPayCode == "JUR"
											m.JURY_HRS = m.JURY_HRS + CURWTKHOURS.TOTHOURS
										CASE lcPayCode == "BRV"
											m.BREAVE_HRS = m.BREAVE_HRS + CURWTKHOURS.TOTHOURS
										CASE lcPayCode == "GTD"
											m.GTD_HRS = m.GTD_HRS + CURWTKHOURS.TOTHOURS
										CASE lcPayCode == "OTH"
											m.OTHER_HRS = m.OTHER_HRS + CURWTKHOURS.TOTHOURS
										CASE lcPayCode == "SD1"
											m.SD1_HRS = m.SD1_HRS + CURWTKHOURS.TOTHOURS
										CASE lcPayCode == "PPV"
											m.PP_VAC_HRS = m.PP_VAC_HRS + CURWTKHOURS.TOTHOURS
										CASE lcPayCode == "PPH"
											m.PP_HDAY_HRS = m.PP_HDAY_HRS + CURWTKHOURS.TOTHOURS
										CASE lcPayCode == "PPB"
											m.PP_BDAY_HRS = m.PP_BDAY_HRS + CURWTKHOURS.TOTHOURS
										CASE lcPayCode == "OFF"
											m.OFFICE_CLOSED_HRS = m.OFFICE_CLOSED_HRS + CURWTKHOURS.TOTHOURS
										CASE lcPayCode == "SPP"
											m.SPP_HRS = m.SPP_HRS + CURWTKHOURS.TOTHOURS
										CASE lcPayCode == "OTP"
											m.OTHER_PAY = m.OTHER_PAY + CURWTKHOURS.TOTPAY
										CASE lcPayCode == "RTR"
											m.RETRO_OTH = m.RETRO_OTH + CURWTKHOURS.TOTPAY
										CASE INLIST(lcPayCode,"CAI","NJI")
											m.CADRVR_PAY = m.CADRVR_PAY + CURWTKHOURS.TOTPAY
										CASE lcPayCode == "PPL"
											m.PREMLUNCH = m.PREMLUNCH + CURWTKHOURS.TOTPAY
										OTHERWISE
											* NOTHING
									ENDCASE

								ENDSCAN
								* accumulate totals
								SELECT CUREMPLOYEES
								GATHER MEMVAR
							ENDSCAN

							WAIT WINDOW NOWAIT "Preparing data..."							

							SELECT CUREMPLOYEES

							GOTO TOP
							IF EOF() THEN
								.TrackProgress("There was no data to export!", LOGIT+SENDIT)
							ELSE

								lcRateType = "HOURLY"

								*!*								WAIT WINDOW NOWAIT "Opening Excel..."
								*!*								oExcel = CREATEOBJECT("excel.application")

								oWorkbook = .oExcel.workbooks.OPEN("F:\UTIL\KRONOS\TEMPLATES\KronosTRReport.XLS")

								***********************************************************************************************************************
								***********************************************************************************************************************
								WAIT WINDOW NOWAIT "Looking for target directory..."
								* see if target directory exists, and, if not, create it
								* e.g. F:\timeclk\adpfiles\01-07-2005
								ldPayDate = ldEndDate + 6

								lcFileDate = PADL(MONTH(ldPayDate),2,"0") + "-"  + PADL(DAY(ldPayDate),2,"0") + "-" + PADL(YEAR(ldPayDate),4,"0")

								lcTargetDirectory = "F:\UTIL\KRONOS\TRREPORTS\" + lcFileDate + "\"
								* for testing
								*lcTargetDirectory = "C:\TIMECLK\ADPFILES\" + lcFileDate + "\"

								* create directory if it doesn't exist
								IF NOT DIRECTORY(lcTargetDirectory) THEN
									MKDIR (lcTargetDirectory)
									WAIT WINDOW TIMEOUT 1 "Made Target Directory " + lcTargetDirectory
								ENDIF

								* if target directory exists, save there
								llSaveAgain = .F.
								IF DIRECTORY(lcTargetDirectory) THEN
									lcFiletoSaveAs = lcTargetDirectory + lcFileDate + " Kronos "  + lcRateType + " for " + lcTRName
									lcXLFileName = lcFiletoSaveAs + ".XLS"
									IF FILE(lcXLFileName) THEN
										DELETE FILE (lcXLFileName)
									ENDIF
									oWorkbook.SAVEAS(lcFiletoSaveAs)
									* set flag to save again after sheet is populated
									llSaveAgain = .T.
								ENDIF
								***********************************************************************************************************************
								***********************************************************************************************************************

								lnRow = 3
								lnStartRow = lnRow + 1
								lcStartRow = ALLTRIM(STR(lnStartRow))

								oWorksheet = oWorkbook.Worksheets[1]
								oWorksheet.RANGE("A" + lcStartRow,"X1000").clearcontents()

								oWorksheet.RANGE("A" + lcStartRow,"X1000").FONT.SIZE = 10
								oWorksheet.RANGE("A" + lcStartRow,"X1000").FONT.NAME = "Arial Narrow"
								oWorksheet.RANGE("A" + lcStartRow,"X1000").FONT.bold = .T.
								

								SET DATE AMERICAN

								lcTitle = "Kronos Payroll Summary for Pay Date: " + DTOC(ldPayDate) + ", Week of: " + lcStartDate + " - " + lcEndDate
								oWorksheet.RANGE("A1").VALUE = lcTitle
								oWorksheet.RANGE("A2").VALUE = "EEs for: " + lcTRName

								* main scan/processing
								SELECT CUREMPLOYEES
								STORE 0 TO num_salary
								SCAN

									lnRow = lnRow + 1
									lcRow = LTRIM(STR(lnRow))
									oWorksheet.RANGE("A" + lcRow).VALUE = "'" + CUREMPLOYEES.DIVISION
									oWorksheet.RANGE("B" + lcRow).VALUE = CUREMPLOYEES.DEPT
									oWorksheet.RANGE("C" + lcRow).VALUE = CUREMPLOYEES.EMPLOYEE
									oWorksheet.RANGE("D" + lcRow).VALUE = CUREMPLOYEES.FILE_NUM
									oWorksheet.RANGE("E" + lcRow).VALUE = CUREMPLOYEES.REG_HRS
									oWorksheet.RANGE("F" + lcRow).VALUE = CUREMPLOYEES.GTD_HRS
									oWorksheet.RANGE("G" + lcRow).VALUE = CUREMPLOYEES.SD1_HRS
									oWorksheet.RANGE("H" + lcRow).VALUE = CUREMPLOYEES.OT_HRS
									oWorksheet.RANGE("I" + lcRow).VALUE = CUREMPLOYEES.OT20_HRS
									oWorksheet.RANGE("J" + lcRow).VALUE = CUREMPLOYEES.SICK_HRS
									*oWorksheet.RANGE("K" + lcRow).VALUE = CUREMPLOYEES.VAC_HRS
									oWorksheet.RANGE("K" + lcRow).VALUE = ( CUREMPLOYEES.VAC_HRS + CUREMPLOYEES.VAC_RLVR_HRS )
									oWorksheet.RANGE("L" + lcRow).VALUE = CUREMPLOYEES.HDAY_HRS
									*oWorksheet.RANGE("M" + lcRow).VALUE = CUREMPLOYEES.BDAY_HRS   
									oWorksheet.RANGE("M" + lcRow).VALUE = CUREMPLOYEES.PERS_HRS									
									
									oWorksheet.RANGE("N" + lcRow).VALUE = CUREMPLOYEES.COMP_HRS
									
									oWorksheet.RANGE("O" + lcRow).VALUE = CUREMPLOYEES.JURY_HRS
									oWorksheet.RANGE("P" + lcRow).VALUE = CUREMPLOYEES.BREAVE_HRS
									oWorksheet.RANGE("Q" + lcRow).VALUE = CUREMPLOYEES.OTHER_HRS
									oWorksheet.RANGE("R" + lcRow).VALUE = CUREMPLOYEES.PP_VAC_HRS
									oWorksheet.RANGE("S" + lcRow).VALUE = CUREMPLOYEES.PP_HDAY_HRS
									
									*oWorksheet.RANGE("T" + lcRow).VALUE = CUREMPLOYEES.PP_BDAY_HRS
									oWorksheet.RANGE("T" + lcRow).VALUE = CUREMPLOYEES.OFFICE_CLOSED_HRS
									
									oWorksheet.RANGE("U" + lcRow).VALUE = CUREMPLOYEES.RETRO_OTH
									oWorksheet.RANGE("V" + lcRow).VALUE = CUREMPLOYEES.OTHER_PAY
									oWorksheet.RANGE("W" + lcRow).VALUE = CUREMPLOYEES.CADRVR_PAY
									oWorksheet.RANGE("X" + lcRow).VALUE = CUREMPLOYEES.PREMLUNCH


									oWorksheet.RANGE("A" + lcRow,"X" + lcRow).Interior.ColorIndex = IIF(((lnRow % 2) = 1),15,0)

									* insert NOPAYs where all values (except prepaid hrs) were zero.
									IF CUREMPLOYEES.REG_HRS = 0.0 ;
											AND CUREMPLOYEES.GTD_HRS = 0.0 ;
											AND CUREMPLOYEES.SD1_HRS = 0.0 ;
											AND CUREMPLOYEES.OT_HRS = 0.0 ;
											AND CUREMPLOYEES.OT20_HRS = 0.0 ;
											AND CUREMPLOYEES.SICK_HRS = 0.0 ;
											AND CUREMPLOYEES.VAC_HRS = 0.0 ;
											AND CUREMPLOYEES.VAC_RLVR_HRS = 0.0 ;
											AND CUREMPLOYEES.HDAY_HRS = 0.0 ;
											AND CUREMPLOYEES.BDAY_HRS = 0.0 ;
											AND CUREMPLOYEES.PERS_HRS = 0.0 ;
											AND CUREMPLOYEES.COMP_HRS = 0.0 ;
											AND CUREMPLOYEES.JURY_HRS = 0.0 ;
											AND CUREMPLOYEES.BREAVE_HRS = 0.0 ;
											AND CUREMPLOYEES.OTHER_HRS = 0.0 ;
											AND CUREMPLOYEES.RETRO_OTH = 0.0 ;
											AND CUREMPLOYEES.OTHER_PAY = 0.0 ;
											AND CUREMPLOYEES.CADRVR_PAY = 0.0 ;
											AND CUREMPLOYEES.OFFICE_CLOSED_HRS = 0.0 ;
											AND CUREMPLOYEES.PREMLUNCH = 0.0 THEN
										oWorksheet.RANGE("E"+lcRow).VALUE="NOPAY"
									ENDIF
									******************************************

								ENDSCAN

								lnEndRow = lnRow
								lcEndRow = ALLTRIM(STR(lnEndRow))

								***********************************
								* 01/20/04 MB - do all the bolding/clearing of numeric columns here in one nested loop,
								* to eliminate a lot of extraneous code above.
								lcColsToProcess = "EFGHIJKLMNOPQRSTUVWX"
								FOR i = 1 TO LEN(lcColsToProcess)
									lcBoldColumn = SUBSTR(lcColsToProcess,i,1)
									FOR j = lnStartRow TO lnEndRow
										lcBoldRow = ALLTRIM(STR(j))
										luCellValue = oWorksheet.RANGE(lcBoldColumn + lcBoldRow).VALUE
										IF TYPE([luCellValue])= "N" THEN
											IF luCellValue > 0.0 THEN
												oWorksheet.RANGE(lcBoldColumn + lcBoldRow).FONT.bold = .T.
											ELSE
												oWorksheet.RANGE(lcBoldColumn + lcBoldRow).VALUE = ""
											ENDIF
										ENDIF
									ENDFOR  && j
								ENDFOR  && i

								* FORMAT LAST THREE COLUMNS FOR CURRENCY
								oWorksheet.RANGE("U" + lcStartRow,"X1000").NumberFormat = "$#,##0.00"								

								***********************************

								*!*								lcJJ = lcRow

								lnRow = lnRow + 2
								lcRow = LTRIM(STR(lnRow))
								oWorksheet.RANGE("C" + lcRow).VALUE = "Control Numbers"
								lnTotalsRow = lnRow + 1
								lcRow = LTRIM(STR(lnTotalsRow))

								oWorksheet.RANGE("A" + lcRow,"V" + lcRow).FONT.SIZE = 9

								oWorksheet.RANGE("D" + lcRow).formula = "=sum(D" + lcStartRow + ":D" + lcEndRow + ")"
								oWorksheet.RANGE("E" + lcRow).formula = "=sum(E" + lcStartRow + ":E" + lcEndRow + ")"
								oWorksheet.RANGE("F" + lcRow).formula = "=sum(F" + lcStartRow + ":F" + lcEndRow + ")"
								oWorksheet.RANGE("G" + lcRow).formula = "=sum(G" + lcStartRow + ":G" + lcEndRow + ")"
								oWorksheet.RANGE("H" + lcRow).formula = "=sum(H" + lcStartRow + ":H" + lcEndRow + ")"
								oWorksheet.RANGE("I" + lcRow).formula = "=sum(I" + lcStartRow + ":I" + lcEndRow + ")"
								oWorksheet.RANGE("J" + lcRow).formula = "=sum(J" + lcStartRow + ":J" + lcEndRow + ")"
								oWorksheet.RANGE("K" + lcRow).formula = "=sum(K" + lcStartRow + ":K" + lcEndRow + ")"
								oWorksheet.RANGE("L" + lcRow).formula = "=sum(L" + lcStartRow + ":L" + lcEndRow + ")"
								oWorksheet.RANGE("M" + lcRow).formula = "=sum(M" + lcStartRow + ":M" + lcEndRow + ")"
								oWorksheet.RANGE("N" + lcRow).formula = "=sum(N" + lcStartRow + ":N" + lcEndRow + ")"
								oWorksheet.RANGE("O" + lcRow).formula = "=sum(O" + lcStartRow + ":O" + lcEndRow + ")"
								oWorksheet.RANGE("P" + lcRow).formula = "=sum(P" + lcStartRow + ":P" + lcEndRow + ")"
								oWorksheet.RANGE("Q" + lcRow).formula = "=sum(Q" + lcStartRow + ":Q" + lcEndRow + ")"
								oWorksheet.RANGE("R" + lcRow).formula = "=sum(R" + lcStartRow + ":R" + lcEndRow + ")"
								oWorksheet.RANGE("S" + lcRow).formula = "=sum(S" + lcStartRow + ":S" + lcEndRow + ")"
								oWorksheet.RANGE("T" + lcRow).formula = "=sum(T" + lcStartRow + ":T" + lcEndRow + ")"
								oWorksheet.RANGE("U" + lcRow).formula = "=sum(U" + lcStartRow + ":U" + lcEndRow + ")"
								oWorksheet.RANGE("V" + lcRow).formula = "=sum(V" + lcStartRow + ":V" + lcEndRow + ")"
								oWorksheet.RANGE("W" + lcRow).formula = "=sum(W" + lcStartRow + ":W" + lcEndRow + ")"
								oWorksheet.RANGE("X" + lcRow).formula = "=sum(X" + lcStartRow + ":X" + lcEndRow + ")"

								oWorksheet.RANGE("C" + lcRow).VALUE = "Salaries= " + ALLTRIM(STR(num_salary))

								* make sure the reference numbers come out on white background
								oWorksheet.RANGE("A" + lcRow,"X" + lcRow).Interior.ColorIndex = 0

								lnRow = lnRow + 1
								lcRow = LTRIM(STR(lnRow))
								*oWorksheet.RANGE("A" + lcStartRow,"T" + lcRow).SELECT
								oWorksheet.PageSetup.PrintArea = "$A$"  + lcStartRow + ":$X$" + lcRow

								* subtract val of file# from column total for all NOPAYs
								FOR lnRow = lnStartRow TO lnEndRow
									lcRow = ALLTRIM(STR(lnRow))
									IF TYPE([oworksheet.range("E" + lcRow).value]) = "C"
										IF oWorksheet.RANGE("E" + lcRow).VALUE = "NOPAY"
											oWorksheet.RANGE("D" + ALLTRIM(STR(lnTotalsRow))).formula = ;
												oWorksheet.RANGE("D" + ALLTRIM(STR(lnTotalsRow))).formula + "-" + ;
												ALLTRIM(STR(oWorksheet.RANGE("D" + lcRow).VALUE))
										ENDIF
									ENDIF
								ENDFOR

								* save again
								IF llSaveAgain THEN
									oWorkbook.SAVE()
								ENDIF
								oWorkbook.CLOSE()

								*******************************************************************************************
								*******************************************************************************************

								lcTopBodyText = ;
									"The attached spreadsheet report shows the payroll information for your employees " + ;
									"which is being sent to ADP for the prior pay period."
								lcTopBodyText = lcTopBodyText + CRLF + CRLF + ;
									"Print, review, and sign the report, then send it to Human Resources."
								lcTopBodyText = lcTopBodyText + CRLF + CRLF + ;
									"If you see anything wrong with the report, contact Human Resources immediately."

								*!*									IF .lSpecial THEN
								*!*
								*!*										TEXT TO lcSpecialtext
								*!*	To all:
								*!*	We are resending these e-mails for your approval.  Please note that both Connie and Lucille have audited the time punches and made any changes that were necessary.  These changes were made only to those employees that were punching in prior/after the 7 minute rule.  That one minute in California costs us 15 minutes of overtime!!!  We can only do these changes for last week, and this week, after that the overtime must be paid.  It will be up to each time-reviewer to speak to their employees, and take further action if necessary if it continues.  Please review this report with the previous one sent, then sign this one and write, �REVISED� on it, and forward to Ericka.  If you see something that does not look correct on this report, contact Lucille immediately, as payroll will run at exactly 12:00 o�clock EST. today.
								*!*	Thanks for all your help this week, for a job well done.  Our first week of �Kronos� went rather smoothly.!!

								*!*										ENDTEXT
								*!*
								*!*										lcTopBodyText = lcSpecialtext + CRLF + CRLF + lcTopBodyText
								*!*
								*!*									ENDIF  &&  .lSpecial


								* now open the ADP Payroll export file and build a list of File#s that are in it, IF THE FILE EXISTS!


								*IF FILE("F:\UTIL\KRONOS\EDI\ADPFILES\EPISXI99.CSV") AND NOT .lSpecial THEN
								IF FILE("F:\UTIL\KRONOS\EDI\ADPFILES\EPISXI99.CSV") THEN

									oWorkbook = .oExcel.workbooks.OPEN("F:\UTIL\KRONOS\EDI\ADPFILES\EPISXI99.CSV")
									oWorksheet = oWorkbook.Worksheets[1]

									IF USED('CURACTUALPAYEXPORT') THEN
										USE IN CURACTUALPAYEXPORT
									ENDIF

									CREATE CURSOR CURACTUALPAYEXPORT ( FILE_NUM INT )

									FOR lnRow = 2 TO 10000
										lcRow = ALLTRIM(STR(lnRow))
										* get File# from Column C for any row which has "EPIP50" in Col B
										luValue = oWorksheet.RANGE("B" + lcRow).VALUE
										IF (NOT ISNULL(luValue)) AND (NOT EMPTY(luValue)) AND (TYPE('luValue') = 'C') AND (ALLTRIM(luValue) = "EPIP50") THEN
											m.FILE_NUM = oWorksheet.RANGE("C" + lcRow).VALUE
											INSERT INTO CURACTUALPAYEXPORT FROM MEMVAR
										ELSE
											* we're done, stop looking
											*SET STEP ON
											EXIT FOR
										ENDIF
									ENDFOR

									*SELECT CURACTUALPAYEXPORT
									*brow

									*.oExcel.VISIBLE = .T.

									*!*								* save again
									*!*								IF llSaveAgain THEN
									*!*									oWorkbook.SAVE()
									*!*								ENDIF

									*.oExcel.QUIT()


									*******************************************************************************************
									*******************************************************************************************

									* create a cursor of CUREMPLOYEES file#s that are *not* in ADP Payroll export file

									IF USED('CURMISSING') THEN
										USE IN CURMISSING
									ENDIF

									SELECT DIVISION, DEPT, EMPLOYEE, FILE_NUM  ;
										FROM CUREMPLOYEES ;
										INTO CURSOR CURMISSING ;
										WHERE INT(VAL(FILE_NUM)) NOT IN (SELECT FILE_NUM FROM CURACTUALPAYEXPORT) ;
										ORDER BY 1, 2, 3

									IF (NOT USED('CURMISSING')) OR EOF('CURMISSING') THEN
										lcTopBodyText = "DATA CHECK: All Employees in this spreadsheet were in the Kronos-->ADP Payroll Export File." + CRLF
									ELSE
										lcTopBodyText = "DATA WARNING: All Employees in this spreadsheet were NOT in the Kronos-->ADP Payroll Export File!" + CRLF + ;
											"The list of Employees follows: " + CRLF + CRLF + ;
											"NAME                                     FILE#" + CRLF + ;
											"---------------------------------------- -----"
										SELECT CURMISSING
										SCAN
											lcTopBodyText = lcTopBodyText + CRLF + LEFT(CURMISSING.EMPLOYEE,40) + " " + CURMISSING.FILE_NUM
										ENDSCAN
									ENDIF

								ELSE
									* nothing
								ENDIF && FILE("F:\UTIL\KRONOS\EDI\ADPFILES\EPISXI99.CSV")

								*******************************************************************************************
								*******************************************************************************************
								*!*								lcTopBodyText = lcTopBodyText + CRLF+ CRLF + ;
								*!*									"==================================================================================================================" + ;
								*!*									CRLF + CRLF

							.cBodyText = lcTopBodyText + CRLF + ;
								"==================================================================================================================" + ;
								CRLF +lcMissingApprovals + CRLF + ;
								"==================================================================================================================" + ;
									CRLF + "<report log follows>" + ;
									CRLF + CRLF + .cBodyText

								*.oExcel.VISIBLE = .T.

								*.oExcel.QUIT()

							ENDIF  && EOF() CUREMPLOYEES

							IF FILE(lcXLFileName) THEN
								.cAttach = lcXLFileName
								.TrackProgress('Created Payroll File : ' + lcXLFileName, LOGIT+SENDIT)
								.TrackProgress('KRONOS TIME REVIEWER REPORT process ended normally.', LOGIT+SENDIT+NOWAITIT)
							ELSE
								.TrackProgress('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!', LOGIT+SENDIT)
								.TrackProgress("ERROR: There was a problem Creating Payroll File : " + lcXLFileName, LOGIT+SENDIT)
								.TrackProgress('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!', LOGIT+SENDIT)
							ENDIF

						ELSE

							.cAttach = ''
							.TrackProgress("We didn't find any employees in Kronos for you this week!", LOGIT+SENDIT)

						ENDIF && USED('CURWTKHOURS') AND (NOT EOF('CURWTKHOURS')) THEN

					ENDIF  &&  .ExecSQL(lcSQL, 'CURWTKEMPLOYEE', RETURN_DATA_MANDATORY)


					IF USED('CURWTKHOURS') THEN
						USE IN CURWTKHOURS
					ENDIF
					IF USED('CURMISSING') THEN
						USE IN CURMISSING
					ENDIF
					IF USED('CURACTUALPAYEXPORT') THEN
						USE IN CURACTUALPAYEXPORT
					ENDIF
					IF USED('CUREMPLOYEES') THEN
						USE IN CUREMPLOYEES
					ENDIF

					SQLDISCONNECT(.nSQLHandle)

					*CLOSE DATABASES ALL
				ELSE
					* connection error
					.TrackProgress('Unable to connect to WTK System.', LOGIT+SENDIT)
				ENDIF   &&  .nSQLHandle > 0
			CATCH TO loError

				.TrackProgress('There was an error.',LOGIT+SENDIT)
				.TrackProgress('The Error # is: ' + TRANSFORM(loError.ErrorNo), LOGIT+SENDIT)
				.TrackProgress('The Error Message is: ' + TRANSFORM(loError.MESSAGE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred in Program: ' + TRANSFORM(loError.PROCEDURE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred at line #: ' + TRANSFORM(loError.LINENO), LOGIT+SENDIT)
				lnNumberOfErrors = lnNumberOfErrors + 1
				IF TYPE('.oExcel') = 'O' AND NOT ISNULL(.oExcel) THEN
					.oExcel.VISIBLE = .T.
				ENDIF
				IF .nSQLHandle > 0 THEN
					SQLDISCONNECT(.nSQLHandle)
				ENDIF
				*CLOSE DATA
				*CLOSE ALL

			ENDTRY

			*CLOSE DATA
			WAIT CLEAR
			***************** INTERNAL email results ******************************
			.TrackProgress('About to send status email.',LOGIT)
			.TrackProgress('==================================================================================================================', SENDIT)
			.TrackProgress('The logfile is: ' + .cLogFile, SENDIT)
			.TrackProgress('KRONOS TIME REVIEWER REPORT process started: ' + .cStartTime, LOGIT+SENDIT)
			.TrackProgress('KRONOS TIME REVIEWER REPORT process finished: ' + TTOC(DATETIME()), LOGIT+SENDIT)

			IF .lSendInternalEmailIsOn THEN
				* try to trap error from not having dartmail dll's registered on user's pc...
				TRY
					DO FORM dartmail2 WITH .cSendTo,.cFrom,.cSubject,.cCC,.cAttach,.cBodyText,"A"
					.TrackProgress('Sent status email.',LOGIT)
				CATCH TO loError
					=MESSAGEBOX('There was an error sending the status email. Please contact Mark Bennett.',0+64,"KRONOS TIME REVIEWER REPORT")
					.TrackProgress('There was an error sending the status email.',LOGIT)
					.TrackProgress('The Error # is: ' + TRANSFORM(loError.ErrorNo), LOGIT)
					.TrackProgress('The Error Message is: ' + TRANSFORM(loError.MESSAGE), LOGIT)
					.TrackProgress('The Error occurred in Program: ' + TRANSFORM(loError.PROCEDURE), LOGIT)
					.TrackProgress('The Error occurred at line #: ' + TRANSFORM(loError.LINENO), LOGIT)
					lnNumberOfErrors = lnNumberOfErrors + 1
					CLOSE DATA
				ENDTRY
			ELSE
				.TrackProgress('Did not send status email: THIS.lSendInternalEmailIsOn = FALSE.',LOGIT)
			ENDIF

		ENDWITH
		RETURN
	ENDFUNC && main


	FUNCTION ExecSQL
		LPARAMETERS tcSQL, tcCursorName, tlNoDataReturnedIsOkay
		LOCAL llRetval, lnResult
		WITH THIS
			* close target cursor if it's open
			IF USED(tcCursorName)
				USE IN (tcCursorName)
			ENDIF
			WAIT WINDOW "Fetching remote data; please wait..." NOWAIT
			lnResult = sqlexec(.nSQLHandle, tcSQL, tcCursorName)
			llRetval = ( lnResult > 0 )
			IF llRetval THEN
				* see if any data came back
				IF NOT tlNoDataReturnedIsOkay THEN
					IF (NOT USED(tcCursorName)) OR EOF(tcCursorName) THEN
						llRetval = .F.
						.TrackProgress('No data was returned by this query: ' +CRLF + tcSQL, LOGIT+SENDIT)
					ENDIF
				ENDIF
			ELSE
				.TrackProgress("Result Code [" + TRANSFORM(lnResult) + "] returned by this query: " + tcSQL, LOGIT+SENDIT)
			ENDIF
			WAIT CLEAR
			RETURN llRetval
		ENDWITH
	ENDFUNC


	FUNCTION SetSpecial
		THIS.lSpecial = .T.
	ENDFUNC


	PROCEDURE TrackProgress
		* do any combination of Wait Window, writing to logfile, and adding to body of email,
		* based on nFlags parameter.
		LPARAMETERS tcExpression, tnFlags
		WITH THIS
			IF BITAND(tnFlags,LOGIT) = LOGIT THEN
				IF .lLoggingIsOn THEN
					?
					? .cProcessName + "  " + TTOC(DATETIME()) + ": " + tcExpression
				ENDIF
			ENDIF
			IF .lWaitWindowIsOn THEN
				IF BITAND(tnFlags,NOWAITIT) = NOWAITIT THEN
					WAIT WINDOW tcExpression NOWAIT
				ENDIF
				IF BITAND(tnFlags,WAITIT) = WAITIT THEN
					WAIT WINDOW tcExpression TIMEOUT .nWaitWindowTimeout
				ENDIF
			ENDIF
			IF BITAND(tnFlags,SENDIT) = SENDIT THEN
				IF .lSendInternalEmailIsOn THEN
					.cBodyText = .cBodyText + tcExpression + CRLF + CRLF
				ENDIF
			ENDIF
		ENDWITH
	ENDPROC  &&  TrackProgress
	
	
	PROCEDURE GetSalariedEmailList
		LOCAL lcList
		lcList = ""
		USE f:\util\kronos\data\timervwr.dbf ALIAS tr AGAIN IN 0 SHARED
		SELECT tr
		SCAN FOR lactive AND (not lhourly)
			IF EMPTY(lcList) THEN
				lcList = ALLTRIM(tr.EMAIL)
			ELSE
				lcList = lcList + ", " + ALLTRIM(tr.EMAIL)
			ENDIF
		ENDSCAN
		_cliptext = lcList
		USE IN tr 
		RETURN
	ENDPROC


ENDDEFINE

