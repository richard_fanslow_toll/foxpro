STORE "" TO m.shipins,m.comments,m.echo
STORE DATE() TO m.wo_date,m.date_rcvd,m.adddt
STORE 0 TO m.wo_num,nPO,nPack
STORE 1 TO m.plid,m.inwologid
PUBLIC m.Style,cVendorPO,m.plinqty,m.plunitsqty,m.weight,m.cuft

m.addby = "FMI-PROC"
lTestProcess = .F.
lBrowFiles = lTesting
m.wo_date = DATE()
m.wo_num = 1
m.acctname = "S3 HOLDING, LLC"

SELECT tempmain
LOCATE
LOCATE FOR UPPER(tempmain.a) = "DESCRIPTION"
nHdr = RECNO()
m.Style = UPPER(ALLTRIM(tempmain.g))
LOCATE
m.reference = UPPER(ALLTRIM(tempmain.a))
LOCATE FOR UPPER(tempmain.a) = "VPO#"
cVendorPO = UPPER(ALLTRIM(STRTRAN(tempmain.a,"VPO#","")))
LOCATE
LOCATE FOR UPPER(tempmain.b) = "NO. OF"
SKIP 2
m.plinqty = INT(VAL(tempmain.b))
m.plunitsqty = INT(VAL(tempmain.g))
m.weight = INT(VAL(tempmain.j)*2.202) && Converted to lbs.
m.cuft = INT(VAL(tempmain.N)*35.315)  && Converted to cu.ft.
INSERT INTO xinwolog FROM MEMVAR
REPLACE xinwolog.comments WITH "VENDORPO*"+cVendorPO
SKIP
IF ALLTRIM(tempmain.c) = "SIZE"
	nBegin = RECNO()
ENDIF

COUNT TO nDetRecs FOR ALLTRIM(tempmain.c)="ASSORT" AND RECNO()>nBegin
FOR i = 1 TO nDetRecs && Processing of individual detail records
	SCAN FOR RECNO()>nBegin
		IF "-"$ALLT(tempmain.a)
			cRange = ALLTRIM(tempmain.a)
			ctnbegin = INT(VAL(LEFT(cRange,AT("-",cRange)-1)))
			ctnend = INT(VAL(ALLTRIM(SUBSTR(cRange,AT("-",cRange)+1))))
			nCtns = (ctnend-ctnbegin+1)
			EXIT
		ENDIF
	ENDSCAN
	LOCATE
	SCAN FOR RECNO()>nBegin
		IF "#"$ALLT(tempmain.b) AND !"PO#"$ALLTRIM(tempmain.b)
			cColor = ALLTRIM(tempmain.b)
			m.color = ALLTRIM(SUBSTR(cColor,AT("#",cColor)+1))
			cColorDesc = LEFT(cColor,AT("#",cColor)-1)
			EXIT
		ENDIF
	ENDSCAN

	LOCATE FOR ALLTRIM(tempmain.c)="ASSORT" AND RECNO()>nBegin
	nBegin = RECNO()
	cString = ALLTRIM(tempmain.c)
	m.pack = STRTRAN(ALLTRIM(SUBSTR(cString,AT("IN",cString)+2)),"PCS CARTON","")
	m.units = .F.
	m.id = ""
	SKIP -1
	STORE INT(VAL(tempmain.N)) TO m.totqty
	INSERT INTO xpl FROM MEMVAR

	m.units = .T.
	m.pack = "1"
	SELECT * FROM TKO_SIZELIST INTO CURSOR sizelist READWRITE
	SELECT tempmain
	SKIP -4 && Begin Detail Record Compilation

*	ASSERT .F. MESSAGE "Inside TEMPMAIN scan...debug"
	FOR jj = 1 TO 4
		FOR ii = 100 TO 106
			STORE CHR(ii) TO i1
			STORE INT(VAL(tempmain.&i1)) TO insertval
			REPLACE sizelist.totqty WITH insertval IN sizelist
			IF !EOF('sizelist')
				SKIP 1 IN sizelist
			ENDIF
		ENDFOR
		IF jj < 4
			SKIP 1 IN tempmain
		ENDIF
	ENDFOR
	SELECT sizelist
	SCAN FOR sizelist.totqty>0
		m.id = ALLTRIM(sizelist.waist)+"X"+ALLTRIM(sizelist.inseam)
		m.totqty = sizelist.totqty
		m.plid = m.plid + 1
		INSERT INTO xpl FROM MEMVAR
	ENDSCAN
	SELECT tempmain
ENDFOR

IF lBrowFiles
	WAIT WINDOW "Browsing INWOLOG/PL files..."+CHR(13)+"CANCEL after browsing if desired" TIMEOUT 3
	SELECT xinwolog
	LOCATE
	BROWSE
	SELECT xpl
	LOCATE
	BROWSE
ENDIF
