*!* FTPFILES.PRG
*!* Sets up OUTBOUND EDI Connection and
*!* sends confirmation or "bad FTP" eMails.

*First specify a variable called Mysite
PARAMETER lcwhichsite,tuserid

runack("FTPFILES")

LOCAL mysite

SET EXCL OFF
SET SAFETY OFF
SET TALK OFF

*Try

pnnotsent = 0
tsubject = ""
tmessage = ""
tattach = ""
lncount = 0
llisregatta = .F.
lcregattaloc = ""

*!* This traps any non-connects outside CuteFTP's handling
ON ERROR DO errorhandle WITH ERROR()

* do f:\main\prg\setup

IF !USED("ftpedilog")
	USE F:\edirouting\ftpedilog IN 0 ALIAS ftpedilog
ENDIF

IF !USED("ftpsetup")
	USE F:\edirouting\ftpsetup IN 0 ALIAS ftpsetup
ENDIF

SELECT ftpsetup

LOCATE FOR ALLTRIM(ftpsetup.transfer) = ALLTRIM(lcwhichsite)
IF !FOUND() && if transfer name does not exist in ftpsetup
	WAIT "This transfer "+lcwhichsite+ ;
		" is not defined in f:\edirouting\ftpsetup.dbf.... Aborting!" WINDOW TIMEOUT 3

	lcftpfile = "Aborted: Not defined in FTPSETUP..."
	lcftptype = "UNK"
	DO badftp

	tsubject= "Job Not Defined in FTPSETUP - FTP Aborted"
	tmessage = "The transfer named "+lcwhichsite+;
		" is not defined in f:\edirouting\ftpsetup.dbf.... FTP Aborted! Check FTPSETUP and repair"
	DO errorhandle

	RETURN
ELSE
	dpath = ALLTRIM(ftpsetup.localpath)

	IF !DIRECTORY(dpath) && if local folder is missing
		WAIT "This directory "+dpath+" does not exist... Aborting!" WINDOW TIMEOUT 3

		lcftpfile = "Aborted: Directory Not Found..."
		lcftptype = "UNK"
		DO badftp

		tsubject= "Directory Doesn't Exist - FTP Aborted"
		tmessage = "The directory "+dpath+" for "+lcwhichsite+;
			" does not exist... FTP Aborted! Please check and repair directory info for this transfer"
		DO errorhandle

		RETURN
	ENDIF

	SET DEFAULT TO &dpath
	lcpath             = ALLTRIM(ftpsetup.localpath)
	lcarchivepath      = ALLTRIM(ftpsetup.archive)

	IF !DIRECTORY(lcarchivepath) && if archive folder is missing
		WAIT "This directory "+lcarchivepath+" does not exist... Aborting!" WINDOW TIMEOUT 3

		lcftpfile = "Aborted: Archive DIR Not Found..."
		lcftptype = "UNK"
		DO badftp

		tsubject= "Archive Path Doesn't Exist - FTP Aborted"
		tmessage = "The archive directory "+lcarchivepath+" for "+lcwhichsite+;
			" does not exist... FTP Aborted! Please check and repair directory info for this transfer"
		DO errorhandle

		RETURN
	ENDIF

	lcremotefolder     = ALLTRIM(ftpsetup.rfolder)
	lcaccountname      = ALLTRIM(ftpsetup.account)
	lceditype          = ALLTRIM(ftpsetup.editype)
	lcthissite         = ALLTRIM(ftpsetup.site)
	lcthislogin        = ALLTRIM(ftpsetup.login)
	lcthispassword     = ALLTRIM(ftpsetup.PASSWORD)
ENDIF

lcoldcaption = _SCREEN.CAPTION
lcwhichsite = ALLTRIM(lcwhichsite)
_SCREEN.CAPTION    = ALLTRIM(ftpsetup.MESSAGE)

SELECT ftpedilog
WAIT WINDOW AT 10,10 CHR(13)+_SCREEN.CAPTION+CHR(13)+"Connecting to "+lcthissite+CHR(13)+"User ID: "+tuserid+CHR(13) NOWAIT

*Creating a connection object and assign it to the variable
mysite = CREATEOBJECT("CuteFTPPro.TEConnection")
mysite.protocol = "FTP"
mysite.HOST     = lcthissite
mysite.login    = lcthislogin
mysite.PASSWORD = lcthispassword
mysite.retries = 3 && Can be adjusted if it causes timeouts
mysite.delay = 15 && Can also be adjusted if it causes timeouts
mysite.useproxy = "BOTH"
mysite.CONNECT
lcmessage = mysite.WAIT(-1,10000)
IF !isblank(lcmessage)
	WAIT WINDOW "Transfer message "+lcmessage TIMEOUT 2
ENDIF

IF ATC( "ERROR",lcmessage)>0 && if CuteFTP Pro can't make a socket connection
	lcftpfile = "Aborted: Transfer Error..."
	lcftptype = "UNK"
	DO badftp

	tsubject = "FTP Socket Connect Error inside Cute FTP Pro TE"
	tmessage = "FTP Connection failure: "+lcwhichsite+", User: "+tuserid
	DO errorhandle
	RETURN
ENDIF

IF mysite.isconnected >=0 && If CuteFTP Pro has another connection error
	WAIT "Could not connect to: " + mysite.HOST + " Aborting!" WINDOW TIMEOUT 3

	lcftpfile = "Aborted: Transfer Error..."
	lcftptype = "UNK"
	DO badftp

	tsubject = "FTP Non-Connect Error inside Cute FTP Pro TE"
	tmessage = "Connection failure: "+lcwhichsite+", User: "+tuserid
	DO errorhandle

	RETURN
ELSE
	WAIT "You are now connected to "+mysite.HOST WINDOW TIMEOUT 3
ENDIF

WAIT WINDOW AT 10,10 CHR(13)+"Setting up folders.........."+CHR(13) TIMEOUT 3

* here is an area for special instructions.
* Regatta needed this because of the way their AS400 handled FTP sessions.....

DO CASE
	CASE INLIST(lcwhichsite,"REGATTA-NJ","753NJ-REGATTA","REG-945-NJ")
		mysite.remotefolder = "/"
		mysite.remotefolder = lcremotefolder
		STORE .T. TO llisregatta
		STORE "NJ" TO lcregattaloc

	CASE INLIST(lcwhichsite,"REGATTA-CA","753CA-REGATTA","REG-945-CA")
		mysite.remotefolder = "/"
		mysite.remotefolder = lcremotefolder
		STORE .T. TO llisregatta
		STORE "CA" TO lcregattaloc

	CASE INLIST(lcwhichsite,"REGATTA-FL","753FL-REGATTA","REG-945-FL")
		mysite.remotefolder = "/"
		mysite.remotefolder = lcremotefolder
		STORE .T. TO llisregatta
		STORE "FL" TO lcregattaloc

	OTHERWISE
		IF EMPTY(lcremotefolder)
			mysite.remotefolder = ""
			* when we login we are at our home folder
		ELSE
			b = mysite.remoteexists(lcremotefolder)
			IF b>=0 THEN && If no remote Regatta folder
				WAIT "Home directory not found...."+lcremotefolder+" for "+lcwhichsite WINDOW TIMEOUT 3

				lcftpfile = "Aborted: No Home DIR..."
				lcftptype = "UNK"
				DO badftp

				tsubject = "Missing Regatta Remote Folder - FTP Aborted"
				tmessage = "Home Directory "+lcremotefolder+" Not Found For "+lcwhichsite
				DO errorhandle

				RETURN
			ELSE
				mysite.remotefolder = ALLTRIM(lcremotefolder)
			ENDIF
		ENDIF
ENDCASE

mysite.localfolder = lcpath
lnnum = ADIR(tarray)

*!*	archivename= ALLTRIM(STR(YEAR(DATE())))+PADL(ALLTRIM(STR(MONTH(DATE()))),2,"0")+;
*!*		PADL(ALLTRIM(STR(DAY(DATE()))),2,"0")+PADL(ALLTRIM(STR(HOUR(DATETIME()))),2,"0")+;
*!*		PADL(ALLTRIM(STR(MINUTE(DATETIME()))),2,"0")+PADL(ALLTRIM(STR(SEC(DATETIME()))),2,"0")

archivename = TTOC(DATETIME(),1)  && replaces code above

STORE "" TO XFILE,ARCHIVEFILE

IF lnnum >= 1
	FOR thisfile = 1  TO lnnum
		*IF tarray[thisfile,2] < 10
		*   LOOP
		*endif
		
		cFilename = TRIM(tarray[thisfile,1])
		XFILE = ADDBS(lcpath)+cFilename
		ARCHIVEFILE = ADDBS(lcarchivepath)+cFilename

		IF llisregatta = .T. AND LEFT(UPPER(cFilename),3) = "ASN"
			ARCHIVEFILE = lcarchivepath+lcregattaloc+archivename+cFilename
		ENDIF

		WAIT WINDOW AT 10,10 CHR(13)+"       FTPing file: "+XFILE+CHR(13) NOWAIT
		IF FILE(XFILE)
			WAIT WINDOW AT 10,10 CHR(13)+"     Now uploading "+XFILE+CHR(13) NOWAIT

			mysite.upload(XFILE)
			lcmessage = mysite.WAIT(-1,2000)
			WAIT WINDOW AT 10,10 CHR(13)+"     Transfer message for "+lcwhichsite+": "+lcmessage+"    "+CHR(13) TIMEOUT 2
			SELECT ftpjobs
			REPLACE terminated WITH .F.
			REPLACE COMPLETE WITH .T.
			REPLACE busy WITH .F.
			REPLACE exectime WITH DATETIME()
			DELETE
			SELECT ftpedilog
			APPEND BLANK
			REPLACE ftpedilog.ftpdate   WITH DATETIME()
			REPLACE ftpedilog.transfer  WITH ftpjobs.jobname
			REPLACE ftpedilog.xfertype  WITH "PUT"
			REPLACE ftpedilog.filename  WITH XFILE
			IF ATC("REGATTA",lcwhichsite)>0
				REPLACE ftpedilog.acct_name WITH UPPER(lcwhichsite)
				REPLACE ftpedilog.arch_name WITH ARCHIVEFILE
			ELSE
				REPLACE ftpedilog.acct_name WITH lcaccountname
				REPLACE ftpedilog.arch_name WITH ARCHIVEFILE
			ENDIF
			REPLACE ftpedilog.TYPE      WITH lceditype
			COPY FILE &XFILE TO &ARCHIVEFILE
			IF FILE(ARCHIVEFILE)
				DELETE FILE &XFILE
			ENDIF
		ELSE
			WAIT WINDOW XFILE+"  not found  !!!!!!!!!!!! " TIMEOUT 3
		ENDIF
		STORE "" TO XFILE,ARCHIVEFILE
	NEXT thisfile
ENDIF

*mysite.tecommand("deletefinished")

mysite.CLOSE

IF pnnotsent = 0
	IF ftpsetup.do_email = .T.
		tattach=""
		lnnum = ADIR(tarray)
		IF lnnum >= 1
			FOR thisfile = 1  TO lnnum
				XFILE = lcpath+tarray[thisfile,1]
				IF FILE(XFILE)
					tattach = tattach+XFILE+";"
				ENDIF
			NEXT thisfile
		ENDIF
		tsendto = ftpsetup.email_addr
		tsubject= TRIM(ftpsetup.subject)+" "+lcwhichsite
		tmessage = TRIM(ftpsetup.mailmsg)+" at "+TTOC(DATETIME())
		tfrom ="pgaidis@fmiint.com"
		DO FORM dartmail2 WITH tsendto,tfrom,tsubject," ",tattach,tmessage,"A"
	ENDIF

	WAIT WINDOW AT 10,10 CHR(13)+"   "+_SCREEN.CAPTION+"   FTP Complete.........      "+CHR(13) TIMEOUT 2
	lcmessage = _SCREEN.CAPTION+"   Data Transfer Complete............"
ELSE

	WAIT WINDOW AT 10,10 CHR(13)+"   "+_SCREEN.CAPTION+"   FTP Aborted.........      "+CHR(13) TIMEOUT 2
	lcmessage = _SCREEN.CAPTION+"   Data Transfer Incomplete for"+lcwhichsite

	lcftpfile = "Aborted: Data Xfer Incomplete..."
	lcftptype = "UNK"
	DO badftp

	tsubject = "Data Transfer Incomplete "+lcwhichsite+" - FTP Aborted"
	tmessage = "Data Transfer was not completed for "+lcwhichsite
	DO errorhandle
ENDIF

SELECT ftpsetup
IF ftpsetup.sendmsg = .T. AND LEN(ALLTRIM(tuserid)) >2
	!net SEND &tuserid &lcmessage
ENDIF

Select ftpsetup
USE IN ftpsetup

SELECT ftpedilog
USE IN ftpedilog

IF lncount = 1
	ON ERROR
ENDIF

WAIT CLEAR
_SCREEN.CAPTION    = lcoldcaption

*!*	Catch To oErr
*!*	  tsubject = "FTP/EDI Upload Error ("+Transform(oErr.ErrorNo)+") at "+Ttoc(Datetime())+"  on file "+xfile
*!*	  tattach  = ""
*!*	  tsendto  = "pgaidis@fmiint.com"
*!*	  tFrom    ="FMI WMS EDI System Operations <fmi-transload-ops@fmiint.com>"

*!*	  tmessage = "Error processing 940 Upload for GBMI.... Filename: "+xfile+Chr(13)
*!*	  tmessage =tmessage+ "Try/CATCH Exeception Message:"+Chr(13)+;
*!*	    [  Error: ] + Str(oErr.ErrorNo) +Chr(13)+;
*!*	    [  LineNo: ] + Str(oErr.Lineno) +Chr(13)+;
*!*	    [  Message: ] + oErr.Message +Chr(13)+;
*!*	    [  Procedure: ] + oErr.Procedure +Chr(13)+;
*!*	    [  Details: ] + oErr.Details +Chr(13)+;
*!*	    [  StackLevel: ] + Str(oErr.StackLevel) +Chr(13)+;
*!*	    [  LineContents: ] + oErr.LineContents+Chr(13)+;
*!*	    [  UserValue: ] + oErr.UserValue+Chr(13)
*!*	  Do Form dartmail2 With tsendto,tFrom,tsubject,tcc,tattach,tmessage,"A"
*!*	Endtry

RETURN

********************************************************************************************************************
*!* COMMENT FROM HERE DOWN IF NOT WORKING
PROCEDURE errorhandle
PARAMETERS lcerror1
lcerrorstr = ALLTRIM(STR(lcerror1))

AERROR(errtemp)

IF !USED("ftperrorlog")
	USE F:\edirouting\ftperrorlog IN 0 ALIAS ftperrorlog
ENDIF
lcCWA = ALIAS()
SELECT ftperrorlog
APPEND FROM ARRAY errtemp
REPLACE ftperrorlog.errdate WITH DATETIME()
USE IN ftperrorlog
SELECT &lcCWA

IF lcerror1>1400
	pnnotsent = 1
	WAIT "Running Procedure ERRORHANDLE"+CHR(13)+;
		"Connection failed - Error: "+lcerrorstr+;
		CHR(13)+"Notification Sent..." WINDOW AT 10,10 TIMEOUT 7
	IF lncount = 0
		DO CASE
			CASE lncount  = 2 && remove this after retry-limit approval finalized
				***
				#IF 0
				CASE lnFTPTries => 3 AND llisregatta = .T.
					lcftpfile = "Aborted: FoxPro ErrCode...FINAL-REGATTA"
					lcftptype = "UNK"
					DO badftp
					tsubject = "URGENT!!!  REGATTA FTP Aborted at "+TTOC(DATETIME,1)
					tmessage = "VFP FTP Error: "+lcerrorstr+"; "+lcwhichsite+;
						"...FTP Aborted. Contact REGATTA to re-submit...possible remote FTP Site error."
					DO errormail
				#ENDIF
			CASE lnFTPTries => 10 AND llisregatta = .F.
				lcftpfile = "Aborted: FoxPro ErrCode...FINAL"
				lcftptype = "UNK"
				DO badftp
				tsubject = "URGENT!!!  FTP Aborted at "+TTOC(DATETIME(),1)
				tmessage = "VFP FTP Error: "+lcerrorstr+"; "+lcwhichsite+;
					"...FTP Aborted. Contact sender to re-submit...possible remote FTP Site error."
			OTHERWISE
				lcftpfile = "Aborted: FoxPro ErrCode Trapped..."
				lcftptype = "UNK"
				DO badftp
				tsubject = "Internal FoxPro Error Codes - FTP Aborted for "+lcwhichsite
				tmessage = "Trapped VFP FTP Error: "+lcerrorstr+"; "+lcwhichsite+"...FTP Aborted. Will retry."
				DO errormail
		ENDCASE
		lncount = 1
	ENDIF

	WAIT CLEAR
	RETURN TO MASTER

ENDIF
ENDPROC
********************************************************************************************************************
PROCEDURE errormail

tattach = ""
tfrom ="joe.bianchi@tollgroup.com"

tsendto = "joe.bianchi@tollgroup.com"
IF OCCURS("EMODAL",lcwhichsite)>0
	tcc = "mwinter@fmiint.com"
ELSE
	tcc = "pgaidis@fmiint.com"
ENDIF
*	tcc = "" && TESTING ONLY!

DO FORM  dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
ENDPROC
********************************************************************************************************************
PROCEDURE badftp

SELECT ftpedilog
APPEND BLANK
REPLACE ftpedilog.ftpdate     WITH DATETIME()
REPLACE ftpedilog.filename    WITH lcftpfile
REPLACE ftpedilog.acct_name   WITH lcwhichsite
REPLACE ftpedilog.TYPE        WITH lcftptype
REPLACE ftpedilog.xfertype    WITH "PUT"
REPLACE ftpedilog.transfer    WITH ftpjobs.jobname

ENDPROC