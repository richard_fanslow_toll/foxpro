PARAMETERS nAcctNum,cCustName,cPPName,lUCC,cOffice,lSQL3,lMoret,lDoCartons,lDoScanpack

IF VARTYPE(cOffice) # "C"
	cOffice = "Y"
ENDIF
csqlserver = IIF(lTesting,"njsql1","tgfnjsql01")
cSQLPass = ""
SELECT 0
USE F:\wh\sqlpassword
cSQLPass = ALLTRIM(sqlpassword.PASSWORD)
USE IN sqlpassword

SET ESCAPE ON
ON ESCAPE CANCEL
cFileOutName = "V"+TRIM(cPPName)+"pp"
nAcct = ALLTRIM(STR(nAcctNum))
IF USED("temp1")
	USE IN temp1
ENDIF

lcDSNLess="driver=SQL Server;server=&cSQLServer;uid=SA;pwd=&cSQLPass;DATABASE=PICKPACK"	&& dy 2/18/18
nHandle=SQLSTRINGCONNECT(m.lcDSNLess,.T.)
SQLSETPROP(0,'DispLogin',3)
SQLSETPROP(0,"dispwarnings",.F.)
WAIT WINDOW "Now processing "+cCustName+" SQL view...please wait" NOWAIT
IF nHandle<1 && bailout
	SET STEP ON
	WAIT WINDOW "Could not make SQL connection" TIMEOUT 3
	cRetMsg = "NO SQL CONNECT"
	RETURN cRetMsg
	THROW
ENDIF

lAppend = .F.

SELECT sqlwo2xu

lLabels = IIF(DATETIME()<DATETIME(2018,04,16,18,00,00) or lTesting,.t.,.f.)
*!* Scans through all WOs within the OUTSHIP BOL#

SCAN
	nWO_Num = sqlwo2xu.wo_num
	nWo   = ALLTRIM(STR(nWO_Num))

		IF usesql() AND !lLabels
		WAIT WINDOW "SQL Records will be selected from CARTONS" TIMEOUT 1
	
	if lucc
			xorderby="order by ship_ref, outshipid, ucc"
		else
			xorderby="order by ship_ref, outshipid, cartonnum"
		endif
		
		if xsqlexec("select * from cartons where wo_num="+nwo+" and ucc#'CUTS' "+xorderby,cFileOutName,,"pickpack") = 0
			ASSERT .F. MESSAGE "At NO RECORDS statement in SQLConnect"
			WAIT WINDOW "No "+cCustName+" records found" TIMEOUT 2
			cRetMsg = "NO SQL CONNECT"
			RETURN cRetMsg
		endif
	
	ELSE
	WAIT WINDOW "SQL Records will be selected from LABELS" TIMEOUT 1

	lcQ1 = [SELECT * FROM dbo.labels Labels]
		lcQ2 = [ WHERE Labels.wo_num = ]
		lcQ3 = " &nWo "
		DO CASE
			CASE lUCC
				lcQ6 = [order by Labels.ship_ref,Labels.outshipid,Labels.ucc]
			OTHERWISE
				lcQ6 = [order by Labels.ship_ref,Labels.outshipid,Labels.CartonNum]
		ENDCASE

		lcsql = lcQ1+lcQ2+lcQ3+lcQ6

		llSuccess=SQLEXEC(nHandle,lcsql,cFileOutName)
	
		IF llSuccess=0  && no records 0 indicates no records and 1 indicates records found
		SET STEP ON 
			ASSERT .F. MESSAGE "At NO RECORDS statement in SQLConnect"
			WAIT WINDOW "No "+cCustName+" records found" TIMEOUT 2
			cRetMsg = "NO SQL CONNECT"
			RETURN cRetMsg
		ENDIF
		SQLCANCEL(nHandle)
	ENDIF

	IF lAppend = .F.
		lAppend = .T.
		SELECT &cFileOutName
		IF lTesting
*!*			ASSERT .f. MESSAGE "In SQLConnect, browsing 'V' file"
*!*			BROWSE
		ENDIF
		COPY TO ("F:\3pl\DATA\temp1")
		USE ("F:\3pl\DATA\temp1") IN 0 ALIAS temp1
	ELSE
		SELECT &cFileOutName
		SCAN
			SCATTER MEMVAR
			INSERT INTO temp1 FROM MEMVAR
		ENDSCAN
	ENDIF
ENDSCAN
IF USED(cFileOutName)
	USE IN &cFileOutName
ENDIF

SELECT temp1
LOCATE

IF lUCC
	SELECT * FROM temp1 ;
		ORDER BY ship_ref,outshipid,ucc ;
		INTO CURSOR &cFileOutName READWRITE
ELSE
	SELECT * FROM temp1 ;
		ORDER BY ship_ref,outshipid,cartonnum ;
		INTO CURSOR &cFileOutName READWRITE
ENDIF
IF lTesting
	SELECT &cFileOutName
*BROWSE
ENDIF

USE IN temp1
USE IN sqlwo2xu
GO BOTT
WAIT CLEAR
*WAIT "" TIMEOUT 3
SQLDISCONNECT(nHandle)

cRetMsg = "OK"
RETURN cRetMsg
