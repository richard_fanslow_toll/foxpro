* based on marcjacobsw832 8/11/2017 MB

utilsetup("RAGANDBONE832")
CLOSE DATA ALL

PUBLIC lTesting
PUBLIC ARRAY a856(1)
ON ERROR debug

lTesting = .T.
DO m:\dev\prg\_setvars WITH lTesting

IF !lTesting
*	Use F:\wh\upcmast Alias upcmast In 0 Shared
	xsqlexec("select * from upcmast where accountid= 6699","upcmastsql",,"wh")
ELSE
	xsqlexec("select * from upcmast where accountid= 6699","upcmastsql",,"wh")
ENDIF


SELECT upcmastsql
INDEX ON upc TAG upc
SET ORDER TO

*Set Step On

*!*	Select * ;
*!*		FROM upcmast ;
*!*		WHERE .F. ;
*!*		INTO Cursor tempmast Readwrite
*!*	Alter Table tempmast Drop Column upcmastid
lcCurrDir = ""

IF lTesting
	lcPath = 'F:\FTPUSERS\RagBone\832INTest\'
	lcArchivePath = 'F:\FTPUSERS\RagBone\832INtest\archive\'
	tsendto = "pgaidis@fmiint.com"
	tcc = ""
ELSE
	lcPath = 'F:\FTPUSERS\RagBone\832IN\'
	lcArchivePath = 'F:\FTPUSERS\RagBone\832IN\archive\'
	tsendto = "pgaidis@fmiint.com"
	tcc = ""
ENDIF


CD &lcPath
len1 = ADIR(ary1,"*.832")
IF len1 = 0
	WAIT WINDOW "No files found...exiting" TIMEOUT 2
	CLOSE DATA ALL
	schedupdate()
	_SCREEN.CAPTION=gscreencaption
	ON ERROR
	RETURN
ENDIF

lcAddedStr  ="Styles Added:"+CHR(13)
lcUpdateStr ="Styles Updated:"+CHR(13)
Set Step On
FOR xtodd = 1 TO len1
	cFilename = ALLTRIM(ary1[xtodd,1])
	xfile = lcPath+cFilename
*****TM added
	SELECT * ;
		FROM upcmastsql ;
		WHERE .F. ;
		INTO CURSOR tempmast READWRITE
	ALTER TABLE tempmast DROP COLUMN upcmastid
	DO m:\dev\prg\createx856a
	DO m:\dev\prg\loadedifile WITH xfile,"|","PIPE4","RAGBONE"

	SET STEP ON 
	m.addby = "FMI-PROC"
	m.adddt = DATETIME()
	m.addproc = "RAB832"
	m.accountid = 6699
	m.pnp = .T.
	m.uom = "EA"

	SELECT x856

*browse


	LOCATE
	SCAN
*SET STEP ON
		DO CASE
			CASE x856.segment = "LIN"
**reset in case segments do not exist for upc - mvw 03/19/14
				STORE "" TO m.sid,m.info,m.descrip
				STORE 0 TO m.price,m.rprice

				IF x856.f2 = "UP"
					m.upc = ALLT(x856.f3)
				ENDIF
				IF x856.f4 = "ST"
					m.style = ALLT(x856.f5)
				ENDIF
				IF x856.f6 = "CL"
					m.color = ALLT(x856.f7)
				ENDIF
				IF x856.f8 = "SZ"
					m.id = ALLT(x856.f9)
				ENDIF

				IF x856.f22 = "SE"
					m.info = "SEASON*"+ALLT(UPPER(x856.f23))  &&Force uppercase
				ENDIF

				IF x856.f2 = "UP"
					m.info = m.info+CHR(13)+"UPC*"+ALLT(x856.f3)  &&ADD UPC TO INFO 3/27/14
				ENDIF


			CASE x856.segment = "REF" AND x856.f1 = "DIV"
				m.info = m.info+CHR(13)+"DIV*"+ALLT(x856.f2)

			CASE x856.segment = "REF" AND x856.f1 = "MRC"       &&&Added 02/27/2013 AX2012 Classification TM
				m.info = m.info+CHR(13)+"MRC*"+ALLT(x856.f2)

			CASE x856.segment = "REF" AND x856.f1 = "MRD"       &&&Added 02/27/2013 AX2012 Body Type TM
				m.info = m.info+CHR(13)+"MRD*"+ALLT(x856.f2)

			CASE x856.f2 = "UP"
				m.info = m.info+CHR(13)+"UPC*"+ALLT(x856.f3)


			CASE x856.segment = "PID" AND x856.f1 = "F" AND x856.f2 = "08"
				m.descrip = ALLT(x856.f5)

			CASE x856.segment = "PID" AND x856.f1 = "F" AND x856.f2 = "73"
				m.info = m.info+CHR(13)+"COLORDESC*"+ALLTRIM(UPPER(x856.f5))

			CASE x856.segment = "CTP" AND x856.f2 = "RTL"
				m.rprice = VAL(x856.f3)
				m.info = m.info+CHR(13)+"RPRICE*"+(x856.f3)
			CASE x856.segment = "CTP" AND x856.f2 = "STD"
				m.price = VAL(x856.f3)

			CASE x856.segment = "CTP" AND x856.f2 = "PCA"
				m.info = m.info+CHR(13)+"SALEPRICE*"+ALLT(x856.f3)

			CASE x856.segment = "REF" AND x856.f1 = "SI"       &&&Added 03/14/2014 AX2012 SID TM
				m.sid  = ALLT(x856.f2)
				m.info = m.info+CHR(13)+"SID*"+ALLT(x856.f2)

			CASE x856.segment = "SLN" && added for Rag and Bone
				INSERT INTO tempmast FROM MEMVAR
		ENDCASE

	ENDSCAN
	SET STEP ON 
	RELEASE m.upc,m.style,m,color,m.id,m.info,m.sid,m.price,m.descrip,m.rprice
	
	useca('upcmast','wh',,,'a-6699')
	SELECT tempmast
*browse


*** we have read in this file into tempmast
 Set Step On
	RecCtr = 0
	NumAdded = 0
	NumUpdated = 0
	m.problem=.F.
	SELECT upcmastsql
	SCATTER FIELDS EXCEPT upcmastid MEMVAR MEMO BLANK
	SELECT tempmast && has all new records.......

	SCAN
		RecCtr = RecCtr +1
		SCATTER FIELDS EXCEPT upcmastid MEMVAR MEMO
		SELECT upcmastsql

		WAIT WINDOW AT 10,10 "Checking Record # "+TRANSFORM(RecCtr) NOWAIT

		IF !SEEK(ALLTRIM(tempmast.upc),"upcmastsql","upc")
			gmasteroffice='N'
			goffice='I'
			insertinto("upcmast","wh",.T.)
		ELSE
			SELECT upcmast
			LOCATE FOR ALLTRIM(upc)=ALLTRIM(tempmast.upc)

			IF FOUND() THEN
				m.updatedt = DATETIME()
				REPLACE upcmast.updatedt     WITH m.updatedt IN upcmast
				REPLACE upcmast.upc          WITH tempmast.upc IN upcmast
				REPLACE upcmast.sid          WITH tempmast.sid IN upcmast
				REPLACE upcmast.price        WITH tempmast.price IN upcmast
				REPLACE upcmast.rprice       WITH tempmast.rprice IN upcmast
				REPLACE upcmast.STYLE        WITH tempmast.STYLE IN upcmast
				REPLACE upcmast.COLOR        WITH tempmast.COLOR IN upcmast
				REPLACE upcmast.ID           WITH tempmast.ID IN upcmast
				REPLACE upcmast.DESCRIP      WITH UPPER(tempmast.DESCRIP) IN upcmast
				REPLACE upcmast.INFO         WITH UPPER(tempmast.INFO) IN upcmast
				REPLACE upcmast.FLAG         WITH .F. IN upcmast
				REPLACE upcmast.whseloc      WITH '' IN upcmast
			ENDIF
		ENDIF
	ENDSCAN

	IF lTesting
		SELECT upcmastsql
		BROW
	ELSE
*		SET STEP ON
*		tu("upcmastsql")
		tu('upcmast')
	ENDIF
************************************** END   AX2012
	tattach = ""
	tFrom ="Toll/FMI EDI System Operations <fmi-transload-ops@fmiint.com>"
	tmessage = "Rag and Bone Stylemaster updated at: "+TTOC(DATETIME())+CHR(13)+;
		CHR(13)+lcAddedStr+CHR(13)+;
		CHR(13)+lcUpdateStr

	IF lTesting
	tcc = "joe.bianchi@tollgroup.com"
	endif
	tSubject = "Rag and Bone Stylemaster Update"
	DO FORM m:\dev\frm\dartmail2 WITH tsendto,tFrom,tSubject,tcc,tattach,tmessage,"A"
	WAIT WINDOW "All 832 Files finished processing...exiting" TIMEOUT 2
	cLoadFile = (lcPath+cFilename)
	USE IN tempmast
*Set Step On
	cArchiveFile = (lcArchivePath+cFilename)
	COPY FILE [&cLoadFile] TO [&cArchiveFile]

	IF !lTesting
		DELETE FILE [&cLoadFile]
	ENDIF
ENDFOR
CLOSE DATA ALL

schedupdate()
_SCREEN.CAPTION=gscreencaption
ON ERROR

