  
* --- 10/15/03 MB
* Rewrote to load PDF files for multiple Invoices at once.
* In the new approach the pdf filenames are the invoice #s,
* so we'll need to associate them with the matching vinvoice records.

WAIT WINDOW "Preparing to Load PDF files..." NOWAIT

LOCAL lnSelect, lcPDFDir, laPDFFiles(1,1), lnNumFiles, i, lcPDFFullFileName, lnNumPages
LOCAL lcInvNum, lnFilesLoaded, lcPDFTable, lcPDFTablePath, lcTempPDFFile, lnTotalNumPages
LOCAL lcPDFRootFileName, lnTotAmt, lnTotCnt, lcMsg

* get array of scanned PDF files
lcPDFDir = "C:\PDF_FILES\"
lnNumFiles = ADIR(laPDFFiles,lcPDFDir + "*.PDF")

IF lnNumFiles < 1 THEN
	x3winmsg("No documents have been scanned!","Error")
	RETURN .F.
ENDIF

*!*	IF DBUSED('ar') THEN
*!*		x3winmsg("Please close all forms before running this process!","Error")
*!*		RETURN .F.
*!*	ENDIF

* save work area
lnSelect = SELECT()

* load Acrobat engine
if set("default")#"M"
	IF TYPE("oPDDoc")="U"
		oPDDoc = CREATEOBJECT("AcroExch.PDDoc")
	ENDIF
endif

* open database

if used("invpdf")
	use in invpdf
endif

useca("invpdf","ar")

set step on 
lnFilesLoaded = 0
lnTotalNumPages = 0

* open invoice view
userv("vinvoice","arsql")

if !used("acctbill")
	xsqlexec("select * from acctbill","acctbill",,"qq")
	index on billtoid tag billtoid
	index on accountid tag accountid
	index on STR(billtoid,4)+STR(accountid,4) tag billacct
endif

* process each file in array
FOR i = 1 TO lnNumFiles

	* get filename
	lcPDFRootFileName = laPDFFiles[i,1]
	lcPDFFullFileName = lcPDFDir + lcPDFRootFileName

	* INVOICE # is the stem filename
	lcInvNum = JUSTSTEM(lcPDFRootFileName)
	lcMsg = "Processing file " + TRANSFORM(i) + " of " + TRANSFORM(lnNumFiles) + ;
		CHR(13) + CHR(13) + "Loading Invoice # " + lcInvNum + "..."
	WAIT WINDOW lcMsg NOWAIT

	* get invoice id based on inv num
	IF USED('curBATCHLOADPDF') THEN
		USE IN curBATCHLOADPDF
	ENDIF
	
	useca("invoice","ar",,,"invnum='"+lcInvNum+"'",,"curbatchloadpdf")
	tinvoiceid = curBATCHLOADPDF.invoiceid
	REQUERY("vinvoice")
	
set step on 

	SELECT vinvoice
	IF EOF() THEN
		* error
		x3winmsg("Invoice # " + lcInvNum + " was not found in the Invoice View!","Error")
	ELSE
		* proceed...
		* get # pages in the scanned .pdf file
		if set("default")#"M"
			oPDDoc.OPEN(lcPDFFullFileName)
			lnNumPages = oPDDoc.GetNumPages
			oPDDoc.CLOSE
		endif
		
		fillca("xinvpdf.xinvpdf","select * from invpdf where invoiceid="+transform(vinvoice.invoiceid))

		* create merged .pdf file if we're adding to an existing .pdf
		IF reccount("invpdf")#0
			lcTempPDFFile = "h:\fox\"+SYS(3)+".pdf"
			COPY MEMO pdf TO (lcTempPDFFile)
			pdfmerge(lcTempPDFFile,lcPDFFullFileName,lcPDFFullFileName)
			DELETE FILE (lcTempPDFFile)
		else
			insert into invpdf (invoiceid, addby, adddt) value (vinvoice.invoiceid, "BATCH", datetime())
		ENDIF

		* put .pdf into memo field
		select invpdf
		APPEND MEMO pdf FROM (lcPDFFullFileName) OVERWRITE

		* set flag in invoice table
		REPLACE pdfloaded WITH .T. IN vinvoice

		**added per Darren 7/25/07 MB
		=seek(str(vinvoice.billtoid,4)+str(vinvoice.accountid,4),"acctbill","billacct")
		if acctbill.main or (vinvoice.accountid=1508 and vinvoice.billtoid=804)
		   replace visibleby with vinvoice.accountid in vinvoice
		else
		   replace visibleby with vinvoice.billtoid in vinvoice
		endif

		* archive the loaded file
		*		COPY FILE (lcPDFFullFileName) TO ("f:\watusi\pdfbak\" + lcPDFRootFileName)
		DELETE FILE (lcPDFFullFileName)

		* save
		tu("vinvoice")
		tu("invpdf")

		* update statistics
		lnFilesLoaded = lnFilesLoaded + 1
		lnTotalNumPages = lnTotalNumPages + lnNumPages
	ENDIF  && EOF()
ENDFOR

CLOSE DATABASES

IF USED('vinvoice') THEN
	USE IN vinvoice
ENDIF
IF USED('invoice') THEN
	USE IN invoice
ENDIF
IF USED('curBATCHLOADPDF') THEN
	USE IN curBATCHLOADPDF
ENDIF
IF USED('invpdf') THEN
	USE IN invpdf
ENDIF

SELECT (lnSelect)

SET BELL TO 'gunshot.wav'
?? CHR(7)
SET BELL TO
WAIT CLEAR

x3winmsg(TRANSFORM(lnFilesLoaded)+" files were processed." + CHR(13) + CHR(13) + ;
	TRANSFORM(lnTotalNumPages)+" scanned pages were loaded.")

RETURN

