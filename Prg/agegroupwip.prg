* Age Group WIP report

utilsetup("AGEGROUPWIP")

if holiday(date())
	return
endif

xsqlexec("select * from whoffice",,,"wh")

if dow(date())=2 and time()="07"
	xweekly=.t.
	xfilter="between(del_date,{"+dtoc(date()-7)+"},{"+dtoc(date()-1)+"})"
else
	xweekly=.f.
endif

select whoffice
scan for !test and inlist(office,"N","Y")
	xoffice=whoffice.office
	goffice=xoffice
	xfolder=whoffice.folder+"\whdata\"
	xofficeloc=proper(whoffice.descrip)

	select ship_ref, consignee, ship_via, cnee_ref, ctnqty, qty as unitqty, bol_no, called, appt, del_date, keyrec from outship where .f. into cursor del readwrite
	select ship_ref, consignee, ship_via, cnee_ref, ctnqty, qty as unitqty, bol_no, called, appt, del_date, keyrec from outship where .f. into cursor appt readwrite
		
	dimension xattach[2]
	xattach[1]="h:\fox\appt.xls"
	xattach[2]="h:\fox\del.xls"

	if xweekly
		xsqlexec("select * from outship where mod='"+goffice+"' and accountid=1285 and "+xfilter,,,"wh")
		scan
			scatter memvar
			select outdet
			sum totqty to m.unitqty for outshipid=outship.outshipid and units
			insert into del from memvar
		endscan
	else
		xsqlexec("select * from outship where mod='"+goffice+"' and accountid=1285 and " + ;
			"del_date={} and between(appt,{"+dtoc(date())+"},{"+dtoc(date()+1)+"}) and ctnqty#0",,,"wh")
		scan
			scatter memvar
			select outdet
			sum totqty to m.unitqty for outshipid=outship.outshipid and units
			insert into appt from memvar
		endscan
	endif
	
	if reccount("del")>0 or reccount("appt")>0
		select del
		copy to h:\fox\del.xls xl5
		select appt
		copy to h:\fox\appt.xls xl5

		if xweekly
			xheader="Last week's TGF deliveries - "+xofficeloc
			do email with "cmalcolm@fmiint.com; howard@agegroup.com; dawn@agegroup.com; Fredrica@agegroup.com",xheader,,"h:\fox\del.xls",.f.,,.t.,,,,,.t.
*			do email with "Dyoung@fmiint.com",xheader,,"h:\fox\del.xls",,,.t.,,,,,.t.
		else
			xheader="TGF Files - "+xofficeloc
			do email with "cmalcolm@fmiint.com; howard@agegroup.com; dawn@agegroup.com; Fredrica@agegroup.com",xheader,,"h:\fox\appt.xls",.f.,,.t.,,,,,.t.
*			do email with "Dyoung@fmiint.com",xheader,,"h:\fox\appt.xls",.f.,,.t.,,,,,.t.
		endif
	endif	

	use in outship
	use in outdet
endscan

*

schedupdate()

_screen.Caption=gscreencaption
on error
