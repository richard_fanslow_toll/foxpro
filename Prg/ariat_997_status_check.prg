SET EXCLUSIVE OFF 
SET SAFETY OFF 
ldCurrentTime = DATE()+3600*24
ldGracePeriod = datetime()-3600*3

lcTimeError = "No 997 for: "+ CHR(13)
lcRejectError = "Rej Error: "+ CHR(13)
SET STEP ON 

xsqlexec("select * from ackdata where accountid=6532 and inlist(edicode,'SW','RC') " + ;
	"and processed=1 and rcvdt#{} and accepted=0 and loaddt>{1/11/2016}","rejects",,"wh")

SELECT Rejects

IF reccount() > 0

  EXPORT TO h:\fox\ARIAT_Rejects_errors.xls type xls

  tsendto ="Tmarg@fmiint.com"
  tfrom = "Toll EDI Operations <toll-edi-ops@tollgroup.com>"
  tSubject = "ARIAT 997 Rejection ERRORS"
  tcc =""
  tattach = "h:\fox\ARIAT_Rejects_errors.xls"
  tmessage= lcRejectError
CD  m:\dev\frm\
  DO FORM dartmail2 WITH tsendto,tfrom,tSubject,tcc,tattach,tmessage,"A"
ENDIF 

IF RECCOUNT() = 0
  
  tsendto ="Tmarg@fmiint.com"
  tfrom = "Toll EDI Operations <toll-edi-ops@tollgroup.com>"
  tSubject = "NO ARIAT 997 Rejection ERRORS"
  tcc =""
  tattach = ""
  tmessage= "All good"
CD  m:\dev\frm\
  DO FORM dartmail2 WITH tsendto,tfrom,tSubject,tcc,tattach,tmessage,"A"

ENDIF 

xsqlexec("select * from ackdata where accountid=6532 and inlist(edicode,'SW','RC') and rcvdt={} " + ;
	"and processed=0 and (loadtime>{1/11/2016} and loadtime<{"+dtoc(ldgraceperiod)+"})","timestatus",,"wh")

SELECT TimeStatus

IF reccount() > 0
  
  EXPORT TO h:\fox\ARIAT_time_errors.xls TYPE XLS

  tsendto ="Tmarg@fmiint.com"
  tfrom = "Toll EDI Operations <toll-edi-ops@tollgroup.com>"
  tSubject = "ARIAT 997 Reconciliation ERRORS"
  tcc =""
  tattach = "h:\fox\ARIAT_time_Errors.xls"
  tmessage= lcTimeError
CD  m:\dev\frm\
  DO FORM dartmail2 WITH tsendto,tfrom,tSubject,tcc,tattach,tmessage,"A"
ENDIF 
IF reccount() = 0

  tsendto ="Tmarg@fmiint.com"
  tfrom = "Toll EDI Operations <toll-edi-ops@tollgroup.com>"
  tSubject = "NO ARIAT 997 Reconciliation ERRORS"
  tcc =""
  tattach = ""
  tmessage= "All good"
CD  m:\dev\frm\
  DO FORM dartmail2 WITH tsendto,tfrom,tSubject,tcc,tattach,tmessage,"A"
ENDIF
RETURN
