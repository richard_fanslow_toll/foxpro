close data all
public nacctnum,coffice,nrecs
do m:\dev\prg\_setvars with .t.

nacctnum = 6420
coffice = "C"
ccustname = "PARTY CITY"
nrecs = 0

ltesting = .f.
loverridebusy =  .f. &&lTesting
normalexit = .f.

lcscreencaption = "Party City ASN Upload.......... Processing File: "
_screen.caption = lcscreencaption

m.accountid = 6565
m.accountname = "TJX COMPANIES"
m.addby = "PCASNPROC"
m.adddt = date()
m.dateloaded = date()
m.qty_type = "CARTONS"

set asserts off
close data all

try
	if !ltesting
*SET STEP ON
		select 0
		use f:\edirouting\ftpsetup shared
		locate for ftpsetup.transfer = "ASN-PARTYCITY-CA"
		if ftpsetup.chkbusy and !loverridebusy
			wait window '940 Proc. Busy...try again later' timeout 1
			close databases all
			normalexit = .t.
			throw
		endif
		replace ftpsetup.chkbusy with .t. for ftpsetup.transfer = "ASN-PARTYCITY-CA" in ftpsetup
		use in ftpsetup
	endif

	cofficeloc = iif(coffice = "I","NJ","CA")

	assert .f. message "At Config load"
	select 0
	use f:\3pl\data\mailmaster alias mm shared
	locate for mm.accountid = nacctnum and mm.office = coffice ;
	and edi_type = "856"
	if found()
		store trim(mm.basepath) to lcpath
		store trim(mm.archpath) to lcarchivepath
		tsendto = iif(mm.use_alt,trim(mm.sendtoalt),trim(mm.sendto))
		tcc = iif(mm.use_alt,trim(mm.ccalt),trim(mm.cc))
		store trim(mm.scaption) to thiscaption
		store mm.testflag      to ltest
		_screen.caption = thiscaption
		if ltesting
			locate for (mm.edi_type = "MISC") and (mm.taskname = "JOETEST")
			tsendto = alltrim(mm.sendto)
			tcc = ""
			tsendtoerr = alltrim(mm.sendto)
			tccerr = ""
		else
			locate for (mm.edi_type = "MISC") and (mm.taskname = "JOETEST")
			tsendto = alltrim(mm.sendto)
			tcc = ""
			locate for (mm.edi_type = "MISC") and (mm.taskname = "GENERAL")
			tsendtoerr = iif(mm.use_alt,trim(mm.sendtoalt),trim(mm.sendto))
			tccerr = iif(mm.use_alt,trim(mm.ccalt),trim(mm.cc))
		endif
		use in mm
	else
		wait window at 10,10  "No parameters set for this acct# "+alltrim(str(nacctnum))+"  ---> Office "+coffice timeout 2
		use in mm
		normalexit = .t.
		throw
	endif

	if usesqlctnucc()
	else
		use f:\wo\wodata\ctnucc alias ctnucc in 0
	endif
	
	use f:\partycity\data\pcdetail in 0
	use f:\3pl\data\partycitylog in 0

	if usesqlctnucc()
		xsqlexec("select * from ctnucc where 1=0","tempctnucc",,"wh")
	else
		select * from f:\wo\wodata\ctnucc where .f. into cursor tempctnucc readwrite
	endif
	select * from pcdetail where .f. into cursor temppcdetail readwrite
	select * from pcdetail where .f. into cursor temppcdetail2 readwrite

	create cursor uploaddat (asnfile c(30),container c(10),ctnqty n(6),totweight n(10,2),totcbm n(10,2))

	cd &lcpath
	len1 = adir(ary1,"*")
	wait window "There are "+alltrim(str(len1))+" ASN files to process" nowait &&Timeout 2
*  m.inwonum = 0
	nsuffix = 0
	set step on

	for j = 1 to len1
		cfilename = alltrim(ary1[j,1])
		xfile = (lcpath+cfilename)
		if substr(cfilename,1,2) = "FA"
			copy file &xfile to "f:\ftpusers\partycity\acks\"+cfilename
			delete file &xfile
			loop
		endif
		lcarchivefile  = (lcarchivepath+cfilename)
		wait window "Importing ASN file  "+xfile timeout 1
		
		if usesqlctnucc()
		else
			select ctnucc
			scatter memvar blank
		endif
		
		select 0
*    CREATE CURSOR tempintake (f1 C(1),f2 C(30),f3 C(20),f4 C(25),f5 C(35),f6 C(25),f7 C(10),f8 C(20),f9 C(15),f10 C(15),f11 C(15))

		create cursor tempintake (f1 c(1),f2 c(250))

		append from &cfilename sdf
		locate
		if ltesting
*      BROWSE
		endif
*    m.inwonum = m.inwonum+1
		m.accountid = nacctnum
		m.asnfile = alltrim(cfilename)
		store datetime() to m.asnloaddt,m.adddt
		m.addby = "TOLLPROC"
		m.addproc = "PC_ASN"
		m.totqty = 1
		m.filename = cfilename
		m.acctname ="PARTY CITY"
		m.type="O"
		m.div ="PCD"
		m.loose = .f.
		currentstyle = ""
		currentpo = ""
		plctr = 0

		scan
			do case
			case tempintake.f1="T"
				m.plweight=0.0
				m.cbm=0.0
				m.weight=0.0
				m.totalkgs=0.0

				currentstyle ="NONE"
				currentpo="NONE"
				m.container = substr(f2,13,11)
				m.hawb = substr(f2,13,11)

**save the trailer close date (pos 2-9, 8 chars) - mvw 09/01/16
				m.loaddt=substr(f2,1,8)
				m.loaddt=ctod(substr(m.loaddt,5,2)+"/"+right(m.loaddt,2)+"/"+left(m.loaddt,4))

			case tempintake.f1="B"
				m.po_num= "PC-"+alltrim(substr(f2,16,5))
				m.store = alltrim(substr(f2,16,6))

				m.store = alltrim(substr(f2,16,6))

				if len(alltrim(m.store)) = 4
					m.store = alltrim(m.store)
				else
					if len(alltrim(m.store)) = 6
						m.store = substr(alltrim(m.store),3)
					endif
				endif
				m.delloc = "PC-"+alltrim(m.store)
				m.po_num = "PC-"+alltrim(m.store)

			case tempintake.f1="D"
				select sum(pl_qty) as pl_qty,sum(weight) as weight,sum(totalkgs) as totalkgs,sum(cbm) as cbm from temppcdetail2 into cursor temp group by delloc
				select temp
				scatter memvar
				insert into temppcdetail from memvar
				select temppcdetail2
				zap

			case tempintake.f1="C"
				plctr = plctr +1
				m.pl_qty = 1
*        m.po_num= Alltrim(Substr(f2,114,12))
				m.ucc = alltrim(substr(f2,1,28))
				m.plweight=val(alltrim(substr(f2,30,7)))/10000

**changed bc m.cbm was never defined prior to this and therefore was generating an error - mvw 09/27/16
*        m.cbm=m.cbm+(Val(Alltrim(Substr(f2,37,7)))/10000)*0.028316847
				m.cbm=(val(alltrim(substr(f2,37,7)))/10000)*0.028316847

				m.weight=val(alltrim(substr(f2,30,7)))/10000
				m.totalkgs=(val(alltrim(substr(f2,30,7)))/10000)/2.2

				m.style = substr(f2,148)
				select tempctnucc
				append blank
				gather memvar fields except ctnuccid

				select temppcdetail2
				m.pl_qty = 1
				insert into temppcdetail2 from memvar
				m.plweight=0.0
				m.cbm=0.0
				m.weight=0.0
				m.totalkgs=0.0
			endcase
		endscan

**** below was for the pipe delimied type ASN files **********************************
*!*      Scan
*!*        Do Case
*!*        Case Inlist(tempintake.f1,"A","E")
*!*          Loop
*!*        Case Inlist(tempintake.f1,"F")
*!*          m.state = Alltrim(tempintake.f7)
*!*        Case tempintake.f1 = "T"
*!*          m.shipid    = Alltrim(tempintake.f2)
*!*          m.container = Alltrim(tempintake.f3)
*!*          m.reference = Alltrim(tempintake.f4)  && this is the SEAL# that will load into "reference"  in ctnucc
*!*        Case tempintake.f1 = "B"
*!*          lcStore = Alltrim(tempintake.f4)
*!*          m.store = Alltrim(tempintake.f4)
*!*          m.hawb  = Alltrim(tempintake.f5)
*!*          m.bol   = Alltrim(tempintake.f5)  && BOL for ctnucc
*!*        Case tempintake.f1 = "S"
*!*          m.storestate = Alltrim(tempintake.f7)
*!*        Case tempintake.f1 = "C"
*!*          m.ucc = Alltrim(tempintake.f2)
*!*          m.ponum = Alltrim(tempintake.f8)
*!*          m.po_num=  Alltrim(m.storestate)+"-"+Alltrim(m.store)
*!*          m.serialno = Alltrim(tempintake.f11)  && this is the Pronum from the ASN used in the conf file
*!*          m.style =Alltrim(tempintake.f8)
*!*          Select wogenpk
*!*          Locate For gpk_pk="CTNUCC"
*!*          m.ctnuccid = gpk_currentnumber
*!*          Replace gpk_currentnumber With gpk_currentnumber + 1
*!*          Select tempctnucc
*!*          Append Blank
*!*          Gather Memvar Fields Except ctnuccid
*!*          Replace tempctnucc.ctnuccid With m.ctnuccid

*!*        Case tempintake.f1 = "D"
*!*          If Inlist(m.storestate,"TX","AR","MS")
*!*            m.delloc = "TEXAS DC"
*!*            m.div ="TXD"
*!*          Else
*!*            m.delloc = "PC-"+Padl(Alltrim(lcStore),4,"0")
*!*            m.div ="PCD"
*!*          Endif
*!*          m.acctname ="PARTY CITY/AMSCAN"
*!*          m.type="O"
*!*          m.loose = .F.
*!*          m.pl_qty   = Val(Alltrim(tempintake.f2))
*!*          m.plweight = Val(Alltrim(tempintake.f3))
*!*          m.weight   = Val(Alltrim(tempintake.f3))
*!*          m.totalkgs = Val(Alltrim(tempintake.f3))/2.2046
*!*          m.cbm      = (Val(Alltrim(tempintake.f4))/1728.0)*0.028316
*!*          Select temppcdetail
*!*          Append Blank
*!*          Gather Memvar Fields Except m.detailid
*!*        Endcase
*!*      Endscan

		if ltesting
			select tempctnucc
			count to nrecs
			crecs = alltrim(str(nrecs))
			locate
			nsuffix = nsuffix+1
			csuffix = alltrim(str(nsuffix))
			copy to c:\tempfox\ctnucc_out&csuffix type xl5
		else
			select temppcdetail
			copy to h:\fox\temppcdetail

**delete existing data for that container if not already on a wo - mvw 08/25/16
			select * from temppcdetail group by container into cursor xtemp
			scan
				xctr=left(xtemp.container,11)
				delete for container=xctr and empty(wo_num) in pcdetail
				if usesqlctnucc()
					xsqlexec("delete from ctnucc where accountid="+transform(nacctnum)+" " + ;
						"and container='"+xctr+"' and empty(inwonum)",,,"wh")
				else
					delete for container=xctr and empty(inwonum) in ctnucc
				endif
			endscan
			use in xtemp

			select pcdetail
			append from h:\fox\temppcdetail

			select container,sum(pl_qty) as ctnqty,sum(weight) as totweight,sum(cbm) as totcbm from temppcdetail group by container into cursor stats readwrite
			select stats
			scatter memvar
			m.asnfile = cfilename
			insert into uploaddat from memvar

			select tempctnucc
			scan
				scatter memvar
				select tempctnucc
*        SCATTER FIELDS EXCEPT ctnuccid TO MEMVAR
				scatter memvar
*        RELEASE m.ctnuccid
				if usesqlctnucc()
					insertinto("ctnucc","wh",.t.)
				else
					select ctnucc
					m.ctnuccid = dygenpk("ctnucc","wo")
					insert into ctnucc from memvar
				endif
			endscan

		endif

		select temppcdetail2
		zap
		select temppcdetail
		zap
		select tempctnucc
		zap

		copy file [&xfile] to [&lcArchiveFile]
		if file(xfile)
			delete file [&xfile]
		endif
		m.seqnum = 0
		m.outwonum = 0
		m.inwonum = 0
		m.type = "ASN"
		m.whenproc = datetime()
		m.filename = cfilename
		m.qty = uploaddat.ctnqty
		m.container= uploaddat.container
		m.inout ="IN"
		select partycitylog
		append blank
		gather memvar

		goodmail()
		select uploaddat
		zap
	endfor


	normalexit = .t.
	use f:\edirouting\ftpsetup shared
	locate
	locate for ftpsetup.transfer = "ASN-PARTYCITY-CA"
	replace ftpsetup.chkbusy with .f for ftpsetup.transfer = "ASN-PARTYCITY-CA" in ftpsetup

catch to oerr
	if !normalexit
		set step on
		assert .f. message "At CATCH..."
		tsubject = "Party City ASN "+cofficeloc+" Upload Error ("+transform(oerr.errorno)+") at "+ttoc(datetime())
		tattach  = ""
		tsendto  = tsendtoerr
		tcc = tccerr
		tmessage = "Party City ASN "+cofficeloc+" Upload Error..... Please fix me........!"
		lcsourcemachine = sys(0)
		lcsourceprogram = sys(16)

		tmessage =tmessage+ "Try/CATCH Exeception Message:"+chr(13)+;
		[  Message: ] + oerr.message +chr(13)+;
		[  Procedure: ] + oerr.procedure +chr(13)+;
		[  LineNo: ] + str(oerr.lineno) +chr(13)+;
		[  Error: ] + str(oerr.errorno) +chr(13)+;
		[  Details: ] + oerr.details +chr(13)+;
		[  StackLevel: ] + str(oerr.stacklevel) +chr(13)+;
		[  LineContents: ] + oerr.linecontents+chr(13)+;
		[  UserValue: ] + oerr.uservalue+chr(13)+;
		[  Computer:  ] +lcsourcemachine+chr(13)+;
		[  ASN file:  ] +xfile+chr(13)+;
		[  Program:   ] +lcsourceprogram

		if type('cErrMsg')#'U' and !empty(cerrmsg)
			tmessage = tmessage+chr(13)+chr(13)+cerrmsg
		endif

		tattach  = ""
		tfrom ="TGFSYSTEM"
		do form m:\dev\frm\dartmail2 with tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
	else
		wait window at 10,10 "Normal Exit " timeout 1
	endif
finally
	close databases all
	on error
endtry
*****************************************************************************************8
procedure goodmail
tsubject= "TGF Party City ASN Upload: " +ttoc(datetime())
tattach = ""
tmessage = "Breakdowns Uploaded for Containers:"+chr(13)+;
"ASN File                                              Container        CtnQty       Weight       CBM "+chr(13)
select uploaddat
scan
	tmessage = tmessage +uploaddat.asnfile+"    "+padr(alltrim(uploaddat.container),12," ")+"   "+padr(alltrim(transform(uploaddat.ctnqty)),8," ")+"    "+padr(alltrim(transform(uploaddat.totweight)),10," ")+padr(alltrim(transform(uploaddat.totcbm)),6," ")+chr(13)
endscan

tsendto ="juan.rocio@tollgroup.com"
tcc="pgaidis@fmiint.com"
tfrom ="TGFSYSTEM"
do form m:\dev\frm\dartmail2 with tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
endproc
*****************************************************************************************8
