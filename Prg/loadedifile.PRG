*!* m:\dev\prg\loadedifile.prg
PARAMETER xfile,delimchar,TransOption,AccountName
*SET STEP ON 
PUBLIC lProcessOK,lcOutstr
SET SAFETY OFF
CLEAR
lProcessOK = .T.
lcOutstr = ""

*WAIT WINDOW AT 10,10 "Loading new EDI File"+CHR(13)+" from file "+xfile+"   ...Please Wait..." NOWAIT

FOR i = 1 TO ALEN(a856)
	a856(i)=""
NEXT i

SELECT x856
ZAP
l940 = .F.
*WAIT WINDOW "Processing "+xfile NOWAIT
xfile2="f:\3pl\data\tempholdfile.txt"

DO CASE
CASE TransOption == "GAP204" && Added 7/10/06
*	ASSERT .f.
	lcOutstr=FILETOSTR(xfile)
	lcOutstr=STRTRAN(lcOutstr,"^G","*")
	lcOutstr=STRTRAN(lcOutstr,"~^M",CHR(13))
	lcOutstr=STRTRAN(lcOutstr,"^M",CHR(13))
	lnNumLines = ALINES(a856,lcOutstr)
CASE TransOption == "NANJING" && Added 7/10/06
	lcOutstr=FILETOSTR(xfile)
	lcOutstr=STRTRAN(lcOutstr,CHR(13),"")
	lcOutstr=STRTRAN(lcOutstr,CHR(10),"")
	lcOutstr=STRTRAN(lcOutstr,"~",CHR(13))
	lnNumLines = ALINES(a856,lcOutstr)
CASE TransOption == "NAUTICA"
	lnNumLines = ALINES(a856,STRTRAN(FILETOSTR(xfile),CHR(7),CHR(13)))
CASE TransOption == "WEARME"
	lcOutstr = ""
	lcOutstr = FILETOSTR(xfile)
	lcOutstr=STRTRAN(lcOutstr,CHR(13),"")
	lcOutstr=STRTRAN(lcOutstr,CHR(10),"")
	lcOutstr=STRTRAN(lcOutstr," `","`")
	lcOutstr=STRTRAN(lcOutstr,"`",CHR(13))
	lnNumLines = ALINES(a856,lcOutstr)
CASE TransOption == "KIDSINTL"
	lcOutstr=FILETOSTR(xfile)
	lcOutstr=STRTRAN(lcOutstr,CHR(13)+CHR(10),"")
	lnNumLines = ALINES(a856,STRTRAN(lcOutstr,CHR(0x85),CHR(10)))
CASE TransOption == "NONE"
	lcOutstr=FILETOSTR(xfile)
	lnNumLines = ALINES(a856,FILETOSTR(xfile))
CASE TransOption == "STARTOLF"
	lnNumLines = ALINES(a856,STRTRAN(FILETOSTR(xfile),"*",CHR(13)))
CASE TransOption == "COURTAULDS"
	lcOutstr = STRTRAN(FILETOSTR(xfile),"'","")
	lnNumLines = ALINES(a856,lcOutstr)
CASE TransOption == "G3-997ACK"
	lcOutstr = STRTRAN(FILETOSTR(xfile),CHR(133),CHR(13))
	lnNumLines = ALINES(a856,lcOutstr)
CASE TransOption == "MODERNSHOE"
	lcOutstr = STRTRAN(FILETOSTR(xfile),CHR(133),"")
	lnNumLines = ALINES(a856,lcOutstr)
CASE TransOption == "STAR"
	lnNumLines = ALINES(a856,STRTRAN(FILETOSTR(xfile),"*",""))
CASE TransOption == "2TILDE"
	lcOutstr=FILETOSTR(xfile)
	lcOutstr=STRTRAN(lcOutstr,"~~","~")
	lcOutstr=STRTRAN(lcOutstr,"~",CHR(13))
	lnNumLines = ALINES(a856,lcOutstr)
CASE TransOption == "BACKSLASH"
	lnNumLines = ALINES(a856,STRTRAN(FILETOSTR(xfile),"\",CHR(13)))
CASE TransOption == "LFAPOS"
	lnNumLines = ALINES(a856,STRTRAN(FILETOSTR(xfile),"'",CHR(13)))
CASE TransOption == "NOEXCLAIM"
	lnNumLines = ALINES(a856,STRTRAN(FILETOSTR(xfile),"!",""))
CASE TransOption == "TILDE"
	lnNumLines = ALINES(a856,STRTRAN(FILETOSTR(xfile),"~",CHR(13)))
CASE TransOption == "NOAPOSTROPHE"
	lnNumLines = ALINES(a856,STRTRAN(FILETOSTR(xfile),"'",""))
CASE TransOption == "NOTILDE"
	lnNumLines = ALINES(a856,STRTRAN(FILETOSTR(xfile),"~",""))
CASE TransOption == "CR_TILDE"
	lcOutstr=FILETOSTR(xfile)
	lcOutstr=STRTRAN(lcOutstr,"~",CHR(13))
	lnNumLines = ALINES(a856,lcOutstr)
CASE TransOption == "NO_CR_TILDE"
	lnNumLines = ALINES(a856,STRTRAN(FILETOSTR(xfile),"~",""))
CASE TransOption == "85H"
	lnNumLines = ALINES(a856,STRTRAN(FILETOSTR(xfile),CHR(0x85),CHR(13)))
	lcOutstr=STRTRAN(FILETOSTR(xfile),CHR(0x85),CHR(13))
	lcOutstr=STRTRAN(lcOutstr,CHR(0x7),"*")
CASE TransOption == "85H2"
	lnNumLines = ALINES(a856,STRTRAN(FILETOSTR(xfile),CHR(0x85),""))
CASE TransOption == "A1"
	lnNumLines = ALINES(a856,STRTRAN(FILETOSTR(xfile),CHR(0xA1),CHR(13)))
	lcOutstr=STRTRAN(FILETOSTR(xfile),CHR(0x7),"*")
	lcOutstr =STRTRAN(lcOutstr,CHR(0xA1),CHR(13))
CASE TransOption == "5EH"
	lnNumLines = ALINES(a856,STRTRAN(FILETOSTR(xfile),CHR(0x5e),CHR(13)))
CASE TransOption == "C5H"
	lnNumLines = ALINES(a856,STRTRAN(FILETOSTR(xfile),CHR(0xc5)))
CASE TransOption == "07H"
	lcOutstr=FILETOSTR(xfile)
	lcOutstr=STRTRAN(lcOutstr,CHR(0x07),CHR(13))
	lnNumLines = ALINES(a856,STRTRAN(FILETOSTR(xfile),CHR(0x07),CHR(13)))
CASE TransOption == "CARROT"
	lnNumLines = ALINES(a856,STRTRAN(FILETOSTR(xfile),"^",CHR(13)))
CASE TransOption == "DOUBLELF"
	lnNumLines = ALINES(a856,STRTRAN(FILETOSTR(xfile),CHR(13)+CHR(13),CHR(13)))
CASE TransOption == "CNTRLU"
	lnNumLines = ALINES(a856,STRTRAN(FILETOSTR(xfile),CHR(0x5e)+CHR(0x55),CHR(13)))
CASE TransOption == "KHNY"
	lcOutstr=FILETOSTR(xfile)
	lcOutstr=STRTRAN(lcOutstr,CHR(13)+CHR(10),"")
	lcOutstr=STRTRAN(lcOutstr,CHR(0x27),CHR(13))
	lnNumLines = ALINES(a856,lcOutstr)
CASE TransOption == "SUCCESS"
	lnNumLines = ALINES(a856,STRTRAN(FILETOSTR(xfile),CHR(0x15),""))
CASE TransOption == "MORET-EDI" && Added 11/03/09
	lcOutstr=FILETOSTR(xfile)
	lcOutstr=STRTRAN(lcOutstr,"~",CHR(13))
	lnNumLines = ALINES(a856,lcOutstr)
CASE TransOption == "OFFICEDEPOT-EDI" && Added 11/03/09
	lcOutstr=FILETOSTR(xfile)
	lcOutstr=STRTRAN(lcOutstr,"|",CHR(13))
	lcOutstr=STRTRAN(lcOutstr,"~",CHR(13))
	lnNumLines = ALINES(a856,lcOutstr)
CASE TransOption == "PIPE" && Added 05/15/2015 PG
	lcOutstr=FILETOSTR(xfile)
	lcOutstr=STRTRAN(lcOutstr,"|",CHR(13))
	lcOutstr=STRTRAN(lcOutstr,"~",CHR(13))
	lnNumLines = ALINES(a856,lcOutstr)
CASE TransOption == "PIPE2" && Added 05/15/2015 PG
	lcOutstr=FILETOSTR(xfile)
	lcOutstr=STRTRAN(lcOutstr,"|","*")
	lnNumLines = ALINES(a856,lcOutstr)
CASE TransOption == "PIPE3" && Added 05/15/2015 PG
	lcOutstr=FILETOSTR(xfile)
	lcOutstr=STRTRAN(lcOutstr,"|","*")
	lcOutstr=STRTRAN(lcOutstr,"~","")
	lnNumLines = ALINES(a856,lcOutstr)
CASE TransOption == "PIPE4" && Added 05/15/2015 PG
  lcOutstr=FILETOSTR(xfile)
  lcOutstr=STRTRAN(lcOutstr,"|","*")
  lcOutstr=STRTRAN(lcOutstr,"~",CHR(13))
  lnNumLines = ALINES(a856,lcOutstr)
CASE TransOption == "SEARSEDI" && Added 1/11/10
	lcOutstr=FILETOSTR(xfile)
	lcOutstr=STRTRAN(lcOutstr,CHR(0x15),CHR(13))
	lnNumLines = ALINES(a856,lcOutstr)
CASE TransOption == "BCNY997" && Added 1/11/10
	lcOutstr=FILETOSTR(xfile)
	lcOutstr=STRTRAN(lcOutstr,"!",CHR(13))
	lnNumLines = ALINES(a856,lcOutstr)
CASE TransOption == "MJ997" && Added 1/13/15
	lcOutstr=FILETOSTR(xfile)
	lcOutstr=STRTRAN(lcOutstr,"~",CHR(13))
	lnNumLines = ALINES(a856,lcOutstr)
CASE TransOption == "G3997" && Added 1/13/15
	lcOutstr=FILETOSTR(xfile)
	lcOutstr=STRTRAN(lcOutstr,"�",CHR(13))
	lnNumLines = ALINES(a856,lcOutstr)
CASE TransOption == "ART997" && Added 1/5/17  TMARG  ARIAT
	lcOutstr=FILETOSTR(xfile)
	lcOutstr=STRTRAN(lcOutstr,"~",CHR(13))
	lnNumLines = ALINES(a856,lcOutstr)
CASE TransOption == "BIO997" && Added 1/9/17  TMARG BIO
	lcOutstr=FILETOSTR(xfile)
	lcOutstr=STRTRAN(lcOutstr,"~",CHR(13))
	lnNumLines = ALINES(a856,lcOutstr)
CASE TransOption == "MERK997" && Added 1/24/17  TMARG  MERK
	lcOutstr=FILETOSTR(xfile)
	lcOutstr=STRTRAN(lcOutstr,"~",CHR(13))
	lnNumLines = ALINES(a856,lcOutstr)
OTHERWISE
	WAIT WINDOW "Invalid translate option" TIMEOUT 2
	lProcessOK = .F.
	RETURN lProcessOK
ENDCASE

SELECT x856

lcCurrentHL = "H"
m.hll="H"

IF l940
	SELECT x856
	APPEND FROM &xfile2 DELIMITED WITH CHARACTER &delimchar
ELSE

	FOR i=1 TO ALEN(a856)
		SCATTER MEMVAR BLANK
		xend = AT(delimchar,a856[i])
		xbeg=AT(delimchar,a856[i])+2

		IF !xend >1
			m.segment=a856[i]
			INSERT INTO x856 FROM MEMVAR
			LOOP
		ELSE
			m.segment=LEFT(a856[i],xbeg-3)
		ENDIF

		FOR j=2 TO OCCURS(delimchar,a856[i])
			jj=LTRIM(STR(j-1))
			xend=AT(delimchar,a856[i],j)
			m.f&jj=SUBSTR(a856[i],xbeg-1,xend-xbeg+1)
			m.sf&jj = xend-xbeg+1
			xbeg=xend+2
		ENDFOR

		jj=LTRIM(STR(j-1))
		m.f&jj=SUBSTR(a856[i],xend+1)
		m.sf&jj = LEN(SUBSTR(a856[i],xend+1))

		IF ALLTRIM(m.segment) = "HL"
			lcCurrentHL = ALLTRIM(m.f3)
			m.hll = lcCurrentHL
		ELSE
			m.hll = lcCurrentHL
		ENDIF
		IF INLIST(ALLTRIM(m.segment),"BSN","ST","SE","CTT","IEA","ISA","GS")
			m.hll = "S"
		ENDIF


		INSERT INTO x856 FROM MEMVAR
	ENDFOR
ENDIF

SELECT x856
LOCATE
DELETE FOR EMPTY(x856.segment)
LOCATE

RETURN lProcessOK
