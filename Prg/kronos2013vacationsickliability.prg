* Create spreadsheet of 2013 Kronos Vacation Liability 

LOCAL loKronos2013VacationLiabilityReport

runack("KRONOSVACATIONLIABILITYREPORT")

loKronos2013VacationLiabilityReport = CREATEOBJECT('Kronos2013VacationLiabilityReport')
loKronos2013VacationLiabilityReport.MAIN()
CLOSE DATABASES ALL
RETURN

#DEFINE LOGIT 1
#DEFINE WAITIT 2
#DEFINE NOWAITIT 4
#DEFINE SENDIT 8
#DEFINE CRLF CHR(13) + CHR(10)
#DEFINE CARRIAGE_RETURN CHR(13)
#DEFINE LINE_FEED CHR(10)
#DEFINE RETURN_DATA_MANDATORY .F.
#DEFINE RETURN_DATA_NOT_MANDATORY .T.
#DEFINE SECS_PER_HOUR 3600
#DEFINE SECS_PER_DAY (3600.00 * 8.00)

#DEFINE ACCRUAL_TAKEN 1
#DEFINE ACCRUAL_GRANT 2
#DEFINE ACCRUAL_RESET 3
#DEFINE CAP_LIMIT_REDUCTION 6
#DEFINE ACCRUAL_CARRY_FORWARD 11

* NOTE:  the codes below are in Accrualcode.accrualcodeid in wfcsuite db.

#DEFINE ACCRUAL_VACATION_AVAILABLE_CODE 801
#DEFINE ACCRUAL_VACATION_ROLLOVER_CODE 1001
#DEFINE ACCRUAL_SICK_AVAILABLE_CODE 802
#DEFINE ACCRUAL_COMP_HOURS_CODE 901
#DEFINE ACCRUAL_BIRTHDAY_CODE 201
#DEFINE ACCRUAL_PERS_DAY_CODE 1052
#DEFINE ACCRUAL_CA_DRIVER_SICK_CODE 1101

*!*	ACCRUALTRANTYPES BY CODE: 
*!*	1 'TAKEN' 
*!*	2 'GRANT' 
*!*	3 'RESET' 
*!*	4 'EARNING ADJUSTMENT' 
*!*	5 'LIMIT ADJUSTMENT'                       
*!*	6 'MAX BALANCE LIMIT ADJUSTMENT' 
*!*	7 'PROBATION' 
*!*	8 'PROBATION END' 
*!*	9 'TRANSFER FROM EMPLOYEE'                       
*!*	10 'TRANSFER TO EMPLOYEE' 
*!*	11 'CARRYFORWARD' 
*!*	12 'SELECTIVE APPLIED' 
*!*	13 'MAX CARRYOVER LIMIT ADJUSTMENT'                       
*!*	14 'MIN CARRYOVER LIMIT ADJUSTMENT' 
*!*	15 'TRANSFER' 
*!*	16 'GRANT EXPIRATION' 
*!*	17 'PROBATION START'                       
*!*	18 'EXPIRING GRANT' 
*!*	19 'PROBATION RESTART' 
*!*	20 'LIMIT ADJUSTMENT CARRYFORWARD' 
*!*	21 'PROBATION LIMIT ADJUSTMENT CARRYFORWARD'                       
*!*	22 'VESTED LIMIT ADJUSTED CARRYFORWARD' 
*!*	23 'TAKEN EXPIRATION' 
*!*	24 'COMBINED TAKEN EXPIRATION'


DEFINE CLASS Kronos2013VacationLiabilityReport AS CUSTOM

	cProcessName = 'Kronos2013VacationLiabilityReport'

	lTestMode = .T.  && .F. FOR NORMAL OPERATION, .T. TO SUPPRESS ALL BUT DEVELOPER EMAILS AND CHANGE INPUT TABLE, ETC.

	lAutoYield = .T.
	
	lRunExtraInfo = .F.
	*cExtraInfoSuffix = ''
	cExtraInfoSuffix = '_SALARIED_VAC_'
	
	lUseCADriverSick = .F.
	
	lJustReportTestEmployee = .F.

	lCalcSickBalance = .T.

	lCalcBDayBalance = .F.

	cStartTime = TTOC(DATETIME())

	* connection properties
	nSQLHandle = 0
	nSQLHandle2 = 0

	* file properties
	nFileHandle = 0

	* date properties
	*dAsOfDate = DATE()
	dAsOfDate = {^2013-12-31}
	
	dtNow = DATETIME()
	dToday = DATE()
	cToday = DTOC(DATE())
	dJanuary1st = {^2013-01-01}

	* filter properties
	*cCompanyCodesToReport = "('AXA','ZXU','SXI')"  && these are KRONOS companies, corresponding to ADP Companies E89, E88, E87
	cCompanyCodesToReport = "('AXA','ZXU')"  && these are KRONOS companies, corresponding to salaried ADP Companies E89, E88
	*cCompanyCodesToReport = "('AXA')"  && officers in E89 in ADP
	*cCompanyCodesToReport = "('ZXU')"  && salaried in E88 in ADP
	*cCompanyCodesToReport = "('SXI')"  && hourly E87 in ADP

	cMiscWhere = ""
	*cMiscWhere = " AND (L.LABORLEV2NM IN ('20','21','22','30')) "  && FOR ANDRES
	*cMiscWhere = " AND (L.LABORLEV4NM IN ('MA1','PA1','PA2','NY1','NY2')) "  && FOR ANDRES
	*cMiscWhere = " AND (L.LABORLEV2NM IN ('71','72','73','74','76','78')) "
	*cMiscWhere = " AND L.LABORLEV2NM IN ('02') "
	*cMiscWhere = " AND L.LABORLEV2NM IN ('03') "
	*cMiscWhere = " AND L.LABORLEV2NM IN ('02')  AND L.LABORLEV3NM IN ('0655') "
	*cMiscWhere = " AND (L.LABORLEV4NM IN ('SP2')) "
	*cMiscWhere = " AND (L.LABORLEV4NM IN ('CA4')) "
	*cMiscWhere = " AND L.LABORLEV4NM IN ('CA4','CA5','CA6','CA7','SP1','SP2') AND (L.LABORLEV2NM IN ('55','58')) "
	*cMiscWhere = " AND L.LABORLEV2NM IN ('60','61') "
	*cMiscWhere = " AND L.LABORLEV2NM IN ('55') "
	*cMiscWhere = " AND L.LABORLEV2NM IN ('37') "
	*cMiscWhere = " AND (L.LABORLEV2NM IN ('55') AND L.LABORLEV3NM IN ('0650')) "
	*cMiscWhere = " AND (NOT (L.LABORLEV2NM IN ('55') AND L.LABORLEV3NM IN ('0650'))) "
	*cMiscWhere = " AND (L.LABORLEV2NM IN ('03','04') AND L.LABORLEV3NM IN ('0650','0660')) "
	*cMiscWhere = " AND (L.LABORLEV4NM IN ('CA1','CA2','CA4','CA5','CA6','CA7','CA8','SP1','SP2')) "
	*cMiscWhere = " AND (L.LABORLEV4NM IN ('IL1','IL2')) "
	*cMiscWhere = " AND (L.LABORLEV2NM IN ('02') AND L.LABORLEV3NM IN ('0655')) "
	*cMiscWhere = " AND (L.LABORLEV7NM IN ('1620')) "
	*cMiscWhere = " AND L.LABORLEV2NM IN ('06') "
	*cMiscWhere = " AND L.LABORLEV4NM IN ('TX1','TX2') "
	*cMiscWhere = " AND (L.LABORLEV4NM NOT IN ('CA1','CA2','CA4','CA5','CA6','CA7','CA8','SP1','SP2')) "
	*cMiscWhere = " AND (L.LABORLEV2NM IN ('04','14')) "  
	*cMiscWhere = " AND (L.LABORLEV4NM IN ('NY1','NY2')) "  
	*cMiscWhere = " AND ((L.LABORLEV7NM IN ('2514')) OR (P.PERSONNUM = '2514')) "
	
	* FF divs below:
	*cMiscWhere = " AND (L.LABORLEV2NM IN ('20','21','22','23','24','25','26','27','28','29','30','35','36','37','38','71','72','73')) "

	lExclude9999s = .T.
	*lExclude9999s = .F.

	lExcludeInactiveNOW = .T.
	*lExcludeInactiveNOW = .F.

	lExcludeInactiveTHEN = .F.
	
	* PART-TIME PROCESSING PROPERTIES
	cPartTimeList = ''

	* table properties
	cAccrualsDataTablePath = 'F:\UTIL\KRONOS\ACCRUALSDATA\'

	* wait window properties
	nWaitWindowTimeout = 2
	lWaitWindowIsOn = .T.

	* logfile properties
	lLoggingIsOn = .T.
	cLogFile = 'F:\UTIL\KRONOS\Logfiles\Kronos2013VacationLiabilityReport_log.txt'

	* INTERNAL email properties
	lSendInternalEmailIsOn = .T.
	cFrom = 'FMI Corporate <fmicorporate@fmiint.com>'
	cSendTo = 'Mark Bennett <mbennett@fmiint.com>'
	*cSendTo = 'lwaldrip@fmiint.com, MarieDeSaye@fmiint.com, mbennett@fmiint.com'
	cCC = ''
	cSubject = 'Kronos Vacation Liability Report for: ' + TTOC(DATETIME())
	cAttach = ''
	cBodyText = ''

	cCOMPUTERNAME = ''
	cUSERNAME = ''

	FUNCTION INIT
		IF NOT DODEFAULT()
			RETURN .F.
		ENDIF
		WITH THIS
			CLEAR
			*SET RESOURCE OFF
			CLOSE DATA
			SET CENTURY ON
			SET DATE AMERICAN
			SET DECIMALS TO 3
			SET HOURS TO 24
			SET ANSI ON
			SET TALK OFF
			SET DELETED ON
			SET CONSOLE OFF
			SET EXCLUSIVE OFF
			SET SAFETY OFF
			SET EXACT OFF
			SET STATUS BAR ON
			SET SYSMENU OFF
			SET ENGINEBEHAVIOR 70
			_VFP.AUTOYIELD = .lAutoYield
			.cCOMPUTERNAME = GETENV("COMPUTERNAME")
			.cUSERNAME = GETENV("USERNAME")
			IF .lTestMode THEN
				.cSendTo = 'mbennett@fmiint.com'
				.cLogFile = 'F:\UTIL\KRONOS\Logfiles\Kronos2013VacationLiabilityReport_log_TESTMODE.txt'
			ENDIF
			.lLoggingIsOn = .lLoggingIsOn AND NOT EMPTY(.cLogFile)
			IF .lLoggingIsOn THEN
				SET ALTERNATE TO (.cLogFile) ADDITIVE
				SET ALTERNATE ON
			ENDIF
			* re-init date properties after all SETTINGS
			.dtNow = DATETIME()
			.dToday = DATE()
			.cToday = DTOC(DATE())
		ENDWITH
	ENDFUNC


	FUNCTION DESTROY
		WITH THIS
			IF .lLoggingIsOn  THEN
				SET ALTERNATE OFF
				SET ALTERNATE TO
			ENDIF
		ENDWITH
		DODEFAULT()
	ENDFUNC


	FUNCTION MAIN
		WITH THIS
			LOCAL lnNumberOfErrors, lcSQLCurrentBalance, loError, lcTopBodyText, llValid
			LOCAL lcXLFileName, ldStartDate, ldEndDate, lcStartDate, lcEndDate, ldDate
			LOCAL lcSQLStartDate, lcSQLEndDate, lcXLFileName, lcSQLJAN1Balance, lcSQLGrantsToDate
			LOCAL oExcel, oWorkbook, oWorksheet, lnRow, lcRow, lnStartRow, lcStartRow, lnEndRow, lnPP, lcPP, lnNN, lcNN, lcJJ
			LOCAL lcColsToProcess, lnColumn, lcBoldColumn, i, j, lcBoldRow, luCellValue, lcTargetDirectory, lcFiletoSaveAs
			LOCAL lcFileDate, lcDivision, lcTitle, llSaveAgain, lcPayCode, lcCompanyCodesToReport, lcMiscWhere
			LOCAL ldAsOfDate, lcAsOfDate, lcSQLGrantJan1, lnAsOfMonth, lnHireMonth, llNewHire, lnProRatedMonths, lcAccruedFormula
			LOCAL lnVacationCap, lnOverVacationCap, llAdjustMaxEffectiveDate, lcSQLGrantsToDateAfterJan1, lcSQLCapLimitReduction
			LOCAL lcSQLAccrualProfile, lcSQLCompBalance, lcSQLRules, lcSQLPersonalGrantJan1, lcSQLSickGrantJan1

			TRY
				lnNumberOfErrors = 0
				lcTopBodyText = ''

				*.TrackProgress('==================================================================================================================', LOGIT+SENDIT)
				.TrackProgress('Kronos 2013 Vacation Liability Report process started....', LOGIT+SENDIT+NOWAITIT)
				.TrackProgress('COMPUTERNAME = ' + .cCOMPUTERNAME, LOGIT+SENDIT)
				.TrackProgress('USERNAME = ' + .cUSERNAME, LOGIT+SENDIT)
				.TrackProgress('==================================================================================================================', LOGIT+SENDIT)
				IF .lTestMode THEN
					.TrackProgress('', LOGIT)
					.TrackProgress('==================================================================================================================', LOGIT+SENDIT)
					.TrackProgress('                        TEST MODE                        ', LOGIT+SENDIT)
					.TrackProgress('==================================================================================================================', LOGIT+SENDIT)
				ENDIF
				.TrackProgress('PROJECT = KRONOSVACATIONLIABILITYREPORT', LOGIT+SENDIT)
				
				*** KEEP TRACK OF PEOPLE WHO HAD ADJUSTMENTS AFTER 1/1/08, SO WE CAN RETRO THEM TO 1/1/08 ON SPREADSHEET
				********************************************************************************************************
				********************************************************************************************************
				********************************************************************************************************
				
				*WAIT WINDOW "Make adjustment for 3094 Yuen, Yuk Yu" TIMEOUT 5  && ADDED 5 VAC ON 3/30/08.
				
				********************************************************************************************************
				********************************************************************************************************
				********************************************************************************************************
				
				IF .lJustReportTestEmployee THEN
					.cMiscWhere = " AND P.PERSONNUM = '999' "
					.lExclude9999s = .F.
					.lExcludeInactiveNOW = .F.
					.lExcludeInactiveTHEN = .F.
				ENDIF

				ldAsOfDate = .dAsOfDate

				lcAsOfDate = STRTRAN(DTOC(ldAsOfDate),"/","-")

				lcSQLCurrentBalance = .GetSQLStringFromDate( ldAsOfDate )
				
				llAdjustMaxEffectiveDate = .T.
				lcSQLJAN1Balance = .GetSQLStringFromDate( .dJanuary1st, llAdjustMaxEffectiveDate )

				lcSQLGrantJan1 = .GetOnlyVacGrantsFromDateRange( .dJanuary1st, .dJanuary1st )
				lcSQLPersonalGrantJan1 = .GetOnlyPersonalGrantsFromDateRange( .dJanuary1st, .dJanuary1st )
				
				
				lcSQLSickGrantJan1 = .GetOnlySickGrantsFromDateRange( .dJanuary1st, .dJanuary1st )
*!*					* one or the other, depending on if querying CA drivers				
*!*					IF .lUseCADriverSick THEN
*!*						* for ca drivers
*!*						lcSQLSickGrantJan1 = .GetOnlyCADRVRSickGrantsFromDateRange( .dJanuary1st, .dJanuary1st )
*!*					ELSE
*!*						* for non-ca drivers
*!*						lcSQLSickGrantJan1 = .GetOnlySickGrantsFromDateRange( .dJanuary1st, .dJanuary1st )
*!*					ENDIF
				
				lcSQLGrantsToDate = .GetOnlyVacGrantsFromDateRange( .dJanuary1st, ldAsOfDate )
				
				lcSQLGrantsToDateAfterJan1 = .GetOnlyVacGrantsFromDateRange( (.dJanuary1st + 1), ldAsOfDate )
				
				*lcSQLCapLimitReduction = .GetOnlyCapLimitReductions( .dJanuary1st, (.dJanuary1st + 1) )
				
				lcSQLAccrualProfile = .GetAccrualProfileSQL()
				
				lcSQLCompBalance = .GetSQLCompBalance( ldAsOfDate )
				
				lcSQLRules = ;
					" SELECT " + ;
					" PERSONNUM AS FILE_NUM, " + ;
					" PERSONFULLNAME AS NAME, " + ;						
					" EMPLOYMENTSTATUS AS STATUS, " + ;
					" HOMELABORLEVELNM1 AS ADP_COMP, " + ;
					" HOMELABORLEVELNM2 AS DIVISION, " + ;
					" HOMELABORLEVELNM3 AS DEPT, " + ;
					" HOMELABORLEVELNM4 AS WORKSITE, " + ;
					" HOMELABORLEVELNM5 AS EETYPE, " + ;
					" HOMELABORLEVELNM6 AS SHIFT, " + ;
					" HOMELABORLEVELNM7 AS TIMERVWR, " + ;
					" PAYRULENAME AS PAYRULE, " + ;
					" ACCRUALPRFLNAME AS ACPROFILE, " + ;
					" DCMDEVGRPNAME AS DEVGROUP, " + ;
					" TIMEZONENAME AS TIMEZONE " + ;
					" FROM VP_EMPLOYEEV42 " + ;
					" WHERE EMPLOYMENTSTATUS = 'Active' " + ;
					" AND HOMELABORLEVELNM1 <> 'TMP' " + ;
					" ORDER BY PERSONNUM "

				lnAsOfMonth = MONTH( ldAsOfDate )

				SET DATE AMERICAN

				.cSubject = 'Kronos 2013 Vacation Liability Report for: ' + lcAsOfDate

				lcCompanyCodesToReport = .cCompanyCodesToReport


				* 5/16/06 MB: Now using APPLYDTM instead of ADJSTARTDTM to solve problem where hours crossing Midnight Saturday were not being reported.
				*					" WHERE (A.ADJSTARTDTM >= '" + lcSQLStartDate + "')" + ;
				*					" AND (A.ADJSTARTDTM < '" + lcSQLEndDate + "')" + ;

				IF .lTestMode THEN
					.TrackProgress('', LOGIT+SENDIT)
					.TrackProgress('lcSQLCurrentBalance =' + lcSQLCurrentBalance, LOGIT+SENDIT)
					.TrackProgress('', LOGIT+SENDIT)
					.TrackProgress('lcSQLJAN1Balance =' + lcSQLJAN1Balance, LOGIT+SENDIT)
					.TrackProgress('', LOGIT+SENDIT)
					.TrackProgress('lcSQLPersonalGrantJan1 =' + lcSQLPersonalGrantJan1, LOGIT+SENDIT)
					.TrackProgress('', LOGIT+SENDIT)
					.TrackProgress('lcSQLSickGrantJan1 =' + lcSQLSickGrantJan1, LOGIT+SENDIT)
					.TrackProgress('', LOGIT+SENDIT)
					.TrackProgress('lcSQLGrantJan1 =' + lcSQLGrantJan1, LOGIT+SENDIT)
					.TrackProgress('', LOGIT+SENDIT)
					.TrackProgress('lcSQLGrantsToDate =' + lcSQLGrantsToDate, LOGIT+SENDIT)
					.TrackProgress('', LOGIT+SENDIT)
					.TrackProgress('lcSQLGrantsToDateAfterJan1 =' + lcSQLGrantsToDateAfterJan1, LOGIT+SENDIT)
					.TrackProgress('', LOGIT+SENDIT)
					*.TrackProgress('lcSQLCapLimitReduction =' + lcSQLCapLimitReduction, LOGIT+SENDIT)
					*.TrackProgress('', LOGIT+SENDIT)
					.TrackProgress('lcSQLAccrualProfile =' + lcSQLAccrualProfile, LOGIT+SENDIT)
					.TrackProgress('', LOGIT+SENDIT)
					.TrackProgress('lcSQLCompBalance =' + lcSQLCompBalance, LOGIT+SENDIT)
					.TrackProgress('', LOGIT+SENDIT)
					.TrackProgress('lcSQLRules =' + lcSQLRules, LOGIT+SENDIT)
				ENDIF

				.TrackProgress('Connecting to WTK System....', LOGIT+SENDIT)

				OPEN DATABASE F:\UTIL\KRONOS\KRONOSEDI

				*.nSQLHandle = SQLCONNECT("KRONOSEDI","tkcsowner","tkcsowner")
				.nSQLHandle = SQLCONNECT("WTK_ON_PAYROLL2","sa","kr0n1tes")

				IF .nSQLHandle > 0 THEN

					IF USED('CURACCRUALS3') THEN
						USE IN CURACCRUALS3
					ENDIF
					IF USED('CURACCRUALS2') THEN
						USE IN CURACCRUALS2
					ENDIF
					IF USED('CURACCRUALS') THEN
						USE IN CURACCRUALS
					ENDIF
					IF USED('CURJAN1GRANT') THEN
						USE IN CURJAN1GRANT
					ENDIF
					IF USED('CURJAN1PERSONALGRANT') THEN
						USE IN CURJAN1PERSONALGRANT
					ENDIF
					IF USED('CURJAN1SICKGRANT') THEN
						USE IN CURJAN1SICKGRANT
					ENDIF
					IF USED('CURJAN1BALANCE') THEN
						USE IN CURJAN1BALANCE
					ENDIF
					IF USED('CURALLGRANTS') THEN
						USE IN CURALLGRANTS
					ENDIF
					IF USED('CURALLGRANTSAFTERJAN1') THEN
						USE IN CURALLGRANTSAFTERJAN1
					ENDIF
					IF USED('CURCAPLIMITREDUCTION') THEN
						USE IN CURCAPLIMITREDUCTION
					ENDIF
					IF USED('CURPROFILES') THEN
						USE IN CURPROFILES
					ENDIF
					IF USED('CURRULES') THEN
						USE IN CURRULES
					ENDIF
					IF USED('CURCOMP') THEN
						USE IN CURCOMP
					ENDIF

					IF .ExecSQL(lcSQLCurrentBalance, 'CURACCRUALS3', RETURN_DATA_MANDATORY) AND ;
						.ExecSQL(lcSQLJAN1Balance, 'CURJAN1BALANCE', RETURN_DATA_MANDATORY) AND ;
						.ExecSQL(lcSQLGrantJan1, 'CURJAN1GRANT', RETURN_DATA_MANDATORY) AND ;
						.ExecSQL(lcSQLPersonalGrantJan1, 'CURJAN1PERSONALGRANT', RETURN_DATA_MANDATORY) AND ;
						.ExecSQL(lcSQLSickGrantJan1, 'CURJAN1SICKGRANT', RETURN_DATA_MANDATORY) AND ;
						.ExecSQL(lcSQLGrantsToDate, 'CURALLGRANTS', RETURN_DATA_MANDATORY) AND ;
						.ExecSQL(lcSQLAccrualProfile, 'CURPROFILES', RETURN_DATA_MANDATORY) AND ;
						.ExecSQL(lcSQLCompBalance, 'CURCOMP', RETURN_DATA_NOT_MANDATORY) AND ;
						.ExecSQL(lcSQLRules, 'CURRULES', RETURN_DATA_MANDATORY) AND ;
						.ExecSQL(lcSQLGrantsToDateAfterJan1, 'CURALLGRANTSAFTERJAN1', RETURN_DATA_NOT_MANDATORY) THEN

*!*							.ExecSQL(lcSQLCapLimitReduction, 'CURCAPLIMITREDUCTION', RETURN_DATA_NOT_MANDATORY) AND ;

*!*				SELECT CURRULES
*!*				GOTO TOP
*!*				BROWSE
*!*				THROW
				
						* create list of part-timers who were excluded
						.cPartTimeList = "The following employees were excluded because they have blank accruals profiles:" + CRLF 
						SELECT CURPROFILES
						SCAN FOR EMPTY(ACPROFILE)
							.cPartTimeList = .cPartTimeList + ALLTRIM(CURPROFILES.FILE_NUM) + "  " + ALLTRIM(CURPROFILES.NAME) + CRLF							
						ENDSCAN						
						
						SELECT CURACCRUALS3
						GOTO TOP

						IF EOF() THEN
							.TrackProgress("There was no data to export!", LOGIT+SENDIT)
						ELSE

							* add fields to CURACCRUALS3
							SELECT 000000.000 AS JAN1VGRANT, ;
								000000.000 AS JAN1PGRANT, ;
								000000.000 AS JAN1SGRANT, ;
								000000.000 AS ALLGRANTS, ;
								000000.000 AS LATEGRANTS, ;
								000000.000 AS JAN1VACBAL, ;
								000000.000 AS JAN1VRLVR, ;
								000000.000 AS COMPHOURS, ;
								000000.000 AS CAPADJUST, ;
								000000.000 AS VACACCRUAL, * ;
								FROM CURACCRUALS3 ;
								INTO CURSOR CURACCRUALS2 ;
								READWRITE

							* now populate COMPHOURS in CURACCRUALS2
							SELECT CURACCRUALS2
							SCAN
								SELECT CURCOMP
								LOCATE FOR FILE_NUM == CURACCRUALS2.FILE_NUM
								IF FOUND() THEN
									REPLACE CURACCRUALS2.COMPHOURS WITH CURCOMP.COMPBAL
								ENDIF
							ENDSCAN

							* now populate JAN1VACBAL & JAN1VRLVR in CURACCRUALS2
							SELECT CURACCRUALS2
							SCAN
								SELECT CURJAN1BALANCE
								LOCATE FOR FILE_NUM == CURACCRUALS2.FILE_NUM
								IF FOUND() THEN
									REPLACE CURACCRUALS2.JAN1VACBAL WITH CURJAN1BALANCE.VACBALANCE, ;
										CURACCRUALS2.JAN1VRLVR WITH CURJAN1BALANCE.VACRLVRBAL
								ENDIF
							ENDSCAN

							* now populate JAN1VGRANT in CURACCRUALS2
							SELECT CURACCRUALS2
							SCAN
								SELECT CURJAN1GRANT
								LOCATE FOR FILE_NUM == CURACCRUALS2.FILE_NUM
								IF FOUND() THEN
									REPLACE CURACCRUALS2.JAN1VGRANT WITH CURJAN1GRANT.VACBALANCE
								ENDIF
							ENDSCAN

							* now populate JAN1PGRANT in CURACCRUALS2
							SELECT CURACCRUALS2
							SCAN
								SELECT CURJAN1PERSONALGRANT
								LOCATE FOR FILE_NUM == CURACCRUALS2.FILE_NUM
								IF FOUND() THEN
									REPLACE CURACCRUALS2.JAN1PGRANT WITH CURJAN1PERSONALGRANT.BALANCE
								ENDIF
							ENDSCAN

							* now populate JAN1SGRANT in CURACCRUALS2
							SELECT CURACCRUALS2
							SCAN
								SELECT CURJAN1SICKGRANT
								LOCATE FOR FILE_NUM == CURACCRUALS2.FILE_NUM
								IF FOUND() THEN
									REPLACE CURACCRUALS2.JAN1SGRANT WITH CURJAN1SICKGRANT.BALANCE
								ENDIF
							ENDSCAN

							* now populate ALLGRANTS in CURACCRUALS2
							SELECT CURACCRUALS2
							SCAN
								SELECT CURALLGRANTS
								LOCATE FOR FILE_NUM == CURACCRUALS2.FILE_NUM
								IF FOUND() THEN
									REPLACE CURACCRUALS2.ALLGRANTS WITH CURALLGRANTS.VACBALANCE
								ENDIF
							ENDSCAN

							* now populate LATEGRANTS in CURACCRUALS2
							SELECT CURACCRUALS2
							SCAN
								SELECT CURALLGRANTSAFTERJAN1
								LOCATE FOR FILE_NUM == CURACCRUALS2.FILE_NUM
								IF FOUND() THEN
									REPLACE CURACCRUALS2.LATEGRANTS WITH CURALLGRANTSAFTERJAN1.VACBALANCE
								ENDIF
							ENDSCAN

*!*								* now populate CAPADJUST in CURACCRUALS2
*!*								SELECT CURACCRUALS2
*!*								SCAN
*!*									SELECT CURCAPLIMITREDUCTION
*!*									LOCATE FOR FILE_NUM == CURACCRUALS2.FILE_NUM
*!*									IF FOUND() THEN
*!*										REPLACE CURACCRUALS2.CAPADJUST WITH CURCAPLIMITREDUCTION.VACBALANCE
*!*									ENDIF
*!*								ENDSCAN

							*!*	SELECT CURACCRUALS
							*!*	BROW

							* get ADP info
							lcFileDate = PADL(MONTH(ldAsOfDate),2,"0") + "-"  + PADL(DAY(ldAsOfDate),2,"0") + "-" + PADL(YEAR(ldAsOfDate),4,"0")

							=SQLDISCONNECT(.nSQLHandle)

							*CLOSE DATABASES

							************************************************************************************************
							************************************************************************************************

							* Now get Hourly Rates from ADP into another cursor so we can add them to the accruals cursor
							* get ss#s from ADP
							OPEN DATABASE F:\UTIL\ADPCONNECTION\CONNECT1

							.nSQLHandle = SQLCONNECT("PCPAYWIN","MBENNETT","VP737A")

							IF .nSQLHandle > 0 THEN
								lcSQL2 = ;
									"SELECT " + ;
									" COMPANYCODE AS ADP_COMP, " + ;
									" {fn IFNULL(FILE#,0000)} AS FILE_NUM, " + ;
									" {fn LEFT(HOMEDEPARTMENT,2)} AS DIVISION, " + ;
									" 0 AS LOTR, " + ;
									" {fn IFNULL(NAME,'                             ')} AS NAME, " + ;
									" {fn IFNULL(RATE1AMT,0000.00)} AS HOURLY_WAGE " + ;
									" FROM REPORTS.V_EMPLOYEE " + ;
									" WHERE STATUS <> 'T' " + ;
									" AND COMPANYCODE IN ('E87','E88','E89') "

								IF .lTestMode THEN
									.TrackProgress('lcSQL2 = ' + lcSQL2, LOGIT+SENDIT)
								ENDIF

								IF USED('CURADPWAGES') THEN
									USE IN CURADPWAGES
								ENDIF

								IF .ExecSQL(lcSQL2, 'CURADPWAGES', RETURN_DATA_MANDATORY) THEN

*!*	SELECT CURADPWAGES
*!*	BROWSE
*!*	THROW


									* calculate hourly rates for non Hourlys in the cursor
									SELECT CURADPWAGES
									SCAN
										DO CASE
											*CASE CURADPWAGES.ADP_COMP <> 'SXI'
											CASE CURADPWAGES.ADP_COMP <> 'E87'
												* SALARY IS BIWEEKLY, CONVERT TO HOURLY
												REPLACE CURADPWAGES.HOURLY_WAGE WITH ( CURADPWAGES.HOURLY_WAGE / 80.000 )
											CASE (CURADPWAGES.DIVISION = '02') AND (CURADPWAGES.HOURLY_WAGE = 0)
												* OTR driver, use $20
												REPLACE CURADPWAGES.HOURLY_WAGE WITH 20.00, CURADPWAGES.LOTR WITH 1
											OTHERWISE
												* regular hourly employee - nothing to change
										ENDCASE
									ENDSCAN

									* now populate hourly_wage in CURACCRUALS2
									SELECT CURACCRUALS2
									SCAN
										SELECT CURADPWAGES
										LOCATE FOR FILE_NUM = VAL(CURACCRUALS2.FILE_NUM)
										IF FOUND() THEN
											REPLACE CURACCRUALS2.HOURLY_WAGE WITH CURADPWAGES.HOURLY_WAGE, ;
												CURACCRUALS2.IN_ADP WITH 1, CURACCRUALS2.LOTR WITH CURADPWAGES.LOTR ;
												IN CURACCRUALS2
										ENDIF
									ENDSCAN

									* populate NULL senioritydates with Hiredate
									SELECT CURACCRUALS2
									SCAN FOR ISNULL(SENIORDATE)
										REPLACE CURACCRUALS2.SENIORDATE WITH CURACCRUALS2.HIREDATE
									ENDSCAN
									
*!*							SELECT CURACCRUALS2
*!*							BROWSE
*!*							THROW


								ENDIF  &&  .ExecSQL(lcSQL2, 'CURADPWAGES', RETURN_DATA_MANDATORY)


								* sort cursor by seniority date
								* AND ADD WHEER CONDITION WHICH EXCLUDES EE'S WITH BLANK ACCRUALS PROFILES
								*!*	SELECT * ;
								*!*		FROM CURACCRUALS2 ;
								*!*		INTO CURSOR CURACCRUALS ;
								*!*		ORDER BY HIREDATE ;
								*!*		READWRITE
								SELECT * ;
									FROM CURACCRUALS2 ;
									INTO CURSOR CURACCRUALS ;
									WHERE FILE_NUM IN ;
									(SELECT FILE_NUM FROM CURPROFILES WHERE NOT EMPTY(ACPROFILE)) ;
									ORDER BY HIREDATE ;
									READWRITE


*!*				SELECT CURACCRUALS
*!*				GOTO top
*!*				brow
*!*				COPY TO c:\a\not_in_adp.xls XL5 FOR IN_ADP = 0
			

								************************************************************************************************
								************************************************************************************************

								WAIT WINDOW NOWAIT "Opening Excel..."
								oExcel = CREATEOBJECT("excel.application")
								oWorkbook = oExcel.workbooks.OPEN("F:\UTIL\KRONOS\TEMPLATES\Kronos2013VacationLiabilityReport.XLS")

								***********************************************************************************************************************
								***********************************************************************************************************************
								WAIT WINDOW NOWAIT "Looking for target directory..."
								* see if target directory exists, and, if not, create it

								lcTargetDirectory = "F:\UTIL\KRONOS\ACCRUALSREPORTS\"
								* for testing
								*lcTargetDirectory = "C:\TIMECLK\ADPFILES\" + lcFileDate + "\"

								* create directory if it doesn't exist
								IF NOT DIRECTORY(lcTargetDirectory) THEN
									MKDIR (lcTargetDirectory)
									WAIT WINDOW TIMEOUT 1 "Made Target Directory " + lcTargetDirectory
								ENDIF

								* if target directory exists, save there
								llSaveAgain = .F.
								IF DIRECTORY(lcTargetDirectory) THEN
									lcFiletoSaveAs = lcTargetDirectory + "Kronos_2013_VACATION_LIABILITY_" + lcFileDate
									lcXLFileName = lcFiletoSaveAs + ".XLS"
									lcXLFileName2 = lcTargetDirectory + "VAC_BALANCE_" + lcFileDate + ".XLS"
									IF FILE(lcXLFileName) THEN
										DELETE FILE (lcXLFileName)
									ENDIF
									oWorkbook.SAVEAS(lcFiletoSaveAs)
									* set flag to save again after sheet is populated
									llSaveAgain = .T.
								ENDIF
								***********************************************************************************************************************
								***********************************************************************************************************************

								lnRow = 2
								lnStartRow = lnRow + 1
								lcStartRow = ALLTRIM(STR(lnStartRow))

								oExcel.VISIBLE = .F.
								oWorksheet = oWorkbook.Worksheets[1]
								oWorksheet.RANGE("A" + lcStartRow,"V1000").clearcontents()

								oWorksheet.RANGE("A" + lcStartRow,"V1000").FONT.SIZE = 10
								oWorksheet.RANGE("A" + lcStartRow,"V1000").FONT.NAME = "Arial Narrow"
								oWorksheet.RANGE("A" + lcStartRow,"V1000").FONT.bold = .T.

								lcTitle = "Kronos 2013 Vacation Liability Balances as of: " + DTOC(ldAsOfDate) + LINE_FEED + ;
									"For Employees Currently Active in ADP" + LINE_FEED + ;
									.cCompanyCodesToReport + LINE_FEED + ;
									.cMiscWhere
									
								oWorksheet.RANGE("A1").VALUE = lcTitle


								* main scan/processing
								
								IF .lJustReportTestEmployee THEN
									* SET IN_ADP = 1 for the test employee so he is reported on
									REPLACE ALL IN_ADP WITH 1 IN CURACCRUALS
								ENDIF								
								
								SELECT CURACCRUALS
								COPY FIELDS WORKSITE, EMPLOYEE, DIVISION, DEPT, FILE_NUM, SENIORDATE, TIMERVWR, COMPHOURS, VACRLVRBAL, PERSDAYBAL, VACBALANCE, SICKDAYS, CADRVRSICK TO (lcXLFileName2) XL5 FOR IN_ADP = 1


			***************************************************************************************************
			** THIS BLOCK IS RUN-ONCE CODE FOR BEGINNING OF YEAR REPORTS -- COMENT OUT LATER
			*.CreateMailMergeDBF()
			*THROW
IF .lRunExtraInfo THEN								
			.TrackProgress("In RunExtraInfo Block!", LOGIT+SENDIT)
			IF USED('CUREXTRAINFO') THEN
				USE IN CUREXTRAINFO
			ENDIF
			
*!*				ldAsOfDate = .dAsOfDate
			
			SELECT ldAsOfDate AS ASOFDATE, A.WORKSITE, A.FILE_NUM, A.EMPLOYEE, A.SENIORDATE, A.ADP_COMP, A.DIVISION, A.DEPT, ;
				A.TIMERVWR, A.TRNUM, A.JAN1VGRANT AS VACGRANT, A.VACBALANCE AS VACATION, A.VACRLVRBAL AS VACRLVR, A.PERSDAYBAL AS PERSONAL, A.SICKDAYS AS SICK, A.CADRVRSICK, ;
				B.ACPROFILE, B.PAYRULE ;
				FROM CURACCRUALS A ;
				INNER JOIN CURRULES B ;
				ON B.FILE_NUM = A.FILE_NUM ;
				INTO CURSOR CUREXTRAINFO ;
				ORDER BY A.DIVISION, A.SENIORDATE
									
			SELECT CUREXTRAINFO
			COPY TO ("C:\A\ACCRUALS_2013_FILES\BALANCES" + .cExtraInfoSuffix + DTOS(.dAsOfDate) + ".XLS") XL5
			
*!*				* UPDATE DBF THAT DRIVES DAILY TR ACCRUALS REPORTS
*!*				SELECT CUREXTRAINFO
*!*				COPY TO F:\UTIL\KRONOS\ACCRUALSDATA\ACCRUALS
			
			oWorkbook.SAVE()
			
	THROW
	
ENDIF && .lRunExtraInfo
			
			****************************************************************************************************
	
	
							IF .lUseCADriverSick THEN
								SELECT CURACCRUALS
								SCAN
									REPLACE CURACCRUALS.SICKDAYS WITH CURACCRUALS.CADRVRSICK IN CURACCRUALS
								ENDSCAN
							ENDIF

								
								SELECT CURACCRUALS
								SCAN FOR IN_ADP = 1

									lnRow = lnRow + 1
									lcRow = LTRIM(STR(lnRow))
									oWorksheet.RANGE("A" + lcRow).VALUE = "'" + ALLTRIM(CURACCRUALS.DIVISION)
									oWorksheet.RANGE("B" + lcRow).VALUE = ALLTRIM(CURACCRUALS.DEPT)
									oWorksheet.RANGE("C" + lcRow).VALUE = ALLTRIM(CURACCRUALS.EMPLOYEE)
									oWorksheet.RANGE("D" + lcRow).VALUE = ALLTRIM(CURACCRUALS.FILE_NUM)
									oWorksheet.RANGE("E" + lcRow).VALUE = ALLTRIM(CURACCRUALS.WORKSITE)
									oWorksheet.RANGE("F" + lcRow).VALUE = CURACCRUALS.HIREDATE
									oWorksheet.RANGE("G" + lcRow).VALUE = CURACCRUALS.SENIORDATE

									oWorksheet.RANGE("H" + lcRow).VALUE = CURACCRUALS.ALLGRANTS
									
*!*										* special processing for EEs hired after 1/1, who will not have a 1/1 balance
*!*										IF (CURACCRUALS.HIREDATE > .dJanuary1st) THEN
*!*											llNewHire = .T.
*!*											lnHireMonth = MONTH(CURACCRUALS.HIREDATE)
*!*											*!*	DO CASE
*!*											*!*		CASE INLIST(lnHireMonth,1,2,3)
*!*											*!*			lnJAN1VACGRANT = 24.0
*!*											*!*		CASE INLIST(lnHireMonth,4,5,6)
*!*											*!*			lnJAN1VACGRANT = 16.0
*!*											*!*		CASE INLIST(lnHireMonth,7,8,9)
*!*											*!*			lnJAN1VACGRANT = 8.0
*!*											*!*		OTHERWISE && CASE INLIST(lnHireMonth,10,11,12)
*!*											*!*			lnJAN1VACGRANT = 0.0
*!*											*!*	ENDCASE
*!*											
*!*											* for new hires, put total of all Grants for the year in Col H
*!*											*oWorksheet.RANGE("H" + lcRow).VALUE = lnJAN1VACGRANT
*!*											oWorksheet.RANGE("H" + lcRow).VALUE = CURACCRUALS.ALLGRANTS
*!*											
*!*											* calc accrued formula, which will be used in Col K
*!*											lnProRatedMonths = (lnAsOfMonth - lnHireMonth) + 1
*!*											lnTotalMonths = 13 - lnHireMonth
*!*											lcAccruedFormula = "=H" + lcRow + "*(" + TRANSFORM( lnProRatedMonths) + "/" + TRANSFORM( lnTotalMonths ) + ")"
*!*											
*!*										ELSE
*!*											* the standard case; no special processing
*!*											llNewHire = .F.
*!*											oWorksheet.RANGE("H" + lcRow).VALUE = CURACCRUALS.JAN1VGRANT
*!*											
*!*											************************************************************************************
*!*											************************************************************************************
*!*											* added 4/30/08 MB
*!*											*** for Old Hires (i.e., not new hires) we want to identify total adjustments after 1/1
*!*											*** and put in column T so that we can manually correct things if necessary.
*!*											IF CURACCRUALS.LATEGRANTS <> 0.0 THEN
*!*												oWorksheet.RANGE("V" + lcRow).VALUE = CURACCRUALS.LATEGRANTS
*!*											ENDIF
*!*											************************************************************************************
*!*											************************************************************************************
*!*											
*!*										ENDIF
									
									
									*  Jan 1 balances
									oWorksheet.RANGE("I" + lcRow).VALUE = CURACCRUALS.JAN1VACBAL
																
									oWorksheet.RANGE("J" + lcRow).VALUE = CURACCRUALS.JAN1VRLVR
									
									* Current balances
									oWorksheet.RANGE("K" + lcRow).VALUE = CURACCRUALS.VACRLVRBAL									
									
									oWorksheet.RANGE("L" + lcRow).VALUE = CURACCRUALS.VACBALANCE

									oWorksheet.RANGE("M" + lcRow).VALUE = "=H" + lcRow + "-L" + lcRow  && Vac Taken
									
*!*										IF llNewHire THEN
*!*											* for new hires we will subtract current hours from all grants for the year (Col H)
*!*											* below is just wrong -- VAC rollover (col K) should not be subtracted in calc of reg Vacation used.
*!*											*oWorksheet.RANGE("M" + lcRow).VALUE = "=H" + lcRow + "-K" + lcRow + "-L" + lcRow  && Vac Taken
*!*											oWorksheet.RANGE("M" + lcRow).VALUE = "=H" + lcRow + "-L" + lcRow  && Vac Taken
*!*										ELSE
*!*											* standard case, subtract current hours from Jan 1 balance (VAC + ROLLOVER)
*!*											* and allow for any Vac cap adjustment in col S
*!*											*oWorksheet.RANGE("M" + lcRow).VALUE = "=I" + lcRow + "+J" + lcRow + "-K" + lcRow + "-L" + lcRow + "+S" + lcRow  && Vac Taken + VAC CAP ADJUSTMENT
*!*											* simplified this by removing Vac Rollover columns -- we really only want current accruable vac here 5/6/11 MB
*!*											oWorksheet.RANGE("M" + lcRow).VALUE = "=I" + lcRow + "-L" + lcRow  && Current year Vac Taken 
*!*										ENDIF
									
									oWorksheet.RANGE("N" + lcRow).VALUE = "=H" + lcRow   && Current Vac Accrued = total monthly grants ytd = column H

*!*										* special processing for EEs hired after 1/1, who will not have a 1/1 balance
*!*										IF llNewHire THEN
*!*											oWorksheet.RANGE("N" + lcRow).VALUE = lcAccruedFormula
*!*										ELSE
*!*											* the standard case
*!*											oWorksheet.RANGE("N" + lcRow).VALUE = "=I" + lcRow + "*(" + TRANSFORM( lnAsOfMonth ) + "/12)"  && Current Vac Accrued
*!*										ENDIF

									* Vac Liability Hours
									* = 100% of current rollover (col K) + current year grant prorated for # of months (col N) - vac Days taken (col M)
									oWorksheet.RANGE("O" + lcRow).VALUE = "=K" + lcRow + "+N" + lcRow + "-M" + lcRow 
									
									 
									oWorksheet.RANGE("P" + lcRow).VALUE = CURACCRUALS.HOURLY_WAGE
									oWorksheet.RANGE("Q" + lcRow).VALUE = "=O" + lcRow + "*P" + lcRow									

*!*										oWorksheet.RANGE("R" + lcRow).VALUE = 320  && current vac cap = 40 days
*!*										
*!*										oWorksheet.RANGE("S" + lcRow).VALUE = CURACCRUALS.CAPADJUST									

									IF .lCalcSickBalance THEN
										IF .lUseCADriverSick THEN
											oWorksheet.RANGE("R" + lcRow).VALUE = CURACCRUALS.CADRVRSICK
										ELSE
											oWorksheet.RANGE("R" + lcRow).VALUE = CURACCRUALS.SICKDAYS
										ENDIF
										IF llNewHire THEN
											lcSICKAccruedFormula = "=R" + lcRow + "*(" + TRANSFORM( lnProRatedMonths) + "/" + TRANSFORM( lnTotalMonths ) + ")"
											oWorksheet.RANGE("S" + lcRow).VALUE = lcSICKAccruedFormula
										ELSE
											* the standard case
											oWorksheet.RANGE("S" + lcRow).VALUE = "=R" + lcRow + "*(" + TRANSFORM( lnAsOfMonth ) + "/12)"  && Current Sick Accrued
										ENDIF
										oWorksheet.RANGE("T" + lcRow).VALUE = "=S" + lcRow + "*P" + lcRow
									ENDIF

									IF .lCalcBDayBalance THEN
										oWorksheet.RANGE("U" + lcRow).VALUE = CURACCRUALS.PERSDAYBAL
									ENDIF

									oWorksheet.RANGE("W" + lcRow).VALUE = CURACCRUALS.JAN1PGRANT
									oWorksheet.RANGE("X" + lcRow).VALUE = CURACCRUALS.JAN1SGRANT

									*oWorksheet.RANGE("T" + lcRow).VALUE = IIF(CURACCRUALS.IN_ADP = 0,"x","")
									
									oWorksheet.RANGE("Y" + lcRow).VALUE = CURACCRUALS.ADP_COMP

									oWorksheet.RANGE("H" + lcRow,"O" + lcRow).NumberFormat = "#,##0.00"
									oWorksheet.RANGE("P" + lcRow,"Q" + lcRow).NumberFormat = "$#,##0.00"
									oWorksheet.RANGE("R" + lcRow,"V" + lcRow).NumberFormat = "#,##0.00"
									oWorksheet.RANGE("T" + lcRow).NumberFormat = "$#,##0.00"

									oWorksheet.RANGE("A" + lcRow,"X" + lcRow).Interior.ColorIndex = IIF(((lnRow % 2) = 1),15,0)
									
									
									*!*	* Vacation Cap processing
									*!*	IF ( NOT llNewHire ) AND ( CURACCRUALS.JAN1VGRANT > 0.0 ) THEN
									*!*		* calculate vac cap
									*!*		lnVacationCap = 2 * CURACCRUALS.JAN1VGRANT
									*!*		oWorksheet.RANGE("Q" + lcRow).VALUE = lnVacationCap
									*!*		* flag if current vac balance >= CAP
									*!*		lnOverVacationCap = CURACCRUALS.VACBALANCE - lnVacationCap
									*!*		IF lnOverVacationCap > 0 THEN
									*!*			oWorksheet.RANGE("R" + lcRow).VALUE = lnOverVacationCap
									*!*			*oWorksheet.RANGE("A" + lcRow,"S" + lcRow).Font.ColorIndex = 3
									*!*		ENDIF
									*!*	ENDIF
																		

								ENDSCAN

								lnEndRow = lnRow
								lcEndRow = ALLTRIM(STR(lnEndRow))

								*oWorksheet.RANGE("A" + lcStartRow,"T" + lcRow).SELECT
								oWorksheet.PageSetup.PrintArea = "$A$"  + lcStartRow + ":$X$" + lcRow

								* save again
								IF llSaveAgain THEN
									oWorkbook.SAVE()
								ENDIF
								oWorkbook.CLOSE()

							ELSE
								* connection error
								.TrackProgress('Unable to connect to ADP Payroll System.', LOGIT+SENDIT)
							ENDIF   &&  .nSQLHandle > 0

							.cBodyText = lcTopBodyText + CRLF + CRLF + .cPartTimeList + CRLF + CRLF + ;
								"==================================================================================================================" + ;
								CRLF + "<report log follows>" + ;
								CRLF + CRLF + .cBodyText

							IF FILE(lcXLFileName) THEN
								.cAttach = lcXLFileName
								.TrackProgress('Created Vacation Liability File : ' + lcXLFileName, LOGIT+SENDIT)
								.TrackProgress('Kronos 2013 Vacation Liability Report process ended normally.', LOGIT+SENDIT+NOWAITIT)
							ELSE
								.TrackProgress('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!', LOGIT+SENDIT)
								.TrackProgress("ERROR: There was a problem Creating Accruals File : " + lcXLFileName, LOGIT+SENDIT)
								.TrackProgress('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!', LOGIT+SENDIT)
							ENDIF
							
							
							**  COPY ALL FIELDS TO A SPREADSHEET
							SELECT CURACCRUALS
							COPY TO ("C:\A\ACCRUALS_2013_FILES\ACCINFO_ALL_" + DTOS(.dAsOfDate) + ".XLS") XL5
							COPY FIELDS ADP_COMP, DIVISION, DEPT, EMPLOYEE, FILE_NUM, WORKSITE, JAN1VGRANT, JAN1PGRANT, JAN1SGRANT, JAN1VRLVR TO ("C:\A\ACCRUALS_2013_FILES\ACCINFO_SOME_" + DTOS(.dAsOfDate) + ".XLS") XL5

						ENDIF  && EOF() CURACCRUALS

					ENDIF  &&  .ExecSQL(lcSQLCurrentBalance, 'CURWTKEMPLOYEE', RETURN_DATA_MANDATORY)

					CLOSE DATABASES ALL
				ELSE
					* connection error
					.TrackProgress('Unable to connect to WTK System.', LOGIT+SENDIT)
				ENDIF   &&  .nSQLHandle > 0
			CATCH TO loError

				.TrackProgress('There was an error.',LOGIT+SENDIT)
				.TrackProgress('The Error # is: ' + TRANSFORM(loError.ERRORNO), LOGIT+SENDIT)
				.TrackProgress('The Error Message is: ' + TRANSFORM(loError.MESSAGE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred in Program: ' + TRANSFORM(loError.PROCEDURE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred at line #: ' + TRANSFORM(loError.LINENO), LOGIT+SENDIT)
				lnNumberOfErrors = lnNumberOfErrors + 1
				IF TYPE('oExcel') = 'O' AND NOT ISNULL(oExcel) THEN
					oExcel.QUIT()
				ENDIF
				CLOSE DATA
				CLOSE ALL

			ENDTRY

			CLOSE DATA
			WAIT CLEAR
			***************** INTERNAL email results ******************************
			.TrackProgress('About to send status email.',LOGIT)
			.TrackProgress('==================================================================================================================', SENDIT)
			.TrackProgress('The logfile is: ' + .cLogFile, SENDIT)
			.TrackProgress('Kronos Vacation Liability Report process started: ' + .cStartTime, LOGIT+SENDIT)
			.TrackProgress('Kronos Vacation Liability Report process finished: ' + TTOC(DATETIME()), LOGIT+SENDIT)

			IF .lSendInternalEmailIsOn THEN
				* try to trap error from not having dartmail dll's registered on user's pc...
				TRY
					DO FORM dartmail2 WITH .cSendTo,.cFrom,.cSubject,.cCC,.cAttach,.cBodyText,"A"
					.TrackProgress('Sent status email.',LOGIT)
				CATCH TO loError
					=MESSAGEBOX('There was an error sending the status email. Please contact Mark Bennett.',0+64,"Kronos Vacation Liability Report")
					.TrackProgress('There was an error sending the status email.',LOGIT)
					.TrackProgress('The Error # is: ' + TRANSFORM(loError.ERRORNO), LOGIT)
					.TrackProgress('The Error Message is: ' + TRANSFORM(loError.MESSAGE), LOGIT)
					.TrackProgress('The Error occurred in Program: ' + TRANSFORM(loError.PROCEDURE), LOGIT)
					.TrackProgress('The Error occurred at line #: ' + TRANSFORM(loError.LINENO), LOGIT)
					lnNumberOfErrors = lnNumberOfErrors + 1
					CLOSE DATA
				ENDTRY
			ELSE
				.TrackProgress('Did not send status email: THIS.lSendInternalEmailIsOn = FALSE.',LOGIT)
			ENDIF

		ENDWITH
		RETURN
	ENDFUNC && main



	PROCEDURE CreateMailMergeDBF

		LOCAL lcLine1, lcLine2, lcLine3, lcLine4, lcLine5, lcLine6, lcLine7, lcLine8, llCalifornia

		IF USED('CURMAILMERGE') THEN
			USE IN CURMAILMERGE
		ENDIF

		SELECT A.FILE_NUM, A.EMPLOYEE, A.SENIORDATE, A.ADP_COMP, A.DIVISION, A.DEPT, A.WORKSITE, ;
			A.JAN1VACBAL/8 AS VACATION, A.JAN1VRLVR/8 AS VROLLOVER, A.PERSDAYBAL/8 AS PERSONAL, A.SICKDAYS/8 AS SICK, ;
			SPACE(70) AS CLINE1, SPACE(70) AS CLINE2, SPACE(70) AS CLINE3, SPACE(70) AS CLINE4, SPACE(70) AS CLINE5, SPACE(70) AS CLINE6, ;
			SPACE(70) AS CLINE7, SPACE(70) AS CLINE8 ;
			FROM CURACCRUALS A ;
			INTO CURSOR CURMAILMERGE ;
			ORDER BY A.DIVISION, A.EMPLOYEE ;
			READWRITE

		SELECT CURMAILMERGE
		SCAN
		
			llCalifornia = INLIST(CURMAILMERGE.WORKSITE,'CA4','CA5','CA6','CA7','CA8','SP1','SP2')
			
			lcLine1 = "For: " + ALLTRIM(CURMAILMERGE.EMPLOYEE)
			
			lcLine2 = "Your effective Hire Date was: " + TRANSFORM(TTOD(CURMAILMERGE.SENIORDATE))
			lcLine3 = "Your 1/1/2013 Vacation grant was: " + TRANSFORM(ROUND(CURMAILMERGE.VACATION,1)) + IIF(CURMAILMERGE.VACATION=1," day"," days")
			IF CURMAILMERGE.VROLLOVER > 0 THEN
				lcLine4 = "Your unused Vacation that rolled over from 2010 was: " + TRANSFORM(ROUND(CURMAILMERGE.VROLLOVER,1)) + IIF(CURMAILMERGE.VROLLOVER=1," day"," days")
				lcLine5 = "Your total 1/1/2013 Vacation balance was: " + TRANSFORM(ROUND(CURMAILMERGE.VACATION + CURMAILMERGE.VROLLOVER,1)) + IIF((CURMAILMERGE.VACATION + CURMAILMERGE.VROLLOVER)=1," day"," days")
				IF llCalifornia THEN
					*lcLine7 = "CA: Your rolled over Vacation days must all be used by 03/31/2013."
					*lcLine8 = "CA: Your granted Vacation days must all be used by 12/31/2013."
					lcLine7 = ""
					lcLine8 = ""
				ELSE
					lcLine7 = "Your rolled over Vacation days must all be used by 03/31/2013."
					lcLine8 = "Your granted Vacation days must all be used by 12/31/2013."
				ENDIF
			ELSE
				lcLine4 = ""
				lcLine5 = ""
				lcLine7 = ""
				lcLine8 = "Your Vacation days must all be used by 12/31/2013."
			ENDIF
			lcLine6 = "Your 1/1/2013 Personal Day balance was: " + TRANSFORM(ROUND(CURMAILMERGE.PERSONAL,1)) + IIF(CURMAILMERGE.PERSONAL=1," day"," days")

			REPLACE CURMAILMERGE.CLINE1 WITH lcLine1, CURMAILMERGE.CLINE2 WITH lcLine2, ;
				CURMAILMERGE.CLINE3 WITH lcLine3, CURMAILMERGE.CLINE4 WITH lcLine4, ;
				CURMAILMERGE.CLINE5 WITH lcLine5, CURMAILMERGE.CLINE6 WITH lcLine6 ;
				CURMAILMERGE.CLINE7 WITH lcLine7, CURMAILMERGE.CLINE8 WITH lcLine8 ;
				IN CURMAILMERGE

		ENDSCAN

*!*			SELECT CURMAILMERGE
*!*			BROWSE
		
		SELECT CURMAILMERGE
		COPY TO C:\A\ACCRUALS_2013_FILES\MERGE.XLS XL5

	ENDPROC
	
	

	FUNCTION GetSQLStringFromDate
		LPARAMETERS tdAsOfDate, tlAdjustMaxEffectiveDate
		LOCAL lcSQL, lcCompanyCodesToReport, lcMiscWhere, lcAdjustMaxEffectiveDate
		LOCAL ldAsOfDate, lcAsOfDate, lcSQLAsOfDate, lcExcludeInactiveTHENWhere, lcTS_EffectiveFrom, lcTS_TODAY_EffectiveTo
		LOCAL lcTS_EffectiveFrom, lcTS_EffectiveTo, lcAccrualsTable, lcSQL2, lnAdjustment, lc9999Where, lcExcludeInactiveNOWWhere

		WITH THIS
			ldAsOfDate = tdAsOfDate

			lcAsOfDate = STRTRAN(DTOC(ldAsOfDate),"/","-")

			SET DATE YMD

			lcSQLAsOfDate = STRTRAN(DTOC(ldAsOfDate),"/","")
			lcTS_EffectiveFrom   = "{ts '" + TRANSFORM(YEAR(ldAsOfDate)) + "-" + PADL(MONTH(ldAsOfDate),2,"0") + "-" + PADL(DAY(ldAsOfDate),2,"0") + " 00:00:00.0'}"
			lcTS_EffectiveTo = "{ts '" + TRANSFORM(YEAR(ldAsOfDate)) + "-" + PADL(MONTH(ldAsOfDate),2,"0") + "-" + PADL(DAY(ldAsOfDate),2,"0") + " 23:59:59.9'}"

			* for getting those ee's active as of today
			lcTS_TODAY_EffectiveFrom   = "{ts '" + TRANSFORM(YEAR(.dToday)) + "-" + PADL(MONTH(.dToday),2,"0") + "-" + PADL(DAY(.dToday),2,"0") + " 00:00:00.0'}"
			lcTS_TODAY_EffectiveTo = "{ts '" + TRANSFORM(YEAR(.dToday)) + "-" + PADL(MONTH(.dToday),2,"0") + "-" + PADL(DAY(.dToday),2,"0") + " 23:59:59.9'}"

			SET DATE AMERICAN

			lcCompanyCodesToReport = .cCompanyCodesToReport

			*************************************
			*************************************
			*************************************
			** Further filtering !
			lcMiscWhere = .cMiscWhere
			*************************************
			*************************************
			*************************************	
			
			* logic to allow balances prior to "signed off" dates to be determined
			* to do this, pass tlAdjustMaxEffectiveDate = .T.
			IF tlAdjustMaxEffectiveDate THEN
				lcAdjustMaxEffectiveDate = ;
					"AND A.EFFECTIVEDATE >= " + CRLF + ;
					"	( " + CRLF + ;
					"	SELECT MAX(EFFECTIVEDATE) " + CRLF + ;
					"	FROM ACCRUALTRAN " + CRLF + ;
					"	WHERE PERSONID = A.PERSONID " + CRLF + ;
					"	AND EFFECTIVEDATE <= " + lcTS_EffectiveFrom + CRLF + ;
					"	AND ACCRUALCODEID = A.ACCRUALCODEID " + CRLF + ;
					"	AND TYPE IN (" + TRANSFORM(ACCRUAL_RESET) + "," + TRANSFORM(ACCRUAL_CARRY_FORWARD) + ") " + CRLF + ;
					"	) "				
			ELSE
				lcAdjustMaxEffectiveDate = ;
					"AND A.EFFECTIVEDATE >= " + CRLF + ;
					"	( " + CRLF + ;
					"	SELECT MAX(EFFECTIVEDATE) " + CRLF + ;
					"	FROM ACCRUALTRAN " + CRLF + ;
					"	WHERE PERSONID = A.PERSONID " + CRLF + ;
					"	AND ACCRUALCODEID = A.ACCRUALCODEID " + CRLF + ;
					"	AND TYPE IN (" + TRANSFORM(ACCRUAL_RESET) + "," + TRANSFORM(ACCRUAL_CARRY_FORWARD) + ") " + CRLF + ;
					"	) "				
			ENDIF

			IF .lExclude9999s THEN
				lc9999Where = " AND L.LABORLEV7DSC <> '9999' "
			ELSE
				lc9999Where = ""
			ENDIF

			IF .lExcludeInactiveNOW THEN
				SET TEXTMERGE ON
				TEXT TO	lcExcludeInactiveNOWWhere NOSHOW
AND (( EXISTS
	 (SELECT * FROM PERSONSTATUSMM
      WHERE ((PERSONSTATUSMM.USERACCTSTATID = 1)
		AND (PERSONSTATUSMM.EFFECTIVEDTM <= <<lcTS_TODAY_EffectiveTo>>)
		AND (PERSONSTATUSMM.EXPIRATIONDTM > <<lcTS_TODAY_EffectiveFrom>>))
		AND (PERSONSTATUSMM.PERSONID = J.PERSONID))
OR EXISTS
	 (SELECT * FROM PERSONSTATUSMM
      WHERE ((PERSONSTATUSMM.EMPLOYMENTSTATID = 1)
		AND (PERSONSTATUSMM.EFFECTIVEDTM <= <<lcTS_TODAY_EffectiveTo>>)
		AND (PERSONSTATUSMM.EXPIRATIONDTM > <<lcTS_TODAY_EffectiveFrom>>))
		AND (PERSONSTATUSMM.PERSONID = J.PERSONID)))
		AND (J.PERSONID > 0)
		AND (J.DELETEDSW = 0))
				ENDTEXT
				SET TEXTMERGE OFF
			ELSE
				lcExcludeInactiveNOWWhere = ""
			ENDIF


			IF .lExcludeInactiveTHEN THEN
				SET TEXTMERGE ON
				TEXT TO	lcExcludeInactiveTHENWhere NOSHOW
AND (( EXISTS
	 (SELECT * FROM PERSONSTATUSMM
      WHERE ((PERSONSTATUSMM.USERACCTSTATID = 1)
		AND (PERSONSTATUSMM.EFFECTIVEDTM <= <<lcTS_EffectiveTo>>)
		AND (PERSONSTATUSMM.EXPIRATIONDTM > <<lcTS_EffectiveFrom>>))
		AND (PERSONSTATUSMM.PERSONID = J.PERSONID))
OR EXISTS
	 (SELECT * FROM PERSONSTATUSMM
      WHERE ((PERSONSTATUSMM.EMPLOYMENTSTATID = 1)
		AND (PERSONSTATUSMM.EFFECTIVEDTM <= <<lcTS_EffectiveTo>>)
		AND (PERSONSTATUSMM.EXPIRATIONDTM > <<lcTS_EffectiveFrom>>))
		AND (PERSONSTATUSMM.PERSONID = J.PERSONID)))
		AND (J.PERSONID > 0)
		AND (J.DELETEDSW = 0))
				ENDTEXT
				SET TEXTMERGE OFF
			ELSE
				lcExcludeInactiveTHENWhere = ""
			ENDIF

			* main SQL

			*************************************************************************************************
			SET TEXTMERGE ON

			TEXT TO	lcSQL NOSHOW

SELECT
	L.LABORLEV7NM AS TRNUM, A.PERSONID, P.PERSONNUM AS FILE_NUM, P.FULLNM AS EMPLOYEE, J.EMPLOYEEID,
	L.LABORLEV1NM AS ADP_COMP, L.LABORLEV2NM AS DIVISION, L.LABORLEV3NM AS DEPT, L.LABORLEV4NM AS WORKSITE, 
	L.LABORLEV7DSC AS TIMERVWR, R.ACTUALCUSTOMDTM AS SENIORDATE,	P.COMPANYHIREDTM AS HIREDATE,
	(SUM(CASE A.ACCRUALCODEID WHEN ACCRUAL_SICK_AVAILABLE_CODE THEN A.AMOUNT ELSE 0.000 END) / SECS_PER_HOUR) AS SICKDAYS,
	(SUM(CASE A.ACCRUALCODEID WHEN ACCRUAL_CA_DRIVER_SICK_CODE THEN A.AMOUNT ELSE 0.000 END) / SECS_PER_HOUR) AS CADRVRSICK,
	(SUM(CASE A.ACCRUALCODEID WHEN ACCRUAL_BIRTHDAY_CODE THEN A.AMOUNT ELSE 0.000 END) / SECS_PER_HOUR) AS B_ACCRUAL,
	(SUM(CASE A.ACCRUALCODEID WHEN ACCRUAL_VACATION_AVAILABLE_CODE THEN A.AMOUNT ELSE 0.000 END) / SECS_PER_HOUR) AS VACBALANCE,
	(SUM(CASE A.ACCRUALCODEID WHEN ACCRUAL_VACATION_ROLLOVER_CODE THEN A.AMOUNT ELSE 0.000 END) / SECS_PER_HOUR) AS VACRLVRBAL,
	(SUM(CASE A.ACCRUALCODEID WHEN ACCRUAL_PERS_DAY_CODE THEN A.AMOUNT ELSE 0.000 END) / SECS_PER_HOUR) AS PERSDAYBAL,
	000000.0000 AS HOURLY_WAGE, 0 AS LOTR, 0 AS IN_ADP
FROM ACCRUALTRAN A
INNER JOIN PERSON P
ON P.PERSONID = A.PERSONID
INNER JOIN JAIDS J
ON J.PERSONID = P.PERSONID
INNER JOIN COMBHOMEACCT C
ON C.EMPLOYEEID = J.EMPLOYEEID
INNER JOIN LABORACCT L
ON L.LABORACCTID = C.LABORACCTID
LEFT OUTER JOIN PRSNCSTMDATEMM R
ON R.PERSONID = J.PERSONID
AND R.CUSTOMDATETYPEID = 1
WHERE A.ACCRUALCODEID IN (ACCRUAL_BIRTHDAY_CODE,ACCRUAL_VACATION_AVAILABLE_CODE,ACCRUAL_SICK_AVAILABLE_CODE,ACCRUAL_VACATION_ROLLOVER_CODE,ACCRUAL_PERS_DAY_CODE,ACCRUAL_CA_DRIVER_SICK_CODE)
<<lcAdjustMaxEffectiveDate>>
AND A.EFFECTIVEDATE <= <<lcTS_EffectiveTo>>
AND J.PERSONID > 0
AND J.DELETEDSW = 0
AND C.EFFECTIVEDTM <= <<lcTS_EffectiveTo>>
AND C.EXPIRATIONDTM > <<lcTS_EffectiveFrom>>
AND L.LABORLEV1NM IN <<lcCompanyCodesToReport>> <<lcMiscWhere>>
<<lc9999Where>> <<lcExcludeInactiveNOWWhere>> <<lcExcludeInactiveTHENWhere>>
GROUP BY L.LABORLEV7NM, A.PERSONID, P.PERSONNUM, P.FULLNM, J.EMPLOYEEID, L.LABORLEV1NM, L.LABORLEV2NM, L.LABORLEV3NM, L.LABORLEV4NM, L.LABORLEV7DSC, R.ACTUALCUSTOMDTM, P.COMPANYHIREDTM
ORDER BY P.FULLNM

			ENDTEXT

		ENDWITH
		RETURN lcSQL
	ENDFUNC  &&  GetSQLStringFromDate


	FUNCTION GetOnlyVacGrantsFromDateRange
		LPARAMETERS tdFromDate, tdToDate
		LOCAL lcSQL, lcTS_EffectiveFrom, lcTS_EffectiveTo
		WITH THIS
			SET DATE YMD
			lcTS_EffectiveFrom   = "{ts '" + TRANSFORM(YEAR(tdFromDate)) + "-" + PADL(MONTH(tdFromDate),2,"0") + "-" + PADL(DAY(tdFromDate),2,"0") + " 00:00:00.0'}"
			lcTS_EffectiveTo = "{ts '" + TRANSFORM(YEAR(tdToDate)) + "-" + PADL(MONTH(tdToDate),2,"0") + "-" + PADL(DAY(tdToDate),2,"0") + " 23:59:59.9'}"
			SET DATE AMERICAN
			*************************************************************************************************
			SET TEXTMERGE ON

			TEXT TO	lcSQL NOSHOW
SELECT
P.PERSONNUM AS FILE_NUM, (SUM(CASE A.ACCRUALCODEID WHEN ACCRUAL_VACATION_AVAILABLE_CODE THEN A.AMOUNT ELSE 0.000 END) / SECS_PER_HOUR) AS VACBALANCE
FROM ACCRUALTRAN A
INNER JOIN PERSON P
ON P.PERSONID = A.PERSONID
WHERE A.ACCRUALCODEID IN (ACCRUAL_VACATION_AVAILABLE_CODE)
AND A.EFFECTIVEDATE >= <<lcTS_EffectiveFrom>>
AND A.EFFECTIVEDATE <= <<lcTS_EffectiveTo>>
AND A.TYPE IN (ACCRUAL_GRANT)
AND A.PERSONID > 0
GROUP BY P.PERSONNUM
ORDER BY P.PERSONNUM
			ENDTEXT

		ENDWITH
		RETURN lcSQL
	ENDFUNC  && GetOnlyVacGrantsFromDateRange



	FUNCTION GetOnlyPersonalGrantsFromDateRange
		LPARAMETERS tdFromDate, tdToDate
		LOCAL lcSQL, lcTS_EffectiveFrom, lcTS_EffectiveTo
		WITH THIS
			SET DATE YMD
			lcTS_EffectiveFrom   = "{ts '" + TRANSFORM(YEAR(tdFromDate)) + "-" + PADL(MONTH(tdFromDate),2,"0") + "-" + PADL(DAY(tdFromDate),2,"0") + " 00:00:00.0'}"
			lcTS_EffectiveTo = "{ts '" + TRANSFORM(YEAR(tdToDate)) + "-" + PADL(MONTH(tdToDate),2,"0") + "-" + PADL(DAY(tdToDate),2,"0") + " 23:59:59.9'}"
			SET DATE AMERICAN
			*************************************************************************************************
			SET TEXTMERGE ON

			TEXT TO	lcSQL NOSHOW
SELECT
P.PERSONNUM AS FILE_NUM, (SUM(CASE A.ACCRUALCODEID WHEN ACCRUAL_PERS_DAY_CODE THEN A.AMOUNT ELSE 0.000 END) / SECS_PER_HOUR) AS BALANCE
FROM ACCRUALTRAN A
INNER JOIN PERSON P
ON P.PERSONID = A.PERSONID
WHERE A.ACCRUALCODEID IN (ACCRUAL_PERS_DAY_CODE)
AND A.EFFECTIVEDATE >= <<lcTS_EffectiveFrom>>
AND A.EFFECTIVEDATE <= <<lcTS_EffectiveTo>>
AND A.TYPE IN (ACCRUAL_GRANT)
AND A.PERSONID > 0
GROUP BY P.PERSONNUM
ORDER BY P.PERSONNUM
			ENDTEXT

		ENDWITH
		RETURN lcSQL
	ENDFUNC  && GetOnlyPersonalGrantsFromDateRange



	FUNCTION GetOnlySickGrantsFromDateRange
		LPARAMETERS tdFromDate, tdToDate
		LOCAL lcSQL, lcTS_EffectiveFrom, lcTS_EffectiveTo
		WITH THIS
			SET DATE YMD
			lcTS_EffectiveFrom   = "{ts '" + TRANSFORM(YEAR(tdFromDate)) + "-" + PADL(MONTH(tdFromDate),2,"0") + "-" + PADL(DAY(tdFromDate),2,"0") + " 00:00:00.0'}"
			lcTS_EffectiveTo = "{ts '" + TRANSFORM(YEAR(tdToDate)) + "-" + PADL(MONTH(tdToDate),2,"0") + "-" + PADL(DAY(tdToDate),2,"0") + " 23:59:59.9'}"
			SET DATE AMERICAN
			*************************************************************************************************
			SET TEXTMERGE ON

			TEXT TO	lcSQL NOSHOW
SELECT
P.PERSONNUM AS FILE_NUM, (SUM(CASE A.ACCRUALCODEID WHEN ACCRUAL_SICK_AVAILABLE_CODE THEN A.AMOUNT ELSE 0.000 END) / SECS_PER_HOUR) AS BALANCE
FROM ACCRUALTRAN A
INNER JOIN PERSON P
ON P.PERSONID = A.PERSONID
WHERE A.ACCRUALCODEID IN (ACCRUAL_SICK_AVAILABLE_CODE)
AND A.EFFECTIVEDATE >= <<lcTS_EffectiveFrom>>
AND A.EFFECTIVEDATE <= <<lcTS_EffectiveTo>>
AND A.TYPE IN (ACCRUAL_GRANT)
AND A.PERSONID > 0
GROUP BY P.PERSONNUM
ORDER BY P.PERSONNUM
			ENDTEXT

		ENDWITH
		RETURN lcSQL
	ENDFUNC  && GetOnlySickGrantsFromDateRange



	FUNCTION GetOnlyCADRVRSickGrantsFromDateRange
		LPARAMETERS tdFromDate, tdToDate
		LOCAL lcSQL, lcTS_EffectiveFrom, lcTS_EffectiveTo
		WITH THIS
			SET DATE YMD
			lcTS_EffectiveFrom   = "{ts '" + TRANSFORM(YEAR(tdFromDate)) + "-" + PADL(MONTH(tdFromDate),2,"0") + "-" + PADL(DAY(tdFromDate),2,"0") + " 00:00:00.0'}"
			lcTS_EffectiveTo = "{ts '" + TRANSFORM(YEAR(tdToDate)) + "-" + PADL(MONTH(tdToDate),2,"0") + "-" + PADL(DAY(tdToDate),2,"0") + " 23:59:59.9'}"
			SET DATE AMERICAN
			*************************************************************************************************
			SET TEXTMERGE ON

			TEXT TO	lcSQL NOSHOW
SELECT
P.PERSONNUM AS FILE_NUM, (SUM(CASE A.ACCRUALCODEID WHEN ACCRUAL_CA_DRIVER_SICK_CODE THEN A.AMOUNT ELSE 0.000 END) / SECS_PER_HOUR) AS BALANCE
FROM ACCRUALTRAN A
INNER JOIN PERSON P
ON P.PERSONID = A.PERSONID
WHERE A.ACCRUALCODEID IN (ACCRUAL_CA_DRIVER_SICK_CODE)
AND A.EFFECTIVEDATE >= <<lcTS_EffectiveFrom>>
AND A.EFFECTIVEDATE <= <<lcTS_EffectiveTo>>
AND A.TYPE IN (ACCRUAL_GRANT)
AND A.PERSONID > 0
GROUP BY P.PERSONNUM
ORDER BY P.PERSONNUM
			ENDTEXT

		ENDWITH
		RETURN lcSQL
	ENDFUNC  && GetOnlyCADRVRSickGrantsFromDateRange



	FUNCTION GetOnlyCapLimitReductions
		LPARAMETERS tdFromDate, tdToDate
		LOCAL lcSQL, lcTS_EffectiveFrom, lcTS_EffectiveTo
		WITH THIS
			SET DATE YMD
			lcTS_EffectiveFrom   = "{ts '" + TRANSFORM(YEAR(tdFromDate)) + "-" + PADL(MONTH(tdFromDate),2,"0") + "-" + PADL(DAY(tdFromDate),2,"0") + " 00:00:00.0'}"
			lcTS_EffectiveTo = "{ts '" + TRANSFORM(YEAR(tdToDate)) + "-" + PADL(MONTH(tdToDate),2,"0") + "-" + PADL(DAY(tdToDate),2,"0") + " 23:59:59.9'}"
			SET DATE AMERICAN
			*************************************************************************************************
			SET TEXTMERGE ON

			TEXT TO	lcSQL NOSHOW
SELECT
P.PERSONNUM AS FILE_NUM, (SUM(CASE A.ACCRUALCODEID WHEN ACCRUAL_VACATION_AVAILABLE_CODE THEN A.AMOUNT ELSE 0.000 END) / SECS_PER_HOUR) AS VACBALANCE
FROM ACCRUALTRAN A
INNER JOIN PERSON P
ON P.PERSONID = A.PERSONID
WHERE A.ACCRUALCODEID IN (ACCRUAL_VACATION_AVAILABLE_CODE)
AND A.EFFECTIVEDATE >= <<lcTS_EffectiveFrom>>
AND A.EFFECTIVEDATE <= <<lcTS_EffectiveTo>>
AND A.TYPE IN (CAP_LIMIT_REDUCTION)
AND A.PERSONID > 0
GROUP BY P.PERSONNUM
ORDER BY P.PERSONNUM
			ENDTEXT

		ENDWITH
		RETURN lcSQL
	ENDFUNC  && GetOnlyCapLimitReductions


	FUNCTION GetAccrualProfileSQL
		LOCAL lcSQL
		SET TEXTMERGE ON
		TEXT TO	lcSQL NOSHOW
SELECT
PERSONNUM AS FILE_NUM,
PERSONFULLNAME AS NAME,
{FN IFNULL(ACCRUALPRFLNAME,'')} AS ACPROFILE
FROM VP_EMPLOYEEV42
WHERE EMPLOYMENTSTATUS = 'Active' and HOMELABORLEVELNM1 <> 'TMP'
ORDER BY ACCRUALPRFLNAME
		ENDTEXT
		RETURN lcSQL
	ENDFUNC



	FUNCTION GetSQLCompBalance
		LPARAMETERS tdAsOfDate
		LOCAL lcSQL, lcCompanyCodesToReport, lcMiscWhere
		LOCAL ldAsOfDate, lcAsOfDate, lcSQLAsOfDate, lcExcludeInactiveTHENWhere, lcTS_EffectiveFrom, lcTS_TODAY_EffectiveTo
		LOCAL lcTS_EffectiveFrom, lcTS_EffectiveTo, lcAccrualsTable, lcSQL2, lnAdjustment, lc9999Where, lcExcludeInactiveNOWWhere

		WITH THIS
			ldAsOfDate = tdAsOfDate

			lcAsOfDate = STRTRAN(DTOC(ldAsOfDate),"/","-")

			SET DATE YMD

			lcSQLAsOfDate = STRTRAN(DTOC(ldAsOfDate),"/","")
			lcTS_EffectiveFrom   = "{ts '" + TRANSFORM(YEAR(ldAsOfDate)) + "-" + PADL(MONTH(ldAsOfDate),2,"0") + "-" + PADL(DAY(ldAsOfDate),2,"0") + " 00:00:00.0'}"
			lcTS_EffectiveTo = "{ts '" + TRANSFORM(YEAR(ldAsOfDate)) + "-" + PADL(MONTH(ldAsOfDate),2,"0") + "-" + PADL(DAY(ldAsOfDate),2,"0") + " 23:59:59.9'}"

			* for getting those ee's active as of today
			lcTS_TODAY_EffectiveFrom   = "{ts '" + TRANSFORM(YEAR(.dToday)) + "-" + PADL(MONTH(.dToday),2,"0") + "-" + PADL(DAY(.dToday),2,"0") + " 00:00:00.0'}"
			lcTS_TODAY_EffectiveTo = "{ts '" + TRANSFORM(YEAR(.dToday)) + "-" + PADL(MONTH(.dToday),2,"0") + "-" + PADL(DAY(.dToday),2,"0") + " 23:59:59.9'}"

			SET DATE AMERICAN

			lcCompanyCodesToReport = .cCompanyCodesToReport

			*************************************
			*************************************
			*************************************
			** Further filtering !
			lcMiscWhere = .cMiscWhere
			*************************************
			*************************************
			*************************************

			IF .lExclude9999s THEN
				lc9999Where = " AND L.LABORLEV7NM <> '9999' "
			ELSE
				lc9999Where = ""
			ENDIF

			IF .lExcludeInactiveNOW THEN
				SET TEXTMERGE ON
				TEXT TO	lcExcludeInactiveNOWWhere NOSHOW
AND (( EXISTS
	 (SELECT * FROM PERSONSTATUSMM
      WHERE ((PERSONSTATUSMM.USERACCTSTATID = 1)
		AND (PERSONSTATUSMM.EFFECTIVEDTM <= <<lcTS_TODAY_EffectiveTo>>)
		AND (PERSONSTATUSMM.EXPIRATIONDTM > <<lcTS_TODAY_EffectiveFrom>>))
		AND (PERSONSTATUSMM.PERSONID = J.PERSONID))
OR EXISTS
	 (SELECT * FROM PERSONSTATUSMM
      WHERE ((PERSONSTATUSMM.EMPLOYMENTSTATID = 1)
		AND (PERSONSTATUSMM.EFFECTIVEDTM <= <<lcTS_TODAY_EffectiveTo>>)
		AND (PERSONSTATUSMM.EXPIRATIONDTM > <<lcTS_TODAY_EffectiveFrom>>))
		AND (PERSONSTATUSMM.PERSONID = J.PERSONID)))
		AND (J.PERSONID > 0)
		AND (J.DELETEDSW = 0))
				ENDTEXT
				SET TEXTMERGE OFF
			ELSE
				lcExcludeInactiveNOWWhere = ""
			ENDIF


			IF .lExcludeInactiveTHEN THEN
				SET TEXTMERGE ON
				TEXT TO	lcExcludeInactiveTHENWhere NOSHOW
AND (( EXISTS
	 (SELECT * FROM PERSONSTATUSMM
      WHERE ((PERSONSTATUSMM.USERACCTSTATID = 1)
		AND (PERSONSTATUSMM.EFFECTIVEDTM <= <<lcTS_EffectiveTo>>)
		AND (PERSONSTATUSMM.EXPIRATIONDTM > <<lcTS_EffectiveFrom>>))
		AND (PERSONSTATUSMM.PERSONID = J.PERSONID))
OR EXISTS
	 (SELECT * FROM PERSONSTATUSMM
      WHERE ((PERSONSTATUSMM.EMPLOYMENTSTATID = 1)
		AND (PERSONSTATUSMM.EFFECTIVEDTM <= <<lcTS_EffectiveTo>>)
		AND (PERSONSTATUSMM.EXPIRATIONDTM > <<lcTS_EffectiveFrom>>))
		AND (PERSONSTATUSMM.PERSONID = J.PERSONID)))
		AND (J.PERSONID > 0)
		AND (J.DELETEDSW = 0))
				ENDTEXT
				SET TEXTMERGE OFF
			ELSE
				lcExcludeInactiveTHENWhere = ""
			ENDIF

			* main SQL

			*************************************************************************************************
			SET TEXTMERGE ON

			TEXT TO	lcSQL NOSHOW

SELECT
V.HOMELABORLEVELNM1 AS ADP_COMP, V.HOMELABORLEVELNM2 AS DIVISION, P.FULLNM AS EMPLOYEE, P.PERSONNUM AS FILE_NUM,
(SUM(CASE A.ACCRUALCODEID WHEN ACCRUAL_COMP_HOURS_CODE THEN A.AMOUNT ELSE 0.000 END) / 3600) AS COMPBAL
FROM ACCRUALTRAN A
INNER JOIN PERSON P
ON P.PERSONID = A.PERSONID
INNER JOIN JAIDS J
ON J.PERSONID = P.PERSONID
INNER JOIN COMBHOMEACCT C
ON C.EMPLOYEEID = J.EMPLOYEEID
INNER JOIN LABORACCT L
ON L.LABORACCTID = C.LABORACCTID
LEFT OUTER JOIN VP_EMPLOYEEV42 V
ON V.PERSONNUM = P.PERSONNUM
WHERE A.ACCRUALCODEID IN (ACCRUAL_COMP_HOURS_CODE)
AND A.TYPE IN (ACCRUAL_TAKEN,ACCRUAL_GRANT)
AND A.PERSONID > 0
AND A.EFFECTIVEDATE <= <<lcTS_EffectiveTo>>
AND C.EFFECTIVEDTM <= <<lcTS_EffectiveTo>>
AND C.EXPIRATIONDTM > <<lcTS_EffectiveFrom>>
AND L.LABORLEV1NM IN <<lcCompanyCodesToReport>> <<lcMiscWhere>> <<lc9999Where>> <<lcExcludeInactiveNOWWhere>> <<lcExcludeInactiveTHENWhere>>
GROUP BY V.HOMELABORLEVELNM1, V.HOMELABORLEVELNM2, P.FULLNM, P.PERSONNUM

			ENDTEXT

		ENDWITH
		RETURN lcSQL
	ENDFUNC  &&  GetSQLCompBalance



	FUNCTION ExecSQL
		LPARAMETERS tcSQL, tcCursorName, tlNoDataReturnedIsOkay
		LOCAL llRetval, lnResult
		WITH THIS
			* close target cursor if it's open
			IF USED(tcCursorName)
				USE IN (tcCursorName)
			ENDIF
			WAIT WINDOW "Fetching remote data; please wait..." NOWAIT
			lnResult = SQLEXEC(.nSQLHandle, tcSQL, tcCursorName)
			llRetval = ( lnResult > 0 )
			IF llRetval THEN
				* see if any data came back
				IF NOT tlNoDataReturnedIsOkay THEN
					IF (NOT USED(tcCursorName)) OR EOF(tcCursorName) THEN
						llRetval = .F.
						.TrackProgress('No data was returned by this query: ' +CRLF + tcSQL, LOGIT+SENDIT)
					ENDIF
				ENDIF
			ELSE
				.TrackProgress("Result Code [" + TRANSFORM(lnResult) + "] returned by this query: " + tcSQL, LOGIT+SENDIT)
			ENDIF
			WAIT CLEAR
			RETURN llRetval
		ENDWITH
	ENDFUNC


	PROCEDURE TrackProgress
		* do any combination of Wait Window, writing to logfile, and adding to body of email,
		* based on nFlags parameter.
		LPARAMETERS tcExpression, tnFlags
		WITH THIS
			IF BITAND(tnFlags,LOGIT) = LOGIT THEN
				IF .lLoggingIsOn THEN
					?
					? .cProcessName + "  " + TTOC(DATETIME()) + ": " + tcExpression
				ENDIF
			ENDIF
			IF .lWaitWindowIsOn THEN
				IF BITAND(tnFlags,NOWAITIT) = NOWAITIT THEN
					WAIT WINDOW tcExpression NOWAIT
				ENDIF
				IF BITAND(tnFlags,WAITIT) = WAITIT THEN
					WAIT WINDOW tcExpression TIMEOUT .nWaitWindowTimeout
				ENDIF
			ENDIF
			IF BITAND(tnFlags,SENDIT) = SENDIT THEN
				IF .lSendInternalEmailIsOn THEN
					.cBodyText = .cBodyText + tcExpression + CRLF + CRLF
				ENDIF
			ENDIF
		ENDWITH
	ENDPROC  &&  TrackProgress

ENDDEFINE

