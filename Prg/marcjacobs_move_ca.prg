Do M:\DEV\PRG\_SETVARS With .T.
Public cfilename,cfileoutname,NormalExit
Set Step On

Try
	lTesting = .F.
	lOverrideBusy = .F.
	NormalExit = .F.
Set Step On
	If !lTesting
		
		If !Used("ftpsetup")
			Use F:\edirouting\ftpsetup Shared In 0
		Endif
SELECT ftpsetup
		cTransfer = "MARCJACOBS-MOVE_CA"
		Locate For ftpsetup.transfer = cTransfer
		If ftpsetup.chkbusy = .T. And !lOverrideBusy
			Wait Window "File is processing...exiting" Timeout 2
			NormalExit = .T.
			Throw
		Endif
		Replace chkbusy With .T. For ftpsetup.transfer = cTransfer
		Use In ftpsetup
	Endif
	cDirIn = "F:\FTPUSERS\MJ_Wholesale_CA\IN\"
	Cd &cDirIn
	cCustname = "MARC JACOBS"

	_Screen.WindowState=1
	_Screen.Caption="Marc Jacobs File Move Process"

	nFound = Adir(ary1,'*.*')
	If nFound = 0
		Close Databases All
		Wait Window "No NJ files found to process...exiting" Timeout 2
		NormalExit = .T.
		Throw
	Endif

	len1 = Alen(ary1,1)

	For i = 1 To len1
		Assert .F. Message "In move loop"
		cfilename = Allt(ary1[i,1])
		Wait Window "Processing "+cfilename Timeout 1

		xfile = (cDirIn+cfilename)
		cString = Filetostr(xfile)
		Set Step On
		Do Case
		Case "GS*FA"$cString  && 997 files
			cDirOut = ("F:\FTPUSERS\MJ_Wholesale_CA\WMS_Out\997in\")

		Case "GS*PO"$cString  && 943 files
			cDirOut = ("F:\FTPUSERS\MJ_Wholesale_CA\WMS_Inbound\943IN\")

		Case "GS*AR"$cString  && 943 files
			cDirOut = ("F:\FTPUSERS\MJ_Wholesale_CA\WMS_Inbound\943IN\")

		Case "GS*SC"$cString  && 832 Stylemaster files
			cDirOut = ("F:\FTPUSERS\MJ_Wholesale\WMS_Inbound\Stylemaster\")

		Case "GS*AN"$cString  && 180/RMA files
			cDirOut = ("F:\FTPUSERS\MJ_Wholesale_CA\WMS_Out\180IN\")

		Case "CMDTL_"$cfilename
			cDirOut = ("F:\FTPUSERS\MJ_Wholesale_CA\WMS_Inbound\credit_memo\")

		Otherwise  && 940 files
			cDirOut = ("F:\FTPUSERS\MJ_Wholesale_CA\WMS_Out\940IN\")
		Endcase
		Set Step On
		Wait Window "File will be moved to folder: "+cDirOut Nowait
		cfileoutname = (cDirOut+cfilename)
		Copy File &xfile To &cfileoutname

		Set Step On
		If File(cfileoutname)
			If File(xfile)
				Delete File [&xfile]
			Endif
		Endif
	Endfor

	Wait Window "Marc Jacobs CA file move process complete" Timeout 1
	Select 0
	If !Used("ftpsetup")
		Use F:\edirouting\ftpsetup Shared In 0
	Endif
	cTransfer = "MARCJACOBS-MOVE_CA"
	Replace chkbusy With .F. For ftpsetup.transfer = cTransfer
	Close Databases All
	Wait Clear
	NormalExit = .T.
Catch To oErr
	If !NormalExit
		Assert .F. Message "At CATCH..."
		Set Step On

		tsubject = cCustname+" File Move Error ("+Transform(oErr.ErrorNo)+") at "+Ttoc(Datetime())
		tattach  = ""
		Select 0
		Use F:\3pl\Data\mailmaster Alias mm
		Locate For mm.edi_type = "MISC" And mm.taskname = "MJPLERROR"
		tsendto = Iif(mm.use_alt,Trim(mm.sendtoalt),Trim(mm.sendto))
		tcc = Iif(mm.use_alt,Trim(mm.ccalt),Trim(mm.cc))
		Use In mm
		tmessage = cCustname+" File Move Error..... Please fix me........!"
		lcSourceMachine = Sys(0)
		lcSourceProgram = Sys(16)

		tmessage =tmessage+ "Try/CATCH Exeception Message:"+Chr(13)+;
			[  Message: ] + oErr.Message +Chr(13)+;
			[  Procedure: ] + oErr.Procedure +Chr(13)+;
			[  LineNo: ] + Str(oErr.Lineno) +Chr(13)+;
			[  Error: ] + Str(oErr.ErrorNo) +Chr(13)+;
			[  Details: ] + oErr.Details +Chr(13)+;
			[  StackLevel: ] + Str(oErr.StackLevel) +Chr(13)+;
			[  LineContents: ] + oErr.LineContents+Chr(13)+;
			[  UserValue: ] + oErr.UserValue+Chr(13)+;
			[  Computer:  ] +lcSourceMachine+Chr(13)+;
			[  940 file:  ] +xfile+Chr(13)+;
			[  Program:   ] +lcSourceProgram

		tattach  = ""
		tFrom ="TGF EDI Operations <toll-edi-ops@tollgroup.com>"
		Do Form M:\DEV\frm\dartmail2 With tsendto,tFrom,tsubject,tcc,tattach,tmessage,"A"
	Else
		Wait Window At 10,10 "Normal Exit " Timeout 2
	Endif
Finally
	Close Databases All
Endtry
