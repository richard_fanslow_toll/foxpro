* Create MTD report of Temp & Hourly Employee hours & REG/OT/DT $ by WORKSITE/DIVISION.
* For Kai. To be run daily.
* EXEs go in F:\UTIL\KRONOS\
LOCAL lnError, lcProcessName, loKRONOSMTDSUMMARYReport

DECLARE INTEGER CreateMutex IN Win32API ;
	STRING @lpMutexAttributes, ;
	STRING bInitialOwner, ;
	STRING @lpName

DECLARE INTEGER GetLastError IN Win32API

* if running in EXE check to see if already running
IF _VFP.STARTMODE > 0 THEN
	lcProcessName = "KRONOSMTDSUMMARY"
	CreateMutex(0,CHR(1),@lcProcessName)
	lnError = GetLastError()
	IF lnError = 183
		WAIT WINDOW TIMEOUT 10 "KRONOS MTD SUMMARY Report process is already running..."
		RETURN .F.
	ENDIF
ENDIF

*utilsetup("KRONOSMTDSUMMARY")

loKRONOSMTDSUMMARYReport = CREATEOBJECT('KRONOSMTDSUMMARYReport')
loKRONOSMTDSUMMARYReport.MAIN()

*schedupdate()

CLOSE DATABASES ALL
RETURN

#DEFINE LOGIT 1
#DEFINE WAITIT 2
#DEFINE NOWAITIT 4
#DEFINE SENDIT 8
#DEFINE CRLF CHR(13) + CHR(10)
#DEFINE LF CHR(10)
#DEFINE RETURN_DATA_MANDATORY .F.
#DEFINE RETURN_DATA_NOT_MANDATORY .T.
#DEFINE DAILY_OT_MULTIPLIER 1.5
#DEFINE DAILY_OT20_MULTIPLIER 2.0
#DEFINE xlEdgeBottom 9
#DEFINE xlContinuous 1
#DEFINE xlMedium -4138

DEFINE CLASS KRONOSMTDSUMMARYReport AS CUSTOM

	cProcessName = 'KRONOSMTDSUMMARYReport'

	lTestMode = .T.  && .F. FOR NORMAL OPERATION, .T. TO SUPPRESS ALL BUT DEVELOPER EMAILS AND CHANGE INPUT TABLE, ETC.

	lAutoYield = .T.

	cStartTime = TTOC(DATETIME())

	cMode = ''

	* connection properties
	nSQLHandle = 0

	* file properties
	nFileHandle = 0

	* filter properties
	cCompanyWhere = " AND (D.LABORLEV1NM IN ('SXI','TMP')) "
	*cCompanyWhere = " "

	cExtraWhere = ""
	*cExtraWhere = " AND ( (D.LABORLEV5NM = 'B') OR (D.LABORLEV4NM = 'CA4') ) "

	* date properties
	dtNow = DATETIME()

	dToday = DATE()
	* to force different pay weeks
	*dToday = {^2011-06-24}

	cToday = DTOC(DATE())

	* wait window properties
	nWaitWindowTimeout = 2
	lWaitWindowIsOn = .T.

	* logfile properties
	lLoggingIsOn = .T.
	cLogFile = 'F:\UTIL\KRONOS\Logfiles\KRONOSMTDSUMMARYReport_log.txt'

	* INTERNAL email properties
	lSendInternalEmailIsOn = .T.
	cFrom = 'FMI Corporate <fmicorporate@fmiint.com>'
	cSendTo = 'mbennett@fmiint.com'
	cCC = 'mbennett@fmiint.com'
	cSubject = 'KRONOS MTD SUMMARY Report'
	cAttach = ''
	cBodyText = ''
	cTopBodyText = ''

	cCOMPUTERNAME = ''
	cUSERNAME = ''

	FUNCTION INIT
		IF NOT DODEFAULT()
			RETURN .F.
		ENDIF
		WITH THIS
			CLEAR
			*SET RESOURCE OFF
			CLOSE DATA
			SET CENTURY ON
			SET DATE AMERICAN
			SET HOURS TO 24
			SET DECIMALS TO 2
			SET ANSI ON
			SET TALK OFF
			SET DELETED ON
			SET CONSOLE OFF
			SET EXCLUSIVE OFF
			SET SAFETY OFF
			SET EXACT OFF
			SET STATUS BAR ON
			SET SYSMENU OFF
			SET ENGINEBEHAVIOR 70
			_VFP.AUTOYIELD = .lAutoYield
			.cCOMPUTERNAME = GETENV("COMPUTERNAME")
			.cUSERNAME = GETENV("USERNAME")
			IF .lTestMode THEN
				.cSendTo = 'mbennett@fmiint.com'
				.cLogFile = 'F:\UTIL\KRONOS\Logfiles\KRONOSMTDSUMMARYReport_log_TESTMODE.txt'
			ENDIF
			.lLoggingIsOn = .lLoggingIsOn AND NOT EMPTY(.cLogFile)
		ENDWITH
	ENDFUNC


	FUNCTION DESTROY
		WITH THIS
			IF .lLoggingIsOn  THEN
				SET ALTERNATE OFF
				SET ALTERNATE TO
			ENDIF
		ENDWITH
		DODEFAULT()
	ENDFUNC


	FUNCTION MAIN
		LPARAMETERS tcMode
		WITH THIS
			LOCAL lnNumberOfErrors, lcSQL, lcSQLADP, lcSQLADP2, ldToday, ldFromDate, ldToDate, lcSQLFromDate, lcSQLToDate
			LOCAL lcOutputFile, lcMonthYear, ldADPFromDate, ldADPToDate, lnDay
			LOCAL lcSQL2, lcSQL3, lcSQL4, lcSQL5, loError, llValid
			LOCAL lcXLFileName, ldStartDate, ldEndDate, lcStartDate, lcEndDate, ldDate
			LOCAL lcSQLStartDate, lcSQLEndDate, lcXLFileName, ldYesterday, lnTotalRow
			LOCAL oExcel, oWorkbook, oWorksheet, lnRow, lcRow, lnStartRow, lcStartRow, lnEndRow, lnPP, lcPP, lnNN, lcNN, lcJJ, lcEndRow
			LOCAL lcColsToProcess, lnColumn, lcBoldColumn, i, j, lcBoldRow, luCellValue, lcTargetDirectory, ldPayDate, lcFiletoSaveAs
			LOCAL lcFileDate, lcDivision, lcTitle, llSaveAgain, lcPayCode, lcSpecialWhere
			LOCAL lcSQLToday, lcSQLTomorrow, ldToday, ldTomorrow, lcExcluded9999s, lcFoxProTimeReviewerTable, lcMissingApprovals
			LOCAL lcAuditSubject, oWorkbookAUDIT, oWorksheetAUDIT, lcSendTo, lcCC, lcBodyText, lcXLFileNameAUDIT, lcTotalRow
			LOCAL lcTRWhere, lnStraightHours, lnStraightDollars, lnOTDollars, lcFilePrefix
			LOCAL ldPass2Date, lcSpreadsheetTemplate, lnMarkup
			LOCAL lcOrderBy, lnTempMarkup, ldCurrentDate, lcSQLStartDate, lcSQLEndDate
			LOCAL lnShiftDiff, lcXLSummaryFileName, lnWage, lnOTPay, lnRegPay, lcRootPDFFilename
			LOCAL ltInpunch, lnTotHours, lcADPFromDate, lcADPToDate, lcADPDivWhere
			LOCAL lnSQLHandle, lcSQLADP, lnMultiplier, lcFinalFilename

			TRY
				lnNumberOfErrors = 0

				IF .lLoggingIsOn THEN
					SET ALTERNATE TO (.cLogFile) ADDITIVE
					SET ALTERNATE ON
				ENDIF

				.TrackProgress('KRONOS MTD SUMMARY Report process started....', LOGIT+SENDIT+NOWAITIT)
				.TrackProgress('cExtraWhere = ' + .cExtraWhere, LOGIT+SENDIT)				
				.TrackProgress('cCompanyWhere = ' + .cCompanyWhere, LOGIT+SENDIT)
				.TrackProgress('COMPUTERNAME = ' + .cCOMPUTERNAME, LOGIT+SENDIT)
				.TrackProgress('USERNAME = ' + .cUSERNAME, LOGIT+SENDIT)
				
				.TrackProgress('PROJECT = KRONOSMTDSUMMARY', LOGIT+SENDIT)
				IF .lTestMode THEN
					.TrackProgress('                        TEST MODE                        ', LOGIT+SENDIT)
				ENDIF

				IF USED('CURADP') THEN
					USE IN CURADP
				ENDIF

				* get ADP hourly rates (for hourly employees)
				.nSQLHandle = SQLCONNECT("PCPAYWIN","MBENNETT","VP737A")

				IF .nSQLHandle > 0 THEN

					SET TEXTMERGE ON
					TEXT TO	lcSQLADP NOSHOW
SELECT
FILE# AS FILE_NUM,
{fn ifnull(MAX(rate1amt),0.00)} AS RATE
FROM REPORTS.V_EMPLOYEE
WHERE COMPANYCODE = 'E87'
GROUP BY FILE#
					ENDTEXT
					SET TEXTMERGE OFF

					.TrackProgress('lcSQLADP =' + lcSQLADP, LOGIT+SENDIT)

					IF .ExecSQL(lcSQLADP, 'CURADP', RETURN_DATA_MANDATORY) THEN

						*!*						SELECT CURADP
						*!*						BROWSE

						=SQLDISCONNECT(.nSQLHandle)

					ENDIF  &&  .ExecSQL(lcSQLADP, 'CURADP', RETURN_DATA_MANDATORY)

				ELSE
					* connection error
					.TrackProgress('Unable to connect to ADP Payroll System.', LOGIT+SENDIT)
					THROW
				ENDIF   &&  .nSQLHandle > 0

				ldToday = .dToday

				lnDay = DAY(ldToday)

				IF lnDay = 1 THEN
					* RUN FOR PRIOR MONTH
					ldFromDate = GOMONTH(ldToday,-1)
					ldToDate = ldToday - lnDay  && this is last day of prior month
				ELSE
					* run for 1st day of month thru yesterday
					ldFromDate = ldToday - lnDay  && this is last day of prior month
					ldFromDate = ldFromDate + 1  && this is the first day of current month
					ldToDate = ldToday - 1  && this is yesterday
				ENDIF
				
	* for special date-range, activate this block
	IF .lTestMode THEN
		ldFromDate = {^2011-05-14}
		ldToDate = {^2011-06-25}
	ENDIF

				*!*					ldADPToDate = ldToDate
				*!*					ldADPFromDate = ldFromDate

				lcMonthYear = TRANSFORM(ldFromDate) + "-" + TRANSFORM(ldToDate)

				.cSubject = 'KRONOS MTD SUMMARY Report for: ' + lcMonthYear
				lcOutputFileSummary = "F:\UTIL\KRONOS\TEMPTIMEREPORTS\KRONOSMTDSUMMARY_" + STRTRAN(lcMonthYear,"/","") + ".XLS"

				lcSQLFromDate = .GetSQLStartDateTimeString( ldFromDate )
				lcSQLToDate = .GetSQLEndDateTimeString( ldToDate )

				*!*				lcADPFromDate = "DATE'" + TRANSFORM(YEAR(ldADPFromDate)) + "-" + PADL(MONTH(ldADPFromDate),2,'0') + "-" + PADL(DAY(ldADPFromDate),2,'0') + "'"
				*!*				lcADPToDate = "DATE'" + TRANSFORM(YEAR(ldADPToDate)) + "-" + PADL(MONTH(ldADPToDate),2,'0') + "-" + PADL(DAY(ldADPToDate),2,'0') + "'"

				IF USED('CURWTKHOURS') THEN
					USE IN CURWTKHOURS
				ENDIF
				IF USED('CURWTKHOURSPRE') THEN
					USE IN CURWTKHOURSPRE
				ENDIF
				IF USED('CURFINAL') THEN
					USE IN CURFINAL
				ENDIF
				IF USED('CURFINALBYDIV') THEN
					USE IN CURFINALBYDIV
				ENDIF

				*!*					" D.LABORLEV3NM AS DEPT, " + ;
				*!*					" D.LABORLEV3DSC AS DEPTNAME, " + ;
				*!*					" C.SHORTNM AS SHORTNAME, " + ;

				* main SQL
				lcSQL = ;
					" SELECT " + ;
					" A.APPLYDTM, " + ;
					" D.LABORLEV1NM AS ADP_COMP, " + ;
					" D.LABORLEV2NM AS DIVISION, " + ;
					" D.LABORLEV2DSC AS DIVNAME, " + ;
					" D.LABORLEV3NM AS DEPT, " + ;
					" D.LABORLEV3DSC AS DEPTNAME, " + ;
					" D.LABORLEV4NM AS WORKSITE, " + ;
					" D.LABORLEV4DSC AS WKSITENAME, " + ;
					" D.LABORLEV5NM AS AGENCYNUM, " + ;
					" D.LABORLEV5DSC AS AGENCY, " + ;
					" C.FULLNM AS EMPLOYEE, " + ;
					" C.PERSONNUM AS FILE_NUM, " + ;
					" C.PERSONID, " + ;
					" A.EMPLOYEEID, " + ;
					" E.ABBREVIATIONCHAR AS PAYCODE, " + ;
					" E.NAME AS PAYCODEDESC, " + ;
					" (A.DURATIONSECSQTY / 3600.00) AS HOURS, " + ;
					" {fn ifnull(A.MONEYAMT,0000.00)} AS MONEY, " + ;
					" 0000.00 AS BASEWAGE, " + ;
					" 0000.00 AS WAGE, " + ;
					" 000000.00 AS REGPAY, " + ;
					" 000000.00 AS OTPAY, " + ;
					" 000000.00 AS DTPAY, " + ;
					" 000000.00 AS TOTPAY " + ;
					" FROM WFCTOTAL A " + ;
					" JOIN WTKEMPLOYEE B " + ;
					" ON B.EMPLOYEEID = A.EMPLOYEEID " + ;
					" JOIN PERSON C " + ;
					" ON C.PERSONID = B.PERSONID " + ;
					" JOIN LABORACCT D " + ;
					" ON D.LABORACCTID = A.LABORACCTID " + ;
					" JOIN PAYCODE E " + ;
					" ON E.PAYCODEID = A.PAYCODEID " + ;
					" WHERE (A.APPLYDTM >= " + lcSQLFromDate + ")" + ;
					" AND (A.APPLYDTM <= " + lcSQLToDate + ")" + ;					
					.cCompanyWhere + ;
					.cExtraWhere


				IF .lTestMode THEN
					.TrackProgress('', LOGIT+SENDIT)
					.TrackProgress('lcSQL =' + lcSQL, LOGIT+SENDIT)
					.TrackProgress('', LOGIT+SENDIT)
					.TrackProgress('ldToDate =' + TRANSFORM(ldToDate), LOGIT+SENDIT)
					.TrackProgress('ldFromDate =' + TRANSFORM(ldFromDate), LOGIT+SENDIT)
				ENDIF

				.nSQLHandle = SQLCONNECT("WTK2","sa","kr0n1tes")

				IF .nSQLHandle > 0 THEN

					IF .ExecSQL(lcSQL, 'CURWTKHOURSPRE', RETURN_DATA_MANDATORY) THEN

						* add a foxpro date field to the cursor
						SELECT TTOD(APPLYDTM) AS WORKDATE, * ;
							FROM CURWTKHOURSPRE ;
							INTO CURSOR CURWTKHOURS ;
							ORDER BY APPLYDTM, ADP_COMP, WORKSITE, DIVISION, DEPT, AGENCYNUM, EMPLOYEE, PAYCODE ;
							READWRITE

						* populate Temp base wages (which are in Kronos and are date-sensitive)
						SET DATE YMD

						ldCurrentDate = ldFromDate

						DO WHILE ldCurrentDate <= ldToDate

							*	 create "YYYYMMDD" safe date format for use in SQL query
							lcSQLStartDate = STRTRAN(DTOC(ldCurrentDate),"/","")

							lcSQLWAGES = ;
								" SELECT " + ;
								" EMPLOYEEID, " + ;
								" BASEWAGEHOURLYAMT AS WAGE, " + ;
								" EFFECTIVEDTM, " + ;
								" EXPIRATIONDTM " + ;
								" FROM BASEWAGERTHIST " + ;
								" WHERE '" + lcSQLStartDate + "' BETWEEN EFFECTIVEDTM AND EXPIRATIONDTM " + ;
								" ORDER BY EMPLOYEEID DESC, EXPIRATIONDTM DESC "

							IF USED('CURWAGES') THEN
								USE IN CURWAGES
							ENDIF

							IF .ExecSQL(lcSQLWAGES, 'CURWAGES', RETURN_DATA_MANDATORY) THEN

								SELECT CURWTKHOURS
								*SCAN FOR (WORKDATE = ldCurrentDate) AND INLIST(ALLTRIM(AGENCYNUM),"A","B")
								SCAN FOR (WORKDATE = ldCurrentDate) AND (UPPER(ALLTRIM(ADP_COMP)) == "TMP")

									* use Temp agency markup to create adjusted wage
									DO CASE
										CASE ALLTRIM(CURWTKHOURS.AGENCYNUM) = 'A'
											lnMarkup = 1.28
										CASE ALLTRIM(CURWTKHOURS.AGENCYNUM) = 'B'
											lnMarkup = 1.275
										OTHERWISE
											lnMarkup = 1.00
									ENDCASE

									* populate WAGE fields in CURWTKHOURSTEMP
									SELECT CURWAGES
									LOCATE FOR EMPLOYEEID = CURWTKHOURS.EMPLOYEEID
									IF FOUND() THEN
										REPLACE CURWTKHOURS.BASEWAGE WITH CURWAGES.WAGE, CURWTKHOURS.WAGE WITH ( CURWAGES.WAGE * lnMarkup )
									ELSE
										REPLACE CURWTKHOURS.BASEWAGE WITH 8.00
									ENDIF
								ENDSCAN
							ELSE
								REPLACE CURWTKHOURS.BASEWAGE WITH 8.00
							ENDIF  && .ExecSQL(lcSQLWAGES

							ldCurrentDate = ldCurrentDate + 1

						ENDDO

						SET DATE AMERICAN


						* Populate non-temp hourly wage
						SELECT CURWTKHOURS
						*SCAN FOR (NOT INLIST(ALLTRIM(AGENCYNUM),"A","B"))
						SCAN FOR (UPPER(ALLTRIM(ADP_COMP)) == "SXI")
							SELECT CURADP
							LOCATE FOR FILE_NUM = VAL(CURWTKHOURS.FILE_NUM)
							IF FOUND() THEN
								REPLACE CURWTKHOURS.BASEWAGE WITH CURADP.RATE, CURWTKHOURS.WAGE WITH CURADP.RATE
							ENDIF
						ENDSCAN

						* populate final Pay based on wage and paycode

						SELECT CURWTKHOURS
						SCAN
							lcPayCode = UPPER(ALLTRIM(CURWTKHOURS.PAYCODE))
							DO CASE
								CASE lcPayCode == "OTM"
									* OVERTIME
									lnMultiplier = 1.50
									REPLACE CURWTKHOURS.OTPAY WITH ( CURWTKHOURS.HOURS * CURWTKHOURS.WAGE * lnMultiplier )
								CASE lcPayCode == "DTM"
									* DOUBLE-TIME
									lnMultiplier = 2.00
									REPLACE CURWTKHOURS.DTPAY WITH ( CURWTKHOURS.HOURS * CURWTKHOURS.WAGE * lnMultiplier )
								CASE INLIST(lcPayCode,"FML","ULV","UPH")
									* THESE ARE THE MAIN UNPAID PAY CODES: FMLA, UNPAID LEAVE, UNPAID HOURS
									lnMultiplier = 0.00
									* no replace needed
								OTHERWISE
									* ASSUME STRAIGHT TIME SUCH AS REG HRS, SICK, VAC
									lnMultiplier = 1.00
									REPLACE CURWTKHOURS.REGPAY WITH ( CURWTKHOURS.HOURS * CURWTKHOURS.WAGE * lnMultiplier )
							ENDCASE

							*REPLACE CURWTKHOURS.PAY WITH ( CURWTKHOURS.HOURS * CURWTKHOURS.WAGE * lnMultiplier ) + CURWTKHOURS.MONEY

						ENDSCAN

IF .lTestMode THEN
						SELECT CURWTKHOURS
						COPY TO C:\A\CURWTKHOURS.XLS XL5 FOR INLIST(WORKDATE,{^2011-05-14},{^2011-05-21},{^2011-06-18},{^2011-06-25})
ENDIF

*!*	IF .lTestMode THEN
*!*		SELECT CURWTKHOURS
*!*		COPY TO C:\A\KMTD_OT20.XLS XL5 FOR UPPER(ALLTRIM(CURWTKHOURS.PAYCODE))== "DTM" AND adp_comp = "TMP"
*!*	ENDIF
						
						SELECT ;
							WKSITENAME, ;
							WORKDATE, ;
							ADP_COMP, ;
							DEPTNAME, ;
							DIVISION, ;
							DIVNAME, ;
							SUM(MONEY) AS MONEY, ;
							SUM(REGPAY) AS REGPAY, ;
							SUM(OTPAY) AS OTPAY, ;
							SUM(DTPAY) AS DTPAY, ;
							SUM(REGPAY + OTPAY + DTPAY + MONEY) AS TOTPAY ;
							FROM CURWTKHOURS ;
							INTO CURSOR CURFINAL ;
							GROUP BY 1, 2, 3, 4, 5, 6 ;
							ORDER BY 1, 2, 3, 4, 5
							
						lcFinalFilename = "C:\A\DASHDATA_MTD_" + DTOS(ldToday) + ".XLS"
						
						SELECT CURFINAL
						COPY TO (lcFinalFilename) XL5

						SELECT ;
							DIVISION, ;
							DIVNAME, ;
							SUM(MONEY) AS MONEY, ;
							SUM(REGPAY) AS REGPAY, ;
							SUM(OTPAY) AS OTPAY, ;
							SUM(DTPAY) AS DTPAY, ;
							SUM(REGPAY + OTPAY + DTPAY + MONEY) AS TOTPAY ;
							FROM CURWTKHOURS ;
							INTO CURSOR CURFINALBYDIV ;
							GROUP BY 1, 2 ;
							ORDER BY 1
							
						lcFinalFilename = "C:\A\DASHDATA_BYDIV_MTD_" + DTOS(ldToday) + ".XLS"
						
						SELECT CURFINALBYDIV
						COPY TO (lcFinalFilename) XL5

*!*							***********************************************************************************
*!*							** output to Excel spreadsheet
*!*							* deleet output file if it already exists...
*!*							IF FILE(lcOutputFileSummary) THEN
*!*								DELETE FILE (lcOutputFileSummary)
*!*							ENDIF

*!*							.TrackProgress('Creating spreadsheet: ' + lcOutputFileSummary, LOGIT+SENDIT+NOWAITIT)
*!*							lcSpreadsheetTemplate = 'F:\UTIL\KRONOS\TEMPLATES\KRONOSMTDSUMMARY.XLS'

*!*							oExcel = CREATEOBJECT("excel.application")
*!*							oExcel.displayalerts = .F.
*!*							oExcel.VISIBLE = .F.
*!*							oWorkbook = oExcel.workbooks.OPEN(lcSpreadsheetTemplate)
*!*							oWorkbook.SAVEAS(lcOutputFileSummary)

*!*							oWorksheet = oWorkbook.Worksheets[1]
*!*							oWorksheet.RANGE("A2","I100").ClearContents()

*!*							lnRow = 1

*!*							SELECT CURFINAL
*!*							SCAN
*!*								lnRow = lnRow + 1
*!*								lnEndRow = lnRow
*!*								lcRow = ALLTRIM(STR(lnRow))
*!*								oWorksheet.RANGE("A"+lcRow).VALUE = CURFINAL.PERIOD
*!*								oWorksheet.RANGE("B"+lcRow).VALUE = CURFINAL.TYPE
*!*								oWorksheet.RANGE("C"+lcRow).VALUE = CURFINAL.DIVNAME
*!*								oWorksheet.RANGE("D"+lcRow).VALUE = "'" + CURFINAL.DIVISION
*!*								oWorksheet.RANGE("E"+lcRow).VALUE = CURFINAL.REG_HRS
*!*								oWorksheet.RANGE("F"+lcRow).VALUE = CURFINAL.OT_HRS
*!*								oWorksheet.RANGE("G"+lcRow).VALUE = CURFINAL.OT20_HRS
*!*								oWorksheet.RANGE("H"+lcRow).VALUE = CURFINAL.TOT_HRS
*!*								oWorksheet.RANGE("I"+lcRow).VALUE = CURFINAL.TOT_REGPAY
*!*								oWorksheet.RANGE("J"+lcRow).VALUE = CURFINAL.TOT_OTPAY
*!*								oWorksheet.RANGE("K"+lcRow).VALUE = CURFINAL.TOT_ALLPAY
*!*								oWorksheet.RANGE("L"+lcRow).VALUE = "=(J" + lcRow + "/K" + lcRow + ")"
*!*							ENDSCAN


*!*							* populate header counts, sums, etc.
*!*							lnTotalRow = lnEndRow + 2
*!*							lcEndRow = ALLTRIM(STR(lnEndRow))
*!*							lcTotalRow = ALLTRIM(STR(lnTotalRow))

*!*							* sum $ columns
*!*							oWorksheet.RANGE("A" + lcTotalRow).VALUE = "Totals:"
*!*							oWorksheet.RANGE("E" + lcTotalRow).VALUE = "=SUM(E2:E" + lcEndRow + ")"
*!*							oWorksheet.RANGE("F" + lcTotalRow).VALUE = "=SUM(F2:F" + lcEndRow + ")"
*!*							oWorksheet.RANGE("G" + lcTotalRow).VALUE = "=SUM(G2:G" + lcEndRow + ")"
*!*							oWorksheet.RANGE("H" + lcTotalRow).VALUE = "=SUM(H2:H" + lcEndRow + ")"
*!*							oWorksheet.RANGE("I" + lcTotalRow).VALUE = "=SUM(I2:I" + lcEndRow + ")"
*!*							oWorksheet.RANGE("J" + lcTotalRow).VALUE = "=SUM(J2:J" + lcEndRow + ")"
*!*							oWorksheet.RANGE("K" + lcTotalRow).VALUE = "=SUM(K2:K" + lcEndRow + ")"

*!*							oWorksheet.RANGE("L" + lcTotalRow).VALUE = "=(J" + lcTotalRow + "/K" + lcTotalRow + ")"

*!*							* UNDERLINE cell columns to be totaled...
*!*							oWorksheet.RANGE("F" + lcEndRow + ":K" + lcEndRow).BORDERS(xlEdgeBottom).LineStyle = xlContinuous
*!*							oWorksheet.RANGE("F" + lcEndRow + ":K" + lcEndRow).BORDERS(xlEdgeBottom).Weight = xlMedium

*!*							* SAVE AND QUIT EXCEL
*!*							oWorkbook.SAVE()
*!*							oExcel.QUIT()
*!*							**************************************************************

*!*							IF FILE(lcOutputFileSummary) THEN
*!*								.TrackProgress('Output to Summary spreadsheet: ' + lcOutputFileSummary, LOGIT+SENDIT)
*!*								* attach output file to email
*!*								.cAttach = lcOutputFileSummary
*!*								.cBodyText = "See attached report. NOTE: #s are subject to change, pending final time card reviews." + ;
*!*									CRLF + CRLF + "(do not reply - this is an automated report)" + ;
*!*									CRLF + CRLF + "<report log follows>" + ;
*!*									CRLF + .cBodyText
*!*							ELSE
*!*								.TrackProgress('ERROR creating Summary spreadsheet: ' + lcOutputFileSummary, LOGIT+SENDIT)
*!*							ENDIF

					ENDIF  &&  .ExecSQL(lcSQL, 'CURWTKHOURSPRE', RETURN_DATA_MANDATORY)

					.TrackProgress('KRONOS MTD SUMMARY Report process finished normally', LOGIT+SENDIT)



				ELSE
					* connection error
					.TrackProgress('Unable to connect to WTK System.', LOGIT+SENDIT)
				ENDIF   &&  .nSQLHandle > 0

			CATCH TO loError

				.TrackProgress('There was an error.',LOGIT+SENDIT)
				.TrackProgress('The Error # is: ' + TRANSFORM(loError.ERRORNO), LOGIT+SENDIT)
				.TrackProgress('The Error Message is: ' + TRANSFORM(loError.MESSAGE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred in Program: ' + TRANSFORM(loError.PROCEDURE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred at line #: ' + TRANSFORM(loError.LINENO), LOGIT+SENDIT)
				lnNumberOfErrors = lnNumberOfErrors + 1
				IF TYPE('oExcel') = 'O' AND NOT ISNULL(oExcel) THEN
					oExcel.QUIT()
				ENDIF
				CLOSE DATA

			ENDTRY

			CLOSE DATA
			WAIT CLEAR
			***************** INTERNAL email results ******************************
			.TrackProgress('About to send status email.',LOGIT)
			.TrackProgress('==================================================================================================================', SENDIT)
			.TrackProgress('The logfile is: ' + .cLogFile, SENDIT)
			.TrackProgress('KRONOS MTD SUMMARY Report process started: ' + .cStartTime, LOGIT+SENDIT)
			.TrackProgress('KRONOS MTD SUMMARY Report process finished: ' + TTOC(DATETIME()), LOGIT+SENDIT)

			IF .lSendInternalEmailIsOn THEN
				* try to trap error from not having dartmail dll's registered on user's pc...
				TRY
					.cBodyText = .cTopBodyText + CRLF + .cBodyText

					DO FORM dartmail2 WITH .cSendTo,.cFrom,.cSubject,.cCC,.cAttach,.cBodyText,"A"
					.TrackProgress('Sent status email.',LOGIT)
				CATCH TO loError
					*=MESSAGEBOX('There was an error sending the status email. Please contact Mark Bennett.',0+64,"KRONOS Temp Time")
					.TrackProgress('There was an error sending the status email.',LOGIT)
					.TrackProgress('The Error # is: ' + TRANSFORM(loError.ERRORNO), LOGIT)
					.TrackProgress('The Error Message is: ' + TRANSFORM(loError.MESSAGE), LOGIT)
					.TrackProgress('The Error occurred in Program: ' + TRANSFORM(loError.PROCEDURE), LOGIT)
					.TrackProgress('The Error occurred at line #: ' + TRANSFORM(loError.LINENO), LOGIT)
					lnNumberOfErrors = lnNumberOfErrors + 1
					CLOSE DATA
				ENDTRY

			ELSE
				.TrackProgress('Did not send status email: THIS.lSendInternalEmailIsOn = FALSE.',LOGIT)
			ENDIF

		ENDWITH
		RETURN
	ENDFUNC && main


	FUNCTION GetSQLStartDateTimeString
		* returns a timestamp string for use in SQL queries
		LPARAMETERS tdDate
		LOCAL lcSetDate, lcSQLDateTimeString
		lcSetDate = SET('DATE')
		SET DATE YMD
		lcSQLDateTimeString   = "{ts '" + TRANSFORM(YEAR(tdDate)) + "-" + PADL(MONTH(tdDate),2,"0") + "-" + PADL(DAY(tdDate),2,"0") + " 00:00:00.0'}"
		SET DATE &lcSetDate.
		RETURN lcSQLDateTimeString
	ENDFUNC


	FUNCTION GetSQLEndDateTimeString
		* returns a timestamp string for use in SQL queries
		LPARAMETERS tdDate
		LOCAL lcSetDate, lcSQLDateTimeString
		lcSetDate = SET('DATE')
		SET DATE YMD
		lcSQLDateTimeString = "{ts '" + TRANSFORM(YEAR(tdDate)) + "-" + PADL(MONTH(tdDate),2,"0") + "-" + PADL(DAY(tdDate),2,"0") + " 23:59:59.9'}"
		SET DATE &lcSetDate.
		RETURN lcSQLDateTimeString
	ENDFUNC


	FUNCTION ExecSQL
		LPARAMETERS tcSQL, tcCursorName, tlNoDataReturnedIsOkay
		LOCAL llRetval, lnResult
		WITH THIS
			* close target cursor if it's open
			IF USED(tcCursorName)
				USE IN (tcCursorName)
			ENDIF
			WAIT WINDOW "Fetching remote data; please wait..." NOWAIT
			lnResult = SQLEXEC(.nSQLHandle, tcSQL, tcCursorName)
			llRetval = ( lnResult > 0 )
			IF llRetval THEN
				* see if any data came back
				IF NOT tlNoDataReturnedIsOkay THEN
					IF (NOT USED(tcCursorName)) OR EOF(tcCursorName) THEN
						llRetval = .F.
						.TrackProgress('No data was returned by this query: ' +CRLF + tcSQL, LOGIT+SENDIT)
					ENDIF
				ENDIF
			ELSE
				.TrackProgress("Result Code [" + TRANSFORM(lnResult) + "] returned by this query: " + tcSQL, LOGIT+SENDIT)
			ENDIF
			WAIT CLEAR
			RETURN llRetval
		ENDWITH
	ENDFUNC


	PROCEDURE TrackProgress
		* do any combination of Wait Window, writing to logfile, and adding to body of email,
		* based on nFlags parameter.
		LPARAMETERS tcExpression, tnFlags
		WITH THIS
			IF BITAND(tnFlags,LOGIT) = LOGIT THEN
				IF .lLoggingIsOn THEN
					?
					? .cProcessName + "  " + TTOC(DATETIME()) + ": " + tcExpression
				ENDIF
			ENDIF
			IF .lWaitWindowIsOn THEN
				IF BITAND(tnFlags,NOWAITIT) = NOWAITIT THEN
					WAIT WINDOW tcExpression NOWAIT
				ENDIF
				IF BITAND(tnFlags,WAITIT) = WAITIT THEN
					WAIT WINDOW tcExpression TIMEOUT .nWaitWindowTimeout
				ENDIF
			ENDIF
			IF BITAND(tnFlags,SENDIT) = SENDIT THEN
				IF .lSendInternalEmailIsOn THEN
					.cBodyText = .cBodyText + tcExpression + CRLF + CRLF
				ENDIF
			ENDIF
		ENDWITH
	ENDPROC  &&  TrackProgress


*!*		FUNCTION GetCalculatedShift
*!*			LPARAMETERS tcTempCo, tInpunchDTM
*!*			LOCAL lcShift, lcTimePortion
*!*			lcShift = "?"
*!*			lcTimePortion = RIGHT(TTOC(tInpunchDTM),8)
*!*			tcTempCo = UPPER(ALLTRIM(tcTempCo))
*!*			WITH THIS
*!*				DO CASE
*!*					CASE tcTempCo = 'A'
*!*						DO CASE
*!*							CASE lcTimePortion < "13:00:00"
*!*								lcShift = "1"
*!*							CASE lcTimePortion < "20:00:00"
*!*								lcShift = "2"
*!*							OTHERWISE
*!*								lcShift = "3"
*!*						ENDCASE
*!*					CASE INLIST(tcTempCo,'B','BJ','BS')
*!*						DO CASE
*!*							CASE lcTimePortion < "12:00:00"
*!*								lcShift = "1"
*!*							OTHERWISE
*!*								lcShift = "2"
*!*						ENDCASE
*!*					OTHERWISE
*!*						* nothing
*!*				ENDCASE
*!*			ENDWITH
*!*			RETURN lcShift
*!*		ENDFUNC



	*!*		FUNCTION getTempAgencyMarkup
	*!*		LPARAMETERS tcTempCo, tcSHORTNAME
	*!*		LOCAL lnMarkup
	*!*		tcTempCo = UPPER(ALLTRIM(tcTempCo))
	*!*		DO CASE
	*!*		CASE tcTempCo = 'A'
	*!*			* Tri-State gets 28% markup unless temp is a rollover, which is 27.5% per Neftali 11/16/09
	*!*			* Rollover will be indicated indicated by "ROLL" in shortname field.
	*!*			DO CASE
	*!*			CASE UPPER(ALLTRIM(tcSHORTNAME)) == "ROLL"
	*!*				lnMarkup = 1.275
	*!*			OTHERWISE
	*!*				* normal case
	*!*				lnMarkup = 1.28
	*!*			ENDCASE
	*!*		CASE INLIST(tcTempCo,'B','BJ','BS')
	*!*			* Select indicated a flat 27.5% with no variations.
	*!*			lnMarkup = 1.275
	*!*		OTHERWISE
	*!*			* another temp co
	*!*			lnMarkup = 1.00
	*!*		ENDCASE
	*!*		RETURN lnMarkup
	*!*		ENDFUNC


	*!*		FUNCTION getShiftDiff
	*!*		LPARAMETERS tcTempCo, tcCalcdShift, tcDept, tnBaseWage
	*!*		LOCAL lnShiftDiff, lcDept, lcCalcdShift
	*!*		tcTempCo = UPPER(ALLTRIM(tcTempCo))
	*!*		lnShiftDiff = 0.00
	*!*		lcDept = ALLTRIM( tcDept )
	*!*		lcCalcdShift = ALLTRIM( tcCalcdShift )
	*!*		DO CASE
	*!*		CASE tcTempCo = 'A'
	*!*			* Tri-State rules, updated per Neftali for base wage check 10/27/09 MB.
	*!*			* basic rules for shift differential:
	*!*			* if lcDept = '0622' (forklift drivers) or CalcdShift = '1' or base wage > 8.00 then no differential
	*!*			* otherwise, give differential.
	*!*			DO CASE
	*!*			CASE (lcDept = '0622') OR (lcCalcdShift = '1') OR (tnBaseWage > 8.00)
	*!*				lnShiftDiff = 0.00
	*!*			OTHERWISE
	*!*				lnShiftDiff = 0.20
	*!*			ENDCASE
	*!*		CASE INLIST(tcTempCo,'B','BJ','BS')
	*!*			* Select rules, per Alex Figueroa and Juan Santisteban 2/10/10 MB.
	*!*			* basic rules for shift differential:
	*!*			* simply, if temp works 2nd shift, they get $0.20 extra
	*!*			DO CASE
	*!*			CASE (lcCalcdShift = '1')
	*!*				lnShiftDiff = 0.00
	*!*			OTHERWISE
	*!*				lnShiftDiff = 0.20
	*!*			ENDCASE
	*!*		OTHERWISE
	*!*			lnShiftDiff = 0.00
	*!*		ENDCASE
	*!*		RETURN lnShiftDiff
	*!*		ENDFUNC


	*!*		PROCEDURE BuildDailyCursor
	*!*		LPARAMETERS tdPass2Date
	*!*		WITH THIS
	*!*			LOCAL lcSQLStartDate, lcSQLEndDate, lcSQLPass2, lcSQLPass2A

	*!*			SET DATE YMD

	*!*			*	 create "YYYYMMDD" safe date format for use in SQL query
	*!*			* NOTE: for SQL we are going to use < EndDate ( not <= ), so we are going to add 1 to that date here for querying purposes.
	*!*			lcSQLStartDate = STRTRAN(DTOC(tdPass2Date),"/","")
	*!*			lcSQLEndDate = STRTRAN(DTOC(tdPass2Date + 1),"/","")

	*!*			SET DATE AMERICAN

	*!*			IF USED('CURWTKHOURSTEMPPRE') THEN
	*!*				USE IN CURWTKHOURSTEMPPRE
	*!*			ENDIF
	*!*			IF USED('CURWTKHOURSTEMP') THEN
	*!*				USE IN CURWTKHOURSTEMP
	*!*			ENDIF
	*!*			IF USED('CURWAGES') THEN
	*!*				USE IN CURWAGES
	*!*			ENDIF

	*!*			* pass 2 SQL
	*!*			* the join to basewagehistory table was causing duplicate records; had to get wage from separate sql
	*!*			* 9/23/09 MB

	*!*			* pass 2 SQL
	*!*			lcSQLPass2 = ;
	*!*				" SELECT " + ;
	*!*				" '2' AS CPASS, " + ;
	*!*				" D.LABORLEV5NM AS AGENCYNUM, " + ;
	*!*				" D.LABORLEV5DSC AS AGENCY, " + ;
	*!*				" D.LABORLEV2NM AS DIVISION, " + ;
	*!*				" D.LABORLEV3NM AS DEPT, " + ;
	*!*				" D.LABORLEV2DSC AS DIVNAME, " + ;
	*!*				" D.LABORLEV3DSC AS DEPTNAME, " + ;
	*!*				" D.LABORLEV6NM AS SHIFT, " + ;
	*!*				" D.LABORLEV7NM AS TIMERVWR, " + ;
	*!*				" C.FULLNM AS EMPLOYEE, " + ;
	*!*				" C.SHORTNM AS SHORTNAME, " + ;
	*!*				" C.PERSONNUM AS FILE_NUM, " + ;
	*!*				" C.PERSONID, " + ;
	*!*				" A.EMPLOYEEID, " + ;
	*!*				" E.ABBREVIATIONCHAR AS PAYCODE, " + ;
	*!*				" E.NAME AS PAYCODEDESC, " + ;
	*!*				" (SUM(A.DURATIONSECSQTY) / 3600.00) AS TOTHOURS, " + ;
	*!*				" SUM(A.MONEYAMT) AS TOTPAY, " + ;
	*!*				" 0000.00 AS WAGE " + ;
	*!*				" FROM WFCTOTAL A " + ;
	*!*				" JOIN WTKEMPLOYEE B " + ;
	*!*				" ON B.EMPLOYEEID = A.EMPLOYEEID " + ;
	*!*				" JOIN PERSON C " + ;
	*!*				" ON C.PERSONID = B.PERSONID " + ;
	*!*				" JOIN LABORACCT D " + ;
	*!*				" ON D.LABORACCTID = A.LABORACCTID " + ;
	*!*				" JOIN PAYCODE E " + ;
	*!*				" ON E.PAYCODEID = A.PAYCODEID " + ;
	*!*				" WHERE (A.APPLYDTM >= '" + lcSQLStartDate + "')" + ;
	*!*				" AND (A.APPLYDTM < '" + lcSQLEndDate + "')" + ;
	*!*				" AND (D.LABORLEV1NM = 'TMP') " + ;
	*!*				.cDivWhere + ;
	*!*				" GROUP BY D.LABORLEV5NM, D.LABORLEV5DSC, D.LABORLEV2NM, D.LABORLEV3NM, D.LABORLEV2DSC, D.LABORLEV3DSC, D.LABORLEV6NM, D.LABORLEV7NM, C.FULLNM, C.SHORTNM, C.PERSONNUM, C.PERSONID, A.EMPLOYEEID, E.ABBREVIATIONCHAR, E.NAME " + ;
	*!*				" ORDER BY 1, 2, 3, 4, 5, 6, 7, 8, 9 "

	*!*			lcSQLPass2A = ;
	*!*				" SELECT " + ;
	*!*				" EMPLOYEEID, " + ;
	*!*				" BASEWAGEHOURLYAMT AS WAGE, " + ;
	*!*				" EFFECTIVEDTM, " + ;
	*!*				" EXPIRATIONDTM " + ;
	*!*				" FROM BASEWAGERTHIST " + ;
	*!*				" WHERE '" + lcSQLStartDate + "' BETWEEN EFFECTIVEDTM AND EXPIRATIONDTM " + ;
	*!*				" ORDER BY EMPLOYEEID DESC, EXPIRATIONDTM DESC "

	*!*			IF .lTestMode THEN
	*!*				.TrackProgress('', LOGIT+SENDIT)
	*!*				.TrackProgress('lcSQLPass2 =' + lcSQLPass2, LOGIT+SENDIT)
	*!*				.TrackProgress('', LOGIT+SENDIT)
	*!*				.TrackProgress('lcSQLPass2A =' + lcSQLPass2A, LOGIT+SENDIT)
	*!*			ENDIF

	*!*			IF .ExecSQL(lcSQLPass2, 'CURWTKHOURSTEMPPRE', RETURN_DATA_NOT_MANDATORY) AND ;
	*!*					.ExecSQL(lcSQLPass2A, 'CURWAGES', RETURN_DATA_MANDATORY) THEN

	*!*				IF USED('CURWTKHOURSTEMPPRE') AND NOT EOF('CURWTKHOURSTEMPPRE') THEN

	*!*					* data was returned

	*!*					* add a foxpro date field to the cursor
	*!*					SELECT tdPass2Date AS PAYDATE, * ;
	*!*						FROM CURWTKHOURSTEMPPRE ;
	*!*						INTO CURSOR CURWTKHOURSTEMP ;
	*!*						ORDER BY EMPLOYEE ;
	*!*						READWRITE

	*!*					*!*	SELECT CURWTKHOURSTEMP
	*!*					*!*	BROWSE
	*!*					*!*	SELECT CURWAGES
	*!*					*!*	BROWSE

	*!*					* populate WAGE field in CURWTKHOURSTEMP
	*!*					SELECT CURWTKHOURSTEMP
	*!*					SCAN
	*!*						SELECT CURWAGES
	*!*						LOCATE FOR EMPLOYEEID = CURWTKHOURSTEMP.EMPLOYEEID
	*!*						IF FOUND() THEN
	*!*							REPLACE CURWTKHOURSTEMP.WAGE WITH CURWAGES.WAGE
	*!*						ENDIF
	*!*					ENDSCAN

	*!*					*!*	SELECT CURWTKHOURSTEMP
	*!*					*!*	BROWSE


	*!*					* now append it to CURWTKHOURS
	*!*					SELECT CURWTKHOURS
	*!*					APPEND FROM DBF('CURWTKHOURSTEMP')

	*!*				ELSE
	*!*					* NO DATA WAS RETURNED -- MAKE A NOTE OF IT IN CASE THERE SHOULD HAVE BEEN!
	*!*					.cTopBodyText = .cTopBodyText + "NOTE: no TEMP data found for date: " + TRANSFORM(tdPass2Date) + CRLF

	*!*				ENDIF  &&  USED('CURWTKHOURSTEMPPRE') AND NOT EOF('CURWTKHOURSTEMPPRE')

	*!*			ENDIF  &&  .ExecSQL(lcSQLPass2, 'CURWTKHOURSTEMPPRE', RETURN_DATA_NOT_MANDATORY)

	*!*		ENDWITH
	*!*		RETURN
	*!*		ENDPROC && BuildDailyCursor


ENDDEFINE
