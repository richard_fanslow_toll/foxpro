utilsetup("AGEGROUP832")
close data all
set step on
zuserid=upper(getenv("USERNAME"))
lcpath = 'F:\FTPUSERS\AGEGROUP\832IN\'
lcarchivepath = 'F:\FTPUSERS\AGEGROUP\832IN\Archive\'
useca("upcmast","wh",,,"select * from upcmast where  accountid in (1285)")

len1 = adir(ary1,lcpath+"*.*")
if len1 = 0
	wait window "No files found...exiting" timeout 2
	close data all
	schedupdate()
	_screen.caption=gscreencaption
	on error
	return
endif
for i = 1 to len1
	cfilename = alltrim(ary1[i,1])
	xfile = lcpath+cfilename
	carchivefile = (lcarchivepath+cfilename)
	cloadfile = (lcpath+cfilename)
	copy file [&cLoadFile] to [&cArchiveFile]


	create cursor age832 (company c(4), division c(4), season c(7),sku100 c(10), sku c(10),  sku100desc c(50), sku200 c(10), sku200desc c(50), sku300 c(10), sku300desc c(20), sku400 c(10), ;
		sku400desc1 c(30), sku400desc2 c(20), nrf c(10) , ppkunit n(4), cartonunit n(4) , sizescale c(5), size1 c(20), upc c(14), uom1 c(5),  wprice n(7,2), rprice n(7,2), cat c(5), group1 n(10), porh c(6),accountid i(4), info m(4), descrip c(50))


	tfile=alltrim(ary1[i,1])
	tfilepath=lcpath+alltrim(ary1[i,1])
	appe from "&tfilepath" type csv
	set step on
	delete file "&tfilepath"
	delete for company='COMP'
	select  *, alltrim(sku100)+alltrim(sku200) as style, sku300 as color, sku400 as id, wprice as price, space(2) as uom from age832 into cursor age832_2 readwrite
************************************sku400 conversion if *N *************************************  look at size1
	replace uom with 'EA' for uom1='EA'
	replace uom with 'CA' for uom1 !='EA'
	replace accountid with 1285 for accountid=0
	store "" to m.info
	set step on
	select age832_2
	scan
*!*			scatter memvar
*!*			m.accountid=1285
*!*		endscan
*!*		scan
		m.info = "COMPANY**"+allt(upper(age832_2.company))
		m.info = m.info+chr(13)+"DIVISION*"+allt(age832_2.division)
		m.info = m.info+chr(13)+"SEASON*"+allt(age832_2.season)
		m.info = m.info+chr(13)+"SKU100*"+trans(age832_2.sku100)
		m.info = m.info+chr(13)+"SKU*"+allt(age832_2.sku)
		m.info = m.info+chr(13)+"SKU100DESC*"+trans(age832_2.sku100desc)
		m.info = m.info+chr(13)+"SKU200*"+transform(age832_2.sku200)
		m.info = m.info+chr(13)+"SKU200DESC*"+transform(age832_2.sku200desc)
		m.info = m.info+chr(13)+"SKU300*"+transform(age832_2.sku300)
		m.info = m.info+chr(13)+"SKU300DESC*"+transform(age832_2.sku300desc)
		m.info = m.info+chr(13)+"SKU400*"+transform(age832_2.sku400)
		m.info = m.info+chr(13)+"SKU400DESC1*"+transform(age832_2.sku400desc1)
		m.info = m.info+chr(13)+"SKU400DESC2*"+transform(age832_2.sku400desc2)
		m.info = m.info+chr(13)+"NRF*"+(age832_2.nrf)		
		m.info = m.info+chr(13)+"PPKUNIT*"+transform(age832_2.ppkunit)		
		m.info = m.info+chr(13)+"CARTONUNIT*"+transform(age832_2.cartonunit)
		m.info = m.info+chr(13)+"SIZESCALE*"+(age832_2.sizescale)
		m.info = m.info+chr(13)+"SIZE1*"+(age832_2.size1)
		m.info = m.info+chr(13)+"UOM1*"+(age832_2.uom1)
		m.info = m.info+chr(13)+"CAT*"+(age832_2.cat)
		m.info = m.info+chr(13)+"GROUP*"+transform(age832_2.group1)
		m.info = m.info+chr(13)+"PORH*"+(age832_2.porh)
		replace age832_2.info with m.info in age832_2
		replace descrip with sku100desc in age832_2
	endscan
	replace id with size1 for id='*N'


	useca("upcmast","wh",,,"select * from upcmast where accountid = 1285")
	xsqlexec("select * from upcmast where accountid= 1285","xupcmast",,"wh")
	select xupcmast
	index on upc tag upc
	set order to

	set step on
	select upcmast
	scatter memvar memo blank
	select age832_2
	recctr=0
	scan
		scatter memvar memo
		recctr = recctr +1
		wait window at 10,10 "Checking Record # "+transform(recctr) nowait
		m.adddt=datetime()
		m.updatedt=datetime()
*		m.addby=zuserid
*		m.updateby=zuserid
		m.addproc='AGE832'
		m.updproc='AGE832'
		m.accountid=1285
		if !empty(upc)

			if !seek(alltrim(age832_2.upc),"xupcmast","upc")
				m.upcmastid=sqlgenpk("upcmast","wh")
				insert into upcmast from memvar
				release M.upcmastid
			else
				select upcmast
				locate for alltrim(upc)=alltrim(age832_2.upc)
				if found() then


					replace style                with age832_2.style   in upcmast
					replace color               with alltrim(age832_2.color)   in upcmast
					replace id                      with alltrim(age832_2.id)   in upcmast
					replace rprice            with (age832_2.rprice)   in upcmast
					replace uom                 with age832_2.uom   in upcmast
					replace descrip           with age832_2.sku100desc   in upcmast
					replace price                with age832_2.price   in upcmast
					replace info                  with age832_2.info   in upcmast
					replace updatedt        with datetime()   in upcmast
				endif
			endif
		endif
	endscan
	set step on
	tu("upcmast")
	select age832_2
	if reccount() > 0
		export to "S:\Agegroup\temp\style_master"  type xls
		tsendto = "tmarg@fmiint.com,ed@agegroupltd.com"
		tattach = "S:\Agegroup\temp\style_master.xls"
		tcc =""
		tfrom ="Toll EDI System Operations <fmi-transload-ops@fmiint.com>"
		tmessage = "Age Group Stylemaster add/u[date_:"+ttoc(datetime())
		tsubject = "Age Group Stylemaster add/u[date"
		do form m:\dev\frm\dartmail2 with tsendto,tfrom,tsubject, tcc,tattach,tmessage  ,"A"

	else
	endif

endfor
close databases all
