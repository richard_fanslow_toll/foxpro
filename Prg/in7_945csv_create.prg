*!* IN7 945 Cartons-to-Units *CSV* (Whse. Shipping Advice) Creation Program
*!* Creation Date: 01.05.2010 by Joe

PARAMETERS cBOL,nWO_Num

PUBLIC ARRAY thisarray(1)
PUBLIC cWO_Num,cWO_NumStr,lTesting,lTestinput
PUBLIC lEmail,lFilesout,dDateTimeCal,tsendto,tcc,tsendtoerr,tccerr,lPSU,nAcctNum,lDoCompare
PUBLIC m.pickticket,m.ucc128,m.pt_linenum,cPTLow,cPTHigh,lStyleError,cProgname,tsendtotest,tcctest
PUBLIC m.gtin_upc,m.shipqty,m.tracknum,m.ctntype,m.ctnweight,m.bolnum,m.pronum,m.linenum,cStatus
PUBLIC m.shipdate,m.shipvia,m.frt_terms,m.freight,m.freightper,cxErrMsg,lDoCatch,nWODelete,lScanned,tfrom
PUBLIC cMod,cMBOL,cFilenameShort,cFilenameOut,cFilenameArch,lParcelType,cWO_NumList,lcPath,cEDIType,cISA_Num

STORE "" TO m.pickticket,m.ucc128,m.style,m.color,m.size,m.sizenum
STORE "" TO m.gtin_upc,m.tracknum,m.ctntype,m.bolnum,m.pronum
STORE "" TO m.shipvia,m.frt_terms,m.freightper,m.linenum
STORE 0  TO m.ctnweight,m.freight,nWODelete
STORE DATE() TO m.shipdate
cTime = DATETIME()

*SET STEP ON
lTesting   = .F.  && Set to .t. for testing
lTestinput = .F.  && Set to .t. for test input (WHP) files only!
lEmail = .T.  && Default = .t.
lScanned = .F. && Set to .t. to run 'scanpacked' loads. Default = .f.
nWODelete =  3200332 && WO# if overages need to be ignored and 945 for original PT only run.
lDoCompare = !lTesting
cStatus = "Error"

DO m:\dev\prg\_setvars WITH lTesting
cProgname = "in7_945csv"
SET DATE AMERICAN
WAIT WINDOW "" TIMEOUT 2

ON ESCAPE CANCEL
IF lTesting
	SET STATUS BAR ON
ENDIF
cxErrMsg = ""
tattach = ""
lIsError = .F.
lDoCatch = .T.
cShip_ref = ""
lCloseOutput = .T.
ccustname = ""
cWO_NumStr = ""
cWO_NumList = ""
lPSU = .F.
lStyleError = .F.

cWinmsg = "At the start of IN7 945 (Cartons to Units) *CSV* process..."
WAIT WINDOW cWinmsg TIMEOUT 1
*ASSERT .F. MESSAGE cWinmsg
TRY
	closefiles()
	lFederated = .F.
	nAcctNum = 6059
	cPPName = "IN7"
	cOffice = "N"
	cMod = cOffice
	gMasterOffice = cOffice
	gOffice = "I"
	cMBOL = ""
	cEDIType = "945"
	cISA_Num = "CSV FILE"
	lParcelType = .F.

	tfrom = "Toll EDI Operations <toll-edi-ops@tollgroup.com>"
	tmessage=""
	tcc = ""
	lPSU = .F.
	lSamples = .F.
	cBOL1 = ""
	lISAFlag = .T.
	lSTFlag = .T.

	IF TYPE("cBOL") = "L"
		IF !lTesting AND !lTestinput
			WAIT WINDOW "BOL not provided...terminating" TIMEOUT 3
			lCloseOutput = .F.
			cxErrMsg = "NO BOL"
			DO EDIUPDATE WITH .T.,cxErrMsg
		ELSE
			DO m:\dev\prg\lookups
			CLOSE DATABASES ALL
			IF !USED('edi_trigger')
				cEDIFolder = "F:\3PL\DATA\"
				USE (cEDIFolder+"edi_trigger") IN 0 ALIAS edi_trigger
			ENDIF
* TEST DATA AREA
			cBOL = "04070086059403303"
			cTime = DATETIME()
			lFedEx = .F.
		ENDIF
	ENDIF
	cBOL=TRIM(cBOL)

	CREATE CURSOR template1 (pickticket c(20),ucc128 c(20),linenum c(10),STYLE c(20),;
		COLOR c(20),SIZE c(15),sizenum c(10),gtin_upc c(20),shipqty N(8),tracknum c(20),;
		ctntype c(10),ctnweight N(8),bolnum c(20),pronum c(15),shipdate d,shipvia c(4),;
		frt_terms c(20),freight N(10,2),freightper c(10))
	SELECT template1

	COPY TO c:\tempfox\template1
*		WAIT WINDOW "template1 copy created in c:\tempfox...open and proceed" TIMEOUT 2

	CLEAR

	SELECT 0
	CREATE CURSOR temp945 (accountid i,isa_num c(10),st_num c(10),wo_num i,bol_no c(20),;
		ship_ref c(20),filename c(50),filedate T)

	IF lTestinput
		cUseFolder = "F:\WHP\WHDATA\"
	ELSE
		xReturn = "XXX"
		xReturn = wf(cOffice,nAcctNum)
		cUseFolder = UPPER(xReturn)
	ENDIF

	IF USED('OUTWOLOG')
		USE IN outwolog
	ENDIF

	USE F:\3pl\DATA\parcel_carriers IN 0 ALIAS parcel_carriers

	IF USED('SHIPMENT')
		USE IN shipment
	ENDIF

	IF USED('PACKAGE')
		USE IN package
	ENDIF

	gOffice=cOffice

	dDate = DATE()-5
	csq1 = [select * from shipment where right(rtrim(str(accountid)),4) = ]+TRANSFORM(nAcctNum)+[ and shipdate >= {]+DTOC(dDate)+[}]
	xsqlexec(csq1,,,"wh")
	INDEX ON shipmentid TAG shipmentid
	INDEX ON pickticket TAG pickticket

	SELECT shipment
	LOCATE
	IF RECCOUNT("shipment") > 0
		xjfilter="shipmentid in ("
		SCAN
			nShipmentId = shipment.shipmentid
			xsqlexec("select * from package where shipmentid="+TRANSFORM(nShipmentId),,,"wh")

			IF RECCOUNT("package")>0
				xjfilter=xjfilter+TRANSFORM(shipmentid)+","
			ELSE
				xjfilter="1=0"
			ENDIF
		ENDSCAN
		xjfilter=LEFT(xjfilter,LEN(xjfilter)-1)+")"

		xsqlexec("select * from package where "+xjfilter,,,"wh")
	ELSE
		xsqlexec("select * from package where .f.",,,"wh")
	ENDIF
	SELECT package
	INDEX ON shipmentid TAG shipmentid
	SET ORDER TO TAG shipmentid

	IF USED('OUTSHIP')
		USE IN outship
	ENDIF

	csq1 = [select * from outship where accountid = ]+TRANSFORM(nAcctNum)+[ and office = ']+cOffice+[' and bol_no = ']+cBOL+[']
	xsqlexec(csq1,,,"wh")
	IF RECCOUNT() = 0
		cErrMsg = "MISS BOL "+cBOL
		DO EDIUPDATE WITH cErrMsg,.T.
		THROW
	ENDIF
	INDEX ON bol_no TAG bol_no
	INDEX on wo_num TAG wo_num

	SELECT outship
	IF !SEEK(cBOL,'outship','bol_no')
		WAIT WINDOW "BOL not found in OUTSHIP" TIMEOUT 2
		cxErrMsg = "BOL NOT FOUND"
		DO EDIUPDATE WITH .T.,cxErrMsg
	ELSE
*		SET STEP ON
		cSCAC = ALLTRIM(outship.scac)
		nWO_Num = outship.wo_num
		lScanned = IIF("SCANNED*TRUE"$outship.shipins,.T.,.F.)
		IF lScanned
			WAIT WINDOW "This is a SCANNED order" TIMEOUT 2
			lDoCompare = .F.
		ENDIF
		SCAN FOR outship.bol_no = cBOL
			IF EMPTY(DCNUM)
				REPLACE DCNUM WITH IIF(storenum>0,ALLTRIM(STR(storenum)),"9999") NEXT 1 IN outship
			ENDIF
		ENDSCAN
	ENDIF
	cWO_Num = ALLTRIM(STR(nWO_Num))
*!*		IF !lTestinput
*!*			IF usesqloutwo()
*!*				xsqlexec("select * from outwolog where wo_num = "+TRANSFORM(nWO_Num)",,,"wh")
*!*			ELSE
*!*				USE (cUseFolder+"OUTWOLOG") IN 0 ALIAS outwolog ORDER TAG wo_num
*!*			ENDIF
*!*		ENDIF

	waitstr = "BOL# "+ALLTRIM(cBOL)+", WO#: "+cWO_Num
	WAIT WINDOW waitstr TIMEOUT 2

	cRetMsg = "X"
	IF lDoCompare
		ASSERT .F. MESSAGE "At SQL Data comparison"
		DO m:\dev\prg\sqldata-COMPARE WITH cUseFolder,cBOL,nWO_Num,cOffice,nAcctNum,cPPName
		IF cRetMsg<>"OK"
			ASSERT .F. MESSAGE "At SDC"
			lCloseOutput = .T.
			cWO_Num = ALLTRIM(STR(nWO_Num))
			cxErrMsg = "SDC: "+cRetMsg+", "+cWO_Num
			DO EDIUPDATE WITH .T.,cxErrMsg
		ENDIF
	ENDIF

	lParcelType = IIF(SEEK(cSCAC,"parcel_carriers","scac"),.T.,.F.)
	lParcelType = IIF(LEFT(cBOL,3)="PSU",.T.,lParcelType)

	IF VARTYPE(nWO_Num)<> "N"
		lCloseOutput = .T.
		cxErrMsg = "BAD WO#"
		DO EDIUPDATE WITH .T.,cxErrMsg
	ENDIF

	lTestMail = lTesting && Sends mail to Joe only
	lFilesout = !lTesting && If true, copies output to FTP folder (default = .t.)
	lStandalone = lTesting

*!*	lTestMail = .t.
*!*	lFilesOut = .f.

	WAIT WINDOW "Now preparing tables and variables" NOWAIT NOCLEAR

*!* SET CUSTOMER CONSTANTS
	ccustname = "IN7"  && Customer Identifier
	cMailName = "IN7"
	cCustLoc = "NJ"
	cCustPrefix = "945j"

	SELECT 0
	USE F:\3pl\DATA\mailmaster ALIAS mm
	LOCATE FOR mm.edi_type = "945" AND mm.office=cOffice AND mm.accountid = nAcctNum
	tsendto = IIF(mm.use_alt,mm.sendtoalt,mm.sendto)
	tcc = IIF(mm.use_alt,mm.ccalt,mm.cc)
	LOCATE
	LOCATE FOR edi_type = "MISC" AND mm.taskname = "GENERAL"
	tsendtoerr = IIF(mm.use_alt,mm.sendtoalt,mm.sendto)
	tccerr = IIF(mm.use_alt,mm.ccalt,mm.cc)
	tsendtotest = IIF(mm.use_alt,mm.sendtoalt,mm.sendto)
	tcctest = IIF(mm.use_alt,mm.ccalt,mm.cc)
	USE IN mm

	IF lTesting OR lTestMail
		tsendto = tsendtotest
		tcc = tcctest
		tsendtoerr = tsendtotest
		tccerr = tcctest
	ENDIF

*!* SET OTHER CONSTANTS
	cCustFolder = UPPER(ccustname)+"-"+cCustLoc
	STORE "" TO lcKey
	STORE 0 TO alength,nLength
	cDelTime = SUBSTR(TTOC(cTime,1),9,4)
	dapptnum = ""

*!* SET OTHER CONSTANTS
	IF INLIST(cOffice,'C','1','2')
		dDateTimeCal = (DATETIME()-(3*3600))
	ELSE
		dDateTimeCal = DATETIME()
	ENDIF
	dt1 = TTOC(dDateTimeCal,1)
	dtmail = TTOC(dDateTimeCal)
	dt2 = dDateTimeCal
	cString = ""

	cdate = DTOS(DATE())
	cStyle = ""
	cPTString = ""
	cTargetStyle = ""

	SELECT outship
	SELECT MIN(ship_ref) AS ptlo FROM outship WHERE outship.bol_no = cBOL INTO CURSOR tempq
	cPTLow = ALLTRIM(tempq.ptlo)
	SELECT MAX(ship_ref) AS pthi FROM outship WHERE outship.bol_no = cBOL INTO CURSOR tempq
	cPTHigh = ALLTRIM(tempq.pthi)
	USE IN tempq
	cPTRange = cPTLow+"-"+cPTHigh+"_"+DTOS(DATE())
	RELEASE PTLow,PTHigh
	cStagingFolder = "f:\ftpusers\"+cCustFolder+"\945-Staging\"
	WAIT WINDOW "Staging folder: "+cStagingFolder TIMEOUT 3
	cOutFolder = "f:\ftpusers\"+cCustFolder+"\945OUT\"
	lcPath = cOutFolder
	cArchiveFolder = "f:\ftpusers\"+cCustFolder+"\945OUT\archive\"

	cFilenameHold = (cStagingFolder+cCustPrefix+"_"+cPTRange+".txt")
	cFilenameShort = JUSTFNAME(cFilenameHold)
	cFilenameOut = (cOutFolder+cFilenameShort)
	cFilenameArch = (cArchiveFolder+cFilenameShort)
	CD &cStagingFolder

	IF USED('OUTDET')
		USE IN outdet
	ENDIF

	selectoutdet()
	SELECT outdet
	INDEX ON outdetid TAG outdetid

&& Added code blocks to the following to overcome BOL vs. WO selection issues, 12.23.2005
	SELECT outship
	LOCATE
	IF USED("sqlwo")
		USE IN sqlwo
	ENDIF
	DELETE FILE "F:\3pl\DATA\sqlwo.dbf"
	SELECT bol_no,wo_num ;
		FROM outship WHERE bol_no = cBOL ;
		GROUP BY 1,2 ;
		ORDER BY 1,2 ;
		INTO DBF F:\3pl\DATA\sqlwo
	USE IN sqlwo
	USE F:\3pl\DATA\sqlwo IN 0 ALIAS sqlwo

*	ASSERT .f. MESSAGE "At SQL Connect"
	cRetMsg = ""
	DO m:\dev\prg\sqlconnect_bol  WITH nAcctNum,ccustname,cPPName,.T.,cOffice

	IF cRetMsg<>"OK"
		ASSERT .F. MESSAGE "At SDC"
		cxErrMsg = "SCB: "+cRetMsg
		DO EDIUPDATE WITH .T.,cxErrMsg
	ENDIF

	IF lScanned
		SELECT * FROM vin7pp INTO CURSOR ppclone READW
	ENDIF

	SELECT vin7pp
	cRecs = TRANSFORM(RECCOUNT())
	LOCATE
	IF EOF()
		ASSERT .F. MESSAGE "VIN7PP empty...debug"
		cxErrMsg = "EMPTY SQL DATA"
		DO EDIUPDATE WITH .T.,cxErrMsg
	ELSE
		WAIT WINDOW "There are "+cRecs+" records in the SQL cursor" TIMEOUT 1
	ENDIF
	IF lTesting
*		BROWSE
	ENDIF
*	ASSERT .F. MESSAGE "Post SQL data...debug"
	SELECT wo_num FROM vin7pp GROUP BY 1 INTO CURSOR temprunid1
	SELECT runid FROM vin7pp GROUP BY 1 INTO CURSOR temprunid2
	SELECT temprunid1
	STORE RECCOUNT() TO nWORecs
	SELECT temprunid2
	STORE RECCOUNT() TO nRIDRecs

	IF nWORecs#nRIDRecs
		cxErrMsg = "MULTI RUNIDS"
		DO EDIUPDATE WITH .T.,cxErrMsg
	ENDIF
	USE IN temprunid1
	USE IN temprunid2

	IF !USED("scacs")
		xsqlexec("select * from scac","scacs",,"wh")
		INDEX ON scac TAG scac
	ENDIF

	SELECT outship
	LOCATE
	WAIT CLEAR
	WAIT WINDOW "Now creating Header information" NOWAIT NOCLEAR

*!* HEADER LEVEL EDI DATA
	SELECT outship
	LOCATE
	IF !SEEK(nWO_Num,'outship','wo_num')
		WAIT CLEAR
		WAIT WINDOW "Invalid Work Order Number - Not Found in OUTSHIP!" TIMEOUT 2
		cxErrMsg = "WO NOT FOUND"
		DO EDIUPDATE WITH .T.,cxErrMsg
	ENDIF

	IF !lTesting
		IF EMPTY(cBOL)  && May be empty if triggered from PT screen, or FedEx shipment
			WAIT CLEAR
			WAIT WINDOW "MISSING Bill of Lading Number!" TIMEOUT 2
			cxErrMsg = "BOL# EMPTY"
			DO EDIUPDATE WITH .T.,cxErrMsg
		ELSE
			IF !SEEK(PADR(TRIM(cBOL),20),"outship","bol_no")
				WAIT CLEAR
				WAIT WINDOW "Invalid Bill of Lading Number - Not Found!" TIMEOUT 2
				cxErrMsg = "BOL# NOT FOUND"
				DO EDIUPDATE WITH .T.,cxErrMsg
			ENDIF
		ENDIF
	ENDIF

*************************************************************************
*1	PTIDSOURCE/OUTSHIP RECORD MAIN LOOP
*	Loops through individual OUTSHIP lines for BOL#
*************************************************************************
	WAIT CLEAR
	WAIT WINDOW "Now creating BOL#-based information..." NOWAIT NOCLEAR
	IF lParcelType
		IF EMPTY(cBOL) OR ISNULL(cBOL)
			cxErrMsg = "UPS BOL Empty"
			DO EDIUPDATE WITH .T.,cxErrMsg
		ENDIF
		STORE cBOL TO m.tracknum
		cmsg = "UPS Tracking # is "+m.tracknum
		WAIT WINDOW cmsg TIMEOUT 2
*		ASSERT .F. MESSAGE cmsg
	ELSE
		STORE ALLTRIM(cBOL) TO m.bolnum
	ENDIF

	IF lTesting
		cScanstr =  "outship.bol_no = cBOL"
	ELSE
		cScanstr = "outship.bol_no = cBOL AND !EMPTYnul(del_date)"
	ENDIF
	COUNT TO N FOR &cScanstr

	IF N=0
		cxErrMsg = "INCOMP BOL"
		DO EDIUPDATE WITH .T.,cxErrMsg
	ENDIF

	cMissDel = ""
	LOCATE
	SELECT outship
	SCAN FOR &cScanstr
		SCATTER MEMVAR MEMO
		IF "GEORGE"$outship.consignee AND "MARTHA"$outship.consignee
			DO EDIUPDATE WITH .F.,"GEORGE & MARTHA"
		ENDIF

		lMacy = IIF("MACY"$m.consignee,.T.,.F.)
		cWO_Num = ALLTRIM(STR(m.wo_num))
		IF !(cWO_Num$cWO_NumStr)
			cWO_NumStr = IIF(EMPTY(cWO_NumStr),cWO_Num,cWO_NumStr+","+cWO_Num)
		ENDIF
		IF !(cWO_Num$cWO_NumList)
			cWO_NumList = IIF(EMPTY(cWO_NumList),cWO_Num,cWO_NumList+CHR(13)+cWO_Num)
		ENDIF
		lPick = .F.
		IF lTesting OR lTestinput
			lPick = .F.
		ELSE
			csq1 = [select * from outwolog where accountid = ]+TRANSFORM(nAcctNum)+[ and wo_num = ]+TRANSFORM(outship.wo_num)
			xsqlexec(csq1,,,"wh")
			IF RECCOUNT() = 0
				cErrMsg = "MISS OUTWOLOG REC FOR WO# "+cWO_Num
				DO EDIUPDATE WITH cErrMsg,.T.
				THROW
			ENDIF
			SELECT outwolog
			INDEX ON wo_num TAG wo_num

			IF SEEK(m.wo_num,"outwolog","wo_num")
				lPick = IIF(outwolog.picknpack,.T.,.F.)
			ENDIF
		ENDIF
		USE IN outwolog

		lPrepack = IIF(lPick,.F.,.T.)
		nTotCtnCount = m.qty
		cPO_Num = ALLTRIM(m.cnee_ref)
		cShip_ref = ALLTRIM(m.ship_ref)
		IF "OV"$cShip_ref
			LOOP
		ENDIF
		STORE cShip_ref TO m.pickticket

*!* Added this code to trap miscounts in OUTDET Units
*		ASSERT .F. MESSAGE "At OUTDET/SQL totqty comparison...DEBUG"
		SELECT outdet
		IF (nWO_Num = nWODelete) OR (outship.outshipid = 1478488)
			SET DELETED OFF
		ENDIF

		SET ORDER TO
		IF nWO_Num = nWODelete
			SUM totqty TO nUnitTot1 FOR !units AND outdet.wo_num = nWO_Num
		ELSE
			SUM totqty TO nUnitTot1 FOR !units AND outdet.outshipid = outship.outshipid
		ENDIF

		SELECT vin7pp
		SELECT DISTINCT ucc;
			FROM vin7pp ;
			WHERE vin7pp.outshipid = outship.outshipid ;
			AND vin7pp.ucc#"CUTS" ;
			AND vin7pp.totqty > 0 ;
			INTO CURSOR tempv
		IF lTesting AND outship.outshipid = 1744231
			BROWSE
			SET STEP ON
		ENDIF

		COUNT TO nUnitTot2 FOR !DELETED()
		IF nUnitTot1<>nUnitTot2
			ASSERT .F. MESSAGE "In Tot Qty Error...debug"
			SET STEP ON
			cxErrMsg = "SQL TOTQTY ERR @ OSID "+TRANSFORM(outship.outshipid)
			DO EDIUPDATE WITH .T.,cxErrMsg
		ENDIF
		USE IN tempv
		SET DELETED ON
		SELECT outdet
		SET ORDER TO outdetid
		LOCATE

*!* End code addition
		SELECT outship
		IF (("PENNEY"$UPPER(outship.consignee)) OR ("KMART"$UPPER(outship.consignee)) OR ("K-MART"$UPPER(outship.consignee)))
			lApptFlag = .T.
		ELSE
			lApptFlag = .F.
		ENDIF
		IF lTestinput OR lTesting
			ddel_date = DATE()
		ELSE
			ddel_date = outship.del_date
			IF EMPTY(ddel_date)
				cMissDel = IIF(EMPTY(cMissDel),"The following PTs had no Delivery Dates:"+CHR(13)+TRIM(cShip_ref),cMissDel+CHR(13)+TRIM(cShip_ref))
			ENDIF
		ENDIF
		STORE ddel_date TO m.shipdate

		IF !lTesting
			IF lParcelType
				cBOL = TRIM(outship.bol_no)
			ELSE
				nWO_Num = outship.wo_num
				cWO_Num = ALLTRIM(STR(nWO_Num))
			ENDIF
		ENDIF

		IF ALLTRIM(outship.SForCSZ) = ","
			BLANK FIELDS outship.SForCSZ NEXT 1 IN outship
		ENDIF

		alength = ALINES(apt,outship.shipins,.T.,CHR(13))

		cTRNum = ""
		cPRONum = ""

		IF (("WALMART"$outship.consignee) OR ("WAL-MART"$outship.consignee) OR ("WAL MART"$outship.consignee))
			IF LEFT(outship.keyrec,2) = "PR"
				STORE TRIM(keyrec) TO cPRONum
				m.pronum = cPRONum
			ENDIF
			IF LEFT(outship.keyrec,2) = "TR"
				STORE TRIM(keyrec) TO cTRNum
			ENDIF
		ENDIF

		nOutshipid = m.outshipid
		IF lParcelType
			cPTString = IIF(EMPTY(cPTString),m.consignee+" "+cBOL+" "+cShip_ref,cPTString+CHR(13)+m.consignee+" "+cBOL+" "+cShip_ref)
		ELSE
			cPTString = IIF(EMPTY(cPTString),m.consignee+" "+cShip_ref,cPTString+CHR(13)+m.consignee+" "+cShip_ref)
		ENDIF
		m.CSZ = TRIM(m.CSZ)

		IF EMPTY(M.CSZ)
			WAIT CLEAR
			WAIT WINDOW "No SHIP-TO City/State/ZIP info...exiting" TIMEOUT 2
			cxErrMsg = "BAD ADDR INFO"
			DO EDIUPDATE WITH .T.,cxErrMsg
		ENDIF
		m.CSZ = ALLTRIM(STRTRAN(m.CSZ,"  "," "))
		IF !","$m.CSZ
			cxErrMsg = "NO COMMA CSZ-"+cShip_ref
			DO EDIUPDATE WITH .T.,cxErrMsg
		ENDIF
		IF !", "$m.CSZ
			REPLACE outship.CSZ WITH STRTRAN(outship.CSZ,",",", ")
			SCATTER FIELDS CSZ MEMVAR
		ENDIF
		nSpaces = OCCURS(" ",ALLTRIM(m.CSZ))
		IF nSpaces = 0
			WAIT WINDOW "Ship-to CSZ Segment Error" TIMEOUT 3
			cxErrMsg = "SHIP-TO CSZ-"+cShip_ref
			DO EDIUPDATE WITH .T.,cxErrMsg
		ELSE
*			ASSERT .F. MESSAGE "At comma check"
			nCommaPos = 0
			nCommaPos = AT(",",m.CSZ)
			IF nCommaPos = 0
				cxErrMsg = "SHIP-TO CSZ-NO COMMA"
				DO EDIUPDATE WITH .T.,cxErrMsg
			ENDIF
			nLastSpace = AT(" ",m.CSZ,nSpaces)
			IF ISALPHA(SUBSTR(TRIM(m.CSZ),AT(" ",m.CSZ,nSpaces-1)+1,2))
				cCity = LEFT(TRIM(m.CSZ),AT(",",m.CSZ)-1)
				cState = SUBSTR(m.CSZ,nCommaPos+2,2)
				cZip = TRIM(SUBSTR(m.CSZ,nLastSpace+1))
			ELSE
				WAIT CLEAR
				WAIT WINDOW "NOT ALPHA: "+SUBSTR(TRIM(m.CSZ),AT(" ",m.CSZ,nSpaces-1)+1,2) TIMEOUT 3
				cCity = LEFT(TRIM(m.CSZ),AT(",",m.CSZ)-1)
				cState = SUBSTR(TRIM(m.CSZ),AT(" ",m.CSZ,nSpaces-2)+1,2)
				cZip = TRIM(SUBSTR(m.CSZ,nLastSpace+1))
			ENDIF
		ENDIF

		STORE "" TO cSForCity,cSForState,cSForZip
		IF !lFederated AND !EMPTY(ALLT(m.shipfor))
			IF EMPTY(M.SForCSZ)
				WAIT CLEAR
				WAIT WINDOW "No SHIP-FOR City/State/ZIP info...exiting" TIMEOUT 2
				cxErrMsg = "BAD ADDR INFO"
				DO EDIUPDATE WITH .T.,cxErrMsg
			ELSE
				m.SForCSZ = ALLTRIM(STRTRAN(m.SForCSZ,"  "," "))
				nSpaces = OCCURS(" ",ALLTRIM(m.SForCSZ))
				IF nSpaces = 0
					m.SForCSZ = STRTRAN(m.SForCSZ,",","")
					cSForCity = ALLTRIM(m.SForCSZ)
					cSForState = ""
					cSForZip = ""
				ELSE
					nCommaPos = AT(",",m.SForCSZ)
					nLastSpace = AT(" ",m.SForCSZ,nSpaces)
					nMinusSpaces = IIF(nSpaces=1,0,1)
					IF ISALPHA(SUBSTR(TRIM(m.SForCSZ),AT(" ",TRIM(m.SForCSZ),nSpaces-nMinusSpaces)+1,2))
						cSForCity = LEFT(TRIM(m.SForCSZ),AT(",",m.SForCSZ)-1)
						cSForState = SUBSTR(m.SForCSZ,nCommaPos+2,2)
						cSForZip = TRIM(RIGHT(TRIM(m.SForCSZ),5))
						IF ISALPHA(cSForZip)
							cSForZip = ""
						ENDIF
					ELSE
						WAIT CLEAR
						WAIT WINDOW "NOT ALPHA: "+SUBSTR(TRIM(m.SForCSZ),AT(" ",m.SForCSZ,nSpaces-1)+1,2) TIMEOUT 3
						cSForCity = LEFT(TRIM(m.SForCSZ),AT(",",m.SForCSZ)-1)
						cSForState = SUBSTR(TRIM(m.SForCSZ),AT(" ",m.SForCSZ,nSpaces-2)+1,2)
						cSForZip = TRIM(RIGHT(TRIM(m.SForCSZ),5))
						IF ISALPHA(cSForZip)
							cSForZip = ""
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ELSE
			cStoreName = segmentget(@apt,"STORENAME",alength)
		ENDIF

		WAIT CLEAR
		WAIT WINDOW "Now creating Line Item information" NOWAIT NOCLEAR

		INSERT INTO temp945 (accountid,isa_num,st_num,wo_num,bol_no,ship_ref,filename,filedate) ;
			VALUES (m.accountid,"","",m.wo_num,m.bol_no,m.ship_ref,cFilenameShort,dt2)

		m.ship_via = ""
		SELECT outship

		IF lParcelType
			SELECT shipment
			IF SEEK(cShip_ref,"shipment","pickticket")
				SELECT package
				SUM package.pkgcharge TO nFreight FOR package.shipmentid=shipment.shipmentid
				STORE nFreight TO m.freight
				STORE "P" TO m.freightper
			ELSE
				WAIT WINDOW "Pickticket "+cShip_ref+" does not exist in SHIPMENT" TIMEOUT 2
				cxErrMsg = "NO PT "+cShip_ref+" in Shipment"
				DO EDIUPDATE WITH .T.,cxErrMsg
			ENDIF
		ENDIF

		IF lPSU
*			ASSERT .F. MESSAGE "IN PSU tracking number select...DEBUG!"
			SELECT shipment
			IF SEEK(cShip_ref,"shipment","pickticket")
				SELECT trknumber ;
					FROM package ;
					WHERE package.shipmentid = shipment.shipmentid ;
					INTO CURSOR tracknum1
				SELECT tracknum1
				nTrk = RECCOUNT()
				LOCATE
			ELSE
				WAIT WINDOW "Pickticket "+cShip_ref+" does not exist in SHIPMENT" TIMEOUT 2
				cxErrMsg = "NO PT "+cShip_ref+": UPS"
				DO EDIUPDATE WITH .T.,cxErrMsg
			ENDIF
			SELECT DISTINCT ucc ;
				FROM vin7pp ;
				WHERE vin7pp.ship_ref = cShip_ref ;
				INTO CURSOR tempv
			SELECT tempv
			nVcnt = RECCOUNT()
			USE IN tempv
			IF nVcnt # nTrk
				cxErrMsg = "SQL>UPS CTN COUNT OFF"
				DO EDIUPDATE WITH .T.,cxErrMsg
			ENDIF
			RELEASE vcnt
		ENDIF

*************************************************************************
*2	DETAIL LOOP - MATCHING SHIP_REF FIELD (FROM OUTSHIP)
*************************************************************************
		WAIT CLEAR
		WAIT WINDOW "Now creating Detail information "+cShip_ref NOWAIT NOCLEAR
		SET DELETED ON
		IF nWO_Num = nWODelete
*			SET DELETED OFF
		ENDIF
		SELECT outdet
		SET ORDER TO TAG outdetid
		SELECT vin7pp
		SET RELATION TO outdetid INTO outdet

		LOCATE
		LOCATE FOR ship_ref = TRIM(cShip_ref) AND outshipid = nOutshipid
		IF !FOUND()
			IF !lTesting
				WAIT WINDOW "PT "+cShip_ref+" NOT FOUND in VIN7PP...ABORTING" TIMEOUT 2
				cxErrMsg = "MISS PT-SQL: "+cShip_ref
				DO EDIUPDATE WITH .T.,cxErrMsg
			ELSE
				LOOP
			ENDIF
		ENDIF

		IF nWO_Num = nWODelete
			scanstr = "vin7pp.wo_num = nWO_Num"
		ELSE
			scanstr = "vin7pp.ship_ref = cShip_ref and vin7pp.outshipid = nOutshipid"
		ENDIF

		SELECT vin7pp
		LOCATE
		LOCATE FOR &scanstr
		cCartonNum= "XXX"

		DO WHILE &scanstr
			SCATTER MEMVAR
			IF vin7pp.ucc = 'CUTS'
				SKIP 1 IN vin7pp
				LOOP
			ENDIF
			l940EDI = IIF("940TYPE*EDI"$outdet.printstuff,.T.,.F.)
			nLoops = OCCURS("SUBDET",outdet.printstuff)
			alength = ALINES(aptdet,outdet.printstuff,.T.,CHR(13))
			lSkipBack = .T.

			IF TRIM(vin7pp.ucc) <> cCartonNum
				STORE TRIM(vin7pp.ucc) TO cCartonNum
			ENDIF
			DO WHILE vin7pp.ucc = cCartonNum
				cUCCNumber = vin7pp.ucc
				IF cUCCNumber =  "CUTS"
					SKIP 1 IN vin7pp
					LOOP
				ENDIF
				WAIT WINDOW cUCCNumber NOWAIT
				nvodid = vin7pp.outdetid
				WAIT WINDOW "VIN7PP OUTDETID: "+TRANSFORM(vin7pp.outdetid) NOWAIT
				IF EMPTY(cUCCNumber) OR ISNULL(cUCCNumber)
					WAIT CLEAR
					WAIT WINDOW "Empty UCC Number in vin7pp "+cShip_ref TIMEOUT 2
					cxErrMsg = "EMPTY UCC# in "+cShip_ref
					DO EDIUPDATE WITH .T.,cxErrMsg
				ENDIF
				cUCCNumber = TRIM(STRTRAN(ALLTRIM(cUCCNumber)," ",""))
				STORE cUCCNumber TO m.ucc128

				nShipDetQty = 1

*!*					IF lTesting
*!*						m.ctnweight = INT(2*outdet.totqty)
*!*						WAIT WINDOW "Carton weight: "+TRANSFORM(m.ctnweight) NOWAIT
*!*					ELSE
				IF EMPTY(outdet.ctnwt) OR outdet.ctnwt = 0
					IF !lTesting
						m.ctnweight = INT(outship.weight/outship.ctnqty)
						IF m.ctnweight = 0
							cxErrMsg = "WEIGHT ERR: PT "+cShip_ref
							DO EDIUPDATE WITH .T.,cxErrMsg
						ENDIF
					ELSE
						m.ctnweight = 5
					ENDIF
				ELSE
					m.ctnweight = outdet.ctnwt
				ENDIF
*!*					ENDIF

				cStyle = TRIM(outdet.STYLE)
				STORE cStyle TO m.style
				cColor = TRIM(outdet.COLOR)
				STORE cColor TO m.color
				IF ISNULL(m.tracknum)
					m.tracknum = ""
				ENDIF
				nShipDetQty = (vin7pp.totqty) && /(INT(VAL(vin7pp.PACK))))
				IF ISNULL(nShipDetQty)
					nShipDetQty = outdet.totqty
				ENDIF

				nOrigDetQty = (vin7pp.qty) && /(INT(VAL(vin7pp.PACK))))
				IF ISNULL(nOrigDetQty)
					nOrigDetQty = nShipDetQty
				ENDIF

				IF l940EDI
					m.shipqty = 0
*					ASSERT .F. MESSAGE "In Subdet of "+TRANSFORM(nvodid)+"...>>DEBUG<<"
					FOR qw = 1 TO alength
						SET EXACT OFF
						cData = ALLTRIM(aptdet[qw])
						cData=STRTRAN(cData,CHR(13),'')
						cData=STRTRAN(cData,CHR(10),'')
						DO CASE
						CASE cData = "SIZE*"
							m.size = ALLT(SUBSTR(cData,AT("*",cData)+1))
						CASE cData = "SIZENUM*"
							m.sizenum = ALLT(SUBSTR(cData,AT("*",cData)+1))
						CASE cData = "UPC*"
							m.gtin_upc = ALLT(SUBSTR(cData,AT("*",cData)+1))
							IF LEN(ALLTRIM(m.gtin_upc))>12
								m.gtin_upc = ALLTRIM(SUBSTR(ALLTRIM(m.gtin_upc),3))
							ENDIF
						CASE cData = "INNERPACK*"
*								ASSERT .F. MESSAGE "In INNERPACK Subdet of "+TRANSFORM(nvodid)+"...>>DEBUG<<"
							IF lScanned
								SELECT ppclone
								LOCATE FOR ppclone.outdetid = vin7pp.outdetid AND ppclone.ID = m.size ;
									AND ppclone.upc = m.gtin_upc AND ppclone.STYLE = vin7pp.STYLE ;
									AND ppclone.COLOR = vin7pp.COLOR ;
									AND ppclone.ucc = vin7pp.ucc
								IF !FOUND()
									WAIT WINDOW "CHECK CODE...PARAM COMBO NOT FOUND!"
									SET STEP ON
								ELSE
									m.shipqty = ppclone.totqty
								ENDIF
								SELECT vin7pp
							ELSE
								m.shipqty = INT(VAL(SUBSTR(cData,AT("*",cData)+1)))
								m.shipqty = m.shipqty/outdet.origqty
							ENDIF
							IF lTesting
								m.ctntype = ""
							ENDIF
							m.tracknum = IIF(lParcelType,cBOL,"")
							m.bolnum = IIF(lParcelType,"",cBOL)
							INSERT INTO template1 FROM MEMVAR
							STORE "" TO m.size,m.sizenum,m.gtin_upc
						ENDCASE
					ENDFOR

				ELSE
					m.sizenum = segmentget(@aptdet,"SIZENUM",alength)
					m.shipqty = IIF(DATETIME()<DATETIME(2016,10,13,13,00,00),vin7pp.totqty,outdet.totqty)
					m.size  = TRIM(outdet.ID)
					m.gtin_upc = TRIM(outdet.upc)

					IF ISNULL(m.tracknum)
						m.tracknum = ""
					ENDIF
					IF ISNULL(CTNTYPE)
					CTNTYPE  = ""
					endif
					INSERT INTO template1 FROM MEMVAR
				ENDIF
				DO WHILE vin7pp.outdetid = nvodid AND vin7pp.ucc = cCartonNum
					SKIP 1 IN vin7pp
				ENDDO
			ENDDO
			lSkipBack = .T.
		ENDDO

*************************************************************************
*2^	END DETAIL MATCHING SHIP_REF LOOP
*************************************************************************
		SELECT outship
		WAIT CLEAR
	ENDSCAN
	WAIT WINDOW "Now updating Template1 records..." NOWAIT NOCLEAR
	SELECT template1
	SUM template1.shipqty TO N
	LOCATE
	IF lTesting
		MESSAGEBOX("Total of Template shipqty: "+ALLT(STR(N)),0,"Template1 Total",8000)
*		WAIT WINDOW "Browsing Template1 records" TIMEOUT 2
	ENDIF
	LOCATE
	IF lScanned
		SCAN
			SCATTER FIELDS ucc128,STYLE,COLOR,SIZE,gtin_upc MEMVAR
			SELECT vin7pp
			LOCATE FOR vin7pp.ucc = TRIM(m.ucc128) ;
				AND vin7pp.STYLE = TRIM(m.style) ;
				AND vin7pp.COLOR = TRIM(m.color) ;
				AND vin7pp.ID = TRIM(m.size) ;
				AND VAL(vin7pp.upc) = VAL(m.gtin_upc)
			IF !FOUND()
				WAIT WINDOW "Record not found..." NOWAIT
				ASSERT .F. MESSAGE "In record not found"
			ENDIF
*			REPLACE template1.shipqty WITH vin7pp.totqty IN template1 NEXT 1
		ENDSCAN
	ENDIF
	WAIT CLEAR
	SELECT template1
	COPY TO c:\tempfox\template1
	LOCATE


*************************************************************************
*1^	END OUTSHIP MAIN LOOP
*************************************************************************
	ASSERT .F. MESSAGE "At end of OUTSHIP loop"
	IF !lTesting
		SELECT edi_trigger
		lDoCatch = .F.
		DO EDIUPDATE WITH .F.,"945 CREATED"
		SELECT edi_trigger
		LOCATE

		IF !USED("ftpedilog")
			SELECT 0
			USE F:\edirouting\ftpedilog ALIAS ftpedilog
			INSERT INTO ftpedilog (TRANSFER,ftpdate,filename,acct_name,TYPE) VALUES ("945-"+ccustname+"-"+cCustLoc,dt2,cFilenameHold,UPPER(ccustname),"945")
			USE IN ftpedilog
		ENDIF
	ENDIF
	closefiles()

	WAIT CLEAR
	IF lStyleError
		cWinmsg = "There were style errors...no 945 will be sent"
	ELSE
		cWinmsg = cCustFolder+" 945 Process complete..."
	ENDIF
	WAIT WINDOW cWinmsg TIMEOUT 2

*!* Create eMail confirmation message

	cOutFolder = "FMI"+cCustLoc

	IF !lStyleError
		tsubject = cMailName+" 945 EDI File from TGF as of "+dtmail+" ("+cCustLoc+")"+CHR(13)
		tattach = " "
		tmessage = "945 EDI Info from TGF, file "+cFilenameShort+CHR(13)
		tmessage = tmessage + "BOL# "+TRIM(cBOL)+CHR(13)
		tmessage = tmessage + "Work Orders: "+cWO_NumStr+","+CHR(13)
		tmessage = tmessage + "containing these picktickets:"+CHR(13)
		tmessage = tmessage + cPTString + CHR(13)
		tmessage = tmessage +"has been created and will be transmitted ASAP."+CHR(13)+CHR(13)
		IF !EMPTY(cMissDel)
			tmessage = tmessage+CHR(13)+CHR(13)+cMissDel+CHR(13)+CHR(13)
		ENDIF
		tmessage = tmessage + "If you have any questions, please eMail or call 732-750-9000 x217."
		IF lEmail
			DO FORM m:\dev\frm\dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
		ENDIF

		cFilenameArchive = UPPER("f:\ftpusers\"+cCustFolder+"\945OUT\945ARCHIVE\"+cCustPrefix+dt1+".txt")

		RELEASE ALL LIKE c_CntrlNum,c_GrpCntrlNum
		WAIT CLEAR
		WAIT WINDOW cMailName+" 945 EDI File output complete" TIMEOUT 1
		
		SET STEP ON 
*!* Transfers files to correct output folders
		ASSERT .F. MESSAGE "At FilesOut Statement...debug"
		SELECT template1
		COPY TO c:\tempfox\template1
		COPY TO [&cFilenameHold] TYPE CSV
		IF lFilesout AND !lTesting && AND lEmail
			COPY FILE [&cFilenameHold] TO [&cFilenameOut]
			COPY FILE [&cFilenameHold] TO [&cFilenameArch]
			DELETE FILE [&cFilenameHold]
			SELECT temp945
			COPY TO "f:\3pl\data\temp945a.dbf"
			USE IN temp945
			SELECT 0
			USE "f:\3pl\data\temp945a.dbf" ALIAS temp945a
			SELECT 0
			USE "f:\3pl\data\pts_sent945.dbf" ALIAS pts_sent945
			IF !lTesting
				APPEND FROM "f:\3pl\data\temp945a.dbf"
			ENDIF
			USE IN pts_sent945
			USE IN temp945a
			DELETE FILE "f:\3pl\data\temp945a.dbf"
		ENDIF
		IF USED('template1')
			USE IN template1
		ENDIF
		cxErrMsg = "945 CREATED"
		DO EDIUPDATE WITH .F.,cxErrMsg

		asn_out_data()

	ELSE
		DELETE FILE &cFilenameHold
		DO EDIUPDATE WITH .T.,cxErrMsg
	ENDIF


CATCH TO oErr
	IF lDoCatch
		ASSERT .F. MESSAGE "In catch...debug"
		SET STEP ON

		tsendto = tsendtoerr
		tcc = tccerr
		tsubject = ccustname+" Error ("+TRANSFORM(oErr.ERRORNO)+") at "+TTOC(DATETIME())
		tattach  = ""

		tmessage = ccustname+" Error processing "+CHR(13)
		tmessage = tmessage+TRIM(PROGRAM())+CHR(13)

		tmessage =tmessage+ "Try/CATCH Exeception Message:"+CHR(13)+;
			[  Error: ] + STR(oErr.ERRORNO) +CHR(13)+;
			[  LineNo: ] + STR(oErr.LINENO) +CHR(13)+;
			[  Message: ] + oErr.MESSAGE +CHR(13)+;
			[  Procedure: ] + oErr.PROCEDURE +CHR(13)+;
			[  Details: ] + oErr.DETAILS +CHR(13)+;
			[  StackLevel: ] + STR(oErr.STACKLEVEL) +CHR(13)+;
			[  LineContents: ] + oErr.LINECONTENTS+CHR(13)+;
			[  UserValue: ] + oErr.USERVALUE
		tmessage =tmessage+CHR(13)+cProgname

		tmessage = tmessage+CHR(13)+"Error Desc: "+cxErrMsg
		tsubject = "945 EDI Poller Error at "+TTOC(DATETIME())
		tattach  = ""
		DO FORM m:\dev\frm\dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
	ENDIF
FINALLY
	SET DELETED ON
	ON ERROR
	closefiles()
ENDTRY

*** END OF CODE BODY

****************************
PROCEDURE EDIUPDATE
****************************
PARAMETER lIsError,cStatus
lDoCatch = .F.
ASSERT .F. MESSAGE "In EDIUPDATE"

IF !lTesting
	SELECT edi_trigger
	nRec = RECNO()
	IF !lIsError
		REPLACE processed WITH .T.,proc945 WITH .T.,file945 WITH cFilenameHold,;
			fin_status WITH "945 CREATED",errorflag WITH .F.,when_proc WITH DATETIME() ;
			FOR edi_trigger.bol = cBOL AND accountid = nAcctNum
	ELSE
		IF cStatus = "GEORGE & MARTHA"
			REPLACE processed WITH .T.,proc945 WITH .F.,file945 WITH "",;
				fin_status WITH "NO 945 NEEDED",errorflag WITH .F.,when_proc WITH DATETIME() ;
				FOR edi_trigger.bol = cBOL AND accountid = nAcctNum
		ELSE
			cTrigStatus = IIF(lStyleError,"STYLE/COLOR ERR",cStatus)
			cComment = IIF(lStyleError,cStatus,"")
			REPLACE processed WITH .T.,proc945 WITH .F.,file945 WITH "",;
				fin_status WITH cTrigStatus,errorflag WITH .T.,comments WITH cComment ;
				FOR edi_trigger.bol = cBOL AND accountid = nAcctNum
			IF lCloseOutput
*				=FCLOSE(nFilenum)
				ERASE &cFilenameHold
			ENDIF
		ENDIF
	ENDIF
ENDIF

IF lIsError
	tsubject = "IN7 945 Process Error "+TTOC(DATETIME())
	tmessage = "Error: "+cStatus
	tattach = " "
	tsendto = tsendtoerr
	tcc = tccerr
	IF !lTesting
		SELECT 0
		USE F:\3pl\DATA\mailmaster ALIAS mm
		LOCATE FOR edi_type = "MISC" AND taskname = "IN7_945ERR"
		lUseAlt = mm.use_alt
		tccadd = ALLTRIM(IIF(lUseAlt,ccalt,cc))
		USE IN mm

		IF TRIM(tccadd) = TRIM(tcc)
			tccadd = ""
		ENDIF

		IF !EMPTY(tccadd)
			IF EMPTY(tcc)
				tcc = tccadd
			ELSE
				tcc = tcc+","+tccadd
			ENDIF
		ENDIF
	ENDIF

	IF lEmail AND cStatus = "SDC: SQL ERROR-LBL"
		tsubject = "945 Error in IN7 BOL "+TRIM(cBOL)+"(At PT "+cShip_ref+")"
		tmessage = "945 Processing for BOL# "+cBOL+", WO# "+cWO_Num+;
			" produced this error: (Office: "+cOffice+"): "+cStatus+CHR(13)+"Check EDI_TRIGGER and re-run"
		IF "TOTQTY ERR"$cStatus
			tmessage = tmessage + CHR(13) + "At OUTSHIPID: "+ALLTRIM(STR(m.outshipid))
		ENDIF
	ENDIF

	IF lStyleError
		ASSERT .F. MESSAGE "At Style/Color error...DEBUG"
		tsubject = "945 Style error(s) in IN7 BOL "+TRIM(cBOL)
		tmessage = "945 Processing for BOL# "+cBOL+", WO# "+cWO_Num+" (office: "+;
			cOffice+") produced these *Missing STYLE/COLOR* errors: "+CHR(13)+CHR(13)+cStatus
	ENDIF

	IF EMPTY(tsubject)
		tsubject = cStatus
	ENDIF
	DO FORM m:\dev\frm\dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
	THROW
ENDIF

IF !lTesting
	SELECT edi_trigger
	LOCATE
ENDIF
ENDPROC

****************************
PROCEDURE segmentget
****************************

PARAMETER thisarray,lcKey,nLength

FOR i = 1 TO nLength
	IF i > nLength
		EXIT
	ENDIF
	lnEnd= AT("*",thisarray[i])
	IF lnEnd > 0
		lcThisKey =TRIM(SUBSTR(thisarray[i],1,lnEnd-1))
		IF OCCURS(lcKey,lcThisKey)>0
			RETURN SUBSTR(thisarray[i],lnEnd+1)
			i = 1
		ENDIF
	ENDIF
ENDFOR

RETURN ""
ENDPROC

****************************
PROCEDURE closefiles
****************************
IF USED('outship')
	USE IN outship
ENDIF
IF USED('outdet')
	USE IN outdet
ENDIF
IF USED('scacs')
	USE IN scacs
ENDIF
IF USED('mm')
	USE IN mm
ENDIF
IF USED('tempx')
	USE IN tempx
ENDIF
IF USED('shipment')
	USE IN shipment
ENDIF
IF USED('package')
	USE IN package
ENDIF
IF USED('clonepp')
	USE IN clonepp
ENDIF
IF USED('parcel_carriers')
	USE IN parcel_carriers
ENDIF
ENDPROC