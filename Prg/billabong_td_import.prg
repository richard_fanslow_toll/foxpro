

utilsetup("BILLABONG_TD_IMPORT")
SET EXCLUSIVE OFF 
SET DELETED ON 
SET TALK OFF  
SET SAFETY OFF


lcpath = 'F:\FTPUSERS\Billabong\worldshipdata\TD\'
lcarchivepath = 'F:\FTPUSERS\Billabong\worldshipdata\TD\archive\'
dropPath = 'W:\TradeDirect\FromTGF\'

** Check if its already used, kill it and map again
IF DIRECTORY("W:\")
	oNet = Createobject("WScript.Network")
	oNet.RemoveNetworkDrive("W:", .T.,.T.)
	Release oNet
Endif

SET STEP ON 

IF !DIRECTORY("W:\") && Doesn't exist - map it
	ONET = CREATEOBJECT("WScript.Network")
	*ONET.MapNetworkDrive(Drive,UNC,Retain,User,Pass) 
	ONET.MapNetworkDrive("W:","\\172.24.55.27\c$",.F.,"172.24.55.27\administrator","T0l1$123")
	IF !DIRECTORY("W:\TradeDirect")
		MD "W:\TradeDirect"
	ENDIF
		IF !DIRECTORY("W:\TradeDirect\FromTGF")
		MD "W:\TradeDirect\FromTGF"
	ENDIF
	
	RELEASE ONET
ENDIF


wait window "HERE NOW" timeout 4
*!*	set step on
*cd [&lcpath]
len1 = adir(ary1,lcpath+"*.*")
if len1 = 0
	wait window "No files found...exiting  NOW" timeout 4
	on error
	return
endif
create cursor bc1 (ship_ref c(20))
for i = 1 to len1
	cfilename = alltrim(ary1[i,1])
	xfile = lcpath+cfilename
	carchivefile = (lcarchivepath+cfilename+ttoc(datetime(),1))
	cloadfile = (lcpath+cfilename)
	copy file [&cLoadFile] to [&cArchiveFile]
	tfile=alltrim(ary1[i,1])
	tfilepath=lcpath+alltrim(ary1[i,1])
	xfilecsv = lcpath+juststem(justfname(xfile))+".csv"
	do excel_to_csv with cfilename, .t.
	tfilepath=xfilecsv
	appe from "&tfilepath" type deli
	delete file  [&xfile]
	delete file  [&xfilecsv]
endfor
select bc1
if reccount() > 0
	delete file s:\billabong\temp\td_import.xls
	export to "S:\Billabong\temp\td_import"   type xls
	tsendto = "tmarg@fmiint.com,todd.margolin@tollgroup.com,doug.wachs@tollgroup.com"
	tattach = "S:\Billabong\temp\td_import.xls"
	tcc =""
	tfrom ="Toll EDI System Operations <fmi-transload-ops@fmiint.com>"
	tmessage = "TD Import files exist:"+ttoc(datetime())
	tsubject = "TD Import files exist"
*DHW*	do form m:\dev\frm\dartmail2 with tsendto,tfrom,tsubject, tcc,tattach,tmessage  ,"A"
else
endif

**************************************  UPLOAD PT'S
select distinct ship_ref from bc1 into cursor bc1 readwrite
delete for empty(ship_ref) in bc1
replace ship_ref with upper(ship_ref) for !empty(ship_ref) in bc1
wait window "HERE NOW2" timeout 4
	useca('outship','wh',,,'select * from outship where inlist(accountid,6718,6719,6722,6744,6745,6746,6747,6748,6749) and del_date is null')
wait window "HERE NOW3" timeout 4
*!*	set step on
select b.* from bc1 a left join outship b on a.ship_ref=b.ship_ref into cursor k1 readwrite
replace scac with 'UTDC' for  inlist(accountid,6718,6719,6722,6744,6745,6746,6747,6748,6749)  and 'CODAMOUNT' $(shipins) and 'TRADE DIRECT' $(ship_via) and scac!='UTDC  '
select distinct ship_ref from k1 where 'CANADALOCATION*M' $(shipins) into cursor vmiss readwrite

select k1
select a.*,labeled from bc1 a left join outship b on a.ship_ref=b.ship_ref into cursor k1 readwrite

select * from k1 where empty(labeled) into cursor k2 readwrite
wait window "HERE NOW3A" timeout 4
********************************TTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT*if reccount() > 0
*	message "not all labeled"
****************************************************gather carton weight from cartons
xsqlexec("select * from cartons where accountid in (6718,6719,6722,6744,6745,6746,6747,6748,6749)",,,"pickpack_sql5")
**useca('CARTONS','pickpack_sql5',,,'select * from cartons where accountid in (6718,6719,6722,6744,6745,6746,6747,6748,6749)')
select distinct ship_ref,ucc,ctnwt from cartons into cursor ctnwt readwrite
select ship_ref, sum(ctnwt) as ctnwt from ctnwt group by 1 into cursor ctnwt2 readwrite
wait window "HERE NOW4" timeout 4
**************************************update weight with cartons weight
****fix COD scac
select * from outship where inlist(accountid,6718,6719,6722,6744,6745,6746,6747,6748,6749) and dydt is null into cursor voutship1 readwrite
select a.*,ctnwt from voutship1 a left join ctnwt2 b on a.ship_ref=b.ship_ref into cursor voutship2 readwrite
replace weight with ctnwt for !isnull(ctnwt) in voutship2

**************************************  FIXING MISSING SCACS

replace scac with 'UTDC' for inlist(accountid,6718,6719,6722,6744,6745,6746,6747,6748,6749) and empty(scac) and 'CODAMOUNT' $(shipins) and 'TRADE DIRECT' $(ship_via)
replace scac with 'USTD' for inlist(accountid,6718,6719,6722,6744,6745,6746,6747,6748,6749) and empty(scac) and 'TRADE DIRECT' $(ship_via)

************************************** IDENTIFY LTL
select a.ship_ref,bol_no,scac from bc1 a left join outship b on a.ship_ref=b.ship_ref into cursor ptbol1 readwrite
select * from ptbol1 where scac='UTTK' into cursor ptbol2 readwrite

select * from voutship2 where alltrim(ship_ref) in (select alltrim(ship_ref) from bc1) into cursor t1 readwrite
select ship_ref as order_number, space(10) as ltl_tllineitemfreightclass, space(10) as ltl_tllineitemlinedescription, ship_ref as packagereference1,;
	space(20) as packagereference2,space(10) as packagereference2a,space(10) as packagereference3, space(10) as packagereference4,address as shiptoaddress1,;
	address2 as shiptoaddress2, space(10)as shiptoaddress3, consignee as shiptoattention, csz as shiptocityortown, consignee as shiptocompanyorname, ;
	'CA' as shiptocountry_territory, csz as shiptopostalcode, csz as shiptostate_province_county, '9497537222' as shiptotelephone,;
	'SHP' as shipmentinformationbillingoption, 'CLOTHING' as shipmentinformationdescriptionofgoods, 'N' as shipmentinformationltl_tlindicator, ;
	'ST' as shipmentinformationservicetype, '9V099V' as shipmentinformationshippernumber, space(10) as shipmentinformationspecialinstructionsforshipment ,;
	'9V099V' as shipfrom_ship_ups_acct_no, 'PACKAGE' as package_pkg_type, weight as package_weight, 'N' as ;
	package_cod_opt, 00000.00 as package_cod_amt, 'N' as packagecashierscheck_moneyorderonlyindicator,;
	'USD' as intdoccurrency, shipins from t1 into cursor t2 readwrite
wait window "HERE NOW5" timeout 4
****end job and email
************************************** IDENTIFY MISSISSAUGA SHIPMENTS
replace shipfrom_ship_ups_acct_no with '9V1A52' for 'CANADALOCATION*C' $(shipins) in t2
replace shipmentinformationshippernumber with '9V1A52' for 'CANADALOCATION*C' $(shipins) in t2
replace all packagereference2 with 'EL '+getmemodata("shipins","SALESORDER") in t2
replace all package_cod_opt with 'Y' for  'CODFLAG*YES' $(shipins) in t2
replace all package_cod_amt with val(getmemodata("shipins","CODCHARGE")) in t2
replace all packagecashierscheck_moneyorderonlyindicator with 'Y' for  'FUNDS*S' $(shipins) in t2

scan
	pos1=atc(',',shiptocityortown)
	replace shiptocityortown with substr(shiptocityortown,1,pos1-1)
endscan
scan
	pos1=atc(',',shiptostate_province_county)
	replace shiptostate_province_county with substr(shiptostate_province_county,pos1+2,2)
endscan
scan
	pos1=atc(',',shiptopostalcode)
	replace shiptopostalcode with substr(shiptopostalcode,pos1+5,7)
endscan
wait window "HERE NOW6" timeout 4
****	%%%%%%%%%%%%%%%%%%%%%%%%%%%%% start ltl process
select a.ship_ref,bol_no,scac from bc1 a left join outship b on a.ship_ref=b.ship_ref into cursor ptbol1 readwrite
select * from ptbol1 where scac='UTTK' into cursor ptbol2 readwrite
*****Check for BOL populated for LTL
select ptbol2
if reccount() > 0
	select * from t2 a left join ptbol2 b on a.order_number==b.ship_ref into cursor t2a readwrite
	replace order_number with bol_no, packagereference1 with bol_no, shipmentinformationltl_tlindicator with 'Y' for !isnull(bol_no) in t2a
	select order_number ,ltl_tllineitemfreightclass ,ltl_tllineitemlinedescription ,packagereference1 ,packagereference2 ,packagereference2a ,;
		packagereference3 ,packagereference4 ,shiptoaddress1 ,shiptoaddress2 ,shiptoaddress3 ,shiptoattention ,shiptocityortown ,;
		shiptocompanyorname ,shiptocountry_territory ,shiptopostalcode ,shiptostate_province_county ,shiptotelephone ,;
		shipmentinformationbillingoption ,shipmentinformationdescriptionofgoods ,shipmentinformationltl_tlindicator ,;
		shipmentinformationservicetype ,shipmentinformationshippernumber ,shipmentinformationspecialinstructionsforshipment ,;
		shipfrom_ship_ups_acct_no ,package_pkg_type ,package_weight ,package_cod_opt ,package_cod_amt ,packagecashierscheck_moneyorderonlyindicator ,;
		intdoccurrency from t2a into cursor t2b readwrite
	replace packagereference2 with substr(packagereference2,1,2) in t2b for shipmentinformationltl_tlindicator='Y'
	select order_number ,ltl_tllineitemfreightclass ,ltl_tllineitemlinedescription ,packagereference1 ,packagereference2 ,packagereference2a ,;
		packagereference3 ,packagereference4 ,shiptoaddress1 ,shiptoaddress2 ,shiptoaddress3 ,shiptoattention ,shiptocityortown ,;
		shiptocompanyorname ,shiptocountry_territory ,shiptopostalcode ,shiptostate_province_county ,shiptotelephone ,;
		shipmentinformationbillingoption ,shipmentinformationdescriptionofgoods ,shipmentinformationltl_tlindicator ,;
		shipmentinformationservicetype ,shipmentinformationshippernumber ,shipmentinformationspecialinstructionsforshipment ,;
		shipfrom_ship_ups_acct_no ,package_pkg_type ,sum(package_weight) as package_weight ,package_cod_opt ,package_cod_amt ,packagecashierscheck_moneyorderonlyindicator ,;
		intdoccurrency from t2b group by order_number ,ltl_tllineitemfreightclass ,ltl_tllineitemlinedescription ,packagereference1 ,packagereference2 ,packagereference2a ,;
		packagereference3 ,packagereference4 ,shiptoaddress1 ,shiptoaddress2 ,shiptoaddress3 ,shiptoattention ,shiptocityortown ,;
		shiptocompanyorname ,shiptocountry_territory ,shiptopostalcode ,shiptostate_province_county ,shiptotelephone ,;
		shipmentinformationbillingoption ,shipmentinformationdescriptionofgoods ,shipmentinformationltl_tlindicator ,;
		shipmentinformationservicetype ,shipmentinformationshippernumber ,shipmentinformationspecialinstructionsforshipment ,;
		shipfrom_ship_ups_acct_no ,package_pkg_type ,package_cod_opt ,package_cod_amt ,packagecashierscheck_moneyorderonlyindicator ,;
		intdoccurrency  into cursor t2c readwrite
	wait window "HERE NOW7" timeout 4
	select * from t2c where shipfrom_ship_ups_acct_no='9V1A52' into cursor vcalgary readwrite
	select t2c
	delete for shipfrom_ship_ups_acct_no='9V1A52'
	
	SET STEP ON && Copy 1
	
	copy to f:\ftpusers\billabong\worldshipdata\td\import\miss\bbg_import_final.csv type csv
	dropFile=dropPath+"MISS\bbg_import_final.csv"
	COPY TO [&dropFile] TYPE CSV 
	select vcalgary

	if reccount() > 0
	
	SET STEP ON && Copy 1
	

		copy to f:\ftpusers\billabong\worldshipdata\td\import\calg\bbg_import_final.csv type csv
		dropFile=dropPath+"CALG\bbg_import_final.csv"
		COPY TO [&dropFile] TYPE CSV 
		select * from vcalgary into cursor vcalgary2 readwrite
	ELSE
		
	SET STEP ON && Copy 1
	

		select * from t2 where shipfrom_ship_ups_acct_no='9V1A52' into cursor vcalgary2 readwrite
		select t2
		delete for shipfrom_ship_ups_acct_no='9V1A52'
		copy to f:\ftpusers\billabong\worldshipdata\td\import\miss\bbg_import_final.csv type csv
		dropFile=dropPath+"MISS\bbg_import_final.csv"
		COPY TO [&dropFile] TYPE CSV 
		select vcalgary2
		if reccount() > 0
			copy to f:\ftpusers\billabong\worldshipdata\td\import\calg\bbg_import_final.csv type csv
				dropFile=dropPath+"CALG\bbg_import_final.csv"
	COPY TO [&dropFile] TYPE CSV 
		endif
	endif

ELSE
	
	SET STEP ON && Copy 1
	

	select * from t2 where shipfrom_ship_ups_acct_no='9V1A52' into cursor vcalgary readwrite
	select vcalgary

	if reccount() > 0

		copy to f:\ftpusers\billabong\worldshipdata\td\import\calg\bbg_import_final.csv type csv
			dropFile=dropPath+"CALG\bbg_import_final.csv"
	COPY TO [&dropFile] TYPE CSV 
***		select * from vcalgary into cursor vcalgary2 readwrite
		select * from t2 where shipfrom_ship_ups_acct_no='9V1A52' into cursor vcalgary2 readwrite
		select t2
		delete for shipfrom_ship_ups_acct_no='9V1A52'
		if reccount() > 0
			copy to f:\ftpusers\billabong\worldshipdata\td\import\miss\bbg_import_final.csv type csv
				dropFile=dropPath+"MISS\bbg_import_final.csv"
	COPY TO [&dropFile] TYPE CSV 
		else
***EMAIL no miss
		endif
	ELSE
		*** This will create an empty vcalgary2 cursor just so that it exists later -DHW
		select * from t2 where shipfrom_ship_ups_acct_no='9V1A52' into cursor vcalgary2 readwrite
		******************************************************************************************

		select t2
		if reccount() > 0
			copy to f:\ftpusers\billabong\worldshipdata\td\import\miss\bbg_import_final.csv type csv
				dropFile=dropPath+"MISS\bbg_import_final.csv"
	COPY TO [&dropFile] TYPE CSV 
		else
***EMAIL no miss
		endif
***EMAIL no calg
*!*			select vcalgary2
*!*			if reccount() > 0
*!*				copy to F:\FTPUSERS\Billabong\worldshipdata\TD\import\calg\bbg_import_final.csv type csv
*!*			endif
	endif
endif
***************************************************** START COMMODITY FILE

useca('outdet','wh',,,'select * from outdet where inlist(accountid,6718,6719,6722,6744,6745,6746,6747,6748,6749) ')

useca('upcmast','wh',,,'select * from upcmast where accountid in (6718,6744,6747)')
select distinct ship_ref from t1 into cursor t1 readwrite
select a.*,outshipid,shipins from t1 a left join outship b on a.ship_ref=b.ship_ref into cursor bc2 readwrite
select a.ship_ref,shipins, b.* from bc2 a left join outdet b on a.outshipid=b.outshipid into cursor bc3 readwrite
select ship_ref,style,color,id, sum(totqty) as qty from bc3 where units group by ship_ref,style,color,id into cursor bc4 readwrite
select a.*,b.coo,info from bc4 a left join upcmast b on a.style=b.style and a.color=b.color and a.id=b.id into cursor bc5 readwrite
select bc5.*,space(100) as nafta,'NMB' as uom,space(12) as htsca,'USD' as currency from bc5  into cursor bc6 readwrite
select a.*,price from bc6 a left join upcmast b on a.style=b.style and a.color=b.color and a.id=b.id into cursor bc7 readwrite

select bc7
scan
	if '-' $(style)
		pos1=atc('-',style)
		replace style with substr(style,1,pos1-1)
	endif
endscan

replace all nafta with getmemodata("info","NAFTA")
replace all htsca with getmemodata("info","HTSCA")
select ship_ref,style,color,sum(qty) as qty,coo,nafta,uom,htsca,currency,price from bc7 group by ship_ref,style,color,coo,nafta,uom,htsca,currency , price into cursor bc8 readwrite
select ship_ref as order_number, nafta as goodsdescriptionofgood, htsca as goodsinv_nafta_cn22tariffcode, coo as goodsinv_nafta_co_cn22country_territoryoforigin, ;
	uom as goodsinvoice_cn22unitofmeasure, qty as goodsinvoice_cn22units, price as goodsinvoice_eei_cn22unitprice,alltrim(style)+alltrim(color) as goodspartnumber, currency as	goodscurrency ;
	from bc8 into cursor bc9 readwrite
replace goodsinv_nafta_cn22tariffcode with '9999999999'     for goodsinv_nafta_cn22tariffcode= '0000000000' in bc9
replace goodsinv_nafta_cn22tariffcode with '9999999999'     for goodsinv_nafta_cn22tariffcode= 'NA' in bc9


***** LTL check and conversion of PT to BOL
select ptbol2
if reccount() > 0
	select ptbol2
	scan
		replace order_number with ptbol2.bol_no for order_number=ptbol2.ship_ref in bc9
	endscan
endif
SET STEP ON && Copy
SELECT vcalgary2

select distinct order_number from vcalgary2 into cursor vcalgary3 readwrite
select b.* from vcalgary3 a left join bc9 b on a.order_number=b.order_number into cursor vcalcommod readwrite
select vcalcommod
if reccount() > 0
	copy to f:\ftpusers\billabong\worldshipdata\td\import\calg\bbg_import_commod.csv type csv
	dropFile=dropPath+"CALG\bbg_import_commod.csv"
	COPY TO [&dropFile] TYPE CSV 
	select bc9
	delete from bc9 where order_number in (select order_number from vcalgary3)
endif
select bc9
if reccount() > 0
   	copy to f:\ftpusers\billabong\worldshipdata\td\import\miss\bbg_import_commod.csv type csv
   		dropFile=dropPath+"MISS\bbg_import_commod.csv"
	COPY TO [&dropFile] TYPE CSV 
else
endif

***************************************************** START PACKAGE FILE

select  distinct ship_ref,substr(ucc,17,4) as vucc,ucc,ctnwt as weight from cartons  into cursor bbg1 readwrite
select distinct ship_ref from bc5 into cursor p1 readwrite
select a.ship_ref, vucc ,ucc,weight from p1 a left join bbg1 b on a.ship_ref=b.ship_ref into cursor p2 readwrite


******************check foro isnull weight  send message and stop process


select * from p2 a left join outship b on a.ship_ref=b.ship_ref into cursor p3 readwrite
select ship_ref_a as ship_ref, alltrim(ship_ref_a)+" "+vucc as ref1,space(20) as ref2, ucc as ref3,space(5) as ref4,space(5) as ref5,;
	"package" as pkg,weight_a as weight, 'N' as codopt, 0 as codamt, 'N' as money,shipins from p3  into cursor p4 readwrite
replace all ref2 with 'EL '+getmemodata("shipins","SALESORDER")
replace ref2 with getmemodata("shipins","ACD")+' '+getmemodata("shipins","SALESORDER") for 'ACD' $(shipins)


***** LTL check and conversion of PT to BOL
select ptbol2
if reccount() > 0
	select ptbol2
	scan
		replace weight with 0  for isnull(weight) and ship_ref=ptbol2.ship_ref in p4
		replace ship_ref with ptbol2.bol_no, ref1 with ptbol2.bol_no for ship_ref=ptbol2.ship_ref in p4

	endscan
endif


select * from p4 where   'CANADALOCATION*C' $(shipins) into cursor vcalg readwrite
delete from p4 where  'CANADALOCATION*C' $(shipins)
select p4
SET STEP ON && copy

if reccount() > 0
	copy to f:\ftpusers\billabong\worldshipdata\td\import\miss\bbg_import_package.csv type csv
		dropFile=dropPath+"MISS\bbg_import_package.csv"
	COPY TO [&dropFile] TYPE CSV 
endif
select vcalg
if reccount() > 0
	copy to f:\ftpusers\billabong\worldshipdata\td\import\calg\bbg_import_package.csv type csv
		dropFile=dropPath+"CALG\bbg_import_package.csv"
	COPY TO [&dropFile] TYPE CSV 
endif
**************************************************  POPULATE OUTSHIP RECORDS AS BEING PROCESSED
set step on
select bc1
if reccount() > 0

	tsendto = "doug.wachs@tollgroup.com,ariana.andrade@tollgroup.com,joni.golding@tollgroup.com"
	tattach = ""
	tcc =""
	tfrom ="Toll EDI System Operations <fmi-transload-ops@fmiint.com>"
	tmessage = "TD Import files exist:"+ttoc(datetime())
	tsubject = "TD Import files exist - please confirm dates of files!!!!!!"
 	do form m:\dev\frm\dartmail2 with tsendto,tfrom,tsubject, tcc,tattach,tmessage  ,"A"
else
endif

select bc1
scan
	select outship
	locate for ship_ref=bc1.ship_ref
	if found("outship")
		replace dydt with date()  in outship
	else
	endif
ENDSCAN
wait window "HERE NOW8" timeout 4
tu('outship')



IF DIRECTORY("W:\")
	oNet = Createobject("WScript.Network")
	oNet.RemoveNetworkDrive("W:", .T.,.T.)
	Release oNet
Endif