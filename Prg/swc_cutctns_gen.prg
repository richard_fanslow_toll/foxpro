PARAMETERS cBOLin
WAIT WINDOW "Now creating SWC Records cursor for BOL "+cBOLin NOWAIT
CREATE CURSOR cutuccsin (ucc c(20))  && Temp cursor

*!*	IF USED('swcdet')
*!*		USE IN swcdet
*!*	ENDIF
*!*	IF USED('swcstyle')
*!*		USE IN swcstyle
*!*	ENDIF

*!*	USE (cUseFolder+"swcdet") IN 0 ALIAS swcdet
*!*	USE (cUseFolder+"swcstyle") IN 0 ALIAS swcstyle

*!*	SELECT * ;
*!*		FROM swcdet,swcstyle ;
*!*		WHERE swcdet.bol_no = cBOLin ;
*!*		AND swcdet.swcdetid = swcstyle.swcdetid ;
*!*		INTO CURSOR swcs READWRITE
*!*	USE IN swcdet
*!*	USE IN swcstyle

gMasterOffice = cOffice
gOffice = cMod

xsqlexec("select * from swcdet where bol_no='"+cbolin+"'","xswcdet",,"wh")

if reccount("xswcdet")>0
	xjfilter="swcdetid in ("
	select xswcdet
	scan 	
		xjfilter=xjfilter+transform(swcdetid)+","
	endscan
	xjfilter=left(xjfilter,len(xjfilter)-1)+")"
else
	xjfilter="1=0"
endif

xsqlexec("select * from swcstyle where "+xjfilter,,,"wh")

select * from xswcdet,swcstyle where xswcdet.swcdetid = swcstyle.swcdetid into cursor swcs readwrite

*browse last for !empty(uccs)

*

SELECT swcs

LOCATE
SCAN
	xx= MEMLINES(uccs)
	IF xx >= 1
		ALINES(aa,uccs,1)
		FOR i =1 TO ALEN(aa,1)
			INSERT INTO cutuccsin (ucc) VALUES (aa[i])
		ENDFOR
	ENDIF
ENDSCAN

SELECT * ;
	FROM cutuccsin ;
	WHERE ISDIGIT(ucc) ;
	GROUP BY ucc ;
	INTO CURSOR cutuccs READWRITE

nCutUCCs = RECCOUNT()
SELECT cutuccs
SCAN
	IF LEN(ALLT(cutuccs.ucc))<20
		REPLACE cutuccs.ucc WITH PADL(ALLT(cutuccs.ucc),20,"0")
	ENDIF
ENDSCAN

LOCATE
INDEX ON ucc TAG ucc
LOCATE
IF !EOF()
	WAIT WINDOW "Now cutting UCCs for BOL "+cBOLin NOWAIT
	DO "m:\dev\prg\main_removectns-sql" WITH cOffice
ELSE
	WAIT WINDOW "No UCCs to cut for BOL "+cBOLin NOWAIT
ENDIF
USE IN cutuccsin
WAIT WINDOW "SWC Records cursor for BOL "+cBOLin+"...all processed." NOWAIT

RETURN
