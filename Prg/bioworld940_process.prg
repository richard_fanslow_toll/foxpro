*!* m:\dev\prg\bioworld940_process.prg
WAIT WINDOW "Now in PROCESS Phase..." NOWAIT

SET DEFAULT  TO &lcPath
lc997Path = 'F:\FTPUSERS\Bioworld\997IN\'

LogCommentStr = "LOADED"

lnNum = ADIR(tarray,"*.*")

IF lnNum = 0
	WAIT WINDOW "No "+cCustname+" 940s to import" TIMEOUT 3
	NormalExit = .T.
	THROW
ENDIF

ASSERT .F. MESSAGE "At initial input file loop"

FOR thisfile = 1  TO lnNum
	Xfile = lcPath+tarray[thisfile,1] && +"."
	cfilename = ALLTRIM(LOWER(tarray[thisfile,1]))
	IF LEFT(cfilename,3) = "FOX"
		DELETE FILE [&xfile]
		LOOP
	ENDIF
	nFileSize = VAL(TRANSFORM(tarray(thisfile,2)))
	IF cfilename = "toll_997"
		WAIT WINDOW "Moving 997 file..." NOWAIT
		c997file = lc997Path+cfilename
		COPY FILE [&xfile] TO [&c997file]
		DELETE FILE [&xfile]
		LOOP
	ENDIF
	WAIT WINDOW "" &message_timeout2
	ArchiveFile  = (lcArchivePath+cfilename)

	WAIT WINDOW "Importing file: "+cfilename &message_timeout2

	lcStr = FILETOSTR(Xfile)
	cDelimiter = SUBSTR(lcStr,4,1)  && they are sending us either ^ or *
	RELEASE lcStr
	cTranslateOption = IIF(cDelimiter="^","TILDE","85H")
	DO m:\dev\prg\loadedifile WITH Xfile,cDelimiter,cTranslateOption,cCustname

	SELECT x856
	LOCATE FOR x856.segment = "GS" AND x856.f1 = "RG"
	IF FOUND() && If a 754
		c754XferFile = ("F:\FTPUSERS\Bioworld\754in\"+cfilename)
		COPY FILE [&xfile] TO [&c754XferFile]  && Moves 754 files to the correct folder
		DELETE FILE [&xfile]
		RELEASE c754XferFile
		LOOP
	ENDIF

*!* Added this block to reassign office/mod for ML overflow inventory, 02.02.2018, Joe
	SELECT x856
	locate
	LOCATE FOR TRIM(x856.segment) = "N1" AND TRIM(x856.f1)="SF"
	IF ALLTRIM(x856.f4) = "MIRA-LOMA"
		cOffice = "L"
		cMod = "L"
		gMasterOffice = cOffice
		gOffice = cMod
		m.office = cOffice
		m.mod = cMod
	ENDIF
*!* End added block

	c940XferFile = ("f:\ftpusers\bioworld\940xfer\"+cfilename)
	COPY FILE [&xfile] TO [&c940XferFile]  && Creates a copy for 997s to run from

	DO create_pt_cursors WITH cUseFolder,lLoadSQL && Added to prep for SQL Server transition

	DO ("m:\dev\prg\"+cCustname+"940_bkdn")

	DO ("m:\dev\prg\all940_import")

NEXT thisfile

&& now clean up the 940in archive files, delete for the upto the last 10 days
deletefile(lcArchivePath,30)

WAIT CLEAR

WAIT "Entire "+cCustname+" 940 Process Now Complete" WINDOW &message_timeout2
NormalExit = .T.
