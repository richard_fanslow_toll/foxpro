*!* This program will be run every day against existing data.

PUBLIC lTesting,lFilesOut,lOverridebusy

lTesting = .f.
lFilesOut = !lTesting
lOverridebusy = .T.

DO m:\dev\prg\_setvars WITH lTesting
CLOSE DATA ALL
CLEAR
SET CENTURY ON
*SET DATE YMD
SET HOURS TO 24
RELEASE ALL LIKE m.*
thiscaption = "BIOWORLD 850 PROCESS"
*_screen.WindowState = IIF(lTesting,2,1)
_SCREEN.CAPTION = thiscaption
xtOrder='0000027878'

xindir="f:\ftpusers\bioworld\850in\"
xoutdir="f:\ftpusers\bioworld\850out-toll\"
xarchdir="f:\ftpusers\bioworld\850out-toll\archive\"
xholddir="f:\ftpusers\bioworld\850hold\"

CD &xindir
xfilecnt=ADIR(afiles,xindir+"*.csv")
IF xfilecnt=0
	WAIT WINDOW "No files to process...exiting" TIMEOUT 3
	RETURN
ENDIF

SELECT 0
USE F:\edirouting\ftpsetup SHARED
LOCATE FOR ftpsetup.transfer = "850-BIOWORLD"
IF ftpsetup.chkbusy AND !lTesting AND lFilesOut AND !lOverridebusy
	WAIT WINDOW "Process busy...exiting" TIMEOUT 3
	CLOSE DATA ALL
	CANCEL
ENDIF
REPLACE ftpsetup.chkbusy WITH .T. FOR ftpsetup.transfer = "850-BIOWORLD"
USE IN ftpsetup

OPEN DATABASE F:\3pl\DATA\bioworld.DBC
USE bioworld!bioworld EXCLUSIVE
ZAP
USE bioworld!bioworld SHARED
USE bioworld!bioworld_fieldnum IN 0 ALIAS bfields SHARED
USE bioworld!bioworld_comp_struc IN 0 ALIAS bcs SHARED
USE bioworld!bioworldvendors IN 0 ALIAS vendors SHARED
USE bioworld!bioworldcustomers IN 0 ALIAS customers SHARED
SELECT 0
USE F:\3pl\DATA\temporderlines ALIAS temporderlines EXCLUSIVE
ZAP
USE F:\3pl\DATA\temporderlines ALIAS temporderlines SHARED

SELECT 0
USE bioworld!bioworld_archive ALIAS barchive SHARED
REPLACE barchive.filercvddt WITH DATE() FOR EMPTY(filercvddt) IN barchive

SELECT * FROM bioworld_comp_struc WHERE .F. INTO CURSOR tempbcs READW
USE IN bcs
LOCATE
APPEND BLANK
AFIELDS(ary1,'tempbcs')
len1 = ALEN(ary1,1)
FOR i = 1 TO len1
	IF ary1[i,2] = "C"
		cFieldname = ALLTRIM(ary1[i,1])
	ENDIF
	REPLACE tempbcs.&cFieldname WITH LOWER(cFieldname) IN tempbcs
ENDFOR
LOCATE

CREATE CURSOR xfilelist (FILE c(50), x c(10), moddt c(8), modtm c(10), moddttm T(1))
APPEND FROM ARRAY afiles
REPLACE ALL moddttm WITH CTOT(moddt+" "+modtm)

SELECT * FROM xfilelist ORDER BY moddttm INTO CURSOR xfilelist
LOCATE

xtollcustcode="BIOWORLD"
xtollorderstatus = "PLC"
xtolltransportcode = "SEA"
xtolldelivport = "USLAX"
xtollusedate = "2100-12-31"

xfilename=xindir+ALLTRIM(xfilelist.FILE)
xarchiveinfile=xindir+"archive\"+ALLTRIM(xfilelist.FILE)
xholdfile=xholddir+ALLTRIM(xfilelist.FILE)
*SET STEP ON
COPY FILE [&xfilename] TO [&xarchiveinfile]


SELECT bioworld
APPEND FROM "&xfilename" TYPE CSV
DELETE FOR ISALPHA(bioworld.line_no)
REPLACE date1 WITH LEFT(ALLTRIM(date1),AT(" ",date1)-1) ALL
REPLACE date2 WITH LEFT(ALLTRIM(date2),AT(" ",date2)-1) ALL
REPLACE exw_required_by_in WITH LEFT(ALLTRIM(exw_required_by_in),AT(" ",exw_required_by_in)-1) ALL
REPLACE order_date_time_in WITH LEFT(ALLTRIM(order_date_time_in),AT(" ",order_date_time_in)-1) ALL
LOCATE
IF lTesting
	LOCATE
*	BROW FOR order_no = xtOrder
ENDIF
REPLACE required_date WITH CTOD(date1) ALL
REPLACE line_custom_attrib3 WITH CTOD(date2) ALL
REPLACE exw_required_by WITH CTOD(exw_required_by_in) ALL
REPLACE order_date_time WITH CTOD(order_date_time_in) ALL

SET DATE YMD
IF lTesting
	LOCATE
*	BROW FOR order_no = xtOrder
ENDIF

AFIELDS(ary1,'bioworld')
len1 = ALEN(ary1,1)
FOR i = 1 TO len1
	cFieldname = ALLTRIM(ary1[i,1])
	IF ary1[i,2] = "C"
		REPLACE bioworld.&cFieldname WITH UPPER(bioworld.&cFieldname) IN bioworld ALL
	ENDIF
ENDFOR
RELEASE ary1

SELECT barchive
WAIT WINDOW "Count of order lines in archive file: "+TRANSFORM(RECCOUNT('barchive')) TIMEOUT 2
LOCATE
AFIELDS(ary1,'barchive')
len1 = ALEN(ary1,1)
FOR i = 1 TO len1
	cFieldname = ALLTRIM(ary1[i,1])
	IF ary1[i,2] = "C"
		REPLACE barchive.&cFieldname WITH UPPER(barchive.&cFieldname) IN barchive ALL
	ENDIF
ENDFOR
RELEASE ary1

AFIELDS(aryarch,'barchive')
lenarch = ALEN(aryarch,1)

AFIELDS(arycurr,'bioworld')
lencurr = ALEN(arycurr,1)

SELECT bioworld
WAIT WINDOW "Count of order lines in new file: "+TRANSFORM(RECCOUNT('bioworld')) TIMEOUT 2
nAdded = 0
nFound = 0
nChanged = 0

SELECT barchive
nCxl = 0
SCAN FOR EMPTY(barchive.cxldt)
	cOrder = ALLTRIM(barchive.order_no)
	IF cOrder = xtOrder
*		SET STEP ON
	ENDIF
	cLine_No = PADL(ALLTRIM(barchive.line_no),3,'0')
	cOrderLine = cOrder+cLine_No
	IF !SEEK(cOrderLine,'bioworld','orderline')
		WAIT WINDOW "Inserting ORDER "+LEFT(cOrderLine,10)+" into temporderlines" NOWAIT
		nCxl = nCxl+1
		INSERT INTO temporderlines (orderline,order_status) VALUES (cOrderLine,"CAN")
		SCATTER MEMVAR
		m.order_status = "CAL" && Preliminary, "Cancel-Line Item"
		INSERT INTO bioworld FROM MEMVAR
	ENDIF
ENDSCAN
WAIT WINDOW "There were "+TRANSFORM(nCxl)+" line items cancelled" TIMEOUT 2

RELEASE ALL LIKE m.*
RELEASE nCxl

SELECT bioworld
SET ORDER TO TAG orderline

SELECT LEFT(temporderlines.orderline,10) AS order_no FROM temporderlines WHERE order_status = "CAN" GROUP BY 1 INTO CURSOR tempcancel
SELECT tempcancel
LOCATE
IF lTesting
*BROWSE FOR order_no = xtorder
ENDIF


SCAN
	cOrder = ALLTRIM(tempcancel.order_no)
	SELECT bioworld
	LOCATE FOR ALLTRIM(bioworld.order_no) = cOrder
	IF !FOUND()
		WAIT WINDOW "Orderline "+cOrderLine+" not found in Bioworld file"
	ENDIF
	COUNT TO nOrd FOR ALLTRIM(bioworld.order_no) = cOrder
	COUNT TO nCan FOR ALLTRIM(bioworld.order_no) = cOrder AND bioworld.order_status = "CAL"
	IF nOrd = nCan
		WAIT WINDOW "Now fully cancelling PO# "+cOrder NOWAIT
		REPLACE bioworld.order_status WITH "CAN" FOR ALLTRIM(bioworld.order_no) = cOrder IN bioworld
		REPLACE barchive.cxldt WITH DATE() FOR barchive.order_no = cOrder AND EMPTY(barchive.cxldt) IN barchive
	ELSE
		WAIT WINDOW "Now partially cancelling PO# "+cOrder NOWAIT
		REPLACE bioworld.order_status WITH "PLC" FOR ALLTRIM(bioworld.order_no) = cOrder AND EMPTY(bioworld.order_status) IN bioworld
		DELETE FOR ALLTRIM(bioworld.order_no) = cOrder AND bioworld.order_status = "CAL"
	ENDIF
ENDSCAN
USE IN tempcancel

SELECT bioworld
SCAN FOR bioworld.order_status = "CAN"
	cOrder =  bioworld.order_no
	cLine_No = PADL(ALLTRIM(bioworld.line_no),3,'0')
	cOrderLine = cOrder+cLine_No
	IF SEEK(cOrderLine,'barchive','orderline')
		REPLACE barchive.cxldt WITH DATE() NEXT 1 IN barchive
	ENDIF
ENDSCAN
LOCATE

*ASSERT .F. MESSAGE "At Bioworld file scan...debug"
SELECT bioworld
IF lTesting
*	WAIT WINDOW "Now at bioworld/archive cross-scan" TIMEOUT 2
*	BROWSE FOR bioworld.order_no = xtOrder
ENDIF
SET ORDER TO TAG orderline
LOCATE
nRecsScanned = 0
SCAN
	nRecsScanned = nRecsScanned+1
	WAIT WINDOW "At record "+TRANSFORM(nRecsScanned) NOWAIT
	SCATTER MEMVAR FIELDS EXCEPT order_status
	IF UPPER(ALLTRIM(m.order_no)) = "ORDER_NO" OR bioworld.order_status = "CAN"
		LOOP
	ENDIF
	cTransType = "X"
	cOrder = ALLTRIM(bioworld.order_no)
	cLine_No = PADL(ALLTRIM(bioworld.line_no),3,'0')
	cOrderLine = cOrder+cLine_No

	IF lTesting
		IF cOrder= xtOrder
			SET STEP ON
		ENDIF
	ENDIF

	IF !SEEK(cOrderLine,'barchive','orderline')
		nAdded = nAdded+1
		REPLACE bioworld.order_status WITH "PLC" IN bioworld NEXT 1
		INSERT INTO temporderlines (orderline,order_status) VALUES (cOrderLine,"ADD")
		STORE DATE() TO m.adddt,m.filercvdt
		INSERT INTO barchive FROM MEMVAR
		RELEASE m.adddt,m.filercvdt
		LOOP
	ELSE
		REPLACE bioworld.order_status WITH "PLC" IN bioworld
		INSERT INTO temporderlines (orderline,order_status,fieldname) VALUES (cOrderLine,"CHG",cFieldname)
	ENDIF

	SELECT orderline FROM temporderlines WHERE INLIST(temporderlines.order_status,"ADD","CHG") INTO CURSOR tempq
	SELECT tempq
	SCAN
		cOrderLine = ALLT(tempq.orderline)
		IF LEFT(cOrderLine,10) = "0000026309"
			SET STEP ON
		ENDIF
		SELECT bioworld
		LOCATE FOR ALLTRIM(bioworld.order_no) = LEFT(cOrderLine,10)
		IF !FOUND()
			WAIT WINDOW "Tempq.orderline "+cOrderLine+" not found in Bioworld file"
		ENDIF
		SCAN
			REPLACE bioworld.order_status WITH "PLC" FOR ALLTRIM(bioworld.order_no) = LEFT(cOrderLine,10) IN bioworld
		ENDSCAN
	ENDSCAN
	USE IN tempq
	SELECT bioworld
ENDSCAN

SELECT bioworld
IF !lFilesOut
*	BROWSE FOR order_no = xtorder
ENDIF
REPLACE bioworld.order_status WITH "PLC" FOR (EMPTY(bioworld.order_status) OR ISDIGIT(bioworld.order_status)) IN bioworld
LOCATE
SELECT DISTINCT order_no ;
	FROM bioworld ;
	WHERE order_status = "PLC" ;
	INTO CURSOR tempbx
SELECT tempbx
LOCATE
SCAN
	IF SEEK(tempbx.order_no,'bioworld','order_no')
		REPLACE bioworld.order_status WITH "PLC" FOR bioworld.order_no = tempbx.order_no IN bioworld
	ENDIF
ENDSCAN


WAIT WINDOW "Records found: "+TRANSFORM(nFound) TIMEOUT 3
WAIT WINDOW "Records added: "+TRANSFORM(nAdded) TIMEOUT 3
WAIT WINDOW "Records changed: "+TRANSFORM(nChanged) TIMEOUT 3
SELECT bioworld
COUNT TO N FOR EMPTY(bioworld.order_status)
DELETE FOR EMPTY(bioworld.order_status)
COUNT TO N FOR !EMPTY(bioworld.order_status)
WAIT WINDOW "Remaining records (completed order_status) in Bioworld file: "+TRANSFORM(N) TIMEOUT 5
IF lTesting
BROW FOR order_no = xtOrder
ENDIF

SELECT tempbcs
APPEND FROM bioworld
LOCATE
IF lTesting
	BROW FOR order_no = xtOrder
ENDIF

*COUNT TO N FOR INLIST(tempbcs.PRODUCT,"SCREENPRINTCHARGE","PACKANDLABOR","MISC ITEM","MISCITEM")
*WAIT WINDOW "Deleting "+TRANSFORM(N)+" unnecesary line items" TIMEOUT 3
*WAIT WINDOW "Deleting "+TRANSFORM(N)+" unnecesary line items" NOWAIT
*DELETE FOR INLIST(tempbcs.PRODUCT,"SCREENPRINTCHARGE","PACKANDLABOR","MISC ITEM","MISCITEM")

LOCATE
REPLACE tempbcs.order_no WITH TRANSFORM(INT(VAL(order_no))) FOR RECNO()>1 IN tempbcs
REPLACE tempbcs.transport_mode WITH xtolltransportcode FOR RECNO()>1 IN tempbcs

REPLACE tempbcs.exw_required_by WITH STRTRAN(tempbcs.exw_required_by,"0011","2011") FOR RECNO()>1 IN tempbcs
REPLACE tempbcs.exw_required_by WITH STRTRAN(tempbcs.exw_required_by,"0012","2012") FOR RECNO()>1 IN tempbcs
REPLACE tempbcs.exw_required_by WITH STRTRAN(tempbcs.exw_required_by,"0013","2013") FOR RECNO()>1 IN tempbcs
REPLACE tempbcs.exw_required_by WITH STRTRAN(tempbcs.exw_required_by,"/","-") FOR RECNO()>1 IN tempbcs
BLANK FIELD tempbcs.exw_required_by FOR LEFT(ALLTRIM(tempbcs.exw_required_by),1) = "-" AND RECNO()>1 IN tempbcs

REPLACE tempbcs.order_date_time WITH STRTRAN(tempbcs.order_date_time,"0011","2011") FOR RECNO()>1 IN tempbcs
REPLACE tempbcs.order_date_time WITH STRTRAN(tempbcs.order_date_time,"0012","2012") FOR RECNO()>1 IN tempbcs
REPLACE tempbcs.order_date_time WITH STRTRAN(tempbcs.order_date_time,"0013","2013") FOR RECNO()>1 IN tempbcs
REPLACE tempbcs.order_date_time WITH STRTRAN(tempbcs.order_date_time,"/","-") FOR RECNO()>1 IN tempbcs
BLANK FIELD tempbcs.order_date_time FOR LEFT(ALLTRIM(tempbcs.order_date_time),1) = "-" AND RECNO()>1 IN tempbcs

REPLACE tempbcs.line_custom_attrib4 WITH tempbcs.order_date_time FOR RECNO()>1 IN tempbcs
REPLACE tempbcs.line_custom_attrib4 WITH xtollusedate FOR ((LEFT(ALLTRIM(tempbcs.line_custom_attrib4),1) = "-") OR EMPTY(ALLT(line_custom_attrib4)))AND RECNO()>1 IN tempbcs

REPLACE tempbcs.required_date WITH STRTRAN(tempbcs.required_date,"0011","2011") FOR RECNO()>1 IN tempbcs
REPLACE tempbcs.required_date WITH STRTRAN(tempbcs.required_date,"0012","2012") FOR RECNO()>1 IN tempbcs
REPLACE tempbcs.required_date WITH STRTRAN(tempbcs.required_date,"0013","2013") FOR RECNO()>1 IN tempbcs
REPLACE tempbcs.required_date WITH STRTRAN(tempbcs.required_date,"/","-") FOR RECNO()>1 IN tempbcs
REPLACE tempbcs.required_date WITH xtollusedate FOR LEFT(ALLTRIM(tempbcs.required_date),1) = "-" AND RECNO()>1 IN tempbcs

REPLACE tempbcs.line_custom_attrib3 WITH STRTRAN(tempbcs.line_custom_attrib3,"0011","2011") FOR RECNO()>1 IN tempbcs
REPLACE tempbcs.line_custom_attrib3 WITH STRTRAN(tempbcs.line_custom_attrib3,"0012","2012") FOR RECNO()>1 IN tempbcs
REPLACE tempbcs.line_custom_attrib3 WITH STRTRAN(tempbcs.line_custom_attrib3,"/","-") FOR RECNO()>1 IN tempbcs
BLANK FIELD tempbcs.line_custom_attrib3 FOR LEFT(ALLTRIM(tempbcs.line_custom_attrib3),1) = "-" AND RECNO()>1 IN tempbcs

REPLACE tempbcs.line_custom_attrib2 WITH ALLTRIM(UPPER(tempbcs.line_custom_attrib2)) FOR RECNO()>1 IN tempbcs
REPLACE tempbcs.client_cust_code WITH xtollcustcode FOR RECNO()>1 IN tempbcs
REPLACE tempbcs.order_status WITH xtollorderstatus FOR RECNO()>1 AND EMPTY(tempbcs.order_status) IN tempbcs
REPLACE tempbcs.linestatus WITH xtollorderstatus FOR RECNO()>1 IN tempbcs
REPLACE tempbcs.delivery_port WITH xtolldelivport FOR RECNO()>1 IN tempbcs
REPLACE tempbcs.destination_port WITH xtolldelivport FOR RECNO()>1 IN tempbcs

LOCATE
IF lTesting
*	brow
ENDIF

SELECT order_no,line_no,supplier_code,origin_port,country_of_origin,line_custom_attrib2 FROM tempbcs WHERE .F. INTO CURSOR tempmissorigin READWRITE && Added per Mike Johnson, 11.29.2011

SELECT tempbcs
nDelPort = 0
SCAN FOR RECNO() > 1
	cScode = ALLTRIM(tempbcs.supplier_code)
	cSname = ALLTRIM(tempbcs.supplier_name)
	cCustomer = UPPER(ALLTRIM(tempbcs.line_custom_attrib2))
	DO CASE
		CASE SEEK(cScode,'vendors','vendcode')
			REPLACE tempbcs.origin_port WITH ALLTRIM(vendors.vendorigin) IN tempbcs NEXT 1
			REPLACE tempbcs.country_of_origin WITH ALLTRIM(vendors.country) IN tempbcs NEXT 1

		CASE SEEK(cSname,'vendors','vendname')
			REPLACE tempbcs.origin_port WITH ALLTRIM(vendors.vendorigin) IN tempbcs NEXT 1
			REPLACE tempbcs.country_of_origin WITH ALLTRIM(vendors.country) IN tempbcs NEXT 1

		OTHERWISE
			nDelPort = nDelPort+1
			SELECT tempbcs
			SCATTER FIELDS order_no,line_no,supplier_code,origin_port,country_of_origin,line_custom_attrib2 MEMVAR
			INSERT INTO tempmissorigin FROM MEMVAR
			DELETE NEXT 1 IN tempbcs
	ENDCASE

	cDelAddrName = UPPER(ALLTRIM(tempbcs.delivery_address_name))
	IF SEEK(cDelAddrName,'customers','custwhse')
		REPLACE tempbcs.address_code WITH ALLTRIM(customers.custcode) IN tempbcs NEXT 1
	ENDIF

	IF EMPTY(tempbcs.exw_required_by)
		REPLACE tempbcs.exw_required_by WITH tempbcs.required_date IN tempbcs NEXT 1
	ENDIF
	REPLACE tempbcs.custom_milestone_est1 WITH tempbcs.exw_required_by IN tempbcs NEXT 1
ENDSCAN

IF nDelPort > 0 AND !lTesting
	WAIT WINDOW "There were "+TRANSFORM(nDelPort)+" records deleted due to missing PORT/COUNTRY information" TIMEOUT 5
*!*	Added the following block per Mike Johnson, 11.29.2011
	SELECT tempmissorigin
	COPY TO "f:\3pl\data\tempmissorigin.xls" TYPE XL5
	tFrom ="TGF WMS Operations <fmi-edi-ops@fmiint.com>"
	SELECT 0
	USE F:\3pl\DATA\mailmaster ALIAS mm
	LOCATE FOR edi_type = '850' AND GROUP = 'BIOWORLD'
	tsendtox = IIF(mm.use_alt,sendtoalt,sendto)
	tccx = IIF(mm.use_alt,ccalt,cc)
	USE IN mm
	tsubject = "Bioworld POs with unmatched vendor codes"
	tattach = "f:\3pl\data\tempmissorigin.xls"
	tmessage = "The attached Excel file contains all POs by line which did not have matching Vendor records."
	DO FORM m:\dev\frm\dartmail2 WITH tsendtox,tFrom,tsubject,tccx,tattach,tmessage,"A"
	USE IN tempmissorigin
	DELETE FILE "f:\3pl\data\tempmissorigin.xls"
	nDelPort = 0
*!*	END OF ADDED BLOCK
	SELECT tempbcs
ENDIF

IF lTesting
	LOCATE
	BROWSE FIELDS order_no,delivery_address_name,address_code
ENDIF

BLANK FIELDS tempbcs.LINE_CUSTOM_DATE1 FOR RECNO()>1
SELECT order_no,supplier_code,supplier_name,exw_required_by,order_date_time,origin_port,destination_port,line_no,PRODUCT,line_description,qty_ordered ;
	FROM tempbcs ;
	WHERE VAL(tempbcs.qty_ordered) = 0 ;
	AND RECNO()>1 ;
	INTO CURSOR tempzeroqty READWR

SELECT tempzeroqty
LOCATE
IF !EOF()
	IF lTesting
		BROWSE
		SET STEP ON
	ENDIF

	cExcelFile = "f:\3pl\data\zeroqty"+LEFT(TTOC(DATETIME(),1),8)+".xls"

	COPY TO [&cExcelFile] TYPE XL5
	USE IN tempzeroqty

	SELECT 0
	USE F:\3pl\DATA\mailmaster ALIAS mm
	IF lTesting
		LOCATE FOR edi_type = "MISC" AND taskname = "GENERAL"
	ELSE
		LOCATE FOR edi_type = '850' AND GROUP = 'BIOWORLD'
	ENDIF
	tsendtox = IIF(mm.use_alt,sendtoalt,sendto)
	tccx = IIF(mm.use_alt,ccalt,cc)
	USE IN mm
	tFrom = "TGF EDI Operations <fmi-edi-ops@fmiint.com>"
	tsubject = "Bioworld POs with Zero Quantity"
	tattach = cExcelFile
	tmessage = "The attached Excel file contains all POs by line which were zero quantity, and have been removed from the iCON file."
	DO FORM m:\dev\frm\dartmail2 WITH tsendtox,tFrom,tsubject,tccx,tattach,tmessage,"A"
	DELETE FILE [&cExcelfile]
ENDIF

SELECT tempbcs
DELETE FOR VAL(tempbcs.qty_ordered) = 0

SELECT barchive
REPLACE barchive.procdt WITH DATE() FOR EMPTY(procdt) IN barchive

DO WHILE .T.
	**changed extenstion to "csv" from "txt" per Bill Lillicrap - mvw 08/29/13
*	xoutfile="PO_TGFDI_"+TTOC(DATETIME(),1)+"BIOWORLD.txt"
	xoutfile="PO_TGFDI_"+TTOC(DATETIME(),1)+"BIOWORLD.csv"

	**changed extenstion to "csv" from "txt" per Bill Lillicrap - mvw 08/29/13
*	xoutfiletemp = "test1.txt"
	xoutfiletemp = "test1.csv"
	xarchiveoutfile=xoutdir+"archive\"+xoutfile
	IF !FILE(xarchiveoutfile)
		EXIT
	ENDIF
ENDDO

xoutfile=xoutdir+xoutfile
xoutfiletemp=xoutdir+xoutfiletemp
SELECT tempbcs
COPY TO c:\tempfox\bioworld_example
COPY TO &xoutfiletemp TYPE DELIMITED WITH CHARACTER "|"
cStr1 = FILETOSTR(xoutfiletemp)
cStr1 = STRTRAN(cStr1,'"','')
IF lFilesOut
	STRTOFILE(cStr1,xoutfile)
ENDIF
STRTOFILE(cStr1,xarchiveoutfile)
RELEASE cStr1
DELETE FILE &xoutfiletemp

DELETE FILE &xfilename

SELECT 0
USE F:\edirouting\ftpsetup SHARED
REPLACE ftpsetup.chkbusy WITH .F. FOR ftpsetup.transfer = "850-BIOWORLD"

SELECT 0
USE F:\edirouting\ftpjobs SHARED
INSERT INTO ftpjobs (jobname,USERID,jobtime) VALUES ('BIO-PO-OUT','JOEB',DATETIME())

CLOSE DATA ALL
RELEASE ALL
SET DATE AMERICAN

WAIT CLEAR
WAIT WINDOW "Entire Bioworld 850 conversion/output process complete...exiting" TIMEOUT 2
