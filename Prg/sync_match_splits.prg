*!*	Have Outship and EDI_Trigger open to start
CLOSE DATA ALL
IF usesqloutwo()
	USE F:\wh2\whdata\outship IN 0 ALIAS outship SHARED
ENDIF

USE F:\3pl\DATA\edi_trigger ORDER TAG ship_ref IN 0 ALIAS edi_trigger SHARED

SELECT PADR(LEFT(ship_ref,AT("~",ship_ref)-1),20) AS ship_ref FROM edi_trigger WHERE accountid = 6521 AND (errorflag OR !processed) AND "~"$ship_ref INTO CURSOR temptrig
SELECT ship_ref,COUNT(ship_ref) AS cnt1,SPACE(20) AS bol,.F. AS matched FROM temptrig GROUP BY 1 INTO CURSOR temppairs READWRITE HAVING cnt1 = 2
BROWSE TIMEOUT 5
*SET STEP ON
LOCATE
SCAN
	cShip_ref = ALLTRIM(temppairs.ship_ref) && Root ship_ref
	WAIT WINDOW "Now replacing data for PT "+cShip_ref NOWAIT
	cShip_ref_C = PADR(ALLTRIM(temppairs.ship_ref)+" ~C",20)
	IF SEEK(cShip_ref_C,"edi_trigger","ship_ref")
		cTrk = edi_trigger.bol
		cTrk6 = RIGHT(TRIM(cTrk),6)
		REPLACE temppairs.matched WITH .T.,temppairs.bol WITH cTrk IN temppairs NEXT 1
		REPLACE edi_trigger.bol WITH cTrk,edi_trigger.processed WITH .F. FOR edi_trigger.ship_ref = cShip_ref IN edi_trigger
		IF usesqloutwo()
			xsqlexec("update outship set bol_no = '"+cTrk+"' where mod = '2' and ship_ref = '"+cShip_ref+"'",,,"wh")
			xsqlexec("update outship set blnum6 = '"+cTrk6+"' where mod = '2' and ship_ref = '"+cShip_ref+"'",,,"wh")
		ELSE
			SELECT outship
			REPLACE outship.bol_no WITH cTrk FOR outship.ship_ref = cShip_ref IN outship
			REPLACE outship.blnum6 WITH cTrk6 FOR outship.ship_ref = cShip_ref IN outship
		ENDIF
	ENDIF
ENDSCAN
*!*	SELECT temppairs
*!*	LOCATE
*!*	BROW
WAIT WINDOW "Replacement process complete" TIMEOUT 2
CLOSE DATA ALL
