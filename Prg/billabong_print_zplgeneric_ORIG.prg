Parameter whichlabel, xprinter, xstartdt, xlastcarton

**** below code used in the label print program to sekect the label records to print

*!*  Locate For sel
*!*  If !Found()
*!*    =Messagebox("Please make a selection before continuing.", 16, "")
*!*    Return
*!*  Else
*!*    lnRunid = tdata.runid
*!*  Endif

*!*  lcFilename = Sys(3)
*!*  lcFilename ="h:\fox\"+lcFilename+".dbf"
*!*  Copy To &lcFilename For sel
*!*  Use &lcFilename In 0 Alias xdata
************************************************************************************

if !empty(xprinter)
	lcPtr=xprinter
endif
xstartdt=iif(empty(xstartdt),{},xstartdt)

** not sure if we need there but it wont hurt
!net Use lpt1 /Delete
!net Use lpt1 &lcPtr


** point to a wave label record of maybe a query to a cursor xdata

lcStr =""
lcStr2=""

lcquery ="select * From cartons  Where ucc = '"+Alltrim(xdata.uccnumber)+Alltrim(xdata.chkdigit)+"'"
useca("cartons","PICKPACK_SQL5",,,lcQuery)
Select upc,qty,totqty,.f. as isShort From cartons Into Cursor xctns readwrite
llShort = .f.

Select xctns
Scan
 If totqty != qty
    replace isShort with .t.
 endif    
Endscan

Select xdata
Scan

  If Inlist(whichlabel,"UCC","BOTH")
    lcStr = Filetostr("f:\wh\zpltemplates\billabong_generic_zpl.txt")

    lcStr = Strtran(lcStr,"<SHIPFROM>",    Alltrim(xdata.acct_name))
    lcStr = Strtran(lcStr,"<SHIPFROMADDR>",Alltrim(xdata.sfromaddr1))
    lcStr = Strtran(lcStr,"<SHIPFROMCSZ>", Alltrim(xdata.sfromcsz))
    lcStr = Strtran(lcStr,"<SHIPTO>",      Alltrim(xdata.shipto))
    lcStr = Strtran(lcStr,"<SHIPTOADDR1>", Alltrim(xdata.staddr1))
    If!Empty(xdata.staddr2)
      lcStr = Strtran(lcStr,"<SHIPTOADDR2>", Alltrim(xdata.staddr2))
    Else
      lcStr = Strtran(lcStr,"<SHIPTOADDR2>", "")
    Endif
    lcStr = Strtran(lcStr,"<SHIPTOCSZ>",   Alltrim(xdata.stcsz))
    lcStr = Strtran(lcStr,"(420) 12345",   "(420) "+Alltrim(xdata.stzip))
    lcStr = Strtran(lcStr,"42012345",      "420"+Substr(Alltrim(xdata.stzip),1,5))
    lcStr = Strtran(lcStr,"<PICKTICKET>",  " PT: "+Alltrim(xdata.pt))
    lcStr = Strtran(lcStr,"<PONUM>",       " PO: "+Alltrim(xdata.cust_po))
    lcStr = Strtran(lcStr,"<XXXXSHIPVIAXXXX>",     " " )
    lcStr = Strtran(lcStr,"<CARTONOF>",    Alltrim(xdata.cartonof))
    lcStr = Strtran(lcStr,"<WONUM>",       "WO#:"+Alltrim(Transform(xdata.wo_num)))
    lcStr = Strtran(lcStr,"80000510200000218128",Alltrim(xdata.uccnumber))
    lcStr = Strtran(lcStr,"<UCCNUMBER>","("+Substr(xdata.uccnumber,1,2)+") "+Substr(xdata.uccnumber,3,1)+" "+Substr(xdata.uccnumber,4,7)+" "+Substr(xdata.uccnumber,12)+" "+Alltrim(chkdigit))

  Endif

** ^FT53,1198^BCN,294,N,N,Y^FD>;><80000510200000218128^FS

  If Inlist(whichlabel,"DETAIL","BOTH")

    lcStr2 = Filetostr("f:\wh\zpltemplates\billabong_detail_zpl_REV2.txt")

    If xstartdt > Date()
      lcStr2= Strtran(lcStr2,"<SHIPDATE>",   "   "+Transform(Month(xstartdt))+"/"+Padl(Transform(Day(xstartdt)),2,"0"))
    Else
      lcStr2= Strtran(lcStr2,"<SHIPDATE>",   "   ")
    endif

    If xlastcarton = .T. 
      lcStr2= Strtran(lcStr2,"<PACKINGLISTENCLOSED>",   "PACKING LIST ENCLOSED")
    Else
      lcStr2= Strtran(lcStr2,"<PACKINGLISTENCLOSED>",   "   ")
    endif

    lcStr2 = Strtran(lcStr2,"<CARTONOF>" ,  Alltrim(xdata.cartonof) )
    lcStr2 = Strtran(lcStr2,"<WONUM>",     "WO#:"+Alltrim(Transform(xdata.wo_num)))
    lcStr2 = Strtran(lcStr2,"<PICKTICKET>"," PT: "+Alltrim(xdata.pt))
    lcStr2 = Strtran(lcStr2,"<SWC>","SWC:"+Alltrim(xdata.swc))
    lcStr2 = Strtran(lcStr2,"<XXXXXXSHIPVIAXXXXXX>",   Alltrim(xdata.ship_via))

    lcStr2 = Strtran(lcStr2,"<RUNID>",  "SWC:"+Alltrim(Transform(xdata.runid)))
    lcStr2 = Strtran(lcStr2,"<PRINTDATE>",  " PRINTED: "+Alltrim(Ttoc(Datetime())))
    lcStr2 = Strtran(lcStr2,"80000510200000218128",Alltrim(xdata.uccnumber))
    lcStr2 = Strtran(lcStr2,"<UCCNUMBER>","("+Substr(xdata.uccnumber,1,2)+") "+Substr(xdata.uccnumber,3,1)+" "+Substr(xdata.uccnumber,4,7)+" "+Substr(xdata.uccnumber,12)+" "+Alltrim(chkdigit))

    lcStr2= Strtran(lcStr2,"<H1>",   Alltrim(xdata.h1))

*set step On 
*    Do writeNline With "<N1>",  Alltrim(xdata.n1)

   lcStr2= Strtran(lcStr2,"<N1>",   Alltrim(xdata.n1))

    If !Empty(Alltrim(xdata.n2))
      lcStr2= Strtran(lcStr2,"<N2>", Alltrim(xdata.n2))
    Else
      lcStr2= Strtran(lcStr2,"<N2>",  "")
    Endif
    If !Empty(Alltrim(xdata.n3))
      lcStr2= Strtran(lcStr2,"<N3>", Alltrim(xdata.n3))
    Else
      lcStr2= Strtran(lcStr2,"<N3>",  "")
    Endif
    If !Empty(Alltrim(xdata.n4))
      lcStr2= Strtran(lcStr2,"<N4>", Alltrim(xdata.n4))
    Else
      lcStr2= Strtran(lcStr2,"<N4>",  "")
    Endif
    If !Empty(Alltrim(xdata.n5))
      lcStr2= Strtran(lcStr2,"<N5>", Alltrim(xdata.n5))
    Else
      lcStr2= Strtran(lcStr2,"<N5>",  "")
    Endif
    If !Empty(Alltrim(xdata.n6))
      lcStr2= Strtran(lcStr2,"<N6>", Alltrim(xdata.n6))
    Else
      lcStr2= Strtran(lcStr2,"<N6>",  "")
    Endif
    If !Empty(Alltrim(xdata.n7))
      lcStr2= Strtran(lcStr2,"<N7>", Alltrim(xdata.n7))
    Else
      lcStr2= Strtran(lcStr2,"<N7>",  "")
    Endif
    If !Empty(Alltrim(xdata.n8))
      lcStr2= Strtran(lcStr2,"<N8>", Alltrim(xdata.n8))
    Else
      lcStr2= Strtran(lcStr2,"<N8>",  "")
    Endif
    If !Empty(Alltrim(xdata.n9))
      lcStr2= Strtran(lcStr2,"<N9>", Alltrim(xdata.n9))
    Else
      lcStr2= Strtran(lcStr2,"<N9>",  "")
    Endif
    If !Empty(Alltrim(xdata.n10))
      lcStr2= Strtran(lcStr2,"<N10>", Alltrim(xdata.n10))
    Else
      lcStr2= Strtran(lcStr2,"<N10>",  "")
    Endif
    If !Empty(Alltrim(xdata.n11))
      lcStr2= Strtran(lcStr2,"<N11>", Alltrim(xdata.n11))
    Else
      lcStr2= Strtran(lcStr2,"<N11>",  "")
    Endif
    If !Empty(Alltrim(xdata.n12))
      lcStr2= Strtran(lcStr2,"<N12>", Alltrim(xdata.n12))
    Else
      lcStr2= Strtran(lcStr2,"<N12>",  "")
    Endif
    If !Empty(Alltrim(xdata.n13))
      lcStr2= Strtran(lcStr2,"<N13>", Alltrim(xdata.n13))
    Else
      lcStr2= Strtran(lcStr2,"<N13>",  "")
    Endif
    If !Empty(Alltrim(xdata.n14))
      lcStr2= Strtran(lcStr2,"<N14>", Alltrim(xdata.n14))
    Else
      lcStr2= Strtran(lcStr2,"<N14>",  "")
    Endif
    If !Empty(Alltrim(xdata.n15))
      lcStr2= Strtran(lcStr2,"<N15>", Alltrim(xdata.n15))
    Else
      lcStr2= Strtran(lcStr2,"<N15>",  "")
    Endif
    If !Empty(Alltrim(xdata.n16))
      lcStr2= Strtran(lcStr2,"<N16>", Alltrim(xdata.n16))
    Else
      lcStr2= Strtran(lcStr2,"<N16>",  "")
    Endif
    If !Empty(Alltrim(xdata.n17))
      lcStr2= Strtran(lcStr2,"<N17>", Alltrim(xdata.n17))
    Else
      lcStr2= Strtran(lcStr2,"<N17",  "")
    Endif
    If !Empty(Alltrim(xdata.n18))
      lcStr2= Strtran(lcStr2,"<N18>", Alltrim(xdata.n18))
    Else
      lcStr2= Strtran(lcStr2,"<N18>",  "")
    Endif
    If !Empty(Alltrim(xdata.n19))
      lcStr2= Strtran(lcStr2,"<N19>", Alltrim(xdata.n19))
    Else
      lcStr2= Strtran(lcStr2,"<N19>",  "")
    Endif
    If !Empty(Alltrim(xdata.n20))
      lcStr2= Strtran(lcStr2,"<N20>", Alltrim(xdata.n20))
    Else
      lcStr2= Strtran(lcStr2,"<N20>",  "")
    Endif
    If !Empty(Alltrim(xdata.n21))
      lcStr2= Strtran(lcStr2,"<N21>", Alltrim(xdata.n21))
    Else
      lcStr2= Strtran(lcStr2,"<N21>",  "")
    Endif
    If !Empty(Alltrim(xdata.n22))
      lcStr2= Strtran(lcStr2,"<N22>", Alltrim(xdata.n22))
    Else
      lcStr2= Strtran(lcStr2,"<N22>",  "")
    Endif
    If !Empty(Alltrim(xdata.n23))
      lcStr2= Strtran(lcStr2,"<N23>", Alltrim(xdata.n23))
    Else
      lcStr2= Strtran(lcStr2,"<N23>",  "")
    Endif
    If !Empty(Alltrim(xdata.n24))
      lcStr2= Strtran(lcStr2,"<N24>", Alltrim(xdata.n24))
    Else
      lcStr2= Strtran(lcStr2,"<N24>",  "")
    Endif
    If !Empty(Alltrim(xdata.n25))
      lcStr2= Strtran(lcStr2,"<N25>", Alltrim(xdata.n25))
    Else
      lcStr2= Strtran(lcStr2,"<N25>",  "")
    Endif
    If !Empty(Alltrim(xdata.n26))
      lcStr2= Strtran(lcStr2,"<N26>", Alltrim(xdata.n26))
    Else
      lcStr2= Strtran(lcStr2,"<N26>",  "")
    Endif
  Endif

  lcOutStr = lcStr+lcStr2

  Strtofile(lcOutStr,"h:\fox\temp.txt")
  lcfile = "h:\fox\temp.txt"
  !Type &lcfile > lpt1
Endscan

Use In xdata
***********************************************************************************
** ok replace the last couple of characters in the line data if this upc was shorted
*************************************************************************************8
Procedure WriteNline
  Parameters mask,linedata
   
  If Empty(linedata)
    lcStr2= Strtran(lcStr2,mask, "")
    return 
  Endif 
 
  Select xctns
  Scan
    If xctns.upc$linedata And xctns.qty!= totqty
      linedata = Substr(linedata,1,Len(Alltrim(linedata-3)))
      linedata = linedata+Transform(xctns.totqty)
      lcStr2 = Strtran(lcStr2,mask,linedata)
      return
    endif  
  Endscan 

  lcStr2 = Strtran(lcStr2,mask,linedata)

Endproc  
**************************************************************************************