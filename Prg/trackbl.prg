parameters lwebsite

xacctname=acctname(xaccountid)

if usesqlbl()
	xsqlexec("select * from bl",,,"wo")
	index on inboundid tag inboundid
	set order to

	xsqlexec("select * from inbound",,,"wo")
	index on inboundid tag inboundid
	set order to

	xsqlexec("select * from delivery",,,"wo")
	index on outboundid tag outboundid
	set order to

	xsqlexec("select * from outbound",,,"wo")
	index on del_mfst tag del_mfst
	index on outboundid tag outboundid
	set order to

else
	use f:\wo\wodata\bl in 0
	use f:\wo\wodata\inbound in 0
	use f:\wo\wodata\delivery in 0
	use f:\wo\wodata\outbound in 0
endif

create cursor c_report (;
	cr_sortby i, ;
	cr_acctname c(50),;
	cr_groupby m, ;
	cr_co_name c(50),;
	cr_rptname c(200),;
	cr_footername c(200),;
	cr_header m, ;
	cr_details m)

select mfst_num, pu_trlr, pu_seal, bl.* ;
	from inbound join bl on inbound.inboundid = bl.inboundid ;
	where bl_num = cblnum and iif(xacctname = 'JONES', bl.acctname = substr(xacctname,1,5), ;
	bl.acctname = substr(xacctname,1,20)) into cursor xin
	
select xin
if !eof() 
	set margin to 0

	lsgroupbystring = chr(13)+"  Bill Of Lading: "+bl_num+space(18+3)+"Work Order: "+ltrim(str(wo_num,7,0))+;
	chr(13)+"         Shipper: "+acctname+space(8)+"Mfst No: "+mfst_num+;
	chr(13)+"     P/U Trailer: "+pu_trlr+space(15+10)+"Seal No: "+pu_seal+;
	chr(13)+"        P/U Date: "+dtoc(pu_date)+space(20+10)+"B/L Qty: "+;
	ltrim(str(ship_qty,6,0))+left(qty_type, 7)+;
	chr(13)+"      Strip Date: "+dtoc(strip_date)+space(20+9)+"Rcvd Qty: "+;
	ltrim(str(rcv_qty,6,0))+left(qty_type, 7)+;
	chr(13)+"Consignee/Consol: "+alltrim(cnee)+",  "+alltrim(deliverto)+;
	chr(13)+chr(13)+chr(13)

	lstimestring = "Run Date: "+dtoc(date())+"  Run Time: "+ttoc(datetime(),2)+"     "+chr(13)+chr(13)

	store "TGF INC." to lscompanyname

	lsreportname = "TGF Trucking Outbound Activity"

	select ofd_date, delivery.* ;
		from outbound left join delivery on outbound.outboundid = delivery.outboundid ;
		where delivery.bl_num = cblnum and iif(xacctname = 'JONES', delivery.acctname = substr(xacctname,1,5), ;
		delivery.acctname = substr(xacctname,1,20)) ;
		into cursor trackout


	select xin &&display last location???
	clastloc = iif(empty(del_qty) and !emptynul(ofd_date), " Last Loc: "+location, "")

	lsheader = "   Appt Date     Appt Ref          Must Ship    Not Before    Intructions"+;
	chr(13)+space(3)+dtoc(trackout.appt_date)+space(5)+trackout.apptrec+space(4)+dtoc(mustdel)+;
	space(6)+dtoc(shipnotb4)+space(5)+message+chr(13)+;
	chr(13)+"       Out For Del    Del Mfst      Qty     Deliv      Remarks"+clastloc+chr(13)

	store "" to lsacctname, lsdetails
	lsgroupby = lstimestring+lsgroupbystring
	lisortby = 0

	insert into c_report (cr_sortby, ;
	cr_groupby, ;
	cr_acctname, ;
	cr_header, ;
	cr_co_name, ;
	cr_rptname, ;
	cr_footername, ;
	cr_details) ;
	values (lisortby, ;
	lsgroupby, ;
	lsacctname, ;
	lsheader, ;
	lscompanyname,;
	lsreportname,;
	lsgroupbystring,;
	lsdetails)

	=adddetailrecordstocursor("c_Report")
endif

select c_report

loadwebpdf("PORTRAIT")

use in inbound
use in bl
use in outbound
use in delivery


************************************************************************************************************************************************
procedure adddetailrecordstocursor
lparameters lsreportcursor

local  lsdetails, lsgroupby, lisortby, lnctr

lsdetails = ""
lsgroupby = lstimestring+lsgroupbystring
lisortby = 1

select trackout
scan
	lsdetails = space(8)+dtoc(ofd_date)+space(7)+del_mfst+space(4)+;
	str(mfst_qty,6,0)+space(4)+str(del_qty,6,0)+space(6)

	lused = .t.
	do case
	case rtv
		lsdetails = lsdetails+"RETURNED TO VENDOR"
	case refused
		lsdetails = lsdetails+"REFUSED-"+reason
	case undel
		lsdetails = lsdetails+"UNDEL-"+reason
	case empty(del_qty) .and. !emptynul(ofd_date)
		lsdetails = lsdetails+"OUT FOR DELIVERY"
	case !empty(del_qty)
		lsdetails = lsdetails+"DELIVERED"

		if del_qty # mfst_qty
			lsdetails = lsdetails+chr(13)+space(55)+reason
		endif
	otherwise
		lused = .f.
	endcase

	if !empty(keyrec) and lused
		lsdetails = lsdetails+chr(13)+space(55)+"KEYREC: "+keyrec+;
		iif(!empty(keyrec1), chr(13)+space(63)+keyrec1, "")
	endif

	if lused
		=adddetails(lsreportcursor,"",lsheader, lsdetails, lsgroupby, lisortby)
	endif
endscan

select xin
scan
	store location to tloc

	lused = .t.
	lsdetails = ""
	do case
	case rtv
		lsdetails = space(33)+str(rcv_qty,6,0)+space(16)+"RETURN TO VENDOR"+;
		chr(13)+space(55)+"Location: "+tloc
	case emptynul(ofd_date) .and. rcv_qty=0
		lsdetails = space(55)+"UNLOADING NOT COMPLETE"
	case emptynul(ofd_date) .and. rcv_qty>0
		lsdetails = space(33)+str(rcv_qty,6,0)+space(16)+"FREIGHT ON HAND"+;
		chr(13)+space(55)+"Location: "+tloc
	otherwise
		lused = .f.
	endcase

	if lused
		=adddetails(lsreportcursor,"",lsheader, lsdetails, lsgroupby, lisortby)
	endif
endscan


********************
procedure adddetails
lparameters lsreportcursor,lsacctname,lsheader, lsdetails, lsgroupby, lisortby

insert into (lsreportcursor) (cr_sortby, ;
cr_groupby, ;
cr_acctname, ;
cr_co_name,;
cr_rptname, ;
cr_footername, ;
cr_header, ;
cr_details) ;
values (	lisortby, ;
lsgroupby, ;
lsacctname, ;
lscompanyname,;
lsreportname,;
lsgroupbystring,;
lsheader, ;
lsdetails)
