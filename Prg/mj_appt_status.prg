
SET EXCLUSIVE OFF 
SET DELETED ON 
SET TALK OFF  
SET SAFETY OFF
do m:\dev\prg\_setvars
Set Deleted off
_screen.WindowState=1
set step on 

xsqlexec("select outshipid, outwologid, wo_num, wo_date, accountid, ship_ref, combgroup, sp, notonwip, del_date, " + ;
	"ptdate, consignee, name, address, address2, csz, combpt, ship_via, cnee_ref, keyrec, picked, labeled, " + ;
	"staged, called, appt, truckloaddt, dept, qty, ctnqty, origqty, weight, cuft, start, cancel, appt_num, appt_time, " + ;	
	"apptremarks, apptstatus, storenum, dcnum, asncode, asndt, rrc_num, bol_no, swcnum, swcover, cacctnum, eaches, " + ;
	"duns, salesrep, div, vendor_num, batch_num, terms, upsacctnum, scac, rout_desc, rout_code, shipfor, sforstore, " + ;
	"sforaddr1, sforaddr2, sforcsz, expdeldt, asnsentdt, asnsenttime, asnsentby, asnackdt, asnacktime, sid, cid, " + ;
	"pulled, combinedpt, spduedt, pulleddt, manpick, manlabel, manstage, blpod, prereceive, delenterdt, " + ;
	"delenterby, blenterdt, blenterby, noshow, wodelete, locked, x2448, dedspclass, splitpt, picknpack, masterpack, " + ;
	"origoutshipid, tilde, routedfor, packed, readcomm, overflow, carrcode, cancelrts, b2, updateby, updatedt, " + ;
	"updproc, addby, adddt, addproc, xferacct, container, specinst, wavenum, avoidship, " + ;
	"country, stageenterby, stageenterdt, stageenterproc, residential, apptenterby, apptenterdt " + ;
	"from outship where mod='J' and accountid=6303 and del_date={}","outship",,"wh")

select *, "          " as procstatus, " " as ordstatus from outship into cursor mjstats readwrite

set step on 

replace all ordstatus with iif(!pulled,"1",iif(!emptynul(del_date),"9",iif(!emptynul(truckloaddt),"8",iif(!emptynul(staged),"7",iif(!emptynul(labeled),"6",iif(!emptynul(picked),"3","2"))))))
replace all procstatus with iif(!pulled,"NOT PULLED",iif(!emptynul(del_date),"DELIVERED",iif(!emptynul(truckloaddt),"LOADED",iif(!emptynul(staged),"STAGED",iif(!emptynul(labeled),"LABELED",iif(!emptynul(picked),"PICKED","PULLED"))))))

*Select bol_no,accountid,ship_ref as Pickticket,consignee,cnee_ref as PO,wo_num,qty,ctnqty,ship_via,Dtoc(start) as start_date,Dtoc(cancel) as cancen_date,Dtoc(routedfor) as routed_date,;
 Dtoc(appt) as appt_date,scac,appt_num,appt_time,apptremarks;
 from outship where InList(accountid,6303) and (Emptynul(del_date) and !empty(appt) and !notonwip) into cursor mjstats readwrite order by appt asc
 
Select mjstats
Export to s:\marcjacobsdata\reports\MJ_Wholesale_APPTS.xls type xls
Copy   To s:\marcjacobsdata\reports\MJ_Wholesale_APPTS.csv type csv

*!*	Select outshipid	,outwologid	,wo_num	,wo_date	,accountid	,ship_ref	,combgroup	,sp	,notonwip	,del_date	,;
*!*	ptdate	,consignee	,name	,address	,address2	,csz	,combpt	,ship_via	,cnee_ref	,keyrec	,picked	,;
*!*	labeled	,staged	,called	,appt	,truckloaddt	,dept	,qty	,ctnqty	,origqty	,weight	,cuft	,start	,;
*!*	cancel	,appt_num	,appt_time	,apptremarks	,apptstatus	,storenum	,dcnum	,asncode	,asndt	,;
*!*	rrc_num	,bol_no	,swcnum	,swcover	,cacctnum	,eaches	,duns	,salesrep	,div	,vendor_num	,batch_num	,;
*!*	terms	,upsacctnum	,scac	,rout_desc	,rout_code	,shipfor	,sforstore	,sforaddr1	,sforaddr2	,;
*!*	sforcsz	,expdeldt	,asnsentdt	,asnsenttime	,asnsentby	,asnackdt	,asnacktime	,sid	,cid	,;
*!*	fromupload	,pulled	,combinedpt	,spduedt	,pulleddt	,manpick	,manlabel	,manstage	,blpod	,;
*!*	prereceive	,delenterdt	,delenterby	,blenterdt	,blenterby	,noshow	,wodelete	,locked	,x2448	,;
*!*	dedspclass	,splitpt	,picknpack	,masterpack	,origoutshipid	,tilde	,routedfor	,packed	,;
*!*	readcomm	,overflow	,dychar	,dydt	,dylog	,archived	,carrcode	,cancelrts	,b2	,updateby	,;
*!*	updatedt	,updproc	,addby	,adddt	,addproc	,deleteby	,deletedt	,delproc	,flag	,xferacct	,;
*!*	container	,specinst	,wavenum	,avoidship	,country	,stageenterby	,stageenterdt	,stageenterproc	,;
*!*	residential	,apptenterby	,apptenterdt, ;
*!*	 "          " as procstatus,Space(1) as ordstatus  from outship where accountid = 6304 and Emptynul(del_date) into cursor mjstats  readwrite

*!*	replace all ordstatus with iif(!pulled,"1",iif(!emptynul(del_date),"9",iif(!emptynul(truckloaddt),"8",iif(!emptynul(staged),"7",iif(!emptynul(labeled),"6",iif(!emptynul(picked),"3","2"))))))
*!*	replace all procstatus with iif(!pulled,"NOT PULLED",iif(!emptynul(del_date),"DELIVERED",iif(!emptynul(truckloaddt),"LOADED",iif(!emptynul(staged),"STAGED",iif(!emptynul(labeled),"LABELED",iif(!emptynul(picked),"PICKED","PULLED"))))))

*!*	*Select bol_no,accountid,ship_ref as Pickticket,consignee,cnee_ref as PO,wo_num,qty,ctnqty,ship_via,Dtoc(start) as start_date,Dtoc(cancel) as cancen_date,Dtoc(routedfor) as routed_date,;
*!*	 Dtoc(appt) as appt_date,scac,appt_num,appt_time,apptremarks;
*!*	 from outship where InList(accountid,6303) and (Emptynul(del_date) and !empty(appt) and !notonwip) into cursor mjstats readwrite order by appt asc
*!*	 
*!*	Select mjstats
*!*	Export to s:\marcjacobsdata\reports\MJ_Retail_APPTS.xls type xls



*!*	Select bol_no,accountid,ship_ref as Pickticket,consignee,cnee_ref as PO,wo_num,qty,ctnqty,ship_via,Dtoc(start) as start_date,Dtoc(cancel) as cancen_date,Dtoc(routedfor) as routed_date,;
*!*	 Dtoc(appt) as appt_date,scac,appt_num,appt_time,apptremarks;
*!*	 from outship where InList(accountid,6304) and (Emptynul(del_date) and !emptynul(appt) and !notonwip) into cursor mjstats readwrite order by appt asc
*!*	 
USE IN outship
USE IN mjstats
return