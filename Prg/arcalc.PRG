lparameter xnotunposted, xasofdt, xartran, xartranfilter

select invoice
if empty(tag(cdx(1),1))		&& dy 4/19/18
	index on invnum tag invnum
	set order to
endif

if empty(xartranfilter)
	xartranfilter="1=1"
endif

* loop through twice if unposted data is requested

if !used("invoiceh")
	use f:\watusi\ardatah\invoiceh in 0
endif

if empty(xartran)
	xnumloops=1
else
	xnumloops=2
endif

xasofdt=evl(xasofdt,date())

create cursor xrpt ( ;
	fromtable c(1), ;
	accountid i, ;
	billtoid i, ;
	company c(1), ;
	docnum c(6), ;
	invnum c(6), ;
	docdt d, ;
	origdocdt d, ;
	lastactdt d, ;
	doctype c(8), ;
	wo_num n(7), ;
	amt1 n(12,2), ;
	amt2 n(12,2), ;
	amt3 n(12,2), ;
	amt4 n(12,2), ;
	amt5 n(12,2), ;
	amt6 n(12,2), ;
	amt7 n(12,2), ;
	amt8 n(12,2), ;
	amt9 n(12,2), ;
	amt10 n(12,2), ;
	paid n(12,2), ;
	owed n(12,2), ;
	onactcomp c(1), ;
	balance n(12,2), ;
	oldinvamt n(12,2), ;
	container c(11), ;
	brokref c(15))

index on invnum tag invnum

* posted invoices

for s=1 to xnumloops
	if s=1
		xartran="artran"
	else
		xartran="artranh"
	endif
	
	m.fromtable="T"

	xdyfilter=iif(taccountid='%',"1=1","shiptokey='"+taccountid+"'")
	xsqlexec("select * from "+xartran+" where transactiontype='I' and "+xdyfilter+" and documentdate<={"+dtoc(xasofdt)+"} and "+xartranfilter,,,"ar")

	scan
		store 0 to m.amt1, m.amt2, m.amt3, m.amt4, m.amt5, m.amt6, m.amt7, m.amt8, m.amt9, m.amt10
		m.accountid=eval("0x"+shiptokey)
		m.billtoid=val(customerkey)
		m.invnum=left(applyto,6)
		m.docdt=documentdate 
		m.origdocdt=m.docdt
		m.lastactdt=m.docdt
		m.doctype="Invoice"
		m.wo_num=0
		m.onactcomp=onactcomp
		m.oldinvamt=0
 
		if isdigit(applyto) or inlist(applyto,"Z","X","S")
			m.docnum=left(applyto,6)
			if seek(m.docnum,"invoice","invnum")
				m.wo_num=invoice.wo_num
			endif
		else
			if applyto="ONACT"
				if !empty(gaccountid) and m.billtoid#gaccountid
					loop
				endif
				m.docnum="ONACT"
				m.doctype="On Acct"
				if seek(m.billtoid,"account","accountid")
					m.accountid=m.billtoid
				else
					=seek(m.billtoid,"acctbill","billtoid")
					m.accountid=acctbill.accountid
				endif
			endif
		endif

		do case
		case type("gover45")="L" and gover45
			do case
			case documentdate<=xasofdt-210
				m.amt5=documentamt
			case documentdate<=xasofdt-150
				m.amt4=documentamt
			case documentdate<=xasofdt-120
				m.amt3=documentamt
			case documentdate<=xasofdt-90
				m.amt2=documentamt
			case between(documentdate,xasofdt-90,xasofdt-46)
				m.amt1=documentamt
			endcase
			
		case type("gover90")="L" and gover90
			do case
			case documentdate<=xasofdt-210
				m.amt5=documentamt
			case documentdate<=xasofdt-180
				m.amt4=documentamt
			case documentdate<=xasofdt-150
				m.amt3=documentamt
			case documentdate<=xasofdt-120
				m.amt2=documentamt
			case between(documentdate,xasofdt-120,xasofdt-91)
				m.amt1=documentamt
			endcase
 			
		case type("gijoe")="L" and gijoe
			do case
			case documentdate<=xasofdt-210
				m.amt8=documentamt
			case documentdate<=xasofdt-180
				m.amt7=documentamt
			case documentdate<=xasofdt-150
				m.amt6=documentamt
			case documentdate<=xasofdt-120
				m.amt5=documentamt
			case documentdate<=xasofdt-90
				m.amt4=documentamt
			case documentdate<=xasofdt-60
				m.amt3=documentamt
			case documentdate<=xasofdt-30
				m.amt2=documentamt
			otherwise
				m.amt1=documentamt
			endcase

		case type("glewislok")="L" and glewislok
			do case
			case documentdate<=xasofdt-360
				m.amt4=documentamt
			case documentdate<=xasofdt-180
				m.amt3=documentamt
			case documentdate<=xasofdt-90
				m.amt2=documentamt
			otherwise
				m.amt1=documentamt
			endcase

		otherwise
			do case
			case documentdate<=xasofdt-90
				m.amt4=documentamt
			case documentdate<=xasofdt-60
				m.amt3=documentamt
			case documentdate<=xasofdt-30
				m.amt2=documentamt
			otherwise
				m.amt1=documentamt
			endcase
		endcase

		insert into xrpt from memvar	
		m.oldinvamt=0
	endscan

	* posted payments/credits
	* the inlists are there because I prematurely archived 2011 invoice data  dy 2/3/15

	m.fromtable="X"

	xsqlexec("select * from "+xartran+" where transactiontype#'I' and documentdate<={"+dtoc(xasofdt)+"} and "+xartranfilter,"xdy",,"ar")

	select * from xdy where isdigit(applyto) or applyto='ONACT' into cursor xdy

	xqq=0
	
	scan
		xqq=xqq+1
		if xqq%100=0
			wait window xqq nowait
		endif	
	
		store 0 to m.amt1, m.amt2, m.amt3, m.amt4, m.amt5, m.amt6, m.amt7, m.amt8, m.amt9, m.amt10
		m.billtoid=val(customerkey)
		m.invnum=left(applyto,6)
		m.docdt=documentdate 
		m.lastactdt=m.docdt
		m.doctype=iif(transactiontype="P","Payment",iif(transactiontype="C","Credit",iif(transactiontype="D","Discount",iif(transactiontype="A","Accrual",""))))
		m.wo_num=0
		m.origdocdt={}
		m.onactcomp=onactcomp

assert applyto#"004151"

		if isdigit(applyto) or inlist(applyto,"Z","X","S")
			m.docnum=left(applyto,6)
			if seek(m.docnum,"invoice","invnum")
				if !empty(gaccountid) and invoice.accountid#gaccountid
					loop
				endif
	 			m.accountid=invoice.accountid
				m.wo_num=invoice.wo_num
				m.origdocdt=invoice.invdt
			else
				if seek(m.docnum,"invoiceh","invnum")
					if !empty(gaccountid) and invoiceh.accountid#gaccountid
						loop
					endif
		 			m.accountid=invoiceh.accountid
					m.wo_num=invoiceh.wo_num
					m.origdocdt=invoiceh.invdt
				endif
			endif
		else
			if applyto="ONACT"
				if !empty(gaccountid) and m.billtoid#gaccountid
					loop
				endif
				m.docnum="ONACT"
				m.doctype="On Acct"
				if seek(m.billtoid,"account","accountid")
					m.accountid=m.billtoid
				else
					=seek(m.billtoid,"acctbill","billtoid")
					m.accountid=acctbill.accountid
				endif
				m.origdocdt=m.docdt
			else
				m.docnum=transform(asc(applyto)-64)+substr(applyto,2,4)
			endif
		endif

		if empty(m.origdocdt)
			if (type("gover90")#"L" or !gover90) or (type("gover45")#"L" or !gover45)
				m.amt1=documentamt
			endif
		else
			if !seek(left(applyto,6),"xrpt","invnum")
	*			asse .f.
			endif
			xdocamt=documentamt
			replace paid with xrpt.paid-xdocamt, ;
					lastactdt with m.docdt in xrpt
					
			m.paid=documentamt

			do case
			case type("gover45")="L" and gover45
				do case
				case m.origdocdt<=xasofdt-210
					m.amt5=documentamt
				case m.origdocdt<=xasofdt-150
					m.amt4=documentamt
				case m.origdocdt<=xasofdt-120
					m.amt3=documentamt
				case m.origdocdt<=xasofdt-90
					m.amt2=documentamt
				case between(m.origdocdt,xasofdt-90,xasofdt-46)
					m.amt1=documentamt
				endcase
				
			case type("gover90")="L" and gover90
				do case
				case m.origdocdt<=xasofdt-210
					m.amt5=documentamt
				case m.origdocdt<=xasofdt-180
					m.amt4=documentamt
				case m.origdocdt<=xasofdt-150
					m.amt3=documentamt
				case m.origdocdt<=xasofdt-120
					m.amt2=documentamt
				case between(m.origdocdt,xasofdt-120,xasofdt-91)
					m.amt1=documentamt
				endcase
				
			case type("gijoe")="L" and gijoe
				do case
				case m.origdocdt<=xasofdt-210
					m.amt8=documentamt
				case m.origdocdt<=xasofdt-180
					m.amt7=documentamt
				case m.origdocdt<=xasofdt-150
					m.amt6=documentamt
				case m.origdocdt<=xasofdt-120
					m.amt5=documentamt
				case m.origdocdt<=xasofdt-90
					m.amt4=documentamt
				case m.origdocdt<=xasofdt-60
					m.amt3=documentamt
				case m.origdocdt<=xasofdt-30
					m.amt2=documentamt
				otherwise
					m.amt1=documentamt
				endcase

			case type("glewislok")="L" and glewislok
				do case
				case documentdate<=xasofdt-360
					m.amt4=documentamt
				case documentdate<=xasofdt-180
					m.amt3=documentamt
				case documentdate<=xasofdt-90
					m.amt2=documentamt
				otherwise
					m.amt1=documentamt
				endcase

			otherwise
				do case
				case m.origdocdt<=xasofdt-90
					m.amt4=documentamt
				case m.origdocdt<=xasofdt-60
					m.amt3=documentamt
				case m.origdocdt<=xasofdt-30
					m.amt2=documentamt
				otherwise
					m.amt1=documentamt
				endcase
			endcase
		endif
assert m.docnum#"868288"
		insert into xrpt from memvar
		m.paid=0
	endscan
next

* unposted invoices

m.fromtable="H"

select farhdr
scan for invoicedate<=xasofdt
	store 0 to m.amt1, m.amt2, m.amt3, m.amt4, m.amt5, m.amt6, m.amt7, m.amt8, m.amt9, m.amt10
	m.billtoid=val(customerkey)
	m.docdt=invoicedate
	m.wo_num=0
	m.origdocdt={}
	m.lastactdt=m.docdt

	if !empty(invoicenumber)
		if xsqlexec("select * from invoice where invnum='"+left(invoicenumber,6)+"'","xinvoice",,"ar",,,,.t.) # 0 ;
			and ((type("gover90")#"L" or !gover90) or (type("gover45")#"L" or !gover45))

			m.accountid=eval("0x"+shiptokey)
			m.wo_num=val(customerponumber)
			m.docnum=left(invoicenumber,6)
			m.invnum=m.docnum
			m.amt1=documenttotal
			m.origdocdt=invoicedate 
			m.doctype="Invoice"
		else
			loop
		endif
	else
		m.invnum=left(applyto,6)
		m.doctype="Credit"

		if isdigit(applyto) or inlist(applyto,"Z","X","S")
			m.docnum=left(applyto,6)
			if seek(m.docnum,"invoice","invnum")
				if !empty(gaccountid) and invoice.accountid#gaccountid
					loop
				endif
				m.accountid=invoice.accountid
				m.wo_num=invoice.wo_num
				m.origdocdt=invoice.invdt
			else
				loop
			endif
		else
			if applyto="ONACT"
				if !empty(gaccountid) and m.billtoid#gaccountid
					loop
				endif
				m.docnum="ONACT"
				m.doctype="On Acct"
				if seek(m.billtoid,"account","accountid")
					m.accountid=m.billtoid
				else
					=seek(m.billtoid,"acctbill","billtoid")
					m.accountid=acctbill.accountid
				endif
				m.origdocdt=invoicedate
			else
				loop
			endif
		endif

		if empty(m.origdocdt)
			if (type("gover90")#"L" or !gover90) or ((type("gover45")#"L" or !gover45))
				m.amt1=-documenttotal
				m.lastactdt=m.docdt
			endif
		else
			if !seek(left(arhdr.applyto,6),"xrpt","invnum")
				asse .f.
			endif
			replace paid with xrpt.paid+arhdr.documenttotal, ;
					lastactdt with m.docdt in xrpt

			do case
			case type("gover45")="L" and gover45
				do case
				case m.origdocdt<=xasofdt-210
					m.amt5=-documenttotal
				case m.origdocdt<=xasofdt-150
					m.amt4=-documenttotal
				case m.origdocdt<=xasofdt-120
					m.amt3=-documenttotal
				case m.origdocdt<=xasofdt-90
					m.amt2=-documenttotal
				case between(m.origdocdt,xasofdt-90,xasofdt-46)
					m.amt1=-documenttotal
				endcase

			case type("gover90")="L" and gover90
				do case
				case m.origdocdt<=xasofdt-210
					m.amt5=-documenttotal
				case m.origdocdt<=xasofdt-180
					m.amt4=-documenttotal
				case m.origdocdt<=xasofdt-150
					m.amt3=-documenttotal
				case m.origdocdt<=xasofdt-120
					m.amt2=-documenttotal
				case between(m.origdocdt,xasofdt-120,xasofdt-91)
					m.amt1=-documenttotal
				endcase

			case type("gijoe")="L" and gijoe
				do case
				case m.origdocdt<=xasofdt-210
					m.amt8=-documentamt
				case m.origdocdt<=xasofdt-180
					m.amt7=-documenttotal
				case m.origdocdt<=xasofdt-150
					m.amt6=-documenttotal
				case m.origdocdt<=xasofdt-120
					m.amt5=-documenttotal
				case m.origdocdt<=xasofdt-90
					m.amt4=-documenttotal
				case m.origdocdt<=xasofdt-60
					m.amt3=-documenttotal
				case m.origdocdt<=xasofdt-30
					m.amt2=-documenttotal
				otherwise
					m.amt1=-documenttotal
				endcase

			case type("glewislok")="L" and glewislok
				do case
				case documentdate<=xasofdt-360
					m.amt4=documentamt
				case documentdate<=xasofdt-180
					m.amt3=documentamt
				case documentdate<=xasofdt-90
					m.amt2=documentamt
				otherwise
					m.amt1=documentamt
				endcase

			otherwise
				do case
				case m.origdocdt<=xasofdt-90
					m.amt4=-documenttotal
				case m.origdocdt<=xasofdt-60
					m.amt3=-documenttotal
				case m.origdocdt<=xasofdt-30
					m.amt2=-documenttotal
				otherwise
					m.amt1=-documenttotal
				endcase
			endcase
		endif
	endif

	insert into xrpt from memvar
endscan

* unposted credit memos

if !xnotunposted
	m.fromtable="H"
	select arhdr_cs
	scan for invoicedate<=xasofdt
		store 0 to m.amt1, m.amt2, m.amt3, m.amt4, m.amt5, m.amt6, m.amt7, m.amt8, m.amt9, m.amt10
		m.billtoid=val(customerkey)
		m.docdt=invoicedate
		m.wo_num=0
		m.origdocdt={}
		m.lastactdt=m.docdt

		if !empty(invoicenumber)
			if seek(left(invoicenumber,6),"invoice","invnum") and ((type("gover90")#"L" or !gover90) or (type("gover45")#"L" or !gover45))
				m.accountid=eval("0x"+shiptokey)
				m.wo_num=val(customerponumber)
				m.docnum=left(invoicenumber,6)
				m.invnum=m.docnum
				m.amt1=documenttotal
				m.origdocdt=invoicedate 
				m.doctype="Invoice"
			else
				loop
			endif
		else
			m.invnum=left(applyto,6)
			m.doctype="Credit"

			if isdigit(applyto) or inlist(applyto,"Z","X","S")
				m.docnum=left(applyto,6)
				if seek(m.docnum,"invoice","invnum")
					if !empty(gaccountid) and invoice.accountid#gaccountid
						loop
					endif
					m.accountid=invoice.accountid
					m.wo_num=invoice.wo_num
					m.origdocdt=invoice.invdt
				else
					loop
				endif
			else
				if applyto="ONACT"
					if !empty(gaccountid) and m.billtoid#gaccountid
						loop
					endif
					m.docnum="ONACT"
					m.doctype="On Acct"
					if seek(m.billtoid,"account","accountid")
						m.accountid=m.billtoid
					else
						=seek(m.billtoid,"acctbill","billtoid")
						m.accountid=acctbill.accountid
					endif
					m.origdocdt=invoicedate
				else
					loop
				endif
			endif

			if empty(m.origdocdt)
				if (type("gover90")#"L" or !gover90) or (type("gover45")#"L" or !gover45)
					m.amt1=-documenttotal
					m.lastactdt=m.docdt
				endif
			else
				if !seek(left(arhdr_cs.applyto,6),"xrpt","invnum")
					asse .f.
				endif
				replace paid with xrpt.paid+arhdr_cs.documenttotal, ;
						lastactdt with m.docdt in xrpt

				do case
				case type("gover45")="L" and gover45
					do case
					case m.origdocdt<=xasofdt-210
						m.amt5=-documenttotal
					case m.origdocdt<=xasofdt-150
						m.amt4=-documenttotal
					case m.origdocdt<=xasofdt-120
						m.amt3=-documenttotal
					case m.origdocdt<=xasofdt-90
						m.amt2=-documenttotal
					case between(m.origdocdt,xasofdt-90,xasofdt-46)
						m.amt1=-documenttotal
					endcase

				case type("gover90")="L" and gover90
					do case
					case m.origdocdt<=xasofdt-210
						m.amt5=-documenttotal
					case m.origdocdt<=xasofdt-180
						m.amt4=-documenttotal
					case m.origdocdt<=xasofdt-150
						m.amt3=-documenttotal
					case m.origdocdt<=xasofdt-120
						m.amt2=-documenttotal
					case between(m.origdocdt,xasofdt-120,xasofdt-91)
						m.amt1=-documenttotal
					endcase

				case type("gijoe")="L" and gijoe
					do case
					case m.origdocdt<=xasofdt-210
						m.amt8=-documenttotal
					case m.origdocdt<=xasofdt-180
						m.amt7=-documenttotal
					case m.origdocdt<=xasofdt-150
						m.amt6=-documenttotal
					case m.origdocdt<=xasofdt-120
						m.amt5=-documenttotal
					case m.origdocdt<=xasofdt-90
						m.amt4=-documenttotal
					case m.origdocdt<=xasofdt-60
						m.amt3=-documenttotal
					case m.origdocdt<=xasofdt-30
						m.amt2=-documenttotal
					otherwise
						m.amt1=-documenttotal
					endcase

				case type("glewislok")="L" and glewislok
					do case
					case documentdate<=xasofdt-360
						m.amt4=documentamt
					case documentdate<=xasofdt-180
						m.amt3=documentamt
					case documentdate<=xasofdt-90
						m.amt2=documentamt
					otherwise
						m.amt1=documentamt
					endcase

				otherwise
					do case
					case m.origdocdt<=xasofdt-90
						m.amt4=-documenttotal
					case m.origdocdt<=xasofdt-60
						m.amt3=-documenttotal
					case m.origdocdt<=xasofdt-30
						m.amt2=-documenttotal
					otherwise
						m.amt1=-documenttotal
					endcase
				endcase
			endif
		endif

		insert into xrpt from memvar
	endscan
endif

* unposted payments

if !xnotunposted
	m.fromtable="S"
	select arselect
	scan for recdate<=xasofdt
		store 0 to m.amt1, m.amt2, m.amt3, m.amt4, m.amt5, m.amt6, m.amt7, m.amt8, m.amt9, m.amt10
		m.billtoid=val(customerkey)
		m.invnum=left(applyto,6)
		m.docdt=recdate 
		m.doctype="Payment"
		m.wo_num=0
		m.origdocdt={}

		if isdigit(applyto) or inlist(applyto,"Z","X","S")
			m.docnum=left(applyto,6)
			if seek(m.docnum,"invoice","invnum")
				if !empty(gaccountid) and invoice.accountid#gaccountid
					loop
				endif
				m.accountid=invoice.accountid
				m.wo_num=invoice.wo_num
				m.origdocdt=invoice.invdt
			else
				loop
			endif
		else
			loop
		endif
		
		if empty(m.origdocdt)
			if (type("gover90")#"L" or !gover90) or (type("gover45")#"L" or !gover45)
				m.amt1=-paidamt
				m.lastactdt=m.docdt
			endif
		else
*			=seek(m.docnum,"xrpt","docnum")
			if !seek(left(arselect.applyto,6),"xrpt","invnum")
*				asse .f.
			endif
			replace paid with xrpt.paid+arselect.paidamt, ;
					lastactdt with m.docdt in xrpt

			do case
			case type("gover45")="L" and gover45
				do case
				case m.origdocdt<=xasofdt-210
					m.amt5=-paidamt
				case m.origdocdt<=xasofdt-150
					m.amt4=-paidamt
				case m.origdocdt<=xasofdt-120
					m.amt3=-paidamt
				case m.origdocdt<=xasofdt-90
					m.amt2=-paidamt
				case between(m.origdocdt,xasofdt-90,xasofdt-46)
					m.amt1=-paidamt
				endcase

			case type("gover90")="L" and gover90
				do case
				case m.origdocdt<=xasofdt-210
					m.amt5=-paidamt
				case m.origdocdt<=xasofdt-180
					m.amt4=-paidamt
				case m.origdocdt<=xasofdt-150
					m.amt3=-paidamt
				case m.origdocdt<=xasofdt-120
					m.amt2=-paidamt
				case between(m.origdocdt,xasofdt-120,xasofdt-91)
					m.amt1=-paidamt
				endcase

			case type("gijoe")="L" and gijoe
				do case
				case m.origdocdt<=xasofdt-210
					m.amt8=-paidamt
				case m.origdocdt<=xasofdt-180
					m.amt7=-paidamt
				case m.origdocdt<=xasofdt-150
					m.amt6=-paidamt
				case m.origdocdt<=xasofdt-120
					m.amt5=-paidamt
				case m.origdocdt<=xasofdt-90
					m.amt4=-paidamt
				case m.origdocdt<=xasofdt-60
					m.amt3=-paidamt
				case m.origdocdt<=xasofdt-30
					m.amt2=-paidamt
				otherwise
					m.amt1=-paidamt
				endcase

			case type("glewislok")="L" and glewislok
				do case
				case documentdate<=xasofdt-360
					m.amt4=documentamt
				case documentdate<=xasofdt-180
					m.amt3=documentamt
				case documentdate<=xasofdt-90
					m.amt2=documentamt
				otherwise
					m.amt1=documentamt
				endcase

			otherwise
				do case
				case m.origdocdt<=xasofdt-90
					m.amt4=-paidamt
				case m.origdocdt<=xasofdt-60
					m.amt3=-paidamt
				case m.origdocdt<=xasofdt-30
					m.amt2=-paidamt
				otherwise
					m.amt1=-paidamt
				endcase
			endcase
		endif
		insert into xrpt from memvar	
	endscan
endif

* set report fields

select xrpt
index on dtos(origdocdt)+docnum+iif(empty(docdt),"Z       ",dtos(docdt))+doctype tag rptorder
replace all owed with amt1+amt2+amt3+amt4+amt5+amt6+amt7+amt8+amt9+amt10-paid

set deleted off
scan for owed=0 and doctype="Invoice" and (empty(goffsetsince) or lastactdt<goffsetsince)
	xrecno=recno()
	xinvnum=invnum
	delete for invnum=xinvnum
	go xrecno
endscan
set deleted on

* this fixes a fucked-up bug with the payment on G6721 and others
delete for inlist(wo_num,379908,369107,364691,374305,360289,378750,381920)

* takes care of archived data
delete for docdt<{5/7/09}	&& dy 6/19/09  4/13/11	5/7/09

* fix for Jim 6/14/11
delete for docnum="613684"

* fix for Jim 4/27/16
delete for docnum="S0"

* per Juan & Jim 2/2/17
* delete for accountid=6695 and docnum="ONACT" and inlist(amt1+amt2+amt3+amt4+amt5+amt6+amt7+amt8+amt9+amt10,-1495.23,-44254.06)

* per Juan & Jim 2/2/17
delete for docnum='35569A'

* per Jim 1/30/18
delete for docnum='252612'

