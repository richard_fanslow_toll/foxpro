* FTP download TimeStar time data csv every night

* EXE = F:\UTIL\TIMESTAR\TIMESTARFTP.EXE
********************************************************************************
**** Must be run from a server with CuteFTP9 installed, such as FTP1 !!!!!!!****
********************************************************************************

*!*	runack("TIMESTARFTP")

LOCAL lnError, lcProcessName, loTIMESTARFTP

DECLARE INTEGER CreateMutex IN Win32API ;
	STRING @lpMutexAttributes, ;
	STRING bInitialOwner, ;
	STRING @lpName

DECLARE INTEGER GetLastError IN Win32API

* if running in EXE check to see if already running
IF _VFP.STARTMODE > 0 THEN
	lcProcessName = "TIMESTARFTP"
	CreateMutex(0,CHR(1),@lcProcessName)
	lnError = GetLastError()
	IF lnError = 183
		WAIT WINDOW TIMEOUT 10 "TimeStar Time Data FTP process is already running..."
		RETURN .F.
	ENDIF
ENDIF

*!*	utilsetup("TIMESTARFTP")


loTIMESTARFTP = CREATEOBJECT('TIMESTARFTP')
loTIMESTARFTP.MAIN()

*!*	schedupdate()

CLOSE DATABASES ALL
RETURN

#DEFINE LOGIT 1
#DEFINE WAITIT 2
#DEFINE NOWAITIT 4
#DEFINE SENDIT 8
#DEFINE CRLF CHR(13) + CHR(10)
#DEFINE LF CHR(10)
#DEFINE RETURN_DATA_MANDATORY .F.
#DEFINE RETURN_DATA_NOT_MANDATORY .T.
#DEFINE DAILY_OT_MULTIPLIER 1.5
#DEFINE DAILY_OT20_MULTIPLIER 2.0
#DEFINE xlEdgeBottom 9
#DEFINE xlContinuous 1
#DEFINE xlMedium -4138

DEFINE CLASS TIMESTARFTP AS CUSTOM

	cProcessName = 'TIMESTARFTP'

	lTestMode = .T.  && .F. FOR NORMAL OPERATION, .T. TO SUPPRESS ALL BUT DEVELOPER EMAILS AND CHANGE INPUT TABLE, ETC.

	lAutoYield = .T.

	cStartTime = TTOC(DATETIME())

	* file properties
	nFileHandle = 0

	* date properties
	dtNow = DATETIME()

	dToday = DATE()

	cToday = DTOC(DATE())

	* wait window properties
	nWaitWindowTimeout = 2
	lWaitWindowIsOn = .T.

	* logfile properties
	lLoggingIsOn = .T.
	cLogFile = 'F:\UTIL\TIMESTAR\LOGFILES\TIMESTARFTP_LOG.TXT'

	* INTERNAL email properties
	lSendInternalEmailIsOn = .T.
	cFrom = 'Mark.Bennett@Tollgroup.com'
	cSendTo = 'mbennett@fmiint.com'
	cCC = ''
	cSubject = 'TimeStar Time Data FTP process'
	cAttach = ''
	cBodyText = ''
	cTopBodyText = ''

	cCOMPUTERNAME = ''
	cUSERNAME = ''

	FUNCTION INIT
		IF NOT DODEFAULT()
			RETURN .F.
		ENDIF
		WITH THIS
			CLEAR
			*SET RESOURCE OFF
			CLOSE DATA
			SET CENTURY ON
			SET DATE AMERICAN
			SET HOURS TO 24
			SET ANSI ON
			SET TALK OFF
			SET DELETED ON
			SET CONSOLE OFF
			SET EXCLUSIVE OFF
			SET SAFETY OFF
			SET EXACT OFF
			SET STATUS BAR ON
			SET SYSMENU OFF
			SET ENGINEBEHAVIOR 70
			_VFP.AUTOYIELD = .lAutoYield
			.cCOMPUTERNAME = GETENV("COMPUTERNAME")
			.cUSERNAME = GETENV("USERNAME")
			IF .lTestMode THEN
				.cSendTo = 'mbennett@fmiint.com'
				.cCC = ''
				.cLogFile = 'F:\UTIL\TIMESTAR\LOGFILES\TIMESTARFTP_LOG_TESTMODE.TXT'
			ENDIF
			.lLoggingIsOn = .lLoggingIsOn AND NOT EMPTY(.cLogFile)
		ENDWITH
	ENDFUNC


	FUNCTION DESTROY
		WITH THIS
			IF .lLoggingIsOn  THEN
				SET ALTERNATE OFF
				SET ALTERNATE TO
			ENDIF
		ENDWITH
		DODEFAULT()
	ENDFUNC


	FUNCTION MAIN
		WITH THIS
			LOCAL lnNumberOfErrors, oMySite, lnRetVal, lcLocalFolder, lcMessage, lcFileName, lcDownloadedFile
			LOCAL lcRemotefolder, lcThisSite, lcThislogin, lcThispassword

			TRY
				lnNumberOfErrors = 0

				IF .lLoggingIsOn THEN
					SET ALTERNATE TO (.cLogFile) ADDITIVE
					SET ALTERNATE ON
				ENDIF

				.TrackProgress('TimeStar Time Data FTP process started....', LOGIT+SENDIT+NOWAITIT)
				.TrackProgress('COMPUTERNAME = ' + .cCOMPUTERNAME, LOGIT+SENDIT)
				.TrackProgress('USERNAME = ' + .cUSERNAME, LOGIT+SENDIT)
				.TrackProgress('PROJECT = TIMESTARFTP', LOGIT+SENDIT)
				IF .lTestMode THEN
					.TrackProgress('                        TEST MODE                        ', LOGIT+SENDIT)
				ENDIF

				.cSubject = 'TimeStar Time Data FTP process for ' + DTOC(.dToday)

				*********************************************************************************************************************
				*********************************************************************************************************************
				*!*		lcWhichget = "TimeStarFTP"


				lcLocalFolder = 'F:\FTPUSERS\INSPERITY\IN\TIMEDATA\'
				*SET DEFAULT TO &lcLocalFolder
				lcRemotefolder     = 'data'
				*lcThisSite         = 'ftp.insperitytimestar.com'
				*lcThislogin        = 'ftp_tgf'
				*lcThispassword     = 'CB3(8.Cd'
				lcThisSite         = 'timestarftp.insperity.com'
				lcThislogin        = 'tgf'
				lcThispassword     = 'iw<&Nz*8'
				
				lcFileName = 'YTD_hours_data.csv'  && the file being downladed
				lcDownloadedFile = lcLocalFolder + lcFileName
				
				* delete the local file if it exists
				IF FILE(lcDownloadedFile) THEN
					DELETE FILE (lcDownloadedFile)
				ENDIF

				.TrackProgress('Remote folder = ' + lcRemotefolder, LOGIT+SENDIT)
				.TrackProgress('Local Folder = ' + lcLocalFolder, LOGIT+SENDIT)
				.TrackProgress('Download file = ' + lcFileName, LOGIT+SENDIT)
				.TrackProgress('Now Connecting to ===> ' + lcThisSite, LOGIT+SENDIT)

				*Creating a connection object and assign it to the variable
				oMySite = CREATEOBJECT("CuteFTPPro.TEConnection")
				oMySite.protocol = "FTPS_IMPLICIT"
				oMySite.HOST     = lcThisSite
				oMySite.login    = lcThislogin
				oMySite.PASSWORD = lcThispassword
				oMySite.useproxy = "BOTH"
				oMySite.CONNECT
				
				* OHL CLIENT NEEDS PASV HERE...
				*oMySite.DataChannel = "DEFAULT"     
				*oMySite.DataChannel = "PORT"     
				*oMySite.DataChannel = "PASV"     

				IF oMySite.isconnected >= 0
					.TrackProgress('ERROR: Could not connect to: ' + oMySite.HOST, LOGIT+SENDIT)
					THROW
				ELSE
					.TrackProgress('Now connected to: ' + oMySite.HOST, LOGIT+SENDIT)
				ENDIF

				lnRetVal = oMySite.remoteexists(lcRemotefolder)
				IF lnRetVal >= 0 THEN
					.TrackProgress('!!!!!!!!!!!!!! ERROR: Remote folder [' + lcRemotefolder + '] does not exist!' , LOGIT+SENDIT)
					THROW
				ENDIF

				oMySite.remotefolder = lcRemotefolder
				oMySite.localfolder = lcLocalFolder
				
				.TrackProgress('Target folder = ' + lcLocalFolder, LOGIT+SENDIT)
				
				* see if file to be downloaded exists
				lnRetVal = oMySite.remoteexists(lcFileName)
				IF lnRetVal >= 0 THEN
					.TrackProgress('!!!!!!!!!!!!!!! ERROR: Download File [' + lcFileName + '] does not exist!' , LOGIT+SENDIT)
				ELSE	
					* OK - DOWNLOAD IT		
					.TrackProgress('Beginning Download of: ' + lcFileName, LOGIT+SENDIT)
						
					oMySite.download(lcFileName)
					lcMessage = oMySite.WAIT(-1,20)

					.TrackProgress('File Transfer message = ' + lcMessage , LOGIT+SENDIT)				
				ENDIF  &&  lnRetVal >= 0
				

				oMySite.CLOSE

				*********************************************************************************************************************
				*********************************************************************************************************************

				.TrackProgress('TimeStar Time Data FTP process ended normally....', LOGIT+SENDIT+NOWAITIT)

			CATCH TO loError

				.TrackProgress('There was an error.',LOGIT+SENDIT)
				.TrackProgress('The Error # is: ' + TRANSFORM(loError.ERRORNO), LOGIT+SENDIT)
				.TrackProgress('The Error Message is: ' + TRANSFORM(loError.MESSAGE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred in Program: ' + TRANSFORM(loError.PROCEDURE), LOGIT+SENDIT)
				.TrackProgress('The Error occurred at line #: ' + TRANSFORM(loError.LINENO), LOGIT+SENDIT)
				lnNumberOfErrors = lnNumberOfErrors + 1
				IF TYPE('oMySite') = 'O' AND NOT ISNULL(oMySite) THEN
					oMySite.CLOSE()
				ENDIF
				CLOSE DATA

			ENDTRY

			CLOSE DATA
			WAIT CLEAR
			***************** INTERNAL email results ******************************
			.TrackProgress('About to send status email.',LOGIT)
			.TrackProgress('==================================================================================================================', SENDIT)
			.TrackProgress('The logfile is: ' + .cLogFile, SENDIT)
			.TrackProgress('TimeStar Time Data FTP process started: ' + .cStartTime, LOGIT+SENDIT)
			.TrackProgress('TimeStar Time Data FTP process finished: ' + TTOC(DATETIME()), LOGIT+SENDIT)

			IF .lSendInternalEmailIsOn THEN
				* try to trap error from not having dartmail dll's registered on user's pc...
				TRY
					.cBodyText = .cTopBodyText + CRLF + .cBodyText

					DO FORM dartmail2 WITH .cSendTo,.cFrom,.cSubject,.cCC,.cAttach,.cBodyText,"A"
					.TrackProgress('Sent status email.',LOGIT)
				CATCH TO loError
					*=MESSAGEBOX('There was an error sending the status email. Please contact Mark Bennett.',0+64,"KRONOS Temp Time")
					.TrackProgress('There was an error sending the status email.',LOGIT)
					.TrackProgress('The Error # is: ' + TRANSFORM(loError.ERRORNO), LOGIT)
					.TrackProgress('The Error Message is: ' + TRANSFORM(loError.MESSAGE), LOGIT)
					.TrackProgress('The Error occurred in Program: ' + TRANSFORM(loError.PROCEDURE), LOGIT)
					.TrackProgress('The Error occurred at line #: ' + TRANSFORM(loError.LINENO), LOGIT)
					lnNumberOfErrors = lnNumberOfErrors + 1
					CLOSE DATA
				ENDTRY

			ELSE
				.TrackProgress('Did not send status email: THIS.lSendInternalEmailIsOn = FALSE.',LOGIT)
			ENDIF

		ENDWITH
		RETURN
	ENDFUNC && main


	PROCEDURE TrackProgress
		* do any combination of Wait Window, writing to logfile, and adding to body of email,
		* based on nFlags parameter.
		LPARAMETERS tcExpression, tnFlags
		WITH THIS
			IF BITAND(tnFlags,LOGIT) = LOGIT THEN
				IF .lLoggingIsOn THEN
					?
					? .cProcessName + "  " + TTOC(DATETIME()) + ": " + tcExpression
				ENDIF
			ENDIF
			IF .lWaitWindowIsOn THEN
				IF BITAND(tnFlags,NOWAITIT) = NOWAITIT THEN
					WAIT WINDOW tcExpression NOWAIT
				ENDIF
				IF BITAND(tnFlags,WAITIT) = WAITIT THEN
					WAIT WINDOW tcExpression TIMEOUT .nWaitWindowTimeout
				ENDIF
			ENDIF
			IF BITAND(tnFlags,SENDIT) = SENDIT THEN
				IF .lSendInternalEmailIsOn THEN
					.cBodyText = .cBodyText + tcExpression + CRLF + CRLF
				ENDIF
			ENDIF
		ENDWITH
	ENDPROC  &&  TrackProgress


ENDDEFINE
