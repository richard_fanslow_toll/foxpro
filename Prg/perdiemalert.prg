* Notifies pending per diems

utilsetup("PERDIEMALERT")

guserid="AUTO"

use f:\wo\wodata\drop in 0

if usesqlmt()
	xsqlexec("select * from empties",,,"wo")
else
	use f:\wo\wodata\empties in 0
endif

for xdy=1 to 3
	do case
	case xdy=1
		xoffice="N"
		xfilter=".t."
		xofficename="Carteret"
		xfilename="h:\pdf\perdiemalertnj.xls"
	case xdy=2
		xoffice="C"
		xfilter="accountid#5837"
		xofficename="San Pedro non-Sears"
		xfilename="h:\pdf\perdiemalertca.xls"
	otherwise
		xoffice="C"
		xfilter="accountid=5837"
		xofficename="San Pedro Sears"
		xfilename="h:\pdf\perdiemalertsears.xls"
	endcase
	
	select "DROP " as type, wo_num, acctname, container, size, ssco, delloc, pdstartdt, pdstartdt-date() as daysleft ;
		from drop where pdstartdt<date()+5 and office=xoffice and empty(trailer) and !empty(container) and &xfilter ;
	union ;
	select "EMPTY" as type, wo_num, acctname, container, size, ssco, padr(yardloc,30) as delloc, pdstartdt, pdstartdt-date() as daysleft ;
		from empties where pdstartdt<date()+5 and office=xoffice and &xfilter ;
	into cursor xrpt order by pdstartdt, acctname, wo_num readwrite
	
	if xoffice="N"
		replace all delloc with "CARTERET, NJ"
	else
		scan for type="EMPTY"
			do case
			case delloc="CA"
				replace delloc with "CARSON, CA"
			case delloc="CI"
				replace delloc with "CITY OF INDUSTRY, CA"
			case delloc="GC"
				replace delloc with "GILBERT CHINO"
			case delloc="MO"
				replace delloc with "MACY'S ONTARIO"
			case delloc="ML"
				replace delloc with "MIRA LOMA, CA"
			case delloc="PL"
				replace delloc with "PLG"
			case delloc="RI"
				replace delloc with "RIALTO, CA"
			case delloc="SP"
				replace delloc with "SAN PEDRO, CA"
			case delloc="SG"
				replace delloc with "SG SAN BERNARDINO"
			case delloc="WI"
				replace delloc with "WILMINGTON, CA"
			endcase	
		endscan
	endif

	select xrpt
	copy to (xfilename) xls
		
	if xoffice="N"
		tsendto = "mike.drew@tollgroup.com, joe.damato@tollgroup.com, thomas.keaveney@tollgroup.com, nadine.donovan@tollgroup.com, olga.cruz@tollgroup.com, irena.pagurek@tollgroup.com, fran.castro@tollgroup.com,"+" jim.lake@tollgroup.com" 
	else
		tsendto = "cesar.nevares@tollgroup.com, annie.najera@tollgroup.com"
	endif

	tcc=""
	tattach = xfilename
	tfrom ="TGF <support@fmiint.com>"
	tmessage = ""
	tsubject = "Per Diem Alert for "+xofficename

	Do Form dartmail2 With tsendto,tFrom,tSubject,tcc,tattach,tmessage,"A"
next

schedupdate()

_screen.Caption=gscreencaption
on error
