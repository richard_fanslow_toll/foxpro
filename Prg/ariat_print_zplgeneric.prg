Parameter xprinter

**** below code used in the label print program to sekect the label records to print

*!*  Locate For sel
*!*  If !Found()
*!*    =Messagebox("Please make a selection before continuing.", 16, "")
*!*    Return
*!*  Else
*!*    lnRunid = tdata.runid
*!*  Endif

*!*  lcFilename = Sys(3)
*!*  lcFilename ="h:\fox\"+lcFilename+".dbf"
*!*  Copy To &lcFilename For sel
*!*  Use &lcFilename In 0 Alias xdata
************************************************************************************

if !empty(xprinter)
	lcPtr=xprinter
Else
  lcPtr = "\\dc3\pgzebra"
endif

** not sure if we need there but it wont hurt
!net Use lpt1 /Delete
!net Use lpt1 &lcPtr


** point to a wave label record of maybe a query to a cursor xdata

lcStr =""
lcStr2=""

Select xdata
  lcStr = Filetostr("f:\wh\zpltemplates\ariat_generic_zpl.txt")

  lcStr = Strtran(lcStr,"<SHIPFROM>",    Alltrim(xdata.acct_name))
  lcStr = Strtran(lcStr,"<SHIPFROMADDR>",Alltrim(xdata.sfromaddr1))
  lcStr = Strtran(lcStr,"<SHIPFROMCSZ>", Alltrim(xdata.sfromcsz))
  lcStr = Strtran(lcStr,"<SHIPTO>",      Alltrim(xdata.shipto))
  lcStr = Strtran(lcStr,"<SHIPTOADDR1>", Alltrim(xdata.staddr1))
  If!Empty(xdata.staddr2)
    lcStr = Strtran(lcStr,"<SHIPTOADDR2>", Alltrim(xdata.staddr2))
  Else
    lcStr = Strtran(lcStr,"<SHIPTOADDR2>", "")
  Endif
  lcStr = Strtran(lcStr,"<SHIPTOCSZ>",     Alltrim(xdata.stcsz))
  lcStr = Strtran(lcStr,"<(420) 12345>",   "(420) "+Substr(Alltrim(xdata.stzip),1,5))
  lcStr = Strtran(lcStr,"42012345",        "420"+Substr(Alltrim(xdata.stzip),1,5))
  lcStr = Strtran(lcStr,"<PICKTICKET>",    "PT: "+Alltrim(xdata.pt))
  lcStr = Strtran(lcStr,"<PONUM>",         "PO: "+Alltrim(xdata.cust_po))
  lcStr = Strtran(lcStr,"<XXXXSHIPVIAXXXX>",     " " )
  lcStr = Strtran(lcStr,"<DEPARTMENT>",    "DEPT:"+Alltrim(xdata.dept))
  lcStr = Strtran(lcStr,"<MARKFORSTORE>",  "STORE:"+Alltrim(xdata.sforstore))
  lcStr = Strtran(lcStr,"<(91) 123456>",   "(91) "+Alltrim(xdata.sforstore))
  lcStr = Strtran(lcStr,"91123456",        "91"+Alltrim(xdata.sforstore))

  If Empty(Alltrim(xdata.n51)) Or Alltrim(xdata.n51) = "NA"
   lcStr = Strtran(lcStr,"<CUSTOMERORDER>", " ")  && A CUSTOMER ORDER#
   lcStr = Strtran(lcStr,"<N51>",           " ")  && A CUSTOMER ORDER#
  else
   lcStr = Strtran(lcStr,"<CUSTOMERORDER>","CUSTOMER ORDER#:")  && A CUSTOMER ORDER#
   lcStr = Strtran(lcStr,"<N51>",           Alltrim(xdata.n51))  && A CUSTOMER ORDER#
  Endif 

  lcStr = Strtran(lcStr,"<WONUM>",         "WO#:"+Alltrim(Transform(xdata.wo_num)))
  lcStr = Strtran(lcStr,"80000510200000218128",Alltrim(xdata.uccnumber))
  lcStr = Strtran(lcStr,"<UCCNUMBER>","("+Substr(xdata.uccnumber,1,2)+") "+Substr(xdata.uccnumber,3,1)+" "+Substr(xdata.uccnumber,4,7)+" "+Substr(xdata.uccnumber,12)+" "+Alltrim(chkdigit))

  lcStr = Strtran(lcStr,"<RUNID>",  "SWC:"+Alltrim(Transform(xdata.runid)))
  lcStr = Strtran(lcStr,"<PRINTDATE>",  " PRINTED: "+Alltrim(Ttoc(Datetime())))


** ^FT53,1198^BCN,294,N,N,Y^FD>;><80000510200000218128^FS

  lcOutStr = lcStr
  Strtofile(lcOutStr,"h:\fox\temp.txt")
  lcfile = "h:\fox\temp.txt"
  !Type &lcfile > lpt1

Use In xdata

