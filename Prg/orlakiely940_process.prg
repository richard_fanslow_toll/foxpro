*!* m:\dev\prg\tjm940_process.prg
IF ltesting
	WAIT WINDOW "Default folder is "+lcpath TIMEOUT 2
ENDIF
CD &lcpath

lctranspath  = "f:\ftpusers\"+ccustname+"\940translate\"
lc997path    = "f:\ftpusers\"+ccustname+"\997in\"

logcommentstr = ""

lnnum = ADIR(tarray,"*.xlsx")

IF lnnum = 0
	WAIT WINDOW AT 10,10 "      No "+ccustname+" 940s to import     " TIMEOUT 3
	normalexit = .T.
	RETURN
ENDIF

WAIT WINDOW "At 940 files loop..." NOWAIT

FOR thisfile = 1  TO lnnum
	cfilenamexx = (lcpath+"temp1.xls")
	IF FILE(cfilenamexx)
		DELETE FILE [&cFilenamexx]
	ENDIF
	cISA_Num = TTOC(DATETIME(),1)

	WAIT "" TIMEOUT 2
	xfile = lcpath+TRIM(tarray[thisfile,1])
	cfilename = LOWER(TRIM(tarray[thisfile,1]))
	nFileSize = VAL(TRANSFORM(tarray(thisfile,2)))
	IF !FILE(xfile)
		LOOP
	ENDIF

	CREATE CURSOR intake (ship_ref c(20),cacctnum c(10),consignee c(35),address1 c(40),address2 c(40),stcity c(25),ststate c(2),stzip c(10),country c(15),;
		shipfor c(35),sforaddr1 c(20),sforaddr2 c(20),sfcity c(25),sfstate c(2),sfzip c(10),sfcountry c(15),start1 c(10),cancel1 c(10),cnee_ref c(25),shipmode c(10),;
		STYLE c(20),upc c(20),totqty N(10),totqty2 N(10),DESC c(50),color c(10),id c(10),csz c(40),sforcsz c(40),start d,cancel d)

	oExcel = CREATEOBJECT("Excel.Application")
	oWorkbook = oExcel.Workbooks.OPEN(xfile)
	WAIT WINDOW "Now saving file "+cfilename NOWAIT NOCLEAR
	oWorkbook.SAVEAS(cfilenamexx,39)  && re-saves Excel sheet as XL95/97 file
	WAIT CLEAR
	WAIT WINDOW "File save complete...continuing" TIMEOUT 1
	oWorkbook.CLOSE(.T.)
	oExcel.QUIT()
	RELEASE oExcel

	SELECT intake
	DELETE ALL
	APPEND FROM [&cfilenamexx] TYPE XL5
	LOCATE
	IF ISALPHA(intake.ship_ref)
		DELETE NEXT 1 IN intake
	ENDIF

	REPLACE intake.csz       WITH ALLTRIM(UPPER(intake.stcity))+", "+ALLTRIM(UPPER(intake.ststate))+" "+ALLT(intake.stzip) ALL IN intake
	REPLACE intake.sforcsz   WITH ALLTRIM(UPPER(intake.sfcity))+", "+ALLTRIM(UPPER(intake.sfstate))+" "+ALLT(intake.sfzip) ALL IN intake
	REPLACE intake.sforaddr1 WITH UPPER(intake.sforaddr1) ALL IN intake
	REPLACE intake.sforaddr2 WITH UPPER(intake.sforaddr2) ALL IN intake
	REPLACE intake.DESC      WITH UPPER(intake.DESC) ALL IN intake
	REPLACE intake.consignee WITH UPPER(intake.consignee) ALL IN intake
	REPLACE intake.address1  WITH UPPER(intake.address1) ALL IN intake
	REPLACE intake.address2  WITH UPPER(intake.address2) ALL IN intake
	REPLACE intake.csz       WITH UPPER(intake.csz) ALL IN intake
	REPLACE intake.sforcsz   WITH UPPER(intake.sforcsz) ALL IN intake
	replace intake.start     WITH CTOD(intake.start1) ALL IN intake
	replace intake.cancel    WITH CTOD(intake.cancel1) ALL  IN intake
  replace intake.color     WITH Upper(intake.color) ALL  IN intake
  replace intake.id        WITH Upper(intake.id) ALL  IN intake

*set step On 

	IF FILE(cfilenamexx)
		DELETE FILE [&cFilenamexx]
	ENDIF
	SELECT intake

	IF ltesting
		LOCATE
*		BROWSE
*		SET STEP ON
	ENDIF

	archivefile = lcarchivepath+cfilename
	transfile = lctranspath++TRIM(tarray[thisfile,1])
	IF !ltesting
*		COPY FILE [&xfile] TO [&Transfile]  && creates 997 file
	ENDIF

	IF !FILE(archivefile)
		COPY FILE [&xfile] TO [&archivefile]
	ENDIF
	WAIT "" TIMEOUT 2
	
	lLoadSQL = .t.
	DO create_pt_cursors WITH cusefolder,lloadsql && Added to prep for SQL Server transition

	DO ("m:\dev\prg\"+ccustname+"940_bkdn")

	ldoImport = .t.
	*SET STEP ON 
	IF !ldoImport
		LOOP
	ELSE
		DO m:\dev\prg\all940_import
	ENDIF

	IF FILE(xfile) && AND !ltesting
		DELETE FILE [&xfile]
	ENDIF

ENDFOR

IF !ltesting
	deletefile(lcarchivepath,20)
ENDIF

WAIT CLEAR
WAIT "Entire "+ccustname+" 940 Process Now Complete" WINDOW TIMEOUT 2
