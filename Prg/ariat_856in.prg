PUBLIC xfile,asn_origin,TranslateOption,lcScreenCaption,etadate,tsendto,tcc,tsendtoerr,tccerr,cCustname,cMod,lcPath,cfilename,lDoSQL,cWhse
PUBLIC nFileSize,cISA_Num,xfile,LogCommentStr
runack("ARIATASN")
xfile = ""
etadate=""
lcTempID = ""

CLOSE DATA ALL
&& these variables help with the EDIINLog store in SQL
cOffice="K"
cMod="K"
nAcctNum = 6532
gOffice = cOffice
gMasterOffice = cOffice
guserid = "ARIAT856"
cCustname="ARIAT"
lcPath=""
cfilename=""
nFileSize = 0
cISA_Num=""
xfile =""
LogCommentStr =""
&&----------------------------------------------------

ltesting = .F.
lOverridebusy = .F.
gMasterOffice ="K"

DO m:\dev\prg\_setvars WITH ltesting
SET STEP ON
ON ERROR DO fmerror WITH ERROR(), PROGRAM(), LINENO(1), SYS(16),, .T.

lcScreenCaption = "Ariat 856 ASN Uploader Process... Processing File: "
_SCREEN.CAPTION = lcScreenCaption

WAIT WINDOW AT 10,10 " Now uploading Ariat 856's........." TIMEOUT 1
TranslateOption = "CR_TILDE" &&"NONE"
asn_origin = "ARIAT"

SELECT 0
USE F:\edirouting\FTPSETUP SHARED
LOCATE FOR FTPSETUP.TRANSFER = "ARIAT-856IN"
IF FOUND()
	IF chkbusy AND !lOverridebusy
		WAIT WINDOW "Ariat 856 Process is busy...exiting" TIMEOUT 2
		NormalExit = .T.
		RETURN
	ENDIF
ENDIF
REPLACE chkbusy WITH .T.,trig_time WITH DATETIME()  FOR FTPSETUP.TRANSFER = "ARIAT-856IN"
USE IN FTPSETUP

*Assert .F. Message "At MM Config load"
SELECT 0
USE F:\3pl\DATA\mailmaster ALIAS mm SHARED
LOCATE FOR mm.accountid = nAcctNum AND edi_type = "856"
IF FOUND()
	STORE TRIM(mm.basepath) TO lcPath
	STORE TRIM(mm.archpath) TO lcArchivePath
	tsendto = IIF(mm.use_alt,TRIM(mm.sendtoalt),TRIM(mm.sendto))
	tcc = IIF(mm.use_alt,TRIM(mm.ccalt),TRIM(mm.cc))
	STORE TRIM(mm.scaption) TO thiscaption
	STORE mm.testflag      TO lTest
	_SCREEN.CAPTION = thiscaption
	LOCATE FOR (mm.edi_type = "MISC") AND (mm.taskname = "GENERAL")
	IF ltesting
		tsendto = IIF(mm.use_alt,TRIM(mm.sendtoalt),TRIM(mm.sendto))
		tcc = IIF(mm.use_alt,TRIM(mm.ccalt),TRIM(mm.cc))
	ENDIF
	tsendtoerr = IIF(mm.use_alt,TRIM(mm.sendtoalt),TRIM(mm.sendto))
	tccerr = IIF(mm.use_alt,TRIM(mm.ccalt),TRIM(mm.cc))
	USE IN mm
ELSE
	WAIT WINDOW AT 10,10  "No parameters set for this acct# "+ALLTRIM(STR(nAcctNum))+"  ---> Office "+cOffice TIMEOUT 2
	USE IN mm
	NormalExit = .T.
	THROW
ENDIF

DO createx856

*Set Step On
WAIT WINDOW AT 10,10  "Processing ARIAT 856s............." TIMEOUT 1

IF ltesting
	cUseFolder = "F:\WHP\WHDATA\"
	cOffice="P"
ELSE
	xReturn = "XX"
	DO m:\dev\prg\wf_alt WITH cOffice,nAcctNum
	cUseFolder = UPPER(xReturn)
ENDIF
cWhse = LOWER("wh"+cOffice)

*USE (cUseFolder+"whgenpk") IN 0 ALIAS whgenpk	dy 12/26/17

useca("pl","wh")
xsqlexec("select * from pl where .f.","xpl",,"wh")
xsqlexec("select * from pl where .f.","tpl",,"wh")

useca("inwolog","wh")
xsqlexec("select * from inwolog where .f.","xinwolog",,"wh")
xsqlexec("select * from inwolog where .f.","tinwo",,"wh")

useca("ctnucc","wh")
xsqlexec("select * from ctnucc where 1=0","tctnucc",,"wh")

DELIMITER = "*"
DO ProcessARIATasn

SELECT 0
USE F:\edirouting\FTPSETUP SHARED
REPLACE chkbusy WITH .F. FOR FTPSETUP.TRANSFER = "ARIAT-856IN"
USE IN FTPSETUP

***********************************************************************************************************
PROCEDURE ProcessARIATasn

*lnNum = Adir(tarray,"f:\ftpusers\ariat-asns\in\*.*")
	lnNum = ADIR(tarray,"F:\FTPUSERS\Ariat\856IN\*.*")

	IF ltesting
		lcPath = "f:\ftpusers\ariat-test\asnin\"
		lnNum = ADIR(tarray,"f:\ftpusers\ariat-test\asnin\*.*")
	ENDIF
	SET STEP ON
	IF lnNum > 0
		FOR thisfile = 1  TO lnNum
			xfile = lcPath+tarray[thisfile,1]  &&+"."
			cfilename =tarray[thisfile,1]  &&+"."
			archivefile = lcArchivePath+tarray[thisfile,1]  &&+"."
			nFileSize = VAL(TRANSFORM(tarray(thisfile,2)))
			WAIT WINDOW "Importing file: "+xfile NOWAIT
			DO loadedifile WITH xfile,"*",TranslateOption,"JONES856"

			SELECT x856
			LOCATE FOR x856.segment = 'GS'
			IF x856.f1 = "FA"
				WAIT WINDOW "This is a 997 FA file...moving to correct folder" TIMEOUT 2
				cfile997in = ("F:\ftpusers\ariat-asns\997in\"+ALLTRIM(JUSTFNAME(xfile)))
				COPY FILE [&xfile] TO [&cfile997in]
				DELETE FILE [&xfile]
				LOOP
			ENDIF
			DO ImportAriatasn
			COPY FILE &xfile TO &archivefile
			IF FILE(archivefile)
				DELETE FILE &xfile
			ENDIF
		NEXT thisfile
	ELSE
		WAIT WINDOW  "No 856's to Import" TIMEOUT 1
	ENDIF

ENDPROC

******************************************************************************************************************
PROCEDURE ImportAriatasn
******************************************************************************************************************

&&Try
	lnPrfCtr = 0

	SET CENTURY ON
	SET DATE TO YMD

	SELECT x856
	GOTO TOP

	lcHLLevel = ""
	lcVendorCode = ""
	lcCurrentHAWB = ""

	testing=.F.

	POs_added = 0
	ShipmentNumber = 1
	ThisShipment = 0
	origawb = ""
	lcCurrentShipID = ""
	lcCurrentArrival = ""
	LAFreight = .F.

** here we clear out asn data already loaded from this file
	SELECT x856
	SET FILTER TO
	GOTO TOP
	m.suppdata = ""
	m.awb =""

* Set Step On
	DO WHILE !EOF("x856")
		WAIT WINDOW "At the outer loop " NOWAIT

		IF TRIM(x856.segment) = "ISA"
			cISA_Num=ALLTRIM(x856.f13)
			m.isa   = ALLTRIM(x856.f13)
			m.dateloaded = DATETIME()
			m.asnfile    = UPPER(JUSTFNAME(xfile))
			SELECT x856
			SKIP 1 IN x856
			LOOP
		ENDIF

		IF INLIST(x856.segment,"GS","ST")
			SELECT x856
			SKIP 1 IN x856
			LOOP
		ENDIF

		IF TRIM(x856.segment) = "BSN"&& and alltrim(x856.f1) = "05"  && must delete then add new info
			m.shipid = ALLTRIM(x856.f2)
			m.bsn    = ALLTRIM(x856.f2)
			SELECT x856
			SKIP 1 IN x856
			LOOP
		ENDIF

		IF AT("HL",x856.segment)>0
			lcHLLevel = ALLTRIM(x856.f3)
		ENDIF

		IF lcHLLevel = "S"
			SELECT tctnucc
			SCATTER MEMVAR MEMO BLANK FIELDS EXCEPT isa,bsn,asnfile,hawb,shipid
			SELECT x856
			m.loaddt = DATE()
			m.adddt = DATETIME()
			addby = "ARIAT856"
			addproc ="TGFAUTO"
			m.dateloaded = DATETIME()
			m.filename   = UPPER(xfile)
			DO WHILE lcHLLevel = "S"

				IF TRIM(x856.segment) = "TD1"
					m.qty     = VAL((ALLTRIM(x856.f2)))
					m.suppdata = m.suppdata +CHR(13)+"TOTCTNS*"+ALLTRIM(x856.f2)
					m.weight  = VAL((ALLTRIM(x856.f7)))
					m.suppdata = m.suppdata +CHR(13)+"TOTWEIGHT*"+ALLTRIM(x856.f7)
					m.cube    = VAL((ALLTRIM(x856.f9)))
					m.suppdata = m.suppdata +CHR(13)+"TOTCUBE*"+ALLTRIM(x856.f9)
				ENDIF

				IF TRIM(x856.segment) = "TD5"
					IF ALLTRIM(x856.f7) = "PA"
						m.destport = ALLTRIM(x856.f8)
					ENDIF
					IF ALLTRIM(x856.f7) = "OR"
						m.originport =  ALLTRIM(x856.f8)
					ENDIF
				ENDIF

				IF TRIM(x856.segment) = "REF" AND UPPER(TRIM(x856.f1)) = "BL"
					m.bol = ALLTRIM(x856.f2)
				ENDIF

				IF TRIM(x856.segment) = "REF" AND UPPER(TRIM(x856.f1)) = "AW"
					m.awb = ALLTRIM(x856.f2)
				ENDIF

				IF TRIM(x856.segment) = "REF" AND UPPER(TRIM(x856.f1)) = "PK"
					m.packinglistnumber = ALLTRIM(x856.f2)
				ENDIF

				IF TRIM(x856.segment) = "DTM" AND ALLTRIM(x856.f1)= "056"
					m.documentdate = ALLTRIM(x856.f2)  &&Substr(Alltrim(x856.f2),5,2)+"/"+Substr(Alltrim(x856.f2),7,2)+"/"+Substr(Alltrim(x856.f2),1,4)\
					etadate= SUBSTR(ALLTRIM(x856.f2),5,2)+"/"+SUBSTR(ALLTRIM(x856.f2),7,2)+"/"+SUBSTR(ALLTRIM(x856.f2),1,4)
				ENDIF

				SELECT x856
				SKIP 1 IN x856
				IF TRIM(x856.segment) = "HL"
					lcHLLevel = TRIM(x856.f3)
				ENDIF
			ENDDO
		ENDIF   && end if HL S

		IF lcHLLevel = "E"
			DO WHILE lcHLLevel = "E"
				IF TRIM(x856.segment) = "TD3"
					m.container = TRIM(x856.f2) +TRIM(x856.f3)
					m.seal = TRIM(x856.f9)
					m.equiptype = TRIM(x856.f1)
					m.seal = TRIM(x856.f9)
				ENDIF
				SELECT x856
				SKIP 1 IN x856
				IF TRIM(x856.segment) = "HL"
					lcHLLevel = TRIM(x856.f3)
				ENDIF
			ENDDO
		ENDIF

		IF lcHLLevel = "O"
			DO WHILE lcHLLevel = "O"
				IF TRIM(x856.segment) = "N1"
					m.warehouse     =  ALLTRIM(x856.f2)
					m.warehousecode =  ALLTRIM(x856.f4)
				ENDIF
				SELECT x856
				SKIP 1 IN x856
				IF TRIM(x856.segment) = "HL"
					lcHLLevel = TRIM(x856.f3)
				ELSE
					LOOP
				ENDIF
			ENDDO
		ENDIF

		IF lcHLLevel = "P"
			DO WHILE lcHLLevel = "P"
				IF TRIM(x856.segment) = "MAN"
					lnPrfCtr = 0
					lcTempID = SUBSTR(ALLTRIM(x856.f2),11)
					m.ucc =  ALLTRIM(x856.f2)
					m.suppdta = ""
					m.suppdata = m.suppdata+CHR(13)+"DOCUMENTDATE*"+m.documentdate
					m.suppdata = m.suppdata+CHR(13)+"SEAL*"+m.seal
					m.suppdata = m.suppdata+CHR(13)+"EQUIPTYPE*"+m.equiptype
					m.suppdata = m.suppdata+CHR(13)+"WAREHOUSE*"+m.warehouse
					m.suppdata = m.suppdata+CHR(13)+"WHCODE*"+m.warehousecode
					m.suppdata = m.suppdata+CHR(13)+"PACKINGLISTNUMBER*"+m.packinglistnumber
				ENDIF

				SELECT x856
				IF !EOF("x856")
					SKIP 1 IN x856
				ELSE
					EXIT
				ENDIF
				IF TRIM(x856.segment) = "HL"
					lcHLLevel = TRIM(x856.f3)
				ENDIF
			ENDDO
		ENDIF

		IF lcHLLevel = "I"
			DO WHILE lcHLLevel = "I"
				IF TRIM(x856.segment) = "LIN"
					m.linenum =  ALLTRIM(x856.f1)
					m.suppdata = m.suppdata +CHR(13)+"LINENUM*"+ALLTRIM(x856.f1)
					m.style   =  SUBSTR(ALLTRIM(x856.f3),11)
					m.suppdata = m.suppdata +CHR(13)+"ORIGSIZE*"+ALLTRIM(x856.f7)
					m.upc     =  ALLTRIM(x856.f5)
					m.color   =  ALLTRIM(x856.f7)
					IF m.color ="00000"
						m.color=""
					ELSE
						m.color   = STRTRAN(m.color,"   "," ")
						m.color   = STRTRAN(m.color,"  "," ")
						IF ".5"$m.color AND !" "$ALLTRIM(m.color)
							m.color = STRTRAN(m.color,".5",".5 ")
						ENDIF
					ENDIF
					m.cat     =  ALLTRIM(x856.f9)
					m.suppdata = m.suppdata +CHR(13)+"CAT*"+ALLTRIM(m.cat)
				ENDIF

				IF TRIM(x856.segment) = "SN1"
					m.pack = ALLTRIM(x856.f2)
					m.totqty   = VAL(ALLTRIM(x856.f2))
					m.suppdata = m.suppdata +CHR(13)+"ITEMQTY*"+ALLTRIM(x856.f2)
					m.uom    = ALLTRIM(x856.f3)
					m.suppdata = m.suppdata +CHR(13)+"ITEMTYPE*"+ALLTRIM(m.uom)
				ENDIF

				IF TRIM(x856.segment) = "PRF"
					lnPrfCtr = lnPrfCtr +1
					m.ponum =  ALLTRIM(x856.f1)
					SELECT tctnucc
					m.acctname ="ARIAT"
					m.accountid = 6532
					m.office = "K"
					SKIP 1 IN x856
					IF (x856.f3) = "P" OR ALLTRIM(x856.segment) ="CTT"  && after the last PO loop is the ending CTT segment
						SKIP -1 IN x856
						IF !lnPrfCtr > 1
							m.id =""
							lcTempID = ""
						ENDIF
					ELSE
						m.id = lcTempID
						SKIP -1 IN x856
					ENDIF

					APPEND BLANK
					GATHER MEMVAR MEMO
					m.suppdata = ""
				ENDIF

				SELECT x856
				IF !EOF("x856")
					SKIP 1 IN x856
				ELSE
					EXIT
				ENDIF
				IF TRIM(x856.segment) = "HL"
					lcHLLevel = TRIM(x856.f3)
				ENDIF
			ENDDO
		ENDIF

		IF EOF("x856")
			EXIT
		ENDIF
	ENDDO &&!eof("x856")

	SELECT tctnucc
	COPY TO h:\fox\tctnucc

	LogCommentStr = LogCommentStr+ALLTRIM(m.container)+"-"+ALLTRIM(m.bol)+"-"+m.awb+"-"+m.shipid

*DO
*asn_in_data()
	DO StoreASNData
	DO CreateInbound
	DO Import_Data

	LogCommentStr = ""
	m.awb=""

ENDPROC

**********************************************************************************************************************
PROCEDURE CreateInbound
	m.plid =0
	m.inwologid =0
*Set Step On

	SELECT tctnucc

	SELECT ucc,STYLE,COLOR,ID,CONTAINER,bol,asnfile,shipid,SUM(totqty) AS inqty FROM tctnucc GROUP BY ucc,STYLE,COLOR,CONTAINER,bol, asnfile INTO CURSOR tempinb READWRITE

	SELECT ucc,COUNT(*) AS CNT FROM tempinb GROUP BY ucc HAVING CNT>1 INTO CURSOR tempmixed READWRITE && these are mixed cartons

	SELECT tempmixed
	SCAN
		SELECT tempinb
		LOCATE FOR tempmixed.ucc = tempinb.ucc
		IF FOUND()
			DELETE FOR tempmixed.ucc = tempinb.ucc IN tempinb
		ENDIF
	ENDSCAN

	SELECT ucc,STYLE,COLOR,ID,SUM(inqty),COUNT(*) AS CNT FROM tempinb GROUP BY STYLE,COLOR,ID,inqty INTO CURSOR tempfullctns READWRITE

*Set Step On

	SELECT tempinb
	GO TOP

	SCATTER MEMVAR
	m.container = tempinb.CONTAINER
	m.reference = tempinb.bol
	m.brokerref = tempinb.shipid
	m.comments = "ASN Upload"+CHR(13)+;
		"ASN FILE: "+tempinb.asnfile

	m.inwologid = m.inwologid + 1
	INSERT INTO xinwolog FROM MEMVAR

	groupctr = 1
	SELECT tempfullctns
	LOCATE
	SCAN
		SCATTER MEMVAR
		m.units  = .F.
		m.id = ""
		m.totqty = tempfullctns.CNT
		m.style  = tempfullctns.STYLE
		m.color  = tempfullctns.COLOR
*  Set Step On
		m.pack   = ALLTRIM(TRANSFORM(tempfullctns.sum_inqty/tempfullctns.CNT))
		m.po = TRANSFORM(groupctr)
		m.plid = m.plid + 1
		INSERT INTO xpl FROM MEMVAR

		m.units  = .T.
		m.totqty = tempfullctns.sum_inqty
		m.style  = tempfullctns.STYLE
		m.color  = tempfullctns.COLOR
		m.pack   = "1"
		m.po = TRANSFORM(groupctr)
		m.plid = m.plid + 1
		INSERT INTO xpl FROM MEMVAR
		groupctr = groupctr +1
	ENDSCAN

	SELECT tempinb
	RECALL ALL
	m.plid = m.plid

	SELECT tempmixed
	GOTO TOP
	SCAN
		SELECT tempinb
		SELECT * FROM tempinb WHERE tempinb.ucc = tempmixed.ucc INTO CURSOR temp READWRITE
		SELECT temp
		SUM inqty TO ctnpack
		SELECT temp
		GO TOP
		SCATTER MEMVAR
		m.units  = .F.
		m.totqty = 1
		m.po = TRANSFORM(groupctr)
*  m.style  = m.id
		m.color  = m.id
		m.pack   = ALLTRIM(TRANSFORM(ctnpack))
		m.po = TRANSFORM(groupctr)
		INSERT INTO xpl FROM MEMVAR
		REPLACE ID WITH "" IN xpl

		SELECT temp
		SCAN
			m.units  = .T.
			m.totqty = temp.inqty
			m.style  = temp.STYLE
			m.color  = temp.COLOR
			m.pack   = "1"
			m.po = TRANSFORM(groupctr)
			m.plid = m.plid + 1
			INSERT INTO xpl FROM MEMVAR
			REPLACE ID WITH "" IN xpl
		ENDSCAN
		groupctr = groupctr +1
	ENDSCAN

*Set Step On

	SELECT xpl
	SUM totqty TO totunits FOR units = .T.
	SUM totqty TO totctns FOR units = .F.

	SELECT xinwolog
	GO TOP
	REPLACE xinwolog.plunitsinqty WITH totunits IN xinwolog
	REPLACE xinwolog.plinqty      WITH totctns IN xinwolog

*Set Step On
ENDPROC
**********************************************************************************************************************
PROCEDURE Import_Data

	SELECT xinwolog
	LOCATE

	SCAN  && Currently in XINWOLOG table
		SELECT inwolog
		cOffice = "K"
		SELECT xinwolog
		SCATTER MEMVAR MEMO
		nwo_num = dygenpk("wonum",cWhse)
		m.wo_num = nwo_num
		m.wo_date = DATE()
		m.addproc ="ARIAT856"
		m.accountid = nAcctNum
		cWO_Num = ALLT(STR(m.wo_num))

		insertinto("inwolog","wh",.T.)
		tu("inwolog")
	ENDSCAN

	SELECT xpl
	SCAN
		SCATTER MEMVAR MEMO
		m.mod = cMod
		m.office = cOffice
		m.inwologid = inwolog.inwologid
		m.accountid = nAcctNum
		m.wo_num = nwo_num
		insertinto("pl","wh",.T.)
	ENDSCAN
	tu("pl")

&& OK, now take the records in tctnucc and move them into the production table

	select tctnucc
	scan 
		scatter memvar memo
		m.inwonum = inwolog.wo_num
		insertinto("ctnucc","wh",.t.)
	endscan
	
	tu("ctnucc")
	
* Deanna Nelson <Deanna.Nelson@tollgroup.com>
* Cheri Foster <Cheri.Foster@tollgroup.com>
* Kimberly Sallee <kimberly.sallee@tollgroup.com>
* Jace Sipes <Jace.Sipes@tollgroup.com>

** OK now send the email

	tfrom    ="TGF/ARIAT EDI Poller Operations <transload-ops@fmiint.com>"
	tsubject = "Ariat ASN Uploader WO#: "+ALLTRIM(TRANSFORM(inwolog.wo_num))+"  Container: "+inwolog.CONTAINER+ "  ETA: "+etadate
	tattach =""
	tmessage = "Inbound Created for Toll-Louisville Warehouse"+CHR(13)
	tmessage = tmessage +;
		"-------------------------------------------------"+CHR(13)+;
		"Work Order    :"+ALLTRIM(TRANSFORM(inwolog.wo_num))+CHR(13)+;
		"Container     :"+inwolog.CONTAINER+CHR(13)+;
		"Filename      : "+xfile+CHR(13)+;
		"ETA           : "+etadate+CHR(13)+;
		"BOL           : "+TRANSFORM(inwolog.REFERENCE)+CHR(13)+;
		"Reference     : "+TRANSFORM(inwolog.brokerref)+CHR(13)+;
		"# of Cartons  : "+ALLTRIM(TRANSFORM(inwolog.plinqty))+CHR(13)+;
		"Containing    : "+ALLTRIM(TRANSFORM(inwolog.plunitsinqty))+" Units"

	DO FORM dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"

	SELECT xinwolog
	ZAP
	SELECT xpl
	ZAP
	SELECT tctnucc
	ZAP
	SELECT x856
	ZAP
	etadate = ""

ENDPROC

**********************************************************************************************************************
PROCEDURE StoreASNData
**********************************************************************************************************************
	lcQuery = [select * from ediinlog where accountid = ]+TRANSFORM(nAcctNum)+[ and isanum = ']+cISA_Num+[']
	xsqlexec(lcQuery,"p1",,"stuff")
	IF RECCOUNT()=0
		USE IN p1
		useca("ediinlog","stuff")  && Creates updatable PTHIST cursor

*      LogCommentStr = "Pick tickets uploaded"
		SELECT ediinlog
		SCATTER MEMVAR MEMO
		m.ediinlogid = dygenpk("ediinlog","WHALL")
		m.acct_name  = cCustname
		m.accountid  = nAcctNum
		m.mod        = cMod
		m.office     = cOffice
		m.filepath   = lcPath
		m.filename   = cfilename
		m.size       = nFileSize
		m.FTIME      = DATE()
		m.FDATETIME  = DATETIME() &&tarray(thisfile,4)
		m.TYPE       = "856"
		m.qty        = 0
		m.isanum     =  cISA_Num
		m.uploadtime = DATETIME()
		m.comments   = LogCommentStr
		m.edidata    = ""
		INSERT INTO ediinlog FROM MEMVAR
		APPEND MEMO edidata FROM &xfile
		tu("ediinlog")
	ENDIF
**********************************************************************************************************************
