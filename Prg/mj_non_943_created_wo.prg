utilsetup("MJ_NON_943_CREATED_WO")
Set Exclusive Off
Set Deleted On
Set Talk Off
Set Safety Off 

goffice='J'
useca("inwolog","wh",,,"mod='"+goffice+"' and  accountid in (6303,6325,6543) and confirmdt>{"+dtoc(date()-10)+"}")

SELECT accountid, wo_num, acct_ref from inwolog;
where INLIST(accountid,6303,6543) AND EMPTY(confirmdt) AND LEN(ALLTRIM(comments))=0; 
AND SUBSTR(reference,1,3)!='MAN'  AND !("CAN" $ PRINTCOMMENTS) AND acct_ref !='TRAN'  AND !transfer AND !zRETURNS;
INTO CURSOR nonwonj
set step on 
goffice='L'
useca("inwolog","wh",,,"mod='"+goffice+"' and  accountid in (6303,6325,6543) and confirmdt>{"+dtoc(date()-10)+"}",,"inwologml")

SELECT accountid, wo_num, acct_ref from inwologml;
where INLIST(accountid,6303,6543) AND EMPTY(confirmdt) AND LEN(ALLTRIM(comments))=0; 
AND SUBSTR(reference,1,3)!='MAN'  AND !("CAN" $ PRINTCOMMENTS) AND acct_ref !='TRAN'  AND !transfer AND !zRETURNS;
INTO CURSOR nonwoml

SELECT * FROM nonwonj UNION select * FROM nonwoml INTO CURSOR nonwo READWRITE 

SELECT nonwo
If Reccount() > 0
  Export To "S:\MarcJacobsData\943_manually_created\manually_created_943_" + Ttoc(Datetime(), 1)  Type Xls
  Export To "S:\MarcJacobsData\943_manually_created\manually_created_943" Type Xls

  tsendto = "tmarg@fmiint.com,pgaidis@fmiint.com"
  tattach = "S:\MarcJacobsData\943_manually_created\manually_created_943.xls"
  tcc =""
  tFrom ="Toll EDI System Operations <fmi-transload-ops@fmiint.com>"
  tmessage = "MANUALLY_CREATED_943_"+Ttoc(Datetime())+"  in  S:\MarcJacobsData\943_manually_created\"
  tSubject = "MANUALLY_CREATED_943"
  Do Form m:\dev\frm\dartmail2 With tsendto,tFrom,tSubject, tcc,tattach,tmessage  ,"A"
ELSE 
  tsendto = "tmarg@fmiint.com"
  tattach = ""
  tcc =""
  tFrom ="Toll EDI System Operations <fmi-transload-ops@fmiint.com>"
  tmessage = "NO_manually_created_943s_exist_"+Ttoc(Datetime())
  tSubject = "NO_manually_created_943s_exist"
  Do Form m:\dev\frm\dartmail2 With tsendto,tFrom,tSubject, tcc,tattach,tmessage  ,"A"
Endif

Close Data All
schedupdate()
_Screen.Caption=gscreencaption