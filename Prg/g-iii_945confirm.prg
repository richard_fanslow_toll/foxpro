
SET ESCAPE ON
ON ESCAPE CANCEL

CLEAR
CLEAR ALL
CLOSE DATABASES ALL
PUBLIC nAccountid,cFilename,dconfirm_dt,lTesting,dDaysBack,cOffice,cState,tsendto,tcc,lUnconfirmed,nfor,lTesting,lStartDigit,lEmail

USE F:\3pl\DATA\edi_trigger IN 0
DO m:\dev\prg\_setvars WITH .T.
DO m:\dev\prg\lookups

lTesting = .T.
lEmail = .T.

_SCREEN.WINDOWSTATE = IIF(lTesting,2,1)
IF lTesting
	lookups()
ELSE
	utilsetup("G-III_945CONFIRM")
ENDIF

IF !lTesting
	SELECT 0
	USE F:\edirouting\FTPSETUP SHARED
	REPLACE chkbusy WITH .F. FOR FTPSETUP.TRANSFER = "997-G3-IN-SP"
	USE IN FTPSETUP
ENDIF

FOR j = 1 TO 3
	CLEAR
	cExcelFile=""
	lExit = .F.
	DO CASE
		CASE j = 1
			cState = "SP"
			cOffice = "C"
		CASE j = 2
			cState = "NJ"
			cOffice = "I"
		OTHERWISE
			cState = "CR"
			cOffice = "X"
	ENDCASE

	WAIT WINDOW "Now Checking Received Confirmations in EDI_TRIGGER for State "+cState TIMEOUT 2
	check_trigger_confirm()
ENDFOR

IF lTesting
	SELECT edi_trigger
	BROWSE FOR g3account(edi_trigger.accountid) AND !ack945 AND when_proc>DATETIME(2013,10,01,00,00,01)
ENDIF

CLOSE DATA ALL
WAIT WINDOW "Process complete" TIMEOUT 2

*******************************
PROCEDURE check_trigger_confirm
*******************************
*SET STEP ON
	IF lTesting
		ASSERT .F. MESSAGE "In Trigger Confirm Mailing"
	ENDIF
	WAIT WINDOW "Now scanning EDI_TRIGGER ("+cState+") for overdue confirmations" NOWAIT
	SELECT accountid,ICASE(cState="NJ","Carteret",cState="CR","Carson","San Pedro") AS warehouse,bol,ship_ref,trig_time ;
		FROM edi_trigger ;
		WHERE g3account(accountid) ;
		AND BETWEEN(TTOD(trig_time),DATE()-10,DATE()) ;
		AND edi_type = "945" ;
		AND !EMPTY(when_proc) ;
		AND when_proc < (DATETIME()-3600) ;
		AND !errorflag ;
		AND fin_status # "NO 945" ;
		AND EMPTY(confirm_dt) ;
		AND office = cOffice ;
		INTO CURSOR tempconf
	SELECT tempconf
	LOCATE
	IF EOF()
		WAIT WINDOW "No unconfirmed 945s for "+cState TIMEOUT 2
		lUnconfirmed = .F.
	ELSE
		IF lTesting
			BROWSE
		ENDIF
		COPY TO ("c:\tempfox\giii_noconfirm"+cState)
		LOCATE
		lUnconfirmed = .T.

		WAIT WINDOW "Errors found...creating Excel error file and mailing" TIMEOUT 3
		cExcelFile = ("F:\3pl\DATA\missconfirms_"+cState+".xls")
		COPY TO [&cExcelFile] TYPE XL5
	ENDIF

	IF lUnconfirmed
		tsubject= "Unconfirmed G-III 945s: Office "+cOffice
		tattach = cExcelFile
		tmessage = "The attached Excel file contains PTs for which we have not received Moret 997s."
	ELSE
		tsubject= "No unconfirmed G-III 945s: Office "+cOffice+" ("+cState+")"
		tattach = ""
		tmessage = "We have received G-III 997s for all sent 945s."
	ENDIF
	tFrom ="Toll EDI Operations <toll-edi-ops@tollgroup.com>"
	SELECT 0
	USE F:\3pl\DATA\mailmaster ALIAS mm SHARED
	IF lTesting OR lTestmail
		LOCATE FOR edi_type = "MISC" AND taskname = "JOETEST"
	ELSE
		LOCATE FOR edi_type = "945" AND GROUP = "G-III"
	ENDIF
	tsendto = IIF(mm.use_alt,sendtoalt,sendto)
	tcc = IIF(mm.use_alt,ccalt,cc)
	USE IN mm

	IF lEmail
		DO FORM m:\dev\frm\dartmail2 WITH tsendto,tFrom,tsubject,tcc,tattach,tmessage,"A"
	ENDIF
	IF FILE(cExcelFile)
		DELETE FILE [&cExcelFile]
	ENDIF
	USE IN tempconf
ENDPROC
