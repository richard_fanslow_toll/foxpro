parameters xtollcustcode

**remove once using with parameters
xtollcustcode="ALBERTSONS"
*xtollcustcode="ITWARKNBB"

if empty(xtollcustcode)
	xFrom="Toll EDI System"
	xSubject="XLS 850 To ICON Transalation Cannot Be Run - Empty Toll Customer Code!!"
	xbody=""
	do form dartmail2 with "mwinter@fmiint.com",xfrom,xsubject," ",,xbody,"a"
	return
endif

wait xtollcustcode+" 850 processing..." window nowait noclear

**all setup done here by customer
do case
case xtollcustcode="ALBERTSONS"
	xtestmode=.f.

	xindir="f:\ftpusers\albertsons\850in\"
	xoutdir=iif(xtestmode,"f:\ftpusers\toll\albertsons\850test\","f:\ftpusers\toll\albertsons\850out\")
	x997dir="f:\ftpusers\albertsons\850-997\"

	xcustname="Albertson's"
	xftpjob=iif(xtestmode,"ALB-TEST-850-ICON","ALB-850-ICON")

	xhdrrowcnt=1 &&number of header rows to be deleted

	**all the column to field mappings
	xpocol="a"
	xpolinecol="d"

case xtollcustcode="ITWARKNBB"
	xtestmode=.t.

	xindir="f:\ftpusers\itw\850in\"
	xoutdir=iif(xtestmode,"f:\ftpusers\toll\itw\850test\","f:\ftpusers\toll\itw\850out\")
	x997dir="f:\ftpusers\itw\850-997\"

	xcustname="ITW"
	xftpjob=iif(xtestmode,"ITW-TEST-850-ICON","ITW-850-ICON")

	**changed from 3, latest test file only had 1 row of header
	xhdrrowcnt=1 &&number of header rows to be deleted

	use f:\ftpusers\itw\data\podata in 0
	select *, .f. as found from podata where .f. into cursor xpodata readwrite

	**all the column to field mappings
	xpocol="d"
	xpolinecol="e"

otherwise
	xFrom="Toll EDI System"
	xSubject="XLS 850 To ICON Transalation Failed - Customer Code '"+xtollcustcode+"' Not Setup!!"
	xbody="xlstoicon850.prg"
	do form dartmail2 with "mwinter@fmiint.com",xfrom,xsubject," ",,xbody,"a"
	wait clear
	return
endcase
**end customer setup


**check if files exist in the inbound folder
xfilecnt=Adir(afiles,xindir+"*.xls*")
if xfilecnt=0
	wait clear
	return
endif

set century on
set hours to 24

create cursor xfilelist (file c(100), x c(10), moddt C(10), modtm c(10), moddttm t(1))
append from array afiles
replace all moddttm with ctot(moddt+" "+modtm)

**handle 997 creation
*!*	scan
*!*		xfile=alltrim(xfilelist.file)
*!*		xinfile=xindir+alltrim(xfilelist.file)
*!*		xfs=filetostr(xinfile)
*!*		if "ST*997*"$xfs
*!*			**save in 997in in case we need them
*!*			copy file &xinfile to &x997in.&xfile
*!*			delete file &xinfile
*!*			delete in xfilelist
*!*		else
*!*			**copy ONLY 850 files to the 997 directory so 997s are sent as needed - mvw 06/08/12
*!*			copy file &xinfile to &x997dir.&xfile
*!*		endif
*!*	endscan

use f:\sysdata\qqdata\unloccode in 0
use f:\sysdata\qqdata\uom in 0

xfilecreated=.f.
select * from xfilelist order by moddttm into cursor xfilelist

scan
	if used("x850")
		use in x850
	endif

	xerrormsg=""
	xfilestr=""

	xfilename=alltrim(xfilelist.file)
	xfile = xindir+alltrim(xfilelist.file)
	xarchiveinfile = xindir+"archive\"+alltrim(xfilelist.file)

	xxfile = xindir+"xtemp.xls"+iif(lower(right(alltrim(xfile),1))="x","x","")
	xxorigfile = xxfile &&in case xlsx, need to delete it as well... xxfile changes to xls afetr saveas
	copy file "&xfile" to "&xxfile"

	oexcel = createobject("excel.application")
	oexcel.displayalerts = .f.
	oworkbook = oexcel.workbooks.open(xxfile,,.f.)

	**must save as csv then as xls 95/97 because we've encountered issues with dates, something to do with formatting i guess - mvw 09/17/13
*	xxfile=iif(lower(right(alltrim(xxfile),1))="x", left(alltrim(xxfile),len(alltrim(xxfile))-1), xxfile)
*	oworkbook.saveas(xxfile,39) && �39� converts the save format to excel95/97
	xxcsvfile=xindir+"xtemp.csv"
	oworkbook.saveas(xxcsvfile,6) && �6� converts the save format to csv
	oworkbook.close()

	oworkbook = oexcel.workbooks.open(xxcsvfile,,.f.)
	xxfile=iif(lower(right(alltrim(xxfile),1))="x", left(alltrim(xxfile),len(alltrim(xxfile))-1), xxfile)
	oworkbook.saveas(xxfile,39) && �39� converts the save format to excel95/97

	oworkbook.close()
	oexcel.quit()
	release oexcel

	select 0
	**need to append into a cursor as oppoed to import bc some fields dont come in correctly
*!*		import from "&xxfile" type xls
	create cursor xtemp (a c(50),b c(50),c c(50),d c(50),e c(50),f c(50),g c(50),h c(50),i c(50),j c(50),k c(50),l c(50),m c(50),;
		n c(50),o c(50),p c(50),q c(50),r c(50),s c(50),t c(50),u c(50),v c(50),w c(50),x c(50),y c(50),z c(50),aa c(50),ab c(50),;
		ac c(50),ad c(50),ae c(50),af c(50),ag c(50),ah c(50),ai c(50),aj c(50),ak c(50),al c(50),am c(50),an c(50),ao c(50),;
		ap c(50),aq c(50),ar c(50),as c(50),at c(50),au c(50),av c(50),aw c(50),ax c(50),ay c(50),az c(50))
	append from "&xxfile" type xls
	locate

	delete file "&xxcsvfile"
	delete file "&xxfile"
	if file("&xxorigfile")
		delete file "&xxorigfile"
	endif
	
	select xtemp
	delete next xhdrrowcnt &&delete header line(s)

	delete for empty(&xpocol) &&delete extra lines in file (empty ponum)
	replace all &xpocol with alltrim(&xpocol), &xpolinecol with alltrim(&xpolinecol), v with alltrim(v)

	do case
	case xtollcustcode="ALBERTSONS"
		select  &xpocol as ponum, ;
				&xpolinecol as lineno, ;
				iif(upper(ap)="CANCEL","CAN","PLC") as action, ;
				upper(z) as suppliercode, ;
				upper(ak) as suppliername, ;
				"" as supplieraddr1, ;
				"" as supplieraddr2, ;
				"" as suppliercity, ;
				"" as supplierstate, ;
				"" as suppliercountry, ;
				"" as supplierzip, ;
				upper(c) as reqdt, ;
				upper(b) as orderdt, ;
				"USD" as currency, ;
				"FOB" as incoterms, ; &&mandatory for Icon
				"SEA" as shipvia, ;
				"" as customdate1, ;
				upper(ab) as fobloccode, ;
				padr(iif(v="8710","USMJA",iif(v="8720","USPNC",iif(v="8750","USIVN",iif(v="8779","USEWI",iif(v="8790","USDNQ",iif(v="8220","USOLQ",;
					iif(v="8261","USBQE",iif(v="8354","USWZM",iif(v="8231","USNSL",iif(v="8252","USPDX",iif(v="8500","USTPE",iif(v="8560","USTRC",;
					iif(v="8608","USAUJ",iif(v="8913","USHBN","USNYC")))))))))))))),10) as delivport, ; &&default to NYC so doesnt error out
				"" as or_customtext1, ;
				"" as or_customtext2, ;
				"" as or_customtext3, ;
				"" as contact1, ;
				"" as msg, ;
				upper(i) as product, ;
				upper(e) as description, ;
				upper(t) as qty, ;
				"" as innerpack, ;
				"" as xouterpack, ;
				"" as uom, ;
				upper(s) as unitprice, ;
				upper(u) as itemreqdt, ;
				"" as it_customtext1, ;
				"" as it_customtext2, ;
				"" as it_customtext3, ;
				"" as it_customtext4, ;
				"" as it_customtext5, ;
				"" as customdecimal1, ;
				"" as customdecimal2, ;
				upper(f) as customdecimal3, ;
				"" as customdecimal4, ;
				"" as customdecimal5, ;
				upper(v) as shiptocode, ;
				padr(iif(v="8710","ALBERTSONS SUNDRIES CNTR",iif(v="8720","ALBERTSONS PONCA CITY-NORTH",iif(v="8750","ALBERTSONS IRVINE",;
					iif(v="8779","PARTNERS",iif(v="8790","ALBERTSONS LANCASTER",iif(v="8220","TOLLESON DISTRIBUTION CENTER",;
					iif(v="8261","BREA DISTRIBUTION CENTER",iif(v="8354","SHAW'S WELLS WARHOUSE",iif(v="8231","SALT LAKE DISTRIBUTION CENTER",;
					iif(v="8252","ALBERTSONS PORTLAND DISTRIBUTION CENTER",iif(v="8500","SAFEWAY",iif(v="8560","SAFEWAY DISTNBUTION CENTER",;
					iif(v="8608","SAFEWAY AUBURN DISTRIBUTION CENTER",iif(v="8913","CPS EAST WAREHOUSE","")))))))))))))),50) as shiptoname, ;
				"" as shiptoaddr1, ;
				"" as shiptoaddr2, ;
				"" as shiptocity, ;
				"" as shiptostate, ;
				"" as shiptocountry, ;
				"" as shiptozip ;
			from xtemp order by &xpocol,&xpolinecol into cursor x850 readwrite

		**need to replace fobloccode - lookup in unloccode
		scan
			**full country name sent in aa, city in ab. Appear to all be China, use that first otherwise lookup just by city (if multiples enter as blank, will error out)
			**may need a full country name to 2-digit country name transalation table if above doesnt work
			select unloccode
			locate for name=alltrim(x850.fobloccode) and country="CN"
			if found()
				replace fobloccode with alltrim(unloccode.country)+alltrim(unloccode.unloccode) in x850
			else
				select * from unloccode where name=alltrim(x850.fobloccode) into cursor xtemp2
				if eof() or reccount("xtemp2")>1
					replace fobloccode with "" in x850
				else
					replace fobloccode with alltrim(xtemp2.country)+alltrim(xtemp2.unloccode) in x850
				endif
				use in xtemp2
			endif
		endscan

		locate

	case xtollcustcode="ITWARKNBB"
		select  &xpocol as ponum, ;
				&xpolinecol as lineno, ;
				"PLC" as action, ;
				"3256" as suppliercode, ;
				"DONGGUAN ARK-LES ELECTRIC COMPONENTS CO., LTD" as suppliername, ;
				"" as supplieraddr1, ;
				"" as supplieraddr2, ;
				"" as suppliercity, ;
				"" as supplierstate, ;
				"" as suppliercountry, ;
				"" as supplierzip, ;
				u as reqdt, ;
				u as orderdt, ;
				"USD" as currency, ;
				"FOB" as incoterms, ; &&mandatory for Icon
				iif(lower(alltrim(r))="air","AIR","SEA") as shipvia, ;
				u as customdate1, ;
				"CNSZX" as fobloccode, ;
				"USELP" as delivport, ;
				a as or_customtext1, ;
				b as or_customtext2, ;
				c as or_customtext3, ;
				"" as contact1, ;
				ab as msg, ;
				f as product, ;
				"" as description, ;
				m as qty, ;
				"" as innerpack, ; &&was col I, moved to col CN (cusatomdecimal5) per rebecca - mvw 10/11/13
				"" as xouterpack, ; &&was col J, moved to col CL (cusatomdecimal3) removed per rebecca - mvw 10/11/13
				"" as uom, ;
				"0.01" as unitprice, ;
				l as itemreqdt, ;
				n as it_customtext1, ;
				o as it_customtext2, ;
				p as it_customtext3, ;
				g as it_customtext4, ;
				ac as it_customtext5, ;
				"" as customdecimal1, ;
				"" as customdecimal2, ;
				j as customdecimal3, ; &&moved from outerpack (BQ) to here - 10/11/13
				"" as customdecimal4, ;
				i as customdecimal5, ; &&moved from innerpack (BP) to here - 10/11/13
				iif(lower(alltrim(c))="wet","50","200") as shiptocode, ;
				c as shiptoname, ;
				"" as shiptoaddr1, ;
				"" as shiptoaddr2, ;
				"" as shiptocity, ;
				"" as shiptostate, ;
				"" as shiptocountry, ;
				"" as shiptozip ;
			from xtemp order by &xpocol,&xpolinecol into cursor x850 readwrite

	otherwise
		xFrom="Toll EDI System"
		xSubject="XLS 850 To ICON Transalation Failed - Customer Code '"+xtollcustcode+"' Not Setup!!"
		xbody="xlstoicon850.prg"
		do form dartmail2 with "mwinter@fmiint.com",xfrom,xsubject," ",,xbody,"a"
		exit
	endcase

	use in xtemp

	select * from x850 ;
		where empty(ponum) or empty(lineno) or empty(action) or empty(suppliercode) or empty(suppliername) or empty(ctod(reqdt)) or empty(ctod(orderdt)) or ;
			empty(currency) or empty(incoterms) or empty(shipvia) or empty(fobloccode) or empty(delivport) or empty(product) or empty(qty) or ;
			empty(unitprice) or empty(shiptocode) or empty(shiptoname) or empty(ctod(itemreqdt));
		into cursor xtemp

	**delete all pos that are missing any required info
	scan
		**send po if only has missing/invalid supplier or ship-to info... send with a value of "UNMATCHED" in name and ID fields - mvw 09/26/13
		**  per Rebecca, see email dated 9/26/13 in Albertson's folder
		if empty(ponum) or empty(lineno) or empty(action) or empty(ctod(reqdt)) or empty(ctod(orderdt)) or empty(currency) or empty(incoterms) or ;
			empty(shipvia) or empty(fobloccode) or empty(delivport) or empty(product) or empty(qty) or empty(unitprice) or empty(ctod(itemreqdt))

			delete for ponum=xtemp.ponum in x850
		endif

		xerrormsg=xerrormsg+alltrim(upper(ponum))+" ("+iif(empty(lineno),"MISSING LINE NO",alltrim(lineno))+"): "+;
			iif(empty(ponum),"PO Num, ","")+iif(empty(lineno),"PO Line No, ","")+iif(empty(action),"PO Action, ","")+;
			iif(empty(suppliercode),"Supplier Code, ","")+iif(empty(suppliername),"Supplier Name, ","")+;
			iif(empty(ctod(reqdt)),"Missing/Invalid RequiredBy Dt, ","")+iif(empty(ctod(orderdt)),"Missing/Invalid Order Dt, ","")+iif(empty(currency),"Currency, ","")+;
			iif(empty(incoterms),"Inco Terms, ","")+iif(empty(shipvia),"Air/Ocean, ","")+iif(empty(fobloccode),"FOB Port, ","")+;
			iif(empty(product),"Product ID, ","")+iif(empty(qty),"Item Qty, ","")+iif(empty(unitprice),"Unit Price, ","")+;
			iif(empty(shiptocode),"Missing Ship To Code, ",iif(empty(shiptoname),"Invalid Ship To Code, ",""))+iif(empty(ctod(itemreqdt)),"Missing/Invalid Item Required Dt, ","")
		xerrormsg=left(xerrormsg,len(xerrormsg)-2)+chr(13)
	endscan

	use in xtemp

	if !empty(xerrormsg)
		xerrormsg="The following list of po lines are missing data or contain invalid data. POs with only missing/invalid supplier & ship-to ID or name "+;
			"errors will be transmitted with an 'UNMATCHED' value into Icon... All other POs on this list will not be transmitted. (Missing/invalid "+;
			"columns are indicated for each PO line): "+chr(13)+chr(13)+xerrormsg+;
			chr(13)+chr(13)+"All other errors are listed below (Again, any pos listed below have not been transmitted):"+chr(13)+chr(13)
	endif

	**need to determine if all lines are cancelled - if all cancelled leave as is, else just delete the line that are cancelled
	select ponum, cnt(1) as totcnt, sum(iif(action="CAN",1,0)) as cancnt from x850 group by ponum into cursor xtemp
	scan for cancnt>0 and totcnt#cancnt
		delete for ponum=xtemp.ponum and action="CAN" in x850
	endscan
	use in xtemp

	select x850
	**send po if only has missing/invalid supplier or ship-to info... send with a value of "UNMATCHED" in name and ID fields - mvw 09/26/13
	**  per Rebecca, see email dated 9/26/13 in Albertson's folder
	replace suppliercode with "UNMATCHED" for empty(suppliercode)
	replace suppliername with "UNMATCHED" for empty(suppliername)
	replace shiptocode with "UNMATCHED" for empty(shiptocode)
	replace shiptoname with "UNMATCHED" for empty(shiptoname)

	locate

	**keep track of previous po line as we have seen instances where PVH has sent the same line number 
	**	multiple times, each containing different skus - mvw 02/14/112
	xprevlineno=""
	**need sln to be defined here now in case i need to combine 2 identical line numbers for same po - mvw 2/14/12
	xslncnt=1

	xerror=.f.
	do while !eof()
		**reset for every po change - mvw 02/14/12
		xprevlineno=""
		xpostr=""

		if used("xpodata") &&only for ITW at this point
			zap in xpodata
		endif

		xponum=alltrim(ponum)

		xaction=alltrim(action)
		xsuppliercode=alltrim(suppliercode)
		xsuppliername=alltrim(suppliername)
		xsupplieraddr1=alltrim(supplieraddr1)
		xsupplieraddr2=alltrim(supplieraddr2)
		xsuppliercity=alltrim(suppliercity)
		xsupplierstate=alltrim(supplierstate)
		xsuppliercountry=alltrim(suppliercountry)
		xsupplierzip=alltrim(supplierzip)

		**change to 3-digit iso country (unloccode.isocountry) - mvw 12/07/11
		if !empty(xsuppliercountry) and len(xsuppliercountry)=2
			=seek(xsuppliercountry,"unloccode","country")
			xsuppliercountry=unloccode.isocountry
		endif

		xcurrency=alltrim(currency)
		xincoterms=alltrim(incoterms)
		xshipvia=alltrim(shipvia)
		xfobloccode=alltrim(fobloccode)
		xdelivport=alltrim(delivport)
		xor_customtext1=alltrim(or_customtext1)
		xor_customtext2=alltrim(or_customtext2)
		xor_customtext3=alltrim(or_customtext3)
		xcontact1=alltrim(contact1)
		xmsg=alltrim(msg)

		xorderdt=alltrim(orderdt)
		xorderdt=right(xorderdt,4)+"-"+left(xorderdt,2)+"-"+substr(xorderdt,4,2)

		xreqdt=alltrim(reqdt)
		xreqdt=right(xreqdt,4)+"-"+left(xreqdt,2)+"-"+substr(xreqdt,4,2)

		xcustomdate1=alltrim(customdate1)
		**if empty or invalid, send ""
		xcustomdate1=iif(empty(xcustomdate1) or empty(ctod(xcustomdate1)),"",right(xcustomdate1,4)+"-"+left(xcustomdate1,2)+"-"+substr(xcustomdate1,4,2))

		do while ponum=padr(xponum,len(ponum))
			xlineno=alltrim(lineno)

			**if duplicate line number for a given po, need to correct - mvw 02/14/12
			if xprevlineno==xlineno
				**skip over duplicate lines, do not send - 10/18/13 mvw
				xerrormsg=xerrormsg+"PO "+xponum+": Line "+xlineno+" Duplicated. Only 1st instance of this line will be sent."+chr(13)
				skip
				loop

				*xslncnt=xslncnt+1
			else
				xslncnt=1
			endif

			xprevlineno=xlineno

			xproduct=alltrim(product)
			xdescription=alltrim(description)

			xqty=alltrim(qty)
			xqty=transform(val(xqty)) &&remove unnecesaary "0" decimal places, ie 50.000000 - mvw 04/16/13

			xinnerpack=alltrim(innerpack)
			xouterpack=alltrim(xouterpack)
			xuom=alltrim(uom)
			xunitprice=alltrim(unitprice)

			xit_customtext1=alltrim(it_customtext1)
			xit_customtext2=alltrim(it_customtext2)
			xit_customtext3=alltrim(it_customtext3)
			xit_customtext4=alltrim(it_customtext4)
			xit_customtext5=alltrim(it_customtext5)

			xcustomdecimal1=alltrim(customdecimal1)
			xcustomdecimal2=alltrim(customdecimal2)
			xcustomdecimal3=alltrim(customdecimal3)
			xcustomdecimal4=alltrim(customdecimal4)
			xcustomdecimal5=alltrim(customdecimal5)

			xshiptocode=alltrim(shiptocode)
			xshiptoname=alltrim(shiptoname)
			xshiptoaddr1=alltrim(shiptoaddr1)
			xshiptoaddr2=alltrim(shiptoaddr2)
			xshiptocity=alltrim(shiptocity)
			xshiptostate=alltrim(shiptostate)
			xshiptocountry=alltrim(shiptocountry)
			xshiptozip=alltrim(shiptozip)

			**change to 3-digit iso country (unloccode.isocountry) - mvw 12/07/11
			if !empty(xshiptocountry) and len(xshiptocountry)=2
				=seek(xshiptocountry,"unloccode","country")
				xshiptocountry=unloccode.isocountry
			endif

			xitemreqdt=alltrim(itemreqdt)
			xditemreqdt=ctod(xitemreqdt) &&actual date var
			xitemreqdt=right(xitemreqdt,4)+"-"+left(xitemreqdt,2)+"-"+substr(xitemreqdt,4,2)

			if xerror
				exit
			endif

			**this should be last segment inside the item loop (PO1 loop), write to file here
			=writetostr() &&procedure below within this prg

			if used("xpodata") &&only for ITW at this point
				insert into xpodata (po_num,lineno,itemno,ctnqty,pack,origqty,shipqty,shiptodate,reqdt) values ;
					(xponum,xlineno,xproduct,val(xouterpack),val(xinnerpack),val(xqty),val(xqty),val(xit_customtext1),xditemreqdt)
			endif

			skip
		enddo

		xpochange=.t.

		**check if PO was changed,ITW sends all POs in system every transmission
		if xtollcustcode="ITWARKNBB"
			xpochange=.f.

			if !seek(padr(xponum,len(podata.po_num)),"podata","po_num")
				xpochange=.t.
			else
				select podata
				scan for po_num=padr(xponum,len(podata.po_num))
					select xpodata
					locate for lineno=podata.lineno
					if !found()
						xpochange=.t.
						exit
					else
						replace found with .t. in xpodata
						if itemno#podata.itemno or ctnqty#podata.ctnqty or pack#podata.pack or origqty#podata.origqty or shipqty#podata.shipqty or ;
						shiptodate#podata.shiptodate or reqdt#podata.reqdt
							xpochange=.t.
							exit
						endif
					endif
				endscan

				if !xpochange
					select xpodata
					locate for !found
					if found()
						xpochange=.t.
					endif
				endif

				select x850
			endif

			if xpochange
				delete for po_num=padr(xponum,len(podata.po_num)) in podata
				insert into podata select po_num,lineno,itemno,ctnqty,pack,origqty,shipqty,shiptodate,reqdt from xpodata
			endif
		endif
		**end ITW PO change verification

		select x850
		if xerror
			**move to next po
			if !eof()
				count while d=padr(xponum,len(d)) to xx
			endif
			xerror=.f.
		else
			**if no changes to the po, do not include in file
			if xpochange
				xfilestr=xfilestr+upper(xpostr)
			endif
		endif
	enddo

	select x850
	locate

	if !empty(xfilestr)
		**start with column headings
		xfilestr="order_no|order_split|order_status|client_cust_code|supplier_code|supplier_name|supplier_address_1|"+;
			"supplier_address_2|supplier_city|supplier_region_state|supplier_country|supplier_postcode|confirm_number|"+;
			"confirm_date|exw_required_by|delivery_required_by|description|order_date_time|order_value|order_currency_code|"+;
			"exchange_rate|incoterms|additional_terms|transport_mode|container_mode|milestone_tracking_date_exw|"+;
			"milestone_tracking_date_etd|milestone_tracking_date_eta|custom_milestone_est1|custom_milestone_act1|"+;
			"custom_milestone_est2|custom_milestone_act2|custom_milestone_est3|custom_milestone_act3|custom_milestone_est4|"+;
			"custom_milestone_act4|sending_agent|sending_agent_name|origin_port|destination_port|custom_text1|custom_text2|"+;
			"custom_text3|custom_text4|custom_text5|custom_date1|custom_date2|custom_decimal1|custom_decimal2|custom_decimal3|"+;
			"custom_decimal4|custom_decimal5|custom_flag1|custom_flag2|custom_flag3|custom_flag4|custom_flag5|contact1|"+;
			"contact2|country_code|notes|line_no|subline_no|linesplit_no|product|line_description|qty_ordered|innerpacks|"+;
			"outerpacks|unit_of_measure|itemprice|lineprice|linestatus|required_date|part_attrib1|part_attrib2|part_attrib3|"+;
			"line_custom_attrib1|line_custom_attrib2|line_custom_attrib3|line_custom_attrib4|line_custom_attrib5|"+;
			"line_custom_date1|line_custom_date2|line_custom_date3|line_custom_date4|line_custom_date5|line_custom_decimal1|"+;
			"line_custom_decimal2|line_custom_decimal3|line_custom_decimal4|line_custom_decimal5|line_custom_flag1|"+;
			"line_custom_flag2|line_custom_flag3|line_custom_flag4|line_custom_flag5|line_custom_text1|container_number|"+;
			"container_packing_order|country_of_origin|dg_substance|dg_flashpoint|weight|volume|weight_type|volume_type|"+;
			"special_instructions|additional_information|delivery_port|address_code|delivery_address_name|"+;
			"delivery_address_address1|delivery_address_address2|delivery_address_city|delivery_address_state|"+;
			"delivery_address_country|delivery_address_postcode"+chr(13)+chr(10)+xfilestr

		do while .t.
			set date ymd
			xoutfile="PO_TGFDI_"+strtran(strtran(strtran(ttoc(datetime()),":","")," ",""),"/")+xtollcustcode+".txt"
			set date american

			xarchiveoutfile=xoutdir+"archive\"+xoutfile
			if !file(xarchiveoutfile)
				exit
			endif
		enddo

		xoutfile=xoutdir+xoutfile
		strtofile(xfilestr,xoutfile)
		copy file &xoutfile to &xarchiveoutfile

		xfilecreated=.t.
	endif

	if !empty(xerrormsg)
		xfrom ="TGFSYSTEM"
		xerrormsg="Error in file '"+xfilename+"'"+chr(13)+chr(13)+xerrormsg
		do form dartmail2 With "mwinter@fmiint.com, rebecca.h.ma@tollgroup.com",xFrom,xcustname+" 850 Translation Error: File '"+xfilename+"'"," ","",xerrormsg,"A"
	endif

	copy file "&xfile" to "&xarchiveinfile"
	delete file "&xfile"
endscan

if xfilecreated and !empty(xftpjob)
	**create an ftpjob
	use f:\edirouting\ftpjobs in 0
	insert into ftpjobs (jobname, userid, jobtime) values (xftpjob,"XLSTOTOLL850",datetime())
	use in ftpjobs
endif

use in x850
use in xfilelist
use in unloccode
use in uom

if used("podata")
	use in podata
endif

wait clear


procedure writetostr

	**moved xhdrstr definition here from the top of the PO1 case because xshipvia isn't defined until after PO1 in the TD5 seg
	xhdrstr=alltrim(xponum)+"|"+; &&A
		"|"+; &&column(s) B empty
		xaction+"|"+; &&C
		xtollcustcode+"|"+;&&D
		xsuppliercode+"|"+; &&E
		xsuppliername+"|"+; &&F
		xsupplieraddr1+"|"+; &&G
		xsupplieraddr2+"|"+; &&H
		xsuppliercity+"|"+; &&I
		xsupplierstate+"|"+; &&J
		xsuppliercountry+"|"+; &&K
		xsupplierzip+"|"+; &&L
		"||"+; &&column(s) M-N empty
		xreqdt+"|"+; &&O
		"||"+; &&column(s) P-Q empty
		xorderdt+"|"+; &&R
		"|"+; &&column(s) S empty
		xcurrency+"|"+; &&T
		"|"+; &&column(s) U empty
		xincoterms+"|"+; &&V
		"|"+; &&column(s) W empty
		xshipvia+"|"+; &&X
		"||"+; &&column(s) Y-Z is empty
		"|"+; &&AA
		"|"+; &&column(s) AB is empty
		xcustomdate1+"|"+; &&AC
		"|||||||||"+; &&column(s) AD-AL empty
		xfobloccode+"|"+; &&AM
		xdelivport+"|"+; &&AN
		xor_customtext1+"|"+; &&AO
		xor_customtext2+"|"+; &&AP
		xor_customtext3+"|"+; &&AQ
		"|||||||||"+; &&column(s) AR-AZ
		"|"+; &&BA (860 flag)
		"||||"+; &&column(s) BB-BE empty
		xcontact1+"|"+; &&BF
		"|"+; &&BG
		"|"+; &&BH
		xmsg+"|" &&BI


	**only add po line if the qty>0, in case Calderon sends cancelled lines with a 0 qty like AE does - mvw 05/09/12
	if val(xqty)>0
		**must change UOM from 2-digit to 3-digit for ICON translation - mvw 04/11/13
		if len(xuom)=2
			**default to units (UNT) if not found is UOM master
			**if the uom3 is only 2 digits, need to trim per Bill L - mvw 07/02/14
			xuom=alltrim(iif(seek(xuom,"uom","uom2"),uom.uom3,"UNT"))
		endif

		**removed the "-" from the lineno as ICON could not accept it. Now line number "001" will go as "0011" 
		**  for the 1st SLN, "0012" for the 2nd and so on. The right-most character will need to be stripped 
		**  for the 856 going back to PVH - mvw 01/12/12
		*xlineno+"-"+transform(xslncnt)+"|" &&BJ
		**no need to add slncnt to po line as there should only be 1 sln per po1 seg
		*xlineno+padl(transform(xslncnt),2,"0")+"|" &&BJ
		xpostr=xpostr+xhdrstr+;
			xlineno+"|"+; &&BJ
			"||"+; &&column(s) BK-BL empty
			xproduct+"|"+; &&BM
			xdescription+"|"+; &&BN
			transform(xqty)+"|"+; &&BO
			xinnerpack+"|"+; &&BP
			xouterpack+"|"+; &&BQ
			xuom+"|"+; &&BR
			transform(xunitprice)+"|"+; &&BS
			"|"+; &&column(s) BT empty
			"PLC"+"|"+; &&BU (always PLC)
			xitemreqdt+"|"+; &&BV
			"|"+; &&BW
			"|"+; &&BX - empty, only need 2 attributes style and sku
			"|"+; &&BY - empty, had held the 1st HTS # - mvw 02/10/12
			xit_customtext1+"|"+; &&BZ
			xit_customtext2+"|"+; &&CA
			xit_customtext3+"|"+; &&CB
			xit_customtext4+"|"+; &&CC
			xit_customtext5+"|"+; &&CD
			"|||||||"+; &&column(s) CE-CK
			xcustomdecimal3+"|"+; &&CL
			"||||||||||"+; &&column(s) CM-CV empty
			"|"+; &&CW
			"||||||||"+;&&column(s) CX-DE empty
			xdelivport+"|"+; &&DF
			xshiptocode+"|"+; &&DG
			xshiptoname+"|"+; &&DH
			xshiptoaddr1+"|"+; &&DI
			xshiptoaddr2+"|"+; &&DJ
			xshiptocity+"|"+; &&DK
			xshiptostate+"|"+; &&DL
			xshiptocountry+"|"+; &&DM
			xshiptozip+; &&DN
			chr(13)+chr(10)
	endif

return
