parameters xaccountid

* replaced column calcs using chr() with calls to GetXLAlphaColumn() to handle columns beyond "Z" 4/28/15 MB

xfile="h:\fox\ctraging.xls"

if file("&xfile")
	xerror=.f.
	try
		delete file &xfile
	catch
		wait "Error deleting file... Cannot continue." window timeout 3
		xerror=.t.
	endtry
	if xerror
		return .f.
	endif
endif

wait window "Collecting data..." nowait

**wotype field added for TJX (6565) - mvw 07/19/16
select *, space(30) as aremarks, iif(addby="TOLLPROC","DI ","POE") as xtype from wolog ;
	where accountid=xaccountid and wo_date>date()-60 and pickedup={} and type="O" ;
	into cursor xbackup readwrite

xacctname=alltrim(acctname)

scan
	xsqlexec("select remarks from availlog where wo_num="+transform(xbackup.wo_num)+" order by adddt desc","xtemp",,"wo")
	
	replace aremarks with xtemp.remarks in xbackup
	locate for inlist(remarks,"D.O. CANCEL","PICKED UP BY ANOTHER COMPANY")
	if found()
		delete in xbackup
	endif
	use in xtemp
endscan

select office, puloc, availdt, sum(1) as totcnt from xbackup group by office, puloc, availdt into cursor xrpt

xnodata=iif(reccount('xrpt')=0,.t.,.f.)

if !xnodata
	wait window "Creating spreadsheet..." nowait

	oexcel=createobject("excel.application")
	oexcel.sheetsinnewworkbook=2
	oworkbook=oexcel.workbooks.add()
	oworkbook.worksheets[1].name="Summary"
	oworkbook.worksheets[2].name="Detail"

	osheet1=oworkbook.worksheets[1]
	xsheet="osheet1"

	**exceltext params: xrange, xtext, xformat, xsize, xnumberformat, xblankifzero, xbackcolor

	**get # of avail date columns required (how many different dates exist)
	select * from xrpt where availdt#{} group by availdt into cursor xtemp readwrite
	xcolcnt=reccount()+4 &&add 4 for terminal & not avail & totals columns
	*xlastcol=chr(65+(xcolcnt-1)) &&need alpha column, start at A (65) and add total # of columns -1
	xlastcol = GetXLAlphaColumn(xcolcnt) &&need alpha column, start at A (65) and add total # of columns -1

	osheet1.columns[1].columnwidth=35
	for i = 2 to xcolcnt
		osheet1.columns[i].columnwidth=10
	endfor

	=exceltext("A1","Available Container Aging Report","bold",14)
	=exceltext("A2","Account: "+xacctname,"bold")
	=exceltext("A3","Run Time: "+transform(datetime()),"bold")

	=exceltext("C4","Available","bold center",,,,"RGB(255,255,128)")
	*osheet1.range("C4:"+chr(64+(xcolcnt-2))+"4").merge
	osheet1.range("C4:" + GetXLAlphaColumn(xcolcnt-2) + "4").merge

	xhdrrow=5

	=exceltext("A"+transform(xhdrrow),"Terminal","bold center",,,,"RGB(255,255,128)")
	=exceltext("B"+transform(xhdrrow),"Not Avail","bold center",,,,"RGB(255,255,128)")

	select xtemp
	*xcol=67
	xcol=3
	scan
		*=exceltext(chr(xcol)+transform(xhdrrow),left(dtoc(availdt),5),"bold center",,,,"RGB(255,255,128)")
		=exceltext(GetXLAlphaColumn(xcol)+transform(xhdrrow),left(dtoc(availdt),5),"bold center",,,,"RGB(255,255,128)")
		xcol=xcol+1
	endscan
	use in xtemp

	*=exceltext(chr(xcol)+transform(xhdrrow),"Avail Total","bold center",,,,"RGB(255,255,128)")
	*=exceltext(chr(xcol+1)+transform(xhdrrow),"Grand Total","bold center",,,,"RGB(255,255,128)")
	=exceltext(GetXLAlphaColumn(xcol)+transform(xhdrrow),"Avail Total","bold center",,,,"RGB(255,255,128)")
	=exceltext(GetXLAlphaColumn(xcol+1)+transform(xhdrrow),"Grand Total","bold center",,,,"RGB(255,255,128)")

	xrow=xhdrrow+1
	select xrpt
	do while !eof()
		xxoffice=office
		xstartrow=xrow
		do while office=xxoffice and !eof()
			xxpuloc=puloc
			scan while puloc=xxpuloc
				if empty(osheet1.range("A"+transform(xrow)).value) or isnull(osheet1.range("A"+transform(xrow)).value)
					osheet1.range("A"+transform(xrow)).value = alltrim(puloc)
				endif

				if emptynul(availdt)
					osheet1.range("B"+transform(xrow)).value = totcnt
				else
					for i = 3 to xcolcnt-2
						*xdate=osheet1.range(chr(64+i)+transform(xhdrrow)).value
						xdate=osheet1.range(GetXLAlphaColumn(i)+transform(xhdrrow)).value
						if availdt=ctod(transform(iif(type("xdate")="T",ttod(xdate),xdate)))
							*osheet1.range(chr(64+i)+transform(xrow)).value = totcnt
							osheet1.range(GetXLAlphaColumn(i)+transform(xrow)).value = totcnt
						endif
					endfor
				endif
			endscan

			select sum(totcnt) as totcnt, sum(iif(!emptynul(availdt),totcnt,0)) as availcnt from xrpt where office=xxoffice and puloc=xxpuloc into cursor xtemp
			*osheet1.range(chr(64+xcolcnt-1)+transform(xrow)).value = availcnt
			osheet1.range(GetXLAlphaColumn(xcolcnt-1)+transform(xrow)).value = availcnt
			*osheet1.range(chr(64+xcolcnt)+transform(xrow)).value = totcnt
			osheet1.range(GetXLAlphaColumn(xcolcnt)+transform(xrow)).value = totcnt
			use in xtemp

			select xrpt
			xrow=xrow+1
		enddo

		xvalue=iif(xxoffice="N","NJ","CA")+" Totals"
		=exceltext("A"+transform(xrow),xvalue,"bold",,,,"RGB(255,255,128)")
		=exceltext("B"+transform(xrow),"=sum(B"+transform(xstartrow)+":B"+transform(xrow-1),"bold",,,,"RGB(255,255,128)")
		for i = 3 to xcolcnt-2
			*=exceltext(chr(64+i)+transform(xrow),"=sum("+chr(64+i)+transform(xstartrow)+":"+chr(64+i)+transform(xrow-1),"bold",,,,"RGB(255,255,128)")
			=exceltext(GetXLAlphaColumn(i)+transform(xrow),"=sum("+GetXLAlphaColumn(i)+transform(xstartrow)+":"+GetXLAlphaColumn(i)+transform(xrow-1),"bold",,,,"RGB(255,255,128)")
		endfor
		*=exceltext(chr(64+xcolcnt-1)+transform(xrow),"=sum("+chr(64+xcolcnt-1)+transform(xstartrow)+":"+chr(64+xcolcnt-1)+transform(xrow-1),"bold",,,,"RGB(255,255,128)")
		=exceltext(GetXLAlphaColumn(xcolcnt-1)+transform(xrow),"=sum("+GetXLAlphaColumn(xcolcnt-1)+transform(xstartrow)+":"+GetXLAlphaColumn(xcolcnt-1)+transform(xrow-1),"bold",,,,"RGB(255,255,128)")
		*=exceltext(chr(64+xcolcnt)+transform(xrow),"=sum("+chr(64+xcolcnt)+transform(xstartrow)+":"+chr(64+xcolcnt)+transform(xrow-1),"bold",,,,"RGB(255,255,128)")
		=exceltext(GetXLAlphaColumn(xcolcnt)+transform(xrow),"=sum("+GetXLAlphaColumn(xcolcnt)+transform(xstartrow)+":"+GetXLAlphaColumn(xcolcnt)+transform(xrow-1),"bold",,,,"RGB(255,255,128)")

		xrow=xrow+2
	enddo

	select sum(iif(emptynul(availdt),totcnt,0)) as notavail, sum(iif(!emptynul(availdt),totcnt,0)) as avail from xrpt into cursor xtemp
	=exceltext("A"+transform(xrow),"Grand Totals","bold",,,,"RGB(255,255,128)")
	=exceltext("B"+transform(xrow),notavail,"bold",,,,"RGB(255,255,128)")
	for i = 3 to xcolcnt-2
		**just change backcolor
		*=exceltext(chr(64+i)+transform(xrow),,"bold",,,,"RGB(255,255,128)")
		=exceltext(GetXLAlphaColumn(i)+transform(xrow),,"bold",,,,"RGB(255,255,128)")
	endfor
	*=exceltext(chr(64+xcolcnt-1)+transform(xrow),avail,"bold",,,,"RGB(255,255,128)")
	=exceltext(GetXLAlphaColumn(xcolcnt-1)+transform(xrow),avail,"bold",,,,"RGB(255,255,128)")
	*=exceltext(chr(64+xcolcnt)+transform(xrow),notavail+avail,"bold",,,,"RGB(255,255,128)")
	=exceltext(GetXLAlphaColumn(xcolcnt)+transform(xrow),notavail+avail,"bold",,,,"RGB(255,255,128)")
	use in xtemp

	xrow=xrow+3


	**create the Appointment Summary Grid - added mvw 06/01/16
	select office, puloc, appt, sum(1) as totcnt from xbackup group by office, puloc, appt where availdt#{} into cursor xrpt

	**get # of appt date columns required (how many different dates exist)
	select * from xrpt where appt#{} group by appt into cursor xtemp readwrite
	xcolcnt=reccount()+5 &&add 5 for terminal, waiting appt, ano appt req'd & 2 totals columns
	xlastcol = GetXLAlphaColumn(xcolcnt) &&need alpha column, start at A (65) and add total # of columns -1

	osheet1.columns[1].columnwidth=35
	for i = 2 to xcolcnt
		osheet1.columns[i].columnwidth=10
	endfor

	=exceltext("B"+transform(xrow),"Appointments For Available Containers","bold center",,,,"RGB(255,255,128)")
	osheet1.range("B"+transform(xrow)+":" + GetXLAlphaColumn(xcolcnt-2) + transform(xrow)).merge

	xhdrrow=xrow+1

	osheet1.range("A"+transform(xhdrrow)).rowheight=26
	=exceltext("A"+transform(xhdrrow),"Terminal","bold center",,,,"RGB(255,255,128)")

	osheet1.range("B"+transform(xhdrrow)).wraptext=.t.
	osheet1.range("C"+transform(xhdrrow)).wraptext=.t.
	=exceltext("B"+transform(xhdrrow),"No Appt Required","bold center",,,,"RGB(255,255,128)")
	=exceltext("C"+transform(xhdrrow),"Awaiting Appt","bold center",,,,"RGB(255,255,128)")

	select xtemp
	xcol=4
	scan
		=exceltext(GetXLAlphaColumn(xcol)+transform(xhdrrow),left(dtoc(appt),5),"bold center",,,,"RGB(255,255,128)")
		xcol=xcol+1
	endscan
	use in xtemp

	=exceltext(GetXLAlphaColumn(xcol)+transform(xhdrrow),"Appt Total","bold center",,,,"RGB(255,255,128)")
	=exceltext(GetXLAlphaColumn(xcol+1)+transform(xhdrrow),"Grand Total","bold center",,,,"RGB(255,255,128)")

	xawaitingappt=0
	xnoappt=0

	xrow=xhdrrow+1
	select xrpt
	do while !eof()
		xxoffice=office
		xstartrow=xrow
		do while office=xxoffice and !eof()
			xxpuloc=puloc
			scan while puloc=xxpuloc
				if empty(osheet1.range("A"+transform(xrow)).value) or isnull(osheet1.range("A"+transform(xrow)).value)
					osheet1.range("A"+transform(xrow)).value = alltrim(puloc)
				endif

				if emptynul(appt)
					**if puloc is in the list of pulocs that require appts OR there are any appts for this puloc/office, place this # in 
					**  "awaiting appt" column (C). Otherwise goes in "No Appt Required" column (B) - mvw 06/02/16
					select cnt(1) as cnt from xrpt where office=xxoffice and puloc=xxpuloc into cursor xtemp

					select xrpt
					if xtemp.cnt>1 or inlist(xxpuloc,"APL","APM","EVERGREEN","HANJIN","ITS","LBCT PIER E","WBCT")
						osheet1.range("C"+transform(xrow)).value = totcnt
						xawaitingappt=xawaitingappt+totcnt
					else
						osheet1.range("B"+transform(xrow)).value = totcnt
						xnoappt=xnoappt+totcnt
					endif
					use in xtemp
				else
					for i = 4 to xcolcnt-2
						*xdate=osheet1.range(chr(64+i)+transform(xhdrrow)).value
						xdate=osheet1.range(GetXLAlphaColumn(i)+transform(xhdrrow)).value
						if appt=ctod(transform(iif(type("xdate")="T",ttod(xdate),xdate)))
							osheet1.range(GetXLAlphaColumn(i)+transform(xrow)).value = totcnt
						endif
					endfor
				endif
			endscan

			select sum(totcnt) as totcnt, sum(iif(!emptynul(appt),totcnt,0)) as apptcnt from xrpt where office=xxoffice and puloc=xxpuloc into cursor xtemp
			osheet1.range(GetXLAlphaColumn(xcolcnt-1)+transform(xrow)).value = apptcnt
			osheet1.range(GetXLAlphaColumn(xcolcnt)+transform(xrow)).value = totcnt
			use in xtemp

			select xrpt
			xrow=xrow+1
		enddo

		xvalue=iif(xxoffice="N","NJ","CA")+" Totals"
		=exceltext("A"+transform(xrow),xvalue,"bold",,,,"RGB(255,255,128)")
		=exceltext("B"+transform(xrow),"=sum(B"+transform(xstartrow)+":B"+transform(xrow-1),"bold",,,,"RGB(255,255,128)")
		=exceltext("C"+transform(xrow),"=sum(C"+transform(xstartrow)+":C"+transform(xrow-1),"bold",,,,"RGB(255,255,128)")
		for i = 4 to xcolcnt-2
			=exceltext(GetXLAlphaColumn(i)+transform(xrow),"=sum("+GetXLAlphaColumn(i)+transform(xstartrow)+":"+GetXLAlphaColumn(i)+transform(xrow-1),"bold",,,,"RGB(255,255,128)")
		endfor
		=exceltext(GetXLAlphaColumn(xcolcnt-1)+transform(xrow),"=sum("+GetXLAlphaColumn(xcolcnt-1)+transform(xstartrow)+":"+GetXLAlphaColumn(xcolcnt-1)+transform(xrow-1),"bold",,,,"RGB(255,255,128)")
		=exceltext(GetXLAlphaColumn(xcolcnt)+transform(xrow),"=sum("+GetXLAlphaColumn(xcolcnt)+transform(xstartrow)+":"+GetXLAlphaColumn(xcolcnt)+transform(xrow-1),"bold",,,,"RGB(255,255,128)")

		xrow=xrow+2
	enddo

	select sum(iif(!emptynul(appt),totcnt,0)) as appt from xrpt into cursor xtemp
	=exceltext("A"+transform(xrow),"Grand Totals","bold",,,,"RGB(255,255,128)")
	=exceltext("B"+transform(xrow),xnoappt,"bold",,,,"RGB(255,255,128)")
	=exceltext("C"+transform(xrow),xawaitingappt,"bold",,,,"RGB(255,255,128)")
	for i = 4 to xcolcnt-2
		**just change backcolor
		=exceltext(GetXLAlphaColumn(i)+transform(xrow),,"bold",,,,"RGB(255,255,128)")
	endfor
	=exceltext(GetXLAlphaColumn(xcolcnt-1)+transform(xrow),appt,"bold",,,,"RGB(255,255,128)")
	=exceltext(GetXLAlphaColumn(xcolcnt)+transform(xrow),xawaitingappt+xnoappt+appt,"bold",,,,"RGB(255,255,128)")
	use in xtemp


	**start detail page
	osheet1=oworkbook.worksheets[2]
	=exceltext("A1","W/O","bold center",,,,"RGB(255,255,128)")
	=exceltext("B1","W/O Date","bold center",,,,"RGB(255,255,128)")
	=exceltext("C1","Office","bold center",,,,"RGB(255,255,128)")
	=exceltext("D1","Container","bold center",,,,"RGB(255,255,128)")
	=exceltext("E1","Terminal","bold center",,,,"RGB(255,255,128)")
	=exceltext("F1","ETA","bold center",,,,"RGB(255,255,128)")
	=exceltext("G1","Avail","bold center",,,,"RGB(255,255,128)")
	=exceltext("H1","Appt","bold center",,,,"RGB(255,255,128)")
	=exceltext("I1","Time","bold center",,,,"RGB(255,255,128)")
	=exceltext("J1","Note","bold",,,,"RGB(255,255,128)")

	**TJX (6565) only field - mvw 07/19/16
	if xaccountid=6565
		=exceltext("K1","Type","bold",,,,"RGB(255,255,128)")
	endif

	select xbackup
	xrow=2
	scan
		osheet1.range("A"+transform(xrow)).value = wo_num
		osheet1.range("B"+transform(xrow)).value = transform(wo_date)
		osheet1.range("C"+transform(xrow)).value = alltrim(office)
		osheet1.range("D"+transform(xrow)).value = alltrim(container)
		osheet1.range("E"+transform(xrow)).value = alltrim(puloc)
		osheet1.range("F"+transform(xrow)).value = transform(eta)
		osheet1.range("G"+transform(xrow)).value = transform(availdt)
		osheet1.range("H"+transform(xrow)).value = transform(appt)
		osheet1.range("I"+transform(xrow)).value = transform(appttm)
		osheet1.range("J"+transform(xrow)).value = transform(aremarks)

		**TJX (6565) only field - mvw 07/19/16
		if xaccountid=6565
			osheet1.range("K"+transform(xrow)).value = xtype
		endif

		xrow=xrow+1
	endscan

	oworkbook.saveas(xfile,39)

	oExcel.quit &&release the file before attempting to send
	release oexcel

	*run "&xfile"
endif

wait clear

use in xbackup
use in xrpt

return !xnodata