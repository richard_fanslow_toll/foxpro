DO M:\DEV\PRG\_SETVARS WITH .T.
PUBLIC cfilename,cfileoutname,NormalExit,cErrMsg,tfrom,cTransfer
PUBLIC ARRAY a856(1)
CLOSE DATA ALL

TRY
	lTesting = .F.
	lOverrideBusy = lTesting
	NormalExit = .F.
	cErrMsg = ""
	tfrom = "Toll EDI Operations <toll-edi-ops@tollgroup.com>"


	IF !lTesting
		SELECT 0
		USE F:\edirouting\ftpsetup SHARED
		cTransfer = "ANF-MOVE"
		LOCATE FOR ftpsetup.transfer = cTransfer
		IF ftpsetup.chkbusy = .T. AND !lOverrideBusy
			WAIT WINDOW "File is processing...exiting" TIMEOUT 2
			NormalExit = .T.
			THROW
		ENDIF
		REPLACE chkbusy WITH .T. FOR ftpsetup.transfer = cTransfer
		USE IN ftpsetup
	ENDIF

	SELECT 0
	USE F:\3pl\DATA\mailmaster ALIAS mm SHARED
	LOCATE
	LOCATE FOR mm.edi_type = "MISC" AND mm.taskname = "GENERAL"
	STORE TRIM(IIF(mm.use_alt,mm.sendtoalt,mm.sendto)) TO tsendtoerr
	STORE TRIM(IIF(mm.use_alt,mm.ccalt,mm.cc)) TO tccerr
	USE IN mm

	cDirIn = "f:\ftpusers\ANF\asnin\"
	CD &cDirIn

	_SCREEN.WINDOWSTATE=IIF(lTesting,2,1)
	_SCREEN.CAPTION="ANF File Move Process"

	nFound = ADIR(ary1,'*.edi')
	IF nFound = 0
		CLOSE DATABASES ALL
		WAIT WINDOW "No ANF 214 files found to process...exiting" TIMEOUT 2
		NormalExit = .t.
		throw
	ENDIF

	len1 = ALEN(ary1,1)

	FOR i = 1 TO len1
*ASSERT .F. MESSAGE "In move loop"
		cfilename = ALLT(ary1[i,1])
		xfile = (cDirIn+cfilename)
		cTempstr = FILETOSTR(xfile)
		IF "ST*214"$cTempstr
*!*				IF lTesting
*!*					WAIT WINDOW "214 file found in ASNIN folder...will move when in production" TIMEOUT 3
*!*				ELSE
				WAIT WINDOW "214 file found in ASNIN folder...moving" TIMEOUT 3
				cDirOut = ("F:\ftpusers\anf\214in\")
				cfileoutname = (cDirOut+cfilename)
				COPY FILE [&xfile] TO [&cFileOutName]
				IF FILE(cfileoutname)
					IF FILE(xfile)
						DELETE FILE [&xfile]
					ENDIF
				ENDIF
*!*				ENDIF
			LOOP
		ENDIF

	ENDFOR

	WAIT WINDOW "ANF move process complete" TIMEOUT 1
	IF !lTesting
		SELECT 0
		USE F:\edirouting\ftpsetup SHARED
		cTransfer = "ANF-MOVE"
		REPLACE chkbusy WITH .F. FOR ftpsetup.transfer = cTransfer
	ENDIF
	CLOSE DATABASES ALL
	WAIT CLEAR
	NormalExit = .T.
	WAIT WINDOW "All A&F files moved" TIMEOUT 1

CATCH TO oErr
	IF NormalExit = .F.
		ASSERT .F. MESSAGE "In Catch section..."
		tsubject = "ANF File Transfer Error ("+TRANSFORM(oErr.ERRORNO)+") at "+TTOC(DATETIME())
		tsendto  = tsendtoerr
		tcc = tccerr
		tmessage = "ANF File transfer Error:"

		IF !EMPTY(cErrMsg)
			tmessage =tmessage+CHR(13)+cErrMsg
		ELSE
			lcSourceMachine = SYS(0)
			lcSourceProgram = "from merkury940 group"
			tmessage =tmessage+ "Try/CATCH Exeception Message:"+CHR(13)+;
				[  Error: ] + STR(oErr.ERRORNO) +CHR(13)+;
				[  LineNo: ] + STR(oErr.LINENO) +CHR(13)+;
				[  Message: ] + oErr.MESSAGE +CHR(13)+;
				[  Procedure: ] + oErr.PROCEDURE +CHR(13)+;
				[  Details: ] + oErr.DETAILS +CHR(13)+;
				[  StackLevel: ] + STR(oErr.STACKLEVEL) +CHR(13)+;
				[  LineContents: ] + oErr.LINECONTENTS+CHR(13)+;
				[  UserValue: ] + oErr.USERVALUE+CHR(13)+;
				[  Computer:  ] +lcSourceMachine+CHR(13)+;
				[  940 file:  ] +xfile+CHR(13)+;
				[  Program:   ] +lcSourceProgram
		ENDIF
		tattach = ""

		DO FORM M:\DEV\frm\dartmail2 WITH tsendto,tfrom,tsubject,tcc,tattach,tmessage,"A"
	ELSE
		WAIT WINDOW AT 10,10 "Normal Exit " TIMEOUT 2
	ENDIF
FINALLY
	ON ERROR
	SET STATUS BAR ON
	CLOSE DATABASES ALL
ENDTRY

